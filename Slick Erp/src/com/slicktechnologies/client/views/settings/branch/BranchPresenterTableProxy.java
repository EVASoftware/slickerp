package com.slicktechnologies.client.views.settings.branch;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;

public class BranchPresenterTableProxy extends SuperTable<Branch>{

	TextColumn<Branch> getBranchIdCol;
	TextColumn<Branch> getBranchNameCol;
	TextColumn<Branch> getBranchPhnCol;
	TextColumn<Branch> getBranchPocNmCol;
	TextColumn<Branch> getBranchPocCellCol;
	TextColumn<Branch> getBranchStatusCol;
	
	@Override
	public void createTable() {
		getBranchIdCol();
		getBranchNameCol();
		getBranchPhnCol();
		getBranchPocNmCol();
		getBranchPocCellCol();
		getBranchStatusCol();
		
	}
	
	

	private void getBranchIdCol() {
		// TODO Auto-generated method stub
		getBranchIdCol=new TextColumn<Branch>() {
			@Override
			public String getValue(Branch object) {
				// TODO Auto-generated method stub
				return object.getCount()+"";
			}
		};
		table.addColumn(getBranchIdCol, "Id");
		table.setColumnWidth(getBranchIdCol, 90, Unit.PX);
		getBranchIdCol.setSortable(true);
	}



	private void getBranchNameCol() {
		// TODO Auto-generated method stub
		getBranchNameCol=new TextColumn<Branch>() {
			@Override
			public String getValue(Branch object) {
				// TODO Auto-generated method stub
				if(object.getBusinessUnitName()!=null){
					return object.getBusinessUnitName();
				}
				return "";	
			}
		};
		table.addColumn(getBranchNameCol, "Name");
		table.setColumnWidth(getBranchNameCol, 90, Unit.PX);
		getBranchNameCol.setSortable(true);
	}



	private void getBranchPhnCol() {
		// TODO Auto-generated method stub
		getBranchPhnCol=new TextColumn<Branch>() {
			@Override
			public String getValue(Branch object) {
				// TODO Auto-generated method stub
				if(object.getCellNumber1()!=null){
					return object.getCellNumber1()+"";
				}
				return "";	
			}
		};
		table.addColumn(getBranchPhnCol, "Phone");
		table.setColumnWidth(getBranchPhnCol, 90, Unit.PX);
		getBranchPhnCol.setSortable(true);
	}



	private void getBranchPocNmCol() {
		// TODO Auto-generated method stub
		getBranchPocNmCol=new TextColumn<Branch>() {
			@Override
			public String getValue(Branch object) {
				// TODO Auto-generated method stub
				if(object.getPocName()!=null){
					return object.getPocName()+"";
				}
				return "";	
			}
		};
		table.addColumn(getBranchPocNmCol, "Contact Name");
		table.setColumnWidth(getBranchPocNmCol, 90, Unit.PX);
		getBranchPocNmCol.setSortable(true);
	}



	private void getBranchPocCellCol() {
		// TODO Auto-generated method stub
		getBranchPocCellCol=new TextColumn<Branch>() {
			@Override
			public String getValue(Branch object) {
				// TODO Auto-generated method stub
				if(object.getPocCell()!=null){
					return object.getPocCell()+"";
				}
				return "";	
			}
		};
		table.addColumn(getBranchPocCellCol, "Contact Phone");
		table.setColumnWidth(getBranchPocCellCol, 90, Unit.PX);
		getBranchPocCellCol.setSortable(true);
	}



	private void getBranchStatusCol() {
		// TODO Auto-generated method stub
		getBranchStatusCol=new TextColumn<Branch>() {
			@Override
			public String getValue(Branch object) {
				// TODO Auto-generated method stub
				if(object.getstatus()==true){
					return "Active";
				}
				return "Inactive";	
			}
		};
		table.addColumn(getBranchStatusCol, "Status");
		table.setColumnWidth(getBranchStatusCol, 90, Unit.PX);
		getBranchStatusCol.setSortable(true);
	}



	@Override
	public void addColumnSorting() {
		super.addColumnSorting();
		
		addSortingOnBranchIdCol();
		addSortingOnBranchNameCol();
		addSortingOnBranchPhnCol();
		addSortingOnBranchPocNmCol();
		addSortingOnBranchPocCellCol();
		addSortingOnBranchStatusCol();
	}



	private void addSortingOnBranchIdCol() {
		List<Branch> list=getDataprovider().getList();
		columnSort=new ListHandler<Branch>(list);
		columnSort.setComparator(getBranchIdCol, new Comparator<Branch>()
		{
			@Override
			public int compare(Branch e1,Branch e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;
					}
				}
				else{
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}



	private void addSortingOnBranchNameCol() {
		List<Branch> list=getDataprovider().getList();
		columnSort=new ListHandler<Branch>(list);
		columnSort.setComparator(getBranchNameCol, new Comparator<Branch>()
		{
			@Override
			public int compare(Branch e1,Branch e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBusinessUnitName()!=null && e2.getBusinessUnitName()!=null){
						return e1.getBusinessUnitName().compareTo(e2.getBusinessUnitName());}
				}
				else{
					return 0;}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}



	private void addSortingOnBranchPhnCol() {
		List<Branch> list=getDataprovider().getList();
		columnSort=new ListHandler<Branch>(list);
		columnSort.setComparator(getBranchPhnCol, new Comparator<Branch>()
		{
			@Override
			public int compare(Branch e1,Branch e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getLandline()!=null && e2.getLandline()!=null){
						return e1.getLandline().compareTo(e2.getLandline());}
				}
				else{
					return 0;}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}



	private void addSortingOnBranchPocNmCol() {
		List<Branch> list=getDataprovider().getList();
		columnSort=new ListHandler<Branch>(list);
		columnSort.setComparator(getBranchPocNmCol, new Comparator<Branch>()
		{
			@Override
			public int compare(Branch e1,Branch e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getPocName()!=null && e2.getPocName()!=null){
						return e1.getPocName().compareTo(e2.getPocName());}
				}
				else{
					return 0;}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}



	private void addSortingOnBranchPocCellCol() {
		List<Branch> list=getDataprovider().getList();
		columnSort=new ListHandler<Branch>(list);
		columnSort.setComparator(getBranchPocCellCol, new Comparator<Branch>()
		{
			@Override
			public int compare(Branch e1,Branch e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getPocLandline()!=null && e2.getPocLandline()!=null){
						return e1.getPocLandline().compareTo(e2.getPocLandline());}
				}
				else{
					return 0;}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
		
		
	}



	private void addSortingOnBranchStatusCol() {
		List<Branch> list=getDataprovider().getList();
		columnSort=new ListHandler<Branch>(list);
		columnSort.setComparator(getBranchStatusCol, new Comparator<Branch>()
		{
			@Override
			public int compare(Branch e1,Branch e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getstatus()!=null && e2.getstatus()!=null){
						return e1.getstatus().compareTo(e2.getstatus());}
				}
				else{
					return 0;}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}



	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
