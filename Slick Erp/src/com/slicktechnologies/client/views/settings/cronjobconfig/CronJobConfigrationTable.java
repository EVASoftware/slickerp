package com.slicktechnologies.client.views.settings.cronjobconfig;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.cronjobcongiration.CronJobConfigration;

public class CronJobConfigrationTable  extends SuperTable<CronJobConfigration>{

	
	TextColumn<CronJobConfigration> countColumn;
	TextColumn<CronJobConfigration> processNameColumn;
	
	public CronJobConfigrationTable() {
		
		super();
		
	}

	@Override
	public void createTable() {
		addColumngetCount();
		addColumngetprocessName();
	}
	
	


	public void addColumnSorting(){
		addSortinggetCount();
		addSortinggetprocessName();
	}


		private void addSortinggetCount() {
			List<CronJobConfigration> list=getDataprovider().getList();
			columnSort=new ListHandler<CronJobConfigration>(list);
			columnSort.setComparator(countColumn, new Comparator<CronJobConfigration>()
					{
				@Override
				public int compare(CronJobConfigration e1,CronJobConfigration e2)
				{
					if(e1!=null && e2!=null)
					{
						if(e1.getCount()== e2.getCount()){
							return 0;}
						if(e1.getCount()> e2.getCount()){
							return 1;}
						else{
							return -1;}
					}
					else{
						return 0;}
				}
					});
			table.addColumnSortHandler(columnSort);
		}
		
		private void addColumngetCount() {
			countColumn=new TextColumn<CronJobConfigration>() {

				@Override
				public String getValue(CronJobConfigration object) {
					if(object.getCount()==-1)
						return "N.A";
					else return object.getCount()+"";
				
				}
			};
			table.addColumn(countColumn, "ID");
			table.setColumnWidth(countColumn, 100, Unit.PX);
			countColumn.setSortable(true);
		}


	
	
			private void addSortinggetprocessName() {
				List<CronJobConfigration> list=getDataprovider().getList();
				columnSort=new ListHandler<CronJobConfigration>(list);
				columnSort.setComparator(processNameColumn, new Comparator<CronJobConfigration>()
						{
					@Override
					public int compare(CronJobConfigration e1,CronJobConfigration e2)
					{
						if(e1!=null && e2!=null)
						{
							if( e1.getCronJobsName()!=null && e2.getCronJobsName()!=null){
								return e1.getCronJobsName().compareTo(e2.getCronJobsName());}
						}
						else{
							return 0;}
						return 0;
					}
						});
				table.addColumnSortHandler(columnSort);
		}
		
			private void addColumngetprocessName() {
				processNameColumn=new TextColumn<CronJobConfigration>()
						{
					@Override
					public String getValue(CronJobConfigration object)
					{
						return object.getCronJobsName().trim();
					}
						};
						table.addColumn(processNameColumn,"Process Name");
						table.setColumnWidth(processNameColumn, 130, Unit.PX);
						processNameColumn.setSortable(true);
			}
	
	
	
	

	@Override
	protected void initializekeyprovider() {
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
}
