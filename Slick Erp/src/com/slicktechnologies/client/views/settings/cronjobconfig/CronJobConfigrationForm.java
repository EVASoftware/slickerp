package com.slicktechnologies.client.views.settings.cronjobconfig;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.config.categoryconfig.CategoryTypes;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.client.views.processconfig.ProcessTypeDetailsTable;
import com.slicktechnologies.shared.common.cronjobcongiration.CronJobConfigration;
import com.slicktechnologies.shared.common.cronjobcongiration.CronJobConfigrationDetails;
import com.slicktechnologies.shared.common.email.EmailTemplate;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class CronJobConfigrationForm extends FormTableScreen<CronJobConfigration> implements ClickHandler, ValueChangeHandler<Boolean>, ChangeHandler{

	ObjectListBox<ConfigCategory>processName;
	ObjectListBox<Config> employeeRole;
	ObjectListBox<ConfigCategory> frequencyList;
	ObjectListBox<Config> olbCustomerGroup;
	CronJobConfigrationDetailsTable processTypeTable;
	IntegerBox tbInterval;
	CheckBox cbOverDue;
	DateBox dbStartingDate;     //added by Ashwini
	IntegerBox tbOverDue;
	Button btnAddTemp;
	RadioButton rbCompany,rbBranch;
	
	
	CheckBox processStatus;
	Button addprocess;
	
	ObjectListBox<String> oblCommunicationChannel;
	ObjectListBox<EmailTemplate> oblTemplateName;
	IntegerBox ibStartDay;
//	ibEndDay
	

	public CronJobConfigrationForm(SuperTable<CronJobConfigration>table,int mode,boolean captionmode)
	{
		super(table, mode, captionmode);
		createGui();
		table.connectToLocal();
	}
	public CronJobConfigrationForm(SuperTable<CronJobConfigration>table,int mode,boolean captionmode,boolean popupFlag)
	{
		super(table, mode, captionmode,popupFlag);
		createGui();
		table.connectToLocal();
	}

	@Override
	public void createScreen() {
		
		initializeWidget();
		
		FormFieldBuilder fbuilder;
		
		fbuilder = new FormFieldBuilder("Process Name",processName);
		FormField olbProcessName= fbuilder.setMandatory(true).setMandatoryMsg("Process Name is Mandatory").setRowSpan(0).setColSpan(0).build();

		
		fbuilder = new FormFieldBuilder("Frequency Type",frequencyList);
		FormField olbType= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Role",employeeRole);//label changed from Employee Role to Role Date:18-08-2022
		FormField olbEmpType= fbuilder.setMandatory(false).setMandatoryMsg("Employee Role is Mandatory.").setRowSpan(0).setColSpan(0).build();
		
		fbuilder=new FormFieldBuilder("",processTypeTable.getTable());
		FormField ftable=fbuilder.setColSpan(5).build();
		
		 /**
		 * Date-07/07/2018
		 * Developer:Ashwini
		 * Des:To add starting date coloumn 
		 */
		
		fbuilder=new FormFieldBuilder("Starting Date",dbStartingDate);
		FormField fdbStartingDate=fbuilder.setMandatory(false).setMandatoryMsg("Employee Role is Mandatory.").setColSpan(0).setRowSpan(0).build();
		
		 /*
		 * End by Ashwini
		 */
		
		fbuilder = new FormFieldBuilder("Follow Up Day/End Day",tbInterval);
		FormField ftbInterval= fbuilder.setMandatory(false).setMandatoryMsg("Employee Role is Mandatory.").setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("Overdue Days",tbOverDue);
		FormField ftbOverDue= fbuilder.setMandatory(false).setMandatoryMsg("Employee Role is Mandatory.").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Customer Group",olbCustomerGroup);
		FormField folbCustomerGroup= fbuilder.setMandatory(false).setMandatoryMsg("Employee Role is Mandatory.").setRowSpan(0).setColSpan(0).build();
		
		fbuilder=new FormFieldBuilder("",cbOverDue);
		FormField fcheckBoxOverDue= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder=new FormFieldBuilder("",rbCompany);
		FormField frbCompanywise= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder=new FormFieldBuilder("",rbBranch);
		FormField frbBranchWise= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder=new FormFieldBuilder("Status",processStatus);
		FormField fcheckBoxStatus= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",addprocess);
		FormField faddprocess= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Communication Channel",oblCommunicationChannel);
		FormField foblCommunicationChannel = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Template Name",oblTemplateName);
		FormField goblTemplateName = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Start Day",ibStartDay);
		FormField fibStartDay = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
//		fbuilder = new FormFieldBuilder("End Day",ibEndDay);
//		FormField fibEndDay = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		FormField [][] formfield = {
				{olbProcessName,olbEmpType,olbType,foblCommunicationChannel,goblTemplateName},
//				{fcheckBoxStatus,ftbInterval,fcheckBoxOverDue,ftbOverDue,fdbStartingDate},
				{fibStartDay,ftbInterval,fcheckBoxOverDue,ftbOverDue,fdbStartingDate},

				{folbCustomerGroup,fcheckBoxStatus,faddprocess},
				{ftable},
			
				
		};
		
		this.fields = formfield;
	}
	

	private void initializeWidget(){
		
		processName = new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(processName, Screen.CRONJOB);
		Screen scr = Screen.FREQUENCY;
		int scrCode = CategoryTypes.getCategoryInternalType(scr);
		frequencyList=new ObjectListBox<ConfigCategory>();
		frequencyList.setTitle("Frequency at which cron job should be executed. i.e. daily, weekly, monthly etc.");
		AppUtility.MakeLiveCategoryConfig(frequencyList, Screen.FREQUENCY);
		
		employeeRole=new ObjectListBox<Config>();
		employeeRole.setTitle("Role of the employee/customer to whom email should go.\r\n"
				+ "Ignore this settings in case it is to be sent to customer i.e. payment reminder or renewal reminder. That is hardcoded in the program");
		AppUtility.MakeLiveConfig(employeeRole,Screen.EMPLOYEEROLE);
		
		olbCustomerGroup=new ObjectListBox<Config>();
		olbCustomerGroup.setTitle("If you want to restrict reminders to only particular type of customer group then you can enter that here. \r\n"
				+ "Again this is currently applicable for only large customer feedback cron job");
		AppUtility.MakeLiveConfig(olbCustomerGroup, Screen.CUSTOMERGROUP);
		
		olbCustomerGroup.addChangeHandler(this);
		
		dbStartingDate=new DateBoxWithYearSelector(); //Added By Ashwini
		dbStartingDate.setTitle("Day from which  reminder or cron job shall be executed\r\n"
				+ "\r\n"
				+ "if you set this to 10th of Jan then cron job will start executing only from 10th Jan ");
		
		tbInterval = new IntegerBox();
		tbInterval.setTitle("Enter here number days before ERP shall start sending messages (follow-ups) before due date\r\n"
				+ "\r\n"
				+ "i.e. lets follow up days say 7& contract is due for renewal on 10th of Jan then renewal reminder will start going from 3rd of Jan (10 - 7)");
		tbOverDue = new IntegerBox();
		tbOverDue.setTitle("Enter this even if you want to send the reminder after due date. Reminder would be sent as per overdue days defined after due date\r\n"
				+ "i.e. if contract renewal is due on 10th of Jan & Overdue day is 7 then contract renewal would be send until 17th of Jan (10+7) ");
		
		cbOverDue = new CheckBox("Include Overdue Days");
		cbOverDue.setValue(false);
		
		rbCompany = new RadioButton("emailGroup", "Company Email");
		rbBranch = new RadioButton("emailGroup", "Branch Email");
		
		rbCompany.setValue(true);
		processTypeTable = new CronJobConfigrationDetailsTable();
		
		processStatus=new CheckBox();
		processStatus.setTitle("Activate or deactivate cron job. If you deactiavte cron job,cron job will not be executed");
		processStatus.setValue(true); 
		
		addprocess = new Button("ADD");
		addprocess.addClickHandler(this);
		tbOverDue.setValue(0);
		tbInterval.setValue(0);
		cbOverDue.addValueChangeHandler(this);
		tbOverDue.setEnabled(false);
		btnAddTemp = new Button("Email Template Setting");
		btnAddTemp.addClickHandler(this);
		
		oblCommunicationChannel = new ObjectListBox<String>();
		oblCommunicationChannel.setTitle("Communication Channel by which we want to send reminder i.e. SMS, email or whatsapp etc");
		oblCommunicationChannel.addItem("--SELECT--");
		oblCommunicationChannel.addItem("Email");
		oblCommunicationChannel.addItem("SMS");
		oblCommunicationChannel.addItem("WhatsApp");

		oblCommunicationChannel.addChangeHandler(this);
		
		oblTemplateName = new ObjectListBox<EmailTemplate>();
		oblTemplateName.setTitle("Template to be used to communicate to the message i.e. email template , SMS template, or whatsapp template");
//		oblTemplateName.addItem("--SELECT--");
		AppUtility.MakeliveEmailTemplate(oblTemplateName);

		
		ibStartDay = new IntegerBox();
		ibStartDay.setTitle("Enter from which day reminder to should be sent. This works along with follow up day.  \r\n"
				+ "\"i.e. if entered start day as 2 & follow up day as 7 then specific template can be used to send the reminder from ( due date - (followup day + start day) until due date. 10th Jan - (7 + 2) . So reminder would be sent from 5th Jan to 10th Jan. This way I can set the different reminders for different period \r\n"
				+ "i.e 15 days before due date - template 1, \r\n"
				+ "10 days before date - template 2, \r\n"
				+ "5 days Before due date template 3\"");
//		ibEndDay = new IntegerBox();
	}
	
	
	@Override
	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			
			/**
			 * @author Vijay Date - 21-04-2021
			 * Des :- updated code to manage popup and regular screen app level menus
			 */
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			}
			
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			
			/**
			 * @author Vijay Date - 21-04-2021
			 * Des :- updated code to manage popup and regular screen app level menus
			 */
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			}
			
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			
			/**
			 * @author Vijay Date - 21-04-2021
			 * Des :- updated code to manage popup and regular screen app level menus
			 */
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			}
			
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.PROCESSCONFIGURATION,LoginPresenter.currentModule.trim());
	}
	
	@Override
	public void updateModel(CronJobConfigration model) {
		
		if(processName.getValue()!=null){
//			model.setProcessName(processName.getValue());
			model.setCronJobsName(processName.getValue());
		}
		
		List<CronJobConfigrationDetails> processConfigType=this.processTypeTable.getDataprovider().getList();
		ArrayList<CronJobConfigrationDetails> arrItems=new ArrayList<CronJobConfigrationDetails>();
		arrItems.addAll(processConfigType);
		model.setCronJobsNameList(arrItems);
		model.setConfigStatus(processStatus.getValue());
//		manageProcessConfig(model);
		model.setUpdatedBy(LoginPresenter.loggedInUser);//14-12-2023
		model.setLastUpdatedDate(new Date());//14-12-2023
		
		presenter.setModel(model);
	}
	
	
	@Override
	public void updateView(CronJobConfigration view) {
		
		processTypeTable.setValue(view.getCronJobsProcessList());
		
		if(view.getCronJobsName()!=null)
			processName.setValue(view.getCronJobsName());
		
		if(view.isConfigStatus()!=null)
			processStatus.setValue(view.isConfigStatus());
		
		presenter.setModel(view);
	}
	
	
	
	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		
		processTypeTable.setEnable(state);
		
	}
	
	
	
	@Override
	public void clear() {
			super.clear();
			processTypeTable.clear();
	}
	
	@Override
	public void onClick(ClickEvent event) {
		
		if(event.getSource().equals(addprocess)){
			
			if(processName.getSelectedIndex()==0)
			{
				showDialogMessage("Please Select Process Name");
			}
			
			if(employeeRole.getSelectedIndex()==0 && !processName.getValue().equalsIgnoreCase("ServiceFeedbackCronJobImpl"))
			{
				showDialogMessage("Please Select Employee Role");
			}
			
			if(processName.getSelectedIndex()!=0&& processName.getSelectedIndex()==0){
				showDialogMessage("Please fill Process Type");
			}
			
			/**
			 * @author Vijay Chougule Date 22-07-2020
			 * Des :- Added validation to Frequency type must define with no of days
			 */
			if(frequencyList.getSelectedIndex()==0){
				showDialogMessage("Please Select Frequency Type");
				return;
			}
			else{
				ConfigCategory freqEntity=frequencyList.getSelectedItem();
				if(freqEntity.getDescription()==null || freqEntity.getDescription().equals("")){
					showDialogMessage("Please define no of days in the Frequency");
					return;
				}
			}
			/**
			 * ends here 
			 */
			
			if(oblCommunicationChannel.getSelectedIndex()!=0 || oblTemplateName.getSelectedIndex()!=0){
				
				if(oblCommunicationChannel.getSelectedIndex()!=0){
					if(oblTemplateName.getSelectedIndex()==0){
						showDialogMessage("Please select template name");
						return;
					}
					if(ibStartDay.getValue()==null){
						showDialogMessage("please add start day");
						return;
					}
					if(tbInterval.getValue()==null){
						showDialogMessage("Please add follow up / end days ");
						return;
					}
				}
				if(oblTemplateName.getSelectedIndex()!=0){
					
					if(oblCommunicationChannel.getSelectedIndex()==0){
						showDialogMessage("Please select communication channel");
						return;
					}
					
					if(ibStartDay.getValue()==null){
						showDialogMessage("please add start day");
						return;
					}
					if(tbInterval.getValue()==null){
						showDialogMessage("Please add follow up / end days ");
						return;
					}
				}
				
				

			}
			
			if(processName.getSelectedIndex()!=0){
				if(processName.getValue().equalsIgnoreCase("ServiceFeedbackCronJobImpl")){
					ConfigCategory freqEntity=frequencyList.getSelectedItem();
					if(freqEntity.getDescription()!=null && !freqEntity.getDescription().equals("") && AppUtility.validateNumricValue(freqEntity.getDescription())){
						try {
							int frequencydays = Integer.parseInt(freqEntity.getDescription());
							if(frequencydays<15){
								showDialogMessage("This cron job is not applicable for frequency days less than 15");
								return;
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						
					}
					
					if(olbCustomerGroup.getSelectedIndex()==0){
						showDialogMessage("Customer group is mandatory for this cron job!");
						return;
					}
					if(!olbCustomerGroup.getValue().equals("HighFrequencyServices")){
						showDialogMessage("customer group must be HighFrequencyServices for this process name");
						return;
					}
				}
			}
			
			
			if(processName.getSelectedIndex()!=0 )
			{
				if(processName.getValue().equalsIgnoreCase("ServiceFeedbackCronJobImpl")){
					
					List<CronJobConfigrationDetails> processLis=processTypeTable.getDataprovider().getList();
					System.out.println("Process Size"+processLis.size());
					if(processLis.size()!=0){
						if(validateProcess()==true){
							addToProcessTable();
						}
					}
					else{
						addToProcessTable();
					}
					
				}
				else{
					if(employeeRole.getSelectedIndex()!=0 ){
						
						List<CronJobConfigrationDetails> processLis=processTypeTable.getDataprovider().getList();
						System.out.println("Process Size"+processLis.size());
						if(processLis.size()!=0){
							if(validateProcess()==true){
								addToProcessTable();
							}
						}
						else{
							addToProcessTable();
						}
					}
				}
				
			}
			}
		}

	public boolean validateProcess()
	{
		List<CronJobConfigrationDetails> processLis=processTypeTable.getDataprovider().getList();
		
		if(processLis.size()!=0){
			

			for(int i=0;i<processLis.size();i++)
			{
				
				if( oblCommunicationChannel.getValue(oblCommunicationChannel.getSelectedIndex())!=null &&
						!oblCommunicationChannel.getValue(oblCommunicationChannel.getSelectedIndex()).equalsIgnoreCase("--select--") &&
						oblTemplateName.getSelectedIndex()!=0 && !oblTemplateName.getValue(oblTemplateName.getSelectedIndex()).equalsIgnoreCase("--select--")){
					
					if(processLis.get(i).getCronJobName().trim().equals(processName.getValue().trim())
							&& processLis.get(i).getEmployeeRole()!=null
							&& processLis.get(i).getEmployeeRole().trim().equals( employeeRole.getValue().trim())
							&& processLis.get(i).isOverdueStatus() == cbOverDue.getValue()
							&& processLis.get(i).getFrequencyType().trim().equals(frequencyList.getValue().trim()) 
							&& processLis.get(i).getCommunicationChannel().trim().equals(oblCommunicationChannel.getValue(oblCommunicationChannel.getSelectedIndex()).trim())
							&& processLis.get(i).getTemplateName().trim().equals(oblTemplateName.getValue(oblTemplateName.getSelectedIndex()).trim()))
					{
							showDialogMessage("CronJob Name, Employee Role, Communication Channel and Template Name setting already exists!");
							return false;
					}
					
				}
				else{
					if(processLis.get(i).getCronJobName().trim().equals(processName.getValue().trim())
							&& processLis.get(i).getEmployeeRole()!=null
							&& processLis.get(i).getEmployeeRole().trim().equals( employeeRole.getValue().trim())
							&& processLis.get(i).isOverdueStatus() == cbOverDue.getValue()
							&& processLis.get(i).getFrequencyType().trim().equals(frequencyList.getValue().trim()) 
							&& processLis.get(i).getCommunicationChannel().trim().equals(oblCommunicationChannel.getValue(oblCommunicationChannel.getSelectedIndex()).trim()))
					{
							showDialogMessage("CronJob Name and Employee Role setting already exists!");
							return false;
					}
				}
				
			}
		}
		
		if(tbInterval.getValue()<0 && !cbOverDue.getValue()){
			showDialogMessage("Please define Follow up days!");
			return false;
		}
		
		if(tbOverDue.getValue()<0 && cbOverDue.getValue()){
			showDialogMessage("Please define OverDue days!");
			return false;
		}
		
		return true;
		
	}
	
	protected void addToProcessTable(){
		
		CronJobConfigrationDetails cronjobEntity = new CronJobConfigrationDetails();
		cronjobEntity.setCronJobName(processName.getValue());
		cronjobEntity.setEmployeeRole(employeeRole.getValue());
		cronjobEntity.setFrequencyType(frequencyList.getValue());
		if(tbInterval.getValue()!=null){
			cronjobEntity.setInterval(tbInterval.getValue());
		}
		cronjobEntity.setStartingDate(dbStartingDate.getValue());//Added by Ashwini
		cronjobEntity.setOverdueDays(tbOverDue.getValue());
		cronjobEntity.setOverdueStatus(cbOverDue.getValue());
		cronjobEntity.setStatus(processStatus.getValue());
		cronjobEntity.setCompanyEmail(rbCompany.getValue());
		cronjobEntity.setCustomerGroup(olbCustomerGroup.getValue());
		
		cronjobEntity.setCreatedDate(new Date());
		ConfigCategory freqEntity=frequencyList.getSelectedItem();
		int days = Integer.valueOf(freqEntity.getDescription());
		cronjobEntity.setFrequencyDay(Integer.parseInt(freqEntity.getDescription()));
		
		cronjobEntity.setCommunicationChannel(oblCommunicationChannel.getValue(oblCommunicationChannel.getSelectedIndex()));
		System.out.println("communication channel "+oblCommunicationChannel.getValue(oblCommunicationChannel.getSelectedIndex()));
		cronjobEntity.setTemplateName(oblTemplateName.getValue(oblTemplateName.getSelectedIndex()));
		Console.log("Template Name "+oblTemplateName.getValue(oblTemplateName.getSelectedIndex()));
		cronjobEntity.setStartDay(ibStartDay.getValue());
//		if(tbInterval.getValue()!=null){
//			cronjobEntity.setEndDay(tbInterval.getValue());
//		}
		processTypeTable.getDataprovider().getList().add(cronjobEntity);
	}


	@Override
	public void onValueChange(ValueChangeEvent<Boolean> event) {

		if(cbOverDue.getValue()){
			tbInterval.setEnabled(false);
			tbInterval.setValue(0);
			tbOverDue.setEnabled(true);
		}
		else{
			tbOverDue.setEnabled(false);
			tbInterval.setEnabled(true);
			tbOverDue.setValue(0);
		}
		
	}


	@Override
	public void onChange(ChangeEvent event) {

		if(olbCustomerGroup.getSelectedIndex()!=0){
			tbOverDue.setEnabled(false);
			tbInterval.setEnabled(true);
			tbOverDue.setValue(0);
			cbOverDue.setEnabled(false);
		}else{
			tbOverDue.setEnabled(false);
			tbInterval.setEnabled(true);
			tbOverDue.setValue(0);
			cbOverDue.setEnabled(true);
		}
		
		if (event.getSource().equals(oblCommunicationChannel)) {
			if(oblCommunicationChannel.getValue(oblCommunicationChannel.getSelectedIndex()).equals("Email")){
				AppUtility.MakeliveEmailTemplate(oblTemplateName);
				Timer timer = new Timer() {
					
					@Override
					public void run() {
						String cronjobName = processName.getValue();
						if(cronjobName.trim().equals(AppConstants.ServiceReminderToclientCronJobImpl)){
							oblTemplateName.setValue("Service Due Reminder");
							oblTemplateName.setEnabled(false);
						}
						else if(cronjobName.trim().equals(AppConstants.CustomerARPaymentDueEmailToClientCronJob)){
							oblTemplateName.setValue("PaymentDueEmail");
							oblTemplateName.setEnabled(false);
						}
						else if(cronjobName.trim().equals(AppConstants.ServiceFeedbackCronJobImpl)){
							oblTemplateName.setValue("Large Customer Feedback");
							oblTemplateName.setEnabled(false);
						}
						else if(cronjobName.trim().equals(AppConstants.ServiceSchedulingCronJobImpl)){
							oblTemplateName.setValue("ServiceSchedulingByCustomer");
							oblTemplateName.setEnabled(false);
						}
						else{
							oblTemplateName.setEnabled(true);
						}
					}
				};
				timer.schedule(3000);
			}
			else if(oblCommunicationChannel.getValue(oblCommunicationChannel.getSelectedIndex()).equals("SMS") ||
					oblCommunicationChannel.getValue(oblCommunicationChannel.getSelectedIndex()).equals(AppConstants.WHATSAPP)){
				reactonInitilizaSMSTemplate(null);
			}
//			else{
//				oblCommunicationChannel.clear();
//				oblCommunicationChannel.addItem("--SELECT--");
//				oblCommunicationChannel.addItem("Email");
//				oblCommunicationChannel.addItem("SMS");
//				oblCommunicationChannel.addItem("WhatsApp");
//			}
		}
	}
	
	public void settoEditState(){
		tbOverDue.setEnabled(false);
		tbInterval.setEnabled(true);
		tbOverDue.setValue(0);
	}
	
	
	private void reactonInitilizaSMSTemplate(final String eventName) {

		oblTemplateName.clear();
		
		final GenricServiceAsync genasync=GWT.create(GenricService.class);

		MyQuerry querry = new MyQuerry();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new SmsTemplate());
		
		genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				Console.log("SMS template Size"+result.size());
				oblTemplateName.addItem("--SELECT--");
				for(SuperModel model : result){
					SmsTemplate smsTemplateEntity = (SmsTemplate) model;
					oblTemplateName.addItem(smsTemplateEntity.getEvent());
				}
				if(eventName!=null){
					Console.log("eventName "+eventName);
					oblTemplateName.setValue(eventName);;
				}
				
				String cronjobName = processName.getValue();
				if(cronjobName.trim().equals(AppConstants.ContractRenewalCronJobImpl)){
					oblTemplateName.setValue("Contract Renewal Cron Job");
					oblTemplateName.setEnabled(false);
				}
				else if(cronjobName.trim().equals(AppConstants.ServiceReminderToclientCronJobImpl)){
					oblTemplateName.setValue("Service Due Cron Job");
					oblTemplateName.setEnabled(false);
				}
				else if(cronjobName.trim().equals(AppConstants.CustomerARPaymentDueEmailToClientCronJob)){
					oblTemplateName.setValue("Amount Receivable Due Cron Job");
					oblTemplateName.setEnabled(false);
				}
				else if(cronjobName.trim().equals(AppConstants.ServiceFeedbackCronJobImpl)){
					oblTemplateName.setValue("Service Completion");
					oblTemplateName.setEnabled(false);
				}
				else if(cronjobName.trim().equals(AppConstants.ServiceSchedulingCronJobImpl)){
					oblTemplateName.setValue("ServiceSchedulingByCustomer");
					oblTemplateName.setEnabled(false);
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
		
	}
	
}
