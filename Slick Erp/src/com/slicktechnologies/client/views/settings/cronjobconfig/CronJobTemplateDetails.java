package com.slicktechnologies.client.views.settings.cronjobconfig;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;

public class CronJobTemplateDetails extends PopupScreen{

	TextBox tbSubject;
	TextArea taEmailBody;
	TextArea taEmailFooter;
	
	
	CronJobTemplateDetails(){
		super();
		createGui();
		/* Added by sheetal:08-12-2021
		 * Des:size of template popup in cron job remainder is increased*/
		getPopup().getElement().addClassName("setWidthAndHeight");
	}
	
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
	}

	
	private void initilizewidget() {
		tbSubject =  new TextBox();
		
		 taEmailBody =  new TextArea();
		 
		 taEmailFooter = new TextArea();
//		serviceCreate = new CheckBox("Create Service");
//		billCreate = new CheckBox("Create billing Doucument");
//		
//		
////		custUploadOkBtn = new Button("Ok");
////		custUploadCancleBtn = new Button("Cancle");
////		custUploadOkBtn.addClickHandler(this);
////		custUploadCancleBtn.addClickHandler(this);
		
	}
	
	@Override
	public void createScreen() {
		initilizewidget();
		
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingExpenseDetails=fbuilder.setlabel("Email Template Setting").widgetType(FieldType.GREYGROUPING).setMandatory(false).setColSpan(5).build();
		fbuilder = new FormFieldBuilder("Subject",tbSubject);
		FormField fdbserviceCreate= fbuilder.setRowSpan(0).setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder("Email Body Details",taEmailBody);
		FormField ftabodyDetails= fbuilder.setRowSpan(0).setColSpan(3).build();
		taEmailBody.getElement().getStyle().setHeight(70, Unit.PX);
		fbuilder = new FormFieldBuilder("Email Footer Details",taEmailFooter);
		FormField ftafooterDetails= fbuilder.setRowSpan(0).setColSpan(3).build();
		taEmailFooter.getElement().getStyle().setHeight(45, Unit.PX);
		
		FormField[][] formfield = {
				{fgroupingExpenseDetails},
				{fdbserviceCreate},
				{ftabodyDetails},
				{ftafooterDetails}
		};


		this.fields=formfield;		

	}

}
