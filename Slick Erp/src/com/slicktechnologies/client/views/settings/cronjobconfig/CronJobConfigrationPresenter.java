package com.slicktechnologies.client.views.settings.cronjobconfig;

import java.util.Vector;

import com.google.gwt.event.dom.client.ClickEvent;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.processconfig.ProcessConfigurationForm;
import com.slicktechnologies.client.views.processconfig.ProcessConfigurationPresenter;
import com.slicktechnologies.client.views.processconfig.ProcessConfigurationTableProxy;
import com.slicktechnologies.shared.common.cronjobcongiration.CronJobConfigration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class CronJobConfigrationPresenter  extends FormTableScreenPresenter<CronJobConfigration>{

	CronJobConfigrationForm form;
	public CronJobConfigrationPresenter(
			FormTableScreen<CronJobConfigration> view, CronJobConfigration model) {
		super(view, model);
		form=(CronJobConfigrationForm) view;
		form.getSupertable().connectToLocal();
		form.retriveTable(getUserQuery());
		form.setPresenter(this);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.PROCESSCONFIGURATION,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
		// TODO Auto-generated constructor stub
	}

	private MyQuerry getUserQuery() {
		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new CronJobConfigration());
		return quer;
	}
	
	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

	
	@Override
	protected void makeNewModel() {
		model = new CronJobConfigration();
	}

	public void setModel(CronJobConfigration entity) {
		model = entity;
	}
	
	public static void initalize() {
		CronJobConfigrationTable gentable=new CronJobConfigrationTable();
		CronJobConfigrationForm form=new CronJobConfigrationForm(gentable,FormTableScreen.UPPER_MODE,true);
		gentable.setView(form);
		gentable.applySelectionModle();
		CronJobConfigrationPresenter presenter=new CronJobConfigrationPresenter(form, new CronJobConfigration());
		AppMemory.getAppMemory().stickPnel(form);
	}

}
