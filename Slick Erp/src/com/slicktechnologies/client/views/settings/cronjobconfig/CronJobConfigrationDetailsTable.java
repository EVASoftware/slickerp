package com.slicktechnologies.client.views.settings.cronjobconfig;

import java.util.Date;











import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.view.client.ProvidesKey;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.cronjobcongiration.CronJobConfigrationDetails;

public class CronJobConfigrationDetailsTable extends SuperTable<CronJobConfigrationDetails> implements ClickHandler {
	
	int templateIndex;
	CronJobTemplateDetails tempPopup = new CronJobTemplateDetails() ;

	TextColumn<CronJobConfigrationDetails> cronJobNameColumn;
	TextColumn<CronJobConfigrationDetails> employeeRoleColumn;
	TextColumn<CronJobConfigrationDetails> frequencyTypeColumn;
	TextColumn<CronJobConfigrationDetails> intervalColumn;
	TextColumn<CronJobConfigrationDetails> overDueColumn;
	Column<CronJobConfigrationDetails, Boolean> overDueStatus;
    Column<CronJobConfigrationDetails,String> deleteColumn;
    Column<CronJobConfigrationDetails, String> templateColumn;
    Column<CronJobConfigrationDetails, Boolean> statusColumn;
    TextColumn<CronJobConfigrationDetails> viewStatusColumn;
    TextColumn<CronJobConfigrationDetails> customerGroupColumn;
    TextColumn<CronJobConfigrationDetails>StartingdateColumn;//Added by Ashwini
    
    TextColumn<CronJobConfigrationDetails> startDay;
    TextColumn<CronJobConfigrationDetails> endDay;
    TextColumn<CronJobConfigrationDetails> communicationChannel;
    TextColumn<CronJobConfigrationDetails> templateName;

	public CronJobConfigrationDetailsTable(){
		super();
		tempPopup.getLblOk().addClickHandler(this);
		tempPopup.getLblCancel().addClickHandler(this);
		tempPopup.getPopup().getElement().addClassName("templatePopup");
		templateIndex =0 ;
	}

	@Override
	public void createTable() {
		
		addColumngetcronJobName();
		addColumngetemployeeRole();
		addColumngetfrequencyTypeColumn();
		addColumngetStartingdateColumn();  //added by Ashwini

		addColumnCommunicationChannel();
		addColumnTemplateName();
		addColumnStartDay();
//		addColumnEndDay();
		addColumngetintervalColumn();

		addColumngetOverDueDaysColumn();
		addColumngetCustomerGroupColumn();
		addcolumngetTemplateColumn();
		addColumnStatus();
		setFieldUpdaterOnStatus();
		addColumngetdelete();
		
		createFieldUpdaterdeleteColumn();
		createFieldUpdaterTemplateColumn();
		table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
	}

	

	private void addColumnEndDay() {
		endDay = new TextColumn<CronJobConfigrationDetails>() {
			
			@Override
			public String getValue(CronJobConfigrationDetails object) {
				if(object.getEndDay()!=0){
					return object.getEndDay()+"";
				}
				else{
					return "NA";
				}
			}
		};
		
		table.addColumn(endDay,"End Day");
		table.setColumnWidth(endDay,200, Unit.PX);
	}

	private void addColumnStartDay() {
		startDay = new TextColumn<CronJobConfigrationDetails>() {
			
			@Override
			public String getValue(CronJobConfigrationDetails object) {
					return object.getStartDay()+"";
			}
		};
		table.addColumn(startDay,"Start Day");
		table.setColumnWidth(startDay,200, Unit.PX);
	}

	private void addColumnTemplateName() {
		templateName = new TextColumn<CronJobConfigrationDetails>() {
			
			@Override
			public String getValue(CronJobConfigrationDetails object) {
				if(object.getTemplateName()!=null){
					return object.getTemplateName();
				}
				else{
					return "NA";
				}
			}
		};
		table.addColumn(templateName,"Template Name");
		table.setColumnWidth(templateName,200, Unit.PX);
	}

	private void addColumnCommunicationChannel() {
		communicationChannel = new TextColumn<CronJobConfigrationDetails>() {
			
			@Override
			public String getValue(CronJobConfigrationDetails object) {
				if(object.getCommunicationChannel()!=null){
					return object.getCommunicationChannel();
				}
				else{
					return "NA";
				}
			}
		};
		table.addColumn(communicationChannel,"Communication Channel");
		table.setColumnWidth(communicationChannel,200, Unit.PX);
	}

	private void addColumngetcronJobName()
	{
		cronJobNameColumn=new TextColumn<CronJobConfigrationDetails>()
				{
			@Override
			public String getValue(CronJobConfigrationDetails object)
			{
				return object.getCronJobName().trim();
			}
				};
				table.addColumn(cronJobNameColumn,"CronJob Name");
				table.setColumnWidth(cronJobNameColumn,200, Unit.PX);
				cronJobNameColumn.setSortable(true);	
	}
	
	private void addColumngetemployeeRole() 
	{
		employeeRoleColumn=new TextColumn<CronJobConfigrationDetails>()
				{
			@Override
			public String getValue(CronJobConfigrationDetails object)
			{
				if(object.getEmployeeRole()!=null){
					return object.getEmployeeRole().toUpperCase().trim();
				}
				else{
					return "";
				}
			}
				};
				table.addColumn(employeeRoleColumn,"Employee Role");
				table.setColumnWidth(employeeRoleColumn, 200, Unit.PX);
				employeeRoleColumn.setSortable(true);		
	}
	
	
	
	private void addColumngetfrequencyTypeColumn() 
	{
		frequencyTypeColumn=new TextColumn<CronJobConfigrationDetails>()
				{
			@Override
			public String getValue(CronJobConfigrationDetails object)
			{
				return object.getFrequencyType().toUpperCase().trim();
			}
				};
				table.addColumn(frequencyTypeColumn,"Frequency Type");
				table.setColumnWidth(frequencyTypeColumn, 200, Unit.PX);
				frequencyTypeColumn.setSortable(true);		
	}
	
	/*
	 * Date:07/07/2018
	 * Developer:Ashwini
	 * Purpose:To add starting date in Cron Job Reminder Setting
	 */
	
	private void addColumngetStartingdateColumn()
	{
		final DateTimeFormat fmt = DateTimeFormat.getFormat("dd/MM/yyyy");

			StartingdateColumn = new TextColumn<CronJobConfigrationDetails>() {
			
			@Override
			public String getValue(CronJobConfigrationDetails object) {
				// TODO Auto-generated method stub
				if(object.getStartingDate()!=null)
					return fmt.format(object.getStartingDate());
				else 
					return "";
			}
		};

				table.addColumn(StartingdateColumn,"Starting Date");
				table.setColumnWidth(StartingdateColumn, 200, Unit.PX);
				StartingdateColumn.setSortable(true);	
	}
	
	   /*
	    * End by Ashwini
	    */
	
	private void addColumngetintervalColumn() 
	{
		intervalColumn=new TextColumn<CronJobConfigrationDetails>()
				{
			@Override
			public String getValue(CronJobConfigrationDetails object)
			{
				return object.getInterval()+"";			}
				};
				
				/**22-12-2020 Added by Priyanka as issue raised by Nitin Sir.**/
				table.addColumn(intervalColumn,"Follow up Days / End Day");
				table.setColumnWidth(intervalColumn, 200, Unit.PX);
				intervalColumn.setSortable(true);		
	}
	
	private void addColumngetOverDueDaysColumn() 
	{
		overDueColumn=new TextColumn<CronJobConfigrationDetails>()
				{
			@Override
			public String getValue(CronJobConfigrationDetails object)
			{
				return object.getOverdueDays()+"";			}
				};
				table.addColumn(overDueColumn,"Overdue Days");
				table.setColumnWidth(overDueColumn, 200, Unit.PX);
				overDueColumn.setSortable(true);		
	}
	
	private void addColumngetCustomerGroupColumn() 
	{
		customerGroupColumn=new TextColumn<CronJobConfigrationDetails>()
				{
			@Override
			public String getValue(CronJobConfigrationDetails object)
			{
				return object.getCustomerGroup();			
				}
				};
				table.addColumn(customerGroupColumn,"Customer Group");
				table.setColumnWidth(customerGroupColumn, 200, Unit.PX);
				customerGroupColumn.setSortable(true);		
	}
	
	
	
	private void addcolumngetTemplateColumn() {
		ButtonCell btnCell = new ButtonCell();
		templateColumn = new Column<CronJobConfigrationDetails,String>(btnCell) {

            public String getValue(CronJobConfigrationDetails object)
            {
                return "Template";
            }
        };
        table.setColumnWidth(templateColumn, 200, Unit.PX);
        table.addColumn(templateColumn, "Template");		
	}
	
	
	protected void addColumnViewgetstatus() {
		
		viewStatusColumn = new TextColumn<CronJobConfigrationDetails>() {
			@Override
			public String getValue(CronJobConfigrationDetails object) {
				return object.isStatus()+"";
			}
		};
		
		table.addColumn(viewStatusColumn, "Status");
		
	}
	
	private void addColumnStatus() {
		CheckboxCell cb = new CheckboxCell();
		statusColumn = new Column<CronJobConfigrationDetails, Boolean>(cb) {

			@Override
			public Boolean getValue(CronJobConfigrationDetails object) {
				return object.isStatus();
			}
		};
		table.addColumn(statusColumn, "Status");
		table.setColumnWidth(statusColumn, 200, Unit.PX);
		statusColumn.setSortable(true);		
	}
	
	private void addColumngetdelete() {
		ButtonCell btnCell = new ButtonCell();
		deleteColumn = new Column<CronJobConfigrationDetails,String>(btnCell) {

            public String getValue(CronJobConfigrationDetails object)
            {
                return "Delete";
            }
        };
        table.setColumnWidth(deleteColumn, 200, Unit.PX);
        table.addColumn(deleteColumn, "Delete");		
	}
	
	
	
	
	
	private void createFieldUpdaterdeleteColumn() {
		 deleteColumn.setFieldUpdater(new FieldUpdater<CronJobConfigrationDetails,String>() {

	            public void update(int index, CronJobConfigrationDetails object, String value)
	            {
	                getDataprovider().getList().remove(index);
	                table.redrawRow(index);
	            }
	        }
	);		
	}
	
	private void createFieldUpdaterTemplateColumn() {
		 templateColumn.setFieldUpdater(new FieldUpdater<CronJobConfigrationDetails,String>() {

	            public void update(int index, CronJobConfigrationDetails object, String value)
	            {
	            	tempPopup.showPopUp();
	            	templateIndex = index;
	            	tempPopup.taEmailBody.setValue(object.getMailBody());
	            	tempPopup.taEmailFooter.setValue(object.getFooter());
	            	tempPopup.tbSubject.setValue(object.getSubject());
	            	
	            }
	        }
	);		
	}
	
	protected void setFieldUpdaterOnStatus()
	{
		statusColumn.setFieldUpdater(new FieldUpdater<CronJobConfigrationDetails, Boolean>() {
			@Override
			public void update(int index, CronJobConfigrationDetails object, Boolean value) {
				object.setStatus(value);
				getDataprovider().getList();
				table.redrawRow(index);
			}
		});
	}
	
	
	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		
		
	for(int i=table.getColumnCount()-1;i>-1;i--){
		table.removeColumn(i);
	}
		if(state==true){
			
			addColumngetcronJobName();
			addColumngetemployeeRole();
			addColumngetfrequencyTypeColumn();
			
			addColumnCommunicationChannel();
			addColumnTemplateName();
			addColumnStartDay();
//			addColumnEndDay();
			addColumngetintervalColumn();
			
			addColumngetOverDueDaysColumn();
			addColumngetCustomerGroupColumn();
			addcolumngetTemplateColumn();
			addColumnStatus();
			setFieldUpdaterOnStatus();
			addColumngetdelete();
			createFieldUpdaterdeleteColumn();
			createFieldUpdaterTemplateColumn();
			addColumngetStartingdateColumn();  //Added by Ashwini
			
		}else {
			
			addColumngetcronJobName();
			addColumngetemployeeRole();
			addColumngetfrequencyTypeColumn();
			
			addColumnCommunicationChannel();
			addColumnTemplateName();
			addColumnStartDay();
//			addColumnEndDay();
			addColumngetintervalColumn();
			
			addColumngetOverDueDaysColumn();
			addColumngetCustomerGroupColumn();
			addColumnViewgetstatus();
			addColumngetStartingdateColumn();  //Added by Ashwini
		}
	}

	@Override
	public void applyStyle() {
		
	}
	
	@Override
	protected void initializekeyprovider() {
		keyProvider = new ProvidesKey<CronJobConfigrationDetails>() {
			@Override
			public Object getKey(CronJobConfigrationDetails item) {
				if (item == null) {
					return null;
				} else
					return new Date().getTime();
			}
		};

	}

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		if(event.getSource() == tempPopup.getLblOk()){
			getDataprovider().getList().get(templateIndex).setFooter(tempPopup.taEmailFooter.getValue());
			getDataprovider().getList().get(templateIndex).setMainBody(tempPopup.taEmailBody.getValue());
			getDataprovider().getList().get(templateIndex).setSubject(tempPopup.tbSubject.getValue());
			popupSettign();
		}
		if(event.getSource() == tempPopup.getLblCancel()){
			popupSettign();
		}
		
	}
	
	public void popupSettign(){
		tempPopup.taEmailFooter.setValue("");
		tempPopup.taEmailBody.setValue("");
		tempPopup.tbSubject.setValue("");
		tempPopup.hidePopUp();
	}
}