package com.slicktechnologies.client.views.contractrenewal;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.google.gwt.view.client.RowCountChangeEvent.Handler;

public class ContractRenewalProductTablePouup extends AbsolutePanel implements Handler{

	public ContractRenewalProductTable productList;
	private Button btnCancel,btnOk;
	TextArea taDesc;
	TextBox netPayable;
	
	public ContractRenewalProductTablePouup() {
		productList=new ContractRenewalProductTable();
		productList.getTable().addRowCountChangeHandler(this);
//		productList.connectToLocal();
		add(productList.getTable(),10,30);
		
		
		InlineLabel netPay = new InlineLabel("Net Payable");
		add(netPay,10,230);
		
		netPayable =new TextBox();
		netPayable.setEnabled(false);
		add(netPayable,150,230);
		
		
		
		InlineLabel description = new InlineLabel("Description");
		add(description,10,260);
		
		taDesc=new TextArea();
		add(taDesc,10,300);
		
		taDesc.getElement().getStyle().setWidth(600, Unit.PX);
		
		
		btnCancel = new Button("Cancel");
		btnOk=new Button("  Ok  ");
		
		add(btnOk, 275, 350);
		add(btnCancel,375,350);
		
		setSize("700px","400px");
		this.getElement().setId("form");
	}
	
	
	
	public ContractRenewalProductTable getProductList() {
		return productList;
	}

	public void setProductList(ContractRenewalProductTable productList) {
		this.productList = productList;
	}

	public Button getBtnCancel() {
		return btnCancel;
	}

	public void setBtnCancel(Button btnCancel) {
		this.btnCancel = btnCancel;
	}

	public Button getBtnOk() {
		return btnOk;
	}

	public void setBtnOk(Button btnOk) {
		this.btnOk = btnOk;
	}

	
	


	public TextBox getNetPayable() {
		return netPayable;
	}



	public void setNetPayable(TextBox netPayable) {
		this.netPayable = netPayable;
	}



	@Override
	public void onRowCountChange(RowCountChangeEvent event) {
		
		if(event.getSource().equals(productList.getTable()))
		{
			double netpay=0;
			for(ContractRenewalProduct obj :productList.getDataprovider().getList())
			{
				netpay = netpay + obj.getNetPayable();
			}	
			
			netPayable.setValue(Math.round(netpay)+"");
		}
	}
}
