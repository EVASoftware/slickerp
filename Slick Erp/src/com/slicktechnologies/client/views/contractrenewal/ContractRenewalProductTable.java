package com.slicktechnologies.client.views.contractrenewal;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.SelectionCell;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.TaxDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.productlayer.Tax;

public class ContractRenewalProductTable extends SuperTable<ContractRenewalProduct> {

	TextColumn<ContractRenewalProduct> getColumnSrNo;
	TextColumn<ContractRenewalProduct> getColumnProductCode;
	TextColumn<ContractRenewalProduct> getColumnProductName;
	/**
	 * Date : 22-11-2017 BY MANISHA
	 * Description :Commented to make the duration field editable.!!
	 */
//	TextColumn<ContractRenewalProduct> getColumnProductDuration;
	/**end**/
//	TextColumn<ContractRenewalProduct> getColumnProductPrice;
	Column<ContractRenewalProduct, String> getColumnProductNewPrice;
	Column<ContractRenewalProduct, String> getColumnProductRemark;
	Column<ContractRenewalProduct, String> getColumnPremises;
	Column<ContractRenewalProduct, String> totalColumn;
	
	/** Date : 22-11-2017 BY MANISHA
	 * Description :Commented to make the duration field editable.!!
	 */
	Column<ContractRenewalProduct, String>  getColumnProductService;
	Column<ContractRenewalProduct, String>  getColumnProductDuration;
    /**End**/
	
	

	
	/**
	 * Date : 22-11-2017 BY MANISHA
	 * Description ::To take the master taxes in drop down.!!
	 */
	Column<ContractRenewalProduct, String> vatColumn, serviceColumn;

	TextColumn<ContractRenewalProduct> viewServiceTaxColumn;
	TextColumn<ContractRenewalProduct> viewVatTaxColumn;

	public ArrayList<TaxDetails> vattaxlist;
	public ArrayList<TaxDetails> servicetaxlist;
	public ArrayList<String> list;
	public ArrayList<String> vatlist;
	/**end**/
	
	/**
	 * Date : 01-12-2017 BY ANIL
	 * 
	 */
	Column<ContractRenewalProduct,String> getEditableProductNameCol;
	/**
	 * End
	 */
	
	/**
	 * Date : 05-12-2017 BY ANIL
	 * Made old price column editable , requirement raised by perfect pest through sonu
	 */
	Column<ContractRenewalProduct, String> geEditableOldPriceCol;
	/**
	 * End
	 */
	 NumberFormat nf=NumberFormat.getFormat("0.00");
	 
	Column<ContractRenewalProduct,Date> startDate;
	Column<ContractRenewalProduct,Date> endDate;
	 
	public static boolean contractdateupdateFlag = false;
	
	Column<ContractRenewalProduct, String> getColumnPercentage;

//	Column<ContractRenewalProduct, String> getColumnProductPrice; //Commented by Ashwini Patil Date:04-08-2023 as old price price should not be editable
	TextColumn<ContractRenewalProduct> getColumnProductPrice; //Added non editable old product price column

	@Override
	public void createTable() {

		// *******sr no for mapping same product*******
		getColumnSrNo();
		getColumnProductCode();
		/**
		 * Date : 04-12-2017
		 */
		getEditableProductNameCol();
//		getColumnProductName();
		/**
		 * End
		 */
		
		getColumnProductStartDate();
		getColumnproductEndDate();
		
		getColumnProductDuration();
		getColumnProductService();
		
		/**
		 * Date : 05-12-2017 By ANIL
		 */
		getColumnProductPrice();
		
		/**Date 10-4-2020 by Amol
		 * commented editable Old Price Column raised by Nitin Sir
		 * 
		 */
//		geEditableOldPriceCol();
		/**
		 * End
		 */
		getColumnPercentage();

		getColumnProductNewPrice();
		getColumnProductRemark();
		getColumnPremises();

		/**
		 * Date : 22-11-2017 BY MANISHA
		 * Description :To take the master taxes in drop down...!!
		 */

		retriveServiceTax();
		/**end**/
		// getColumnSercviceTaxAndVatTax();

	}

	

	



	







	private void getColumnproductEndDate() {


		final DateTimeFormat fmt = DateTimeFormat.getFormat("dd/MM/yyyy");
		DatePickerCell date = new DatePickerCell(fmt);
		endDate = new Column<ContractRenewalProduct, Date>(date) {
			@Override
			public Date getValue(ContractRenewalProduct object) {
				System.out.println("end date "+object.getEndDate());
				if (object.getEndDate() != null) {
					return fmt.parse(fmt.format(object.getEndDate()));
				} else {
					Date date=CalendarUtil.copyDate(object.getStartDate());
					CalendarUtil.addDaysToDate(date,object.getDuaration()-1);
					object.setEndDate(date);
					return fmt.parse(fmt.format(date));
				}
			}
		};
		table.addColumn(endDate, "#End Date");
		table.setColumnWidth(endDate, 100, Unit.PX);
		
	
	}







	private void getColumnProductStartDate() {

		final DateTimeFormat fmt = DateTimeFormat.getFormat("dd/MM/yyyy");
		DatePickerCell date= new DatePickerCell(fmt);
		
		startDate = new Column<ContractRenewalProduct, Date>(date) {
			
			@Override
			public Date getValue(ContractRenewalProduct object) {
				
				System.out.println("startDate"+object.getStartDate());

				if(object.getStartDate()!=null){
						return fmt.parse(fmt.format(object.getStartDate()));
					}
				else{
				object.setStartDate(new Date());
					return fmt.parse(fmt.format(new Date()));
				}
			
			}
		};
		
		table.setColumnWidth(startDate, 90, Unit.PX);
		table.addColumn(startDate,"#Start Date");
		table.setColumnWidth(startDate, 100,Unit.PX);
	}







	protected void createViewServiceTaxColumn() {
		/**
		 * Date : 22-11-2017 BY MANISHA
		 * Description :Change the name of tax label...!!
		 */
		viewServiceTaxColumn = new TextColumn<ContractRenewalProduct>() {
			public String getValue(ContractRenewalProduct object) {
				return object.getServiceTaxEdit();
			}

		};
		table.addColumn(viewServiceTaxColumn, "#Tax 2");
		table.setColumnWidth(viewServiceTaxColumn, 100, Unit.PX);
		
		/**end**/
	}

	protected void createViewVatTaxColumn() {

		/**
		 * Date : 22-11-2017 BY MANISHA
		 * Description :Change the name of tax label...!!
		 */
		viewVatTaxColumn = new TextColumn<ContractRenewalProduct>() {
			public String getValue(ContractRenewalProduct object) {
				// TODO Auto-generated method stub
				return object.getVatTaxEdit();
			}

		};
		table.addColumn(viewVatTaxColumn, "#Tax 1");
		table.setColumnWidth(viewVatTaxColumn, 100, Unit.PX);
		/**end**/
	}

	/**
	 * Date : 22-11-2017 BY MANISHA
	 * Description :.To take the master taxes in drop down..!!
	 */
	private void retriveServiceTax() {
		final GenricServiceAsync service = GWT.create(GenricService.class);
		MyQuerry query = new MyQuerry();
		Company c = new Company();
		System.out.println("Company Id :: " + c.getCompanyId());

		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;

		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);

		query.setFilters(filtervec);
		query.setQuerryObject(new TaxDetails());

		service.getSearchResult(query,
				new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onFailure(Throwable caught) {
					}

					public void onSuccess(ArrayList<SuperModel> result) {

						System.out.println("result size of tax " + result);
						list = new ArrayList<String>();
						servicetaxlist = new ArrayList<TaxDetails>();
						vattaxlist = new ArrayList<TaxDetails>();
						vatlist = new ArrayList<String>();

						List<TaxDetails> backlist = new ArrayList<TaxDetails>();

						for (SuperModel model : result) {
							TaxDetails entity = (TaxDetails) model;

							backlist.add(entity);
							servicetaxlist.add(entity);
							vattaxlist.add(entity);
						}

						for (int i = 0; i < backlist.size(); i++) {

							//
							list.add(backlist.get(i).getTaxChargeName());
							System.out.println("list size" + list.size());
							//
							vatlist.add(backlist.get(i).getTaxChargeName());
							System.out.println("valist size" + vatlist.size());
							//
						}

						addEditColumnVatTax();
						addEditColumnServiceTax();
						createColumnNetPayableColumn();
						addFieldUpdater();
					}

				});
	}

	/**end**/
	
	// private void getColumnServiceTaxAndVatTax() {
	// final GenricServiceAsync service = GWT.create(GenricService.class);
	// MyQuerry query = new MyQuerry();
	// Company c=new Company();
	// System.out.println("Company Id :: "+c.getCompanyId());
	//
	// Vector<Filter> filtervec=new Vector<Filter>();
	// Filter filter = null;
	//
	// filter = new Filter();
	// filter.setQuerryString("companyId");
	// filter.setLongValue(c.getCompanyId());
	// filtervec.add(filter);
	//
	// query.setFilters(filtervec);
	// query.setQuerryObject(new TaxDetails());
	//
	// service.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>()
	// {
	// @Override
	// public void onFailure(Throwable caught) {
	// }
	//
	//
	// public void onSuccess(ArrayList<SuperModel> result) {
	//
	// System.out.println("result size of tax "+result);
	// list=new ArrayList<String>();
	// servicetaxlist=new ArrayList<TaxDetails>();
	// vattaxlist = new ArrayList<TaxDetails>();
	// vatlist=new ArrayList<String>();
	// List<TaxDetails> backlist=new ArrayList<TaxDetails>();
	//
	// for (SuperModel model : result) {
	// TaxDetails entity = (TaxDetails) model;
	//
	// backlist.add(entity);
	// servicetaxlist.add(entity);
	// vattaxlist.add(entity);
	// }
	//
	// for(int i=0;i<backlist.size();i++){
	// if(backlist.get(i).getServiceTax().equals(true)){
	// list.add(backlist.get(i).getTaxChargeName());
	// System.out.println("list size"+list.size());
	// }
	// else if(backlist.get(i).getVatTax().equals(true)){
	// vatlist.add(backlist.get(i).getTaxChargeName());
	// System.out.println("valist size"+vatlist.size());
	// }
	// }
	//
	//
	//
	// addEditColumnVatTax();
	// addEditColumnServiceTax();
	// createColumnNetPayableColumn();
	// addFieldUpdater();
	// }
	// });
	// }
	/**
	 * Date : 22-11-2017 BY MANISHA
	 * Description :.To take the  taxes from drop down..!!
	 */

	private void addEditColumnVatTax() {
		SelectionCell employeeselection = new SelectionCell(vatlist);
		vatColumn = new Column<ContractRenewalProduct, String>(
				employeeselection) {
			@Override
			public String getValue(ContractRenewalProduct object) {
				if (object.getVatTax().getTaxConfigName() != null
						&& !object.getVatTax().getTaxConfigName().equals("")) {

					object.setVatTaxEdit(object.getVatTax().getTaxConfigName());

					return object.getVatTax().getTaxConfigName();
				} else
					return "N/A";
			}
		};
		table.addColumn(vatColumn, "#Tax 1");
		table.setColumnWidth(vatColumn, 130, Unit.PX);
	}

	private void addEditColumnServiceTax() {

		SelectionCell employeeselection = new SelectionCell(list);
		serviceColumn = new Column<ContractRenewalProduct, String>(
				employeeselection) {
			@Override
			public String getValue(ContractRenewalProduct object) {
				if (object.getServiceTax().getTaxConfigName() != null
						&& !object.getServiceTax().getTaxConfigName()
								.equals("")) {

					object.setServiceTaxEdit(object.getServiceTax()
							.getTaxConfigName());

					return object.getServiceTax().getTaxConfigName();
				} else
					return "NA";

			}

		};
		table.addColumn(serviceColumn, "#Tax 2");
		table.setColumnWidth(serviceColumn, 140, Unit.PX);
	}

	/********************************* ------------------------ends here------------------------------*****************************/

	private void createColumnNetPayableColumn() {

		TextCell editCell = new TextCell();
		totalColumn = new Column<ContractRenewalProduct, String>(editCell) {
			@Override
			public String getValue(ContractRenewalProduct object) {
				return object.getNetPayable() + "";
			}
		};
		table.addColumn(totalColumn, "Total");
		table.setColumnWidth(totalColumn, 100, Unit.PX);
		
		/**
		 * Date 5-6-2018
		 * By Jayshree 
		 */
		
//		TextCell editCell = new TextCell();
//		totalColumn = new Column<ContractRenewalProduct, String>(editCell) {
//			@Override
//			public String getValue(ContractRenewalProduct object) {
//				return nf.format((calculateTotalAmount(object)));
//			}
//
//			
//		};
//		table.addColumn(totalColumn, "Total");
//		table.setColumnWidth(totalColumn, 100, Unit.PX);
		
		//End By jayshree
		
	}

	private void getColumnSrNo() {

		getColumnSrNo = new TextColumn<ContractRenewalProduct>() {

			@Override
			public String getValue(ContractRenewalProduct object) {
				return object.getSrNo() + "";
			}
		};
		table.addColumn(getColumnSrNo, "Sr.No.");
		table.setColumnWidth(getColumnSrNo, 80, Unit.PX);

	}

	private void getColumnPremises() {

		EditTextCell cell = new EditTextCell();
		getColumnPremises = new Column<ContractRenewalProduct, String>(cell) {
			@Override
			public String getValue(ContractRenewalProduct object) {
				if (object.getPremises() != null) {
					return object.getPremises() + "";
				}
				return "";
			}
		};

		table.addColumn(getColumnPremises, "#Premises");
		// table.setColumnWidth(getColumnPremises, "130px");
		table.setColumnWidth(getColumnPremises, 130, Unit.PX);
	}

	public  double calculateTotalAmount(ContractRenewalProduct object) {
		
		if(object.getNetPayable()!=0){
			
			return Math.round(object.getNetPayable());
		}
		else{
			double total=0;
			SuperProduct product=object.getPrduct();
			double tax=removeAllTaxes(product);
			double origPrice=object.getProductNewPrice();
			System.out.println("origPrice"+object.getProductNewPrice());
			System.out.println("origPrice"+origPrice);
			double totalAmt= 0;
			double vatTaxAmt =0;
			double setTaxAmt =0;
			
			if(object.getVatTax().getPercentage()!=0 && object.getVatTax().getTaxPrintName()!=null && !object.getVatTax().getTaxPrintName().equals("")){
				vatTaxAmt = origPrice * (object.getVatTax().getPercentage() / 100);
				if(object.getServiceTax().getPercentage()!=0 && object.getServiceTax().getTaxPrintName()!=null && !object.getServiceTax().getTaxPrintName().equals("")){
					setTaxAmt = origPrice * (object.getServiceTax().getPercentage()/100);
					totalAmt = origPrice + vatTaxAmt + setTaxAmt;
			    }else{
					totalAmt = origPrice + vatTaxAmt;
				}
			}else{
			    if(object.getServiceTax().getPercentage()!=0 && object.getServiceTax().getTaxPrintName()!=null && !object.getServiceTax().getTaxPrintName().equals("")){   
					setTaxAmt = origPrice * (object.getServiceTax().getPercentage()/100);
					totalAmt = origPrice + setTaxAmt;
				}
				if(object.getVatTax().getPercentage()!=0 && (object.getVatTax().getTaxPrintName()==null||object.getVatTax().getTaxPrintName().equals("")) ){
					vatTaxAmt =  origPrice * (object.getVatTax().getPercentage() / 100);
					totalAmt=vatTaxAmt;
					if(object.getServiceTax().getPercentage() !=0 && (object.getServiceTax().getTaxPrintName()==null||object.getServiceTax().getTaxPrintName().equals(""))){
						setTaxAmt = totalAmt * (object.getServiceTax().getPercentage() / 100);
						totalAmt = totalAmt +setTaxAmt;
//						System.out.println("new price "+totalAmt );
					}
				}else{
					if(object.getServiceTax().getPercentage() !=0 && (object.getServiceTax().getTaxPrintName()==null||object.getServiceTax().getTaxPrintName().equals(""))){
						setTaxAmt = origPrice * (object.getServiceTax().getPercentage() / 100);
						totalAmt = totalAmt +setTaxAmt;
//						System.out.println("new price "+totalAmt );
					}
				}
				
			}
			
			
//			object.setNetPayable(total);
			System.out.println("calculateTotalAmount"+totalAmt);
			return Math.round(totalAmt);
		}
		
		
	}
	
	private void setFieldUpdaterPremises() {

		System.out.println("inside new product price updator..");
		getColumnPremises
				.setFieldUpdater(new FieldUpdater<ContractRenewalProduct, String>() {
					@Override
					public void update(int index,
							ContractRenewalProduct object, String value) {
						try {
							// double val1=Double.parseDouble(value);
							object.setPremises(value);
							
							/**Extra semicolon removed by manisha**/
							
						} catch (Exception e) {
						}
						table.redrawRow(index);
					}
				});

	}

	private void setFieldUpdaterOnProductRemark() {
		System.out.println("inside new product price updator..");
		getColumnProductRemark
				.setFieldUpdater(new FieldUpdater<ContractRenewalProduct, String>() {
					@Override
					public void update(int index,
							ContractRenewalProduct object, String value) {
						try {
							// double val1=Double.parseDouble(value);
							object.setRemark(value);
							/**Extra semicolon removed by manisha**/
						} catch (Exception e) {
						}
						table.redrawRow(index);
					}
				});
	}

	
	private void getColumnProductRemark() {
		EditTextCell cell = new EditTextCell();
		getColumnProductRemark = new Column<ContractRenewalProduct, String>(cell) {
			@Override
			public String getValue(ContractRenewalProduct object) {
				if (object.getRemark() != null) {
					return object.getRemark() + "";
				}
				return "";
			}
		};
		table.addColumn(getColumnProductRemark, "#Remark");
		table.setColumnWidth(getColumnProductRemark, "130px");
	}

	
	private void setFieldUpdaterOnProductNewPrice() {
		System.out.println("inside new product price updator..");
		getColumnProductNewPrice
				.setFieldUpdater(new FieldUpdater<ContractRenewalProduct, String>() {
					@Override
					public void update(int index,
							ContractRenewalProduct object, String value) {
						try {
							double val1 = Double.parseDouble(value);
							if(val1!=0){
								object.setProductNewPrice(val1);
								object.setPercentage(0);
								
								double totalAmt = gettaxAmount(val1,object);
								System.out.println("totalAmt == "+totalAmt);
								object.setNetPayable(totalAmt);
								
							}
							else{
								
								double tax1 = removeAllTaxes(object.getPrduct());
								double amt1 = object.getProductNewPrice() - tax1;
								Console.log("new price update amt1="+amt1);
//								if(nf.parse(nf.format(amt1))!=val1){
								if(Math.round(amt1)!=val1){
									System.out.println("inside if");
								object.getPrduct().setPrice(val1);
								double tax = removeAllTaxes(object.getPrduct());
								double amt = val1 - tax;
								object.setProductNewPrice(val1);
								
								object.setProductPriceNew2(val1);
								Console.log("new price update amt="+amt);
								System.out.println("new price updated  == in price field updator"+val1);
								double totalAmt = calculateTotalAmt(object
										.getVatTax().getTaxPrintName(), object
										.getServiceTax().getTaxPrintName(), amt,
										object.getServiceTax().getPercentage(),
										object.getVatTax().getPercentage());
								object.setNetPayable(totalAmt);
								
								}
								
							}
							
							table.redrawRow(index);
							RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
						
							
						} catch (Exception e) {
						}
						table.redrawRow(index);
					}
				});
	}

	private void getColumnProductCode() {
		getColumnProductCode = new TextColumn<ContractRenewalProduct>() {
			@Override
			public String getValue(ContractRenewalProduct object) {
				if (object.getProductCode() != null) {
					return object.getProductCode();
				}
				return "";
			}
		};
		table.addColumn(getColumnProductCode, "Product Code");
		table.setColumnWidth(getColumnProductCode, "100px");
	}

	private void getColumnProductName() {
		getColumnProductName = new TextColumn<ContractRenewalProduct>() {
			@Override
			public String getValue(ContractRenewalProduct object) {
				if (object.getProductName() != null) {
					return object.getProductName();
				}
				return "";
			}
		};
		table.addColumn(getColumnProductName, "Product Name");
		table.setColumnWidth(getColumnProductName, "100px");
	}


	private void getColumnProductDuration() {
      /**Date: 25/11/2017 By:Manisha
		 * Desription :To make duration field editable;
		 */
		        EditTextCell cell = new EditTextCell();
		        getColumnProductDuration = new Column<ContractRenewalProduct, String>(cell) {
		        	
			/**end**/
			@Override
			public String getValue(ContractRenewalProduct object) {
				if (object.getDuaration() != -1) {
					return object.getDuaration() + "";
				}
				return "";
			}
		};
		table.addColumn(getColumnProductDuration, "#Duration");
		table.setColumnWidth(getColumnProductDuration, "100px");
	}

	private void getColumnProductService(){
		
		 EditTextCell cell = new EditTextCell();
		getColumnProductService=new Column<ContractRenewalProduct, String>(cell) {

			@Override
			public String getValue(ContractRenewalProduct object) {
				// TODO Auto-generated method stub
				if(object.getNoOfServices() !=-1)
				{
					return object.getNoOfServices()+"";
				}
				return "";				
			}
		
		};
		table.addColumn(getColumnProductService, "#Service");
		table.setColumnWidth(getColumnProductService, "100px");
	}
	
	private void getColumnProductPrice() {
		
//		Commented by Ashwini Patil Date:4-08-2023 as this column shows old price of product and should not be editable
//		EditTextCell cell = new EditTextCell();
//
//		getColumnProductPrice = new Column<ContractRenewalProduct, String>(cell) {
//			
//			@Override
//			public String getValue(ContractRenewalProduct object) {
//				return object.getProductPrice()+"";
//			}
//		};
		
		
		getColumnProductPrice = new TextColumn<ContractRenewalProduct>() {
			public String getValue(ContractRenewalProduct object) {
				return object.getProductPrice()+"";
			}

		};
		
		
//		getColumnProductPrice = new TextColumn<ContractRenewalProduct>() {
//			@Override
//			public String getValue(ContractRenewalProduct object) {
//				
//				if(object.getProductPrice()!=0){
//					return object.getProductPrice()+"";
//				}
//				else{
//					if (object.getProductPrice() != -1) {
//						
//						SuperProduct prod=object.getPrduct();
//						prod.setPrice(object.getProductPrice());
//						double tax = removeAllTaxes(object.getPrduct());
//						double amt = object.getProductPrice() - tax;
//						// object.setProductPrice(amt);//@author Abhinav
//						// Bihade,@since 16/01/2020, Des: Commented this line for Is
//						// inclusive
////						return amt + "";
//						Console.log("price="+object.getProductPrice());
//						Console.log("tax="+tax);
//						Console.log("amt="+amt);
//						
//						if(object.getPrduct().getVatTax().isInclusive()&&object.getPrduct().getServiceTax().isInclusive()){
//							Console.log("is inclusive");
//							tax=removeAllTaxes(prod);
//							Console.log("tax="+tax);
//							amt=object.getProductPrice()-tax;
//							return amt+"";//Ashwini Patil Date:26-04-2023 pecopp want product original price even if is inclusive is selected
//						}else
//							return amt + "";
//					}
//					return "";
//				}
//		
//			}
//		};
		table.addColumn(getColumnProductPrice, "Product Price");
		table.setColumnWidth(getColumnProductPrice, "100px");
	}

	private void getColumnProductNewPrice() {
		EditTextCell cell = new EditTextCell();
		getColumnProductNewPrice = new Column<ContractRenewalProduct, String>(
				cell) {
			@Override
			public String getValue(ContractRenewalProduct object) {
				
				if(object.getProductNewPrice()!=0){
					return Math.round(object.getProductNewPrice())+"";
				}
				else{
					if (object.getProductNewPrice() != -1) {
						object.getPrduct().setPrice(object.getProductNewPrice());
						System.out.println("object.getProductNewPrice()"+object.getProductNewPrice());
						double tax = removeAllTaxes(object.getPrduct());
						System.out.println("TAX AMT"+tax);
						System.out.println("product price"+object.getProductPrice());
						System.out.println("object.getProductNewPrice() "+object.getProductNewPrice());
						double amt = object.getProductNewPrice() - tax;
						System.out.println("AMT____"+amt);
						// object.setProductNewPrice(amt); //@author Abhinav
						// Bihade,@since 16/01/2020, Des:Commented this line for Is
						// inclusive
						//return nf.format(amt)+"";
						
						return Math.round(amt)+"";
						

					}
					return "";
				}
				
			}
		};
		table.addColumn(getColumnProductNewPrice, "#Product New Price");
		table.setColumnWidth(getColumnProductNewPrice, "130px");
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub

	}

	@Override
	public void addFieldUpdater() {
		
		setFieldUpdaterOnProdictPrice();
		setFieldUpdaterOnProductNewPrice();
		setFieldUpdaterOnProductRemark();
		/**Date :25/11/2017 By:Manisha
		 * Description :TO Make duration and service editable
		  */
		setFieldUpdaterOnDuration();
		setFieldUpdaterOnService();
		/**End for Manisha**/
		setFieldUpdaterPremises();
		updateServiceTaxColumn();
		updateVatTaxColumn();
		
		updateStartDate();
		UpdateEndDate();
		
		updatePriceByPercentage();
	}



	private void setFieldUpdaterOnProdictPrice() {

		getColumnProductPrice.setFieldUpdater(new FieldUpdater<ContractRenewalProduct, String>() {
			
			@Override
			public void update(int index, ContractRenewalProduct object, String value) {
				// TODO Auto-generated method stub
				try {
					double val1 = Double.parseDouble(value);
					if(val1!=0){
						System.out.println("old price changed");
						object.setProductPrice(val1);
						object.setProductNewPrice(val1);
						object.setProductPriceNew2(val1);
						System.out.println("value"+val1);
						
						double totalAmt = gettaxAmount(val1,object);
						object.setNetPayable(totalAmt);
						
						table.redrawRow(index);
						RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
					
					}
				} catch (Exception e) {
				}
				
			}
		});
		
		
	}















	private void updatePriceByPercentage() {
		
		getColumnPercentage.setFieldUpdater(new FieldUpdater<ContractRenewalProduct, String>() {
			
			@Override
			public void update(int index, ContractRenewalProduct object, String value) {
				// TODO Auto-generated method stub
				
				try {
					
					double val1 = 0;
					try {
						val1 = Double.parseDouble(value);
						object.setPercentage(val1);
					} catch (Exception e) {
					}
					
					
//					double newprice = 	object.getProductPriceNew2();
					double newprice = 	object.getProductPrice();

					double percentageAmt = (newprice*object.getPercentage())/100;
					double updatedAmount = newprice+(percentageAmt);
					Console.log("updatedAmount"+updatedAmount);
					Console.log("percentageAmt"+percentageAmt);
					System.out.println("updatedAmount"+updatedAmount);
					object.setProductNewPrice(updatedAmount);
					
					double totalAmt = gettaxAmount(updatedAmount,object);
					object.setNetPayable(totalAmt);
					
//					object.getPrduct().setPrice(updatedAmount);
//					double tax = removeAllTaxes(object.getPrduct());
//					Console.log("tax="+tax);
//					double amt = updatedAmount - tax;
//					Console.log("amt ="+amt);
//					System.out.println("amt ="+amt);
//					object.setProductNewPrice(updatedAmount);
//
//					double totalAmt = calculateTotalAmt(object
//							.getVatTax().getTaxPrintName(), object
//							.getServiceTax().getTaxPrintName(), amt,
//							object.getServiceTax().getPercentage(),
//							object.getVatTax().getPercentage());
//					object.setNetPayable(totalAmt);
					
					table.redrawRow(index);
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);

				} catch (Exception e) {
				}
				

			}
		});
		
		
	}















	















	private void UpdateEndDate() {
		endDate.setFieldUpdater(new FieldUpdater<ContractRenewalProduct, Date>() {
			
			@Override
			public void update(int index, ContractRenewalProduct object, Date value) {

				object.setEndDate(value);
				int duration=CalendarUtil.getDaysBetween(object.getStartDate(), object.getEndDate());
				duration++;
				object.setDuaration(duration);
				System.out.println("Contract Duration : "+duration+"  Dur "+object.getDuaration());
				contractdateupdateFlag = true;
				table.redrawRow(index);
			
				
			}
		});
		
	}


	private void updateStartDate() {
		startDate.setFieldUpdater(new FieldUpdater<ContractRenewalProduct, Date>() {
			
			@Override
			public void update(int index, ContractRenewalProduct object, Date value) {
				object.setStartDate(value);

				object.setStartDate(value);
				Date date=CalendarUtil.copyDate(object.getStartDate());
				CalendarUtil.addDaysToDate(date, object.getDuaration()-1);	
				object.setEndDate(date);
				
				contractdateupdateFlag = true;
				
				table.redrawRow(index);
			}
		});
	}







	/**Date :25/11/2017 By:Manisha
	 * Description :TO Make duration and service editable
	  */

	private void setFieldUpdaterOnDuration() {
			getColumnProductDuration.setFieldUpdater(new FieldUpdater<ContractRenewalProduct, String>() {

				@Override
				public void update(int index, ContractRenewalProduct object,String value) {
//					try{
//					
//						object.setDuaration(Integer.parseInt(value));
//					}
//					catch(Exception e){
//					}
//					table.redrawRow(index);
					
					try{
						Integer val1=Integer.parseInt(value.trim());
						object.setDuaration(val1);
						Date date=CalendarUtil.copyDate(object.getStartDate());
						CalendarUtil.addDaysToDate(date, object.getDuaration()-1);
						object.setEndDate(date);
					}
					catch (Exception e)
					{
					}
					table.redrawRow(index);
					
				}
			});
			
	   }
	
	
	private void setFieldUpdaterOnService() {
		getColumnProductService.setFieldUpdater(new FieldUpdater<ContractRenewalProduct, String>() {

			@Override
			public void update(int index, ContractRenewalProduct object,String value) {
				try{
				
					object.setNoOfServices(Integer.parseInt(value));
				}
				catch(Exception e){
				}
				table.redrawRow(index);
			}
		});
	
	}
    /*********************************End********************/                   


	/**
	 * Date : 22-11-2017 BY MANISHA
	 * Description :.To update the taxes value..!!
	 */
	private void updateVatTaxColumn() {

		vatColumn.setFieldUpdater(new FieldUpdater<ContractRenewalProduct, String>() {

					@Override
					public void update(int index,
							ContractRenewalProduct object, String value) {
						try {
							String val1 = (value.trim());
							object.setVatTaxEdit(val1);
							//
							Tax tax = new Tax();

							for (int i = 0; i < vattaxlist.size(); i++) {
								if (val1.trim().equals(
										vattaxlist.get(i).getTaxChargeName())) {
									tax.setTaxName(val1);
									tax.setTaxConfigName(val1);
									tax.setPercentage(vattaxlist.get(i)
											.getTaxChargePercent());
									tax.setTaxPrintName(vattaxlist.get(i)
											.getTaxPrintName());

									break;
								}
							}
							object.setVatTax(tax);

							double totalAmt = calculateTotalAmt(object
									.getVatTax().getTaxPrintName(), object
									.getServiceTax().getTaxPrintName(), object
									.getProductNewPrice(), object
									.getServiceTax().getPercentage(), object
									.getVatTax().getPercentage());
							object.setNetPayable(totalAmt);

							RowCountChangeEvent.fire(table, getDataprovider()
									.getList().size(), true);

						} catch (NumberFormatException e) {
						}
						table.redrawRow(index);
					}
				});
	}

	/**
	 * Date : 22-11-2017 BY MANISHA
	 * Description :.To update the taxes value.........!!!!!!!
	 */
	private void updateServiceTaxColumn() {

		serviceColumn
				.setFieldUpdater(new FieldUpdater<ContractRenewalProduct, String>() {

					@Override
					public void update(int index,
							ContractRenewalProduct object, String value) {
						try {
							String val1 = (value.trim());
							object.setServiceTaxEdit(val1);
							// Tax tax = object.getServiceTax();
							// above line commneted by vijay and below new line
							// added by vijay on 27-03-2017
							Tax tax = new Tax();
							for (int i = 0; i < servicetaxlist.size(); i++) {
								if (val1.trim().equals(
										servicetaxlist.get(i)
												.getTaxChargeName())) {
									tax.setTaxName(val1);
									tax.setTaxConfigName(val1);
									tax.setPercentage(servicetaxlist.get(i)
											.getTaxChargePercent());
									tax.setTaxPrintName(servicetaxlist.get(i)
											.getTaxPrintName());
									break;
								}
							}
							object.setServiceTax(tax);

							double totalAmt = calculateTotalAmt(object
									.getVatTax().getTaxPrintName(), object
									.getServiceTax().getTaxPrintName(), object
									.getProductNewPrice(), object
									.getServiceTax().getPercentage(), object
									.getVatTax().getPercentage());
							
							object.setNetPayable(totalAmt);

							RowCountChangeEvent.fire(table, getDataprovider()
									.getList().size(), true);

						} catch (NumberFormatException e) {
						}
						table.redrawRow(index);
					}
				});
	}

	/**end**/
	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub

	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
	}

	public double removeAllTaxes(SuperProduct entity) {
		double vat = 0, service = 0;
		double tax = 0, retrVat = 0, retrServ = 0;
		System.out.println("entity.getPrice()==== "+entity.getPrice());
		if (entity instanceof ServiceProduct) {
			ServiceProduct prod = (ServiceProduct) entity;
			if (prod.getServiceTax() != null
					&& prod.getServiceTax().isInclusive() == true) {
				service = prod.getServiceTax().getPercentage();
			}
			if (prod.getVatTax() != null
					&& prod.getVatTax().isInclusive() == true) {
				vat = prod.getVatTax().getPercentage();
			}
		}

		if (entity instanceof ItemProduct) {
			ItemProduct prod = (ItemProduct) entity;
			if (prod.getVatTax() != null
					&& prod.getVatTax().isInclusive() == true) {
				vat = prod.getVatTax().getPercentage();
			}
			if (prod.getServiceTax() != null
					&& prod.getServiceTax().isInclusive() == true) {
				service = prod.getServiceTax().getPercentage();
			}
		}

		
		if (vat != 0 && service == 0) {
			retrVat = entity.getPrice() / (1 + (vat / 100));
			retrVat = entity.getPrice() - retrVat;
		}
		if (service != 0 && vat == 0) {
			retrServ = entity.getPrice() / (1 + service / 100);
			retrServ = entity.getPrice() - retrServ;
		}
		if (service != 0 && vat != 0) {
//			// Here if both are inclusive then first remove service tax and then
//			// on that amount
//			// calculate vat.
//			double removeServiceTax = (entity.getPrice() / (1 + service / 100));
//
//			// double taxPerc=service+vat;
//			// retrServ=(entity.getPrice()/(1+taxPerc/100)); //line changed
//			// below
//			retrServ = (removeServiceTax / (1 + vat / 100));
//			retrServ = entity.getPrice() - retrServ;
			
			
			if(entity instanceof ItemProduct)
			{
				ItemProduct prod=(ItemProduct) entity;
				if(prod.getServiceTax().getTaxPrintName()!=null && ! prod.getServiceTax().getTaxPrintName().equals("")
				   && prod.getVatTax().getTaxPrintName()!=null && ! prod.getVatTax().getTaxPrintName().equals(""))
				{

					double dot = service + vat;
					retrServ=(entity.getPrice()/(1+dot/100));
					retrServ=entity.getPrice()-retrServ;
//					retrVat=0;

				}
				else{
					// Here if both are inclusive then first remove service tax and then on that amount
					// calculate vat.
					double removeServiceTax=(entity.getPrice()/(1+service/100));
					//double taxPerc=service+vat;
					//retrServ=(entity.getPrice()/(1+taxPerc/100)); //line changed below
					retrServ=(removeServiceTax/(1+vat/100));
					retrServ=entity.getPrice()-retrServ;
				}
			}
			
			if(entity instanceof ServiceProduct)
			{
				ServiceProduct prod=(ServiceProduct) entity;
				if(prod.getServiceTax().getTaxPrintName()!=null && ! prod.getServiceTax().getTaxPrintName().equals("")
				   && prod.getVatTax().getTaxPrintName()!=null && ! prod.getVatTax().getTaxPrintName().equals(""))
				{
					double dot = service + vat;
					retrServ=(entity.getPrice()/(1+dot/100));
					retrServ=entity.getPrice()-retrServ;
				}
				else
				{
					// Here if both are inclusive then first remove service tax and then on that amount
					// calculate vat.
					double removeServiceTax=(entity.getPrice()/(1+service/100));
					//double taxPerc=service+vat;
					//retrServ=(entity.getPrice()/(1+taxPerc/100)); //line changed below
					retrServ=(removeServiceTax/(1+vat/100));
					retrServ=entity.getPrice()-retrServ;
				}
			}
		}
		System.out.println("retrVat "+retrVat);
		System.out.println("retrServ "+retrServ);

		tax = retrVat + retrServ;
		return tax;
	}

	/**Date :1/11/2017 By :Manisha
	 * Description :TO calculate total amount.
	 * 
	 */
	
	public static double calculateTotalAmt(String vattaxname,
			String sertaxname, double newPrice, double serTax, double vatTax) {

		double totalAmt = 0;
		double vatTaxAmt = 0;
		double setTaxAmt = 0;
		if (vatTax == 12.5 || vatTax == 5) {
			vatTaxAmt = newPrice * (vatTax / 100);

		}
		totalAmt = newPrice + vatTaxAmt;

		if (serTax == 14.5 || serTax == 15 || serTax == 18) {
			setTaxAmt = totalAmt * (serTax / 100);
			System.out.println(setTaxAmt);

			totalAmt = totalAmt + setTaxAmt;

		}

		if (vatTax != 0 && !vattaxname.equals("VAT")
				&& !sertaxname.equalsIgnoreCase("SERVICE")) {
			vatTaxAmt = newPrice * (vatTax / 100);
		}
		System.out.println(vatTaxAmt);
		totalAmt = newPrice + vatTaxAmt;

		if (serTax != 0 && !sertaxname.equalsIgnoreCase(null)
				
				&& !sertaxname.equalsIgnoreCase("")) 
		{
			setTaxAmt = newPrice * (serTax / 100);
			totalAmt = totalAmt + setTaxAmt;
		}

		System.out.println("new price " + totalAmt);
		return totalAmt;

	}
	/*********end********/
	
	/**
	 * Date : 04-12-2017 BY ANIL
	 * 
	 */
	private void getEditableProductNameCol() {
		EditTextCell editCell=new EditTextCell();
		getEditableProductNameCol=new Column<ContractRenewalProduct,String>(editCell){
			@Override
			public String getValue(ContractRenewalProduct object)
			{
				
				if(object.getProductName()!=null && !object.getProductName().equals("")){
					return object.getProductName();
				}
				else{
					return "";
				}
			}
		};
		table.addColumn(getEditableProductNameCol,"#Name");
		table.setColumnWidth(getEditableProductNameCol, 100,Unit.PX);
		
		getEditableProductNameCol.setFieldUpdater(new FieldUpdater<ContractRenewalProduct, String>() {

			@Override
			public void update(int index, ContractRenewalProduct object,String value) {
				object.setProductName(value);
				if(object.getPrduct()!=null){
					object.getPrduct().setProductName(value);
				}
				table.redraw();
			}
		});
	}
	
	/**
	 * End
	 */
	
	
	private void geEditableOldPriceCol() {
		EditTextCell cell = new EditTextCell();
		geEditableOldPriceCol = new Column<ContractRenewalProduct, String>(
				cell) {
			@Override
			public String getValue(ContractRenewalProduct object) {
				if (object.getProductPrice() != -1) {
					double tax = removeAllTaxes(object.getPrduct(),
							object.getProductPrice());
					double amt = object.getProductPrice() - tax;
					// object.setProductPrice(amt);//@author Abhinav
					// Bihade,@since 16/01/2020, Des: Commented this line for Is
					// inclusive
					//return nf.format(amt)+"";
					return Math.round(amt)+"";
				}
				return "";
			}
		};
		table.addColumn(geEditableOldPriceCol, "#Product Price");
		table.setColumnWidth(geEditableOldPriceCol, "100px");
		
		geEditableOldPriceCol.setFieldUpdater(new FieldUpdater<ContractRenewalProduct, String>() {
			@Override
			public void update(int index, ContractRenewalProduct object,String value) {
				try {
					double val1 = Double.parseDouble(value);
					
					double tax = removeAllTaxes(object.getPrduct(),object.getProductPrice());
					double amt = object.getProductPrice() - tax;
//					if(nf.parse(nf.format(amt))!=val1){
					if(Math.round(amt)!=val1){
						object.setProductPrice(val1);
						table.redrawRow(index);
					}
					
				} catch (Exception e) {
					GWTCAlert alert=new GWTCAlert();
					alert.alert("Only numeric value is allowed!");
				}
				
			}
		});
		
	}

	protected double removeAllTaxes(SuperProduct entity, double productPrice) {

		double vat = 0, service = 0;
		double tax = 0, retrVat = 0, retrServ = 0;
		if (entity instanceof ServiceProduct) {
			ServiceProduct prod = (ServiceProduct) entity;
			if (prod.getServiceTax() != null
					&& prod.getServiceTax().isInclusive() == true) {
				service = prod.getServiceTax().getPercentage();
			}
			if (prod.getVatTax() != null
					&& prod.getVatTax().isInclusive() == true) {
				vat = prod.getVatTax().getPercentage();
			}
		}

		if (entity instanceof ItemProduct) {
			ItemProduct prod = (ItemProduct) entity;
			if (prod.getVatTax() != null
					&& prod.getVatTax().isInclusive() == true) {
				vat = prod.getVatTax().getPercentage();
			}
			if (prod.getServiceTax() != null
					&& prod.getServiceTax().isInclusive() == true) {
				service = prod.getServiceTax().getPercentage();
			}
		}

		if (vat != 0 && service == 0) {
			retrVat = productPrice / (1 + (vat / 100));
			retrVat = productPrice - retrVat;
		}
		if (service != 0 && vat == 0) {
			retrServ = productPrice / (1 + service / 100);
			retrServ = productPrice - retrServ;
		}
		if (service != 0 && vat != 0) {
			// // Here if both are inclusive then first remove service tax and
			// then
			// // on that amount
			// // calculate vat.
			// double removeServiceTax = (entity.getPrice() / (1 + service /
			// 100));
			//
			// // double taxPerc=service+vat;
			// // retrServ=(entity.getPrice()/(1+taxPerc/100)); //line changed
			// // below
			// retrServ = (removeServiceTax / (1 + vat / 100));
			// retrServ = entity.getPrice() - retrServ;

			if (entity instanceof ItemProduct) {
				ItemProduct prod = (ItemProduct) entity;
				if (prod.getServiceTax().getTaxPrintName() != null
						&& !prod.getServiceTax().getTaxPrintName().equals("")
						&& prod.getVatTax().getTaxPrintName() != null
						&& !prod.getVatTax().getTaxPrintName().equals("")) {

					double dot = service + vat;
					retrServ = (productPrice / (1 + dot / 100));
					retrServ = productPrice - retrServ;
					// retrVat=0;

				} else {
					// Here if both are inclusive then first remove service tax
					// and then on that amount
					// calculate vat.
					double removeServiceTax = (productPrice / (1 + service / 100));
					// double taxPerc=service+vat;
					// retrServ=(productPrice/(1+taxPerc/100)); //line changed
					// below
					retrServ = (removeServiceTax / (1 + vat / 100));
					retrServ = productPrice - retrServ;
				}
			}

			if (entity instanceof ServiceProduct) {
				ServiceProduct prod = (ServiceProduct) entity;
				if (prod.getServiceTax().getTaxPrintName() != null
						&& !prod.getServiceTax().getTaxPrintName().equals("")
						&& prod.getVatTax().getTaxPrintName() != null
						&& !prod.getVatTax().getTaxPrintName().equals("")) {
					double dot = service + vat;
					retrServ = (productPrice / (1 + dot / 100));
					retrServ = productPrice - retrServ;
				} else {
					// Here if both are inclusive then first remove service tax
					// and then on that amount
					// calculate vat.
					double removeServiceTax = (productPrice / (1 + service / 100));
					// double taxPerc=service+vat;
					// retrServ=(productPrice/(1+taxPerc/100)); //line changed
					// below
					retrServ = (removeServiceTax / (1 + vat / 100));
					retrServ = productPrice - retrServ;
				}
			}
		}
		tax = retrVat + retrServ;
		return tax;

	}

	
	private void getColumnPercentage() {

		 EditTextCell cell = new EditTextCell();
		 getColumnPercentage = new Column<ContractRenewalProduct, String>(cell) {
			
			@Override
			public String getValue(ContractRenewalProduct object) {
				return object.getPercentage()+"";
			}
		};
		table.addColumn(getColumnPercentage, "#Percentage");
		table.setColumnWidth(getColumnPercentage, "100px");
		
		
	}
	
	
	public double gettaxAmount(double updatedAmount,
			ContractRenewalProduct object) {
		
		double tax1Amount = 0;
		double tax2Amount = 0;
		
		if(object.getVatTax()!=null && object.getVatTax().getPercentage()!=0){
			tax1Amount = (updatedAmount*object.getVatTax().getPercentage())/100;
		}
		if(object.getServiceTax()!=null && object.getServiceTax().getPercentage()!=0){
			tax2Amount = (updatedAmount*object.getServiceTax().getPercentage())/100;
		}
		double totalAmount = updatedAmount +tax1Amount+tax2Amount;
		
		return totalAmount;
	}
}