package com.slicktechnologies.client.views.contractrenewal;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.contractrenewal.ContractRenewal;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class ContractRenewalForm extends FormScreen<ContractRenewal>implements ClickHandler, ValueChangeHandler<String> {

	DateBox fromdate, todate;
	ObjectListBox<Branch> olbBranch;
	ObjectListBox<Employee> olbeSalesPerson;
	TextBox tbContractId;
	ListBox lbStatus;
	PersonInfoComposite pic;
	
	ContractRenewalTable table;
	ContractRenewalTable processedtable;
	
	Label label;
	
	Button btnGo;
	
	Button btnDownload;
	Button btnDownload1;
	TextBox tbblanck;
	
	Button btnResetFilters;
	
	public ContractRenewalForm() {
		super(FormStyle.DEFAULT);
//		super();
		createGui();
		processLevelBar.btnLabels[0].setVisible(true);
		processLevelBar.btnLabels[1].setVisible(true);
		processLevelBar.btnLabels[2].setVisible(true);
		processLevelBar.btnLabels[3].setVisible(true);
		processLevelBar.btnLabels[4].setVisible(true);
		processLevelBar.btnLabels[5].setVisible(true);
		processLevelBar.btnLabels[6].setVisible(true);
		processLevelBar.btnLabels[7].setVisible(true);
		processLevelBar.btnLabels[8].setVisible(true);
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("ContractRenewal", "ShowMarkRenewedButton")) 
		{	processLevelBar.btnLabels[9].setVisible(true);
			processLevelBar.btnLabels[9].getElement().addClassName("redtext");
		}
/************these buttons for updating  the code in contract, quotation and services ****************/
		
//		processLevelBar.btnLabels[8].setVisible(true);
//		processLevelBar.btnLabels[9].setVisible(true);
//		processLevelBar.btnLabels[10].setVisible(true);
	}
	
	
	private void initalizeWidget() {

		table=new ContractRenewalTable();
		processedtable=new ContractRenewalTable();
		
		fromdate = new DateBoxWithYearSelector();
		todate = new DateBoxWithYearSelector();
		
		Date startDateOfMonth = new Date();
		CalendarUtil.setToFirstDayOfMonth(startDateOfMonth);
		
		Date endDateOfMonth = new Date();
		CalendarUtil.setToFirstDayOfMonth(endDateOfMonth);
		CalendarUtil.addMonthsToDate(endDateOfMonth, 1);
		CalendarUtil.setToFirstDayOfMonth(endDateOfMonth);
		CalendarUtil.addDaysToDate(endDateOfMonth, -1);
		
		fromdate.setValue(startDateOfMonth);
		todate.setValue(endDateOfMonth);
		
		olbBranch= new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbBranch);
		
		olbeSalesPerson=new ObjectListBox<Employee>();
		AppUtility.makeSalesPersonListBoxLive(olbeSalesPerson);
		
		tbContractId=new TextBox();
		tbContractId.addValueChangeHandler(this);
		
		MyQuerry querry=new MyQuerry();
		Filter filter=new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		querry.setQuerryObject(new Customer());
		pic = new PersonInfoComposite(querry, false);
		
		btnGo=new Button("GO");
		
		lbStatus=new ListBox();
		lbStatus.addItem("--SELECT--");
//		lbStatus.addItem("All");
		lbStatus.addItem("Unprocessed");
		lbStatus.addItem("Processed");
		lbStatus.addItem(AppConstants.RENEWED);
		lbStatus.addItem(AppConstants.DONOTRENEW);
		/** Date 07-12-202 by Vijay as per Nitin Sir not required so commented below ***/
//		lbStatus.addItem("Never Renew");
		
		label=new Label("OR");
		
	
		btnDownload=new Button("Download");
		btnDownload1=new Button("Download");
		tbblanck = new TextBox();
		tbblanck.setVisible(false);
		
		btnResetFilters = new Button("Reset Filters");
		
	}
	
	
	@Override
	public void createScreen() {
		initalizeWidget();
	
		//Ashwini Patil Date:28-07-2023 removed "Update Quotation" option as Ajinkya reported issue that it is not working. Also there is no meaningful code found for this so Nitin sir asked to hide the option
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("ContractRenewal", "PCAMBCustomization"))
		{
			System.out.println("in side if ");
			//processlevelBarNames=new String[]{"Process Renewal","Email Reminder","Print Reminder","Print Covering Letter","Remind Me Later","Renew Contract","Do Not Renew","Send SMS","Update Contract","Update Quotation","Update Service"};
			processlevelBarNames=new String[]{"Process Renewal","Email Reminder","Print Reminder","Print Covering Letter","Remind Me Later","Renew Contract","Do Not Renew","Send SMS","Update Service","View Services"}; //"Update Contract" removed by Ashwini Patil
		}else if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("ContractRenewal", "ShowMarkRenewedButton")) {
			processlevelBarNames=new String[]{"Process Renewal","Email Reminder","Print Reminder","Remind Me Later","Renew Contract","Do Not Renew","Send SMS","Update Service","View Services",AppConstants.markcontractasrenewed};//"Update Contract" removed by Ashwini Patil
		}else{
			//processlevelBarNames=new String[]{"Process Renewal","Email Reminder","Print Reminder","Remind Me Later","Renew Contract","Do Not Renew","Send SMS","Update Contract","Update Quotation","Update Service"};
			processlevelBarNames=new String[]{"Process Renewal","Email Reminder","Print Reminder","Remind Me Later","Renew Contract","Do Not Renew","Send SMS","Update Service","View Services"};//"Update Contract" removed by Ashwini Patil
		}
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingSearchFilter=fbuilder.setlabel("Search Filters").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",pic);
		FormField pic= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Branch",olbBranch);
		FormField olbBranch= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
//		fbuilder = new FormFieldBuilder("Sales Person",olbeSalesPerson);
//		FormField olbeSalesPerson= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("From Date",fromdate);
		FormField fromdate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("To Date",todate);
		FormField todate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Contract Id",tbContractId);
		FormField tbContractId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Status",lbStatus);
		FormField lbStatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",btnGo);
		FormField btnGo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingSearchResult=fbuilder.setlabel("Unprocessed Contract Renewals").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",table.getTable());
		FormField table= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingSearchProcessedResult=fbuilder.setlabel("(Processed / Renewed / Do not renew) Contract Renewals").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",processedtable.getTable());
		FormField processedtable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
//		fbuilder = new FormFieldBuilder("",label);
//		FormField label= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
//		
//		fbuilder = new FormFieldBuilder("",tbblanck);
//		FormField tbblanck= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder("",btnDownload);
		FormField btnDownload= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",btnDownload1);
		FormField btnDownload1= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",btnResetFilters);
		FormField fbtnResetFilters = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		FormField[][] formfield = {   
				{fgroupingSearchFilter},
				{pic},
				{lbStatus,olbBranch,fromdate,todate,},
				{tbContractId,fbtnResetFilters,btnGo},
				{fgroupingSearchResult},
				{btnDownload},
				{table},
				{fgroupingSearchProcessedResult},
				{btnDownload1},
				{processedtable}
		};
		this.fields=formfield;	
	}
	
	

	@Override
	public void toggleAppHeaderBarMenu() {
		if (AppMemory.getAppMemory().currentState == ScreeenState.NEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Discard") || text.contains("Email") || text.contains(AppConstants.MESSAGE) || text.contains(AppConstants.COMMUNICATIONLOG)) {
					menus[k].setVisible(true);
				} else
					menus[k].setVisible(false);
			}
		}
		else if (AppMemory.getAppMemory().currentState == ScreeenState.EDIT) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Discard"))
					menus[k].setVisible(true);
				else
					menus[k].setVisible(false);
			}
		}
		else if (AppMemory.getAppMemory().currentState == ScreeenState.VIEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
//				if (text.contains("Discard"))
				if (text.contains("Discard") || text.contains("Email") || text.contains(AppConstants.MESSAGE) || text.contains(AppConstants.COMMUNICATIONLOG)) 
					menus[k].setVisible(true);
				else
					menus[k].setVisible(false);
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.CONTRACTRENEWAL,LoginPresenter.currentModule.trim());
	}

	@Override
	public void updateModel(ContractRenewal model) {
		
	}

	@Override
	public void updateView(ContractRenewal model) {
		
	}
	
	@Override
	public void onClick(ClickEvent event) {
	}


	@Override
	public boolean validate() {
//		return super.validate();
		boolean superboolean=super.validate();
		if(superboolean==false){
			return false;
		}
		if(table.getDataprovider().getList().size()!=0){
			List<RenewalResult> selectedlist= getAllSelectedRecords();
			if(selectedlist.size()>1){
				showDialogMessage("Please select only one record ! ");
				return false;
			}
		}else{
			showDialogMessage("Please select at least one record from table ! ");
			return false;
		}
		
		return true;
	}
	
	
	/**
	  * This method is used for getting all selected records .
	  * When the checkboxes are selected  then these records are fetched in arraylist
	  * and this method returns that list
	  * @return
	  */
	 
	 public List<RenewalResult> getAllSelectedRecords()
	 {
		 List<RenewalResult> lisbillingtable=table.getDataprovider().getList();
		 ArrayList<RenewalResult> selectedlist=new ArrayList<RenewalResult>();
		 for(RenewalResult doc:lisbillingtable)
		 {
			 if(doc.getRecordSelect()==true){
				 doc.setStatus("Inprocessed");
//				 doc.setRecordSelect(false);
				 selectedlist.add(doc);
			 }
		 }
		 return selectedlist;
	 }
	 
	 public List<RenewalResult> getAllUncheckedRecords()
	 {
		 List<RenewalResult> lisbillingtable=table.getDataprovider().getList();
		 ArrayList<RenewalResult> uncheckedlist=new ArrayList<RenewalResult>();
		 for(RenewalResult doc:lisbillingtable)
		 {
			 if(doc.getRecordSelect()==false){
				 uncheckedlist.add(doc);
			 }
		 }
		 return uncheckedlist;
	 }

	 public List<RenewalResult> getAllProcessedSelectedRecords()
	 {
		 List<RenewalResult> lisbillingtable=processedtable.getDataprovider().getList();
		 ArrayList<RenewalResult> selectedlist=new ArrayList<RenewalResult>();
		 for(RenewalResult doc:lisbillingtable)
		 {
			 if(doc.getRecordSelect()==true)
				 selectedlist.add(doc);
		 }
		 return selectedlist;
	 }
	 
	 public List<RenewalResult> getAllProcessedUncheckedRecords()
	 {
		 List<RenewalResult> lisbillingtable=processedtable.getDataprovider().getList();
		 ArrayList<RenewalResult> selectedlist=new ArrayList<RenewalResult>();
		 for(RenewalResult doc:lisbillingtable)
		 {
			 if(doc.getRecordSelect()==false)
				 selectedlist.add(doc);
		 }
		 return selectedlist;
	 }

	@Override
	public void setToNewState() {
		super.setToNewState();
	}


	@Override
	public void setToViewState() {
		super.setToViewState();
	}


	@Override
	public void setToEditState() {
		super.setToEditState();
		
	}


	public Button getBtnDownload() {
		return btnDownload;
	}


	public void setBtnDownload(Button btnDownload) {
		this.btnDownload = btnDownload;
	}


	public Button getBtnDownload1() {
		return btnDownload1;
	}


	public void setBtnDownload1(Button btnDownload1) {
		this.btnDownload1 = btnDownload1;
	}


	public TextBox getTbblanck() {
		return tbblanck;
	}


	public void setTbblanck(TextBox tbblanck) {
		this.tbblanck = tbblanck;
	}


	@Override
	public void refreshTableData() {
		// TODO Auto-generated method stub
		super.refreshTableData();
		table.getTable().redraw();
		processedtable.getTable().redraw();
	}


	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
		
		if(event.getSource().equals(tbContractId)){
			if(tbContractId.getValue()!=null && !tbContractId.getValue().equals("")){
				pic.setEnable(false);
				if (!LoginPresenter.branchRestrictionFlag) 
					olbBranch.setEnabled(false);
				fromdate.setEnabled(false);
				todate.setEnabled(false);
				lbStatus.setEnabled(false);
				
				pic.clear();
				if (!LoginPresenter.branchRestrictionFlag) 
				olbBranch.setSelectedIndex(0);
				fromdate.setValue(null);
				todate.setValue(null);
				lbStatus.setSelectedIndex(0);
			}
			else{
				pic.setEnable(true);
				olbBranch.setEnabled(true);
				fromdate.setEnabled(true);
				todate.setEnabled(true);
				lbStatus.setEnabled(true);
				
			}
			
		}
	}
	

}
