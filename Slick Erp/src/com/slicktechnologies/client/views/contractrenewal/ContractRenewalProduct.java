package com.slicktechnologies.client.views.contractrenewal;

import java.util.Date;

import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.productlayer.Tax;

public class ContractRenewalProduct {
	
	
	String productCode;
	String productName;
	int duaration;
	double productPrice;
	double productNewPrice;
	String remark;
	
	String premises;
	int srNo;
	protected boolean recordSelect;
	
	/**
	 * rohan added this variable for adding No of services in contract renewal 
	 */
	
	int NoOfServices;
	
	SuperProduct product;
	/**
	 * @author Vijay 
	 * added for revise start end Date 
	 */
	Date startDate;
	Date endDate;
	
	/**
	 * rohan added this for tax edit in contract renewal and changes required by ultra pest 
	 * 
	 */
	
	double percentage;
	/**
	 * @author Vijay Added for logic purpose 
	 * Des :- when we added percentage the new price will set as per percentage if we set percentage 0 then productprice
	 * should be updated so added this field to maintin old price;
	 */
	double productPriceNew2;
	
	public SuperProduct getPrduct() {

		if (serviceProduct == null)
			return itemProduct;

		else if (itemProduct == null)
			return serviceProduct;
		else {
			return null;
		}
	}

	/**
	 * Sets the prduct.
	 *
	 * @param product
	 *            the new prduct
	 */
	public void setPrduct(SuperProduct product) {
		if (product instanceof ServiceProduct)
			serviceProduct = (ServiceProduct) product;
		else if (product instanceof ItemProduct)
			itemProduct = (ItemProduct) product;
		System.out.println("Service Product IS " + serviceProduct);
		System.out.println("Item Product IS " + itemProduct);
	}
	
	
	/** The service product */
	private ServiceProduct serviceProduct;
	
	/** The item product */
	private ItemProduct itemProduct;
	double netPayable;
	String area = "NA";
	String serviceTaxEdit;
	String vatTaxEdit;
	  /**
	   * ends here 
	   */
	
	
	
	/**
	 * Gets the service tax.
	 *
	 * @return the service tax
	 */
	public Tax getServiceTax() {
		if(serviceProduct!=null)
			return serviceProduct.getServiceTax();
		else
			return itemProduct.getServiceTax();
	}

	/**
	 * Sets the service tax.
	 *
	 * @param serviceTax the new service tax
	 */
	public void setServiceTax(Tax serviceTax) {
		if(serviceProduct!=null)
			serviceProduct.setServiceTax(serviceTax);
		else
			itemProduct.setServiceTax(serviceTax);
	}
	
	
	
	/**
	 * Gets the vat tax.
	 *
	 * @return the vat tax
	 */
	public Tax getVatTax() {
		if(itemProduct!=null)
			return itemProduct.getVatTax();
		else
			return serviceProduct.getVatTax();
	}
	
	
	/**
	 * Sets the vat tax.
	 *
	 * @param vatTaxEntity the new vat tax
	 */
	public void setVatTax(Tax vatTaxEntity) {
		if(itemProduct!=null)
			itemProduct.setVatTax(vatTaxEntity);
		else
			serviceProduct.setVatTax(vatTaxEntity);
	}
	
	public ContractRenewalProduct() {
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public int getDuaration() {
		return duaration;
	}

	public void setDuaration(int duaration) {
		this.duaration = duaration;
	}

	public double getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(double productPrice) {
		this.productPrice = productPrice;
	}

	public double getProductNewPrice() {
		return productNewPrice;
	}

	public void setProductNewPrice(double productNewPrice) {
		this.productNewPrice = productNewPrice;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getPremises() {
		return premises;
	}

	public void setPremises(String premises) {
		this.premises = premises;
	}

	public boolean isRecordSelect() {
		return recordSelect;
	}

	public void setRecordSelect(boolean recordSelect) {
		this.recordSelect = recordSelect;
	}

	public int getSrNo() {
		return srNo;
	}

	public void setSrNo(int srNo) {
		this.srNo = srNo;
	}

	public String getServiceTaxEdit() {
		return serviceTaxEdit;
	}

	public void setServiceTaxEdit(String serviceTaxEdit) {
		this.serviceTaxEdit = serviceTaxEdit;
	}

	public String getVatTaxEdit() {
		return vatTaxEdit;
	}

	public void setVatTaxEdit(String vatTaxEdit) {
		this.vatTaxEdit = vatTaxEdit;
	}

	public double getNetPayable() {
		return netPayable;
	}

	public void setNetPayable(double netPayable) {
		this.netPayable = netPayable;
	}

	public int getNoOfServices() {
		return NoOfServices;
	}

	public void setNoOfServices(int noOfServices) {
		NoOfServices = noOfServices;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public double getPercentage() {
		return percentage;
	}

	public void setPercentage(double percentage) {
		this.percentage = percentage;
	}

	public double getProductPriceNew2() {
		return productPriceNew2;
	}

	public void setProductPriceNew2(double productPriceNew2) {
		this.productPriceNew2 = productPriceNew2;
	}
	
	
	

}
