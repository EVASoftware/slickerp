package com.slicktechnologies.client.views.contractrenewal;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.reusabledata.ConditionDialogBox;
import com.slicktechnologies.client.services.CommunicationLogService;
import com.slicktechnologies.client.services.CommunicationLogServiceAsync;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.EmailService;
import com.slicktechnologies.client.services.EmailServiceAsync;
import com.slicktechnologies.client.services.SmsService;
import com.slicktechnologies.client.services.SmsServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.communicationlog.CommunicationLogPopUp;
import com.slicktechnologies.client.views.complain.ServiceDateBoxPopup;
import com.slicktechnologies.client.views.contract.ContractForm;
import com.slicktechnologies.client.views.contract.ContractPresenter;
import com.slicktechnologies.client.views.contract.ContractService;
import com.slicktechnologies.client.views.contract.ContractServiceAsync;
import com.slicktechnologies.client.views.customertraining.CustomerTrainingAttendies;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.client.views.humanresource.allocation.leavecalendarallocation.AllocationResult;
import com.slicktechnologies.client.views.humanresource.allocation.leavecalendarallocation.LeaveAllocationService;
import com.slicktechnologies.client.views.humanresource.allocation.leavecalendarallocation.LeaveAllocationServiceAsync;
import com.slicktechnologies.client.views.paymentinfo.invoicedetails.DescriptionPopup;
import com.slicktechnologies.client.views.popups.NewEmailPopUp;
import com.slicktechnologies.client.views.popups.SMSPopUp;
import com.slicktechnologies.client.views.popups.SingleDropDownPopup;
import com.slicktechnologies.client.views.service.ServiceForm;
import com.slicktechnologies.shared.BranchWiseScheduling;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.contractrenewal.ContractRenewal;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.helperlayer.SmsConfiguration;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesorder.SalesQuotation;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.servicerelated.ServiceSchedule;
import com.slicktechnologies.shared.common.workorder.WorkOrder;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;

public class ContractRenewalPresenter extends FormScreenPresenter<ContractRenewal> implements ClickHandler{


	static ContractRenewalForm form;
	static ContractRenewalServiceAsync contractRenewalServ = GWT.create(ContractRenewalService.class);
	EmailServiceAsync emailService=GWT.create(EmailService.class);
	final static  GenricServiceAsync async = GWT.create(GenricService.class);
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	ServiceDateBoxPopup datePopup = new ServiceDateBoxPopup("Date","Description");
	PopupPanel panel = new PopupPanel(true);
	
	ConditionDialogBox conditionPopup=new ConditionDialogBox("Do you want to print on preprinted Stationery",AppConstants.YES,AppConstants.NO);
	SmsTemplatePopup smsPopup=new SmsTemplatePopup();
	int customQuotation=0;
	ContractRenewal contractRenewal;
	int cnt =0;
	/**
	 * nidhi
	 * 
	 * @param view
	 * @param model
	 */
	ContractNonRenewalPopup nonRenewalMsgPopup =null;
	boolean renewalFlag = false;
	
	SingleDropDownPopup singledropdownpopup = new SingleDropDownPopup();
	
	ConditionDialogBox conditionPopupThai=new ConditionDialogBox("Select language to print renewal letter",AppConstants.ENGLISH,AppConstants.THAI);
	
	NewEmailPopUp emailpopup = new NewEmailPopUp();
	SMSPopUp smspopup = new SMSPopUp();

	CommunicationLogPopUp CommunicationLogPopUp = new CommunicationLogPopUp();
	CommunicationLogServiceAsync communicationService = GWT.create(CommunicationLogService.class);
	
	PopupPanel panelone = new PopupPanel(true);
	int cntone=0; //panelone  cntone
	ServiceDateBoxPopup otpPopup=new ServiceDateBoxPopup("OTP"); //Ashwini Patil Date:5-07-2023 to restrict download with otp for hi tech client
	PopupPanel otppopupPanel;
	int generatedOTP;
	int downloadBtnNo=0;
	SmsServiceAsync smsService = GWT.create(SmsService.class);
	GeneralViewDocumentPopup genralInvoicePopup=new GeneralViewDocumentPopup();
	DescriptionPopup descriptionPopup=new DescriptionPopup();
	
	public ContractRenewalPresenter(UiScreen<ContractRenewal> view,ContractRenewal model) {
		super(view, model);
		form = (ContractRenewalForm) view;
		form.btnGo.addClickHandler(this);
		datePopup.getBtnOne().addClickHandler(this);
		datePopup.getBtnTwo().addClickHandler(this);
		
		smsPopup.getBtnOne().addClickHandler(this);
		smsPopup.getBtnTwo().addClickHandler(this);
		
		form.btnDownload.addClickHandler(this);
		form.btnDownload1.addClickHandler(this);
		
		conditionPopup.getBtnOne().addClickHandler(this);
		conditionPopup.getBtnTwo().addClickHandler(this);
		
		/**
		 * Date 20-03-2019 by Vijay for NBHC CCPM Quotation is mandatory to renew contract.
		 */
		singledropdownpopup.getLblOk().addClickHandler(this);
		singledropdownpopup.getLblCancel().addClickHandler(this);
		
		
		conditionPopupThai.getBtnOne().setText(AppConstants.ENGLISH);
		conditionPopupThai.getBtnTwo().setText(AppConstants.THAI);
		
		conditionPopupThai.getBtnOne().addClickHandler(this);
		conditionPopupThai.getBtnTwo().addClickHandler(this);
	
		CommunicationLogPopUp.getBtnOk().addClickHandler(this);
		CommunicationLogPopUp.getBtnCancel().addClickHandler(this);
		CommunicationLogPopUp.getBtnAdd().addClickHandler(this);
		
		form.btnResetFilters.addClickHandler(this);
		otpPopup.getBtnOne().addClickHandler(this);
		otpPopup.getBtnTwo().addClickHandler(this);
		descriptionPopup.getLblOk().addClickHandler(this);
		descriptionPopup.getLblCancel().addClickHandler(this);
	}
	
	
	public static void initalize() {
		ContractRenewalForm form = new ContractRenewalForm();
		ContractRenewalPresenter presenter = new ContractRenewalPresenter(form,new ContractRenewal());
		AppMemory.getAppMemory().stickPnel(form);
	}

	public boolean validateStatus(List<RenewalResult> list){
		for(int i=0;i<list.size();i++){
			if(list.get(i).getStatus().equals("Renewed")){
				form.showDialogMessage("Please select vaild record!");
				return false;
			}
		}
		return true;
	}
	
	public static void showMsg(String msg){
		form.showDialogMessage(msg);
	}
	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel label = (InlineLabel) e.getSource();
		String text = label.getText().trim();
		
		if (text.equals("Process Renewal")){
			final List<RenewalResult>selectedlist=form.getAllSelectedRecords();
			final List<RenewalResult>uncheckedlist=form.getAllUncheckedRecords();
			if(selectedlist.size()!=0){
				ArrayList<ContractRenewal> conRenList=getContractrenewalProcessList(selectedlist);
				
				form.showWaitSymbol();
				contractRenewalServ.processContractRenewal(conRenList, new AsyncCallback<ArrayList<RenewalResult>>() {

					@Override
					public void onFailure(Throwable caught) {
						form.hideWaitSymbol();
						form.showDialogMessage("Unexpected Error !");
					}

					@Override
					public void onSuccess(ArrayList<RenewalResult> result) {
//						if(result!=null){
							form.table.connectToLocal();
//							form.processedtable.connectToLocal();
							
							form.table.getTable().redrawHeaders();
							form.table.getTable().redraw();
							if(uncheckedlist.size()!=0){
								form.table.getDataprovider().setList(uncheckedlist);
								
							}else{
								
							}
//							form.processedtable.getDataprovider().getList().addAll(selectedlist);
							
							/**
							 * Date 28-03-2017
							 * added by vijay becs when we click on process renewal its showing its status Inprocessed. before this change this RPC doesnt return anything
							 * with new code this rpc return processed list that assigned to processed table
							 */
							List<RenewalResult> processedlist = new ArrayList<RenewalResult> () ;
							processedlist.addAll(result);
							form.processedtable.getDataprovider().getList().addAll(processedlist);
							//table.redrawHeaders();
								form.processedtable.getTable().redrawHeaders();
							
							/**
							 * ends here
							 */
							
							form.hideWaitSymbol();
							if(result.size()==0){
								form.showDialogMessage("This contract is already processed");

							}
							else{
								form.showDialogMessage("Selected records have been marked for processing !");
							}
//						}
					}
				});
			}else{
				form.showDialogMessage("No unprocesssed reecord found!");
			}
		}
		
		if (text.equals("Email Reminder")){
			List<RenewalResult>list=form.getAllProcessedSelectedRecords();
			if(list.size()==0){
				form.showDialogMessage("Please select processed record !");
			}else if(list.size()>1){
				form.showDialogMessage("Please select only one processed record !");
			}else{
				if(validateStatus(list)){
					setContractRenewal(list);
//					reactOnEmail();
					sendEmail();
				}
			}
			
		}
		
		/**
		 * This code is added by ajinkya jadhav for printing PCAMB covering letter
		 * Date : 10/2/2017
		 */
	
		if (text.equals("Print Covering Letter")){
			
			System.out.println("in side print covering letter  ");
			
			List<RenewalResult>list=form.getAllProcessedSelectedRecords();
			if(list.size()==0){
				form.showDialogMessage("Please select processed record !");
			}else if(list.size()>1){
				form.showDialogMessage("Please select only one processed record !");
			}else{
				setContractRenewal(list);
				reactOnPrintCoveringLetter();
			}
		}
		
		/**
		 * ends here 
		 */
		
		if (text.equals("Print Reminder")){
			List<RenewalResult>list=form.getAllProcessedSelectedRecords();
			if(list.size()==0){
				form.showDialogMessage("Please select processed record !");
			}else if(list.size()>1){
				form.showDialogMessage("Please select only one processed record !");
			}else{
				setContractRenewal(list);
				reactOnPrint();
			}
			
		}
		if (text.equals("Remind Me Later")){
			
			List<RenewalResult>list=form.getAllProcessedSelectedRecords();
			if(list.size()>0){
				if(validateStatus(list)){
					panel = new PopupPanel(true);
					panel.add(datePopup);
					datePopup.clear();
					panel.show();
					panel.center();
				}
			}else{
				form.showDialogMessage("Please select processed record !");
			}
			
			
		}
		if (text.equals("Renew Contract")){
			
			List<RenewalResult>list=form.getAllProcessedSelectedRecords();
			if(list.size()==0){
				form.showDialogMessage("Please select processed record !");
			}else if(list.size()>1){
				form.showDialogMessage("Please select only one processed record !");
			}else{
				if(validateStatus(list)){
//					setContractRenewalFrom(list);
					/**
					 * Date 20-03-2019 By Vijay 
					 * Des :- For NBHC CCPM for renew contract first Quotation must create then allow
					 * to renew contract with process config.
					 */
					if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("ContractRenewal", "EnableQuotationMandatoryForContractRenwal")){
						singledropdownpopup.showPopUp();
						singledropdownpopup.makequotaionlive(list.get(0).getCustomerId());
					}
					else{
						checkProcessedContractRenewal(list);
					}
					
				}
			}
			
			
		}
		if (text.equals("Do Not Renew")){
			List<RenewalResult>list=form.getAllProcessedSelectedRecords();
			if(list.size()>0){
				if(validateStatus(list)){/*
					final ArrayList<ContractRenewal> reminderList =getContractDoNotRenewList(list,AppConstants.DONOTRENEW);
					contractRenewalServ.setStatusDoNotRenew(reminderList, new AsyncCallback<ArrayList<ContractRenewal>>() {
						
						@Override
						public void onSuccess(ArrayList<ContractRenewal> result) {
							form.showDialogMessage("Data save Successfully!");
							
							final List<RenewalResult>selectedlist=form.getAllProcessedSelectedRecords();
							final List<RenewalResult>uncheckedlist=form.getAllProcessedUncheckedRecords();
							
							form.processedtable.connectToLocal();
							
							ArrayList<RenewalResult> renewallist=getRenewalListFrmContractRenewal(result);
							
							form.processedtable.getDataprovider().setList(uncheckedlist);
							
						}
						
						@Override
						public void onFailure(Throwable caught) {
							form.showDialogMessage("Unexpected Error!");
						}
					});
				*/ 
					/**
					 * nidhi
					 * 9-06-2018
					 * }
					 */

					renewalFlag = false;
					nonRenewalMsgPopup = new ContractNonRenewalPopup();
					nonRenewalMsgPopup.showPopUp();
					nonRenewalMsgPopup.getLblOk().addClickHandler(this);
					nonRenewalMsgPopup.getLblCancel().addClickHandler(this);
					
				
				
				}
				
			}else{
				form.showDialogMessage("Please select processed record !");
			}
		}
		/**
		 * @author Vijay Date 07-12-2020
		 * Des :- As per Nitin Sir instruction Never Renew is not required so i have commented the same
		 */
//		if (text.equals("Never Renew")){
//			List<RenewalResult>list=form.getAllProcessedSelectedRecords();
//			if(list.size()>0){
//				if(validateStatus(list)){/*
//					final ArrayList<ContractRenewal> reminderList =getContractDoNotRenewList(list,AppConstants.NEVERRENEW);
//					contractRenewalServ.setStatusNeverRenew(reminderList, new AsyncCallback<ArrayList<ContractRenewal>>() {
//						
//						@Override
//						public void onSuccess(ArrayList<ContractRenewal> result) {
//							form.showDialogMessage("Data save Successfully!");
//							final List<RenewalResult>selectedlist=form.getAllProcessedSelectedRecords();
//							final List<RenewalResult>uncheckedlist=form.getAllProcessedUncheckedRecords();
//							
//							form.processedtable.connectToLocal();
//							
//							ArrayList<RenewalResult> renewallist=getRenewalListFrmContractRenewal(result);
//							
//							form.processedtable.getDataprovider().setList(uncheckedlist);
//						}
//						
//						@Override
//						public void onFailure(Throwable caught) {
//							form.showDialogMessage("Unexpected Error!");
//						}
//					});
//				*/
//					/**
//					 * nidhi
//					 * 9-06-2018
//					 */
//					
//
//					renewalFlag = true;
//					nonRenewalMsgPopup = new ContractNonRenewalPopup();
//					nonRenewalMsgPopup.showPopUp();
//					nonRenewalMsgPopup.getLblOk().addClickHandler(this);
//					nonRenewalMsgPopup.getLblCancel().addClickHandler(this);
//					
//				
//				}
//					 
//			}else{
//				form.showDialogMessage("Please select processed record !");
//			}
//		}
		if (text.equals("Update Contract")){
			updateContractEntity();

		}
		if (text.equals("Update Quotation")){
			updateQuotationEntity();

		}
		if (text.equals("Update Service")){
			updateServiceEntity();

		}
		if (text.equals("Send SMS")){
			List<RenewalResult>list=form.getAllProcessedSelectedRecords();
			if(list.size()>0){
				if(validateStatus(list)){
					panel = new PopupPanel(true);
					panel.add(smsPopup);
					smsPopup.clear();
					panel.show();
					panel.center();
				}
			}else{
				form.showDialogMessage("Please select processed record !");
			}
			
		}
		
		if(text.equals("View Services")) {
			Console.log("View Services clicked");

			final List<RenewalResult> unprocessedSelectedList=form.getAllSelectedRecords();
			List<RenewalResult> processedSelectedlist=form.getAllProcessedSelectedRecords();
			int noofselectedRecord=0;
			if(processedSelectedlist.size()==0&&unprocessedSelectedList.size()==0){
				form.showDialogMessage("Please select a record !");
			}else if(processedSelectedlist.size()>1||unprocessedSelectedList.size()>1){
				form.showDialogMessage("Please select only one record !");
			}else if(processedSelectedlist.size()>0&&unprocessedSelectedList.size()>0){
				form.showDialogMessage("Please select only one record !");
			}else{
				
					
				MyQuerry querry = new MyQuerry();
				Vector<Filter> filtervec=new Vector<Filter>();
				Filter temp=null;
				Company comp=new Company();
				temp=new Filter();
				temp.setQuerryString("companyId");				
				temp.setLongValue(comp.getCompanyId());
				Console.log("companyId="+comp.getCompanyId());
				filtervec.add(temp);
				
				
				temp=new Filter();
				temp.setQuerryString("count");				
				if(processedSelectedlist.size()>0) {
					temp.setIntValue(processedSelectedlist.get(0).getContractId());
					Console.log("Contract id="+processedSelectedlist.get(0).getContractId());
				}else {
					temp.setIntValue(unprocessedSelectedList.get(0).getContractId());
					Console.log("Contract id="+unprocessedSelectedList.get(0).getContractId());
				}
				filtervec.add(temp);
				
				querry.setFilters(filtervec);
				querry.setQuerryObject(new Contract());
				
				
				service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {

					@Override
					public void onFailure(Throwable caught) {
						form.showDialogMessage("Failed to load contract");						
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						
						if(result!=null&&result.size()>0) {
							Contract c=(Contract)result.get(0);
							reactOnViewServices(c);
						}else
							form.showDialogMessage("failed to load contract in success");
					}


				});
				
			}
			
		
		}
		if (text.equals(AppConstants.markcontractasrenewed)){
			Console.log("markcontractasrenewed clicked");

			final List<RenewalResult> unprocessedSelectedList=form.getAllSelectedRecords();
			List<RenewalResult> processedSelectedlist=form.getAllProcessedSelectedRecords();
			int noofselectedRecord=0;
			if(processedSelectedlist.size()==0&&unprocessedSelectedList.size()==0){
				form.showDialogMessage("Please select a processed record !");
			}else if(unprocessedSelectedList.size()>0){
				form.showDialogMessage("Please select one processed record !");
			}else if(processedSelectedlist.size()>1){
				form.showDialogMessage("Please select only one processed record !");
			}else {
				if(processedSelectedlist.get(0).getStatus().equals("Renewed")) {
					form.showDialogMessage("This contract is already renewed!");
					return;
				}else {
					descriptionPopup.getPopup().getElement().addClassName("descriptionpopup");
					
					descriptionPopup.showPopUp();
				}
			}
		}
	}
	public void reactOnViewServices(Contract c) {
		genralInvoicePopup =  new GeneralViewDocumentPopup(false,true,true);
		genralInvoicePopup.btnClose.addClickHandler(this);
		genralInvoicePopup.navigateScreen("Customer Service List", c, null,true);
		Timer timer = new Timer() {
			@Override
			public void run() {
				applyHandlerOnProcessBarMenu();
			}
		};
		timer.schedule(3000);
	}
	public void applyHandlerOnProcessBarMenu() {
		ServiceForm view=(ServiceForm) genralInvoicePopup.getPresenter().getView();
		
		if (view.getProcessLevelBar() != null) {
			InlineLabel[] lbl = view.getProcessLevelBar().btnLabels;
			for (int k = 0; k < lbl.length; k++) {
				lbl[k].addClickHandler(this);
				Console.log("in for"+lbl[k]);
			}
		}
	}
	
		private void reactOnPrintCoveringLetter() {
			
			final String url = GWT.getModuleBaseURL() + "PcambCoveringLetterpdf" + "?ContractId="+ model.getContractId()+ "&"+"CompanyId="+ model.getCompanyId();
			Window.open(url, "test", "enabled");		
		}

	
		private void setContractRenewalFrom(final List<RenewalResult> list) {
			
			int contractId=list.get(0).getContractId();
			MyQuerry querry = new MyQuerry();
			Company c=new Company();
			Vector<Filter> filtervec=new Vector<Filter>();
			Filter temp=null;
			
			temp=new Filter();
			temp.setLongValue(c.getCompanyId());
			temp.setQuerryString("companyId");
			filtervec.add(temp);
			
			
			temp=new Filter();
			temp.setIntValue(contractId);
			temp.setQuerryString("count");
			filtervec.add(temp);
			
			querry.setFilters(filtervec);
			querry.setQuerryObject(new Contract());
			
			service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				 
	            public void onSuccess(ArrayList<SuperModel> result)
	            {
	            	System.out.println(" result size :: "+result.size());
	            	if(result.size()!=0)
	            	{
	            		final Contract entity=(Contract) result.get(0);
	            		entity.setTicketNumber(-1);
	            		entity.setComplaintFlag(false);
	            		/**
	            		 * Date :29/11/2017 By: Manisha 
	            		 * setting description from renwal popup to contract description
	            		 */
	            		entity.setDescription(list.get(0).getDescription());
	            		
	            		/**End**/
	            		if(list.get(0).getSalesPerson()!=null)
	            		entity.setEmployee(list.get(0).getSalesPerson());
	            		entity.setBranch(list.get(0).getBranch()); //added on 14-06-2024
	            		/**
	            		 * @author Anil , Date : 17-04-2020
	            		 * in case of large service contract we are storing scheduling list on different entity
	            		 * in that case we will get zero scheduling list size and system will not allow to renew the contract
	            		 */
	            		if(entity.getServiceScheduleList().size()==0){
	            			Console.log("SERVICE SCHEDULE LIST ");
	        				Console.log("COM_ID "+entity.getCompanyId()+" DOC_TYPE "+"Contract"+" DOC_ID "+entity.getCount());
	        				final ContractServiceAsync conSer=GWT.create(ContractService.class);
	        				
	        				conSer.getScheduleServiceSearchResult(entity.getCompanyId(), "Contract", entity.getCount(), new AsyncCallback<ArrayList<ServiceSchedule>>() {
	        					@Override
	        					public void onFailure(Throwable caught) {
	        						Console.log("SERVICE SCHEDULE FAILURE ");
	        					}
	        					@Override
	        					public void onSuccess(ArrayList<ServiceSchedule> result) {
//	        						view.setServiceScheduleList(result);
	        						Console.log("SERVICE SCHEDULE SUCCESS "+result.size());
	        						if(result.size()!=0){
	        							entity.setServiceScheduleList(result);
	        						}
	        						reactOnContractRenewal(entity,list.get(0).getItems());
	        					}
	        				});
	            		}else{
	            			reactOnContractRenewal(entity,list.get(0).getItems());
	            		}
	            		
	            		/**
	            		 * 
	            		 */
	            		
	            		reactOnContractRenewal(entity,list.get(0).getItems());
	            	}
	            }

	            public void onFailure(Throwable throwable)
	            {
	            	form.showDialogMessage("An unexpected error occurred!");
	            	throwable.printStackTrace();
	            }

	        });
		
		}

		private void checkProcessedContractRenewal(final List<RenewalResult> list){
			int contractId=list.get(0).getContractId();
			MyQuerry querry = new MyQuerry();
			Company c=new Company();
			Vector<Filter> filtervec=new Vector<Filter>();
			Filter temp=null;
			
			temp=new Filter();
			temp.setLongValue(c.getCompanyId());
			temp.setQuerryString("companyId");
			filtervec.add(temp);
			
			
			temp=new Filter();
			temp.setIntValue(contractId);
			temp.setQuerryString("refContractCount");
			filtervec.add(temp);
			
			querry.setFilters(filtervec);
			querry.setQuerryObject(new Contract());
			
			service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				 
	            public void onSuccess(ArrayList<SuperModel> result)
	            {
	            	System.out.println(" result size :: "+result.size());
	            	if(result.size()!=0)
	            	{
	            		Contract entity=(Contract) result.get(0);
	            		if(entity.getStatus().equals(Contract.CANCELLED)){
	            			System.out.println("Contract is CANCELLED");
	            			setContractRenewalFrom(list);
	            		}else{
	            			form.showDialogMessage("Contract is processed for renewal !");
	            		}
	            	}else{
	            		System.out.println("Contract is NEW");
	            		setContractRenewalFrom(list);
	            	}
	            }

	            public void onFailure(Throwable throwable)
	            {
	            	form.showDialogMessage("An unexpected error occurred!");
	            	throwable.printStackTrace();
	            }

	        });
		}
	            	
		

		private void setContractRenewal(List<RenewalResult> list){
		
			model=new ContractRenewal();
			
			model.setContractId(list.get(0).getContractId());
			model.setContractDate(list.get(0).getContractDate());
			model.setContractEndDate(list.get(0).getContractEndDate());
			model.setBranch(list.get(0).getBranch());
			model.setSalesPerson(list.get(0).getSalesPerson());
			
			model.setCustomerId(list.get(0).getCustomerId());
			model.setCustomerName(list.get(0).getCustomerName());
			model.setCustomerCell(list.get(0).getCustomerCell());
			
			model.setCity(list.get(0).getCity());
			model.setLocality(list.get(0).getLocality());
			model.setItems(list.get(0).getItems());
			model.setStatus(AppConstants.PROCESSED);
			model.setDate(list.get(0).getDate());
			model.setCompanyId(UserConfiguration.getCompanyId());
			
			/**date: 27/11/2017 By: Manisha
			 * Description :To set the duration value..!!
			 */
			model.setDuration(list.get(0).getDuration());
			model.setDescription(list.get(0).getDescription());
			
			/**end **/
	}
	
	private  ArrayList<ContractRenewal> getContractrenewalProcessList(List<RenewalResult> list){
		ArrayList<ContractRenewal> contractRenewalList=new ArrayList<ContractRenewal>();
		
		for(RenewalResult result:list){
			ContractRenewal con=new ContractRenewal();
			
			con.setContractId(result.getContractId());
			con.setContractDate(result.getContractDate());
			con.setContractEndDate(result.getContractEndDate());
			con.setBranch(result.getBranch());
			con.setSalesPerson(result.getSalesPerson());
			
			con.setCustomerId(result.getCustomerId());
			con.setCustomerName(result.getCustomerName());
			con.setCustomerCell(result.getCustomerCell());
			
			con.setCity(result.getCity());
			con.setLocality(result.getLocality());
			con.setItems(result.getItems());
			con.setStatus(AppConstants.PROCESSED);
			con.setDate(result.getDate());
			
			/**Manisha**/
			con.setDescription(result.getDescription());
			/**End**/
			// Date 11 April 2017 added by vijay for PCAMB contract amount and type
			con.setNetPayable(result.getNetPayable());
			if(result.getContractType()!=null)
			con.setContractType(result.getContractType());
			if(result.getContractCategory()!=null)
				con.setContractCategory(result.getContractCategory());//16-1-2024
			Console.log("category set ");
			contractRenewalList.add(con);
		}
		
		return contractRenewalList;
	}
	
	
	private  ArrayList<ContractRenewal> getContractRenewalReminderList(List<RenewalResult> list,Date reminderDate,String desc){
		ArrayList<ContractRenewal> contractRenewalList=new ArrayList<ContractRenewal>();
		
		for(RenewalResult result:list){
			ContractRenewal con=new ContractRenewal();
			
			con.setContractId(result.getContractId());
			con.setContractDate(result.getContractDate());
			con.setContractEndDate(reminderDate);
			con.setBranch(result.getBranch());
			con.setSalesPerson(result.getSalesPerson());
			
			con.setCustomerId(result.getCustomerId());
			con.setCustomerName(result.getCustomerName());
			con.setCustomerCell(result.getCustomerCell());
			
			con.setCity(result.getCity());
			con.setLocality(result.getLocality());
			con.setItems(result.getItems());
			
			con.setStatus(AppConstants.PROCESSED);
			con.setDate(result.getDate());
			con.setCompanyId(UserConfiguration.getCompanyId());
			if(desc!=null){
				con.setDescription(desc);
			}
			contractRenewalList.add(con);
		}
		
		return contractRenewalList;
	}
	
	
	private  ArrayList<ContractRenewal> getContractDoNotRenewList(List<RenewalResult> list,String status){
		ArrayList<ContractRenewal> contractRenewalList=new ArrayList<ContractRenewal>();
		
		for(RenewalResult result:list){
			ContractRenewal con=new ContractRenewal();
			
			con.setContractId(result.getContractId());
			con.setContractDate(result.getContractDate());
			con.setContractEndDate(result.getContractEndDate());
			con.setBranch(result.getBranch());
			con.setSalesPerson(result.getSalesPerson());
			
			con.setCustomerId(result.getCustomerId());
			con.setCustomerName(result.getCustomerName());
			con.setCustomerCell(result.getCustomerCell());
			
			con.setCity(result.getCity());
			con.setLocality(result.getLocality());
			con.setItems(result.getItems());
			
			con.setStatus(status);
			con.setDate(result.getDate());
			con.setCompanyId(UserConfiguration.getCompanyId());
			/**
			 * nidhi
			 * 9-06-2018
			 */
			con.setNonRenewalRemark((nonRenewalMsgPopup.getOlbNonRenewalRemark().getSelectedIndex()!=0) ? nonRenewalMsgPopup.getOlbNonRenewalRemark().getValue(nonRenewalMsgPopup.getOlbNonRenewalRemark().getSelectedIndex()).trim() :  "");
//(nonRenewalMsgPopup.getTbremark().getValue() !=null) ? nonRenewalMsgPopup.getTbremark().getValue().trim() :  ""
			contractRenewalList.add(con);
		}
		
		return contractRenewalList;
	}
	
	private ArrayList<RenewalResult> getRenewalListFrmContractRenewal(List<ContractRenewal> list){
		
		ArrayList<RenewalResult> list1=new ArrayList<RenewalResult>();
		for(int i=0;i<list.size();i++){
			RenewalResult model=new RenewalResult();
			
			model.setContractId(list.get(i).getContractId());
			model.setContractDate(list.get(i).getContractDate());
			model.setContractEndDate(list.get(i).getContractEndDate());
			model.setBranch(list.get(i).getBranch());
			model.setSalesPerson(list.get(i).getSalesPerson());
			
			model.setCustomerId(list.get(i).getCustomerId());
			model.setCustomerName(list.get(i).getCustomerName());
			model.setCustomerCell(list.get(i).getCustomerCell());
			
			model.setCity(list.get(i).getCity());
			model.setLocality(list.get(i).getLocality());
			model.setItems(list.get(i).getItems());
			model.setStatus(list.get(i).getStatus());
			model.setDate(list.get(i).getDate());
			model.setDescription(list.get(i).getDescription());
			
			list1.add(model);
		}
		
		return list1;
	
}
	
	private void updateContractEntity() {
		
		Vector<Filter> filtervec=new Vector<Filter>();
		
		MyQuerry querry = new MyQuerry();
		
		Filter filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(UserConfiguration.getCompanyId());
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Contract());
		form.showWaitSymbol();
		async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				if (result.size() != 0) {
					for(SuperModel model:result){
						Contract con=(Contract) model;
						con.setRenewProcessed(AppConstants.NO);
						con.setTicketNumber(-1);
						con.setComplaintFlag(false);
						con.setIsQuotation(false);
						
						async.save(con,new AsyncCallback<ReturnFromServer>() {
							@Override
							public void onFailure(Throwable caught) {
								form.hideWaitSymbol();
								form.showDialogMessage("An Unexpected error occurred!");
							}
							@Override
							public void onSuccess(ReturnFromServer result) {
								form.hideWaitSymbol();
								form.showDialogMessage("ENTITY UPDATED SUCCESSFULLY!");
							}
						});
					}
				}
			}

			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
			}
		});
		
	}

	private void updateQuotationEntity() {
		
		Vector<Filter> filtervec=new Vector<Filter>();
		
		MyQuerry querry = new MyQuerry();
		
		Filter filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(UserConfiguration.getCompanyId());
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Quotation());
		form.showWaitSymbol();
		async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				if (result.size() != 0) {
					for(SuperModel model:result){
						Quotation con=(Quotation) model;
						con.setTicketNumber(-1);
						con.setComplaintFlag(false);
						con.setIsQuotation(true);
						
						async.save(con,new AsyncCallback<ReturnFromServer>() {
							@Override
							public void onFailure(Throwable caught) {
								form.hideWaitSymbol();
								form.showDialogMessage("An Unexpected error occurred!");
							}
							@Override
							public void onSuccess(ReturnFromServer result) {
								form.hideWaitSymbol();
								form.showDialogMessage("ENTITY UPDATED SUCCESSFULLY!");
							}
						});
					}
				}
			}

			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
			}
		});
		
	}
	
	private void updateServiceEntity() {
		
		Vector<Filter> filtervec=new Vector<Filter>();
		
		MyQuerry querry = new MyQuerry();
		
		Filter filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(UserConfiguration.getCompanyId());
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Service());
		form.showWaitSymbol();
		async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				if (result.size() != 0) {
					for(SuperModel model:result){
						Service con=(Service) model;
						con.setTicketNumber(-1);
						
						async.save(con,new AsyncCallback<ReturnFromServer>() {
							@Override
							public void onFailure(Throwable caught) {
								form.hideWaitSymbol();
								form.showDialogMessage("An Unexpected error occurred!");
							}
							@Override
							public void onSuccess(ReturnFromServer result) {
								form.hideWaitSymbol();
								form.showDialogMessage("ENTITY UPDATED SUCCESSFULLY!");
							}
						});
					}
				}
			}

			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
			}
		});
		
	}
	
	
	private void updateContractRenewalEntity() {
		
		Vector<Filter> filtervec=new Vector<Filter>();
		
		MyQuerry querry = new MyQuerry();
		
		Filter filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(UserConfiguration.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("status");
		filter.setStringValue("Inprocessed");
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new ContractRenewal());
		form.showWaitSymbol();
		async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				form.hideWaitSymbol();
				if (result.size() != 0) {
					for(SuperModel model:result){
						ContractRenewal con=(ContractRenewal) model;
						con.setStatus(AppConstants.PROCESSED);
						
						async.save(con,new AsyncCallback<ReturnFromServer>() {
							@Override
							public void onFailure(Throwable caught) {
								form.hideWaitSymbol();
								form.showDialogMessage("An Unexpected error occurred!");
							}
							@Override
							public void onSuccess(ReturnFromServer result) {
								form.hideWaitSymbol();
								form.showDialogMessage("ENTITY UPDATED SUCCESSFULLY!");
							}
						});
					}
				}
			}

			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
			}
		});
		
	}



	@Override
	public void onClick(ClickEvent event) {
		super.onClick(event);
		System.out.println("INSIDE PRESENTER CLICK");
		if(event.getSource().equals(form.btnGo)){
			if(validateDate()){
				ArrayList<Filter> contractFilter ;
				ArrayList<Filter> contractRenewalFilter ;
				if(form.lbStatus.getValue(form.lbStatus.getSelectedIndex()).equals(AppConstants.PROCESSED)){
					contractFilter = new ArrayList<Filter>();
					contractRenewalFilter = getContractRenewalFilter();
				}
				else if(form.lbStatus.getValue(form.lbStatus.getSelectedIndex()).equals(AppConstants.UNPROCESSED)){
					contractFilter = getContractFilter();
					contractRenewalFilter = new ArrayList<Filter>();
				}else if(form.lbStatus.getValue(form.lbStatus.getSelectedIndex()).equals(AppConstants.RENEWED)){
					contractFilter = new ArrayList<Filter>();
					contractRenewalFilter = getContractRenewalFilter();					
				}else if(form.lbStatus.getValue(form.lbStatus.getSelectedIndex()).equals(AppConstants.DONOTRENEW)){
					contractFilter = new ArrayList<Filter>();
					contractRenewalFilter = getContractRenewalFilter();
				}else {
					contractFilter = getContractFilter();
					contractRenewalFilter = getContractRenewalFilter();
				}
				form.showWaitSymbol();
				contractRenewalServ.getRenewalStatus(contractFilter,contractRenewalFilter, new AsyncCallback<ArrayList<RenewalResult>>(){
					@Override
					public void onFailure(Throwable caught) {
						form.hideWaitSymbol();
						form.showDialogMessage("Unexpected Error!");
						form.table.connectToLocal();
						form.processedtable.connectToLocal();
					}
					@Override
					public void onSuccess(ArrayList<RenewalResult> result) {
						form.hideWaitSymbol();
						Console.log("RENWAL LIST SIZE : "+result.size());
						form.table.connectToLocal();
						form.processedtable.connectToLocal();
						if(result.size()!=0){
							List<RenewalResult> unprocessedList=new ArrayList<RenewalResult>();
							List<RenewalResult> processedList=new ArrayList<RenewalResult>();
							
							for(RenewalResult renres:result){
								if(renres.getRenewProcessed().equals(AppConstants.NO)){
									renres.setStatus("Unprocessed");//Ashwini Patil 19-10-2023 as ultra wants to see the same status as drop down fileters on contract renewal screen
									unprocessedList.add(renres);
									
								}else{
									processedList.add(renres);
									
								}
							}
							List<RenewalResult> UNPROCESSED=unprocessedList;
							List<RenewalResult> PROCESSED=processedList;
							
							Console.log("UNPROCESSED LIST SIZE : "+unprocessedList.size());
							Console.log("PROCESSED LIST SIZE : "+processedList.size());
							
							Console.log("unprocessedList SIZE : "+UNPROCESSED.size());
							Console.log("processedList SIZE : "+PROCESSED.size());
							form.table.getDataprovider().setList(UNPROCESSED);
							
							form.processedtable.getDataprovider().setList(PROCESSED);
						}
						else{
							form.showDialogMessage("No Result Found!");
						}
					} 
			
				});
			}
		}
		
		
		
		
		if (event.getSource() == datePopup.getBtnOne()) {
			if (datePopup.getServiceDate().getValue() != null) {
				Date serviceDate = datePopup.getServiceDate().getValue();
				System.out.println("Date :::::::: " + serviceDate);
				
				String desc=null;
				if(datePopup.getTaDesc().getValue()!=null){
					desc=datePopup.getTaDesc().getValue();
				}
				
				System.out.println("Description :: "+desc);
				
				List<RenewalResult>list=form.getAllProcessedSelectedRecords();
				reactToReminderDate(list,serviceDate,desc);
				panel.hide();
				
			} else {
				form.showDialogMessage("Please select date!");
			}
		}
		if (event.getSource() == datePopup.getBtnTwo()) {
			panel.hide();
		}
		
		
	
		if (event.getSource() == smsPopup.getBtnOne()) {
			if (!smsPopup.getTaDesc().getValue().equals("")) {
				System.out.println("INSIDE OK BUTTON CLICK...");
				String desc=smsPopup.getTaDesc().getValue();
				System.out.println("Description :: "+desc);
				
				reactToSMS(desc);
				panel.hide();
				
			} else {
				form.showDialogMessage("Please select SMS Template!");
			}
		}
		if (event.getSource() == smsPopup.getBtnTwo()) {
			System.out.println("INSIDE OK BUTTON CLICK...");
			panel.hide();
		}
		
		if(event.getSource().equals(form.btnDownload)){
			downloadBtnNo=1;
			if(form.table.getDataprovider().getList()==null|| form.table.getDataprovider().getList().size()==0) {
				form.showDialogMessage("No unprocessed records !");
				return;
			}
			
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "RestrictContractsDownloadWithOTP")){
				generatedOTP=(int) Math.floor(100000 + Math.random() * 900000);
				Console.log("generatedOTP="+generatedOTP);
				smsService.checkConfigAndSendContractDownloadOTPMessage(model.getCompanyId(), LoginPresenter.loggedInUser, generatedOTP, new  AsyncCallback<String>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						form.showDialogMessage("Failed");
					}

					@Override
					public void onSuccess(String result) {
						// TODO Auto-generated method stub
						Console.log("in success");
						if(result!=null&&result.equalsIgnoreCase("success")) {						
							otpPopup.clearOtp();
							otppopupPanel = new PopupPanel(true);
							otppopupPanel.add(otpPopup);
							otppopupPanel.show();
							otppopupPanel.center();
						}else {
							form.showDialogMessage(result);
						}
							
					}
				} );
			
			}else
				runRegularDownload1Program();
			
		}
		
		if(event.getSource().equals(form.btnDownload1)){
			downloadBtnNo=2;
			if(form.processedtable.getDataprovider().getList()==null||form.processedtable.getDataprovider().getList().size()==0) {
				form.showDialogMessage("No processed records !");
				return;
			}
						
			
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "RestrictContractsDownloadWithOTP")){
				generatedOTP=(int) Math.floor(100000 + Math.random() * 900000);
				Console.log("generatedOTP="+generatedOTP);
				smsService.checkConfigAndSendContractDownloadOTPMessage(model.getCompanyId(), LoginPresenter.loggedInUser, generatedOTP, new  AsyncCallback<String>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						form.showDialogMessage("Failed");
					}

					@Override
					public void onSuccess(String result) {
						// TODO Auto-generated method stub
						Console.log("in success");
						if(result!=null&&result.equalsIgnoreCase("success")) {						
							otpPopup.clearOtp();
							otppopupPanel = new PopupPanel(true);
							otppopupPanel.add(otpPopup);
							otppopupPanel.show();
							otppopupPanel.center();
						}else {
							form.showDialogMessage(result);
						}
							
					}
				} );
			
			}else
				runRegularDownload2Program();
			
		}
		
		
		
//    rohanadded code here for preprinted status  ********************
		if(event.getSource().equals(conditionPopup.getBtnOne()))
		{
			/**
			 * Nidhi added this code for NBHC CCPM Date : 17-07-2017
			 */
			if (AppUtility.checkForProcessConfigurartionIsActiveOrNot("ContractRenewal", "OnlyForNBHC")){
				final String url = GWT.getModuleBaseURL() + "pdcontractrenewal"	+ "?ContractId=" + model.getContractId() + "&"+ "CompanyId=" + model.getCompanyId() + "&"+ "preprint=" + "yes&flag=" + true;
				Window.open(url, "test", "enabled");
				panel.hide();
			} else {
				final String url = GWT.getModuleBaseURL() + "pdcontractrenewal"	+ "?ContractId=" + model.getContractId() + "&"+ "CompanyId=" + model.getCompanyId() + "&"+ "preprint=" + "yes&flag=" + false;
				Window.open(url, "test", "enabled");
				panel.hide();

			}
			
		}
			
		if(event.getSource().equals(conditionPopup.getBtnTwo()))
		{
			/**
			 * Nidhi added this code for NBHC CCPM Date : 17-07-2017
			 */
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("ContractRenewal","OnlyForNBHC")){
				final String url = GWT.getModuleBaseURL() + "pdcontractrenewal" + "?ContractId="+ model.getContractId()+ "&"+"CompanyId="+ model.getCompanyId()+ "&"+"preprint="+"no&flag="+true;
				Window.open(url, "test", "enabled");
				panel.hide();
			}else{
				final String url = GWT.getModuleBaseURL() + "pdcontractrenewal" + "?ContractId="+ model.getContractId()+ "&"+"CompanyId="+ model.getCompanyId()+ "&"+"preprint="+"no&flag="+false;
				Window.open(url, "test", "enabled");
				panel.hide();
				
			}
			
			
		}
		if(nonRenewalMsgPopup != null && event.getSource().equals(nonRenewalMsgPopup.getLblOk())){
			reactOnRenewalOk();
		}
		if(nonRenewalMsgPopup != null && event.getSource().equals(nonRenewalMsgPopup.getLblCancel())){
			nonRenewalMsgPopup.hidePopUp();
			nonRenewalMsgPopup = null;
		}
		
		/**
		 * Date 20-03-2019 by Vijay
		 * Des :- NBHC CCPM contract renewal quotation is mandatory
		 */
		if(event.getSource().equals(singledropdownpopup.getLblOk())){
			if(singledropdownpopup.getOlbQuotationId().getSelectedIndex()==0){
				form.showDialogMessage("Create valid quotation before you renew the contract!");
			}else{
				singledropdownpopup.hidePopUp();
				List<RenewalResult>list=form.getAllProcessedSelectedRecords();
				checkProcessedContractRenewal(list);
			}
			
		}
		if(event.getSource().equals(singledropdownpopup.getLblCancel())){
			singledropdownpopup.hidePopUp();
		}
		
		
		if(event.getSource().equals(conditionPopupThai.getBtnOne()))
		{
			
			if (AppUtility.checkForProcessConfigurartionIsActiveOrNot("ContractRenewal", "PC_PrintRenewalLetterInEnglishAndThai")){
				final String url = GWT.getModuleBaseURL() + "pdcontractrenewal"	+ "?ContractId=" + model.getContractId() + "&"+ "CompanyId=" + model.getCompanyId()+"&"+"preprint="+"English";
				Window.open(url, "test", "enabled");
				panelone.hide();
			} 
			
		}
			
		if(event.getSource().equals(conditionPopupThai.getBtnTwo()))
		{
			
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("ContractRenewal","PC_PrintRenewalLetterInEnglishAndThai")){
				final String url = GWT.getModuleBaseURL() + "pdcontractrenewal" + "?ContractId="+ model.getContractId()+ "&"+"CompanyId="+ model.getCompanyId()+"&"+"preprint="+"Thai";
				Window.open(url, "test", "enabled");
				panelone.hide();
			}
			
		}
		
		
		
		if(event.getSource() == CommunicationLogPopUp.getBtnOk()){
			form.showWaitSymbol();
		    List<InteractionType> list = CommunicationLogPopUp.getCommunicationLogTable().getDataprovider().getList();
		    ArrayList<InteractionType> interactionlist = new ArrayList<InteractionType>();
		    interactionlist.addAll(list);
		    boolean checkNewInteraction = AppUtility.checkNewInteractionAdded(interactionlist);
		    if(checkNewInteraction==false){
		    	form.showDialogMessage("Please add new interaction details");
		    	form.hideWaitSymbol();
		    }else{	
//		    	final Date followUpDate = interactionlist.get(interactionlist.size()-1).getInteractionDueDate();
		    	 communicationService.saveCommunicationLog(interactionlist, new AsyncCallback<Void>() {
					@Override
					public void onFailure(Throwable caught) {
						form.showDialogMessage("Unexpected Error");
						form.hideWaitSymbol();
						LoginPresenter.communicationLogPanel.hide();
					}

					@Override
					public void onSuccess(Void result) {
						form.showDialogMessage("Data Save Successfully");
						form.hideWaitSymbol();
//						model.setFollowUpDate(followUpDate);
//						form.getDbleaddate().setValue(followUpDate);
						LoginPresenter.communicationLogPanel.hide();
					}
				});
		    }
		}
		if(event.getSource() == CommunicationLogPopUp.getBtnCancel()){
			LoginPresenter.communicationLogPanel.hide();
		}
		if(event.getSource() == CommunicationLogPopUp.getBtnAdd()){
				form.showWaitSymbol();
				String remark = CommunicationLogPopUp.getRemark().getValue();
				Date dueDate = CommunicationLogPopUp.getDueDate().getValue();
				String interactiongGroup =null;
				if(CommunicationLogPopUp.getOblinteractionGroup().getSelectedIndex()!=0)
					interactiongGroup=CommunicationLogPopUp.getOblinteractionGroup().getValue(CommunicationLogPopUp.getOblinteractionGroup().getSelectedIndex());
				boolean validationFlag = AppUtility.validateCommunicationlog(remark,dueDate);
				
				List<RenewalResult>list=form.getAllProcessedSelectedRecords();
				if(list.size()==0){
					form.showDialogMessage("Please select processed record !");
				}else if(list.size()>1){
					form.showDialogMessage("Please select only one processed record !");
				}else{
					if(validationFlag){
						PersonInfo personinfo = new PersonInfo();
						personinfo.setCount(list.get(0).getCustomerId());
						personinfo.setFullName(list.get(0).getCustomerName());
						personinfo.setCellNumber(list.get(0).getCustomerCell());

						InteractionType communicationLog =  AppUtility.getCommunicationLog(AppConstants.SERVICEMODULE,AppConstants.CONTRACTRENEWALL,list.get(0).getContractId(),
								list.get(0).getSalesPerson(),remark,dueDate,personinfo,null,interactiongGroup, list.get(0).getBranch(),list.get(0).getStatus());
						CommunicationLogPopUp.getCommunicationLogTable().getDataprovider().getList().add(communicationLog);
						CommunicationLogPopUp.getRemark().setValue("");
						CommunicationLogPopUp.getDueDate().setValue(null);
						CommunicationLogPopUp.getOblinteractionGroup().setSelectedIndex(0);
					}
				}
				
				
				form.hideWaitSymbol();
		}
		
		if(event.getSource().equals(form.btnResetFilters)){
			ReactOnClearReset();
		}
		if(event.getSource() == otpPopup.getBtnOne()){
			Console.log("otp popup ok button clicked");
			Console.log("otp value="+otpPopup.getOtp().getValue()+" downloadBtnNo="+downloadBtnNo);
			if(otpPopup.getOtp().getValue()!=null){
				Console.log("in otp not null");
				if(generatedOTP==otpPopup.getOtp().getValue()){
					Console.log("in otp matched");
					if(downloadBtnNo==1)
						runRegularDownload1Program();
					else if(downloadBtnNo==2)
						runRegularDownload2Program();
					otppopupPanel.hide();
				}else {
					form.showDialogMessage("Invalid OTP");	
					smsService.checkConfigAndSendWrongOTPAttemptMessage(model.getCompanyId(), LoginPresenter.loggedInUser, generatedOTP, "Contract", new  AsyncCallback<String>() {

						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							form.showDialogMessage("Failed");
						}

						@Override
						public void onSuccess(String result) {
							// TODO Auto-generated method stub
							Console.log("InvalidOTPDownloadNotification result="+result);
						}
					} );
				
				}
			}else
				form.showDialogMessage("Enter OTP");
		}
		if(event.getSource() == otpPopup.getBtnTwo()){
			otppopupPanel.hide();
		}
		if(event.getSource()==genralInvoicePopup.btnClose){
			try{
			Console.log("On CLICK OF CLOSE...");
			if(genralInvoicePopup!=null){
				Console.log("in if(genralInvoicePopup!=null)");
				if(genralInvoicePopup.getPresenter()!=null){
					
					if(genralInvoicePopup.getPresenter().getModel()==null||genralInvoicePopup.getPresenter().getModel().getCount()==0){
						AppMemory.getAppMemory().currentScreen = Screen.CONTRACTRENEWAL;
						LoginPresenter.currentModule="Service";
						LoginPresenter.currentDocumentName="Contract Renewal";
						AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Contract Renewal", Screen.CONTRACTRENEWAL);						
						List<RenewalResult> unprocessedrecords=form.table.getDataprovider().getList();
						List<RenewalResult> processedrecords=form.processedtable.getDataprovider().getList();
						ContractRenewalForm form = new ContractRenewalForm();						
						form.table.getDataprovider().setList(unprocessedrecords);
						form.processedtable.getDataprovider().setList(processedrecords);
						ContractRenewalPresenter presenter = new ContractRenewalPresenter(form,new ContractRenewal());
						AppMemory.getAppMemory().stickPnel(form);
						form.setToViewState();
						form.toggleAppHeaderBarMenu();
						Console.log("end of on close in list form");
						return;
					}
					Console.log("presenter="+genralInvoicePopup.getPresenter().getModel().toString());
				
				}
			}
			
			AppMemory.getAppMemory().currentScreen = Screen.CONTRACTRENEWAL;
			Console.log("end of on close");
			}
			catch(Exception e){
				e.printStackTrace();
				Console.log(e.getMessage());
			}
		}
		if(event.getSource().equals(descriptionPopup.getLblOk())) {

			Console.log("ok clicked on description popup");
			String desc="";
			final List<RenewalResult> processedSelectedlist=form.getAllProcessedSelectedRecords();			
			final RenewalResult selectedrenewal=processedSelectedlist.get(0);
			
			
			
			MyQuerry querry = new MyQuerry();
			Vector<Filter> filtervec=new Vector<Filter>();
			Filter temp=null;
			final Company comp=new Company();
			temp=new Filter();
			temp.setQuerryString("companyId");				
			temp.setLongValue(comp.getCompanyId());
			filtervec.add(temp);
			
			
			temp=new Filter();
			temp.setQuerryString("contractId");	
			temp.setIntValue(processedSelectedlist.get(0).getContractId());
				
			filtervec.add(temp);
			
			querry.setFilters(filtervec);
			querry.setQuerryObject(new ContractRenewal());
			
			service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					if(result!=null&&result.size()>0) {
						ContractRenewal renewal=(ContractRenewal)result.get(0);
						if(descriptionPopup.gettaDescription().getText()!=null) {
							renewal.setDescription(descriptionPopup.gettaDescription().getText());
						}
						renewal.setStatus("Renewed");
						async.save(renewal,new AsyncCallback<ReturnFromServer>() {
							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("Failed to mark Contract as renewed");	
							}
							@Override
							public void onSuccess(ReturnFromServer result) {
								MyQuerry querry = new MyQuerry();
								Vector<Filter> filtervec=new Vector<Filter>();
								Filter temp=null;
								temp=new Filter();
								temp.setQuerryString("companyId");				
								temp.setLongValue(comp.getCompanyId());
								filtervec.add(temp);
								
								
								temp=new Filter();
								temp.setQuerryString("count");	
								temp.setIntValue(processedSelectedlist.get(0).getContractId());
									
								filtervec.add(temp);
								
								querry.setFilters(filtervec);
								querry.setQuerryObject(new Contract());
								
								service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
									
									@Override
									public void onSuccess(ArrayList<SuperModel> result) {
										if(result!=null&&result.size()>0) {
											Contract con=(Contract)result.get(0);
											con.setRenewFlag(true);
											if(descriptionPopup.gettaDescription().getText()!=null) {
												con.setUpdationLog(LoginPresenter.loggedInUser+ " marked this contract as renewed on "+new Date()+" using 'mark contract as renewed' option with description: "+ descriptionPopup.gettaDescription().getText());
											}else
												con.setUpdationLog(LoginPresenter.loggedInUser+ " marked this contract as renewed on "+new Date()+" using 'mark contract as renewed' option");
											
											async.save(con, new AsyncCallback<ReturnFromServer>() {
												
												@Override
												public void onSuccess(ReturnFromServer result) {
													form.showDialogMessage("Contract successfully marked as renewed");	
													
													List<RenewalResult> processedList=form.processedtable.getDataprovider().getList();
													Console.log("processedList size="+processedList.size());
													processedList.remove(selectedrenewal);
													Console.log("processedList size after removing selected object="+processedList.size());
													
													if(descriptionPopup.gettaDescription().getText()!=null) {
														selectedrenewal.setDescription(descriptionPopup.gettaDescription().getText());
													}
													selectedrenewal.setStatus("Renewed");
													processedList.add(selectedrenewal);
													Console.log("processedList size after adding selected object="+processedList.size());
													form.processedtable.clear();
													form.processedtable.getDataprovider().setList(processedList);
													Console.log("processedList set to table");
													
													
													
													
												}
												
												@Override
												public void onFailure(Throwable caught) {
													form.showDialogMessage("Failed to update contract");	
												}
											});
										}else
											form.showDialogMessage("Contract not found!");
									}
									
									@Override
									public void onFailure(Throwable caught) {
										form.showDialogMessage("Failed to load contract");	
										
									}
								});
								
								
							}
						
						});
					}else {
						form.showDialogMessage("Failed to update");
					}
				}
				
				@Override
				public void onFailure(Throwable caught) {
					form.showDialogMessage("Failed to update");					
				}
			});
			
			
			
			
			descriptionPopup.hidePopUp();
		
		}
		if(event.getSource().equals(descriptionPopup.getLblCancel()))
		{
			Console.log("cancel clicked on description popup");
			descriptionPopup.hidePopUp();
		}
	}

	private void ReactOnClearReset() {
		
		form.pic.clear();
		form.tbContractId.setValue("");
		form.olbBranch.setSelectedIndex(0);
		form.fromdate.setValue(null);
		form.todate.setValue(null);
		form.lbStatus.setSelectedIndex(0);
		
		//Ashwini Patil Date:21-12-2023 enabling all filters as they were getting disabled after applying contractid filter
		form.pic.setEnable(true); 
		form.olbBranch.setEnabled(true);
		form.fromdate.setEnabled(true);
		form.todate.setEnabled(true);
		form.lbStatus.setEnabled(true);
	}


public void reactOnRenewalOk(){
		
		if(nonRenewalMsgPopup.validate()){
			if(renewalFlag){
				renewalFlag = false;
				List<RenewalResult>list=form.getAllProcessedSelectedRecords();
				final ArrayList<ContractRenewal> reminderList =getContractDoNotRenewList(list,AppConstants.NEVERRENEW);
				contractRenewalServ.setStatusNeverRenew(reminderList, new AsyncCallback<ArrayList<ContractRenewal>>() {
					
					@Override
					public void onSuccess(ArrayList<ContractRenewal> result) {
						form.showDialogMessage("Data save Successfully!");
						final List<RenewalResult>selectedlist=form.getAllProcessedSelectedRecords();
						final List<RenewalResult>uncheckedlist=form.getAllProcessedUncheckedRecords();
						
						form.processedtable.connectToLocal();
						
						ArrayList<RenewalResult> renewallist=getRenewalListFrmContractRenewal(result);
						
						form.processedtable.getDataprovider().setList(uncheckedlist);
					}
					
					@Override
					public void onFailure(Throwable caught) {
						form.showDialogMessage("Unexpected Error!");
					}
				});
				
				nonRenewalMsgPopup.hidePopUp();
				nonRenewalMsgPopup = null;
			}else{
				List<RenewalResult>list=form.getAllProcessedSelectedRecords();
				final ArrayList<ContractRenewal> reminderList =getContractDoNotRenewList(list,AppConstants.DONOTRENEW);
				contractRenewalServ.setStatusDoNotRenew(reminderList, new AsyncCallback<ArrayList<ContractRenewal>>() {
					
					@Override
					public void onSuccess(ArrayList<ContractRenewal> result) {
						form.showDialogMessage("Data save Successfully!");
						
						final List<RenewalResult>selectedlist=form.getAllProcessedSelectedRecords();
						final List<RenewalResult>uncheckedlist=form.getAllProcessedUncheckedRecords();
						
						form.processedtable.connectToLocal();
						
						ArrayList<RenewalResult> renewallist=getRenewalListFrmContractRenewal(result);
						
						form.processedtable.getDataprovider().setList(uncheckedlist);
						
					}
					
					@Override
					public void onFailure(Throwable caught) {
						form.showDialogMessage("Unexpected Error!");
					}
				});
				nonRenewalMsgPopup.hidePopUp();
				nonRenewalMsgPopup = null;
			}

			
		}
	}
	
	private void reactToReminderDate(List<RenewalResult> list,Date serviceDate,String desc) {
		final ArrayList<ContractRenewal> reminderList =getContractRenewalReminderList(list,serviceDate,desc);
		Console.log("LoginPresenter.loggedInUser "+LoginPresenter.loggedInUser);
		contractRenewalServ.setReminder(reminderList,LoginPresenter.loggedInUser, new AsyncCallback<ArrayList<ContractRenewal>>() {
			@Override
			public void onSuccess(ArrayList<ContractRenewal> result) {
				final List<RenewalResult>selectedlist=form.getAllProcessedSelectedRecords();
				final List<RenewalResult>uncheckedlist=form.getAllProcessedUncheckedRecords();
//				form.table.connectToLocal();
				form.processedtable.connectToLocal();
				ArrayList<RenewalResult> renewallist=getRenewalListFrmContractRenewal(result);
				uncheckedlist.addAll(renewallist);
				form.processedtable.getDataprovider().setList(uncheckedlist);
				form.showDialogMessage("Reminder Date is set Successfully!");
			}
			@Override
			public void onFailure(Throwable caught) {
				form.showDialogMessage("Unexpected Error!");
			}
		});
	}


	public boolean validateDate(){
		/**
		 * Date : 24-10-2017  BY ANIL
		 * if branch restriction is on then we have to select branch selection is mandatory
		 */
		if (LoginPresenter.branchRestrictionFlag) {
			if (form.olbBranch.getSelectedIndex() == 0) {
				form.showDialogMessage("Please select branch !");
				return false;
			}
		}
		
		/**
		 * End
		 */
		Date formDate;
		Date toDate;
		if(form.pic.getValue()!=null || !form.tbContractId.getValue().equals("")){
			return true;
		}else{
			if(form.fromdate.getValue()!=null&&form.todate.getValue()!=null){
				formDate=form.fromdate.getValue();
				toDate=form.todate.getValue();
				
				int diffdays = AppUtility.getDifferenceDays(formDate, toDate);
				Console.log("diffdays "+diffdays);
				if(diffdays>31){
					form.showDialogMessage("Please select from date and to date range within 31 days");
					return false;
				}
				
				if(toDate.equals(formDate)||toDate.after(formDate)){
					return true;
				}else{
					form.showDialogMessage("To Date should be greater than From Date.");
					return false;
				}
				
			}
			else{
				form.showDialogMessage("Please select From Date and To Date!");
				return false;
			}
		}
		
	}
	
	
	
	public ArrayList<Filter>  getContractFilter()
	{
		System.out.println("INSIDE FILTERS ");
		ArrayList<Filter> filtervec=new ArrayList<Filter>();
		Filter temp=null;
		
		temp=new Filter();
		temp.setQuerryString("companyId");
		if(UserConfiguration.getCompanyId()!=null)
			temp.setLongValue(UserConfiguration.getCompanyId());
		filtervec.add(temp);
		
		
//		temp=new Filter();
//		temp.setStringValue(AppConstants.NO);
//		temp.setQuerryString("renewProcessed");
//		filtervec.add(temp);

		if(!form.pic.getId().getValue().equals(""))
		{  
			temp=new Filter();
			temp.setIntValue(Integer.parseInt(form.pic.getId().getValue()));
			temp.setQuerryString("cinfo.count");
			filtervec.add(temp);
		}

//		if(!(form.pic.getName().getValue().equals("")))
//		{
//			temp=new Filter();
//			temp.setStringValue(form.pic.getName().getValue());
//			temp.setQuerryString("cinfo.fullName");
//			filtervec.add(temp);
//		}
		
//		if(!form.pic.getPhone().getValue().equals(""))
//		{
//			temp=new Filter();
//			temp.setLongValue(Long.parseLong(form.pic.getPhone().getValue()));
//			temp.setQuerryString("cinfo.cellNumber");
//			filtervec.add(temp);
//		}
		
		if(form.olbBranch.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(form.olbBranch.getValue().trim());
			temp.setQuerryString("branch");
			filtervec.add(temp);
		}
//		if(form.olbeSalesPerson.getSelectedIndex()!=0)
//		{
//			temp=new Filter();
//			temp.setStringValue(form.olbeSalesPerson.getValue().trim());
//			temp.setQuerryString("employee");
//			filtervec.add(temp);
//		}
		
		if(form.fromdate.getValue()!=null){
			temp = new Filter();
			temp.setQuerryString("endDate >=");
			temp.setDateValue(form.fromdate.getValue());
			filtervec.add(temp);
		}
			
		if(form.todate.getValue()!=null){
			temp = new Filter();
			temp.setQuerryString("endDate <=");
			temp.setDateValue(form.todate.getValue());
			filtervec.add(temp);
		}
		
		if(!form.tbContractId.getValue().equals("")){
			temp=new Filter();
			temp.setIntValue(Integer.parseInt(form.tbContractId.getValue().trim()));
			temp.setQuerryString("count");
			filtervec.add(temp);
		}
		
		if(form.lbStatus.getSelectedIndex()!=0)
		{
			if(form.lbStatus.getValue(form.lbStatus.getSelectedIndex()).equals(AppConstants.UNPROCESSED)){
				temp=new Filter();
				temp.setStringValue(Contract.APPROVED);
				temp.setQuerryString("status");
				filtervec.add(temp);
			}
//			else{
//				temp=new Filter();
//				temp.setStringValue(form.lbStatus.getValue(form.lbStatus.getSelectedIndex()).trim());
//				temp.setQuerryString("status");
//				filtervec.add(temp);
//			}
		}
		
//		temp=new Filter();
//		temp.setBooleanvalue(false);
//		temp.setQuerryString("customerInterestFlag");
//		filtervec.add(temp);

		/**
		 * @author Vijay Date :- 28-07-2022
		 * Des :- added to show one time contract in unsuccessful
		 */
		if(form.lbStatus.getSelectedIndex()!=0 && (form.lbStatus.getValue(form.lbStatus.getSelectedIndex()).equals(AppConstants.UNSUCCESSFUL) || 
				form.lbStatus.getValue(form.lbStatus.getSelectedIndex()).equals(AppConstants.Unsucessful) || 
				form.lbStatus.getValue(form.lbStatus.getSelectedIndex()).equals("Unsucessful")))
		{
			
			temp=new Filter();
			temp.setBooleanvalue(true);
			temp.setQuerryString("customerInterestFlag");
			filtervec.add(temp);
			
			temp=new Filter();
			temp.setStringValue(Contract.APPROVED);
			temp.setQuerryString("status");
			filtervec.add(temp);
		}
		else {
			temp=new Filter();
			temp.setBooleanvalue(false);
			temp.setQuerryString("customerInterestFlag");
			filtervec.add(temp);
			
			if(form.lbStatus.getSelectedIndex()==0)
			{
				temp=new Filter();
				temp.setStringValue(Contract.APPROVED);
				temp.setQuerryString("status");
				filtervec.add(temp);
			}
		}
		
		return filtervec;
	}
	
	public ArrayList<Filter>  getContractRenewalFilter()
	{
		System.out.println("INSIDE FILTERS ");
		ArrayList<Filter> filtervec=new ArrayList<Filter>();
		Filter temp=null;
		
		temp=new Filter();
		temp.setQuerryString("companyId");
		if(UserConfiguration.getCompanyId()!=null)
			temp.setLongValue(UserConfiguration.getCompanyId());
		filtervec.add(temp);
		

		if(!form.pic.getId().getValue().equals(""))
		{  
			temp=new Filter();
			temp.setIntValue(Integer.parseInt(form.pic.getId().getValue()));
			temp.setQuerryString("customerId");
			filtervec.add(temp);
		}

//		if(!(form.pic.getName().getValue().equals("")))
//		{
//			temp=new Filter();
//			temp.setStringValue(form.pic.getName().getValue());
//			temp.setQuerryString("customerName");
//			filtervec.add(temp);
//		}
		
//		if(!form.pic.getPhone().getValue().equals(""))
//		{
//			temp=new Filter();
//			temp.setLongValue(Long.parseLong(form.pic.getPhone().getValue()));
//			temp.setQuerryString("customerCell");
//			filtervec.add(temp);
//		}
		
		if(form.olbBranch.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(form.olbBranch.getValue().trim());
			temp.setQuerryString("branch");
			filtervec.add(temp);
		}
//		if(form.olbeSalesPerson.getSelectedIndex()!=0)
//		{
//			temp=new Filter();
//			temp.setStringValue(form.olbeSalesPerson.getValue().trim());
//			temp.setQuerryString("salesPerson");
//			filtervec.add(temp);
//		}
		
		if(form.fromdate.getValue()!=null){
			temp = new Filter();
			temp.setQuerryString("contractEndDate >=");
			temp.setDateValue(form.fromdate.getValue());
			filtervec.add(temp);
		}
			
		if(form.todate.getValue()!=null){
			temp = new Filter();
			temp.setQuerryString("contractEndDate <=");
			temp.setDateValue(form.todate.getValue());
			filtervec.add(temp);
		}
		
		if(!form.tbContractId.getValue().equals("")){
			temp=new Filter();
			temp.setIntValue(Integer.parseInt(form.tbContractId.getValue().trim()));
			temp.setQuerryString("contractId");
			filtervec.add(temp);
		}
		
		if(form.lbStatus.getSelectedIndex()!=0)
		{
			
			if((form.lbStatus.getValue(form.lbStatus.getSelectedIndex()).equals(AppConstants.Successful) || 
					form.lbStatus.getValue(form.lbStatus.getSelectedIndex()).equals("Sucessfull"))||(form.lbStatus.getValue(form.lbStatus.getSelectedIndex()).equals(AppConstants.RENEWED) || 
							form.lbStatus.getValue(form.lbStatus.getSelectedIndex()).equals(AppConstants.RENEWED)))
			{
				temp=new Filter();
				temp.setStringValue(AppConstants.RENEWED);
				temp.setQuerryString("status");
				filtervec.add(temp);
			}
			else if((form.lbStatus.getValue(form.lbStatus.getSelectedIndex()).equals(AppConstants.DONOTRENEW) || 
							form.lbStatus.getValue(form.lbStatus.getSelectedIndex()).equals(AppConstants.DONOTRENEW)))
			{
				temp=new Filter();
				temp.setStringValue(AppConstants.DONOTRENEW);
				temp.setQuerryString("status");
				filtervec.add(temp);
			}
			else{
				temp=new Filter();
				temp.setStringValue(form.lbStatus.getValue(form.lbStatus.getSelectedIndex()).trim());
				temp.setQuerryString("status");
				filtervec.add(temp);
			}
			
		}
//		else{
//			temp=new Filter();
//			temp.setStringValue(AppConstants.PROCESSED);
//			temp.setQuerryString("status");
//			filtervec.add(temp);
//		}

		return filtervec;
	}



	@Override
	public void reactOnPrint() {
		
//		System.out.println("in side react on print");
//		
//		MyQuerry querry = new MyQuerry();
//	  	Company c = new Company();
//	  	Vector<Filter> filtervec=new Vector<Filter>();
//	  	Filter filter = null;
//	  	filter = new Filter();
//	  	filter.setQuerryString("companyId");
//		filter.setLongValue(c.getCompanyId());
//		filtervec.add(filter);
//		filter = new Filter();
//		filter.setQuerryString("processName");
//		filter.setStringValue("ContractRenewal");
//		filtervec.add(filter);
//		
//		filter = new Filter();
//		filter.setQuerryString("processList.status");
//		filter.setBooleanvalue(true);
//		filtervec.add(filter);
//		
//		querry.setFilters(filtervec);
//		querry.setQuerryObject(new ProcessConfiguration());
//		
//		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
//			
//			@Override
//			public void onFailure(Throwable caught) {
//			}			
//
//			@Override
//			public void onSuccess(ArrayList<SuperModel> result) {
//				System.out.println(" result set size +++++++"+result.size());
//				
//				List<ProcessTypeDetails> processList =new ArrayList<ProcessTypeDetails>();
//				
//				int pestoIndia=0;
//				
//			
//				if(result.size()==0){
//					
//					final String url = GWT.getModuleBaseURL() + "pdcontractrenewal" + "?ContractId="+ model.getContractId()+ "&"+"CompanyId="+ model.getCompanyId()+"&"+"preprint="+"plane";
//					Window.open(url, "test", "enabled");
//				}
//				else{
//				
//					for(SuperModel model:result)
//					{
//						ProcessConfiguration processConfig=(ProcessConfiguration)model;
//						processList.addAll(processConfig.getProcessList());
//						
//					}
//				
//				for(int k=0;k<processList.size();k++){	
//						
//				
//				if(processList.get(k).getProcessType().trim().equalsIgnoreCase("PestoIndiaQuotations")&&processList.get(k).isStatus()==true){
//					
//					pestoIndia=pestoIndia+1;
//				
//				}
//				
//				if(processList.get(k).getProcessType().trim().equalsIgnoreCase("CompanyAsLetterHead")&&processList.get(k).isStatus()==true){
//					
//					cnt=cnt+1;
//				
//				}
//				
//				if(processList.get(k).getProcessType().trim().equalsIgnoreCase("PrintCustomQuotation")&&processList.get(k).isStatus()==true){
//					
//					customQuotation=customQuotation+1;
//				
//				}
//				
//				if(processList.get(k).getProcessType().trim().equalsIgnoreCase("PCAMBCustomization")&&processList.get(k).isStatus()==true){
//					System.out.println("in side conditions");
//					  final String url = GWT.getModuleBaseURL() + "PcambContractRenewalLetterpdf"+"?ContractId="+ model.getContractId()+ "&"+"CompanyId="+ model.getCompanyId();
//						Window.open(url, "test", "enabled");
//						return;
//				}
//				
//				
//				}
//				
//				
//			    if(pestoIndia>0){
//					
//			    	final String url = GWT.getModuleBaseURL() + "pdfrenewal"+"?ContractId="+ model.getContractId()+ "&"+"CompanyId="+ model.getCompanyId();
//					Window.open(url, "test", "enabled");
//			    	
//				}
//			    else if (cnt > 0){
//			    	
//			    	System.out.println("in side react on prinnt pestQuotation");
//					panel=new PopupPanel(true);
//					panel.add(conditionPopup);
//					panel.setGlassEnabled(true);
//					panel.show();
//					panel.center();
//			    }
//			    
//			    else if (customQuotation > 0){
//			    	
//			    	System.out.println("in side react on prinnt pestQuotation");
//					panel=new PopupPanel(true);
//					panel.add(conditionPopup);
//					panel.setGlassEnabled(true);
//					panel.show();
//					panel.center();
//			    	
//			    }
//				else
//				{
//					
//					final String url = GWT.getModuleBaseURL() + "pdcontractrenewal" + "?ContractId="+ model.getContractId()+ "&"+"CompanyId="+ model.getCompanyId()+"&"+"preprint="+"plane";
//					Window.open(url, "test", "enabled");
//					
//				}
//			}
//			}
//		});
//		//**************************changes ends here ********************************
		
		/**
		 * Nidhi added this code for NBHC CCPM Date : 17-07-2017
		 */
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("ContractRenewal", "CompanyAsLetterHead")){
			
			cnt=cnt+1;
			System.out.println("in side react on prinnt pestQuotation");
			panel=new PopupPanel(true);
			panel.add(conditionPopup);
			panel.setGlassEnabled(true);
			panel.show();
			panel.center();
			
		}else if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("ContractRenewal","PC_PrintRenewalLetterInEnglishAndThai")){
			cntone=cntone+1;  //panelone  cntone
			System.out.println("in side react on prinnt pestQuotation");
			panelone=new PopupPanel(true);
			panelone.add(conditionPopupThai);
			panelone.setGlassEnabled(true);
			panelone.show();
			panelone.center();
				
		}else if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("ContractRenewal","PCAMBCustomization")){
			final String url = GWT.getModuleBaseURL()
					+ "PcambContractRenewalLetterpdf" + "?ContractId="
					+ model.getContractId() + "&" + "CompanyId="
					+ model.getCompanyId();
			Window.open(url, "test", "enabled");
			return;
		}else if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("ContractRenewal","OnlyForNBHC")){
			final String url = GWT.getModuleBaseURL() + "pdcontractrenewal" + "?ContractId="+ model.getContractId()+ "&"+"CompanyId="+ model.getCompanyId()+"&preprint="+"plane"+"&flag="+true;
			Window.open(url, "test", "enabled");
		}else{
			final String url = GWT.getModuleBaseURL() + "pdcontractrenewal" + "?ContractId="+ model.getContractId()+ "&CompanyId="+ model.getCompanyId()+"&"+"preprint="+"plane&flag="+false;
			Window.open(url, "test", "enabled");
		}
		
		
	}

	@Override
	public void reactOnEmail() {
//		boolean conf = Window.confirm("Do you really want to send email?");
//		if (conf == true) {
//			emailService.initiateContractRenewalEmail( model,new AsyncCallback<Void>() {
//				@Override
//				public void onFailure(Throwable caught) {
//					Window.alert("Resource Quota Ended ");
//					caught.printStackTrace();
//				}
//
//				@Override
//				public void onSuccess(Void result) {
//					Window.alert("Email Sent Sucessfully !");
//				}
//			});
//		}
		
		List<RenewalResult>list=form.getAllProcessedSelectedRecords();
		if(list.size()==0){
			form.showDialogMessage("Please select processed record !");
		}else if(list.size()>1){
			form.showDialogMessage("Please select only one processed record !");
		}else{
			
			/**
			 * @author Vijay Date 19-03-2021
			 * Des :- above old code commented 
			 * Added New Email Popup Functionality
			 */
			emailpopup.showPopUp();
			setEmailPopUpData();
			
			/**
			 * ends here
			 */
		}
	}

	@Override
	protected void makeNewModel() {
	}
	
	
	
	
	
	private void reactOnContractRenewal(final Contract model,final List <SalesLineItem> newpriceprodList) {
		

		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Contract",Screen.CONTRACT);
		final ContractForm form=ContractPresenter.initalize();
		
		form.showWaitSymbol();
		Timer t = new Timer() {
		      @Override
		      public void run() {
				  String response=AppUtility.validateProductCode(model.getItems());
					 
		    	  form.setToNewState();
		    	  form.hideWaitSymbol();
		    	  
		    	  /**
		  		 * Date : 13-05-2017 By ANIl
		  		 */
		  		form.checkCustomerBranch(model.getCinfo().getCount());
		  		/**
		  		 * End
		  		 */
		  		/**
		  		 * Date : 19-07-2017 By ANIl
		  		 */
		  		form.checkCustomerStatus(model.getCinfo().getCount(),true,true);
		  		/**
		  		 * End
		  		 */
		    	  form.getTbContractId().setText("");
		    	  /**
		    	   * nidhi
		    	   * 16-01-2018
		    	   * unchecked all check box by default 
		    	   */
		    	  form.getCustomerContactList(model.getCount(),null);
		    	  form.getCbRenewFlag().setValue(false);
		    	  form.getCbServiceWiseBilling().setValue(false);
		    	  form.getChkbillingatBranch().setValue(false);
		    	  form.getChkservicewithBilling().setValue(false);
		    	  form.getCbConsolidatePrice().setValue(false);
		    	  /***
		    	   *  end
		    	   */
		    	  if(model.getLeadCount()==-1){
		    		  form.getTbLeadId().setText("");
		    	  }
		    	  form.getPersonInfoComposite().setEnabled(false);
		    	  if(model.getRefNo()!=null){
		    		  form.getTbReferenceNumber().setValue(model.getRefNo());
		    	  }else{
		    		  form.getTbReferenceNumber().setText("");
		    	  }
		    	  
		    	  if(model.getReferedBy()!=""){
		    		  form.getTbRefeRRedBy().setValue(model.getReferedBy());
		    	  }else{
		    		  form.getTbRefeRRedBy().setText("");
		    	  }
		    	  
		    	  if(model.getRefDate()!=null){
		    		  form.getDbrefernceDate().setValue(model.getRefDate());
		    	  }else{
		    		  form.getDbrefernceDate().setValue(null);
		    	  }
		    	  
		    	  
		    	  form.getPersonInfoComposite().setValue(model.getCinfo());
		    	  form.getOlbbBranch().setValue(model.getBranch());
		    	  Branch branchEntity =form.getOlbbBranch().getSelectedItem();
		    	  if(branchEntity.getNumberRange()!=null&&!branchEntity.getNumberRange().equals("")) {
						form.olbcNumberRange.setValue(branchEntity.getNumberRange());
						
		    	  }else {
		    		  	String range=LoginPresenter.branchWiseNumberRangeMap.get(model.getBranch());
				  		if(range!=null&&!range.equals("")) {
							form.olbcNumberRange.setValue(range);							
						}else {
							form.olbcNumberRange.setSelectedIndex(0);
						}					
		    	  }
		    	  if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("NumberRange", "MakeNumberRangeReadOnly"))
		    	  {	form.olbcNumberRange.setEnabled(false);
			    	 
		    	  }	
		    	  form.getOlbApproverName().setValue(model.getApproverName());
		    	  /**
		    	   * @author Vijay Date :- 27-10-2020
		    	   * Des  Bug :- when contract renew it should not map lead id in new renewed contract
		    	   */
//		    	  if(model.getLeadCount()!=-1){
//		    		  form.getTbLeadId().setValue(model.getLeadCount()+"");
//		    	  }
		    	  /**
					 * Commented old code
					 * By Anil Date : 13-05-2017
					 */
//		    	  if(model.getContractCount()!=-1){
//		    		  form.getTbContractId().setValue(model.getContractCount()+"");
//		    	  }
		    	  form.getIbOldContract().setValue(model.getCount()+"");
		    	  if(model.getQuotationCount()!=-1){
		    		  form.getTbQuotatinId().setValue(model.getQuotationCount()+"");
		    	  }
		    	  form.getOlbcPaymentMethods().setValue(model.getPaymentMethod());
		    	  form.getOlbeSalesPerson().setValue(model.getEmployee());
		    	  form.getOlbContractGroup().setValue(model.getGroup());
		    	  form.getOlbContractCategory().setValue(model.getCategory());
		    	  form.getOlbContractType().setValue(model.getType());
		    	  
		    	  if(response.equals("")) {
		    	  form.getProdTaxTable().setValue(model.getProductTaxes());
		    	  form.getChargesTable().setValue(model.getProductCharges());
		    	  }
//		    	  form.getPaymentTermsTable().setValue(model.getPaymentTermsList());        		Commented by:Viraj Date:18-01-2019 Description:To not map payment terms
		    	  
		    	  /**
					 * Date:27/11/2017 BY MANISHA
					 * Description :To Map the description after renewing contract...!!!!!
					 */
		    	  form.getTaDescription().setValue(model.getDescription());
		    	  /**end**/
		    	  
		    	  /**
		    	   * rohan make this as false to remove check for rate contract and branch wise billing 
		    	   */
		    	  
		    	  form.getChkbillingatBranch().setValue(false);
		    	  form.getChkservicewithBilling().setValue(false);
		    	  /**
		    	   * ends here 
		    	   */
		    	  
		    	  ArrayList <SalesLineItem> arraylis= new ArrayList<SalesLineItem>();
		    	  List<SalesLineItem> listProduct= new ArrayList<SalesLineItem>();
		    	  
		    	  for(int i=0;i<newpriceprodList.size();i++){
		    		  newpriceprodList.get(i).setOldProductPrice(0);
		    	  }
		    	  
		    	  listProduct.addAll(newpriceprodList);
		    	  
		    	  /**
		    	   * nidhi
		    	   * 16-01-2018
		    	   * for save value as per all contract 
		    	   */
		    	  form.getCbRenewFlag().setValue(model.isRenewFlag());
		    	  form.getCbServiceWiseBilling().setValue(model.isServiceWiseBilling());
		    	  form.getChkbillingatBranch().setValue(model.isBranchWiseBilling());
		    	  form.getCbStartOfPeriod().setValue(false);
		    	  
		    	  /***
		    	   *  end
		    	   */
		    	  System.out.println("listproduct size==="+listProduct.size());
		    	 
		    	  for(int i=0;i<listProduct.size();i++)
		    	  {
		    		 SalesLineItem sales= new SalesLineItem();
		    		  
		    		sales.setPrduct(listProduct.get(i).getPrduct());
		    		
		    		
		    		//Ashwini Patil Date:25-12-2023 when we click on renew contract, product startdate exceeds by 1 or 2 year
//		    		CalendarUtil.addDaysToDate(listProduct.get(i).getStartDate(),listProduct.get(i).getDuration()+1);
		    		/**
					 * Date : 09-06-2017 By ANIL
					 * PCAMB RENEWAL ERROR FOR NO. OF BRANCH ERROR
					 */
					//  rohan added this code for sessting service sr no while renew contract
					if(listProduct.get(i).getProductSrNo()==0){
						sales.setProductSrNo(i+1);
					}else{
						sales.setProductSrNo(listProduct.get(i).getProductSrNo());
					}
					
					/**
					 * End
					 */
					
					/**
					 * @author Anil
					 * @since 10-06-2020
					 * contract data get corrupt at the time of renwal becuase of product master document upload
					 */
					sales.setTermsAndConditions(new DocumentUpload());
					sales.setProductImage(new DocumentUpload());
					sales.getPrduct().setProductImage1(new DocumentUpload());
					sales.getPrduct().setProductImage2(new DocumentUpload());
					sales.getPrduct().setProductImage3(new DocumentUpload());
					sales.getPrduct().setComment("");
					sales.getPrduct().setCommentdesc("");
					sales.getPrduct().setCommentdesc1("");
					sales.getPrduct().setCommentdesc2("");
					
		    		sales.setStartDate(listProduct.get(i).getStartDate()); 
		    		sales.setQuantity(listProduct.get(i).getQty());
		    		sales.setDuration(listProduct.get(i).getDuration());
		    		sales.setNumberOfService(listProduct.get(i).getNumberOfServices());
		    		sales.setPrice(listProduct.get(i).getPrice());
		    		sales.setVatTax(listProduct.get(i).getVatTax());
		    		sales.setServiceTax(listProduct.get(i).getServiceTax());
		    		sales.setPercentageDiscount(listProduct.get(i).getPercentageDiscount());
		    		/**
					 * Date:27/11/2017 BY MANISHA
					 * Description :To Map the product attributes with contract products ...!!!!!
					 */
		    		
		    		sales.setPremisesDetails(listProduct.get(i).getPremisesDetails());
		    		/**
					 * Date:16-06-2017 BY ANIL
					 */
					sales.setRemark(listProduct.get(i).getRemark());
					/**
					 * End
					 */
					
		    		/**
					 * Date : 13-05-2017 By ANIL
					 */
					sales.setBranchSchedulingInfo(listProduct.get(i).getBranchSchedulingInfo());
					sales.setBranchSchedulingInfo1(listProduct.get(i).getBranchSchedulingInfo1());
					sales.setBranchSchedulingInfo2(listProduct.get(i).getBranchSchedulingInfo2());
					sales.setBranchSchedulingInfo3(listProduct.get(i).getBranchSchedulingInfo3());
					sales.setBranchSchedulingInfo4(listProduct.get(i).getBranchSchedulingInfo4());
					/**
					 * End
					 */
		    		
					/**
					 * Date 02-05-2018 
					 * Developer : Vijay
					 * Des :- Below code is customer branches stored in string so it can not load more than 120 branches
					 * so i have updated below code using hashmap. So old contract i will get the branches from string and new contract from hashmpa if branches exist 
					 */
					if(listProduct.get(i).getCustomerBranchSchedulingInfo()!=null){
						sales.setCustomerBranchSchedulingInfo(listProduct.get(i).getCustomerBranchSchedulingInfo());
					}else{

						
						/**
						 * Date 04-05-2018 By vijay
						 * if condition for old contract renewal and else block for new updated new contract renewal
						 */
						
						String[] branchArray=new String[10];
						if(listProduct.get(i).getBranchSchedulingInfo()!=null
								&&!listProduct.get(i).getBranchSchedulingInfo().equals("")){
							branchArray[0]=listProduct.get(i).getBranchSchedulingInfo();
						}
						if(listProduct.get(i).getBranchSchedulingInfo1()!=null
								&&!listProduct.get(i).getBranchSchedulingInfo1().equals("")){
							branchArray[1]=listProduct.get(i).getBranchSchedulingInfo1();
						}
						if(listProduct.get(i).getBranchSchedulingInfo2()!=null
								&&!listProduct.get(i).getBranchSchedulingInfo2().equals("")){
							branchArray[2]=listProduct.get(i).getBranchSchedulingInfo2();
						}
						if(listProduct.get(i).getBranchSchedulingInfo3()!=null
								&&!listProduct.get(i).getBranchSchedulingInfo3().equals("")){
							branchArray[3]=listProduct.get(i).getBranchSchedulingInfo3();
						}
						if(listProduct.get(i).getBranchSchedulingInfo4()!=null
								&&!listProduct.get(i).getBranchSchedulingInfo4().equals("")){
							branchArray[4]=listProduct.get(i).getBranchSchedulingInfo4();
						}
						
						if(listProduct.get(i).getBranchSchedulingInfo5()!=null
								&&!listProduct.get(i).getBranchSchedulingInfo5().equals("")){
							branchArray[5]=listProduct.get(i).getBranchSchedulingInfo5();
						}
						if(listProduct.get(i).getBranchSchedulingInfo6()!=null
								&&!listProduct.get(i).getBranchSchedulingInfo6().equals("")){
							branchArray[6]=listProduct.get(i).getBranchSchedulingInfo6();
						}
						if(listProduct.get(i).getBranchSchedulingInfo7()!=null
								&&!listProduct.get(i).getBranchSchedulingInfo7().equals("")){
							branchArray[7]=listProduct.get(i).getBranchSchedulingInfo7();
						}
						if(listProduct.get(i).getBranchSchedulingInfo8()!=null
								&&!listProduct.get(i).getBranchSchedulingInfo8().equals("")){
							branchArray[8]=listProduct.get(i).getBranchSchedulingInfo8();
						}
						if(listProduct.get(i).getBranchSchedulingInfo9()!=null
								&&!listProduct.get(i).getBranchSchedulingInfo9().equals("")){
							branchArray[9]=listProduct.get(i).getBranchSchedulingInfo9();
						}
						
						ArrayList<BranchWiseScheduling> branchSchedulingList=null;
						if(listProduct.get(i).getBranchSchedulingInfo()!=null
								&&!listProduct.get(i).getBranchSchedulingInfo().equals("")){
							branchSchedulingList = AppUtility.getBranchSchedulingList(branchArray);
						}else{
							if(listProduct.get(i).getCustomerBranchSchedulingInfo()!=null){
							branchSchedulingList = listProduct.get(i).getCustomerBranchSchedulingInfo().get(listProduct.get(i).getProductSrNo());
							}

						}
						if(branchSchedulingList!=null&&branchSchedulingList.size()!=0){
							String branchAray[]=AppUtility.getCustomersUpdatedBranchSchedulingList(branchSchedulingList);
							if(branchAray.length>0){
								ArrayList<BranchWiseScheduling> branchlist = AppUtility.getBranchSchedulingList(branchAray);
								Integer prodSrNo = listProduct.get(i).getProductSrNo();
								HashMap<Integer, ArrayList<BranchWiseScheduling>> customerBranchlist = new HashMap<Integer, ArrayList<BranchWiseScheduling>>();
								customerBranchlist.put(prodSrNo, branchlist);
								sales.setCustomerBranchSchedulingInfo(customerBranchlist);
						
							}
						}
					
					}
					/**
					 * ends here
					 */
					
					/**
					 * Date:23/06/2018 BY Vijay
					 */
					if(listProduct.get(i).getArea()!=null && !listProduct.get(i).getArea().equals(""))
						sales.setArea(listProduct.get(i).getArea());
					/**
					 * ends here
					 */
					
					
					
					/**
			  		 * Date 28-09-2018 By Vijay 
			  		 * Des:- NBHC CCPM for every contract this must be true by default it does not allow to change for user
			  		 */
			  		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "OnlyForNBHC")){
			  			form.cbStartOfPeriod.setValue(true);
			  			form.cbStartOfPeriod.setEnabled(false);
			  		}
			  		/**
			  		 * ends here
			  		 */
			  		
			  		/**
			  		 * Date 20-03-2019 by vijay for NBHC CCPM for renewal contracts must have the quotation id
			  		 */
					if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("ContractRenewal", "EnableQuotationMandatoryForContractRenwal")){
				  		if(singledropdownpopup!=null && singledropdownpopup.getOlbQuotationId().getSelectedIndex()!=0){
					  		String quotationId[] = singledropdownpopup.getOlbQuotationId().getValue(singledropdownpopup.getOlbQuotationId().getSelectedIndex()).split("/");
					  		form.getTbQuotatinId().setValue(quotationId[0]);
				  		}
					}
					
		    		arraylis.add(sales);
		    	  }
		    	  
		    	  /**
					 * Date 26-07-2019 by Vijay 
					 * Des :- NBHC CCPM for renewal contract set default to AMC and Rate.
					 */
					if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "EnableRenewalContractTypeDefault")){
						if(model.isContractRate()){
							form.getOlbContractType().setValue("Rate Contract(Renewal)");
							form.getOlbContractType().setEnabled(false);
						}
						else{
							form.getOlbContractType().setValue("AMC(Renewal)");
							form.getOlbContractType().setEnabled(false);
						}
					}
					/**
					 * ends here
					 */
					
//		    	  form.getSaleslineitemtable().setValue(arraylis);
//		    	  List<ServiceSchedule> popuplist=form.reactfordefaultTableOnRenewal(arraylis);
//				  form.getServiceScheduleTable().getDataprovider().setList(popuplist);
		    	  
					
					if(response.equals("")) {
		    	  /**
		    	   * Date : 13-05-2017 By Anil
		    	   * This method update itemlist by adding branch info at product level.
		    	   */
				if(model.getItems().get(0).getBranchSchedulingInfo()!=null&&!model.getItems().get(0).getBranchSchedulingInfo().equals("")){
					form.getSaleslineitemtable().setValue(arraylis);
					List<ServiceSchedule> popuplist = form.reactfordefaultTableOnRenewal(arraylis);
					form.getServiceScheduleTable().getDataprovider().setList(popuplist);
				}else{
				  
		    	  List<SalesLineItem>itemList=AppUtility.getItemList(arraylis, model.getServiceScheduleList(),form.customerbranchlist,model.getBranch());
		    	  if(itemList!=null){
		    		  form.getSaleslineitemtable().setValue(itemList);
		    		  ArrayList<SalesLineItem> items = new ArrayList<SalesLineItem>(itemList);
		    		  List<ServiceSchedule> popuplist = form.reactfordefaultTableOnRenewal(items);
					  form.getServiceScheduleTable().getDataprovider().setList(popuplist);
		    	  }else{
	//		    		 form.setToNewState();
		    		  form.showDialogMessage("No. of branches at product level and no. of selected branches at service scheduling popup are different.");
	//		    		 return;
		    	  }
				}
					}
					else
						form.showDialogMessage(response);
				/**
	    	    * End
	    	    */
				/**
				 * nidhi
				 * 27-04-2018
				 * for renewal contract from old contract
				 */
				form.contractRenewFlag =true;
				/**
				 * end
				 */
				if(response.equals("")) {
		    	  form.getDototalamt().setValue(model.getTotalAmount());
		    	  form.getDonetpayamt().setValue(model.getNetpayable());
				}
		      }
		    };
		    t.schedule(3000);
	}
	
	
	public static void saveProductDetailsOfTable(RenewalResult renewalResult){
		form.showWaitSymbol();
		System.out.println("SAVE CONTRACT ID :: "+renewalResult.getContractId());
		System.out.println(" SAVE CONTRACT price :: "+renewalResult.getItems().get(0).getPrice());
		ContractRenewal conren=setContractRenewalFromRenewalResult(renewalResult);
		contractRenewalServ.saveProductDetails(conren, new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
				form.showDialogMessage("Unexpected Error !");
			}

			@Override
			public void onSuccess(Void result) {
				form.hideWaitSymbol();
				form.showDialogMessage("Data save Successfully !");
			}
		});
	}
	
	private static ContractRenewal setContractRenewalFromRenewalResult(RenewalResult list){
		
		System.out.println("SAVE CONTRACT ID 1 :: "+list.getContractId());
		ContractRenewal model=new ContractRenewal();
		
		model.setContractId(list.getContractId());
		model.setContractDate(list.getContractDate());
		model.setContractEndDate(list.getContractEndDate());
		model.setBranch(list.getBranch());
		model.setSalesPerson(list.getSalesPerson());
		model.setCustomerId(list.getCustomerId());
		model.setCustomerName(list.getCustomerName());
		model.setCustomerCell(list.getCustomerCell());
		model.setDate(list.getDate());//Ashwini Patil Date:27-04-2023
		
		model.setCity(list.getCity());
		model.setLocality(list.getLocality());
		model.setItems(list.getItems());
		model.setStatus(list.getStatus());
		model.setDate(list.getDate());
		
		model.setDescription(list.getDescription());
		
		model.setCompanyId(UserConfiguration.getCompanyId());
		/****Manisha added this line for setting net payable on 4/11/2017**/
		
		model.setNetPayable(list.getNetPayable());
		/********end********/
		return model;
	
	}
	
	public void reactToSMS(final String desc){
		Vector<Filter> filtrvec = new Vector<Filter>();
		Filter filtr = null;
		
		filtr = new Filter();
		filtr.setLongValue(model.getCompanyId());
		filtr.setQuerryString("companyId");
		filtrvec.add(filtr);
		
		filtr = new Filter();
		filtr.setBooleanvalue(true);
		filtr.setQuerryString("status");
		filtrvec.add(filtr);
		
		MyQuerry querry = new MyQuerry();
		querry.setFilters(filtrvec);
		querry.setQuerryObject(new SmsConfiguration());
//		form.showWaitSymbol();
		
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				if(result.size()!=0){
					for(SuperModel model:result){
						SmsConfiguration smsconfig = (SmsConfiguration) model;
						boolean smsStatus = smsconfig.getStatus();
						String accountsid = smsconfig.getAccountSID();
						String authotoken = smsconfig.getAuthToken();
						String fromnumber = smsconfig.getPassword();
						
						if(smsStatus==true){
						 sendSMS(accountsid, authotoken, fromnumber,desc);
						}
						else{
							form.showDialogMessage("SMS Configuration is InActive");
						}
					}
				}
				
			}
			@Override
			public void onFailure(Throwable caught) {
				
				
			}
		});
	}
	
	
	private void sendSMS(final String accountsid, final String authotoken,final String fromnumber,String actualMsg) {
		SmsServiceAsync smsserviceAsync=GWT.create(SmsService.class);
		
		List<RenewalResult>list=form.getAllProcessedSelectedRecords();
		ArrayList<RenewalResult> arrAttendies=new ArrayList<RenewalResult>();
		
		arrAttendies.addAll(list);
		System.out.println("message==="+actualMsg);
		
		System.out.println("in side  send sms "+list.size());
		smsserviceAsync.sendContractRenewalSMS(actualMsg, arrAttendies, accountsid, authotoken, fromnumber,UserConfiguration.getCompanyId(),LoginPresenter.bhashSMSFlag, new AsyncCallback<Integer>() {
			@Override
			public void onFailure(Throwable caught) {
				caught.printStackTrace();
				form.showDialogMessage("Unable to send sms");
			}
			@Override
			public void onSuccess(Integer result) {
				if(result==1){
					form.showDialogMessage("Sms sent successfully !");
				}
			}
		});
		
	}
	
	
	public void reactOnDownload1() {
		
		/**
		 * Date 08-07-2017 By Vijay
		 * for Unprocessed contract download data with process config 
		 * data will be download as per filters selected  
		 *  and else block for old code data will download as per table list
		 */
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("ContractRenewal", "UnprocessedContractRenewalReport")){
			
	 		StringBuilder filterQuerry = new StringBuilder();
	 		StringBuilder filterValue = new StringBuilder();
	 		
 			filterQuerry.append("renewProcessed"+"~");
 			filterValue.append("NO"+"~");
 			
	 		if(!form.pic.getId().getValue().equals(""))
			{  
				filterQuerry.append("cinfo.count"+"~");
				filterValue.append(Integer.parseInt(form.pic.getId().getValue())+"~");
			}
	 		if(!(form.pic.getName().getValue().equals("")))
			{
				filterQuerry.append("cinfo.fullName"+"~");
				filterValue.append(form.pic.getName().getValue()+"~");
			}
	 		if(form.olbBranch.getSelectedIndex()!=0){
				filterQuerry.append("branch"+"~");
				filterValue.append(form.olbBranch.getValue().trim()+"~");
			}
	 		
	 		if(form.olbeSalesPerson.getSelectedIndex()!=0)
			{
	 			filterQuerry.append("employee"+"~");
				filterValue.append(form.olbeSalesPerson.getValue().trim()+"~");
			}
			
			if(form.fromdate.getValue()!=null){
				filterQuerry.append("endDate >="+"~");
				filterValue.append(AppUtility.parseDate(form.fromdate.getValue())+"~");
			}
			
			if(form.fromdate.getValue()!=null){
				filterQuerry.append("endDate <="+"~");
				filterValue.append(AppUtility.parseDate(form.todate.getValue())+"~");
			}
			
			if(!form.tbContractId.getValue().equals("")){
				filterQuerry.append("count"+"~");
				filterValue.append(form.tbContractId.getValue().trim()+"~");
			}
			
			if(form.lbStatus.getSelectedIndex()!=0){
				
				if(form.lbStatus.getValue(form.lbStatus.getSelectedIndex()).equals(AppConstants.UNPROCESSED)){
					filterQuerry.append("status"+"~");
					filterValue.append(Contract.APPROVED+"~");
				}else{
					filterQuerry.append("status"+"~");
					filterValue.append(form.lbStatus.getValue(form.lbStatus.getSelectedIndex()).trim()+"~");
				}
			}else{
				
				filterQuerry.append("status"+"~");
				filterValue.append(Contract.APPROVED+"~");
			}
			
			filterQuerry.append("companyId");
			filterValue.append(UserConfiguration.getCompanyId());
			
			String querry = filterQuerry.toString();
			String value = filterValue.toString();
			
			String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
			final String url = gwt + "csvservlet" + "?type=" + 132+ "&" +"querry="+querry+ "&" +"value="+value;
			Window.open(url, "test", "enabled");
			
		}else{
			
			ArrayList<RenewalResult> serviceList = new ArrayList<RenewalResult>();
			List<RenewalResult> list = (List<RenewalResult>) form.table.getDataprovider().getList();
			serviceList.addAll(list);
			csvservice.setContractRenewallist(serviceList, new AsyncCallback<Void>() {
				@Override
				public void onFailure(Throwable caught) {
					System.out.println("RPC call Failed" + caught);
				}
				@Override
				public void onSuccess(Void result) {
					String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url = gwt + "csvservlet" + "?type=" + 106;
					Window.open(url, "test", "enabled");
				}
			});
		}
		
	}

	
	@Override
	public void reactOnDownload() {
		
		/**
		 * Date 08-07-2017 By Vijay
		 * for Unprocessed contract download data with process config 
		 * data will be download as per filters selected  
		 * and else block for old code
		 */
		
		StringBuilder filterQuerry = new StringBuilder();
 		StringBuilder filterValue = new StringBuilder(); 
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("ContractRenewal", "ProcessedContractRenewalReport")){
			
			if(!form.pic.getId().getValue().equals(""))
			{  
				filterQuerry.append("customerId"+"~");
				filterValue.append(Integer.parseInt(form.pic.getId().getValue())+"~");
			}

			if(!(form.pic.getName().getValue().equals("")))
			{
				filterQuerry.append("customerName"+"~");
				filterValue.append(form.pic.getName().getValue()+"~");
			}
			
			if(form.olbBranch.getSelectedIndex()!=0){
				filterQuerry.append("branch"+"~");
				filterValue.append(form.olbBranch.getValue().trim()+"~");
			}
			if(form.olbeSalesPerson.getSelectedIndex()!=0)
			{
				filterQuerry.append("salesPerson"+"~");
				filterValue.append(form.olbeSalesPerson.getValue().trim()+"~");
			}
			
			if(form.fromdate.getValue()!=null){
				filterQuerry.append("contractEndDate >="+"~");
				filterValue.append(AppUtility.parseDate(form.fromdate.getValue())+"~");
			}
				
			if(form.todate.getValue()!=null){
				filterQuerry.append("contractEndDate <="+"~");
				filterValue.append(AppUtility.parseDate(form.todate.getValue())+"~");
			}
			
			if(!form.tbContractId.getValue().equals("")){
				filterQuerry.append("contractId"+"~");
				filterValue.append(form.tbContractId.getValue().trim()+"~");
			}
			
			if(form.lbStatus.getSelectedIndex()!=0)
			{
				filterQuerry.append("status"+"~");
				filterValue.append(form.lbStatus.getValue(form.lbStatus.getSelectedIndex()).trim()+"~");
			}else{
				filterQuerry.append("status"+"~");
				filterValue.append(AppConstants.PROCESSED+"~");
			}
			
			filterQuerry.append("companyId");
			filterValue.append(UserConfiguration.getCompanyId());
			
			String querry = filterQuerry.toString();
			String value = filterValue.toString();
			
			String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
			final String url = gwt + "csvservlet" + "?type=" + 133+ "&" +"querry="+querry+ "&" +"value="+value;
			Window.open(url, "test", "enabled");
			
		}else{
			
			ArrayList<RenewalResult> serviceList = new ArrayList<RenewalResult>();
			List<RenewalResult> list = (List<RenewalResult>) form.processedtable.getDataprovider().getList();
			serviceList.addAll(list);
			csvservice.setContractRenewallist(serviceList, new AsyncCallback<Void>() {
				@Override
				public void onFailure(Throwable caught) {
					System.out.println("RPC call Failed" + caught);
				}
				@Override
				public void onSuccess(Void result) {
					String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url = gwt + "csvservlet" + "?type=" + 105;
					Window.open(url, "test", "enabled");
				}
			});
		}
		
	}
	
	public void sendEmail() {
		boolean conf = Window.confirm("Do you really want to send email?");
		if (conf == true) {
			emailService.initiateContractRenewalEmail( model,new AsyncCallback<Void>() {
				@Override
				public void onFailure(Throwable caught) {
					Window.alert("Resource Quota Ended ");
					caught.printStackTrace();
				}

				@Override
				public void onSuccess(Void result) {
					Window.alert("Email Sent Sucessfully !");
				}
			});
		}
	}
	
	private void setEmailPopUpData() {
		final List<RenewalResult>list= form.getAllProcessedSelectedRecords();

//		form.showWaitSymbol();
//		String customerEmail = "";
//
//		String branchName = "";
//		if(list.get(0).getBranch()!=null){
//			branchName = list.get(0).getBranch();
//		}
//		String salesPerson ="";
//		if(list.get(0).getSalesPerson()!=null){
//			salesPerson = list.get(0).getSalesPerson();
//		}
//		
//		String fromEmailId = AppUtility.getFromEmailAddress(branchName,salesPerson);
//		customerEmail = AppUtility.getCustomerEmailid(list.get(0).getCustomerId());
//		
//		String customerName = list.get(0).getCustomerName();
//		if(!customerName.equals("") && !customerEmail.equals("")){
//			label : AppUtility.loadContactPersonlist(emailpopup.olbContactlistEmailId,"Customer",list.get(0).getCustomerId(),customerName,customerEmail,null);
//		}
//		else{
//	
//			MyQuerry querry = AppUtility.getcustomerQuerry(list.get(0).getCustomerId());
//			async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
//				
//				@Override
//				public void onSuccess(ArrayList<SuperModel> result) {
//					// TODO Auto-generated method stub
//					for(SuperModel smodel : result){
//						Customer custEntity  = (Customer) smodel;
//						System.out.println("customer Name = =="+custEntity.getFullname());
//						System.out.println("Customer Email = == "+custEntity.getEmail());
//						
//						if(custEntity.getEmail()!=null){
//							label : AppUtility.loadContactPersonlist(emailpopup.olbContactlistEmailId,"Customer",list.get(0).getCustomerId(),custEntity.getFullname(),custEntity.getEmail(),null);
//						}
//						else{
//							label : AppUtility.loadContactPersonlist(emailpopup.olbContactlistEmailId,"Customer",list.get(0).getCustomerId(),custEntity.getFullname(),custEntity.getEmail(),null);
//						}
//						break;
//					}
//				}
//				
//				@Override
//				public void onFailure(Throwable caught) {
//					// TODO Auto-generated method stub
//				}
//			});
//		}
//		
//		Screen screenName=(Screen) AppMemory.getAppMemory().currentScreen;
//		Console.log("screenName "+screenName);
//		AppUtility.makeLiveSmsTemplateConfig(emailpopup.oblTemplateName,LoginPresenter.currentModule.trim(),screenName);
//		emailpopup.taFromEmailId.setValue(fromEmailId);
//		emailpopup.smodel = getRenewalEntity(list.get(0));
//		form.hideWaitSymbol();
		
		

		
		MyQuerry querry = new MyQuerry();
		Company c=new Company();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		
		temp=new Filter();
		temp.setLongValue(c.getCompanyId());
		temp.setQuerryString("companyId");
		filtervec.add(temp);
		
		
		temp=new Filter();
		temp.setIntValue(list.get(0).getContractId());
		temp.setQuerryString("contractId");
		filtervec.add(temp);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new ContractRenewal());
		
		form.showWaitSymbol();
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				form.hideWaitSymbol();
				
				for(SuperModel model : result){
					final ContractRenewal contractRenewalEntity = (ContractRenewal) model;
					String customerEmail = "";
					
					String branchName = "";
					if(contractRenewalEntity.getBranch()!=null){
						branchName = contractRenewalEntity.getBranch();
					}
					String salesPerson ="";
					if(contractRenewalEntity.getSalesPerson()!=null){
						salesPerson = contractRenewalEntity.getSalesPerson();
					}
					
					String fromEmailId = AppUtility.getFromEmailAddress(branchName,salesPerson);
					customerEmail = AppUtility.getCustomerEmailid(contractRenewalEntity.getCustomerId());
					
					String customerName = contractRenewalEntity.getCustomerName();
					if(!customerName.equals("") && !customerEmail.equals("")){
						label : AppUtility.loadContactPersonlist(emailpopup.olbContactlistEmailId,"Customer",contractRenewalEntity.getCustomerId(),customerName,customerEmail,null);
					}
					else{
				
						MyQuerry querry = AppUtility.getcustomerQuerry(contractRenewalEntity.getCustomerId());
						async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
							
							@Override
							public void onSuccess(ArrayList<SuperModel> result) {
								// TODO Auto-generated method stub
								for(SuperModel smodel : result){
									Customer custEntity  = (Customer) smodel;
									System.out.println("customer Name = =="+custEntity.getFullname());
									System.out.println("Customer Email = == "+custEntity.getEmail());
									
									if(custEntity.getEmail()!=null){
										label : AppUtility.loadContactPersonlist(emailpopup.olbContactlistEmailId,"Customer",contractRenewalEntity.getCustomerId(),custEntity.getFullname(),custEntity.getEmail(),null);
									}
									else{
										label : AppUtility.loadContactPersonlist(emailpopup.olbContactlistEmailId,"Customer",contractRenewalEntity.getCustomerId(),custEntity.getFullname(),custEntity.getEmail(),null);
									}
									break;
								}
							}
							
							@Override
							public void onFailure(Throwable caught) {
								// TODO Auto-generated method stub
							}
						});
					}
					
					Screen screenName=(Screen) AppMemory.getAppMemory().currentScreen;
					Console.log("screenName "+screenName);
					AppUtility.makeLiveSmsTemplateConfig(emailpopup.oblTemplateName,LoginPresenter.currentModule.trim(),screenName);
					emailpopup.taFromEmailId.setValue(fromEmailId);
					contractRenewalEntity.setCount(contractRenewalEntity.getContractId());
					emailpopup.smodel = contractRenewalEntity;
					break;
				}

			}
			
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
			}
		});
	
		
		
	}


	private SuperModel getRenewalEntity(RenewalResult renewalResult) {
		
		ContractRenewal contractrenewal = new ContractRenewal();
		if(renewalResult.getBranch()!=null)
		contractrenewal.setBranch(renewalResult.getBranch());
		contractrenewal.setCompanyId(renewalResult.getCompanyId());
		if(renewalResult.getContractDate()!=null)
		contractrenewal.setContractDate(renewalResult.getContractDate());
		if(renewalResult.getContractEndDate()!=null)
		contractrenewal.setContractEndDate(renewalResult.getContractEndDate());
		contractrenewal.setContractId(renewalResult.getContractId());
		if(renewalResult.getContractType()!=null)
		contractrenewal.setContractType(renewalResult.getContractType());
		contractrenewal.setCustomerId(renewalResult.getCustomerId());
		contractrenewal.setCustomerCell(renewalResult.getCustomerCell());
		contractrenewal.setCustomerName(renewalResult.getCustomerName());
		if(renewalResult.getDate()!=null)
		contractrenewal.setDate(renewalResult.getDate());
		if(renewalResult.getDescription()!=null)
		contractrenewal.setDescription(renewalResult.getDescription());
		contractrenewal.setDuration(renewalResult.getDuration());
		contractrenewal.setItems(renewalResult.getItems());
		contractrenewal.setNetPayable(renewalResult.getNetPayable());
		contractrenewal.setProductCode(renewalResult.getProductCode());
		contractrenewal.setProductId(renewalResult.getProductId());
		contractrenewal.setProductName(renewalResult.getProductName());
		contractrenewal.setProductNewPrice(renewalResult.getProductNewPrice());
		contractrenewal.setProductPrice(renewalResult.getProductPrice());
		if(renewalResult.getSalesPerson()!=null)
		contractrenewal.setSalesPerson(renewalResult.getSalesPerson());
		return contractrenewal;
	}
	
	@Override
	public void reactOnSMS() {
		loadSMSPopUpData();
	}

	private void loadSMSPopUpData() {
		
		List<RenewalResult>list=form.getAllProcessedSelectedRecords();
		if(list.size()==0){
			form.showDialogMessage("Please select processed record !");
		}else if(list.size()>1){
			form.showDialogMessage("Please select only one processed record !");
		}else{
			
			getUpdatedProcessedData(list.get(0).getContractId());
		}
		
		
	}


	private void getUpdatedProcessedData(int contractId) {
		
		MyQuerry querry = new MyQuerry();
		Company c=new Company();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		
		temp=new Filter();
		temp.setLongValue(c.getCompanyId());
		temp.setQuerryString("companyId");
		filtervec.add(temp);
		
		
		temp=new Filter();
		temp.setIntValue(contractId);
		temp.setQuerryString("contractId");
		filtervec.add(temp);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new ContractRenewal());
		
		form.showWaitSymbol();
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				form.hideWaitSymbol();
				
				smspopup.showPopUp();

				for(SuperModel model : result){
					ContractRenewal conrenewal = (ContractRenewal) model;

					form.showWaitSymbol();

					String customerCellNo = conrenewal.getCustomerCell()+"";

					String customerName = conrenewal.getCustomerName();
					if(!customerName.equals("") && !customerCellNo.equals("")){
						AppUtility.loadContactPersonlist(smspopup.olbContactlistEmailId,"Customer",conrenewal.getCustomerId(),customerName,customerCellNo,null,true);
					}
					Screen screenName=(Screen) AppMemory.getAppMemory().currentScreen;
					Console.log("screenName "+screenName);
					AppUtility.makeLiveSmsTemplateConfig(smspopup.oblsmsTemplate,LoginPresenter.currentModule.trim(),screenName,true,AppConstants.SMS);
//					smspopup.smodel = getRenewalEntity(list.get(0));
					smspopup.getRbSMS().setValue(true);
					conrenewal.setCount(conrenewal.getContractId());
					smspopup.smodel = conrenewal;
					form.hideWaitSymbol();
					break;
				}
				
				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
			}
		});
	}


	@Override
	public void reactOnCommunicationLog() {
		

		List<RenewalResult>list=form.getAllProcessedSelectedRecords();
		if(list.size()==0){
			form.showDialogMessage("Please select processed record !");
		}else if(list.size()>1){
			form.showDialogMessage("Please select only one processed record !");
		}else{
			

			
			form.showWaitSymbol();
			MyQuerry querry = AppUtility.communicationLogQuerry(list.get(0).getContractId(), model.getCompanyId(),AppConstants.SERVICEMODULE,AppConstants.CONTRACTRENEWALL);

			async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					
					ArrayList<InteractionType> list = new ArrayList<InteractionType>();
					
					for(SuperModel model : result){
						
						InteractionType interactionType = (InteractionType) model;
						list.add(interactionType);
					}
					form.hideWaitSymbol();
					Comparator<InteractionType> comp=new Comparator<InteractionType>() {
						@Override
						public int compare(InteractionType e1, InteractionType e2) {
							Integer ee1=e1.getCount();
							Integer ee2=e2.getCount();
							return ee2.compareTo(ee1);
							
						}
					};
					Collections.sort(list,comp);
					/**
					 * End
					 */
					CommunicationLogPopUp.getRemark().setValue("");
					CommunicationLogPopUp.getDueDate().setValue(null);
					CommunicationLogPopUp.getOblinteractionGroup().setSelectedIndex(0);
					CommunicationLogPopUp.getCommunicationLogTable().getDataprovider().setList(list);
					
					LoginPresenter.communicationLogPanel = new PopupPanel(true);
					LoginPresenter.communicationLogPanel.add(CommunicationLogPopUp);
					LoginPresenter.communicationLogPanel.show();
					LoginPresenter.communicationLogPanel.center();
				}
				
				@Override
				public void onFailure(Throwable caught) {
					form.hideWaitSymbol();
				}
			});
			
		
		}
		
	}
	
	
	public void runRegularDownload1Program(){
		// Date 08-07-2017 By Vijay added if condition for new download with filters
		Console.log("in runRegularDownload1Program");
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("ContractRenewal", "UnprocessedContractRenewalReport")){
			reactOnDownload1();
		}
		else if(form.table.getDataprovider().getList().size()!=0){
			reactOnDownload1();
		}else{
			form.showDialogMessage("No unprocessed records !");
		}
		
	}
	public void runRegularDownload2Program(){
		Console.log("in runRegularDownload2Program");
		// Date 08-07-2017 By Vijay added if condition for new download with filters
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("ContractRenewal", "ProcessedContractRenewalReport")){
			reactOnDownload();
		}
		else if(form.processedtable.getDataprovider().getList().size()!=0){
			reactOnDownload();
		}else{
			form.showDialogMessage("No processed records !");
		}
	}
	
}
