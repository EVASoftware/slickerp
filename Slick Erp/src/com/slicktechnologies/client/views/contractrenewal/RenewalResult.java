package com.slicktechnologies.client.views.contractrenewal;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Entity;
import com.slicktechnologies.shared.SalesLineItem;

public class RenewalResult implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7832832554419202465L;
	
	int contractId;
	Date contractDate;
	Date contractEndDate;
	String branch;
	String salesPerson;
	int customerId;
	String customerName;
	long customerCell;
	String city;
	String locality;
	int productId;
	String productName;
	String productCode;
	double productPrice;
	double productNewPrice;
	int duration;
	String year;
	String status;
	Date date;
	String user;
	String renewProcessed;
	protected boolean recordSelect;
	protected ArrayList<SalesLineItem>items;
	
	String description;
	
	// Date 11 April 2017 added by vijay requirement from PCAAMB
	double netPayable;
	String contractType;
		
	/**
	 * nidhi 20-12-2018
	 */
	Integer refContractCount ;
	double oldContractValue;
	double avgValue;
	Long companyId;
	
	String nonRenewalRemark;
	
	Date contractStartDate;
	String contractCategory;//16-01-2024 Ultra pest requirement
	
	public RenewalResult() {
		date=new Date();
		items=new ArrayList<SalesLineItem>();
		description="";
		contractType="";
		nonRenewalRemark ="";
		contractCategory="";
	}


	public String getNonRenewalRemark() {
		return nonRenewalRemark;
	}



	public void setNonRenewalRemark(String nonRenewalRemark) {
		this.nonRenewalRemark = nonRenewalRemark;
	}



	public int getContractId() {
		return contractId;
	}



	public void setContractId(int contractId) {
		this.contractId = contractId;
	}



	public Date getContractDate() {
		return contractDate;
	}



	public void setContractDate(Date contractDate) {
		this.contractDate = contractDate;
	}



	public String getBranch() {
		return branch;
	}



	public void setBranch(String branch) {
		this.branch = branch;
	}



	public String getSalesPerson() {
		return salesPerson;
	}



	public void setSalesPerson(String salesPerson) {
		this.salesPerson = salesPerson;
	}



	public String getCustomerName() {
		return customerName;
	}



	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}



	public long getCustomerCell() {
		return customerCell;
	}



	public void setCustomerCell(long customerCell) {
		this.customerCell = customerCell;
	}



	public String getCity() {
		return city;
	}



	public void setCity(String city) {
		this.city = city;
	}



	public String getLocality() {
		return locality;
	}



	public void setLocality(String locality) {
		this.locality = locality;
	}



	public int getProductId() {
		return productId;
	}



	public void setProductId(int productId) {
		this.productId = productId;
	}



	public String getProductName() {
		return productName;
	}



	public void setProductName(String productName) {
		this.productName = productName;
	}



	public String getProductCode() {
		return productCode;
	}



	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}



	public double getProductPrice() {
		return productPrice;
	}



	public void setProductPrice(double productPrice) {
		this.productPrice = productPrice;
	}



	public double getProductNewPrice() {
		return productNewPrice;
	}



	public void setProductNewPrice(double productNewPrice) {
		this.productNewPrice = productNewPrice;
	}



	public int getDuration() {
		return duration;
	}



	public void setDuration(int duration) {
		this.duration = duration;
	}



	public String getYear() {
		return year;
	}



	public void setYear(String year) {
		this.year = year;
	}



	public String getStatus() {
		return status;
	}



	public void setStatus(String status) {
		this.status = status;
	}



	public Date getDate() {
		return date;
	}



	public void setDate(Date date) {
		this.date = date;
	}



	public String getUser() {
		return user;
	}



	public void setUser(String user) {
		this.user = user;
	}



	public int getCustomerId() {
		return customerId;
	}



	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}



	public boolean getRecordSelect() {
		return recordSelect;
	}



	public void setRecordSelect(boolean recordSelect) {
		this.recordSelect = recordSelect;
	}



	public Date getContractEndDate() {
		return contractEndDate;
	}



	public void setContractEndDate(Date contractEndDate) {
		this.contractEndDate = contractEndDate;
	}


	/**
	 * Gets the items.
	 * @return the items
	 */
	public List<SalesLineItem> getItems() 
	{
		return items;
	}

	/**
	 * Sets the items.
	 * @param items the new items
	 */
	public void setItems(List<SalesLineItem> items) 
	{
		ArrayList<SalesLineItem> arrayitems=new ArrayList<SalesLineItem>();
		arrayitems.addAll(items);
		this.items = arrayitems;
	}



	public String getRenewProcessed() {
		return renewProcessed;
	}



	public void setRenewProcessed(String renewProcessed) {
		this.renewProcessed = renewProcessed;
	}



	public String getDescription() {
		return description;
	}



	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getContractType() {
		return contractType;
	}


	public void setContractType(String contractType) {
		this.contractType = contractType;
	}
	
	
	public double getNetPayable() {
		return netPayable;
	}

	public void setNetPayable(double netPayable) {
		this.netPayable = netPayable;
	}



	public Integer getRefContractCount() {
		return refContractCount;
	}



	public void setRefContractCount(Integer refContractCount) {
		this.refContractCount = refContractCount;
	}



	public double getOldContractValue() {
		return oldContractValue;
	}



	public void setOldContractValue(double oldContractValue) {
		this.oldContractValue = oldContractValue;
	}



	public double getAvgValue() {
		return avgValue;
	}



	public void setAvgValue(double avgValue) {
		this.avgValue = avgValue;
	}



	public Long getCompanyId() {
		return companyId;
	}



	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}



	public Date getContractStartDate() {
		return contractStartDate;
	}



	public void setContractStartDate(Date contractStartDate) {
		this.contractStartDate = contractStartDate;
	}

	public String getContractCategory() {
		return contractCategory;
	}



	public void setContractCategory(String contractCategory) {
		this.contractCategory = contractCategory;
	}



}
