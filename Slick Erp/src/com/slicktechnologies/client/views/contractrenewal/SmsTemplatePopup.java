package com.slicktechnologies.client.views.contractrenewal;

import java.util.Vector;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.TextArea;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;

public class SmsTemplatePopup extends AbsolutePanel implements ChangeHandler  {

	Button btnOne,btnTwo;
	ObjectListBox<SmsTemplate> smsEventList;
	TextArea taDesc;
	
	public SmsTemplatePopup() {
		
		InlineLabel displayMessage = new InlineLabel("SMS Templates");
		add(displayMessage,10,10);
		
		smsEventList = new ObjectListBox<SmsTemplate>();
		smsEventList.addChangeHandler(this);
		MyQuerry querry = new MyQuerry(new Vector<Filter>(),new SmsTemplate());
		smsEventList.MakeLive(querry);
		add(smsEventList,10,30);
		
		smsEventList.getElement().getStyle().setHeight(20, Unit.PX);
		
		
		
		InlineLabel description = new InlineLabel("Message");
		add(description,10,70);
		
		taDesc=new TextArea();
		taDesc.setEnabled(false);
		add(taDesc,10,90);
		
		taDesc.getElement().getStyle().setWidth(330, Unit.PX);
		taDesc.getElement().getStyle().setHeight(70, Unit.PX);
		
		btnOne=new Button("Ok");
		btnTwo=new Button("Cancel");
		
		btnOne.getElement().getStyle().setWidth(50, Unit.PX);
		btnTwo.getElement().getStyle().setWidth(60, Unit.PX);
		
		add(btnOne,10,180);
		add(btnTwo,120,180); 
		setSize("370px", "230px");
		this.getElement().setId("form");
		
	}

	@Override
	public void onChange(ChangeEvent event) {
		
		if(event.getSource().equals(smsEventList)){
			if(smsEventList.getSelectedIndex()!=0){
				SmsTemplate smsTemp=smsEventList.getSelectedItem();
				taDesc.setValue(smsTemp.getMessage());
			}else{
				taDesc.setValue(null);
			}
		}
		
	}
	
	public void clear(){
		smsEventList.setSelectedIndex(0);
		taDesc.setValue(null);
	}

	
	
	/**************************************Getters And Setters******************************************/
	
	public Button getBtnOne() {
		return btnOne;
	}

	public void setBtnOne(Button btnOne) {
		this.btnOne = btnOne;
	}

	public Button getBtnTwo() {
		return btnTwo;
	}

	public void setBtnTwo(Button btnTwo) {
		this.btnTwo = btnTwo;
	}

	public ObjectListBox<SmsTemplate> getSmsEventList() {
		return smsEventList;
	}

	public void setSmsEventList(ObjectListBox<SmsTemplate> smsEventList) {
		this.smsEventList = smsEventList;
	}

	public TextArea getTaDesc() {
		return taDesc;
	}

	public void setTaDesc(TextArea taDesc) {
		this.taDesc = taDesc;
	}
	
	
	
}
