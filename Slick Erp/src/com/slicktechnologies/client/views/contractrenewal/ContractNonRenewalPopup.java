package com.slicktechnologies.client.views.contractrenewal;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.helperlayer.Config;

public class ContractNonRenewalPopup extends PopupScreen{

	ObjectListBox<Config> olbNonRenewalRemark;
	
	public ContractNonRenewalPopup(){
		super();
		createGui();
		
	}
	
	private void initializeWidgets() {
		olbNonRenewalRemark = new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbNonRenewalRemark, Screen.DONOTRENEW);

	}

	
	@Override
	public void createScreen() {
		// TODO Auto-generated method stub
		initializeWidgets();
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField frenewalInfo = fbuilder.setlabel("Non Renewal Remark").widgetType(FieldType.Grouping).build();
		
		fbuilder = new FormFieldBuilder("* Remark",  olbNonRenewalRemark);
		FormField ftbremark = fbuilder.setMandatory(true).setMandatoryMsg("Remark is Mandatory!").setMandatory(true).setRowSpan(0).setColSpan(2).build();
		
		FormField [][]formfield = {
				{frenewalInfo},
				{ftbremark},
		};
		this.fields=formfield;
				
	}
	
	@Override
	public boolean validate() {
		 boolean superValidate = super.validate();
		
		return superValidate;
	};
	
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
	}

	public ObjectListBox<Config> getOlbNonRenewalRemark() {
		return olbNonRenewalRemark;
	}

	public void setOlbNonRenewalRemark(ObjectListBox<Config> olbNonRenewalRemark) {
		this.olbNonRenewalRemark = olbNonRenewalRemark;
	}


	
}
