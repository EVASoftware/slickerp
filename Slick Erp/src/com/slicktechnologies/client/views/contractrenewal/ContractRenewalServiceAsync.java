package com.slicktechnologies.client.views.contractrenewal;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.contractrenewal.ContractRenewal;

public interface ContractRenewalServiceAsync {
	public void  getRenewalStatus(ArrayList<Filter>filter,ArrayList<Filter>filter1,AsyncCallback<ArrayList<RenewalResult>> callback);
	public void processContractRenewal(ArrayList<ContractRenewal> contractRenewalList,AsyncCallback<ArrayList<RenewalResult>> callback);
	public void setReminder(ArrayList<ContractRenewal> renewalList,String loggedinuser,AsyncCallback<ArrayList<ContractRenewal>> callback);
	public void setStatusDoNotRenew(ArrayList<ContractRenewal> renewalList,AsyncCallback<ArrayList<ContractRenewal>> callback);
	public void setStatusNeverRenew(ArrayList<ContractRenewal> renewalList,AsyncCallback<ArrayList<ContractRenewal>> callback);
	public void saveProductDetails(ContractRenewal contractRenewalList,AsyncCallback<Void> callback);
	
	public void contractRenewalEntry(Contract contractEntity, String status,String description,AsyncCallback<String> callback);

}
