package com.slicktechnologies.client.views.contractrenewal;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.slicktechnologies.client.views.contractrenewal.RenewalResult;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.contractrenewal.ContractRenewal;

@RemoteServiceRelativePath("contractrenewalservice")
public interface ContractRenewalService extends RemoteService{
	public ArrayList<RenewalResult>  getRenewalStatus(ArrayList<Filter>filter,ArrayList<Filter>filter1);
	public ArrayList<RenewalResult> processContractRenewal(ArrayList<ContractRenewal> contractRenewalList);
	public ArrayList<ContractRenewal> setReminder(ArrayList<ContractRenewal> renewalList,String loggedinuser);
	public ArrayList<ContractRenewal> setStatusDoNotRenew(ArrayList<ContractRenewal> renewalList);
	public ArrayList<ContractRenewal> setStatusNeverRenew(ArrayList<ContractRenewal> renewalList);
	public void saveProductDetails(ContractRenewal contractRenewalList);
	
	public String contractRenewalEntry(Contract contractEntity, String status,String description);
}
