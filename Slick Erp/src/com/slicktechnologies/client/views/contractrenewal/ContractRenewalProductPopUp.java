package com.slicktechnologies.client.views.contractrenewal;

import java.util.Date;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.google.gwt.view.client.RowCountChangeEvent.Handler;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.personlayer.Employee;

public class ContractRenewalProductPopUp extends PopupScreen implements Handler, ChangeHandler{

	public ContractRenewalProductTable productList;
	TextArea taDesc;
	TextBox netPayable;
	PersonInfoComposite personInfo;
	TextBox tbcontractId;
	ObjectListBox<Employee> olbeSalesPerson;
	TextBox tbPercentage;
	DateBox renewalDate;
	
	public ContractRenewalProductPopUp(){
		super();
		createGui();
	}
	
	private void initiateWidget() {
		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new Customer());
		personInfo=new PersonInfoComposite(querry,false);
		personInfo.setEnable(false);
		
		productList=new ContractRenewalProductTable();
		productList.getTable().addRowCountChangeHandler(this);
		
		netPayable =new TextBox();
		netPayable.setEnabled(false);	
		
		taDesc=new TextArea();
		
		tbcontractId = new TextBox();
		tbcontractId.setEnabled(false);
		
		tbcontractId.getElement().getStyle().setWidth(200,Unit.PX);
		netPayable.getElement().getStyle().setWidth(200,Unit.PX);

		olbeSalesPerson=new ObjectListBox<Employee>();
		AppUtility.makeSalesPersonListBoxLive(olbeSalesPerson);
		
		
		tbPercentage= new TextBox();
		tbPercentage.addChangeHandler(this);
		
		renewalDate=new DateBoxWithYearSelector();
	}
	
	@Override
	public void createScreen() {

		initiateWidget();
		
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fCustInfo = fbuilder.setlabel("Customer Information").widgetType(FieldType.Grouping).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",  personInfo);
		FormField fpersonInfo = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Contract Id",  tbcontractId);
		FormField ftbcontractId = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("",  productList.getTable());
		FormField fproductTable = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
				
		fbuilder = new FormFieldBuilder();
		FormField fproductInfo = fbuilder.setlabel("Product Information").widgetType(FieldType.Grouping).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Net Payable",  netPayable);
		FormField fNetPayable = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fblankgroup=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("Sales Person",olbeSalesPerson);
		FormField olbeSalesPerson= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Percentage",tbPercentage);
		FormField fdbPercentage= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Renewal Date",renewalDate);
		FormField frenewalDate= fbuilder.setMandatory(true).setMandatoryMsg("Renewal date mandatory").setRowSpan(0).setColSpan(0).build();
		
		
		FormField [][]formfield = {
				{fCustInfo},
				{fpersonInfo},
				{ftbcontractId,frenewalDate, olbeSalesPerson,fdbPercentage,fblankgroup},
				{fproductInfo},
				{fproductTable},
				{fblankgroup,fNetPayable},

		};
		this.fields=formfield;
	}
	
	@Override
	public void onRowCountChange(RowCountChangeEvent event) {
		
		if(event.getSource().equals(productList.getTable()))
		{
			double netpay=0;
			for(ContractRenewalProduct obj :productList.getDataprovider().getList())
			{
				netpay = netpay + obj.getNetPayable();
			}	
			
			netPayable.setValue(Math.round(netpay)+"");
		}
	}

	@Override
	public void onClick(ClickEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public TextBox getNetPayable() {
		return netPayable;
	}

	public void setNetPayable(TextBox netPayable) {
		this.netPayable = netPayable;
	}


	public TextBox getTbPercentage() {
		return tbPercentage;
	}

	public void setTbPercentage(TextBox tbPercentage) {
		this.tbPercentage = tbPercentage;
	}

	@Override
	public void onChange(ChangeEvent event) {

		if(event.getSource()==tbPercentage){
			List<ContractRenewalProduct> list = productList.getDataprovider().getList();
			double netpay=0;
			System.out.println("tbPercentage.getValue()"+tbPercentage.getValue());
			String percetagevalue = 0+"";
			if(tbPercentage.getValue()!=null && !tbPercentage.getValue().equals("")){
				percetagevalue = tbPercentage.getValue();
			}
//			if(tbPercentage.getValue()!=null && !tbPercentage.getValue().equals("")){
				for(ContractRenewalProduct object :list){
					
//						double newprice ;
//						if(percetagevalue.equals("0")){
//							newprice = 	object.getProductPriceNew2();
//						}
//						else{
//							newprice = 	object.getProductNewPrice();
//						}
						
//					    double newprice = object.getProductPriceNew2();
						double newprice = object.getProductPrice();

						String strpercentage = percetagevalue;
						double percetage = 0;
						if(strpercentage!=null && !strpercentage.equals("0")){
							percetage = Double.parseDouble(strpercentage);
						}
						
						double percentageAmt = (newprice*percetage)/100;
						double updatedAmount = newprice+(percentageAmt);
						Console.log("newprice"+newprice);
						Console.log("updatedAmount"+updatedAmount);
						Console.log("percentageAmt"+percentageAmt);
						
						object.setProductNewPrice(updatedAmount);
						object.setPercentage(percetage);
						
						
						ContractRenewalProductTable table = new ContractRenewalProductTable();
						object.setNetPayable(table.gettaxAmount(updatedAmount, object));
						
						netpay = netpay + object.getNetPayable();
	
				}
				productList.getDataprovider().setList(list);
				netPayable.setValue(Math.round(netpay)+"");



			}

			
		}
	
	

}
