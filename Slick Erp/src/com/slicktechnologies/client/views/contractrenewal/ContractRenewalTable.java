package com.slicktechnologies.client.views.contractrenewal;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.google.code.p.gwtchismes.client.GWTCGlassPanel;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.ValueUpdater;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.Header;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;

public class ContractRenewalTable extends SuperTable<RenewalResult> implements ClickHandler{
	
int rowIndex;

NumberFormat nf=NumberFormat.getFormat("0.00");
DateTimeFormat fmt = DateTimeFormat.getFormat("dd-MM-yyyy");

//ContractRenewalProductTablePouup prodTblPop=new ContractRenewalProductTablePouup();
//PopupPanel panel=new PopupPanel(true);

ContractRenewalProductPopUp productpopup = new ContractRenewalProductPopUp();

TextColumn<RenewalResult> getColumnContractId;
TextColumn<RenewalResult> getColumncontractDate;
TextColumn<RenewalResult> getColumncontractEndDate;
TextColumn<RenewalResult> getColumnbranch;
TextColumn<RenewalResult> getColumnsalesPerson;
TextColumn<RenewalResult> getColumncustomerId;
TextColumn<RenewalResult> getColumncustomerName;
TextColumn<RenewalResult> getColumncustomerCell;
TextColumn<RenewalResult> getColumncity;
TextColumn<RenewalResult> getColumnlocality;
//TextColumn<RenewalResult> getColumnproductId;
//TextColumn<RenewalResult> getColumnproductName;
//TextColumn<RenewalResult> getColumnproductCode;
//TextColumn<RenewalResult> getColumnproductPrice;
//TextColumn<RenewalResult> getColumnproductNewPrice;
TextColumn<RenewalResult> getColumnduration;
TextColumn<RenewalResult> getColumnyear;
TextColumn<RenewalResult> getColumnstatus;
TextColumn<RenewalResult> getColumndate;

TextColumn<RenewalResult> getColumndesc;

//Column<RenewalResult, String> getColumnproductNewPrice1;
Column<RenewalResult, Boolean> checkRecord;
Column<RenewalResult, String> viewButtonColumn;

//Date 11 April 2017 added by vijay requirement from PCAAMB
TextColumn<RenewalResult> getColumnNetPayable;
TextColumn<RenewalResult> getColumnContractType;

/*
 * Date:15/09/2018
 * Developer:Ashwini
 * Des:To add checkbox 
 */

Header<Boolean>selectAllHeader;
Column <RenewalResult,Boolean>checkColumn;
protected GWTCGlassPanel glassPanel;

/*
 * End by Ashwini
 */


TextColumn<RenewalResult> getcolumnRenewalRemark;


public ContractRenewalTable() {
	super();
	setHeight("200px");
	
	productpopup.getLblOk().addClickHandler(this);
	productpopup.getLblCancel().addClickHandler(this);
}

@Override
public void createTable() {
	addColumnCheckBox();//Added by Ashwini
//	addColumnCheckBox(); //commented by Ashwini
	createColumnViewProductColumn();
	getColumnContractId();
	getColumncontractDate();
	getColumncontractEndDate();
	
	getColumnContractNetPayable();
	
	getColumnbranch();
	getColumnsalesPerson();
	getColumncustomerName();
	getColumncustomerCell();
	
	/**Date 10-4-2020 commented product view column by Amol and 
    move this column to 1st ,raised by Nitin Sir**/
//	createColumnViewProductColumn();	
	getColumncity();
	getColumnlocality();
	
//	getColumnproductId();
//	getColumnproductName();
//	getColumnproductCode();
//	getColumnproductPrice();
//	getColumnproductNewPrice();
//	addColumnproductNewprice1();
	
//	getColumnyear();
	getColumnstatus();
	getColumnContractType();
//	getColumndate();
	getColumndesc();
	getColumncustomerId();

//	setFieldUpdaterOnCheckBox(); //commented by Ashwini
//	setFieldUpdaterOnProductNewPrice();
	
	updateColumnViewProductColumn();
	
	getColumnRenewalRemark();
	
}





//public void addColumnproductNewprice1() {
//	EditTextCell cell = new EditTextCell();
//	getColumnproductNewPrice1 = new Column<RenewalResult, String>(cell) {
//		@Override
//		public String getValue(RenewalResult object) {
//			return object.getProductNewPrice() + "";
//		}
//	};
//	table.addColumn(getColumnproductNewPrice1, "Product New Price");
//	table.setColumnWidth(getColumnproductNewPrice1,130,Unit.PX);
//	getColumnproductNewPrice1.setSortable(true);
//}



/**
* Date 11 April 2017 added by vijay requirement from PCAAMB
* for to show contract type
*/
private void getColumnContractType() {

	getColumnContractType = new TextColumn<RenewalResult>() {
		
		@Override
		public String getValue(RenewalResult object) {
			if(object.getContractType()!=null){
				return object.getContractType();
			}else{
				return "";	
			}
		}
	};
	table.addColumn(getColumnContractType,"Contract Type");
	table.setColumnWidth(getColumnContractType, "100px");
}


/**
 * Date 11 April 2017 added by vijay requirement from PCAAMB
 * for to show net payable amount of contract
 */

private void getColumnContractNetPayable() {
	getColumnNetPayable = new TextColumn<RenewalResult>() {
		
		@Override
		public String getValue(RenewalResult object) {
			return object.getNetPayable()+"";
		}
	};
	table.addColumn(getColumnNetPayable, "Net Payable");
	table.setColumnWidth(getColumnNetPayable, "100px");
}

private void getColumndesc() {
	getColumndesc=new TextColumn<RenewalResult>() {
		@Override
		public String getValue(RenewalResult object) {
			if(object.getDescription()!=null){
				return object.getDescription();
			}
			return "";
		}
	};
	table.addColumn(getColumndesc, "Description");
	table.setColumnWidth(getColumndesc,"130px");
	getColumndesc.setSortable(true);
}

private void getColumncontractEndDate() {
	getColumncontractEndDate=new TextColumn<RenewalResult>() {
		@Override
		public String getValue(RenewalResult object) {
			return fmt.format(object.getContractEndDate())+"";
		}
	};
	table.addColumn(getColumncontractEndDate, "Contract End Date");
	table.setColumnWidth(getColumncontractEndDate,"130px");
	getColumncontractEndDate.setSortable(true);
}

private void getColumnContractId() {
	getColumnContractId=new TextColumn<RenewalResult>() {
		@Override
		public String getValue(RenewalResult object) {
			return object.getContractId()+"";
		}
	};
	table.addColumn(getColumnContractId, "Contract Id");
	table.setColumnWidth(getColumnContractId,"130px");
	getColumnContractId.setSortable(true);
}

private void getColumncontractDate() {
	getColumncontractDate=new TextColumn<RenewalResult>() {
		@Override
		public String getValue(RenewalResult object) {
			return fmt.format(object.getContractDate())+"";
		}
	};
	table.addColumn(getColumncontractDate, "Contract Date");
	table.setColumnWidth(getColumncontractDate,"130px");
	getColumncontractDate.setSortable(true);
	
}

private void getColumnbranch() {
	getColumnbranch=new TextColumn<RenewalResult>() {
		@Override
		public String getValue(RenewalResult object) {
			return object.getBranch()+"";
		}
	};
	table.addColumn(getColumnbranch, "Branch");
	table.setColumnWidth(getColumnbranch,"130px");
	getColumnbranch.setSortable(true);
}

private void getColumnsalesPerson() {
	getColumnsalesPerson=new TextColumn<RenewalResult>() {
		@Override
		public String getValue(RenewalResult object) {
			return object.getSalesPerson()+"";
		}
	};
	table.addColumn(getColumnsalesPerson, "Sales Person");
	table.setColumnWidth(getColumnsalesPerson,"130px");
	getColumnsalesPerson.setSortable(true);
}

private void getColumncustomerId() {
	getColumncustomerId=new TextColumn<RenewalResult>() {
		@Override
		public String getValue(RenewalResult object) {
			return object.getCustomerId()+"";
		}
	};
	table.addColumn(getColumncustomerId, "Customer Id");
	table.setColumnWidth(getColumncustomerId,"130px");
	getColumncustomerId.setSortable(true);
}

private void getColumncustomerName() {
	getColumncustomerName=new TextColumn<RenewalResult>() {
		@Override
		public String getValue(RenewalResult object) {
			return object.getCustomerName()+"";
		}
	};
	table.addColumn(getColumncustomerName, "Customer Name");
	table.setColumnWidth(getColumncustomerName,"130px");
	getColumncustomerName.setSortable(true);
}

private void getColumncustomerCell() {
	getColumncustomerCell=new TextColumn<RenewalResult>() {
		@Override
		public String getValue(RenewalResult object) {
			return object.getCustomerCell()+"";
		}
	};
	table.addColumn(getColumncustomerCell, "Customer Cell");
	table.setColumnWidth(getColumncustomerCell,"130px");
	getColumncustomerCell.setSortable(true);
}

private void getColumncity() {
	getColumncity=new TextColumn<RenewalResult>() {
		@Override
		public String getValue(RenewalResult object) {
			return object.getCity()+"";
		}
	};
	table.addColumn(getColumncity, "City");
	table.setColumnWidth(getColumncity,"130px");
	getColumncity.setSortable(true);
}

private void getColumnlocality() {
	getColumnlocality=new TextColumn<RenewalResult>() {
		@Override
		public String getValue(RenewalResult object) {
			return object.getLocality()+"";
		}
	};
	table.addColumn(getColumnlocality, "Locality");
	table.setColumnWidth(getColumnlocality,"130px");
	getColumnlocality.setSortable(true);
}

//private void getColumnproductId() {
//	getColumnproductId=new TextColumn<RenewalResult>() {
//		@Override
//		public String getValue(RenewalResult object) {
//			return object.getLocality()+"";
//		}
//	};
//	table.addColumn(getColumnproductId, "Product Id");
//	table.setColumnWidth(getColumnproductId,"130px");
//	getColumnproductId.setSortable(true);
//}
//
//private void getColumnproductName() {
//	getColumnproductName=new TextColumn<RenewalResult>() {
//		@Override
//		public String getValue(RenewalResult object) {
//			return object.getProductName()+"";
//		}
//	};
//	table.addColumn(getColumnproductName, "Product Name");
//	table.setColumnWidth(getColumnproductName,"130px");
//	getColumnproductName.setSortable(true);
//}
//
//private void getColumnproductCode() {
//	getColumnproductCode=new TextColumn<RenewalResult>() {
//		@Override
//		public String getValue(RenewalResult object) {
//			return object.getProductCode()+"";
//		}
//	};
//	table.addColumn(getColumnproductCode, "Product Code");
//	table.setColumnWidth(getColumnproductCode,"130px");
//	getColumnproductCode.setSortable(true);
//}
//
//private void getColumnproductPrice() {
//	getColumnproductPrice=new TextColumn<RenewalResult>() {
//		@Override
//		public String getValue(RenewalResult object) {
//			return nf.format(object.getProductPrice())+"";
//		}
//	};
//	table.addColumn(getColumnproductPrice, "Product Price");
//	table.setColumnWidth(getColumnproductPrice,"130px");
//	getColumnproductPrice.setSortable(true);
//}
//
//private void getColumnproductNewPrice() {
//	getColumnproductNewPrice=new TextColumn<RenewalResult>() {
//		@Override
//		public String getValue(RenewalResult object) {
//			return nf.format(object.getProductNewPrice())+"";
//		}
//	};
//	table.addColumn(getColumnproductNewPrice, "New Price");
//	table.setColumnWidth(getColumnproductNewPrice,"130px");
//	getColumnproductNewPrice.setSortable(true);
//}

private void getColumnyear() {
	getColumnyear=new TextColumn<RenewalResult>() {
		@Override
		public String getValue(RenewalResult object) {
			return object.getYear()+"";
		}
	};
	table.addColumn(getColumnyear, "Year");
	table.setColumnWidth(getColumnyear,"130px");
	getColumnyear.setSortable(true);
}

private void getColumnstatus() {
	getColumnstatus=new TextColumn<RenewalResult>() {
		@Override
		public String getValue(RenewalResult object) {
			return object.getStatus()+"";
		}
	};
	table.addColumn(getColumnstatus, "Status");
	table.setColumnWidth(getColumnstatus,"130px");
	getColumnstatus.setSortable(true);
}

private void getColumndate() {
	getColumndate=new TextColumn<RenewalResult>() {
		@Override
		public String getValue(RenewalResult object) {
			return fmt.format(object.getDate())+"";
		}
	};
	table.addColumn(getColumndate, "Date");
	table.setColumnWidth(getColumndate,"130px");
	getColumndate.setSortable(true);
}

/*
 * Date:15/09/2018
 * Developer:Ashwini
 * Des:to add checkbox
 */

private void addColumnCheckBox(){
	
	checkColumn= new Column<RenewalResult, Boolean>(new CheckboxCell()) {
		@Override
		public Boolean getValue(RenewalResult object) {
			return object.getRecordSelect();
		}
		
	};
	

	
	checkColumn.setFieldUpdater(new FieldUpdater<RenewalResult, Boolean>() {
		@Override
		public void update(int index, RenewalResult object, Boolean value) {
			System.out.println("Check Column ....");
			object.setRecordSelect(value);
			table.redrawRow(index);
			selectAllHeader.getValue();
//			selectAllHeader.getCell().
//			table.redrawHeaders();
			
			glassPanel = new GWTCGlassPanel();
			glassPanel.show();
			Timer timer = new Timer() {
				@Override
				public void run() {
					glassPanel.hide();
					System.out.println("12345::"+glassPanel);
				}
			};
			timer.schedule(1000); 
		}
	});
	selectAllHeader =new Header<Boolean>(new CheckboxCell()) {
		@Override
		public Boolean getValue() {
			if(getDataprovider().getList().size()!=0){
				
			}
		return false;
			
		}
	};
	selectAllHeader.setUpdater(new ValueUpdater<Boolean>(){

		@Override
		public void update(Boolean value) {
			// TODO Auto-generated method stub
			List<RenewalResult> list=getDataprovider().getList();
			for(RenewalResult object:list){
				object.setRecordSelect(value);
			}
			getDataprovider().setList(list);
			table.redraw();
			addColumnSorting();
			
		}
		
	});
	table.addColumn(checkColumn,selectAllHeader);
	table.setColumnWidth(checkColumn, 60, Unit.PX);
	}

/*
 * end by Ashwini
 */

/*
 * commented by Ashwini
 */

//private void addColumnCheckBox()
//{
//	CheckboxCell checkCell= new CheckboxCell();
//	checkRecord = new Column<RenewalResult, Boolean>(checkCell) {
//
//		@Override
//		public Boolean getValue(RenewalResult object) {
//			
//			return object.getRecordSelect();
//		}
//	};
//	table.addColumn(checkRecord,"Select");
//	table.setColumnWidth(checkRecord, 60, Unit.PX);
//}
//
////////////////////////////////// UDATER METHOD /////////////////////////////////////////
//
//private void setFieldUpdaterOnCheckBox()
//{
//	System.out.println("inside check box updator..");
//	checkRecord.setFieldUpdater(new FieldUpdater<RenewalResult, Boolean>() {
//		
//		@Override
//		public void update(int index, RenewalResult object, Boolean value) {
//			try{
//				Boolean val1=(value);
//				object.setRecordSelect(val1);
//			}
//			catch(Exception e)
//			{
//			}
//			table.redrawRow(index);
//		
//		}
//	});
//}



public void createColumnViewProductColumn() {
	ButtonCell btnCell = new ButtonCell();
	viewButtonColumn = new Column<RenewalResult, String>(btnCell) {
		@Override
		public String getValue(RenewalResult object) {
			/**Date 10-4-2020 by Amol Rename the "View Product" to "Revise"
			 * raised by Nitin Sir
			 */
			
//			return "View Product";
			return "Revise";
		}
	};
	table.addColumn(viewButtonColumn, "");
	table.setColumnWidth(viewButtonColumn,130,Unit.PX);

}

public void updateColumnViewProductColumn() {
	viewButtonColumn.setFieldUpdater(new FieldUpdater<RenewalResult, String>() {
		@Override
		public void update(int index, RenewalResult object, String value) {
			
			System.out.println("View product button click...");
//			if(getAllSelectedRecords().size()==1){
			
//				panel=new PopupPanel(true);
//				panel.add(prodTblPop);
//				prodTblPop.productList.connectToLocal();
				/**
				 * @author Vijay Date 07-12-2020
				 * Des :- updated Popup with new popup screen and some changes
				 */
				productpopup.showPopUp();
				productpopup.productList.clear();
				productpopup.productList.getTable().redraw();
				productpopup.netPayable.setValue("");
				
				productpopup.tbcontractId.setValue(object.getContractId()+"");
				
				PersonInfo personInfo = new PersonInfo();
				if(object.getCustomerName()!=null)
				personInfo.setFullName(object.getCustomerName());
				personInfo.setCount(object.getCustomerId());
				personInfo.setCellNumber(object.getCustomerCell());
				productpopup.personInfo.setValue(personInfo);
				productpopup.renewalDate.setValue(object.getDate());//Ashwini Patil Date:27-04-2023
				/**
				 * Date 28-03-2018 By Vijay for description issue
				 * Issue :- when we added description to one record that description also displays to other records issue 
				 */
				if(object.getDescription()!=null)
					productpopup.taDesc.setValue(object.getDescription());
				
				if(object.getSalesPerson()!=null&&!object.getSalesPerson().equals(""))
					productpopup.olbeSalesPerson.setValue(object.getSalesPerson());
				/**
				 * ends here
				 */
				
				List<SalesLineItem> renewallist = new ArrayList<SalesLineItem>();
				renewallist.addAll(object.getItems());
				
				for(int i=0;i<object.getItems().size();i++){
					ContractRenewalProduct product=new ContractRenewalProduct();
					product.setSrNo(object.getItems().get(i).getProductSrNo());
					product.setProductCode(object.getItems().get(i).getProductCode());
					product.setProductName(object.getItems().get(i).getProductName());
					product.setDuaration(object.getItems().get(i).getDuration());
					product.setProductPrice(object.getItems().get(i).getOldProductPrice());
					System.out.println("object.getItems().get(i).getOldProductPrice() "+object.getItems().get(i).getOldProductPrice());
					product.setProductNewPrice(object.getItems().get(i).getPrice());
					product.setProductPriceNew2(object.getItems().get(i).getPrice());
					System.out.println("object.getItems().get(i).getPrice() "+object.getItems().get(i).getPrice());
					System.out.println("object.getItems().get(i).getOldProductPrice() "+object.getItems().get(i).getOldProductPrice());
					product.setPremises(object.getItems().get(i).getPremisesDetails());
					//  rohan added this for setting number of services Date : 30 may 2017
					product.setNoOfServices(object.getItems().get(i).getNumberOfServices());
//					product.setPrduct(object.getItems().get(i).getPrduct());
//					product.getServiceTax().setTaxConfigName(object.getItems().get(i).getServiceTax().getTaxConfigName());
//					product.getVatTax().setTaxConfigName(object.getItems().get(i).getVatTax().getTaxConfigName());
					/**
					 * Date 27-03-2017
					 * added by vijay
					 */
					product.setRemark(object.getItems().get(i).getRemark());
					/**Date 29/11/2017 By :Manisha
					 * Desscription :To map the product attribute of contract renewal..!!
					 */
					
					object.getItems().get(i).getPrduct().setComment("");
					object.getItems().get(i).getPrduct().setCommentdesc("");
					object.getItems().get(i).getPrduct().setCommentdesc1("");
					object.getItems().get(i).getPrduct().setCommentdesc2("");
					object.getItems().get(i).getPrduct().setProductImage(new DocumentUpload());
					object.getItems().get(i).getPrduct().setProductImage1(new DocumentUpload());
					object.getItems().get(i).getPrduct().setProductImage2(new DocumentUpload());
					object.getItems().get(i).getPrduct().setProductImage3(new DocumentUpload());
					object.getItems().get(i).getPrduct().setTermsAndConditions(new DocumentUpload());
					
					System.out.println("object.getContractEndDate() "+object.getContractEndDate());
					
					product.setStartDate(object.getItems().get(i).getStartDate());
					product.setEndDate(object.getItems().get(i).getEndDate());
					
					product.setPrduct(object.getItems().get(i).getPrduct());
					
					/**End**/
					
					/**Date 29/11/2017 By :Manisha
					 * Desscription :Commented by Manisha to map the product attribute of contract renewal..!!
					 */
//					if(object.getItems().get(i).getPrduct() instanceof ItemProduct){
////						ItemProduct itemProduct = new ItemProduct();
////						itemProduct.setProductName(object.getItems().get(i).getProductName());
////						itemProduct.setCount(object.getItems().get(i).getPrduct().getCount());
////						itemProduct.setProductCode(object.getItems().get(i).getPrduct().getProductCode());
////						itemProduct.setProductCategory(object.getItems().get(i).getPrduct().getProductCategory());
////						itemProduct.setServiceTax(object.getItems().get(i).getPrduct().getServiceTax());
////						itemProduct.setVatTax(object.getItems().get(i).getPrduct().getVatTax());
////						itemProduct.setProductImage(new DocumentUpload());
////						itemProduct.setProductImage1(new DocumentUpload());
////						itemProduct.setProductImage2(new DocumentUpload());
////						itemProduct.setProductImage3(new DocumentUpload());
////						itemProduct.setComment("");
////						itemProduct.setCommentdesc("");
////						itemProduct.setCommentdesc1("");
////						itemProduct.setCommentdesc2("");
//						product.setPrduct(object.getItems().);
//					System.out.println("MAAAAAAAA"+object.getItems().get(i).getPrduct().getServiceTax());
//					System.out.println(object.getItems().get(i).getPrduct().getVatTax());
//					}else{
//						ServiceProduct serviceProduct = new ServiceProduct();
//						serviceProduct.setCount(object.getItems().get(i).getPrduct().getCount());
//						serviceProduct.setProductCode(object.getItems().get(i).getPrduct().getProductCode());
//						/**
//						 * Date : 22-11-2017 BY MANISHA
//						 * Description :To get the name of product after clicking on view button..!!
//						 */
//						serviceProduct.setProductName(object.getItems().get(i).getProductName());
//						/**End**/
//						serviceProduct.setProductCategory(object.getItems().get(i).getPrduct().getProductCategory());
//						serviceProduct.setServiceTax(object.getItems().get(i).getPrduct().getServiceTax());
//						serviceProduct.setVatTax(object.getItems().get(i).getPrduct().getVatTax());
//						product.setPrduct(serviceProduct);
//						System.out.println("LLLLLLLLLLLLLLLL"+object.getItems().get(i).getPrduct().getServiceTax());
//						System.out.println(object.getItems().get(i).getPrduct().getVatTax());
//					}
					
					/**End**/
					product.getServiceTax().setTaxConfigName(object.getItems().get(i).getServiceTax().getTaxConfigName());
					product.getVatTax().setTaxConfigName(object.getItems().get(i).getVatTax().getTaxConfigName());
					
					double servicetax = product.getServiceTax().getPercentage();
					double vattax = product.getVatTax().getPercentage();
					
					/**
					 * Date : 22-11-2017 BY MANISHA
					 * Description :To calculate total amount after clicking on view button..!!
					 */
					String vattaxname=product.getVatTax().getTaxPrintName();
					String sertaxname=product.getServiceTax().getTaxPrintName();
					
					/**@author Abhinav Bihade
					 * @since 15/01/2020
					 * As per Ashwini Bhagwat's Requirement On contract renewal screen Is inclusive not working
					 */
					System.out.println("object.getItems().get(i).getPrice() "+object.getItems().get(i).getPrice());
					
					double tax =removeAllTaxes(product.getPrduct());
					double amt = product.getProductNewPrice() - tax;
					System.out.println("amtamtamt"+amt);
					double total = ContractRenewalProductTable.calculateTotalAmt(vattaxname,sertaxname,amt,servicetax	,vattax);
					System.out.println("object.getItems().get(i).isRevisedpriceflag() "+object.getItems().get(i).isRevisedpriceflag());
					if(object.getItems().get(i).isRevisedpriceflag()){
						
						
						product.setNetPayable(object.getItems().get(i).getRenewalProductNetpayable());

					}
					else{
						/**
						 * @author Vijay Date -15-05-2023
						 * Des updated code to resolve icnlusive issue for pecopp
						 */
						if(object.getItems().get(i).isInclusive())//condition added on 16-07-2024 as old prince after discount was getting overwritten here
							product.setProductPrice(amt);
						product.setProductNewPrice(amt);
						product.setProductPriceNew2(amt);
						
						/**
						 * ends here
						 */
						System.out.println("total "+total);
						product.setNetPayable(total);
					}
					
					

					productpopup.productList.getDataprovider().getList().add(product);
				}
//				
				
//				
//				panel.show();
//				panel.center();
				rowIndex=index;
//			}else if(getAllSelectedRecords().size()>1){
//				ContractRenewalPresenter.showMsg("Please select only one record!");
//			}
//			else{
//				ContractRenewalPresenter.showMsg("Please select record!");
//			}
			
		}
	});
}

 public List<RenewalResult> getAllSelectedRecords()
 {
	 List<RenewalResult> lisbillingtable=getDataprovider().getList();
	 ArrayList<RenewalResult> selectedlist=new ArrayList<RenewalResult>();
	 for(RenewalResult doc:lisbillingtable)
	 {
		 if(doc.getRecordSelect()==true)
			 selectedlist.add(doc);
	 }
	 return selectedlist;
 }


//private void setFieldUpdaterOnProductNewPrice()
//{
//	System.out.println("inside new product price updator..");
//	getColumnproductNewPrice1.setFieldUpdater(new FieldUpdater<RenewalResult, String>() {
//		
//		@Override
//		public void update(int index, RenewalResult object, String value) {
//			try{
//				double val1=Double.parseDouble(value);
//				
//				object.setProductNewPrice(val1);
//			}
//			catch(Exception e)
//			{
//			}
//			table.redrawRow(index);
//		
//		}
//	});
//}

///////////////////////////////// SORTING METHODS ////////////////////////////////////

public void addColumnSorting() {
	addColumnSortingContractId();
	addColumnSortingcontractDate();
	addColumnSortingcontractEndDate();
	addColumnSortingbranch();
	addColumnSortingsalesPerson();
	addColumnSortingcustomerId();
	addColumnSortingcustomerCell();
	addColumnSortingcity();
	addColumnSortinglocality();
//	addColumnSortingproductId();
//	addColumnSortingproductName();
//	addColumnSortingproductCode();
//	addColumnSortingproductPrice();
//	addColumnSortingproductNewPrice();
	addColumnSortingduration();
	addColumnSortingyear();
	addColumnSortingstatus();
}

private void addColumnSortingContractId() {
	List<RenewalResult> list = getDataprovider().getList();
	columnSort = new ListHandler<RenewalResult>(list);
	columnSort.setComparator(getColumnContractId, new Comparator<RenewalResult>() {
		@Override
		public int compare(RenewalResult e1, RenewalResult e2) {
			if (e1 != null && e2 != null) {
				if (e1.getContractId() == e2.getContractId()) {
					return 0;
				}
				if (e1.getContractId() > e2.getContractId()) {
					return 1;
				} else {
					return -1;
				}
			} else {
				return 0;
			}
		}
	});
	table.addColumnSortHandler(columnSort);
}

private void addColumnSortingcontractDate() {
	List<RenewalResult> list = getDataprovider().getList();
	columnSort = new ListHandler<RenewalResult>(list);
	columnSort.setComparator(getColumncontractDate, new Comparator<RenewalResult>() {
		@Override
		public int compare(RenewalResult e1, RenewalResult e2) {
			if (e1 != null && e2 != null) {
				if (e1.getContractDate() != null && e2.getContractDate() != null) {
					return e1.getContractDate().compareTo(e2.getContractDate());
				}
			} else {
				return 0;
			}
			return 0;
		}
	});
	table.addColumnSortHandler(columnSort);
}

private void addColumnSortingcontractEndDate() {
	List<RenewalResult> list = getDataprovider().getList();
	columnSort = new ListHandler<RenewalResult>(list);
	columnSort.setComparator(getColumncontractEndDate, new Comparator<RenewalResult>() {
		@Override
		public int compare(RenewalResult e1, RenewalResult e2) {
			if (e1 != null && e2 != null) {
				if (e1.getContractEndDate() != null && e2.getContractEndDate() != null) {
					return e1.getContractEndDate().compareTo(e2.getContractEndDate());
				}
			} else {
				return 0;
			}
			return 0;
		}
	});
	table.addColumnSortHandler(columnSort);
}

private void addColumnSortingbranch() {
	List<RenewalResult> list = getDataprovider().getList();
	columnSort = new ListHandler<RenewalResult>(list);
	columnSort.setComparator(getColumnbranch, new Comparator<RenewalResult>() {
		@Override
		public int compare(RenewalResult e1, RenewalResult e2) {
			if (e1 != null && e2 != null) {
				if (e1.getBranch() != null && e2.getBranch() != null) {
					return e1.getBranch().compareTo(e2.getBranch());
				}
			} else {
				return 0;
			}
			return 0;
		}
	});
	table.addColumnSortHandler(columnSort);
}

private void addColumnSortingsalesPerson() {
	List<RenewalResult> list = getDataprovider().getList();
	columnSort = new ListHandler<RenewalResult>(list);
	columnSort.setComparator(getColumnsalesPerson, new Comparator<RenewalResult>() {
		@Override
		public int compare(RenewalResult e1, RenewalResult e2) {
			if (e1 != null && e2 != null) {
				if (e1.getSalesPerson() != null && e2.getSalesPerson() != null) {
					return e1.getSalesPerson().compareTo(e2.getSalesPerson());
				}
			} else {
				return 0;
			}
			return 0;
		}
	});
	table.addColumnSortHandler(columnSort);
}

private void addColumnSortingcustomerId() {
	List<RenewalResult> list = getDataprovider().getList();
	columnSort = new ListHandler<RenewalResult>(list);
	columnSort.setComparator(getColumnContractId, new Comparator<RenewalResult>() {
		@Override
		public int compare(RenewalResult e1, RenewalResult e2) {
			if (e1 != null && e2 != null) {
				if (e1.getContractId() == e2.getContractId()) {
					return 0;
				}
				if (e1.getContractId() > e2.getContractId()) {
					return 1;
				} else {
					return -1;
				}
			} else {
				return 0;
			}
		}
	});
	table.addColumnSortHandler(columnSort);
}

private void addColumnSortingcustomerCell() {
	List<RenewalResult> list = getDataprovider().getList();
	columnSort = new ListHandler<RenewalResult>(list);
	columnSort.setComparator(getColumncustomerCell, new Comparator<RenewalResult>() {
		@Override
		public int compare(RenewalResult e1, RenewalResult e2) {
			if (e1 != null && e2 != null) {
				if (e1.getCustomerCell()== e2.getCustomerCell()) {
					return 0;
				}
				if (e1.getCustomerCell() > e2.getCustomerCell()) {
					return 1;
				} else {
					return -1;
				}
			} else {
				return 0;
			}
		}
	});
	table.addColumnSortHandler(columnSort);
}

private void addColumnSortingcity() {
	List<RenewalResult> list = getDataprovider().getList();
	columnSort = new ListHandler<RenewalResult>(list);
	columnSort.setComparator(getColumncity, new Comparator<RenewalResult>() {
		@Override
		public int compare(RenewalResult e1, RenewalResult e2) {
			if (e1 != null && e2 != null) {
				if (e1.getCity() != null && e2.getCity() != null) {
					return e1.getCity().compareTo(e2.getCity());
				}
			} else {
				return 0;
			}
			return 0;
		}
	});
	table.addColumnSortHandler(columnSort);
}

private void addColumnSortinglocality() {
	List<RenewalResult> list = getDataprovider().getList();
	columnSort = new ListHandler<RenewalResult>(list);
	columnSort.setComparator(getColumnlocality, new Comparator<RenewalResult>() {
		@Override
		public int compare(RenewalResult e1, RenewalResult e2) {
			if (e1 != null && e2 != null) {
				if (e1.getLocality() != null && e2.getLocality() != null) {
					return e1.getLocality().compareTo(e2.getLocality());
				}
			} else {
				return 0;
			}
			return 0;
		}
	});
	table.addColumnSortHandler(columnSort);
}

//private void addColumnSortingproductId() {
//	List<RenewalResult> list = getDataprovider().getList();
//	columnSort = new ListHandler<RenewalResult>(list);
//	columnSort.setComparator(getColumnproductId, new Comparator<RenewalResult>() {
//		@Override
//		public int compare(RenewalResult e1, RenewalResult e2) {
//			if (e1 != null && e2 != null) {
//				if (e1.getProductId() == e2.getProductId()) {
//					return 0;
//				}
//				if (e1.getProductId() > e2.getProductId()) {
//					return 1;
//				} else {
//					return -1;
//				}
//			} else {
//				return 0;
//			}
//		}
//	});
//	table.addColumnSortHandler(columnSort);
//}
//
//private void addColumnSortingproductName() {
//	List<RenewalResult> list = getDataprovider().getList();
//	columnSort = new ListHandler<RenewalResult>(list);
//	columnSort.setComparator(getColumnproductName, new Comparator<RenewalResult>() {
//		@Override
//		public int compare(RenewalResult e1, RenewalResult e2) {
//			if (e1 != null && e2 != null) {
//				if (e1.getProductName() != null && e2.getProductName() != null) {
//					return e1.getProductName().compareTo(e2.getProductName());
//				}
//			} else {
//				return 0;
//			}
//			return 0;
//		}
//	});
//	table.addColumnSortHandler(columnSort);
//}
//
//private void addColumnSortingproductCode() {
//	List<RenewalResult> list = getDataprovider().getList();
//	columnSort = new ListHandler<RenewalResult>(list);
//	columnSort.setComparator(getColumnproductCode, new Comparator<RenewalResult>() {
//		@Override
//		public int compare(RenewalResult e1, RenewalResult e2) {
//			if (e1 != null && e2 != null) {
//				if (e1.getProductCode() != null && e2.getProductCode() != null) {
//					return e1.getProductCode().compareTo(e2.getProductCode());
//				}
//			} else {
//				return 0;
//			}
//			return 0;
//		}
//	});
//	table.addColumnSortHandler(columnSort);
//}
//
//private void addColumnSortingproductPrice() {
//	List<RenewalResult> list = getDataprovider().getList();
//	columnSort = new ListHandler<RenewalResult>(list);
//	columnSort.setComparator(getColumnproductPrice, new Comparator<RenewalResult>() {
//		@Override
//		public int compare(RenewalResult e1, RenewalResult e2) {
//			if (e1 != null && e2 != null) {
//				if (e1.getProductPrice() == e2.getProductPrice()) {
//					return 0;
//				}
//				if (e1.getProductPrice() > e2.getProductPrice()) {
//					return 1;
//				} else {
//					return -1;
//				}
//			} else {
//				return 0;
//			}
//		}
//	});
//	table.addColumnSortHandler(columnSort);
//}
//
//private void addColumnSortingproductNewPrice() {
//	List<RenewalResult> list = getDataprovider().getList();
//	columnSort = new ListHandler<RenewalResult>(list);
//	columnSort.setComparator(getColumnproductNewPrice, new Comparator<RenewalResult>() {
//		@Override
//		public int compare(RenewalResult e1, RenewalResult e2) {
//			if (e1 != null && e2 != null) {
//				if (e1.getProductNewPrice() == e2.getProductNewPrice()) {
//					return 0;
//				}
//				if (e1.getProductNewPrice() > e2.getProductNewPrice()) {
//					return 1;
//				} else {
//					return -1;
//				}
//			} else {
//				return 0;
//			}
//		}
//	});
//	table.addColumnSortHandler(columnSort);
//}

private void addColumnSortingduration() {
	List<RenewalResult> list = getDataprovider().getList();
	columnSort = new ListHandler<RenewalResult>(list);
	columnSort.setComparator(getColumnduration, new Comparator<RenewalResult>() {
		@Override
		public int compare(RenewalResult e1, RenewalResult e2) {
			if (e1 != null && e2 != null) {
				if (e1.getDuration() == e2.getDuration()) {
					return 0;
				}
				if (e1.getDuration() > e2.getDuration()) {
					return 1;
				} else {
					return -1;
				}
			} else {
				return 0;
			}
		}
	});
	table.addColumnSortHandler(columnSort);
}

private void addColumnSortingyear() {
	List<RenewalResult> list = getDataprovider().getList();
	columnSort = new ListHandler<RenewalResult>(list);
	columnSort.setComparator(getColumnyear, new Comparator<RenewalResult>() {
		@Override
		public int compare(RenewalResult e1, RenewalResult e2) {
			if (e1 != null && e2 != null) {
				if (e1.getYear() != null && e2.getYear() != null) {
					return e1.getYear().compareTo(e2.getYear());
				}
			} else {
				return 0;
			}
			return 0;
		}
	});
	table.addColumnSortHandler(columnSort);
}

private void addColumnSortingstatus() {
	List<RenewalResult> list = getDataprovider().getList();
	columnSort = new ListHandler<RenewalResult>(list);
	columnSort.setComparator(getColumnstatus, new Comparator<RenewalResult>() {
		@Override
		public int compare(RenewalResult e1, RenewalResult e2) {
			if (e1 != null && e2 != null) {
				if (e1.getStatus() != null && e2.getStatus() != null) {
					return e1.getStatus().compareTo(e2.getStatus());
				}
			} else {
				return 0;
			}
			return 0;
		}
	});
	table.addColumnSortHandler(columnSort);
}






@Override
protected void initializekeyprovider() {
	
}

@Override
public void addFieldUpdater() {
	updateColumnViewProductColumn();
}

@Override
public void setEnable(boolean state) {
	
}

@Override
public void applyStyle() {
	
}

@Override
public void onClick(ClickEvent event) {
	if(event.getSource()==productpopup.getLblOk()){
		
		System.out.println("onclick on ok button");			
		
		ArrayList<RenewalResult> list=new ArrayList<RenewalResult>();
		if(productpopup.productList.getDataprovider().getList().size()!=0){
			list.addAll(getDataprovider().getList());
			List<ContractRenewalProduct> prodList=productpopup.productList.getDataprovider().getList();
			String desc="";
			
			if(productpopup.taDesc.getValue()!=null){
				System.out.println("DESCRIPTION NOT NULL ...!!!"+productpopup.taDesc.getValue());
				desc=productpopup.taDesc.getValue();
			}
			
			
			
			for(int i=0;i<prodList.size();i++){
				for(int j=0;j<list.get(rowIndex).getItems().size();j++){
					System.out.println("R product list size"+prodList.get(i).getSrNo());
					System.out.println("R list item size"+list.get(rowIndex).getItems().get(j).getProductSrNo());
					
					if(prodList.get(i).getSrNo()==list.get(rowIndex).getItems().get(j).getProductSrNo()){
						
						System.out.println("in side condition "+prodList.get(i).getSrNo());
						System.out.println("R list item size"+list.get(rowIndex).getItems().get(j).getProductSrNo());
						
						
						list.get(rowIndex).setDescription(desc);
						list.get(rowIndex).getItems().get(j).setPremisesDetails(prodList.get(i).getPremises());
//						list.get(rowIndex).getItems().get(j).setServiceTax(prodList.get(i).getServiceTax());
//						list.get(rowIndex).getItems().get(j).setVatTax(prodList.get(i).getVatTax());
						list.get(rowIndex).getItems().get(j).setPrduct(prodList.get(i).getPrduct());

						//vijay added below on 29-03-2017 as per new changes
						list.get(rowIndex).getItems().get(j).setPrice(prodList.get(i).getProductNewPrice());
						list.get(rowIndex).getItems().get(j).setRemark(prodList.get(i).getRemark());
						list.get(rowIndex).getItems().get(j).setDuration(prodList.get(i).getDuaration());
						//   rohan added this code for setting number of services Date : 30 may 2017 
						list.get(rowIndex).getItems().get(j).setNumberOfService(prodList.get(i).getNoOfServices());
						/**
						 * Date : 05-12-2017 By ANIL
						 * setting updated old price 
						 */
						list.get(rowIndex).getItems().get(j).setOldProductPrice(prodList.get(i).getProductPrice());
						System.out.println("prodList.get(i).getProductPrice() "+prodList.get(i).getProductPrice());
						/**
						 * End
						 */
						
						if(ContractRenewalProductTable.contractdateupdateFlag) {
							System.out.println("prodList.get(i).getStartDate() "+prodList.get(i).getStartDate());
							System.out.println("prodList.get(i).getEndDate() "+prodList.get(i).getEndDate());
							list.get(rowIndex).getItems().get(j).setStartDate(prodList.get(i).getStartDate());
							list.get(rowIndex).getItems().get(j).setEndDate(prodList.get(i).getEndDate());
						}
						
						if(productpopup.olbeSalesPerson.getSelectedIndex()!=0){
							list.get(rowIndex).setSalesPerson(productpopup.olbeSalesPerson.getValue(productpopup.olbeSalesPerson.getSelectedIndex()));			
						}
						if(productpopup.renewalDate.getValue()!=null){
							list.get(rowIndex).setDate(productpopup.renewalDate.getValue());						
						}
						
						list.get(rowIndex).getItems().get(j).setRevisedpriceflag(true);
						list.get(rowIndex).getItems().get(j).setContractRenewalRevisedPercentage(prodList.get(i).getPercentage());
						list.get(rowIndex).getItems().get(j).setRenewalProductNetpayable(prodList.get(i).getNetPayable());
						System.out.println("prodList.get(i).getNetPayable() "+prodList.get(i).getNetPayable());
					}
					//   rohan added this code 
//					else
//					{
//						list.get(rowIndex).getItems().get(j).setPrice(prodList.get(i).getProductNewPrice());
//						list.get(rowIndex).setDescription(desc);
//						list.get(rowIndex).getItems().get(j).setPremisesDetails(prodList.get(i).getPremises());
////						list.get(rowIndex).getItems().get(j).setServiceTax(prodList.get(i).getServiceTax());
////						list.get(rowIndex).getItems().get(j).setVatTax(prodList.get(i).getVatTax());
//						list.get(rowIndex).getItems().get(j).setPrduct(prodList.get(i).getPrduct());
					
//					//vijay added below on 29-03-2017 as per new changes
//					list.get(rowIndex).getItems().get(j).setPrice(prodList.get(i).getProductNewPrice());
//					list.get(rowIndex).getItems().get(j).setRemark(prodList.get(i).getRemark());
//					list.get(rowIndex).getItems().get(j).setDuration(prodList.get(i).getDuaration());
//					}
				}
			}
			
			// Date 11 April 2017 added by vijay requirement from PCAAMB for contract Amount
//			list.get(rowIndex).setNetPayable(prodList.get(0).getNetPayable());
			
			/**Date : 22-11-2017 BY MANISHA
			 * Description :To get the Net payable amount in renewal table..!!
			 */
			if(productpopup.getNetPayable().getValue()!=null&&!productpopup.getNetPayable().getValue().equals("")){
				list.get(rowIndex).setNetPayable(Double.parseDouble(productpopup.getNetPayable().getValue().trim()));
			}
			/**end**/
			
			/**Added by sheetal:17-12-2021**/
			Date contractEndDate = getContractEndDate(list.get(rowIndex).getItems());
			list.get(rowIndex).setContractEndDate(contractEndDate);
			Date contractDate = getContractDate(list.get(rowIndex).getItems());
			list.get(rowIndex).setContractDate(contractDate);
			getDataprovider().getList().clear();
			getDataprovider().getList().addAll(list);
		}
		
		if(!list.get(rowIndex).getStatus().equals(Contract.APPROVED)){
			System.out.println("ON POPUP OK BUTTON STATUS IS NOT APPROVED !");
			System.out.println("CONTRACT ID :: "+list.get(rowIndex).getContractId());
			System.out.println("CONTRACT price :: "+list.get(rowIndex).getItems().get(0).getPrice());
			
			ContractRenewalPresenter.saveProductDetailsOfTable(list.get(rowIndex));
		}
		
		productpopup.hidePopUp();

	}
	
	if(event.getSource()==productpopup.getLblCancel()){
		productpopup.hidePopUp();
	}
	
}

/**@author Abhinav Bihade
 * @since 15/01/2020
 * As per Ashwini Bhagwat Requirement On contract renewal screen Is inclusive not working
 */
public double removeAllTaxes(SuperProduct entity) {
	double vat = 0, service = 0;
	double tax = 0, retrVat = 0, retrServ = 0;
	if (entity instanceof ServiceProduct) {
		ServiceProduct prod = (ServiceProduct) entity;
		if (prod.getServiceTax() != null
				&& prod.getServiceTax().isInclusive() == true) {
			service = prod.getServiceTax().getPercentage();
		}
		if (prod.getVatTax() != null
				&& prod.getVatTax().isInclusive() == true) {
			vat = prod.getVatTax().getPercentage();
		}
	}

	if (entity instanceof ItemProduct) {
		ItemProduct prod = (ItemProduct) entity;
		if (prod.getVatTax() != null
				&& prod.getVatTax().isInclusive() == true) {
			vat = prod.getVatTax().getPercentage();
		}
		if (prod.getServiceTax() != null
				&& prod.getServiceTax().isInclusive() == true) {
			service = prod.getServiceTax().getPercentage();
		}
	}

	
	if (vat != 0 && service == 0) {
		retrVat = entity.getPrice() / (1 + (vat / 100));
		retrVat = entity.getPrice() - retrVat;
	}
	if (service != 0 && vat == 0) {
		retrServ = entity.getPrice() / (1 + service / 100);
		retrServ = entity.getPrice() - retrServ;
	}
	if (service != 0 && vat != 0) {
//		// Here if both are inclusive then first remove service tax and then
//		// on that amount
//		// calculate vat.
//		double removeServiceTax = (entity.getPrice() / (1 + service / 100));
//
//		// double taxPerc=service+vat;
//		// retrServ=(entity.getPrice()/(1+taxPerc/100)); //line changed
//		// below
//		retrServ = (removeServiceTax / (1 + vat / 100));
//		retrServ = entity.getPrice() - retrServ;
		
		
		if(entity instanceof ItemProduct)
		{
			ItemProduct prod=(ItemProduct) entity;
			if(prod.getServiceTax().getTaxPrintName()!=null && ! prod.getServiceTax().getTaxPrintName().equals("")
			   && prod.getVatTax().getTaxPrintName()!=null && ! prod.getVatTax().getTaxPrintName().equals(""))
			{

				double dot = service + vat;
				retrServ=(entity.getPrice()/(1+dot/100));
				retrServ=entity.getPrice()-retrServ;
//				retrVat=0;

			}
			else{
				// Here if both are inclusive then first remove service tax and then on that amount
				// calculate vat.
				double removeServiceTax=(entity.getPrice()/(1+service/100));
				//double taxPerc=service+vat;
				//retrServ=(entity.getPrice()/(1+taxPerc/100)); //line changed below
				retrServ=(removeServiceTax/(1+vat/100));
				retrServ=entity.getPrice()-retrServ;
			}
		}
		
		if(entity instanceof ServiceProduct)
		{
			ServiceProduct prod=(ServiceProduct) entity;
			if(prod.getServiceTax().getTaxPrintName()!=null && ! prod.getServiceTax().getTaxPrintName().equals("")
			   && prod.getVatTax().getTaxPrintName()!=null && ! prod.getVatTax().getTaxPrintName().equals(""))
			{
				double dot = service + vat;
				retrServ=(entity.getPrice()/(1+dot/100));
				retrServ=entity.getPrice()-retrServ;
			}
			else
			{
				// Here if both are inclusive then first remove service tax and then on that amount
				// calculate vat.
				double removeServiceTax=(entity.getPrice()/(1+service/100));
				//double taxPerc=service+vat;
				//retrServ=(entity.getPrice()/(1+taxPerc/100)); //line changed below
				retrServ=(removeServiceTax/(1+vat/100));
				retrServ=entity.getPrice()-retrServ;
			}
		}
	}
	tax = retrVat + retrServ;
	return tax;
}
        /**Added by sheetal:17-12-2021**/ 
		private Date getContractEndDate(List<SalesLineItem> items) {
			Date endDate = items.get(0).getEndDate();
			Date Date = items.get(0).getEndDate();
			for(SalesLineItem salesline : items){
				if(salesline.getEndDate().after(Date)){
					endDate = salesline.getEndDate();
				}
			}
			return endDate;
		}
        
		private Date getContractDate(List<SalesLineItem> items) {
			Date startDate = items.get(0).getStartDate();
			Date Date = items.get(0).getStartDate();
			for(SalesLineItem salesline : items){
				if(salesline.getStartDate().before(Date)){
					startDate = salesline.getStartDate();
				}
			}
			return startDate;
		}
		
		private void getColumnRenewalRemark() {

			getcolumnRenewalRemark = new TextColumn<RenewalResult>() {
				
				@Override
				public String getValue(RenewalResult object) {
					if(object.getNonRenewalRemark()!=null){
						return object.getNonRenewalRemark();
					}
					else{
						return "";
					}
				}
			};
			table.addColumn(getcolumnRenewalRemark,"Remark");
			table.setColumnWidth(getcolumnRenewalRemark, "100px");
		}
		
}
