

package com.slicktechnologies.client.views.project.concproject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.reusabledata.ConditionDialogBox;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.utility.AppConnector;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.account.expensemanagment.ExpensemanagmentForm;
import com.slicktechnologies.client.views.account.expensemanagment.ExpensemanagmentPresenter;
import com.slicktechnologies.client.views.account.multipleexpensemanagment.MultipleExpensemanagmentForm;
import com.slicktechnologies.client.views.account.multipleexpensemanagment.MultipleExpensemanagmentPresenter;
import com.slicktechnologies.client.views.inventory.materialissuenote.MaterialIssueNoteForm;
import com.slicktechnologies.client.views.inventory.materialissuenote.MaterialIssueNotePresenter;
import com.slicktechnologies.client.views.inventory.materialmovementnote.MaterialMovementNotePresenter;
import com.slicktechnologies.client.views.inventory.materialmovementnote.MaterialMovementNoteForm;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.inventory.MaterialProduct;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.productgroup.ProductGroupDetails;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.servicerelated.ClientSideAsset;
import com.slicktechnologies.shared.common.servicerelated.CompanyAsset;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;
import com.slicktechnologies.shared.common.servicerelated.ServicedClientSideAsset;

// Referenced classes of package com.slicktechnologies.client.views.project.concProject:
//            ProjectForm, ProjectPresenterTableProxy, ProjectPresenterSearchProxy

public class ProjectPresenter extends FormTableScreenPresenter<ServiceProject> implements ValueChangeHandler<String>, ChangeHandler
{
	    public  ProjectForm form;
	    CsvServiceAsync csvservice;
	    GenricServiceAsync service;
	    
	    List<CompanyAsset> companyAssetList = new ArrayList<CompanyAsset>(); 
	    List<MaterialProduct> mplist=new ArrayList<MaterialProduct>();
	    List<MaterialProduct> productWithOneWareHouse=new ArrayList<MaterialProduct>();
	    ReturnMaterialPopUp popup = new ReturnMaterialPopUp();
	    PopupPanel panel,conditionPopuppanel ;
	    
	    /**
	     *   rohan added this for NBHC With process configuration
	     *   Date : 19/01/2017
	     *   
	     */
	    
	    ConditionDialogBox conditionPopup = new ConditionDialogBox(" ", AppConstants.YES, AppConstants.NO);
	    
	    
	    /**
	     * ends here 
	     */
	    
	    final HashMap<Integer, ArrayList<ProductInventoryViewDetails>> map = new HashMap<Integer, ArrayList<ProductInventoryViewDetails>>();
	    
	    public ProjectPresenter(FormTableScreen<ServiceProject> view, ServiceProject model)
	    {
	        super(view, model);
	        csvservice = (CsvServiceAsync)GWT.create(CsvService.class);
	        service = (GenricServiceAsync)GWT.create(GenricService.class);
	        form = (ProjectForm)view;
	        form.getSupertable().connectToLocal();
//	        form.retriveTable(getProjectQuery());
	        form.setPresenter(this);
	        form.serviceId.addValueChangeHandler(this);
	        
	        popup.getMinID().addChangeHandler(this);
	        popup.getBtnOk().addClickHandler(this);
			popup.getBtnCancel().addClickHandler(this);
			
			//**********rohan ************************
			conditionPopup.getBtnOne().addClickHandler(this);
			conditionPopup.getBtnTwo().addClickHandler(this);
	        
	        boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.PROJECT,LoginPresenter.currentModule.trim());
			if(isDownload==false){
				form.getSearchpopupscreen().getDwnload().setVisible(false);
			}
	    }
	    
	    
	    public ProjectPresenter(FormTableScreen<ServiceProject> view, ServiceProject model,MyQuerry querry)
	    {
	        super(view, model);
	        csvservice = (CsvServiceAsync)GWT.create(CsvService.class);
	        service = (GenricServiceAsync)GWT.create(GenricService.class);
	        form = (ProjectForm)view;
	        
	        form.getSupertable().connectToLocal();
	        form.retriveTable(querry);
	        
	        form.setPresenter(this);
	        form.serviceId.addValueChangeHandler(this);
	        
	        popup.getMinID().addChangeHandler(this);
	        popup.getBtnOk().addClickHandler(this);
			popup.getBtnCancel().addClickHandler(this);
			
			//**********rohan ************************
			conditionPopup.getBtnOne().addClickHandler(this);
			conditionPopup.getBtnTwo().addClickHandler(this);
	        
	        boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.PROJECT,LoginPresenter.currentModule.trim());
			if(isDownload==false){
				form.getSearchpopupscreen().getDwnload().setVisible(false);
			}
	    }
	    
	    public ProjectPresenter(FormTableScreen<ServiceProject> view, ServiceProject model,int contractid)
	    {
	        super(view, model);
	        csvservice = (CsvServiceAsync)GWT.create(CsvService.class);
	        service = (GenricServiceAsync)GWT.create(GenricService.class);
	        form = (ProjectForm)view;
	        form.getSupertable().connectToLocal();
	        form.retriveTable(getProjectQueryOnContract(contractid));
	        form.setPresenter(this);
	        form.serviceId.addValueChangeHandler(this);
	        
	        popup.getMinID().addChangeHandler(this);
	        popup.getBtnOk().addClickHandler(this);
			popup.getBtnCancel().addClickHandler(this);
			
			
			//**********rohan ************************
			conditionPopup.getBtnOne().addClickHandler(this);
			conditionPopup.getBtnTwo().addClickHandler(this);
	        
	        boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.PROJECT,LoginPresenter.currentModule.trim());
			if(isDownload==false){
				form.getSearchpopupscreen().getDwnload().setVisible(false);
			}
	    }
	  
   

    public void reactToProcessBarEvents(ClickEvent e)
    {
        InlineLabel lbl = (InlineLabel)e.getSource();
        if(lbl.getText().contains("Delete") && form.validate())
        {
            List<ServiceProject> data = form.getSupertable().getDataprovider().getList();
            for(int i = 0; i < data.size(); i++)
            {
                if(((ServiceProject)data.get(i)).getId().equals(((ServiceProject)model).getId()))
                    data.remove(i);
                form.getSupertable().getDataprovider().refresh();
            }

            reactOnDelete();
        }
        if(lbl.getText().contains("New"))
        {
           reactToNew();
        }
        
        if(lbl.getText().contains("Mark Completed"))
        {
          if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("ServiceProject","MAKEMATERIALISSUEMANDATORY"))
          {
        	  checkforMaterialIssuedOrNot("Mark Completed",lbl);
          }
          else
          {
        		  markCompleted();  
        	
          }
           
        }
       
        if(lbl.getText().contains(AppConstants.ISSUEMATERIAL))
        {	
        	if(form.getMaterialProdTable().getDataprovider().getList().size()==0)
      	  {
      		  form.showDialogMessage("Please Add Required Material for Issue..!!");
      	  }
      	  else
      	  {
//      		if(checkForProcessConfigurartionIsActiveOrNot("ServiceProject","MAKEMATERIALISSUEMANDATORY"))
//            {
      		  
      		  	/**
				 * Date : 11-02-2017 By Rohan
				 * changing label from Issue material to processing
				 * so if user clicks that button twice then it will not process for second time
				 */
      		  		lbl.setText("Processing.....");
				/**
				 * End
				 */
      		  
      		  
          	  checkforMaterialIssuedOrNot(AppConstants.ISSUEMATERIAL,lbl);
//            }
      	  }
        }
        if(lbl.getText().contains(AppConstants.RETURNMATERIAL))
        {	
        	showPopupWithListBox();
        }
        
        if(lbl.getText().contains("Register Expense")){
        	
        	System.out.println(" hi vijay");
        	getExpenseManagement();
        }
    }
    
    
    private void checkforMaterialIssuedOrNot(final String typeOfButton, final InlineLabel lbl)
    {
    	
    	System.out.println("in side check formaterial method ");
    	
    	MyQuerry querry = new MyQuerry();
	  	Company c = new Company();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
	  	filter = new Filter();
	  	filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("serviceId");
		filter.setIntValue(Integer.parseInt(form.getServiceId().getValue().trim()));
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new MaterialIssueNote());
		
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				
				System.out.println("rohan result  size"+result.size());
				
				int approved =0;
				int created =0;
				int request=0;
				int cancel =0;
				int reject=0;
				if(result.size() == 0)
				{
					if(typeOfButton.equals("Mark Completed"))
					{
						form.showDialogMessage("Please issue material");
					}
					else if(typeOfButton.equals(AppConstants.ISSUEMATERIAL))
					{
						getmaterialfromServiceProjectForm();
			        	getMaterialProductFromToolsList();
			        	getdefaultwarehousedetails();
					}
				}
				else
				{
					for(SuperModel model:result)
					{
						MaterialIssueNote min =(MaterialIssueNote)model;
					
						
						if(min.getStatus().equalsIgnoreCase("Approved"))
						{
							approved = approved +1;
						}
						
						else if(min.getStatus().equalsIgnoreCase("Created"))
						{
							created = created +1;
						}
						
						else if(min.getStatus().equalsIgnoreCase("Requested"))
						{
							request = request +1;
						}
						else if(min.getStatus().equalsIgnoreCase("Cancelled"))
						{
							cancel = cancel +1;
						}
						else if(min.getStatus().equalsIgnoreCase("Rejected"))
						{
							reject = reject +1;
						}
						
					}
				}
				
				if(typeOfButton.equals("Mark Completed"))
				{
					if(reject > 0){
						form.showDialogMessage("Your MIN in rejected status against project");
					}
					else if(created > 0)
					{
						form.showDialogMessage("Please issue material as MIN is peinding against project");
					}
					else if(request > 0)
					{
						form.showDialogMessage("Please approve MIN as it is in requested state");
					}
					else if(approved >0)
					{
						markCompleted();
					}
					else
					{
						form.showDialogMessage("Please issue material");
					}
				}
				else if(typeOfButton.equals(AppConstants.ISSUEMATERIAL))
				{
					
					if(reject > 0){
						form.showDialogMessage("Your MIN in rejected status against project");
					}
					else if(created > 0)
					{
						form.showDialogMessage("You have pending MIN againts this service. Approved that first..!");
					}
					else if(approved >0)
					{
						System.out.println("in side issue material");
						
//						conditionPopup=new ConditionDialogBox("You have "+approved+" approved MIN against this service..Do you want to continue ?",AppConstants.YES,AppConstants.NO);
						conditionPopup.getDisplayMessage().setText("You have "+approved+" approved MIN against this service..Do you want to continue ?");
						conditionPopuppanel=new PopupPanel(true);
						conditionPopuppanel.add(conditionPopup);
						conditionPopuppanel.setGlassEnabled(true);
						conditionPopuppanel.show();
						conditionPopuppanel.center();
					}else if(request > 0)
					{
						form.showDialogMessage("Please approve MIN as it is in requested state");
					}
					else if(cancel >0){
						getmaterialfromServiceProjectForm();
			        	getMaterialProductFromToolsList();
			        	getdefaultwarehousedetails();
					}
					
					
				}
				
			}
		});
		
    }
    
  

	private void getExpenseManagement() {

		System.out.println(" in side method");
		if(AppConnector.cheackForExpense()){
			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Expense Management", Screen.EXPENSEMANAGMENT);
			ExpensemanagmentPresenter.initalize();
			final ExpensemanagmentForm form=ExpensemanagmentPresenter.form;
			form.setToNewState();
			Timer t = new Timer() {
			      @Override
			      public void run() {
			     	 
			    	  form.getTbServiceId().setValue(model.getserviceId()+"");
			    	 
			     }
			};
			t.schedule(3000);
		}
		else
		{
			//  rohan added this code for multiple expense mngt
			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Expense Management", Screen.MULTIPLEEXPENSEMANAGMENT);
			final MultipleExpensemanagmentForm form= new MultipleExpensemanagmentForm();
			MultipleExpensemanagmentPresenter.initalize();
			
			
			form.setToNewState();
			Timer t = new Timer() {
			      @Override
			      public void run() {
			     	 
			    	  form.getTbServiceId().setValue(model.getserviceId()+"");
			    	 
			     }
			};
			t.schedule(3000);
			
		}
	}
		
    public void reactOnPrint()
    {
    }

    public void reactOnDownload()
    {
    }

    public void reactOnEmail()
    {
    }

    protected void makeNewModel()
    {
        model = new ServiceProject();
        setModel(model);
        form.serviceId.setEnabled(true);
    }

    public MyQuerry getProjectQuery()
    {
    	MyQuerry quer = new MyQuerry(new Vector<Filter>(),new ServiceProject());
    	return quer;    	
    }
    
    
    public MyQuerry getProjectQueryOnContract(int id)
    {
    	Filter filter=new Filter();
    	filter.setQuerryString("contractId");
    	filter.setIntValue(id);
    	Vector<Filter>vec=new Vector<Filter>();
    	vec.add(filter);
    	MyQuerry quer = new MyQuerry(vec, new ServiceProject());
        return quer;
    }

    @SuppressWarnings("unused")
	public static ProjectForm initalize()
    {
    	AppMemory.getAppMemory().currentState=ScreeenState.NEW;
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Customer Project",Screen.PROJECT);
    	ProjectPresenterTable gentableScreen = new ProjectPresenterTableProxy();
        ProjectForm form = new ProjectForm(gentableScreen, 1, true);
        gentableScreen.setView(form);
        gentableScreen.applySelectionModle();
        ProjectPresenterTable gentableSearch = new ProjectPresenterTableProxy();
        gentableSearch.setView(form);
        gentableSearch.applySelectionModle();
        ProjectPresenterSearch.staticSuperTable = gentableSearch;
        ProjectPresenterSearch searchpopup = new ProjectPresenterSearchProxy();
        form.setSearchpopupscreen(searchpopup);
        ProjectPresenter presenter = new ProjectPresenter(form, new ServiceProject());
        AppMemory.getAppMemory().stickPnel(form);
        return form;
    }
    
    
    @SuppressWarnings("unused")
	public static ProjectForm initalize(MyQuerry querry)
    {
        
    	AppMemory.getAppMemory().currentState=ScreeenState.NEW;
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Customer Project",Screen.PROJECT);
    	ProjectPresenterTable gentableScreen = new ProjectPresenterTableProxy();
        ProjectForm form = new ProjectForm(gentableScreen, 1, true);
        gentableScreen.setView(form);
        gentableScreen.applySelectionModle();
        ProjectPresenterTable gentableSearch = new ProjectPresenterTableProxy();
        gentableSearch.setView(form);
        gentableSearch.applySelectionModle();
        ProjectPresenterSearch.staticSuperTable = gentableSearch;
        ProjectPresenterSearch searchpopup = new ProjectPresenterSearchProxy();
        form.setSearchpopupscreen(searchpopup);
        ProjectPresenter presenter = new ProjectPresenter(form, new ServiceProject(),querry);
        AppMemory.getAppMemory().stickPnel(form);
        return form;
    }

    
    @SuppressWarnings("unused")
	public static ProjectForm  initalize(int ContractId)
    {
    	AppMemory.getAppMemory().currentState=ScreeenState.NEW;
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Customer Project",Screen.PROJECT);
    	ProjectPresenterTable gentableScreen = new ProjectPresenterTableProxy();
        ProjectForm form = new ProjectForm(gentableScreen, 1, true);
        gentableScreen.setView(form);
        gentableScreen.applySelectionModle();
        ProjectPresenterTable gentableSearch = new ProjectPresenterTableProxy();
        gentableSearch.setView(form);
        gentableSearch.applySelectionModle();
        ProjectPresenterSearch.staticSuperTable = gentableSearch;
        ProjectPresenterSearch searchpopup = new ProjectPresenterSearchProxy();
        form.setSearchpopupscreen(searchpopup);
        ProjectPresenter presenter = new ProjectPresenter(form, new ServiceProject(),ContractId);
        AppMemory.getAppMemory().stickPnel(form);
        return form;
    }
    
    private void reactToNew()
    {
        form.setToNewState();
        initalize();
        form.toggleAppHeaderBarMenu();
    }
    
	public void setServiceDetails()
	{
		Integer serviceId = Integer.parseInt(form.serviceId.getValue());
	    if(serviceId != null)
	    {
	        Filter temp = new Filter();
	        temp.setIntValue(serviceId);
	        temp.setQuerryString("count");
	        MyQuerry querry = new MyQuerry();
	        querry.setQuerryObject(new Service());
	        querry.getFilters().add(temp);
	        service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			 
	        	
	            public void onSuccess(ArrayList<SuperModel> result)
	            {
	                if(result.size()==0)
	                {
	                	form.showDialogMessage("No Service exists Corresponding to this ID !");
	                	form.ibContractId.setText("");
	                	form.serviceId.setText("");
	                	form.pic.clear();
	                	
	                	form.ibContractId.setText("");
	                	form.pic.clear();
	                	form.serviceEngineer.setText("");
	                	form.serviceStaus.setText("");
	                	form.serviceDate.getTextBox().setText("");
	                	form.ibContractId.setText("");
	                	form.dbContractEndDate.getTextBox().setText("");
	                	form.dbContractStartDate.getTextBox().setText("");
	                	return;
	                }
	            	Service cont = (Service)result.get(0);
	                form.getDbContractStartDate().setValue(cont.getContractStartDate());
	                form.getDbContractEndDate().setValue(cont.getContractEndDate());
	                form.getPic().setValue(cont.getPersonInfo());
	                
	                form.serviceDate.setValue(cont.getServiceDate());
	                form.serviceStaus.setValue(cont.getStatus());
	                form.serviceEngineer.setValue(cont.getEmployee());
	                form.ibContractId.setValue(cont.getContractCount()+"");
	                form.pic.makeMandatoryMessageFalse();
	                //Making the Client Side tool olb live
	                Filter filter=new Filter();
	                filter.setQuerryString("personInfo.count");
	                filter.setIntValue(cont.getPersonInfo().getCount());
	                MyQuerry querry=new MyQuerry();
	                querry.setQuerryObject(new ClientSideAsset());
	                querry.getFilters().add(filter);
	                form.olbClientSideAsset.MakeLive(querry);
	                form.addressComposite.setValue(cont.getAddress());
	            }
	
	            public void onFailure(Throwable throwable)
	            {
	            }
	
	        }
	);
	    }
	}
   
	public void onValueChange(ValueChangeEvent<String> event)
    {
        this.setServiceDetails();
    }
	
	
//******************************************************************************************************//
	
	  private void getmaterialfromServiceProjectForm()
	  {
		  ArrayList<ProductGroupList> productlis=new ArrayList<ProductGroupList>();
		  productlis.addAll(form.getMaterialProdTable().getDataprovider().getList());
		  
		  for(int i=0;i<productlis.size();i++){
			  
		  MaterialProduct material=new MaterialProduct();
			
			material.setMaterialProductId(productlis.get(i).getProduct_id());
			material.setMaterialProductCode(productlis.get(i).getCode());
			material.setMaterialProductName(productlis.get(i).getName());
			material.setMaterialProductUOM(productlis.get(i).getUnit());
			material.setMaterialProductRequiredQuantity(productlis.get(i).getQuantity());
  		    /*** Date 04-01-2019by Vijay for NBHC map planned qty into MIN ****/
			material.setMaterialProductPlannedQuantity(productlis.get(i).getPlannedQty());
			System.out.println("productlis.get(i).getPlannedQty()");
			material.setMaterialProductRemarks("");
			
			mplist.add(material);
		  }
	  }
	  
	  
	  private void showPopupWithListBox() {
		  
			MyQuerry querry = new MyQuerry();
		  	Company c = new Company();
		  	Vector<Filter> filtervec=new Vector<Filter>();
		  	Filter filter = null;
		  	filter = new Filter();
		  	filter.setQuerryString("companyId");
			filter.setLongValue(c.getCompanyId());
			filtervec.add(filter);
			filter = new Filter();
			filter.setQuerryString("serviceId");
			filter.setIntValue(Integer.parseInt(form.getServiceId().getValue().trim()));
			filtervec.add(filter);
			querry.setFilters(filtervec);
			querry.setQuerryObject(new MaterialIssueNote());
			
			service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onFailure(Throwable caught) {
				}

				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					System.out.println(" result set size +++++++"+result.size());
					List<String> list=new ArrayList<String>();
					
//					if(result.size()==0){
					
					for(SuperModel model:result)
					{
						MaterialIssueNote minEntity=(MaterialIssueNote)model;
						list.add(minEntity.getCount()+"");
					}
					popup.getMinID().clear();
					popup.getMinID().addItem("SELECT");
					
					for(int i=0;i<list.size();i++){
					popup.getMinID().addItem(list.get(i));
					}
					
					
					panel=new PopupPanel(true);
					panel.add(popup);
					panel.show();
					panel.center();
//					}
//					else
//					{
//						form.showDialogMessage("Material issue note for this service is not created..!");
//					}
					
				}
			});
		}
	  
	  
	  private void returnMaterial(int minId)
	  {
			MyQuerry querry = new MyQuerry();
		  	Company c = new Company();
		  	Vector<Filter> filtervec=new Vector<Filter>();
		  	Filter filter = null;
		  	filter = new Filter();
		  	filter.setQuerryString("companyId");
			filter.setLongValue(c.getCompanyId());
			filtervec.add(filter);
			filter = new Filter();
			filter.setQuerryString("serviceId");
			filter.setIntValue(Integer.parseInt(form.getServiceId().getValue().trim()));
			filtervec.add(filter);
			querry.setFilters(filtervec);
			
			
			filter = new Filter();
			filter.setQuerryString("count");
			filter.setIntValue(minId);
			filtervec.add(filter);
			querry.setFilters(filtervec);
			
			
			querry.setQuerryObject(new MaterialIssueNote());
			
			service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onFailure(Throwable caught) {
				}

				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					
//					if(result.size()!=0){
			
					for(SuperModel model:result)
						{
						final MaterialIssueNote MINEntity=(MaterialIssueNote)model;
						
						if(MINEntity.getStatus().trim().equals(MaterialIssueNote.APPROVED))
						{
							AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Material Movement Note", Screen.MATERIALMOVEMENTNOTE);
							MaterialMovementNotePresenter.initialize();
							final MaterialMovementNoteForm form=MaterialMovementNotePresenter.form;
							form.setToNewState();
							Timer t = new Timer() {
							      @Override
							      public void run() 
							      {
							    	  form.getTbMmnTitle().setValue("Service ID -"+MINEntity.getServiceId());
							    	  form.getIbMmnWoId().setValue(MINEntity.getMinWoId()+"");
							    	  form.getDbMmnDate().setValue(MINEntity.getMinDate());
							    	  form.getTbserviceId().setValue(MINEntity.getServiceId()+"");
							    	  form.getOblMmnTransactionType().setValue("RETURN");
							    	  form.getDbMmnMinDate().setValue(MINEntity.getMinDate());
							    	  form.getIbMmnMinId().setValue(MINEntity.getCount()+"");
							    	  form.getOrderId().setValue(MINEntity.getMinSoId()+"");
							    	  form.getOblMmnCategory().setValue(MINEntity.getCategory());
							    	  form.getOblMmnType().setValue(MINEntity.getType());
							    	  form.getTaMmnDescription().setValue("Ref MIN ID - "+MINEntity.getCount()+"\n"+MINEntity.getMinDescription());
							    	  form.getOblMmnBranch().setValue(MINEntity.getBranch());
							    	  form.getOblMmnApproverName().setValue(MINEntity.getApproverName());
							    	  form.getOblMmnPersonResponsible().setValue(MINEntity.getEmployee());
							    	  form.getOblMmnRequestedBy().setValue(MINEntity.getEmployee());
							    	  form.getSubProductTableMmn().setValue(MINEntity.getSubProductTablemin());
							    	  form.getStrWarehouse().setVisible(false);
							    	  form.getStrStrBin().setVisible(false);
							    	  form.getStrStrLoc().setVisible(false);
							    	  form.checkMinQuantity(MINEntity.getCount());
								   }

							  	};
								t.schedule(3000);
						}
						else{
							form.showDialogMessage("Material Issue Note is not approved!");
						}
					}
//				}
//				else
//				{
//					form.showDialogMessage("MIN for this service is not created");
//				}
				}
			});
	  }
	
	
	
	
	
  public void makeClientAssetListBoxLive(int count)
  {
	  Filter filter=new Filter();
      filter.setQuerryString("personInfo.count");
      filter.setIntValue(count);
      MyQuerry querry=new MyQuerry();
      querry.setQuerryObject(new ClientSideAsset());
      querry.getFilters().add(filter);
      form.olbClientSideAsset.MakeLive(querry);
  }
  
  public void markCompleted()
  {
	  ServiceProject proj=(ServiceProject) getModel();
	  boolean isServicable=true;
	  if(proj.getClientsideAssettable().size()!=0)
	  {
		  List<ServicedClientSideAsset> array=proj.getClientsideAssettable();
	    	
	    	
	    		
	    	
	    	for(int i=0;i<array.size();i++)
	    	{
	    		if(array.get(i).getMandatoryService()==true&&array.get(i).isServicedStatus()==false)
	    		{
	    		  isServicable=false;
	    		  System.out.println("Status is "+array.get(i).isServicedStatus());
	    		}
	    	}
	  }
	  if(isServicable==true)
	  {
		  model.setProjectStatus(ServiceProject.COMPLETED);
		  service.save(model,new AsyncCallback<ReturnFromServer>() {

			@Override
			public void onFailure(Throwable caught) {
				view.showDialogMessage("Failed to Mark Complete !");
				
			}

			@Override
			public void onSuccess(ReturnFromServer result) {
				view.showDialogMessage("Mark Completed !");
				form.status.setText(ServiceProject.COMPLETED);
				form.setMenuAsPerStatus();
				
			}
		});
	  }
	  
	  else
		  form.showDialogMessage("Project cannot be mark completed unless all mandatory "
		  		+ "Client assets are serviced !");
	  
	  
  }
  
  
  
  private MyQuerry getMaterialFromProductGroupId() {
	  	
	  	MyQuerry querry = new MyQuerry();
	  	Company c = new Company();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
	  	filter = new Filter();
	  	filter.setQuerryString("companyId");
			filter.setLongValue(c.getCompanyId());
			filtervec.add(filter);
			filter = new Filter();
			filter.setQuerryString("count");
			filter.setIntValue(model.getBillMaterialLis().get(0).getProdGroupId());
			filtervec.add(filter);
			querry.setFilters(filtervec);
			querry.setQuerryObject(new ProductGroupDetails());
			
			return querry;
	  }
  
  private void issueMaterial() {
	  	
	  System.out.println("in side material issue note");
	  
	  
   	  final List<MaterialProduct> productList = new ArrayList<MaterialProduct>();
	  List<ProductInventoryViewDetails> productinventoryList = new ArrayList<ProductInventoryViewDetails>();
	  for (int i = 0; i < mplist.size(); i++) {
		  map.containsKey(mplist.get(i).getMaterialProductId());
		  productinventoryList= map.get(mplist.get(i).getMaterialProductId());
		  
		  if(productinventoryList!=null)
		  {
		  for (int j = 0; j < productinventoryList.size(); j++) {
    		  
    		  MaterialProduct product = new MaterialProduct();
    	
    		  product.setMaterialProductId(productinventoryList.get(j).getProdid());
//    		  product.setMaterialProductCode(productinventoryList.get(j).getProdcode());
    		  /*** Date 22-04-2019 by Vijay product code not mapping in MIN so fixed bug ****/
    		  product.setMaterialProductCode(mplist.get(i).getMaterialProductCode());
    		  product.setMaterialProductName(productinventoryList.get(j).getProdname());
    		  product.setMaterialProductRequiredQuantity(mplist.get(i).getMaterialProductRequiredQuantity());
    		  product.setMaterialProductAvailableQuantity(productinventoryList.get(j).getAvailableqty());
    		  product.setMaterialProductUOM(mplist.get(i).getMaterialProductUOM());
    		  product.setMaterialProductRemarks(mplist.get(i).getMaterialProductRemarks());
    		  product.setMaterialProductWarehouse(productinventoryList.get(j).getWarehousename());
    		  product.setMaterialProductStorageLocation(productinventoryList.get(j).getStoragelocation());
    		  product.setMaterialProductStorageBin(productinventoryList.get(j).getStoragebin());
    		  /*** Date 04-01-2019by Vijay for NBHC map planned qty into MIN ****/
    		  product.setMaterialProductPlannedQuantity(mplist.get(i).getMaterialProductPlannedQuantity());

    		  
    		  productList.add(product);
    	  }
		  }
		  else
		  {
			  MaterialProduct product1 = new MaterialProduct();
			  product1.setMaterialProductId(mplist.get(i).getMaterialProductId());
			  product1.setMaterialProductCode(mplist.get(i).getMaterialProductCode());
			  product1.setMaterialProductName(mplist.get(i).getMaterialProductName());
			  product1.setMaterialProductRequiredQuantity(mplist.get(i).getMaterialProductRequiredQuantity());
    		  product1.setMaterialProductUOM(mplist.get(i).getMaterialProductUOM());
    		  product1.setMaterialProductRemarks(mplist.get(i).getMaterialProductRemarks());
    		  /*** Date 04-01-2019by Vijay for NBHC map planned qty into MIN ****/
    		  product1.setMaterialProductPlannedQuantity(mplist.get(i).getMaterialProductPlannedQuantity());

			  productList.add(product1);
		  }
	} 
	  
	  
	  System.out.println("product list size"+  productList.size());
	
	  
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Material Issue Note", Screen.MATERIALISSUENOTE);
		MaterialIssueNotePresenter.initialize();
		final MaterialIssueNoteForm form=MaterialIssueNotePresenter.form;
		form.setToNewState();
		Timer t = new Timer() {
		      @Override
		      public void run() {
		     	 
		    	  form.getdbMinMrnDate().setValue(new Date());
		    	  form.getTbServiceId().setValue(model.getserviceId()+"");
		    	  form.getDbMinWoDate().setValue(model.getserviceDate());
		    	  form.getIbMinSoId().setValue(model.getContractId()+"");
		    	  form.getDbMinSoDate().setValue(model.getContractEndDate());
		    	  form.getDbMinDeliveryDate().setValue(model.getserviceDate());
		    	  form.getTbMinTitle().setValue("Service - ID"+model.getserviceId()+"");
		    	  form.getDbMinDate().setValue(new Date());
		    	  form.getSubProductTableMin().setValue(productList);
		    	  // rohan added this code for restriction of material issue form project
		    	  
		    	  form.getProductInfoComposite().getProdName().setEnabled(false);
		    	  form.getProductInfoComposite().getProdCode().setEnabled(false);
		    	  form.getProductInfoComposite().getProdID().setEnabled(false);
		    	  
		    	  form.getBtnAdd().setEnabled(false);
		    	  form.setColumnSelectWareHouseEditable();
//		    	  form.getSubProductTableMin().setValue(mplist);
		 
		    	  
		    	  form.getPersonInfoComposite().setValue(model.getPersonInfo());
		    	  
		    	  
		      }
		    	  
		};
		t.schedule(3000);
	}
	
		
		  private void getdefaultwarehousedetails()
		  {
			  
			  //   rohan added this codefor science utsav 
			  
			  for (int i = 0; i < mplist.size(); i++) {
	    		  final int productId=  mplist.get(i).getMaterialProductId();
	    			MyQuerry querry = new MyQuerry();
	    		  	Company c = new Company();
	    		  	Vector<Filter> filtervec=new Vector<Filter>();
	    		  	Filter filter = null;
	    		  	filter = new Filter();
	    		  	filter.setQuerryString("companyId");
	    			filter.setLongValue(c.getCompanyId());
	    			filtervec.add(filter);
	    			filter = new Filter();
	    			filter.setQuerryString("productinfo.prodID");
	    			filter.setIntValue(mplist.get(i).getMaterialProductId());
	    			filtervec.add(filter);
	    			querry.setFilters(filtervec);
	    			querry.setQuerryObject(new ProductInventoryView());
	    			
	    			service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
	    				
	    				@Override
	    				public void onFailure(Throwable caught) {
	    				}

	    				@Override
	    				public void onSuccess(ArrayList<SuperModel> result) {
	    					
	    					for(SuperModel model:result)
	    					{
	    						ProductInventoryView product=(ProductInventoryView)model;
	    						
	    						if(product.getDetails().size()== 1 ){
	    						for (int j = 0; j < product.getDetails().size(); j++) 
	    						{
	    							if( productId==product.getDetails().get(j).getProdid())
	    							{
	    								map.put(productId, product.getDetails());
	    								System.out.println("after putting data to map");
	    							}
								}
	    						}
	    						
	    					}
	    					
	    					
	    				}
	    			});
	    		}
			  
			  Timer time = new Timer() {
				
				@Override
				public void run() {
					 System.out.println("map size "+map.size());
						issueMaterial();
				}
			};
			time.schedule(3000);
		  }
	
		  
  
	  private MyQuerry getMaterialProductFromToolsQuerry(String count) {
			
		  	MyQuerry querry = new MyQuerry();
		  	Company c = new Company();
		  	Vector<Filter> filtervec=new Vector<Filter>();
		  	Filter filter = null;
		  	filter = new Filter();
		  	filter.setQuerryString("companyId");
			filter.setLongValue(c.getCompanyId());
			filtervec.add(filter);
			filter = new Filter();
			filter.setQuerryString("productCode");
			filter.setStringValue(count);
			filtervec.add(filter);
			querry.setFilters(filtervec);
			querry.setQuerryObject(new ItemProduct());
			return querry;
	}




		private void getMaterialProductFromToolsList(){
			
			List<CompanyAsset> compnyassetlis= form.tooltable.getDataprovider().getList();
			
			for(int i=0;i<compnyassetlis.size();i++){
				
			     	  service.getSearchResult(getMaterialProductFromToolsQuerry("PI"+compnyassetlis.get(i).getCount()), new AsyncCallback<ArrayList<SuperModel>>() {
				  			
				  			@Override
				  			public void onFailure(Throwable caught) {
				  			}
		
				  			@Override
				  			public void onSuccess(ArrayList<SuperModel> result) {
				  			
				  				for(SuperModel model:result)
				  				{
				  					
				  					ItemProduct sup=(ItemProduct)model;
				  					MaterialProduct material=new MaterialProduct();
				  					
				  					material.setMaterialProductId(sup.getCount());
				  					material.setMaterialProductCode(sup.getProductCode());
				  					material.setMaterialProductName(sup.getProductName());
				  					material.setMaterialProductUOM(sup.getUnitOfMeasurement());
				  					material.setMaterialProductRequiredQuantity(1);
				  					
				  					mplist.add(material);
				  		    	  
				  		    	}		
				  			}
			     	  });
			}
		}

  
		 public static class ProjectPresenterSearch extends SearchPopUpScreen<ServiceProject>
		    {

		        public MyQuerry getQuerry()
		        {
		            return null;
		        }

		        public ProjectPresenterSearch()
		        {
		        }

				@Override
				public boolean validate() {
					// TODO Auto-generated method stub
					return true;
				}
		    }

		    public static class ProjectPresenterTable extends SuperTable<ServiceProject>
		        implements GeneratedVariableRefrence
		    {

		        public Object getVarRef(String varName)
		        {
		            return null;
		        }

		        public void createTable()
		        {
		        }

		        protected void initializekeyprovider()
		        {
		        }

		        public void addFieldUpdater()
		        {
		        }

		        public void setEnable(boolean flag)
		        {
		        }

		        public void applyStyle()
		        {
		        }

		        public ProjectPresenterTable()
		        {
		        }
		    }


		   
		    @Override
			public void onChange(ChangeEvent event) {
				
			  if (popup.getMinID().getSelectedIndex()!=0){
				  
				  getDetailsofMin();
			  }
		  }
		    
		    @Override
			public void onClick(ClickEvent event) {
				super.onClick(event);
				
				if(event.getSource()==popup.getBtnOk()){
					panel.hide();
					if(popup.getMinID().getSelectedIndex()!=0){
						int index= popup.getMinID().getSelectedIndex();
					returnMaterial(Integer.parseInt(popup.getMinID().getItemText(index)));
					}
					else{
						form.showDialogMessage("MIN for this service is not created");
					}
				}
			
				if(event.getSource()==popup.getBtnCancel()){
					
					panel.hide();
					popup.getMinTitle().setValue("");
					popup.getStatus().setValue("");
					
				}
				
				
				
				if(event.getSource()==conditionPopup.getBtnOne()){
					getmaterialfromServiceProjectForm();
		        	getMaterialProductFromToolsList();
		        	getdefaultwarehousedetails();
		        	
		        	conditionPopuppanel.hide();
				}
				
				if(event.getSource()==conditionPopup.getBtnTwo())
				{
					conditionPopuppanel.hide();
				}
			}
		    
		    private void getDetailsofMin()
			{		
				
					form.showWaitSymbol();
					Timer timer=new Timer() 
				     {
						@Override
						public void run() 
						{
							
							MyQuerry querry = new MyQuerry();
							Company c=new Company();
							Vector<Filter> filtervec=new Vector<Filter>();
							Filter filter = null;
							
							filter = new Filter();
							filter.setQuerryString("companyId");
							filter.setLongValue(c.getCompanyId());
							filtervec.add(filter);
							
							filter = new Filter();
							filter.setQuerryString("count");
							int index = popup.getMinID().getSelectedIndex();
							filter.setIntValue(Integer.parseInt(popup.getMinID().getItemText(index)));
							filtervec.add(filter);
							querry.setFilters(filtervec);
							querry.setQuerryObject(new MaterialIssueNote());
							
							service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
								
								@Override
								public void onFailure(Throwable caught) {
									form.hideWaitSymbol();
									form.showDialogMessage("An Unexpected error occurred in getDetailsofMin method!");
								}
					
								@Override
								public void onSuccess(ArrayList<SuperModel> result) {
									
									System.out.println("result size is ========"+result.size());
									
									for(SuperModel model:result)
									{
										MaterialIssueNote minEntity=(MaterialIssueNote)model;
										
										popup.getMinTitle().setValue(minEntity.getMinTitle());
										popup.getStatus().setValue(minEntity.getStatus());
									}
									}
								 });
					
					 form.hideWaitSymbol();
						}
				     };
			  	 timer.schedule(2000);
			}
}
