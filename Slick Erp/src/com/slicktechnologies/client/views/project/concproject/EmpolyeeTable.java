// Decompiled by DJ v3.12.12.98 Copyright 2014 Atanas Neshkov  Date: 16-Sep-14 11:03:32 AM
// Home Page:  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   EmpolyeeTable.java

package com.slicktechnologies.client.views.project.concproject;

import com.google.gwt.cell.client.*;
import com.google.gwt.user.cellview.client.*;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.view.client.ProvidesKey;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import java.util.Comparator;
import java.util.List;

public class EmpolyeeTable extends SuperTable<EmployeeInfo>
{

	 TextColumn<EmployeeInfo> nameColumn;
	    TextColumn<EmployeeInfo> idColumn;
	    TextColumn<EmployeeInfo> phoneColumn;
	    Column<EmployeeInfo,String> deleteColumn;
	    TextColumn<EmployeeInfo> disignationColumn;
	    
	    
    public EmpolyeeTable()
    {
    	super();
    }

    protected void createColumndeleteColumn()
    {
        ButtonCell btnCell = new ButtonCell();
        deleteColumn = new Column<EmployeeInfo,String>(btnCell) {

            public String getValue(EmployeeInfo object)
            {
                return "Delete";
            }

        }
;
        table.addColumn(deleteColumn, "Delete");
    }

    private void createColumnName()
    {
        nameColumn = new TextColumn<EmployeeInfo>() {

            public String getValue(EmployeeInfo object)
            {
                return object.getFullName();
            }

        }
;
        table.addColumn(nameColumn, "Employee Name");
        nameColumn.setSortable(true);
    }

    private void createColumnId()
    {
        idColumn = new TextColumn<EmployeeInfo>() {

            public String getValue(EmployeeInfo object)
            {
                return (new StringBuilder()).append(object.getEmpCount()).toString();
            }
        };
        table.addColumn(idColumn, "Id");
        idColumn.setSortable(true);
    }

    private void createColumnPhone()
    {
        phoneColumn = new TextColumn<EmployeeInfo>() {

            public String getValue(EmployeeInfo object)
            {
                return (new StringBuilder()).append(object.getCellNumber()).toString();
            }

        }
;
        table.addColumn(phoneColumn, "Phone");
        phoneColumn.setSortable(true);
    }

    public void createTable()
    {
        createColumnName();
        createColumnId();
        createColumnPhone();
        createColumnEmployeeDesignation();
        createColumndeleteColumn();
        table.setWidth("100%");
        createFieldUpdaterdeleteColumn();
        table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
    }

	private void createColumnEmployeeDesignation() {
		
		disignationColumn = new TextColumn<EmployeeInfo>() {

            public String getValue(EmployeeInfo object)
            {
            	if(object.getDesignation()!=null && !object.getDesignation().equals(""))
            	{
            		return object.getDesignation();
            	}
            	else
            	{
            		return "NA";
            	}
            }

        }
;
        table.addColumn(disignationColumn, "Employee Designation");
        disignationColumn.setSortable(true);
	}

	protected void createFieldUpdaterdeleteColumn()
    {
        deleteColumn.setFieldUpdater(new FieldUpdater<EmployeeInfo,String>() {

            public void update(int index, EmployeeInfo object, String value)
            {
                getDataprovider().getList().remove(index);
                table.redrawRow(index);
            }
        });
    }

    protected void initializekeyprovider()
    {
        keyProvider = new ProvidesKey<EmployeeInfo>() {

            public Object getKey(EmployeeInfo item)
            {
                if(item == null)
                    return null;
                else
                    return item.getId();
            }

           
        }
;
    }

    public void addColumnSorting()
    {
        addNameSorting();
        
    }

    
	public void addNameSorting()
    {
        List list = getDataprovider().getList();
        columnSort = new com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler(list);
        columnSort.setComparator(nameColumn, new Comparator<EmployeeInfo>() {

            public int compare(EmployeeInfo e1, EmployeeInfo e2)
            {
                
                if(e1 != null && e2 != null)
                {
                    if(e1.getFullName() != null && e2.getFullName() != null)
                        return e1.getFullName().compareTo(e2.getFullName());
                    else
                        return 0;
                } else
                {
                    return 0;
                }
            }
        }
);
        table.addColumnSortHandler(columnSort);
    }

	 @Override
		public void setEnable(boolean state)
		{
	        for(int i=table.getColumnCount()-1;i>-1;i--)
	    	  table.removeColumn(i); 
	          if(state==true)
	          {
	        	  createColumnName();
	        	  createColumnId();
	              createColumnPhone();
	              createColumnEmployeeDesignation();
	      		createColumndeleteColumn();
	      		createFieldUpdaterdeleteColumn();
	          }
	          
	          else
	          {
	        	  createColumnName();
	        	  createColumnId();
	              createColumnPhone();
	              createColumnEmployeeDesignation();
	      	
	          }
		}

    public void applyStyle()
    {
    }

    public void addFieldUpdater()
    {
    }

   

}
