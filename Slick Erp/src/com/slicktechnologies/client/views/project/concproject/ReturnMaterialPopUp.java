package com.slicktechnologies.client.views.project.concproject;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;

public class ReturnMaterialPopUp extends AbsolutePanel {
	
	ListBox minID;
	TextBox minTitle;
	TextBox status;
	Button  btnCancel,btnOk;
	
	public ReturnMaterialPopUp() {
		
		btnCancel= new Button("Cancel");
		btnOk= new Button("Ok");
		
		InlineLabel popupName = new InlineLabel("Return Material");
		add(popupName,160,10);
		popupName.getElement().getStyle().setFontSize(20, Unit.PX);
		
		InlineLabel minIdLable = new InlineLabel("Min ID");
		add(minIdLable,10,50);
		minID= new ListBox();
		add(minID,10,70);
		minID.setSize("100px", "20px");
		
		
		InlineLabel minTitleLable = new InlineLabel("Min Title");
		add(minTitleLable,130,50);
		minTitle= new TextBox();
		add(minTitle,130,70);
		
		minTitle.setSize("135px", "18px");
		minTitle.setEnabled(false);
		
		InlineLabel statusLable = new InlineLabel("Min Status");
		add(statusLable,290,50);
		status= new TextBox();
		add(status,290,70);
		
		status.setSize("100px", "18px");
		status.setEnabled(false);
		
		add(btnOk,100,200);
		add(btnCancel,200,200);
		setSize("400px", "250px");
		this.getElement().setId("form");
	}

	//******************************getters and setters ***************************
	
	public ListBox getMinID() {
		return minID;
	}

	public void setMinID(ListBox minID) {
		this.minID = minID;
	}

	public TextBox getMinTitle() {
		return minTitle;
	}

	public void setMinTitle(TextBox minTitle) {
		this.minTitle = minTitle;
	}

	public TextBox getStatus() {
		return status;
	}

	public void setStatus(TextBox status) {
		this.status = status;
	}

	public Button getBtnCancel() {
		return btnCancel;
	}

	public void setBtnCancel(Button btnCancel) {
		this.btnCancel = btnCancel;
	}

	public Button getBtnOk() {
		return btnOk;
	}

	public void setBtnOk(Button btnOk) {
		this.btnOk = btnOk;
	}

	

}
