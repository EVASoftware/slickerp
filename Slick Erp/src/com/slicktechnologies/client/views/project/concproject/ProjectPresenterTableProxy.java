// Decompiled by DJ v3.12.12.98 Copyright 2014 Atanas Neshkov  Date: 16-Sep-14 11:04:10 AM
// Home Page:  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   ProjectPresenterTableProxy.java

package com.slicktechnologies.client.views.project.concproject;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.ProvidesKey;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;

// Referenced classes of package com.slicktechnologies.client.views.project.concProject:
//            ProjectPresenter

public class ProjectPresenterTableProxy extends ProjectPresenter.ProjectPresenterTable
{
	TextColumn<ServiceProject> getServiceId;
    TextColumn<ServiceProject> getServiceDate;
    TextColumn<ServiceProject> getPocNameColumn;
    TextColumn<ServiceProject> getProjectNameColumn;
    TextColumn<ServiceProject> getClientSideAssetColumn;
    TextColumn<ServiceProject> getCountColumn;
    TextColumn<ServiceProject> getContractIdColumn;
    TextColumn<ServiceProject> getStatusColumn;
    TextColumn<ServiceProject> getServiceStatusColumn;
    TextColumn<ServiceProject> getServiceEngineerColumn;
    TextColumn<ServiceProject> getCustomerIdColumn;
    TextColumn<ServiceProject> getCustomerFullNameColumn;
    TextColumn<ServiceProject>getCustomerCellNumberColumn;

   

    public ProjectPresenterTableProxy()
    {
       
    }

    public void createTable()
    {
        addColumngetCount();
        addColumngetProjectName();
        addColumngetPocName();
        addColumngetContractId();
        addColumngetServiceId();
        addColumngetCustomerId();
        addColumngetCustomerFullName();
        addColumngetCustomerCellNumber();
        addColumngetServiceDate();
        addColumngetServiceEngineer();
        addColumngetServiceStatus();
       
        addColumngetStatus();
        applyStyle();
        
    }

    protected void initializekeyprovider()
    {
        keyProvider = new ProvidesKey<ServiceProject>() {

            public Object getKey(ServiceProject item)
            {
                if(item == null)
                    return null;
                else
                    return item.getId();
            }

            
        }
;
    }

    public void setEnable(boolean flag)
    {
    }

    public void applyStyle()
    {
    	table.setColumnWidth(getServiceId,100,Unit.PX);
    	table.setColumnWidth(getServiceDate,100,Unit.PX);
    	table.setColumnWidth(getPocNameColumn,120,Unit.PX);
    	table.setColumnWidth(getProjectNameColumn,110,Unit.PX);
    	table.setColumnWidth(getCountColumn,100,Unit.PX);
    	table.setColumnWidth(getContractIdColumn,100,Unit.PX);
    	table.setColumnWidth(getServiceStatusColumn,100,Unit.PX);
    	table.setColumnWidth(getServiceEngineerColumn,120,Unit.PX);
    	table.setColumnWidth(getCustomerIdColumn,100,Unit.PX);
    	table.setColumnWidth(getCustomerFullNameColumn,120,Unit.PX);
    	table.setColumnWidth(getCustomerCellNumberColumn,120,Unit.PX);
    	table.setColumnWidth(getStatusColumn,100,Unit.PX);
    	
    }

    public void addColumnSorting()
    {
        addSortinggetPocName();
        addSortinggetProjectName();
        addSortinggetServiceId();
        addSortinggetServiceDate();
        addSortinggetContractId();
        addSortinggetCount();
        addSortingEmployee();
        addSortingServiceStatus();
        addSortinggetProjectStatus();
       
        addSortinggetCustomerId();
        addSortinggetCustomerFullName();
        addSortinggetCustomerCellNumber();
       
        
        
    }

    public void addFieldUpdater()
    {
    }

    protected void addColumngetCount()
    {
        getCountColumn = new TextColumn<ServiceProject>() {

            public String getValue(ServiceProject object)
            {
                if(object.getCount() == -1)
                    return "N.A";
                else
                    return (new StringBuilder(String.valueOf(object.getCount()))).toString();
            }

            

           
        }
;
        table.addColumn(getCountColumn, "Id");
        getCountColumn.setSortable(true);
    }
    
    
    public void addColumngetServiceEngineer()
    {
    	getServiceEngineerColumn=new TextColumn<ServiceProject>() {
			
			@Override
			public String getValue(ServiceProject object) {
				
				return object.getserviceEngineer();
			}
		};
//		table.addColumn(getServiceEngineerColumn,"Service Engineer");
		/**@Sheetal:17-03-2022 
		 *  Des : Renaming Service Engineer to Technician, requirement by nitin sir**/
		table.addColumn(getServiceEngineerColumn,"Technician");
		getServiceEngineerColumn.setSortable(true);
    }
    
    public void addColumngetServiceStatus()
    {
    	getServiceStatusColumn=new TextColumn<ServiceProject>() {
			
			@Override
			public String getValue(ServiceProject object) {
				
				return object.getServiceStatus();
			}
		};table.addColumn(getServiceStatusColumn,"Service Status");
		getServiceStatusColumn.setSortable(true);
    }
    
    protected void addSortinggetCount()
	  {
	  List<ServiceProject> list=getDataprovider().getList();
	  columnSort=new ListHandler<ServiceProject>(list);
	  columnSort.setComparator(getCountColumn, new Comparator<ServiceProject>()
	  {
	  @Override
	  public int compare(ServiceProject e1,ServiceProject e2)
	  {
	  if(e1!=null && e2!=null)
	  {
	  if(e1.getCount()== e2.getCount()){
	  return 0;}
	  if(e1.getCount()> e2.getCount()){
	  return 1;}
	  else{
	  return -1;}
	  }
	  else{
	  return 0;}
	  }
	  });
	  table.addColumnSortHandler(columnSort);
	  }
    
    
    
    protected void addSortinggetContractId()
	  {
	  List<ServiceProject> list=getDataprovider().getList();
	  columnSort=new ListHandler<ServiceProject>(list);
	  columnSort.setComparator(getContractIdColumn, new Comparator<ServiceProject>()
	  {
	  @Override
	  public int compare(ServiceProject e1,ServiceProject e2)
	  {
	  if(e1!=null && e2!=null)
	  {
	  if(e1.getContractId()== e2.getContractId()){
	  return 0;}
	  if(e1.getContractId()> e2.getContractId()){
	  return 1;}
	  else{
	  return -1;}
	  }
	  else{
	  return 0;}
	  }
	  });
	  table.addColumnSortHandler(columnSort);
	  }

    protected void addColumngetContractId()
    {
        getContractIdColumn = new TextColumn<ServiceProject>() {

            public String getValue(ServiceProject object)
            {
                if(object.getContractId() == -1)
                    return "N.A";
                else
                    return (new StringBuilder(String.valueOf(object.getContractId()))).toString();
            }

           

            
        }
;
        table.addColumn(getContractIdColumn, "Contract Id");
        getContractIdColumn.setSortable(true);
    }

    protected void addSortinggetPocName()
    {
        java.util.List<ServiceProject> list = getDataprovider().getList();
        columnSort = new ListHandler<ServiceProject>(list);
        columnSort.setComparator(getPocNameColumn, new Comparator<ServiceProject>() {

            public int compare(ServiceProject e1, ServiceProject e2)
            {
                if(e1 != null && e2 != null)
                {
                    if(e1.getPocName() != null && e2.getPocName() != null)
                        return e1.getPocName().compareTo(e2.getPocName());
                    else
                        return 0;
                } else
                {
                    return 0;
                }
            }

           
        }
);
        table.addColumnSortHandler(columnSort);
    }
    
    
    protected void addSortingServiceStatus()
    {
        java.util.List<ServiceProject> list = getDataprovider().getList();
        columnSort = new ListHandler<ServiceProject>(list);
        columnSort.setComparator(getServiceStatusColumn, new Comparator<ServiceProject>() {

            public int compare(ServiceProject e1, ServiceProject e2)
            {
                if(e1 != null && e2 != null)
                {
                    if(e1.getServiceStatus() != null && e2.getServiceStatus() != null)
                        return e1.getServiceStatus().compareTo(e2.getServiceStatus());
                    else
                        return 0;
                } else
                {
                    return 0;
                }
            }

           
        }
);
        table.addColumnSortHandler(columnSort);
    }
    
    protected void addSortingEmployee()
    {
        java.util.List<ServiceProject> list = getDataprovider().getList();
        columnSort = new ListHandler<ServiceProject>(list);
        columnSort.setComparator(getServiceEngineerColumn, new Comparator<ServiceProject>() {

            public int compare(ServiceProject e1, ServiceProject e2)
            {
                if(e1 != null && e2 != null)
                {
                    if(e1.getserviceEngineer() != null && e2.getserviceEngineer() != null)
                        return e1.getserviceEngineer().compareTo(e2.getserviceEngineer());
                    else
                        return 0;
                } else
                {
                    return 0;
                }
            }

           
        }
);
        table.addColumnSortHandler(columnSort);
    }
    
    
    protected void addSortinggetServiceEngineer()
    {
        java.util.List<ServiceProject> list = getDataprovider().getList();
        columnSort = new ListHandler<ServiceProject>(list);
        columnSort.setComparator(getServiceEngineerColumn, new Comparator<ServiceProject>() {

            public int compare(ServiceProject e1, ServiceProject e2)
            {
                if(e1 != null && e2 != null)
                {
                    if(e1.getserviceEngineer() != null && e2.getserviceEngineer() != null)
                        return e1.getserviceEngineer().compareTo(e2.getserviceEngineer());
                    else
                        return 0;
                } else
                {
                    return 0;
                }
            }

           
        }
);
        table.addColumnSortHandler(columnSort);
    }

    protected void addColumngetPocName()
    {
        getPocNameColumn = new TextColumn<ServiceProject>() {

            public String getValue(ServiceProject object)
            {
            	
                return (new StringBuilder(String.valueOf(object.getPocName()))).toString();
            }

            
        }
;
        table.addColumn(getPocNameColumn, "POC Name");
        getPocNameColumn.setSortable(true);
    }
    
   
    protected void addColumngetStatus()
    {
        getStatusColumn = new TextColumn<ServiceProject>() {

            public String getValue(ServiceProject object)
            {
            	return object.getProjectStatus()+"";
            }

           
           
        }
;
        table.addColumn(getStatusColumn, "Project Status");
        getStatusColumn.setSortable(true);
    }
    
    
    protected void addSortinggetProjectStatus()
    {
        java.util.List<ServiceProject> list = getDataprovider().getList();
        columnSort = new ListHandler<ServiceProject>(list);
        columnSort.setComparator(getStatusColumn, new Comparator<ServiceProject>() {

            public int compare(ServiceProject e1, ServiceProject e2)
            {
                if(e1 != null && e2 != null)
                {
                    if(e1.getProjectStatus() != null && e2.getProjectStatus() != null)
                        return e1.getProjectStatus().compareTo(e2.getProjectStatus());
                    else
                        return 0;
                } else
                {
                    return 0;
                }
            }

           

            
            
        }
);
        table.addColumnSortHandler(columnSort);
    }

    protected void addSortinggetProjectName()
    {
        java.util.List<ServiceProject> list = getDataprovider().getList();
        columnSort = new ListHandler<ServiceProject>(list);
        columnSort.setComparator(getProjectNameColumn, new Comparator<ServiceProject>() {

            public int compare(ServiceProject e1, ServiceProject e2)
            {
                if(e1 != null && e2 != null)
                {
                    if(e1.getProjectName() != null && e2.getProjectName() != null)
                        return e1.getProjectName().compareTo(e2.getProjectName());
                    else
                        return 0;
                } else
                {
                    return 0;
                }
            }

           

            
            
        }
);
        table.addColumnSortHandler(columnSort);
    }

    protected void addColumngetProjectName()
    {
        getProjectNameColumn = new TextColumn<ServiceProject>() {

            public String getValue(ServiceProject object)
            {
                return (new StringBuilder(String.valueOf(object.getProjectName()))).toString();
            }
   }
;
        table.addColumn(getProjectNameColumn, "Project Name");
        getProjectNameColumn.setSortable(true);
    }

    protected void addSortinggetClientSideAsset()
    {
        java.util.List<ServiceProject> list = getDataprovider().getList();
        columnSort = new ListHandler<ServiceProject>(list);
        columnSort.setComparator(getClientSideAssetColumn, new Comparator<ServiceProject>() {

            public int compare(ServiceProject e1, ServiceProject e2)
            {
                if(e1 != null && e2 != null)
                {
                   // if(e1.getClientSideAsset() != null && e2.getClientSideAsset() != null)
                       // return e1.getClientSideAsset().compareTo(e2.getClientSideAsset());
                   // else
                        return 0;
                } else
                {
                    return 0;
                }
            }}

       
);
        table.addColumnSortHandler(columnSort);
    }

    protected void addColumngetClientSideAsset()
    {
        getClientSideAssetColumn = new TextColumn<ServiceProject>() {

            public String getValue(ServiceProject object)
            {
                return "";
            	// return (new StringBuilder(String.valueOf(object.getClientSideAsset()))).toString();
            }


            
        }
;
        table.addColumn(getClientSideAssetColumn, "ClientSide Asset");
        getClientSideAssetColumn.setSortable(true);
    }
    
    
    protected void addSortinggetServiceId()
    {
        java.util.List<ServiceProject> list = getDataprovider().getList();
        columnSort = new ListHandler<ServiceProject>(list);
        columnSort.setComparator(getServiceId, new Comparator<ServiceProject>() {

            public int compare(ServiceProject e1, ServiceProject e2)
            {
                if(e1 != null && e2 != null)
                {
                   if(e1.getCount()==e2.getCount())
                	   return 0;
                   if(e1.getCount()>e2.getCount())
                	   return 1;
                   else
                	   return -1;
                   
                } 
                
                return -1;
            }}

       
);
        table.addColumnSortHandler(columnSort);
    }

    protected void addColumngetServiceId()
    {
        getServiceId = new TextColumn<ServiceProject>() {

            public String getValue(ServiceProject object)
            {
                return (new StringBuilder(String.valueOf(object.getserviceId()))).toString();
            }


            
        }
;
        table.addColumn(getServiceId, "Service ID");
        getServiceId.setSortable(true);
    }
    
    
    protected void addSortinggetServiceDate()
    {
        java.util.List<ServiceProject> list = getDataprovider().getList();
        columnSort = new ListHandler<ServiceProject>(list);
        columnSort.setComparator(getServiceDate, new Comparator<ServiceProject>() {

            public int compare(ServiceProject e1, ServiceProject e2)
            {
                if(e1 != null && e2 != null)
                {
                   if(e1.getserviceDate()!=null && e2.getserviceDate()!=null)
                	   return e1.getserviceDate().compareTo(e2.getserviceDate());
                   else
                	   return 0;
                   
                } 
                
                return -1;
            }}

       
);
        table.addColumnSortHandler(columnSort);
    }

    protected void addColumngetServiceDate()
    {
    	getServiceDate = new TextColumn<ServiceProject>() {

            public String getValue(ServiceProject object)
            {
                if(object.getserviceDate()==null)
                	return "N.A";
            	return AppUtility.parseDate(object.getserviceDate());
            }


            
        }
;
        table.addColumn(getServiceDate, "Service Date");
        getServiceDate.setSortable(true);
    }
    
    
    protected void addSortinggetCustomerId()
	{
		List<ServiceProject> list=getDataprovider().getList();
		columnSort=new ListHandler<ServiceProject>(list);
		columnSort.setComparator(getCustomerIdColumn, new Comparator<ServiceProject>()
				{
			@Override
			public int compare(ServiceProject e1,ServiceProject e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetCustomerId()
	{
		getCustomerIdColumn=new TextColumn<ServiceProject>()
				{
			@Override
			public String getValue(ServiceProject object)
			{
				if( object.getCount()==-1)
					return "N.A";
				else return object.getCustomerCount()+"";
			}
				};
				table.addColumn(getCustomerIdColumn,"Customer Id");
				table.setColumnWidth(getCustomerIdColumn,100,Unit.PX);
				getCustomerIdColumn.setSortable(true);
	}
	
	protected void addSortinggetCustomerFullName()
	{
		List<ServiceProject> list=getDataprovider().getList();
		columnSort=new ListHandler<ServiceProject>(list);
		columnSort.setComparator(getCustomerFullNameColumn, new Comparator<ServiceProject>()
				{
			@Override
			public int compare(ServiceProject e1,ServiceProject e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getCustomerName()!=null && e2.getCustomerName()!=null){
						return e1.getCustomerName().compareTo(e2.getCustomerName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetCustomerFullName()
	{
		getCustomerFullNameColumn=new TextColumn<ServiceProject>()
				{
			@Override
			public String getValue(ServiceProject object)
			{
				return object.getCustomerName()+"";
			}
				};
				table.addColumn(getCustomerFullNameColumn,"Customer Name");
				table.setColumnWidth(getCustomerFullNameColumn,150,Unit.PX);
				getCustomerFullNameColumn.setSortable(true);
	}
	
	
	protected void addSortinggetCustomerCellNumber()
	{
		List<ServiceProject> list=getDataprovider().getList();
		columnSort=new ListHandler<ServiceProject>(list);
		columnSort.setComparator(getCustomerCellNumberColumn, new Comparator<ServiceProject>()
				{
			@Override
			public int compare(ServiceProject e1,ServiceProject e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCustomerCellNumber()== e2.getCustomerCellNumber()){
						return 0;}
					if(e1.getCustomerCellNumber()> e2.getCustomerCellNumber()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetCustomerCellNumber()
	{
		getCustomerCellNumberColumn=new TextColumn<ServiceProject>()
				{
			@Override
			public String getValue(ServiceProject object)
			{
				return object.getCustomerCellNumber()+"";
			}
				};
				table.addColumn(getCustomerCellNumberColumn,"Customer Cell");
				table.setColumnWidth(getCustomerCellNumberColumn,150,Unit.PX);
				getCustomerCellNumberColumn.setSortable(true);
	}
    

    
   
}
