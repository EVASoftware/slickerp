// Decompiled by DJ v3.12.12.98 Copyright 2015 Atanas Neshkov  Date: 16-Sep-15 11:03:52 AM
// Home Page:  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   ProjectForm.java

package com.slicktechnologies.client.views.project.concproject;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.AddressComposite;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.EmailTextBox;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.simplesoftwares.client.library.mywidgets.PhoneNumberBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.project.toolGroup.ToolGroupTable;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.client.views.salesorder.SalesOrderPresenter;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.inventory.BillProductDetails;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productgroup.ProductGroupDetails;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.servicerelated.ClientSideAsset;
import com.slicktechnologies.shared.common.servicerelated.CompanyAsset;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;
import com.slicktechnologies.shared.common.servicerelated.ServicedClientSideAsset;
import com.slicktechnologies.shared.common.servicerelated.ToolGroup;

// Referenced classes of package com.slicktechnologies.client.views.project.concProject:
//            EmpolyeeTable

public class ProjectForm extends FormTableScreen<ServiceProject>implements ClickHandler
{
	

   public TextBox ibContractId;
   public TextBox serviceId;
   public TextBox serviceStaus;
   public DateBox serviceDate;
   public TextBox serviceEngineer;
   public Button adressClear;
   public Button pocClear;
   public TextBox status;
   
   
    DateBox dbContractStartDate;
    DateBox dbContractEndDate;
    PersonInfoComposite pic;
    TextBox ibProjectId;
    TextBox tbProjectName;
    TextBox tbPointOfContactName;
    TextArea taComments;
    AddressComposite addressComposite;
    EmailTextBox etbPointOfContactEmail;
    public ObjectListBox<ClientSideAsset> olbClientSideAsset;
    ObjectListBox<ToolGroup> olbToolGroup;
    ObjectListBox<CompanyAsset> olbTool;
    PhoneNumberBox pnbPointOfContactLandline;
    PhoneNumberBox pnbPointOfContactCell;
    Button btnAddTool;
    Button btnAdd1;
    Button btnClientSideAsset;
    Button btnEmployee;
    ToolTable tooltable;
    ToolGroupTable toolgrouptable;
    ClientSideAssetTable clientsideassettable;
  //  CheckBox status;
    ObjectListBox<Employee> olbEmployee;
    EmpolyeeTable emptable;
    FormField fpic;
    ProjectBillOfMaterial bomtable;
    ProductInfoComposite pc;
    Button addSteps;
    IntegerBox ibServiceSrNo;
    MaterialProductGroupTable materialProdTable;
    
    ServiceProject serviceProjectObj;
    
    final GenricServiceAsync async=GWT.create(GenricService.class);
	

    public ProjectForm()
    {
        createGui();
        dbContractEndDate.setEnabled(false);
        dbContractStartDate.setEnabled(false);
        pic.setEnable(false);
        ibProjectId.setEnabled(false);
        serviceDate.setEnabled(false);
        serviceStaus.setEnabled(false);
        serviceEngineer.setEnabled(false);
        status.setText(ServiceProject.SCHEDULED);
        status.setEnabled(false);
        materialProdTable.connectToLocal();
        
        
    }

    public ProjectForm(SuperTable<ServiceProject> table, int mode, boolean captionmode)
    {
        super(table, mode, captionmode);
        createGui();
        dbContractEndDate.setEnabled(false);
        dbContractStartDate.setEnabled(false);
        pic.setEnable(false);
        ibProjectId.setEnabled(false);
        serviceDate.setEnabled(false);
        serviceStaus.setEnabled(false);
        serviceEngineer.setEnabled(false);
        ibContractId.setEnabled(false);
        status.setText(ServiceProject.SCHEDULED);
        status.setEnabled(false);
        materialProdTable.connectToLocal();
        
    }

    private void initalizeWidget()
    {
        ibContractId = new TextBox();
        dbContractStartDate = new DateBoxWithYearSelector();
        dbContractEndDate = new DateBoxWithYearSelector();
        pic = AppUtility.customerInfoComposite(new Customer());
        ibProjectId = new TextBox();
        tbProjectName = new TextBox();
        taComments = new TextArea();
        addressComposite = new AddressComposite();
        tbPointOfContactName = new TextBox();
      //  status = new CheckBox();
        pnbPointOfContactLandline = new PhoneNumberBox();
        pnbPointOfContactCell = new PhoneNumberBox();
        etbPointOfContactEmail = new EmailTextBox();
       
        serviceStaus=new TextBox();
        serviceEngineer=new TextBox();
        serviceDate=new DateBoxWithYearSelector();
        
        olbClientSideAsset = new ObjectListBox<ClientSideAsset>();
        AppUtility.makeClientSideAssetListBoxLive(olbClientSideAsset);
        serviceId=new TextBox();
        
        pc=AppUtility.initiateSalesProductComposite(new SuperProduct());
        
        olbToolGroup = new ObjectListBox<ToolGroup>();
        AppUtility.makeToolGroupListBoxLive(olbToolGroup);
        olbTool = new ObjectListBox<CompanyAsset>();
        AppUtility.makeCompanyAssetListBoxLive(olbTool);
        tooltable = new ToolTable();
        tooltable.connectToLocal();
        toolgrouptable = new ToolGroupTable();
        toolgrouptable.connectToLocal();
        clientsideassettable = new ClientSideAssetTable();
        clientsideassettable.connectToLocal();
        btnAddTool = new Button("Add Tool");
        btnAddTool.addClickHandler(this);
        btnAdd1 = new Button("Add ToolSet");
        btnAdd1.addClickHandler(this);
        btnClientSideAsset = new Button("Add ClientSide Asset");
        btnClientSideAsset.addClickHandler(this);
        btnEmployee = new Button("Add Employee");
        btnEmployee.addClickHandler(this);
        olbEmployee = new ObjectListBox<Employee>();
//        AppUtility.makeSalesPersonListBoxLive(olbEmployee);
        olbEmployee.makeEmployeeLive(AppConstants.SERVICEMODULE, AppConstants.CUSTOMERPROJECT, "Technician");
        emptable = new EmpolyeeTable();
        emptable.connectToLocal();
        
        pic.getCustomerId().getHeaderLabel().setText("Customer ID");
		pic.getCustomerName().getHeaderLabel().setText("Customer Name");
		pic.getCustomerCell().getHeaderLabel().setText("Customer Cell");
		adressClear=new Button("Clear");
		
		adressClear.addClickHandler(this);
		
		materialProdTable=new MaterialProductGroupTable();
		
		ibServiceSrNo=new IntegerBox();
		ibServiceSrNo.setEnabled(false);
		status=new TextBox();
		bomtable=new ProjectBillOfMaterial();
	    bomtable.connectToLocal();
	    addSteps=new Button("ADD");
	    addSteps.addClickHandler(this);
	    
	    
	    checkForProcessCongigurations();
    }

    private void checkForProcessCongigurations() {
		
	}

	@SuppressWarnings("unused")
	public void createScreen()
    {
        initalizeWidget();
        processlevelBarNames = (new String[] {"New","Mark Completed",AppConstants.ISSUEMATERIAL,AppConstants.RETURNMATERIAL,"Register Expense"});
        
        
        FormFieldBuilder fbuilder = new FormFieldBuilder();
       
        fbuilder = new FormFieldBuilder("* Service ID", serviceId);
        FormField fibServiceId = fbuilder.setMandatory(true).setMandatoryMsg("Service Id is Mandatory !").setRowSpan(0).setColSpan(0).build();
        
        
        
        FormField fgroupingContractDetails = fbuilder.setlabel("Service Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
        fbuilder = new FormFieldBuilder("* Contract Id", ibContractId);
        FormField fibContractId = fbuilder.setMandatory(true).setMandatoryMsg("Contract Id is Mandatory !").setRowSpan(0).setColSpan(0).build();
        //fbuilder = new FormFieldBuilder("Status", status);
        //FormField fieldstatus = fbuilder.build();
        fbuilder = new FormFieldBuilder("Contract Start Date", dbContractStartDate);
        FormField fibContractStartDate = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
        fbuilder = new FormFieldBuilder("Contract End Date", dbContractEndDate);
        FormField fibContractEndDate = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
        FormField fgroupingProjectDetails = fbuilder.setlabel("Project Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
        fbuilder = new FormFieldBuilder("* Project Id", ibProjectId);
        FormField fibProjectId = fbuilder.setMandatory(false).setMandatoryMsg("Project Id is Mandatory !").setRowSpan(0).setColSpan(0).build();
        fbuilder = new FormFieldBuilder("* Project Name", tbProjectName);
        FormField ftbProjectName = fbuilder.setMandatory(true).setMandatoryMsg("Project Name is Mandatory !").setRowSpan(0).setColSpan(0).build();
        fbuilder = new FormFieldBuilder("Project Comments", taComments);
        FormField ftaComments = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
        FormField fgroupingAddressInformation = fbuilder.setlabel("Address Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
        fbuilder = new FormFieldBuilder("", addressComposite);
        FormField faddressComposite = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
        FormField fgroupingPersonInformation = fbuilder.setlabel("Person Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
        fbuilder = new FormFieldBuilder("Point Of Contact Name", tbPointOfContactName);
        FormField ftbPointOfContactName = fbuilder.setMandatory(false).setMandatoryMsg("Point Of Contact Name is Mandatory !").setRowSpan(0).setColSpan(0).build();
        fbuilder = new FormFieldBuilder("Point Of Contact Landline", pnbPointOfContactLandline);
        FormField fpnbPointOfContactLandline = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
        fbuilder = new FormFieldBuilder("Point Of Contact Cell", pnbPointOfContactCell);
        FormField fpnbPointOfContactCell = fbuilder.setMandatory(false).setMandatoryMsg("Point Of Contact Cell is Mandatory !").setRowSpan(0).setColSpan(0).build();
        fbuilder = new FormFieldBuilder("Point Of Contact Email", etbPointOfContactEmail);
        FormField fetbPointOfContactEmail = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
        FormField fgroupingClientAssetInfo = fbuilder.setlabel("Client Asset Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
        fbuilder = new FormFieldBuilder("Client Asset", olbClientSideAsset);
        FormField folbClientSideAsset = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
        FormField fgroupingToolAssetInfo = fbuilder.setlabel("Tool Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
        FormField fgroupingToolSetInfo = fbuilder.setlabel("ToolSet Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
        fbuilder = new FormFieldBuilder("Tool", olbTool);
        FormField folbTool = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
        fbuilder = new FormFieldBuilder("", btnAddTool);
        FormField fbtnAdd = fbuilder.build();
        fbuilder = new FormFieldBuilder("Tool Set", olbToolGroup);
        FormField folbToolGroup = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
        fbuilder = new FormFieldBuilder("", btnAdd1);
        FormField fbtnAdd1 = fbuilder.build();
        fbuilder = new FormFieldBuilder("", btnClientSideAsset);
        FormField fbtnclientAsset = fbuilder.build();
        FormField fgroupingProjectInfo = fbuilder.setlabel("Project Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
        fbuilder = new FormFieldBuilder("", tooltable.getTable());
        FormField ftooltable = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
        fbuilder = new FormFieldBuilder("", toolgrouptable.getTable());
        FormField ftoolgrouptable = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
        fbuilder = new FormFieldBuilder("", clientsideassettable.getTable());
        FormField fclientsideassettable = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
        FormField fgroupingEmployeeInfo = fbuilder.setlabel("Technician Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
        fbuilder = new FormFieldBuilder("Employee", olbEmployee);
        FormField folbEmployee = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
        fbuilder = new FormFieldBuilder("", btnEmployee);
        FormField fbtnEmp = fbuilder.build();
        fbuilder = new FormFieldBuilder("", emptable.getTable());
        FormField femptable = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
        
        fbuilder = new FormFieldBuilder("Service Status",serviceStaus);
        FormField fserviceStatus = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
        fbuilder = new FormFieldBuilder("Service Date",serviceDate);
        FormField fServiceDate = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
        
        fbuilder = new FormFieldBuilder("", materialProdTable.getTable());
 	     FormField fmaterialProdTable = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
        
        fbuilder = new FormFieldBuilder("",adressClear);
        FormField fclear = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
        
        fbuilder = new FormFieldBuilder("Status",status);
        FormField fstatus = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
//        fbuilder = new FormFieldBuilder("Service Engineer",serviceEngineer);
        /**@Sheetal:17-03-2022 
		 *  Des : Renaming Service Engineer to Technician, requirement by nitin sir**/
        fbuilder = new FormFieldBuilder("Technician",serviceEngineer);
        FormField fServiceEngineer = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
        fbuilder = new FormFieldBuilder();
        
        FormField fgroupingCustomerInformation = fbuilder.setlabel("Customer Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
        fbuilder = new FormFieldBuilder("", pic);
         fpic = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
         
         fbuilder = new FormFieldBuilder();
         FormField fMaterialrequired = fbuilder.setlabel("Material Required").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
         fbuilder = new FormFieldBuilder("", bomtable.getTable());
 	     FormField fbomtable = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
 	     
 	    fbuilder = new FormFieldBuilder("",addSteps);
		 FormField faddStep= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		 
		fbuilder = new FormFieldBuilder("",pc);
		FormField fpc =fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		
		 fbuilder = new FormFieldBuilder("Service Sr No", ibServiceSrNo);
	     FormField fibServiceSrNo = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
         
         
         
        FormField formfield[][] = {
            {fgroupingContractDetails}, 
            {fibServiceId,fibServiceSrNo,fServiceEngineer,fServiceDate,},
            {fserviceStatus,fibContractId, fibContractStartDate, fibContractEndDate},
            {fpic}, 
            {fgroupingProjectDetails},
            {fibProjectId, ftbProjectName,fstatus},
            {ftaComments}, 
            {fgroupingAddressInformation}, 
            {fclear},
            {faddressComposite},
            {fgroupingPersonInformation}, 
            {ftbPointOfContactName, fpnbPointOfContactLandline, fpnbPointOfContactCell, fetbPointOfContactEmail}, 
            {fgroupingEmployeeInfo},
            {folbEmployee, fbtnEmp},
            {femptable}, 
            {fgroupingClientAssetInfo}, 
            {folbClientSideAsset, fbtnclientAsset}, 
            {fclientsideassettable}, 
            {fgroupingToolAssetInfo}, 
            {folbToolGroup, fbtnAdd1, folbTool, fbtnAdd}, 
            {ftooltable},
            {fMaterialrequired},
          {fpc,faddStep},
          {fmaterialProdTable}
        };
        fields = formfield;
    }

    @SuppressWarnings("unchecked")
	public void updateModel(ServiceProject model)
    {
    	if(!serviceId.getValue().equals(""))
    		model.setserviceId(Integer.parseInt(serviceId.getValue()));
    	if(!ibContractId.getValue().equals(""))
    		model.setContractId(Integer.parseInt(ibContractId.getValue()));
        if(tbProjectName.getValue() != null)
            model.setProjectName(tbProjectName.getValue());
        if(tbPointOfContactName.getValue() != null)
            model.setPocName(tbPointOfContactName.getValue());
        if(taComments.getValue() != null)
            model.setComment(taComments.getValue());
        if(addressComposite.getValue() != null)
            model.setAddr(addressComposite.getValue());
        if(etbPointOfContactEmail.getValue() != null)
            model.setPocEmail(etbPointOfContactEmail.getValue());
        if(pnbPointOfContactLandline.getValue() != null)
            model.setPocLandline(((Long)pnbPointOfContactLandline.getValue()).longValue());
        if(pnbPointOfContactCell.getValue() != null)
            model.setPocCell(((Long)pnbPointOfContactCell.getValue()).longValue());
        if(ibServiceSrNo.getValue()!=null)
        	model.setServiceSrNo(ibServiceSrNo.getValue());
        
        if(olbToolGroup.getValue() != null)
            model.setToolGroup(olbToolGroup.getValue());
        if(olbTool.getValue() != null)
            model.setTool(olbTool.getValue());
        if(olbEmployee.getValue() != null)
            model.setEmp(olbEmployee.getValue());
        if(serviceDate.getValue() != null)
            model.setserviceDate(serviceDate.getValue());
        if(serviceStaus.getValue() != null)
        {
            System.out.println("Service Status is "+serviceStaus.getValue());
        	model.setServiceStatus(serviceStaus.getValue());
            
        }
        if(serviceEngineer.getValue() != null)
            model.setserviceEngineer(serviceEngineer.getValue());
        if(pic.getValue() != null)
            model.setPersonInfo(pic.getValue());
        if(dbContractStartDate.getValue() != null)
            model.setContractStartDate(dbContractStartDate.getValue());
        if(dbContractEndDate.getValue() != null)
            model.setContractEndDate(dbContractEndDate.getValue());
       // if(status.getValue() != null)
         //   model.setStatus(status.getValue().booleanValue());
        if(tooltable.getValue() != null)
            model.setTooltable(tooltable.getValue());
        if(clientsideassettable.getValue() != null)
            model.setClientsideAssettable(clientsideassettable.getValue());
        if(emptable.getValue() != null)
            model.setTechnicians(emptable.getValue());
        if(status.getValue()!=null)
        	model.setProjectStatus(status.getValue());
        model.setBillMaterialLis(bomtable.getValue());
        
        model.setProdDetailsList(materialProdTable.getValue());
        
        /**
         *  nidhi
         *  28-02-2018
         *  for map text box poc name in service project and min
         */
        if(pic.getTbpocName().getValue()!=null){
        	  model.getPersonInfo().setPocName(pic.getTbpocName().getValue());
              
        }
      /**
         * end
         */
        
        serviceProjectObj = model;
        
        presenter.setModel(model);
    }

    @SuppressWarnings("unchecked")
	public void updateView(ServiceProject view)
    {
    	serviceProjectObj=view;
    	
    	/**
		 * Date 20-04-2018 By vijay for orion pest locality load  
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Locality", "DontLoadAllLocalityAndCity")){
			setAddressDropDown();
		}
		/**
		 * ends here
		 */
		
        if(view.getserviceId()!=-1)
        	serviceId.setValue(view.getserviceId()+"");
    	if(view.getCount() != -1)
            ibProjectId.setValue(view.getCount()+"");
        if(view.getContractId() != -1)
            ibContractId.setValue(view.getContractId()+"");
        if(view.getProjectName() != null)
            tbProjectName.setValue(view.getProjectName());
        if(view.getPocName() != null)
            tbPointOfContactName.setValue(view.getPocName());
        if(view.getComment() != null)
            taComments.setValue(view.getComment());
        if(view.getAddr() != null)
            addressComposite.setValue(view.getAddr());
        if(view.getPocEmail() != null)
            etbPointOfContactEmail.setValue(view.getPocEmail());
        if(view.getPocLandline() != null)
            pnbPointOfContactLandline.setValue(view.getPocLandline());
        if(view.getPocCell() != null)
            pnbPointOfContactCell.setValue(view.getPocCell());
        //if(view.getClientSideAsset() != null)
           // olbClientSideAsset.setValue(view.getClientSideAsset());
        if(view.getServiceSrNo()!=null)
        	ibServiceSrNo.setValue(view.getServiceSrNo());
        if(view.getTool() != null)
            olbTool.setValue(view.getTool());
        if(view.getToolGroup() != null)
            olbToolGroup.setValue(view.getToolGroup());
        if(view.getEmp() != null)
            olbEmployee.setValue(view.getEmp());
        if(view.getPersonInfo() != null)
            pic.setValue(view.getPersonInfo());
        
        if(view.getContractStartDate() != null)
            dbContractStartDate.setValue(view.getContractStartDate());
        if(view.getContractEndDate() != null)
            dbContractEndDate.setValue(view.getContractEndDate());
        if(view.getserviceDate()!=null)
            serviceDate.setValue(view.getserviceDate());
        if(view.getserviceEngineer()!=null)
            serviceEngineer.setValue(view.getserviceEngineer());
        
//        materialProdTable.setValue(view.getProdDetailsList());
        
        if(view.getServiceStatus()!=null)
            serviceStaus.setValue(view.getServiceStatus());
        
        {
        	ProjectPresenter prPresenter=(ProjectPresenter) presenter;
        	prPresenter.makeClientAssetListBoxLive(view.getCount());
        }
        if(view.getTooltable() != null)
            tooltable.setValue(view.getTooltable());
        if(view.getClientsideAssettable() != null)
            clientsideassettable.setValue(view.getClientsideAssettable());
          emptable.setValue(view.getTechnicians());
        if(view.getProjectStatus() != null)
            status.setValue(view.getProjectStatus());
        
        if(view.getBillMaterialLis().size()!=0)
        {
        	dataToProductListType(view.getBillMaterialLis());
        }
        else{
        	System.out.println("Bill Of MaterialExists Else");
        	materialProdTable.setValue(view.getProdDetailsList());
        }
        //status.setValue(Boolean.valueOf(view.isStatus()));
        bomtable.setValue(view.getBillMaterialLis());
        presenter.setModel(view);
    }

    /**
   	 * Date 20-04-2018 By vijay
   	 * here i am refreshing address composite data if global list size greater than drop down list size
   	 * because we are adding locality and city from an entity in view state to global list if doest not exist in global list 
   	 */
   	private void setAddressDropDown() {
   		if(LoginPresenter.globalLocality.size()>addressComposite.locality.getItems().size()){
   			addressComposite.locality.setListItems(LoginPresenter.globalLocality);
   		}
   	}
   	/**
   	 * ends here 
   	 */
    
    public void toggleAppHeaderBarMenu()
    {
        if(AppMemory.getAppMemory().currentState == ScreeenState.NEW)
        {
            InlineLabel menus[] = AppMemory.getAppMemory().skeleton.getMenuLabels();
            for(int k = 0; k < menus.length; k++)
            {
                String text = menus[k].getText();
                if(text.equals("Save") || text.equals("Discard") || text.equals("Search")||text.contains(AppConstants.NAVIGATION))
                    menus[k].setVisible(true);
                else
                    menus[k].setVisible(false);
            }

        } else
        if(AppMemory.getAppMemory().currentState == ScreeenState.EDIT)
        {
            InlineLabel menus[] = AppMemory.getAppMemory().skeleton.getMenuLabels();
            for(int k = 0; k < menus.length; k++)
            {
                String text = menus[k].getText();
                if(text.equals("Save") || text.equals("Discard")||text.contains(AppConstants.NAVIGATION))
                    menus[k].setVisible(true);
                else
                    menus[k].setVisible(false);
            }

        } else
        if(AppMemory.getAppMemory().currentState == ScreeenState.VIEW)
        {
            InlineLabel menus[] = AppMemory.getAppMemory().skeleton.getMenuLabels();
            for(int k = 0; k < menus.length; k++)
            {
                String text = menus[k].getText();
                if(text.equals("Discard") || text.equals("Edit") || text.equals("Search")||text.contains(AppConstants.NAVIGATION))
                    menus[k].setVisible(true);
                else
                    menus[k].setVisible(false);
            }

        }
        AuthorizationHelper.setAsPerAuthorization(Screen.PROJECT,LoginPresenter.currentModule.trim());
    }
    
    
    public void toggleAppHeaderBarAsperStatus()
    {
    	 InlineLabel menus[] = AppMemory.getAppMemory().skeleton.getMenuLabels(); 
    	for(int k = 0; k < menus.length; k++)
         {
             String text = menus[k].getText();
             if(text.equals("Save") || text.equals("Edit"))
                 menus[k].setVisible(false);
             
         }
    }

    @Override
	public void setToViewState() {
		
		super.setToViewState();
		setMenuAsPerStatus();
		
		//    rohan added this code for navigation
		SuperModel model=new ServiceProject();
		model=serviceProjectObj;
		AppUtility.addDocumentToHistoryTable(AppConstants.SERVICEMODULE,AppConstants.CUSTOMERPROJECT, serviceProjectObj.getCount(), Integer.parseInt(pic.getId().getValue()),pic.getName().getValue(),Long.parseLong(pic.getPhone().getValue()), false, model, null);
		
	}
    
    public void setAppHeaderBarAsPerStatus()
	{
		ServiceProject entity=(ServiceProject) presenter.getModel();
		String status=entity.getProjectStatus();
		
		if(status.equals(ServiceProject.COMPLETED))
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Discard")||text.contains("Search")||text.contains(AppConstants.NAVIGATION)){
					menus[k].setVisible(true); 
					System.out.println("Value of text is "+menus[k].getText());
				}
				else
				{
					menus[k].setVisible(false);  
					

				}

			}

		}

		


		

	}
    
    public void toggleProcessLevelMenu()
	{
		ServiceProject entity=(ServiceProject) presenter.getModel();
		
		String status=entity.getProjectStatus();
        String serviceStatus=entity.getServiceStatus();
		for(int i=0;i<getProcesslevelBarNames().length;i++)
		{
			InlineLabel label=getProcessLevelBar().btnLabels[i];
			String text=label.getText().trim();

			if(status.equals(ServiceProject.COMPLETED)||serviceStatus.equals(Service.SERVICESTATUSCANCELLED)||
					serviceStatus.equals(Service.SERVICESTATUSCLOSED)||serviceStatus.equals(Service.SERVICESTATUSCOMPLETED))
			{
				if(text.equals("Mark Completed")){
					label.setVisible(false);
				}
				
				if(text.equals("Delete")){
					label.setVisible(false);
				}
				
				if(text.equals(AppConstants.ISSUEMATERIAL)){
					label.setVisible(false);
				}
				if(text.equals(AppConstants.RETURNMATERIAL)){
					label.setVisible(false);
				}
				if(text.equals("New"))
					label.setVisible(false);
			}
			
		}	

	}
	/**
	 * The method is responsible for changing Application state as per 
	 * status
	 */
	public void setMenuAsPerStatus()
	{
		this.setAppHeaderBarAsPerStatus();
		this.toggleProcessLevelMenu();
		
	}

	public void setCount(int count)
    {
        ibProjectId.setValue(count+"");
    }

    public void onClick(ClickEvent event)
    {
        if(event.getSource().equals(btnAddTool))
        {
            if(this.olbTool.getValue()!=null)
        	  reactOnAddTool();
            
        }
        if(event.getSource().equals(btnClientSideAsset))
        {
        	if(this.olbClientSideAsset.getValue()!=null)
        		reactOnAddClientAsset();
        }
        if(event.getSource().equals(btnEmployee))
        {
        	if(this.olbEmployee.getValue()!=null)
        		reactOnAddEmployee();
        }
        if(event.getSource().equals(btnAdd1))
        {
        	if(this.olbToolGroup.getValue()!=null)
        		reactOnToolSet();
        }
        if(event.getSource().equals(adressClear))
        {
        	addressComposite.clear();
        }
        
        if(event.getSource().equals(addSteps)){
    		
        	if(pc.getValue()==null)
			{
				showDialogMessage("Please select product!");	
			}
        	
        	boolean flagValueProd = validateProductData(pc.getProdID().getValue().trim());
        	if(flagValueProd==true&&pc.getValue()!=null)
        	{
        		showDialogMessage("Product already added!");
        	}
        	else{
        		addDataToMaterialTable();
        	}
    	}
    }
    
    public void dataToProductListType(List<BillProductDetails> billProdList)
    {
    	
    	if(billProdList.size()!=0)
    	{
    		for(int h=0;h<billProdList.size();h++)
    		{
    			MyQuerry querry=new MyQuerry();
    			Vector<Filter> filtervec=new Vector<Filter>();
    			Company c=new Company();
    			Filter temp=null;
    			temp=new Filter();
    			temp.setQuerryString("companyId");
    			temp.setLongValue(c.getCompanyId());
    			filtervec.add(temp);
    			
    			temp=new Filter();
    			temp.setQuerryString("count");
    			temp.setIntValue(billProdList.get(h).getProdGroupId());
    			filtervec.add(temp);
    			
//    			temp=new Filter();
//    			temp.setQuerryString("product_id");
//    			temp.setIntValue(billProdList.get(h).getProdId());
//    			filtervec.add(temp);
    			
    			querry.setFilters(filtervec);
    			querry.setQuerryObject(new ProductGroupDetails());
    			
    			async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {

						@Override
						public void onFailure(Throwable caught) {
							showDialogMessage("An Unexpected Error Occured !");
							
						}
						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							
							System.out.println("Result"+result.size());
							for(SuperModel bmodel:result)
							{
								ProductGroupDetails pglentity =(ProductGroupDetails)bmodel;
								
								for(int b=0;b<pglentity.getPitems().size();b++)
								{
									ProductGroupList prodListData=new ProductGroupList();
					    			prodListData.setProduct_id(pglentity.getPitems().get(b).getProduct_id());
					    			prodListData.setCode(pglentity.getPitems().get(b).getCode());
					    			prodListData.setName(pglentity.getPitems().get(b).getName());
					    			prodListData.setProductGroupId(pglentity.getPitems().get(b).getProductGroupId());
					    			prodListData.setTitle(pglentity.getPitems().get(b).getTitle());
					    			prodListData.setQuantity(pglentity.getPitems().get(b).getQuantity());
					    			if(pglentity.getPitems().get(b).getUnit()!=null){
					    				prodListData.setUnit(pglentity.getPitems().get(b).getUnit());
					    			}
					    			materialProdTable.getDataprovider().getList().add(prodListData);
								}
								
							}
						}
					});
    		}
    	}
    }
    
    
    private void addDataToMaterialTable()
    {
    	ProductGroupList pgdEntity=new ProductGroupList();	
		pgdEntity.setCode(pc.getProdCode().getValue());
		pgdEntity.setName(pc.getProdName().getValue());
//			pgdEntity.setQuantity(dbQuantity.getValue());
		pgdEntity.setUnit(pc.getUnitOfmeasuerment().getValue());
		pgdEntity.setProduct_id(Integer.parseInt(pc.getProdID().getValue()));
		
		materialProdTable.getDataprovider().getList().add(pgdEntity);
    }
    
    private boolean validateProductData(String prodId)
    {
    	if(this.getMaterialProdTable().getDataprovider().getList().size()!=0){
	    	int productId=Integer.parseInt(prodId);
			List<ProductGroupList> materialList=materialProdTable.getDataprovider().getList();
			for(int i=0;i<materialList.size();i++)
			{
				if(materialList.get(i).getProduct_id()==productId){
					return true;
				}
			}
    	}
		return false;
	}

    private void reactOnAddEmployee()
    {
    	if(olbEmployee.getSelectedIndex()==0)
    	{
    		showDialogMessage("Please select employee!");
    	}
    	
    	if(olbEmployee.getSelectedIndex()!=0)
    	{
    		List<EmployeeInfo> list = emptable.getDataprovider().getList();
    		
    		if(list.size()!=0)
    		{
    			if(validateEmployees()==true){
    				addToEmployeeTable();
				}
			}
			else{
				addToEmployeeTable();
			}
    		olbEmployee.setSelectedIndex(0);
    	}
    }

    public boolean validateEmployees()
  	{
  		List<EmployeeInfo> empList=emptable.getDataprovider().getList();
  		
  		if(empList.size()!=0){
  			for(int i=0;i<empList.size();i++)
  			{
  				if(empList.get(i).getFullName().trim().equals(olbEmployee.getValue()))
  				{
  					showDialogMessage("Employee already exists!");
  					return false;
  				}
  			}
  		}
  		return true;
  	}
      
      protected void addToEmployeeTable(){
  		
      	Employee empEntity=olbEmployee.getSelectedItem();
  		EmployeeInfo empInfoEntity = new EmployeeInfo();
  		
  		empInfoEntity.setEmpCount(empEntity.getCount());
  		empInfoEntity.setFullName(empEntity.getFullName());
      	empInfoEntity.setCellNumber(empEntity.getCellNumber1());
      	empInfoEntity.setDesignation(empEntity.getDesignation());
      	empInfoEntity.setDepartment(empEntity.getDepartMent());
      	empInfoEntity.setEmployeeType(empEntity.getEmployeeType());
      	empInfoEntity.setEmployeerole(empEntity.getRoleName());
      	empInfoEntity.setBranch(empEntity.getBranchName());
      	empInfoEntity.setCountry(empEntity.getCountry());
      	
  		emptable.getDataprovider().getList().add(empInfoEntity);
  		
  	}
    
    
    private void reactOnAddClientAsset()
    {
        ClientSideAsset asset = olbClientSideAsset.getSelectedItem();
        ServicedClientSideAsset serv=new ServicedClientSideAsset();
        serv.setAsset(asset);
        List<ServicedClientSideAsset> list = clientsideassettable.getDataprovider().getList();
        if(list != null)
        {
            if(list.contains(serv)==false)
        	    list.add(serv);
            
        }
        olbClientSideAsset.setSelectedIndex(0);
    }

    private void reactOnAddTool()
    {
        CompanyAsset asset = olbTool.getSelectedItem();
        List<CompanyAsset> list = tooltable.getDataprovider().getList();
        if(list != null)
        {
            if(list.contains(asset)==false)
        	   list.add(asset);
            olbTool.setSelectedIndex(0);
        }
        olbTool.setSelectedIndex(0);
    }
    
    
    private void addingNoOfServ() {
    	
    	final MyQuerry querry = new MyQuerry();
		final Company c=new Company();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		filter = new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(Integer.parseInt(pc.getProdID().getValue()));
		filtervec.add(filter);
		filter=new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new ServiceProduct());
		
		async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				if (result.size() != 0) {
					ServiceProduct sup=(ServiceProduct) result.get(0);
					
					final int noOfSer=sup.getNumberOfServices();
					
					Vector<Filter> filtervec=new Vector<Filter>();
					Filter filter = null;
					filter = new Filter();
					filter.setQuerryString("product_id");
					filter.setIntValue(Integer.parseInt(pc.getProdID().getValue()));
					filtervec.add(filter);
					filter=new Filter();
					filter.setQuerryString("companyId");
					filter.setLongValue(c.getCompanyId());
					filtervec.add(filter);
					querry.setFilters(filtervec);
					querry.setQuerryObject(new ProductGroupList());
					
					async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
						@Override
						public void onFailure(Throwable caught) {
						}

						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							if (result.size() != 0) {
								ProductGroupList sup=(ProductGroupList) result.get(0);
								
								int PGD = sup.getProductGroupId();
								String title = sup.getTitle();
								
								ArrayList<BillProductDetails> list=new ArrayList<BillProductDetails>();
							
								
								
								for(int i=1;i<=noOfSer;i++){
									
									BillProductDetails bill=new BillProductDetails();
									
										bill.setServiceNo(i);
										bill.setProdId(Integer.parseInt(pc.getProdID().getValue()));
										bill.setProdName(pc.getProdName().getValue());
										bill.setProdCode(pc.getProdCode().getValue());
										bill.setProdGroupTitle(title);
										bill.setProdGroupId(PGD);
										list.add(bill);
										
								}
								bomtable.getDataprovider().getList().addAll(list);	
							}
						}
					});
					
			    }
			}	
			@Override
			public void onFailure(Throwable caught) {
					
			}
		});
	}
    
	private void reactOnToolSet()
    {
        ToolGroup toolgrp = olbToolGroup.getSelectedItem();
        List<CompanyAsset> array = toolgrp.getArraylistTool();
        List<CompanyAsset>compast= tooltable.getDataprovider().getList();
        if(array != null)
        {
        	for(CompanyAsset ast:array)
        	{
        	    if(compast.contains(ast)==false)
        		   tooltable.getDataprovider().getList().add(ast);
        	}  
        }
        olbToolGroup.setSelectedIndex(0);
    }

    public void clear()
    {
        super.clear();
        emptable.clear();
        clientsideassettable.clear();
        materialProdTable.clear();
        tooltable.clear();
        status.setText(ServiceProject.SCHEDULED);
    }

    public PersonInfoComposite getPic()
    {
        return pic;
    }

    public void setPic(PersonInfoComposite pic)
    {
        this.pic = pic;
    }

    
    public DateBox getDbContractStartDate()
    {
        return dbContractStartDate;
    }

    public void setDbContractStartDate(DateBox dbContractStartDate)
    {
        this.dbContractStartDate = dbContractStartDate;
    }

    public DateBox getDbContractEndDate()
    {
        return dbContractEndDate;
    }

    public void setDbContractEndDate(DateBox dbContractEndDate)
    {
        this.dbContractEndDate = dbContractEndDate;
    }

    public void initalizeClientSideAssetComboBox()
    {
       
        MyQuerry querry = new MyQuerry();
        querry.setQuerryObject(new ClientSideAsset());
        Filter categoryFilter = new Filter();
        categoryFilter.setQuerryString("ContractId");
        int id = Integer.parseInt(getIbContractId().getValue());
        System.out.println(id);
        categoryFilter.setIntValue(Integer.valueOf(id));
        querry.getFilters().add(categoryFilter);
        olbClientSideAsset.MakeLive(querry);
    }

    @Override
	 public boolean validate()
	 {
		 boolean superValidate = super.validate();
		 if(superValidate==false){
       	 return false;
       }
      
       if(!validateProjectProdQty()){
			showDialogMessage("Please enter appropriate quantity required!");
			return false;
		}
       return true;
	 }
    
    public boolean validateProjectProdQty()
	{
		List<ProductGroupList> prodlis=materialProdTable.getDataprovider().getList();
		int ctr=0;
		for(int i=0;i<prodlis.size();i++)
		{
			if(prodlis.get(i).getQuantity()==0)
			{
				ctr=ctr+1;
			}
		}
		
		if(ctr==0){
			return true;
		}
		else{
			return false;
		}
	}

    public void setEnable(boolean state)
    {
        super.setEnable(state);
        dbContractEndDate.setEnabled(false);
        dbContractStartDate.setEnabled(false);
        pic.setEnable(false);
        ibProjectId.setEnabled(false);
        tooltable.setEnable(state);
        clientsideassettable.setEnable(state);
        emptable.setEnable(state);
        serviceEngineer.setEnabled(false);
        serviceDate.setEnabled(false);
        serviceStaus.setEnabled(false);
        System.out.println("ID OF MODEL "+presenter.getModel().getId());
        if(presenter.getModel().getId()!=null)
        		serviceId.setEnabled(false);
        this.ibContractId.setEnabled(false);
        status.setEnabled(false);
        ibServiceSrNo.setEnabled(false);
        
        //Date 04-04-2017 added by vijay
        materialProdTable.setEnable(state);
    }
    
   
    
    public boolean isServicedAll1()
    {
    	List<ServicedClientSideAsset> array=clientsideassettable.getDataprovider().getList();
    	if(array.size()==0)
    		return false;
    	if(array==null)
    		return  false;
    	boolean val=true;
    	for(int i=0;i<array.size();i++)
    	{
    		if(array.get(i).isServicedStatus()==false)
    		  return false;
    	}
    	
    	return true;
    }
    
    
    @Override
	public void setToEditState() {
		super.setToEditState();
		
		this.processLevelBar.setVisibleFalse(false);
	}

	public TextBox getIbContractId() {
		return ibContractId;
	}

	public void setIbContractId(TextBox ibContractId) {
		this.ibContractId = ibContractId;
	}

	public TextBox getServiceId() {
		return serviceId;
	}

	public void setServiceId(TextBox serviceId) {
		this.serviceId = serviceId;
	}

	public TextBox getIbProjectId() {
		return ibProjectId;
	}

	public void setIbProjectId(TextBox ibProjectId) {
		this.ibProjectId = ibProjectId;
	}

	public MaterialProductGroupTable getMaterialProdTable() {
		return materialProdTable;
	}

	public void setMaterialProdTable(MaterialProductGroupTable materialProdTable) {
		this.materialProdTable = materialProdTable;
	}
	
   
    
   /* public boolean isTestReportAttach()
    {
    	ServicedClientSideAsset array=(ServicedClientSideAsset) clientsideassettable.getDataprovider().getList();
    	if(array==null)
    		return  false;
    	if(array.isTestreportattach()==true)
    	{
    		DocumentUploadPopUp popup=new DocumentUploadPopUp();
    	}	 
    	
    	return false;
    }*/
}
