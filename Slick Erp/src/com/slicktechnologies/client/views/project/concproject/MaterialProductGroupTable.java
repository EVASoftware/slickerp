package com.slicktechnologies.client.views.project.concproject;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.inventory.MaterialProduct;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;

public class MaterialProductGroupTable extends SuperTable<ProductGroupList> {
	
	TextColumn<ProductGroupList>getProductIdcolumn;
	TextColumn<ProductGroupList>getCodecolumn;
	TextColumn<ProductGroupList>getNamecolumn;
	TextColumn<ProductGroupList>getQuantitycolumn;
	TextColumn<ProductGroupList>getUnitcolumn;
//	TextColumn<ProductGroupList>viewcolumnProductQuantity;
	Column<ProductGroupList, String> columnProductQuantity;
	private Column<ProductGroupList,String>Delete;
	
	/** Date 05 oct 2017 added by vijay for Technician Scheduling info need to map here and for manual inventory info ***/
	Column<ProductGroupList, String> addColumn;
	TextColumn<ProductGroupList> warehouseViewColumn;
	TextColumn<ProductGroupList> columnviewBin;
	TextColumn<ProductGroupList> columnviewLoc;
	TextColumn<ProductGroupList> getColumnReturnQuantity;
	
	/*** Ends here *******************/
	
	/**
	 * nidhi ||*|| 29-12-2018
	 */
	TextColumn<ProductGroupList> getViewColumnProductPlannedQty;
	
	/**
	 * @author Anil @since 30-09-2021
	 * Adding Mixing no. and lot no. field for innovative raised by Rahul Tiwari
	 */
	TextColumn<ProductGroupList> viewColMixingNo;
	TextColumn<ProductGroupList> viewColLotNo;
	Column<ProductGroupList, String> editColMixingNo;
	Column<ProductGroupList, String> editColLotNo;
	
	NumberFormat nf=NumberFormat.getFormat("0.00");
	public MaterialProductGroupTable(){
		super();
	}
	
	public void createTable(){
		addColumngetProductId();
		addColumngetCode();
		addColumngetName();
		/**
		 *nidhi ||*|| 29-12-2018 for display planned qty
		 */
		if(LoginPresenter.billofMaterialActive){
			addColumnProductViewPlannedQty();
		}
		addColumnProductQuantity();
		addColumngetUnit();
		//added by vijay for warehouse info
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "MINBasedOnConsumedQuantity")){
			addColumnViewConsumedQuantity();
		}else{
			addColumnViewReturnQuantity();
		}
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "PC_ServiceReportV3")){
			editColMixingNo();
			editColLotNo();
		}
		
		viewWarehouse();
		viewstarageLoc();
		viewstorageBin();
		addColumnDelete();
		addFieldUpdater();
	}
	
	private void editColLotNo() {
		EditTextCell editCell = new EditTextCell();
		editColLotNo = new Column<ProductGroupList, String>(editCell) {
			@Override
			public String getValue(ProductGroupList object) {
				if(object.getLotNo()!=null){
					return object.getLotNo();
				}
				return "";
			}
		};
		table.addColumn(editColLotNo, "#Lot No.");
		table.setColumnWidth(editColLotNo,153,Unit.PX);
		
		editColLotNo.setFieldUpdater(new FieldUpdater<ProductGroupList, String>() {
			@Override
			public void update(int index, ProductGroupList object,String value) {
				try {
					object.setLotNo(value);
				}catch(Exception e) {
				}
				table.redrawRow(index);
			}
		});
	}

	private void editColMixingNo() {
		EditTextCell editCell = new EditTextCell();
		editColMixingNo = new Column<ProductGroupList, String>(editCell) {
			@Override
			public String getValue(ProductGroupList object) {
				if(object.getMixingRatio()!=null){
					return object.getMixingRatio();
				}
				return "";
			}
		};
		table.addColumn(editColMixingNo, "#Mixing Ratio");
		table.setColumnWidth(editColMixingNo,153,Unit.PX);
		
		editColMixingNo.setFieldUpdater(new FieldUpdater<ProductGroupList, String>() {
			@Override
			public void update(int index, ProductGroupList object,String value) {
				try {
					object.setMixingRatio(value);
				}catch(Exception e) {
				}
				table.redrawRow(index);
			}
		});
	}

	private void viewColLotNo() {
		viewColLotNo = new TextColumn<ProductGroupList>() {
			@Override
			public String getValue(ProductGroupList object) {
				if(object.getLotNo()!=null){
					return object.getLotNo();
				}
				return "";
			}
		};
		table.addColumn(viewColLotNo, "Lot No.");
		table.setColumnWidth(viewColLotNo,153,Unit.PX);
	}

	private void viewColMixingNo() {
		viewColMixingNo = new TextColumn<ProductGroupList>() {
			@Override
			public String getValue(ProductGroupList object) {
				if(object.getMixingRatio()!=null){
					return object.getMixingRatio();
				}
				return "";
			}
		};
		table.addColumn(viewColMixingNo, "Mixing Ratio");
		table.setColumnWidth(viewColMixingNo,153,Unit.PX);
	}

	public void addeditColumn() {
		addColumngetProductId();
		addColumngetCode();
		addColumngetName();
		/**
		 *nidhi ||*|| 29-12-2018 for display planned qty
		 */
		if(LoginPresenter.billofMaterialActive){
			addColumnProductViewPlannedQty();
		}
		addColumnProductQuantity();
		addColumngetUnit();
		//added by vijay for warehouse info
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "MINBasedOnConsumedQuantity")){
			addColumnViewConsumedQuantity();
		}else{
			addColumnViewReturnQuantity();
		}
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "PC_ServiceReportV3")){
			editColMixingNo();
			editColLotNo();
		}
		
		viewWarehouse();
		viewstarageLoc();
		viewstorageBin();
		addColumnDelete();
		addFieldUpdater();
		
	}
	
	public void addViewColumn() {
		
		addColumngetProductId();
		addColumngetCode();
		addColumngetName();
		/**
		 *nidhi ||*|| 29-12-2018 for display planned qty
		 */
		if(LoginPresenter.billofMaterialActive){
			addColumnProductViewPlannedQty();
		}
		addColumngetQuantity();
//		viewColumnProductQuantity();
		addColumngetUnit();
		
		//added by vijay for warehouse info
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "MINBasedOnConsumedQuantity")){
			addColumnViewConsumedQuantity();
		}else{
			addColumnViewReturnQuantity();
		}
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "PC_ServiceReportV3")){
			viewColMixingNo();
			viewColLotNo();
		}
		
		viewWarehouse();
		viewstarageLoc();
		viewstorageBin();
	}
	
	@Override
	public void setEnable(boolean state) {
		
		System.out.println("rohan state value="+state);
		int tablecolcount = this.table.getColumnCount();
		for (int i = tablecolcount - 1; i > -1; i--){
			table.removeColumn(i);
		}
	
		if (state == false){
			addViewColumn();
		}
		else{
			addeditColumn();
		}
	}
	
	private void addColumnViewReturnQuantity() {

		getColumnReturnQuantity = new TextColumn<ProductGroupList>() {
			
			@Override
			public String getValue(ProductGroupList object) {
				return object.getReturnQuantity()+"";
			}
		};
		table.addColumn(getColumnReturnQuantity, "Return Quantity");
		table.setColumnWidth(getColumnReturnQuantity, 100, Unit.PX);
	}
	
	
	public void viewWarehouse() {
		warehouseViewColumn = new TextColumn<ProductGroupList>() {
			@Override
			public String getValue(ProductGroupList object) {
				return object.getWarehouse();
			}
		};
		table.addColumn(warehouseViewColumn, "Warehouse");
		table.setColumnWidth(warehouseViewColumn,153,Unit.PX);
	}
	
	public void viewstarageLoc() {
		columnviewLoc = new TextColumn<ProductGroupList>() {
			@Override
			public String getValue(ProductGroupList object) {
				return object.getStorageLocation();
			}
		};
		table.addColumn(columnviewLoc, "Storage Location");
		table.setColumnWidth(columnviewLoc,153,Unit.PX);
	}
	public void viewstorageBin() {
		columnviewBin = new TextColumn<ProductGroupList>() {
			@Override
			public String getValue(ProductGroupList object) {
				return object.getStorageBin();
			}
		};
		table.addColumn(columnviewBin, "Storage Bin");
		table.setColumnWidth(columnviewBin,153,Unit.PX);
	}

	
//	protected void viewColumnProductQuantity()
//	{
//		System.out.println("inside view product qty");
//		viewcolumnProductQuantity=new TextColumn<ProductGroupList>() {
//			public String getValue(ProductGroupList object)
//			{
//				return object.getQuantity()+"";
//			}
//		};
//		table.addColumn(viewcolumnProductQuantity,"#Qty" );
//		table.setColumnWidth(columnProductQuantity,90,Unit.PX);
//	}
	
	public void addColumnProductQuantity() {
		
		EditTextCell editCell = new EditTextCell();
		columnProductQuantity = new Column<ProductGroupList, String>(editCell) {
			@Override
			public String getValue(ProductGroupList object) {
				return object.getQuantity() + "";
			}
		};
		table.addColumn(columnProductQuantity, "#Qty");
		table.setColumnWidth(columnProductQuantity,90,Unit.PX);
	}
	
	
	
	private void addColumnDelete() {
		ButtonCell btnCell= new ButtonCell();
		Delete = new Column<ProductGroupList, String>(btnCell) {

			@Override
			public String getValue(ProductGroupList object) {

				return "Delete";
			}
		};
		table.addColumn(Delete,"Delete");
	}

	
	private void setFieldUpdaterOnDelete() {
		Delete.setFieldUpdater(new FieldUpdater<ProductGroupList, String>() {
			@Override
			public void update(int index, ProductGroupList object, String value) {
				getDataprovider().getList().remove(index);
			}
		});
	}
	
	protected void addColumngetProductId()
	{
		getProductIdcolumn=new TextColumn<ProductGroupList>()
			{
			@Override
			public String getValue(ProductGroupList object)
			{
				return object.getProduct_id()+"";
			}
				};
				table.addColumn(getProductIdcolumn,"Product Id");
				table.setColumnWidth(getProductIdcolumn, 80,Unit.PX);
	}
	
	protected void addColumngetCode()
	{
		getCodecolumn=new TextColumn<ProductGroupList>() {
			public String getValue(ProductGroupList object)
			{
				return object.getCode()+"";
			}
		};
		table.addColumn(getCodecolumn,"Code" );
		table.setColumnWidth(getCodecolumn, 80,Unit.PX);
	}
	protected void addColumngetName()
	{
		getNamecolumn=new TextColumn<ProductGroupList>() {
			public String getValue(ProductGroupList object)
			{
				return object.getName()+"";
			}
		};
		table.addColumn(getNamecolumn,"Name" );
		table.setColumnWidth(getNamecolumn, 80,Unit.PX);
	}
	
	
	protected void addColumngetQuantity()
	{
		getQuantitycolumn=new TextColumn<ProductGroupList>() {
			public String getValue(ProductGroupList object)
			{
				return object.getQuantity()+"";
			}
		};
		table.addColumn(getQuantitycolumn,"#Qty" );
		table.setColumnWidth(getQuantitycolumn, 80,Unit.PX);
	}
	
	protected void addColumngetUnit()
	{
		getUnitcolumn=new TextColumn<ProductGroupList>() {
			public String getValue(ProductGroupList object)
			{
				return object.getUnit()+"";
			}
		};
		table.addColumn(getUnitcolumn,"UOM" );
		table.setColumnWidth(getUnitcolumn, 80,Unit.PX);
	}
	
	

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		createFieldUpdaterprodQtyColumn();
		setFieldUpdaterOnDelete();
	}

	
	// Product Quantity Update Method
	protected void createFieldUpdaterprodQtyColumn() {
		columnProductQuantity.setFieldUpdater(new FieldUpdater<ProductGroupList, String>() {
					@Override
					public void update(int index, ProductGroupList object,String value) {
						try {
							double val1;
								val1 = Double.parseDouble(value.trim());
								object.setQuantity(val1);
						} catch (NumberFormatException e) {
						}
						table.redrawRow(index);
					}
				});
	}
	

	@Override
	public void applyStyle() {
		
	}

	/**
	 * nidhi ||*||
	 */
	
	
	private void addColumnProductViewPlannedQty() {
		getViewColumnProductPlannedQty=new TextColumn<ProductGroupList>() {
			
			@Override
			public String getValue(ProductGroupList object) {
				return nf.format(object.getPlannedQty());
			}
		};
		table.addColumn(getViewColumnProductPlannedQty,"Planned Quantity");
		table.setColumnWidth(getViewColumnProductPlannedQty,81,Unit.PX);
	}
	
	private void addColumnViewConsumedQuantity() {

		getColumnReturnQuantity = new TextColumn<ProductGroupList>() {
			
			@Override
			public String getValue(ProductGroupList object) {
				return object.getReturnQuantity()+"";
			}
		};
		table.addColumn(getColumnReturnQuantity, "Consumed Quantity");
		table.setColumnWidth(getColumnReturnQuantity, 100, Unit.PX);
	}
}
