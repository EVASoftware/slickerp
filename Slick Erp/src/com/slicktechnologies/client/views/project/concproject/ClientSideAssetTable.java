package com.slicktechnologies.client.views.project.concproject;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.ProvidesKey;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.role.ScreenAuthorization;
import com.slicktechnologies.shared.common.servicerelated.ServicedClientSideAsset;
import com.slicktechnologies.shared.common.servicerelated.ServicedClientSideAsset;

public class ClientSideAssetTable extends SuperTable<ServicedClientSideAsset> 
{
	TextColumn<ServicedClientSideAsset> idColumn;
	TextColumn<ServicedClientSideAsset> nameColumn;
	TextColumn<ServicedClientSideAsset> brandColumn;
	TextColumn<ServicedClientSideAsset> modelNoColumn;
	TextColumn<ServicedClientSideAsset> srNoColumn;
	TextColumn<ServicedClientSideAsset> lattitudeColumn;
	TextColumn<ServicedClientSideAsset> longitudeColumn;
	TextColumn<ServicedClientSideAsset> viewServiceStatus;
	
	TextColumn<ServicedClientSideAsset> isMandatoryService;
	TextColumn<ServicedClientSideAsset> viewserviceStatus;
	TextColumn<ServicedClientSideAsset> statusColumn;
	Column<ServicedClientSideAsset,String> deleteColumn;
	TextColumn<ServicedClientSideAsset> viewserviceDateColumn;
	Column<ServicedClientSideAsset,Date> serviceDateColumn;
	
	Column<ServicedClientSideAsset,Boolean>serviceStatusColumn;
	ListHandler<ServicedClientSideAsset> columnSort;
	
	
	
	public ClientSideAssetTable()
	{
		super();
	}
	
	public ClientSideAssetTable(UiScreen<ServicedClientSideAsset> view)
	{
		
		super(view);
		
	}
	
	private void createColumnName()
	{
	  nameColumn =  new TextColumn<ServicedClientSideAsset>()
  		  {
       @Override
       public String getValue(ServicedClientSideAsset object) 
       {
          return object.getName();
       }
    };
    table.addColumn(nameColumn, "Name");
    nameColumn.setSortable(true);
   }


	private void createColumnLattitude()
	{
	  lattitudeColumn =  new TextColumn<ServicedClientSideAsset>()
  		  {
       @Override
       public String getValue(ServicedClientSideAsset object) 
       {
          return object.getLattitude();
       }
    };
    table.addColumn(lattitudeColumn, "Lattitude");
    lattitudeColumn.setSortable(true);
   }

	
	private void createColumnLongitude()
	{
	  longitudeColumn =  new TextColumn<ServicedClientSideAsset>()
  		  {
       @Override
       public String getValue(ServicedClientSideAsset object) 
       {
          return object.getLongitude();
       }
    };
    table.addColumn(longitudeColumn, "Longitude");
    longitudeColumn.setSortable(true);
   }

	private void createColumnBrand()
	{
	  brandColumn =  new TextColumn<ServicedClientSideAsset>()
  		  {
       @Override
       public String getValue(ServicedClientSideAsset object) 
       {
          return object.getBrand();
       }
    };
    table.addColumn(brandColumn, "Brand");
    brandColumn.setSortable(true);
   }
	
	private void createColumnModelNo()
	{
	  modelNoColumn =  new TextColumn<ServicedClientSideAsset>()
  		  {
       @Override
       public String getValue(ServicedClientSideAsset object) 
       {
          return object.getModelNo();
       }
    };
    table.addColumn(modelNoColumn, "Model Number");
    modelNoColumn.setSortable(true);
   }

	
	private void createColumnSrNo()
	{
	  srNoColumn =  new TextColumn<ServicedClientSideAsset>()
  		  {
       @Override
       public String getValue(ServicedClientSideAsset object) 
       {
          return object.getSrNo();
       }
    };
    table.addColumn(srNoColumn, "Sr Number");
    srNoColumn.setSortable(true);
   }
	
	
	private void createColumnServiceDate()
	{
		DateTimeFormat fmt = DateTimeFormat.getFormat(PredefinedFormat.DATE_SHORT);
		DatePickerCell date= new DatePickerCell(fmt);
		serviceDateColumn=new Column<ServicedClientSideAsset, Date>(date) {

			@Override
			public Date getValue(ServicedClientSideAsset object) {
				 if(object.getServiceDate()==null)
					 return new Date();
				 else
					 return object.getServiceDate();
			}
		};
		
    table.addColumn(serviceDateColumn, "Service Date");
    serviceDateColumn.setSortable(true);
   }
	
	private void createColumnViewServiceDate()
	{
		
		viewserviceDateColumn =  new TextColumn<ServicedClientSideAsset>()
  		  {
       @Override
       public String getValue(ServicedClientSideAsset object) 
       {
          return AppUtility.parseDate(object.getServiceDate());
       }
    };
    table.addColumn(viewserviceDateColumn, "Service Date");
    viewserviceDateColumn.setSortable(true);
   }

	
	protected void createColumndeleteColumn()
	{
		ButtonCell btnCell= new ButtonCell();
		deleteColumn=new Column<ServicedClientSideAsset,String>(btnCell)
				{
			@Override
			public String getValue(ServicedClientSideAsset object)
			{
				return  "Delete" ;
			}
				};
				table.addColumn(deleteColumn,"Delete");
	}
	
	protected void createColumnEditServiceStatus()
	{
		CheckboxCell cb=new CheckboxCell();
		serviceStatusColumn=new Column<ServicedClientSideAsset, Boolean>(cb) {

			@Override
			public Boolean getValue(ServicedClientSideAsset object) {
				return object.isServicedStatus();
			}
		};
		table.addColumn(serviceStatusColumn,"Service Status");		
	}
	
	
	
	protected void addColumnisMandatory()
	{
		//CheckboxCell cb=new CheckboxCell();
		isMandatoryService=new TextColumn<ServicedClientSideAsset>() {

			@Override
			public String getValue(ServicedClientSideAsset object) {
				if(object.getMandatoryService()==true)
				return "Yes";
				else
					return "No";
			}
		};
		table.addColumn(isMandatoryService,"Mandatory Service");		
	}
	
	protected void addColumnviewServiceStatus()
	{
		//CheckboxCell cb=new CheckboxCell();
		viewServiceStatus=new TextColumn<ServicedClientSideAsset>() {

			@Override
			public String getValue(ServicedClientSideAsset object) {
				if(object.isServicedStatus()==true)
				return "Completed";
				else
					return "Scheduled";
			}
		};
		table.addColumn(viewServiceStatus,"Service Status");		
	}
 public void createTable()
	{
		  
		   createColumnName();
		   createColumnBrand();
		   createColumnModelNo();
		   createColumnSrNo();
		   createColumnLattitude();
		   createColumnLongitude();
		   createColumnServiceDate();
		   addColumnisMandatory();
		   createColumnEditServiceStatus();
		   createColumndeleteColumn();
		   addFieldUpdater();
		   
		   table.setWidth("100%");
		  
		  
	}
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
	 
 
@Override
protected void initializekeyprovider() {
	keyProvider = new ProvidesKey<ServicedClientSideAsset>() {
		@Override
		public Object getKey(ServicedClientSideAsset item) {
			if(item==null)
				return null;
			else
				return item.getId();
		}
	};
	
 }





@Override
public void setEnable(boolean state)
{
    for(int i=table.getColumnCount()-1;i>-1;i--)
	  table.removeColumn(i); 
      if(state==true)
      {
    	  createColumnName();
    	  createColumnBrand();
 		  createColumnModelNo();
 		  createColumnSrNo();
 		  createColumnLattitude();
 		  createColumnLongitude();
 		  addColumnisMandatory();
 		  createColumnServiceDate();
 		  createColumnEditServiceStatus();
 		  addFieldUpdater();
 	 }
      
      else
      {
    	  createColumnName();
    	  createColumnBrand();
 		  createColumnModelNo();
 		  createColumnSrNo();
 		  createColumnLattitude();
 		  createColumnLongitude();
 		  addColumnisMandatory();
 		  createColumnViewServiceDate();
 		  addColumnviewServiceStatus();
 		  
  	
      }
}

public void applyStyle() {
}

@Override
public void addFieldUpdater() {
	setFieldUpdaterdeleteColumn();
	setFieldupdaterServicedStatus();
	setFieldupdaterServiceDate();
	
	
}
/*******************************************************************************************************/

private void setFieldupdaterServiceDate()
{
	 
	serviceDateColumn.setFieldUpdater(new FieldUpdater<ServicedClientSideAsset,Date>() {
		
		@Override
		public void update(int index, ServicedClientSideAsset object, Date value) {
			if(value!=null)
				object.setServiceDate(value);
			table.redrawRow(index);
			
		}
	});
	 
}



private void setFieldupdaterServicedStatus()
{

	
	serviceStatusColumn.setFieldUpdater(new FieldUpdater<ServicedClientSideAsset,Boolean>() {

		@Override
		public void update(int index, ServicedClientSideAsset object,
				Boolean value) 
		{
			object.setServicedStatus(value);
			table.redrawRow(index);
			
		}

		
		
	});
	 
}

protected void setFieldUpdaterdeleteColumn()
{
	deleteColumn.setFieldUpdater(new FieldUpdater<ServicedClientSideAsset,String>()
			{
		@Override
		public void update(int index,ServicedClientSideAsset object,String value)
		{
			getDataprovider().getList().remove(object);

			table.redrawRow(index);
		}
			});
}

/*******************************************************************************************************/


}
