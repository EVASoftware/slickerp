package com.slicktechnologies.client.views.project.concproject;

import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.simplesoftwares.client.library.composite.UploadComposite;

public class DocumentUploadPopUp 
{
	PopupPanel panel;
	
	protected UploadComposite testreport;
	protected InlineLabel label;
	
	public DocumentUploadPopUp()
	{
		panel=new PopupPanel();
		//label=new InlineLabel("Test Report Attach");
		//panel.add(label);
		testreport=new UploadComposite();
		panel.add(testreport);
	}

	public PopupPanel getPanel() {
		return panel;
	}

	public void setPanel(PopupPanel panel) {
		this.panel = panel;
	}

	public UploadComposite getTestreport() {
		return testreport;
	}

	public void setTestreport(UploadComposite testreport) {
		this.testreport = testreport;
	}

	public void show()
	{
		System.out.println("PPPPPPPPPPPPPPPPPPPPradnyaaaaaaaaaaaaaaaaaaa");
		panel.show();
		panel.setPixelSize(500, 500);
		panel.center();
	}
	
}
