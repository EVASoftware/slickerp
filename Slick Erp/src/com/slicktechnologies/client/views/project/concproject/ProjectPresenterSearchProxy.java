// Decompiled by DJ v3.12.12.98 Copyright 2014 Atanas Neshkov  Date: 16-Sep-14 11:40:58 AM
// Home Page:  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   ProjectPresenterSearchProxy.java

package com.slicktechnologies.client.views.project.concproject;

import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;

import java.util.Vector;

// Referenced classes of package com.slicktechnologies.client.views.project.concProject:
//            ProjectPresenter

public class ProjectPresenterSearchProxy extends ProjectPresenter.ProjectPresenterSearch
{public TextBox tbPointOfContactName;
public TextBox tbProjectName;
public TextBox olbClientSideAsset;
public IntegerBox ibContractId;
public PersonInfoComposite personInfo;
public IntegerBox serviceId;


public ListBox serviceStatus;
public ObjectListBox<Employee>olbEmployees;
public IntegerBox contractId;
public IntegerBox projectId;
public TextBox projectName;
public TextBox projectStatus;

//public DateBox serviceDate;

public DateComparator dateComparator;

public ProjectPresenterSearchProxy()
{
    createGui();
    this.dwnload.setVisible(false);
}

public void initWidget()
{
    MyQuerry querry = new MyQuerry();
    querry.setQuerryObject(new Customer());
    personInfo = new PersonInfoComposite(querry, false);
    tbPointOfContactName = new TextBox();
    tbProjectName = new TextBox();
    olbClientSideAsset = new TextBox();
    ibContractId = new IntegerBox();
    serviceId=new IntegerBox();
    serviceStatus=new ListBox();
    olbEmployees=new ObjectListBox<Employee>();
    projectId=new IntegerBox();
   
    /***
     * Here we commented the serviceDate and use datecomparator for date search
     * in these search we pass date as equlity filter but the date format issue for date storage in database
     * we used datecomparator 
     * by Anil 
     */
//    serviceDate=new DateBoxWithYearSelector();
    dateComparator= new DateComparator("serviceDate",new ServiceProject());
    
//    AppUtility.makeSalesPersonListBoxLive(olbEmployees);
    olbEmployees.makeEmployeeLive(AppConstants.SERVICEMODULE, AppConstants.CUSTOMERPROJECT, "Technician");
    AppUtility.setStatusListBox(serviceStatus,Service.getStatusList());
    
    
}

public void createScreen()
{
    initWidget();
    FormFieldBuilder builder = new FormFieldBuilder("", personInfo);
    FormField fpersonInfo = builder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
    builder = new FormFieldBuilder("POC Name", tbPointOfContactName);
    FormField ftbPointOfContactName = builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
    builder = new FormFieldBuilder("Project Name", tbProjectName);
    FormField ftbProjectName = builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
    builder = new FormFieldBuilder("Client Side Asset", olbClientSideAsset);
    FormField folbClientSideAsset = builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
    builder = new FormFieldBuilder("Contract Id", ibContractId);
    FormField fibContractId = builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
    
    builder = new FormFieldBuilder("Service Id",serviceId);
    FormField fibServiceId = builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
    
//    builder = new FormFieldBuilder("Service Date",serviceDate);
//    FormField fdbServiceDate = builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
    
    builder = new FormFieldBuilder("Service Date",dateComparator.getFromDate());
	FormField fdateComparator= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
    
    builder = new FormFieldBuilder("Service Status",serviceStatus);
    FormField fdbServiceStatus = builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
    
    builder = new FormFieldBuilder("Technician",olbEmployees);
    FormField folbTechnician = builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
    
    builder = new FormFieldBuilder("Project ID",projectId);
    FormField folbProjectId = builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
    
    builder = new FormFieldBuilder("Project Status",projectId);
    FormField folbProjectStatus = builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
    
    
    
    
    fields = new FormField[][]{{fibContractId,fibServiceId,/*fdbServiceDate*/ fdateComparator,fdbServiceStatus},
    		                   {folbTechnician,folbProjectId,ftbProjectName,ftbPointOfContactName},
    		                   {fpersonInfo}};
}

public MyQuerry getQuerry()
{
    Vector<Filter> filtervec = new Vector<Filter>();
    Filter temp = null;
    if(dateComparator.getValue()!=null)
	{
		filtervec.addAll(dateComparator.getValue());
	}
    if(!tbPointOfContactName.getValue().trim().equals(""))
    {
        temp = new Filter();
        temp.setStringValue(tbPointOfContactName.getValue().trim());
        filtervec.add(temp);
        temp.setQuerryString("pointOfContact.fullname");
        filtervec.add(temp);
    }
    if(!tbProjectName.getValue().trim().equals(""))
    {
        temp = new Filter();
        temp.setStringValue(tbProjectName.getValue().trim());
        filtervec.add(temp);
        temp.setQuerryString("projectName");
        filtervec.add(temp);
    }
    
    if(personInfo.getIdValue()!=-1)
    {
    temp=new Filter();
    temp.setIntValue(personInfo.getIdValue());
    temp.setQuerryString("personInfo.count");
    filtervec.add(temp);
    }
    
    if(!(personInfo.getFullNameValue().equals("")))
    {
    temp=new Filter();
    temp.setStringValue(personInfo.getFullNameValue());
    temp.setQuerryString("personInfo.fullName");
    filtervec.add(temp);
    }
    
    /**
	   * Commented By Priyanka
	   * Des : Search data using only Id and name as per requiremnet of Rahul Tiwari
	   */
//    if(personInfo.getCellValue()!=-1l)
//    {
//    temp=new Filter();
//    temp.setLongValue(personInfo.getCellValue());
//    temp.setQuerryString("personInfo.cellNumber");
//    filtervec.add(temp);
//    }
    
   /* if(!olbClientSideAsset.getValue().trim().equals(""))
    {
        temp = new Filter();
        temp.setStringValue(olbClientSideAsset.getValue().trim());
        filtervec.add(temp);
        temp.setQuerryString("clientSideAsset");
        filtervec.add(temp);
    }*/
    if(ibContractId.getValue() != null)
    {
        temp = new Filter();
        temp.setIntValue((Integer)ibContractId.getValue());
        filtervec.add(temp);
        temp.setQuerryString("contractId");
        filtervec.add(temp);
    }
    
    if(serviceId.getValue() != null)
    {
        temp = new Filter();
        temp.setIntValue(serviceId.getValue());
        filtervec.add(temp);
        temp.setQuerryString("serviceId");
        filtervec.add(temp);
    }
    
//    if(serviceDate.getValue() != null)
//    {
//        temp = new Filter();
//        temp.setDateValue(serviceDate.getValue());
//        filtervec.add(temp);
//        temp.setQuerryString("serviceDate");
//        filtervec.add(temp);
//    }
    
    if(serviceStatus.getSelectedIndex()!=0)
    {
        temp = new Filter();
        int selIndex=serviceStatus.getSelectedIndex();
        String selectedItem=serviceStatus.getItemText(selIndex);
        temp.setStringValue(selectedItem);
        filtervec.add(temp);
        temp.setQuerryString("serviceStatus");
        filtervec.add(temp);
    }
    
    
    if(olbEmployees.getValue()!=null)
    {
        temp = new Filter();
       
        temp.setStringValue(olbEmployees.getValue());
        filtervec.add(temp);
        temp.setQuerryString("serviceEngineer");
        filtervec.add(temp);
    }
    
    if(projectId.getValue()!=null)
    {
        temp = new Filter();
       
        temp.setIntValue(projectId.getValue());
        filtervec.add(temp);
        temp.setQuerryString("count");
        filtervec.add(temp);
    }
    
    
    
    
    
    MyQuerry querry = new MyQuerry();
    querry.setFilters(filtervec);
    querry.setQuerryObject(new ServiceProject());
    return querry;
}

public void clear()
{
    super.clear();
}

@Override
public boolean validate() {
	// TODO Auto-generated method stub
	return super.validate();
}

}
