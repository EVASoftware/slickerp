// Decompiled by DJ v3.12.12.98 Copyright 2014 Atanas Neshkov  Date: 16-Sep-14 11:05:16 AM
// Home Page:  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   ProjectInfoTable.java

package com.slicktechnologies.client.views.project.concproject;

import com.google.gwt.user.cellview.client.*;
import com.google.gwt.view.client.ProvidesKey;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;

public class ProjectInfoTable extends SuperTable<ServiceProject>
{

    public ProjectInfoTable()
    {
    }

    public ProjectInfoTable(UiScreen<ServiceProject> view)
    {
        super(view);
    }

    public void createTable()
    {
        createColumnProjectName();
        createColumnPocName();
        createColumnPocCell();
        createColumnAddree1();
        createColumnAddree2();
        table.setKeyboardSelectionPolicy(com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy.ENABLED);
    }

    protected void createColumnProjectName()
    {
        Name = new TextColumn<ServiceProject>() {

            public String getValue(ServiceProject object)
            {
                return object.getProjectName();
            }

           
        }
;
        table.addColumn(Name, "Project Name");
    }

    protected void createColumnAddree1()
    {
        AddressLine1 = new TextColumn<ServiceProject>() {

            public String getValue(ServiceProject object)
            {
                return object.getAddr().getAddrLine1();
            }

        }
;
        table.addColumn(AddressLine1, "Address1");
    }

    protected void createColumnAddree2()
    {
        AddressLine2 = new TextColumn<ServiceProject>() {

            public String getValue(ServiceProject object)
            {
                return object.getAddr().getAddrLine2();
            }

        }
;
        table.addColumn(AddressLine2, "Address2");
    }

    protected void createColumnPocName()
    {
        PocName = new TextColumn<ServiceProject>() {

            public String getValue(ServiceProject object)
            {
                return object.getPocName();
            }

        }
;
        table.addColumn(PocName, "POC Name");
    }

    protected void createColumnPocCell()
    {
        PocCell = new TextColumn<ServiceProject>() {

            public String getValue(ServiceProject object)
            {
                return (new StringBuilder()).append(object.getPocCell()).toString();
            }

          
        }
;
        table.addColumn(PocCell, "POC Cell");
    }

    public void addFieldUpdater()
    {
    }

    public void addColumnSorting()
    {
    }

    public void setEnable(boolean flag)
    {
    }

    public void applyStyle()
    {
    }

    protected void initializekeyprovider()
    {
        keyProvider = new ProvidesKey<ServiceProject>() {

            public Object getKey(ServiceProject item)
            {
                if(item == null)
                    return null;
                else
                    return item.getId();
            }

           
        }
;
    }

    TextColumn<ServiceProject> projectId;
    TextColumn<ServiceProject> Name;
    TextColumn<ServiceProject> AddressLine1;
    TextColumn<ServiceProject> AddressLine2;
    TextColumn<ServiceProject> PocName;
    TextColumn<ServiceProject> PocCell;
}
