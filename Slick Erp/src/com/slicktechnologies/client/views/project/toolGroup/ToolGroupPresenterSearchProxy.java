// Decompiled by DJ v3.12.12.98 Copyright 2014 Atanas Neshkov  Date: 16-Sep-14 11:43:55 AM
// Home Page:  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   ToolGroupPresenterSearchProxy.java

package com.slicktechnologies.client.views.project.toolGroup;

import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.servicerelated.CompanyAsset;
import com.slicktechnologies.shared.common.servicerelated.ToolGroup;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

// Referenced classes of package com.slicktechnologies.client.views.project.toolGroup:
//            ToolGroupPresenter

public class ToolGroupPresenterSearchProxy extends ToolGroupPresenter.ToolGroupPresenterSearch
{

    public Object getVarRef(String varName)
    {
        if(varName.equals("tbcode"))
            return tbcode;
        if(varName.equals("groupName"))
            return groupName;
        if(varName.equals("groupId"))
            return groupId;
        if(varName.equals("status"))
            return status;
        else
            return null;
    }

  
	public ToolGroupPresenterSearchProxy()
    {
        createGui();
    }

    public void initWidget()
    {
        tbcode = new TextBox();
        groupName = new TextBox();
        groupId = new IntegerBox();
       // olbTool = new ObjectListBox<CompanyAsst>();
        //AppUtility.makeCompanyAssetListBoxLive(olbTool);
        status=new ListBox();
        status.addItem("Select");
        status.addItem("Active");
        status.addItem("InActive");
        
        
    }

    public void createScreen()
    {
        initWidget();
        FormFieldBuilder builder = new FormFieldBuilder("Code", tbcode);
        FormField ftbcode = builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
        builder = new FormFieldBuilder("ToolSet Name", groupName);
        FormField fgroupName = builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
        builder = new FormFieldBuilder("ToolSet Id", groupId);
        FormField fgroupId = builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
        builder = new FormFieldBuilder("Status", status);
        FormField folbTool = builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
        fields = (new FormField[][] {
            new FormField[] {
                fgroupId, fgroupName
            }, new FormField[] {
                folbTool, ftbcode
            }
        });
    }

    public MyQuerry getQuerry()
    {
        Vector<Filter> filtervec = new Vector<Filter>();
        Filter temp = null;
        if(groupId.getValue() != null)
        {
            temp = new Filter();
            temp.setIntValue((Integer)groupId.getValue());
            filtervec.add(temp);
            temp.setQuerryString("count");
            filtervec.add(temp);
        }
        if(!groupName.getValue().trim().equals(""))
        {
            temp = new Filter();
            temp.setStringValue(groupName.getValue().trim());
            filtervec.add(temp);
            temp.setQuerryString("name");
            filtervec.add(temp);
        }
       
        if(status.getSelectedIndex()!=0)
        {
        	temp=new Filter();
        	int index=status.getSelectedIndex();
        	String item=status.getItemText(index);
        	
        	
        	if(status.getItemText(index).contains("Active"))
        	{
        		temp.setQuerryString("status");
        		temp.setBooleanvalue(true);
        	}
        	if(status.getItemText(index).contains("InActive"))
        	{
        		temp.setQuerryString("status");
        		temp.setBooleanvalue(false);
        		
        	}
        	filtervec.add(temp);
        }
        if(!tbcode.getValue().trim().equals(""))
        {
            temp = new Filter();
            temp.setStringValue(tbcode.getValue().trim());
            filtervec.add(temp);
            temp.setQuerryString("code");
            filtervec.add(temp);
        }
        MyQuerry querry = new MyQuerry();
        querry.setFilters(filtervec);
        querry.setQuerryObject(new ToolGroup());
        return querry;
    }

    public TextBox tbcode;
    public TextBox groupName;
    public IntegerBox groupId;
    //public ObjectListBox<CompanyAsst> olbTool;
    public ListBox status;
    
    @Override
  	public boolean validate() {
  		// TODO Auto-generated method stub
  		return super.validate();
  	}

}
