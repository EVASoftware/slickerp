// Decompiled by DJ v3.12.12.98 Copyright 2014 Atanas Neshkov  Date: 16-Sep-14 11:43:40 AM
// Home Page:  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   ToolGroupPresenter.java

package com.slicktechnologies.client.views.project.toolGroup;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.view.client.ListDataProvider;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.*;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.servicerelated.ToolGroup;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

// Referenced classes of package com.slicktechnologies.client.views.project.toolGroup:
//            ToolGroupForm, ToolGroupPresenterTableProxy, ToolGroupPresenterSearchProxy

public class ToolGroupPresenter extends FormTableScreenPresenter<ToolGroup>
{
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
    public static class ToolGroupPresenterSearch extends SearchPopUpScreen<ToolGroup>
    {

        public MyQuerry getQuerry()
        {
            return null;
        }

        public ToolGroupPresenterSearch()
        {
        }

		@Override
		public boolean validate() {
			// TODO Auto-generated method stub
			return true;
		}
    }

    public static class ToolGroupPresenterTable extends SuperTable<ToolGroup>
        implements GeneratedVariableRefrence
    {

        public Object getVarRef(String varName)
        {
            return null;
        }

        public void createTable()
        {
        }

        protected void initializekeyprovider()
        {
        }

        public void addFieldUpdater()
        {
        }

        public void setEnable(boolean flag)
        {
        }

        public void applyStyle()
        {
        }

        public ToolGroupPresenterTable()
        {
        }
        
       
    }


    public ToolGroupPresenter(FormTableScreen<ToolGroup> view, ToolGroup model)
    {
        super(view, model);
        csvservice = (CsvServiceAsync)GWT.create(CsvService.class);
        form = (ToolGroupForm)view;
        form.getSupertable().connectToLocal();
        form.retriveTable(getConfigQuery());
        form.setPresenter(this);
        
        boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.TOOLGROUP,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
    }

    public void reactToProcessBarEvents(ClickEvent e)
    {
        InlineLabel lbl = (InlineLabel)e.getSource();
        if(lbl.getText().contains("Delete") && form.validate())
        {
            List<ToolGroup> data = form.getSupertable().getDataprovider().getList();
            for(int i = 0; i < data.size(); i++)
            {
                if(((ToolGroup)data.get(i)).getId().equals(((ToolGroup)model).getId()))
                    data.remove(i);
                form.getSupertable().getDataprovider().refresh();
            }

            reactOnDelete();
        }
        if(lbl.getText().contains("New"))
        {
            form.setToNewState();
            initalize();
        }
    }

    public static ToolGroupForm initalize()
    {
        ToolGroupPresenterTable gentableScreen = new ToolGroupPresenterTableProxy();
        ToolGroupForm form = new ToolGroupForm(gentableScreen, 1, true);
        gentableScreen.setView(form);
        gentableScreen.applySelectionModle();
        ToolGroupPresenterTable gentableSearch = new ToolGroupPresenterTableProxy();
        gentableSearch.setView(form);
        gentableSearch.applySelectionModle();
        ToolGroupPresenterSearch.staticSuperTable = gentableSearch;
        ToolGroupPresenterSearch searchpopup = new ToolGroupPresenterSearchProxy();
        form.setSearchpopupscreen(searchpopup);
        ToolGroupPresenter presenter = new ToolGroupPresenter(form, new ToolGroup());
        AppMemory.getAppMemory().stickPnel(form);
		return form;
    }

    public void reactOnPrint()
    {
    }

    public void reactOnEmail()
    {
    }

    protected void makeNewModel()
    {
        model = new ToolGroup();
    }

    public MyQuerry getConfigQuery()
    {
        MyQuerry quer = new MyQuerry(new Vector<Filter>(), new ToolGroup());
        return quer;
    }

    
    @Override
	public void reactOnDownload() 
	{
    	System.out.println("iiiiiiiiiiiiiiiiiiiii");
		ArrayList<ToolGroup> custarray=new ArrayList<ToolGroup>();
		List<ToolGroup> list=(List<ToolGroup>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
		
		custarray.addAll(list);
		
		csvservice.setToolSetlist(custarray, new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);
				
			}

			@Override
			public void onSuccess(Void result) {
				

				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "csvservlet"+"?type="+22;
				Window.open(url, "test", "enabled");
				
			}
		});

	}
    public void setModel(ToolGroup entity)
    {
        model = entity;
    }

    
    ToolGroupForm form;
   
}
