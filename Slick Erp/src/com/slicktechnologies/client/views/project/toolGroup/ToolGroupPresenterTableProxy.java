// Decompiled by DJ v3.12.12.98 Copyright 2014 Atanas Neshkov  Date: 16-Sep-14 11:45:29 AM
// Home Page:  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   ToolGroupPresenterTableProxy.java

package com.slicktechnologies.client.views.project.toolGroup;

import com.google.gwt.user.cellview.client.*;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.ProvidesKey;
import com.slicktechnologies.shared.common.servicerelated.CompanyAsset;
import com.slicktechnologies.shared.common.servicerelated.ToolGroup;

import java.util.Comparator;
import java.util.List;

// Referenced classes of package com.slicktechnologies.client.views.project.toolGroup:
//            ToolGroupPresenter

public class ToolGroupPresenterTableProxy extends ToolGroupPresenter.ToolGroupPresenterTable
{

    public Object getVarRef(String varName)
    {
        if(varName.equals("getCodeColumn"))
            return getCodeColumn;
        if(varName.equals("getNameColumn"))
            return getNameColumn;
        if(varName.equals("getGroupidColumn"))
            return getGroupidColumn;
        if(varName.equals("getStatusColumn"))
            return getStatusColumn;
        else
            return null;
    }

    public ToolGroupPresenterTableProxy()
    {
    }

    public void createTable()
    {
        addColumngetGroupid();
        addColumngetName();
        addColumngetCode();
        addColumngetStatus();
    }

    protected void initializekeyprovider()
    {
        keyProvider = new ProvidesKey<ToolGroup>() {

            public Object getKey(ToolGroup item)
            {
                if(item == null)
                    return null;
                else
                    return item.getId();
            }

           
        }
;
    }

    public void setEnable(boolean flag)
    {
    }

    public void applyStyle()
    {
    }

    public void addColumnSorting()
    {
        addSortinggetGroupid();
        addSortinggetName();
        addSortinggetCode();
        addSortinggetStatus();
    }

    public void addFieldUpdater()
    {
    }

    protected void addSortinggetGroupid()
    {
        List<ToolGroup> list = getDataprovider().getList();
        columnSort = new ListHandler<ToolGroup>(list);
        columnSort.setComparator(getGroupidColumn, new Comparator<ToolGroup>() {

            public int compare(ToolGroup e1, ToolGroup e2)
            {
                if(e1 != null && e2 != null)
                {
                    if(e1.getCount() == e2.getCount())
                        return 0;
                    return e1.getCount() <= e2.getCount() ? -1 : 1;
                } else
                {
                    return 0;
                }
            }

           
        }
);
        table.addColumnSortHandler(columnSort);
    }

    protected void addColumngetGroupid()
    {
        getGroupidColumn = new TextColumn<ToolGroup>() {

            public String getValue(ToolGroup object)
            {
                return (new StringBuilder(String.valueOf(object.getCount()))).toString();
            }

            
        }
;
        table.addColumn(getGroupidColumn, "ToolSet Id");
        getGroupidColumn.setSortable(true);
    }

    protected void addSortinggetName()
    {
        List<ToolGroup> list = getDataprovider().getList();
        columnSort = new ListHandler<ToolGroup>(list);
        columnSort.setComparator(getNameColumn, new Comparator<ToolGroup>() {

            public int compare(ToolGroup e1, ToolGroup e2)
            {
                if(e1 != null && e2 != null)
                {
                    if(e1.getName() != null && e2.getName() != null)
                        return e1.getName().compareTo(e2.getName());
                    else
                        return 0;
                } else
                {
                    return 0;
                }
            }

            
        }
);
        table.addColumnSortHandler(columnSort);
    }

    protected void addColumngetName()
    {
        getNameColumn = new TextColumn<ToolGroup>() {

            public String getValue(ToolGroup object)
            {
                return (new StringBuilder(String.valueOf(object.getName()))).toString();
            }

        }
;
        table.addColumn(getNameColumn, "ToolSet Name");
        getNameColumn.setSortable(true);
    }

    protected void addColumngetStatus()
    {
        getStatusColumn = new TextColumn<ToolGroup>() {

            public String getValue(ToolGroup object)
            {
                if(object.isStatus()==true)
                	return "Active";
                else
                	return "In Active";
            }
        }
;
        table.addColumn(getStatusColumn, "Status");
        getStatusColumn.setSortable(true);
    }

    protected void addSortinggetCode()
    {
        List<ToolGroup> list = getDataprovider().getList();
        columnSort = new ListHandler<ToolGroup>(list);
        columnSort.setComparator(getCodeColumn, new Comparator<ToolGroup>() {

            public int compare(ToolGroup e1, ToolGroup e2)
            {
                if(e1 != null && e2 != null)
                {
                    if(e1.getCode() != null && e2.getCode() != null)
                        return e1.getCode().compareTo(e2.getCode());
                    else
                        return 0;
                } else
                {
                    return 0;
                }
            }

            

           
        }
);
        table.addColumnSortHandler(columnSort);
    }
    
    
    
    protected void addSortinggetStatus()
    {
        List<ToolGroup> list = getDataprovider().getList();
        columnSort = new ListHandler<ToolGroup>(list);
        columnSort.setComparator(getStatusColumn, new Comparator<ToolGroup>() {

            public int compare(ToolGroup e1, ToolGroup e2)
            {
                if(e1 != null && e2 != null)
                {
                   if(e1.isStatus()==e2.isStatus())
                	   return 0;
                   else
                	   return -1;
                } else
                {
                    return 0;
                }
            }

            

           
        }
);
        table.addColumnSortHandler(columnSort);
    }

    protected void addColumngetCode()
    {
        getCodeColumn = new TextColumn<ToolGroup>() {

            public String getValue(ToolGroup object)
            {
                return (new StringBuilder(String.valueOf(object.getCode()))).toString();
            }

            
        }
;
        table.addColumn(getCodeColumn, "Code");
        getCodeColumn.setSortable(true);
    }

    TextColumn<ToolGroup> getCodeColumn;
    TextColumn<ToolGroup> getNameColumn;
    TextColumn<ToolGroup> getGroupidColumn;
    TextColumn<ToolGroup> getStatusColumn;
}
