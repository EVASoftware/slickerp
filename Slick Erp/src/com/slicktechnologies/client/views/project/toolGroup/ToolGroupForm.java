// Decompiled by DJ v3.12.12.98 Copyright 2014 Atanas Neshkov  Date: 16-Sep-14 11:43:27 AM
// Home Page:  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   ToolGroupForm.java

package com.slicktechnologies.client.views.project.toolGroup;

import java.util.List;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.project.toolGroup.ToolTable;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.servicerelated.CompanyAsset;
import com.slicktechnologies.shared.common.servicerelated.ToolGroup;

public class ToolGroupForm extends FormTableScreen<ToolGroup>
    implements ClickHandler
{

	 public TextBox groupName;
	    public CheckBox status;
	    TextBox tbcode;
	    public ObjectListBox<CompanyAsset> olbTool;
	    protected IntegerBox groupId;
	    Button btnAdd;
	    ToolTable tooltable;
	    private FormField fieldname;
	    private FormField fieldstatus;
	    private FormField fieldolbTool;
	    private FormField fieldCode;
	    private FormField fieldid;
	    private FormField fieldbtnAdd;
	    private FormField fieldTable;
	    private FormFieldBuilder buildname;
	    private FormFieldBuilder buildstatus;
	    private FormFieldBuilder buildolbTool;
	    private FormFieldBuilder buildCode;
	    private FormFieldBuilder buildid;
	    private FormFieldBuilder buildbtnAdd;
	    private FormFieldBuilder buildTable;
	    private FormFieldBuilder fbuilder;
	 
	    ToolGroup toolGrpObj;

    public ToolGroupForm(SuperTable<ToolGroup> table, int mode, boolean captionmode)
    {
        super(table, mode, captionmode);
        createGui();
        groupId.setEnabled(false);
    }

    public void toggleAppHeaderBarMenu()
    {
        if(AppMemory.getAppMemory().currentState == ScreeenState.NEW)
        {
            InlineLabel menus[] = AppMemory.getAppMemory().skeleton.getMenuLabels();
            for(int k = 0; k < menus.length; k++)
            {
                String text = menus[k].getText();
                if(text.contains("Save") || text.equals("Search") || text.contains("Discard")|| text.contains(AppConstants.NAVIGATION))
                    menus[k].setVisible(true);
                else
                    menus[k].setVisible(false);
            }

        } else
        if(AppMemory.getAppMemory().currentState == ScreeenState.EDIT)
        {
            InlineLabel menus[] = AppMemory.getAppMemory().skeleton.getMenuLabels();
            for(int k = 0; k < menus.length; k++)
            {
                String text = menus[k].getText();
                if(text.contains("Save") || text.contains("Discard")|| text.contains(AppConstants.NAVIGATION))
                    menus[k].setVisible(true);
                else
                    menus[k].setVisible(false);
            }

        } else
        if(AppMemory.getAppMemory().currentState == ScreeenState.VIEW)
        {
            InlineLabel menus[] = AppMemory.getAppMemory().skeleton.getMenuLabels();
            for(int k = 0; k < menus.length; k++)
            {
                String text = menus[k].getText();
                if(text.contains("Edit") || text.contains("Discard")|| text.contains(AppConstants.NAVIGATION))
                    menus[k].setVisible(true);
                else
                    menus[k].setVisible(false);
            }

        }
        AuthorizationHelper.setAsPerAuthorization(Screen.TOOLGROUP,LoginPresenter.currentModule.trim());
    }

    private void initializeWidgets()
    {
        groupName = new TextBox();
        status = new CheckBox();
        olbTool = new ObjectListBox<CompanyAsset>();
        AppUtility.makeCompanyAssetListBoxLive(olbTool);
        tbcode = new TextBox();
        groupId = new IntegerBox();
        btnAdd = new Button("Add Tool");
        btnAdd.addClickHandler(this);
        tooltable = new ToolTable();
        tooltable.connectToLocal();
    }

    public void createScreen()
    {
        initializeWidgets();
        processlevelBarNames = (new String[] {"Delete", "New"});
        fbuilder = new FormFieldBuilder();
        FormField fgroupingGroupDetails = fbuilder.setlabel("Tool Set Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
        FormField fgroupingToolDetails = fbuilder.setlabel("Tool Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
        buildname = new FormFieldBuilder("* Tool Set Name", groupName);
        fieldname = buildname.setMandatory(true).setMandatoryMsg("Tool Set Name is Mandatory").build();
        buildstatus = new FormFieldBuilder("Status", status);
        fieldstatus = buildstatus.build();
        buildolbTool = new FormFieldBuilder("Tool", olbTool);
        fieldolbTool = buildolbTool.setMandatory(false).build();
        buildCode = new FormFieldBuilder("* ToolSet Code", tbcode);
        fieldCode = buildCode.setMandatory(true).setMandatoryMsg("Tool Set code is Mandatory").build();
        buildid = new FormFieldBuilder("* Id", groupId);
        fieldid = buildid.setMandatory(false).setMandatoryMsg("Tool Set Id is Mandatory").build();
        buildbtnAdd = new FormFieldBuilder("", btnAdd);
        fieldbtnAdd = buildbtnAdd.build();
        buildTable = new FormFieldBuilder("", tooltable.getTable());
        fieldTable = buildTable.setColSpan(5).build();
        fields = (new FormField[][] {
            new FormField[] {
                fgroupingGroupDetails
            }, new FormField[] {
                fieldid, fieldname, fieldCode, fieldstatus
            }, new FormField[] {
                fgroupingToolDetails
            }, new FormField[] {
                fieldolbTool, fieldbtnAdd
            }, new FormField[] {
                fieldTable
            }
        });
        status.setValue(Boolean.valueOf(true));
        groupName.getElement().setId("configname");
    }

    public void updateModel(ToolGroup model)
    {
        if(groupName.getValue() != null)
            model.setName(groupName.getValue());
        if(status.getValue() != null)
            model.setStatus(status.getValue().booleanValue());
        if(olbTool.getValue() != null)
            model.setCategoryName(olbTool.getValue());
        if(tbcode.getValue() != null)
            model.setCode(tbcode.getValue());
        if(tooltable.getValue() != null)
            model.setArraylistTool(tooltable.getValue());
        
        
        toolGrpObj=model;
        presenter.setModel(model);
    }

    public void updateView(ToolGroup view)
    {
    	toolGrpObj=view;
    	if(view.getCount()!=-1)
    		groupId.setValue(Integer.valueOf(view.getCount()));
        if(view.getName() != null)
            groupName.setValue(view.getName());
        if(view.getCode() != null)
            tbcode.setValue(view.getCode());
        if(view.getCategoryName() != null)
            olbTool.setValue(view.getCategoryName());
        if(view.getArraylistTool() != null)
            tooltable.setValue(view.getArraylistTool());
        status.setValue(Boolean.valueOf(view.isStatus()));
        presenter.setModel(view);
    }

    
    @Override
 	public void setToViewState() {
 		super.setToViewState();
 		
 		SuperModel model=new ToolGroup();
 		model=toolGrpObj;
 		AppUtility.addDocumentToHistoryTable(AppConstants.ASSETMANEGEMENTMODULE,AppConstants.TOOLSET, toolGrpObj.getCount(), null,null,null, false, model, null);
 	
 	}
     
     @Override
 	public void setToNewState() {
 		super.setToNewState();
 		
 		System.out.println("Inside NEW method");
 		if(toolGrpObj!=null){
 			SuperModel model=new ToolGroup();
 			model=toolGrpObj;
 			AppUtility.addDocumentToHistoryTable(AppConstants.ASSETMANEGEMENTMODULE,AppConstants.TOOLSET, toolGrpObj.getCount(), null,null,null, false, model, null);
 		
 		}
 	}
    
    public void setCount(int count)
    {
        groupId.setValue(Integer.valueOf(count));
    }
    public void clear()
    {
        super.clear();
        tooltable.clear();
        status.setValue(Boolean.valueOf(true));
    }

    public void onClick(ClickEvent event)
    {
        if(event.getSource().equals(btnAdd))
            reactOnAddInToolTable();
    }

    private void reactOnAddInToolTable()
    {
        if(olbTool.getValue()!=null)
        {
    	CompanyAsset asset = (CompanyAsset)olbTool.getSelectedItem();
        List<CompanyAsset> list = tooltable.getDataprovider().getList();
        if(list != null)
        {
            if(list.contains(asset)==false)
        	   list.add(asset);
            AppUtility.makeCompanyAssetListBoxLive(olbTool);
            
        }
        
        }
    }

    public boolean validate()
    {
        boolean superValidate = super.validate();
        
        if(superValidate==false)
        	return false;
        if(tooltable.getDataprovider().getList().size() == 0)
        {
            showDialogMessage("Please Select at least one Tool !");
            return false;
            
            
        }
        return true;

    }

    @Override
	public void setEnable(boolean state) {
    	super.setEnable(state);
		tooltable.setEnable(state);
		this.groupId.setEnabled(false);
	}

      
}
