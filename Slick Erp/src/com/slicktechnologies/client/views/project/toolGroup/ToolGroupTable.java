// Decompiled by DJ v3.12.12.98 Copyright 2014 Atanas Neshkov  Date: 16-Sep-14 11:45:47 AM
// Home Page:  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   ToolGroupTable.java

package com.slicktechnologies.client.views.project.toolGroup;

import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.user.cellview.client.*;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.ProvidesKey;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.servicerelated.ToolGroup;

import java.io.PrintStream;
import java.util.Comparator;

public class ToolGroupTable extends SuperTable<ToolGroup>
{

    public ToolGroupTable()
    {
    }

    public ToolGroupTable(UiScreen<ToolGroup> view)
    {
        super(view);
    }

    private void createColumnName()
    {
        nameColumn = new TextColumn<ToolGroup>() {

            public String getValue(ToolGroup object)
            {
                return object.getName();
            }

            
        }
;
        table.addColumn(nameColumn, "Name");
        nameColumn.setSortable(true);
    }

    private void createColumnCategory()
    {
        categoryColumn = new TextColumn<ToolGroup>() {

            public String getValue(ToolGroup object)
            {
                return object.getCategoryName();
            }

           

            
           
        }
;
        table.addColumn(categoryColumn, "Tool Name");
        categoryColumn.setSortable(true);
    }

    private void createColumnCode()
    {
        codeColumn = new TextColumn<ToolGroup>() {

            public String getValue(ToolGroup object)
            {
                return object.getCode();
            }

           
        }
;
        table.addColumn(codeColumn, "Code");
        codeColumn.setSortable(true);
    }

    private void createColumnId()
    {
        idColumn = new TextColumn<ToolGroup>() {

            public String getValue(ToolGroup object)
            {
                return (new StringBuilder()).append(object.getId()).toString();
            }

            
        }
;
        table.addColumn(idColumn, "Id");
        idColumn.setSortable(true);
    }

    private void createColumnStatus()
    {
        statusColumn = new TextColumn<ToolGroup>() {

            public String getValue(ToolGroup object)
            {
                if(object.isStatus())
                    return AppUtility.ACTIVE;
                else
                    return AppUtility.INACTIVE;
            }

            

            
            
        }
;
        table.addColumn(statusColumn, "Status");
        statusColumn.setSortable(true);
    }

    public void createTable()
    {
        createColumnId();
        createColumnName();
        createColumnCategory();
        createColumnCode();
        createColumnStatus();
        table.setWidth("100%");
    }

    private void setFieldupdaterName()
    {
        nameColumn.setFieldUpdater(new FieldUpdater<ToolGroup,String>() {

            public void update(int index, ToolGroup object, String value)
            {
                object.setName(value);
                table.redrawRow(index);
            }

           
        }
);
    }

    
    

    protected void initializekeyprovider()
    {
        keyProvider = new ProvidesKey<ToolGroup>() {

            public Object getKey(ToolGroup item)
            {
                if(item == null)
                    return null;
                else
                    return item.getId();
            }

            
        }
;
    }

    public void addColumnSorting()
    {
        addNameSorting();
        addStatusSorting();
    }

    public void addNameSorting()
    {
        java.util.List<ToolGroup> list = getDataprovider().getList();
        columnSort = new ListHandler<ToolGroup>(list);
        columnSort.setComparator(nameColumn, new Comparator<ToolGroup>() {

            public int compare(ToolGroup e1, ToolGroup e2)
            {
                System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>.");
                if(e1 != null && e2 != null)
                {
                    if(e1.getName() != null && e2.getName() != null)
                        return e1.getName().compareTo(e2.getName());
                    else
                        return 0;
                } else
                {
                    return 0;
                }
            }

          

            
           
        }
);
        table.addColumnSortHandler(columnSort);
    }

    public void addStatusSorting()
    {
        java.util.List<ToolGroup> list = getDataprovider().getList();
        columnSort = new ListHandler<ToolGroup>(list);
        columnSort.setComparator(statusColumn, new Comparator<ToolGroup>() {

            public int compare(ToolGroup e1, ToolGroup e2)
            {
                if(e1 != null && e2 != null)
                {
                    if(e1.isStatus() == e2.isStatus())
                        return 0;
                    return !e1.isStatus() ? -1 : 1;
                } else
                {
                    return -1;
                }
            }

          
            
        }
);
        table.addColumnSortHandler(columnSort);
    }

    public void setEnable(boolean flag)
    {
    }

    public void applyStyle()
    {
    }

    public void addFieldUpdater()
    {
        setFieldupdaterName();
        
    }

    TextColumn<ToolGroup> nameColumn;
    TextColumn<ToolGroup> idColumn;
    TextColumn<ToolGroup> statusColumn;
    TextColumn<ToolGroup> categoryColumn;
    TextColumn<ToolGroup> codeColumn;

}
