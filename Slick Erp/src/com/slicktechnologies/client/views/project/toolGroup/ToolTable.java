// Decompiled by DJ v3.12.12.98 Copyright 2014 Atanas Neshkov  Date: 16-Sep-14 11:42:39 AM
// Home Page:  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   ToolTable.java

package com.slicktechnologies.client.views.project.toolGroup;

import com.google.gwt.cell.client.*;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.cellview.client.*;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.ProvidesKey;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.servicerelated.CompanyAsset;

import java.io.PrintStream;
import java.util.*;

public class ToolTable extends SuperTable<CompanyAsset>
{
	 TextColumn<CompanyAsset> nameColumn;
	    TextColumn<CompanyAsset> statusColumn;
	    TextColumn<CompanyAsset> idColumn;
	    TextColumn<CompanyAsset> brandColumn;
	    TextColumn<CompanyAsset> modelNoColumn;
	    TextColumn<CompanyAsset> srNoColumn;
	    TextColumn<CompanyAsset> manufactureDateColumn;
	    TextColumn<CompanyAsset> installationDateColumn;
	    Column<CompanyAsset,String> deleteColumn;


    public ToolTable()
    {
    }

    public ToolTable(UiScreen<CompanyAsset> view)
    {
        super(view);
    }

    private void createColumnName()
    {
        nameColumn = new TextColumn<CompanyAsset>() {

            public String getValue(CompanyAsset object)
            {
            	if(object.getName()!=null){
            		return object.getName();
            	}
            	else{
            		return "";
            	}
            }
        };
        table.addColumn(nameColumn, "Name");
        nameColumn.setSortable(true);
    }

    private void createColumnBrand()
    {
        brandColumn = new TextColumn<CompanyAsset>() {

            public String getValue(CompanyAsset object)
            {
            	if(object.getBrand()!=null){
            		return object.getBrand();
            	}
            	else{
            		return "";
            	}
            }
        };
        table.addColumn(brandColumn, "Brand");
        brandColumn.setSortable(true);
    }

    private void createColumnModelNo()
    {
        modelNoColumn = new TextColumn<CompanyAsset>() {

            public String getValue(CompanyAsset object)
            {
            	if(object.getModelNo()!=null){
            		return object.getModelNo();
            	}
            	else{
            		return "";
            	}
            }

           
        }
;
        table.addColumn(modelNoColumn, "Model Number");
        modelNoColumn.setSortable(true);
    }

    private void createColumnSrNo()
    {
        srNoColumn = new TextColumn<CompanyAsset>() {

            public String getValue(CompanyAsset object)
            {
            	if(object.getSrNo()!=null){
            		return object.getSrNo();
            	}
            	else{
            		return "";
            	}
            }

           
        }
;
        table.addColumn(srNoColumn, "Sr Number");
        srNoColumn.setSortable(true);
    }

    private void createColumnManufactureDate()
    {
        DateTimeFormat fmt = DateTimeFormat.getFormat(DateTimeFormat.PredefinedFormat.DATE_SHORT);
        DatePickerCell date = new DatePickerCell(fmt);
       
        manufactureDateColumn = new TextColumn<CompanyAsset>() {

            public String getValue(CompanyAsset object)
            {
            	if(object.getDateOfManufacture()!=null){
            		return AppUtility.parseDate(object.getDateOfManufacture());
            	}
            	else{
            		return "";
            	}
            }
        }
;
        table.addColumn(manufactureDateColumn, "Manufacture Date");
        manufactureDateColumn.setSortable(true);
    }

    private void createColumnInstallationDate()
    {
        installationDateColumn = new TextColumn<CompanyAsset>() {

            public String getValue(CompanyAsset object)
            {
            	if(object.getDateOfInstallation()!=null){
            		return AppUtility.parseDate(object.getDateOfInstallation());
            	}
            	else{
            		return "";
            	}
            }
        };
        table.addColumn(installationDateColumn, "Installation Date");
        installationDateColumn.setSortable(true);
    }

    protected void createColumndeleteColumn()
    {
        ButtonCell btnCell = new ButtonCell();
        deleteColumn = new Column<CompanyAsset,String>(btnCell) {

            public String getValue(CompanyAsset object)
            {
                return "Delete";
            }
        }
;
        table.addColumn(deleteColumn, "Delete");
    }

    public void createTable()
    {
        createColumnName();
        createColumnBrand();
        createColumnModelNo();
        createColumnSrNo();
        createColumnManufactureDate();
        createColumnInstallationDate();
        createColumndeleteColumn();
        table.setWidth("100%");
        createFieldUpdaterdeleteColumn();
    }

    private void setFieldupdaterName()
    {
        nameColumn.setFieldUpdater(new FieldUpdater<CompanyAsset,String>() {

            public void update(int index, CompanyAsset object, String value)
            {
                object.setName(value);
                


           
        }}
);
    }

    private void setFieldupdaterBrand()
    {
        brandColumn.setFieldUpdater(new FieldUpdater<CompanyAsset,String>() {

            public void update(int index, CompanyAsset object, String value)
            {
                object.setBrand(value);
            }}    

);
    }

    private void setFieldupdaterModelNo()
    {
        modelNoColumn.setFieldUpdater(new FieldUpdater<CompanyAsset,String>() {

            public void update(int index, CompanyAsset object, String value)
            {
                object.setModelNo(value);
                
            }}
);
    }

    private void setFieldupdaterSrNo()
    {
        srNoColumn.setFieldUpdater(new FieldUpdater<CompanyAsset,String>() {

            public void update(int index, CompanyAsset object, String value)
            {
                object.setSrNo(value);
                

            }
        }
);
    }

   /* private void setFieldupdaterManufactureDate()
    {
        manufactureDateColumn.setFieldUpdater(new FieldUpdater<CompanyAsst,Date>() {

            public void update(int index, CompanyAsst object, Date value)
            {
                object.setDateOfManufacture(value);
                

            }
        }
);
    }

    private void setFieldupdaterInstallationDate()
    {
        installationDateColumn.setFieldUpdater(new FieldUpdater<CompanyAsst,Date>() {

            public void update(int index, CompanyAsst object, Date value)
            {
                object.setDateOfInstallation(value);
                

        }}
);
    }*/

    protected void initializekeyprovider()
    {
        keyProvider = new ProvidesKey<CompanyAsset>() {

            public Object getKey(CompanyAsset item)
            {
                if(item == null)
                    return null;
                else
                    return item.getId();
            }

           
        }
;
    }

    public void addColumnSorting()
    {
        addNameSorting();
    }

    public void addNameSorting()
    {
        List<CompanyAsset> list = getDataprovider().getList();
        columnSort = new ListHandler<CompanyAsset>(list);
        columnSort.setComparator(nameColumn, new Comparator<CompanyAsset>() {

            public int compare(CompanyAsset e1, CompanyAsset e2)
            {
               
                if(e1 != null && e2 != null)
                {
                    if(e1.getName() != null && e2.getName() != null)
                        return e1.getName().compareTo(e2.getName());
                    else
                        return 0;
                } else
                {
                    return 0;
                }
            }

           
        }
);
        table.addColumnSortHandler(columnSort);
    }

    protected void createFieldUpdaterdeleteColumn()
    {
        deleteColumn.setFieldUpdater(new FieldUpdater<CompanyAsset,String>() {

            public void update(int index, CompanyAsset object, String value)
            {
                getDataprovider().getList().remove(index);
                table.redrawRow(index);
                
            }

           
        }
);
    }

   

    public void applyStyle()
    {
    }

    public void addFieldUpdater()
    {
        setFieldupdaterName();
        setFieldupdaterBrand();
        setFieldupdaterModelNo();
        setFieldupdaterSrNo();
     //   setFieldupdaterManufactureDate();
        //setFieldupdaterInstallationDate();
    }

   
    
    @Override
	public void setEnable(boolean state)
	{
        for(int i=table.getColumnCount()-1;i>-1;i--)
    	  table.removeColumn(i); 
          if(state==true)
          {
        	  createColumnName();
              createColumnBrand();
              createColumnModelNo();
              createColumnSrNo();
              createColumnManufactureDate();
              createColumnInstallationDate();
      		createColumndeleteColumn();
      		createFieldUpdaterdeleteColumn();
          }
          
          else
          {
        	  createColumnName();
              createColumnBrand();
              createColumnModelNo();
              createColumnSrNo();
              createColumnManufactureDate();
              createColumnInstallationDate();
      		addFieldUpdater();
      	
          }
	}

}
