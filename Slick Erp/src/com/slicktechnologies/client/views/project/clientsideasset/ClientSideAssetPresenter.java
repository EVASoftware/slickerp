package com.slicktechnologies.client.views.project.clientsideasset;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.servicerelated.ClientSideAsset;
import com.slicktechnologies.shared.common.servicerelated.CompanyAsset;

public class ClientSideAssetPresenter extends FormScreenPresenter<ClientSideAsset>
{

	ClientSideAssetForm form;
	 CsvServiceAsync csvservice=GWT.create(CsvService.class);
	 GenricServiceAsync service=GWT.create(GenricService.class);
	public ClientSideAssetPresenter(FormScreen<ClientSideAsset> view, ClientSideAsset model) {
		super(view, model);
		form=(ClientSideAssetForm) view;
		form.setPresenter(this);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.CLIENTSIDEASSET,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
		
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel lbl= (InlineLabel) e.getSource();
		if(lbl.getText().contains("New"))
		{
			form.setToNewState();
			ClientSideAssetPresenter.initalize();
		  
		}
	}
	
	@SuppressWarnings("unused")
	public static ClientSideAssetForm initalize()
	{
		 ClientSideAssetPresenterTable gentableScreen=new ClientSideAssetPresenterTableProxy();
			
			ClientSideAssetForm  form=new   ClientSideAssetForm();
			gentableScreen.setView(form);
			gentableScreen.applySelectionModle();
			
			 ClientSideAssetPresenterTable gentableSearch=new ClientSideAssetPresenterTableProxy();
			 gentableSearch.setView(form);
				gentableSearch.applySelectionModle();
			ClientSideAssetPresenterSearch.staticSuperTable=gentableSearch;
			 ClientSideAssetPresenterSearch searchpopup=new ClientSideAssetPresenterSearchProxy();
			           form.setSearchpopupscreen(searchpopup);
			
			 ClientSideAssetPresenter  presenter=new   ClientSideAssetPresenter  (form,new  ClientSideAsset());
			AppMemory.getAppMemory().stickPnel(form);
			return form;
		 
	}
	
	
		/**
		 * The Class ExpensemanagmentPresenterTable.
		 */
		@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.servicerelated.ClientSideAsset")
		 public static class  ClientSideAssetPresenterTable extends SuperTable< ClientSideAsset> implements GeneratedVariableRefrence{

			/* (non-Javadoc)
			 * @see com.slicktechnologies.client.utility.GeneratedVariableRefrence#getVarRef(java.lang.String)
			 */
			@Override
			public Object getVarRef(String varName) {
		
				return null;
			}

			/* (non-Javadoc)
			 * @see com.simplesoftwares.client.library.appstructure.SuperTable#createTable()
			 */
			@Override
			public void createTable() {
		
				
			}

			/* (non-Javadoc)
			 * @see com.simplesoftwares.client.library.appstructure.SuperTable#initializekeyprovider()
			 */
			@Override
			protected void initializekeyprovider() 
			{
	
				
			}

			/* (non-Javadoc)
			 * @see com.simplesoftwares.client.library.appstructure.SuperTable#addFieldUpdater()
			 */
			@Override
			public void addFieldUpdater() 
			{
		
				
			}

			/* (non-Javadoc)
			 * @see com.simplesoftwares.client.library.appstructure.SuperTable#setEnable(boolean)
			 */
			@Override
			public void setEnable(boolean state)
			{
			
				
			}

			/* (non-Javadoc)
			 * @see com.simplesoftwares.client.library.appstructure.ViewContainer#applyStyle()
			 */
			@Override
			public void applyStyle() 
			{

				
			}} ;
			
			/**
			 * The Class ExpensemanagmentPresenterSearch.
			 */
			@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.servicerelated.ClientSideAsset")
			 public static class   ClientSideAssetPresenterSearch extends SearchPopUpScreen<  ClientSideAsset>{

				/* (non-Javadoc)
				 * @see com.simplesoftwares.client.library.appstructure.SearchPopUpScreen#getQuerry()
				 */
				@Override
				public MyQuerry getQuerry() {
			
					return null;
				}

				@Override
				public boolean validate() {
					// TODO Auto-generated method stub
					return true;
				}};
	
	

	@Override
	public void reactOnPrint() {
		
		
	}

	@Override
	public void reactOnEmail() {
		
		
	}

	@Override
	protected void makeNewModel() {
	
		model=new ClientSideAsset();
		
	}
	
	 public void reactOnDownload()
	    {
	    	ArrayList<ClientSideAsset> custarray=new ArrayList<ClientSideAsset>();
			List<ClientSideAsset> list=(List<ClientSideAsset>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
		
			custarray.addAll(list);
			
			csvservice.setclientsideassetlist(custarray, new AsyncCallback<Void>() {

				@Override
				public void onFailure(Throwable caught) {
					System.out.println("RPC call Failed"+caught);
					
				}

				@Override
				public void onSuccess(Void result) {
					

					String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url=gwt + "csvservlet"+"?type="+24;
					Window.open(url, "test", "enabled");
					
				}
			});
	    }


	public MyQuerry getClientSideAssetQuery()
	{
		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new ClientSideAsset());
		return quer;
	}
	
	public void setModel(ClientSideAsset entity)
	{
		model=entity;
	}

/***************************************Buisness Logic Part************************************************/
	public void validateContractId()
	{
		Integer contractId = (Integer)form.ibContractid.getValue();
	    if(contractId != null)
	    {
	        Filter temp = new Filter();
	        temp.setIntValue(contractId);
	        temp.setQuerryString("personInfo.count");
	        MyQuerry querry = new MyQuerry();
	        querry.setQuerryObject(new Contract());
	        querry.getFilters().add(temp);
	        service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			 
	        	
	            public void onSuccess(ArrayList<SuperModel> result)
	            {
	                if(result.size()==0)
	                {
	                	form.showDialogMessage("No Approved Contract exists Corresponding to this ID !");
	                	form.ibContractid.setText("");
	                	form.pic.clear();
	                	return;
	                }
	            	Contract cont = (Contract)result.get(0);
	                if(cont.getStartDate() == null)
	                {
	                    form.showDialogMessage("The Contract does not have any service !");
	                    form.ibContractid.setText("");
	                    form.pic.clear();
	                	return;
	                } else
	                {
	                 //   form.getDbContractStartDate().setValue(cont.getStartDate());
	                  //  form.getDbContractEndDate().setValue(cont.getEndDate());
	                    form.getPic().setValue(cont.getCinfo());
	                   // ProjectPresenter.form.fpic.getMandatoryMsgWidget().setVisible(false);
	                    form.pic.makeMandatoryMessageFalse();
	                }
	            }
	
	            public void onFailure(Throwable throwable)
	            {
	            	form.showDialogMessage("Failed to retreive Contract details !");
	            }
	
	        }
	);
	    }
	}







}
