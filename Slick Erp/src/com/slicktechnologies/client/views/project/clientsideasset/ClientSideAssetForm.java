package com.slicktechnologies.client.views.project.clientsideasset;

import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.LongBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.composite.UploadComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.config.configurations.ConfigTypes;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.servicerelated.ClientSideAsset;


public class ClientSideAssetForm extends FormScreen<ClientSideAsset>  implements ClickHandler, ChangeHandler{
	
	final GenricServiceAsync async =GWT.create(GenricService.class);


	/********************************* Variable Declaration********************************************/
	 IntegerBox ibContractid;
	 PersonInfoComposite pic;
	
	 TextBox assetName,tbBrand,tbModelNo,tbSrNo,tbPurchsedFrom,tblattitude,tblongitude,ibPoNo;
	 DateBox dbDateofManufacture,dbdateofInstallation,dbpurchaseDate;
	 UploadComposite upload;
	 ObjectListBox<Config> olbAssetCategory;
	 DateBox cbWarenty;
	 CheckBox mandatoryService;
	
	 TextBox tbAssetid;
	 TextBox tbSaleOrderId;
	 TextBox tbWorkOrderId;
	 TextBox tbProductId;
	 TextBox tbMaterialId;
	
	/********************************* Variable Declaration Ends********************************************/
	 ClientSideAsset clientSideAssetObj;
	
	public ClientSideAssetForm()
	{
		super();
		createGui();
		tbAssetid.setEnabled(false);
		
		
	}
	/**
	 * Instantiates a new config form.
	 *
	 * @param table the table
	 * @param mode the mode
	 * @param captionmode the captionmode
	 */
	
	
	public ClientSideAssetForm  (String[] processlevel, FormField[][] fields,
			FormStyle formstyle) 
	{
		super(processlevel, fields, formstyle);
		createGui();
		tbAssetid.setEnabled(false);
	}
	

	//Form Filed Variables

	public PersonInfoComposite getPic() {
		return pic;
	}
	public void setPic(PersonInfoComposite pic) {
		this.pic = pic;
	}



	
	
	/** The buildstatus. */
	
	
	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen#toggleAppHeaderBarMenu()
	 */
	@Override
	public void toggleAppHeaderBarMenu()
	{
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Save")||text.contains("Discard")|| text.contains("Search")|| text.contains(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
				
			}
		}
		
		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Save")||text.contains("Discard")|| text.contains(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
			}
		}
		
		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Edit")||text.contains("Discard")||text.contains("Search")|| text.contains(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.CLIENTSIDEASSET,LoginPresenter.currentModule.trim());
	}

	/**
	 * Initialize widgets.
	 */
	private void initializeWidgets()
	{
		tbAssetid=new TextBox();
		
		/** The name. */
		 assetName=new TextBox();
		 pic = AppUtility.customerInfoComposite(new Customer());
		 tbBrand=new TextBox();
		 tbModelNo=new TextBox();
		 tbSrNo=new TextBox();
		 tbPurchsedFrom=new TextBox();;
		 dbDateofManufacture=new DateBoxWithYearSelector();
		 dbdateofInstallation=new DateBoxWithYearSelector();
		 upload=new UploadComposite();
		 cbWarenty=new DateBoxWithYearSelector();
		 tblattitude=new TextBox();
		 tblongitude=new TextBox();
		 dbpurchaseDate=new DateBoxWithYearSelector();
		 ibPoNo=new TextBox();
		 ibContractid=new IntegerBox();
		 olbAssetCategory=new ObjectListBox<Config>();
		 mandatoryService=new CheckBox();
		 
		 tbSaleOrderId = new TextBox();
		 tbWorkOrderId = new TextBox();
		 tbProductId = new TextBox();
		 tbMaterialId = new TextBox();
		 
		 AppUtility.MakeLiveConfig(olbAssetCategory, Screen.CLIENTSIDEASSETCATEGORY);
		
	}
	
	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen#createScreen()
	 */
	@Override
	public void createScreen() {
		
		/** The fieldstatus. */
		 FormField fieldname,fieldid,fieldBrand,fieldModelNo,fieldSrNo,fieldDateOfManufacture,fieldDateOfInstallation,fieldContractId,
		fieldPurchsedFrom,fieldUpload,fieldWarenty,fieldAssetCategory,fieldlattitude,fieldlongitude,fpic,fieldpurchaseDate,fieldpoNo,fieldtbMaterialId;
		
		 FormFieldBuilder fbuilder;
		 processlevelBarNames=new String[]{"Delete","New"};
		initializeWidgets();
		
		fbuilder=new FormFieldBuilder();
		
		
		FormField freferencedetails=fbuilder.setlabel("Reference Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();

		fbuilder = new FormFieldBuilder("Sales Order Id",tbSaleOrderId);
		FormField fibSaleOrderId= fbuilder.setMandatory(false).build();
		
		fbuilder = new FormFieldBuilder("WorkOrder Id",tbWorkOrderId);
		FormField fibWorkOrderId= fbuilder.setMandatory(false).build();
		
		fbuilder = new FormFieldBuilder("Product Id",tbProductId);
		FormField fibProductId= fbuilder.setMandatory(false).build();
		
		// FormField fgroupingCustomerInformation = fbuilder.setlabel("Customer Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
	        fbuilder = new FormFieldBuilder("", pic);
	         fpic = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		
	    fbuilder=new FormFieldBuilder("* Name",assetName);
		fieldname=fbuilder.setMandatory(true).setMandatoryMsg("Name is Mandatory !").build();
		
		fbuilder=new FormFieldBuilder("* Asset Id",tbAssetid);
		fieldid=fbuilder.setMandatory(false).setMandatoryMsg("Id is Mandatory !").build();
		
		fbuilder=new FormFieldBuilder("Material Id",tbMaterialId);
		fieldtbMaterialId=fbuilder.setMandatory(false).build();
		
		fbuilder=new FormFieldBuilder("Model Number",tbModelNo);
		fieldModelNo=fbuilder.setMandatory(false).build();
		
		fbuilder=new FormFieldBuilder("Brand",tbBrand);
		fieldBrand=fbuilder.setMandatory(false).build();
		
		fbuilder=new FormFieldBuilder("Sr. Number",tbSrNo);
		fieldSrNo=fbuilder.setMandatory(false).build();
		
		fbuilder=new FormFieldBuilder("Date of Manufacture",dbDateofManufacture);
		fieldDateOfManufacture=fbuilder.setMandatory(false).build();
		
		fbuilder=new FormFieldBuilder("Date of Installation",dbdateofInstallation);
		fieldDateOfInstallation=fbuilder.setMandatory(false).build();
		
		fbuilder=new FormFieldBuilder("Upload Image",upload);
		fieldUpload=fbuilder.setMandatory(true).build();
		
		fbuilder=new FormFieldBuilder("Warranty Until",cbWarenty);
		fieldWarenty=fbuilder.setMandatory(false).build();
		
		fbuilder=new FormFieldBuilder("Purchased From", tbPurchsedFrom);
		fieldPurchsedFrom=fbuilder.setMandatory(false).build();
		
		fbuilder=new FormFieldBuilder("Purchased Date", dbpurchaseDate);
		fieldpurchaseDate=fbuilder.setMandatory(false).build();
		
		fbuilder=new FormFieldBuilder("PO No", ibPoNo);
		fieldpoNo=fbuilder.setMandatory(false).build();
		
		fbuilder = new FormFieldBuilder("* Asset Categoty",olbAssetCategory);
		fieldAssetCategory= fbuilder.setMandatory(true).setMandatoryMsg("Asset Category is Mandatory !").build();

		fbuilder=new FormFieldBuilder("Lattitude", tblattitude);
		fieldlattitude=fbuilder.setMandatory(false).build();
		
		fbuilder=new FormFieldBuilder("Longitude", tblongitude);
		fieldlongitude=fbuilder.setMandatory(false).build();
		
		fbuilder=new FormFieldBuilder("* Contract Id", ibContractid);
		fieldContractId=fbuilder.setMandatory(true).setMandatoryMsg("Contract Id is Mandatory !").build();
		
		fbuilder=new FormFieldBuilder("Service Mandatory",mandatoryService);
		FormField fieldManService=fbuilder.build();
		
		
		FormField fgroupingAssetInfo=fbuilder.setlabel("ClientSide Asset Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		
		this.fields=new FormField[][]{
				{freferencedetails},
				{fibSaleOrderId,fibWorkOrderId,fibProductId},
				{fgroupingAssetInfo},
				{fpic,fieldManService},
				{fieldid,fieldtbMaterialId,fieldname,fieldAssetCategory},
				{fieldBrand,fieldModelNo,fieldSrNo,fieldPurchsedFrom,},
				{fieldpoNo,fieldpurchaseDate,fieldDateOfManufacture,fieldDateOfInstallation},
				{fieldWarenty,fieldUpload,fieldlattitude,fieldlongitude}
			};		
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen#updateModel(java.lang.Object)
	 */
	@Override
	public void updateModel(ClientSideAsset model)
	{
		if(assetName.getValue()!=null)
			model.setName(assetName.getValue());
		if(tbBrand.getValue()!=null)
			model.setBrand(tbBrand.getValue());
		if(tbModelNo.getValue()!=null)
			model.setModelNo(tbModelNo.getValue());
		if(tbSrNo.getValue()!=null)
			model.setSrNo(tbSrNo.getValue());
		if(tbPurchsedFrom.getValue()!=null)
			model.setPurchasedFrom(tbPurchsedFrom.getValue());
		if(dbDateofManufacture.getValue()!=null)
			model.setDateOfManufacture(dbDateofManufacture.getValue());
		if(dbdateofInstallation.getValue()!=null)
			model.setDateOfInstallation(dbdateofInstallation.getValue());
		if(cbWarenty.getValue()!=null)
			model.setWarrenty(cbWarenty.getValue());
		if(olbAssetCategory.getValue()!=null)
			model.setCategory(olbAssetCategory.getValue());
		if(tblattitude.getValue()!=null)
			model.setLattitude(tblattitude.getValue());
		if(tblongitude.getValue()!=null)
			model.setLongitude(tblongitude.getValue());
		if(dbpurchaseDate.getValue()!=null)
			model.setPODate(dbpurchaseDate.getValue());
		if(ibPoNo.getValue()!=null)
			model.setPoNo(ibPoNo.getValue());
		if(ibContractid.getValue()!=null)
			model.setContractId(ibContractid.getValue());
		 if(pic.getValue() != null)
	            model.setPersonInfo(pic.getValue());
		 if(mandatoryService.getValue() != null)
	            model.setMandatoryService(mandatoryService.getValue());
		 model.setDocument(upload.getValue());
	 
			
		 if(!tbSaleOrderId.getValue().equals(""))
			 model.setSalesOrderId(Integer.parseInt(tbSaleOrderId.getValue()));
		 
		 if(!tbWorkOrderId.getValue().equals(""))
			 model.setWorkOrderId(Integer.parseInt(tbWorkOrderId.getValue()));
		 
		 if(!tbProductId.getValue().equals(""))
			 model.setProductId(Integer.parseInt(tbProductId.getValue()));
		 
		 if(!tbMaterialId.getValue().equals(""))
			 model.setMaterialId(Integer.parseInt(tbMaterialId.getValue()));
		
		 clientSideAssetObj=model;
		
		 presenter.setModel(model);
	
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen#updateView(java.lang.Object)
	 */
	@Override
	public void updateView(ClientSideAsset view) 
	{
		
		
		clientSideAssetObj=view;
		
		checkAssetCategory(view.getCategory());

		
		 if(view.getCount() != -1)
	            tbAssetid.setValue(Integer.valueOf(view.getCount())+"");
		if(view.getName()!=null)
			assetName.setValue(view.getName());
		if(view.getBrand()!=null)
			tbBrand.setValue(view.getBrand());
		if(view.getModelNo()!=null)
			tbModelNo.setValue(view.getModelNo());
		if(view.getSrNo()!=null)
			tbSrNo.setValue(view.getSrNo());
		if(view.getPurchasedFrom()!=null)
			tbPurchsedFrom.setValue(view.getPurchasedFrom());
		if(view.getDateOfManufacture()!=null)
			dbDateofManufacture.setValue(view.getDateOfManufacture());
		if(view.getDateOfInstallation()!=null)
			dbdateofInstallation.setValue(view.getDateOfInstallation());
		if(view.getDocument()!=null)
			upload.setValue(view.getDocument());
		if(view.getLattitude()!=null)
			tblattitude.setValue(view.getLattitude());
		if(view.getLongitude()!=null)
			tblongitude.setValue(view.getLongitude());
		if(view.getCategory()!=null)
			olbAssetCategory.setValue(view.getCategory());
		
		if(view.getPODate()!=null)
			dbpurchaseDate.setValue(view.getPODate());
		if(view.getContractId()!=null)
			ibContractid.setValue(view.getContractId());
		if(view.getPoNo()!=null)
			ibPoNo.setValue(view.getPoNo());
		if(view.getPersonInfo() != null)
            pic.setValue(view.getPersonInfo());
		cbWarenty.setValue(view.isWarrenty());
		
		if(view.getMandatoryService()!=null)
			mandatoryService.setValue(view.getMandatoryService());
		
		
		if(view.getSalesOrderId()!=-1)
			tbSaleOrderId.setValue(view.getSalesOrderId()+"");
		if(view.getWorkOrderId()!=-1)
			tbWorkOrderId.setValue(view.getWorkOrderId()+"");
		if(view.getProductId()!=-1)
			tbProductId.setValue(view.getProductId()+"");
		if(view.getMaterialId()!=null)
			tbMaterialId.setValue(view.getMaterialId()+"");

		presenter.setModel(view);
	}

	
	private void checkAssetCategory(String category) {
		// TODO Auto-generated method stub
		
		System.out.println("== 1");
		
		Boolean flag = false;
		for(int i=0;i<olbAssetCategory.getItemCount();i++)
		{
			String text = olbAssetCategory.getItemText(i);
			if(text.equals(category)){
				flag=true;
				break;
			}
		}
		
		if(flag==false){
			
			System.out.println("== 2");

			getConfigDetails(category);
		}
		
	}
	
	
	private void getConfigDetails(String category) {
		// TODO Auto-generated method stub
		
		System.out.println("== 3");

		
		MyQuerry query = new MyQuerry();
		
		
	  	Company c = new Company();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
	  	
	  	filter = new Filter();
	  	filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("type");
	  	filter.setIntValue(ConfigTypes.getConfigType(Screen.CLIENTSIDEASSETCATEGORY));
	  	filtervec.add(filter);   
	  	
	  	filter = new Filter();
	  	filter.setQuerryString("name");
	  	filter.setStringValue(category);
		filtervec.add(filter);   

	  	filter = new Filter();
	  	filter.setQuerryString("status");
	  	filter.setBooleanvalue(true);
		filtervec.add(filter);   
		
		
	    
		query.setFilters(filtervec);
		query.setQuerryObject(new Config());
		
		async.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
//				System.out.println("===="+result.get(0));
				System.out.println("Result"+result.size());
				System.out.println("Count Before =="+olbAssetCategory.getItemCount());
				
				if(result.size()!=0){
				Config config =(Config) result.get(0);
				LoginPresenter.globalConfig.add(config);
				olbAssetCategory.addItem(config.getName());
				olbAssetCategory.setValue(config.getName());
				System.out.println("Count After =="+olbAssetCategory.getItemCount());
				
				}

			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				System.out.println("Fail==========");

			}
		});
	}
	/**
	 * Sets the presenter.
	 *
	 * @param presenter the new presenter
	 */
	public void setPresenter(ClientSideAssetPresenter presenter) {
		this.presenter = presenter;
	}

	

	@Override
	public void setToViewState() {
		super.setToViewState();
		
		SuperModel model=new ClientSideAsset();
		model=clientSideAssetObj;
		AppUtility.addDocumentToHistoryTable(AppConstants.ASSETMANEGEMENTMODULE,AppConstants.CLIENTASSET, clientSideAssetObj.getCount(), clientSideAssetObj.getCustomerCount(),clientSideAssetObj.getCustomerName(),clientSideAssetObj.getCustomerCell(), false, model, null);
	
		
	}
	
	@Override
	public void setToNewState() {
		super.setToNewState();
		
		System.out.println("Inside NEW method");
		if(clientSideAssetObj!=null){

			SuperModel model=new ClientSideAsset();
			model=clientSideAssetObj;
			AppUtility.addDocumentToHistoryTable(AppConstants.ASSETMANEGEMENTMODULE,AppConstants.CLIENTASSET, clientSideAssetObj.getCount(), clientSideAssetObj.getCustomerCount(),clientSideAssetObj.getCustomerName(),clientSideAssetObj.getCustomerCell(), false, model, null);
		
		}
	}
	
	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen#clear()
	 */
	@Override
	public void clear() 
	{
		super.clear();	
	}

	 public void setCount(int count)
	    {
	        tbAssetid.setValue(Integer.valueOf(count)+"");
	    }
	@Override
	public boolean validate()
	{
		boolean superValidate=super.validate();
		if(superValidate==false)
			return superValidate;
		Date warrantyDate=this.cbWarenty.getValue();
		Date installationDate=this.dbdateofInstallation.getValue();
		Date manufacturingDate=this.dbDateofManufacture.getValue();
		Date purchaseDate=this.dbpurchaseDate.getValue();
	    if(installationDate!=null &&warrantyDate!=null)
	    {
		boolean lastDate=warrantyDate.after(installationDate);
		if(lastDate!=true)
		{
			showDialogMessage("Please Select Warranty Date after Installation Date!");
			return false;
		}
		}
	    if(installationDate!=null&&purchaseDate!=null)
	    	
	    {
	    	boolean midleDate=installationDate.after(purchaseDate);
	    	if(midleDate!=true)
			{
				showDialogMessage("Please Select Installation Date after Purchase Date!");
				return false;
			}
	    }
	    
	    
	    
	
		if(purchaseDate!=null && manufacturingDate!=null)
		{
		boolean firstDate=purchaseDate.after(manufacturingDate);
		if(firstDate!=true)
		{
			showDialogMessage("Please Select Purchase Date after Manufacturing Date!");
			return false;
		}
		
		}
		
		return true;
	}
	
	
	@Override
	public void setToEditState() {
		// TODO Auto-generated method stub
		super.setToEditState();
		this.tbAssetid.setEnabled(false);
		this.ibContractid.setEnabled(false);
		this.pic.setEnabled(false);
		this.mandatoryService.setEnabled(false);
	}
	
	public void setEnabled(boolean state)
	{
		super.setEnable(state);
		this.tbAssetid.setEnabled(false);
	}
	
	
	
	public ObjectListBox<Config> getOlbAssetCategory() {
		return olbAssetCategory;
	}
	public void setOlbAssetCategory(ObjectListBox<Config> olbAssetCategory) {
		this.olbAssetCategory = olbAssetCategory;
	}
	@Override
	public void onChange(ChangeEvent event) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
	}
	

}
