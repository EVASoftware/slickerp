package com.slicktechnologies.client.views.project.clientsideasset;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.ProvidesKey;
import com.slicktechnologies.client.views.project.clientsideasset.ClientSideAssetPresenter.ClientSideAssetPresenterTable;
import com.slicktechnologies.shared.common.servicerelated.ClientSideAsset;

public class ClientSideAssetPresenterTableProxy extends ClientSideAssetPresenterTable {
	TextColumn<ClientSideAsset> getLattitudeColumn;
	TextColumn<ClientSideAsset> getLongitudeColumn;
	TextColumn<ClientSideAsset> getAssetidColumn;
	TextColumn<ClientSideAsset> getBrandColumn;
	TextColumn<ClientSideAsset> getCategoryColumn;
	TextColumn<ClientSideAsset> getNameColumn;
	TextColumn<ClientSideAsset> getContractId;
	//TextColumn<ClientSideAsset> getCountColumn;
	public Object getVarRef(String varName)
	{
		if(varName.equals("getLattitudeColumn"))
			return this.getLattitudeColumn;
		if(varName.equals("getLongitudeColumn"))
			return this.getLongitudeColumn;
		if(varName.equals("getAssetidColumn"))
			return this.getAssetidColumn;
		if(varName.equals("getBrandColumn"))
			return this.getBrandColumn;
		if(varName.equals("getCategoryColumn"))
			return this.getCategoryColumn;
		if(varName.equals("getNameColumn"))
			return this.getNameColumn;
		
		return null ;
	}
	public ClientSideAssetPresenterTableProxy()
	{
		super();
	}
	@Override public void createTable() {
		//addColumngetCount();
		addColumngetAssetid();
		//addColumngetContractId();
		addColumngetName();
		addColumngetCategory();
		addColumngetBrand();
		addColumngetLattitude();
		addColumngetLongitude();
		addColumnSorting();
	}
	@Override
	protected void initializekeyprovider() {
		keyProvider= new ProvidesKey<ClientSideAsset>()
				{
			@Override
			public Object getKey(ClientSideAsset item)
			{
				if(item==null)
				{
					return null;
				}
				else
					return item.getId();
			}
				};
	}
	@Override
	public void setEnable(boolean state)
	{
	}
	@Override
	public void applyStyle()
	{
	}
	public void addColumnSorting(){
		addSortinggetAssetid();
		addSortinggetName();
		addSortinggetCategory();
		addSortinggetBrand();
		addSortinggetLattitude();
		addSortinggetLongitude();
		//addSortinggetContractId();
	}
	@Override public void addFieldUpdater() {
	}
	/*protected void addColumngetCount()
	{
		getCountColumn=new TextColumn<ClientSideAsset>()
				{
			@Override
			public String getValue(ClientSideAsset object)
			{
				if( object.getCount()==-1)
					return "N.A";
				else return object.getCount()+"";
			}
				};
				table.addColumn(getCountColumn,"Id");
	}*/
	protected void addSortinggetAssetid()
	{
		List<ClientSideAsset> list=getDataprovider().getList();
		columnSort=new ListHandler<ClientSideAsset>(list);
		columnSort.setComparator(getAssetidColumn, new Comparator<ClientSideAsset>()
				{
			@Override
			public int compare(ClientSideAsset e1,ClientSideAsset e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetAssetid()
	{
		getAssetidColumn=new TextColumn<ClientSideAsset>()
				{
			@Override
			public String getValue(ClientSideAsset object)
			{
				return object.getCount()+"";
			}
				};
				table.addColumn(getAssetidColumn,"Asset Id");
				getAssetidColumn.setSortable(true);
	}
	protected void addSortinggetName()
	{
		List<ClientSideAsset> list=getDataprovider().getList();
		columnSort=new ListHandler<ClientSideAsset>(list);
		columnSort.setComparator(getNameColumn, new Comparator<ClientSideAsset>()
				{
			@Override
			public int compare(ClientSideAsset e1,ClientSideAsset e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getName()!=null && e2.getName()!=null){
						return e1.getName().compareTo(e2.getName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetName()
	{
		getNameColumn=new TextColumn<ClientSideAsset>()
				{
			@Override
			public String getValue(ClientSideAsset object)
			{
				return object.getName()+"";
			}
				};
				table.addColumn(getNameColumn,"Asset Name");
				getNameColumn.setSortable(true);
	}
	protected void addSortinggetCategory()
	{
		List<ClientSideAsset> list=getDataprovider().getList();
		columnSort=new ListHandler<ClientSideAsset>(list);
		columnSort.setComparator(getCategoryColumn, new Comparator<ClientSideAsset>()
				{
			@Override
			public int compare(ClientSideAsset e1,ClientSideAsset e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getCategory()!=null && e2.getCategory()!=null){
						return e1.getCategory().compareTo(e2.getCategory());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetCategory()
	{
		getCategoryColumn=new TextColumn<ClientSideAsset>()
				{
			@Override
			public String getValue(ClientSideAsset object)
			{
				return object.getCategory()+"";
			}
				};
				table.addColumn(getCategoryColumn,"Category");
				getCategoryColumn.setSortable(true);
	}
	protected void addSortinggetBrand()
	{
		List<ClientSideAsset> list=getDataprovider().getList();
		columnSort=new ListHandler<ClientSideAsset>(list);
		columnSort.setComparator(getBrandColumn, new Comparator<ClientSideAsset>()
				{
			@Override
			public int compare(ClientSideAsset e1,ClientSideAsset e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBrand()!=null && e2.getBrand()!=null){
						return e1.getBrand().compareTo(e2.getBrand());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetBrand()
	{
		getBrandColumn=new TextColumn<ClientSideAsset>()
				{
			@Override
			public String getValue(ClientSideAsset object)
			{
				return object.getBrand()+"";
			}
				};
				table.addColumn(getBrandColumn,"Brand");
				getBrandColumn.setSortable(true);
	}
	protected void addSortinggetLattitude()
	{
		List<ClientSideAsset> list=getDataprovider().getList();
		columnSort=new ListHandler<ClientSideAsset>(list);
		columnSort.setComparator(getLattitudeColumn, new Comparator<ClientSideAsset>()
				{
			@Override
			public int compare(ClientSideAsset e1,ClientSideAsset e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getLattitude()!=null && e2.getLattitude()!=null){
						return e1.getLattitude().compareTo(e2.getLattitude());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetLattitude()
	{
		getLattitudeColumn=new TextColumn<ClientSideAsset>()
				{
			@Override
			public String getValue(ClientSideAsset object)
			{
				return object.getLattitude()+"";
			}
				};
				table.addColumn(getLattitudeColumn,"Lattitude");
				getLattitudeColumn.setSortable(true);
	}
	protected void addSortinggetLongitude()
	{
		List<ClientSideAsset> list=getDataprovider().getList();
		columnSort=new ListHandler<ClientSideAsset>(list);
		columnSort.setComparator(getLongitudeColumn, new Comparator<ClientSideAsset>()
				{
			@Override
			public int compare(ClientSideAsset e1,ClientSideAsset e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getLongitude()!=null && e2.getLongitude()!=null){
						return e1.getLongitude().compareTo(e2.getLongitude());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetLongitude()
	{
		getLongitudeColumn=new TextColumn<ClientSideAsset>()
				{
			@Override
			public String getValue(ClientSideAsset object)
			{
				return object.getLongitude()+"";
			}
				};
				table.addColumn(getLongitudeColumn,"Longitude");
				getLongitudeColumn.setSortable(true);
	}
	
	
	protected void addSortinggetContractId()
	{
		List<ClientSideAsset> list=getDataprovider().getList();
		columnSort=new ListHandler<ClientSideAsset>(list);
		columnSort.setComparator(getContractId, new Comparator<ClientSideAsset>()
				{
			@Override
			public int compare(ClientSideAsset e1,ClientSideAsset e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getContractId()!=null && e2.getContractId()!=null){
						return e1.getContractId().compareTo(e2.getContractId());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetContractId()
	{
		getContractId=new TextColumn<ClientSideAsset>()
				{
			@Override
			public String getValue(ClientSideAsset object)
			{
				return object.getContractId()+"";
			}
				};
				table.addColumn(getContractId,"Contract Id");
				getContractId.setSortable(true);
	}
}
