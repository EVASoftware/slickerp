package com.slicktechnologies.client.views.project.clientsideasset;

import java.util.Vector;

import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.project.clientsideasset.ClientSideAssetPresenter.ClientSideAssetPresenterSearch;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.servicerelated.ClientSideAsset;

public class ClientSideAssetPresenterSearchProxy extends ClientSideAssetPresenterSearch {
	public TextBox tblattitude;
	public TextBox tblongitude;
	public IntegerBox ibAssetid;
	public TextBox tbBrand;
	public  ObjectListBox<Config> olbAssetCategory;
	public TextBox assetName;
	
	public IntegerBox ibSalesOrderId;
	public IntegerBox ibWorkOrderId;
	public IntegerBox ibproductId;
	
	public PersonInfoComposite personInfo;
	
	
	public Object getVarRef(String varName)
	{
		if(varName.equals("tblattitude"))
			return this.tblattitude;
		if(varName.equals("tblongitude"))
			return this.tblongitude;
		if(varName.equals("ibAssetid"))
			return this.ibAssetid;
		if(varName.equals("tbBrand"))
			return this.tbBrand;
		if(varName.equals("olbAssetCategory"))
			return this.olbAssetCategory;
		if(varName.equals("assetName"))
			return this.assetName;
		return null ;
	}
	public ClientSideAssetPresenterSearchProxy()
	{
		super();
		createGui();
	}
	public void initWidget()
	{
		tblattitude= new TextBox();
		tblongitude= new TextBox();
		ibAssetid= new IntegerBox();
		tbBrand= new TextBox();
		olbAssetCategory= new  ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbAssetCategory, Screen.CLIENTSIDEASSETCATEGORY);
		assetName= new TextBox();
		
		ibSalesOrderId= new IntegerBox();
		ibWorkOrderId = new IntegerBox();
		ibproductId = new IntegerBox();
		
		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new Customer());
		personInfo=new PersonInfoComposite(querry,false);
	}
	public void createScreen()
	{
		initWidget();
		FormFieldBuilder builder;
		builder = new FormFieldBuilder("Lattitude",tblattitude);
		FormField ftblattitude= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Longitude",tblongitude);
		FormField ftblongitude= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Asset Id",ibAssetid);
		FormField fibAssetid= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Brand",tbBrand);
		FormField ftbBrand= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Category",olbAssetCategory);
		FormField folbAssetCategory= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Asset Name",assetName);
		FormField fassetName= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Sales Order Id",ibSalesOrderId);
		FormField fibSalesOrderId= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Work Order Id",ibWorkOrderId);
		FormField fibWorkOrderId= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Product Id",ibproductId);
		FormField fibproductId= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("",personInfo);
		FormField fpersonInfo= builder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		
		
		this.fields=new FormField[][]{
				{fibSalesOrderId,fibWorkOrderId,fibproductId},
				{fibAssetid,fassetName,folbAssetCategory},
				{ftbBrand,ftblattitude,ftblongitude},
				{fpersonInfo}
		};
	}
	public MyQuerry getQuerry()
	{
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		if(ibAssetid.getValue()!=null){
			temp=new Filter();
			temp.setIntValue(ibAssetid.getValue());
			filtervec.add(temp);
			temp.setQuerryString("count");
			filtervec.add(temp);
		}
		if(assetName.getValue().trim().equals("")==false){
			temp=new Filter();temp.setStringValue(assetName.getValue().trim());
			filtervec.add(temp);
			temp.setQuerryString("name");
			filtervec.add(temp);
		}
		if(olbAssetCategory.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbAssetCategory.getValue().trim());
			filtervec.add(temp);
			temp.setQuerryString("category");
			filtervec.add(temp);
		}
		if(tbBrand.getValue().trim().equals("")==false){
			temp=new Filter();temp.setStringValue(tbBrand.getValue().trim());
			filtervec.add(temp);
			temp.setQuerryString("brand");
			filtervec.add(temp);
		}
		if(tblattitude.getValue().trim().equals("")==false){
			temp=new Filter();temp.setStringValue(tblattitude.getValue().trim());
			filtervec.add(temp);
			temp.setQuerryString("lattitude");
			filtervec.add(temp);
		}
		if(tblongitude.getValue().trim().equals("")==false){
			temp=new Filter();temp.setStringValue(tblongitude.getValue().trim());
			filtervec.add(temp);
			temp.setQuerryString("longitude");
			filtervec.add(temp);
		}
		
		if(ibSalesOrderId.getValue()!=null){
			System.out.println("hi");
			temp=new Filter();
			temp.setIntValue(ibSalesOrderId.getValue());
			filtervec.add(temp);
			temp.setQuerryString("salesOrderId");
			filtervec.add(temp);
		}
		
		if(ibWorkOrderId.getValue()!=null){
			System.out.println("h2i");

			temp=new Filter();
			temp.setIntValue(ibWorkOrderId.getValue());
			filtervec.add(temp);
			temp.setQuerryString("workOrderId");
			filtervec.add(temp);
		}
		
		if(ibproductId.getValue()!=null){
			System.out.println("hi3");

			temp=new Filter();
			temp.setIntValue(ibproductId.getValue());
			filtervec.add(temp);
			temp.setQuerryString("productId");
			filtervec.add(temp);
		}
		
		
		if(personInfo.getIdValue()!=-1)
		{
			System.out.println("CUSTOMER ID NOT NULL ::: "+personInfo.getIdValue());
			temp=new Filter();
			temp.setIntValue(personInfo.getIdValue());
			temp.setQuerryString("personInfo.count");
			filtervec.add(temp);
		}
		  
		if(!(personInfo.getFullNameValue().equals("")))
		{
			System.out.println("CUSTOMER NAME NOT NULL "+personInfo.getFullNameValue());
			temp=new Filter();
			temp.setStringValue(personInfo.getFullNameValue());
			temp.setQuerryString("personInfo.fullName");
			filtervec.add(temp);
		}
		if(personInfo.getCellValue()!=-1l)
		{
			System.out.println("CUSTOMER CELL NOT NULL "+personInfo.getCellValue());
			temp=new Filter();
			temp.setLongValue(personInfo.getCellValue());
			temp.setQuerryString("personInfo.cellNumber");
			filtervec.add(temp);
		}
		
		MyQuerry querry= new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new ClientSideAsset());
		return querry;
	}
	
	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
		return super.validate();
	}
}
