
// Decompiled by DJ v3.12.12.98 Copyright 2014 Atanas Neshkov  Date: 16-Sep-14 11:42:04 AM
// Home Page:  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   ToolPresenter.java

package com.slicktechnologies.client.views.project.tool;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.*;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.complain.ServiceDateBoxPopup;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.servicerelated.AssetMovementInfo;
import com.slicktechnologies.shared.common.servicerelated.CompanyAsset;
import com.google.gwt.user.client.Timer;
import java.util.*;

public class ToolPresenter extends FormScreenPresenter<CompanyAsset>  implements ChangeHandler{	    
	ToolForm form;
 	CsvServiceAsync csvservice;
 	
 	/**
 	 * For retiring asset 
 	 * Date : 201-10-2016 By Anil
 	 * Release : 30 Sept 2016 
 	 * Project : Purchase Modification(NBHC)
 	 */
 	ServiceDateBoxPopup retireAssetPopup=new ServiceDateBoxPopup("Retire Date", "Remark");
 	AssetMovementPopup assetMovementPopup=new AssetMovementPopup();
 	PopupPanel panel;
 	/**
 	 * End
 	 */
 	

    public ToolPresenter(FormScreen<CompanyAsset> view, CompanyAsset model)
    {
        super(view, model);
        csvservice = (CsvServiceAsync)GWT.create(CsvService.class);
        form = (ToolForm)view;
        form.setPresenter(this);
        
        form.assetName.addChangeHandler(this);
        
        retireAssetPopup.getBtnOne().addClickHandler(this);
        retireAssetPopup.getBtnTwo().addClickHandler(this);
        assetMovementPopup.getBtnOk().addClickHandler(this);
        assetMovementPopup.getBtnCancel().addClickHandler(this);
        
        boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.COMPANYASSET,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
    }
    public void reactToProcessBarEvents(ClickEvent e){
    	InlineLabel lbl = (InlineLabel)e.getSource();
        if(lbl.getText().contains("New")){
            form.setToNewState();
            initalize();
        }
        if(lbl.getText().contains(AppConstants.ASSIGNASSET)){
        	assetMovementPopup.clear();
        	panel=new PopupPanel(true);
        	panel.add(assetMovementPopup);
        	panel.show();
        	panel.center();
        }
        if(lbl.getText().contains(AppConstants.RETIREASSET)){
        	retireAssetPopup.clear();
        	Date date=new Date();
        	retireAssetPopup.getServiceDate().setValue(date);
        	panel=new PopupPanel(true);
        	panel.add(retireAssetPopup);
        	panel.show();
        	panel.center();
        	
        }
        
        if(lbl.getText().contains("Copy Asset"))
        {
        	copyAseetFromPreviousData();
        }
    }

    public static ToolForm initalize()
    {
        ToolForm form = new ToolForm();
        ToolPresenterTable gentableSearch = new ToolPresenterTableProxyForSearch();
        gentableSearch.setView(form);
        gentableSearch.applySelectionModle();
        ToolPresenterSearch.staticSuperTable = gentableSearch;
        ToolPresenterSearch searchpopup = new ToolPresenterSearchProxy();
        form.setSearchpopupscreen(searchpopup);
        ToolPresenter presenter = new ToolPresenter(form, new CompanyAsset());
        AppMemory.getAppMemory().stickPnel(form);
		return form;
    }

   public void reactOnDownload(){
    	ArrayList<CompanyAsset> custarray=new ArrayList<CompanyAsset>();
		List<CompanyAsset> list=(List<CompanyAsset>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
		custarray.addAll(list);
		csvservice.setToollist(custarray, new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);
			}
			@Override
			public void onSuccess(Void result) {
				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "csvservlet"+"?type="+23;
				Window.open(url, "test", "enabled");
			}
		});
    }

	public void reactOnPrint(){
	}

    public void reactOnEmail()
    {
    }

    protected void makeNewModel()
    {
        model = new CompanyAsset();
    }

    public MyQuerry getConfigQuery()
    {
        MyQuerry quer = new MyQuerry(new Vector<Filter>(), new CompanyAsset());
        return quer;
    }

    public void setModel(CompanyAsset entity)
    {
        model = entity;
    }
	    
    @Override
	public void onChange(ChangeEvent event) {
		
	}
    
    
    
    
    
	    
    @Override
	public void onClick(ClickEvent event) {
		super.onClick(event);
		
		if(event.getSource().equals(retireAssetPopup.getBtnOne())){
			
			if(retireAssetPopup.getServiceDate().getValue()==null){
				form.showDialogMessage("Please select retire date!");
				return;
			}
			
			model.setRetireDate(retireAssetPopup.getServiceDate().getValue());
			String remark="";
			
			if(model.getDescription()!=null&&!model.getDescription().equals("")){
				remark=remark+model.getDescription()+"\n";
			}
			if(retireAssetPopup.getTaDesc().getValue()!=null){
				remark=remark+retireAssetPopup.getTaDesc().getValue();
			}
			model.setDescription(remark);
			model.setStatus(false);
			form.showWaitSymbol();
			service.save(model, new AsyncCallback<ReturnFromServer>() {
				@Override
				public void onFailure(Throwable caught) {
					form.hideWaitSymbol();
				}
				@Override
				public void onSuccess(ReturnFromServer result) {
					form.hideWaitSymbol();
					form.cbstatus.setValue(model.getStatus());
					form.dbRetireDate.setValue(model.getRetireDate());
					form.taDesription.setValue(model.getDescription());
					form.showDialogMessage("Asset Retired.");
					form.setMenuAsPerStatus();
				}
			});
			panel.hide();
		}
		if(event.getSource().equals(retireAssetPopup.getBtnTwo())){
			panel.hide();
		}
		
		if(event.getSource().equals(assetMovementPopup.getBtnOk())){
			if(assetMovementPopup.validate()){
				AssetMovementInfo info=getLastAssetMovementDetails();
				if(info!=null){
					System.out.println("INFO NOT NULL");
					if(assetMovementPopup.getDbFromDate().getValue().after(info.getFromDate())){
						int lastSrNo=info.getCount();
						lastSrNo++;
						info.setToDate(assetMovementPopup.getDbFromDate().getValue());
						AssetMovementInfo obj=new AssetMovementInfo();
						obj.setCount(lastSrNo);
						obj.setFromDate(assetMovementPopup.getDbFromDate().getValue());
						if(assetMovementPopup.getOlbWarehouse().getSelectedIndex()!=0){
							obj.setWarehouseName(assetMovementPopup.getOlbWarehouse().getValue());
						}
						obj.setLocation(assetMovementPopup.getTaLocation().getValue());
						obj.setCreatedBy(LoginPresenter.loggedInUser);
						model.getAssetMovementList().add(obj);
						
					}else{
						form.showDialogMessage("Form date should be greater than last movement date.");
						return;
					}
				}else{
					System.out.println("INFO  NULL");
					AssetMovementInfo obj=new AssetMovementInfo();
					obj.setCount(1);
					obj.setFromDate(assetMovementPopup.getDbFromDate().getValue());
					if(assetMovementPopup.getOlbWarehouse().getSelectedIndex()!=0){
						obj.setWarehouseName(assetMovementPopup.getOlbWarehouse().getValue());
					}
					obj.setLocation(assetMovementPopup.getTaLocation().getValue());
					obj.setCreatedBy(LoginPresenter.loggedInUser);
					model.getAssetMovementList().add(obj);
				}
				System.out.println("LIST SIZE "+model.getAssetMovementList().size());
				form.showWaitSymbol();
				service.save(model, new AsyncCallback<ReturnFromServer>() {
					
					@Override
					public void onSuccess(ReturnFromServer result) {
						form.hideWaitSymbol();
						form.assetMovTbl.getDataprovider().setList(model.getAssetMovementList());
						form.assetMovTbl.getTable().redraw();
						form.showDialogMessage("Asset movement completed.");
					}
					
					@Override
					public void onFailure(Throwable caught) {
						form.hideWaitSymbol();
					}
				});
				
				
				panel.hide();
			}
			
		}
		if(event.getSource().equals(assetMovementPopup.getBtnCancel())){
			panel.hide();
		}
	}





    /**
     * This method returns the last movement info from movement list
     * Date : 20-10-2016 By Anil
     * Release : 30 SEPT 2016 
     * Project: PURCHASE MODIFICATION(NBHC)
     */
	private AssetMovementInfo getLastAssetMovementDetails() {
		AssetMovementInfo info=new AssetMovementInfo();
		if(model.getAssetMovementList().size()!=0){
			int size=model.getAssetMovementList().size();
			info=model.getAssetMovementList().get(size-1);
			return info;
		}
		
		return null;
	}






	public static class ToolPresenterSearch extends SearchPopUpScreen<CompanyAsset>
    {

        public MyQuerry getQuerry()
        {
            return null;
        }

        public ToolPresenterSearch()
        {
        }

		@Override
		public boolean validate() {
			// TODO Auto-generated method stub
			return true;
		}
    }

    public static class ToolPresenterTable extends SuperTable<CompanyAsset>
        implements GeneratedVariableRefrence
    {

        public Object getVarRef(String varName)
        {
            return null;
        }

        public void createTable()
        {
        }

        protected void initializekeyprovider()
        {
        }

        public void addFieldUpdater()
        {
        }

        public void setEnable(boolean flag)
        {
        }

        public void applyStyle()
        {
        }

        public ToolPresenterTable()
        {
        }
    }
	    
    
    /**
    * Develpoed by : Rohan Bhagde
    * Reason : this is required by NBHC for copy asset and make it as new to reduce the work 
    * date : 19/10/2016
    */
   private void copyAseetFromPreviousData() {
   	
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Company Asset",Screen.COMPANYASSET);
		final ToolForm form=ToolPresenter.initalize();
		form.showWaitSymbol();
		Timer t = new Timer() {
		      @Override
		      public void run() {
		
		    	  form.setToNewState();
		    	  

		    	  CompanyAsset view = model;
		    		
		    	    if(view.getName() != null)
		    	        form.assetName.setValue(view.getName());
		    	    if(view.getBrand() != null)
		    	    	form.tbBrand.setValue(view.getBrand());
		    	    if(view.getModelNo() != null)
		    	    	form.tbModelNo.setValue(view.getModelNo());
		    	    if(view.getSrNo() != null)
		    	    	form.tbSrNo.setValue(view.getSrNo());
		    	    if(view.getPurchasedFrom() != null)
		    	    	form.tbPurchsedFrom.setValue(view.getPurchasedFrom());
		    	    if(view.getDateOfManufacture() != null)
		    	    	form.dbDateofManufacture.setValue(view.getDateOfManufacture());
		    	    if(view.getDateOfInstallation() != null)
		    	    	form.dbdateofInstallation.setValue(view.getDateOfInstallation());
		    	    if(view.getCategory() != null)
		    	    	form.olbAssetCategory.setValue(view.getCategory());
		    	    if(view.getPODate() != null)
		    	    	form.dbDateOfPurchase.setValue(view.getPODate());
		    	    if(view.getPoNo() != null)
		    	    	form.ibPONo.setValue(view.getPoNo());
		    	    form.cbWarenty.setValue(view.isWarrenty());
		    	    
		    	    form.cbstatus.setValue(view.getStatus());
		    	    
		    	    
		    	    if(view.getBranch()!=null)
		    	    	form.olbbBranch.setValue(view.getBranch());
		    	  form.hideWaitSymbol();
		      }
		};
		t.schedule(2000);
	}
    
	    
}
