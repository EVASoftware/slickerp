// Decompiled by DJ v3.12.12.98 Copyright 2014 Atanas Neshkov  Date: 16-Sep-14 11:42:59 AM
// Home Page:  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   ToolPresenterTableProxy.java

package com.slicktechnologies.client.views.project.tool;

import com.google.gwt.user.cellview.client.*;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.ProvidesKey;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.servicerelated.CompanyAsset;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;

import java.util.Comparator;
import java.util.List;

// Referenced classes of package com.slicktechnologies.client.views.project.tool:
//            ToolPresenter

public class ToolPresenterTableProxy extends ToolPresenter.ToolPresenterTable
{
	TextColumn<CompanyAsset> getAssetidColumn;
    TextColumn<CompanyAsset> getBrandColumn;
    TextColumn<CompanyAsset> getCategoryColumn;
    TextColumn<CompanyAsset> getNameColumn;
    TextColumn<CompanyAsset> getSrNoColumn;
    TextColumn<CompanyAsset> getModelNoColumn;
    TextColumn<CompanyAsset> getPurchaseDateColumn;
    TextColumn<CompanyAsset> getWarrantyUntilDateColumn;
    TextColumn<CompanyAsset> getPoNoColumn;
    TextColumn<CompanyAsset> getDateOfManufacturingColumn;
    TextColumn<CompanyAsset> getDateOfInstallationColumn;

    public Object getVarRef(String varName)
    {
        if(varName.equals("getAssetidColumn"))
            return getAssetidColumn;
        if(varName.equals("getBrandColumn"))
            return getBrandColumn;
        if(varName.equals("getCategoryColumn"))
            return getCategoryColumn;
        if(varName.equals("getNameColumn"))
            return getNameColumn;
        if(varName.equals("getSrNoColumn"))
        	return getSrNoColumn;
        if(varName.equals("getModelNoColumn"))
        	return getModelNoColumn;
        if(varName.equals("getPurchaseDateColumn"))
        	return getPurchaseDateColumn;
        if(varName.equals("getWarrantyUntilDateColumn"))
        	return getWarrantyUntilDateColumn;
        if(varName.equals("getPoNoColumn"))
        	return getPoNoColumn;
        if(varName.equals("getDateOfManufacturingColumn"))
        	return getDateOfManufacturingColumn;
        if(varName.equals("getDateOfInstallationColumn"))
        	return getDateOfInstallationColumn;
        else
            return null;
    }

    public ToolPresenterTableProxy()
    {
    }

    public void createTable()
    {
        addColumngetAssetid();
        addColumngetName();
        addColumngetCategory();
        addColumngetBrand();
        //addColumngetSrNo();
        //addColumngetModelNo();
        //addColumngetPurchaseDate();
        //addColumngetWarrantyUntil();
        //addColumngetPoNo();
        //addColumngetManufacturingDate();
        //addColumngetInstallationDate();
    }

    protected void initializekeyprovider()
    {
        keyProvider = new ProvidesKey<CompanyAsset>() {

            public Object getKey(CompanyAsset item)
            {
                if(item == null)
                    return null;
                else
                    return item.getId();
            }

           
        }
;
    }

    public void setEnable(boolean flag)
    {
    }

    public void applyStyle()
    {
    }

    public void addColumnSorting()
    {
        addSortinggetAssetid();
        addSortinggetName();
        addSortinggetCategory();
        addSortinggetBrand();
       // addSortinggetSrNo();
        //addSortinggetModelNo();
        //addSortinggetPoNo();
    }

    public void addFieldUpdater()
    {
    }

    
	protected void addSortinggetAssetid()
    {
        List<CompanyAsset> list = getDataprovider().getList();
        columnSort = new ListHandler<CompanyAsset>(list);
        columnSort.setComparator(getAssetidColumn, new Comparator<CompanyAsset>() {

            public int compare(CompanyAsset e1, CompanyAsset e2)
            {
                if(e1 != null && e2 != null)
                {
                    if(e1.getCount() == e2.getCount())
                        return 0;
                    return e1.getCount() <= e2.getCount() ? -1 : 1;
                } else
                {
                    return 0;
                }
            }

        }
);
        table.addColumnSortHandler(columnSort);
    }

    protected void addColumngetAssetid()
    {
        getAssetidColumn = new TextColumn<CompanyAsset>() {

            public String getValue(CompanyAsset object)
            {
                return (new StringBuilder(String.valueOf(object.getCount()))).toString();
            }

        }
;
        table.addColumn(getAssetidColumn, "Asset Id");
        getAssetidColumn.setSortable(true);
    }

    
	protected void addSortinggetName()
    {
        List<CompanyAsset> list = getDataprovider().getList();
        columnSort = new ListHandler<CompanyAsset>(list);
        columnSort.setComparator(getNameColumn, new Comparator<CompanyAsset>() {

            public int compare(CompanyAsset e1, CompanyAsset e2)
            {
                if(e1 != null && e2 != null)
                {
                    if(e1.getName() != null && e2.getName() != null)
                        return e1.getName().compareTo(e2.getName());
                    else
                        return 0;
                } else
                {
                    return 0;
                }
            }

        }
);
        table.addColumnSortHandler(columnSort);
    }

    protected void addColumngetName()
    {
        getNameColumn = new TextColumn<CompanyAsset>() {

            public String getValue(CompanyAsset object)
            {
                return (new StringBuilder(String.valueOf(object.getName()))).toString();
            }

        }
;
        table.addColumn(getNameColumn, "Asset Name");
        getNameColumn.setSortable(true);
    }

    
    protected void addColumngetSrNo()
    {
        getSrNoColumn = new TextColumn<CompanyAsset>() {

            public String getValue(CompanyAsset object)
            {
                return (new StringBuilder(String.valueOf(object.getSrNo()))).toString();
            }

        }
;
        table.addColumn(getSrNoColumn, "Sr No");
        getSrNoColumn.setSortable(true);
    }
    
    protected void addColumngetModelNo()
    {
        getModelNoColumn = new TextColumn<CompanyAsset>() {

            public String getValue(CompanyAsset object)
            {
                return (new StringBuilder(String.valueOf(object.getModelNo()))).toString();
            }

        }
;
        table.addColumn(getModelNoColumn, "Model No");
        getModelNoColumn.setSortable(true);
    }

    protected void addSortinggetModelNo()
    {
        List<CompanyAsset> list = getDataprovider().getList();
        columnSort = new ListHandler<CompanyAsset>(list);
        columnSort.setComparator(getModelNoColumn, new Comparator<CompanyAsset>() {

            public int compare(CompanyAsset e1, CompanyAsset e2)
            {
                if(e1 != null && e2 != null)
                {
                    if(e1.getName() != null && e2.getName() != null)
                        return e1.getModelNo().compareTo(e2.getModelNo());
                    else
                        return 0;
                } else
                {
                    return 0;
                }
            }

        }
);
        table.addColumnSortHandler(columnSort);
    }
    
    protected void addColumngetPoNo()
    {
        getPoNoColumn = new TextColumn<CompanyAsset>() {

            public String getValue(CompanyAsset object)
            {
                return (new StringBuilder(String.valueOf(object.getPoNo()))).toString();
            }

        }
;
        table.addColumn(getPoNoColumn, "Po No");
        getPoNoColumn.setSortable(true);
    }

    
    protected void addSortinggetPoNo()
    {
        List<CompanyAsset> list = getDataprovider().getList();
        columnSort = new ListHandler<CompanyAsset>(list);
        columnSort.setComparator(getPoNoColumn, new Comparator<CompanyAsset>() {

            public int compare(CompanyAsset e1, CompanyAsset e2)
            {
                if(e1 != null && e2 != null)
                {
                    if(e1.getName() != null && e2.getName() != null)
                        return e1.getPoNo().compareTo(e2.getPoNo());
                    else
                        return 0;
                } else
                {
                    return 0;
                }
            }

        }
);
        table.addColumnSortHandler(columnSort);
    }
    protected void addColumngetPurchaseDate()
    {
        getPurchaseDateColumn = new TextColumn<CompanyAsset>() {

            public String getValue(CompanyAsset object)
            {
                return AppUtility.parseDate(object.getPODate());
            }

        }
;
        table.addColumn(getPurchaseDateColumn, "Purchase Date");
        getPurchaseDateColumn.setSortable(true);
    }

    
    protected void addColumngetWarrantyUntil()
    {
        getWarrantyUntilDateColumn = new TextColumn<CompanyAsset>() {

            public String getValue(CompanyAsset object)
            {
                return AppUtility.parseDate(object.getWarrenty());
            }

        }
;
        table.addColumn(getWarrantyUntilDateColumn, "Warranty Until Date");
        getWarrantyUntilDateColumn.setSortable(true);
    }

    
    protected void addColumngetManufacturingDate()
    {
        getDateOfManufacturingColumn = new TextColumn<CompanyAsset>() {

            public String getValue(CompanyAsset object)
            {
                return AppUtility.parseDate(object.getDateOfManufacture());
            }

        }
;
        table.addColumn(getDateOfManufacturingColumn, "Manufacturing Date");
        getDateOfManufacturingColumn.setSortable(true);
    }

    
    protected void addColumngetInstallationDate()
    {
        getDateOfInstallationColumn = new TextColumn<CompanyAsset>() {

            public String getValue(CompanyAsset object)
            {
                return AppUtility.parseDate(object.getDateOfInstallation());
            }

        }
;
        table.addColumn(getDateOfInstallationColumn, "Installation Date");
        getDateOfInstallationColumn.setSortable(true);
    }

    
	protected void addSortinggetCategory()
    {
        List<CompanyAsset> list = getDataprovider().getList();
        columnSort = new ListHandler<CompanyAsset>(list);
        columnSort.setComparator(getCategoryColumn, new Comparator<CompanyAsset>() {

            public int compare(CompanyAsset e1, CompanyAsset e2)
            {
                if(e1 != null && e2 != null)
                {
                    if(e1.getCategory() != null && e2.getCategory() != null)
                        return e1.getCategory().compareTo(e2.getCategory());
                    else
                        return 0;
                } else
                {
                    return 0;
                }
            }

           
        }
);
        table.addColumnSortHandler(columnSort);
    }

    protected void addColumngetCategory()
    {
        getCategoryColumn = new TextColumn<CompanyAsset>() {

            public String getValue(CompanyAsset object)
            {
                return (new StringBuilder(String.valueOf(object.getCategory()))).toString();
            }

        }
;
        table.addColumn(getCategoryColumn, "Category");
        getCategoryColumn.setSortable(true);
    }

    
	protected void addSortinggetBrand()
    {
        List<CompanyAsset> list = getDataprovider().getList();
        columnSort = new ListHandler<CompanyAsset>(list);
        columnSort.setComparator(getBrandColumn, new Comparator<CompanyAsset>() {

            public int compare(CompanyAsset e1, CompanyAsset e2)
            {
                if(e1 != null && e2 != null)
                {
                    if(e1.getBrand() != null && e2.getBrand() != null)
                        return e1.getBrand().compareTo(e2.getBrand());
                    else
                        return 0;
                } else
                {
                    return 0;
                }
            }

        }
);
        table.addColumnSortHandler(columnSort);
    }
	
	
	protected void addSortinggetSrNo()
    {
        List<CompanyAsset> list = getDataprovider().getList();
        columnSort = new ListHandler<CompanyAsset>(list);
        columnSort.setComparator(getSrNoColumn, new Comparator<CompanyAsset>() {

            public int compare(CompanyAsset e1, CompanyAsset e2)
            {
                if(e1 != null && e2 != null)
                {
                    if(e1.getBrand() != null && e2.getBrand() != null)
                        return e1.getSrNo().compareTo(e2.getSrNo());
                    else
                        return 0;
                } else
                {
                    return 0;
                }
            }

        }
);
        table.addColumnSortHandler(columnSort);
    }

    protected void addColumngetBrand()
    {
        getBrandColumn = new TextColumn<CompanyAsset>() {

            public String getValue(CompanyAsset object)
            {
                return (new StringBuilder(String.valueOf(object.getBrand()))).toString();
            }

        }
;
        table.addColumn(getBrandColumn, "Brand");
        getBrandColumn.setSortable(true);
    }

    
}
