// Decompiled by DJ v3.12.12.98 Copyright 2014 Atanas Neshkov  Date: 16-Sep-14 11:42:22 AM
// Home Page:  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   ToolPresenterSearchProxy.java

package com.slicktechnologies.client.views.project.tool;

import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.servicerelated.CompanyAsset;

import java.util.Vector;

// Referenced classes of package com.slicktechnologies.client.views.project.tool:
//            ToolPresenter

public class ToolPresenterSearchProxy extends ToolPresenter.ToolPresenterSearch
{

    public Object getVarRef(String varName)
    {
        if(varName.equals("ibAssetid"))
            return ibAssetid;
        if(varName.equals("tbBrand"))
            return tbBrand;
        if(varName.equals("olbAssetCategory"))
            return olbAssetCategory;
        if(varName.equals("assetName"))
            return assetName;
        if(varName.equals("olbbBranch"))
            return olbBranch;
        
        
        else
            return null;
    }

    public ToolPresenterSearchProxy()
    {
        createGui();
    }

    public void initWidget()
    {
        ibAssetid = new IntegerBox();
        tbBrand = new TextBox();
        olbAssetCategory = new ObjectListBox<Config>();
        AppUtility.MakeLiveConfig(olbAssetCategory, Screen.ASSETCATEGORY);
        assetName = new TextBox();
        
        olbBranch=new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbBranch);
        
    }

    public void createScreen()
    {
        initWidget();
        FormFieldBuilder builder = new FormFieldBuilder("Asset Id", ibAssetid);
        FormField fibAssetid = builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
        builder = new FormFieldBuilder("Brand", tbBrand);
        FormField ftbBrand = builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
        builder = new FormFieldBuilder("Category", olbAssetCategory);
        FormField folbAssetCategory = builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
        builder = new FormFieldBuilder("Asset Name", assetName);
        FormField fassetName = builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
    
        builder = new FormFieldBuilder("Branch",olbBranch);
		FormField folbBranch= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
        
        fields = (new FormField[][] {
            new FormField[] {
                fibAssetid, fassetName
            }, new FormField[] {
                folbAssetCategory,folbBranch ,ftbBrand}
        });
    }

    public MyQuerry getQuerry()
    {
        Vector<Filter> filtervec = new Vector<Filter>();
        Filter temp = null;
        if(ibAssetid.getValue() != null)
        {
            temp = new Filter();
            temp.setIntValue((Integer)ibAssetid.getValue());
            filtervec.add(temp);
            temp.setQuerryString("count");
            filtervec.add(temp);
        }
        if(!assetName.getValue().trim().equals(""))
        {
            temp = new Filter();
            temp.setStringValue(assetName.getValue().trim());
            filtervec.add(temp);
            temp.setQuerryString("name");
            filtervec.add(temp);
        }
        if(olbAssetCategory.getSelectedIndex() != 0)
        {
            temp = new Filter();
            temp.setStringValue(olbAssetCategory.getValue().trim());
            filtervec.add(temp);
            temp.setQuerryString("category");
            filtervec.add(temp);
        }
        if(!tbBrand.getValue().trim().equals(""))
        {
            temp = new Filter();
            temp.setStringValue(tbBrand.getValue().trim());
            filtervec.add(temp);
            temp.setQuerryString("brand");
            filtervec.add(temp);
        }
     
        if(olbBranch.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(olbBranch.getValue().trim());
			temp.setQuerryString("branch");
			filtervec.add(temp);
		}
        
        
        MyQuerry querry = new MyQuerry();
        querry.setFilters(filtervec);
        querry.setQuerryObject(new CompanyAsset());
        return querry;
    }

    public IntegerBox ibAssetid;
    public TextBox tbBrand;
    public ObjectListBox olbAssetCategory;
    public TextBox assetName;
    public ObjectListBox olbBranch;
	
    
    @Override
	public boolean validate() {
		if(LoginPresenter.branchRestrictionFlag)
		{
		if(olbBranch.getSelectedIndex()==0)
		{
			showDialogMessage("Select Branch");
			return false;
		}
		}
		
		return super.validate();
	}
    
    
}
