package com.slicktechnologies.client.views.project.tool;

import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.servicerelated.AssetMovementInfo;

public class AssetMovementTable extends SuperTable<AssetMovementInfo> {
	TextColumn<AssetMovementInfo> getSrNoCol;
	TextColumn<AssetMovementInfo> getWarehouseNameCol;
	TextColumn<AssetMovementInfo> getLocationCol;
	TextColumn<AssetMovementInfo> getFromDateCol;
	TextColumn<AssetMovementInfo> getToDateCol;
	TextColumn<AssetMovementInfo> getPersonResponsibleCol;
	
	public AssetMovementTable() {
		super();
	}

	@Override
	public void createTable() {
		getSrNoCol();
		getWarehouseNameCol();
		getLocationCol();
		getFromDateCol();
		getToDateCol();
		getPersonResponsibleCol();
	}

	private void getSrNoCol() {
		getSrNoCol=new TextColumn<AssetMovementInfo>() {
			@Override
			public String getValue(AssetMovementInfo object) {
				return object.getCount()+"";
			}
		};
		table.addColumn(getSrNoCol,"Sr. No.");
	}

	private void getWarehouseNameCol() {
		getWarehouseNameCol=new TextColumn<AssetMovementInfo>() {
			@Override
			public String getValue(AssetMovementInfo object) {
				return object.getWarehouseName()+"";
			}
		};
		table.addColumn(getWarehouseNameCol,"Warehouse");
	}

	private void getLocationCol() {
		getLocationCol=new TextColumn<AssetMovementInfo>() {
			@Override
			public String getValue(AssetMovementInfo object) {
				return object.getLocation();
			}
		};
		table.addColumn(getLocationCol,"Location");
	}

	private void getFromDateCol() {
		getFromDateCol=new TextColumn<AssetMovementInfo>() {
			@Override
			public String getValue(AssetMovementInfo object) {
				if(object.getFromDate()!=null){
					return AppUtility.parseDate(object.getFromDate());
				}
				return "";
			}
		};
		table.addColumn(getFromDateCol,"From Date");
		
	}

	private void getToDateCol() {
		getToDateCol=new TextColumn<AssetMovementInfo>() {
			@Override
			public String getValue(AssetMovementInfo object) {
				if(object.getToDate()!=null){
					return AppUtility.parseDate(object.getToDate());
				}
				return "";
			}
		};
		table.addColumn(getToDateCol,"To Date");
		
	}

	private void getPersonResponsibleCol() {
		getPersonResponsibleCol=new TextColumn<AssetMovementInfo>() {
			@Override
			public String getValue(AssetMovementInfo object) {
				return object.getCreatedBy()+"";
			}
		};
		table.addColumn(getPersonResponsibleCol,"Person Responsible");
	}

	@Override
	protected void initializekeyprovider() {
		
	}

	@Override
	public void addFieldUpdater() {
		
	}

	@Override
	public void setEnable(boolean state) {
		
	}

	@Override
	public void applyStyle() {
		
	}

}
