package com.slicktechnologies.client.views.project.tool;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.view.client.ProvidesKey;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.articletype.ArticleTypeTravel;

public class ArticleTypeTravelTable extends SuperTable<ArticleTypeTravel>{


	TextColumn<ArticleTypeTravel> getgetArticleTypeNameColumn;
	TextColumn<ArticleTypeTravel> getArticleTypeValueColumn;
	TextColumn<ArticleTypeTravel> getArticleValidityColumn;
	TextColumn<ArticleTypeTravel> getArticleIssuedAtColumn;
	TextColumn<ArticleTypeTravel> getArticleIssuedDateColumn;
	
	TextColumn<ArticleTypeTravel> getArticleIssuedByColumn;
	
	TextColumn<ArticleTypeTravel> getArticleValidityUntilDateColumn;
	
	private Column<ArticleTypeTravel, String> delete;
	
	
	public ArticleTypeTravelTable() {
		super();
	}
	
	
	@Override
	public void createTable() {
		getgetArticleTypeNameColumn();
		getArticleTypeValueColumn();
		getArticleValidityColumn();
		getArticleIssuedDateColumn();
		getArticleIssuedAtColumn();
		getArticleIssuedByColumn();
		getArticleValidityUntilColumn();
		addColumnDelete();
		setFieldUpdaterOnDelete();
	    table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);

	}
	
	private void getArticleValidityUntilColumn() {


		getArticleValidityUntilDateColumn = new TextColumn<ArticleTypeTravel>() {
			@Override
			public String getValue(ArticleTypeTravel object) {
				return AppUtility.parseDate(object.getValidityuntil());
			}
		};
		table.addColumn(getArticleValidityUntilDateColumn,"Validity Until");
	
		
	}
	
	public void getgetArticleTypeNameColumn()
	{
		getgetArticleTypeNameColumn = new TextColumn<ArticleTypeTravel>() {
			@Override
			public String getValue(ArticleTypeTravel object) {
				return object.getArticleTypeName();
			}
		};
		table.addColumn(getgetArticleTypeNameColumn,"Article Type");
	}
	
	public void getArticleTypeValueColumn()
	{
		getArticleTypeValueColumn = new TextColumn<ArticleTypeTravel>() {
			@Override
			public String getValue(ArticleTypeTravel object) {
				return object.getArticleTypeValue();
			}
		};
		table.addColumn(getArticleTypeValueColumn,"Article Number");
	}
	
	public void getArticleValidityColumn()
	{
		getArticleValidityColumn = new TextColumn<ArticleTypeTravel>() {
			@Override
			public String getValue(ArticleTypeTravel object) {
				return object.getValidity()+"";
			}
		};
		table.addColumn( getArticleValidityColumn,"Validity(In Days)");
	}
	
	public void getArticleIssuedAtColumn()
	{
		getArticleIssuedAtColumn = new TextColumn<ArticleTypeTravel>() {
			@Override
			public String getValue(ArticleTypeTravel object) {
				return object.getIssuedAt();
			}
		};
		table.addColumn(getArticleIssuedAtColumn,"Issued At");
	}
	
	
	
	public void getArticleIssuedDateColumn()
	{
		getArticleIssuedDateColumn = new TextColumn<ArticleTypeTravel>() {
			@Override
			public String getValue(ArticleTypeTravel object) {
				return AppUtility.parseDate(object.getIssueDate());
			}
		};
		table.addColumn(getArticleIssuedDateColumn,"Issued Date");
	}
	
	public void getArticleIssuedByColumn()
	{
		getArticleIssuedByColumn = new TextColumn<ArticleTypeTravel>() {
			@Override
			public String getValue(ArticleTypeTravel object) {
				return object.getIssuedBy();
			}
		};
		table.addColumn(getArticleIssuedByColumn,"Issued By");
	}

	
	
	private void addColumnDelete()
	{
		ButtonCell btnCell= new ButtonCell();
		delete = new Column<ArticleTypeTravel, String>(btnCell) {

			@Override
			public String getValue(ArticleTypeTravel object) {
				
				return "Delete";
			}
		};
		table.addColumn(delete,"Delete");
	}
	
	private void setFieldUpdaterOnDelete()
	{
		delete.setFieldUpdater(new FieldUpdater<ArticleTypeTravel, String>() {
			
			@Override
			public void update(int index, ArticleTypeTravel object, String value) {
				getDataprovider().getList().remove(index);
				table.redrawRow(index);
			}
		});
	}
	
	
	private void addeditColumn()
	{
		getgetArticleTypeNameColumn();
		getArticleTypeValueColumn();
		getArticleValidityColumn();
		getArticleIssuedAtColumn();
		getArticleIssuedDateColumn();
		getArticleIssuedByColumn();
		getArticleValidityUntilColumn();
		addColumnDelete();
		setFieldUpdaterOnDelete();
	}
	
	private void addViewColumn()
	{
		getgetArticleTypeNameColumn();
		getArticleTypeValueColumn();
		getArticleValidityColumn();
		getArticleIssuedAtColumn();
		getArticleIssuedDateColumn();
		getArticleIssuedByColumn();
		getArticleValidityUntilColumn();
	}
	
	public  void setEnabled(boolean state)
	{
		int tablecolcount=this.table.getColumnCount();
		for(int i=tablecolcount-1;i>-1;i--)
		  table.removeColumn(i);
		if(state ==true)
			addeditColumn();
		if(state==false)
			addViewColumn();
	}
		

	
	@Override
	protected void initializekeyprovider() {
		keyProvider = new ProvidesKey<ArticleTypeTravel>() {
			@Override
			public Object getKey(ArticleTypeTravel item) {
				if(item==null)
					return null;
				else
					return item.getId();
			}
		};
		
	}

	@Override
	public void addFieldUpdater() {
		
	}

	@Override
	public void setEnable(boolean state) {
		int tablecolcount=this.table.getColumnCount();
		for(int i=tablecolcount-1;i>-1;i--)
		  table.removeColumn(i);
		if(state ==true)
			addeditColumn();
		if(state==false)
			addViewColumn();
		
	}

	@Override
	public void applyStyle() {
		
	}
}
