package com.slicktechnologies.client.views.project.tool;

import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.dom.client.Style.FontStyle;
import com.google.gwt.dom.client.Style.FontWeight;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.shared.common.inventory.WareHouse;

public class AssetMovementPopup extends AbsolutePanel{
	ObjectListBox<WareHouse> olbWarehouse;
	TextArea taLocation;
	DateBox dbFromDate;
	Button btnOk,btnCancel;
	
	private void initializeWidget(){
		olbWarehouse = new ObjectListBox<WareHouse>();
		Vector<Filter> temp=new  Vector<Filter>();
		Filter filter=new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		temp.add(filter);
		MyQuerry querry = new MyQuerry(temp, new WareHouse());
		olbWarehouse.MakeLive(querry);
		
		taLocation=new TextArea();
		taLocation.getElement().getStyle().setWidth(300, Unit.PX);
		dbFromDate=new DateBoxWithYearSelector();
		DateTimeFormat dateFormat = DateTimeFormat.getFormat("dd/MM/yyyy"); 
		dbFromDate.setFormat(new DateBox.DefaultFormat(dateFormat));
		
		btnOk=new Button("Ok");
		btnOk.getElement().getStyle().setWidth(80, Unit.PX);
		btnCancel=new Button("Cancel");
		btnCancel.getElement().getStyle().setWidth(80, Unit.PX);
		
	}
	
	public AssetMovementPopup() {
		initializeWidget();
		
		InlineLabel label=new InlineLabel("Asset Movement");
		label.getElement().getStyle().setFontSize(15, Unit.PX);
		label.getElement().getStyle().setFontStyle(FontStyle.NORMAL);
		label.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		
		InlineLabel fromDateLbl=new InlineLabel("From Date");
		InlineLabel warehouseLbl=new InlineLabel("Warehouse");
		InlineLabel locationLbl=new InlineLabel("Location");
		
		add(label,150,10);
		
		add(fromDateLbl,10,40);
		add(dbFromDate,10,65);
		
		add(warehouseLbl,10,100);
		add(olbWarehouse,10,125);
		
		add(locationLbl,10,160);
		add(taLocation,10,185);
		
		add(btnOk,100,250);
		add(btnCancel,220,250);
		
		setSize("400px","300px");
		this.getElement().setId("login-form");
	}
	
	public void clear(){
		olbWarehouse.setSelectedIndex(0);
		taLocation.setValue("");
		dbFromDate.setValue(null);
	}
	
	public boolean validate(){
		if(dbFromDate.getValue()==null){
			GWTCAlert alert=new GWTCAlert();
			alert.alert("Please select from date !");
			return false;
		}
		if(olbWarehouse.getSelectedIndex()==0&&taLocation.getValue().equals("")){
			GWTCAlert alert=new GWTCAlert();
			alert.alert("Please select warehouse and/or enter location !");
			return false;
		}
		return true;
	}
	
	
	
	
	////////////////////////////////////////// GETTER SETTER //////////////////////////////////////////////

	public ObjectListBox<WareHouse> getOlbWarehouse() {
		return olbWarehouse;
	}

	public void setOlbWarehouse(ObjectListBox<WareHouse> olbWarehouse) {
		this.olbWarehouse = olbWarehouse;
	}

	public TextArea getTaLocation() {
		return taLocation;
	}

	public void setTaLocation(TextArea taLocation) {
		this.taLocation = taLocation;
	}

	public DateBox getDbFromDate() {
		return dbFromDate;
	}

	public void setDbFromDate(DateBox dbFromDate) {
		this.dbFromDate = dbFromDate;
	}

	public Button getBtnOk() {
		return btnOk;
	}

	public void setBtnOk(Button btnOk) {
		this.btnOk = btnOk;
	}

	public Button getBtnCancel() {
		return btnCancel;
	}

	public void setBtnCancel(Button btnCancel) {
		this.btnCancel = btnCancel;
	}
	
	
	
	
	
	
	
	

}
