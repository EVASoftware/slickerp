package com.slicktechnologies.client.views.project.tool;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.dom.client.Style.FontStyle;
import com.google.gwt.dom.client.Style.FontWeight;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.composite.UploadComposite;

public class UploadAssetPopup extends AbsolutePanel {
	Button downloadFormat;
	UploadComposite upload;
	Button btnOk,btnCancel;
	
	SuperModel model;
	
	private void initializeWidget(){
		downloadFormat=new Button("Download Format");
		upload=new UploadComposite();
		upload.getElement().getStyle().setHeight(25, Unit.PX);
		btnOk=new Button("Ok");
		btnOk.getElement().getStyle().setWidth(80, Unit.PX);
		btnCancel=new Button("Cancel");
		btnCancel.getElement().getStyle().setWidth(80, Unit.PX);
	}
	
	
	
	public UploadAssetPopup() {
		initializeWidget();
		
		InlineLabel label=new InlineLabel("Upload Assets");
		label.getElement().getStyle().setFontSize(15, Unit.PX);
		label.getElement().getStyle().setFontStyle(FontStyle.NORMAL);
		label.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		
		add(label,150,10);
		add(downloadFormat,10,50);
		
		add(upload,10,100);
		
		add(btnOk,100,150);
		add(btnCancel,220,150);
		
		
		setSize("400px","200px");
		this.getElement().setId("login-form");
	}
	
	public UploadAssetPopup(String uploadTnC) {
		initializeWidget();
		
		InlineLabel label=new InlineLabel(uploadTnC);
		label.getElement().getStyle().setFontSize(15, Unit.PX);
		label.getElement().getStyle().setFontStyle(FontStyle.NORMAL);
		label.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		
		add(label,150,10);
//		add(downloadFormat,10,50);
		
		add(upload,10,100);
		
		add(btnOk,170,150);
		add(btnCancel,290,150);
		
		
		setSize("550px","200px");
		this.getElement().setId("login-form");
	}
	
	public void clear(){
		upload.clear();
	}
	
	public boolean validate(){
		
		
		if(upload.getValue()==null){
			GWTCAlert alert=new GWTCAlert();
			alert.alert("Please upload excel !");
		}
		return true;
	}

	public Button getDownloadFormat() {
		return downloadFormat;
	}

	public void setDownloadFormat(Button downloadFormat) {
		this.downloadFormat = downloadFormat;
	}

	public UploadComposite getUpload() {
		return upload;
	}

	public void setUpload(UploadComposite upload) {
		this.upload = upload;
	}

	public Button getBtnOk() {
		return btnOk;
	}

	public void setBtnOk(Button btnOk) {
		this.btnOk = btnOk;
	}

	public Button getBtnCancel() {
		return btnCancel;
	}

	public void setBtnCancel(Button btnCancel) {
		this.btnCancel = btnCancel;
	}
	
	
}
