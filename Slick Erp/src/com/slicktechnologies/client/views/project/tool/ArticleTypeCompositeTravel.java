package com.slicktechnologies.client.views.project.tool;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.composite.CompositeInterface;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.articletype.ArticleTypeTravel;
import com.slicktechnologies.shared.common.helperlayer.Config;
public class ArticleTypeCompositeTravel extends Composite implements ClickHandler, HasValue<ArrayList<ArticleTypeTravel>>,CompositeInterface{


	
	private FlowPanel content;
	
	private Button addButton;
	private ArticleTypeTravelTable articletypetable;
	private ObjectListBox<Config>olbarticletypename;
	private TextBox articletypevalue;
	
	private IntegerBox validity;
	
	private TextBox issuedAt;
	
	private TextBox issuedBy;
	private DateBox issueDate;
	
	private Object form;
	
	
	public ArticleTypeCompositeTravel() {
		content=new FlowPanel();
	    initWidget(content);
	    createTableStructure();
	    articletypetable.connectToLocal();
	    applyStyle();
	}
	
	
	private void createTableStructure()
	{
		DateTimeFormat dateFormat = DateTimeFormat.getFormat("dd/MM/yyyy");
		
        Grid ArticleGrid = new Grid(4,7);
		
    
		InlineLabel lblarticletype = new InlineLabel("Article Type");
		InlineLabel lblarticletypevalue = new InlineLabel("Article Number");
		InlineLabel lblvalidity = new InlineLabel("Validity(In Days)");
		InlineLabel lblissuedate = new InlineLabel("Issue Date");
		InlineLabel lblissueatt = new InlineLabel("Issued At");
		InlineLabel lblissueby = new InlineLabel("Issued By");
		
		
		olbarticletypename=new ObjectListBox<Config>();
//		AppUtility.MakeLiveConfig(olbarticletypename, Screen.ARTICLETYPE);
		AppUtility.MakeLiveConfig(olbarticletypename, Screen.ASSETARTICLETYPE);
		olbarticletypename.getElement().getStyle().setHeight(24, Unit.PX);
		
		articletypevalue=new TextBox();
		articletypevalue.getElement().getStyle().setHeight(13, Unit.PX);
		
		validity = new IntegerBox();
		validity.getElement().getStyle().setHeight(19, Unit.PX);
		
		issueDate=new DateBoxWithYearSelector();
		issueDate.setFormat(new DateBox.DefaultFormat(dateFormat));
		
		issuedAt=new TextBox();
		issuedAt.getElement().getStyle().setHeight(13, Unit.PX);
		
		issuedBy=new TextBox();
		issuedBy.getElement().getStyle().setHeight(13, Unit.PX);
		
		
		
		addButton= new Button("Add");
		addButton.addClickHandler(this);
		
		
		ArticleGrid.setWidget(0,0,lblarticletype);
		ArticleGrid.setWidget(1, 0,olbarticletypename);
		
		ArticleGrid.setWidget(0,1,lblarticletypevalue);
		ArticleGrid.setWidget(1, 1,articletypevalue);
		
		ArticleGrid.setWidget(0,2,lblvalidity);
		ArticleGrid.setWidget(1, 2,validity);
		
		ArticleGrid.setWidget(0,3,lblissuedate);
		ArticleGrid.setWidget(1, 3,issueDate);
		
		ArticleGrid.setWidget(0, 4, lblissueatt);
		ArticleGrid.setWidget(1, 4, issuedAt);
		
		ArticleGrid.setWidget(2, 0,lblissueby);
		ArticleGrid.setWidget(3, 0,issuedBy);
		
		ArticleGrid.setWidget(2, 1,new InlineLabel(" "));
		ArticleGrid.setWidget(3, 1,addButton);
		
		
		ArticleGrid.setCellPadding(5);
		ArticleGrid.setCellSpacing(5);
		content.add(ArticleGrid);
		
		articletypetable =new ArticleTypeTravelTable();
		content.add(articletypetable.content);
	}
	
	
	
	
	
	@Override
	public HandlerRegistration addValueChangeHandler(ValueChangeHandler<ArrayList<ArticleTypeTravel>> handler) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setEnable(boolean status) {
		validity.setEnabled(status);
		olbarticletypename.setEnabled(status);
		articletypevalue.setEnabled(status);
		issuedAt.setEnabled(status);
		issuedBy.setEnabled(status);
		issueDate.setEnabled(status);
		articletypetable.setEnabled(status);
		addButton.setEnabled(status);
		
	}

	@Override
	public void clear() {
		this.articletypetable.connectToLocal();
		this.articletypevalue.setText("");
		this.validity.setText("");
		this.issuedAt.setText("");
		this.issuedBy.setText("");
		this.issueDate.setValue(null);;
		
		this.olbarticletypename.setSelectedIndex(0);
		
	}
	
	public void applyStyle()
	{
		content.setWidth("98%");
	}
	

	@Override
	public boolean validate() {
		
		if(olbarticletypename.getSelectedIndex()!=0&&!articletypevalue.getValue().equals("")
				&&validity.getValue()!=null&&issueDate.getValue()!=null&&!issuedAt.getValue().equals("")
				&&!issuedBy.getValue().equals("")){
			
			 final GWTCAlert alert = new GWTCAlert(); 
		     alert.alert("Please add article information to table !");
		     return false;  
		     
		}else if(olbarticletypename.getSelectedIndex()!=0||!articletypevalue.getValue().equals("")
				||validity.getValue()!=null||issueDate.getValue()!=null||!issuedAt.getValue().equals("")
				||!issuedBy.getValue().equals("")){
			
			 final GWTCAlert alert = new GWTCAlert(); 
		     alert.alert("Please fill complete article information or remove entered information.");
		     return false;  
		}
		
		return true;
	}

	@Override
	public ArrayList<ArticleTypeTravel> getValue() {
		List<ArticleTypeTravel>list=this.articletypetable.getDataprovider().getList();
		ArrayList<ArticleTypeTravel>vec=new ArrayList<ArticleTypeTravel>();
		vec.addAll(list);
		return vec;
	}

	@Override
	public void setValue(ArrayList<ArticleTypeTravel> value) {
		articletypetable.connectToLocal();
		articletypetable.getDataprovider().getList().addAll(value);
		
	}

	@Override
	public void setValue(ArrayList<ArticleTypeTravel> value, boolean fireEvents) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onClick(ClickEvent event) {
		 String ArticleType=olbarticletypename.getValue();
		 if(ArticleType==null)
		 {
			 final GWTCAlert alert = new GWTCAlert(); 
		     alert.alert("Article type Cannot be empty !");
		     return;  
		 }
		 
		 String articletypvalue=this.articletypevalue.getText().trim();
		 if(articletypvalue.equals(""))
		 {
			 final GWTCAlert alert = new GWTCAlert(); 
		     alert.alert("Article Type Value Cannot be empty !");
		     return; 
		 }
		 
		 String validiity=this.validity.getText().trim();
		 if(validiity.equals(""))
		 {
			 final GWTCAlert alert = new GWTCAlert(); 
		     alert.alert("Validity Cannot be empty !");
		     return; 
		 }
		 
		 String issueat=this.issuedAt.getText().trim();
		 if(issueat.equals(""))
		 {
			 final GWTCAlert alert = new GWTCAlert(); 
		     alert.alert("Issue At Cannot be empty !");
		     return; 
		 }
		 
		 String issuedBy=this.issuedBy.getText().trim();
		 if(issuedBy.equals(""))
		 {
			 final GWTCAlert alert = new GWTCAlert(); 
		     alert.alert("Issued By Cannot be empty !");
		     return; 
		 }
		 
		 if(issueDate.getValue()==null)
		 {
			 final GWTCAlert alert = new GWTCAlert(); 
		     alert.alert("Issue Date Cannot be empty !");
		     return; 
		 }
		 
		 
		 
		 
		 if(validateProcess()==true){
			 addToArticleTypeTable();
		 }
		
	}
	
	
	protected void addToArticleTypeTable() {
		
		ArticleTypeTravel Articletypedetails = new ArticleTypeTravel();
		
		 Articletypedetails.setArticleTypeName(olbarticletypename.getValue());
		 Articletypedetails.setArticleTypeValue(articletypevalue.getText().trim());
		 Articletypedetails.setValidity(validity.getValue());
		 Articletypedetails.setIssuedAt(issuedAt.getValue());
		 Articletypedetails.setIssuedBy(issuedBy.getValue());
		 Articletypedetails.setIssueDate(issueDate.getValue());
		 Articletypedetails.setStatus("Active");
		 
		 Date validityuntil = getValidityUntil(validity.getValue(),issueDate.getValue());	
		 Articletypedetails.setValidityuntil(validityuntil);
		 
		 
			if(olbarticletypename.getValue()!=null && articletypevalue.getText().trim()!=null )
			{
				this.articletypetable.getDataprovider().getList().add(Articletypedetails);
				
				this.articletypevalue.setText("");
				this.validity.setText("");
				this.issuedAt.setText("");
				this.issuedBy.setText("");
				this.issueDate.setValue(null);;
				this.olbarticletypename.setSelectedIndex(0);
			}
	}

	private Date getValidityUntil(Integer value, Date issuedate) {
		CalendarUtil.addDaysToDate(issuedate, value);
		System.out.println("Expiry Date====" + issuedate);
		return issuedate;
	}

	public boolean validateProcess() {
		List<ArticleTypeTravel> ArticleLis=articletypetable.getDataprovider().getList();
		if(ArticleLis.size()!=0){
			for(int i=0;i<ArticleLis.size();i++)
			{
				if(ArticleLis.get(i).getArticleTypeName().trim().equals(olbarticletypename.getValue().trim()))
				{
					 final GWTCAlert alert = new GWTCAlert(); 
				     alert.alert("Article Type already added!");
					 return false;
				}
			}
		}
		return true;
	}


	public FlowPanel getContent() {
		return content;
	}


	public void setContent(FlowPanel content) {
		this.content = content;
	}


	public Button getAddButton() {
		return addButton;
	}


	public void setAddButton(Button addButton) {
		this.addButton = addButton;
	}


	public ArticleTypeTravelTable getArticletypetable() {
		return articletypetable;
	}


	public void setArticletypetable(ArticleTypeTravelTable articletypetable) {
		this.articletypetable = articletypetable;
	}


	public ObjectListBox<Config> getOlbarticletypename() {
		return olbarticletypename;
	}


	public void setOlbarticletypename(ObjectListBox<Config> olbarticletypename) {
		this.olbarticletypename = olbarticletypename;
	}


	public TextBox getArticletypevalue() {
		return articletypevalue;
	}


	public void setArticletypevalue(TextBox articletypevalue) {
		this.articletypevalue = articletypevalue;
	}


	public IntegerBox getValidity() {
		return validity;
	}


	public void setValidity(IntegerBox validity) {
		this.validity = validity;
	}


	public TextBox getIssuedAt() {
		return issuedAt;
	}


	public void setIssuedAt(TextBox issuedAt) {
		this.issuedAt = issuedAt;
	}


	public TextBox getIssuedBy() {
		return issuedBy;
	}


	public void setIssuedBy(TextBox issuedBy) {
		this.issuedBy = issuedBy;
	}


	public DateBox getIssueDate() {
		return issueDate;
	}


	public void setIssueDate(DateBox issueDate) {
		this.issueDate = issueDate;
	}


	public Object getForm() {
		return form;
	}


	public void setForm(Object form) {
		this.form = form;
	}
	
	
}
