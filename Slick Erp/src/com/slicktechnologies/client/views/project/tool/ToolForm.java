

package com.slicktechnologies.client.views.project.tool;


import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;


import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.UploadComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.articletype.ArticleTypeTravel;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.inventory.StorageLocation;
import com.slicktechnologies.shared.common.inventory.Storagebin;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.servicerelated.CompanyAsset;


public class ToolForm extends FormScreen<CompanyAsset> implements ClickHandler, ChangeHandler{

	public TextBox ibAssetid;
	public TextBox assetName;
	public TextBox tbBrand;
	public TextBox tbModelNo;
	public TextBox tbSrNo;
	public TextBox tbPurchsedFrom;
	public TextBox ibPONo;
	public DateBox dbDateofManufacture;
	public DateBox dbdateofInstallation;
	public DateBox dbDateOfPurchase;
	public UploadComposite upload;
	public ObjectListBox<Config> olbAssetCategory;
	public DateBox cbWarenty;

	ArticleTypeCompositeTravel tbArticleTypeInfoComposite;
	CheckBox cbstatus;
	ArrayList<ArticleTypeTravel> articletypetravellist = new ArrayList<ArticleTypeTravel>();
	CompanyAsset comapnyassetobj;
	
	
	/**
	 * Date 06-10-2016 By Anil
	 * Release 30 Sept 2016
	 */
		TextBox tbReferenceNumber;
	/**
	 * END
	 */
	
	/**
	 * rohan added this branch drop down for NBHC
	 * Date : 13/10/2016
	 */
	ObjectListBox<Branch> olbbBranch;
	
	/**
	 * Warehouse and Storage location stores the information that 
	 * on which warehouse asset is stored
	 * Date : 20-10-2016 By Anil
	 * Release : 30 Sept 2016 
	 * PROJECT : PURCHASE MODDIFICATION(NBHC)
	 */
	
	final GenricServiceAsync async = GWT.create(GenricService.class);
	ObjectListBox<WareHouse> olbWarehouse;
	ListBox lbStorageLoc;
	ListBox lbStoragebin;
	AssetMovementTable assetMovTbl;
	DateBox dbRetireDate;
//	TextBox tbStatus;
	TextArea taDesription;
	TextBox tbProductId;
	
	/**
	 * End
	 */
	
	/**
	 * this is grn Id field
	 * Date : 22-10-2016 By Anil
	 * Release : 30 Sept 2016 
	 * PROJECT : PURCHASE MODDIFICATION(NBHC)
	 */
	TextBox tbGrnId;
	/** date 02.08.2018 added by komal for sales Price**/
	DoubleBox dbSalesPrice;


	public ToolForm() {
		super();
		createGui();
		ibAssetid.setEnabled(false);
		tbArticleTypeInfoComposite.setForm(this);
		cbstatus.setValue(true);
		
//		tbStatus.setValue(AppConstants.ACTIVE);
//		tbStatus.setEnabled(false);
		dbRetireDate.setEnabled(false);
		tbProductId.setEnabled(false);
		tbGrnId.setEnabled(false);
	
	}

	public ToolForm(String[] processlevel, FormField[][] fields,FormStyle formstyle) 
	{
		super(processlevel, fields, formstyle);
		createGui();
		ibAssetid.setEnabled(false);
		tbArticleTypeInfoComposite.setForm(this);
	}

	private void initializeWidgets()
	{
	    ibAssetid = new TextBox();
	    assetName = new TextBox();
	    tbBrand = new TextBox();
	    tbModelNo = new TextBox();
	    tbSrNo = new TextBox();
	    tbPurchsedFrom = new TextBox();
	    dbDateofManufacture = new DateBoxWithYearSelector();
	    dbdateofInstallation = new DateBoxWithYearSelector();
	    upload = new UploadComposite();
	    cbWarenty = new DateBoxWithYearSelector();
	    dbDateOfPurchase =new DateBoxWithYearSelector();
	    ibPONo = new TextBox();
	    olbAssetCategory = new ObjectListBox<Config>();
	    AppUtility.MakeLiveConfig(olbAssetCategory, Screen.ASSETCATEGORY);
	    
	    tbArticleTypeInfoComposite = new ArticleTypeCompositeTravel();
		cbstatus = new CheckBox();
		cbstatus.setValue(true);
		
		tbReferenceNumber=new TextBox();
		
		
		
		olbWarehouse = new ObjectListBox<WareHouse>();
		Vector<Filter> temp=new  Vector<Filter>();
		Filter filter=new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		temp.add(filter);
		MyQuerry querry = new MyQuerry(temp, new WareHouse());
		olbWarehouse.MakeLive(querry);
		olbWarehouse.addChangeHandler(this);
		
		lbStorageLoc=new ListBox();
		lbStorageLoc.addChangeHandler(this);
		lbStorageLoc.addItem("--SELECT--");
		lbStoragebin=new ListBox();
		lbStoragebin.addItem("--SELECT--");
		
		assetMovTbl=new AssetMovementTable();
		
		dbRetireDate=new DateBoxWithYearSelector();
//		tbStatus=new TextBox();	
		taDesription=new TextArea();
		
		tbProductId=new TextBox();
		tbProductId.setEnabled(false);
		
		tbGrnId=new TextBox();
		tbGrnId.setEnabled(false);
		
		olbbBranch=new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbbBranch);
		/** date 02.08.2018 added by komal for sales Price**/
		dbSalesPrice = new DoubleBox();
	}



	public void createScreen()
	{
	    processlevelBarNames = (new String[] {"New",AppConstants.ASSIGNASSET,AppConstants.RETIREASSET,"Copy Asset"});
	    initializeWidgets();
	    FormFieldBuilder fbuilder = new FormFieldBuilder();
	    FormField fgroupingAssetInfo = fbuilder.setlabel("Asset Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).setKeyField(true).build();
	    FormFieldBuilder buildname = new FormFieldBuilder("* Name", assetName);
	    FormField fieldname = buildname.setMandatory(true).setMandatoryMsg("Name is Mandatory").build();
	    FormFieldBuilder buildid = new FormFieldBuilder("Asset Id", ibAssetid);
	    FormField fieldid = buildid.setMandatory(false).setMandatoryMsg("Id is Mandatory").build();
	    FormFieldBuilder buildModelNo = new FormFieldBuilder("Model Number", tbModelNo);
	    FormField fieldModelNo = buildModelNo.setMandatory(false).build();
	    FormFieldBuilder buildBrand = new FormFieldBuilder("Brand", tbBrand);
	    FormField fieldBrand = buildBrand.setMandatory(false).build();
	    FormFieldBuilder buildSrNo = new FormFieldBuilder("Sr. Number", tbSrNo);
	    FormField fieldSrNo = buildSrNo.setMandatory(false).build();
	    FormFieldBuilder buildDateOfManufacture = new FormFieldBuilder("Manufacturing On", dbDateofManufacture);
	    FormField fieldDateOfManufacture = buildDateOfManufacture.setMandatory(false).build();
	    FormFieldBuilder buildDateOfInstallation = new FormFieldBuilder("Installation On", dbdateofInstallation);
	    FormField fieldDateOfInstallation = buildDateOfInstallation.setMandatory(false).build();
	    FormFieldBuilder buildUpload = new FormFieldBuilder("Upload Image", upload);
	    FormField fieldUpload = buildUpload.setMandatory(false).build();
	    FormFieldBuilder buildWarenty = new FormFieldBuilder("Warranty Until", cbWarenty);
	    FormField fieldWarenty = buildWarenty.setMandatory(false).build();
	    FormFieldBuilder buildPurchsedFrom = new FormFieldBuilder("Vendor", tbPurchsedFrom);//Rename Purchase from as a Vendor for simplification project.
	    FormField fieldPurchsedFrom = buildPurchsedFrom.setMandatory(false).build();
	    FormFieldBuilder buildAssetCategory = new FormFieldBuilder("* Asset Category", olbAssetCategory);
	    FormField fieldAssetCategory = buildAssetCategory.setMandatory(true).setMandatoryMsg("Asset Category is Mandatory").build();
	    FormFieldBuilder buildPurchaseDate = new FormFieldBuilder("Purchase Date", dbDateOfPurchase);
	    FormField fieldPurchaseDate = buildPurchaseDate.setMandatory(false).build();
	    FormFieldBuilder buildPONo = new FormFieldBuilder("PO No", ibPONo);
	    FormField fieldPONo = buildPONo.setMandatory(false).build();
	    
	
	    FormFieldBuilder fbuilder1 = new FormFieldBuilder();
		FormField fgroupingArticleTypeInformation=fbuilder1.setlabel("Article Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
	    FormFieldBuilder fbuilder2 = new FormFieldBuilder("",tbArticleTypeInfoComposite);
	    FormField farticleComposite= fbuilder2.setMandatory(false).setRowSpan(0).setColSpan(4).build();
	    
		fbuilder = new FormFieldBuilder("Status",cbstatus);
	    FormField fcbstatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	    
	    fbuilder = new FormFieldBuilder("Reference Number",tbReferenceNumber);
	    FormField ftbReferenceNumber= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	    
	    fbuilder = new FormFieldBuilder();
		FormField fGroupingStorageInformation=fbuilder1.setlabel("History").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
	    
	    fbuilder = new FormFieldBuilder("Warehouse",olbWarehouse);
	    FormField foblwarehouse= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	    
	    fbuilder = new FormFieldBuilder("Storage Location",lbStorageLoc);
	    FormField flsStorageLoc= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	    
	    fbuilder = new FormFieldBuilder("Storage Bin",lbStoragebin);
	    FormField flsStoragebin= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	    
	    fbuilder = new FormFieldBuilder();
		FormField fGroupingMovementInformation=fbuilder1.setlabel("Reference/General").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		 fbuilder = new FormFieldBuilder();
		 FormField fGroupingImagesInformation=fbuilder1.setlabel("Image").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
	    
		fbuilder = new FormFieldBuilder("",assetMovTbl.getTable());
	    FormField fassetMovTbl= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
	    
//	    fbuilder = new FormFieldBuilder("Status",tbStatus);
//	    FormField ftbStatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	    
	    fbuilder = new FormFieldBuilder("Retire Date",dbRetireDate);
	    FormField fdbRetireDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	    
	    fbuilder = new FormFieldBuilder("Description",taDesription);
	    FormField ftaDesription= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
	    
	    fbuilder = new FormFieldBuilder("Product Id",tbProductId);
	    FormField ftbProductId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	    
	    fbuilder = new FormFieldBuilder("GRN Id",tbGrnId);
	    FormField ftbGrnId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	    
	    
	    fbuilder = new FormFieldBuilder("* Branch",olbbBranch);
		FormField folbbBranch= fbuilder.setMandatory(true).setMandatoryMsg("Branch is Mandatory").setRowSpan(0).setColSpan(0).build();
	    
		/** date 02.08.2018 added by komal for sales Price**/
		fbuilder = new FormFieldBuilder("Purchase Price",dbSalesPrice);
		FormField fdbSalesPrice= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
//	    FormField[][] formfield = { 
//	    		{fgroupingAssetInfo},
//	    		{fieldid,fieldname,fieldAssetCategory,fieldBrand},
//	    		{fieldModelNo,fieldSrNo,fieldPurchsedFrom,fieldPONo},
//	    		{fieldPurchaseDate,fieldDateOfManufacture,fieldDateOfInstallation,fieldWarenty},
//	    		{ftbReferenceNumber,ftbProductId,fdbRetireDate,fcbstatus},
//	    		{ftbGrnId,folbbBranch,fieldUpload,fdbSalesPrice},
//	    		{ftaDesription},
//	    		{fgroupingArticleTypeInformation},
//	    		{farticleComposite},
//	    		{fGroupingStorageInformation},
//	    		{foblwarehouse,flsStorageLoc,flsStoragebin},
//	    		{fGroupingMovementInformation},
//	    		{fassetMovTbl}
//	    };
		
		FormField[][] formfield = { 
	    		{fgroupingAssetInfo},
	    		{fieldid,fieldname,fieldAssetCategory,folbbBranch},
	    		{fieldWarenty,fieldBrand,fieldModelNo,fcbstatus},
	    		{fieldSrNo},
	    		/**HISTORY**/
	    		{fGroupingStorageInformation},
	    		{foblwarehouse,flsStorageLoc,flsStoragebin},
	    		{fassetMovTbl},
	    		/**Reference**/
	    		{fGroupingMovementInformation},
	    		{ftbProductId,fieldPONo,fieldPurchaseDate,fieldPurchsedFrom},
	    		{fdbSalesPrice,ftbGrnId,fieldDateOfManufacture,ftbReferenceNumber},
	    		{fdbRetireDate,fieldDateOfInstallation,},
	    		{ftaDesription},
	    		/**Article Info**/
	    		{fgroupingArticleTypeInformation},
	    		{farticleComposite},
	    		/**Images**/
	    		{fGroupingImagesInformation},
	    		{fieldUpload},
	    		
	    		
	    		
	    		
	    };
	    
	    this.fields=formfield;	
	    
	}

	public void updateModel(CompanyAsset model)
	{
	    if(assetName.getValue() != null)
	        model.setName(assetName.getValue());
	    if(tbBrand.getValue() != null)
	        model.setBrand(tbBrand.getValue());
	    if(tbModelNo.getValue() != null)
	        model.setModelNo(tbModelNo.getValue());
	    if(tbSrNo.getValue() != null)
	        model.setSrNo(tbSrNo.getValue());
	    if(tbPurchsedFrom.getValue() != null)
	        model.setPurchasedFrom(tbPurchsedFrom.getValue());
	    if(dbDateofManufacture.getValue() != null)
	        model.setDateOfManufacture(dbDateofManufacture.getValue());
	    if(dbdateofInstallation.getValue() != null)
	        model.setDateOfInstallation(dbdateofInstallation.getValue());
	    if(cbWarenty.getValue() != null)
	        model.setWarrenty(cbWarenty.getValue());
	    if(olbAssetCategory.getValue() != null)
	        model.setCategory(olbAssetCategory.getValue());
	    if(dbDateOfPurchase.getValue() != null)
	        model.setPODate(dbDateOfPurchase.getValue());
	    if(ibPONo.getValue() != null)
	        model.setPoNo(ibPONo.getValue());
	    
	    ArrayList<ArticleTypeTravel> artilcelist = new ArrayList<ArticleTypeTravel>();
	    artilcelist.addAll(tbArticleTypeInfoComposite.getValue());
	    artilcelist.addAll(articletypetravellist);
	    
	    model.setArticleTypeDetails(artilcelist);
	    model.setStatus(cbstatus.getValue());
	    
	    model.setDocument(upload.getValue());
	    
	    if(olbbBranch.getValue(olbbBranch.getSelectedIndex())!=null)
			model.setBranch(olbbBranch.getValue(olbbBranch.getSelectedIndex()));
	    
	    if(tbReferenceNumber.getValue()!=null){
	    	model.setReferenceNumber(tbReferenceNumber.getValue());
	    }
	    
	    
	    if(dbRetireDate.getValue()!=null){
	    	model.setRetireDate(dbRetireDate.getValue());
	    }
	    if(taDesription.getValue()!=null){
	    	model.setDescription(taDesription.getValue());
	    }
	    if(olbWarehouse.getSelectedIndex()!=0){
	    	model.setWarehouseName(olbWarehouse.getValue());
	    }
	    if(lbStorageLoc.getSelectedIndex()!=0){
	    	model.setStorageLocation(lbStorageLoc.getValue(lbStorageLoc.getSelectedIndex()));
	    }
	    if(lbStoragebin.getSelectedIndex()!=0){
	    	model.setStorageBin(lbStoragebin.getValue(lbStoragebin.getSelectedIndex()));
	    }
	    if(assetMovTbl.getValue()!=null){
	    	model.setAssetMovementList(assetMovTbl.getValue());
	    }
	    if(!tbProductId.getValue().equals("")){
	    	model.setProductId(Integer.parseInt(tbProductId.getValue()));
	    }
	    if(!tbGrnId.getValue().equals("")){
	    	model.setGrnId(Integer.parseInt(tbGrnId.getValue()));
	    }
	    /** date 02.08.2018 added by komal for sales Price**/
	    if(dbSalesPrice.getValue() !=null){
	    	model.setPrice(dbSalesPrice.getValue());
	    }

	    comapnyassetobj=model;
	    
	    presenter.setModel(model);
	}

	public void updateView(CompanyAsset view)
	{
		comapnyassetobj=view;
		
		 if(view.getCount() != -1)
			 ibAssetid.setValue(Integer.valueOf(view.getCount())+"");
	    if(view.getName() != null)
	        assetName.setValue(view.getName());
	    if(view.getBrand() != null)
	        tbBrand.setValue(view.getBrand());
	    if(view.getModelNo() != null)
	        tbModelNo.setValue(view.getModelNo());
	    if(view.getSrNo() != null)
	        tbSrNo.setValue(view.getSrNo());
	    if(view.getPurchasedFrom() != null)
	        tbPurchsedFrom.setValue(view.getPurchasedFrom());
	    if(view.getDateOfManufacture() != null)
	        dbDateofManufacture.setValue(view.getDateOfManufacture());
	    if(view.getDateOfInstallation() != null)
	        dbdateofInstallation.setValue(view.getDateOfInstallation());
        if(view.getDocument()!=null)
          upload.setValue(view.getDocument());
	    if(view.getCategory() != null)
	        olbAssetCategory.setValue(view.getCategory());
	    if(view.getPODate() != null)
	        dbDateOfPurchase.setValue(view.getPODate());
	    if(view.getPoNo() != null)
	        ibPONo.setValue(view.getPoNo());
	    cbWarenty.setValue(view.isWarrenty());
	    
	    tbArticleTypeInfoComposite.setValue(updateArticleInfoStatusTable(view.getArticleTypeDetails()));
	    cbstatus.setValue(view.getStatus());
	    
	    System.out.println("Val "+presenter);
	    
	    if(view.getReferenceNumber()!=null){
	    	tbReferenceNumber.setValue(view.getReferenceNumber());
	    }
	    if(view.getRetireDate()!=null){
	    	dbRetireDate.setValue(view.getRetireDate());
	    }
	    if(view.getDescription()!=null){
	    	taDesription.setValue(view.getDescription());
	    }
	    if(view.getAssetMovementList()!=null){
	    	assetMovTbl.setValue(view.getAssetMovementList());
	    }
	    if(view.getWarehouseName()!=null){
	    	olbWarehouse.setValue(view.getWarehouseName());
	    	retriveStorageLocation(view.getWarehouseName(),view.getStorageLocation(),view.getStorageBin(),true);
	    }
	    if(view.getProductId()!=0){
	    	tbProductId.setValue(view.getProductId()+"");
	    }
	    
	    if(view.getBranch()!=null)
			olbbBranch.setValue(view.getBranch());
	    
	    if(view.getGrnId()!=0){
	    	tbGrnId.setValue(view.getGrnId()+"");
	    }
	    /** date 02.08.2018 added by komal for sales Price**/
	    dbSalesPrice.setValue(view.getPrice());
	    presenter.setModel(view);
	}

	private ArrayList<ArticleTypeTravel> updateArticleInfoStatusTable(ArrayList<ArticleTypeTravel> list) {
	articletypetravellist = new ArrayList<ArticleTypeTravel>();
	ArrayList<ArticleTypeTravel> articlelist = new ArrayList<ArticleTypeTravel>();
	for(ArticleTypeTravel articletravel :list){
		if(articletravel.getStatus().equals("Active")){
			articlelist.add(articletravel);
		}else{
			articletypetravellist.add(articletravel);
		}
	}
	return articlelist;
}


	public void toggleAppHeaderBarMenu()
	{
	    if(AppMemory.getAppMemory().currentState == ScreeenState.NEW)
	    {
	        InlineLabel menus[] = AppMemory.getAppMemory().skeleton.getMenuLabels();
	        for(int k = 0; k < menus.length; k++)
	        {
	            String text = menus[k].getText();
	            if(text.contains("Save") || text.contains("Discard") || text.equals("Search")|| text.equals(AppConstants.NAVIGATION))
	                menus[k].setVisible(true);
	            else
	                menus[k].setVisible(false);
	        }
	
	    } else
	    if(AppMemory.getAppMemory().currentState == ScreeenState.EDIT)
	    {
	        InlineLabel menus[] = AppMemory.getAppMemory().skeleton.getMenuLabels();
	        for(int k = 0; k < menus.length; k++)
	        {
	            String text = menus[k].getText();
	            if(text.contains("Save") || text.contains("Discard")|| text.equals(AppConstants.NAVIGATION))
	                menus[k].setVisible(true);
	            else
	                menus[k].setVisible(false);
	        }
	
	    } else
	    if(AppMemory.getAppMemory().currentState == ScreeenState.VIEW)
	    {
	        InlineLabel menus[] = AppMemory.getAppMemory().skeleton.getMenuLabels();
	        for(int k = 0; k < menus.length; k++)
	        {
	            String text = menus[k].getText();
	            if(text.contains("Edit") || text.contains("Discard")|| text.contains("Search")|| text.equals(AppConstants.NAVIGATION))
	                menus[k].setVisible(true);
	            else
	                menus[k].setVisible(false);
	        }
	
	    }
	    AuthorizationHelper.setAsPerAuthorization(Screen.COMPANYASSET,LoginPresenter.currentModule.trim());
	}
	
	public void clear()
	{
	    super.clear();
	}
	
	public void setCount(int count)
	{
	    ibAssetid.setValue(Integer.valueOf(count)+"");
	}
	
	public boolean validate()
	{
	    boolean superValidate = super.validate();
	    if(superValidate==false)
	    	return false;
	    
	    Date warrantyDate = cbWarenty.getValue();
	    Date installationDate = dbdateofInstallation.getValue();
	    Date manufacturingDate = dbDateofManufacture.getValue();
	    Date purchaseDate = dbDateOfPurchase.getValue();
	    
	    if(warrantyDate!=null&&installationDate!=null)
	    {
	    	 boolean lastDate = warrantyDate.after(installationDate);
	    	 if(!lastDate)
	    	 {
	             showDialogMessage(" Warranty Date Should be after Installation Date !");
	             return false;
	    	 }
	    	
	    }
	    
	    if(installationDate!=null && purchaseDate!=null)
	    {
	    	 boolean midleDate = installationDate.after(purchaseDate);
	    	 if(!midleDate)
	    	 {
	             showDialogMessage(" Installation Date  Should be after Purchase Date !");
	             return false;
	    	 }
	        
	    }
	    if(purchaseDate!=null&& manufacturingDate!=null)
	    {
		    boolean firstDate = purchaseDate.after(manufacturingDate);
		    if(!firstDate)
		    {
		        showDialogMessage("Purchase Date  Should be after Manufacturing Date!");
		        return false;
		    }
	    }
	    
	    if(tbArticleTypeInfoComposite.validate()){
	    	return true;
	    }
	    return true;
	}
	
	
	
	@Override
	public void onClick(ClickEvent event) {
		System.out.println("Inside Click Method");
	}
	
	
	
	
	@Override
	public void setToNewState() {
		super.setToNewState();
		System.out.println("Inside NEW method");
		if(comapnyassetobj!=null){
	
			SuperModel model=new CompanyAsset();
			model=comapnyassetobj;
			AppUtility.addDocumentToHistoryTable(AppConstants.ASSETMANEGEMENTMODULE,AppConstants.COMPANYASSET, comapnyassetobj.getCount(), null,null,null, false, model, null);
	
		}
	}
	
	@Override
	public void setToEditState() {
		super.setToEditState();
		this.processLevelBar.setVisibleFalse(false);
		
	}
	
	
	@Override
	public void setToViewState() {
		super.setToViewState();
		setMenuAsPerStatus();
		SuperModel model=new CompanyAsset();
		model=comapnyassetobj;
		AppUtility.addDocumentToHistoryTable(AppConstants.ASSETMANEGEMENTMODULE,AppConstants.COMPANYASSET, comapnyassetobj.getCount(), null,null,null, false, model, null);
	
	}
	
	public void setMenuAsPerStatus() {
		setAppHeaderMenuAsPerStatus();
		setToggleLevelMenuAsPerStatus();
	}

	private void setAppHeaderMenuAsPerStatus() {
		CompanyAsset entity = (CompanyAsset) presenter.getModel();
		if (entity.getStatus()==false&&entity.getRetireDate()!=null){
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Discard") || text.contains("Search")|| text.contains(AppConstants.NAVIGATION)){
					menus[k].setVisible(true);
				} else {
					menus[k].setVisible(false);
				}
			}
		}
	}

	private void setToggleLevelMenuAsPerStatus() {
		CompanyAsset entity = (CompanyAsset) presenter.getModel();
		
		for (int i = 0; i < getProcesslevelBarNames().length; i++) {
			InlineLabel label = getProcessLevelBar().btnLabels[i];
			String text = label.getText().trim();
			
			if (entity.getStatus()==false&&entity.getRetireDate()!=null) {
				if (text.equals(AppConstants.NEW)){
					label.setVisible(true);
				}
				if (text.equals(AppConstants.ASSIGNASSET)){
					label.setVisible(false);
				}
				if (text.equals(AppConstants.RETIREASSET)){
					label.setVisible(false);
				}
			} 
		}
	}

	@Override
	public void setEnable(boolean value)
	{
		super.setEnable(value);
		ibAssetid.setEnabled(false);
//		tbStatus.setEnabled(false);
		dbRetireDate.setEnabled(false);
		tbProductId.setEnabled(false);
	}

	@Override
	public void onChange(ChangeEvent event) {
		if(event.getSource().equals(olbWarehouse)){
			if(olbWarehouse.getSelectedIndex()!=0){
				retriveStorageLocation(olbWarehouse.getValue(),null,null,false);
			}else{
				lbStoragebin.setSelectedIndex(0);
				lbStorageLoc.setSelectedIndex(0);
			}
		}
		if(event.getSource().equals(lbStorageLoc)){
			if(lbStorageLoc.getSelectedIndex()!=0){
				retriveStorageBin(lbStorageLoc.getValue(lbStorageLoc.getSelectedIndex()),null,false);
			}else{
				lbStoragebin.setSelectedIndex(0);
			}
		}
	}
	
	private void retriveStorageLocation(String warehouseName,final String storageLocation,final String storageBin,final boolean viewFlag){
		MyQuerry query = new MyQuerry();
		Company c=new Company();
		System.out.println("Company Id :: "+c.getCompanyId());
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("warehouseName");
		filter.setStringValue(warehouseName);
		filtervec.add(filter);
		
		query.setFilters(filtervec);
		query.setQuerryObject(new StorageLocation());
		
		async.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				lbStorageLoc.clear();
				lbStoragebin.clear();
				lbStorageLoc.addItem("--SELECT--");
				lbStoragebin.addItem("--SELECT--");
				for (SuperModel model : result) {
					StorageLocation entity = (StorageLocation) model;
					lbStorageLoc.addItem(entity.getBusinessUnitName());
					System.out.println("Location :"+entity.getBusinessUnitName());
				}
				
				if(viewFlag){
					int count=lbStorageLoc.getItemCount();
					for(int i=0;i<count;i++){
						if(storageLocation.equals(lbStorageLoc.getItemText(i))){
							lbStorageLoc.setSelectedIndex(i);
							retriveStorageBin(storageLocation,storageBin,viewFlag);
							break;
						}
					}
				}
				
			}
		});
	}
	
	// ****************************** Retrieving Storage Bin From Storage location **********************************
	
	private void retriveStorageBin(String storageLocation,final String storageBin,final boolean viewFlag){
		MyQuerry query = new MyQuerry();
		Company c=new Company();
		System.out.println("Company Id :: "+c.getCompanyId());
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("storagelocation");
		filter.setStringValue(storageLocation);
		filtervec.add(filter);
		
		query.setFilters(filtervec);
		query.setQuerryObject(new Storagebin());
		
		async.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				lbStoragebin.clear();
				lbStoragebin.addItem("--SELECT--");
				for (SuperModel model : result) {
					Storagebin entity = (Storagebin) model;
					lbStoragebin.addItem(entity.getBinName());
					System.out.println("BIN :"+entity.getBinName());
				}
				
				if(viewFlag){
					int count=lbStoragebin.getItemCount();
					for(int i=0;i<count;i++){
						if(storageBin.equals(lbStoragebin.getItemText(i))){
							lbStoragebin.setSelectedIndex(i);
							break;
						}
					}
				}
			}
		});
	}
	
}
