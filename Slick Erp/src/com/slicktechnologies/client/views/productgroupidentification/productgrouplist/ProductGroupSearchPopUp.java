package com.slicktechnologies.client.views.productgroupidentification.productgrouplist;

import java.util.Vector;

import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;

public class ProductGroupSearchPopUp extends SearchPopUpScreen<ProductGroupList> {

	public IntegerBox tbProductGroupId,tbProductId;
	public TextBox tbCode,tbName,tbTitle;
	ProductInfoComposite pic;

	public ProductGroupSearchPopUp(){
	    super();
	    createGui();
	}
		public ProductGroupSearchPopUp(boolean b) {
		super(b);
		createGui();
	}
		
		public void initWidget(){
			
			tbProductGroupId = new IntegerBox();
			tbProductId = new IntegerBox();
			tbCode = new TextBox();
			tbName = new TextBox();
			tbTitle=new TextBox();
			pic=AppUtility.initiateSalesProductComposite(new SuperProduct());
			//			tbQuantity = new TextBox();
//			tbUnit = new TextBox();
		}
		public void createScreen(){	
		initWidget();
		FormFieldBuilder builder;
		
		builder = new FormFieldBuilder("Product Group ID",tbProductGroupId);
		FormField ftbProductGroupId =builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Product ID",tbProductId);
		FormField ftbProductId=builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	
		builder = new FormFieldBuilder("Code",tbCode);
		FormField ftbCode= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Name",tbName);
		FormField ftbName= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Title",tbTitle);
		FormField ftbTitle = builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("",pic);
		FormField fpic =builder.setMandatory(false).setRowSpan(0).setColSpan(5).build();



		this.fields= new FormField[][]{
				{},
				{ftbProductGroupId,ftbTitle},
				{fpic},
		};
		}

		
	
	@Override
	public MyQuerry getQuerry() {

			Vector<Filter> filtervec=new Vector<Filter>();
			Filter temp=null;
				
			
			if (tbProductGroupId.getValue()!=null){
			temp =new Filter();
			temp.setIntValue(tbProductGroupId.getValue());
			temp.setQuerryString("productGroupId");
			filtervec.add(temp);
			}

			if (!tbTitle.getValue().equals("")){
				temp=new Filter();
				temp.setStringValue(tbTitle.getValue().trim());
				temp.setQuerryString("title");
				filtervec.add(temp);
			}
			
			
			if (pic.getIdValue()!=-1){
				temp=new Filter();
				temp.setIntValue(pic.getIdValue());
				temp.setQuerryString("product_id");
				filtervec.add(temp);
			}
			
			if (!pic.getProdCode().getValue().equals("")){
				temp=new Filter();
				temp.setStringValue(pic.getProdCode().getValue().trim());
				temp.setQuerryString("code");
				filtervec.add(temp);
			}


			if (!pic.getProdName().getValue().equals("")){
				temp=new Filter();
				temp.setStringValue(pic.getProdName().getValue().trim());
				temp.setQuerryString("name");
				filtervec.add(temp);
			}
			
			temp =new Filter();
			temp.setBooleanvalue(true);
			temp.setQuerryString("status");
			filtervec.add(temp);

			MyQuerry querry = new MyQuerry();
			querry.setFilters(filtervec);
			querry.setQuerryObject(new ProductGroupList());
			return querry;
	}	
	
	public InlineLabel getDwnload() {
		return dwnload;
	}
	@Override
	public boolean validate() {
		return true;
	}		
	}


