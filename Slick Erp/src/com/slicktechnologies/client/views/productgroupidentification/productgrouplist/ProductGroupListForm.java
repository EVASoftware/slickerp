package com.slicktechnologies.client.views.productgroupidentification.productgrouplist;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreen;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class ProductGroupListForm extends TableScreen<ProductGroupList> implements ClickHandler{

	
	
	public ProductGroupListForm(SuperTable<ProductGroupList> superTable){
		super(superTable);
		createGui();
		superTable.connectToLocal();
 	}
		
	@Override
	public void createScreen() 
	{
	}

	/**
	 * method template to update the model with token entity name
	 */
	@Override
	public void updateModel(ProductGroupList model)
	{
		//<ReplasableUpdateModelFromViewCode>
	}

	@SuppressWarnings("unused")
	private void initalizeWidget()
	{
	}

	@Override
	public void updateView(ProductGroupList view) 
	{
		//<ReplasableUpdateViewFromModelCode>
	}
	
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void toggleAppHeaderBarMenu() {
		if(superTable!=null&&superTable.getListDataProvider().getList()!=null){
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Rows:")){
					menus[k].setText("Rows:"+superTable.getListDataProvider().getList().size());	
					menus[k].getElement().setId("addlabel2");
				}
			}
		}
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Search")||text.equals("Download")||text.equals(AppConstants.NAVIGATION)||text.contains("Rows:"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		  		
			}
		}
		
		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals("Download")||text.equals(AppConstants.NAVIGATION)||text.contains("Rows:"))
					menus[k].setVisible(true); 
				
				else
					menus[k].setVisible(false);  		   
			}
		}
		
		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Search")||text.equals("Download")||text.equals(AppConstants.NAVIGATION)||text.contains("Rows:")
						)
						menus[k].setVisible(true); 
				
				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.PRODUCTGROUPLIST,LoginPresenter.currentModule.trim());
	}

}
