package com.slicktechnologies.client.views.productgroupidentification.productgrouplist;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;

public class ProductGroupListTable extends SuperTable<ProductGroupList> {
	
	TextColumn<ProductGroupList>getProductGroupIdcolumn;
	TextColumn<ProductGroupList>getProductIdcolumn;
	TextColumn<ProductGroupList>getCodecolumn;
	TextColumn<ProductGroupList>getNamecolumn;
	TextColumn<ProductGroupList>getQuantitycolumn;
	TextColumn<ProductGroupList>getUnitcolumn;
	
	public ProductGroupListTable(){
		super();
	}

	@Override
	public void createTable() {

	addColumngetProductGroupId();
	addColumngetProductId();
	addColumngetCode();
	addColumngetName();
	addColumngetQuantity();
	addColumngetUnit();
		
	table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
	}

	private void addColumngetUnit() {

	getUnitcolumn=new TextColumn<ProductGroupList>() 
	{
	public String getValue(ProductGroupList object)
			{
				return object.getUnit()+"";
			}
	};
	table.addColumn(getUnitcolumn,"UOM" );
	getUnitcolumn.setSortable(true);
	}

	
	
	private void addColumngetQuantity() {
	getQuantitycolumn=new TextColumn<ProductGroupList>() 
	{
	public String getValue(ProductGroupList object)
			{
				return object.getQuantity()+"";
			}
	};
		table.addColumn(getQuantitycolumn,"Quantity" );
		getQuantitycolumn.setSortable(true);
	}


	private void addColumngetName() {

		getNamecolumn=new TextColumn<ProductGroupList>() {
			public String getValue(ProductGroupList object)
			{
				return object.getName()+"";
			}
		};
		table.addColumn(getNamecolumn,"Name" );
		getNamecolumn.setSortable(true);
	}


	private void addColumngetCode() {

		getCodecolumn=new TextColumn<ProductGroupList>() {
			public String getValue(ProductGroupList object)
			{
				return object.getCode()+"";
			}
		};
		table.addColumn(getCodecolumn,"Code" );
		getCodecolumn.setSortable(true);
	}


	private void addColumngetProductId() {
		
		getProductIdcolumn=new TextColumn<ProductGroupList>()
				{
			@Override
			public String getValue(ProductGroupList object)
			{
				return object.getProduct_id()+"";
			}
				};
				table.addColumn(getProductIdcolumn,"Product ID");
				getProductIdcolumn.setSortable(true);
	}
	


	private void addColumngetProductGroupId() {

		getProductGroupIdcolumn=new TextColumn<ProductGroupList>() {
			@Override
			public String getValue(ProductGroupList object)
			{
				return object.getProductGroupId()+"";
			}
		};		
		table.addColumn(getProductGroupIdcolumn,"Product Group ID");
		getProductGroupIdcolumn.setSortable(true);
	}

//**********************column sorting****************
	public void addColumnSorting() {
		
	addColumnProductGroupIdSorting();
	addColumnProductIdSorting();
	addColumnCodeSorting();
	addColumnNameSorting();
	addColumnQuantitySorting();
	addColumnUOMSorting();
	}
	
	private void addColumnUOMSorting() {

	List<ProductGroupList> list = getDataprovider().getList();
	columnSort = new ListHandler<ProductGroupList>(list);
	Comparator<ProductGroupList> com = new Comparator<ProductGroupList>() {

	@Override
	public int compare(ProductGroupList e1,ProductGroupList e2) {

				if(e1!=null && e2!=null)
				{
					if( e1.getUnit()!=null && e2.getUnit()!=null){
						return e1.getUnit().compareTo(e2.getUnit());}
				}
				    else
				{
				  return 0;}
				  return 0;
				}
		};
		columnSort.setComparator(getUnitcolumn, com);
		table.addColumnSortHandler(columnSort);
	}


	private void addColumnQuantitySorting() {
		
		List<ProductGroupList> list = getDataprovider().getList();
		columnSort = new ListHandler<ProductGroupList>(list);
		Comparator<ProductGroupList> com = new Comparator<ProductGroupList>() {

			@Override
			public int compare(ProductGroupList e1,ProductGroupList e2) {

				if(e1!=null && e2!=null){
					if(e1.getQuantity()== e2.getQuantity())
					{
						return 0;
					}
						if(e1.getQuantity()> e2.getQuantity())	
					{
						return 1;
					}
						else
					{
						return -1;
					}}else					
				
				return 0;
			
				}
		};
		columnSort.setComparator(getQuantitycolumn, com);
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnNameSorting() {
		List<ProductGroupList> list = getDataprovider().getList();
		columnSort = new ListHandler<ProductGroupList>(list);
		Comparator<ProductGroupList> com = new Comparator<ProductGroupList>() {

			@Override
			public int compare(ProductGroupList e1,ProductGroupList e2) {

				if(e1!=null && e2!=null)
				{
					if( e1.getName()!=null && e2.getName()!=null){
						return e1.getName().compareTo(e2.getName());}
				}
				  else{
				  return 0;}
				  return 0;
				}
		};
		columnSort.setComparator(getNamecolumn, com);
		table.addColumnSortHandler(columnSort);
	}
		
	


	private void addColumnCodeSorting() {
	
		List<ProductGroupList> list = getDataprovider().getList();
		columnSort = new ListHandler<ProductGroupList>(list);
		Comparator<ProductGroupList> com = new Comparator<ProductGroupList>() {

			@Override
			public int compare(ProductGroupList e1,ProductGroupList e2) {

				if(e1!=null && e2!=null)
				{
					if( e1.getCode()!=null && e2.getCode()!=null)
					{
					return e1.getCode().compareTo(e2.getCode());
					}
				}
				  else
				  {
				  return 0;
				  }
				  return 0;
				}
				};
		columnSort.setComparator(getCodecolumn,com);
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnProductIdSorting() {
	
		List<ProductGroupList> list = getDataprovider().getList();
		columnSort = new ListHandler<ProductGroupList>(list);
		Comparator<ProductGroupList> com = new Comparator<ProductGroupList>() {

			@Override
			public int compare(ProductGroupList e1,ProductGroupList e2) {

				if(e1!=null && e2!=null){
					if(e1.getProduct_id()== e2.getProduct_id())
					{
						return 0;
					}
						if(e1.getProduct_id()> e2.getProduct_id())	
					{
						return 1;
					}
						else
					{
						return -1;
					}}else					
				
						return 0;
					}
			};
		columnSort.setComparator(getProductIdcolumn, com);
		table.addColumnSortHandler(columnSort);
	}
	
	private void addColumnProductGroupIdSorting() {

		List<ProductGroupList> list = getDataprovider().getList();
		columnSort = new ListHandler<ProductGroupList>(list);
		Comparator<ProductGroupList> com = new Comparator<ProductGroupList>() {

			@Override
			public int compare(ProductGroupList e1,ProductGroupList e2) {

				if(e1!=null && e2!=null){
					if(e1.getProductGroupId()== e2.getProductGroupId())
					{
						return 0;
					}
						if(e1.getProductGroupId()> e2.getProductGroupId())	
					{
						return 1;
					}
						else
					{
						return -1;
					}}else					
				
				return 0;
			
				}
		};
		columnSort.setComparator(getProductGroupIdcolumn,com);
		table.addColumnSortHandler(columnSort);
	}
	  
//*******************getter and setter********************
	public TextColumn<ProductGroupList> getGetProductGroupIdcolumn() {
		return getProductGroupIdcolumn;
	}


	public TextColumn<ProductGroupList> getGetProductIdcolumn() {
		return getProductIdcolumn;
	}


	public void setGetProductIdcolumn(
			TextColumn<ProductGroupList> getProductIdcolumn) {
		this.getProductIdcolumn = getProductIdcolumn;
	}


	public TextColumn<ProductGroupList> getGetCodecolumn() {
		return getCodecolumn;
	}


	public void setGetCodecolumn(TextColumn<ProductGroupList> getCodecolumn) {
		this.getCodecolumn = getCodecolumn;
	}


	public TextColumn<ProductGroupList> getGetNamecolumn() {
		return getNamecolumn;
	}


	public void setGetNamecolumn(TextColumn<ProductGroupList> getNamecolumn) {
		this.getNamecolumn = getNamecolumn;
	}


	public TextColumn<ProductGroupList> getGetQuantitycolumn() {
		return getQuantitycolumn;
	}


	public void setGetQuantitycolumn(
			TextColumn<ProductGroupList> getQuantitycolumn) {
		this.getQuantitycolumn = getQuantitycolumn;
	}


	public TextColumn<ProductGroupList> getGetUnitcolumn() {
		return getUnitcolumn;
	}


	public void setGetUnitcolumn(TextColumn<ProductGroupList> getUnitcolumn) {
		this.getUnitcolumn = getUnitcolumn;
	}


	public void setGetProductGroupIdcolumn(
			TextColumn<ProductGroupList> getProductGroupIdcolumn) {
		this.getProductGroupIdcolumn = getProductGroupIdcolumn;
	}


	@Override
	protected void initializekeyprovider() {
		
	}
	@Override
	public void addFieldUpdater() {
		
	}
	@Override
	public void setEnable(boolean state) {
		
	}
	@Override
	public void applyStyle() {
		
	}

	
}
