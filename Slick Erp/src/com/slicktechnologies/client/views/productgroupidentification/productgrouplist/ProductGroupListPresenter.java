package com.slicktechnologies.client.views.productgroupidentification.productgrouplist;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.view.client.NoSelectionModel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreen;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreenPresenter;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.device.DeviceForm;
import com.slicktechnologies.client.views.device.DevicePresenter;
import com.slicktechnologies.client.views.productgroupidentification.productgroupdetails.ProductGroupDetailsForm;
import com.slicktechnologies.client.views.productgroupidentification.productgroupdetails.ProductGroupDetailsPresenter;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
import com.slicktechnologies.shared.common.productgroup.ProductGroupDetails;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;

public class ProductGroupListPresenter extends TableScreenPresenter<ProductGroupList>{

		TableScreen<ProductGroupList>form;
		
		CsvServiceAsync csvservice=GWT.create(CsvService.class);
		final GenricServiceAsync async=GWT.create(GenricService.class);
	
	public ProductGroupListPresenter(TableScreen<ProductGroupList> view,ProductGroupList model) {
		super(view, model);
		form=view;
//		view.retriveTable(getServiceQuery());
		setTableSelectionOnService();
	    form.getSearchpopupscreen().getDwnload().setVisible(false);
	    
	    boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.PRODUCTGROUPLIST,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}

	public ProductGroupListPresenter(TableScreen<ProductGroupList> view,ProductGroupList model,MyQuerry querry){
	
		super(view, model);
		form=view;
		view.retriveTable(querry);
		setTableSelectionOnService();
		
		 boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.PRODUCTGROUPLIST,LoginPresenter.currentModule.trim());
			if(isDownload==false){
				form.getSearchpopupscreen().getDwnload().setVisible(false);
			}
	}
	
	private void setTableSelectionOnService() {
		
		 final NoSelectionModel<ProductGroupList> selectionModelMyObj = new NoSelectionModel<ProductGroupList>();
	     SelectionChangeEvent.Handler  tableHandler = new SelectionChangeEvent.Handler() 
	     {
	         @Override
	         public void onSelectionChange(SelectionChangeEvent event) 
	         {
	        	 final ProductGroupList entity=selectionModelMyObj.getLastSelectedObject();
	        	 AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
	        	 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Product/Product Group Details",Screen.PRODUCTGROUPDETAILS);
	        	 final ProductGroupDetailsForm form = ProductGroupDetailsPresenter.initialize();
	        	
	        	 AppMemory.getAppMemory().stickPnel(form);
	        	 form.showWaitSymbol();
	        	
				 Timer timer=new Timer() 
	        	 {
	 				@Override
	 				public void run() {
	 					
	 					MyQuerry querry=new MyQuerry();
	 					Vector<Filter> filtervec=new Vector<Filter>();
	 					Filter temp=null;
	 					
	 					temp=new Filter();
	 					temp.setQuerryString("companyId");
	 					temp.setLongValue(entity.getCompanyId());
	 					filtervec.add(temp);
	 					
	 					temp=new Filter();
	 					temp.setQuerryString("count");
	 					temp.setIntValue(entity.getProductGroupId());
	 					filtervec.add(temp);
	 					
	 					querry.setFilters(filtervec);
	 					querry.setQuerryObject(new ProductGroupDetails());
	 					
	 					async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {

	 						@Override
	 						public void onFailure(Throwable caught) {
	 							form.showDialogMessage("An Unexpected Error occured !");
	 							
	 						}
	 						@Override
	 						public void onSuccess(ArrayList<SuperModel> result) {
	 							
	 							
	 							for(SuperModel pmodel:result)
	 							{
	 								ProductGroupDetails pgi=(ProductGroupDetails)pmodel;
	 								form.updateView(pgi);
	 								form.setToViewState();
	 							}
	 							
	 						}
	 					});
	 					
	 					form.hideWaitSymbol();
	 				}
	 			};
	             timer.schedule(3000); 
	          }
	     };
	     
	     // Add the handler to the selection model
	     selectionModelMyObj.addSelectionChangeHandler( tableHandler );
	     // Add the selection model to the table
	     form.getSuperTable().getTable().setSelectionModel(selectionModelMyObj);
	}
	@Override
	 public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();
		
		if(text.equals(""))
        reactTo();
	}

	@Override
	public void reactOnPrint() {
		
	}

	@Override
	public void reactOnEmail() {
		
	}
	
	@Override
	public void reactOnDownload() {
		CsvServiceAsync async=GWT.create(CsvService.class);
		ArrayList<ProductGroupList> custarray=new ArrayList<ProductGroupList>();
		List<ProductGroupList> list=form.getSuperTable().getListDataProvider().getList();
		
		custarray.addAll(list); 
		
		
		async.setProductGrpList(custarray, new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);
			}

			@Override
			public void onSuccess(Void result) {
				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "csvservlet"+"?type="+65;
				Window.open(url, "test", "enabled");
			}
		});
	}
	protected void makeNewModel(){
		
		model = new ProductGroupList();
	}
	
	public  static void initalize()
	{
		ProductGroupListTable table=new ProductGroupListTable();
		ProductGroupListForm form=new ProductGroupListForm(table);
		
		ProductGroupSearchPopUp.staticSuperTable=table;
		
		ProductGroupSearchPopUp searchPopup= new ProductGroupSearchPopUp(false);
		form.setSearchpopupscreen(searchPopup);
		ProductGroupListPresenter presenter =new ProductGroupListPresenter(form,new ProductGroupList());
		form.setToNewState();
		AppMemory.getAppMemory().stickPnel(form);
	}
	
	public static void initalize(MyQuerry querry)
	{
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Product Group List",Screen.PRODUCTGROUPDETAILS);
		ProductGroupListTable table= new ProductGroupListTable();
		ProductGroupListForm form=new ProductGroupListForm(table);
		ProductGroupSearchPopUp.staticSuperTable=table;
		ProductGroupSearchPopUp searchPopUp=new ProductGroupSearchPopUp(false);
		form.setSearchpopupscreen(searchPopUp);
		ProductGroupListPresenter presenter =new ProductGroupListPresenter(form, new ProductGroupList(),querry);
		form.setToNewState();
		AppMemory.getAppMemory().stickPnel(form);
	}
	private MyQuerry getServiceQuery() {
		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new ProductGroupList());
		return quer;
		
	}
	private void reactTo() {
		
	}
}
