package com.slicktechnologies.client.views.productgroupidentification.productgroupdetails;

import java.util.Vector;

import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.client.views.productgroupidentification.productgroupdetails.ProductGroupDetailsPresenter.ProductGroupPresenterSearch;
import com.slicktechnologies.shared.common.productgroup.ProductGroupDetails;

public class ProductGroupDetailsPresenterSearch extends ProductGroupPresenterSearch{
    
	public IntegerBox tbProductGroupId,tbProductId;
	public TextBox tbCode,tbName;
	
	
	public ProductGroupDetailsPresenterSearch(){
		
		super();
		createGui();
	}
	
	public void initWidget(){
		
		tbProductGroupId = new IntegerBox();
		tbProductId = new IntegerBox();
		tbCode = new TextBox();
		tbName = new TextBox();
	}

	public void createScreen(){
		initWidget();
		FormFieldBuilder builder;
		
		builder = new FormFieldBuilder("ProductGroupId",tbProductGroupId);
		FormField ftbProductGroupId =builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Product ID",tbProductId);
		FormField ftbProductId=builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	
		builder = new FormFieldBuilder("Code",tbCode);
		FormField ftbCode= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Name",tbName);
		FormField ftbName= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		this.fields= new FormField[][]{
				{},
				{ftbProductGroupId,ftbProductId,ftbCode,ftbName},
				};
		}

		
		@Override
	public MyQuerry getQuerry() {
		// TODO Auto-generated method stub

			Vector<Filter> filtervec=new Vector<Filter>();
			Filter temp=null;
				
			if (tbProductGroupId.getValue()!=null){
			temp =new Filter();
			temp.setIntValue(tbProductGroupId.getValue());
			temp.setQuerryString("count");
			filtervec.add(temp);
			}
			
			if (tbProductId.getValue()!=null){
				temp=new Filter();
				temp.setIntValue(tbProductId.getValue());
				temp.setQuerryString("pitems.product_id");
				filtervec.add(temp);
			}
			
			if (!tbCode.getValue().equals("")){
				temp=new Filter();
				temp.setStringValue(tbCode.getValue());
				temp.setQuerryString("pitems.code");
				filtervec.add(temp);
			}
			if(!tbName.getValue().equals("")){
				temp=new Filter();
				temp.setStringValue(tbName.getValue());
				temp.setQuerryString("pitems.name");
				filtervec.add(temp);
			}
			
			MyQuerry querry = new MyQuerry();
			querry.setFilters(filtervec);
			querry.setQuerryObject(new ProductGroupDetails());
			return querry;
		}

		@Override
		public boolean validate() {
			// TODO Auto-generated method stub
			return true;
		}
		}
