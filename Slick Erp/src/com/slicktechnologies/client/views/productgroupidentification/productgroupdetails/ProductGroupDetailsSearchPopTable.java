package com.slicktechnologies.client.views.productgroupidentification.productgroupdetails;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.slicktechnologies.client.views.productgroupidentification.productgroupdetails.ProductGroupDetailsPresenter.ProductGroupDetailsPresenterTable;
import com.slicktechnologies.shared.common.productgroup.ProductGroupDetails;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;

public class ProductGroupDetailsSearchPopTable extends ProductGroupDetailsPresenterTable{
	


	TextColumn<ProductGroupDetails>getProductIdcolumn;
	TextColumn<ProductGroupDetails>getCodecolumn;
	TextColumn<ProductGroupDetails>getNamecolumn;
	TextColumn<ProductGroupDetails>getQuantitycolumn;
	TextColumn<ProductGroupDetails>getUnitcolumn;
	TextColumn<ProductGroupDetails> viewProductIdcolumn,viewCodecolumn,viewNamecolumn,viewQuantitycolumn,viewUnitcolumn;
	TextColumn<ProductGroupDetails> viewGroupTitlecolumn,getGroupDetailTitleColumn;
	
	public ProductGroupDetailsSearchPopTable(){
		super();
	}
	
	public Object getVarRef(String varName)
	{
		if(varName.equals("getProductIdcolumn"))
			return this.getProductIdcolumn;
		if(varName.equals("viewGroupTitlecolumn"))
			return this.viewGroupTitlecolumn;
		return null ;
	}
	
	public void createTable(){
		addColumngetProductId();
		addColumngetName();
	}
	
	
	protected void addColumngetProductId()
	{
		getProductIdcolumn=new TextColumn<ProductGroupDetails>()
			{
			@Override
			public String getValue(ProductGroupDetails object)
			{
				return object.getCount()+"";
			}
				};
				table.addColumn(getProductIdcolumn,"Product Id");
	}
	
	protected void addColumngetName()
	{
		getNamecolumn=new TextColumn<ProductGroupDetails>() {
			public String getValue(ProductGroupDetails object)
			{
				return object.getTitle()+"";
			}
		};
		table.addColumn(getNamecolumn,"Name" );
	}
	
	
	
	

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	public void addColumnSorting(){
	}




	@Override
	public void applyStyle() {
		
	}

}
