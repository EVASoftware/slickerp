package com.slicktechnologies.client.views.productgroupidentification.productgroupdetails;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;

public class ProductGroupDetailsPresenterTableProxy extends SuperTable<ProductGroupList>{

	TextColumn<ProductGroupList>getProductIdcolumn;
	TextColumn<ProductGroupList>getCodecolumn;
	TextColumn<ProductGroupList>getNamecolumn;
	TextColumn<ProductGroupList>getQuantitycolumn;
	TextColumn<ProductGroupList>getUnitcolumn;
	private Column<ProductGroupList,String>Delete;
	TextColumn<ProductGroupList> viewProductIdcolumn,viewCodecolumn,viewNamecolumn,viewQuantitycolumn,viewUnitcolumn;
	
	public ProductGroupDetailsPresenterTableProxy(){
		super();
	}
	
	public void createTable(){
		addColumngetProductId();
		addColumngetCode();
		addColumngetName();
		addColumngetQuantity();
		addColumngetUnit();
		addColumnDelete();
		setFieldUpdaterOnDelete();
	}
	
	
	private void addColumnDelete() {
		ButtonCell btnCell= new ButtonCell();
		Delete = new Column<ProductGroupList, String>(btnCell) {

			@Override
			public String getValue(ProductGroupList object) {

				return "Delete";
			}
		};
		table.addColumn(Delete,"Delete");
	}

	
	private void setFieldUpdaterOnDelete() {
		Delete.setFieldUpdater(new FieldUpdater<ProductGroupList, String>() {
			@Override
			public void update(int index, ProductGroupList object, String value) {
				getDataprovider().getList().remove(index);
			}
		});
	}
	
	protected void addColumngetProductId()
	{
		getProductIdcolumn=new TextColumn<ProductGroupList>()
			{
			@Override
			public String getValue(ProductGroupList object)
			{
				return object.getProduct_id()+"";
			}
				};
				table.addColumn(getProductIdcolumn,"Product Id");
	}
	
	protected void addColumngetCode()
	{
		getCodecolumn=new TextColumn<ProductGroupList>() {
			public String getValue(ProductGroupList object)
			{
				return object.getCode()+"";
			}
		};
		table.addColumn(getCodecolumn,"Code" );
	}
	protected void addColumngetName()
	{
		getNamecolumn=new TextColumn<ProductGroupList>() {
			public String getValue(ProductGroupList object)
			{
				return object.getName()+"";
			}
		};
		table.addColumn(getNamecolumn,"Name" );
	}
	
	
	protected void addColumngetQuantity()
	{
		getQuantitycolumn=new TextColumn<ProductGroupList>() {
			public String getValue(ProductGroupList object)
			{
				return object.getQuantity()+"";
			}
		};
		table.addColumn(getQuantitycolumn,"Quantity" );
	}
	protected void addColumngetUnit()
	{
		getUnitcolumn=new TextColumn<ProductGroupList>() {
			public String getValue(ProductGroupList object)
			{
				return object.getUnit()+"";
			}
		};
		table.addColumn(getUnitcolumn,"UOM" );
	}
	
	

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		
		for(int i=table.getColumnCount()-1;i>-1;i--)
	    	  table.removeColumn(i); 
	        	  
	      
	          if(state==true){
	        	  
	        	addColumngetProductId();
	      		addColumngetCode();
	      		addColumngetName();
	      		addColumngetQuantity();
	      		addColumngetUnit();
	      		addColumnDelete();
	      		setFieldUpdaterOnDelete();
	          }
	          else{
	        	  viewCreateProductIdcolumn();
	        	  viewCreateCodecolumn();
	        	  viewCreateNamecolumn();
	        	  viewCreateQuantitycolumn();
	        	  viewCreateUnitcolumn();
	        	  
	          }
	}

	private void viewCreateUnitcolumn() {
		
		viewUnitcolumn=new TextColumn<ProductGroupList>() {
			public String getValue(ProductGroupList object)
			{
				return object.getUnit()+"";
			}
		};
		table.addColumn(viewUnitcolumn,"UOM" );
		
	}

	private void viewCreateQuantitycolumn() {
		
		viewQuantitycolumn=new TextColumn<ProductGroupList>() {
			public String getValue(ProductGroupList object)
			{
				return object.getQuantity()+"";
			}
		};
		table.addColumn(viewQuantitycolumn,"Quantity" );
		
	}

	private void viewCreateNamecolumn() {
		
		viewNamecolumn=new TextColumn<ProductGroupList>() {
			public String getValue(ProductGroupList object)
			{
				return object.getName()+"";
			}
		};
		table.addColumn(viewNamecolumn,"Name" );
		
	}

	private void viewCreateCodecolumn() {
		
		viewCodecolumn=new TextColumn<ProductGroupList>() {
			public String getValue(ProductGroupList object)
			{
				return object.getCode()+"";
			}
		};
		table.addColumn(viewCodecolumn,"Code" );
	}

	private void viewCreateProductIdcolumn() {
		
		viewProductIdcolumn=new TextColumn<ProductGroupList>()
				{
				@Override
				public String getValue(ProductGroupList object)
				{
					return object.getProduct_id()+"";
				}
					};
					table.addColumn(viewProductIdcolumn,"Product Id");
		
	}

	@Override
	public void applyStyle() {
		
	}
}
