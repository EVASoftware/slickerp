package com.slicktechnologies.client.views.productgroupidentification.productgroupdetails;

import java.util.List;


import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.config.configurations.ConfigTypes;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
import com.slicktechnologies.shared.common.productgroup.ProductGroupDetails;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class ProductGroupDetailsForm extends FormScreen<ProductGroupDetails> implements ClickHandler{

	TextBox tbTitle,tbCode,tbName,tbUnit;
	IntegerBox tbProductId,tbSrNo;
	TextBox tbProductGroupId;
	DoubleBox dbQuantity;
    CheckBox tbStatus;
    Button addSteps;
    
    ProductGroupDetailsPresenterTableProxy stepsTable;
    ProductInfoComposite pic;
    
    ProductGroupDetails prodGrpDetObj;
    DoubleBox dbArea ;
    ObjectListBox<Config> olbBaseUnit;
    TextBox tbProUnit;
	public ProductGroupDetailsForm(){
    	super();
       createGui();
       stepsTable.connectToLocal();
	}
	
	public ProductGroupDetailsForm(String[] processlevel,FormField[][] fields,FormStyle formstyle){
		
		super(processlevel,fields,formstyle);
		createGui();
		
	}	
		
	private void initalizeWidget(){
		
		tbProductGroupId=new TextBox();
		tbProductGroupId.setEnabled(false);
		tbProductId=new IntegerBox();
	
		tbTitle=new TextBox();
		tbStatus=new CheckBox();
		tbCode=new TextBox();
		tbName=new TextBox();
		tbUnit=new TextBox();
		dbQuantity=new DoubleBox();
		addSteps=new Button("Add");
		addSteps.addClickHandler(this);
		stepsTable=new ProductGroupDetailsPresenterTableProxy();
		pic=AppUtility.initiateSalesProductComposite(new SuperProduct());
		/**
		 * nidhi
		 * 10-09-2018
		 */
		olbBaseUnit = new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbBaseUnit, Screen.SERVICEUOM);
		tbProUnit  = new TextBox();
		tbProUnit.setEnabled(false);
		dbArea = new DoubleBox();
	
	}
	
	@Override
	public void createScreen() {

	initalizeWidget();	
	
	this.processlevelBarNames = new String[]{"New"};
	
	FormFieldBuilder fbuilder = new FormFieldBuilder();
	FormField fMaterialGroup=fbuilder.setlabel("Material Group").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
	
	fbuilder = new FormFieldBuilder("Product Group ID",tbProductGroupId);
	FormField ftbProductGroupId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	
	fbuilder = new FormFieldBuilder("* Title",tbTitle);
	FormField ftbTitle= fbuilder.setMandatory(true).setMandatoryMsg("Product Title is mandatory!!!").setRowSpan(0).setColSpan(0).build();
	
	fbuilder = new FormFieldBuilder("Quantity",dbQuantity);
	FormField fdbQuantity= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	
	fbuilder = new FormFieldBuilder("Status",tbStatus);
	FormField ftbStatus= fbuilder.setMandatory(true).setMandatoryMsg("Status is mandetory").setRowSpan(0).setColSpan(0).build();
	
	
	fbuilder = new FormFieldBuilder("",addSteps);
	FormField faddStep= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	
	fbuilder = new FormFieldBuilder("",stepsTable.getTable());
	FormField fstepsTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(5).build();
	
	fbuilder = new FormFieldBuilder("",pic);
	FormField fcom =fbuilder.setMandatory(false).setRowSpan(0).setColSpan(5).build();
	
	fbuilder = new FormFieldBuilder("* Area",dbArea);
	FormField fdbArea =fbuilder.setMandatory(false).setRowSpan(0).setMandatoryMsg("Area of Product is mandatory!!!").setColSpan(0).build();
	
	fbuilder = new FormFieldBuilder("Product Unit",tbProUnit);
	FormField ftbProUnit =fbuilder.setMandatory(false).setRowSpan(0).setMandatoryMsg("Area of Product is mandatory!!!").setColSpan(0).build();
	
	fbuilder = new FormFieldBuilder("* Unit",olbBaseUnit);
	FormField folbBaseUnit =fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).setMandatoryMsg("Unit of area is mandatory!!!").build();
	FormField [][] formFields ={
			{fMaterialGroup},
			{ftbProductGroupId,ftbTitle,ftbStatus},
			{fdbArea,folbBaseUnit},
            {fcom},
            {fdbQuantity,ftbProUnit,faddStep},
            {fstepsTable}
           };
	this.fields=formFields;
	tbStatus.setValue(Boolean.valueOf(true));
	}
	
	
	@Override
	public void updateModel(ProductGroupDetails model) {
		if(tbTitle.getValue()!=null)
			model.setTitle(tbTitle.getValue());	
		
		if(tbStatus.getValue()!=null)
			model.setStatus(tbStatus.getValue());
		 
		model.setPitems(stepsTable.getValue());
		
		model.setArea(dbArea.getValue());
		model.setUnit(olbBaseUnit.getValue());
		prodGrpDetObj=model;
	}

	@Override
	public void updateView(ProductGroupDetails view) {
		
		prodGrpDetObj=view;
		
		if(view.getTitle()!=null){
			tbTitle.setValue(view.getTitle());
			}
			stepsTable.setValue(view.getPitems());
			
			tbProductGroupId.setValue(view.getCount()+"");
		
			if(view.getStatus()!=null){
				tbStatus.setValue(view.getStatus());
			}
			
			dbArea.setValue(view.getArea());
			olbBaseUnit.setValue(view.getUnit());
	}
	

	/**
	 * Toggles the app header bar menus as per screen state
	 */

	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION) ||text.equals("Search"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION)||text.equals("Search"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit")||text.equals(AppConstants.NAVIGATION)||text.equals("Search"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		
		AuthorizationHelper.setAsPerAuthorization(Screen.PRODUCTGROUP,LoginPresenter.currentModule.trim());
	}
	
	public void setCount(int count)
	{
		tbProductGroupId.setValue(count+"");
		
	}
	
	
	
	@Override
	public void onClick(ClickEvent event) {
	
	if(event.getSource().equals(addSteps)){
		
	
		if(!validateProduct()){
			showDialogMessage("You Can Not Add Duplicate Data");
		}
//		if(!validateDecimal()){
//			showDialogMessage("You Can Not Add More Than 2 Digit After Decimal in Quantity ");
//		}
		
		if(!validateQuantityBox())
		{
			showDialogMessage("Please enter appropriate quantity required!");
		}
		
		if(!validateQuantity()){
			showDialogMessage("Quantity can not be zero(i.e '0')");
		}
		
		if(validateProduct()&&validateQuantity()&&validateQuantityBox()){
			
			ProductGroupList pgdEntity=new ProductGroupList();	
			
			if(tbProductId.getValue()!= null||tbCode.getValue()!=null||tbName.getValue()!=null||dbQuantity.getValue()!=0||tbUnit!=null){
				
				pgdEntity.setCode(pic.getProdCode().getValue());
				pgdEntity.setName(pic.getProdName().getValue());
				pgdEntity.setQuantity(dbQuantity.getValue());
				pgdEntity.setUnit(pic.getUnitOfmeasuerment().getValue());
				pgdEntity.setProduct_id(Integer.parseInt(pic.getProdID().getValue()));
				
				stepsTable.getDataprovider().getList().add(pgdEntity);
				
			}	
		}
	}
}

	@Override
	public boolean validate() {
		
		boolean superres = super.validate();
		boolean tableval = true , tbArea  = true, olUnit  = true;
		if(stepsTable.getDataprovider().getList().size()==0){
			showDialogMessage("Please add atleast one data");
			
			 tableval= false;
		}
		if(dbArea.getValue() ==null || dbArea.getValue() == 0){
			showDialogMessage("Please add Area");
			tbArea  = false;
		}
		if(olbBaseUnit.getSelectedIndex()==0){
			showDialogMessage("Please select Unit.");
			olUnit = false;
		}
		return (tableval&&superres && tbArea && olUnit);
	}
	
	public boolean validateDecimal(){

		NumberFormat nf = NumberFormat.getFormat("0.00"); 
		if(Double.parseDouble(nf.format(dbQuantity.getValue()))== dbQuantity.getValue()){
			return true;
		}
		return false;
	} 
	
	
	public boolean validateProduct(){
		
		List<ProductGroupList>list= stepsTable.getDataprovider().getList();
		int prodId=Integer.parseInt(pic.getProdID().getValue());
		for(ProductGroupList temp:list){
			if(prodId==temp.getProduct_id()){
				return false;
			}
		}
		return true;
	}

	
	public boolean validateQuantity(){
	
		double Qty = dbQuantity.getValue();
		
		if(Qty==0){
			return false;
		}
		return true;
	}
	
	
	public boolean validateQuantityBox()
	{
		if(dbQuantity.getValue().equals("")||dbQuantity.getValue()==0){
			return false;
		}
		return true;
	}
	
	
	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		dbArea.setEnabled(state);
		olbBaseUnit.setEnabled(state);
		tbProductGroupId.setEnabled(false);
		this.stepsTable.setEnable(state);
	}
	
	@Override
	public void setToEditState() {
		super.setToEditState();
		
		this.processLevelBar.setVisibleFalse(false);
	}
	
	@Override
	public void setToViewState() {
		super.setToViewState();
		
		SuperModel model=new ProductGroupDetails();
		model=prodGrpDetObj;
		AppUtility.addDocumentToHistoryTable(AppConstants.PRODUCTMODULE,AppConstants.PRODUCTGROUPDETAILS, prodGrpDetObj.getCount(), null,null,null, false, model, null);
	
	}
	
	//************getters and setters************
	
	public TextBox getTbTitle() {
		return tbTitle;
	}
	public void setTbTitle(TextBox tbTitle) {
		this.tbTitle = tbTitle;
	}
	public CheckBox getTbStatus() {
		return tbStatus;
	}
	public void setTbStatus(CheckBox tbStatus) {
		this.tbStatus = tbStatus;
	}
	public TextBox getTbCode() {
		return tbCode;
	}
	public void setTbCode(TextBox tbCode) {
		this.tbCode = tbCode;
	}
	public TextBox getTbName() {
		return tbName;
	}
	public void setTbName(TextBox tbName) {
		this.tbName = tbName;
	}
	public TextBox getTbUnit() {
		return tbUnit;
	}
	public void setTbUnit(TextBox tbUnit) {
		this.tbUnit = tbUnit;
	}
	
	public TextBox getTbProductGroupId() {
		return tbProductGroupId;
	}

	public void setTbProductGroupId(TextBox tbProductGroupId) {
		this.tbProductGroupId = tbProductGroupId;
	}

	public IntegerBox getTbProductId() {
		return tbProductId;
	}
	public void setTbProductId(IntegerBox tbProductId) {
		this.tbProductId = tbProductId;
	}
	public DoubleBox getDbQuantity() {
		return dbQuantity;
	}
	public void setDbQuantity(DoubleBox dbQuantity) {
		this.dbQuantity = dbQuantity;
	}

	public ProductGroupDetailsPresenterTableProxy getStepsTable() {
		return stepsTable;
	}

	public void setStepsTable(ProductGroupDetailsPresenterTableProxy stepsTable) {
		this.stepsTable = stepsTable;
	}

	public IntegerBox getTbSrNo() {
		return tbSrNo;
	}

	public void setTbSrNo(IntegerBox tbSrNo) {
		this.tbSrNo = tbSrNo;
	}

	public Button getAddSteps() {
		return addSteps;
	}

	public void setAddSteps(Button addSteps) {
		this.addSteps = addSteps;
	}

	public ProductInfoComposite getPic() {
		return pic;
	}

	public void setPic(ProductInfoComposite pic) {
		this.pic = pic;
	}

	public ProductGroupDetails getProdGrpDetObj() {
		return prodGrpDetObj;
	}

	public void setProdGrpDetObj(ProductGroupDetails prodGrpDetObj) {
		this.prodGrpDetObj = prodGrpDetObj;
	}

	public DoubleBox getDbArea() {
		return dbArea;
	}

	public void setDbArea(DoubleBox dbArea) {
		this.dbArea = dbArea;
	}

	public ObjectListBox<Config> getOlbBaseUnit() {
		return olbBaseUnit;
	}

	public void setOlbBaseUnit(ObjectListBox<Config> olbBaseUnit) {
		this.olbBaseUnit = olbBaseUnit;
	}

	public TextBox getTbProUnit() {
		return tbProUnit;
	}

	public void setTbProUnit(TextBox tbProUnit) {
		this.tbProUnit = tbProUnit;
	}
	
}