package com.slicktechnologies.client.views.productgroupidentification.productgroupdetails;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.products.serviceproduct.ServiceProductPresenterSearchProxy;
import com.slicktechnologies.client.views.products.serviceproduct.ServiceproductPresenterTableProxy;
import com.slicktechnologies.client.views.products.serviceproduct.ServiceproductPresenter.ServiceproductPresenterSearch;
import com.slicktechnologies.client.views.products.serviceproduct.ServiceproductPresenter.ServiceproductPresenterTable;
import com.slicktechnologies.shared.common.productgroup.ProductGroupDetails;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class ProductGroupDetailsPresenter extends FormScreenPresenter<ProductGroupDetails> implements ClickHandler, SelectionHandler<Suggestion> {

	ProductGroupDetailsForm form;

	public ProductGroupDetailsPresenter(FormScreen<ProductGroupDetails> view,ProductGroupDetails model) {
		super(view, model);
		form =(ProductGroupDetailsForm)view;
		form.setPresenter(this);
		form.getPic().getProdID().addSelectionHandler(this);
		form.getPic().getProdName().addSelectionHandler(this);
		form.getPic().getProdCode().addSelectionHandler(this);
		 boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.PRODUCTGROUPDETAILS,LoginPresenter.currentModule.trim());
			if(isDownload==false){
				form.getSearchpopupscreen().getDwnload().setVisible(false);
			}
		
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		
			InlineLabel lbl= (InlineLabel) e.getSource();
			
			if(lbl.getText().contains("New"))
			{
				form.setToNewState();
				this.initialize();
		}
		}
		

	@Override
	public void reactOnPrint() {
		
	}

	@Override
	public void reactOnEmail() {
		
	}

	@Override
	protected void makeNewModel() {
		model= new ProductGroupDetails();
	}
	
	public static ProductGroupDetailsForm initialize(){
		System.out.println("in side product group");
		ProductGroupDetailsForm form = new ProductGroupDetailsForm();
		
		ProductGroupDetailsPresenterTable gentableScreen=new ProductGroupDetailsSearchPopTable();
		gentableScreen.setView(form);
		gentableScreen.applySelectionModle();
		ProductGroupPresenterSearch.staticSuperTable=gentableScreen;
		ProductGroupPresenterSearch searchpopup=new ProductGroupDetailsPresenterSearch();
		form.setSearchpopupscreen(searchpopup);
		
		ProductGroupDetailsPresenter presenter=new ProductGroupDetailsPresenter(form, new ProductGroupDetails());		
		AppMemory.getAppMemory().stickPnel(form);
		return form;		 
	}
	
	@Override
	public void onClick(ClickEvent event) {
		super.onClick(event);
	}

	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.productgroup.ProductGroupIdentification")
	public static  class ProductGroupPresenterSearch extends SearchPopUpScreen<ProductGroupDetails>{

		@Override
		public MyQuerry getQuerry() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public boolean validate() {
			// TODO Auto-generated method stub
			return true;
		}}

	
	
	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.productgroup.ProductGroupIdentification")
	 public static class  ProductGroupDetailsPresenterTable extends SuperTable<ProductGroupDetails> implements GeneratedVariableRefrence{

		@Override
		public Object getVarRef(String varName) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void createTable() {
			// TODO Auto-generated method stub
			
		}

		@Override
		protected void initializekeyprovider() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void addFieldUpdater() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void setEnable(boolean state) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void applyStyle() {
			// TODO Auto-generated method stub
			
		}} ;
	
	
	
	@Override
	public void onSelection(SelectionEvent<Suggestion> event) {
		// TODO Auto-generated method stub
		if (event.getSource().equals(form.getPic().getProdID())
				|| event.getSource().equals(
						form.getPic().getProdCode())
				|| event.getSource().equals(
						form.getPic().getProdName())) {
			form.getTbProUnit().setValue(form.getPic().getUnitOfmeasuerment().getValue());
		}
	};
	
	
	
	}
	
