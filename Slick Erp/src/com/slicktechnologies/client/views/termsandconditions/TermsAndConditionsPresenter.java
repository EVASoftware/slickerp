package com.slicktechnologies.client.views.termsandconditions;

import java.util.Vector;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.views.smstemplate.SmsTemplateForm;
import com.slicktechnologies.client.views.smstemplate.SmsTemplatePresenter;
import com.slicktechnologies.client.views.smstemplate.SmsTemplateTable;
import com.slicktechnologies.shared.common.helperlayer.Declaration;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
import com.slicktechnologies.shared.common.helperlayer.TermsAndConditions;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class TermsAndConditionsPresenter extends FormTableScreenPresenter<TermsAndConditions> {

	
	
	TermsAndConditionsForm form;
	
	
	public TermsAndConditionsPresenter(FormTableScreen<TermsAndConditions> view, TermsAndConditions model){
		super(view,model);
		form=(TermsAndConditionsForm) view;
		form.getSupertable().connectToLocal();
		form.retriveTable(getDectrationTemplates());
		form.setPresenter(this);
//		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.SMSTEMPLATE,LoginPresenter.currentModule.trim());
//		if(isDownload==false){
//			form.getSearchpopupscreen().getDwnload().setVisible(false);
//		}
	}
	
	
	
	
	private MyQuerry getDectrationTemplates() {
		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new TermsAndConditions());
		return quer;
	}




	public static void initalize() {
		
		TermsAndConditionsTable gentableScreen=new TermsAndConditionsTable();
		
		TermsAndConditionsForm  form=new  TermsAndConditionsForm(gentableScreen,FormTableScreen.UPPER_MODE,true);
		gentableScreen.setView(form);
		gentableScreen.applySelectionModle();
		
		
		TermsAndConditionsPresenter  presenter=new  TermsAndConditionsPresenter(form,new TermsAndConditions());
		AppMemory.getAppMemory().stickPnel(form);
		
	}


	/**
	 * Method template to set the processBar events
	 */

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {

		InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();
		
		if(text.equals("New"))
		{
			form.setToNewState();
			initalize();
		}	
	}




	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}




	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}


	/*
	 * Method template to set Myquerry object
	 */
	public MyQuerry getsmsConfigQuery()
	{
		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new TermsAndConditions());
		return quer;
	}
	

	@Override
	protected void makeNewModel() {
		model=new TermsAndConditions();

	}
	
	public void setModel(TermsAndConditions entity)
	{
		model=entity;
	}


}

