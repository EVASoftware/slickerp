package com.slicktechnologies.client.views.termsandconditions;

import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
import com.slicktechnologies.shared.common.helperlayer.TermsAndConditions;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class TermsAndConditionsForm extends FormTableScreen<TermsAndConditions>{

	
	TextBox tbTitle;
	CheckBox chkboxStatus;
	TextArea taMessage;
	TextBox ibId;
	ObjectListBox<Type> olbDocumentName;
	IntegerBox sequenceNo;
	
	public TermsAndConditionsForm  (SuperTable<TermsAndConditions> table, int mode,
			boolean captionmode) {
		super(table, mode, captionmode);
		createGui();

	}
	
	
	private void initalizeWidget(){
		
		tbTitle = new TextBox();
		chkboxStatus = new CheckBox();
		chkboxStatus.setValue(true);
		taMessage = new TextArea();
		taMessage.setVisibleLines(15);
		
		ibId = new TextBox();
		ibId.setEnabled(false);
		olbDocumentName = new ObjectListBox<Type>();
		olbDocumentName.addItem("Contract");
		olbDocumentName.addItem("Quotation");
		olbDocumentName.addItem("SalesQuotation");
		sequenceNo=new IntegerBox();
	}
	
	
	@Override
	public void createScreen() {
		
		initalizeWidget();
		
		
		this.processlevelBarNames=new String[]{"New"};
		
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingMessageInformation=fbuilder.setlabel("Terms and Conditions Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		fbuilder = new FormFieldBuilder("ID",ibId);
		FormField fibId = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Title",tbTitle);
		FormField ftbEvent= fbuilder.setMandatory(true).setMandatoryMsg("Title is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Message",taMessage);
		FormField ftaMessage = fbuilder.setMandatory(true).setMandatoryMsg("Message is Mandatory!").setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Status",chkboxStatus);
		FormField fchkbxStatus = fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Document",olbDocumentName);
		FormField folbDocumentName = fbuilder.setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Sequence No.",sequenceNo);
		FormField fsequenceNo = fbuilder.setRowSpan(0).setColSpan(0).build();
		
		FormField[][] formfield = { {fgroupingMessageInformation},
				  {fibId,folbDocumentName,fsequenceNo,ftbEvent,fchkbxStatus},
				  {ftaMessage},
				  };

				this.fields=formfield;		
		
	}
	
	
	@Override
	public void updateModel(TermsAndConditions model) 
	{
		if(tbTitle.getValue()!=null)
			model.setTitle(tbTitle.getValue());
		if(taMessage.getValue()!=null)
			model.setMsg(taMessage.getValue());
		model.setStatus(chkboxStatus.getValue());
		if(sequenceNo.getValue()!=null&&sequenceNo.getValue()>0)
			model.setSequenceNumber(sequenceNo.getValue());
		else
			model.setSequenceNumber(1);
		
		
		model.setDocument(olbDocumentName.getValue(olbDocumentName.getSelectedIndex()));
			
			presenter.setModel(model);
	}
	
	
	@Override
	public void updateView(TermsAndConditions view) 
	{
		if(view.getTitle()!=null)
			tbTitle.setValue(view.getTitle());
		if(view.getMsg()!=null)
			taMessage.setValue(view.getMsg());
		ibId.setValue(view.getCount()+"");
		chkboxStatus.setValue(view.getStatus());
		if(view.getDocument()!=null)
			olbDocumentName.setValue(view.getDocument());
		sequenceNo.setValue(view.getSequenceNumber());
		
		presenter.setModel(view);

	}	
	
	
	/**
	 * Toggles the app header bar menus as per screen state
	 */

	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.SMSTEMPLATE,LoginPresenter.currentModule.trim());
	}

	/**
	 * sets the id textbox with the passed count value. 
	 */
	@Override
	public void setCount(int count)
	{
		ibId.setValue(count+"");	
	}

	
	
	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		ibId.setEnabled(false);
	}

	

	//******************getters and setters *********************
	
	
	public TextBox getTbTitle() {
		return tbTitle;
	}
	public void setTbTitle(TextBox tbTitle) {
		this.tbTitle = tbTitle;
	}
	public CheckBox getChkboxStatus() {
		return chkboxStatus;
	}
	public void setChkboxStatus(CheckBox chkboxStatus) {
		this.chkboxStatus = chkboxStatus;
	}
	public TextArea getTaDeclarationMessag() {
		return taMessage;
	}
	public void setTaDeclarationMessag(TextArea taDeclarationMessag) {
		this.taMessage = taDeclarationMessag;
	}


	@Override
	public void setToNewState() {
		// TODO Auto-generated method stub
		super.setToNewState();
		chkboxStatus.setValue(true);
	}
	
	
	
}

