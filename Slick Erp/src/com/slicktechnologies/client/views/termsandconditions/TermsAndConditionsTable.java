package com.slicktechnologies.client.views.termsandconditions;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.helperlayer.TermsAndConditions;

public class TermsAndConditionsTable extends SuperTable<TermsAndConditions>{

	TextColumn<TermsAndConditions> getCountColumn;
	TextColumn<TermsAndConditions> getDocumentColumn;
	TextColumn<TermsAndConditions> getSequenceNoColumn;
	TextColumn<TermsAndConditions> getTitleColumn;
	TextColumn<TermsAndConditions> getTermsAndConditionsMessageColumn;
	TextColumn<TermsAndConditions> getStatusColumn;
	
	
	public TermsAndConditionsTable()
	{
		super();
	}

	@Override
	public void createTable() {
		addColumngetCount();
		addColumnDocument();
		addColumnSequenceNo();
		addColumngetTitle();
		addColumngetTermsAndConditionsMessage();
		addColumngetStatus();
		
		
		
	}

	private void addColumngetStatus() {
		getStatusColumn = new TextColumn<TermsAndConditions>() {
			
			@Override
			public String getValue(TermsAndConditions object) {
				if(object.getStatus()==true)
					return "Active";
				else return "In Active";
						
			}
		};
		table.addColumn(getStatusColumn, "Status");
		getStatusColumn.setSortable(true);
		table.setColumnWidth(getStatusColumn, 100, Unit.PX);
		
	}


	private void addColumngetTermsAndConditionsMessage() {
		getTermsAndConditionsMessageColumn = new TextColumn<TermsAndConditions>() {
		
			@Override
			public String getValue(TermsAndConditions object) {
				return object.getMsg();
			}
		};
		table.addColumn(getTermsAndConditionsMessageColumn,"TermsAndConditions Message");
		getTermsAndConditionsMessageColumn.setSortable(true);
		table.setColumnWidth(getTermsAndConditionsMessageColumn, 100, Unit.PX);
	}

	private void addColumngetTitle() {
		getTitleColumn = new TextColumn<TermsAndConditions>() {
			
			@Override
			public String getValue(TermsAndConditions object) {
				return object.getTitle();
			}
		};
		table.addColumn(getTitleColumn,"Title");
		getTitleColumn.setSortable(true);
		table.setColumnWidth(getTitleColumn, 100, Unit.PX);
		
	}

	
	private void addColumngetCount() {

		getCountColumn = new TextColumn<TermsAndConditions>() {

			@Override
			public String getValue(TermsAndConditions object) {
				if(object.getCount()==-1)
					return "N.A";
				else return object.getCount()+"";
			}
		};
		table.addColumn(getCountColumn, "ID");
		getCountColumn.setSortable(true);
		table.setColumnWidth(getCountColumn, 100, Unit.PX);
	}

	
	private void addColumnDocument() {
		getDocumentColumn = new TextColumn<TermsAndConditions>() {
			
			@Override
			public String getValue(TermsAndConditions object) {
				if( object.getDocument()!=null)
					return object.getDocument();
				else
					return "";
			}
		};
		table.addColumn(getDocumentColumn,"Document");
		getDocumentColumn.setSortable(true);
		table.setColumnWidth(getDocumentColumn, 100, Unit.PX);
		
	}
	
	private void addColumnSequenceNo() {
		getSequenceNoColumn = new TextColumn<TermsAndConditions>() {
			
			@Override
			public String getValue(TermsAndConditions object) {
				return object.getSequenceNumber()+"";
			}
		};
		table.addColumn(getSequenceNoColumn,"Sequence No.");
		getSequenceNoColumn.setSortable(true);
		table.setColumnWidth(getSequenceNoColumn, 100, Unit.PX);
		
	}
	public void addColumnSorting(){
			addSortinggetCount();
			addSortinggetTitle();
			addSortingetTermsAndConditionsMessage();
			addSortinggetStatus();	
			addSortingDocument();
			addSortingSequenceNo();
	}
	
	
	private void addSortinggetStatus() {
			
		
		List<TermsAndConditions> list = getDataprovider().getList();
		columnSort = new ListHandler<TermsAndConditions>(list);
		columnSort.setComparator(getStatusColumn, new Comparator<TermsAndConditions>() {
			
			@Override
			public int compare(TermsAndConditions e1, TermsAndConditions e2) {

				if(e1!=null && e2!=null)
				{
					if(e1.getStatus()==e2.getStatus()){
						return 0;
					}
					else{
						return -1;
					}
				}
				else{
					return 0;
				}
				
			}
		});
		
		table.addColumnSortHandler(columnSort);
	}


	private void addSortingetTermsAndConditionsMessage() {
		List<TermsAndConditions> list = getDataprovider().getList();
		columnSort = new ListHandler<TermsAndConditions>(list);
		columnSort.setComparator(getTermsAndConditionsMessageColumn, new Comparator<TermsAndConditions>() {

			@Override
			public int compare(TermsAndConditions e1, TermsAndConditions e2) {
				
				if(e1!=null && e2!=null)
					{
						if( e1.getMsg()!=null && e2.getMsg()!=null){
							return e1.getMsg().compareTo(e2.getMsg());}
					}
					else{
						return 0;}
					return 0;
			}
		});
		
		table.addColumnSortHandler(columnSort); 
	}
	

	private void addSortinggetTitle() {

		List<TermsAndConditions> list = getDataprovider().getList();
		columnSort = new ListHandler<TermsAndConditions>(list);
		columnSort.setComparator(getTitleColumn, new Comparator<TermsAndConditions>() {

			@Override
			public int compare(TermsAndConditions e1, TermsAndConditions e2) {
				
				if(e1!=null && e2!=null)
					{
						if( e1.getTitle()!=null && e2.getTitle()!=null){
							return e1.getTitle().compareTo(e2.getTitle());}
					}
					else{
						return 0;}
					return 0;
			}
		});
		
		table.addColumnSortHandler(columnSort); 
		
	}
	private void addSortingDocument() {

		List<TermsAndConditions> list = getDataprovider().getList();
		columnSort = new ListHandler<TermsAndConditions>(list);
		columnSort.setComparator(getDocumentColumn, new Comparator<TermsAndConditions>() {

			@Override
			public int compare(TermsAndConditions e1, TermsAndConditions e2) {
				
				if(e1!=null && e2!=null)
					{
						if( e1.getDocument()!=null && e2.getDocument()!=null){
							return e1.getDocument().compareTo(e2.getDocument());}
					}
					else{
						return 0;}
					return 0;
			}
		});
		
		table.addColumnSortHandler(columnSort); 
		
	}
	
	private void addSortingSequenceNo() {

		List<TermsAndConditions> list = getDataprovider().getList();
		columnSort = new ListHandler<TermsAndConditions>(list);
		columnSort.setComparator(getSequenceNoColumn, new Comparator<TermsAndConditions>() {

			@Override
			public int compare(TermsAndConditions e1, TermsAndConditions e2) {
				
				if(e1!=null && e2!=null)
					{
						if( e1.getSequenceNumber()!=null && e2.getSequenceNumber()!=null){
							return e1.getSequenceNumber().compareTo(e2.getSequenceNumber());}
					}
					else{
						return 0;}
					return 0;
			}
		});
		
		table.addColumnSortHandler(columnSort); 
		
	}
	
	

	private void addSortinggetCount() {

		List<TermsAndConditions> list=getDataprovider().getList();
		columnSort=new ListHandler<TermsAndConditions>(list);
		columnSort.setComparator(getCountColumn, new Comparator<TermsAndConditions>()
				{
			@Override
			public int compare(TermsAndConditions e1,TermsAndConditions e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}


}
