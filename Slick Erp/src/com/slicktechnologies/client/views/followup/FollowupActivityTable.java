package com.slicktechnologies.client.views.followup;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;
import com.slicktechnologies.shared.common.paymentlayer.*;

import java.util.Comparator;

import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.google.gwt.cell.client.FieldUpdater;

import java.util.List;
import java.util.Date;

import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.EditTextCell;
import  com.google.gwt.cell.client.NumberCell;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import  com.google.gwt.cell.client.DatePickerCell;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.*;
import com.google.gwt.event.dom.client.*;
import com.slicktechnologies.shared.*;
import com.slicktechnologies.shared.common.*;
import com.slicktechnologies.shared.common.businessprocesslayer.*;
import com.slicktechnologies.shared.common.businessunitlayer.*;
import com.slicktechnologies.shared.common.helperlayer.*;
import com.slicktechnologies.shared.common.personlayer.*;
import com.slicktechnologies.shared.common.productlayer.*;


public class FollowupActivityTable extends SuperTable<Action>  
{
	TextColumn<Action> description,actionTakenColumn,statusColumn;
  Column<Action,Date> dueDateColumn;
  Column<Action,Number> idColumn;
  
	
	
	public FollowupActivityTable()
	{
		super();
	}
	public FollowupActivityTable(UiScreen<Action> view)
	{
		super(view);	
	}

	public void createTable()
	{
		 createColumnidColumn();
createColumndescription();
createColumndueDateColumn();
createColumnactionTakenColumn();
createColumnstatusColumn();

		
		table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);		 
	}
	
	protected void createColumnidColumn()
{
NumberCell editCell=new NumberCell();
idColumn=new Column<Action,Number>(editCell)
{
@Override
public Number getValue(Action object)
{
if(object.getId()==0){
return 0;}
else{
return object.getId();}
}
};
table.addColumn(idColumn,"Activity Id");
}
protected void createColumndescription()
{
description=new TextColumn<Action>()
{
   @Override public String getValue(Action object){
  return object.getDescription();
}
};
table.addColumn(description,"Description");
description.setSortable(true);
}
protected void createColumndueDateColumn()
{
DateTimeFormat fmt = DateTimeFormat.getFormat(PredefinedFormat.DATE_SHORT);
DatePickerCell date= new DatePickerCell(fmt);
dueDateColumn=new Column<Action,Date>(date)
{
@Override
public Date getValue(Action object)
{
if(object.getActivityDueDate()!=null){
return object.getActivityDueDate();}
else{object.setActivityDueDate(new Date());
return new Date();
}
}
};
table.addColumn(dueDateColumn,"Due Date");
}
protected void createColumnactionTakenColumn()
{
actionTakenColumn=new TextColumn<Action>()
{
   @Override public String getValue(Action object){
  return object.getActionTaken();
}
};
table.addColumn(actionTakenColumn,"Action Taken");
actionTakenColumn.setSortable(true);
}
protected void createColumnstatusColumn()
{
statusColumn=new TextColumn<Action>()
{
   @Override public String getValue(Action object){
  return object.getStatus();
}
};
table.addColumn(statusColumn,"Status");
statusColumn.setSortable(true);
}


      
	@Override public void addFieldUpdater() 
	{
		

	}
	
	

	public void addColumnSorting(){
	
		createSortingidColumn();
createSortingdescription();
createSortingdueDateColumn();
createSortingactionTakenColumn();
createSortingstatusColumn();

	}
	
	protected void createSortingidColumn(){
List<Action> list=getDataprovider().getList();
columnSort=new ListHandler<Action>(list);
columnSort.setComparator(idColumn, new Comparator<Action>()
{
@Override
public int compare(Action e1,Action e2)
{
if(e1!=null && e2!=null)
{if(e1.getId()== e2.getId()){return 0;}if(e1.getId()> e2.getId()){return 1;}else{return -1;}}else{return 0;}}
});
table.addColumnSortHandler(columnSort);
}
protected void createSortingdescription(){
List<Action> list=getDataprovider().getList();
columnSort=new ListHandler<Action>(list);
columnSort.setComparator(description, new Comparator<Action>()
{
@Override
public int compare(Action e1,Action e2)
{
if(e1!=null && e2!=null)
{if( e1.getDescription()!=null && e2.getDescription()!=null){
return e1.getDescription().compareTo(e2.getDescription());}
}
else{
return 0;}
return 0;
}
});
table.addColumnSortHandler(columnSort);
}
protected void createSortingdueDateColumn(){
List<Action> list=getDataprovider().getList();
columnSort=new ListHandler<Action>(list);
columnSort.setComparator(dueDateColumn, new Comparator<Action>()
{
@Override
public int compare(Action e1,Action e2)
{
if(e1!=null && e2!=null)
{if( e1.getActivityDueDate()!=null && e2.getActivityDueDate()!=null){
return e1.getActivityDueDate().compareTo(e2.getActivityDueDate());}
}
else{
return 0;}
return 0;
}
});
table.addColumnSortHandler(columnSort);
}
protected void createSortingactionTakenColumn(){
List<Action> list=getDataprovider().getList();
columnSort=new ListHandler<Action>(list);
columnSort.setComparator(actionTakenColumn, new Comparator<Action>()
{
@Override
public int compare(Action e1,Action e2)
{
if(e1!=null && e2!=null)
{if( e1.getActionTaken()!=null && e2.getActionTaken()!=null){
return e1.getActionTaken().compareTo(e2.getActionTaken());}
}
else{
return 0;}
return 0;
}
});
table.addColumnSortHandler(columnSort);
}
protected void createSortingstatusColumn(){
List<Action> list=getDataprovider().getList();
columnSort=new ListHandler<Action>(list);
columnSort.setComparator(statusColumn, new Comparator<Action>()
{
@Override
public int compare(Action e1,Action e2)
{
if(e1!=null && e2!=null)
{if( e1.getStatus()!=null && e2.getStatus()!=null){
return e1.getStatus().compareTo(e2.getStatus());}
}
else{
return 0;}
return 0;
}
});
table.addColumnSortHandler(columnSort);
}

	
	@Override
	public void setEnable(boolean state)
	{
	
	}
	
	@Override
	public void applyStyle()
	{
	
	}
	@Override
	protected void initializekeyprovider() {
		keyProvider= new ProvidesKey<Action>()
		{
			@Override
			public Object getKey(Action item)
			{
				if(item==null)
				{
					return null;
				}
				else
					return item.getId();
			}
		};
	}
}
