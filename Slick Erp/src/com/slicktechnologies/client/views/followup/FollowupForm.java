package com.slicktechnologies.client.views.followup;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.businessprocesslayer.Activity;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
/**
 * FormTablescreen template.
 */
public class FollowupForm extends FormTableScreen<Activity>  implements ClickHandler {

	//Token to add the varialble declarations
	DateBox dbCreationDate;
  ObjectListBox<Branch> olbbBranch;
  IntegerBox tbLeadId;
  FollowupActivityTable activitytable;
  ObjectListBox<Config> olbcCategory,olbcType,olbcStatus;
  TextArea taComments;
  PersonInfoComposite pic;
  ObjectListBox<Employee> olbeSalesPerson;
  
	
	//Token to add the concrete presenter class name
	//protected <PresenterClassName> presenter;
	
	
	
	public FollowupForm  (SuperTable<Activity> table, int mode,
			boolean captionmode) {
		super(table, mode, captionmode);
		createGui();
		
	}
	
	private void initalizeWidget()
	{
		
dbCreationDate=new DateBoxWithYearSelector();
    
olbbBranch=new ObjectListBox<Branch>();
    AppUtility.makeBranchListBoxLive(olbbBranch);

tbLeadId=new IntegerBox();
    
activitytable=new FollowupActivityTable();
    
olbcCategory=new ObjectListBox<Config>();
    AppUtility.MakeLiveConfig(olbcCategory,Screen.FOLLOWUPCATEGORY);

olbcType=new ObjectListBox<Config>();
    AppUtility.MakeLiveConfig(olbcType,Screen.FOLLOWUPTYPE);

olbcStatus=new ObjectListBox<Config>();
    AppUtility.MakeLiveConfig(olbcStatus,Screen.FOLLOWUPSTATUS);

taComments=new TextArea();
    
pic=AppUtility.customerInfoComposite(new Customer());
olbeSalesPerson=new ObjectListBox<Employee>();
    AppUtility.makeSalesPersonListBoxLive(olbeSalesPerson);

	}

	/*
	 * Method template to create the formtable screen
	 */
	
	@Override
	public void createScreen() {
		
		//Token to initialize the processlevel menus.
	    initalizeWidget();
		
		
		
		//////////////////////////////////Form Field Declaration/////////////////////////////////////////////////
	    
		
		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////
		
		//Token to initialize formfield
		FormFieldBuilder fbuilder;
fbuilder = new FormFieldBuilder();
FormField fgroupingCustomerInformation=fbuilder.setlabel("Customer Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
fbuilder = new FormFieldBuilder("",pic);
FormField fpic= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
fbuilder = new FormFieldBuilder();
FormField fgroupingOfficeUse=fbuilder.setlabel("Office Use").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
fbuilder = new FormFieldBuilder("* Sales Person",olbeSalesPerson);
FormField folbeSalesPerson= fbuilder.setMandatory(true).setMandatoryMsg("Sales Person is Mandatory").setRowSpan(0).setColSpan(0).build();
fbuilder = new FormFieldBuilder("* Branch",olbbBranch);
FormField folbbBranch= fbuilder.setMandatory(true).setMandatoryMsg("Branch is Mandatory").setRowSpan(0).setColSpan(0).build();
fbuilder = new FormFieldBuilder("* Category",olbcCategory);
FormField folbcCategory= fbuilder.setMandatory(true).setMandatoryMsg("Category is Mandatory").setRowSpan(0).setColSpan(0).build();
fbuilder = new FormFieldBuilder("* Type",olbcType);
FormField folbcType= fbuilder.setMandatory(true).setMandatoryMsg("Type is Mandatory").setRowSpan(0).setColSpan(0).build();
fbuilder = new FormFieldBuilder("Status",olbcStatus);
FormField folbcStatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
fbuilder = new FormFieldBuilder("Lead Id",tbLeadId);
FormField ftbLeadId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
fbuilder = new FormFieldBuilder("Creation Date",dbCreationDate);
FormField fdbCreationDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
fbuilder = new FormFieldBuilder();
FormField fgroupingDescription=fbuilder.setlabel("Description").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
fbuilder = new FormFieldBuilder("Comments",taComments);
FormField ftaComments= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
fbuilder = new FormFieldBuilder();
FormField fgroupingActivityTable=fbuilder.setlabel("Activity Table").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
fbuilder = new FormFieldBuilder("",activitytable.getTable());
FormField factivitytable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();

		
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////
		
		
		FormField[][] formfield = {   {fgroupingCustomerInformation},
  {fpic},
  {fgroupingOfficeUse},
  {folbeSalesPerson,folbbBranch,folbcCategory,folbcType},
  {folbcStatus,ftbLeadId,fdbCreationDate},
  {fgroupingDescription},
  {ftaComments},
  {fgroupingActivityTable},
  {factivitytable},
  };

		
        this.fields=formfield;		
   }

	/**
	 * method template to update the model with token entity name
	 */
	@Override
	public void updateModel(Activity model) 
	{
		
		if(pic.getValue()!=null)
  model.setPerson(pic.getValue());
if(olbeSalesPerson.getValue()!=null)
  model.setEmployee(olbeSalesPerson.getValue());
if(olbbBranch.getValue()!=null)
  model.setBranch(olbbBranch.getValue());
if(olbcCategory.getValue()!=null)
  model.setCategory(olbcCategory.getValue());

if(olbcStatus.getValue()!=null)
  model.setStatus(olbcStatus.getValue());
if(dbCreationDate.getValue()!=null)
  model.setCreationDate(dbCreationDate.getValue());
if(taComments.getValue()!=null)
  model.setActivityDescription(taComments.getValue());
//if(activitytable.getValue()!=null)
 // model.setActivites(activitytable.getValue());

		
	}

	/**
	 * method template to update the view with token entity name
	 */
	@Override
	public void updateView(Activity view) 
	{
		
		
		if(view.getPerson()!=null)
  pic.setValue(view.getPerson());
if(view.getEmployee()!=null)
  olbeSalesPerson.setValue(view.getEmployee());
if(view.getBranch()!=null)
  olbbBranch.setValue(view.getBranch());
if(view.getCategory()!=null)
  olbcCategory.setValue(view.getCategory());

if(view.getStatus()!=null)
  olbcStatus.setValue(view.getStatus());
if(view.getCreationDate()!=null)
  dbCreationDate.setValue(view.getCreationDate());
if(view.getActivityDescription()!=null)
  taComments.setValue(view.getActivityDescription());
if(view.getActivites()!=null)
  activitytable.setValue(view.getActivites());

	}

	/**
	 * Toggles the app header bar menus as per screen state
	 */
	
	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
				
			}
		}
		
		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
					menus[k].setVisible(true); 
				
				else
					menus[k].setVisible(false);  		   
			}
		}
		
		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit"))
						menus[k].setVisible(true); 
				
				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.FOLLOWUP,LoginPresenter.currentModule.trim());
	}
	
	/**
	 * sets the id textbox with the passed count value. 
	 */
	@Override
	public void setCount(int count)
	{
	
	}

	
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
	}

	

}
