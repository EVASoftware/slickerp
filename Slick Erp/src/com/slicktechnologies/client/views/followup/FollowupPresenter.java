package com.slicktechnologies.client.views.followup;
import com.google.gwt.event.dom.client.ClickEvent;
import java.util.Vector;
import com.google.gwt.user.client.ui.*;
import com.simplesoftwares.client.library.appstructure.search.*;
import com.simplesoftwares.client.library.appstructure.formtablescreen.*;
import com.slicktechnologies.shared.*;
import com.slicktechnologies.shared.common.*;
import com.slicktechnologies.shared.common.businessprocesslayer.*;
import com.slicktechnologies.shared.common.businessunitlayer.*;
import com.slicktechnologies.shared.common.helperlayer.*;
import com.slicktechnologies.shared.common.personlayer.*;
import com.slicktechnologies.shared.common.productlayer.*;
import java.util.Vector;
import com.simplesoftwares.client.library.appstructure.search.*;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.google.gwt.core.shared.GWT;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.slicktechnologies.shared.common.paymentlayer.*;
import com.google.gwt.event.dom.client.*;
/**
 *  FormTableScreen presenter template
 */
public class FollowupPresenter extends FormTableScreenPresenter<Activity>  {

	//Token to set the concrete form
	FollowupForm form;
	
	public FollowupPresenter (FormTableScreen<Activity> view,
			Activity model) {
		super(view, model);
		form=(FollowupForm) view;
		form.getSupertable().connectToLocal();
		form.retriveTable(getActionQuery());
		form.setPresenter(this);
	}

	/**
	 * Method template to set the processBar events
	 */
	@Override
	public void reactToProcessBarEvents(ClickEvent e) 
   {
		InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();
		
		if(text.equals(""))
  reactTo();

		
	}
	
	

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

	/**
	 * Method template to set new model
	 */
	@Override
	protected void makeNewModel() {
		
		model=new Activity();
	}
	
	/*
	 * Method template to set Myquerry object
	 */
	public MyQuerry getActionQuery()
	{
		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new Activity());
		return quer;
	}
	
	public void setModel(Activity entity)
	{
		model=entity;
	}
	
	public static void initalize()
	{
			 FollowupPresenterTable gentableScreen=GWT.create( FollowupPresenterTable.class);
			
			FollowupForm  form=new  FollowupForm(gentableScreen,FormTableScreen.UPPER_MODE,true);
			gentableScreen.setView(form);
			gentableScreen.applySelectionModle();
			
			   FollowupPresenterTable gentableSearch=GWT.create( FollowupPresenterTable.class);
			     FollowupPresenterSearch.staticSuperTable=gentableSearch;
			       FollowupPresenterSearch searchpopup=GWT.create( FollowupPresenterSearch.class);
			           form.setSearchpopupscreen(searchpopup);
			
			 FollowupPresenter  presenter=new  FollowupPresenter  (form,new Activity());
			AppMemory.getAppMemory().stickPnel(form);
			
			
		 
	}
	
	
		@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.businessprocesslayer.Activity")
		 public static class  FollowupPresenterTable extends SuperTable<Activity> implements GeneratedVariableRefrence{

			@Override
			public Object getVarRef(String varName) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void createTable() {
				// TODO Auto-generated method stub
				
			}

			@Override
			protected void initializekeyprovider() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void addFieldUpdater() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void setEnable(boolean state) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void applyStyle() {
				// TODO Auto-generated method stub
				
			}} ;
			
			@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.businessprocesslayer.Activity")
			 public static class  FollowupPresenterSearch extends SearchPopUpScreen<  Activity>{

				@Override
				public MyQuerry getQuerry() {
					// TODO Auto-generated method stub
					return null;
				}

				@Override
				public boolean validate() {
					// TODO Auto-generated method stub
					return true;
				}};
				
	  private void reactTo()
  {
}
	

}
