package com.slicktechnologies.client.views.reports;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.slicktechnologies.shared.common.DummyEntityOnlyForScreen;

public class SalesRegisterFormPresenter extends FormScreenPresenter<DummyEntityOnlyForScreen> implements ClickHandler {

	SalesRegisterForm form;
	
	public SalesRegisterFormPresenter(UiScreen<DummyEntityOnlyForScreen> view,
			DummyEntityOnlyForScreen model) {
		super(view, model);
		form=(SalesRegisterForm) view;
		
		// TODO Auto-generated constructor stub
	}
	
	public static void initialize(){
		SalesRegisterForm form=new SalesRegisterForm();
		form.setToNewState();
		AppMemory.getAppMemory().stickPnel(form);
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void makeNewModel() {
		// TODO Auto-generated method stub
		
	}

}
