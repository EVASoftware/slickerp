package com.slicktechnologies.client.views.reports;

import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.slicktechnologies.client.views.paymentinfo.invoicedetails.SalesRegisterPopUp;
import com.slicktechnologies.shared.common.DummyEntityOnlyForScreen;

public class SalesRegisterForm extends FormScreen<DummyEntityOnlyForScreen>{
	
	SalesRegisterPopUp popup;
	
	public SalesRegisterForm() {
		// TODO Auto-generated constructor stub
		super();
		createGui();
	}

	@Override
	public void toggleAppHeaderBarMenu() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void createScreen() {
		// TODO Auto-generated method stub
		popup=new SalesRegisterPopUp();
		popup.getLblCancel().setVisible(false);
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgrouping=fbuilder.setlabel("Sales Register").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("",popup.content);
		FormField fpop= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		FormField[][] formfield = {  
				{fgrouping},
				{fpop},
		};
		this.fields=formfield;
	}

	@Override
	public void updateModel(DummyEntityOnlyForScreen model) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateView(DummyEntityOnlyForScreen model) {
		// TODO Auto-generated method stub
		
	}

}
