package com.slicktechnologies.client.views.reports;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.PopupPanel;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.appskeleton.AbstractScreen;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.CustomerNameChangeService;
import com.slicktechnologies.client.services.CustomerNameChangeServiceAsync;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.services.ServerUnitConversionServices;
import com.slicktechnologies.client.services.ServerUnitConversionServicesAsync;
import com.slicktechnologies.client.services.UpdateService;
import com.slicktechnologies.client.services.UpdateServiceAsync;
import com.slicktechnologies.client.services.XlsxService;
import com.slicktechnologies.client.services.XlsxServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.contract.ContractPresenterSearchProxy;
import com.slicktechnologies.client.views.contract.ContractService;
import com.slicktechnologies.client.views.contract.ContractServiceAsync;
import com.slicktechnologies.client.views.contractrenewal.RenewalResult;
import com.slicktechnologies.client.views.customerDetails.CustomerDetailsForm;
import com.slicktechnologies.client.views.customerDetails.CustomerDetailsPresenter;
import com.slicktechnologies.client.views.inventory.productinventorytransaction.ClosingReport;
import com.slicktechnologies.client.views.inventory.productinventorytransaction.WarehouseInfo;
import com.slicktechnologies.client.views.popups.EmailPopup;
import com.slicktechnologies.client.views.settings.employee.EmployeeBranch;
import com.slicktechnologies.shared.AmcAssuredRevenueReport;
import com.slicktechnologies.shared.BOMServiceListBean;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.DummyEntityOnlyForScreen;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.inventory.ProductInventoryTransaction;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

public class ReportPresenter extends  FormScreenPresenter<DummyEntityOnlyForScreen> implements ClickHandler{

	public static ReportForm form;
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	GeneralServiceAsync genservice=GWT.create(GeneralService.class);
	ContractServiceAsync async=GWT.create(ContractService.class);
	UpdateServiceAsync updateSer = GWT.create(UpdateService.class);
	/**
	 * nidhi
	 *  *:*:*
	 *  
	 * @param view
	 * @param model
	 */
	ServerUnitConversionServicesAsync serAsync = GWT.create(ServerUnitConversionServices.class);
	GenricServiceAsync genricServiceAsync=GWT.create(GenricService.class);
	
	CustomerNameChangeServiceAsync serviceRevenuelossReport = GWT.create(CustomerNameChangeService.class);
	EmailPopup mailpopup=new EmailPopup();
//	public static ArrayList<String> emailIdList = new ArrayList<String>();
	PopupPanel panel=new PopupPanel();
	
	public ReportPresenter(UiScreen<DummyEntityOnlyForScreen> view,
			DummyEntityOnlyForScreen model) {
		super(view, model);
		form = (ReportForm) view;
		form.getBtdownload().addClickHandler(this);
		form.getBtSendMail().addClickHandler(this);
		
		mailpopup.getBtAdd().addClickHandler(this);
		mailpopup.getLblCancel().addClickHandler(this);
		mailpopup.getLblOk().addClickHandler(this);
		
		// TODO Auto-generated constructor stubreportForm
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void makeNewModel() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		if(form.getBtdownload().equals(event.getSource())){
			if(AppMemory.getAppMemory().currentScreen != Screen.RENEWALCONTRACTREPORT && validate()){
				if (AppMemory.getAppMemory().currentScreen == Screen.REVENUEREPORT){
					form.showWaitSymbol();
					String branchName = form.getOlbBranch().getValue(form.getOlbBranch().getSelectedIndex());
					async.serviceRevanueReport(UserConfiguration.getCompanyId(), form.getFromDate().getValue(), form.getToDate().getValue(),branchName, new AsyncCallback<String>(){

							@Override
							public void onFailure(Throwable caught) {
								// TODO Auto-generated method stub
								form.showDialogMessage("error : "+caught);
								form.hideWaitSymbol();
							}

							@Override
							public void onSuccess(String result) {
								// TODO Auto-generated method stub
								//form.showDialogMessage(result);
								if(result.equals("Success")){
									String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
									final String url = gwt + "csvservlet" + "?type=" + 123;
									Window.open(url, "test", "enabled");
									form.hideWaitSymbol();
								}else{
									form.showDialogMessage(result);
									form.hideWaitSymbol();
								}
								
							}
				 			
				 		});
				}
				else if(AppMemory.getAppMemory().currentScreen == Screen.TECHNICIANREVENUEREPORT) {
					form.showWaitSymbol();
					String branchName = form.getOlbBranch().getValue(form.getOlbBranch().getSelectedIndex());
					async.technicianRevanueReport(UserConfiguration.getCompanyId(), form.getFromDate().getValue(), form.getToDate().getValue(),branchName, new AsyncCallback<String>(){

						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							form.showDialogMessage("error : "+caught);
							form.hideWaitSymbol();
						}

						@Override
						public void onSuccess(String result) {
							// TODO Auto-generated method stub
							if(result.equals("Success")){
								String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
								final String url = gwt + "csvservlet" + "?type=" + 124;
								Window.open(url, "test", "enabled");
								form.hideWaitSymbol();
							}else{
								form.showDialogMessage(result);
								form.hideWaitSymbol();
							}
							
						}
			 			
			 		});

				}
				else if (AppMemory.getAppMemory().currentScreen == Screen.SERVICEREVENUEREPORT){
				form.showWaitSymbol();
					 ArrayList<String> brName = new ArrayList<String>();
					 if(form.getOlbBranch().getSelectedIndex() == 0){
						
						 for(int i=1;i<form.getOlbBranch().getItemCount() ; i++){
							 brName.add(form.getOlbBranch().getValue(i).trim());
						 }
						
					 }else{
						 brName.add(form.getOlbBranch().getValue(form.getOlbBranch().getSelectedIndex()).trim());
					 }
					 updateSer.dailyFumigationValueReportDetails(UserConfiguration.getCompanyId(), form.getFromDate().getValue(),form.getToDate().getValue() ,brName , new AsyncCallback<Void>() {
							
							@Override
							public void onSuccess(Void result) {
								/**
								 * Updated By:Viraj
								 * Date: 13-06-2019
								 * Description: To set fromDate and toDate
								 */
								XlsxServiceAsync xlxsService = GWT.create(XlsxService.class);
								xlxsService.setServiceFromToDate(form.getFromDate().getValue(), form.getToDate().getValue(), new AsyncCallback<Void>() {

									@Override
									public void onFailure(Throwable caught) {
										// TODO Auto-generated method stub
										
									}

									@Override
									public void onSuccess(Void result) {
										String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
										final String url=gwt + "CreateXLXSServlet"+"?type="+2;
										Window.open(url, "test", "enabled");
										form.hideWaitSymbol();
										
									}
								});
								/** Ends **/
							}
							
							@Override
							public void onFailure(Throwable caught) {
								// TODO Auto-generated method stub
								//form.hideWaitSymbol();
							}
						});
				}
				else if( AppMemory.getAppMemory().currentScreen == Screen.COMMODITYFUMIGATIONREPORT) {
				form.showWaitSymbol();
					 ArrayList<String> brName = new ArrayList<String>();
					 if(form.getOlbBranch().getSelectedIndex() == 0){
						
						 for(int i=1;i<form.getOlbBranch().getItemCount() ; i++){
							 brName.add(form.getOlbBranch().getValue(i).trim());
						 }
						
					 }else{
						 brName.add(form.getOlbBranch().getValue(form.getOlbBranch().getSelectedIndex()).trim());
					 }
					 System.out.println("selected branch - fumigation report-" + brName.toString());
					 updateSer.dailyFumigationReportDetails(UserConfiguration.getCompanyId(),form.getFromDate().getValue(),form.getToDate().getValue(),brName, new AsyncCallback<Void>() {
							
							@Override
							public void onSuccess(Void result) {
		
								String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
								final String url=gwt + "CreateXLXSServlet"+"?type="+1;
								Window.open(url, "test", "enabled");
								form.hideWaitSymbol();
							}
				
							
							@Override
							public void onFailure(Throwable caught) {
								// TODO Auto-generated method stub
								form.hideWaitSymbol();
							}
						});
				}
			else if (AppMemory.getAppMemory().currentScreen == Screen.CLOSINGSTOCKREPORT) {
				form.showWaitSymbol();	
				String branch = null , warehouse = null;
					if(form.getOlbBranch().getSelectedIndex()!=0){
						branch = form.getOlbBranch().getValue();
					}
					if(form.getOlbWarehouse().getSelectedIndex()!=0){
						warehouse = form.getOlbWarehouse().getValue();
					}
					getClosingReport(form.getFromDate().getValue(),form.getToDate().getValue(),branch,warehouse);
					form.hideWaitSymbol();
					
			}/** date 14.03.2018 added by komal for nbhc new report **/
			else if(AppMemory.getAppMemory().currentScreen == Screen.ACTIVECONTRACTREPORT){
				form.showWaitSymbol();	
				
				StringBuilder filterValue = new StringBuilder();
		 		StringBuilder filterQuerry = new StringBuilder();
		 						 		
		 		if(form.getFromDate().getValue()!=null){
		 			filterQuerry.append("endDate >="+"~");
		 			filterValue.append(AppUtility.parseDate(form.getFromDate().getValue())+"~");
		 		}
		 		if(form.olbContractStatus.getSelectedIndex()!=0){
		 			filterQuerry.append("status"+"~");
		 			filterValue.append(form.getOlbContractStatus().getValue(form.getOlbContractStatus().getSelectedIndex()).trim()+"~");
		 		}
		 		if(form.olbBranch.getSelectedIndex()!=0){
		 			filterQuerry.append("branch"+"~");
		 			filterValue.append(form.getOlbBranch().getValue().trim()+"~");
		 		}
		 		Company c  = new Company();
		 		filterQuerry.append("companyId");
		 		filterValue.append(c.getCompanyId());
		 		
		 		String querry = filterQuerry.toString();
		 		String value = filterValue.toString();
		 		
		 		String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();

				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "ContractReport") || AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "ContractDownloadNew")){
				 		
						if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "ContractReport")){
								final String url = gwt + "csvservlet" + "?type=" + 127+ "&" +"querry="+querry+ "&" +"value="+value;
								Window.open(url, "test", "enabled");
								form.hideWaitSymbol();
						}
						if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "ContractDownloadNew")){
						
							final String url = gwt + "csvservlet" + "?type=" + 134+ "&" +"querry="+querry+ "&" +"value="+value;
							Window.open(url, "test", "enabled");
							form.hideWaitSymbol();
						}												
				}
				/**
				 * @author Vijay Date :- 25-07-2020
				 * Des :- when there is no process configuration is active the Default Contract download format will download
				 *  for Active contract Report. This is for other clients to use
				 */
				else{
					final String url = gwt + "csvservlet" + "?type=" + 134+ "&" +"querry="+querry+ "&" +"value="+value;
					Window.open(url, "test", "enabled");
					form.hideWaitSymbol();
				}
				
				
				}/** date 15.03.2018 added by komal for nbhc new report **/
			else if(AppMemory.getAppMemory().currentScreen == Screen.AMCASSUREDREVENUEREPORT){
				String status = "";
				form.showWaitSymbol();
				if(form.getOlbContractStatus().getSelectedIndex()!= 0){
					status = form.getOlbContractStatus().getValue(form.getOlbContractStatus().getSelectedIndex());
				}
				async.getAmcAssuredRevenueReport(UserConfiguration.getCompanyId(), form.getFromDate().getValue(), form.getOlbBranch().getValue(), new AsyncCallback<ArrayList<AmcAssuredRevenueReport>>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						System.out.println("Error : " + caught);
						form.hideWaitSymbol();
					}

					@Override
					public void onSuccess(
							ArrayList<AmcAssuredRevenueReport> result) {
						// TODO Auto-generated method stub
						if(result.size()!=0){
							String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
							final String url = gwt + "csvservlet" + "?type=" + 136;
							Window.open(url, "test", "enabled");
							form.hideWaitSymbol();
						}else{
							form.showDialogMessage("No result found");
							form.hideWaitSymbol();
						}
					}
				});
			}/**
				nidhi
				*:*:*
				*2-10-2018
				*/else if(AppMemory.getAppMemory().currentScreen == Screen.BOMSERVICEMATRIALREPORT){
					reactonBOMReport();
				}
				/**
				nidhi
				*:*:*
				*2-10-2018
				*/else if(AppMemory.getAppMemory().currentScreen == Screen.SERVICEINVOICEREPORT){
					form.showWaitSymbol();
					 ArrayList<String> brName = new ArrayList<String>();
					 if(form.getOlbBranch().getSelectedIndex() == 0){
						
						 for(int i=1;i<form.getOlbBranch().getItemCount() ; i++){
							 brName.add(form.getOlbBranch().getValue(i).trim());
						 }
						
					 }else{
						 brName.add(form.getOlbBranch().getValue(form.getOlbBranch().getSelectedIndex()).trim());
					 }
					 
					 updateSer.serviceInvoiceIdMapping(UserConfiguration.getCompanyId(), form.getFromDate().getValue(),form.getToDate().getValue() ,brName, new AsyncCallback<Integer>() {
						
						@Override
						public void onSuccess(Integer result) {
							// TODO Auto-generated method stub

							
							String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
							final String url=gwt + "csvservlet"+"?type="+154;
							Window.open(url, "test", "enabled");
							form.hideWaitSymbol();
						
						}
						
						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							
						}
					});
				}else if(AppMemory.getAppMemory().currentScreen == Screen.BRANCHWISESPLITINVOICEREPORT){
					if(validate()){
						reactOnBranchwiseSplitReport();
					}
				}
				/**
				 * Date 26-07-2019 by Vijay for NBHC CCPM Cancelled Services for Approved contracts only
				 */
				else if(AppMemory.getAppMemory().currentScreen == Screen.SERVICEREVENUELOSSREPORT){
					if(validate()){
						reactOnServiceRevenueLossReport(true,null);
					}
				}
					
			}else if(AppMemory.getAppMemory().currentScreen == Screen.RENEWALCONTRACTREPORT){
				if(validateDate()){
					ArrayList<Filter> contractFilter ;
					ArrayList<Filter> contractRenewalFilter ;
						contractFilter = getContractFilter();
						contractRenewalFilter = getContractRenewalFilter();
					form.showWaitSymbol();
					async.contractRenewalList(contractFilter, contractRenewalFilter, new AsyncCallback<ArrayList<RenewalResult>>() {
						
						@Override
						public void onSuccess(ArrayList<RenewalResult> result) {
							// TODO Auto-generated method stub
							if(result.size()>0){
								async.contractRenewalReeport(result, new AsyncCallback<ArrayList<RenewalResult>>() {
									
									@Override
									public void onSuccess(ArrayList<RenewalResult> result1) {
										// TODO Auto-generated method stub
										if(result1.size()>0){
											String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
											final String url=gwt + "csvservlet"+"?type="+165;
											Window.open(url, "test", "enabled");
											form.hideWaitSymbol();
										}else{
											form.showDialogMessage("Unexpected Error!");
											form.hideWaitSymbol();
										}
									}
									
									@Override
									public void onFailure(Throwable caught) {
										// TODO Auto-generated method stub
										form.showDialogMessage("Unexpected load contract!");
										form.hideWaitSymbol();
									}
								});
							}else{
								form.showDialogMessage("No Data Loaded");
								form.hideWaitSymbol();
							}
						}
						
						@Override
						public void onFailure(Throwable caught) {
							form.hideWaitSymbol();
							form.showDialogMessage("Unexpected Error!");
							// TODO Auto-generated method stub
							
						}
					});/*(contractFilter,contractRenewalFilter, new AsyncCallback<ArrayList<RenewalResult>>(){
						@Override
						public void onFailure(Throwable caught) {
							form.hideWaitSymbol();
							form.showDialogMessage("Unexpected Error!");
						}
						@Override
						public void onSuccess(ArrayList<RenewalResult> result) {
							form.hideWaitSymbol();
							Console.log("RENWAL LIST SIZE : "+result.size());
							form.table.connectToLocal();
							form.processedtable.connectToLocal();
							if(result.size()!=0){
								List<RenewalResult> unprocessedList=new ArrayList<RenewalResult>();
								List<RenewalResult> processedList=new ArrayList<RenewalResult>();
								
								for(RenewalResult renres:result){
									if(renres.getRenewProcessed().equals(AppConstants.NO)){
										unprocessedList.add(renres);
										
									}else{
										processedList.add(renres);
										
									}
								}
								List<RenewalResult> UNPROCESSED=unprocessedList;
								List<RenewalResult> PROCESSED=processedList;
								
								Console.log("UNPROCESSED LIST SIZE : "+unprocessedList.size());
								Console.log("PROCESSED LIST SIZE : "+processedList.size());
								
								Console.log("unprocessedList SIZE : "+UNPROCESSED.size());
								Console.log("processedList SIZE : "+PROCESSED.size());
								
								form.table.getDataprovider().setList(UNPROCESSED);
								
								form.processedtable.getDataprovider().setList(PROCESSED);
							}
							else{
								form.showDialogMessage("No Result Found!");
							}
						} 
				
					});*/
				}
			}
			/**
			 * Date 02-09-2019 by Vijay
			 * Des :- NBHC Inventory Management Lock Seal Deviation report
			 */
			else if(AppMemory.getAppMemory().currentScreen == Screen.LOCKSEALDEVIATIONREPORT){
				if(validate()){
					reactonLockSealDeviationReport();
				}
			}
		}
		
		
		if(form.getBtSendMail().equals(event.getSource())){
			mailpopup.showPopUp();
			
			Console.log("Employee List Size"+LoginPresenter.globalEmployee.size());
			for(Employee obj:LoginPresenter.globalEmployee){
				Console.log("Inside Loop");
				if(UserConfiguration.getInfo().getEmpCount()==obj.getCount()){
					Console.log("Inside IFFFF"+obj.getEmail());
					mailpopup.getTbEmailBox().setValue(obj.getEmail());
					break;
				}
			}
			
	}
		
		
		
		if(event.getSource().equals(mailpopup.getLblOk())){
			ArrayList<String> emailIdList = new ArrayList<String>();
			if(mailpopup.getEmailTable().getDataprovider().getList().size() > 0){	
				for(EmployeeBranch branch : mailpopup.getEmailTable().getDataprovider().getList()){
					emailIdList.add(branch.getBranchName());
				}
		}else{
			form.showDialogMessage("Please add emailId.");
			return;
		}
			
			if(AppMemory.getAppMemory().currentScreen == Screen.SERVICEREVENUELOSSREPORT){
				if(validate()){
					reactOnServiceRevenueLossReport(false,emailIdList);
				}
			}
			
			
		}
		
		
		
	}
	
	

	

	public static void initialize(AbstractScreen value)
	{
		ReportForm reportForm = new ReportForm();
		AppMemory.getAppMemory().currentScreen=value;
		ReportPresenter presenter= new ReportPresenter(reportForm, new DummyEntityOnlyForScreen());
		AppMemory.getAppMemory().stickPnel(reportForm);	
	}
	private boolean validate(){
		if(form.getFromDate().getValue()==null){
			form.showDialogMessage("Please select from date.");
 			return false;

		}
		/** Date 26-07-2019 by Vijay for NBHC CCPM **/
		if(AppMemory.getAppMemory().currentScreen == Screen.SERVICEREVENUELOSSREPORT || 
				AppMemory.getAppMemory().currentScreen == Screen.LOCKSEALDEVIATIONREPORT ){
			if(form.getToDate().getValue()==null){
				form.showDialogMessage("Please select To date.");
	 			return false;	
			}
			return true;
        }
		/*** ends here ******/
		if(!(AppMemory.getAppMemory().currentScreen == Screen.ACTIVECONTRACTREPORT)  && !(AppMemory.getAppMemory().currentScreen == Screen.AMCASSUREDREVENUEREPORT)
				){ 
			if(form.getToDate().getValue()==null){
				form.showDialogMessage("Please select To date.");
	 			return false;	
			}
			/**
			 *  nidhi
			 *  12-04-2018
			 *  for add branch validation
			 **/
			if(!(AppMemory.getAppMemory().currentScreen == Screen.BRANCHWISESPLITINVOICEREPORT)){
				if(form.getOlbBranch().getSelectedIndex()==0){
	    			form.showDialogMessage("Please select branch");
	    			return false;
	    		}
			}
		}
        if(AppMemory.getAppMemory().currentScreen == Screen.CLOSINGSTOCKREPORT){
        	if(LoginPresenter.branchRestrictionFlag==true&&!UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin")){
        		if(form.getOlbWarehouse().getSelectedIndex()==0){
        			form.showDialogMessage("Please select warehouse");
        			return false;
        		}
        		
        	}
        }
    	if(!(AppMemory.getAppMemory().currentScreen == Screen.ACTIVECONTRACTREPORT)  && !(AppMemory.getAppMemory().currentScreen == Screen.AMCASSUREDREVENUEREPORT)){
			if(form.getToDate().getValue().before(form.getFromDate().getValue())){
	 			form.showDialogMessage("To date should be greater than from date.");
	 			return false;
	 		}
    	}
		/** date 14.03.2018 added by komal for nbhc **/
		if(AppMemory.getAppMemory().currentScreen == Screen.ACTIVECONTRACTREPORT){
        		if(form.getOlbBranch().getSelectedIndex()==0){
        			form.showDialogMessage("Please select branch");
        			return false;
        		}
        		if(form.getOlbContractStatus().getSelectedIndex()==0){
        			form.showDialogMessage("Please select status");
        			return false;
        		}
        }
		/** date 14.03.2018 added by komal for nbhc **/
		if(AppMemory.getAppMemory().currentScreen == Screen.AMCASSUREDREVENUEREPORT){
        		if(form.getOlbBranch().getSelectedIndex()==0){
        			form.showDialogMessage("Please select branch");
        			return false;
        		}
        }
		
		return true;	
	}
	private void getClosingReport(Date fromDate, Date toDate, final String branch,final  String warehouse) {
		
		Vector<Filter> temp=new Vector<Filter>();
		Filter filter=null;
		MyQuerry querry=new MyQuerry();
		
//		filter=new Filter();
//		filter.setQuerryString("inventoryTransactionDateTime >=");
//		filter.setDateValue(fromDate);
//		temp.add(filter);
//		
//		filter=new Filter();
//		filter.setQuerryString("inventoryTransactionDateTime <=");
//		filter.setDateValue(toDate);
//		temp.add(filter);
		
		filter=new Filter();
		filter.setQuerryString("creationDate >=");
		filter.setDateValue(fromDate);
		temp.add(filter);
		
		filter=new Filter();
		filter.setQuerryString("creationDate <=");
		filter.setDateValue(toDate);
		temp.add(filter);
		
		if(branch!=null){
			filter=new Filter();
			filter.setQuerryString("branch");
			filter.setStringValue(branch);
			temp.add(filter);
		}
		
		if(warehouse!=null){
			filter=new Filter();
			filter.setQuerryString("warehouseName");
			filter.setStringValue(warehouse);
			temp.add(filter);
		}
		
		filter=new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(UserConfiguration.getCompanyId());
		temp.add(filter);
		
		querry.setFilters(temp);
		querry.setQuerryObject(new ProductInventoryTransaction());
		//form.showWaitSymbol();
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();	
			}
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				form.hideWaitSymbol();
				
				ArrayList<ProductInventoryTransaction> transactionList=new ArrayList<ProductInventoryTransaction>();
				if(result.size()!=0){
					for(SuperModel model:result){
						ProductInventoryTransaction pit=(ProductInventoryTransaction) model;
						transactionList.add(pit);
					}
					Console.log("Transaction List Size : "+transactionList.size());
					final ArrayList<ClosingReport> closingStockReport=getClosingStockReport(transactionList);
					Console.log("Closing Stock List Size : "+closingStockReport.size());
                   /**  date 01-02-2018 added by komal for nbhc closing report requirement **/
					genservice.getProductInventoryViewDetails(UserConfiguration.getCompanyId(), branch, warehouse, new AsyncCallback<ArrayList<ProductInventoryViewDetails>>() {
						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							Console.log("falied" + caught);
						}
						@Override
						public void onSuccess(ArrayList<ProductInventoryViewDetails> result) {
							
							ArrayList<ClosingReport> closingStockReport1 = new ArrayList<ClosingReport>();
							if(result.size()!=0){
							ArrayList<ProductInventoryViewDetails> list = new ArrayList<ProductInventoryViewDetails>();
							list.addAll(result);
							for(ProductInventoryViewDetails wh : result){
								for(ClosingReport c : closingStockReport) {
									
							if(c.getWarehouseName().equalsIgnoreCase(wh.getWarehousename()) &&
									c.getStorageLocation().equalsIgnoreCase(wh.getStoragelocation()) &&
									c.getStorageBin().equalsIgnoreCase(wh.getStoragebin()) &&
									c.getProductId() == wh.getProdid()){
								list.remove(wh);						
							  }
							 }
							}
							
							if(list.size()!=0){
								closingStockReport1 = generateClosingReportForOtherProducts(list ,branch);
							}
							if(closingStockReport1.size()!=0){
								closingStockReport.addAll(closingStockReport1);
							}
							}
							if(closingStockReport.size()!=0 || closingStockReport1.size()!=0){
								downloadClosingStockReport(closingStockReport);
							}else{
								form.showDialogMessage("No records found");
							}
							form.hideWaitSymbol();	
						}
					});				
					
				}else{
					//form.showDialogMessage("No records found for given duration.");
					genservice.getProductInventoryViewDetails(UserConfiguration.getCompanyId(), branch, warehouse, new AsyncCallback<ArrayList<ProductInventoryViewDetails>>() {
						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							
						}
						@Override
						public void onSuccess(ArrayList<ProductInventoryViewDetails> result) {
							// TODO Auto-generated method stub
							ArrayList<ProductInventoryViewDetails> list = new ArrayList<ProductInventoryViewDetails>();
							list.addAll(result);
							ArrayList<ClosingReport> closingStockReport1 = new ArrayList<ClosingReport>();
							if(list.size()!=0){
								closingStockReport1 = generateClosingReportForOtherProducts(list ,branch);
							}
							if(closingStockReport1.size()!=0){
								downloadClosingStockReport(closingStockReport1);
							}else{
								form.showDialogMessage("No records found");
							}
							form.hideWaitSymbol();	
						}
					});					
				}
			}
		});	
	}

	protected void downloadClosingStockReport(ArrayList<ClosingReport> closingStockReport) {
		csvservice.setClosingStockReport(closingStockReport, new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);
				
			}
			@Override
			public void onSuccess(Void result) {
				
				Timer t = new Timer() {
					@Override
					public void run() {
						String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
						final String url = gwt + "csvservlet"+ "?type=" + 122;
						Window.open(url, "test", "enabled");
					}
				};
				t.schedule(2000);
			}
		});
		
	}

	protected ArrayList<ClosingReport> getClosingStockReport(ArrayList<ProductInventoryTransaction> transactionList) {
		HashSet<Integer> productHS=new HashSet<Integer>();
		HashSet<String> branchHS=new HashSet<String>();
		HashSet<WarehouseInfo> whHS=new HashSet<WarehouseInfo>();
		ArrayList<ClosingReport> closingStockReport=new ArrayList<ClosingReport>();
		
		HashSet<String> warehouseHS=new HashSet<String>();
		
		
		for(int i=0;i<transactionList.size();i++){
			if(transactionList.get(i).getDocumentType().equals("Product Inventory View")
					||transactionList.get(i).getDocumentType().equals(AppConstants.PHYSICALINVENTORY)){
				transactionList.remove(i);
				i--;
			}
		}
		Console.log("Updated Transaction List Size : "+transactionList.size());
		
		Comparator<ProductInventoryTransaction> comparator = new Comparator<ProductInventoryTransaction>() {  
			@Override  
			public int compare(ProductInventoryTransaction o1, ProductInventoryTransaction o2) {
				Integer prodId1=o1.getProductId();
				Integer prodId2=o2.getProductId();
				return prodId1.compareTo(prodId2);  
			}  
		};  
		Collections.sort(transactionList,comparator);
		
		for(ProductInventoryTransaction pit:transactionList){
			productHS.add(pit.getProductId());
			branchHS.add(pit.getBranch());
			
			String warehouseInfo=pit.getWarehouseName()+"$"+pit.getStorageLocation()+"$"+pit.getStorageBin();
			warehouseHS.add(warehouseInfo);
//			WarehouseInfo wh=new WarehouseInfo();
//			wh.setWarehouseName(pit.getWarehouseName());
//			wh.setStorageLocation(pit.getStorageLocation());
//			wh.setStorageBin(pit.getStorageBin());
//			whHS.add(wh);
			
				
		}
		ArrayList<Integer> productList=new ArrayList<Integer>(productHS);
		Console.log("No. of Unique Product "+productList.size());
		ArrayList<String> branchList=new ArrayList<String>(branchHS);
		Console.log("No. of Unique Branch "+branchList.size());
		
		ArrayList<String> warehouseList=new ArrayList<String>(warehouseHS);
		Console.log("No. of Unique Warehouse 1 - "+warehouseList.size());
		ArrayList<WarehouseInfo> whList=new ArrayList<WarehouseInfo>();
		for(String warehouse:warehouseList){
			String[] warehouseArr = warehouse.split("[$]");
			
			WarehouseInfo wh=new WarehouseInfo();
			wh.setWarehouseName(warehouseArr[0]);
			wh.setStorageLocation(warehouseArr[1]);
			wh.setStorageBin(warehouseArr[2]);
			whList.add(wh);
		}
//		ArrayList<WarehouseInfo> whList=new ArrayList<WarehouseInfo>(whHS);
		Console.log("No. of Unique Warehouse "+whList.size());
		
		for(String branch:branchList){
			for(WarehouseInfo wh:whList){
				for(Integer productId:productList){
					ArrayList<ProductInventoryTransaction> prodWiseTranList=getProductWiseTransactionList(branch,wh,productId,transactionList);
//					Console.log("###Branch : "+branch+" WH : "+wh.getWarehouseName()+"-"+wh.getStorageLocation()+"-"+wh.getStorageBin()+" "+"Product: "+productId+"--"+prodWiseTranList.size());
					
					if(prodWiseTranList.size()!=0){
						ClosingReport report=generateClosingReport(prodWiseTranList);
						closingStockReport.add(report);
					}
				}
			}
		}
		Console.log("Closing Stock Report Size : "+closingStockReport.size());
		
		return closingStockReport;
	}

	private ClosingReport generateClosingReport(ArrayList<ProductInventoryTransaction> prodWiseTranList) {
		double openingStock=0;
		double totalGrnQty=0;
		double totalMinQty=0;
		double totalMmnInQty=0;
		double totalMmnOutQty=0;
		double totalMmnTransferInQty=0;
		double totalMmnTransferOutQty=0;
		double closingStock=0;
		
		
		Comparator<ProductInventoryTransaction> comparator = new Comparator<ProductInventoryTransaction>() {  
			@Override  
			public int compare(ProductInventoryTransaction o1, ProductInventoryTransaction o2) {
				Integer count1=o1.getCount();
				Integer count2=o2.getCount();
				
				return count1.compareTo(count2);  
			}  
		};  
		Collections.sort(prodWiseTranList,comparator);
		
		openingStock=prodWiseTranList.get(0).getOpeningStock();
		closingStock=prodWiseTranList.get(prodWiseTranList.size()-1).getClosingStock();
		
		for(ProductInventoryTransaction trans:prodWiseTranList){
			if(trans.getDocumentType().equals(AppConstants.GRN)){
				totalGrnQty=totalGrnQty+trans.getProductQty();
			}else if(trans.getDocumentType().equals(AppConstants.MIN)){
				totalMinQty=totalMinQty+trans.getProductQty();
			}else if(trans.getDocumentType().equals(AppConstants.MMN)){
				if(trans.getDocumentSubType()!=null&&!trans.getDocumentSubType().equals("")){
					if(trans.getDocumentSubType().equals("IN")){
						totalMmnInQty=totalMmnInQty+trans.getProductQty();
					}else if(trans.getDocumentSubType().equals("OUT")){
						totalMmnOutQty=totalMmnOutQty+trans.getProductQty();
					}else if(trans.getDocumentSubType().equals("TRANSFERIN")){
						totalMmnTransferInQty=totalMmnTransferInQty+trans.getProductQty();
					}else if(trans.getDocumentSubType().equals("TRANSFEROUT")){
						totalMmnTransferOutQty=totalMmnTransferOutQty+trans.getProductQty();
					}
				}
			}
		}
		
		ClosingReport object=new ClosingReport();
		object.setBranch(prodWiseTranList.get(0).getBranch());
		object.setWarehouseName(prodWiseTranList.get(0).getWarehouseName());
		object.setStorageLocation(prodWiseTranList.get(0).getStorageLocation());
		object.setStorageBin(prodWiseTranList.get(0).getStorageBin());
		object.setProductId(prodWiseTranList.get(0).getProductId());
		object.setProductCode(prodWiseTranList.get(0).getProductCode());
		object.setProductName(prodWiseTranList.get(0).getProductName());
		object.setProductName(prodWiseTranList.get(0).getProductName());
		object.setOpeningStock(openingStock);
		object.setTotalGrnQty(totalGrnQty);
		object.setTotalMinQty(totalMinQty);
		object.setTotalMmnInQty(totalMmnInQty);
		object.setTotalMmnOutQty(totalMmnOutQty);
		object.setTotalMmnTransferInQty(totalMmnTransferInQty);
		object.setTotalMmnTransferOutQty(totalMmnTransferOutQty);
		object.setClosingStock(closingStock);
		
		return object;
	}

	private ArrayList<ProductInventoryTransaction> getProductWiseTransactionList(String branch, WarehouseInfo wh, Integer productId,ArrayList<ProductInventoryTransaction> transactionList) {
		ArrayList<ProductInventoryTransaction> prodWiseTranList=new ArrayList<ProductInventoryTransaction>();
		
		for(ProductInventoryTransaction pit:transactionList){
			if(branch.equals(pit.getBranch())
				&&wh.getWarehouseName().equals(pit.getWarehouseName())
				&&wh.getStorageLocation().equals(pit.getStorageLocation())
				&&wh.getStorageBin().equals(pit.getStorageBin())
				&&productId==pit.getProductId()){
				
				prodWiseTranList.add(pit);
			}
		}
		
		Comparator<ProductInventoryTransaction> comparator = new Comparator<ProductInventoryTransaction>() {  
			@Override  
			public int compare(ProductInventoryTransaction o1, ProductInventoryTransaction o2) {
				return o1.getCreationDate().compareTo(o2.getCreationDate());  
			}  
		};  
		Collections.sort(prodWiseTranList,comparator);
		
		return prodWiseTranList;
	}
	
//	date 01-02-2018 added by komal for nbhc 
	private ArrayList<ClosingReport>  generateClosingReportForOtherProducts(ArrayList<ProductInventoryViewDetails> prodViewList ,String branch) {
		ArrayList<ClosingReport> list = new ArrayList<ClosingReport>();
		ClosingReport object;
		for(ProductInventoryViewDetails prodView : prodViewList){
		object=new ClosingReport();
		if(branch == null){
			object.setBranch(prodView.getCreatedBy());
		}else{
			object.setBranch(branch);
		}
		
		object.setWarehouseName(prodView.getWarehousename());
		object.setStorageLocation(prodView.getStoragelocation());
		object.setStorageBin(prodView.getStoragebin());
		object.setProductId(prodView.getProdid());
		object.setProductCode(prodView.getProdcode());
		object.setProductName(prodView.getProdname());
		object.setOpeningStock(prodView.getAvailableqty());
		object.setTotalGrnQty(0);
		object.setTotalMinQty(0);
		object.setTotalMmnInQty(0);
		object.setTotalMmnOutQty(0);
		object.setTotalMmnTransferInQty(0);
		object.setTotalMmnTransferOutQty(0);
		object.setClosingStock(prodView.getAvailableqty());
		list.add(object);
		}
		return list;
	}
	void reactonBOMReport(){
		form.showWaitSymbol();
//		form.getFromDate().getValue(),form.getToDate().getValue(),branch
		serAsync.getServiceBOMReport(UserConfiguration.getCompanyId(), form.getFromDate().getValue(), form.getToDate().getValue()
						,form.getOlbBranch().getValue(form.getOlbBranch().getSelectedIndex()),new AsyncCallback<BOMServiceListBean>() {
							
							@Override
							public void onSuccess(BOMServiceListBean result) {
								// TODO Auto-generated method stub
								if(result.getServiceProList().size()!=0 
										|| result.getServiceCompeted().size() !=0 
										|| result.getServicePeddingLst().size()!=0){
									serAsync.getServiceGetUpdate(result, UserConfiguration.getCompanyId(), new AsyncCallback<BOMServiceListBean>() {
										
										@Override
										public void onSuccess(BOMServiceListBean result1) {
											// TODO Auto-generated method stub
											if(result1.getServiceProList().size()!=0 
													|| result1.getServiceCompeted().size() !=0 
													|| result1.getServicePeddingLst().size()!=0){
												serAsync.getServiceGetWithMrnUpdate(result1, UserConfiguration.getCompanyId(), new AsyncCallback<BOMServiceListBean>() {
													
													@Override
													public void onSuccess(BOMServiceListBean result2) {
														// TODO Auto-generated method stub
															if(result2!=null){

																
																String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
																final String url=gwt + "CreateXLXSServlet"+"?type="+4;
																Window.open(url, "test", "enabled");
																form.hideWaitSymbol();
															
															}
													}
													
													@Override
													public void onFailure(Throwable caught) {
														// TODO Auto-generated method stub
														form.hideWaitSymbol();
													}
												});
											}else{
												form.showDialogMessage("Error fetch data");
												form.hideWaitSymbol();
											}
										}
										
										@Override
										public void onFailure(Throwable caught) {
											// TODO Auto-generated method stub
											form.showDialogMessage("Failed");
											form.hideWaitSymbol();
										}
									});
								}else{
									form.showDialogMessage("Unable to load data. Try again!!");
									form.hideWaitSymbol();
								}
							}
							
							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("Data loading error.");
								form.hideWaitSymbol();
							}
						});
	}
	

	public ArrayList<Filter>  getContractFilter()
	{
		System.out.println("INSIDE FILTERS ");
		ArrayList<Filter> filtervec=new ArrayList<Filter>();
		Filter temp=null;
		
		temp=new Filter();
		
		
		

		if(!form.pic.getId().getValue().equals(""))
		{  
			temp=new Filter();
			temp.setIntValue(Integer.parseInt(form.pic.getId().getValue()));
			temp.setQuerryString("cinfo.count");
			filtervec.add(temp);
		}

		if(!(form.pic.getName().getValue().equals("")))
		{
			temp=new Filter();
			temp.setStringValue(form.pic.getName().getValue());
			temp.setQuerryString("cinfo.fullName");
			filtervec.add(temp);
		}
		
		if(form.olbBranch.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(form.olbBranch.getValue().trim());
			temp.setQuerryString("branch");
			filtervec.add(temp);
		}
		if(form.olbeSalesPerson.getSelectedIndex()!=0)
		{
			temp=new Filter();
			temp.setStringValue(form.olbeSalesPerson.getValue().trim());
			temp.setQuerryString("employee");
			filtervec.add(temp);
		}
		
		if(form.fromDate.getValue()!=null){
			temp = new Filter();
			temp.setQuerryString("creationDate >=");
			temp.setDateValue(form.fromDate.getValue());
			filtervec.add(temp);
		}
			
		if(form.toDate.getValue()!=null){
			temp = new Filter();
			temp.setQuerryString("creationDate <=");
			temp.setDateValue(form.toDate.getValue());
			filtervec.add(temp);
		}
		
		
		
		/*temp=new Filter();
		temp.setIntValue(0);
		temp.setQuerryString("refContractCount !=");
		filtervec.add(temp);
		
		temp=new Filter();
		temp.setIntValue(-1);
		temp.setQuerryString("refContractCount !=");
		filtervec.add(temp);*/

		temp.setQuerryString("companyId");
		if(UserConfiguration.getCompanyId()!=null)
			temp.setLongValue(UserConfiguration.getCompanyId());
		filtervec.add(temp);
		
		return filtervec;
	}
	
	public ArrayList<Filter>  getContractRenewalFilter()
	{
		System.out.println("INSIDE FILTERS ");
		ArrayList<Filter> filtervec=new ArrayList<Filter>();
		Filter temp=null;
		
		temp=new Filter();
		temp.setQuerryString("companyId");
		if(UserConfiguration.getCompanyId()!=null)
			temp.setLongValue(UserConfiguration.getCompanyId());
		filtervec.add(temp);
		

		if(!form.pic.getId().getValue().equals(""))
		{  
			temp=new Filter();
			temp.setIntValue(Integer.parseInt(form.pic.getId().getValue()));
			temp.setQuerryString("customerId");
			filtervec.add(temp);
		}

		if(!(form.pic.getName().getValue().equals("")))
		{
			temp=new Filter();
			temp.setStringValue(form.pic.getName().getValue());
			temp.setQuerryString("customerName");
			filtervec.add(temp);
		}
//		if(!form.pic.getPhone().getValue().equals(""))
//		{
//			temp=new Filter();
//			temp.setLongValue(Long.parseLong(form.pic.getPhone().getValue()));
//			temp.setQuerryString("customerCell");
//			filtervec.add(temp);
//		}
		
		if(form.olbBranch.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(form.olbBranch.getValue().trim());
			temp.setQuerryString("branch");
			filtervec.add(temp);
		}
		if(form.olbeSalesPerson.getSelectedIndex()!=0)
		{
			temp=new Filter();
			temp.setStringValue(form.olbeSalesPerson.getValue().trim());
			temp.setQuerryString("salesPerson");
			filtervec.add(temp);
		}
		
		if(form.fromDate.getValue()!=null){
			temp = new Filter();
			temp.setQuerryString("creationDate >=");
			temp.setDateValue(form.fromDate.getValue());
			filtervec.add(temp);
		}
			
		if(form.toDate.getValue()!=null){
			temp = new Filter();
			temp.setQuerryString("creationDate <=");
			temp.setDateValue(form.toDate.getValue());
			filtervec.add(temp);
		}
		

		temp=new Filter();
		temp.setIntValue(0);
		temp.setQuerryString("refContractCount >");
		filtervec.add(temp);		

		return filtervec;
	}
	
	public boolean validateDate(){
		/**
		 * Date : 24-10-2017  BY ANIL
		 * if branch restriction is on then we have to select branch selection is mandatory
		 */
		if (LoginPresenter.branchRestrictionFlag) {
			if (form.olbBranch.getSelectedIndex() == 0) {
				form.showDialogMessage("Please select branch !");
				return false;
			}
		}
		
		/**
		 * End
		 */
		Date formDate;
		Date toDate;
		if(form.pic.getValue()!=null ||!form.tbContractId.getValue().equals("")){
			return true;
		}else{
			if(form.fromDate.getValue()!=null&&form.toDate.getValue()!=null){
				formDate=form.fromDate.getValue();
				toDate=form.toDate.getValue();
				if(toDate.equals(formDate)||toDate.after(formDate)){
					return true;
				}else{
					form.showDialogMessage("To Date should be greater than From Date.");
					return false;
				}
				
			}
			else{
				form.showDialogMessage("Please select From Date and To Date!");
				return false;
			}
		}
		
	}
	private void reactOnBranchwiseSplitReport(){

		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		
		if(form.fromDate.getValue()!=null){
			temp = new Filter();
			temp.setQuerryString("invoiceDate >=");
			temp.setDateValue(form.fromDate.getValue());
			filtervec.add(temp);
		}
			
		if(form.toDate.getValue()!=null){
			temp = new Filter();
			temp.setQuerryString("invoiceDate <=");
			temp.setDateValue(form.toDate.getValue());
			filtervec.add(temp);
		}
		  
		  if(form.lbInvoiceStatus.getSelectedIndex()!=0){
				temp=new Filter();
				temp.setStringValue(form.lbInvoiceStatus.getItemText(form.lbInvoiceStatus.getSelectedIndex()));
				filtervec.add(temp);
				temp.setQuerryString("status");
				filtervec.add(temp);
			}	
		
				if(form.olbBranch.getSelectedIndex()!=0){
					temp=new Filter();
					temp.setStringValue(form.olbBranch.getValue().trim());
					temp.setQuerryString("branch");
					filtervec.add(temp);
				}
		
		MyQuerry querry= new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Invoice());
		genricServiceAsync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				ArrayList<Invoice> invoicingarray = new ArrayList<Invoice>();
				if(result == null && result.size() == 0){
					form.showDialogMessage("No Data Found.");
				}else{
					for(SuperModel model : result){
						Invoice invoice = (Invoice) model;
						invoicingarray.add(invoice);
					}
				csvservice.setinvoicelist(invoicingarray, new AsyncCallback<Void>() {

					@Override
					public void onFailure(Throwable caught) {
						System.out.println("RPC call Failed"+caught);
						
					}

					@Override
					public void onSuccess(Void result) {
						String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
						final String url=gwt + "csvservlet"+"?type="+170;
						Window.open(url, "test", "enabled");
					}
				});
				}
			}
		});	
	}
	
	
	private void reactOnServiceRevenueLossReport(final boolean flag, ArrayList<String> emailIdList) {
		Company company = new Company();
		ArrayList<String> strBranchList = new ArrayList<String>();
		ArrayList<Branch> brnachlist = (ArrayList<Branch>) form.olbBranch.getItems();
		for(Branch branch : brnachlist){
			strBranchList.add(branch.getBusinessUnitName());
		}
		System.out.println("Brnach list "+strBranchList);
//		genservice.getCancelledServiceOfApprovedContract(company.getCompanyId(), form.fromDate.getValue(), form.toDate.getValue(),strBranchList, new AsyncCallback<String>() {
//
//			@Override
//			public void onFailure(Throwable caught) {
//				// TODO Auto-generated method stub
//				
//			}
//
//			@Override
//			public void onSuccess(String result) {
//				// TODO Auto-generated method stub
//				if(flag){
//					if(result.equals("Success")){
//						if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "ServiceReport")){
//							String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
//							final String url = gwt + "csvservlet" + "?type=" + 137;
//							Window.open(url, "test", "enabled");
//						}else{
//							String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
//							final String url=gwt + "csvservlet"+"?type="+5;
//							Window.open(url, "test", "enabled");
//						}
//					}
//					else{
//						form.showDialogMessage(result);
//					}	
//				}
//				else{
//					form.showDialogMessage(result);
//				}
//				
//				
//				
//			}
//		});
		
		/**
		 * Date 04-11-2019 By Vijay Des :- Changed RPC due to getting some unknown Message on server on generalservice
		 */
		form.showWaitSymbol();
		serviceRevenuelossReport.getCancelledServiceOfApprovedContract(company.getCompanyId(), form.fromDate.getValue(), form.toDate.getValue(),flag,strBranchList,emailIdList, 
				new AsyncCallback<String>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						form.showDialogMessage("Failed");
						form.hideWaitSymbol();
						mailpopup.hidePopUp();
					}

					@Override
					public void onSuccess(String result) {
						// TODO Auto-generated method stub
						if(flag){
						if(result.equals("Success")){
							if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "ServiceReport")){
								String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
								final String url = gwt + "csvservlet" + "?type=" + 179;
								Window.open(url, "test", "enabled");
							}else{
								String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
								final String url=gwt + "csvservlet"+"?type="+5;
								Window.open(url, "test", "enabled");
							}
						}
						else{
							form.showDialogMessage(result);
						}
						}else{
							Console.log("Inside else of RPC");
						form.hideWaitSymbol();
						form.showDialogMessage(result);
						mailpopup.hidePopUp();
						}
					}
				});
		
		
		
		
	}
	
	private void reactonLockSealDeviationReport() {
		
		Company company = new Company();
		ArrayList<String> strBranchList = new ArrayList<String>();
		ArrayList<Branch> brnachlist = (ArrayList<Branch>) form.olbBranch.getItems();
		for(Branch branch : brnachlist){
			strBranchList.add(branch.getBusinessUnitName());
		}
		
		genservice.getLockSealDeviationReport(company.getCompanyId(), form.fromDate.getValue(), form.toDate.getValue(),strBranchList, new AsyncCallback<String>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(String result) {
				// TODO Auto-generated method stub
				if(result.equals("Success")){
					String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url=gwt + "csvservlet"+"?type="+177;
					Window.open(url, "test", "enabled");
				}
				else{
					form.showDialogMessage(result);
				}
			}
		});
		
	}
}
