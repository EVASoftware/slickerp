package com.slicktechnologies.client.views.reports;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.contractrenewal.ContractRenewalTable;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.DummyEntityOnlyForScreen;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

public class ReportForm extends FormScreen<DummyEntityOnlyForScreen>{

	public DateBox fromDate;
	public DateBox toDate;
	
	ObjectListBox<WareHouse> olbWarehouse;
	ObjectListBox<Branch> olbBranch;
	Button btdownload;
	Button btSendMail;
	ListBox olbContractStatus;
	
	
	ObjectListBox<Employee> olbeSalesPerson;
	TextBox tbContractId;
	PersonInfoComposite pic;
	Label label;
	/** date 19.4.2019 added by komal to load invoice status **/
	ListBox lbInvoiceStatus;
	public ReportForm() {
		super();
		createGui();
		InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
		for(int k=0;k<menus.length;k++)
		{
			String text=menus[k].getText();
				menus[k].setVisible(false);  		   
				
		}
	}
	private void initalizeWidget()
	{
		fromDate =new DateBoxWithYearSelector();
		toDate = new DateBoxWithYearSelector();
		olbBranch=new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbBranch);
	
		olbWarehouse=new ObjectListBox<WareHouse>();
		AppUtility.initializeWarehouse(olbWarehouse);
        btdownload = new Button("Download");
        btSendMail=new Button("Send Mail");
        /** date 14.03.2018 added by komal for nbhc **/
        olbContractStatus=new ListBox();
		AppUtility.setStatusListBox(olbContractStatus, Contract.getStatusList());

		


		
		olbBranch= new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbBranch);
		
		olbeSalesPerson=new ObjectListBox<Employee>();
		AppUtility.makeSalesPersonListBoxLive(olbeSalesPerson);
		
		tbContractId=new TextBox();
		
		MyQuerry querry=new MyQuerry();
		Filter filter=new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		querry.setQuerryObject(new Customer());
		pic = new PersonInfoComposite(querry, false);
		
		label=new Label("OR");
		/** date 19.4.2019 added by komal to load invoice status **/
		lbInvoiceStatus = new ListBox();
		AppUtility.setStatusListBox(lbInvoiceStatus, Invoice.getStatusList());
	
	}
	

	@Override
	public void toggleAppHeaderBarMenu() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void createScreen() {
		// TODO Auto-generated method stub
		initalizeWidget();
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingReportInformation=fbuilder.setlabel("Report Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		if(AppMemory.getAppMemory().currentScreen == Screen.ACTIVECONTRACTREPORT){
		fbuilder = new FormFieldBuilder("Active Contract From Date" , fromDate);
		}else if(AppMemory.getAppMemory().currentScreen == Screen.AMCASSUREDREVENUEREPORT){
			fbuilder = new FormFieldBuilder("AMC From Date" , fromDate);
		}else{
			fbuilder = new FormFieldBuilder("From Date" , fromDate);			
		}
		FormField ffromDate = fbuilder.setMandatory(true).setMandatoryMsg("From date is mandatory").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("To Date" , toDate);
		FormField ftoDate = fbuilder.setMandatory(true).setMandatoryMsg("To date is mandatory").setRowSpan(0).setColSpan(0).build();
	
		fbuilder = new FormFieldBuilder("Branch" , olbBranch);
		FormField folbBranch = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Status" , olbContractStatus);
		FormField folbContractStatus = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	
		fbuilder = new FormFieldBuilder("WareHouse" , olbWarehouse);
		FormField folbWarehouse= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();	
	
		fbuilder = new FormFieldBuilder("" , btdownload);
		FormField fbtdownload = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("" , btSendMail);
		FormField fbtSendMail = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		/*if(AppMemory.getAppMemory().currentScreen == Screen.REVENUEREPORT || AppMemory.getAppMemory().currentScreen == Screen.TECHNICIANREVENUEREPORT){
		FormField[][] formfield = {  
				{fgroupingReportInformation},
				{ffromDate ,ftoDate , fbtdownload}
		};
		this.fields=formfield;
		}
			10-04-2018
			for add branch filter for revenureport and technician revenuereport
		*/
		
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingSearchFilter=fbuilder.setlabel("Search Filters").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",pic);
		FormField pic= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Branch",olbBranch);
		FormField olbBranch= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Sales Person",olbeSalesPerson);
		FormField olbeSalesPerson= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("Contract Id",tbContractId);
		FormField tbContractId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingSearchResult=fbuilder.setlabel("Unprocessed Contract Renewals").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		
		
		fbuilder = new FormFieldBuilder("",label);
		FormField label= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		/** date 19.4.2019 added by komal to load invoice status **/
		fbuilder = new FormFieldBuilder("Status",lbInvoiceStatus);
		FormField flbInvoiceStatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		if(AppMemory.getAppMemory().currentScreen == Screen.REVENUEREPORT 
				|| AppMemory.getAppMemory().currentScreen == Screen.TECHNICIANREVENUEREPORT
				|| AppMemory.getAppMemory().currentScreen == Screen.SERVICEREVENUEREPORT
				|| AppMemory.getAppMemory().currentScreen == Screen.COMMODITYFUMIGATIONREPORT
				 || AppMemory.getAppMemory().currentScreen == Screen.BOMSERVICEMATRIALREPORT
				 || AppMemory.getAppMemory().currentScreen == Screen.SERVICEINVOICEREPORT){
			FormField[][] formfield = {  
					{fgroupingReportInformation},
					{ffromDate ,ftoDate  ,folbBranch,fbtdownload}
		};
			this.fields=formfield;
		}else 
		if(AppMemory.getAppMemory().currentScreen == Screen.CLOSINGSTOCKREPORT){
			FormField[][] formfield = { 
					{fgroupingReportInformation},
					{ffromDate , ftoDate , folbBranch , folbWarehouse},
					{fbtdownload }
		};
			this.fields=formfield;
		}else	
		/** date 14.03.2018 added by komal for nbhc reports **/
		if(AppMemory.getAppMemory().currentScreen == Screen.ACTIVECONTRACTREPORT){
			FormField[][] formfield = { 
					{fgroupingReportInformation},
					{ffromDate , folbBranch , folbContractStatus , fbtdownload}
		};
			this.fields=formfield;
		}else
		/** date 14.03.2018 added by komal fro nbhc reports **/
		if(AppMemory.getAppMemory().currentScreen == Screen.AMCASSUREDREVENUEREPORT){
			FormField[][] formfield = { 
					{fgroupingReportInformation},
					{ffromDate , folbBranch ,fbtdownload}
		};
			this.fields=formfield;
		}else
		if(AppMemory.getAppMemory().currentScreen == Screen.RENEWALCONTRACTREPORT){ /** nidih 20-12-2018 */
			FormField[][] formfield = {   
					{fgroupingSearchFilter},
					{pic},
					{label},
					{olbBranch,olbeSalesPerson,ffromDate ,ftoDate},
					{fbtdownload}
			};
			this.fields=formfield;	
		}else
		/** date 19.4.2019 added by komal to load invoice status **/
		if(AppMemory.getAppMemory().currentScreen == Screen.BRANCHWISESPLITINVOICEREPORT ){
			FormField[][] formfield = {  
					{fgroupingReportInformation},
					{ffromDate ,ftoDate  ,folbBranch,flbInvoiceStatus},
					{fbtdownload}
		};
			this.fields=formfield;
		}
		/**
		 * Date 26-07-2019 by Vijay
		 * Des :- NBHC CCPM Service Revenue loss report. cancelled services only for approved Contracts
		 */
		else if(AppMemory.getAppMemory().currentScreen == Screen.SERVICEREVENUELOSSREPORT){
			FormField[][] formfield = {  
					{fgroupingReportInformation},
					{ffromDate ,ftoDate,folbBranch},
					{fbtdownload,fbtSendMail}
			};
			this.fields=formfield;
		}
		/**
		 * Date 02-09-2019 by Vijay
		 * Des :- NBHC Inventory Management Deviation Report
		 */
		else if(AppMemory.getAppMemory().currentScreen == Screen.LOCKSEALDEVIATIONREPORT){
			FormField[][] formfield = {  
					{fgroupingReportInformation},
					{ffromDate ,ftoDate,folbBranch},
					{fbtdownload}
			};
			this.fields=formfield;
		}
	}

	@Override
	public void updateModel(DummyEntityOnlyForScreen model) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateView(DummyEntityOnlyForScreen model) {
		// TODO Auto-generated method stub
		
	}
	public DateBox getFromDate() {
		return fromDate;
	}
	public void setFromDate(DateBox fromDate) {
		this.fromDate = fromDate;
	}
	public DateBox getToDate() {
		return toDate;
	}
	public void setToDate(DateBox toDate) {
		this.toDate = toDate;
	}
	public ObjectListBox<WareHouse> getOlbWarehouse() {
		return olbWarehouse;
	}
	public void setOlbWarehouse(ObjectListBox<WareHouse> olbWarehouse) {
		this.olbWarehouse = olbWarehouse;
	}
	public ObjectListBox<Branch> getOlbBranch() {
		return olbBranch;
	}
	public void setOlbBranch(ObjectListBox<Branch> olbBranch) {
		this.olbBranch = olbBranch;
	}
	public Button getBtdownload() {
		return btdownload;
	}
	public void setBtdownload(Button btdownload) {
		this.btdownload = btdownload;
	}
	
	
	
	
	public Button getBtSendMail() {
		return btSendMail;
	}
	public void setBtSendMail(Button btSendMail) {
		this.btSendMail = btSendMail;
	}
	public ListBox getOlbContractStatus() {
		return olbContractStatus;
	}
	public void setOlbContractStatus(ListBox olbContractStatus) {
		this.olbContractStatus = olbContractStatus;
	}
	public ObjectListBox<Employee> getOlbeSalesPerson() {
		return olbeSalesPerson;
	}
	public void setOlbeSalesPerson(ObjectListBox<Employee> olbeSalesPerson) {
		this.olbeSalesPerson = olbeSalesPerson;
	}
	public PersonInfoComposite getPic() {
		return pic;
	}
	public void setPic(PersonInfoComposite pic) {
		this.pic = pic;
	}
	public Label getLabel() {
		return label;
	}
	public void setLabel(Label label) {
		this.label = label;
	}

}
