package com.slicktechnologies.client.views.emailtemplateconfig;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.config.CategoryTypeFactory;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.email.EmailTemplate;
import com.slicktechnologies.shared.common.email.EmailTemplateConfiguration;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class EmailTemplateConfigurationForm extends FormTableScreen<EmailTemplateConfiguration> implements ChangeHandler {

	ObjectListBox<ConfigCategory> oblModuleName;
	ObjectListBox<Type> oblDocumentName;

	ObjectListBox<EmailTemplate> oblTemplateName;

	TextBox tbtemplateconfigId;
	CheckBox cbStatus;
	
	EmailTemplateConfiguration emailTemplateConfigobj;
	
	ObjectListBox<String> oblCommunicationChannel;
	CheckBox  chkautomaticmsg, chkSendAutoMsg;


	public EmailTemplateConfigurationForm  (SuperTable<EmailTemplateConfiguration> table, int mode,
			boolean captionmode) {
		super(table, mode, captionmode);
		createGui();

	}
	
	private void initalizeWidget() {
		
		oblModuleName = new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(oblModuleName, Screen.MODULENAME);
		oblModuleName.addChangeHandler(this);

		oblDocumentName = new ObjectListBox<Type>();
		AppUtility.makeTypeListBoxLive(this.oblDocumentName, Screen.DOCUMENTNAME);

		oblTemplateName = new ObjectListBox<EmailTemplate>();
		oblTemplateName.addItem("--SELECT--");
		AppUtility.MakeliveEmailTemplate(oblTemplateName);

		
		tbtemplateconfigId = new TextBox();
		tbtemplateconfigId.setEnabled(false);
		
		cbStatus = new CheckBox();
		cbStatus.setValue(true);
		
		oblCommunicationChannel = new ObjectListBox<String>();
		oblCommunicationChannel.addItem("--SELECT--");
		oblCommunicationChannel.addItem("Email");
		oblCommunicationChannel.addItem("SMS");
		oblCommunicationChannel.addItem("WhatsApp");

		oblCommunicationChannel.addChangeHandler(this);

		chkautomaticmsg = new CheckBox();
		chkautomaticmsg.setTitle("Enable  template to sent message automatically i.e. on contract approval, quotation  approval etc");
		chkautomaticmsg.setEnabled(false);
		
		chkSendAutoMsg = new CheckBox();
		chkSendAutoMsg.setTitle("Enable to send the message automatically i.e. on cotranct approval, quotation approval");
		chkSendAutoMsg.setEnabled(false);
		
	}

	@Override
	public void createScreen() {
		
		initalizeWidget();
		
		
		this.processlevelBarNames=new String[]{"New"};
		
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingEmailInformation=fbuilder.setlabel("Message Template Confuguration").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		
		fbuilder = new FormFieldBuilder("ID",tbtemplateconfigId);
		FormField ftbtemplateconfigId = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Module Name",oblModuleName);
		FormField foblModuleName= fbuilder.setMandatory(true).setMandatoryMsg("Module Name is Mandatory!").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Document Name",oblDocumentName);
		FormField foblDocumentName = fbuilder.setMandatory(true).setMandatoryMsg("Document Name is Mandatory!").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Template Name",oblTemplateName);
		FormField goblTemplateName = fbuilder.setMandatory(true).setMandatoryMsg("Template Name is Mandatory!").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Communication Channel",oblCommunicationChannel);
		FormField foblCommunicationChannel = fbuilder.setMandatory(true).setMandatoryMsg("Communication Channel is Mandatory!").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Status",cbStatus);
		FormField fchkbxStatus = fbuilder.setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("Auto Message",chkautomaticmsg);
		FormField fchkautomaticmsg = fbuilder.setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("Send Auto Message",chkSendAutoMsg);
		FormField fchkSendAutoMsg = fbuilder.setRowSpan(0).setColSpan(0).build();
		
		
		FormField[][] formfield = { {fgroupingEmailInformation},
				  {ftbtemplateconfigId,foblModuleName,foblDocumentName,foblCommunicationChannel,goblTemplateName},
				  {fchkbxStatus,fchkautomaticmsg,fchkSendAutoMsg},
				  };

				this.fields=formfield;		
		
	}


	@Override
	public void updateModel(EmailTemplateConfiguration model) 
	{
		if(oblModuleName.getSelectedIndex()!=0)
			model.setModuleName(oblModuleName.getValue());
		if(oblDocumentName.getSelectedIndex()!=0){
			model.setDocumentName(oblDocumentName.getValue(oblDocumentName.getSelectedIndex()));
		}
		
		if(oblTemplateName.getSelectedIndex()!=0){
			model.setTemplateName(oblTemplateName.getValue(oblTemplateName.getSelectedIndex()));
			Console.log("Template Name"+oblTemplateName.getValue(oblTemplateName.getSelectedIndex()));
		}
		
		if(cbStatus.getValue()!=null)
			model.setStatus(cbStatus.getValue());
		
		if(oblCommunicationChannel.getSelectedIndex()!=0){
			model.setCommunicationChannel(oblCommunicationChannel.getValue(oblCommunicationChannel.getSelectedIndex()));
		}
		
		if(chkautomaticmsg.getValue()!=null) {
			model.setAutomaticMsg(chkautomaticmsg.getValue());
		}
		if(chkSendAutoMsg.getValue()!=null) {
			model.setSendMsgAuto(chkSendAutoMsg.getValue());
		}
		
		manageGlobalList(model);
		
		emailTemplateConfigobj=model;
		presenter.setModel(model);
	}
	
	
	@Override
	public void updateView(EmailTemplateConfiguration view) 
	{
		emailTemplateConfigobj=view;
		
		if(view.getCommunicationChannel()!=null && !view.getCommunicationChannel().equals("")
				&& view.getCommunicationChannel().equals("Email")){
			AppUtility.MakeliveEmailTemplate(oblTemplateName);
			
		}
		else if(view.getCommunicationChannel()!=null && !view.getCommunicationChannel().equals("")
				&& (view.getCommunicationChannel().equals("SMS") ||  view.getCommunicationChannel().equals(AppConstants.WHATSAPP))){
			reactonInitilizaSMSTemplate(view.getTemplateName());
		}
		Console.log("oblTemplateName item"+oblTemplateName.getItemCount());
		
		
		if(view.getModuleName()!=null)
			oblModuleName.setValue(view.getModuleName());
		
		if(view.getDocumentName()!=null)
			oblDocumentName.setValue(view.getDocumentName());
		if(view.getTemplateName()!=null)
			oblTemplateName.setValue(view.getTemplateName());
		
		tbtemplateconfigId.setValue(view.getCount()+"");
		cbStatus.setValue(view.isStatus());
		
		if(view.getCommunicationChannel()!=null){
			oblCommunicationChannel.setValue(view.getCommunicationChannel());
		}
		
		chkautomaticmsg.setValue(view.isAutomaticMsg());
		chkSendAutoMsg.setValue(view.isSendMsgAuto());
		
		final String templateName =view.getTemplateName();
		Timer timer = new Timer() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				if(templateName!=null && !templateName.equals("")) {
					oblTemplateName.setValue(templateName);
				}
				
			}
		};
		timer.schedule(3000);//Ashwini Patil Date: 18-05-2022 Changed timer timing as email template name was not getting updated
		
		
		presenter.setModel(view);

	}	
	
	
	/**
	 * Toggles the app header bar menus as per screen state
	 */

	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.EMAILTEMPLATECONFIGURATION,LoginPresenter.currentModule.trim());
	}

	/**
	 * sets the id textbox with the passed count value. 
	 */
	@Override
	public void setCount(int count)
	{
		tbtemplateconfigId.setValue(count+"");	
	}

	
	
	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		tbtemplateconfigId.setEnabled(false);
		chkautomaticmsg.setEnabled(false);
	}

	@Override
	public boolean validate() {
		boolean superRes=super.validate();
		
	    return superRes;
	}

	@Override
	public void setToViewState() {
		super.setToViewState();
		

		SuperModel model=new SmsTemplate();
		model=emailTemplateConfigobj;
		AppUtility.addDocumentToHistoryTable(AppConstants.SETTINGSMODULE,AppConstants.EMAILTEMPLATECONFIGURATION, emailTemplateConfigobj.getCount(), null,null,null, false, model, null);
	
//		if(emailTemplateConfigobj.getCommunicationChannel().equals("Email")){
//			AppUtility.MakeliveEmailTemplate(oblTemplateName);
//
//		}
//		else if(emailTemplateConfigobj.getCommunicationChannel().equals("SMS")){
//			reactonInitilizaSMSTemplate();
//		}
		
	}

	
	@Override
	public void setToNewState() {
		super.setToNewState();
		
		System.out.println("Inside NEW method");
		if(emailTemplateConfigobj!=null){
			SuperModel model=new SmsTemplate();
			model=emailTemplateConfigobj;
			AppUtility.addDocumentToHistoryTable(AppConstants.SETTINGSMODULE,AppConstants.EMAILTEMPLATECONFIGURATION, emailTemplateConfigobj.getCount(), null,null,null, false, model, null);
		
		}
	}

	@Override
	public void onChange(ChangeEvent event) {

		if (event.getSource().equals(oblModuleName)) {
			if (oblModuleName.getSelectedIndex() != 0) {
				ConfigCategory cat = oblModuleName.getSelectedItem();
				if (cat != null) {
					AppUtility.makeLiveTypeDropDown(oblDocumentName,cat.getCategoryName(), cat.getCategoryCode(),cat.getInternalType());
				}
			}
		}
		
		if (event.getSource().equals(oblCommunicationChannel)) {
			if(oblCommunicationChannel.getValue(oblCommunicationChannel.getSelectedIndex()).equals("Email")){
				AppUtility.MakeliveEmailTemplate(oblTemplateName);

			}
			else if(oblCommunicationChannel.getValue(oblCommunicationChannel.getSelectedIndex()).equals("SMS") ||
					oblCommunicationChannel.getValue(oblCommunicationChannel.getSelectedIndex()).equals(AppConstants.WHATSAPP)){
				reactonInitilizaSMSTemplate(null);
			}
			else{
				oblCommunicationChannel.clear();
				oblCommunicationChannel.addItem("--SELECT--");
			}
		}

	}
	
	

	public void manageGlobalList(EmailTemplateConfiguration emailTemplateConfigModel)
	{
		ScreeenState scrState=AppMemory.getAppMemory().currentState;
		
		if(scrState.equals(ScreeenState.NEW))
		{
			LoginPresenter.globalEmailTemplateConfiglist.add(emailTemplateConfigModel);
		}
		if(scrState.equals(ScreeenState.EDIT)){
			for(int i=0;i<LoginPresenter.globalEmailTemplateConfiglist.size();i++)
			{
				if(LoginPresenter.globalEmailTemplateConfiglist.get(i).getId().equals(emailTemplateConfigModel.getId()))
				{
					LoginPresenter.globalEmailTemplateConfiglist.add(emailTemplateConfigModel);
					LoginPresenter.globalEmailTemplateConfiglist.remove(i);
				}
			}
		}
		
	}

	
	private void reactonInitilizaSMSTemplate(final String eventName) {

		oblTemplateName.clear();
		
		final GenricServiceAsync genasync=GWT.create(GenricService.class);

		MyQuerry querry = new MyQuerry();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new SmsTemplate());
		
		genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				Console.log("SMS template Size"+result.size());
				oblTemplateName.addItem("--SELECT--");
				for(SuperModel model : result){
					SmsTemplate smsTemplateEntity = (SmsTemplate) model;
					oblTemplateName.addItem(smsTemplateEntity.getEvent());
					

					ScreeenState scrState=AppMemory.getAppMemory().currentState;
					if(scrState.equals(ScreeenState.NEW))
					{
						if(smsTemplateEntity.getAutomaticMsg()!=null)
						chkautomaticmsg.setValue(smsTemplateEntity.getAutomaticMsg());
						
						if(chkautomaticmsg.getValue()!=null && chkautomaticmsg.getValue()) {
							chkSendAutoMsg.setEnabled(true);
						}
					}
				}
				if(eventName!=null){
					Console.log("eventName "+eventName);
					oblTemplateName.setValue(eventName);;
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
		
	}

	@Override
	public void setToEditState() {
		super.setToEditState();
		
		if(chkautomaticmsg.getValue()!=null && chkautomaticmsg.getValue()) {
			chkSendAutoMsg.setEnabled(true);
		}
	}
	
	

}
