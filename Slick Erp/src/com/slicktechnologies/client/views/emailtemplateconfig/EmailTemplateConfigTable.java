package com.slicktechnologies.client.views.emailtemplateconfig;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.email.EmailTemplate;
import com.slicktechnologies.shared.common.email.EmailTemplateConfiguration;

public class EmailTemplateConfigTable extends SuperTable<EmailTemplateConfiguration>{
	
	
	TextColumn<EmailTemplateConfiguration> getcolumnId;
	TextColumn<EmailTemplateConfiguration> getcolumnModuleName;
	TextColumn<EmailTemplateConfiguration> getcolumnDocumentName;
	TextColumn<EmailTemplateConfiguration> getcolumnTemplateName;
	TextColumn<EmailTemplateConfiguration> getcolumnStatus;
	TextColumn<EmailTemplateConfiguration> getcolumnDefault;
	TextColumn<EmailTemplateConfiguration> getcolumnCommunicationChannel;

	TextColumn<EmailTemplateConfiguration> getcolumnAutomaticmsg;
	TextColumn<EmailTemplateConfiguration> getcolumnSendAutoMsg;

	public EmailTemplateConfigTable(){
		super();
	}

	@Override
	public void createTable() {

		addColumnId();
		addColumnModuleName();
		addColumnDocumentName();
		addColumnCommunicationChannel();
		addColumnTemplateName();

		addColumnAutomaticMsg();
		addColumnSendAutoMsg();

		addColumnStatus();
		
	}

	private void addColumnSendAutoMsg() {
		getcolumnSendAutoMsg = new TextColumn<EmailTemplateConfiguration>() {
			
			@Override
			public String getValue(EmailTemplateConfiguration object) {
				if(object.isSendMsgAuto()) {
					return "YES";
				}
				else {
					return "NO";
				}
			}
		};
		table.addColumn(getcolumnSendAutoMsg, "Send Message Automatic");
		getcolumnId.setSortable(true);
		table.setColumnWidth(getcolumnSendAutoMsg, 100, Unit.PX);
	}

	private void addColumnAutomaticMsg() {
		
		getcolumnAutomaticmsg = new TextColumn<EmailTemplateConfiguration>() {
			
			@Override
			public String getValue(EmailTemplateConfiguration object) {
				if(object.isAutomaticMsg()) {
					return "Automatic";
				}
				else {
					return "Manual";
				}
			}
		};
		table.addColumn(getcolumnAutomaticmsg, "Message Type");
		getcolumnId.setSortable(true);
		table.setColumnWidth(getcolumnAutomaticmsg, 100, Unit.PX);
	}

	private void addColumnCommunicationChannel() {
		getcolumnCommunicationChannel = new TextColumn<EmailTemplateConfiguration>() {
			
			@Override
			public String getValue(EmailTemplateConfiguration object) {
				if(object.getCommunicationChannel()!=null){
					return object.getCommunicationChannel();
				}
				else{
					return "";
				}
			}
		};
		table.addColumn(getcolumnCommunicationChannel, "Communication Channel");
		getcolumnId.setSortable(true);
		table.setColumnWidth(getcolumnCommunicationChannel, 100, Unit.PX);
	}

	private void addColumnId() {

		getcolumnId = new TextColumn<EmailTemplateConfiguration>() {
			
			@Override
			public String getValue(EmailTemplateConfiguration object) {
					
				return object.getCount()+"";
			}
		};
		table.addColumn(getcolumnId, "ID");
		getcolumnId.setSortable(true);
		table.setColumnWidth(getcolumnId, 100, Unit.PX);
	
	
	}

	private void addColumnModuleName() {

		getcolumnModuleName = new TextColumn<EmailTemplateConfiguration>() {
			
			@Override
			public String getValue(EmailTemplateConfiguration object) {
				if(object.getModuleName()!=null){
					return object.getModuleName();
				}
				else{
					return "";
				}
			}
		};
		table.addColumn(getcolumnModuleName, "Module Name");
		getcolumnModuleName.setSortable(true);
		table.setColumnWidth(getcolumnModuleName, 100, Unit.PX);
	
	}

	private void addColumnDocumentName() {

		getcolumnDocumentName = new TextColumn<EmailTemplateConfiguration>() {
			
			@Override
			public String getValue(EmailTemplateConfiguration object) {
				if(object.getDocumentName()!=null){
					return object.getDocumentName();
				}
				else{
					return "";
				}
			}
		};
		table.addColumn(getcolumnDocumentName, "Document Name");
		getcolumnDocumentName.setSortable(true);
		table.setColumnWidth(getcolumnDocumentName, 100, Unit.PX);
		
	
	}

	private void addColumnTemplateName() {

		getcolumnTemplateName = new TextColumn<EmailTemplateConfiguration>() {
			
			@Override
			public String getValue(EmailTemplateConfiguration object) {
				if(object.getTemplateName()!=null){
					return object.getTemplateName();
				}
				else{
					return "";
				}
			}
		};
		table.addColumn(getcolumnTemplateName, "Template Name");
		getcolumnTemplateName.setSortable(true);
		table.setColumnWidth(getcolumnTemplateName, 100, Unit.PX);
		
	
	}

	private void addColumnStatus() {
		
		getcolumnStatus = new TextColumn<EmailTemplateConfiguration>() {
			
			@Override
			public String getValue(EmailTemplateConfiguration object) {
				if(object.isStatus()){
					return "Active";
				}
				else{
					return "InActive";
				}
			}
		};
		table.addColumn(getcolumnStatus, "Status");
		getcolumnStatus.setSortable(true);
		table.setColumnWidth(getcolumnStatus, 100, Unit.PX);
		
	}

	
	public void addColumnSorting(){
		addSortingCount();
		addSortingModuleName();
		addSortingDocumentName();
		addSortingTemplateName();
		addSortingStatus();

	}

	private void addSortingCount() {

		List<EmailTemplateConfiguration> list=getDataprovider().getList();
		columnSort=new ListHandler<EmailTemplateConfiguration>(list);
		columnSort.setComparator(getcolumnId, new Comparator<EmailTemplateConfiguration>()
				{
			@Override
			public int compare(EmailTemplateConfiguration e1,EmailTemplateConfiguration e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
			
			
	}

	private void addSortingModuleName() {
		List<EmailTemplateConfiguration> list = getDataprovider().getList();
		columnSort = new ListHandler<EmailTemplateConfiguration>(list);
		columnSort.setComparator(getcolumnModuleName, new Comparator<EmailTemplateConfiguration>() {

			@Override
			public int compare(EmailTemplateConfiguration e1, EmailTemplateConfiguration e2) {
				
				if(e1!=null && e2!=null)
					{
						if( e1.getModuleName()!=null && e2.getModuleName()!=null){
							return e1.getModuleName().compareTo(e2.getModuleName());}
					}
					else{
						return 0;}
					return 0;
			}
		});
		
		table.addColumnSortHandler(columnSort); 
			
	}

	private void addSortingDocumentName() {
		List<EmailTemplateConfiguration> list = getDataprovider().getList();
		columnSort = new ListHandler<EmailTemplateConfiguration>(list);
		columnSort.setComparator(getcolumnDocumentName, new Comparator<EmailTemplateConfiguration>() {

			@Override
			public int compare(EmailTemplateConfiguration e1, EmailTemplateConfiguration e2) {
				
				if(e1!=null && e2!=null)
					{
						if( e1.getDocumentName()!=null && e2.getDocumentName()!=null){
							return e1.getDocumentName().compareTo(e2.getDocumentName());}
					}
					else{
						return 0;}
					return 0;
			}
		});
		
		table.addColumnSortHandler(columnSort); 
			
	}

	private void addSortingTemplateName() {
		List<EmailTemplateConfiguration> list = getDataprovider().getList();
		columnSort = new ListHandler<EmailTemplateConfiguration>(list);
		columnSort.setComparator(getcolumnTemplateName, new Comparator<EmailTemplateConfiguration>() {

			@Override
			public int compare(EmailTemplateConfiguration e1, EmailTemplateConfiguration e2) {
				
				if(e1!=null && e2!=null)
					{
						if( e1.getTemplateName()!=null && e2.getTemplateName()!=null){
							return e1.getTemplateName().compareTo(e2.getTemplateName());}
					}
					else{
						return 0;}
					return 0;
			}
		});
		
		table.addColumnSortHandler(columnSort); 
			
	}

	private void addSortingStatus() {
		List<EmailTemplateConfiguration> list = getDataprovider().getList();
		columnSort = new ListHandler<EmailTemplateConfiguration>(list);
		columnSort.setComparator(getcolumnStatus, new Comparator<EmailTemplateConfiguration>() {
			
			@Override
			public int compare(EmailTemplateConfiguration e1, EmailTemplateConfiguration e2) {

				if(e1!=null && e2!=null)
				{
					if(e1.isStatus()==e2.isStatus()){
						return 0;
					}
					else{
						return -1;
					}
				}
				else{
					return 0;
				}
				
			}
		});
		
		table.addColumnSortHandler(columnSort);
	}


	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
