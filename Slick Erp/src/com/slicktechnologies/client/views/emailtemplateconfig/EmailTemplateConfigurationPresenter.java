package com.slicktechnologies.client.views.emailtemplateconfig;

import java.util.Vector;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.emailtemplate.EmailTemplateForm;
import com.slicktechnologies.client.views.emailtemplate.EmailTemplatePresenter;
import com.slicktechnologies.client.views.emailtemplate.EmailTemplateTable;
import com.slicktechnologies.shared.common.email.EmailTemplate;
import com.slicktechnologies.shared.common.email.EmailTemplateConfiguration;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class EmailTemplateConfigurationPresenter extends FormTableScreenPresenter<EmailTemplateConfiguration>{

	EmailTemplateConfigurationForm form;
	
	public EmailTemplateConfigurationPresenter(FormTableScreen<EmailTemplateConfiguration> view,
			EmailTemplateConfiguration model) {
		super(view, model);
		form=(EmailTemplateConfigurationForm) view;
		form.getSupertable().connectToLocal();
		form.retriveTable(getQuery());
		form.setPresenter(this);
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.EMAILTEMPLATE,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}


	public static EmailTemplateConfigurationForm initalize() {
		
		EmailTemplateConfigTable gentableScreen=new EmailTemplateConfigTable();
		
		EmailTemplateConfigurationForm  form=new  EmailTemplateConfigurationForm(gentableScreen,FormTableScreen.UPPER_MODE,true);
		gentableScreen.setView(form);
		gentableScreen.applySelectionModle();

		EmailTemplateConfigurationPresenter  presenter=new  EmailTemplateConfigurationPresenter(form,new EmailTemplateConfiguration());
		AppMemory.getAppMemory().stickPnel(form);
		return form;
		
	}


	/**
	 * Method template to set the processBar events
	 */

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {

		InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();
		
		if(text.equals("New"))
		{
			form.setToNewState();
			initalize();
		}	
	}


	@Override
	protected void makeNewModel() {
		model=new EmailTemplateConfiguration();

	}
	
	public void setModel(EmailTemplateConfiguration entity)
	{
		model=entity;
	}


	private MyQuerry getQuery() {
		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new EmailTemplateConfiguration());
		return quer;
	}
	
	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

}
