package com.slicktechnologies.client.views.serviceliaison.liaisonsteps;


import java.util.ArrayList;
import java.util.List;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.CompositeInterface;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.serviceLiaisonLayer.LiaisonStep;
import com.slicktechnologies.shared.common.serviceLiaisonLayer.LiaisonSubstep;

public class LiaisonStepsForm extends FormTableScreen<LiaisonStep> implements ClickHandler,HasValue<ArrayList<LiaisonSubstep>>,CompositeInterface
{
	IntegerBox stepsId;
	TextBox stepsName;
	TextBox tbresponsestep;
	TextArea description;
	CheckBox status,mandatory;
	
	IntegerBox days,ibresponseSLA;
	
	ObjectListBox<LiaisonStep> olbpreRequisiteStep;
	Button addResponse;
	CheckBox cbreponsemandatory;
	
	TextArea tadescriptionResponse;
	ReponseStepInfoTable table;
	InlineLabel label;
	LiaisonStep liasionobj;
	
	public LiaisonStepsForm(SuperTable<LiaisonStep> table, int mode,
			boolean captionmode) {
		super(table, mode, captionmode);
		createGui();
		table.connectToLocal();
	}

	private void initalizeWidget()
	{
		stepsId=new IntegerBox();
		stepsId.setEnabled(false);
		stepsName=new TextBox();
		description=new TextArea();
		status=new CheckBox();
		status.setValue(true);
	
		this.olbpreRequisiteStep=new ObjectListBox<LiaisonStep>();
		days=new IntegerBox();
		
		
		
		
		mandatory=new CheckBox();
		mandatory.addClickHandler(this);
		
		tbresponsestep=new TextBox();
		ibresponseSLA=new IntegerBox();
		cbreponsemandatory=new CheckBox();
		
		tadescriptionResponse=new TextArea();
		addResponse=new Button("Add Response");
		addResponse.addClickHandler(this);
		table=new ReponseStepInfoTable();
		label=new InlineLabel("*For edit response steps delete and add again");
		this.initalizeSteps();
	}

	/*
	 * Method template to create the formtable screen
	 */

	@Override
	public void createScreen() {

		//Token to initialize the processlevel menus.
		
		
		initalizeWidget();

		
		//////////////////////////////////Form Field Declaration/////////////////////////////////////////////////

		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////

		//Token to initialize formfield
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingStepsInformation=fbuilder.setlabel("Request steps Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(3).build();
		fbuilder = new FormFieldBuilder("Step ID",stepsId);
		FormField fstepsId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Name",stepsName);
		FormField fstepsName= fbuilder.setMandatory(true).setMandatoryMsg("Name is mandatory").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Description",description);
		FormField fdescription= fbuilder.setMandatory(true).setMandatoryMsg("Description is mandatory").setRowSpan(0).setColSpan(3).build();
		fbuilder = new FormFieldBuilder("Status",status);
		FormField fstatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
				fbuilder = new FormFieldBuilder("Parent Step",olbpreRequisiteStep);
		FormField folbpreRequisiteStep= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* SLA (Days)",days);
		FormField fdays= fbuilder.setMandatory(true).setMandatoryMsg("No of Days is mandatory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Mandatory",mandatory);
		FormField fmandatory= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	
		FormField fgroupingResponseInformation=fbuilder.setlabel("Response steps Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(3).build();
		fbuilder = new FormFieldBuilder("* Name",tbresponsestep);
		FormField ftbresponsestep= fbuilder.setMandatory(false).setMandatoryMsg("Substep name Mandatory").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* SLA",ibresponseSLA);
		FormField fibresponseSLA= fbuilder.setMandatory(false).setMandatoryMsg("SLA is mandatory").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Mandatory",cbreponsemandatory);
		FormField fcbreponsemandatory= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Description",tadescriptionResponse);
		FormField ftadescriptionResponse= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		fbuilder = new FormFieldBuilder("",addResponse);
		FormField faddResponse= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		FormField fgroupingResponseStepsInformation=fbuilder.setlabel("Response steps").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(3).build();
		
		
		
		fbuilder = new FormFieldBuilder("",table.getTable());
		FormField ftable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();

		fbuilder = new FormFieldBuilder("",label);
		FormField flabel= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	
		
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////


		FormField[][] formfield = {   {fgroupingStepsInformation},
				{fstepsId,fstepsName,fdays},
				{fdescription},{folbpreRequisiteStep,fmandatory,fstatus}
				,{fgroupingResponseInformation},
				{ftbresponsestep,fibresponseSLA,fcbreponsemandatory},
				{ftadescriptionResponse},{faddResponse,flabel},{fgroupingResponseStepsInformation},
				{ftable}
		};


		this.fields=formfield;		
	}

	
	/**
	 * method template to update the model with token entity name
	 */
	@Override
	public void updateModel(LiaisonStep model) 
	{
		
		if(stepsName.getValue()!=null)
			model.setStepName(stepsName.getValue());
		if(description.getValue()!=null)
			model.setDescription(description.getValue());
		model.setSLA(days.getValue());
				if(olbpreRequisiteStep.getValue()!=null)
			model.setParentStep(olbpreRequisiteStep.getValue());
		else
			model.setParentStep("");
		
		if(status.getValue()!=null)
			model.setStatus(status.getValue());
	//		model.setStatus("Active");
		//else
		//	model.setStatus("Inactive");
		if(mandatory.getValue()!=null)
			model.setMandatory(mandatory.getValue());
		
		List<LiaisonSubstep> daysalloc = table.getDataprovider().getList();
	    ArrayList<LiaisonSubstep> alloc = new ArrayList<LiaisonSubstep>();
	    alloc.addAll(daysalloc);
		model.setResponseinfo(alloc);

		liasionobj=model;
		presenter.setModel(model);
		//	presenter.setModel(model);
	}

	/**
	 * method template to update the view with token entity name
	 */
	@Override
	public void updateView(LiaisonStep view) 
	{
		
		liasionobj=view;
		
		stepsId.setValue(view.getCount());
		if(view.getStepName()!=null)
			stepsName.setValue(view.getStepName());
		if(view.getDescription()!=null)
			description.setValue(view.getDescription());
		days.setValue(view.getSLA());
				if(view.getParentStep()!=null)
			olbpreRequisiteStep.setValue(view.getParentStep());
		
		status.setValue(view.getStatus());
			
		mandatory.setValue(view.getMandatory());
			
		
			if(view.getResponseinfo()!=null)
				table.setValue(view.getResponseinfo());
				presenter.setModel(view);

	}

	/**
	 * Toggles the app header bar menus as per screen state
	 */

	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		
		 AuthorizationHelper.setAsPerAuthorization(Screen.LIAISONSTEPS,LoginPresenter.currentModule.trim());
	}
	/**
	 * sets the id textbox with the passed count value. 
	 */
	@Override
	public void setCount(int count)
	{
		this.stepsId.setValue(presenter.getModel().getCount());
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		super.setEnable(state);
		stepsId.setEnabled(false);
	}
	
	@Override
	public void onClick(ClickEvent event)
	{
		
		
		if(event.getSource()==this.addResponse)
		{
			if(validateSubstep()&&tbresponsestep.getValue()!=null)
			{
		
			
			LiaisonSubstep info=new LiaisonSubstep();
			info.setReponseName(tbresponsestep.getValue());
			info.setReponseSLA(ibresponseSLA.getValue());
			info.setReponseMandatory(cbreponsemandatory.getValue());
			
			info.setReponseDescription(tadescriptionResponse.getValue());
			table.getDataprovider().getList().add(info);
			
			tbresponsestep.setValue("");
			ibresponseSLA.setValue(null);
			tadescriptionResponse.setValue("");
		
			}
			}
	
	}
	protected void initalizeSteps()
	{
		MyQuerry querry = new MyQuerry();
		querry.setQuerryObject(new LiaisonStep());
		olbpreRequisiteStep.MakeLive(querry);
		
		
	}
	
	@Override
	public boolean validate()
	{
		boolean superRes= super.validate();
		if(superRes==false)
			return false;
	
	if(table.getDataprovider().getList().isEmpty()==true)
	{
		this.showDialogMessage("Please Add Reponse Steps");
		return false;
	}
	
	
	if(stepsName.getValue().trim().equals(olbpreRequisiteStep.getValue()))
	{
		this.showDialogMessage("Request step and Parent step cant be same");
		return false;
	}
		return true;
	}
	

	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<ArrayList<LiaisonSubstep>> handler) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void fireEvent(GwtEvent<?> event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ArrayList<LiaisonSubstep> getValue() {
		List<LiaisonSubstep>list=this.table.getDataprovider().getList();
		ArrayList<LiaisonSubstep>vec=new ArrayList<LiaisonSubstep>();
		vec.addAll(list);
		return vec;
	}

	@Override
	public void setValue(ArrayList<LiaisonSubstep> value) {
		table.connectToLocal();
		table.getDataprovider().getList().addAll(value);
		
		
	}

	@Override
	public void setValue(ArrayList<LiaisonSubstep> value, boolean fireEvents) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void clear()
	{
		super.clear();
		table.clear();
	}
	
	public boolean validateSubstep()
	{
		boolean val=true;
		
		if(tbresponsestep.getValue().trim().equals(""))
		{
			this.showDialogMessage("Request Step name is mandatory");
			return false;
		}
		
		if(ibresponseSLA.getValue()==null)
		{
			this.showDialogMessage("Request step SLA is mandatory");
			return false;
		}
		if(tadescriptionResponse.getValue().equals(""))
		{
			this.showDialogMessage("Request step Description is mandatory");
			return false;
		}
		
		List<LiaisonSubstep> list=table.getDataprovider().getList();
		if(list.size()!=0)
		{
			
				String name=tbresponsestep.getValue();
				for(int i=0;i<list.size();i++)
				{
					if(name.equals(list.get(i).getReponseName()))
					{
						this.showDialogMessage("This Request step already exists in table");
						return false;
					}
				}
			
		}
		return true;
		
	}

	@Override
	public void setToViewState()
	{
		super.setToViewState();
		table.setEnable(false);
		
		SuperModel model=new LiaisonStep();
		model=liasionobj;
		AppUtility.addDocumentToHistoryTable(AppConstants.SERVICEMODULE,AppConstants.LIASIONSTEPS, liasionobj.getCount(), null,null,null, false, model, null);
	}

	@Override
	public void setToEditState()
	{
		super.setToEditState();
		table.setEnable(true);
	}

}
