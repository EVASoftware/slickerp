package com.slicktechnologies.client.views.serviceliaison.liaisonsteps;

import com.google.gwt.user.cellview.client.TextColumn;
import com.slicktechnologies.client.views.serviceliaison.liaisonsteps.LiaisonStepsPresenter.LiaisonStepsPresenterTable;
import com.slicktechnologies.shared.common.serviceLiaisonLayer.LiaisonStep;

public class LiaisonStepPresenterTableProxy extends LiaisonStepsPresenterTable
{
	
	TextColumn<LiaisonStep> ColumnID;
	TextColumn<LiaisonStep> ColumnName;
	TextColumn<LiaisonStep> ColumnSLA;
	TextColumn<LiaisonStep> ColumnDescription;
	TextColumn<LiaisonStep> ColumnMandatory;
	TextColumn<LiaisonStep> ColumnDependentStep;
	TextColumn<LiaisonStep> ColumnStatus;
	
	@Override
	public void createTable()
	{
		addColumnID();
		addColumnName();
		addColumnSLA();
		addColumnDescription();
		addColumnMandatory();
		addColumnDependentStep();
		addColumnStatus();
	}
	
	public void addColumnID()
	{
		ColumnID=new TextColumn<LiaisonStep>() {
			
			@Override
			public String getValue(LiaisonStep object) {
				// TODO Auto-generated method stub
				return object.getCount()+"";
			}
		};table.addColumn(ColumnID,"ID");
	}
	
	public void addColumnName()
	{
		ColumnName=new TextColumn<LiaisonStep>() {
			
			@Override
			public String getValue(LiaisonStep object) {
				// TODO Auto-generated method stub
				return object.getStepName();
			}
		};table.addColumn(ColumnName,"Name");
	}
	
	public void addColumnSLA()
	{
		ColumnSLA=new TextColumn<LiaisonStep>() {
			
			@Override
			public String getValue(LiaisonStep object) {
				// TODO Auto-generated method stub
				return object.getSLA()+"";
			}
		};table.addColumn(ColumnSLA,"SLA");
	}
	
public void addColumnDescription()
{
	ColumnDescription=new TextColumn<LiaisonStep>() {
		
		@Override
		public String getValue(LiaisonStep object) {
			// TODO Auto-generated method stub
			return object.getDescription();
		}
	};table.addColumn(ColumnDescription,"Description");
}
	public void addColumnMandatory()
	{
		ColumnMandatory=new TextColumn<LiaisonStep>() {
			
			@Override
			public String getValue(LiaisonStep object) {
				// TODO Auto-generated method stub
				if(object.getMandatory()==true)
					return "Yes";
				else
					return "No";
				
			}
		};table.addColumn(ColumnMandatory,"Mandatory");
	}
	
	public void addColumnDependentStep()
	{
		ColumnDependentStep=new TextColumn<LiaisonStep>() {
			
			@Override
			public String getValue(LiaisonStep object) {
				// TODO Auto-generated method stub
				return object.getParentStep();
			}
		};table.addColumn(ColumnDependentStep,"Dependent Step");
	}
	
	public void addColumnStatus()
	{
		ColumnStatus=new TextColumn<LiaisonStep>() {
			
			@Override
			public String getValue(LiaisonStep object) {
				// TODO Auto-generated method stub
				if(object.getStatus()==true){
				return "Active";
				}
				else{
					return "Inactive";
				}
				
			}
		};table.addColumn(ColumnStatus,"Status");
	}
}
