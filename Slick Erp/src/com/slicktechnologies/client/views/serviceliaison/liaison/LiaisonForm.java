package com.slicktechnologies.client.views.serviceliaison.liaison;

import java.util.ArrayList;
import java.util.Collection;


import java.util.List;

import javax.swing.event.ListDataEvent;

import com.google.gwt.aria.client.TextboxRole;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.ToggleButton;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.AddressComposite;
import com.simplesoftwares.client.library.composite.CompositeInterface;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.composite.UploadComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.serviceliaison.liaisonsteps.ReponseStepInfoTable;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.ConcreteBusinessProcess;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.HRProcess;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.serviceLiaisonLayer.Liaison;
import com.slicktechnologies.shared.common.serviceLiaisonLayer.LiaisonGroup;
import com.slicktechnologies.shared.common.serviceLiaisonLayer.LiaisonStepLineItem;
import com.slicktechnologies.shared.common.serviceLiaisonLayer.LiaisonStep;
import com.slicktechnologies.shared.common.serviceLiaisonLayer.LiaisonSubstepLineItem;
import com.slicktechnologies.shared.common.serviceLiaisonLayer.LiaisonSubstep;



public class LiaisonForm extends FormScreen<Liaison>implements ChangeHandler,ClickHandler,HasValue<ArrayList<LiaisonStepLineItem>>,CompositeInterface

{
	final GenricServiceAsync async=GWT.create(GenricService.class);
	IntegerBox tbContractId;
	IntegerBox ibLiaisonId;
	IntegerBox ibServiceId;
	public TextBox tbStatus;
	TextBox tbLiaisonTitle;
	DateBox tbServiceDate;
	public TextBox tbBranch;
	public TextBox tbServiceEngg;
	
	UploadComposite upTestReport,upDevice;
	PersonInfoComposite poc;
	AddressComposite addr;
	
	public Button btnAdd;
	DateBox dbfromDate,dbToDate,dbLiaisonStartDate,dbLisisonendDate;
	public StepTable requestStepTable;
	public SubStepTable responseStepTable;
	public ObjectListBox<LiaisonGroup> olbliaisongroup;
	ArrayList<LiaisonStepLineItem> item;
	public static String bool="";
	public static int size=0;
	public static ArrayList<Integer> arrlist = new ArrayList<Integer>(10);
	public static ArrayList<Integer> range=new ArrayList<Integer>(10);
	Button addbutton,deletebutton;
	
	Liaison liaisonobj;
	 
	public LiaisonForm()
	{
		super();
		createGui();
	}
	
	public LiaisonForm  (String[] processlevel, FormField[][] fields,
			FormStyle formstyle) 
	{
		super(processlevel, fields, formstyle);
		requestStepTable.connectToLocal();
		responseStepTable.connectToLocal();
		createGui();
	}
	
	private void initalizeWidget()
	{

		tbContractId=new IntegerBox();
		tbContractId.setEnabled(false);
		poc=AppUtility.customerInfoComposite(new Customer());
		
		tbServiceEngg=new TextBox();
		tbServiceEngg.setEnabled(false);
		olbliaisongroup=new ObjectListBox<LiaisonGroup>();
		//AppUtility.makeSalesPersonListBoxLive(tbServiceEngg);
		tbBranch=new TextBox();
		tbBranch.setEnabled(false);
		//AppUtility.makeBranchListBoxLive(tbBranch);
		tbServiceDate=new DateBox();
		tbServiceDate.setEnabled(false);
		tbStatus=new TextBox();
		tbStatus.setText(Liaison.LIASIONSCHEDULE);
		tbStatus.setEnabled(false);
	//	 AppUtility.setStatusListBox(tbStatus,Service.getStatusList());
		
		upTestReport=new UploadComposite();
		upDevice=new UploadComposite();
		addr=new AddressComposite();
		
		btnAdd=new Button("Add");
		dbfromDate=new DateBox();
		dbfromDate.setEnabled(false);
		dbToDate=new DateBox();
		dbToDate.setEnabled(false);
		requestStepTable=new StepTable();
		this.initalizeGroup();
		olbliaisongroup.addClickHandler(this);
		olbliaisongroup.addChangeHandler(this);
		responseStepTable=new SubStepTable();
		//btnAdd.addClickHandler(this);
		item=new ArrayList<LiaisonStepLineItem>();
		ibLiaisonId=new IntegerBox();
		ibLiaisonId.setEnabled(false);
		ibServiceId=new IntegerBox();
		tbLiaisonTitle=new TextBox();
		addbutton=new Button("Add Group");
		addbutton.addClickHandler(this);
		deletebutton=new Button("Delete Group");
		deletebutton.addClickHandler(this);
		ibServiceId.addChangeHandler(this);
		dbLiaisonStartDate=new DateBox();
		dbLisisonendDate=new DateBox();
		poc.setEnable(false);
		
	}



	@Override
	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Save")||text.contains("Discard")||text.contains("Search")||text.contains(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
				
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Save")||text.contains("Discard")||text.contains(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 
				
				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Edit")||text.contains("Discard")||text.contains("Search")||text.contains(AppConstants.NAVIGATION))
						
						menus[k].setVisible(true); 
				
				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.LIASION,LoginPresenter.currentModule.trim());

		
	}
	
	public void setAppHeaderBarAsPerStatus()
	{
		Liaison entity=(Liaison) presenter.getModel();
		String status=entity.getStatus();
		
		if(status.equals(Liaison.LIASIONCOMPLETED)||status.equals(Liaison.LIASIONCOMPLETED))
			
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Discard")||text.contains("Search")||text.contains(AppConstants.NAVIGATION))
						
				{
					menus[k].setVisible(true); 
					System.out.println("Value of text is "+menus[k].getText());
				}
				else
				{
					menus[k].setVisible(false);  
					

				}

			}

		}

	

		
	}
	
	public void toggleProcessLevelMenu()
	{
		Liaison entity=(Liaison) presenter.getModel();
		
		String status=entity.getStatus();

		for(int i=0;i<getProcesslevelBarNames().length;i++)
		{
			InlineLabel label=getProcessLevelBar().btnLabels[i];
			String text=label.getText().trim();

			if(status.equals(Liaison.LIASIONCANCELLED)||(status.equals(Liaison.LIASIONCOMPLETED)))
			{
				if(text.equals("Mark Complete"))
					label.setVisible(false);
				else if(text.equals("Cancel"))
					label.setVisible(false);
				
			}
		}

	
	}
	
	public void setMenuAsPerStatus()
	{
		this.setAppHeaderBarAsPerStatus();
		this.toggleProcessLevelMenu();
		
	}
	
	public void setToViewState()
	{
		super.setToViewState();
		setMenuAsPerStatus();
		
		SuperModel model=new Liaison();
		model=liaisonobj;
		AppUtility.addDocumentToHistoryTable(AppConstants.SERVICEMODULE,AppConstants.LIASION, liaisonobj.getCount(), Integer.parseInt(poc.getId().getValue()),poc.getName().getValue(),Long.parseLong(poc.getPhone().getValue()), false, model, null);
		
	}
	
	@Override
	public void createScreen() {
		
		this.processlevelBarNames=new String[]{"Mark Complete","Cancel","New"};
		initalizeWidget();
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		fbuilder = new FormFieldBuilder("Steps Group",olbliaisongroup);
		FormField folbliaisongroup= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Liaison ID",ibLiaisonId);
		FormField fibLiaisonId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		FormField fgroupingCustomerInformation=fbuilder.setlabel("Customer Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		FormField fgroupingContractInformation=fbuilder.setlabel("Contract Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("* Service Id",ibServiceId);
		FormField fibServiceId= fbuilder.setMandatory(true).setMandatoryMsg("Service ID is mandatory").setRowSpan(0).setColSpan(0).build();

		
		
		fbuilder = new FormFieldBuilder("Contract Id",tbContractId);
		FormField ftbContractId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Contract start Date",dbfromDate);
		FormField fdbfromDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Contract end Date",dbToDate);
		FormField fdbToDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("",poc);
		FormField fpic= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder("",requestStepTable.getTable());
		FormField frequestStepTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",responseStepTable.getTable());
		FormField fresponseStepTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();

		
		FormField fgroupingServiceInformation=fbuilder.setlabel("Service Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("* Employee",tbServiceEngg);
		FormField ftbServiceEngg= fbuilder.setMandatory(true).setMandatoryMsg("Employee is mandatory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Branch",tbBranch);
		FormField ftbBranch= fbuilder.setMandatory(true).setMandatoryMsg("Branch is mandatory").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Service Date",tbServiceDate);
		FormField ftbSrviceDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Status",tbStatus);
		FormField ftbStatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Liaison Summary Report",upTestReport);
		FormField fupTestReport= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		FormField fgroupingStepsInformation=fbuilder.setlabel("Liaison Request steps").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		FormField fgroupingsubStepInformation=fbuilder.setlabel("Liaison Response steps").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		FormField fgroupingLiaiosonInformation=fbuilder.setlabel("Liaison Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();

		
		fbuilder = new FormFieldBuilder("* Title",tbLiaisonTitle);
		FormField ftbLiaisonTitle= fbuilder.setMandatory(true).setMandatoryMsg("Title is mandatory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",addbutton);
		FormField fadd= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",deletebutton);
		FormField fdelete= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Liaison Start Date",dbLiaisonStartDate);
		FormField fliaisonstartdt= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Liaison End Date",dbLisisonendDate);
		FormField fliaisonenddt= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////


		FormField[][] formfield = {
				
				{fgroupingServiceInformation},
				{fibServiceId,ftbServiceEngg,ftbSrviceDate,ftbBranch},
				{fgroupingContractInformation},
				{ftbContractId,fdbfromDate,fdbToDate},
				{fgroupingLiaiosonInformation,}, 
				{fibLiaisonId,ftbLiaisonTitle,fliaisonstartdt,fliaisonenddt},
				{fupTestReport,ftbStatus},
				{fgroupingCustomerInformation},
				{fpic},
				{fgroupingStepsInformation},{folbliaisongroup},{fadd,fdelete},{frequestStepTable},
				{fgroupingsubStepInformation},{fresponseStepTable}
				
		};

		this.fields=formfield;


		
	}

	@Override
	public void updateModel(Liaison model) {
		model.setServiceId(ibServiceId.getValue());
		model.setContractCount(tbContractId.getValue());
		
		if(poc.getValue()!=null)
		{
			model.setFullName(poc.getName().getValue());
			model.setCustomerId(Integer.parseInt(poc.getId().getValue()));
			model.setCellNumber(poc.getCellValue());
		}
			model.setContractCount(tbContractId.getValue());
			if(dbfromDate.getValue()!=null)
				model.setContractStartDate(dbfromDate.getValue());
			if(dbToDate.getValue()!=null)
				model.setContractEndDate(dbToDate.getValue());
			if(tbServiceEngg.getValue()!=null)
				model.setEmployee(tbServiceEngg.getValue());
			if(tbBranch.getValue()!=null)
				model.setBranch(tbBranch.getValue());
			if(tbServiceDate.getValue()!=null)
				model.setServiceDate(tbServiceDate.getValue());
			if(tbStatus.getValue()!=null)
				model.setStatus(tbStatus.getValue());
			if(upTestReport.getValue()!=null)
				model.setUptestReport(upTestReport.getValue());
			if(tbLiaisonTitle.getValue()!=null)
				model.setLiaisonTitle(tbLiaisonTitle.getValue());
			if(dbLiaisonStartDate.getValue()!=null)
				model.setLiaisonStartDate(dbLiaisonStartDate.getValue());
			if(dbLisisonendDate.getValue()!=null)
				model.setLiaisonEndDate(dbLisisonendDate.getValue());
			
			List<LiaisonStepLineItem> daysalloc = requestStepTable.getDataprovider().getList();
		    List<LiaisonStepLineItem> alloc = new ArrayList<LiaisonStepLineItem>();
		    alloc.addAll(daysalloc);
			model.setRequestlineitem(alloc);

			liaisonobj=model;
			presenter.setModel(model);
			
	}

	@Override
	public void updateView(Liaison view) {
		
		liaisonobj=view;
		
		ibLiaisonId.setValue(view.getCount());
		ibServiceId.setValue(view.getServiceId());
		tbContractId.setValue(view.getContractCount());
		if(view.getPersonInfo()!=null)
			poc.setValue(view.getPersonInfo());
		if(view.getContractStartDate()!=null)
			dbfromDate.setValue(view.getContractStartDate());
		if(view.getContractEndDate()!=null)
			dbToDate.setValue(view.getContractEndDate());
		if(view.getEmployee()!=null)
			tbServiceEngg.setValue(view.getEmployee());
		if(view.getBranch()!=null)
			tbBranch.setValue(view.getBranch());
		if(view.getServiceDate()!=null)
			tbServiceDate.setValue(view.getServiceDate());
		if(view.getStatus()!=null)
			tbStatus.setValue(view.getStatus());
		if(view.getUptestReport()!=null)
			upTestReport.setValue(view.getUptestReport());
		if(view.getLiaisonTitle()!=null)
			tbLiaisonTitle.setValue(view.getLiaisonTitle());
		if(view.getLiaisonStartDate()!=null)
			dbLiaisonStartDate.setValue(view.getLiaisonStartDate());
		if(view.getLiaisonEndDate()!=null)
			dbLisisonendDate.setValue(view.getLiaisonEndDate());
		
		if(view.getRequestlineitem()!=null)
		{
		   requestStepTable.setValue(view.getRequestlineitem());
		  List<LiaisonStepLineItem>lis= requestStepTable.getListDataProvider().getList();
		  List<LiaisonSubstepLineItem>subStep=lis.get(0).getSubStepLineItem();
		  responseStepTable.setValue(subStep);
		   
		   
		}
		
		if(view.getFullName()!=null)
			poc.getName().setValue(view.getFullName());
		poc.getId().setValue(view.getCount()+"");
		poc.getPhone().setValue(view.getCellNumber()+"");
		
		
		
		
		presenter.setModel(view);
	}
	
	protected void initalizeGroup()
	{
		MyQuerry querry = new MyQuerry();
		Filter filter=new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		querry.getFilters().add(filter);
		querry.setQuerryObject(new LiaisonGroup());
		
		olbliaisongroup.MakeLive(querry);
	}

	@Override
	public void onChange(ChangeEvent event) {
	
		if(event.getSource().equals(ibServiceId)&&ibServiceId.getValue()!=0&&ibServiceId.getValue()!=null)
		{
			setServiceData();
				
		}
	}
	
	
	
	
	
	public void setServiceData()
	{
		int i=ibServiceId.getValue();
		MyQuerry querry = new MyQuerry();
		Filter filter=new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(i);
		querry.getFilters().add(filter);
		querry.setQuerryObject(new Service());
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				
				if(result.size()!=0)
				{
				System.out.println(result.get(0));
				Service service=(Service) result.get(0);
				int cout=service.getCount();
				tbContractId.setValue(service.getContractCount());
				tbServiceDate.setValue(service.getServiceDate());
				dbfromDate.setValue(service.getContractStartDate());
				dbToDate.setValue(service.getContractEndDate());
				tbServiceEngg.setValue(service.getEmployee());
				poc.getName().setText(service.getCustomerName());
				poc.getId().setText(service.getCount()+"");
				poc.getPhone().setText(service.getCellNumber()+"");
				tbBranch.setValue(service.getBranch());
				}
				else
				{
					
					showDialogMessage("Service for this ID does not exist");
					tbContractId.setValue(null);
					tbServiceDate.setValue(null);
					dbfromDate.setValue(null);
					dbToDate.setValue(null);
					tbServiceEngg.setValue("");
					poc.getName().setText("");
					poc.getId().setText("");
					poc.getPhone().setText("");
					tbBranch.setValue("");
					ibServiceId.setValue(null);
				}
				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});

	}

	
	@Override
	public void onClick(ClickEvent event) {
		
		if(event.getSource().equals(addbutton)&&olbliaisongroup.getValue()!=null)
		{
			if(validateGroup("add"))
			SetRequestTable();
		}
		if(event.getSource().equals(deletebutton)&&olbliaisongroup.getValue()!=null)
		{
			if(validateGroup("delete"))
			deleteGroup();
		}
		
	}

	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<ArrayList<LiaisonStepLineItem>> handler) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void fireEvent(GwtEvent<?> event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ArrayList<LiaisonStepLineItem> getValue() {
		List<LiaisonStepLineItem>list=this.requestStepTable.getDataprovider().getList();
		ArrayList<LiaisonStepLineItem>vec=new ArrayList<LiaisonStepLineItem>();
		vec.addAll(list);
		return vec;

	}

	@Override
	public void setValue(ArrayList<LiaisonStepLineItem> value) {
		requestStepTable.connectToLocal();
		requestStepTable.getDataprovider().getList().addAll(value);
		
	}

	@Override
	public void setValue(ArrayList<LiaisonStepLineItem> value,
			boolean fireEvents) {
		// TODO Auto-generated method stub
		
	}
	
	
	public void SetRequestTable()
	{
		int i=0;
		if(requestStepTable.getDataprovider().getList().size()==0)
		 i=1;
		else i=requestStepTable.getDataprovider().getList().size()+1;
		
		LiaisonGroup grp=olbliaisongroup.getSelectedItem();
		String name=olbliaisongroup.getValue();
		ArrayList<LiaisonStep> desc=grp.getStepsinfo();
		
		ArrayList<LiaisonStepLineItem> li=new ArrayList<LiaisonStepLineItem>();
		
		for(LiaisonStep temp:desc)
		{
			
			ArrayList<LiaisonSubstepLineItem> res=new ArrayList<LiaisonSubstepLineItem>();
			LiaisonStepLineItem ad=new LiaisonStepLineItem();
			LiaisonStepLineItem item=new LiaisonStepLineItem();
			
			
		
			ArrayList<LiaisonSubstep> object=temp.getResponseinfo();
			System.out.println("size of respo"+object.size());
			
			for(LiaisonSubstep desss:object)
			{
			
				LiaisonSubstepLineItem resp=new LiaisonSubstepLineItem();
				resp.setRequestStep(temp.getStepName());
				resp.setRefindex(i);
				resp.setRespo(desss);
				res.add(resp);
				item.setSubStepLineItem(res);
				System.out.println("print");
				System.out.println(temp.getStepName()+"  "+item.getSubStepLineItem().size());
				
				
			}
			
			item.setLisionStep(temp);
			li.add(item);
			
			System.out.println(item.getStep()+"**"+item.getSubStepLineItem());
			ad.setGroupName(name);
			ad.setSLA(item.getSLA());
			ad.setStep(item.getStep());
			ad.setMandatory(item.getMandatory());
			ad.setParentStep(item.getParentStep());
			ad.setIndex(i);
			ad.setSubStepLineItem(item.getSubStepLineItem());
		
			requestStepTable.getDataprovider().getList().add(ad);
			
		
			
			responseStepTable.getDataprovider().getList().addAll(item.getSubStepLineItem());
			
	
			
			i++;
			
			}
		
		LiaisonPresenter pres=(LiaisonPresenter) presenter;
		pres.getResponse();
		responseStepTable.setvisible();
			
		}
		
		
	public void deleteGroup()
	{
		String grp=olbliaisongroup.getValue().trim();
		List<LiaisonStepLineItem> list=requestStepTable.getDataprovider().getList();
		List<LiaisonSubstepLineItem> reqlist=responseStepTable.getDataprovider().getList();
		for(int i=0;i<list.size();i++)
		{
			
			if(grp.equals(list.get(i).getGroupName().trim()))
			{
				String stepName=list.get(i).getStep();
				requestStepTable.getDataprovider().getList().remove(i);
				i--;
				responseStepTable.connectToLocal();
				if(list.size()!=0)
				{
					reqlist=list.get(0).getSubStepLineItem();
					responseStepTable.getDataprovider().setList(reqlist);
					
				
				}
			}
		}
	}
		
		
	
@Override	
public void clear()
{
	System.out.println("inside clear");
	super.clear();
	responseStepTable.connectToLocal();
	requestStepTable.connectToLocal();
	tbStatus.setText(Liaison.LIASIONSCHEDULE);
	
}
	
@Override
public void setEnable(boolean state) {
	super.setEnable(state);
	ibLiaisonId.setEnabled(false);
	tbStatus.setEnabled(false);
	tbContractId.setEnabled(false);
	tbServiceDate.setEnabled(false);
	tbServiceEngg.setEnabled(false);
	tbBranch.setEnabled(false);
	dbfromDate.setEnabled(false);
	dbToDate.setEnabled(false);
	poc.setEnable(false);
	requestStepTable.setEnable(state);
	responseStepTable.setEnable(state);
	
}




public boolean validateGroup(String operator)
{
	if(requestStepTable.getDataprovider().getList().size()!=0)
	{
		List<LiaisonStepLineItem> list=requestStepTable.getDataprovider().getList();
		for(int i=0;i<list.size();i++)
		{
			if(operator.equals("add")&&olbliaisongroup.getValue().equals(list.get(i).getGroupName()))
			{
				this.showDialogMessage("This Group is already exist");
				return false;
			}
			
	if(operator.equals("delete")&&!olbliaisongroup.getValue().equals(list.get(i).getGroupName()))
		{	
			if(i==list.size()-1){
			this.showDialogMessage("This Group does not exist");
			return false;
			}
					
		}
			
	}
	return true;
}
return true;

}



}