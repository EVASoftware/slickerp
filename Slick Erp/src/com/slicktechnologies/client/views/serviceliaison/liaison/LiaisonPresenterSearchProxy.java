package com.slicktechnologies.client.views.serviceliaison.liaison;

import java.util.ArrayList;
import java.util.Vector;

import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.AddressComposite;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.composite.UploadComposite;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.serviceliaison.liaison.LiaisonPresenter.LiaisonPresenterSearch;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.serviceLiaisonLayer.Liaison;
import com.slicktechnologies.shared.common.serviceLiaisonLayer.LiaisonGroup;
import com.slicktechnologies.shared.common.serviceLiaisonLayer.LiaisonStepLineItem;

public class LiaisonPresenterSearchProxy extends LiaisonPresenterSearch{

	
	IntegerBox tbContractId;
	IntegerBox ibLiaisonId;
	IntegerBox ibServiceId;
	ListBox tbStatus;
	DateBox tbServiceDate;
	public ObjectListBox<Branch> tbBranch;
	public ObjectListBox<Employee> tbServiceEngg;
	
	UploadComposite upTestReport,upDevice;
	PersonInfoComposite poc;
	AddressComposite addr;
	
	public Button btnAdd;
	DateBox dbfromDate,dbToDate;
	StepTable requestStepTable;
	SubStepTable responseStepTable;
	ObjectListBox<LiaisonGroup> olbliaisongroup;
	public DateComparator dateComparator;
	TextBox tbliaisonTitle;
	
	
	public Object getVarRef(String varName)
	  {
		  
	   return null ;
	  }
	  public LiaisonPresenterSearchProxy()
	  {
	  super();
	  createGui();
	  }
	  
	  
	  
	  public void initWidget()
	  {
		  tbContractId=new IntegerBox();
			poc=AppUtility.customerInfoComposite(new Customer());
			
			tbServiceEngg=new ObjectListBox<Employee>();
			olbliaisongroup=new ObjectListBox<LiaisonGroup>();
			AppUtility.makeSalesPersonListBoxLive(tbServiceEngg);
			tbBranch=new ObjectListBox<Branch>();
			AppUtility.makeBranchListBoxLive(tbBranch);
			tbServiceDate=new DateBox();
			tbStatus=new ListBox();
			
			 AppUtility.setStatusListBox(tbStatus,Liaison.getStatusList());
			
			upTestReport=new UploadComposite();
			upDevice=new UploadComposite();
			addr=new AddressComposite();
			
			btnAdd=new Button("Add");
			dbfromDate=new DateBox();
			dbToDate=new DateBox();
			requestStepTable=new StepTable();
			
			
			
			responseStepTable=new SubStepTable();
			//btnAdd.addClickHandler(this);
			
			ibLiaisonId=new IntegerBox();
			ibServiceId=new IntegerBox();
dateComparator=new DateComparator("creationDate",new Liaison());
tbliaisonTitle=new TextBox();
		  
		  
		  
		  
	  }
	

	  public void createScreen() {
		
			initWidget();
	FormFieldBuilder fbuilder;
			
			fbuilder = new FormFieldBuilder();
			
			fbuilder = new FormFieldBuilder("Steps Group",olbliaisongroup);
			FormField folbliaisongroup= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			
			fbuilder = new FormFieldBuilder("Liaison ID",ibLiaisonId);
			FormField fibLiaisonId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			
			
			FormField fgroupingCustomerInformation=fbuilder.setlabel("Customer Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
			FormField fgroupingContractInformation=fbuilder.setlabel("Contract Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
			
			fbuilder = new FormFieldBuilder("Service Id",ibServiceId);
			FormField fibServiceId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

			fbuilder = new FormFieldBuilder("Title",tbliaisonTitle);
			FormField fTitle= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

			
			fbuilder = new FormFieldBuilder("Contract Id",tbContractId);
			FormField ftbContractId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("Contract Start Date",dateComparator.getFromDate());
			FormField fdbfromDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			
			fbuilder = new FormFieldBuilder("Contract End Date",dateComparator.getToDate());
			FormField fdbToDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			
			
			fbuilder = new FormFieldBuilder("",poc);
			FormField fpic= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
			
			
			
			FormField fgroupingServiceInformation=fbuilder.setlabel("Service Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
			
			fbuilder = new FormFieldBuilder("Service Enggineer",tbServiceEngg);
			FormField ftbServiceEngg= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			
			fbuilder = new FormFieldBuilder("Branch",tbBranch);
			FormField ftbBranch= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("Service Date",tbServiceDate);
			FormField ftbSrviceDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("Status",tbStatus);
			FormField ftbStatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			
			fbuilder = new FormFieldBuilder("Upload Test Report",upTestReport);
			FormField fupTestReport= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("Upload Device Document",upDevice);
			FormField fupDevice= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			
			FormField fgroupingDeviceInformation=fbuilder.setlabel("Liaison Request Steps").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
			FormField fgroupingResponseInformation=fbuilder.setlabel("Liaison Reponse Steps").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
			

			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

			////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////


			FormField[][] formfield = {  
					{fibLiaisonId,fTitle,fibServiceId,ftbContractId},
					{fdbfromDate,fdbToDate,ftbSrviceDate,ftbServiceEngg},
					{ftbBranch,ftbStatus},
					{fpic},
					
					
					
			};

			this.fields=formfield;


	  }

	
	  public MyQuerry getQuerry()
	  {
	  Vector<Filter> filtervec=new Vector<Filter>();
	  Filter temp=null;
	  
	 if(tbBranch.getSelectedIndex()!=0) 
	 {
		 temp=new Filter();
		  temp.setStringValue(tbBranch.getValue().trim());
		  temp.setQuerryString("branch");
		  filtervec.add(temp);

	 }
	
	 if(tbServiceEngg.getSelectedIndex()!=0)
	 {
		 temp=new Filter();
		 temp.setStringValue(tbServiceEngg.getValue().trim());
		 temp.setQuerryString("employee");
		 filtervec.add(temp);
	 }
	  
	 
	  if(dateComparator.getValue()!=null)
	  {
	     filtervec.addAll(dateComparator.getValue());
	  }
	  if(ibServiceId.getValue()!=null)
	  {
		  temp=new Filter();
		  temp.setIntValue(ibServiceId.getValue());
		  temp.setQuerryString("serviceId");
		  filtervec.add(temp);
	  }
	 if(tbServiceDate.getValue()!=null)
	 {
		 temp=new Filter();
		 temp.setDateValue(tbServiceDate.getValue());
		 temp.setQuerryString("serviceDate");
		 filtervec.add(temp);
	 }
	  if(tbContractId.getValue()!=null)
	  {
		  temp=new Filter();
		  temp.setIntValue(tbContractId.getValue());
		  temp.setQuerryString("contractCount");
		  filtervec.add(temp);
	  }
	  
	  if(poc.getIdValue()!=-1)
	  {
	  temp=new Filter();
	  temp.setIntValue(poc.getIdValue());
	  temp.setQuerryString("personInfo.count");
	  filtervec.add(temp);
	  }
	  
	  if(!(poc.getFullNameValue().equals("")))
	  {
	  temp=new Filter();
	  temp.setStringValue(poc.getFullNameValue());
	  temp.setQuerryString("personInfo.fullName");
	  filtervec.add(temp);
	  }
	  if(poc.getCellValue()!=-1l)
	  {
	  temp=new Filter();
	  temp.setLongValue(poc.getCellValue());
	  temp.setQuerryString("personInfo.cellNumber");
	  filtervec.add(temp);
	  }
	  
	  if(tbStatus.getSelectedIndex()!=0)
	  {
		 
		  temp=new Filter();
		  int item=tbStatus.getSelectedIndex();
		  String sel=tbStatus.getItemText(item);
		  temp.setStringValue(sel);
		  temp.setQuerryString("status");
		  filtervec.add(temp);
	  }


	if(ibLiaisonId.getValue()!=null)
	{
		 temp=new Filter();
		  temp.setIntValue(ibLiaisonId.getValue());
		  temp.setQuerryString("count");
		  filtervec.add(temp);
	}
	
	if(!tbliaisonTitle.getValue().trim().equals(""))
	{
		temp=new Filter();
		temp.setStringValue(tbliaisonTitle.getValue());
		temp.setQuerryString("liaisonTitle");
		filtervec.add(temp);
	}
	 
	 
	 
	 
	  MyQuerry querry= new MyQuerry();
	  querry.setFilters(filtervec);
	  querry.setQuerryObject(new Liaison());
	 
	  
	  return querry;
	  }
	@Override
	public boolean validate() {
		if(LoginPresenter.branchRestrictionFlag)
		{
		if(tbBranch.getSelectedIndex()==0)
		{
			showDialogMessage("Select Branch");
			return false;
		}
		}
		
			return super.validate();
	}
	
	
	  
}
