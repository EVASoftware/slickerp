package com.slicktechnologies.client.views.serviceliaison.liaison;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.serviceliaison.liaison.LiaisonPresenter.LiaisonPresenterTable;



import com.slicktechnologies.shared.common.serviceLiaisonLayer.Liaison;


public class LiaisonPresenterTableProxy extends LiaisonPresenterTable{

	TextColumn<Liaison> getLiaisonIdColumn;
	  TextColumn<Liaison> getEmployeeidColumn;
	  TextColumn<Liaison> getEmployeeNameColumn;
	  TextColumn<Liaison> getEmployeeCellNoColumn;
	  TextColumn<Liaison> getServiceIdColumn;
	  TextColumn<Liaison> getContractIdColumn;
	  TextColumn<Liaison> getFormDateColumn;
	  TextColumn<Liaison> getToDateColumn;
	  TextColumn<Liaison> getServiceEngineerColumn;
	  TextColumn<Liaison> getBranchColumn;
	  TextColumn<Liaison> getServiceDateColumn;
	  TextColumn<Liaison> getStatusColumn;
	  TextColumn<Liaison> getTitleColumn;
	  TextColumn<Liaison> getCountColumn;
	  
	  TextColumn<Liaison> getDesignationColumn;
	  
	  public Object getVarRef(String varName)
	  {
		  
		  	  if(varName.equals("getLiaisonIdColumn"))
			  return this.getLiaisonIdColumn;
			  if(varName.equals("getEmployeeidColumn"))
			  return this.getEmployeeidColumn;
			  if(varName.equals("getEmployeeNameColumn"))
			  return this.getEmployeeNameColumn;
			  if(varName.equals("getEmployeeCellNoColumn"))
			  return this.getEmployeeCellNoColumn;
			  if(varName.equals("getServiceIdColumn"))
			  return this.getServiceIdColumn;
			  if(varName.equals("getContractIdColumn"))
			  return this.getContractIdColumn;
			  if(varName.equals("getFormDateColumn"))
			  return this.getFormDateColumn;
			  if(varName.equals("getToDateColumn"))
			  return this.getToDateColumn;
			  if(varName.equals("getServiceEngineerColumn"))
			  return this.getServiceEngineerColumn;
			  if(varName.equals("getBranchColumn"))
			  return this.getBranchColumn;
			  if(varName.equals("getStatusColumn"))
			  return this.getStatusColumn;
			  if(varName.equals("getServiceDateColumn"))
			  return this.getServiceDateColumn;

		  
		  
		  return null;
	  }
	  
	  public LiaisonPresenterTableProxy()
	  {
		  super();
	  }
	  
	  
	  @Override public void createTable() {
		 addColumnLiaisonId();
		 addColumnLiaisonTitle();
		 addColumnEmployeeId();
		 addColumnEmployeeName();
		 addColumnEmployeeCellNo();
		 addColumnServiceId();
		 addColumnContractId();
		 addColumnFromDate();
		 addColumnToDate();
		 addColumnServiceEngineer();
		 addColumnBrnach();
		 addColumnServiceDate();
		 addColumnStatus();
		 table.setColumnWidth(getLiaisonIdColumn, "50px");
		 table.setColumnWidth(getEmployeeCellNoColumn, "110px");
		 table.setColumnWidth(getServiceEngineerColumn, "130px");
		 table.setColumnWidth(getEmployeeNameColumn, "130px");
		  
	  }
	  
	  @Override
	  protected void initializekeyprovider() {
	  keyProvider= new ProvidesKey<Liaison>()
	  {
	  @Override
	  public Object getKey(Liaison item)
	  {
	  if(item==null)
	  {
	  return null;
	  }
	  else
	  return item.getId();
	  }
	  };
	  }
	  @Override
	  public void setEnable(boolean state)
	  {
	  }
	  @Override
	  public void applyStyle()
	  {
	  }
	 
	  public void addColumnSorting()
	   {
		 
		  	 addSortingColumnEmployeeId();
			 addSortingColumnEmployeeName();
			 addSortingColumnEmployeeCellNo();
			 addSortingColumnServiceId();
			 addSortingColumnContractId();
			 addSortingColumnFromDate();
			 addSortingColumnToDate();
			 addSortingColumnServiceEngineer();
			 addSortingColumnBrnach();
			 addSortingColumnServiceDate();
			 addSortingColumnStatus();

	   }
	  
	  @Override public void addFieldUpdater() {
	  }

	  
	  public void addColumnLiaisonId()
	  {
		  getLiaisonIdColumn=new TextColumn<Liaison>() {
			
			@Override
			public String getValue(Liaison object) {
				// TODO Auto-generated method stub
				return object.getCount()+"";
			}
		};table.addColumn(getLiaisonIdColumn,"ID");
		
	  }
	
	  public void addColumnLiaisonTitle()
	  {
		  getTitleColumn=new TextColumn<Liaison>() {
			
			@Override
			public String getValue(Liaison object) {
				// TODO Auto-generated method stub
				return object.getLiaisonTitle();
			}
		};table.addColumn(getTitleColumn,"Title");
	  }
	  
	  public void addColumnEmployeeId()
	  {
		  getEmployeeidColumn=new TextColumn<Liaison>() {
			
			@Override
			public String getValue(Liaison object) {
				// TODO Auto-generated method stub
				return object.getPersonInfo().getCount()+"";
			}
		};table.addColumn(getEmployeeidColumn,"CustomerID");
		getEmployeeidColumn.setSortable(true);
	  }
	  
	  public void addColumnEmployeeName()
	  {
		  getEmployeeNameColumn=new TextColumn<Liaison>() {
			
			@Override
			public String getValue(Liaison object) {
				// TODO Auto-generated method stub
				return object.getPersonInfo().getFullName()+"";
			}
		};table.addColumn(getEmployeeNameColumn,"Customer Name");
		getEmployeeNameColumn.setSortable(true);
	  }
	  
	  public void addColumnEmployeeCellNo()
	  {
		  getEmployeeCellNoColumn=new TextColumn<Liaison>() {
			
			@Override
			public String getValue(Liaison object) {
				// TODO Auto-generated method stub
				return object.getPersonInfo().getCellNumber()+"";
			}
		};table.addColumn(getEmployeeCellNoColumn,"Customer Cell");
		getEmployeeCellNoColumn.setSortable(true);
	  }
	  
	  public void addColumnServiceId()
	  {
		  getServiceIdColumn=new TextColumn<Liaison>() {
			
			@Override
			public String getValue(Liaison object) {
				// TODO Auto-generated method stub
				return object.getServiceId()+"";
			}
		};table.addColumn(getServiceIdColumn,"ServiceID");
		getServiceIdColumn.setSortable(true);
	  }
	  
	  
	  public void addColumnContractId()
	  {
		  getContractIdColumn=new TextColumn<Liaison>() {
			
			@Override
			public String getValue(Liaison object) {
				// TODO Auto-generated method stub
				
				if(object.getContractCount()==null)
					return "N/A";
				else
				return object.getContractCount()+"";
			}
		};table.addColumn(getContractIdColumn,"ContractID");
		getContractIdColumn.setSortable(true);
	  }
	  
	  
	  public void addColumnFromDate()
	  {
		  getFormDateColumn=new TextColumn<Liaison>() {
			
			@Override
			public String getValue(Liaison object) {
				if(object.getContractStartDate()==null)
					return "N/A";
				else
				return AppUtility.parseDate(object.getContractStartDate());
			}
		};table.addColumn(getFormDateColumn,"Contract From Date");
		getFormDateColumn.setSortable(true);
	  }
	  
	  
	  public void addColumnToDate()
	  {
		  getToDateColumn=new TextColumn<Liaison>() {
		
		@Override
		public String getValue(Liaison object) {
			
			if(object.getContractEndDate()==null)
				return "N/A";
			else
			return AppUtility.parseDate(object.getContractEndDate());
		}
		};table.addColumn(getToDateColumn,"Contract End Date");
		getToDateColumn.setSortable(true);
	  }
	  
	  
	  public void addColumnServiceEngineer()
	  {
		  getServiceEngineerColumn=new TextColumn<Liaison>() {
		
		@Override
		public String getValue(Liaison object) {
			// TODO Auto-generated method stub
			return object.getEmployee();
		}
		  };table.addColumn(getServiceEngineerColumn,"Employee");
		  getServiceEngineerColumn.setSortable(true);
	  }
	  
	public void addColumnBrnach()
	{
		getBranchColumn=new TextColumn<Liaison>() {
			
			@Override
			public String getValue(Liaison object) {
				// TODO Auto-generated method stub
				return object.getBranch();
			}
		};table.addColumn(getBranchColumn,"Branch");
		getBranchColumn.setSortable(true);
	}
	  
	  
	 public void addColumnServiceDate()
	 {
		 getServiceDateColumn=new TextColumn<Liaison>() {
			
			@Override
			public String getValue(Liaison object) {
				// TODO Auto-generated method stub
				if(object.getServiceDate()==null)
					return "N/A";
				else
				return AppUtility.parseDate(object.getServiceDate());
			}
		};table.addColumn(getServiceDateColumn,"Service Date");
		getServiceDateColumn.setSortable(true);
	 }
	  
	  public void addColumnStatus()
	  {
		  getStatusColumn=new TextColumn<Liaison>() {
			
			@Override
			public String getValue(Liaison object) {
				// TODO Auto-generated method stub
				if(object.getStatus().equals(""))
					return "N/A";
				else
				return object.getStatus();
			}
		};table.addColumn(getStatusColumn,"Status");
		getStatusColumn.setSortable(true);
	  }
	  
	  
	public void addSortingColumnEmployeeId()
	{
		 List<Liaison> list=getDataprovider().getList();
		  columnSort=new ListHandler<Liaison>(list);
		  columnSort.setComparator(getEmployeeidColumn, new Comparator<Liaison>()
		  {
		  @Override
		  public int compare(Liaison e1,Liaison e2)
		  {
		  if(e1!=null && e2!=null)
		  {
		  if(e1.getPersonInfo().getCount()== e2.getPersonInfo().getCount()){
		  return 0;}
		  if(e1.getPersonInfo().getCount()> e2.getPersonInfo().getCount()){
		  return 1;}
		  else{
		  return -1;}
		  }
		  else{
		  return 0;}
		  }
		  });
		  table.addColumnSortHandler(columnSort);

	}
	  
	 public void addSortingColumnEmployeeName()
	 {
		 List<Liaison> list=getDataprovider().getList();
		  columnSort=new ListHandler<Liaison>(list);
		  columnSort.setComparator(getEmployeeNameColumn, new Comparator<Liaison>()
		  {
		  @Override
		  public int compare(Liaison e1,Liaison e2)
		  {
		  if(e1!=null && e2!=null)
		  {
		  if( e1.getPersonInfo().getFullName()!=null && e2.getPersonInfo().getFullName()!=null){
		  return e1.getPersonInfo().getFullName().compareTo(e2.getPersonInfo().getFullName());}
		  }
		  else{
		  return 0;}
		  return 0;
		  }
		  });
		  table.addColumnSortHandler(columnSort);

	 }
	  
	  
	  public void addSortingColumnEmployeeCellNo()
	  {
		  List<Liaison> list=getDataprovider().getList();
		  columnSort=new ListHandler<Liaison>(list);
		  columnSort.setComparator(getEmployeeCellNoColumn, new Comparator<Liaison>()
		  {
		  @Override
		  public int compare(Liaison e1,Liaison e2)
		  {
		  if(e1!=null && e2!=null)
		  {
		  if(e1.getPersonInfo().getCellNumber()== e2.getPersonInfo().getCellNumber()){
		  return 0;}
		  if(e1.getPersonInfo().getCellNumber()> e2.getPersonInfo().getCellNumber()){
		  return 1;}
		  else{
		  return -1;}
		  }
		  else{
		  return 0;}
		  }
		  });
		  table.addColumnSortHandler(columnSort);

	  }
	  
	  public void addSortingColumnServiceId()
	  {
	 List<Liaison> list=getDataprovider().getList();
	  columnSort=new ListHandler<Liaison>(list);
	  columnSort.setComparator(getServiceIdColumn, new Comparator<Liaison>()
	  {
	  @Override
	  public int compare(Liaison e1,Liaison e2)
	  {
	  if(e1!=null && e2!=null)
	  {
	  if(e1.getServiceId()== e2.getServiceId()){
	  return 0;}
	  if(e1.getServiceId()> e2.getServiceId()){
	  return 1;}
	  else{
	  return -1;}
	  }
	  else{
	  return 0;}
	  }
	  });
	  table.addColumnSortHandler(columnSort);
	  }

	  public void addSortingColumnContractId()
	  {
		  List<Liaison> list=getDataprovider().getList();
		  columnSort=new ListHandler<Liaison>(list);
		  columnSort.setComparator(getContractIdColumn, new Comparator<Liaison>()
		  {
		  @Override
		  public int compare(Liaison e1,Liaison e2)
		  {
		  if(e1!=null && e2!=null)
		  {
		  if(e1.getContractCount()== e2.getContractCount()){
		  return 0;}
		  if(e1.getContractCount()> e2.getContractCount()){
		  return 1;}
		  else{
		  return -1;}
		  }
		  else{
		  return 0;}
		  }
		  });
		  table.addColumnSortHandler(columnSort);

	  }
	  
	public void addSortingColumnFromDate()
	{
		 List<Liaison> list=getDataprovider().getList();
		  columnSort=new ListHandler<Liaison>(list);
		  columnSort.setComparator(getFormDateColumn, new Comparator<Liaison>()
		  {
		  @Override
		  public int compare(Liaison e1,Liaison e2)
		  {
		  if(e1!=null && e2!=null)
		  {
		  if( e1.getContractStartDate()!=null && e2.getContractStartDate()!=null){
		  return e1.getContractStartDate().compareTo(e2.getContractStartDate());}
		  }
		  else{
		  return 0;}
		  return 0;
		  }
		  });
		  table.addColumnSortHandler(columnSort);
	}
	  
	public void addSortingColumnToDate()
	{
		 List<Liaison> list=getDataprovider().getList();
		  columnSort=new ListHandler<Liaison>(list);
		  columnSort.setComparator(getToDateColumn, new Comparator<Liaison>()
		  {
		  @Override
		  public int compare(Liaison e1,Liaison e2)
		  {
		  if(e1!=null && e2!=null)
		  {
		  if( e1.getContractEndDate()!=null && e2.getContractEndDate()!=null){
		  return e1.getContractEndDate().compareTo(e2.getContractEndDate());}
		  }
		  else{
		  return 0;}
		  return 0;
		  }
		  });
		  table.addColumnSortHandler(columnSort);
		
	}
	
	public void addSortingColumnServiceEngineer()
	{
		List<Liaison> list=getDataprovider().getList();
		columnSort=new ListHandler<Liaison>(list);
		columnSort.setComparator(getServiceEngineerColumn, new Comparator<Liaison>()
				{
			@Override
			public int compare(Liaison e1,Liaison e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getEmployee()!=null && e2.getEmployee()!=null){
						return e1.getEmployee().compareTo(e2.getEmployee());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);

	}
	
	public void addSortingColumnBrnach()
	{
		List<Liaison> list=getDataprovider().getList();
		columnSort=new ListHandler<Liaison>(list);
		columnSort.setComparator(getBranchColumn, new Comparator<Liaison>()
				{
			@Override
			public int compare(Liaison e1,Liaison e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBranch()!=null && e2.getBranch()!=null){
						return e1.getBranch().compareTo(e2.getBranch());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);

	}
	
	public void addSortingColumnServiceDate()
	{
		 List<Liaison> list=getDataprovider().getList();
		  columnSort=new ListHandler<Liaison>(list);
		  columnSort.setComparator(getToDateColumn, new Comparator<Liaison>()
		  {
		  @Override
		  public int compare(Liaison e1,Liaison e2)
		  {
		  if(e1!=null && e2!=null)
		  {
		  if( e1.getServiceDate()!=null && e2.getServiceDate()!=null){
		  return e1.getServiceDate().compareTo(e2.getServiceDate());}
		  }
		  else{
		  return 0;}
		  return 0;
		  }
		  });
		  table.addColumnSortHandler(columnSort);

	}
	
	public void addSortingColumnStatus()
	{
		List<Liaison> list=getDataprovider().getList();
		columnSort=new ListHandler<Liaison>(list);
		columnSort.setComparator(getStatusColumn, new Comparator<Liaison>()
				{
			@Override
			public int compare(Liaison e1,Liaison e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getStatus()!=null && e2.getStatus()!=null){
						return e1.getStatus().compareTo(e2.getStatus());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);


	}
	
}
	
