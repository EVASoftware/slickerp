package com.slicktechnologies.client.views.serviceliaison.liaison;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.Cell;
import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.DateCell;
import com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.SelectionCell;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Text;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.RowStyles;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;


import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.serviceLiaisonLayer.LiaisonStepLineItem;




public class StepTable extends SuperTable<LiaisonStepLineItem>{
	
	TextColumn<LiaisonStepLineItem> name;
	TextColumn<LiaisonStepLineItem> SLA;
	//TextColumn<Liaison> requestStep;
	TextColumn<LiaisonStepLineItem> mandatory;
	TextColumn<LiaisonStepLineItem> dependentStep;
	TextColumn<LiaisonStepLineItem> description;
	private Column<LiaisonStepLineItem,Boolean> stattus;
	Column<LiaisonStepLineItem,Date> dateColumn;
	Column<LiaisonStepLineItem,String> comment;
	Column<LiaisonStepLineItem,DocumentUpload> doc;
	Column<LiaisonStepLineItem,ObjectListBox<Employee>> person;
	Column<LiaisonStepLineItem,String> commentColumn;
	public ArrayList<String> employeeList;
	private Column<LiaisonStepLineItem, String> editserviceEng;
	TextColumn<LiaisonStepLineItem> index;
	TextColumn<LiaisonStepLineItem> groupName;
	TextColumn<LiaisonStepLineItem> viewemployeename;
	TextColumn<LiaisonStepLineItem> viewcomment;
	TextColumn<LiaisonStepLineItem> viewdate;
	TextColumn<LiaisonStepLineItem> viewstatus;
	
	

	SubStepTable response = new SubStepTable();
	LiaisonForm form;
	
	
	public StepTable()
	{
		//super();
		employeeList=new ArrayList<String>();
		//response=new ResponseStepsInfoTable();
		
		
	}
	/*public RequestStepsInfoTable(ResponseStepsInfoTable response)
	{
		//super();
		
		System.out.println("response 123" + response);
		int size=response.getDataprovider().getList().size();
		System.out.println("size=======dipak"+size);
		//response=new ResponseStepsInfoTable();
		employeeList=new ArrayList<String>();
		this.response= response;

		int size2=this.response.getDataprovider().getList().size();
		System.out.println("size=======dipak123" +size2);
		
	}*/
	
	@Override
	public void createTable() {
		addColumnIndex();
		addColumnGroupName();
		addColumnName();
		addColumnSLA();
		//addColumnRequestorResponse();
		addColumnMandatory();
		addColumnDependentStep();
		addColumnDescription();
		addColumnDate();
		retriveEmployees();
		
	
	}
	
	private void addeditColumn()
	{
		addColumnIndex();
		addColumnGroupName();
		addColumnName();
		addColumnSLA();
		//addColumnRequestorResponse();
		addColumnMandatory();
		addColumnDependentStep();
		addColumnDescription();
		addColumnDate();
		retriveEmployees();
	
	}
	
	
	private void addViewColumn()
	{
		addColumnIndex();
		addColumnGroupName();
		addColumnName();
		addColumnSLA();
		//addColumnRequestorResponse();
		addColumnMandatory();
		addColumnDependentStep();
		addColumnDescription();
		
		addColumnViewemployee();
		addColumnViewdate();
		addColumnViewComment();
		addColumnViewStatus();
		
		
		
	}
	
	public void addColumnViewComment()
	{
		viewcomment=new TextColumn<LiaisonStepLineItem>() {
			
			@Override
			public String getValue(LiaisonStepLineItem object) {
				// TODO Auto-generated method stub
				return object.getComment();
			}
		};table.addColumn(viewcomment,"Comment");
	}
	
	public void addColumnViewdate()
	{
		viewdate=new TextColumn<LiaisonStepLineItem>() {
			
			@Override
			public String getValue(LiaisonStepLineItem object) {
				// TODO Auto-generated method stub
				return AppUtility.parseDate(object.getDate());
			}
		};table.addColumn(viewdate,"Date");
	}
	
	public void addColumnViewStatus()
	{
		viewstatus=new TextColumn<LiaisonStepLineItem>() {
			
			@Override
			public String getValue(LiaisonStepLineItem object) {
				if(object.getStatusss()==true)
				return "Complete";
				else
					return "Incomplete";
			}
		};table.addColumn(viewstatus,"Status");
	}
	
	public void addColumnViewemployee()
	{
		viewemployeename=new TextColumn<LiaisonStepLineItem>() {
			
			@Override
			public String getValue(LiaisonStepLineItem object) {
				// TODO Auto-generated method stub
				return object.getEmployee();
			}
		};table.addColumn(viewemployeename,"Responsible Person");
	}
	
	
	public void addColumnGroupName()
	{
		groupName=new TextColumn<LiaisonStepLineItem>() {
			
			@Override
			public String getValue(LiaisonStepLineItem object) {
				// TODO Auto-generated method stub
				return object.getGroupName();
			}
		};table.addColumn(groupName,"Group Name");
	}
	public void addColumnIndex()
	{
		index=new TextColumn<LiaisonStepLineItem>() {
			
			@Override
			public String getValue(LiaisonStepLineItem object) {
				// TODO Auto-generated method stub
				
				
			return object.getIndex()+"";
			}
		};table.addColumn(index,"Index");
	}
	
	
	
	public void retriveEmployees()
	{
		final GenricServiceAsync service=GWT.create(GenricService.class);
		Vector<Filter>filtervalue=new Vector<Filter>(); 
		MyQuerry query=new MyQuerry();
		query.setFilters(filtervalue);
		query.setQuerryObject(new Employee());
		System.out.println("service"+service);
		service.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
			
		@Override
		public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				
				for(SuperModel model:result)
				{
				 Employee entity=(Employee) model;
				 employeeList.add(entity.getFullname());
				}
				addEditColumnEmployee();
				addeditcomment();
				//addcolumndocument();
				addColumnStatus();
				addFieldUpdater();
				
			}
		});
		
	}
	
	public void addEditColumnEmployee()
	{
		SelectionCell employeeselection=new SelectionCell(employeeList);
		editserviceEng=new Column<LiaisonStepLineItem, String>(employeeselection) {

			@Override
			public String getValue(LiaisonStepLineItem object) {	
				
				if(object.getEmployee()!=null){
				return object.getEmployee();}
				else
					return null;
			}
		};table.addColumn(editserviceEng,"Reponsible Person");
		
	}
	
	public void addcolumndocument()
	{
		
		Cell<DocumentUpload> document=(Cell<DocumentUpload>) new DocumentUpload();
		doc=new Column<LiaisonStepLineItem, DocumentUpload>(document) {
			
			@Override
			public DocumentUpload getValue(LiaisonStepLineItem object) {
				// TODO Auto-generated method stub
				return null;
			}
		};table.addColumn(doc,"Document");
	}
	
	public void addColumnComment()
	{
		EditTextCell editCell=new EditTextCell();
		comment=new TextColumn<LiaisonStepLineItem>() {
			
			@Override
			public String getValue(LiaisonStepLineItem object) {
				// TODO Auto-generated method stub
				return null;
			}
		};table.addColumn(comment,"Comment");
	}
	
	public void addeditcomment()
	{
		EditTextCell editCell=new EditTextCell();
		//TextCell editCell=new TextCell();
		commentColumn=new Column<LiaisonStepLineItem, String>(editCell) {
			
			@Override
			public String getValue(LiaisonStepLineItem object) {
				// TODO Auto-generated method stub
				if(object.getComment()==null){
					return " ";}
				else{
				return object.getComment();
				}
			}
		};table.addColumn(commentColumn,"Comment");
	}

 	public void addColumnStatus()
	{
		CheckboxCell chkcell=new CheckboxCell();
		stattus=new Column<LiaisonStepLineItem, Boolean>(chkcell) {
			
			@Override
			public Boolean getValue(LiaisonStepLineItem object) {
				// TODO Auto-generated method stub
				
				
				return object.getStatusss();
			}
		};table.addColumn(stattus,"Status");
	}
	
	public void addColumnDate()
	{
		DateTimeFormat fmt = DateTimeFormat.getFormat(PredefinedFormat.DATE_SHORT);
		DatePickerCell date= new DatePickerCell(fmt);
		dateColumn=new Column<LiaisonStepLineItem, Date>(date) {
			
			@Override
			public Date getValue(LiaisonStepLineItem object) {
				// TODO Auto-generated method stub
				if(object.getDate()!=null){
					return object.getDate();}
				else{object.setDate(new Date());
				return object.getDate();
				}
			}};table.addColumn(dateColumn,"Date");
	}
	
	
	
	public void addColumnName()
	{
		name=new TextColumn<LiaisonStepLineItem>() {
			
			@Override
			public String getValue(LiaisonStepLineItem object) {
				// TODO Auto-generated method stub
				return object.getStep();
			}
		};table.addColumn(name,"Name");
	}
	
	public void addColumnSLA()
	{
		SLA=new TextColumn<LiaisonStepLineItem>() {
			
			@Override
			public String getValue(LiaisonStepLineItem object) {
				// TODO Auto-generated method stub
				return object.getSLA()+"";
			}
		};table.addColumn(SLA,"SLA");
	}
	
	public void addColumnMandatory()
	{
		mandatory=new TextColumn<LiaisonStepLineItem>() {
			
			@Override
			public String getValue(LiaisonStepLineItem object) {
				// TODO Auto-generated method stub
				return object.getMandatory()+"";
			}
		};table.addColumn(mandatory,"Mandatory");
	}

	public void addColumnDependentStep()
	{
	dependentStep=new TextColumn<LiaisonStepLineItem>() {
		
		@Override
		public String getValue(LiaisonStepLineItem object) {
			// TODO Auto-generated method stub
			return object.getParentStep();
		}
	};	table.addColumn(dependentStep,"Dependent Step");
	}
	
	public void addColumnDescription()
	{
		description=new TextColumn<LiaisonStepLineItem>() {
			
			@Override
			public String getValue(LiaisonStepLineItem object) {
				// TODO Auto-generated method stub
				return object.getDescription();
			}
		};table.addColumn(description,"Description");
	}
	
	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		createFieldUpdaterEmployee();
		createFieldUpdaterComment();
		createFieldUpdaterdateColumn();
		createFieldUpdaterStatus();
		
	//	addEditColumnEmployee();
	}

	@Override
	public void setEnable(boolean state) {
		int tablecolcount=this.table.getColumnCount();
		for(int i=tablecolcount-1;i>-1;i--)
		  table.removeColumn(i);
		if(state ==true)
			addeditColumn();
		if(state==false)
			addViewColumn();
		
	}

	@Override
	public void applyStyle() {
		if(LiaisonPresenter.bool==1){
			//changeColor();	
			}

		
	}
	
	protected void createFieldUpdaterComment()
	{
		commentColumn.setFieldUpdater(new FieldUpdater<LiaisonStepLineItem, String>()
				{
			@Override

			public void update(int index,LiaisonStepLineItem object,String value)
			{

				try{
					String val1=(value.trim());
					if(val1!=null){
					object.setComment(val1);}
					else 
						object.setComment("no");
					
				}
				catch (NumberFormatException e)
				{

				}


				table.redrawRow(index);
			}
				});
	}

	protected void createFieldUpdaterdateColumn()
	{
		dateColumn.setFieldUpdater(new FieldUpdater<LiaisonStepLineItem, Date>()
				{
			@Override
			public void update(int index,LiaisonStepLineItem
					object,Date value)
			{
				object.setDate(value);
				table.redrawRow(index);
			}
				});
	}
	
	protected void createFieldUpdaterEmployee()
	{
		editserviceEng.setFieldUpdater(new FieldUpdater<LiaisonStepLineItem, String>()
				{
			@Override

			public void update(int index,LiaisonStepLineItem object,String value)
			{

				try{
					String val1=(value.trim());
					
					object.setEmployee(val1);
					 
						
					
				}
				catch (NumberFormatException e)
				{

				}


				table.redrawRow(index);
			}
				});
	}

	
	protected void createFieldUpdaterStatus()
	{	
		
	
		stattus.setFieldUpdater(new FieldUpdater<LiaisonStepLineItem, Boolean>()
				{
			@Override

			public void update(int index,LiaisonStepLineItem object,Boolean value)
			{

				try{
					Boolean val1=(value);
					
					object.setStatusss(val1);
					 
						
					
				}
				catch (NumberFormatException e)
				{

				}


				table.redrawRow(index);
			}
				});
	}

	
	public void changeColor()
	{
		System.out.println("insidestyle"+LiaisonPresenter.k);
		table.getRowElement(LiaisonPresenter.k).scrollIntoView();
		table.setRowStyles(new RowStyles<LiaisonStepLineItem>() {
			
			@Override
			public String getStyleNames(LiaisonStepLineItem row, int rowIndex) {
				if(rowIndex==LiaisonPresenter.k){
					return "changecolorRed";
				}
				else
					return "normaltext";
				
			}
		});
		
		table.redraw();
	}

	public void changetoNormalColor()
	{
		System.out.println("insidestyle"+LiaisonPresenter.k);
		table.setRowStyles(new RowStyles<LiaisonStepLineItem>() {
			
			@Override
			public String getStyleNames(LiaisonStepLineItem row, int rowIndex) {
				if(rowIndex==LiaisonPresenter.k){
					return "normaltext";
				}
				else
					return "normaltext";
				
			}
		});
		
		table.redraw();
	}

	
	
}
