package com.slicktechnologies.client.views.serviceliaison.liaison;

import java.util.ArrayList;
import java.util.List;


import com.gargoylesoftware.htmlunit.protocol.about.Handler;
import com.google.appengine.api.xmpp.Message;
import com.google.gwt.cell.client.Cell;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.view.client.NoSelectionModel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.humanresource.advance.AdvancePresenterSearchProxy;
import com.slicktechnologies.client.views.humanresource.advance.AdvancePresenterTableProxy;
import com.slicktechnologies.client.views.serviceliaison.liaisonsteps.ReponseStepInfoTable;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.serviceLiaisonLayer.Liaison;
import com.slicktechnologies.shared.common.serviceLiaisonLayer.LiaisonGroup;
import com.slicktechnologies.shared.common.serviceLiaisonLayer.LiaisonStepLineItem;
import com.slicktechnologies.shared.common.serviceLiaisonLayer.LiaisonSubstepLineItem;


import com.slicktechnologies.shared.common.serviceLiaisonLayer.LiaisonSubstep;

public class LiaisonPresenter extends FormScreenPresenter<Liaison> implements ChangeHandler
{
	
	public static LiaisonForm form;
	final GenricServiceAsync async=GWT.create(GenricService.class);
	 public static int size=0;
	 public static int rowcont=1;
	 //public ResponseStepsInfoTable responseStepTable;
	 public static int bool=0;
	public static String title="";
	public static int k=0;
	public static ArrayList<Integer> arrlist;/* = new ArrayList<Integer>(10);*/
	public static ArrayList<Integer> arrlt;
	
	public LiaisonPresenter  (FormScreen<Liaison> view, Liaison model) {
		super(view, model);
		form=(LiaisonForm) view;
		setTableSelectionOnStepTable();
		form.olbliaisongroup.addChangeHandler(this);
	form.requestStepTable.connectToLocal();
	form.responseStepTable.connectToLocal();
	
	  boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.LIASION,LoginPresenter.currentModule.trim());
			if(isDownload==false){
				form.getSearchpopupscreen().getDwnload().setVisible(false);
			}
	
	}

	public LiaisonPresenter (FormScreen<Liaison> view, Liaison model,MyQuerry querry) {
		super(view, model);
		form=(LiaisonForm) view;
		setTableSelectionOnStepTable();
		form.olbliaisongroup.addChangeHandler(this);
	form.requestStepTable.connectToLocal();
	form.responseStepTable.connectToLocal();
	
	  boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.LIASION,LoginPresenter.currentModule.trim());
			if(isDownload==false){
				form.getSearchpopupscreen().getDwnload().setVisible(false);
			}
		}
	
	
	
	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		
		InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();
		
		if(text.equals("Mark Complete"))
		{
			
			if(validateAll()==true)
			{
			System.out.println("completed");	
			ChangeStatus(Liaison.LIASIONCOMPLETED);
			}
		}
		if(text.equals("Cancel"))
		{
			ChangeStatus(Liaison.LIASIONCANCELLED);
		}
		if(text.equals("New"))
		{
			LiaisonPresenter.initalize();
			form.setToNewState();
		}
	}

	@Override
	public void reactOnPrint() {


	}

	@Override
	public void reactOnDownload() 
	{
		CsvServiceAsync async=GWT.create(CsvService.class);
		ArrayList<Liaison> liaisonarray=new ArrayList<Liaison>();
		List<Liaison> list=form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
		
		
		liaisonarray.addAll(list); 
		System.out.println(liaisonarray.size());
		
		
		async.setLiaison(liaisonarray, new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);
				
			}

			@Override
			public void onSuccess(Void result) {
				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "csvservlet"+"?type="+44;
				Window.open(url, "test", "enabled");
			}
		});

	}

	

	@Override
	public void reactOnEmail() {


	}

	/**
	 * Method token to make new model
	 */
	@Override
	protected void makeNewModel() {
		model=new Liaison();
	}




	public static LiaisonForm initalize()
	{
		
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Liaison",Screen.LIASION);
		
		LiaisonPresenterTable gentable=new LiaisonPresenterTableProxy();
		form=new  LiaisonForm();
		gentable.setView(form);
		gentable.applySelectionModle();
		LiaisonPresenterTableProxy gentableSearch= new LiaisonPresenterTableProxy();

		
		 LiaisonPresenterSearch.staticSuperTable=gentableSearch;
		    gentableSearch.setView(form);
			gentableSearch.applySelectionModle();
		      LiaisonPresenterSearch searchpopup=new LiaisonPresenterSearchProxy();

		           form.setSearchpopupscreen(searchpopup);
		       
		 
		
		 LiaisonPresenter  presenter=new LiaisonPresenter(form,new Liaison());
		AppMemory.getAppMemory().stickPnel(form);
		return form;
		}
	
	
	public static LiaisonForm initalize(MyQuerry querry)
	{
		form=new  LiaisonForm();
		
		
		LiaisonPresenterTable gentable=new LiaisonPresenterTableProxy();
		
		gentable.setView(form);
		 gentable.applySelectionModle();
		 LiaisonPresenterTable gentableSearch=new LiaisonPresenterTableProxy();
		 LiaisonPresenterSearch.staticSuperTable=gentableSearch;
		    gentableSearch.setView(form);
			gentableSearch.applySelectionModle();
		      LiaisonPresenterSearch searchpopup=new LiaisonPresenterSearchProxy();

		           form.setSearchpopupscreen(searchpopup);
		 LiaisonPresenter  presenter=new LiaisonPresenter(form,new Liaison(),querry);
		AppMemory.getAppMemory().stickPnel(form);
		System.out.println("initalize22222");
		return form;
		
	}

	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.serviceLiaisonLayer.Liaison")
	public static  class LiaisonPresenterSearch extends SearchPopUpScreen<Liaison>{

		@Override
		public MyQuerry getQuerry() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public boolean validate() {
			// TODO Auto-generated method stub
			return true;
		}};

		@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.serviceLiaisonLayer.Liaison")
		public static class LiaisonPresenterTable extends SuperTable<Liaison> implements GeneratedVariableRefrence{

			@Override
			public Object getVarRef(String varName) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void createTable() {
				// TODO Auto-generated method stub

			}

			@Override
			protected void initializekeyprovider() {
				// TODO Auto-generated method stub

			}

			@Override
			public void addFieldUpdater() {
				// TODO Auto-generated method stub

			}

			@Override
			public void setEnable(boolean state) {
				// TODO Auto-generated method stub

			}

			@Override
			public void applyStyle() {
				// TODO Auto-generated method stub

			}} ;
			
	public void setTableSelectionOnStepTable()
				 {
				  System.out.println("inside selection");
					 final NoSelectionModel<LiaisonStepLineItem> selectionModelMyObj = new NoSelectionModel<LiaisonStepLineItem>();
				     
					 SelectionChangeEvent.Handler  tableHandler = new SelectionChangeEvent.Handler()
					 {
					 @Override
			         public void onSelectionChange(SelectionChangeEvent event) 
			         {
			        	  form.responseStepTable.connectToLocal();
						 final LiaisonStepLineItem entity=selectionModelMyObj.getLastSelectedObject();
			        	  List<LiaisonSubstepLineItem>subStep=entity.getSubStepLineItem();
			   		      form.responseStepTable.setValue(subStep);
			        	
			         }
					 };
					 
					  selectionModelMyObj.addSelectionChangeHandler( tableHandler );
					  form.requestStepTable.getTable().setSelectionModel(selectionModelMyObj);
					
				 }

			@Override
			public void onChange(ChangeEvent event) {
			
				/*
				if(event.getSource()==form.olbliaisongroup)
				{
					form.requestStepTable.connectToLocal();
					form.responseStepTable.connectToLocal();
					if(form.olbliaisongroup.getValue()!=null){
					form.SetRequestTable();
					System.out.println("Response 456" + form.responseStepTable);
					int size=form.responseStepTable.getDataprovider().getList().size();
					System.out.println("size=======sumedh"+size);
					
					
					}
					*/
				}
			
				
			

			
			
			public void getResponse()
			{
				arrlist=new ArrayList<Integer>(10);
				List<LiaisonSubstepLineItem> list=form.responseStepTable.getDataprovider().getList();
				
				for(int i=0;i<list.size();i++)
				{
					if(list.get(i).getRefindex()==rowcont)
					{
						System.out.println("i=="+i);
						arrlist.add(i);
					}
					else
						System.out.println("invisible fields");
				}
				

				
			}

			
			public void ChangeStatus(final String status)
			{
				
				model.setStatus(status);
				async.save(model,new AsyncCallback<ReturnFromServer>() {

					@Override
					public void onFailure(Throwable caught) {
						form.showDialogMessage("An Unexpected Error occured !");
						
					}

					@Override
					public void onSuccess(ReturnFromServer result) {
					//	form.setMenuAsPerStatus();
						form.setToViewState();
						form.tbStatus.setText(status);
						form.setMenuAsPerStatus();
						
					}
				});

			}
			
			public void markResponse()
			{
				arrlt=new ArrayList<Integer>();
				System.out.println("inside markresponse");
				List<LiaisonSubstepLineItem> responsedata=form.responseStepTable.getDataprovider().getList();
				for(int i=0;i<responsedata.size();i++)
				{
					if(responsedata.get(i).getReponseMandatory().equals("Yes"))
					{
						if(responsedata.get(i).getStatusss().equals(false))
						{
							System.out.println("***********"+responsedata.get(i).getStatusss());
							k=i;
							arrlt.add(i);
						//	form.responseStepTable.changeColor();
							
							
						}
						
				}
				}
				System.out.println("mandatory"+arrlt);
				for(int j=0;j<arrlt.size();j++)
				{
					j=k;
					form.responseStepTable.changeColor();
				}
				

			}
			
			
			
	/***********************validate methods**********************************/
			
			
			
			/******************validate mandatory Step***********************/
			
			public boolean validateMandatorySteps()
			{
				List<LiaisonStepLineItem> request=form.requestStepTable.getDataprovider().getList();
				
				for(int i=0;i<request.size();i++)
				{
					if(request.get(i).getMandatory()==true&&request.get(i).getStatusss().equals(false))
					{
						form.showDialogMessage("Complete Mandatory Steps");
						form.requestStepTable.changeColor();
					   return false;
				   }
				
			    }
				
				return true;
			
			}
			
			
			/******************validate parent step************************/
			
			public boolean validateParentStep()
			{
				String depeStep="";
				List<LiaisonStepLineItem> request=form.requestStepTable.getDataprovider().getList();
				for(int i=0;i<request.size();i++)
				{
				if(!request.get(i).getParentStep().equals("")&&request.get(i).getStatusss()==true)
				{					
					    depeStep=request.get(i).getParentStep();
					    for(int k=0;k<request.size();k++)
					    {
					    	if(request.get(k).getStep().equals(depeStep)&&request.get(k).getStatusss()==false)
					    	{
							   form.showDialogMessage(depeStep+" is parent step it must be completed ");
							    return false;
						     }
					   }
					}
			
				}
				
				return true;
			}
			
			/**************validate mandatory Substep******************/
			
			public boolean validateMandatorySubstep()
			{
				List<LiaisonSubstepLineItem> responsedata=form.responseStepTable.getDataprovider().getList();
				for(int i=0;i<responsedata.size();i++)
				{
				if(responsedata.get(i).getReponseMandatory().equals(true)&&responsedata.get(i).getStatusss().equals(false))
					{
								form.showDialogMessage("Complete Mandatory Substeps");
								rowcont=responsedata.get(i).getRefindex();
								getResponse();
								form.responseStepTable.setvisible();
								return false;	
					}
					
				}
				return true;
			}
			
			/***************validate non mandatory step**************/
			
			public boolean validateNonMandatoryStep()
			{
				List<LiaisonStepLineItem> request=form.requestStepTable.getDataprovider().getList();
				for(int i=0;i<request.size();i++)
				{
		
				if(request.get(i).getMandatory()==false&&request.get(i).getStatusss().equals(false))
				{
					boolean conf=Window.confirm("Non mandatory Step not completed..");
							if(conf==true)
								{
						System.out.println("press ok");
						i=request.size();
						return true;
						
								}
					else{
						System.out.println("cancel");
						return false;
						}
				}
			
			}
				return true;
			}
			
			
			/***********validate non mandatory Substep****************/
			
			public boolean validateNonmandatorySubstep()
			{
				List<LiaisonSubstepLineItem> responsedata=form.responseStepTable.getDataprovider().getList();
				for(int i=0;i<responsedata.size();i++){
				if(responsedata.get(i).getReponseMandatory().equals(false)&&responsedata.get(i).getStatusss().equals(false))
				{
							boolean conf=Window.confirm("Non mandatory Substep not completed");
							if(conf==true){
								i=responsedata.size();
								
											}
							else return false;
				}
				}

				return true;
			}
			
			public boolean validateAll()
			{
					
			
				boolean a =validateMandatorySteps();
				if(a==false)
					return false;
				boolean b=validateMandatorySteps();
				if(b==false)
					return false;
				
				boolean c=validateParentStep();
				if(c==false)
					return false;
				
				boolean d=validateNonMandatoryStep();
				if(d==false)
					return false;
				
				boolean e=validateNonmandatorySubstep();
				if(e==false)
					return false;
				
				return true;
			}
			
}
