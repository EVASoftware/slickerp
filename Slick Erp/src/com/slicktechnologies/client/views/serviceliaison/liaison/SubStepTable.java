package com.slicktechnologies.client.views.serviceliaison.liaison;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.SelectionCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.RowStyles;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.personlayer.Employee;



import com.slicktechnologies.shared.common.serviceLiaisonLayer.LiaisonStepLineItem;
import com.slicktechnologies.shared.common.serviceLiaisonLayer.LiaisonSubstepLineItem;


public class SubStepTable extends SuperTable<LiaisonSubstepLineItem>{

	
	TextColumn<LiaisonSubstepLineItem> name;
	TextColumn<LiaisonSubstepLineItem> SLA;
	//TextColumn<Liaison> requestStep;
	TextColumn<LiaisonSubstepLineItem> mandatory;
	TextColumn<LiaisonSubstepLineItem> dependentStep;
	TextColumn<LiaisonSubstepLineItem> description;
	private Column<LiaisonSubstepLineItem,Boolean> stattus;
	Column<LiaisonSubstepLineItem,Date> dateColumn;
	Column<LiaisonSubstepLineItem,String> comment;
	Column<LiaisonSubstepLineItem,DocumentUpload> doc;
	Column<LiaisonSubstepLineItem,ObjectListBox<Employee>> person;
	Column<LiaisonSubstepLineItem,String> commentColumn;
	public ArrayList<String> employeeList;
	private Column<LiaisonSubstepLineItem, String> editserviceEng;
	TextColumn<LiaisonSubstepLineItem> index;
	TextColumn<LiaisonSubstepLineItem> requestStep;
	TextColumn<LiaisonSubstepLineItem> viewEmployee;
	TextColumn<LiaisonSubstepLineItem> viewDate;
	TextColumn<LiaisonSubstepLineItem> viewComment;
	TextColumn<LiaisonSubstepLineItem> viewStatus;
	
	
	public SubStepTable()
	{
		super();
		employeeList=new ArrayList<String>();
	}
	
	@Override
	public void createTable() {
		//addRefIndex();
		addColumnRequestName();
		addColumnName();
		addColumnSLA();
		
		addColumnMandatory();
	
		addColumnDescription();
		addColumnDate();
		retriveEmployees();
		

		
	}
	
	public void addeditColumn()
	{
		addColumnRequestName();
		addColumnName();
		addColumnSLA();
		
		addColumnMandatory();
	
		addColumnDescription();
		addColumnDate();
		retriveEmployees();
	
	}
	
	public void addViewColumn()
	{
		addColumnRequestName();
		addColumnName();
		addColumnSLA();
		
		addColumnMandatory();
	
		addColumnDescription();
		addColumnViewDate();
		addColumnViewEmployee();
		addColumnViewComment();
		addColumnViewStatus();
	
	}
	
	public void addColumnViewDate()
	{
		viewDate=new TextColumn<LiaisonSubstepLineItem>() {
			
			@Override
			public String getValue(LiaisonSubstepLineItem object) {
				// TODO Auto-generated method stub
				return AppUtility.parseDate(object.getDate());
			}
		};table.addColumn(viewDate,"Date");
	}
	
	public void addColumnViewEmployee()
	{
		viewEmployee=new TextColumn<LiaisonSubstepLineItem>() {
			
			@Override
			public String getValue(LiaisonSubstepLineItem object) {
				// TODO Auto-generated method stub
				return object.getEmployee();
			}
		};table.addColumn(viewEmployee,"Responsible Person");
	}
	
	
	public void addColumnViewComment()
	{
		viewComment=new TextColumn<LiaisonSubstepLineItem>() {
			
			@Override
			public String getValue(LiaisonSubstepLineItem object) {
				// TODO Auto-generated method stub
				return object.getComment();
			}
		};table.addColumn(viewComment,"Comment");
	}
	
	public void addColumnViewStatus()
	{
		viewStatus=new TextColumn<LiaisonSubstepLineItem>() {
			
			@Override
			public String getValue(LiaisonSubstepLineItem object) {
				if(object.getStatusss()==true)
				return "Complete";
				else return "Incoplete";
			}
		};table.addColumn(viewStatus,"Status");
	}
	public void addColumnRequestName()
	{
		requestStep=new TextColumn<LiaisonSubstepLineItem>() {
			
			@Override
			public String getValue(LiaisonSubstepLineItem object) {
				// TODO Auto-generated method stub
				return object.getRequestStep();
			}
		};table.addColumn(requestStep,"Request Step");
	}
	
	public void retriveEmployees()
	{
		final GenricServiceAsync service=GWT.create(GenricService.class);
		Vector<Filter>filtervalue=new Vector<Filter>(); 
		MyQuerry query=new MyQuerry();
		query.setFilters(filtervalue);
		query.setQuerryObject(new Employee());
		System.out.println("service"+service);
		service.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
			
		@Override
		public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				
				for(SuperModel model:result)
				{
				 Employee entity=(Employee) model;
				 employeeList.add(entity.getFullname());
				}
				addEditColumnEmployee();
				addeditcomment();
				//addcolumndocument();
				addColumnStatus();
				addFieldUpdater();
				
			}
		});
		
	}
	public void addeditcomment()
	{
		EditTextCell editCell=new EditTextCell();
		commentColumn=new Column<LiaisonSubstepLineItem, String>(editCell) {
			
			@Override
			public String getValue(LiaisonSubstepLineItem object) {
				// TODO Auto-generated method stub
				if(object.getComment()==null)
					return "";
				else
				return object.getComment();
			}
		};table.addColumn(commentColumn,"Comment");
	}
	
	public void addRefIndex()
	{
		index=new TextColumn<LiaisonSubstepLineItem>() {
			
			@Override
			public String getValue(LiaisonSubstepLineItem object) {
				// TODO Auto-generated method stub
				return object.getRefindex()+"";
			}
		};table.addColumn(index,"Ref Index");
	}
	
	public void addColumnStatus()
	{
		CheckboxCell chkcell=new CheckboxCell();
		stattus=new Column<LiaisonSubstepLineItem, Boolean>(chkcell) {
			
			@Override
			public Boolean getValue(LiaisonSubstepLineItem object) {
				// TODO Auto-generated method stub
				
				return object.getStatusss();
							}
		};table.addColumn(stattus,"Status");
	}
	
	/*public void addColumnComment()
	{
		EditTextCell editCell=new EditTextCell();
		comment=new TextColumn<LiaisonResponseLineItem>() {
			
			@Override
			public String getValue(LiaisonResponseLineItem object) {
				// TODO Auto-generated method stub
				return null;
			}
		};table.addColumn(comment,"Comment");
	}*/
	
	public void addEditColumnEmployee()
	{
		SelectionCell employeeselection=new SelectionCell(employeeList);
		editserviceEng=new Column<LiaisonSubstepLineItem, String>(employeeselection) {

			@Override
			public String getValue(LiaisonSubstepLineItem object) {	
				
				
				return object.getEmployee();
			}
		};table.addColumn(editserviceEng,"Reponsible Person");
		
	}
	
	public void addColumnName()
	{
		name=new TextColumn<LiaisonSubstepLineItem>() {
			
			@Override
			public String getValue(LiaisonSubstepLineItem object) {
				// TODO Auto-generated method stub
				return object.getReponseName();
			}
		};table.addColumn(name,"Name");
	}
	
	public void addColumnSLA()
	{
		SLA=new TextColumn<LiaisonSubstepLineItem>() {
			
			@Override
			public String getValue(LiaisonSubstepLineItem object) {
				// TODO Auto-generated method stub
				return object.getReponseSLA()+"";
			}
		};table.addColumn(SLA,"SLA");
	}
	
	public void addColumnMandatory()
	{
		mandatory=new TextColumn<LiaisonSubstepLineItem>() {
			
			@Override
			public String getValue(LiaisonSubstepLineItem object) {
				// TODO Auto-generated method stub
				return object.getReponseMandatory()+"";
			}
		};table.addColumn(mandatory,"Mandatory");
	}
	
	public void addColumnDescription()
	{
		description=new TextColumn<LiaisonSubstepLineItem>() {
			
			@Override
			public String getValue(LiaisonSubstepLineItem object) {
				// TODO Auto-generated method stub
				return object.getReponseDescription();
			}
		};table.addColumn(description,"Description");
	}
	
	public void addColumnDate()
	{
		DateTimeFormat fmt = DateTimeFormat.getFormat(PredefinedFormat.DATE_SHORT);
		DatePickerCell date= new DatePickerCell(fmt);
		dateColumn=new Column<LiaisonSubstepLineItem, Date>(date) {
		
			@Override
			public Date getValue(LiaisonSubstepLineItem object) {
				// TODO Auto-generated method stub
				if(object.getDate()!=null){
					return object.getDate();}
				else{object.setDate(new Date());
				return new Date();
				}
			}};table.addColumn(dateColumn,"Date");
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		createFieldUpdaterEmployee();
		createFieldUpdaterComment();
		createFieldUpdaterdateColumn();
		createFieldUpdaterStatus();

	}

	protected void createFieldUpdaterComment()
	{
		commentColumn.setFieldUpdater(new FieldUpdater<LiaisonSubstepLineItem, String>()
				{
			@Override

			public void update(int index,LiaisonSubstepLineItem object,String value)
			{

				try{
					String val1=(value.trim());
					if(val1!=null){
					object.setComment(val1);}
					else 
						object.setComment("no");
					
				}
				catch (NumberFormatException e)
				{

				}


				table.redrawRow(index);
			}
				});
	}


	
	protected void createFieldUpdaterdateColumn()
	{
		dateColumn.setFieldUpdater(new FieldUpdater<LiaisonSubstepLineItem, Date>()
				{
			@Override
			public void update(int index,LiaisonSubstepLineItem
					object,Date value)
			{
				object.setDate(value);
				table.redrawRow(index);
			}
				});
	}

	protected void createFieldUpdaterEmployee()
	{
		editserviceEng.setFieldUpdater(new FieldUpdater<LiaisonSubstepLineItem, String>()
				{
			@Override

			public void update(int index,LiaisonSubstepLineItem object,String value)
			{

				try{
					String val1=(value.trim());
					
					object.setEmployee(val1);
					 
						
					
				}
				catch (NumberFormatException e)
				{

				}


				table.redrawRow(index);
			}
				});
	}

	
	protected void createFieldUpdaterStatus()
	{
		stattus.setFieldUpdater(new FieldUpdater<LiaisonSubstepLineItem, Boolean>()
				{
			@Override

			public void update(int index,LiaisonSubstepLineItem object,Boolean value)
			{

				try{
					Boolean val1=(value);
					
					object.setStatusss(val1);
					 
						
					
				}
				catch (NumberFormatException e)
				{

				}


				table.redrawRow(index);
			}
				});
	}


	
	@Override
	public void setEnable(boolean state) {
		int tablecolcount=this.table.getColumnCount();
		for(int i=tablecolcount-1;i>-1;i--)
		  table.removeColumn(i);
		if(state ==true)
			addeditColumn();
		if(state==false)
			addViewColumn();

		
	}

	@Override
	public void applyStyle() {
				//setRowVisible();
		//if(LiaisonPresenter.bool==1){
			//changeColor();	}
		 if(LiaisonPresenter.bool==2)
		{
			setvisible();
		}
	
		
	}

	public void setvisible()
	{
		
		int start=0;
		int range=0;
		
		System.out.println("in visible method");
		//table.setVisible(true);
		//LiaisonPresenter.arrlist.get(index)
		 start=LiaisonPresenter.arrlist.get(0);
		 range=LiaisonPresenter.arrlist.size();
		table.setVisibleRange(start,range);
		System.out.println("start=="+start+"range"+range);
	}
	
	
	
	public void changeColor()
	{
		System.out.println("inside change color"+LiaisonPresenter.k);
		int row=LiaisonPresenter.k;
		
		table.setRowStyles(new RowStyles<LiaisonSubstepLineItem>() {
			
			@Override
			public String getStyleNames(LiaisonSubstepLineItem row, int rowIndex) {
				
				
				
				if(rowIndex==LiaisonPresenter.k)
				{
					
				return "changecolorRed";
				}
				else if(rowIndex!=LiaisonPresenter.k)
				{
					
					for(int k=0;k<LiaisonPresenter.arrlt.size();k++){
						if(rowIndex==LiaisonPresenter.arrlt.get(k)){
							return "changecolorRed";
						}
					
					}
				}
				else
					return "null";
				return "null";
				
			}
		});
		
		table.redraw();
		
	}
	
	
	public void changetoNormalColor()
	{
		System.out.println("inside normal color"+LiaisonPresenter.k);
		table.setRowStyles(new RowStyles<LiaisonSubstepLineItem>() {
			
			@Override
			public String getStyleNames(LiaisonSubstepLineItem row, int rowIndex) {
				if(rowIndex==LiaisonPresenter.k){
					return "normaltext";
				}
				else
					return "";
				
			}
		});
		
		table.redraw();
		
		
	}
}
