package com.slicktechnologies.client.views.serviceliaison.liaisongroup;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.CompositeInterface;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.serviceLiaisonLayer.LiaisonGroup;
import com.slicktechnologies.shared.common.serviceLiaisonLayer.LiaisonStep;


public class LiaisonGroupForm extends FormTableScreen<LiaisonGroup> implements ClickHandler,HasValue<ArrayList<LiaisonStep>>,CompositeInterface
{

	
	IntegerBox ibLiaisoGroupId;
	TextBox tbgroupName;
	
	CheckBox status;
	ObjectListBox<LiaisonStep> olbsteps;
		Button add,sub;
	LiaisonGroupInfoTable table;
	LiaisonGroup liasionObj;
	
	
	public LiaisonGroupForm(SuperTable<LiaisonGroup> table, int mode,
			boolean captionmode) {
		super(table, mode, captionmode);
		createGui();
		table.connectToLocal();
	}

	private void initalizeWidget()
	{
		ibLiaisoGroupId=new IntegerBox();
		ibLiaisoGroupId.setEnabled(false);
		status=new CheckBox();
		status.setValue(true);
		this.olbsteps=new ObjectListBox<LiaisonStep>();
		tbgroupName=new TextBox();
		
		add=new Button("Add");
		
		add.addClickHandler(this);
		table =new LiaisonGroupInfoTable();
		
		this.initalizeSteps();
	}

	/*
	 * Method template to create the formtable screen
	 */

	@Override
	public void createScreen() {

		//Token to initialize the processlevel menus.
		
		
		initalizeWidget();

		
		//////////////////////////////////Form Field Declaration/////////////////////////////////////////////////

		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////

		//Token to initialize formfield
		FormFieldBuilder fbuilder;
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingStepsInformation=fbuilder.setlabel("Group Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		
		fbuilder = new FormFieldBuilder("Group ID",ibLiaisoGroupId);
		FormField fibLiaisoGroupId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Request Step",olbsteps);
		FormField folbsteps= fbuilder.setMandatory(false).setMandatoryMsg("Name is mandatory").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Status",status);
		FormField fstatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		
		fbuilder = new FormFieldBuilder("* Group Name",tbgroupName);
		FormField ftbgroupName= fbuilder.setMandatory(true).setMandatoryMsg("Group name is mandatory").setRowSpan(0).setColSpan(0).build();

		
		fbuilder = new FormFieldBuilder("",add);
		FormField fadd= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",table.getTable());
		FormField ftable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(5).build();
		
		
		
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////


		FormField[][] formfield = {   {fgroupingStepsInformation},
				{fibLiaisoGroupId,ftbgroupName,folbsteps,fadd,fstatus},{ftable}
			
		};


		this.fields=formfield;		
	}

	
	/**
	 * method template to update the model with token entity name
	 */
	@Override
	public void updateModel(LiaisonGroup model) 
	{
		if(tbgroupName.getValue()!=null)
			model.setGroupName(tbgroupName.getValue());
		if(status.getValue()!=null)
			model.setStatus(status.getValue());
		
		
		List<LiaisonStep> daysalloc = table.getDataprovider().getList();
	    ArrayList<LiaisonStep> alloc = new ArrayList<LiaisonStep>();
	    alloc.addAll(daysalloc);
		model.setStepsinfo(alloc);
		
	//	model.setStepsinfo(table.set);

		liasionObj=model;
			presenter.setModel(model);
	}

	/**
	 * method template to update the view with token entity name
	 */
	@Override
	public void updateView(LiaisonGroup view) 
	{
		liasionObj=view;
		
		ibLiaisoGroupId.setValue(view.getCount());
		if(view.getGroupName()!=null)
			tbgroupName.setValue(view.getGroupName());
		status.setValue(view.getStatus());
		table.setValue(view.getStepsinfo());
		
				
				presenter.setModel(view);

	}

	/**
	 * Toggles the app header bar menus as per screen state
	 */

	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		
		 AuthorizationHelper.setAsPerAuthorization(Screen.LIAISONGROUP,LoginPresenter.currentModule.trim());
	}

	/**
	 * sets the id textbox with the passed count value. 
	 */
	@Override
	public void setCount(int count)
	{
		this.ibLiaisoGroupId.setValue(presenter.getModel().getCount());
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		super.setEnable(state);
		ibLiaisoGroupId.setEnabled(false);
		table.setEnable(state);
	}
	
	@Override
	public void onClick(ClickEvent event)
	{
		if(event.getSource()==this.add)
		{
			
			String name=olbsteps.getValue();
			
			
				List<LiaisonStep> list=table.getDataprovider().getList();
				for(LiaisonStep val:list)
				{
					if(name.equals(val.getStepName()))
					{
						this.showDialogMessage("Can not add duplicate Request Step");
						return;
						
					}
				}
					
				addRequestStep();
					}
				}

			
	
	
		public void addRequestStep()
		{
			LiaisonStep ad=new LiaisonStep();
			LiaisonStep info=olbsteps.getSelectedItem();
			ad.setStepName(olbsteps.getValue());
			ad.setSLA(info.getSLA());
			ad.setDescription(info.getDescription());
			ad.setMandatory(info.getMandatory());
			ad.setParentStep(info.getParentStep());
			ad.setResponseinfo(info.getResponseinfo());
			table.getDataprovider().getList().add(ad);

		}
		
			
	
	protected void initalizeSteps()
	{
		MyQuerry querry = new MyQuerry();
		Filter filter=new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		querry.getFilters().add(filter);
		querry.setQuerryObject(new LiaisonStep());
		
		olbsteps.MakeLive(querry);
	}

	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<ArrayList<LiaisonStep>> handler) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void fireEvent(GwtEvent<?> event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ArrayList<LiaisonStep> getValue() {
		List<LiaisonStep>list=this.table.getDataprovider().getList();
		ArrayList<LiaisonStep>vec=new ArrayList<LiaisonStep>();
		vec.addAll(list);
		return vec;
	}

	@Override
	public void setValue(ArrayList<LiaisonStep> value) {
		table.connectToLocal();
		table.getDataprovider().getList().addAll(value);

		
	}

	@Override
	public void setValue(ArrayList<LiaisonStep> value, boolean fireEvents) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void clear() {
			super.clear();
			table.clear();
			
	}
	
	
	@Override
	public boolean validate()
	{
		
		
		
		System.out.println("validate");
		
		if(super.validate()){
		if(this.table.getDataprovider().getList().size()==0)
		{
			this.showDialogMessage("Add steps");
			return false;
		}
		else if(validateDependent()==true){
			return true;
		}
		else return false;
			
		
		}
		return false;
		
		
	}

	public boolean validateDependent()
	{
		System.out.println("validate dependent");
		boolean val=true;
		String depeStep="";
		List<LiaisonStep> list=table.getDataprovider().getList();
		for(int i=0;i<list.size();i++)
		{
			if(!list.get(i).getParentStep().trim().equals("")){
				 depeStep=list.get(i).getParentStep();
				 
				 int k;
				 for(k=0;k<list.size();k++)
					if(list.get(k).getStepName().equals(depeStep))
							 break;
							 
					if(k==list.size())
					{
						this.showDialogMessage("Add Parent Step "+list.get(i).getParentStep()+" for "+list.get(i).getStepName());
						return false;
					}		 
						
						
					}
		}
		
		list=table.getDataprovider().getList();
		
		return val;
	}
	
	@Override
	public void setToViewState()
	{
		super.setToViewState();
		table.setEnable(false);
		
		SuperModel model=new LiaisonGroup();
		model=liasionObj;
		AppUtility.addDocumentToHistoryTable(AppConstants.SERVICEMODULE,AppConstants.LIASIONGROUP, liasionObj.getCount(), null,null,null, false, model, null);
		
	}
	
	@Override
	public void setToEditState()
	{
		super.setToEditState();
		table.setEnable(true);
	}
	
}
