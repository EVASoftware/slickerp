package com.slicktechnologies.client.views.serviceliaison.liaisongroup;

import java.util.ArrayList;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.view.client.ProvidesKey;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.serviceLiaisonLayer.LiaisonGroup;

import com.slicktechnologies.shared.common.serviceLiaisonLayer.LiaisonStep;
//import com.slicktechnologies.shared.common.serviceLiaisonLayer.LiaisonStepsResponseinfo;



public class LiaisonGroupInfoTable extends SuperTable<LiaisonStep>
{

	TextColumn<LiaisonStep> steps;
	TextColumn<LiaisonStep> description;
	TextColumn<LiaisonStep> SLA;
	TextColumn<LiaisonStep> RequestStep;
	TextColumn<LiaisonStep> depedentStep;
	TextColumn<LiaisonStep> mandatory;
	TextColumn<LiaisonStep> response;

	
	private Column<LiaisonStep, String> delete;
	 public LiaisonGroupInfoTable() {
		super();
	}
	
	@Override
	public void createTable() {
		addColumnSteps();
		addColumnSLA();
		addColumnDescription();
		addColumnMandatory();
		addColumnDepenedentStep();
	
		addColumnDelete();
		setFieldUpdaterOnDelete();
		table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
		
	}
	
	public void addColumnResponseSteps()
	{
		response=new TextColumn<LiaisonStep>() {
			
			@Override
			public String getValue(LiaisonStep object) {
				// TODO Auto-generated method stub
				return object.getResponseinfo().toString();
			}
		};table.addColumn(response,"Response Steps");
	}
	
	private void addeditColumn()
	{
		addColumnSteps();
		addColumnSLA();
		addColumnDescription();
		addColumnMandatory();
		addColumnDepenedentStep();
		
		addColumnDelete();
		setFieldUpdaterOnDelete();
		
		
	}
	
	private void addViewColumn()
	{
		addColumnSteps();
		addColumnSLA();
		addColumnDescription();
		addColumnMandatory();
		addColumnDepenedentStep();
	}
	
	
	public void addColumnSteps()
	{
	steps= new TextColumn<LiaisonStep>() {
		
		@Override
		public String getValue(LiaisonStep object) {
			// TODO Auto-generated method stub
			return object.getStepName();
		}
	};table.addColumn(steps,"Steps");
	}

	public void addColumnSLA()
	{
		SLA=new TextColumn<LiaisonStep>() {
			
			@Override
			public String getValue(LiaisonStep object) {
				// TODO Auto-generated method stub
				return object.getSLA()+"";
			}
		};table.addColumn(SLA,"SLA");
	}
	
	public void addColumnDescription()
	{
		description=new TextColumn<LiaisonStep>() {
			
			@Override
			public String getValue(LiaisonStep object) {
				// TODO Auto-generated method stub
				return object.getDescription();
			}
		};table.addColumn(description,"Description");
	}
	
	public void addColumnMandatory()
	{
		mandatory=new TextColumn<LiaisonStep>() {
			
			@Override
			public String getValue(LiaisonStep object) {
				// TODO Auto-generated method stub
				return object.getMandatory()+"";
			}
		};table.addColumn(mandatory,"Mandatory");
	}
	public void addColumnDepenedentStep()
	{
		depedentStep=new TextColumn<LiaisonStep>() {
			
			@Override
			public String getValue(LiaisonStep object) {
				// TODO Auto-generated method stub
				return object.getParentStep();
			}
		};table.addColumn(depedentStep,"Parent Step");
	}
	
	public void addColumnDelete()
	{
		ButtonCell btnCell= new ButtonCell();
		delete = new Column<LiaisonStep, String>(btnCell) {

			@Override
			public String getValue(LiaisonStep object) {
				
				return "Delete";
			}
		};
		table.addColumn(delete,"Delete");

	}
	
	private void setFieldUpdaterOnDelete()
	{
		delete.setFieldUpdater(new FieldUpdater<LiaisonStep, String>() {
			
			@Override
			public void update(int index, LiaisonStep object, String value) {
				getDataprovider().getList().remove(index);
				table.redrawRow(index);
			
			}
		});
	}

	
	@Override
	protected void initializekeyprovider() {
		keyProvider = new ProvidesKey<LiaisonStep>() {
			@Override
			public Object getKey(LiaisonStep item) {
				if(item==null)
					return null;
				else
					return item.getId();
			}
		};
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		int tablecolcount=this.table.getColumnCount();
		for(int i=tablecolcount-1;i>-1;i--)
		  table.removeColumn(i);
		if(state ==true)
			addeditColumn();
		if(state==false)
			addViewColumn();
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
