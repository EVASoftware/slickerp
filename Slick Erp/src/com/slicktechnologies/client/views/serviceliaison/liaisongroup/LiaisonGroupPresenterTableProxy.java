package com.slicktechnologies.client.views.serviceliaison.liaisongroup;

import com.google.gwt.user.cellview.client.TextColumn;
import com.slicktechnologies.client.views.serviceliaison.liaisongroup.LiaisonGroupPresenter.LiaisonGroupPresenterTable;
import com.slicktechnologies.shared.common.serviceLiaisonLayer.LiaisonGroup;

public class LiaisonGroupPresenterTableProxy extends LiaisonGroupPresenterTable
{
	TextColumn<LiaisonGroup> columnID;
	TextColumn<LiaisonGroup> columnName;
	TextColumn<LiaisonGroup> columnStatus;
	
	
	@Override
	public void createTable()
	{
		addColumnID();
		addColumnName();
		addColumnStatus();
	}

	public void addColumnID()
	{
		columnID=new TextColumn<LiaisonGroup>() {
			
			@Override
			public String getValue(LiaisonGroup object) {
				// TODO Auto-generated method stub
				return object.getCount()+"";
			}
		};table.addColumn(columnID,"ID");
	}
	
	public void addColumnName()
	{
		columnName=new TextColumn<LiaisonGroup>() {
			
			@Override
			public String getValue(LiaisonGroup object) {
				// TODO Auto-generated method stub
				return object.getGroupName();
			}
		};table.addColumn(columnName,"Group Name");
	}
	
	public void addColumnStatus()
	{
		columnStatus=new TextColumn<LiaisonGroup>() {
			
			@Override
			public String getValue(LiaisonGroup object) {
				// TODO Auto-generated method stub
				if(object.getStatus()==true)
					return "Active";
				else 
					return "Inactive";
			}
		};table.addColumn(columnStatus,"Status");
	}
	
}
