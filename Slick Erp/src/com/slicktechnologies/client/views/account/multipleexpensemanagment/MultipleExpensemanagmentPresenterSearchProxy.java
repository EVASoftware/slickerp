package com.slicktechnologies.client.views.account.multipleexpensemanagment;

import java.util.Vector;

import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.config.CategoryTypeFactory;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.account.multipleexpensemanagment.MultipleExpensemanagmentPresenter.MultipleExpensemanagmentPresenterSearch;
import com.slicktechnologies.client.views.composites.EmployeeInfoComposite;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.PettyCash;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Expense;
import com.slicktechnologies.shared.common.businessprocesslayer.MultipleExpenseMngt;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.personlayer.Employee;


public class MultipleExpensemanagmentPresenterSearchProxy extends MultipleExpensemanagmentPresenterSearch{

	public ObjectListBox<Branch> olbBranch;
	public DateComparator dateComparator;
	public ListBox lbStatus;
	public static CheckBox cbexptypepettycash;
	public ObjectListBox<PettyCash> olbpetycash;
	
	public ObjectListBox<Employee> olbApproverName;
	IntegerBox tbexpenseId,tbreferenceid,tbsalesorderId;
	
	ObjectListBox<Config> olbExpenseGroup;
	ObjectListBox<Type>olbType;
	ObjectListBox<ConfigCategory>olbCategory;
	

	public DateComparator expenseDateComparator;
	ObjectListBox<Employee> olbEmployee;
	
	public PersonInfoComposite personInfo;

	
	
	public MultipleExpensemanagmentPresenterSearchProxy()
	{
		super();
		createGui();
	}
	
	public void initWidget()
	{
		olbBranch= new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbBranch);
		lbStatus= new ListBox();
		AppUtility.setStatusListBox(lbStatus,Expense.getStatusList());
		cbexptypepettycash=new CheckBox();
		olbpetycash=new ObjectListBox<PettyCash>();
		olbpetycash.MakeLive(new MyQuerry(new Vector<Filter>(),new PettyCash()));
		
		tbexpenseId=new IntegerBox();
		
		olbApproverName=new ObjectListBox<Employee>();
		AppUtility.makeApproverListBoxLive(olbApproverName, "Expense Management");
		
		olbExpenseGroup=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbExpenseGroup,Screen.EXPENSEGROUP);
		
		olbType=new ObjectListBox<Type>();
		AppUtility.makeTypeListBoxLive(this.olbType, Screen.EXPENSETYPE);
		
		olbCategory=new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(olbCategory, Screen.EXPENSECATEGORY);
		
		dateComparator= new DateComparator("creationDate",new MultipleExpenseMngt());
		
		expenseDateComparator= new DateComparator("expenseList.expenseDate",new MultipleExpenseMngt());
		olbEmployee=new ObjectListBox<Employee>();
		AppUtility.makeSalesPersonListBoxLive(olbEmployee);
		
		
		tbreferenceid = new IntegerBox();
		tbsalesorderId = new IntegerBox();

		 MyQuerry querry=new MyQuerry();
		 querry.setQuerryObject(new Customer());
		 personInfo=new PersonInfoComposite(querry,false);

	}
	public void createScreen()
	{
		initWidget();
		FormFieldBuilder builder;
		builder = new FormFieldBuilder("Branch",olbBranch);
		FormField folbBranch= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("From Date (Creation Date)",dateComparator.getFromDate());
		FormField ffromdateComparator= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("To Date(Creation Date)",dateComparator.getToDate());
		FormField ftodateComparator= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Expense Category",olbCategory);
		FormField folbExpenseCategory= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Expense Status",lbStatus);
		FormField flbststus= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Is expense type a pettycash",cbexptypepettycash);
		FormField fcbexptypepettycash= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Petty Cash Name",olbpetycash);
		FormField folbpettycashname= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		  
		builder = new FormFieldBuilder("* Approver Name",olbApproverName);
		FormField folbApproverName= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	
		builder = new FormFieldBuilder("Expense ID",tbexpenseId);
		FormField ftbexpenseId= builder.setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Expense Group",olbExpenseGroup);
		FormField folbExpenseGroup= builder.setMandatory(false).setMandatoryMsg("Group is Mandatory!").setRowSpan(0).setColSpan(0).build();

		builder = new FormFieldBuilder("Expense Type",olbType);
		FormField folbExpenseType= builder.setMandatory(false).setMandatoryMsg("Type is Mandatory!").setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("From Date (Expense Date)",expenseDateComparator.getFromDate());
		FormField fexpenseDateComparatorfrom= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("To Date(Expense Date)",expenseDateComparator.getToDate());
		FormField expenseDateComparatorto= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Employee",olbEmployee);
		FormField folbEmployee= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Reference Number",tbreferenceid);
		FormField ftbreferenceid= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Sales Order Id",tbsalesorderId);
		FormField ftbsalesorderId= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("",personInfo);
		FormField fpersonInfo= builder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		
		this.fields=new FormField[][]{
				
			{ftbexpenseId,fcbexptypepettycash,folbpettycashname,flbststus},
			{ffromdateComparator,ftodateComparator,folbBranch,folbApproverName},
			{folbExpenseGroup,folbExpenseCategory,folbExpenseType,folbEmployee},
			{fexpenseDateComparatorfrom,expenseDateComparatorto,ftbreferenceid,ftbsalesorderId},
			{fpersonInfo}
		};
	}
	public MyQuerry getQuerry()
	{
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		if(dateComparator.getValue()!=null)
		{
			filtervec.addAll(dateComparator.getValue());
		}
		if(expenseDateComparator.getValue()!=null)
		{
			filtervec.addAll(expenseDateComparator.getValue());
		}
		if(olbBranch.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(olbBranch.getValue().trim());
			temp.setQuerryString("branch");
			filtervec.add(temp);
		}
		
		if(olbApproverName.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(olbApproverName.getValue().trim());
			temp.setQuerryString("approverName");
			filtervec.add(temp);
		}
		
		
		if(tbexpenseId.getValue()!=null ){
			temp=new Filter();
			temp.setIntValue(tbexpenseId.getValue());
			temp.setQuerryString("count");
			filtervec.add(temp);
		}
		
		
		if(cbexptypepettycash.getValue()==true)
		{
			temp=new Filter();
			temp.setBooleanvalue(cbexptypepettycash.getValue());
			temp.setQuerryString("expTypeAsPettyCash");
			filtervec.add(temp);
		}
		if(olbpetycash.getSelectedIndex()!=0)
		{
			temp=new Filter();temp.setStringValue(olbpetycash.getValue().trim());
			  temp.setQuerryString("pettyCashName");
			  filtervec.add(temp);
		}
		
		if(lbStatus.getSelectedIndex()!=0){
			temp=new Filter();
			int selValue=lbStatus.getSelectedIndex();
			String selectedVal=lbStatus.getItemText(selValue);
			temp.setStringValue(selectedVal);
			filtervec.add(temp);
			temp.setQuerryString("status");
			filtervec.add(temp);
		}
		
		if(olbExpenseGroup.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(olbExpenseGroup.getValue().trim());
			temp.setQuerryString("expenseList.expenseGroup");
			filtervec.add(temp);
		}
		
		if(olbCategory.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(olbCategory.getValue().trim());
			temp.setQuerryString("expenseList.expenseCategory");
			filtervec.add(temp);
		}
		
		if(olbType.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(olbType.getValue().trim());
			temp.setQuerryString("expenseList.expenseType");
			filtervec.add(temp);
		}
		if(olbEmployee.getSelectedIndex()!=0)
		{
			temp=new Filter();temp.setStringValue(olbEmployee.getValue().trim());
			  temp.setQuerryString("employee");
			  filtervec.add(temp);
		}
		
		if(tbreferenceid.getValue()!=null && !tbreferenceid.getValue().equals("") ){
			temp=new Filter();
			temp.setIntValue(tbreferenceid.getValue());
			temp.setQuerryString("referenceNumber");
			filtervec.add(temp);
		}
		if(tbsalesorderId.getValue()!=null && !tbsalesorderId.getValue().equals("") ){
			temp=new Filter();
			temp.setIntValue(tbsalesorderId.getValue());
			temp.setQuerryString("salesOrderId");
			filtervec.add(temp);
		}
		
			if(personInfo.getIdValue()!=-1){
			  temp=new Filter();
			  temp.setIntValue(personInfo.getIdValue());
			  temp.setQuerryString("personInfo.count");
			  filtervec.add(temp);
			}
		  
		  if(!(personInfo.getFullNameValue().equals(""))) {
			  temp=new Filter();
			  temp.setStringValue(personInfo.getFullNameValue());
			  temp.setQuerryString("personInfo.fullName");
			  filtervec.add(temp);
		  }
		  if(personInfo.getCellValue()!=-1l){
			  temp=new Filter();
			  temp.setLongValue(personInfo.getCellValue());
			  temp.setQuerryString("personInfo.cellNumber");
			  filtervec.add(temp);
		  }
		
		MyQuerry querry= new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new MultipleExpenseMngt());
		return querry;
	}
	
	
}
