package com.slicktechnologies.client.views.account.multipleexpensemanagment;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.businessprocesslayer.ExpenseReturnHistory;

public class ReturnExpenseAmountHistoryTable extends SuperTable<ExpenseReturnHistory>{

	TextColumn<ExpenseReturnHistory> columnExpenseReturnDate;
	TextColumn<ExpenseReturnHistory> columnExpenseReturnAmt;
	TextColumn<ExpenseReturnHistory> columnExpensePersonResponsible;
	
	
	public ReturnExpenseAmountHistoryTable(){
		super();
	}
	
	
	@Override
	public void createTable() {
		createColumnExpenseReturnDate();
		createColumnExpenseReturnAmount();
		CreateColumnExpenseReturnPersonResponsible();
	}

	private void CreateColumnExpenseReturnPersonResponsible() {
		// TODO Auto-generated method stub
		columnExpensePersonResponsible = new TextColumn<ExpenseReturnHistory>() {
			
			@Override
			public String getValue(ExpenseReturnHistory object) {

				if(object.getPersonResponsible()!=null){
					return object.getPersonResponsible();
				}
				return "";
			}
		};
		table.addColumn(columnExpensePersonResponsible,"Person Responsibe");
		table.setColumnWidth(columnExpensePersonResponsible, 120,Unit.PX);
	}


	private void createColumnExpenseReturnAmount() {

		columnExpenseReturnAmt = new TextColumn<ExpenseReturnHistory>() {
			
			@Override
			public String getValue(ExpenseReturnHistory object) {
				return object.getExpenseReturnAmt()+"";
			}
		};
		table.addColumn(columnExpenseReturnAmt,"Transaction Amount");
		table.setColumnWidth(columnExpenseReturnAmt, 120,Unit.PX);
	}

	private void createColumnExpenseReturnDate() {

		final DateTimeFormat fmt = DateTimeFormat.getFormat("dd/MM/yyyy");
		columnExpenseReturnDate = new TextColumn<ExpenseReturnHistory>() {
			
			@Override
			public String getValue(ExpenseReturnHistory object) {
				if(object.getExpenseReturnDate()!=null){
					return AppUtility.parseDate(object.getExpenseReturnDate());
//					return fmt.parse(fmt.format(object.getServiceDate()));
				}
				return "";
			}
		};
		table.addColumn(columnExpenseReturnDate,"Transaction Date");
		table.setColumnWidth(columnExpenseReturnDate, 100,Unit.PX);
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
