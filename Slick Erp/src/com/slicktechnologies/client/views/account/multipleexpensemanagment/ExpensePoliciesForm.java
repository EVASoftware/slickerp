package com.slicktechnologies.client.views.account.multipleexpensemanagment;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.businessprocesslayer.ExpensePolicies;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class ExpensePoliciesForm extends FormTableScreen<ExpensePolicies> implements ClickHandler{
	
	TextBox tbExpPolicyId;
	ObjectListBox<Config> oblClassificationOfCity;
	ObjectListBox<Config> oblExpGrp;
	ObjectListBox<ConfigCategory> oblExpCategory;
	ObjectListBox<Config> oblEmpRole;
	
	DoubleBox dbExpLimit;
	CheckBox cbAtActual;
	CheckBox cbPerKm;
	DoubleBox dbRatePerKm;
	
	CheckBox cbStatus;
	
	
	public ExpensePoliciesForm(SuperTable<ExpensePolicies> table, int mode,boolean captionmode) {
		super(table, mode, captionmode);
		createGui();
		cbStatus.setValue(true);
		cbAtActual.setValue(false);
		cbPerKm.setValue(false);
		dbRatePerKm.setEnabled(false);
	}
	
	private void initalizeWidget() {

		tbExpPolicyId = new TextBox();
		tbExpPolicyId.setEnabled(false);
		
		
		cbStatus = new CheckBox();
		cbStatus = new CheckBox();
		cbStatus.setValue(true);
		
		oblClassificationOfCity=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(oblClassificationOfCity,Screen.CLASSIFICATIONOFCITY);
		
		oblExpGrp=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(oblExpGrp,Screen.EXPENSEGROUP);
		
		oblEmpRole=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(oblEmpRole,Screen.EMPLOYEEROLE);
		
		oblExpCategory=new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(oblExpCategory, Screen.EXPENSECATEGORY);
		
		dbExpLimit=new DoubleBox();
		cbAtActual=new CheckBox();
		cbAtActual.setValue(false);
		
		
		cbPerKm=new CheckBox();
		cbPerKm.setValue(false);
		cbPerKm.addClickHandler(this);
		dbRatePerKm=new DoubleBox();
		dbRatePerKm.setEnabled(false);
		
		
	}

	
	

	@Override
	public void createScreen() {
		initalizeWidget();

		this.processlevelBarNames = new String[] { "New" };
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingPLInformation = fbuilder.setlabel("Expense Policies Details")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Expense Policy ID", tbExpPolicyId);
		FormField ftbExpPolicyId = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Class", oblClassificationOfCity);
		FormField foblClassificationOfCity = fbuilder.setMandatory(true)
				.setMandatoryMsg("Class is mandatory !").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Employee Role", oblEmpRole);
		FormField foblEmpRole = fbuilder.setMandatory(true).setMandatoryMsg("Employee Role is mandatory !").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Expense Group", oblExpGrp);
		FormField foblExpGrp = fbuilder.setMandatory(true).setMandatoryMsg("Expense Group is mandatory !").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Expense Category", oblExpCategory);
		FormField foblExpCategory = fbuilder.setMandatory(true).setMandatoryMsg("Expense Category is mandatory !").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Expense Limit", dbExpLimit);
		FormField fdbExpLimit = fbuilder.setMandatory(false).setMandatoryMsg("Expense Limit is mandatory !").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("At Actual", cbAtActual);
		FormField fcbAtActual = fbuilder.setMandatory(false).setMandatoryMsg("Expense Limit is mandatory !").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Per KM", cbPerKm);
		FormField fcbPerKm = fbuilder.setMandatory(false).setMandatoryMsg("Expense Limit is mandatory !").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Rate/KM", dbRatePerKm);
		FormField fdbRatePerKm = fbuilder.setMandatory(false).setMandatoryMsg("Expense Limit is mandatory !").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Status", cbStatus);
		FormField fcbStatus = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		FormField[][] formfield = {
				{ fgroupingPLInformation },
				{ ftbExpPolicyId, foblEmpRole,foblClassificationOfCity,fcbStatus },
				{ foblExpGrp,foblExpCategory,fdbExpLimit,fcbAtActual},
				{ fcbPerKm,fdbRatePerKm},
				
				
				
		};
		this.fields = formfield;
	}

	@Override
	public void updateModel(ExpensePolicies model) {
		if(oblEmpRole.getValue()!=null){
			model.setEmployeeRole(oblEmpRole.getValue());
		}
		if(oblClassificationOfCity.getValue()!=null){
			model.setCityClass(oblClassificationOfCity.getValue());
		}
		if(oblExpGrp.getValue()!=null){
			model.setExpenseGrp(oblExpGrp.getValue());
		}
		if(oblExpCategory.getValue()!=null){
			model.setExpenseCategory(oblExpCategory.getValue());
		}
		
		if(dbExpLimit.getValue()!=null){
			model.setExpenseLimit(dbExpLimit.getValue());
		}else{
			model.setExpenseLimit(0d);
		}
		model.setAtActual(cbAtActual.getValue());
		model.setPerKm(cbPerKm.getValue());
		if(dbRatePerKm.getValue()!=null){
			model.setRatePerKm(dbRatePerKm.getValue());
		}else{
			model.setRatePerKm(0d);
		}
		model.setStatus(cbStatus.getValue());
		presenter.setModel(model);
		
	}

	@Override
	public void updateView(ExpensePolicies view) {
		tbExpPolicyId.setValue(view.getCount()+"");
		if(view.getCityClass()!=null){
			oblClassificationOfCity.setValue(view.getCityClass());
		}
		if(view.getEmployeeRole()!=null){
			oblEmpRole.setValue(view.getEmployeeRole());
		}
		if(view.getExpenseGrp()!=null){
			oblExpGrp.setValue(view.getExpenseGrp());
		}
		if(view.getExpenseCategory()!=null){
			oblExpCategory.setValue(view.getExpenseCategory());
		}
		
		if(view.getExpenseLimit()!=0){
			dbExpLimit.setValue(view.getExpenseLimit());
		}
		cbAtActual.setValue(view.isAtActual());
		cbPerKm.setValue(view.isPerKm());
		if(view.getRatePerKm()!=0){
			dbRatePerKm.setValue(view.getRatePerKm());
		}
		cbStatus.setValue(view.isStatus());
		
		presenter.setModel(view);
		
	}
	
	
	@Override
	public void toggleAppHeaderBarMenu() {
		if (AppMemory.getAppMemory().currentState == ScreeenState.NEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Save") || text.equals("Discard")) {
					menus[k].setVisible(true);
				} else{
					menus[k].setVisible(false);
				}

			}
		}

		else if (AppMemory.getAppMemory().currentState == ScreeenState.EDIT) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Save") || text.equals("Discard")){
					menus[k].setVisible(true);
				}
				else{
					menus[k].setVisible(false);
				}
			}
		}

		else if (AppMemory.getAppMemory().currentState == ScreeenState.VIEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Discard") || text.equals("Edit")){
					menus[k].setVisible(true);
				}
				else{
					menus[k].setVisible(false);
				}
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.EXPENSEPOLICIES,LoginPresenter.currentModule.trim());
	}

	@Override
	public void setCount(int count) {
		this.tbExpPolicyId.setValue(presenter.getModel().getCount() + "");
	}

	@Override
	public void setToEditState() {
		super.setToEditState();
		this.processLevelBar.setVisibleFalse(false);
	}
	@Override
	public void clear() {
		super.clear();
		cbStatus.setValue(true);
		tbExpPolicyId.setEnabled(false);
		cbAtActual.setValue(false);
		cbPerKm.setValue(false);
		dbRatePerKm.setEnabled(false);
	}

	@Override
	public boolean validate() {
		boolean superRes = super.validate();

		if (superRes == false){
			return false;
		}
		return true;
	}

	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		tbExpPolicyId.setEnabled(false);
		
		if(cbPerKm.getValue()==false){
			dbRatePerKm.setEnabled(false);
		}
	}

	@Override
	public void setToNewState() {
		super.setToNewState();
	}

	@Override
	public void setToViewState() {
		super.setToViewState();
	}

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		if(event.getSource()==cbPerKm){
			if(cbPerKm.getValue()==true){
				dbRatePerKm.setEnabled(true);
				dbExpLimit.setValue(null);
				dbExpLimit.setEnabled(false);
				cbAtActual.setValue(false);
				cbAtActual.setEnabled(false);
			}else{
				dbRatePerKm.setEnabled(false);
				dbExpLimit.setEnabled(true);
				cbAtActual.setEnabled(true);
			}
		}
	}

	
	
	
}
