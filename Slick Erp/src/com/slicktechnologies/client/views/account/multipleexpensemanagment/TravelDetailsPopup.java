package com.slicktechnologies.client.views.account.multipleexpensemanagment;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.shared.common.helperlayer.City;
import com.slicktechnologies.shared.common.helperlayer.Config;

public class TravelDetailsPopup extends PopupScreen implements ValueChangeHandler<String>{

	
	DateBox dbFromDate;
	DateBox dbToDate;
	
	ObjectListBox<City> oblFromCity;
	ObjectListBox<City> oblToCity;
	
	
	DoubleBox dbKmTravelled;
	
	
	
	public TravelDetailsPopup(){
		super();
		createGui();
	}
	
	private void initializeWidgets() {
		dbFromDate=new DateBoxWithYearSelector();
		dbToDate=new DateBoxWithYearSelector();
		
		oblFromCity=new ObjectListBox<City>();
		City.makeOjbectListBoxLive(oblFromCity);
		
		oblToCity=new ObjectListBox<City>();
		City.makeOjbectListBoxLive(oblToCity);
		
		dbKmTravelled=new DoubleBox();
		
		
	}
	
	
	@Override
	public void createScreen() {
		// TODO Auto-generated method stub
		initializeWidgets();
		

		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fcustomerGroping = fbuilder.setlabel("Travel Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(3).build();
		
		
		fbuilder = new FormFieldBuilder("Departure Date",dbFromDate);
		FormField fdbFromDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Arrival Date",dbToDate);
		FormField fdbToDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Departure City",oblFromCity);
		FormField foblFromCity= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Arrival City",oblToCity);
		FormField foblToCity= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Km Travelled",dbKmTravelled);
		FormField fdbKmTravelled= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	
		
		FormField formfied [][] = {
				{fcustomerGroping},
				{fdbKmTravelled,fdbFromDate,foblFromCity},
				{fdbToDate,foblToCity},
				
				
		};
		this.fields = formfied;
		
	}

	
	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
		// TODO Auto-generated method stub
		
	}

	public DateBox getDbFromDate() {
		return dbFromDate;
	}

	public void setDbFromDate(DateBox dbFromDate) {
		this.dbFromDate = dbFromDate;
	}

	public DateBox getDbToDate() {
		return dbToDate;
	}

	public void setDbToDate(DateBox dbToDate) {
		this.dbToDate = dbToDate;
	}

	public ObjectListBox<City> getOblFromCity() {
		return oblFromCity;
	}

	public void setOblFromCity(ObjectListBox<City> oblFromCity) {
		this.oblFromCity = oblFromCity;
	}

	public ObjectListBox<City> getOblToCity() {
		return oblToCity;
	}

	public void setOblToCity(ObjectListBox<City> oblToCity) {
		this.oblToCity = oblToCity;
	}

	public DoubleBox getDbKmTravelled() {
		return dbKmTravelled;
	}

	public void setDbKmTravelled(DoubleBox double1) {
		this.dbKmTravelled = double1;
	}
	
	

	
}
