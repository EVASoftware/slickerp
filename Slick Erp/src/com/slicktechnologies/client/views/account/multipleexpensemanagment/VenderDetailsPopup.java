package com.slicktechnologies.client.views.account.multipleexpensemanagment;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.composite.AddressComposite;
import com.simplesoftwares.client.library.mywidgets.EmailTextBox;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;

public class VenderDetailsPopup extends PopupScreen implements ValueChangeHandler<String>{

	
	TextBox tbInvoiceNo,tbVendor;
	
	
	
	
	
	public VenderDetailsPopup(){
		super();
		createGui();
	}
	
	
	private void initializeWidgets() {
		tbInvoiceNo=new TextBox();
		tbVendor=new TextBox();
		
		
	}
	@Override
	public void createScreen() {
		// TODO Auto-generated method stub
		initializeWidgets();
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fcustomerGroping = fbuilder.setlabel("Vendor Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("Invoice No",tbInvoiceNo);
		FormField ftbInvoiceNo= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Vendor",tbVendor);
		FormField ftbVendor= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		FormField formfied [][] = {
				{fcustomerGroping},
				{ftbInvoiceNo,ftbVendor},
				
		};
		this.fields = formfied;
	}
	
	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
		// TODO Auto-generated method stub
		
	}


	public TextBox getTbInvoiceNo() {
		return tbInvoiceNo;
	}


	public void setTbInvoiceNo(TextBox tbInvoiceNo) {
		this.tbInvoiceNo = tbInvoiceNo;
	}


	public TextBox getTbVendor() {
		return tbVendor;
	}


	public void setTbVendor(TextBox tbVendor) {
		this.tbVendor = tbVendor;
	}
	
	

}
