package com.slicktechnologies.client.views.account.multipleexpensemanagment;

import com.google.gwt.dom.client.Style.BorderStyle;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;

public class ExpenseReturnAmountHistoryPopup extends PopupScreen {

	 ReturnExpenseAmountHistoryTable historyTable;
	
	public ExpenseReturnAmountHistoryPopup(){
		super();
		createGui();
		/**
		 * @author Anil
		 * @since 05-01-2021
		 * to increase the width
		 * raised by Vaishnavi
		 */
		content.getElement().getStyle().setWidth(510, Unit.PX);
//		popup.setWidth("505px");
	}
	

	private void initilizewidget() {

		historyTable = new ReturnExpenseAmountHistoryTable();
	}
	
	@Override
	public void createScreen() {

		initilizewidget();
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingExpenseDetails=fbuilder.setlabel("Return Expense Amount History").widgetType(FieldType.GREYGROUPING).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",historyTable.getTable());
		FormField fdbexpenseReturnAmt= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		
		FormField[][] formfield = {
				{fgroupingExpenseDetails},
				{fdbexpenseReturnAmt}
		};


		this.fields=formfield;		
	}
	
	


	@Override
	public void onClick(ClickEvent event) {

		
	}


	public ReturnExpenseAmountHistoryTable getHistoryTable() {
		return historyTable;
	}


	public void setHistoryTable(ReturnExpenseAmountHistoryTable historyTable) {
		this.historyTable = historyTable;
	}


	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
//		super.applyStyle();
//		content.getElement().setId("popupformcontent");
		content.getElement().getStyle().setWidth(510, Unit.PX);
		content.getElement().getStyle().setBorderWidth(1, Unit.PX);
		content.getElement().getStyle().setBorderStyle(BorderStyle.SOLID);
		form.getElement().setId("form");  //form   popupform
		horizontal.getElement().addClassName("popupcentering");
		lblOk.getElement().setId("addbutton");
		lblCancel.getElement().setId("addbutton");
		
		popup.center();
	}
	
	
	

}
