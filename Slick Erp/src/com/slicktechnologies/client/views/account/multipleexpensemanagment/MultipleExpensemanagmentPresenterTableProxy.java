package com.slicktechnologies.client.views.account.multipleexpensemanagment;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.account.multipleexpensemanagment.MultipleExpensemanagmentPresenter.MultipleExpensemanagmentPresenterTable;
import com.slicktechnologies.shared.common.businessprocesslayer.MultipleExpenseMngt;

public class MultipleExpensemanagmentPresenterTableProxy extends MultipleExpensemanagmentPresenterTable{

	TextColumn<MultipleExpenseMngt> getCountColumn;
	TextColumn<MultipleExpenseMngt> getBranchColumn;
	TextColumn<MultipleExpenseMngt> getStatusColumn;
	TextColumn<MultipleExpenseMngt> getPettyCashName ;
	TextColumn<MultipleExpenseMngt> getApprovalName ;
	
	TextColumn<MultipleExpenseMngt> getExpGroup;
	TextColumn<MultipleExpenseMngt> getExpCategory;
	TextColumn<MultipleExpenseMngt> getExpType;
	TextColumn<MultipleExpenseMngt> getExpDate;
	TextColumn<MultipleExpenseMngt> getExpCreationDate;
	
	TextColumn<MultipleExpenseMngt> getReferenceIdColumn;

	public MultipleExpensemanagmentPresenterTableProxy()
	{
		super();
	}
	
	@Override 
	public void createTable() {
		addColumngetCount();
		addColumngetPettyCashName();
		addColumngetApprovalName();
		
		try{
		getExpGroup();
		getExpCategory();
		getExpType();
		
		getExpDate();
		getExpCreationDate();
		}catch(Exception e){
			
		}
		
		addColumngetBranch();
		addColumngetStatus();
		
		addColumnReferenceId();
		
	}
	
	private void addColumnReferenceId() {

		getReferenceIdColumn = new TextColumn<MultipleExpenseMngt>() {
			
			@Override
			public String getValue(MultipleExpenseMngt object) {
				if(object.getReferenceNumber()!=0){
					return object.getReferenceNumber()+"";
				}
				else{
					return "";
				}
			}
		};
		table.addColumn(getReferenceIdColumn, "Reference Number");
		table.setColumnWidth(getReferenceIdColumn, 100, Unit.PX);
		getReferenceIdColumn.setSortable(true);
	}

	private void getExpGroup() {
		getExpGroup = new TextColumn<MultipleExpenseMngt>() {
			@Override
			public String getValue(MultipleExpenseMngt object) {
				
				return  getExpNameFromList(object.getExpenseList(),"Group");
			}
		};
		table.addColumn(getExpGroup, "Expense Group");
		table.setColumnWidth(getExpGroup, 100, Unit.PX);
		getExpGroup.setSortable(true);
		
	}

	private void getExpCategory() {
		getExpCategory = new TextColumn<MultipleExpenseMngt>() {
			@Override
			public String getValue(MultipleExpenseMngt object) {
				
				return getExpNameFromList(object.getExpenseList(),"Category");
			}
		};
		table.addColumn(getExpCategory, "Expense Category");
		table.setColumnWidth(getExpCategory, 100, Unit.PX);
		getExpCategory.setSortable(true);
		
	}

	private void getExpType() {
		getExpType = new TextColumn<MultipleExpenseMngt>() {
			@Override
			public String getValue(MultipleExpenseMngt object) {
				
				return getExpNameFromList(object.getExpenseList(),"Type");
			}
		};
		table.addColumn(getExpType, "Expense Type");
		table.setColumnWidth(getExpType, 100, Unit.PX);
		getExpType.setSortable(true);
		
	}

	private void getExpDate() {
		getExpDate = new TextColumn<MultipleExpenseMngt>() {
			@Override
			public String getValue(MultipleExpenseMngt object) {
				
				return getExpNameFromList(object.getExpenseList(),"Expense Date");
			}
		};
		table.addColumn(getExpDate, "Expense Date");
		table.setColumnWidth(getExpDate, 100, Unit.PX);
		getExpDate.setSortable(true);
		
	}

	private void getExpCreationDate() {
		getExpCreationDate = new TextColumn<MultipleExpenseMngt>() {
			@Override
			public String getValue(MultipleExpenseMngt object) {
				if(object.getCreationDate()!=null){
				return AppUtility.parseDate(object.getCreationDate());
				}else{
					return "";
				}
			}
		};
		table.addColumn(getExpCreationDate, "Creation Date");
		table.setColumnWidth(getExpCreationDate, 100, Unit.PX);
		getExpCreationDate.setSortable(true);
		
	}
	
	public String getExpNameFromList(ArrayList<ExpenseManagement> expList, String type){
		if(expList==null){
			return "";
		}
		String name="";
		switch(type){
			
			case "Expense Date":
				for(ExpenseManagement exp:expList){
					if(exp.getExpenseDate()!=null){
					name=name+AppUtility.parseDate(exp.getExpenseDate())+",";
					}
				}
				break;
			case "Group":
				for(ExpenseManagement exp:expList){
					name=name+exp.getExpenseGroup()+",";
				}
				break;
			case "Category":
				for(ExpenseManagement exp:expList){
					name=name+exp.getExpenseCategory()+",";
				}
				break;
			case "Type":
				for(ExpenseManagement exp:expList){
					name=name+exp.getExpenseType()+",";
				}
				break;
			default:
				break;
		}
		try{
			if(!name.equals(""))
				name=name.substring(0,name.length()-1);
		}catch(Exception e){
			
		}
		return name;
		
	}

	private void addColumngetApprovalName() {
		
		getApprovalName=new TextColumn<MultipleExpenseMngt>()
				{
			@Override
			public String getValue(MultipleExpenseMngt object)
			{
				return object.getApproverName();
			}
				};
				table.addColumn(getApprovalName,"Approver Name");
				table.setColumnWidth(getApprovalName,100,Unit.PX);
				getApprovalName.setSortable(true);
	}

	protected void addColumngetCount()
	{
		getCountColumn=new TextColumn<MultipleExpenseMngt>()
				{
			@Override
			public String getValue(MultipleExpenseMngt object)
			{
				if( object.getCount()==-1)
					return "N.A";
				else return object.getCount()+"";
			}
				};
				table.addColumn(getCountColumn,"ID");
				table.setColumnWidth(getCountColumn,100,Unit.PX);
				getCountColumn.setSortable(true);
	}
	
	
	private void addColumngetPettyCashName() {
		getPettyCashName=new TextColumn<MultipleExpenseMngt>()
				{
			@Override
			public String getValue(MultipleExpenseMngt object)
			{
				if(object.getPettyCashName()!=null)
				{
				return object.getPettyCashName();
				}
				else
				{
					return "NA";
				}
			}
				};
				table.addColumn(getPettyCashName,"Pettycash Name");
				table.setColumnWidth(getPettyCashName,120,Unit.PX);
				getPettyCashName.setSortable(true);
	}
	
	
	protected void addColumngetBranch()
	{
		getBranchColumn=new TextColumn<MultipleExpenseMngt>()
				{
			@Override
			public String getValue(MultipleExpenseMngt object)
			{
				return object.getBranch();
			}
				};
				table.addColumn(getBranchColumn,"Branch");
				table.setColumnWidth(getBranchColumn,100,Unit.PX);
				getBranchColumn.setSortable(true);
	}
	
	protected void addColumngetStatus()
	{
		getStatusColumn=new TextColumn<MultipleExpenseMngt>()
				{
			@Override
			public String getValue(MultipleExpenseMngt object)
			{
				return object.getStatus()+"";
			}
				};
				table.addColumn(getStatusColumn,"Status");
				table.setColumnWidth(getStatusColumn,100,Unit.PX);
				getStatusColumn.setSortable(true);
	}
	
	public void addColumnSorting(){
		addSortinggetCount();
		addSortinggetApprovalName();
		addSortinggetBranch();
		addSortinggetStatus();
		addSortinggetPettyCashName();
		
		addSortingToGroup();
		addSortingToCategory();
		addSortingToType();
		addSortingToExpDate();
		addSortingToCreationDate();
		
		addSortinggetReferenceNumber();

	}
	
	private void addSortinggetReferenceNumber() {

		List<MultipleExpenseMngt> list=getDataprovider().getList();
		columnSort=new ListHandler<MultipleExpenseMngt>(list);
		columnSort.setComparator(getCountColumn, new Comparator<MultipleExpenseMngt>()
				{
			@Override
			public int compare(MultipleExpenseMngt e1,MultipleExpenseMngt e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getReferenceNumber()== e2.getReferenceNumber()){
						return 0;}
					if(e1.getReferenceNumber()> e2.getReferenceNumber()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	
	}

	private void addSortingToGroup() {
		List<MultipleExpenseMngt> list = getDataprovider().getList();
		columnSort = new ListHandler<MultipleExpenseMngt>(list);
		columnSort.setComparator(getExpGroup,new Comparator<MultipleExpenseMngt>() {
			@Override
			public int compare(MultipleExpenseMngt e1,MultipleExpenseMngt e2) {
				if (e1 != null && e2 != null) {
					if (e1.getExpenseList() != null&& e2.getExpenseList() != null) {
						return getExpNameFromList(e1.getExpenseList(),"Group").compareTo(getExpNameFromList(e2.getExpenseList(),"Group"));
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}

	private void addSortingToCategory() {
		List<MultipleExpenseMngt> list = getDataprovider().getList();
		columnSort = new ListHandler<MultipleExpenseMngt>(list);
		columnSort.setComparator(getExpCategory,new Comparator<MultipleExpenseMngt>() {
			@Override
			public int compare(MultipleExpenseMngt e1,MultipleExpenseMngt e2) {
				if (e1 != null && e2 != null) {
					if (e1.getExpenseList() != null&& e2.getExpenseList() != null) {
						return getExpNameFromList(e1.getExpenseList(),"Category").compareTo(getExpNameFromList(e2.getExpenseList(),"Category"));
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}

	private void addSortingToType() {
		List<MultipleExpenseMngt> list = getDataprovider().getList();
		columnSort = new ListHandler<MultipleExpenseMngt>(list);
		columnSort.setComparator(getExpType,new Comparator<MultipleExpenseMngt>() {
			@Override
			public int compare(MultipleExpenseMngt e1,MultipleExpenseMngt e2) {
				if (e1 != null && e2 != null) {
					if (e1.getExpenseList() != null&& e2.getExpenseList() != null) {
						return getExpNameFromList(e1.getExpenseList(),"Type").compareTo(getExpNameFromList(e2.getExpenseList(),"Type"));
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}

	private void addSortingToExpDate() {
		List<MultipleExpenseMngt> list = getDataprovider().getList();
		columnSort = new ListHandler<MultipleExpenseMngt>(list);
		columnSort.setComparator(getExpDate,new Comparator<MultipleExpenseMngt>() {
			@Override
			public int compare(MultipleExpenseMngt e1,MultipleExpenseMngt e2) {
				if (e1 != null && e2 != null) {
					if (e1.getExpenseList() != null&& e2.getExpenseList() != null) {
						return getExpNameFromList(e1.getExpenseList(),"Expense Date").compareTo(getExpNameFromList(e2.getExpenseList(),"Expense Date"));
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}

	private void addSortingToCreationDate() {
		List<MultipleExpenseMngt> list = getDataprovider().getList();
		columnSort = new ListHandler<MultipleExpenseMngt>(list);
		columnSort.setComparator(getExpCreationDate,new Comparator<MultipleExpenseMngt>() {
			@Override
			public int compare(MultipleExpenseMngt e1,MultipleExpenseMngt e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCreationDate() != null&& e2.getCreationDate() != null) {
						return e1.getCreationDate().compareTo(e2.getCreationDate());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}

	private void addSortinggetApprovalName() {
		List<MultipleExpenseMngt> list=getDataprovider().getList();
		columnSort=new ListHandler<MultipleExpenseMngt>(list);
		columnSort.setComparator(getApprovalName, new Comparator<MultipleExpenseMngt>()
				{
			@Override
			public int compare(MultipleExpenseMngt e1,MultipleExpenseMngt e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getApproverName()!=null && e2.getApproverName()!=null){
						return e1.getApproverName().compareTo(e2.getApproverName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addSortinggetCount()
	{
		List<MultipleExpenseMngt> list=getDataprovider().getList();
		columnSort=new ListHandler<MultipleExpenseMngt>(list);
		columnSort.setComparator(getCountColumn, new Comparator<MultipleExpenseMngt>()
				{
			@Override
			public int compare(MultipleExpenseMngt e1,MultipleExpenseMngt e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	
	protected void addSortinggetBranch()
	{
		List<MultipleExpenseMngt> list=getDataprovider().getList();
		columnSort=new ListHandler<MultipleExpenseMngt>(list);
		columnSort.setComparator(getBranchColumn, new Comparator<MultipleExpenseMngt>()
				{
			@Override
			public int compare(MultipleExpenseMngt e1,MultipleExpenseMngt e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBranch()!=null && e2.getBranch()!=null){
						return e1.getBranch().compareTo(e2.getBranch());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	
	protected void addSortinggetStatus()
	{
		List<MultipleExpenseMngt> list=getDataprovider().getList();
		columnSort=new ListHandler<MultipleExpenseMngt>(list);
		columnSort.setComparator(getStatusColumn, new Comparator<MultipleExpenseMngt>()
				{
			@Override
			public int compare(MultipleExpenseMngt e1,MultipleExpenseMngt e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getStatus()!=null && e2.getStatus()!=null){
						return e1.getStatus().compareTo(e2.getStatus());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	private void addSortinggetPettyCashName() {
		
		List<MultipleExpenseMngt> list=getDataprovider().getList();
		columnSort=new ListHandler<MultipleExpenseMngt>(list);
		columnSort.setComparator(getPettyCashName, new Comparator<MultipleExpenseMngt>()
				{
			@Override
			public int compare(MultipleExpenseMngt e1,MultipleExpenseMngt e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getPettyCashName()!=null && e2.getPettyCashName()!=null){
						return e1.getPettyCashName().compareTo(e2.getPettyCashName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
}
