package com.slicktechnologies.client.views.account.multipleexpensemanagment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.google.gwt.user.client.ui.PopupPanel;
import com.itextpdf.text.log.SysoCounter;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.approvalutility.ApprovableFormScreenPresenter;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.services.UpdateService;
import com.slicktechnologies.client.services.UpdateServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.account.expensemanagment.ExpenseManagmentService;
import com.slicktechnologies.client.views.account.expensemanagment.ExpenseManagmentServiceAsync;
import com.slicktechnologies.client.views.account.expensemanagment.ExpensemanagmentForm;
import com.slicktechnologies.client.views.account.expensemanagment.ExpensemanagmentPresenter;
import com.slicktechnologies.client.views.account.expensemanagment.ExpensemanagmentPresenterSearchProxy;
import com.slicktechnologies.client.views.account.expensemanagment.ExpensemanagmentPresenterTableProxy;
import com.slicktechnologies.client.views.account.expensemanagment.PopupForPettyCashName;
import com.slicktechnologies.client.views.account.expensemanagment.ExpensemanagmentPresenter.ExpensemanagmentPresenterSearch;
import com.slicktechnologies.client.views.account.expensemanagment.ExpensemanagmentPresenter.ExpensemanagmentPresenterTable;
import com.slicktechnologies.client.views.documentcancellation.DocumentCancellationPopUp;
import com.slicktechnologies.client.views.purchase.quickpurchase.NewVendorPopUP;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.PettyCash;
import com.slicktechnologies.shared.PettyCash.ApproverAmount;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Expense;
import com.slicktechnologies.shared.common.businessprocesslayer.MultipleExpenseMngt;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class MultipleExpensemanagmentPresenter extends ApprovableFormScreenPresenter<MultipleExpenseMngt> implements RowCountChangeEvent.Handler,ChangeHandler,ValueChangeHandler<Double>{

	MultipleExpensemanagmentForm form;
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	final GenricServiceAsync genasync=GWT.create(GenricService.class);
	PopupForPettyCashName pettycashPopup = new PopupForPettyCashName();
	PopupPanel panel;
	ExpenseManagmentServiceAsync expenseService = GWT.create(ExpenseManagmentService.class); 
	
	//   rohan added this for cancellation expense document Date: 13-04-2017 
		DocumentCancellationPopUp popup = new DocumentCancellationPopUp();
		PopupPanel cancelpanel;
	//  ends here 
		
	// rohan added this code for Cancel Contract on Date : 28/2/2017  
	UpdateServiceAsync update = GWT.create(UpdateService.class);
	
	/** Date 07-08-2017 added by vijay for expense return amt **/
	ReturnExpensePopup returnExepensePopup = new ReturnExpensePopup();
	/** Date 08-08-2017 added by vijay for expense return amt history **/
	ExpenseReturnAmountHistoryPopup returnHistoryPopup = new ExpenseReturnAmountHistoryPopup();
	
	VenderDetailsPopup  vendorPopup = new VenderDetailsPopup();
	
	TravelDetailsPopup travelPopup = new TravelDetailsPopup();
	/**
	 * Instantiates a new expensemanagment presenter.
	 *
	 * @param view the view
	 * @param model the model
	 */
	public MultipleExpensemanagmentPresenter (FormScreen<MultipleExpenseMngt> view,MultipleExpenseMngt model) {
		super(view, model);
		Console.log(" 2A ");
		form=(MultipleExpensemanagmentForm) view;
		Console.log(" 2A1 ");
		form.setPresenter(this);
		Console.log(" 2A2 ");
		
		Console.log(" 2A3 ");
		form.cbexptype.addClickHandler(this);
		Console.log(" 2B ");
		form.expenseMngtTable.getTable().addRowCountChangeHandler(this);
		form.olbpettycashname.addChangeHandler(this);
		form.totalTotalAmt.addValueChangeHandler(this);
		Console.log(" 2C ");
		pettycashPopup.getBtnOk().addClickHandler(this);
		pettycashPopup.getBtnCancel().addClickHandler(this);
		Console.log(" 2D ");
		popup.getBtnOk().addClickHandler(this);
		popup.getBtnCancel().addClickHandler(this);
		Console.log(" 2E ");
		/** Date 07-08-2017 added by vijay for expense return amt **/
		returnExepensePopup.getLblOk().addClickHandler(this);
		returnExepensePopup.getLblCancel().addClickHandler(this);
		Console.log(" 2F ");
		
		/**Added By Priyanka**/
		form.getBtnNewCustomer1().addClickHandler(this);
		vendorPopup.getLblOk().addClickHandler(this);
		vendorPopup.getLblCancel().addClickHandler(this);  //travelPopup
		Console.log(" 2G ");
		form.getBtnNewCustomer2().addClickHandler(this);
		travelPopup.getLblOk().setText("Add details");//Ashwini Patil changed label as "Add Details" since it was going in different condition of on click due to label match
		travelPopup.getLblOk().addClickHandler(this);
		travelPopup.getLblCancel().addClickHandler(this);
		/** End **/
		Console.log(" 2H ");
		
		try{
			form.getSearchpopupscreen().getPopup().setWidth("1500px");
		
			boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.MULTIPLEEXPENSEMANAGMENT,LoginPresenter.currentModule.trim());
			if(isDownload==false){
				form.getSearchpopupscreen().getDwnload().setVisible(false);
			}
		
		}catch(Exception e){
			
		}
		Console.log(" 2I ");
	}

	
	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.EntityPresenter#reactOnPrint()
	 */
	@Override
	public void reactOnPrint() {
		/**
		 * Date : 13-09-2017 By JAYSHREE
		 */
//		final String url1 = GWT.getModuleBaseURL() + "pdfexpense"+"?Id="+model.getId()+ "&" + "type=" + "multiple";
//		Window.open(url1, "test", "enabled");
		
		final String url1 = GWT.getModuleBaseURL() + "pdfexpense"+"?Id="+model.getId()+ "&" + "type=" + "multiple"+"&"+"companyId="+model.getCompanyId();
		Window.open(url1, "test", "enabled");
		
		/**
		 * End
		 */
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.EntityPresenter#reactOnEmail()
	 */
	@Override
	public void reactOnEmail() {
		
		
	}
	
	@Override
	public void reactOnDownload() 
	{
		ArrayList<MultipleExpenseMngt> custarray=new ArrayList<MultipleExpenseMngt>();
		List<MultipleExpenseMngt> list=(List<MultipleExpenseMngt>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
		
		custarray.addAll(list);
		
		csvservice.setMultiplExpenselist(custarray, new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);
				
			}

			@Override
			public void onSuccess(Void result) {
				

				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "csvservlet"+"?type="+120;
				Window.open(url, "test", "enabled");
				
			}
		});

	}
	
	
	
	

	/**
	 * Method template to set new model.
	 */
	@Override
	protected void makeNewModel() {
		
		model=new MultipleExpenseMngt();
	}
	
	/*
	 * Method template to set Myquerry object
	 */
	/**
	 * Gets the expense query.
	 *
	 * @return the expense query
	 */
	public MyQuerry getExpenseQuery()
	{
		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new MultipleExpenseMngt());
		return quer;
	}
	
	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.EntityPresenter#setModel(com.simplesoftwares.client.library.appstructure.SuperModel)
	 */
	public void setModel(MultipleExpenseMngt entity)
	{
		model=entity;
	}
	
	/**
	 * Initalize.
	 * @return 
	 */
	public static MultipleExpensemanagmentForm initalize()
	{
		Console.log("MUL EXP 1");
			MultipleExpensemanagmentPresenterTable gentableScreen=new MultipleExpensemanagmentPresenterTableProxy();
			Console.log("MUL EXP 2");
			MultipleExpensemanagmentForm  form=new  MultipleExpensemanagmentForm();
			gentableScreen.setView(form);
			gentableScreen.applySelectionModle();
			Console.log("MUL EXP 3");
			
//			MultipleExpensemanagmentPresenterTable gentableSearch=new MultipleExpensemanagmentPresenterTableProxy();
//			gentableSearch.setView(form);
//			gentableSearch.applySelectionModle();
			
			MultipleExpensemanagmentPresenterSearch.staticSuperTable=gentableScreen;
			MultipleExpensemanagmentPresenterSearch searchpopup=new MultipleExpensemanagmentPresenterSearchProxy();
			form.setSearchpopupscreen(searchpopup);
			Console.log("MUL EXP 4");
			
			MultipleExpensemanagmentPresenter  presenter=new  MultipleExpensemanagmentPresenter(form,new MultipleExpenseMngt());
			Console.log("MUL EXP 5");
			
			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Accounts/Expense Management",Screen.MULTIPLEEXPENSEMANAGMENT);

			AppMemory.getAppMemory().stickPnel(form);
			return form;
			
			
		 
	}
	
	@Override
	public void reactToProcessBarEvents(ClickEvent e) 
	{
		super.reactToProcessBarEvents(e);
		InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();

		
		if(text.equals("New")) {
			if(form.isPopUpAppMenubar()){
				Console.log("New clicked on expense popup");
				return;				
			}
			reactToNew();
		}
		
		
		if(text.equals("Update Record"))
		{
			panel=new PopupPanel(true);
			panel.add(pettycashPopup);
			panel.setGlassEnabled(true);
			panel.show();
			panel.center();
		}
		
		if(text.equals("Migrate Record"))
		{
			migrateDataFromExpenseMngt();
		}
		
		if(text.equals("Cancel Expense"))
		{
			reactOnCancelExpense();
		}
		
		if(text.equals("Update Customer"))
		{
			reactToCustomerUpdate();
		}
		
		/** Date 05-08-2017 added by vijay for return expense amount **/
		if(text.equals(AppConstants.RETURNEXPENSE)){
			System.out.println("hi");
			if(form.cbexptype.getValue()){
				reactonReturnExpense();
			}else{
				form.showDialogMessage("Can not Return Expense! petty cash not found for this expense");
			}
		}
		
		/** Date 08-08-2017 added by vijay for View return expense amount **/
		if(text.equals(AppConstants.viewReturnAmtHistory)){
			reactOnViewReturnAmountHistory();
		}
	}
	
	/** Date 08-08-2017 added by vijay for View return expense amount **/
	private void reactOnViewReturnAmountHistory() {

		returnHistoryPopup.getHistoryTable().getDataprovider().setList(model.getReturnExpenseHistory());
		returnHistoryPopup.showPopUp();
		returnHistoryPopup.getHorizontal().setVisible(false);
		returnHistoryPopup.getPopup().setWidth("500Px");
		
	}


/** Date 05-08-2017 added by vijay for return expense amount **/
	
	private void reactonReturnExpense() {

		
		returnExepensePopup.showPopUp();
	}


		private void reactToCustomerUpdate() {
			form.showWaitSymbol();
			update.updateCustomer(UserConfiguration.getCompanyId(), "UpdateCustomerStatus", new AsyncCallback<String>() {

				@Override
				public void onFailure(Throwable caught) {
					form.hideWaitSymbol();
					form.showDialogMessage("Failed!!");
				}

				@Override
				public void onSuccess(String result) {
					form.hideWaitSymbol();
					form.showDialogMessage(result);
				}
			});
		}


		private void reactOnCancelExpense() {
			
			popup.getPaymentDate().setValue(new Date() + "");
			popup.getPaymentID().setValue(
					form.tbexpenseId.getValue().trim());
			popup.getPaymentStatus().setValue(
					form.getstatustextbox().getValue().trim());
			cancelpanel = new PopupPanel(true);
			cancelpanel.add(popup);
			cancelpanel.show();
			cancelpanel.center();
		}


	private void migrateDataFromExpenseMngt() {
	
		
		expenseService.migrateDataFromExpenseMngt(UserConfiguration.getCompanyId(),new AsyncCallback<Void>()
				{

					@Override
					public void onFailure(Throwable caught) {
						form.showDialogMessage("Error In Expense Data Migration Successfull");
					}

					@Override
					public void onSuccess(Void result) {
						
						form.showDialogMessage("Data Migration Successfull");
					}
			
				});
	}


	private void reactToNew()
	{
		form.setToNewState();
		this.initalize();
		form.toggleAppHeaderBarMenu();
	}
	
	
		/**
		 * The Class ExpensemanagmentPresenterTable.
		 */
		@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.businessprocesslayer.MulipleExpenseMngt")
		 public static class  MultipleExpensemanagmentPresenterTable extends SuperTable<MultipleExpenseMngt> implements GeneratedVariableRefrence{

			/* (non-Javadoc)
			 * @see com.slicktechnologies.client.utility.GeneratedVariableRefrence#getVarRef(java.lang.String)
			 */
			@Override
			public Object getVarRef(String varName) {
		
				return null;
			}

			/* (non-Javadoc)
			 * @see com.simplesoftwares.client.library.appstructure.SuperTable#createTable()
			 */
			@Override
			public void createTable() {
		
				
			}

			/* (non-Javadoc)
			 * @see com.simplesoftwares.client.library.appstructure.SuperTable#initializekeyprovider()
			 */
			@Override
			protected void initializekeyprovider() 
			{
	
				
			}

			/* (non-Javadoc)
			 * @see com.simplesoftwares.client.library.appstructure.SuperTable#addFieldUpdater()
			 */
			@Override
			public void addFieldUpdater() 
			{
		
				
			}

			/* (non-Javadoc)
			 * @see com.simplesoftwares.client.library.appstructure.SuperTable#setEnable(boolean)
			 */
			@Override
			public void setEnable(boolean state)
			{
			
				
			}

			/* (non-Javadoc)
			 * @see com.simplesoftwares.client.library.appstructure.ViewContainer#applyStyle()
			 */
			@Override
			public void applyStyle() 
			{

				
			}} ;
			
			/**
			 * The Class ExpensemanagmentPresenterSearch.
			 */
			@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.businessprocesslayer.MulipleExpenseMngt")
			 public static class  MultipleExpensemanagmentPresenterSearch extends SearchPopUpScreen<MultipleExpenseMngt>{

				/* (non-Javadoc)
				 * @see com.simplesoftwares.client.library.appstructure.SearchPopUpScreen#getQuerry()
				 */
				@Override
				public MyQuerry getQuerry() {
			
					return null;
				}

				@Override
				public boolean validate() {
					return true;
				}}

			
			@Override
			public void onClick(ClickEvent event) {
				super.onClick(event);
				
				if(event.getSource()==form.cbexptype)
				{
					if(form.cbexptype.getValue()==true)
					{
//						form.olbApproverName.setSelectedIndex(0);
						form.olbpettycashname.setEnabled(true);
//						form.olbApproverName.setEnabled(false);
					}
					else
					{
						form.olbpettycashname.setSelectedIndex(0);
						form.olbpettycashname.setEnabled(false);
						form.olbApproverName.setEnabled(true);
						form.olbApproverName.setSelectedIndex(0);
					}
				}
				
				
				
				if(event.getSource().equals(form.getBtnNewCustomer1())){
					
					vendorPopup.showPopUp();
					
					
					vendorPopup.getTbInvoiceNo().setValue(form.getTbInvoiceNo().getValue());
					vendorPopup.getTbVendor().setValue(form.getTbVendor().getValue());
					
				}
				
				if(event.getSource()==vendorPopup.getLblOk()){
					System.out.println("YOU ARE ON TRACK.....!!!!");
					 saveNewVendor();
					vendorPopup.hidePopUp();
					
				}
				
				
				if(event.getSource()==vendorPopup.getLblCancel()){
					System.out.println("Cancel");
					vendorPopup.hidePopUp();
				}
				
				
				if(event.getSource()==form.btnNewCustomer2){
					travelPopup.showPopUp();
				}
				
				
				if(event.getSource()==travelPopup.getLblOk()){
					
						saveTravelDetails();
						travelPopup.hidePopUp();
					
				}
				
				if(event.getSource()==travelPopup.getLblCancel()){
					System.out.println("Cancel");
					travelPopup.hidePopUp();
				}
				
				
				
			//	rohan commented this today
//				if(event.getSource()==pettycashPopup.btnOk){
//					
//					if(pettycashPopup.getOblPettycashName().getSelectedIndex()!=0)
//					{
//						final String pettycashName =pettycashPopup.getOblPettycashName().getValue(pettycashPopup.getOblPettycashName().getSelectedIndex()).trim();
//						expenseService.pettycashBalanceDeductAndUpdatePettcashTransaction(pettycashName,model, new AsyncCallback<Void>() {
//
//							@Override
//							public void onFailure(Throwable caught) {
//								
//							}
//
//							@Override
//							public void onSuccess(Void result) {
//								
//								form.showDialogMessage("Pettycash Updated and Transaction Created...!!");
//								
//								model.setPettyCashName(pettycashName);
//								genasync.save(model, new AsyncCallback<ReturnFromServer>()
//								{
//
//									@Override
//									public void onFailure(Throwable caught) {
//										// TODO Auto-generated method stub
//										
//									}
//
//									@Override
//									public void onSuccess(ReturnFromServer result) {
//										
//										form.getOlbpettycashname().setValue(model.getPettyCashName());
//										form.setToViewState();
//									}
//							
//								});
//								
//							}
//						});
//						
//						panel.hide();
//					}
//					else
//					{
//						form.showDialogMessage("Please Select Pettycash Name..!!");
//						panel.hide();
//					}
//					
//				}
//				
//				if(event.getSource()==pettycashPopup.btnCancel){
//					panel.hide();
//				}
				
				
				
				if (event.getSource() == popup.getBtnOk()) {
					cancelpanel.hide();

					if (form.getstatustextbox().getValue().equals(Contract.APPROVED)) {
						
						if(validateRemark()){
							reactOnMultipleExpense(popup.getRemark().getValue().trim());
						}
						else{
							form.showDialogMessage("Please enter valid remark to cancel expense..!");
						}
					}

				} else if (event.getSource() == popup.getBtnCancel()) {
					cancelpanel.hide();
					popup.remark.setValue("");
				}
				
				
				
				/**
				 * Date 07-08-2017 added by vijay for Expense return
				 */
				
					if(event.getSource() instanceof InlineLabel)
					{
						InlineLabel lbl= (InlineLabel) event.getSource();
						System.out.println(returnExepensePopup.getLblOk().getText());
						System.out.println(lbl.getText());
						if(lbl.getText().equals(returnExepensePopup.getLblOk().getText())){
							System.out.println(returnExepensePopup.dbexpenseReturnAmt.getValue());
							if(returnExepensePopup.dbexpenseReturnAmt.getValue()==null || returnExepensePopup.dbexpenseReturnAmt.getValue()==0 ){
								form.showDialogMessage("Expense return amount can not be blank or zero ");
							}
							else if(returnExepensePopup.dbexpenseReturnAmt.getValue()>getAvailableTotalAmount(model)){
								form.showDialogMessage("Return Amount is greater than availble expense amount "+getAvailableTotalAmount(model)+"");
							}
							else{
								reactOnExpenseReturnOk(returnExepensePopup.dbexpenseReturnAmt.getValue());
							}
						}
						if(lbl.getText().equals(returnExepensePopup.getLblCancel().getText())){
							returnExepensePopup.getPopup().hide();
						}
					}
			}
		
			private void saveTravelDetails() {
				// TODO Auto-generated method stub
				//final Expense cust=new Expense();
				
//				if(!travelPopup.getDbKmTravelled().getValue().equals(" ")){
//					travelPopup.setDbKmTravelled(travelPopup.getDbKmTravelled().getValue());
//				}
				//travelPopup.dbKmTravelled.getValue();
				//travelPopup.dbFromDate.getValue();
			//	travelPopup.oblFromCity.getValue();
				//travelPopup.dbToDate.getValue();
				//travelPopup.oblToCity.getValue();
				
				form.travelKM= travelPopup.dbKmTravelled.getValue();
				form.travelFromDate =travelPopup.dbFromDate.getValue();
				form.travelFromCity =travelPopup.oblFromCity.getValue();
				form.travelToDate = travelPopup.dbToDate.getValue();
				form.travelToCity =travelPopup.oblToCity.getValue();
			}


			private void saveNewVendor() {
				
				// TODO Auto-generated method stub
				final Expense cust=new Expense();
				
//				
//				if(model.getSapInvoiceId()!=null)
//					refDocPopUp.getTbRefInvoiceId().setValue(model.getSapInvoiceId());
//					if(model.getSAPNetValue()!=0)
//					refDocPopUp.getDbNetValue().setValue(model.getSAPNetValue());
//					if(model.getSAPTaxAmount()!=0)
//					refDocPopUp.getDbTaxAmount().setValue(model.getSAPTaxAmount());
//					if(model.getSAPTotalAmount()!=0)
//					refDocPopUp.getDbTotalAmount().setValue(model.getSAPTotalAmount());
					
//				if(!custpoppup.getPnbLandlineNo().getValue().equals("")){
//					cust.setCellNumber1(custpoppup.getPnbLandlineNo().getValue());
//				}
//				if(!custpoppup.getEtbEmail().getValue().equals("")){
//					cust.setEmail(custpoppup.getEtbEmail().getValue().trim());
//				}
				
				if(!vendorPopup.getTbInvoiceNo().getValue().equals(" ")){
					cust.setInvoiceNumber(vendorPopup.getTbInvoiceNo().getValue());
				}
				
				if(!vendorPopup.getTbVendor().getValue().equals(" ")){
					cust.setVendor(vendorPopup.getTbVendor().getValue());
				}
				
				
//				vendorPopup.tbInvoiceNo.getValue();
//				vendorPopup.tbVendor.getValue();
				
			}


			private double getAvailableTotalAmount(MultipleExpenseMngt multiexpenseModel) {

				double totalReturnAmt=multiexpenseModel.getTotalAmount();
				
				if(multiexpenseModel.getReturnExpenseHistory().size()!=0){
					
					double returnamt = 0;
					for(int i=0;i<multiexpenseModel.getReturnExpenseHistory().size();i++){
						
						returnamt += multiexpenseModel.getReturnExpenseHistory().get(i).getExpenseReturnAmt();
					}
					System.out.println("HI "+returnamt);
					totalReturnAmt = totalReturnAmt - returnamt ;
					 
				}
				System.out.println("AMOUNT =="+totalReturnAmt);
				
				return totalReturnAmt;
			}
			
			private void reactOnExpenseReturnOk(Double expensereturnAmt) {
				
				
				GeneralServiceAsync genService = GWT.create(GeneralService.class);
				String loginUser ="";
				if(LoginPresenter.loggedInUser!=null){
					loginUser = LoginPresenter.loggedInUser;
				}
				
				genService.returnExpenseAmount(model, expensereturnAmt,loginUser, new AsyncCallback<MultipleExpenseMngt>() {

					@Override
					public void onFailure(Throwable caught) {
						form.showDialogMessage("Unexpected Error Occurred");
						returnExepensePopup.getPopup().hide();
					}

					@Override
					public void onSuccess(MultipleExpenseMngt result) {
						form.showDialogMessage("Return Amount Deposited Successfully ");
						form.getPresenter().setModel(result);
						returnExepensePopup.getPopup().hide();
						
					}
				});
				
			}
			
			private void reactOnMultipleExpense(final String remark) {
			/**
			 * This method is used to cancel expense mngt
			 */
					
				final String loginUser = LoginPresenter.loggedInUser.trim();
				form.showWaitSymbol();
				model.setStatus(MultipleExpenseMngt.CANCELLED);
				model.setDescription(model.getDescription() + "\n" + "Expense Id ="
						+ model.getCount() + " "
						+ "Expense Status = "+model.getStatus()
						+ "\n"
						+ "has been cancelled by " + loginUser
						+ " with remark" + "\n" + "Remark ="
						+ remark);
				update.reactOnCancelExpenseMngt(model,remark,loginUser, new AsyncCallback<Void>() {
					
					@Override
					public void onSuccess(Void result) {
						
						form.getstatustextbox().setValue(MultipleExpenseMngt.CANCELLED);
						form.getTaDescription().setValue(model.getDescription());
//						form.getTaDescription().setValue(model.getDescription() + "\n" + "Expense Id ="
//						+ model.getCount() + " "
//						+ "Expense Status = "+model.getStatus()
//						+ "\n"
//						+ "has been cancelled by " + loginUser
//						+ " with remark" + "\n" + "Remark ="
//						+ remark);
						
						form.hideWaitSymbol();
						form.showDialogMessage("Expense Cancellation Successful..!");
						form.setToViewState();
						
					}
					
					@Override
					public void onFailure(Throwable caught) {
						form.hideWaitSymbol();
						form.showDialogMessage("Fail Try Again..!");
					}
				});
			/**
			 * ends here  	
			 */
			}


			private boolean validateRemark(){
				
				if(popup.getRemark().getValue().trim()!= null && !popup.getRemark().getValue().trim().equals("")){
					
					return true;
				}
				return false;
			}
			
			@Override
			public void onValueChange(ValueChangeEvent<Double> event) {
//				if(event.getSource()==form.totalTotalAmt)
//				{
//					if(form.olbpettycashname.getSelectedItem()!=null&&form.totalTotalAmt.getValue()!=null)
//					{
//						System.out.println("Inside condition ");
//						reactOnExpenseFormField();
//					}
//				}
			}


			@Override
			public void onChange(ChangeEvent event) {
				
				
				if(event.getSource()==form.olbpettycashname)
				{
					System.out.println("in side change evet petty cash");
					if(form.olbpettycashname.getSelectedItem()!=null&&form.totalTotalAmt.getValue()!=null)
					{
						reactOnExpenseFormField();
					}
				}
			};
			
			public void reactOnExpenseFormField()
			{
				System.out.println("Inside method ");
				if(form.cbexptype.getValue()==true)
				{
					PettyCash petty=form.olbpettycashname.getSelectedItem();
					ArrayList<ApproverAmount> listPetty=petty.getAmountApproved();
					
					Collections.sort(listPetty,new  Comparator<ApproverAmount>(){

						@Override
						public int compare(ApproverAmount o1, ApproverAmount o2) {
							if(o1.getAmount()>o2.getAmount())
								return 1;
							if(o1.getAmount()==o2.getAmount())
								return 0;
							else
								return -1;
						}
					}
					);
					
					double petybalance=petty.getPettyCashBalance();
					double expenseAmt=form.totalTotalAmt.getValue();
					/**
					 * @author Anil , Date : 07-08-2019
					 */
					double configAmount = 0;
					for (int i = 0; i < listPetty.size(); i++) {
						configAmount = listPetty.get(i).getAmount();
		
						if (configAmount >= expenseAmt) {
							form.olbApproverName.setValue(listPetty.get(i)
									.getEmployeeName().trim());
							break;
						}
					}
					if (configAmount < expenseAmt) {
						form.showDialogMessage("Approver not assigned for entered amount!");
						form.olbApproverName.setSelectedIndex(0);
					}
					
					/**
					 * End
					 */
					if(expenseAmt>petybalance)
					{
						form.showDialogMessage("No sufficient balance in petty cash!");
						form.olbApproverName.setSelectedIndex(0);
					}
				}
			}


			@Override
			public void reactOnSearch() {
				super.reactOnSearch();
				
				MultipleExpensemanagmentPresenterSearchProxy.cbexptypepettycash.setValue(false);
			}


			@Override
			public void onRowCountChange(RowCountChangeEvent event) {
				calculateTotalExpenseAmout();
				
			}
			
			
			/**
			 * This is used for calculation of total expense amout 
			 */
				private void calculateTotalExpenseAmout() {
					List<ExpenseManagement> expList = form.expenseMngtTable.getDataprovider().getList();
					double total=0;
					for (int i = 0; i < expList.size(); i++) {
						total = total +expList.get(i).getAmount();
					}
					
					form.getTotalTotalAmt().setValue(total);
					
					
					if(form.olbpettycashname.getSelectedItem()!=null 
							&& form.totalTotalAmt.getValue()!=null)
					{
						System.out.println("Inside condition on click of add (Row count changes)");
						reactOnExpenseFormField();
					}
				}
}
