package com.slicktechnologies.client.views.account.multipleexpensemanagment;

import java.util.Date;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.Service;

public class ExpenseManagementTable extends SuperTable<ExpenseManagement>{

	TextColumn<ExpenseManagement> invoiceIdColumn,categoryColumn,typeColumn,groupColumn;
	TextColumn<ExpenseManagement> expenseDateColumn,employeeColumn,amountColumn,currencyColumn;
	TextColumn<ExpenseManagement> paymentMethodColumn,vendorColumn;
	Column<ExpenseManagement,String> deleteColumn;
	
	TextColumn<ExpenseManagement> getDeptDateCol,getDeptCityCol,getArrvDateCol,getArrvCityCol,getKmTravelledCol,getEmpRoleCol;
	
	@Override
	public void createTable() {
		
		
//		createEmployeeColumn();
//		createExpenseDateColumn();
//		createAmountColumn();
//		createInvoiceIdColumn();
//		createGroupColumn();
//		createCategoryColumn();
//		createTypeColumn();
//		
//		
//		getEmpRoleCol();
//		getDeptDateCol();
//		getDeptCityCol();
//		getArrvDateCol();
//		getArrvCityCol();
//		getKmTravelledCol();
//		
//		
////		createCurrencyColumn();
//		createPaymentMethodColumn();
//		createvendorColumn();
//		createColumndeleteColumn();
		
		createEmployeeColumn();
		getEmpRoleCol();
		createCategoryColumn();
		createTypeColumn();
		createExpenseDateColumn();
		createAmountColumn();
		createPaymentMethodColumn();
		createGroupColumn();
		createvendorColumn();
		createInvoiceIdColumn();
		getKmTravelledCol();
		getDeptDateCol();
		getDeptCityCol();
		getArrvDateCol();
		getArrvCityCol();
		createColumndeleteColumn();
		
		
		addFieldUpdater();
	}


	private void getEmpRoleCol() {
		getEmpRoleCol = new TextColumn<ExpenseManagement>() {
			@Override
			public String getValue(ExpenseManagement object) {
				if (object.getEmployeeRole() != null) {
					return object.getEmployeeRole();
				} else {
					return "N A";
				}
			}
		};
		table.addColumn(getEmpRoleCol, "Employee Role");
		table.setColumnWidth(getEmpRoleCol, 100, Unit.PX);
		
	}


	private void getDeptDateCol() {
		getDeptDateCol = new TextColumn<ExpenseManagement>() {
			@Override
			public String getValue(ExpenseManagement object) {
				if (object.getFromDate() != null) {
					return AppUtility.parseDate(object.getFromDate());
				} else {
					return "N A";
				}
			}
		};
		table.addColumn(getDeptDateCol, "Dept. Date");
		table.setColumnWidth(getDeptDateCol, 100, Unit.PX);
		
	}


	private void getDeptCityCol() {
		getDeptCityCol = new TextColumn<ExpenseManagement>() {
			@Override
			public String getValue(ExpenseManagement object) {
				if (object.getFromCity() != null) {
					return object.getFromCity();
				} else {
					return "N A";
				}
			}
		};
		table.addColumn(getDeptCityCol, "Dept. City");
		table.setColumnWidth(getDeptCityCol, 100, Unit.PX);
		
	}


	private void getArrvDateCol() {
		getArrvDateCol = new TextColumn<ExpenseManagement>() {
			@Override
			public String getValue(ExpenseManagement object) {
				if (object.getToDate() != null) {
					return AppUtility.parseDate(object.getToDate());
				} else {
					return "N A";
				}
			}
		};
		table.addColumn(getArrvDateCol, "Arrv. Date");
		table.setColumnWidth(getArrvDateCol, 100, Unit.PX);
		
	}


	private void getArrvCityCol() {
		getArrvCityCol = new TextColumn<ExpenseManagement>() {
			@Override
			public String getValue(ExpenseManagement object) {
				if (object.getToCity() != null) {
					return object.getToCity();
				} else {
					return "N A";
				}
			}
		};
		table.addColumn(getArrvCityCol, "Arrv. City");
		table.setColumnWidth(getArrvCityCol, 100, Unit.PX);
		
	}


	private void getKmTravelledCol() {
		getKmTravelledCol = new TextColumn<ExpenseManagement>() {
			@Override
			public String getValue(ExpenseManagement object) {
				if (object.getKmTravelled() != 0) {
					return object.getKmTravelled()+"";
				} else {
					return "";
				}
			}
		};
		table.addColumn(getKmTravelledCol, "Km Travelled");
		table.setColumnWidth(getKmTravelledCol, 100, Unit.PX);
		
	}


	private void createAmountColumn() {
		amountColumn = new TextColumn<ExpenseManagement>() {
			@Override
			public String getValue(ExpenseManagement object) {
				if (object.getAmount() == 0) {
					return 0 + "";
				} else {
					return object.getAmount() + "";
				}
			}
		};
		table.addColumn(amountColumn, "Amount");
		table.setColumnWidth(amountColumn, 100, Unit.PX);
	}

	protected void createColumndeleteColumn(){
		ButtonCell btnCell = new ButtonCell();
		deleteColumn = new Column<ExpenseManagement, String>(btnCell) {
			@Override
			public String getValue(ExpenseManagement object) {
				return "Delete";
			}
		};
		table.addColumn(deleteColumn, "Delete");
		table.setColumnWidth(deleteColumn, 100, Unit.PX);
	}
	
	private void createInvoiceIdColumn() {

		invoiceIdColumn = new TextColumn<ExpenseManagement>() {
			@Override
			public String getValue(ExpenseManagement object) {
				if (object.getInvoiceNumber() != null) {
					return object.getInvoiceNumber();
				} else {
					return "N A";
				}
			}
		};
		table.addColumn(invoiceIdColumn, "Invoice Id");
		table.setColumnWidth(invoiceIdColumn, 100, Unit.PX);
	}



	private void createCurrencyColumn() {

		currencyColumn = new TextColumn<ExpenseManagement>() {
			@Override
			public String getValue(ExpenseManagement object) {
				if (object.getCurrency() != null) {
					return object.getCurrency();
				} else {
					return "N A";
				}
			}
		};
		table.addColumn(currencyColumn, "Currency");
		table.setColumnWidth(currencyColumn, 100, Unit.PX);
	}	
	
	private void createvendorColumn() {

		vendorColumn = new TextColumn<ExpenseManagement>() {
			@Override
			public String getValue(ExpenseManagement object) {
				if (object.getVendor() != null) {
					return object.getVendor();
				} else {
					return "N A";
				}
			}
		};
		table.addColumn(vendorColumn, "Vendor Name");
		table.setColumnWidth(vendorColumn, 100, Unit.PX);
	}
	
	
	private void createPaymentMethodColumn() {

		paymentMethodColumn = new TextColumn<ExpenseManagement>() {
			@Override
			public String getValue(ExpenseManagement object) {
				if (object.getPaymentMethod() != null) {
					return object.getPaymentMethod();
				} else {
					return "N A";
				}
			}
		};
		table.addColumn(paymentMethodColumn, "Payment Method");
		table.setColumnWidth(paymentMethodColumn, 100, Unit.PX);
	}
	

	private void createGroupColumn() {

		groupColumn = new TextColumn<ExpenseManagement>() {
			@Override
			public String getValue(ExpenseManagement object) {
				if (object.getExpenseGroup() != null) {
					return object.getExpenseGroup();
				} else {
					return "N A";
				}
			}
		};
		table.addColumn(groupColumn, "Group");
		table.setColumnWidth(groupColumn, 100, Unit.PX);
	}


	protected void createCategoryColumn(){
		categoryColumn = new TextColumn<ExpenseManagement>() {
			@Override
			public String getValue(ExpenseManagement object) {
				if (object.getExpenseCategory() != null) {
					return object.getExpenseCategory();
				} else {
					return "N A";
				}
			}
		};
		table.addColumn(categoryColumn, "Category");
		table.setColumnWidth(categoryColumn, 100, Unit.PX);
	}
	
	
	private void createTypeColumn() {

		typeColumn = new TextColumn<ExpenseManagement>() {
			@Override
			public String getValue(ExpenseManagement object) {
				if (object.getExpenseType() != null) {
					return object.getExpenseType();
				} else {
					return "N A";
				}
			}
		};
		table.addColumn(typeColumn, "Type");
		table.setColumnWidth(typeColumn, 100, Unit.PX);
	}
	
	private void createExpenseDateColumn() {

		expenseDateColumn = new TextColumn<ExpenseManagement>() {

			@Override
			public String getValue(ExpenseManagement object) {
				return AppUtility.parseDate(object.getExpenseDate());
			}
		};
		table.addColumn(expenseDateColumn, "Expense Date");
		table.setColumnWidth(expenseDateColumn, 80, Unit.PX);
	}
	
	private void createEmployeeColumn() {

		employeeColumn = new TextColumn<ExpenseManagement>() {
			@Override
			public String getValue(ExpenseManagement object) {
				if (object.getEmployee() != null) {
					return object.getEmployee();
				} else {
					return "N A";
				}
			}
		};
		table.addColumn(employeeColumn, "Employee");
		table.setColumnWidth(employeeColumn, 100, Unit.PX);
	}
	
	@Override
	protected void initializekeyprovider() {
		
	}

	@Override
	public void addFieldUpdater() {
		createFieldUpdaterdeleteColumn();
	}
	
	protected void createFieldUpdaterdeleteColumn(){
		deleteColumn.setFieldUpdater(new FieldUpdater<ExpenseManagement, String>() {
			@Override
			public void update(int index, ExpenseManagement object,
					String value) {
				getDataprovider().getList().remove(object);
				table.redrawRow(index);
			}
		});
	}

	@Override
	public void setEnable(boolean state) {
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
