package com.slicktechnologies.client.views.account.multipleexpensemanagment;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;









import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.composite.UploadComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.approvalutility.ApprovableFormScreen;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.composites.EmployeeInfoComposite;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.shared.PettyCash;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Expense;
import com.slicktechnologies.shared.common.businessprocesslayer.ExpensePolicies;
import com.slicktechnologies.shared.common.businessprocesslayer.MultipleExpenseMngt;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.City;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class MultipleExpensemanagmentForm extends ApprovableFormScreen<MultipleExpenseMngt> implements ClickHandler, ChangeHandler, SelectionHandler<Suggestion>{

	/** expense Date */
	DateBox dbExpenseDate;
	
	TextBox tbexpenseId;
	/** repersents Branch */
	ObjectListBox<Branch> olbBranch;
	
	/** The olb expense group. */
	ObjectListBox<Config>olbCurrency,olbPaymentMethod,olbExpenseGroup;
	
	/** The olb category. */
	ObjectListBox<ConfigCategory>olbCategory;
	
	/** The olb type. */
	ObjectListBox<Type>olbType;
	
	/** The tb status. */
	TextBox tbInvoiceNo,tbVendor,tbStatus;
	
	/** The ta description. */
	TextArea taDescription;
	
	/** The uc choose file. */
	UploadComposite ucChooseFile;
	
	/** The db amount. */
	DoubleBox dbAmount;
	
	/** The olb approver name. */
	ObjectListBox<Employee> olbEmployee,olbApproverName;
	
	CheckBox cbexptype;
	ObjectListBox<PettyCash> olbpettycashname;
	TextBox tbremark;
	
	TextBox tbAccount;
	FormField fgroupingExpenseDetails;
	MultipleExpenseMngt expenseobj;
	
	TextBox tbServiceId;
	
	Button btnNewCustomer1;
	Button btnNewCustomer2;
	
	/**
	 *rohan has added this flag for validating petty cash name from withdraw button
	 */
	
	boolean pettyCashNameFlag = false;
	
	/**
	 * ends here 
	 */
	
	/**
	 * Rohan added this table for multiple expenses at the same time
	 */
	Button addDetails;
	ExpenseManagementTable expenseMngtTable;
	DoubleBox totalTotalAmt;
	
	
	/**
	 * @author Anil , Date : 23-08-2019
	 */
	DateBox dbFromDate;
	DateBox dbToDate;
	
	ObjectListBox<City> oblFromCity;
	ObjectListBox<City> oblToCity;
	
	ObjectListBox<Config> oblEmployeeRole;
	
	DoubleBox dbKmTravelled;
	
	/**
	 * @author Anil , Date : 26-08-2019
	 * Changing employee drop down list to employee composite
	 */
	EmployeeInfoComposite personInfo;
	
	//Ashwini Patil Date:23-12-2022
	double travelKM=0;
	Date travelFromDate,travelToDate;
	String travelFromCity,travelToCity;
	
	public PersonInfoComposite pInfo;
	public TextBox tbSalesOrderId,tbreferenceId;

	//***********************************Variable Declaration Ends********************************************//


	//Token to add the concrete presenter class name
	//protected <PresenterClassName> presenter;




	/**
	 * Instantiates a new expensemanagment form.
	 *
	 * @param table the table
	 * @param mode the mode
	 * @param captionmode the captionmode
	 */
	public MultipleExpensemanagmentForm  () {
		
		createGui();
		
//		expenseMngtTable.connectToLocal();
	}
	
	//***********************************Variable Initialization********************************************//

	/**
	 * Initalize widget.
	 */
	private void initalizeWidget()
	{

		dbExpenseDate=new DateBoxWithYearSelector();

		olbBranch=new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbBranch);

		olbExpenseGroup=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbExpenseGroup,Screen.EXPENSEGROUP);
		olbCategory=new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(getOlbCategory(), Screen.EXPENSECATEGORY);
		olbCategory.addChangeHandler(this);
		olbType=new ObjectListBox<Type>();
		AppUtility.makeTypeListBoxLive(this.olbType, Screen.EXPENSETYPE);

		olbCurrency=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbCurrency,Screen.CURRENCY);

		olbPaymentMethod=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbPaymentMethod,Screen.PAYMENTMETHODS);

		tbInvoiceNo=new TextBox();
		tbStatus=new TextBox();
		tbStatus.setEnabled(false);
		tbStatus.setText(MultipleExpenseMngt.CREATED);
		tbVendor=new TextBox();

		taDescription=new TextArea();

		ucChooseFile=new UploadComposite();

		dbAmount=new DoubleBox();

		tbexpenseId=new TextBox();
		tbexpenseId.setEnabled(false);
		
		tbremark=new TextBox();
		tbremark.setEnabled(false);
		
//		olbEmployee=new ObjectListBox<Employee>();
//		AppUtility.makeSalesPersonListBoxLive(olbEmployee);
//		olbEmployee.addChangeHandler(this);

		olbApproverName=new ObjectListBox<Employee>();
		AppUtility.makeApproverListBoxLive(olbApproverName, "Expense Management");
		cbexptype=new CheckBox();
		olbpettycashname=new ObjectListBox<PettyCash>();
		olbpettycashname.MakeLive(new MyQuerry(new Vector<Filter>(), new PettyCash()));
		olbpettycashname.setEnabled(false);
		
		tbAccount=new TextBox();

		tbServiceId = new TextBox();
		
		expenseMngtTable = new ExpenseManagementTable();
		
		addDetails=new Button("ADD");
		addDetails.addClickHandler(this);
		
		totalTotalAmt = new DoubleBox();
		totalTotalAmt.setEnabled(false);
		
		dbFromDate=new DateBoxWithYearSelector();
		dbToDate=new DateBoxWithYearSelector();
		
		oblFromCity=new ObjectListBox<City>();
		City.makeOjbectListBoxLive(oblFromCity);
		
		oblToCity=new ObjectListBox<City>();
		City.makeOjbectListBoxLive(oblToCity);
		
		dbKmTravelled=new DoubleBox();
		
		oblEmployeeRole=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(oblEmployeeRole, Screen.EMPLOYEEROLE);
		
		personInfo=AppUtility.employeeInfoComposite(new EmployeeInfo(),false);
		personInfo.addSelectionHandler(this);
		
		
		 btnNewCustomer1 = new Button("Vendor Details");
		 btnNewCustomer1.addClickHandler(this);
		
		 btnNewCustomer2 = new Button("Travel Details");
		 btnNewCustomer2.addClickHandler(this);
		 
		 tbSalesOrderId = new TextBox();
		 MyQuerry querry=new MyQuerry();
		 querry.setQuerryObject(new Customer());
		 pInfo=new PersonInfoComposite(querry,false);
		 tbreferenceId = new TextBox();
		 
	}

	/*
	 * Method template to create the formtable screen
	 */

	/* (non-Javadoc)
	 * @see com.slicktechnologies.client.approvalutility.ApprovableFormTableScreen#createScreen()
	 */
	@Override
	public void createScreen()  {

		//Token to initialize the processlevel menus.
		initalizeWidget();

		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Expense","UpdateRecordButtonVisibility"))
		{
			processlevelBarNames=new String[]{ManageApprovals.SUBMIT,"Request For Approval","Cancel Approval Request","New","Migrate Record","Cancel Expense","Update Customer",AppConstants.RETURNEXPENSE,AppConstants.viewReturnAmtHistory};
		}	
		else
		{
			/**23-09-2017 sagar sore Submit button added for self approval **/
			processlevelBarNames=new String[]{ManageApprovals.SUBMIT,"Request For Approval","Cancel Approval Request","New","Cancel Expense",AppConstants.RETURNEXPENSE,AppConstants.viewReturnAmtHistory};
		}
		//////////////////////////////////Form Field Declaration/////////////////////////////////////////////////


		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////

		//Token to initialize formfield
		
		String mainScreenLabel="EXPENSES";
		if(expenseobj!=null&&expenseobj.getCount()!=0){
			mainScreenLabel=expenseobj.getCount()+" "+"/"+expenseobj.getStatus()+" "+ "/"+AppUtility.parseDate(expenseobj.getCreationDate());
		}
		

		//fgroupingExpenseDetails.getHeaderLabel().setText(mainScreenLabel);
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		//FormField fgroupingExpenseDetails=fbuilder.setlabel("Expense Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		fgroupingExpenseDetails=fbuilder.setlabel(mainScreenLabel).widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).setKeyField(true).build();
		fbuilder = new FormFieldBuilder("Invoice No",tbInvoiceNo);
		FormField ftbInvoiceNo= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Expense ID",tbexpenseId);
		FormField ftbexpenseId= fbuilder.setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Expense Category",olbCategory);
		FormField folbCategory= fbuilder.setMandatory(false).setMandatoryMsg("Category is Mandatory!").setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Expense Group",olbExpenseGroup);
		FormField folbExpenseGroup= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Expense Type",olbType);
		FormField folbExpenseType= fbuilder.setMandatory(false).setMandatoryMsg("Type is Mandatory!").setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("* Status",tbStatus);
		FormField ftbExpenseStatus= fbuilder.setMandatory(true).setMandatoryMsg("Status is Mandatory!").setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("* Expense Date",dbExpenseDate);
		FormField fdbExpenseDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Currency",olbCurrency);
		FormField folbCurrency= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Amount",dbAmount);
		FormField fdbAmount= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Payment Method",olbPaymentMethod);
		FormField folbPaymentMethod= fbuilder.setMandatory(false).setMandatoryMsg("Payment Method is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Vendor",tbVendor);
		FormField ftbVendor= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
//		fbuilder = new FormFieldBuilder("Employee",olbEmployee);
//		FormField folbEmployee= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Branch",olbBranch);
		FormField folbBranch= fbuilder.setMandatory(true).setMandatoryMsg("Branch is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingExpenseDocuments=fbuilder.setlabel("Attachment").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		fbuilder = new FormFieldBuilder("Choose File",ucChooseFile);
		FormField fucChooseFile= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingDescriptions=fbuilder.setlabel("Reference/General").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		fbuilder = new FormFieldBuilder("Description (Max 500 characters)",taDescription);
		FormField ftaDescription= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(5).build();
		fbuilder = new FormFieldBuilder("Remark",tbremark);
		FormField ftbremark= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("* Approver Name",olbApproverName);
		FormField folbApproverName= fbuilder.setMandatory(true).setMandatoryMsg("Approver Name  is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingPettyCash=fbuilder.setlabel("Petty Cash Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();		
		fbuilder = new FormFieldBuilder("Is expense type a petty cash",cbexptype);
		FormField fcbexptype= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Petty Cash Name",olbpettycashname);
		FormField folbpettycashname= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Account",tbAccount);
		FormField ftbAccount= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Service ID",tbServiceId);
		FormField ftbreferenceNo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder(" ",expenseMngtTable.getTable());
		FormField ftable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(5).build();
		
		fbuilder = new FormFieldBuilder("",addDetails);
		FormField faddDetails= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Total Amount",totalTotalAmt);
		FormField ftotalTotalAmt= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingmultipleExpenseDetails=fbuilder.setlabel("Expense Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();

		
		fbuilder = new FormFieldBuilder("Departure Date",dbFromDate);
		FormField fdbFromDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Arrival Date",dbToDate);
		FormField fdbToDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Departure City",oblFromCity);
		FormField foblFromCity= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Arrival City",oblToCity);
		FormField foblToCity= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("KM Travelled",dbKmTravelled);
		FormField fdbKmTravelled= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Employee Role",oblEmployeeRole);
		FormField foblEmployeeRole= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder(" ",personInfo);
		FormField fpersonInfo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		
		fbuilder = new FormFieldBuilder("",btnNewCustomer1);
		FormField fbtnNewCustomer = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",btnNewCustomer2);
		FormField fbtnNewCustomer1 = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/**
		 * @author Anil @since 15-01-2021
		 * adding blank label to reduce the width of approver name dorp down
		 * raised by Vaishnavi
		 */
		InlineLabel tb=new InlineLabel(".");
		tb.getElement().getStyle().setMarginRight(90, Unit.PX);
		tb.getElement().getStyle().setColor("white");
		fbuilder = new FormFieldBuilder("",tb);
		FormField fblankgroup=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingOrderInformation=fbuilder.setlabel("Order Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();

		fbuilder = new FormFieldBuilder("",pInfo);
		FormField fpInfo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Sales Order Id",tbSalesOrderId);
		FormField ftbSalesOrderId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Reference Number",tbreferenceId);
		FormField ftbreferenceId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder();
		FormField fblankgroup2=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////


//		FormField[][] formfield = {{fgroupingExpenseDetails},
//				{ftbexpenseId,folbBranch,ftbExpenseStatus,ftbremark},
//				{fpersonInfo},
//				{foblEmployeeRole,ftbreferenceNo},
//				
//				{fgroupingmultipleExpenseDetails},
//				{fdbFromDate,foblFromCity,fdbToDate,foblToCity},
//				{ftbInvoiceNo,folbExpenseGroup,folbCategory,folbExpenseType},
//				{fdbExpenseDate,fdbAmount,folbPaymentMethod,ftbVendor},
//				{fdbKmTravelled,faddDetails},
//				{ftable},
//				{ftotalTotalAmt},
//				
//				{fgroupingPettyCash},
//				{fcbexptype,folbpettycashname,ftbAccount,folbApproverName},
//				{fgroupingExpenseDocuments},
//				{fucChooseFile},
//				{fgroupingDescriptions},
//				{ftaDescription},
				
				FormField[][] formfield = {
						/**Expenses**/
						{fgroupingExpenseDetails},
						{fpersonInfo},
						{folbApproverName,folbBranch,ftbremark,fblankgroup},
						
						/**Petty Cash**/
//						{fgroupingPettyCash},
						{fcbexptype,folbpettycashname,ftbAccount},
						
						/**Expenses Information**/
						{fgroupingmultipleExpenseDetails},
						{folbCategory,folbExpenseType,fdbExpenseDate,fdbAmount},
						{folbPaymentMethod,folbExpenseGroup,fbtnNewCustomer,fbtnNewCustomer1},
						{faddDetails},
						{ftable},
						{ftotalTotalAmt},
						
//						{fdbFromDate,foblFromCity,fdbToDate,foblToCity},
//						{ftbInvoiceNo,ftbVendor,fdbKmTravelled},
						
						
						
						/**Reference**/
						{fgroupingDescriptions},
						{foblEmployeeRole,ftbreferenceNo,ftbreferenceId},
						{ftaDescription},
						/**Attachment**/
						{fgroupingExpenseDocuments},
						{fucChooseFile},
						/**** Order Information ****/
						{fgroupingOrderInformation},
						{fpInfo},
						{ftbSalesOrderId,fblankgroup2},


		};


		this.fields=formfield;		
	}

	/**
	 * method template to update the model with token entity name.
	 *
	 * @param model the model
	 */
	@Override
	public void updateModel(MultipleExpenseMngt model) 
	{
		if(olbBranch.getValue()!=null)
			model.setBranch(olbBranch.getValue());
			model.setExpenseDocument(ucChooseFile.getValue());
		if(taDescription.getValue()!=null)
			model.setDescription(taDescription.getValue());
		if(olbApproverName.getValue()!=null)
			model.setApproverName(olbApproverName.getValue());
		if(tbStatus.getValue()!=null)
			model.setStatus(tbStatus.getValue());
		
		if(cbexptype.getValue()!=null)
			model.setExpTypeAsPettyCash(cbexptype.getValue());
		if(olbpettycashname.getValue()!=null)
			model.setPettyCashName(olbpettycashname.getValue());

		if(tbAccount.getValue()!=null)
			model.setAccount(tbAccount.getValue());
		
		if(!tbServiceId.getValue().equals(""))
			model.setServiceId(Integer.parseInt(tbServiceId.getValue()));
		
		if(totalTotalAmt.getValue()!=null)
			model.setTotalAmount(totalTotalAmt.getValue());
		
		//Date 17-08-2017 added by vijay
//		if(olbEmployee.getValue()!=null){
//			model.setEmployee(olbEmployee.getValue());
//		}
		
		/** 25-09-2017 sagar sore To save value PettyCash checkbox in multiple expense management*/
		if(cbexptype.getValue()!=null)
		{
			System.out.println("Checkboc type "+cbexptype.getText());
			model.setExpTypeAsPettyCash(cbexptype.getValue());
			
		}
		List<ExpenseManagement> expenseLis=this.expenseMngtTable.getDataprovider().getList();
		ArrayList<ExpenseManagement> arrItems=new ArrayList<ExpenseManagement>();
		arrItems.addAll(expenseLis);
		model.setExpenseList(arrItems);
		
		if(personInfo.getValue()!=null){
			model.setEmpId(personInfo.getEmployeeId());
			model.setEmpCell(personInfo.getCellNumber());
			model.setEmployee(personInfo.getEmployeeName());
		}
		
		if(oblEmployeeRole.getValue()!=null){
			model.setEmployeeRole(oblEmployeeRole.getValue());
		}

		if(!tbSalesOrderId.getValue().equals("") && !tbSalesOrderId.getValue().equals(""))
			model.setSalesOrderId(Integer.parseInt(tbSalesOrderId.getValue()));
		
		if(pInfo.getValue()!=null && !pInfo.getValue().equals(""))
			model.setPersonInfo(pInfo.getValue());
		
		if(tbreferenceId.getValue()!=null && !tbreferenceId.getValue().equals(""))
			model.setReferenceNumber(Integer.parseInt(tbreferenceId.getValue()));
		
		expenseobj=model;
		
		presenter.setModel(model);

	}
	
	/**
	 * method template to update the view with token entity name.
	 *
	 * @param view the view
	 */
	@Override
	public void updateView(MultipleExpenseMngt view) 
	{
		
		expenseobj=view;
		
			tbexpenseId.setValue(view.getCount()+"");
		if(view.getBranch()!=null)
			olbBranch.setValue(view.getBranch());
		if(view.getExpenseDocument()!=null)
			ucChooseFile.setValue(view.getExpenseDocument());
		if(view.getDescription()!=null)
			taDescription.setValue(view.getDescription());
		if(view.getApproverName()!=null)
			olbApproverName.setValue(view.getApproverName());
		if(view.getStatus()!=null)
			tbStatus.setValue(view.getStatus());
//		if(view.getExpTypeAsPettyCash()!=null)
//			cbexptype.setValue(view.getExpTypeAsPettyCash());
		if(view.getPettyCashName()!=null)
			olbpettycashname.setValue(view.getPettyCashName());
		if(view.getRemark()!=null){
			tbremark.setValue(view.getRemark());
		}
		if(view.getAccount()!=null){
			tbAccount.setValue(view.getAccount());
		}

		expenseMngtTable.setValue(view.getExpenseList());
		
		if(view.getServiceId()!=0){
			tbServiceId.setValue(view.getServiceId()+"");
		}
		
		if(view.getTotalAmount()!=0)
			totalTotalAmt.setValue(view.getTotalAmount());
		
	
		//Date 17-08-2017 added by vijay
//		if(view.getEmployee()!=null){
//			olbEmployee.setValue(view.getEmployee());
//		}
		
		if(view.getEmployee()!=null){
			personInfo.setEmployeeName(view.getEmployee());
		}
		if(view.getEmpId()!=0){
			personInfo.setEmployeeId(view.getEmpId());
		}
		if(view.getEmpCell()!=0){
			personInfo.setCellNumber(view.getEmpCell());
		}
		
		if(view.getSalesOrderId()!=0){
			tbSalesOrderId.setValue(view.getSalesOrderId()+"");
		}
		if(view.getPersonInfo()!=null){
			pInfo.setValue(view.getPersonInfo());
		}
		if(view.getReferenceNumber()!=0){
			tbreferenceId.setValue(view.getReferenceNumber()+"");
		}
		
		/* 
		 * for approval process
		 *  nidhi
		 *  5-07-2017
		 */
		if(presenter != null){
			presenter.setModel(view);
		}
		/*
		 *  end
		 */
		
		/** 23-09-2017 sagar sore To set saved petty cash checkbox value of */
//		if(cbexptype.getValue()!=null)
//		{
			cbexptype.setValue(view.isExpTypeAsPettyCash());
//		}
		
		/**
		 * @author Anil
		 * @since 05-01-2021
		 * employee role was not getting mapped to dorpdown list when we search expense data
		 * raised by Vaishnavi
		 */
			
		if(view.getEmployeeRole()!=null){
			oblEmployeeRole.setValue(view.getEmployeeRole());
		}
	}
	
	
	/**
	 * Toggles the app header bar menus as per screen state.
	 */

	public void toggleAppHeaderBarMenu() {
		System.out.println("currentState::::::::::::::::"+AppMemory.getAppMemory().currentState);
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			/**
			 * @author Anil @since 15-07-2021
			 * for summary screen
			 */
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Save")||text.contains("Discard")||text.contains("Search")||text.contains(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			/**
			 * @author Anil @since 15-07-2021
			 * for summary screen
			 */
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Save")||text.contains("Discard")||text.contains(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			/**
			 * @author Anil @since 15-07-2021
			 * for summary screen
			 */
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Edit")||text.contains("Discard")||text.contains("Search")||text.contains("Print")||text.contains(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
			

			
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.MULTIPLEEXPENSEMANAGMENT,LoginPresenter.currentModule.trim());

	}
	
	
	public void toggleProcessLevelMenu()
	{
		MultipleExpenseMngt entity=(MultipleExpenseMngt) presenter.getModel();
		
		String status=entity.getStatus();

		for(int i=0;i<getProcesslevelBarNames().length;i++)
		{
			InlineLabel label=getProcessLevelBar().btnLabels[i];
			String text=label.getText().trim();			
			
			if(status.equals(MultipleExpenseMngt.APPROVED))
			{
				if((text.contains(AppConstants.NEW))|| text.contains(AppConstants.RETURNEXPENSE) || text.contains(AppConstants.viewReturnAmtHistory))
					label.setVisible(true);
				else
					label.setVisible(false);
				
				if(LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN")){
					if((text.contains("Cancel Expense"))){
						label.setVisible(true);
					}
					
					if((text.contains("Update Customer"))){
						label.setVisible(true);
					}
				}
				else{
					if((text.contains("Cancel Expense"))){
						label.setVisible(false);
					}
				}
				
				if(text.equals(ManageApprovals.SUBMIT))
					label.setVisible(false);
			}

			else if(status.equals(MultipleExpenseMngt.REQUESTED))
			{
				if((text.contains(ManageApprovals.CANCELAPPROVALREQUEST)))
					label.setVisible(true);
				else
					label.setVisible(false);
				
				if((text.contains("Cancel Expense"))){
					label.setVisible(false);
				}
				/** Date 06-08-2017 added by vijay for return expense **/
				if(text.contains(AppConstants.RETURNEXPENSE)){
					label.setVisible(false);
				}
				
				if(text.contains(AppConstants.viewReturnAmtHistory)){
					label.setVisible(false);
				}
				
				if(text.equals(ManageApprovals.SUBMIT))
					label.setVisible(false);
			}
			
			else if(status.equals(MultipleExpenseMngt.REJECTED))
			{
				if(text.contains(AppConstants.NEW))
					label.setVisible(true);
				else
					label.setVisible(false);
				
				if((text.contains("Cancel Expense"))){
					label.setVisible(false);
				}
				
				/** Date 06-08-2017 added by vijay for return expense **/
				if(text.contains(AppConstants.RETURNEXPENSE)){
					label.setVisible(false);
				}
				
				if(text.contains(AppConstants.viewReturnAmtHistory)){
					label.setVisible(false);
				}
				if(text.equals(ManageApprovals.SUBMIT))
					label.setVisible(false);
			}
			
			else if(status.equals(MultipleExpenseMngt.CREATED))
			{
				if(text.contains("Migrate Record"))
					label.setVisible(true);
				
				if((text.contains("Cancel Expense"))){
					label.setVisible(false);
				}
				/** Date 06-08-2017 added by vijay for return expense **/
				if(text.contains(AppConstants.RETURNEXPENSE)){
					label.setVisible(false);
				}
				
				if(text.contains(AppConstants.viewReturnAmtHistory)){
					label.setVisible(false);
				}
			}
			else if(status.equals(MultipleExpenseMngt.CANCELLED))
			{
				if(text.contains(AppConstants.NEW))
					label.setVisible(true);
				else
					label.setVisible(false);
				
				/** Date 06-08-2017 added by vijay for return expense **/
				if(text.contains(AppConstants.RETURNEXPENSE)){
					label.setVisible(false);
				}
				
				if(text.contains(AppConstants.viewReturnAmtHistory)){
					label.setVisible(true);//Ashwini Patil Date:10-03-2023 made it true as per Ankita pest requirement
				}
				
				if(text.equals(ManageApprovals.SUBMIT))
					label.setVisible(false);
			}
		//	Console.log("in changeProcessLevel isHideNavigationButtons="+isHideNavigationButtons());
			
			//Ashwini Patil Date:10-07-2024
			if(isHideNavigationButtons()||isPopUpAppMenubar()) {
				Console.log("in changeProcessLevel isHideNavigationButtons");
				if(text.equalsIgnoreCase(AppConstants.NEW)){
					label.setVisible(false);	
				}
			}
			
		}
		
		
	}
	public void setAppHeaderBarAsPerStatus()
	{
		MultipleExpenseMngt entity=(MultipleExpenseMngt) presenter.getModel();
		String status=entity.getStatus();
		

		if(status.equals(MultipleExpenseMngt.APPROVED))
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			/**
			 * @author Anil @since 15-07-2021
			 * for summary screen
			 */
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Discard")||text.contains("Search")||text.contains("Print")||text.contains(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
			}
		}
		
		if(status.equals(MultipleExpenseMngt.REQUESTED))
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			/**
			 * @author Anil @since 15-07-2021
			 * for summary screen
			 */
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Discard")||text.contains("Search")||text.contains(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
			}
		}
		
		//Ashwini Patil Date:10-03-2023 Need to remove edit button after cancellation
		if(status.equals(MultipleExpenseMngt.CANCELLED))
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			/**
			 * @author Anil @since 15-07-2021
			 * for summary screen
			 */
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Discard")||text.contains("Search")||text.contains("Print")||text.contains(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
			}
		}
	}

	
	
	
	
	
	@Override
	public void setToViewState() {
		super.setToViewState();
		setAppHeaderBarAsPerStatus();
		toggleProcessLevelMenu();
		
		SuperModel model=new MultipleExpenseMngt();
		model=expenseobj;
		AppUtility.addDocumentToHistoryTable(AppConstants.ACCOUNTMODULE,AppConstants.MULTIPLEEXPENSEMANAGEMET, expenseobj.getCount(), null,null,null, false, model, null);
		
		String mainScreenLabel="EXPENSES";
		if(expenseobj!=null&&expenseobj.getCount()!=0){
			mainScreenLabel=expenseobj.getCount()+" "+"/"+expenseobj.getStatus()+" "+ "/"+AppUtility.parseDate(expenseobj.getCreationDate());
		}
		

		fgroupingExpenseDetails.getHeaderLabel().setText(mainScreenLabel);
	
	}

	@Override
	public void setToEditState() {
		super.setToEditState();
		this.processLevelBar.setVisibleFalse(false);
		changeStatusToCreated();
		
		String mainScreenLabel="EXPENSES";
		if(expenseobj!=null&&expenseobj.getCount()!=0){
			mainScreenLabel=expenseobj.getCount()+" "+"/"+expenseobj.getStatus()+" "+ "/"+AppUtility.parseDate(expenseobj.getCreationDate());
		}
		

		fgroupingExpenseDetails.getHeaderLabel().setText(mainScreenLabel);
	}

	private void changeStatusToCreated() {
		MultipleExpenseMngt entity=(MultipleExpenseMngt) presenter.getModel();
		String status=entity.getStatus();
		
		if(MultipleExpenseMngt.REJECTED.equals(status.trim()))
		{
			this.tbStatus.setValue(MultipleExpenseMngt.CREATED);
		}
	}

	/**
	 * sets the id textbox with the passed count value.
	 *
	 * @param count the new count
	 */
	@Override
	public void setCount(int count)
	{
		tbexpenseId.setValue(count+"");
		try{
			presenter.getModel().setCount(count);
		}catch(Exception e){
			
		}
	}
	
	@Override
	public boolean validate() {
		boolean superRes= super.validate();
		boolean exppettycash=true;
		boolean expenseDetails=true;
		
		if(this.cbexptype.getValue()==true&&this.olbpettycashname.getSelectedIndex()==0)
		{
			this.showDialogMessage("Petty Cash Name is Mandatory!");
			exppettycash=false;
		}
		
		if(this.isPettyCashNameFlag())
		{
			if(this.getOlbpettycashname().getSelectedIndex()==0)
			{
				this.showDialogMessage("Petty Cash Name is Mandatory!");
				exppettycash=false;
			}
			
		}
		
		if(this.expenseMngtTable.getDataprovider().getList().size()==0)
		{
			this.showDialogMessage("Please add expense details in table ..!");
			expenseDetails = false;
		}
		
		return superRes&&exppettycash&expenseDetails;
	}
	
	
	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen#clear()
	 */
	@Override
	public void clear()
	{
		super.clear();
		this.tbStatus.setText(MultipleExpenseMngt.CREATED);
		tbStatus.setEnabled(false);
	}
	
	@Override
	public void setEnable(boolean status)
	{
		super.setEnable(status);
		this.tbStatus.setEnabled(false);
		olbpettycashname.setEnabled(false);
		tbexpenseId.setEnabled(false);
		this.getTotalTotalAmt().setEnabled(false);
		tbremark.setEnabled(false);
	}
	
	
	/*******************************************Type Drop Down Logic**************************************/
	
	@Override
	public void onChange(ChangeEvent event) {
		if(event.getSource().equals(olbCategory))
		{
			if(olbCategory.getSelectedIndex()!=0){
				ConfigCategory cat=olbCategory.getSelectedItem();
				if(cat!=null){
					AppUtility.makeLiveTypeDropDown(olbType, cat.getCategoryName(), cat.getCategoryCode(), cat.getInternalType());
				}
			}
		}
		
//		if(event.getSource().equals(olbEmployee)){
//			if(olbEmployee.getSelectedIndex()!=0){
//				Employee employee=olbEmployee.getSelectedItem();
//				if(employee!=null){
//					oblEmployeeRole.setValue(employee.getRoleName());
//				}
//			}
//		}
	}
	
	
	/*********************************************Getters And Setters****************************************/

	/**
	 * Gets the db expense date.
	 *
	 * @return the db expense date
	 */
	public DateBox getDbExpenseDate() {
		return dbExpenseDate;
	}

	/**
	 * Sets the db expense date.
	 *
	 * @param dbExpenseDate the new db expense date
	 */
	public void setDbExpenseDate(DateBox dbExpenseDate) {
		this.dbExpenseDate = dbExpenseDate;
	}

	/**
	 * Gets the olb branch.
	 *
	 * @return the olb branch
	 */
	public ObjectListBox<Branch> getOlbBranch() {
		return olbBranch;
	}

	/**
	 * Sets the olb branch.
	 *
	 * @param olbBranch the new olb branch
	 */
	public void setOlbBranch(ObjectListBox<Branch> olbBranch) {
		this.olbBranch = olbBranch;
	}

	/**
	 * Gets the olb currency.
	 *
	 * @return the olb currency
	 */
	public ObjectListBox<Config> getOlbCurrency() {
		return olbCurrency;
	}

	/**
	 * Sets the olb currency.
	 *
	 * @param olbCurrency the new olb currency
	 */
	public void setOlbCurrency(ObjectListBox<Config> olbCurrency) {
		this.olbCurrency = olbCurrency;
	}

	/**
	 * Gets the olb payment method.
	 *
	 * @return the olb payment method
	 */
	public ObjectListBox<Config> getOlbPaymentMethod() {
		return olbPaymentMethod;
	}

	/**
	 * Sets the olb payment method.
	 *
	 * @param olbPaymentMethod the new olb payment method
	 */
	public void setOlbPaymentMethod(ObjectListBox<Config> olbPaymentMethod) {
		this.olbPaymentMethod = olbPaymentMethod;
	}

	/**
	 * Gets the olb expense group.
	 *
	 * @return the olb expense group
	 */
	public ObjectListBox<Config> getOlbExpenseGroup() {
		return olbExpenseGroup;
	}

	/**
	 * Sets the olb expense group.
	 *
	 * @param olbExpenseGroup the new olb expense group
	 */
	public void setOlbExpenseGroup(ObjectListBox<Config> olbExpenseGroup) {
		this.olbExpenseGroup = olbExpenseGroup;
	}

	/**
	 * Gets the olb category.
	 *
	 * @return the olb category
	 */
	public ObjectListBox<ConfigCategory> getOlbCategory() {
		return olbCategory;
	}

	/**
	 * Sets the olb category.
	 *
	 * @param olbCategory the new olb category
	 */
	public void setOlbCategory(ObjectListBox<ConfigCategory> olbCategory) {
		this.olbCategory = olbCategory;
	}

	/**
	 * Gets the olb type.
	 *
	 * @return the olb type
	 */
	public ObjectListBox<Type> getOlbType() {
		return olbType;
	}

	/**
	 * Sets the olb type.
	 *
	 * @param olbType the new olb type
	 */
	public void setOlbType(ObjectListBox<Type> olbType) {
		this.olbType = olbType;
	}

	/**
	 * Gets the tb invoice no.
	 *
	 * @return the tb invoice no
	 */
	public TextBox getTbInvoiceNo() {
		return tbInvoiceNo;
	}

	/**
	 * Sets the tb invoice no.
	 *
	 * @param tbInvoiceNo the new tb invoice no
	 */
	public void setTbInvoiceNo(TextBox tbInvoiceNo) {
		this.tbInvoiceNo = tbInvoiceNo;
	}

	/**
	 * Gets the tb vendor.
	 *
	 * @return the tb vendor
	 */
	public TextBox getTbVendor() {
		return tbVendor;
	}

	/**
	 * Sets the tb vendor.
	 *
	 * @param tbVendor the new tb vendor
	 */
	public void setTbVendor(TextBox tbVendor) {
		this.tbVendor = tbVendor;
	}

	/**
	 * Gets the tb status.
	 *
	 * @return the tb status
	 */
	public TextBox getTbStatus() {
		return tbStatus;
	}

	/**
	 * Sets the tb status.
	 *
	 * @param tbStatus the new tb status
	 */
	public void setTbStatus(TextBox tbStatus) {
		this.tbStatus = tbStatus;
	}

	/**
	 * Gets the ta description.
	 *
	 * @return the ta description
	 */
	public TextArea getTaDescription() {
		return taDescription;
	}

	/**
	 * Sets the ta description.
	 *
	 * @param taDescription the new ta description
	 */
	public void setTaDescription(TextArea taDescription) {
		this.taDescription = taDescription;
	}

	/**
	 * Gets the uc choose file.
	 *
	 * @return the uc choose file
	 */
	public UploadComposite getUcChooseFile() {
		return ucChooseFile;
	}

	/**
	 * Sets the uc choose file.
	 *
	 * @param ucChooseFile the new uc choose file
	 */
	public void setUcChooseFile(UploadComposite ucChooseFile) {
		this.ucChooseFile = ucChooseFile;
	}

	/**
	 * Gets the db amount.
	 *
	 * @return the db amount
	 */
	public DoubleBox getDbAmount() {
		return dbAmount;
	}

	/**
	 * Sets the db amount.
	 *
	 * @param dbAmount the new db amount
	 */
	public void setDbAmount(DoubleBox dbAmount) {
		this.dbAmount = dbAmount;
	}

	/**
	 * Gets the olb employee.
	 *
	 * @return the olb employee
	 */
//	public ObjectListBox<Employee> getOlbEmployee() {
//		return olbEmployee;
//	}
//
//	/**
//	 * Sets the olb employee.
//	 *
//	 * @param olbEmployee the new olb employee
//	 */
//	public void setOlbEmployee(ObjectListBox<Employee> olbEmployee) {
//		this.olbEmployee = olbEmployee;
//	}

	/**
	 * Gets the olb approver name.
	 *
	 * @return the olb approver name
	 */
	public ObjectListBox<Employee> getOlbApproverName() {
		return olbApproverName;
	}

	/**
	 * Sets the olb approver name.
	 *
	 * @param olbApproverName the new olb approver name
	 */
	public void setOlbApproverName(ObjectListBox<Employee> olbApproverName) {
		this.olbApproverName = olbApproverName;
	}

	public CheckBox getCbexptype() {
		return cbexptype;
	}

	public void setCbexptype(CheckBox cbexptype) {
		this.cbexptype = cbexptype;
	}

	public ObjectListBox<PettyCash> getOlbpettycashname() {
		return olbpettycashname;
	}

	public void setOlbpettycashname(ObjectListBox<PettyCash> olbpettycashname) {
		this.olbpettycashname = olbpettycashname;
	}

	public TextBox getTbremark() {
		return tbremark;
	}

	public void setTbremark(TextBox tbremark) {
		this.tbremark = tbremark;
	}

	/* (non-Javadoc)
	 * @see com.slicktechnologies.client.approvalutility.ApprovableScreen#getstatustextbox()
	 */
	@Override
	public TextBox getstatustextbox() {
	
		return this.tbStatus;
	}

	public TextBox getTbServiceId() {
		return tbServiceId;
	}

	public void setTbServiceId(TextBox tbServiceId) {
		this.tbServiceId = tbServiceId;
	}

	public boolean isPettyCashNameFlag() {
		return pettyCashNameFlag;
	}

	public void setPettyCashNameFlag(boolean pettyCashNameFlag) {
		this.pettyCashNameFlag = pettyCashNameFlag;
	}


	@Override
	public void onClick(ClickEvent event) {
		if(event.getSource().equals(this.addDetails))
		{
			Console.log("add button clicked");
			/** sagar sore showing empty field names in dialog message  **/
			String validate=validateForFields();
			/**
			 * @author Anil , Date : 26-08-2019
			 */
//			if(this.getOlbEmployee().getSelectedIndex()==0) 
//			{
//				showDialogMessage("Please select employee");
//			}
			
			if(personInfo.getValue()==null||personInfo.getEmployeeId()==-1){
				showDialogMessage("Please select employee");
			}
			/**old code 22-09-2017 below line commented by sagar sore***/ 
			//else if(validateForTable()== false){
			else if(validate.equals("true")){	
				/**old code 14/10/2017 commented by komal */
//				if(validateEmployee()){
//					addDetailsToxpenseMngtTable();
//				}	
				/**Updated code**/
				/**Date 14/10/2017 added by komal for Allow Multiple Employee in Expense process type **/
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("MultipleExpenseMngt","AllowMultipleEmployeeInExpense"))
				{	        
//					addDetailsToxpenseMngtTable();
					validateExpenseLimit();
				}
				else
				{			
					//Date 17-08-2017 added by vijay as per need this by rohan
					if(validateEmployee()){
//						addDetailsToxpenseMngtTable();
						validateExpenseLimit();
					}
				}

			}
			else
			{
				/** old code22-09-2017 commented by sagar sore **/ 
//				showDialogMessage("Please select table fields to add in the table..! ");
				/** 22-09-2017 sagar sore To show field names in dialog message**/
				showDialogMessage(validate.substring(0, validate.length()-1));
			}

		}
	}

	
	/**
	 * Date 17-08-2017 added by vijay for employee must add to multiple expense 
	 * and we can not add different employee in expense table
	 * @return
	 */

	private boolean validateEmployee() {
		List<ExpenseManagement> list = expenseMngtTable.getDataprovider().getList();
		for(int i=0;i<list.size();i++){
//			String employeeName = olbEmployee.getValue(olbEmployee.getSelectedIndex());
			String employeeName = personInfo.getEmployeeName();
			if(!employeeName.equals(expenseMngtTable.getDataprovider().getList().get(i).getEmployee())){
				showDialogMessage("Can not add different employee");
				return false;
			}else{
				return true;
			}
		}
		
		return true;
	}
	
	/*
	 * 22-07-2017
	 * by sagar sore [this implemented for catching field names to show them in dialog message]
	 * 
	*/
	private String validateForFields() {
		String field="Please fill following field\n";
		
		/**
		 * @author Anil , Date : 23-08-2019
		 * Making invoice number non mandatory
		 */
//		if((this.getTbInvoiceNo().getValue().equals("")))
//		{	    	
//			field+=" Invoice no,\n";	    
//		}
		/**
		 * @author Anil , Date : 29-09-2021
		 * Making Expense group non mandatory
		 * As per the nitin sir instruction
		 */
//	    if(this.getOlbExpenseGroup().getSelectedIndex()==0)
//		{
//			field+=" Expense Group,\n";
//		}
		if(this.getOlbCategory().getSelectedIndex()==0)
		{
			field+=" Expense Category,\n";
		}
		 if(this.getDbExpenseDate().getValue()==null)
		{
			field+=" Expense date,";
		}
	    if(this.getDbAmount().getValue()==null)
		{
			field+=" Amount,\n";
		}
	    
	    
		if(field.trim().equals("Please fill following field"))
		{
			field="true";
		}
		return field;
	}

	private boolean validateForTable() {
	
	if(this.olbCategory.getSelectedIndex()==0)
	{
		return true;
	}
	else if(this.getOlbCategory().getSelectedIndex()==0)
	{
		return true;
	}
	else if(this.getOlbExpenseGroup().getSelectedIndex()==0)
	{
		return true;
	}
	else if(this.getOlbType().getSelectedIndex()==0)
	{
		return true;
	}
	//Date 17-08-2017 commented by vijay

//	else if(this.getOlbEmployee().getSelectedIndex()==0)
//	{
//		return true;
//	}
	else if(this.getDbAmount().getValue()==0)
	{
		return true;
	}
	else if(this.getTbInvoiceNo().getValue()==null && this.getTbInvoiceNo().equals(""))
	{
		return true;
	}
	
	return false;
}

private void addDetailsToxpenseMngtTable() {
	
	ExpenseManagement exp = new ExpenseManagement();
	
	if(this.getTbInvoiceNo().getValue().trim()!=null && !this.getTbInvoiceNo().getValue().trim().equals(""))
	{
		exp.setInvoiceNumber(this.getTbInvoiceNo().getValue().trim());
	}
	else
	{
		exp.setInvoiceNumber("NA");
	}
	
	if(getOlbExpenseGroup().getSelectedIndex()!=0&&this.getOlbExpenseGroup().getValue(getOlbExpenseGroup().getSelectedIndex()).trim()!=null && !this.getOlbExpenseGroup().getValue(getOlbExpenseGroup().getSelectedIndex()).trim().equals(""))
	{
		exp.setExpenseGroup(this.getOlbExpenseGroup().getValue(getOlbExpenseGroup().getSelectedIndex()).trim());
	}
	else
	{
		exp.setExpenseGroup(" ");
	}
	
	
	if(this.getOlbCategory().getValue(getOlbCategory().getSelectedIndex()).trim()!=null && !this.getOlbCategory().getValue(getOlbCategory().getSelectedIndex()).trim().equals(""))
	{
		exp.setExpenseCategory(this.getOlbCategory().getValue(getOlbCategory().getSelectedIndex()).trim());
	}
	else
	{
		exp.setExpenseCategory("NA");
	}
	
	if(getOlbType().getSelectedIndex()!=0&&this.getOlbType().getValue(getOlbType().getSelectedIndex()).trim()!=null && !this.getOlbType().getValue(getOlbType().getSelectedIndex()).trim().equals(""))
	{
		exp.setExpenseType(this.getOlbType().getValue(getOlbType().getSelectedIndex()).trim());
	}
	else
	{
		exp.setExpenseType("");
	}
	
	if(this.getDbExpenseDate().getValue()!=null)
	{
		exp.setExpenseDate(this.getDbExpenseDate().getValue());
	}
	/**
	 * @author Anil , Date : 26-08-2019
	 */
//	if(this.getOlbEmployee().getValue(getOlbEmployee().getSelectedIndex()).trim()!=null && !this.getOlbEmployee().getValue(getOlbEmployee().getSelectedIndex()).trim().equals(""))
//	{
//		exp.setEmployee(this.getOlbEmployee().getValue(getOlbEmployee().getSelectedIndex()).trim());
//	}
//	else
//	{
//		exp.setEmployee("NA");
//	}
	if(personInfo.getValue()!=null){
		exp.setEmployee(personInfo.getValue().getFullName());
		exp.setEmployeeId(personInfo.getValue().getEmpCount());
		exp.setEmployeeCell(personInfo.getValue().getCellNumber());
	}else{
		exp.setEmployee("NA");
	}
	
	if(this.getDbAmount().getValue()!=null && this.getDbAmount().getValue()!=0)
	{
		exp.setAmount(this.getDbAmount().getValue());
	}
	
	if(this.getOlbCurrency().getValue(getOlbCurrency().getSelectedIndex()).trim()!=null && !this.getOlbCurrency().getValue(getOlbCurrency().getSelectedIndex()).trim().equals(""))
	{
		exp.setCurrency(this.getOlbCurrency().getValue(getOlbCurrency().getSelectedIndex()).trim());
	}
	else
	{
		exp.setCurrency("NA");
	}
	
	if(this.getOlbPaymentMethod().getValue(getOlbPaymentMethod().getSelectedIndex()).trim()!=null && !this.getOlbPaymentMethod().getValue(getOlbPaymentMethod().getSelectedIndex()).trim().equals(""))
	{
		exp.setPaymentMethod(this.getOlbPaymentMethod().getValue(getOlbPaymentMethod().getSelectedIndex()).trim());
	}
	else
	{
		exp.setCurrency("NA");
	}
	
	if(this.getTbVendor().getValue().trim()!=null && !this.getTbVendor().getValue().trim().equals(""))
	{
		exp.setVendor(this.getTbVendor().getValue());
	}
	else
	{
		exp.setVendor("NA");
	}
	
	/**
	 * @author Anil , Date : 23-08-2019
	 */
//	if(dbFromDate.getValue()!=null){
//		exp.setFromDate(dbFromDate.getValue());
//	}
//	if(oblFromCity.getValue()!=null){
//		exp.setFromCity(oblFromCity.getValue());
//	}
//	if(dbToDate.getValue()!=null){
//		exp.setToDate(dbToDate.getValue());
//	}
//	if(oblToCity.getValue()!=null){
//		exp.setToCity(oblToCity.getValue());
//	}
//	if(dbKmTravelled.getValue()!=null){
//		exp.setKmTravelled(dbKmTravelled.getValue());
//	}

//Ashwini Patil Date:3-01-2023
	if(oblEmployeeRole.getValue()!=null){
		exp.setEmployeeRole(oblEmployeeRole.getValue());
	}
	
	if(travelFromDate!=null){
	exp.setFromDate(travelFromDate);
	}
	if(travelFromCity!=null){
		exp.setFromCity(travelFromCity);
	}
	if(travelToDate!=null){
		exp.setToDate(travelToDate);
	}
	if(travelToCity!=null){
		exp.setToCity(travelToCity);
	}
	if(travelKM!=0){
		exp.setKmTravelled(travelKM);
	}
 
	/**
	 * End
	 */
	expenseMngtTable.getDataprovider().getList().add(exp);
	Console.log("expense added to expenseMngtTable");
	
	travelKM=0;
	travelFromDate=null;
	travelToDate=null;
	travelFromCity=null;
	travelToCity=null;
	Console.log("travel values cleared");
}



public DoubleBox getTotalTotalAmt() {
	return totalTotalAmt;
}

public void setTotalTotalAmt(DoubleBox totalTotalAmt) {
	this.totalTotalAmt = totalTotalAmt;
}

@Override
public void setToNewState() {
	super.setToNewState();
	
//	System.out.println("in side new state ");
//	this.processLevelBar.setVisibleFalse(false);
}
	
	public void validateExpenseLimit(){
		Console.log("validateExpenseLimit");
		if(oblEmployeeRole.getSelectedIndex()==0
				||olbExpenseGroup.getSelectedIndex()==0
				||olbCategory.getSelectedIndex()==0
				||oblToCity.getSelectedIndex()==0
				||dbAmount.getValue()==null){
			addDetailsToxpenseMngtTable();
			return;
		}
		
		String className="";
		City arrivalCity=oblToCity.getSelectedItem();
		if(arrivalCity.getClassName()==null||arrivalCity.getClassName().equals("")){
			addDetailsToxpenseMngtTable();
			return;
		}else{
			className=arrivalCity.getClassName();	
		}
		GenricServiceAsync async = GWT.create(GenricService.class);
		MyQuerry querry=new MyQuerry();
		Filter temp=null;
		Vector<Filter> filterVec=new Vector<Filter>();
		
		temp=new Filter();
		temp.setQuerryString("cityClass");
		temp.setStringValue(className);
		filterVec.add(temp);
		
		temp=new Filter();
		temp.setQuerryString("employeeRole");
		temp.setStringValue(oblEmployeeRole.getValue());
		filterVec.add(temp);
		
		temp=new Filter();
		temp.setQuerryString("expenseGrp");
		temp.setStringValue(olbExpenseGroup.getValue());
		filterVec.add(temp);
		
		temp=new Filter();
		temp.setQuerryString("expenseCategory");
		temp.setStringValue(olbCategory.getValue());
		filterVec.add(temp);
		
		querry.setFilters(filterVec);
		querry.setQuerryObject(new ExpensePolicies());
		showWaitSymbol();
		
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				hideWaitSymbol();
				
				if(result!=null&&result.size()!=0){
					ExpensePolicies expPolicies=(ExpensePolicies) result.get(0);
					if(expPolicies.isAtActual()==true){
						if(expPolicies.getExpenseLimit()!=0){
							if(dbAmount.getValue()>expPolicies.getExpenseLimit()){
								showDialogMessage("Expense limit is exceeding.\nLimit is "+expPolicies.getExpenseLimit());
								return;
							}
						}else{
							addDetailsToxpenseMngtTable();;
						}
						
					}else if(expPolicies.isAtActual()==false&&expPolicies.isPerKm()==false){
						if(dbAmount.getValue()>expPolicies.getExpenseLimit()){
							showDialogMessage("Expense limit is exceeding.\nLimit is "+expPolicies.getExpenseLimit());
							return;
						}
						
					}else if(expPolicies.isPerKm()==true){
						if(dbKmTravelled.getValue()==null||dbKmTravelled.getValue()==0){
							showDialogMessage("Please add km travelled.");
							return;
						}
						double expenseAmount=dbKmTravelled.getValue()*expPolicies.getRatePerKm();
						if(dbAmount.getValue()>expenseAmount){
							showDialogMessage("Expense limit is exceeding.\nLimit is "+expenseAmount);
							return;
						}
					}
					addDetailsToxpenseMngtTable();
					
				}else{
					addDetailsToxpenseMngtTable();
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				hideWaitSymbol();
			}
		});
		
		
	}

	@Override
	public void onSelection(SelectionEvent<Suggestion> event) {
		// TODO Auto-generated method stub
		if(event.getSource()==personInfo.getId()||event.getSource()==personInfo.getName()||event.getSource()==personInfo.getPhone()){
			EmployeeInfo info=personInfo.getEmpentity();
			if(info!=null){
				oblEmployeeRole.setValue(info.getEmployeerole());
			}
		}
	}

	public EmployeeInfoComposite getPersonInfo() {
		return personInfo;
	}

	public void setPersonInfo(EmployeeInfoComposite personInfo) {
		this.personInfo = personInfo;
	}
	
	

	public Button getBtnNewCustomer1() {
		return btnNewCustomer1;
	}

	public void setBtnNewCustomer1(Button btnNewCustomer1) {
		this.btnNewCustomer1 = btnNewCustomer1;
	}

	public Button getBtnNewCustomer2() {
		return btnNewCustomer2;
	}

	public void setBtnNewCustomer2(Button btnNewCustomer2) {
		this.btnNewCustomer2 = btnNewCustomer2;
	}

	
	@Override
	public void refreshTableData() {
		// TODO Auto-generated method stub
		super.refreshTableData();
		expenseMngtTable.getTable().redraw();
	}
	
	
	
}
