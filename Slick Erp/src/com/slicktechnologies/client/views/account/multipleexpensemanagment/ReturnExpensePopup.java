package com.slicktechnologies.client.views.account.multipleexpensemanagment;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;

public class ReturnExpensePopup extends PopupScreen{
	
	DoubleBox dbexpenseReturnAmt;
	IntegerBox ibsample;
	
	public ReturnExpensePopup(){
		super();
		createGui();
	}

	private void initilizewidget() {
		
		dbexpenseReturnAmt = new DoubleBox();
		ibsample = new IntegerBox();
		
	}
	
	@Override
	public void createScreen() {
		initilizewidget();
		
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingExpenseDetails=fbuilder.setlabel("Expense Details").widgetType(FieldType.GREYGROUPING).setMandatory(false).setColSpan(5).build();
		
		fbuilder = new FormFieldBuilder("Expense Return Amount",dbexpenseReturnAmt);
		FormField fdbexpenseReturnAmt= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		
		FormField[][] formfield = {
				{fgroupingExpenseDetails},
				{fdbexpenseReturnAmt}
		};


		this.fields=formfield;		

	}

	@Override
	public void onClick(ClickEvent event) {

	}


}
