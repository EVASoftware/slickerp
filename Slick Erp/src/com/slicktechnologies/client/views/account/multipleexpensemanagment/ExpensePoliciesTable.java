package com.slicktechnologies.client.views.account.multipleexpensemanagment;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.businessprocesslayer.ExpensePolicies;

public class ExpensePoliciesTable extends SuperTable<ExpensePolicies>{
	TextColumn<ExpensePolicies> getCountColumn;
	TextColumn<ExpensePolicies> getCityClassNameColumn;
	TextColumn<ExpensePolicies> getEmployeeRoleColumn;
	TextColumn<ExpensePolicies> getExpenseGroupsColumn;
	TextColumn<ExpensePolicies> getExpenseCaltegoryColumn;
	TextColumn<ExpensePolicies> getStatusColumn;
	  
	public ExpensePoliciesTable() {
		super();
	}
	  
	@Override
	public void createTable() {
		addColumngetCount();
		getEmployeeRoleColumn();
		getCityClassNameColumn();
		getExpenseGroupsColumn();
		getExpenseCaltegoryColumn();
		addColumngetStatus();
	}

	@Override
	protected void initializekeyprovider() {
		keyProvider = new ProvidesKey<ExpensePolicies>() {
			@Override
			public Object getKey(ExpensePolicies item) {
				if (item == null) {
					return null;
				} else
					return item.getId();
			}
		};
	}

	@Override
	public void setEnable(boolean state) {
	}

	@Override
	public void applyStyle() {
	}

	@Override
	public void addFieldUpdater() {
	}
	
	protected void addColumngetCount() {
		getCountColumn = new TextColumn<ExpensePolicies>() {
			@Override
			public String getValue(ExpensePolicies object) {
				if (object.getCount() == -1)
					return "N.A";
				else
					return object.getCount() + "";
			}
		};
		table.addColumn(getCountColumn, "Id");
		getCountColumn.setSortable(true);
	}
	
	protected void getCityClassNameColumn() {
		getCityClassNameColumn = new TextColumn<ExpensePolicies>() {
			@Override
			public String getValue(ExpensePolicies object) {
				return object.getCityClass() + "";
			}
		};
		table.addColumn(getCityClassNameColumn, "Class");
		getCityClassNameColumn.setSortable(true);
	}

	

	protected void getEmployeeRoleColumn() {
		getEmployeeRoleColumn = new TextColumn<ExpensePolicies>() {
			@Override
			public String getValue(ExpensePolicies object) {
				return object.getEmployeeRole() + "";
			}
		};
		table.addColumn(getEmployeeRoleColumn, "Employee Role");
		getEmployeeRoleColumn.setSortable(true);
	}
	
	private void getExpenseGroupsColumn() {
		getExpenseGroupsColumn = new TextColumn<ExpensePolicies>() {
			@Override
			public String getValue(ExpensePolicies object) {
				return object.getExpenseGrp() + "";
			}
		};
		table.addColumn(getExpenseGroupsColumn, "Expense Group");
		getExpenseGroupsColumn.setSortable(true);
		
	}
	
	private void getExpenseCaltegoryColumn() {
		getExpenseCaltegoryColumn = new TextColumn<ExpensePolicies>() {
			@Override
			public String getValue(ExpensePolicies object) {
				return object.getExpenseCategory() + "";
			}
		};
		table.addColumn(getExpenseCaltegoryColumn, "Expense Category");
		getExpenseCaltegoryColumn.setSortable(true);
		
	}
	

	protected void addColumngetStatus() {
		getStatusColumn = new TextColumn<ExpensePolicies>() {
			@Override
			public String getValue(ExpensePolicies object) {
				if (object.isStatus() == true)
					return "Active";
				else
					return "In Active";
			}
		};
		table.addColumn(getStatusColumn, "Status");
		getStatusColumn.setSortable(true);
	}
	
	public void addColumnSorting() {
		addSortinggetCount();
		addSortinggetName();
		addSortingRateCol();
		addSortingStatus();
	}

	

	protected void addSortinggetCount() {
		List<ExpensePolicies> list = getDataprovider().getList();
		columnSort = new ListHandler<ExpensePolicies>(list);
		columnSort.setComparator(getCountColumn, new Comparator<ExpensePolicies>() {
			@Override
			public int compare(ExpensePolicies e1, ExpensePolicies e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCount() == e2.getCount()) {
						return 0;
					}
					if (e1.getCount() > e2.getCount()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	

	protected void addSortinggetName() {
		List<ExpensePolicies> list = getDataprovider().getList();
		columnSort = new ListHandler<ExpensePolicies>(list);
		columnSort.setComparator(getCityClassNameColumn, new Comparator<ExpensePolicies>() {
			@Override
			public int compare(ExpensePolicies e1, ExpensePolicies e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCityClass() != null && e2.getCityClass() != null) {
						return e1.getCityClass().compareTo(e2.getCityClass());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	protected void addSortingRateCol() {
		List<ExpensePolicies> list = getDataprovider().getList();
		columnSort = new ListHandler<ExpensePolicies>(list);
		columnSort.setComparator(getEmployeeRoleColumn,new Comparator<ExpensePolicies>() {
			
			@Override
			public int compare(ExpensePolicies e1, ExpensePolicies e2) {
				if (e1 != null && e2 != null) {
					if (e1.getEmployeeRole() != null && e2.getEmployeeRole() != null) {
						return e1.getEmployeeRole().compareTo(e2.getEmployeeRole());
					}
				} else {
					return 0;
				}
				return 0;
			}
			
		});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortingStatus() {
		List<ExpensePolicies> list = getDataprovider().getList();
		columnSort = new ListHandler<ExpensePolicies>(list);
		columnSort.setComparator(getStatusColumn, new Comparator<ExpensePolicies>() {
			@Override
			public int compare(ExpensePolicies e1, ExpensePolicies e2) {
				if (e1 != null && e2 != null) {
					if (e1.isStatus() == true)
						return 1;
					else
						return -1;

				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortingExpGrpCol() {
		List<ExpensePolicies> list = getDataprovider().getList();
		columnSort = new ListHandler<ExpensePolicies>(list);
		columnSort.setComparator(getExpenseGroupsColumn,new Comparator<ExpensePolicies>() {
			
			@Override
			public int compare(ExpensePolicies e1, ExpensePolicies e2) {
				if (e1 != null && e2 != null) {
					if (e1.getExpenseGrp() != null && e2.getExpenseGrp() != null) {
						return e1.getExpenseGrp().compareTo(e2.getExpenseGrp());
					}
				} else {
					return 0;
				}
				return 0;
			}
			
		});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortingExpCatCol() {
		List<ExpensePolicies> list = getDataprovider().getList();
		columnSort = new ListHandler<ExpensePolicies>(list);
		columnSort.setComparator(getExpenseCaltegoryColumn,new Comparator<ExpensePolicies>() {
			
			@Override
			public int compare(ExpensePolicies e1, ExpensePolicies e2) {
				if (e1 != null && e2 != null) {
					if (e1.getExpenseCategory() != null && e2.getExpenseCategory() != null) {
						return e1.getExpenseCategory().compareTo(e2.getExpenseCategory());
					}
				} else {
					return 0;
				}
				return 0;
			}
			
		});
		table.addColumnSortHandler(columnSort);
	}

}
