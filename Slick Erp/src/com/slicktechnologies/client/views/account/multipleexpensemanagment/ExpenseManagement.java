package com.slicktechnologies.client.views.account.multipleexpensemanagment;

import java.util.Date;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;


/**
 * Developed By :Rohan Bhagde 
 * Date         :29/11/2016
 * Reason       :This is user for multiple expenses against single invoice 
 * Required by  :NBHC ( vaishali mam )
 */

@Embed
public class ExpenseManagement extends SuperModel{

	private static final long serialVersionUID = -5839441048452543695L;
	@Index
	String invoiceNumber;
	@Index
	String expenseGroup;
	@Index
	String expenseCategory;
	@Index
	String expenseType;
	@Index
	Date expenseDate;
	@Index
	String employee;
	double amount;
	String currency;
	@Index
	String paymentMethod;
	@Index
	String vendor;
	
	
	/**
	 * @author Anil , Date :23-08-2019
	 */
	
	@Index
	Date fromDate;
	@Index
	Date toDate;
	@Index
	String fromCity;
	@Index 
	String toCity;
	
	double kmTravelled;
	
	@Index
	String employeeRole;
	
	String remark;
	
	@Index
	int employeeId;
	@Index
	long employeeCell;
	
	/***********************************************  Getters and setters   *****************************************/
	
	public int getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}
	public long getEmployeeCell() {
		return employeeCell;
	}
	public void setEmployeeCell(long employeeCell) {
		this.employeeCell = employeeCell;
	}
	
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	public String getExpenseGroup() {
		return expenseGroup;
	}
	public void setExpenseGroup(String expenseGroup) {
		this.expenseGroup = expenseGroup;
	}
	public String getExpenseCategory() {
		return expenseCategory;
	}
	public void setExpenseCategory(String expenseCategory) {
		this.expenseCategory = expenseCategory;
	}
	public String getExpenseType() {
		return expenseType;
	}
	public void setExpenseType(String expenseType) {
		this.expenseType = expenseType;
	}
	public Date getExpenseDate() {
		return expenseDate;
	}
	public void setExpenseDate(Date expenseDate) {
		this.expenseDate = expenseDate;
	}
	public String getEmployee() {
		return employee;
	}
	public void setEmployee(String employee) {
		this.employee = employee;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public String getVendor() {
		return vendor;
	}
	public void setVendor(String vendor) {
		this.vendor = vendor;
	}
	
	
	
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date dbFromDate) {
		this.fromDate = dbFromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date dbToDate) {
		this.toDate = dbToDate;
	}
	public String getFromCity() {
		return fromCity;
	}
	public void setFromCity(String fromCity) {
		this.fromCity = fromCity;
	}
	public String getToCity() {
		return toCity;
	}
	public void setToCity(String toCity) {
		this.toCity = toCity;
	}
	public double getKmTravelled() {
		return kmTravelled;
	}
	public void setKmTravelled(double kmTravelled) {
		this.kmTravelled = kmTravelled;
	}
	public String getEmployeeRole() {
		return employeeRole;
	}
	public void setEmployeeRole(String employeeRole) {
		this.employeeRole = employeeRole;
	}
	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}
	
	

	
	
	
}
