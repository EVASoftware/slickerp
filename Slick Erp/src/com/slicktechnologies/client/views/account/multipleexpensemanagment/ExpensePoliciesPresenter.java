package com.slicktechnologies.client.views.account.multipleexpensemanagment;

import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.shared.common.businessprocesslayer.ExpensePolicies;

public class ExpensePoliciesPresenter extends FormTableScreenPresenter<ExpensePolicies> {
	ExpensePoliciesForm form;
	final GenricServiceAsync async = GWT.create(GenricService.class);
	public ExpensePoliciesPresenter(FormTableScreen<ExpensePolicies> view,ExpensePolicies model) {
		super(view, model);
		form = (ExpensePoliciesForm) view;
		form.getSupertable().connectToLocal();
		form.retriveTable(getExpPoliciesQuery());
		form.setPresenter(this);
	}
	
	public MyQuerry getExpPoliciesQuery() {
		MyQuerry quer = new MyQuerry(new Vector<Filter>(), new ExpensePolicies());
		return quer;
	}
	
	public static void initalize() {
		ExpensePoliciesTable gentableScreen = new ExpensePoliciesTable();
		ExpensePoliciesForm form = new ExpensePoliciesForm(gentableScreen,FormTableScreen.UPPER_MODE, true);
		gentableScreen.setView(form);
		gentableScreen.applySelectionModle();
		ExpensePoliciesPresenter presenter = new ExpensePoliciesPresenter(form,new ExpensePolicies());
		AppMemory.getAppMemory().stickPnel(form);

	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel lbl = (InlineLabel) e.getSource();
		if (lbl.getText().contains("New")) {
			form.setToNewState();
			this.initalize();
		}
	}

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void makeNewModel() {
		model = new ExpensePolicies();
	}

}
