package com.slicktechnologies.client.views.account.paymentdashboard;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CommunicationLogService;
import com.slicktechnologies.client.services.CommunicationLogServiceAsync;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.dashboard.leaddashboard.CommunicationSmartPoup;
import com.slicktechnologies.client.views.dashboard.leaddashboard.LeadDashboardForm;
import com.slicktechnologies.client.views.dashboard.leaddashboard.LeadDashboardPresenter;
import com.slicktechnologies.client.views.dashboard.leaddashboard.LeadDashboardSearchForm;
import com.slicktechnologies.client.views.device.DeviceForm;
import com.slicktechnologies.client.views.device.DevicePresenter;
import com.slicktechnologies.client.views.lead.LeadForm;
import com.slicktechnologies.client.views.lead.LeadPresenter;
import com.slicktechnologies.client.views.paymentinfo.paymentdetails.PaymentDetailsForm;
import com.slicktechnologies.client.views.paymentinfo.paymentdetails.PaymentDetailsPresenter;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;


public class PaymentdashboardPresenter implements ClickHandler,CellClickHandler{

	PaymentdashboardForm form;
	PaymentDashboardSearchForm searchForm;
	
	GenricServiceAsync service=GWT.create(GenricService.class);
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	ArrayList<CustomerPayment> globalCustomerPaymentList=new ArrayList<CustomerPayment>();

	CommunicationSmartPoup commSmrtPopup=new CommunicationSmartPoup();
	CommunicationLogServiceAsync communicationService = GWT.create(CommunicationLogService.class);
	
	CustomerPayment paymentEntity=null;
	ListGridRecord selectedRecord=null;
	int position;
	
	protected AppMemory mem;
	
	public PaymentdashboardPresenter(PaymentdashboardForm homeForm,PaymentDashboardSearchForm searchpopup) {
		this.form=homeForm;
		this.searchForm=searchpopup;
		form.grid.addCellClickHandler(this);
		mem=AppMemory.getAppMemory();
		setEventHandeling();
//		retrieveLeadData();//Ashwini Patil Date:23-08-2024 to stop auto loading

	}
	
	private void setEventHandeling() {
		form.getBtnGo().addClickHandler(this);
		searchForm.golbl.addClickHandler(this);
		
		commSmrtPopup.getBtnOk().addClickHandler(this);
		commSmrtPopup.getBtnCancel().addClickHandler(this);
		commSmrtPopup.getBtnAdd().addClickHandler(this);
		
		InlineLabel label[] = AppMemory.getAppMemory().skeleton.getMenuLabels();
		for (int k = 0; k < label.length; k++) {
			if (AppMemory.getAppMemory().skeleton.registration[k] != null)
				AppMemory.getAppMemory().skeleton.registration[k].removeHandler();
		}
		for (int k = 0; k < label.length; k++) {
			AppMemory.getAppMemory().skeleton.registration[k] = label[k].addClickHandler(this);
		}
	}

	public static void initialize(){
		System.out.println("INSIDE CHART INITIALIZATION");
		PaymentdashboardForm homeForm = new PaymentdashboardForm();
		AppMemory.getAppMemory().currentScreen=Screen.HOME;
		AppMemory.getAppMemory().skeleton.getProcessName().setText(AppConstants.ACCOUNTMODULE+"/"+"Payment Dashboard");
		
		PaymentDashboardSearchForm searchForm=new PaymentDashboardSearchForm();
		PaymentdashboardPresenter presenter= new PaymentdashboardPresenter(homeForm,searchForm);
		
		CommunicationSmartPoup commSmrtPopup=new CommunicationSmartPoup();
		
		
		AppMemory.getAppMemory().stickPnel(homeForm);	
	}
	
	public void retrieveLeadData(){
		
		Long compId=UserConfiguration.getCompanyId();
	    System.out.println("Company Id :: "+compId);
		    
		List<String> branchList ;
		branchList= new ArrayList<String>();
		System.out.println("item count in branch "+LoginPresenter.globalBranch.size());
		for (int i = 0; i < LoginPresenter.globalBranch.size(); i++) {
			branchList.add(LoginPresenter.globalBranch.get(i).getBusinessUnitName());
		}
		System.out.println("branchList list "+branchList.size());
	
		
		Date date=new Date();
		System.out.println("Today Date ::::::::: "+date);
		CalendarUtil.addDaysToDate(date,7);
	    System.out.println("Changed Date + 7 ::::::::: "+date);
		
		Vector<Filter> filtervec=null;
		Filter filter = null;
		MyQuerry querry;
		filtervec=new Vector<Filter>();
		
		List<String> statusList=new ArrayList<String>();
		
		filtervec=new Vector<Filter>();
		statusList=new ArrayList<String>();
		statusList.add(CustomerPayment.CREATED);
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(compId);
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("accountType");
		filter.setStringValue(AppConstants.BILLINGACCOUNTTYPEAR);
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("status IN");
		filter.setList(statusList);
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("branch IN");
		filter.setList(branchList);
		filtervec.add(filter);
				
		filter = new Filter();
		filter.setQuerryString("followUpDate <=");
		filter.setDateValue(date);
		filtervec.add(filter);
		/**
		 * ends here
		 */	
		
		querry=new MyQuerry();
		querry.setQuerryObject(new CustomerPayment());
		querry.setFilters(filtervec);
		this.retriveTable(querry);
		
	}
	
	
	public void retriveTable(MyQuerry querry){
		service.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				globalCustomerPaymentList=new ArrayList<>();
				
				if(result.size()==0){
					SC.say("No result found.");
				}
				for(SuperModel model:result){
					CustomerPayment lead=(CustomerPayment) model;
					globalCustomerPaymentList.add(lead);
				}
				form.setDataToListGrid(globalCustomerPaymentList);
			}
			@Override
			public void onFailure(Throwable caught) {
				caught.printStackTrace();
			}
		}); 
	 }
	

	@Override
	public void onClick(ClickEvent event) {
		System.out.println("CLICK EVENT..!!!");
		if(event.getSource() instanceof InlineLabel){
			InlineLabel lbl= (InlineLabel) event.getSource();
			if(lbl.getText().equals("Download")){
				reactOnDownload();
			}
			if(lbl.getText().equals("Search")){
				System.out.println("ON CLICK OF SEARCH..!!!");
				searchForm.showPopUp();
			}
			if(lbl.getText().equals("Go")){
				reactOnGo();
			}
		}
		
		if (event.getSource().equals(form.getBtnGo())) {
			System.out.println("BTN GO CLICK");
			if(LoginPresenter.branchRestrictionFlag==true&&form.getOlbbranch().getSelectedIndex()==0){
				SC.say("Please select branch.");
				return;
			}
			System.out.println("FRM DT "+form.getDbFromDate().getValue());
			if (form.getOlbbranch().getSelectedIndex() == 0
					&& form.getOlbsalesPerson().getSelectedIndex() == 0
					&& form.getDbFromDate().getValue()==null
					&& form.getDbToDate().getValue()==null) {
				SC.say("Please select atleast one filter.");
				return;
			}else {

				if (form.getOlbsalesPerson().getSelectedIndex() != 0
						&& form.getOlbbranch().getSelectedIndex() == 0
						&& (form.getDbFromDate().getValue() == null && form.getDbToDate() != null)) {

					searchByFromToDate();

				} else if (form.getOlbsalesPerson().getSelectedIndex() == 0
						&& form.getOlbbranch().getSelectedIndex() != 0
						&& (form.getDbFromDate().getValue() == null && form.getDbToDate() != null)) {

					searchByFromToDate();
				} else if (form.getOlbsalesPerson().getSelectedIndex() != 0
						&& form.getOlbbranch().getSelectedIndex() != 0
						&& (form.getDbFromDate().getValue() == null && form.getDbToDate() != null)) {

					searchByFromToDate();
				} else {
					if (form.getDbFromDate().getValue() == null
							|| form.getDbToDate().getValue() == null) {
						
						SC.say("Select From and To Date.");
					}
					if (form.getDbFromDate().getValue() != null&& form.getDbToDate() != null) {
						Date formDate = form.getDbFromDate().getValue();
						Date toDate = form.getDbToDate().getValue();
						if (toDate.equals(formDate) || toDate.after(formDate)) {
							CalendarUtil.addDaysToDate(formDate, 7);
							if (toDate.before(formDate)) {
								searchByFromToDate();
							} else {
								SC.say("From Date and To Date Difference Can Not Be More Than 7 Days.");
							}
						} else {
							SC.say("To Date should be greater than From Date.");
						}
					}
				}
			}
		}
		
		
		
		if (event.getSource() == commSmrtPopup.getBtnOk()) {
//			glassPanel.show();
			List<InteractionType> list = commSmrtPopup.getCommunicationLogTable().getDataprovider().getList();
			ArrayList<InteractionType> interactionlist = new ArrayList<InteractionType>();
			interactionlist.addAll(list);

			boolean checkNewInteraction = AppUtility.checkNewInteractionAdded(interactionlist);

			if (checkNewInteraction == false) {
				SC.say("Please add new interaction details");
//				glassPanel.hide();
			} else {
				final Date followUpDate = interactionlist.get(interactionlist.size() - 1).getInteractionDueDate();
				communicationService.saveCommunicationLog(interactionlist,new AsyncCallback<Void>() {
					@Override
					public void onFailure(Throwable caught) {
						SC.say("Unexpected Error");
//						glassPanel.hide();
//						LoginPresenter.communicationLogPanel.hide();
					}

					@Override
					public void onSuccess(Void result) {
						commSmrtPopup.hidePopup();
						SC.say("Data Save Successfully");
//						List<Lead> list = getDataprovider().getList();
//						list.get(rowIndex).setFollowUpDate(followUpDate);
//						Lead leadObj = list.get(rowIndex);
						paymentEntity.setFollowUpDate(followUpDate);
						selectedRecord.setAttribute("followupDate", AppUtility.parseDate(followUpDate));
						savePayment(paymentEntity);
						form.grid.getDataSource().updateData(selectedRecord);
						form.grid.redraw();
//						followUpFlag = false;
//						table.redraw();
//						glassPanel.hide();
//						communicationPanel.hide();
					}
				});
			}

		}
		if (event.getSource() == commSmrtPopup.getBtnCancel()) {
//			followUpFlag = false;
//			communicationPanel.hide();
			
			commSmrtPopup.hidePopup();
		}
		if (event.getSource() == commSmrtPopup.getBtnAdd()) {
//			glassPanel.show();
			String remark = commSmrtPopup.getRemark().getValue();
			Date dueDate = commSmrtPopup.getDueDate().getValue();
			String interactiongGroup = null;
			if (commSmrtPopup.getOblinteractionGroup().getSelectedIndex() != 0)
				interactiongGroup = commSmrtPopup.getOblinteractionGroup().getValue(commSmrtPopup.getOblinteractionGroup().getSelectedIndex());
				boolean validationFlag = AppUtility.validateCommunicationlogSmartGwt(remark, dueDate);

				List<InteractionType> list = commSmrtPopup.getCommunicationLogTable().getDataprovider().getList();
			
				Comparator<InteractionType> comp = new Comparator<InteractionType>() {
					@Override
					public int compare(InteractionType e1, InteractionType e2) {
						if (e1.getCount() == e2.getCount()) {
							return 0;
						}
						if (e1.getCount() > e2.getCount()) {
							return 1;
						} else {
							return -1;
						}
					}
				};
				Collections.sort(list, comp);
	
				if (validationFlag) {
					InteractionType communicationLog = AppUtility.getCommunicationLog(AppConstants.ACCOUNTMODULE,
									AppConstants.PAYMENTDETAILS, paymentEntity.getCount(),paymentEntity.getEmployee(), remark, dueDate,
									paymentEntity.getPersonInfo(), null, interactiongGroup,paymentEntity.getBranch(),"");
					list.add(communicationLog);
					commSmrtPopup.getCommunicationLogTable().getDataprovider().setList(list);
					commSmrtPopup.getRemark().setValue("");
					commSmrtPopup.getDueDate().setValue(null);
					commSmrtPopup.getOblinteractionGroup().setSelectedIndex(0);
				}
//				glassPanel.hide();
		}
		
	}
	
	private void savePayment(CustomerPayment leadObj) {
		service.save(leadObj, new AsyncCallback<ReturnFromServer>() {
			@Override
			public void onFailure(Throwable caught) {
//				glassPanel.hide();
				SC.say("An Unexpected error occurred!");
			}
			@Override
			public void onSuccess(ReturnFromServer result) {
//				glassPanel.hide();
				SC.say("Saved successfully!");
			}
		});

	}
	
	private void searchByFromToDate() {
		System.out.println("FROM AND DATE ---");
		Long compId = UserConfiguration.getCompanyId();
		Vector<Filter> filtervec = null;
		Filter filter = null;
		MyQuerry querry;

		filtervec = new Vector<Filter>();

		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(compId);
		filtervec.add(filter);
		
		List<String> statusList=new ArrayList<String>();
		statusList.add(CustomerPayment.CREATED);
		
		
		filter = new Filter();
		filter.setQuerryString("accountType");
		filter.setStringValue(AppConstants.BILLINGACCOUNTTYPEAR);
		filtervec.add(filter);
		
//		statusList=AppUtility.getConfigValue(6);
		
//		for(int i=0;i<statusList.size();i++){
//			if(statusList.get(i).trim().equalsIgnoreCase(AppConstants.Sucessful)
//					||statusList.get(i).trim().equalsIgnoreCase(AppConstants.Unsucessful)
//					||statusList.get(i).trim().equalsIgnoreCase(AppConstants.CANCELLED)
//					||statusList.get(i).trim().equalsIgnoreCase(AppConstants.CANCEL)
//					||statusList.get(i).trim().equalsIgnoreCase(AppConstants.REJECTED)
//					||statusList.get(i).trim().equalsIgnoreCase(AppConstants.CLOSED)
//					||statusList.get(i).trim().equalsIgnoreCase("Completed")
//					||statusList.get(i).trim().equalsIgnoreCase(AppConstants.Successful)
//					||statusList.get(i).trim().equalsIgnoreCase("Quotation Sent")){
//				statusList.remove(i);
//				i--;
//			}
//		}
		
		filter = new Filter();
		filter.setQuerryString("status IN");
		filter.setList(statusList);
		filtervec.add(filter);

		/**
		 * @author Anil @since 21-09-2021
		 * Changing date filter to payment date raised by Rahul Tiwari for olite
		 */
		if (form.getDbFromDate().getValue() != null) {
			filter = new Filter();
//			filter.setQuerryString("followUpDate >=");
			filter.setQuerryString("paymentDate >=");
			filter.setDateValue(form.getDbFromDate().getValue());
			filtervec.add(filter);
		}

		if (form.getDbToDate().getValue() != null) {

			filter = new Filter();
//			filter.setQuerryString("followUpDate <=");
			filter.setQuerryString("paymentDate <=");
			filter.setDateValue(form.getDbToDate().getValue());
			filtervec.add(filter);
		}

		if (form.getOlbbranch().getSelectedIndex() != 0) {
			filter = new Filter();
			filter.setQuerryString("branch");
			filter.setStringValue(form.getOlbbranch().getValue());
			filtervec.add(filter);

		}

		if (form.getOlbsalesPerson().getSelectedIndex() != 0) {
			filter = new Filter();
			filter.setQuerryString("employee");
			filter.setStringValue(form.getOlbsalesPerson().getValue());
			filtervec.add(filter);
		}

		querry = new MyQuerry();
		querry.setQuerryObject(new CustomerPayment());
		querry.setFilters(filtervec);
		this.retriveTable(querry);

	}
	
	
	public void reactOnGo() {

		if (!searchForm.personInfo.getId().getValue().equals("")
				&& !searchForm.personInfo.getName().getValue().equals("")
				&& !searchForm.personInfo.getPhone().getValue().equals("")) {
			searchForm.hidePopUp();
			MyQuerry querry = createLeadFilter();
			retriveTable(querry);
			
		}

		if (searchForm.personInfo.getId().getValue().equals("")
				&& searchForm.personInfo.getName().getValue().equals("")
				&& searchForm.personInfo.getPhone().getValue().equals("")) {
			SC.say("Please enter customer information");
		}

	}

	private MyQuerry createLeadFilter() {
		Vector<Filter> filterVec = new Vector<Filter>();
		Filter temp;

		if (searchForm.personInfo.getId().getText().trim().equals("") == false) {
			temp = new Filter();
			temp.setLongValue(Long.parseLong(searchForm.personInfo.getId().getValue()));
			temp.setQuerryString("personInfo.count");
			filterVec.add(temp);
		}

		if ((searchForm.personInfo.getName().getText()).trim().equals("") == false) {
			temp = new Filter();
			temp.setStringValue(searchForm.personInfo.getName().getValue());
			temp.setQuerryString("personInfo.fullName");
			filterVec.add(temp);
		}

		if (searchForm.personInfo.getPhone().getText().trim().equals("") == false) {
			temp = new Filter();
			temp.setLongValue(Long.parseLong(searchForm.personInfo.getPhone().getValue()));
			temp.setQuerryString("personInfo.cellNumber");
			filterVec.add(temp);
		}

		MyQuerry lead = new MyQuerry();
		lead.setQuerryObject(new CustomerPayment());
		lead.setFilters(filterVec);

		return lead;
	}

	private void reactOnDownload() {
		ArrayList<CustomerPayment> custPayList = new ArrayList<CustomerPayment>();
		ListGridRecord[] allRecords = form.grid.getRecords();
        if (allRecords != null) {
        	List<ListGridRecord> allRecordsList = Arrays.asList(allRecords);
	   		for(int i=0;i<allRecordsList.size();i++){
	   			 final CustomerPayment model=getPaymentDetails(Integer.parseInt(allRecordsList.get(i).getAttribute("ID").trim()));
	   			 if(model!=null){
	   				custPayList.add(model);
	   			 }
	   		}
        }
		
        csvservice.setpaymentlist(custPayList, new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);
			}
			@Override
			public void onSuccess(Void result) {
				
				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "csvservlet"+"?type="+16;
				Window.open(url, "test", "enabled");
			}
		});
	}

	private CustomerPayment getPaymentDetails(int ID) {
		for(CustomerPayment object:globalCustomerPaymentList){
			if(object.getCount()==ID){
				return object;
			}
		}
		return null;
	}

	@Override
	public void onCellClick(CellClickEvent event) {
		
		ListGridRecord record = event.getRecord();
		int colNum = event.getColNum();
		ListGridField field = form.grid.getField(colNum);
		String fieldName = form.grid.getFieldName(colNum);
		String fieldTitle = field.getTitle();
		position=event.getRowNum();
		System.out.println("Clicked " + fieldTitle + ":"+ record.getAttribute(fieldName) + "  Lead Id :: "+ record.getAttribute("ID")+"POSITION "+position);
		paymentEntity = getPaymentDetails((Integer.parseInt(record.getAttribute("ID").trim())));
		if (fieldName.equals("followupButton")) {
			selectedRecord = record;
			getLogDetails(paymentEntity);
		}else{
			mapToLeadScreen();
		}
	}
	
	private void mapToLeadScreen() {
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Accounts/Customer Payment", Screen.PAYMENTDETAILS);
		final CustomerPayment entity = paymentEntity;
		final PaymentDetailsForm form = PaymentDetailsPresenter.initalize();

		form.showWaitSymbol();
		Timer timer = new Timer() {

			@Override
			public void run() {
				form.hideWaitSymbol();
				form.updateView(entity);
				form.setToViewState();
//				form.getPic().setEnabled(false);
			}
		};
		timer.schedule(5000);
    }

	private void getLogDetails(CustomerPayment lead) {
		
		MyQuerry querry = AppUtility.communicationLogQuerry(lead.getCount(),lead.getCompanyId(), AppConstants.ACCOUNTMODULE,AppConstants.PAYMENTDETAILS);
		service.getSearchResult(querry,	new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				ArrayList<InteractionType> list = new ArrayList<InteractionType>();
				for (SuperModel model : result) {
					InteractionType interactionType = (InteractionType) model;
					list.add(interactionType);
				}
				Comparator<InteractionType> comp = new Comparator<InteractionType>() {
					@Override
					public int compare(InteractionType e1,InteractionType e2) {
						if (e1.getCount() == e2.getCount()) {
							return 0;
						}
						if (e1.getCount() > e2.getCount()) {
							return 1;
						} else {
							return -1;
						}
					}
				};
				Collections.sort(list, comp);
				
				commSmrtPopup.getRemark().setValue("");
				commSmrtPopup.getDueDate().setValue(null);
				commSmrtPopup.getOblinteractionGroup().setSelectedIndex(0);
				commSmrtPopup.getCommunicationLogTable().getDataprovider().setList(list);
				commSmrtPopup.showPopup();
			}

			@Override
			public void onFailure(Throwable caught) {
			}
		});

	}
	



}
