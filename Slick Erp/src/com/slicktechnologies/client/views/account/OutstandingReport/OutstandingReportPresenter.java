package com.slicktechnologies.client.views.account.OutstandingReport;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.google.gwt.view.client.RowCountChangeEvent.Handler;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.DummyEntityOnlyForScreen;
import com.slicktechnologies.shared.common.Outstanding;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

public class OutstandingReportPresenter  extends FormScreenPresenter<DummyEntityOnlyForScreen> implements SelectionHandler<Suggestion> ,ClickHandler, Handler{
	
	OutstandingReportForm form;
	
	final GenricServiceAsync async =GWT.create(GenricService.class);
	NumberFormat fmt = NumberFormat.getFormat("0.00");
	
	double sum =0;
	double netbilledAmount,netPaymentAmount,netUnbilledamnt ;
	
	ArrayList<Invoice> invoiceList = new ArrayList<Invoice>();
	ArrayList<BillingDocument> billingList = new ArrayList<BillingDocument>();
	ArrayList<CustomerPayment> payment1List = new ArrayList<CustomerPayment>();
	ArrayList<CustomerPayment> payment2List = new ArrayList<CustomerPayment>();
	ArrayList<Outstanding> unbilledOsList = new ArrayList<Outstanding>();
	ArrayList<Outstanding> billedOsList = new ArrayList<Outstanding>();
	ArrayList<Outstanding> paymentOsList = new ArrayList<Outstanding>();
	
	
	public OutstandingReportPresenter(FormScreen<DummyEntityOnlyForScreen> view,DummyEntityOnlyForScreen model) {
		super(view, model);
		form =(OutstandingReportForm)view;
		form.personInfoComposite.getId().addSelectionHandler(this);
		form.personInfoComposite.getName().addSelectionHandler(this);
		form.personInfoComposite.getPhone().addSelectionHandler(this);
		form.getSearchbutton().addClickHandler(this);
		
		form.rb1Branchlevel.addClickHandler(this);
		form.rb2Customerlevel.addClickHandler(this);
		form.rb3Contractlevel.addClickHandler(this);
		
		form.unbilledTable.getTable().addRowCountChangeHandler(this);
		form.billedTable.getTable().addRowCountChangeHandler(this);
		form.paymentTable.getTable().addRowCountChangeHandler(this);
		
		form.olbbranch.clear();
		form.olbbranch.setEnabled(false);
		form.dbFromDate.setValue(null);
		form.dbToDate.setValue(null);
		form.dbFromDate.setEnabled(false);
		form.dbToDate.setEnabled(false);
		form.personInfoComposite.setEnabled(false);
		form.tbTotalRevenue.setValue(null); 
   		form.tbTotalRevenue.setEnabled(false);
   		form.tbPaymentsOutstanding.setValue(null);
   		form.tbPaymentsOutstanding.setEnabled(false);
   		form.tbBilledOutstanding.setValue(null); 
   		form.tbBilledOutstanding.setEnabled(false);
   		form.tbUnbilledOutstanding.setValue(null);
   		form.tbUnbilledOutstanding.setEnabled(false);
   		
   		form.contractCount.setSelectedIndex(0);
		form.contractCount.setEnabled(false);
		form.contractStatus.setValue("");
		form.contractStatus.setEnabled(false);
		form.endDate.setValue(null);
		form.endDate.setEnabled(false);
		form.startDate.setValue(null);
		form.startDate.setEnabled(false);
		
		form.setPresenter(this);
	}
	/************************************ Methods Contract level Starts Here *********************************************/
	private void getInvocieDetailsList() 
	{
		System.out.println("Inside getInvocieDetailsList method ");
		
		MyQuerry querry = new MyQuerry();
	  	Company c = new Company();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
	  	
	  	List<String> statusList=new ArrayList<String>();
	  	statusList.add(Invoice.CREATED);
	  	statusList.add(Invoice.REQUESTED);
	  	statusList.add(Invoice.PROFORMAINVOICE);
	  	
		filter = new Filter();
		filter.setQuerryString("contractCount");
		filter.setIntValue(Integer.parseInt(form.contractCount.getValue(form.contractCount.getSelectedIndex())));
		filtervec.add(filter);
		
		filter = new Filter();
	  	filter.setQuerryString("status IN");
		filter.setList(statusList);
		filtervec.add(filter);
		
		filter = new Filter();
	  	filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Invoice());
		
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onFailure(Throwable caught) 
			{
				
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) 
			{
				invoiceList=new ArrayList<Invoice>();
				for(SuperModel model :result){
					Invoice entity=(Invoice) model;
					invoiceList.add(entity);
				}
				
				for(Invoice invoice:invoiceList){
					Outstanding report = new Outstanding();
					
					report.setCustId(invoice.getPersonInfo().getCount());
					report.setCustCellNo(invoice.getPersonInfo().getCellNumber()+"");
					report.setCustName(invoice.getPersonInfo().getFullName());	
					report.setPoc(invoice.getPersonInfo().getPocName());
					report.setContractId(invoice.getContractCount());
					report.setDocType("Invoice");
					report.setDocID(invoice.getCount());
					report.setTotal(invoice.getNetPayable());
					report.setStatus(invoice.getStatus());
					unbilledOsList.add(report);
				}
				
				if(unbilledOsList.size()!=0)
				{
				   form.unbilledTable.getDataprovider().getList().addAll(unbilledOsList);
				}
				else
				{
					form.showDialogMessage("No Result Found ");
					
				}
			}
		});
	}
	
	private void getUnbilledBillingDetailsListForContractLevel() 
	{
		System.out.println("Inside unbilledBillingDetailsList metod ");
		MyQuerry querry = new MyQuerry();
	  	Company c = new Company();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
	  	
	  	List<String> statusList=new ArrayList<String>();
	  	statusList.add(Invoice.CREATED);
	  	statusList.add(Invoice.REQUESTED);
	  	
		filter = new Filter();
		filter.setQuerryString("contractCount");
		filter.setIntValue(Integer.parseInt(form.contractCount.getValue(form.contractCount.getSelectedIndex())));
		filtervec.add(filter);
		
		filter = new Filter();
	  	filter.setQuerryString("status IN");
		filter.setList(statusList);
		filtervec.add(filter);
		
		filter = new Filter();
	  	filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new BillingDocument());
		
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onFailure(Throwable caught) 
			{
				
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				
				billingList=new ArrayList<BillingDocument>();
				unbilledOsList=new ArrayList<Outstanding>();
				
				for(SuperModel model :result)
				{
					BillingDocument entity=(BillingDocument) model;
					billingList.add(entity);
				}
				
				for(BillingDocument billDocument:billingList)
				{
					Outstanding report = new Outstanding();
					
					report.setCustId(billDocument.getPersonInfo().getCount());
					report.setCustCellNo(billDocument.getPersonInfo().getCellNumber()+"");
					report.setCustName(billDocument.getPersonInfo().getFullName());	
					report.setPoc(billDocument.getPersonInfo().getPocName());
					report.setContractId(billDocument.getContractCount());
					report.setDocType("Billing Document");
					report.setDocID(billDocument.getCount());
					report.setTotal(billDocument.getTotalBillingAmount());
					report.setStatus(billDocument.getStatus());
					unbilledOsList.add(report);
				}
				
				getInvocieDetailsList();                            //unbilled Invoice details , unbilled Tabled
			}
			
		});
	}
	///////////////////////////////////////////////////////////////////////////////
	
	///////////////////////////////////////////////////////////////////////////////
	public void getPaymentDetailsListForContractLevel()
	{                                         
       System.out.println("Inside getPaymentDetailsList method ");
		
		MyQuerry querry = new MyQuerry();
	  	Company c = new Company();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
	  	
	  	List<String> statusList=new ArrayList<String>();
	  	statusList.add(CustomerPayment.CLOSED); 
	  	
		filter = new Filter();
		filter.setQuerryString("contractCount");
		filter.setIntValue(Integer.parseInt(form.contractCount.getValue(form.contractCount.getSelectedIndex())));
		filtervec.add(filter);
		
		filter = new Filter();
	  	filter.setQuerryString("status IN");
		filter.setList(statusList);
		filtervec.add(filter);
		
		filter = new Filter();
	  	filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new CustomerPayment());
		
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onFailure(Throwable caught)
			{
				
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) 
			{
				payment2List =new ArrayList<CustomerPayment>();
				paymentOsList = new ArrayList<Outstanding>();
				for(SuperModel model :result){
					CustomerPayment entity=(CustomerPayment) model;
					payment2List.add(entity);
				}
				
				for(CustomerPayment custPayment: payment2List)
				{
					Outstanding report = new Outstanding();
					
					report.setCustId(custPayment.getPersonInfo().getCount());
					report.setCustCellNo(custPayment.getPersonInfo().getCellNumber()+"");
					report.setCustName(custPayment.getPersonInfo().getFullName());	
					report.setPoc(custPayment.getPersonInfo().getPocName());
					report.setContractId(custPayment.getContractCount());
					report.setDocType("Payment Document");
					report.setDocID(custPayment.getCount());
					report.setTotal(custPayment.getNetPay());
					report.setTotal(custPayment.getTotalBillingAmount());
					report.setStatus(custPayment.getStatus());
					paymentOsList.add(report); 
				}
				
				form.paymentTable.getDataprovider().getList().addAll(paymentOsList);

//				double total = 0;
//				netPaymentAmount = 0 ;
//				if(paymentOsList.size()!=0)
//				{
//				for(int i = 0 ; i<paymentOsList.size();i++)
//				{
//					 total = paymentOsList.get(i).getTotal();
//					 netPaymentAmount= netPaymentAmount+total;
//				}
//				System.out.println("total amount "+netPaymentAmount);
//				
//				}
//				else
//				{
//					netPaymentAmount = 0 ;	
//				}
//				
//				form.tbPaymentsOutstanding.setValue(netPaymentAmount);
//				form.tbPaymentsOutstanding.setEnabled(false);
//				
////				getNetamountRenevue();
//				double NetRevenue = netbilledAmount + netUnbilledamnt + netPaymentAmount ;
//				form.tbTotalRevenue.setValue(NetRevenue);
//				form.tbTotalRevenue.setEnabled(false);
			}
		});
		
	}
	
	///////////////////////////////////////////////////////////////////////////////
	
	//////////////////////////////////////////////////////////////////////////////       
	public void getBilledDetailsListForContractLevel()
	
	{ 
		System.out.println("Inside getBilledDetailsList method ");
		
		MyQuerry querry = new MyQuerry();
	  	Company c = new Company();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
	  	
	  	List<String> statusList=new ArrayList<String>();
	  	statusList.add(CustomerPayment.CREATED); 
	  	
		filter = new Filter();
		filter.setQuerryString("contractCount");
		filter.setIntValue(Integer.parseInt(form.contractCount.getValue(form.contractCount.getSelectedIndex())));
		filtervec.add(filter);
		
		filter = new Filter();
	  	filter.setQuerryString("status IN");
		filter.setList(statusList);
		filtervec.add(filter);
		
		filter = new Filter();
	  	filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new CustomerPayment());
		
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onFailure(Throwable caught)
			{
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) 
			{
				payment1List =new ArrayList<CustomerPayment>();
				billedOsList = new ArrayList<Outstanding>();
				for(SuperModel model :result){
					CustomerPayment entity=(CustomerPayment) model;
					payment1List.add(entity);
				}
				
				for(CustomerPayment custPayment: payment1List)
				{
					Outstanding report = new Outstanding();
					
					report.setCustId(custPayment.getPersonInfo().getCount());
					report.setCustCellNo(custPayment.getPersonInfo().getCellNumber()+"");
					report.setCustName(custPayment.getPersonInfo().getFullName());	
					report.setPoc(custPayment.getPersonInfo().getPocName());
					report.setContractId(custPayment.getContractCount());
					report.setDocType("Payment Document");
					report.setDocID(custPayment.getCount());
					report.setTotal(custPayment.getNetPay());
					report.setTotal(custPayment.getTotalBillingAmount());
					report.setStatus(custPayment.getStatus());
					billedOsList.add(report); 
				}
				
				form.billedTable.getDataprovider().getList().addAll(billedOsList);
				
//				double total = 0;
//			    netbilledAmount = 0 ;
//			    if(billedOsList.size()!=0){
//				for(int i = 0 ; i<billedOsList.size();i++)
//				{
//					 total = billedOsList.get(i).getTotal();
//					 netbilledAmount= netbilledAmount+total;
//				}
//			    }
//			    else
//			    {
//			    	 netbilledAmount= netbilledAmount+total;
//			    }
//			    
//				System.out.println("billed Amount "+netbilledAmount);
//				form.tbBilledOutstanding.setValue(netbilledAmount);
//				form.tbBilledOutstanding.setEnabled(false);
				
				
				
			}
		});
		
	}
	
	/************************************ Methods Contract level Ends Here ****************************************/
	
	/************************************Methods Branch level Starts Here *********************************************/
	
	private void getBranchInvocieDetailsList() 
	{
		System.out.println("Inside getInvocieDetailsList method ");
		
		MyQuerry querry = new MyQuerry();
	  	Company c = new Company();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
	  	
	  	List<String> statusList=new ArrayList<String>();
	  	statusList.add(Invoice.CREATED);
	  	statusList.add(Invoice.REQUESTED);
	  	statusList.add(Invoice.PROFORMAINVOICE);
	  
      /////////////////////////////////
	  	
		filter = new Filter();
		filter.setQuerryString("branch");
		filter.setStringValue(form.getOlbbranch().getValue());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("invoiceDate");
		filter.setDateValue((form.dbFromDate.getValue()));
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("invoiceDate");
		filter.setDateValue((form.dbToDate.getValue()));
		filtervec.add(filter);
		
		/////////////////////////////////
		
		filter = new Filter();
	  	filter.setQuerryString("status IN");
		filter.setList(statusList);
		filtervec.add(filter);
		
		
		
		filter = new Filter();
	  	filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Invoice());
		
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onFailure(Throwable caught) 
			{
				
				
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) 
			{
				invoiceList=new ArrayList<Invoice>();
				for(SuperModel model :result){
					Invoice entity=(Invoice) model;
					invoiceList.add(entity);
				}
				
				for(Invoice invoice:invoiceList){
					Outstanding report = new Outstanding();
					
					report.setCustId(invoice.getPersonInfo().getCount());
					report.setCustCellNo(invoice.getPersonInfo().getCellNumber()+"");
					report.setCustName(invoice.getPersonInfo().getFullName());	
					report.setPoc(invoice.getPersonInfo().getPocName());
					report.setContractId(invoice.getContractCount());
					report.setDocType("Invoice");
					report.setDocID(invoice.getCount());
					report.setTotal(invoice.getNetPayable());
					report.setStatus(invoice.getStatus());
					
					unbilledOsList.add(report);
				}
				if(unbilledOsList.size()!=0)
				{
				   form.unbilledTable.getDataprovider().getList().addAll(unbilledOsList);
				}
				else
				{
					form.showDialogMessage("No Result Found ");
					
				}
			}
		});
	}
	
	private void getUnbilledBillingDetailsListForBranchLevel() 
	{
		System.out.println("Inside unbilledBillingDetailsList metod ");
		MyQuerry querry = new MyQuerry();
	  	Company c = new Company();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
	  	
	  	List<String> statusList=new ArrayList<String>();
	  	statusList.add(Invoice.CREATED);
	  	statusList.add(Invoice.REQUESTED);
	  	
		
		/////////////////////////////////////

		if(form.getDbFromDate().getValue()!=null)
		{
		filter = new Filter();
		filter.setQuerryString("billingDocumentInfo.billingDate >=");   //  Customer Payment  billingDocumentInfo.billingDate
		filter.setDateValue(form.dbFromDate.getValue());
		filtervec.add(filter);
		}
		
		if(form.getDbToDate().getValue()!=null)
		{
		  filter = new Filter();
		  filter.setQuerryString("billingDocumentInfo.billingDate <=");
		  filter.setDateValue(form.getDbToDate().getValue());
		  filtervec.add(filter);
		}
		
		if(form.getOlbbranch().getSelectedIndex()!=0)
		{
			System.out.println("Inside Branch");
				filter = new Filter();
				filter.setQuerryString("branch");
				filter.setStringValue(form.getOlbbranch().getValue());
				filtervec.add(filter);
	    }
		////////////////////////////////////
		
		filter = new Filter();
	  	filter.setQuerryString("status IN");
		filter.setList(statusList);
		filtervec.add(filter);
		
		filter = new Filter();
	  	filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new BillingDocument());
		
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onFailure(Throwable caught) {
				
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				
				billingList=new ArrayList<BillingDocument>();
				unbilledOsList=new ArrayList<Outstanding>();
				
				for(SuperModel model :result)
				{
					BillingDocument entity=(BillingDocument) model;
					billingList.add(entity);
				}
			
				
				for(BillingDocument billDocument:billingList)
				{
					Outstanding report = new Outstanding();
					
					report.setCustId(billDocument.getPersonInfo().getCount());
					report.setCustCellNo(billDocument.getPersonInfo().getCellNumber()+"");
					report.setCustName(billDocument.getPersonInfo().getFullName());	
					report.setPoc(billDocument.getPersonInfo().getPocName());
					report.setContractId(billDocument.getContractCount());
					report.setDocType("Billing Document");
					report.setDocID(billDocument.getCount());
					report.setTotal(billDocument.getTotalBillingAmount());
					report.setStatus(billDocument.getStatus());
					unbilledOsList.add(report);
				}
				
				getBranchInvocieDetailsList();                            //unbilled Invoice details , unbilled Tabled
			}
			
		});
	}
//	///////////////////////////////////////////////////////////////////////////////
//	
//	///////////////////////////////////////////////////////////////////////////////
	public void getPaymentDetailsListForBranchLevel()
	{                                         
       System.out.println("Inside getPaymentDetailsList method ");
		
		MyQuerry querry = new MyQuerry();
	  	Company c = new Company();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
	  	
	  	List<String> statusList=new ArrayList<String>();
	  	statusList.add(CustomerPayment.CLOSED); 
	  	
//		/////////////////////////////////
	  	
		filter = new Filter();
		filter.setQuerryString("branch");
		filter.setStringValue(form.getOlbbranch().getValue());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("paymentDate >=");
		filter.setDateValue((form.dbFromDate.getValue()));
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("paymentDate <=");
		filter.setDateValue((form.dbToDate.getValue()));
		filtervec.add(filter);
		
		/////////////////////////////////
		
		filter = new Filter();
	  	filter.setQuerryString("status IN");
		filter.setList(statusList);
		filtervec.add(filter);
		
		filter = new Filter();
	  	filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new CustomerPayment());
		
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onFailure(Throwable caught)
			{
				
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) 
			{
				payment2List =new ArrayList<CustomerPayment>();
				paymentOsList = new ArrayList<Outstanding>();
				for(SuperModel model :result){
					CustomerPayment entity=(CustomerPayment) model;
					payment2List.add(entity);
				}
				
				for(CustomerPayment custPayment: payment2List)
				{
					Outstanding report = new Outstanding();
					
					report.setCustId(custPayment.getPersonInfo().getCount());
					report.setCustCellNo(custPayment.getPersonInfo().getCellNumber()+"");
					report.setCustName(custPayment.getPersonInfo().getFullName());	
					report.setPoc(custPayment.getPersonInfo().getPocName());
					report.setContractId(custPayment.getContractCount());
					report.setDocType("Payment Document");
					report.setDocID(custPayment.getCount());
					report.setTotal(custPayment.getNetPay());
					report.setTotal(custPayment.getTotalBillingAmount());
					report.setStatus(custPayment.getStatus());
					paymentOsList.add(report); 
					
				}
				
//				if(paymentOsList.size()!=0)
//				{
//					form.paymentTable.getDataprovider().getList().addAll(paymentOsList);
//				}
//				else
//				{
//					form.showDialogMessage("No Result Found ");
//				}
				
			}
		});
		
	}
	
	///////////////////////////////////////////////////////////////////////////////
	
	//////////////////////////////////////////////////////////////////////////////       
	public void getBilledDetailsListForBranchLevel()
	
	{ 
		
		System.out.println("Inside getBilledDetailsList method ");
   
		
		MyQuerry querry = new MyQuerry();
	  	Company c = new Company();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
	  	
	  	List<String> statusList=new ArrayList<String>();
	  	statusList.add(CustomerPayment.CREATED); 
	  	
//		/////////////////////////////////
	  	
		filter = new Filter();
		filter.setQuerryString("branch");
		filter.setStringValue(form.getOlbbranch().getValue());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("paymentDate >=");
		filter.setDateValue((form.dbFromDate.getValue()));
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("paymentDate <=");
		filter.setDateValue((form.dbToDate.getValue()));
		filtervec.add(filter);
		
		/////////////////////////////////
		
		filter = new Filter();
	  	filter.setQuerryString("status IN");
		filter.setList(statusList);
		filtervec.add(filter);
		
		filter = new Filter();
	  	filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new CustomerPayment());
		
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onFailure(Throwable caught)
			{
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) 
			{
				payment1List =new ArrayList<CustomerPayment>();
				billedOsList = new ArrayList<Outstanding>();
				for(SuperModel model :result){
					CustomerPayment entity=(CustomerPayment) model;
					payment1List.add(entity);
				}
				
				for(CustomerPayment custPayment: payment1List)
				{
					Outstanding report = new Outstanding();
					
					report.setCustId(custPayment.getPersonInfo().getCount());
					report.setCustCellNo(custPayment.getPersonInfo().getCellNumber()+"");
					report.setCustName(custPayment.getPersonInfo().getFullName());	
					report.setPoc(custPayment.getPersonInfo().getPocName());
					report.setContractId(custPayment.getContractCount());
					report.setDocType("Payment Document");
					report.setDocID(custPayment.getCount());
					report.setTotal(custPayment.getNetPay());
					report.setTotal(custPayment.getTotalBillingAmount());
					report.setStatus(custPayment.getStatus());
					billedOsList.add(report); 
				}
				
//				if(billedOsList.size()!=0)
//				{
//					form.billedTable.getDataprovider().getList().addAll(billedOsList);
//				}
//				else
//				{
//					form.showDialogMessage("No Result Found ");
//				}
				
			}
		});
		
	}
	
	/************************************Methods Branch level Ends Here ****************************************************/
	
	/************************************Methods Customer level Starts Here ***********************************************/
	
	
	private void getCustomerInvocieDetailsList() 
	{
		System.out.println("Inside getInvocieDetailsList method ");
		
		MyQuerry querry = new MyQuerry();
	  	Company c = new Company();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
	  	
	  	List<String> statusList=new ArrayList<String>();
	  	statusList.add(Invoice.CREATED);
	  	statusList.add(Invoice.REQUESTED);
	  	statusList.add(Invoice.PROFORMAINVOICE);
	  
      /////////////////////////////////
	  	
	  	filter = new Filter();
		filter.setQuerryString("personInfo.count");
		filter.setIntValue(Integer.parseInt(form.personInfoComposite.getId().getValue().trim()));
		filtervec.add(filter);
		
		/////////////////////////////////
		
		filter = new Filter();
	  	filter.setQuerryString("status IN");
		filter.setList(statusList);
		filtervec.add(filter);
		
		filter = new Filter();
	  	filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Invoice());
		
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onFailure(Throwable caught) 
			{
				
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) 
			{
				invoiceList=new ArrayList<Invoice>();
				for(SuperModel model :result){
					Invoice entity=(Invoice) model;
					invoiceList.add(entity);
				}
				
				for(Invoice invoice:invoiceList)
				{
					Outstanding report = new Outstanding();
					
					report.setCustId(invoice.getPersonInfo().getCount());
					report.setCustCellNo(invoice.getPersonInfo().getCellNumber()+"");
					report.setCustName(invoice.getPersonInfo().getFullName());	
					report.setPoc(invoice.getPersonInfo().getPocName());
					report.setContractId(invoice.getContractCount());
					report.setDocType("Invoice");
					report.setDocID(invoice.getCount());
					report.setTotal(invoice.getNetPayable());
					report.setStatus(invoice.getStatus());
					unbilledOsList.add(report);
				}
				
				if(unbilledOsList.size()!=0)
				{
				   form.unbilledTable.getDataprovider().getList().addAll(unbilledOsList);
				}
				else
				{
					form.showDialogMessage("No Result Found ");
					
				}
			}
		});
		
		
	}
	
	private void getUnbilledBillingDetailsListCustomerLevel() 
	 {
		System.out.println("Inside unbilledBillingDetailsList metod ");
		MyQuerry querry = new MyQuerry();
	  	Company c = new Company();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
	  	
	  	List<String> statusList=new ArrayList<String>();
	  	statusList.add(Invoice.CREATED);
	  	statusList.add(Invoice.REQUESTED);
	  	
//	 /////////////////////////////////
	  
		filter = new Filter();
		filter.setQuerryString("personInfo.count");
		filter.setIntValue(Integer.parseInt(form.personInfoComposite.getId().getValue().trim()));
		filtervec.add(filter);
	
		/////////////////////////////////
		
		filter = new Filter();
	  	filter.setQuerryString("status IN");
		filter.setList(statusList);
		filtervec.add(filter);
		
		filter = new Filter();
	  	filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new BillingDocument());
		
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onFailure(Throwable caught) {
				
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				
				billingList=new ArrayList<BillingDocument>();
				unbilledOsList=new ArrayList<Outstanding>();
				
				for(SuperModel model :result)
				{
					BillingDocument entity=(BillingDocument) model;
					billingList.add(entity);
				}
			
				
				for(BillingDocument billDocument:billingList)
				{
					Outstanding report = new Outstanding();
					
					report.setCustId(billDocument.getPersonInfo().getCount());
					report.setCustCellNo(billDocument.getPersonInfo().getCellNumber()+"");
					report.setCustName(billDocument.getPersonInfo().getFullName());	
					report.setPoc(billDocument.getPersonInfo().getPocName());
					report.setContractId(billDocument.getContractCount());
					report.setDocType("Billing Document");
					report.setDocID(billDocument.getCount());
					report.setTotal(billDocument.getTotalBillingAmount());
					report.setStatus(billDocument.getStatus());
					unbilledOsList.add(report);
				}
				
				
				getCustomerInvocieDetailsList();                            //unbilled Invoice details , unbilled Tabled
			}
			
		});
	}
//	///////////////////////////////////////////////////////////////////////////////
//	
//	///////////////////////////////////////////////////////////////////////////////
	public void getPaymentDetailsListForCustomerLevel()
	{                                         
       System.out.println("Inside getPaymentDetailsList method ");
		
		MyQuerry querry = new MyQuerry();
	  	Company c = new Company();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
	  	
	  	List<String> statusList=new ArrayList<String>();
	  	statusList.add(CustomerPayment.CLOSED); 
	  	
         /////////////////////////////////
	  	
	  	filter = new Filter();
		filter.setQuerryString("personInfo.count");
		filter.setIntValue(Integer.parseInt(form.personInfoComposite.getId().getValue().trim()));
		filtervec.add(filter);
		
		/////////////////////////////////
		
		filter = new Filter();
	  	filter.setQuerryString("status IN");
		filter.setList(statusList);
		filtervec.add(filter);
		
		filter = new Filter();
	  	filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new CustomerPayment());
		
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onFailure(Throwable caught)
			{
				
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) 
			{
				payment2List =new ArrayList<CustomerPayment>();
				paymentOsList = new ArrayList<Outstanding>();
				for(SuperModel model :result){
					CustomerPayment entity=(CustomerPayment) model;
					payment2List.add(entity);
				}
				
				for(CustomerPayment custPayment: payment2List)
				{
					Outstanding report = new Outstanding();
					
					report.setCustId(custPayment.getPersonInfo().getCount());
					report.setCustCellNo(custPayment.getPersonInfo().getCellNumber()+"");
					report.setCustName(custPayment.getPersonInfo().getFullName());	
					report.setPoc(custPayment.getPersonInfo().getPocName());
					report.setDocType("Payment Document");
					report.setDocID(custPayment.getCount());
					report.setTotal(custPayment.getNetPay());
					report.setTotal(custPayment.getTotalBillingAmount());
					report.setStatus(custPayment.getStatus());
					paymentOsList.add(report); 
				}
				
				form.paymentTable.getDataprovider().getList().addAll(paymentOsList);

			}
			
		});
		
	}
	
	///////////////////////////////////////////////////////////////////////////////
	
	//////////////////////////////////////////////////////////////////////////////       
	public void getBilledDetailsListForCustomerLevel()
	
	{ 
		
		System.out.println("Inside getBilledDetailsList method ");
   
		
		MyQuerry querry = new MyQuerry();
	  	Company c = new Company();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
	  	
	  	List<String> statusList=new ArrayList<String>();
	  	statusList.add(CustomerPayment.CREATED); 
	  	
//		 /////////////////////////////////
	  	
	  	filter = new Filter();
		filter.setQuerryString("personInfo.count");
		filter.setIntValue(Integer.parseInt(form.personInfoComposite.getId().getValue().trim()));
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Customer());
		
		/////////////////////////////////
		
		
		filter = new Filter();
	  	filter.setQuerryString("status IN");
		filter.setList(statusList);
		filtervec.add(filter);
		
		filter = new Filter();
	  	filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new CustomerPayment());
		
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onFailure(Throwable caught)
			{
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) 
			{
				payment1List =new ArrayList<CustomerPayment>();
				billedOsList = new ArrayList<Outstanding>();
				for(SuperModel model :result){
					CustomerPayment entity=(CustomerPayment) model;
					payment1List.add(entity);
				}
				
				for(CustomerPayment custPayment: payment1List)
				{
					Outstanding report = new Outstanding();
					
					report.setCustId(custPayment.getPersonInfo().getCount());
					report.setCustCellNo(custPayment.getPersonInfo().getCellNumber()+"");
					report.setCustName(custPayment.getPersonInfo().getFullName());	
					report.setPoc(custPayment.getPersonInfo().getPocName());
					report.setDocType("Payment Document");
					report.setDocID(custPayment.getCount());
					report.setTotal(custPayment.getNetPay());
					report.setTotal(custPayment.getTotalBillingAmount());
					report.setStatus(custPayment.getStatus());
					billedOsList.add(report); 
				}
				
				form.billedTable.getDataprovider().getList().addAll(billedOsList);
				
			}
		});
		
	}
	
	/************************************Methods Customer level Ends Here  ************************************************/
	
private boolean ValidateDate()
{
		
		Date fromdate = form.dbFromDate.getValue();
		int fromMonth =fromdate.getMonth();       
		Date todate = form.dbToDate.getValue();
		int toMonth =todate.getMonth();                
		
		
		if(fromMonth==toMonth){
			return true;
		}
		else{
		
			form.showDialogMessage("From date and to date should be of same date(Month)..!");
			return false;
		}
		
		
	}
	
	@Override
	public void onClick(ClickEvent event) 
	{
		super.onClick(event);
		
		System.out.println("Button Clicked Event");
		if(event.getSource().equals(form.getSearchbutton()))
		{
			form.unbilledTable.connectToLocal();
			form.billedTable.connectToLocal();
			form.paymentTable.connectToLocal();
			
			System.out.println("Search Button Clicked");

			if( form.getBranchlevel().getValue() ||  form.getCustomerlevel().getValue()
					|| form.getContractlevel().getValue())
			{
				System.out.println("Inside outstanding level ");
				
				if(form.getBranchlevel().getValue())
				{
					form.personInfoComposite.setEnabled(false);
					
					form.contractCount.setSelectedIndex(0);
					form.contractCount.setEnabled(false);
					
					System.out.println("Inside Branch Level"+form.getBranchlevel().getValue());
					if (form.olbbranch.getSelectedIndex()!=0)
					{
						System.out.println("Branch selected index"+ form.olbbranch.getSelectedIndex());
						
						if(form.dbFromDate.getValue() !=null && form.dbToDate.getValue()!=null)
						{
							
							System.out.println("inside Frm dt and to dt");
							if(ValidateDate())
							{
								System.out.println("inside Validate Dt Method ");
								
								invoiceList.clear();
								billingList.clear();
								billingList.clear();
								payment1List.clear();
								payment2List.clear();
								getUnbilledBillingDetailsListForBranchLevel();
								getBilledDetailsListForBranchLevel();
								getPaymentDetailsListForBranchLevel();
								
							}
						}
						else
						{
							System.out.println("Null Dt(else null dt codn)");
							form.showDialogMessage("Please select from date & to date..!");
						}
					}
					else
					{
						System.out.println("else branch condn");
						form.showDialogMessage("Please select branch..!");
					}
				
				}
				else if(form.getCustomerlevel().getValue())
				{   
					System.out.println("inside Customer Level "+form.getCustomerlevel().getValue());
					
					
					if (!form.getPersonInfoComposite().getId().getValue().equals(""))
					{    
						System.out.println("Inside person compositeInfo"+form.getPersonInfoComposite().getId().getValue());
						invoiceList.clear();
						billingList.clear();
						billingList.clear();
						payment1List.clear();       
						payment2List.clear();
						
						getUnbilledBillingDetailsListCustomerLevel();
						getBilledDetailsListForCustomerLevel();
						getPaymentDetailsListForCustomerLevel();
						
					}
					else
					{ 
						form.showDialogMessage("Please select customer information..!");
					}
				}
				
				else if(form.getContractlevel().getValue())
				{   
					
					System.out.println("Inside contract level "+form.getContractlevel().getValue());
					
					if(!form.getPersonInfoComposite().getId().getValue().equals(""))
					{
						System.out.println("Composite Value"+form.getPersonInfoComposite().getId().getValue());
						
						if(form.getContractCount().getSelectedIndex()!=0)
						{
							System.out.println("Inside Contract Count with index Value "+form.getContractCount() +" index "+form.getContractCount().getSelectedIndex() );
							
							if(form.getContractStatus().getValue().equals(Contract.APPROVED))
							{
								System.out.println("Inside Status Approved");
								
								invoiceList.clear();
								billingList.clear();
								billingList.clear();
								payment1List.clear();
								payment2List.clear();
								getUnbilledBillingDetailsListForContractLevel();
								getBilledDetailsListForContractLevel();
								getPaymentDetailsListForContractLevel();
								
								
							}
							else{
								
								form.showDialogMessage("Contract status is not Approvrd");
								}
							}
						else{
							form.showDialogMessage("Please select atleast one contract");
						}
					}
					else
					{
						form.showDialogMessage("Please select customer information");
					}
				}
			}
			else{
				form.showDialogMessage("Please select how you want to see Outstandings ");
			}
		}
		
   if(event.getSource()==form.getCustomerlevel())
   {
	   System.out.println("Inside Customer Level  else level are disabled");
	       
	        form.unbilledTable.connectToLocal();
		    form.billedTable.connectToLocal();
		    form.paymentTable.connectToLocal();
	   
	   		form.tbTotalRevenue.setValue(null); 
	   		form.tbTotalRevenue.setEnabled(false);
	   		form.tbPaymentsOutstanding.setValue(null);
	   		form.tbPaymentsOutstanding.setEnabled(false);
	   		form.tbBilledOutstanding.setValue(null); 
	   		form.tbBilledOutstanding.setEnabled(false);
	   		form.tbUnbilledOutstanding.setValue(null);
	   		form.tbUnbilledOutstanding.setEnabled(false);
	         
			
			form.getCustomerlevel().setValue(true);
			form.getContractlevel().setValue(false);
			form.getBranchlevel().setValue(false);
			
			form.olbbranch.setSelectedIndex(0);
			form.olbbranch.setEnabled(false);
			
			form.dbFromDate.setEnabled(false);
			form.dbFromDate.setValue(null);
			
			form.dbToDate.setEnabled(false);
			form.dbToDate.setValue(null);
			
			form.personInfoComposite.clear();
			form.personInfoComposite.setEnable(true);
			
//			form.contractCount.clear();
//			form.contractCount.setValue(0, "Select");
			form.contractCount.setSelectedIndex(0);
			form.contractCount.setEnabled(false);
			
			form.contractStatus.setValue("");
			form.contractStatus.setEnabled(false);
			
			form.endDate.setValue(null);
			form.endDate.setEnabled(false);
			
			form.startDate.setValue(null);
			form.startDate.setEnabled(false);
			
		}
		if(event.getSource()==form.getContractlevel())
		{
			System.out.println("Inside Contract Level  else levels are disabled");
			
			form.unbilledTable.connectToLocal();
		    form.billedTable.connectToLocal();
		    form.paymentTable.connectToLocal();
			
			form.tbTotalRevenue.setValue(null); 
        	form.tbTotalRevenue.setEnabled(false);
		  	form.tbPaymentsOutstanding.setValue(null);
        	form.tbPaymentsOutstanding.setEnabled(false);
	   		form.tbBilledOutstanding.setValue(null); 
        	form.tbBilledOutstanding.setEnabled(false);
            form.tbUnbilledOutstanding.setValue(null);
        	form.tbUnbilledOutstanding.setEnabled(false);
			
			form.getContractlevel().setValue(true);
			form.getCustomerlevel().setValue(false);
			form.getBranchlevel().setValue(false);
			
			form.olbbranch.setSelectedIndex(0);
			form.olbbranch.setEnabled(false);
			
			form.dbFromDate.setEnabled(false);
			form.dbFromDate.setValue(null);
			
			form.dbToDate.setEnabled(false);
			form.dbToDate.setValue(null);
			
			form.personInfoComposite.clear();
			form.personInfoComposite.setEnable(true);
			
//			form.contractCount.clear();
//			form.contractCount.setValue(0, "Select");
			form.contractCount.setSelectedIndex(0);
			form.contractCount.setEnabled(true);
			
			form.contractStatus.setValue("");
			form.contractStatus.setEnabled(false);
			
			form.endDate.setValue(null);
			form.endDate.setEnabled(false);
			
			form.startDate.setValue(null);
			form.startDate.setEnabled(false);
					
		}
		
        if(event.getSource()==form.getBranchlevel())
        {
        	System.out.println("Inside Branch Level  else levels are disabled");
        	
        	form.unbilledTable.connectToLocal();
		    form.billedTable.connectToLocal();
		    form.paymentTable.connectToLocal();
        	
        	form.personInfoComposite.clear();
			form.personInfoComposite.setEnable(false);
			form.contractCount.setSelectedIndex(0);
			form.contractCount.setEnabled(false);
        	form.tbTotalRevenue.setValue(null); 
        	form.tbTotalRevenue.setEnabled(false);
		  	form.tbPaymentsOutstanding.setValue(null);
        	form.tbPaymentsOutstanding.setEnabled(false);
	   		form.tbBilledOutstanding.setValue(null); 
        	form.tbBilledOutstanding.setEnabled(false);
            form.tbUnbilledOutstanding.setValue(null);
        	form.tbUnbilledOutstanding.setEnabled(false);
			
			form.getBranchlevel().setValue(true);
			form.getContractlevel().setValue(false);
			form.getCustomerlevel().setValue(false);
			
			
			form.olbbranch.setEnabled(true);
			form.dbFromDate.setEnabled(true);
			form.dbToDate.setEnabled(true);
			
			
			form.contractStatus.setValue("");
			form.contractStatus.setEnabled(false);
			
			form.endDate.setValue(null);
			form.endDate.setEnabled(false);
			
			form.startDate.setValue(null);
			form.startDate.setEnabled(false);
			
		}
	
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		
	}

	@Override
	public void reactOnPrint() {
		
	}

	@Override
	public void reactOnEmail() {
		
	}

	@Override
	protected void makeNewModel() {
		
	}

//	public static void initalize() {
//		
//	}
	
	
public static OutstandingReportForm initialize()
{
		
	OutstandingReportForm form = new OutstandingReportForm();
		
	    OutstandingReportPresenter presenter =  new OutstandingReportPresenter(form, new DummyEntityOnlyForScreen());		
		AppMemory.getAppMemory().stickPnel(form);
		return form;		 
		
		
 }

@Override
public void onSelection(SelectionEvent<Suggestion> event)
{
	if (event.getSource().equals(form.personInfoComposite.getId())||event.getSource().equals(form.personInfoComposite.getName())||
			event.getSource().equals(form.personInfoComposite.getPhone())) 
	{
		if(form.rb3Contractlevel.getValue().equals(true)){
		getcontractIdfomCustomerID();
		}
	}
}

private void getcontractIdfomCustomerID() 
{
	
	MyQuerry querry = new MyQuerry();
  	Company c = new Company();
  	Vector<Filter> filtervec=new Vector<Filter>();
  	Filter filter = null;
  	filter = new Filter();
  	filter.setQuerryString("companyId");
	filter.setLongValue(c.getCompanyId());
	filtervec.add(filter);
	filter = new Filter();
	filter.setQuerryString("cinfo.count");
	filter.setIntValue(Integer.parseInt(form.personInfoComposite.getId().getValue().trim()));
	filtervec.add(filter);
	querry.setFilters(filtervec);
	querry.setQuerryObject(new Contract());

	async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() 
	{
		
		@Override
		public void onFailure(Throwable caught)
		{
		}

		@Override
		public void onSuccess(ArrayList<SuperModel> result) {
			
			if(result.size()==0){
				
				form.showDialogMessage("No contract exist for this customer");
				form.personInfoComposite.clear();
			}
			else{
			
			final List<String> contableList=  new ArrayList<String>();
			for(SuperModel model:result)
			{
				
				Contract conEntity = (Contract)model;
				contableList.add(conEntity.getCount()+"");
			}
			
			
			
				for(int i=0;i<contableList.size();i++){
				     form.getContractCount().addItem(contableList.get(i));
				}
		}
		}
		});
	
}
@Override
public void onRowCountChange(RowCountChangeEvent event) 
{    double  unbilltotal = 0;   
     double  billtotal = 0;
     double  paymenttotal = 0;
     
     if(form.unbilledTable.getDataprovider().getList().size()!=0||
    	form.billedTable.getDataprovider().getList().size()!=0||
    	form.paymentTable.getDataprovider().getList().size()!=0)
     {
	
	  for(int i = 0; i<form.unbilledTable.getDataprovider().getList().size();i++)
	  {
		  unbilltotal = unbilltotal + unbilledOsList.get(i).getTotal();  
	  }
	  
	  for(int i = 0; i<form.billedTable.getDataprovider().getList().size();i++)
	  {
		  billtotal = billtotal + billedOsList.get(i).getTotal();  
	  }
	  
	  for(int i = 0; i<form.paymentTable.getDataprovider().getList().size();i++)
	  {
		  paymenttotal = paymenttotal + paymentOsList.get(i).getTotal();  
	  }
	  
	  form.tbUnbilledOutstanding.setValue(unbilltotal);
	  form.tbUnbilledOutstanding.setEnabled(false);
	  
	  form.tbBilledOutstanding.setValue(billtotal);
	  form.tbBilledOutstanding.setEnabled(false);
	  
	  
	  form.tbPaymentsOutstanding.setValue(paymenttotal);
	  form.tbPaymentsOutstanding.setEnabled(false);
		
		double NetRevenue = unbilltotal + billtotal + paymenttotal ;
		form.tbTotalRevenue.setValue(NetRevenue);
		form.tbTotalRevenue.setEnabled(false);
		

     }
     else{
    	 
    	 form.tbUnbilledOutstanding.setValue(unbilltotal);
   	     form.tbUnbilledOutstanding.setEnabled(false);
   	  
   	     form.tbBilledOutstanding.setValue(billtotal);
   	     form.tbBilledOutstanding.setEnabled(false);
   	  
   	  
   	     form.tbPaymentsOutstanding.setValue(paymenttotal);
   		 form.tbPaymentsOutstanding.setEnabled(false);
   		
   		 double NetRevenue = unbilltotal + billtotal + paymenttotal ;
   		 
   		 form.tbTotalRevenue.setValue(NetRevenue);
   		 form.tbTotalRevenue.setEnabled(false);
    	 
     }
     
    }


}
