package com.slicktechnologies.client.views.account.OutstandingReport;

import com.google.gwt.cell.client.TextCell;
import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.Outstanding;
import com.slicktechnologies.shared.common.inventory.ExpenseCost;
import com.slicktechnologies.shared.common.inventory.MaterialConsumptionReport;

public class OutstandingTable extends SuperTable<Outstanding>
{
	
	TextColumn<Outstanding> getColumndocType;
	TextColumn<Outstanding> getColumndocID;
	TextColumn<Outstanding> getColumnDate ;
	TextColumn<Outstanding> getColumnBranch ;
	TextColumn<Outstanding> getColumnCustId ;
	TextColumn<Outstanding> getColumnCustName ;
	TextColumn<Outstanding> getColumnCustCellNo ;
	TextColumn<Outstanding> getColumnCustPOC ;
	TextColumn<Outstanding> getColumnStatus ;
	Column<Outstanding,String> getColumntotal;
	
	
	public OutstandingTable() 
	{
		super();
		setHeight("300px");
	}
	
	
	@Override
	public void createTable() {

		addColumndocType();
		addColumndocID();
		addColumnDate();
		addColumnBranch();
		addColumnCustId();
		addColumnCustName();
		addColumnCustCellNo();
		addColumnCustPOC();
		addColumnStatus();
		addColumntotal();
		
	}

	
private void addColumndocType() {

	
	getColumndocType = new TextColumn<Outstanding>() 
			{
		@Override
		public String getValue(Outstanding object) {
			if (object.getDocType()!=null){
				return object.getDocType();
			}
			return null;
		}
	};
	table.addColumn(getColumndocType, "Doc Type");
	table.setColumnWidth(getColumndocType,90,Unit.PX);

		
	}

private void addColumndocID() 
{

	getColumndocID = new TextColumn<Outstanding>() {
		@Override
		public String getValue(Outstanding object) {
			if (object.getDocID() == -1)
				return "N.A";
			else
				return object.getDocID() + "";
		}
	};
	table.addColumn(getColumndocID, "Doc ID");
	table.setColumnWidth(getColumndocID,90,Unit.PX);
		
	}


private void addColumnDate() {
	getColumnDate = new TextColumn<Outstanding>(){
		@Override
		public String getValue(Outstanding object) {
			if (object.getDate() == "")
				return "N.A";
			else
				return object.getDate() + "";
		}
		
	};
	table.addColumn(getColumnDate, "Date");
	table.setColumnWidth(getColumnDate,90,Unit.PX);
		
	}


private void addColumnBranch() {

	getColumnBranch = new TextColumn<Outstanding>(){
		@Override
		public String getValue(Outstanding object) {
			if (object.getBranch() == "")
				return "N.A";
			else
				return object.getBranch() + "";
		}
		
	};
	table.addColumn(getColumnBranch, "Branch");
	table.setColumnWidth(getColumnBranch,90,Unit.PX);
		
	
		
	}


private void addColumnCustId()
{
	getColumnCustId = new TextColumn<Outstanding>(){
		@Override
		public String getValue(Outstanding object) {
			if (object.getCustId() == -1)
				return "N.A";
			else
				return object.getCustId() + "";
		}
		
	};
	table.addColumn(getColumnCustId, "Customer Id");
	table.setColumnWidth(getColumnCustId,90,Unit.PX);
		
	}


private void addColumnCustName() {

	getColumnCustName = new TextColumn<Outstanding>(){
		@Override
		public String getValue(Outstanding object) {
			if (object.getCustName() == "")
				return "N.A";
			else
				return object.getCustName() + "";
		}
		
	};
	table.addColumn(getColumnCustName, "Customer Name");
	table.setColumnWidth(getColumnCustName,90,Unit.PX);
		
	}


private void addColumnCustCellNo() {
	
	getColumnCustCellNo = new TextColumn<Outstanding>(){
		@Override
		public String getValue(Outstanding object) {
			if (object.getCustCellNo() == "")
				return "N.A";
			else
				return object.getCustCellNo() + "";
		}
		
	};
	table.addColumn(getColumnCustCellNo, "Cell No");
	table.setColumnWidth(getColumnCustCellNo,90,Unit.PX);
		
	}


private void addColumnCustPOC() {
	
	getColumnCustPOC = new TextColumn<Outstanding>(){
		@Override
		public String getValue(Outstanding object) {
			if (object.getPoc() == "")   
				return "N.A";
			else
				return object.getPoc() + "";
		}
		
	};
	
	table.addColumn(getColumnCustPOC, "POC Name");
	table.setColumnWidth(getColumnCustPOC,90,Unit.PX);
	
	}


private void addColumnStatus() 
{
	getColumnStatus = new TextColumn<Outstanding>(){
		@Override
		public String getValue(Outstanding object) {
			if (object.getStatus().equals("")&& object.getStatus()!= null)   
				return "N.A";
			else
				return object.getStatus()+"" ;
		}
		
	};
	
	table.addColumn(getColumnStatus, "Status");
	table.setColumnWidth(getColumnStatus,90,Unit.PX);
	
		
	}


private void addColumntotal() 
{

	TextCell editCell=new TextCell();
	getColumntotal=new Column<Outstanding,String>(editCell)
	{
		
		@Override
		public String getCellStyleNames(Context context, Outstanding object) {
			if(object.getDocType().equals("In")){
				 return "red";
			}
			return "black";
	}
		
		
		@Override
		public String getValue(Outstanding object)
		{
//			object.setTotal(object.getQuantity()*object.getPrice());
			if(object.getDocType().equals("Billing Details")){
				double value=-object.getTotal();
				return value+"";
			}
			else if(object.getDocType().equals("Invoice Details")){
				double value=-object.getTotal();
				return value+"";
			}
			else if(object.getDocType().equals("Payment Details")){
				double value=-object.getTotal();
				return value+"";
			}
//			else{
//				return object.getTotal()+"Billing Details";
//			}
			return object.getTotal()+"";
		}
			};
			table.addColumn(getColumntotal,"Total");
			table.setColumnWidth(getColumntotal,90,Unit.PX);
	}


/***********************************************End Here **************************************************/
	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
