package com.slicktechnologies.client.views.account.OutstandingReport;

import java.util.ArrayList;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.DummyEntityOnlyForScreen;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;


public class OutstandingReportForm extends FormScreen <DummyEntityOnlyForScreen>  implements ChangeHandler 
{
	PersonInfoComposite personInfoComposite;
	Button searchbutton;
	ListBox contractCount;
	RadioButton rb1Branchlevel , rb2Customerlevel , rb3Contractlevel ; 
	DateBox dbFromDate;
	DateBox dbToDate;
	Label lblTotalRevenue,lblUnbilledOutstanding,lblBilledOutstanding,
	      lblPaymentsOutstanding,lblOutstandingAmnt,lblReceivedAmt;
	ObjectListBox<Branch> olbbranch ; 
	TextBox contractStatus;
	DateBox startDate,endDate;
	DoubleBox tbTotalRevenue, tbUnbilledOutstanding ,tbBilledOutstanding ,tbPaymentsOutstanding  ;
	
	
    OutstandingTable unbilledTable ;
    OutstandingTable billedTable   ;
    OutstandingTable paymentTable  ;
    double netPayable ;
	
	final GenricServiceAsync async=GWT.create(GenricService.class);
	
	public OutstandingReportForm() 
	{
		super();
		toggleAppHeaderBarMenu();
	       createGui();
	       
	       unbilledTable.connectToLocal();
	       billedTable.connectToLocal();
	       paymentTable.connectToLocal();
	}
	

	@SuppressWarnings("unused")
	private void initalizeWidget()
	{
		
		searchbutton= new Button("Search");
		searchbutton.getElement().getStyle().setWidth(80, Unit.PX);
		searchbutton.getElement().getStyle().setHeight(30, Unit.PX);
		
		
		/*************************** person Composite Initialization ******************************/
		personInfoComposite=AppUtility.customerInfoComposite(new Customer());
		personInfoComposite.getCustomerId().getHeaderLabel().setText("* Customer ID");
		personInfoComposite.getCustomerName().getHeaderLabel().setText("* Customer Name");
		personInfoComposite.getCustomerCell().getHeaderLabel().setText("* Customer Cell");
		
		//***********************rohan changes here for searching for by branch and sales person ****
		
		olbbranch =new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbbranch);
		
		//************************************changes ends here ********************
		
		dbFromDate=new DateBoxWithYearSelector();
		dbToDate=new DateBoxWithYearSelector();
		DateTimeFormat dateFormat = DateTimeFormat.getFormat("dd-MM-yyyy");
		dbFromDate.setFormat(new DateBox.DefaultFormat(dateFormat));
		dbToDate.setFormat(new DateBox.DefaultFormat(dateFormat));

		contractCount= new ListBox();
		contractCount.addItem("Select");
		contractCount.addChangeHandler(this);
		
		contractStatus= new TextBox();
		contractStatus.setEnabled(false);
		
//		startDate= new DateBox();
		startDate = new DateBoxWithYearSelector();
		startDate.setEnabled(false);
		
//		endDate = new DateBox();
		endDate = new DateBoxWithYearSelector();
		endDate.setEnabled(false);
		
		
		
		rb1Branchlevel = new RadioButton("");
		rb2Customerlevel = new RadioButton("");
		rb3Contractlevel = new RadioButton("");
		
		lblTotalRevenue = new Label("Total Revenue "); 
		lblUnbilledOutstanding = new Label("Unbilled Outstanding ");
		lblBilledOutstanding = new Label("Billed Outstanding ");
		lblPaymentsOutstanding = new Label("Payments Outstanding ");
//		lblOutstandingAmnt = new Label("Outstanding Amount ");
//		lblReceivedAmt = new Label("Received Amount");
//		
		tbTotalRevenue = new DoubleBox();
		tbUnbilledOutstanding = new DoubleBox();
		tbBilledOutstanding = new DoubleBox();
		tbPaymentsOutstanding = new DoubleBox();
//		tbOutstandingAmnt = new TextBox();
//		tbReceivedAmt = new TextBox();
		
		unbilledTable = new OutstandingTable();
		
		billedTable = new OutstandingTable();
		paymentTable = new OutstandingTable();
		
	
	}

	@Override
	public void createScreen() {
        
		initalizeWidget();
		
	FormFieldBuilder fbuilder;
		
		fbuilder = new FormFieldBuilder();
		FormField flblChooseYourWay= fbuilder.setlabel("Please Select Anyone ").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		//////////////////////////////
		fbuilder = new FormFieldBuilder(" Branch Level",rb1Branchlevel);
		FormField rbBranchLevel= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();	
		
		fbuilder = new FormFieldBuilder(" Customer Level",rb2Customerlevel);
		FormField rbCustomerLevel= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();	
		
		fbuilder = new FormFieldBuilder(" Contract Level",rb3Contractlevel);
		FormField rbContractLevel= fbuilder.setMandatory(true).setMandatoryMsg("Registration No. is Mandatory").setRowSpan(0).setColSpan(0).build();	
		
	
		/////////////////////////////
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingBranchInformation=fbuilder.setlabel("Branch Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("*Branch ",olbbranch);
		FormField fbranch=fbuilder.setMandatory(true).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("From Date",dbFromDate);
		FormField ffromDate=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("TO Date",dbToDate);
		FormField ftodate=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingCustomerInformation=fbuilder.setlabel("Customer Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		
		fbuilder = new FormFieldBuilder("",personInfoComposite);
		FormField fpersonInfoComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder(" ",searchbutton);
		FormField fsearchbutton= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder();
		FormField   fblnk = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		////////////////////////////////////////////////////////////////
		fbuilder = new FormFieldBuilder();
		FormField fgroupingContractInformation=fbuilder.setlabel("Contract Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		fbuilder = new FormFieldBuilder("Contract ID",contractCount);
		FormField fcontractCount= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Contract Status ",contractStatus);
		FormField fcontractStatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Start Date ",startDate);
		FormField fstartDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("End Date ",endDate);
		FormField fendDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		////////////////////////////////// summery table ////////////////////// 
		FormField fgroupingSummary=fbuilder.setlabel("Summary Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();

		fbuilder = new FormFieldBuilder("",lblTotalRevenue);
		FormField lblTotalReceived= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",lblUnbilledOutstanding);
		FormField lblUnbilledOutstanding= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",lblBilledOutstanding);
		FormField lblBilledOutstanding= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",lblPaymentsOutstanding);
		FormField lblPaymentsOutstanding= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
//		fbuilder = new FormFieldBuilder("",lblOutstandingAmnt);
//		FormField lblOutstandingAmnt = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
//		
//		fbuilder = new FormFieldBuilder("",lblReceivedAmt);
//		FormField lblReceivedAmt= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("",tbTotalRevenue);
		FormField tbTotalReceived= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",tbUnbilledOutstanding);
		FormField tbUnbilledOutstanding= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",tbBilledOutstanding);
		FormField tbBilledOutstanding= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",tbPaymentsOutstanding);
		FormField tbPaymentsOutstanding= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
//		fbuilder = new FormFieldBuilder("",tbOutstandingAmnt);
//		FormField tbOutstandingAmnt = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
//		
//		fbuilder = new FormFieldBuilder("",tbReceivedAmt);
//		FormField tbReceivedAmt= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		

		///////////////////////////// end here ////////////////////////////
	
		fbuilder = new FormFieldBuilder();
		FormField fgroupingunbilledtable=fbuilder.setlabel(" Unbilled Outstanding ").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		fbuilder = new FormFieldBuilder("",unbilledTable.getTable());
		FormField funbilledtable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(5).build();

		fbuilder = new FormFieldBuilder();
		FormField fgroupingbilledtable=fbuilder.setlabel(" Billed Outstanding ").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		fbuilder = new FormFieldBuilder("",billedTable.getTable());
		FormField fbilledtable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(5).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingepaymenttable=fbuilder.setlabel("Payment Received ").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		fbuilder = new FormFieldBuilder("",paymentTable.getTable());
		FormField fPaymenttable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(5).build();

		
		FormField[][] formFields = 
			{ 
				{flblChooseYourWay},
				{rbBranchLevel, rbCustomerLevel,rbContractLevel},
				
				{fgroupingBranchInformation},
				{fbranch , ffromDate , ftodate },
				
				{fgroupingCustomerInformation},
				{fpersonInfoComposite},
				
				{fgroupingContractInformation},
				{fcontractCount,fcontractStatus,fstartDate,fendDate},
				
				{fblnk,fblnk,fblnk,fsearchbutton},
				
				{fgroupingSummary},
				{lblTotalReceived,tbTotalReceived},
				{lblUnbilledOutstanding,tbUnbilledOutstanding},
				{lblBilledOutstanding,tbBilledOutstanding},
				{lblPaymentsOutstanding,tbPaymentsOutstanding},
				{fgroupingunbilledtable},
				{funbilledtable},
				{fgroupingbilledtable},
				{fbilledtable},
				{fgroupingepaymenttable},
				{fPaymenttable},
				
		    };
		this.fields=formFields;
		
	}

	@Override
	public void updateModel(DummyEntityOnlyForScreen model) 
	{
		
	}

	@Override
	public void updateView(DummyEntityOnlyForScreen model)
	{
		
	}
	//////////////////////////////////////////////////////
//	private void toggleAppHeaderBarMenu() {
//		InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
//		for(int k=0;k<menus.length;k++)
//		{
//			String text=menus[k].getText();
//			if(text.contains("Search"))
//			{
//				menus[k].setVisible(true); 
//			}
//			else
//				menus[k].setVisible(false);  		   
//			
//		}
//		
//	}

	/////////////////////////////////////////////////////
	

	@Override
	public void toggleAppHeaderBarMenu() {

		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Print")||text.equals("Download")||text.equals("Discard"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Print")||text.equals("Download")||text.equals("Discard"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Print")||text.equals("Download")||text.equals("Discard"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
//		
		AuthorizationHelper.setAsPerAuthorization(Screen.OUTSTANDINGREPORT,LoginPresenter.currentModule.trim());
	
	}
	
	      /********************************** getters & setters *********************************/
	

	public DateBox getDbFromDate() {
		return dbFromDate;
	}

	public void setDbFromDate(DateBox dbFromDate) {
		this.dbFromDate = dbFromDate;
	}

	public DateBox getDbToDate() {
		return dbToDate;
	}

	public void setDbToDate(DateBox dbToDate) {
		this.dbToDate = dbToDate;
	}

	public Button getSearchbutton() {
		
		return searchbutton;
	}
	
	public void setSearchButton(Button searchbutton) {
		this.searchbutton = searchbutton ;
	}
	
	public ListBox getContractCount() {
		return contractCount;
	}

	public void setContractCount(ListBox contractCount) {
		this.contractCount = contractCount;
	}

	public TextBox getContractStatus() {
		return contractStatus;
	}

	public void setContractStatus(TextBox contractStatus) {
		this.contractStatus = contractStatus;
	}

	
	public ObjectListBox<Branch> getOlbbranch() {
		return olbbranch;
	}

	public void setOlbbranch(ObjectListBox<Branch> olbbranch) {
		this.olbbranch = olbbranch;
	}
	

	public RadioButton getBranchlevel() {
		return rb1Branchlevel;
	}

	public void setBranchLevel(RadioButton Branchlevel) {
		rb1Branchlevel = Branchlevel;
	}

	public RadioButton getCustomerlevel() {
		return rb2Customerlevel;
	}

	public void setCustomerlevel(RadioButton customerlevel) {
		rb2Customerlevel = customerlevel;
	}

	public RadioButton getContractlevel() {
		return rb3Contractlevel;
	}

	public void setContractlevel(RadioButton contractlevel) {
		this.rb3Contractlevel = contractlevel;
	}
	
	public PersonInfoComposite getPersonInfoComposite() {
		return personInfoComposite;
	}

	public void setPersonInfoComposite(PersonInfoComposite personInfoComposite) {
		this.personInfoComposite = personInfoComposite;
	}
	
	public DateBox getStartDate() {
		return startDate;
	}

	public void setStartDate(DateBox startDate) {
		this.startDate = startDate;
	}

	public DateBox getEndDate() {
		return endDate;
	}

	public void setEndDate(DateBox endDate) {
		this.endDate = endDate;
	}
	
	
//	public double getNetAmt() {
//		return netPayable;
//	}

//	public void setNetAmt( double netPayable) 
//	{
//		this.netPayable = netPayable;
//	}
	

	
	/////////////////////////////////////////////////////////
	
	
	public DoubleBox gettbTotalRevenue() {  //1
		return tbTotalRevenue;
	}



	public void settbTotalRevenue(DoubleBox tbTotalRevenue) {
		this.tbTotalRevenue = tbTotalRevenue;
	}



	public DoubleBox gettbUnbilledOutstanding() {//2
		return tbUnbilledOutstanding;
	}



	public void settbUnbilledOutstanding(DoubleBox tbUnbilledOutstanding) {
		this.tbUnbilledOutstanding = tbUnbilledOutstanding;
	}

	
	public DoubleBox gettbBilledOutstanding() {  //3
		return tbBilledOutstanding;
	}

	public void settbBilledOutstanding(DoubleBox tbBilledOutstanding) {
		this.tbBilledOutstanding = tbBilledOutstanding;
	}



	public DoubleBox gettbPaymentsOutstanding() {//4
		return tbPaymentsOutstanding;
	}

	public void settbPaymentsOutstanding(DoubleBox tbPaymentsOutstanding) {
		this.tbPaymentsOutstanding = tbPaymentsOutstanding;
	}
	
//	public DoubleBox gettbOutstandingAmnt() {  //5
//		return tbOutstandingAmnt;
//	}
//
//
//
//	public void settbOutstandingAmnt(DoubleBox tbOutstandingAmnt) {
//		this.tbOutstandingAmnt = tbOutstandingAmnt;
//	}
//
//
//
//	public DoubleBox gettbReceivedAmt() {//6
//		return tbReceivedAmt;
//	}
//
//
//
//	public void settbReceivedAmt(DoubleBox tbReceivedAmt) {
//		this.tbReceivedAmt = tbReceivedAmt;
//	}
//	

	public OutstandingTable getunbilledTable() 
	{//7
		return unbilledTable;
	}



	public void setunbilledTable(OutstandingTable unbilledTable) {
		this.unbilledTable = unbilledTable;
	}
	

	public OutstandingTable getbilledTable() 
	{//8
		return billedTable;
	}



	public void setbilledTable(OutstandingTable billedTable) {  
		this.billedTable = billedTable;
	}
	

	public OutstandingTable getpaymenttable() 
	{//9
		return paymentTable;
	}

	public void setpaymenttable(OutstandingTable paymentTable)
    {
		this.paymentTable = paymentTable;
	}
	
	@Override
	public void onChange(ChangeEvent event)
	
	{
		
		  if (this.getContractCount().getSelectedIndex()==0){
			  unbilledTable.connectToLocal();
			  billedTable.connectToLocal();
			  paymentTable.connectToLocal();
			  
			  this.getContractStatus().setValue("");
			  this.getStartDate().setValue(null);
			  this.getEndDate().setValue(null);
		  }
		  else{
			  unbilledTable.connectToLocal();
			  billedTable.connectToLocal();
			  paymentTable.connectToLocal();
			  getContractDetails();
		  }
	}

	private void getContractDetails() {

		// TODO Auto-generated method stub
		showWaitSymbol();
		Timer timer=new Timer() 
	     {
			@Override
			public void run() 
			{
				
				MyQuerry querry = new MyQuerry();
				Company c=new Company();
				Vector<Filter> filtervec=new Vector<Filter>();
				Filter filter = null;
				
				filter = new Filter();
				filter.setQuerryString("companyId");
				filter.setLongValue(c.getCompanyId());
				filtervec.add(filter);
				
				filter = new Filter();   
				filter.setQuerryString("count");
				int index = contractCount.getSelectedIndex();
				filter.setIntValue(Integer.parseInt(contractCount.getItemText(index)));
				filtervec.add(filter);
				querry.setFilters(filtervec);
				querry.setQuerryObject(new Contract());
				
				
				
				async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() 
						{
					
					@Override
					public void onFailure(Throwable caught) {
					}
		
					@Override
					public void onSuccess(ArrayList<SuperModel> result)
					{
						
						System.out.println("result size is ========"+result.size());
						
						for(SuperModel model:result)
						{
							Contract conEntity=(Contract)model;
//							contractCount.setValue(0, "Select");
							contractStatus.setValue(conEntity.getStatus());
							startDate.setValue(conEntity.getStartDate());
							endDate.setValue(conEntity.getEndDate());
						}
						}
					 });
		
		 hideWaitSymbol();
			}
	     };
  	 timer.schedule(2000);
		
	}

}
