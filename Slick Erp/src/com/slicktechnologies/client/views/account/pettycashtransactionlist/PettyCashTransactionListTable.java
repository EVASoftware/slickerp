package com.slicktechnologies.client.views.account.pettycashtransactionlist;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.PettyCashDeposits;
import com.slicktechnologies.shared.common.PettyCashTransaction;

public class PettyCashTransactionListTable extends SuperTable<PettyCashTransaction> {
	
	
	TextColumn<PettyCashTransaction> getColumnId;
	TextColumn<PettyCashTransaction> getColumnDocId;
	TextColumn<PettyCashTransaction> getColumnDocName;
	TextColumn<PettyCashTransaction> getColumnPettyCashName;
	TextColumn<PettyCashTransaction> getColumnDocDate;
	TextColumn<PettyCashTransaction> getColumnPersonResponsible;
	TextColumn<PettyCashTransaction> getColumnApproverName;
	TextColumn<PettyCashTransaction> getColumnOpeningStock;
	TextColumn<PettyCashTransaction> getColumnAction;
	TextColumn<PettyCashTransaction> getColumnTransactionAmt;
	TextColumn<PettyCashTransaction> getColumnClosingStock;
	TextColumn<PettyCashTransaction> getColumnTransactionTime;
	
	TextColumn<PettyCashTransaction> getColumnDocumentDate;
	/**Added by sheetal:23-12-2021
	   Adding column Expense category for Hygienic Pest Control�**/
	TextColumn<PettyCashTransaction> getColumnExpenseCategory;
	
	/** Sheetal : 07-04-2022
	 * Des : Adding column of description for ankita pest control **/
	TextColumn<PettyCashTransaction> getColumnDescription;
	
	PettyCashTransactionListTable(){
		super();
		setHeight("1600px");
		
	}

	@Override
	public void createTable() {
		// TODO Auto-generated method stub
		
		createColumnId();
		
		CreateColumnPettyCashName();
		CreateColumnTrancDate();
		CreateColumnTranTime();
		CreateColumnApproverName();
		CreateColumnExpenseCategory();
		CreateColumnOpeningStock();
		CreateColumnAction();
		CreateColumnTransactionAmt();
		CreateColumnClosingStock();
		createColumnDocId();
		CreateColumnDocName();
		createColumnDocDate();
		CreateColumnPersonResponsoble();
		CreateColumnDescription();
	}
	
	private void createColumnDocDate() {
		getColumnDocumentDate = new TextColumn<PettyCashTransaction>() {
			
			@Override
			public String getValue(PettyCashTransaction object) {
				if(object.getDocumentDate()!=null){
					return AppUtility.parseDate(object.getDocumentDate())+"";
				}
				return "";
			}
		};
		
		table.addColumn(getColumnDocumentDate,"Doc.Date");
		table.setColumnWidth(getColumnDocumentDate, 90,Unit.PX);
		getColumnDocumentDate.setSortable(true);
	}
	
	private void createColumnSortingOnDocumentDate() {
		List<PettyCashTransaction> list = getDataprovider().getList();
		columnSort = new ListHandler<PettyCashTransaction>(list);
		columnSort.setComparator(getColumnDocumentDate,new Comparator<PettyCashTransaction>() {
			@Override
			public int compare(PettyCashTransaction e1,PettyCashTransaction e2) {
				if (e1 != null && e2 != null) {
					if (e1.getDocumentDate() != null&& e2.getDocumentDate() != null) {
						return e1.getDocumentDate().compareTo(e2.getDocumentDate());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void CreateColumnTranTime() {
		
		getColumnTransactionTime = new TextColumn<PettyCashTransaction>() {
			
			@Override
			public String getValue(PettyCashTransaction object) {
				return object.getTransactionTime();
			}
		};
		
		table.addColumn(getColumnTransactionTime,"Tran.Time");
		table.setColumnWidth(getColumnTransactionTime, 90,Unit.PX);
		getColumnTransactionTime.setSortable(true);
	}

	private void CreateColumnPettyCashName() {
		
		getColumnPettyCashName = new TextColumn<PettyCashTransaction>() {
			
			@Override
			public String getValue(PettyCashTransaction object) {
				return object.getPettyCashName();
			}
		};
		
		table.addColumn(getColumnPettyCashName,"Petty Cash Name");
		table.setColumnWidth(getColumnPettyCashName, 100,Unit.PX);
		getColumnPettyCashName.setSortable(true);
	}

	private void CreateColumnDocName() {

		getColumnDocName = new TextColumn<PettyCashTransaction>() {
			
			@Override
			public String getValue(PettyCashTransaction object) {
				return object.getDocumentName();
			}
		};
		table.addColumn(getColumnDocName,"Document Name");
		table.setColumnWidth(getColumnDocName, 90,Unit.PX);
		getColumnDocName.setSortable(true);
	
	}

	private void createColumnId() {
		getColumnId = new TextColumn<PettyCashTransaction>() {
			
			@Override
			public String getValue(PettyCashTransaction object) {
				return object.getCount()+"";
			}
		};
		table.addColumn(getColumnId,"ID");
		table.setColumnWidth(getColumnId, 90,Unit.PX);
		getColumnId.setSortable(true);
	}

	private void createColumnDocId() {
	
		getColumnDocId = new TextColumn<PettyCashTransaction>() {
			
			@Override
			public String getValue(PettyCashTransaction object) {
				return object.getDocumentId()+"";
			}
		};
		table.addColumn(getColumnDocId,"Document Id");
		table.setColumnWidth(getColumnDocId, 110,Unit.PX);
		getColumnDocId.setSortable(true);
	}

	private void CreateColumnTrancDate() {
		getColumnDocDate = new TextColumn<PettyCashTransaction>() {
			
			@Override
			public String getValue(PettyCashTransaction object) {
				return AppUtility.parseDate(object.getTransactionDate())+"";
			}
		};
		
		table.addColumn(getColumnDocDate,"Tran.Date");
		table.setColumnWidth(getColumnDocDate, 90,Unit.PX);
		getColumnDocDate.setSortable(true);
		
	}

	private void CreateColumnPersonResponsoble() {
		getColumnPersonResponsible = new TextColumn<PettyCashTransaction>() {
			
			@Override
			public String getValue(PettyCashTransaction object) {
				return object.getPersonResponsible();
			}
		};
		table.addColumn(getColumnPersonResponsible,"Person Responsible");
		table.setColumnWidth(getColumnPersonResponsible, 110,Unit.PX);
		getColumnPersonResponsible.setSortable(true);
	}

	private void CreateColumnApproverName() {
		getColumnApproverName = new TextColumn<PettyCashTransaction>() {
			
			@Override
			public String getValue(PettyCashTransaction object) {
				return object.getAppoverName();
			}
		};
		table.addColumn(getColumnApproverName,"Approver Name");
		table.setColumnWidth(getColumnApproverName, 90,Unit.PX);
		getColumnApproverName.setSortable(true);
	}
	
	private void CreateColumnExpenseCategory() {
		getColumnExpenseCategory = new TextColumn<PettyCashTransaction>() {
			
			@Override
			public String getValue(PettyCashTransaction object) {
				return object.getexpenseCategory();
			}
		};
		table.addColumn(getColumnExpenseCategory,"Expense Category");
		table.setColumnWidth(getColumnExpenseCategory, 130,Unit.PX);
		getColumnExpenseCategory.setSortable(true);
	}

	private void CreateColumnOpeningStock() {
		getColumnOpeningStock = new TextColumn<PettyCashTransaction>() {
			
			@Override
			public String getValue(PettyCashTransaction object) {
				return object.getOpeningStock()+"";
			}
		};
		table.addColumn(getColumnOpeningStock,"Opening Balance");
		table.setColumnWidth(getColumnOpeningStock, 90,Unit.PX);
		getColumnOpeningStock.setSortable(true);
	}

	private void CreateColumnAction() {
		getColumnAction = new TextColumn<PettyCashTransaction>() {
			
			@Override
			public String getValue(PettyCashTransaction object) {
				return object.getAction();
			}
		};
		table.addColumn(getColumnAction,"Action");
		table.setColumnWidth(getColumnAction,80,Unit.PX);
		getColumnAction.setSortable(true);
	}

	private void CreateColumnTransactionAmt() {

		getColumnTransactionAmt = new TextColumn<PettyCashTransaction>() {
			
			@Override
			public String getValue(PettyCashTransaction object) {
				return object.getTransactionAmount()+"";
			}
		};
		
		table.addColumn(getColumnTransactionAmt,"Transaction Amt");
		table.setColumnWidth(getColumnTransactionAmt, 90,Unit.PX);
		getColumnTransactionAmt.setSortable(true);
	}

	private void CreateColumnClosingStock() {
		getColumnClosingStock = new TextColumn<PettyCashTransaction>() {
			
			@Override
			public String getValue(PettyCashTransaction object) {
				return object.getClosingStock()+"";
			}
		};
		
		table.addColumn(getColumnClosingStock,"Closing Balance");
		table.setColumnWidth(getColumnClosingStock, 90,Unit.PX);
		getColumnClosingStock.setSortable(true);
	}
	
	private void CreateColumnDescription() {
		getColumnDescription = new TextColumn<PettyCashTransaction>() {

			@Override
			public String getValue(PettyCashTransaction object) {
				return object.getDescription();
			}
		};
		
		table.addColumn(getColumnDescription,"Description");
		table.setColumnWidth(getColumnDescription, 140,Unit.PX);
		getColumnDescription.setSortable(true);
		
	}

	
	public void addColumnSorting() {
		
		createColumnSortingId();
		createColumnSortingDocId();
		CeateColumnSortingDocName();
		CreateColumnSortingPettyCashName();
		CreateColumnSortingDocDate();
		CreateColumnSortingPersonResponsoble();
		CreateColumnSortingApproverName();
		CreateColumnSortingExpenseCategory();
		CreateColumnSortingOpeningStock();
		CreateColumnSortingAction();
		CreateColumnSortingTransactionAmt();
		CreateColumnSortingClosingStock();
		CreateColumnSortingTransactionTime();
		createColumnSortingOnDocumentDate();
		CreateColumnSortingDescription();
		
	}
	
	
	
	
	private void CreateColumnSortingDescription() {
		List<PettyCashTransaction> list=getDataprovider().getList();
		columnSort=new ListHandler<PettyCashTransaction>(list);
		columnSort.setComparator(getColumnDescription, new Comparator<PettyCashTransaction>(){
				
			@Override
			public int compare(PettyCashTransaction e1,PettyCashTransaction e2) {
				if (e1 != null && e2 != null) {
					if (e1.getDescription() != null && e2.getDescription() != null) {
						return e1.getDescription().compareTo(e2.getDescription());
					}
				         } else {
				        	 return 0;
				         }
				         return 0;
				}
			});
		   table.addColumnSortHandler(columnSort);
	}

	private void CreateColumnSortingTransactionTime() {

		List<PettyCashTransaction> list=getDataprovider().getList();
		columnSort=new ListHandler<PettyCashTransaction>(list);
		columnSort.setComparator(getColumnTransactionTime, new Comparator<PettyCashTransaction>()
				{
			@Override
			public int compare(PettyCashTransaction e1,PettyCashTransaction e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getTransactionTime()!=null && e2.getTransactionTime()!=null){
						return e1.getTransactionTime().compareTo(e2.getTransactionTime());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}

	private void CreateColumnSortingPettyCashName() {

		List<PettyCashTransaction> list = getDataprovider().getList();
		columnSort = new ListHandler<PettyCashTransaction>(list);
		columnSort.setComparator(getColumnPettyCashName,new Comparator<PettyCashTransaction>() {
			@Override
			public int compare(PettyCashTransaction e1,PettyCashTransaction e2) {
				if (e1 != null && e2 != null) {
					if (e1.getPettyCashName() != null && e2.getPettyCashName() != null) {
						return e1.getPettyCashName().compareTo(e2.getPettyCashName());
					}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
		
	}

	private void CeateColumnSortingDocName() {

		List<PettyCashTransaction> list = getDataprovider().getList();
		columnSort = new ListHandler<PettyCashTransaction>(list);
		columnSort.setComparator(getColumnDocName,new Comparator<PettyCashTransaction>() {
			@Override
			public int compare(PettyCashTransaction e1,PettyCashTransaction e2) {
				if (e1 != null && e2 != null) {
					if (e1.getDocumentName() != null && e2.getDocumentName() != null) {
						return e1.getDocumentName().compareTo(e2.getDocumentName());
					}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	private void CreateColumnSortingClosingStock() {

		List<PettyCashTransaction> list = getDataprovider().getList();
		columnSort = new ListHandler<PettyCashTransaction>(list);
		columnSort.setComparator(getColumnClosingStock,new Comparator<PettyCashTransaction>() {
					@Override
					public int compare(PettyCashTransaction e1,PettyCashTransaction e2) {
						if (e1 != null && e2 != null) {
							if (e1.getClosingStock() == e2.getClosingStock()) {
								return 0;
							}
							if (e1.getClosingStock() > e2.getClosingStock()) {
								return 1;
							} else {
								return -1;
							}
						} else {
							return 0;
						}
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	private void CreateColumnSortingTransactionAmt() {

		List<PettyCashTransaction> list = getDataprovider().getList();
		columnSort = new ListHandler<PettyCashTransaction>(list);
		columnSort.setComparator(getColumnTransactionAmt,new Comparator<PettyCashTransaction>() {
					@Override
					public int compare(PettyCashTransaction e1,PettyCashTransaction e2) {
						if (e1 != null && e2 != null) {
							if (e1.getTransactionAmount() == e2.getTransactionAmount()) {
								return 0;
							}
							if (e1.getTransactionAmount() > e2.getTransactionAmount()) {
								return 1;
							} else {
								return -1;
							}
						} else {
							return 0;
						}
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	private void CreateColumnSortingAction() {

		List<PettyCashTransaction> list = getDataprovider().getList();
		columnSort = new ListHandler<PettyCashTransaction>(list);
		columnSort.setComparator(getColumnAction,new Comparator<PettyCashTransaction>() {
			@Override
			public int compare(PettyCashTransaction e1,PettyCashTransaction e2) {
				if (e1 != null && e2 != null) {
					if (e1.getAction() != null && e2.getAction() != null) {
						return e1.getAction().compareTo(e2.getAction());
					}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	private void CreateColumnSortingOpeningStock() {

		List<PettyCashTransaction> list = getDataprovider().getList();
		columnSort = new ListHandler<PettyCashTransaction>(list);
		columnSort.setComparator(getColumnOpeningStock,new Comparator<PettyCashTransaction>() {
					@Override
					public int compare(PettyCashTransaction e1,PettyCashTransaction e2) {
						if (e1 != null && e2 != null) {
							if (e1.getOpeningStock() == e2.getOpeningStock()) {
								return 0;
							}
							if (e1.getOpeningStock() > e2.getOpeningStock()) {
								return 1;
							} else {
								return -1;
							}
						} else {
							return 0;
						}
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	private void CreateColumnSortingApproverName() {

		List<PettyCashTransaction> list = getDataprovider().getList();
		columnSort = new ListHandler<PettyCashTransaction>(list);
		columnSort.setComparator(getColumnApproverName,new Comparator<PettyCashTransaction>() {
			@Override
			public int compare(PettyCashTransaction e1,PettyCashTransaction e2) {
				if (e1 != null && e2 != null) {
					if (e1.getAppoverName() != null && e2.getAppoverName() != null) {
						return e1.getAppoverName().compareTo(e2.getAppoverName());
					}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	private void CreateColumnSortingExpenseCategory() {
		
		List<PettyCashTransaction> list = getDataprovider().getList();
		columnSort = new ListHandler<PettyCashTransaction>(list);
		columnSort.setComparator(getColumnExpenseCategory,new Comparator<PettyCashTransaction>() {
			@Override
			public int compare(PettyCashTransaction e1,PettyCashTransaction e2) {
				if (e1 != null && e2 != null) {
					if (e1.getexpenseCategory() != null && e2.getexpenseCategory() != null) {
						return e1.getexpenseCategory().compareTo(e2.getexpenseCategory());
					}
				         } else {
				        	 return 0;
				         }
				         return 0;
				}
			});
		table.addColumnSortHandler(columnSort);
	}

	private void CreateColumnSortingPersonResponsoble() {

		List<PettyCashTransaction> list = getDataprovider().getList();
		columnSort = new ListHandler<PettyCashTransaction>(list);
		columnSort.setComparator(getColumnPersonResponsible,new Comparator<PettyCashTransaction>() {
			@Override
			public int compare(PettyCashTransaction e1,PettyCashTransaction e2) {
				if (e1 != null && e2 != null) {
					if (e1.getPersonResponsible() != null && e2.getPersonResponsible() != null) {
						return e1.getPersonResponsible().compareTo(e2.getPersonResponsible());
					}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	private void CreateColumnSortingDocDate() {

		List<PettyCashTransaction> list = getDataprovider().getList();
		columnSort = new ListHandler<PettyCashTransaction>(list);
		columnSort.setComparator(getColumnDocDate,new Comparator<PettyCashTransaction>() {
			@Override
			public int compare(PettyCashTransaction e1,PettyCashTransaction e2) {
				if (e1 != null && e2 != null) {
					if (e1.getTransactionDate()!= null&& e2.getTransactionDate() != null) {
						return e1.getTransactionDate().compareTo(e2.getTransactionDate());
					}
				} 
				else {
							return 0;
					}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	private void createColumnSortingDocId() {

		List<PettyCashTransaction> list = getDataprovider().getList();
		columnSort = new ListHandler<PettyCashTransaction>(list);
		columnSort.setComparator(getColumnDocId,new Comparator<PettyCashTransaction>() {
					@Override
					public int compare(PettyCashTransaction e1,PettyCashTransaction e2) {
						if (e1 != null && e2 != null) {
							if (e1.getDocumentId() == e2.getDocumentId()) {
								return 0;
							}
							if (e1.getDocumentId() > e2.getDocumentId()) {
								return 1;
							} else {
								return -1;
							}
						} else {
							return 0;
						}
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	private void createColumnSortingId() {

		List<PettyCashTransaction> list = getDataprovider().getList();
		columnSort = new ListHandler<PettyCashTransaction>(list);
		columnSort.setComparator(getColumnId,new Comparator<PettyCashTransaction>() {
					@Override
					public int compare(PettyCashTransaction e1,PettyCashTransaction e2) {
						if (e1 != null && e2 != null) {
							if (e1.getCount() == e2.getCount()) {
								return 0;
							}
							if (e1.getCount() > e2.getCount()) {
								return 1;
							} else {
								return -1;
							}
						} else {
							return 0;
						}
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
