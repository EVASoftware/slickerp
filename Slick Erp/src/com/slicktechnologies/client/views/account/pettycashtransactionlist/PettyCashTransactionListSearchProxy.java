package com.slicktechnologies.client.views.account.pettycashtransactionlist;

import java.util.Vector;

import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.PettyCash;
import com.slicktechnologies.shared.common.PettyCashTransaction;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.scheduling.TeamManagement;

public class PettyCashTransactionListSearchProxy  extends SearchPopUpScreen<PettyCashTransaction>{

	
	public DateComparator dateComparator;
	public ObjectListBox<PettyCash> olbPettyCashName;
	public ObjectListBox<Employee> olbApproverName;
	public ObjectListBox<Employee> olbPersonResponsible;
	
	/**
	 * @author Anil , Date : 24-08-2019
	 * document date comparator
	 */
	public DateComparator docDateComparator;
	
	public PettyCashTransactionListSearchProxy() {
		super();
		createGui();
	}
	
	public PettyCashTransactionListSearchProxy(boolean b) {
		super(b);
		createGui();
	}
	
	
	public void initWidget()
	{
		
		dateComparator=new DateComparator("transactionDate",new PettyCashTransaction());
		
		olbApproverName=new ObjectListBox<Employee>();
		AppUtility.makeSalesPersonListBoxLive(olbApproverName);
		
		olbPersonResponsible = new ObjectListBox<Employee>();
		AppUtility.makeSalesPersonListBoxLive(olbPersonResponsible);
		
		olbPettyCashName =new ObjectListBox<PettyCash>();
		this.initializepettycashNameListBox();
		
		docDateComparator=new DateComparator("documentDate",new PettyCashTransaction());

	}

	private void initializepettycashNameListBox() {

		MyQuerry querry = new MyQuerry();
		querry.setQuerryObject(new PettyCash());
		olbPettyCashName.MakeLive(querry);
	}
	
	public void createScreen()
	{
		initWidget();
		
		FormFieldBuilder builder;
		
		builder = new FormFieldBuilder("From Date (Transaction Date)",dateComparator.getFromDate());
		FormField fdateComparatorfrom= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("To Date (Transaction Date)",dateComparator.getToDate());
		FormField fdateComparatorto= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Petty Cah Name",olbPettyCashName);
		FormField folbPettyCashName= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Approver Name",olbApproverName);
		FormField folbApproverName= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Person Responsible",olbPersonResponsible);
		FormField folbPersonResponsible= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("From Date (Document Date)",docDateComparator.getFromDate());
		FormField fdocDateComparatorfrom= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("To Date (Document Date)",docDateComparator.getToDate());
		FormField fdocDateComparatorto= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		this.fields=new FormField[][]{
				{fdateComparatorfrom,fdateComparatorto,folbPettyCashName,folbApproverName},
				{folbPersonResponsible,fdocDateComparatorfrom,fdocDateComparatorto}
		};
	}
	
	
	@Override
	public MyQuerry getQuerry() {
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter=null;
		
			if(dateComparator.getValue()!=null)
				filtervec.addAll(dateComparator.getValue());
			
			if(docDateComparator.getValue()!=null)
				filtervec.addAll(docDateComparator.getValue());
		
		
			if(olbApproverName.getSelectedIndex()!=0){
				 filter=new Filter();
				 filter.setStringValue(olbApproverName.getValue().trim());
				 filter.setQuerryString("appoverName");
				 filtervec.add(filter);
			}
			
			if(olbPersonResponsible.getSelectedIndex()!=0){
				 filter=new Filter();
				 filter.setStringValue(olbPersonResponsible.getValue().trim());
				 filter.setQuerryString("personResponsible");
				 filtervec.add(filter);
			}
			
			if(olbPettyCashName.getSelectedIndex()!=0){
				 filter=new Filter();
				 filter.setStringValue(olbPettyCashName.getValue().trim());
				 filter.setQuerryString("pettyCashName");
				 filtervec.add(filter);
			}
		 
			MyQuerry querry= new MyQuerry();
			querry.setFilters(filtervec);
			querry.setQuerryObject(new PettyCashTransaction());
			return querry;
	}

	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
		return true;
	}
}
