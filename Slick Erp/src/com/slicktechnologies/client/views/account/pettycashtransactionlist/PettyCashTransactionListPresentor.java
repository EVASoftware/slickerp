package com.slicktechnologies.client.views.account.pettycashtransactionlist;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.view.client.NoSelectionModel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreen;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreenPresenter;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.PettyCashTransaction;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.MultipleExpenseMngt;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.inventory.ProductInventoryTransaction;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

public class PettyCashTransactionListPresentor extends TableScreenPresenter<PettyCashTransaction>{

	TableScreen<PettyCashTransaction>form;
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	
	 /** Added by Sheetal : 24-12-2021**/
	GeneralViewDocumentPopup genralInvoicePopup = new GeneralViewDocumentPopup();
	public PopupPanel generalPanel;
	
	final GenricServiceAsync genasync=GWT.create(GenricService.class);

	public PettyCashTransactionListPresentor(TableScreen<PettyCashTransaction> view, PettyCashTransaction model) {
		super(view, model);
		form=view;
		setTableSelectionOnService();
		form.getSearchpopupscreen().getDwnload().setVisible(false);
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.PETTYCASHTRANSACTIONLIST,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}
	
	public PettyCashTransactionListPresentor(TableScreen<PettyCashTransaction> view,PettyCashTransaction model,MyQuerry querry) {
		super(view, model);
		form=view;
		view.retriveTable(querry);
		setTableSelectionOnService();
		form.getSearchpopupscreen().getDwnload().setVisible(false);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.PETTYCASHTRANSACTIONLIST,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}

	}
	
	public static void initalize()
	{
		PettyCashTransactionListTable table=new PettyCashTransactionListTable();
		PettyCashTransactionListForm form=new PettyCashTransactionListForm(table);
		
		PettyCashTransactionListSearchProxy.staticSuperTable=table;
		PettyCashTransactionListSearchProxy searchPopUp=new PettyCashTransactionListSearchProxy(false);
		form.setSearchpopupscreen(searchPopUp);
		/**Added by sheetal:24-12-2021
		 * Petty cash transaction list window to occupy full screen
		 */
		int height = Window.getClientHeight();
		Console.log("Window Height : " + height);
		height = height * 80 / 100;
		Console.log("Updated Height : " + height);
		searchPopUp.getSupertable().getTable().setHeight(height+"px");
		/**end**/
		PettyCashTransactionListPresentor presenter=new PettyCashTransactionListPresentor(form, new PettyCashTransaction());
		form.setToViewState();
		AppMemory.getAppMemory().stickPnel(form);
	}
	
	public static void initalize(MyQuerry querry)
	{
		PettyCashTransactionListTable table=new PettyCashTransactionListTable();
		PettyCashTransactionListForm form=new PettyCashTransactionListForm(table);
		
		PettyCashTransactionListSearchProxy.staticSuperTable=table;
		PettyCashTransactionListSearchProxy searchPopUp=new PettyCashTransactionListSearchProxy(false);
		form.setSearchpopupscreen(searchPopUp);
		/**Added by sheetal:24-12-2021
		 * Petty cash transaction list window to occupy full screen
		 */
		int height = Window.getClientHeight();
		Console.log("Window Height : " + height);
		height = height * 80 / 100;
		Console.log("Updated Height : " + height);
		searchPopUp.getSupertable().getTable().setHeight(height+"px");
		/**end**/
		PettyCashTransactionListPresentor presenter=new PettyCashTransactionListPresentor(form, new PettyCashTransaction(),querry);
		form.setToNewState();
		AppMemory.getAppMemory().stickPnel(form);
	}

	public MyQuerry getProductTransactionListQuery()
	{
		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new ProductInventoryTransaction());
		return quer;
	}
	
	@Override
	protected void makeNewModel() {
		model=new PettyCashTransaction();
	}
	
	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void reactOnDownload() {
		
		if(form.getSuperTable().getDataprovider().getList().size()==0)
		{
			form.showDialogMessage("No record found for download");
		}
		
		if(form.getSuperTable().getDataprovider().getList().size()>0)
		{
		ArrayList<PettyCashTransaction> pettycashtrasactionarray=new ArrayList<PettyCashTransaction>();
		List<PettyCashTransaction> listbill=form.getSuperTable().getListDataProvider().getList();
		
		pettycashtrasactionarray.addAll(listbill); 
		
		csvservice.setPettyCashTrasactionList(pettycashtrasactionarray, new AsyncCallback<Void>() {
			
			@Override
			public void onSuccess(Void result) {
				// TODO Auto-generated method stub
				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "csvservlet"+"?type="+108;
				Window.open(url, "test", "enabled");
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				System.out.println("RPC call Failed"+caught);
			}
		});
		
		}
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}
	/**Added by sheetal:24-12-2021**/
	 public void setTableSelectionOnService()
	 {
		 final NoSelectionModel<PettyCashTransaction> selectionModelMyObj = new NoSelectionModel<PettyCashTransaction>();
	     
	     SelectionChangeEvent.Handler  tableHandler = new SelectionChangeEvent.Handler() 
	     {
	         @Override
	         public void onSelectionChange(SelectionChangeEvent event) {
	
	        	 Timer timer = new Timer() {
	        		 @Override
						public void run() {
	        	           
	        			 PettyCashTransaction entity = selectionModelMyObj.getLastSelectedObject();
	        			 
	        			 	if(entity.getDocumentName().equals("Petty Cash Deposit")){
	        			 		form.showDialogMessage("Please go to Petty cash deposit history");
	        			 	}
	        			 	else{
	        			 		
	        			 		final MyQuerry querry=new MyQuerry();
		        				Vector<Filter> temp=new Vector<Filter>();
		        				Filter filter=null;
		        				
		        				filter=new Filter();
		        				filter.setQuerryString("count");
		        				filter.setIntValue(entity.getDocumentId());
		        				temp.add(filter);
		        				
		        				querry.setFilters(temp);
		        				querry.setQuerryObject(new MultipleExpenseMngt());
		        				
		        				genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
									
									@Override
									public void onSuccess(ArrayList<SuperModel> result) {
										// TODO Auto-generated method stub
										if(result.size()>0){
											MultipleExpenseMngt expense = (MultipleExpenseMngt) result.get(0);
											AppMemory.getAppMemory().currentScreen = Screen.PETTYCASHTRANSACTIONLIST;
						        			 genralInvoicePopup.setModel(AppConstants.MULTIPLEEXPENSEMANAGEMET,expense);
												generalPanel = new PopupPanel();
												generalPanel.add(genralInvoicePopup);
												generalPanel.show();
												generalPanel.center();
										}
										 
									}
									
									@Override
									public void onFailure(Throwable caught) {
										// TODO Auto-generated method stub
										
									}
								});
	        			 	}
	        			 	
	        				
	        			
	        		 }
	         };
	         timer.schedule(2000);
	       }
	      
	     };
	  // Add the handler to the selection model
	  			selectionModelMyObj.addSelectionChangeHandler(tableHandler);
	  			// Add the selection model to the table
	  			form.getSuperTable().getTable().setSelectionModel(selectionModelMyObj);
	 }
}