package com.slicktechnologies.client.views.account.pettycashtransactions;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import org.apache.poi.ss.formula.ptg.TblPtg;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.account.expensemanagment.ExpensemanagmentForm;
import com.slicktechnologies.client.views.account.expensemanagment.ExpensemanagmentPresenter;
import com.slicktechnologies.client.views.account.multipleexpensemanagment.MultipleExpensemanagmentForm;
import com.slicktechnologies.client.views.account.multipleexpensemanagment.MultipleExpensemanagmentPresenter;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.shared.PettyCash;
import com.slicktechnologies.shared.PettyCashDeposits;
import com.slicktechnologies.shared.common.PettyCashTransaction;
import com.slicktechnologies.shared.common.businessprocesslayer.Expense;
import com.slicktechnologies.shared.common.businessprocesslayer.MultipleExpenseMngt;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class PettyCashTransactionPresenter extends FormTableScreenPresenter<PettyCash> implements ChangeHandler {
	
	PettyCashTransactionForm form;
	final GenricServiceAsync async=GWT.create(GenricService.class);
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	
	/**
	 * @author Anil @since 28-07-2021
	 */
	GeneralViewDocumentPopup genralInvoicePopup = new GeneralViewDocumentPopup();
	
	public PettyCashTransactionPresenter(FormTableScreen<PettyCash> view,
			PettyCash model) {
		super(view, model);
		form=(PettyCashTransactionForm) view;
		form.getSupertable().connectToLocal();
		form.retriveTable(getPettyCashTransactionQuery());
		form.setPresenter(this);
		form.btnwithdraw.addClickHandler(this);
		form.btnaddamount.addClickHandler(this);
		form.btnviewhistory.addClickHandler(this);
		form.olbpettycashname.addChangeHandler(this);
		form.olbpettycash.addChangeHandler(this);
		
		genralInvoicePopup.btnClose.addClickHandler(this);
	
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.PETTYCASHTRANSACTION,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnDownload() {
		ArrayList<PettyCash> pettycasharray=new ArrayList<PettyCash>();
		List<PettyCash> list= form.getSearchpopupscreen().getSupertable().getListDataProvider().getList();
		pettycasharray.addAll(list);
		if(list !=null) {
			for(PettyCash p:list) {
				Console.log("petty cash details :"+ p.getPettyCashName()+"amount : "+((PettyCashDeposits) p).getDepositAmount()+ "desc :"+((PettyCashDeposits) p).getDescription());
			}
		}
		csvservice.setpettycashlist(pettycasharray, new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);
			}
			
			@Override
			public void onSuccess(Void result) {
	
				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "csvservlet"+"?type="+12;
				Window.open(url, "test", "enabled");
			}
		});
	}
	
	@Override
	protected void makeNewModel() {
		model=new PettyCash();
	}
	
	/*
	 * Method template to set Myquerry object
	 */
	public MyQuerry getPettyCashTransactionQuery()
	{
		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new PettyCash());
		return quer;
	}
	
	public void setModel(PettyCash entity)
	{
		model=entity;
	}
	
	public static PettyCashTransactionForm initalize()
	{
		PettyCashTransactionPresenterTable gentableScreen=new PettyCashTransactionPresenterTableProxy();
		PettyCashTransactionForm  form=new PettyCashTransactionForm(gentableScreen,FormTableScreen.UPPER_MODE,true);
		gentableScreen.setView(form);
		gentableScreen.applySelectionModle();
		PettyCashDepositTableProxy gentableSearch=new PettyCashDepositTableProxy();
	    PettyCashTransactionPresenterSearch.staticSuperTable=gentableSearch;
		PettyCashTransactionPresenterSearch searchpopup=new PettyCashTransactionPresenterSearchProxy();
	    form.setSearchpopupscreen(searchpopup);
		PettyCashTransactionPresenter presenter=new PettyCashTransactionPresenter(form,new PettyCash());
		AppMemory.getAppMemory().stickPnel(form);
		return form;
	}
	
	
	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.PettyCash")
	public static class PettyCashTransactionPresenterTable extends SuperTable<PettyCash> implements GeneratedVariableRefrence{
	
		@Override
		public Object getVarRef(String varName) {
			// TODO Auto-generated method stub
			return null;
		}
	
		@Override
		public void createTable() {
			// TODO Auto-generated method stub
		}
	
		@Override
		protected void initializekeyprovider() {
			// TODO Auto-generated method stub
		}
	
		@Override
		public void addFieldUpdater() {
			// TODO Auto-generated method stub
		}
	
		@Override
		public void setEnable(boolean state) {
			// TODO Auto-generated method stub
		}
	
		@Override
		public void applyStyle() {
			// TODO Auto-generated method stub
	}} ;
			
	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.PettyCash")
	 public static class PettyCashTransactionPresenterSearch extends SearchPopUpScreen<PettyCash>{

		@Override
		public MyQuerry getQuerry() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public boolean validate() {
			// TODO Auto-generated method stub
			return true;
	}};


	@Override
	public void onClick(ClickEvent event) {
		super.onClick(event);
		
		if(event.getSource()==form.btnwithdraw){
			double balanceamount=0;
			if(form.olbpettycashname.getValue()==null||form.olbpettycashname.getValue().equals(null)){
				form.showDialogMessage("Petty Cash Name cannot be blank!");
			}
			if(form.olbpettycashname.getValue()!=null){
				balanceamount=form.dbbalanceAmount.getValue();
			}
			if(form.olbpettycashname.getValue()!=null&&balanceamount==0){
				form.showDialogMessage("No sufficient balance in "+form.olbpettycashname.getValue());
			}
			if(form.olbpettycashname.getValue()!=null&&balanceamount>0){
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("PettyCash", "OnlyForAnkita")){
					reactOnWithdraw();
				}
				else{
					reactOnWithdrawAndGoToMUltipleExpense();
				}
			}
		}
		
		if(event.getSource()==form.btnaddamount){
			
			if(form.olbpettycashname.getValue()==null){
				form.showDialogMessage("Petty Cash Name cannot be blank!");
				return;
			}
			if(form.dbdepositAmount.getValue()==null){
				form.showDialogMessage("Amount cannot be blank!");
				return;
			}
			if(form.dbdepositAmount.getValue()!=null&&form.dbdepositAmount.getValue()<1){
				form.showDialogMessage("Amount cannot be 0!");
				return;
			}
			
			boolean flag = true;
			
			
			if(form.rbpettycash.getValue()==true){
				
				if(form.olbpettycash.getValue()==null){
					form.showDialogMessage("Petty Cash Name cannot be blank!");
					flag = false;
				}else if(form.olbpettycash.getValue().trim().equalsIgnoreCase(form.olbpettycashname.getValue().trim())){
					form.showDialogMessage("Petty Cash Name should be different!");
					flag = false;
				}else{
					
					
					PettyCash petty=form.olbpettycash.getSelectedItem();
					double balanceamount=0;
					double entered_amount=0;
					
					balanceamount=petty.getPettyCashBalance();
					entered_amount=form.dbdepositAmount.getValue();
					
					if(balanceamount>=entered_amount){
						
						double currentBalance=petty.getPettyCashBalance()-form.dbdepositAmount.getValue();
					
						form.dbavailableblnc.setValue(currentBalance);
						form.showWaitSymbol();
					
						PettyCashDeposits pettyDeposits=new PettyCashDeposits();
						pettyDeposits.setPettyCashName(form.olbpettycash.getValue().trim());
						pettyDeposits.setEmployee(form.tbpersonresponsible.getValue().trim());
						pettyDeposits.setDepositAmount(form.dbdepositAmount.getValue());
						if(form.taDescription.getValue()!=null)
							pettyDeposits.setDescription(form.taDescription.getValue());
						Date depositDate=new Date();
						DateTimeFormat dateFormat=DateTimeFormat.getFormat(PredefinedFormat.DATE_SHORT);
						String strDate=dateFormat.format(depositDate);
						Date saveDate=DateTimeFormat.getShortDateFormat().parse(strDate);
						String saveTime=DateTimeFormat.getShortTimeFormat().format(depositDate);
						double openingstock = petty.getPettyCashBalance();
						petty.setPettyCashBalance(currentBalance);
						
						async.save(petty,new AsyncCallback<ReturnFromServer>() {
							@Override
							public void onFailure(Throwable caught) {
								Console.log("RPC call failed!");
//								form.hideWaitSymbol();
							}
							@Override
							public void onSuccess(ReturnFromServer result) {
								Console.log("PettyCash from saved ");
								form.fillInView();
//								form.hideWaitSymbol();
							}}
						);
						
						savedepo(pettyDeposits);
						
						PettyCashTransaction pettycash = new PettyCashTransaction();
						pettycash.setOpeningStock(openingstock);
						pettycash.setTransactionAmount(form.dbdepositAmount.getValue());
						pettycash.setClosingStock(currentBalance);
						pettycash.setDocumentId(petty.getCount());
						pettycash.setDocumentName("Petty Cash Deposit");
						pettycash.setTransactionDate(saveDate);
						pettycash.setAppoverName("");
						pettycash.setPersonResponsible(petty.getEmployee());
						pettycash.setAction("-");
						pettycash.setDocumentTitle(petty.getPettyCashName());
						pettycash.setPettyCashName(petty.getPettyCashName());
						pettycash.setTransactionTime(saveTime);
						if(form.taDescription.getValue()!=null)
						pettycash.setDescription(form.taDescription.getValue());
						
						async.save(pettycash, new AsyncCallback<ReturnFromServer>() {
							
							@Override
							public void onSuccess(ReturnFromServer result) {
								// TODO Auto-generated method stub
								Console.log("Transaction list saved");
								form.setToViewState();
								form.hideWaitSymbol();
							}
							
							@Override
							public void onFailure(Throwable caught) {
								// TODO Auto-generated method stub
								Console.log("Failed ======");
								form.hideWaitSymbol();
							}
						});
						form.hideWaitSymbol();						
						
						reactOnAddbtn("Petty Cash Desposite"); //Ashwini Patil 3-01-2023
					}else{
						flag = false;
						form.showDialogMessage("Entered amout is greater than available amount.");
					}
				}
			}
			
			if(form.rbcashdeposit.getValue()==true){
				if(form.olbpettycashname.getValue()!=null&&form.dbdepositAmount.getValue()!=null&&form.dbdepositAmount.getValue()>0 && flag){
					form.showWaitSymbol();
					reactOnAddbtn("Cash Desposite");
					form.olbpettycash.setValue("--SELECT--");
					form.dbavailableblnc.setValue(null);
				}	
			}
			
			if(form.rbBank.getValue()==true){
				if(form.objPaymentMode.getSelectedIndex()!=0){
					form.showWaitSymbol();
					reactOnAddbtn("Bank - "+form.objPaymentMode.getSelectedItem().getPaymentBankName());
					form.olbpettycash.setValue("--SELECT--");
					form.dbavailableblnc.setValue(null);
				}
			}
		}
		
		if(event.getSource()==form.btnviewhistory){
			if(form.olbpettycashname.getValue()==null){
				form.showDialogMessage("Petty Cash Name cannot be blank!");
			}
			if(form.olbpettycashname.getValue()!=null){
				reactOnDepositHistory();
			}
		}
		
		if(event.getSource()==genralInvoicePopup.btnClose){
			form.showWaitSymbol();
			form.olbpettycashname=new ObjectListBox<PettyCash>();
			form.olbpettycashname.MakeLive(new MyQuerry(new Vector<Filter>(), new PettyCash()));
			form.olbpettycash=new ObjectListBox<PettyCash>();
			form.olbpettycash.MakeLive(new MyQuerry(new Vector<Filter>(), new PettyCash()));
			
			AppMemory.getAppMemory().currentScreen = Screen.PETTYCASHTRANSACTION;
			
			form.initializeScreen();
			form.intializeScreenMenus();
			form.addClickEventOnScreenMenus();
			form.addClickEventOnActionAndNavigationMenus();
			for (int k = 0; k < mem.skeleton.menuLabels.length; k++) {
				if (mem.skeleton.registration[k] != null)
					mem.skeleton.registration[k].removeHandler();
			}
			for (int k = 0; k < mem.skeleton.menuLabels.length; k++)
				mem.skeleton.registration[k] = mem.skeleton.menuLabels[k].addClickHandler(this);
			
			Timer timer = new Timer() {
				@Override
				public void run() {
					form.hideWaitSymbol();
					form.olbpettycashname.setValue(model.getPettyCashName());
					form.fillInView();
					form.setToViewState();
				}
			};
			timer.schedule(3000);
			
		}
		
	}
				
				
				
				
				
				/** Code for On click of withdraw button**/
				
				protected void reactOnWithdrawAndGoToMUltipleExpense()
				{
//					final String pettyname=form.olbpettycashname.getValue();
// 		    		System.out.println("Pettycash Name"+pettyname);
//					final MultipleExpenseMngt mulExpEntity=new MultipleExpenseMngt();
//					AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Accounts/Expense Management", Screen.MULTIPLEEXPENSEMANAGMENT);
//					MultipleExpensemanagmentPresenter.initalize();
//					final MultipleExpensemanagmentForm multipleform=MultipleExpensemanagmentPresenter.form;
//					
//					Timer t = new Timer() {
//					      @Override
//					      public void run() {
//					    	  multipleform.updateView(mulExpEntity);
//					    	  multipleform.setToNewState();
//					    	  multipleform.getCbexptype().setValue(true);
//					    	  multipleform.getCbexptype().setEnabled(true);
//					    	  multipleform.getOlbpettycashname().setValue(pettyname);
//					    	  multipleform.getDbAmount().setValue(null);
//					    	  multipleform.setPettyCashNameFlag(true);
//					    	  multipleform.getTotalTotalAmt().setEnabled(false);
//					    	  multipleform.getOlbpettycashname().setEnabled(true);
//					      }
//				    };
//				    t.schedule(2000);
					MultipleExpenseMngt mulExpEntity=new MultipleExpenseMngt();
					mulExpEntity.setPettyCashName(form.getOlbpettycashname().getValue());
					mulExpEntity.setBranch(model.getBranch());
					
					genralInvoicePopup.setModel(AppConstants.MULTIPLEXPMANAGEMENT,mulExpEntity);
				}
				
				

				/** Code for On click of withdraw button**/
				
				protected void reactOnWithdraw()
				{
					final String pettyname=form.olbpettycashname.getValue();
					final Expense expentity=new Expense();
					AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Accounts/Expense Management", Screen.EXPENSEMANAGMENT);
					ExpensemanagmentPresenter.initalize();
					final ExpensemanagmentForm form=ExpensemanagmentPresenter.form;
					
					Timer t = new Timer() {
					      @Override
					      public void run() {
					    	 
					    	  form.updateView(expentity);
					    	  form.getOlbApproverName().setEnabled(false);
					    	  form.getCbexptype().setValue(true);
					    	  form.getCbexptype().setEnabled(false);
					    	  form.getOlbpettycashname().setValue(pettyname);
					    	  form.getDbAmount().setValue(null);
					    	  form.setPettyCashNameFlag(true);
					    	  form.setToNewState();
					    	  form.setAppHeaderBarAsPerStatus();
					      }
					    };
					    t.schedule(3000);
				}
				
				/**Code for onclick of Deposit History button**/
				
				protected void reactOnDepositHistory()
				{
					if(view.getSearchpopupscreen()!=null)
					{
						view.getSearchpopupscreen().showPopUp();
					}
				}
				
				
				/** Code for onclick of add button **/
				
				protected void reactOnAddbtn(String remark){
					PettyCash pettybal=form.olbpettycashname.getSelectedItem();
					double currentBalance=pettybal.getPettyCashBalance()+form.dbdepositAmount.getValue();
					
					//Get current date and time
					
					/************ vijay for transaction list ***********************/
					double openingstock = pettybal.getPettyCashBalance();
					/**
					 * Updated By:Viraj
					 * Date: 24-06-2019
					 * Description: To save depositDate when selected or the current date as deposit date
					 */
					Date depositDate=new Date();
					if(form.depositDate.getValue() != null) {
						depositDate = form.depositDate.getValue();
					}
					/** Ends **/
					DateTimeFormat dateFormat=DateTimeFormat.getFormat(PredefinedFormat.DATE_SHORT);
					String strDate=dateFormat.format(depositDate);
					Date saveDate=DateTimeFormat.getShortDateFormat().parse(strDate);
					String saveTime=DateTimeFormat.getShortTimeFormat().format(depositDate);
					
					// Set deposit info to petty cash deposits entity
					
					PettyCashDeposits pettyDeposits=new PettyCashDeposits();
					pettyDeposits.setPettyCashName(form.olbpettycashname.getValue().trim());
					pettyDeposits.setEmployee(form.tbpersonresponsible.getValue().trim());
					pettyDeposits.setDepositAmount(form.dbdepositAmount.getValue());
					pettyDeposits.setDepositDate(saveDate);
					pettyDeposits.setDepositTime(saveTime);
					
					// Set pettycash balance value
					pettybal.setPettyCashBalance(currentBalance);
					
					/**
					 * @author Anil @since 28-07-2021
					 * Setting remark for transaction
					 */
					pettyDeposits.setRemark(remark);
					
					save(pettybal);
					savedepo(pettyDeposits);
					
					form.getSupertable().connectToLocal();
					form.retriveTable(getPettyCashTransactionQuery());
					
					
					/**************************** vijay for transaction list *******************/
					System.out.println("hi vijay ====");
					PettyCashTransaction pettycash = new PettyCashTransaction();
					pettycash.setOpeningStock(openingstock);
					pettycash.setTransactionAmount(form.dbdepositAmount.getValue());
					pettycash.setClosingStock(currentBalance);
					pettycash.setDocumentId(pettybal.getCount());
					pettycash.setDocumentName("Petty Cash Deposit");
					pettycash.setTransactionDate(saveDate);
					pettycash.setAppoverName("");
					pettycash.setPersonResponsible(pettybal.getEmployee());
					pettycash.setAction("+");
					pettycash.setDocumentTitle(pettybal.getPettyCashName());
					pettycash.setPettyCashName(pettybal.getPettyCashName());
					pettycash.setTransactionTime(saveTime);
					
					/**
					 * @author Anil @since 28-07-2021
					 * Setting remark for transaction
					 */
					pettycash.setRemark(remark);
					
					async.save(pettycash, new AsyncCallback<ReturnFromServer>() {
						@Override
						public void onSuccess(ReturnFromServer result) {
							// TODO Auto-generated method stub
							System.out.println("Transaction list saved");
						}
						
						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							System.out.println("Failed ======");
						}
					});
				}
				
				/** Code for saving amount to be deposited to petty cash**/
				
				private void save(PettyCash pettybalance)
				{
					async.save(pettybalance,new AsyncCallback<ReturnFromServer>() {

						@Override
						public void onFailure(Throwable caught) {
							System.out.println("RPC call failed!");
							form.hideWaitSymbol();
						}

						@Override
						public void onSuccess(ReturnFromServer result) {
							form.dbdepositAmount.setValue(null);
//							form.showDialogMessage("Amount Deposited Successfully!",GWTCAlert.OPTION_ROUNDED_BLUE,GWTCAlert.OPTION_ANIMATION);
							
							form.showDialogMessage("Amount Deposited Successfully!");
							form.fillInView();
							form.hideWaitSymbol();
						}}

						);
				}
				
				
				/** Code for saving deposit logs**/
				
				private void savedepo(PettyCashDeposits pettydeposits)
				{
					Console.log("in savedepo");
					async.save(pettydeposits,new AsyncCallback<ReturnFromServer>() {

						@Override
						public void onFailure(Throwable caught) {
							Console.log("RPC call failed!");
						}

						@Override
						public void onSuccess(ReturnFromServer result) {
							Console.log("SAVED");
						}
					});
				}
				
				
			/** Code for filling form fields on select of record from table **/
				
				@Override
				public void onChange(ChangeEvent event) {
					if(form.olbpettycashname.getSelectedItem()!=null)
					{
						form.fillInView();
					}
					else
					{
						form.tbpersonresponsible.setValue(null);
						form.tbbranch.setValue(null);
						form.dbdepositAmount.setValue(null);
					}
					
					if(form.olbpettycash.getSelectedItem()!=null)
					{
						form.rbpettycash.setValue(true);
						form.fillinview2();
					}
				}
				
				
}
