package com.slicktechnologies.client.views.account.pettycashtransactions;

import java.util.Comparator;
import java.util.List;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.PettyCashDeposits;

public class PettyCashDepositTableProxy extends SuperTable<PettyCashDeposits> {
	
	TextColumn<PettyCashDeposits> getPettyCashNameColumn;
	TextColumn<PettyCashDeposits> getPersonResponsibleColumn;
	TextColumn<PettyCashDeposits> getDepositDateColumn;
	TextColumn<PettyCashDeposits> getDepositTimeColumn;
	TextColumn<PettyCashDeposits> getDepositAmountColumn;
	/**
	 * date 4-3-2017
	 * added by vijay
	 * when we deposite cash from payment document into petty cash
	 * then its payment doc id display here
	 */
	TextColumn<PettyCashDeposits> getRefNumber;
	
	public PettyCashDepositTableProxy() {
		super();
	}
	
	@Override public void createTable() {
		
		addColumngetPettyCashName();
		addColumngetPersonResponsible();
		addColumngetDepositAmount();
		addColumngetDepositDate();
		addColumngetDepositTime();
		addColumngetRefNumber();
	}
	
	@Override
	protected void initializekeyprovider() {
		keyProvider= new ProvidesKey<PettyCashDeposits>()
				{
			@Override
			public Object getKey(PettyCashDeposits item)
			{
				if(item==null)
				{
					return null;
				}
				else
					return item.getId();
			}
				};
	}
	
	@Override
	public void setEnable(boolean state)
	{
	}
	@Override
	public void applyStyle()
	{
	}

	public void addColumnSorting(){
		addSortinggetPettyCashName();
		addSortinggetPersonResponsible();
		addSortinggetDepositAmount();
		addSortinggetDepositDate();
		addSortinggetDepositTime();
		
		addSortinggetRefNumber();
	}
	
	
	private void addSortinggetRefNumber() {
		List<PettyCashDeposits> list=getDataprovider().getList();
		columnSort=new ListHandler<PettyCashDeposits>(list);
		columnSort.setComparator(getRefNumber, new Comparator<PettyCashDeposits>() {

			@Override
			public int compare(PettyCashDeposits e1, PettyCashDeposits e2) {
				if(e1!=null && e2!=null)
				{
					if(e1.getPaymentdocId()== e2.getPaymentdocId()){
						return 0;}
					if(e1.getPaymentdocId()> e2.getPaymentdocId()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
		});
		table.addColumnSortHandler(columnSort);	
	}
	
	@Override public void addFieldUpdater() {
	}

	
	protected void addSortinggetPettyCashName()
	{
		List<PettyCashDeposits> list=getDataprovider().getList();
		columnSort=new ListHandler<PettyCashDeposits>(list);
		columnSort.setComparator(getPettyCashNameColumn, new Comparator<PettyCashDeposits>()
				{
			@Override
			public int compare(PettyCashDeposits e1,PettyCashDeposits e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getPettyCashName()!=null && e2.getPettyCashName()!=null){
						return e1.getPettyCashName().compareTo(e2.getPettyCashName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetPettyCashName()
	{
		getPettyCashNameColumn=new TextColumn<PettyCashDeposits>()
				{
			@Override
			public String getValue(PettyCashDeposits object)
			{
				return object.getPettyCashName();
			}
				};
				table.addColumn(getPettyCashNameColumn,"Petty Cash");
				getPettyCashNameColumn.setSortable(true);
	}

	
	
	protected void addSortinggetPersonResponsible()
	{
		List<PettyCashDeposits> list=getDataprovider().getList();
		columnSort=new ListHandler<PettyCashDeposits>(list);
		columnSort.setComparator(getPersonResponsibleColumn, new Comparator<PettyCashDeposits>()
				{
			@Override
			public int compare(PettyCashDeposits e1,PettyCashDeposits e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getEmployee()!=null && e2.getEmployee()!=null){
						return e1.getEmployee().compareTo(e2.getEmployee());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetPersonResponsible()
	{
		getPersonResponsibleColumn=new TextColumn<PettyCashDeposits>()
				{
			@Override
			public String getValue(PettyCashDeposits object)
			{
				return object.getEmployee();
			}
				};
				table.addColumn(getPersonResponsibleColumn,"Person Responsible");
				getPersonResponsibleColumn.setSortable(true);
	}

	
	protected void addSortinggetDepositAmount()
	{
		List<PettyCashDeposits> list=getDataprovider().getList();
		columnSort=new ListHandler<PettyCashDeposits>(list);
		columnSort.setComparator(getDepositDateColumn, new Comparator<PettyCashDeposits>()
				{
			@Override
			public int compare(PettyCashDeposits e1,PettyCashDeposits e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getDepositAmount()!=null && e2.getDepositAmount()!=null){
						return e1.getDepositAmount().compareTo(e2.getDepositAmount());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetDepositAmount()
	{
		getDepositAmountColumn=new TextColumn<PettyCashDeposits>()
				{
			@Override
			public String getValue(PettyCashDeposits object)
			{
				if(object.getDepositAmount()==null)
				{
					return null;
				}
				else
				{
				return object.getDepositAmount()+"";
				}
			}
				};
				table.addColumn(getDepositAmountColumn,"Amount");
				getDepositAmountColumn.setSortable(true);
	}

	protected void addSortinggetDepositDate()
	{
		List<PettyCashDeposits> list=getDataprovider().getList();
		columnSort=new ListHandler<PettyCashDeposits>(list);
		columnSort.setComparator(getDepositDateColumn, new Comparator<PettyCashDeposits>()
				{
			@Override
			public int compare(PettyCashDeposits e1,PettyCashDeposits e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getDepositDate()!=null && e2.getDepositDate()!=null){
						return e1.getDepositDate().compareTo(e2.getDepositDate());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetDepositDate()
	{
		getDepositDateColumn=new TextColumn<PettyCashDeposits>()
				{
			@Override
			public String getValue(PettyCashDeposits object)
			{
				if(object.getDepositDate()==null)
				{
					return null;
				}
				else
				{
					return AppUtility.parseDate(object.getDepositDate());
				}
			}
				};
				table.addColumn(getDepositDateColumn,"Deposit Date");
				getDepositDateColumn.setSortable(true);
	}

	protected void addSortinggetDepositTime()
	{
		List<PettyCashDeposits> list=getDataprovider().getList();
		columnSort=new ListHandler<PettyCashDeposits>(list);
		columnSort.setComparator(getDepositTimeColumn, new Comparator<PettyCashDeposits>()
				{
			@Override
			public int compare(PettyCashDeposits e1,PettyCashDeposits e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getDepositTime()!=null && e2.getDepositTime()!=null){
						return e1.getDepositTime().compareTo(e2.getDepositTime());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetDepositTime()
	{
		getDepositTimeColumn=new TextColumn<PettyCashDeposits>()
				{
			@Override
			public String getValue(PettyCashDeposits object)
			{
				if(object.getDepositTime()==null)
				{
					return null;
				}
				else
				{
				return object.getDepositTime();
				}
			}
				};
				table.addColumn(getDepositTimeColumn,"Deposit Time");
				getDepositTimeColumn.setSortable(true);
	}
	
private void addColumngetRefNumber() {
		
		getRefNumber = new TextColumn<PettyCashDeposits>() {
			
			@Override
			public String getValue(PettyCashDeposits object) {
				if(object.getPaymentdocId()!=-2){
					return object.getPaymentdocId()+"";
				}
				return "";
			}
		};
		table.addColumn(getRefNumber,"Reference Id");
		getRefNumber.setSortable(true);
	}
	
	

}
