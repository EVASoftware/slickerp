package com.slicktechnologies.client.views.account.pettycashtransactions;


import java.util.Date;
import java.util.Vector;

import com.google.gwt.dom.client.Style.TextAlign;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.PettyCash;
import com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;


public class PettyCashTransactionForm extends FormTableScreen<PettyCash> implements ClickHandler,ChangeHandler{

//	IntegerBox ibbalanceamount;
	Button btnaddamount;
	Button btnwithdraw;
//	IntegerBox ibdepositamount;
	ObjectListBox<PettyCash> olbpettycashname;
	TextBox tbpersonresponsible;
	TextBox tbbranch;
	Button btnviewhistory;
	PettyCash pettyCashObj;
	
	/**
	 * Date 08-08-2017 added by vijay for changing intger to double
	 * old code commented and new code added
	 */ 
	DoubleBox dbdepositAmount;
	DoubleBox dbbalanceAmount;
	
	/**
	 * Date 29/03/2018 added by Manisha
	 * petty cash entry issued, amount transfer to petty to employees not showing in that employee
       Need transfer of parent petty cash to child petty cash
	 */ 
	RadioButton rbpettycash;
	RadioButton rbcashdeposit;
	ObjectListBox<PettyCash> olbpettycash;
	DoubleBox dbavailableblnc;
	/**End**/
	/**
	 * Updated By: Viraj
	 * Date: 24-06-2019
	 * Description: To add deposit date 
	 */
	DateBox depositDate;
	/** Ends **/
	
	/**
	 * @author Anil @since 28-07-2021
	 * Adding bank details option here
	 */
	RadioButton rbBank;
	ObjectListBox<CompanyPayment> objPaymentMode;
	FormField fobjPaymentMode;
	FormField fpettycash;
	FormField ftbavailableblnc;
	TextArea taDescription; //Ashwini Patil Date:17-04-2024 added description field in petty cash deposit section on request of ultra pest
	
	public PettyCashTransactionForm(SuperTable<PettyCash> table, int mode,boolean captionmode) {
		super(table, mode, captionmode);
		createGui();
		objPaymentMode.setVisible(false);
		fobjPaymentMode.getHeaderLabel().setVisible(false);
		
		olbpettycash.setVisible(false);
		dbavailableblnc.setVisible(false);
		fpettycash.getHeaderLabel().setVisible(false);
		ftbavailableblnc.getHeaderLabel().setVisible(false);
	}

	private void initalizeWidget(){
		
		dbdepositAmount=new DoubleBox();
		dbbalanceAmount=new DoubleBox(); 
		dbbalanceAmount.setEnabled(false);
		changeFontSize(this.dbbalanceAmount, 18, Unit.PX);
		changeHeight(dbbalanceAmount, 35, Unit.PX);
		
		tbpersonresponsible=new TextBox();
		tbpersonresponsible.setEnabled(false);
		tbbranch=new TextBox();
		tbbranch.setEnabled(false);
		btnaddamount=new Button("Add To Petty Cash");
		btnwithdraw=new Button("Withdraw");
		changeHeight(btnwithdraw, 45, Unit.PX);
		changeWidth(btnwithdraw, 100, Unit.PX);
		changeFontSize(btnwithdraw, 18, Unit.PX);
		olbpettycashname=new ObjectListBox<PettyCash>();
		olbpettycashname.MakeLive(new MyQuerry(new Vector<Filter>(), new PettyCash()));
		olbpettycashname.setEnabled(false);
		btnviewhistory=new Button("View Deposit History");
		
		rbpettycash=new RadioButton("From Petty Cash");
		rbpettycash.setValue(false);
		rbpettycash.addClickHandler(this);
		
		
		rbcashdeposit=new RadioButton("Cash Deposit");
		rbcashdeposit.setValue(false);
		rbcashdeposit.addClickHandler(this);
		
		olbpettycash=new ObjectListBox<PettyCash>();
		olbpettycash.MakeLive(new MyQuerry(new Vector<Filter>(), new PettyCash()));
		
		
		dbavailableblnc=new DoubleBox();
		/**
		 * Updated By: Viraj
		 * Date: 24-06-2019
		 * Description: To add deposit date
		 */
		depositDate = new DateBoxWithYearSelector();
		depositDate.setValue(new Date());
		depositDate.setEnabled(true);
		/** Ends **/
		
		
		rbBank=new RadioButton("Bank");
		rbBank.setValue(false);
		rbBank.addClickHandler(this);
		
		objPaymentMode = new ObjectListBox<CompanyPayment>();
		
		Filter filter1=new Filter();
		filter1.setQuerryString("companyId");
		filter1.setLongValue(UserConfiguration.getCompanyId());
		
		Filter filter2=new Filter();
		filter2.setQuerryString("paymentStatus");
		filter2.setBooleanvalue(true);
		
		taDescription=new TextArea();
		tbbranch.setEnabled(true);
		AppUtility.makeObjectListBoxLive(objPaymentMode, new CompanyPayment(), filter1,filter2);
	}


	@Override
	public void createScreen() {

		//Token to initialize the processlevel menus.
		initalizeWidget();

		//////////////////////////////////Form Field Declaration/////////////////////////////////////////////////

		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////

		//Token to initialize formfield
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingPettyCashInformation=fbuilder.setlabel("Petty Cash Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Petty Cash Name",olbpettycashname);
		FormField ftbpettycashname= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Person Responsible",tbpersonresponsible);
		FormField ftbpersonresponsible= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Branch",tbbranch);
		FormField ftbbranch= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Balance",dbbalanceAmount);
		FormField fibbalance= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingDepositAmount=fbuilder.setlabel("Deposit Amount").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Enter Amount",dbdepositAmount);
		FormField ftbdepositamount= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",btnaddamount);
		FormField fbtnaddamount= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingWithdrawAmount=fbuilder.setlabel("Withdraw Amount").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("",btnwithdraw);
		FormField fbtnwithdraw= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",btnviewhistory);
		FormField fbtnviewhistory= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("From Petty Cash",rbpettycash);
		FormField frbpettycash= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Cash Deposit",rbcashdeposit);
		FormField frbcashdeposit= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Petty Cash ",olbpettycash);
//		FormField fpettycash= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fpettycash= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Available Balance",dbavailableblnc);
//		FormField ftbavailableblnc= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		ftbavailableblnc= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		/**
		 * Updated By: Viraj
		 * Date: 24-06-2019
		 * Description: To add deposit date
		 */
		fbuilder = new FormFieldBuilder("Deposit Date",depositDate);
		FormField fdepositDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		/** Ends **/
		
		fbuilder = new FormFieldBuilder("Description (Max 500 characters)",taDescription);
		FormField ftaDescription= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		
		
		fbuilder = new FormFieldBuilder("Bank",rbBank);
		FormField frbBank= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Bank",objPaymentMode);
		fobjPaymentMode= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////

		
		
		FormField[][] formfield = {   
				{fgroupingPettyCashInformation},
				{ftbpettycashname,ftbpersonresponsible,ftbbranch},
				{fibbalance,fbtnwithdraw,fbtnviewhistory},
				{fgroupingDepositAmount},
				{frbpettycash,frbcashdeposit,frbBank},
				{fpettycash,ftbavailableblnc},
				{fobjPaymentMode},
				{ftaDescription},
				{ftbdepositamount,fdepositDate,fbtnaddamount},
		};


		this.fields=formfield;		
	}

	/**
	 * method template to update the model with token entity name.
	 *
	 * @param model the model
	 */
	@Override
	public void updateModel(PettyCash model) 
	{
		pettyCashObj=model;
		presenter.setModel(model);
	}

	/**
	 * method template to update the view with token entity name
	 */
	@Override
	public void updateView(PettyCash view) 
	{
		pettyCashObj=view;
		olbpettycashname.setValue(view.getPettyCashName());
		fillInView();

		presenter.setModel(view);

	}

	
	/**
	 * Toggles the app header bar menus as per screen state
	 */

	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		
		AuthorizationHelper.setAsPerAuthorization(Screen.PETTYCASHTRANSACTION,LoginPresenter.currentModule.trim());
	}
	
	
	/**
	 * sets the id textbox with the passed count value. 
	 */
	@Override
	public void setCount(int count)
	{
	}
	
	
	@Override
	public void setToViewState() {
		super.setToViewState();
		this.dbdepositAmount.setEnabled(true);
		this.btnaddamount.setEnabled(true);
		this.btnwithdraw.setEnabled(true);
		this.btnviewhistory.setEnabled(true);
		
		this.rbcashdeposit.setEnabled(true);
		this.rbpettycash.setEnabled(true);
		this.dbavailableblnc.setEnabled(false);
		this.olbpettycash.setEnabled(true);
		this.taDescription.setEnabled(true);
		
		rbBank.setEnabled(true);
		objPaymentMode.setEnabled(true);
		
		
		/**
		 * Updated By: Viraj
		 * Date: 24-06-2019
		 * Description: To add deposit date
		 */
		this.depositDate.setValue(new Date());
		this.depositDate.setEnabled(true);
		/** Ends **/
		
		SuperModel model=new PettyCash();
		model=pettyCashObj;
		AppUtility.addDocumentToHistoryTable(AppConstants.ACCOUNTMODULE,AppConstants.PETTYCASHTRANSACTION, pettyCashObj.getCount(), null,null,null, false, model, null);
	
	}
	
	@Override
	public void setToNewState() {
		super.setToNewState();
		/**
		 * Updated By: Viraj
		 * Date: 24-06-2019
		 * Description: To add deposit date
		 */
		this.depositDate.setValue(new Date());
		this.depositDate.setEnabled(true);
		/** Ends **/
		System.out.println("Inside NEW method");
		if(pettyCashObj!=null){
			SuperModel model=new PettyCash();
			model=pettyCashObj;
			AppUtility.addDocumentToHistoryTable(AppConstants.ACCOUNTMODULE,AppConstants.PETTYCASHTRANSACTION, pettyCashObj.getCount(), null,null,null, false, model, null);
		
		}
	}

	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		tbpersonresponsible.setEnabled(false);
		tbbranch.setEnabled(false);
		dbbalanceAmount.setEnabled(false);
		olbpettycashname.setEnabled(false);
		this.dbavailableblnc.setEnabled(false);
		/**
		 * Updated By: Viraj
		 * Date: 24-06-2019
		 * Description: To add deposit date
		 */
		this.depositDate.setEnabled(true);
		/** Ends **/
	}
	
	public void fillInView()
	{
		PettyCash petty=olbpettycashname.getSelectedItem();
		tbpersonresponsible.setValue(petty.getEmployee());
		tbbranch.setValue(petty.getBranch());
		dbbalanceAmount.setValue(petty.getPettyCashBalance());
		rbpettycash.setValue(false);
		rbcashdeposit.setValue(false);
		
		rbpettycash.setEnabled(true);
		rbcashdeposit.setEnabled(true);
		this.dbavailableblnc.setEnabled(false);
		
		
		olbpettycash.setVisible(false);
		dbavailableblnc.setVisible(false);
		
		fpettycash.getHeaderLabel().setVisible(false);
		ftbavailableblnc.getHeaderLabel().setVisible(false);
		
		rbBank.setValue(false);
		objPaymentMode.setVisible(false);
		fobjPaymentMode.getHeaderLabel().setVisible(false);
	}
	
	
	/**To change the font size of widgets**/
	private void changeFontSize(Widget widg,double size,Unit unit)
	{
		widg.getElement().getStyle().setFontSize(size, unit);
	}

	/**To change the height of widgets**/
	private void changeHeight(Widget widg,double size,Unit unit)
	{
		widg.getElement().getStyle().setHeight(size, unit);
	}

	/**To change the width of widgets**/
	private void changeWidth(Widget widg,double size,Unit unit)
	{
		widg.getElement().getStyle().setWidth(size, unit);
	}

	@Override
	public void onChange(ChangeEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onClick(ClickEvent event) {
		if (event.getSource().equals(rbpettycash)) {
			if (rbpettycash.getValue() == true) {
				olbpettycash.setVisible(true);
				dbavailableblnc.setVisible(true);
				olbpettycash.setEnabled(true);
				dbavailableblnc.setEnabled(false);
				rbcashdeposit.setValue(false);
				rbpettycash.setValue(true);
				
				fpettycash.getHeaderLabel().setVisible(true);
				ftbavailableblnc.getHeaderLabel().setVisible(true);
				
				rbBank.setValue(false);
				objPaymentMode.setVisible(false);
				fobjPaymentMode.getHeaderLabel().setVisible(false);
			}
		}

		if (event.getSource().equals(rbcashdeposit)) {
			if (rbcashdeposit.getValue() == true) {
				olbpettycash.setVisible(false);
				rbpettycash.setValue(false);
				dbavailableblnc.setVisible(false);
				
				fpettycash.getHeaderLabel().setVisible(false);
				ftbavailableblnc.getHeaderLabel().setVisible(false);
				
				rbBank.setValue(false);
				objPaymentMode.setVisible(false);
				fobjPaymentMode.getHeaderLabel().setVisible(false);
			}
		}
		
		if (event.getSource().equals(rbBank)) {
			if (rbBank.getValue() == true) {
				rbpettycash.setValue(false);
				olbpettycash.setVisible(false);
				dbavailableblnc.setVisible(false);
				
				fpettycash.getHeaderLabel().setVisible(false);
				ftbavailableblnc.getHeaderLabel().setVisible(false);
				
				rbcashdeposit.setValue(false);
				
				rbBank.setValue(true);
				objPaymentMode.setVisible(true);
				fobjPaymentMode.getHeaderLabel().setVisible(true);
			}
		}
	}

	public void fillinview2(){	
	PettyCash petty=olbpettycash.getSelectedItem();
	dbavailableblnc.setValue(petty.getPettyCashBalance());
	System.out.println(petty.getPettyCashBalance());
	}

	public ObjectListBox<PettyCash> getOlbpettycashname() {
		return olbpettycashname;
	}

	public void setOlbpettycashname(ObjectListBox<PettyCash> olbpettycashname) {
		this.olbpettycashname = olbpettycashname;
	}

	public TextBox getTbpersonresponsible() {
		return tbpersonresponsible;
	}

	public void setTbpersonresponsible(TextBox tbpersonresponsible) {
		this.tbpersonresponsible = tbpersonresponsible;
	}

	public TextBox getTbbranch() {
		return tbbranch;
	}

	public void setTbbranch(TextBox tbbranch) {
		this.tbbranch = tbbranch;
	}

	public PettyCash getPettyCashObj() {
		return pettyCashObj;
	}

	public void setPettyCashObj(PettyCash pettyCashObj) {
		this.pettyCashObj = pettyCashObj;
	}

	public ObjectListBox<PettyCash> getOlbpettycash() {
		return olbpettycash;
	}

	public void setOlbpettycash(ObjectListBox<PettyCash> olbpettycash) {
		this.olbpettycash = olbpettycash;
	}
	/**
	 * Updated By: Viraj
	 * Date: 24-06-2019
	 * Description: To Store deposit Date
	 */
	public DateBox getDepositDate() {
		return depositDate;
	}

	public void setDepositDate(DateBox depositDate) {
		this.depositDate = depositDate;
	}
	/** Ends **/

}
