package com.slicktechnologies.client.views.account.pettycashtransactions;

import java.util.Vector;

import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.account.pettycashtransactions.PettyCashTransactionPresenter.PettyCashTransactionPresenterSearch;
import com.slicktechnologies.shared.PettyCash;
import com.slicktechnologies.shared.PettyCashDeposits;
import com.slicktechnologies.shared.common.personlayer.Employee;

public class PettyCashTransactionPresenterSearchProxy extends PettyCashTransactionPresenterSearch {
	
	public DateComparator dateComparator;
	public ObjectListBox<PettyCash> olbpetycash;
	public ObjectListBox<Employee> olbpersonresponsible;
	
	/**
	 * Date :- 04-03-2017
	 * added by vijay
	 * for reference no search
	 */
	public IntegerBox ibReferenceId;
	
	/**
	 * ends here
	 */
	
	public Object getVarRef(String varName)
	{
		 
		return null ;
	}
	
	public PettyCashTransactionPresenterSearchProxy() {
		super();
		createGui();
	}
	
	public void initWidget()
	{
		olbpetycash=new ObjectListBox<PettyCash>();
		olbpetycash.MakeLive(new MyQuerry(new Vector<Filter>(), new PettyCash()));
		olbpersonresponsible=new ObjectListBox<Employee>();
		AppUtility.makeSalesPersonListBoxLive(olbpersonresponsible);
		dateComparator= new DateComparator("depositDate",new PettyCashDeposits());
		
		ibReferenceId = new IntegerBox();
	}
	public void createScreen()
	{
		initWidget();
		FormFieldBuilder builder;
		builder = new FormFieldBuilder("From Date",dateComparator.getFromDate());
		FormField ffromdateComparator= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		builder = new FormFieldBuilder("To Date",dateComparator.getToDate());
		FormField ftodateComparator= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Petty Cash",olbpetycash);
		  FormField folbpetycash= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		  
		  builder = new FormFieldBuilder("Person Responsible",olbpersonresponsible);
		  FormField folbpersonresponsible= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		  builder = new FormFieldBuilder("Reference Id",ibReferenceId);
		  FormField ftbReferenceId= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		  
		this.fields=new FormField[][]{
				{folbpetycash,ffromdateComparator,ftodateComparator},
				{folbpersonresponsible,ftbReferenceId}//folbpetycash
		};
	}
	
	
	public MyQuerry getQuerry()
	{
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		if(dateComparator.getValue()!=null)
		{
			filtervec.addAll(dateComparator.getValue());
		}
		if(olbpersonresponsible.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbpersonresponsible.getValue().trim());
			temp.setQuerryString("employee");
			filtervec.add(temp);
		}
		if(olbpetycash.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbpetycash.getValue().trim());
			temp.setQuerryString("pettyCashName");
			filtervec.add(temp);
		}
		
		if(ibReferenceId.getValue()!=null){
			temp=new Filter();
			temp.setIntValue(ibReferenceId.getValue());
			temp.setQuerryString("paymentdocId");
			filtervec.add(temp);
		}
		
		MyQuerry querry= new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new PettyCashDeposits());
		return querry;
	}
	
	
}
