package com.slicktechnologies.client.views.account.pettycashtransactions;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;
import com.slicktechnologies.client.views.account.pettycashtransactions.PettyCashTransactionPresenter.PettyCashTransactionPresenterTable;
import com.slicktechnologies.shared.PettyCash;

public class PettyCashTransactionPresenterTableProxy extends PettyCashTransactionPresenterTable {
	
	TextColumn<PettyCash> getPettyCashNameColumn;
	TextColumn<PettyCash> getPersonResponsibleColumn;
	TextColumn<PettyCash> getBalanceColumn;
	TextColumn<PettyCash> getBranchColumn;
	TextColumn<PettyCash> getCountColumn;
	
	
	public PettyCashTransactionPresenterTableProxy()
	{
		super();
	}
	
	@Override public void createTable() {
		addColumngetCount();
		addColumngetName();
		addColumngetPersonResponsible();
		addColumngetBranch();
		addColumngetBalance();
		
	}
	@Override
	protected void initializekeyprovider() {
		keyProvider= new ProvidesKey<PettyCash>()
				{
			@Override
			public Object getKey(PettyCash item)
			{
				if(item==null)
				{
					return null;
				}
				else
					return item.getId();
			}
				};
	}
	@Override
	public void setEnable(boolean state)
	{
	}
	@Override
	public void applyStyle()
	{
	}

	public void addColumnSorting(){
		addSortinggetCount();
		addSortinggetName();
		addSortinggetPersonResponsible();
		addSortinggetBranch();
		addSortinggetBalance();
	}
	@Override public void addFieldUpdater() {
	}
	protected void addSortinggetCount()
	{
		List<PettyCash> list=getDataprovider().getList();
		columnSort=new ListHandler<PettyCash>(list);
		columnSort.setComparator(getCountColumn, new Comparator<PettyCash>()
				{
			@Override
			public int compare(PettyCash e1,PettyCash e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetCount()
	{
		getCountColumn=new TextColumn<PettyCash>()
				{
			@Override
			public String getValue(PettyCash object)
			{
				if( object.getCount()==-1)
					return "N.A";
				else return object.getCount()+"";
			}
				};
				table.addColumn(getCountColumn,"Id");
				getCountColumn.setSortable(true);
	}
	
	
	protected void addSortinggetName()
	{
		List<PettyCash> list=getDataprovider().getList();
		columnSort=new ListHandler<PettyCash>(list);
		columnSort.setComparator(getPettyCashNameColumn, new Comparator<PettyCash>()
				{
			@Override
			public int compare(PettyCash e1,PettyCash e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getPettyCashName()!=null && e2.getPettyCashName()!=null){
						return e1.getPettyCashName().compareTo(e2.getPettyCashName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetName()
	{
		getPettyCashNameColumn=new TextColumn<PettyCash>()
				{
			@Override
			public String getValue(PettyCash object)
			{
				return object.getPettyCashName();
			}
				};
				table.addColumn(getPettyCashNameColumn,"Petty Cash");
				getPettyCashNameColumn.setSortable(true);
	}

	
	
	protected void addSortinggetPersonResponsible()
	{
		List<PettyCash> list=getDataprovider().getList();
		columnSort=new ListHandler<PettyCash>(list);
		columnSort.setComparator(getPersonResponsibleColumn, new Comparator<PettyCash>()
				{
			@Override
			public int compare(PettyCash e1,PettyCash e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getEmployee()!=null && e2.getEmployee()!=null){
						return e1.getEmployee().compareTo(e2.getEmployee());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetPersonResponsible()
	{
		getPersonResponsibleColumn=new TextColumn<PettyCash>()
				{
			@Override
			public String getValue(PettyCash object)
			{
				return object.getEmployee();
			}
				};
				table.addColumn(getPersonResponsibleColumn,"Person Responsible");
				getPersonResponsibleColumn.setSortable(true);
	}

	
	
	protected void addSortinggetBranch()
	{
		List<PettyCash> list=getDataprovider().getList();
		columnSort=new ListHandler<PettyCash>(list);
		columnSort.setComparator(getBranchColumn, new Comparator<PettyCash>()
				{
			@Override
			public int compare(PettyCash e1,PettyCash e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBranch()!=null && e2.getBranch()!=null){
						return e1.getBranch().compareTo(e2.getBranch());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetBranch()
	{
		getBranchColumn=new TextColumn<PettyCash>()
				{
			@Override
			public String getValue(PettyCash object)
			{
				return object.getBranch();
			}
				};
				table.addColumn(getBranchColumn,"Branch");
				getBranchColumn.setSortable(true);
	}
	
	protected void addSortinggetBalance()
	{
		List<PettyCash> list=getDataprovider().getList();
		columnSort=new ListHandler<PettyCash>(list);
		columnSort.setComparator(getBalanceColumn, new Comparator<PettyCash>()
				{
			@Override
			public int compare(PettyCash e1,PettyCash e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getPettyCashBalance()!=null && e2.getPettyCashBalance()!=null){
						return e1.getPettyCashBalance().compareTo(e2.getPettyCashBalance());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetBalance()
	{
		getBalanceColumn=new TextColumn<PettyCash>()
				{
			@Override
			public String getValue(PettyCash object)
			{
				return object.getPettyCashBalance()+"";
			}
				};
				table.addColumn(getBalanceColumn,"Balance");
				getBalanceColumn.setSortable(true);
	}

}
