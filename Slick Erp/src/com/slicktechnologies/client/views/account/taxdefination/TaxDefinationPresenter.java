package com.slicktechnologies.client.views.account.taxdefination;

import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.shared.TaxDefination;

public class TaxDefinationPresenter extends FormTableScreenPresenter<TaxDefination>
{
	TaxDefinationForm form;
	
	public TaxDefinationPresenter (FormTableScreen<TaxDefination> view,
			TaxDefination model) {
		super(view, model);
		form=(TaxDefinationForm) view;
		form.getSupertable().connectToLocal();
		form.retriveTable(getTaxDefinationQuery());
		form.setPresenter(this);
	}

	/**
	 * Method template to set the processBar events
	 */
	@Override
	public void reactToProcessBarEvents(ClickEvent e) 
   {
		InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();
		
		if(text.equals(""))
			  reactTo();

		
	}
	
	

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

	/**
	 * Method template to set new model
	 */
	@Override
	protected void makeNewModel() {
		
		model=new TaxDefination();
	}
	
	/*
	 * Method template to set Myquerry object
	 */
	public MyQuerry getTaxDefinationQuery()
	{
		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new TaxDefination());
		return quer;
	}
	
	public void setModel(TaxDefination entity)
	{
		model=entity;
	}
	
	public static void initalize()
	{
		TaxDefinationPresenterTable gentableScreen=GWT.create( TaxDefinationPresenterTable.class);
			
		TaxDefinationForm  form=new  TaxDefinationForm(gentableScreen,FormTableScreen.UPPER_MODE,true);
			gentableScreen.setView(form);
			gentableScreen.applySelectionModle();
			
			  //// CountryPresenterTable gentableSearch=GWT.create( CountryPresenterTable.class);
			  ////   CountryPresenterSearch.staticSuperTable=gentableSearch;
			  ////     CountryPresenterSearch searchpopup=GWT.create( CountryPresenterSearch.class);
			  ////         form.setSearchpopupscreen(searchpopup);
			
			TaxDefinationPresenter  presenter=new  TaxDefinationPresenter  (form,new TaxDefination());
			AppMemory.getAppMemory().stickPnel(form);
			
			
		 
	}
	
	
		@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.TaxDefination")
		 public static class  TaxDefinationPresenterTable extends SuperTable<TaxDefination> implements GeneratedVariableRefrence{

			@Override
			public Object getVarRef(String varName) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void createTable() {
				// TODO Auto-generated method stub
				
			}

			@Override
			protected void initializekeyprovider() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void addFieldUpdater() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void setEnable(boolean state) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void applyStyle() {
				// TODO Auto-generated method stub
				
			}} ;
			
			@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.TaxDefination")
			 public static class  TaxDefinationPresenterSearch extends SearchPopUpScreen<  TaxDefination>{

				@Override
				public MyQuerry getQuerry() {
					// TODO Auto-generated method stub
					return null;
				}

				@Override
				public boolean validate() {
					// TODO Auto-generated method stub
					return true;
				}};
				
				  private void reactTo()
				  {
				}



	
	
	
}
