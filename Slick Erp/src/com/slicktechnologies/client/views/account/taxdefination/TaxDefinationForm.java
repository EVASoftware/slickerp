package com.slicktechnologies.client.views.account.taxdefination;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.shared.GLAccount;
import com.slicktechnologies.shared.TaxDefination;

public class TaxDefinationForm extends FormTableScreen<TaxDefination> implements ClickHandler{
	
	//Token to add the varialble declarations
	IntegerBox ibTaxid;
	TextBox ibtaxName;
	TextBox tbglaccountname;
	TextArea tadescription;
	DoubleBox dbpercent,dbfixValue;
	CheckBox cbfix;
	CheckBox cbinclusive;
	ObjectListBox<GLAccount> olbGLAccountName;
	CheckBox cbstatus;
	ObjectListBox<TaxDefination> olbparentTax;
	


	public TaxDefinationForm(SuperTable<TaxDefination> table, int mode,
			boolean captionmode) {
		super(table, mode, captionmode);
		createGui();
	}

	private void initalizeWidget()
	{
		ibTaxid=new IntegerBox();
		ibTaxid.setEnabled(false);
		ibtaxName=new TextBox();
	//	tbglaccountname=new TextBox();
		tadescription=new TextArea();
		dbpercent=new DoubleBox();
		dbfixValue=new DoubleBox();
		dbfixValue.setEnabled(false);
		cbfix=new CheckBox();
		cbfix.addClickHandler(this);
		cbinclusive=new CheckBox();
		this.olbGLAccountName=new ObjectListBox<GLAccount>();
		this.initalizeGLAccountNameListBox();
		cbstatus=new CheckBox();
		this.olbparentTax=new ObjectListBox<TaxDefination>();
		this.initalizeTaxDefination();
		
		
	}

	/*
	 * Method template to create the formtable screen
	 */

	@Override
	public void createScreen() {

		//Token to initialize the processlevel menus.
		
		
		initalizeWidget();

		
		//////////////////////////////////Form Field Declaration/////////////////////////////////////////////////

		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////

		//Token to initialize formfield
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingTaxInformation=fbuilder.setlabel("Tax Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		fbuilder = new FormFieldBuilder("Tax ID",ibTaxid);
		FormField fibTaxid= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Name",ibtaxName);
		FormField fibtaxName= fbuilder.setMandatory(true).setMandatoryMsg("Tax code is mandatory").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Description",tadescription);
		FormField ftadescription= fbuilder.setMandatory(false).setMandatoryMsg("").setRowSpan(0).setColSpan(5).build();
		fbuilder = new FormFieldBuilder("% Tax",dbpercent);
		FormField fdbpercent=fbuilder.setMandatory(false).setMandatoryMsg("% Tax is mandatory").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Fix Value",dbfixValue);
		FormField fdbfixValue=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Inclusive",cbinclusive);
		FormField fcbinclusive=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("GLAccount",olbGLAccountName);
		FormField folbGLAccountName=fbuilder.setMandatory(true).setMandatoryMsg("GLAccount is mandatory").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Fix",cbfix);
		FormField fcbfix=fbuilder.setMandatory(true).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Status",cbstatus);
		FormField fcbstatus=fbuilder.setMandatory(true).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Parent Tax",olbparentTax);
		FormField folbparentTax=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();


		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////


		FormField[][] formfield = {   {fgroupingTaxInformation},
				{fibTaxid,fibtaxName,fcbfix,fdbfixValue,fdbpercent},
				{fcbinclusive,folbGLAccountName,folbparentTax,fcbstatus},{ftadescription}
		};


		this.fields=formfield;		
	}

	
	/**
	 * method template to update the model with token entity name
	 */
	@Override
	public void updateModel(TaxDefination model) 
	{
		if(ibtaxName.getValue()!=null)
		model.setTaxName(ibtaxName.getValue());
		if(tadescription.getValue()!=null)
			model.setDescription(tadescription.getValue());
		if(cbfix.getValue()==false){
			if(dbpercent.getValue()!=null)
		model.setPercent(dbpercent.getValue());
		}
			model.setFixed(cbfix.getValue());
			if(cbfix.getValue()==true)
		model.setFixValue(dbfixValue.getValue());
		if(cbinclusive.getValue()==true)
			model.setInclusive("Inclusive");
		else
			model.setInclusive("Exclusive");
		if(olbGLAccountName.getValue()!=null)
			model.setGlaccountName(olbGLAccountName.getValue());
		if(cbstatus.getValue()!=null)
		{
			if(cbstatus.getValue()==true)
				model.setStatus("Active");
			else
				model.setStatus("Inactive");
		}
		if(olbparentTax.getValue()!=null)
			model.setParentTax(olbparentTax.getValue());
		presenter.setModel(model);
	}

	/**
	 * method template to update the view with token entity name
	 */
	@Override
	public void updateView(TaxDefination view) 
	{
		if(view.getTaxName()!=null)
			ibtaxName.setValue(view.getTaxName());
		
		if(view.getDescription()!=null)
			tadescription.setValue(view.getDescription());
		dbpercent.setValue(view.getPercent());
		if(view.getFixed()!=null)
			cbfix.setValue(view.getFixed());
		dbfixValue.setValue(view.getFixValue());
		if(view.getInclusive()=="Inclusive")
			cbinclusive.setValue(true);
		else
			cbinclusive.setValue(false);
		if(view.getGlaccountName()!=null)
			olbGLAccountName.setValue(view.getGlaccountName());
			
		if(view.getStatus()!=null)
		{
			if(view.getStatus()=="Active")
				cbstatus.setValue(true);
			else
				cbstatus.setValue(false);
				
		}
		if(view.getParentTax()!=null)
			olbparentTax.setValue(view.getParentTax());
		presenter.setModel(view);

	}

	/**
	 * Toggles the app header bar menus as per screen state
	 */

	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
	}

	/**
	 * sets the id textbox with the passed count value. 
	 */
	@Override
	public void setCount(int count)
	{
		this.ibTaxid.setValue(presenter.getModel().getCount());
	}

	
	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		super.setEnable(state);
		ibTaxid.setEnabled(false);
		if(cbfix.getValue()==true){
			dbfixValue.setEnabled(true);
			dbpercent.setEnabled(false);
		}
		else
		{
			dbfixValue.setEnabled(false);
			dbpercent.setEnabled(true);
		
		}
		
	}

	


@Override
public void onClick(ClickEvent event) {
	if(cbfix.getValue()==true){
		dbfixValue.setEnabled(true);
		dbpercent.setEnabled(false);
		dbpercent.setText("");
	}
	else
	{
		dbfixValue.setEnabled(false);
		dbfixValue.setText("");
		dbpercent.setEnabled(true);
		
	
	}
	
}

protected void initalizeGLAccountNameListBox()
{
	MyQuerry querry = new MyQuerry();
	querry.setQuerryObject(new GLAccount());
	olbGLAccountName.MakeLive(querry);
}

protected void initalizeTaxDefination()
{
	MyQuerry querry = new MyQuerry();
	querry.setQuerryObject(new TaxDefination());
	olbparentTax.MakeLive(querry);
}


@Override
public boolean validate()
{
	boolean superRes= super.validate();
	if(superRes==false)
		return false;
	

	if(cbfix.getValue()==true){
		if(dbfixValue.getValue()==null)
		{
			this.showDialogMessage("Fix Value is mandatory");
			return false;
		}
	}
	else if(cbfix.getValue()==false)
		{
			if(dbpercent.getValue()==null)
			{
				this.showDialogMessage("Tax is mandatory");
				return false;
			}
		}
	
	
	
	return true;
	
}



}
