package com.slicktechnologies.client.views.account.dashboard;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.code.p.gwtchismes.client.GWTCGlassPanel;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.view.client.ProvidesKey;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CommunicationLogService;
import com.slicktechnologies.client.services.CommunicationLogServiceAsync;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.communicationlog.CommunicationLogPopUp;
import com.slicktechnologies.shared.common.BillUnbilledAr;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.customerlog.CustomerLogDetails;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
/**
 * @author Viraj
 * Date: 25-01-2019
 * Description: To display data about billed and unbilled dashboard
 */
public class BillUnbilledArTable extends SuperTable<BillUnbilledAr> implements ClickHandler{

	TextColumn<BillUnbilledAr> getColumnId;
	/**
	 * Updated By:Viraj
	 * Date: 09-03-2019
	 * Description: To add Doc date and Doc type
	 */
	TextColumn<BillUnbilledAr> getDocType;
	TextColumn<BillUnbilledAr> getDocDate;
	/** Ends **/
	TextColumn<BillUnbilledAr> getFollowUpDate;
	TextColumn<BillUnbilledAr> getCustomerName;
	TextColumn<BillUnbilledAr> getCustomerCell;
	TextColumn<BillUnbilledAr> getContractId;
	TextColumn<BillUnbilledAr> getSalesAmt;
	TextColumn<BillUnbilledAr> getInvoiceAmt;
	TextColumn<BillUnbilledAr> getStatus;
	TextColumn<BillUnbilledAr> getBranch;
	Column<BillUnbilledAr , String> followUpDateButton;
	/**
	 * Updated By: Viraj
	 * Date: 13-03-2019
	 * Description: To add balance amt in billed
	 */
	TextColumn<BillUnbilledAr> getBalanceAmt;
	/** Ends **/
	PopupPanel communicationPanel;
	CommunicationLogPopUp communicationLogPopUp = new CommunicationLogPopUp();
	CommunicationLogServiceAsync communicationService = GWT.create(CommunicationLogService.class);
	int rowIndex ;
	CustomerPayment customerPayment;
	final GWTCAlert alert = new GWTCAlert(); 
	final GWTCGlassPanel glassPanel = new GWTCGlassPanel();
	final GenricServiceAsync service = GWT.create(GenricService.class);
	static boolean followUpFlag =  false;
	
	boolean btnFlag = false;		
	
	public BillUnbilledArTable() {
		super();
		getTable().setHeight("500px");
	}
	/**
	 * Updated By: Viraj 
	 * Date: 16-02-2019
	 * Description: For communication log
	 */
	public BillUnbilledArTable(boolean billedFlag) {
		super(billedFlag);
		getTable().setHeight("500px");
		communicationLogPopUp.getBtnOk().addClickHandler(this);
		communicationLogPopUp.getBtnCancel().addClickHandler(this);
		communicationLogPopUp.getBtnAdd().addClickHandler(this);
		btnFlag = billedFlag;
		System.out.println("Inside parameterised constructer: "+btnFlag);
		createTable(billedFlag);
	}
	
	public void createTable(boolean billedFlag) {
		
		addColumnGetColumnId();
		System.out.println("flag: "+btnFlag);
		/**
		 * Updated By:Viraj
		 * Date: 09-03-2019
		 * Description: To add Doc date and Doc type
		 */
		addColumnGetDocType();
		addColumnGetDocDate();
		/** Ends **/
		if(btnFlag) {
			createColumnFollowupDate();
			updateColumnFollowupDate();
		}
		
		addColumnGetFollowUp();
		addColumnGetCustomerName();
		addColumnGetCustomerCell();
		addColumnGetContractId();
		addColumnGetSalesAmt();
		addColumnGetInvoiceAmt();
		addColumnGetBalAmt();			//Updated by: Viraj Date:13-03-2019 Description: To show balance amt
		addColumnGetStatus();
		addColumnGetBranch();
		
		addColumnSorting();
		
	}
	/** Ends **/
	
	@Override
	public void createTable() {
		
		addColumnGetColumnId();
		/**
		 * Updated By:Viraj
		 * Date: 09-03-2019
		 * Description: To add Doc date and Doc type
		 */
		addColumnGetDocType();
		addColumnGetDocDate();
		/** Ends **/
		addColumnGetFollowUp();
		addColumnGetCustomerName();
		addColumnGetCustomerCell();
		addColumnGetContractId();
		addColumnGetSalesAmt();
		addColumnGetInvoiceAmt();
		addColumnGetStatus();
		addColumnGetBranch();		//Updated by: Viraj Date:13-03-2019 Description: To sort balance amt
		
		addColumnSorting();
		
	}
	
	public void addColumnSorting(){
		addSortingGetColumnId();
		/**
		 * Updated By:Viraj
		 * Date: 09-03-2019
		 * Description: To add Doc date and Doc type
		 */
		addSortingGetDocType();
		addSortingGetDocDate();
		/** Ends **/
		addSortingGetFollowUp();
		addSortingGetCustomerName();
		addSortingGetCustomerCell();
		addSortingGetContractId();
		addSortingGetSalesAmt();
		addSortingGetInvoiceAmt();
		addSortingGetStatus();
		addSortingGetBranch();
		addSortingGetBalance();
	}
	
	/**
	 * Updated By:Viraj
	 * Date: 13-03-2019
	 * Description: To show balance amt
	 */
	private void addColumnGetBalAmt() {
		getBalanceAmt = new TextColumn<BillUnbilledAr>() {
			
			@Override
			public String getValue(BillUnbilledAr object) {
				return object.getBalAmt()+"";
			}
		};
		table.addColumn(getBalanceAmt,"Balance Amt");
		table.setColumnWidth(getBalanceAmt, "100px");
		getBalanceAmt.setSortable(true);
		
	}
	
	private void addSortingGetBalance() {
		List<BillUnbilledAr> list=getDataprovider().getList();
		  columnSort=new ListHandler<BillUnbilledAr>(list);
		  columnSort.setComparator(getBalanceAmt, new Comparator<BillUnbilledAr>() {
			  @Override
			  public int compare(BillUnbilledAr e1,BillUnbilledAr e2) {
				  if(e1!=null && e2!=null) {
					  if(e1.getBalAmt() == e2.getBalAmt()) {
						  return 0;
					  }
					  if(e1.getBalAmt() > e2.getBalAmt()) {
						  return 1;
					  }
					  else {
						  return -1;
					  }
				  }
				  else {
					  return 0;
				  }
			  }
		  });
		  table.addColumnSortHandler(columnSort);
		
	}
	/** Ends **/
	
	/**
	 * Updated By:Viraj
	 * Date: 09-03-2019
	 * Description: To add Doc date and Doc type
	 */
	private void addColumnGetDocDate() {
		getDocDate = new TextColumn<BillUnbilledAr>() {
			
			@Override
			public String getValue(BillUnbilledAr object) {
				if(object.getDocDate() != null) {
					return AppUtility.parseDate(object.getDocDate())+"";
				} else {
					return "";
				}
			}
		};
		table.addColumn(getDocDate,"Due Date");
		table.setColumnWidth(getDocDate, "100px");
		getDocDate.setSortable(true);
	}
	
	private void addSortingGetDocDate() {
		List<BillUnbilledAr> list=getDataprovider().getList();
		columnSort=new ListHandler<BillUnbilledAr>(list);
		columnSort.setComparator(getDocDate, new Comparator<BillUnbilledAr>()
		{
		@Override
		public int compare(BillUnbilledAr e1,BillUnbilledAr e2)
		{
		if(e1!=null && e2!=null)
		{
			 if(e1.getDocDate() == null && e2.getDocDate()==null){
			        return 0;
			    }
			    if(e1.getDocDate() == null) return 1;
			    if(e2.getDocDate() == null) return -1;
			    
			    return e1.getDocDate().compareTo(e2.getDocDate());
		}
		return 0;
		}
		});
		table.addColumnSortHandler(columnSort);
		
	}
	
	private void addColumnGetDocType() {
		getDocType = new TextColumn<BillUnbilledAr>() {
			
			@Override
			public String getValue(BillUnbilledAr object) {
					return object.getDocType();
			}
		};
		table.addColumn(getDocType, "Type");
	    table.setColumnWidth(getDocType,"100px");
	    getDocType.setSortable(true);
		
	}
	
	private void addSortingGetDocType() {
		
		List<BillUnbilledAr> list=getDataprovider().getList();
		  columnSort=new ListHandler<BillUnbilledAr>(list);
		  columnSort.setComparator(getDocType, new Comparator<BillUnbilledAr>() {
			  @Override
			  public int compare(BillUnbilledAr e1,BillUnbilledAr e2) {
				  if(e1!=null && e2!=null)
					{
						if( e1.getDocType()!=null && e2.getDocType()!=null){
							return e1.getDocType().compareTo(e2.getDocType());}
					}
				  else {
					  return 0;
				  }
			 return 0;
			  }
		  });
		  table.addColumnSortHandler(columnSort);
		
	}
	/** Ends **/
	
	private void addColumnGetBranch() {
		getBranch = new TextColumn<BillUnbilledAr>() {
			@Override
			public String getValue(BillUnbilledAr object) {
					return object.getBranch();
			}
		};
		table.addColumn(getBranch, "Branch");
	    table.setColumnWidth(getBranch,"100px");
		getBranch.setSortable(true);
	}

	private void addSortingGetBranch() {
		List<BillUnbilledAr> list=getDataprovider().getList();
		  columnSort=new ListHandler<BillUnbilledAr>(list);
		  columnSort.setComparator(getBranch, new Comparator<BillUnbilledAr>() {
			  @Override
			  public int compare(BillUnbilledAr e1,BillUnbilledAr e2) {
				  if(e1!=null && e2!=null)
					{
						if( e1.getBranch()!=null && e2.getBranch()!=null){
							return e1.getBranch().compareTo(e2.getBranch());}
					}
				  else {
					  return 0;
				  }
			 return 0;
			  }
		  });
		  table.addColumnSortHandler(columnSort);
		
	}
	
	private void addColumnGetStatus() {
		getStatus = new TextColumn<BillUnbilledAr>() {
			
			@Override
			public String getValue(BillUnbilledAr object) {
				return object.getStatus();
			}
		};
		table.addColumn(getStatus,"Status");
		table.setColumnWidth(getStatus, "100px");
		getStatus.setSortable(true);
	}
	
	private void addSortingGetStatus() {
		  List<BillUnbilledAr> list=getDataprovider().getList();
		  columnSort=new ListHandler<BillUnbilledAr>(list);
		  columnSort.setComparator(getStatus, new Comparator<BillUnbilledAr>() {
			  @Override
			  public int compare(BillUnbilledAr e1,BillUnbilledAr e2) {
				  if(e1!=null && e2!=null)
					{
						if( e1.getStatus()!=null && e2.getStatus()!=null){
							return e1.getStatus().compareTo(e2.getStatus());}
					}
				  else {
					  return 0;
				  }
			 return 0;
			  }
		  });
		  table.addColumnSortHandler(columnSort);
		
	}

	private void addColumnGetInvoiceAmt() {
		getInvoiceAmt = new TextColumn<BillUnbilledAr>() {
			
			@Override
			public String getValue(BillUnbilledAr object) {
				return object.getInvoiceAmt()+"";
			}
		};
		table.addColumn(getInvoiceAmt,"Invoice Amt");
		table.setColumnWidth(getInvoiceAmt, "100px");
		getInvoiceAmt.setSortable(true);
	}

	private void addSortingGetInvoiceAmt() {
		  List<BillUnbilledAr> list=getDataprovider().getList();
		  columnSort=new ListHandler<BillUnbilledAr>(list);
		  columnSort.setComparator(getInvoiceAmt, new Comparator<BillUnbilledAr>() {
			  @Override
			  public int compare(BillUnbilledAr e1,BillUnbilledAr e2) {
				  if(e1!=null && e2!=null) {
					  if(e1.getInvoiceAmt() == e2.getInvoiceAmt()) {
						  return 0;
					  }
					  if(e1.getInvoiceAmt() > e2.getInvoiceAmt()) {
						  return 1;
					  }
					  else {
						  return -1;
					  }
				  }
				  else {
					  return 0;
				  }
			  }
		  });
		  table.addColumnSortHandler(columnSort);
		
	}
	
	private void addColumnGetSalesAmt() {
		getSalesAmt = new TextColumn<BillUnbilledAr>() {
			
			@Override
			public String getValue(BillUnbilledAr object) {
				return object.getSalesAmount()+"";
			}
		};
		table.addColumn(getSalesAmt,"Sales Amt");
		table.setColumnWidth(getSalesAmt, "100px");
		getSalesAmt.setSortable(true);
	}

	private void addSortingGetSalesAmt() {
		  List<BillUnbilledAr> list=getDataprovider().getList();
		  columnSort=new ListHandler<BillUnbilledAr>(list);
		  columnSort.setComparator(getSalesAmt, new Comparator<BillUnbilledAr>() {
			  @Override
			  public int compare(BillUnbilledAr e1,BillUnbilledAr e2) {
				  if(e1!=null && e2!=null) {
					  if(e1.getSalesAmount() == e2.getSalesAmount()) {
						  return 0;
					  }
					  if(e1.getSalesAmount() > e2.getSalesAmount()) {
						  return 1;
					  }
					  else {
						  return -1;
					  }
				  }
				  else {
					  return 0;
				  }
			  }
		  });
		  table.addColumnSortHandler(columnSort);
		
	}
	
	private void addColumnGetContractId() {
		getContractId = new TextColumn<BillUnbilledAr>() {
			
			@Override
			public String getValue(BillUnbilledAr object) {
				return object.getContractId()+"";
			}
		};
		table.addColumn(getContractId,"Contract Id");
		table.setColumnWidth(getContractId, "100px");
		getContractId.setSortable(true);
	}

	private void addSortingGetContractId() {
		  List<BillUnbilledAr> list=getDataprovider().getList();
		  columnSort=new ListHandler<BillUnbilledAr>(list);
		  columnSort.setComparator(getContractId, new Comparator<BillUnbilledAr>() {
			  @Override
			  public int compare(BillUnbilledAr e1,BillUnbilledAr e2) {
				  if(e1!=null && e2!=null) {
					  if(e1.getContractId() == e2.getContractId()) {
						  return 0;
					  }
					  if(e1.getContractId() > e2.getContractId()) {
						  return 1;
					  }
					  else {
						  return -1;
					  }
				  }
				  else {
					  return 0;
				  }
			  }
		  });
		  table.addColumnSortHandler(columnSort);
		
	}
	
	private void addColumnGetCustomerCell() {
		getCustomerCell = new TextColumn<BillUnbilledAr>() {
			
			@Override
			public String getValue(BillUnbilledAr object) {
				return object.getCustomerCell()+"";
			}
		};
		table.addColumn(getCustomerCell,"Customer Cell");
		table.setColumnWidth(getCustomerCell, "100px");
		getCustomerCell.setSortable(true);
	}

	private void addSortingGetCustomerCell() {
		  List<BillUnbilledAr> list=getDataprovider().getList();
		  columnSort=new ListHandler<BillUnbilledAr>(list);
		  columnSort.setComparator(getCustomerCell, new Comparator<BillUnbilledAr>() {
			  @Override
			  public int compare(BillUnbilledAr e1,BillUnbilledAr e2) {
				  if(e1!=null && e2!=null) {
					  if(e1.getCustomerCell() == e2.getCustomerCell()) {
						  return 0;
					  }
					  if(e1.getCustomerCell() > e2.getCustomerCell()) {
						  return 1;
					  }
					  else {
						  return -1;
					  }
				  }
				  else {
					  return 0;
				  }
			  }
		  });
		  table.addColumnSortHandler(columnSort);
		
	}
	
	private void addColumnGetCustomerName() {
		getCustomerName = new TextColumn<BillUnbilledAr>() {
			
			@Override
			public String getValue(BillUnbilledAr object) {
				return object.getCustomerName();
			}
		};
		table.addColumn(getCustomerName,"Customer Name");
		table.setColumnWidth(getCustomerName, "100px");
		getCustomerName.setSortable(true);
	}

	private void addSortingGetCustomerName() {
		  List<BillUnbilledAr> list=getDataprovider().getList();
		  columnSort=new ListHandler<BillUnbilledAr>(list);
		  columnSort.setComparator(getCustomerName, new Comparator<BillUnbilledAr>() {
			  @Override
			  public int compare(BillUnbilledAr e1,BillUnbilledAr e2) {
				  if(e1!=null && e2!=null)
					{
						if( e1.getCustomerName()!=null && e2.getCustomerName()!=null){
							return e1.getCustomerName().compareTo(e2.getCustomerName());}
					}
				  else {
					  return 0;
				  }
			 return 0;
			  }
		  });
		  table.addColumnSortHandler(columnSort);
		
	}
	
	private void addColumnGetFollowUp() {
		final Date todaysDate=new Date();
		CalendarUtil.addDaysToDate(todaysDate,-1);
		todaysDate.setHours(23);
		todaysDate.setMinutes(59);
		todaysDate.setSeconds(59);
		System.out.println("Todays Date Converted ::: "+todaysDate);
		
		getFollowUpDate = new TextColumn<BillUnbilledAr>() {
			
			@Override
			public String getCellStyleNames(Context context, BillUnbilledAr object) {
				if(object.getFollowUp()!=null&&(object.getFollowUp().before(todaysDate))&&(object.getStatus().equals("Created")||object.getStatus().equals("Requested"))){
					 return "red";
				 }
				 else if(object.getStatus().equals("Approved")||object.getStatus().equals("Closed")){
					 return "green";
				 }
				 else{
					 return "black";
				 }
			}
			
			@Override
			public String getValue(BillUnbilledAr object) {
				if(object.getFollowUp() != null) {
					return AppUtility.parseDate(object.getFollowUp())+"";
				} else {
					return "";
				}
			}
		};
		table.addColumn(getFollowUpDate,"Follow Up");
		table.setColumnWidth(getFollowUpDate, "100px");
		getFollowUpDate.setSortable(true);
	}
	/**
	 * Updated By: Viraj 
	 * Date: 16-02-2019
	 * Description: For communication log
	 */
	protected void createColumnFollowupDate() {
		ButtonCell btnCell = new ButtonCell();
		System.out.println("INSIDE followup date button cell");
	  followUpDateButton = new Column<BillUnbilledAr, String>(btnCell) {
			
			@Override
			public String getValue(BillUnbilledAr object) {
				// TODO Auto-generated method stub
				return "Follow-up Date";
			}
		};                 
		
		table.addColumn(followUpDateButton, "");
		table.setColumnWidth(followUpDateButton,130,Unit.PX);
		
	} 
	
	protected void updateColumnFollowupDate()
	{
		// TODO Auto-generated method stub
		followUpDateButton.setFieldUpdater(new FieldUpdater<BillUnbilledAr, String>() {
			
					@Override
					public void update(int index, final BillUnbilledAr object, String value) {
						
						
						rowIndex=index;
						followUpFlag = true;
						if(object.getDocType().equals(AppConstants.PAYMENTDETAILS)){
							System.out.println("Inside paymentDetails query table");
							MyQuerry query1 = new MyQuerry();
							Company c1=new Company();
							Vector<Filter> filtervec1=new Vector<Filter>();
							Filter filter1 = null;
							
							filter1 = new Filter();
							filter1.setQuerryString("companyId");
							filter1.setLongValue(c1.getCompanyId());
							filtervec1.add(filter1);
							
							filter1 = new Filter();
							filter1.setQuerryString("count");
							filter1.setLongValue(object.getId());
							filtervec1.add(filter1);
							
							query1.setFilters(filtervec1);
							query1.setQuerryObject(new CustomerPayment());
							
							service.getSearchResult(query1, new AsyncCallback<ArrayList<SuperModel>>() {

								@Override
								public void onFailure(Throwable caught) {
									// TODO Auto-generated method stub
									
								}

								@Override
								public void onSuccess(ArrayList<SuperModel> result) {
									for(SuperModel model : result) {
										customerPayment = (CustomerPayment)model;
										System.out.println("customerPayment: "+customerPayment);
									}
									System.out.println("count: "+customerPayment.getCount());
									System.out.println("companyId: "+customerPayment.getCompanyId());
									getLogDetails(customerPayment);
								}
							});
							
							
						}
						
					}
				});
		
	}
	
	private void getLogDetails(CustomerPayment customerPayment)
	{
		glassPanel.show();
		System.out.println("Inside getLogDetails count: "+customerPayment.getCount());
		System.out.println("Inside getLogDetails companyId: "+customerPayment.getCompanyId());
		MyQuerry querry = AppUtility.communicationLogQuerry(customerPayment.getCount(), customerPayment.getCompanyId(),AppConstants.ACCOUNTMODULE,AppConstants.PAYMENTDETAILS);
		
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				
				ArrayList<InteractionType> list = new ArrayList<InteractionType>();
				communicationLogPopUp.getCommunicationLogTable().clear();
				list.clear();
				for(SuperModel model : result){
					
					InteractionType interactionType = (InteractionType) model;
					list.add(interactionType);
					
				}
				Comparator<InteractionType> comp=new Comparator<InteractionType>() {
					@Override
					public int compare(InteractionType e1, InteractionType e2) {
						Integer ee1=e1.getCount();
						Integer ee2=e2.getCount();
						return ee2.compareTo(ee1);
					}
				};
				Collections.sort(list,comp);
	           
				glassPanel.hide();
				communicationLogPopUp.getRemark().setValue("");
				communicationLogPopUp.getDueDate().setValue(null);
				communicationLogPopUp.getOblinteractionGroup().setSelectedIndex(0);
				communicationLogPopUp.getCommunicationLogTable().getDataprovider().setList(list);
				communicationPanel = new PopupPanel(true);
				communicationPanel.add(communicationLogPopUp);
				
				communicationPanel.center();
				communicationPanel.show();
					
			}
			
			@Override
			public void onFailure(Throwable caught) {
				glassPanel.hide();
			}
		});
		
	}
	/** Ends **/
	private void addSortingGetFollowUp() {
		List<BillUnbilledAr> list=getDataprovider().getList();
		columnSort=new ListHandler<BillUnbilledAr>(list);
		columnSort.setComparator(getFollowUpDate, new Comparator<BillUnbilledAr>()
		{
		@Override
		public int compare(BillUnbilledAr e1,BillUnbilledAr e2)
		{
		if(e1!=null && e2!=null)
		{
			 if(e1.getFollowUp() == null && e2.getFollowUp()==null){
			        return 0;
			    }
			    if(e1.getFollowUp() == null) return 1;
			    if(e2.getFollowUp() == null) return -1;
			    
			    return e1.getFollowUp().compareTo(e2.getFollowUp());
		}
		return 0;
		}
		});
		table.addColumnSortHandler(columnSort);
		
	}
	
	private void addColumnGetColumnId() {
		getColumnId = new TextColumn<BillUnbilledAr>() {
			
			@Override
			public String getValue(BillUnbilledAr object) {
				return object.getId()+"";
			}
		};
		table.addColumn(getColumnId,"Id");
		table.setColumnWidth(getColumnId, "100px");
		getColumnId.setSortable(true);
	}
	
	private void addSortingGetColumnId() {
		  List<BillUnbilledAr> list=getDataprovider().getList();
		  columnSort=new ListHandler<BillUnbilledAr>(list);
		  columnSort.setComparator(getColumnId, new Comparator<BillUnbilledAr>() {
			  @Override
			  public int compare(BillUnbilledAr e1,BillUnbilledAr e2) {
				  if(e1!=null && e2!=null) {
					  if(e1.getId() == e2.getId()) {
						  return 0;
					  }
					  if(e1.getId() > e2.getId()) {
						  return 1;
					  }
					  else {
						  return -1;
					  }
				  }
				  else {
					  return 0;
				  }
			  }
		  });
		  table.addColumnSortHandler(columnSort);
		
	}

	@Override
	protected void initializekeyprovider() {
		keyProvider= new ProvidesKey<BillUnbilledAr>()
				{
			@Override
			public Object getKey(BillUnbilledAr item)
			{
				if(item==null)
				{
					return null;
				}
				else
					return item.getId();
			}
				};
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

	/**
	 * Updated By: Viraj 
	 * Date: 16-02-2019
	 * Description: For communication log
	 */
	@Override
	public void onClick(ClickEvent event) {
		if(event.getSource() == communicationLogPopUp.getBtnOk()){
			
			glassPanel.show();
		    List<InteractionType> list = communicationLogPopUp.getCommunicationLogTable().getDataprovider().getList();
		   
		    ArrayList<InteractionType> interactionlist = new ArrayList<InteractionType>();
		    interactionlist.addAll(list);
		    
		    boolean checkNewInteraction = AppUtility.checkNewInteractionAdded(interactionlist);
		    
		    if(checkNewInteraction==false){
		    	alert.alert("Please add new interaction details");
		    	glassPanel.hide();
		    }	
		    else{	
		    	
		    	final Date followUpDate = interactionlist.get(interactionlist.size()-1).getInteractionDueDate();
		    	 communicationService.saveCommunicationLog(interactionlist, new AsyncCallback<Void>() {
		    		 
						@Override
						public void onFailure(Throwable caught) {
							alert.alert("Unexpected Error");
							glassPanel.hide();
							LoginPresenter.communicationLogPanel.hide();
						}

						@Override
						public void onSuccess(Void result) {
							alert.alert("Data Save Successfully");
							List<BillUnbilledAr> list = getDataprovider().getList();
							list.get(rowIndex).setFollowUp(followUpDate);
							CustomerPayment CustomerPaymentObj = customerPayment;
							saveCustomerPayment(CustomerPaymentObj);
							followUpFlag = false;
							table.redraw();
							glassPanel.hide();
							communicationPanel.hide();
						} 
					});
		    }
		   
		}
		if(event.getSource() == communicationLogPopUp.getBtnCancel()){
			followUpFlag = false;
			communicationPanel.hide();
		}
		if(event.getSource() == communicationLogPopUp.getBtnAdd()){
				glassPanel.show();
				String remark = communicationLogPopUp.getRemark().getValue();
				Date dueDate = communicationLogPopUp.getDueDate().getValue();
				String interactiongGroup =null;
				if(communicationLogPopUp.getOblinteractionGroup().getSelectedIndex()!=0)
					interactiongGroup=communicationLogPopUp.getOblinteractionGroup().getValue(communicationLogPopUp.getOblinteractionGroup().getSelectedIndex());
				boolean validationFlag = AppUtility.validateCommunicationlog(remark,dueDate);
				
				List<InteractionType> list = communicationLogPopUp.getCommunicationLogTable().getDataprovider().getList();
				/**
				 * Date : 08-11-2017 BY Komal
				 */
				Comparator<InteractionType> comp=new Comparator<InteractionType>() {
					@Override
					public int compare(InteractionType e1, InteractionType e2) {
						if (e1.getCount() == e2.getCount()) {
							return 0;
						}
						if (e1.getCount() > e2.getCount()) {
							return 1;
						} else {
							return -1;
						}
					}
				};
				Collections.sort(list,comp);
	            
				
				if(validationFlag){
					InteractionType communicationLog =  AppUtility.getCommunicationLog(AppConstants.ACCOUNTMODULE,AppConstants.PAYMENTDETAILS,customerPayment.getCount(),customerPayment.getEmployee(),remark,dueDate,customerPayment.getPersonInfo(),null,interactiongGroup, customerPayment.getBranch(),"");
					list.add(communicationLog);
					communicationLogPopUp.getCommunicationLogTable().getDataprovider().setList(list);;
					communicationLogPopUp.getRemark().setValue("");
					communicationLogPopUp.getDueDate().setValue(null);
					communicationLogPopUp.getOblinteractionGroup().setSelectedIndex(0);
				}
				glassPanel.hide();
		}
		
	}
	/** Ends **/
	
	private void saveCustomerPayment(CustomerPayment CustomerPaymentObj) {
		service.save(CustomerPaymentObj,new AsyncCallback<ReturnFromServer>() {
		@Override
		public void onFailure(Throwable caught) {
			glassPanel.hide();
			
		     alert.alert("An Unexpected error occurred!");
		}
		@Override
		public void onSuccess(ReturnFromServer result) {
			glassPanel.hide();
			
		     alert.alert("Saved successfully!");
		}
	});
	
}
	
}
