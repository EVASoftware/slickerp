package com.slicktechnologies.client.views.account.dashboard;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.ListBox;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.customerlog.CustomerLogDetailsPresenter;
import com.slicktechnologies.shared.common.BillUnbilledAr;
import com.slicktechnologies.shared.common.DummyEntityOnlyForScreen;
import com.slicktechnologies.shared.common.customerlog.CustomerLogDetails;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
/**
 * @author Viraj
 * Date: 24-01-2019
 * Description: To display billed and unbilled in tabular format
 */
public class ArDashboardPresenter extends FormScreenPresenter<DummyEntityOnlyForScreen> implements ClickHandler{

	ArDashboardForm form;
	GenricServiceAsync genasync = GWT.create(GenricService.class);
	ArrayList<BillUnbilledAr> unBilledList = new ArrayList<BillUnbilledAr>();
	ArrayList<BillUnbilledAr> BilledList = new ArrayList<BillUnbilledAr>();
	int totalCountunBilled = 0;
	double tAmtReceivableunBilled = 0.0;
	double tAmountReceivableInvoice;
	double tAmountReceivableBilled;
	int countInvoice;
	int countBilled;
	NumberFormat df =NumberFormat.getFormat("#0.00");
	public ArDashboardPresenter(FormScreen<DummyEntityOnlyForScreen> view,
			DummyEntityOnlyForScreen model) {
		super(view, model);
		form = (ArDashboardForm) view;
		form.setPresenter(this);
		System.out.println("Inside constructor presentor");
		form.getBtgo().addClickHandler(this);
		form.getClear().addClickHandler(this);
	}
	
	public static ArDashboardForm initalize() {
		ArDashboardForm form = new ArDashboardForm();
		ArDashboardPresenter presenter = new ArDashboardPresenter(
				form, new DummyEntityOnlyForScreen());
		AppMemory.getAppMemory().stickPnel(form);
		return form;
	}

	@Override
	public void onClick(ClickEvent event) {
		super.onClick(event);
		System.out.println("Inside click method");
		
		/**
		 * Updated By: Viraj
		 * Date: 13-03-2019
		 * Description: To clearing all the paramters of search
		 */
		if(event.getSource().equals(form.getClear())) {
			form.dbfromDate.setValue(null);
			form.dbtoDate.setValue(null);
			form.olbBranch.setValue("");
			form.personInfoComposite.clear();
			form.lbStatus.clear();
			
		//	form.lbStatus=new ListBox();
			AppUtility.setStatusListBox(form.lbStatus, form.getStatusList());
		}
		/** Ends **/
		if(event.getSource().equals(form.getBtgo())) {
			System.out.println("Inside click event of go button");
			/**
			 * Updated By: Viraj
			 * Date: 08-03-2019
			 * Description: To check status if not selected from dropdown
			 */
			List<String> bStatusList = CustomerPayment.getStatusList();
			for(int i=0;i<bStatusList.size();i++) {
				if(bStatusList.get(i).equalsIgnoreCase(CustomerPayment.PAYMENTCLOSED)
						|| bStatusList.get(i).equalsIgnoreCase(CustomerPayment.CANCELLED)) {
					bStatusList.remove(i);
					i--;
				}
			}
			
			List<String> iStatusList = Invoice.getStatusList();
			for(int i=0;i<iStatusList.size();i++) {
				if(iStatusList.get(i).equalsIgnoreCase(Invoice.CANCELLED)
						|| iStatusList.get(i).equalsIgnoreCase(Invoice.PROFORMAINVOICE)
						|| iStatusList.get(i).equalsIgnoreCase(Invoice.APPROVED)) {
					iStatusList.remove(i);
					i--;
				}
			}
			
			List<String> bdocStatusList = BillingDocument.getStatusList();
			for(int i=0;i<bdocStatusList.size();i++) {
				if(bdocStatusList.get(i).equalsIgnoreCase(BillingDocument.CANCELLED)
						|| bdocStatusList.get(i).equalsIgnoreCase(BillingDocument.BILLINGINVOICED)) {
					bdocStatusList.remove(i);
					i--;
				}
			}
			/** Ends **/
			Console.log("From date@@@ "+form.getDbfromDate().getValue());
			Console.log("To date@@@ "+form.getDbtoDate().getValue());
			Console.log("branch name### "+ form.getOlbBranch().getValue());
			Console.log("customer name### "+ form.personInfoComposite.getValue());
			
			if(form.personInfoComposite.getValue()==null&&form.getDbfromDate().getValue() == null && form.getDbtoDate().getValue() == null
					&& form.getOlbBranch().getSelectedIndex() == 0){
				form.showDialogMessage("Please select search filters !");
				return;
			}
			
			if(form.personInfoComposite.getValue()==null && (form.getOlbBranch().getSelectedIndex() != 0 && 
					(form.getDbfromDate().getValue()== null ||form.getDbtoDate().getValue() == null))
					 || ((form.getDbfromDate().getValue()!= null ||form.getDbtoDate().getValue() != null) && 
					 form.getOlbBranch().getSelectedIndex() == 0))
			{
				form.showDialogMessage("Please select From Date and To Date with Branch filter");			
			}
			else if((form.personInfoComposite.getValue()!=null ) && (form.getDbfromDate().getValue() != null || form.getDbtoDate().getValue() != null
					|| form.getOlbBranch().getSelectedIndex() != 0)){
				form.showDialogMessage("Wrong filter selection ! Please select Branch with From Date and To Date filter Or select only Customer !");
			}else{
//			if((form.personInfoComposite.getValue()==null||form.personInfoComposite.getIdValue() == 0)&&(form.getDbfromDate().getValue() != null &&form.getDbtoDate().getValue() != null
//					&& form.getOlbBranch().getSelectedIndex() != 0)) {
				BilledList.clear();
				unBilledList.clear();
				Console.log("condition 1111 ");
//				if(form.tabId == 1) {
					reactOnGoForPayment(bStatusList);
//				} else {
					form.unBilledTable.getListDataProvider().getList().clear();
					reactOnGoForInvoice(iStatusList);
					reactOnGoForBilling(bdocStatusList);
//				}
//				form.hideWaitSymbol();
//			}
//					else {
//				Console.log("condition ## ");
//				form.showDialogMessage("Please select From Date and To Date with Branch filter");
//				
//			}
				
//				if((form.personInfoComposite.getValue()!=null ) && (form.getDbfromDate().getValue() == null && form.getDbtoDate().getValue() == null
//						&& form.getOlbBranch().getSelectedIndex() == 0)) {
//					BilledList.clear();
//					unBilledList.clear();
//					if(form.tabId == 1) {
//						reactOnGoForPayment(bStatusList);
//					} else {
//						form.unBilledTable.getListDataProvider().getList().clear();
//						reactOnGoForInvoice(iStatusList);
//						reactOnGoForBilling(bdocStatusList);
//					}
//				} else {
//					Console.log("condition 3333 ");
//					form.showDialogMessage("Wrong filter selection with From Date and To Date ! Please select branch or status filters with Date");
//				}
			}	
		}
	}
	/**
	 * Date: 25-01-2019
	 * Description: To add data in table payment,billed and invoice
	 */
	private void reactOnGoForPayment(List<String> list) {
		Console.log("reactOnGoForPayment 11");
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;
		MyQuerry querry;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(UserConfiguration.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("accountType");
		filter.setStringValue(AppConstants.BILLINGACCOUNTTYPEAR);
		filtervec.add(filter);
		
		if(form.getDbfromDate().getValue() != null) {
			filter = new Filter();
			filter.setQuerryString("paymentDate >=");
			filter.setDateValue(form.getDbfromDate().getValue());
			filtervec.add(filter);
		}
		
		if (form.getDbtoDate().getValue() != null) {
			filter = new Filter();
			filter.setQuerryString("paymentDate <=");
			filter.setDateValue(form.getDbtoDate().getValue());
			filtervec.add(filter);
		}
		
		if(form.getLbStatus().getSelectedIndex() != 0) {
			filter = new Filter();
			filter.setQuerryString("status");
			filter.setStringValue(form.getLbStatus().getValue(form.getLbStatus().getSelectedIndex()));
			filtervec.add(filter);
		} else {
			filter = new Filter();
			filter.setQuerryString("status IN");
			filter.setList(list);
			filtervec.add(filter);
		}
		
		if(form.getOlbBranch().getSelectedIndex() != 0) {
			filter = new Filter();
			filter.setQuerryString("branch");
			filter.setStringValue(form.olbBranch.getValue());
			filtervec.add(filter);
		}
		
		if(!form.getPersonInfoComposite().getFullNameValue().equals("")) {
			filter = new Filter();
			filter.setQuerryString("personInfo.fullName");
			filter.setStringValue(form.getPersonInfoComposite().getFullNameValue());
			filtervec.add(filter);
		}
		
		if(form.getPersonInfoComposite().getCellValue() != -1) {
			filter = new Filter();
			filter.setQuerryString("personInfo.cellNumber");
			filter.setLongValue(form.getPersonInfoComposite().getCellValue());
			filtervec.add(filter);
		}
		
		if(form.getPersonInfoComposite().getIdValue() != -1) {
			filter = new Filter();
			filter.setQuerryString("personInfo.count");
			filter.setIntValue(form.getPersonInfoComposite().getIdValue());
			filtervec.add(filter);
		}
		
		querry = new MyQuerry();
		querry.setQuerryObject(new CustomerPayment());
		querry.setFilters(filtervec);
//		form.showWaitSymbol();
		
		genasync.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {

			@Override
			public void onFailure(Throwable caught) {
				System.out.println("Error in Payment Data");
//				form.hideWaitSymbol();
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				Console.log("Inside Payment success11: " +result.size());
				int count = 0;
				double tAmountReceivable = 0;
				form.getTotalCount().setValue("");
				form.getTotalRecievableAmt().setValue("");
//				form.billedTable.getListDataProvider().getList().clear();
				
				if(result.size() != 0) {
					for(SuperModel model : result) {
						count += 1;
						CustomerPayment payment = (CustomerPayment) model;
						BillUnbilledAr billList = new BillUnbilledAr();
						tAmountReceivable += payment.getBalanceAmount();    //updated By: Viraj Date: 13-03-2019 Description: To show total of bal amt
						
						billList.setBranch(payment.getBranch());
						billList.setContractId(payment.getContractCount());
						billList.setCustomerCell(payment.getPersonInfo().getCellNumber());
						billList.setCustomerName(payment.getPersonInfo().getFullName());
						billList.setFollowUp(payment.getFollowUpDate());
						billList.setId(payment.getCount());
						billList.setInvoiceAmt(payment.getInvoiceAmount());
						billList.setSalesAmount(payment.getTotalSalesAmount());
						billList.setStatus(payment.getStatus());
						/**
						 * Updated By: Viraj
						 * Date: 09-03-2019
						 * Description: To show doc date and type
						 */
						billList.setDocType(AppConstants.PAYMENTDETAILS);
						billList.setDocDate(payment.getPaymentDate());
						/** Ends **/
						billList.setBalAmt(payment.getBalanceAmount());  			//Updated By: Viraj Date: 13-03-2019 Description:To show balance amt 
						BilledList.add(billList);
					}
				}
				form.billedTable.getTable().redraw();
				Console.log("Billed List Size: "+BilledList.size());
				if(BilledList.size() != 0) {
					form.billedTable.getListDataProvider().setList(BilledList);
				}
				form.getTotalCount1().setValue(count+"");
				form.getTotalRecievableAmt1().setValue(df.format(tAmountReceivable)+"");
//				form.hideWaitSymbol();
			}
		});
		
	}

	private void reactOnGoForInvoice(List<String> list) {
		
		Console.log("reactOnGoForInvoice");
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;
		MyQuerry querry;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(UserConfiguration.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("accountType");
		filter.setStringValue(AppConstants.BILLINGACCOUNTTYPEAR);
		filtervec.add(filter);
		
		if(form.getDbfromDate().getValue() != null) {
			filter = new Filter();
			filter.setQuerryString("invoiceDate >=");
			filter.setDateValue(form.getDbfromDate().getValue());
			filtervec.add(filter);
		}
		
		if (form.getDbtoDate().getValue() != null) {
			filter = new Filter();
			filter.setQuerryString("invoiceDate <=");
			filter.setDateValue(form.getDbtoDate().getValue());
			filtervec.add(filter);
		}
		
		if(form.getLbStatus().getSelectedIndex() != 0) {
			filter = new Filter();
			filter.setQuerryString("status");
			filter.setStringValue(form.getLbStatus().getValue(form.getLbStatus().getSelectedIndex()));
			filtervec.add(filter);
		} else {
			filter = new Filter();
			filter.setQuerryString("status IN");
			filter.setList(list);
			filtervec.add(filter);
		}
		
		if(form.getOlbBranch().getSelectedIndex() != 0) {
			filter = new Filter();
			filter.setQuerryString("branch");
			filter.setStringValue(form.olbBranch.getValue());
			filtervec.add(filter);
		}
		
		if(!form.getPersonInfoComposite().getFullNameValue().equals("")) {
			filter = new Filter();
			filter.setQuerryString("personInfo.fullName");
			filter.setStringValue(form.getPersonInfoComposite().getFullNameValue());
			filtervec.add(filter);
		}
		
		if(form.getPersonInfoComposite().getCellValue() != -1) {
			filter = new Filter();
			filter.setQuerryString("personInfo.cellNumber");
			filter.setLongValue(form.getPersonInfoComposite().getCellValue());
			filtervec.add(filter);
		}
		
		if(form.getPersonInfoComposite().getIdValue() != -1) {
			filter = new Filter();
			filter.setQuerryString("personInfo.count");
			filter.setIntValue(form.getPersonInfoComposite().getIdValue());
			filtervec.add(filter);
		}
		
		querry = new MyQuerry();
		querry.setQuerryObject(new Invoice());
		querry.setFilters(filtervec);
		
//		form.showWaitSymbol();
		genasync.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {

			@Override
			public void onFailure(Throwable caught) {
				System.out.println("Error in Invoice Data");
//				form.hideWaitSymbol();
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				System.out.println("Inside Invoice success: " +result.size());
				countInvoice = 0;
				tAmountReceivableInvoice = 0;
				
				if(result.size() != 0) {
					for(SuperModel model : result) {
						countInvoice += 1;
						Invoice invoice = (Invoice) model;
						BillUnbilledAr unbillList = new BillUnbilledAr();
						tAmountReceivableInvoice += invoice.getInvoiceAmount();
						
						unbillList.setBranch(invoice.getBranch());
						unbillList.setContractId(invoice.getContractCount());
						unbillList.setCustomerCell(invoice.getPersonInfo().getCellNumber());
						unbillList.setCustomerName(invoice.getPersonInfo().getFullName());
//						unbillList.setFollowUp(invoice.getInvoiceDate());
						unbillList.setId(invoice.getCount());
						unbillList.setInvoiceAmt(invoice.getInvoiceAmount());
						unbillList.setSalesAmount(invoice.getTotalSalesAmount());
						unbillList.setStatus(invoice.getStatus());
						/**
						 * Updated By: Viraj
						 * Date: 09-03-2019
						 * Description: To show doc date and type
						 */
						unbillList.setDocType(AppConstants.INVOICEDETAILS);
						unbillList.setDocDate(invoice.getInvoiceDate());
						/** Ends **/
						unBilledList.add(unbillList);
					}
					Console.log("InvoiceList Size: "+ result.size());
				}
//				form.hideWaitSymbol();
			}
		});
		
	}

	private void reactOnGoForBilling(List<String> list) {
		Console.log("reactOnGoForInvoice");
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;
		MyQuerry querry;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(UserConfiguration.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("accountType");
		filter.setStringValue(AppConstants.BILLINGACCOUNTTYPEAR);
		filtervec.add(filter);
		
		if(form.getDbfromDate().getValue() != null) {
			filter = new Filter();
			filter.setQuerryString("billingDocumentInfo.billingDate >=");
			filter.setDateValue(form.getDbfromDate().getValue());
			filtervec.add(filter);
		}
		
		if (form.getDbtoDate().getValue() != null) {
			filter = new Filter();
			filter.setQuerryString("billingDocumentInfo.billingDate <=");
			filter.setDateValue(form.getDbtoDate().getValue());
			filtervec.add(filter);
		}
		
		if(form.getLbStatus().getSelectedIndex() != 0) {
			filter = new Filter();
			filter.setQuerryString("status");
			filter.setStringValue(form.getLbStatus().getValue(form.getLbStatus().getSelectedIndex()));
			filtervec.add(filter);
		} else {
			filter = new Filter();
			filter.setQuerryString("status IN");
			filter.setList(list);
			filtervec.add(filter);
		}
		
		if(form.getOlbBranch().getSelectedIndex() != 0) {
			filter = new Filter();
			filter.setQuerryString("branch");
			filter.setStringValue(form.olbBranch.getValue());
			filtervec.add(filter);
		}
		
		if(!form.getPersonInfoComposite().getFullNameValue().equals("")) {
			filter = new Filter();
			filter.setQuerryString("personInfo.fullName");
			filter.setStringValue(form.getPersonInfoComposite().getFullNameValue());
			filtervec.add(filter);
		}
		
		if(form.getPersonInfoComposite().getCellValue() != -1) {
			filter = new Filter();
			filter.setQuerryString("personInfo.cellNumber");
			filter.setLongValue(form.getPersonInfoComposite().getCellValue());
			filtervec.add(filter);
		}
		
		if(form.getPersonInfoComposite().getIdValue() != -1) {
			filter = new Filter();
			filter.setQuerryString("personInfo.count");
			filter.setIntValue(form.getPersonInfoComposite().getIdValue());
			filtervec.add(filter);
		}
		
		querry = new MyQuerry();
		querry.setQuerryObject(new BillingDocument());
		querry.setFilters(filtervec);
//		form.showWaitSymbol();
		genasync.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {

			@Override
			public void onFailure(Throwable caught) {
				System.out.println("Error in Billing Data");
//				form.hideWaitSymbol();
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				System.out.println("Inside billing document success: " +result.size());
				countBilled = 0;
				tAmountReceivableBilled = 0;
				form.getTotalCount().setValue("");
				form.getTotalRecievableAmt().setValue("");
				
				if(result.size() != 0) {
					for(SuperModel model : result) {
						countBilled += 1;
						BillingDocument bc = (BillingDocument) model;
						BillUnbilledAr unbillList = new BillUnbilledAr();
						tAmountReceivableBilled += bc.getTotalBillingAmount();
						
						unbillList.setBranch(bc.getBranch());
						unbillList.setContractId(bc.getContractCount());
						unbillList.setCustomerCell(bc.getPersonInfo().getCellNumber());
						unbillList.setCustomerName(bc.getPersonInfo().getFullName());
//						unbillList.setFollowUp(bc.getBillingDate());
						unbillList.setId(bc.getCount());
						unbillList.setInvoiceAmt(bc.getTotalBillingAmount());
						unbillList.setSalesAmount(bc.getTotalSalesAmount());
						unbillList.setStatus(bc.getStatus());
						/**
						 * Updated By: Viraj
						 * Date: 09-03-2019
						 * Description: To show doc date and type
						 */
						unbillList.setDocType(AppConstants.BILLINGDETAILS);
						unbillList.setDocDate(bc.getBillingDate());
						/** Ends **/
						unBilledList.add(unbillList);
					}
				}
//				form.hideWaitSymbol();
				Console.log("unBilledList Size: "+unBilledList.size());
				Console.log("BillingList Size: "+result.size());
				if(unBilledList.size() != 0) {
					form.unBilledTable.getListDataProvider().setList(unBilledList);
				}
				totalCountunBilled = countInvoice + countBilled;
				tAmtReceivableunBilled = tAmountReceivableInvoice + tAmountReceivableBilled;
				form.unBilledTable.getTable().redraw();
				
				form.getTotalCount().setValue(totalCountunBilled+"");
				form.getTotalRecievableAmt().setValue(df.format(tAmtReceivableunBilled)+"");
			}
		});
		
	}
/** Ends **/
	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void makeNewModel() {
		// TODO Auto-generated method stub
		
	}

}
/** Ends **/