package com.slicktechnologies.client.views.account.dashboard;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TabPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.DummyEntityOnlyForScreen;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
/**
 * @author Viraj
 * Date: 24-01-2019
 * Description: To display billed and unbilled in tabular format
 */
public class ArDashboardForm extends FormScreen<DummyEntityOnlyForScreen> implements ClickHandler{

	PersonInfoComposite personInfoComposite;
    DateBox dbfromDate;
    DateBox dbtoDate;
    Button btgo;
    ObjectListBox<Branch> olbBranch;
    ListBox lbStatus;
    TabPanel tabPanel;
    int tabId;
    FlowPanel flowPanel1;
	FlowPanel flowPanel2;
    VerticalPanel vPanel1,vPanel2;
    HorizontalPanel hPanel1,hPanel2,hPanel3,hPanel4,hPanel5,hPanel6;
    TextBox totalCount;
    TextBox totalRecievableAmt;
    TextBox totalCount1;
    TextBox totalRecievableAmt1;
    BillUnbilledArTable billedTable;
    BillUnbilledArTable unBilledTable;
    Label label1,label2,label3,label4,label5,label6;
    /**
     * Updated By: Viraj
     * Date: 13-03-2019
     * Description: To add button to clear the data in search parameter
     */
    Button clear;
    /** Ends **/
    
    /**
     * Updated By: Viraj
     * Date: 19-03-2019
     * Description: To add download button for csv 
     */
    FlowPanel tableheader;
    String[] tableheaderbarnames ;
    String[] billHeader = new String[]{AppConstants.DOWNLOAD};
    String[] unbilledHeader = new String[]{AppConstants.DOWNLOAD};
    CsvServiceAsync csvservice=GWT.create(CsvService.class);
    /** Ends **/
    
    
    public ArDashboardForm() {
    	super();
		
//		tabPanel.addStyleName("gwt-TabPanelHeight");
        tabPanel.addSelectionHandler(new SelectionHandler<Integer>() {
       		@Override
       		public void onSelection(SelectionEvent<Integer> event) {
       			tabId = event.getSelectedItem();
       			System.out.println("TAB ID "+tabId); 
       			 	
       			 if(tabId==0) { 
       				 unBilledTable.getTable().redraw();
       				 unBilledTable.addColumnSorting();
       			 } else if(tabId==1) {
       				billedTable.getTable().redraw();
       				billedTable.addColumnSorting();
       			 }
       		}
       	});
        flowPanel1=new FlowPanel();
		flowPanel2=new FlowPanel();
		
        setHeaderMenu(unbilledHeader);
		flowPanel1.add(tableheader);
		setHeaderMenu(billHeader);
		flowPanel2.add(tableheader);
		
		hPanel5 = new HorizontalPanel();
		hPanel5.add(label4);
		
		hPanel1 = new HorizontalPanel();
		hPanel1.getElement().getStyle().setWidth(100, Unit.PCT);
		hPanel1.add(totalCount);
		hPanel1.add(label4);
		hPanel1.add(totalRecievableAmt);
		
		hPanel3 = new HorizontalPanel();
		hPanel3.getElement().getStyle().setWidth(100, Unit.PCT);
		hPanel3.add(label1);
		hPanel3.add(label3);
		hPanel3.add(label2);
		
		vPanel1 = new VerticalPanel();
		vPanel1.setWidth("100%");
		vPanel1.add(hPanel3);
		vPanel1.add(hPanel1);
//		vPanel2.add(hPanel5);
		vPanel1.add(unBilledTable.getTable());
		flowPanel1.add(vPanel1);
		
		hPanel2 = new HorizontalPanel();
		hPanel2.getElement().getStyle().setWidth(100, Unit.PCT);
		hPanel2.add(totalCount1);
		hPanel2.add(label4);
		hPanel2.add(totalRecievableAmt1);
		
		hPanel4 = new HorizontalPanel();
		hPanel4.getElement().getStyle().setWidth(100, Unit.PCT);
		hPanel4.add(label5);
		hPanel4.add(label3);
		hPanel4.add(label6);
		
		vPanel2 = new VerticalPanel();
		vPanel2.setWidth("100%");
		vPanel2.add(hPanel4);
		vPanel2.add(hPanel2);
//		vPanel2.add(hPanel5);
		vPanel2.add(billedTable.getTable());
		flowPanel2.add(vPanel2);
		
		
		tabPanel.add(flowPanel1 , "Unbilled");
		tabPanel.add(flowPanel2 , "Billed");
		tabPanel.setSize("100%", "500px");
		tabPanel.selectTab(0);
		
		createGui();
	}

	private void initalizeWidget(){
    	
    	personInfoComposite=AppUtility.customerInfoComposite(new Customer());
		personInfoComposite.getCustomerId().getHeaderLabel().setText("* Customer ID");
		personInfoComposite.getCustomerName().getHeaderLabel().setText("* Customer Name");
		personInfoComposite.getCustomerCell().getHeaderLabel().setText("* Customer Cell");
		personInfoComposite.getPocName().getHeaderLabel().setText("* Customer POC");
		
		olbBranch=new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbBranch);
		
		lbStatus=new ListBox();
		AppUtility.setStatusListBox(lbStatus, this.getStatusList());
		
//		dbfromDate = new DateBox();
		dbfromDate = new DateBoxWithYearSelector();

//		dbtoDate = new DateBox();
		dbtoDate = new DateBoxWithYearSelector();

		btgo = new Button("GO");
		
		totalCount = new TextBox();
		totalCount.setEnabled(false);
		
		totalRecievableAmt = new TextBox();
		totalRecievableAmt.setEnabled(false);
		
		totalCount1 = new TextBox();
		totalCount1.setEnabled(false);
		
		totalRecievableAmt1 = new TextBox();
		totalRecievableAmt1.setEnabled(false);
		
		label1 = new Label("Total No of Records");
		label1.setWidth("180px");
		label2 = new Label("Total Unbilled Amount Receivable");		//updated by:Viraj Date:13-03-2019 Description:Changed Amt to Amount 
		label5 = new Label("Total No of Records");
		label5.setWidth("180px");
		label6 = new Label("Total Billed Amount Receivable");		//updated by:Viraj Date:13-03-2019 Description:Changed Amt to Amount
		label3 = new Label();
		label3.setSize("40px", "18px");
		label4 = new Label();
		label4.setSize("48px", "30px");
		
		clear = new Button("Clear");	//updated by:Viraj Date:13-03-2019 Description:To add button to clear the data in search parameter
    }

	@Override
	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				menus[k].setVisible(false);
			}
		}
		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				menus[k].setVisible(false);
			}
		}
		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				menus[k].setVisible(false);
			}
		}

		
	}

	@Override
	public void createScreen() {
		
		initalizeWidget();
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		tabPanel = new TabPanel();
		billedTable = new BillUnbilledArTable(true);
		unBilledTable = new BillUnbilledArTable();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingFilter = fbuilder.setlabel("Filter").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(7).build();
		
		fbuilder = new FormFieldBuilder("",personInfoComposite);
		FormField fpersonInfoComposite = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(6).build();
		
		fbuilder = new FormFieldBuilder("Branch",olbBranch);
		FormField folbBranch = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Status",lbStatus);
		FormField flbStatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("From Date", dbfromDate );
		FormField fdbfromDate = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("To Date", dbtoDate);
		FormField fdbtoDate = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",btgo);
		FormField fbtgo = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",clear);
		FormField fbtClear = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",tabPanel);
		FormField ftabPanel = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(7).build();
		
		FormField[][] formfield = {
				{fgroupingFilter},
				{fpersonInfoComposite,fbtClear},
				{folbBranch,fdbfromDate,fdbtoDate,fbtgo},
				{ftabPanel}
			
		};
		this.fields=formfield;
	}

	@Override
	public void updateModel(DummyEntityOnlyForScreen model) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		
		this.totalCount.setEnabled(false);
		this.totalCount1.setEnabled(false);
		this.totalRecievableAmt.setEnabled(false);
		this.totalRecievableAmt1.setEnabled(false);
		
	}
	
	public List<String> getStatusList() {
		
		ArrayList<String> statuslist=new ArrayList<String>();
		statuslist.add(BillingDocument.CREATED);
		statuslist.add(BillingDocument.REQUESTED);
		statuslist.add(BillingDocument.REJECTED);
		statuslist.add(BillingDocument.APPROVED);
		statuslist.add(BillingDocument.CANCELLED);
		statuslist.add(BillingDocument.BILLINGINVOICED);
		statuslist.add(BillingDocument.PROFORMAINVOICE);
		statuslist.add(BillingDocument.CLOSED);
		statuslist.add(BillingDocument.PAYMENTCLOSED);
		statuslist.add(BillingDocument.PROCESSED);
		
		return statuslist;
	}
	
	@Override
	public void updateView(DummyEntityOnlyForScreen model) {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * 
	 */
	private void setHeaderMenu(String[] str){
		tableheaderbarnames= str;
		tableheader= new FlowPanel();
		tableheader.getElement().setId("tableheader");
		setTableHeaderBar();	
		tableheader.getElement().getStyle().setMarginTop(10, Unit.PX);
		tableheader.getElement().getStyle().setMarginBottom(10, Unit.PX);
	}
	protected void setTableHeaderBar() {
		// TODO Auto-generated method stub
		
		for(int j=0;j<getTableheaderbarnames().length;j++)
		{
		    InlineLabel lbl;
			lbl=new InlineLabel(getTableheaderbarnames()[j]);
            getTableheader().add(lbl);
			lbl.getElement().setId("tableheadermenu");
			lbl.addClickHandler(this);
		}
		
	}
	
	public FlowPanel getTableheader() {
		return tableheader;
	}

	public void setTableheader(FlowPanel tableheader) {
		this.tableheader = tableheader;
	}

	public String[] getTableheaderbarnames() {
		return tableheaderbarnames;
	}

	public void setTableheaderbarnames(String[] tableheaderbarnames) {
		this.tableheaderbarnames = tableheaderbarnames;
	}
	
	public String[] getBillHeader() {
		return billHeader;
	}

	public void setBillHeader(String[] billHeader) {
		this.billHeader = billHeader;
	}

	public String[] getUnbilledHeader() {
		return unbilledHeader;
	}

	public void setUnbilledHeader(String[] unbilledHeader) {
		this.unbilledHeader = unbilledHeader;
	}
	/** Ends **/

	/**
	 * Updated By: Viraj
	 * Date: 13-03-2019
	 * Description: For clearing all the paramters of search
	 */
	public Button getClear() {
		return clear;
	}

	public void setClear(Button clear) {
		this.clear = clear;
	}
	/** Ends **/
	public TextBox getTotalCount1() {
		return totalCount1;
	}

	public void setTotalCount1(TextBox totalCount1) {
		this.totalCount1 = totalCount1;
	}

	public TextBox getTotalRecievableAmt1() {
		return totalRecievableAmt1;
	}

	public void setTotalRecievableAmt1(TextBox totalRecievableAmt1) {
		this.totalRecievableAmt1 = totalRecievableAmt1;
	}

	public TextBox getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(TextBox totalCount) {
		this.totalCount = totalCount;
	}

	public TextBox getTotalRecievableAmt() {
		return totalRecievableAmt;
	}

	public void setTotalRecievableAmt(TextBox totalRecievableAmt) {
		this.totalRecievableAmt = totalRecievableAmt;
	}

	public DateBox getDbfromDate() {
		return dbfromDate;
	}

	public void setDbfromDate(DateBox dbfromDate) {
		this.dbfromDate = dbfromDate;
	}

	public DateBox getDbtoDate() {
		return dbtoDate;
	}

	public void setDbtoDate(DateBox dbtoDate) {
		this.dbtoDate = dbtoDate;
	}

	public Button getBtgo() {
		return btgo;
	}

	public void setBtgo(Button btgo) {
		this.btgo = btgo;
	}

	public ObjectListBox<Branch> getOlbBranch() {
		return olbBranch;
	}

	public void setOlbBranch(ObjectListBox<Branch> olbBranch) {
		this.olbBranch = olbBranch;
	}

	public ListBox getLbStatus() {
		return lbStatus;
	}

	public void setLbStatus(ListBox lbStatus) {
		this.lbStatus = lbStatus;
	}

	public PersonInfoComposite getPersonInfoComposite() {
		return personInfoComposite;
	}

	public void setPersonInfoComposite(PersonInfoComposite personInfoComposite) {
		this.personInfoComposite = personInfoComposite;
	}

	@Override
	public void onClick(ClickEvent event) {
		/**
	     * Updated By: Viraj
	     * Date: 22-03-2019
	     * Description: To add download button for csv 
	     */
		ArDashboardPresenter arPresenter = (ArDashboardPresenter) presenter;
		
		if(event.getSource() instanceof InlineLabel){
			InlineLabel lbl=(InlineLabel) event.getSource();
			String title=lbl.getText().trim();
			
			if(title.equals(AppConstants.DOWNLOAD)){
				
				if(tabId == 0){
					csvservice.setBilledUnbilledList(arPresenter.unBilledList, new AsyncCallback<Void>() {

						@Override
						public void onFailure(Throwable caught) {
							System.out.println("RPC call Failed"+caught);
						}
						@Override
						public void onSuccess(Void result) {
							
							String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
							final String url=gwt + "csvservlet"+"?type="+168;
							Window.open(url, "test", "enabled");
						}
					});
					
				}else if(tabId ==1){
					
					csvservice.setBilledUnbilledList(arPresenter.BilledList, new AsyncCallback<Void>() {

						@Override
						public void onFailure(Throwable caught) {
							System.out.println("RPC call Failed"+caught);
						}
						@Override
						public void onSuccess(Void result) {
							
							String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
							final String url=gwt + "csvservlet"+"?type="+168;
							Window.open(url, "test", "enabled");
						}
					});
				}
			}
		}
		/** Ends **/
	}
	
	

}
/** Ends **/