package com.slicktechnologies.client.views.account.glaccount;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.GLAccount;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class GLAccountForm extends FormTableScreen<GLAccount> implements ClickHandler {

	//Token to add the varialble declarations
		IntegerBox ibglaccountid;
		IntegerBox ibglaccountno;
		TextBox tbglaccountname;
		TextArea taglaccountdescription;
		CheckBox cbstatus;
		ObjectListBox<Config> olbglaccountgroup;
	
		public GLAccountForm(SuperTable<GLAccount> table, int mode,
				boolean captionmode) {
			super(table, mode, captionmode);
			createGui();
		}

		private void initalizeWidget()
		{
			ibglaccountid=new IntegerBox();
			ibglaccountid.setEnabled(false);
			ibglaccountno=new IntegerBox();
			tbglaccountname=new TextBox();
			taglaccountdescription=new TextArea();
			olbglaccountgroup=new ObjectListBox<Config>();
			AppUtility.MakeLiveConfig(olbglaccountgroup, Screen.GLACCOUNTGROUP);
			cbstatus=new CheckBox();
			cbstatus.setValue(true);
		}

		/*
		 * Method template to create the formtable screen
		 */

		@Override
		public void createScreen() {

			//Token to initialize the processlevel menus.
			initalizeWidget();

			//////////////////////////////////Form Field Declaration/////////////////////////////////////////////////

			//////////////////////////// Form Field Initialization////////////////////////////////////////////////////

			//Token to initialize formfield
			FormFieldBuilder fbuilder;
			fbuilder = new FormFieldBuilder();
			FormField fgroupingGLAccountInformation=fbuilder.setlabel("GL Account Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
			fbuilder = new FormFieldBuilder("GL Account ID",ibglaccountid);
			FormField fibglaccountid= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("GL Account No",ibglaccountno);
			FormField fibglaccountno= fbuilder.setMandatory(true).setMandatoryMsg("GL Account no is mandatory").setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("GL Account Name",tbglaccountname);
			FormField ftbglaccountname= fbuilder.setMandatory(true).setMandatoryMsg("GL Account Name is mandatory").setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("* GL Account Group",olbglaccountgroup);
			FormField folbglaccountgroup=fbuilder.setMandatory(true).setMandatoryMsg("GL Account Group is mandatory").setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("Description",taglaccountdescription);
			FormField ftadescription=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(5).build();
			fbuilder = new FormFieldBuilder("Status",cbstatus);
			FormField fcbstatus=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();


			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

			////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////


			FormField[][] formfield = {   {fgroupingGLAccountInformation},
					{fibglaccountid,fibglaccountno,ftbglaccountname,folbglaccountgroup,fcbstatus},
					{ftadescription},
			};


			this.fields=formfield;		
		}

		
		/**
		 * method template to update the model with token entity name
		 */
		@Override
		public void updateModel(GLAccount model) 
		{
			model.setGlAccountNo(ibglaccountno.getValue());
			if(tbglaccountname.getValue()!=null)
				model.setGlAccountName(tbglaccountname.getValue());
			if(olbglaccountgroup.getValue()!=null)
				model.setGlAccountGroup(olbglaccountgroup.getValue());
			if(taglaccountdescription.getValue()!=null)
				model.setDescription(taglaccountdescription.getValue());
		
			if(cbstatus.getValue()!=null)
			{
				if(cbstatus.getValue()==true)
					model.setStatus("Active");
				else
					model.setStatus("Inactive");
			}
			

			presenter.setModel(model);
		}

		/**
		 * method template to update the view with token entity name
		 */
		@Override
		public void updateView(GLAccount view) 
		{
			ibglaccountno.setValue(view.getGlAccountNo());
			if(view.getGlAccountName()!=null)
				tbglaccountname.setValue(view.getGlAccountName());
			if(view.getGlAccountGroup()!=null)
				olbglaccountgroup.setValue(view.getGlAccountGroup());
			if(view.getDescription()!=null)
				taglaccountdescription.setValue(view.getDescription());
			if(view.getStatus()!=null)
			{
				if(view.getStatus()=="Active")
					cbstatus.setValue(true);
				else
					cbstatus.setValue(false);
					
			}
			
			presenter.setModel(view);

		}

		/**
		 * Toggles the app header bar menus as per screen state
		 */

		public void toggleAppHeaderBarMenu() {
			if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.equals("Save")||text.equals("Discard"))
					{
						menus[k].setVisible(true); 
					}
					else
						menus[k].setVisible(false);  		   

				}
			}

			else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.equals("Save")||text.equals("Discard"))
						menus[k].setVisible(true); 

					else
						menus[k].setVisible(false);  		   
				}
			}

			else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.equals("Discard")||text.equals("Edit"))
						menus[k].setVisible(true); 

					else
						menus[k].setVisible(false);  		   
				}
			}
			AuthorizationHelper.setAsPerAuthorization(Screen.GLACCOUNT,LoginPresenter.currentModule.trim());
		}

		/**
		 * sets the id textbox with the passed count value. 
		 */
		@Override
		public void setCount(int count)
		{
			this.ibglaccountid.setValue(presenter.getModel().getCount());
		}

		
		@Override
		public void setEnable(boolean state) {
			// TODO Auto-generated method stub
			super.setEnable(state);
			ibglaccountid.setEnabled(false);
			cbstatus.setValue(true);
		}

		
	
	
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
	}

}
