package com.slicktechnologies.client.views.account.glaccount;

import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.GLAccount;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;


public class GLAccountPresenter extends FormTableScreenPresenter<GLAccount>{
	
	//Token to set the concrete form
		GLAccountForm form;
		
		public GLAccountPresenter (FormTableScreen<GLAccount> view, GLAccount model) {
			super(view, model);
			form=(GLAccountForm) view;
			form.getSupertable().connectToLocal();
			form.retriveTable(getGLAccountQuery());
			form.setPresenter(this);
			
			boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.GLACCOUNT,LoginPresenter.currentModule.trim());
			if(isDownload==false){
				form.getSearchpopupscreen().getDwnload().setVisible(false);
			}
		}

		/**
		 * Method template to set the processBar events
		 */
		@Override
		public void reactToProcessBarEvents(ClickEvent e) 
	   {
			InlineLabel label=(InlineLabel) e.getSource();
			String text=label.getText().trim();
			
			if(text.equals(""))
				  reactTo();

			
		}
		
		

		@Override
		public void reactOnPrint() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void reactOnEmail() {
			// TODO Auto-generated method stub
			
		}

		/**
		 * Method template to set new model
		 */
		@Override
		protected void makeNewModel() {
			
			model=new GLAccount();
		}
		
		/*
		 * Method template to set Myquerry object
		 */
		public MyQuerry getGLAccountQuery()
		{
			MyQuerry quer=new MyQuerry(new Vector<Filter>(), new GLAccount());
			return quer;
		}
		
		public void setModel(GLAccount entity)
		{
			model=entity;
		}
		
		public static void initalize()
		{
			GLAccountPresenterTable gentableScreen=GWT.create( GLAccountPresenterTable.class);
				
			GLAccountForm  form=new  GLAccountForm(gentableScreen,FormTableScreen.UPPER_MODE,true);
				gentableScreen.setView(form);
				gentableScreen.applySelectionModle();
				
				  //// CountryPresenterTable gentableSearch=GWT.create( CountryPresenterTable.class);
				  ////   CountryPresenterSearch.staticSuperTable=gentableSearch;
				  ////     CountryPresenterSearch searchpopup=GWT.create( CountryPresenterSearch.class);
				  ////         form.setSearchpopupscreen(searchpopup);
				
				GLAccountPresenter  presenter=new  GLAccountPresenter  (form,new GLAccount());
				AppMemory.getAppMemory().stickPnel(form);
				
				
			 
		}
		
		
			@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.GLAccount")
			 public static class  GLAccountPresenterTable extends SuperTable<GLAccount> implements GeneratedVariableRefrence{

				@Override
				public Object getVarRef(String varName) {
					// TODO Auto-generated method stub
					return null;
				}

				@Override
				public void createTable() {
					// TODO Auto-generated method stub
					
				}

				@Override
				protected void initializekeyprovider() {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void addFieldUpdater() {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void setEnable(boolean state) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void applyStyle() {
					// TODO Auto-generated method stub
					
				}} ;
				
				@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.GLAccount")
				 public static class  GLAccountPresenterSearch extends SearchPopUpScreen<  GLAccount>{

					@Override
					public MyQuerry getQuerry() {
						// TODO Auto-generated method stub
						return null;
					}

					@Override
					public boolean validate() {
						// TODO Auto-generated method stub
						return true;
					}};
					
					  private void reactTo()
					  {
					}



}
