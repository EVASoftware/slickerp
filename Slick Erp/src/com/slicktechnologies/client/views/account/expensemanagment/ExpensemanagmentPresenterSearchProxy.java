package com.slicktechnologies.client.views.account.expensemanagment;

import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SuperTable;

import java.util.ArrayList;
import java.util.Vector;

import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.shared.*;
import com.slicktechnologies.shared.common.*;
import com.slicktechnologies.shared.common.businessprocesslayer.*;
import com.slicktechnologies.shared.common.businessunitlayer.*;
import com.slicktechnologies.shared.common.helperlayer.*;
import com.slicktechnologies.shared.common.personlayer.*;
import com.slicktechnologies.shared.common.productlayer.*;
import com.slicktechnologies.shared.common.businessprocesslayer.Expense;
import com.slicktechnologies.client.views.account.expensemanagment.ExpensemanagmentPresenter.ExpensemanagmentPresenterSearch;
import com.simplesoftwares.client.library.composite.CommonSearchComposite;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.slicktechnologies.client.config.CategoryTypeFactory;
import com.slicktechnologies.client.utility.AppUtility;
import com.google.gwt.user.client.ui.CheckBox;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utility.AppConstants;

public class ExpensemanagmentPresenterSearchProxy extends ExpensemanagmentPresenterSearch {
	public TextBox tbInvoiceNumber;
	public DateComparator dateBoxExpenseDate;
	public ObjectListBox<Branch> olbBranch;
	public ObjectListBox<Employee> olbEmployee;
	public DateComparator dateComparator;
	public ObjectListBox<ConfigCategory> olbExpenseCategory;
	public ObjectListBox<Config>olbExpenseGroup,olbPaymentMethod;
	public ObjectListBox<Type> olbConfigExpenseType;
	public ListBox lbStatus;
	public TextBox tbvendor;
	public static CheckBox cbexptypepettycash;
	public ObjectListBox<PettyCash> olbpetycash;


	public Object getVarRef(String varName)
	{
		if(varName.equals("tbInvoiceNumber"))
			return this.tbInvoiceNumber;

		if(varName.equals("dateBoxExpenseDate"))
			return this.dateBoxExpenseDate;
		if(varName.equals("olbBranch"))
			return this.olbBranch;
		if(varName.equals("olbEmployee"))
			return this.olbEmployee;
		if(varName.equals("dateComparator"))
			return this.dateComparator;
		 if(varName.equals("olbConfigExpenseType"))
			 return this.olbConfigExpenseType;
		 if(varName.equals("olbExpenseCategory"))
			 return this.olbExpenseCategory;
			 
		 
		return null ;
	}
	public ExpensemanagmentPresenterSearchProxy()
	{
		super();
		createGui();
	}
	public void initWidget()
	{
		tbInvoiceNumber= new TextBox();
		tbvendor=new TextBox();
		dateBoxExpenseDate= new DateComparator("expenseDate",new Expense());
		olbBranch= new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbBranch);
		olbEmployee= new ObjectListBox<Employee>();
		AppUtility.makeSalesPersonListBoxLive(olbEmployee);
		dateComparator= new DateComparator("expenseDate",new Expense());
		olbExpenseGroup=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbExpenseGroup,Screen.EXPENSEGROUP);
        olbExpenseCategory= new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(olbExpenseCategory,Screen.EXPENSECATEGORY);
		olbConfigExpenseType=new ObjectListBox<Type>();
		AppUtility.makeTypeListBoxLive(olbConfigExpenseType, Screen.EXPENSETYPE);
		CategoryTypeFactory.initiateOneManyFunctionality(olbExpenseCategory,olbConfigExpenseType);
		lbStatus= new ListBox();
		AppUtility.setStatusListBox(lbStatus,Expense.getStatusList());
		olbPaymentMethod=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbPaymentMethod, Screen.PAYMENTMETHODS);
		cbexptypepettycash=new CheckBox();
		olbpetycash=new ObjectListBox<PettyCash>();
		olbpetycash.MakeLive(new MyQuerry(new Vector<Filter>(),new PettyCash()));

	}
	public void createScreen()
	{
		initWidget();
		FormFieldBuilder builder;
		builder = new FormFieldBuilder("Invoice Number",tbInvoiceNumber);
		FormField ftbInvoiceNumber= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Vendor",tbvendor);
		FormField ftbvendor= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("From Date (Expense Date)",dateBoxExpenseDate.getFromDate());
		FormField fdateBoxExpenseDate= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("To Date (Expense Date)",dateBoxExpenseDate.getToDate());
		FormField ftodateBoxExpenseDate= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Branch",olbBranch);
		FormField folbBranch= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Employee",olbEmployee);
		FormField folbEmployee= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		//builder = new FormFieldBuilder("From Date (Creation Date)",dateComparator.getFromDate());
		//FormField ffromdateComparator= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		//builder = new FormFieldBuilder("To Date(Creation Date)",dateComparator.getToDate());
		//FormField ftodateComparator= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Expense Group",olbExpenseGroup);
		FormField fgroup= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Expense Category",olbExpenseCategory);
		FormField folbExpenseCategory= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Expense Type",olbConfigExpenseType);
		FormField folbConfigExpenseType= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Expense Status",lbStatus);
		FormField flbststus= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Payment Method",olbPaymentMethod);
		FormField folbpaymentmethod= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Is expense type a pettycash",cbexptypepettycash);
		FormField fcbexptypepettycash= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Petty Cash Name",olbpetycash);
		FormField folbpettycashname= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		  
		this.fields=new FormField[][]{
				{fdateBoxExpenseDate,ftodateBoxExpenseDate},
				{fgroup,folbExpenseCategory,folbConfigExpenseType,flbststus},
				{folbEmployee,folbBranch,ftbInvoiceNumber,folbpaymentmethod},
				{ftbvendor,fcbexptypepettycash,folbpettycashname}
		};
	}
	public MyQuerry getQuerry()
	{
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		if(dateComparator.getValue()!=null)
		{
			filtervec.addAll(dateComparator.getValue());
		}
		if(dateBoxExpenseDate.getValue()!=null){
			filtervec.addAll(dateBoxExpenseDate.getValue());
		}
		if(olbEmployee.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(olbEmployee.getValue().trim());
			temp.setQuerryString("employee");
			filtervec.add(temp);
		}
		if(olbBranch.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbBranch.getValue().trim());
			temp.setQuerryString("branch");
			filtervec.add(temp);
		}
		if(olbExpenseCategory.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbExpenseCategory.getValue().trim());
			temp.setQuerryString("category");
			filtervec.add(temp);
		}
		
		if(olbExpenseGroup.getSelectedIndex()!=0)
		{
			 temp=new Filter();temp.setStringValue(olbExpenseGroup.getValue().trim());
			  temp.setQuerryString("group");
			  filtervec.add(temp);
		}
		
		if(olbConfigExpenseType.getSelectedIndex()!=0)
		{
			temp=new Filter();temp.setStringValue(olbConfigExpenseType.getValue().trim());
			  temp.setQuerryString("type");
			  filtervec.add(temp);
		}
		if(tbInvoiceNumber.getValue().trim().equals("")==false){
			temp=new Filter();temp.setStringValue(tbInvoiceNumber.getValue().trim());
			filtervec.add(temp);
			temp.setQuerryString("invoiceNumber");
			filtervec.add(temp);
		}
		
		if(olbPaymentMethod.getSelectedIndex()!=0)
		{
			temp=new Filter();temp.setStringValue(olbPaymentMethod.getValue().trim());
			  temp.setQuerryString("paymentMethod");
			  filtervec.add(temp);
		}
		
		if(tbvendor.getValue().trim().equals("")==false){
			temp=new Filter();
			temp.setStringValue(tbvendor.getValue().trim());
			filtervec.add(temp);
			temp.setQuerryString("vendor");
			filtervec.add(temp);
		}
		if(cbexptypepettycash.getValue()==true)
		{
			temp=new Filter();
			temp.setBooleanvalue(cbexptypepettycash.getValue());
			filtervec.add(temp);
			temp.setQuerryString("expTypeAsPettyCash");
			filtervec.add(temp);
		}
		if(olbpetycash.getSelectedIndex()!=0)
		{
			temp=new Filter();temp.setStringValue(olbpetycash.getValue().trim());
			  temp.setQuerryString("pettyCashName");
			  filtervec.add(temp);
		}
		
		if(lbStatus.getSelectedIndex()!=0){
			temp=new Filter();
			int selValue=lbStatus.getSelectedIndex();
			String selectedVal=lbStatus.getItemText(selValue);
			temp.setStringValue(selectedVal);
			filtervec.add(temp);
			temp.setQuerryString("status");
			filtervec.add(temp);
		}
		
		MyQuerry querry= new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Expense());
		return querry;
	}
	
	
	
}
