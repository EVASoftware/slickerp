package com.slicktechnologies.client.views.account.expensemanagment;

import java.util.Vector;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.PettyCash;

public class PopupForPettyCashName extends AbsolutePanel {
	
	ObjectListBox<PettyCash> oblPettycashName;
	Button btnCancel, btnOk;
	
	
	public PopupForPettyCashName()
	{
		btnCancel = new Button("Cancel");
		btnOk = new Button("Ok");
		
		
		InlineLabel remark1 = new InlineLabel("Select Pettycash Name");
		add(remark1, 60, 10);
		remark1.getElement().getStyle().setFontSize(20, Unit.PX);

		
		oblPettycashName=new ObjectListBox<PettyCash>();
		oblPettycashName.MakeLive(new MyQuerry(new Vector<Filter>(), new PettyCash()));
		
		add(oblPettycashName, 80, 100);
		
		
		add(btnOk, 80, 280);
		add(btnCancel, 180, 280);
		setSize("420px", "320px");
		this.getElement().setId("form");
	}


	public ObjectListBox<PettyCash> getOblPettycashName() {
		return oblPettycashName;
	}


	public void setOblPettycashName(ObjectListBox<PettyCash> oblPettycashName) {
		this.oblPettycashName = oblPettycashName;
	}


	public Button getBtnCancel() {
		return btnCancel;
	}


	public void setBtnCancel(Button btnCancel) {
		this.btnCancel = btnCancel;
	}


	public Button getBtnOk() {
		return btnOk;
	}


	public void setBtnOk(Button btnOk) {
		this.btnOk = btnOk;
	}



}
