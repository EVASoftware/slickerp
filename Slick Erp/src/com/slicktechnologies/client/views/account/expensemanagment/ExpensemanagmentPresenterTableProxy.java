package com.slicktechnologies.client.views.account.expensemanagment;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;

import java.util.Comparator;

import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.google.gwt.cell.client.FieldUpdater;

import java.util.List;
import java.util.Date;

import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.cell.client.EditTextCell;
import  com.google.gwt.cell.client.NumberCell;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import  com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.dom.client.Style.Unit;
import com.slicktechnologies.shared.common.businessprocesslayer.Expense;
import com.slicktechnologies.client.views.account.expensemanagment.ExpensemanagmentPresenter.ExpensemanagmentPresenterTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;

public class ExpensemanagmentPresenterTableProxy extends ExpensemanagmentPresenterTable {
//MulipleExpenseMngt
	TextColumn<Expense> getInvoiceNumberColumn;
	TextColumn<Expense> getAmountColumn;
	TextColumn<Expense> getVendorColumn;
	TextColumn<Expense> getExpenseDateColumn;
	TextColumn<Expense> getBranchColumn;
	TextColumn<Expense> getEmployeeColumn;
	TextColumn<Expense> getStatusColumn;
	TextColumn<Expense> getCreationDateColumn;
	TextColumn<Expense> getGroupColumn;
	TextColumn<Expense> getCategoryColumn;
	TextColumn<Expense> getTypeColumn;
	TextColumn<Expense> getCountColumn;
	TextColumn<Expense> getPettyCashName ;
	public Object getVarRef(String varName)
	{
		if(varName.equals("getInvoiceNumberColumn"))
			return this.getInvoiceNumberColumn;
		if(varName.equals("getAmountColumn"))
			return this.getAmountColumn;
		if(varName.equals("getVendorColumn"))
			return this.getVendorColumn;
		if(varName.equals("getExpenseDateColumn"))
			return this.getExpenseDateColumn;
		if(varName.equals("getBranchColumn"))
			return this.getBranchColumn;
		if(varName.equals("getEmployeeColumn"))
			return this.getEmployeeColumn;
		if(varName.equals("getStatusColumn"))
			return this.getStatusColumn;
		if(varName.equals("getCreationDateColumn"))
			return this.getCreationDateColumn;
		if(varName.equals("getGroupColumn"))
			return this.getGroupColumn;
		if(varName.equals("getCategoryColumn"))
			return this.getCategoryColumn;
		if(varName.equals("getTypeColumn"))
			return this.getTypeColumn;
		if(varName.equals("getCountColumn"))
			return this.getCountColumn;
		return null ;
	}
	public ExpensemanagmentPresenterTableProxy()
	{
		super();
	}
	@Override 
	public void createTable() {
		addColumngetCount();
		addColumngetInvoiceNumber();
		addColumngetPettyCashName();
		addColumngetExpenseDate();
		addColumngetAmount();
		addColumngetBranch();
		addColumngetEmployee();
		addColumngetGroup();
		addColumngetCategory();
		addColumngetType();
		addColumngetVendor();
		addColumngetCreationDate();
		addColumngetStatus();
		
	}

	private void addColumngetPettyCashName() {
		getPettyCashName=new TextColumn<Expense>()
				{
			@Override
			public String getValue(Expense object)
			{
				if(object.getPettyCashName()!=null)
				{
				return object.getPettyCashName();
				}
				else
				{
					return "NA";
				}
			}
				};
				table.addColumn(getPettyCashName,"Pettycash Name");
				table.setColumnWidth(getPettyCashName,120,Unit.PX);
				getPettyCashName.setSortable(true);
	}
	
	@Override
	protected void initializekeyprovider() {
		keyProvider= new ProvidesKey<Expense>()
				{
			@Override
			public Object getKey(Expense item)
			{
				if(item==null)
				{
					return null;
				}
				else
					return item.getId();
			}
				};
	}
	@Override
	public void setEnable(boolean state)
	{
	}
	@Override
	public void applyStyle()
	{
	}
	public void addColumnSorting(){
		addSortinggetCount();
		addSortinggetBranch();
		addSortinggetEmployee();
		addSortinggetStatus();
		addSortinggetGroup();
		addSortinggetCategory();
		addSortinggetType();
		addSortinggetVendor();
		addSortinggetInvoiceNumber();
		addSortinggetAmount();
		addSortinggetExpenseDate();
		addSortinggetcreationDate();
		addSortinggetPettyCashName();
	}
	
	private void addSortinggetPettyCashName() {
		
		List<Expense> list=getDataprovider().getList();
		columnSort=new ListHandler<Expense>(list);
		columnSort.setComparator(getPettyCashName, new Comparator<Expense>()
				{
			@Override
			public int compare(Expense e1,Expense e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getPettyCashName()!=null && e2.getPettyCashName()!=null){
						return e1.getPettyCashName().compareTo(e2.getPettyCashName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	@Override public void addFieldUpdater() {
	}
	protected void addSortinggetCount()
	{
		List<Expense> list=getDataprovider().getList();
		columnSort=new ListHandler<Expense>(list);
		columnSort.setComparator(getCountColumn, new Comparator<Expense>()
				{
			@Override
			public int compare(Expense e1,Expense e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetCount()
	{
		getCountColumn=new TextColumn<Expense>()
				{
			@Override
			public String getValue(Expense object)
			{
				if( object.getCount()==-1)
					return "N.A";
				else return object.getCount()+"";
			}
				};
				table.addColumn(getCountColumn,"ID");
				table.setColumnWidth(getCountColumn,100,Unit.PX);
				getCountColumn.setSortable(true);
	}
	protected void addColumngetCreationDate()
	{
		getCreationDateColumn=new TextColumn<Expense>()
				{
			@Override
			public String getValue(Expense object)
			{
				if(object.getCreationDate()!=null){
					return AppUtility.parseDate(object.getCreationDate())+"";
					}
				else{
					
					return "N.A";
				}
			}
				};
				table.addColumn(getCreationDateColumn,"Creation Date");
				table.setColumnWidth(getCreationDateColumn,110,Unit.PX);
				getCreationDateColumn.setSortable(true);
	}
	
	protected void addSortinggetBranch()
	{
		List<Expense> list=getDataprovider().getList();
		columnSort=new ListHandler<Expense>(list);
		columnSort.setComparator(getBranchColumn, new Comparator<Expense>()
				{
			@Override
			public int compare(Expense e1,Expense e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBranch()!=null && e2.getBranch()!=null){
						return e1.getBranch().compareTo(e2.getBranch());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetcreationDate()
	{
		List<Expense> list=getDataprovider().getList();
		columnSort=new ListHandler<Expense>(list);
		columnSort.setComparator(getCreationDateColumn, new Comparator<Expense>()
				{
			@Override
			public int compare(Expense e1,Expense e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getCreationDate()!=null && e2.getCreationDate()!=null){
						return e1.getCreationDate().compareTo(e2.getCreationDate());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetBranch()
	{
		getBranchColumn=new TextColumn<Expense>()
				{
			@Override
			public String getValue(Expense object)
			{
				return object.getBranch()+"";
			}
				};
				table.addColumn(getBranchColumn,"Branch");
				table.setColumnWidth(getBranchColumn,100,Unit.PX);
				getBranchColumn.setSortable(true);
	}
	protected void addSortinggetEmployee()
	{
		List<Expense> list=getDataprovider().getList();
		columnSort=new ListHandler<Expense>(list);
		columnSort.setComparator(getEmployeeColumn, new Comparator<Expense>()
				{
			@Override
			public int compare(Expense e1,Expense e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getEmployee()!=null && e2.getEmployee()!=null){
						return e1.getEmployee().compareTo(e2.getEmployee());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetEmployee()
	{
		getEmployeeColumn=new TextColumn<Expense>()
				{
			@Override
			public String getValue(Expense object)
			{
				return object.getEmployee()+"";
			}
				};
				table.addColumn(getEmployeeColumn,"Employee");
				table.setColumnWidth(getEmployeeColumn,120,Unit.PX);
				getEmployeeColumn.setSortable(true);
	}
	protected void addSortinggetStatus()
	{
		List<Expense> list=getDataprovider().getList();
		columnSort=new ListHandler<Expense>(list);
		columnSort.setComparator(getStatusColumn, new Comparator<Expense>()
				{
			@Override
			public int compare(Expense e1,Expense e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getStatus()!=null && e2.getStatus()!=null){
						return e1.getStatus().compareTo(e2.getStatus());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetStatus()
	{
		getStatusColumn=new TextColumn<Expense>()
				{
			@Override
			public String getValue(Expense object)
			{
				return object.getStatus()+"";
			}
				};
				table.addColumn(getStatusColumn,"Status");
				table.setColumnWidth(getStatusColumn,100,Unit.PX);
				getStatusColumn.setSortable(true);
	}
	protected void addSortinggetGroup()
	{
		List<Expense> list=getDataprovider().getList();
		columnSort=new ListHandler<Expense>(list);
		columnSort.setComparator(getGroupColumn, new Comparator<Expense>()
				{
			@Override
			public int compare(Expense e1,Expense e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getGroup()!=null && e2.getGroup()!=null){
						return e1.getGroup().compareTo(e2.getGroup());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetGroup()
	{
		getGroupColumn=new TextColumn<Expense>()
				{
			@Override
			public String getValue(Expense object)
			{
				return object.getGroup()+"";
			}
				};
				table.addColumn(getGroupColumn,"Group");
				table.setColumnWidth(getGroupColumn,100,Unit.PX);
				getGroupColumn.setSortable(true);
	}
	protected void addSortinggetCategory()
	{
		List<Expense> list=getDataprovider().getList();
		columnSort=new ListHandler<Expense>(list);
		columnSort.setComparator(getCategoryColumn, new Comparator<Expense>()
				{
			@Override
			public int compare(Expense e1,Expense e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getCategory()!=null && e2.getCategory()!=null){
						return e1.getCategory().compareTo(e2.getCategory());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetCategory()
	{
		getCategoryColumn=new TextColumn<Expense>()
				{
			@Override
			public String getValue(Expense object)
			{
				return object.getCategory()+"";
			}
				};
				table.addColumn(getCategoryColumn,"Category");
				table.setColumnWidth(getCategoryColumn,110,Unit.PX);
				getCategoryColumn.setSortable(true);
	}
	protected void addSortinggetType()
	{
		List<Expense> list=getDataprovider().getList();
		columnSort=new ListHandler<Expense>(list);
		columnSort.setComparator(getTypeColumn, new Comparator<Expense>()
				{
			@Override
			public int compare(Expense e1,Expense e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getType()!=null && e2.getType()!=null){
						return e1.getType().compareTo(e2.getType());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetType()
	{
		getTypeColumn=new TextColumn<Expense>()
				{
			@Override
			public String getValue(Expense object)
			{
				return object.getType()+"";
			}
				};
				table.addColumn(getTypeColumn,"Type");
				table.setColumnWidth(getTypeColumn,100,Unit.PX);
				getTypeColumn.setSortable(true);
	}
	protected void addSortinggetVendor()
	{
		List<Expense> list=getDataprovider().getList();
		columnSort=new ListHandler<Expense>(list);
		columnSort.setComparator(getVendorColumn, new Comparator<Expense>()
				{
			@Override
			public int compare(Expense e1,Expense e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getVendor()!=null && e2.getVendor()!=null){
						return e1.getVendor().compareTo(e2.getVendor());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetVendor()
	{
		getVendorColumn=new TextColumn<Expense>()
				{
			@Override
			public String getValue(Expense object)
			{
				return object.getVendor()+"";
			}
				};
				table.addColumn(getVendorColumn,"Vendor");
				table.setColumnWidth(getVendorColumn,110,Unit.PX);
				getVendorColumn.setSortable(true);
	}
	protected void addSortinggetInvoiceNumber()
	{
		List<Expense> list=getDataprovider().getList();
		columnSort=new ListHandler<Expense>(list);
		columnSort.setComparator(getInvoiceNumberColumn, new Comparator<Expense>()
				{
			@Override
			public int compare(Expense e1,Expense e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getInvoiceNumber()!=null && e2.getInvoiceNumber()!=null){
						return e1.getInvoiceNumber().compareTo(e2.getInvoiceNumber());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetInvoiceNumber()
	{
		getInvoiceNumberColumn=new TextColumn<Expense>()
				{
			@Override
			public String getValue(Expense object)
			{
				return object.getInvoiceNumber()+"";
			}
				};
				table.addColumn(getInvoiceNumberColumn,"Invoice No");
				table.setColumnWidth(getInvoiceNumberColumn,100,Unit.PX);
				getInvoiceNumberColumn.setSortable(true);
	}
	protected void addSortinggetAmount()
	{
		List<Expense> list=getDataprovider().getList();
		columnSort=new ListHandler<Expense>(list);
		columnSort.setComparator(getAmountColumn, new Comparator<Expense>()
				{
			@Override
			public int compare(Expense e1,Expense e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getAmount()== e2.getAmount()){
						return 0;}
					if(e1.getAmount()> e2.getAmount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetAmount()
	{
		getAmountColumn=new TextColumn<Expense>()
				{
			@Override
			public String getValue(Expense object)
			{
				return object.getAmount()+"";
			}
				};
				table.addColumn(getAmountColumn,"Amount");
				table.setColumnWidth(getAmountColumn,90,Unit.PX);
				getAmountColumn.setSortable(true);
	}
	protected void addSortinggetExpenseDate()
	{
		List<Expense> list=getDataprovider().getList();
		columnSort=new ListHandler<Expense>(list);
		columnSort.setComparator(getExpenseDateColumn, new Comparator<Expense>()
				{
			@Override
			public int compare(Expense e1,Expense e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getExpenseDate()!=null && e2.getExpenseDate()!=null){
						return e1.getExpenseDate().compareTo(e2.getExpenseDate());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetExpenseDate()
	{
		
		getExpenseDateColumn=new TextColumn<Expense>()
				{

					@Override
					public String getValue(Expense object) {
						if(object.getExpenseDate()!=null)
							return AppUtility.parseDate(object.getExpenseDate());
						return "N.A";
					}
			
				};
				table.addColumn(getExpenseDateColumn,"Expense Date");
				table.setColumnWidth(getExpenseDateColumn,110,Unit.PX);
				getExpenseDateColumn.setSortable(true);
	}
	
}
