package  com.slicktechnologies.client.views.account.expensemanagment;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.approvalutility.ApprovableFormScreenPresenter;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.PettyCash;
import com.slicktechnologies.shared.PettyCash.ApproverAmount;
import com.slicktechnologies.shared.common.businessprocesslayer.Expense;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

/**
 *  Expense Managment Presenter
 */
public class ExpensemanagmentPresenter extends ApprovableFormScreenPresenter<Expense> implements ChangeHandler,ValueChangeHandler<Double> {

	//Token to set the concrete form
	/** The form. */
	public static ExpensemanagmentForm form;
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	final GenricServiceAsync genasync=GWT.create(GenricService.class);
	PopupForPettyCashName pettycashPopup = new PopupForPettyCashName();
	PopupPanel panel;
	ExpenseManagmentServiceAsync expenseService = GWT.create(ExpenseManagmentService.class); 
	/**
	 * Instantiates a new expensemanagment presenter.
	 *
	 * @param view the view
	 * @param model the model
	 */
	public ExpensemanagmentPresenter (FormScreen<Expense> view,
			Expense model) {
		super(view, model);
		form=(ExpensemanagmentForm) view;
		form.setPresenter(this);
		form.getSearchpopupscreen().getPopup().setWidth("1500px");
		form.cbexptype.addClickHandler(this);
		form.olbpettycashname.addChangeHandler(this);
		form.dbAmount.addValueChangeHandler(this);
		
		pettycashPopup.getBtnOk().addClickHandler(this);
		pettycashPopup.getBtnCancel().addClickHandler(this);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.EXPENSEMANAGMENT,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}

	
	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.EntityPresenter#reactOnPrint()
	 */
	@Override
	public void reactOnPrint() {
		
		final String url1 = GWT.getModuleBaseURL() + "pdfexpense"+"?Id="+model.getId()+"&" + "type=" + "Single"+"&"+"companyId="+model.getCompanyId();
		Window.open(url1, "test", "enabled");
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.EntityPresenter#reactOnEmail()
	 */
	@Override
	public void reactOnEmail() {
		
		
	}
	
	@Override
	public void reactOnDownload() 
	{
		ArrayList<Expense> custarray=new ArrayList<Expense>();
		List<Expense> list=(List<Expense>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
		
		custarray.addAll(list);
		
		csvservice.setexpenselist(custarray, new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);
				
			}

			@Override
			public void onSuccess(Void result) {
				

				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "csvservlet"+"?type="+4;
				Window.open(url, "test", "enabled");
				
			}
		});

	}
	
	
	
	

	/**
	 * Method template to set new model.
	 */
	@Override
	protected void makeNewModel() {
		
		model=new Expense();
	}
	
	/*
	 * Method template to set Myquerry object
	 */
	/**
	 * Gets the expense query.
	 *
	 * @return the expense query
	 */
	public MyQuerry getExpenseQuery()
	{
		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new Expense());
		return quer;
	}
	
	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.EntityPresenter#setModel(com.simplesoftwares.client.library.appstructure.SuperModel)
	 */
	public void setModel(Expense entity)
	{
		model=entity;
	}
	
	/**
	 * Initalize.
	 * @return 
	 */
	public static ExpensemanagmentForm initalize()
	{
		
			 ExpensemanagmentPresenterTable gentableScreen=new ExpensemanagmentPresenterTableProxy();
			
			ExpensemanagmentForm  form=new  ExpensemanagmentForm();
			gentableScreen.setView(form);
			gentableScreen.applySelectionModle();
			
			ExpensemanagmentPresenterTable gentableSearch=new ExpensemanagmentPresenterTableProxy();
			gentableSearch.setView(form);
			gentableSearch.applySelectionModle();
			
			ExpensemanagmentPresenterSearch.staticSuperTable=gentableSearch;
			ExpensemanagmentPresenterSearch searchpopup=new ExpensemanagmentPresenterSearchProxy();
			form.setSearchpopupscreen(searchpopup);
			
			 ExpensemanagmentPresenter  presenter=new  ExpensemanagmentPresenter  (form,new Expense());
			AppMemory.getAppMemory().stickPnel(form);
			return form;
			
			
		 
	}
	
	@Override
	public void reactToProcessBarEvents(ClickEvent e) 
	{
		super.reactToProcessBarEvents(e);
		InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();

		
		if(text.equals("New"))
			reactToNew();
		
		
		if(text.equals("Update Record"))
		{
			panel=new PopupPanel(true);
			panel.add(pettycashPopup);
			panel.setGlassEnabled(true);
			panel.show();
			panel.center();
		}
	}
	


	private void reactToNew()
	{
		form.setToNewState();
		this.initalize();
		form.toggleAppHeaderBarMenu();
	}
	
	
		/**
		 * The Class ExpensemanagmentPresenterTable.
		 */
		@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.businessprocesslayer.Expense")
		 public static class  ExpensemanagmentPresenterTable extends SuperTable<Expense> implements GeneratedVariableRefrence{

			/* (non-Javadoc)
			 * @see com.slicktechnologies.client.utility.GeneratedVariableRefrence#getVarRef(java.lang.String)
			 */
			@Override
			public Object getVarRef(String varName) {
		
				return null;
			}

			/* (non-Javadoc)
			 * @see com.simplesoftwares.client.library.appstructure.SuperTable#createTable()
			 */
			@Override
			public void createTable() {
		
				
			}

			/* (non-Javadoc)
			 * @see com.simplesoftwares.client.library.appstructure.SuperTable#initializekeyprovider()
			 */
			@Override
			protected void initializekeyprovider() 
			{
	
				
			}

			/* (non-Javadoc)
			 * @see com.simplesoftwares.client.library.appstructure.SuperTable#addFieldUpdater()
			 */
			@Override
			public void addFieldUpdater() 
			{
		
				
			}

			/* (non-Javadoc)
			 * @see com.simplesoftwares.client.library.appstructure.SuperTable#setEnable(boolean)
			 */
			@Override
			public void setEnable(boolean state)
			{
			
				
			}

			/* (non-Javadoc)
			 * @see com.simplesoftwares.client.library.appstructure.ViewContainer#applyStyle()
			 */
			@Override
			public void applyStyle() 
			{

				
			}} ;
			
			/**
			 * The Class ExpensemanagmentPresenterSearch.
			 */
			@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.businessprocesslayer.Expense")
			 public static class  ExpensemanagmentPresenterSearch extends SearchPopUpScreen<  Expense>{

				/* (non-Javadoc)
				 * @see com.simplesoftwares.client.library.appstructure.SearchPopUpScreen#getQuerry()
				 */
				@Override
				public MyQuerry getQuerry() {
			
					return null;
				}

				@Override
				public boolean validate() {
					// TODO Auto-generated method stub
					return true;
				}}

			
			@Override
			public void onClick(ClickEvent event) {
				super.onClick(event);
				
				if(event.getSource()==form.cbexptype)
				{
					if(form.cbexptype.getValue()==true)
					{
						form.olbApproverName.setSelectedIndex(0);
						form.olbpettycashname.setEnabled(true);
						form.olbApproverName.setEnabled(false);
					}
					else
					{
						form.olbpettycashname.setSelectedIndex(0);
						form.olbpettycashname.setEnabled(false);
						form.olbApproverName.setEnabled(true);
						form.olbApproverName.setSelectedIndex(0);
					}
				}
				
				
				
				if(event.getSource()==pettycashPopup.btnOk){
					
					if(pettycashPopup.getOblPettycashName().getSelectedIndex()!=0)
					{
						final String pettycashName =pettycashPopup.getOblPettycashName().getValue(pettycashPopup.getOblPettycashName().getSelectedIndex()).trim();
						expenseService.pettycashBalanceDeductAndUpdatePettcashTransaction(pettycashName,model, new AsyncCallback<Void>() {

							@Override
							public void onFailure(Throwable caught) {
								
							}

							@Override
							public void onSuccess(Void result) {
								
								form.showDialogMessage("Pettycash Updated and Transaction Created...!!");
								
								model.setPettyCashName(pettycashName);
								genasync.save(model, new AsyncCallback<ReturnFromServer>()
								{

									@Override
									public void onFailure(Throwable caught) {
										// TODO Auto-generated method stub
										
									}

									@Override
									public void onSuccess(ReturnFromServer result) {
										
										form.getOlbpettycashname().setValue(model.getPettyCashName());
										form.setToViewState();
									}
							
								});
								
							}
						});
						
						panel.hide();
					}
					else
					{
						form.showDialogMessage("Please Select Pettycash Name..!!");
						panel.hide();
					}
					
				}
				
				if(event.getSource()==pettycashPopup.btnCancel){
					panel.hide();
				}
			}
				
			
			
			@Override
			public void onValueChange(ValueChangeEvent<Double> event) {
				if(event.getSource()==form.dbAmount)
				{
					if(form.olbpettycashname.getSelectedItem()!=null&&form.dbAmount.getValue()!=null)
					{
						reactOnExpenseFormField();
					}
				}
			}


			@Override
			public void onChange(ChangeEvent event) {
				if(event.getSource()==form.olbpettycashname)
				{
					if(form.olbpettycashname.getSelectedItem()!=null&&form.dbAmount.getValue()!=null)
					{
						reactOnExpenseFormField();
					}
				}
			};
			
			public void reactOnExpenseFormField()
			{
				if(form.cbexptype.getValue()==true)
				{
					PettyCash petty=form.olbpettycashname.getSelectedItem();
					ArrayList<ApproverAmount> listPetty=petty.getAmountApproved();
					
					Collections.sort(listPetty,new  Comparator<ApproverAmount>(){

						@Override
						public int compare(ApproverAmount o1, ApproverAmount o2) {
							if(o1.getAmount()>o2.getAmount())
								return 1;
							if(o1.getAmount()==o2.getAmount())
								return 0;
							else
								return -1;
						}
					}
					);
					
					double petybalance=petty.getPettyCashBalance();
					double expenseAmt=form.dbAmount.getValue();
					double configAmount=0;
					
					System.out.println("petybalance="+petybalance+"-"+"expenseAmt="+expenseAmt);
					
					for(int i=0;i<listPetty.size();i++)
					{
						configAmount=listPetty.get(i).getAmount();
						
						if(configAmount>=expenseAmt)
						{
						form.olbApproverName.setValue(listPetty.get(i).getEmployeeName().trim());
						break;
						}
					}
					if(configAmount<expenseAmt)
					{
						form.showDialogMessage("Approver not assigned for entered amount!");
						form.olbApproverName.setSelectedIndex(0);
					}
					if(expenseAmt>petybalance)
					{
						form.showDialogMessage("No sufficient balance in petty cash!");
						form.olbApproverName.setSelectedIndex(0);
					}
				}
			}


			@Override
			public void reactOnSearch() {
				super.reactOnSearch();
				
				ExpensemanagmentPresenterSearchProxy.cbexptypepettycash.setValue(false);
			}
}
