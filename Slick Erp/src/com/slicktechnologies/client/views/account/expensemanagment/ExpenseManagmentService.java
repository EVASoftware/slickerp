package com.slicktechnologies.client.views.account.expensemanagment;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.slicktechnologies.shared.common.businessprocesslayer.Expense;



@RemoteServiceRelativePath("updateexpensemanagmentservice")
public interface ExpenseManagmentService extends RemoteService{

	public void pettycashBalanceDeductAndUpdatePettcashTransaction(String pettycashName,Expense model);
	
//  rohan added this method for migrating data from expense mngt on date: 5/1/2016
	public void migrateDataFromExpenseMngt(Long companyId);
}
