package com.slicktechnologies.client.views.account.expensemanagment;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.*;
import com.simplesoftwares.client.library.appskeleton.*;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.*;
import com.simplesoftwares.client.library.mywidgets.*;
import com.slicktechnologies.client.approvalutility.ApprovableFormScreen;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.config.CategoryTypeFactory;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.*;
import com.slicktechnologies.client.views.account.multipleexpensemanagment.ExpenseManagementTable;
import com.slicktechnologies.client.views.assesmentreport.AssesmentReportEmbedForTable;
import com.slicktechnologies.client.views.project.concproject.ProjectPresenter;
import com.slicktechnologies.shared.*;
import com.slicktechnologies.shared.common.businessprocesslayer.*;
import com.slicktechnologies.shared.common.businessunitlayer.*;
import com.slicktechnologies.shared.common.helperlayer.*;
import com.slicktechnologies.shared.common.personlayer.*;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickHandler;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.google.gwt.event.dom.client.*;
// TODO: Auto-generated Javadoc
/**
 * Repersents ExpenseForm
 */
public class ExpensemanagmentForm extends ApprovableFormScreen<Expense>  implements ClickHandler, ChangeHandler {

	//***********************************Variable Declaration Starts********************************************//
	//Token to add the varialble declarations
	/** expense Date */
	DateBox dbExpenseDate;
	
	TextBox tbexpenseId;
	/** repersents Branch */
	ObjectListBox<Branch> olbBranch;
	
	/** The olb expense group. */
	ObjectListBox<Config>olbCurrency,olbPaymentMethod,olbExpenseGroup;
	
	/** The olb category. */
	ObjectListBox<ConfigCategory>olbCategory;
	
	/** The olb type. */
	ObjectListBox<Type>olbType;
	
	/** The tb status. */
	TextBox tbInvoiceNo,tbVendor,tbStatus;
	
	/** The ta description. */
	TextArea taDescription;
	
	/** The uc choose file. */
	UploadComposite ucChooseFile;
	
	/** The db amount. */
	DoubleBox dbAmount;
	
	/** The olb approver name. */
	ObjectListBox<Employee> olbEmployee,olbApproverName;
	
	CheckBox cbexptype;
	ObjectListBox<PettyCash> olbpettycashname;
	TextBox tbremark;
	
	TextBox tbAccount;
	
	Expense expenseobj;
	
	TextBox tbServiceId;
	
	
	
	/**
	 *rohan has added this flag for validating petty cash name from withdraw button
	 */
	
	boolean pettyCashNameFlag = false;
	
	/**
	 * ends here 
	 */
	
	/**
	 * Rohan added this table for multiple expenses at the same time
	 */
//	Button addDetails;
//	ExpenseManagementTable expenseMngtTable;
//	IntegerBox totalTotalAmt;
	
	//***********************************Variable Declaration Ends********************************************//


	//Token to add the concrete presenter class name
	//protected <PresenterClassName> presenter;



	/**
	 * Instantiates a new expensemanagment form.
	 *
	 * @param table the table
	 * @param mode the mode
	 * @param captionmode the captionmode
	 */
	public ExpensemanagmentForm  () {
		
		createGui();
		
//		expenseMngtTable.connectToLocal();
	}
	
	//***********************************Variable Initialization********************************************//

	/**
	 * Initalize widget.
	 */
	private void initalizeWidget()
	{

		dbExpenseDate=new DateBoxWithYearSelector();

		olbBranch=new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbBranch);

		olbExpenseGroup=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbExpenseGroup,Screen.EXPENSEGROUP);
		olbCategory=new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(getOlbCategory(), Screen.EXPENSECATEGORY);
		olbCategory.addChangeHandler(this);
		olbType=new ObjectListBox<Type>();
		AppUtility.makeTypeListBoxLive(this.olbType, Screen.EXPENSETYPE);

		olbCurrency=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbCurrency,Screen.CURRENCY);

		olbPaymentMethod=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbPaymentMethod,Screen.PAYMENTMETHODS);

		tbInvoiceNo=new TextBox();
		tbStatus=new TextBox();
		tbStatus.setEnabled(false);
		tbStatus.setText(Expense.CREATED);
		tbVendor=new TextBox();

		taDescription=new TextArea();

		ucChooseFile=new UploadComposite();

		dbAmount=new DoubleBox();

		tbexpenseId=new TextBox();
		tbexpenseId.setEnabled(false);
		
		tbremark=new TextBox();
		tbremark.setEnabled(false);
		
		olbEmployee=new ObjectListBox<Employee>();
		AppUtility.makeSalesPersonListBoxLive(olbEmployee);

		olbApproverName=new ObjectListBox<Employee>();
		AppUtility.makeApproverListBoxLive(olbApproverName, "Expense Management");
		cbexptype=new CheckBox();
		olbpettycashname=new ObjectListBox<PettyCash>();
		olbpettycashname.MakeLive(new MyQuerry(new Vector<Filter>(), new PettyCash()));
		olbpettycashname.setEnabled(false);
		
		tbAccount=new TextBox();

		tbServiceId = new TextBox();
		
//		expenseMngtTable = new ExpenseManagementTable();
//		
//		addDetails=new Button("ADD");
//		addDetails.addClickHandler(this);
//		
//		totalTotalAmt = new IntegerBox();
//		totalTotalAmt.setEnabled(false);
	}

	/*
	 * Method template to create the formtable screen
	 */

	/* (non-Javadoc)
	 * @see com.slicktechnologies.client.approvalutility.ApprovableFormTableScreen#createScreen()
	 */
	@Override
	public void createScreen()  {

		//Token to initialize the processlevel menus.
		initalizeWidget();

		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Expense","UpdateRecordButtonVisibility"))
		{
			processlevelBarNames=new String[]{"Request For Approval","Cancel Approval Request","New","Update Record"};
		}	
		else
		{
			processlevelBarNames=new String[]{"Request For Approval","Cancel Approval Request","New"};
		}
		//////////////////////////////////Form Field Declaration/////////////////////////////////////////////////


		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////

		//Token to initialize formfield
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingExpenseDetails=fbuilder.setlabel("Expense Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		fbuilder = new FormFieldBuilder("Invoice No",tbInvoiceNo);
		FormField ftbInvoiceNo= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Expense ID",tbexpenseId);
		FormField ftbexpenseId= fbuilder.setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Category",olbCategory);
		FormField folbCategory= fbuilder.setMandatory(false).setMandatoryMsg("Category is Mandatory!").setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Expense Group",olbExpenseGroup);
		FormField folbExpenseGroup= fbuilder.setMandatory(false).setMandatoryMsg("Group is Mandatory!").setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Expense Type",olbType);
		FormField folbExpenseType= fbuilder.setMandatory(false).setMandatoryMsg("Type is Mandatory!").setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("* Status",tbStatus);
		FormField ftbExpenseStatus= fbuilder.setMandatory(true).setMandatoryMsg("Status is Mandatory!").setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("* Expense Date",dbExpenseDate);
		FormField fdbExpenseDate= fbuilder.setMandatory(true).setMandatoryMsg("Expense Date is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Currency",olbCurrency);
		FormField folbCurrency= fbuilder.setMandatory(true).setMandatoryMsg("Currency is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Amount",dbAmount);
		FormField fdbAmount= fbuilder.setMandatory(true).setMandatoryMsg("Amount is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Payment Method",olbPaymentMethod);
		FormField folbPaymentMethod= fbuilder.setMandatory(true).setMandatoryMsg("Payment Method is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Vendor",tbVendor);
		FormField ftbVendor= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Employee",olbEmployee);
		FormField folbEmployee= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Branch",olbBranch);
		FormField folbBranch= fbuilder.setMandatory(true).setMandatoryMsg("Branch is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingExpenseDocuments=fbuilder.setlabel("Expense Documents").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		fbuilder = new FormFieldBuilder("Choose File",ucChooseFile);
		FormField fucChooseFile= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingDescriptions=fbuilder.setlabel("Descriptions").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		fbuilder = new FormFieldBuilder("Description",taDescription);
		FormField ftaDescription= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(5).build();
		fbuilder = new FormFieldBuilder("Remark",tbremark);
		FormField ftbremark= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("* Approver Name",olbApproverName);
		FormField folbApproverName= fbuilder.setMandatory(true).setMandatoryMsg("Approver Name  is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingPettyCash=fbuilder.setlabel("Petty Cash Expenses").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();		
		fbuilder = new FormFieldBuilder("Is expense type a petty cash",cbexptype);
		FormField fcbexptype= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Petty Cash Name",olbpettycashname);
		FormField folbpettycashname= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Account",tbAccount);
		FormField ftbAccount= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Service ID",tbServiceId);
		FormField ftbreferenceNo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

//		fbuilder = new FormFieldBuilder(" ",expenseMngtTable.getTable());
//		FormField ftable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
//		
//		fbuilder = new FormFieldBuilder("",addDetails);
//		FormField faddDetails= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
//		
		
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////


		FormField[][] formfield = {{fgroupingExpenseDetails},
				{ftbexpenseId,ftbInvoiceNo,folbExpenseGroup,folbCategory,folbExpenseType},
				{ftbExpenseStatus,fdbExpenseDate,folbApproverName,folbBranch,folbEmployee},
				{fdbAmount,folbCurrency,folbPaymentMethod,ftbVendor,ftbremark},
				{ftbreferenceNo},
//				{ftable},faddDetails
				{fgroupingPettyCash},
				{fcbexptype,folbpettycashname,ftbAccount},
				{fgroupingExpenseDocuments},
				{fucChooseFile},
				{fgroupingDescriptions},
				{ftaDescription},
		};


		this.fields=formfield;		
	}

	/**
	 * method template to update the model with token entity name.
	 *
	 * @param model the model
	 */
	@Override
	public void updateModel(Expense model) 
	{
		if(tbInvoiceNo.getValue()!=null)
			model.setInvoiceNumber(tbInvoiceNo.getValue());

		if(dbExpenseDate.getValue()!=null)
			model.setExpenseDate(dbExpenseDate.getValue());
		if(olbCurrency.getValue()!=null)
			model.setCurrency(olbCurrency.getValue());
		if(dbAmount.getValue()!=null)
			model.setAmount(dbAmount.getValue());
		if(olbPaymentMethod.getValue()!=null)
			model.setPaymentMethod(olbPaymentMethod.getValue());
		if(tbVendor.getValue()!=null)
			model.setVendor(tbVendor.getValue());
		if(olbEmployee.getValue()!=null)
			model.setEmployee(olbEmployee.getValue());
		if(olbBranch.getValue()!=null)
			model.setBranch(olbBranch.getValue());
		
			model.setExpenseDocument(ucChooseFile.getValue());
		if(taDescription.getValue()!=null)
			model.setDescription(taDescription.getValue());
		if(olbApproverName.getValue()!=null)
			model.setApproverName(olbApproverName.getValue());
		if(olbExpenseGroup.getValue()!=null)
			model.setGroup(olbExpenseGroup.getValue());
		if(olbCategory.getValue()!=null)
			model.setCategory(olbCategory.getValue());

		if(olbType.getValue()!=null)
			model.setType(olbType.getValue(olbType.getSelectedIndex()));
		if(tbStatus.getValue()!=null)
			model.setStatus(tbStatus.getValue());
		if(cbexptype.getValue()!=null)
			model.setExpTypeAsPettyCash(cbexptype.getValue());
		if(olbpettycashname.getValue()!=null)
			model.setPettyCashName(olbpettycashname.getValue());

		if(tbAccount.getValue()!=null)
			model.setAccount(tbAccount.getValue());
		
		if(!tbServiceId.getValue().equals(""))
			model.setServiceId(Integer.parseInt(tbServiceId.getValue()));
		
		
//		List<ExpenseManagement> expenseLis=this.expenseMngtTable.getDataprovider().getList();
//		ArrayList<ExpenseManagement> arrItems=new ArrayList<ExpenseManagement>();
//		arrItems.addAll(expenseLis);
//		model.setExpenseList(arrItems);
		
		

		expenseobj=model;
		
		presenter.setModel(model);

	}
	
	/**
	 * method template to update the view with token entity name.
	 *
	 * @param view the view
	 */
	@Override
	public void updateView(Expense view) 
	{
		
		expenseobj=view;
		
			tbexpenseId.setValue(view.getCount()+"");
		if(view.getInvoiceNumber()!=null)
			tbInvoiceNo.setValue(view.getInvoiceNumber());
		if(view.getCategory()!=null)
			olbCategory.setValue(view.getCategory());
		if(view.getExpenseDate()!=null)
			dbExpenseDate.setValue(view.getExpenseDate());
		if(view.getCurrency()!=null)
			olbCurrency.setValue(view.getCurrency());
		if(view.getAmount()!=null)
			dbAmount.setValue(view.getAmount());
		if(view.getPaymentMethod()!=null)
			olbPaymentMethod.setValue(view.getPaymentMethod());
		if(view.getVendor()!=null)
			tbVendor.setValue(view.getVendor());
		if(view.getEmployee()!=null)
			olbEmployee.setValue(view.getEmployee());
		if(view.getBranch()!=null)
			olbBranch.setValue(view.getBranch());
		if(view.getExpenseDocument()!=null)
			ucChooseFile.setValue(view.getExpenseDocument());
		if(view.getDescription()!=null)
			taDescription.setValue(view.getDescription());
		if(view.getApproverName()!=null)
			olbApproverName.setValue(view.getApproverName());
		if(view.getCategory()!=null)
			olbCategory.setValue(view.getCategory());
		if(view.getGroup()!=null)
			olbExpenseGroup.setValue(view.getGroup());
		if(view.getType()!=null)
			olbType.setValue(view.getType());
		if(view.getStatus()!=null)
			tbStatus.setValue(view.getStatus());
		if(view.getExpTypeAsPettyCash()!=null)
			cbexptype.setValue(view.getExpTypeAsPettyCash());
		if(view.getPettyCashName()!=null)
			olbpettycashname.setValue(view.getPettyCashName());
		if(view.getRemark()!=null){
			tbremark.setValue(view.getRemark());
		}
		if(view.getAccount()!=null){
			tbAccount.setValue(view.getAccount());
		}
		
		if(view.getServiceId()!=0){
			tbServiceId.setValue(view.getServiceId()+"");
		}
		
//		expenseMngtTable.setValue(view.getExpenseList());
		
		
		
		/* 
		 * for approval process
		 *  nidhi
		 *  5-07-2017
		 */
		if(presenter != null){
			presenter.setModel(view);
		}
		/*
		 *  end
		 */
	}

	/**
	 * Toggles the app header bar menus as per screen state.
	 */

	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Save")||text.contains("Discard")||text.contains("Search")||text.contains(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Save")||text.contains("Discard")||text.contains(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Edit")||text.contains("Discard")||text.contains("Search")||text.contains("Print")||text.contains(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
			

			
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.EXPENSEMANAGMENT,LoginPresenter.currentModule.trim());

	}
	
	
	public void toggleProcessLevelMenu()
	{
		Expense entity=(Expense) presenter.getModel();
		
		String status=entity.getStatus();

		for(int i=0;i<getProcesslevelBarNames().length;i++)
		{
			InlineLabel label=getProcessLevelBar().btnLabels[i];
			String text=label.getText().trim();

			if(status.equals(Expense.APPROVED))
			{
				if((text.contains(AppConstants.NEW)))
					label.setVisible(true);
				else
					label.setVisible(false);
			}

			else if(status.equals(Expense.REQUESTED))
			{
				if((text.contains(ManageApprovals.CANCELAPPROVALREQUEST)))
					label.setVisible(true);
				else
					label.setVisible(false);
			}
			
			else if(status.equals(Expense.REJECTED))
			{
				if(text.contains(AppConstants.NEW))
					label.setVisible(true);
				else
					label.setVisible(false);
			}
		}
	}

	

	public void setAppHeaderBarAsPerStatus()
	{
		Expense entity=(Expense) presenter.getModel();
		String status=entity.getStatus();
		

		if(status.equals(Expense.APPROVED))
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Discard")||text.contains("Search")||text.contains("Print")||text.contains(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
			}
		}
		
		if(status.equals(Expense.REQUESTED))
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Discard")||text.contains("Search")||text.contains(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
			}
		}
	}

	
	
	
	
	
	@Override
	public void setToViewState() {
		super.setToViewState();
		setAppHeaderBarAsPerStatus();
		
		SuperModel model=new Expense();
		model=expenseobj;
		AppUtility.addDocumentToHistoryTable(AppConstants.ACCOUNTMODULE,AppConstants.EXPENSEMANAGEMET, expenseobj.getCount(), null,null,null, false, model, null);
	
	}

	@Override
	public void setToEditState() {
		super.setToEditState();
		this.processLevelBar.setVisibleFalse(false);
	}

	/**
	 * sets the id textbox with the passed count value.
	 *
	 * @param count the new count
	 */
	@Override
	public void setCount(int count)
	{
		tbexpenseId.setValue(count+"");
	}


	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) {


	}
	


	@Override
	public boolean validate() {
		boolean superRes= super.validate();
		boolean exppettycash=true;
		
		if(this.cbexptype.getValue()==true&&this.olbpettycashname.getSelectedIndex()==0)
		{
			this.showDialogMessage("Petty Cash Name is Mandatory!");
			exppettycash=false;
		}
		
		if(this.isPettyCashNameFlag())
		{
			if(this.getOlbpettycashname().getSelectedIndex()==0)
			{
				this.showDialogMessage("Petty Cash Name is Mandatory!");
				exppettycash=false;
			}
			
		}
		
		return superRes&&exppettycash;
	}
	
	
	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen#clear()
	 */
	@Override
	public void clear()
	{
		super.clear();
		this.tbStatus.setText(Expense.CREATED);
		tbStatus.setEnabled(false);
	}
	
	@Override
	public void setEnable(boolean status)
	{
		super.setEnable(status);
		this.tbStatus.setEnabled(false);
		olbpettycashname.setEnabled(false);
		tbexpenseId.setEnabled(false);
		tbremark.setEnabled(false);
	}
	
	
	/*******************************************Type Drop Down Logic**************************************/
	
	@Override
	public void onChange(ChangeEvent event) {
		if(event.getSource().equals(olbCategory))
		{
			if(olbCategory.getSelectedIndex()!=0){
				ConfigCategory cat=olbCategory.getSelectedItem();
				if(cat!=null){
					AppUtility.makeLiveTypeDropDown(olbType, cat.getCategoryName(), cat.getCategoryCode(), cat.getInternalType());
				}
			}
		}
	}
	
	
	/*********************************************Getters And Setters****************************************/

	/**
	 * Gets the db expense date.
	 *
	 * @return the db expense date
	 */
	public DateBox getDbExpenseDate() {
		return dbExpenseDate;
	}

	/**
	 * Sets the db expense date.
	 *
	 * @param dbExpenseDate the new db expense date
	 */
	public void setDbExpenseDate(DateBox dbExpenseDate) {
		this.dbExpenseDate = dbExpenseDate;
	}

	/**
	 * Gets the olb branch.
	 *
	 * @return the olb branch
	 */
	public ObjectListBox<Branch> getOlbBranch() {
		return olbBranch;
	}

	/**
	 * Sets the olb branch.
	 *
	 * @param olbBranch the new olb branch
	 */
	public void setOlbBranch(ObjectListBox<Branch> olbBranch) {
		this.olbBranch = olbBranch;
	}

	/**
	 * Gets the olb currency.
	 *
	 * @return the olb currency
	 */
	public ObjectListBox<Config> getOlbCurrency() {
		return olbCurrency;
	}

	/**
	 * Sets the olb currency.
	 *
	 * @param olbCurrency the new olb currency
	 */
	public void setOlbCurrency(ObjectListBox<Config> olbCurrency) {
		this.olbCurrency = olbCurrency;
	}

	/**
	 * Gets the olb payment method.
	 *
	 * @return the olb payment method
	 */
	public ObjectListBox<Config> getOlbPaymentMethod() {
		return olbPaymentMethod;
	}

	/**
	 * Sets the olb payment method.
	 *
	 * @param olbPaymentMethod the new olb payment method
	 */
	public void setOlbPaymentMethod(ObjectListBox<Config> olbPaymentMethod) {
		this.olbPaymentMethod = olbPaymentMethod;
	}

	/**
	 * Gets the olb expense group.
	 *
	 * @return the olb expense group
	 */
	public ObjectListBox<Config> getOlbExpenseGroup() {
		return olbExpenseGroup;
	}

	/**
	 * Sets the olb expense group.
	 *
	 * @param olbExpenseGroup the new olb expense group
	 */
	public void setOlbExpenseGroup(ObjectListBox<Config> olbExpenseGroup) {
		this.olbExpenseGroup = olbExpenseGroup;
	}

	/**
	 * Gets the olb category.
	 *
	 * @return the olb category
	 */
	public ObjectListBox<ConfigCategory> getOlbCategory() {
		return olbCategory;
	}

	/**
	 * Sets the olb category.
	 *
	 * @param olbCategory the new olb category
	 */
	public void setOlbCategory(ObjectListBox<ConfigCategory> olbCategory) {
		this.olbCategory = olbCategory;
	}

	/**
	 * Gets the olb type.
	 *
	 * @return the olb type
	 */
	public ObjectListBox<Type> getOlbType() {
		return olbType;
	}

	/**
	 * Sets the olb type.
	 *
	 * @param olbType the new olb type
	 */
	public void setOlbType(ObjectListBox<Type> olbType) {
		this.olbType = olbType;
	}

	/**
	 * Gets the tb invoice no.
	 *
	 * @return the tb invoice no
	 */
	public TextBox getTbInvoiceNo() {
		return tbInvoiceNo;
	}

	/**
	 * Sets the tb invoice no.
	 *
	 * @param tbInvoiceNo the new tb invoice no
	 */
	public void setTbInvoiceNo(TextBox tbInvoiceNo) {
		this.tbInvoiceNo = tbInvoiceNo;
	}

	/**
	 * Gets the tb vendor.
	 *
	 * @return the tb vendor
	 */
	public TextBox getTbVendor() {
		return tbVendor;
	}

	/**
	 * Sets the tb vendor.
	 *
	 * @param tbVendor the new tb vendor
	 */
	public void setTbVendor(TextBox tbVendor) {
		this.tbVendor = tbVendor;
	}

	/**
	 * Gets the tb status.
	 *
	 * @return the tb status
	 */
	public TextBox getTbStatus() {
		return tbStatus;
	}

	/**
	 * Sets the tb status.
	 *
	 * @param tbStatus the new tb status
	 */
	public void setTbStatus(TextBox tbStatus) {
		this.tbStatus = tbStatus;
	}

	/**
	 * Gets the ta description.
	 *
	 * @return the ta description
	 */
	public TextArea getTaDescription() {
		return taDescription;
	}

	/**
	 * Sets the ta description.
	 *
	 * @param taDescription the new ta description
	 */
	public void setTaDescription(TextArea taDescription) {
		this.taDescription = taDescription;
	}

	/**
	 * Gets the uc choose file.
	 *
	 * @return the uc choose file
	 */
	public UploadComposite getUcChooseFile() {
		return ucChooseFile;
	}

	/**
	 * Sets the uc choose file.
	 *
	 * @param ucChooseFile the new uc choose file
	 */
	public void setUcChooseFile(UploadComposite ucChooseFile) {
		this.ucChooseFile = ucChooseFile;
	}

	/**
	 * Gets the db amount.
	 *
	 * @return the db amount
	 */
	public DoubleBox getDbAmount() {
		return dbAmount;
	}

	/**
	 * Sets the db amount.
	 *
	 * @param dbAmount the new db amount
	 */
	public void setDbAmount(DoubleBox dbAmount) {
		this.dbAmount = dbAmount;
	}

	/**
	 * Gets the olb employee.
	 *
	 * @return the olb employee
	 */
	public ObjectListBox<Employee> getOlbEmployee() {
		return olbEmployee;
	}

	/**
	 * Sets the olb employee.
	 *
	 * @param olbEmployee the new olb employee
	 */
	public void setOlbEmployee(ObjectListBox<Employee> olbEmployee) {
		this.olbEmployee = olbEmployee;
	}

	/**
	 * Gets the olb approver name.
	 *
	 * @return the olb approver name
	 */
	public ObjectListBox<Employee> getOlbApproverName() {
		return olbApproverName;
	}

	/**
	 * Sets the olb approver name.
	 *
	 * @param olbApproverName the new olb approver name
	 */
	public void setOlbApproverName(ObjectListBox<Employee> olbApproverName) {
		this.olbApproverName = olbApproverName;
	}

	public CheckBox getCbexptype() {
		return cbexptype;
	}

	public void setCbexptype(CheckBox cbexptype) {
		this.cbexptype = cbexptype;
	}

	public ObjectListBox<PettyCash> getOlbpettycashname() {
		return olbpettycashname;
	}

	public void setOlbpettycashname(ObjectListBox<PettyCash> olbpettycashname) {
		this.olbpettycashname = olbpettycashname;
	}

	public TextBox getTbremark() {
		return tbremark;
	}

	public void setTbremark(TextBox tbremark) {
		this.tbremark = tbremark;
	}

	/* (non-Javadoc)
	 * @see com.slicktechnologies.client.approvalutility.ApprovableScreen#getstatustextbox()
	 */
	@Override
	public TextBox getstatustextbox() {
	
		return this.tbStatus;
	}

	public TextBox getTbServiceId() {
		return tbServiceId;
	}

	public void setTbServiceId(TextBox tbServiceId) {
		this.tbServiceId = tbServiceId;
	}

	public boolean isPettyCashNameFlag() {
		return pettyCashNameFlag;
	}

	public void setPettyCashNameFlag(boolean pettyCashNameFlag) {
		this.pettyCashNameFlag = pettyCashNameFlag;
	}

//	public IntegerBox getTotalTotalAmt() {
//		return totalTotalAmt;
//	}
//
//	public void setTotalTotalAmt(IntegerBox totalTotalAmt) {
//		this.totalTotalAmt = totalTotalAmt;
//	}


	

}
