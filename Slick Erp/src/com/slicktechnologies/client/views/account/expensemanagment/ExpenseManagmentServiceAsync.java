package com.slicktechnologies.client.views.account.expensemanagment;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.slicktechnologies.shared.common.businessprocesslayer.Expense;


public interface ExpenseManagmentServiceAsync {

	public void pettycashBalanceDeductAndUpdatePettcashTransaction(String pettycashName, Expense model, AsyncCallback<Void> asyncCallback);
	
	//  rohan added this method for migrating data from expense mngt on date: 5/1/2016
	public void migrateDataFromExpenseMngt(Long companyId, AsyncCallback<Void> asyncCall);
}
