package com.slicktechnologies.client.views.account.pettycash;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.PettyCash;
import com.slicktechnologies.shared.PettyCash.ApproverAmount;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class PettyCashPresenter extends FormTableScreenPresenter<PettyCash> {
	
	PettyCashForm form;

	
	public PettyCashPresenter(FormTableScreen<PettyCash> view,
			PettyCash model) {
		super(view, model);
		form=(PettyCashForm) view;
		form.getSupertable().connectToLocal();
		form.retriveTable(getPettyCashQuery());
		form.setPresenter(this);
		form.btnadd.addClickHandler(this);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.PETTYCASH,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}

	/**
	 * Method template to set the processBar events
	 */
	@Override
	public void reactToProcessBarEvents(ClickEvent e) 
   {
		InlineLabel lbl= (InlineLabel) e.getSource();
		
	}

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void makeNewModel() {
		model=new PettyCash();
	}
	
	/*
	 * Method template to set Myquerry object
	 */
	public MyQuerry getPettyCashQuery()
	{
		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new PettyCash());
		return quer;
	}
	
	public void setModel(PettyCash entity)
	{
		model=entity;
	}
	
	public static void initalize()
	{
		
		
		
		
		PettyCashPresenterTable gentableScreen=new PettyCashPresenterTableProxy();
		PettyCashForm  form=new  PettyCashForm(gentableScreen,FormTableScreen.UPPER_MODE,true);
			gentableScreen.setView(form);
			gentableScreen.applySelectionModle();
			
			  //// PettyCashPresenterTable gentableSearch=GWT.create(PettyCashPresenterTable.class);
			  ////   PettyCashPresenterSearch.staticSuperTable=gentableSearch;
			  ////     PettyCashPresenterSearch searchpopup=GWT.create(PettyCashPresenterSearch.class);
			  ////         form.setSearchpopupscreen(searchpopup);
			
			PettyCashPresenter  presenter=new PettyCashPresenter(form,new PettyCash());
			AppMemory.getAppMemory().stickPnel(form);
			
	}
	
	
		@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.PettyCash")
		 public static class PettyCashPresenterTable extends SuperTable<PettyCash> implements GeneratedVariableRefrence{

			@Override
			public Object getVarRef(String varName) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void createTable() {
				// TODO Auto-generated method stub
				
			}

			@Override
			protected void initializekeyprovider() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void addFieldUpdater() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void setEnable(boolean state) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void applyStyle() {
				// TODO Auto-generated method stub
				
			}} ;
			
			@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.PettyCash")
			 public static class PettyCashPresenterSearch extends SearchPopUpScreen<PettyCash>{

				@Override
				public MyQuerry getQuerry() {
					// TODO Auto-generated method stub
					return null;
				}

				@Override
				public boolean validate() {
					// TODO Auto-generated method stub
					return true;
				}};
				
				
				
				@Override
				public void onClick(ClickEvent event) {
					super.onClick(event);
					
					if(event.getSource()==form.btnadd)
					{
					if(form.olbapprovername.getValue()==null||form.olbapprovername.getValue().equals(null))
					{
						form.showDialogMessage("Approver Name cannot be blank!");
					}
					if(form.ibamountupto.getValue()==null)
					{
						form.showDialogMessage("Amount cannot be blank!");
					}
					
					if(form.olbapprovername.getValue()!=null&&form.ibamountupto.getValue()!=null)
					{
						reactOnAddBtn();
					}
									
					}
					
				}//onclick method close
					
				
				
				public void reactOnAddBtn()
				{
					int flag=0;
					
					ApproverAmount approverAmt=new ApproverAmount();
					int amtupto=form.ibamountupto.getValue();
					String appName=form.olbapprovername.getValue();
					approverAmt.setAmount(amtupto);
					approverAmt.setEmployeeName(appName);
						
					List<ApproverAmount> listApprover=form.approvertable.getDataprovider().getList();
					ArrayList<ApproverAmount> alloc=new ArrayList<ApproverAmount>();
					alloc.addAll(listApprover);
					String apprName="";
					double amt=0;
						
					if(alloc.size()==0)
					{
						flag=0;
					}
					if(amtupto<1)
					   {
						   form.showDialogMessage("Amount cannot be 0!");
						   flag=1;
						   form.ibamountupto.setValue(null);
						   form.olbapprovername.setSelectedIndex(0);
					   }
					for(int i=0;i<alloc.size();i++)
					{
						apprName=alloc.get(i).getEmployeeName();
						amt=alloc.get(i).getAmount();
						if(apprName.equals(appName))
						{
							form.showDialogMessage("Approver limit already defined!");
							flag=1;
							form.ibamountupto.setValue(null);
							form.olbapprovername.setSelectedIndex(0);
						}
					   if(amtupto==amt)
						{
							form.showDialogMessage("Amount already added!");
							flag=1;
							form.ibamountupto.setValue(null);
							form.olbapprovername.setSelectedIndex(0);
						}
						if(!apprName.equals(appName)&&amtupto!=amt)
						{
							flag=0;
						}
					}
							
						if(flag==0&&form.olbapprovername.getValue()!=null&&form.ibamountupto.getValue()!=null)
						{
							form.approvertable.getDataprovider().getList().add(approverAmt);
							form.olbapprovername.setSelectedIndex(0);
							form.ibamountupto.setValue(null);
						}

				}
				
				
				
				
}
