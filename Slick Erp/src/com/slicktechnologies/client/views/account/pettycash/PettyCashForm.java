package com.slicktechnologies.client.views.account.pettycash;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.composite.CompositeInterface;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.PettyCash;
import com.slicktechnologies.shared.PettyCash.ApproverAmount;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class PettyCashForm extends FormTableScreen<PettyCash> implements ClickHandler,HasValue<ArrayList<ApproverAmount>>,CompositeInterface {
	
	IntegerBox ibpettycashid;
	TextBox tbpettycashname;
	ObjectListBox<Employee> olbpersonresponsible;
	CheckBox cbstatus;
	ObjectListBox<Branch> olbbranch;
	PettyCashApproverTable approvertable;
	Button btnadd;
	ObjectListBox<Employee> olbapprovername;
	IntegerBox ibamountupto;
	
	public PettyCashForm(SuperTable<PettyCash> table, int mode,
			boolean captionmode) {
		super(table, mode, captionmode);
		createGui();
		approvertable.connectToLocal();
	}

	private void initalizeWidget()
	{

		ibpettycashid=new IntegerBox();
		ibpettycashid.setEnabled(false);
		tbpettycashname=new TextBox();
		olbbranch=new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbbranch);
		cbstatus=new CheckBox();
		cbstatus.setValue(true);
		olbpersonresponsible=new ObjectListBox<Employee>();
		AppUtility.makeSalesPersonListBoxLive(olbpersonresponsible);
		btnadd=new Button("ADD");
		approvertable=new PettyCashApproverTable();
		olbapprovername=new ObjectListBox<Employee>();
		AppUtility.makeSalesPersonListBoxLive(olbapprovername);
		ibamountupto=new IntegerBox();
	}

	/*
	 * Method template to create the formtable screen
	 */

	@Override
	public void createScreen() {

		//Token to initialize the processlevel menus.
		initalizeWidget();

		//////////////////////////////////Form Field Declaration/////////////////////////////////////////////////

		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////

		//Token to initialize formfield
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingPettyCashInformation=fbuilder.setlabel("Petty Cash Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Petty Cash ID",ibpettycashid);
		FormField fibpettycashid= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Name",tbpettycashname);
		FormField ftbpettycashname= fbuilder.setMandatory(true).setMandatoryMsg("Name is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Person Responsible",olbpersonresponsible);
		FormField folbemployee= fbuilder.setMandatory(true).setMandatoryMsg("Person Responsible is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Status",cbstatus);
		FormField fcbstatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Branch",olbbranch);
		FormField folbbranch= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingApproverInformation=fbuilder.setlabel("Approver Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Amount Upto",ibamountupto);
		FormField fibamountupto= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Approver Name",olbapprovername);
		FormField folbapprovername= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",btnadd);
		FormField fbtnadd= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",approvertable.getTable());
		FormField fapprovertable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////


		FormField[][] formfield = {   {fgroupingPettyCashInformation},
				{fibpettycashid,ftbpettycashname,folbemployee,fcbstatus},
				{folbbranch},
				{fgroupingApproverInformation},
				{fibamountupto,folbapprovername,fbtnadd},
				{fapprovertable}
		};


		this.fields=formfield;		
	}

	/**
	 * method template to update the model with token entity name
	 */
	@Override
	public void updateModel(PettyCash model) 
	{

		if(tbpettycashname.getValue()!=null)
			model.setPettyCashName(tbpettycashname.getValue());
		if(olbpersonresponsible.getValue()!=null)
			model.setEmployee(olbpersonresponsible.getValue());
		if(cbstatus.getValue()!=null)
			model.setPettyCashStatus(cbstatus.getValue());
		if(olbbranch.getValue()!=null)
			model.setBranch(olbbranch.getValue());
		List<ApproverAmount> approverDetails=approvertable.getDataprovider().getList();
		System.out.println("Sizeeeeee"+approverDetails.size());
	    ArrayList<ApproverAmount> alloc = new ArrayList<ApproverAmount>();
	    alloc.addAll(approverDetails);
		model.setAmountApproved(alloc);


		presenter.setModel(model);
	}

	/**
	 * method template to update the view with token entity name
	 */
	@Override
	public void updateView(PettyCash view) 
	{
		ibpettycashid.setValue(view.getCount());
		if(view.getPettyCashName()!=null)
			tbpettycashname.setValue(view.getPettyCashName());
		if(view.getEmployee()!=null)
			olbpersonresponsible.setValue(view.getEmployee());
			cbstatus.setValue(view.isPettyCashStatus());
		if(view.getBranch()!=null)
			olbbranch.setValue(view.getBranch());
		approvertable.setValue(view.getAmountApproved());


		presenter.setModel(view);

	}

	/**
	 * Toggles the app header bar menus as per screen state
	 */

	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.PETTYCASH,LoginPresenter.currentModule.trim());
	}

	/**
	 * sets the id textbox with the passed count value. 
	 */
	@Override
	public void setCount(int count)
	{
		this.ibpettycashid.setValue(presenter.getModel().getCount());
	}

	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		ibpettycashid.setEnabled(false);
		cbstatus.setValue(true);
		approvertable.setEnable(state);
	}

	@Override
	public void clear() {
			super.clear();
			approvertable.clear();
	}
	
	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<ArrayList<ApproverAmount>> handler) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void fireEvent(GwtEvent<?> event) {
		// TODO Auto-generated method stub
	}

	@Override
	public ArrayList<ApproverAmount> getValue() {
		List<ApproverAmount>list=this.approvertable.getDataprovider().getList();
		ArrayList<ApproverAmount>vec=new ArrayList<ApproverAmount>();
		vec.addAll(list);
		return vec;
	}

	@Override
	public void setValue(ArrayList<ApproverAmount> value) {
		approvertable.connectToLocal();
		approvertable.getDataprovider().getList().addAll(value);
	}

	@Override
	public void setValue(ArrayList<ApproverAmount> value, boolean fireEvents) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public boolean validate() {
		boolean superValidate=super.validate();
		boolean tableValidate=true;
		if(this.approvertable.getDataprovider().getList().size()==0)
		{
			showDialogMessage("Please add at least one approver information !");
			tableValidate=false;
			
		}
		return tableValidate && superValidate;
	}
	
	
	

}
