package com.slicktechnologies.client.views.account.pettycash;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.account.pettycash.PettyCashPresenter.PettyCashPresenterTable;
import com.slicktechnologies.shared.PettyCash;
import com.slicktechnologies.shared.common.helperlayer.Config;

public class PettyCashPresenterTableProxy extends PettyCashPresenterTable {
	
	
	TextColumn<PettyCash> getPettyCashNameColumn;
	TextColumn<PettyCash> getPersonResponsibleColumn;
	TextColumn<PettyCash> getStatusColumn;
	TextColumn<PettyCash> getBranchColumn;
	TextColumn<PettyCash> getCountColumn;
	
	
	public PettyCashPresenterTableProxy()
	{
		super();
	}
	
	@Override public void createTable() {
		addColumngetCount();
		addColumngetName();
		addColumngetPersonResponsible();
		addColumngetBranch();
		addColumngetStatus();
		
	}
	@Override
	protected void initializekeyprovider() {
		keyProvider= new ProvidesKey<PettyCash>()
				{
			@Override
			public Object getKey(PettyCash item)
			{
				if(item==null)
				{
					return null;
				}
				else
					return item.getId();
			}
				};
	}
	@Override
	public void setEnable(boolean state)
	{
	}
	@Override
	public void applyStyle()
	{
	}

	public void addColumnSorting(){
		addSortinggetCount();
		addSortinggetName();
		addSortinggetPersonResponsible();
		addSortinggetStatus();
		addSortinggetBranch();
	}
	@Override public void addFieldUpdater() {
	}
	protected void addSortinggetCount()
	{
		List<PettyCash> list=getDataprovider().getList();
		columnSort=new ListHandler<PettyCash>(list);
		columnSort.setComparator(getCountColumn, new Comparator<PettyCash>()
				{
			@Override
			public int compare(PettyCash e1,PettyCash e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetCount()
	{
		getCountColumn=new TextColumn<PettyCash>()
				{
			@Override
			public String getValue(PettyCash object)
			{
				if( object.getCount()==-1)
					return "N.A";
				else return object.getCount()+"";
			}
				};
				table.addColumn(getCountColumn,"Id");
				getCountColumn.setSortable(true);
	}
	
	
	protected void addSortinggetName()
	{
		List<PettyCash> list=getDataprovider().getList();
		columnSort=new ListHandler<PettyCash>(list);
		columnSort.setComparator(getPettyCashNameColumn, new Comparator<PettyCash>()
				{
			@Override
			public int compare(PettyCash e1,PettyCash e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getPettyCashName()!=null && e2.getPettyCashName()!=null){
						return e1.getPettyCashName().compareTo(e2.getPettyCashName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetName()
	{
		getPettyCashNameColumn=new TextColumn<PettyCash>()
				{
			@Override
			public String getValue(PettyCash object)
			{
				return object.getPettyCashName();
			}
				};
				table.addColumn(getPettyCashNameColumn,"Name");
				getPettyCashNameColumn.setSortable(true);
	}

	
	
	protected void addSortinggetPersonResponsible()
	{
		List<PettyCash> list=getDataprovider().getList();
		columnSort=new ListHandler<PettyCash>(list);
		columnSort.setComparator(getPersonResponsibleColumn, new Comparator<PettyCash>()
				{
			@Override
			public int compare(PettyCash e1,PettyCash e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getEmployee()!=null && e2.getEmployee()!=null){
						return e1.getEmployee().compareTo(e2.getEmployee());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetPersonResponsible()
	{
		getPersonResponsibleColumn=new TextColumn<PettyCash>()
				{
			@Override
			public String getValue(PettyCash object)
			{
				return object.getEmployee()+"";
			}
				};
				table.addColumn(getPersonResponsibleColumn,"Person Responsible");
				getPersonResponsibleColumn.setSortable(true);
	}

	
	
	protected void addSortinggetBranch()
	{
		List<PettyCash> list=getDataprovider().getList();
		columnSort=new ListHandler<PettyCash>(list);
		columnSort.setComparator(getBranchColumn, new Comparator<PettyCash>()
				{
			@Override
			public int compare(PettyCash e1,PettyCash e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBranch()!=null && e2.getBranch()!=null){
						return e1.getBranch().compareTo(e2.getBranch());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetBranch()
	{
		getBranchColumn=new TextColumn<PettyCash>()
				{
			@Override
			public String getValue(PettyCash object)
			{
				return object.getBranch()+"";
			}
				};
				table.addColumn(getBranchColumn,"Branch");
				getBranchColumn.setSortable(true);
	}
	
	protected void addSortinggetStatus()
	{
		List<PettyCash> list=getDataprovider().getList();
		columnSort=new ListHandler<PettyCash>(list);
		columnSort.setComparator(getStatusColumn, new Comparator<PettyCash>()
				{
					
					@Override
					public int compare(PettyCash e1, PettyCash e2) 
					{
						if(e1!=null && e2!=null)
						{
							if(e1.isPettyCashStatus()==e2.isPettyCashStatus())
								
							return 0;
							else if(e1.isPettyCashStatus()==true)
								return 1;
							else
								return -1;
						}
						else
							return -1;
					}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetStatus()
	{
		getStatusColumn=new TextColumn<PettyCash>()
				{
			@Override
			public String getValue(PettyCash object)
			{
				if(object.isPettyCashStatus()==true)
		           	return AppUtility.ACTIVE;
		           else
		           	return AppUtility.INACTIVE;
			}
				};
				table.addColumn(getStatusColumn,"Status");
				getStatusColumn.setSortable(true);
	}

}
