package com.slicktechnologies.client.views.account.pettycash;

import java.util.Date;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.view.client.ProvidesKey;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.PettyCash.ApproverAmount;

public class PettyCashApproverTable extends SuperTable<ApproverAmount> {
	
	TextColumn<ApproverAmount> amountUptoColumn;
	TextColumn<ApproverAmount> approverNameColumn;
	private Column<ApproverAmount, String> delete;
	
	public PettyCashApproverTable() {
		super();
	}
	
	
	@Override
	public void createTable() {
		addColumnAmountUpto();
		addColumnApproverName();
		addColumnDelete();
		setFieldUpdaterOnDelete();
	    table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
	}

	
	private void addeditColumn()
	{
		addColumnAmountUpto();
		addColumnApproverName();
		addColumnDelete();
		setFieldUpdaterOnDelete();
	}
	
	private void addViewColumn()
	{
		addColumnAmountUpto();
		addColumnApproverName();
	}
	
	
	public void addColumnAmountUpto()
	{
		amountUptoColumn=new TextColumn<ApproverAmount>() {
			@Override
			public String getValue(ApproverAmount object) {
				return object.getAmount()+"";
			}
		};
		table.addColumn(amountUptoColumn,"Amount");
	}

	public void addColumnApproverName()
	{
		approverNameColumn=new TextColumn<ApproverAmount>() {
			@Override
			public String getValue(ApproverAmount object) {
				return object.getEmployeeName();
			}
		};
		table.addColumn(approverNameColumn,"Approver Name");
	}
	
	
	  private void addColumnDelete()
		{
			ButtonCell btnCell= new ButtonCell();
			delete = new Column<ApproverAmount, String>(btnCell) {

				@Override
				public String getValue(ApproverAmount object) {
					
					return "Delete";
				}
			};
			table.addColumn(delete,"Delete");
		}

		private void setFieldUpdaterOnDelete()
		{
			delete.setFieldUpdater(new FieldUpdater<ApproverAmount, String>() {
				
				@Override
				public void update(int index, ApproverAmount object, String value) {
					getDataprovider().getList().remove(object);
				
				}
			});
		}

	
	
	
		public  void setEnabled(boolean state)
		{
			int tablecolcount=this.table.getColumnCount();
			for(int i=tablecolcount-1;i>-1;i--)
			  table.removeColumn(i);
			if(state ==true)
				addeditColumn();
			if(state==false)
				addViewColumn();
		}

	

	@Override
	protected void initializekeyprovider() {
		keyProvider = new ProvidesKey<ApproverAmount>() {
			@Override
			public Object getKey(ApproverAmount item) {
				if(item==null)
					return null;
				else
					return new Date().getTime();
			}
		};
	
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		int tablecolcount=this.table.getColumnCount();
		for(int i=tablecolcount-1;i>-1;i--)
		  table.removeColumn(i);
		if(state ==true)
			addeditColumn();
		if(state==false)
			addViewColumn();
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
