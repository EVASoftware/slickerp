package com.slicktechnologies.client.views.salespersontargetachievement;

import java.util.Date;

import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.salesperson.TargetInformation;

public class SalesPersonTargetAchievementTable extends SuperTable<TargetInformation>{

	TextColumn<TargetInformation> getcolumnDuration;
	TextColumn<TargetInformation> getcolumnTarget;
	TextColumn<TargetInformation> getcolumnpercentage;
	TextColumn<TargetInformation> getcolumnflatAmount;
	TextColumn<TargetInformation> getcolumnproductCategory;
	TextColumn<TargetInformation> getcolumnfinancialyear;
	TextColumn<TargetInformation> getColumnAchievedTarget;
	TextColumn<TargetInformation> getcolumnEmployeeName;
	TextColumn<TargetInformation> getcolumnMonths;
	TextColumn<TargetInformation> getcolumnSalesPeronIncentivesAmt;


	
	SalesPersonTargetAchievementTable(){
		super();
		setHeight("400px");
	}
	
	@Override
	public void createTable() {

		addcolumnEmployeeName();
		addcolumnProductCategory();
		addcolumnFinancialYear();
		addcolumnDuration();
		addcolumnMonths();
		addcolumnTarget();
		addcolumnPercenatge();
		addcolumnflatAmount();
		addcolumnAchievedTarget();
		addcolumnSalespersonIncentivesAmt();
	}

	private void addcolumnSalespersonIncentivesAmt() {

		getcolumnSalesPeronIncentivesAmt = new TextColumn<TargetInformation>() {
			
			@Override
			public String getValue(TargetInformation object) {
				return object.getEmployeeIncentives()+"";
			}
		};
		table.addColumn(getcolumnSalesPeronIncentivesAmt,"Incentive");
		table.setColumnWidth(getcolumnSalesPeronIncentivesAmt, 110,Unit.PX);
	}

	private void addcolumnMonths() {
		getcolumnMonths = new TextColumn<TargetInformation>() {
			
			@Override
			public String getValue(TargetInformation object) {
				return object.getMonths();
			}
		};
		table.addColumn(getcolumnMonths,"Months");
		table.setColumnWidth(getcolumnMonths, 110,Unit.PX);
	}
	
	private void addcolumnEmployeeName() {

		getcolumnEmployeeName = new TextColumn<TargetInformation>() {
			
			@Override
			public String getValue(TargetInformation object) {
				return object.getEmployeeName();
			}
		};
		table.addColumn(getcolumnEmployeeName,"Employee Name");
		table.setColumnWidth(getcolumnEmployeeName, 90,Unit.PX);
	}

	private void addcolumnAchievedTarget() {

		getColumnAchievedTarget = new TextColumn<TargetInformation>() {
			@Override
			public String getCellStyleNames(Context context,TargetInformation object) {
				if(object.getAchievedTargetAmt()>=object.getTarget()){
					 return "green";
				 }
				else {
					 return "red";
				 }
			}

			@Override
			public String getValue(TargetInformation object) {
				return object.getAchievedTargetAmt()+"";
			}
		};
		table.addColumn(getColumnAchievedTarget,"Actual");
		table.setColumnWidth(getColumnAchievedTarget, 90,Unit.PX);
	}

	private void  addcolumnFinancialYear() {
		getcolumnfinancialyear = new TextColumn<TargetInformation>() {
			
			@Override
			public String getValue(TargetInformation object) {
				return object.getFinancialYear();
			}
		};
		
		table.addColumn(getcolumnfinancialyear,"Financial Year");
		table.setColumnWidth(getcolumnfinancialyear, 100,Unit.PX);
	}



	private void addcolumnDuration() {
		getcolumnDuration = new TextColumn<TargetInformation>() {
			
			@Override
			public String getValue(TargetInformation object) {
				return object.getDuration();
			}
		};
		table.addColumn(getcolumnDuration,"Duration");
		table.setColumnWidth(getcolumnDuration, 90,Unit.PX);
	}



	private void addcolumnProductCategory() {

		getcolumnproductCategory = new TextColumn<TargetInformation>() {
			
			@Override
			public String getValue(TargetInformation object) {
				return object.getProductCategory();
			}
		};
		table.addColumn(getcolumnproductCategory,"Product Category");
		table.setColumnWidth(getcolumnproductCategory, 90,Unit.PX);
	}



	private void addcolumnTarget() {

		getcolumnTarget = new TextColumn<TargetInformation>() {
			
			@Override
			public String getValue(TargetInformation object) {
				return object.getTarget()+"";
			}
		};
		table.addColumn(getcolumnTarget,"Target");
		table.setColumnWidth(getcolumnTarget, 90,Unit.PX);
	}



	private void addcolumnPercenatge() {

		getcolumnpercentage = new TextColumn<TargetInformation>() {
			
			@Override
			public String getValue(TargetInformation object) {
				return object.getPercatage()+"";
			}
		};
		table.addColumn(getcolumnpercentage,"Incentives %");
		table.setColumnWidth(getcolumnpercentage, 80,Unit.PX);
	}

	private void addcolumnflatAmount() {
		
		getcolumnflatAmount = new TextColumn<TargetInformation>() {
			
			@Override
			public String getValue(TargetInformation object) {
				return object.getFlatAmount()+"";
			}
		};
		
		table.addColumn(getcolumnflatAmount,"Incentives Amt");
		table.setColumnWidth(getcolumnflatAmount, 80,Unit.PX);	
		
		
	}
	
	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
