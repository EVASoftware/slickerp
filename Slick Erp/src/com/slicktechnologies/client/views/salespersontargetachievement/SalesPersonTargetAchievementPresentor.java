package com.slicktechnologies.client.views.salespersontargetachievement;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.SalesPersonAchievementService;
import com.slicktechnologies.client.services.SalesPersonAchievementServiceAsync;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.DummyEntityOnlyForScreen;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.salesperson.SalesPersonTargets;
import com.slicktechnologies.shared.common.salesperson.TargetInformation;

public class SalesPersonTargetAchievementPresentor extends FormScreenPresenter<DummyEntityOnlyForScreen> implements ClickHandler{

	SalesPersonTargetAchievementForm form;
	final GenricServiceAsync async =GWT.create(GenricService.class);

	final SalesPersonAchievementServiceAsync salespersonAsync = GWT.create(SalesPersonAchievementService.class);
	
	CsvServiceAsync csvservice=GWT.create(CsvService.class);

	
	public SalesPersonTargetAchievementPresentor(UiScreen<DummyEntityOnlyForScreen> view, DummyEntityOnlyForScreen model) {
		super(view, model);
		form = (SalesPersonTargetAchievementForm) view;
		form.btnemployeeGo.addClickHandler(this);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.SALESPERSONPERFORMANCE,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
		
		}

	public static void initalize(){
		
		SalesPersonTargetAchievementForm form = new SalesPersonTargetAchievementForm();
		AppMemory.getAppMemory().currentScreen = Screen.SALESPERSONPERFORMANCE;
		SalesPersonTargetAchievementPresentor presentor = new SalesPersonTargetAchievementPresentor(form, new DummyEntityOnlyForScreen());
		AppMemory.getAppMemory().stickPnel(form);
		
	}
	
	@Override
	public void onClick(ClickEvent event) {
		super.onClick(event);
		
		if(event.getSource()==form.btnemployeeGo){
			System.out.println(" inside employee go btn");
			gettarAchievementResultInfo();
		}
	}

	private void gettarAchievementResultInfo() {
		System.out.println(" employee id =="+form.employeeInfo.getEmployeeId());
		String financialyear = form.olbcFinancialCalender.getValue(form.olbcFinancialCalender.getSelectedIndex());
		if(form.olbcFinancialCalender.getSelectedIndex()==0){
			form.showDialogMessage("Please Select Financial Year");
		}
		else if(form.olbcProductCategory.getSelectedIndex()!=0 && form.olbemployeeRole.getSelectedIndex()!=0 && form.employeeInfo.getEmployeeId()!=-1){
			form.showDialogMessage("Please Select either Product Category or Employee or Employee Role");
		}
		else if(form.olbcProductCategory.getSelectedIndex()!=0){
			
			if(form.olbemployeeRole.getSelectedIndex()!=0 || form.employeeInfo.getEmployeeId()!=-1){
				form.showDialogMessage("Please Select either Product Category or Employee or Employee Role");

			}else{
				String productCategory = form.olbcProductCategory.getValue(form.olbcProductCategory.getSelectedIndex());
				salespersonAsync.getProductCategoryDetails(financialyear, productCategory, model.getCompanyId(), new AsyncCallback<ArrayList<TargetInformation>>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void onSuccess(ArrayList<TargetInformation> result) {
						form.achivementTable.clear();
						if(result.size()==0){
							form.showDialogMessage("No Result To Display");
						}else{
							form.achivementTable.getDataprovider().setList(result);

						}
					}
				});
			}
			
		}
		else if(form.olbemployeeRole.getSelectedIndex()!=0){
			if(form.olbcProductCategory.getSelectedIndex()!=0 || form.employeeInfo.getEmployeeId()!=-1){
				form.showDialogMessage("Please Select either Product Category or Employee or Employee Role");
			}else{
				String employeeRole = form.olbemployeeRole.getValue(form.olbemployeeRole.getSelectedIndex());
				salespersonAsync.getemployeeRoleDetails(financialyear, employeeRole, model.getCompanyId(), new AsyncCallback<ArrayList<TargetInformation>>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void onSuccess(ArrayList<TargetInformation> result) {
						form.achivementTable.clear();
						if(result.size()==0){
							form.showDialogMessage("No Result To Display");
						}else{
							form.achivementTable.getDataprovider().setList(result);

						}
					}
				});
				
			}
			
		}
		else{
			
			String employeeeName = form.employeeInfo.getEmployeeName();
			salespersonAsync.getAchievementDetails(form.employeeInfo.getEmployeeId(), financialyear, model.getCompanyId(),employeeeName, new AsyncCallback<ArrayList<TargetInformation>>() {

				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void onSuccess(ArrayList<TargetInformation> result) {
					System.out.println(" result size =="+result);
					
					form.achivementTable.clear();
					if(result.size()==0){
						form.showDialogMessage("No Result To Display");
					}else{
						form.achivementTable.getDataprovider().setList(result);

					}
					
				}
			});
			
		}
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void makeNewModel() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnDownload() {
		
		List<TargetInformation> list = form.achivementTable.getDataprovider().getList();
		
		ArrayList<TargetInformation> targetactualArray = new ArrayList<TargetInformation>();
		
		for(int i=0;i<list.size();i++){
			
			TargetInformation targetinfo = new TargetInformation();
			
			targetinfo.setEmployeeName(list.get(i).getEmployeeName());
			if(list.get(i).getProductCategory()!=null){
				targetinfo.setProductCategory(list.get(i).getProductCategory());
			}
			targetinfo.setFinancialYear(list.get(i).getFinancialYear());
			targetinfo.setDuration(list.get(i).getDuration());
			targetinfo.setMonths(list.get(i).getMonths());
			targetinfo.setTarget(list.get(i).getTarget());
			targetinfo.setPercatage(list.get(i).getPercatage());
			targetinfo.setFlatAmount(list.get(i).getFlatAmount());
			targetinfo.setAchievedTargetAmt(list.get(i).getAchievedTargetAmt());
			targetinfo.setEmployeeIncentives(list.get(i).getEmployeeIncentives());
			targetactualArray.add(targetinfo);
			
		}
		
		String financialyear = form.olbcFinancialCalender.getValue(form.olbcFinancialCalender.getSelectedIndex());
		
		csvservice.setSalesPersonTargetPerformanceReport(targetactualArray, financialyear, new AsyncCallback<Void>() {
			
			@Override
			public void onSuccess(Void result) {
				// TODO Auto-generated method stub
				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "csvservlet"+"?type="+110;
				Window.open(url, "test", "enabled");
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
		
		

	}
	
	
}
