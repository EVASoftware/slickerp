package com.slicktechnologies.client.views.salespersontargetachievement;

import java.util.Vector;

import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.composites.EmployeeInfoComposite;
import com.slicktechnologies.shared.FinancialCalender;
import com.slicktechnologies.shared.common.DummyEntityOnlyForScreen;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.productlayer.Category;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class SalesPersonTargetAchievementForm extends FormScreen<DummyEntityOnlyForScreen>{

	SalesPersonTargetAchievementTable achivementTable;
	
	public EmployeeInfoComposite employeeInfo;
	
	ObjectListBox<FinancialCalender> olbcFinancialCalender;
	
	ObjectListBox<Config> olbemployeeRole;
	ObjectListBox<Category> olbcProductCategory;
	Button btnemployeeGo;
	
	Label lbblank;
	
	public SalesPersonTargetAchievementForm(){
		super();
		createGui();
		achivementTable.connectToLocal();
	}
	

	private void initalizeWidget() {

		employeeInfo=AppUtility.employeeInfoComposite(new EmployeeInfo(),false);

		achivementTable = new SalesPersonTargetAchievementTable();
		
		olbcFinancialCalender = new ObjectListBox<FinancialCalender>();
		initializeCalendarBox();
		
		
		olbemployeeRole=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbemployeeRole,Screen.EMPLOYEEROLE);
		
		olbcProductCategory=new ObjectListBox<Category>();
		initalizeCategoryComboBox();
		
		btnemployeeGo = new Button("Go");
		lbblank = new Label();
	}

	
	private void initalizeCategoryComboBox() {

		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new Category());
		Filter categoryFilter = new Filter();
		categoryFilter.setQuerryString("isServiceCategory");
		categoryFilter.setBooleanvalue(true);
		Filter statusfilter=new Filter();
		   statusfilter.setQuerryString("status");
		   statusfilter.setBooleanvalue(true);
		querry.getFilters().add(categoryFilter);
		querry.getFilters().add(statusfilter);
		this.olbcProductCategory.MakeLive(querry);
	}



	private void initializeCalendarBox() {

		MyQuerry querry=new MyQuerry();
		Vector<Filter> filervec = new Vector<Filter>();
		
		Filter filter = null;
		
		filter=new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(UserConfiguration.getCompanyId());
		filervec.add(filter);
		
		querry.setFilters(filervec);
		querry.setQuerryObject(new FinancialCalender());

		this.olbcFinancialCalender.MakeLive(querry);
	}
	

	@Override
	public void createScreen() {
		initalizeWidget();
		
		FormFieldBuilder fbuilder;
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingEmployeeInformation=fbuilder.setlabel("Sales Person Search").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",employeeInfo);
		FormField femployeeInfo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Financial Year",olbcFinancialCalender);
		FormField folbcFinancialCalender= fbuilder.setMandatory(false).setRowSpan(1).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",btnemployeeGo);
		FormField fbtnGo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("",lbblank);
		FormField flbblank= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingEmployeeRoleInformation=fbuilder.setlabel("Target Actual Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Employee Role",olbemployeeRole);
		FormField folbemployeeRole= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("",achivementTable.getTable());
		FormField fachivementTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Product Category",olbcProductCategory);
		FormField folbcProductCategory= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		FormField[][] formFields = {   
				{fgroupingEmployeeInformation},
				{folbcFinancialCalender,folbcProductCategory,folbemployeeRole},
				{femployeeInfo},
				{fbtnGo,flbblank},
				{fgroupingEmployeeRoleInformation},
				{fachivementTable}
				
		};
		this.fields=formFields;
		
	}

	
	


	@Override
	public void toggleAppHeaderBarMenu() {

		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Download"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Download"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.SALESPERSONPERFORMANCE,LoginPresenter.currentModule.trim());
		
	}
	
	@Override
	public void updateModel(DummyEntityOnlyForScreen model) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateView(DummyEntityOnlyForScreen model) {
		// TODO Auto-generated method stub
		
	}

}
