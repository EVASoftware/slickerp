package com.slicktechnologies.client.views.quickcontract;

import java.util.ArrayList;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.AddressComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.EmailTextBox;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;

public class NewCustomerPopup extends PopupScreen implements ValueChangeHandler<String>{

	
	TextBox tbCompanyName;
	TextBox tbFullName;
	
	EmailTextBox etbEmail;
	TextBox tbCellNo;
	ObjectListBox<Branch> olbbBranch;
	AddressComposite addressComp;
	TextBox tbGstNumber;
	
	public NewCustomerPopup(){
		super();
		createGui();
	}
	
	private void initializeWidgets() {
		tbCompanyName = new TextBox();
		tbCompanyName.addValueChangeHandler(this);
		tbFullName = new TextBox();
		tbFullName.addValueChangeHandler(this);
		etbEmail = new EmailTextBox();
		olbbBranch = new ObjectListBox<Branch>();
		AppUtility.makeCustomerBranchListBoxLive(olbbBranch);
		addressComp = new AddressComposite(true);
		tbGstNumber = new TextBox();
		tbCellNo = new TextBox();
		
	}

	
	@Override
	public void createScreen() {
		initializeWidgets();
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fcustomerGroping = fbuilder.setlabel("Customer Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new  FormFieldBuilder("Company Name",tbCompanyName);
		FormField ftbCompanyName = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Full Name",tbFullName);
		FormField ftbFullName = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Email",etbEmail);
		FormField fetbEmail = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Cell Number",tbCellNo);
		FormField ftbCellNo = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Branch",olbbBranch);
		FormField folbbBranch = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("GST Number",tbGstNumber);
		FormField ftbGstNumber = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fcustomerAddressGroping = fbuilder.setlabel("Customer Billing Address").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
//		fbuilder = new FormFieldBuilder("",addressComp);
//		FormField faddressComp = fbuilder.setMandatory(false).setColSpan(0).setRowSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",addressComp);
		FormField faddressComp= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		FormField formfied [][] = {
				{fcustomerGroping},
				{ftbCompanyName,ftbFullName,fetbEmail,ftbCellNo},
				{folbbBranch,ftbGstNumber},
				{fcustomerAddressGroping},
				{faddressComp},
				
		};
		this.fields = formfied;
		
	}
	
	
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
		ScreeenState currentScreen= AppMemory.getAppMemory().currentState;
		if(currentScreen.equals(ScreeenState.NEW)){
			if(event.getSource().equals(tbFullName)){
				validateCustomer();
			}
			
			if(event.getSource().equals(tbCompanyName)){
				validateCompanyName();
			}
		}
		
	}
	
	
	

	private void validateCustomer(){
		final GenricServiceAsync async = GWT.create(GenricService.class);

		if(!getTbFullName().getValue().equals("")){
			String custnameval=getTbFullName().getValue().trim();
			custnameval=custnameval.toUpperCase().trim();
			if(custnameval!=null){

					final MyQuerry querry=new MyQuerry();
					Vector<Filter> filtervec=new Vector<Filter>();
					Filter temp=null;
					
					
					temp=new Filter();
					temp.setQuerryString("isCompany");
					if(!getTbCompanyName().getValue().equals("")){
						System.out.println(" value =="+getTbCompanyName().getValue());
						temp.setBooleanvalue(true);
					}
					if(getTbCompanyName().getValue().equals("")){
						temp.setBooleanvalue(false);
						System.out.println("valueeeee =="+getTbCompanyName().getValue());
					}
					filtervec.add(temp);
					
					temp=new Filter();
					temp.setQuerryString("companyId");
					temp.setLongValue(UserConfiguration.getCompanyId());
					filtervec.add(temp);
					
					temp=new Filter();
					temp.setQuerryString("fullname");
					temp.setStringValue(custnameval);
					filtervec.add(temp);
					
					querry.setFilters(filtervec);
					querry.setQuerryObject(new Customer());
					async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {

							@Override
							public void onFailure(Throwable caught) {
								final GWTCAlert alert = new GWTCAlert(); 
							     alert.alert("An Unexpected Error occured !");
							}
							@Override
							public void onSuccess(ArrayList<SuperModel> result) {
								System.out.println("Success"+result.size());
								if(result.size()!=0){
									final GWTCAlert alert = new GWTCAlert(); 
								     alert.alert("Customer Name Already Exists");
								     
								     getTbFullName().setValue("");
								}
							}
						});
			}
		}
	}

	
	private void validateCompanyName() {

		final GenricServiceAsync async = GWT.create(GenricService.class);

		if(getTbCompanyName().getValue().trim()!=null){
			final MyQuerry querry=new MyQuerry();
			Vector<Filter> filtervec=new Vector<Filter>();
			Filter temp=null;
			
			temp=new Filter();
			temp.setQuerryString("isCompany");
			temp.setBooleanvalue(true);
			filtervec.add(temp);
			
			temp=new Filter();
			temp.setQuerryString("companyName");
			temp.setStringValue(getTbCompanyName().getValue().toUpperCase().trim());
			filtervec.add(temp);
			
			temp=new Filter();
			temp.setQuerryString("companyId");
			temp.setLongValue(UserConfiguration.getCompanyId());
			filtervec.add(temp);
			
			querry.setFilters(filtervec);
			querry.setQuerryObject(new Customer());
			async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {

					@Override
					public void onFailure(Throwable caught) {
						 final GWTCAlert alert = new GWTCAlert(); 
					     alert.alert("An Unexpected Error occured !");
						
					}
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						System.out.println("Success"+result.size());
						if(result.size()!=0){
							 final GWTCAlert alert = new GWTCAlert(); 
						     alert.alert("Company Name Already Exists!");
						     getTbCompanyName().setValue("");
						}
					}
					
				});
		}

	
	}

	public TextBox getTbCompanyName() {
		return tbCompanyName;
	}

	public void setTbCompanyName(TextBox tbCompanyName) {
		this.tbCompanyName = tbCompanyName;
	}

	public TextBox getTbFullName() {
		return tbFullName;
	}

	public void setTbFullName(TextBox tbFullName) {
		this.tbFullName = tbFullName;
	}
	
	public EmailTextBox getEtbEmail() {
		return etbEmail;
	}

	public void setEtbEmail(EmailTextBox etbEmail) {
		this.etbEmail = etbEmail;
	}

	public TextBox getTbCellNo() {
		return tbCellNo;
	}

	public void setTbCellNo(TextBox tbCellNo) {
		this.tbCellNo = tbCellNo;
	}

	public ObjectListBox<Branch> getOlbbBranch() {
		return olbbBranch;
	}

	public void setOlbbBranch(ObjectListBox<Branch> olbbBranch) {
		this.olbbBranch = olbbBranch;
	}

	public AddressComposite getAddressComp() {
		return addressComp;
	}

	public void setAddressComp(AddressComposite addressComp) {
		this.addressComp = addressComp;
	}

	public TextBox getTbGstNumber() {
		return tbGstNumber;
	}

	public void setTbGstNumber(TextBox tbGstNumber) {
		this.tbGstNumber = tbGstNumber;
	}
	

}
