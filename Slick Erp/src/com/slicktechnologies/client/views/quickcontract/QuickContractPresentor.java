package com.slicktechnologies.client.views.quickcontract;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.Vector;
import java.util.logging.Level;

import org.apache.poi.hssf.record.DBCellRecord;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.google.gwt.view.client.RowCountChangeEvent.Handler;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.reusabledata.ConditionDialogBox;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.EmailService;
import com.slicktechnologies.client.services.EmailServiceAsync;
import com.slicktechnologies.client.services.MaterialConsumptionService;
import com.slicktechnologies.client.services.MaterialConsumptionServiceAsync;
import com.slicktechnologies.client.services.SmsService;
import com.slicktechnologies.client.services.SmsServiceAsync;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.services.UpdateService;
import com.slicktechnologies.client.services.UpdateServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.ApproverFactory;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utility.UnitConversionUtility;
import com.slicktechnologies.client.views.complain.ServiceDateBoxPopup;
import com.slicktechnologies.client.views.contract.CancellationPopUp;
import com.slicktechnologies.client.views.contract.ContractPresenter.ContractPresenterSearch;
import com.slicktechnologies.client.views.contract.ContractPresenter.ContractPresenterTable;
import com.slicktechnologies.client.views.contract.ContractForm;
import com.slicktechnologies.client.views.contract.ContractPresenter;
import com.slicktechnologies.client.views.contract.ContractPresenterSearchProxy;
import com.slicktechnologies.client.views.contract.ContractPresenterTableProxy;
import com.slicktechnologies.client.views.contract.ContractService;
import com.slicktechnologies.client.views.contract.ContractServiceAsync;
import com.slicktechnologies.client.views.contract.SalesLineItemTable;
import com.slicktechnologies.client.views.documentcancellation.CancellationSummary;
import com.slicktechnologies.client.views.documentcancellation.CancellationSummaryPopUp;
import com.slicktechnologies.client.views.documentcancellation.CancellationSummaryPopUpTable;
import com.slicktechnologies.client.views.documentcancellation.DocumentCancellationPopUp;
import com.slicktechnologies.client.views.materialconsumptionreport.MaterialConsumptionReportForm;
import com.slicktechnologies.client.views.materialconsumptionreport.MaterialConsumptionReportPresenter;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.BillingDetailsForm;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.BillingDetailsPresenter;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.FromAndToDateBoxPopup;
import com.slicktechnologies.client.views.paymentinfo.billinglist.BillingListPresenter;
import com.slicktechnologies.client.views.popups.NewEmailPopUp;
import com.slicktechnologies.client.views.popups.ServiceCancellationPopup;
import com.slicktechnologies.client.views.project.concproject.ProjectForm;
import com.slicktechnologies.client.views.project.concproject.ProjectPresenter;
import com.slicktechnologies.client.views.quicksalesorder.ViewInvoicePaymentDeliveryNotePopup;
import com.slicktechnologies.client.views.scheduleservice.ServiceSchedulePopUp;
import com.slicktechnologies.client.views.service.ServicePresenter;
import com.slicktechnologies.client.views.workorder.WorkOrderForm;
import com.slicktechnologies.client.views.workorder.WorkOrderPresenter;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.BranchWiseScheduling;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Sales;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.TaxDetails;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cancelsummary.CancelSummary;
import com.slicktechnologies.shared.common.contractcancel.CancelContract;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.inventory.BillOfMaterial;
import com.slicktechnologies.shared.common.inventory.MaterialConsumptionReport;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.inventory.MaterialMovementNote;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.role.ScreenAuthorization;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.SalesOrderProductLineItem;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;
import com.slicktechnologies.shared.common.servicerelated.ClientSideAsset;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;
import com.slicktechnologies.shared.common.servicerelated.ServiceSchedule;
import com.slicktechnologies.shared.common.workorder.WorkOrder;
import com.slicktechnologies.client.utils.Console;

public class QuickContractPresentor extends FormScreenPresenter<Contract> implements Handler, ChangeHandler, ValueChangeHandler<Integer>, SelectionHandler<Suggestion>, ClickHandler {

	public static  QuickContractForm form;
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	EmailServiceAsync emailService=GWT.create(EmailService.class);
	final ContractServiceAsync async=GWT.create(ContractService.class);
	static List<CancellationSummary> cancellis=new ArrayList<CancellationSummary>();
	CancellationSummaryPopUp summaryPopup1 = new CancellationSummaryPopUp();
	PopupPanel summaryPanel;
	DocumentCancellationPopUp popup = new DocumentCancellationPopUp();
	PopupPanel cancelpanel ;
	final GenricServiceAsync genasync=GWT.create(GenricService.class);
	final MaterialConsumptionServiceAsync consumptionAsync=GWT.create(MaterialConsumptionService.class);
	ArrayList<MaterialConsumptionReport> consumptionList=new ArrayList<MaterialConsumptionReport>();
	
	GeneralServiceAsync quickcontractserviceimpl = GWT.create(GeneralService.class);
	
	//************changes changes here ****************
		ConditionDialogBox conditionPopup=new ConditionDialogBox("Do you want to print on preprinted Stationery",AppConstants.YES,AppConstants.NO);
		PopupPanel panel ;
	//****************ends here ***********	
	
	ServiceSchedulePopUp serviceSchedulePopUp=new ServiceSchedulePopUp("Contract Id", "Contract Date");
	PopupPanel schedulePanel;
	DateTimeFormat format = DateTimeFormat.getFormat("c");
	public static int custcount=0;
	public static  int specificday=0;
	
/************************* updation Code *********************/
	
	FromAndToDateBoxPopup frmAndToPop=new FromAndToDateBoxPopup();
	ArrayList<TaxDetails> vattaxlist;
	ArrayList<TaxDetails> servicetaxlist;	
				
	int cnt=0;
	int pestoIndia=0;
	int vcare=0;
	
	
	ViewInvoicePaymentServicePopUp invoicePaymentPopup;
	PopupPanel invoicePaymentPanel;
	
 	/** Date 28-02-2018 By vijay for New customer popup ****/
	NewCustomerPopup newcustomerPopup = new NewCustomerPopup();
	ServiceCancellationPopup  cancellation = new ServiceCancellationPopup();
	boolean contractDisContinue = false;
	UpdateServiceAsync update = GWT.create(UpdateService.class);
	CancellationPopUp cancellationPopup = new CancellationPopUp();
	
	NewEmailPopUp emailpopup = new NewEmailPopUp();
	
	/**
	 * @author Anil @since 18-05-2021
	 * Submit flag to avoid duplicate creation of bill, invoice, payment and services.
	 * raised by Rahul Tiwari for ecopesterp
	 */
	boolean submitFlag=false;
	boolean accessPayments=false; //Ashwini Patil Date:8-06-2023 to restrict access to payment as per user authorization
	ServiceDateBoxPopup otpPopup=new ServiceDateBoxPopup("OTP"); //Ashwini Patil Date:5-07-2023 to restrict download with otp for hi tech client
	PopupPanel otppopupPanel;
	int generatedOTP;
	SmsServiceAsync smsService = GWT.create(SmsService.class);
	public QuickContractPresentor(FormScreen<Contract> view, Contract model) {
		super(view, model);
		form = (QuickContractForm) view;
		form.setPresenter(this);
		
		form.getSaleslineitemtable().getTable().addRowCountChangeHandler(this);
		form.getChargesTable().getTable().addRowCountChangeHandler(this);
		form.getPersonInfoComposite().getId().addSelectionHandler(this);
		form.getPersonInfoComposite().getName().addSelectionHandler(this);
		form.getPersonInfoComposite().getPhone().addSelectionHandler(this);
		popup.getBtnOk().addClickHandler(this);
		popup.getBtnCancel().addClickHandler(this);
		summaryPopup1.getBtnOk().addClickHandler(this);
		summaryPopup1.getBtnDownload().addClickHandler(this);
		
		
		//**********rohan ************************
		conditionPopup.getBtnOne().addClickHandler(this);
		conditionPopup.getBtnTwo().addClickHandler(this);
		cancellationPopup.getLblOk().addClickHandler(this);
		cancellationPopup.getLblCancel().addClickHandler(this);
		
		/*******************************Service Schedule Logic********************************/
		form.f_btnservice.addClickHandler(this);
		form.f_btnservice.setEnabled(true);
		serviceSchedulePopUp.getBtnOk().addClickHandler(this);
		serviceSchedulePopUp.getBtnCancel().addClickHandler(this);
		serviceSchedulePopUp.getBtnReset().addClickHandler(this);
		
		serviceSchedulePopUp.getP_default().addClickHandler(this);
		serviceSchedulePopUp.getP_customise().addClickHandler(this);
		
		serviceSchedulePopUp.getP_mondaytofriday().addChangeHandler(this);
		serviceSchedulePopUp.getP_interval().addValueChangeHandler(this);
		
		serviceSchedulePopUp.getP_servicehours().addChangeHandler(this);
		serviceSchedulePopUp.getP_servicemin().addChangeHandler(this);
		serviceSchedulePopUp.getP_ampm().addChangeHandler(this);
		
		/*************************************************************************************/
		frmAndToPop.btnOne.addClickHandler(this);
		frmAndToPop.btnTwo.addClickHandler(this);
		form.dbpaymentrecieved.addChangeHandler(this);
		form.getTbFullName().addChangeHandler(this);
		form.getTbCompanyName().addChangeHandler(this);
		//Vijay on 3 march 2017
		serviceSchedulePopUp.getP_serviceWeek().addChangeHandler(this);

		//Date 14-08-2017 added by vijay for invocie payment tab panel popup
		
	
		List<ScreenAuthorization> moduleValidation = null;
		if(UserConfiguration.getRole()!=null){
			moduleValidation=UserConfiguration.getRole().getAuthorization();
		}
		for(int f=0;f<moduleValidation.size();f++)
		{
			if(moduleValidation.get(f).getScreens().trim().equals("Payment Details")||moduleValidation.get(f).getScreens().trim().equals("Payment List")||moduleValidation.get(f).getScreens().trim().equals("PAYMENTDETAILS")||moduleValidation.get(f).getScreens().trim().equals("PAYMENTLIST")){
				accessPayments=true;
				break;
			}
		}
		
		invoicePaymentPopup = new ViewInvoicePaymentServicePopUp(accessPayments);
		invoicePaymentPopup.btnClose.addClickHandler(this);

		 /** Date 28-02-2018 By vijay for New customer ****/
		form.btnNewCustomer.addClickHandler(this);
		newcustomerPopup.getLblOk().addClickHandler(this);
		newcustomerPopup.getLblCancel().addClickHandler(this);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.QUICKCONTRACT,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
		otpPopup.getBtnOne().addClickHandler(this);
		otpPopup.getBtnTwo().addClickHandler(this);
	}

	public static QuickContractForm initalize(){
		/*
		 * 24-06-2017
		 * nidhi
		 **/
		SalesLineItemTable.newBranchFlag = false;
		/*
		 * end
		 */
		QuickContractForm form=new  QuickContractForm();
		ContractPresenterTable gentable=null;
		ContractPresenterSearch searchpopup=null;
		if(form.pedioContractFlag){
			gentable=new ContractPresenterTableProxy(form.pedioContractFlag);
			gentable.setView(form);
			gentable.applySelectionModle();
			ContractPresenterSearch.staticSuperTable=gentable;
			searchpopup=new QuickContractSearchProxy();
			form.setSearchpopupscreen(searchpopup);
			}else{
			gentable=new ContractPresenterTableProxy();
			gentable.setView(form);
			gentable.applySelectionModle();
			ContractPresenterSearch.staticSuperTable=gentable;
			searchpopup=new ContractPresenterSearchProxy();
			form.setSearchpopupscreen(searchpopup);
			}
		form.addClickEventOnActionAndNavigationMenus(); //Ashwini Patil Date:15-03-2022 Description:this line was missing due to which 3 line menu was not coming
		
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Quick Contract",Screen.QUICKCONTRACT);
		QuickContractPresentor  presenter=new QuickContractPresentor(form,new Contract());
		AppMemory.getAppMemory().stickPnel(form);
		
		return form;
		
	}
	
		
	
	
	@Override
	public void reactToProcessBarEvents(ClickEvent e) {

		InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();
		if(text.equals("JobCard"))
			reactToJobCard();
		if(text.equals("New")){
			System.out.println("On new click ==");
			reactToNew();
		}
			
		if(text.equals(AppConstants.PRINTJOBCARD))
			reactOnJobCard();
		if(text.equals("Email"))
			reactOnEmail();
		if(text.equals("Submit"))
			reactOnSubmit(label);
		
//		if(text.equals("Update Taxes")){
//			
//			panel = new PopupPanel(true);
//			panel.add(frmAndToPop);
//			panel.show();
//			panel.center();
//			
//		}
		
		/**
		 *  nidhi
		 *  19-07-2017
		 *  quick contract renewal method and process add in quick contract
		 *  
		 */
		if(text.equals(AppConstants.CONTRACTRENEWAL)){
			reactOnQuickContractRenewal();
		}
		/*
		 *  end
		 */
		
		/**
		 * Date : 29-07-2017 By ANIL
		 * 
		 */
		if (text.equals(AppConstants.VIEWBILL)) {
			viewBillingDocuments();
		}
	
		/**
		 * Date : 14-08-2017 By Vijay
		 */
		if(text.equals(AppConstants.VIEWINVOICEPAYMENT)){
			reatcOnViewInvoicePaymentAndService();
		}
		if (text.equals(AppConstants.CANCLECONTRACT)) {
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "CancelOpenDocumentsDirectly")){
				cancellation.showPopUp();
			}else{
			contractDisContinue = false;
			form.showWaitSymbol();
			boolean conf = Window.confirm("Do you really want to cancel this contract?");
			if(conf==true){
			getAllDocumentDetails();
			}
			
			}
		}
		if (text.equals(AppConstants.CANCELLATIONSUMMARY)) {
			getSummaryFromContractID();
		}
	}
	
	
	private void getSummaryFromContractID() {

		MyQuerry querry = new MyQuerry();
		Company c = new Company();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;

		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);

		filter = new Filter();
		filter.setQuerryString("docID");
		filter.setIntValue(Integer.parseInt(form.getTbContractId().getValue()
				.trim()));
		filtervec.add(filter);

		filter = new Filter();
		filter.setQuerryString("docType");
		filter.setStringValue(AppConstants.APPROVALCONTRACT);
		filtervec.add(filter);

		querry.setFilters(filtervec);
		querry.setQuerryObject(new CancelSummary());
		System.out.println("querry completed......");
		genasync.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {

					@Override
					public void onFailure(Throwable caught) {
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						for (SuperModel smodel : result) {
							CancelSummary cancelEntity = (CancelSummary) smodel;

							summaryPanel = new PopupPanel(true);
							summaryPanel.add(summaryPopup1);
							summaryPanel.show();
							summaryPanel.center();
							summaryPopup1.setSummaryTable(cancelEntity
									.getCancelLis());
						}
					}
				});
	}

	private void getAllDocumentDetails() {
		Console.log("inside getAllDocumentDetails");
	update.getRecordsForContractCancel(model.getCompanyId(), model.getCount(),contractDisContinue,true,"QuickContractPresenter", new AsyncCallback<ArrayList<CancelContract>>(){

		@Override
		public void onSuccess(ArrayList<CancelContract> result) {
			// TODO Auto-generated method stub
			cancellationPopup.getAllContractDocumentTable().getDataprovider().getList().clear();
            System.out.println("result size cancel : "+result.size());
            if(!(model.getStatus().equalsIgnoreCase(Contract.CANCELLED)) && !contractDisContinue){
                CancelContract canContract = new CancelContract();
        		canContract.setDocumnetId(model.getCount());
        		canContract.setBranch(model.getBranch());
        		canContract.setBpName(model.getCinfo().getFullName());
        		canContract.setDocumentStatus(model.getStatus());
        		canContract.setDocumnetName(AppConstants.CONTRACT);
        		canContract.setRecordSelect(true);
        		cancellationPopup.getAllContractDocumentTable().getDataprovider().getList().add(canContract);
            }
            cancellationPopup.getAllContractDocumentTable().getDataprovider().getList().addAll(result);
			cancellationPopup.getAllContractDocumentTable().getTable().redraw();
			
			if(cancellationPopup.getAllContractDocumentTable().getDataprovider().getList().size() > 0){
				cancellationPopup.showPopUp();
				Timer timer=new Timer() {
					@Override
					public void run() {
						form.hideWaitSymbol();
					}
				};
				timer.schedule(3000); 
			
				}else{
					if(!contractDisContinue){
						form.showDialogMessage("No documents are found for cancellation");
					}else{
						
					}
				}	
		} 
		
		@Override
		public void onFailure(Throwable caught) {
			// TODO Auto-generated method stub
			System.out.println("can not load data");
		}
		
	});


}

	/**
	 * Date 14-08-2017 added by vijay for getting billing deatils
	 */
	private void reatcOnViewInvoicePaymentAndService() {
		
		MyQuerry query = getQuery(model);
		query.setQuerryObject(new BillingDocument());

		genasync.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
			
				System.out.println("Result size=="+result.size());
				ArrayList<BillingDocument> billinglist = new ArrayList<BillingDocument>();
				for(SuperModel model : result){
					BillingDocument billingEntity = (BillingDocument) model;
					
					billinglist.add(billingEntity);
				}

				invoicePaymentPopup.companyId=model.getCompanyId();
				invoicePaymentPopup.OrderId=model.getCount();
				invoicePaymentPopup.getTabBillingTable().getDataprovider().setList(billinglist);
				invoicePaymentPopup.getTabBillingTable().getTable().redraw();
				
				/**
				 * @author Anil @since 28-05-2021
				 */
				invoicePaymentPopup.tabPanel.selectTab(0);
				invoicePaymentPopup.getTabInvoiceTable().getTable().redraw();
				invoicePaymentPopup.tabPaymentTable.getTable().redraw();
				invoicePaymentPopup.tabServiceTable.getTable().redraw();
				
				invoicePaymentPanel = new PopupPanel();
				
				invoicePaymentPanel.add(invoicePaymentPopup);
				invoicePaymentPanel.center();
				invoicePaymentPanel.show();
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				form.showDialogMessage("Unexpected Error Occurred");

			}
		});
		
	
	}
	
	private MyQuerry getQuery(Contract model) {

		MyQuerry querry = new MyQuerry();
		
		Vector<Filter> filtervec = new Vector<Filter>();
		
		Filter filter = new Filter();
		filter.setIntValue(model.getCount());
		filter.setQuerryString("contractCount");
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setLongValue(model.getCompanyId());
		filter.setQuerryString("companyId");
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setStringValue("Service Order");
		filter.setQuerryString("typeOfOrder");
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		return querry;
	}
	
	

	private void viewBillingDocuments() {
		final MyQuerry querry=new MyQuerry();
		Vector<Filter> temp=new Vector<Filter>();
		Filter filter=null;
		
		filter=new Filter();
		filter.setQuerryString("contractCount");
		filter.setIntValue(model.getCount());
		temp.add(filter);
		
		querry.setFilters(temp);
		querry.setQuerryObject(new BillingDocument());
		form.showWaitSymbol();
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				form.hideWaitSymbol();
				if(result.size()==0){
					form.showDialogMessage("No billing document found.");
					return;
				}
				if(result.size()==1){
					final BillingDocument billDocument=(BillingDocument) result.get(0);
					final BillingDetailsForm form=BillingDetailsPresenter.initalize();
					Timer timer=new Timer() {
						@Override
						public void run() {
							form.updateView(billDocument);
							form.setToViewState();
						}
					};
					timer.schedule(1000);
				}else{
					BillingListPresenter.initalize(querry);
				}
				
			}
		});
		
	}
	
	private void reactOnQuickContractRenewal(){
		try {
			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Quick Contract", Screen.QUICKCONTRACT);
			final QuickContractForm form = QuickContractPresentor.initalize();
			
			/**
			 * Date : 12-05-2017 By ANIl
			 */
			form.checkCustomerBranch(model.getCinfo().getCount());
			/**
			 * End
			 */
			form.showWaitSymbol();
			Timer t = new Timer() {
				@Override
				public void run() {
					form.setToNewState();
					form.hideWaitSymbol();
					form.fieldForRenewalContract((Contract)model);
					form.findMaxServiceSrNumber();
					
				}
			};
			t.schedule(1000);
			form.hideWaitSymbol();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void reactOnSubmit(InlineLabel label) {
		System.out.println("I am in submit");
		
		if(!model.getStatus().equals("Approved")){
			label.setVisible(false);
			createBillingDocs(label);
		}
			
		
	}


	private void createBillingDocs(final InlineLabel label) {
		/**
		 * @author Anil @since 18-05-2021
		 * submitflag value true for the first click , if same button get clicked for second time then it will return the call
		 */
		if(submitFlag){
			return;
		}
		submitFlag=true;
		
		form.showWaitSymbol();
		
		quickcontractserviceimpl.createBillingDocs(model, new AsyncCallback<ArrayList<Integer>>() {


			@Override
			public void onSuccess(final ArrayList<Integer> result) {
				// TODO Auto-generated method stub
				label.setVisible(true);
				submitFlag=false;
				if(result!=null){
//					if(result.contains(1))
					
					
					model.setStatus(Contract.APPROVED);
					form.tbStatus.setValue(Contract.APPROVED);
					form.setMenuAsPerStatus();
					
					/**
					 *  30-12-2020 Added By Priyanka.
					 *  DES - issue raised by Vaishnavi mam As a part of Simplification Project.
					 */
					String mainScreenLabel="Contract Details";
					if(form.contractObject!=null&&form.contractObject.getCount()!=0){
						mainScreenLabel=form.contractObject.getCount()+" "+"/"+" "+form.contractObject.getStatus()+" "+ "/"+" "+AppUtility.parseDate(form.contractObject.getCreationDate());
					}
					

					form.fgroupingCustomerInformation.getHeaderLabel().setText(mainScreenLabel);
					/**
					 *   end
					 */
					
					/**
					 * @author Anil @since 26-05-20
					 */
					Timer time = new Timer() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							if (result.contains(1)) {
								form.hideWaitSymbol();

								form.showDialogMessage("Completed Successfully");


							} 
							
							form.hideWaitSymbol();

						}
					};
					time.schedule(2000);
							
							
					
//					if (result.contains(5)) {
//						form.hideWaitSymbol();
//
//						form.showDialogMessage("Completed Successfully");
//
//
//					} 
//					
//					form.hideWaitSymbol();


//					else if (result.contains(3)) {
//						System.out.println("Billing Documents Created Successfully");
//					} else if (result.contains(4)) {
//						System.out.println("Invoice Created Successfully");
//					}else if (result.contains(5)) {
//						form.showDialogMessage("Completed Successfully");
//						System.out.println("payment created Successfully");
//					}
					
					
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				submitFlag=false;
				label.setVisible(true);
			}
			
		});
		
		
	}

	@Override
	public void reactOnPrint() {

		//*************rohan changes ********************
			
			MyQuerry querry = new MyQuerry();
		  	Company c = new Company();
		  	Vector<Filter> filtervec=new Vector<Filter>();
		  	Filter filter = null;
		  	filter = new Filter();
		  	filter.setQuerryString("companyId");
			filter.setLongValue(c.getCompanyId());
			filtervec.add(filter);
			filter = new Filter();
			filter.setQuerryString("processName");
			filter.setStringValue("Contract");
			filtervec.add(filter);
			
			filter = new Filter();
			filter.setQuerryString("processList.status");
			filter.setBooleanvalue(true);
			filtervec.add(filter);
			
			querry.setFilters(filtervec);
			querry.setQuerryObject(new ProcessConfiguration());
			
			service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onFailure(Throwable caught) {
				}			

				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					System.out.println(" result set size +++++++"+result.size());
					
					List<ProcessTypeDetails> processList =new ArrayList<ProcessTypeDetails>();
					
					
					if(result.size()==0){
						
						final String url = GWT.getModuleBaseURL() + "pdfservice"+"?Id="+model.getId()+"&"+"type="+"c"+"&"+"preprint="+"plane";
						Window.open(url, "test", "enabled");
					}
					else{
					
						for(SuperModel model:result)
						{
							ProcessConfiguration processConfig=(ProcessConfiguration)model;
							processList.addAll(processConfig.getProcessList());
							
						}
					
					for(int k=0;k<processList.size();k++){	
					if(processList.get(k).getProcessType().trim().equalsIgnoreCase("CompanyAsLetterHead")&&processList.get(k).isStatus()==true){
						
						cnt=cnt+1;
					
					}
					
					
					if(processList.get(k).getProcessType().trim().equalsIgnoreCase("PestoIndiaQuotations")&&processList.get(k).isStatus()==true){
						
						pestoIndia=pestoIndia+1;
					
					}
					
					if(processList.get(k).getProcessType().trim().equalsIgnoreCase("VCareCustomization")&&processList.get(k).isStatus()==true){
						
						vcare=vcare+1;
					
					}
					
					}
					
					
					if(cnt > 0){
						System.out.println("in side react on prinnt");
						panel=new PopupPanel(true);
						panel.add(conditionPopup);
						panel.setGlassEnabled(true);
						panel.show();
						panel.center();
					}
					else if(pestoIndia > 0){
						System.out.println("in side react on prinnt");
						panel=new PopupPanel(true);
						panel.add(conditionPopup);
						panel.setGlassEnabled(true);
						panel.show();
						panel.center();
						
					}
					else if(vcare > 0){
						System.out.println("in side react on prinnt");
						panel=new PopupPanel(true);
						panel.add(conditionPopup);
						panel.setGlassEnabled(true);
						panel.show();
						panel.center();
						
					}
					
					else
					{
						final String url = GWT.getModuleBaseURL() + "pdfservice"+"?Id="+model.getId()+"&"+"type="+"c"+"&"+"preprint="+"plane";
						 Window.open(url, "test", "enabled");
						
					}
				}
				}
			});
			//**************************changes ends here ********************************
	}

	public void reactOnJobCard()
	{
		final String url = GWT.getModuleBaseURL() + "jobcardpdf"+"?Id="+model.getId();
		 Window.open(url, "test", "enabled");
	}

	@Override
	public void reactOnDownload()
	{
		if(view.getSearchpopupscreen().getSupertable().getListDataProvider().getList()==null||view.getSearchpopupscreen().getSupertable().getListDataProvider().getList().size()==0)
		{
			form.showDialogMessage("No records found for download!");
			return;
		}
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "RestrictContractsDownloadWithOTP")){
			generatedOTP=(int) Math.floor(100000 + Math.random() * 900000);
			Console.log("generatedOTP="+generatedOTP);
			smsService.checkConfigAndSendContractDownloadOTPMessage(model.getCompanyId(), LoginPresenter.loggedInUser, generatedOTP, new  AsyncCallback<String>() {

				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					form.showDialogMessage("Failed");
				}

				@Override
				public void onSuccess(String result) {
					// TODO Auto-generated method stub
					Console.log("in success");
					if(result!=null&&result.equalsIgnoreCase("success")) {						
						otpPopup.clearOtp();
						otppopupPanel = new PopupPanel(true);
						otppopupPanel.add(otpPopup);
						otppopupPanel.show();
						otppopupPanel.center();
					}else {
						form.showDialogMessage(result);
					}
						
				}
			} );
		
		}else
			runRegularDownloadProgram();
	}
	
	@Override
	public void reactOnEmail() {


//		boolean conf = Window.confirm("Do you really want to send email?");
//		if (conf == true){
//			emailService.initiateServiceContractEmail((Sales) model,new AsyncCallback<Void>() {
//	
//				@Override
//				public void onFailure(Throwable caught) {
//					Window.alert("Resource Quota Ended ");
//					caught.printStackTrace();
//				}
//	
//				@Override
//				public void onSuccess(Void result) {
//					Window.alert("Email Sent Sucessfully !");
//					
//				}
//			});
//		}
		
		/**
		 * @author Vijay Date 19-03-2021
		 * Des :- above old code commented 
		 * Added New Email Popup Functionality
		 */
		emailpopup.showPopUp();
		setEmailPopUpData();
		
		/**
		 * ends here
		 */
	
	}
	

	@Override
	protected void makeNewModel() {
		model=new Contract();		
	}
	
	@Override
	public void onClick(ClickEvent event) {
		super.onClick(event);
		
		
		if(event.getSource()==frmAndToPop.btnOne){
			getTaxesDetails();
			panel.hide();
		}
		
		if(event.getSource()==frmAndToPop.btnTwo){
			panel.hide();
		}
		
		
		if(event.getSource().equals(conditionPopup.getBtnOne()))
		{
			if(cnt >0)
			{
				System.out.println("in side one yes");
				final String url = GWT.getModuleBaseURL() + "pdfservice"+"?Id="+model.getId()+"&"+"type="+"c"+"&"+"preprint="+"yes";
				Window.open(url, "test", "enabled");
					panel.hide();
			}
			
			else if(pestoIndia > 0)
			{
				System.out.println("Pesto india quotation");
				final String url = GWT.getModuleBaseURL() + "pdfpestoIndiaservice"+"?Id="+model.getId()+"&"+"type="+"c"+"&"+"preprint="+"yes"; 
				Window.open(url, "test", "enabled");
				panel.hide();
			}
			else if(vcare > 0)
			{
				System.out.println("vcare contract");
				final String url = GWT.getModuleBaseURL() +"Contract"+"?Id="+model.getId()+"&"+"preprint="+"yes";
				 Window.open(url, "test", "enabled");
				panel.hide();
			}
		
		}
		
		if(event.getSource().equals(conditionPopup.getBtnTwo()))
		{
			if(cnt >0)
			{
				System.out.println("inside two no");
				
				final String url = GWT.getModuleBaseURL() + "pdfservice"+"?Id="+model.getId()+"&"+"type="+"c"+"&"+"preprint="+"no";
				Window.open(url, "test", "enabled");
				    panel.hide();
				
			}
			
			else if(pestoIndia > 0)
			{
				System.out.println("Pesto india quotation");
				final String url = GWT.getModuleBaseURL() + "pdfpestoIndiaservice"+"?Id="+model.getId()+"&"+"type="+"c"+"&"+"preprint="+"no"; //+"&"+"group="+model.getGroup();//1st PDF
				Window.open(url, "test", "enabled");
			
				 panel.hide();
			
			}
			else if(vcare > 0)
			{
				System.out.println("vcare contract");
				final String url = GWT.getModuleBaseURL() +"Contract"+"?Id="+model.getId()+"&"+"preprint="+"no";
				 Window.open(url, "test", "enabled");
				panel.hide();
			}
		}

		
		
		
		if(event.getSource()==popup.getBtnOk()){
			cancelpanel.hide();
			
//			if(form.getstatustextbox().getValue().equals(Contract.APPROVED)){
//				checkForBillingDetailsStatus();
//			}
			
		}
		else if(event.getSource()==popup.getBtnCancel()){
			cancelpanel.hide();
			popup.remark.setValue("");
		}
		
		
		if(event.getSource()==summaryPopup1.getBtnOk()){
			summaryPanel.hide();
			
		}
		else if(event.getSource()==summaryPopup1.getBtnDownload()){
			summaryPanel.hide();
			reactOnCancelSummaryDownload();
		}
		
		
		/******************************Service Schedule Logic********************************/
		
		if(event.getSource().equals(form.f_btnservice)){
			if(form.getSaleslineitemtable().getDataprovider().getList().size()==0){
				form.showDialogMessage("Add Atleast One Product");
			}
			else
			{
				reactToService();
				reactforTable();
				serviceSchedulePopUp.getPopScheduleTable().setEnable(false);
			}
		}
		
		if(event.getSource().equals(serviceSchedulePopUp.getBtnOk()))
		{

			int cnt=0;
			int pic=0;
			List<SalesLineItem> salesitemlis=form.saleslineitemtable.getDataprovider().getList();
			List<ServiceSchedule> prodservicelist=this.serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList();
			
			for(int k=0;k<salesitemlis.size();k++)
			{
				for(int i=0;i<prodservicelist.size();i++)
				{
					if(calculateEndDate(salesitemlis.get(k).getStartDate(),salesitemlis.get(k).getDuration()).equals(prodservicelist.get(i).getScheduleServiceDate()))
					{
						cnt=cnt+1;
					}
				}
			}
			
			if(cnt>1){
				form.showDialogMessage("Please Schedule services properly");
			}
			else{
				reactOnOk();
			}
		}
		
		if(event.getSource()==serviceSchedulePopUp.getBtnCancel()){
			schedulePanel.hide();
		}	
	
		if(event.getSource()==serviceSchedulePopUp.getP_default()){
			reactfordefaultTable();
			serviceSchedulePopUp.getPopScheduleTable().setEnable(false);
		}
		
		if(event.getSource()==serviceSchedulePopUp.getP_customise()){
			List<ServiceSchedule> prodservicelist=this.serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList();
			if(prodservicelist.size()!=0){
				serviceSchedulePopUp.getPopScheduleTable().setEnable(true);
				form.checkCustomerBranch(Integer.parseInt(form.getPersonInfoComposite().getId().getValue()));
				reactforcustumize();
			}else{
				form.showDialogMessage("Please Select Either Equal or Interval");
				serviceSchedulePopUp.getP_customise().setValue(false);
			}
			
		}
		if(event.getSource()==serviceSchedulePopUp.getBtnReset()){
			reactforreset();
		}

		//Date 14-08-2017 added by vijay for invoice payment popup close
		if(event.getSource() == invoicePaymentPopup.btnClose){
			this.initalize();
			
			Timer time = new Timer() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					form.updateView(model);
		        	form.setToViewState();
					invoicePaymentPanel.hide();
				}
			};
			time.schedule(2000);
//			/** Date 09-03-2021 by Vijay updated code for smooth execution **/
//			AppMemory.getAppMemory().currentScreen = Screen.QUICKCONTRACT;
//			if(form.getProcesslevelBarNames()!=null)
//			form.setProcesslevelBarNames(form.getProcesslevelBarNames());
//			if(form.getProcessLevelBar()!=null)
//			form.setProcessLevelBar(form.getProcessLevelBar());
//			if(form.getProcesslevel()!=null)
//			form.setProcesslevel(form.getProcesslevel());
//
//			/**
//			 * @author Anil @since 31-05-2021
//			 * Menu overlapping issue raised by Rahul Tiwari
//			 */
////			if(!AppMemory.getAppMemory().enableMenuBar||FormStyle.DEFAULT==form.formstyle){
////				form.content.add(form.processLevelBar.content);	
////			}else{
////				form.addClickEventOnActionAndNavigationMenus();
////			}
//			
//			form.initializeScreen();
//			form.intializeScreenMenus();
//			form.addClickEventOnScreenMenus();
//			form.addClickEventOnActionAndNavigationMenus();
//			for (int k = 0; k < mem.skeleton.menuLabels.length; k++) {
//				if (mem.skeleton.registration[k] != null)
//					mem.skeleton.registration[k].removeHandler();
//			}
//
//			for (int k = 0; k < mem.skeleton.menuLabels.length; k++)
//				mem.skeleton.registration[k] = mem.skeleton.menuLabels[k].addClickHandler(this);
//			
//			form.setToViewState();
//			invoicePaymentPanel.hide();
//			setEventHandeling();
		}
		
		/** Date 28-02-2018 By vijay for New customer popup ****/
		if(event.getSource()==form.btnNewCustomer){
			newcustomerPopup.showPopUp();
		}
		
		
		if(event.getSource()==newcustomerPopup.getLblOk()){
			if(validateCustomer()){
				saveNewCustomer();
			}
		}
		
		if(event.getSource()==newcustomerPopup.getLblCancel()){
			System.out.println("Cancel");
			newcustomerPopup.hidePopUp();
		}
		
//		if(event.getSource() instanceof InlineLabel){
//			InlineLabel lnlInline = (InlineLabel) event.getSource();
//			String lblText = lnlInline.getText().trim();
//			System.out.println("lbltext"+lblText);
//			if(lblText.equals("OK")){
//				
//			}
//			if(lblText.equals("Cancel")){
//				System.out.println("Cancel");
//				newcustomerPopup.hidePopUp();
//			}
//			
//	    }
		/**
		 * ends here
		 */
         if(event.getSource() == cancellationPopup.getLblOk()){
			
			/*** Date 23-03-2019 by Vijay for NBHC CCPM with cancellation remark with configuration values and old code in else block****/
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "EnableCancellationRemark")){
				if(cancellationPopup.getOlbCancellationRemark().getSelectedIndex()==0){
					form.showDialogMessage("Please enter valid remark to cancel contract..!");
				}
				else{
					
					reactOnCancelContractButton(cancellationPopup.getOlbCancellationRemark().getValue());
					cancellationPopup.hidePopUp();
				}
			} /*** ends here ****/
			else{
				if(cancellationPopup.getRemark().getValue().trim()!= null && !cancellationPopup.getRemark().getValue().trim().equals("")){
					//  react on contract cancel
					Console.log("cancellationPopup.getLblOk() inside else ");
					reactOnCancelContractButton(cancellationPopup.getRemark().getValue().trim());
					cancellationPopup.hidePopUp();
				}
				else{
					form.showDialogMessage("Please enter valid remark to cancel contract..!");
				}
			}
			
		}
        if(event.getSource() == cancellationPopup.getLblCancel()){
        	cancellationPopup.hidePopUp();
			cancellationPopup.remark.setValue("");
		}
        if(event.getSource() == otpPopup.getBtnOne()){
			Console.log("otp popup ok button clicked");
			Console.log("otp value="+otpPopup.getOtp().getValue());
			if(otpPopup.getOtp().getValue()!=null){
				if(generatedOTP==otpPopup.getOtp().getValue()){
					runRegularDownloadProgram();
					otppopupPanel.hide();
				}else {
					form.showDialogMessage("Invalid OTP");	
					smsService.checkConfigAndSendWrongOTPAttemptMessage(model.getCompanyId(), LoginPresenter.loggedInUser, generatedOTP, "Contract", new  AsyncCallback<String>() {

						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							form.showDialogMessage("Failed");
						}

						@Override
						public void onSuccess(String result) {
							// TODO Auto-generated method stub
							Console.log("InvalidOTPDownloadNotification result="+result);
						}
					} );
				
				}
			}else
				form.showDialogMessage("Enter OTP");
		}
		if(event.getSource() == otpPopup.getBtnTwo()){
			otppopupPanel.hide();
		}
	}

	private void reactOnCancelContractButton(final String remark){
		
		Console.log("inside reactOnCancelContractButton");
		final String loginUser = LoginPresenter.loggedInUser.trim();
		List<CancelContract> cancelContractList = cancellationPopup.getAllContractDocumentTable().getDataprovider().getList();
		ArrayList<CancelContract> selectedCancelContractList = new ArrayList<CancelContract>();
		for(CancelContract canContract : cancelContractList){
			if(canContract.isRecordSelect()){
				selectedCancelContractList.add(canContract);
			}
		}
		System.out.println("Selected list size :" + selectedCancelContractList.size());
		update.saveContractCancelData(model, remark, loginUser, selectedCancelContractList, new AsyncCallback<Void>(){

			@Override
			public void onSuccess(Void result) {
				// TODO Auto-generated method stub
				
				if(!(model.getStatus().equals(Contract.CANCELLED))  && !contractDisContinue){
						model.setStatus(Contract.CANCELLED);
						form.getTbStatus().setValue(Contract.CANCELLED);
						form.getTaDescription().setValue(model.getDescription() + "\n" + "Contract Id ="
						+ model.getCount() + " "
						+ "Contract Status = "+model.getStatus()
						+ "\n"
						+ "has been cancelled by " + loginUser
						+ " with remark" + "\n" + "Remark ="
						+ remark);
						
						form.hideWaitSymbol();
						form.showDialogMessage("Contract Cancellation process started. It may take few minutes.");
						form.setToViewState();
						// Date 01-04-2019 by Vijay for NBHC CCPM to download cancelled remark only ***/
						model.setRemark(remark);
						model.setCancellationDate(new Date());
				}
				
			
				/**
				 * nihdi
				 * 19-05-2018
				 * for discontinue contract 
				 */
				if(contractDisContinue){
					model.setStatus(Contract.CONTRACTDISCOUNTINED);
					model.setDescription(model.getDescription() + "\n" + "Contract Id ="
					+ model.getCount() + " "
					+ "Contract Status = "+model.getStatus()
					+ "\n"
					+ "has been cancelled by " + loginUser
					+ " with remark" + "\n" + "Remark ="
					+ remark);
					form.getTbStatus().setValue(Contract.CONTRACTDISCOUNTINED);
					form.getTaDescription().setValue(model.getDescription() + "\n" + "Contract Id ="
					+ model.getCount() + " "
					+ "Contract Status = "+model.getStatus()
					+ "\n"
					+ "has been cancelled by " + loginUser
					+ " with remark" + "\n" + "Remark ="
					+ remark);
					contractDisContinue = false;
					// Date 01-04-2019 by Vijay for NBHC CCPM to download cancelled remark only ***/
					model.setRemark(remark);
					genasync.save(model, new AsyncCallback<ReturnFromServer>() {
						
						@Override
						public void onSuccess(ReturnFromServer result) {
							// TODO Auto-generated method stub
							form.hideWaitSymbol();
							form.showDialogMessage("Contract Discontinued Successfully..!");
							form.setToViewState();
						}
						
						
						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							
						}
					});
					
				}
			
			}
			
			
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				form.hideWaitSymbol();
				form.showDialogMessage("Please Try again...!");
				
			}

			
			
		});
		
	}
	
	public boolean validateCustomer() {

		Console.log("inside validate customer 11");
		
		//  rohan added these validations 
		
		if(newcustomerPopup.getTbFullName().getValue().equals("")){
			form.showDialogMessage("Please add Customer Fullname..!");
			return false;
		}
		
		if(newcustomerPopup.getOlbbBranch().getSelectedIndex()==0){
			form.showDialogMessage("Branch is mandetory..!");
			return false;
		}
		
		if(newcustomerPopup.getTbCellNo().getValue().equals("") && newcustomerPopup.getEtbEmail().getValue().equals("")){
			form.showDialogMessage("Please Fill either customer cell or customer email..!");
			return false;
		}
		
		System.out.println("value address=="+newcustomerPopup.getAddressComp());
		
		if(newcustomerPopup.getAddressComp().getValue()==null){
			form.showDialogMessage("Please add address..!");
			return false;
		}
		
		if(!newcustomerPopup.getTbFullName().getValue().equals("") || !newcustomerPopup.getTbCompanyName().getValue().equals("")  ){
			if(newcustomerPopup.getAddressComp().getAdressline1().getValue().equals("") || newcustomerPopup.getAddressComp().getCountry().getSelectedIndex()==0 || newcustomerPopup.getAddressComp().getState().getSelectedIndex()==0 || newcustomerPopup.getAddressComp().getCity().getSelectedIndex()==0 ){
				form.showDialogMessage("Please add new customer complete address!");
				return false;
			}
		}
		
//		if(!custpoppup.getTbfName().getValue().equals("")&&custpoppup.getTblName().getValue().equals("")){
//			form.showDialogMessage("Please Fill Customer Last Name!");
//			return false;
//		}
//		if(custpoppup.getTbfName().getValue().equals("")&&!custpoppup.getTblName().getValue().equals("")){
//			form.showDialogMessage("Please Fill Customer First Name!");
//			return false;
//		}
//		
//		if((!custpoppup.getTbfName().getValue().equals("")&&!custpoppup.getTblName().getValue().equals(""))&&(custpoppup.getPnbLandlineNo().getValue().equals(""))){
//			form.showDialogMessage("Please Fill customer cell!");
//			return false;
//		}
		
		/**
		  * Date 26-07-2018 By Vijay
		  * Des:- GSTIN Number can not be greater than 15 characters Validation
		  */
		if(newcustomerPopup.getTbGstNumber().getValue().trim()!=null && !newcustomerPopup.getTbGstNumber().getValue().trim().equals("")){
			if(newcustomerPopup.getTbGstNumber().getValue().trim().length()>15 || newcustomerPopup.getTbGstNumber().getValue().trim().length()<15){
				form.showDialogMessage("GSTIN number must be 15 characters!");
				return false;
			}
		}
		
		return true;
	}
	
	private void saveNewCustomer() {
		final Customer cust=new Customer();
		
		if(!newcustomerPopup.getTbFullName().getValue().trim().equals("")){
			cust.setFullname(newcustomerPopup.getTbFullName().getValue().toUpperCase().trim());
		}
		if(!newcustomerPopup.getTbCompanyName().getValue().trim().equals(""))
		{
			System.out.println("Inside if condition");
			cust.setCompanyName(newcustomerPopup.getTbCompanyName().getValue().toUpperCase().trim());
			cust.setCompany(true);
		}
		
		
		if(!newcustomerPopup.getTbCellNo().getValue().equals("")){
			cust.setCellNumber1(Long.parseLong(newcustomerPopup.getTbCellNo().getValue().trim()));
		}
		if(!newcustomerPopup.getEtbEmail().getValue().equals("")){
			cust.setEmail(newcustomerPopup.getEtbEmail().getValue().trim());
		}
		//  rohan added this code for setting branch in customer 
		
		if(newcustomerPopup.getOlbbBranch().getSelectedIndex()!=0){
			cust.setBranch(newcustomerPopup.getOlbbBranch().getValue(newcustomerPopup.getOlbbBranch().getSelectedIndex()).trim());
		}
		
		/**
		 * Date : 08-08-2017 by ANIL
		 */
		boolean addressFlag=false;
		if(newcustomerPopup.getAddressComp().getValue()!=null){
			cust.setAdress(newcustomerPopup.getAddressComp().getValue());
			cust.setSecondaryAdress(newcustomerPopup.getAddressComp().getValue());
			addressFlag=true;
		}
		
		if(newcustomerPopup.getTbGstNumber().getValue()!=null&&!newcustomerPopup.getTbGstNumber().getValue().equals("")){
			ArticleType articalType=new ArticleType();
			/**
			 * @author Anil @since 25-10-2021
			 * Gstin number should be capture against Service invoice otherwise it will not print on invoice
			 * Raised by Rahul Tiwari
			 */
//			articalType.setDocumentName("Invoice");
			articalType.setDocumentName("Invoice Details");
			articalType.setArticleTypeName("GSTIN");
			articalType.setArticleTypeValue(newcustomerPopup.getTbGstNumber().getValue());
			articalType.setArticlePrint("Yes");
			cust.getArticleTypeDetails().add(articalType);
			
			/**
			 * @author Anil @since 25-10-2021
			 * Gstin number should be capture against Contract otherwise it will not print on invoice
			 * Raised by Rahul Tiwari
			 */
			ArticleType articalType1=new ArticleType();
			articalType1.setDocumentName("Contract");
			articalType1.setArticleTypeName("GSTIN");
			articalType1.setArticleTypeValue(newcustomerPopup.getTbGstNumber().getValue());
			articalType1.setArticlePrint("Yes");
			cust.getArticleTypeDetails().add(articalType1);
		}
	    form.customerEmail=cust.getEmail();
	    Console.log("new customer email value "+form.customerEmail);
		if(addressFlag){
			cust.setNewCustomerFlag(false);
		}else{
			cust.setNewCustomerFlag(true);
		}
		/**
		 * End
		 */
		form.showWaitSymbol();
		genasync.save(cust,new AsyncCallback<ReturnFromServer>() {
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
				form.showDialogMessage("An unexpected error occurred");
			}
			@Override
			public void onSuccess(ReturnFromServer result) {
				System.out.println("BEFORRERERRERERERRERE");
				newcustomerPopup.hidePopUp();
				cust.setCount(result.count);
				setCustomerDetails(cust);
				form.hideWaitSymbol();
			}
		});
	}
	
	public void setCustomerDetails(Customer entity){
		try {
			PersonInfo info=new PersonInfo();
			
			if(entity.isCompany()==true)
			{
				info.setCount(entity.getCount());
				info.setFullName(entity.getCompanyName());
				info.setCellNumber(entity.getCellNumber1());
				info.setPocName(entity.getFullname());
			}
			else
			{
				info.setCount(entity.getCount());
				info.setFullName(entity.getFullname());
				info.setCellNumber(entity.getCellNumber1());
				info.setPocName(entity.getFullname());
			}
			
		    ContractPresenterSearchProxy search = (ContractPresenterSearchProxy) form.getSearchpopupscreen();
		    search.personInfo.setPersonInfoHashMap(info);
			
			/**
			 *  customer object set in personInfoComposite and 
			 *   and new entered value displayed in form using  
			 *   setNewCustomerDetails Method in LeadForm. java class
			 */
			
			PersonInfoComposite.globalCustomerArray.add(info);
			setNewCustomerDetails(info);
			form.showDialogMessage("Customer Saved Successfully!");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}
	
	public void setNewCustomerDetails(PersonInfo pInfo){
		try {
			
			
			form.personInfoComposite.getId().setValue(pInfo.getCount()+"");
			form.personInfoComposite.getName().setValue(pInfo.getFullName());
			form.personInfoComposite.getTbpocName().setValue(pInfo.getPocName());
			form.personInfoComposite.getPhone().setValue(pInfo.getCellNumber()+"");
			System.out.println(" set customer values  "+pInfo.getFullName());
			form.personInfoComposite.setPersonInfoHashMap(pInfo);
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}
	
	/**
	 * ends here
	 */
	
	
	private void reactToCancel()
	{
		model.setStatus(Contract.CONTRACTCANCEL);
		form.getTbStatus().setText(Contract.CONTRACTCANCEL);
		changeStatus("Failed To Cancel Contract !","Contract Cancelled !");
		form.setAppHeaderBarAsPerStatus();
		form.setMenuAsPerStatus();
	}
	
	private void reactToJobCard()
	{
		final String url = GWT.getModuleBaseURL() + "jobcardpdf"+"?Id="+model.getId();
		Window.open(url, "test", "enabled");
	}
	private void reactToNew()
	{
		System.out.println(" in reacton new method ");
		form.setToNewState();
		initalize();
	}
	
	
	private void changeStatus(final String failureMessage,final String successMessage)
	{
		async.changeStatus(model,new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				form.showDialogMessage(failureMessage);
			}

			@Override
			public void onSuccess(Void result) {
				form.showDialogMessage(successMessage);
			}
		});
	}
	
	
	@Override
	public void onRowCountChange(RowCountChangeEvent event) {
		NumberFormat nf=NumberFormat.getFormat("0.00");
		System.out.println("Row count change =========");
		if(event.getSource().equals(form.getSaleslineitemtable().getTable()))
		{
			System.out.println("Row count change ========= 1 ");

			this.reactOnProductsChange();
			if(form.getSaleslineitemtable().getDataprovider().getList().size()==0)
			{
				form.getServiceScheduleTable().connectToLocal();
				serviceSchedulePopUp.getPopScheduleTable().connectToLocal();
			}
			
//			if(form.servicesFlag!=true){
//				reactFordeleteproduct(form.getSaleslineitemtable().identifyProductId());
//			}
//			form.servicesFlag=false;
			reactFordeleteproduct(form.getSaleslineitemtable()
					.identifyProductId(), form.getSaleslineitemtable()
					.identifySrNo());
	    }
		
		if(event.getSource().equals(form.getChargesTable().getTable())){
			if(form.getDoamtincltax().getValue()!=null){
				double newnetpay=form.getDoamtincltax().getValue()+form.getChargesTable().calculateNetPayable();
				newnetpay=Math.round(newnetpay);
				int netPayAmt=(int) newnetpay;
				newnetpay=netPayAmt;
				form.getDonetpayamt().setValue(Double.parseDouble(nf.format(newnetpay)));
				/******************** vijay *******************/
				if(form.getDbpaymentrecieved().getValue()!=null){
					double balancePayment=newnetpay-form.getDbpaymentrecieved().getValue();
					System.out.println("balance payment ===="+balancePayment);
					form.getDbbalancepayment().setValue(Double.parseDouble(nf.format(balancePayment)));
				}else{
					form.getDbbalancepayment().setValue(Double.parseDouble(nf.format(newnetpay)));
				}
			}
		}
	
	}
	
		
	

	private double fillNetPayable(double amtincltax)
	{
		double amtval=0;
		if(form.getChargesTable().getDataprovider().getList().size()==0)
		{
			amtval=amtincltax;
		}
		if(form.getChargesTable().getDataprovider().getList().size()!=0)
		{
			amtval=amtincltax+form.getChargesTable().calculateNetPayable();
		}
		amtval=Math.round(amtval);
		int retAmtVal=(int) amtval;
		amtval=retAmtVal;
		return amtval;
	}
	
		
	private void reactOnProductsChange()
	{
		System.out.println("Row count change ========= 2222");

		form.showWaitSymbol();
		Timer timer=new Timer() 
	     {
			@Override
			public void run() 
			{
			 NumberFormat nf=NumberFormat.getFormat("0.00");
			 double totalExcludingTax=form.getSaleslineitemtable().calculateTotalExcludingTax();
			    totalExcludingTax=Double.parseDouble(nf.format(totalExcludingTax));
			    form.getDototalamt().setValue(totalExcludingTax);
			    
				boolean chkSize=form.getSaleslineitemtable().removeChargesOnDelete();
				if(chkSize==false){
					form.getChargesTable().connectToLocal();
				}
				
				try {
					form.prodTaxTable.connectToLocal();
					form.addProdTaxes();
					double totalIncludingTax=form.getDototalamt().getValue()+form.getProdTaxTable().calculateTotalTaxes();
					form.getDoamtincltax().setValue(Double.parseDouble(nf.format(totalIncludingTax)));
					form.updateChargesTable();
				} catch (Exception e) {
					e.printStackTrace();
				}
				double netPay=fillNetPayable(form.getDoamtincltax().getValue());
				netPay=Math.round(netPay);
				int netPayable=(int) netPay;
				netPay=netPayable;
				form.getDonetpayamt().setValue(Double.parseDouble(nf.format(netPay)));
				
				/********************* vijay ********************/
				if(form.getDbpaymentrecieved().getValue()!=null){
					double balancePayment=netPay-form.getDbpaymentrecieved().getValue();
					System.out.println("balance payment ===="+balancePayment);
					form.getDbbalancepayment().setValue(Double.parseDouble(nf.format(balancePayment)));
				}else{
					form.getDbbalancepayment().setValue(Double.parseDouble(nf.format(netPay)));
				}
				
				form.hideWaitSymbol();
			}
	     };
    	 timer.schedule(3000);
	}

	@Override
	public void onSelection(SelectionEvent<Suggestion> event) {
		if(event.getSource().equals(form.getPersonInfoComposite().getId())||event.getSource().equals(form.getPersonInfoComposite().getName())||event.getSource().equals(form.getPersonInfoComposite().getPhone())){
//			form.checkCustomerStatus(form.getPersonInfoComposite().getIdValue());
			
			ContractPresenter.custcount=form.getPersonInfoComposite().getIdValue();
			
			
			form.getCustomerAddress(form.getPersonInfoComposite().getIdValue());


			/**
			 *  nidhi
			 *  29-01-2018
			 *  for multiple contact list in drop down
			 */
			int custcountno = form.getPersonInfoComposite().getIdValue();
			form.getCustomerContactList(custcountno);
			/**
			 *  end
			 */
		}
		
//		if(event.getSource().equals(form.getPersonInfoComposite().getId()) || event.getSource().equals(form.getPersonInfoComposite().getName()) ||event.getSource().equals(form.getPersonInfoComposite().getPhone()) ){
//			
//			form.getCustomerAddress(form.getPersonInfoComposite().getIdValue());
//		}
	}
	
	
	private void reactOnCancelSummaryDownload()
	{
		ArrayList<CancelSummary> cancelsummarylist=new ArrayList<CancelSummary>();
		List<CancellationSummary> cancel= summaryPopup1.getSummaryTable().getDataprovider().getList();
		CancelSummary cancelEntity =new CancelSummary();
		cancelEntity.setCancelLis(cancel);
		
		cancelsummarylist.add(cancelEntity);
		csvservice.setCancelSummaryDetails(cancelsummarylist, new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);
			}

			@Override
			public void onSuccess(Void result) {
				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "csvservlet"+"?type="+82;
				Window.open(url, "test", "enabled");
			}
		});
		
	}
		
		
	
	/*********************************Service Schedule Logic**********************************/

	@Override
	public void onChange(ChangeEvent event) {
		// TODO Auto-generated method stub
		System.out.println("Onc chnge ------ ");
		if(event.getSource().equals(serviceSchedulePopUp.getP_mondaytofriday())){
			
			serviceSchedulePopUp.getP_default().setValue(false);
			serviceSchedulePopUp.getP_customise().setValue(false);
			serviceSchedulePopUp.getP_interval().setValue(null);
			serviceSchedulePopUp.getP_servicehours().setSelectedIndex(0);
			serviceSchedulePopUp.getP_servicemin().setSelectedIndex(0);
			serviceSchedulePopUp.getP_ampm().setSelectedIndex(0);
			
			serviceSchedulePopUp.getP_servicehours().setEnabled(true);
			serviceSchedulePopUp.getP_servicemin().setEnabled(true);
			serviceSchedulePopUp.getP_ampm().setEnabled(true);
			serviceSchedulePopUp.getP_customise().setEnabled(true);
			
			reactOnMondaytoFridayList(serviceSchedulePopUp.getP_mondaytofriday().getSelectedIndex(), serviceSchedulePopUp.getP_serviceWeek().getSelectedIndex());
			
			serviceSchedulePopUp.getPopScheduleTable().setEnable(false);
		}
		
		if(event.getSource().equals(serviceSchedulePopUp.getP_servicehours())){
			
			if(serviceSchedulePopUp.getP_servicehours().getSelectedIndex()!=0&&serviceSchedulePopUp.getP_servicemin().getSelectedIndex()!=0&&serviceSchedulePopUp.getP_ampm().getSelectedIndex()!=0){
				List<ServiceSchedule> prodservicelist=this.serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList();
				if(prodservicelist.size()!=0){
					reactOnHoursList();
				}else{
					form.showDialogMessage("Please Select Either Equal or Interval");
					serviceSchedulePopUp.getP_servicehours().setSelectedIndex(0);
					serviceSchedulePopUp.getP_servicemin().setSelectedIndex(0);
					serviceSchedulePopUp.getP_ampm().setSelectedIndex(0);
				}
			}
		}
		
		if(event.getSource().equals(serviceSchedulePopUp.getP_servicemin())){
			
			if(serviceSchedulePopUp.getP_servicehours().getSelectedIndex()!=0&&serviceSchedulePopUp.getP_servicemin().getSelectedIndex()!=0&&serviceSchedulePopUp.getP_ampm().getSelectedIndex()!=0){
				List<ServiceSchedule> prodservicelist=this.serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList();
				if(prodservicelist.size()!=0){
					reactOnHoursList();
				}else{
					form.showDialogMessage("Please Select Either Equal or Interval");
					serviceSchedulePopUp.getP_servicehours().setSelectedIndex(0);
					serviceSchedulePopUp.getP_servicemin().setSelectedIndex(0);
					serviceSchedulePopUp.getP_ampm().setSelectedIndex(0);
				}
			}
		}
		
		if(event.getSource().equals(serviceSchedulePopUp.getP_ampm())){
			
			if(serviceSchedulePopUp.getP_servicehours().getSelectedIndex()!=0&&serviceSchedulePopUp.getP_servicemin().getSelectedIndex()!=0&&serviceSchedulePopUp.getP_ampm().getSelectedIndex()!=0){
				List<ServiceSchedule> prodservicelist=this.serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList();
				if(prodservicelist.size()!=0){
					reactOnHoursList();
				}else{
					form.showDialogMessage("Please Select Either Equal or Interval");
					serviceSchedulePopUp.getP_servicehours().setSelectedIndex(0);
					serviceSchedulePopUp.getP_servicemin().setSelectedIndex(0);
					serviceSchedulePopUp.getP_ampm().setSelectedIndex(0);
				}
			}
		}
	
		if(event.getSource().equals(form.dbpaymentrecieved)){
			System.out.println("you are here on change");
			
			System.out.println(" net payment ==== "+form.donetpayamt.getValue());
			System.out.println("hi recievd == "+form.dbpaymentrecieved.getValue());
			System.out.println("balance =="+form.dbbalancepayment.getValue());
			if(form.dbpaymentrecieved.getValue()==null){
				form.dbpaymentrecieved.setValue(0d);
				form.dbbalancepayment.setValue(form.donetpayamt.getValue());
			}
				
			
			boolean valid =	validation(form.donetpayamt.getValue(),form.dbpaymentrecieved.getValue());
			if(valid == false){
				form.showDialogMessage("Payment recieved amount not greater than net payable amount");
				form.dbpaymentrecieved.setText(null);
				form.dbbalancepayment.setValue(form.donetpayamt.getValue());
			}
			if(valid){
				form.dbbalancepayment.setValue(form.donetpayamt.getValue()-form.dbpaymentrecieved.getValue());
				System.out.println(" if chnged value then == "+ form.dbbalancepayment.getValue());
			}
			
		}
				
		if(event.getSource().equals(form.getTbFullName())){
			System.out.println("hi vijay");
			validateCustomer();
		}
		
		if(event.getSource().equals(form.getTbCompanyName())){
			validateClientName();
		}
	
			
		if(event.getSource().equals(serviceSchedulePopUp.getP_serviceWeek())){
			serviceSchedulePopUp.getP_default().setValue(false);
			serviceSchedulePopUp.getP_customise().setValue(false);
			serviceSchedulePopUp.getP_interval().setValue(null);
			serviceSchedulePopUp.getP_servicehours().setSelectedIndex(0);
			serviceSchedulePopUp.getP_servicemin().setSelectedIndex(0);
			serviceSchedulePopUp.getP_ampm().setSelectedIndex(0);

			serviceSchedulePopUp.getP_servicehours().setEnabled(true);
			serviceSchedulePopUp.getP_servicemin().setEnabled(true);
			serviceSchedulePopUp.getP_ampm().setEnabled(true);
			serviceSchedulePopUp.getP_customise().setEnabled(true);

			reactOnWeekList(serviceSchedulePopUp.getP_mondaytofriday().getSelectedIndex(),serviceSchedulePopUp.getP_serviceWeek().getSelectedIndex());
			
			serviceSchedulePopUp.getPopScheduleTable().setEnable(false);
		}
	
	}
	
	
	/**
	 * Date : 08-03-2017
	 * added by vijay
	 * for scheduling week wise services 
	 * by default Monday is service day of that specific week
	 * requirement from PCAAMB
	 */
	
	/**
	 * Here i am scheduling service days by week of month and day of week
	 * if we not selected any day then service day is scheduled by monday of week
	 * if we select both day and week and service date scheduled by as per selected day and week
	 */
	
	private void reactOnWeekList(int selectedIndex, int selectedWeekindex) {
		

		
		if(selectedIndex==0 && selectedWeekindex==0){
			reactforTable();
			serviceSchedulePopUp.getP_mondaytofriday().setSelectedIndex(0);
		}else{ 
				System.out.println("Only for week scheduling services");
				List<SalesLineItem> salesitemlis = form.saleslineitemtable
						.getDataprovider().getList();
				ArrayList<ServiceSchedule> serschelist = new ArrayList<ServiceSchedule>();
				serschelist.clear();
				List<ServiceSchedule> popuptablelist = serviceSchedulePopUp
						.getPopScheduleTable().getDataprovider().getList();
				boolean flag =false;
				
				boolean greterservicesthanweeks = false;
				
				boolean sameMonthserviceflag = false;
				Date temp =null;
				
				
				
				for (int i = 0; i < salesitemlis.size(); i++) {

					int qty = 0;
					qty = (int) salesitemlis.get(i).getQty();
					for (int k = 0; k < qty; k++) {

						long noServices = (long) (salesitemlis.get(i)
								.getNumberOfServices());
						int noOfdays = salesitemlis.get(i).getDuration();
						int interval = (int) (noOfdays / salesitemlis.get(i)
								.getNumberOfServices());
						Date servicedate = salesitemlis.get(i).getStartDate();
						Date d = new Date(servicedate.getTime());
						
						int months = noOfdays/30;
						System.out.println("no of months===="+months);
						if(noServices>months){
							greterservicesthanweeks = true;
							form.showDialogMessage("Services greater than availabe weeks can not schedule services with weeks");
							serviceSchedulePopUp.getP_serviceWeek().setSelectedIndex(0);
							break;
						}
						

						Date productEndDate = new Date(servicedate.getTime());
						CalendarUtil.addDaysToDate(productEndDate, noOfdays);
						Date prodenddate = new Date(productEndDate.getTime());
						productEndDate = prodenddate;

						for (int j = 0; j < noServices; j++) {

							ServiceSchedule scheduleEntity = new ServiceSchedule();

							Date Stardaate = null;
							Stardaate = salesitemlis.get(i).getStartDate();

							System.out.println("In side method RRRRRRRRRRRRR"
									+ salesitemlis.get(i).getProductSrNo());
							scheduleEntity.setSerSrNo(salesitemlis.get(i)
									.getProductSrNo());

							scheduleEntity.setScheduleStartDate(Stardaate);
							scheduleEntity.setScheduleProdId(salesitemlis.get(i)
									.getPrduct().getCount());
							scheduleEntity.setScheduleProdName(salesitemlis.get(i)
									.getProductName());
							scheduleEntity.setScheduleDuration(salesitemlis.get(i)
									.getDuration());
							scheduleEntity.setScheduleNoOfServices(salesitemlis
									.get(i).getNumberOfServices());
							scheduleEntity.setScheduleServiceNo(j + 1);

							scheduleEntity.setScheduleProdStartDate(Stardaate);
							scheduleEntity.setScheduleProdEndDate(productEndDate);

							scheduleEntity.setServicingBranch(form.olbbBranch
									.getValue());
							scheduleEntity.setTotalServicingTime(salesitemlis
									.get(i).getServicingTIme());

							String calculatedServiceTime = "";
							if (popuptablelist.size() != 0) {
								calculatedServiceTime = popuptablelist.get(j)
										.getScheduleServiceTime();
							} else {
								calculatedServiceTime = "Flexible";
							}

							scheduleEntity
									.setScheduleServiceTime(calculatedServiceTime);

							if (form.customerbranchlist.size() == qty) {
								scheduleEntity
										.setScheduleProBranch(form.customerbranchlist
												.get(k).getBusinessUnitName());

								System.out
										.println("updateServices presenter cust Branch Name "
												+ form.customerbranchlist.get(k)
														.getBusinessUnitName());
								System.out.println("servicing Branch "
										+ form.customerbranchlist.get(k)
												.getBranch());
								if (form.customerbranchlist.get(k).getBranch() != null) {
									System.out.println("if servicing Branch "
											+ form.customerbranchlist.get(k)
													.getBranch());
									scheduleEntity
											.setServicingBranch(form.customerbranchlist
													.get(k).getBranch());
								} else {
									System.out.println("else servicing Branch "
											+ form.customerbranchlist.get(k)
													.getBranch());
									scheduleEntity
											.setServicingBranch(form.olbbBranch
													.getValue());
								}
							} else if (form.customerbranchlist.size() == 0) {
								scheduleEntity
										.setScheduleProBranch("Service Address");

								scheduleEntity.setServicingBranch(form.olbbBranch
										.getValue());

							} else {
								scheduleEntity.setScheduleProBranch(null);
							}
							
							Date day = new Date(d.getTime());
							
							/**
							 * Date 1 jun 2017 added by vijay for service getting same month 
							 */
							if(temp!=null){
								String currentServiceMonth =  DateTimeFormat.getFormat( "d-M-yyyy" ).format( day ).split("-")[1];
								String lastserviceDateMonth = DateTimeFormat.getFormat( "d-M-yyyy" ).format( temp ).split("-")[1];
								if(currentServiceMonth.equals(lastserviceDateMonth)){
									sameMonthserviceflag =true;
								}
							}
							
							if(sameMonthserviceflag){
								CalendarUtil.addDaysToDate(day, 30);
							}
							/**
							 * ends here
							 */
							
							CalendarUtil.setToFirstDayOfMonth(day);
							
							if(selectedWeekindex==1){
								 day = AppUtility.getCalculatedDate(day,selectedIndex);
								if(day.before(Stardaate)){
									if(j==0){
										day =Stardaate;
									}
									else{
										flag = true;
									}
								}
								if(flag || j>0){
									 day =  AppUtility.getCalculatedDate(day,selectedIndex);
								}
								
							}
							else if(selectedWeekindex==2){
								
								CalendarUtil.addDaysToDate(day, 7);
								 day =  AppUtility.getCalculatedDate(day,selectedIndex);
								if(day.before(Stardaate)){
									if(j==0){
										day =Stardaate;
									}
									else{
										flag = true;
									}
								}
								if(flag || j>0){
									CalendarUtil.setToFirstDayOfMonth(day);
									CalendarUtil.addDaysToDate(day, 7);
									 day =  AppUtility.getCalculatedDate(day,selectedIndex);
								}
								 
							}else if(selectedWeekindex==3){
								CalendarUtil.addDaysToDate(day, 14);
								 day =  AppUtility.getCalculatedDate(day,selectedIndex);
								if(day.before(Stardaate)){
									if(j==0){
										day =Stardaate;
									}
									else{
										flag = true;
									}
								}
								if(flag || j>0){
									CalendarUtil.setToFirstDayOfMonth(day);
									CalendarUtil.addDaysToDate(day, 14);
									 day =  AppUtility.getCalculatedDate(day,selectedIndex);
								}
							}
							else if(selectedWeekindex==4){
								CalendarUtil.addDaysToDate(day, 21);
								 day =  AppUtility.getCalculatedDate(day,selectedIndex);
								if(day.before(Stardaate)){
									if(j==0){
										day =Stardaate;
									}
									else{
										flag = true;
									}
								}
								if(flag || j>0){
									CalendarUtil.setToFirstDayOfMonth(day);
									CalendarUtil.addDaysToDate(day, 21);
									 day =  AppUtility.getCalculatedDate(day,selectedIndex);
								}
							}
							
							temp = day;
							
							
							/**
							 * Date 26 jun 2017 added by vijay for issues problems in 3 and 4 service gap 
							 */
							 
							System.out.println(productEndDate.before(day) == false);
							if (productEndDate.before(day) == false) {
								scheduleEntity.setScheduleServiceDate(day);
							} else {
								scheduleEntity.setScheduleServiceDate(productEndDate);
								
							}
							
							System.out.println("DDDDDDDDDD"+d);
							
							CalendarUtil.addDaysToDate(d, interval);
							System.out.println("DDDDDDDDDD ============= "+d);

							int days =0;
							String serviceStartDate =  DateTimeFormat.getFormat( "d-M-yyyy" ).format( Stardaate ).split("-")[0];
							System.out.println("serviceStartDate"+serviceStartDate);
							if(serviceStartDate.equals("1")){
									
								String currentServiceDate =  DateTimeFormat.getFormat( "d-M-yyyy" ).format( d ).split("-")[0];
								System.out.println("currentServiceDay"+currentServiceDate);
								
								 days = AppUtility.checkDateMonthOFEndAndAddDays(currentServiceDate,d);
								System.out.println("VIJAY NEW DATE+++++++"+days);
							
								CalendarUtil.addDaysToDate(d, days);

							}

							System.out.println("Sceduled Date==="+d);
							Date tempdate = new Date(d.getTime());
							
							d = tempdate;
							System.out.println("Interval added date"+d);
							
							/**
							 * ends here
							 */
							
//							if (productEndDate.before(day) == false) {
//								scheduleEntity.setScheduleServiceDate(day);
//							} else {
//								scheduleEntity.setScheduleServiceDate(productEndDate);
//							}
							
							
							scheduleEntity.setScheduleServiceDay(ContractForm.serviceDay(scheduleEntity
											.getScheduleServiceDate()));

							scheduleEntity.setServiceRemark(salesitemlis.get(i).getRemark());
							
							/**
							 * Date 03-04-2017 added by vijay for week number
							 */
							int weeknumber = AppUtility.getweeknumber(scheduleEntity.getScheduleServiceDate());
							scheduleEntity.setWeekNo(weeknumber);
							
							/**
							 * ends here
							 */
							
							serschelist.add(scheduleEntity);
						}
					}

					if(greterservicesthanweeks){
						break;
					}
					serviceSchedulePopUp.getPopScheduleTable().getDataprovider()
							.setList(serschelist);
				}
				
		}
	}
	
	/**
	 * ends here
	 */
	
	
	private boolean validation(Double netpayable, Double recievedamt) {
		
			if(recievedamt>netpayable){
				System.out.println("12345");
				return false;
			
		}
		
		return true;
	}

	@Override
	public void onValueChange(ValueChangeEvent<Integer> event) {
		

		if(event.getSource()==serviceSchedulePopUp.getP_interval()){
			valueChangeMethodLogicForScheduling();
		}
		serviceSchedulePopUp.getPopScheduleTable().setEnable(false);
		
		

		
		
		
	
		
	
	}
	
		
	private void validateClientName() {


		if(form.getTbCompanyName().getValue().trim()!=null){
			final MyQuerry querry=new MyQuerry();
			Vector<Filter> filtervec=new Vector<Filter>();
			Filter temp=null;
			
			temp=new Filter();
			temp.setQuerryString("isCompany");
			temp.setBooleanvalue(true);
			filtervec.add(temp);
			
			temp=new Filter();
			temp.setQuerryString("companyName");
			temp.setStringValue(form.getTbCompanyName().getValue().toUpperCase().trim());
			filtervec.add(temp);
			
			temp=new Filter();
			temp.setQuerryString("companyId");
			temp.setLongValue(model.getCompanyId());
			filtervec.add(temp);
			
			querry.setFilters(filtervec);
			querry.setQuerryObject(new Customer());
			genasync.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {

					@Override
					public void onFailure(Throwable caught) {
						form.showDialogMessage("An Unexpected Error occured !");
						
					}
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						System.out.println("Success"+result.size());
						if(result.size()!=0){
//							clientflag=false;
							form.showDialogMessage("Company Name Already Exists!");
							form.tbCompanyName.setValue("");
						}
						if(result.size()==0){
//							clientflag=true;
						}
					}
					
				});
		}

	
	}

//	private void validateCustomer() {
//
//		
//
////		if(!form.getTbFirstName().getValue().equals("")&&!form.getTbLastName().getValue().equals("")){
//			if(!form.getTbFullName().getValue().equals("")){
//			String custnameval=form.getTbFullName().getValue();
//			
//			custnameval=custnameval.toUpperCase().trim();
//			if(custnameval!=null){
//
//					final MyQuerry querry=new MyQuerry();
//					Vector<Filter> filtervec=new Vector<Filter>();
//					Filter temp=null;
//					
//					temp=new Filter();
//					temp.setQuerryString("isCompany");
//					if(!form.getTbCompanyName().getValue().equals("")){
//						System.out.println(" value =="+form.getTbCompanyName().getValue());
//						temp.setBooleanvalue(true);
//					}
//					if(form.getTbCompanyName().getValue().equals("")){
//						temp.setBooleanvalue(false);
//						System.out.println("valueeeee =="+form.getTbCompanyName().getValue());
//					}
//					filtervec.add(temp);
//					
//					temp=new Filter();
//					temp.setQuerryString("companyId");
//					temp.setLongValue(model.getCompanyId());
//					filtervec.add(temp);
//					
//					temp=new Filter();
//					temp.setQuerryString("fullname");
//					temp.setStringValue(custnameval);
//					filtervec.add(temp);
//					
//					querry.setFilters(filtervec);
//					querry.setQuerryObject(new Customer());
//					genasync.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
//
//							@Override
//							public void onFailure(Throwable caught) {
//								form.showDialogMessage("An Unexpected Error occured !");
//								
//							}
//							@Override
//							public void onSuccess(ArrayList<SuperModel> result) {
//								System.out.println("Success"+result.size());
//								if(result.size()!=0){
//									form.showDialogMessage("Customer Name Already Exists");
//									form.tbFullName.setValue("");
//									
//								}
////								if(result.size()==0){
//////									custflag=true;
////								}
//							}
//						});
//			}
//		}
//	
//	}

			/** NOTE : For Service Schedule Logic . Any change add here */
	/**************************************Service Scheduling Logic************************************/
			/************************Service Schedule Logic Starts Here**************************/
			
			private void reactforreset() {

				serviceSchedulePopUp.getP_default().setValue(false);
				serviceSchedulePopUp.getP_interval().setValue(null);
				serviceSchedulePopUp.getP_mondaytofriday().setSelectedIndex(0);
				serviceSchedulePopUp.getP_customise().setValue(false);
				
				serviceSchedulePopUp.getP_servicehours().setSelectedIndex(0);
				serviceSchedulePopUp.getP_servicemin().setSelectedIndex(0);
				serviceSchedulePopUp.getP_ampm().setSelectedIndex(0);
				
				serviceSchedulePopUp.getP_customise().setEnabled(false);
				serviceSchedulePopUp.getP_servicehours().setEnabled(false);
				serviceSchedulePopUp.getP_servicemin().setEnabled(false);
				serviceSchedulePopUp.getP_ampm().setEnabled(false);
				
				serviceSchedulePopUp.getPopScheduleTable().connectToLocal();
			}

			private void reactforcustumize() {

//				if(specificday==1){
//					form.showDialogMessage("You Selected Specific Day is Remove");
//					specificday=0;
//				}
				
				serviceSchedulePopUp.getP_default().setValue(false);
				serviceSchedulePopUp.getP_interval().setValue(null);
//				serviceSchedulePopUp.getP_mondaytofriday().setSelectedIndex(0);
				
				serviceSchedulePopUp.getP_servicehours().setSelectedIndex(0);
				serviceSchedulePopUp.getP_servicemin().setSelectedIndex(0);
				serviceSchedulePopUp.getP_ampm().setSelectedIndex(0);
				
				
			}

			
			
			private void reactforTable() {
				System.out.println("REACT TO TABLE....");
				for(int i=0;i<form.serviceScheduleTable.getDataprovider().getList().size();i++){
					System.out.println();
					 System.out.println("SERVICING BRANCH ::: "+form.serviceScheduleTable.getDataprovider().getList().get(i).getServicingBranch());
					 System.out.println("SERVICING TIME ::: "+form.serviceScheduleTable.getDataprovider().getList().get(i).getTotalServicingTime());
					 System.out.println();
				}
				List<ServiceSchedule> serviceitemlist=form.serviceScheduleTable.getDataprovider().getList();
				List<SalesLineItem> servlist=form.getSaleslineitemtable().getDataprovider().getList();
				ArrayList<ServiceSchedule> serschelist=new ArrayList<ServiceSchedule>();
				System.out.println("AFTER!!!!");
				for(int i=0;i<serviceitemlist.size();i++){
					
					 System.out.println("SERVICE SR NO"+serviceitemlist.get(i).getSerSrNo());
					 System.out.println("SERVICING BRANCH ::: "+serviceitemlist.get(i).getServicingBranch());
					 System.out.println("SERVICING TIME ::: "+serviceitemlist.get(i).getTotalServicingTime());
					 System.out.println();
				 }
				
				for(int i=0;i<serviceitemlist.size();i++)
				{
					ServiceSchedule scheduleEntity =new ServiceSchedule();
					
					
					if(serviceitemlist.get(i).getScheduleStartDate()!=null){
						scheduleEntity.setScheduleStartDate(serviceitemlist.get(i).getScheduleStartDate());
					}else{
						scheduleEntity.setScheduleStartDate(null);
					}
					
					//   rohan added  this for pesto india changes 
					
					if(serviceitemlist.get(i).getSerSrNo()!=0){
						scheduleEntity.setSerSrNo(serviceitemlist.get(i).getSerSrNo());
					}else{
						scheduleEntity.setSerSrNo(0);
					}
					
					
					
					if(serviceitemlist.get(i).getScheduleProdId()!=0){
						scheduleEntity.setScheduleProdId(serviceitemlist.get(i).getScheduleProdId());
					}else{
						scheduleEntity.setScheduleProdId(0);
					}
					if(!serviceitemlist.get(i).getScheduleProdName().equals("")){
						scheduleEntity.setScheduleProdName(serviceitemlist.get(i).getScheduleProdName());
					}else{
						scheduleEntity.setScheduleProdName(null);
					}
					if(serviceitemlist.get(i).getScheduleDuration()!=0){
						scheduleEntity.setScheduleDuration(serviceitemlist.get(i).getScheduleDuration());
					}else{
						scheduleEntity.setScheduleDuration(0);
					}
					
					if(serviceitemlist.get(i).getScheduleNoOfServices()!=0){
						scheduleEntity.setScheduleNoOfServices(serviceitemlist.get(i).getScheduleNoOfServices());
					}else{
						scheduleEntity.setScheduleNoOfServices(0);
					}
					if(serviceitemlist.get(i).getScheduleServiceNo()!=0){
						scheduleEntity.setScheduleServiceNo(serviceitemlist.get(i).getScheduleServiceNo());
					}else{
						scheduleEntity.setScheduleServiceNo(0);
					}
					if(serviceitemlist.get(i).getScheduleServiceDate()!=null){
						scheduleEntity.setScheduleServiceDate(serviceitemlist.get(i).getScheduleServiceDate());
					}else{
						scheduleEntity.setScheduleServiceDate(null);
					}
					
					if(serviceitemlist.get(i).getScheduleProdStartDate()!=null){
						scheduleEntity.setScheduleProdStartDate(serviceitemlist.get(i).getScheduleProdStartDate());
					}else{
						scheduleEntity.setScheduleProdStartDate(null);
					}
					if(serviceitemlist.get(i).getScheduleProdEndDate()!=null){
						scheduleEntity.setScheduleProdEndDate(serviceitemlist.get(i).getScheduleProdEndDate());
					}else{
						scheduleEntity.setScheduleProdEndDate(null);
					}
					
					System.out.println("time get ===== "+serviceitemlist.get(i).getScheduleServiceTime());
					if(serviceitemlist.get(i).getScheduleServiceTime()!=null){
						scheduleEntity.setScheduleServiceTime(serviceitemlist.get(i).getScheduleServiceTime());
					}else{
						scheduleEntity.setScheduleServiceTime("Flexible");
					}
					
					if(serviceitemlist.get(i).getPremisesDetails()!=null){
						scheduleEntity.setPremisesDetails(serviceitemlist.get(i).getPremisesDetails());
					}else{
						scheduleEntity.setPremisesDetails(null);
					}
					
					
					if(serviceitemlist.get(i).getScheduleProBranch()!=null){
						scheduleEntity.setScheduleProBranch(serviceitemlist.get(i).getScheduleProBranch());
					}else{
						scheduleEntity.setScheduleProBranch(null);
					}
					if(serviceitemlist.get(i).getScheduleServiceDay()!=null){
						scheduleEntity.setScheduleServiceDay(serviceitemlist.get(i).getScheduleServiceDay());
					}else{
						scheduleEntity.setScheduleServiceDay(null);
					}
					System.out.println("BRANCH NOT NULL ...."+serviceitemlist.get(i).getServicingBranch());
					if(serviceitemlist.get(i).getServicingBranch()!=null){
//						System.out.println("BRANCH NOT NULL ...."+serviceitemlist.get(i).getServicingBranch());
						scheduleEntity.setServicingBranch(serviceitemlist.get(i).getServicingBranch());
					}else{
						scheduleEntity.setServicingBranch(null);
					}
					System.out.println("SERVICING TIME NOT NULL ...."+serviceitemlist.get(i).getTotalServicingTime());
					if(serviceitemlist.get(i).getTotalServicingTime()!=null){
//						System.out.println("SERVICING TIME NOT NULL ...."+serviceitemlist.get(i).getTotalServicingTime());
						scheduleEntity.setTotalServicingTime(serviceitemlist.get(i).getTotalServicingTime());
					}else{
						scheduleEntity.setServicingBranch(null);
					}
					
					System.out.println("Vijay clicked on schedule btn ======");
					System.out.println("Value for checkbox"+serviceitemlist.get(i).isCheckService());
					scheduleEntity.setCheckService(serviceitemlist.get(i).isCheckService());
					
					
					// vijay
					if(serviceitemlist.get(i).getServiceRemark()!=null){
						scheduleEntity.setServiceRemark(serviceitemlist.get(i).getServiceRemark());
					}else{
						scheduleEntity.setServiceRemark("");
					}
					
					/**
					 * Date 01-04-2017 added by vijay for week number
					 */
					scheduleEntity.setWeekNo(serviceitemlist.get(i).getWeekNo());
					/**
					 * ends here
					 */
					
					
					serschelist.add(scheduleEntity);
				}
				
				serviceSchedulePopUp.getPopScheduleTable().getDataprovider().setList(serschelist);
				
			}
			
			
			private void reactfordefaultTable() {
				serviceSchedulePopUp.getP_customise().setValue(false);
				serviceSchedulePopUp.getP_interval().setValue(null);
				serviceSchedulePopUp.getP_mondaytofriday().setSelectedIndex(0);

				serviceSchedulePopUp.getP_customise().setEnabled(true);
				serviceSchedulePopUp.getP_servicehours().setEnabled(true);
				serviceSchedulePopUp.getP_servicemin().setEnabled(true);
				serviceSchedulePopUp.getP_ampm().setEnabled(true);

				serviceSchedulePopUp.getP_servicehours().setSelectedIndex(0);
				serviceSchedulePopUp.getP_servicemin().setSelectedIndex(0);
				serviceSchedulePopUp.getP_ampm().setSelectedIndex(0);
				//date 04-04-2017 added by vijay
				serviceSchedulePopUp.getP_serviceWeek().setSelectedIndex(0);

				List<SalesLineItem> salesitemlis = form.saleslineitemtable.getDataprovider().getList();
				ArrayList<ServiceSchedule> serschelist = new ArrayList<ServiceSchedule>();
				serschelist.clear();

				List<ServiceSchedule> popuptablelist = serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList();
				boolean unitFlag = false;
				for (int i = 0; i < salesitemlis.size(); i++) {
					/**
					 * Date : 29-03-2017 By ANil
					 */
					boolean branchSchFlag=false;
					int qty=0;
					/**
					 * Date : 03-04-2017 By ANIL
					 */
//					ArrayList<BranchWiseScheduling> branchSchedulingList=AppUtility.getBranchSchedulingList(salesitemlis.get(i).getBranchSchedulingInfo());
					
					/**
					 * Date 02-05-2018 
					 * Developer : Vijay
					 * Des :- Below code is customer branches stored in string so it can not load more than 120 branches
					 * so i have updated below code using hashmap.
					 */
					
//					/**
//					 * Date : 12-06-2017 BY ANIL
//					 */
//					String[] branchArray=new String[10];
//					
//					if(salesitemlis.get(i).getBranchSchedulingInfo()!=null
//							&&!salesitemlis.get(i).getBranchSchedulingInfo().equals("")){
//						branchArray[0]=salesitemlis.get(i).getBranchSchedulingInfo();
//					}
//					if(salesitemlis.get(i).getBranchSchedulingInfo1()!=null
//							&&!salesitemlis.get(i).getBranchSchedulingInfo1().equals("")){
//						branchArray[1]=salesitemlis.get(i).getBranchSchedulingInfo1();
//					}
//					if(salesitemlis.get(i).getBranchSchedulingInfo2()!=null
//							&&!salesitemlis.get(i).getBranchSchedulingInfo2().equals("")){
//						branchArray[2]=salesitemlis.get(i).getBranchSchedulingInfo2();
//					}
//					if(salesitemlis.get(i).getBranchSchedulingInfo3()!=null
//							&&!salesitemlis.get(i).getBranchSchedulingInfo3().equals("")){
//						branchArray[3]=salesitemlis.get(i).getBranchSchedulingInfo3();
//					}
//					if(salesitemlis.get(i).getBranchSchedulingInfo4()!=null
//							&&!salesitemlis.get(i).getBranchSchedulingInfo4().equals("")){
//						branchArray[4]=salesitemlis.get(i).getBranchSchedulingInfo4();
//					}
//					
//					if(salesitemlis.get(i).getBranchSchedulingInfo5()!=null
//							&&!salesitemlis.get(i).getBranchSchedulingInfo5().equals("")){
//						branchArray[5]=salesitemlis.get(i).getBranchSchedulingInfo5();
//					}
//					if(salesitemlis.get(i).getBranchSchedulingInfo6()!=null
//							&&!salesitemlis.get(i).getBranchSchedulingInfo6().equals("")){
//						branchArray[6]=salesitemlis.get(i).getBranchSchedulingInfo6();
//					}
//					if(salesitemlis.get(i).getBranchSchedulingInfo7()!=null
//							&&!salesitemlis.get(i).getBranchSchedulingInfo7().equals("")){
//						branchArray[7]=salesitemlis.get(i).getBranchSchedulingInfo7();
//					}
//					if(salesitemlis.get(i).getBranchSchedulingInfo8()!=null
//							&&!salesitemlis.get(i).getBranchSchedulingInfo8().equals("")){
//						branchArray[8]=salesitemlis.get(i).getBranchSchedulingInfo8();
//					}
//					if(salesitemlis.get(i).getBranchSchedulingInfo9()!=null
//							&&!salesitemlis.get(i).getBranchSchedulingInfo9().equals("")){
//						branchArray[9]=salesitemlis.get(i).getBranchSchedulingInfo9();
//					}
//					
//					/**
//					 * End
//					 */
//					ArrayList<BranchWiseScheduling> branchSchedulingList=AppUtility.getBranchSchedulingList(branchArray);
					/**
					 * ends here
					 */
					
					/** Date 02-05-2018 By vijay for updated customer branch scheduling using hashmap **/
					ArrayList<BranchWiseScheduling> branchSchedulingList = salesitemlis.get(i).getCustomerBranchSchedulingInfo().get(salesitemlis.get(i).getProductSrNo());
					
					
					if(branchSchedulingList!=null
							&&branchSchedulingList.size()!=0){
						branchSchFlag=true;
						qty=branchSchedulingList.size();
					}else{
						qty=(int) salesitemlis.get(i).getQty();
					}
					
					/**
					 * End
					 */
					if(branchSchFlag){
						
						for (int k = 0; k < qty; k++) {
							if(branchSchedulingList.get(k).isCheck()==true){
								
							long noServices = (long) (salesitemlis.get(i).getNumberOfServices());
							int noOfdays = salesitemlis.get(i).getDuration();
							/********* below one line commented by vijay this interval calculation missing precision so services not getting created still end date ***********/
//							int interval = (int) (noOfdays / salesitemlis.get(i).getNumberOfServices());
							
							/*** Date 28 sep 2017 below code added by vijay interval calculation with precision and using roundoff ******************/
							double days =  noOfdays;
							double noofservices= salesitemlis.get(i).getNumberOfServices();
							double interval =  days / noofservices;
							double temp = interval;
							double tempvalue=interval;
							/******************** ends here ********************/
							
							Date servicedate = salesitemlis.get(i).getStartDate();
							Date d = new Date(servicedate.getTime());
							Date productEndDate = new Date(servicedate.getTime());
							CalendarUtil.addDaysToDate(productEndDate, noOfdays);
							Date prodenddate = new Date(productEndDate.getTime());
							productEndDate = prodenddate;
							Date tempdate = new Date();
							for (int j = 0; j < noServices; j++) {
									
								ServiceSchedule ssch = new ServiceSchedule();
								Date Stardaate = salesitemlis.get(i).getStartDate();

								ssch.setSerSrNo(salesitemlis.get(i).getProductSrNo());
								ssch.setScheduleStartDate(Stardaate);
								ssch.setScheduleProdId(salesitemlis.get(i).getPrduct().getCount());
								ssch.setScheduleProdName(salesitemlis.get(i).getProductName());
								ssch.setScheduleDuration(salesitemlis.get(i).getDuration());
								ssch.setScheduleNoOfServices(salesitemlis.get(i).getNumberOfServices());
								ssch.setScheduleServiceNo(j + 1);
								ssch.setScheduleProdStartDate(Stardaate);
								ssch.setScheduleProdEndDate(productEndDate);
								ssch.setScheduleServiceTime("Flexible");

								ssch.setServicingBranch(form.olbbBranch.getValue());
								ssch.setTotalServicingTime(salesitemlis.get(i).getServicingTIme());

								
							
								ssch.setScheduleProBranch(branchSchedulingList.get(k).getBranchName());
								if (branchSchedulingList.get(k).getServicingBranch()!=null) {
									ssch.setServicingBranch(branchSchedulingList.get(k).getServicingBranch());
								} 
								
								int dayIndex=AppUtility.getDayIndex(branchSchedulingList.get(k).getDay());
								
								if(dayIndex!=0){
									String dayOfWeek1 = format.format(d);
									int adate = Integer.parseInt(dayOfWeek1);
									int adday1 = dayIndex - 1;
									int adday = 0;
									if (adate <= adday1) {
										adday = (adday1 - adate);
									} else {
										adday = (7 - adate + adday1);
									}
									
									Date newDate = new Date(d.getTime());
									CalendarUtil.addDaysToDate(newDate, adday);
									Date day = new Date(newDate.getTime());

									if (productEndDate.before(day) == false) {
										ssch.setScheduleServiceDate(day);
									} else {
										ssch.setScheduleServiceDate(productEndDate);
									}

									/*** added by vijay for interval calculation *************/
									int interval2 =(int) Math.round(interval);
									
									
									CalendarUtil.addDaysToDate(d, interval2);
									Date tempdate1 = new Date(d.getTime());
									d = tempdate1;
									ssch.setScheduleServiceDay(ContractForm.serviceDay(ssch.getScheduleServiceDate()));
									
									/************* added by vijay for scheduling interval calculation with including precision using roundoff*********************/
									interval =(interval+temp)-tempvalue;
								    tempvalue = Math.round(interval);
								    /**************** ends here **********************************/
								    
								}else{
									
									/*** added by vijay for interval calculation *************/
									int interval2 =(int) Math.round(interval);
									
									if (j > 0) {
										CalendarUtil.addDaysToDate(d, interval2);
										tempdate = new Date(d.getTime());
										
										/************* added by vijay for scheduling interval including calculation using roundoff *********************/
										interval =(interval+temp)-tempvalue;
									    tempvalue = Math.round(interval);
									    /**************** ends here **********************************/
									}
									
									if (j == 0) {
										ssch.setScheduleServiceDate(servicedate);
									}
									if (j != 0) {
										if (productEndDate.before(d) == false) {
											ssch.setScheduleServiceDate(tempdate);
										} else {
											ssch.setScheduleServiceDate(productEndDate);
										}
									}
								}
								

//								ssch.setScheduleServiceDay(ContractForm.serviceDay(ssch.getScheduleServiceDate()));
								ssch.setServiceRemark(salesitemlis.get(i).getRemark());
								
								/**
								 * Date 01-04-2017 added by vijay for week number
								 */
								int weaknumber = AppUtility.getweeknumber(ssch.getScheduleServiceDate());
								ssch.setWeekNo(weaknumber);
								
								/**
								 * ends here
								 */
								/***
								  * nidhi *:*:*
								  * 18-09-2018
								  * for map bill product
								  * if any update please update in else conditions also
								  */
								 if(LoginPresenter.billofMaterialActive){
									 
									 BillOfMaterial billPrDt = AppUtility.verifyBillofMaterilProd(salesitemlis.get(i).getPrduct());
									 if(billPrDt!=null){
										 
										 
										 UnitConversionUtility unitConver = new UnitConversionUtility();

										 
										 if(branchSchedulingList.get(k).getUnitOfMeasurement() != null && unitConver.varifyUnitConversion(billPrDt, branchSchedulingList.get(k).getUnitOfMeasurement())){
											 List<ServiceSchedule> seList = new ArrayList<ServiceSchedule>();
											 seList.add(ssch);
											 seList = unitConver.getServiceitemProList(seList,salesitemlis.get(i).getPrduct(),salesitemlis.get(i),billPrDt);
											 if(seList!=null && seList.size()>0)
												 ssch = seList.get(0);
											 
										 }else{
											 unitFlag = true;
//											 form.showDialogMessage("Unit of service area and product group area is differeant and conversion of unit is not avaliable.");
										 }
										 
										 
										 
										 
									 }
								 }
								 /**
								  * end
								  */
								serschelist.add(ssch);
							}
						}
						}
					
					}else{
					
//					int qty=0;
//					qty=(int) salesitemlis.get(i).getQty();
					for (int k = 0; k < qty; k++) {
						long noServices = (long) (salesitemlis.get(i).getNumberOfServices());
						int noOfdays = salesitemlis.get(i).getDuration();
						/********* below one line commented by vijay this interval calculation missing precision so services not getting created still end date ***********/
//						int interval = (int) (noOfdays / salesitemlis.get(i).getNumberOfServices());
						
						/*** Date 28 sep 2017 below code added by vijay interval calculation with precision and using roundoff ******************/
						double days =  noOfdays;
						double noofservices= salesitemlis.get(i).getNumberOfServices();
						double interval =  days / noofservices;
						double temp = interval;
						double tempvalue=interval;
						/******************** ends here ********************/
						
						calculateEndDate(salesitemlis.get(i).getStartDate(),salesitemlis.get(i).getDuration());
						Date servicedate = salesitemlis.get(i).getStartDate();
						
						Date d = new Date(servicedate.getTime());
						Date tempdate = new Date();
						for (int j = 0; j < noServices; j++) {

							/*** added by vijay for interval calculation *************/
							int interval2 =(int) Math.round(interval);
							
							if (j > 0) {
								CalendarUtil.addDaysToDate(d, interval2);
								tempdate = new Date(d.getTime());
								
								/************* added by vijay *********************/
								interval =(interval+temp)-tempvalue;
							    tempvalue = Math.round(interval);
							    /************** ends here ************************/
							
							}

							ServiceSchedule ssch = new ServiceSchedule();

							// rohan added this 1 field
							ssch.setSerSrNo(salesitemlis.get(i).getProductSrNo());

							Date Stardaate = null;
							Stardaate = salesitemlis.get(i).getStartDate();
							ssch.setScheduleStartDate(Stardaate);
							ssch.setScheduleProdId(salesitemlis.get(i).getPrduct().getCount());
							ssch.setScheduleProdName(salesitemlis.get(i).getProductName());
							ssch.setScheduleDuration(salesitemlis.get(i).getDuration());
							ssch.setScheduleNoOfServices(salesitemlis.get(i).getNumberOfServices());
							ssch.setScheduleServiceNo(j + 1);
							ssch.setScheduleProdStartDate(Stardaate);

							ssch.setServicingBranch(form.olbbBranch.getValue());
							ssch.setTotalServicingTime(salesitemlis.get(i).getServicingTIme());

							String calculatedServiceTime = "";
							if (popuptablelist.size() != 0) {
								calculatedServiceTime = popuptablelist.get(j).getScheduleServiceTime();
							} else {
								calculatedServiceTime = "Flexible";
							}

							ssch.setScheduleServiceTime(calculatedServiceTime);

							if (form.customerbranchlist.size() == qty) {
								ssch.setScheduleProBranch(form.customerbranchlist.get(k).getBusinessUnitName());

								/**
								 * rohan added this code for automatic servicing branch
								 * setting for NBHC
								 */
								if (form.customerbranchlist.get(k).getBranch() != null) {
									ssch.setServicingBranch(form.customerbranchlist.get(k).getBranch());
								} else {
									ssch.setServicingBranch(form.olbbBranch.getValue());
								}
								/**
								 * ends here
								 */

							} else if (form.customerbranchlist.size() == 0) {
								ssch.setScheduleProBranch("Service Address");

								/**
								 * rohan added this code for automatic servicing branch
								 * setting for NBHC
								 */
								ssch.setServicingBranch(form.olbbBranch.getValue());
								/**
								 * ends here
								 */

							} else {
								ssch.setScheduleProBranch("");
							}

							ssch.setScheduleProdEndDate(calculateEndDate(salesitemlis.get(i).getStartDate(), salesitemlis.get(i).getDuration()));
							if (j == 0) {
								ssch.setScheduleServiceDate(servicedate);
							}
							if (j != 0) {
								if (calculateEndDate(salesitemlis.get(i).getStartDate(),salesitemlis.get(i).getDuration()).before(d) == false) {
									ssch.setScheduleServiceDate(tempdate);
								} else {
									ssch.setScheduleServiceDate(calculateEndDate(salesitemlis.get(i).getStartDate(),salesitemlis.get(i).getDuration()));
								}
							}
							ssch.setScheduleServiceDay(ContractForm.serviceDay(ssch.getScheduleServiceDate()));
							ssch.setServiceRemark(salesitemlis.get(i).getRemark());
							
							/**
							 * Date 01-04-2017 added by vijay for week number
							 */
							int weaknumber = AppUtility.getweeknumber(ssch.getScheduleServiceDate());
							ssch.setWeekNo(weaknumber);
							
							/**
							 * ends here
							 */
							 /***
							  * nidhi *:*:*
							  * 18-09-2018
							  * for map bill product
							  * if any update please update in else conditions also
							  */
							 if(LoginPresenter.billofMaterialActive){
								 
								 BillOfMaterial billPrDt = AppUtility.verifyBillofMaterilProd(salesitemlis.get(i).getPrduct());
								 if(billPrDt!=null){
									 
									 
									 UnitConversionUtility unitConver = new UnitConversionUtility();

									 
									 if(branchSchedulingList.get(k).getUnitOfMeasurement() != null && unitConver.varifyUnitConversion(billPrDt, branchSchedulingList.get(k).getUnitOfMeasurement())){
										 List<ServiceSchedule> seList = new ArrayList<ServiceSchedule>();
										 seList.add(ssch);
										 seList = unitConver.getServiceitemProList(seList,salesitemlis.get(i).getPrduct(),salesitemlis.get(i),billPrDt);
										 if(seList!=null && seList.size()>0)
											 ssch = seList.get(0);
										 
									 }else{
										unitFlag = true; 
//										 form.showDialogMessage("Unit of service area and product group area is differeant and conversion of unit is not avaliable.");
									 }
									 
									 
									 
									 
								 }
							 }
							 /**
							  * end
							  */
							serschelist.add(ssch);
						}
					}
				}

					/**
					 * nidhi |*|
					 * 7-01-2018
					 */
					if(unitFlag){
						 form.showDialogMessage("Unit of service are and product group detail area not mapped at branch level. Or conversion not available");
					}
					serviceSchedulePopUp.getPopScheduleTable().getDataprovider().setList(serschelist);
					
					/**
					 * Date : 25-11-2017 BY ANIL
					 */
					form.globalScheduleServiceList=new ArrayList<ServiceSchedule>();
					form.globalScheduleServiceList=serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList();
					/**
					 * End
					 */
				}
			}
			
	private Date calculateEndDate(Date servicedt,int noOfdays)
	{
		Date servicedate = servicedt;
		Date d=new Date(servicedate.getTime());
		Date productEndDate= new Date(servicedate.getTime());
		CalendarUtil.addDaysToDate(productEndDate, noOfdays);
		Date prodenddate=new Date(productEndDate.getTime());
		 productEndDate=prodenddate;
		 
		 return productEndDate;
		
	}		
	
	
	//**********************changes ends here *************************			
	
		private void reactOnOk() {
			
			System.out.println("In ok button ==================  ");
			
			System.out.println();
			
			List<ServiceSchedule> prodservicelist=this.serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList();
			ArrayList<ServiceSchedule> prodserviceArr=new ArrayList<ServiceSchedule>();
			List<SalesLineItem> salesitemlis=form.saleslineitemtable.getDataprovider().getList();
//			System.out.println("Start date  === "+AppUtility.calculateQuotationStartDate(salesitemlis));
			Date bigdate=new Date();
			Date smalldate=new Date();
			
			if(prodservicelist.size()!=0){
				if(serviceSchedulePopUp.getP_mondaytofriday().getSelectedIndex()!=0){
					form.f_serviceDay=serviceSchedulePopUp.getP_mondaytofriday().getItemText(serviceSchedulePopUp.getP_mondaytofriday().getSelectedIndex());
				}else{
					form.f_serviceDay="Not Select";
				}
				
			if((serviceSchedulePopUp.getP_servicehours().getSelectedIndex()!=0&&serviceSchedulePopUp.getP_servicemin().getSelectedIndex()!=0&&serviceSchedulePopUp.getP_ampm().getSelectedIndex()!=0)
				||(serviceSchedulePopUp.getP_servicehours().getSelectedIndex()==0&&serviceSchedulePopUp.getP_servicemin().getSelectedIndex()==0&&serviceSchedulePopUp.getP_ampm().getSelectedIndex()==0)){
				
			int branchflag=0;
				
			for(int i=0;i<prodservicelist.size();i++){
				if(form.customerbranchlist.size()!=0){
					if(prodservicelist.get(i).getScheduleProBranch()==null||prodservicelist.get(i).getScheduleProBranch().equalsIgnoreCase("SELECT")){
						branchflag=1;
					}
				}
				
				ServiceSchedule sss=new ServiceSchedule();
			//  rohan added this 1 field 
				System.out.println("In side method RRRRRRRRRRRRR"+prodservicelist.get(i).getSerSrNo());
				sss.setSerSrNo(prodservicelist.get(i).getSerSrNo());
				
				
				
				sss.setScheduleStartDate(prodservicelist.get(i).getScheduleStartDate());
				sss.setScheduleProdId(prodservicelist.get(i).getScheduleProdId());
				sss.setScheduleProdName(prodservicelist.get(i).getScheduleProdName());
				sss.setScheduleDuration(prodservicelist.get(i).getScheduleDuration());
				sss.setScheduleNoOfServices(prodservicelist.get(i).getScheduleNoOfServices());
				sss.setScheduleServiceNo(prodservicelist.get(i).getScheduleServiceNo());
//				System.out.println("service date == "+prodservicelist.get(i).getService_Date());
				if(bigdate.before(prodservicelist.get(i).getScheduleServiceDate())){
					bigdate=prodservicelist.get(i).getScheduleServiceDate();
				}
				if(smalldate.after(prodservicelist.get(i).getScheduleServiceDate())){
					smalldate=prodservicelist.get(i).getScheduleServiceDate();
				}
				sss.setScheduleServiceDate(prodservicelist.get(i).getScheduleServiceDate());
				sss.setScheduleProdStartDate(prodservicelist.get(i).getScheduleProdStartDate());
				sss.setScheduleProdEndDate(prodservicelist.get(i).getScheduleProdEndDate());
				sss.setScheduleServiceTime(prodservicelist.get(i).getScheduleServiceTime());
				sss.setPremisesDetails(prodservicelist.get(i).getPremisesDetails());
				sss.setServicingBranch(prodservicelist.get(i).getServicingBranch());
				sss.setTotalServicingTime(prodservicelist.get(i).getTotalServicingTime());
				
				if(prodservicelist.get(i).getScheduleProBranch()!=null){
					sss.setScheduleProBranch(prodservicelist.get(i).getScheduleProBranch());
				}else{
					sss.setScheduleProBranch("");
				}
				sss.setScheduleServiceDay(prodservicelist.get(i).getScheduleServiceDay());
				
				System.out.println("Hi in ok btn ======"+ prodservicelist.get(i).isCheckService());
				
				sss.setCheckService(prodservicelist.get(i).isCheckService());
				
				//vijay
				sss.setServiceRemark(prodservicelist.get(i).getServiceRemark());
				/**
				 * Date 03-04-2017 added by vijay for week number
				 */
				sss.setWeekNo(prodservicelist.get(i).getWeekNo());
				/**
				 * ends here
				 */
				prodserviceArr.add(sss);
				
			}
			Date d=new Date(bigdate.getTime());
//			CalendarUtil.addDaysToDate(d, -1);
			Date d1=new Date(smalldate.getTime());
			CalendarUtil.addDaysToDate(d1, 1);
			
			if(branchflag==0)
			{
				form.getServiceScheduleTable().getDataprovider().setList(prodserviceArr);
				schedulePanel.hide();
				
			}else{
				form.showDialogMessage("Please Set Branch By Clicking On Customize");
			}
			}else{
				form.showDialogMessage("Please Schedule Service Time Properly");
			}
		  }else{
			  form.showDialogMessage("Please Schedule Service Date");
		  }
			
		}
		
		

		private void reactToService() 
		{
			if(form.tbContractId.getValue()==null){
				serviceSchedulePopUp.getP_docid().setValue("");
			}else{
				serviceSchedulePopUp.getP_docid().setValue(form.tbContractId.getValue()+"");
			}
			if(form.dbContractDate.getValue()==null){
				serviceSchedulePopUp.getP_docdate().setValue(null);
			}else{
				serviceSchedulePopUp.getP_docdate().setValue(form.dbContractDate.getValue());
			}
			
			serviceSchedulePopUp.getP_mondaytofriday().setSelectedIndex(0);
			serviceSchedulePopUp.getP_default().setValue(false);
			serviceSchedulePopUp.getP_customise().setValue(false);
			serviceSchedulePopUp.getP_interval().setValue(null);
			serviceSchedulePopUp.getP_servicehours().setSelectedIndex(0);
			serviceSchedulePopUp.getP_servicemin().setSelectedIndex(0);
			serviceSchedulePopUp.getP_ampm().setSelectedIndex(0);
			// below setter added by vijay on 8-03-2017
			serviceSchedulePopUp.getP_serviceWeek().setSelectedIndex(0);
			schedulePanel=new PopupPanel(true);
			
			ScreeenState transactionStatus=AppMemory.getAppMemory().currentState;
			if(transactionStatus.toString().equals("VIEW")){
				serviceSchedulePopUp.getP_default().setEnabled(false);
				serviceSchedulePopUp.getP_mondaytofriday().setEnabled(false);
				serviceSchedulePopUp.getP_interval().setEnabled(false);
				serviceSchedulePopUp.getP_customise().setEnabled(false);
				serviceSchedulePopUp.getPopScheduleTable().setEnable(false);
				serviceSchedulePopUp.getBtnReset().setEnabled(false);
				serviceSchedulePopUp.getP_servicehours().setEnabled(false);
				serviceSchedulePopUp.getP_servicemin().setEnabled(false);
				serviceSchedulePopUp.getP_ampm().setEnabled(false);
				// below setter added by vijay on 16-03-2017
				serviceSchedulePopUp.getP_serviceWeek().setEnabled(false);
			}
			if(transactionStatus.toString().equals("EDIT")){
				serviceSchedulePopUp.getP_default().setEnabled(true);
				serviceSchedulePopUp.getP_mondaytofriday().setEnabled(true);
				serviceSchedulePopUp.getP_interval().setEnabled(true);
				serviceSchedulePopUp.getP_customise().setEnabled(true);
				serviceSchedulePopUp.getPopScheduleTable().setEnable(false);
				serviceSchedulePopUp.getBtnReset().setEnabled(true);
				serviceSchedulePopUp.getP_servicehours().setEnabled(true);
				serviceSchedulePopUp.getP_servicemin().setEnabled(true);
				serviceSchedulePopUp.getP_ampm().setEnabled(true);
				// below setter added by vijay on 16-03-2017
				serviceSchedulePopUp.getP_serviceWeek().setEnabled(true);
			}
			
			schedulePanel.add(serviceSchedulePopUp);
			schedulePanel.show();
			/*
			 * nidhi
			 * 05-08-2017
			 *  for make schedule popup responsive
			 *  style css class declared in SimpleERP.css
			 */
			schedulePanel.getElement().addClassName("schedulepopup");
			/*
			 * end
			 */
			schedulePanel.center();
		}

		
		
	private void reactOnMondaytoFridayList(int selectedIndex, int selectedServiceWeekIndex) {
		if (selectedIndex == 0) {
			reactforTable();
			serviceSchedulePopUp.getP_serviceWeek().setSelectedIndex(0);
		} else {
			boolean unitFlag= false;
			specificday = 1;
			List<SalesLineItem> salesitemlis = form.saleslineitemtable.getDataprovider().getList();
			ArrayList<ServiceSchedule> serschelist = new ArrayList<ServiceSchedule>();
			serschelist.clear();
			List<ServiceSchedule> popuptablelist = serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList();

			boolean flag =false;
			boolean sameMonthserviceflag = false;
			Date temp =null;
			
			for (int i = 0; i < salesitemlis.size(); i++) {
//				int qty = 0;
//				qty = (int) salesitemlis.get(i).getQty();
				
				/**
				 * Date : 29-03-2017 By ANil
				 */
				boolean branchSchFlag=false;
				int qty=0;
				
				
				/**
				 * Date 02-05-2018 
				 * Developer : Vijay
				 * Des :- Below code is customer branches stored in string so it can not load more than 120 branches
				 * so i have updated below code using hashmap.
				 */
				
//				/**
//				 * Date : 03-04-2017 By ANIL
//				 */
////				ArrayList<BranchWiseScheduling> branchSchedulingList=AppUtility.getBranchSchedulingList(salesitemlis.get(i).getBranchSchedulingInfo());
//				
//				/**
//				 * Date : 12-06-2017 BY ANIl
//				 */
//				String[] branchArray=new String[10];
//				
//				if(salesitemlis.get(i).getBranchSchedulingInfo()!=null
//						&&!salesitemlis.get(i).getBranchSchedulingInfo().equals("")){
//					branchArray[0]=salesitemlis.get(i).getBranchSchedulingInfo();
//				}
//				if(salesitemlis.get(i).getBranchSchedulingInfo1()!=null
//						&&!salesitemlis.get(i).getBranchSchedulingInfo1().equals("")){
//					branchArray[1]=salesitemlis.get(i).getBranchSchedulingInfo1();
//				}
//				if(salesitemlis.get(i).getBranchSchedulingInfo2()!=null
//						&&!salesitemlis.get(i).getBranchSchedulingInfo2().equals("")){
//					branchArray[2]=salesitemlis.get(i).getBranchSchedulingInfo2();
//				}
//				if(salesitemlis.get(i).getBranchSchedulingInfo3()!=null
//						&&!salesitemlis.get(i).getBranchSchedulingInfo3().equals("")){
//					branchArray[3]=salesitemlis.get(i).getBranchSchedulingInfo3();
//				}
//				if(salesitemlis.get(i).getBranchSchedulingInfo4()!=null
//						&&!salesitemlis.get(i).getBranchSchedulingInfo4().equals("")){
//					branchArray[4]=salesitemlis.get(i).getBranchSchedulingInfo4();
//				}
//				
//				if(salesitemlis.get(i).getBranchSchedulingInfo5()!=null
//						&&!salesitemlis.get(i).getBranchSchedulingInfo5().equals("")){
//					branchArray[5]=salesitemlis.get(i).getBranchSchedulingInfo5();
//				}
//				if(salesitemlis.get(i).getBranchSchedulingInfo6()!=null
//						&&!salesitemlis.get(i).getBranchSchedulingInfo6().equals("")){
//					branchArray[6]=salesitemlis.get(i).getBranchSchedulingInfo6();
//				}
//				if(salesitemlis.get(i).getBranchSchedulingInfo7()!=null
//						&&!salesitemlis.get(i).getBranchSchedulingInfo7().equals("")){
//					branchArray[7]=salesitemlis.get(i).getBranchSchedulingInfo7();
//				}
//				if(salesitemlis.get(i).getBranchSchedulingInfo8()!=null
//						&&!salesitemlis.get(i).getBranchSchedulingInfo8().equals("")){
//					branchArray[8]=salesitemlis.get(i).getBranchSchedulingInfo8();
//				}
//				if(salesitemlis.get(i).getBranchSchedulingInfo9()!=null
//						&&!salesitemlis.get(i).getBranchSchedulingInfo9().equals("")){
//					branchArray[9]=salesitemlis.get(i).getBranchSchedulingInfo9();
//				}
//				/**
//				 * End
//				 */
//				ArrayList<BranchWiseScheduling> branchSchedulingList=AppUtility.getBranchSchedulingList(branchArray);
				
				/** Date 02-05-2018 By vijay for updated branch scheduling using hashmap **/
				ArrayList<BranchWiseScheduling> branchSchedulingList = salesitemlis.get(i).getCustomerBranchSchedulingInfo().get(salesitemlis.get(i).getProductSrNo());
				
				/**
				 * ends here
				 */
				
				if(branchSchedulingList!=null
						&&branchSchedulingList.size()!=0){
					branchSchFlag=true;
					qty=branchSchedulingList.size();
				}else{
					qty=(int) salesitemlis.get(i).getQty();
				}
				
				/**
				 * End
				 */
				if(branchSchFlag){
					for (int k = 0; k < qty; k++) {
						if(branchSchedulingList.get(k).isCheck()==true){
						long noServices = (long) (salesitemlis.get(i).getNumberOfServices());
						int noOfdays = salesitemlis.get(i).getDuration();
//						int interval = (int) (noOfdays / salesitemlis.get(i).getNumberOfServices());
						
						/*** Date 28 sep 2017 added by vijay  for scheduling proper calculation with precision number
						 *  for interval  
						 */
						double nodays =  noOfdays;
						double noofservices= salesitemlis.get(i).getNumberOfServices();
						double interval =  nodays / noofservices;
						double tempinterval = interval;
						double tempvalue=interval;
						/******************** ends here ********************/
						
						Date servicedate = salesitemlis.get(i).getStartDate();
						Date d = new Date(servicedate.getTime());
	
						Date productEndDate = new Date(servicedate.getTime());
						CalendarUtil.addDaysToDate(productEndDate, noOfdays);
						Date prodenddate = new Date(productEndDate.getTime());
						productEndDate = prodenddate;
	
						for (int j = 0; j < noServices; j++) {
	
							ServiceSchedule scheduleEntity = new ServiceSchedule();
	
							Date Stardaate = null;
							Stardaate = salesitemlis.get(i).getStartDate();
	
							scheduleEntity.setSerSrNo(salesitemlis.get(i).getProductSrNo());
							scheduleEntity.setScheduleStartDate(Stardaate);
							scheduleEntity.setScheduleProdId(salesitemlis.get(i).getPrduct().getCount());
							scheduleEntity.setScheduleProdName(salesitemlis.get(i).getProductName());
							scheduleEntity.setScheduleDuration(salesitemlis.get(i).getDuration());
							scheduleEntity.setScheduleNoOfServices(salesitemlis.get(i).getNumberOfServices());
							scheduleEntity.setScheduleServiceNo(j + 1);
	
							scheduleEntity.setScheduleProdStartDate(Stardaate);
							scheduleEntity.setScheduleProdEndDate(productEndDate);
	
							scheduleEntity.setServicingBranch(form.olbbBranch.getValue());
							scheduleEntity.setTotalServicingTime(salesitemlis.get(i).getServicingTIme());
	
							String calculatedServiceTime = "";
							if (popuptablelist.size() != 0) {
								calculatedServiceTime = popuptablelist.get(j).getScheduleServiceTime();
							} else {
								calculatedServiceTime = "Flexible";
							}
	
							scheduleEntity.setScheduleServiceTime(calculatedServiceTime);
	
//							if (form.customerbranchlist.size() == qty) {
//								scheduleEntity.setScheduleProBranch(form.customerbranchlist.get(k).getBusinessUnitName());
//								if (form.customerbranchlist.get(k).getBranch() != null) {
//									scheduleEntity.setServicingBranch(form.customerbranchlist.get(k).getBranch());
//								} else {
//									scheduleEntity.setServicingBranch(form.olbbBranch.getValue());
//								}
//							} else if (form.customerbranchlist.size() == 0) {
//								scheduleEntity.setScheduleProBranch("Service Address");
//								scheduleEntity.setServicingBranch(form.olbbBranch.getValue());
//							} else {
//								scheduleEntity.setScheduleProBranch(null);
//							}
							
							scheduleEntity.setScheduleProBranch(branchSchedulingList.get(k).getBranchName());
							if (branchSchedulingList.get(k).getServicingBranch()!=null) {
								scheduleEntity.setServicingBranch(branchSchedulingList.get(k).getServicingBranch());
							} 
	
							String dayOfWeek1 = format.format(d);
							int adate = Integer.parseInt(dayOfWeek1);
							int adday1 = selectedIndex - 1;
							
							int adday = 0;
							if (adate <= adday1) {
								adday = (adday1 - adate);
							} else {
								adday = (7 - adate + adday1);
							}
							Date newDate = new Date(d.getTime());
							CalendarUtil.addDaysToDate(newDate, adday);
	
							Date day = new Date(newDate.getTime());
							
							/**
							 * Date 8-03-2017
							 * added by vijay for scheduling service with week day and week
							 */
							
							if(selectedServiceWeekIndex!=0){
								
								/**
								 * Date 1 jun 2017 added by vijay for service getting same month 
								 */
								if(temp!=null){
									String currentServiceMonth =  DateTimeFormat.getFormat( "d-M-yyyy" ).format( day ).split("-")[1];
									String lastserviceDateMonth = DateTimeFormat.getFormat( "d-M-yyyy" ).format( temp ).split("-")[1];
									if(currentServiceMonth.equals(lastserviceDateMonth)){
										sameMonthserviceflag =true;
									}else{
										sameMonthserviceflag= false;
									}
								}
								
								if(sameMonthserviceflag){
									CalendarUtil.addDaysToDate(day, 10);
								}
								/**
								 * ends here
								 */
								
								CalendarUtil.setToFirstDayOfMonth(day);

								if(selectedServiceWeekIndex==1){
								 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);
								if(day.before(Stardaate)){
									if(j==0){
										day =Stardaate;
									}
									else{
										flag = true;
									}
								}
								if(flag || j>0){
									CalendarUtil.setToFirstDayOfMonth(day);
									 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);;
								}
							 }
							else if(selectedServiceWeekIndex==2){
									CalendarUtil.addDaysToDate(day, 7);
									 day = AppUtility.getCalculatedDatewithWeek(day, adday1);
									if(day.before(Stardaate)){
										if(j==0){
											day =Stardaate;
										}
										else{
											flag = true;
										}
									}
									if(flag || j>0){
										CalendarUtil.setToFirstDayOfMonth(day);
										CalendarUtil.addDaysToDate(day, 7);
										 day = AppUtility.getCalculatedDatewithWeek(day, adday1);;
									}
							}else if(selectedServiceWeekIndex==3){
									CalendarUtil.addDaysToDate(day, 14);
									 day = AppUtility.getCalculatedDatewithWeek(day, adday1);
									if(day.before(Stardaate)){
										if(j==0){
											day =Stardaate;
										}
										else{
											flag = true;
										}
									}
									if(flag || j>0){
										CalendarUtil.setToFirstDayOfMonth(day);
										CalendarUtil.addDaysToDate(day, 14);
										 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);;
									}
							}
							else if(selectedServiceWeekIndex==4){
									CalendarUtil.addDaysToDate(day, 21);

									 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);
									if(day.before(Stardaate)){
										if(j==0){
											day =Stardaate;
										}
										else{
											flag = true;
										}
									}
									if(flag || j>0){
										CalendarUtil.setToFirstDayOfMonth(day);
										CalendarUtil.addDaysToDate(day, 21);
										 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);;
									}
							 }
								 temp = day;
								 
								 /**
								  * Date 26 jun 2017 added by vijay 
								  * for getting issues in 4 services and 6 services gap  with day selection
								  */
									
									CalendarUtil.addDaysToDate(d, (int)interval);
									int days =0;
									String serviceStartDate =  DateTimeFormat.getFormat( "d-M-yyyy" ).format( Stardaate ).split("-")[0];
									if(serviceStartDate.equals("1")){
										String currentServiceDate =  DateTimeFormat.getFormat( "d-M-yyyy" ).format( d ).split("-")[0];
										 days = AppUtility.checkDateMonthOFEndAndAddDays(currentServiceDate,d);
										CalendarUtil.addDaysToDate(d, days);

									}
									/**
									 * ends here
									 */
							}
							/**
							 * ends here
							 */
								
							if (productEndDate.before(day) == false) {
								scheduleEntity.setScheduleServiceDate(day);
							} else {
								scheduleEntity.setScheduleServiceDate(productEndDate);
							}
							
							/*** Date 28 sep 2017 added by vijay for interval calculation *************/
							int interval2 =(int) Math.round(interval);


							//Date 26 jun 2017 aaded by vijay this only when week is not selected
							if(selectedServiceWeekIndex==0){
								CalendarUtil.addDaysToDate(d, interval2);
							}  // ends here
							
							Date tempdate = new Date(d.getTime());
							d = tempdate;
							
							
							/************* Date 28 sep 2017 added by vijay for scheduling interval calculation with including precision using roundoff*********************/
							interval =(interval+tempinterval)-tempvalue;
						    tempvalue = Math.round(interval);
						    /**************** ends here **********************************/;
						    
						    
							scheduleEntity.setScheduleServiceDay(ContractForm.serviceDay(scheduleEntity.getScheduleServiceDate()));
							//  Vijay addd this code 
							scheduleEntity.setServiceRemark(salesitemlis.get(i).getRemark());
							/**
							 * Date 03-04-2017 added by vijay for week number
							 */
							int weeknumber = AppUtility.getweeknumber(scheduleEntity.getScheduleServiceDate());
							scheduleEntity.setWeekNo(weeknumber);
							
							/**
							 * ends here
							 */
							 /***
							  * nidhi *:*:*
							  * 18-09-2018
							  * for map bill product
							  * if any update please update in else conditions also
							  */
							 if(LoginPresenter.billofMaterialActive){
								 
								 BillOfMaterial billPrDt = AppUtility.verifyBillofMaterilProd(salesitemlis.get(i).getPrduct());
								 if(billPrDt!=null){
									 
									 
									 UnitConversionUtility unitConver = new UnitConversionUtility();

									 
									 if(branchSchedulingList.get(k).getUnitOfMeasurement() != null && unitConver.varifyUnitConversion(billPrDt, branchSchedulingList.get(k).getUnitOfMeasurement())){
										 List<ServiceSchedule> seList = new ArrayList<ServiceSchedule>();
										 seList.add(scheduleEntity);
										 seList = unitConver.getServiceitemProList(seList,salesitemlis.get(i).getPrduct(),salesitemlis.get(i),billPrDt);
										 if(seList!=null && seList.size()>0)
											 scheduleEntity = seList.get(0);
										 
									 }else{
										 unitFlag = true;
//										 form.showDialogMessage("Unit of service area and product group area is differeant and conversion of unit is not avaliable.");
									 }
									 
									 
									 
									 
								 }
							 }
							 /**
							  * end
							  */
							serschelist.add(scheduleEntity);
						}
					}
					}
				}else{
					for (int k = 0; k < qty; k++) {
						long noServices = (long) (salesitemlis.get(i).getNumberOfServices());
						int noOfdays = salesitemlis.get(i).getDuration();
//						int interval = (int) (noOfdays / salesitemlis.get(i).getNumberOfServices());
						
						/*** Date 28 sep 2017 added by vijay  for scheduling proper calculation with precision number
						 *  for interval  
						 */
						double nodays =  noOfdays;
						double noofservices= salesitemlis.get(i).getNumberOfServices();
						double interval =  nodays / noofservices;
						double tempinterval = interval;
						double tempvalue=interval;
						/******************** ends here ********************/
						
						Date servicedate = salesitemlis.get(i).getStartDate();
						Date d = new Date(servicedate.getTime());
	
						Date productEndDate = new Date(servicedate.getTime());
						CalendarUtil.addDaysToDate(productEndDate, noOfdays);
						Date prodenddate = new Date(productEndDate.getTime());
						productEndDate = prodenddate;
	
						for (int j = 0; j < noServices; j++) {
	
							ServiceSchedule scheduleEntity = new ServiceSchedule();
	
							Date Stardaate = null;
							Stardaate = salesitemlis.get(i).getStartDate();
	
							scheduleEntity.setSerSrNo(salesitemlis.get(i).getProductSrNo());
	
							scheduleEntity.setScheduleStartDate(Stardaate);
							scheduleEntity.setScheduleProdId(salesitemlis.get(i).getPrduct().getCount());
							scheduleEntity.setScheduleProdName(salesitemlis.get(i).getProductName());
							scheduleEntity.setScheduleDuration(salesitemlis.get(i).getDuration());
							scheduleEntity.setScheduleNoOfServices(salesitemlis.get(i).getNumberOfServices());
							scheduleEntity.setScheduleServiceNo(j + 1);
	
							scheduleEntity.setScheduleProdStartDate(Stardaate);
							scheduleEntity.setScheduleProdEndDate(productEndDate);
	
							scheduleEntity.setServicingBranch(form.olbbBranch.getValue());
							scheduleEntity.setTotalServicingTime(salesitemlis.get(i).getServicingTIme());
	
							String calculatedServiceTime = "";
							if (popuptablelist.size() != 0) {
								calculatedServiceTime = popuptablelist.get(j).getScheduleServiceTime();
							} else {
								calculatedServiceTime = "Flexible";
							}
	
							scheduleEntity.setScheduleServiceTime(calculatedServiceTime);
	
							if (form.customerbranchlist.size() == qty) {
								scheduleEntity.setScheduleProBranch(form.customerbranchlist.get(k).getBusinessUnitName());
								/**
								 * rohan added this code for automatic servicing
								 * branch setting for NBHC
								 */
								if (form.customerbranchlist.get(k).getBranch() != null) {
									scheduleEntity.setServicingBranch(form.customerbranchlist.get(k).getBranch());
								} else {
									scheduleEntity.setServicingBranch(form.olbbBranch.getValue());
								}
								/**
								 * ends here
								 */
							} else if (form.customerbranchlist.size() == 0) {
								scheduleEntity.setScheduleProBranch("Service Address");
								/**
								 * rohan added this code for automatic servicing
								 * branch setting for NBHC
								 */
								scheduleEntity.setServicingBranch(form.olbbBranch.getValue());
								/**
								 * ends here
								 */
							} else {
								scheduleEntity.setScheduleProBranch(null);
							}
	
							String dayOfWeek1 = format.format(d);
							int adate = Integer.parseInt(dayOfWeek1);
							int adday1 = selectedIndex - 1;
							
							int adday = 0;
							if (adate <= adday1) {
								adday = (adday1 - adate);
							} else {
								adday = (7 - adate + adday1);
							}
							Date newDate = new Date(d.getTime());
							CalendarUtil.addDaysToDate(newDate, adday);
	
							Date day = new Date(newDate.getTime());
							
							/**
							 * Date 8-03-2017
							 * added by vijay for scheduling service with week day and week
							 */
							if(selectedServiceWeekIndex!=0){
								
								/**
								 * Date 1 jun 2017 added by vijay for service getting same month 
								 */
								if(temp!=null){
									String currentServiceMonth =  DateTimeFormat.getFormat( "d-M-yyyy" ).format( day ).split("-")[1];
									String lastserviceDateMonth = DateTimeFormat.getFormat( "d-M-yyyy" ).format( temp ).split("-")[1];
									if(currentServiceMonth.equals(lastserviceDateMonth)){
										sameMonthserviceflag =true;
									}else{
										sameMonthserviceflag= false;
									}
								}
								
								if(sameMonthserviceflag){
									CalendarUtil.addDaysToDate(day, 10);
								}
								/**
								 * ends here
								 */
								
								CalendarUtil.setToFirstDayOfMonth(day);
								if(selectedServiceWeekIndex==1){
								 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);
								if(day.before(Stardaate)){
									if(j==0){
										day =Stardaate;
									}
									else{
										flag = true;
									}
								}
								if(flag || j>0){
									CalendarUtil.setToFirstDayOfMonth(day);
									 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);;
								}
							 }
							else if(selectedServiceWeekIndex==2){
									
									CalendarUtil.addDaysToDate(day, 7);

									 day = AppUtility.getCalculatedDatewithWeek(day, adday1);
									if(day.before(Stardaate)){
										if(j==0){
											day =Stardaate;
										}
										else{
											flag = true;
										}
									}
									if(flag || j>0){
										CalendarUtil.setToFirstDayOfMonth(day);
										CalendarUtil.addDaysToDate(day, 7);
										 day = AppUtility.getCalculatedDatewithWeek(day, adday1);;
									}
							}else if(selectedServiceWeekIndex==3){
									CalendarUtil.addDaysToDate(day, 14);
									 day = AppUtility.getCalculatedDatewithWeek(day, adday1);
									if(day.before(Stardaate)){
										if(j==0){
											day =Stardaate;
										}
										else{
											flag = true;
										}
									}
									if(flag || j>0){
										CalendarUtil.setToFirstDayOfMonth(day);
										CalendarUtil.addDaysToDate(day, 14);
										 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);;
									}
							}
							else if(selectedServiceWeekIndex==4){
									CalendarUtil.addDaysToDate(day, 21);

									 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);
									if(day.before(Stardaate)){
										if(j==0){
											day =Stardaate;
										}
										else{
											flag = true;
										}
									}
									if(flag || j>0){
										CalendarUtil.setToFirstDayOfMonth(day);
										CalendarUtil.addDaysToDate(day, 21);
										 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);;
									}
							     }
								
								 temp = day;
								 
								 /**
								  * Date 26 jun 2017 added by vijay 
								  * for getting issues in 4 services and 6 services gap 
								  */
									
									CalendarUtil.addDaysToDate(d, (int)interval);
									int days =0;
									String serviceStartDate =  DateTimeFormat.getFormat( "d-M-yyyy" ).format( Stardaate ).split("-")[0];
									if(serviceStartDate.equals("1")){
										String currentServiceDate =  DateTimeFormat.getFormat( "d-M-yyyy" ).format( d ).split("-")[0];
										 days = AppUtility.checkDateMonthOFEndAndAddDays(currentServiceDate,d);
										CalendarUtil.addDaysToDate(d, days);

									}
									
									/**
									 * ends here
									 */
							}
							/**
							 * ends here
							 */
							
							if (productEndDate.before(day) == false) {
								scheduleEntity.setScheduleServiceDate(day);
							} else {
								scheduleEntity.setScheduleServiceDate(productEndDate);
							}
							
							/*** Date 28 sep 2017 added by vijay for interval calculation *************/
							int interval2 =(int) Math.round(interval);
							
							//Date 26 jun if condtion added by vijay for applicable only weeak is not selected
							if(selectedServiceWeekIndex==0){
								CalendarUtil.addDaysToDate(d, interval2);
							}
							//ends here
							
							Date tempdate = new Date(d.getTime());
							d = tempdate;
							scheduleEntity.setScheduleServiceDay(ContractForm.serviceDay(scheduleEntity.getScheduleServiceDate()));
							//  Vijay addd this code 
							scheduleEntity.setServiceRemark(salesitemlis.get(i).getRemark());
							
							
							/************* Date 28 sep 2017 added by vijay for scheduling interval calculation with including precision using roundoff*********************/
							interval =(interval+tempinterval)-tempvalue;
						    tempvalue = Math.round(interval);
						    /**************** ends here **********************************/;
						    
						    
							
							/**
							 * Date 03-04-2017 added by vijay for week number
							 */
							int weeknumber = AppUtility.getweeknumber(scheduleEntity.getScheduleServiceDate());
							scheduleEntity.setWeekNo(weeknumber);
							
							/**
							 * ends here
							 */
							 /***
							  * nidhi *:*:*
							  * 18-09-2018
							  * for map bill product
							  * if any update please update in else conditions also
							  */
							 if(LoginPresenter.billofMaterialActive){
								 
								 BillOfMaterial billPrDt = AppUtility.verifyBillofMaterilProd(salesitemlis.get(i).getPrduct());
								 if(billPrDt!=null){
									 
									 
									 UnitConversionUtility unitConver = new UnitConversionUtility();

									 
									 if(branchSchedulingList.get(k).getUnitOfMeasurement() != null && unitConver.varifyUnitConversion(billPrDt, branchSchedulingList.get(k).getUnitOfMeasurement())){
										 List<ServiceSchedule> seList = new ArrayList<ServiceSchedule>();
										 seList.add(scheduleEntity);
										 seList = unitConver.getServiceitemProList(seList,salesitemlis.get(i).getPrduct(),salesitemlis.get(i),billPrDt);
										 if(seList!=null && seList.size()>0)
											 scheduleEntity = seList.get(0);
										 
									 }else{
										 unitFlag =true;
//										 form.showDialogMessage("Unit of service area and product group area is differeant and conversion of unit is not avaliable.");
									 }
									 
									 
									 
									 
								 }
							 }
							 /**
							  * end
							  */
							serschelist.add(scheduleEntity);
						}
					}
				}

				serviceSchedulePopUp.getPopScheduleTable().getDataprovider().setList(serschelist);
				/**
				 * Date : 25-11-2017 BY ANIL
				 */
				form.globalScheduleServiceList=new ArrayList<ServiceSchedule>();
				form.globalScheduleServiceList=serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList();
				/**
				 * End
				 */
				if(unitFlag){
					 form.showDialogMessage("Unit of service are and product group detail area not mapped at branch level. Or conversion not available");
				}
			}
		}
	}

			
	private void reactOnHoursList() {

		List<ServiceSchedule> scheduleLis=this.serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList();
		ArrayList<ServiceSchedule> arrOfServices=new ArrayList<ServiceSchedule>();
		for(int i=0;i<scheduleLis.size();i++)
		{
			ServiceSchedule serviceScheduleEntity=new ServiceSchedule();
			
			String serviceHrs=serviceSchedulePopUp.getP_servicehours().getItemText(serviceSchedulePopUp.getP_servicehours().getSelectedIndex());
			String serviceMin=serviceSchedulePopUp.getP_servicemin().getItemText(serviceSchedulePopUp.getP_servicemin().getSelectedIndex());
			String serviceAmPm=serviceSchedulePopUp.getP_ampm().getItemText(serviceSchedulePopUp.getP_ampm().getSelectedIndex());
			String calculatedServiceTime=serviceHrs+":"+serviceMin+""+serviceAmPm;
			System.out.println("Sout"+calculatedServiceTime);
			serviceScheduleEntity.setScheduleServiceTime(calculatedServiceTime);
			serviceScheduleEntity.setScheduleProdStartDate(serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList().get(i).getScheduleProdStartDate());
			serviceScheduleEntity.setScheduleProdId(serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList().get(i).getScheduleProdId());
			serviceScheduleEntity.setScheduleProdName(serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList().get(i).getScheduleProdName());
			serviceScheduleEntity.setScheduleDuration(serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList().get(i).getScheduleDuration());
			serviceScheduleEntity.setScheduleNoOfServices(serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList().get(i).getScheduleNoOfServices());
			serviceScheduleEntity.setScheduleServiceNo(serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList().get(i).getScheduleServiceNo());
			serviceScheduleEntity.setScheduleServiceDate(serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList().get(i).getScheduleServiceDate());
			serviceScheduleEntity.setScheduleProdEndDate(serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList().get(i).getScheduleProdEndDate());
			serviceScheduleEntity.setScheduleStartDate(serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList().get(i).getScheduleStartDate());
			serviceScheduleEntity.setScheduleProBranch(serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList().get(i).getScheduleProBranch());
			serviceScheduleEntity.setScheduleServiceDay(serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList().get(i).getScheduleServiceDay());
			serviceScheduleEntity.setServicingBranch(form.olbbBranch.getValue());
			serviceScheduleEntity.setTotalServicingTime(serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList().get(i).getTotalServicingTime());
			
			serviceScheduleEntity.setSerSrNo(serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList().get(i).getSerSrNo());
			serviceScheduleEntity.setServiceRemark(serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList().get(i).getServiceRemark());
			serviceScheduleEntity.setCheckService(serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList().get(i).isCheckService());
			serviceScheduleEntity.setWeekNo(serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList().get(i).getWeekNo());

			arrOfServices.add(serviceScheduleEntity);
		}
		serviceSchedulePopUp.getPopScheduleTable().getDataprovider().setList(arrOfServices);
	}
			
	protected void valueChangeMethodLogicForScheduling()
	{
		
		
		serviceSchedulePopUp.getP_customise().setValue(false);
		serviceSchedulePopUp.getP_default().setValue(false);
		serviceSchedulePopUp.getP_mondaytofriday().setSelectedIndex(0);

		serviceSchedulePopUp.getP_servicehours().setSelectedIndex(0);
		serviceSchedulePopUp.getP_servicemin().setSelectedIndex(0);
		serviceSchedulePopUp.getP_ampm().setSelectedIndex(0);

		serviceSchedulePopUp.getP_servicehours().setEnabled(true);
		serviceSchedulePopUp.getP_servicemin().setEnabled(true);
		serviceSchedulePopUp.getP_ampm().setEnabled(true);
		serviceSchedulePopUp.getP_customise().setEnabled(true);

		//04-04-2017 added by vijay 
		serviceSchedulePopUp.getP_serviceWeek().setSelectedIndex(0);
				
		int selectedExcludDay = serviceSchedulePopUp.getE_mondaytofriday().getSelectedIndex();
		
		List<SalesLineItem> salesitemlis = form.saleslineitemtable.getDataprovider().getList();
		ArrayList<ServiceSchedule> serschelist = new ArrayList<ServiceSchedule>();
		serschelist.clear();

		List<ServiceSchedule> popuptablelist = serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList();

		for (int i = 0; i < salesitemlis.size(); i++) {


//			int qty = 0;
//			qty = (int) salesitemlis.get(i).getQty();
			
			/**
			 * Date : 29-03-2017 By ANil
			 */
			boolean branchSchFlag=false;
			int qty=0;
			
			/**
			 * Date 02-05-2018 
			 * Developer : Vijay
			 * Des :- Below code is customer branches stored in string so it can not load more than 120 branches
			 * so i have updated below code using hashmap. 
			 */
			
			
			/** Date 02-05-2018 By vijay for updated branch scheduling using hashmap **/
			ArrayList<BranchWiseScheduling> branchSchedulingList = salesitemlis.get(i).getCustomerBranchSchedulingInfo().get(salesitemlis.get(i).getProductSrNo());
			
		/**
		 * ends here	
		 */
			if(branchSchedulingList!=null
					&&branchSchedulingList.size()!=0){
				branchSchFlag=true;
				qty=branchSchedulingList.size();
			}else{
				qty=(int) salesitemlis.get(i).getQty();
			}
			
			/**
			 * End
			 */
			if(branchSchFlag){
				for (int k = 0; k < qty; k++) {
					
					if(branchSchedulingList.get(k).isCheck()==true){
						
				/*	long noServices = (long) (item.getNumberOfServices());
					int noOfdays = item.getDuration();
					double nodays =  noOfdays;
					double noofservices= item.getNumberOfServices();
					int inreval = nodays / noofservices;
					if(serviceSchedulePopUp.getP_interval().getValue() != null || serviceSchedulePopUp.getP_interval().getValue() != 0){
						inreval =  serviceSchedulePopUp.getP_interval().getValue().doubleValue();
					}
					int noOfdays = salesitemlis.get(i).getDuration();*/

						
					long noServices = (long) (salesitemlis.get(i).getNumberOfServices());
					int noOfdays = salesitemlis.get(i).getDuration();
					double nodays =  noOfdays;
					double noofservices= salesitemlis.get(i).getNumberOfServices();
//					int inreval = serviceSchedulePopUp.getP_interval().getValue();
					int inreval =  (int) (nodays / noofservices);
					if(serviceSchedulePopUp.getP_interval().getValue() != null && serviceSchedulePopUp.getP_interval().getValue() != 0){
						inreval =  serviceSchedulePopUp.getP_interval().getValue();
					}	
					Date servicedate = new Date(salesitemlis.get(i).getStartDate().getTime());
	
					Date productEndDate = new Date(servicedate.getTime());
					CalendarUtil.addDaysToDate(productEndDate, noOfdays);
					Date prodenddate = new Date(productEndDate.getTime());
					productEndDate = prodenddate;
					
					

					/**
					 * nidhi
					 *  ))..
					 *  for exclude day.
					 */
				/*	int startday = servicedate.getDay();
					
					if(selectedExcludDay !=0  && startday == selectedExcludDay-1){
						CalendarUtil.addDaysToDate(servicedate, 1);
					}
					Date d = new Date(servicedate.getTime());
					int lastday = productEndDate.getDay();
					
					if(selectedExcludDay !=0  && lastday == selectedExcludDay-1){
						CalendarUtil.addDaysToDate(productEndDate, -1);
					}
					*/
					/**
					 * end
					 */
					
					/**
					 * nidhi
					 *  ))..
					 *  for exclude day.
					 */
					productEndDate = calculateEndDate(servicedate, noOfdays-1);
					int startday = servicedate.getDay();
					
					if(selectedExcludDay !=0  && startday == selectedExcludDay-1){
						CalendarUtil.addDaysToDate(servicedate, 1);
					}
//					Date d = new Date(servicedate.getTime());
					
					if(selectedExcludDay !=0 ){
						CalendarUtil.addDaysToDate(productEndDate, -1);
					}
					
					int lastday = productEndDate.getDay();
					
					if(selectedExcludDay !=0  && lastday == selectedExcludDay-1){
						CalendarUtil.addDaysToDate(productEndDate, -1);
					}
					Date d = new Date(servicedate.getTime());
					/**
					 * end
					 */
					Date tempdate = new Date();
					tempdate = servicedate;
					for (int j = 0; j < noServices; j++) {
						/*if (j > 0) {
							CalendarUtil.addDaysToDate(d, inreval);
							tempdate = new Date(d.getTime());
						}*/
						

						if (j > 0) {
							CalendarUtil.addDaysToDate(d, inreval);
						}
	
						/**
						 * nidhi
						 *  ))..
						 *  for exclude day.
						 */
						int serDay = d.getDay();
						if(selectedExcludDay !=0  && serDay == selectedExcludDay-1){
							CalendarUtil.addDaysToDate(d, 1);
						}
						
						tempdate = new Date(d.getTime());
						
						ServiceSchedule ssch = new ServiceSchedule();
						Date Stardaate = null;
						
						ssch.setSerSrNo(salesitemlis.get(i).getProductSrNo());
						Stardaate = salesitemlis.get(i).getStartDate();
						ssch.setScheduleStartDate(Stardaate);
						ssch.setScheduleProdId(salesitemlis.get(i).getPrduct().getCount());
						ssch.setScheduleProdName(salesitemlis.get(i).getProductName());
						ssch.setScheduleDuration(salesitemlis.get(i).getDuration());
						ssch.setScheduleNoOfServices(salesitemlis.get(i).getNumberOfServices());
						ssch.setScheduleServiceNo(j + 1);
						ssch.setScheduleProdStartDate(Stardaate);
						ssch.setScheduleProdEndDate(productEndDate);
						ssch.setServicingBranch(form.olbbBranch.getValue());
						ssch.setTotalServicingTime(salesitemlis.get(i).getServicingTIme());
						String calculatedServiceTime = "";
						if (popuptablelist.size() != 0) {
							calculatedServiceTime = popuptablelist.get(j).getScheduleServiceTime();
						} else {
							calculatedServiceTime = "Flexible";
						}
	
						//vijay
						ssch.setServiceRemark(salesitemlis.get(i).getRemark());
						ssch.setScheduleServiceTime(calculatedServiceTime);
	
						
						ssch.setScheduleProBranch(branchSchedulingList.get(k).getBranchName());
						if (branchSchedulingList.get(k).getServicingBranch()!=null) {
							ssch.setServicingBranch(branchSchedulingList.get(k).getServicingBranch());
						} 
						
						
	
						if (j == 0) {
							ssch.setScheduleServiceDate(servicedate);
							ssch.setScheduleServiceDay(ContractForm.serviceDay(ssch.getScheduleServiceDate()));
							serschelist.add(ssch);
						}
						if (j != 0) {
	
							if (productEndDate.after(d)) {
								ssch.setScheduleServiceDate(tempdate);
								ssch.setScheduleServiceDay(ContractForm.serviceDay(ssch.getScheduleServiceDate()));
								serschelist.add(ssch);
							} else {
								ssch.setScheduleServiceDate(productEndDate);
								ssch.setScheduleServiceDay(ContractForm.serviceDay(ssch.getScheduleServiceDate()));
								serschelist.add(ssch);
							}
						}
						/**
						 * Date 03-04-2017 added by vijay for week number
						 */
						int weeknumber = AppUtility.getweeknumber(ssch.getScheduleServiceDate());
						ssch.setWeekNo(weeknumber);
						 /***
						  * nidhi *:*:*
						  * 18-09-2018
						  * for map bill product
						  * if any update please update in else conditions also
						  */
						 if(LoginPresenter.billofMaterialActive && ssch.getServiceProductList()!=null){
							 ssch.setServiceProductList(ssch.getServiceProductList());
							 
						 }
						 /**
						  * end
						  */
						/**
						 * ends here
						 */
					}
				}
				}
				
			}else{
				for (int k = 0; k < qty; k++) {
					long noServices = (long) (salesitemlis.get(i).getNumberOfServices());
					int noOfdays = salesitemlis.get(i).getDuration();
					double nodays =  noOfdays;
					double noofservices= salesitemlis.get(i).getNumberOfServices();
					int inreval =  (int) (nodays / noofservices);
					if(serviceSchedulePopUp.getP_interval().getValue() != null && serviceSchedulePopUp.getP_interval().getValue() != 0){
						inreval =  serviceSchedulePopUp.getP_interval().getValue();
					}	
					Date servicedate = new Date(salesitemlis.get(i).getStartDate().getTime());
					Date d = new Date(servicedate.getTime());
	
					Date productEndDate = new Date(servicedate.getTime());
					CalendarUtil.addDaysToDate(productEndDate, noOfdays);
					Date prodenddate = new Date(productEndDate.getTime());
					productEndDate = prodenddate;
					
					/**
					 * nidhi
					 *  ))..
					 *  for exclude day.
					 */
					int startday = servicedate.getDay();
					
					if(selectedExcludDay !=0  && startday == selectedExcludDay-1){
						CalendarUtil.addDaysToDate(servicedate, 1);
					}
					int lastday = productEndDate.getDay();
					
					if(selectedExcludDay !=0  && lastday == selectedExcludDay-1){
						CalendarUtil.addDaysToDate(productEndDate, -1);
					}
					/**
					 * end
					 */
					Date tempdate = new Date();
					tempdate = servicedate;
					for (int j = 0; j < noServices; j++) {
	
						
						
						if (j > 0) {
							CalendarUtil.addDaysToDate(d, inreval);
						}
	
						/**
						 * nidhi
						 *  ))..
						 *  for exclude day.
						 */
						int serDay = d.getDay();
						if(selectedExcludDay !=0  && serDay == selectedExcludDay-1){
							CalendarUtil.addDaysToDate(d, 1);
						
						}
						
						tempdate = new Date(d.getTime());
						/**
						 * end
						 */
						
						ServiceSchedule ssch = new ServiceSchedule();
						Date Stardaate = null;
						
						ssch.setSerSrNo(salesitemlis.get(i).getProductSrNo());
						Stardaate = salesitemlis.get(i).getStartDate();
						ssch.setScheduleStartDate(Stardaate);
						ssch.setScheduleProdId(salesitemlis.get(i).getPrduct().getCount());
						ssch.setScheduleProdName(salesitemlis.get(i).getProductName());
						ssch.setScheduleDuration(salesitemlis.get(i).getDuration());
						ssch.setScheduleNoOfServices(salesitemlis.get(i).getNumberOfServices());
						ssch.setScheduleServiceNo(j + 1);
						ssch.setScheduleProdStartDate(Stardaate);
						ssch.setScheduleProdEndDate(prodenddate);
						ssch.setServicingBranch(form.olbbBranch.getValue());
						ssch.setTotalServicingTime(salesitemlis.get(i).getServicingTIme());
						String calculatedServiceTime = "";
						if (popuptablelist.size() != 0) {
							calculatedServiceTime = popuptablelist.get(j).getScheduleServiceTime();
						} else {
							calculatedServiceTime = "Flexible";
						}
	
						//vijay
						ssch.setServiceRemark(salesitemlis.get(i).getRemark());
						
						ssch.setScheduleServiceTime(calculatedServiceTime);
	
						if (form.customerbranchlist.size() == qty) {
							ssch.setScheduleProBranch(form.customerbranchlist.get(k).getBusinessUnitName());
							/**
							 * rohan added this code for automatic servicing branch
							 * setting for NBHC
							 */
							if (form.customerbranchlist.get(k).getBranch() != null) {
								ssch.setServicingBranch(form.customerbranchlist.get(k).getBranch());
							} else {
								ssch.setServicingBranch(form.olbbBranch.getValue());
							}
							/**
							 * ends here
							 */
	
						} else if (form.customerbranchlist.size() == 0) {
							ssch.setScheduleProBranch("Service Address");
	
							/**
							 * rohan added this code for automatic servicing branch
							 * setting for NBHC
							 */
							ssch.setServicingBranch(form.olbbBranch.getValue());
							/**
							 * ends here
							 */
	
						} else {
							ssch.setScheduleProBranch(null);
						}
					
						
	
						if (j == 0) {
							ssch.setScheduleServiceDate(servicedate);
							ssch.setScheduleServiceDay(ContractForm.serviceDay(ssch.getScheduleServiceDate()));
							serschelist.add(ssch);
						}
						if (j != 0) {
	
							if (productEndDate.after(d)) {
								ssch.setScheduleServiceDate(tempdate);
								ssch.setScheduleServiceDay(ContractForm.serviceDay(ssch.getScheduleServiceDate()));
								serschelist.add(ssch);
							} else {
								ssch.setScheduleServiceDate(productEndDate);
								ssch.setScheduleServiceDay(ContractForm.serviceDay(ssch.getScheduleServiceDate()));
								serschelist.add(ssch);
							}
						}
						
						/**
						 * Date 03-04-2017 added by vijay for week number
						 */
						int weeknumber = AppUtility.getweeknumber(ssch.getScheduleServiceDate());
						ssch.setWeekNo(weeknumber);
						 /***
						  * nidhi *:*:*
						  * 18-09-2018
						  * for map bill product
						  * if any update please update in else conditions also
						  */
						 if(LoginPresenter.billofMaterialActive && ssch.getServiceProductList()!=null){
							 ssch.setServiceProductList(ssch.getServiceProductList());
						 }
						 /**
						  * end
						  */
								/**
						 * ends here
						 */
					}
				}
		}

			serviceSchedulePopUp.getPopScheduleTable().getDataprovider().setList(serschelist);
			/**
			 * Date : 25-11-2017 BY ANIL
			 */
			form.globalScheduleServiceList=new ArrayList<ServiceSchedule>();
			form.globalScheduleServiceList=serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList();
			/**
			 * End
			 */
		}
	
	
	
}
			
			private void reactFordeleteproduct(int proid,int srNo) {
				
				int prodid1=proid;
				if(form.getSaleslineitemtable().getDataprovider().getList().size()!=0)
				{
					System.out.println("Before Updare Services Method");
					updateServices(prodid1,srNo);
				}
			}
			
		 void updateServices(int changeproductid,int srNo)
			{
				

				
				System.out.println("PRODUCT ID "+changeproductid+" SR NO. "+srNo);

				List<ServiceSchedule> origserviceitemlist = form.serviceScheduleTable.getDataprovider().getList();
				List<Boolean> serLis = new ArrayList<Boolean>();
				List<ServiceSchedule> newlist = new ArrayList<ServiceSchedule>();
				newlist.clear();

				for (int j = 0; j < origserviceitemlist.size(); j++) {
					boolean prodIdCheck = findProductId(origserviceitemlist.get(j)
							.getScheduleProdId(), origserviceitemlist.get(j).getSerSrNo());
					if (prodIdCheck == false) {
						serLis.add(false);
					} else {
						serLis.add(true);
					}
				}

				form.serviceScheduleTable.connectToLocal();
				boolean unitFlag = false;
				for (int k = 0; k < serLis.size(); k++) {

					System.out.println("boolean loop " + serLis.get(k));
					if (serLis.get(k)) {
//						System.out.println(" conditin for dele product");
//						System.out.println(" chnge product" + changeproductid);
//						System.out.println(" form pooup table product"
//								+ origserviceitemlist.get(k).getScheduleProdId());
						//
						// old code
						// if(changeproductid!=origserviceitemlist.get(k).getScheduleProdId()
						// && srNo != origserviceitemlist.get(k).getSerSrNo()){

						// new code by for rohan for same product add in contract
						if (srNo != origserviceitemlist.get(k).getSerSrNo()) {

							System.out.println(" conditin for dele product 11");

							ServiceSchedule serviceScheduleObj = new ServiceSchedule();

							serviceScheduleObj.setSerSrNo(origserviceitemlist.get(k)
									.getSerSrNo());

							serviceScheduleObj.setScheduleStartDate(origserviceitemlist
									.get(k).getScheduleStartDate());
							serviceScheduleObj.setScheduleProdId(origserviceitemlist
									.get(k).getScheduleProdId());
							serviceScheduleObj.setScheduleProdName(origserviceitemlist
									.get(k).getScheduleProdName());
							serviceScheduleObj.setScheduleDuration(origserviceitemlist
									.get(k).getScheduleDuration());
							serviceScheduleObj
									.setScheduleNoOfServices(origserviceitemlist.get(k)
											.getScheduleNoOfServices());
							serviceScheduleObj.setScheduleServiceNo(origserviceitemlist
									.get(k).getScheduleServiceNo());
							serviceScheduleObj
									.setScheduleServiceDate(origserviceitemlist.get(k)
											.getScheduleServiceDate());

							if (origserviceitemlist.get(k).getScheduleProdEndDate() != null) {
								serviceScheduleObj
										.setScheduleProdEndDate(origserviceitemlist
												.get(k).getScheduleProdEndDate());
							} else {
								serviceScheduleObj.setScheduleProdEndDate(new Date());
							}

							if (origserviceitemlist.get(k).getScheduleProdStartDate() != null) {
								serviceScheduleObj
										.setScheduleProdStartDate(origserviceitemlist
												.get(k).getScheduleProdStartDate());
							} else {
								serviceScheduleObj.setScheduleProdStartDate(new Date());
							}
							serviceScheduleObj
									.setScheduleServiceTime(origserviceitemlist.get(k)
											.getScheduleServiceTime());
							serviceScheduleObj.setPremisesDetails(origserviceitemlist
									.get(k).getPremisesDetails());
							serviceScheduleObj.setScheduleProBranch(origserviceitemlist
									.get(k).getScheduleProBranch());
							serviceScheduleObj
									.setScheduleServiceDay(origserviceitemlist.get(k)
											.getScheduleServiceDay());
							serviceScheduleObj.setServicingBranch(origserviceitemlist
									.get(k).getServicingBranch());
							serviceScheduleObj
									.setTotalServicingTime(origserviceitemlist.get(k)
											.getTotalServicingTime());
							// vijay
							serviceScheduleObj.setServiceRemark(origserviceitemlist
									.get(k).getServiceRemark());

							/**
							 * Date 03-04-2017 added by vijay for week number
							 */
							int weeknumber = AppUtility.getweeknumber(serviceScheduleObj.getScheduleServiceDate());
							serviceScheduleObj.setWeekNo(weeknumber);
							
							/**
							 * ends here
							 */
							 /***
							  * nidhi *:*:*
							  * 18-09-2018
							  * for map bill product
							  * if any update please update in else conditions also
							  */
							 if(LoginPresenter.billofMaterialActive){
								 serviceScheduleObj.setServiceProductList(serviceScheduleObj.getServiceProductList());
							 }
							 /**
							  * end
							  */
//							 /**
//							  * @author Anil
//							  * @since 06-06-2020
//							  * copying tat details
//							  */
//							 if(form.complainTatFlag){
//								form.copyComponentAndTatDetails(serviceScheduleObj,origserviceitemlist.get(k)); 
//							 }
							newlist.add(serviceScheduleObj);
						}
					}
				}

				List<SalesLineItem> formLis = form.getSaleslineitemtable().getDataprovider().getList();

				for (int i = 0; i < formLis.size(); i++) {

					if (changeproductid == formLis.get(i).getPrduct().getCount()
							&& srNo == formLis.get(i).getProductSrNo()) {
						System.out.println("CHANGED PRODUCT "+changeproductid+" SR "+srNo);
						/**
						 * Date : 29-03-2017 By ANil
						 */
						boolean branchSchFlag=false;
						
						int qty = 0;
//						qty = (int) formLis.get(i).getQty();
						
						/**
						 * Date 02-05-2018 
						 * Developer : Vijay
						 * Des :- Below code is customer branches stored in string so it can not load more than 120 branches
						 * so i have updated below code using hashmap. 
						 */
						
//						/**
//						 * Date : 03-04-2017 By ANIL
//						 */
////						ArrayList<BranchWiseScheduling> branchSchedulingList=AppUtility.getBranchSchedulingList(formLis.get(i).getBranchSchedulingInfo());
//						
//						/**
//						 * Date : 12-06-2017 BY ANIL
//						 */
//						String[] branchArray=new String[10];
//						if(formLis.get(i).getBranchSchedulingInfo()!=null
//								&&!formLis.get(i).getBranchSchedulingInfo().equals("")){
//							branchArray[0]=formLis.get(i).getBranchSchedulingInfo();
//						}
//						if(formLis.get(i).getBranchSchedulingInfo1()!=null
//								&&!formLis.get(i).getBranchSchedulingInfo1().equals("")){
//							branchArray[1]=formLis.get(i).getBranchSchedulingInfo1();
//						}
//						if(formLis.get(i).getBranchSchedulingInfo2()!=null
//								&&!formLis.get(i).getBranchSchedulingInfo2().equals("")){
//							branchArray[2]=formLis.get(i).getBranchSchedulingInfo2();
//						}
//						if(formLis.get(i).getBranchSchedulingInfo3()!=null
//								&&!formLis.get(i).getBranchSchedulingInfo3().equals("")){
//							branchArray[3]=formLis.get(i).getBranchSchedulingInfo3();
//						}
//						if(formLis.get(i).getBranchSchedulingInfo4()!=null
//								&&!formLis.get(i).getBranchSchedulingInfo4().equals("")){
//							branchArray[4]=formLis.get(i).getBranchSchedulingInfo4();
//						}
//						
//						if(formLis.get(i).getBranchSchedulingInfo5()!=null
//								&&!formLis.get(i).getBranchSchedulingInfo5().equals("")){
//							branchArray[5]=formLis.get(i).getBranchSchedulingInfo5();
//						}
//						if(formLis.get(i).getBranchSchedulingInfo6()!=null
//								&&!formLis.get(i).getBranchSchedulingInfo6().equals("")){
//							branchArray[6]=formLis.get(i).getBranchSchedulingInfo6();
//						}
//						if(formLis.get(i).getBranchSchedulingInfo7()!=null
//								&&!formLis.get(i).getBranchSchedulingInfo7().equals("")){
//							branchArray[7]=formLis.get(i).getBranchSchedulingInfo7();
//						}
//						if(formLis.get(i).getBranchSchedulingInfo8()!=null
//								&&!formLis.get(i).getBranchSchedulingInfo8().equals("")){
//							branchArray[8]=formLis.get(i).getBranchSchedulingInfo8();
//						}
//						if(formLis.get(i).getBranchSchedulingInfo9()!=null
//								&&!formLis.get(i).getBranchSchedulingInfo9().equals("")){
//							branchArray[9]=formLis.get(i).getBranchSchedulingInfo9();
//						}
//						
//						/**
//						 * End
//						 */
//						ArrayList<BranchWiseScheduling> branchSchedulingList=AppUtility.getBranchSchedulingList(branchArray);
						
						/** Date 02-05-2018 By vijay for updated branch scheduling using hashmap **/
						ArrayList<BranchWiseScheduling> branchSchedulingList = null;
						if(formLis.get(i).getCustomerBranchSchedulingInfo()!=null)
						 branchSchedulingList = formLis.get(i).getCustomerBranchSchedulingInfo().get(formLis.get(i).getProductSrNo());
					
						/**
						 * ends here	
						 */
						
						if(branchSchedulingList!=null&&branchSchedulingList.size()!=0){
							branchSchFlag=true;
							qty=branchSchedulingList.size();
						}else{
							qty=(int) formLis.get(i).getQty();
						}
						
						/**
						 * End
						 */
						if(branchSchFlag){
						for (int k = 0; k < qty; k++) {
							if(branchSchedulingList.get(k).isCheck()==true){

							System.out.println("condition == ");

							long noServices = (long) (formLis.get(i).getNumberOfServices());
							int noOfdays = formLis.get(i).getDuration();
							
							/********* below one line commented by vijay this interval calculation missing precision so services not getting created still end date ***********/
//							int interval = (int) (noOfdays / formLis.get(i).getNumberOfServices());
							
							/*** Date 28 sep 2017 added by vijay  for scheduling proper calculation with precision number
							 *  for interval  
							 */
							double days =  noOfdays;
							double noofservices= formLis.get(i).getNumberOfServices();
							double interval =  days / noofservices;
							System.out.println(Math.round(days/noofservices));
							System.out.println("interval "+interval);
							double temp = interval;
							double tempvalue=interval;
							/******************** ends here ********************/

							
							Date servicedate = formLis.get(i).getStartDate();
							Date d = new Date(servicedate.getTime());
							Date productEndDate = new Date(servicedate.getTime());
							CalendarUtil.addDaysToDate(productEndDate, noOfdays);
							Date prodenddate = new Date(productEndDate.getTime());
							productEndDate = prodenddate;
							Date tempdate = new Date();
							for (int j = 0; j < noServices; j++) {
								/**
								 * Date :09/06/2017   by Ajinkya 
								 * Nbhc Scheduling Issue 
								 */
//								if (j > 0) {
//									CalendarUtil.addDaysToDate(d, interval);
//									tempdate = new Date(d.getTime());
//								}
								
								/**
								 *  End Here
								 */

								ServiceSchedule ssch = new ServiceSchedule();
								Date Stardaate = formLis.get(i).getStartDate();

								ssch.setSerSrNo(formLis.get(i).getProductSrNo());

								ssch.setScheduleStartDate(Stardaate);
								ssch.setScheduleProdId(formLis.get(i).getPrduct()
										.getCount());
								ssch.setScheduleProdName(formLis.get(i)
										.getProductName());
								ssch.setScheduleDuration(formLis.get(i).getDuration());
								ssch.setScheduleNoOfServices(formLis.get(i)
										.getNumberOfServices());
								ssch.setScheduleServiceNo(j + 1);
								ssch.setScheduleProdStartDate(Stardaate);
								ssch.setScheduleProdEndDate(productEndDate);
								ssch.setScheduleServiceTime("Flexible");

								ssch.setServicingBranch(form.olbbBranch.getValue());
								ssch.setTotalServicingTime(formLis.get(i).getServicingTIme());

								ssch.setScheduleProBranch(branchSchedulingList.get(k).getBranchName());
								if (branchSchedulingList.get(k).getServicingBranch()!=null) {
									ssch.setServicingBranch(branchSchedulingList.get(k).getServicingBranch());
								} 
								
								int dayIndex=AppUtility.getDayIndex(branchSchedulingList.get(k).getDay());
								
								if(dayIndex!=0){
									String dayOfWeek1 = format.format(d);
									int adate = Integer.parseInt(dayOfWeek1);
									int adday1 = dayIndex - 1;
									int adday = 0;
									if (adate <= adday1) {
										adday = (adday1 - adate);
									} else {
										adday = (7 - adate + adday1);
									}
									
									Date newDate = new Date(d.getTime());
									CalendarUtil.addDaysToDate(newDate, adday);
									Date day = new Date(newDate.getTime());

									if (productEndDate.before(day) == false) {
										ssch.setScheduleServiceDate(day);
									} else {
										ssch.setScheduleServiceDate(productEndDate);
									}
									
									/*** added by vijay for interval calculation *************/
									int interval2 =(int) Math.round(interval);


									CalendarUtil.addDaysToDate(d, interval2);
									Date tempdate1 = new Date(d.getTime());
									d = tempdate1;
									ssch.setScheduleServiceDay(ContractForm.serviceDay(ssch.getScheduleServiceDate()));
									
									/************* added by vijay for scheduling interval calculation with including precision using roundoff*********************/
									interval =(interval+temp)-tempvalue;
								    tempvalue = Math.round(interval);
								    /**************** ends here **********************************/
			
								    
								}else{
									
									/*** added by vijay for interval calculation *************/
									int interval2 =(int) Math.round(interval);
									
									if (j > 0) {
										CalendarUtil.addDaysToDate(d, interval2);
										tempdate = new Date(d.getTime());
										
										/************* added by vijay for scheduling interval including calculation using roundoff *********************/
										interval =(interval+temp)-tempvalue;
									    tempvalue = Math.round(interval);
									    /**************** ends here **********************************/

									}
									
									if (j == 0) {
										ssch.setScheduleServiceDate(servicedate);
									}
									if (j != 0) {
										if (productEndDate.before(d) == false) {
											ssch.setScheduleServiceDate(tempdate);
										} else {
											ssch.setScheduleServiceDate(productEndDate);
										}
									}
								}

								

								ssch.setScheduleServiceDay(ContractForm.serviceDay(ssch.getScheduleServiceDate()));
								ssch.setServiceRemark(formLis.get(i).getRemark());
								/**
								 * Date 01-04-2017 added by vijay for week number
								 */
								int weeknumber = AppUtility.getweeknumber(ssch.getScheduleServiceDate());
								ssch.setWeekNo(weeknumber);
								
								/**
								 * ends here
								 */
								 /***
								  * nidhi *:*:*
								  * 18-09-2018
								  * for map bill product
								  * if any update please update in else conditions also
								  */
								 if(LoginPresenter.billofMaterialActive){
									 
									 BillOfMaterial billPrDt = AppUtility.verifyBillofMaterilProd(formLis.get(i).getPrduct());
									 if(billPrDt!=null){
										 
										 
										 UnitConversionUtility unitConver = new UnitConversionUtility();

										 
										 if(branchSchedulingList.get(k).getUnitOfMeasurement() != null && unitConver.varifyUnitConversion(billPrDt, branchSchedulingList.get(k).getUnitOfMeasurement())){
											 List<ServiceSchedule> seList = new ArrayList<ServiceSchedule>();
											 seList.add(ssch);
											 seList = unitConver.getServiceitemProList(seList,formLis.get(i).getPrduct(),formLis.get(i),billPrDt);
											 if(seList!=null && seList.size()>0)
												 ssch = seList.get(0);
											 
										 }else{
											 unitFlag = true;
//											 form.showDialogMessage("Unit of service area and product group area is differeant and conversion of unit is not avaliable.");
										 }
										 
										 
										 
										 
									 }
								 }
								 /**
								  * end
								  */
								newlist.add(ssch);
								
//								/**
//								 * @author Anil
//								 * @since 06-06-2020
//								 * Updating tat and component details on scheduling object
//								 */
//								if(form.complainTatFlag){
//									List<ServiceSchedule>list=form.updateSchedulingListComponentWise(ssch, branchSchedulingList.get(k));
//									if(list!=null&&list.size()!=0){
//										newlist.addAll(list);
//									}
//								}
							}
						}

						}
						}
						else{
							
							for (int k = 0; k < qty; k++) {
								
								long noServices = (long) (formLis.get(i).getNumberOfServices());
								int noOfdays = formLis.get(i).getDuration();
								/********* below one line commented by vijay this interval calculation missing precision so services not getting created still end date ***********/
//								int interval = (int) (noOfdays / formLis.get(i).getNumberOfServices());
								
								/*** Date 28 sep 2017 below code added by vijay interval calculation with precision and using roundoff ******************/
								double days =  noOfdays;
								double noofservices= formLis.get(i).getNumberOfServices();
								double interval =  days / noofservices;
								System.out.println(Math.round(days/noofservices));
								System.out.println("interval "+interval);
								double temp = interval;
								double tempvalue=interval;
								/******************** ends here ********************/
								
								Date servicedate = formLis.get(i).getStartDate();
								Date d = new Date(servicedate.getTime());
								Date productEndDate = new Date(servicedate.getTime());
								CalendarUtil.addDaysToDate(productEndDate, noOfdays);
								Date prodenddate = new Date(productEndDate.getTime());
								productEndDate = prodenddate;
								Date tempdate = new Date();
								
								for (int j = 0; j < noServices; j++) {
									
									/*** added by vijay for interval calculation *************/
									int interval2 =(int) Math.round(interval);
									
									if (j > 0) {
										CalendarUtil.addDaysToDate(d, interval2);
										tempdate = new Date(d.getTime());
										
										/************* added by vijay *********************/
										interval =(interval+temp)-tempvalue;
									    tempvalue = Math.round(interval);
									    /************** ends here ************************/
									}
			
									ServiceSchedule ssch = new ServiceSchedule();
									Date Stardaate = formLis.get(i).getStartDate();
			
									ssch.setSerSrNo(formLis.get(i).getProductSrNo());
									ssch.setScheduleStartDate(Stardaate);
									ssch.setScheduleProdId(formLis.get(i).getPrduct().getCount());
									ssch.setScheduleProdName(formLis.get(i).getProductName());
									ssch.setScheduleDuration(formLis.get(i).getDuration());
									ssch.setScheduleNoOfServices(formLis.get(i).getNumberOfServices());
									ssch.setScheduleServiceNo(j + 1);
									ssch.setScheduleProdStartDate(Stardaate);
									ssch.setScheduleProdEndDate(productEndDate);
									ssch.setScheduleServiceTime("Flexible");
			
									ssch.setServicingBranch(form.olbbBranch.getValue());
									ssch.setTotalServicingTime(formLis.get(i).getServicingTIme());
			
									if (j == 0) {
										ssch.setScheduleServiceDate(servicedate);
									}
									if (j != 0) {
										if (productEndDate.before(d) == false) {
											ssch.setScheduleServiceDate(tempdate);
										} else {
											ssch.setScheduleServiceDate(productEndDate);
										}
									}
									
									if (form.customerbranchlist.size() == qty) {
										String branchName = form.customerbranchlist.get(k).getBusinessUnitName();
										ssch.setScheduleProBranch(branchName);
			
										/**
										 * rohan added this code for automatic servicing
										 * branch setting for NBHC
										 */
			
										if (form.customerbranchlist.get(k).getBranch() != null) {
											ssch.setServicingBranch(form.customerbranchlist.get(k).getBranch());
										} else {
											ssch.setServicingBranch(form.olbbBranch.getValue());
										}
										/**
										 * ends here
										 */
			
									} else if (form.customerbranchlist.size() == 0) {
										ssch.setScheduleProBranch("Service Address");
			
										/**
										 * rohan added this code for automatic servicing
										 * branch setting for NBHC
										 */
										ssch.setServicingBranch(form.olbbBranch.getValue());
										/**
										 * ends here
										 */
			
									} else {
										ssch.setScheduleProBranch(null);
									}
			
									ssch.setScheduleServiceDay(ContractForm.serviceDay(ssch.getScheduleServiceDate()));
									ssch.setServiceRemark(formLis.get(i).getRemark());
									/**
									 * Date 01-04-2017 added by vijay for week number
									 */
									int weeknumber = AppUtility.getweeknumber(ssch.getScheduleServiceDate());
									ssch.setWeekNo(weeknumber);
									
									/**
									 * ends here
									 */
									 /***
									  * nidhi *:*:*
									  * 18-09-2018
									  * for map bill product
									  * if any update please update in else conditions also
									  */
									 if(LoginPresenter.billofMaterialActive){
										 
										 BillOfMaterial billPrDt = AppUtility.verifyBillofMaterilProd(formLis.get(i).getPrduct());
										 if(billPrDt!=null){
											 
											 
											 UnitConversionUtility unitConver = new UnitConversionUtility();

											 
											 if(branchSchedulingList.get(k).getUnitOfMeasurement() != null && unitConver.varifyUnitConversion(billPrDt, branchSchedulingList.get(k).getUnitOfMeasurement())){
												 List<ServiceSchedule> seList = new ArrayList<ServiceSchedule>();
												 seList.add(ssch);
												 seList = unitConver.getServiceitemProList(seList,formLis.get(i).getPrduct(),formLis.get(i),billPrDt);
												 if(seList!=null && seList.size()>0)
													 ssch = seList.get(0);
												 
											 }else{
												 unitFlag = true;
//												 form.showDialogMessage("Unit of service area and product group area is differeant and conversion of unit is not avaliable.");
											 }
											 
											 
											 
											 
										 }
									 }
									 /**
									  * end
									  */
									newlist.add(ssch);
								}
							}
						}
					}
					if(unitFlag){
						form.showDialogMessage("Unit of service are and product group detail area not mapped at branch level. Or conversion not available");
					}
				}

				form.serviceScheduleTable.getDataprovider().setList(newlist);
			}
		 
		 
		 private boolean checkservicedateisbeforeTodayDate(Date todayDate, Date scheduleddate) {
				boolean servicesFlag =false;
				System.out.println("In the method ===============");
				System.out.println("Service Today Date ====="+todayDate);
				System.out.println("Schedule date ==="+scheduleddate);
				
				if(scheduleddate.before(todayDate)){
					System.out.println("000000000000000000000");
					servicesFlag=true;
				}
				return servicesFlag;
				
			}
			
			protected boolean findProductId(int prodId)
			{
				List<SalesLineItem> formLis=form.getSaleslineitemtable().getDataprovider().getList();
				boolean flagVal=false;
				for(int i=0;i<formLis.size();i++)
				{
					if(formLis.get(i).getPrduct().getCount()==prodId){
						flagVal=true;
					}
				}
				return flagVal;
			}
			
				
				private void getTaxesDetails() {
					final GenricServiceAsync service = GWT.create(GenricService.class);
					MyQuerry query = new MyQuerry();
					Company c=new Company();
					System.out.println("Company Id :: "+c.getCompanyId());
					
					Vector<Filter> filtervec=new Vector<Filter>();
					Filter filter = null;
					
					filter = new Filter();
					filter.setQuerryString("companyId");
					filter.setLongValue(c.getCompanyId());
					filtervec.add(filter);
					
					query.setFilters(filtervec);
					query.setQuerryObject(new TaxDetails());
					form.showWaitSymbol();
					service.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
						@Override
						public void onFailure(Throwable caught) {
						}

					
						public void onSuccess(ArrayList<SuperModel> result) {
							
							System.out.println("result size of tax "+result);
							servicetaxlist=new ArrayList<TaxDetails>();
							vattaxlist = new ArrayList<TaxDetails>();
							List<TaxDetails> backlist=new ArrayList<TaxDetails>();
							
							for (SuperModel model : result) {
								TaxDetails entity = (TaxDetails) model;
								backlist.add(entity);
								if(entity.getServiceTax().equals(true)){
									servicetaxlist.add(entity);
								}
								if(entity.getVatTax().equals(true)){
									vattaxlist.add(entity);
								}
							}
							System.out.println();
							System.out.println();
							for(int i=0;i<servicetaxlist.size();i++){
								System.out.println(servicetaxlist.get(i).getTaxChargeName()+" "+servicetaxlist.get(i).getTaxChargePercent());
							}
							System.out.println();
							System.out.println();
							for(int i=0;i<vattaxlist.size();i++){
								System.out.println(vattaxlist.get(i).getTaxChargeName()+" "+vattaxlist.get(i).getTaxChargePercent());
							}
							System.out.println();
							System.out.println();
							
							updateTaxes();
							
						}
					});
				}

				private void updateTaxes() {
					Vector<Filter> filtervec=new Vector<Filter>();
					
					MyQuerry querry = new MyQuerry();
					Filter filter = null;
					
					filter = new Filter();
					filter.setQuerryString("companyId");
					filter.setLongValue(UserConfiguration.getCompanyId());
					filtervec.add(filter);
					
					filter = new Filter();
					filter.setQuerryString("startDate >=");
					filter.setDateValue(frmAndToPop.fromDate.getValue());
					filtervec.add(filter);
					
					filter = new Filter();
					filter.setQuerryString("startDate <=");
					filter.setDateValue(frmAndToPop.toDate.getValue());
					filtervec.add(filter);
					
//					filter = new Filter();
//					filter.setQuerryString("status");
//					filter.setStringValue(BillingDocument.CREATED);
//					filtervec.add(filter);
					
					querry.setFilters(filtervec);
					querry.setQuerryObject(new Contract());
//					form.showWaitSymbol();
					genasync.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							if (result.size() != 0) {
								for(SuperModel model:result){
									Contract con=(Contract) model;
									
									for(int i=0;i<con.getItems().size();i++){
										
										if(con.getItems().get(i).getServiceTax()!=null){
											for(int j=0;j<servicetaxlist.size();j++){
												if(con.getItems().get(i).getServiceTax().getPercentage()==servicetaxlist.get(j).getTaxChargePercent()){
													con.getItems().get(i).setServiceTaxEdit(servicetaxlist.get(j).getTaxChargeName());
												}
											}
										}
										
										if(con.getItems().get(i).getVatTax()!=null){
											for(int j=0;j<vattaxlist.size();j++){
												if(con.getItems().get(i).getVatTax().getPercentage()==vattaxlist.get(j).getTaxChargePercent()){
													con.getItems().get(i).setVatTaxEdit(vattaxlist.get(j).getTaxChargeName());
												}
											}
										}
									}
									
									genasync.save(con,new AsyncCallback<ReturnFromServer>() {
										@Override
										public void onFailure(Throwable caught) {
											form.hideWaitSymbol();
										}
										@Override
										public void onSuccess(ReturnFromServer result) {
											form.hideWaitSymbol();
											form.showDialogMessage("Entity updated successfully!");
										}
									});
								}
							}
						}

						@Override
						public void onFailure(Throwable caught) {
							form.hideWaitSymbol();
							form.showDialogMessage("Unexpected Error!");
						}
					});
				}
				
				protected boolean findProductId(int prodId, int srNo) {
					List<SalesLineItem> formLis = form.getSaleslineitemtable()
							.getDataprovider().getList();
					boolean flagVal = false;
					for (int i = 0; i < formLis.size(); i++) {
						if (formLis.get(i).getPrduct().getCount() == prodId
								&& formLis.get(i).getProductSrNo() == srNo) {
							flagVal = true;
						}
					}
					return flagVal;
				}
				public static void setScheduleServiceOnSchedulingTable(ArrayList<ServiceSchedule> list){
					System.out.println("SCHEDULE LIST : "+list.size());
//					serviceSchedulePopUp.getPopScheduleTable().connectToLocal();
//					serviceSchedulePopUp.getPopScheduleTable().getDataprovider().setList(list);
					form.getServiceScheduleTable().connectToLocal();
					form.getServiceScheduleTable().getDataprovider().setList(list);
//					System.out.println("SCHEDULE TBL : "+serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList().size());
				}
				
				

				private void setEmailPopUpData() {
					form.showWaitSymbol();
					String customerEmail = "";

					String branchName = form.olbbBranch.getValue();
					String fromEmailId = AppUtility.getFromEmailAddress(branchName,model.getEmployee());
					customerEmail = AppUtility.getCustomerEmailid(model.getCinfo().getCount());
					
					String customerName = model.getCinfo().getFullName();
					if(!customerName.equals("") && !customerEmail.equals("")){
						label : AppUtility.loadContactPersonlist(emailpopup.olbContactlistEmailId,"Customer",model.getCinfo().getCount(),customerName,customerEmail,null);
						emailpopup.taToEmailId.setValue(customerEmail);
					}
					else{
				
						MyQuerry querry = AppUtility.getcustomerQuerry(model.getCinfo().getCount());
						genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
							
							@Override
							public void onSuccess(ArrayList<SuperModel> result) {
								// TODO Auto-generated method stub
								for(SuperModel smodel : result){
									Customer custEntity  = (Customer) smodel;
									System.out.println("customer Name = =="+custEntity.getFullname());
									System.out.println("Customer Email = == "+custEntity.getEmail());
									
									if(custEntity.getEmail()!=null){
										label : AppUtility.loadContactPersonlist(emailpopup.olbContactlistEmailId,"Customer",model.getCinfo().getCount(),custEntity.getFullname(),custEntity.getEmail(),null);
										emailpopup.taToEmailId.setValue(custEntity.getEmail());
									}
									else{
										label : AppUtility.loadContactPersonlist(emailpopup.olbContactlistEmailId,"Customer",model.getCinfo().getCount(),custEntity.getFullname(),custEntity.getEmail(),null);
									}
									break;
								}
							}
							
							@Override
							public void onFailure(Throwable caught) {
								// TODO Auto-generated method stub
							}
						});
					}
					
					Screen screenName=(Screen) AppMemory.getAppMemory().currentScreen;
					Console.log("screenName "+screenName);
					AppUtility.makeLiveSmsTemplateConfig(emailpopup.oblTemplateName,LoginPresenter.currentModule.trim(),screenName);
					emailpopup.taFromEmailId.setValue(fromEmailId);
					emailpopup.smodel = model;
					form.hideWaitSymbol();
					
				}

				public void runRegularDownloadProgram() {

					ArrayList<Contract> custarray=new ArrayList<Contract>();
					List<Contract> list=(List<Contract>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
					
					custarray.addAll(list);
					
					csvservice.setcontractlist(custarray, new AsyncCallback<Void>() {

						@Override
						public void onFailure(Throwable caught) {
							System.out.println("RPC call Failed"+caught);
							
						}

						@Override
						public void onSuccess(Void result) {
							if(form.pedioContractFlag){
								String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
								final String url=gwt + "csvservlet"+"?type="+183;
								Window.open(url, "test", "enabled");
							}else{
								String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
								final String url=gwt + "csvservlet"+"?type="+6;
								Window.open(url, "test", "enabled");
							}
						}
					});

				
				}
}
