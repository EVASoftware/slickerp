package com.slicktechnologies.client.views.quickcontract;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.TextAlign;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.client.ui.ValueBoxBase.TextAlignment;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.AddressComposite;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.composite.UploadComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.EmailTextBox;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.simplesoftwares.client.library.mywidgets.PhoneNumberBox;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utility.UnitConversionUtility;
import com.slicktechnologies.client.views.complaindashboard.complainHomePresenter;
import com.slicktechnologies.client.views.contract.ContractForm;
import com.slicktechnologies.client.views.contract.PaymentTable;
import com.slicktechnologies.client.views.contract.PaymentTermsTable;
import com.slicktechnologies.client.views.contract.ProductChargesTable;
import com.slicktechnologies.client.views.contract.ProductTaxesTable;
import com.slicktechnologies.client.views.contract.SalesLineItemTable;
import com.slicktechnologies.client.views.popups.EmailIdPopup;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.client.views.scheduleservice.ServiceSchedulePopUp;
import com.slicktechnologies.client.views.scheduleservice.ServiceScheduleTable;
import com.slicktechnologies.shared.BranchWiseScheduling;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.ProductOtherCharges;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.contactidentification.ContactPersonIdentification;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.inventory.BillOfMaterial;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.servicerelated.ServiceSchedule;
import com.slicktechnologies.client.utils.Console;

public class QuickContractForm extends FormScreen<Contract> implements ClickHandler, ChangeHandler{

	ObjectListBox<Branch> olbbBranch;
	TextBox tbReferenceNumber;
	DateBox dbrefernceDate;
	ObjectListBox<Config> olbcPriority,olbcPaymentMethods;
	PaymentTable paymenttable;
	TextBox tbContractId;
	TextBox tbRefeRRedBy;
	TextBox tbStatus;
	TextArea taDescription;
	PersonInfoComposite personInfoComposite;
	ObjectListBox<Employee> olbeSalesPerson,olbApproverName;
	DateBox dbContractStartDate,dbContractEndDate;
	ObjectListBox<Config> olbContractGroup;
	ObjectListBox<Type> olbContractType;
	ObjectListBox<ConfigCategory> olbContractCategory;
	IntegerBox ibCreditPeriod;
	DoubleBox dototalamt,doamtincltax,donetpayamt;
	TextBox tbremark;
	TextBox tbLeadId,tbQuotatinId;
	IntegerBox ibdays;
	DoubleBox dopercent;
	TextBox tbcomment,tbdonotrenew;
	Button addPaymentTerms;
	PaymentTermsTable paymentTermsTable;
	SalesLineItemTable saleslineitemtable;
	ProductChargesTable chargesTable;
	Button addOtherCharges;
	ProductTaxesTable prodTaxTable;
	ProductInfoComposite prodInfoComposite;
	Button btnaddproducts;
	DateBox dbContractDate;
	CheckBox cbRenewFlag;
	TextBox ibOldContract;
	String f_serviceDay,f_serviceTime;
	Button f_btnservice;
	Date f_conStartDate;
	Date f_conEndDate;
	int f_duration;
	ServiceScheduleTable serviceScheduleTable;
	ServiceSchedule scheduleEntity;
	ServiceSchedulePopUp servicepop=new ServiceSchedulePopUp("Contract Id", "Contract Date");
	public ArrayList<CustomerBranchDetails> customerbranchlist=new ArrayList<CustomerBranchDetails>();
	public boolean servicesFlag=false;
	static DateTimeFormat format = DateTimeFormat.getFormat("c"); 
	final GenricServiceAsync genasync=GWT.create(GenricService.class);
	
	Contract contractObject;
	FormField fgroupingCustomerInformation;
	DoubleBox dbpaymentrecieved,dbbalancepayment;
	
//  rohan added this upload documnet for NBHC
	UploadComposite ucUploadTAndCs;
	
	/********************* changes for cash and chechque ********************/
	public ObjectListBox<Config> olbcNumberRange;
	
	int productSrNo=0;
	
	TextBox tbFullName, tbCompanyName;
	PhoneNumberBox pnbCellNo;
	EmailTextBox etbEmail;
	
	AddressComposite customerAddressComposite;
	
	NumberFormat numfmt = NumberFormat.getFormat("0.00");
	
	boolean customerEditFlag=false;
	
	// vijay  this date used for in quick contract which is invoice date
	DateBox dbinvoiceDate;
		
	TextBox tbRefNo2;
	
	/**
	 * rohan added customer branch for setting branch in customer so that customer will be search while 
	 * branch level restriction is on
	 * Date : 27/2/2017
	 */
	ObjectListBox<Branch> olbbCustomerBranch;
	
	
	/**
	 * rohan added this code for integrated pest control 
	 * Date:12-05-2017
	 * this is used is used to add technician in contract form 	
	 */
	
	ObjectListBox<Employee> olbTechnicainName;
	
	/**
	 * ends here 	
	 */
	
	/**
	 * Date 1-07-2017 added by vijay for Tax validation
	 */
	Date date = DateTimeFormat.getFormat("yyyy-MM-dd HH:mm:ss").parse("2017-07-01 00:00:00");
	final DateTimeFormat fmt = DateTimeFormat.getFormat("dd/MM/yyyy");

	/**
	 * ends here
	 */
	
//  rohan added this code for payment details in quick contract Date : 12-06-2017
		TextBox tbbankAccNo,tbbankName,tbbankBranch,tbchequeNo,tbreferenceno,tbChequeIssuedBy;
		DateBox dbChequeDate,dbtransferDate;
	
	// Date 02-09-2017 added by  vijay for new customer GST Number
	TextBox tbGSTNumber;	
	
	 /** Date 06/2/2018 added by komal for consolidate price checkbox **/
	 CheckBox cbConsolidatePrice;
			
	 /** Date 28-02-2018 By vijay for New customer ****/
	 Button btnNewCustomer;
	 
	/****************************************Constructor*************************************************/
	 /**
		 *  nidhi
		 *  29-01-2018
		 *  add for multiple contact list on customer
		 */
		ObjectListBox<String> objContactList;
		
		
	/**
	 * @author Anil
	 * @since 11-06-2020
	 * This flag is used to minimize the no. of contract fields for user
	 */
	boolean pedioContractFlag=false;
	public List<ServiceSchedule> globalScheduleServiceList=new ArrayList<ServiceSchedule>();
	EmailIdPopup mailpopup=new EmailIdPopup();
		public String customerEmail="";
		
	Customer customerEntity;	
		
	public  QuickContractForm() {
		super();
		createGui();
		this.tbContractId.setEnabled(false);
		this.tbQuotatinId.setEnabled(false);
		this.tbStatus.setEnabled(false);
		this.tbStatus.setText(Contract.CREATED);
		saleslineitemtable.connectToLocal();
		chargesTable.connectToLocal();
		prodTaxTable.connectToLocal();
			mailpopup.getLblCancel().addClickHandler(this);
		mailpopup.getLblOk().addClickHandler(this);
		pedioContractFlag=AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "PedioContract");
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("NumberRange", "MakeNumberRangeReadOnly"))
  		{	olbcNumberRange.setEnabled(false);
  		}
	}
	
	public QuickContractForm  (String[] processlevel, FormField[][] fields,
			FormStyle formstyle) {
		super(processlevel, fields, formstyle);
		createGui();
	}
	
	//***********************************Variable Initialization********************************************//

		/**
		 * Method template to initialize the declared variables.
		 */
		@SuppressWarnings("unused")
		private void initalizeWidget(){
			
			tbReferenceNumber=new TextBox();
			
			MyQuerry custquerry1 = new MyQuerry();
//			Filter custfilteer1 = new Filter();
//			custfilteer1.setQuerryString("status");
//			custfilteer1.setBooleanvalue(true);
			custquerry1.setQuerryObject(new Customer());
			personInfoComposite = new PersonInfoComposite(custquerry1);
			
			
			
			olbbBranch=new ObjectListBox<Branch>();
			AppUtility.makeBranchListBoxLive(olbbBranch);
			olbbBranch.addChangeHandler(this);
	
			olbcPaymentMethods=new ObjectListBox<Config>();
			AppUtility.MakeLiveConfig(olbcPaymentMethods,Screen.PAYMENTMETHODS);
			olbcPaymentMethods.addChangeHandler(this);
			
//			paymenttable=new PaymentTable();
//			paymenttable.connectToLocal();
			dbContractDate=new DateBoxWithYearSelector();
			tbContractId=new TextBox();
			tbQuotatinId = new TextBox();
			tbQuotatinId.setEnabled(false);
			tbStatus=new TextBox();
			tbStatus.setEnabled(false);
			if(pedioContractFlag){
				saleslineitemtable=new SalesLineItemTable(pedioContractFlag);
			}else{
				saleslineitemtable=new SalesLineItemTable();
			}
			/**
			 * @author Anil,Date : 04-02-2019
			 * Setting form variable to null
			 */
			saleslineitemtable.form = null;
			
			/**
			 * End
			 */
			saleslineitemtable.documentType="QuickContract";
//			personInfoComposite=AppUtility.customerInfoComposite(new Customer());
			olbeSalesPerson=new ObjectListBox<Employee>();
//			AppUtility.makeSalesPersonListBoxLive(olbeSalesPerson);
			olbeSalesPerson.makeEmployeeLive(AppConstants.SERVICEMODULE, AppConstants.QUICKCONTRACT, "Sales Person");
			
			olbContractGroup=new ObjectListBox<Config>();
			AppUtility.MakeLiveConfig(olbContractGroup, Screen.CONTRACTGROUP);
			olbContractCategory=new ObjectListBox<ConfigCategory>();
			AppUtility.MakeLiveCategoryConfig(olbContractCategory, Screen.CONTRACTCATEGORY);
			olbContractCategory.addChangeHandler(this);
			olbContractType=new ObjectListBox<Type>();
			AppUtility.makeTypeListBoxLive(olbContractType,Screen.CONTRACTTYPE);
			
			olbApproverName=new ObjectListBox<Employee>();
			AppUtility.makeApproverListBoxLive(olbApproverName,"Contract");
			
			this.tbContractId.setEnabled(false);
			
			dbContractStartDate=new DateBoxWithYearSelector();
			dbContractEndDate=new DateBoxWithYearSelector();
			dbContractStartDate.setEnabled(false);
			dbContractEndDate.setEnabled(false);
			
			personInfoComposite.getCustomerId().getHeaderLabel().setText("* Customer ID");
			personInfoComposite.getCustomerName().getHeaderLabel().setText("* Customer Name");
			personInfoComposite.getCustomerCell().getHeaderLabel().setText("* Customer Cell");
			
			prodInfoComposite=AppUtility.initiateServiceProductComposite(new SuperProduct());
			ibCreditPeriod=new IntegerBox();
			chargesTable=new ProductChargesTable();
			addOtherCharges=new Button("+");
			addOtherCharges.addClickHandler(this);
			prodTaxTable=new ProductTaxesTable();
			dototalamt=new DoubleBox();
			dototalamt.setEnabled(false);
			doamtincltax=new DoubleBox();
			doamtincltax.setEnabled(false);
			
			donetpayamt=new DoubleBox();
			donetpayamt.setEnabled(false);
			
			btnaddproducts=new Button("ADD");
			btnaddproducts.addClickHandler(this);
			this.changeWidgets();
			cbRenewFlag=new CheckBox();
			cbRenewFlag.setValue(false);
			
			f_btnservice = new Button("Schedule");
			this.f_btnservice.setEnabled(true);
			serviceScheduleTable = new ServiceScheduleTable();
			scheduleEntity= new ServiceSchedule();
			
			olbcNumberRange = new ObjectListBox<Config>();
			AppUtility.MakeLiveConfig(olbcNumberRange, Screen.NUMBERRANGE);
			olbcNumberRange.addChangeHandler(this);
		
			dbpaymentrecieved= new DoubleBox();
			dbpaymentrecieved.setWidth("200px");
			dbpaymentrecieved.setAlignment(TextAlignment.CENTER);
			
			dbbalancepayment = new DoubleBox();
			dbbalancepayment.setEnabled(false);
			dbbalancepayment.setWidth("200px");
			dbbalancepayment.setAlignment(TextAlignment.CENTER);
			
			tbCompanyName =new TextBox();
			tbFullName = new TextBox();
			pnbCellNo =  new PhoneNumberBox();
			etbEmail=new EmailTextBox();
			
			dbinvoiceDate = new DateBoxWithYearSelector();
			
			customerAddressComposite = new AddressComposite(true);
			
			//  rohan added this upload documnet for NBHC
			ucUploadTAndCs=new UploadComposite();
			
			taDescription = new TextArea();
			tbRefNo2 = new TextBox();
			
			//  rohan added this for loading customer branch
			olbbCustomerBranch = new ObjectListBox<Branch>();
			AppUtility.makeCustomerBranchListBoxLive(olbbCustomerBranch);
			
			olbTechnicainName= new ObjectListBox<Employee>();
			olbTechnicainName.makeEmployeeLive(AppConstants.SERVICEMODULE, AppConstants.CONTRACT, "Sales Person");
		
		
			tbbankAccNo=new TextBox();
			tbbankAccNo.setEnabled(false);
			
			tbbankName=new TextBox();
			tbbankName.setEnabled(false);
			
			tbbankBranch=new TextBox();
			tbbankBranch.setEnabled(false);
			
			tbchequeNo=new TextBox(); 
			tbchequeNo.setEnabled(false);
			
			dbChequeDate=new DateBoxWithYearSelector();
			dbChequeDate.setEnabled(false);
			
			tbreferenceno=new TextBox();
			tbreferenceno.setEnabled(false);
			
			dbtransferDate=new DateBoxWithYearSelector();
			dbtransferDate.setEnabled(false);
			
			tbChequeIssuedBy=new TextBox();
			// Date 02-03-2018 By vijay
			tbChequeIssuedBy.setEnabled(false);
			
			/*
			 *  nidhi
			 *  19-07-2017
			 *  this field use for set old contract Id 
			 */
			ibOldContract=new TextBox();
			ibOldContract.setEnabled(false);
			/*
			 *  end
			 */
		
			tbGSTNumber = new TextBox();
			 /** Date 06/2/2018 added by komal for consolidate price checkbox **/
			 cbConsolidatePrice = new CheckBox();
			 if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","ConsolidatePrice")){
				 cbConsolidatePrice.setValue(true);
			 }else{
				 cbConsolidatePrice.setValue(false);
			 }
			 /**
			  * end komal
			  */
			 
			 /** Date 05-03-2018 By vijay for lead id **/ 
			tbLeadId=new TextBox();
			this.tbLeadId.setEnabled(false);
			
			/** Date 28-02-2018 By vijay for New customer ****/
			 btnNewCustomer = new Button("New Customer");
			 
			 /**
				 *  nidhi
				 *  29-01-2018
				 */
				objContactList = new ObjectListBox<String>();
				objContactList.removeAllItems();
				objContactList.addItem("--Select--");
				/**
				 * end
				 */
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("NumberRange", "MakeNumberRangeReadOnly"))
		  		{	olbcNumberRange.setEnabled(false);
		  		}
		}
	
	

	/**
	 * method template to create screen formfields
	 */
	@Override
	public void createScreen() {
		// TODO Auto-generated method stub
		
		pedioContractFlag=AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "PedioContract");
		initalizeWidget();
		
		int customerColspan=3;
		String orderIdLabel="Order ID";
		if(pedioContractFlag){
			customerColspan=4;
			orderIdLabel="Contract ID";
			
			personInfoComposite.setWidth("102%");
			btnaddproducts.getElement().getStyle().setMarginTop(11, Unit.PX);
			this.processlevelBarNames=new String[]{AppConstants.NEW,"Submit"};
			
		}else{
		
			/**
			 * Date : 12-10-2017 BY ANIL
			 * Removed view bill button 
			 */
			this.processlevelBarNames=new String[]{AppConstants.NEW,"Submit",AppConstants.CONTRACTRENEWAL,AppConstants.VIEWINVOICEPAYMENT,AppConstants.CANCLECONTRACT,AppConstants.CANCELLATIONSUMMARY};
			
		}
		////////////////////////////Form Field Initialization////////////////////////////////////////////////////
		//Token to initialize formfield
		String mainScreenLabel="Contract Details";
		if(contractObject!=null&&contractObject.getCount()!=0){
			mainScreenLabel=contractObject.getCount()+" "+"/"+" "+contractObject.getStatus()+" "+"/"+" "+AppUtility.parseDate(contractObject.getCreationDate());
		}
		
		//fgroupingCustomerInformation.setLabel(mainScreenLabel);
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		fgroupingCustomerInformation=fbuilder.setlabel(mainScreenLabel).widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).setKeyField(true).build();
		//FormField fgroupingCustomerInformation=fbuilder.setlabel("Select Existing Customer ").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("",personInfoComposite);
		FormField fpersonInfoComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(customerColspan).build();
		
		fbuilder = new FormFieldBuilder(orderIdLabel,tbContractId);
		FormField ftbContractId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Quotation ID",tbQuotatinId);
		FormField ftbQuotationId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Reference Number 1",tbReferenceNumber);
		FormField frefNo= fbuilder.setMandatory(false).setMandatoryMsg("Ref Number is mandatory").setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingSalesInformation=fbuilder.setlabel("Sales Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingSalesInformation1=fbuilder.setlabel("Classification").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Status",tbStatus);
		FormField ftbStatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Branch",olbbBranch);
		FormField folbbBranch= fbuilder.setMandatory(true).setMandatoryMsg("Branch is Mandatory").setRowSpan(0).setColSpan(0).build();
		/**@Sheetal:01-02-2022
        Des : Making Sales person non mandatory,requirement by UDS Water**/
		fbuilder = new FormFieldBuilder("Sales Person",olbeSalesPerson);
		FormField folbeSalesPerson= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Payment Methods",olbcPaymentMethods);
		FormField folbcPaymentMethods= fbuilder.setMandatory(false).setMandatoryMsg("Payment Methods is Mandatory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Approver Name",olbApproverName);
		FormField folbApproverName= fbuilder.setMandatory(true).setMandatoryMsg("Approver Name is Mandatory").setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("Contract Group",olbContractGroup);
		FormField folbContractGroup= fbuilder.setMandatory(false).setMandatoryMsg("Contract Group is Mandatory").setRowSpan(0).setColSpan(0).build();
		
		FormField folbContractCategory ;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "OnlyForNBHC")){
		fbuilder = new FormFieldBuilder("* Contract Category",olbContractCategory);
		 folbContractCategory= fbuilder.setMandatory(true).setMandatoryMsg("Contract Category is Mandatory").setRowSpan(0).setColSpan(0).build();
		}else{
		fbuilder = new FormFieldBuilder("Contract Category",olbContractCategory);
		 folbContractCategory= fbuilder.setMandatory(false).setMandatoryMsg("Contract Category is Mandatory").setRowSpan(0).setColSpan(0).build();
		}
		
		fbuilder = new FormFieldBuilder("Contract Type",olbContractType);
		FormField folbContractType= fbuilder.setMandatory(false).setMandatoryMsg("Contract Type is Mandatory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Contract Date",dbContractDate);
		FormField fdbContractDate= fbuilder.setMandatory(true).setMandatoryMsg("Contract Date is Mandatory!").setRowSpan(0).setColSpan(0).build();
	
		fbuilder = new FormFieldBuilder("Credit Period (Days)",ibCreditPeriod);
		FormField fibCreditPeriod= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
//		fbuilder = new FormFieldBuilder("Number Range",olbcNumberRange);
//		FormField folbcNumberRange= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/** Date 02 Oct 2017  Number range mandatory with configuration added by vijay *********/
		
		FormField folbcNumberRange;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("NumberRange", "MakeNumberRangeMandatory")){
			fbuilder = new FormFieldBuilder("*Number Range",olbcNumberRange);
			folbcNumberRange= fbuilder.setMandatory(true).setMandatoryMsg("Number Range is Mandatory!").setRowSpan(0).setColSpan(0).build();
		}else{
			fbuilder = new FormFieldBuilder("Number Range",olbcNumberRange);
			folbcNumberRange= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingProducts=fbuilder.setlabel("Product").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fblankgroup=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("",prodInfoComposite);
		FormField fprodInfoComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder("",btnaddproducts);
		FormField fbaddproducts= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",f_btnservice);
		FormField fbtnservice= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fblankgroupthree=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder("",saleslineitemtable.getTable());
		FormField fsaleslineitemtable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Total Amount",dototalamt);
		FormField fpayCompTotalAmt= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",prodTaxTable.getTable());
		FormField fprodTaxTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		
		fbuilder = new FormFieldBuilder("",addOtherCharges);
		FormField faddOtherCharges= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Total Amount",doamtincltax);
		FormField fdoAmtIncludingTax= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",chargesTable.getTable());
		FormField fchargesTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fblankgroupone=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Net Receivable",donetpayamt);
		FormField fpayCompNetPay= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Payment Recieved",dbpaymentrecieved);
		FormField fpaymentrecieved= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Balance Payment",dbbalancepayment);
		FormField fbalancepayment= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		 /** Date 28-02-2018 By vijay for New customer we added button so these below fields no needs so commented ****/

//		fbuilder = new FormFieldBuilder("Company Name",tbCompanyName);
//		FormField ftbCompanyName= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
//		
//		fbuilder = new FormFieldBuilder("Customer Name",tbFullName);
//		FormField ftbFullName= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
//		
//		fbuilder = new FormFieldBuilder("Cell No",pnbCellNo);
//		FormField fpnbCellNo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
//		
//		fbuilder = new FormFieldBuilder("Email",etbEmail);
//		FormField fetbEmail= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
//		
//		
//		fbuilder = new FormFieldBuilder("",customerAddressComposite);
//		FormField fcustomerAddressComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
//		
//		fbuilder = new FormFieldBuilder();
//		FormField fgroupingNewCustomerInformation=fbuilder.setlabel(" New Customer ").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("* Invoice Date",dbinvoiceDate);
		FormField fdbinvoiceDate= fbuilder.setMandatory(true).setMandatoryMsg("Invoice Date is Mandetory").setRowSpan(0).setColSpan(0).build();
		
		//  rohan added this upload documnet for NBHC
		fbuilder = new FormFieldBuilder();
		FormField fgroupingDocuments=fbuilder.setlabel("Attachment").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Upload T&Cs",ucUploadTAndCs);
		FormField fucUploadTAndCs= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		
		// Date 07 Feb 2017 added by vijay for PCACAMB
		fbuilder = new FormFieldBuilder();
		
		fbuilder = new FormFieldBuilder("Description (Max 500 characters)",taDescription);
		FormField ftaDescription= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Reference Number 2",tbRefNo2);
		FormField ftbRefNo2= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		// rohan added this code for adding customer branch
		fbuilder = new FormFieldBuilder("Customer Branch",olbbCustomerBranch);
		FormField folbbCustomerBranch= fbuilder.setMandatory(false).setMandatoryMsg("Customer Branch is Mandatory").setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("Technician Name",olbTechnicainName);
		FormField folbTechnicainName= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
	//   rohan added this code for payment details in quick contract 
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingDescription=fbuilder.setlabel("Reference/General").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		FormField fPaymentInformation=fbuilder.setlabel("Payment Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Cheque-Bank Account No.",tbbankAccNo);
		FormField ftbbankAccNo=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Cheque-Bank Name",tbbankName);
		FormField ftbbankName=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Cheque-Bank Branch",tbbankBranch);
		FormField ftbbankBranch=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Cheque No",tbchequeNo);
		FormField fibchequeNo=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Cheque Date",dbChequeDate);
		FormField fdbChequeDate=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Reference No.",tbreferenceno);
		FormField ftbreferenceno=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Digital-Transfer Date",dbtransferDate);
		FormField fdbtransferDate=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Cheque Issued By",tbChequeIssuedBy);
		FormField ftbChequeIssuedBy=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		
		/*
		 *  nidhi
		 *  19-07-2017
		 *  for ibOldContract text box display perpose
		 */
		fbuilder = new FormFieldBuilder("Previous Order ID",ibOldContract);
		FormField fibOldContract= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		/*
		 * end
		 */
		
		
		fbuilder = new FormFieldBuilder("GST Number",tbGSTNumber);
		FormField ftbGSTNumber=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/** date 06-02-2018 added by komal for consolidate price checkbox **/
		fbuilder = new FormFieldBuilder("Consolidate Price",cbConsolidatePrice);
		FormField fcbConsolidatePrice= fbuilder.setMandatory(true).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Lead ID",tbLeadId);
		FormField ftbLeadId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		 /** Date 28-02-2018 By vijay for New customer ****/
		fbuilder = new FormFieldBuilder("",btnNewCustomer);
		FormField fbtnNewCustomer = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////


		/**
		 * nidhi
		 * 29-01-2018
		 * get multiple list of contact details of customer
		 */
		fbuilder = new FormFieldBuilder("POC Name",objContactList);
		FormField fobjContactList= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		if(pedioContractFlag){
			FormField[][] formfield = {   
				{fgroupingCustomerInformation},
				{fbtnNewCustomer},
				{fpersonInfoComposite},
				
				{fgroupingSalesInformation},
				{ftbContractId,fdbContractDate,folbbBranch,ftbStatus},
				{folbTechnicainName},
				
				{fgroupingProducts},
				{fprodInfoComposite,fbaddproducts},{fbtnservice},
				{fsaleslineitemtable},
				
				{fgroupingDescription},
				{ftaDescription},
			};
			this.fields=formfield;
			return;
		}
		
		FormField[][] formfield = {   
//				{fgroupingCustomerInformation},
//				{fbtnNewCustomer},
//				{fpersonInfoComposite,fobjContactList},
////				{fgroupingNewCustomerInformation},
////				{ftbCompanyName,ftbFullName,fpnbCellNo,fetbEmail},
////				{folbbCustomerBranch,ftbGSTNumber},
////				{fcustomerAddressComposite},
//				{fgroupingSalesInformation},
//				{ftbContractId,ftbStatus,folbbBranch,folbeSalesPerson},
//				{folbApproverName,folbContractGroup,folbContractCategory,folbContractType},
//				{fdbContractDate,fibCreditPeriod,folbcNumberRange,folbcPaymentMethods},
//				{ftbLeadId,ftbQuotationId,frefNo,ftbRefNo2},
//				{folbTechnicainName,fibOldContract,fdbinvoiceDate},
//			//  rohan added this code for adding payment details in quick contract 
//				{fPaymentInformation},
//				{ftbbankAccNo,ftbbankName,ftbbankBranch,fibchequeNo},
//				{fdbChequeDate,fdbtransferDate,ftbreferenceno,ftbChequeIssuedBy},
//				{fcbConsolidatePrice},/** date 06-02-2018 added by komal **/
//				{fgroupingDescription},
//				{ftaDescription},
//				//  rohan added this upload documnet for NBHC
//				{fgroupingDocuments},
//				{fucUploadTAndCs},
//				//  ends here 
//				{fgroupingProducts},
//				{fprodInfoComposite,fbaddproducts},{fbtnservice},
//				{fsaleslineitemtable},
//				{fblankgroupthree,fpayCompTotalAmt},
//				{fblankgroupthree},
//				{fblankgroup,fprodTaxTable},
//				{fblankgroup,faddOtherCharges,fdoAmtIncludingTax},
//				{fblankgroup,fchargesTable},
//				{fblankgroup,fblankgroupone,fpayCompNetPay},
//				{fblankgroup,fblankgroupone,fpaymentrecieved},
//				{fblankgroup,fblankgroupone,fbalancepayment}
				
				/**MainScreen**/
				{fgroupingCustomerInformation},
				{fbtnNewCustomer,fpersonInfoComposite},
				{fobjContactList,folbbBranch,folbeSalesPerson,fdbContractDate},
				{fdbinvoiceDate,folbcNumberRange,folbApproverName},
				/**Product**/
				{fgroupingProducts},
				{fbtnservice,fcbConsolidatePrice,folbTechnicainName},
				{fprodInfoComposite,fbaddproducts},
				{fsaleslineitemtable},
				{fblankgroupthree,fpayCompTotalAmt},
				{fblankgroupthree},
				{fblankgroup,fprodTaxTable},
				{fblankgroup,faddOtherCharges,fdoAmtIncludingTax},
				{fblankgroup,fchargesTable},
				{fblankgroup,fblankgroupone,fpayCompNetPay},
				{fblankgroup,fblankgroupone,fpaymentrecieved},
				{fblankgroup,fblankgroupone,fbalancepayment},
				/**Payment Info**/
				{fPaymentInformation},
				{fibCreditPeriod,folbcPaymentMethods},
				{ftbbankAccNo,ftbbankName,ftbbankBranch,fibchequeNo},
				{fdbChequeDate,ftbChequeIssuedBy,fdbtransferDate,ftbreferenceno},
				/**Classification**/
				{fgroupingSalesInformation1},
				{folbContractGroup,folbContractCategory,folbContractType},
				/**Reference/Genral**/
				{fgroupingDescription},
				{ftbLeadId,ftbQuotationId,frefNo,ftbRefNo2},
				{fibOldContract},
				{ftaDescription},
				/**Attachment**/
				{fgroupingDocuments},
				{fucUploadTAndCs},
				
				
		};

		this.fields=formfield;
		
	}

	
	
	@Override
	public void updateModel(Contract model) {
	
		System.out.println("Hiiiiiiiiiiiiiiiiiiiiiiiiii" );
		System.out.println();
		if(personInfoComposite.getValue()!=null){
			model.setCinfo(personInfoComposite.getValue());
		}
		
//		if(tbFullName.getValue()!=null ||)
		
		if(tbStatus.getValue()!=null)
			model.setStatus(tbStatus.getValue());
		if(olbbBranch.getValue()!=null)
			model.setBranch(olbbBranch.getValue());
		if(olbeSalesPerson.getValue()!=null)
			model.setEmployee(olbeSalesPerson.getValue());
		if(olbcPaymentMethods.getValue()!=null)
			model.setPaymentMethod(olbcPaymentMethods.getValue());
		
		if(dbContractDate.getValue()!=null)
			model.setContractDate(dbContractDate.getValue());
		if(olbContractType.getValue()!=null)
			model.setType(olbContractType.getValue(olbContractType.getSelectedIndex()));
		if(olbContractGroup.getValue()!=null)
			model.setGroup(olbContractGroup.getValue());
		if(olbContractCategory.getValue()!=null)
			model.setCategory(olbContractCategory.getValue());
		if(olbApproverName.getValue()!=null)
			model.setApproverName(olbApproverName.getValue());
		if(dototalamt.getValue()!=null)
			model.setTotalAmount(dototalamt.getValue());
		if(donetpayamt.getValue()!=null)
			model.setNetpayable(donetpayamt.getValue());
		if(!tbContractId.getValue().equals(""))
			model.setContractCount(Integer.parseInt(tbContractId.getValue()));
		
		if(!tbQuotatinId.getValue().equals("")){
			model.setQuotationCount(Integer.parseInt(tbQuotatinId.getValue()));
		}
		else
		{
			model.setQuotationCount(0);
		}
		if(ibCreditPeriod.getValue()!=null){
			model.setCreditPeriod(ibCreditPeriod.getValue());
		}
		/*
		 *  nidhi
		 *  20-07-2017
		 *  for save oldcontract id for renewal
		 */
		
		if(!ibOldContract.getValue().equals("")){
			model.setRefContractCount(Integer.parseInt(ibOldContract.getValue()));
		}
		cbRenewFlag.setValue(false);
		/*
		 *  end
		 */
		
		List<SalesLineItem> saleslis=this.getSaleslineitemtable().getDataprovider().getList();
		if(saleslis.size()!=0){
			model.setStartDate(AppUtility.calculateQuotationStartDate(saleslis));
		}
		
		if(saleslis.size()!=0)
		{
			model.setEndDate(AppUtility.calculateQuotationEndDate(saleslis));
		}
		
		List<SalesLineItem> saleslist=this.getSaleslineitemtable().getDataprovider().getList();
		ArrayList<SalesLineItem> arrItems=new ArrayList<SalesLineItem>();
		arrItems.addAll(saleslist);
		model.setItems(arrItems);
		
//		List<PaymentTerms> payTermsLis=this.paymentTermsTable.getDataprovider().getList();
//		ArrayList<PaymentTerms> payTermsArr=new ArrayList<PaymentTerms>();
//		payTermsArr.addAll(payTermsLis);
//		model.setPaymentTermsList(payTermsArr);
		
		List<ProductOtherCharges> productTaxLis=this.prodTaxTable.getDataprovider().getList();
		ArrayList<ProductOtherCharges> productTaxArr=new ArrayList<ProductOtherCharges>();
		productTaxArr.addAll(productTaxLis);
		model.setProductTaxes(productTaxArr);
		
		List<ProductOtherCharges> productChargeLis=this.chargesTable.getDataprovider().getList();
		ArrayList<ProductOtherCharges> productChargeArr=new ArrayList<ProductOtherCharges>();
		productChargeArr.addAll(productChargeLis);
		model.setProductCharges(productChargeArr);
		
		List<ServiceSchedule> prodservicelist=this.serviceScheduleTable.getDataprovider().getList();
		ArrayList<ServiceSchedule> prodserviceArr=new ArrayList<ServiceSchedule>();
		prodserviceArr.addAll(prodservicelist);
		model.setServiceScheduleList(prodserviceArr);
		
		if(f_serviceDay!=null){
			model.setScheduleServiceDay(f_serviceDay);
		}
		
		
		if(olbcNumberRange.getValue()!=null){
			model.setNumberRange(olbcNumberRange.getValue(olbcNumberRange.getSelectedIndex()));
		}
		
		/*********************** vijay *********************/
		
		if(dbpaymentrecieved.getValue()!=null){
			model.setPaymentRecieved(dbpaymentrecieved.getValue());
		}
		if(dbbalancepayment.getValue()!=null){
			model.setBalancePayment(dbbalancepayment.getValue());
		}
		
		ArrayList<PaymentTerms> PayTermslist=new ArrayList<PaymentTerms>();
		

		PayTermslist = getpaymentterms(donetpayamt.getValue(),dbpaymentrecieved.getValue());
		
		model.setPaymentTermsList(PayTermslist);

		 /** Date 28-02-2018 By vijay for New customer we added in popup so these below fields no needs so commented ****/

//		if(tbCompanyName.getValue()!=null){
//			model.setNewcompanyname(tbCompanyName.getValue());
//		}
//		
//		if(tbFullName.getValue()!=null){
//			model.setNewcustomerfullName(tbFullName.getValue());
//		}
//		
//		if(pnbCellNo.getValue()!=null){
//			model.setNewcustomercellNumber(pnbCellNo.getValue());
//		}
//		
//		if(etbEmail.getValue()!=null){
//			model.setNewcustomerEmail(etbEmail.getValue());
//		}
//		
//		if(customerAddressComposite.getValue()!=null){
//			model.setNewcustomerAddress(customerAddressComposite.getValue());
//		}
//		
//	//  rohan added this code for setting customer branch 
//		if(olbbCustomerBranch.getSelectedIndex()!= 0)
//		{
//			model.setNewCustomerBranch(olbbCustomerBranch.getValue(olbbCustomerBranch.getSelectedIndex()).trim());
//		}
		
		/**********************************************/
		if(model.getNewcustomerfullName()!=null && !model.getNewcustomerfullName().equals("") && model.getCinfo().getCount()==0){
			customerEditFlag = true;
		}
		
		if(model.getNewcompanyname()!=null && !model.getNewcompanyname().equals("") && model.getCinfo().getCount()==0){
			customerEditFlag = true;
		}
		
		if(tbReferenceNumber.getValue()!=null)
			model.setRefNo(tbReferenceNumber.getValue());
	
		
		if(dbinvoiceDate.getValue()!=null){
			model.setQuickContractInvoiceDate(dbinvoiceDate.getValue());
		}
		
		//  rohan added this for NBHC
		model.setDocument(ucUploadTAndCs.getValue());
		
		// vijay added this for PCACAMB
		if(taDescription.getValue()!=null)
			model.setDescription(taDescription.getValue());
		if(tbRefNo2.getValue()!=null)
			model.setRefNo2(tbRefNo2.getValue());
		
		if(olbTechnicainName.getSelectedIndex()!=0){
			model.setTechnicianName(olbTechnicainName.getValue(olbTechnicainName.getSelectedIndex()));
		}
		
	//  rohan added this for adding payment info in the contract 
		
		if(tbbankAccNo.getValue()!=null)
			model.setBankAccNo(tbbankAccNo.getValue());
		if(tbbankName.getValue()!=null)
			model.setBankName(tbbankName.getValue());
		if(tbbankBranch.getValue()!=null)
			model.setBankBranch(tbbankBranch.getValue());
		if(tbchequeNo.getValue()!=null)
			model.setChequeNo(tbchequeNo.getValue());
		if(dbChequeDate.getValue()!=null)
			model.setChequeDate(dbChequeDate.getValue());
		if(tbreferenceno.getValue()!=null)
			model.setReferenceNo(tbreferenceno.getValue());
		if(dbtransferDate.getValue()!=null)
			model.setAmountTransferDate(dbtransferDate.getValue());
		if(!tbChequeIssuedBy.getValue().equals("")){
			model.setChequeIssuedBy(tbChequeIssuedBy.getValue());
		}
		
		if(tbGSTNumber.getValue()!=null){
			model.setCustomerGSTNumber(tbGSTNumber.getValue());
		}
		/** Date 06/2/2018 added by komal for consolidate price checkbox **/
		if(cbConsolidatePrice.getValue() !=null){
			model.setConsolidatePrice(cbConsolidatePrice.getValue());
		}
		
		if(!tbLeadId.getValue().equals(""))
			model.setLeadCount(Integer.parseInt(tbLeadId.getValue()));
		/**
		 *  nidhi
		 *  29-01-2018
		 *  contact person
		 */
//		objContactList.setValue(view.getPocName());
//		showDialogMessage("Poc name -- " + objContactList.getValue(objContactList.getSelectedIndex()));
		if(objContactList.getSelectedIndex()!=0){
			model.setPocName(objContactList.getValue(objContactList.getSelectedIndex()));
		}
		/**
		 * end
		 */
		
		model.setQuickContract(true);
		
		contractObject=model;
		
		presenter.setModel(model);
		
		
	}

	private ArrayList<PaymentTerms> getpaymentterms(Double netpayble, Double paymentrecieved) {

		double percentage = 0 ;

		System.out.println(" net payable =="+netpayble);
		System.out.println(" payment recived === "+paymentrecieved);
		if(paymentrecieved==null || paymentrecieved==0){
			percentage = 100;
		}else{
			percentage = getpercentage(netpayble,paymentrecieved);
		}
			
		System.out.println(" percentage  == "+percentage);
		ArrayList<PaymentTerms> billingPayTerms=new ArrayList<PaymentTerms>();
		
		if(100-percentage==0 || 100-percentage==100.0){
			
			if(paymentrecieved == null || paymentrecieved==0){
				
				PaymentTerms paymentTerms=new PaymentTerms();

				System.out.println(" for 100 % balance payment ");
				paymentTerms.setPayTermDays(0);
//				double paypercent= Math.round(percentage);
				
				String pay = numfmt.format(percentage);
				double paypercent= Double.parseDouble(pay);
				
				paymentTerms.setPayTermPercent(paypercent);
				paymentTerms.setPayTermComment("Balance Payment");
				billingPayTerms.add(paymentTerms);
				return billingPayTerms;
			}
			else{
				
				PaymentTerms paymentTerms=new PaymentTerms();

				System.out.println(" for 100 % payment recieved ");
				paymentTerms.setPayTermDays(0);
//				double paypercent= Math.round(percentage);
				String pay = numfmt.format(percentage);
				double paypercent= Double.parseDouble(pay);
				
				paymentTerms.setPayTermPercent(paypercent);
				paymentTerms.setPayTermComment("Payment Recieved");
				billingPayTerms.add(paymentTerms);
				return billingPayTerms;
			}
			
			
			
			
//			PaymentTerms paymentTerms=new PaymentTerms();
//
//			System.out.println(" for 100 % payment ");
//			paymentTerms.setPayTermDays(0);
//			double paypercent= Math.round(percentage);
//
//			paymentTerms.setPayTermPercent(paypercent);
//			paymentTerms.setPayTermComment("Payment Recieved");
//			billingPayTerms.add(paymentTerms);
//			return billingPayTerms;
			
		}else{
			
			for(int i=0;i<2;i++){
				System.out.println(" for partial payment");
				if(i==1){
					double percent = 100-percentage;
					System.out.println("Percentage ==== $$$$$$$$$$$$$$$$$ "+percent);
					PaymentTerms paymentTerms=new PaymentTerms();
//					Double.parseDouble(df.format(percent))
					paymentTerms.setPayTermDays(0);  
					double paypercent= Math.round(percent);
					paymentTerms.setPayTermPercent(paypercent);
					paymentTerms.setPayTermComment("Balance Payment");
					billingPayTerms.add(paymentTerms);
					
					return billingPayTerms;

				}else{
					PaymentTerms paymentTerms=new PaymentTerms();

					System.out.println("Payment recieved billing doc ================");
					paymentTerms.setPayTermDays(0);
					
					double paypercent= Math.round(percentage);

					paymentTerms.setPayTermPercent(paypercent);
					paymentTerms.setPayTermComment("Payment Recieved");
					billingPayTerms.add(paymentTerms);

				}
				
			}
		
		}
		
		
//		paymentTerms.setPayTermPercent(percentage);
//		if(paymentermscomment){
//			paymentTerms.setPayTermComment("Payment Recieved");
//		}else{
//			paymentTerms.setPayTermComment("Balance Payment");
//		}
		return billingPayTerms;
	}

	private double getpercentage(Double netpayble, Double paymentrecieved) {
		double totalpercentage;
		totalpercentage= paymentrecieved/netpayble*100;
		return totalpercentage;
	}

	@Override
	public void updateView(Contract view) {
		// TODO Auto-generated method stub
		
		contractObject=view;
		
        	tbContractId.setValue(view.getCount()+"");
        	
//        	this.checkCustomerStatus(view.getCinfo().getCount());
        	
//        	if(view.getTicketNumber()!=-1){
//        		tbTicketNumber.setValue(view.getTicketNumber()+"");
//        		System.out.println("TICKET NUMBER :: "+view.getTicketNumber()+" && COMPLAINT FLAG :: "+view.getComplaintFlag() );
//        		}
        	
		if(view.getCinfo().getCellNumber()!=null){
			personInfoComposite.setValue(view.getCinfo());

		}
		
		if(view.getQuotationCount()!=0){
			tbQuotatinId.setValue(view.getQuotationCount()+"");
		}
		else
		{
			tbQuotatinId.setValue("0");
		}
		/**
    	 *  nidhi
    	 *  29-01-2018
    	 *  get customer contact list
    	 */
    	this.getCustomerContactList(view.getCinfo().getCount());
    	/** end */
		
		if(view.getStartDate()!=null){
			dbContractStartDate.setValue(view.getStartDate());
		}
		if(view.getEndDate()!=null){
			dbContractEndDate.setValue(view.getEndDate());
		}
		if(view.getContractDate()!=null)
			dbContractDate.setValue(view.getContractDate());
		if(view.getStatus()!=null)
			tbStatus.setValue(view.getStatus());
		if(view.getBranch()!=null)
			olbbBranch.setValue(view.getBranch());
		if(view.getEmployee()!=null)
			olbeSalesPerson.setValue(view.getEmployee());
		if(view.getPaymentMethod()!=null)
			olbcPaymentMethods.setValue(view.getPaymentMethod());
		saleslineitemtable.setValue(view.getItems());
		/*if(view.getPayments()!=null)
			paymenttable.setValue(view.getPayments());*/
		if(view.getCategory()!=null)
			olbContractCategory.setValue(view.getCategory());
		if(view.getType()!=null)
			olbContractType.setValue(view.getType());
		if(view.getGroup()!=null)
			olbContractGroup.setValue(view.getGroup());
		if(view.getApproverName()!=null)
			olbApproverName.setValue(view.getApproverName());
		
		if(view.getCreditPeriod()!=null){
			ibCreditPeriod.setValue(view.getCreditPeriod());
		}
//		if(view.isCustomerInterestFlag()==false){
//			tbdonotrenew.setValue("");
//		}
//		else{
//			tbdonotrenew.setValue(AppConstants.CONTRACTDONOTRENEW);
//			tbdonotrenew.getElement().addClassName("redtext");
//		}

			prodTaxTable.setValue(view.getProductTaxes());
			chargesTable.setValue(view.getProductCharges());
			dototalamt.setValue(view.getTotalAmount());
			donetpayamt.setValue(view.getNetpayable());
			
			
			
			serviceScheduleTable.setValue(view.getServiceScheduleList());
			if(view.getScheduleServiceDay()!=null){
				f_serviceDay=view.getScheduleServiceDay();
			}
			
			if(view.getNumberRange()!=null)
				olbcNumberRange.setValue(view.getNumberRange());
			
			/**************** vijay ***************************/
			
			
			if(view.getRefNo()!=null)
				tbReferenceNumber.setValue(view.getRefNo());
			
			if(view.getPaymentRecieved()!=0){
				dbpaymentrecieved.setValue(view.getPaymentRecieved());
			}
			
			if(view.getBalancePayment()!=0){
				dbbalancepayment.setValue(view.getBalancePayment());
				
			}
			
			 /** Date 28-02-2018 By vijay for New customer we added in popup so these below fields no needs so commented ****/
//			if(view.getNewcompanyname()!=null){
//				tbCompanyName.setValue(view.getNewcompanyname());
//			}
//			
//			if(view.getNewcustomerfullName()!=null){
//				tbFullName.setValue(view.getNewcustomerfullName());
//			}
//			
//			if(view.getNewcustomercellNumber()!=null){
//				pnbCellNo.setValue(view.getNewcustomercellNumber());
//			}
//			
//			if(view.getNewcustomerEmail()!=null){
//				etbEmail.setValue(view.getNewcustomerEmail());
//			}
//			
//			if(view.getNewcustomerAddress()!=null){
//				customerAddressComposite.setValue(view.getNewcustomerAddress());
//			}
//			
//			//  rohan added this code for setting customer branch 
//			if(view.getNewCustomerBranch()!= null && !view.getNewCustomerBranch().equals(""))
//			{
//				olbbCustomerBranch.setValue(view.getNewCustomerBranch());
//			}
			
			if(view.getQuickContractInvoiceDate()!=null){
				dbinvoiceDate.setValue(view.getQuickContractInvoiceDate());
			}
			
			//  rohan added this for NBHC
			if(view.getDocument()!=null){
				ucUploadTAndCs.setValue(view.getDocument());
			}
			/***********************************************/
			
			if(view.getDescription()!=null)
				taDescription.setValue(view.getDescription());
			
			if(view.getRefNo2()!=null){
				tbRefNo2.setValue(view.getRefNo2());
			}
			
		//  rohan added this code for adding payment info in the system 
			if(view.getBankAccNo()!=null)
				tbbankAccNo.setValue(view.getBankAccNo());
			if(view.getBankName()!=null)
				tbbankName.setValue(view.getBankName());
			if(view.getBankBranch()!=null)
				tbbankBranch.setValue(view.getBankBranch());
			if(view.getChequeNo()!=null)
				tbchequeNo.setValue(view.getChequeNo());
			if(view.getChequeDate()!=null)
				dbChequeDate.setValue(view.getChequeDate());
			if(view.getReferenceNo()!=null)
				tbreferenceno.setValue(view.getReferenceNo());
			if(view.getAmountTransferDate()!=null)
				dbtransferDate.setValue(view.getAmountTransferDate());
			if(view.getChequeIssuedBy()!=null && !view.getChequeIssuedBy().equals(""))
				tbChequeIssuedBy.setValue(view.getChequeIssuedBy());
			
			/*
			 *  nidhi
			 *  19-07-2017
			 *  for display contract old ref id
			 */
			if(view.getRefContractCount()!=0 && view.getRefContractCount()!=-1){
				ibOldContract.setValue(view.getRefContractCount()+"");
			}
			else
			{
				ibOldContract.setValue("0");
			}
			/*
			 *  end
			 */
			
			if(view.getCustomerGSTNumber()!=null){
				tbGSTNumber.setValue(view.getCustomerGSTNumber());
			}
			/** Date 06/2/2018 added by komal for consolidate price checkbox **/	
			cbConsolidatePrice.setValue(view.isConsolidatePrice());
			/**
			 * end komal
			 */
			
			if(view.getLeadCount()!=-1)
				tbLeadId.setValue(view.getLeadCount()+"");
			/**
			 *  nidhi
			 *  29-01-2018
			 *  contact person
			 */
			objContactList.setValue(view.getPocName());
			/**
			 * end
			 */
			
			/**
			 * @author Anil
			 * @since 13-06-2020
			 */
			if(view.getTechnicianName()!=null){
				olbTechnicainName.setValue(view.getTechnicianName());
			}
			
		presenter.setModel(view);
	
	}
	
	
	public void fieldForRenewalContract(Contract view){
		try {
			
		if(view.getCinfo().getCellNumber()!=null){
			personInfoComposite.setValue(view.getCinfo());

		}
		
		if(view.getQuotationCount()!=0){
			tbQuotatinId.setValue(view.getQuotationCount()+"");
		}
		else
		{
			tbQuotatinId.setValue("0");
		}
		
		ibOldContract.setValue(view.getCount() + "");
		if(view.getBranch()!=null)
			olbbBranch.setValue(view.getBranch());
		if(view.getEmployee()!=null)
			olbeSalesPerson.setValue(view.getEmployee());
		if(view.getPaymentMethod()!=null)
			olbcPaymentMethods.setValue(view.getPaymentMethod());
		saleslineitemtable.setValue(view.getItems());
		/*if(view.getPayments()!=null)
			paymenttable.setValue(view.getPayments());*/
		if(view.getCategory()!=null)
			olbContractCategory.setValue(view.getCategory());
		if(view.getType()!=null)
			olbContractType.setValue(view.getType());
		if(view.getGroup()!=null)
			olbContractGroup.setValue(view.getGroup());
		if(view.getApproverName()!=null)
			olbApproverName.setValue(view.getApproverName());
		
		if(view.getCreditPeriod()!=null){
			ibCreditPeriod.setValue(view.getCreditPeriod());
		}

			prodTaxTable.setValue(view.getProductTaxes());
			chargesTable.setValue(view.getProductCharges());
			dototalamt.setValue(view.getTotalAmount());
			donetpayamt.setValue(view.getNetpayable());
			
			
			
			serviceScheduleTable.setValue(view.getServiceScheduleList());
			if(view.getScheduleServiceDay()!=null){
				f_serviceDay=view.getScheduleServiceDay();
			}
			
			if(view.getNumberRange()!=null)
				olbcNumberRange.setValue(view.getNumberRange());
			
			/**************** vijay ***************************/
			
			
			if(view.getRefNo()!=null)
				tbReferenceNumber.setValue(view.getRefNo());
			
			if(view.getPaymentRecieved()!=0){
				dbpaymentrecieved.setValue(view.getPaymentRecieved());
			}
			
			if(view.getBalancePayment()!=0){
				dbbalancepayment.setValue(view.getBalancePayment());
				
			}
			
			
			if(view.getNewcompanyname()!=null){
				tbCompanyName.setValue(view.getNewcompanyname());
			}
			
			if(view.getNewcustomerfullName()!=null){
				tbFullName.setValue(view.getNewcustomerfullName());
			}
			
			if(view.getNewcustomercellNumber()!=null){
				pnbCellNo.setValue(view.getNewcustomercellNumber());
			}
			
			if(view.getNewcustomerEmail()!=null){
				etbEmail.setValue(view.getNewcustomerEmail());
			}
			
			if(view.getNewcustomerAddress()!=null){
				customerAddressComposite.setValue(view.getNewcustomerAddress());
			}
			
			//  rohan added this code for setting customer branch 
			if(view.getNewCustomerBranch()!= null && !view.getNewCustomerBranch().equals(""))
			{
				olbbCustomerBranch.setValue(view.getNewCustomerBranch());
			}
			
			if(view.getQuickContractInvoiceDate()!=null){
				dbinvoiceDate.setValue(view.getQuickContractInvoiceDate());
			}
			
			//  rohan added this for NBHC
			if(view.getDocument()!=null){
				ucUploadTAndCs.setValue(view.getDocument());
			}
			/***********************************************/
			
			if(view.getDescription()!=null)
				taDescription.setValue(view.getDescription());
			
			if(view.getRefNo2()!=null){
				tbRefNo2.setValue(view.getRefNo2());
			}
			
		//  rohan added this code for adding payment info in the system 
			if(view.getBankAccNo()!=null)
				tbbankAccNo.setValue(view.getBankAccNo());
			if(view.getBankName()!=null)
				tbbankName.setValue(view.getBankName());
			if(view.getBankBranch()!=null)
				tbbankBranch.setValue(view.getBankBranch());
			if(view.getChequeNo()!=null)
				tbchequeNo.setValue(view.getChequeNo());
			if(view.getChequeDate()!=null)
				dbChequeDate.setValue(view.getChequeDate());
			if(view.getReferenceNo()!=null)
				tbreferenceno.setValue(view.getReferenceNo());
			if(view.getAmountTransferDate()!=null)
				dbtransferDate.setValue(view.getAmountTransferDate());
			if(view.getChequeIssuedBy()!=null && !view.getChequeIssuedBy().equals(""))
				tbChequeIssuedBy.setValue(view.getChequeIssuedBy());
			
			dbContractStartDate.setValue(null);
			dbContractDate.setValue(null);
			dbContractEndDate.setValue(null);
			dbinvoiceDate.setValue(null);
			tbContractId.setValue("");
			tbStatus.setValue(view.CREATED);
			tbGSTNumber.setValue(view.getCustomerGSTNumber());
			this.setEnable(true);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Toggles the app header bar menus as per screen state
	 */

	@Override
	public void toggleAppHeaderBarMenu() {
		// TODO Auto-generated method stub

		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Save")||text.contains("Discard")||text.contains("Search")||text.contains(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
				
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Save")||text.contains("Discard")||text.contains(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 
				
				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Edit")||text.contains("Discard")||text.contains("Search")||text.contains("Print")||text.contains(AppConstants.NAVIGATION))
						menus[k].setVisible(true); 
				
				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.QUICKCONTRACT,LoginPresenter.currentModule.trim());
	
	}
	


/**
 * Toogels the Application Process Level Menu as Per Application State
 */
	public void toggleProcessLevelMenu()
	{
		Contract entity=(Contract) presenter.getModel();
		System.out.println("In the Toggle menu");
		String status=entity.getStatus();
		
		System.out.println("GET STATUS ====="+status);
		boolean renewStatus=entity.isRenewFlag();
		boolean donotrenewStatus=entity.isCustomerInterestFlag();
		for(int i=0;i<getProcesslevelBarNames().length;i++)
		{
			InlineLabel label=getProcessLevelBar().btnLabels[i];
			String text=label.getText().trim();

			System.out.println("Text ===== *********"  +text);
			
			if(status.equals(Contract.CREATED))
			{
				Console.log("inside created status");
				if(text.equals(AppConstants.SUBMIT)){
					label.setVisible(true);
				}
				if(text.equals(AppConstants.PAYMENT))
					label.setVisible(false);
				else if(text.equals(AppConstants.CANCLECONTRACT))
					label.setVisible(false);
				else if(text.equals(AppConstants.VIEWSERVICES))
					label.setVisible(false);
				else if(text.equals(AppConstants.PRINTJOBCARD))
					label.setVisible(false);
//				else if(text.equals(AppConstants.MANAGEPROJECT))
//					label.setVisible(false);
				else if(text.equals(AppConstants.CONTRACTRENEWAL))
					label.setVisible(false);
				else if(text.equals(AppConstants.CANCELLATIONSUMMARY))
					label.setVisible(false);
				else if(text.equals(AppConstants.WORKORDER))
					label.setVisible(false);
				else if(text.equals(AppConstants.CUSTOMERINTEREST))
					label.setVisible(false);
				if(text.equals(AppConstants.MATERIALREQUIREDREPORT))
					label.setVisible(false);
				if(text.equals(AppConstants.VIEWBILL))
					label.setVisible(false);
				if(text.equals(AppConstants.VIEWINVOICEPAYMENT))
					label.setVisible(false);
				
			}
			else if(status.equals(Contract.APPROVED)&&renewStatus==false&&donotrenewStatus==false)
			{
				Console.log("inside APPROVED status");
				if(text.equals(AppConstants.Approve))
					label.setVisible(false);	
				if(text.equals(AppConstants.VIEWSERVICES))
					label.setVisible(true);	
				if(entity.getbalance()>0)
				{
					if(text.equals(AppConstants.PAYMENT))
						label.setVisible(true);
				}
				else if(text.equals(AppConstants.CANCLECONTRACT))
					label.setVisible(true);
				if(text.equals(AppConstants.PRINTJOBCARD))
					label.setVisible(true);
//				if(text.equals(AppConstants.MANAGEPROJECT))
//					label.setVisible(true);
				/*
				 *  nidhi
				 *  19-07-2017
				 *  
				 */
				if(text.equals(AppConstants.CONTRACTRENEWAL))
					label.setVisible(true);
				/*
				 *  end
				 */
				if(text.equals(AppConstants.CANCELLATIONSUMMARY))
					label.setVisible(false);
				if(text.equals(AppConstants.WORKORDER))
					label.setVisible(true);
				if(text.equals(AppConstants.CUSTOMERINTEREST))
					label.setVisible(true);
				if(text.equals(AppConstants.MATERIALREQUIREDREPORT))
					label.setVisible(true);
				
				System.out.println("Hi heloooooooooooooooooooooo");
				
				if(text.equals(AppConstants.SUBMIT)){
					label.setVisible(false);
				}
				if(text.equals(AppConstants.VIEWBILL))
					label.setVisible(true);
				
				if(text.equals(AppConstants.VIEWINVOICEPAYMENT))
					label.setVisible(true);
			}
			else if(status.equals(Contract.APPROVED)&&renewStatus==true)
			{
				Console.log("inside APPROVED renewStatus status");
				System.out.println("Hi heloooooooooooooooooooooo 1");

				if(text.equals(AppConstants.Approve))
					label.setVisible(false);	
				if(text.equals(AppConstants.VIEWSERVICES))
					label.setVisible(true);	
				if(entity.getbalance()>0)
				{
					if(text.equals(AppConstants.PAYMENT))
						label.setVisible(true);
				}
				else if(text.equals(AppConstants.CANCLECONTRACT))
					label.setVisible(false);
				if(text.equals(AppConstants.PRINTJOBCARD))
					label.setVisible(true);
//				if(text.equals(AppConstants.MANAGEPROJECT))
//					label.setVisible(true);
				/*
				 *  nidhi
				 *  19-07-2017
				 *   CONTRACTRENEWAL field make visible for approval contract
				 */
				else if(text.equals(AppConstants.CONTRACTRENEWAL)){
					label.setVisible(false);
				}
				/*
				 *  end
				 */
				if(text.equals(AppConstants.CANCELLATIONSUMMARY))
					label.setVisible(false);
				if(text.equals(AppConstants.WORKORDER))
					label.setVisible(true);
				if(text.equals(AppConstants.CUSTOMERINTEREST))
					label.setVisible(false);
				if(text.equals(AppConstants.MATERIALREQUIREDREPORT))
					label.setVisible(true);
				if(text.equals(AppConstants.VIEWBILL))
					label.setVisible(true);
				if(text.equals(AppConstants.VIEWINVOICEPAYMENT))
					label.setVisible(true);
				
				if(text.equals(AppConstants.SUBMIT)){
					label.setVisible(false);
				}
				
			}
			else if(status.equals(Contract.CONTRACTEXPIRED))
			{
				Console.log("inside CONTRACTEXPIRED status");
				if(text.contains(AppConstants.RENEW))
					label.setVisible(true);
				else
					label.setVisible(false);
				
				if(text.equals(AppConstants.CANCELLATIONSUMMARY))
					label.setVisible(false);
				if(text.equals(AppConstants.CUSTOMERINTEREST))
					label.setVisible(false);
				if(text.equals(AppConstants.VIEWBILL))
					label.setVisible(false);
				if(text.equals(AppConstants.VIEWINVOICEPAYMENT))
					label.setVisible(false);
				if(text.equals(AppConstants.SUBMIT)){
					label.setVisible(false);
				}
			}

			
			
			else if(status.equals(Contract.REQUESTED))
			{
				Console.log("inside REQUESTED status");
				if((text.contains(AppConstants.PRINTJOBCARD)||text.contains(AppConstants.PAYMENT)||
						text.contains(AppConstants.VIEWSERVICES)||text.contains(AppConstants.VIEWSERVICES)||
						text.contains(ManageApprovals.APPROVALREQUEST))||text.contains(AppConstants.CREATECONTRACT)||text.contains(AppConstants.CONTRACTRENEWAL))
					label.setVisible(false);
				else
					label.setVisible(true);
				
				if(text.equals(AppConstants.MATERIALREQUIREDREPORT))
					label.setVisible(false);
				if(text.equals(AppConstants.CANCLECONTRACT))
					label.setVisible(false);
				if(text.equals(AppConstants.CANCELLATIONSUMMARY))
					label.setVisible(false);
				if(text.equals(AppConstants.WORKORDER))
					label.setVisible(false);
				if(text.equals(AppConstants.CUSTOMERINTEREST))
					label.setVisible(false);
				if(text.equals(AppConstants.VIEWBILL))
					label.setVisible(false);
				
				if(text.equals(AppConstants.VIEWINVOICEPAYMENT))
					label.setVisible(false);
				
				if(text.equals(AppConstants.SUBMIT)){
					label.setVisible(false);
				}
			}
			
			else if(status.equals(Contract.REJECTED))
			{
				Console.log("inside REJECTED status");
				if(text.contains(AppConstants.NEW))
					label.setVisible(true);
				else
					label.setVisible(false);
				
				if(text.equals(AppConstants.CANCELLATIONSUMMARY))
					label.setVisible(false);
				else if(text.equals(AppConstants.WORKORDER))
					label.setVisible(false);
				if(text.equals(AppConstants.CUSTOMERINTEREST))
					label.setVisible(false);
				if(text.equals(AppConstants.MATERIALREQUIREDREPORT))
					label.setVisible(false);
				if(text.equals(AppConstants.VIEWBILL))
					label.setVisible(false);
				
				if(text.equals(AppConstants.VIEWINVOICEPAYMENT))
					label.setVisible(false);
				
				if(text.equals(AppConstants.SUBMIT)){
					label.setVisible(false);
				}
			}
			else if(status.equals(Contract.APPROVED)&&donotrenewStatus==true)
			{
				System.out.println("Hi heloooooooooooooooooooooo 2");

				Console.log("inside APPROVED  donotrenewStatus status");
				if(text.equals(AppConstants.CUSTOMERINTEREST)){
					label.setVisible(false);
				}
				else if(text.equals(AppConstants.CONTRACTRENEWAL)){
					label.setVisible(false);
				}
				else if(text.equals(ManageApprovals.APPROVALREQUEST)){
					label.setVisible(false);
				}
				else if(text.equals(ManageApprovals.CANCELAPPROVALREQUEST)){
					label.setVisible(false);
				}
				else if(text.equals(AppConstants.CANCELLATIONSUMMARY)){
					label.setVisible(false);
				}
				else if(text.equals(AppConstants.MATERIALREQUIREDREPORT))
					label.setVisible(true);
				else{
					label.setVisible(true);
				}
				if(text.equals(AppConstants.VIEWBILL))
					label.setVisible(true);
				
				if(text.equals(AppConstants.VIEWINVOICEPAYMENT))
					label.setVisible(false);
				
				if(text.equals(AppConstants.SUBMIT)){
					label.setVisible(false);
				}
			}
			else if(status.equals(Contract.CONTRACTCANCEL))
			{
				Console.log("inside CONTRACTCANCEL status");
				
				
				if(text.equals(ManageApprovals.APPROVALREQUEST))
					label.setVisible(false);
				if(text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
					label.setVisible(false);
				
				if((text.contains(AppConstants.NEW)))
					label.setVisible(true);
				else
					label.setVisible(false);
				
				if(text.equals(AppConstants.CANCLECONTRACT))
					label.setVisible(false);
				if(text.equals(AppConstants.CONTRACTRENEWAL))
					label.setVisible(false);
				
				
				if(text.equals(AppConstants.VIEWINVOICEPAYMENT))
					label.setVisible(false);
				
				if(text.equals(AppConstants.SUBMIT)){
					label.setVisible(false);
				}
				if(text.equals(AppConstants.CANCELLATIONSUMMARY)){
					label.setVisible(true);
				}
				Console.log("inside CONTRACTCANCEL status ");
			}
//			else if(status.equals(Contract.APPROVED))
//			{
//				
//				System.out.println("Hi heloooooooooooooooooooooo");
//				
//				if(text.equals(AppConstants.QUICKCONTRACT)){
//					label.setVisible(false);
//				}
//				
//			}
		}
	}
	
	
	/**
	 * Sets the Application Header Bar as Per Status
	 */

	public void setAppHeaderBarAsPerStatus()
	{
		Contract entity=(Contract) presenter.getModel();
		String status=entity.getStatus();
		
		if(status.equals(Contract.CONTRACTEXPIRED)||status.equals(Contract.CONTRACTCANCEL)||status.equals(Contract.REJECTED)||status.equals(Contract.APPROVED))
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Discard")||text.contains("Search")||text.contains("Print")||text.equals(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
					System.out.println("Value of text is "+menus[k].getText());
				}
				else
				{
					menus[k].setVisible(false);  
				}
			}
		}

		if(status.equals(Contract.APPROVED))
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Discard")||text.contains("Search")||text.contains("Email")
						||text.contains("Print")||text.equals(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 

				}
				else
				{
					menus[k].setVisible(false);  
				}
			}
		}
		
		if(status.equals(Contract.REQUESTED))
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Discard")||text.contains("Search")||text.contains("Print")||text.equals(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 

				}
				else
				{
					menus[k].setVisible(false);  
				}
			}
		}
		
		if(status.equals(Contract.REJECTED))
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Discard")||text.contains("Search")||text.contains("Print")||text.contains("Edit")||text.equals(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 

				}
				else
				{
					menus[k].setVisible(false);  
				}
			}
		}
		
	/*	if(status.equals(Contract.CREATED))
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Discard")||text.contains("Search")||text.contains("Save")
						||text.contains("Edit"))
				{
					menus[k].setVisible(true); 

				}
				else
				{
					menus[k].setVisible(false);  
				}
			}
		}
*/


		/*else if(status.equals(Contract.CREATED))
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Discard")||text.contains("Search")
						||text.contains("Print")||text.contains("Save")||text.contains("Edit"))
				{
					menus[k].setVisible(true); 

				}
				else
				{
					menus[k].setVisible(false);  
				}
			}
		}*/
	}
	
	
	/**
	 * The method is responsible for changing Application state as per 
	 * status
	 */
	public void setMenuAsPerStatus()
	{
		this.setAppHeaderBarAsPerStatus();
		this.toggleProcessLevelMenu();
		
	}

	
	/**
	 * sets the id textbox with the passed count value. 
	 */
	@Override
	public void setCount(int count)
	{
		tbContractId.setValue(count+"");
	}
	
	/****************************************On Click Mehtod*******************************************/
	/**
	 * This method is called on click of any widget.
	 * 
	 *  Involves Add Charges Button (Plus symbol)
	 * 	Involves OtmComposite i.e Products which are added
	 */
	
	
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
		if(event.getSource().equals(mailpopup.getLblOk())){
			Console.log("inside mailpopup ok button "+this.getPersonInfoComposite().getIdValue());
			validateCustomerEmailId(this.getPersonInfoComposite().getIdValue());
			
		}
		
		
		
		if(event.getSource().equals(addOtherCharges)){
			int size=chargesTable.getDataprovider().getList().size();
			if(this.doamtincltax.getValue()!=null){
				ProductOtherCharges prodCharges=new ProductOtherCharges();
				prodCharges.setAssessableAmount(this.getDoamtincltax().getValue());
				prodCharges.setFlagVal(false);
				prodCharges.setIndexCheck(size+1);
				this.chargesTable.getDataprovider().getList().add(prodCharges);
			}
		}
		
		if(event.getSource().equals(btnaddproducts)){
			System.out.println("Hi   111111111111");
			
			if(personInfoComposite.getId().getValue().equals("")){
				showDialogMessage("Please add customer information!");
			}
			 /**
			 * Date 10/04/2018 By Vijay
			 * setting contract Date to start date
			 */
			else if(dbContractDate.getValue()==null){
				showDialogMessage("Please add Contract Date!");
				return;
			}
			
//			if(!this.tbFullName.getValue().equals("") || !this.tbCompanyName.getValue().equals("")  ){
//				System.out.println(" for address incomplete");
//				System.out.println("add == "+this.customerAddressComposite.getAdressline1().getValue());
//				
//				if(this.customerAddressComposite.getAdressline1().getValue().equals("") || this.customerAddressComposite.getCountry().getSelectedIndex()==0 || this.customerAddressComposite.getState().getSelectedIndex()==0 || this.customerAddressComposite.getCity().getSelectedIndex()==0 ){
//					showDialogMessage("Please add new customer complete address!");
//					return;
//				}
//					
//			}
			
			
//			else{
				
				System.out.println(" hi ===== 222222");
				
				if(olbbBranch.getValue()!=null){
					if(!prodInfoComposite.getProdCode().getValue().equals("")){
						System.out.println("PrintPCD"+prodInfoComposite.getProdCode().getValue());
						this.checkCustomerBranch(personInfoComposite.getIdValue());
						
						//  this code is commented by rohan for pesto india changes 
						
//						boolean productValidation=validateProductDuplicate(prodInfoComposite.getProdCode().getValue().trim());
//						
//						if(productValidation==true){
//							showDialogMessage("Product already exists!");
//						}
//						else{
							this.ProdctType(this.prodInfoComposite.getProdCode().getValue().trim(),this.prodInfoComposite.getProdID().getValue().trim());
							/**
							 * Date : 12-10-2017 BY ANIL
							 * After adding product ,product composite should be clear
							 */
							prodInfoComposite.clear();
//						}
					}
				}else{
					showDialogMessage("Please select branch !");
				}
//			}
		}
		
	}
	
	
	
	

	public boolean checkProducts(String pcode)
	{
		List<SalesLineItem> salesitemlis=saleslineitemtable.getDataprovider().getList();
		for(int i=0;i<salesitemlis.size();i++)
		{
			if(salesitemlis.get(i).getProductCode().equals(pcode)||salesitemlis.get(i).getProductCode()==pcode){
				return true;
			}
		}
		return false;
	}
	
	
	public void ProdctType(String pcode,String productId)
	{
		final GenricServiceAsync genasync=GWT.create(GenricService.class);
		
		Vector<Filter> filtervec=new Vector<Filter>();
		int prodId=Integer.parseInt(productId);
		
		final MyQuerry querry=new MyQuerry();
		Filter filter=new Filter();
		filter.setQuerryString("productCode");
		filter.setStringValue(pcode.trim());
		filtervec.add(filter);
		
		filter=new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(prodId);
		filtervec.add(filter);
		
		filter=new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);
		
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new SuperProduct());
		showWaitSymbol();
		 Timer timer=new Timer() 
    	 {
				@Override
				public void run() {
			genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onFailure(Throwable caught) {
					showDialogMessage("An Unexpected error occurred!");
				}
	
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					
					
					if(result.size()==0)
					{
						showDialogMessage("Please check whether product status is active or not!");
					}
					SalesLineItem lis=null;
					
					for(SuperModel model:result)
					{
						SuperProduct superProdEntity = (SuperProduct)model;
//						lis=AppUtility.ReactOnAddProductComposite(superProdEntity);
						Branch branchEntity = olbbBranch.getSelectedItem();
						boolean gstApplicable = false;
						if(olbcNumberRange.getSelectedIndex()!=0){
							Config configEntity = olbcNumberRange.getSelectedItem();
							if(configEntity!=null)
							gstApplicable = configEntity.isGstApplicable();
						}
						else{
							gstApplicable = true;
						}
						lis=AppUtility.ReactOnAddProductComposite(superProdEntity,customerEntity,branchEntity,gstApplicable);

						
						lis.setQuantity(1.0);
						lis.setProductSrNo(productSrNo+1);
						
						
						
						/**
						 * Date 30-04-2018 
						 * Developer : Vijay
						 * Des : Customer braches adding it into hashmap so we can load more customer bracnhes large data
						 */
						String custUOM = "";
//						if(cust.getUnitOfMeasurement()!=null && cust.getUnitOfMeasurement().trim().length()>0){
//							custUOM = cust.getUnitOfMeasurement();
//						}else{
							custUOM = superProdEntity.getUnitOfMeasurement();
//						}
						ArrayList<BranchWiseScheduling> branchlist = AppUtility.getCustomerBranchSchedulinglistInfo(customerbranchlist,olbbBranch.getValue(),custUOM,0);
						Integer prodSrNo = lis.getProductSrNo();
						
						
						HashMap<Integer, ArrayList<BranchWiseScheduling>> customerBranchlist = new HashMap<Integer, ArrayList<BranchWiseScheduling>>();
						customerBranchlist.put(prodSrNo, branchlist);
						lis.setCustomerBranchSchedulingInfo(customerBranchlist);
						
						
						
						
					    getSaleslineitemtable().getDataprovider().getList().add(lis);
					    productSrNo++;
					    System.out.println("Rohan I value "+productSrNo);
					    
					    /**
						 * Date 10/04/2018 By Vijay
						 * setting contract Date to start date
						 */
						lis.setStartDate(dbContractDate.getValue());
						
						/**
						 * ends here
						 */
					}
					
					if(serviceScheduleTable.getDataprovider().getList().size() == 0){
						
						 List<ServiceSchedule> popuplist=reactfordefaultTable(lis);
						 getServiceScheduleTable().getDataprovider().setList(popuplist);
						 
						 
						 //  rohan code for testing 
						 
						 for (int i = 0; i < getServiceScheduleTable().getDataprovider().getList().size(); i++) {
							
							System.out.println("my warm up rohan"+getServiceScheduleTable().getDataprovider().getList().get(i).getSerSrNo());
						}
						 
						}else{
							
							 List<ServiceSchedule> privlist=serviceScheduleTable.getDataprovider().getList();
									 
							 List<ServiceSchedule> popuplist=reactfordefaultTable(lis);
							 
							 List<ServiceSchedule> popuplist1=new ArrayList<ServiceSchedule>();
							 
							 popuplist1.addAll(privlist);
							 popuplist1.addAll(popuplist);
							 
							 getServiceScheduleTable().getDataprovider().setList(popuplist1);
							 
							 for (int i = 0; i < getServiceScheduleTable().getDataprovider().getList().size(); i++) {
									
									System.out.println("my warm up rohan12345"+getServiceScheduleTable().getDataprovider().getList().get(i).getSerSrNo());
								}
							}
					}
				 });
				hideWaitSymbol();
 				}
	 	  };
          timer.schedule(3000); 
	}
	
	
/*****************************************Add Product Taxes Method***********************************/
	
	/**
	 * This method is called when a product is added to SalesLineItem table.
	 * Taxes related to corresponding products will be added in ProductTaxesTable.
	 * @throws Exception
	 */
	public void addProdTaxes() throws Exception
	{
		List<SalesLineItem> salesLineItemLis=this.saleslineitemtable.getDataprovider().getList();
		List<ProductOtherCharges> taxList=this.prodTaxTable.getDataprovider().getList();
		NumberFormat nf=NumberFormat.getFormat("#.00");
		for(int i=0;i<salesLineItemLis.size();i++)
		{
			double priceqty=0,taxPrice=0;
			
			//*******************old code for discount **************
			
//			if(salesLineItemLis.get(i).getPercentageDiscount()==null){
//				taxPrice=this.getSaleslineitemtable().removeAllTaxes(salesLineItemLis.get(i).getPrduct());
//				priceqty=(salesLineItemLis.get(i).getPrice()-taxPrice)*salesLineItemLis.get(i).getQty();
//				priceqty=Double.parseDouble(nf.format(priceqty));
//			}
//			if(salesLineItemLis.get(i).getPercentageDiscount()!=null){
//				taxPrice=this.getSaleslineitemtable().removeAllTaxes(salesLineItemLis.get(i).getPrduct());
//				priceqty=(salesLineItemLis.get(i).getPrice()-taxPrice);
//				priceqty=priceqty-(priceqty*salesLineItemLis.get(i).getPercentageDiscount()/100);
//				priceqty=priceqty*salesLineItemLis.get(i).getQty();
//				priceqty=Double.parseDouble(nf.format(priceqty));
//			}
			
			
			//****************new code for discount by rohan ***************
			
			double area =0;
			if(!salesLineItemLis.get(i).getArea().equalsIgnoreCase("NA"))
			{
				area = Double.parseDouble(salesLineItemLis.get(i).getArea());
			}
			else
			{
				area = 1.0;
			}
			
	if((salesLineItemLis.get(i).getPercentageDiscount()==null && salesLineItemLis.get(i).getPercentageDiscount()==0) && (salesLineItemLis.get(i).getDiscountAmt()==0) ){
				
				System.out.println("inside both 0 condition");
				
				taxPrice=this.getSaleslineitemtable().removeAllTaxes(salesLineItemLis.get(i).getPrduct());
				priceqty=(salesLineItemLis.get(i).getPrice()-taxPrice) * area;
				priceqty=Double.parseDouble(nf.format(priceqty));
			}
			
			else if(((salesLineItemLis.get(i).getPercentageDiscount()!=null)&& (salesLineItemLis.get(i).getDiscountAmt()!=0))){
				
				System.out.println("inside both not null condition");
				
				taxPrice=this.getSaleslineitemtable().removeAllTaxes(salesLineItemLis.get(i).getPrduct());
				priceqty=(salesLineItemLis.get(i).getPrice()-taxPrice)* area;
				priceqty=priceqty-(priceqty*salesLineItemLis.get(i).getPercentageDiscount()/100);
				
				priceqty=priceqty-salesLineItemLis.get(i).getDiscountAmt();
				
//				priceqty=priceqty*salesLineItemLis.get(i).getQty();
				priceqty=Double.parseDouble(nf.format(priceqty));
			}
			else 
			{
				System.out.println("inside oneof the null condition");
				
				
				taxPrice=this.getSaleslineitemtable().removeAllTaxes(salesLineItemLis.get(i).getPrduct());
				priceqty=(salesLineItemLis.get(i).getPrice()-taxPrice)* area;
				
				if(salesLineItemLis.get(i).getPercentageDiscount()!=null){
				priceqty=priceqty-(priceqty*salesLineItemLis.get(i).getPercentageDiscount()/100);
				}
				else 
				{
				priceqty=priceqty-salesLineItemLis.get(i).getDiscountAmt();
				}
//				priceqty=priceqty*salesLineItemLis.get(i).getQty();
				priceqty=Double.parseDouble(nf.format(priceqty));
			}
			
			//****************changes ends here *********************************
			
		/**
		 * Date 03-07-2017 below code commented by vijay 
		 * for this below code by default showing vat and service tax percent 0 no need this
		 */
	
//			if(salesLineItemLis.get(i).getVatTax().getPercentage()==0){
//				ProductOtherCharges pocentity=new ProductOtherCharges();
//				pocentity.setChargeName("VAT");
//				pocentity.setChargePercent(salesLineItemLis.get(i).getVatTax().getPercentage());
//				pocentity.setIndexCheck(i+1);
//				int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getVatTax().getPercentage(),"VAT");
//				if(indexValue!=-1){
//					pocentity.setAssessableAmount(0);
//					this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
//				}
//				
//				if(indexValue==-1){
//					pocentity.setAssessableAmount(0);
//				}
//				this.prodTaxTable.getDataprovider().getList().add(pocentity);
//			}
//			
//			if(salesLineItemLis.get(i).getServiceTax().getPercentage()==0){
//				ProductOtherCharges pocentity=new ProductOtherCharges();
//				pocentity.setChargeName("Service Tax");
//				pocentity.setChargePercent(salesLineItemLis.get(i).getServiceTax().getPercentage());
//				pocentity.setIndexCheck(i+1);
//				int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getServiceTax().getPercentage(),"Service");
//				if(indexValue!=-1){
//					pocentity.setAssessableAmount(0);
//					this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
//				}
//				
//				if(indexValue==-1){
//					pocentity.setAssessableAmount(0);
//				}
//				this.prodTaxTable.getDataprovider().getList().add(pocentity);
//			}

			/**
			 * ends here	
			 */
		
			if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
				
			//  rohan added this conditions
				if(salesLineItemLis.get(i).getVatTax().getTaxPrintName()!=null && !salesLineItemLis.get(i).getVatTax().getTaxPrintName().equals(""))
				{
					System.out.println("Inside vat GST");
					ProductOtherCharges pocentity=new ProductOtherCharges();
					//   old code commented by rohan on date : 22-6-2017
	//				pocentity.setChargeName("VAT");
					//   new code by rohan Date 22-6-2017 
					System.out.println("salesLineItemLis.get(i).getVatTax().getTaxName()"+salesLineItemLis.get(i).getVatTax().getTaxPrintName());
					pocentity.setChargeName(salesLineItemLis.get(i).getVatTax().getTaxPrintName());
					pocentity.setChargePercent(salesLineItemLis.get(i).getVatTax().getPercentage());
					pocentity.setIndexCheck(i+1);
					int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getVatTax().getPercentage(),salesLineItemLis.get(i).getVatTax().getTaxPrintName());
					
					if(indexValue!=-1){
						System.out.println("Index As Not -1");
						pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
//						pocentity.setAssessableAmount(priceqty);
						this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
					}
					if(indexValue==-1){
						pocentity.setAssessableAmount(priceqty);
					}
					this.prodTaxTable.getDataprovider().getList().add(pocentity);
				}
				else{
					System.out.println("Vattx");
					ProductOtherCharges pocentity=new ProductOtherCharges();
					pocentity.setChargeName("VAT");
					pocentity.setChargePercent(salesLineItemLis.get(i).getVatTax().getPercentage());
					pocentity.setIndexCheck(i+1);
					int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getVatTax().getPercentage(),"VAT");
					
					if(indexValue!=-1){
						System.out.println("Index As Not -1");
						pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
						this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
					}
					if(indexValue==-1){
						pocentity.setAssessableAmount(priceqty);
					}
					this.prodTaxTable.getDataprovider().getList().add(pocentity);
				}
			}
			
			if(salesLineItemLis.get(i).getServiceTax().getPercentage()!=0){
				if(salesLineItemLis.get(i).getServiceTax().getTaxPrintName()!=null && !salesLineItemLis.get(i).getServiceTax().getTaxPrintName().equals(""))
				{
					System.out.println("Inside service GST");
					ProductOtherCharges pocentity=new ProductOtherCharges();
					//   old code commented by rohan on date : 22-6-2017
	//				pocentity.setChargeName("VAT");
					//   new code by rohan Date 22-6-2017 
					System.out.println("salesLineItemLis.get(i).getServiceTax().getTaxName()"+salesLineItemLis.get(i).getServiceTax().getTaxPrintName());
					pocentity.setChargeName(salesLineItemLis.get(i).getServiceTax().getTaxPrintName());
					pocentity.setChargePercent(salesLineItemLis.get(i).getServiceTax().getPercentage());
					pocentity.setIndexCheck(i+1);
					int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getVatTax().getPercentage(),salesLineItemLis.get(i).getServiceTax().getTaxPrintName());
					
					if(indexValue!=-1){
						System.out.println("Index As Not -1");
						pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
//						pocentity.setAssessableAmount(priceqty);
						this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
					}
					if(indexValue==-1){
						pocentity.setAssessableAmount(priceqty);
					}
					this.prodTaxTable.getDataprovider().getList().add(pocentity);
				}
				else{
						System.out.println("ServiceTx");
						ProductOtherCharges pocentity=new ProductOtherCharges();
						pocentity.setChargeName("Service Tax");
						pocentity.setChargePercent(salesLineItemLis.get(i).getServiceTax().getPercentage());
						pocentity.setIndexCheck(i+1);
						int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getServiceTax().getPercentage(),"Service");
						if(indexValue!=-1){
							System.out.println("Index As Not -1");
							double assessValue=0;
							if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
								assessValue=priceqty+(priceqty*salesLineItemLis.get(i).getVatTax().getPercentage()/100);
								pocentity.setAssessableAmount(assessValue+taxList.get(indexValue).getAssessableAmount());
								this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
							}
							if(salesLineItemLis.get(i).getVatTax().getPercentage()==0){
								pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
								this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
							}
							
						
					}
					if(indexValue==-1){
						System.out.println("Index As -1");
						double assessVal=0;
						if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
							assessVal=priceqty+(priceqty*salesLineItemLis.get(i).getVatTax().getPercentage()/100);
							pocentity.setAssessableAmount(assessVal);
						}
						if(salesLineItemLis.get(i).getVatTax().getPercentage()==0){
							pocentity.setAssessableAmount(priceqty);
						}
						
					}
					this.prodTaxTable.getDataprovider().getList().add(pocentity);
				}
			}
			
			
		}
	}
	
	/*********************************Check Tax Percent*****************************************/
	/**
	 * This method returns 
	 * @param taxValue
	 * @param taxName
	 * @return
	 */
	
	public int checkTaxPercent(double taxValue,String taxName)
	{
		List<ProductOtherCharges> taxesList=this.prodTaxTable.getDataprovider().getList();
		for(int i=0;i<taxesList.size();i++)
		{
			double listval=taxesList.get(i).getChargePercent();
			int taxval=(int)listval;
			
			if(taxName.equals("Service")){
				if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("Service Tax")){
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("Service Tax")){
					return i;
				}
			}
			if(taxName.equals("VAT")){
				if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("VAT")){
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("VAT")){
					return i;
				}
			}
			
		//  rohan added this for GST
			if(taxName.equals("CGST")){
				if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("CGST")){
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("CGST")){
					return i;
				}
			}
			
		//  rohan added this for GST
			if(taxName.equals("SGST")){
				if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("SGST")){
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("SGST")){
					return i;
				}
			}
			
		//  rohan added this for GST
			if(taxName.equals("IGST")){
				if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("IGST")){
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("IGST")){
					return i;
				}
			}
		}
		return -1;
	}
	
	/*************************Update Charges Table****************************************/
	/**
	 * This method updates the charges table.
	 * The table is updated when any row value is changed through field updater in table i.e
	 * row change event is fired
	 */
	
	public void updateChargesTable()
	{
		List<ProductOtherCharges> prodOtrLis=this.chargesTable.getDataprovider().getList();
		ArrayList<ProductOtherCharges> prodOtrArr=new ArrayList<ProductOtherCharges>();
		prodOtrArr.addAll(prodOtrLis);
		
		this.chargesTable.connectToLocal();
		System.out.println("prodo"+prodOtrArr.size());
		for(int i=0;i<prodOtrArr.size();i++)
		{
			ProductOtherCharges prodChargesEnt=new ProductOtherCharges();
			prodChargesEnt.setChargeName(prodOtrArr.get(i).getChargeName());
			prodChargesEnt.setChargePercent(prodOtrArr.get(i).getChargePercent());
			prodChargesEnt.setChargeAbsValue(prodOtrArr.get(i).getChargeAbsValue());
			//prodChargesEnt.setAssessableAmount(prodOtrArr.get(i).getAssessableAmount());
			prodChargesEnt.setFlagVal(prodOtrArr.get(i).getFlagVal());
			prodChargesEnt.setIndexCheck(prodOtrArr.get(i).getIndexCheck());
			if(prodOtrArr.get(i).getFlagVal()==false){
				prodChargesEnt.setAssessableAmount(this.getDoamtincltax().getValue());
			}
			if(prodOtrArr.get(i).getFlagVal()==true){
				double assesableVal=this.retrieveSurchargeAssessable(prodOtrArr.get(i).getIndexCheck());
				prodChargesEnt.setAssessableAmount(assesableVal);
			}
			
			this.chargesTable.getDataprovider().getList().add(prodChargesEnt);
		}
	}
	
	public double retrieveSurchargeAssessable(int indexValue)
	{
		List<ProductOtherCharges> otrChrgLis=this.chargesTable.getDataprovider().getList();
		ArrayList<ProductOtherCharges> otrChrgArr=new ArrayList<ProductOtherCharges>();
		otrChrgArr.addAll(otrChrgLis);
	
		double getAssessVal=0;
		for(int i=0;i<otrChrgArr.size();i++)
		{
			if(otrChrgArr.get(i).getFlagVal()==false&&otrChrgArr.get(i).getIndexCheck()==indexValue)
			{
				if(otrChrgArr.get(i).getChargePercent()!=0){
					getAssessVal=otrChrgArr.get(i).getAssessableAmount()*otrChrgArr.get(i).getChargePercent()/100;
				}
				if(otrChrgArr.get(i).getChargeAbsValue()!=0){
					getAssessVal=otrChrgArr.get(i).getChargeAbsValue();
				}
			}
		}
		return getAssessVal;
	}
	
	
	public boolean validateContractProdQty()
	{
		List<SalesLineItem> contractprodlis=saleslineitemtable.getDataprovider().getList();
		int ctr=0;
		for(int i=0;i<contractprodlis.size();i++)
		{
			if(contractprodlis.get(i).getQty()==0)
			{
				ctr=ctr+1;
			}
		}
		
		if(ctr==0){
			return true;
		}
		else{
			return false;
		}
	}
	
	
	public boolean validateServicePopupBranch(){
		System.out.println("  customer branch "+customerbranchlist.size());
		if(customerbranchlist.size()!=0){
			List<ServiceSchedule> prodservicelist=this.serviceScheduleTable.getDataprovider().getList();
			for(int i=0;i<prodservicelist.size();i++){
				
				if(prodservicelist.get(i).getScheduleProBranch()==null){
					return false;
				}
			}
			return true;
		}else{
			return true;
		}
	}
	
	
	/************************************Overridden Methods*******************************************/
	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
//		getstatustextbox().setEnabled(false);
		this.tbContractId.setEnabled(false);
		this.saleslineitemtable.setEnable(state);
		this.chargesTable.setEnable(state);
		this.prodTaxTable.setEnable(state);
		this.doamtincltax.setEnabled(false);
		this.donetpayamt.setEnabled(false);
		this.dototalamt.setEnabled(false);
		this.dbContractStartDate.setEnabled(false);
		this.dbContractEndDate.setEnabled(false);
		this.f_btnservice.setEnabled(true);
		this.ibOldContract.setEnabled(false);
		this.tbQuotatinId.setEnabled(false);
		this.tbStatus.setEnabled(false);
		
		/**
		 * Date : 12-10-2017 BY ANIL
		 * if number range is selected and contract id got generated then number range should not be editable
		 */
		if(tbContractId.getValue()!=null&&!tbContractId.getValue().equals("")){
			this.olbcNumberRange.setEnabled(false);
		}
		/**
		 * End
		 */
		
		this.tbLeadId.setEnabled(false);
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("NumberRange", "MakeNumberRangeReadOnly"))
  		{	olbcNumberRange.setEnabled(false);
  		}
	}

	@Override
	public void setToViewState() {
	
		super.setToViewState();
		setMenuAsPerStatus();
		
		SuperModel model=new Contract();
		model=contractObject;
		
		
		if(customerEditFlag==true){
			System.out.println("Customer edit =======   ");
			this.setToNewState();
			QuickContractPresentor.initalize();
			
		}
		
		AppUtility.addDocumentToHistoryTable(AppConstants.SERVICEMODULE,AppConstants.QUICKCONTRACTSCREEN, contractObject.getCount(), Integer.parseInt(personInfoComposite.getId().getValue()),personInfoComposite.getName().getValue(),Long.parseLong(personInfoComposite.getPhone().getValue()), false, model, null);
	
		String mainScreenLabel="Contract Details";
		if(contractObject!=null&&contractObject.getCount()!=0){
			mainScreenLabel=contractObject.getCount()+" "+"/"+" "+contractObject.getStatus()+" "+ "/"+" "+AppUtility.parseDate(contractObject.getCreationDate());
		}
		

		fgroupingCustomerInformation.getHeaderLabel().setText(mainScreenLabel);
	
	
	}
	
	@Override
	public void setToEditState() {
	
		super.setToEditState();
		this.processLevelBar.setVisibleFalse(false);
		
//		if(!tbQuotatinId.getValue().equals(""))
//		{
////			baddproducts.setEnabled(false);
//			addOtherCharges.setEnabled(false);
//		}
//		changeStatusToCreated();
		
		String mainScreenLabel="Contract Details";
		if(contractObject!=null&&contractObject.getCount()!=0){
			mainScreenLabel=contractObject.getCount()+" "+"/"+" "+contractObject.getStatus()+" "+ "/"+" "+AppUtility.parseDate(contractObject.getCreationDate());
		}
		

		fgroupingCustomerInformation.getHeaderLabel().setText(mainScreenLabel);
		
	}
	
	public void changeStatusToCreated()
	{
//		SalesOrder entity=(SalesOrder) presenter.getModel();
//		String status=entity.getStatus();
//		
//		if(SalesOrder.REJECTED.equals(status.trim()))
//		{
//			this.tbStatus.setValue(SalesOrder.CREATED);
//		}
	}
	
	@Override
	public void clear() {
		super.clear();
//		getstatustextbox().setText(Contract.CREATED);
	}
	
//	@Override
//	public TextBox getstatustextbox() {
//		return this.tbStatus;
//	}
	
	/**
	 * Validate For Duplicate Products
	 */
	
	public boolean validateProductDuplicate(String pcode)
	{
		List<SalesLineItem> salesItemLis=this.getSaleslineitemtable().getDataprovider().getList();
		for(int i=0;i<salesItemLis.size();i++)
		{
			System.out.println("salesl"+salesItemLis.get(i).getProductCode());
			if(salesItemLis.get(i).getProductCode().trim().equals(pcode)){
				return true;
			}
		}
		System.out.println("Out");
		return false;
	}
	

	public void validateCustomerEmailId(int count){
		 Console.log("inside validateCustomerEmailId "+count);
		
		
		final MyQuerry querry=new MyQuerry();
		Vector<Filter> temp=new Vector<Filter>();
		Filter filter=null;
		Company c = new Company();
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		temp.add(filter);
		
		
		
		filter=new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(count);
		temp.add(filter);
		
		querry.setFilters(temp);
		querry.setQuerryObject(new Customer());
//		showWaitSymbol();
		
		genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
            Console.log("result size "+result.size());
				for(SuperModel model:result){
					
					Customer customer = (Customer)model;
					
						customer.setEmail(mailpopup.getTbEmailBox().getValue().trim());
						Console.log("cust email "+customer.getEmail());
						customerEmail=customer.getEmail();
						genasync.save(customer, new AsyncCallback<ReturnFromServer>() {
							
							@Override
							public void onSuccess(ReturnFromServer result) {
								Console.log("Customer email updated successFully "+customerEmail);
								showDialogMessage("Customer email updated successFully");
							}
							
							@Override
							public void onFailure(Throwable caught) {
								// TODO Auto-generated method stub
								showDialogMessage("Data Saved UnsuccessFully");

							}
						});	
						mailpopup.hidePopUp();
					
					
				}
				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				showDialogMessage("Unexpected Error Ouccured");
				hideWaitSymbol();

			}
		});
		
		
		
	
		
	}
	/****************************************Validation Method(Overridden)**********************************/
	/**
	 * Validates form fields
	 */
	
	@Override
	 public boolean validate()
	 {
		  boolean superValidate = super.validate();
		  if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "EmailMandatoryWhileCreatingContract")){
			  Console.log("customerEmail 111--"+customerEmail);
				if(customerEmail.equals("")){
					Console.log("customerEmail inside if "+customerEmail);
					mailpopup.showPopUp();
					return false;
				}
			  }
		  
		  
		if(saleslineitemtable.getDataprovider().getList().size() == 0)
        {
            showDialogMessage("Please Select at least one Product!");
            return false;
        }
        
               
  //***********rohan remove this as per ultra pest control requirement *************       
        
//        if(validateContractPrice()>0){
//        	showDialogMessage("Please enter product price greater than 0!");
//        	return false;
//        }
        
        if(!validateContractProdQty()){
			showDialogMessage("Please enter appropriate quantity required!");
			return false;
		}
		
        if(validateServicePopupBranch()==false){
        	this.showDialogMessage("Please Select Branch in Schedule Popup");
        	return false;
        }
        
        /* developed by vijay
         * Release 31 august 2016
         * this validation is developed for when invoice date is less than contract Date then bellow message will show and stop saving
         * and if invoice date is blank then this validation message will not show
         */
        if(validateinvoiceDate(dbContractDate.getValue(),dbinvoiceDate.getValue())==false){
        	this.showDialogMessage("Invoice should be greater or equal contract Date");
        	return false;
        }
      
        if(superValidate==false){
        	 return false;
        }
        
        /**
      	 * Date 03-07-2017 added by vijay tax validation
      	 */
      		List<ProductOtherCharges> prodtaxlist = this.prodTaxTable.getDataprovider().getList();
      		for(int i=0;i<prodtaxlist.size();i++){
      			
      			if(dbContractDate.getValue()!=null && dbContractDate.getValue().before(date)){
      				if(prodtaxlist.get(i).getChargeName().equalsIgnoreCase("CGST") || prodtaxlist.get(i).getChargeName().equalsIgnoreCase("SGST")
      						|| prodtaxlist.get(i).getChargeName().equalsIgnoreCase("IGST")){
      					showDialogMessage("As Contract date "+fmt.format(dbContractDate.getValue())+" GST Tax is not applicable");
      					return false;
      				}
      			}
      			else if(dbContractDate.getValue()!=null && dbContractDate.getValue().equals(date) || dbContractDate.getValue().after(date)){
      				if(prodtaxlist.get(i).getChargeName().equalsIgnoreCase("CST") || prodtaxlist.get(i).getChargeName().equalsIgnoreCase("Service Tax")
      						|| prodtaxlist.get(i).getChargeName().equalsIgnoreCase("VAT")){
      					showDialogMessage("As Contract date "+fmt.format(dbContractDate.getValue())+" VAT/CST/Service Tax is not applicable");
      					return false;
      				}
      			}
      		}
      		/**
      		 * ends here
      		 */
       
        return true;
	 }
	
	private boolean validateinvoiceDate(Date contractDate, Date invoiceDate) {
		
		if(invoiceDate!=null){
			if(invoiceDate.before(contractDate)){
				return false;
			}
		}
		return true;
	}
	
	/**********************************Change Widgets Method******************************************/
	/**
	 * This method represents changing the widgets style. Used for modifying payment composite
	 * widget fields
	 */
	
	private void changeWidgets()
	{
		//  For changing the text alignments.
		this.changeTextAlignment(dototalamt);
		this.changeTextAlignment(donetpayamt);
		this.changeTextAlignment(doamtincltax);
		
		//  rohan added this for making balance payment and Amt received 
//		this.changeTextAlignment(dbpaymentrecieved);
//		this.changeTextAlignment(dbbalancepayment);
		
		//  For changing widget widths.
		this.changeWidth(dototalamt, 200, Unit.PX);
		this.changeWidth(donetpayamt, 200, Unit.PX);
		this.changeWidth(doamtincltax, 200, Unit.PX);
		
		// rohan added this for making balance payment and Amt received 
//		this.changeWidth(dbpaymentrecieved, 200, Unit.PX);
//		this.changeWidth(dbbalancepayment, 200, Unit.PX);
	}
	
	/*******************************Text Alignment Method******************************************/
	/**
	 * Method used for align of payment composite fields
	 * @param widg
	 */
	
	private void changeTextAlignment(Widget widg)
	{
		widg.getElement().getStyle().setTextAlign(TextAlign.CENTER);
	}
	
	/*****************************Change Width Method**************************************/
	/**
	 * Method used for changing width of fields
	 * @param widg
	 * @param size
	 * @param unit
	 */
	
	private void changeWidth(Widget widg,double size,Unit unit)
	{
		widg.getElement().getStyle().setWidth(size, unit);
	}
	
	
	public void checkCustomerStatus(int cCount)
	{
		MyQuerry querry=new MyQuerry();
		Company c=new Company();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		
		temp=new Filter();
		temp.setQuerryString("companyId");
		temp.setLongValue(c.getCompanyId());
		filtervec.add(temp);
		
		temp=new Filter();
		temp.setQuerryString("count");
		temp.setIntValue(cCount);
		filtervec.add(temp);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Customer());
		
		genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onFailure(Throwable caught) {
				showDialogMessage("An Unexpected error occurred!");
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
					for(SuperModel model:result)
					{
						Customer custEntity = (Customer)model;
						if(custEntity.getNewCustomerFlag()==true){
							showDialogMessage("Please fill customer information by editing customer");
							personInfoComposite.clear();
						}
						else if(AppConstants.ACTIVE.equals(custEntity.getStatus().trim())&&(custEntity.getNewCustomerFlag()==false)){
							 getPersonInfoComposite().getId().getElement().addClassName("personactive");
				    		 getPersonInfoComposite().getName().getElement().addClassName("personactive");
				    		 getPersonInfoComposite().getPhone().getElement().addClassName("personactive");
				    		 getPersonInfoComposite().getTbpocName().getElement().addClassName("personactive");
						}
						customerEmail=custEntity.getEmail();
					}
				}
			 });
	}
	
	/*************************************Service Scheduling************************************/

	private List<ServiceSchedule> reactfordefaultTable(SalesLineItem lis) {
		
		
		List<SalesLineItem> salesitemlis=new ArrayList<SalesLineItem>();
		salesitemlis.add(lis);
//		System.out.println("@@@@@@@@ PRODUCT LIST SIZE ::: "+salesitemlis.size());
		ArrayList<ServiceSchedule> serschelist=new ArrayList<ServiceSchedule>();
		serschelist.clear();
		boolean unitflag = false;
		for(int i=0;i<salesitemlis.size();i++){
			
			System.out.println("lis size"+salesitemlis.get(i).getBranchSchedulingInfo()+"-"+salesitemlis.get(i).getBranchSchedulingInfo1()
					+"-"+salesitemlis.get(i).getBranchSchedulingInfo2()+"-"+salesitemlis.get(i).getBranchSchedulingInfo3());
			/**
			 * 
			 * Date : 29-03-2017 By ANil
			 */
			boolean branchSchFlag=false;
			int qty=0;
			
//			if(salesitemlis.get(i).getBranchSchedulingList()!=null
//					&&salesitemlis.get(i).getBranchSchedulingList().size()!=0){
//				branchSchFlag=true;
//				for(BranchWiseScheduling branch:salesitemlis.get(i).getBranchSchedulingList()){
//					if(branch.isCheck()==true){
//						qty++;
//					}
//				}
//			}else{
//				qty=(int) salesitemlis.get(i).getQty();
//			}
			
			
			/**
			 * Date 02-05-2018 
			 * Developer : Vijay
			 * Des :- Below code is customer branches stored in string so it can not load more than 120 branches
			 * so i have updated below code using hashmap. below old code commneted and updated code below
			 */
			
//			/**
//			 * Date : 03-04-2017 By ANIL
//			 */
//			
////			ArrayList<BranchWiseScheduling> branchSchedulingList=AppUtility.getBranchSchedulingList(salesitemlis.get(i).getBranchSchedulingInfo());
//			/**
//			 * Date : 12-06-2017 BY ANIL
//			 */
//			String[] branchArray=new String[10];
//			if(salesitemlis.get(i).getBranchSchedulingInfo()!=null
//					&&!salesitemlis.get(i).getBranchSchedulingInfo().equals("")){
//				branchArray[0]=salesitemlis.get(i).getBranchSchedulingInfo();
//			}
//			if(salesitemlis.get(i).getBranchSchedulingInfo1()!=null
//					&&!salesitemlis.get(i).getBranchSchedulingInfo1().equals("")){
//				branchArray[1]=salesitemlis.get(i).getBranchSchedulingInfo1();
//			}
//			if(salesitemlis.get(i).getBranchSchedulingInfo2()!=null
//					&&!salesitemlis.get(i).getBranchSchedulingInfo2().equals("")){
//				branchArray[2]=salesitemlis.get(i).getBranchSchedulingInfo2();
//			}
//			if(salesitemlis.get(i).getBranchSchedulingInfo3()!=null
//					&&!salesitemlis.get(i).getBranchSchedulingInfo3().equals("")){
//				branchArray[3]=salesitemlis.get(i).getBranchSchedulingInfo3();
//			}
//			if(salesitemlis.get(i).getBranchSchedulingInfo4()!=null
//					&&!salesitemlis.get(i).getBranchSchedulingInfo4().equals("")){
//				branchArray[4]=salesitemlis.get(i).getBranchSchedulingInfo4();
//			}
//			
//			if(salesitemlis.get(i).getBranchSchedulingInfo5()!=null
//					&&!salesitemlis.get(i).getBranchSchedulingInfo5().equals("")){
//				branchArray[5]=salesitemlis.get(i).getBranchSchedulingInfo5();
//			}
//			if(salesitemlis.get(i).getBranchSchedulingInfo6()!=null
//					&&!salesitemlis.get(i).getBranchSchedulingInfo6().equals("")){
//				branchArray[6]=salesitemlis.get(i).getBranchSchedulingInfo6();
//			}
//			if(salesitemlis.get(i).getBranchSchedulingInfo7()!=null
//					&&!salesitemlis.get(i).getBranchSchedulingInfo7().equals("")){
//				branchArray[7]=salesitemlis.get(i).getBranchSchedulingInfo7();
//			}
//			if(salesitemlis.get(i).getBranchSchedulingInfo8()!=null
//					&&!salesitemlis.get(i).getBranchSchedulingInfo8().equals("")){
//				branchArray[8]=salesitemlis.get(i).getBranchSchedulingInfo8();
//			}
//			if(salesitemlis.get(i).getBranchSchedulingInfo9()!=null
//					&&!salesitemlis.get(i).getBranchSchedulingInfo9().equals("")){
//				branchArray[9]=salesitemlis.get(i).getBranchSchedulingInfo9();
//			}
//			
//			/**
//			 * End
//			 */
//			
//			ArrayList<BranchWiseScheduling> branchSchedulingList=AppUtility.getBranchSchedulingList(branchArray);
			
			/** Date 02-05-2018 By vijay for updated branch scheduling using hashmap **/
			ArrayList<BranchWiseScheduling> branchSchedulingList = salesitemlis.get(i).getCustomerBranchSchedulingInfo().get(salesitemlis.get(i).getProductSrNo());
			
			if(branchSchedulingList!=null&&branchSchedulingList.size()!=0){
				branchSchFlag=true;
				qty=branchSchedulingList.size();
//				for(BranchWiseScheduling branch:branchSchedulingList){
//					if(branch.isCheck()==true){
//						qty++;
//					}
//				}
			}else{
				qty=(int) salesitemlis.get(i).getQty();
			}
			
			/**
			 * End
			 */
			
			if(branchSchFlag==true){
				for(int k=0;k < qty;k++){
					
				 	if(branchSchedulingList.get(k).isCheck()==true){
					long noServices=(long) (salesitemlis.get(i).getNumberOfServices());
					int noOfdays=salesitemlis.get(i).getDuration();
					int interval= (int) (noOfdays/salesitemlis.get(i).getNumberOfServices());
//					Date servicedate=new Date();
					
					/**
					 * Date 16-06-2018 By vijay
					 * Des :- backdated contract, service schedule Date service date issue becuase start date set as contract date
					 */
					Date servicedate=null;

					if(dbContractDate.getValue()!=null){
						servicedate = dbContractDate.getValue();
					}else{
						servicedate = new Date();
					}
					/**
					 * ends here
					 */
					
					Date d=new Date(servicedate.getTime());
					Date productEndDate= new Date(servicedate.getTime());
					CalendarUtil.addDaysToDate(productEndDate, noOfdays);
					Date prodenddate=new Date(productEndDate.getTime());
					productEndDate=prodenddate;
					Date tempdate=new Date();
					
					for(int j=0;j<noServices;j++){
						if(j>0)
						{
							CalendarUtil.addDaysToDate(d, interval);
							tempdate=new Date(d.getTime());
						}
						
						ServiceSchedule ssch=new ServiceSchedule();
//						Date Stardaate=new Date();
						 /** Date 16-06-2018 By vijay above old line commented **/
						Date Stardaate=servicedate;

						
						//  rohan added this 1 field 
//						System.out.println("In side method RRRRRRRRRRRRR"+salesitemlis.get(i).getProductSrNo());
						ssch.setSerSrNo(salesitemlis.get(i).getProductSrNo());
						
						ssch.setScheduleStartDate(Stardaate);
						ssch.setScheduleProdId(salesitemlis.get(i).getPrduct().getCount());
						ssch.setScheduleProdName(salesitemlis.get(i).getProductName());
						ssch.setScheduleDuration(salesitemlis.get(i).getDuration());
						ssch.setScheduleNoOfServices(salesitemlis.get(i).getNumberOfServices());
						ssch.setScheduleServiceNo(j+1);
						ssch.setScheduleProdStartDate(Stardaate);
						ssch.setScheduleProdEndDate(productEndDate);
						ssch.setTotalServicingTime(salesitemlis.get(i).getServicingTIme());
						/**
						 *   rohan commented this code for NBHC 
						 *   Purpose : wanetd to set servicing branch as customer branch is selected  
						 */
//						ssch.setServicingBranch(olbbBranch.getValue());
						/**
						 * ends here 
						 */
						
//						System.out.println("SERVICING TIME ::: "+salesitemlis.get(i).getServicingTIme());
//						System.out.println("BRANCH ::: "+olbbBranch.getValue());
						ssch.setScheduleServiceTime("Flexible");
						
						if(j==0){
							ssch.setScheduleServiceDate(servicedate);
						}
						if(j!=0){
							if(productEndDate.before(d)==false){
								ssch.setScheduleServiceDate(tempdate);
							}else{
								ssch.setScheduleServiceDate(productEndDate);
							}
						}
					
						ssch.setScheduleProBranch(branchSchedulingList.get(k).getBranchName());
						if (branchSchedulingList.get(k).getServicingBranch()!=null) {
							ssch.setServicingBranch(branchSchedulingList.get(k).getServicingBranch());
						} 
						
						int dayIndex=AppUtility.getDayIndex(branchSchedulingList.get(k).getDay());
						
						if(dayIndex!=0){
							String dayOfWeek1 = format.format(d);
							int adate = Integer.parseInt(dayOfWeek1);
							int adday1 = dayIndex - 1;
							int adday = 0;
							if (adate <= adday1) {
								adday = (adday1 - adate);
							} else {
								adday = (7 - adate + adday1);
							}
							
							Date newDate = new Date(d.getTime());
							CalendarUtil.addDaysToDate(newDate, adday);
							Date day = new Date(newDate.getTime());

							if (productEndDate.before(day) == false) {
								ssch.setScheduleServiceDate(day);
							} else {
								ssch.setScheduleServiceDate(productEndDate);
							}

							CalendarUtil.addDaysToDate(d, interval);
							Date tempdate1 = new Date(d.getTime());
							d = tempdate1;
							ssch.setScheduleServiceDay(ContractForm.serviceDay(ssch.getScheduleServiceDate()));
						}
						
						/**
						 * Date 01-04-2017 added by vijay for week number
						 */
						int weaknumber = AppUtility.getweeknumber(ssch.getScheduleServiceDate());
						ssch.setWeekNo(weaknumber);
						
						/**
						 * ends here
						 */
						
					ssch.setScheduleServiceDay(serviceDay(ssch.getScheduleServiceDate()));
					
					 /***
					  *  *:*:*
					  * nidhi
					  * 18-09-2018
					  * for map bill product
					  * if any update please update in else conditions also
					  */
					 if(LoginPresenter.billofMaterialActive){
						 
						 BillOfMaterial billPrDt = AppUtility.verifyBillofMaterilProd(lis.getPrduct());
						 if(billPrDt!=null){
							 
							 
							 UnitConversionUtility unitConver = new UnitConversionUtility();

							 
							 if(unitConver.varifyUnitConversion(billPrDt, lis.getAreaUnit())){
								 
								 List<ServiceSchedule> sev = new ArrayList<ServiceSchedule>();
								 sev.add(ssch);
								 sev = unitConver.getServiceitemProList(sev,lis.getPrduct(),lis,billPrDt);
								 
								 if(sev!=null && sev.size()>0){
									 ssch = sev.get(0).copyOfObject();
								 }
								 
								 
							 }else{
								 unitflag = true;
//								 showDialogMessage("Unit of service area and product group area is differeant and conversion of unit is not avaliable.");
							 }
							 
							 
							 
							 
						 }
					 }
					 /**
					  * end
					  */
					serschelist.add(ssch);
					}
					
				}
				}
			}else{
			
			for(int k=0;k < qty;k++){
			
			 	
			long noServices=(long) (salesitemlis.get(i).getNumberOfServices());
			int noOfdays=salesitemlis.get(i).getDuration();
			int interval= (int) (noOfdays/salesitemlis.get(i).getNumberOfServices());
			Date servicedate=new Date();
			
			/**
			 * Date 16-06-2018 By vijay
			 * Des :- backdated contract, service schedule Date service date issue becuase start date set as contract date
			 */
//			Date servicedate=null;

			if(dbContractDate.getValue()!=null){
				servicedate = dbContractDate.getValue();
			}else{
				servicedate = new Date();
			}
			/**
			 * ends here
			 */
			
			Date d=new Date(servicedate.getTime());
			Date productEndDate= new Date(servicedate.getTime());
			CalendarUtil.addDaysToDate(productEndDate, noOfdays);
			Date prodenddate=new Date(productEndDate.getTime());
			productEndDate=prodenddate;
			Date tempdate=new Date();
			
			for(int j=0;j<noServices;j++){
				if(j>0)
				{
					CalendarUtil.addDaysToDate(d, interval);
					tempdate=new Date(d.getTime());
				}
				
				ServiceSchedule ssch=new ServiceSchedule();
				/**
				 * Date 16-06-2018 By vijay
				 */
//				Date Stardaate=new Date();
				Date Stardaate=servicedate;
				/**
				 * ends here
				 */
				
				//  rohan added this 1 field 
//				System.out.println("In side method RRRRRRRRRRRRR"+salesitemlis.get(i).getProductSrNo());
				ssch.setSerSrNo(salesitemlis.get(i).getProductSrNo());
				
				ssch.setScheduleStartDate(Stardaate);
				ssch.setScheduleProdId(salesitemlis.get(i).getPrduct().getCount());
				ssch.setScheduleProdName(salesitemlis.get(i).getProductName());
				ssch.setScheduleDuration(salesitemlis.get(i).getDuration());
				ssch.setScheduleNoOfServices(salesitemlis.get(i).getNumberOfServices());
				ssch.setScheduleServiceNo(j+1);
				ssch.setScheduleProdStartDate(Stardaate);
				ssch.setScheduleProdEndDate(productEndDate);
				ssch.setTotalServicingTime(salesitemlis.get(i).getServicingTIme());
				/**
				 *   rohan commented this code for NBHC 
				 *   Purpose : wanetd to set servicing branch as customer branch is selected  
				 */
//				ssch.setServicingBranch(olbbBranch.getValue());
				/**
				 * ends here 
				 */
				
//				System.out.println("SERVICING TIME ::: "+salesitemlis.get(i).getServicingTIme());
//				System.out.println("BRANCH ::: "+olbbBranch.getValue());
				ssch.setScheduleServiceTime("Flexible");
				
				if(j==0){
					ssch.setScheduleServiceDate(servicedate);
				}
				if(j!=0){
					if(productEndDate.before(d)==false){
						ssch.setScheduleServiceDate(tempdate);
					}else{
						ssch.setScheduleServiceDate(productEndDate);
					}
				}
				
				if(customerbranchlist.size()==qty){
					ssch.setScheduleProBranch(customerbranchlist.get(k).getBusinessUnitName());
					/**
					 * rohan added this code for automatic servicing branch setting for NBHC 
					 */
					
					if(customerbranchlist.get(k).getBranch()!=null)
					{
						ssch.setServicingBranch(customerbranchlist.get(k).getBranch());
					}
					else
					{
						ssch.setServicingBranch(olbbBranch.getValue());
					}
					/**
					 * ends here 
					 */
				}else if(customerbranchlist.size()==0){
					ssch.setScheduleProBranch("Service Address");
					/**
					 * rohan added this code for automatic servicing branch setting for NBHC 
					 */
						ssch.setServicingBranch(olbbBranch.getValue());
					/**
					 * ends here 
					 */	
						
				}else{
					ssch.setScheduleProBranch(null);
				}
				
				ssch.setScheduleServiceDay(serviceDay(ssch.getScheduleServiceDate()));
				
				/**
				 * Date 01-04-2017 added by vijay for week number
				 */
				int weaknumber = AppUtility.getweeknumber(ssch.getScheduleServiceDate());
				ssch.setWeekNo(weaknumber);
				
				/**
				 * ends here
				 */
				 /***
				  *  *:*:*
				  * nidhi
				  * 18-09-2018
				  * for map bill product
				  * if any update please update in else conditions also
				  */
				 if(LoginPresenter.billofMaterialActive){
					 
					 BillOfMaterial billPrDt = AppUtility.verifyBillofMaterilProd(lis.getPrduct());
					 if(billPrDt!=null){
						 
						 
						 UnitConversionUtility unitConver = new UnitConversionUtility();

						 
						 if(unitConver.varifyUnitConversion(billPrDt, lis.getAreaUnit())){
							 
							 List<ServiceSchedule> sev = new ArrayList<ServiceSchedule>();
							 sev.add(ssch);
							 sev = unitConver.getServiceitemProList(sev,lis.getPrduct(),lis,billPrDt);
							 
							 if(sev!=null && sev.size()>0){
								 ssch = sev.get(0).copyOfObject();
							 }
							 
							 
						 }else{
							 /**
							  * nidhi 7-1-2018 |*|
							  */
							 unitflag = true;
							 /**
							  * end
							  */
//							 showDialogMessage("Unit of service area and product group area is differeant and conversion of unit is not avaliable.");
						 }
						 
						 
						 
						 
					 }
				 }
				 /**
				  * end
				  */
				serschelist.add(ssch);
			}
		}
			
		}
			
			
		}
		
		
		//  rohan added this for testing 
		
//		for (int l = 0; l < serschelist.size(); l++)
//		{
//			System.out.println("My Rohan in service schedule table "+serschelist.get(l).getSerSrNo());
//		}
		/**
		 * nidhi |*|
		 */
		if(unitflag){
			showDialogMessage("Unit of service are and product group detail area not mapped at branch level. Or conversion not available");
		}
		
		return serschelist;
	}


	public void checkCustomerBranch(int customerId) {

		int custid=customerId;
		MyQuerry querry=new MyQuerry();
		Company c=new Company();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		
		temp=new Filter();
		temp.setQuerryString("companyId");
		temp.setLongValue(c.getCompanyId());
		filtervec.add(temp);
		
		temp=new Filter();
		temp.setQuerryString("cinfo.count");
		temp.setIntValue(custid);
		filtervec.add(temp);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new CustomerBranchDetails());
		genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onFailure(Throwable caught) {
				showDialogMessage("An Unexpected error occurred!");
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				customerbranchlist.clear();
				
				if(result.size()>0)
				{
					for(SuperModel model:result)
					{
						CustomerBranchDetails custbranchEntity = (CustomerBranchDetails)model;
						CustomerBranchDetails bbb=new CustomerBranchDetails(); 
				    	bbb.setBusinessUnitName(custbranchEntity.getBusinessUnitName());
						bbb.setCinfo(custbranchEntity.getCinfo());
						customerbranchlist.add(bbb);
						
						System.out.println("Customer Branch ==== &&& "+customerbranchlist.size());
					}
				}
				}
			 });
		}
	
	public static String serviceDay(Date d){
		String day=null;
		
		String dayOfWeek = format.format(d);
		if(dayOfWeek.equals("0"))
		   return day="Sunday";
		if(dayOfWeek.equals("1"))
			return day="Monday";
		if(dayOfWeek.equals("2"))
			return day="Tuesday";
		if(dayOfWeek.equals("3"))
			return day="Wednesday";
		if(dayOfWeek.equals("4"))
			return day="Thursday";
		if(dayOfWeek.equals("5"))
			return day="Friday";
		if(dayOfWeek.equals("6"))
			return day="Saturday";
		
		return day;
	}
	
	/************************************Getters And Setters*******************************************/
	public ObjectListBox<Employee> getOlbApproverName() {
		return olbApproverName;
	}

	public void setOlbApproverName(ObjectListBox<Employee> olbApproverName) {
		this.olbApproverName = olbApproverName;
	}

	public ObjectListBox<Config> getOlbContractGroup() {
		return olbContractGroup;
	}

	public void setOlbContractGroup(ObjectListBox<Config> olbContractGroup) {
		this.olbContractGroup = olbContractGroup;
	}

	public ObjectListBox<Type> getOlbContractType() {
		return olbContractType;
	}

	public void setOlbContractType(ObjectListBox<Type> olbContractType) {
		this.olbContractType = olbContractType;
	}

	public ObjectListBox<ConfigCategory> getOlbContractCategory() {
		return olbContractCategory;
	}

	public void setOlbContractCategory(
			ObjectListBox<ConfigCategory> olbContractCategory) {
		this.olbContractCategory = olbContractCategory;
	}
	
	public ProductChargesTable getChargesTable() {
		return chargesTable;
	}

	public void setChargesTable(ProductChargesTable chargesTable) {
		this.chargesTable = chargesTable;
	}

	public ProductTaxesTable getProdTaxTable() {
		return prodTaxTable;
	}

	public void setProdTaxTable(ProductTaxesTable prodTaxTable) {
		this.prodTaxTable = prodTaxTable;
	}

	public SalesLineItemTable getSaleslineitemtable() {
		return saleslineitemtable;
	}
	public void setSaleslineitemtable(SalesLineItemTable saleslineitemtable) {
		this.saleslineitemtable = saleslineitemtable;
	}
	public ObjectListBox<Branch> getOlbbBranch() {
		return olbbBranch;
	}

	public void setOlbbBranch(ObjectListBox<Branch> olbbBranch) {
		this.olbbBranch = olbbBranch;
	}


	public ObjectListBox<Config> getOlbcPaymentMethods() {
		return olbcPaymentMethods;
	}

	public void setOlbcPaymentMethods(ObjectListBox<Config> olbcPaymentMethods) {
		this.olbcPaymentMethods = olbcPaymentMethods;
	}

	public TextBox getTbContractId() {
		return tbContractId;
	}

	public void setTbContractId(TextBox tbContractId) {
		this.tbContractId = tbContractId;
	}

	public PersonInfoComposite getPersonInfoComposite() {
		return personInfoComposite;
	}

	public void setPersonInfoComposite(PersonInfoComposite personInfoComposite) {
		this.personInfoComposite = personInfoComposite;
	}

	public ObjectListBox<Employee> getOlbeSalesPerson() {
		return olbeSalesPerson;
	}

	public void setOlbeSalesPerson(ObjectListBox<Employee> olbeSalesPerson) {
		this.olbeSalesPerson = olbeSalesPerson;
	}

	public DoubleBox getDototalamt() {
		return dototalamt;
	}

	public void setDototalamt(DoubleBox dototalamt) {
		this.dototalamt = dototalamt;
	}

	public DoubleBox getDoamtincltax() {
		return doamtincltax;
	}

	public void setDoamtincltax(DoubleBox doamtincltax) {
		this.doamtincltax = doamtincltax;
	}

	public DoubleBox getDonetpayamt() {
		return donetpayamt;
	}

	public void setDonetpayamt(DoubleBox donetpayamt) {
		this.donetpayamt = donetpayamt;
	}

	public IntegerBox getIbCreditPeriod() {
		return ibCreditPeriod;
	}

	public void setIbCreditPeriod(IntegerBox ibCreditPeriod) {
		this.ibCreditPeriod = ibCreditPeriod;
	}

	public Button getAddOtherCharges() {
		return addOtherCharges;
	}

	public void setAddOtherCharges(Button addOtherCharges) {
		this.addOtherCharges = addOtherCharges;
	}


	public Button getF_btnservice() {
		return f_btnservice;
	}

	public void setF_btnservice(Button f_btnservice) {
		this.f_btnservice = f_btnservice;
	}


	public ServiceScheduleTable getServiceScheduleTable() {
		return serviceScheduleTable;
	}

	public void setServiceScheduleTable(ServiceScheduleTable serviceScheduleTable) {
		this.serviceScheduleTable = serviceScheduleTable;
	}

	public ServiceSchedule getScheduleEntity() {
		return scheduleEntity;
	}

	public void setScheduleEntity(ServiceSchedule scheduleEntity) {
		this.scheduleEntity = scheduleEntity;
	}

	public ServiceSchedulePopUp getServicepop() {
		return servicepop;
	}

	public void setServicepop(ServiceSchedulePopUp servicepop) {
		this.servicepop = servicepop;
	}

	
	public TextBox getTbStatus() {
		return tbStatus;
	}

	public void setTbStatus(TextBox tbStatus) {
		this.tbStatus = tbStatus;
	}

	
	
	
	public DoubleBox getDbpaymentrecieved() {
		return dbpaymentrecieved;
	}

	public void setDbpaymentrecieved(DoubleBox dbpaymentrecieved) {
		this.dbpaymentrecieved = dbpaymentrecieved;
	}

	public DoubleBox getDbbalancepayment() {
		return dbbalancepayment;
	}

	public void setDbbalancepayment(DoubleBox dbbalancepayment) {
		this.dbbalancepayment = dbbalancepayment;
	}

	
	
	
	
	public TextBox getTbFullName() {
		return tbFullName;
	}

	public void setTbFullName(TextBox tbFullName) {
		this.tbFullName = tbFullName;
	}
	
	

	public TextBox getTbQuotatinId() {
		return tbQuotatinId;
	}

	public void setTbQuotatinId(TextBox tbQuotatinId) {
		this.tbQuotatinId = tbQuotatinId;
	}

	public TextBox getTbCompanyName() {
		return tbCompanyName;
	}

	public void setTbCompanyName(TextBox tbCompanyName) {
		this.tbCompanyName = tbCompanyName;
	}

	@Override
	public void onChange(ChangeEvent event) {
		

		String payMethodVal=null;
		if(this.olbcPaymentMethods.getSelectedIndex()!=0)
		{
			payMethodVal=olbcPaymentMethods.getValue(olbcPaymentMethods.getSelectedIndex()).trim();
			if(payMethodVal.equalsIgnoreCase("Cash"))
			{
				clearFields();
				this.tbbankAccNo.setEnabled(false);
				this.tbbankName.setEnabled(false);
				this.tbbankBranch.setEnabled(false);
				this.tbchequeNo.setEnabled(false);
				this.dbChequeDate.setEnabled(false);
				this.tbreferenceno.setEnabled(false);
				this.dbtransferDate.setEnabled(false);
				// Date 02-04-2018 By vijay
				tbChequeIssuedBy.setEnabled(false);
			}
			else if(payMethodVal.equalsIgnoreCase("Cheque"))
			{
				clearFields();
				this.tbbankAccNo.setEnabled(true);
				this.tbbankName.setEnabled(true);
				this.tbbankBranch.setEnabled(true);
				this.tbchequeNo.setEnabled(true);
				this.dbChequeDate.setEnabled(true);
				this.tbreferenceno.setEnabled(false);
				this.dbtransferDate.setEnabled(false);
				// Date 02-04-2018 By vijay
				tbChequeIssuedBy.setEnabled(true);
			}
			else if(payMethodVal.equalsIgnoreCase("ECS"))
			{
				clearFields();
				this.tbreferenceno.setEnabled(true);
				this.dbtransferDate.setEnabled(true);
				this.tbbankAccNo.setEnabled(true);
				this.tbbankName.setEnabled(true);
				this.tbbankBranch.setEnabled(true);
				this.tbchequeNo.setEnabled(false);
				this.dbChequeDate.setEnabled(false);
				// Date 02-04-2018 By vijay
				tbChequeIssuedBy.setEnabled(true);
			}
			else if(payMethodVal.equalsIgnoreCase("NEFT"))
			{
				clearFields();
				this.tbbankAccNo.setEnabled(false);
				this.tbbankName.setEnabled(false);
				this.tbbankBranch.setEnabled(false);
				this.tbchequeNo.setEnabled(false);
				this.dbChequeDate.setEnabled(false);
				this.tbreferenceno.setEnabled(true);
				this.dbtransferDate.setEnabled(true);
				// Date 02-04-2018 By vijay
				tbChequeIssuedBy.setEnabled(false);
			}
		}		
		else
		{
			clearFields();
			this.tbbankAccNo.setEnabled(false);
			this.tbbankName.setEnabled(false);
			this.tbbankBranch.setEnabled(false);
			this.tbchequeNo.setEnabled(false);
			this.dbChequeDate.setEnabled(false);
			this.tbreferenceno.setEnabled(false);
			this.dbtransferDate.setEnabled(false);
			// Date 02-04-2018 By vijay
			tbChequeIssuedBy.setEnabled(false);
		}
		
		
		if(event.getSource().equals(olbContractCategory))
		{
			if(olbContractCategory.getSelectedIndex()!=0){
				ConfigCategory cat=olbContractCategory.getSelectedItem();
				if(cat!=null){
					AppUtility.makeLiveTypeDropDown(olbContractType, cat.getCategoryName(), cat.getCategoryCode(), cat.getInternalType());
				}
			}
		}
		
		if(event.getSource().equals(olbcNumberRange)){
			updateTaxesDetails(customerEntity);
		}
		if(event.getSource().equals(olbbBranch)){
			if(olbbBranch.getSelectedIndex()!=0) {
				/*
				 * Ashwini Patil 
				 * Date:13-05-2024 
				 * Added for ultima search. 
				 * If number range is selected on branch then it will automatically get selected on contract screen.
				 */
				
				Branch branchEntity =olbbBranch.getSelectedItem();
				if(branchEntity!=null) {
					Console.log("branch entity not null");
					Console.log("branch "+branchEntity.getBusinessUnitName()+"number range="+branchEntity.getNumberRange());
					if(branchEntity.getNumberRange()!=null&&!branchEntity.getNumberRange().equals("")) {
						olbcNumberRange.setValue(branchEntity.getNumberRange());
						Console.log("Number range set to drop down by default");
					}else {
						Console.log("in else Number range");
						String range=LoginPresenter.branchWiseNumberRangeMap.get(branchEntity.getBusinessUnitName());
						if(range!=null&&!range.equals("")) {
							olbcNumberRange.setValue(range);
							Console.log("in else Number range set to drop down by default");							
						}else {
							olbcNumberRange.setSelectedIndex(0);
						}
					}
				}else
					Console.log("branch entity null");
				updateTaxesDetails(customerEntity);
			}
		}
	}
	
	private void clearFields()
	{
		this.tbbankAccNo.setValue("");
		this.tbbankName.setValue("");
		this.tbbankBranch.setValue("");
		this.tbchequeNo.setValue(null);
		this.dbChequeDate.setValue(null);
		this.dbtransferDate.setValue(null);
		this.tbreferenceno.setValue("");
	}

	public void getCustomerAddress(int idValue) {
		// TODO Auto-generated method stub
		

		MyQuerry querry=new MyQuerry();
		Company c=new Company();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		
		temp=new Filter();
		temp.setQuerryString("companyId");
		temp.setLongValue(c.getCompanyId());
		filtervec.add(temp);
		
		temp=new Filter();
		temp.setQuerryString("count");
		temp.setIntValue(idValue);
		filtervec.add(temp);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Customer());
		
		genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onFailure(Throwable caught) {
				showDialogMessage("An Unexpected error occurred!");
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
					for(SuperModel model:result)
					{
						Customer custEntity = (Customer)model;
						
						customerAddressComposite.setValue(custEntity.getAdress());
						
						customerAddressComposite.setEnable(false);
						tbCompanyName.setValue("");
						tbCompanyName.setEnabled(false);
						tbFullName.setValue("");
						tbFullName.setEnabled(false);
						pnbCellNo.setValue(0l);
						pnbCellNo.setEnabled(false);
						etbEmail.setValue("");
						etbEmail.setEnabled(false);
						
						//  rohan set value from customer
						olbbBranch.setValue(custEntity.getBranch());
						olbeSalesPerson.setValue(custEntity.getEmployee());
						olbApproverName.setValue(custEntity.getApproverName());
						
						/**
						 * Date : 04-08-2017 BY ANIL
						 */
						olbbCustomerBranch.setEnabled(false);
						/**
						 * End
						 */
						customerEntity = custEntity;
					}
					
					if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT || AppMemory.getAppMemory().currentState==ScreeenState.NEW){
						hideWaitSymbol();
						updateTaxesDetails(customerEntity);
					}
				}
			 });
	
	}

	
	/**
	 *  nidhi
	 *  29-01-2018
	 *  for multiple contact list details
	 */
	
	public void getCustomerContactList(int customerCount){
		try {

			MyQuerry querry=new MyQuerry();
			Company c=new Company();
			Vector<Filter> filtervec=new Vector<Filter>();
			Filter temp=null;
			
			temp=new Filter();
			temp.setQuerryString("companyId");
			temp.setLongValue(c.getCompanyId());
			filtervec.add(temp);
			
			temp=new Filter();
			temp.setQuerryString("personInfo.count");
			temp.setIntValue(customerCount);
			filtervec.add(temp);
			
			querry.setFilters(filtervec);
			querry.setQuerryObject(new ContactPersonIdentification());
			
			genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onFailure(Throwable caught) {
					showDialogMessage("An Unexpected error occurred!");
				}

				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
//					showDialogMessage("Check customer details --" + result.s);
					objContactList.removeAllItems();
					objContactList.addItem("--Select--");
						for(SuperModel model:result)
						{
							ContactPersonIdentification custEntity = (ContactPersonIdentification)model;

							objContactList.addItem(custEntity.getName());
						}
					}
				 });
		
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}
	
	public AddressComposite getCustomerAddressComposite() {
		return customerAddressComposite;
	}

	public void setCustomerAddressComposite(
			AddressComposite customerAddressComposite) {
		this.customerAddressComposite = customerAddressComposite;
	}

	public PhoneNumberBox getPnbCellNo() {
		return pnbCellNo;
	}

	public void setPnbCellNo(PhoneNumberBox pnbCellNo) {
		this.pnbCellNo = pnbCellNo;
	}

	public EmailTextBox getEtbEmail() {
		return etbEmail;
	}

	public void setEtbEmail(EmailTextBox etbEmail) {
		this.etbEmail = etbEmail;
	}

	public TextBox getTbLeadId() {
		return tbLeadId;
	}

	public void setTbLeadId(TextBox tbLeadId) {
		this.tbLeadId = tbLeadId;
	}

	public List<ServiceSchedule> getGlobalScheduleServiceList() {
		return globalScheduleServiceList;
	}

	public void setGlobalScheduleServiceList(
			List<ServiceSchedule> globalScheduleServiceList) {
		this.globalScheduleServiceList = globalScheduleServiceList;
	}
		public String getCustomerEmail() {
		return customerEmail;
	}

	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}
	
	public void findMaxServiceSrNumber() {
		int maxSrNumber = 0 ;
		System.out.println("rohan in side max service number");
		
		List<SalesLineItem> list = this.saleslineitemtable.getDataprovider().getList();
		if(list != null && list.size() > 0){
		System.out.println("mazza list size "+list.size());
		
		Comparator<SalesLineItem> compare = new  Comparator<SalesLineItem>() {
			
			@Override
			public int compare(SalesLineItem o1, SalesLineItem o2) {
				return Integer.compare(o2.getProductSrNo(), o1.getProductSrNo());
			}
		};
		Collections.sort(list,compare);
		
			System.out.println("product sr number "+list.get(0).getProductSrNo());
			maxSrNumber = list.get(0).getProductSrNo();
			productSrNo = maxSrNumber;
		}
	}

	public TextArea getTaDescription() {
		return taDescription;
	}

	public void setTaDescription(TextArea taDescription) {
		this.taDescription = taDescription;
	}

	/**
	 *  30-12-2020 Added by Priyanka issue raised by Nitin Sir
	 */

	@Override
	public void refreshTableData() {
		// TODO Auto-generated method stub
		super.refreshTableData();
		  
		saleslineitemtable.getTable().redraw();
		paymenttable.getTable().redraw();
		paymentTermsTable.getTable().redraw();
		chargesTable.getTable().redraw();
		prodTaxTable.getTable().redraw();
	}
	
	protected void updateTaxesDetails(Customer cust) {
		List<SalesLineItem> prodlist = saleslineitemtable.getDataprovider().getList();
		if(prodlist.size()>0){
			Branch branchEntity =olbbBranch.getSelectedItem();
			boolean gstApplicable = false;
			if(olbcNumberRange.getSelectedIndex()!=0){
				Config configEntity = olbcNumberRange.getSelectedItem();
				if(configEntity!=null)
				gstApplicable = configEntity.isGstApplicable();
			}
			else{
				gstApplicable = true;
			}
			if(cust!=null){
				System.out.println("customer onselection");
				 String customerBillingaddressState = cust.getAdress().getState();
				 List<SalesLineItem> productlist = AppUtility.updateTaxesDetails(prodlist, branchEntity, customerBillingaddressState,gstApplicable);
				 saleslineitemtable.getDataprovider().setList(productlist);
				 saleslineitemtable.getTable().redraw();
				RowCountChangeEvent.fire(saleslineitemtable.getTable(), saleslineitemtable.getDataprovider().getList().size(), true);
				setEnable(true);
				hideWaitSymbol();
			}
			
		}
	}

	public ObjectListBox<Config> getOlbcNumberRange() {
		return olbcNumberRange;
	}

	public void setOlbcNumberRange(ObjectListBox<Config> olbcNumberRange) {
		this.olbcNumberRange = olbcNumberRange;
	}

	
}
