package com.slicktechnologies.client.views.quickcontract;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.Service;

public class QuickContractServiceTable extends SuperTable<Service>{

	TextColumn<Service> getcolumnContractId;
	TextColumn<Service> getColumnServiceId;
	TextColumn<Service> getColumnCustomerName;
	TextColumn<Service> getcolumnServiceDate;
	TextColumn<Service> getColumnStatus;
	
	
	public QuickContractServiceTable(){
		createGui();
		setHeight("350px");
		
	}
	
	@Override
	public void createTable() {
		addColumnContractId();
		addColumnServiceId();
		addColumnCustomerName();
		addColumnServiceDate();
		addColumnStatus();
	}
	
	@Override
	public void addColumnSorting() {
		/****11-12-2019 Deepak Salve added this method ****/
		addColumnServiceIdSort();
	}

	private void addColumnContractId() {

		getcolumnContractId = new TextColumn<Service>() {
			
			@Override
			public String getValue(Service object) {
				return object.getContractCount()+"";
			}
		};
		table.addColumn(getcolumnContractId,"Contract Id");
		table.setColumnWidth(getcolumnContractId, 130,Unit.PX);
	}


	private void addColumnServiceId() {

		getColumnServiceId = new TextColumn<Service>() {
			
			@Override
			public String getValue(Service object) {
				return object.getCount()+"";
			}
		};
		table.addColumn(getColumnServiceId,"Service Id");
		table.setColumnWidth(getColumnServiceId, 130,Unit.PX);
		getColumnServiceId.setSortable(true);
	}


	private void addColumnCustomerName() {
		getColumnCustomerName = new TextColumn<Service>() {
			
			@Override
			public String getValue(Service object) {
				if(object.getPersonInfo()!=null)
					return object.getPersonInfo().getFullName();
				return "";
			}
		};
		table.addColumn(getColumnCustomerName,"Customer Name");
		table.setColumnWidth(getColumnCustomerName, 130,Unit.PX);
	}


	private void addColumnServiceDate() {

		getcolumnServiceDate = new TextColumn<Service>() {
			
			@Override
			public String getValue(Service object) {
				if(object.getServiceDate()!=null)
				return AppUtility.parseDate(object.getServiceDate());
				return "";
			}
		};
		
		table.addColumn(getcolumnServiceDate,"Service Date");
		table.setColumnWidth(getcolumnServiceDate, 110,Unit.PX);
	}


	private void addColumnStatus() {

		getColumnStatus = new TextColumn<Service>() {
			
			@Override
			public String getValue(Service object) {
				if(object.getStatus()!=null)
				return object.getStatus();
				return "";
			}
		};
		
		table.addColumn(getColumnStatus,"Status");
		table.setColumnWidth(getColumnStatus, 110,Unit.PX);
	}


	

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
//		addColumnServiceIdSort();
		
	}
	/****11-12-2019 Deepak Salve added this method ****/
	private void addColumnServiceIdSort() {
		// TODO Auto-generated method stub
		 
		 List<Service> list=getDataprovider().getList();
		 columnSort=new ListHandler<Service>(list);
		 System.out.println(" Sorted List Size "+list.size());
		 columnSort.setComparator(getColumnServiceId,new Comparator<Service>() {

			@Override
			public int compare(Service data1, Service data2) {
				// TODO Auto-generated method stub
				
				int serviceid1=data1.getCount();
				int serviceid2=data2.getCount();
				
				System.out.println(" Sorted List Value 1 "+serviceid1);
				System.out.println(" Sorted List Value 2 "+serviceid2);
				
				if(serviceid1== serviceid2){
				    	  return 0;
			    }
			   if(serviceid1> serviceid2){
					  return 1;
				}
				else{
					  return -1;
					 }
			}
		});
		
		 table.addColumnSortHandler(columnSort);
		 
//		  List<Service> list=getDataprovider().getList();
//		  System.out.println(" List Value "+list.size());
//		  columnSort=new ListHandler<Service>(list);
//		  columnSort.setComparator(getColumnServiceId, new Comparator<Service>()
//		  {
//		  @Override
//		  public int compare(Service e1,Service e2)
//		  {
//		  if(e1!=null && e2!=null)
//		  {
//		  if(e1.getCount()== e2.getCount()){
//		  return 0;}
//		  if(e1.getCount()> e2.getCount()){
//		  return 1;}
//		  else{
//		  return -1;}
//		  }
//		  else{
//		  return 0;}
//		  }
//		  });
//		  table.addColumnSortHandler(columnSort);
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
