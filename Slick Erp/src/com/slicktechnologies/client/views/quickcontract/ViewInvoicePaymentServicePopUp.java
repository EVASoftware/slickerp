package com.slicktechnologies.client.views.quickcontract;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCGlassPanel;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.TabPanel;
import com.google.gwt.view.client.NoSelectionModel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.client.views.quicksalesorder.QuickSalesOrderBillingTable;
import com.slicktechnologies.client.views.quicksalesorder.QuickSalesOrderInvoiceTable;
import com.slicktechnologies.client.views.quicksalesorder.QuickSalesOrderPaymentTable;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.twilio.sdk.resource.instance.monitor.Alert;

public class ViewInvoicePaymentServicePopUp extends AbsolutePanel implements ClickHandler{

	
	public static QuickSalesOrderBillingTable tabBillingTable;
	public static QuickSalesOrderInvoiceTable tabInvoiceTable;
	public static QuickSalesOrderPaymentTable tabPaymentTable;
	public static QuickContractServiceTable tabServiceTable;

	public long companyId;
	public int OrderId;
	public static boolean showAllFlag=true;
	
	final GenricServiceAsync genasync=GWT.create(GenricService.class);
	protected GWTCGlassPanel glassPanel;
	
	
	GeneralViewDocumentPopup genralInvoicePopup;
	public static PopupPanel generalPanel;
	
	FlowPanel btnClosePanel;
	public Image btnClose;
	public TabPanel tabPanel;
	boolean accessPayment=false;
	
	public ViewInvoicePaymentServicePopUp(boolean accessPay){
		genralInvoicePopup = new GeneralViewDocumentPopup();
		genralInvoicePopup.btnClose.addClickHandler(this);
		tabBillingTable = new QuickSalesOrderBillingTable();
		tabInvoiceTable = new QuickSalesOrderInvoiceTable();
		tabPaymentTable = new QuickSalesOrderPaymentTable();
		tabServiceTable = new QuickContractServiceTable();
		Console.log("in ViewInvoicePaymentServicePopUp accesspay="+accessPay);
		accessPayment=accessPay;
//		TabPanel tabPanel = new TabPanel();
		tabPanel = new TabPanel();
		
		btnClosePanel = new FlowPanel();
		
		btnClose = new Image("/images/closebtnred.png");
		btnClose.getElement().setClassName("topRight");
		btnClosePanel.add(btnClose);
		add(btnClosePanel);
		
		tabPanel.addSelectionHandler(new SelectionHandler<Integer>() {
			
			@Override
			public void onSelection(SelectionEvent<Integer> event) {
				int tabId = event.getSelectedItem();
				 System.out.println("TAB ID "+tabId);
				 
				 glassPanel = new GWTCGlassPanel();
				 
					
				 if(tabId==0){
					 tabBillingTable.getTable().redraw();
				 }
				 if(tabId==1){
					 tabInvoiceTable.getTable().redraw();
					 setInvoiceTableDate();
					 setTabelSelectionOnInvoiceTable();
				 }
				 else if(tabId==2){
					 if(accessPayment){
						 tabPaymentTable.getTable().redraw();
						 setPaymentTableData();
						 setTableSelectionOnPaymentTableData();
					 }else{
						 Window.alert("You are not authorized to view this screen!");
					 }
					 
				 }else if(tabId==3){
					 tabServiceTable.getTable().redraw();
					 setServiceTableData();
					 setTableSelectionOnServiceTable();

				 }
				
			}
		});
		
		
		
		setTableSelectionOnBillingTable();
		
		FlowPanel flowPanel1=new FlowPanel();
		FlowPanel flowPanel2=new FlowPanel();
		FlowPanel flowPanel3=new FlowPanel();
		FlowPanel flowPanel4=new FlowPanel();
		
		// create titles for tabs
		String tabTitlebilling = AppConstants.BILLINGDETAILS;
		String tabTitleInvoice = AppConstants.PROCESSCONFIGINV;
		String tabTitlePayment = AppConstants.PAYMENT;
		String tabTitleService = AppConstants.CUSTOMERSERVICE;
		
		flowPanel1.add(tabBillingTable.getTable());
		flowPanel2.add(tabInvoiceTable.getTable());
		flowPanel3.add(tabPaymentTable.getTable());
		flowPanel4.add(tabServiceTable.getTable());
		
		// create tabs
		tabPanel.add(flowPanel1, tabTitlebilling);
		tabPanel.add(flowPanel2, tabTitleInvoice);
		tabPanel.add(flowPanel3, tabTitlePayment);
		tabPanel.add(flowPanel4, tabTitleService);
		
		// select first tab
		tabPanel.selectTab(0);
		tabPanel.setSize("1107px", "430px");
		add(tabPanel, 10, 30);
		setSize("1127px", "450px");
		this.getElement().setId("form");
	}


	
	private void setTableSelectionOnBillingTable() {
		// TODO Auto-generated method stub
		final NoSelectionModel<BillingDocument> selectionModelInvoiceObj = new NoSelectionModel<BillingDocument>();
		SelectionChangeEvent.Handler tableselectionHandler= new SelectionChangeEvent.Handler() {
			
			@Override
			public void onSelectionChange(SelectionChangeEvent event) {
				// TODO Auto-generated method stub
				System.out.println("HELO Vijay Inside Billing Selection");
				BillingDocument billingEntity = selectionModelInvoiceObj.getLastSelectedObject();
				
//				AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Accounts/Billing Details",Screen.BILLINGDETAILS);
				AppMemory.getAppMemory().currentScreen = Screen.BILLINGDETAILS;
				genralInvoicePopup = new GeneralViewDocumentPopup();

				genralInvoicePopup.setModel(AppConstants.BILLINGDETAILS,billingEntity);
				generalPanel = new PopupPanel(false,true);//Ashwini Patil Date:24-03-2022 Description: If User clicks outside the popup system will not respond to that event. Popup needs to be closed first
				generalPanel.add(genralInvoicePopup);
				generalPanel.show();
				generalPanel.center();
				
			}
		};
		selectionModelInvoiceObj.addSelectionChangeHandler(tableselectionHandler);
		tabBillingTable.getTable().setSelectionModel(selectionModelInvoiceObj);
		
	}


	private void setTableSelectionOnServiceTable() {

		final NoSelectionModel<Service> selectionModelInvoiceObj = new NoSelectionModel<Service>();
		SelectionChangeEvent.Handler tableselectionHandler= new SelectionChangeEvent.Handler() {
			
			@Override
			public void onSelectionChange(SelectionChangeEvent event) {
				// TODO Auto-generated method stub
				System.out.println("HELO Vijay Inside Service Selection");
				Service serviceEntity = selectionModelInvoiceObj.getLastSelectedObject();
//			    AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Customer Service",Screen.SERVICE);
				AppMemory.getAppMemory().currentScreen = Screen.SERVICE;

				genralInvoicePopup = new GeneralViewDocumentPopup();

				genralInvoicePopup.setModel(AppConstants.CUSTOMERSERVICE,serviceEntity);
				generalPanel = new PopupPanel(false,true);//Ashwini Patil Date:24-03-2022 Description: If User clicks outside the popup system will not respond to that event. Popup needs to be closed first
				generalPanel.add(genralInvoicePopup);
				generalPanel.show();
				generalPanel.center();
			}
		};
		selectionModelInvoiceObj.addSelectionChangeHandler(tableselectionHandler);
		tabServiceTable.getTable().setSelectionModel(selectionModelInvoiceObj);
	}
	
	private void setTableSelectionOnPaymentTableData() {

		final NoSelectionModel<CustomerPayment> selectionModelInvoiceObj = new NoSelectionModel<CustomerPayment>();
		SelectionChangeEvent.Handler tableselectionHandler= new SelectionChangeEvent.Handler() {
			
			@Override
			public void onSelectionChange(SelectionChangeEvent event) {
				// TODO Auto-generated method stub
				System.out.println("HELO Vijay Inside PAyment Selection");
				CustomerPayment paymentEntity = selectionModelInvoiceObj.getLastSelectedObject();
//			    AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Accounts/Payment Details",Screen.PAYMENTDETAILS);
				AppMemory.getAppMemory().currentScreen = Screen.PAYMENTDETAILS;

				genralInvoicePopup = new GeneralViewDocumentPopup();

				genralInvoicePopup.setModel(AppConstants.PAYMENT,paymentEntity);
				generalPanel = new PopupPanel(false,true);//Ashwini Patil Date:24-03-2022 Description: If User clicks outside the popup system will not respond to that event. Popup needs to be closed first
				generalPanel.add(genralInvoicePopup);
				generalPanel.show();
				generalPanel.center();
			}
		};
		selectionModelInvoiceObj.addSelectionChangeHandler(tableselectionHandler);
		tabPaymentTable.getTable().setSelectionModel(selectionModelInvoiceObj);
	}
	

	public void setTabelSelectionOnInvoiceTable() {

		final NoSelectionModel<Invoice> selectionModelInvoiceObj = new NoSelectionModel<Invoice>();
		SelectionChangeEvent.Handler tableselectionHandler= new SelectionChangeEvent.Handler() {
			
			@Override
			public void onSelectionChange(SelectionChangeEvent event) {
				// TODO Auto-generated method stub
				System.out.println("HELO Vijay Inside Invoice Selection");
				Invoice invoiceEntity = selectionModelInvoiceObj.getLastSelectedObject();
//		        AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Accounts/Invoice Details",Screen.INVOICEDETAILS);
				AppMemory.getAppMemory().currentScreen = Screen.INVOICEDETAILS;

//				genralInvoicePopup = new GeneralViewDocumentPopup();

				genralInvoicePopup.setModel(AppConstants.PROCESSCONFIGINV,invoiceEntity);
				generalPanel = new PopupPanel(false,true);//Ashwini Patil Date:24-03-2022 Description: If User clicks outside the popup system will not respond to that event. Popup needs to be closed first
				generalPanel.add(genralInvoicePopup);
				generalPanel.show();
				generalPanel.center();
			}
		};
		selectionModelInvoiceObj.addSelectionChangeHandler(tableselectionHandler);
		tabInvoiceTable.getTable().setSelectionModel(selectionModelInvoiceObj);
		Console.log("end of setTabelSelectionOnInvoiceTable");
		if(genralInvoicePopup!=null){
			Console.log("click handler added to genralInvoicePopup");
			genralInvoicePopup.btnClose.addClickHandler(this);
		}
	}
	
	public QuickSalesOrderInvoiceTable getTabInvoiceTable() {
		return tabInvoiceTable;
	}


	public void setTabInvoiceTable(QuickSalesOrderInvoiceTable tabInvoiceTable) {
		this.tabInvoiceTable = tabInvoiceTable;
	}
	
	
	private void setServiceTableData() {
		
		glassPanel.show();
		
		
		MyQuerry querry = getServiceQuerry(this.companyId,this.OrderId);
		
		genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				System.out.println("SIZE sevice =="+result.size());
				ArrayList<Service> servicelist = new ArrayList<Service>();
				for(SuperModel model : result){
					Service service = (Service) model;
					servicelist.add(service);	
				}
				
				/**
				 Date 10-12-2019 Deepak Salve added this code for sort service id in order.
				 * */
				if(result.size()!=0){
					if(servicelist.size()!=0){
				Comparator<Service> ServiceSortID=new Comparator<Service>() {

					@Override
					public int compare(Service record1, Service record2) {
						// TODO Auto-generated method stub
						int serviceid1=record1.getCount();
						int serviceid2=record2.getCount();
						
//						 if(record1.getCount();!=null && serviceid2!=null)
//						  {
						  if(serviceid1== serviceid2){
						     return 0;
						  }
						  if(serviceid1> serviceid2){
						      return 1;
						  }
						  else{
						    return -1;
						  }
//						  }
//						  else{
//						  return 0;}
//						
					}
				};
				 Collections.sort(servicelist, ServiceSortID);
			  }
			}
				/**** Ends ****/
				
				tabServiceTable.getDataprovider().setList(servicelist);
				tabServiceTable.addColumnSorting(); // 11-12-2019 Deepak Salve added this line
				glassPanel.hide();
				/*** Ends **/
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				glassPanel.hide();
			}
		});
	}
	
	


	private void setPaymentTableData() {

		glassPanel.show();
		
		MyQuerry querry = getPaymentQuerry(this.companyId,this.OrderId);
		
		genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				ArrayList<CustomerPayment> paymentlist = new ArrayList<CustomerPayment>();
				
				for(SuperModel model : result){
					CustomerPayment payment = (CustomerPayment) model;
					paymentlist.add(payment);
				}
				
				if (result.size() != 0) {
					if (paymentlist.size() != 0) {
						Comparator<CustomerPayment> countsort = new Comparator<CustomerPayment>() {

							@Override
							public int compare(CustomerPayment r1, CustomerPayment r2) {
								int count1 = r1.getCount();
								int count2 = r2.getCount();
								if (count1 == count2) {
									return 0;
								}
								if (count1 > count2) {
									return 1;
								} else {
									return -1;
								}
							}
						};
						Collections.sort(paymentlist, countsort);
					}
				}
				
				tabPaymentTable.getDataprovider().setList(paymentlist);
				glassPanel.hide();
				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				glassPanel.hide();
			}
		});
	}

	
	public void setInvoiceTableDate() {
		// TODO Auto-generated method stub
		glassPanel.show();
		System.out.println("Sales order id=="+OrderId);
		System.out.println("Company ID==="+companyId);
		Console.log("in invoicequery showAllFlag="+showAllFlag);
		MyQuerry querry = getInvoiceQuerry(this.companyId,this.OrderId);

		genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				
				ArrayList<Invoice> invoicelist = new ArrayList<Invoice>();
				
				for(SuperModel model : result){
					Invoice invoice = (Invoice) model;
					invoicelist.add(invoice);
				}
				
				if (result.size() != 0) {
					if (invoicelist.size() != 0) {
						Comparator<Invoice> countSort = new Comparator<Invoice>() {

							@Override
							public int compare(Invoice r1, Invoice r2) {
								int count1 = r1.getCount();
								int count2 = r2.getCount();
								if (count1 == count2) {
									return 0;
								}
								if (count1 > count2) {
									return 1;
								} else {
									return -1;
								}
							}
						};
						Collections.sort(invoicelist, countSort);
					}
				}
				
				tabInvoiceTable.getDataprovider().setList(invoicelist);
				glassPanel.hide();
				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
		
	}

	private MyQuerry getInvoiceQuerry(long companyId, int orderId) {


		MyQuerry querry = new MyQuerry();
		
		Vector<Filter> filtervec = new Vector<Filter>();
		
		Filter filter = new Filter();
		filter.setIntValue(orderId);
		filter.setQuerryString("contractCount");
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setLongValue(companyId);
		filter.setQuerryString("companyId");
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setStringValue("Service Order");
		filter.setQuerryString("typeOfOrder");
		filtervec.add(filter);
		
		if(!showAllFlag) {
			List<String> lstatus=new ArrayList<String>();
			lstatus.add("Created");
			lstatus.add("PInvoice");
			
			filter = new Filter();
			filter.setList(lstatus);
			filter.setQuerryString("status IN");
			filtervec.add(filter);
		}
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Invoice());
		return querry;
	}



	private MyQuerry getPaymentQuerry(long companyId, int orderId) {

		Console.log("in getPaymentQuerry showallflag="+showAllFlag);
		MyQuerry querry = new MyQuerry();
		
		Vector<Filter> filtervec = new Vector<Filter>();
		
		Filter filter = new Filter();
		filter.setIntValue(orderId);
		filter.setQuerryString("contractCount");
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setLongValue(companyId);
		filter.setQuerryString("companyId");
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setStringValue("Service Order");
		filter.setQuerryString("typeOfOrder");
		filtervec.add(filter);
		
		if(!showAllFlag) {
			List<String> lstatus=new ArrayList<String>();
			lstatus.add("Created");			
			
			filter = new Filter();
			filter.setList(lstatus);
			filter.setQuerryString("status IN");
			filtervec.add(filter);
		}
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new CustomerPayment());
		
		return querry;
	}

	
	private MyQuerry getServiceQuerry(long companyId, int orderId) {

		MyQuerry querry = new MyQuerry();
		
		Vector<Filter> filtervec = new Vector<Filter>();
		
		Filter filter = new Filter();
		filter.setIntValue(orderId);
		filter.setQuerryString("contractCount");
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setLongValue(companyId);
		filter.setQuerryString("companyId");
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Service());
		
		return querry;
	}



	public QuickSalesOrderBillingTable getTabBillingTable() {
		return tabBillingTable;
	}



	public void setTabBillingTable(QuickSalesOrderBillingTable tabBillingTable) {
		this.tabBillingTable = tabBillingTable;
	}



	public Image getBtnClose() {
		return btnClose;
	}



	public void setBtnClose(Image btnClose) {
		this.btnClose = btnClose;
	}

	@Override
	public void onClick(ClickEvent event) {
		if (event.getSource() == genralInvoicePopup.btnClose) {
			Console.log("invoice popup btn close");
			tabInvoiceTable.getTable().redraw();
			setInvoiceTableDate();
			setTabelSelectionOnInvoiceTable();
		}
	}
	
	
}
