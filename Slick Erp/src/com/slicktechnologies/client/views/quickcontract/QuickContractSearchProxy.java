package com.slicktechnologies.client.views.quickcontract;

import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.contract.ContractPresenterSearchProxy;

public class QuickContractSearchProxy extends ContractPresenterSearchProxy{

	@Override
	public void createScreen() {
		// TODO Auto-generated method stub
//		super.createScreen();
		
		pedioContractFlag=AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "PedioContract");

		initWidget();
		FormFieldBuilder builder;
		builder = new FormFieldBuilder("", personInfo);
		FormField fpersonInfo = builder.setMandatory(false).setRowSpan(0)
				.setColSpan(3).build();
		builder = new FormFieldBuilder("Contract Id", tbContractId);
		FormField ftbContractId = builder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		builder = new FormFieldBuilder("Quotation Id", tbQuotationId);
		FormField ftbQuotationId = builder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		builder = new FormFieldBuilder("Ticket Id", tbTicketNumber);
		FormField ftbTicketNumber = builder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		builder = new FormFieldBuilder("Lead Id", tbLeadId);
		FormField ftbLeadId = builder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		builder = new FormFieldBuilder("Branch", olbBranch);
		FormField folbBranch = builder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		builder = new FormFieldBuilder("Sales Person", olbEmployee);
		FormField folbEmployee = builder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		builder = new FormFieldBuilder("From Date (Creation Date)",
				dateComparator.getFromDate());
		FormField fdateComparator = builder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		builder = new FormFieldBuilder("To Date (Creation Date)",
				dateComparator.getToDate());
		FormField fdateComparator1 = builder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		builder = new FormFieldBuilder("Contract Group", olbContractGroup);
		FormField folbQuotationGroup = builder.setMandatory(false)
				.setRowSpan(0).setColSpan(0).build();

		builder = new FormFieldBuilder("Contract Category", olbContractCategory);
		FormField folbQuotationCategory = builder.setMandatory(false)
				.setRowSpan(0).setColSpan(0).build();

		builder = new FormFieldBuilder("Contract Type", olbContractType);
		FormField folbQuotationType = builder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		builder = new FormFieldBuilder("Contract Status", olbContractStatus);
		FormField folbQuotationStatus = builder.setMandatory(false)
				.setRowSpan(0).setColSpan(0).build();

		builder = new FormFieldBuilder("Ref Number 1", tbRefNumber);
		FormField frefNumber = builder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		builder = new FormFieldBuilder("Created By", olbCreatedBy);
		FormField folbCreatedBy = builder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		builder = new FormFieldBuilder("Ref Number 2", tbRefNum2);
		FormField ftbRefNum2 = builder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		builder = new FormFieldBuilder("Warehouse", tbWarehouse);
		FormField fWarehouse = builder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		builder = new FormFieldBuilder("Storage Location", tbStorageLocation);
		FormField fStorageLoc = builder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		builder = new FormFieldBuilder("Stack No", tbStackNo);
		FormField fStackNo = builder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		builder = new FormFieldBuilder(
				"* Note : If no filter is selected, by default today's data will be displayed on click of Go Button.",
				null);
		FormField fnote = builder.setMandatory(false).setRowSpan(0)
				.setColSpan(3).build();

		/*
		 * nidhi 30-06-2017 search for segment and this field is added in 4 row
		 * , LAst col
		 */
		if (AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","OnlyForNBHC")) {
			builder = new FormFieldBuilder("Segment Type", olbCustomerCategory);
		} else {
			builder = new FormFieldBuilder("Segment Type", tbSegment);
		}
		FormField folbCustCategory = builder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		/*
		 * end
		 */

		// ****************Date 04/09/2017 by jayshree add the production
		// composite in search popup****************
		builder = new FormFieldBuilder("", prodInfoComposite);
		FormField fprodInfoComposite = builder.setMandatory(false)
				.setRowSpan(0).setColSpan(3).build();

		/** 02-10-2017 sagar sore[ adding Number Range field in search pop up] **/
		FormField folbcNumberRange;
		builder = new FormFieldBuilder("Number Range", olbcNumberRange);
		folbcNumberRange = builder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		builder = new FormFieldBuilder("From Date (Start Date)",
				startDateComparator.getFromDate());
		FormField fstartDateComparatorfrom = builder.setMandatory(false)
				.setRowSpan(0).setColSpan(0).build();

		builder = new FormFieldBuilder("To Date (Start Date)",
				startDateComparator.getToDate());
		FormField fstartDateComparatorto = builder.setMandatory(false)
				.setRowSpan(0).setColSpan(0).build();

		builder = new FormFieldBuilder("From Date (End Date)",
				endDateComparator.getFromDate());
		FormField fendDateComparatorfrom = builder.setMandatory(false)
				.setRowSpan(0).setColSpan(0).build();

		builder = new FormFieldBuilder("To Date (End Date)",
				endDateComparator.getToDate());
		FormField fendDateComparatorto = builder.setMandatory(false)
				.setRowSpan(0).setColSpan(0).build();

		/**
		 * Date 12-01-2018 By Vijay differentiated date combination search and
		 * other filter search
		 */
		builder = new FormFieldBuilder();
		FormField fgroupingDateFilterInformation = builder
				.setlabel("Date Filter Combination")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(5).build();

		builder = new FormFieldBuilder();
		FormField fgroupingOtherFilterInformation = builder
				.setlabel("Other Filter Combination")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(5).build();
		/**
		 * ends here
		 */

		/**
		 * Date : 17/2/2018 BY MANISHA Change contract period name as salesource
		 * as per sonu's requirement..!!
		 */
		FormField folbcontractPeriod;
		if (AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract",
				"OnlyForOrionPestSolutions")) {
			builder = new FormFieldBuilder("Sale Source", olbcontractPeriod);
			folbcontractPeriod = builder.setMandatory(false).setRowSpan(0)
					.setColSpan(0).build();
		} else {
			builder = new FormFieldBuilder("Contract Period", olbcontractPeriod);
			folbcontractPeriod = builder.setMandatory(false).setRowSpan(0)
					.setColSpan(0).build();
		}
		/**
		 * End
		 **/

		// builder = new FormFieldBuilder("Contract Period",olbcontractPeriod);
		// FormField folbcontractPeriod =
		// builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		/**
		 * Date 19-03-2019 By Vijay Des :- NBHC CCPM Non Renewal contract search
		 * filter
		 */
		builder = new FormFieldBuilder("Non Renewal", olbNonRenewal);
		FormField foblNonRenewal = builder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		/**
		 * Date 03-04-2019 by Vijay for Cancelled records search
		 */
		builder = new FormFieldBuilder("From Date (Cancelled Date)",
				cancellationDateComparator.getFromDate());
		FormField fcancellationFrmDateComparator = builder.setMandatory(false)
				.setRowSpan(0).setColSpan(0).build();

		builder = new FormFieldBuilder("To Date (Cancelled Date)",
				cancellationDateComparator.getToDate());
		FormField fcancellationToDateComparator = builder.setMandatory(false)
				.setRowSpan(0).setColSpan(0).build();

		if (pedioContractFlag) {
			this.fields = new FormField[][] {
					{ fgroupingDateFilterInformation },
					{ fdateComparator, fdateComparator1,fstartDateComparatorfrom, fstartDateComparatorto },
					{ fendDateComparatorfrom, fendDateComparatorto, folbBranch,folbQuotationStatus }, { ftbContractId },
					{ fpersonInfo }, { fnote } };
			return;
		}

		/**
		 * Date 12-07-2018 By Vijay Des :- for NBHC CCPM need only limited
		 * search fields and else for all standard
		 */
		if (AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract",
				"OnlyForNBHC")) {
			this.fields = new FormField[][] {
					{ fgroupingDateFilterInformation },
					{ fdateComparator, fdateComparator1,
							fstartDateComparatorfrom, fstartDateComparatorto },
					{ fendDateComparatorfrom, fendDateComparatorto, folbBranch,
							folbQuotationStatus },
					{ fcancellationFrmDateComparator,
							fcancellationToDateComparator, folbQuotationGroup,
							foblNonRenewal },
					{ fgroupingOtherFilterInformation },
					{ folbQuotationCategory, folbQuotationType,
							ftbTicketNumber, folbcontractPeriod },
					{ ftbQuotationId, ftbLeadId, ftbContractId, folbEmployee },
					{ fWarehouse, fStorageLoc, fStackNo, ftbRefNum2 },
					{ fpersonInfo }, { fnote } };
		} else {
			this.fields = new FormField[][] {
					/**
					 * 02-10-2017 sagar sore [folbcNumberRange added and order
					 * of fields changed]
					 **/
					/** old code **/
					// {fdateComparator,fdateComparator1,folbQuotationGroup,folbQuotationCategory},
					// {folbQuotationType,folbQuotationStatus,folbEmployee,folbBranch},
					// {ftbQuotationId,ftbLeadId,ftbContractId,ftbTicketNumber},
					// {frefNumber,ftbRefNum2,folbCreatedBy,folbCustCategory},
					// {fWarehouse,fStorageLoc,fStackNo},
					// {fpersonInfo},
					// {fprodInfoComposite},
					// {fnote}
					/*** updated order **/
					// {fdateComparator,fdateComparator1,folbQuotationGroup,folbQuotationCategory},
					// {folbQuotationType,folbQuotationStatus,folbcNumberRange,folbBranch},
					// {ftbQuotationId,ftbLeadId,ftbContractId,folbEmployee},
					// {frefNumber,ftbRefNum2,folbCreatedBy,folbCustCategory},
					// {fWarehouse,fStorageLoc,fStackNo,ftbTicketNumber},
					// {fpersonInfo},
					// {fprodInfoComposite},
					// {fstartDateComparatorfrom,fstartDateComparatorto,fendDateComparatorfrom,fendDateComparatorto},
					// {fnote}

					/**
					 * date 12-01-2018 by vijay above old code commented and
					 * below groping added
					 **/

					{ fgroupingDateFilterInformation },
					{ fdateComparator, fdateComparator1,
							fstartDateComparatorfrom, fstartDateComparatorto },
					{ fendDateComparatorfrom, fendDateComparatorto, folbBranch,
							folbQuotationStatus },
					{ fcancellationFrmDateComparator,
							fcancellationToDateComparator, folbQuotationGroup,
							foblNonRenewal },
					{ fgroupingOtherFilterInformation },
					{ folbQuotationCategory, folbQuotationType,
							folbcNumberRange, ftbTicketNumber },
					{ ftbQuotationId, ftbLeadId, ftbContractId, folbEmployee },
					{ frefNumber, ftbRefNum2, folbCreatedBy, folbCustCategory },
					{ fWarehouse, fStorageLoc, fStackNo, folbcontractPeriod },
					{ fpersonInfo }, { fprodInfoComposite }, { fnote } };
		}

	}
	
	

}
