package com.slicktechnologies.client.views.communicationlog;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.common.complain.ComplainList;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;

public class CommunicationLogTable extends SuperTable<InteractionType>{

	boolean isLeadFlag=false;
	TextColumn<InteractionType> getColumnSrNo;
	TextColumn<InteractionType> getColumnCreationDate;
	TextColumn<InteractionType> getColumnTime;
	TextColumn<InteractionType> getColumnUserName;
	TextColumn<InteractionType> getColumnRemark;
	TextColumn<InteractionType> getColumnDueDate;
	TextColumn<InteractionType> getColumnInteractionGroup;
	TextColumn<InteractionType> getColumnLeadStatus;
	TextColumn<InteractionType> getColumnSalesPerson;
	
	Column<InteractionType,String> deleteColumn;
	
	
	public CommunicationLogTable(){
		super();	
		setHeight("340Px");
	}
	
	public CommunicationLogTable(boolean isLead){
		super(isLead);	
		setHeight("340Px");
		isLeadFlag=isLead;
		Console.log("inside my constructor11 "+isLeadFlag);
		createTable1();
	}
	
	public void createTable1() {
		createColumnCreationDate();
		createColumnTimeColumn();
		createColumnUserColumn();
		createColumnRemarkColumn();
		createColumndueDateColumn();
		createColumnInteractionGroup();
		Console.log("lead flag "+isLeadFlag);
		if(isLeadFlag){
			Console.log("lead flag22 "+isLeadFlag);
			createColumnLeadStatus();
			createColumnSalesPerson();	
		}
		createColumndeleteColumn();
		addFieldUpdater();
		table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);	
	}
	
	@Override
	public void createTable() {
		createColumnCreationDate();
		createColumnTimeColumn();
		createColumnUserColumn();
		createColumnRemarkColumn();
		createColumndueDateColumn();
		createColumnInteractionGroup();
		Console.log("lead flag "+isLeadFlag);
		if(isLeadFlag){
			Console.log("lead flag22 "+isLeadFlag);
			createColumnLeadStatus();
			createColumnSalesPerson();	
		}
		createColumndeleteColumn();
		addFieldUpdater();
		table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);	
	}

	private void createColumnSalesPerson() {

		getColumnSalesPerson = new TextColumn<InteractionType>() {
			
			@Override
			public String getValue(InteractionType object) {
				if(object.getInteractionPersonResponsible()!=null)
					return object.getInteractionPersonResponsible();
				return "";
			}
		};
		 table.addColumn(getColumnSalesPerson,"Sales Person");
		 table.setColumnWidth(getColumnSalesPerson, 100,Unit.PX);

	}

	private void createColumnLeadStatus() {

		getColumnLeadStatus = new TextColumn<InteractionType>() {
			
			@Override
			public String getValue(InteractionType object) {
				if(object.getInteractionStatus()!=null)
					return object.getInteractionStatus();
				return "";
			}
		};
		 table.addColumn(getColumnLeadStatus,"Lead Status");
		 table.setColumnWidth(getColumnLeadStatus, 100,Unit.PX);

	}

	private void createColumnInteractionGroup() {

		getColumnInteractionGroup = new TextColumn<InteractionType>() {
			
			@Override
			public String getValue(InteractionType object) {
				if(object.getInteractionGroup()!=null)
					return object.getInteractionGroup();
				return "";
			}
		};
		if(isLeadFlag){
			 table.addColumn(getColumnInteractionGroup,"Remark");
		}else{
			 table.addColumn(getColumnInteractionGroup,"Interaction Group");	
		}
		
		 table.setColumnWidth(getColumnInteractionGroup, 100,Unit.PX);

	}

	private void createColumnCreationDate() {

		getColumnCreationDate = new TextColumn<InteractionType>() {
			
			@Override
			public String getValue(InteractionType object) {
				if(object.getinteractionCreationDate()!=null){
					return AppUtility.parseDate(object.getinteractionCreationDate());
				}
				return "";
			}
		};
		table.addColumn(getColumnCreationDate,"Creation Date");
		table.setColumnWidth(getColumnCreationDate, 100,Unit.PX);
		
	}

	private void createColumnTimeColumn() {

		getColumnTime = new TextColumn<InteractionType>() {
			
			@Override
			public String getValue(InteractionType object) {
				if(object.getInteractionTime()!=null){
					return object.getInteractionTime();
				}
				return "";
			}
		};
		table.addColumn(getColumnTime,"Time");
		table.setColumnWidth(getColumnTime, 80,Unit.PX);
		
	}

	private void createColumnUserColumn() {

		getColumnUserName = new TextColumn<InteractionType>() {
			
			@Override
			public String getValue(InteractionType object) {
				if(object.getCreatedBy()!=null){
					return object.getCreatedBy();
				}
				return "";
			}
		};
		table.addColumn(getColumnUserName,"User Name");
		table.setColumnWidth(getColumnUserName, 90,Unit.PX);
		
	}

	private void createColumnRemarkColumn() {

		getColumnRemark = new TextColumn<InteractionType>() {
			
			@Override
			public String getValue(InteractionType object) {
				if(object.getInteractionPurpose()!=null){
					return object.getInteractionPurpose();
				}
				return "";
			}
		};
          if(isLeadFlag){
        	  table.addColumn(getColumnRemark,"Note");
		}else{
			table.addColumn(getColumnRemark,"Interaction Remark");
		}
		
		table.setColumnWidth(getColumnRemark, 200,Unit.PX);
		
	}

	private void createColumndueDateColumn() {

		getColumnDueDate = new TextColumn<InteractionType>() {
			
			@Override
			public String getValue(InteractionType object) {
				if(object.getInteractionDueDate()!=null){
					return AppUtility.parseDate(object.getInteractionDueDate());
				}
				return null;
			}
		};
        if(isLeadFlag){
        	table.addColumn(getColumnDueDate,"Follow Up Date");
		}else{
			table.addColumn(getColumnDueDate,"Interaction Due Date");
		}
		
		table.setColumnWidth(getColumnDueDate, 100, Unit.PX);
		
	}

	private void createColumndeleteColumn() {

		ButtonCell btnCell = new ButtonCell();
		deleteColumn = new Column<InteractionType, String>(btnCell) {
			
			@Override
			public void render(Context context, InteractionType object,
					SafeHtmlBuilder sb) {
				if(object.isCommunicationlogFlag()){
					super.render(context, object, sb);
				}
				
			}
			
			@Override
			public String getValue(InteractionType object) {
				if(object.isCommunicationlogFlag()){
					return "Delete";
				}
				return "";
			}

			
			
		};
		table.addColumn(deleteColumn,"Delete");
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		createFieldUpdaterdeleteColumn();
	}

	private void createFieldUpdaterdeleteColumn() {
		deleteColumn.setFieldUpdater(new FieldUpdater<InteractionType, String>() {
			
			@Override
			public void update(int index, InteractionType object, String value) {
				
				getDataprovider().getList().remove(object);
				table.redrawRow(index);
			}
		});
		
		RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);

	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		Console.log("value of lead flag in set Enable method "+isLeadFlag);
//	    this.clear();
//		createTable();
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
