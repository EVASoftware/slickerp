package com.slicktechnologies.client.views.communicationlog;

import java.util.Date;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.view.client.NoSelectionModel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.interaction.interactiondetails.InteractionForm;
import com.slicktechnologies.client.views.interaction.interactiondetails.InteractionPresenter;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;

public class CommunicationLogPopUp extends AbsolutePanel {
	
	private TextArea remark;
	private DateBox dueDate;
	Button btnAdd,btnOk,btnCancel;
	
	CommunicationLogTable communicationLogTable;
	ObjectListBox<Config> oblinteractionGroup;
	
	GWTCAlert alert = new GWTCAlert();
	
	
	public CommunicationLogPopUp(){
		
		
		InlineLabel lbRemark = new InlineLabel("Interaction Remark (500 character only)");
		add(lbRemark,10,20);
		
		remark = new TextArea();
		remark.setWidth("400px");
		add(remark,10,40);
		
		InlineLabel lbDueDate = new InlineLabel("Interaction Due Date");
		add(lbDueDate,440,20);
		
		dueDate = new DateBoxWithYearSelector();
		dueDate.setWidth("120px");
		add(dueDate,440,40);
		
		DateTimeFormat dateFormat = DateTimeFormat.getFormat("dd-MM-yyyy"); 
		dueDate.setFormat(new DateBox.DefaultFormat(dateFormat));
		
		InlineLabel lblInteractionGroup = new InlineLabel("Interaction Group");
		add(lblInteractionGroup,590,20);
		
		oblinteractionGroup = new ObjectListBox<Config>();
		/**** Date 19-10-2018 By Vijay for interaction group overlap issue updated width   ***/
		oblinteractionGroup.getElement().getStyle().setWidth(110, Unit.PX);
		AppUtility.MakeLiveConfig(oblinteractionGroup, Screen.GROUPTYPE);
		add(oblinteractionGroup,590,40);
		
		
		btnAdd = new Button("Add");
		add(btnAdd,740,35);
		
		communicationLogTable = new CommunicationLogTable();
		communicationLogTable.connectToLocal();
		add(communicationLogTable.getTable(),10,100);
		
		btnOk = new Button("Save");
		add(btnOk,270,470);
		
		btnCancel = new Button("Cancel");
		add(btnCancel,380,470);
		
		
		setSize("800px", "500px");
		this.getElement().setId("form");
		
		
		setTableSelectionOnInteractionToDo();

		
	}


	private void setTableSelectionOnInteractionToDo() {

		 final NoSelectionModel<InteractionType> selectionModelMyObj = new NoSelectionModel<InteractionType>();
	     
	     SelectionChangeEvent.Handler  tableHandler = new SelectionChangeEvent.Handler() 
	     {
	         @Override
	         public void onSelectionChange(SelectionChangeEvent event) 
	         {
	        	 final InteractionType entity=selectionModelMyObj.getLastSelectedObject();
	        	 if(entity.isCommunicationlogFlag() == false){
	        	 LoginPresenter.communicationLogPanel.hide();	 
	        	 AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
	        	 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Interaction TO DO",Screen.INTERACTION);
	        	 final InteractionForm form = InteractionPresenter.initalize();
	        	 form.showWaitSymbol();
				 AppMemory.getAppMemory().stickPnel(form);
	        	
				 Timer timer=new Timer() 
	        	 {
	 				@Override
	 				public void run() {
	 					 form.hideWaitSymbol();
					     form.updateView(entity);
					     form.setToViewState();
	 				}
	 			};
	            
	             timer.schedule(3000); 
	             
	        	 }
	          }
	     };
	     
	     // Add the handler to the selection model
	     selectionModelMyObj.addSelectionChangeHandler( tableHandler );
	     // Add the selection model to the table
	     
	     communicationLogTable.getTable().setSelectionModel(selectionModelMyObj);
	 
	
	}

	public ObjectListBox<Config> getOblinteractionGroup() {
		return oblinteractionGroup;
	}

	public void setOblinteractionGroup(ObjectListBox<Config> oblinteractionGroup) {
		this.oblinteractionGroup = oblinteractionGroup;
	}

	private void reactOnAdd() {

	InteractionType interaction = new InteractionType();
	
	Date date = new Date();
	DateTimeFormat dtf = DateTimeFormat.getFormat("HH:mm:ss");
    String time=new String(dtf.format(date).toString());

    interaction.setinteractionCreationDate(date);
    interaction.setInteractionTime(time);
    interaction.setCreatedBy(LoginPresenter.loggedInUser);
    interaction.setInteractionPurpose(remark.getValue());
    interaction.setInteractionDueDate(dueDate.getValue());
    interaction.setCommunicationlogFlag(true);
    this.communicationLogTable.getDataprovider().getList().add(interaction);
	
	}

	public Button getBtnOk() {
		return btnOk;
	}

	public void setBtnOk(Button btnOk) {
		this.btnOk = btnOk;
	}

	public Button getBtnCancel() {
		return btnCancel;
	}

	public void setBtnCancel(Button btnCancel) {
		this.btnCancel = btnCancel;
	}

	public CommunicationLogTable getCommunicationLogTable() {
		return communicationLogTable;
	}

	public void setCommunicationLogTable(CommunicationLogTable communicationLogTable) {
		this.communicationLogTable = communicationLogTable;
	}
	
	public Button getBtnAdd() {
		return btnAdd;
	}

	public void setBtnAdd(Button btnAdd) {
		this.btnAdd = btnAdd;
	}	
	public TextArea getRemark() {
		return remark;
	}

	public void setRemark(TextArea remark) {
		this.remark = remark;
	}

	public DateBox getDueDate() {
		return dueDate;
	}

	public void setDueDate(DateBox dueDate) {
		this.dueDate = dueDate;
	}
	
	

}
