package com.slicktechnologies.client.views.salesorder;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;

public class ItemProductDetailsPopUp extends PopupScreen{
	
	TextArea taDetails ;
	TextArea taDetails1;
	public ItemProductDetailsPopUp(){
		super();
		createGui();
		popup.getElement().addClassName("popItemPro");
		popup.setWidth("70%");
	}
	
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
	}

	private void initializeWidgets() {
		taDetails = new TextArea();
		taDetails1 = new TextArea();
		taDetails.setWidth("300px");
		taDetails1.setWidth("300px");
		taDetails.setHeight("200px");
		taDetails1.setHeight("200px");
	}

	
	@Override
	public void createScreen() {
		// TODO Auto-generated method stub
		initializeWidgets();
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField frenewalInfo = fbuilder.setlabel("Non Renewal Remark").setColSpan(4).widgetType(FieldType.Grouping).build();
		
		fbuilder = new FormFieldBuilder("Description 1",taDetails);
		FormField ftaComments= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Description 2",taDetails1);
		FormField ftaComments1= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		FormField [][]formfield = {
				{frenewalInfo},
				{ftaComments},
				{ftaComments1}
		};
		this.fields=formfield;
				
	}

	public TextArea getTaDetails() {
		return taDetails;
	}

	public void setTaDetails(TextArea taDetails) {
		this.taDetails = taDetails;
	}

	public TextArea getTaDetails1() {
		return taDetails1;
	}

	public void setTaDetails1(TextArea taDetails1) {
		this.taDetails1 = taDetails1;
	}

}
