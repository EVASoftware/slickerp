package com.slicktechnologies.client.views.salesorder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.TextAlign;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.AddressComposite;
import com.simplesoftwares.client.library.composite.OneToManyComposite;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.composite.UploadComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.approvalutility.ApprovableFormScreen;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.config.CategoryTypeFactory;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.contract.ContractForm;
import com.slicktechnologies.client.views.contract.OtherChargesTable;
import com.slicktechnologies.client.views.contract.PaymentTermsTable;
import com.slicktechnologies.client.views.contract.ProductChargesTable;
import com.slicktechnologies.client.views.contract.ProductTaxesTable;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.client.views.salesquotation.SalesQuotationPresenter;
import com.slicktechnologies.client.views.salesquotation.VendorAndOtherChargesMarginPopup;
import com.slicktechnologies.shared.ProductOtherCharges;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.TaxDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cnc.CNC;
import com.slicktechnologies.shared.common.cnc.Consumables;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.inventory.GRNDetails;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.productlayer.Tax;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.salesorder.OtherCharges;
import com.slicktechnologies.shared.common.salesorder.OtherChargesMargin;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesorder.SalesQuotation;
import com.slicktechnologies.shared.common.salesorder.VendorMargin;

public class SalesOrderForm extends ApprovableFormScreen<SalesOrder> implements ClickHandler, ChangeHandler {

	ObjectListBox<Branch> olbbBranch;
	TextBox tbReferenceNumber;
	ObjectListBox<Config> olbcPaymentMethods;
	TextBox tbContractId;
	TextBox tbRefeRRedBy;
	DateBox dbReferenceDate;
	TextBox tbQuotationStatus;
	TextArea taDescription;
	/**date 20-6-2020 by Amol added description two and vendor price raised by **/
	TextArea taDescriptionTwo;
	DoubleBox dbVendorPrice;
	UploadComposite ucUploadTAndCs;
	PersonInfoComposite personInfoComposite;
	ObjectListBox<Employee> olbeSalesPerson,olbApproverName;
	ObjectListBox<Config> olbSalesOrderGroup;
	ObjectListBox<Type> olbSalesOrderType;
	ObjectListBox<ConfigCategory> olbSalesOrderCategory;
	ListBox cbcformlis;
	ProductInfoComposite prodInfoComposite;
	DoubleBox dototalamt,doamtincltax,donetpayamt;
	TextBox tbLeadId,tbQuotatinId;
	IntegerBox ibdays;
	DoubleBox dopercent;
	IntegerBox ibCreditPeriod;
	TextBox tbcomment;
	Button addPaymentTerms;
	PaymentTermsTable paymentTermsTable;
	SalesOrderLineItemTable salesorderlineitemtable;
	ProductChargesTable chargesTable;
	Button addOtherCharges;
	ProductTaxesTable prodTaxTable;
	Button baddproducts;
	ObjectListBox<TaxDetails> olbcstpercent;
	DateBox dbdeliverydate;
	AddressComposite acshippingcomposite;
	CheckBox cbcheckaddress;
	DateBox dbsalesorderdate;
	TextBox tbremark;
	/******Date 2-2-2019
	 added by amol**
	 ***/
	TextBox ibRefOrderNo;
	Boolean refOrderNo=false;
	SalesOrder salesorderobj;
	FormField fgroupingCustomerInformation;
	public ObjectListBox<Config> olbcNumberRange;//Added by Ashwini
	
	final GenricServiceAsync genasync=GWT.create(GenricService.class);
	
	/**
	 * Date 11 july 2017 added by vijay for Cform validation
	 */
	Date date = DateTimeFormat.getFormat("yyyy-MM-dd HH:mm:ss").parse("2017-07-01 00:00:00");
	final DateTimeFormat fmt = DateTimeFormat.getFormat("dd/MM/yyyy");
	/**
	 * ends here
	 */

//  added by vijay for warrentyUntil calculation for product level
	public static Date salesorderDate=null;
	
	/**
	 * Date : 14-09-2017 BY ANIL
	 * Adding other charges table
	 */
	OtherChargesTable tblOtherCharges;
	Button addOthrChargesBtn;
	DoubleBox dbOtherChargesTotal;
	/**
	 * End
	 */
	
	/**
	 * Date 03-05-2018 By vijay
	 * for same product sr no because allow to add duplicate product so
	 * differentiate same product with product SRNO
	 */
	public static int productSrNo=0;
	/**
	 * ends here
	 */
	
	/**
	 * Date 08/06/2018
	 * Developer :- Vijay
	 * Des :- for final total amt Round off amt 
	 */
	DoubleBox dbfinalTotalAmt;
	TextBox tbroundoffAmt;
	/**
	 * ends here
	 */
	/** date 3.10.2018 added by komal for po and grn id**/
	public TextBox tbPOId;
	public TextBox tbGRNId;
	
	
	/**
	 * @author Anil , Date : 16-04-2019
	 * Added customer branch dropdown and poc name textbox
	 */
	public ObjectListBox<CustomerBranchDetails> oblCustomerBranch;
	TextBox tbCustPocName;
	

	/**
	 * @author Anil
	 * @since 17-07-2020
	 * For PTSPL raised by Rahul Tiwari
	 * calculating vendor and other charges margin
	 */
	public Button costingBtn;
	public boolean marginFlag;
	public List<VendorMargin> vendorMargins;
	public List<OtherChargesMargin> otherChargesMargins;
	public Double totalVendorMargin;
	public Double totalOcMargin;
	public VendorAndOtherChargesMarginPopup marginPopup=new VendorAndOtherChargesMarginPopup();
	
	public CNC cncEntity = null;
	
	/**
	 * @author Anil
	 * @since 30-11-2020
	 * If customer branch is selected the also update billing address along with shipping address
	 */
	Address billingAddress;
	
	public Address customerServiceAddress;
	public Address customerBillingAddress;
	
	public Customer cust;
	
	/**
	 * @author Anil @since 10-11-2021
	 * Sunrise SCM
	 */
	TextBox tbBudget;
	String billingType;
	String costCode;
	String carpetArea;
	boolean uploadConsumableFlag=false;
	
	ObjectListBox<CompanyPayment> objPaymentMode;
	ArrayList<CompanyPayment> paymentModeList = new ArrayList<CompanyPayment>();
	
	public SalesOrderForm() {
		super();
		createGui();
		this.tbContractId.setEnabled(false);
		this.tbQuotationStatus.setEnabled(false);
		this.tbQuotationStatus.setText(SalesOrder.CREATED);
		salesorderlineitemtable.connectToLocal();
		paymentTermsTable.connectToLocal();
		chargesTable.connectToLocal();
		prodTaxTable.connectToLocal();
		
		/**
		 * Date :14-09-2017 By ANIL
		 */
		tblOtherCharges.connectToLocal();
		dbOtherChargesTotal.setEnabled(false);
		/**
		 * ENd
		 */
		/** date 3.10.2018 added by komal for po and grn id**/
		tbPOId.setEnabled(false);;
		tbGRNId.setEnabled(false);;
		
		marginFlag=AppUtility.checkForProcessConfigurartionIsActiveOrNot("SalesOrder","CalculateVendorAndOtherChargesMargin");
		marginPopup.getLblOk().addClickHandler(this);
		marginPopup.getLblCancel().addClickHandler(this);
		
		cncEntity = null;
		tbBudget.setEnabled(false);
		uploadConsumableFlag=AppUtility.checkForProcessConfigurartionIsActiveOrNot("CNC", "PC_UploadLocationWiseBudget");

	}
	
	public SalesOrderForm  (String[] processlevel, FormField[][] fields, FormStyle formstyle) 
	{
		super(processlevel, fields, formstyle);
		createGui();
	}

	//***********************************Variable Initialization********************************************//

		/**
		 * Method template to initialize the declared variables.
		 */
		@SuppressWarnings("unused")
		private void initalizeWidget()
		{
			uploadConsumableFlag=AppUtility.checkForProcessConfigurartionIsActiveOrNot("CNC", "PC_UploadLocationWiseBudget");
			tbReferenceNumber=new TextBox();
			tbReferenceNumber.addChangeHandler(this);
			
			olbbBranch=new ObjectListBox<Branch>();
			AppUtility.makeBranchListBoxLive(olbbBranch);
			olbcPaymentMethods=new ObjectListBox<Config>();
			AppUtility.MakeLiveConfig(olbcPaymentMethods,Screen.PAYMENTMETHODS);
			tbLeadId=new TextBox();
			tbQuotatinId=new TextBox();
			tbContractId=new TextBox();
			dbsalesorderdate=new DateBoxWithYearSelector();
			tbRefeRRedBy=new TextBox();
			tbQuotationStatus=new TextBox();
			dbReferenceDate=new DateBoxWithYearSelector();
			taDescription=new TextArea();
			taDescriptionTwo=new TextArea();
			dbVendorPrice=new DoubleBox();
			ucUploadTAndCs=new UploadComposite();
			salesorderlineitemtable=new SalesOrderLineItemTable();
			personInfoComposite=AppUtility.customerInfoComposite(new Customer());
			olbeSalesPerson=new ObjectListBox<Employee>();
//			AppUtility.makeSalesPersonListBoxLive(olbeSalesPerson);
			olbeSalesPerson.makeEmployeeLive(AppConstants.SALESMODULE, AppConstants.SALESORDER, "Sales Person");
			olbSalesOrderGroup=new ObjectListBox<Config>();
			olbSalesOrderCategory=new ObjectListBox<ConfigCategory>();
			AppUtility.MakeLiveCategoryConfig(olbSalesOrderCategory, Screen.SALESORDERCATEGORY);
			olbSalesOrderCategory.addChangeHandler(this);
			olbSalesOrderType=new ObjectListBox<Type>();
			AppUtility.makeTypeListBoxLive(olbSalesOrderType,Screen.SALESORDERTYPE);
			AppUtility.MakeLiveConfig(olbSalesOrderGroup, Screen.SALESORDERGROUP);
			olbApproverName=new ObjectListBox<Employee>();
			AppUtility.makeApproverListBoxLive(olbApproverName,"Sales Order");
			this.tbContractId.setEnabled(false);
			this.tbLeadId.setEnabled(false);
			this.tbQuotatinId.setEnabled(false);
			personInfoComposite.getCustomerId().getHeaderLabel().setText("* Customer ID");
			personInfoComposite.getCustomerName().getHeaderLabel().setText("* Customer Name");
			personInfoComposite.getCustomerCell().getHeaderLabel().setText("* Customer Cell");
			ibdays=new IntegerBox();
			ibCreditPeriod=new IntegerBox();
			dopercent=new DoubleBox();
			tbcomment=new TextBox();
			prodInfoComposite=AppUtility.initiateSalesProductComposite(new SuperProduct());
			addPaymentTerms=new Button("ADD");
			addPaymentTerms.addClickHandler(this);
			paymentTermsTable=new PaymentTermsTable();
			chargesTable=new ProductChargesTable();
			addOtherCharges=new Button("+");
			addOtherCharges.addClickHandler(this);
			prodTaxTable=new ProductTaxesTable();
			dototalamt=new DoubleBox();
			dototalamt.setEnabled(false);
			doamtincltax=new DoubleBox();
			doamtincltax.setEnabled(false);
			donetpayamt=new DoubleBox();
			donetpayamt.setEnabled(false);
			
			/**
			 * Date : 14-09-2017 By ANIL
			 */
			tblOtherCharges=new OtherChargesTable();
			
			addOthrChargesBtn=new Button("+");
			addOthrChargesBtn.addClickHandler(this);
			
			dbOtherChargesTotal=new DoubleBox();
			dbOtherChargesTotal.setEnabled(false);
			
			
			ibRefOrderNo = new TextBox();
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("PurchaseOrder", "MonthwiseNumberGeneration")){
				ibRefOrderNo.setEnabled(false);
			}
			
			
			
			/**
			 * End
			 */
			
	/**
	 * nidhi
	 * 12-06-2018
	 * for stop null pointer		
	 */
//			this.changeWidgets();
			
			tbremark=new TextBox();
			tbremark.setEnabled(false);
			
//			dbdeliverydate=new DateBox();
			// Date 22-06-2020 to set time mid of te day to date by vijay
			dbdeliverydate = new DateBoxWithYearSelector();
			cbcheckaddress=new CheckBox();
			cbcheckaddress.setValue(false);
			acshippingcomposite=new AddressComposite();
			acshippingcomposite.setEnable(false);
			
			cbcformlis=new ListBox();
			cbcformlis.addItem("SELECT");
			cbcformlis.addItem(AppConstants.YES);
			cbcformlis.addItem(AppConstants.NO);
			cbcformlis.setEnabled(false);
			olbcstpercent=new ObjectListBox<TaxDetails>();
			olbcstpercent.setEnabled(false);
			initializePercentCst();
			baddproducts=new Button("ADD");
			baddproducts.addClickHandler(this);
			
			
			dbfinalTotalAmt = new DoubleBox();
			dbfinalTotalAmt.setEnabled(false);
			tbroundoffAmt = new TextBox();
			tbroundoffAmt.addChangeHandler(this);
			/*
			 * Date:19/07/2018
			 * Developer:Ashwini
			 */
			olbcNumberRange = new ObjectListBox<Config>();
			AppUtility.MakeLiveConfig(olbcNumberRange, Screen.NUMBERRANGE);
			olbcNumberRange.addChangeHandler(this);
			
			/*
			 * End by Ashwini
			 */
			/** date 3.10.2018 added by komal for po and grn id**/
			tbPOId = new TextBox();
			tbGRNId = new TextBox();

			this.changeWidgets();
			
			
			oblCustomerBranch=new ObjectListBox<CustomerBranchDetails>();
			oblCustomerBranch.addItem("--SELECT--");
			oblCustomerBranch.addChangeHandler(this);
			tbCustPocName=new TextBox();
			
			costingBtn=new Button("Costing");
			costingBtn.addClickHandler(this);
			
			tbBudget=new TextBox();
			
			objPaymentMode = new ObjectListBox<CompanyPayment>();
			loadPaymentMode();
			
		}

		/**
		 * method template to create screen formfields
		 */
		@Override
		public void createScreen() {

			marginFlag=AppUtility.checkForProcessConfigurartionIsActiveOrNot("SalesOrder","CalculateVendorAndOtherChargesMargin");
			initalizeWidget();
		//	setDisabled(paymentComposite);
			/**
			 * Date : 29-07-2017 BY ANIL
			 * added view bill button on process level bar
			 *  Date : 19-04-2018 BY ANIL
			 * added view amc ,view PO and create PO button for HVAC/Rohan/Nitin Sir
			 */
			//Token to initialize the processlevel menus.
			//this.processlevelBarNames=new String[]{ManageApprovals.APPROVALREQUEST,ManageApprovals.CANCELAPPROVALREQUEST,AppConstants.CANCLESALESORDER,AppConstants.CANCELLATIONSUMMARY,AppConstants.WORKORDER,
					//AppConstants.NEW,"Create AMC",AppConstants.MATERIALSATUS,ManageApprovals.SUBMIT,AppConstants.VIEWBILL,AppConstants.CREATEPO,AppConstants.VIEWAMC,AppConstants.VIEWPO,AppConstants.CUSTOMERADDRESS,"Sales Order Margin","View Delivery Note","Copy Order"};
			/**
			 * Date : 28-12-2020 By Priyanka
			 * Des : Change Navigation flow as per requirement of simplification Project raised by Nitin Sir.
			 */
			this.processlevelBarNames=new String[]{ManageApprovals.SUBMIT,AppConstants.CANCLESALESORDER,AppConstants.NEW,"Sales Order Margin",AppConstants.WORKORDER,"Create AMC",AppConstants.VIEWAMC,AppConstants.CREATEPO,AppConstants.VIEWPO,AppConstants.VIEWBILL,
					                              "View Delivery Note",AppConstants.MATERIALSATUS,AppConstants.CUSTOMERADDRESS,ManageApprovals.APPROVALREQUEST,ManageApprovals.CANCELAPPROVALREQUEST,AppConstants.CANCELLATIONSUMMARY,AppConstants.COPY,AppConstants.REGISTEREXPENSE,AppConstants.VIEWEXPENSES,AppConstants.VIEWINVOICEPAYMENT};
			
			/////////////////////// Form Field Initialization////////////////////////////////////////////////////
			//Token to initialize formfield
			
			String mainScreenLabel="Sales Order";
			if(salesorderobj!=null&&salesorderobj.getCount()!=0){
				mainScreenLabel=salesorderobj.getCount()+" "+"/"+" "+salesorderobj.getStatus()+" "+"/"+" "+AppUtility.parseDate(salesorderobj.getCreationDate());
			}
				//fgroupingCustomerInformation.getHeaderLabel().setText(mainScreenLabel);
			FormFieldBuilder fbuilder;
			fbuilder = new FormFieldBuilder();
			fgroupingCustomerInformation=fbuilder.setlabel(mainScreenLabel).widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).setKeyField(true).build();
			//FormField fgroupingCustomerInformation=fbuilder.setlabel("Customer Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
			fbuilder = new FormFieldBuilder("",personInfoComposite);
			FormField fpersonInfoComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
			fbuilder = new FormFieldBuilder("Lead ID",tbLeadId);
			FormField ftbLeadId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("Quotation ID",tbQuotatinId);
			FormField ftbQuotatinId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("Sales Order ID",tbContractId);
			FormField ftbContractId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			FormField ftbReferenceNumber= null;
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("SalesOrder", "MakeReferenceNumberMandatory")){
				fbuilder = new FormFieldBuilder("* Reference Number",tbReferenceNumber);
				ftbReferenceNumber= fbuilder.setMandatory(true).setMandatoryMsg("Reference Number is mandatory.").setRowSpan(0).setColSpan(0).build();
			}else{
				fbuilder = new FormFieldBuilder("Reference Number",tbReferenceNumber);
				ftbReferenceNumber= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			}
			fbuilder = new FormFieldBuilder("Referred By",tbRefeRRedBy);
			FormField ftbRefeRRedBy= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("Reference Date",dbReferenceDate);
			FormField fdbReferenceDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("* Sales Order Date",dbsalesorderdate);
			FormField fdbsalesorderdate= fbuilder.setMandatory(true).setMandatoryMsg("Sales Order Date is Mandatory!").setRowSpan(0).setColSpan(0).build();
			//fbuilder = new FormFieldBuilder("Valid Untill",validuntil);
			//FormField fvaliduntil= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder();
			FormField fgroupingDocuments=fbuilder.setlabel("Attachment").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
			fbuilder = new FormFieldBuilder("Upload T&Cs",ucUploadTAndCs);
			FormField fucUploadTAndCs= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder();
			FormField fgroupingSalesInformation=fbuilder.setlabel("Classification").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
			fbuilder = new FormFieldBuilder("Status",tbQuotationStatus);
			FormField ftbQuotationStatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("* Branch",olbbBranch);
			FormField folbbBranch= fbuilder.setMandatory(true).setMandatoryMsg("Branch is Mandatory").setRowSpan(0).setColSpan(0).build();
			/**@Sheetal:01-02-2022
	        Des : Making Sales person non mandatory,requirement by UDS Water**/
			fbuilder = new FormFieldBuilder("Sales Person",olbeSalesPerson);
			FormField folbeSalesPerson= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("Payment Methods",olbcPaymentMethods);
			FormField folbcPaymentMethods= fbuilder.setMandatory(false).setMandatoryMsg("Payment Methods is Mandatory").setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder();
			FormField fgroupingDescription=fbuilder.setlabel("Description").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
			fbuilder = new FormFieldBuilder("Description 1",taDescription);
			FormField ftaDescription= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
			
			fbuilder = new FormFieldBuilder("Description 2",taDescriptionTwo);
			FormField ftaDescriptionTwo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
			
			
			fbuilder = new FormFieldBuilder("Vendor Price",dbVendorPrice);
			FormField fdbVendorPrice= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			
			fbuilder = new FormFieldBuilder();
			FormField fgroupingProducts=fbuilder.setlabel("Product").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
			fbuilder = new FormFieldBuilder("Sales Order Type",olbSalesOrderType);
			FormField folbContractType= fbuilder.setMandatory(false).setMandatoryMsg("Sales Order Type is Mandatory").setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("Sales Order Group",olbSalesOrderGroup);
			FormField folbContractGroup= fbuilder.setMandatory(false).setMandatoryMsg("Sales Order Group is Mandatory").setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("Credit Days",ibCreditPeriod);
			FormField fibCreditPeriod= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("Sales Order Category",olbSalesOrderCategory);
			FormField folbContractCategory= fbuilder.setMandatory(false).setMandatoryMsg("Sales Order Category is Mandatory").setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("* Approver Name",olbApproverName);
			FormField folbApproverName= fbuilder.setMandatory(true).setMandatoryMsg("Approver Name is Mandatory").setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder();
			FormField fgroupingPaymentTermsDetails=fbuilder.setlabel("Payment Terms").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
			fbuilder = new FormFieldBuilder("Days",ibdays);
			FormField fibdays= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("Percent",dopercent);
			FormField fdopercent= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("Comment",tbcomment);
			FormField ftbcomment= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("Remark",tbremark);
			FormField ftbremark= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("",addPaymentTerms);
			FormField faddPaymentTerms= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("",paymentTermsTable.getTable());
			FormField fpaymentTermsTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
			fbuilder = new FormFieldBuilder();
			FormField fblankgroup=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
			fbuilder = new FormFieldBuilder();
			FormField fblankgroupthree=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
			fbuilder = new FormFieldBuilder();
			FormField fblankgroupone=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("",prodTaxTable.getTable());
			FormField fprodTaxTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
			//fbuilder = new FormFieldBuilder("Total Amount",doAmtExcludingTax);
			//FormField fdoAmtExcludingTax= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
			fbuilder = new FormFieldBuilder("Total Amount",doamtincltax);
			FormField fdoAmtIncludingTax= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
			fbuilder = new FormFieldBuilder("Total Amount",dototalamt);
			FormField fpayCompTotalAmt= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
			fbuilder = new FormFieldBuilder("",addOtherCharges);
			FormField faddOtherCharges= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("",chargesTable.getTable());
			FormField fchargesTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
			fbuilder = new FormFieldBuilder("Net Payable",donetpayamt);
			FormField fpayCompNetPay= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
			fbuilder = new FormFieldBuilder();
			FormField fgroupingShippingInfo=fbuilder.setlabel("Shipping Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
			fbuilder = new FormFieldBuilder("* Delivery Date",dbdeliverydate);
			FormField fdbdeliverydate= fbuilder.setMandatory(true).setMandatoryMsg("Delivery Date is Mandatory!").setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("Is Shipping Address Different",cbcheckaddress);
			FormField fcbcheckaddress= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("",acshippingcomposite);
			FormField facshippingcomposite=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
			fbuilder = new FormFieldBuilder("",baddproducts);
			FormField fbaddproducts= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("",prodInfoComposite);
			FormField fprodInfoComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
			fbuilder = new FormFieldBuilder("C Form",cbcformlis);
			FormField fcbcform= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("CST Percent",olbcstpercent);
			FormField folbcstpercent= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("",salesorderlineitemtable.getTable());
			FormField fsalesorderlineitemtable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
			
			
			fbuilder = new FormFieldBuilder("",tblOtherCharges.getTable());
			FormField fothChgTbl= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
			
			fbuilder = new FormFieldBuilder("",addOthrChargesBtn);
			FormField faddOthrChargesBtn= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			
			fbuilder = new FormFieldBuilder("Total Amount",dbOtherChargesTotal);
			FormField fdbOtherChargesTotal= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
			
			/**
			 * Date 08-06-2018 By vijay
			 * Des :- for final Total amt and roundoff amt
			 */
			fbuilder = new FormFieldBuilder("Total Amount",dbfinalTotalAmt);
			FormField fdbfinalTotalAmt = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
			
			fbuilder = new FormFieldBuilder("Round Off",tbroundoffAmt);
			FormField ftbroundoffAmt = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
			
			/**
			 * ends here
			 */
			
			
			/*
			 * Date:19/07/2018
			 * Developer:Ashwini
			 */
			FormField folbcNumberRange;
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("SalesOrder", "MakeNumberRangeMandatory")){
				fbuilder = new FormFieldBuilder("*Number Range",olbcNumberRange);
				folbcNumberRange= fbuilder.setMandatory(true).setMandatoryMsg("Number Range is Mandatory!").setRowSpan(0).setColSpan(0).build();
			}else{
				fbuilder = new FormFieldBuilder("Number Range",olbcNumberRange);
				folbcNumberRange= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			}
			
			/*
			 * Ashwini Patil
			 * Date:29-05-2024
			 * Ultima search want to map number range directly from branch so they want this field read only
			 */
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("NumberRange", "MakeNumberRangeReadOnly"))
				olbcNumberRange.setEnabled(false);
			/** date 3.10.2018 added by komal for po and grn id**/
			fbuilder = new FormFieldBuilder("PO Id",tbPOId);
			FormField ftbPOId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			
			fbuilder = new FormFieldBuilder("GRN Id",tbGRNId);
			FormField ftbGRNId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

			
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("SalesOrder", "RefrenceOrderNumber")){
				refOrderNo=true;
			}else{
				refOrderNo = false;
			}
			FormField fibRefOrderNo=null;
			if(refOrderNo){
			fbuilder = new FormFieldBuilder("Supplier's Ref./Order No.", ibRefOrderNo);
			 fibRefOrderNo = fbuilder.setMandatory(false).setMandatoryMsg("").setRowSpan(0).setColSpan(0).build();
			}else{
				fbuilder = new FormFieldBuilder();
				 fibRefOrderNo = fbuilder.setMandatory(false).setMandatoryMsg("").setRowSpan(0).setColSpan(0).build();
			}
			
			FormField fgroupingreferenceIfo=fbuilder.setlabel("Reference/General").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();

			
			fbuilder = new FormFieldBuilder("Customer Branch",oblCustomerBranch);
			FormField foblCustomerBranch= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			
			fbuilder = new FormFieldBuilder("POC Name",tbCustPocName);
			FormField ftbCustPocName= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			
			/*
			 * ends here
			 */
			
			fbuilder = new FormFieldBuilder("" , costingBtn);
			FormField fcostingBtn = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).setVisible(marginFlag).build();
			
			fbuilder = new FormFieldBuilder("Budget" , tbBudget);
			FormField ftbBudget = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			
			fbuilder = new FormFieldBuilder("Payment Mode",objPaymentMode);
			FormField fobjPaymentMode= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			
			
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

			////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////
			/**Date 4-2-2019 
			 * @author Amol
			 * added process Configuration  for reference order No TextBox
			 * 
			 */
		
			FormField[][] formfield ={ 
//					{fgroupingCustomerInformation},
//					{fpersonInfoComposite},
//					/***Date 18-5-2020 by Amol raised by Nitin Sir comment cform and cst percent****/
////					{ftbContractId ,fdbsalesorderdate ,fcbcform ,folbcstpercent},
//					{ftbContractId ,fdbsalesorderdate},
//					{fgroupingreferenceIfo},//added by komal
//					{ftbLeadId , ftbQuotatinId, ftbPOId ,ftbGRNId },
//					{ftbReferenceNumber,fdbReferenceDate,ftbRefeRRedBy,fibRefOrderNo},
//					{fgroupingDocuments},
//					{fucUploadTAndCs},
//					{fgroupingSalesInformation},
//					{ftbQuotationStatus,folbbBranch,folbeSalesPerson,folbApproverName},
//					{folbcPaymentMethods,folbContractGroup,folbContractCategory,folbContractType},
////					{fibCreditPeriod,ftbremark},//Commented by Ashwini
//					{fibCreditPeriod,folbcNumberRange,fdbVendorPrice,ftbremark},//Added by Ashwini
//					{fgroupingDescription},
//					{ftaDescription},
//					{ftaDescriptionTwo},
//					{fgroupingPaymentTermsDetails},
//					{fibdays,fdopercent,ftbcomment,faddPaymentTerms},
//					{fpaymentTermsTable},
//					{fgroupingProducts},
//					{fcostingBtn},
//					{fprodInfoComposite,fbaddproducts},
//					{fsalesorderlineitemtable},
//					{fblankgroupthree,fpayCompTotalAmt},
//					
//					{fblankgroup,faddOthrChargesBtn},
//					{fblankgroup,fothChgTbl},/** Date : 14-09-2017 BY ANIL**/
//					{fblankgroupthree,fdbOtherChargesTotal},
//					
//					
//					{fblankgroupthree},
//					{fblankgroup,fprodTaxTable},
////					{fblankgroup,faddOtherCharges,fdoAmtIncludingTax},/** Date : 20-09-2017 BY ANIL**/
////					{fblankgroup,fchargesTable},
//					
//					/*** Date 08-06-2018 By vijay **/
//					{fblankgroup,fblankgroupone,fdbfinalTotalAmt},
//					{fblankgroup,fblankgroupone,ftbroundoffAmt},
//					/*** ends here ****/
//					
//					{fblankgroup,fblankgroupone,fpayCompNetPay},
//					{fgroupingShippingInfo},
//					{foblCustomerBranch,ftbCustPocName,fdbdeliverydate,fcbcheckaddress},
//					{facshippingcomposite}
//			};
					/**Main Screen**/
					{fgroupingCustomerInformation},
					{fpersonInfoComposite},
					{fdbsalesorderdate,foblCustomerBranch,folbbBranch,folbeSalesPerson},
					{folbApproverName,ftbremark,ftbBudget,folbcNumberRange},
					/**Product**/
					{fgroupingProducts},
					{fcostingBtn},
					{fprodInfoComposite,fbaddproducts},
					{fsalesorderlineitemtable},
					{fblankgroupthree,fpayCompTotalAmt},
					{fblankgroup,faddOthrChargesBtn},
					{fblankgroup,fothChgTbl},
					{fblankgroupthree,fdbOtherChargesTotal},
					{fblankgroupthree},
					{fblankgroup,fprodTaxTable},
					{fblankgroup,fblankgroupone,fdbfinalTotalAmt},
					{fblankgroup,fblankgroupone,ftbroundoffAmt},
					{fblankgroup,fblankgroupone,fpayCompNetPay},
					
					/**Shipping Address**/
					{fgroupingShippingInfo},
					{ftbCustPocName,fdbdeliverydate,fcbcheckaddress},
					{facshippingcomposite},
					/**Payment Terms**/
					{fgroupingPaymentTermsDetails},
					{folbcPaymentMethods,fobjPaymentMode,fibCreditPeriod},
					{fibdays,fdopercent,ftbcomment,faddPaymentTerms},
					{fpaymentTermsTable},
					
					/**Classification**/
					{fgroupingSalesInformation},
					{folbContractGroup,folbContractCategory,folbContractType},
					/**General and Reference **/
					{fgroupingreferenceIfo},
					{ftbLeadId,ftbQuotatinId, ftbPOId ,ftbGRNId },
					{ftbReferenceNumber,fdbReferenceDate,ftbRefeRRedBy,fdbVendorPrice},
					{ftaDescription},
					{ftaDescriptionTwo},
					/**Attachment**/
					{fgroupingDocuments},
					{fucUploadTAndCs},	
					
			};

					
					
			this.fields=formfield;
			}	
			

			

//		}
		
		/**
		 * method template to update the model with token entity name
		 */
		@SuppressWarnings("unchecked")
		@Override
		public void updateModel(SalesOrder model) 
		{
			if(personInfoComposite.getValue()!=null)
				model.setCinfo(personInfoComposite.getValue());
			if(tbReferenceNumber.getValue()!=null)
				model.setRefNo(tbReferenceNumber.getValue());
			if(tbRefeRRedBy.getValue()!=null)
				model.setReferedBy(tbRefeRRedBy.getValue());
			if(dbReferenceDate.getValue()!=null){
				model.setReferenceDate(dbReferenceDate.getValue());
			}
			//if(validuntil.getValue()!=null)
				//model.setValidUntill(validuntil.getValue());
			   
				model.setDocument(ucUploadTAndCs.getValue());
			if(tbQuotationStatus.getValue()!=null)
				model.setStatus(tbQuotationStatus.getValue());
			/***2-2-2019 added by amol***/
			if (ibRefOrderNo.getValue() != null)
				model.setRefOrderNO(ibRefOrderNo.getValue());
			
			if(dbsalesorderdate.getValue()!=null)
				model.setSalesOrderDate(dbsalesorderdate.getValue());
			if(olbbBranch.getValue()!=null)
				model.setBranch(olbbBranch.getValue());
			if(olbeSalesPerson.getValue()!=null)
				model.setEmployee(olbeSalesPerson.getValue());
			if(olbcPaymentMethods.getValue()!=null)
				model.setPaymentMethod(olbcPaymentMethods.getValue());
			if(taDescription.getValue()!=null)
				model.setDescription(taDescription.getValue());
			
			if(taDescriptionTwo.getValue()!=null)
				model.setDescriptiontwo(taDescriptionTwo.getValue());
			
			if(dbVendorPrice.getValue()!=null)
				model.setVendorPrice(dbVendorPrice.getValue());
			
			
			if(olbSalesOrderType.getValue()!=null)
				model.setType(olbSalesOrderType.getValue(olbSalesOrderType.getSelectedIndex()));
			if(olbSalesOrderGroup.getValue()!=null)
				model.setGroup(olbSalesOrderGroup.getValue());
			if(olbSalesOrderCategory.getValue()!=null)
				model.setCategory(olbSalesOrderCategory.getValue());
			if(olbApproverName.getValue()!=null)
				model.setApproverName(olbApproverName.getValue());
			if(dototalamt.getValue()!=null)
				model.setTotalAmount(dototalamt.getValue());
			if(donetpayamt.getValue()!=null)
				model.setNetpayable(donetpayamt.getValue());
			if(!tbLeadId.getValue().equals(""))
				model.setLeadCount(Integer.parseInt(tbLeadId.getValue()));
			if(!tbQuotatinId.getValue().equals(""))
				model.setQuotationCount(Integer.parseInt(tbQuotatinId.getValue()));
			if(!tbContractId.getValue().equals(""))
				model.setContractCount(Integer.parseInt(tbContractId.getValue()));
			
			if(ibCreditPeriod.getValue()!=null){
				model.setCreditPeriod(ibCreditPeriod.getValue());
			}
			
			if(cbcformlis.getSelectedIndex()!=0){
				model.setCformstatus(cbcformlis.getItemText(cbcformlis.getSelectedIndex()));
			}
			
			if(olbcstpercent.getValue()!=null){
				TaxDetails cstPerc=olbcstpercent.getSelectedItem();
				model.setCstpercent(cstPerc.getTaxChargePercent());
			}
			
			if(olbcstpercent.getValue()!=null){
				model.setCstName(olbcstpercent.getValue());
			}
			
			List<SalesLineItem> saleslis=this.getSalesorderlineitemtable().getDataprovider().getList();
			ArrayList<SalesLineItem> arrItems=new ArrayList<SalesLineItem>();
			arrItems.addAll(saleslis);
			model.setItems(arrItems);
			
			List<PaymentTerms> payTermsLis=this.paymentTermsTable.getDataprovider().getList();
			ArrayList<PaymentTerms> payTermsArr=new ArrayList<PaymentTerms>();
			payTermsArr.addAll(payTermsLis);
			model.setPaymentTermsList(payTermsArr);
			
			List<ProductOtherCharges> productTaxLis=this.prodTaxTable.getDataprovider().getList();
			ArrayList<ProductOtherCharges> productTaxArr=new ArrayList<ProductOtherCharges>();
			productTaxArr.addAll(productTaxLis);
			model.setProductTaxes(productTaxArr);
			
			/**
			 * Date : 20-09-2017 BY ANIL
			 */
			List<OtherCharges> otherChargesList=this.tblOtherCharges.getDataprovider().getList();
			ArrayList<OtherCharges> otherChargesArr=new ArrayList<OtherCharges>();
			otherChargesArr.addAll(otherChargesList);
			model.setOtherCharges(otherChargesArr);
			
			/**
			 * End
			 */
			
			
			List<ProductOtherCharges> productChargeLis=this.chargesTable.getDataprovider().getList();
			ArrayList<ProductOtherCharges> productChargeArr=new ArrayList<ProductOtherCharges>();
			productChargeArr.addAll(productChargeLis);
			model.setProductCharges(productChargeArr);
			
			if(dbdeliverydate.getValue()!=null)
				model.setDeliveryDate(dbdeliverydate.getValue());
//			if(cbcheckaddress.getValue()!=null){
		
				model.setCheckAddress(cbcheckaddress.getValue());
			
//			}
			if(acshippingcomposite.getValue()!=null)
				model.setShippingAddress(acshippingcomposite.getValue());
			
			/** Date 18-sep-2017 added by vijay becs this total amt showing only screen now i am saving it into database ***********/
			if(doamtincltax.getValue()!=null){
				model.setInclutaxtotalAmount(doamtincltax.getValue());
			}
			
			/**
			 * Date 08/06/2018
			 * Developer :- Vijay
			 * Des :- for final total amt Round off amt 
			 */
			  if(dbfinalTotalAmt.getValue()!=null){
				  model.setTotalFinalAmount(dbfinalTotalAmt.getValue());;
			  }
			  if(tbroundoffAmt.getValue()!=null && !tbroundoffAmt.getValue().equals("")){
				  model.setRoundOffAmount(Double.parseDouble(tbroundoffAmt.getValue()));
			  }
			/**
			 * ends here
			 */
			  
			  /*
			   * Date:19/07/2018
			   * Developer:Ashini
			   * Des:for adding Number Range in Saled order	  
			   */
					  if(olbcNumberRange.getValue()!=null){
						  model.setNumberRange(olbcNumberRange.getValue(olbcNumberRange.getSelectedIndex()));
					  }else{
						  /** date 9.10.2018 added by komal**/
						  model.setNumberRange("");
					  }
					  
			 /*
			  * End by Ashwini
			  */
		
			if(oblCustomerBranch.getValue()!=null){
				model.setCustBranch(oblCustomerBranch.getValue());
			}else{
				model.setCustBranch("");
			}
			if(tbCustPocName.getValue()!=null){
				model.setCustPocName(tbCustPocName.getValue());
			}
			
			if(marginFlag){
				if(vendorMargins==null){
					vendorMargins=new ArrayList<VendorMargin>();
				}
				if(otherChargesMargins==null){
					otherChargesMargins=new ArrayList<OtherChargesMargin>();
				}
				marginPopup.reactOnCostingSave(salesorderlineitemtable.getValue(),vendorMargins,otherChargesMargins);
				totalVendorMargin=marginPopup.dbTotalVendorMargin.getValue();
				totalOcMargin=marginPopup.dbOtherChargesMargin.getValue();
				
				Console.log("Vendor "+vendorMargins.size());
				Console.log("Other "+otherChargesMargins.size());
			}
			if(vendorMargins!=null){
				model.setVendorMargins(vendorMargins);
			}
			if(otherChargesMargins!=null){
				model.setOtherChargesMargins(otherChargesMargins);
			}
			if(totalVendorMargin!=null){
				model.setTotalVendorMargin(totalVendorMargin);
			}
			if(totalOcMargin!=null){
				model.setTotalOtherChargesMargin(totalOcMargin);
			}
			
			if(billingAddress!=null){
				model.setNewcustomerAddress(billingAddress);
			}
			  
			if(customerBillingAddress!=null){
				model.setNewcustomerAddress(customerBillingAddress);
			}
			if(customerServiceAddress!=null){
				model.setShippingAddress(customerServiceAddress);
			}
			
			model.setBillType(billingType);
			if(tbBudget.getValue()!=null&&!tbBudget.getValue().equals("")){
				model.setBudget(Double.parseDouble(tbBudget.getValue()));
			}
			model.setCostCode(costCode);
			model.setCarpetArea(carpetArea);
			
			if(objPaymentMode.getSelectedIndex()!=0){
				model.setPaymentMode(objPaymentMode.getValue(objPaymentMode.getSelectedIndex()));
			}else{
				model.setPaymentMode("");
				
			}
			
			salesorderobj=model;
			presenter.setModel(model);

		}

		/**
		 * method template to update the view with token entity name
		 */
		@SuppressWarnings("unchecked")
		@Override
		public void updateView(SalesOrder view) 
		{
			salesorderobj=view;
			/**
			 * Date 20-04-2018 By vijay for orion pest locality load  
			 */
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Locality", "DontLoadAllLocalityAndCity")){
				setAddressDropDown();
			}
			/**
			 * ends here
			 */
			
	        	tbContractId.setValue(view.getCount()+"");
	        	
	        	this.checkCustomerStatus(view.getCinfo().getCount());
	        	
			if(view.getCinfo()!=null)
				personInfoComposite.setValue(view.getCinfo());
			if(view.getRefNo()!=null)
				tbReferenceNumber.setValue(view.getRefNo());
			if(view.getReferedBy()!=null)
				tbRefeRRedBy.setValue(view.getReferedBy());
			if(view.getReferenceDate()!=null)
				dbReferenceDate.setValue(view.getReferenceDate());
			if(view.getSalesOrderDate()!=null)
				dbsalesorderdate.setValue(view.getSalesOrderDate());
			if(view.getDocument()!=null)
				ucUploadTAndCs.setValue(view.getDocument());
			if(view.getStatus()!=null)
				tbQuotationStatus.setValue(view.getStatus());
			if(view.getBranch()!=null)
				olbbBranch.setValue(view.getBranch());
			if(view.getEmployee()!=null)
				olbeSalesPerson.setValue(view.getEmployee());
			if(view.getPaymentMethod()!=null)
				olbcPaymentMethods.setValue(view.getPaymentMethod());
			if(view.getDescription()!=null)
				taDescription.setValue(view.getDescription());
			
			if(view.getDescriptiontwo()!=null)
				taDescriptionTwo.setValue(view.getDescriptiontwo());
			

			if(view.getVendorPrice()!=0)
				dbVendorPrice.setValue(view.getVendorPrice());
			
			/*if(view.getPayments()!=null)
				paymenttable.setValue(view.getPayments());*/
			if(view.getCategory()!=null)
				olbSalesOrderCategory.setValue(view.getCategory());
			if(view.getType()!=null)
				olbSalesOrderType.setValue(view.getType());
			if(view.getGroup()!=null)
				olbSalesOrderGroup.setValue(view.getGroup());
			if(view.getApproverName()!=null)
				olbApproverName.setValue(view.getApproverName());
/****2-2-2019 added by amol***/
			ibRefOrderNo.setValue(view.getRefOrderNO());
			if(view.getCreditPeriod()!=null){
				ibCreditPeriod.setValue(view.getCreditPeriod());
			}
			
			if(view.getLeadCount()!=-1)
				tbLeadId.setValue(view.getLeadCount()+"");
			if(view.getQuotationCount()!=-1)
				tbQuotatinId.setValue(view.getQuotationCount()+"");

				
			if(view.getDeliveryDate()!=null)
				dbdeliverydate.setValue(view.getDeliveryDate());
//			if(view.getCheckAddress()!=null)
				cbcheckaddress.setValue(view.getCheckAddress());
			if(view.getShippingAddress()!=null)
				acshippingcomposite.setValue(view.getShippingAddress());
			salesorderlineitemtable.setValue(view.getItems());
			if(view.getCformstatus()!=null)
			{
				for(int i=0;i<cbcformlis.getItemCount();i++)
				{
					String data=cbcformlis.getItemText(i);
					if(data.equals(view.getCformstatus()))
					{
						cbcformlis.setSelectedIndex(i);
						break;
					}
				}
			}
			
			if(view.getRemark()!=null){
				tbremark.setValue(view.getRemark());
			}
			
			olbcstpercent.setValue(view.getCstName());
			paymentTermsTable.setValue(view.getPaymentTermsList());
			
			
			/**
			 * Date : 20-09-2017 BY ANIL
			 */
			tblOtherCharges.setValue(view.getOtherCharges());
			/**
			 * End
			 */
//			dbOtherChargesTotal.setValue(view.getTotalAmt());
			
			if (view.getOtherCharges() != null) {
				double sumOfOtherCharges = 0;
				for (OtherCharges object : this.tblOtherCharges.getDataprovider().getList()) {
					sumOfOtherCharges = sumOfOtherCharges + object.getAmount();
				}
				dbOtherChargesTotal.setValue(sumOfOtherCharges);
			}
			
			
			
			
			
			prodTaxTable.setValue(view.getProductTaxes());
			
			
			
			chargesTable.setValue(view.getProductCharges());
			dototalamt.setValue(view.getTotalAmount());
			donetpayamt.setValue(view.getNetpayable());
			
			
			doamtincltax.setValue(view.getInclutaxtotalAmount());


			/**
			 * Date 08/06/2018
			 * Developer :- Vijay
			 * Des :- for final total amt Round off amt 
			 */
			
			if(view.getTotalFinalAmount()!=0){
				dbfinalTotalAmt.setValue(view.getTotalFinalAmount());
			}
			if(view.getRoundOffAmount()!=0){
				tbroundoffAmt.setValue(view.getRoundOffAmount()+"");
			}
			/**
			 * ends here
			 */
			
			/* 
			 * for approval process
			 *  nidhi
			 *  5-07-2017
			 */

			/*
			 * Date:19/07/2018
			 * Developer:Ashwini
			 * for number range 
			 */
			
			if(view.getNumberRange()!=null)
				olbcNumberRange.setValue(view.getNumberRange());
			/*
			 * End by Ashwini
			 */
			/** date 3.10.2018 added by komal for grn and po id**/
			if(view.getPoId() != 0){
				tbPOId.setValue(view.getPoId()+"");
			}
			if(view.getGrnId() != 0){
				tbGRNId.setValue(view.getGrnId()+"");
			}
			
			loadCustomerBranch(view.getCustBranch(),view.getCinfo().getCount());
			if(view.getCustPocName()!=null){
				tbCustPocName.setValue(view.getCustPocName());
			}
			
			if(view.getVendorMargins()!=null){
				vendorMargins=view.getVendorMargins();
			}
			if(view.getOtherChargesMargins()!=null){
				otherChargesMargins=view.getOtherChargesMargins();
			}
			totalVendorMargin=view.getTotalOtherChargesMargin();
			totalOcMargin=view.getTotalOtherChargesMargin();
			
			if(view.getNewcustomerAddress()!=null){
				billingAddress=view.getNewcustomerAddress();
			}
			
			if(view.getBudget()!=0){
				tbBudget.setValue(view.getBudget()+"");
			}
			billingType=view.getBillType();
			costCode=view.getCostCode();
			carpetArea=view.getCarpetArea();
			
			if(view.getPaymentMode()!=null){
            	objPaymentMode.setValue(view.getPaymentMode());
            }
			
			if(presenter != null){
				presenter.setModel(view);
			}
			/*
			 *  end
			 */
			
			
		}

		/**
		 * @author Anil , Date : 16-04-2019
		 * @param custBranch
		 * @param custId
		 * this method is used to load customer branches and set value if viewing the doc
		 */
		public void loadCustomerBranch(final String custBranch,int custId) {
			// TODO Auto-generated method stub
			GenricServiceAsync async=GWT.create(GenricService.class);
			MyQuerry querry=new MyQuerry();
			Filter temp=null;
			Vector<Filter> vecList=new Vector<Filter>();
			
			temp=new Filter();
			temp.setQuerryString("cinfo.count");
			temp.setIntValue(custId);
			vecList.add(temp);
			
			temp=new Filter();
			temp.setQuerryString("status");
			temp.setBooleanvalue(true);
			vecList.add(temp);
			
			querry.setFilters(vecList);
			querry.setQuerryObject(new CustomerBranchDetails());
			async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {

				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
				}

				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					// TODO Auto-generated method stub
					oblCustomerBranch.clear();
					oblCustomerBranch.addItem("--SELECT--");
//					tbCustPocName.setValue("");
					if(result!=null&&result.size()!=0){
						ArrayList<CustomerBranchDetails>custBranchList=new ArrayList<CustomerBranchDetails>();
						for(SuperModel model:result){
							CustomerBranchDetails obj=(CustomerBranchDetails) model;
							custBranchList.add(obj);
						}
						
						oblCustomerBranch.setListItems(custBranchList);
						if(custBranch!=null){
							oblCustomerBranch.setValue(custBranch);
						}
					}
				}
			});
			
		}

		/**
		 * Date 20-04-2018 By vijay
		 * here i am refreshing address composite data if global list size greater than drop down list size
		 * because we are adding locality and city from an entity in view state to global list if doest not exist in global list 
		 */
		private void setAddressDropDown() {
			if(LoginPresenter.globalLocality.size()>acshippingcomposite.locality.getItems().size()){
				acshippingcomposite.locality.setListItems(LoginPresenter.globalLocality);
			}
		}
		/**
		 * ends here 
		 */
		
		/**
		 * Toggles the app header bar menus as per screen state
		 */

		public void toggleAppHeaderBarMenu() {
			if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				/**
				 * @author Anil @since 10-12-2021
				 */
				if(isPopUpAppMenubar()){
					 menus=GeneralViewDocumentPopup.applevelMenu;
				}
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.contains("Save")||text.contains("Discard")||text.contains("Search")||text.contains(AppConstants.NAVIGATION))
					{
						menus[k].setVisible(true); 
					}
					else
						menus[k].setVisible(false);  		   
					
				}
			}

			else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				/**
				 * @author Anil @since 10-12-2021
				 */
				if(isPopUpAppMenubar()){
					 menus=GeneralViewDocumentPopup.applevelMenu;
				}
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.contains("Save")||text.contains("Discard")||text.contains(AppConstants.NAVIGATION)|| text.contains(AppConstants.COMMUNICATIONLOG))
						menus[k].setVisible(true); 
					
					else
						menus[k].setVisible(false);  		   
				}
			}

			else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				/**
				 * @author Anil @since 10-12-2021
				 */
				if(isPopUpAppMenubar()){
					 menus=GeneralViewDocumentPopup.applevelMenu;
				}
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.contains("Edit")||text.contains("Discard")||text.contains("Search")||text.contains("Print")||text.contains(AppConstants.NAVIGATION)|| text.contains(AppConstants.COMMUNICATIONLOG))
							menus[k].setVisible(true); 
					
					else
						menus[k].setVisible(false);  		   
				}
			}
			AuthorizationHelper.setAsPerAuthorization(Screen.SALESORDER,LoginPresenter.currentModule.trim());
		}

	/**
	 * Toogels the Application Process Level Menu as Per Application State
	 */
		public void toggleProcessLevelMenu()
		{
			SalesOrder entity=(SalesOrder) presenter.getModel();
			
			String status=entity.getStatus();

			for(int i=0;i<getProcesslevelBarNames().length;i++)
			{
				InlineLabel label=getProcessLevelBar().btnLabels[i];
				String text=label.getText().trim();

				if(status.equals(SalesOrder.CREATED))
				{
					if(getManageapproval().isSelfApproval())
					{  
						if(text.equals(ManageApprovals.APPROVALREQUEST))
							label.setVisible(false);
						if(text.equals(ManageApprovals.SUBMIT))
							label.setVisible(true);
					}
					else
					{  
						if(text.equals(ManageApprovals.APPROVALREQUEST))
							label.setVisible(true);
						if(text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
							label.setVisible(false);
						if(text.equals(ManageApprovals.SUBMIT))
							label.setVisible(false);
					}
					if((text.contains(AppConstants.CANCLESALESORDER)))
						label.setVisible(false);
					if((text.contains(AppConstants.CANCELLATIONSUMMARY)))
						label.setVisible(false);
					if((text.contains(AppConstants.WORKORDER)))
						label.setVisible(false);
					
					/**
					 * Date : 05-08-2017 By ANIL
					 */
					if(text.equals(AppConstants.VIEWBILL)){
						label.setVisible(false);
					}
					/**
					 * End
					 */
					
					/**
					 * Date : 19-04-2018 BY ANIL
					 */
					if(text.equals(AppConstants.VIEWAMC)){
						label.setVisible(false);
					}
					if(text.equals(AppConstants.CREATEPO)){
						label.setVisible(false);
					}
					if(text.equals(AppConstants.VIEWPO)){
						label.setVisible(false);
					}
					/**
					 * End
					 */
					if(text.equals(AppConstants.CUSTOMERADDRESS)){
						label.setVisible(true);
					}
//					if(text.equals("Copy Order")){
//						label.setVisible(false);
//					}
					if(text.equals(AppConstants.COPY)) {
						label.setVisible(true);
					}
					
					// Added by Priyanka Des : Delivery note option from salesorder form.
					if(text.equals("View Delivery Note")){
						label.setVisible(false);
					}
					
					if(text.equals(AppConstants.REGISTEREXPENSE)){
						label.setVisible(false);
					}
					if(text.equals(AppConstants.VIEWEXPENSES)){
						label.setVisible(false);
					}
					
					if(text.equals(AppConstants.VIEWINVOICEPAYMENT)){
						label.setVisible(false);
					}
					
				}
				else if(status.equals(SalesOrder.APPROVED))
				{
					if(text.equals(AppConstants.Approve))
						label.setVisible(false);	
//					if((text.contains(AppConstants.CANCLESALESORDER)))
//						label.setVisible(true);
					
					/**
					 * rohan added this code for NBHC Changes ADMIN role should  
					 */
					if(LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN")){
						if((text.contains(AppConstants.CANCLESALESORDER)))
							label.setVisible(true);
					}
					else
					{
						if((text.contains(AppConstants.CANCLESALESORDER)))
							label.setVisible(false);
					}
					
					if((text.contains(AppConstants.CANCELLATIONSUMMARY)))
						label.setVisible(false);
					if((text.contains(AppConstants.WORKORDER)))
						label.setVisible(true);
					if(text.equals(ManageApprovals.SUBMIT))
						label.setVisible(false);
					/**
					 * Date : 05-08-2017 By ANIL
					 */
					if(text.equals(AppConstants.VIEWBILL)){
						label.setVisible(true);
					}
					if(text.equals(AppConstants.CUSTOMERADDRESS)){
						label.setVisible(true);
					}
					/**
					 * End
					 */
					
					/**
					 * Date : 19-04-2018 BY ANIL
					 */
					if(text.equals(AppConstants.VIEWAMC)){
						label.setVisible(true);
					}
					if(text.equals(AppConstants.CREATEPO)){
						label.setVisible(true);
					}
					if(text.equals(AppConstants.VIEWPO)){
						label.setVisible(true);
					}
//					if(text.equals("Copy Order")){
//						label.setVisible(false);
//					}
					/**
					 * End
					 */
					
					// Added by Priyanka Des : Delivery note option from salesorder form.
					if(text.equals("View Delivery Note")){
						label.setVisible(true);
					}
					
					if(text.equals(AppConstants.COPY)) {
						label.setVisible(true);
					}
					
					if(text.equals(AppConstants.REGISTEREXPENSE)){
						label.setVisible(true);
					}
					if(text.equals(AppConstants.VIEWEXPENSES)){
						label.setVisible(true);
					}
					if(text.equals(AppConstants.VIEWINVOICEPAYMENT)){
						label.setVisible(true);
					}
				}

				else if(status.equals(SalesOrder.SALESORDERCANCEL))
				{
					if((text.contains(AppConstants.NEW)))
						label.setVisible(true);
					else
						label.setVisible(false);
					
					if((text.contains(AppConstants.CANCELLATIONSUMMARY)))
						label.setVisible(true);
					/** date 10-02-2018 commented by  to show cancel sales order **/
//					if((text.contains(AppConstants.CANCLESALESORDER)))
//						label.setVisible(false);
					/** date 10-02-2018 added by komal to show cancel sales order **/
					if((text.contains(AppConstants.CANCLESALESORDER)))
						label.setVisible(true);
					if(text.equals(ManageApprovals.SUBMIT))
						label.setVisible(false);
					/**
					 * Date : 05-08-2017 By ANIL
					 */
					if(text.equals(AppConstants.VIEWBILL)){
						label.setVisible(false);
					}
					/**
					 * End
					 */
					
					/**
					 * Date : 19-04-2018 BY ANIL
					 */
					if(text.equals(AppConstants.VIEWAMC)){
						label.setVisible(false);
					}
					if(text.equals(AppConstants.CREATEPO)){
						label.setVisible(false);
					}
					if(text.equals(AppConstants.VIEWPO)){
						label.setVisible(false);
					}
//					if(text.equals("Copy Order")){
//						label.setVisible(true);
//					}
					/**
					 * End
					 */
					
					// by Priyanka
					if(text.equals("View Delivery Note")){
						label.setVisible(true);
					}
					if(text.equals(AppConstants.COPY)) {
						label.setVisible(true);
					}
					
					if(text.equals(AppConstants.REGISTEREXPENSE)){
						label.setVisible(false);
					}
					if(text.equals(AppConstants.VIEWEXPENSES)){
						label.setVisible(false);
					}
					if(text.equals(AppConstants.VIEWINVOICEPAYMENT)){
						label.setVisible(true);
					}
				}
				
				else if(status.equals(SalesOrder.REQUESTED))
				{
					if((text.contains(ManageApprovals.APPROVALREQUEST)))
						label.setVisible(false);
					else
						label.setVisible(true);
					
					if((text.contains(AppConstants.CANCLESALESORDER)))
						label.setVisible(false);
					if((text.contains(AppConstants.CANCELLATIONSUMMARY)))
						label.setVisible(false);
					if((text.contains(AppConstants.WORKORDER)))
						label.setVisible(false);
					if(text.equals(ManageApprovals.SUBMIT))
						label.setVisible(false);
					/**
					 * Date : 05-08-2017 By ANIL
					 */
					if(text.equals(AppConstants.VIEWBILL)){
						label.setVisible(false);
					}
					/**
					 * End
					 */
					
					/**
					 * Date : 19-04-2018 BY ANIL
					 */
					if(text.equals(AppConstants.VIEWAMC)){
						label.setVisible(false);
					}
					if(text.equals(AppConstants.CREATEPO)){
						label.setVisible(false);
					}
					if(text.equals(AppConstants.VIEWPO)){
						label.setVisible(false);
					}
					if(text.equals(AppConstants.CUSTOMERADDRESS)){
						label.setVisible(true);
					}
					/**
					 * End
					 */
//					if(text.equals("Copy Order")){
//						label.setVisible(false);
//					}
					
					// by Priyanka
					if(text.equals("View Delivery Note")){
						label.setVisible(false);
					}
					
					if(text.equals(AppConstants.COPY)) {
						label.setVisible(true);
					}
					
					if(text.equals(AppConstants.REGISTEREXPENSE)){
						label.setVisible(false);
					}
					if(text.equals(AppConstants.VIEWEXPENSES)){
						label.setVisible(false);
					}
					if(text.equals(AppConstants.VIEWINVOICEPAYMENT)){
						label.setVisible(false);
					}
				}
				
				else if(status.equals(SalesOrder.REJECTED))
				{
					if(text.contains(AppConstants.NEW))
						label.setVisible(true);
					else
						label.setVisible(false);
					
					if((text.contains(AppConstants.CANCELLATIONSUMMARY)))
						label.setVisible(false);
					if((text.contains(AppConstants.CANCLESALESORDER)))
						label.setVisible(false);
					if(text.equals(ManageApprovals.SUBMIT))
						label.setVisible(false);
					/**
					 * Date : 05-08-2017 By ANIL
					 */
					if(text.equals(AppConstants.VIEWBILL)){
						label.setVisible(false);
					}
					/**
					 * End
					 */
					
					/**
					 * Date : 19-04-2018 BY ANIL
					 */
					if(text.equals(AppConstants.VIEWAMC)){
						label.setVisible(false);
					}
					if(text.equals(AppConstants.CREATEPO)){
						label.setVisible(false);
					}
					if(text.equals(AppConstants.VIEWPO)){
						label.setVisible(false);
					}
					/**
					 * End
					 */
//					if(text.equals("Copy Order")){
//						label.setVisible(false);
//					}
					
					// by Priyanka
					if(text.equals("View Delivery Note")){
						label.setVisible(false);
					}
					if(text.equals(AppConstants.COPY)) {
						label.setVisible(true);
					}
					if(text.equals(AppConstants.REGISTEREXPENSE)){
						label.setVisible(false);
					}
					if(text.equals(AppConstants.VIEWEXPENSES)){
						label.setVisible(false);
					}
					if(text.equals(AppConstants.VIEWINVOICEPAYMENT)){
						label.setVisible(false);
					}
				}
				
				
				if(marginFlag){
					if(entity.getStatus().equals("Approved")){
						if(text.equals("Sales Order Margin")){
							label.setVisible(true);
						}
						if(text.equals("View Delivery Note")){
							label.setVisible(true);
						}
					}else{
						if(text.equals("Sales Order Margin")){
							label.setVisible(false);
						}
						if(text.equals("View Delivery Note")){
							label.setVisible(true);
						}
					}
				}else{
					if(text.equals("Sales Order Margin")){
						label.setVisible(false);
					}
					if(text.equals("View Delivery Note")){
						label.setVisible(true);
					}
				}
				
				
              //Ashwini Patil Date:20-09-2022
    			if(isHideNavigationButtons()) {
    				Console.log("in changeProcessLevel isHideNavigationButtons");
    				if(text.equalsIgnoreCase(AppConstants.NEW)
    						||text.equalsIgnoreCase(AppConstants.WORKORDER)
    						||text.equalsIgnoreCase("Create AMC")
    						||text.equalsIgnoreCase(AppConstants.VIEWAMC)
    						||text.equalsIgnoreCase(AppConstants.CREATEPO)
    						||text.equalsIgnoreCase(AppConstants.VIEWPO)
    						||text.equalsIgnoreCase(AppConstants.VIEWBILL)
    						||text.equalsIgnoreCase("View Delivery Note")
    						||text.equalsIgnoreCase(AppConstants.COPY)){
    					label.setVisible(false);	
    				}
    			}
			}
		}
		/**
		 * Sets the Application Header Bar as Per Status
		 */

		public void setAppHeaderBarAsPerStatus()
		{
			SalesOrder entity=(SalesOrder) presenter.getModel();
			String status=entity.getStatus();
			
			if(status.equals(SalesOrder.SALESORDERCANCEL)||status.equals(SalesOrder.REJECTED)||status.equals(SalesOrder.APPROVED))
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.contains("Discard")||text.contains("Search")
							||text.contains("Print")||text.contains(AppConstants.NAVIGATION)||text.contains(AppConstants.COMMUNICATIONLOG))
					{
						menus[k].setVisible(true); 
						System.out.println("Value of text is "+menus[k].getText());
					}
					else
					{
						menus[k].setVisible(false);  
					}
				}
			}

			if(status.equals(SalesOrder.APPROVED))
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.contains("Discard")||text.contains("Search")||text.contains("Email")
							||text.contains("Print")||text.contains(AppConstants.NAVIGATION)||text.contains(AppConstants.COMMUNICATIONLOG))
					{
						menus[k].setVisible(true); 

					}
					else
					{
						menus[k].setVisible(false);  
					}
				}
			}
			
			if(status.equals(SalesOrder.REQUESTED))
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.contains("Discard")||text.contains("Search")||text.contains("Print")||text.contains(AppConstants.NAVIGATION)||text.contains(AppConstants.COMMUNICATIONLOG))
					{
						menus[k].setVisible(true); 
					}
					else
					{
						menus[k].setVisible(false);  
					}
				}
			}
			
			if(status.equals(SalesOrder.REJECTED))
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.contains("Discard")||text.contains("Search")||text.contains("Print")||text.contains("Edit")||text.contains(AppConstants.NAVIGATION)||text.contains(AppConstants.COMMUNICATIONLOG))
					{
						menus[k].setVisible(true); 
					}
					else
					{
						menus[k].setVisible(false);  
					}
				}
			}
		}
		
		
		/**
		 * The method is responsible for changing Application state as per 
		 * status
		 */
		public void setMenuAsPerStatus()
		{
			this.setAppHeaderBarAsPerStatus();
			this.toggleProcessLevelMenu();
		}
 
		
		
		
		
		/**
		 * sets the id textbox with the passed count value. 
		 */
		@Override
		public void setCount(int count)
		{
			tbContractId.setValue(count+"");
		}
		
		/****************************************On Click Mehtod*******************************************/
		/**
		 * This method is called on click of any widget.
		 * 
		 * 	Involves Payment Terms Button  (ADD)
		 *  Involves Add Charges Button (Plus symbol)
		 * 	Involves OtmComposite i.e Products which are added
		 */
		
		
		
		@Override
		public void onClick(ClickEvent event) 
		{
			if(event.getSource().equals(addPaymentTerms)){
				reactOnAddPaymentTerms();
			}
			if(event.getSource().equals(addOtherCharges)){
				int size=chargesTable.getDataprovider().getList().size();
				if(this.doamtincltax.getValue()!=null){
					ProductOtherCharges prodCharges=new ProductOtherCharges();
					prodCharges.setAssessableAmount(this.getDoamtincltax().getValue());
					prodCharges.setFlagVal(false);
					prodCharges.setIndexCheck(size+1);
					this.chargesTable.getDataprovider().getList().add(prodCharges);
				}
			}
			if(event.getSource().equals(baddproducts)){
				boolean addressFlag=true;
				if(personInfoComposite.getId().getValue().equals("")){
					showDialogMessage("Please add customer information!");
				}
				
				if(acshippingcomposite.getState().getSelectedIndex()==0&&acshippingcomposite.getCountry().getSelectedIndex()==0&&acshippingcomposite.getCity().getSelectedIndex()==0)
				{
					addressFlag=false;
					showDialogMessage("Please select shipping address!");
				}
				
				/**
				 * Date 12 july 2017 added by vijay for GST and C form manage
				 */
				if(dbsalesorderdate.getValue()==null){
					showDialogMessage("Please add Sales Order date first");
					return;
				}
				/**
				 * ends here
				 */
				/** date 22.11.2018 added by komal for by default selection of IGST **/
				if(olbbBranch.getSelectedIndex() == 0){
					showDialogMessage("Please select Branch.");
					return;
				}
				if(!prodInfoComposite.getProdCode().getValue().equals("")&&!personInfoComposite.getId().getValue().equals("")){
					/**
					 * Date 03-05-2018 By Vijay 
					 * for duplicate product allow to add
					 */
//					boolean productValidation=validateProductDuplicate(prodInfoComposite.getProdCode().getValue().trim());
//					if(productValidation==true){
//						showDialogMessage("Product already exists!");
//					}
//					else{
						if(validateState()&&addressFlag==true){
							this.ProdctType(this.prodInfoComposite.getProdCode().getValue().trim(),this.prodInfoComposite.getProdID().getValue().trim());
						}
						if(!validateState()&&validateCForm()&&addressFlag==true){
							this.ProdctType(this.prodInfoComposite.getProdCode().getValue().trim(),this.prodInfoComposite.getProdID().getValue().trim());
						}
//					}
				}
			}
			
			
			/**
			 * Date : 14-09-2017 By ANIL
			 */
				
			if(event.getSource().equals(addOthrChargesBtn)){
				Tax tax1=new Tax();
				Tax tax2=new Tax();
				OtherCharges prodCharges=new OtherCharges();
				prodCharges.setOtherChargeName("");
				prodCharges.setAmount(0);
				prodCharges.setTax1(tax1);
				prodCharges.setTax2(tax2);
				this.tblOtherCharges.getDataprovider().getList().add(prodCharges);
			}
			/**
			 * End
			 */
			
			if(event.getSource().equals(costingBtn)){
				marginPopup.reactOnCosting(salesorderlineitemtable.getValue(),vendorMargins,otherChargesMargins);
			}
			
			if(event.getSource().equals(marginPopup.getLblOk())){
				
				vendorMargins=marginPopup.vendorMarginTbl.getValue();
				otherChargesMargins=marginPopup.otherChargesMarginTbl.getValue();
				totalVendorMargin=marginPopup.dbTotalVendorMargin.getValue();
				totalOcMargin=marginPopup.dbOtherChargesMargin.getValue();
				marginPopup.hidePopUp();
			}
			
			if(event.getSource().equals(marginPopup.getLblCancel())){
				marginPopup.hidePopUp();
			}
		}
		
		public boolean validateCForm()
		{
			if(!validateState()&&this.getCbcformlis().getSelectedIndex()==0){
				showDialogMessage("Please select C Form");
				return false;
			}
			if(!validateState()&&this.getCbcformlis().getItemText(getCbcformlis().getSelectedIndex()).equals(AppConstants.YES)){
				if(this.getOlbcstpercent().getSelectedIndex()==0){
					showDialogMessage("Please select CST Percent");
					return false;
				}
				else{
					return true;
				}
			}
			return true;
		}
		
		public boolean validateState()
		{
			/** Date 12 july 2017 added by vijay for GST
			 * below if condition code is applicable only if Sales order date is less than 1 july 2017 for Cform
			 * below if condition added by vijay and else block for no need this state validation for 1 july 2017 after sales order
			 */
			
			if(this.dbsalesorderdate.getValue()!=null && this.dbsalesorderdate.getValue().before(date)){
				if(this.getAcshippingcomposite().getState().getValue().trim().equals(SalesOrderPresenter.companyStateVal.trim())){
					return true;
				}
				return false;
			
			}else{
				return true;	
			}
		}

		
		@Override
		public TextBox getstatustextbox() {
			
			return this.tbQuotationStatus;
		}

		/******************************React On Add Payment Terms Method*******************************/
		/**
		 * This method is called when add button of payment terms is clicked.
		 * This method involves the inclusion of payment terms in the corresponding table
		 */
		
		public void reactOnAddPaymentTerms()
		{
			int payTrmDays=0;
			double payTrmPercent=0;
			String payTrmComment="";
			boolean flag=true;
			boolean payTermsFlag=true;
			
			if(tbcomment.getValue()==null||tbcomment.getValue().equals(""))
			{
				this.showDialogMessage("Comment cannot be empty!");
			}
			
			boolean checkTermDays=this.validatePaymentTrmDays(ibdays.getValue());
			if(checkTermDays==false)
			{
				if(chk==0){
					showDialogMessage("Days already defined.Please enter unique days!");
				}
//				clearPaymentTermsFields();
				flag=false;
			}
			
			if(ibdays.getValue()!=null&&ibdays.getValue()>=0){
				payTrmDays=ibdays.getValue();
			}
			else{
				showDialogMessage("Days cannot be empty!");
				flag=false;
			}
			
			
			if(dopercent.getValue()!=null&&dopercent.getValue()>=0)
			{
				if(dopercent.getValue()>100){
					showDialogMessage("Percent cannot be greater than 100");
					dopercent.setValue(null);
					flag=false;
				}
				else if(dopercent.getValue()==0){
					showDialogMessage("Percent should be greater than 0!");
					dopercent.setValue(null);
				}
				else{
				payTrmPercent=dopercent.getValue();
				}
			}
			else{
				showDialogMessage("Percent cannot be empty!");
			}
			
			if(tbcomment.getValue()!=null){
				payTrmComment=tbcomment.getValue();
			}
			
			System.out.println("Payment"+paymentTermsTable.getDataprovider().getList().size());
			if(paymentTermsTable.getDataprovider().getList().size()>11)
			{
				showDialogMessage("Total number of payment terms allowed are 12");
				payTermsFlag=false;
			}
			
			if(!payTrmComment.equals("")&&payTrmPercent!=0&&flag==true&&payTermsFlag==true)
			{
				PaymentTerms payTerms=new PaymentTerms();
				payTerms.setPayTermDays(payTrmDays);
				payTerms.setPayTermPercent(payTrmPercent);
				payTerms.setPayTermComment(payTrmComment);
				paymentTermsTable.getDataprovider().getList().add(payTerms);
				clearPaymentTermsFields();
			}
		}
		
		public boolean checkProducts(String pcode)
		{
			List<SalesLineItem> salesitemlis=salesorderlineitemtable.getDataprovider().getList();
			for(int i=0;i<salesitemlis.size();i++)
			{
				if(salesitemlis.get(i).getProductCode().equals(pcode)||salesitemlis.get(i).getProductCode()==pcode){
					return true;
				}
			}
			return false;
		}
		
		/*****************************************Add Product Taxes Method***********************************/
		
		/**
		 * This method is called when a product is added to SalesLineItem table.
		 * Taxes related to corresponding products will be added in ProductTaxesTable.
		 * @throws Exception
		 */
		public void addProdTaxes() throws Exception
		{
			List<SalesLineItem> salesLineItemLis=this.salesorderlineitemtable.getDataprovider().getList();
			List<ProductOtherCharges> taxList=this.prodTaxTable.getDataprovider().getList();
			NumberFormat nf=NumberFormat.getFormat("0.00");
			String cformval="";
			int cformindex=this.getCbcformlis().getSelectedIndex();
			if(cformindex!=0){
				cformval=this.getCbcformlis().getItemText(cformindex);
			}
			for(int i=0;i<salesLineItemLis.size();i++)
			{
				double priceqty=0,taxPrice=0,origPrice=0;
//				if(salesLineItemLis.get(i).getPercentageDiscount()==null){
//					taxPrice=this.getSalesorderlineitemtable().removeAllTaxes(salesLineItemLis.get(i).getPrduct());
//					priceqty=(salesLineItemLis.get(i).getPrice()-taxPrice)*salesLineItemLis.get(i).getQty();
//					priceqty=Double.parseDouble(nf.format(priceqty));
//				}
//				if(salesLineItemLis.get(i).getPercentageDiscount()!=null){
//					taxPrice=this.getSalesorderlineitemtable().removeAllTaxes(salesLineItemLis.get(i).getPrduct());
//					priceqty=(salesLineItemLis.get(i).getPrice()-taxPrice);
//					priceqty=priceqty-(priceqty*salesLineItemLis.get(i).getPercentageDiscount()/100);
//					priceqty=priceqty*salesLineItemLis.get(i).getQty();
//					priceqty=Double.parseDouble(nf.format(priceqty));
//				}
				
				taxPrice=this.getSalesorderlineitemtable().removeAllTaxes(salesLineItemLis.get(i).getPrduct());
				
				origPrice=(salesLineItemLis.get(i).getPrice()-taxPrice);
				if((salesLineItemLis.get(i).getPercentageDiscount()==null && salesLineItemLis.get(i).getPercentageDiscount()==0) && (salesLineItemLis.get(i).getDiscountAmt()==0) ){
						
						System.out.println("inside both 0 condition");
//						priceqty=origPrice*salesLineItemLis.get(i).getQty();
						/******************** Square Area Calculation code added in if condition and without square area calculation code in else block old code ***************************/
						if(!salesLineItemLis.get(i).getArea().equalsIgnoreCase("NA")){
							double squareArea = Double.parseDouble(salesLineItemLis.get(i).getArea());
							priceqty=origPrice*squareArea;
							priceqty=Double.parseDouble(nf.format(priceqty));
							System.out.println("RRRRRRRRRRRRRR price == "+priceqty);
						}else{
							priceqty=origPrice*salesLineItemLis.get(i).getQty();

						}
					}
					
					else if((salesLineItemLis.get(i).getPercentageDiscount()!=null)&& (salesLineItemLis.get(i).getDiscountAmt()!=0)){
						
						System.out.println("inside both not null condition");
						/******************** Square Area Calculation code added in if condition and without square area calculation code in else block old code ***************************/
						if(!salesLineItemLis.get(i).getArea().equalsIgnoreCase("NA")){
							double squareArea = Double.parseDouble(salesLineItemLis.get(i).getArea());
							priceqty=origPrice*squareArea;
							priceqty= priceqty-(priceqty*salesLineItemLis.get(i).getPercentageDiscount()/100);
							priceqty=priceqty-salesLineItemLis.get(i).getDiscountAmt();
							priceqty=Double.parseDouble(nf.format(priceqty));
						}else{
							priceqty=origPrice*salesLineItemLis.get(i).getQty();
							priceqty=priceqty-(priceqty*salesLineItemLis.get(i).getPercentageDiscount()/100);
							priceqty=priceqty-salesLineItemLis.get(i).getDiscountAmt();
						}
						
					}
					else 
					{
						System.out.println("inside oneof the null condition");
						
							if(salesLineItemLis.get(i).getPercentageDiscount()!=null){
								System.out.println("inside getPercentageDiscount oneof the null condition");
								/******************** Square Area Calculation code added in if condition and without square area calculation code in else block old code ***************************/
								if(!salesLineItemLis.get(i).getArea().equalsIgnoreCase("NA")){
									Double squareArea = Double.parseDouble(salesLineItemLis.get(i).getArea());
									priceqty = origPrice*squareArea;
									priceqty=priceqty-(priceqty*salesLineItemLis.get(i).getPercentageDiscount()/100);
								}else{
									priceqty=origPrice*salesLineItemLis.get(i).getQty();
									priceqty=priceqty-(priceqty*salesLineItemLis.get(i).getPercentageDiscount()/100);
								}
							}
							else 
							{
								System.out.println("inside getDiscountAmt oneof the null condition");
								/******************** Square Area Calculation code added in if condition and without square area calculation code in else block old code ***************************/
								if(!salesLineItemLis.get(i).getArea().equals("NA")){
									Double squareArea = Double.parseDouble(salesLineItemLis.get(i).getArea());
									priceqty = origPrice*squareArea;
									priceqty=priceqty-salesLineItemLis.get(i).getDiscountAmt();
								}else{
									priceqty=origPrice*salesLineItemLis.get(i).getQty();
									priceqty=priceqty-salesLineItemLis.get(i).getDiscountAmt();
								}
							}
							
//							total=total*object.getQty();
							
					}
				
				
				System.out.println("Total Prodcut"+priceqty);
				
				System.out.println("Total Prodcut"+priceqty);
				
				/**
				 * Date 20 july 2017 below code commented by vijay 
				 * for this below code by default showing vat and service tax percent 0 no nedd this if
				 * product master does not have any taxes
				 */
				
//				if(salesLineItemLis.get(i).getVatTax().getPercentage()==0&&cbcformlis.getSelectedIndex()==0){
//					ProductOtherCharges pocentity=new ProductOtherCharges();
//					if(validateState()){
//						pocentity.setChargeName(AppConstants.VAT);
//					}
//					if(!validateState()){
//						pocentity.setChargeName(AppConstants.CENTRALTAX);
//					}
//					pocentity.setChargePercent(salesLineItemLis.get(i).getVatTax().getPercentage());
//					pocentity.setIndexCheck(i+1);
//					int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getVatTax().getPercentage(),AppConstants.VAT);
//					if(indexValue!=-1){
////						pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
//						pocentity.setAssessableAmount(0);
//						this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
//					}
//					
//					if(indexValue==-1){
//						pocentity.setAssessableAmount(0);
////						pocentity.setAssessableAmount(priceqty);
//					}
//					this.prodTaxTable.getDataprovider().getList().add(pocentity);
//				}
//				
//				if(salesLineItemLis.get(i).getServiceTax().getPercentage()==0){
////					if(validateState()){
//					ProductOtherCharges pocentity=new ProductOtherCharges();
//					pocentity.setChargeName(AppConstants.SERVICETAX);
//					pocentity.setChargePercent(salesLineItemLis.get(i).getServiceTax().getPercentage());
//					pocentity.setIndexCheck(i+1);
//					int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getServiceTax().getPercentage(),"Service");
//					if(indexValue!=-1){
////						pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
//						pocentity.setAssessableAmount(0);
//						this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
//					}
//					
//					if(indexValue==-1){
//						pocentity.setAssessableAmount(0);
////						pocentity.setAssessableAmount(priceqty);
//					}
//					this.prodTaxTable.getDataprovider().getList().add(pocentity);
////					}
//					
////					if(!validateState()&&getCbcformlis().getItemText(getCbcformlis().getSelectedIndex()).equals(AppConstants.NO)){
////						ProductOtherCharges pocentity=new ProductOtherCharges();
////						pocentity.setChargeName("Service Tax");
////						pocentity.setChargePercent(salesLineItemLis.get(i).getServiceTax().getPercentage());
////						pocentity.setIndexCheck(i+1);
////						pocentity.setAssessableAmount(priceqty);
////						this.prodTaxTable.getDataprovider().getList().add(pocentity);
////					}
//					
//					
//				}
			/**
			 * Adding Vat Tax to table if the company state and delivery state is equal
			 */
				
			if(validateState()){
				if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
					
					/**
					 * Date 12 july 2017 below if condition added by vijay for GST old code added in else block
					 */
					if(salesLineItemLis.get(i).getVatTax().getTaxPrintName()!=null && !salesLineItemLis.get(i).getVatTax().getTaxPrintName().equals("")){
						
						System.out.println("GST VAT");
						ProductOtherCharges pocentity=new ProductOtherCharges();
						
						//Below old code commented by vijay
//						pocentity.setChargeName("VAT");
						// below code added by vijay for GST
						pocentity.setChargeName(salesLineItemLis.get(i).getVatTax().getTaxPrintName());
						pocentity.setChargePercent(salesLineItemLis.get(i).getVatTax().getPercentage());
						pocentity.setIndexCheck(i+1);
						int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getVatTax().getPercentage(),salesLineItemLis.get(i).getVatTax().getTaxPrintName());
						
						if(indexValue!=-1){
							System.out.println("Index As Not -1");
							pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
							this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
						}
						if(indexValue==-1){
							System.out.println("VAt");
							pocentity.setAssessableAmount(priceqty);
						}
						this.prodTaxTable.getDataprovider().getList().add(pocentity);
						
					}else{
						
						System.out.println("Vattx");
						ProductOtherCharges pocentity=new ProductOtherCharges();
						pocentity.setChargeName("VAT");
						pocentity.setChargePercent(salesLineItemLis.get(i).getVatTax().getPercentage());
						pocentity.setIndexCheck(i+1);
						int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getVatTax().getPercentage(),"VAT");
						
						if(indexValue!=-1){
							System.out.println("Index As Not -1");
							pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
							this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
						}
						if(indexValue==-1){
							System.out.println("VAt");
							pocentity.setAssessableAmount(priceqty);
						}
						this.prodTaxTable.getDataprovider().getList().add(pocentity);
					}
						
					
				}
			}
				
			/**
			 * Adding Vat Tax as CST if company state and delivery state is not equal and C form is submitted.
			 */
			
			/**
			 * Date 12 july 2017 added by vijay for GST
			 * below code is applicable only if Sales order date is less than 1 july 2017 for Cform
			 * below if condition added by vijay
			 */
			if(this.dbsalesorderdate.getValue()!=null && this.dbsalesorderdate.getValue().before(date)){
				
				if(this.getCbcformlis().getSelectedIndex()!=0&&AppConstants.YES.equals(cformval.trim())){
//					if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
						System.out.println("Vattx");
						ProductOtherCharges pocentity=new ProductOtherCharges();
						TaxDetails cstPerc=olbcstpercent.getSelectedItem();
						pocentity.setChargeName("CST");
						pocentity.setChargePercent(cstPerc.getTaxChargePercent());
						pocentity.setIndexCheck(i+1);
						int indexValue=this.checkTaxPercent(cstPerc.getTaxChargePercent(),"CST");
						
						if(indexValue!=-1){
							System.out.println("Index As Not -1");
							pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
							this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
						}
						if(indexValue==-1){
							pocentity.setAssessableAmount(priceqty);
						}
						this.prodTaxTable.getDataprovider().getList().add(pocentity);
//					}
				}
			}
				
				/**
				 * Adding Vat Tax as CST if company state and delivery state is not equal and C form is not submitted.
				 */
				
			/**
			 * Date 12 july 2017 added by vijay for GST
			 * below code is applicable only if Sales order date is less than 1 july 2017 for Cform
			 * below if condition added by vijay
			 */
			if(this.dbsalesorderdate.getValue()!=null && this.dbsalesorderdate.getValue().before(date)){
				
				if(this.getCbcformlis().getSelectedIndex()!=0&&AppConstants.NO.equals(cformval.trim())){
					if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
						System.out.println("Vattx");
						ProductOtherCharges pocentity=new ProductOtherCharges();
						pocentity.setChargeName("CST");
						pocentity.setChargePercent(salesLineItemLis.get(i).getVatTax().getPercentage());
						pocentity.setIndexCheck(i+1);
						int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getVatTax().getPercentage(),"VAT");
						
						if(indexValue!=-1){
							System.out.println("vsctIndex As Not -1");
							pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
							this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
						}
						if(indexValue==-1){
							System.out.println("vcst-1");
							pocentity.setAssessableAmount(priceqty);
						}
						this.prodTaxTable.getDataprovider().getList().add(pocentity);
					}
				}
				
			}
				/**
				 * Adding service tax to the table
				 */
				
				if(salesLineItemLis.get(i).getServiceTax().getPercentage()!=0){
					System.out.println("hi vijay ");
					System.out.println("ServiceTx");
					ProductOtherCharges pocentity=new ProductOtherCharges();
					
					/**
					 * Date 11 july 2017 if condition added by vijay for GST and old code added in else block 
					 */
					if(salesLineItemLis.get(i).getServiceTax().getTaxPrintName()!=null && !salesLineItemLis.get(i).getServiceTax().getTaxPrintName().equals(""))
					{
						pocentity.setChargeName(salesLineItemLis.get(i).getServiceTax().getTaxPrintName());
					}else{
						pocentity.setChargeName("Service Tax");
					}
					/**
					 * ends here
					 */
					
					pocentity.setChargePercent(salesLineItemLis.get(i).getServiceTax().getPercentage());
					pocentity.setIndexCheck(i+1);
//					int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getServiceTax().getPercentage(),"Service");
					
					/**
					 * Date 12 july 2017 if condition for old code and else block for GST added by vijay
					 */
					int indexValue;
					if(salesLineItemLis.get(i).getServiceTax().getTaxPrintName()!=null && !salesLineItemLis.get(i).getServiceTax().getTaxPrintName().equals(""))
					{
						indexValue =this.checkTaxPercent(salesLineItemLis.get(i).getServiceTax().getPercentage(),salesLineItemLis.get(i).getServiceTax().getTaxPrintName());
					}else{
						indexValue =this.checkTaxPercent(salesLineItemLis.get(i).getServiceTax().getPercentage(),"Service");

					}
					/**
					 * ends here
					 */
					
					if(indexValue!=-1){
						System.out.println("Index As Not -1");
						
						/**
						 * If validate state method is true
						 */
						
						if(validateState()){
							double assessValue=0;
							if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
								
								/**
								 * Date 12 july 2017 if condition added by vijay for GST and old code added in else block 
								 */
								if(salesLineItemLis.get(i).getServiceTax().getTaxPrintName()!=null && !salesLineItemLis.get(i).getServiceTax().getTaxPrintName().equals("")){
									System.out.println("For GST");
									assessValue=priceqty;
									pocentity.setAssessableAmount(assessValue+taxList.get(indexValue).getAssessableAmount());
									System.out.println("price =="+assessValue);
									System.out.println(assessValue+taxList.get(indexValue).getAssessableAmount());
								 }else{
									 System.out.println("old code ");
									 	assessValue=priceqty+(priceqty*salesLineItemLis.get(i).getVatTax().getPercentage()/100);
										pocentity.setAssessableAmount(assessValue+taxList.get(indexValue).getAssessableAmount());
								 }
								this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
							}
							if(salesLineItemLis.get(i).getVatTax().getPercentage()==0){
								pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
								this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
							}
						}
						
						/** Date 12 july 2017 added by vijay for GST
						 * below code is applicable only if Sales order date is less than 1 july 2017 for Cform
						 * below if condition added by vijay
						 */
						if(this.dbsalesorderdate.getValue()!=null && this.dbsalesorderdate.getValue().before(date)){
							
						/**
						 * If validate state method is true and cform is submittted.
						 */
						
						if(!validateState()&&this.getCbcformlis().getItemText(getCbcformlis().getSelectedIndex()).equals(AppConstants.YES)){
							double assessValue=0;
							if(this.getOlbcstpercent().getSelectedIndex()!=0){
								TaxDetails cstperc1=olbcstpercent.getSelectedItem();
								assessValue=priceqty+(priceqty*cstperc1.getTaxChargePercent()/100);
								pocentity.setAssessableAmount(assessValue+taxList.get(indexValue).getAssessableAmount());
								this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
							}
						}
						
						/**
						 * If validate state method is true and cform is not submitted.
						 */
						
						if(!validateState()&&this.getCbcformlis().getItemText(getCbcformlis().getSelectedIndex()).equals(AppConstants.NO)){
							double assessValue=0;
							assessValue=priceqty+(priceqty*salesLineItemLis.get(i).getVatTax().getPercentage()/100);
							pocentity.setAssessableAmount(assessValue+taxList.get(indexValue).getAssessableAmount());
							this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
						}
						
					  }
					/**
					 * ends here
					 */
					}
					if(indexValue==-1){
						System.out.println("Index As -1");
						
						if(validateState()){
							double assessVal=0;
							if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
								
								/**
								 * Date 12 july 2017 if condition added by vijay for GST and old code added into else block
								 */
								if(salesLineItemLis.get(i).getServiceTax().getTaxPrintName()!=null && !salesLineItemLis.get(i).getServiceTax().getTaxPrintName().equals("")){
									System.out.println("For GST");
									assessVal=priceqty;
									pocentity.setAssessableAmount(assessVal);
									
								}
								else{
									assessVal=priceqty+(priceqty*salesLineItemLis.get(i).getVatTax().getPercentage()/100);
									pocentity.setAssessableAmount(assessVal);
								}
								
							}
							if(salesLineItemLis.get(i).getVatTax().getPercentage()==0){
								pocentity.setAssessableAmount(priceqty);
							}
						}
						
						/** Date 12 july 2017 added by vijay for GST
						 * below code is applicable only if Sales order date is less than 1 july 2017 for Cform
						 * below if condition added by vijay
						 */
						if(this.dbsalesorderdate.getValue()!=null && this.dbsalesorderdate.getValue().before(date)){
							
						if(!validateState()&&this.getCbcformlis().getItemText(getCbcformlis().getSelectedIndex()).equals(AppConstants.NO)){
							double assessVal=0;
							if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
								assessVal=priceqty+(priceqty*salesLineItemLis.get(i).getVatTax().getPercentage()/100);
								pocentity.setAssessableAmount(assessVal);
							}
							if(salesLineItemLis.get(i).getVatTax().getPercentage()==0){
								pocentity.setAssessableAmount(priceqty);
							}
						}
						
						if(!validateState()&&this.getCbcformlis().getItemText(getCbcformlis().getSelectedIndex()).equals(AppConstants.YES)){
							double assessVal=0;
							if(this.getOlbcstpercent().getSelectedIndex()!=0){
								TaxDetails cstperc2=olbcstpercent.getSelectedItem();
								assessVal=priceqty+(priceqty*cstperc2.getTaxChargePercent()/100);
								pocentity.setAssessableAmount(assessVal);
							}
						}
					   }
						/**
						 * ends here
						 */
					}
					this.prodTaxTable.getDataprovider().getList().add(pocentity);
				}
			}
		}

		
		/**
		 * Date : 15-09-2017 BY ANIL
		 * Adding calculation part for other charges
		 */
		public void addOtherChargesInTaxTbl(){
			List<ProductOtherCharges> taxList=this.prodTaxTable.getDataprovider().getList();
			for(OtherCharges otherCharges:tblOtherCharges.getDataprovider().getList()){
				/**
				 * If GST tax is not applicable then we will consider tax1 as vat tax field and tax2 as service tax field.
				 */
				if(otherCharges.getTax1().getTaxPrintName()!=null&&!otherCharges.getTax1().getTaxPrintName().equals("")){
					boolean updateTaxFlag=true;
					if(otherCharges.getTax1().getTaxPrintName().equalsIgnoreCase("SELECT")){
						updateTaxFlag=false;
//						return;
					}
					if(otherCharges.getTax1().getPercentage()==0){
						updateTaxFlag=false;
//						return;
					}
					if(updateTaxFlag){
						ProductOtherCharges pocentity=new ProductOtherCharges();
						pocentity.setChargeName(otherCharges.getTax1().getTaxPrintName());
						pocentity.setChargePercent(otherCharges.getTax1().getPercentage());
	//					pocentity.setIndexCheck(i+1);
						int indexValue=this.checkTaxPercent(otherCharges.getTax1().getPercentage(),otherCharges.getTax1().getTaxPrintName());
						if(indexValue!=-1){
							pocentity.setAssessableAmount(otherCharges.getAmount()+taxList.get(indexValue).getAssessableAmount());
							this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
						}
						if(indexValue==-1){
							pocentity.setAssessableAmount(otherCharges.getAmount());
						}
						this.prodTaxTable.getDataprovider().getList().add(pocentity);
					}
				}else{
					boolean updateTaxFlag=true;
					if(otherCharges.getTax1().getPercentage()==0){
						updateTaxFlag=false;
//						return;
					}
					if(updateTaxFlag){
						ProductOtherCharges pocentity=new ProductOtherCharges();
						pocentity.setChargeName("VAT");
						pocentity.setChargePercent(otherCharges.getTax1().getPercentage());
	//					pocentity.setIndexCheck(i+1);
						int indexValue=this.checkTaxPercent(otherCharges.getTax1().getPercentage(),"VAT");
						if(indexValue!=-1){
							pocentity.setAssessableAmount(otherCharges.getAmount()+taxList.get(indexValue).getAssessableAmount());
							this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
						}
						if(indexValue==-1){
							pocentity.setAssessableAmount(otherCharges.getAmount());
						}
						this.prodTaxTable.getDataprovider().getList().add(pocentity);
					}
				}
				
				if(otherCharges.getTax2().getTaxPrintName()!=null&&!otherCharges.getTax2().getTaxPrintName().equals("")){
					System.out.println("INSIDE GST TAX PRINT NAME : "+otherCharges.getTax2().getTaxPrintName());
					boolean updateTaxFlag=true;
					if(otherCharges.getTax2().getTaxPrintName().equalsIgnoreCase("SELECT")){
						updateTaxFlag=false;
//						return;
					}
					if(otherCharges.getTax2().getPercentage()==0){
						updateTaxFlag=false;
//						return;
					}
					if(updateTaxFlag){
						ProductOtherCharges pocentity=new ProductOtherCharges();
						pocentity.setChargeName(otherCharges.getTax2().getTaxPrintName());
						pocentity.setChargePercent(otherCharges.getTax2().getPercentage());
	//					pocentity.setIndexCheck(i+1);
						int indexValue=this.checkTaxPercent(otherCharges.getTax2().getPercentage(),otherCharges.getTax2().getTaxPrintName());
						if(indexValue!=-1){
							pocentity.setAssessableAmount(otherCharges.getAmount()+taxList.get(indexValue).getAssessableAmount());
							this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
						}
						if(indexValue==-1){
							pocentity.setAssessableAmount(otherCharges.getAmount());
						}
						this.prodTaxTable.getDataprovider().getList().add(pocentity);
					}
				}else{
					System.out.println("ST OR NON GST");
					boolean updateTaxFlag=true;
					if(otherCharges.getTax2().getPercentage()==0){
						updateTaxFlag=false;
//						return;
					}
					if(otherCharges.getTax1().getTaxPrintName()!=null&&!otherCharges.getTax1().getTaxPrintName().equals("")){
						updateTaxFlag=false;
//						return;
					}
					if(updateTaxFlag){
						ProductOtherCharges pocentity=new ProductOtherCharges();
						pocentity.setChargeName("Service Tax");
						pocentity.setChargePercent(otherCharges.getTax2().getPercentage());
	//					pocentity.setIndexCheck(i+1);
						int indexValue=this.checkTaxPercent(otherCharges.getTax2().getPercentage(),"Service");
						if(indexValue!=-1){
						 	double assessValue=otherCharges.getAmount()+(otherCharges.getAmount()*otherCharges.getTax1().getPercentage()/100);
							pocentity.setAssessableAmount(assessValue+taxList.get(indexValue).getAssessableAmount());
							this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
						}
						if(indexValue==-1){
							double assessValue=otherCharges.getAmount()+(otherCharges.getAmount()*otherCharges.getTax1().getPercentage()/100);
							pocentity.setAssessableAmount(assessValue);
						}
						this.prodTaxTable.getDataprovider().getList().add(pocentity);
					}
				}
			}
		}
		
		/*********************************Check Tax Percent*****************************************/
		/**
		 * This method returns 
		 * @param taxValue
		 * @param taxName
		 * @return
		 */
		
		public int checkTaxPercent(double taxValue,String taxName)
		{
			System.out.println("Tav Value"+taxValue);
			List<ProductOtherCharges> taxesList=this.prodTaxTable.getDataprovider().getList();
			
			for(int i=0;i<taxesList.size();i++)
			{
				double listval=taxesList.get(i).getChargePercent();
				int taxval=(int)listval;
				
				/**
				 * @author Vijay  Date 15-09-2020
				 * Des :- when IGST percentage is in decimal then its showing multiple taxes in prodtaxes so updated the code
				 */
				double taxval2 = listval;
				
				if(taxName.equals("Service")){
					if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("Service Tax")){
						return i;
					}
					if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("Service Tax")){
						System.out.println("zero");
						return i;
					}
				}
				if(taxName.equals("VAT")){
					if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("VAT")){
						return i;
					}
					if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("VAT")){
						System.out.println("zero");
						return i;
					}
				}
				if(taxName.equals("CST")){
					if(taxValue==taxesList.get(i).getChargePercent()&&taxesList.get(i).getChargeName().trim().equals("CST")){
						return i;
					}
				}
			//  vijay added this for GST on 12 july 2017
				if(taxName.equals("SGST")){
					if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("SGST")){
						return i;
					}
					if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("SGST")){
						return i;
					}
				}
				
				//  vijay added this for GST on 12 july 2017
				if(taxName.equals("CGST")){
					if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("CGST")){
						return i;
					}
					if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("CGST")){
						return i;
					}
				}
				
				//  vijay added this for GST on 12 july 2017
				if(taxName.equals("IGST")){
					if(taxValue==taxesList.get(i).getChargePercent()&&(taxval!=0 || taxval2!=0)&&taxesList.get(i).getChargeName().trim().equals("IGST")){
						return i;
					}
					if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("IGST")){
						return i;
					}
				}
			}
			return -1;
		}
		
		
		
		/*************************Update Charges Table****************************************/
		/**
		 * This method updates the charges table.
		 * The table is updated when any row value is changed through field updater in table i.e
		 * row change event is fired
		 */
		
		public void updateChargesTable()
		{
			List<ProductOtherCharges> prodOtrLis=this.chargesTable.getDataprovider().getList();
			ArrayList<ProductOtherCharges> prodOtrArr=new ArrayList<ProductOtherCharges>();
			prodOtrArr.addAll(prodOtrLis);
			
			this.chargesTable.connectToLocal();
			System.out.println("prodo"+prodOtrArr.size());
			for(int i=0;i<prodOtrArr.size();i++)
			{
				ProductOtherCharges prodChargesEnt=new ProductOtherCharges();
				prodChargesEnt.setChargeName(prodOtrArr.get(i).getChargeName());
				prodChargesEnt.setChargePercent(prodOtrArr.get(i).getChargePercent());
				prodChargesEnt.setChargeAbsValue(prodOtrArr.get(i).getChargeAbsValue());
				//prodChargesEnt.setAssessableAmount(prodOtrArr.get(i).getAssessableAmount());
				prodChargesEnt.setFlagVal(prodOtrArr.get(i).getFlagVal());
				prodChargesEnt.setIndexCheck(prodOtrArr.get(i).getIndexCheck());
				if(prodOtrArr.get(i).getFlagVal()==false){
					prodChargesEnt.setAssessableAmount(this.getDoamtincltax().getValue());
				}
				if(prodOtrArr.get(i).getFlagVal()==true){
					double assesableVal=this.retrieveSurchargeAssessable(prodOtrArr.get(i).getIndexCheck());
					prodChargesEnt.setAssessableAmount(assesableVal);
				}
				
				this.chargesTable.getDataprovider().getList().add(prodChargesEnt);
			}
		}
		
		public double retrieveSurchargeAssessable(int indexValue)
		{
			List<ProductOtherCharges> otrChrgLis=this.chargesTable.getDataprovider().getList();
			ArrayList<ProductOtherCharges> otrChrgArr=new ArrayList<ProductOtherCharges>();
			otrChrgArr.addAll(otrChrgLis);
		
			double getAssessVal=0;
			for(int i=0;i<otrChrgArr.size();i++)
			{
				if(otrChrgArr.get(i).getFlagVal()==false&&otrChrgArr.get(i).getIndexCheck()==indexValue)
				{
					if(otrChrgArr.get(i).getChargePercent()!=0){
						getAssessVal=otrChrgArr.get(i).getAssessableAmount()*otrChrgArr.get(i).getChargePercent()/100;
					}
					if(otrChrgArr.get(i).getChargeAbsValue()!=0){
						getAssessVal=otrChrgArr.get(i).getChargeAbsValue();
					}
				}
			}
			return getAssessVal;
		}

		/************************************Overridden Methods*******************************************/


		@Override
		public void setEnable(boolean state) {
			super.setEnable(state);
			getstatustextbox().setEnabled(false);
			this.tbContractId.setEnabled(false);
			this.tbLeadId.setEnabled(false);
			this.tbQuotatinId.setEnabled(false);
			this.salesorderlineitemtable.setEnable(state);
			this.paymentTermsTable.setEnable(state);
			this.chargesTable.setEnable(state);
			this.prodTaxTable.setEnable(state);
			cbcheckaddress.setEnabled(false);//13-02-2023
			this.doamtincltax.setEnabled(false);
			this.donetpayamt.setEnabled(false);
			this.dototalamt.setEnabled(false);
			this.olbcstpercent.setEnabled(false);
			this.cbcformlis.setEnabled(false);
			this.acshippingcomposite.setEnable(false);
			tbremark.setEnabled(false);
			
			/**
			 * Date : 20-09-2017 BY ANIL
			 */
			this.tblOtherCharges.setEnable(state);
			dbOtherChargesTotal.setEnabled(false);
			/**
			 * end
			 */
			
			/*
			 * Date:19/07/2018
			 * Developer:Ashwini
			 */
			if(tbContractId.getValue()!=null&&!tbContractId.getValue().equals("")){
				this.olbcNumberRange.setEnabled(false);
			}
			/*
			 * End here
			 */
			/** date 3.10.2018 added by komal for grn and po id**/
			tbPOId.setEnabled(false);
			tbGRNId.setEnabled(false);
			
			marginPopup.getLblOk().setVisible(state);
			marginPopup.vendorMarginTbl.setEnable(state);
			marginPopup.otherChargesMarginTbl.setEnable(state);
			tbBudget.setEnabled(false);
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("NumberRange", "MakeNumberRangeReadOnly"))
		  	{	olbcNumberRange.setEnabled(false);	  		
		  	}
		}

		@Override
		public void setToViewState() {
		
			super.setToViewState();
			setMenuAsPerStatus();
		
			SuperModel model=new SalesOrder();
			model=salesorderobj;
			AppUtility.addDocumentToHistoryTable(AppConstants.SALESMODULE,AppConstants.SALESORDER, salesorderobj.getCount(), Integer.parseInt(personInfoComposite.getId().getValue()),personInfoComposite.getName().getValue(),Long.parseLong(personInfoComposite.getPhone().getValue()), false, model, null);
		
			if(marginFlag){
				costingBtn.setEnabled(true);
			}
			
			String mainScreenLabel="Sales Order";
			if(salesorderobj!=null&&salesorderobj.getCount()!=0){
				mainScreenLabel=salesorderobj.getCount()+" "+"/"+" "+salesorderobj.getStatus()+" "+"/"+" "+AppUtility.parseDate(salesorderobj.getCreationDate());
			}
				fgroupingCustomerInformation.getHeaderLabel().setText(mainScreenLabel);
		}
		@Override
		public void setToEditState() {
		
			super.setToEditState();
			this.processLevelBar.setVisibleFalse(false);
			
			if(olbcstpercent.getValue()!=null){
				olbcstpercent.setEnabled(true);
			}
			if(cbcformlis.getSelectedIndex()!=0){
				cbcformlis.setEnabled(true);
			}
			
			if(!tbQuotatinId.getValue().equals(""))
			{
				baddproducts.setEnabled(false);
				addOtherCharges.setEnabled(false);
			}
			
			this.olbcNumberRange.setEnabled(false);//Added by Ashwini
			changeStatusToCreated();
			
//			if(cncEntity == null && tbReferenceNumber.getValue()!=null && !tbReferenceNumber.getValue().equals("") && AppUtility.checkForProcessConfigurartionIsActiveOrNot("SalesOrder", "ValidateCNCActualBIllingBudget") ){
			if(cncEntity == null && tbReferenceNumber.getValue()!=null && !tbReferenceNumber.getValue().equals("")){
				reactonLoadCNC(tbReferenceNumber.getValue());
			}
			
			String mainScreenLabel="Sales Order";
			if(salesorderobj!=null&&salesorderobj.getCount()!=0){
				mainScreenLabel=salesorderobj.getCount()+" "+"/"+" "+salesorderobj.getStatus()+" "+"/"+" "+AppUtility.parseDate(salesorderobj.getCreationDate());
			}
				fgroupingCustomerInformation.getHeaderLabel().setText(mainScreenLabel);
			
		}
		
		public void changeStatusToCreated()
		{
			SalesOrder entity=(SalesOrder) presenter.getModel();
			String status=entity.getStatus();
			
			if(SalesOrder.REJECTED.equals(status.trim()))
			{
				this.tbQuotationStatus.setValue(SalesOrder.CREATED);
			}
		}
		
		@Override
		public void clear() {
			super.clear();
			getstatustextbox().setText(SalesOrder.CREATED);
			
			vendorMargins=null;
			otherChargesMargins=null;
			totalOcMargin=null;
			totalVendorMargin=null;
		}
		
		
		
		/****************************************Validation Method(Overridden)**********************************/
		/**
		 * Validates form fields
		 */
		
		@Override
		 public boolean validate()
		 {
			 boolean superValidate = super.validate();
			 if(superValidate==false){
	        	 return false;
	        }
	       
			if(salesorderlineitemtable.getDataprovider().getList().size()==0)
	        {
	            showDialogMessage("Please Select at least one Product!");
	            return false;
	        }
	        
	        boolean valPayPercent=this.validatePaymentTermPercent();
	        if(valPayPercent==false)
	        {
	        	showDialogMessage("Payment Term Not 100%, Please Correct!");
	        	return false;
	        }
	        
	        /**
			 * Date 11 july 2017 added by vijay for GST
			 * below code is applicable only if quotation date is less than 1 july 2017 for Cform
			 * below if condition added by vijay
			 */
			if(this.dbsalesorderdate.getValue()!=null && this.dbsalesorderdate.getValue().before(date)){
				
			
	        if(this.getAcshippingcomposite().getState().getSelectedIndex()!=0){
	        	String companyStateVal=SalesOrderPresenter.companyStateVal.trim();
	        	System.out.println("comp"+companyStateVal);
	        	Console.log(companyStateVal);
	        	if(!getAcshippingcomposite().getState().getValue().trim().equals(companyStateVal)&&getCbcformlis().getSelectedIndex()==0){
	        		showDialogMessage("C Form is Mandatory!");
	        		return false;
	        	}
	        }
	        	
	        if(getCbcformlis().getItemText(getCbcformlis().getSelectedIndex()).trim().equals(AppConstants.YES)&&getOlbcstpercent().getSelectedIndex()==0){
	        	showDialogMessage("Please select CST Percent!");
	        	return false;
	        }
	        
			}
	        
	        if(validateSalesOrderPrice()>0){
	        	showDialogMessage("Please enter product price greater than 0!");
	        	return false;
	        }
	        
	        
	        if(!validateSalesOrderProdQty()){
				showDialogMessage("Please enter appropriate quantity required!");
				return false;
			}
			
	        
	        /**
			 * Date 03-07-2017 added by vijay tax validation
			 */
			List<ProductOtherCharges> prodtaxlist = this.prodTaxTable.getDataprovider().getList();
			for(int i=0;i<prodtaxlist.size();i++){
				
				if(dbsalesorderdate.getValue()!=null && dbsalesorderdate.getValue().before(date)){
					if(prodtaxlist.get(i).getChargeName().equalsIgnoreCase("CGST") || prodtaxlist.get(i).getChargeName().equalsIgnoreCase("SGST")
							|| prodtaxlist.get(i).getChargeName().equalsIgnoreCase("IGST")){
						showDialogMessage("As Sales Order date "+fmt.format(dbsalesorderdate.getValue())+" GST Tax is not applicable");
						return false;
					}
				}
				/**
				 * @author Anil @since 21-07-2021
				 * Issue raised by Rahul for Innovative Pest(Thailand) 
				 * Vat is applicable there so system should not through validation for VAT
				 */
//				else if(dbsalesorderdate.getValue()!=null && dbsalesorderdate.getValue().equals(date) || dbsalesorderdate.getValue().after(date)){
//					if(prodtaxlist.get(i).getChargeName().equalsIgnoreCase("CST") || prodtaxlist.get(i).getChargeName().equalsIgnoreCase("Service Tax")
//							|| prodtaxlist.get(i).getChargeName().equalsIgnoreCase("VAT")){
//						showDialogMessage("As Sales Order date "+fmt.format(dbsalesorderdate.getValue())+" VAT/CST/Service Tax is not applicable");
//						return false;
//					}
//				}
			}
			/**
			 * ends here
			 */
			/**
			 * nidhi
			 * 20-09-2018
			 */
			if(LoginPresenter.mapModelSerialNoFlag ){
				if(!validateSerailNo()){
					showDialogMessage("Please verify Product Qty and serial number selection.");
					return false;
				}
			}
			
			/**
			 * @author Vijay 
			 * @since 19-08-2020
			 * Des :- for CNC Actual bill exceeds the Consuble bill Budget then it should validate 
			 */
//			if(tbReferenceNumber.getValue()!=null && !tbReferenceNumber.getValue().equals("") && AppUtility.checkForProcessConfigurartionIsActiveOrNot("SalesOrder", "ValidateCNCActualBIllingBudget") ){
			if(tbReferenceNumber.getValue()!=null && !tbReferenceNumber.getValue().equals("")){
				double sumOfActualBillOpsBudget=0;
				Console.log("cncEntity"+cncEntity);
				
				if(uploadConsumableFlag){
					if(cncEntity!=null){
						if(oblCustomerBranch.getValue()!=null){
							if(cncEntity.getConsumablesList()!=null&&cncEntity.getConsumablesList().size()!=0){
								for(Consumables consumables :cncEntity.getConsumablesList()){
									if(consumables.getBillingType().equals("ACTUALS") && consumables.getOpsBudget()!=null&& !consumables.getOpsBudget().equals("")){
										if(consumables.getSiteLocation()!=null&&consumables.getSiteLocation().equals(oblCustomerBranch.getValue())){
											sumOfActualBillOpsBudget= Double.parseDouble(consumables.getOpsBudget());
											break;
										}
									}
								}
							}
						}
						
						Console.log("sumOfActualBillOpsBudget"+sumOfActualBillOpsBudget);
						if(sumOfActualBillOpsBudget!=0){
							Console.log("dototalamt.getValue"+dototalamt.getValue());
							if(dototalamt.getValue()!=null && dototalamt.getValue()>sumOfActualBillOpsBudget){
								showDialogMessage("For "+oblCustomerBranch.getValue()+", the fixed budget in CNC ("+cncEntity.getCount()+") is amount "+sumOfActualBillOpsBudget);
								return false;
							}
						}
						
					}
					
				}else{
					if(cncEntity!=null){
						for(Consumables consumables :cncEntity.getConsumablesList()){
							if(consumables.getBillingType().equals("ACTUALS") && consumables.getOpsBudget()!=null
																			  && !consumables.getOpsBudget().equals("")){
								sumOfActualBillOpsBudget += Double.parseDouble(consumables.getOpsBudget());
							}
						}
					}
					Console.log("sumOfActualBillOpsBudget"+sumOfActualBillOpsBudget);
					if(sumOfActualBillOpsBudget!=0){
						Console.log("dototalamt.getValue"+dototalamt.getValue());
						double salesOrderTotalAmt = dototalamt.getValue();
						if(dototalamt.getValue()!=null && dototalamt.getValue()>sumOfActualBillOpsBudget){
							showDialogMessage("Budget of the CNC is amount "+sumOfActualBillOpsBudget);
							return false;
						}
					}
				}
			}
			/**
			 * ends here
			 */

	        return true;
		 }

		boolean validateSerailNo(){
			boolean flag = true;
			

			for(int i=0;i<salesorderlineitemtable.getDataprovider().getList().size();i++){
				int qty = (int) salesorderlineitemtable.getDataprovider().getList().get(i).getQty();
				int serqty = 0;
				if(salesorderlineitemtable.getDataprovider().getList().get(i).getProSerialNoDetails()!=null  
						&& salesorderlineitemtable.getDataprovider().getList().get(i).getProSerialNoDetails().size()>0
						&& salesorderlineitemtable.getDataprovider().getList().get(i).getProSerialNoDetails().containsKey(0)){
					for(int j = 0;j<salesorderlineitemtable.getDataprovider().getList().get(i).getProSerialNoDetails().get(0).size() ; j++){
						if (salesorderlineitemtable.getDataprovider().getList().get(i).getProSerialNoDetails().get(0).get(j).isStatus()){
							serqty++;
						}
					}
					if(serqty>0 && serqty==qty){
						flag = true;
					}else{
						flag = false;
						break;
					}
				}
			}
			
			return flag; 
					
		}
		/********************************Clear Payment Terms Fields*******************************************/
		
		
		public boolean validateSalesOrderProdQty()
		{
			List<SalesLineItem> orderlis=salesorderlineitemtable.getDataprovider().getList();
			int ctr=0;
			for(int i=0;i<orderlis.size();i++)
			{
				if(orderlis.get(i).getQty()==0)
				{
					ctr=ctr+1;
				}
			}
			
			if(ctr==0){
				return true;
			}
			else{
				return false;
			}
		}
		
		
		public void clearPaymentTermsFields(){
			ibdays.setValue(null);
			dopercent.setValue(null);
			tbcomment.setValue("");
		}
		
		/*******************************Validate Payment Term Percent*****************************************/
		/**
		 * This method validates payment term percent. i.e the total of percent should be equal to 100
		 * @return
		 */
		
		public boolean validatePaymentTermPercent()
		{
			List<PaymentTerms> payTermsLis=this.paymentTermsTable.getDataprovider().getList();
			double totalPayPercent=0;
			for(int i=0;i<payTermsLis.size();i++)
			{
				totalPayPercent=totalPayPercent+payTermsLis.get(i).getPayTermPercent();
			}
			System.out.println("Validation of Payment Percent as 100"+totalPayPercent);
			
			if(totalPayPercent!=100)
			{
				return false;
			}
			
			return true;
		}
		
		/********************************Validate Payment Terms Days******************************************/
		/**
		 * This method represents the validation of payment terms days
		 * @param retrDays
		 * @return
		 */
		
		int chk=0;
		public boolean validatePaymentTrmDays(int retrDays)
		{
			List<PaymentTerms> payTermsLis=this.paymentTermsTable.getDataprovider().getList();
			for(int i=0;i<payTermsLis.size();i++)
			{
				if(retrDays==payTermsLis.get(i).getPayTermDays())
				{
					return false;
				}
				if(retrDays<payTermsLis.get(i).getPayTermDays())
				{
					showDialogMessage("Days should be defined in increasing order!");
					chk=1;
					return false;
				}
				else{
					chk=0;
				}
				if(retrDays>10000){
					showDialogMessage("Days cannot be greater than 4 digits!");
					return false;
				}
			}
			return true;
		}
		
		/**
		 * 
		 * @param pcode
		 * @return
		 */
		
		public boolean validateProductDuplicate(String pcode)
		{
			List<SalesLineItem> salesItemLis=this.getSalesorderlineitemtable().getDataprovider().getList();
			for(int i=0;i<salesItemLis.size();i++)
			{
				System.out.println("salesl"+salesItemLis.get(i).getProductCode());
				if(salesItemLis.get(i).getProductCode().trim().equals(pcode)){
					System.out.println("In");
					return true;
				}
			}
			System.out.println("Out");
			return false;
		}
		
		public int validateSalesOrderPrice()
		{
			List<SalesLineItem> salesItemLis=this.getSalesorderlineitemtable().getDataprovider().getList();
			int ctr=0;
			for(int i=0;i<salesItemLis.size();i++)
			{
				if(salesItemLis.get(i).getPrice()==0){
					ctr=ctr+1;
				}
			}
			
			return ctr;
		}
		
		public void ProdctType(String pcode,String productId)
		{
			final GenricServiceAsync genasync=GWT.create(GenricService.class);
			
			int prodId=Integer.parseInt(productId);
			
			Vector<Filter> filtervec=new Vector<Filter>();
			final MyQuerry querry=new MyQuerry();
			
			Filter filter=new Filter();
			filter.setQuerryString("productCode");
			filter.setStringValue(pcode.trim());
			filtervec.add(filter);
			
			filter=new Filter();
			filter.setQuerryString("count");
			filter.setIntValue(prodId);
			filtervec.add(filter);
			
			filter=new Filter();
			filter.setQuerryString("status");
			filter.setBooleanvalue(true);
			filtervec.add(filter);
			
			querry.setFilters(filtervec);
			querry.setQuerryObject(new SuperProduct());
			showWaitSymbol();
			 Timer timer=new Timer() 
        	 {
 				@Override
 				public void run() {
				genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
					
					@Override
					public void onFailure(Throwable caught) {
						showDialogMessage("An Unexpected error occurred!");
					}
		
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						
						if(result.size()==0)
						{
							showDialogMessage("Please check whether product status is active or not!");
						}
						
						Branch branchEntity = olbbBranch.getSelectedItem();
						boolean gstApplicable = false;
						if(olbcNumberRange.getSelectedIndex()!=0){
							Config configEntity = olbcNumberRange.getSelectedItem();
							if(configEntity!=null){
								gstApplicable = configEntity.isGstApplicable();
								System.out.println("gstApplicable $$ ==== "+gstApplicable);

							}
						}
						else{
							gstApplicable = true;
						}
						System.out.println("gstApplicable ==== "+gstApplicable);
						for(SuperModel model:result)
						{
							SuperProduct superProdEntity = (SuperProduct)model;
//							SalesLineItem lis=AppUtility.ReactOnAddProductCompositeForSales(superProdEntity);
							SalesLineItem lis=AppUtility.ReactOnAddProductCompositeForSales(superProdEntity,cust,branchEntity,gstApplicable);
							/***************** vijay********************/
							Date warrantyuntildate = getWarrantyEndDate(dbsalesorderdate.getValue(),lis.getWarrantyInMonth());
							lis.setWarrantyUntil(warrantyuntildate);
							lis.setPreviousTax1(lis.getServiceTax());
							lis.setPreviousTax2(lis.getVatTax());
//							/** date 22.11.2018 added by komal for bt default selection of IGST **/
//							if(AppUtility.isAddressStateDifferent(olbbBranch.getSelectedItem().getAddress().getState(), acshippingcomposite.getState().getValue())){
//								lis = AppUtility.setByDefaultTaxAsIGST(lis ,true);
//							}
							
							/** end komal**/
							//Date 21 july 2017 added by vijay for valid until calculation
							if(dbsalesorderdate.getValue()!=null)
								salesorderDate = dbsalesorderdate.getValue();
							
							/** Date 03-05-2018 By vijay for Product SrNo and allow to add same product **/
							lis.setProductSrNo(productSrNo+1);
							
						    getSalesorderlineitemtable().getDataprovider().getList().add(lis);
						    /** Date 03-05-2018 By vijay for Product SrNo **/
						    productSrNo++;
						}
						}
					 });
					hideWaitSymbol();
	 				}
		 	  };
	          timer.schedule(1000); 
          	/**
	    	  * @author Vijay Chougulw
	    	  * Date 17-06-2020
	    	  * Des :- reduced timer 3000 to 1000 because shash getting freeze screen while adding product in sales order
	    	  */
		}
		
		private Date getWarrantyEndDate(Date salesorderDate,int warrantyInMonth) {
			 CalendarUtil.addMonthsToDate(salesorderDate, warrantyInMonth);
				return salesorderDate;
		}
		
		public void initializePercentCst()
		{
			Timer timer=new Timer() 
		    {
				@Override
				public void run() 
				{
					MyQuerry querry=new MyQuerry();
					Company comEntity=new Company();
					Vector<Filter> filter=new Vector<Filter>();
					Filter temp=null;
					
					temp=new Filter();
					temp.setQuerryString("companyId");
					temp.setLongValue(comEntity.getCompanyId());
					filter.add(temp);
					
					temp=new Filter();
					temp.setQuerryString("isCentralTax");
					temp.setBooleanvalue(true);
					filter.add(temp);
					
					temp=new Filter();
					temp.setQuerryString("taxChargeStatus");
					temp.setBooleanvalue(true);
					filter.add(temp);
					
					querry.setFilters(filter);
					querry.setQuerryObject(new TaxDetails());
					olbcstpercent.MakeLive(querry);
				}
	    	 };
	    	 timer.schedule(1500);
		}
		
		
		/**********************************Change Widgets Method******************************************/
		/**
		 * This method represents changing the widgets style. Used for modifying payment composite
		 * widget fields
		 */
		
		private void changeWidgets()
		{
			//  For changing the text alignments.
			this.changeTextAlignment(dototalamt);
			this.changeTextAlignment(donetpayamt);
			this.changeTextAlignment(doamtincltax);
			
			//  For changing widget widths.
			this.changeWidth(dototalamt, 200, Unit.PX);
			this.changeWidth(donetpayamt, 200, Unit.PX);
			this.changeWidth(doamtincltax, 200, Unit.PX);
			
			/**
			 * Date : 19-09-2017 By ANIL
			 */
			this.changeTextAlignment(dbOtherChargesTotal);
			this.changeWidth(dbOtherChargesTotal, 200, Unit.PX);
			
			/** Date 08-06-2018 By vijay **/
			this.changeTextAlignment(dbfinalTotalAmt);
			this.changeTextAlignment(tbroundoffAmt);
			this.changeWidth(dbfinalTotalAmt,200,Unit.PX);
			this.changeWidth(tbroundoffAmt, 200, Unit.PX);
		}
		
		/*******************************Text Alignment Method******************************************/
		/**
		 * Method used for align of payment composite fields
		 * @param widg
		 */
		
		private void changeTextAlignment(Widget widg)
		{
			widg.getElement().getStyle().setTextAlign(TextAlign.CENTER);
		}
		
		/*****************************Change Width Method**************************************/
		/**
		 * Method used for changing width of fields
		 * @param widg
		 * @param size
		 * @param unit
		 */
		
		private void changeWidth(Widget widg,double size,Unit unit)
		{
			widg.getElement().getStyle().setWidth(size, unit);
		}

		public void checkCustomerStatus(int cCount)
		{
			MyQuerry querry=new MyQuerry();
			Company c=new Company();
			Vector<Filter> filtervec=new Vector<Filter>();
			Filter temp=null;
			
			temp=new Filter();
			temp.setQuerryString("companyId");
			temp.setLongValue(c.getCompanyId());
			filtervec.add(temp);
			
			temp=new Filter();
			temp.setQuerryString("count");
			temp.setIntValue(cCount);
			filtervec.add(temp);
			
			querry.setFilters(filtervec);
			querry.setQuerryObject(new Customer());
			
			genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onFailure(Throwable caught) {
					showDialogMessage("An Unexpected error occurred!");
				}

				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
						for(SuperModel model:result)
						{
							Customer custEntity = (Customer)model;
							
							if(custEntity.getNewCustomerFlag()==true){
								showDialogMessage("Please fill customer information by editing customer");
								personInfoComposite.clear();
							}
							else if(AppConstants.ACTIVE.equals(custEntity.getStatus().trim())&&(custEntity.getNewCustomerFlag()==false)){
								 getPersonInfoComposite().getId().getElement().addClassName("personactive");
								 getPersonInfoComposite().getName().getElement().addClassName("personactive");
								 getPersonInfoComposite().getPhone().getElement().addClassName("personactive");
								 getPersonInfoComposite().getTbpocName().getElement().addClassName("personactive");
							}
							
							cust = custEntity;
							
						}
						hideWaitSymbol();
						if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT || AppMemory.getAppMemory().currentState==ScreeenState.NEW){
							updateTaxesDetails(cust);
						}
					}
				 });
			}
		
		/*******************************************Type Drop Down Logic**************************************/
		
		@Override
		public void onChange(ChangeEvent event) {
			
			if(event.getSource().equals(oblCustomerBranch)){
				if(oblCustomerBranch.getSelectedIndex()!=0){
					CustomerBranchDetails obj=oblCustomerBranch.getSelectedItem();
					if(obj!=null){
						if(obj.getPocName()!=null){
							tbCustPocName.setValue(obj.getPocName());
							acshippingcomposite.setValue(obj.getAddress());
							billingAddress=obj.getBillingAddress();
						}
						
						/**
						 * @author Anil @since 11-11-2021
						 * copying site wise budget details
						 * for Sunrise SCM
						 */
						if(uploadConsumableFlag){
							if(cncEntity!=null){
								if(cncEntity.getConsumablesList()!=null&&cncEntity.getConsumablesList().size()!=0){
									boolean updateFlag=false;
									for(Consumables con:cncEntity.getConsumablesList()){
										if(con.getSiteLocation().equals(oblCustomerBranch.getValue())){
											if(con.getBillingType().equals("ACTUALS")){
												tbBudget.setValue(con.getOpsBudget());
											}else{
												tbBudget.setValue(con.getChargable());
											}
											billingType=con.getBillingType();
											costCode=con.getCostCode();
											carpetArea=con.getBranchCarpetArea();
											updateFlag=true;
											break;
										}
									}
									
									if(!updateFlag){
										tbBudget.setText("");
										billingType="";
										costCode="";
										carpetArea="";
									}
								}
							}
						}
					}
				}else{
					tbCustPocName.setValue("");
					acshippingcomposite.clear();
					billingAddress=null;
					
					tbBudget.setText("");
					billingType="";
					costCode="";
					carpetArea="";
				}
			}
			
			if(event.getSource().equals(olbSalesOrderCategory))
			{
				if(olbSalesOrderCategory.getSelectedIndex()!=0){
					ConfigCategory cat=olbSalesOrderCategory.getSelectedItem();
					if(cat!=null){
						AppUtility.makeLiveTypeDropDown(olbSalesOrderType, cat.getCategoryName(), cat.getCategoryCode(), cat.getInternalType());
					}
				}
			}
			
			/**
			 * Date 08/06/2018
			 * Developer :- Vijay
			 * Des :- for round off amount chnage calculation and change net payable accourdingly
			 */
			if(event.getSource().equals(tbroundoffAmt)){
				NumberFormat nf = NumberFormat.getFormat("0.00");
				double netPay=this.getDbfinalTotalAmt().getValue();
				this.getDbfinalTotalAmt().setValue(netPay);
				if(tbroundoffAmt.getValue().equals("") || tbroundoffAmt.getValue()==null){
					this.getDonetpayamt().setValue(netPay);
				}else{
					if(!tbroundoffAmt.getValue().equals("")&& tbroundoffAmt.getValue()!=null){
						String roundOff = tbroundoffAmt.getValue();
						double roundoffAmt =  AppUtility.calculateRoundOff(roundOff, netPay);
						if(roundoffAmt!=0){
							this.getDonetpayamt().setValue(roundoffAmt);
						}else{
							this.getDonetpayamt().setValue(netPay);
							this.getTbroundoffAmt().setValue("");
						}
					}
				}
			}
			/**
			 * ends here
			 */
			
			if(event.getSource().equals(tbReferenceNumber)){
				cncEntity = null;
//				if(tbReferenceNumber.getValue()!=null && !tbReferenceNumber.getValue().equals("") && AppUtility.checkForProcessConfigurartionIsActiveOrNot("SalesOrder", "ValidateCNCActualBIllingBudget") ){
				if(tbReferenceNumber.getValue()!=null && !tbReferenceNumber.getValue().equals("")){	
					reactonLoadCNC(tbReferenceNumber.getValue());
				}
			}
			
			if(event.getSource().equals(olbcNumberRange)){
				updateTaxesDetails(cust);
			}
			
			
			
		}
		
		
		

		/***********************************Getters And Setters************************************/
		
		public ObjectListBox<Branch> getOlbbBranch() {
			return olbbBranch;
		}

		public void setOlbbBranch(ObjectListBox<Branch> olbbBranch) {
			this.olbbBranch = olbbBranch;
		}

		
		public TextBox getTbReferenceNumber() {
			return tbReferenceNumber;
		}

		public void setTbReferenceNumber(TextBox tbReferenceNumber) {
			this.tbReferenceNumber = tbReferenceNumber;
		}

		public ObjectListBox<Config> getOlbcPaymentMethods() {
			return olbcPaymentMethods;
		}

		public void setOlbcPaymentMethods(ObjectListBox<Config> olbcPaymentMethods) {
			this.olbcPaymentMethods = olbcPaymentMethods;
		}

		public TextBox getTbContractId() {
			return tbContractId;
		}

		public void setTbContractId(TextBox tbContractId) {
			this.tbContractId = tbContractId;
		}

		public TextBox getTbRefeRRedBy() {
			return tbRefeRRedBy;
		}

		public void setTbRefeRRedBy(TextBox tbRefeRRedBy) {
			this.tbRefeRRedBy = tbRefeRRedBy;
		}

		public TextBox getTbQuotationStatus() {
			return tbQuotationStatus;
		}

		public void setTbQuotationStatus(TextBox tbQuotationStatus) {
			this.tbQuotationStatus = tbQuotationStatus;
		}

		public TextArea getTaDescription() {
			return taDescription;
		}

		public void setTaDescription(TextArea taDescription) {
			this.taDescription = taDescription;
		}

		
		
		
		public DoubleBox getDbVendorPrice() {
			return dbVendorPrice;
		}

		public void setDbVendorPrice(DoubleBox dbVendorPrice) {
			this.dbVendorPrice = dbVendorPrice;
		}

		public TextArea getTaDescriptionTwo() {
			return taDescriptionTwo;
		}

		public void setTaDescriptionTwo(TextArea taDescriptionTwo) {
			this.taDescriptionTwo = taDescriptionTwo;
		}

		public UploadComposite getUcUploadTAndCs() {
			return ucUploadTAndCs;
		}

		public void setUcUploadTAndCs(UploadComposite ucUploadTAndCs) {
			this.ucUploadTAndCs = ucUploadTAndCs;
		}

		public PersonInfoComposite getPersonInfoComposite() {
			return personInfoComposite;
		}

		public void setPersonInfoComposite(PersonInfoComposite personInfoComposite) {
			this.personInfoComposite = personInfoComposite;
		}

		public ObjectListBox<Employee> getOlbeSalesPerson() {
			return olbeSalesPerson;
		}

		public void setOlbeSalesPerson(ObjectListBox<Employee> olbeSalesPerson) {
			this.olbeSalesPerson = olbeSalesPerson;
		}

		public ObjectListBox<Employee> getOlbApproverName() {
			return olbApproverName;
		}

		public void setOlbApproverName(ObjectListBox<Employee> olbApproverName) {
			this.olbApproverName = olbApproverName;
		}

		public ObjectListBox<Config> getOlbSalesOrderGroup() {
			return olbSalesOrderGroup;
		}

		public void setOlbSalesOrderGroup(ObjectListBox<Config> olbSalesOrderGroup) {
			this.olbSalesOrderGroup = olbSalesOrderGroup;
		}

		public ObjectListBox<Type> getOlbSalesOrderType() {
			return olbSalesOrderType;
		}

		public void setOlbSalesOrderType(ObjectListBox<Type> olbSalesOrderType) {
			this.olbSalesOrderType = olbSalesOrderType;
		}

		public ObjectListBox<ConfigCategory> getOlbSalesOrderCategory() {
			return olbSalesOrderCategory;
		}

		public void setOlbSalesOrderCategory(
				ObjectListBox<ConfigCategory> olbSalesOrderCategory) {
			this.olbSalesOrderCategory = olbSalesOrderCategory;
		}

		public DoubleBox getDototalamt() {
			return dototalamt;
		}

		public void setDototalamt(DoubleBox dototalamt) {
			this.dototalamt = dototalamt;
		}

		public DoubleBox getDoamtincltax() {
			return doamtincltax;
		}

		public void setDoamtincltax(DoubleBox doamtincltax) {
			this.doamtincltax = doamtincltax;
		}

		public DoubleBox getDonetpayamt() {
			return donetpayamt;
		}

		public void setDonetpayamt(DoubleBox donetpayamt) {
			this.donetpayamt = donetpayamt;
		}

		public TextBox getTbLeadId() {
			return tbLeadId;
		}

		public void setTbLeadId(TextBox tbLeadId) {
			this.tbLeadId = tbLeadId;
		}

		public TextBox getTbQuotatinId() {
			return tbQuotatinId;
		}

		public void setTbQuotatinId(TextBox tbQuotatinId) {
			this.tbQuotatinId = tbQuotatinId;
		}

		public IntegerBox getIbdays() {
			return ibdays;
		}

		public void setIbdays(IntegerBox ibdays) {
			this.ibdays = ibdays;
		}

		public DoubleBox getDopercent() {
			return dopercent;
		}

		public void setDopercent(DoubleBox dopercent) {
			this.dopercent = dopercent;
		}

		public TextBox getTbcomment() {
			return tbcomment;
		}

		public void setTbcomment(TextBox tbcomment) {
			this.tbcomment = tbcomment;
		}

		public PaymentTermsTable getPaymentTermsTable() {
			return paymentTermsTable;
		}

		public void setPaymentTermsTable(PaymentTermsTable paymentTermsTable) {
			this.paymentTermsTable = paymentTermsTable;
		}

		public SalesOrderLineItemTable getSalesorderlineitemtable() {
			return salesorderlineitemtable;
		}

		public void setSalesorderlineitemtable(
				SalesOrderLineItemTable salesorderlineitemtable) {
			this.salesorderlineitemtable = salesorderlineitemtable;
		}

		public ProductChargesTable getChargesTable() {
			return chargesTable;
		}

		public void setChargesTable(ProductChargesTable chargesTable) {
			this.chargesTable = chargesTable;
		}

		public ProductTaxesTable getProdTaxTable() {
			return prodTaxTable;
		}

		public void setProdTaxTable(ProductTaxesTable prodTaxTable) {
			this.prodTaxTable = prodTaxTable;
		}

		public DateBox getDbdeliverydate() {
			return dbdeliverydate;
		}

		public void setDbdeliverydate(DateBox dbdeliverydate) {
			this.dbdeliverydate = dbdeliverydate;
		}

		public AddressComposite getAcshippingcomposite() {
			return acshippingcomposite;
		}

		public void setAcshippingcomposite(AddressComposite acshippingcomposite) {
			this.acshippingcomposite = acshippingcomposite;
		}

		public CheckBox getCbcheckaddress() {
			return cbcheckaddress;
		}

		public void setCbcheckaddress(CheckBox cbcheckaddress) {
			this.cbcheckaddress = cbcheckaddress;
		}

		public ListBox getCbcformlis() {
			return cbcformlis;
		}

		public void setCbcformlis(ListBox cbcformlis) {
			this.cbcformlis = cbcformlis;
		}

		public ProductInfoComposite getProdInfoComposite() {
			return prodInfoComposite;
		}

		public void setProdInfoComposite(ProductInfoComposite prodInfoComposite) {
			this.prodInfoComposite = prodInfoComposite;
		}

		public ObjectListBox<TaxDetails> getOlbcstpercent() {
			return olbcstpercent;
		}

		public void setOlbcstpercent(ObjectListBox<TaxDetails> olbcstpercent) {
			this.olbcstpercent = olbcstpercent;
		}

		public IntegerBox getIbCreditPeriod() {
			return ibCreditPeriod;
		}

		public void setIbCreditPeriod(IntegerBox ibCreditPeriod) {
			this.ibCreditPeriod = ibCreditPeriod;
		}

		public Button getAddPaymentTerms() {
			return addPaymentTerms;
		}

		public void setAddPaymentTerms(Button addPaymentTerms) {
			this.addPaymentTerms = addPaymentTerms;
		}

		public Button getAddOtherCharges() {
			return addOtherCharges;
		}

		public void setAddOtherCharges(Button addOtherCharges) {
			this.addOtherCharges = addOtherCharges;
		}

		public Button getBaddproducts() {
			return baddproducts;
		}

		public void setBaddproducts(Button baddproducts) {
			this.baddproducts = baddproducts;
		}

		public TextBox getTbremark() {
			return tbremark;
		}

		public void setTbremark(TextBox tbremark) {
			this.tbremark = tbremark;
		}
		
		
		public OtherChargesTable getTblOtherCharges() {
			return tblOtherCharges;
		}

		public void setTblOtherCharges(OtherChargesTable tblOtherCharges) {
			this.tblOtherCharges = tblOtherCharges;
		}

		public DoubleBox getDbfinalTotalAmt() {
			return dbfinalTotalAmt;
		}

		public void setDbfinalTotalAmt(DoubleBox dbfinalTotalAmt) {
			this.dbfinalTotalAmt = dbfinalTotalAmt;
		}

		public TextBox getTbroundoffAmt() {
			return tbroundoffAmt;
		}

		public void setTbroundoffAmt(TextBox tbroundoffAmt) {
			this.tbroundoffAmt = tbroundoffAmt;
		}
		public DateBox getDbsalesorderdate() {
			return dbsalesorderdate;
		}

		public void setDbsalesorderdate(DateBox dbsalesorderdate) {
			this.dbsalesorderdate = dbsalesorderdate;
		}

		public TextBox getTbPOId() {
			return tbPOId;
		}

		public void setTbPOId(TextBox tbPOId) {
			this.tbPOId = tbPOId;
		}

		public TextBox getTbGRNId() {
			return tbGRNId;
		}

		public void setTbGRNId(TextBox tbGRNId) {
			this.tbGRNId = tbGRNId;
		}

		public TextBox getIbRefOrderNo() {
			return ibRefOrderNo;
		}

		public void setIbRefOrderNo(TextBox ibRefOrderNo) {
			this.ibRefOrderNo = ibRefOrderNo;
		}

		private void reactonLoadCNC(String OrderId) {

			cncEntity = null;
			
			int cncOrderId = Integer.parseInt(OrderId);
			MyQuerry querry = new MyQuerry();

			Vector<Filter> filtervec = new Vector<Filter>();
			Filter filter = null;
			
			filter = new Filter();
			filter.setQuerryString("count");
			filter.setIntValue(cncOrderId);
			filtervec.add(filter);
			
			querry.setFilters(filtervec);
			querry.setQuerryObject(new CNC());
			
			genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					// TODO Auto-generated method stub
					if(result.size()!=0){
					for(SuperModel model : result){
						CNC cnc = (CNC) model;
						cncEntity = cnc;
						break;
					}
					
					/**
					 * @author Anil @since 11-11-2021
					 * copying site wise budget details
					 * for Sunrise SCM
					 */
					if(uploadConsumableFlag){
						if(cncEntity!=null){
							if(cncEntity.getConsumablesList()!=null&&cncEntity.getConsumablesList().size()!=0){
								boolean updateFlag=false;
								for(Consumables con:cncEntity.getConsumablesList()){
									if(con.getSiteLocation().equals(oblCustomerBranch.getValue())){
										if(con.getBillingType().equals("ACTUALS")){
											tbBudget.setValue(con.getOpsBudget());
										}else{
											tbBudget.setValue(con.getChargable());
										}
										billingType=con.getBillingType();
										costCode=con.getCostCode();
										carpetArea=con.getBranchCarpetArea();
										updateFlag=true;
										break;
									}
								}
								
								if(!updateFlag){
									tbBudget.setText("");
									billingType="";
									costCode="";
									carpetArea="";
								}
							}
						}
					}
					}
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					
				}
			});
					
		
			
		}
		
		@Override
		public void refreshTableData() {
			// TODO Auto-generated method stub
			super.refreshTableData();
			prodTaxTable.getTable().redraw();
			chargesTable.getTable().redraw();
			salesorderlineitemtable.getTable().redraw();
			paymentTermsTable.getTable().redraw();
			tblOtherCharges.getTable().redraw();
			
		}
		
		protected void updateTaxesDetails(Customer cust) {
			List<SalesLineItem> prodlist = salesorderlineitemtable.getDataprovider().getList();
			if(prodlist.size()>0){
				Branch branchEntity =olbbBranch.getSelectedItem();
				
				boolean gstApplicable = false;
				if(olbcNumberRange.getSelectedIndex()!=0){
					Config configEntity = olbcNumberRange.getSelectedItem();
					if(configEntity!=null)
					gstApplicable = configEntity.isGstApplicable();
				}
				else{
					gstApplicable = true;
				}
				
				if(cust!=null ){
					System.out.println("customer onselection");
					 String customerBillingaddressState = cust.getAdress().getState();
					 List<SalesLineItem> productlist = AppUtility.updateTaxesDetails(prodlist, branchEntity, customerBillingaddressState,gstApplicable);
					 salesorderlineitemtable.getDataprovider().setList(productlist);
					 salesorderlineitemtable.getTable().redraw();
					RowCountChangeEvent.fire(salesorderlineitemtable.getTable(), salesorderlineitemtable.getDataprovider().getList().size(), true);
					setEnable(true);
				}
				
			}
		}
		
		public void loadPaymentMode() {
			try {
				 GenricServiceAsync async =  GWT.create(GenricService.class);

				MyQuerry querry = new MyQuerry();
				
				
				Company c = new Company();
				Vector<Filter> filtervec = new Vector<Filter>();
				Filter filter = null;
				
				filter = new Filter();
				filter.setQuerryString("companyId");
				filter.setLongValue(c.getCompanyId());
				filtervec.add(filter);
				
				querry.setFilters(filtervec);
				
				
				querry.setQuerryObject(new CompanyPayment());

				async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
					
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						// TODO Auto-generated method stub

						System.out.println(" in side date load size"+result.size());
						Console.log("get result size of payment mode --" + result.size());
						objPaymentMode.clear();
						objPaymentMode.addItem("--SELECT--");
						if (result.size() == 0) {

						} else {
							for (SuperModel model : result) {
								CompanyPayment payList = (CompanyPayment) model;
								paymentModeList.add(payList);
							}
							
							if (paymentModeList.size() != 0) {
								objPaymentMode.setListItems(paymentModeList);
							}
							
							for(int i=1;i<objPaymentMode.getItemCount();i++){
								String paymentDet = objPaymentMode.getItemText(i);
								Console.log("SERVICE DETAILS : "+paymentDet);
							}	
						}
					
					}
					
					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						
					}
				});
				

			} catch (Exception e) {
				e.printStackTrace();
			}
		}


}
