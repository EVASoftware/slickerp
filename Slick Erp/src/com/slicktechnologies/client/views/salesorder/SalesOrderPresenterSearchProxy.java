package com.slicktechnologies.client.views.salesorder;

import java.util.Vector;

import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.config.CategoryTypeFactory;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.salesorder.SalesOrderPresenter.SalesOrderPresenterSearch;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;

public class SalesOrderPresenterSearchProxy extends SalesOrderPresenterSearch {
	
	public PersonInfoComposite personInfo;
	public IntegerBox tbContractId;
	public IntegerBox tbQuotationId;
	public IntegerBox tbLeadId;
	public ObjectListBox<Branch> olbBranch;
	public ObjectListBox<Employee> olbEmployee;
	public DateComparator dateComparator;
	public ObjectListBox<Config> olbcNumberRange;//Added by Ashwini
	public ObjectListBox<Config> olbContractGroup;
	public ObjectListBox<ConfigCategory> olbContractCategory;
	public ObjectListBox<Type> olbContractType;
	public ListBox olbContractStatus;
	public ObjectListBox<Config> olbContractPriority;
	/** date 18.9.2018 added by komal for reference number search **/
	public TextBox tbReferenceNumber;
	/** date 3.10.2018 added by komal for po and grn id**/
	public TextBox tbPOId;
	public TextBox tbGRNId;

	public Object getVarRef(String varName)
	{
		if(varName.equals("personInfo"))
			return this.personInfo;
		if(varName.equals("tbContractId"))
			return this.tbContractId;
		if(varName.equals("tbQuotationId"))
			return this.tbQuotationId;
		if(varName.equals("tbLeadId"))
			return this.tbLeadId;
		if(varName.equals("olbBranch"))
			return this.olbBranch;
		if(varName.equals("olbEmployee"))
			return this.olbEmployee;
		if(varName.equals("dateComparator"))
			return this.dateComparator;
		
		if(varName.equals("olbQuotationGroup"))
			return this.olbContractGroup;
					
		if(varName.equals("olbQuotationCategory"))
			return this.olbContractCategory;
					
		if(varName.equals("olbQuotationType"))
			return this.olbContractType;
					
		if(varName.equals("olbQuotationStatus"))
			return this.olbContractStatus;
								
		if(varName.equals("olbQuotationPriority"))
			return this.olbContractPriority;
		/*
		 * Date:20/07/2018
		 * Developer:Ashwini
		 * Purpose:To add number range coloumn in search popup
		 */
		if(varName.equals("olbcNumberRange"))
			return this.olbcNumberRange;
		/*
		 * end by Ashwini
		 */
		if(varName.equals("tbReferenceNumber"))
			return this.tbReferenceNumber;
		/** date 3.10.2018 added by komal for po and grn id**/
		if(varName.equals("tbPOId"))
			return this.tbPOId;
		if(varName.equals("tbGRNId"))
			return this.tbGRNId;
		return null ;
	}
	public SalesOrderPresenterSearchProxy()
	{
		super();
		createGui();
	}
	public void initWidget()
	{
		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new Customer());
		personInfo=new PersonInfoComposite(querry,false);
		tbContractId= new IntegerBox();
		tbQuotationId= new IntegerBox();
		tbLeadId= new IntegerBox();
		olbBranch= new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbBranch);
		olbEmployee= new ObjectListBox<Employee>();
//		AppUtility.makeSalesPersonListBoxLive(olbEmployee);
		olbEmployee.makeEmployeeLive(AppConstants.SALESMODULE, AppConstants.SALESORDER, "Sales Person");
		
		dateComparator= new DateComparator("creationDate",new SalesOrder());
		
		olbContractGroup=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbContractGroup, Screen.SALESORDERGROUP);
		
		olbContractCategory=new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(olbContractCategory, Screen.SALESORDERCATEGORY);
		
		olbContractType=new ObjectListBox<Type>();
		AppUtility.makeTypeListBoxLive(olbContractType, Screen.SALESORDERTYPE);
		
		olbContractStatus=new ListBox();
		AppUtility.setStatusListBox(olbContractStatus, SalesOrder.getStatusList());
		
		/*
		 * Date:20/07/2018
		 * Developer:Ashwini
		 */
		
		olbcNumberRange = new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbcNumberRange, Screen.NUMBERRANGE);
		
		/*
		 * End by Ashwini
		 */
		
		//olbContractPriority=new ObjectListBox<Config>();
		//AppUtility.MakeLiveConfig(olbContractPriority, Screen.CONTRACTPRIORITY);
//		CategoryTypeFactory.initiateOneManyFunctionality(olbContractCategory,olbContractType);
		
		personInfo.getCustomerId().getHeaderLabel().setText("Customer ID");
		personInfo.getCustomerName().getHeaderLabel().setText("Customer Name");
		personInfo.getCustomerCell().getHeaderLabel().setText("Customer Cell");
		/** date 18.9.2018 added by komal for reference number search **/
		tbReferenceNumber = new TextBox();
		/** date 3.10.2018 added by komal for po and grn id**/
		tbPOId = new TextBox();
		tbGRNId = new TextBox();

	}
	public void createScreen()
	{
		initWidget();
		FormFieldBuilder builder;
		builder = new FormFieldBuilder("",personInfo);
		FormField fpersonInfo= builder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		builder = new FormFieldBuilder("Sales Order Id",tbContractId);
		FormField ftbContractId= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Quotation Id",tbQuotationId);
		FormField ftbQuotationId= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Lead Id",tbLeadId);
		FormField ftbLeadId= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Branch",olbBranch);
		FormField folbBranch= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Sales Person",olbEmployee);
		FormField folbEmployee= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("From Date",dateComparator.getFromDate());
		FormField fdateComparator= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("To Date",dateComparator.getToDate());
		FormField fdateComparator1= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		builder = new FormFieldBuilder("Sales Order Group",olbContractGroup);
		FormField folbQuotationGroup= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Sales Order Category",olbContractCategory);
		FormField folbQuotationCategory= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Sales Order Type",olbContractType);
		FormField folbQuotationType= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Sales Order Status",olbContractStatus);
		FormField folbQuotationStatus= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/*
		 * Date:21/07/2018
		 * Developer:Ashwini
		 * Des:To add number range coloumn in sales order search popup.
		 */
		FormField folbcNumberRange;	
		builder = new FormFieldBuilder("Number Range",olbcNumberRange);
		folbcNumberRange= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/*
		 * End by Ashwini
		 */
		
		//builder = new FormFieldBuilder("Contract Priority",olbContractPriority);
		//FormField folbQuotationPriority= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		/** date 18.9.2018 added by komal for reference number search **/
		FormField ftbReferenceNumber;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("SalesOrder", "MakeRefNumberMandatory")){
			builder = new FormFieldBuilder("CNC Number",tbReferenceNumber);
			ftbReferenceNumber= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}else{
			builder = new FormFieldBuilder("Reference Number",tbReferenceNumber);
			ftbReferenceNumber= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
		/** date 3.10.2018 added by komal for po and grn id**/
		builder = new FormFieldBuilder("PO Id",tbPOId);
		FormField ftbPOId= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("GRN Id",tbGRNId);
		FormField ftbGRNId= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();


		this.fields=new FormField[][]{
				{fdateComparator,fdateComparator1,folbQuotationGroup,folbQuotationCategory,folbQuotationType},
//				{folbQuotationStatus,folbEmployee,folbBranch,},//Commented by Ashwini
				{folbQuotationStatus,folbEmployee,folbBranch,folbcNumberRange,ftbReferenceNumber},//Added by Ashwini
				{ftbLeadId,ftbQuotationId,ftbContractId,ftbPOId ,ftbGRNId},/** date 3.10.2018 added by komal for po and grn id**/
				{fpersonInfo}
		};
	}
	
	public MyQuerry getQuerry()
	{
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		if(dateComparator.getValue()!=null)
		{
			filtervec.addAll(dateComparator.getValue());
		}
		if(olbEmployee.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbEmployee.getValue().trim());
			temp.setQuerryString("employee");
			filtervec.add(temp);
		}
		if(olbBranch.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbBranch.getValue().trim());
			temp.setQuerryString("branch");
			filtervec.add(temp);
		}
		if(personInfo.getIdValue()!=-1)
		  {
		  temp=new Filter();
		  temp.setIntValue(personInfo.getIdValue());
		  temp.setQuerryString("cinfo.count");
		  filtervec.add(temp);
		  }
		  
		  if(!(personInfo.getFullNameValue().equals("")))
		  {
		  temp=new Filter();
		  temp.setStringValue(personInfo.getFullNameValue());
		  temp.setQuerryString("cinfo.fullName");
		  filtervec.add(temp);
		  }
		  if(personInfo.getCellValue()!=-1l)
		  {
		  temp=new Filter();
		  temp.setLongValue(personInfo.getCellValue());
		  temp.setQuerryString("cinfo.cellNumber");
		  filtervec.add(temp);
		  }
		  if(tbQuotationId.getValue()!=null){
				temp=new Filter();
				temp.setIntValue(tbQuotationId.getValue());
				filtervec.add(temp);
				temp.setQuerryString("quotationCount");
				filtervec.add(temp);
			}
			if(tbLeadId.getValue()!=null){
				temp=new Filter();
				temp.setIntValue(tbLeadId.getValue());
				filtervec.add(temp);
				temp.setQuerryString("leadCount");
				filtervec.add(temp);
			}
			if(tbContractId.getValue()!=null){
				temp=new Filter();
				temp.setIntValue(tbContractId.getValue());
				filtervec.add(temp);
				temp.setQuerryString("count");
				filtervec.add(temp);
			}
			
		
		if(olbContractGroup.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(olbContractGroup.getValue().trim());
			temp.setQuerryString("group");
			filtervec.add(temp);
		}
		
		if(olbContractCategory.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbContractCategory.getValue().trim());
			temp.setQuerryString("category");
			filtervec.add(temp);
		}
		
		if(olbContractType.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbContractType.getValue().trim());
			temp.setQuerryString("type");
			filtervec.add(temp);
		}
		
		if(olbContractStatus.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbContractStatus.getValue(olbContractStatus.getSelectedIndex()).trim());
			temp.setQuerryString("status");
			filtervec.add(temp);
		}
		
		/*
		   * Date:21/07/2018
		   * Developer:Ashwini
		   * Des:to filter number range i.e billing or non billing
		   */

		if(olbcNumberRange.getSelectedIndex()!=0){
			System.out.println("Number Range");
			temp=new Filter();
			temp.setStringValue(olbcNumberRange.getValue().trim());
			temp.setQuerryString("numberRange");
			filtervec.add(temp);
		}
		
		   /*
			 * End by Ashwini
			 */
		   
		/**
		 * date 18.9.2018 added by komal for sasha to search sales order using cncId 
		 */
		if(tbReferenceNumber.getValue()!=null && !tbReferenceNumber.getValue().equals("")){
			temp=new Filter();
			temp.setStringValue(tbReferenceNumber.getValue().trim());
			temp.setQuerryString("refNo");
			filtervec.add(temp);
		}
		/** date 3.10.2018 added by komal for po and grn id**/
		if(tbPOId.getValue() != null && !tbPOId.getValue().equals("")){
			temp=new Filter();
			temp.setIntValue(Integer.parseInt(tbPOId.getValue()));
			temp.setQuerryString("poId");
			filtervec.add(temp);
		}
		if(tbGRNId.getValue() != null && !tbGRNId.equals("") && tbGRNId.getValue().trim().length()>0){
			temp=new Filter();
			temp.setIntValue(Integer.parseInt(tbGRNId.getValue()));
			temp.setQuerryString("grnId");
			filtervec.add(temp);
		}
		MyQuerry querry= new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new SalesOrder());
		return querry;
	}
	@Override
	public boolean validate() {
		if(LoginPresenter.branchRestrictionFlag)
		{
		if(olbBranch.getSelectedIndex()==0)
		{
			showDialogMessage("Select Branch");
			return false;
		}
		}	
		
		

		
		//Ashwini Patil Date:25-01-2024 	

		boolean filterAppliedFlag=false;
		if(dateComparator.getValue()!=null)
		{
			Console.log("1");
		}
		if(olbEmployee.getSelectedIndex()!=0){
			Console.log("2");
			filterAppliedFlag=true;
		}
		if(olbBranch.getSelectedIndex()!=0){
			Console.log("3");
			filterAppliedFlag=true;
		}
		if(personInfo.getIdValue()!=-1)
		  {
			Console.log("4");
			filterAppliedFlag=true;
		  }
		  
		  if(!(personInfo.getFullNameValue().equals("")))
		  {
			  Console.log("5");
			  filterAppliedFlag=true;
		  }
		  if(personInfo.getCellValue()!=-1l)
		  {
			  Console.log("6");
			  filterAppliedFlag=true;
		  }
		  if(tbQuotationId.getValue()!=null){
			  Console.log("7");
			  filterAppliedFlag=true;
			}
			if(tbLeadId.getValue()!=null){
				Console.log("8");
				filterAppliedFlag=true;
			}
			if(tbContractId.getValue()!=null){
				Console.log("9");
				filterAppliedFlag=true;
			}
			
		
		if(olbContractGroup.getSelectedIndex()!=0){
			Console.log("10");
			filterAppliedFlag=true;
		}
		
		if(olbContractCategory.getSelectedIndex()!=0){
			Console.log("11");
			filterAppliedFlag=true;
		}
		
		if(olbContractType.getSelectedIndex()!=0){
			Console.log("12");
			filterAppliedFlag=true;
		}
		
		if(olbContractStatus.getSelectedIndex()!=0){
			Console.log("13");
			filterAppliedFlag=true;
		}
		if(olbcNumberRange.getSelectedIndex()!=0){
			Console.log("14");
			filterAppliedFlag=true;
		}
		
		if(tbReferenceNumber.getValue()!=null && !tbReferenceNumber.getValue().equals("")){
			Console.log("15");
			filterAppliedFlag=true;
		}
		if(tbPOId.getValue() != null && !tbPOId.getValue().equals("")){
			Console.log("16");
			filterAppliedFlag=true;
		}
		if(tbGRNId.getValue() != null && !tbGRNId.equals("") && tbGRNId.getValue().trim().length()>0){
			Console.log("17");
			filterAppliedFlag=true;
		}
		

		if((dateComparator.getFromDate().getValue()!=null && dateComparator.getToDate().getValue()!=null)){
			filterAppliedFlag=true;
				Console.log("18");
		}
		
		if(!filterAppliedFlag){
				showDialogMessage("Please apply at least one filter!");
				return false;
		}
					
		if((dateComparator.getFromDate().getValue()!=null && dateComparator.getToDate().getValue()!=null)){
				int diffdays = AppUtility.getDifferenceDays(dateComparator.getFromDate().getValue(), dateComparator.getToDate().getValue());
				Console.log("diffdays "+diffdays);
				if(diffdays>31){
						showDialogMessage("Please select from date and to date range within 30 days");
						return false;
				}
		}

				
				
		
		
		return true;
	}
	
	
}
