package com.slicktechnologies.client.views.salesorder;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.SelectionCell;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.view.client.ProvidesKey;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.customerlog.amc.AMCAllocationPopup;
import com.slicktechnologies.client.views.inventory.recievingnote.InventoryLocationPopUp;
import com.slicktechnologies.client.views.inventory.recievingnote.StorageLocationBin;
import com.slicktechnologies.client.views.popups.ProductSerailNumberPopup;
import com.slicktechnologies.client.views.quicksalesorder.QuickSalesOrderForm;
import com.slicktechnologies.shared.ProductSerialNoMapping;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.TaxDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.inventory.Storagebin;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.productlayer.Tax;
import com.slicktechnologies.shared.common.salesorder.VendorMargin;
import com.slicktechnologies.shared.common.servicerelated.CompanyAsset;
import com.google.gwt.event.dom.client.ClickEvent;

public class SalesOrderLineItemTable extends SuperTable<SalesLineItem> implements ClickHandler {
	

	Column<SalesLineItem,String> deleteColumn;
	Column<SalesLineItem,String> vatColumn,serviceColumn,totalColumn;
	TextColumn<SalesLineItem> prodCategoryColumn,prodCodeColumn,viewProdNameColumn;
	Column<SalesLineItem, String> priceColumn;
	Column<SalesLineItem,String> prodQtyColumn;
	Column<SalesLineItem, String>prodPercentDiscColumn;
	TextColumn<SalesLineItem> viewdiscountAmt;
	TextColumn<SalesLineItem> viewprodQtyColumn,viewPriceColumn,viewprodPercDiscColumn;
	TextColumn<SalesLineItem> uomColumn,prodIdColumn;
	NumberFormat nf=NumberFormat.getFormat("0.00");
	public String checkUpdater="";
	
	Column<SalesLineItem,String> discountAmt;
	
	Column<SalesLineItem,String> prodNameColumn;
	
	/******************* vijay *****************/
//	Column<SalesLineItem,Date> warrantyUntildDateColumn;
	TextColumn<SalesLineItem> viewwarrantyUntildDateColumn;
	Column<SalesLineItem, String> warrantyInMonthsColumn;
	TextColumn<SalesLineItem> viewwarrentyInMonthsColumn;
	Date oldDate=null;
	
	
	/**
	 * Date 12 july 2017 added by vijay for GST Tax
	 */
	
	TextColumn<SalesLineItem> viewServiceTaxColumn;
	TextColumn<SalesLineItem> viewVatTaxColumn;
	
	public  ArrayList<TaxDetails> vattaxlist;
	public  ArrayList<TaxDetails> servicetaxlist;
	public  ArrayList<String> list;
	public  ArrayList<String> vatlist;
	
	TextColumn<SalesLineItem> viewHsnCodeColumn;
	Column<SalesLineItem,String> hsnCodeColumn;
	
	/**
	 * ends here
	 */

	
	/**
	 * Date 21 july 2017 added by vijay for Quick Sales
	 */
	
	InventoryLocationPopUp invLoc=new InventoryLocationPopUp();
	PopupPanel panel=new PopupPanel(true);
	int rowIndex;
	
	Column<SalesLineItem, String> addwarehouseBtnColumn;
	TextColumn<SalesLineItem> getColumnProductAvailableQty;
	public  ArrayList<Storagebin> storagelocList1;
	
	Column<SalesLineItem, String> warehouseColumn;
	TextColumn<SalesLineItem> warehouseViewColumn;
	
	Column<SalesLineItem, String> storagebin;
	Column<SalesLineItem, String> columnviewBin;
	
	Column<SalesLineItem, String> storageLoc;
	TextColumn<SalesLineItem> columnviewLoc;
	
	
	/**
	 * ends here
	 */
	
	/****** Date 24-08-2017 added by vijay for Area ****/
	Column<SalesLineItem, String> prodAreaColumn;
	TextColumn<SalesLineItem> viewProdAreaColumn;
	
	//NOTE//
	
	//   19December
	
	// In this table the total amount column is changed.
	// The value of total amount is calc by price*qty. And taxes which are inclusive are subtracted from price*qty
	
	
	/**
	 * Date : 16-04-2018 BY ANIL
	 * Below column shows the total days of warranty and no of services
	 */
	
	Column<SalesLineItem,String> getColNo_Of_Service;
	TextColumn<SalesLineItem> viewColNo_Of_Services;
	
	Column<SalesLineItem,String> getColWarrantyDuration;
	TextColumn<SalesLineItem> viewColWarrantyDuration;
	
	Column<SalesLineItem,String> getColPremiseDetails;
	TextColumn<SalesLineItem> viewColPremiseDetails;
	
	Column<SalesLineItem,Boolean> getColIsTaxInclusive;
	TextColumn<SalesLineItem> viewColIsTaxInclusive;
	
/** date 23/1/2018 added by komal for hvac (amc drop down) **/
	
	TextColumn<SalesLineItem> amcNumberViewColumn;
	
	AMCAllocationPopup amcPopup = new AMCAllocationPopup();
	
	Column<SalesLineItem, String> addAmcBtnColumn;
	
	SalesLineItem salesLineItem;
/***
 * end komal
 */
	/**
	 * nidhi
	 * 9-06-2018
	 * for productDetail
	 */
	Column<SalesLineItem, String> getDetailsButtonColumn;
	ItemProductDetailsPopUp itemDtPopUp = new ItemProductDetailsPopUp();
	/**
	 * end
	 */
	/**
	 * nidhi
	 * 21-08-2018
	 * for map serial number
	 */
	Column<SalesLineItem, String> addProSerialNo;
	Column<SalesLineItem, String> viewProSerialNo;
	boolean tableState = false;
	ProductSerailNumberPopup prodSerialPopup = new ProductSerailNumberPopup(true,false);
	HashMap<Integer, ArrayList<ProductSerialNoMapping>> proSerialList  = new HashMap<Integer, ArrayList<ProductSerialNoMapping>>();

	/**
	 * @author Anil
	 * @since 20-07-2020
	 */
	boolean marginFlag=AppUtility.checkForProcessConfigurartionIsActiveOrNot("SalesOrder","CalculateVendorAndOtherChargesMargin");
	
	public SalesOrderLineItemTable()
	{
		super();
		/**
		 * nidhi
		 * 
		 */
		itemDtPopUp.getLblCancel().addClickHandler(this);
		itemDtPopUp.getLblOk().addClickHandler(this);
		/**
		 * end
		 */
		invLoc.getAddButton().addClickHandler(this);
		invLoc.getCancelButton().addClickHandler(this);
		/**
		 * nidhi
		 * 22-08-2018
		 */
		prodSerialPopup.getLblOk().addClickHandler(this);
		prodSerialPopup.getLblCancel().addClickHandler(this);
		table.setHeight("300px");
		
	}
	public SalesOrderLineItemTable(UiScreen<SalesLineItem> view)
	{
		super(view);
		/**
		 * nidhi
		 * 
		 */
		itemDtPopUp.getLblCancel().addClickHandler(this);
		itemDtPopUp.getLblOk().addClickHandler(this);
		/**
		 * end
		 */
		/**
		 * nidhi
		 * 22-08-2018
		 */
		prodSerialPopup.getLblOk().addClickHandler(this);
		prodSerialPopup.getLblCancel().addClickHandler(this);
		table.setHeight("300px");
	}

	public void createTable()
	{
		//		createColumnprodCategoryColumn();
		createColumnprodNameColumn();
		createColumnUnitOfMeasurement();
		addColumnProductAvailableQty();
		createColumnprodQtyColumn();
		
		
		/**
		 * Date : 17-04-2018 BY ANIL
		 * Requirement by: HVAC (Rohan/Nitin Sir)
		 * Description : As we have decided to put area col value in qty itself ,
		 * so no need to have two different fields
		 * warranty duration will be coming from service product duration a
		 */
//		createColumnWarrantyInMonths();
//		// Date 21 july 2017 below column commented by vijay as per nitin sir
////		createColumnWarrantyUntilColumn();
//		createColumnprodAreaColumn();
		
		/**
		 * End
		 */
		

		
		createColumnpriceColumn();
		
		/**
		 * Date 10 july 2017 added by vijay other column added into on success method
		 */
		retriveVatandServiceTax();
		
//		createColumnvatColumn();
//		createColumnserviceColumn();
//		createColumnPercDiscount();
//		// rohan added this column 
//		createColumnDiscountAmt();
//		createColumntotalColumn();
//		createColumndeleteColumn();
//		addFieldUpdater();
		table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);	
		}
	
	
	
	/**
	 * Date : this cols added for capturing service details 
	 * which will be created at the time of sales invoice creation
	 */
	
	private void getColIsTaxInclusive() {
		
		
		
		CheckboxCell cell=new CheckboxCell();
		getColIsTaxInclusive=new Column<SalesLineItem, Boolean>(cell) {
			@Override
			public Boolean getValue(SalesLineItem object) {
				
				if(object.getVatTax()!=null&&object.getServiceTax()!=null){
					if(object.getVatTax().isInclusive()==true&&object.getServiceTax().isInclusive()==true){
						object.setInclusive(true);
					}else if(object.getVatTax().isInclusive()==false&&object.getServiceTax().isInclusive()==false){
						object.setInclusive(false);
					}else{
						object.setInclusive(false);
					}
				}
				
				return object.isInclusive();
			}
			
			@Override
			public void onBrowserEvent(Context context, Element elem,SalesLineItem object, NativeEvent event) {
				
				if(marginFlag==false){
					super.onBrowserEvent(context, elem, object, event);
					return;
				}
				
				if(object.isReadOnly()==false
						||((UserConfiguration.getRole()!=null&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin"))
						||(UserConfiguration.getRole()!=null&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Zonal Coordinator")))){
					super.onBrowserEvent(context, elem, object, event);
				}
			}
		};
		table.addColumn(getColIsTaxInclusive, "Is Inclusive");
		table.setColumnWidth(getColIsTaxInclusive,80,Unit.PX);
		
		getColIsTaxInclusive.setFieldUpdater(new FieldUpdater<SalesLineItem, Boolean>() {
			@Override
			public void update(int index, SalesLineItem object, Boolean value) {
				object.setInclusive(value);
				object.getServiceTax().setInclusive(value);
				object.getVatTax().setInclusive(value);
				
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				table.redraw();
			}
		});
	}
	
	private void viewColIsTaxInclusive() {
		viewColIsTaxInclusive=new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				// TODO Auto-generated method stub
				
				if(object.isInclusive()==true){
					return "Yes";
				}else{
					return "No";
				}
			}
		};
		table.addColumn(viewColIsTaxInclusive, "Is Inclusive");
		table.setColumnWidth(viewColIsTaxInclusive,80,Unit.PX);
	}
	
	private void getColPremiseDetails() {

		EditTextCell cell=new EditTextCell();
		getColPremiseDetails=new Column<SalesLineItem, String>(cell) {
			@Override
			public String getValue(SalesLineItem object) {
				if(object.getPremisesDetails()!=null){
					return object.getPremisesDetails()+"";
				}
				return "";
			}
			@Override
			public void onBrowserEvent(Context context, Element elem,SalesLineItem object, NativeEvent event) {
				
				if(marginFlag==false){
					super.onBrowserEvent(context, elem, object, event);
					return;
				}
				
				if(object.isReadOnly()==false
						||((UserConfiguration.getRole()!=null&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin"))
						||(UserConfiguration.getRole()!=null&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Zonal Coordinator")))){
					super.onBrowserEvent(context, elem, object, event);
				}
			}
		};
		table.addColumn(getColPremiseDetails, "#Premises");
		table.setColumnWidth(getColPremiseDetails,120,Unit.PX);
		
		getColPremiseDetails.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {
			@Override
			public void update(int index, SalesLineItem object, String value) {
				object.setPremisesDetails(value);
				table.redraw();
			}
		});
	
	}
	private void viewColPremiseDetails() {
		viewColPremiseDetails=new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				if(object.getPremisesDetails()!=null){
					return object.getPremisesDetails()+"";
				}
				return "";
			}
		};
		table.addColumn(viewColPremiseDetails, "#Premises");
		table.setColumnWidth(viewColPremiseDetails,120,Unit.PX);
	}
	private void getColNo_Of_Service() {
		EditTextCell cell=new EditTextCell();
		getColNo_Of_Service=new Column<SalesLineItem, String>(cell) {
			@Override
			public String getValue(SalesLineItem object) {
				return object.getNo_Of_Services()+"";
			}
		};
		table.addColumn(getColNo_Of_Service, "#No. Of Services");
		table.setColumnWidth(getColNo_Of_Service,70,Unit.PX);
		
		getColNo_Of_Service.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {
			@Override
			public void update(int index, SalesLineItem object, String value) {
				try{
					int no_of_services=Integer.parseInt(value);
					object.setNo_Of_Services(no_of_services);
				}catch(NumberFormatException e){
					GWTCAlert alert =new GWTCAlert();
					alert.alert("Please enter only numeric value.");
				}
				table.redraw();
			}
		});
	}
	private void viewColNo_Of_Services() {
		viewColNo_Of_Services=new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				return object.getNo_Of_Services()+"";
			}
		};
		table.addColumn(viewColNo_Of_Services, "No. Of Services");
		table.setColumnWidth(viewColNo_Of_Services,70,Unit.PX);
	}
	private void getColWarrantyDuration() {
		EditTextCell cell=new EditTextCell();
		getColWarrantyDuration=new Column<SalesLineItem, String>(cell) {
			@Override
			public String getValue(SalesLineItem object) {
				return object.getWarrantyDuration()+"";
			}
		};
		table.addColumn(getColWarrantyDuration, "Warranty (In Days)");
		table.setColumnWidth(getColWarrantyDuration,80,Unit.PX);
		
		getColWarrantyDuration.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {
			@Override
			public void update(int index, SalesLineItem object, String value) {
				try{
					int warrantyDuration=Integer.parseInt(value);
					object.setWarrantyDuration(warrantyDuration);
				}catch(NumberFormatException e){
					GWTCAlert alert =new GWTCAlert();
					alert.alert("Please enter only numeric value.");
				}
				table.redraw();
			}
		});
	}
	
	private void viewColWarrantyDuration() {
		viewColWarrantyDuration=new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				return object.getWarrantyDuration()+"";
			}
		};
		table.addColumn(viewColWarrantyDuration, "Warranty (In Days)");
		table.setColumnWidth(viewColWarrantyDuration,80,Unit.PX);
	}
	
	/**
	 * END
	 */
	

	/**** Date 24-08-2017 added by vijay for Area ****/
	private void createColumnprodAreaColumn() {
			EditTextCell editCell=new EditTextCell();
			prodAreaColumn = new Column<SalesLineItem, String>(editCell) {
				
				@Override
				public String getValue(SalesLineItem object) {
					if(object.getArea()!=null){
						return object.getArea();
					}else{
						return "";
					}
				}
				
				@Override
				public void onBrowserEvent(Context context, Element elem,SalesLineItem object, NativeEvent event) {
					
					if(marginFlag==false){
						super.onBrowserEvent(context, elem, object, event);
						return;
					}
					
					if(object.isReadOnly()==false
							||((UserConfiguration.getRole()!=null&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin"))
							||(UserConfiguration.getRole()!=null&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Zonal Coordinator")))){
						super.onBrowserEvent(context, elem, object, event);
					}
				}
			};
			
			table.addColumn(prodAreaColumn,"# Area");
			table.setColumnWidth(prodAreaColumn, 100,Unit.PX);
		}
	
	private void retriveVatandServiceTax() {

		final GenricServiceAsync service = GWT.create(GenricService.class);
		MyQuerry query = new MyQuerry();
		Company c=new Company();
		System.out.println("Company Id :: "+c.getCompanyId());
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		query.setFilters(filtervec);
		query.setQuerryObject(new TaxDetails());
		
		service.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
			}

		
			public void onSuccess(ArrayList<SuperModel> result){
				
				System.out.println("result size of tax "+result);
				list=new ArrayList<String>();
				servicetaxlist=new ArrayList<TaxDetails>();
				vattaxlist = new ArrayList<TaxDetails>();
				vatlist=new ArrayList<String>();
				List<TaxDetails> backlist=new ArrayList<TaxDetails>();
				
				for (SuperModel model : result) {
					TaxDetails entity = (TaxDetails) model;
					
					backlist.add(entity);
					servicetaxlist.add(entity);
					vattaxlist.add(entity);
				}
				
				for(int i=0;i<backlist.size();i++){
						list.add(backlist.get(i).getTaxChargeName());
						System.out.println("list size"+list.size());
						vatlist.add(backlist.get(i).getTaxChargeName());
						System.out.println("valist size"+vatlist.size());
				}
				
								
				/**
				 * Date : 17-04-2018 BY ANIL
				 */
				//getColIsTaxInclusive();
				/**
				 * END
				 */
				
				createColumnPercDiscount();
				// rohan added this column 
				createColumnDiscountAmt();
				createColumntotalColumn();
				addEditColumnVatTax();
				addEditColumnServiceTax();
				/**
				 * Date : 17-04-2018 BY ANIL
				 */
				getColIsTaxInclusive();
				/**
				 * END
				 */
				getColPremiseDetails();
				
				
				
				//Date 21 july 2017 added by vijay for quick sales order
				
				viewWarehouse();
				viewstarageLoc();
				viewstorageBin();
				createColumnAddColumn();
				// ends here
				/**
				 * nidhi
				 * 21-08-2018
				 * for add serial number
				 */
				if(LoginPresenter.mapModelSerialNoFlag){
					createColumnAddProSerialNoColumn();
				}
				
				
				
				
				/**Date 25-6-2020 by Amol Commented this Column raised by Rahul T.***/
//				getColWarrantyDuration();
//				getColNo_Of_Service();
//				//DATE 23.1.2018 added by komal
//				createViewAmcNumberColumn();
//				createColumnAddAmcColumn();

				
				createColumnprodCodeColumn();
				createColumnProdCountColumn();
				createColumnprodHSNCodeColumn();
				/**
				 * nidhi
				 * 9-06-2018
				 * 
				 */
				getDetailsButtonColumn();
				createColumndeleteColumn();
				addFieldUpdater();
			}
		});
	}
	
	
	/**
	 * Date 21 july 2017 added by vijay for QuickSalesOrder for Delivery Note
	 */
	public void createColumnAddColumn() {
		ButtonCell btnCell = new ButtonCell();
		addwarehouseBtnColumn = new Column<SalesLineItem, String>(btnCell) {
			@Override
			public String getValue(SalesLineItem object) {
				return "Select Warehouse";
			}
//			@Override
//			public void onBrowserEvent(Context context, Element elem,SalesLineItem object, NativeEvent event) {
//				
//				if(marginFlag==false){
//					super.onBrowserEvent(context, elem, object, event);
//					return;
//				}
//				
//				if(object.isReadOnly()==false
//						||((UserConfiguration.getRole()!=null&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin"))
//						||(UserConfiguration.getRole()!=null&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Zonal Coordinator")))){
//					super.onBrowserEvent(context, elem, object, event);
//				}
//			}
		};
		table.addColumn(addwarehouseBtnColumn, "");
		table.setColumnWidth(addwarehouseBtnColumn,170,Unit.PX);

	}
	
	public void viewWarehouse() {
		warehouseViewColumn = new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				return object.getItemProductWarehouseName();
			}
		};
		table.addColumn(warehouseViewColumn, "Warehouse");
		table.setColumnWidth(warehouseViewColumn,153,Unit.PX);
	}
	
	public void viewstarageLoc() {
		columnviewLoc = new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				return object.getItemProductStrorageLocation();
			}
		};
		table.addColumn(columnviewLoc, "Storage Location");
		table.setColumnWidth(columnviewLoc,153,Unit.PX);
	}
	public void viewstorageBin() {
		columnviewBin = new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				return object.getItemProductStorageBin();
			}
		};
		table.addColumn(columnviewBin, "Storage Bin");
		table.setColumnWidth(columnviewBin,153,Unit.PX);
	}
	
	private void addColumnProductAvailableQty() {
		getColumnProductAvailableQty = new TextColumn<SalesLineItem>() {
			
			@Override
			public String getValue(SalesLineItem object) {
				return nf.format(object.getItemProductAvailableQty());
			}
		};
		table.addColumn(getColumnProductAvailableQty, "Stock");
		table.setColumnWidth(getColumnProductAvailableQty,180,Unit.PX);
	}
	
	/**
	 * ends here
	 */
	
	
	private void createColumnprodHSNCodeColumn() {
		EditTextCell editCell=new EditTextCell();
		hsnCodeColumn=new Column<SalesLineItem,String>(editCell)
		{
			@Override
			public String getValue(SalesLineItem object)
			{
				
				if(object.getPrduct().getHsnNumber()!=null && !object.getPrduct().getHsnNumber().equals("")){
					return object.getPrduct().getHsnNumber();
				}
				else{
					return "";
				}
			}
			
			@Override
			public void onBrowserEvent(Context context, Element elem,SalesLineItem object, NativeEvent event) {
				
				if(marginFlag==false){
					super.onBrowserEvent(context, elem, object, event);
					return;
				}
				
				if(object.isReadOnly()==false
						||((UserConfiguration.getRole()!=null&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin"))
						||(UserConfiguration.getRole()!=null&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Zonal Coordinator")))){
					super.onBrowserEvent(context, elem, object, event);
				}
			}
		};
		table.addColumn(hsnCodeColumn,"#HSN Code");
		table.setColumnWidth(hsnCodeColumn, 100,Unit.PX);
	}
	
	private void addEditColumnVatTax() {
		SelectionCell employeeselection= new SelectionCell(vatlist);
		vatColumn = new Column<SalesLineItem, String>(employeeselection) {
			@Override
			public String getValue(SalesLineItem object) {
				if (object.getVatTax().getTaxConfigName()!= null
						&&!object.getVatTax().getTaxConfigName().equals("")) {
					
					object.setVatTaxEdit(object.getVatTax().getTaxConfigName());
					
					return object.getVatTax().getTaxConfigName();
				} else
					return "NA";
			}
			
			@Override
			public void onBrowserEvent(Context context, Element elem,SalesLineItem object, NativeEvent event) {
				
				if(marginFlag==false){
					super.onBrowserEvent(context, elem, object, event);
					return;
				}
				
				if(object.isReadOnly()==false
						||((UserConfiguration.getRole()!=null&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin"))
						||(UserConfiguration.getRole()!=null&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Zonal Coordinator")))){
					super.onBrowserEvent(context, elem, object, event);
				}
			}
		};
		table.addColumn(vatColumn, "#Tax 1");
		table.setColumnWidth(vatColumn,130,Unit.PX);
		table.setColumnWidth(vatColumn, 100,Unit.PX);
		
	}
	
	private void addEditColumnServiceTax() {
		SelectionCell employeeselection= new SelectionCell(list);
		serviceColumn = new Column<SalesLineItem, String>(employeeselection) {
			@Override
			public String getValue(SalesLineItem object) {
				if (object.getServiceTax().getTaxConfigName()!= null
						&&!object.getServiceTax().getTaxConfigName().equals("")) {
					
					object.setServiceTaxEdit(object.getServiceTax().getTaxConfigName());

					return object.getServiceTax().getTaxConfigName();
				} else
					return "NA";
			}
			
			@Override
			public void onBrowserEvent(Context context, Element elem,SalesLineItem object, NativeEvent event) {
				
				if(marginFlag==false){
					super.onBrowserEvent(context, elem, object, event);
					return;
				}
				
				if(object.isReadOnly()==false
						||((UserConfiguration.getRole()!=null&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin"))
						||(UserConfiguration.getRole()!=null&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Zonal Coordinator")))){
					super.onBrowserEvent(context, elem, object, event);
				}
			}
		};
		table.addColumn(serviceColumn, "#Tax 2");
		table.setColumnWidth(serviceColumn,140,Unit.PX);
		table.setColumnWidth(serviceColumn, 100,Unit.PX);
	}
	
	private void createColumnDiscountAmt() {
		
		EditTextCell editCell=new EditTextCell();
		discountAmt=new Column<SalesLineItem,String>(editCell)
				{
			@Override
			public String getValue(SalesLineItem object)
			{
				if(object.getDiscountAmt()==0){
					return 0+"";}
				else{
					return object.getDiscountAmt()+"";}
			}
				};
				table.addColumn(discountAmt,"#Disc Amt");
				table.setColumnWidth(discountAmt, 100,Unit.PX);
		
	}
	private void createColumnWarrantyInMonths() {

		EditTextCell editCell = new EditTextCell();
		
		warrantyInMonthsColumn = new Column<SalesLineItem, String>(editCell) {
			
			@Override
			public String getValue(SalesLineItem object) {
				if(object.getWarrantyInMonth()!=-1){
					return object.getWarrantyInMonth() +"";
				}
				return 0+"";
			}
			
			@Override
			public void onBrowserEvent(Context context, Element elem,SalesLineItem object, NativeEvent event) {
				
				if(marginFlag==false){
					super.onBrowserEvent(context, elem, object, event);
					return;
				}
				
				if(object.isReadOnly()==false
						||((UserConfiguration.getRole()!=null&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin"))
						||(UserConfiguration.getRole()!=null&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Zonal Coordinator")))){
					super.onBrowserEvent(context, elem, object, event);
				}
			}
		};
		
		table.addColumn(warrantyInMonthsColumn,"Warranty In Months");
		table.setColumnWidth(warrantyInMonthsColumn, 110, Unit.PX);
	}
	
	
//	private void createColumnWarrantyUntilColumn() {
//
//		DateTimeFormat fmt = DateTimeFormat.getFormat(PredefinedFormat.YEAR_MONTH_DAY);
//		DatePickerCell date= new DatePickerCell(fmt);
//		warrantyUntildDateColumn=new Column<SalesLineItem,Date>(date)
//				{
//			@Override
//			public Date getValue(SalesLineItem object)
//			{
//				if(object.getWarrantyUntil()!=null){
//					System.out.println("oldDate"+object.getWarrantyUntil());
//					oldDate=object.getWarrantyUntil();
//					return object.getWarrantyUntil();
//					
//				}
//				System.out.println("oldDate"+oldDate);
//
//				return oldDate;
//
//			}
//				};
//				table.addColumn(warrantyUntildDateColumn,"Warranty Until");
//				table.setColumnWidth(warrantyUntildDateColumn, 110, Unit.PX);
//	}
	
	
	protected void createColumnProdCountColumn()
	{
		prodIdColumn=new TextColumn<SalesLineItem>()
				{
			@Override public String getValue(SalesLineItem object){
				if(object.getPrduct().getCount()!=0){
					return object.getPrduct().getCount()+"";
				}
				else{
					return "";
				}
			}
				};
				table.addColumn(prodIdColumn,"Prod ID");
				table.setColumnWidth(prodIdColumn,120,Unit.PX);

	}

	protected void createColumnprodCategoryColumn()
	{
		prodCategoryColumn=new TextColumn<SalesLineItem>()
				{
			@Override public String getValue(SalesLineItem object){
				return object.getProductCategory();
			}
				};
				table.addColumn(prodCategoryColumn,"Category");
				table.setColumnWidth(prodCategoryColumn,100,Unit.PX);

	}
	protected void createColumnprodCodeColumn()
	{
		prodCodeColumn=new TextColumn<SalesLineItem>()
				{
			@Override public String getValue(SalesLineItem object){
				return object.getProductCode();
			}
				};
				table.addColumn(prodCodeColumn,"PCode");
				table.setColumnWidth(prodCodeColumn,120,Unit.PX);


	}
	protected void createColumnprodNameColumn()
	{
		EditTextCell editCell=new EditTextCell();
		prodNameColumn=new Column<SalesLineItem,String>(editCell)
				{
			@Override
			public String getValue(SalesLineItem object)
			{
				return object.getProductName();
			}
				};
				table.addColumn(prodNameColumn,"#Name");
				table.setColumnWidth(prodNameColumn, 120,Unit.PX);


	}
	protected void createColumnprodQtyColumn()
	{
		EditTextCell editCell=new EditTextCell();
		prodQtyColumn=new Column<SalesLineItem,String>(editCell)
				{
			@Override
			public String getValue(SalesLineItem object)
			{
				if(object.getQty()==0){
					return 0+"";}
				else{
					return object.getQty()+"";}
			}
			
			@Override
			public void onBrowserEvent(Context context, Element elem,SalesLineItem object, NativeEvent event) {
				
				if(marginFlag==false){
					super.onBrowserEvent(context, elem, object, event);
					return;
				}
				
				if(object.isReadOnly()==false
						||((UserConfiguration.getRole()!=null&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin"))
						||(UserConfiguration.getRole()!=null&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Zonal Coordinator")))){
					super.onBrowserEvent(context, elem, object, event);
				}
			}
				};
				table.addColumn(prodQtyColumn,"#Qty");
				table.setColumnWidth(prodQtyColumn,120,Unit.PX);

		}
	
	
	protected void createViewQtyColumn()
	{
		
		viewprodQtyColumn=new TextColumn<SalesLineItem>()
				{
			@Override
			public String getValue(SalesLineItem object)
			{
				if(object.getQty()==0){
					return 0+"";}
				else{
					return object.getQty()+"";
				}
			}
				};
				table.addColumn(viewprodQtyColumn,"#Qty");
				table.setColumnWidth(viewprodQtyColumn,100,Unit.PX);

	}
	
	protected void createColumnUnitOfMeasurement()
	{
		uomColumn=new TextColumn<SalesLineItem>()
				{
			@Override public String getValue(SalesLineItem object){
				if(object.getUnitOfMeasurement()!=null){
					return object.getUnitOfMeasurement();
				}
				else{
					return "";
				}
			}
				};
				table.addColumn(uomColumn,"UOM");
				table.setColumnWidth(uomColumn,120,Unit.PX);

	}

	protected void createColumnPercDiscount()
	{
		EditTextCell editCell=new EditTextCell();
		prodPercentDiscColumn=new Column<SalesLineItem,String>(editCell)
				{
			@Override
			public String getValue(SalesLineItem object)
			{
				if(object.getPercentageDiscount()==0||object.getPercentageDiscount()==null){
					return 0+"";}
				else{
					return object.getPercentageDiscount()+"";}
			}
				};
				table.addColumn(prodPercentDiscColumn,"#% Disc");
				table.setColumnWidth(prodPercentDiscColumn,120,Unit.PX);

	}
	
	
	protected void createViewPercDiscount()
	{
		
		viewprodPercDiscColumn=new TextColumn<SalesLineItem>()
				{
			@Override
			public String getValue(SalesLineItem object)
			{
				if(object.getPercentageDiscount()==0){
					return 0+"";}
				else{
					return object.getPercentageDiscount()+"";}
			}
				};
				table.addColumn(viewprodPercDiscColumn,"#% Disc");
				table.setColumnWidth(viewprodPercDiscColumn,120,Unit.PX);

	}
	
	protected void createColumnpriceColumn()
	{
		EditTextCell editCell=new EditTextCell();
		priceColumn=new Column<SalesLineItem,String>(editCell)
				{
			@Override
			public String getValue(SalesLineItem object)
			{
				SuperProduct product=object.getPrduct();
				double tax=removeAllTaxes(product);
				double origPrice=object.getPrice()-tax;
				
				return nf.format(origPrice);
			}
			
			@Override
			public void onBrowserEvent(Context context, Element elem,SalesLineItem object, NativeEvent event) {
				
				if(marginFlag==false){
					super.onBrowserEvent(context, elem, object, event);
					return;
				}
				
				if(object.isReadOnly()==false
						||((UserConfiguration.getRole()!=null&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin"))
						||(UserConfiguration.getRole()!=null&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Zonal Coordinator")))){
					super.onBrowserEvent(context, elem, object, event);
				}
			}
				};
				table.addColumn(priceColumn,"#Price");
				table.setColumnWidth(priceColumn,120,Unit.PX);

	}
	
	protected void createViewColumnpriceColumn()
	{
		
		viewPriceColumn=new TextColumn<SalesLineItem>()
				{
			@Override
			public String getValue(SalesLineItem object)
			{
				if(object.getPrice()==0){
					return 0+"";}
				else{
					SuperProduct product=object.getPrduct();
					double tax=removeAllTaxes(product);
					double origPrice=object.getPrice()-tax;
					
					return nf.format(origPrice);
					}
			}
				};
				table.addColumn(viewPriceColumn,"#Price");
				table.setColumnWidth(viewPriceColumn,120,Unit.PX);

	}
	
	
	protected void createColumnvatColumn()
	{
		TextCell editCell=new TextCell();
		vatColumn=new Column<SalesLineItem,String>(editCell)
				{
			@Override
			public String getValue(SalesLineItem object)
			{
//				if(object.getVatTax()!=null){
//					return object.getVatTax().getPercentage()+"";
//				}
//				else{ 
//					return "N.A.";
//				}
				return object.getVatTaxEdit();
			}
				};
				table.addColumn(vatColumn, "# Tax 1");
				table.setColumnWidth(vatColumn, 100,Unit.PX);

	}
	protected void createColumnserviceColumn()
	{
		TextCell editCell=new TextCell();
		serviceColumn=new Column<SalesLineItem,String>(editCell)
		{
			@Override
			public String getValue(SalesLineItem object)
			{
				//SuperProduct prod=object.getPrduct();
//				if(object.getServiceTax()!=null){
//					return object.getServiceTax().getPercentage()+"";
//				}
//				else{ 
//					return "N.A.";
//				}
				return object.getServiceTaxEdit();
			}
				};
				table.addColumn(serviceColumn, "# Tax 2");
				table.setColumnWidth(serviceColumn, 100,Unit.PX);

	}
	protected void createColumntotalColumn()
	{
		TextCell editCell=new TextCell();
		totalColumn=new Column<SalesLineItem,String>(editCell)
				{
			@Override
			public String getValue(SalesLineItem object)
			{
				double total=0;
				SuperProduct product=object.getPrduct();
				double tax=removeAllTaxes(product);
				double origPrice=object.getPrice()-tax;
				
				
//				if(object.getPercentageDiscount()==null){
//					total=origPrice*object.getQty();
//				}
//				if(object.getPercentageDiscount()!=null){
//					total=origPrice-(origPrice*object.getPercentageDiscount()/100);
//					total=total*object.getQty();
////					total=Math.round(total);
//				}
				
				
				/** Date 05-08-2017
				 *  added by vijay add code for area calculation
				 */
				
				if(object.getArea().equals("") || object.getArea().equals("0")){
					object.setArea("NA");
				}
				System.out.println("area == "+object.getArea());
				//****************new code for discount by rohan ******************
				if((object.getPercentageDiscount()==null && object.getPercentageDiscount()==0) && (object.getDiscountAmt()==0) ){
					
					System.out.println("inside both 0 condition");
					/******************** Square Area Calculation code added in if condition and without square area calculation code in else block ***************************/
					System.out.println("Get value from area =="+ object.getArea());
					System.out.println("total amount before area calculation =="+total);
					if(!object.getArea().equalsIgnoreCase("NA") ){
					double area = Double.parseDouble(object.getArea());
					 total = origPrice*area;
					System.out.println(" Final TOTAL if no discount per & no discount Amt ==="+total);
					}else{
						total=origPrice*object.getQty();
					}
				}
				
				else if((object.getPercentageDiscount()!=null)&& (object.getDiscountAmt()!=0)){
					
					System.out.println("inside both not null condition");
					/******************** Square Area Calculation code added in if condition and without square area calculation code in else block ***************************/
					System.out.println("total amount before area calculation =="+total);
					if(!object.getArea().equalsIgnoreCase("NA")){
						double area = Double.parseDouble(object.getArea());
						total=origPrice*area;
						System.out.println("total before discount per ===="+total);
						total = total - (total*object.getPercentageDiscount()/100);
						System.out.println("after discount per total === "+total);
						total = total - object.getDiscountAmt();
						System.out.println("after discount AMT total === "+total);

					System.out.println(" Final TOTAL   discount per &  discount Amt ==="+total);
					}else{
						
						total=origPrice*object.getQty();
						total=total-(total*object.getPercentageDiscount()/100);
						total=total-object.getDiscountAmt();
					}
					
				}
				else 
				{
					System.out.println("inside oneof the null condition");
					
						if(object.getPercentageDiscount()!=null){
							System.out.println("inside getPercentageDiscount oneof the null condition");
							/******************** Square Area Calculation code added in if condition and without square area calculation code in else block old code ***************************/
							if(!object.getArea().equalsIgnoreCase("NA")){
								double area = Double.parseDouble(object.getArea());
								total=origPrice*area;
								total = total - (total*object.getPercentageDiscount()/100);
								
							}else{
								total=origPrice*object.getQty();
								total=total-(total*object.getPercentageDiscount()/100);
							}
						}
						else 
						{
							System.out.println("inside getDiscountAmt oneof the null condition");
							/******************** Square Area Calculation code added in if condition and without square area calculation code in else block old code ***************************/
							if(!object.getArea().equalsIgnoreCase("NA")){
								double area = Double.parseDouble(object.getArea());
								total=origPrice*area;
								total = total - object.getDiscountAmt();
								
							}else{
								total=origPrice*object.getQty();
								total=total-object.getDiscountAmt();
							}
						}
						
//						total=total*object.getQty();
						
				}
				
				/**
				 * @author Anil
				 * @since 17-07-2020
				 */
				object.setTotalAmount(nf.parse(nf.format(total)));
				return nf.format(total);
			}
				};
				table.addColumn(totalColumn,"Total");
				table.setColumnWidth(totalColumn,120,Unit.PX);

	}
	protected void createColumndeleteColumn()
	{
		ButtonCell btnCell= new ButtonCell();
		deleteColumn=new Column<SalesLineItem,String>(btnCell)
				{
			@Override
			public String getValue(SalesLineItem object)
			{
				return  "Delete" ;
			}
			@Override
			public void onBrowserEvent(Context context, Element elem,SalesLineItem object, NativeEvent event) {
				
				if(marginFlag==false){
					super.onBrowserEvent(context, elem, object, event);
					return;
				}
				
				if(object.isReadOnly()==false
						||((UserConfiguration.getRole()!=null&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin"))
						||(UserConfiguration.getRole()!=null&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Zonal Coordinator")))){
					super.onBrowserEvent(context, elem, object, event);
				}
			}
			
				};
				table.addColumn(deleteColumn,"Delete");
				table.setColumnWidth(deleteColumn,120,Unit.PX);

				
	}



	@Override public void addFieldUpdater() 
	{
		System.out.println("HI VIJAY ADD FIELD UPDATER");
		createFieldUpdaterprodQtyColumn();
		createFieldUpdaterpriceColumn();
		createFieldUpdaterPercDiscColumn();
		createFieldUpdaterDiscAmtColumn();
		createFieldUpdaterdeleteColumn();
//		createFieldUpdaterwarrantyUntilColumn();
		
		/**
		 * Date 11 jul 2017 added by vijay for GST and taxes
		 */
		
		updateServiceTaxColumn();
		updateVatTaxColumn();
		updateHSNCode();
		
		/**
		 * ends here
		 */
		
		/**
		 * Date 21 july 2017 added by vijay for QuickSalesOrder
		 */
		
		updateWarehouseBtn();
//		updateWarrentyInMonths();
		
		/**
		 * ends here
		 */
//		/** Date 05-08-2017 added by vijay for Area wise calculation **/
//		createFieldUpdaterProdAreaColumn();
		
//		/** date 23/1/2018 added by komal for hvac  **/
//		updateAmcBtn();
		
		/**
		 * nidhi
		 * 9-06-2018
		 */
		createFieldUpdaterOnBranchesColumn();
		/**
		 * nidhi
		 * 21-08-2018
		 */
		if(LoginPresenter.mapModelSerialNoFlag){
			if(addProSerialNo!=null)
			createFieldUpdaterAddSerialNo();
			if(viewProSerialNo!=null)
			createFieldUpdaterViewSerialNo();
		}
	}
	
/** Date 05-08-2017 added by vijay for Area wise calculation **/
	
	private void createFieldUpdaterProdAreaColumn() {
		prodAreaColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {
			@Override
			public void update(int index, SalesLineItem object, String value) {
				
				try{
					object.setArea(value);
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				}
				catch (NumberFormatException e)
				{

				}
				table.redrawRow(index);
			}
		});
	}
	
	private void updateHSNCode() {
		hsnCodeColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {

			@Override
			public void update(int index, SalesLineItem object, String value) {
				object.getPrduct().setHsnNumber(value);
				table.redrawRow(index);
			}
		});
	}
	
	private void updateWarrentyInMonths() {

		warrantyInMonthsColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {
				
				@Override
				public void update(int index, SalesLineItem object, String value) {
					try{
						int val1=Integer.parseInt(value.trim());
						object.setWarrantyInMonth(val1);
						
						if(QuickSalesOrderForm.salesorderDate!=null){
							Date warrantyuntildate = getWarrantyEndDate(QuickSalesOrderForm.salesorderDate,val1);
							object.setWarrantyUntil(warrantyuntildate);
						}
						
						
						RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);

					}
					catch (NumberFormatException e)
					{

					}
					table.redrawRow(index);
				}

				
			});
		
		
	}
	
	private Date getWarrantyEndDate(Date salesorderDate, int warrantyInMonth) {

			CalendarUtil.addMonthsToDate(salesorderDate, warrantyInMonth);
			return salesorderDate;
	}
	
	private void updateWarehouseBtn() {
		
		addwarehouseBtnColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {
			
			@Override
			public void update(int index, SalesLineItem object, String value) {
				
				panel=new PopupPanel(true);
				panel.add(invLoc);
				InventoryLocationPopUp.initialzeWarehouse(object.getPrduct().getCount());
				
				if(object.getItemProductWarehouseName()!=null&&object.getItemProductStrorageLocation()!=null
						&&object.getItemProductStorageBin()!=null){
					System.out.println("Inside table not null list settt...........");
					for(int i=0;i<InventoryLocationPopUp.getLsWarehouse().getItemCount();i++)
					{
						String data=InventoryLocationPopUp.lsWarehouse.getItemText(i);
						if(data.equals(object.getItemProductWarehouseName()))
						{
							InventoryLocationPopUp.lsWarehouse.setSelectedIndex(i);
							break;
						}
					}
					
					invLoc.getLsStorageLoc().clear();
					invLoc.lsStorageLoc.addItem("--SELECT--");
					invLoc.lsStorageLoc.addItem(object.getItemProductStrorageLocation());
					
					
					invLoc.getLsStoragebin().clear();
					invLoc.lsStoragebin.addItem("--SELECT--");
					
					for(int i=0;i<invLoc.arraylist2.size();i++){
						if(invLoc.arraylist2.get(i).getWarehouseName().equals(object.getItemProductWarehouseName())
								&&invLoc.arraylist2.get(i).getStorageLocation().equals(object.getItemProductStrorageLocation())){
							invLoc.lsStoragebin.addItem(invLoc.arraylist2.get(i).getStorageBin());
						}
					}
					
					for(int i=0;i<invLoc.getLsStorageLoc().getItemCount();i++)
					{
						String data=invLoc.lsStorageLoc.getItemText(i);
						if(data.equals(object.getItemProductStrorageLocation()))
						{
							invLoc.lsStorageLoc.setSelectedIndex(i);
							break;
						}
					}
					for(int i=0;i<invLoc.getLsStoragebin().getItemCount();i++)
					{
						String data=invLoc.lsStoragebin.getItemText(i);
						if(data.equals(object.getItemProductStrorageLocation()))
						{
							invLoc.lsStoragebin.setSelectedIndex(i);
							break;
						}
					}
				}
				
				
				
				invLoc.formView();
				panel.show();
				panel.center();
				rowIndex=index;
			}
		});
	}
	
	
	public void updateServiceTaxColumn()
	{

		serviceColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {
			
			@Override
			public void update(int index, SalesLineItem object, String value) {
				try {
					
					String val1 = (value.trim());
					object.setServiceTaxEdit(val1);
					Tax tax = object.getServiceTax();
					for(int i=0;i<servicetaxlist.size();i++)
					{
						if(val1.trim().equals(servicetaxlist.get(i).getTaxChargeName())){
							tax.setTaxName(val1);
							tax.setPercentage(servicetaxlist.get(i).getTaxChargePercent());
							/**
							 * rohan added this for GST implementation Date :23-06-2017
							 */
							tax.setTaxPrintName(servicetaxlist.get(i).getTaxPrintName());
							/**
							 * Date : 10-08-2017 By Anil
							 */
							tax.setTaxConfigName(val1);
							/**
							 * End
							 */
							/**
							 * nidhi
							 * 8-06-2018
							 */
							Tax vatTax = AppUtility.checkTaxValue(tax, object.getVatTax(), vattaxlist);
							if(vatTax!=null){
								object.setVatTax(vatTax);
							}
							/**
							 * end
							 */
							break;
						}
					}
					object.setServiceTax(tax);
					System.out.println("TAX==="+tax.getTaxPrintName());
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
					
				} catch (NumberFormatException e) {
				}
				table.redrawRow(index);
			}
		});
	}
	
	public void updateVatTaxColumn()
	{

		vatColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {
			
			@Override
			public void update(int index, SalesLineItem object, String value) {
				try {
					String val1 = (value.trim());
					object.setVatTaxEdit(val1);
					Tax tax = object.getVatTax();
					for(int i=0;i<vattaxlist.size();i++)
					{
						if(val1.trim().equals(vattaxlist.get(i).getTaxChargeName())){
							tax.setTaxName(val1);
							tax.setPercentage(vattaxlist.get(i).getTaxChargePercent());
							tax.setTaxPrintName(vattaxlist.get(i).getTaxPrintName());
							/**
							 * rohan added this for GST implementation Date :23-06-2017
							 */
							tax.setTaxPrintName(vattaxlist.get(i).getTaxPrintName());
							
							/**
							 * Date : 10-08-2017 By Anil
							 */
							tax.setTaxConfigName(val1);
							/**
							 * End
							 */
							/**
							 * nidhi
							 * 8-06-2018
							 * for tax selection option
							 */
//							checkTaxValue(tax,object);
							Tax taxDt = AppUtility.checkTaxValue(tax, object.getServiceTax(), vattaxlist);
							if(taxDt != null){
								object.setServiceTax(taxDt);
							}
							/**
							 * end
							 */
							break;
						}
					}
					object.setVatTax(tax);
					
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
					
				} catch (NumberFormatException e) {
				}
				table.redrawRow(index);
			}
		});
	}
	
	

	private void createFieldUpdaterDiscAmtColumn() {
		discountAmt.setFieldUpdater(new FieldUpdater<SalesLineItem, String>()
				{
			@Override

			public void update(int index,SalesLineItem object,String value)
			{

				try{
					Double val1=Double.parseDouble(value.trim());
					object.setDiscountAmt(val1);
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);

				}
				catch (NumberFormatException e)
				{

				}
				table.redrawRow(index);
			}
				});
	}
//	private void createFieldUpdaterwarrantyUntilColumn() {
//
//		warrantyUntildDateColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, Date>() {
//
//			@Override
//			public void update(int index, SalesLineItem object, Date value) {
//
//					
//					object.setWarrantyUntil(value);
//					table.redrawRow(index);
//				
//			}
//		});
//	}
	
	protected void createFieldUpdaterprodQtyColumn()
	{
		prodQtyColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, String>()
				{
			@Override

			public void update(int index,SalesLineItem object,String value)
			{

				try{
					Double val1=Double.parseDouble(value.trim());
					object.setQuantity(val1);
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);

				}
				catch (NumberFormatException e)
				{

				}
				table.redrawRow(index);
			}
				});
	}
	
	protected void createFieldUpdaterPercDiscColumn()
	{
		prodPercentDiscColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, String>()
				{
			@Override

			public void update(int index,SalesLineItem object,String value)
			{

				try{
					Double val1=Double.parseDouble(value.trim());
					object.setPercentageDiscount(val1);
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);

				}
				catch (NumberFormatException e)
				{

				}
				table.redrawRow(index);
			}
				});
	}
	
	protected void createFieldUpdaterpriceColumn()
	{
		priceColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, String>()
				{
			@Override
			public void update(int index,SalesLineItem object,String value)
			{
				try
				{
					Double val1=Double.parseDouble(value.trim());
			
				object.setPrice(val1);
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				
				}
				catch(Exception e)
				{
				}
				table.redrawRow(index);
			}
				});
	}
	protected void createFieldUpdaterdeleteColumn()
	{
		deleteColumn.setFieldUpdater(new FieldUpdater<SalesLineItem,String>()
				{
			@Override
			public void update(int index,SalesLineItem object,String value)
			{
				getDataprovider().getList().remove(object);
				table.redrawRow(index);
			}
				});
	}

	private void createColumnWarrantyUntilView() {

		final DateTimeFormat fmt1 = DateTimeFormat.getFormat(PredefinedFormat.DATE_FULL);
		viewwarrantyUntildDateColumn=new TextColumn<SalesLineItem>() {
			
			@Override
			public String getValue(SalesLineItem object) {
				if(object.getWarrantyUntil()!=null)
					return fmt1.format(object.getWarrantyUntil());
				return "";
			}
		};
			
		table.addColumn(viewwarrantyUntildDateColumn,"Warranty Until");
		table.setColumnWidth(viewwarrantyUntildDateColumn, 110, Unit.PX);
		
	}

	public void addColumnSorting(){

		//createSortinglbtColumn();

	}

/*	protected void createSortinglbtColumn(){
		List<SalesLineItem> list=getDataprovider().getList();
		columnSort=new ListHandler<SalesLineItem>(list);
		columnSort.setComparator(lbtColumn, new Comparator<SalesLineItem>()
				{
			@Override
			public int compare(SalesLineItem e1,SalesLineItem e2)
			{
				if(e1!=null && e2!=null)
				{if( e1.getLbtTax()!=null && e2.getLbtTax()!=null){
					return e1.getLbtTax().compareTo(e2.getLbtTax());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}*/


	@Override
	public void setEnable(boolean state)
	{
          
		
        for(int i=table.getColumnCount()-1;i>-1;i--)
    	  table.removeColumn(i); 
        	  
      
          if(state==true)
          {
        	createColumnProdCountColumn();
//        	createColumnprodCategoryColumn();
      		createColumnprodCodeColumn();
//      		createColumnprodNameColumn();
      		createColumnViewProdNameColumn();
      		
      		// vijay moved on 12 july 2017
      		createColumnWarrantyInMonths();
//    		createColumnWarrantyUntilColumn();
    		
      		createColumnprodQtyColumn();
      		createColumnprodAreaColumn();
      		createColumnUnitOfMeasurement();
      		createColumnpriceColumn();
      		
      		/**
    		 * Date 10 july 2017 added by vijay other columns added into on success method
    		 */
    		retriveVatandServiceTax();
    		
//      		createColumnvatColumn();
//      		createColumnserviceColumn();
//      		createColumnPercDiscount();
//      		createColumnDiscountAmt();
//      		createColumntotalColumn();
//      		createColumndeleteColumn();
//      		createColumnWarrantyUntilColumn();
//      		addFieldUpdater();
      		}
 
          else
          {
        	createColumnProdCountColumn();
//        	createColumnprodCategoryColumn();
      		createColumnprodCodeColumn();
      		createColumnprodNameColumn();
    		
      		createViewQtyColumn();
      		
      		/**
      		 * Date : 17-04-2018 By ANIL
      		 */
//      	createColumnWarrantyUntilView();
//      		viewColumnWarrantyInMonths();
//      		createViewColumnProdAreaView();
      		/**
      		 * End
      		 */
      		

      		createColumnUnitOfMeasurement();
      		createViewColumnpriceColumn();
      		createColumnvatColumn();
      		createColumnserviceColumn();
      		
      		viewColIsTaxInclusive();
      		
      		createViewPercDiscount();
      		createViewDiscountAmt();
      		createColumntotalColumn();
      		createColumnViewHSNCodeColumn();
      		
      		addColumnProductAvailableQty();
			viewWarehouse();
			viewstarageLoc();
			viewstorageBin();
			
//			viewColNo_Of_Services();
//			viewColWarrantyDuration();
			/**
			 * nidhi
			 * 21-08-2018
			 * for add serial number
			 */
			if(LoginPresenter.mapModelSerialNoFlag){
				createViewColumnAddProSerialNoColumn();
			}
			
			addFieldUpdater();
			viewColPremiseDetails();
			/** date 23/1/2018 added by komal for hvac  **/
      		createViewAmcNumberColumn();

      		
          }
	}
	
	/****** Date 05-08-2017 added by vijay for Area ****/
	private void createViewColumnProdAreaView() {
		
		viewProdAreaColumn = new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				return object.getArea();
			}
		};
		table.addColumn(viewProdAreaColumn,"#Area");
		table.setColumnWidth(viewProdAreaColumn, 100,Unit.PX);
	}
	
	
	protected void createColumnViewProdNameColumn(){
		
		viewProdNameColumn = new TextColumn<SalesLineItem>()
			{
					@Override 
					public String getValue(SalesLineItem object){
						return object.getProductName();
					}
			};
			table.addColumn(viewProdNameColumn,"Name");
			table.setColumnWidth(viewProdNameColumn, 120,Unit.PX);
	}

	private void viewColumnWarrantyInMonths() {

		viewwarrentyInMonthsColumn = new TextColumn<SalesLineItem>() {
			
			@Override
			public String getValue(SalesLineItem object) {
				if(object.getWarrantyInMonth()!=-1){
					return object.getWarrantyInMonth() +"";
				}
				return null;
			}
		};
		
		table.addColumn(warrantyInMonthsColumn,"Warranty In Months");
		table.setColumnWidth(warrantyInMonthsColumn, 110, Unit.PX);
	}
	
	private void createColumnViewHSNCodeColumn() {
		
		viewHsnCodeColumn=new TextColumn<SalesLineItem>()
			{
				@Override
				public String getValue(SalesLineItem object)
				{
					return object.getPrduct().getHsnNumber();
				}
			};
			table.addColumn(viewHsnCodeColumn,"#HSN Code");
			table.setColumnWidth(viewHsnCodeColumn, 100,Unit.PX);
	}

	protected void createViewDiscountAmt()
	{
		viewdiscountAmt=new TextColumn<SalesLineItem>()
				{
			@Override
			public String getValue(SalesLineItem object)
			{
				if(object.getDiscountAmt()==0){
					return 0+"";}
				else{
					return object.getDiscountAmt()+"";}
			}
				};
				table.addColumn(viewdiscountAmt,"#Disc Amt");
				table.setColumnWidth(viewdiscountAmt, 100,Unit.PX);
	}
	

	@Override
	public void applyStyle()
	{

	}
	@Override
	protected void initializekeyprovider() {
		keyProvider= new ProvidesKey<SalesLineItem>()
				{
			@Override
			public Object getKey(SalesLineItem item)
			{
				if(item==null)
				{
					return null;
				}
				else
					return item.getId();
			}
				};
	}

	public double calculateTotalExcludingTax()
	{
		List<SalesLineItem>list=getDataprovider().getList();
//		double sum=0,priceVal=0;
		double total=0,origPrice=0;
		for(int i=0;i<list.size();i++)
		{
			SalesLineItem entity=list.get(i);
			SuperProduct prod=entity.getPrduct();
			double taxAmt=removeAllTaxes(prod);
//			if(entity.getPercentageDiscount()==null){
//				priceVal=entity.getPrice()-taxAmt;
//				priceVal=priceVal*entity.getQty();
//				sum=sum+priceVal;
//			}
//			if(entity.getPercentageDiscount()!=null){
//				priceVal=entity.getPrice()-taxAmt;
//				priceVal=priceVal-(priceVal*entity.getPercentageDiscount()/100);
//				priceVal=priceVal*entity.getQty();
//				
//				sum=sum+priceVal;
//			}
//			
//	
//		}
//		return sum;
			
			
			//*****************new code for discount by rohan ***************** 
			 origPrice = entity.getPrice()-taxAmt;
			if((entity.getPercentageDiscount()==null && entity.getPercentageDiscount()==0) && (entity.getDiscountAmt()==0) ){
				
				System.out.println("inside both 0 condition");
				/******************** Square Area Calculation code added in if condition and without square area calculation code in else block ***************************/
				if(!entity.getArea().equalsIgnoreCase("NA")){
				double squareArea = Double.parseDouble(entity.getArea());
				origPrice=origPrice*squareArea;
				total+=origPrice;
				System.out.println("RRRRRRRRRRRRRR total"+total);
				}
				else{
					
//					total=origPrice*entity.getQty();
					/**
					 * Date 13 july 2017 below code added by vijay for 2 or more products proper calculation
					 * and  above old code commented
					 */
					origPrice = origPrice*entity.getQty();
					total+=origPrice;
				}
			}
			
			else if((entity.getPercentageDiscount()!=null)&& (entity.getDiscountAmt()!=0)){
				
				System.out.println("inside both not null condition");
				/******************** Square Area Calculation code added in if condition and without square area calculation code in else block ***************************/
				if(!entity.getArea().equalsIgnoreCase("NA")){
					double squareArea = Double.parseDouble(entity.getArea());
					origPrice=origPrice*squareArea;
					origPrice=origPrice-(origPrice*entity.getPercentageDiscount()/100);
					origPrice=origPrice-entity.getDiscountAmt();
					total+=origPrice;
					System.out.println("RRRRRRRRRRRRRR total"+total);
					
				}else{
					
//					total=origPrice*entity.getQty();
//					total=total-(total*entity.getPercentageDiscount()/100);
//					total=total-entity.getDiscountAmt();
					
					/**
					 * Date 13 july 2017 below code added by vijay for 2 or more products proper calculation
					 * and  above old code commented
					 */
					
					origPrice=origPrice*entity.getQty();
					origPrice=origPrice-(origPrice*entity.getPercentageDiscount()/100);
					origPrice=origPrice-entity.getDiscountAmt();
					total +=origPrice;
				}
				
			}
			else 
			{
				System.out.println("inside oneof the null condition");
				
					if(entity.getPercentageDiscount()!=null){
						System.out.println("inside getPercentageDiscount oneof the null condition");
						if(!entity.getArea().equalsIgnoreCase("NA")){
							double squareArea = Double.parseDouble(entity.getArea());
							origPrice=origPrice*squareArea;
							origPrice=origPrice-(origPrice*entity.getPercentageDiscount()/100);
							total +=origPrice;
						}else{
							
//							total=origPrice*entity.getQty();
//							total=total-(total*entity.getPercentageDiscount()/100);
							
							/**
							 * Date 13 july 2017 below code added by vijay for 2 or more products proper calculation
							 * and  above old code commented
							 */
							origPrice=origPrice*entity.getQty();
							origPrice=origPrice-(origPrice*entity.getPercentageDiscount()/100);
							total +=origPrice;

						}

					}
					else 
					{
						System.out.println("inside getDiscountAmt oneof the null condition");
						if(!entity.getArea().equalsIgnoreCase("NA")){
							double squareArea = Double.parseDouble(entity.getArea());
							origPrice=origPrice*squareArea;
							origPrice=origPrice*entity.getQty();
							origPrice=origPrice-entity.getDiscountAmt();
							total +=origPrice;

						}else{
							
//							total=origPrice*entity.getQty();
//							total=total-entity.getDiscountAmt();
							
							/**
							 * Date 13 july 2017 below code added by vijay for 2 or more products proper calculation
							 * and  above old code commented
							 */
							origPrice=origPrice*entity.getQty();
							origPrice=origPrice-entity.getDiscountAmt();
							total +=origPrice;
		
						}
					}
					
//					total=total*object.getQty();
					
			}
			
	
		}
		return total;
	}
	


	public double removeAllTaxes(SuperProduct entity)
	{
		double vat = 0,service = 0;
		double tax=0,retrVat=0,retrServ=0;
		if(entity instanceof ServiceProduct)
		{
			ServiceProduct prod=(ServiceProduct) entity;
			if(prod.getServiceTax()!=null&&prod.getServiceTax().isInclusive()==true){
				service=prod.getServiceTax().getPercentage();
			}
			if(prod.getVatTax()!=null&&prod.getVatTax().isInclusive()==true){
				vat=prod.getVatTax().getPercentage();
			}
		}

		if(entity instanceof ItemProduct)
		{
			ItemProduct prod=(ItemProduct) entity;
			if(prod.getVatTax()!=null&&prod.getVatTax().isInclusive()==true){
				vat=prod.getVatTax().getPercentage();
			}
			if(prod.getServiceTax()!=null&&prod.getServiceTax().isInclusive()==true){
				service=prod.getServiceTax().getPercentage();
			}
		}
		
		if(vat!=0&&service==0){
			retrVat=entity.getPrice()/(1+(vat/100));
			retrVat=entity.getPrice()-retrVat;
		}
		if(service!=0&&vat==0){
			retrServ=entity.getPrice()/(1+service/100);
			retrServ=entity.getPrice()-retrServ;
		}
		if(service!=0&&vat!=0){
//			// Here if both are inclusive then first remove service tax and then on that amount
//			// calculate vat.
//			double removeServiceTax=(entity.getPrice()/(1+service/100));
//						
//			//double taxPerc=service+vat;
//			//retrServ=(entity.getPrice()/(1+taxPerc/100)); //line changed below
//			retrServ=(removeServiceTax/(1+vat/100));
//			retrServ=entity.getPrice()-retrServ;
			
			/** above old code commented and added in else block
			 * Date 11 july 2017 added by vijay for GST
			 */
			
			if(entity instanceof ItemProduct)
			{
				ItemProduct prod=(ItemProduct) entity;
				if(prod.getServiceTax().getTaxPrintName()!=null && ! prod.getServiceTax().getTaxPrintName().equals("")
				   && prod.getVatTax().getTaxPrintName()!=null && ! prod.getVatTax().getTaxPrintName().equals(""))
				{

					double dot = service + vat;
					retrServ=(entity.getPrice()/(1+dot/100));
					retrServ=entity.getPrice()-retrServ;
//					retrVat=0;

				}
				else{
					// Here if both are inclusive then first remove service tax and then on that amount
					// calculate vat.
					double removeServiceTax=(entity.getPrice()/(1+service/100));
					//double taxPerc=service+vat;
					//retrServ=(entity.getPrice()/(1+taxPerc/100)); //line changed below
					retrServ=(removeServiceTax/(1+vat/100));
					retrServ=entity.getPrice()-retrServ;
				}
			}
			
			/**
			 * ends here
			 */
			
		}
		tax=retrVat+retrServ;
		System.out.println("Returning Tax"+tax);
		return tax;
	}
	
	
		public boolean removeChargesOnDelete(){
		
		int tableSize=getDataprovider().getList().size();
		System.out.println("Printing Here The Table Size"+tableSize);
		
		if(tableSize<1){
			return false;
		}
		return true;
	}
	
		
		/**
		 * Date 21 july 2017 added by vijay for quick sales order	
		 */
			@Override
			public void onClick(ClickEvent event) {
				if(event.getSource()==invLoc.getAddButton())
				{	
					if(invLoc.getLsWarehouse().getSelectedIndex()!=0&&invLoc.getLsStorageLoc().getSelectedIndex()!=0&&invLoc.getLsStoragebin().getSelectedIndex()!=0){
						ArrayList<SalesLineItem> list=new ArrayList<SalesLineItem>();
						if(getDataprovider().getList().size()!=0){
							list.addAll(getDataprovider().getList());
							for( int i=rowIndex;i<getDataprovider().getList().size();i++){
								list.get(rowIndex).setItemProductWarehouoseName(invLoc.getLsWarehouse().getValue(invLoc.getLsWarehouse().getSelectedIndex()));
								list.get(rowIndex).setItemProductStorageLocation(invLoc.getLsStorageLoc().getValue(invLoc.getLsStorageLoc().getSelectedIndex()));
								list.get(rowIndex).setItemStorageBin(invLoc.getLsStoragebin().getValue(invLoc.getLsStoragebin().getSelectedIndex()));
								list.get(rowIndex).setItemProductAvailableQty(invLoc.getAvailableQty());
								
								getDataprovider().getList().clear();
								getDataprovider().getList().addAll(list);
								
							}
						}
						panel.hide();
					}
					if(invLoc.getLsWarehouse().getSelectedIndex()==0&&invLoc.getLsStorageLoc().getSelectedIndex()==0&&invLoc.getLsStoragebin().getSelectedIndex()==0){
						SalesOrderPresenter.showMessage("Please Select Warehouse.");
					}
					else if(invLoc.getLsStorageLoc().getSelectedIndex()==0&&invLoc.getLsStoragebin().getSelectedIndex()==0){
						SalesOrderPresenter.showMessage("Please Select Storage Location.");
					}
					else if(invLoc.getLsStoragebin().getSelectedIndex()==0){
						SalesOrderPresenter.showMessage("Please Select Storage Bin.");
					}
					
				}
				if(event.getSource()==invLoc.getCancelButton())
				{	
					panel.hide();
				}
				
			/**
			 * nidhi
			 * 9-06-2018
			 */
				if(event.getSource().equals(itemDtPopUp.getLblOk())){
					getDataprovider().getList().get(rowIndex).getPrduct().setComment((itemDtPopUp.getTaDetails().getValue() != null) ? itemDtPopUp.getTaDetails().getValue() : "");
					getDataprovider().getList().get(rowIndex).getPrduct().setCommentdesc((itemDtPopUp.getTaDetails1().getValue() != null) ? itemDtPopUp.getTaDetails1().getValue() : "");
					itemDtPopUp.hidePopUp();
				}else if(event.getSource().equals(itemDtPopUp.getLblCancel())){
					itemDtPopUp.hidePopUp();
				}
				
				
				/**
				 * nidhi
				 * 22-08-2018
				 */
				if(event.getSource() == prodSerialPopup.getLblOk()){
					ArrayList<ProductSerialNoMapping> proList = new ArrayList<ProductSerialNoMapping>();
//					proList.addAll(prodSerialPopup.getProSerNoTable().getDataprovider().getList());
					List<ProductSerialNoMapping> mainproList = prodSerialPopup.getProSerNoTable().getDataprovider().getList();
					for(int i =0 ; i <mainproList.size();i++){
						System.out.println(mainproList.get(i).getProSerialNo() + "  --- get serial number -- ");
						if(mainproList.get(i).getProSerialNo().trim().length()>0 && mainproList.get(i).isStatus()){
							proList.add(mainproList.get(i));
						}
					}
					
					ArrayList<SalesLineItem> list=new ArrayList<SalesLineItem>();
					list.addAll(getDataprovider().getList());
					
					if(proList.size() !=0 && proList.size() < list.get(rowIndex).getQty() && mainproList.size()>list.get(rowIndex).getQty() ){
						GWTCAlert alert = new GWTCAlert();
						alert.alert("Please select serial no atlist equal to product quantity");
						return;
					}
					
					if(proList.size() !=0 ){
						list.get(rowIndex).setQuantity((double) proList.size());
						
					}
					list.get(rowIndex).getProSerialNoDetails().clear();
					list.get(rowIndex).getProSerialNoDetails().put(0, proList);
					getDataprovider().getList().clear();
					
					getDataprovider().setList(list);
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
//					table.redrawRow(rowIndex);
					prodSerialPopup.hidePopUp();
				}
				if(event.getSource()== prodSerialPopup.getLblCancel()){
					prodSerialPopup.hidePopUp();
				}
				
			
			
			}
			
			/**
			 * ends here
			 */
			protected void createViewAmcNumberColumn(){				
				amcNumberViewColumn=new TextColumn<SalesLineItem>() {
					public String getValue(SalesLineItem object)
					{
						return object.getAmcNumber();
					}
				};
				table.addColumn(amcNumberViewColumn,"AMC Number" );
				table.setColumnWidth(amcNumberViewColumn, 100,Unit.PX);
			}
			/** 23/1/2018 added by komal for hvac**/
			public void createColumnAddAmcColumn() {
				ButtonCell btnCell = new ButtonCell();
				addAmcBtnColumn = new Column<SalesLineItem, String>(btnCell) {
					@Override
					public String getValue(SalesLineItem object) {
						return "Select AMC";
					}
				};
				table.addColumn(addAmcBtnColumn, "");
				table.setColumnWidth(addAmcBtnColumn,170,Unit.PX);

			}
			private void updateAmcBtn() {				
				addAmcBtnColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {
					@Override
					public void update(int index, final SalesLineItem object,
							String value) {
						
						rowIndex = index;
						salesLineItem = object;
						if(QuickSalesOrderForm.cList.size()>0){
							amcPopup.showPopUp();  
							ArrayList<CompanyAsset> list = new ArrayList<CompanyAsset>();
							if(QuickSalesOrderForm.cList.size() >0){
								for(String assetName :QuickSalesOrderForm.cList){
									CompanyAsset c = new CompanyAsset();
									c.setAmcStatus("Allocated");
									c.setName(assetName);
									list.add(c);
								}
							}
							
							amcPopup.getAmcAllocationTable().clear();
							amcPopup.getAmcAllocationTable().getDataprovider().setList(list);
						}
						else{
							amcPopup.getAmcAllocationTable().clear();
							amcPopup.showPopUp();
						}
						
					}
				});
			}
			
			/**
			 * nidhi
			 * 9-06-2018
			 * for details
			 */
			
			protected void getDetailsButtonColumn() {
				ButtonCell btnCell = new ButtonCell();
				getDetailsButtonColumn = new Column<SalesLineItem, String>(btnCell) {
					@Override
					public String getValue(SalesLineItem object) {
						return "Product Details";
					}
					
					@Override
					public void onBrowserEvent(Context context, Element elem,SalesLineItem object, NativeEvent event) {
						
						if(marginFlag==false){
							super.onBrowserEvent(context, elem, object, event);
							return;
						}
						
						if(object.isReadOnly()==false
								||((UserConfiguration.getRole()!=null&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin"))
								||(UserConfiguration.getRole()!=null&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Zonal Coordinator")))){
							super.onBrowserEvent(context, elem, object, event);
						}
					}
				};
				
				
				table.addColumn(getDetailsButtonColumn, "Product Details");
				table.setColumnWidth(getDetailsButtonColumn, 100, Unit.PX);
			}

			protected void createFieldUpdaterOnBranchesColumn() {
				getDetailsButtonColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {
					@Override
					public void update(int index, SalesLineItem object,String value) {
						rowIndex = index;
						itemDtPopUp.showPopUp();
						itemDtPopUp.getTaDetails().setValue(object.getPrduct().getComment());
						itemDtPopUp.getTaDetails1().setValue(object.getPrduct().getCommentdesc());
						
					}
				});
			}
			


			public void createColumnAddProSerialNoColumn() {
				ButtonCell btnCell = new ButtonCell();
				addProSerialNo = new Column<SalesLineItem, String>(btnCell) {
					@Override
					public String getValue(SalesLineItem object) {
						return "Product Serial Nos";
					}
					
					@Override
					public void onBrowserEvent(Context context, Element elem,SalesLineItem object, NativeEvent event) {
						
						if(marginFlag==false){
							super.onBrowserEvent(context, elem, object, event);
							return;
						}
						
						if(object.isReadOnly()==false
								||((UserConfiguration.getRole()!=null&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin"))
								||(UserConfiguration.getRole()!=null&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Zonal Coordinator")))){
							super.onBrowserEvent(context, elem, object, event);
						}
					}
				};
				table.addColumn(addProSerialNo, "");
				table.setColumnWidth(addProSerialNo,180,Unit.PX);

			}
			public void createFieldUpdaterAddSerialNo() {
				addProSerialNo.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {
					@Override
					public void update(int index, final SalesLineItem object, String value) {
						rowIndex = index;
						InventoryLocationPopUp.initialzeWarehouse(object.getPrduct().getCount());
						prodSerialPopup.setProSerialenble(true,true);
						prodSerialPopup.getPopup().setSize("50%", "40%");
						/**
						 * nidhi
						 * 23-08-2018
						 */
						 Timer timer=new Timer() 
			        	 {
			 				@Override
			 				public void run() {
			 					proSerialList.clear();
								if(object.getItemProductWarehouseName()!=null && object.getItemProductStorageBin()!=null  && object.getItemProductStrorageLocation()!=null
										&& object.getItemProductWarehouseName().length()>0 && object.getItemProductStorageBin().length()>0  && object.getItemProductStrorageLocation().length()>0){
									ArrayList<SalesLineItem> list=new ArrayList<SalesLineItem>();
									
									for(StorageLocationBin strBIn : InventoryLocationPopUp.strLocStrBin){
										System.out.println("wr = "+object.getItemProductWarehouseName() 
												+" bin =  " +object.getItemProductStorageBin()
												+ " lc = " +object.getItemProductStrorageLocation());
										if(strBIn.getWarehouseName().equals(object.getItemProductWarehouseName()) 
												&& strBIn.getStorageLocation().equals(object.getItemProductStrorageLocation())
												&& strBIn.getStorageBin().equals(object.getItemProductStorageBin()))
										{
											
											ArrayList<ProductSerialNoMapping> prMp= new ArrayList<ProductSerialNoMapping>();
											if(strBIn.getProdSerialNoList().get(0)!=null && strBIn.getProdSerialNoList().get(0).size()>0){
												
												for(ProductSerialNoMapping prmain : strBIn.getProdSerialNoList().get(0)){
													ProductSerialNoMapping pr1 = new ProductSerialNoMapping();
													pr1.setAvailableStatus(prmain.isAvailableStatus());
													pr1.setNewAddNo(prmain.isNewAddNo());
													pr1.setStatus(prmain.isStatus());
													pr1.setProSerialNo(prmain.getProSerialNo());
													prMp.add(pr1);
												}
												
											}
											proSerialList.put(0, prMp);
											System.out.println("get size -- " + proSerialList.get(0).toArray().toString());
//											proSerialList.putAll(strBIn.getProdSerialNoList());
											list.addAll(getDataprovider().getList());
											ArrayList<ProductSerialNoMapping> proserMap = list.get(rowIndex).getProSerialNoDetails().get(0);
										}
									
									}
								}
								
									
									if(object.getProSerialNoDetails().size()>0 || proSerialList.size()>0){
										
										ArrayList<ProductSerialNoMapping> serNo = new ArrayList<ProductSerialNoMapping>();
										System.out.println("size  -- " + serNo.size());
										if(object.getProSerialNoDetails().containsKey(0)){
											serNo.addAll(object.getProSerialNoDetails().get(0));
										}
										if(!proSerialList.containsKey(0)  || proSerialList.keySet().size() == 0 || proSerialList.get(0) == null){
											proSerialList.put(0,new ArrayList<ProductSerialNoMapping>());
										}else{
											System.out.println("pro called -- " + proSerialList.keySet());
										}
										int countpr = 0 ;
										if(proSerialList.get(0) != null){
											 countpr = proSerialList.get(0).size();
										}
										
										if(serNo.size()>0 || (proSerialList.size()>0)){
											boolean get= false;
											
											for(int i=0;i<serNo.size();i++){
												get =false;
												int j=0;
												if(proSerialList.get(0) !=null){
													for(j=0;j<proSerialList.get(0).size();j++){
														if(proSerialList.get(0).get(j).getProSerialNo().equals(serNo.get(i).getProSerialNo())){
															proSerialList.get(0).get(j).setAvailableStatus(true);
															proSerialList.get(0).get(j).setStatus(true);
															get= true;
															break;
														}
													}
												}
												
												if(!get){
													ProductSerialNoMapping prodt = new ProductSerialNoMapping();
													prodt.setProSerialNo(serNo.get(i).getProSerialNo());
													prodt.setAvailableStatus(false);
													prodt.setStatus(true);
													proSerialList.get(0).add(prodt);
												}
											}
											
										}
										if(countpr< object.getQty()){
											
											if(!proSerialList.containsKey(0) || proSerialList.keySet().size() == 0 || proSerialList.get(0) == null){
												proSerialList.put(0,new ArrayList<ProductSerialNoMapping>());
											}else{
												System.out.println("pro called -- " + proSerialList.keySet());
											}
											
												int count = (int) (object.getQty()- countpr);
												for(int i = 0 ; i < count ; i ++ ){
													ProductSerialNoMapping pro = new ProductSerialNoMapping();
													pro.setAvailableStatus(false);
													pro.setStatus(false);
													proSerialList.get(0).add(pro);
												}
												prodSerialPopup.getProSerNoTable().getDataproviderlist().addAll(proSerialList.get(0));
											
											
										}
										
									}
									if(proSerialList.size()==0){
										if(!proSerialList.containsKey(0)){
											proSerialList.put(0, new ArrayList<ProductSerialNoMapping>());
										}
										
										ArrayList<ProductSerialNoMapping> serNo = new ArrayList<ProductSerialNoMapping>();
										for(int i = 0 ; i < object.getQty(); i ++ ){
											ProductSerialNoMapping pro = new ProductSerialNoMapping();
											pro.setProSerialNo("");
											pro.setAvailableStatus(false);
											pro.setNewAddNo(true);
											serNo.add(pro);
										}
										proSerialList.put(0, serNo);
									}
									System.out.println("get ser no list  -- " +proSerialList.get(0).toString());
									prodSerialPopup.getProSerNoTable().getDataprovider().getList().clear();
									prodSerialPopup.getProSerNoTable().getDataprovider().getList().addAll(proSerialList.get(0));
									prodSerialPopup.getProSerNoTable().getTable().redraw();
								prodSerialPopup.showPopUp();
							
			 					
			 				}
			 			 };
				        	 timer.schedule(2000);
			 				
						
					}
				});
			}
			
			public void createFieldUpdaterViewSerialNo() {
				viewProSerialNo.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {
					@Override
					public void update(int index, SalesLineItem object, String value) {
						prodSerialPopup.setProSerialenble(false,false);
						prodSerialPopup.getPopup().setSize("50%", "40%");
						rowIndex  = index;
						if(object.getItemProductWarehouseName()!=null && object.getItemProductStorageBin()!=null  && object.getItemProductStrorageLocation()!=null
								&& object.getItemProductWarehouseName().length()>0 && object.getItemProductStorageBin().length()>0  && object.getItemProductStrorageLocation().length()>0){
							ArrayList<SalesLineItem> list=new ArrayList<SalesLineItem>();
							/**
							 * nidhi
							 * 23-08-2018
							 */
							for(StorageLocationBin strBIn :InventoryLocationPopUp.strLocStrBin){
								System.out.println("wr = "+object.getItemProductWarehouseName() 
										+" bin =  " +object.getItemProductStorageBin()
										+ " lc = " +object.getItemProductStrorageLocation());
								if(strBIn.getWarehouseName().equals(object.getItemProductWarehouseName()) 
										&& strBIn.getStorageLocation().equals(object.getItemProductStrorageLocation())
										&& strBIn.getStorageBin().equals(object.getItemProductStorageBin()))
								{
									proSerialList.clear();
									proSerialList.putAll(strBIn.getProdSerialNoList());
									list.addAll(getDataprovider().getList());
									ArrayList<ProductSerialNoMapping> proserMap = list.get(rowIndex).getProSerialNoDetails().get(0);
								}
							}
						}
						if(object.getProSerialNoDetails().size()>0){
							
							ArrayList<ProductSerialNoMapping> serNo = new ArrayList<ProductSerialNoMapping>();
							System.out.println("size  -- " + serNo.size());
							
							if(object.getProSerialNoDetails().containsKey(0)){
								serNo.addAll(object.getProSerialNoDetails().get(0));
							}
							
							if(proSerialList != null && proSerialList.get(0).size()>0 && serNo.size()>0){
								boolean get= false;
								for(int i=0;i<serNo.size();i++){
									get =false;
									int j=0;
									for(j=0;j<proSerialList.get(0).size();j++){
										if(proSerialList.get(0).get(j).getProSerialNo().equals(serNo.get(i).getProSerialNo())){
											serNo.get(j).setAvailableStatus(true);
											serNo.get(j).setStatus(true);
											get= true;
											break;
										}
									}
									if(!get){
										serNo.get(i).setAvailableStatus(false);
										serNo.get(i).setStatus(true);
									}
								}
								
							}
							
							prodSerialPopup.getProSerNoTable().getDataprovider().getList().clear();
							prodSerialPopup.getProSerNoTable().getDataprovider().getList().addAll(serNo);
							prodSerialPopup.getProSerNoTable().getTable().redraw();
						}
						prodSerialPopup.showPopUp();
					
					}
				});
			}
			
			public void createViewColumnAddProSerialNoColumn() {
				ButtonCell btnCell = new ButtonCell();
				viewProSerialNo = new Column<SalesLineItem, String>(btnCell) {
					@Override
					public String getValue(SalesLineItem object) {
						return "View Product Serial Nos";
					}
				};
				table.addColumn(viewProSerialNo, "");
				table.setColumnWidth(viewProSerialNo,180,Unit.PX);

			}
			
			
			
		private void createFieldUpdaterOnProductName() {
			prodNameColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {
				
				@Override
				public void update(int index, SalesLineItem object, String value) {
					object.setProductName(value);
					table.redrawRow(index);
				}
			});
			
		}

}
