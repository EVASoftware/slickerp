package com.slicktechnologies.client.views.salesorder;

import java.util.ArrayList;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.BorderStyle;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.salesorder.OtherCharges;
import com.slicktechnologies.shared.common.salesorder.OtherChargesMargin;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesorder.VendorMargin;

public class SalesMarginPopup extends PopupScreen {
	
	TextBox tbActualMargin,tbActualMarginPer,tbSalesMargin,tbSalesMarginPer,tbPurchaseMargin,tbPurchaseMarginPer;
	SalesMarginSummaryTable salesSummaryTbl,purchaseSummaryTbl;
	GenricServiceAsync service=GWT.create(GenricService.class);
	
	double actualSalesPrice=0;
	double actualSalesOc=0;
	double actualSalesTotal=0;
	
	double estimatedSalesPrice=0;
	double estimatedSalesOc=0;
	double estimatedSalesTotal=0;
	
	double actualPurchasePrice=0;
	double actualPurchaseOc=0;
	double actualPurchaseTotal=0;
	
	double salesMargin=0;
	double salesMarginPer=0;
	
	double purchaseMargin=0;
	double purchaseMarginPer=0;
	
	double actualMargin=0;
	double actualMarginPer=0;
	
	public SalesMarginPopup() {
		super();
		createGui();
		salesSummaryTbl.connectToLocal();
		purchaseSummaryTbl.connectToLocal();
	}
	
	private void initializeWidget() {
		tbActualMargin=new TextBox();
		tbActualMargin.setEnabled(false);
		tbActualMarginPer=new TextBox();
		tbActualMarginPer.setEnabled(false);
		
		tbSalesMargin=new TextBox();
		tbSalesMargin.setEnabled(false);
		tbSalesMarginPer=new TextBox();
		tbSalesMarginPer.setEnabled(false);
		
		tbPurchaseMargin=new TextBox();
		tbPurchaseMargin.setEnabled(false);
		tbPurchaseMarginPer=new TextBox();
		tbPurchaseMarginPer.setEnabled(false);
		
		salesSummaryTbl=new SalesMarginSummaryTable();
		purchaseSummaryTbl=new SalesMarginSummaryTable(true);
		
	}
	
	public void clear(){
		salesSummaryTbl.connectToLocal();
		purchaseSummaryTbl.connectToLocal();
		
		tbActualMargin.setText("");
		tbActualMarginPer.setText("");
		tbSalesMargin.setText("");
		tbSalesMarginPer.setText("");
		tbPurchaseMargin.setText("");
		tbPurchaseMarginPer.setText("");
		
		actualSalesPrice=0;
		actualSalesOc=0;
		actualSalesTotal=0;
		
		estimatedSalesPrice=0;
		estimatedSalesOc=0;
		estimatedSalesTotal=0;
		
		actualPurchasePrice=0;
		actualPurchaseOc=0;
		actualPurchaseTotal=0;
		
		salesMargin=0;
		salesMarginPer=0;
		
		purchaseMargin=0;
		purchaseMarginPer=0;
		
		actualMargin=0;
		actualMarginPer=0;
		
	}
	
	@Override
	public void createScreen() {
		initializeWidget();
		
		FormFieldBuilder fbuilder;
		
		fbuilder = new FormFieldBuilder();
		FormField fActMarginGrping=fbuilder.setlabel("Actual Margin").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder();
		FormField fSalesGrping=fbuilder.setlabel("Sales Department").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder();
		FormField fPurchaseGrping=fbuilder.setlabel("Purchase Department").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",salesSummaryTbl.getTable());
		FormField fsalesSummaryTbl= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("",purchaseSummaryTbl.getTable());
		FormField fpurchaseSummaryTbl= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Total Margin",tbActualMargin);
		FormField ftbActualMargin= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("% Margin",tbActualMarginPer);
		FormField ftbActualMarginPer= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Total Margin",tbSalesMargin);
		FormField ftbSalesMargin= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("% Margin",tbSalesMarginPer);
		FormField ftbSalesMarginPer= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Total Margin",tbPurchaseMargin);
		FormField ftbPurchaseMargin= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("% Margin",tbPurchaseMarginPer);
		FormField ftbPurchaseMarginPer= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		FormField[][] formfield = {
				{fActMarginGrping},
				{ftbActualMargin,ftbActualMarginPer},
				{fSalesGrping},
				{fsalesSummaryTbl},
				{ftbSalesMargin,ftbSalesMarginPer},
				{fPurchaseGrping},
				{fpurchaseSummaryTbl},
				{ftbPurchaseMargin,ftbPurchaseMarginPer}
		};

		this.fields=formfield;
		
	}

	
	@Override
	public void onClick(ClickEvent event) {
		
	}
	
	@Override
	public void applyStyle() {
		content.getElement().getStyle().setWidth(650, Unit.PX);
		content.getElement().getStyle().setBorderWidth(1, Unit.PX);
		content.getElement().getStyle().setBorderStyle(BorderStyle.SOLID);
		form.getElement().setId("form");
		horizontal.getElement().addClassName("popupcentering");
		lblOk.getElement().setId("addbutton");
		lblCancel.getElement().setId("addbutton");
		
		lblOk.setVisible(false);
		lblCancel.setVisible(false);
		popup.center();
	}
	
	public void loadSummaryDetails(SalesOrder salesOrder){
		showWaitSymbol();
		if(salesOrder!=null){
			if(salesOrder.getVendorMargins()!=null&&salesOrder.getVendorMargins().size()!=0){
				for(VendorMargin obj:salesOrder.getVendorMargins()){
					actualSalesPrice=actualSalesPrice+obj.getSalesPrice();
					estimatedSalesPrice=estimatedSalesPrice+obj.getVendorPrice();
				}
			}
			
			if(salesOrder.getOtherChargesMargins()!=null&&salesOrder.getOtherChargesMargins().size()!=0){
				for(OtherChargesMargin obj:salesOrder.getOtherChargesMargins()){
					estimatedSalesOc=estimatedSalesOc+obj.getOtherCharges();
				}
			}
			
			if(salesOrder.getOtherCharges()!=null&&salesOrder.getOtherCharges().size()!=0){
				for(OtherCharges obj:salesOrder.getOtherCharges()){
					actualSalesOc=actualSalesOc+obj.getAmount();
				}
			}
			actualSalesTotal=actualSalesPrice+actualSalesOc;
			estimatedSalesTotal=estimatedSalesPrice+estimatedSalesOc;
			
			salesMargin=actualSalesTotal-estimatedSalesTotal;
			if(actualSalesTotal!=0){
				salesMarginPer=(salesMargin/actualSalesTotal)*100;
			}
			
			SalesMarginSummary sales=new SalesMarginSummary();
			sales.setPriceType("Sales Order Price");
			sales.setProductPrice(actualSalesPrice);
			sales.setOtherCharges(actualSalesOc);
			sales.setTotal(actualSalesTotal);
			salesSummaryTbl.getDataprovider().getList().add(sales);
			
			SalesMarginSummary sales1=new SalesMarginSummary();
			sales1.setPriceType("Estimated Price (Sales Dpt)");
			sales1.setProductPrice(estimatedSalesPrice);
			sales1.setOtherCharges(estimatedSalesOc);
			sales1.setTotal(estimatedSalesTotal);
			salesSummaryTbl.getDataprovider().getList().add(sales1);
			
			tbSalesMargin.setValue(Math.round(salesMargin)+"");
			tbSalesMarginPer.setValue(Math.round(salesMarginPer)+"");
			
			MyQuerry querry = new MyQuerry();
			Company c = new Company();
			Vector<Filter> filtervec = new Vector<Filter>();
			Filter filter = null;
			
			filter = new Filter();
			filter.setQuerryString("companyId");
			filter.setLongValue(c.getCompanyId());
			filtervec.add(filter);
			
			filter = new Filter();
			filter.setQuerryString("refOrderNO");
			filter.setStringValue(salesOrder.getCount()+"");
			filtervec.add(filter);
			
			filter = new Filter();
			filter.setQuerryString("status");
			filter.setStringValue("Approved");
			filtervec.add(filter);
			
			querry.setFilters(filtervec);
			querry.setQuerryObject(new PurchaseOrder());
			service.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
				@Override
				public void onFailure(Throwable caught) {
					showDialogMessage("Error : "+caught.getMessage());
					hideWaitSymbol();
				}
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					hideWaitSymbol();
					
					if(result.size()!=0){
						for(SuperModel model:result){
							PurchaseOrder purchaseOrder=(PurchaseOrder) model;
							double price = 0;
							double oc=0;
							if(purchaseOrder.getVendorMargins()!=null&&purchaseOrder.getVendorMargins().size()!=0){
								for(VendorMargin obj:purchaseOrder.getVendorMargins()){
									actualPurchasePrice=actualPurchasePrice+obj.getPurchasePrice();
									price=price+obj.getPurchasePrice();
								}
							}
							
							if(purchaseOrder.getOtherChargesMargins()!=null&&purchaseOrder.getOtherChargesMargins().size()!=0){
								for(OtherChargesMargin obj:purchaseOrder.getOtherChargesMargins()){
									actualPurchaseOc=actualPurchaseOc+obj.getOtherCharges();
									oc=oc+obj.getOtherCharges();
								}
							}
							SalesMarginSummary purchaseSummary=new SalesMarginSummary();
							purchaseSummary.setPriceType(purchaseOrder.getCount()+"");
							purchaseSummary.setProductPrice(price);
							purchaseSummary.setOtherCharges(oc);
							purchaseSummary.setTotal(price+oc);
							purchaseSummaryTbl.getDataprovider().getList().add(purchaseSummary);
							
						}
						
						actualPurchaseTotal=actualPurchasePrice+actualPurchaseOc;
						
						SalesMarginSummary purchaseSummary=new SalesMarginSummary();
						purchaseSummary.setPriceType("Total");
						purchaseSummary.setProductPrice(actualPurchasePrice);
						purchaseSummary.setOtherCharges(actualPurchaseOc);
						purchaseSummary.setTotal(actualPurchaseTotal);
						purchaseSummaryTbl.getDataprovider().getList().add(purchaseSummary);
						
						purchaseMargin=estimatedSalesTotal-actualPurchaseTotal;
						if(estimatedSalesTotal!=0){
							purchaseMarginPer=(purchaseMargin/estimatedSalesTotal)*100;
						}
						
						actualMargin=actualSalesTotal-actualPurchaseTotal;
						if(actualSalesTotal!=0){
							actualMarginPer=(actualMargin/actualSalesTotal)*100;
						}
						
						
						tbPurchaseMargin.setValue(Math.round(purchaseMargin)+"");
						tbPurchaseMarginPer.setValue(Math.round(purchaseMarginPer)+"");
						
						tbActualMargin.setValue(Math.round(actualMargin)+"");
						tbActualMarginPer.setValue(Math.round(actualMarginPer)+"");
						
						
					}else{
//						actualMargin=actualSalesPrice-actualPurchaseTotal;
//						if(actualSalesPrice!=0){
//							actualMarginPer=(actualMargin/actualSalesPrice)*100;
//						}
						
						tbActualMargin.setValue(Math.round(salesMargin)+"");
						tbActualMarginPer.setValue(Math.round(salesMarginPer)+"");
					}
					
				}
			});
		}
		
	}

}
