package com.slicktechnologies.client.views.salesorder;

import com.simplesoftwares.client.library.appstructure.SuperModel;

public class SalesMarginSummary extends SuperModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4404060786022774106L;
	
	String priceType;
	double productPrice;
	double otherCharges;
	double total;

	
	public String getPriceType() {
		return priceType;
	}


	public void setPriceType(String priceType) {
		this.priceType = priceType;
	}


	public double getProductPrice() {
		return productPrice;
	}


	public void setProductPrice(double productPrice) {
		this.productPrice = productPrice;
	}


	public double getOtherCharges() {
		return otherCharges;
	}


	public void setOtherCharges(double otherCharges) {
		this.otherCharges = otherCharges;
	}


	public double getTotal() {
		return total;
	}


	public void setTotal(double total) {
		this.total = total;
	}


	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}

}
