package com.slicktechnologies.client.views.salesorder;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.slicktechnologies.shared.common.contractcancel.CancelContract;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;

public interface SalesOrderServiceAsync {

	void changeStatus(SalesOrder salesorder, AsyncCallback<Void> callback);
	
	/**
	 * date 08/02/2018 added by komal for sales order cancel (new)
	 */
	void getRecordsForSalesOrderCancel(Long companyId, int orderId,String customerName ,AsyncCallback<ArrayList<CancelContract>> asyncCallback);

	void saveSalesOrderCancelData(SalesOrder salesOrder, String remark,String loginUser, ArrayList<CancelContract> cancelContractList,AsyncCallback<Void> callback);
	/**
	 * end komal
	 */

}
