package com.slicktechnologies.client.views.salesorder;

import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;

public class SalesMarginSummaryTable extends SuperTable<SalesMarginSummary> {

	TextColumn<SalesMarginSummary>priceTypeCol;
	TextColumn<SalesMarginSummary>productPriceCol;
	TextColumn<SalesMarginSummary>otherChargesCol;
	TextColumn<SalesMarginSummary> totalCol;
	
	public SalesMarginSummaryTable() {
		super();
	}
	
	public SalesMarginSummaryTable(boolean purchaseFlag) {
		super(purchaseFlag);
		createTable1();
	}
	
	@Override
	public void createTable() {
		priceTypeCol();
		productPriceCol();
		otherChargesCol();
		totalCol();
	}
	public void createTable1() {
		poIdCol();
		productPriceCol();
		otherChargesCol();
		totalCol();
	}
	
	private void totalCol() {
		totalCol = new TextColumn<SalesMarginSummary>() {
			@Override
			public String getValue(SalesMarginSummary object) {
				return object.getTotal()+"";
			}
		};		
		table.addColumn(totalCol, "Total");
	}
	
	private void productPriceCol(){
		productPriceCol=new TextColumn<SalesMarginSummary>() {
			@Override
			public String getValue(SalesMarginSummary object) {
				return object.getProductPrice()+"";
			}
		};
		table.addColumn(productPriceCol, "Product Price");
	}
	
	
	private void otherChargesCol(){
		otherChargesCol=new TextColumn<SalesMarginSummary>() {
			@Override
			public String getValue(SalesMarginSummary object) {
				return object.getOtherCharges() + "";
			}
		};
		table.addColumn(otherChargesCol, "Other Charges");
	}
	
	private void priceTypeCol(){
		priceTypeCol=new TextColumn<SalesMarginSummary>() {
			@Override
			public String getValue(SalesMarginSummary object) {
				return object.getPriceType();
			}
		};
		table.addColumn(priceTypeCol, "Price Type");
	}
	
	private void poIdCol(){
		priceTypeCol=new TextColumn<SalesMarginSummary>() {
			@Override
			public String getValue(SalesMarginSummary object) {
				return object.getPriceType();
			}
		};
		table.addColumn(priceTypeCol, "PO Id");
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
//		for(int i=table.getColumnCount()-1;i>-1;i--)
//	    	  table.removeColumn(i); 
//	        
//		if(state){
//			priceTypeCol();
//			productPriceCol();
//			otherChargesCol();
//			totalCol();
//		}else{
//			priceTypeCol();
//			productPriceCol();
//			otherChargesCol();
//		}
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
