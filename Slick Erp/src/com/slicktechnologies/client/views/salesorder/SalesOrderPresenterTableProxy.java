package com.slicktechnologies.client.views.salesorder;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.NumberCell;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.ProvidesKey;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.salesorder.SalesOrderPresenter.SalesOrderPresenterTable;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;

public class SalesOrderPresenterTableProxy extends SalesOrderPresenterTable {
	
	TextColumn<SalesOrder> getCustomerCellNumberColumn;
	TextColumn<SalesOrder> getCustomerFullNameColumn;
	TextColumn<SalesOrder> getCustomerIdColumn;
	TextColumn<SalesOrder> getSalesOrderCountColumn;
	TextColumn<SalesOrder> getQuotationCountColumn;
	TextColumn<SalesOrder> getLeadCountColumn;
	TextColumn<SalesOrder> getBranchColumn;
	TextColumn<SalesOrder> getCustomerBranchColumn;//Ashwini Patil Date:23-03-2023
	TextColumn<SalesOrder> getEmployeeColumn;
	TextColumn<SalesOrder> getStatusColumn;
	TextColumn<SalesOrder> getCreationDateColumn;
	TextColumn<SalesOrder> getGroupColumn;
	TextColumn<SalesOrder> getCategoryColumn;
	TextColumn<SalesOrder> getTypeColumn;
	TextColumn<SalesOrder> getCountColumn;
	public Object getVarRef(String varName)
	{
		if(varName.equals("getCustomerCellNumberColumn"))
			return this.getCustomerCellNumberColumn;
		if(varName.equals("getCustomerFullNameColumn"))
			return this.getCustomerFullNameColumn;
		if(varName.equals("getCustomerIdColumn"))
			return this.getCustomerIdColumn;
		if(varName.equals("getSalesOrderCountColumn"))
			return this.getSalesOrderCountColumn;
		if(varName.equals("getQuotationCountColumn"))
			return this.getQuotationCountColumn;
		if(varName.equals("getLeadCountColumn"))
			return this.getLeadCountColumn;
		if(varName.equals("getBranchColumn"))
			return this.getBranchColumn;
		if(varName.equals("getEmployeeColumn"))
			return this.getEmployeeColumn;
		if(varName.equals("getStatusColumn"))
			return this.getStatusColumn;
		if(varName.equals("getCreationDateColumn"))
			return this.getCreationDateColumn;
		if(varName.equals("getGroupColumn"))
			return this.getGroupColumn;
		if(varName.equals("getCategoryColumn"))
			return this.getCategoryColumn;
		if(varName.equals("getTypeColumn"))
			return this.getTypeColumn;
		if(varName.equals("getCountColumn"))
			return this.getCountColumn;
		return null ;
	}
	public SalesOrderPresenterTableProxy()
	{
		super();
	}
	@Override public void createTable() {
		addColumngetCount();
		addColumngetLeadCount();
		addColumngetQuotationCount();
		addColumngetCustomerId();
		addColumngetCustomerFullName();
		addColumngetCustomerCellNumber();
		addColumngetEmployee();
		addColumngetBranch();
		addColumngetCustomerBranch();
		addColumngetCreationDate();
		addColumngetGroup();
		addColumngetCategory();
		addColumngetType();
		addColumngetStatus();
		
		
		System.out.println("Ajay-----------");
	}
	@Override
	protected void initializekeyprovider() {
		keyProvider= new ProvidesKey<SalesOrder>()
				{
			@Override
			public Object getKey(SalesOrder item)
			{
				if(item==null)
				{
					return null;
				}
				else
					return item.getId();
			}
				};
	}
	@Override
	public void setEnable(boolean state)
	{
	}
	@Override
	public void applyStyle()
	{
	}
	public void addColumnSorting(){
		addSortinggetCount();
		addSortinggetBranch();
		addSortinggetEmployee();
		addSortinggetStatus();
		addSortinggetLeadCount();
		addSortinggetGroup();
		addSortinggetQuotationCount();
		addSortinggetCategory();
		addSortinggetCustomerId();
		addSortinggetType();
		addSortinggetCustomerFullName();
		addSortinggetCustomerCellNumber();
		addSortinggetSalesOrderCount();
		addSortinggetCreationDate();
	}
	@Override public void addFieldUpdater() {
	}
	protected void addSortinggetCount()
	{
		List<SalesOrder> list=getDataprovider().getList();
		columnSort=new ListHandler<SalesOrder>(list);
		columnSort.setComparator(getCountColumn, new Comparator<SalesOrder>()
				{
			@Override
			public int compare(SalesOrder e1,SalesOrder e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetCount()
	{
		getCountColumn=new TextColumn<SalesOrder>()
				{
			@Override
			public String getValue(SalesOrder object)
			{
				if( object.getCount()==-1)
					return "N.A";
				else return object.getCount()+"";
			}
				};
				table.addColumn(getCountColumn,"ID");

				table.setColumnWidth(getCountColumn,100,Unit.PX);
				getCountColumn.setSortable(true);
	}
	protected void addColumngetCreationDate()
	{
		DateTimeFormat fmt = DateTimeFormat.getFormat(PredefinedFormat.DATE_SHORT);
		DatePickerCell date= new DatePickerCell(fmt);
		getCreationDateColumn=new TextColumn<SalesOrder>()
				{
			@Override
			public String getValue(SalesOrder object)
			{
				if(object.getCreationDate()!=null)
					return AppUtility.parseDate(object.getCreationDate());
				else
				return "N.A";
			
			}
				};
				table.addColumn(getCreationDateColumn,"Creation Date");
				table.setColumnWidth(getCreationDateColumn,100,Unit.PX);
				getCreationDateColumn.setSortable(true);
	}
	
	protected void addSortinggetCreationDate()
	{
		List<SalesOrder> list=getDataprovider().getList();
		columnSort=new ListHandler<SalesOrder>(list);
		columnSort.setComparator(getCreationDateColumn, new Comparator<SalesOrder>()
				{
			@Override
			public int compare(SalesOrder e1,SalesOrder e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getCreationDate()!=null && e2.getCreationDate()!=null){
						return e1.getCreationDate().compareTo(e2.getCreationDate());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetBranch()
	{
		List<SalesOrder> list=getDataprovider().getList();
		columnSort=new ListHandler<SalesOrder>(list);
		columnSort.setComparator(getBranchColumn, new Comparator<SalesOrder>()
				{
			@Override
			public int compare(SalesOrder e1,SalesOrder e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBranch()!=null && e2.getBranch()!=null){
						return e1.getBranch().compareTo(e2.getBranch());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetBranch()
	{
		getBranchColumn=new TextColumn<SalesOrder>()
				{
			@Override
			public String getValue(SalesOrder object)
			{
				return object.getBranch()+"";
			}
				};
				table.addColumn(getBranchColumn,"Branch");
				table.setColumnWidth(getBranchColumn, 80, Unit.PX);
				getBranchColumn.setSortable(true);
	}
	protected void addColumngetCustomerBranch()
	{
		getCustomerBranchColumn=new TextColumn<SalesOrder>()
		{
			@Override
			public String getValue(SalesOrder object)
			{
				if(object.getCustBranch()!=null)
					return object.getCustBranch()+"";
				else
					return "";
			}
		};
				table.addColumn(getCustomerBranchColumn,"Customer Branch");
				table.setColumnWidth(getCustomerBranchColumn, 80, Unit.PX);
				getCustomerBranchColumn.setSortable(true);
	}
	protected void addSortinggetEmployee()
	{
		List<SalesOrder> list=getDataprovider().getList();
		columnSort=new ListHandler<SalesOrder>(list);
		columnSort.setComparator(getEmployeeColumn, new Comparator<SalesOrder>()
				{
			@Override
			public int compare(SalesOrder e1,SalesOrder e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getEmployee()!=null && e2.getEmployee()!=null){
						return e1.getEmployee().compareTo(e2.getEmployee());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetEmployee()
	{
		getEmployeeColumn=new TextColumn<SalesOrder>()
				{
			@Override
			public String getValue(SalesOrder object)
			{
				return object.getEmployee()+"";
			}
				};
				table.addColumn(getEmployeeColumn,"Sales Person");
				getEmployeeColumn.setSortable(true);
				table.setColumnWidth(getEmployeeColumn,100,Unit.PX);

	}
	protected void addSortinggetStatus()
	{
		List<SalesOrder> list=getDataprovider().getList();
		columnSort=new ListHandler<SalesOrder>(list);
		columnSort.setComparator(getStatusColumn, new Comparator<SalesOrder>()
				{
			@Override
			public int compare(SalesOrder e1,SalesOrder e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getStatus()!=null && e2.getStatus()!=null){
						return e1.getStatus().compareTo(e2.getStatus());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetStatus()
	{
		getStatusColumn=new TextColumn<SalesOrder>()
				{
			@Override
			public String getValue(SalesOrder object)
			{
				return object.getStatus()+"";
			}
				};
				table.addColumn(getStatusColumn,"Status");
				getStatusColumn.setSortable(true);
				table.setColumnWidth(getStatusColumn,80,Unit.PX);

	}
	protected void addSortinggetLeadCount()
	{
		List<SalesOrder> list=getDataprovider().getList();
		columnSort=new ListHandler<SalesOrder>(list);
		columnSort.setComparator(getLeadCountColumn, new Comparator<SalesOrder>()
				{
			@Override
			public int compare(SalesOrder e1,SalesOrder e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getLeadCount()== e2.getLeadCount()){
						return 0;}
					if(e1.getLeadCount()> e2.getLeadCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetLeadCount()
	{
		getLeadCountColumn=new TextColumn<SalesOrder>()
				{
			@Override
			public String getValue(SalesOrder object)
			{
				if( object.getLeadCount()==-1)
					return "N.A";
				else return object.getLeadCount()+"";
			}
				};
				table.addColumn(getLeadCountColumn,"Lead Id");
				table.setColumnWidth(getLeadCountColumn,80,Unit.PX);

				getLeadCountColumn.setSortable(true);
	}
	protected void addSortinggetGroup()
	{
		List<SalesOrder> list=getDataprovider().getList();
		columnSort=new ListHandler<SalesOrder>(list);
		columnSort.setComparator(getGroupColumn, new Comparator<SalesOrder>()
				{
			@Override
			public int compare(SalesOrder e1,SalesOrder e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getGroup()!=null && e2.getGroup()!=null){
						return e1.getGroup().compareTo(e2.getGroup());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetGroup()
	{
		getGroupColumn=new TextColumn<SalesOrder>()
				{
			@Override
			public String getValue(SalesOrder object)
			{
				return object.getGroup()+"";
			}
				};
				table.addColumn(getGroupColumn,"Group");
				table.setColumnWidth(getGroupColumn,120,Unit.PX);
				getGroupColumn.setSortable(true);
	}
	protected void addSortinggetQuotationCount()
	{
		List<SalesOrder> list=getDataprovider().getList();
		columnSort=new ListHandler<SalesOrder>(list);
		columnSort.setComparator(getQuotationCountColumn, new Comparator<SalesOrder>()
				{
			@Override
			public int compare(SalesOrder e1,SalesOrder e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getQuotationCount()== e2.getQuotationCount()){
						return 0;}
					if(e1.getQuotationCount()> e2.getQuotationCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);


	}
	protected void addColumngetQuotationCount()
	{
		getQuotationCountColumn=new TextColumn<SalesOrder>()
				{
			@Override
			public String getValue(SalesOrder object)
			{
				if( object.getQuotationCount()==-1)
					return "N.A";
				else return object.getQuotationCount()+"";
			}
				};
				table.addColumn(getQuotationCountColumn,"Quotation Id");
				table.setColumnWidth(getQuotationCountColumn,90,Unit.PX);
				getQuotationCountColumn.setSortable(true);
	}
	protected void addSortinggetCategory()
	{
		List<SalesOrder> list=getDataprovider().getList();
		columnSort=new ListHandler<SalesOrder>(list);
		columnSort.setComparator(getCategoryColumn, new Comparator<SalesOrder>()
				{
			@Override
			public int compare(SalesOrder e1,SalesOrder e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getCategory()!=null && e2.getCategory()!=null){
						return e1.getCategory().compareTo(e2.getCategory());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetCategory()
	{
		getCategoryColumn=new TextColumn<SalesOrder>()
				{
			@Override
			public String getValue(SalesOrder object)
			{
				return object.getCategory()+"";
			}
				};
				table.addColumn(getCategoryColumn,"Category");
				table.setColumnWidth(getCategoryColumn, "120px");
				getCategoryColumn.setSortable(true);
	}
	protected void addSortinggetCustomerId()
	{
		List<SalesOrder> list=getDataprovider().getList();
		columnSort=new ListHandler<SalesOrder>(list);
		columnSort.setComparator(getCustomerIdColumn, new Comparator<SalesOrder>()
				{
			@Override
			public int compare(SalesOrder e1,SalesOrder e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCustomerId()== e2.getCustomerId()){
						return 0;}
					if(e1.getCustomerId()> e2.getCustomerId()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetCustomerId()
	{
		getCustomerIdColumn=new TextColumn<SalesOrder>()
				{
			@Override
			public String getValue(SalesOrder object)
			{
				if( object.getCustomerId()==-1)
					return "N.A";
				else return object.getCustomerId()+"";
			}
				};
				table.addColumn(getCustomerIdColumn,"Customer Id");
				table.setColumnWidth(getCustomerIdColumn,100,Unit.PX);
				getCustomerIdColumn.setSortable(true);
	}
	protected void addSortinggetType()
	{
		List<SalesOrder> list=getDataprovider().getList();
		columnSort=new ListHandler<SalesOrder>(list);
		columnSort.setComparator(getTypeColumn, new Comparator<SalesOrder>()
				{
			@Override
			public int compare(SalesOrder e1,SalesOrder e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getType()!=null && e2.getType()!=null){
						return e1.getType().compareTo(e2.getType());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetType()
	{
		getTypeColumn=new TextColumn<SalesOrder>()
				{
			@Override
			public String getValue(SalesOrder object)
			{
				return object.getType()+"";
			}
				};
				table.addColumn(getTypeColumn,"Type");
				table.setColumnWidth(getTypeColumn,100,Unit.PX);
				getTypeColumn.setSortable(true);
	}
	protected void addSortinggetCustomerFullName()
	{
		List<SalesOrder> list=getDataprovider().getList();
		columnSort=new ListHandler<SalesOrder>(list);
		columnSort.setComparator(getCustomerFullNameColumn, new Comparator<SalesOrder>()
				{
			@Override
			public int compare(SalesOrder e1,SalesOrder e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getCustomerFullName()!=null && e2.getCustomerFullName()!=null){
						return e1.getCustomerFullName().compareTo(e2.getCustomerFullName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);

	}
	protected void addColumngetCustomerFullName()
	{
		getCustomerFullNameColumn=new TextColumn<SalesOrder>()
				{
			@Override
			public String getValue(SalesOrder object)
			{
				return object.getCustomerFullName()+"";
			}
				};
				table.addColumn(getCustomerFullNameColumn,"Customer Name");
				table.setColumnWidth(getCustomerFullNameColumn,150,Unit.PX);
				getCustomerFullNameColumn.setSortable(true);
	}
	protected void addSortinggetCustomerCellNumber()
	{
		List<SalesOrder> list=getDataprovider().getList();
		columnSort=new ListHandler<SalesOrder>(list);
		columnSort.setComparator(getCustomerCellNumberColumn, new Comparator<SalesOrder>()
				{
			@Override
			public int compare(SalesOrder e1,SalesOrder e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCustomerCellNumber()== e2.getCustomerCellNumber()){
						return 0;}
					if(e1.getCustomerCellNumber()> e2.getCustomerCellNumber()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetCustomerCellNumber()
	{
		getCustomerCellNumberColumn=new TextColumn<SalesOrder>()
				{
			@Override
			public String getValue(SalesOrder object)
			{
				return object.getCustomerCellNumber()+"";
			}
				};
				table.addColumn(getCustomerCellNumberColumn,"Customer Cell");
				table.setColumnWidth(getCustomerCellNumberColumn,150,Unit.PX);
				getCustomerCellNumberColumn.setSortable(true);
	}
	protected void addSortinggetSalesOrderCount()
	{
		List<SalesOrder> list=getDataprovider().getList();
		columnSort=new ListHandler<SalesOrder>(list);
		columnSort.setComparator(getSalesOrderCountColumn, new Comparator<SalesOrder>()
				{
			@Override
			public int compare(SalesOrder e1,SalesOrder e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getContractCount()== e2.getContractCount()){
						return 0;}
					if(e1.getContractCount()> e2.getContractCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetSalesOrderCount()
	{
		getSalesOrderCountColumn=new TextColumn<SalesOrder>()
				{
			@Override
			public String getValue(SalesOrder object)
			{
				if( object.getContractCount()==-1)
					return "N.A";
				else return object.getContractCount()+"";
			}
				};
				table.addColumn(getSalesOrderCountColumn,"SalesOrder Id");
				table.setColumnWidth(getSalesOrderCountColumn,150,Unit.PX);

				getSalesOrderCountColumn.setSortable(true);
	}
	
}
