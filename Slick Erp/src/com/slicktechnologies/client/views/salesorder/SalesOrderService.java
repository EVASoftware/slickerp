package com.slicktechnologies.client.views.salesorder;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.slicktechnologies.shared.common.contractcancel.CancelContract;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;

@RemoteServiceRelativePath("salesorderservice")
public interface SalesOrderService extends RemoteService {
	public void changeStatus(SalesOrder salesorder);

	/**
	 * date 01/11/2017 added by komal for contract cancel (new)
	 */
	ArrayList<CancelContract> getRecordsForSalesOrderCancel(Long companyId,int orderId , String customerName);

	void saveSalesOrderCancelData(SalesOrder salesOrder, String remark,String loginUser, ArrayList<CancelContract> cancelContractList);
	/**
	 * end komal
	 */
}
