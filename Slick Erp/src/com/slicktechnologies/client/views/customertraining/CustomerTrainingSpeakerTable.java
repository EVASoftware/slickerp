package com.slicktechnologies.client.views.customertraining;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;

public class CustomerTrainingSpeakerTable extends SuperTable<CustomerTrainingSpeaker>{
	
//	TextColumn<CustomerTrainingSpeaker> getColumnSrNo;
	TextColumn<CustomerTrainingSpeaker> getColumnSpeakerName;
	TextColumn<CustomerTrainingSpeaker> getColumnSpeakerCellno;
	TextColumn<CustomerTrainingSpeaker> getColumnSpeakerEmail;
	TextColumn<CustomerTrainingSpeaker> getColumnSpeakerTitle;
	private Column<CustomerTrainingSpeaker,String>Delete;

	public CustomerTrainingSpeakerTable() {
		super();
//		setHeight("300px");
	}
	
	
	@Override
	public void createTable() {
		
//		addColumnSrNo();
		addColumnSpeakerName();
		addColumnSpeakerTitle();
		addColumnSpeakerCellno();
		addColumnSpeakerEmail();
		addColumnDelete();
		setFieldUpdaterOnDelete();
	}

	private void addColumnDelete() {
		ButtonCell btnCell= new ButtonCell();
		Delete = new Column<CustomerTrainingSpeaker, String>(btnCell) {

			@Override
			public String getValue(CustomerTrainingSpeaker object) {

				return "Delete";
			}
		};
		table.addColumn(Delete,"Delete");
	}
	
	private void setFieldUpdaterOnDelete() {
		Delete.setFieldUpdater(new FieldUpdater<CustomerTrainingSpeaker, String>() {
			@Override
			public void update(int index, CustomerTrainingSpeaker object, String value) {
				getDataprovider().getList().remove(index);
			}
		});
	}
	
	
	
	
			private void addColumnSpeakerTitle() {
				
				getColumnSpeakerTitle = new TextColumn<CustomerTrainingSpeaker>() {
					@Override
					public String getValue(CustomerTrainingSpeaker object) {
						if (object.getSpeakerTitle()!=null){
							return object.getSpeakerTitle();
						}
						return null;
					}
				};
				table.addColumn(getColumnSpeakerTitle, "Title");
			}


			private void addColumnSpeakerEmail() {
				
				getColumnSpeakerEmail = new TextColumn<CustomerTrainingSpeaker>() {
					@Override
					public String getValue(CustomerTrainingSpeaker object) {
						if (object.getSpeakerEmail()!=null){
							return object.getSpeakerEmail();
						}
						return null;
					}
				};
				table.addColumn(getColumnSpeakerEmail, "Email");
			}


			private void addColumnSpeakerCellno() {
				
				getColumnSpeakerCellno = new TextColumn<CustomerTrainingSpeaker>() {
					@Override
					public String getValue(CustomerTrainingSpeaker object) {
						if (object.getSpeakercellno()!=null){
							return object.getSpeakercellno()+"";
						}
						return null;
					}
				};
				table.addColumn(getColumnSpeakerCellno, "Contact Number");
			}


			private void addColumnSpeakerName() {
		
				getColumnSpeakerName = new TextColumn<CustomerTrainingSpeaker>() {
					@Override
					public String getValue(CustomerTrainingSpeaker object) {
						if (object.getSpeakerName()!=null){
							return object.getSpeakerName();
						}
						return null;
					}
				};
				table.addColumn(getColumnSpeakerName, "Speaker Name");
			}


//			private void addColumnSrNo() {
//				
//				getColumnSrNo = new TextColumn<CustomerTrainingSpeaker>() {
//					@Override
//					public String getValue(CustomerTrainingSpeaker object) {
//						if (object.getSrNo()!=0){
//							return object.getSrNo()+"";
//						}
//						return null;
//					}
//				};
//				table.addColumn(getColumnSrNo, "Sr No");
//				
//			}
		
		

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		int tablecolcount = this.table.getColumnCount();
		for (int i = tablecolcount - 1; i > -1; i--){
			table.removeColumn(i);
		}
		if (state == true){
			addColumnSpeakerName();
			addColumnSpeakerTitle();
			addColumnSpeakerCellno();
			addColumnSpeakerEmail();
			addColumnDelete();
			setFieldUpdaterOnDelete();
		}
		if (state == false){
			addColumnSpeakerName();
			addColumnSpeakerTitle();
			addColumnSpeakerCellno();
			addColumnSpeakerEmail();
		}
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}



}
