package com.slicktechnologies.client.views.customertraining;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.common.CustomerTrainingDetails;

public class CustomerTrainingTableProxy extends SuperTable<CustomerTrainingDetails>{
	
	TextColumn<CustomerTrainingDetails>getIdcolumn;
	TextColumn<CustomerTrainingDetails>getTitlecolumn;
	
	TextColumn<CustomerTrainingDetails>getStatuscolumn;
	TextColumn<CustomerTrainingDetails>getsalesPersoncolumn;
	TextColumn<CustomerTrainingDetails>getApproverNamecolumn;
	TextColumn<CustomerTrainingDetails>getPersonResponsiablecolumn;
	

	@Override
	public void createTable() {
		
		addColumngetId();
		addColumngetitle();
		addColumngetStatus();
		addColumngetitleSalesperson();
		addColumngeApproverName();
		addColumngePersonResponsiable();
	}
	
	
	
	
	

		private void addColumngePersonResponsiable() {
			getPersonResponsiablecolumn=new TextColumn<CustomerTrainingDetails>() {
				public String getValue(CustomerTrainingDetails object)
				{
					return object.getPocName();
				}
			};
			table.addColumn(getPersonResponsiablecolumn,"Person Responsiable");
			getPersonResponsiablecolumn.setSortable(true);
		}

		private void addColumngeApproverName() {
			
			getApproverNamecolumn=new TextColumn<CustomerTrainingDetails>() {
				public String getValue(CustomerTrainingDetails object)
				{
					return object.getApproverName();
				}
			};
			table.addColumn(getApproverNamecolumn,"Approver");
			getApproverNamecolumn.setSortable(true);
		}
	
		private void addColumngetitleSalesperson() {
			
			getsalesPersoncolumn=new TextColumn<CustomerTrainingDetails>() {
				public String getValue(CustomerTrainingDetails object)
				{
					return object.getEmployee();
				}
			};
			table.addColumn(getsalesPersoncolumn,"Sales Person");
			getsalesPersoncolumn.setSortable(true);
		}
	
		private void addColumngetStatus() {
			
			getStatuscolumn=new TextColumn<CustomerTrainingDetails>() {
				public String getValue(CustomerTrainingDetails object)
				{
					return object.getStatus();
				}
			};
			table.addColumn(getStatuscolumn,"Status");
			getStatuscolumn.setSortable(true);
		}

	private void addColumngetId() {
		
		getIdcolumn=new TextColumn<CustomerTrainingDetails>() {
			public String getValue(CustomerTrainingDetails object)
			{
				return object.getCount()+"";
			}
		};
		table.addColumn(getIdcolumn,"Id" );
		getIdcolumn.setSortable(true);
		
	}

	private void addColumngetitle() {
		
		getTitlecolumn=new TextColumn<CustomerTrainingDetails>() {
			public String getValue(CustomerTrainingDetails object)
			{
				return object.getTitle();
			}
		};
		table.addColumn(getTitlecolumn,"Title");
		getTitlecolumn.setSortable(true);
		
	}

	
	public void addColumnSorting() {
			
			addColumnIDSorting();
			addColumnTitleSorting();
			addColumStatusSorting();
			addColumnApproverNameSorting();
			addColumnSalesPersonSorting();
			addColumnPersonResponsibleSorting();
	}
	
	
					private void addColumnPersonResponsibleSorting(){
						
						List<CustomerTrainingDetails> list=getDataprovider().getList();
						columnSort=new ListHandler<CustomerTrainingDetails>(list);
						columnSort.setComparator(getPersonResponsiablecolumn, new Comparator<CustomerTrainingDetails>()
								{
							@Override
							public int compare(CustomerTrainingDetails e1,CustomerTrainingDetails e2)
							{
								if(e1!=null && e2!=null)
								{
									if( e1.getPocName()!=null && e2.getPocName()!=null){
										return e1.getPocName().compareTo(e2.getPocName());}
								}
								else{
									return 0;}
								return 0;
							}
								});
						table.addColumnSortHandler(columnSort);
					}
				
				
				
				
				
				
					private void addColumnSalesPersonSorting() {
						
						List<CustomerTrainingDetails> list=getDataprovider().getList();
						columnSort=new ListHandler<CustomerTrainingDetails>(list);
						columnSort.setComparator(getsalesPersoncolumn, new Comparator<CustomerTrainingDetails>()
								{
							@Override
							public int compare(CustomerTrainingDetails e1,CustomerTrainingDetails e2)
							{
								if(e1!=null && e2!=null)
								{
									if( e1.getEmployee()!=null && e2.getEmployee()!=null){
										return e1.getEmployee().compareTo(e2.getEmployee());}
								}
								else{
									return 0;}
								return 0;
							}
								});
						table.addColumnSortHandler(columnSort);
						
					}
				
				
				
				
				
				
					private void addColumnApproverNameSorting(){
						
						List<CustomerTrainingDetails> list=getDataprovider().getList();
						columnSort=new ListHandler<CustomerTrainingDetails>(list);
						columnSort.setComparator(getApproverNamecolumn, new Comparator<CustomerTrainingDetails>()
								{
							@Override
							public int compare(CustomerTrainingDetails e1,CustomerTrainingDetails e2)
							{
								if(e1!=null && e2!=null)
								{
									if( e1.getApproverName()!=null && e2.getApproverName()!=null){
										return e1.getApproverName().compareTo(e2.getApproverName());}
								}
								else{
									return 0;}
								return 0;
							}
								});
						table.addColumnSortHandler(columnSort);
						
					}
				
				
				
				
				
				
					private void addColumStatusSorting() {
						
						List<CustomerTrainingDetails> list=getDataprovider().getList();
						columnSort=new ListHandler<CustomerTrainingDetails>(list);
						columnSort.setComparator(getStatuscolumn, new Comparator<CustomerTrainingDetails>()
								{
							@Override
							public int compare(CustomerTrainingDetails e1,CustomerTrainingDetails e2)
							{
								if(e1!=null && e2!=null)
								{
									if( e1.getStatus()!=null && e2.getStatus()!=null){
										return e1.getStatus().compareTo(e2.getStatus());}
								}
								else{
									return 0;}
								return 0;
							}
								});
						table.addColumnSortHandler(columnSort);
						

						
						
						
					}
				
				
				
				
				
				
					private void addColumnTitleSorting() {
						
						List<CustomerTrainingDetails> list=getDataprovider().getList();
						columnSort=new ListHandler<CustomerTrainingDetails>(list);
						columnSort.setComparator(getTitlecolumn, new Comparator<CustomerTrainingDetails>()
								{
							@Override
							public int compare(CustomerTrainingDetails e1,CustomerTrainingDetails e2)
							{
								if(e1!=null && e2!=null)
								{
									if( e1.getTitle()!=null && e2.getTitle()!=null){
										return e1.getTitle().compareTo(e2.getTitle());}
								}
								else{
									return 0;}
								return 0;
							}
								});
						table.addColumnSortHandler(columnSort);
						
					}
				
				
				
				
				
				
					private void addColumnIDSorting() {
						
						List<CustomerTrainingDetails> list=getDataprovider().getList();
						columnSort=new ListHandler<CustomerTrainingDetails>(list);
						columnSort.setComparator(getIdcolumn, new Comparator<CustomerTrainingDetails>()
								{
							@Override
							public int compare(CustomerTrainingDetails e1,CustomerTrainingDetails e2)
							{
								if(e1!=null && e2!=null)
								{
									if(e1.getCount()== e2.getCount()){
										return 0;}
									if(e1.getCount()> e2.getCount()){
										return 1;}
									else{
										return -1;}
								}
								else{
									return 0;}
							}
								});
						table.addColumnSortHandler(columnSort);
						
					}






	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
	
	

}
