package com.slicktechnologies.client.views.customertraining;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineLabel;

public class SendInvitationPopupup extends AbsolutePanel {

	CheckBox sendSMS,sendEmail;
	Button btnCancel,btnSend;
	
	
	public SendInvitationPopupup() {
		
		btnCancel= new Button("Cancel");
		btnSend= new Button("Send Invitation");
		
		InlineLabel remark1 = new InlineLabel("Sending Options");
		add(remark1,140,10);
		remark1.getElement().getStyle().setFontSize(20, Unit.PX);
		
		
		InlineLabel processType = new InlineLabel("Send SMS");
		add(processType,10,50);
		sendSMS= new CheckBox();
		add(sendSMS,10,70);
		
		sendSMS.setSize("100px", "18px");
		
		InlineLabel processId = new InlineLabel("Send Email");
		add(processId,150,50);
		sendEmail= new CheckBox();
		add(sendEmail,150,70);
		
		add(btnSend,100,100);
		add(btnCancel,250,100);
		setSize("400px", "250px");
		this.getElement().setId("form");
	}

	
	//****************************getters and setters **********************************
	
	
	public CheckBox getSendSMS() {
		return sendSMS;
	}


	public void setSendSMS(CheckBox sendSMS) {
		this.sendSMS = sendSMS;
	}


	public CheckBox getSendEmail() {
		return sendEmail;
	}


	public void setSendEmail(CheckBox sendEmail) {
		this.sendEmail = sendEmail;
	}


	public Button getBtnCancel() {
		return btnCancel;
	}


	public void setBtnCancel(Button btnCancel) {
		this.btnCancel = btnCancel;
	}


	public Button getBtnSend() {
		return btnSend;
	}


	public void setBtnSend(Button btnSend) {
		this.btnSend = btnSend;
	}
	
}
