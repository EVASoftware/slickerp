package com.slicktechnologies.client.views.customertraining;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;

public class CustomerTrainingAttendiesTable extends SuperTable<CustomerTrainingAttendies> {
	

	
//	TextColumn<CustomerTrainingAttendies> getColumnSrNo;
	TextColumn<CustomerTrainingAttendies> getColumnCustId;
	TextColumn<CustomerTrainingAttendies> getColumnCustFirstName;
	TextColumn<CustomerTrainingAttendies> getColumnCustLastName;
	TextColumn<CustomerTrainingAttendies> getColumnCustCellno;
	TextColumn<CustomerTrainingAttendies> getColumnCustType;
	TextColumn<CustomerTrainingAttendies> getColumnCustCategory;
	TextColumn<CustomerTrainingAttendies> getColumnCustGroup;
	TextColumn<CustomerTrainingAttendies> getColumnCustpriority;
	TextColumn<CustomerTrainingAttendies> getColumnCustEmail;
	private Column<CustomerTrainingAttendies,String>Delete;
	

	public CustomerTrainingAttendiesTable() {
		super();
//		setHeight("300px");
	}
	
	
	@Override
	public void createTable() {
		
//		addColumnSrNo();
		addColumnCustId();
		addColumnCustFirstName();
		addColumnCustLastName();
		addColumnCustCellno();
		addColumnCustEmail();
		addColumnCustType();
		addColumnCustCategory();
		addColumnCustGroup();
		addColumnCustPriority();
		addColumnDelete();
		setFieldUpdaterOnDelete();
		
		
		
	}
	
	
			private void addColumnDelete() {
				ButtonCell btnCell= new ButtonCell();
				Delete = new Column<CustomerTrainingAttendies, String>(btnCell) {
		
					@Override
					public String getValue(CustomerTrainingAttendies object) {
		
						return "Delete";
					}
				};
				table.addColumn(Delete,"Delete");
			}

			


			private void setFieldUpdaterOnDelete() {
				Delete.setFieldUpdater(new FieldUpdater<CustomerTrainingAttendies, String>() {
					@Override
					public void update(int index, CustomerTrainingAttendies object, String value) {
						getDataprovider().getList().remove(index);
					}
				});
			}
	
	
			private void addColumnCustPriority() {
				
				getColumnCustpriority = new TextColumn<CustomerTrainingAttendies>() {
					@Override
					public String getValue(CustomerTrainingAttendies object) {
						if (object.getPriority()!=null){
							return object.getPriority();
						}
						return null;
					}
				};
				table.addColumn(getColumnCustpriority, "Priority");
		
			}


			private void addColumnCustGroup() {
				
				getColumnCustGroup = new TextColumn<CustomerTrainingAttendies>() {
					@Override
					public String getValue(CustomerTrainingAttendies object) {
						if (object.getGroup()!=null){
							return object.getGroup();
						}
						return null;
					}
				};
				table.addColumn(getColumnCustGroup, "Group");
		
			}


			private void addColumnCustCategory() {
				
				getColumnCustCategory = new TextColumn<CustomerTrainingAttendies>() {
					@Override
					public String getValue(CustomerTrainingAttendies object) {
						if (object.getCategory()!=null){
							return object.getCategory();
						}
						return null;
					}
				};
				table.addColumn(getColumnCustCategory, "Category");
		
			}


			private void addColumnCustType() {
				
				getColumnCustType = new TextColumn<CustomerTrainingAttendies>() {
					@Override
					public String getValue(CustomerTrainingAttendies object) {
						if (object.getType()!=null){
							return object.getType();
						}
						return null;
					}
				};
				table.addColumn(getColumnCustType, "Type");
		
			}


			private void addColumnCustEmail() {
				
				getColumnCustEmail = new TextColumn<CustomerTrainingAttendies>() {
					@Override
					public String getValue(CustomerTrainingAttendies object) {
						if (object.getCustEmail()!=null){
							return object.getCustEmail();
						}
						return null;
					}
				};
				table.addColumn(getColumnCustEmail, "Email");
		
			}


			private void addColumnCustCellno() {
				
				getColumnCustCellno = new TextColumn<CustomerTrainingAttendies>() {
					@Override
					public String getValue(CustomerTrainingAttendies object) {
						if (object.getCustCell()!=null){
							return object.getCustCell();
						}
						return null;
					}
				};
				table.addColumn(getColumnCustCellno, "CellNo");
		
			}


			private void addColumnCustFirstName() {
				
				getColumnCustFirstName = new TextColumn<CustomerTrainingAttendies>() {
					@Override
					public String getValue(CustomerTrainingAttendies object) {
						if (object.getCustFirstName()!=null){
							return object.getCustFirstName();
						}
						return null;
					}
				};
				table.addColumn(getColumnCustFirstName, "First Name");
		
			}
			
			private void addColumnCustLastName() {
				
				getColumnCustLastName = new TextColumn<CustomerTrainingAttendies>() {
					@Override
					public String getValue(CustomerTrainingAttendies object) {
						if (object.getCustLastName()!=null){
							return object.getCustLastName();
						}
						return null;
					}
				};
				table.addColumn(getColumnCustLastName, "Last Name");
		
			}


			private void addColumnCustId() {
		
				getColumnCustId = new TextColumn<CustomerTrainingAttendies>() {
					@Override
					public String getValue(CustomerTrainingAttendies object) {
						if (object.getCustId()!=0){
							return object.getCustId()+"";
						}
						return null;
					}
				};
				table.addColumn(getColumnCustId, "Id");
			}


//			private void addColumnSrNo() {
//				
//				getColumnSrNo = new TextColumn<CustomerTrainingAttendies>() {
//					@Override
//					public String getValue(CustomerTrainingAttendies object) {
//						if (object.getSrNo()!=0){
//							return object.getSrNo()+"";
//						}
//						return null;
//					}
//				};
//				table.addColumn(getColumnSrNo, "Sr No");
//				
//			}
		
		

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		int tablecolcount = this.table.getColumnCount();
		for (int i = tablecolcount - 1; i > -1; i--){
			table.removeColumn(i);
		}
		if (state == true){
			addColumnCustId();
			addColumnCustFirstName();
			addColumnCustLastName();
			addColumnCustCellno();
			addColumnCustEmail();
			addColumnCustType();
			addColumnCustCategory();
			addColumnCustGroup();
			addColumnCustPriority();
			addColumnDelete();
			setFieldUpdaterOnDelete();
		}
		if (state == false){
			addColumnCustId();
			addColumnCustFirstName();
			addColumnCustLastName();
			addColumnCustCellno();
			addColumnCustEmail();
			addColumnCustType();
			addColumnCustCategory();
			addColumnCustGroup();
			addColumnCustPriority();
		}
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}





}
