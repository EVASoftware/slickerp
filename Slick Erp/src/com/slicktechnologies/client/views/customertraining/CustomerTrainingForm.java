package com.slicktechnologies.client.views.customertraining;


import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.composite.UploadComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.approvalutility.ApprovableFormScreen;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.CustomerTrainingDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
//import com.slicktechnologies.shared.common.helperlayer.SmsConfig;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class CustomerTrainingForm extends ApprovableFormScreen<CustomerTrainingDetails> implements ChangeHandler{

	TextBox custTrainingId,title,speaker,speakerCell,speakerTitle,speakerEmailID,custFirstName,custLastName,custcellNo,custemailID;
	DateBox fromDate;
	TextBox pocCellno,pocEmail,from_time,status;
	Label reflable;
	TextArea description,address;
	
	CheckBox sendSMS,sendEmail,eventmanagement;
	
	ObjectListBox<Type> olbCustomerType;
	ObjectListBox<ConfigCategory> olbCustomerCategory;
	ObjectListBox<Config> olbCustomerGroup,olbCustomerPriBox;
	
//	SendInvitationPopupup popup= new SendInvitationPopupup();
//	PopupPanel panel ;
	
//	final GenricServiceAsync async=GWT.create(GenricService.class);
	
	/** The olb approver name. */
	ObjectListBox<Employee> olbeSalesPerson,olbApproverName,pocName;
	
	ObjectListBox<SmsTemplate> smsEventList;
	
	
	Button addSpeakerbutton,addAttendiesbyComposite,addAttendiesbutton,addCustByCatergory,
	addCustByType,addCustByGroup,addCustByPriority;
	PersonInfoComposite personInfoComposite;
	CustomerTrainingSpeakerTable speakertable;
	CustomerTrainingAttendiesTable attendiestable;
	
	//**************i have to make it live later
//	ListBox smsEvents;
	TextArea smsTemplate;
	Button addTemplate,clearTable;
	
	//******vijay on 5/9 ***********
	UploadComposite uploadexcel ;
	Button addAttendiesbyexcelbutton;
	//************************
	
	GenricServiceAsync service=(GenricServiceAsync)GWT.create(GenricService.class); 
	
	public CustomerTrainingForm(String[] processlevel, FormField[][] fields,
			FormStyle formstyle) 
	{
		super(processlevel, fields, formstyle);
		createGui();
		
//		this.getAddAttendiesbutton().addClickHandler(this);
//		this.getAddSpeakerbutton().addClickHandler(this);
//		
//		
//		this.getOlbCustomerCategory().addClickHandler(this);
//		this.getOlbCustomerType().addClickHandler(this);
//		this.getAddCustByGroup().addClickHandler(this);
//		this.getAddCustByPriority().addClickHandler(this);
//		this.getStatus().setValue(CustomerTrainingDetails.CREATED);
		
//		this.getSendEmail().addClickHandler(this);
//		this.getSendSMS().addClickHandler(this);
		
//		this.getSmsEventList().addChangeHandler(this);
//		this.getPocName().addChangeHandler(this);
		
		
//		popup.getBtnSend().addClickHandler(this);
//		popup.getBtnCancel().addClickHandler(this);
	}


	
	public CustomerTrainingForm() {
			super();
	       createGui();
	       
	       this.getStatus().setValue(CustomerTrainingDetails.CREATED);
	       
//	       popup.getBtnSend().addClickHandler(this);
//			popup.getBtnCancel().addClickHandler(this);
	}
	
	@SuppressWarnings("unused")
	private void initalizeWidget()
	{
		
		custTrainingId= new TextBox();
		custTrainingId.setEnabled(false);
		title= new TextBox();
//		fromDate= new DateBox();
		fromDate = new DateBoxWithYearSelector();
		fromDate.setEnabled(false);
//		toDate= new DateBox();
		speaker= new TextBox();
		speaker.setEnabled(false);
		speakerCell= new TextBox();
		speakerCell.setEnabled(false);
		speakerEmailID = new TextBox();
		speakerEmailID.setEnabled(false);
		speakerTitle = new TextBox();
		speakerTitle.setEnabled(false);
		from_time= new TextBox();
		from_time.setEnabled(false);
//		to_time= new TextBox();
		status= new TextBox();
		status.setEnabled(false);
//		this.status.setText(CustomerTrainingDetails.CREATED);
		
		//*****vijay
		uploadexcel = new UploadComposite();
		addAttendiesbyexcelbutton = new Button("Add");  // for excel
		
		addSpeakerbutton= new Button("Add");
		addSpeakerbutton.setEnabled(false);
//		addSpeakerbutton.addClickHandler(this);
		
		addAttendiesbutton= new Button("Add");
//		addAttendiesbutton.addClickHandler(this);
		
		addAttendiesbyComposite= new Button("Add");
//		addAttendiesbyComposite.addClickHandler(this);
	
		pocCellno= new TextBox();
		pocCellno.setEnabled(false);
		pocEmail= new TextBox();
		pocEmail.setEnabled(false);
		
		custFirstName= new TextBox();
		custLastName= new TextBox();
		custcellNo= new TextBox();
		custemailID= new TextBox();
		description= new TextArea();
		description.setEnabled(false);
		address= new TextArea();
		address.setEnabled(false);
		
		
		sendSMS = new CheckBox();
		sendSMS.setValue(false);
//		sendSMS.addClickHandler(this);
		sendEmail= new CheckBox();
		sendEmail.setValue(false);
//		sendEmail.addClickHandler(this);
		eventmanagement = new CheckBox();
		eventmanagement.setValue(false);
//		eventmanagement.addClickHandler(this);
		
		
		
		addCustByCatergory= new Button("Add Cust by Category");
		olbCustomerCategory= new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(olbCustomerCategory, Screen.CUSTOMERCATEGORY);
		
		olbCustomerCategory.addChangeHandler(this);

		addCustByType= new Button("Add Cust by Type");
		olbCustomerType=new ObjectListBox<Type>();
		AppUtility.makeTypeListBoxLive(olbCustomerType, Screen.CUSTOMERTYPE);
		
		addCustByGroup = new Button("Add Cust by Group");
		olbCustomerGroup=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbCustomerGroup, Screen.CUSTOMERGROUP);
		
		addCustByPriority = new Button("Add Cust by Priority");
		olbCustomerPriBox=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbCustomerPriBox, Screen.CUSTOMERPRIORITY);
		
		
		olbeSalesPerson=new ObjectListBox<Employee>();
//		AppUtility.makeSalesPersonListBoxLive(olbeSalesPerson);
		olbeSalesPerson.makeEmployeeLive(LoginPresenter.currentModule, AppConstants.CAMPAIGNMANAGEMENT, "Sales Person");
		
		olbApproverName=new ObjectListBox<Employee>();
		AppUtility.makeSalesPersonListBoxLive(olbApproverName);
		
		pocName=new ObjectListBox<Employee>();
//		AppUtility.makeSalesPersonListBoxLive(pocName);
		pocName.makeEmployeeLive(LoginPresenter.currentModule, AppConstants.CAMPAIGNMANAGEMENT, "Person Responsible");
		
		reflable = new Label();
		reflable.getElement().getStyle().setFontSize(10, Unit.PX);
		
		smsEventList = new ObjectListBox<SmsTemplate>();
		MyQuerry querry = new MyQuerry(new Vector<Filter>(),new SmsTemplate());
		smsEventList.MakeLive(querry);
		smsEventList.setEnabled(false);
		
		MyQuerry custquerry = new MyQuerry();
		Filter custfilteer = new Filter();
		custfilteer.setQuerryString("status");
		custfilteer.setBooleanvalue(true);
		custquerry.setQuerryObject(new Customer());
		personInfoComposite = new PersonInfoComposite(custquerry, false);
		
		
		
		smsTemplate= new TextArea();
		addTemplate= new Button("Add Template");
		addTemplate.setEnabled(false);
		smsTemplate.setEnabled(false);
		
		
		
		clearTable = new Button("Clear Table");
		speakertable = new CustomerTrainingSpeakerTable();
		attendiestable = new CustomerTrainingAttendiesTable();
		
	}

	
	@Override
	public void createScreen() {
		
		initalizeWidget();
		
		this.processlevelBarNames = new String[] {ManageApprovals.APPROVALREQUEST,ManageApprovals.CANCELAPPROVALREQUEST,"Send SMS","Send Email","Mark Executed"};
		/////////////////////////////////// Form Field Initialization////////////////////////////////////////////////////
		//Token to initialize formfield speaker
		FormFieldBuilder fbuilder = null;
		
		fbuilder = new FormFieldBuilder();
		FormField ffeedbackinfo=fbuilder.setlabel("Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		
		fbuilder = new FormFieldBuilder("ID",custTrainingId);
		FormField ffeedbackId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("* Title",title);
		FormField fTitle= fbuilder.setMandatory(true).setMandatoryMsg("This is mandatory").setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("Status",status);
		FormField fstatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("* Approver Name",olbApproverName);
		FormField folbApproverName= fbuilder.setMandatory(true).setMandatoryMsg("Approver Name is mandatory!").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Sales Person",olbeSalesPerson);
		FormField folbeSalesPerson= fbuilder.setMandatory(true).setMandatoryMsg("Sales Person is mandatory!").setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder();
		FormField fspeakerdetails=fbuilder.setlabel("Speaker Details ( Incase Conference / Seminar type campaign)").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		
		fbuilder = new FormFieldBuilder("Date ",fromDate);
		FormField ffromDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
//		fbuilder = new FormFieldBuilder("To Date",toDate);
//		FormField ftoDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("Time ",from_time);
		FormField ffrom_time= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
//		fbuilder = new FormFieldBuilder("To Time",to_time);
//		FormField fto_time= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("Speaker Name",speaker);
		FormField fspeaker= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("Cell Number",speakerCell);
		FormField fspeakerCell= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("Email",speakerEmailID);
		FormField fspeakerEmailID= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("Title",speakerTitle);
		FormField fspeakerTitle= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder(" ",addSpeakerbutton);
		FormField faddSpeakerbutton= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder(" ",speakertable.getTable());
		FormField fspeakertable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(5).build();
		
		FormField fgroupingAttendiesInformation=fbuilder.setlabel("Target Customers ").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		fbuilder = new FormFieldBuilder("",personInfoComposite);
		FormField fpersonInfoComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(5).build();
		
		fbuilder = new FormFieldBuilder("First Name",custFirstName);
		FormField fcustFirstName= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Last Name",custLastName);
		FormField fcustLastName= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Customer CellNo",custcellNo);
		FormField fcustcellNo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Customer Email",custemailID);
		FormField fcustemailID= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder(" ",addAttendiesbutton);
		FormField faddAttendiesbutton= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder(" ",addAttendiesbyComposite);
		FormField faddAttendiesbyComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder(" ",attendiestable.getTable() );
		FormField fattendiestable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(5).build();
		
//		FormField fgroupingPocInformation=fbuilder.setlabel("Point of Contact Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		
		fbuilder = new FormFieldBuilder("Person Responsible",pocName);
		FormField fpocName= fbuilder.setMandatory(true).setMandatoryMsg("This is mandatory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Contact",pocCellno);
		FormField fpocCellno= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Email",pocEmail);
		FormField fpocEmial= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Description",description);
		FormField fdescription= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(5).build();
		
		fbuilder = new FormFieldBuilder("Venue Address",address);
		FormField faddress= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(5).build();
		
		fbuilder = new FormFieldBuilder("Customer Category",olbCustomerCategory);
	    FormField folbcategory= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	     
	    fbuilder = new FormFieldBuilder(" ",addCustByCatergory);
		FormField faddCustCatergory= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("Customer Type",olbCustomerType);
	    FormField folbCustomerType= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder(" ",addCustByType);
		FormField faddCustType= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("Customer Group",olbCustomerGroup);
	    FormField folbCustomerGroup= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder(" ",addCustByGroup);
		FormField faddCustByGroup= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("Customer Priority",olbCustomerPriBox);
	    FormField folbCustomerPriBox= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder(" ",addCustByPriority);
		FormField faddCustByPriority= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fsmsdetails=fbuilder.setlabel("Sms Template Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		
		fbuilder = new FormFieldBuilder("SMS Template",smsTemplate);
		FormField fsmsTemplate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(5).build();
		
		fbuilder = new FormFieldBuilder("Select SMS Template ",smsEventList);
	    FormField fsmsEventList= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder(" ",addTemplate);
		FormField faddTemplate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder(" ",clearTable);
		FormField fclearTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("Send SMS",sendSMS);
		
		FormField fsendSMS= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Send E-mail",sendEmail);
		FormField fsendEmail= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Event",eventmanagement);
		FormField feventmgmt= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		//***********vijay ************
		fbuilder = new FormFieldBuilder("",uploadexcel);
		FormField fexceluploader = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder(" ",addAttendiesbyexcelbutton);
		FormField faddExcelAttendiesbutton= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		FormField[][] formFields = {  
				{ffeedbackinfo},
				{ffeedbackId,fTitle,fstatus},
				
				//**********fsendSMS,fsendEmail,fevent
				{folbApproverName,folbeSalesPerson,fsendSMS,fsendEmail,feventmgmt},
				{fdescription},
				{fpocName,fpocCellno,fpocEmial},
				{fspeakerdetails},
				{ffromDate,ffrom_time},
				{faddress},
				{fspeaker,fspeakerTitle,fspeakerCell,fspeakerEmailID,faddSpeakerbutton},
				{fspeakertable},
				{fgroupingAttendiesInformation},
				{fpersonInfoComposite},
				{faddAttendiesbyComposite},
				{fcustFirstName,fcustLastName,fcustcellNo,fcustemailID,faddAttendiesbutton},
				{folbcategory,faddCustCatergory,folbCustomerType,faddCustType},
				{folbCustomerGroup,faddCustByGroup,folbCustomerPriBox,faddCustByPriority,fclearTable},
				{fexceluploader,faddExcelAttendiesbutton},
				{fattendiestable},
				{fsmsdetails},
				{fsmsEventList,faddTemplate},
				{fsmsTemplate}
				
				
				
		};
		this.fields=formFields;
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public void updateModel(CustomerTrainingDetails model) {
		
		
		if(this.eventmanagement.getValue()!=null){
			model.setEventManagement(this.eventmanagement.getValue());
		}
		
		if(this.sendEmail.getValue()!=null){
			model.setSendEmail(this.getSendEmail().getValue());
		}
		
		if(this.sendSMS.getValue()!=null){
			model.setSendSMS(this.getSendSMS().getValue());
		}
		
		
		if(this.address.getValue()!=null)
			model.setAddress(this.address.getValue());
		
		if(this.title.getValue()!=null)
			model.setTitle(this.title.getValue());
		
		if(this.getStatus().getValue()!=null)
			model.setStatus(this.getStatus().getValue());
		
		if(this.getPocName().getValue()!=null){
			model.setPocName(this.getPocName().getValue());
		}
		if(this.getPocCellno().getValue()!=null)
			model.setPocContact(this.getPocCellno().getValue());
		
		if(this.getPocEmail().getValue()!=null)
			model.setPocEmail(this.getPocEmail().getValue());
		
		if(this.getFromDate().getValue()!=null)
			model.setFrom_dt(this.getFromDate().getValue());
		
//		if(this.getToDate().getValue()!=null)
//			model.setTo_dt(this.getToDate().getValue());
		
		if(this.getFrom_time().getValue()!=null)
			model.setFrom_time(this.getFrom_time().getValue());
		
//		if(this.getTo_time().getValue()!=null)
//			model.setTo_time(this.getTo_time().getValue());
		
		if(!this.getDescription().getValue().equals(""))
			model.setDescription(this.getDescription().getValue());
		
		if(!this.getSmsTemplate().getValue().equals(""))
			model.setSmsTemplate(this.getSmsTemplate().getValue());
		
		if(!this.getOlbApproverName().getValue().trim().equals(""))
			model.setApproverName(this.getOlbApproverName().getValue().trim());
		
		if(!this.getOlbeSalesPerson().getValue().trim().equals(""))
			model.setEmployee(this.getOlbeSalesPerson().getValue().trim());
		
		
		List<CustomerTrainingAttendies> custList=this.getAttendiestable().getDataprovider().getList();
		ArrayList<CustomerTrainingAttendies> attendieslist=new ArrayList<CustomerTrainingAttendies>();
		attendieslist.addAll(custList);
		model.setAttendiesLis(attendieslist);
		
		
		List<CustomerTrainingSpeaker> speakerList=this.getSpeakertable().getDataprovider().getList();
		ArrayList<CustomerTrainingSpeaker> guestList=new ArrayList<CustomerTrainingSpeaker>();
		guestList.addAll(speakerList);
		model.setSpreakersLis(guestList);
		
		presenter.setModel(model);

	}


	public static CustomerTrainingForm initalize()
	{
				CustomerTrainingForm form=new  CustomerTrainingForm();
				CustomerTrainingTableProxy gentable=new CustomerTrainingTableProxy();
				gentable.setView(form);
				gentable.applySelectionModle();
				CustomerTrainingSearchPopup.staticSuperTable=gentable;
				CustomerTrainingSearchPopup searchpopup=new CustomerTrainingSearchPopup();
				form.setSearchpopupscreen(searchpopup);
				CustomerTrainingPresenter  presenter=new CustomerTrainingPresenter(form,new CustomerTrainingDetails());
				AppMemory.getAppMemory().stickPnel(form);
				
				
		return form;
	}
	
	
	

	@Override
	public void updateView(CustomerTrainingDetails view) {
		
			this.getEventmanagement().setValue(view.isEventManagement());

		
			this.getSendEmail().setValue(view.isSendEmail());

			this.getSendSMS().setValue(view.isSendSMS());
		
			this.getTitle().setValue(view.getTitle());
		
		
		if(view.getTitle()!=null)
			this.getTitle().setValue(view.getTitle());
		
		if(view.getAddress()!=null)
			this.getAddress().setValue(view.getAddress());
		
		
		if(view.getCount()!=0)
			this.getCustTrainingId().setValue(view.getCount()+"");
		
		if(view.getStatus()!=null)
			this.getStatus().setValue(view.getStatus());
		
		if(view.getPocName()!=null)
			this.getPocName().setValue(view.getPocName());
		
		if(view.getPocContact()!=null)
			this.getPocCellno().setValue(view.getPocContact());
		
		if(view.getPocEmail()!=null)
			this.getPocEmail().setValue(view.getPocEmail());
		
		if(view.getFrom_dt()!=null)
			this.getFromDate().setValue(view.getFrom_dt());
		
//		if(view.getTo_dt()!=null)
//			this.getToDate().setValue(view.getTo_dt());
		
		if(view.getFrom_time()!=null)
			this.getFrom_time().setValue(view.getFrom_time());
		
//		if(view.getTo_time()!=null)
//			this.getTo_time().setValue(view.getTo_time());
		
		if(view.getDescription()!=null)
			this.getDescription().setValue(view.getDescription());
		
		if(view.getSmsTemplate()!=null)
			this.getSmsTemplate().setValue(view.getSmsTemplate());
		
		if(view.getEmployee()!=null)
			this.getOlbeSalesPerson().setValue(view.getEmployee());
		
		if(view.getApproverName()!=null)
			this.getOlbApproverName().setValue(view.getApproverName());
		
		attendiestable.setValue(view.getAttendiesLis());
		speakertable.setValue(view.getSpreakersLis());
		
		/* 
		 * for approval process
		 *  nidhi
		 *  5-07-2017
		 */
		if(presenter != null){
			presenter.setModel(view);
		}
		/*
		 *  end
		 */
		
	}

	
	/**
	 * sets the id textbox with the passed count value. 
	 */
	@Override
	public void setCount(int count)
	{
		custTrainingId.setValue(count+"");
	}
	
	
	//***********************validation for target cust list !=0 **************
	
	
	@Override
	public boolean validate() {
		
		boolean superValidate = super.validate();
		if (superValidate == false)
			return false;
		if(validationForAttendiesTableSize()== false){
			showDialogMessage("Please add atleast one customer....");
			return false;
		}
		
		if(validationForSendingSMS()== false){
			showDialogMessage("SMS template box shuold not be empty....");
			return false;
		}
		
		
		if(validationForSendingEmail()== false){
			showDialogMessage("Description box shuold not be empty....");
			return false;
		}
		
		
		return true;
		
	}


	private boolean validationForSendingEmail() {
		
		System.out.println("this.getSendSMS().getValue()==="+this.getSendEmail().getValue());
		if(this.getSendEmail().getValue()==true){
		if(this.getDescription().getValue().equals("")){
			return false;
		}
		return true;
		}
		else{
		return true;
		}
		
	}



	private boolean validationForSendingSMS() {
		
		System.out.println("this.getSendSMS().getValue()==="+this.getSendSMS().getValue());
		if(this.getSendSMS().getValue()==true){
		if(this.getSmsTemplate().getValue().equals("")){
			return false;
		}
		return true;
		}
		else{
			return true;
		}
	}



	private boolean validationForAttendiesTableSize() {
		
		if(this.getAttendiestable().getDataprovider().getList().size()==0){
			return false;
		}
		else {
			return true;
		}
		
	}



	//*****************************changes ends here **************************
	
	
	


	/* (non-Javadoc)
	 * @see com.slicktechnologies.client.approvalutility.ApprovableScreen#getstatustextbox()
	 */
	@Override
	public TextBox getstatustextbox() {
		return this.status;
	}
	
	
	@Override
	public void setToNewState() {
		// TODO Auto-generated method stub
		super.setToNewState();
	}



	@Override
	public void setToViewState() {
	
		super.setToViewState();
		setMenuAsPerStatus();
		setAppHeaderBarAsPerStatus();
	}
	
	
	@Override
	public void setToEditState() {
	
		super.setToEditState();
		
		System.out.println("send email value"+this.getSendEmail().getValue());
		if(this.getSendEmail().getValue().equals(true)){
			this.description.setEnabled(true);
		}
		
		System.out.println("send sms value"+this.getSendSMS().getValue());
		if(this.getSendSMS().getValue().equals(true)){
			
			
			this.smsTemplate.setEnabled(false);
			this.smsEventList.setEnabled(true);
			this.addTemplate.setEnabled(true);
		}
		this.processLevelBar.setVisibleFalse(false);
		
	}
	
	//******************rohan changes here for the menu as per status ************************ 

	/**
	 * Sets the Application Header Bar as Per Status
	 */

	public void setAppHeaderBarAsPerStatus()
	{
		CustomerTrainingDetails entity=(CustomerTrainingDetails) presenter.getModel();
		String status=entity.getStatus();
		
		if(status.equals(CustomerTrainingDetails.CREATED))
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Discard")||text.contains("Search")
						||text.contains("Edit"))
				{
					menus[k].setVisible(true); 
					System.out.println("Value of text is "+menus[k].getText());
				}
				else
				{
					menus[k].setVisible(false);  
				}
			}
		}

		if(status.equals(CustomerTrainingDetails.APPROVED))
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Discard")||text.contains("Search"))
				{
					menus[k].setVisible(true); 

				}
				else
				{
					menus[k].setVisible(false);  
				}
			}
		}
		
		
		if(status.equals(CustomerTrainingDetails.EXECUTED))
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Discard")||text.contains("Search"))
				{
					menus[k].setVisible(true); 

				}
				else
				{
					menus[k].setVisible(false);  
				}
			}
		}
		
		if(status.equals(CustomerTrainingDetails.REQUESTED))
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Discard")||text.contains("Search"))
				{
					menus[k].setVisible(true); 

				}
				else
				{
					menus[k].setVisible(false);  
				}
			}
		}
		
		if(status.equals(CustomerTrainingDetails.REJECTED))
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Discard")||text.contains("Search")||text.contains("Edit"))
				{
					menus[k].setVisible(true); 

				}
				else
				{
					menus[k].setVisible(false);  
				}
			}
		}
		
	/*	if(status.equals(Contract.CREATED))
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Discard")||text.contains("Search")||text.contains("Save")
						||text.contains("Edit"))
				{
					menus[k].setVisible(true); 

				}
				else
				{
					menus[k].setVisible(false);  
				}
			}
		}
*/


		/*else if(status.equals(Contract.CREATED))
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Discard")||text.contains("Search")
						||text.contains("Print")||text.contains("Save")||text.contains("Edit"))
				{
					menus[k].setVisible(true); 

				}
				else
				{
					menus[k].setVisible(false);  
				}
			}
		}*/
	}
	
	
	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		getstatustextbox().setEnabled(false);
		this.getCustTrainingId().setEnabled(false);
		
		this.getPocCellno().setEnabled(false);
		this.getPocEmail().setEnabled(false);
		
		
		
		this.getDescription().setEnabled(false);
		
		this.getSmsTemplate().setEnabled(false);
		this.getSmsEventList().setEnabled(false);
		this.addTemplate.setEnabled(false);
		speakertable.setEnable(false);
		attendiestable.setEnable(false);
		
		
	}
	
	@Override
	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.contains("Search"))
				{
					menus[k].setVisible(true); 
				}
				else{
					menus[k].setVisible(false);  
				}
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")){
					menus[k].setVisible(true); 
				}
				else{
					menus[k].setVisible(false);
				}
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit")||text.contains("Search")){
					menus[k].setVisible(true); 
				}
				else{
					menus[k].setVisible(false); 
				}
			}
		}
		
		AuthorizationHelper.setAsPerAuthorization(Screen.CUSTOMERTRAINING,LoginPresenter.currentModule.trim());
	}
	
	
	
//	@Override
//	public void onChange(ChangeEvent event) {
//		
//			  if (this.getPocName().getSelectedIndex()==0){
//				  this.getPocCellno().setValue("");
//				  this.getPocEmail().setValue("");
//			  }
//			  else{
//				  getPocDetails();
//			  }
//			  
//			  
//				if(this.getSmsEventList().getSelectedIndex()==0){
//					
//					System.out.println("in side on change ");
//					
//					  this.getSmsTemplate().setValue("");
//				}
//			  
//			  
//	}

	public void getPocDetails() {
		showWaitSymbol();
		Timer timer=new Timer() 
	     {
			@Override
			public void run() 
			{
				MyQuerry querry = new MyQuerry();
				Company c=new Company();
				Vector<Filter> filtervec=new Vector<Filter>();
				Filter filter = null;
				
				filter = new Filter();
				filter.setQuerryString("companyId");
				filter.setLongValue(c.getCompanyId());
				filtervec.add(filter);
				
				filter = new Filter();
				filter.setQuerryString("fullname");
				int index = pocName.getSelectedIndex();
				filter.setStringValue(pocName.getItemText(index));
				filtervec.add(filter);
				querry.setFilters(filtervec);
				querry.setQuerryObject(new Employee());
				
				
				service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
					
					@Override
					public void onFailure(Throwable caught) {
					}
		
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						
						System.out.println("result size is ========"+result.size());
						
						for(SuperModel model:result)
						{
							Employee empEntity=(Employee)model;
							
							getPocCellno().setValue(empEntity.getCellNumber1()+"");
							getPocEmail().setValue(empEntity.getEmail());
						}
						}
					 });
		
		 hideWaitSymbol();
			}
	     };
  	 timer.schedule(2000);
		
	}
	
	
	
	
	
	
	
//	@Override
//	public void onClick(ClickEvent event) {
////			super.onClick(event);
//			
//		System.out.println("inside click event");
//		//******************rohan chnage here for the on check ********************
//		
////		if(event.getSource()==this.getEventmanagement()){
////			
////			System.out.println("send email status ==="+this.getEventmanagement().getValue());
////			
////			if(this.getEventmanagement().getValue()==true){
////				
////				this.getFrom_time().setEnabled(true);
////				this.getFromDate().setEnabled(true);
////				this.getAddress().setEnabled(true);
////				this.getSpeaker().setEnabled(true);
////				this.getSpeakerCell().setEnabled(true);
////				this.getSpeakerEmailID().setEnabled(true);
////				this.getSpeakerTitle().setEnabled(true);
////				this.getAddSpeakerbutton().setEnabled(true);
////				
////			}
////			else
////			{
////				
////				this.getFrom_time().setEnabled(false);
////				this.getFromDate().setEnabled(false);
////				this.getAddress().setEnabled(false);
////				this.getSpeaker().setEnabled(false);
////				this.getSpeakerCell().setEnabled(false);
////				this.getSpeakerEmailID().setEnabled(false);
////				this.getSpeakerTitle().setEnabled(false);
////				this.getAddSpeakerbutton().setEnabled(false);
////				
////			}
////		}
////		
////		
////		
////		
////			if(event.getSource()==this.getSendEmail()){
////				
////				System.out.println("send email status ==="+this.getSendEmail().getValue());
////				
////				if(this.getSendEmail().getValue()==true){
////					
////					this.getDescription().setEnabled(true);
////				}
////				else
////				{
////					
////					this.getDescription().setEnabled(false);
////					
////				}
////			}
////			
////			
////			
////			if(event.getSource().equals(this.getSendSMS())){
////					
////					System.out.println("send email status ==="+this.getSendEmail().getValue());
////					
////					if(this.getSendSMS().getValue()==true){
////						
////						this.getSmsTemplate().setEnabled(true);
////						this.getSmsEventList().setEnabled(true);
////						this.getAddTemplate().setEnabled(true);
////					}
////					else
////					{
////						this.getSmsTemplate().setEnabled(false);
////						this.getSmsEventList().setEnabled(false);
////						this.getAddTemplate().setEnabled(false);
////					}
////				}
//			
//		
//		
//		//*******************************changes ends here ***************************
//		
//			if(event.getSource().equals(this.getAddSpeakerbutton())){
//						
//				if(!this.title.getValue().equals("")){
//							
//							if(!this.getSpeaker().getValue().equals(""))
//								{
//								if(!this.getSpeakerTitle().getValue().equals(""))
//									{
//									if((!this.getSpeakerCell().getValue().equals(""))||(!this.getSpeakerEmailID().getValue().equals("")))
//										{
//											
//										CustomerTrainingSpeaker speaker =new  CustomerTrainingSpeaker();
//										
//										
//										System.out.println("speaker name ===="+this.getSpeaker().getValue().trim());
//										speaker.setSpeakerName(this.getSpeaker().getValue().trim());
//										speaker.setSpeakercellno(Long.parseLong(this.getSpeakerCell().getValue().trim()));
//										speaker.setSpeakerEmail(this.getSpeakerEmailID().getValue().trim());
//										speaker.setSpeakerTitle(this.getSpeakerTitle().getValue().trim());
//										this.speakertable.getDataprovider().getList().add(speaker);
//										
//										this.getSpeaker().setValue("");
//										this.getSpeakerCell().setValue("");
//										this.getSpeakerEmailID().setValue("");
//										this.getSpeakerTitle().setValue("");
//										
//										}
//							else
//								{
//									this.showDialogMessage("Please enter atleast one contact information");
//								}
//								}
//							else
//								{
//									this.showDialogMessage("Please enter speaker title");
//								}
//								}
//							else
//								{
//									this.showDialogMessage("Please enter speaker name");
//								}
//								}
//							else
//								{
//									this.showDialogMessage("Please enter title first");
//								}
//					
//			}
//			
//			if(event.getSource().equals(this.getAddTemplate())){
//				
//				
//			        if(this.smsEventList.getValue()!=null)
//			            {
//			        	MyQuerry querry = new MyQuerry();
//					  	Company c = new Company();
//					  	Vector<Filter> filtervec=new Vector<Filter>();
//					  	Filter filter = null;
//					  	filter = new Filter();
//					  	filter.setQuerryString("companyId");
//						filter.setLongValue(c.getCompanyId());
//						filtervec.add(filter);
//						System.out.println("this.getSmsTemplate().getValue().trim()====="+this.getSmsEventList().getValue().trim());
//						filter = new Filter();
//						filter.setQuerryString("event");
//						filter.setStringValue(this.getSmsEventList().getValue().trim());
//						filtervec.add(filter);
//						querry.setFilters(filtervec);
//						
//						querry.setQuerryObject(new SmsTemplate());
//						
//						service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
//							
//							@Override
//							public void onFailure(Throwable caught) {
//							}
//
//							@Override
//							public void onSuccess(ArrayList<SuperModel> result) {
//								
//						System.out.println(" result size=="+result.size());
//							for(SuperModel model:result)
//							{
//								final SmsTemplate smsEntity=(SmsTemplate)model;
//								
//								if(smsEntity.getStatus()==true){
//									smsTemplate.setValue("");
//									smsTemplate.setValue(smsEntity.getMessage());
//								}
//								else {
//									showDialogMessage("Sms template status is inactive");
//								}
//							}
//							}
//							});
//			            	
//			            }
//			       else
//			        	{	
//			    	   		smsTemplate.setValue("");
//			        		showDialogMessage("Select atlest one template from template list");
//			        	}
//				
//				
//				
//				
//				
//				
//				
//			}
//			
//			if(event.getSource().equals(this.getAddAttendiesbyComposite())){
//				
//				if(!this.getPersonInfoComposite().getName().getValue().equals("")){
//					
//					
//					MyQuerry querry = new MyQuerry();
//				  	Company c = new Company();
//				  	Vector<Filter> filtervec=new Vector<Filter>();
//				  	Filter filter = null;
//				  	filter = new Filter();
//				  	filter.setQuerryString("companyId");
//					filter.setLongValue(c.getCompanyId());
//					filtervec.add(filter);
//					
//					System.out.println(" cust id value ===="+Integer.parseInt(this.getPersonInfoComposite().getId().getValue()));
//					System.out.println("cust name==="+ this.getPersonInfoComposite().getName().getValue().trim());
//					
//					filter = new Filter();
//					filter.setQuerryString("count");
//					filter.setIntValue(Integer.parseInt(this.getPersonInfoComposite().getId().getValue()));
//					filtervec.add(filter);
//					querry.setFilters(filtervec);
//					
//					
////					filter = new Filter();
////					filter.setQuerryString("fullname");
////					filter.setStringValue(this.getPersonInfoComposite().getName().getValue().trim());
////					filtervec.add(filter);
////					querry.setFilters(filtervec);
//					
////					filter = new Filter();
////					filter.setQuerryString("contacts.cellNo1");
////					filter.setStringValue(this.getPersonInfoComposite().getPhone().getValue().trim());
////					filtervec.add(filter);
////					querry.setFilters(filtervec);
//					
//					querry.setQuerryObject(new Customer());
//					
//					service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
//						
//						@Override
//						public void onFailure(Throwable caught) {
//						}
//
//						@Override
//						public void onSuccess(ArrayList<SuperModel> result) {
//							
//					System.out.println(" result size=="+result.size());
//						for(SuperModel model:result)
//						{
//							final Customer custEntity=(Customer)model;
//
//							
//							if(custEntity.isNewCustomerFlag()==true){
//								showDialogMessage("Please fill customer information by editing customer");
//								personInfoComposite.clear();
//							}
//							else {
//								
//								
//								CustomerTrainingAttendies attendies = new CustomerTrainingAttendies();
//								
//								System.out.println("table size ==="+attendiestable.getDataprovider().getList().size());
//								attendies.setSrNo(attendiestable.getDataprovider().getList().size()+1);
//								
//								attendies.setCustId(custEntity.getCount());
//								attendies.setCustCell(custEntity.getCellNumber1()+"");
//								attendies.setCustFirstName(custEntity.getFirstName());
//								attendies.setCustLastName(custEntity.getLastName());
//								attendies.setCustEmail(custEntity.getEmail());
//								attendies.setType(custEntity.getType());
//								attendies.setCategory(custEntity.getCategory());
//								attendies.setGroup(custEntity.getGroup());
//								attendies.setPriority(custEntity.getCustomerPriority());
//								
//								attendiestable.getDataprovider().getList().add(attendies);
//								
//								personInfoComposite.clear();
//								
//							System.out.println("attendiestable.getDataprovider().getList() size"+attendiestable.getDataprovider().getList().size());
//								
//						
//							}
//							
//						}
//						}
//						});
//					
//				}
//				else 
//				{
//					this.showDialogMessage("Plese enter customer information ");
//				}
//				
//			}
//			
//			
//			if(event.getSource().equals(this.getAddAttendiesbutton())){
//				
//						if(!this.getCustFirstName().getValue().equals(""))
//							{
//							if(!this.getCustLastName().getValue().equals(""))
//								{
//								if(!this.getCustcellNo().getValue().equals("")||(!this.getCustemailID().getValue().equals("")))
//									{
//									
//									
//									MyQuerry querry = new MyQuerry();
//								  	Company c = new Company();
//								  	Vector<Filter> filtervec=new Vector<Filter>();
//								  	Filter filter = null;
//								  	filter = new Filter();
//								  	filter.setQuerryString("companyId");
//									filter.setLongValue(c.getCompanyId());
//									filtervec.add(filter);
//									
//									System.out.println("cell no ======"+this.getCustcellNo().getValue());
//									if(!this.getCustcellNo().getValue().equals("")){
//									filter = new Filter();
//									filter.setQuerryString("contacts.cellNo1");
//									filter.setLongValue(Long.parseLong(this.getCustcellNo().getValue()));
//									filtervec.add(filter);
//									querry.setFilters(filtervec);
//									}
//									System.out.println("Email====== "+this.getCustemailID().getValue());
//									if(!this.getCustemailID().getValue().equals("")){
//										filter = new Filter();
//										filter.setQuerryString("contacts.email");
//										filter.setStringValue(this.getCustemailID().getValue());
//										filtervec.add(filter);
//										querry.setFilters(filtervec);
//										}
//									
//									
//									
//									querry.setQuerryObject(new Customer());
//									
//									service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
//										
//										@Override
//										public void onFailure(Throwable caught) {
//										}
//
//										@Override
//										public void onSuccess(ArrayList<SuperModel> result) {
//											
//											System.out.println(" result size=="+result.size());
//											
//											if(result.size()==0)
//											{
//												CustomerTrainingAttendies attendies = new CustomerTrainingAttendies();
//												
//												
//												attendies.setCustCell(getCustcellNo().getValue());
//												attendies.setCustFirstName(getCustFirstName().getValue());
//												attendies.setCustLastName(getCustLastName().getValue());
//												attendies.setCustEmail(getCustemailID().getValue());
//												
//												attendiestable.getDataprovider().getList().add(attendies);
//											
//												
//												getCustcellNo().setValue("");
//												getCustemailID().setValue("");
//												getCustFirstName().setValue("");
//												getCustLastName().setValue("");
//											}
//											else
//											{
//												showDialogMessage("Customer is already present in the system");
//												
//											}
//										}
//										});
//										}
//							else
//									{
//									this.showDialogMessage("Plese enter atleast one contact information ");
//									}
//								
//								}
//							else
//								{
//								this.showDialogMessage("Plese enter customer last name ");
//								}
//							}
//						else
//							{
//							this.showDialogMessage("Plese enter customer first name ");
//							}
//								
//			}
//			
//			
//			
//			
//			
//			
//			if(event.getSource().equals(this.getClearTable())){
//				attendiestable.connectToLocal();
//			}
//			
//			
//			if(event.getSource().equals(addCustByType)){
//				
//				MyQuerry querry = new MyQuerry();
//			  	Company c = new Company();
//			  	Vector<Filter> filtervec=new Vector<Filter>();
//			  	Filter filter = null;
//			  	filter = new Filter();
//			  	filter.setQuerryString("companyId");
//				filter.setLongValue(c.getCompanyId());
//				filtervec.add(filter);
//				filter = new Filter();
//				filter.setQuerryString("businessProcess.type");
//				filter.setStringValue(this.getOlbCustomerType().getValue().trim());
//				filtervec.add(filter);
//				querry.setFilters(filtervec);
//				querry.setQuerryObject(new Customer());
//				
//				service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
//					
//					@Override
//					public void onFailure(Throwable caught) {
//					}
//
//					@Override
//					public void onSuccess(ArrayList<SuperModel> result) {
//						ArrayList<Customer> customerList=new ArrayList<Customer>();
//						System.out.println(" result set size +++++++"+result.size());
//						
//						if(result.size()==0){
//							showDialogMessage("Customer for selected type is not available");
//						}
//						else{
//							
//						for(SuperModel model:result)
//						{
//							Customer custEntity=(Customer)model;
//							
//							customerList.add(custEntity);
//						}
//						
//						
//						
//						ArrayList<CustomerTrainingAttendies> filteredCustomers=retrieveDataForTable(customerList);
//						
//						attendiestable.connectToLocal();
//						
//						List<CustomerTrainingAttendies> parentlistwithNubering= new ArrayList<CustomerTrainingAttendies>(); 
//						
//						for(int i=0;i<filteredCustomers.size();i++){
//							
//							CustomerTrainingAttendies attendies = new CustomerTrainingAttendies();
//							attendies.setSrNo(i+1);
//							attendies.setCustId(filteredCustomers.get(i).getCustId());
//							attendies.setCustFirstName(filteredCustomers.get(i).getCustFirstName());
//							attendies.setCustLastName(filteredCustomers.get(i).getCustLastName());
//							attendies.setCustCell(filteredCustomers.get(i).getCustCell());
//							attendies.setCustEmail(filteredCustomers.get(i).getCustEmail());
//							
//							attendies.setGroup(filteredCustomers.get(i).getGroup());
//							attendies.setPriority(filteredCustomers.get(i).getPriority());
//							attendies.setType(filteredCustomers.get(i).getType());
//							attendies.setCategory(filteredCustomers.get(i).getCategory());
//							
//							parentlistwithNubering.add(attendies);
//						}
//						
//						attendiestable.getDataprovider().setList(parentlistwithNubering);
//						
//						olbCustomerType.setValue("");
//					}
//					}
//				});
//						
//			}
//			
//			
//				if(event.getSource().equals(addCustByGroup)){
//				
//				MyQuerry querry = new MyQuerry();
//			  	Company c = new Company();
//			  	Vector<Filter> filtervec=new Vector<Filter>();
//			  	Filter filter = null;
//			  	filter = new Filter();
//			  	filter.setQuerryString("companyId");
//				filter.setLongValue(c.getCompanyId());
//				filtervec.add(filter);
//				filter = new Filter();
//				filter.setQuerryString("businessProcess.group");
//				filter.setStringValue(this.olbCustomerGroup.getValue().trim());
//				filtervec.add(filter);
//				querry.setFilters(filtervec);
//				querry.setQuerryObject(new Customer());
//				
//				service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
//					
//					@Override
//					public void onFailure(Throwable caught) {
//					}
//
//					@Override
//					public void onSuccess(ArrayList<SuperModel> result) {
//						ArrayList<Customer> customerList=new ArrayList<Customer>();
//						System.out.println(" result set size +++++++"+result.size());
//						
//						if(result.size()==0){
//							showDialogMessage("Customer for selected group is not available");
//						}
//						else{
//						for(SuperModel model:result)
//						{
//							Customer custEntity=(Customer)model;
//							
//							customerList.add(custEntity);
//						}
//						
//						
//						
//						ArrayList<CustomerTrainingAttendies> filteredCustomers=retrieveDataForTable(customerList);
//						
//						attendiestable.connectToLocal();
//						
//						List<CustomerTrainingAttendies> parentlistwithNubering= new ArrayList<CustomerTrainingAttendies>(); 
//						
//						for(int i=0;i<filteredCustomers.size();i++){
//							
//							CustomerTrainingAttendies attendies = new CustomerTrainingAttendies();
//							attendies.setSrNo(i+1);
//							attendies.setCustId(filteredCustomers.get(i).getCustId());
//							attendies.setCustFirstName(filteredCustomers.get(i).getCustFirstName());
//							attendies.setCustLastName(filteredCustomers.get(i).getCustLastName());
//							attendies.setCustCell(filteredCustomers.get(i).getCustCell());
//							attendies.setCustEmail(filteredCustomers.get(i).getCustEmail());
//							
//							attendies.setGroup(filteredCustomers.get(i).getGroup());
//							attendies.setPriority(filteredCustomers.get(i).getPriority());
//							attendies.setType(filteredCustomers.get(i).getType());
//							attendies.setCategory(filteredCustomers.get(i).getCategory());
//							
//							parentlistwithNubering.add(attendies);
//						}
//						attendiestable.getDataprovider().setList(parentlistwithNubering);
//						
//						olbCustomerGroup.setValue("");
//					}
//					}
//				});
//						
//			}
//				
//				if(event.getSource().equals(addCustByPriority)){
//					
//					MyQuerry querry = new MyQuerry();
//				  	Company c = new Company();
//				  	Vector<Filter> filtervec=new Vector<Filter>();
//				  	Filter filter = null;
//				  	filter = new Filter();
//				  	filter.setQuerryString("companyId");
//					filter.setLongValue(c.getCompanyId());
//					filtervec.add(filter);
//					filter = new Filter();
//					filter.setQuerryString("customerPriority");
//					filter.setStringValue(this.olbCustomerPriBox.getValue().trim());
//					filtervec.add(filter);
//					querry.setFilters(filtervec);
//					querry.setQuerryObject(new Customer());
//					
//					service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
//						
//						@Override
//						public void onFailure(Throwable caught) {
//						}
//
//						@Override
//						public void onSuccess(ArrayList<SuperModel> result) {
//							
//							ArrayList<Customer> customerList=new ArrayList<Customer>();
//							System.out.println(" result set size +++++++"+result.size());
//							if(result.size()==0){
//								showDialogMessage("Customer for selected priority is not available");
//							}
//							else{
//							
//							for(SuperModel model:result)
//							{
//								Customer custEntity=(Customer)model;
//								
//								customerList.add(custEntity);
//							}
//							
//							
//							
//							ArrayList<CustomerTrainingAttendies> filteredCustomers=retrieveDataForTable(customerList);
//							
//							attendiestable.connectToLocal();
//							
//							
//							List<CustomerTrainingAttendies> parentlistwithNubering= new ArrayList<CustomerTrainingAttendies>(); 
//							
//							for(int i=0;i<filteredCustomers.size();i++){
//								
//								CustomerTrainingAttendies attendies = new CustomerTrainingAttendies();
//								attendies.setSrNo(i+1);
//								attendies.setCustId(filteredCustomers.get(i).getCustId());
//								attendies.setCustFirstName(filteredCustomers.get(i).getCustFirstName());
//								attendies.setCustLastName(filteredCustomers.get(i).getCustLastName());
//								attendies.setCustCell(filteredCustomers.get(i).getCustCell());
//								attendies.setCustEmail(filteredCustomers.get(i).getCustEmail());
//								attendies.setGroup(filteredCustomers.get(i).getGroup());
//								attendies.setPriority(filteredCustomers.get(i).getPriority());
//								attendies.setType(filteredCustomers.get(i).getType());
//								attendies.setCategory(filteredCustomers.get(i).getCategory());
//								
//								parentlistwithNubering.add(attendies);
//							}
//							
//							
//							attendiestable.getDataprovider().setList(parentlistwithNubering);
//							
//							olbCustomerPriBox.setValue("");
//						}
//						}
//					});
//							
//				}
//			
//			
//			
//			if(event.getSource().equals(addCustByCatergory))
//			{
//				
//				MyQuerry querry = new MyQuerry();
//			  	Company c = new Company();
//			  	Vector<Filter> filtervec=new Vector<Filter>();
//			  	Filter filter = null;
//			  	filter = new Filter();
//			  	filter.setQuerryString("companyId");
//				filter.setLongValue(c.getCompanyId());
//				filtervec.add(filter);
//				filter = new Filter();
//				filter.setQuerryString("businessProcess.category");
//				filter.setStringValue(this.getOlbCustomerCategory().getValue().trim());
//				filtervec.add(filter);
//				querry.setFilters(filtervec);
//				querry.setQuerryObject(new Customer());
//				
//				service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
//					
//					@Override
//					public void onFailure(Throwable caught) {
//					}
//
//					@Override
//					public void onSuccess(ArrayList<SuperModel> result) {
//						ArrayList<Customer> customerList=new ArrayList<Customer>();
//						System.out.println(" result set size +++++++"+result.size());
//						
//						if(result.size()==0){
//							showDialogMessage("Customer for selected category is not available");
//						}
//						else{
//							
//						for(SuperModel model:result)
//						{
//							Customer custEntity=(Customer)model;
//							
//							customerList.add(custEntity);
//						}
//						
//						
//						
//						ArrayList<CustomerTrainingAttendies> filteredCustomers=retrieveDataForTable(customerList);
//						
//						attendiestable.connectToLocal();
//						
//						List<CustomerTrainingAttendies> parentlistwithNubering= new ArrayList<CustomerTrainingAttendies>(); 
//						
//						for(int i=0;i<filteredCustomers.size();i++){
//							
//							CustomerTrainingAttendies attendies = new CustomerTrainingAttendies();
//							attendies.setSrNo(i+1);
//							attendies.setCustId(filteredCustomers.get(i).getCustId());
//							attendies.setCustFirstName(filteredCustomers.get(i).getCustFirstName());
//							attendies.setCustLastName(filteredCustomers.get(i).getCustLastName());
//							attendies.setCustCell(filteredCustomers.get(i).getCustCell());
//							attendies.setCustEmail(filteredCustomers.get(i).getCustEmail());
//							
//							attendies.setGroup(filteredCustomers.get(i).getGroup());
//							attendies.setPriority(filteredCustomers.get(i).getPriority());
//							attendies.setType(filteredCustomers.get(i).getType());
//							attendies.setCategory(filteredCustomers.get(i).getCategory());
//							
//							parentlistwithNubering.add(attendies);
//						}
//						
//						attendiestable.getDataprovider().setList(parentlistwithNubering);
//						
//						olbCustomerCategory.setValue("");
//					}
//					}
//				});
//						
//						
//				
//				
//			}
//		}
	
		public ArrayList<CustomerTrainingAttendies> retrieveDataForTable(ArrayList<Customer> custList)
		{
			List<CustomerTrainingAttendies> arrOfAttendies=attendiestable.getDataprovider().getList();
			ArrayList<CustomerTrainingAttendies> filteredCustomers=new ArrayList<CustomerTrainingAttendies>();
			for(int g=0;g<custList.size();g++)
			{
				if(!validateCustomer(custList.get(g).getCount(), arrOfAttendies))
				{
					CustomerTrainingAttendies attendiesEntity=new CustomerTrainingAttendies();
					attendiesEntity.setSrNo(g+1);
					attendiesEntity.setCustFirstName(custList.get(g).getFirstName());
					attendiesEntity.setCustLastName(custList.get(g).getLastName());
					attendiesEntity.setCustId(custList.get(g).getCount());
					attendiesEntity.setCustCell(custList.get(g).getCellNumber1()+"");
					attendiesEntity.setCustEmail(custList.get(g).getEmail());
					
					attendiesEntity.setGroup(custList.get(g).getGroup());
					attendiesEntity.setCategory(custList.get(g).getCategory());
					attendiesEntity.setPriority(custList.get(g).getCustomerPriority());
					attendiesEntity.setType(custList.get(g).getType());
					
					
					
					filteredCustomers.add(attendiesEntity);
				}
			}
			
			if(arrOfAttendies.size()!=0)
			{
				filteredCustomers.addAll(arrOfAttendies);
			}
			
			
			return filteredCustomers;
		}
		
		private boolean validateCustomer(int custCount,List<CustomerTrainingAttendies> lis)
		{
			int ctr=0;
			for(int h=0;h<lis.size();h++)
			{
				if(lis.get(h).getCustId()==custCount)
				{
					ctr=ctr+1;
				}
			}
			
			if(ctr==0)
			{
				return false;
			}
			else{
				return true;
			}
		}
		
		/**
		 * The method is responsible for changing Application state as per 
		 * status
		 */
		public void setMenuAsPerStatus()
		{
			this.toggleAppHeaderBarMenu();
			this.toggleProcessLevelMenu();
			
		}
		
		public void toggleProcessLevelMenu() {
			
			System.out.println("inside toggle as per status.....");
			CustomerTrainingDetails entity = (CustomerTrainingDetails) presenter.getModel();
			String status=entity.getStatus();
			
			System.out.println("inside toggle status value ....."+status);
			
			//*************for sms button **************************
			String smstemplate = entity.getSmsTemplate();
			
			for (int i = 0; i < getProcesslevelBarNames().length; i++) {
				InlineLabel label = getProcessLevelBar().btnLabels[i];
				String text = label.getText().trim();
				
				if ((status.equals(CustomerTrainingDetails.APPROVED))) {
					if (text.equals("Send Email")){
						label.setVisible(true);
					}
					
					if (text.equals("Mark Executed")){
						label.setVisible(true);
					}
					
					if (text.equals("Send SMS")){
						
					if(!smstemplate.equals("")){	
						label.setVisible(true);
					}
					else{
						label.setVisible(false);
					}
					
					}
					
				}
				
				if ((status.equals(CustomerTrainingDetails.CREATED))) {
					
					if (text.equals(ManageApprovals.APPROVALREQUEST)){
						label.setVisible(true);
					}
					if (text.equals(ManageApprovals.CANCELAPPROVALREQUEST)){
						label.setVisible(false);
					}
					
					if (text.equals("Send Email")){
						label.setVisible(false);
					}
					
					if (text.equals("Send SMS")){
						label.setVisible(false);
					}
					if (text.equals("Mark Executed")){
						label.setVisible(false);
					}
					
					
				}
				
				if ((status.equals(CustomerTrainingDetails.REQUESTED))) {
					if (text.equals(ManageApprovals.CANCELAPPROVALREQUEST)){
						label.setVisible(true);
					}
					
					if (text.equals("Send Email")){
						label.setVisible(false);
					}
					
					if (text.equals("Send SMS")){
						label.setVisible(false);
					}
					if (text.equals("Mark Executed")){
						label.setVisible(false);
					}
					
				}
				
				if ((status.equals(CustomerTrainingDetails.EXECUTED))) {
					if (text.equals(ManageApprovals.CANCELAPPROVALREQUEST)){
						label.setVisible(false);
					}
					
					if (text.equals("Send Email")){
						label.setVisible(false);
					}
					
					if (text.equals("Send SMS")){
						label.setVisible(false);
					}
					if (text.equals(ManageApprovals.APPROVALREQUEST)){
						label.setVisible(false);
					}
					if (text.equals("Mark Executed")){
						label.setVisible(false);
					}
					
				}
				
				
				
				if ((status.equals(CustomerTrainingDetails.REJECTED) || status.equals(CustomerTrainingDetails.CANCELLED))) {
					if (text.equals(AppConstants.NEW))
						label.setVisible(true);
					else
						label.setVisible(false);
					
					if (text.equals("send Email")){
						label.setVisible(false);
					}
					
					if (text.equals("send SMS")){
						label.setVisible(false);
					}
				}
			}

		}
	//****************************getters and setters ******************************************

				public TextBox getCustTrainingId() {
					return custTrainingId;
				}



				public void setCustTrainingId(TextBox custTrainingId) {
					this.custTrainingId = custTrainingId;
				}



				public TextBox getSpeaker() {
					return speaker;
				}



				public void setSpeaker(TextBox speaker) {
					this.speaker = speaker;
				}



				public Label getReflable() {
					return reflable;
				}



				public void setReflable(Label reflable) {
					this.reflable = reflable;
				}



				public Button getAddSpeakerbutton() {
					return addSpeakerbutton;
				}



				public void setAddSpeakerbutton(Button addSpeakerbutton) {
					this.addSpeakerbutton = addSpeakerbutton;
				}



				public Button getAddAttendiesbutton() {
					return addAttendiesbutton;
				}



				public void setAddAttendiesbutton(Button addAttendiesbutton) {
					this.addAttendiesbutton = addAttendiesbutton;
				}

				
				public TextBox getTitle() {
					return title;
				}

				
				public void setTitle(TextBox title) {
					this.title = title;
				}


				
				public TextBox getCustcellNo() {
					return custcellNo;
				}



				public void setCustcellNo(TextBox custcellNo) {
					this.custcellNo = custcellNo;
				}



				public TextBox getCustemailID() {
					return custemailID;
				}



				public void setCustemailID(TextBox custemailID) {
					this.custemailID = custemailID;
				}



				public DateBox getFromDate() {
					return fromDate;
				}



				public void setFromDate(DateBox fromDate) {
					this.fromDate = fromDate;
				}



//				public DateBox getToDate() {
//					return toDate;
//				}
//
//
//
//				public void setToDate(DateBox toDate) {
//					this.toDate = toDate;
//				}



				public PersonInfoComposite getPersonInfoComposite() {
					return personInfoComposite;
				}



				public void setPersonInfoComposite(PersonInfoComposite personInfoComposite) {
					this.personInfoComposite = personInfoComposite;
				}



				public CustomerTrainingSpeakerTable getSpeakertable() {
					return speakertable;
				}



				public void setSpeakertable(CustomerTrainingSpeakerTable speakertable) {
					this.speakertable = speakertable;
				}



				public CustomerTrainingAttendiesTable getAttendiestable() {
					return attendiestable;
				}



				public void setAttendiestable(CustomerTrainingAttendiesTable attendiestable) {
					this.attendiestable = attendiestable;
				}



				public TextBox getSpeakerCell() {
					return speakerCell;
				}



				public void setSpeakerCell(TextBox speakerCell) {
					this.speakerCell = speakerCell;
				}



				public TextBox getSpeakerEmailID() {
					return speakerEmailID;
				}



				public void setSpeakerEmailID(TextBox speakerEmailID) {
					this.speakerEmailID = speakerEmailID;
				}


				
				public ObjectListBox<Employee> getPocName() {
					return pocName;
				}



				public void setPocName(ObjectListBox<Employee> pocName) {
					this.pocName = pocName;
				}



				public TextBox getPocCellno() {
					return pocCellno;
				}



				public void setPocCellno(TextBox pocCellno) {
					this.pocCellno = pocCellno;
				}



				public TextBox getPocEmial() {
					return pocEmail;
				}



				public void setPocEmial(TextBox pocEmial) {
					this.pocEmail = pocEmial;
				}



				public TextBox getCustFirstName() {
					return custFirstName;
				}



				public void setCustFirstName(TextBox custFirstName) {
					this.custFirstName = custFirstName;
				}



				public TextBox getCustLastName() {
					return custLastName;
				}



				public void setCustLastName(TextBox custLastName) {
					this.custLastName = custLastName;
				}



				public TextArea getDescription() {
					return description;
				}



				public void setDescription(TextArea description) {
					this.description = description;
				}



				public ObjectListBox<Type> getOlbCustomerType() {
					return olbCustomerType;
				}



				public void setOlbCustomerType(ObjectListBox<Type> olbCustomerType) {
					this.olbCustomerType = olbCustomerType;
				}



				public ObjectListBox<ConfigCategory> getOlbCustomerCategory() {
					return olbCustomerCategory;
				}



				public void setOlbCustomerCategory(
						ObjectListBox<ConfigCategory> olbCustomerCategory) {
					this.olbCustomerCategory = olbCustomerCategory;
				}



				public Button getAddCustByCatergory() {
					return addCustByCatergory;
				}



				public void setAddCustByCatergory(Button addCustByCatergory) {
					this.addCustByCatergory = addCustByCatergory;
				}



				public Button getAddCustByType() {
					return addCustByType;
				}



				public void setAddCustByType(Button addCustByType) {
					this.addCustByType = addCustByType;
				}



				public Button getAddCustByGroup() {
					return addCustByGroup;
				}



				public void setAddCustByGroup(Button addCustByGroup) {
					this.addCustByGroup = addCustByGroup;
				}



				public Button getAddCustByPriority() {
					return addCustByPriority;
				}



				public void setAddCustByPriority(Button addCustByPriority) {
					this.addCustByPriority = addCustByPriority;
				}



				public Button getAddTemplate() {
					return addTemplate;
				}



				public void setAddTemplate(Button addTemplate) {
					this.addTemplate = addTemplate;
				}



				public Button getClearTable() {
					return clearTable;
				}



				public void setClearTable(Button clearTable) {
					this.clearTable = clearTable;
				}



				public ObjectListBox<Config> getOlbCustomerGroup() {
					return olbCustomerGroup;
				}



				public void setOlbCustomerGroup(ObjectListBox<Config> olbCustomerGroup) {
					this.olbCustomerGroup = olbCustomerGroup;
				}



				public ObjectListBox<Config> getOlbCustomerPriBox() {
					return olbCustomerPriBox;
				}



				public void setOlbCustomerPriBox(ObjectListBox<Config> olbCustomerPriBox) {
					this.olbCustomerPriBox = olbCustomerPriBox;
				}



				public ObjectListBox<SmsTemplate> getSmsEventList() {
					return smsEventList;
				}



				public void setSmsEventList(ObjectListBox<SmsTemplate> smsEventList) {
					this.smsEventList = smsEventList;
				}



				public TextArea getSmsTemplate() {
					return smsTemplate;
				}



				public void setSmsTemplate(TextArea smsTemplate) {
					
					if(smsTemplate!=null){
					
						this.smsTemplate = smsTemplate;
					}
						
					
				}



				public GenricServiceAsync getService() {
					return service;
				}



				public void setService(GenricServiceAsync service) {
					this.service = service;
				}



				public TextBox getFrom_time() {
					return from_time;
				}



				public void setFrom_time(TextBox from_time) {
					this.from_time = from_time;
				}



//				public TextBox getTo_time() {
//					return to_time;
//				}
//
//
//
//				public void setTo_time(TextBox to_time) {
//					this.to_time = to_time;
//				}



				public TextBox getStatus() {
					return status;
				}



				public void setStatus(TextBox status) {
					this.status = status;
				}



				public TextBox getPocEmail() {
					return pocEmail;
				}



				public void setPocEmail(TextBox pocEmail) {
					this.pocEmail = pocEmail;
				}



				public ObjectListBox<Employee> getOlbeSalesPerson() {
					return olbeSalesPerson;
				}



				public void setOlbeSalesPerson(ObjectListBox<Employee> olbeSalesPerson) {
					this.olbeSalesPerson = olbeSalesPerson;
				}



				public ObjectListBox<Employee> getOlbApproverName() {
					return olbApproverName;
				}



				public void setOlbApproverName(ObjectListBox<Employee> olbApproverName) {
					this.olbApproverName = olbApproverName;
				}



				public TextBox getSpeakerTitle() {
					return speakerTitle;
				}



				public void setSpeakerTitle(TextBox speakerTitle) {
					this.speakerTitle = speakerTitle;
				}



				public TextArea getAddress() {
					return address;
				}



				public void setAddress(TextArea address) {
					this.address = address;
				}



				public Button getAddAttendiesbyComposite() {
					return addAttendiesbyComposite;
				}



				public void setAddAttendiesbyComposite(Button addAttendiesbyComposite) {
					this.addAttendiesbyComposite = addAttendiesbyComposite;
				}



				public CheckBox getSendSMS() {
					return sendSMS;
				}



				public void setSendSMS(CheckBox sendSMS) {
					this.sendSMS = sendSMS;
				}



				public CheckBox getSendEmail() {
					return sendEmail;
				}



				public void setSendEmail(CheckBox sendEmail) {
					this.sendEmail = sendEmail;
				}



				public CheckBox getEventmanagement() {
					return eventmanagement;
				}



				public void setEventmanagement(CheckBox eventmanagement) {
					this.eventmanagement = eventmanagement;
				}



				public Button getAddAttendiesbyexcelbutton() {
					return addAttendiesbyexcelbutton;
				}



				public void setAddAttendiesbyexcelbutton(Button addAttendiesbyexcelbutton) {
					this.addAttendiesbyexcelbutton = addAttendiesbyexcelbutton;
				}



				@Override
				public void refreshTableData() {
					// TODO Auto-generated method stub
					super.refreshTableData();
					speakertable.getTable().redraw();
					attendiestable.getTable().redraw();
				}



				@Override
				public void onChange(ChangeEvent event) {
					// TODO Auto-generated method stub
					if(event.getSource().equals(olbCustomerCategory))
					{
						if(olbCustomerCategory.getSelectedIndex()!=0){
							ConfigCategory cat=olbCustomerCategory.getSelectedItem();
							if(cat!=null){
								AppUtility.makeLiveTypeDropDown(olbCustomerType, cat.getCategoryName(), cat.getCategoryCode(), cat.getInternalType());
							}
						}
					}
				}
				
				
	
		}
