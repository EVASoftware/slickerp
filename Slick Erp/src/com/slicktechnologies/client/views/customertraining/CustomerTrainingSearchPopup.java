package com.slicktechnologies.client.views.customertraining;

import java.util.Vector;

import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.common.CustomerTrainingDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.personlayer.Employee;

public class CustomerTrainingSearchPopup extends SearchPopUpScreen<CustomerTrainingDetails>{

	public IntegerBox tbId;
	public ObjectListBox<Employee> olbEmployee,olbApproverName,olbPersonResponsible;
	public ListBox olbStatus;
	
	
	
//	public Object getVarRef(String varName)
//	{
//		if(varName.equals("personInfo"))
//			return this.personInfo;
//		if(varName.equals("tbContractId"))
//			return this.tbContractId;
//		if(varName.equals("tbQuotationId"))
//			return this.tbQuotationId;
//		if(varName.equals("tbLeadId"))
//			return this.tbLeadId;
//		if(varName.equals("olbBranch"))
//			return this.olbBranch;
//		if(varName.equals("olbEmployee"))
//			return this.olbEmployee;
//		if(varName.equals("dateComparator"))
//			return this.dateComparator;
//		if(varName.equals("olbQuotationGroup"))
//			return this.olbContractGroup;
//		if(varName.equals("olbQuotationCategory"))
//			return this.olbContractCategory;
//		if(varName.equals("olbQuotationType"))
//			return this.olbContractType;
//		if(varName.equals("olbQuotationStatus"))
//			return this.olbContractStatus;
//		if(varName.equals("olbQuotationPriority"))
//			return this.olbContractPriority;
//		return null ;
//	}
	
	public CustomerTrainingSearchPopup()
	{
		super();
		createGui();
	}
	public void initWidget()
	{
		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new Customer());
		tbId= new IntegerBox();

		olbEmployee= new ObjectListBox<Employee>();
//		AppUtility.makeSalesPersonListBoxLive(olbEmployee);
		olbEmployee.makeEmployeeLive(LoginPresenter.currentModule, AppConstants.CAMPAIGNMANAGEMENT, "Sales Person");
		
		olbApproverName=new ObjectListBox<Employee>();
		AppUtility.makeSalesPersonListBoxLive(olbApproverName);
		
		olbStatus=new ListBox();
		AppUtility.setStatusListBox(olbStatus, CustomerTrainingDetails.getStatusList());
		
		
		olbPersonResponsible= new ObjectListBox<Employee>();
//		AppUtility.makeSalesPersonListBoxLive(olbPersonResponsible);
		olbPersonResponsible.makeEmployeeLive(LoginPresenter.currentModule, AppConstants.CAMPAIGNMANAGEMENT, "Person Responsible");
		
	}
	public void createScreen()
	{
		initWidget();
		FormFieldBuilder builder;
		builder = new FormFieldBuilder("Id",tbId);
		FormField ftbId= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Sales Person",olbEmployee);
		FormField folbEmployee= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Approver Name",olbApproverName);
		FormField folbApproval= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Status",olbStatus);
		FormField folbStatus= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Person Responsible ",olbPersonResponsible);
		FormField folbperson= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		this.fields=new FormField[][]{
				{ftbId,folbStatus},
				{folbEmployee,folbApproval,folbperson},
		};
	}
	
	
	public MyQuerry getQuerry()
	{
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
			
		
		if(tbId.getValue()!=null){
			System.out.println("tbId.getValue()===="+tbId.getValue());
			temp=new Filter();
			temp.setIntValue(tbId.getValue());
//			filtervec.add(temp);
			temp.setQuerryString("count");
			filtervec.add(temp);
		}
		
		
		 if(olbEmployee.getSelectedIndex()!=0){
			 System.out.println("olbEmployee.getValue().trim()"+olbEmployee.getValue().trim());
				temp=new Filter();temp.setStringValue(olbEmployee.getValue().trim());
				temp.setQuerryString("employee");
				filtervec.add(temp);
			}
		 
		 if(olbApproverName.getSelectedIndex()!=0){
			 System.out.println("olbApproverName.getValue().trim()"+olbApproverName.getValue().trim());
				temp=new Filter();temp.setStringValue(olbApproverName.getValue().trim());
				temp.setQuerryString("approverName");
				filtervec.add(temp);
			}
		
		 if(olbStatus.getSelectedIndex()!=0){
				temp=new Filter();
				temp.setStringValue(olbStatus.getValue(olbStatus.getSelectedIndex()).trim());
				temp.setQuerryString("status");
				filtervec.add(temp);
			}
		 
		 if(olbPersonResponsible.getSelectedIndex()!=0){
			 System.out.println("olbPersonResponsible.getValue().trim()"+olbPersonResponsible.getValue().trim());
				temp=new Filter();temp.setStringValue(olbPersonResponsible.getValue().trim());
				temp.setQuerryString("pocName");
				filtervec.add(temp);
			}
		 
		 
		MyQuerry querry= new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new CustomerTrainingDetails());
		return querry;
	}
	@Override
	public boolean validate() {
		return true;
	}
	
	
}
