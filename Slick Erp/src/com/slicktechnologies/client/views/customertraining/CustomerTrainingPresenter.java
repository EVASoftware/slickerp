package com.slicktechnologies.client.views.customertraining;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.approvalutility.ApprovableFormScreenPresenter;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.EmailService;
import com.slicktechnologies.client.services.EmailServiceAsync;
import com.slicktechnologies.client.services.ReadingExcelService;
import com.slicktechnologies.client.services.ReadingExcelServiceAsync;
import com.slicktechnologies.client.services.SmsService;
import com.slicktechnologies.client.services.SmsServiceAsync;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.customer.CustomerForm;
import com.slicktechnologies.client.views.documentcancellation.CancellationSummary;
import com.slicktechnologies.client.views.documentcancellation.CancellationSummaryPopUpTable;
import com.slicktechnologies.client.views.inventory.materialissuenote.MaterialIssueNoteForm;
import com.slicktechnologies.client.views.inventory.materialissuenote.MaterialIssueNotePresenter;
import com.slicktechnologies.client.views.inventory.materialissuenote.MaterialIssueNoteSerachProxy;
import com.slicktechnologies.client.views.inventory.materialissuenote.MaterialIssueNoteTableProxy;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Sales;
import com.slicktechnologies.shared.common.CustomerTrainingDetails;
import com.slicktechnologies.shared.common.ExcelRecordsList;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cancelsummary.CancelSummary;
import com.slicktechnologies.shared.common.helperlayer.SmsConfiguration;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;

public class CustomerTrainingPresenter extends ApprovableFormScreenPresenter<CustomerTrainingDetails> implements ClickHandler,ChangeHandler{

	
	static CustomerTrainingForm form;
	final GenricServiceAsync async =GWT.create(GenricService.class);
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	EmailServiceAsync emailService=GWT.create(EmailService.class);
//	SendInvitationPopupup popup= new SendInvitationPopupup();
//	PopupPanel panel ;
	CustomerTrainingAttendiesTable attendiestable;
	Logger logger = Logger.getLogger("NameOfYourLogger");

	ReadingExcelServiceAsync readingexcelasyc = GWT.create(ReadingExcelService.class);
	CustomerTrainingSpeakerTable speakertable;
	
	
	public CustomerTrainingPresenter(FormScreen<CustomerTrainingDetails> view,
			CustomerTrainingDetails model) {
		super(view, model);

		form =(CustomerTrainingForm)view;
		form.getAddAttendiesbutton().addClickHandler(this);
		form.getAddSpeakerbutton().addClickHandler(this);
		form.getSearchpopupscreen().getDwnload().setVisible(false);
		
		
		
		
		form.getAddCustByCatergory().addClickHandler(this);
		form.getAddCustByGroup().addClickHandler(this);
		form.getAddCustByType().addClickHandler(this);
		form.getAddCustByPriority().addClickHandler(this);
		
		
		form.sendSMS.addClickHandler(this);
		form.sendEmail.addClickHandler(this);
		form.getClearTable().addClickHandler(this);
		form.getPocName().addChangeHandler(this);
		form.eventmanagement.addClickHandler(this);
		form.getAddTemplate().addClickHandler(this);
		form.getSmsEventList().addChangeHandler(this);
		
		form.getAddAttendiesbyComposite().addClickHandler(this);
		
		form.getAddAttendiesbyexcelbutton().addClickHandler(this);
		
	}
	
	
	 
	
	
	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		
		super.reactToProcessBarEvents(e);
		InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();
		
							
				if(text.equals("Send SMS")){
					
					/********************************************Sms Method*****************************************/
					
					Vector<Filter> filtrvec = new Vector<Filter>();
					Filter filtr = null;
					
					filtr = new Filter();
					filtr.setLongValue(model.getCompanyId());
					filtr.setQuerryString("companyId");
					filtrvec.add(filtr);
					
					filtr = new Filter();
					filtr.setBooleanvalue(true);
					filtr.setQuerryString("status");
					filtrvec.add(filtr);
					
					MyQuerry querry = new MyQuerry();
					querry.setFilters(filtrvec);
					querry.setQuerryObject(new SmsConfiguration());
//					form.showWaitSymbol();
					
					async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
						
						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							if(result.size()!=0){
								for(SuperModel model:result){
									SmsConfiguration smsconfig = (SmsConfiguration) model;
									boolean smsStatus = smsconfig.getStatus();
									String accountsid = smsconfig.getAccountSID();
									String authotoken = smsconfig.getAuthToken();
									String fromnumber = smsconfig.getPassword();
									
									if(smsStatus==true){
									 sendSMS(accountsid, authotoken, fromnumber);
									}
									else{
										form.showDialogMessage("SMS Configuration is InActive");
									}
								}
							}
							
						}
						@Override
						public void onFailure(Throwable caught) {
							
							
						}
					});
					
					/****************************************sms end *************************************/
					
				}
				
				if(text.equals("Send Email")){
					
					boolean conf = Window.confirm("Do you really want to send email?");
					if (conf == true) {
						emailService.initiateCustomerListEmail((CustomerTrainingDetails) model,new AsyncCallback<Void>() {
				
							@Override
							public void onFailure(Throwable caught) {
								Window.alert("Resource Quota Ended ");
								caught.printStackTrace();
								
							}
				
							@Override
							public void onSuccess(Void result) {
								Window.alert("Email Sent Sucessfully !");
								
							}
						});
					
				}
															
				}
				
				if(text.equals("Mark Executed")){
					
					System.out.println("in side mark executed");		
					model.setStatus(CustomerTrainingDetails.EXECUTED);
					
					async.save(model,new AsyncCallback<ReturnFromServer>() {
						
						@Override
						public void onFailure(Throwable caught) {
							form.showDialogMessage("An Unexpected Error occured !");
						}
						@Override
						public void onSuccess(ReturnFromServer result) {
							
							form.getStatus().setValue(CustomerTrainingDetails.EXECUTED);
							form.setMenuAsPerStatus();
							form.setAppHeaderBarAsPerStatus();
						}
					});
					
				}
			
			
	}
		
	private void sendSMS(final String accountsid, final String authotoken,final String fromnumber) {
		SmsServiceAsync smsserviceAsync=GWT.create(SmsService.class);
		
		List<CustomerTrainingAttendies> custlist =form.getAttendiestable().getDataprovider().getList();
		ArrayList<CustomerTrainingAttendies> arrAttendies=new ArrayList<CustomerTrainingAttendies>();
		arrAttendies.addAll(custlist);
		String actualMsg = form.getSmsTemplate().getValue().trim();
		System.out.println("message==="+actualMsg);
		
		System.out.println("in side  send sms "+custlist.size());
		smsserviceAsync.sendSmsToCustomers(actualMsg, arrAttendies, accountsid, authotoken, fromnumber,LoginPresenter.bhashSMSFlag, new AsyncCallback<Integer>() {
			
			@Override
			public void onFailure(Throwable caught) {
				caught.printStackTrace();
				form.showDialogMessage("Unable to send sms");
			}

			@Override
			public void onSuccess(Integer result) {
			if(result==1){
				
				form.showDialogMessage("Sms send successfully to all customers...!!!");
			}
				
			}
		});
		
	}
//						String serNo = form.getIbServiceSrNo().getValue()+"";
//						System.out.println("Service No===="+serNo);
//						
//						cellNo = form.getPoc().getCellValue();
//						String serDate = AppUtility.parseDate(form.getTbServiceDate().getValue());
//						
//						String cutomerName = templateMsgwithbraces.replace("{CustomerName}", custName);
//						String productName = cutomerName.replace("{ProductName}", prodName);
//						String serviceNo = productName.replace("{ServiceNo}", serNo);
//						
//						
//						 actualMsg = serviceNo.replace("{ServiceDate}", serDate);
//
//						companyName = Slick_Erp.businessUnitName;
//						
//						System.out.println("Actual MSG:"+actualMsg);
//						
//						System.out.println("Company Name========================:"+companyName);
//						
//					
//							@Override
//							public void onFailure(Throwable caught) {
//								form.hideWaitSymbol();
//							}
//
//							@Override
//							public void onSuccess(Integer result) {
//								form.hideWaitSymbol();
//								if(result==1)
//								{
//									form.showDialogMessage("SMS Sent Successfully!");
//									saveSmsHistory();
//
//									
//
//								}
//							}} );
//						form.hideWaitSymbol();
//					}
//					
//					form.hideWaitSymbol();
//				}
			
				
//			}
//			@Override
//			public void onFailure(Throwable caught) {
//			}
//		});

//				form.hideWaitSymbol();
		
	
	
	public static String fillFullName()
	{
		String fullname="";
			fullname=form.getCustFirstName().getValue().trim()+" "+form.getCustLastName().getValue().trim();
		System.out.println("Full Name Of Cust"+fullname);
		return fullname;
	}
	
		
	@Override
	public void onClick(ClickEvent event) {
		super.onClick(event);
		
		

//		super.onClick(event);
		
	System.out.println("inside click event");
	//******************rohan chnage here for the on check ********************
	
	if(event.getSource()==form.getEventmanagement()){
		
		System.out.println("send email status ==="+form.getEventmanagement().getValue());
		
		if(form.getEventmanagement().getValue()==true){
			
			form.getFrom_time().setEnabled(true);
			form.getFromDate().setEnabled(true);
			form.getAddress().setEnabled(true);
			form.getSpeaker().setEnabled(true);
			form.getSpeakerCell().setEnabled(true);
			form.getSpeakerEmailID().setEnabled(true);
			form.getSpeakerTitle().setEnabled(true);
			form.getAddSpeakerbutton().setEnabled(true);
			
		}
		else
		{
			
			form.getFrom_time().setEnabled(false);
			form.getFromDate().setEnabled(false);
			form.getAddress().setEnabled(false);
			form.getSpeaker().setEnabled(false);
			form.getSpeakerCell().setEnabled(false);
			form.getSpeakerEmailID().setEnabled(false);
			form.getSpeakerTitle().setEnabled(false);
			form.getAddSpeakerbutton().setEnabled(false);
			
		}
	}
	
	
	
	
		if(event.getSource()==form.getSendEmail()){
			
			System.out.println("send email status ==="+form.getSendEmail().getValue());
			
			if(form.getSendEmail().getValue()==true){
				
				form.getDescription().setEnabled(true);
			}
			else
			{
				
				form.getDescription().setEnabled(false);
				
			}
		}
		
		
		
		if(event.getSource().equals(form.getSendSMS())){
				
				System.out.println("send email status ==="+form.getSendEmail().getValue());
				
				if(form.getSendSMS().getValue()==true){
					
					form.getSmsTemplate().setEnabled(false);
					form.getSmsEventList().setEnabled(true);
					form.getAddTemplate().setEnabled(true);
				}
				else
				{
					form.getSmsTemplate().setEnabled(false);
					form.getSmsEventList().setEnabled(false);
					form.getAddTemplate().setEnabled(false);
				}
			}
		
	
	
	//*******************************changes ends here ***************************
	
		if(event.getSource().equals(form.getAddSpeakerbutton())){
					
			if(!form.title.getValue().equals("")){
						
						if(!form.getSpeaker().getValue().equals(""))
							{
							if(!form.getSpeakerTitle().getValue().equals(""))
								{
								if((!form.getSpeakerCell().getValue().equals(""))||(!form.getSpeakerEmailID().getValue().equals("")))
									{
										
									CustomerTrainingSpeaker speaker =new  CustomerTrainingSpeaker();
									
									
									System.out.println("speaker name ===="+form.getSpeaker().getValue().trim());
									speaker.setSpeakerName(form.getSpeaker().getValue().trim());
									speaker.setSpeakercellno(Long.parseLong(form.getSpeakerCell().getValue().trim()));
									speaker.setSpeakerEmail(form.getSpeakerEmailID().getValue().trim());
									speaker.setSpeakerTitle(form.getSpeakerTitle().getValue().trim());
									form.speakertable.getDataprovider().getList().add(speaker);
									
									form.getSpeaker().setValue("");
									form.getSpeakerCell().setValue("");
									form.getSpeakerEmailID().setValue("");
									form.getSpeakerTitle().setValue("");
									
									}
						else
							{
							form.showDialogMessage("Please enter atleast one contact information");
							}
							}
						else
							{
							form.showDialogMessage("Please enter speaker title");
							}
							}
						else
							{
							form.showDialogMessage("Please enter speaker name");
							}
							}
						else
							{
							form.showDialogMessage("Please enter title first");
							}
				
		}
		
		if(event.getSource().equals(form.getAddTemplate())){
			
			
		        if(form.smsEventList.getValue()!=null)
		            {
		        	MyQuerry querry = new MyQuerry();
				  	Company c = new Company();
				  	Vector<Filter> filtervec=new Vector<Filter>();
				  	Filter filter = null;
				  	filter = new Filter();
				  	filter.setQuerryString("companyId");
					filter.setLongValue(c.getCompanyId());
					filtervec.add(filter);
					System.out.println("this.getSmsTemplate().getValue().trim()====="+form.getSmsEventList().getValue().trim());
					filter = new Filter();
					filter.setQuerryString("event");
					filter.setStringValue(form.getSmsEventList().getValue().trim());
					filtervec.add(filter);
					querry.setFilters(filtervec);
					
					querry.setQuerryObject(new SmsTemplate());
					
					service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
						
						@Override
						public void onFailure(Throwable caught) {
						}

						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							
					System.out.println(" result size=="+result.size());
						for(SuperModel model:result)
						{
							final SmsTemplate smsEntity=(SmsTemplate)model;
							
							if(smsEntity.getStatus()==true){
								form.smsTemplate.setValue("");
								form.smsTemplate.setValue(smsEntity.getMessage());
							}
							else {
								form.showDialogMessage("Sms template status is inactive");
							}
						}
						}
						});
		            	
		            }
		       else
		        	{	
		    	   form.smsTemplate.setValue("");
		    	   form.showDialogMessage("Select atlest one template from template list");
		        	}
			
		}
		
		if(event.getSource().equals(form.getAddAttendiesbyComposite())){
			
			if(!form.getPersonInfoComposite().getName().getValue().equals("")){
				
				
				MyQuerry querry = new MyQuerry();
			  	Company c = new Company();
			  	Vector<Filter> filtervec=new Vector<Filter>();
			  	Filter filter = null;
			  	filter = new Filter();
			  	filter.setQuerryString("companyId");
				filter.setLongValue(c.getCompanyId());
				filtervec.add(filter);
				
				System.out.println(" cust id value ===="+Integer.parseInt(form.getPersonInfoComposite().getId().getValue()));
				System.out.println("cust name==="+ form.getPersonInfoComposite().getName().getValue().trim());
				
				filter = new Filter();
				filter.setQuerryString("count");
				filter.setIntValue(Integer.parseInt(form.getPersonInfoComposite().getId().getValue()));
				filtervec.add(filter);
				querry.setFilters(filtervec);
				
				
//				filter = new Filter();
//				filter.setQuerryString("fullname");
//				filter.setStringValue(this.getPersonInfoComposite().getName().getValue().trim());
//				filtervec.add(filter);
//				querry.setFilters(filtervec);
				
//				filter = new Filter();
//				filter.setQuerryString("contacts.cellNo1");
//				filter.setStringValue(this.getPersonInfoComposite().getPhone().getValue().trim());
//				filtervec.add(filter);
//				querry.setFilters(filtervec);
				
				querry.setQuerryObject(new Customer());
				
				service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
					
					@Override
					public void onFailure(Throwable caught) {
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						
				System.out.println(" result size=="+result.size());
					for(SuperModel model:result)
					{
						final Customer custEntity=(Customer)model;

						
						if(custEntity.getNewCustomerFlag()==true){
							form.showDialogMessage("Please fill customer information by editing customer");
							form.personInfoComposite.clear();
						}
						else {
							
							
							CustomerTrainingAttendies attendies = new CustomerTrainingAttendies();
							
							System.out.println("table size ==="+form.getAttendiestable().getDataprovider().getList().size());
							attendies.setSrNo(form.getAttendiestable().getDataprovider().getList().size()+1);
							
							attendies.setCustId(custEntity.getCount());
							attendies.setCustCell(custEntity.getCellNumber1()+"");
							attendies.setCustFirstName(custEntity.getFirstName());
							attendies.setCustLastName(custEntity.getLastName());
							attendies.setCustEmail(custEntity.getEmail());
							attendies.setType(custEntity.getType());
							attendies.setCategory(custEntity.getCategory());
							attendies.setGroup(custEntity.getGroup());
							attendies.setPriority(custEntity.getCustomerPriority());
							
							form.getAttendiestable().getDataprovider().getList().add(attendies);
							
							form.personInfoComposite.clear();
							
//						System.out.println("attendiestable.getDataprovider().getList() size"+attendiestable.getDataprovider().getList().size());
							
					
						}
						
					}
					}
					});
				
			}
			else 
			{
				form.showDialogMessage("Plese enter customer information ");
			}
			
		}
		
		
		if(event.getSource().equals(form.getAddAttendiesbutton())){
			
					if(!form.getCustFirstName().getValue().equals(""))
						{
						if(!form.getCustLastName().getValue().equals(""))
							{
							if(!form.getCustcellNo().getValue().equals("")||(!form.getCustemailID().getValue().equals("")))
								{
								
								
									String custnameval=fillFullName();
									custnameval=custnameval.toUpperCase().trim();
									if(custnameval!=null){
										
								
								MyQuerry querry = new MyQuerry();
							  	Company c = new Company();
							  	Vector<Filter> filtervec=new Vector<Filter>();
							  	Filter filter = null;
							  	filter = new Filter();
							  	filter.setQuerryString("companyId");
								filter.setLongValue(c.getCompanyId());
								filtervec.add(filter);
								
								
								filter = new Filter();
								filter.setQuerryString("fullname");
								filter.setStringValue(custnameval);
								filtervec.add(filter);
								querry.setFilters(filtervec);
								
								System.out.println("cell no ======"+form.getCustcellNo().getValue());
								if(!form.getCustcellNo().getValue().equals("")){
								filter = new Filter();
								filter.setQuerryString("contacts.cellNo1");
								filter.setLongValue(Long.parseLong(form.getCustcellNo().getValue()));
								filtervec.add(filter);
								querry.setFilters(filtervec);
								}
								System.out.println("Email====== "+form.getCustemailID().getValue());
								if(!form.getCustemailID().getValue().equals("")){
									filter = new Filter();
									filter.setQuerryString("contacts.email");
									filter.setStringValue(form.getCustemailID().getValue());
									filtervec.add(filter);
									querry.setFilters(filtervec);
									}
								
								
								
								querry.setQuerryObject(new Customer());
								
								service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
									
									@Override
									public void onFailure(Throwable caught) {
									}

									@Override
									public void onSuccess(ArrayList<SuperModel> result) {
										
										System.out.println(" result size=="+result.size());
										
										if(result.size()==0)
										{
											CustomerTrainingAttendies attendies = new CustomerTrainingAttendies();
											
											
											attendies.setCustCell(form.getCustcellNo().getValue());
											attendies.setCustFirstName(form.getCustFirstName().getValue());
											attendies.setCustLastName(form.getCustLastName().getValue());
											attendies.setCustEmail(form.getCustemailID().getValue());
											
											form.getAttendiestable().getDataprovider().getList().add(attendies);
										
											
											form.getCustcellNo().setValue("");
											form.getCustemailID().setValue("");
											form.getCustFirstName().setValue("");
											form.getCustLastName().setValue("");
										}
										else
										{
											form.showDialogMessage("Customer is already present in the system");
											
										}
									}
									});
								  }	
								}
						else
								{
							form.showDialogMessage("Plese enter atleast one contact information ");
								}
							
							}
						else
							{
							form.showDialogMessage("Plese enter customer last name ");
							}
						}
					else
						{
						form.showDialogMessage("Plese enter customer first name ");
						}
							
		}
		
		
		if(event.getSource().equals(form.getClearTable())){
			form.getAttendiestable().connectToLocal();
		}
		
		
		if(event.getSource().equals(form.addCustByType)){
			
			MyQuerry querry = new MyQuerry();
		  	Company c = new Company();
		  	Vector<Filter> filtervec=new Vector<Filter>();
		  	Filter filter = null;
		  	filter = new Filter();
		  	filter.setQuerryString("companyId");
			filter.setLongValue(c.getCompanyId());
			filtervec.add(filter);
			filter = new Filter();
			filter.setQuerryString("businessProcess.type");
			filter.setStringValue(form.getOlbCustomerType().getValue().trim());
			filtervec.add(filter);
			querry.setFilters(filtervec);
			querry.setQuerryObject(new Customer());
			
			service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onFailure(Throwable caught) {
				}

				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					ArrayList<Customer> customerList=new ArrayList<Customer>();
					System.out.println(" result set size +++++++"+result.size());
					
					if(result.size()==0){
						form.showDialogMessage("Customer for selected type is not available");
					}
					else{
						
					for(SuperModel model:result)
					{
						Customer custEntity=(Customer)model;
						
						customerList.add(custEntity);
					}
					
					
					
					ArrayList<CustomerTrainingAttendies> filteredCustomers=form.retrieveDataForTable(customerList);
					
					form.getAttendiestable().connectToLocal();
					
					List<CustomerTrainingAttendies> parentlistwithNubering= new ArrayList<CustomerTrainingAttendies>(); 
					
					for(int i=0;i<filteredCustomers.size();i++){
						
						CustomerTrainingAttendies attendies = new CustomerTrainingAttendies();
						attendies.setSrNo(i+1);
						attendies.setCustId(filteredCustomers.get(i).getCustId());
						attendies.setCustFirstName(filteredCustomers.get(i).getCustFirstName());
						attendies.setCustLastName(filteredCustomers.get(i).getCustLastName());
						attendies.setCustCell(filteredCustomers.get(i).getCustCell());
						attendies.setCustEmail(filteredCustomers.get(i).getCustEmail());
						
						attendies.setGroup(filteredCustomers.get(i).getGroup());
						attendies.setPriority(filteredCustomers.get(i).getPriority());
						attendies.setType(filteredCustomers.get(i).getType());
						attendies.setCategory(filteredCustomers.get(i).getCategory());
						
						parentlistwithNubering.add(attendies);
					}
					
					form.getAttendiestable().getDataprovider().setList(parentlistwithNubering);
					
					form.getOlbCustomerType().setValue("");
				}
				}
			});
					
		}
		
		
			if(event.getSource().equals(form.addCustByGroup)){
			
			MyQuerry querry = new MyQuerry();
		  	Company c = new Company();
		  	Vector<Filter> filtervec=new Vector<Filter>();
		  	Filter filter = null;
		  	filter = new Filter();
		  	filter.setQuerryString("companyId");
			filter.setLongValue(c.getCompanyId());
			filtervec.add(filter);
			filter = new Filter();
			filter.setQuerryString("businessProcess.group");
			filter.setStringValue(form.olbCustomerGroup.getValue().trim());
			filtervec.add(filter);
			querry.setFilters(filtervec);
			querry.setQuerryObject(new Customer());
			
			service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onFailure(Throwable caught) {
				}

				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					ArrayList<Customer> customerList=new ArrayList<Customer>();
					System.out.println(" result set size +++++++"+result.size());
					
					if(result.size()==0){
						form.showDialogMessage("Customer for selected group is not available");
					}
					else{
					for(SuperModel model:result)
					{
						Customer custEntity=(Customer)model;
						
						customerList.add(custEntity);
					}
					
					
					
					ArrayList<CustomerTrainingAttendies> filteredCustomers=form.retrieveDataForTable(customerList);
					
					form.attendiestable.connectToLocal();
					
					List<CustomerTrainingAttendies> parentlistwithNubering= new ArrayList<CustomerTrainingAttendies>(); 
					
					for(int i=0;i<filteredCustomers.size();i++){
						
						CustomerTrainingAttendies attendies = new CustomerTrainingAttendies();
						attendies.setSrNo(i+1);
						attendies.setCustId(filteredCustomers.get(i).getCustId());
						attendies.setCustFirstName(filteredCustomers.get(i).getCustFirstName());
						attendies.setCustLastName(filteredCustomers.get(i).getCustLastName());
						attendies.setCustCell(filteredCustomers.get(i).getCustCell());
						attendies.setCustEmail(filteredCustomers.get(i).getCustEmail());
						
						attendies.setGroup(filteredCustomers.get(i).getGroup());
						attendies.setPriority(filteredCustomers.get(i).getPriority());
						attendies.setType(filteredCustomers.get(i).getType());
						attendies.setCategory(filteredCustomers.get(i).getCategory());
						
						parentlistwithNubering.add(attendies);
					}
					form.getAttendiestable().getDataprovider().setList(parentlistwithNubering);
					
					form.getOlbCustomerGroup().setValue("");
				}
				}
			});
					
		}
			
			if(event.getSource().equals(form.addCustByPriority)){
				
				MyQuerry querry = new MyQuerry();
			  	Company c = new Company();
			  	Vector<Filter> filtervec=new Vector<Filter>();
			  	Filter filter = null;
			  	filter = new Filter();
			  	filter.setQuerryString("companyId");
				filter.setLongValue(c.getCompanyId());
				filtervec.add(filter);
				filter = new Filter();
				filter.setQuerryString("customerPriority");
				filter.setStringValue(form.olbCustomerPriBox.getValue().trim());
				filtervec.add(filter);
				querry.setFilters(filtervec);
				querry.setQuerryObject(new Customer());
				
				service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
					
					@Override
					public void onFailure(Throwable caught) {
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						
						ArrayList<Customer> customerList=new ArrayList<Customer>();
						System.out.println(" result set size +++++++"+result.size());
						if(result.size()==0){
							form.showDialogMessage("Customer for selected priority is not available");
						}
						else{
						
						for(SuperModel model:result)
						{
							Customer custEntity=(Customer)model;
							
							customerList.add(custEntity);
						}
						
						
						
						ArrayList<CustomerTrainingAttendies> filteredCustomers=form.retrieveDataForTable(customerList);
						
						form.getAttendiestable().connectToLocal();
						
						
						List<CustomerTrainingAttendies> parentlistwithNubering= new ArrayList<CustomerTrainingAttendies>(); 
						
						for(int i=0;i<filteredCustomers.size();i++){
							
							CustomerTrainingAttendies attendies = new CustomerTrainingAttendies();
							attendies.setSrNo(i+1);
							attendies.setCustId(filteredCustomers.get(i).getCustId());
							attendies.setCustFirstName(filteredCustomers.get(i).getCustFirstName());
							attendies.setCustLastName(filteredCustomers.get(i).getCustLastName());
							attendies.setCustCell(filteredCustomers.get(i).getCustCell());
							attendies.setCustEmail(filteredCustomers.get(i).getCustEmail());
							attendies.setGroup(filteredCustomers.get(i).getGroup());
							attendies.setPriority(filteredCustomers.get(i).getPriority());
							attendies.setType(filteredCustomers.get(i).getType());
							attendies.setCategory(filteredCustomers.get(i).getCategory());
							
							parentlistwithNubering.add(attendies);
						}
						
						
						form.getAttendiestable().getDataprovider().setList(parentlistwithNubering);
						
						form.getOlbCustomerPriBox().setValue("");
					}
					}
				});
						
			}
		
		
		
		if(event.getSource().equals(form.addCustByCatergory))
		{
			
			MyQuerry querry = new MyQuerry();
		  	Company c = new Company();
		  	Vector<Filter> filtervec=new Vector<Filter>();
		  	Filter filter = null;
		  	filter = new Filter();
		  	filter.setQuerryString("companyId");
			filter.setLongValue(c.getCompanyId());
			filtervec.add(filter);
			filter = new Filter();
			filter.setQuerryString("businessProcess.category");
			filter.setStringValue(form.getOlbCustomerCategory().getValue().trim());
			filtervec.add(filter);
			querry.setFilters(filtervec);
			querry.setQuerryObject(new Customer());
			
			service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onFailure(Throwable caught) {
				}

				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					ArrayList<Customer> customerList=new ArrayList<Customer>();
					System.out.println(" result set size +++++++"+result.size());
					
					if(result.size()==0){
						form.showDialogMessage("Customer for selected category is not available");
					}
					else{
						
					for(SuperModel model:result)
					{
						Customer custEntity=(Customer)model;
						
						customerList.add(custEntity);
					}
					
					
					
					ArrayList<CustomerTrainingAttendies> filteredCustomers=form.retrieveDataForTable(customerList);
					
					form.getAttendiestable().connectToLocal();
					
					List<CustomerTrainingAttendies> parentlistwithNubering= new ArrayList<CustomerTrainingAttendies>(); 
					
					for(int i=0;i<filteredCustomers.size();i++){
						
						CustomerTrainingAttendies attendies = new CustomerTrainingAttendies();
						attendies.setSrNo(i+1);
						attendies.setCustId(filteredCustomers.get(i).getCustId());
						attendies.setCustFirstName(filteredCustomers.get(i).getCustFirstName());
						attendies.setCustLastName(filteredCustomers.get(i).getCustLastName());
						attendies.setCustCell(filteredCustomers.get(i).getCustCell());
						attendies.setCustEmail(filteredCustomers.get(i).getCustEmail());
						
						attendies.setGroup(filteredCustomers.get(i).getGroup());
						attendies.setPriority(filteredCustomers.get(i).getPriority());
						attendies.setType(filteredCustomers.get(i).getType());
						attendies.setCategory(filteredCustomers.get(i).getCategory());
						
						parentlistwithNubering.add(attendies);
					}
					
					form.getAttendiestable().getDataprovider().setList(parentlistwithNubering);
					
					form.getOlbCustomerCategory().setValue("");
				}
				}
			});
					
					
			
			
		}
	
		
		//*****************vijay **********
		
		final ArrayList<ExcelRecordsList> attendieslist=new ArrayList<ExcelRecordsList>();

		if(event.getSource().equals(form.getAddAttendiesbyexcelbutton())){
			System.out.println("11111111111");
			if(form.uploadexcel.getValue()==null){
				
				System.out.println("Please select file");
				form.showDialogMessage("Please Select File");

			}else if(form.getAttendiestable().getDataprovider().getList().size()!=0){
				
				form.showDialogMessage("Records Aleredy added in the table");
				
			}
			else{
			readingexcelasyc.getAttendiesdetails(model.getCompanyId(),new AsyncCallback<ArrayList<ExcelRecordsList>>(){

				@Override
				public void onFailure(Throwable caught) {
					form.showDialogMessage("An unexpected error occurred. Please try again!");
					
				}

				@Override
				public void onSuccess(ArrayList<ExcelRecordsList> result) {
					// TODO Auto-generated method stub
					
					
					List<CustomerTrainingAttendies> excelDataList = new ArrayList<CustomerTrainingAttendies>();
					
					System.out.println("Hiiiiiiiiiiiiiiiiii 111111");
					if(result.size()!=0)
					{
						System.out.println("Hiiiiiiiiiiiiiiiiii 22222222");

						attendieslist.addAll(result);
						
						System.out.println("Size:"+attendieslist.size());
			            logger.log(Level.SEVERE,"In the trainnigpresentor:"+attendieslist.size());

						
						for(int j=0;j<attendieslist.size();j++){
							System.out.println(attendieslist.get(j).getFirstName());
							System.out.println(attendieslist.get(j).getLastName());
							System.out.println(attendieslist.get(j).getCellNo());
							System.out.println(attendieslist.get(j).getEmailId());
							

						}
						
						for(int p=0; p<attendieslist.size();p++){
							
							CustomerTrainingAttendies attendies = new CustomerTrainingAttendies();
							if(attendieslist.get(p).getFirstName().equalsIgnoreCase("NA")){
							    attendies.setCustFirstName("");
							}
							else{   
								attendies.setCustFirstName(attendieslist.get(p).getFirstName());
							}
							
							if(attendieslist.get(p).getLastName().equalsIgnoreCase("NA")){
								attendies.setCustLastName("");
							}
							else{
								attendies.setCustLastName(attendieslist.get(p).getLastName());
							}
							
							
							if(attendieslist.get(p).getCellNo().equalsIgnoreCase("NA")){
								attendies.setCustCell("");
							}
							else{
								attendies.setCustCell(attendieslist.get(p).getCellNo()+"");
							}	
							
							if(attendieslist.get(p).getEmailId().equalsIgnoreCase("NA")){	
								attendies.setCustEmail("");
							}
							else{
								attendies.setCustEmail(attendieslist.get(p).getEmailId());
							}
							
							
							excelDataList.add(attendies);
							
							
						}
						
						
						form.getAttendiestable().getDataprovider().setList(excelDataList);
						
						
						System.out.println("Hiiiiiiiiiiiiiiiiii Final size"+excelDataList.size());
			            logger.log(Level.SEVERE,"Hiiiiiiiiiiiiiiiiii Final list size"+excelDataList.size());


					}
					
					else{
							System.out.println("In else block");
					}
					
					
				}
				
			});
			
		}
			
			
			
		}
		
	}
	
	
	
	@Override
	public void reactOnPrint() {
	}

	@Override
	public void reactOnEmail() {
		
	}
	
	@Override
	public void reactOnDownload()
	{
			
	}

	
	@Override
	protected void makeNewModel() {
		model=new CustomerTrainingDetails();
	}
	
	
//	public static CustomerFeedbackForm initialize(){
//		
//	
//		
//	}


	public static CustomerTrainingForm initalize() {
		
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Campaign Management",Screen.CUSTOMERTRAINING); 
		CustomerTrainingForm form=new  CustomerTrainingForm();

		CustomerTrainingTableProxy gentable=new CustomerTrainingTableProxy();
		gentable.setView(form);
		gentable.applySelectionModle();
		
		CustomerTrainingSearchPopup.staticSuperTable=gentable;
		CustomerTrainingSearchPopup searchpopup=new CustomerTrainingSearchPopup();
		form.setSearchpopupscreen(searchpopup);

	    CustomerTrainingPresenter  presenter=new CustomerTrainingPresenter(form,new CustomerTrainingDetails());
		AppMemory.getAppMemory().stickPnel(form);
//		form.setMenuAsPerStatus();
//		form.setAppHeaderBarAsPerStatus();
		
		return form;

		
	}


	@Override
	public void onChange(ChangeEvent event) {
		
			 if (form.getPocName().getSelectedIndex()==0){
				 form.getPocCellno().setValue("");
				 form.getPocEmail().setValue("");
			  }
			  else{
				  form.getPocDetails();
			  }
			  
			  
				if(form.getSmsEventList().getSelectedIndex()==0){
					
					System.out.println("in side on change ");
					
					form.getSmsTemplate().setValue("");
				}
	}

	
	

}
