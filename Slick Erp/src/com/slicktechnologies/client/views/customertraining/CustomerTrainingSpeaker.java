package com.slicktechnologies.client.views.customertraining;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Embed;

@Embed
public class CustomerTrainingSpeaker implements Serializable{
	
	
		
	private static final long serialVersionUID = -750565591257893527L;
	
	
		int srNo;
		String speakerName;
		Long speakercellno;
		String speakerEmail;
		String speakerTitle;
		
		//**********************getters and setters ***********************************

		

		public String getSpeakerTitle() {
			return speakerTitle;
		}


		public void setSpeakerTitle(String speakerTitle) {
			this.speakerTitle = speakerTitle;
		}


		public Long getSpeakercellno() {
			return speakercellno;
		}


		public void setSpeakercellno(Long speakercellno) {
			this.speakercellno = speakercellno;
		}


		public String getSpeakerEmail() {
			return speakerEmail;
		}


		public void setSpeakerEmail(String speakerEmail) {
			this.speakerEmail = speakerEmail;
		}


		public String getSpeakerName() {
			return speakerName;
		}


		public void setSpeakerName(String speakerName) {
			this.speakerName = speakerName;
		}


		public int getSrNo() {
			return srNo;
		}


		public void setSrNo(int srNo) {
			this.srNo = srNo;
		}
		

}
