package com.slicktechnologies.client.views.customertraining;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Embed;

@Embed
public class CustomerTrainingAttendies implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6559100742714362896L;
	
	int srNo;
	int custId;
	String custFirstName;
	String custLastName;
	String custCell;
	String custEmail;
	
	String type;
	String category;
	String group;
	String priority;
	
	
	//******************************getters and setters ***************************
	
	
	public int getSrNo() {
		return srNo;
	}
	public void setSrNo(int srNo) {
		this.srNo = srNo;
	}
	public int getCustId() {
		return custId;
	}
	public void setCustId(int custId) {
		this.custId = custId;
	}
	public String getCustCell() {
		return custCell;
	}
	public void setCustCell(String custCell) {
		this.custCell = custCell;
	}
	public String getCustEmail() {
		return custEmail;
	}
	public void setCustEmail(String custEmail) {
		this.custEmail = custEmail;
	}
	public String getCustFirstName() {
		return custFirstName;
	}
	public void setCustFirstName(String custFirstName) {
		this.custFirstName = custFirstName;
	}
	public String getCustLastName() {
		return custLastName;
	}
	public void setCustLastName(String custLastName) {
		this.custLastName = custLastName;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public String getPriority() {
		return priority;
	}
	public void setPriority(String priority) {
		this.priority = priority;
	}
	
	
	
}
