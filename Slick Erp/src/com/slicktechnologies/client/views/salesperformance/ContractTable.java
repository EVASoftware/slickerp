package com.slicktechnologies.client.views.salesperformance;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;

public class ContractTable extends SuperTable<SalesPersonValue>{

	TextColumn<SalesPersonValue> getColumnCustomerId;
	TextColumn<SalesPersonValue> getColumnCustomerName;
	TextColumn<SalesPersonValue> getColumnCustomerCell;
	TextColumn<SalesPersonValue> getColumncontractId;
	TextColumn<SalesPersonValue> getColumncontractDate;
	TextColumn<SalesPersonValue> getColumncontractStatus;
	TextColumn<SalesPersonValue> getColumnBranch;
	
	
	@Override
	public void createTable() {
		addColumnCustomerId();
		addColumnCustomerName();
		addColumnCustomerCell();
		addColumnId();
		addColumnDate();
		addColumnBranch();
		addColumnStatus();
	}

	
	
	private void addColumnCustomerId() {
		getColumnCustomerId = new TextColumn<SalesPersonValue>() {
			
			@Override
			public String getValue(SalesPersonValue object) {
				 return object.getContractcustomerId()+"";
			}
		};
		table.addColumn(getColumnCustomerId,"Customer ID");
		table.setColumnWidth(getColumnCustomerId, 70,Unit.PX);
	}



	private void addColumnCustomerName() {
		getColumnCustomerName = new TextColumn<SalesPersonValue>() {
			
			@Override
			public String getValue(SalesPersonValue object) {
				return object.getContractcustomerName();
			}
		};
		table.addColumn(getColumnCustomerName,"Customer Name");
		table.setColumnWidth(getColumnCustomerName, 70,Unit.PX);
	}



	private void addColumnCustomerCell() {
		getColumnCustomerCell = new TextColumn<SalesPersonValue>() {
			
			@Override
			public String getValue(SalesPersonValue object) {
				return object.getContractcustomerCell()+"";
			}
		};
		table.addColumn(getColumnCustomerCell,"Customer Cell");
		table.setColumnWidth(getColumnCustomerCell, 70,Unit.PX);
	}



	private void addColumnId() {
		getColumncontractId = new TextColumn<SalesPersonValue>() {
			
			@Override
			public String getValue(SalesPersonValue object) {
				if(object.getContractId()!=0){
					return object.getContractId()+"";
				}else{
					return "";
				}
				
			}
		};
		table.addColumn(getColumncontractId,"Contract Id");
		table.setColumnWidth(getColumncontractId, 70,Unit.PX);
	}



	private void addColumnDate() {
		getColumncontractDate = new TextColumn<SalesPersonValue>() {
			
			@Override
			public String getValue(SalesPersonValue object) {
				if(object.getContractDate()!=null){
					return AppUtility.parseDate(object.getContractDate());
				}else{
					return "";
				}
			}
		};
		table.addColumn(getColumncontractDate,"Contract Date");
		table.setColumnWidth(getColumncontractDate, 70,Unit.PX);
	}



	private void addColumnStatus() {
		getColumncontractStatus = new TextColumn<SalesPersonValue>() {
			
			@Override
			public String getValue(SalesPersonValue object) {
				if(object.getContractStatus()!=null){
					return object.getContractStatus();
				}else{
					return "";
				}
				
			}
		};
		table.addColumn(getColumncontractStatus,"Status");
		table.setColumnWidth(getColumncontractStatus, 70,Unit.PX);
	}



	private void addColumnBranch() {
		getColumnBranch = new TextColumn<SalesPersonValue>() {
			
			@Override
			public String getValue(SalesPersonValue object) {
				return object.getContractBranch();
			}
		};
		table.addColumn(getColumnBranch,"Branch");
		table.setColumnWidth(getColumnBranch, 80,Unit.PX);		
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
