package com.slicktechnologies.client.views.salesperformance;

import java.io.Serializable;
import java.util.Date;

import com.googlecode.objectify.annotation.Embed;

@Embed
public class SalesPersonValue implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6339934860315255362L;
	
	/********************************* Attributes ***************************/
	
	//  for lead informations
	int leadcustomerId;
	String leadcustomerName;
	long leadcustomerCell;
	int leadId;
	Date leadCreationDate;
	String leadTitle;
	String leadStatus;
	String leadBranch;
	
	// for Quotation information
	
	int quotationcustomerId;
	String quotationcustomerName;
	long quotationcustomerCell;
	int quotationId;
	Date quotationDate;
	String quotationStatus;
	String quotationBranch;
	
	// for contract information
	int contractcustomerId;
	String contractcustomerName;
	long contractcustomerCell;
	int contractId;
	Date contractDate;
	String contractStatus;
	String contractBranch;
	
	
	
	/************************ getters and setters ***************************/
	
	public int getLeadId() {
		return leadId;
	}
	public void setLeadId(int leadId) {
		this.leadId = leadId;
	}
	public Date getLeadCreationDate() {
		return leadCreationDate;
	}
	public void setLeadCreationDate(Date leadCreationDate) {
		this.leadCreationDate = leadCreationDate;
	}
	public String getLeadTitle() {
		return leadTitle;
	}
	public void setLeadTitle(String leadTitle) {
		this.leadTitle = leadTitle;
	}
	public String getLeadStatus() {
		return leadStatus;
	}
	public void setLeadStatus(String leadStatus) {
		this.leadStatus = leadStatus;
	}
	public String getLeadBranch() {
		return leadBranch;
	}
	public void setLeadBranch(String leadBranch) {
		this.leadBranch = leadBranch;
	}
	public int getLeadcustomerId() {
		return leadcustomerId;
	}
	public void setLeadcustomerId(int leadcustomerId) {
		this.leadcustomerId = leadcustomerId;
	}
	public String getLeadcustomerName() {
		return leadcustomerName;
	}
	public void setLeadcustomerName(String leadcustomerName) {
		this.leadcustomerName = leadcustomerName;
	}
	public long getLeadcustomerCell() {
		return leadcustomerCell;
	}
	public void setLeadcustomerCell(long leadcustomerCell) {
		this.leadcustomerCell = leadcustomerCell;
	}
	
	
	
	
	public int getQuotationId() {
		return quotationId;
	}
	public void setQuotationId(int quotationId) {
		this.quotationId = quotationId;
	}
	public Date getQuotationDate() {
		return quotationDate;
	}
	public void setQuotationDate(Date quotationDate) {
		this.quotationDate = quotationDate;
	}
	public String getQuotationStatus() {
		return quotationStatus;
	}
	public void setQuotationStatus(String quotationStatus) {
		this.quotationStatus = quotationStatus;
	}
	public int getQuotationcustomerId() {
		return quotationcustomerId;
	}
	public void setQuotationcustomerId(int quotationcustomerId) {
		this.quotationcustomerId = quotationcustomerId;
	}
	public String getQuotationcustomerName() {
		return quotationcustomerName;
	}
	public void setQuotationcustomerName(String quotationcustomerName) {
		this.quotationcustomerName = quotationcustomerName;
	}
	public long getQuotationcustomerCell() {
		return quotationcustomerCell;
	}
	public void setQuotationcustomerCell(long quotationcustomerCell) {
		this.quotationcustomerCell = quotationcustomerCell;
	}
	public String getQuotationBranch() {
		return quotationBranch;
	}
	public void setQuotationBranch(String quotationBranch) {
		this.quotationBranch = quotationBranch;
	}
	
	
	
	
	public int getContractId() {
		return contractId;
	}
	public void setContractId(int contractId) {
		this.contractId = contractId;
	}
	public Date getContractDate() {
		return contractDate;
	}
	public void setContractDate(Date contractDate) {
		this.contractDate = contractDate;
	}
	public String getContractStatus() {
		return contractStatus;
	}
	public void setContractStatus(String contractStatus) {
		this.contractStatus = contractStatus;
	}
	public int getContractcustomerId() {
		return contractcustomerId;
	}
	public void setContractcustomerId(int contractcustomerId) {
		this.contractcustomerId = contractcustomerId;
	}
	public String getContractcustomerName() {
		return contractcustomerName;
	}
	public void setContractcustomerName(String contractcustomerName) {
		this.contractcustomerName = contractcustomerName;
	}
	public String getContractBranch() {
		return contractBranch;
	}
	public void setContractBranch(String contractBranch) {
		this.contractBranch = contractBranch;
	}
	public long getContractcustomerCell() {
		return contractcustomerCell;
	}
	public void setContractcustomerCell(long contractcustomerCell) {
		this.contractcustomerCell = contractcustomerCell;
	}

	
	
	

}
