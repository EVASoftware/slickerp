package com.slicktechnologies.client.views.salesperformance;


import java.util.Comparator;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.Service;

public class LeadTable extends SuperTable<SalesPersonValue>{
	
	TextColumn<SalesPersonValue> getColumnCustomerId;
	TextColumn<SalesPersonValue> getColumnCustomerName;
	TextColumn<SalesPersonValue> getColumnCustomerCell;
	TextColumn<SalesPersonValue> getColumnleadId;
	TextColumn<SalesPersonValue> getColumnleadCreationDate;
	TextColumn<SalesPersonValue> getColumnleadTitle;
	TextColumn<SalesPersonValue> getColumnleadStatus;
	TextColumn<SalesPersonValue> getColumnBranch;
	

	
	@Override
	public void createTable() {
		
		addColumnCustomerId();
		addColumnCustomerName();
		addColumnCustomerCell();
		addColumnLeadId();
		addColumnLeadDate();
		addColumnLeadTitle();
		addcolumnBranch();
		addColumnLeadStatus();
		
		
	}
	
	
	

	private void addColumnCustomerId() {
		getColumnCustomerId = new  TextColumn<SalesPersonValue>() {
			
			@Override
			public String getValue(SalesPersonValue object) {
				return object.getLeadcustomerId()+"";
			}
		};
		
		table.addColumn(getColumnCustomerId,"Customer Id");
		table.setColumnWidth(getColumnCustomerId, 80,Unit.PX);
		getColumnCustomerId.setSortable(true);
	}




	private void addColumnCustomerName() {
		getColumnCustomerName = new  TextColumn<SalesPersonValue>() {
			
			@Override
			public String getValue(SalesPersonValue object) {
				return object.getLeadcustomerName();
			}
		};
		table.addColumn(getColumnCustomerName,"Customer Name");
		table.setColumnWidth(getColumnCustomerName, 80,Unit.PX);
		getColumnCustomerName.setSortable(true);
	}




	private void addColumnCustomerCell() {
		getColumnCustomerCell = new TextColumn<SalesPersonValue>() {
			
			@Override
			public String getValue(SalesPersonValue object) {
				return object.getLeadcustomerCell()+"";
			}
		};
		table.addColumn(getColumnCustomerCell,"Customer Cell");
		table.setColumnWidth(getColumnCustomerCell, 80,Unit.PX);
		getColumnCustomerCell.setSortable(true);
	}




	private void addcolumnBranch() {
		getColumnBranch = new TextColumn<SalesPersonValue>() {
			
			@Override
			public String getValue(SalesPersonValue object) {
				return object.getLeadBranch();
			}
		};
		
		table.addColumn(getColumnBranch,"Branch");
		table.setColumnWidth(getColumnBranch, 80,Unit.PX);
		getColumnBranch.setSortable(true);
	}




	private void addColumnLeadId() {
		getColumnleadId = new TextColumn<SalesPersonValue>() {
			
			@Override
			public String getValue(SalesPersonValue object) {
				return object.getLeadId()+"";
			}
		};
		table.addColumn(getColumnleadId,"Lead Id");
		table.setColumnWidth(getColumnleadId, 80,Unit.PX);
		getColumnleadId.setSortable(true);
	}




	private void addColumnLeadDate() {
		getColumnleadCreationDate = new TextColumn<SalesPersonValue>() {
			
			@Override
			public String getValue(SalesPersonValue object) {
				if(object.getLeadCreationDate()!=null){
					return AppUtility.parseDate(object.getLeadCreationDate());
				}else{
					return "";
				}
			}
		};
		
		table.addColumn(getColumnleadCreationDate,"Lead Date");
		table.setColumnWidth(getColumnleadCreationDate, 80,Unit.PX);
		getColumnleadCreationDate.setSortable(true);
	}




	private void addColumnLeadTitle() {
		getColumnleadTitle = new TextColumn<SalesPersonValue>() {
			
			@Override
			public String getValue(SalesPersonValue object) {
				return object.getLeadTitle();
			}
		};
		
		table.addColumn(getColumnleadTitle,"Title");
		table.setColumnWidth(getColumnleadTitle, 80,Unit.PX);
		getColumnleadTitle.setSortable(true);
	}

	private void addColumnLeadStatus() {
	
		getColumnleadStatus = new TextColumn<SalesPersonValue>() {
			
			@Override
			public String getValue(SalesPersonValue object) {
				return object.getLeadStatus();
			}
		};
		
		table.addColumn(getColumnleadStatus,"Status");
		table.setColumnWidth(getColumnleadStatus, 80,Unit.PX);
		getColumnleadStatus.setSortable(true);
	}
	


	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}




	

}
