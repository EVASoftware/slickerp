package com.slicktechnologies.client.views.salesperformance;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;


import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.google.gwt.user.client.ui.ValueBoxBase.TextAlignment;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.common.DummyEntityOnlyForScreen;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.salesperson.SalesPersonTargets;
import com.slicktechnologies.shared.common.salesperson.TargetInformation;

public class SalesPerformancePresentor extends FormScreenPresenter<DummyEntityOnlyForScreen> implements SelectionHandler<Suggestion>,ClickHandler{

	SalesPerformanceForm form;
	
	final GenricServiceAsync async =GWT.create(GenricService.class);
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	NumberFormat numformat = NumberFormat.getFormat("0.00");
	
	public SalesPerformancePresentor(UiScreen<DummyEntityOnlyForScreen> view,DummyEntityOnlyForScreen model) {
		super(view, model);
		form = (SalesPerformanceForm) view;
		form.employeeInfo.getId().addSelectionHandler(this);
		form.employeeInfo.getName().addSelectionHandler(this);
		form.employeeInfo.getPhone().addSelectionHandler(this);
		form.btnGo.addClickHandler(this);
		
		Date date= new Date();
		int month = date.getMonth();
		form.dbToDate.setValue(calulateEndDateOfMonth(month));
		form.dbFromDate.setValue(calulateStartDateOfMonth(month));
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.SALESPERSONDASHBOARD,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}

	public static void initalize()
	{
		
		SalesPerformanceForm homeform = new SalesPerformanceForm();
		AppMemory.getAppMemory().currentScreen = Screen.SALESPERSONDASHBOARD;
		SalesPerformancePresentor presentor = new SalesPerformancePresentor(homeform, new DummyEntityOnlyForScreen());
		AppMemory.getAppMemory().stickPnel(homeform);
		
		
	}
	
	
	public Date calulateEndDateOfMonth(int selectedMonth){
		DateTimeFormat fmt = DateTimeFormat.getFormat("yyyy-MM-dd");
		System.out.println("Inside End Month ");
		System.out.println("Selected Month "+selectedMonth);
		String endDate;
		
		Date date = new Date();
		
		date.setMonth(selectedMonth);
		CalendarUtil.setToFirstDayOfMonth(date);
		CalendarUtil.addDaysToDate(date, -1);
		endDate=fmt.format(date);
		Console.log("END DATE OF MONTH "+endDate);
		System.out.println(" end date =="+endDate);
		
		 Date lastmonthendDate = fmt.parse(endDate);
		 System.out.println(" End date =="+lastmonthendDate);
		return lastmonthendDate;
	
	}
	
	public Date calulateStartDateOfMonth(int selectedMonth){
		DateTimeFormat fmt = DateTimeFormat.getFormat("yyyy-MM-dd");
		System.out.println("Inside Start Month ");
		System.out.println("Selected Month "+selectedMonth);
		String startDate;
		
		Date date = new Date();
		date.setDate(1);
		date.setMonth(selectedMonth-1);
		
		startDate=fmt.format(date);
		
		System.out.println("Start Date Of Month "+startDate);
		Console.log("START DATE OF MONTH "+startDate);
		Date lastmonthstartDate = fmt.parse(startDate);
		System.out.println(" Date =="+lastmonthstartDate);
		return lastmonthstartDate;
	
	}
	
	
	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onClick(ClickEvent event) {
		super.onClick(event);
		
		if(event.getSource()== form.btnGo){
			System.out.println(" inside go btn");
			getAllDetails(form.dbFromDate.getValue(),form.dbToDate.getValue());
		}
		
	}


	private void getAllDetails(Date fromdate, Date todate) {
		System.out.println(" employee name =="+form.employeeInfo.getEmployeeName());
		
		if(fromdate!=null && todate!=null && !form.employeeInfo.getEmployeeName().equals("")){
			
			form.getLeadTable().clear();
			form.getTbsuccessLead().setValue("");
			form.getTbsuccessleadper().setValue("");
			form.getTbunsuccessLead().setValue("");
			form.getTbunsuccessleadper().setValue("");
			form.getTbinprogLead().setValue("");
			form.getTbinprogLeadper().setValue("");
			getleadDetails(fromdate,todate,form.employeeInfo.getEmployeeName());
			
			form.getQuotationTable().clear();
			form.getTbsuccessQuotation().setValue("");
			form.getTbsuccessquotationper().setValue("");
			form.getTbunsuccessQuotation().setValue("");
			form.getTbunsuccessquotationper().setValue("");
			form.getTbinprogQuotation().setValue("");
			form.getTbinprogQuotationper().setValue("");
			getquotationDetails(fromdate,todate,form.employeeInfo.getEmployeeName());
			
			form.getContractTable().clear();
			form.getTbsuccessContract().setValue("");
			form.getTbsuccesscontractper().setValue("");
			form.getTbinprogContract().setValue("");
			form.getTbinprogContractper().setValue("");
			getcontractdetails(fromdate,todate,form.employeeInfo.getEmployeeName());
		}
		else if(form.employeeInfo.getEmployeeName().equals("")){
			form.showDialogMessage("please select employee");
		}
		else{
			form.showDialogMessage("please select date");
		}
	}

	private void getcontractdetails(Date fromdate, Date todate,	String employeeName) {

		MyQuerry querry = new MyQuerry();
		
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter=null;
		
		
		filter = new Filter();
		filter.setQuerryString("employee");
		filter.setStringValue(employeeName);
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("contractDate >=");
		filter.setDateValue(fromdate);
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("contractDate <=");
		filter.setDateValue(todate);
		filtervec.add(filter);
		
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Contract());
		
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {

				System.out.println("Contract result size =="+result.size());
				List<Contract> list = new ArrayList<Contract>();
				form.getContractTable().clear();
				
				if(result.size()!=0){
					List<SalesPersonValue> contractlist = new ArrayList<SalesPersonValue>();
					
					for(SuperModel model : result){
						
						Contract contract =  (Contract) model;
						list.add(contract);
					}
					
					for(int i=0;i<list.size();i++){
						
						SalesPersonValue contract = new  SalesPersonValue();
						contract.setContractcustomerId(list.get(i).getCinfo().getCount());
						contract.setContractcustomerName(list.get(i).getCinfo().getFullName());
						contract.setContractcustomerCell(list.get(i).getCinfo().getCellNumber());
						contract.setContractId(list.get(i).getCount());
						contract.setContractDate(list.get(i).getContractDate());
						contract.setContractBranch(list.get(i).getBranch());
						contract.setContractStatus(list.get(i).getStatus());
						contractlist.add(contract);
					}
					System.out.println("contractlist list size === "+contractlist.size());
					
					form.getContractTable().connectToLocal();
					form.getContractTable().getDataprovider().setList(contractlist);
					
					if(contractlist.size()!=0)
						getallContractDetails(contractlist);
					
				}
			
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
	}

	private void getquotationDetails(Date fromdate, Date todate,String employeeName) {

		MyQuerry querry = new MyQuerry();
		
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter=null;
		
		
		filter = new Filter();
		filter.setQuerryString("employee");
		filter.setStringValue(employeeName);
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("quotationDate >=");
		filter.setDateValue(fromdate);
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("quotationDate <=");
		filter.setDateValue(todate);
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Quotation());
		
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {

				System.out.println("Quotation result size =="+result.size());
				List<Quotation> list = new ArrayList<Quotation>();
				form.getQuotationTable().clear();
				if(result.size()!=0){
					List<SalesPersonValue> quotationlist = new ArrayList<SalesPersonValue>();
					
					for(SuperModel model : result){
						
						Quotation quote = (Quotation) model;
						list.add(quote);
					}
					
					for(int i=0;i<list.size();i++){
						
						SalesPersonValue quotation = new  SalesPersonValue();
						quotation.setQuotationcustomerId(list.get(i).getCinfo().getCount());
						quotation.setQuotationcustomerName(list.get(i).getCinfo().getFullName());
						quotation.setQuotationcustomerCell(list.get(i).getCinfo().getCellNumber());
						quotation.setQuotationId(list.get(i).getCount());
						quotation.setQuotationDate(list.get(i).getQuotationDate());
						quotation.setQuotationBranch(list.get(i).getBranch());
						quotation.setQuotationStatus(list.get(i).getStatus());
						quotationlist.add(quotation);
					}
					System.out.println("Quotation list size === "+quotationlist.size());
					
					form.getQuotationTable().connectToLocal();
					form.getQuotationTable().getDataprovider().setList(quotationlist);
					
					if(quotationlist.size()!=0)
						getallQuotationDetails(quotationlist);
					
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
	}

	private void getleadDetails(Date fromdate, Date todate, String employeeName) {

		MyQuerry querry = new MyQuerry();
		
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter=null;
		
		
		filter = new Filter();
		filter.setQuerryString("employee");
		filter.setStringValue(employeeName);
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("creationDate >=");
		filter.setDateValue(fromdate);
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("creationDate <=");
		filter.setDateValue(todate);
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Lead());
		
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				List<Lead> list = new ArrayList<Lead>();
				System.out.println(" result == "+result.size());
				form.getLeadTable().clear();
				if(result.size()!=0){
					List<SalesPersonValue> leadlist = new ArrayList<SalesPersonValue>();
					
					for(SuperModel model :result){
						
						Lead lead = (Lead) model;
						list.add(lead);
					}
					
					for(int i=0;i<list.size();i++){
						
						SalesPersonValue leadinfo = new SalesPersonValue();
						leadinfo.setLeadcustomerId(list.get(i).getPersonInfo().getCount());
						leadinfo.setLeadcustomerName(list.get(i).getPersonInfo().getFullName());
						leadinfo.setLeadcustomerCell(list.get(i).getPersonInfo().getCellNumber());
						leadinfo.setLeadId(list.get(i).getCount());
						leadinfo.setLeadCreationDate(list.get(i).getCreationDate());
						leadinfo.setLeadTitle(list.get(i).getTitle());
						leadinfo.setLeadBranch(list.get(i).getBranch());
						leadinfo.setLeadStatus(list.get(i).getStatus());
						leadlist.add(leadinfo);
					}
					
					System.out.println("new Lead list size ==== "+leadlist.size());
					form.getLeadTable().connectToLocal();
					form.getLeadTable().getDataprovider().setList(leadlist);
					
					if(list.size()!=0){
						getleadAllpercentage(leadlist);
					}
					
					
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
	}

	


	protected void getallContractDetails(List<SalesPersonValue> contractlist) {

		List<SalesPersonValue> contractsuccessfulllist = new ArrayList<SalesPersonValue>();
		List<SalesPersonValue> contractInprogressfulllist = new ArrayList<SalesPersonValue>();
		
		for(int i=0;i<contractlist.size();i++ )
		{
			if(contractlist.get(i).getContractStatus().equalsIgnoreCase("Approved")){
				contractsuccessfulllist.add(contractlist.get(i));
			}
			else if(!contractlist.get(i).getContractStatus().equalsIgnoreCase("Cancelled")){
				contractInprogressfulllist.add(contractlist.get(i));
			}
		}
		
		System.out.println("final successfull lead size =="+contractsuccessfulllist.size());
		System.out.println(" total lead size ==="+contractlist.size());
		
		double successfullContract = contractsuccessfulllist.size();
		double totalContracts = contractlist.size();
		double contractsuccesspercent = successfullContract/totalContracts*100;
		System.out.println(" percentage ===== "+contractsuccesspercent);
		String successpercent = numformat.format(contractsuccesspercent)+"%";
		form.getTbsuccessContract().setValue(contractsuccessfulllist.size()+"");
		form.getTbsuccesscontractper().setValue(successpercent);
		
		double inprogresscontract = contractInprogressfulllist.size();
		double contractInProgresspercent = inprogresscontract/totalContracts*100;
		String inprogressper = numformat.format(contractInProgresspercent)+"%";
		form.getTbinprogContract().setValue(contractInprogressfulllist.size()+"");
		form.getTbinprogContractper().setValue(inprogressper);
		
	
	
	}


	protected void getallQuotationDetails(List<SalesPersonValue> quotlist) {
		
		
		List<SalesPersonValue> quotesuccessfulllist = new ArrayList<SalesPersonValue>();
		List<SalesPersonValue> quoteUnsuccessfulllist = new ArrayList<SalesPersonValue>();
		List<SalesPersonValue> qouteInprogressfulllist = new ArrayList<SalesPersonValue>();
		
		for(int i=0;i<quotlist.size();i++ )
		{
			if(quotlist.get(i).getQuotationStatus().equalsIgnoreCase("Successful")){
				quotesuccessfulllist.add(quotlist.get(i));
			}
			else if(quotlist.get(i).getQuotationStatus().equalsIgnoreCase("UnSuccessful")){
				quoteUnsuccessfulllist.add(quotlist.get(i));
			}
			else{
				qouteInprogressfulllist.add(quotlist.get(i));
			}
		}
		
		System.out.println("final successfull lead size =="+quotesuccessfulllist.size());
		System.out.println(" total lead size ==="+quotlist.size());
		
		double successfullquote = quotesuccessfulllist.size();
		double totalquotetions = quotlist.size();
		double qoutesuccesspercent = successfullquote/totalquotetions*100;
		System.out.println(" percentage ===== "+qoutesuccesspercent);
		String successpercent = numformat.format(qoutesuccesspercent)+"%";
		form.getTbsuccessQuotation().setValue(quotesuccessfulllist.size()+"");
		form.getTbsuccessQuotation().setAlignment(TextAlignment.CENTER);
		form.getTbsuccessquotationper().setValue(successpercent);
		
		double unsuccessfulquote = quoteUnsuccessfulllist.size();
		double quoteUnsuccesspercent = unsuccessfulquote/totalquotetions*100;
		String unsuccesspercent = numformat.format(quoteUnsuccesspercent)+"%";
		form.getTbunsuccessQuotation().setValue(quoteUnsuccessfulllist.size()+"");
		form.getTbunsuccessQuotation().setAlignment(TextAlignment.CENTER);
		form.getTbunsuccessquotationper().setValue(unsuccesspercent);
		
		double inprogressquote = qouteInprogressfulllist.size();
		double quoteInProgresspercent = inprogressquote/totalquotetions*100;
		String inprogresspercent = numformat.format(quoteInProgresspercent)+"%";
		form.getTbinprogQuotation().setValue(qouteInprogressfulllist.size()+"");
		form.getTbinprogQuotation().setAlignment(TextAlignment.CENTER);
		form.getTbinprogQuotationper().setValue(inprogresspercent);
		
	
	}


	protected void getleadAllpercentage(List<SalesPersonValue> list) {
		
		System.out.println("in lead percenatge ==");
		
		List<SalesPersonValue> leadsuccessfulllist = new ArrayList<SalesPersonValue>();
		List<SalesPersonValue> leadUnsuccessfulllist = new ArrayList<SalesPersonValue>();
		List<SalesPersonValue> leadInprogressfulllist = new ArrayList<SalesPersonValue>();
		
		for(int i=0;i<list.size();i++ )
		{
			if(list.get(i).getLeadStatus().equalsIgnoreCase("Successful")){
				leadsuccessfulllist.add(list.get(i));
			}
			else if(list.get(i).getLeadStatus().equalsIgnoreCase("UnSuccessful")){
				leadUnsuccessfulllist.add(list.get(i));
			}
			else if(!list.get(i).getLeadStatus().equalsIgnoreCase("cancelled")){
				leadInprogressfulllist.add(list.get(i));
			}
		}
		
		System.out.println("final successfull lead size =="+leadsuccessfulllist.size());
		System.out.println(" total lead size ==="+list.size());
		
		double successfull = leadsuccessfulllist.size();
		double totallead = list.size();
		double leadsuccesspercent = successfull/totallead*100;
		System.out.println(" percentage ===== "+leadsuccesspercent);
		String successper = numformat.format(leadsuccesspercent)+"%";
		System.out.println("success lead per "+successper);
		form.getTbsuccessLead().setValue(leadsuccessfulllist.size()+"");
		form.getTbsuccessLead().setAlignment(TextAlignment.CENTER);
		form.getTbsuccessleadper().setValue(successper);
		
		double unsuccessfulllead = leadUnsuccessfulllist.size();
		double leadUnsuccesspercent = unsuccessfulllead/totallead*100;
		String unsuccessper = numformat.format(leadUnsuccesspercent)+"%";
		form.getTbunsuccessLead().setValue(leadUnsuccessfulllist.size()+"");
		form.getTbunsuccessLead().setAlignment(TextAlignment.CENTER);
		form.getTbunsuccessleadper().setValue(unsuccessper);
		
		double inprogresslead = leadInprogressfulllist.size();
		double leadInProgresspercent = inprogresslead/totallead*100;
		String inprogressper = numformat.format(leadInProgresspercent)+"%";
		form.getTbinprogLead().setValue(leadInprogressfulllist.size()+"");
		form.getTbinprogLead().setAlignment(TextAlignment.CENTER);
		form.getTbinprogLeadper().setValue(inprogressper);

		
	}
	
	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void makeNewModel() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onSelection(SelectionEvent<Suggestion> event) {

	}

	@Override
	public void reactOnDownload() {
		System.out.println(" inside download");
		
		ArrayList<SalesPersonValue> leadreportArray = new ArrayList<SalesPersonValue>();
		List<SalesPersonValue>	list = form.leadTable.getDataprovider().getList();
		
		for(int i=0;i<list.size();i++){
			SalesPersonValue leadinfo = new SalesPersonValue();
			leadinfo.setLeadcustomerId(list.get(i).getLeadcustomerId());
			leadinfo.setLeadcustomerName(list.get(i).getLeadcustomerName());
			leadinfo.setLeadcustomerCell(list.get(i).getLeadcustomerCell());
			leadinfo.setLeadId(list.get(i).getLeadId());
			leadinfo.setLeadCreationDate(list.get(i).getLeadCreationDate());
			leadinfo.setLeadTitle(list.get(i).getLeadTitle());
			leadinfo.setLeadBranch(list.get(i).getLeadBranch());
			leadinfo.setLeadStatus(list.get(i).getLeadBranch());
			leadreportArray.add(leadinfo);
			
		}
		
		ArrayList<SalesPersonValue> quotationArray = new ArrayList<SalesPersonValue>();
		List<SalesPersonValue> quoationlist =form.quotationTable.getDataprovider().getList();
		
		for(int i=0;i<quoationlist.size();i++){
			SalesPersonValue quotationinfo = new SalesPersonValue();
			quotationinfo.setQuotationcustomerId(quoationlist.get(i).getQuotationcustomerId());
			quotationinfo.setQuotationcustomerName(quoationlist.get(i).getQuotationcustomerName());
			quotationinfo.setQuotationcustomerCell(quoationlist.get(i).getQuotationcustomerCell());
			quotationinfo.setQuotationId(quoationlist.get(i).getQuotationId());
			quotationinfo.setQuotationDate(quoationlist.get(i).getQuotationDate());
			quotationinfo.setQuotationBranch(quoationlist.get(i).getQuotationBranch());
			quotationinfo.setQuotationStatus(quoationlist.get(i).getQuotationStatus());
			quotationArray.add(quotationinfo);
			
		}
		
		ArrayList<SalesPersonValue> contractArray = new ArrayList<SalesPersonValue>();
		List<SalesPersonValue> contractlist =form.contractTable.getDataprovider().getList();
		
		for(int i=0;i<contractlist.size();i++){
			SalesPersonValue contractinfo = new SalesPersonValue();
			contractinfo.setContractcustomerId(contractlist.get(i).getContractcustomerId());
			contractinfo.setContractcustomerName(contractlist.get(i).getContractcustomerName());
			contractinfo.setContractcustomerCell(contractlist.get(i).getContractcustomerCell());
			contractinfo.setContractId(contractlist.get(i).getContractId());
			contractinfo.setContractDate(contractlist.get(i).getContractDate());
			contractinfo.setContractBranch(contractlist.get(i).getContractBranch());
			contractinfo.setContractStatus(contractlist.get(i).getContractStatus());
			contractArray.add(contractinfo);
		}
		
		ArrayList<String> summarylist = new ArrayList<String>();
		summarylist.add(form.tbsuccessLead.getValue());
		summarylist.add(form.tbsuccessleadper.getValue());
		summarylist.add(form.tbsuccessQuotation.getValue());
		summarylist.add(form.tbsuccessquotationper.getValue());
		summarylist.add(form.tbsuccessContract.getValue());
		summarylist.add(form.tbsuccesscontractper.getValue());
		summarylist.add(form.tbunsuccessLead.getValue());
		summarylist.add(form.tbunsuccessleadper.getValue());
		summarylist.add(form.tbunsuccessQuotation.getValue());
		summarylist.add(form.tbunsuccessquotationper.getValue());
		summarylist.add(form.tbinprogLead.getValue());
		summarylist.add(form.tbinprogLeadper.getValue());
		summarylist.add(form.tbinprogQuotation.getValue());
		summarylist.add(form.tbinprogQuotationper.getValue());
		summarylist.add(form.tbinprogContract.getValue());
		summarylist.add(form.tbinprogContractper.getValue());
		
		
		ArrayList<String> personinfowithDate = new ArrayList<String>();
		personinfowithDate.add(form.employeeInfo.getEmployeeId()+"");
		personinfowithDate.add(form.employeeInfo.getEmployeeName());
		personinfowithDate.add(form.employeeInfo.getCellNumber()+"");
		personinfowithDate.add(AppUtility.parseDate(form.dbFromDate.getValue()));
		personinfowithDate.add(AppUtility.parseDate(form.dbToDate.getValue()));
		
		csvservice.setSalesPersonPerformanceReport(personinfowithDate,summarylist, leadreportArray, quotationArray, contractArray, new AsyncCallback<Void>() {
			
			@Override
			public void onSuccess(Void result) {

				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "csvservlet"+"?type="+109;
				Window.open(url, "test", "enabled");
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
	}
	
	
	

}
