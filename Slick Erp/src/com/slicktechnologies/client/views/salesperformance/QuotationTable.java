package com.slicktechnologies.client.views.salesperformance;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;

public class QuotationTable extends SuperTable<SalesPersonValue>{

	TextColumn<SalesPersonValue> getColumnCustomerId;
	TextColumn<SalesPersonValue> getColumnCustomerName;
	TextColumn<SalesPersonValue> getColumnCustomerCell;
	TextColumn<SalesPersonValue> getquotationId;
	TextColumn<SalesPersonValue> getquotationDate;
	TextColumn<SalesPersonValue> getquotationStatus;
	TextColumn<SalesPersonValue> getColumnBranch;
	
	@Override
	public void createTable() {
		addColumnCustomerId();
		addColumnCustomerName();
		addColumnCustomerCell();
		addColumnQuotationId();
		addColumnQuotationDate();
		addColumnBranch();
		addColumnQuotationStatus();
		
	}
	
	private void addColumnBranch() {
		getColumnBranch = new TextColumn<SalesPersonValue>() {
			
			@Override
			public String getValue(SalesPersonValue object) {
				return object.getQuotationBranch();
			}
		};
		table.addColumn(getColumnBranch,"Branch");
		table.setColumnWidth(getColumnBranch, 80,Unit.PX);
	}

	private void addColumnCustomerId() {
		getColumnCustomerId = new  TextColumn<SalesPersonValue>() {
			
			@Override
			public String getValue(SalesPersonValue object) {
				return object.getQuotationcustomerId()+"";
			}
		};
		
		table.addColumn(getColumnCustomerId,"Customer Id");
		table.setColumnWidth(getColumnCustomerId, 80,Unit.PX);
	}




	private void addColumnCustomerName() {
		getColumnCustomerName = new  TextColumn<SalesPersonValue>() {
			
			@Override
			public String getValue(SalesPersonValue object) {
				return object.getQuotationcustomerName();
			}
		};
		table.addColumn(getColumnCustomerName,"Customer Name");
		table.setColumnWidth(getColumnCustomerName, 80,Unit.PX);
	}




	private void addColumnCustomerCell() {
		getColumnCustomerCell = new TextColumn<SalesPersonValue>() {
			
			@Override
			public String getValue(SalesPersonValue object) {
				return object.getQuotationcustomerCell()+"";
			}
		};
		table.addColumn(getColumnCustomerCell,"Customer Cell");
		table.setColumnWidth(getColumnCustomerCell, 80,Unit.PX);
	}

	private void addColumnQuotationId() {

		getquotationId = new TextColumn<SalesPersonValue>() {
			
			@Override
			public String getValue(SalesPersonValue object) {
				return object.getQuotationId()+"";
			}
		};
		table.addColumn(getquotationId,"Quotation Id");
		table.setColumnWidth(getquotationId, 70,Unit.PX);
	}




	private void addColumnQuotationDate() {
		
		getquotationDate = new TextColumn<SalesPersonValue>() {
			
			@Override
			public String getValue(SalesPersonValue object) {
				if(object.getQuotationDate()!=null){
				return AppUtility.parseDate(object.getQuotationDate());
				}else{
					return "";
				}
			}
		};
		table.addColumn(getquotationDate,"Quotation Date");
		table.setColumnWidth(getquotationDate, 70,Unit.PX);
	}




	private void addColumnQuotationStatus() {
		getquotationStatus = new TextColumn<SalesPersonValue>() {
			
			@Override
			public String getValue(SalesPersonValue object) {
				return object.getQuotationStatus();
			}
		};
		table.addColumn(getquotationStatus,"Status");
		table.setColumnWidth(getquotationStatus, 70,Unit.PX);
	}




	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
	

}
