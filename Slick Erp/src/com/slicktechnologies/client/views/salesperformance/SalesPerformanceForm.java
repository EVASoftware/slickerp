package com.slicktechnologies.client.views.salesperformance;


import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.composites.EmployeeInfoComposite;
import com.slicktechnologies.shared.common.DummyEntityOnlyForScreen;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class SalesPerformanceForm extends FormScreen<DummyEntityOnlyForScreen>{

	public EmployeeInfoComposite employeeInfo;

	
	/** Lead table which can come on form component can be made generic */
	public LeadTable leadTable;
	
	/** Quotation table which can come on form component can be made generic */
	public QuotationTable quotationTable;
	
	/** Contract table which can come on form component can be made generic */
	public ContractTable contractTable;
	
	TextBox tbsuccessLead,tbsuccessQuotation,tbsuccessContract,tbsuccessleadper,tbsuccessquotationper,tbsuccesscontractper;
	TextBox tbunsuccessLead,tbunsuccessQuotation,tbunsuccessContract,tbunsuccessleadper,tbunsuccessquotationper,tbunsuccesscontractper;
	TextBox tbinprogLead,tbinprogQuotation,tbinprogContract,tbinprogLeadper,tbinprogQuotationper,tbinprogContractper;
	Label lbsucessfull, lbUnsuccess,lbInprogress, lbfrmdate,lbtodate, lbblank;
	
	DateBox dbFromDate;
	DateBox dbToDate;
	Button btnGo,btnshow;
	
	public SalesPerformanceForm() {
		super();
		createGui();
		leadTable.connectToLocal();
		quotationTable.connectToLocal();
		contractTable.connectToLocal();
	}	
	
	private void initalizeWidget() {
		
		employeeInfo=AppUtility.employeeInfoComposite(new EmployeeInfo(),false);
		
		leadTable = new LeadTable();

		quotationTable = new QuotationTable();
		
		contractTable = new ContractTable();
		
		lbsucessfull = new Label("Successful");
		lbUnsuccess = new Label("Unsuccessful");
		lbInprogress = new Label("In progress");
		
		lbblank = new Label();
		
		lbfrmdate = new Label("From Date");
		lbtodate = new Label("To Date");
		
		tbsuccessLead = new TextBox();
		tbsuccessLead.setEnabled(false);
		tbsuccessQuotation = new TextBox();
		tbsuccessQuotation.setEnabled(false);
		tbsuccessContract = new TextBox();
		tbsuccessContract.setEnabled(false);
		
		tbunsuccessLead = new TextBox();
		tbunsuccessLead.setEnabled(false);
		tbunsuccessQuotation = new TextBox();
		tbunsuccessQuotation.setEnabled(false);
		tbunsuccessContract = new TextBox();
		tbunsuccessContract.setEnabled(false);
		
		tbinprogLead = new TextBox();
		tbinprogLead.setEnabled(false);
		tbinprogQuotation = new TextBox();
		tbinprogQuotation.setEnabled(false);
		tbinprogContract = new TextBox();
		tbinprogContract.setEnabled(false);
		
		dbFromDate = new DateBoxWithYearSelector();
		dbToDate = new DateBoxWithYearSelector();
		
		btnGo = new Button("Go");
		
		tbsuccessleadper = new TextBox();
		tbsuccessleadper.setEnabled(false);
		tbsuccessquotationper = new TextBox();
		tbsuccessquotationper.setEnabled(false);
		tbsuccesscontractper = new TextBox();
		tbsuccesscontractper.setEnabled(false);
		
		tbunsuccessleadper = new TextBox();
		tbunsuccessleadper.setEnabled(false);
		tbunsuccessquotationper = new TextBox();
		tbunsuccessquotationper.setEnabled(false);
		tbunsuccesscontractper = new TextBox();
		tbunsuccesscontractper.setEnabled(false);
		
		tbinprogLeadper = new TextBox();
		tbinprogLeadper.setEnabled(false);
		tbinprogQuotationper = new TextBox();
		tbinprogQuotationper.setEnabled(false);
		tbinprogContractper = new TextBox();
		tbinprogContractper.setEnabled(false);
		
		btnshow = new Button("Show Target Result");
		
		
		
		
	}
	
	


	@Override
	public void createScreen() {
		// TODO Auto-generated method stub
		initalizeWidget();
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		
		FormField fgroupingEmployeeInformation=fbuilder.setlabel("Employee Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(8).build();
		
		fbuilder = new FormFieldBuilder("",employeeInfo);
		FormField fperson= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(6).build();
		
		
		fbuilder = new FormFieldBuilder("",lbblank);
		FormField flbblank= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("",lbfrmdate);
		FormField flbfrmdate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("",lbtodate);
		FormField flbtodate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("",dbFromDate);
		FormField fdbFromDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("",dbToDate);
		FormField fdbToDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("",btnGo);
		FormField fbtnGo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		
		//Ashwini Patil Date:20-06-2022 Label status was missing.
		FormField fdesc=fbuilder.setlabel("Status").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(2).build();
		
		FormField flead=fbuilder.setlabel("Lead - Count ").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(0).build();
		FormField fleadper=fbuilder.setlabel("%").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(0).build();

		FormField fquotation=fbuilder.setlabel("Quotation - Count").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(0).build();
		FormField fquotationper=fbuilder.setlabel("%").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(0).build();

		FormField fcontract=fbuilder.setlabel("Contract - Count").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(0).build();
		FormField fcontractper=fbuilder.setlabel("%").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(0).build();


		fbuilder = new FormFieldBuilder("",lbsucessfull);
		FormField flbsucessfull= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("",tbsuccessLead);
		FormField ftbLead= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",tbsuccessQuotation);
		FormField ftbQuotation= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",tbsuccessContract);
		FormField ftbContract= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",lbUnsuccess);
		FormField flbUnsuccess= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("",tbunsuccessLead);
		FormField ftbunsuccessLead= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",tbunsuccessQuotation);
		FormField ftbunsuccessQuotation= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",tbunsuccessContract);
		FormField ftbunsuccessContract= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("",lbInprogress);
		FormField flbInprogress= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("",tbinprogLead);
		FormField ftbinprogLead= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",tbinprogQuotation);
		FormField ftbinprogQuotation= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",tbinprogContract);
		FormField ftbinprogContract= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();


		FormField fleadgrouping=fbuilder.setlabel("Lead").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(8).build();
		
		fbuilder = new FormFieldBuilder("",leadTable.getTable());
		FormField fleadTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(8).build();
		
		FormField fquotationgrouping=fbuilder.setlabel("Quotation").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(8).build();
		
		fbuilder = new FormFieldBuilder("",quotationTable.getTable());
		FormField fquotationtable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(8).build();
		
		
		FormField fcontractgrouping=fbuilder.setlabel("Contract").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(8).build();
		
		fbuilder = new FormFieldBuilder("",contractTable.getTable());
		FormField fcontractTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(8).build();
		
		
		fbuilder = new FormFieldBuilder("",tbsuccessleadper);
		FormField ftbsuccessleadper= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",tbsuccessquotationper);
		FormField ftbsuccessquotationper= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",tbsuccesscontractper);
		FormField ftbsuccesscontractper= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",tbunsuccessleadper);
		FormField ftbunsuccessleadper= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",tbunsuccessquotationper);
		FormField ftbunsuccessquotationper= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",tbunsuccesscontractper);
		FormField ftbunsuccesscontractper= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",tbinprogLeadper);
		FormField ftbinprogLeadper= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",tbinprogQuotationper);
		FormField ftbinprogQuotationper= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",tbinprogContractper);
		FormField ftbinprogContractper= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		

		
		FormField[][] formFields = {   
				{fgroupingEmployeeInformation},
				{fperson,flbblank},
				{flbfrmdate,flbtodate,flbblank},
				{fdbFromDate,fdbToDate,fbtnGo},
				{flbblank},
				{fdesc,flead,fleadper,fquotation,fquotationper,fcontract,fcontractper},
				{flbsucessfull,ftbLead,ftbsuccessleadper,ftbQuotation,ftbsuccessquotationper,ftbContract,ftbsuccesscontractper},
				{flbUnsuccess,ftbunsuccessLead,ftbunsuccessleadper,ftbunsuccessQuotation,ftbunsuccessquotationper,ftbunsuccessContract,ftbunsuccesscontractper},
				{flbInprogress,ftbinprogLead,ftbinprogLeadper,ftbinprogQuotation,ftbinprogQuotationper,ftbinprogContract,ftbinprogContractper},
				{fleadgrouping},
				{fleadTable},
				{fquotationgrouping},
				{fquotationtable},
				{fcontractgrouping},
				{fcontractTable}
				
		};
		this.fields=formFields;
	}

	

	@Override
	public void updateModel(DummyEntityOnlyForScreen model) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateView(DummyEntityOnlyForScreen model) {
		// TODO Auto-generated method stub
		
	}
	
	
	@Override
	public void toggleAppHeaderBarMenu() {


		
		
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Download"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals("Download"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Download"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.SALESPERSONDASHBOARD,LoginPresenter.currentModule.trim());

	
	}

	public LeadTable getLeadTable() {
		return leadTable;
	}

	public void setLeadTable(LeadTable leadTable) {
		this.leadTable = leadTable;
	}

	public QuotationTable getQuotationTable() {
		return quotationTable;
	}

	public void setQuotationTable(QuotationTable quotationTable) {
		this.quotationTable = quotationTable;
	}

	public ContractTable getContractTable() {
		return contractTable;
	}

	public void setContractTable(ContractTable contractTable) {
		this.contractTable = contractTable;
	}

	public TextBox getTbsuccessLead() {
		return tbsuccessLead;
	}

	public void setTbsuccessLead(TextBox tbsuccessLead) {
		this.tbsuccessLead = tbsuccessLead;
	}

	public TextBox getTbsuccessQuotation() {
		return tbsuccessQuotation;
	}

	public void setTbsuccessQuotation(TextBox tbsuccessQuotation) {
		this.tbsuccessQuotation = tbsuccessQuotation;
	}

	public TextBox getTbsuccessContract() {
		return tbsuccessContract;
	}

	public void setTbsuccessContract(TextBox tbsuccessContract) {
		this.tbsuccessContract = tbsuccessContract;
	}

	public TextBox getTbunsuccessLead() {
		return tbunsuccessLead;
	}

	public void setTbunsuccessLead(TextBox tbunsuccessLead) {
		this.tbunsuccessLead = tbunsuccessLead;
	}

	public TextBox getTbunsuccessQuotation() {
		return tbunsuccessQuotation;
	}

	public void setTbunsuccessQuotation(TextBox tbunsuccessQuotation) {
		this.tbunsuccessQuotation = tbunsuccessQuotation;
	}

	public TextBox getTbunsuccessContract() {
		return tbunsuccessContract;
	}

	public void setTbunsuccessContract(TextBox tbunsuccessContract) {
		this.tbunsuccessContract = tbunsuccessContract;
	}

	public TextBox getTbinprogLead() {
		return tbinprogLead;
	}

	public void setTbinprogLead(TextBox tbinprogLead) {
		this.tbinprogLead = tbinprogLead;
	}

	public TextBox getTbinprogQuotation() {
		return tbinprogQuotation;
	}

	public void setTbinprogQuotation(TextBox tbinprogQuotation) {
		this.tbinprogQuotation = tbinprogQuotation;
	}

	public TextBox getTbinprogContract() {
		return tbinprogContract;
	}

	public void setTbinprogContract(TextBox tbinprogContract) {
		this.tbinprogContract = tbinprogContract;
	}

	public TextBox getTbsuccessleadper() {
		return tbsuccessleadper;
	}

	public void setTbsuccessleadper(TextBox tbsuccessleadper) {
		this.tbsuccessleadper = tbsuccessleadper;
	}

	public TextBox getTbsuccessquotationper() {
		return tbsuccessquotationper;
	}

	public void setTbsuccessquotationper(TextBox tbsuccessquotationper) {
		this.tbsuccessquotationper = tbsuccessquotationper;
	}

	public TextBox getTbsuccesscontractper() {
		return tbsuccesscontractper;
	}

	public void setTbsuccesscontractper(TextBox tbsuccesscontractper) {
		this.tbsuccesscontractper = tbsuccesscontractper;
	}

	public TextBox getTbunsuccessleadper() {
		return tbunsuccessleadper;
	}

	public void setTbunsuccessleadper(TextBox tbunsuccessleadper) {
		this.tbunsuccessleadper = tbunsuccessleadper;
	}

	public TextBox getTbunsuccessquotationper() {
		return tbunsuccessquotationper;
	}

	public void setTbunsuccessquotationper(TextBox tbunsuccessquotationper) {
		this.tbunsuccessquotationper = tbunsuccessquotationper;
	}

	public TextBox getTbunsuccesscontractper() {
		return tbunsuccesscontractper;
	}

	public void setTbunsuccesscontractper(TextBox tbunsuccesscontractper) {
		this.tbunsuccesscontractper = tbunsuccesscontractper;
	}

	public TextBox getTbinprogLeadper() {
		return tbinprogLeadper;
	}

	public void setTbinprogLeadper(TextBox tbinprogLeadper) {
		this.tbinprogLeadper = tbinprogLeadper;
	}

	public TextBox getTbinprogQuotationper() {
		return tbinprogQuotationper;
	}

	public void setTbinprogQuotationper(TextBox tbinprogQuotationper) {
		this.tbinprogQuotationper = tbinprogQuotationper;
	}

	public TextBox getTbinprogContractper() {
		return tbinprogContractper;
	}

	public void setTbinprogContractper(TextBox tbinprogContractper) {
		this.tbinprogContractper = tbinprogContractper;
	}
	
	
	

}
