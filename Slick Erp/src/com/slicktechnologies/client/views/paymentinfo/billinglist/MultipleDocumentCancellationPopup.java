package com.slicktechnologies.client.views.paymentinfo.billinglist;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.TextArea;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;

public class MultipleDocumentCancellationPopup extends PopupScreen{

	public TextArea taRemark;

	public MultipleDocumentCancellationPopup(){
		super();
		createGui();
	}
	
	@Override
	public void createScreen() {
		
		taRemark = new TextArea();
		
		
		FormFieldBuilder fbuilder;
		
		fbuilder = new FormFieldBuilder();
		FormField fCancellationInformation=fbuilder.setlabel("Cancellation Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Remark",taRemark);
		FormField ftaRemark = fbuilder.setMandatory(true).setColSpan(0).setRowSpan(0).build();
		
		FormField[][] formfield = {  
				{fCancellationInformation},
				{ftaRemark},
		};
		this.fields=formfield;
		
	}
	
	
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
	}

	
	public TextArea getTaRemark() {
		return taRemark;
	}

	public void setTaRemark(TextArea taRemark) {
		this.taRemark = taRemark;
	}
	
}

