package com.slicktechnologies.client.views.paymentinfo.billinglist;

import java.util.Comparator;
import java.util.List;

import com.google.code.p.gwtchismes.client.GWTCGlassPanel;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.ValueUpdater;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.cellview.client.Header;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.service.ServicePresenter;
import com.google.code.p.gwtchismes.client.GWTCGlassPanel;

public class BillingListTable extends SuperTable<BillingDocument> {
	
	TextColumn<BillingDocument> billingIdColumn;
	TextColumn<BillingDocument> billingDateColumn;
	TextColumn<BillingDocument> customerIdColumn;
	TextColumn<BillingDocument> customerNameColumn;
	TextColumn<BillingDocument> customerCellColumn;
	TextColumn<BillingDocument> contractIdColumn;
	TextColumn<BillingDocument> salesAmtColumn;
	TextColumn<BillingDocument> billingAmtColumn;
	TextColumn<BillingDocument> invoiceDateColumn;
	TextColumn<BillingDocument> paymentDateColumn;
	TextColumn<BillingDocument> approverNameColumn;
	TextColumn<BillingDocument> billingCategoryColumn;
	TextColumn<BillingDocument> billingTypeColumn;
	TextColumn<BillingDocument> billingGroupColumn;
	TextColumn<BillingDocument> accountTypeColumn;
	TextColumn<BillingDocument> statusColumn;
	TextColumn<BillingDocument> branchColumn;
	private Column<BillingDocument, Boolean> checkRecord;
	
	//  vijay added this on 15/2/2017
	TextColumn<BillingDocument> rateContractServiceId;
	Column<BillingDocument, Boolean> checkColumn;
	Header<Boolean> selectAllHeader;
	protected GWTCGlassPanel glassPanel;
	
	private String DEFAULTHEIGHT="500px";
	
	/*
	 * nidhi
	 */
	TextColumn<BillingDocument> segmentcolumn;
	/*
	 * end
	 */
	
	NumberFormat nf=NumberFormat.getFormat("0.00");
	
	/**
	 *  nidhi
	 *  20-07-2017
	 *   for display service quantity and unit
	 */
	TextColumn<BillingDocument> quantityColumn;
	TextColumn<BillingDocument> uomColumn;
	/*
	 *  end
	 */
	
	/**
	 *  nidhi
	 *  24-07-2017
	 *   for display service quantity and unit
	 */
	TextColumn<BillingDocument> refColumn;
	/*
	 *  end
	 */
	/** 03-10-2017 sagar sore [To add Number range column in Search pop up result ]**/
	TextColumn<BillingDocument> numberRangeColumn;
	/** date 1.11.2018 added by komal for type of order and sub bill type **/
	TextColumn<BillingDocument> getColTypeOfOrder;
	TextColumn<BillingDocument> getColSubBillType;
	
	/**
	 * nidhi 14-12-2018
	 */
	TextColumn<BillingDocument> getColCancleAmt;
	TextColumn<BillingDocument> getColCancleRemark;
	boolean parcialBillDisableFlag = AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "DisableParticalBillDocument");
	
	Column<BillingDocument, String> productDetailsButton;//Ashwini Patil Date:3-07-2024 for ultima search
	TextColumn<BillingDocument> finalTotalAmtColumn;//Ashwini Patil Date:10-07-2024 ultima want to see billing amt column without taxes
	
	public BillingListTable() {
		super();
		 parcialBillDisableFlag = AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "DisableParticalBillDocument");
		 setHeight(DEFAULTHEIGHT);
		// setWidth();
		 
	}

	@Override
	public void createTable() {		
		 parcialBillDisableFlag = AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "DisableParticalBillDocument");
		/**03-10-2017 sagar sore [number range column added and order of columns changed]**/
		/**old code**/
//		addAnotherCheckBoxCell();
//		addColumnBillingId();
//		addColumnBillingDate();
//		addColumnCustomerId();
//		addColumnCustomerName();
//		addColumnCustomerPhone();
//		addColumnAccountType();
//		addColumnContractId();
//		addColumnSalesAmount();
//		addColumnBillingAmount();
//		addColumnInvoiceDate();
//		addColumnPaymentDate();
//		addColumnBranch();
//		addColumnApproverName();
//		addColumnBillingStatus();
////		setFieldUpdaterOnCheckBox();
//		addColumnRateContractServiceId();
//		addColumnSegment();
		 
		
//        /**updated code**/
//		addAnotherCheckBoxCell();
//		addColumnBillingId();
//		addColumnBillingDate();
//		addColumnCustomerId();		
//		addColumnCustomerName();
//		addColumnCustomerPhone();
//		addColumnAccountType();
//		addColumnBillingStatus();
//		addColumnBranch();
//		addColumnNumberRange();
//		addColumnContractId();
//		addColumnSalesAmount();
//		addColumnBillingAmount();
//		addColumnInvoiceDate();
//		addColumnPaymentDate();
//		addColumnBranch();
//		addColumnApproverName();
//		addColumnRateContractServiceId();
//		addColumnSegment();
//		
//		/**
//		 *  nidhi
//		 *  for quantity and uom display
//		 */
//		addColumnQuantity();
//		addColumnUom();
//		/**
//		 *  end
//		 */
//		/**
//		 *  nidhi
//		 *  for quantity and uom display
//		 *  24-07-2017
//		 */
//		addColumnRefNo();
//		/**
//		 *  end
//		 */
//		getColTypeOfOrder();
//		getColSubBillType();
		 
		 /**
			 *  Added By Priyanka Date - 30-08-2021
			 *  Des :Billing List Column repositioning requirement raised by Nitin Sir
			 */
		 
		 addAnotherCheckBoxCell();
		 addColumnBillingId();
		 addColumnBillingDate();
		 addColumnBillingStatus();
		 addColumnCustomerName();
		 addColumnCustomerPhone();
		 createColumnProductDetails();
		 addColumnFinalBillingAmount();
		 addColumnBillingAmount();
		 getColSubBillType();
		 addColumnInvoiceDate();
		 addColumnPaymentDate();
		 addColumnApproverName();
		 addColumnNumberRange();
		 addColumnRefNo();
		 addColumnSalesAmount();
		 addColumnContractId();
		 addColumnBranch();
		 addColumnAccountType();
		 getColTypeOfOrder();
		 addColumnCustomerId();
		 addColumnRateContractServiceId();
		 addColumnSegment();
		 addColumnQuantity();
		 addColumnUom();
		 
		 /**
		  *  End
		  */
		 
		if(parcialBillDisableFlag){
			getColCancleAmt();
			getColCancleAmtRemark();
			
		}
	    table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
	}
	
	public void addColumnSorting()
	{

		addSortinggetBillingCount();
		addSortinggetBillingDate();
		addSortinggetCustomerId();
		addSortinggetCustomerFullName();
		addSortinggetCustomerCellNumber();
		addSortinggetContractCount();
		addSortingAccountType();
		addSortinggetSalesAmount();
		addSortinggetFinalBillingAmount();
		addSortinggetBillingAmount();
		addSortingInvoiceDate();
		addSortingPaymentDate();
		addSortinggetApproverName();
		addSortinggetStatus();
		addSortinggetBranch();
		addSortingRateContractServiceId();
		addSortingSegment();
		/*
		 *  nidhi
		 *  20-07-2017
		 *  
		 */
		addSortingUom();
		addSortingQuantity();
		/**
		 *  end
		 */
		
		/*
		 *  nidhi
		 *  24-07-2017
		 *  
		 */
		addSortingRefNo();
		/**
		 *  end
		 */
		/**03-10-2017 sagar sore [adding number range column]**/
		addSortinggetNumberRange();
		addSortingTypeOfOrder();
		addSortingSubBillType();
		if(parcialBillDisableFlag){
			addSortingCancleAmt();
			addSortingCancleAmtRemark();
			
		}
	}
private void addColumnSegment() {
		
		segmentcolumn = new TextColumn<BillingDocument>() {
			
			@Override
			public String getValue(BillingDocument object) {
				if(object.getSegment()!= null)
					return object.getSegment() +"";
				else 
					return "";
			}
		};
		
		table.addColumn(segmentcolumn,"Segment");
		table.setColumnWidth(segmentcolumn, 100, Unit.PX);
		segmentcolumn.setSortable(true);
		
	}

protected void addSortingSegment()
{
	List<BillingDocument> list=getDataprovider().getList();
	columnSort=new ListHandler<BillingDocument>(list);
	columnSort.setComparator(segmentcolumn, new Comparator<BillingDocument>()
			{
		@Override
		public int compare(BillingDocument e1,BillingDocument e2)
		{
			if(e1!=null && e2!=null)
			{
				if( e1.getBranch()!=null && e2.getBranch()!=null){
					return e1.getBranch().compareTo(e2.getBranch());}
			}
			else{
				return 0;}
			return 0;
		}
			});
	table.addColumnSortHandler(columnSort);
}
private void addColumnQuantity() {
	
	quantityColumn = new TextColumn<BillingDocument>() {
		
		@Override
		public String getValue(BillingDocument object) {
			if(object.getQuantity()!=0)
				return object.getQuantity() +"";
			else 
				return "";
		}
	};
	
	table.addColumn(quantityColumn,"Quantity");
	table.setColumnWidth(quantityColumn, 100, Unit.PX);
	quantityColumn.setSortable(true);
	
}

public void addColumnUom()
{
	uomColumn=new TextColumn<BillingDocument>() {

		@Override
		public String getValue(BillingDocument object) {
			return object.getUom();
		}
	};
	
	table.addColumn(uomColumn,"UOM");
	table.setColumnWidth(uomColumn, 100, Unit.PX);
	uomColumn.setSortable(true);
}	

public void addColumnRefNo()
{
	refColumn=new TextColumn<BillingDocument>() {

		@Override
		public String getValue(BillingDocument object) {
			return object.getRefNumber();
		}
	};
	
	table.addColumn(refColumn,"REFERANCE NO.");
	table.setColumnWidth(refColumn, 100, Unit.PX);
	refColumn.setSortable(true);
}	

private void addSortingQuantity() {
	List<BillingDocument> list=getDataprovider().getList();
	columnSort=new ListHandler<BillingDocument>(list);
	columnSort.setComparator(quantityColumn, new Comparator<BillingDocument>()
			{
		@Override
		public int compare(BillingDocument e1,BillingDocument e2)
		{
			if(e1!=null && e2!=null)
			{
				if(e1.getQuantity()== e2.getQuantity()){
					return 0;}
				if(e1.getQuantity()> e2.getQuantity()){
					return 1;}
				else{
					return -1;
				}
			}
			else{
				return 0;}
		}
			});
	table.addColumnSortHandler(columnSort);
}


protected void addSortingUom()
{
	List<BillingDocument> list=getDataprovider().getList();
	columnSort=new ListHandler<BillingDocument>(list);
	columnSort.setComparator(uomColumn, new Comparator<BillingDocument>()
			{
		@Override
		public int compare(BillingDocument e1,BillingDocument e2)
		{
			if(e1!=null && e2!=null)
			{
				if( e1.getUom()!=null && e2.getUom()!=null){
					return e1.getUom().compareTo(e2.getUom());}
			}
			else{
				return 0;}
			return 0;
		}
			});
	table.addColumnSortHandler(columnSort);
}

protected void addSortingRefNo()
{
	List<BillingDocument> list=getDataprovider().getList();
	columnSort=new ListHandler<BillingDocument>(list);
	columnSort.setComparator(refColumn, new Comparator<BillingDocument>()
			{
		@Override
		public int compare(BillingDocument e1,BillingDocument e2)
		{
			if(e1!=null && e2!=null)
			{
				if( e1.getRefNumber()!=null && e2.getRefNumber()!=null){
					return e1.getRefNumber().compareTo(e2.getRefNumber());}
			}
			else{
				return 0;}
			return 0;
		}
			});
	table.addColumnSortHandler(columnSort);
}
	private void addColumnRateContractServiceId() {
		
		rateContractServiceId = new TextColumn<BillingDocument>() {
			
			@Override
			public String getValue(BillingDocument object) {
				if(object.getRateContractServiceId()!=0)
					return object.getRateContractServiceId() +"";
				else 
					return "";
			}
		};
		
		table.addColumn(rateContractServiceId,"Service Id");
		table.setColumnWidth(rateContractServiceId, 100, Unit.PX);
		rateContractServiceId.setSortable(true);
		
	}

	
	private void addSortingRateContractServiceId() {
		List<BillingDocument> list=getDataprovider().getList();
		columnSort=new ListHandler<BillingDocument>(list);
		columnSort.setComparator(rateContractServiceId, new Comparator<BillingDocument>()
				{
			@Override
			public int compare(BillingDocument e1,BillingDocument e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getRateContractServiceId()== e2.getRateContractServiceId()){
						return 0;}
					if(e1.getRateContractServiceId()> e2.getRateContractServiceId()){
						return 1;}
					else{
						return -1;
					}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	private void addAnotherCheckBoxCell() {
		checkColumn=new Column<BillingDocument, Boolean>(new CheckboxCell()) {
			@Override
			public Boolean getValue(BillingDocument object) {
				return object.getRecordSelect();
			}
		};
		
		checkColumn.setFieldUpdater(new FieldUpdater<BillingDocument, Boolean>() {
			@Override
			public void update(int index, BillingDocument object, Boolean value) {
				System.out.println("Check Column ....");
				Console.log("INSIDE CHECKBOX UPDATOR METHOD - "+BillingListPresenter.viewDocumentFlag);
				BillingListPresenter.viewDocumentFlag=false;
				Console.log("AFTER - "+BillingListPresenter.viewDocumentFlag);
				object.setRecordSelect(value);
				table.redrawRow(index);
				selectAllHeader.getValue();
				table.redrawHeaders();
				
				glassPanel = new GWTCGlassPanel();
				glassPanel.show();
				Timer timer = new Timer() {
					@Override
					public void run() {
						glassPanel.hide();
					}
				};
				timer.schedule(1000); 
			}
		});
		
		
		selectAllHeader =new Header<Boolean>(new CheckboxCell()) {
			@Override
			public Boolean getValue() {
				if(getDataprovider().getList().size()!=0){
					
				}
				return false;
			}
		};
		
		
		selectAllHeader.setUpdater(new ValueUpdater<Boolean>() {
			@Override
			public void update(Boolean value) {
				List<BillingDocument> list=getDataprovider().getList();
				for(BillingDocument object:list){
					object.setRecordSelect(value);
				}
				getDataprovider().setList(list);
				table.redraw();
				addColumnSorting();
			}
		});
		table.addColumn(checkColumn,selectAllHeader);
		table.setColumnWidth(checkColumn, 60, Unit.PX);
	}
	
	public void addColumnBranch()
	{
		branchColumn=new TextColumn<BillingDocument>() {

			@Override
			public String getValue(BillingDocument object) {
				return object.getBranch();
			}
		};
		
		table.addColumn(branchColumn,"Branch");
		table.setColumnWidth(branchColumn, 100, Unit.PX);
		branchColumn.setSortable(true);
	}
	
	protected void addSortinggetBranch()
	{
		List<BillingDocument> list=getDataprovider().getList();
		columnSort=new ListHandler<BillingDocument>(list);
		columnSort.setComparator(branchColumn, new Comparator<BillingDocument>()
				{
			@Override
			public int compare(BillingDocument e1,BillingDocument e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBranch()!=null && e2.getBranch()!=null){
						return e1.getBranch().compareTo(e2.getBranch());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetBillingCount()
	{
		List<BillingDocument> list=getDataprovider().getList();
		columnSort=new ListHandler<BillingDocument>(list);
		columnSort.setComparator(billingIdColumn, new Comparator<BillingDocument>()
				{
			@Override
			public int compare(BillingDocument e1,BillingDocument e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;
					}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	
	public void addColumnBillingId()
	{
		
		billingIdColumn=new TextColumn<BillingDocument>() {

			@Override
			public String getValue(BillingDocument object) {
				return object.getCount()+"";
			}
		};
		table.addColumn(billingIdColumn,"Billing ID");
		table.setColumnWidth(billingIdColumn, 100, Unit.PX);
		billingIdColumn.setSortable(true);
	}
	
	protected void addSortinggetBillingDate()
	{
		List<BillingDocument> list=getDataprovider().getList();
		columnSort=new ListHandler<BillingDocument>(list);
		columnSort.setComparator(billingDateColumn, new Comparator<BillingDocument>()
				{
			@Override
			public int compare(BillingDocument e1,BillingDocument e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBillingDate()!=null && e2.getBillingDate()!=null){
						return e1.getBillingDate().compareTo(e2.getBillingDate());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	public void addColumnBillingDate()
	{
		billingDateColumn=new TextColumn<BillingDocument>() {

			@Override
			public String getValue(BillingDocument object) {
				if(object.getBillingDate()!=null)
				{
				    String billDate=AppUtility.parseDate(object.getBillingDate());
				    return billDate;
				}
				return "";
			}
		};
		
		table.addColumn(billingDateColumn,"Billing Date");
		table.setColumnWidth(billingDateColumn, 100, Unit.PX);
        billingDateColumn.setSortable(true);
	}
	
	protected void addSortinggetCustomerId()
	{
		List<BillingDocument> list=getDataprovider().getList();
		columnSort=new ListHandler<BillingDocument>(list);
		columnSort.setComparator(customerIdColumn, new Comparator<BillingDocument>()
				{
			@Override
			public int compare(BillingDocument e1,BillingDocument e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getPersonInfo().getCount()== e2.getPersonInfo().getCount()){
						return 0;}
					if(e1.getPersonInfo().getCount()> e2.getPersonInfo().getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}

	
	private void addColumnCustomerId() {
		customerIdColumn=new TextColumn<BillingDocument>() {

			@Override
			public String getValue(BillingDocument object) {
				return object.getCustomerCount()+"";
			}
		};
		
		table.addColumn(customerIdColumn,"BP ID");
		table.setColumnWidth(customerIdColumn, 110, Unit.PX);
		customerIdColumn.setSortable(true);
		
	}
	
	protected void addSortinggetCustomerFullName()
	{
		List<BillingDocument> list=getDataprovider().getList();
		columnSort=new ListHandler<BillingDocument>(list);
		columnSort.setComparator(customerNameColumn, new Comparator<BillingDocument>()
				{
			@Override
			public int compare(BillingDocument e1,BillingDocument e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getPersonInfo().getFullName()!=null && e2.getPersonInfo().getFullName()!=null){
						return e1.getPersonInfo().getFullName().compareTo(e2.getPersonInfo().getFullName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}

	public void addColumnCustomerName()
	{
		customerNameColumn=new TextColumn<BillingDocument>() {

			@Override
			public String getValue(BillingDocument object) {
				return object.getName();
			}
		};
		/**
		 *  Added By Priyanka Date - 30-08-2021
		 *  Des :Billing List Column repositioning requirement raised by Nitin Sir
		 */
		//table.addColumn(customerNameColumn,"BP Name");
		table.addColumn(customerNameColumn,"Customer Name");
		table.setColumnWidth(customerNameColumn, 120, Unit.PX);
		customerNameColumn.setSortable(true);
	}
	
	protected void addSortinggetCustomerCellNumber()
	{
		List<BillingDocument> list=getDataprovider().getList();
		columnSort=new ListHandler<BillingDocument>(list);
		columnSort.setComparator(customerCellColumn, new Comparator<BillingDocument>()
				{
			@Override
			public int compare(BillingDocument e1,BillingDocument e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getPersonInfo().getCellNumber()== e2.getPersonInfo().getCellNumber()){
						return 0;}
					if(e1.getPersonInfo().getCellNumber()> e2.getPersonInfo().getCellNumber()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	public void addColumnCustomerPhone()
	{
		customerCellColumn=new TextColumn<BillingDocument>() {

			@Override
			public String getValue(BillingDocument object) {
				return object.getCellNumber()+"";
			}
		};
		
		table.addColumn(customerCellColumn,"Cell Number");
		table.setColumnWidth(customerCellColumn, 120, Unit.PX);
		customerCellColumn.setSortable(true);
	}
	
	
	protected void addSortingAccountType()
	{
		List<BillingDocument> list=getDataprovider().getList();
		columnSort=new ListHandler<BillingDocument>(list);
		columnSort.setComparator(accountTypeColumn, new Comparator<BillingDocument>()
		{
			@Override
			public int compare(BillingDocument e1,BillingDocument e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getAccountType()!=null && e2.getAccountType()!=null){
						return e1.getAccountType().compareTo(e2.getAccountType());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}

	public void addColumnAccountType()
	{
		accountTypeColumn=new TextColumn<BillingDocument>() {

			@Override
			public String getValue(BillingDocument object) {
				return object.getAccountType();
			}
		};
		
		table.addColumn(accountTypeColumn,"Acc. Type");
		table.setColumnWidth(accountTypeColumn, 90, Unit.PX);
		accountTypeColumn.setSortable(true);
	}
	
	
	protected void addSortinggetContractCount()
	{
		List<BillingDocument> list=getDataprovider().getList();
		columnSort=new ListHandler<BillingDocument>(list);
		columnSort.setComparator(contractIdColumn, new Comparator<BillingDocument>()
				{
			@Override
			public int compare(BillingDocument e1,BillingDocument e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getContractCount()== e2.getContractCount()){
						return 0;}
					if(e1.getContractCount()> e2.getContractCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	private void addColumnContractId() {
		contractIdColumn=new TextColumn<BillingDocument>() {

			@Override
			public String getValue(BillingDocument object) {
				return object.getContractCount()+"";
			}
		};
		
		table.addColumn(contractIdColumn,"Order ID");
		table.setColumnWidth(contractIdColumn, 100, Unit.PX);
		contractIdColumn.setSortable(true);
		
	}
	
	protected void addSortinggetSalesAmount()
	{
		List<BillingDocument> list=getDataprovider().getList();
		columnSort=new ListHandler<BillingDocument>(list);
		columnSort.setComparator(salesAmtColumn, new Comparator<BillingDocument>()
				{
			@Override
			public int compare(BillingDocument e1,BillingDocument e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getTotalSalesAmount()== e2.getTotalSalesAmount()){
						return 0;}
					if(e1.getTotalSalesAmount()> e2.getTotalSalesAmount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}

	public void addColumnSalesAmount()
	{
		salesAmtColumn=new TextColumn<BillingDocument>() {

			@Override
			public String getValue(BillingDocument object) {
				return object.getTotalSalesAmount()+"";
			}
		};
		
		table.addColumn(salesAmtColumn,"Order Amt");
		table.setColumnWidth(salesAmtColumn, 100, Unit.PX);
		salesAmtColumn.setSortable(true);
	}
	
	protected void addSortinggetBillingAmount()
	{
		List<BillingDocument> list=getDataprovider().getList();
		columnSort=new ListHandler<BillingDocument>(list);
		columnSort.setComparator(billingAmtColumn, new Comparator<BillingDocument>()
				{
			@Override
			public int compare(BillingDocument e1,BillingDocument e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getTotalBillingAmount()== e2.getTotalBillingAmount()){
						return 0;}
					if(e1.getTotalBillingAmount()> e2.getTotalBillingAmount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	public void addColumnBillingAmount()
	{
		billingAmtColumn=new TextColumn<BillingDocument>() {

			@Override
			public String getValue(BillingDocument object) {
				if(object.getTotalBillingAmount()!=0)
				{
					return nf.format(object.getTotalBillingAmount());
				}
				else{
					return "";
				}
			}
		};
		
		table.addColumn(billingAmtColumn,"Billing Amt");
		table.setColumnWidth(billingAmtColumn, 100, Unit.PX);
		billingAmtColumn.setSortable(true);
	}
	
	protected void addSortingInvoiceDate()
	{
		List<BillingDocument> list=getDataprovider().getList();
		columnSort=new ListHandler<BillingDocument>(list);
		columnSort.setComparator(invoiceDateColumn, new Comparator<BillingDocument>()
				{
			@Override
			public int compare(BillingDocument e1,BillingDocument e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getInvoiceDate()!=null && e2.getInvoiceDate()!=null){
						return e1.getInvoiceDate().compareTo(e2.getInvoiceDate());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	
	public void addColumnInvoiceDate()
	{
		invoiceDateColumn=new TextColumn<BillingDocument>() {

			@Override
			public String getValue(BillingDocument object) {
				if(object.getInvoiceDate()!=null)
				{
				    String invDate=AppUtility.parseDate(object.getInvoiceDate());
				    return invDate;
				}
				return "";
			}
		};
		
		table.addColumn(invoiceDateColumn,"Invoice Date");
		table.setColumnWidth(invoiceDateColumn, 120, Unit.PX);
		invoiceDateColumn.setSortable(true);
	}

	protected void addSortingPaymentDate()
	{
		List<BillingDocument> list=getDataprovider().getList();
		columnSort=new ListHandler<BillingDocument>(list);
		columnSort.setComparator(paymentDateColumn, new Comparator<BillingDocument>()
				{
			@Override
			public int compare(BillingDocument e1,BillingDocument e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getPaymentDate()!=null && e2.getPaymentDate()!=null){
						return e1.getPaymentDate().compareTo(e2.getPaymentDate());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	
	public void addColumnPaymentDate()
	{
		paymentDateColumn=new TextColumn<BillingDocument>() {

			@Override
			public String getValue(BillingDocument object) {
				if(object.getPaymentDate()!=null)
				{
				    String payDate=AppUtility.parseDate(object.getPaymentDate());
				    return payDate;
				}
				return "";
			}
		};
		
		table.addColumn(paymentDateColumn,"Payment Date");
		table.setColumnWidth(paymentDateColumn, 120, Unit.PX);
		paymentDateColumn.setSortable(true);
	}
	
	protected void addSortinggetApproverName()
	{
		List<BillingDocument> list=getDataprovider().getList();
		columnSort=new ListHandler<BillingDocument>(list);
		columnSort.setComparator(approverNameColumn, new Comparator<BillingDocument>()
				{
			@Override
			public int compare(BillingDocument e1,BillingDocument e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getApproverName()!=null && e2.getApproverName()!=null){
						return e1.getApproverName().compareTo(e2.getApproverName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}

	
	public void addColumnApproverName()
	{
		approverNameColumn=new TextColumn<BillingDocument>() {

			@Override
			public String getValue(BillingDocument object) {
				if(object.getApproverName()!=null)
				{
					return object.getApproverName();
				}
				return "";
			}
		};
		
		table.addColumn(approverNameColumn,"Approver Name");
		table.setColumnWidth(approverNameColumn, 120, Unit.PX);
		approverNameColumn.setSortable(true);
	}	

	public void addColumnBillingCategory()
	{
		billingCategoryColumn=new TextColumn<BillingDocument>() {

			@Override
			public String getValue(BillingDocument object) {
				if(object.getBillingCategory()!=null)
				{
					return object.getBillingCategory();
				}
				return "";
			}
		};
		
		table.addColumn(billingCategoryColumn,"Billing Category");
		table.setColumnWidth(billingCategoryColumn, 120, Unit.PX);
		billingCategoryColumn.setSortable(true);
	}	

	public void addColumnBillingType()
	{
		billingTypeColumn=new TextColumn<BillingDocument>() {

			@Override
			public String getValue(BillingDocument object) {
				if(object.getBillingType()!=null)
				{
					return object.getBillingType();
				}
				return "";
			}
		};
		
		table.addColumn(billingTypeColumn,"Billing Type");
		table.setColumnWidth(billingTypeColumn, 120, Unit.PX);
		billingTypeColumn.setSortable(true);
	}	
	
	public void addColumnBillingGroup()
	{
		billingGroupColumn=new TextColumn<BillingDocument>() {

			@Override
			public String getValue(BillingDocument object) {
				if(object.getBillingGroup()!=null)
				{
					return object.getBillingGroup();
				}
				return "";
			}
		};
		
		table.addColumn(billingGroupColumn,"Billing Group");
		table.setColumnWidth(billingGroupColumn, 100, Unit.PX);
		billingGroupColumn.setSortable(true);
	}		
	
	protected void addSortinggetStatus()
	{
		List<BillingDocument> list=getDataprovider().getList();
		columnSort=new ListHandler<BillingDocument>(list);
		columnSort.setComparator(statusColumn, new Comparator<BillingDocument>()
				{
			@Override
			public int compare(BillingDocument e1,BillingDocument e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getStatus()!=null && e2.getStatus()!=null){
						return e1.getStatus().compareTo(e2.getStatus());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	public void addColumnBillingStatus()
	{
		statusColumn=new TextColumn<BillingDocument>() {

			@Override
			public String getValue(BillingDocument object) {
				return object.getStatus();
			}
		};
		
		table.addColumn(statusColumn,"Status");
		table.setColumnWidth(statusColumn, 100, Unit.PX);
		statusColumn.setSortable(true);
	}	
	
	private void addColumnCheckBox()
	{
		CheckboxCell checkCell= new CheckboxCell();
		checkRecord = new Column<BillingDocument, Boolean>(checkCell) {

			@Override
			public Boolean getValue(BillingDocument object) {
				
				return object.getRecordSelect();
			}
		};
		table.addColumn(checkRecord,"Select");
		table.setColumnWidth(checkRecord, 60, Unit.PX);
	}

	private void setFieldUpdaterOnCheckBox()
	{
		checkRecord.setFieldUpdater(new FieldUpdater<BillingDocument, Boolean>() {
			
			@Override
			public void update(int index, BillingDocument object, Boolean value) {
				try{
					Boolean val1=(value);
					object.setRecordSelect(val1);
				}
				catch(Exception e)
				{
				}
				table.redrawRow(index);
			
			}
		});
	}
	
	/**03-10-2017 sagar sore [adding number range column]**/
	public void addColumnNumberRange()
	{
		numberRangeColumn=new TextColumn<BillingDocument>() {

			@Override
			public String getValue(BillingDocument object) {
				return object.getNumberRange();
			}
		};
		
		table.addColumn(numberRangeColumn,"Number Range");
		table.setColumnWidth(numberRangeColumn, 120, Unit.PX);
		numberRangeColumn.setSortable(true);
	}	
	
	protected void addSortinggetNumberRange()
	{
		List<BillingDocument> list=getDataprovider().getList();
		columnSort=new ListHandler<BillingDocument>(list);
		columnSort.setComparator(numberRangeColumn, new Comparator<BillingDocument>()
				{
			@Override
			public int compare(BillingDocument e1,BillingDocument e2)
			{
				if (e1 != null && e2 != null) {
					
					if (e1.getNumberRange() == null && e2.getNumberRange() == null){
						return 0;
					}
					if(e1.getNumberRange() == null) return 1;							
					if(e2.getNumberRange() == null) return -1;							
					return e1.getNumberRange().compareTo(e2.getNumberRange());

				}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	/**end**/
	
	
	
	
	
	
	
	
	
		
	

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
	/** date 1.11.2018 added by komal for type of order and sub bill type **/
	public void getColTypeOfOrder()
	{
		getColTypeOfOrder=new TextColumn<BillingDocument>() {

			@Override
			public String getValue(BillingDocument object) {
				return object.getTypeOfOrder();
			}
		};
		/**
		 *  Added By Priyanka Date - 30-08-2021
		 *  Des :Billing List Column repositioning requirement raised by Nitin Sir
		 */
		//table.addColumn(getColTypeOfOrder,"Type Of Order");
		table.addColumn(getColTypeOfOrder,"Order Type");
		table.setColumnWidth(getColTypeOfOrder, 120, Unit.PX);
		getColTypeOfOrder.setSortable(true);
	}	
	
	protected void addSortingTypeOfOrder()
	{
		List<BillingDocument> list=getDataprovider().getList();
		columnSort=new ListHandler<BillingDocument>(list);
		columnSort.setComparator(getColTypeOfOrder, new Comparator<BillingDocument>()
				{
			@Override
			public int compare(BillingDocument e1,BillingDocument e2)
			{
				if (e1 != null && e2 != null) {
					
					if (e1.getNumberRange() == null && e2.getNumberRange() == null){
						return 0;
					}
					if(e1.getNumberRange() == null) return 1;							
					if(e2.getNumberRange() == null) return -1;							
					return e1.getNumberRange().compareTo(e2.getNumberRange());

				}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	public void getColSubBillType()
	{
		getColSubBillType=new TextColumn<BillingDocument>() {

			@Override
			public String getValue(BillingDocument object) {
				return object.getSubBillType();
			}
		};
		
		table.addColumn(getColSubBillType,"Sub Bill Type");
		table.setColumnWidth(getColSubBillType, 120, Unit.PX);
		getColSubBillType.setSortable(true);
	}	
	
	protected void addSortingSubBillType()
	{
		List<BillingDocument> list=getDataprovider().getList();
		columnSort=new ListHandler<BillingDocument>(list);
		columnSort.setComparator(getColSubBillType, new Comparator<BillingDocument>()
				{
			@Override
			public int compare(BillingDocument e1,BillingDocument e2)
			{
				if (e1 != null && e2 != null) {
					
					if (e1.getNumberRange() == null && e2.getNumberRange() == null){
						return 0;
					}
					if(e1.getNumberRange() == null) return 1;							
					if(e2.getNumberRange() == null) return -1;							
					return e1.getNumberRange().compareTo(e2.getNumberRange());

				}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}

	/**end**/

	public void getColCancleAmt()
	{
		getColCancleAmt=new TextColumn<BillingDocument>() {

			@Override
			public String getValue(BillingDocument object) {
				return object.getCancleAmt()+"";
			}
		};
		
		table.addColumn(getColCancleAmt,"Cancel Amount");
		table.setColumnWidth(getColCancleAmt, 120, Unit.PX);
		getColCancleAmt.setSortable(true);
	}	
	
	public void getColCancleAmtRemark()
	{
		getColCancleRemark=new TextColumn<BillingDocument>() {

			@Override
			public String getValue(BillingDocument object) {
				if(object.getCancleAmtRemark()!=null)
					return object.getCancleAmtRemark()+"";
				else{
					return "";
				}
			}
		};
		
		table.addColumn(getColCancleRemark,"Cancel Amount Remark");
		table.setColumnWidth(getColCancleRemark, 120, Unit.PX);
		getColCancleRemark.setSortable(true);
	}	
	
	protected void addSortingCancleAmt()
	{
		List<BillingDocument> list=getDataprovider().getList();
		columnSort=new ListHandler<BillingDocument>(list);
		columnSort.setComparator(getColCancleAmt, new Comparator<BillingDocument>()
				{
			@Override
			public int compare(BillingDocument e1,BillingDocument e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCancleAmt()== e2.getCancleAmt()){
						return 0;}
					if(e1.getCancleAmt()> e2.getCancleAmt()){
						return 1;}
					else{
						return -1;
					}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	

	protected void addSortingCancleAmtRemark()
	{
		List<BillingDocument> list=getDataprovider().getList();
		columnSort=new ListHandler<BillingDocument>(list);
		columnSort.setComparator(getColCancleRemark, new Comparator<BillingDocument>()
				{
			@Override
			public int compare(BillingDocument e1,BillingDocument e2)
			{
				if (e1 != null && e2 != null) {
					
					if (e1.getCancleAmtRemark() == null && e2.getCancleAmtRemark() == null){
						return 0;
					}
					if(e1.getCancleAmtRemark() == null) return 1;							
					if(e2.getCancleAmtRemark() == null) return -1;							
					return e1.getCancleAmtRemark().compareTo(e2.getCancleAmtRemark());

				}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	private void createColumnProductDetails() {

		ButtonCell btnCell = new ButtonCell();
		productDetailsButton = new Column<BillingDocument, String>(btnCell) {

			@Override
			public String getValue(BillingDocument object) {
				return "Product Details";
			}
		};

		table.addColumn(productDetailsButton, "");
		table.setColumnWidth(productDetailsButton, 110, Unit.PX);
		
		productDetailsButton.setFieldUpdater(new FieldUpdater<BillingDocument, String>() {
			
			@Override
			public void update(int index, BillingDocument object, String value) {
				
				BillingListPresenter.viewDocumentFlag = false;
				BillingListPresenter.productdetailsFlag = true;				
			}
				
		});
		
	}
	public void addColumnFinalBillingAmount()
	{
		finalTotalAmtColumn=new TextColumn<BillingDocument>() {

			@Override
			public String getValue(BillingDocument object) {
				if(object.getFinalTotalAmt()!=0)
				{
					return nf.format(object.getFinalTotalAmt());
				}
				else{
					return "";
				}
			}
		};
		
		table.addColumn(finalTotalAmtColumn,"Base Billing Amt");
		table.setColumnWidth(finalTotalAmtColumn, 100, Unit.PX);
		finalTotalAmtColumn.setSortable(true);
	}
	
	protected void addSortinggetFinalBillingAmount()
	{
		List<BillingDocument> list=getDataprovider().getList();
		columnSort=new ListHandler<BillingDocument>(list);
		columnSort.setComparator(finalTotalAmtColumn, new Comparator<BillingDocument>()
				{
			@Override
			public int compare(BillingDocument e1,BillingDocument e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getFinalTotalAmt()== e2.getFinalTotalAmt()){
						return 0;}
					if(e1.getFinalTotalAmt()> e2.getFinalTotalAmt()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
}
