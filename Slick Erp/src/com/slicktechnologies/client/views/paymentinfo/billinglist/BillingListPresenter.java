package com.slicktechnologies.client.views.paymentinfo.billinglist;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCGlassPanel;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.i18n.client.TimeZone;
import com.google.gwt.i18n.client.TimeZoneInfo;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.view.client.NoSelectionModel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreen;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreenPresenter;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.BillingDetailsForm;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.BillingDetailsPresenter;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.BillingDetailsSearchPopUp;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.SalesOrderProductTable;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.inventory.GRN;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.salesorder.OtherCharges;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.BillingDocumentDetails;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.SalesOrderProductLineItem;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;

public class BillingListPresenter extends TableScreenPresenter<BillingDocument> {
	
	TableScreen<BillingDocument>form;
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	final GenricServiceAsync async=GWT.create(GenricService.class);
	static GWTCGlassPanel glassPanel=new GWTCGlassPanel();

	public static boolean viewDocumentFlag=true;
	public static boolean productdetailsFlag=false;//Ashwini Patil Date:1-07-2024
	BillingDocument billingObj=null;
	
	/**
	 * Date 12-05-2018
	 * Developer: Vijay
	 * Des :- when multiple billing merged into one billing billing docs and billing docs having different
	 * branches then branch name set to blank
	 * Requirements :- Orion pest
	 */
	public static boolean diffrentBranchflag = false;
	/**
	 * ends here
	 */
	
	
	/**
	 * Date 07-07-2018 By Vijay
	 * Des :- Admin can able to cancel multile billing documents which are not invoiced
	 * Requirements :- NBHC CCPM
	 */
	GeneralServiceAsync genralAsync = GWT.create(GeneralService.class);
	MultipleDocumentCancellationPopup cancellationpopup = new MultipleDocumentCancellationPopup();

	PopupPanel productDetailsPanel;
	ProductDetailsPopup productPopup=new ProductDetailsPopup();
	/**
	 * ends here
	 */
	
	public BillingListPresenter(TableScreen<BillingDocument> view, BillingDocument model) {
		super(view, model);
		form=view;
		/**
		 *  nidhi
		 *  24-07-2017
		 *  this method stop calling for not display billing list on page load
		 *  
		 */
		//view.retriveTable(getBillingListQuery());
		
		/**
		 *  end
		 */
		setTableSelectionOnBillingDocument();
		form.getSearchpopupscreen().getDwnload().setVisible(false);
		
		cancellationpopup.getLblOk().addClickHandler(this);
		cancellationpopup.getLblCancel().addClickHandler(this);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.BILLINGLIST,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
		productPopup.getBtnCancel().addClickHandler(this);
	}
	
	public BillingListPresenter(TableScreen<BillingDocument> view,
			BillingDocument model,MyQuerry querry) {
		super(view, model);
		form=view;
		view.retriveTable(querry);
		setTableSelectionOnBillingDocument();
		
		cancellationpopup.getLblOk().addClickHandler(this);
		cancellationpopup.getLblCancel().addClickHandler(this);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.BILLINGLIST,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
		productPopup.getBtnCancel().addClickHandler(this);
	}
	
	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();
		
		if(text.equals("Process Billing Document")){
			reactOnProcessBilling();
		}
		
		/**
		 * Date 07-07-2018 By Vijay
		 * Des :- Admin can able to cancel multile billing documents which are not invoiced
		 * Requirements :- NBHC CCPM
		 */
		if(text.equals(AppConstants.CANCELBILLING)){
			reactOnCancel();
		}
		/**
		 * ends here
		 */
		
	}

	
	/**
	 * Date 07-07-2018 By Vijay
	 * Des :- Admin can able to cancel multile billing documents which are not invoiced
	 * Requirements :- NBHC CCPM
	 */
	
	private void reactOnCancel() {
		
		if(validateCancellationBillingList()){
			cancellationpopup.showPopUp();
		}
		
	}
	
	private boolean validateCancellationBillingList() {
		List<BillingDocument> BillingDocumentList = getAllSelectedRecords();
		if(BillingDocumentList.size()==0){
			form.showDialogMessage("No Billing Document selected. Please select atleast one Billing Document!");
			return false;
		}
		for(int i=0;i<BillingDocumentList.size();i++){
			if(BillingDocumentList.get(i).getStatus().equalsIgnoreCase(BillingDocument.BILLINGINVOICED)	){
				form.showDialogMessage("Please select only non invoice billing documnet");
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * ends here
	 */
	
	@Override
	public void reactOnPrint() {
		
	}

	@Override
	public void reactOnEmail() {
		
	}
	
	@Override
	public void reactOnDownload() {
		
		if(form.getSuperTable().getDataprovider().getList().size()==0)
		{
			form.showDialogMessage("No record in billing list to download");
		}
		
		if(form.getSuperTable().getDataprovider().getList().size()>0)
		{
		final ArrayList<BillingDocument> conbillingarray=new ArrayList<BillingDocument>();
		List<BillingDocument> listbill=form.getSuperTable().getListDataProvider().getList();
		
		conbillingarray.addAll(listbill); 
		
		
		csvservice.setconbillinglist(conbillingarray, new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);
				
			}

			@Override
			public void onSuccess(Void result) {
				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "csvservlet"+"?type="+14;
				Window.open(url, "test", "enabled");
				
				
////				final String url=gwt + "CreateXLXSServlet"+"?type="+14;
////				Window.open(url, "test", "enabled");
//				
//				boolean addOrderNoAndGrnFlag = false;
//				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "AddOrderNoAndGrnNo")){
//					addOrderNoAndGrnFlag = true;
//				}
//				if(addOrderNoAndGrnFlag){
//					ArrayList<SuperModel> superlist = new ArrayList<SuperModel>();
//					superlist.addAll(conbillingarray);
//					csvservice.getData(superlist, AppConstants.BILLINGDETAILS, new AsyncCallback<ArrayList<SuperModel>>() {
//
//						@Override
//						public void onFailure(Throwable caught) {
//							// TODO Auto-generated method stub
//							
//						}
//
//						@Override
//						public void onSuccess(ArrayList<SuperModel> result) {
//							// TODO Auto-generated method stub
//							ArrayList<PurchaseOrder> polist = new ArrayList<PurchaseOrder>();
//							ArrayList<GRN> grnlist = new ArrayList<GRN>();
//
//							for(SuperModel model : result){
//								if(model.getDocumentName().equals(AppConstants.PURCHASEORDER)){
//									PurchaseOrder po = (PurchaseOrder) model;
//									polist.add(po);
//								}
//								else if(model.getDocumentName().equals(AppConstants.GRN)){
//									GRN grn = (GRN) model;
//									grnlist.add(grn);
//								}
//							}
//							downloadData(true,conbillingarray,polist,grnlist);
//
//						}
//					});
//					
//				}
//				else{
//					downloadData(false,conbillingarray,null,null);
//				}
//				
			}
		});
		}
	}
	
	/*
	 * Method template to make a new model
	 */
	@Override
	protected void makeNewModel() {
		model=new BillingDocument();
	}
	
	public static void initalize()
	{
//		glassPanel.show();
//		Timer timer=new Timer() 
//   	 	{
//			@Override
//			public void run() 
//			{
				BillingListTable table=new BillingListTable();
				BillingListForm form=new BillingListForm(table);
				BillingDetailsSearchPopUp.staticSuperTable=table;
				BillingDetailsSearchPopUp searchPopUp=new BillingDetailsSearchPopUp(false);
				form.setSearchpopupscreen(searchPopUp);
				BillingListPresenter presenter=new BillingListPresenter(form, new BillingDocument());
				form.setToViewState();
				AppMemory.getAppMemory().stickPnel(form);
				glassPanel.hide();
//			}
//		};
//		timer.schedule(3000); 
		
	}
	
	public static void initalize(MyQuerry querry)
	{
		System.out.println("initialize111");
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Billing Details",Screen.BILLINGDETAILS);
		BillingListTable table=new BillingListTable();
		BillingListForm form=new BillingListForm(table);
		BillingDetailsSearchPopUp.staticSuperTable=table;
		BillingDetailsSearchPopUp searchPopUp=new BillingDetailsSearchPopUp(false);
		form.setSearchpopupscreen(searchPopUp);
		BillingListPresenter presenter=new BillingListPresenter(form, new BillingDocument(),querry);
		form.setToViewState();
		/**
		 * Date : 29-07-2017 BY ANIL
		 * This line was added to update the name of screen loaded from any other screen or by clicking on billing list on account model
		 */
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Accounts/Billing List",Screen.BILLINGLIST);
		/**
		 * END
		 */
		AppMemory.getAppMemory().stickPnel(form);
	}
	
	/*
	 * Method template to set the query object in specific to search criteria and return the same
	 * @return MyQuerry object
	 */
	public MyQuerry getBillingListQuery()
	{
		MyQuerry quer=new MyQuerry();
		Vector<Filter> filtervec=new Vector<Filter>();
		List<String> statusList=new ArrayList<String>();
		statusList.add(BillingDocument.CREATED);
		statusList.add(BillingDocument.REQUESTED);
		statusList.add(BillingDocument.APPROVED);
		
		Date billDate=new Date();
		CalendarUtil.addDaysToDate(billDate, 1);
		Console.log("BIll Date"+billDate);
		Console.log("billdate"+billDate);
		
		Filter temp=null;
		
		temp=new Filter();
		temp.setLongValue(model.getCompanyId());
		temp.setQuerryString("companyId");
		filtervec.add(temp);
		
		temp=new Filter();
		temp.setList(statusList);
		temp.setQuerryString("status IN");
		filtervec.add(temp);
		
		/**
		 * Developed by : Rohan added this branch  
		 * 
		 */
		
		List<String> branchList ;
		branchList= new ArrayList<String>();
		System.out.println("item count in branch "+LoginPresenter.globalBranch.size());
		for (int i = 0; i < LoginPresenter.globalBranch.size(); i++) {
			branchList.add(LoginPresenter.globalBranch.get(i).getBusinessUnitName());
		}
		System.out.println("branchList list "+branchList.size());
		
		temp = new Filter();
		temp.setQuerryString("branch IN");
		temp.setList(branchList);
		filtervec.add(temp);
		/**
		 * ends here 
		 */
		
		temp=new Filter();
		temp.setDateValue(billDate);
		temp.setQuerryString("billingDocumentInfo.billingDate <=");
		filtervec.add(temp);
		
		quer.setFilters(filtervec);
		quer.setQuerryObject(new BillingDocument());
		
		
		return quer;
	}
	
	 public void setTableSelectionOnBillingDocument()
	 {
		 //   old code commented by anil
		 
//		 final NoSelectionModel<BillingDocument> selectionModelMyObj = new NoSelectionModel<BillingDocument>();
//	     
//	     SelectionChangeEvent.Handler  tableHandler = new SelectionChangeEvent.Handler() 
//	     {
//	         @Override
//	         public void onSelectionChange(SelectionChangeEvent event) 
//	         {
//	        	 final BillingDocument entity=selectionModelMyObj.getLastSelectedObject();
//	        	 AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
//	        	 BillingDetailsForm.flag=true;
//	        	 BillingDetailsForm.flag1=true;
//	        	 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Accounts/Billing Details",Screen.BILLINGDETAILS);
//	        	 SalesOrderProductTable.newModeFlag=true;
//	        	 final BillingDetailsForm form = BillingDetailsPresenter.initalize();
//	        	
//				 AppMemory.getAppMemory().stickPnel(form);
//                 form.showWaitSymbol();
//				 Timer timer=new Timer() 
//	        	 {
//	 				@Override
//	 				public void run() {
//	 					 form.updateView(entity);
//	 					 form.setToViewState();
//	 					 BillingDetailsForm.flag=false;
//	 					 BillingDetailsForm.flag1=false;
//	 					
//	 					 /**
//	 					  * Steps For Retrieving Charges Based On Contract Id
//	 					  */
//	 					
//	 					MyQuerry querry=new MyQuerry();
//	 					Vector<Filter> filtervec=new Vector<Filter>();
//	 					Filter filter=null;
//	 					filter=new Filter();
//	 					filter.setQuerryString("contractId");
//	 					filter.setIntValue(entity.getContractCount());
//	 					filtervec.add(filter);
//	 					
//	 					filter=new Filter();
//	 					filter.setQuerryString("companyId");
//	 					filter.setLongValue(entity.getCompanyId());
//	 					filtervec.add(filter);
//	 					
//	 					filter=new Filter();
//	 					filter.setQuerryString("identifyOrder");
//	 					if(AppConstants.ORDERTYPESALES.equals(entity.getTypeOfOrder().trim())){
//	 						filter.setStringValue(AppConstants.BILLINGSALESFLOW);
//	 					}
//	 					if(AppConstants.ORDERTYPESERVICE.equals(entity.getTypeOfOrder().trim())){
//	 						filter.setStringValue(AppConstants.BILLINGSERVICEFLOW);
//	 					}
//	 					
//	 					if(AppConstants.ORDERTYPEPURCHASE.equals(entity.getTypeOfOrder().trim())){
//	 						filter.setStringValue(AppConstants.BILLINGPURCHASEFLOW);
//	 					}
//	 					filtervec.add(filter);
//	 					querry.setFilters(filtervec);
//	 					
//	 					querry.setQuerryObject(new TaxesAndCharges());
//	 					
//	 					async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
//
//	 						@Override
//	 						public void onFailure(Throwable caught) {
//	 							form.hideWaitSymbol();
//	 							form.showDialogMessage("An Unexpected Error occured !");
//	 							
//	 						}
//	 						@Override
//	 						public void onSuccess(ArrayList<SuperModel> result) {
//	 							final ArrayList<ContractCharges> arrConCharges=new ArrayList<ContractCharges>();
//	 							for(SuperModel bmodel:result)
//	 							{
//	 								TaxesAndCharges tncentity =(TaxesAndCharges)bmodel;
//	 								List<ContractCharges> lisConCharges=tncentity.getTaxesChargesList();
//	 								arrConCharges.addAll(lisConCharges);
//	 								System.out.println("Size Of Taxes And Charges"+arrConCharges.size());
//	 							}
//	 							if(entity.getBillingOtherCharges().size()!=0){
//	 								List<ContractCharges> updatedbalanceLis=updateChargesBalance(arrConCharges,entity.getBillingOtherCharges(),entity.getStatus());
//	 								ArrayList<ContractCharges> updatedBalanceArr=new ArrayList<ContractCharges>();
//	 								updatedBalanceArr.addAll(updatedbalanceLis);
//	 								form.getBillingChargesTable().setValue(updatedBalanceArr);
//	 							}
//	 							if(entity.getBillingOtherCharges().size()==0){
//	 								form.getBillingChargesTable().setValue(arrConCharges);
//	 							}
//	 						}
//	 					});
//	 					 
//	 					
//	 			/**
//	 			 * End Of Retrieving Charges Table		
//	 			 */
//	 					 double retrieveTotalAmt=form.getSalesProductTable().calculateTotalBillAmt()+form.getBillingTaxTable().calculateTotalTaxes()+form.getBillingChargesTable().calculateTotalBillingCharges();
//	 					// retrieveTotalAmt=retrieveTotalAmt+calculateTotalTaxAmt(entity.getBillingTaxes(),entity.getArrPayTerms().get(0).getPayTermPercent());
//	 					 BillingDocumentDetails billingdoc=new BillingDocumentDetails();
//	 					 //**************anil ***********
//	 					 if(entity.getContractCount()!=0)
//	 						billingdoc.setOrderId(entity.getContractCount());
//	 					 if(entity.getCount()!=0)
//	 						billingdoc.setBillId(entity.getCount());
//	 					 if(entity.getBillingDate()!=null)
//	 						billingdoc.setBillingDate(entity.getBillingDate());
//	 					 if(entity.getBillingCategory()!=null)
//	 						billingdoc.setBillingCategory(entity.getBillingCategory());
//	 					 if(entity.getBillingType()!=null)
//	 						billingdoc.setBillingType(entity.getBillingType());
//	 					 if(entity.getBillingGroup()!=null)
//	 						billingdoc.setBillingGroup(entity.getBillingGroup());
//	 					 if(entity.getTotalBillingAmount()!=null)
//	 						billingdoc.setBillAmount(retrieveTotalAmt);
//	 					 if(entity.getStatus()!=null)
//	 						billingdoc.setBillstatus(entity.getStatus());
//	 					 
//	 					 form.getBillingDocumentTable().getDataprovider().getList().add(billingdoc);
//					     BillingDetailsPresenter presenter=(BillingDetailsPresenter) form.getPresenter();
//					     presenter.form.setMenus();
//					     form.hideWaitSymbol();
//					     SalesOrderProductTable.newModeFlag=false;
//					     
//					     
//	 				}
//	 			};
//	             timer.schedule(3000); 
//	          }
//	     };
//	     selectionModelMyObj.addSelectionChangeHandler(tableHandler);
//	     form.getSuperTable().getTable().setSelectionModel(selectionModelMyObj);
		 
		 //  new code by anil 

		 Console.log("----------------------- 1");
		 System.out.println("----------------------- 1");
		 final NoSelectionModel<BillingDocument> selectionModelMyObj = new NoSelectionModel<BillingDocument>();
		 Console.log("----------------------- 2");
		 System.out.println("----------------------- 2");
	     SelectionChangeEvent.Handler  tableHandler = new SelectionChangeEvent.Handler() 
	     {
	         @Override
	         public void onSelectionChange(SelectionChangeEvent event) 
	         {
	        	 Console.log("----------------------- 2.1 "+viewDocumentFlag);
	        	 Timer timer1=new Timer() 
	        	 {
	        		 
	 				@Override
	 				public void run() {
	        	 
	 					 Console.log("----------------------- 2.2 "+viewDocumentFlag);
	        	 if(viewDocumentFlag){
	        		 Console.log("----------------------- 3 "+viewDocumentFlag);
		        	 System.out.println("----------------------- 3"+viewDocumentFlag);
		        	 final BillingDocument entity=selectionModelMyObj.getLastSelectedObject();
		        	 AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
		        	 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Accounts/Billing Details",Screen.BILLINGDETAILS);
		        	 SalesOrderProductTable.newModeFlag=true;
		        	 Console.log("----------------------- 4");
		        	 System.out.println("----------------------- 4");
		        	 final BillingDetailsForm form = BillingDetailsPresenter.initalize();
	//	        	 form.setToViewState();
					 AppMemory.getAppMemory().stickPnel(form);
	                 form.showWaitSymbol();
					 Timer timer=new Timer() 
		        	 {
		 				@Override
		 				public void run() {
		 					Console.log("----------------------- 5");
		 					 System.out.println("----------------------- 5");
		 					 form.updateView(entity);
		 					 form.setToViewState();
						     BillingDetailsPresenter presenter=(BillingDetailsPresenter) form.getPresenter();
						     presenter.form.setMenus();
						     form.hideWaitSymbol();
		 				}
		 			};
		            timer.schedule(3000); 
		            Console.log("----------------------- 6");
		            System.out.println("----------------------- 6");
	        	 }else{

						if(productdetailsFlag)
						{
							
							productdetailsFlag=false;
							final BillingDocument entity = selectionModelMyObj.getLastSelectedObject();
							billingObj=entity;
							showProductDetails(entity);									
						}
						viewDocumentFlag = true;
					
	        		 Console.log("----------------------- 6.1 "+viewDocumentFlag);
	        		 System.out.println("----------------------- 6.1");
	        		 viewDocumentFlag=true; 
	        		 Console.log("----------------------- 6.2 "+viewDocumentFlag);
	        	 }
	             
	 			}
		 		};
		        timer1.schedule(1000); 
	             
	             
	          }
	     };
	     System.out.println("----------------------- 7");
	     selectionModelMyObj.addSelectionChangeHandler(tableHandler);
	     System.out.println("----------------------- 8");
	     form.getSuperTable().getTable().setSelectionModel(selectionModelMyObj);
	     System.out.println("----------------------- 9");
	     
		 
	 }
	 
	 private void reactOnProcessBilling()
	 {
		 boolean validationresult=this.validateAllSelectedBillingDocument();
		 System.out.println("Validation Result"+validationresult);
		 if(validationresult==true)
		 {
			 /**
			  * Old Code commented by ANil On 23-10-2017
			  */
//			 initializeToBillingDetails();
			 /**
			  * New Code
			  */
			 initializeToNewBillingDetails();
		 }
	 }
	 
	 /**
	  * Date : 18-10-2017 BY ANIL
	  * Setting order id and billing in in other charges list used when we club multiple bill and generate single bill.
	  */
	 private List<OtherCharges> fillBillingOtherChargesTable() {
		 List<BillingDocument> listsel=getAllSelectedRecords();
		 ArrayList<OtherCharges> arrBilingOtherCharges=new ArrayList<OtherCharges>();
		
		 for(BillingDocument object:listsel){
			 if(object.getOtherCharges()!=null){
				 for(OtherCharges oc:object.getOtherCharges()){
					oc.setOrderId(object.getContractCount());
					oc.setBillingId(object.getCount());
				 }
				 arrBilingOtherCharges.addAll(object.getOtherCharges()); 
			 }
		 }
		 
		 return arrBilingOtherCharges;
	}
	 
	 /**
	  * Date : 23-10-2017 BY ANIL
	  */
	 private void initializeToNewBillingDetails() {

		 AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
		 final BillingDetailsForm form = BillingDetailsPresenter.initalize();
		 BillingDetailsForm.flag1=false;
		 BillingDetailsForm.flag=true;
		 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Billing Details",Screen.BILLINGDETAILS);
		 AppMemory.getAppMemory().stickPnel(form);
//		 form.showWaitSymbol();
		 // Get All Selected Records which are in approved status.
		 final List<BillingDocument> listsel=getAllSelectedRecords();
		 Console.log("1.Process listselcount="+listsel.size());
		  /** date 1.12.2018 added by komal for cnc combined invoice**/
		 String subBillType = listsel.get(0).getSubBillType().trim();
		 for(BillingDocument doc : listsel){
			 if(doc.getSubBillType() != null && !doc.getSubBillType().equals("")){
			 if(Invoice.checkBillSubType(doc.getSubBillType())){
				 subBillType = AppConstants.CONSOLIDATEBILL;
				 break;
			 }
			 }
		 }
		 Console.log("2.Process");
		 // fill selected billing documents in billing table in billing details screen.
		 List<BillingDocumentDetails> billingTableData=this.fillBillingDocumentTableOnClick();
		 final ArrayList<BillingDocumentDetails> multipleBillDocs=new ArrayList<BillingDocumentDetails>();
		 multipleBillDocs.addAll(billingTableData);
		 
		 Console.log("3.Process");
		 // use for merging products of multiple contract or orders 
		 List<SalesOrderProductLineItem> billingProductTableData=this.fillBillingProductTableOnClick();
		 final ArrayList<SalesOrderProductLineItem> multipleBillProdDocs=new ArrayList<SalesOrderProductLineItem>();
		 multipleBillProdDocs.addAll(billingProductTableData);
		 
		 Console.log("4.Process");
		 /**
		  * Date : 18-10-2017 BY ANIL
		  * Merging other charges 
		  */
		 List<OtherCharges> otherChrgList=fillBillingOtherChargesTable();
		 ArrayList<OtherCharges> othChrgsList=new ArrayList<OtherCharges>(otherChrgList);
		 double sumOfOtherChargesAmt=0;
		 for(OtherCharges obj:othChrgsList){
			 sumOfOtherChargesAmt=sumOfOtherChargesAmt+obj.getAmount();
		 }
		 Console.log("5.Process");
		 /**
		  * End
		  */
		 
		 /**
		  * Date : 23-10-2017 BY ANIL
		  * Calculating sum of Total amount ,discount,other charges and round off. 
		  */
		 double sumOfTotalAmount=0;
		 double sumOfDiscountAmt=0;
		 double sumOfRoundOffAmt=0;
		 HashSet<Integer> serivceId = new HashSet<Integer>();
		 for(BillingDocument entity:listsel){
			 for(SalesOrderProductLineItem obj:entity.getSalesOrderProducts()){
				 if(obj.getBasePaymentAmount()!=0){
					 sumOfTotalAmount=sumOfTotalAmount+obj.getBasePaymentAmount();
				 }else{
					 sumOfTotalAmount=sumOfTotalAmount+obj.getBaseBillingAmount(); 
				 }
			 }
			 sumOfDiscountAmt=sumOfDiscountAmt+entity.getDiscountAmt();
			 sumOfRoundOffAmt=sumOfRoundOffAmt+entity.getRoundOffAmt();
			 
			 /** 15-10-2018
				 * nidhi 
				 *  ||*
				 */
				if(entity.getRateContractServiceId()!=0){
					serivceId.add(entity.getRateContractServiceId());
				}else if(entity.getServiceId().size()>0){
					serivceId.addAll(entity.getServiceId());
				}
		 }
		
		 /**
		  * End
		  */
		 
		 Console.log("6.Process");
		 
		 
		 // Calculate billing amount using selected billing documents
		 final double calBillingAmount=this.calculateTotalBillingAmount(billingTableData);
		 final String typeOrder=listsel.get(0).getTypeOfOrder().trim();
		 // Get Order Count
		 final int contractid=listsel.get(0).getContractCount();
		 final int billingid=listsel.get(0).getCount();
		 /**
		  * Date : 18-10-2017 BY ANIL
		  */
		 final BillingDocument billingEntity=listsel.get(0);
		 Console.log("Get contract count in -- " + billingEntity.getContractCount());
		 billingEntity.setTotalAmount(sumOfTotalAmount);
		 billingEntity.setDiscountAmt(sumOfDiscountAmt);
		 billingEntity.setFinalTotalAmt(sumOfTotalAmount-sumOfDiscountAmt);
		 billingEntity.setOtherCharges(othChrgsList);
		 billingEntity.setTotalOtherCharges(sumOfOtherChargesAmt);
		 billingEntity.setRoundOffAmt(sumOfRoundOffAmt);
		 billingEntity.setSalesOrderProducts(multipleBillProdDocs);
		 billingEntity.setArrayBillingDocument(multipleBillDocs);
		 /**
		  * End
		  */
		 
		/**
		 * Date 14 Feb 2017
		 * added by vijay
		 * when multiple billing document one invoice then i need billing period From date first billing document 
		 * and billing period To date need last billing document
		 * so i am sorting billing document by billing id then i set to billing period From date and To date.
		 */
		 form.setToViewState();
		 Console.log("7.Process");
		 final List<BillingDocument> selectedlist=getAllSelectedRecords();
		 	System.out.println("Selected billing docs ===="+selectedlist.size());
		 	
		 	/*** Date 11-08-2020 By Vijay if CNC project Name and Contract number different in different billing docs then set as blank **/
		 	String cncProjectName = "";
		 	if(selectedlist.get(0).getProjectName()!=null){
		 		cncProjectName = selectedlist.get(0).getProjectName();
		 	}
		 	String cncContractNumber = "";
		 	if(selectedlist.get(0).getCncContractNumber()!=null){
		 		cncContractNumber = selectedlist.get(0).getCncContractNumber();
		 	}
		 	
			billingEntity.setProjectName(cncProjectName);
			billingEntity.setCncContractNumber(cncContractNumber);

			int creditperiod =  selectedlist.get(0).getCreditPeriod();
			billingEntity.setCreditPeriod(creditperiod);
			
		 	Collections.sort(selectedlist, new Comparator<BillingDocument>() {
			@Override
			public int compare(BillingDocument o1, BillingDocument o2) {
				Integer count1= o1.getCount();
				Integer count2 = o2.getCount();
				return count1.compareTo(count2);
			}
		});
		 	Console.log("8.Process");
		Date billingperiodTodate= null;	
		for(int i=0;i<selectedlist.size();i++){
			if(i==0){
			    form.getDbbillingperiodFromDate().setValue(selectedlist.get(i).getBillingPeroidFromDate());
			}
			if(i==selectedlist.size()-1){
				billingperiodTodate = selectedlist.get(i).getBillingPeroidToDate();
			}
			
			if(selectedlist.get(i).getProjectName()!=null && !selectedlist.get(i).getProjectName().equals(cncProjectName)){
				billingEntity.setProjectName("");
			}
			if(selectedlist.get(i).getCncContractNumber()!=null && !selectedlist.get(i).getCncContractNumber().equals(cncContractNumber)){
				billingEntity.setCncContractNumber("");
			}
			if(selectedlist.get(i).getCreditPeriod() != creditperiod){
				billingEntity.setCreditPeriod(0);
			}
		}
		Console.log("9.Process");
		/**
		 * End here
		 */
		
		List<ContractCharges> blankList=new ArrayList<ContractCharges>();
		billingEntity.setBillingOtherCharges(blankList);
		if(isMultipleContractBilling()){
//			billingEntity.setContractCount(0);
			Console.log("9.Process");
			/**
			 * @author Anil
			 * @since 13-08-2020
			 * Earlier for multicontract bill we were setting contract count as zero which causes error at the 
			 * time of invoice printing
			 * So On Approval of Nitin sir and vaishali commenting this code
			 */
//			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "OnlyForOrionPestSolutions")==false){
//				billingEntity.setContractCount(0);
//			}
			
			
			billingEntity.setTotalSalesAmount(0);
			form.multiContractFlag=true;
		}/*else{
			billingEntity.setContractCount(contractid);
		}*/
		
		//vijay for billing peroid To date
		if(billingperiodTodate!=null)
			billingEntity.setBillingPeroidToDate(billingperiodTodate);
		billingEntity.setStatus(BillingDocument.APPROVED);
		
		

		/**
		 *  nidhi
		 *   12-1-2017
		 *   for get proper billing period from date to date if multiple contract is there
		 *   
		 */
		
		/*
		 * Ashwini Patil 
		 * Date:10-06-2024
		 * ultima search uploaded contracts and bills got created as per outstanding amount but billing period is blank and because of that multiple bills are not getting processed.
		 * Handling this with condition, if there is no billing period in bill then in invoice billing period will be set blank, which canbe changed later by editing invoice
		 */ 
		if(form.multiContractFlag){
			
			Console.log("10.Process multiContractFlag");
			Date Todate=null;
			Date fromDate=null;
			if(selectedlist.get(0).getBillingPeroidToDate()!=null&&selectedlist.get(0).getBillingPeroidFromDate()!=null){
				Console.log("selectedlist.get(0).getBillingPeroidToDate()="+selectedlist.get(0).getBillingPeroidToDate());
				Todate= selectedlist.get(0).getBillingPeroidToDate();
				fromDate = selectedlist.get(0).getBillingPeroidFromDate();
			}
			Console.log("10.1.Process");
			for(int i=0;i<selectedlist.size();i++){
				if(fromDate!=null&&Todate!=null&& selectedlist.get(i).getBillingPeroidToDate()!=null&&selectedlist.get(i).getBillingPeroidFromDate()!=null){
					if(fromDate.before(selectedlist.get(i).getBillingPeroidFromDate())){
						fromDate = selectedlist.get(i).getBillingPeroidFromDate();
					}
					
					if((selectedlist.get(i).getBillingPeroidToDate()).after(Todate) ){
						Todate = selectedlist.get(i).getBillingPeroidToDate();
					}
				}
			
			}
			Console.log("10.2.Process");
			if(fromDate!=null)
			 form.getDbbillingperiodFromDate().setValue(fromDate);
			if(Todate!=null)
			 form.getDbbillingperiodToDate().setValue(Todate);
			 Date contractfromDate=null;
			 Date contractTodate=null;
			 if(selectedlist.get(0).getBillingPeroidToDate()!=null)
				 contractTodate= selectedlist.get(0).getBillingPeroidToDate();
			 if(selectedlist.get(0).getContractStartDate()!=null)
				 contractfromDate = selectedlist.get(0).getContractStartDate();
			 Console.log("10.3.Process");
			 for(int i=0;i<selectedlist.size();i++){
				 if(fromDate!=null&&Todate!=null){
						
					if(selectedlist.get(i).getContractStartDate()!=null && fromDate.before(selectedlist.get(i).getContractStartDate())){
						contractfromDate = selectedlist.get(i).getContractStartDate();
					}
					
					if(selectedlist.get(i).getContractEndDate()!=null && (selectedlist.get(i).getContractEndDate()).after(Todate) ){
						contractTodate = selectedlist.get(i).getContractEndDate();
					}
				 }
				}
			 Console.log("10.4.Process");
//			 billingEntity.setContractCount(0);
			 /**
			 * @author Anil
			 * @since 13-08-2020
			 * Earlier for multicontract bill we were setting contract count as zero which causes error at the 
			 * time of invoice printing
			 * So On Approval of Nitin sir and vaishali commenting this code
			 */
//			 if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "OnlyForOrionPestSolutions")==false){
//					billingEntity.setContractCount(0);
//			 }
			 billingEntity.setRemark("This Invoice is Created against multiple contract of same customer As dated on :" +AppUtility.parseDate(new Date()));
			
			 if(contractfromDate!=null) {
				 billingEntity.setContractStartDate(contractfromDate);
				 billingEntity.setConStartDur(contractfromDate);
			 }
			 if(contractTodate!=null) {
				 billingEntity.setContractEndDate(contractTodate);				 
				 billingEntity.setConEndDur(contractTodate);
			 }
			 
			 Console.log("11.Process");
		}else if(form.multiContractFlag){
			 billingEntity.setRemark("This Invoice is Created against multiple contract of same customer As dated on :" +AppUtility.parseDate(new Date()));
			
		}

		/**
		 *  end
		 */
		/**
		 * nidhi
		 * 10-4-2018
		 * for multiple billing different approval person 
		 */
		
		boolean valApproverName = validateApproverName(listsel);
		
		if(!valApproverName)
		{
			billingEntity.setApproverName(LoginPresenter.loggedInUser);
		}
		/**
		 * end
		 */
		
		
		/**
		 * Date 12-05-2018
		 * Developer: Vijay
		 * Des :- when multiple billing merged into one billing billing docs and billing docs having different
		 * branches then branch name set to blank
		 * Requirements :- Orion pest
		 */
		if(diffrentBranchflag){
			billingEntity.setBranch("");
		}
		/**
		 * Ends here
		 */
		/**
		 * nidhi
		 *  15-10-2018 
		 *  ||*
		 */
		if(serivceId.size()>0){
			billingEntity.getServiceId().clear();
			billingEntity.getServiceId().addAll(serivceId);
		}
		/**
		 *end
		 */
		
		 /** date 1.12.2018 added by komal for cnc combined invoice**/
		billingEntity.setSubBillType(subBillType);
		 /** date 1.12.2018 added by komal for cnc combined invoice**/
		billingEntity.setSubBillType(subBillType);
		if(subBillType.equalsIgnoreCase(AppConstants.CONSOLIDATEBILL)){
			int count = 0;
			if(billingEntity.getRefNumber() != null && !billingEntity.getRefNumber().equalsIgnoreCase("")){
				try{
				count = Integer.parseInt(billingEntity.getRefNumber());
				}catch(Exception e){
					count = 0;
				}
			} 
			billingEntity.setContractCount(count);
		}
		Console.log("12.Process");
		/**
		 * @author Anil @since 05-04-2021
		 * Adding Management fees percent 
		 * For Alkosh raised by Rahul Tiwari
		 */
		double managementFeePer=0;
		for(BillingDocument bill:selectedlist){
			managementFeePer+=bill.getManagementFeesPercent();
		}
		if(managementFeePer!=0){
			managementFeePer=managementFeePer/selectedlist.size();
		}
		billingEntity.setManagementFeesPercent(managementFeePer);
		
		/**
		 * @author Anil @since 15-05-2021
		 * Setting type of order
		 * For envocare raised by vaishnavi
		 */
		billingEntity.setTypeOfOrder(typeOrder);
		

		
		Timer t = new Timer() {
			@Override
			public void run() {
				Console.log("Get contract count in set view-- " + billingEntity.getContractCount());
				form.updateView(billingEntity);
				form.getDototalbillamt().setValue(calBillingAmount);
				BillingDetailsPresenter presenter=(BillingDetailsPresenter) form.getPresenter();
			    presenter.form.setMenus();
			    presenter.multipleContractBillingInvoiceFlag=true;
				form.getBillingDocumentTable().getDataprovider().getList().addAll(multipleBillDocs);
				System.out.println("BILLING DOCUMNET TABLE......! "+form.getBillingDocumentTable().getDataprovider().getList().size());
				form.getSalesProductTable().getDataprovider().setList(multipleBillProdDocs);
				System.out.println("BILLING PRODUCT TABLE......! "+form.getSalesProductTable().getDataprovider().getList().size());
				presenter.reactOnProductsChange();
				BillingDetailsForm.flag=false;
				BillingDetailsForm.flag1=true;
				/**
				 *  nidhi
				 *  15-01-2018
				 */
				Console.log("13.Process");
				form.setToViewState();
				Console.log("14.Process");
				form.getSalesProductTable().getTable().redraw();
				form.getSalesProductTable().setEnable(false);
				/**
				 *  end
				 */
//				form.hideWaitSymbol();
			}
		};
		t.schedule(2000);
	 }
	 /**
	  * End
	  */

	private void initializeToBillingDetails()
	 {
		 AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
		 final BillingDetailsForm form = BillingDetailsPresenter.initalize();
		 BillingDetailsForm.flag1=false;
		 BillingDetailsForm.flag=true;
		 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Billing Details",Screen.BILLINGDETAILS);
//		 form.setToViewState();
		 AppMemory.getAppMemory().stickPnel(form);
		 form.showWaitSymbol();
		 
		 
		 // Get All Selected Records which are in approved status.
		 final List<BillingDocument> listsel=getAllSelectedRecords();
		 
		 // fill selected billing documents in billing table in billing details screen.
		 List<BillingDocumentDetails> billingTableData=this.fillBillingDocumentTableOnClick();
		 final ArrayList<BillingDocumentDetails> multipleBillDocs=new ArrayList<BillingDocumentDetails>();
		 multipleBillDocs.addAll(billingTableData);
		 
		 
		 // use for merging products of multiple contract or orders 
		 List<SalesOrderProductLineItem> billingProductTableData=this.fillBillingProductTableOnClick();
		 final ArrayList<SalesOrderProductLineItem> multipleBillProdDocs=new ArrayList<SalesOrderProductLineItem>();
		 multipleBillProdDocs.addAll(billingProductTableData);
		 
		 List<ContractCharges> billingOtherChargesTableData=this.fillBillingOtherChargesTableOnClick();
		 System.out.println("CONTRACT CHARGES LIST SIZE :: "+billingOtherChargesTableData.size());
		 final ArrayList<ContractCharges> multipleBillOtherChargeDocs=new ArrayList<ContractCharges>();
		 multipleBillOtherChargeDocs.addAll(billingOtherChargesTableData);
		 
		 
		 // Calculate billing amount using selected billing documents
		 final double calBillingAmount=this.calculateTotalBillingAmount(billingTableData);
		 
		 final String typeOrder=listsel.get(0).getTypeOfOrder().trim();
		 
		 
		 // Get Order Count
		 final int contractid=listsel.get(0).getContractCount();
		 final int billingid=listsel.get(0).getCount();
		 
		 Timer timer=new Timer() 
    	 {
			@Override
			public void run() 
			{
				 MyQuerry querry=new MyQuerry();
				 Vector<Filter> filtrvec=new Vector<Filter>();
				 Filter filterval=null;
				 
				 filterval=new Filter();
				 filterval.setQuerryString("contractCount");
				 filterval.setIntValue(contractid);
				 filtrvec.add(filterval);
				 
				 filterval=new Filter();
				 filterval.setQuerryString("count");
				 filterval.setIntValue(billingid);
				 filtrvec.add(filterval);
				 
				 filterval=new Filter();
				 filterval.setQuerryString("companyId");
				 filterval.setLongValue(model.getCompanyId());
				 filtrvec.add(filterval);
				 
				 filterval=new Filter();
				 filterval.setQuerryString("typeOfOrder");
				 if(AppConstants.ORDERTYPESALES.equals(typeOrder)){
					 filterval.setStringValue(AppConstants.ORDERTYPESALES);
				 }
				 if(AppConstants.ORDERTYPESERVICE.equals(typeOrder)){
					 filterval.setStringValue(AppConstants.ORDERTYPESERVICE);
				 }
				 if(AppConstants.ORDERTYPEPURCHASE.equals(typeOrder)){
					 filterval.setStringValue(AppConstants.ORDERTYPEPURCHASE);
				 }
				 filtrvec.add(filterval);
				 
				 querry.setFilters(filtrvec);
				 querry.setQuerryObject(new BillingDocument());
				 async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
						
						@Override
						public void onFailure(Throwable caught) {
							form.hideWaitSymbol();
							form.showDialogMessage("An Unexpected error occurred!");
						}
			
						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							
							/**
							 * Date 14 Feb 2017
							 * added by vijay
							 * when multiple billing document one invoice then i need billing period From date first billing document 
							 * and billing period To date need last billing document
							 * so i am sorting billing document by billing id then i set to billing period From date and To date.
							 */
							 form.setToViewState();
							 
							 
							 final List<BillingDocument> selectedlist=getAllSelectedRecords();
							 	System.out.println("Selected billing docs ===="+selectedlist.size());
							 	Collections.sort(selectedlist, new Comparator<BillingDocument>() {
						 
								@Override
								public int compare(BillingDocument o1, BillingDocument o2) {
									
									Integer count1= o1.getCount();
									Integer count2 = o2.getCount();
									
									return count1.compareTo(count2);
								}
							});
								
							 Date billingperiodTodate= null;	
							for(int i=0;i<selectedlist.size();i++){
								System.out.println("billing id =="+selectedlist.get(i).getCount());
								if(i==0){
								    form.getDbbillingperiodFromDate().setValue(selectedlist.get(i).getBillingPeroidFromDate());
								}
								System.out.println("i==="+i);
								System.out.println("value"+(selectedlist.size()-1));
								if(i==selectedlist.size()-1){
									
									billingperiodTodate = selectedlist.get(i).getBillingPeroidToDate();
									System.out.println("To Date =="+selectedlist.get(i).getBillingPeroidToDate());
 								}
 
 							}
							 
							/**
							 * End here
							 */
							
							for(SuperModel model:result)
							{
//								BillingDocument bdentity = (BillingDocument)model;
//								form.updateView(bdentity);
//								BillingDetailsForm.flag1=false;
//								BillingDetailsForm.flag=true;
//								form.getDototalbillamt().setValue(calBillingAmount);
//								bdentity.setStatus(BillingDocument.APPROVED);
//								BillingDetailsPresenter presenter=(BillingDetailsPresenter) form.getPresenter();
//							    presenter.form.setMenus();
							    
								
								BillingDocument bdentity = (BillingDocument)model;
								List<ContractCharges> blankList=new ArrayList<ContractCharges>();
								bdentity.setBillingOtherCharges(blankList);
								if(isMultipleContractBilling()){
									bdentity.setContractCount(0);
									bdentity.setTotalSalesAmount(0);
									form.multiContractFlag=true;
								}
								
								//vijay for billing peroid To date
								if(billingperiodTodate!=null)
								bdentity.setBillingPeroidToDate(billingperiodTodate);
								
								form.updateView(bdentity);
								BillingDetailsForm.flag1=false;
								BillingDetailsForm.flag=true;
								form.getDototalbillamt().setValue(calBillingAmount);
								bdentity.setStatus(BillingDocument.APPROVED);
								BillingDetailsPresenter presenter=(BillingDetailsPresenter) form.getPresenter();
							    presenter.form.setMenus();
							    presenter.multipleContractBillingInvoiceFlag=true;
							    
							}
							form.getBillingDocumentTable().getDataprovider().getList().addAll(multipleBillDocs);
							System.out.println("BILLING DOCUMNET TABLE......! "+form.getBillingDocumentTable().getDataprovider().getList().size());
//							form.getSalesProductTable().connectToLocal();
							form.getSalesProductTable().getDataprovider().setList(multipleBillProdDocs);
							System.out.println("BILLING PRODUCT TABLE......! "+form.getSalesProductTable().getDataprovider().getList().size());
							
							Timer t=new Timer() {
								@Override
								public void run() {
									form.getBillingChargesTable().getDataprovider().setList(multipleBillOtherChargeDocs);
									System.out.println("BILLING OTHER CHARGES TABLE......! "+form.getBillingChargesTable().getDataprovider().getList().size());
									form.calculateNetPayable();
								}
							};
							t.schedule(2000);
							}
						 });
				 
				 form.hideWaitSymbol();
			}
    	 };
    	 timer.schedule(3000); 
	 }
	 
	 
	 private List<BillingDocumentDetails> fillBillingDocumentTableOnClick()
	 {
		 List<BillingDocument> listsel=getAllSelectedRecords();
		 ArrayList<BillingDocumentDetails> arrBilingDoc=new ArrayList<BillingDocumentDetails>();
		 for(int i=0;i<listsel.size();i++)
		 {
			 BillingDocumentDetails billingdoc=new BillingDocumentDetails();
			 
			 billingdoc.setOrderId(listsel.get(i).getContractCount());
			 
			 billingdoc.setBillId(listsel.get(i).getCount());
			 billingdoc.setBillingDate(listsel.get(i).getBillingDate());
			 billingdoc.setBillingCategory(listsel.get(i).getBillingCategory());
			 billingdoc.setBillingGroup(listsel.get(i).getBillingGroup());
			 billingdoc.setBillingType(listsel.get(i).getBillingType());
			 billingdoc.setBillAmount(listsel.get(i).getTotalBillingAmount());
			 billingdoc.setBillstatus(listsel.get(i).getStatus());
			 arrBilingDoc.add(billingdoc);
		 }
		 return arrBilingDoc;
	 }
	 
	 /**
	  * This method is used for calculating billing amount when checkboxes in billing list is selected.
	  * If one or more than one billing documents are selected then the total billing amount is calculated.
	  * This method calulates and returns the total billing amount.
	  * @param liscalbilling
	  * @return
	  */
	 
	 private double calculateTotalBillingAmount(List<BillingDocumentDetails> liscalbilling)
	 {
		 System.out.println("In Calculate Total Billing Amount");
		 System.out.println("Size Here CTBA"+liscalbilling.size());
		 double billingAmt=0;
		 for(int i=0;i<liscalbilling.size();i++)
		 {
			 billingAmt=billingAmt+liscalbilling.get(i).getBillAmount();
		 }
		 System.out.println("Calculated Form Bill AMt"+billingAmt);
		 return billingAmt;
	 }
	 
	/* private double calculateIndividualBillingAmount(List<BillingDocumentDetails> liscalbilling)
	 {
		 double tableBillingAmt=0;
		 List<SalesOrderProductLineItem> salesOrderProd=null;
		 for(int i=0;i<liscalbilling.size();i++)
		 {
			 salesOrderProd=liscalbilling.get(i).get
		 }
	 }*/
	 
	 
	 
	 /**************************************************************************************************/
	 
	 /**
	  * This method is used for getting all selected records through billing list.
	  * When the checkboxes are selected in billing list then these records are fetched in arraylist
	  * and this method returns that list
	  * @return
	  */
	 
	 private List<BillingDocument> getAllSelectedRecords()
	 {
		 List<BillingDocument> lisbillingtable=form.getSuperTable().getDataprovider().getList();
		 ArrayList<BillingDocument> selectedlist=new ArrayList<BillingDocument>();
		 for(BillingDocument doc:lisbillingtable)
		 {
			 if(doc.getRecordSelect()==true)
				 selectedlist.add(doc);
		 }
		 return selectedlist;
	 }
	 
	 private boolean validateAllSelectedBillingDocument()
	 {
		 String msg="";
			boolean temp=true;
			List<BillingDocument>billingdoc= getAllSelectedRecords();
			if(billingdoc.size()==0)
			{
				form.showDialogMessage("No Billing Document selected. Please select atleast one Billing Document!");
				return false;
			}
			
			//This line is commented is by Anil Date 0/10/2015
			/**
			 *  nidhi
			 *  12-01-2018
			 *  commented belowed line for stop validation stop multiple contract billing 
			 *  document check.
			 */
//			boolean valContractId=validateContractId(billingdoc);
			
			/**
			 *  end
			 */
		
			
			
//			boolean valInvoiceDate=validateInvoiceDate(billingdoc);
//			boolean valPaymentDate=validatePaymentDate(billingdoc);
			
			boolean valCustomer=validateCustomer(billingdoc);
			System.out.println("Customer Flag :: "+valCustomer);
			
			boolean valAccountType=validateAccountType(billingdoc);
			
			boolean valApproverName=validateApproverName(billingdoc);
			boolean valStatus=validateStatus(billingdoc);
			
			
			//This line is commented is by Anil Date 0/10/2015
//			if(valContractId==false)
//			{
//				msg="Contract ID of all selected Billing Document should be same "+"\n";
//				temp=false;
//			}
//			
			if(valCustomer==false){
				msg=msg+"Customer of all selected Billing Document should be same "+"\n";
				temp=false;
			}
			
			if(valAccountType==false){
				msg=msg+"Account Type of all selected Billing Document should be same "+"\n";
				temp=false;
			}
			
			
//			if(valInvoiceDate==false)
//			{
//				msg=msg+"Invoice Date of all selected Billing Document should be same"+"\n";
//				temp=false;
//			}
//			
//			if(valPaymentDate==false)
//			{
//				msg=msg+"Payment Date of all selected Billing Document should be same"+"\n";
//				temp=false;
//			}
			
			/*
			 * nidhi
			 * stop validation for multiple billing diff approval name
			 * as NBHC requirment
			 * if(valApproverName==false)
			{
				msg=msg+"Approver Name of all selected Billing Document should be same"+"\n";
				temp=false;
			}*/
			//This line is commented is by Anil Date 0/10/2015
			/**
			 *  nidhi
			 *  12-1-2018
			 *  commented belowes condition for stop varify single contract only
			 */
//			if(valContractId==false)
//			{
//				msg="Contract ID of all selected Billing Document should be same "+"\n";
//				temp=false;
//			}
			/**
			 *  end
			 */
			
			/**
			 * Date 12-05-2018
			 * Developer: Vijay
			 * Des :- when multiple billing merged into one billing billing docs and billing docs having different
			 * branches then branch name set to blank
			 * Requirements :- Orion pest
			 */
			boolean branchValidationFlag = AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "EnableSetBranchAsServicingBranch");
			diffrentBranchflag = false;
			/**
			 * ends here
			 */
			
			/**
			 *  nidhi
			 *  13-1-2018
			 *  for branch validation 
			 */
			boolean valBranch = validateContractBranch(billingdoc,branchValidationFlag);
			/**
			 *  end
			 */
			
			if(valStatus==false)
			{
				msg=msg+"Status of all selected Billing Document should be Approved/Created"+"\n";
				temp=false;
			}
	        if(temp==false)
	        {
	        	form.showDialogMessage(msg);
	        	return false;
	        }
	        
	        /**
	         * nidhi
	         * 13-1-2018
	         * check for branch
	         * 
	         */
	        if(!diffrentBranchflag){ // vijay added on 12-05-2018 if condition for orion pest not required branch validation if process config is active
		      if(!valBranch){
		    	  return false;
		      }
	        } 
		      /**
		       *  end
		       */
	        
	        /**
	         * @author Anil
	         * @since 21-01-2022
	         * Need to add validation for selecting on service wise bill or amc bill that mean user is not
	         * allowed to merge to different kind of contract's bill
	         * rised by Nitin and Nithila for Hygeinic Pest
	         */
	        
	        boolean serviceWiseBillFlag=false;
	        boolean amcBillFlag=false;
			for(BillingDocument bill:billingdoc){
				if(bill.getRateContractServiceId()!=0){
					serviceWiseBillFlag=true;
				}else{
					amcBillFlag=true;
				}
			}
			
			if(serviceWiseBillFlag&&amcBillFlag){
				form.showDialogMessage("Please select either amc bill or service wise/rate contract bill!");
				return false;
			}
			
			return true;
	 }
	 
	 /**
	  * Validation for Contract Id.
	  * The selected billing documents should be of same Contract ID.
	  * @param lis
	  * @return
	  */
	 private boolean validateContractId(List<BillingDocument> lis)
	 {
		 int baseContractId=0;
		 if(lis.get(0).getContractCount()!=null)
		 {
		     baseContractId=lis.get(0).getContractCount();
			 for(int i=0;i<lis.size();i++)
			 {
				if(baseContractId!=lis.get(i).getContractCount())
					return false;
			 }
		 }
		 	return true;
	 }
	 
	 /**
	  * Validation for Invoice Date.
	  * The selected billing documents should be of same Invoice Date.
	  * @param lis
	  * @return
	  */
	 private boolean validateInvoiceDate(List<BillingDocument> lis)
	 {
		 String baseInvoiceDate="";
		 if(lis.get(0).getInvoiceDate()!=null)
		 {
			 baseInvoiceDate=AppUtility.parseDate(lis.get(0).getInvoiceDate());
			 System.out.println("Invoice Date"+baseInvoiceDate);
			 for(int i=0;i<lis.size();i++)
			 {
				 System.out.println("App"+AppUtility.parseDate(lis.get(i).getInvoiceDate()));
				if(!baseInvoiceDate.equals(AppUtility.parseDate(lis.get(i).getInvoiceDate())))
					return false;
			 }
		 
		 }	return true;
	 }
	 
	 private boolean validatePaymentDate(List<BillingDocument> lis)
	 {
		 String basePaymentDate="";
		 if(lis.get(0).getPaymentDate()!=null)
		 {
	 		 basePaymentDate=AppUtility.parseDate(lis.get(0).getPaymentDate());
			 for(int i=0;i<lis.size();i++)
			 {
				if(!basePaymentDate.equals(AppUtility.parseDate(lis.get(i).getPaymentDate())))
					return false;
			 }
		 }
		 	return true;
	 }
	 
	 private boolean validateApproverName(List<BillingDocument> lis)
	 {
		 String baseApproverName="";
		 if(lis.get(0).getApproverName()!=null)
		 {
		     baseApproverName=lis.get(0).getApproverName();
			 for(int i=0;i<lis.size();i++)
			 {
				if(!baseApproverName.equals(lis.get(i).getApproverName()))
					return false;
			 }
		 }
		 	return true;
	 }
	 
	 private boolean validateStatus(List<BillingDocument> lis)
	 {
		 /**
		  * @author Anil
		  * @since 22-10-2020
		  * Also allowed created status bill to merge
		  * Raised by Rahul Tiwari for Sun rise
		  */
		 String baseStatus="Approved";
		 for(int i=0;i<lis.size();i++)
		 {
			if(!baseStatus.equals(lis.get(i).getStatus())&&!lis.get(i).getStatus().equals("Created"))
				return false;
		 }
		 	return true;
	 }
	 
	 
	 private double calculateTotalTaxAmt(List<ContractCharges> listContarctCharge,double payPerc)
	 {
		 double totalTax=0;double assess=0;
		 for(int i=0;i<listContarctCharge.size();i++)
		 {
			 assess=listContarctCharge.get(i).getTaxChargeAssesVal()*payPerc/100;
			 totalTax=totalTax+(assess*listContarctCharge.get(i).getTaxChargePercent()/100);
		 }
		 return totalTax;
	 }
	 
	 /**
	  * This method is called when record on billing list is clicked and size of billing charges
	  * of individual billing document is not zero.
	  * Here balance amount is retrieved from master table i.e. taxes and charges
	  */
	 
	 private List<ContractCharges> updateChargesBalance(List<ContractCharges> chargeLis,List<ContractCharges> formChargeLis,String curStatus)
	 {
		 ArrayList<ContractCharges> updatedBalArr=new ArrayList<ContractCharges>();
		 
		 for(int i=0;i<formChargeLis.size();i++)
		 {
			 ContractCharges ccEntity=new ContractCharges();
			 
			 ccEntity.setOrderId(formChargeLis.get(i).getOrderId());
			 ccEntity.setBillingId(formChargeLis.get(i).getBillingId());
			 
			 ccEntity.setTaxChargeName(formChargeLis.get(i).getTaxChargeName());
			 ccEntity.setTaxChargePercent(formChargeLis.get(i).getTaxChargePercent());
			 ccEntity.setTaxChargeAbsVal(formChargeLis.get(i).getTaxChargeAbsVal());
			 ccEntity.setTaxChargeAssesVal(formChargeLis.get(i).getTaxChargeAssesVal());
			 if(!curStatus.trim().equals(BillingDocument.CREATED)){
				 ccEntity.setChargesBalanceAmt(formChargeLis.get(i).getChargesBalanceAmt());
			 }
			 else{
				 ccEntity.setChargesBalanceAmt(chargeLis.get(i).getChargesBalanceAmt());
			 }
			 ccEntity.setPaypercent(formChargeLis.get(i).getPaypercent());
			 
			 updatedBalArr.add(ccEntity);
		 }
		 return updatedBalArr;
	 }
	 
	 
	 private List<SalesOrderProductLineItem> fillBillingProductTableOnClick()
	 {
		 List<BillingDocument> listsel=getAllSelectedRecords();
		 ArrayList<SalesOrderProductLineItem> arrBilingDoc=new ArrayList<SalesOrderProductLineItem>();
		 int productSrNo=0;
		 for(int i=0;i<listsel.size();i++)
		 {
//			 arrBilingDoc.addAll(listsel.get(i).getSalesOrderProducts());
			 for(SalesOrderProductLineItem item:listsel.get(i).getSalesOrderProducts()){
				 productSrNo++;
				 item.setProductSrNumber(productSrNo);
				 arrBilingDoc.add(item);
			 }
		 }
		 return arrBilingDoc;
	 }
	 
	 private List<ContractCharges> fillBillingOtherChargesTableOnClick()
	 {
		
		 List<BillingDocument> listsel=getAllSelectedRecords();
		 ArrayList<ContractCharges> arrBilingDoc=new ArrayList<ContractCharges>();
		 ArrayList<ContractCharges> arrBilingCombineDoc=new ArrayList<ContractCharges>();
		 for(int i=0;i<listsel.size();i++)
		 {
			 if(listsel.get(i).getBillingOtherCharges().size()!=0){
					arrBilingDoc.addAll(listsel.get(i).getBillingOtherCharges());
			 }
		 }
		 System.out.println("OTHER CHARGES LIST ::: "+arrBilingDoc.size());
		 
		 System.out.println();
			for(int k=0;k<arrBilingDoc.size();k++){
				System.out.println("ORDER ID "+arrBilingDoc.get(k).getOrderId()+" VIEW TAX "+arrBilingDoc.get(k).getTaxChargeName());
			}
			System.out.println();
		 
		 return arrBilingDoc;
	 } 
	 
	 public boolean isMultipleContractBilling(){
			System.out.println("INSIDE CHECKING MULTIPLE CONTRACT BILLING......");
			List<BillingDocumentDetails> billtablelis = fillBillingDocumentTableOnClick();
			
			int contractId=billtablelis.get(0).getOrderId();
			for(int i=0;i<billtablelis.size();i++){
				if(billtablelis.get(i).getOrderId()!=contractId){
					return true;
				}
			}
			return false;
		}
	 	
	 private boolean validateCustomer(List<BillingDocument> lis)
	 {
		 System.out.println("Inside Customer Vallidation !!!");
		 int baseCustomerId=0;
		 if(lis.get(0).getPersonInfo().getCount()!=0)
		 {
			 baseCustomerId=lis.get(0).getPersonInfo().getCount();
			 System.out.println("Customer Count :: "+baseCustomerId);
			 for(int i=0;i<lis.size();i++)
			 {
				System.out.println("Loop Customer Count :: "+lis.get(i).getPersonInfo().getCount());
				if(baseCustomerId!=lis.get(i).getPersonInfo().getCount()){
					System.out.println("Inside CUST COUNT Not Matched !!!");
					return false;
				}
			 }
		 }
		 return true;
	 }
	 
	 private boolean validateAccountType(List<BillingDocument> lis)
	 {
		 String baseAccountType="";
		 if(lis.get(0).getAccountType()!=null)
		 {
			 baseAccountType=lis.get(0).getAccountType();
			 for(int i=0;i<lis.size();i++)
			 {
				if(baseAccountType!=lis.get(i).getAccountType())
					return false;
			 }
		 }
		 	return true;
	 }
	 
	 /**
	  * Validation for Branch.
	  * The selected billing documents should be of same Branch.
	  * @param lis
	 * @param branchValidationFlag 
	  * @return
	  */
	 private boolean validateContractBranch(List<BillingDocument> lis, boolean branchValidationFlag)
	 {
//		 int baseContractId=0;
		 String branch = "";
		 if(lis.get(0).getContractCount()!=null)
		 {
			 branch=lis.get(0).getBranch();
			 for(int i=0;i<lis.size();i++)
			 {
				 if(lis.get(i).getBranch().trim()!=""){
					 if(!branch.trim().equalsIgnoreCase(lis.get(i).getBranch().trim())){
						 if(branchValidationFlag)/** This flag added by vijay for Orion Pest change branch validation not required if process config is active**/{
						 		diffrentBranchflag = true;
						 }
						 	if(!branchValidationFlag)
							form.showDialogMessage("All Billing Document Branch Should be same.");
							return false;
						}
				 }else{
					 if(branchValidationFlag)/** This flag added by vijay for Orion Pest change branch validation not required if process config is active**/{
						 diffrentBranchflag = true;
					 }
					 if(!branchValidationFlag)
					 form.showDialogMessage("All Billing Document Branch Should not be blank.");
					 return false;
				 }
				
				
			 }
		 }
		 	return true;
	 }
	 
	 
	 /**
		 * Date 07-07-2018 By Vijay
		 * Des :- Admin can able to cancel multiple billing documents which are not invoiced
		 * Requirements :- NBHC CCPM
		 */
		 
		@Override
		public void onClick(ClickEvent event) {
			super.onClick(event);

			if(event.getSource()==cancellationpopup.getLblOk()){
				if(cancellationpopup.getTaRemark().getValue()!=null && !cancellationpopup.getTaRemark().getValue().equals("")){
					
					final List<BillingDocument> BillingDocumentList = getAllSelectedRecords();
					ArrayList<BillingDocument> billsDoc = new ArrayList<BillingDocument>();
					billsDoc.addAll(BillingDocumentList);
					
					genralAsync.MultipleBillingDocsCancellation(billsDoc, cancellationpopup.getTaRemark().getValue(), new AsyncCallback<Boolean>() {

						@Override
						public void onFailure(Throwable caught) {
							cancellationpopup.hidePopUp();
						}

						@Override
						public void onSuccess(Boolean result) {
							
							List<BillingDocument> list = form.getSuperTable().getDataprovider().getList();
							for(int i=0;i<BillingDocumentList.size();i++){
								for(int j=0;j<list.size();j++){
									if(BillingDocumentList.get(i).getCount()==list.get(j).getCount()){
										list.get(j).setStatus("Cancelled");
									}
								}
							}
							
							form.showDialogMessage("Billing cancelled successfully!");
							form.getSuperTable().getDataprovider().setList(list);
							form.getSuperTable().getTable().redraw();
							cancellationpopup.hidePopUp();
						}
					});
					
				}else{
					form.showDialogMessage("Please enter valid remark to cancel billing documents");
				}
				
			}
			if(event.getSource()==cancellationpopup.getLblCancel()){
				cancellationpopup.hidePopUp();
			}
			if(event.getSource()==productPopup.getBtnCancel()) {
				Console.log("productPopup.getBtnCancel() clicked");
				productDetailsPanel.hide();
			}
		}
		 
		/**
		 * ends here
		 */
		
		

		private void downloadData(boolean addOrderNoAndGrnFlag, List<BillingDocument> listbill, ArrayList<PurchaseOrder> polist, ArrayList<GRN> grnlist) {

			int columnCount = 37;

			if(addOrderNoAndGrnFlag){
				columnCount +=2;	
			}
			
			ArrayList<String> billingList = new ArrayList<String>();
			billingList.add("BILLING ID");
			billingList.add("BILLING DATE");
			billingList.add("BP ID");
			billingList.add("BP NAME");
			/*** 13-12-2019 Deepak Salve added this code for Customer Branch Download***/
			billingList.add("Customer Branch");
			/***End***/
			billingList.add("PERSON CELL");
			billingList.add("ORDER ID");
			
			if(addOrderNoAndGrnFlag){
				billingList.add("ORDER NO");
				billingList.add("GRN NO");
			}
			billingList.add("ORDER START DATE");
			billingList.add("ORDER END DATE");
			/**@Sheetal : 02-03-2022
			 *  Des  : Adding Billing Period Start date and End date, requirement by pest-o-sheild**/
			billingList.add("BILLING PERIOD START DATE");
			billingList.add("BILLING PERIOD END DATE");
			/**end**/
			billingList.add("ACCOUNT TYPE");
			billingList.add("ORDER AMOUNT");
			billingList.add("BILLING AMOUNT");
			billingList.add("BILLING CATEGORY");
			billingList.add("BILLING TYPE");
			billingList.add("BILLING GROUP");
			billingList.add("INVOICE DATE");
			billingList.add("PAYMENT DATE");
			billingList.add("APPROVER NAME");
			billingList.add("BRANCH");
			billingList.add("COMMENT");
			billingList.add("STATUS");
			billingList.add("SEGMENT");
			billingList.add("SERVICE ID");
			billingList.add("QUANTITY");
			billingList.add("UOM");
			/**
			 *  NIdhi
			 *  24-07-2017
			 */
			billingList.add("REFERANCE NO.");
			/**
			 *  end
			 */
			
			/*** Date 11-09-2017 added by vijay for discount amt round off  ***********/
			billingList.add("TOTAL AMOUNT");
			billingList.add("DISCOUNT AMOUNT");
			billingList.add("TOTAL AMOUNT");
			billingList.add("TOTAL AMOUNT");
			billingList.add("TOTAL AMOUNT");
			billingList.add("ROUND OFF");
			billingList.add("TOTAL BILLING AMOUNT");
			/**
			 * Date 14-5-2018
			 * By jayshree add sale person 
			 */
			billingList.add("SALES PERSON");
			
			/**
			 * Date 06-07-2018 By Vijay
			 * Des :- For NBHC
			 */
			billingList.add("INVOICE ID");
			/**
			 * ends here
			 */
			
			

			for(BillingDocument billinglis:listbill)
			{
				
				billingList.add(billinglis.getCount()+"");

				if(billinglis.getBillingDate()!=null){
					billingList.add(AppUtility.parseDate(billinglis.getBillingDate(),"dd-MMM-yyyy"));
				}
				else{
					billingList.add("");
				}
				billingList.add(billinglis.getPersonInfo().getCount()+"");
				billingList.add(billinglis.getPersonInfo().getFullName());

				/***Deepak Salve add this code for Customer Branch Download***/
				System.out.println("Customer Branch "+billinglis.getCustomerBranch());
				if(billinglis.getCustomerBranch()!=null&&!billinglis.getCustomerBranch().equals("")){
					billingList.add(billinglis.getCustomerBranch());
				}
				else{
					billingList.add("");
				}
				/***End***/
				billingList.add(billinglis.getPersonInfo().getCellNumber()+"");
				billingList.add(billinglis.getContractCount()+"");

				/** Date 12-08-2020 by Vijay for sasha Supplier's Ref./Order No and Receipt Note No ***/
				if(addOrderNoAndGrnFlag){
					if(billinglis.getReferenceNumer()!=null && !billinglis.getReferenceNumer().equals("")){
						billingList.add(billinglis.getReferenceNumer());
					}
					else{
						billingList.add(getRefOrderNo(polist, billinglis.getContractCount()));
					}
					if(billinglis.getReferenceNumber2()!=null && !billinglis.getReferenceNumber2().equals("")){
						billingList.add(billinglis.getReferenceNumber2());
					}
					else{
						if(billinglis.getRefNumber()!=null && !billinglis.getRefNumber().equals("")){
							int grnId = Integer.parseInt(billinglis.getRefNumber());
							billingList.add(getGRNReceiptNoteNo(grnlist, grnId));
						}
						else{
							billingList.add("");
						}
					}
				}

				/**
				 * rohan added this for pest terminators 
				 */
				if(billinglis.getContractStartDate()!=null){
					billingList.add(AppUtility.parseDate(billinglis.getContractStartDate(),"dd-MMM-yyyy"));
				}
				else{
					billingList.add("N.A");
				}
				if(billinglis.getContractEndDate()!=null){
					billingList.add(AppUtility.parseDate(billinglis.getContractEndDate(),"dd-MMM-yyyy"));
				}
				else{
					billingList.add("N.A");
				}
				/**
				 * ends here 
				 */
				/**@Sheetal : 02-03-2022
				 *  Des  : Adding Billing Period Start date and End date, requirement by pest-o-sheild**/
				if(billinglis.getBillingPeroidFromDate()!=null){
					billingList.add(AppUtility.parseDate(billinglis.getBillingPeroidFromDate(),"dd-MMM-yyyy"));
				}
				else{
					billingList.add("N.A");
				}
				
				if(billinglis.getBillingPeroidToDate()!=null){
					billingList.add(AppUtility.parseDate(billinglis.getBillingPeroidToDate(),"dd-MMM-yyyy"));
				}
				else{
					billingList.add("N.A");
				}
				/**end**/
				billingList.add(billinglis.getAccountType());
				billingList.add(billinglis.getTotalSalesAmount()+"");
				if(billinglis.getTotalBillingAmount()!=null){
					billingList.add(billinglis.getTotalBillingAmount()+"");
				}else{
					billingList.add("");
				}
				
				if(billinglis.getBillingCategory()!=null){
					billingList.add(billinglis.getBillingCategory());
				}
				else{
					billingList.add("");
				}
				
				if (billinglis.getBillingType() != null) {
					billingList.add(billinglis.getBillingType());
				} else {
					billingList.add("");
				}
				if (billinglis.getBillingGroup() != null) {
					billingList.add(billinglis.getBillingGroup());
				} else {
					billingList.add("");
				}
				if (billinglis.getInvoiceDate() != null) {
					billingList.add(AppUtility.parseDate(billinglis.getInvoiceDate(),"dd-MMM-yyyy"));
				} else {
					billingList.add("");
				}
				if (billinglis.getPaymentDate() != null) {
					billingList.add(AppUtility.parseDate(billinglis.getPaymentDate(),"dd-MMM-yyyy"));
				} else {
					billingList.add("");
				}
				if (billinglis.getApproverName() != null) {
					billingList.add(billinglis.getApproverName());
				} else {
					billingList.add("");
				}
				//  rohan added here branch for NBHC 
				if (billinglis.getBranch() != null) {
					billingList.add(billinglis.getBranch());
				} else {
					billingList.add("");
				}
				//  ends here 	
					
				if(billinglis.getComment()!=null && !billinglis.getComment().equals("")){
					/**
					 * Date 11/10/2018 By Vijay
					 * Des :- Download getting issue due to comment contains comma so i have commneted old code and added below code
					 */
					billingList.add(billinglis.getComment());
					/**
					 * ends here
					 */
				}else{
					billingList.add("");
				}
				billingList.add(billinglis.getStatus());
				
				
			if(billinglis.getSegment()!=null){
				billingList.add(billinglis.getSegment());
			}
			else{
				billingList.add("");
			}
			/**
			 * Date : 17-07-2017 Nidhi
			 */
			if (billinglis.getRateContractServiceId() != 0) {
				billingList.add(billinglis.getRateContractServiceId() + "");
			} else {
				billingList.add("");
			}
			/**
			 * End
			 */
				
			/*
			 *  NIDHI
			 *  20-07-2017
			 *  
			 */
			if (billinglis.getQuantity() != 0) {
				billingList.add(billinglis.getQuantity()+"");
			} else {
				billingList.add("");
			}
			
			if (billinglis.getUom() != null) {
				billingList.add(billinglis.getUom());
			} else {
				billingList.add("");
			}
			/*
			 *  end
			 */
			
			/**
			 *  nidhi
			 *  24-07-2017
			 *  
			 */
			
			if (billinglis.getRefNumber() != null) {
				billingList.add(billinglis.getRefNumber());
			} else {
				billingList.add("");
			}
			/*
			 *  end
			 */
			
			/*** Date 11-09-2017 added by vijay for discount amt round off  ***********/
			billingList.add(billinglis.getTotalAmount()+"");
			billingList.add(billinglis.getDiscountAmt()+"");
			billingList.add(billinglis.getFinalTotalAmt()+"");
			billingList.add(billinglis.getTotalAmtIncludingTax()+"");
			billingList.add(billinglis.getGrandTotalAmount()+"");
			billingList.add(billinglis.getRoundOffAmt()+"");
			billingList.add(billinglis.getTotalBillingAmount()+"");
			/**
			 * Date 14-5-2018
			 * by jayshree
			 * add sales person name
			 */
			billingList.add(billinglis.getEmployee());
			//End by jayshree
			/**
			 * Date 06-07-2018 By Vijay
			 * Des :- invoice Id. Requirement from NBHC 
			 */
			if(billinglis.getInvoiceCount()!=0){
				billingList.add(billinglis.getInvoiceCount()+"");
			}else{
				billingList.add("");
			}
			/**
			 * ends here
			 */
			}
				

			csvservice.setExcelData(billingList, columnCount, AppConstants.BILLINGDETAILS, new AsyncCallback<String>() {
				
				@Override
				public void onSuccess(String result) {
					// TODO Auto-generated method stub
					
					String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url=gwt + "CreateXLXSServlet"+"?type="+14;
					Window.open(url, "test", "enabled");
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					
				}
			});

		}
		
		
		private String getGRNReceiptNoteNo(List<GRN> grnlist, int GRNId) {

			for(GRN grnEntity : grnlist){
				if(grnEntity.getCount()==GRNId){
					if(grnEntity.getRefNo()!=null && !grnEntity.getRefNo().equals("")){
						return grnEntity.getRefNo();
					}
				}
			}
			
			return "";
		}



		private String getRefOrderNo(List<PurchaseOrder> polist,Integer contractCount) {
		
			for(PurchaseOrder purchase : polist){
				if(purchase.getCount()==contractCount){
					if(purchase.getRefOrderNO()!=null){
						return purchase.getRefOrderNO();
					}
				}
			}
			
			return "";
		}
		
		private void showProductDetails(BillingDocument bill) {
			Console.log("product details clicked");	
			if(bill.getAccountType().equals("AR")) {
			 MyQuerry querry=new MyQuerry();
			 Vector<Filter> filtrvec=new Vector<Filter>();
			 Filter filterval=null;
			 
			 filterval=new Filter();
			 filterval.setQuerryString("count");
			 filterval.setIntValue(bill.getContractCount());
			 filtrvec.add(filterval);
			 
			 filterval=new Filter();
			 filterval.setQuerryString("companyId");
			 filterval.setLongValue(bill.getCompanyId());
			 filtrvec.add(filterval);
			 
			 querry.setFilters(filtrvec);
			 querry.setQuerryObject(new Contract());
			
			async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					if(result!=null&&result.size()>0) {
						Contract con=(Contract)result.get(0);
						productDetailsPanel = new PopupPanel(true);
						productPopup.getproductTable().getDataprovider().setList(con.getItems());						
						
						productDetailsPanel.add(productPopup);
						productDetailsPanel.show();
						productDetailsPanel.center();
					
					}else
						form.showDialogMessage("Failed");
					
				}
				
				@Override
				public void onFailure(Throwable caught) {
					form.showDialogMessage("Failed to load details");
					
				}
			});
			
			
			}else
				form.showDialogMessage("Applicable only for Contract bills");
			
		}
		
}
