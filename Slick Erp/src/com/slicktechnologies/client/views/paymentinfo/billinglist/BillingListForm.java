package com.slicktechnologies.client.views.paymentinfo.billinglist;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreen;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;

public class BillingListForm extends TableScreen<BillingDocument> {
	
	
	//Token to add variable declaration
	//
	
	public BillingListForm(SuperTable<BillingDocument> superTable) {
		super(superTable);
		createGui();
	}
	
	@SuppressWarnings("unused")
	private void initalizeWidget()
	{
		//<ReplasbleVariableInitalizerBlock>
	}
	
	@Override
	public void createScreen() 
	{
		initalizeWidget();

		/**
		 * Date 07-07-2018 By Vijay
		 * Des :- Multiple billing documents cancellation
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice","OnlyForNBHC"))
		{
			if(LoginPresenter.myUserEntity.getRole().getRoleName().equals("ADMIN")){
				this.processlevelBarNames=new String[]{"Process Billing Document",AppConstants.CANCELBILLING};
			}else{
				this.processlevelBarNames=new String[]{"Process Billing Document"};
			}
		}else{
			this.processlevelBarNames=new String[]{"Process Billing Document"};
		}
		
		
		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////
		//Token to initialize formfield
	}
	
	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		super.setEnable(state);
	}

	/**
	 * method template to update the model with token entity name
	 */
	@Override
	public void updateModel(BillingDocument model)
	{
		//<ReplasableUpdateModelFromViewCode>
	}

	/**
	 * method template to update the view with token entity name
	 */
	@Override
	public void updateView(BillingDocument view) 
	{
		//<ReplasableUpdateViewFromModelCode>
	}
	
	/**
	 * Toggles the app header bar menus as per screen state
	 */

	public void toggleAppHeaderBarMenu() {
		if(superTable!=null&&superTable.getListDataProvider().getList()!=null){
			Console.log("in toggleAppHeaderBarMenu count="+superTable.getListDataProvider().getList().size());
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Rows:")){
					menus[k].setText("Rows:"+superTable.getListDataProvider().getList().size());	
					menus[k].getElement().setId("addlabel2");
				}
			}
		}
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Download")||text.equals("Search")||text.equals(AppConstants.NAVIGATION)||text.contains("Rows:"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		  		
			}
		}
		
		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION)||text.contains("Rows:"))
					menus[k].setVisible(true); 
				
				else
					menus[k].setVisible(false);  		   
			}
		}
		
		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
		
		
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){//18-10-2022
				
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else
				menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Download")||text.equals("Search")||text.equals(AppConstants.NAVIGATION)||text.contains("Rows:"))
						menus[k].setVisible(true); 
				else
					menus[k].setVisible(false);  		   
			}
			
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.BILLINGLIST,LoginPresenter.currentModule.trim());
		//Ashwini Patil Date:20-09-2022
		if(isHideNavigationButtons()) {
				Console.log("in toggleAppHeaderBarMenu isHideNavigationButtons");
				
				for(int i=0;i<getProcesslevelBarNames().length;i++)
				{
					InlineLabel label=getProcessLevelBar().btnLabels[i];
					String text=label.getText().trim();

					label.setVisible(false);
				}
		}
	}
	
	
	@Override
	public void retriveTable(MyQuerry querry) {
		superTable.connectToLocal();
		service.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				
//				superTable.getTable().setVisibleRange(0,result.size());
				
				/**
				 * @author abhinav Bihade
				 * @since 26/12/2019
				 * As per Rahul tiwari's Requirement for Orkin : After approving contract we click on " View Bill" option to view Bill. 
				 * Billing Id should be by default sorted in Ascending Order
				 */
				
				Comparator<SuperModel> billingDocument =new Comparator<SuperModel>() {

					@Override
					public int compare(SuperModel billDoc1,SuperModel billDoc2) {
						Integer count1= billDoc1.getCount();
						Integer count2= billDoc2.getCount();
//						return count1.compareTo(count2);
						/**
						 * @author Anil @since 30-03-2021
						 * Raised by Rahul Tiwari for sunrise
						 */
						return count2.compareTo(count1);
					
					}
					
				};
				Collections.sort(result,billingDocument);
				for(SuperModel model:result)
				superTable.getListDataProvider().getList().add((BillingDocument) model);
//				superTable.getTable().setVisibleRange(0,result.size());
			}
			
			@Override
			public void onFailure(Throwable caught) {
			caught.printStackTrace();
			}
		});
	}
	

}
