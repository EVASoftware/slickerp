package com.slicktechnologies.client.views.paymentinfo.billinglist;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.ValueUpdater;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.Header;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.quotation.QuotationForm;
import com.slicktechnologies.client.views.quotation.QuotationPresenter;
import com.slicktechnologies.shared.BranchWiseScheduling;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.SalesOrderProductLineItem;

public class ProductDetailsTable extends SuperTable<SalesLineItem> implements ClickHandler{

	TextColumn<SalesLineItem> prodNameColumn;
	TextColumn<SalesLineItem> premisesColumn;
	TextColumn<SalesLineItem> custBranchesColumn;
	Header<Boolean> selectAllHeader;
	public BillingListForm form;
	
	int rowIndex=0;	
	
	boolean showOnlyBranchName=false;
			
	public ProductDetailsTable() {
		
	}
	
	@Override
	public void createTable() {
		addProdNameColumn();
		addPremisesColumn();
		addCustomerBranchColumn();

	}
	
	
	
	private void addProdNameColumn() {
		prodNameColumn=new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				if(object.getProductName()!=null){
					return object.getProductName();
				}
				return "";
			}
		};
		table.addColumn(prodNameColumn,"Product Name");
		table.setColumnWidth(prodNameColumn,250, Unit.PX);
	}
	private void addPremisesColumn() {
		premisesColumn=new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				if(object.getPremisesDetails()!=null){
					return object.getPremisesDetails();
				}
				return "";
			}
		};
		table.addColumn(premisesColumn,"Premises");
		table.setColumnWidth(premisesColumn,250, Unit.PX);
	}
	private void addCustomerBranchColumn() {
		custBranchesColumn=new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				if(object.getCustomerBranchSchedulingInfo()!=null){
					Console.log("getCustomerBranchSchedulingInfo not null for product "+object.getProductName());
					ArrayList<BranchWiseScheduling> branchSchedulingList=null;
					branchSchedulingList = object.getCustomerBranchSchedulingInfo().get(object.getProductSrNo());						
					ArrayList<String> selectedBranches=new ArrayList<String>();
					if(branchSchedulingList!=null) {
						for(BranchWiseScheduling binfo:branchSchedulingList) {
							if(binfo.isCheck()) {
								selectedBranches.add(binfo.getBranchName());
							}								
						}
					}
					if(selectedBranches.size()>0)
						return selectedBranches.toString();
					return "";
				}
				return "";
			}
		};
		table.addColumn(custBranchesColumn,"Customer Branches");
		table.setColumnWidth(custBranchesColumn,250, Unit.PX);
	}
	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void addColumnSorting() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onClick(ClickEvent event) {
		
	}

}

