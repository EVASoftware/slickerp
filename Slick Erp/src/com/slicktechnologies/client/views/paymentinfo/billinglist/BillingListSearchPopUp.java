package com.slicktechnologies.client.views.paymentinfo.billinglist;

import java.util.Vector;

import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

public class BillingListSearchPopUp extends SearchPopUpScreen<BillingDocument> implements SelectionHandler<Suggestion> {
	

	public IntegerBox ibbillingid;
	public IntegerBox ibcontractid;
	public PersonInfoComposite personInfo;
	public PersonInfoComposite vendorInfo;
	public DateComparator dateComparator;
	public ObjectListBox<ConfigCategory> olbbillingcategory;
	public ObjectListBox<Type> olbbillingtype;
	public ObjectListBox<Config> olbbillinggroup;
	public ListBox lbstatus;
	public ObjectListBox<Employee> olbApproverName;
	public ListBox lbaccountType;
	public ObjectListBox<Branch> olbBranch;
	
	public IntegerBox ibrateContractServiceId;
	/**03-10-2017 sagar sore [ To add field for number range in search popup]***/
	public ObjectListBox<Config> olbcNumberRange;
	/** date 20.10.2018 added by komal**/
	ObjectListBox<String> olbTypeOforder;
	ObjectListBox<String> olbSubBillType;
	public BillingListSearchPopUp() {
		super();
		createGui();
	}
	
	public BillingListSearchPopUp(boolean b) {
		super(b);
		createGui();
	}
	
	public void initWidget()
	{
		ibbillingid= new IntegerBox();
		ibcontractid=new IntegerBox();
		
		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new Customer());
		personInfo=new PersonInfoComposite(querry,false);
		
		MyQuerry vendorquerry=new MyQuerry();
		vendorquerry.setQuerryObject(new Vendor());
		vendorInfo=new PersonInfoComposite(vendorquerry,false);
		
		
		dateComparator=new DateComparator("billingDocumentInfo.billingDate",new BillingDocument());
		olbbillingcategory=new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(olbbillingcategory, Screen.BILLINGCATEGORY);
		olbbillingtype=new ObjectListBox<Type>();
		AppUtility.makeTypeListBoxLive(olbbillingtype, Screen.BILLINGTYPE);
		olbbillinggroup=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbbillinggroup, Screen.BILLINGGROUP);
		lbstatus= new ListBox();
		AppUtility.setStatusListBox(lbstatus,BillingDocument.getStatusList());
		olbApproverName=new ObjectListBox<Employee>();
		AppUtility.makeSalesPersonListBoxLive(olbApproverName);
		
		personInfo.getId().addSelectionHandler(this);
		vendorInfo.getId().addSelectionHandler(this);
		
		lbaccountType=new ListBox();
		lbaccountType.addItem("SELECT");
		lbaccountType.addItem(AppConstants.BILLINGACCOUNTTYPEAP);
		lbaccountType.addItem(AppConstants.BILLINGACCOUNTTYPEAR);
		
		personInfo.getCustomerId().getHeaderLabel().setText("Customer ID");
		personInfo.getCustomerName().getHeaderLabel().setText("Customer Name");
		personInfo.getCustomerCell().getHeaderLabel().setText("Customer Cell");
		
		vendorInfo.getCustomerId().getHeaderLabel().setText("Vendor ID");
		vendorInfo.getCustomerName().getHeaderLabel().setText("Vendor Name");
		vendorInfo.getCustomerCell().getHeaderLabel().setText("Vendor Cell");
		
		//**************rohan made changes here ***************
		
		olbBranch= new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbBranch);
		
		ibrateContractServiceId = new IntegerBox();
		
		/**03-10-2017 sagar sore[ Number range initialization, Displaying values added from process configuration]***/
		olbcNumberRange= new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbcNumberRange,Screen.NUMBERRANGE);
		
		/** date 20.10.2018 added by komal**/
		olbTypeOforder = new ObjectListBox<String>();
		olbTypeOforder.addItem("--SELECT--");
		olbTypeOforder.addItem(AppConstants.ORDERTYPESALES);
		olbTypeOforder.addItem(AppConstants.ORDERTYPESERVICE);
		olbTypeOforder.addItem(AppConstants.ORDERTYPEPURCHASE);
		
		olbSubBillType = new ObjectListBox<String>();
		AppUtility.setStatusListBox(olbSubBillType, Invoice.getSubBillTypeList());
//		olbSubBillType.addItem("--SELECT--");
//		olbSubBillType.addItem(AppConstants.STAFFINGANDRENTAL);
//		olbSubBillType.addItem(AppConstants.CONSUMABLES);
//		olbSubBillType.addItem(AppConstants.NATIONALHOLIDAY);
//		olbSubBillType.addItem(AppConstants.OVERTIME);
//		olbSubBillType.addItem(AppConstants.OTHERSERVICES);
	}
	
	public void createScreen()
	{
		initWidget();
		FormFieldBuilder builder;
		builder = new FormFieldBuilder("Billing ID",ibbillingid);
		FormField fibbillingid= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("",personInfo);
		FormField fPersonInfo= builder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		builder = new FormFieldBuilder("",vendorInfo);
		FormField fvendorInfo= builder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		builder = new FormFieldBuilder("Order ID",ibcontractid);
		FormField fibcontractid= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("From Date (Billing Date)",dateComparator.getFromDate());
		FormField fdateComparatorfrom= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("To Date (Billing Date)",dateComparator.getToDate());
		FormField fdateComparatorto= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Billing Category",olbbillingcategory);
		FormField folbbillingcategory= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Billing Type",olbbillingtype);
		FormField folbbillingtype= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Billing Group",olbbillinggroup);
		FormField folbbillinggroup= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Billing Status",lbstatus);
		FormField flbstatus=builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Approver Name",olbApproverName);
		FormField folbApproverName= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Account Type",lbaccountType);
		FormField flbaccountType= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Branch",olbBranch);
		FormField folbBranch= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Service id",ibrateContractServiceId);
		FormField fibrateContractServiceId= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/**03-10-2017 sagar sore[ adding search by number range]**/
		FormField folbcNumberRange;	
		builder = new FormFieldBuilder("Number Range",olbcNumberRange);
		folbcNumberRange= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/** date 20.10.2018 added by komal**/
		builder = new FormFieldBuilder("Type Of Order",olbTypeOforder);
		FormField folbTypeOforder= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Sub Bill Type",olbSubBillType);
		FormField folbSubBillType= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		this.fields=new FormField[][]{
				/**To add number range field and order of fields altered**/
				/**old code**/
//				{fibbillingid,fdateComparatorfrom,fdateComparatorto,flbstatus},
//				{folbApproverName,folbbillingcategory,folbbillingtype,folbbillinggroup},
//				{fibcontractid,flbaccountType,folbBranch,fibrateContractServiceId},
//				{fPersonInfo},
//				{fvendorInfo}
				/**Updated order**/
				{fibbillingid,fdateComparatorfrom,fdateComparatorto,flbstatus},
				{folbcNumberRange,folbbillingcategory,folbbillingtype,folbbillinggroup},
				{fibcontractid,flbaccountType,folbBranch,fibrateContractServiceId},
				{fPersonInfo},
				{fvendorInfo},
				{folbApproverName,folbTypeOforder , folbSubBillType},/** date 20.10.2018 added by komal**/
		};
	}
	

	public MyQuerry getQuerry()
	{
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		
		if(dateComparator.getValue()!=null)
			filtervec.addAll(dateComparator.getValue());
		
		if(olbbillingcategory.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbbillingcategory.getValue().trim());
			temp.setQuerryString("billingDocumentInfo.billingCategory");
			filtervec.add(temp);
		}
		
		if(olbbillingtype.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbbillingtype.getValue().trim());
			temp.setQuerryString("billingDocumentInfo.billingType");
			filtervec.add(temp);
		}
		
		if(olbbillinggroup.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbbillinggroup.getValue().trim());
			temp.setQuerryString("billingDocumentInfo.billingGroup");
			filtervec.add(temp);
		}

		if(ibbillingid.getValue()!=null){
			temp=new Filter();
			temp.setIntValue(ibbillingid.getValue());
			filtervec.add(temp);
			temp.setQuerryString("count");
			filtervec.add(temp);
		}
		
		if(ibcontractid.getValue()!=null){
			temp=new Filter();
			temp.setIntValue(ibcontractid.getValue());
			filtervec.add(temp);
			temp.setQuerryString("contractCount");
			filtervec.add(temp);
		}
		
		if(lbaccountType.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(lbaccountType.getItemText(lbaccountType.getSelectedIndex()).trim());
			temp.setQuerryString("accountType");
			filtervec.add(temp);
		}
		
		if(personInfo.getIdValue()!=-1)
		  {
		  temp=new Filter();
		  temp.setIntValue(personInfo.getIdValue());
		  temp.setQuerryString("personInfo.count");
		  filtervec.add(temp);
		  }
		  
		  if(!(personInfo.getFullNameValue().equals("")))
		  {
		  temp=new Filter();
		  temp.setStringValue(personInfo.getFullNameValue());
		  temp.setQuerryString("personInfo.fullName");
		  filtervec.add(temp);
		  }
		  if(personInfo.getCellValue()!=-1l)
		  {
		  temp=new Filter();
		  temp.setLongValue(personInfo.getCellValue());
		  temp.setQuerryString("personInfo.cellNumber");
		  filtervec.add(temp);
		  }
		  
		  
		  
		  if(vendorInfo.getIdValue()!=-1)
		  {
		  temp=new Filter();
		  temp.setIntValue(vendorInfo.getIdValue());
		  temp.setQuerryString("personInfo.count");
		  filtervec.add(temp);
		  }
		  
		  if(!(vendorInfo.getFullNameValue().equals("")))
		  {
		  temp=new Filter();
		  temp.setStringValue(vendorInfo.getFullNameValue());
		  temp.setQuerryString("personInfo.fullName");
		  filtervec.add(temp);
		  }
		  if(vendorInfo.getCellValue()!=-1l)
		  {
		  temp=new Filter();
		  temp.setLongValue(vendorInfo.getCellValue());
		  temp.setQuerryString("personInfo.cellNumber");
		  filtervec.add(temp);
		  }
		  
		  if(lbstatus.getSelectedIndex()!=0){
				temp=new Filter();
				temp.setStringValue(lbstatus.getItemText(lbstatus.getSelectedIndex()));
				filtervec.add(temp);
				temp.setQuerryString("status");
				filtervec.add(temp);
			}
		  if(olbApproverName.getSelectedIndex()!=0){
				temp=new Filter();temp.setStringValue(olbApproverName.getValue().trim());
				temp.setQuerryString("approverName");
				filtervec.add(temp);
			}
		  
			if (LoginPresenter.branchRestrictionFlag == true
					&& olbBranch.getSelectedIndex() == 0) {
				temp = new Filter();
				temp.setList(AppUtility.getbranchlist());
				temp.setQuerryString("branch IN");
				filtervec.add(temp);
			} else {
				if (olbBranch.getSelectedIndex() != 0) {
					temp = new Filter();
					temp.setStringValue(olbBranch.getValue().trim());
					temp.setQuerryString("branch");
					filtervec.add(temp);
				}
			}
			
			
			if(ibrateContractServiceId.getValue()!=null){
				temp=new Filter();
				temp.setIntValue(ibrateContractServiceId.getValue());
				temp.setQuerryString("rateContractServiceId");
				filtervec.add(temp);
			}
			 /**03-10-2017 sagar sore[ filter for search by number range i.e. Billing and Non-billing]**/
			if(olbcNumberRange.getSelectedIndex()!=0){
                temp=new Filter();
				temp.setStringValue(olbcNumberRange.getValue().trim());
				temp.setQuerryString("numberRange");
				filtervec.add(temp);
			}
			/** date 20.10.2018 added by komal**/
			if(olbTypeOforder.getSelectedIndex() != 0){
				temp=new Filter();
				temp.setStringValue(olbTypeOforder.getValue(olbTypeOforder.getSelectedIndex()));
				temp.setQuerryString("typeOfOrder");
				filtervec.add(temp);
			}
			
			if(olbSubBillType.getSelectedIndex() != 0){
				temp=new Filter();
				temp.setStringValue(olbSubBillType.getValue(olbSubBillType.getSelectedIndex()));
				temp.setQuerryString("subBillType");
				filtervec.add(temp);
			}
		MyQuerry querry= new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new BillingDocument());
		return querry;
	}

	@Override
	public void onSelection(SelectionEvent<Suggestion> event) 
	{
		if(event.getSource().equals(personInfo.getId())||event.getSource().equals(personInfo.getName())||event.getSource().equals(personInfo.getPhone()))
		{
			if(!vendorInfo.getId().getValue().equals("")||!vendorInfo.getName().getValue().equals("")||!vendorInfo.getPhone().getValue().equals(""))
			{
				vendorInfo.clear();
			}
		}
		
		if(event.getSource().equals(vendorInfo.getId())||event.getSource().equals(vendorInfo.getName())||event.getSource().equals(vendorInfo.getPhone()))
		{
			if(!personInfo.getId().getValue().equals("")&&!personInfo.getName().getValue().equals("")&&!personInfo.getPhone().getValue().equals(""))
			{
				personInfo.clear();
			}
		}
	}

	@Override
	public boolean validate() {
//		if(LoginPresenter.branchRestrictionFlag)
//		{
//		if(olbBranch.getSelectedIndex()==0)
//		{
//			showDialogMessage("Select Branch");
//			return false;
//		}
//		}
		return true;
	}
	
	
}
