package com.slicktechnologies.client.views.paymentinfo.billinglist;


import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.dom.client.Style.FontStyle;
import com.google.gwt.dom.client.Style.FontWeight;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.ListBox;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.BranchWiseScheduling;

public class ProductDetailsPopup extends AbsolutePanel implements ChangeHandler{


	ProductDetailsTable productTable;
	Button btnOk,btnCancel;
	
	private void initializeWidget(){
			
		productTable=new ProductDetailsTable();
		productTable.getTable().setHeight("200px");
		btnOk=new Button("Ok");
		btnOk.getElement().getStyle().setWidth(60, Unit.PX);
		btnCancel=new Button("Close");
		}
	
	public ProductDetailsPopup() {
		initializeWidget();
		
		InlineLabel label=new InlineLabel("");
//		label.getElement().getStyle().setFontSize(20, Unit.PX);
//		label.getElement().getStyle().setFontStyle(FontStyle.NORMAL);
		label.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		
		add(label,35,15);
		FlowPanel flowPanel=new FlowPanel();
		flowPanel.add(productTable.getTable());
		
		
			flowPanel.setWidth("780px");
//			add(btnOk,125,250);
			add(btnCancel,400,350);
		
		flowPanel.setHeight("180px");
//		flowPanel.getElement().getStyle().setBorderWidth(1, Unit.PX);
		
		add(flowPanel,10,40);		

		setSize("800px","400px");
		
		this.getElement().setId("login-form");
	}
	
	public void clear(){
		productTable.connectToLocal();
	}

	
	public ProductDetailsTable getproductTable() {
		return productTable;
	}

	public void setproductTable(ProductDetailsTable productTable) {
		this.productTable = productTable;
	}

	@Override
	public void onChange(ChangeEvent event) {
		System.out.println("ON CHANGE ADV SCH POPUP");
		
				
	}

	/*
	 * Nidhi 20-06-2017
	 */
	public Button getBtnOk() {
		return btnOk;
	}

	public void setBtnOk(Button btnOk) {
		this.btnOk = btnOk;
	}

	public Button getBtnCancel() {
		return btnCancel;
	}

	public void setBtnCancel(Button btnCancel) {
		this.btnCancel = btnCancel;
	}
	
	
}

