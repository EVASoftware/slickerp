package com.slicktechnologies.client.views.paymentinfo.paymentdetails;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.composite.UploadComposite;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;

public class TDSUpdatePopUp extends PopupScreen{

	IntegerBox ibTdsID;
	DateBox dbTDSDate;
	CheckBox cbIsTDSCertificate;
	UploadComposite ucUploadTDSCs;
	
	
	public TDSUpdatePopUp(){
		super();
		createGui();
	}
	
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
	}

	private void initializeWidgets() {
		// TODO Auto-generated method stub
		ibTdsID = new IntegerBox();
		dbTDSDate = new DateBoxWithYearSelector();
		dbTDSDate.getElement().getStyle().setWidth(200, Unit.PX);
		cbIsTDSCertificate = new CheckBox();
//		cbIsTDSCertificate.addClickHandler(this);
		
		ucUploadTDSCs=new UploadComposite();
	}
	
	@Override
	public void createScreen() {
		// TODO Auto-generated method stub
		initializeWidgets();
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingTDSInformation=fbuilder.setlabel("TDS Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(2).build();
		fbuilder = new FormFieldBuilder("TDS Id",ibTdsID);
		FormField fibTdsID= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("TDS Date",dbTDSDate);
		FormField fdbTDSDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Is TDS Certificate Recieved",cbIsTDSCertificate);
		FormField fcbIsTDSCertificate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Upload TDS Certificate",ucUploadTDSCs);
		FormField fucUploadTAndCs= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		FormField[][] formfield = {   {fgroupingTDSInformation},
				{fibTdsID,fdbTDSDate},
				{fcbIsTDSCertificate,fucUploadTAndCs},
		};
		this.fields=formfield;
			
	}

	

	
	/************************ getter and setter *****************************/
	
	
	public IntegerBox getIbTdsID() {
		return ibTdsID;
	}

	public void setIbTdsID(IntegerBox ibTdsID) {
		this.ibTdsID = ibTdsID;
	}

	public DateBox getDbTDSDate() {
		return dbTDSDate;
	}

	public void setDbTDSDate(DateBox dbTDSDate) {
		this.dbTDSDate = dbTDSDate;
	}

	public CheckBox getCbIsTDSCertificate() {
		return cbIsTDSCertificate;
	}

	public void setCbIsTDSCertificate(CheckBox cbIsTDSCertificate) {
		this.cbIsTDSCertificate = cbIsTDSCertificate;
	}

	public UploadComposite getUcUploadTDSCs() {
		return ucUploadTDSCs;
	}

	public void setUcUploadTDSCs(UploadComposite ucUploadTDSCs) {
		this.ucUploadTDSCs = ucUploadTDSCs;
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		ibTdsID.setEnabled(state);
		dbTDSDate.setEnabled(state);
		cbIsTDSCertificate.setEnabled(state);
		ucUploadTDSCs.setEnable(state);
	}
	
	
	

}
