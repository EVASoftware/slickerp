package com.slicktechnologies.client.views.paymentinfo.paymentdetails;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

public class PaymentDetailsTableProxy extends SuperTable<CustomerPayment> {
	
	TextColumn<CustomerPayment> getCountColumn;
	TextColumn<CustomerPayment> invoiceIdColumn;
	TextColumn<CustomerPayment> invoiceTypeColumn;
	TextColumn<CustomerPayment> customerIdColumn;
	TextColumn<CustomerPayment> customerNameColumn;
	TextColumn<CustomerPayment> customerCellColumn;
	TextColumn<CustomerPayment> contractCountColumn;
	TextColumn<CustomerPayment> invoiceAmtColumn;
	TextColumn<CustomerPayment> paymentReceivedAmtColumn;
	/**
	 * Date 23-4-2018
	 * By jayshree
	 * Des.add the sales person name column
	 */
	TextColumn<CustomerPayment> salespersoncolumn;
	TextColumn<CustomerPayment> accountTypeColumn;
	TextColumn<CustomerPayment> statusColumn;
	TextColumn<CustomerPayment> branchColumn;
	// rohan added this code for showing balance payment in table
	TextColumn<CustomerPayment> paymentBalanceAmtColumn;
	
	/*
	 *  nidhi
	 *   1-07-2017
	 */
	TextColumn<CustomerPayment> segmentColumn;
	/*
	 *  end
	 */
	
	/*
	 *  nidhi
	 *   18-07-2017 
	 *   display service Id 
	 */
	
	
	TextColumn<CustomerPayment> serviceIdColumn;
	/*
	 *  end
	 */

	/** 03-10-2017 sagar sore [adding Number range column in Invoice list ]**/
	TextColumn<CustomerPayment> numberRangeColumn;
	
	/**17-10-2017 sagar sore**/
	TextColumn<CustomerPayment> paymentDateColumn;
	
	/**
	 * Date : 18-11-2017 BY ANIL
	 */
	TextColumn<CustomerPayment> getColRefNum;
	/** date 1.11.2018 added by komal for type of order and sub bill type **/
	TextColumn<CustomerPayment> getColTypeOfOrder;
	TextColumn<CustomerPayment> getColSubBillType;

	public PaymentDetailsTableProxy() {
		super();
	}
	
	@Override
	public void createTable() {
		
		/**03-10-2017 sagar sore [number range column added and order of columns updated (addColumnCustomerId() moved to right in table)]**/
		/**old code**/
//		addColumnCount();
//		addColumnCustomerPaymentCount();
//		addColumnInvoiceType();
//		addColumnCustomerId();
//		addColumnCustomerName();
//		addColumnCustomerPhone();
//		addColumnAccountType();
//		addColumnContractCount();
//		addColumnInvoiceAmount();
//		addColumnPaymentReceived();
//		addColumnBalanceAmount();
//		addColumnStatus();
//		addColumnBranch();
//		/*
//		 *  nidhi
//		 */
//		addColumnServiceId();
//		/*
//		 *  end
//		 */
//		/*
//		 *  nidhi
//		 */
//		addColumnSegment();
//		/*
//		 *  end
//		 */
		
		/**updated order**/
		addColumnCount();
		addColumnInvoiceCount();
		addColumnInvoiceType();	
		/**17-10-2017 sagar sore [To add payment date column]**/
		addColumnPaymentDate();
		addColumnCustomerName();
		addColumnCustomerPhone();
		addColumnAccountType();
	    addColumnNumberRange();
		addColumnContractCount();
		addColumnInvoiceAmount();
		/**
		 * Date 24-4-2018
		 * By jayshree
		 * Add sales person name
		 */
		addColumnSalesPersonName();
		addColumnPaymentReceived();
		addColumnBalanceAmount();
		addColumnStatus();
		addColumnBranch();
		addColumnCustomerId();
		/*
		 *  nidhi
		 */
		addColumnServiceId();
		/*
		 *  end
		 */
		/*
		 *  nidhi
		 */
		addColumnSegment();
		/*
		 *  end
		 */	
		getColRefNum();
		getColTypeOfOrder();
		getColSubBillType();
	}
	
	private void addColumnSalesPersonName() {

		salespersoncolumn=new TextColumn<CustomerPayment>() {

			@Override
			public String getValue(CustomerPayment object) {
				return object.getEmployee()+"";
			}
		};
		
		table.addColumn(salespersoncolumn,"Sales Person Name");
		table.setColumnWidth(salespersoncolumn, 90, Unit.PX);
		salespersoncolumn.setSortable(true);
	
		
	}

	private void getColRefNum() {
		getColRefNum = new TextColumn<CustomerPayment>() {
			
			@Override
			public String getValue(CustomerPayment object) {
				if(object.getRefNumber()!= null)
					return object.getRefNumber() +"";
				else 
					return "";
			}
		};
		table.addColumn(getColRefNum,"Reference Number");
		table.setColumnWidth(getColRefNum, 100, Unit.PX);
		getColRefNum.setSortable(true);
	}
	
	private void addSortingOnRefNum() {
		List<CustomerPayment> list = getDataprovider().getList();
		columnSort = new ListHandler<CustomerPayment>(list);
		columnSort.setComparator(getColRefNum,new Comparator<CustomerPayment>() {
			@Override
			public int compare(CustomerPayment e1, CustomerPayment e2) {
				if (e1 != null && e2 != null) {
					if (e1.getRefNumber() != null&& e2.getRefNumber() != null) {
						return e1.getRefNumber().compareTo(e2.getRefNumber());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	
	
	public void addColumnServiceId()
	{
		serviceIdColumn =new TextColumn<CustomerPayment>() {

			@Override
			public String getValue(CustomerPayment object) {
				return object.getRateContractServiceId()+"";
			}
		};
		
		table.addColumn(serviceIdColumn,"Service Id");
		table.setColumnWidth(serviceIdColumn, 100, Unit.PX);
		serviceIdColumn.setSortable(true);
	}	
	
	public void addColumnSegment()
	{
		segmentColumn=new TextColumn<CustomerPayment>() {

			@Override
			public String getValue(CustomerPayment object) {
				return object.getSegment();
			}
		};
		
		table.addColumn(segmentColumn,"Segment");
		table.setColumnWidth(segmentColumn, 100, Unit.PX);
		segmentColumn.setSortable(true);
	}	
	
	
	public void addColumnCount()
	{
		getCountColumn=new TextColumn<CustomerPayment>() {

			@Override
			public String getValue(CustomerPayment object) {
				return object.getCount()+"";
			}
		};
		table.addColumn(getCountColumn,"Payment ID");
		table.setColumnWidth(getCountColumn, 100, Unit.PX);
		getCountColumn.setSortable(true);
	}

	
	public void addColumnInvoiceCount()
	{
		
		invoiceIdColumn=new TextColumn<CustomerPayment>() {

			@Override
			public String getValue(CustomerPayment object) {
				return object.getInvoiceCount()+"";
			}
		};
		table.addColumn(invoiceIdColumn,"Invoice ID");
		table.setColumnWidth(invoiceIdColumn, 100, Unit.PX);
		invoiceIdColumn.setSortable(true);
	}
	
	public void addColumnInvoiceType()
	{
		
		invoiceTypeColumn=new TextColumn<CustomerPayment>() {

			@Override
			public String getValue(CustomerPayment object) {
				return object.getInvoiceType();
			}
		};
		table.addColumn(invoiceTypeColumn,"Inv. Doc Type");
		table.setColumnWidth(invoiceTypeColumn, 120, Unit.PX);
		invoiceTypeColumn.setSortable(true);
	}

	private void addColumnCustomerId() {
		customerIdColumn=new TextColumn<CustomerPayment>() {

			@Override
			public String getValue(CustomerPayment object) {
				return object.getCustomerCount()+"";
			}
		};
		
		table.addColumn(customerIdColumn,"BP ID");
		table.setColumnWidth(customerIdColumn, 100, Unit.PX);
		customerIdColumn.setSortable(true);
		
	}
	
	public void addColumnCustomerName()
	{
		customerNameColumn=new TextColumn<CustomerPayment>() {

			@Override
			public String getValue(CustomerPayment object) {
				return object.getName();
			}
		};
		
		table.addColumn(customerNameColumn,"BP Name");
		table.setColumnWidth(customerNameColumn, 120, Unit.PX);
		customerNameColumn.setSortable(true);
	}
	
	public void addColumnCustomerPhone()
	{
		customerCellColumn=new TextColumn<CustomerPayment>() {

			@Override
			public String getValue(CustomerPayment object) {
				return object.getCellNumber()+"";
			}
		};
		
		table.addColumn(customerCellColumn,"Cell Number");
		table.setColumnWidth(customerCellColumn, 120, Unit.PX);
		customerCellColumn.setSortable(true);
	}
	
	public void addColumnAccountType()
	{
		accountTypeColumn=new TextColumn<CustomerPayment>() {

			@Override
			public String getValue(CustomerPayment object) {
				return object.getAccountType();
			}
		};
		
		table.addColumn(accountTypeColumn,"Acc. Type");
		table.setColumnWidth(accountTypeColumn, 90, Unit.PX);
		accountTypeColumn.setSortable(true);
	}
	
	public void addColumnContractCount()
	{
		contractCountColumn=new TextColumn<CustomerPayment>() {

			@Override
			public String getValue(CustomerPayment object) {
				    return object.getContractCount()+"";
			}
		};
		
		table.addColumn(contractCountColumn,"Order ID");
		table.setColumnWidth(contractCountColumn, 100, Unit.PX);
		contractCountColumn.setSortable(true);
	}
	
	public void addColumnInvoiceAmount()
	{
		invoiceAmtColumn=new TextColumn<CustomerPayment>() {

			@Override
			public String getValue(CustomerPayment object) {
				return object.getInvoiceAmount()+"";
			}
		};
		
		table.addColumn(invoiceAmtColumn,"Invoice Amt");
		table.setColumnWidth(invoiceAmtColumn, 100, Unit.PX);
		invoiceAmtColumn.setSortable(true);
	}
	
	
	public void addColumnBalanceAmount()
	{
		paymentBalanceAmtColumn=new TextColumn<CustomerPayment>() {

			@Override
			public String getValue(CustomerPayment object) {
				/**
				 * Date:24-10-2017 BY sagar 
				 *  
				 */		
				return object.getBalanceAmount()+"";
				/**
				 * End
				 */
			}
		};
		
		table.addColumn(paymentBalanceAmtColumn,"Balance Amt");
		table.setColumnWidth(paymentBalanceAmtColumn, 90, Unit.PX);
		paymentBalanceAmtColumn.setSortable(true);
	}	
	
	
	
	public void addColumnPaymentReceived()
	{
		paymentReceivedAmtColumn=new TextColumn<CustomerPayment>() {

			@Override
			public String getValue(CustomerPayment object) {
				return object.getPaymentReceived()+"";
			}
		};
		
		table.addColumn(paymentReceivedAmtColumn,"Received Amt");
		table.setColumnWidth(paymentReceivedAmtColumn, 90, Unit.PX);
		paymentReceivedAmtColumn.setSortable(true);
	}		


	public void addColumnStatus()
	{
		statusColumn=new TextColumn<CustomerPayment>() {

			@Override
			public String getValue(CustomerPayment object) {
				return object.getStatus();
			}
		};
		
		table.addColumn(statusColumn,"Status");
		table.setColumnWidth(statusColumn, 90, Unit.PX);
		statusColumn.setSortable(true);
	}		
	
	public void addColumnBranch()
	{
		branchColumn=new TextColumn<CustomerPayment>() {

			@Override
			public String getValue(CustomerPayment object) {
				return object.getBranch();
			}
		};
		
		table.addColumn(branchColumn,"Branch");
		table.setColumnWidth(branchColumn, 90, Unit.PX);
		branchColumn.setSortable(true);
	}		
	
	
	public void addColumnSorting(){
		addSortinggetCount();
		addSortinggetInvoiceCount();
		addSortinggetInvoiceType();
		addSortinggetCustomerId();
		addSortinggetCustomerFullName();
		addSortinggetCustomerCellNumber();
		addSortingAccountType();
		addSortinggetContractCount();
		addSortinggetInvoiceAmount();
		/**
		 * Date 24-4-2018
		 * By jayshree
		 * Des.add sale person name
		 */
		addSortinggetSalePersonName();
		addSortinggetPaymentReceived();
		addSortinggetStatus();
		addSortinggetBranch();
		
		addColumnBalanceAmountSorting();
		
		/*
		 *  nidhi
		 */
		addSortinggetSegment();
		/*
		 * end
		 */
		addSortinggetServiceId();
		/**03-10-2017 sagar sore [adding number range column]**/
		addSortinggetNumberRange();
		/**17-10-2017 sagar sore [To add payment date column sorting]**/
		addSortinggetPaymentDate();
		addSortingOnRefNum();
		addSortingTypeOfOrder();
		addSortingSubBillType();
	}
	
	private void addSortinggetSalePersonName() {

		List<CustomerPayment> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerPayment>(list);
		columnSort.setComparator(salespersoncolumn, new Comparator<CustomerPayment>()
				{
			@Override
			public int compare(CustomerPayment e1,CustomerPayment e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getEmployee()!=null && e2.getEmployee()!=null){
						return e1.getEmployee().compareTo(e2.getEmployee());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	
	}

	protected void addSortinggetServiceId()
	{
		List<CustomerPayment> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerPayment>(list);
		columnSort.setComparator(serviceIdColumn, new Comparator<CustomerPayment>()
				{
			@Override
			public int compare(CustomerPayment e1,CustomerPayment e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetSegment()
	{
		List<CustomerPayment> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerPayment>(list);
		columnSort.setComparator(segmentColumn, new Comparator<CustomerPayment>()
				{
			@Override
			public int compare(CustomerPayment e1,CustomerPayment e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getSegment()!=null && e2.getSegment()!=null){
						return e1.getSegment().compareTo(e2.getSegment());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}

	
	protected void addColumnBalanceAmountSorting()
	{
		List<CustomerPayment> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerPayment>(list);
		columnSort.setComparator(paymentBalanceAmtColumn, new Comparator<CustomerPayment>()
				{
			@Override
			public int compare(CustomerPayment e1,CustomerPayment e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getBalanceAmount()== e2.getBalanceAmount()){
						return 0;}
					if(e1.getBalanceAmount()> e2.getBalanceAmount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	
	
	protected void addSortinggetCount()
	{
		List<CustomerPayment> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerPayment>(list);
		columnSort.setComparator(getCountColumn, new Comparator<CustomerPayment>()
				{
			@Override
			public int compare(CustomerPayment e1,CustomerPayment e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetInvoiceCount()
	{
		List<CustomerPayment> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerPayment>(list);
		columnSort.setComparator(invoiceIdColumn, new Comparator<CustomerPayment>()
				{
			@Override
			public int compare(CustomerPayment e1,CustomerPayment e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getInvoiceCount()== e2.getInvoiceCount()){
						return 0;}
					if(e1.getInvoiceCount()> e2.getInvoiceCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetInvoiceType()
	{
		List<CustomerPayment> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerPayment>(list);
		columnSort.setComparator(invoiceTypeColumn, new Comparator<CustomerPayment>()
				{
			@Override
			public int compare(CustomerPayment e1,CustomerPayment e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getInvoiceType()!=null && e2.getInvoiceType()!=null){
						return e1.getInvoiceType().compareTo(e2.getInvoiceType());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetCustomerId()
	{
		List<CustomerPayment> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerPayment>(list);
		columnSort.setComparator(customerIdColumn, new Comparator<CustomerPayment>()
				{
			@Override
			public int compare(CustomerPayment e1,CustomerPayment e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getPersonInfo().getCount()== e2.getPersonInfo().getCount()){
						return 0;}
					if(e1.getPersonInfo().getCount()> e2.getPersonInfo().getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetCustomerFullName()
	{
		List<CustomerPayment> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerPayment>(list);
		columnSort.setComparator(customerNameColumn, new Comparator<CustomerPayment>()
				{
			@Override
			public int compare(CustomerPayment e1,CustomerPayment e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getPersonInfo().getFullName()!=null && e2.getPersonInfo().getFullName()!=null){
						return e1.getPersonInfo().getFullName().compareTo(e2.getPersonInfo().getFullName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetCustomerCellNumber()
	{
		List<CustomerPayment> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerPayment>(list);
		columnSort.setComparator(customerCellColumn, new Comparator<CustomerPayment>()
				{
			@Override
			public int compare(CustomerPayment e1,CustomerPayment e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getPersonInfo().getCellNumber()== e2.getPersonInfo().getCellNumber()){
						return 0;}
					if(e1.getPersonInfo().getCellNumber()> e2.getPersonInfo().getCellNumber()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortingAccountType()
	{
		List<CustomerPayment> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerPayment>(list);
		columnSort.setComparator(accountTypeColumn, new Comparator<CustomerPayment>()
				{
			@Override
			public int compare(CustomerPayment e1,CustomerPayment e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getAccountType()!=null && e2.getAccountType()!=null){
						return e1.getAccountType().compareTo(e2.getAccountType());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetContractCount()
	{
		List<CustomerPayment> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerPayment>(list);
		columnSort.setComparator(contractCountColumn, new Comparator<CustomerPayment>()
				{
			@Override
			public int compare(CustomerPayment e1,CustomerPayment e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getContractCount()== e2.getContractCount()){
						return 0;}
					if(e1.getContractCount()> e2.getContractCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetInvoiceAmount()
	{
		List<CustomerPayment> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerPayment>(list);
		columnSort.setComparator(invoiceAmtColumn, new Comparator<CustomerPayment>()
				{
			@Override
			public int compare(CustomerPayment e1,CustomerPayment e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getInvoiceAmount()== e2.getInvoiceAmount()){
						return 0;}
					if(e1.getInvoiceAmount()> e2.getInvoiceAmount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetPaymentReceived()
	{
		List<CustomerPayment> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerPayment>(list);
		columnSort.setComparator(paymentReceivedAmtColumn, new Comparator<CustomerPayment>()
				{
			@Override
			public int compare(CustomerPayment e1,CustomerPayment e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getPaymentReceived()== e2.getPaymentReceived()){
						return 0;}
					if(e1.getPaymentReceived()> e2.getPaymentReceived()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetStatus()
	{
		List<CustomerPayment> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerPayment>(list);
		columnSort.setComparator(statusColumn, new Comparator<CustomerPayment>()
				{
			@Override
			public int compare(CustomerPayment e1,CustomerPayment e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getStatus()!=null && e2.getStatus()!=null){
						return e1.getStatus().compareTo(e2.getStatus());
					}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetBranch()
	{
		List<CustomerPayment> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerPayment>(list);
		columnSort.setComparator(branchColumn, new Comparator<CustomerPayment>()
				{
			@Override
			public int compare(CustomerPayment e1,CustomerPayment e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBranch()!=null && e2.getBranch()!=null){
						return e1.getBranch().compareTo(e2.getBranch());
					}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	/**03-10-2017 sagar sore [To create number range column and sorting it]**/
	public void addColumnNumberRange()
	{
		numberRangeColumn=new TextColumn<CustomerPayment>() {

			@Override
			public String getValue(CustomerPayment object) {
				return object.getNumberRange();
			}
		};
		
		table.addColumn(numberRangeColumn,"Number Range");
		table.setColumnWidth(numberRangeColumn, 120, Unit.PX);
		numberRangeColumn.setSortable(true);
	}	
	
	protected void addSortinggetNumberRange()
	{
		List<CustomerPayment> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerPayment>(list);
		columnSort.setComparator(numberRangeColumn, new Comparator<CustomerPayment>()
				{
			@Override
			public int compare(CustomerPayment e1,CustomerPayment e2)
			{
				if (e1 != null && e2 != null) {
					
					if (e1.getNumberRange() == null && e2.getNumberRange() == null){
						return 0;
					}
					if(e1.getNumberRange() == null) return 1;							
					if(e2.getNumberRange() == null) return -1;							
					return e1.getNumberRange().compareTo(e2.getNumberRange());

				}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	/**end**/
	
	/**17-07-2017 sagar sore {To add payment date column and column sorting}**/
	public void addColumnPaymentDate()
	{
		paymentDateColumn=new TextColumn<CustomerPayment>() {

			@Override
			public String getValue(CustomerPayment object) {
				if(object.getPaymentDate()!=null)
				{
				    String serDate=AppUtility.parseDate(object.getPaymentDate());
				    return serDate;
				}
				return "Not Set";
			}
		};
		
		table.addColumn(paymentDateColumn,"Payment Date");
		table.setColumnWidth(paymentDateColumn, 100, Unit.PX);
        paymentDateColumn.setSortable(true);
		
			}	
	
	protected void addSortinggetPaymentDate()
	{
		List<CustomerPayment> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerPayment>(list);
		columnSort.setComparator(paymentDateColumn, new Comparator<CustomerPayment>()
				{
			@Override
			public int compare(CustomerPayment e1,CustomerPayment e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getPaymentDate()!=null && e2.getPaymentDate()!=null){
						return e1.getPaymentDate().compareTo(e2.getPaymentDate());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);


	}
	/**end**/	


	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
	/** date 1.11.2018 added by komal for type of order and sub bill type **/
	public void getColTypeOfOrder()
	{
		getColTypeOfOrder=new TextColumn<CustomerPayment>() {

			@Override
			public String getValue(CustomerPayment object) {
				return object.getTypeOfOrder();
			}
		};
		
		table.addColumn(getColTypeOfOrder,"Type Of Order");
		table.setColumnWidth(getColTypeOfOrder, 120, Unit.PX);
		getColTypeOfOrder.setSortable(true);
	}	
	
	protected void addSortingTypeOfOrder()
	{
		List<CustomerPayment> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerPayment>(list);
		columnSort.setComparator(getColTypeOfOrder, new Comparator<CustomerPayment>()
				{
			@Override
			public int compare(CustomerPayment e1,CustomerPayment e2)
			{
				if (e1 != null && e2 != null) {
					
					if (e1.getNumberRange() == null && e2.getNumberRange() == null){
						return 0;
					}
					if(e1.getNumberRange() == null) return 1;							
					if(e2.getNumberRange() == null) return -1;							
					return e1.getNumberRange().compareTo(e2.getNumberRange());

				}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	public void getColSubBillType()
	{
		getColSubBillType=new TextColumn<CustomerPayment>() {

			@Override
			public String getValue(CustomerPayment object) {
				return object.getSubBillType();
			}
		};
		
		table.addColumn(getColSubBillType,"Sub Bill Type");
		table.setColumnWidth(getColSubBillType, 120, Unit.PX);
		getColSubBillType.setSortable(true);
	}	
	
	protected void addSortingSubBillType()
	{
		List<CustomerPayment> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerPayment>(list);
		columnSort.setComparator(getColSubBillType, new Comparator<CustomerPayment>()
				{
			@Override
			public int compare(CustomerPayment e1,CustomerPayment e2)
			{
				if (e1 != null && e2 != null) {
					
					if (e1.getNumberRange() == null && e2.getNumberRange() == null){
						return 0;
					}
					if(e1.getNumberRange() == null) return 1;							
					if(e2.getNumberRange() == null) return -1;							
					return e1.getNumberRange().compareTo(e2.getNumberRange());

				}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}

	/**end**/



}
