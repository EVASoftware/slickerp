package com.slicktechnologies.client.views.paymentinfo.paymentdetails;

import java.util.Vector;

import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.paymentinfo.paymentdetails.PaymentDetailsPresenter.PaymentDetailsPresenterSearch;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

public class PaymentDetailsSearchProxy extends SearchPopUpScreen<CustomerPayment> implements SelectionHandler<Suggestion> {
	
	public IntegerBox ibinvoiceid;
	public IntegerBox ibpaymentid;
	public IntegerBox ibcontractid;
	public PersonInfoComposite personInfo;
	public PersonInfoComposite vendorInfo;
	public DateComparator dateComparator;
	public ListBox lbstatus,lbinvoicetype;
	public TextBox tbinvoicetype;
	public ObjectListBox<Config> olbPaymentMethod;
	public ListBox lbaccountType;
	//************rohan changes here **************
	public ObjectListBox<Branch> olbBranch;
	
	
	
	/*
	 * nidhi
	 * 30-06-2017
	 *  customer category search
	 */
	public ObjectListBox<ConfigCategory> olbCustomerCategory;
	public TextBox tbSegment;
	/*
	 * end
	 */
	
	/*
	 *  nidhi
	 *  17-07-2017
	 *  for rate contract service id search
	 */
	public IntegerBox ibrateContractServiceId;
	
	/*
	 *   end
	 */
	/** 03-10-2017 sagar sore [To add number range field in search pop up]***/
	public ObjectListBox<Config> olbcNumberRange;
	
	/**
	 * Date : 18-11-2017 BY ANIL
	 */
	TextBox tbReferenceNumber;
	
	/**
	 * Date 21-12-2017 added by vijay
	 * for TDS ID 
	 */
	IntegerBox ibTDSId;
	/** date 26.6.2018 added by komal for vendor payment flag**/
	String accountType = ""; 
	/** date 20.10.2018 added by komal**/
	ObjectListBox<String> olbTypeOforder;
	ObjectListBox<String> olbSubBillType;
	
	
	public Object getVarRef(String varName)
	{
		if(varName.equals("ibinvoiceid"))
			return this.ibinvoiceid;
		if(varName.equals("ibpaymentid"))
			return this.ibpaymentid;
		if(varName.equals("ibcontractid"))
			return this.ibcontractid;
		if(varName.equals("tbLeadId"))
			return this.personInfo;
		if(varName.equals("personInfo"))
			return this.olbBranch;
		if(varName.equals("vendorInfo"))
			return this.vendorInfo;
		if(varName.equals("dateComparator"))
			return this.dateComparator;
		
		if(varName.equals("lbstatus"))
			return this.lbstatus;
					
		if(varName.equals("lbinvoicetype"))
			return this.lbinvoicetype;
					
		if(varName.equals("tbinvoicetype"))
			return this.tbinvoicetype;
					
		if(varName.equals("olbPaymentMethod"))
			return this.olbPaymentMethod;
								
		if(varName.equals("lbaccountType"))
			return this.lbaccountType;
		/** Date 11/10/2017 added by komal for followup date **/
		if(varName.equals("olbBranch"))
			return this.olbBranch;

		if(varName.equals("olbCustomerCategory"))
			return this.olbCustomerCategory;
		
		if(varName.equals("tbSegment"))
			return this.tbSegment;
		
		if(varName.equals("ibrateContractServiceId"))
			return this.ibrateContractServiceId;
		
		if(varName.equals("olbcNumberRange"))
			return this.olbcNumberRange;
		
		if(varName.equals("tbReferenceNumber"))
			return this.tbReferenceNumber;
		
		if(varName.equals("ibTDSId"))
			return this.ibTDSId;
		
		if(varName.equals("olbTypeOforder"))
			return this.olbTypeOforder;
		
		if(varName.equals("olbSubBillType"))
			return this.olbSubBillType;
		
		return null ;
	}
	
	public PaymentDetailsSearchProxy() {
		super();
		createGui();
	}
	
	
	public PaymentDetailsSearchProxy(boolean b) {
		super(b);
		createGui();
	}
	/** date 26.6.2018 added by komal for vendor payment flag**/
	public PaymentDetailsSearchProxy(String accType) {
		super();
		accountType = accType;
		createGui();
	}
	
	public PaymentDetailsSearchProxy(String accType, boolean b) {
		super(b);
		accountType = accType;
		createGui();
	}

	public void initWidget()
	{
		ibinvoiceid=new IntegerBox();
		ibpaymentid=new IntegerBox();
		ibcontractid=new IntegerBox();
		
		
		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new Customer());
		personInfo=new PersonInfoComposite(querry,false);
		
		//**************rohan made changes here ************ 
		
		olbBranch= new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbBranch);
		
		
		
		MyQuerry vendorquerry=new MyQuerry();
		vendorquerry.setQuerryObject(new Vendor());
		vendorInfo=new PersonInfoComposite(vendorquerry,false);
		
		
		dateComparator=new DateComparator("paymentDate",new CustomerPayment());
		lbstatus= new ListBox();
		AppUtility.setStatusListBox(lbstatus,CustomerPayment.getStatusList());
		olbPaymentMethod=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbPaymentMethod, Screen.PAYMENTMETHODS);
		lbinvoicetype=new ListBox();
		lbinvoicetype.addItem("SELECT");
		lbinvoicetype.addItem("Proforma Invoice");
		lbinvoicetype.addItem("Tax Invoice");
		
		lbaccountType=new ListBox();
		lbaccountType.addItem("SELECT");
		lbaccountType.addItem(AppConstants.BILLINGACCOUNTTYPEAP);
		lbaccountType.addItem(AppConstants.BILLINGACCOUNTTYPEAR);

		personInfo.getCustomerId().getHeaderLabel().setText("Customer ID");
		personInfo.getCustomerName().getHeaderLabel().setText("Customer Name");
		personInfo.getCustomerCell().getHeaderLabel().setText("Customer Cell");
		
		vendorInfo.getCustomerId().getHeaderLabel().setText("Vendor ID");
		vendorInfo.getCustomerName().getHeaderLabel().setText("Vendor Name");
		vendorInfo.getCustomerCell().getHeaderLabel().setText("Vendor Cell");
		
		personInfo.getId().addSelectionHandler(this);
		vendorInfo.getId().addSelectionHandler(this);
		personInfo.getName().addSelectionHandler(this);
		vendorInfo.getName().addSelectionHandler(this);
		personInfo.getPhone().addSelectionHandler(this);
		vendorInfo.getPhone().addSelectionHandler(this);
		
		/*
		 * 	nidhi
		 *  30-06-2017
		 *  for search
		 */
		
		
		olbCustomerCategory= new ObjectListBox<ConfigCategory>();
//		AppUtility.makeTypeListBoxLive(olbCustomerCategory, Screen.CONTRACTTYPE);
		AppUtility.MakeLiveCategoryConfig(olbCustomerCategory, Screen.CUSTOMERCATEGORY);
				
		tbSegment = new TextBox();
		/* 
		 * 
		 *   end
		 */
		
		/*
		 *  nidhi
		 *  18-07-2017
		 */
		ibrateContractServiceId = new IntegerBox();
		/*
		 *  
		 */
		/**03-10-2017 sagar sore[NumberRange initialization, Displaying values from process configuration]***/
		olbcNumberRange= new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbcNumberRange,Screen.NUMBERRANGE);
		
		tbReferenceNumber=new TextBox();
		
		/**
		 * Date 21-12-2017 added by vijay
		 */
		ibTDSId = new IntegerBox();
		/** date 20.10.2018 added by komal**/
		olbTypeOforder = new ObjectListBox<String>();
		olbTypeOforder.addItem("--SELECT--");
		olbTypeOforder.addItem(AppConstants.ORDERTYPESALES);
		olbTypeOforder.addItem(AppConstants.ORDERTYPESERVICE);
		olbTypeOforder.addItem(AppConstants.ORDERTYPEPURCHASE);
		
		olbSubBillType = new ObjectListBox<String>();
		AppUtility.setStatusListBox(olbSubBillType, Invoice.getSubBillTypeList());
//		olbSubBillType.addItem("--SELECT--");
//		olbSubBillType.addItem(AppConstants.STAFFINGANDRENTAL);
//		olbSubBillType.addItem(AppConstants.CONSUMABLES);
//		olbSubBillType.addItem(AppConstants.NATIONALHOLIDAY);
//		olbSubBillType.addItem(AppConstants.OVERTIME);
//		olbSubBillType.addItem(AppConstants.OTHERSERVICES);
	}
	
	public void createScreen()
	{
		initWidget();
		FormFieldBuilder builder;
		builder = new FormFieldBuilder("Payment ID",ibpaymentid);
		FormField fibpaymentid= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Invoice ID",ibinvoiceid);
		FormField fibinvoiceid= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("",personInfo);
		FormField fPersonInfo= builder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		builder = new FormFieldBuilder("",vendorInfo);
		FormField fvendorInfo= builder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		builder = new FormFieldBuilder("Order ID",ibcontractid);
		FormField fibcontractid= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("From Date (Payment Date)",dateComparator.getFromDate());
		FormField fdateComparatorfrom= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("To Date (Payment Date)",dateComparator.getToDate());
		FormField fdateComparatorto= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Status",lbstatus);
		FormField flbstatus=builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Invoice Document Type",lbinvoicetype);
		FormField flbinvoicetype= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Payment Mehthod",olbPaymentMethod);
		FormField folbPaymentMethod=builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Account Type",lbaccountType);
		FormField flbaccountType= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Branch",olbBranch);
		FormField folbBranch=builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/*
		 * 	nidhi
		 * 	30-6-2017
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "OnlyForNBHC"))
		{
			builder = new FormFieldBuilder("Segment Type",olbCustomerCategory);
			
		}
		else{
			builder = new FormFieldBuilder("Segment Type",tbSegment);
		}
		FormField ftbSagment= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/*
		 *  nidhi
		 *  17-07-2017
		 *  Service Id serach
		 */
		
		builder = new FormFieldBuilder("Service ID",ibrateContractServiceId);
		FormField fibserviceid= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		/*
		 *  end
		 */
		/**03-10-2017 sagar sore[ adding search by number range]**/
		FormField folbcNumberRange;	
		builder = new FormFieldBuilder("Number Range",olbcNumberRange);
		folbcNumberRange= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/**
		 * Date : 18-11-2017 BY ANIL
		 */
		builder = new FormFieldBuilder("Reference Number",tbReferenceNumber);
		FormField ftbReferenceNumber= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/**
		 * Date 21-12-2017 added by vijay
		 */
		builder = new FormFieldBuilder("TDS Id",ibTDSId);
		FormField fibTDSId= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/**
		 * Date 25-01-2018 By Vijay 
		 * differentiated date combination search and other filter search
		 */
		builder = new FormFieldBuilder();
		FormField fgroupingDateFilterInformation=builder.setlabel("Date Filter Combination").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();

		builder = new FormFieldBuilder();
		FormField fgroupingOtherFilterInformation=builder.setlabel("Other Filter Combination").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		/**
		 * ends here
		 */
		/** date 20.10.2018 added by komal**/
		builder = new FormFieldBuilder("Type Of Order",olbTypeOforder);
		FormField folbTypeOforder= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Sub Bill Type",olbSubBillType);
		FormField folbSubBillType= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		this.fields=new FormField[][]{
				/**To add field number range and order of fields updated**/
				/**old code**/
//				{fdateComparatorfrom,fdateComparatorto,folbBranch},
//				{fibpaymentid,fibinvoiceid,fibcontractid},
//				{flbstatus,flbinvoicetype,folbPaymentMethod},
//				{flbaccountType,fibserviceid,ftbSagment},
//				{fPersonInfo},
//				{fvendorInfo}
				/**updated code**/
				{fgroupingDateFilterInformation},
				{fdateComparatorfrom,fdateComparatorto,folbBranch,flbstatus},
				{fgroupingOtherFilterInformation},
				{fibpaymentid,fibinvoiceid,flbinvoicetype,folbcNumberRange},
				{fibserviceid,ftbSagment,fibcontractid,ftbReferenceNumber},//flbaccountType
				{folbPaymentMethod,fibTDSId ,folbTypeOforder , folbSubBillType},/** date 20.10.2018 added by komal**/
				{fPersonInfo},
				{fvendorInfo}
				
		};
	}
	
	@Override
	public MyQuerry getQuerry() {
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		
		if(dateComparator.getValue()!=null)
			filtervec.addAll(dateComparator.getValue());
		
		if(ibinvoiceid.getValue()!=null){
			temp=new Filter();
			temp.setIntValue(ibinvoiceid.getValue());
			filtervec.add(temp);
			temp.setQuerryString("invoiceCount");
			filtervec.add(temp);
		}
		
		if(ibpaymentid.getValue()!=null){
			temp=new Filter();
			temp.setIntValue(ibpaymentid.getValue());
			filtervec.add(temp);
			temp.setQuerryString("count");
			filtervec.add(temp);
		}
		
		if(ibcontractid.getValue()!=null){
			temp=new Filter();
			temp.setIntValue(ibcontractid.getValue());
			filtervec.add(temp);
			temp.setQuerryString("contractCount");
			filtervec.add(temp);
		}
		
		/** date 26.6.2018 changed by komal**/
		//if(lbaccountType.getSelectedIndex()!=0){	
			temp=new Filter();
			//changed by komal
			if(accountType.equalsIgnoreCase(AppConstants.BILLINGACCOUNTTYPEAP)){
				temp.setStringValue(AppConstants.BILLINGACCOUNTTYPEAP);
			}else{
				temp.setStringValue(AppConstants.BILLINGACCOUNTTYPEAR);
			}
			temp.setQuerryString("accountType");
			filtervec.add(temp);
		//}

		
		  if(olbPaymentMethod.getSelectedIndex()!=0){
				temp=new Filter();temp.setStringValue(olbPaymentMethod.getValue().trim());
				temp.setQuerryString("paymentMethod");
				filtervec.add(temp);
			}
		  if(lbinvoicetype.getSelectedIndex()!=0){
				temp=new Filter();
				temp.setStringValue(lbinvoicetype.getItemText(lbinvoicetype.getSelectedIndex()));
				filtervec.add(temp);
				temp.setQuerryString("invoiceType");
				filtervec.add(temp);
			}
		  
		if(personInfo.getIdValue()!=-1)
		  {
		  temp=new Filter();
		  temp.setIntValue(personInfo.getIdValue());
		  temp.setQuerryString("personInfo.count");
		  filtervec.add(temp);
		  }
		  
		  if(!(personInfo.getFullNameValue().equals("")))
		  {
		  temp=new Filter();
		  temp.setStringValue(personInfo.getFullNameValue());
		  temp.setQuerryString("personInfo.fullName");
		  filtervec.add(temp);
		  }
		  if(personInfo.getCellValue()!=-1l)
		  {
		  temp=new Filter();
		  temp.setLongValue(personInfo.getCellValue());
		  temp.setQuerryString("personInfo.cellNumber");
		  filtervec.add(temp);
		  }
		  
		  if(vendorInfo.getIdValue()!=-1)
		  {
		  temp=new Filter();
		  temp.setIntValue(vendorInfo.getIdValue());
		  temp.setQuerryString("personInfo.count");
		  filtervec.add(temp);
		  }
		  
		  if(!(vendorInfo.getFullNameValue().equals("")))
		  {
		  temp=new Filter();
		  temp.setStringValue(vendorInfo.getFullNameValue());
		  temp.setQuerryString("personInfo.fullName");
		  filtervec.add(temp);
		  }
		  if(vendorInfo.getCellValue()!=-1l)
		  {
		  temp=new Filter();
		  temp.setLongValue(vendorInfo.getCellValue());
		  temp.setQuerryString("personInfo.cellNumber");
		  filtervec.add(temp);
		  }
		  
		  if(lbstatus.getSelectedIndex()!=0){
				temp=new Filter();
				temp.setStringValue(lbstatus.getItemText(lbstatus.getSelectedIndex()));
				filtervec.add(temp);
				temp.setQuerryString("status");
				filtervec.add(temp);
			}
		
		  
		  if(LoginPresenter.branchRestrictionFlag==true && olbBranch.getSelectedIndex()==0)
			{
				temp=new Filter();
				temp.setList(AppUtility.getbranchlist());
				temp.setQuerryString("branch IN");
				filtervec.add(temp);
			}
			else{
				if(olbBranch.getSelectedIndex()!=0){
					temp=new Filter();
					temp.setStringValue(olbBranch.getValue().trim());
					temp.setQuerryString("branch");
					filtervec.add(temp);
				}
			}
						
						
			/*
			 *  nidhi 
			 *  1-07-2017
			 *  
			 */
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "OnlyForNBHC"))
			{
				if(olbCustomerCategory.getSelectedIndex()!=0){
					temp=new Filter();
					temp.setStringValue(olbCustomerCategory.getValue().trim());
					temp.setQuerryString("segment");
					filtervec.add(temp);
				}
			}else{
					
				if(!tbSegment.getValue().equals("")){
					
					temp=new Filter();
					temp.setStringValue(tbSegment.getValue().trim());
					temp.setQuerryString("segment");
					filtervec.add(temp);
				}
			}
			/*
			 *  end
			 */
			/*
			 *  nidhi
			 *   18-07-2017
			 *    service ID serach
			 */
			
			if(ibrateContractServiceId.getValue()!=null){
				temp=new Filter();
				temp.setIntValue(ibrateContractServiceId.getValue());
				temp.setQuerryString("rateContractServiceId");
				filtervec.add(temp);
			}	
			/*
			 *  end
			 */
		  
			 /**03-10-2017 sagar sore[ filter for search by number range i.e. Billing and Non-billing]**/
		if(olbcNumberRange.getSelectedIndex()!=0)
		 {
				temp=new Filter();
				temp.setStringValue(olbcNumberRange.getValue().trim());
				temp.setQuerryString("numberRange");
				filtervec.add(temp);
		 }	
						
		/**
		 * Date : 18-11-2017 BY ANIL
		 */
		if(tbReferenceNumber.getValue()!=null&&!tbReferenceNumber.getValue().equals("")){
			temp=new Filter();
			temp.setStringValue(tbReferenceNumber.getValue());
			temp.setQuerryString("refNumber");
			filtervec.add(temp);
		}
		
		
		/**
		 * Date 21-12-2017 added by vijay
		 * for TDS ID search
		 */
		if(ibTDSId.getValue()!=null){
			temp = new Filter();
			temp.setIntValue(ibTDSId.getValue());
			temp.setQuerryString("tdsId");
			filtervec.add(temp);
		}
		
		/** date 20.10.2018 added by komal**/
		if(olbTypeOforder.getSelectedIndex() != 0){
			temp=new Filter();
			temp.setStringValue(olbTypeOforder.getValue(olbTypeOforder.getSelectedIndex()));
			temp.setQuerryString("typeOfOrder");
			filtervec.add(temp);
		}
		
		if(olbSubBillType.getSelectedIndex() != 0){
			temp=new Filter();
			temp.setStringValue(olbSubBillType.getValue(olbSubBillType.getSelectedIndex()));
			temp.setQuerryString("subBillType");
			filtervec.add(temp);
		}
		MyQuerry querry= new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new CustomerPayment());
		return querry;
	}

	
	
	@Override
	public void onSelection(SelectionEvent<Suggestion> event) 
	{
		if(event.getSource().equals(personInfo.getId())||event.getSource().equals(personInfo.getName())||event.getSource().equals(personInfo.getPhone()))
		{
			if(!vendorInfo.getId().getValue().equals("")||!vendorInfo.getName().getValue().equals("")||!vendorInfo.getPhone().getValue().equals(""))
			{
				vendorInfo.clear();
			}
		}
		
		if(event.getSource().equals(vendorInfo.getId())||event.getSource().equals(vendorInfo.getName())||event.getSource().equals(vendorInfo.getPhone()))
		{
			if(!personInfo.getId().getValue().equals("")&&!personInfo.getName().getValue().equals("")&&!personInfo.getPhone().getValue().equals(""))
			{
				personInfo.clear();
			}
		}
	}

	@Override
	public boolean validate() {
		
//		if(LoginPresenter.branchRestrictionFlag)
//		{
//		if(olbBranch.getSelectedIndex()==0)
//		{
//			showDialogMessage("Select Branch");
//			return false;
//		}
//		}
		
		/**
		 * Date 25-01-2018 By Vijay 
		 * for Date combination validation
		 */
		String msg = "Wrong filter selection with From Date and To Date ! Please select proper filters with Date";
		if(dateComparator.getFromDate().getValue()!=null || dateComparator.getToDate().getValue()!=null ){
			if(ibinvoiceid.getValue()!=null || ibpaymentid.getValue()!=null || ibcontractid.getValue()!=null ||
			   lbaccountType.getSelectedIndex()!=0 || olbPaymentMethod.getSelectedIndex()!=0 || lbinvoicetype.getSelectedIndex()!=0 ||
			   personInfo.getIdValue()!=-1 || !(personInfo.getFullNameValue().equals("")) || personInfo.getCellValue()!=-1l ||
			   vendorInfo.getIdValue()!=-1 || !(vendorInfo.getFullNameValue().equals("")) || vendorInfo.getCellValue()!=-1l ||
			   olbCustomerCategory.getSelectedIndex()!=0  ||
			   !tbSegment.getValue().equals("") || ibrateContractServiceId.getValue()!=null ||	olbcNumberRange.getSelectedIndex()!=0 ||
			   tbReferenceNumber.getValue()!=null&&!tbReferenceNumber.getValue().equals("") || ibTDSId.getValue()!=null ){
				showDialogMessage(msg);
				return false;
			}
		}
		/**
		 * ends here
		 */
		

		
		//Ashwini Patil Date:27-09-2023 
		boolean filterAppliedFlag=false;
		
		if((dateComparator.getFromDate().getValue()!=null && dateComparator.getToDate().getValue()!=null)||olbBranch.getSelectedIndex()!=0||lbstatus.getSelectedIndex()!=0||
				ibpaymentid.getValue()!=null ||ibinvoiceid.getValue()!=null ||lbinvoicetype.getSelectedIndex()!=0 ||olbcNumberRange.getSelectedIndex()!=0||
				ibrateContractServiceId.getValue()!=null|| !tbSegment.getValue().equals("")||ibcontractid.getValue()!=null ||!tbReferenceNumber.getValue().equals("")||
				olbPaymentMethod.getSelectedIndex()!=0 ||ibTDSId.getValue()!=null|| olbTypeOforder.getSelectedIndex()!=0||olbSubBillType.getSelectedIndex()!=0 ||
				personInfo.getIdValue()!=-1 || vendorInfo.getIdValue()!=-1){
			filterAppliedFlag=true;
		}
		
		if(!filterAppliedFlag){
			showDialogMessage("Please apply at least one filter!");
			return false;
		}
		
		if((dateComparator.getFromDate().getValue()!=null && dateComparator.getToDate().getValue()!=null)){
			int diffdays = AppUtility.getDifferenceDays(dateComparator.getFromDate().getValue(), dateComparator.getToDate().getValue());
			Console.log("diffdays "+diffdays);
			if(diffdays>31){
				showDialogMessage("Please select from date and to date range within 30 days");
				return false;
			}
		}	
		return true;
	}
	

	

}
