package com.slicktechnologies.client.views.paymentinfo.paymentdetails;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.Slick_Erp;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.reusabledata.ConditionDialogBox;
import com.slicktechnologies.client.services.CommonService;
import com.slicktechnologies.client.services.CommonServiceAsync;
import com.slicktechnologies.client.services.CommunicationLogService;
import com.slicktechnologies.client.services.CommunicationLogServiceAsync;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.EmailService;
import com.slicktechnologies.client.services.EmailServiceAsync;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.services.SmsService;
import com.slicktechnologies.client.services.SmsServiceAsync;
import com.slicktechnologies.client.services.UpdateService;
import com.slicktechnologies.client.services.UpdateServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.cnc.CNCForm;
import com.slicktechnologies.client.views.cnc.CNCPresenter;
import com.slicktechnologies.client.views.communicationlog.CommunicationLogPopUp;
import com.slicktechnologies.client.views.contract.ContractForm;
import com.slicktechnologies.client.views.contract.ContractPresenter;
import com.slicktechnologies.client.views.documentcancellation.DocumentCancellationPopUp;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.BillingDetailsForm;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.BillingDetailsPresenter;
import com.slicktechnologies.client.views.paymentinfo.billinglist.BillingListPresenter;
import com.slicktechnologies.client.views.paymentinfo.invoicedetails.InvoiceDetailsForm;
import com.slicktechnologies.client.views.paymentinfo.invoicedetails.InvoiceDetailsPresenter;
import com.slicktechnologies.client.views.paymentinfo.invoicelist.InvoiceListPresenter;
import com.slicktechnologies.client.views.paymentinfo.paymentlist.PaymentListPresenter;
import com.slicktechnologies.client.views.paymentinfo.vendorinvoicedetails.VendorInvoiceDetailsForm;
import com.slicktechnologies.client.views.paymentinfo.vendorinvoicedetails.VendorInvoiceDetailsPresenter;
import com.slicktechnologies.client.views.paymentinfo.vendorinvoicelist.VendorInvoiceListPresenter;
import com.slicktechnologies.client.views.popups.NewEmailPopUp;
import com.slicktechnologies.client.views.popups.PaymentUploaderPopUp;
import com.slicktechnologies.client.views.popups.SMSPopUp;
import com.slicktechnologies.client.views.popups.TallyFilePaymentUploaderPopUp;
import com.slicktechnologies.client.views.purchase.purchaseorder.PurchaseOrderForm;
import com.slicktechnologies.client.views.purchase.purchaseorder.PurchaseOrderPresenter;
import com.slicktechnologies.client.views.purchase.servicepo.ServicePoForm;
import com.slicktechnologies.client.views.purchase.servicepo.ServicePoPresenter;
import com.slicktechnologies.client.views.salesorder.SalesOrderForm;
import com.slicktechnologies.client.views.salesorder.SalesOrderPresenter;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.PettyCash;
import com.slicktechnologies.shared.PettyCashDeposits;
import com.slicktechnologies.shared.common.PettyCashTransaction;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessprocesslayer.ServicePo;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessprocesslayer.VendorDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cnc.CNC;
import com.slicktechnologies.shared.common.contactidentification.ContactPersonIdentification;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.helperlayer.SmsConfiguration;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.BillingDocumentDetails;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;
import com.slicktechnologies.shared.common.salesprocess.VendorInvoice;
import com.slicktechnologies.shared.common.smshistory.SmsHistory;
import com.slicktechnologies.shared.common.tallyaccounts.AccountingInterface;

public class PaymentDetailsPresenter extends FormScreenPresenter<CustomerPayment> implements ValueChangeHandler<Double>, ChangeHandler{
	
	public PaymentDetailsForm form;
	final GenricServiceAsync async=GWT.create(GenricService.class);
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	EmailServiceAsync emailService=GWT.create(EmailService.class);
	DocumentCancellationPopUp popup = new DocumentCancellationPopUp();
	PopupPanel panel ;
	
	//   rohan added this code for updating payment details
	UpdateServiceAsync updateAsync = GWT.create(UpdateService.class);
	
	/****  for SMS ****/
	final SmsServiceAsync smsserviceAsync=GWT.create(SmsService.class);
	private String actualmsg;
	private long cellNo;
	private String companyName;
	int payableAmt =0;
	double finalTaxAmt =0;
	int hygeia=0;
	double totalNoOfTaxes =0;
	
	/**
	 * Date 1 march 2017 added by vijay 
	 * below flag for validation purpose to add cash payment into petty cash entry if process configuration is active.
	 * requirement for pestcure 
	 */
	public static boolean processConfigAddtoPettyCashEntry = false;
	/**
	 * ends here
	 * 
	 */
	
	/** Date 21-12-2017 added by vijay for TDS Update Popup *************/
	TDSUpdatePopUp tdsUpdatePopup = new TDSUpdatePopUp();
	GeneralServiceAsync generalasync = GWT.create(GeneralService.class);
	
	/**
	 * Date 09-05-2018
	 * Developer : Vijay
	 * Des : for Communication Log
	 */
	CommunicationLogPopUp CommunicationLogPopUp = new CommunicationLogPopUp();
	CommunicationLogServiceAsync communicationService = GWT.create(CommunicationLogService.class);
	/**
	 * ends here
	 */
	
	PaymentUploaderPopUp paymentupload = new PaymentUploaderPopUp();
	TallyFilePaymentUploaderPopUp tallypaymentupload = new TallyFilePaymentUploaderPopUp();
	
	//  rohan added this cnt for preprint functionality 
	ConditionDialogBox conditionPopupforPreprint=new ConditionDialogBox("Do you want to print on preprinted Stationery",AppConstants.YES,AppConstants.NO);
	int cnt=0;
	
	int pecopp = 0;
	
 	NewEmailPopUp emailpopup = new NewEmailPopUp();
 	SMSPopUp smspopup = new SMSPopUp();
    CommonServiceAsync commonservice = GWT.create(CommonService.class);

	public PaymentDetailsPresenter(UiScreen<CustomerPayment> view, CustomerPayment model) {
		super(view, model);
		form=(PaymentDetailsForm) view;
		form.setPresenter(this);
		popup.getBtnOk().addClickHandler(this);
		popup.getBtnCancel().addClickHandler(this);
		
		form.getCbtdsApplicable().addClickHandler(this);
		form.getDbamtreceived().addValueChangeHandler(this);
		form.getDbtdsValue().addValueChangeHandler(this);
		form.getTbtdsPerValue().addValueChangeHandler(this);
		form.getIbNetPay().addValueChangeHandler(this);
		conditionPopupforPreprint.getBtnOne().addClickHandler(this);
		conditionPopupforPreprint.getBtnTwo().addClickHandler(this);
		
		form.olbPaymentMethod.addChangeHandler(this);
		form.cbaddintoPettyCash.addClickHandler(this);
		
		/** Date 21-12-2017 added by vijay for TDS Update Popup clickhandler ************/
		tdsUpdatePopup.getLblOk().addClickHandler(this);
		tdsUpdatePopup.getLblCancel().addClickHandler(this);
		
		/**
		 * Date 09-05-2018
		 * Developer : Vijay
		 * Des : for Communication Log
		 */
		CommunicationLogPopUp.getBtnOk().addClickHandler(this);
		CommunicationLogPopUp.getBtnCancel().addClickHandler(this);
		CommunicationLogPopUp.getBtnAdd().addClickHandler(this);
		/**
		 * ends here
		 */
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.PAYMENTDETAILS,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();
		
		if(text.equals(AppConstants.SUBMIT)){
			/*** Date 13-10-2018 By Vijay For dont allow user to click twice ***/
			form.showWaitSymbol();
			reactOnSubmit();
		}
		
		if(text.equals("Email")){
			reactOnEmail();
		}
		if(text.equals(AppConstants.CANCELPAYMENT)){
			popup.getPaymentDate().setValue(new Date()+"");
			popup.getPaymentID().setValue(form.getIbpaymentid().getValue());
			popup.getPaymentStatus().setValue(form.getTbstatus().getValue());
			panel=new PopupPanel(true);
			panel.add(popup);
			panel.show();
			panel.center();
		}
		
		//  rohan removed this code because of no need 
//		if(text.equals("Update Records")){
//			reactOnUpdateRecords();
//		}
		
		/**
		 * Date : 10-10-2017 BY ANIL
		 */
		if(text.equals(AppConstants.VIEWINVOICE)){
			if(form.isPopUpAppMenubar()){
				Console.log("Accounts Payment POPUP : View Invoice clicked!!");
				return;
			}
			viewInvoiceDocument();
		}
		/**Added by Sheetal : 15-02-2022**/
		if(text.equals(AppConstants.VIEWORDER))
		{
			if(form.isPopUpAppMenubar()){
				Console.log("Accounts Payment POPUP : View Invoice clicked!!");
				return;
			}
			viewOrderDocument();
		}
		
		if(text.equals(AppConstants.VIEWBILL))
		{
			if(form.isPopUpAppMenubar()){
				Console.log("Accounts Payment POPUP : View Bill clicked!!");
				return;
			}
			viewBillDocument();
		}
		/**end**/
		/**
		 * Date 21-12-2017 added by vijay
		 * for TDS update info and TDS Certificate
		 */
		if(text.equals("Update TDS")){
			if(model.isTdsApplicable()){
				ShowTDSPopUp();
			}else{
				form.showDialogMessage("TDS is not applicable for this payment document");
			}
		}
		/**
		 * ends here
		 */
		if(text.equals("Update All Data")){
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CustomerPayment", "Accounting Interface")){
				form.showDialogMessage("Please deactivate process configuration of Payment document Accounting interface first");
			}else{
				
				generalasync.updateAllPaymentDocumentWithTDSflagIndex(model.getCompanyId(), new AsyncCallback<Integer>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						form.showDialogMessage("Unexpected Error Occurred");
					}

					@Override
					public void onSuccess(Integer result) {
						// TODO Auto-generated method stub
						form.showDialogMessage("Date Updated Successfully");

					}
				});
			}
			
		}
		
		/**
		 * @author Vijay Date :- 13-10-2020
		 * Des :- payment uploader
		 */
		if(text.equals(AppConstants.PAYMENTUPLOAD)){
			paymentupload.showPopUp();
		}
		
		if(text.equals(AppConstants.TALLYPAYMENTUPLOAD)){
			tallypaymentupload.showPopUp();
		}
		
		if(text.equals("Update Accounting Interface")){
			reactonupdateAccountingInterface();
		}
		

		
	}
	/**Added by Sheetal : 15-02-2022**/
	private void viewBillDocument() {

		
		int invoiceId=0;
		
			invoiceId=model.getInvoiceCount();
	
		final MyQuerry querry=new MyQuerry();
		Vector<Filter> temp=new Vector<Filter>();
		Filter filter=null;
		
		filter=new Filter();
		filter.setQuerryString("invoiceCount");
//		filter.setIntValue(model.getCount());
		filter.setIntValue(invoiceId);
		temp.add(filter);
		
		querry.setFilters(temp);
		querry.setQuerryObject(new BillingDocument());
		form.showWaitSymbol();
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				form.hideWaitSymbol();
				if(result.size()==0){
					form.showDialogMessage("No billing document found.");
					return;
				}
				if(result.size()==1){
					final BillingDocument billDocument=(BillingDocument) result.get(0);
					final BillingDetailsForm form=BillingDetailsPresenter.initalize();
					Timer timer=new Timer() {
						@Override
						public void run() {
							form.updateView(billDocument);
							form.setToViewState();
						}
					};
					timer.schedule(1000);
				}else{
					BillingListPresenter.initalize(querry);
				}
				
			}
		});
		
	
		
	}

	/**Added by Sheetal : 15-02-2022**/
	private void viewOrderDocument() {

		final MyQuerry querry=new MyQuerry();
		Vector<Filter> temp=new Vector<Filter>();
		Filter filter=null;
		
		filter=new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(model.getContractCount());
		temp.add(filter);
		
		querry.setFilters(temp);
		if(model.getTypeOfOrder().equals(AppConstants.ORDERTYPESERVICE)){
			querry.setQuerryObject(new Contract());
		}else if(model.getTypeOfOrder().equals(AppConstants.ORDERTYPESALES)){
			querry.setQuerryObject(new SalesOrder());
		}else if(model.getTypeOfOrder().equals(AppConstants.ORDERTYPEPURCHASE)){
			querry.setQuerryObject(new PurchaseOrder());
		}else if(model.getTypeOfOrder().equals(AppConstants.ORDERTYPESERVICEPO)){
			querry.setQuerryObject(new ServicePo());
		}
		if(model.getSubBillType() != null && !model.getSubBillType().equals("")){
			if(checkBillSubType(model.getSubBillType())){
				querry.setQuerryObject(new CNC());
			}
		}
		form.showWaitSymbol();
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				form.hideWaitSymbol();
				if(result.size()==0){
					form.showDialogMessage("No order document found.");
					return;
				}
				
				if(model.getTypeOfOrder().equals(AppConstants.ORDERTYPESERVICE)){
					if(model.getSubBillType() != null && !model.getSubBillType().equals("")){
						if(checkBillSubType(model.getSubBillType())){
							final CNC billDocument=(CNC) result.get(0);
							final CNCForm form=CNCPresenter.initalize();
							Timer timer=new Timer() {
								@Override
								public void run() {
									form.updateView(billDocument);
									form.setToViewState();
								}
							};
							timer.schedule(4000);
						}
					}else{
					final Contract billDocument=(Contract) result.get(0);
					final ContractForm form=ContractPresenter.initalize();
					Timer timer=new Timer() {
						@Override
						public void run() {
							form.updateView(billDocument);
							form.setToViewState();
						}
					};
					timer.schedule(1000);
					}
				}else if(model.getTypeOfOrder().equals(AppConstants.ORDERTYPESALES)){
					final SalesOrder billDocument=(SalesOrder) result.get(0);
					final SalesOrderForm form=SalesOrderPresenter.initalize();
					Timer timer=new Timer() {
						@Override
						public void run() {
							form.updateView(billDocument);
							form.setToViewState();
						}
					};
					timer.schedule(1000);
				}else if(model.getTypeOfOrder().equals(AppConstants.ORDERTYPEPURCHASE)){
					final PurchaseOrder billDocument=(PurchaseOrder) result.get(0);
					final PurchaseOrderForm form=PurchaseOrderPresenter.initalize();
					Timer timer=new Timer() {
						@Override
						public void run() {
							form.updateView(billDocument);
							form.setToViewState();
						}
					};
					timer.schedule(1000);
				}/***11-1-2019 added by amol***/
				else if (model.getTypeOfOrder().equals(AppConstants.ORDERTYPESERVICEPO)){
					final ServicePo billDocument=(ServicePo) result.get(0);
					final ServicePoForm form=ServicePoPresenter.initialize();
					Timer timer=new Timer() {
						@Override
						public void run() {
							form.updateView(billDocument);
							form.setToViewState();
						}
					};
					timer.schedule(1000);
				
					
				}
				
				
			}
		});
		
	
		
	}

	/**
	 * Date 21-12-2017 added by vijay
	 * for TDS Popup for update TDS info and TDS Certificate
	 */
	private void ShowTDSPopUp() {
		// TODO Auto-generated method stub
		tdsUpdatePopup.setEnable(false);
	
		
		tdsUpdatePopup.getLblOk().setText("Update");
		tdsUpdatePopup.showPopUp();
		
		if(model.getTdsId()!=0)
			tdsUpdatePopup.getIbTdsID().setValue(model.getTdsId());
		if(model.getTdsDate()!=null)
			tdsUpdatePopup.getDbTDSDate().setValue(model.getTdsDate());
		if(model.getTdsCertificate()!=null)
			tdsUpdatePopup.getUcUploadTDSCs().setValue(model.getTdsCertificate());
		tdsUpdatePopup.getCbIsTDSCertificate().setValue(model.isTdsCertificateReceived());
		
	}
	/**
	 * ends here
	 */
	public void getTotalNoOfTaxesFromInvoices(final boolean tdsPercentFlag,final boolean netPayFlag) {
//		totalNoOfTaxes =0;
		final MyQuerry querry=new MyQuerry();
		Vector<Filter> temp=new Vector<Filter>();
		Filter filter=null;
		
		filter=new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(model.getInvoiceCount());
		temp.add(filter);
		
		querry.setFilters(temp);
		if(model.getAccountType().equalsIgnoreCase(AppConstants.ACCOUNTTYPEAP)){
			querry.setQuerryObject(new VendorInvoice());
		}else{
			querry.setQuerryObject(new Invoice());
		}
		form.showWaitSymbol();
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				form.hideWaitSymbol();
				if(result.size()==0){
					form.showDialogMessage("No invoice document found.");
					return;
				}
				else {
                      
					for (SuperModel model : result) {
						/**
						 * @author Anil @since 30-06-2021
						 * for vendor payment system was throwing type cast error
						 * issue raised by vaishnavi
						 */
						if(model instanceof VendorInvoice){
							VendorInvoice invoice = (VendorInvoice) model;
							totalNoOfTaxes=invoice.getTotalAmtIncludingTax()/invoice.getFinalTotalAmt();
						}else{
							Invoice invoice = (Invoice) model;
//							for(ContractCharges contractCharge:invoice.getBillingTaxes()){
//								totalNoOfTaxes=totalNoOfTaxes+contractCharge.getTaxChargePercent();
//							}
							totalNoOfTaxes=invoice.getTotalAmtIncludingTax()/invoice.getFinalTotalAmt();
						}
						
						Console.log("value of total no of taxes "+totalNoOfTaxes);
					}
//					DecimalFormat df = new DecimalFormat("0.00");
					if(tdsPercentFlag){
						Console.log("Inside my tds percent");
						if(form.getTbtdsPerValue()!=null&&form.getTbtdsPerValue().getValue()!=0){
							
							form.getDbtdsValue().setEnabled(false);
						
//							Base Amt = Net Receivable/(Tax Rate-(TDS(%)/100))   -----> No Round-off
	//
//						    Tax Amt = Base Amt x (Tax Rate-1)  ----> No Round-off
	//
//							Gross Amt = Base Amt + Tax Amt   ----> Round-off
							
							double amountReceived=0;
							double netReceivedAmt=0;
							double tax=0;
							double tdsPerValue=0;
							
							Console.log("totalno of invoice taxes "+totalNoOfTaxes);
							
							tdsPerValue=form.getTbtdsPerValue().getValue();
							
							netReceivedAmt=form.getIbNetPay().getValue();
////							tax=(totalNoOfTaxes/100);
//							
//							Console.log("total taxes "+tax);
//							Console.log("net recieved amt "+netReceivedAmt);
//							Console.log("tds percentage "+tdsPerValue);
//							
//							
//							
//							amountReceived=netReceivedAmt/(totalNoOfTaxes-(tdsPerValue/100));
//							Console.log("Adjusted base Amount "+amountReceived);
//							
////							double amtReceived=df.format(amountReceived);
//							
//							form.getDbReceivedRevenue().setValue(amountReceived);	
//							
//							
//							
//							double adjtax=0;
//							double tax1=0;
////							tax1=(totalNoOfTaxes/100);
//							
//							adjtax= amountReceived*(totalNoOfTaxes-1);
//							Console.log(" Adjusted Tax Amount "+adjtax);
//							form.getDbReceivedTax().setValue(adjtax);
//							
//							double grossAmt1=0;
//							grossAmt1=amountReceived+adjtax;
//							Console.log(" Amount Received/Paid "+grossAmt1);
//							form.getDbamtreceived().setValue((double) Math.round(grossAmt1));

							/**
							 * @author Vijay Date :- 12-10-2020
							 * Des :- above old code commented and added new code as per new logic given by Nitin Sir
							 * if there is TDS amount it will deduct base amount and whatever first amount will receive first 
							 * adjust in tax amount then into base amount
							 */
							double balanceRevenue = form.getDbBalanceRevenue().getValue();
							double tdsAmount = (balanceRevenue * tdsPerValue)/100;
							double grossAmt=netReceivedAmt+tdsAmount;

							//Ashwini Patil Date:30-06-2022 Innovative requested no round off for amount including tax
							if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CONTRACT", "PC_NO_ROUNDOFF_INVOICE_CONTRACT")){
								
								form.getDbamtreceived().setValue(grossAmt);
							}else {
								form.getDbamtreceived().setValue((double) Math.round(grossAmt));
							}

							setUpdatedValueAdustedBaseAndTaxAmount(netReceivedAmt,0,tdsAmount);
							/**
							 * ends here
							 */

							
						}
					}else if(netPayFlag){
						Console.log("Inside else if tds netPayFlag");
						/**
						 * @author Anil @since 30-06-2021
						 * added one more condition with or 
						 */
						if(form.getIbNetPay().getValue()==null||form.getIbNetPay().getValue()==0)
						{
							Console.log("inside netPay is zero");
							form.getDbReceivedTax().setValue(0.0);
							form.dbReceivedTax.setEnabled(false);
							form.getDbReceivedRevenue().setValue(0.0);
							form.dbReceivedRevenue.setEnabled(false);
							form.getDbamtreceived().setValue(0.0);
							form.dbamtreceived.setEnabled(false);
							form.getTbtdsPerValue().setValue(0.0);
							form.tbtdsPerValue.setEnabled(false);
							form.getDbtdsValue().setValue(0.0);
							form.dbtdsValue.setEnabled(false);
						}else{
							/**
							 * @author Anil @since 30-06-2021 
							 * .getvalue was missing
							 */
							if(form.getTbtdsPerValue().getValue()!=null&&form.getTbtdsPerValue().getValue()!=0){
								Console.log("inside if tds percent is not zero");
								
								double amountReceived=0;
								double netReceivedAmt=0;
								double tax=0;
								double tdsPerValue=0;
								
								Console.log("totalno of invoice taxes "+totalNoOfTaxes);
								
								tdsPerValue=form.getTbtdsPerValue().getValue();
								
								netReceivedAmt=form.getIbNetPay().getValue();
//								tax=(totalNoOfTaxes/100);
								
								Console.log("total taxes "+tax);
								Console.log("net recieved amt "+netReceivedAmt);
								Console.log("tds percentage "+tdsPerValue);
								
//								
//								
//								amountReceived=netReceivedAmt/(totalNoOfTaxes-(tdsPerValue/100));
//								Console.log("Adjusted base Amount "+amountReceived);
//								
////								double amtReceived=df.format(amountReceived);
//								
//								form.getDbReceivedRevenue().setValue(amountReceived);	
//								
//								
//								
//								double adjtax=0;
//								double tax1=0;
////								tax1=(totalNoOfTaxes/100);
//								
//								adjtax= amountReceived*(totalNoOfTaxes-1);
//								Console.log(" Adjusted Tax Amount "+adjtax);
//								form.getDbReceivedTax().setValue(adjtax);
//								
//								double grossAmt1=0;
//								grossAmt1=amountReceived+adjtax;
//								Console.log(" Amount Received/Paid "+grossAmt1);
//								
//								form.getDbamtreceived().setValue((double) Math.round(grossAmt1));
								
								/**
								 * @author Vijay Date :- 12-10-2020
								 * Des :- above old code commented and added new code as per new logic given by Nitin Sir
								 * if there is TDS amount it will deduct base amount and whatever first amount will receive first 
								 * adjust in tax amount then into base amount
								 */
								
								double balanceRevenue = form.getDbBalanceRevenue().getValue();
								double tdsAmount = (balanceRevenue * tdsPerValue)/100;
								double grossAmt=netReceivedAmt+tdsAmount;
								//Ashwini Patil Date:30-06-2022 Innovative requested no round off for amount including tax
								if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CONTRACT", "PC_NO_ROUNDOFF_INVOICE_CONTRACT")){
									
									form.getDbamtreceived().setValue(grossAmt);
								}else {
									form.getDbamtreceived().setValue((double) Math.round(grossAmt));
								}
								setUpdatedValueAdustedBaseAndTaxAmount(netReceivedAmt,0,tdsAmount);

								
								
							}else{
								Console.log("inside else tds percent ZEROOO == ");
								
//								double tax2=0;
								double netReceivedAmt=0;
								double tdsAmt=0;
								double grossAmt=0;
								double baseAmt=0;
								netReceivedAmt=(form.getIbNetPay().getValue());
								tdsAmt=form.getDbtdsValue().getValue();
								grossAmt=netReceivedAmt+tdsAmt;
								
								
								//Ashwini Patil Date:30-06-2022 Innovative requested no round off for amount including tax
								if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CONTRACT", "PC_NO_ROUNDOFF_INVOICE_CONTRACT")){
									
									form.getDbamtreceived().setValue(grossAmt);
								}else {
									form.getDbamtreceived().setValue((double) Math.round(grossAmt));
								}
								
////								tax2=(totalNoOfTaxes/100)+1;
//								baseAmt=grossAmt/(totalNoOfTaxes);
//								form.getDbReceivedRevenue().setValue(baseAmt);
//								
//								form.getDbReceivedTax().setValue(grossAmt-baseAmt);
//								form.getDbBalanceTax().
								
								/**
								 * @author Vijay Date :- 12-10-2020
								 * Des :- above old code commented and added new code as per new logic given by Nitin Sir
								 * if there is TDS amount it will deduct base amount and whatever first amount will receive first 
								 * adjust in tax amount then into base amount
								 */
								setUpdatedValueAdustedBaseAndTaxAmount(netReceivedAmt,baseAmt,tdsAmt);
								
							}
							
						}
						
						
					}
					
					else{
						Console.log("Inside else tds flag");
						if(form.getDbtdsValue().getValue()!=null && form.getDbtdsValue().getValue()!=0)
						{
							
							System.out.println("amt received ="+form.getDbamtreceived().getValue());
							System.out.println("tds amt ="+form.getDbtdsValue().getValue());
							form.getTbtdsPerValue().setEnabled(false);
							
//					
//							Gross Amt = Net Receivable + TDS Amt
		//
//							Base Amt = Gross Amt/ Tax Rate
		//
//							Tax Amt = Gross Amt - Base Amt
							
							double tax2=0;
							double netReceivedAmt=0;
							double tdsAmt=0;
							double grossAmt=0;
							double baseAmt=0;
							netReceivedAmt=(form.getIbNetPay().getValue());
							tdsAmt=form.getDbtdsValue().getValue();
							grossAmt=netReceivedAmt+tdsAmt;
							
							//Ashwini Patil Date:30-06-2022 Innovative requested no round off for amount including tax
							if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CONTRACT", "PC_NO_ROUNDOFF_INVOICE_CONTRACT")){
								Console.log("In no round off");
								form.getDbamtreceived().setValue(grossAmt);
							}else {
								form.getDbamtreceived().setValue((double) Math.round(grossAmt));
							}
							
//							
////							tax2=(totalNoOfTaxes/100)+1;
//							baseAmt=grossAmt/(totalNoOfTaxes);
//							form.getDbReceivedRevenue().setValue(baseAmt);
//							
//							form.getDbReceivedTax().setValue(grossAmt-baseAmt);
							
							/**
							 * @author Vijay Date :- 12-10-2020
							 * Des :- above old code commented and added new code as per new logic given by Nitin Sir
							 * if there is TDS amount it will deduct base amount and whatever first amount will receive first 
							 * adjust in tax amount then into base amount
							 */
							setUpdatedValueAdustedBaseAndTaxAmount(netReceivedAmt,baseAmt,tdsAmt);

						}
					}
				}
				
			}

			
		});
		
		
	}
	
	private void viewInvoiceDocument() {
		final MyQuerry querry=new MyQuerry();
		Vector<Filter> temp=new Vector<Filter>();
		Filter filter=null;
		
		filter=new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(model.getInvoiceCount());
		temp.add(filter);
		
		querry.setFilters(temp);
		if(model.getAccountType().equalsIgnoreCase(AppConstants.ACCOUNTTYPEAP)){
			querry.setQuerryObject(new VendorInvoice());
		}else{
			querry.setQuerryObject(new Invoice());
		}
		form.showWaitSymbol();
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				form.hideWaitSymbol();
				if(result.size()==0){
					form.showDialogMessage("No invoice document found.");
					return;
				}
				if(result.size()==1){
					/** date 25.6.2018 added by komal for vendor invoice form **/
					if(model.getAccountType().equalsIgnoreCase(AppConstants.ACCOUNTTYPEAP)){
						final VendorInvoice billDocument=(VendorInvoice) result.get(0);
						final VendorInvoiceDetailsForm form=VendorInvoiceDetailsPresenter.initalize();
						Timer timer=new Timer() {
							@Override
							public void run() {
								form.updateView(billDocument);
								form.setToViewState();
							}
						};
						timer.schedule(1000);
					}else{
					final Invoice billDocument=(Invoice) result.get(0);
					final InvoiceDetailsForm form=InvoiceDetailsPresenter.initalize();
					Timer timer=new Timer() {
						@Override
						public void run() {
							form.updateView(billDocument);
							form.setToViewState();
						}
					};
					timer.schedule(1000);
					}
				}else{
					/** date 25.6.2018 added by komal for vendor invoice list **/
					if(model.getAccountType().equalsIgnoreCase(AppConstants.ACCOUNTTYPEAP)){
						VendorInvoiceListPresenter.initalize(querry);
					}else{
					InvoiceListPresenter.initalize(querry);
					}
				}
				
			}
		});
		
	}

//	private void reactOnUpdateRecords() {
//		form.showWaitSymbol();
//		updateAsync.updatePaymentDetailsRecords(model.getCompanyId(), new AsyncCallback<Void>() {
//			
//			@Override
//			public void onSuccess(Void result) {
//				form.hideWaitSymbol();
//				System.out.println("Update Successfull.....!");
//			}
//			
//			@Override
//			public void onFailure(Throwable caught) {
//				
//			}
//		});
//	}

	@Override
	public void reactOnPrint() {
		
		//   rohan comments this code as om pest control wants to print from invoice 
		
		MyQuerry querry = new MyQuerry();
	  	Company c = new Company();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
	  	filter = new Filter();
	  	filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("processName");
		filter.setStringValue("CustomerPayment");
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("processList.status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new ProcessConfiguration());
		
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onFailure(Throwable caught) {
			}			

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				System.out.println(" result set size +++++++"+result.size());
				
				List<ProcessTypeDetails> processList =new ArrayList<ProcessTypeDetails>();
				if(result.size()==0){
					final String url = GWT.getModuleBaseURL() + "customerpayslip"+"?Id="+model.getId()+"&"+"preprint="+"plane";
					Window.open(url, "test", "enabled");
					
				}
				else{
				
					for(SuperModel model:result)
					{
						ProcessConfiguration processConfig=(ProcessConfiguration)model;
						processList.addAll(processConfig.getProcessList());
						
					}
				
				for(int k=0;k<processList.size();k++)
				{	
					if (processList.get(k).getProcessType().trim().equalsIgnoreCase("HygeiaPestManagement")&& processList.get(k).isStatus() == true) {

						hygeia = hygeia + 1;

					}
					
					if(processList.get(k).getProcessType().trim().equalsIgnoreCase("CompanyAsLetterHead")&&processList.get(k).isStatus()==true){
						
						cnt=cnt+1;
					
					}
					
					if (processList.get(k).getProcessType().trim().equalsIgnoreCase("EnableCustomPaymentReciept")&& processList.get(k).isStatus() == true) {

						pecopp = pecopp + 1;
						Console.log("pecopp =="+pecopp);
					}
				}
				
					if(cnt>0){
						System.out.println("in side react on prinnt cnt");
						panel=new PopupPanel(true);
						panel.add(conditionPopupforPreprint);
						panel.setGlassEnabled(true);
						panel.show();
						panel.center();
					}
					else if(hygeia>0)
					{
						System.out.println("in side react on prinnt cnt");
						panel=new PopupPanel(true);
						panel.add(conditionPopupforPreprint);
						panel.setGlassEnabled(true);
						panel.show();
						panel.center();
						
					}
					else if(pecopp>0){
						Console.log("inside pecopp");
						final String url = GWT.getModuleBaseURL() + "custompayslip"+"?Id="+model.getId()+"&"+"preprint="+"plane";
						Window.open(url, "test", "enabled");
					}
					else
					{
						final String url = GWT.getModuleBaseURL() + "customerpayslip"+"?Id="+model.getId()+"&"+"preprint="+"plane";
						Window.open(url, "test", "enabled");
					}
			}
			}
		});
		//**************************changes ends here ********************************
	}

	@Override
	public void reactOnEmail() {
		
//		boolean conf = Window.confirm("Do you really want to send email?");
//		if (conf == true) {
//		emailService.initiatePaymentReceiveEmail(model,new AsyncCallback<Void>() {
//
//			@Override
//			public void onFailure(Throwable caught) {
//				Window.alert("Unable To Send Email");
//				caught.printStackTrace();
//			}
//
//			@Override
//			public void onSuccess(Void result) {
//				Window.alert("Email Sent Sucessfully !");
//			}
//		});
//		}
		
		/**
		 * @author Vijay Date 19-03-2021
		 * Des :- above old code commented 
		 * Added New Email Popup Functionality
		 */
		emailpopup.showPopUp();
		setEmailPopUpData();
		
		/**
		 * ends here
		 */
		
	}
	
	@Override
	public void reactOnDownload(){
		
		final ArrayList<CustomerPayment> paylist=new ArrayList<CustomerPayment>();
		List<CustomerPayment> listbill=(List<CustomerPayment>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
		paylist.addAll(listbill); 
		csvservice.setpaymentlist(paylist, new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);
				
			}

			@Override
			public void onSuccess(Void result) {
				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "csvservlet"+"?type="+16;
				Window.open(url, "test", "enabled");
				
////				final String url=gwt + "CreateXLXSServlet"+"?type="+16;
////				Window.open(url, "test", "enabled");
//				
//				
//				ArrayList<String> summarypaymentlist = new ArrayList<String>();
//				summarypaymentlist.add("BRANCH");
//				summarypaymentlist.add("COUNT");
//				summarypaymentlist.add("AMOUNT RECEIVED");
//				/** date 03.03.2018 added by komal for revenue and tax calculation **/
//				summarypaymentlist.add("BASE");
//				summarypaymentlist.add("TAX");
//				summarypaymentlist.add("TDS");
//				summarypaymentlist.add("OUTSTANDING");
//				summarypaymentlist.add("Write OFF");
//
//				int summaryPaymentColumnCount = summarypaymentlist.size();
//				
//				HashMap<String, ArrayList<CustomerPayment>> paymnethashmaplist = new HashMap<String, ArrayList<CustomerPayment>>();
//				
//				for(int i=0;i<paylist.size();i++){
//					
//					ArrayList<CustomerPayment> list = new ArrayList<CustomerPayment>();
//					if(paymnethashmaplist.containsKey(paylist.get(i).getBranch())){
//						list = paymnethashmaplist.get(paylist.get(i).getBranch());
//						list.add(paylist.get(i));
//					}else{
//						list.add(paylist.get(i));
//					}
//					paymnethashmaplist.put(paylist.get(i).getBranch(), list);
//				}
//				
//				Set s = paymnethashmaplist.entrySet();
//				Iterator iterator = s.iterator();
//				
//				double sumTotalAmt=0;
//				double sumTDSTotalAmt=0;
//				double sumAmtReceived=0;
//				double sumbalancedAmount=0;
//				double sumCount =0;
//				/** date 03.03.2018 added by komal for revenue and tax calculation **/
//				double sumReceivedRevenue = 0;
//				double sumReceivedTax = 0;		
//				/**
//				 * end komal
//				 */
//
//				/**
//				 * Date 27-09-2018 By Vijay
//				 * Des Write off Amt
//				 */
//				double sumWriteOffAmt=0;
//				
//				while (iterator.hasNext()) {	
//					ArrayList<CustomerPayment> paymentlist  = new ArrayList<CustomerPayment>();
//					Map.Entry<String, ArrayList<CustomerPayment>> specificBranchlist = (Map.Entry<String, ArrayList<CustomerPayment>>)iterator.next();
//					paymentlist.addAll(specificBranchlist.getValue());
//					
//					double totalAmount = 0;
//					double tdsTotalAmt = 0;
//					double amountReceived = 0;
//					double balancedAmount = 0;
//					/** date 03.03.2018 added by komal for revenue and tax calculation **/
//					double receivedRevenue = 0;
//					double receivedTax = 0;		
//					/**
//					 * end komal
//					 */
//					/**
//					 * Date 27-09-2018 By Vijay
//					 * Des Write off Amt
//					 */
//					double totalWriteOffAmt=0;
//					
//					summarypaymentlist.add(paymentlist.get(0).getBranch());
//					summarypaymentlist.add(paymentlist.size()+"");
//					
//					HashSet<Integer> customerPayment = new HashSet<Integer>();
//					
//					for(int j=0;j<paymentlist.size();j++){
//						
//						if(!paymentlist.get(j).getStatus().equals("Cancelled")){
//
//						customerPayment.add(paymentlist.get(j).getInvoiceCount());
//						
//						
//						if(paymentlist.get(j).getStatus().equals("Closed")){
//							amountReceived += paymentlist.get(j).getPaymentReceived();
//							/** date 03.03.2018 added by komal for revenue and tax calculation **/
//							receivedRevenue += paymentlist.get(j).getReceivedRevenue();
//							receivedTax += paymentlist.get(j).getReceivedTax();		
//							/**
//							 * end komal
//							 */
//							 /**
//							 * Date 25-10-2018 By Vijay showing as per the amount recieved for TDS
//							 */
//							if(paymentlist.get(j).isTdsApplicable()){
//								tdsTotalAmt += paymentlist.get(j).getTdsTaxValue();
//							}
//						}
//						if(paymentlist.get(j).getStatus().equals("Created")){
//							balancedAmount += paymentlist.get(j).getPaymentAmt();
//						}
//						
//						}
//						if(paymentlist.get(j).getStatus().equals("Cancelled")){
//							totalWriteOffAmt += paymentlist.get(j).getPaymentAmt();
//						}
//						
//					}
//					
//					
//					summarypaymentlist.add(amountReceived+"");
//					/** date 03.03.2018 added by komal for revenue and tax calculation **/
//					summarypaymentlist.add(receivedRevenue+"");
//					summarypaymentlist.add(receivedTax+"");
//					/**
//					 * end komal
//					 */
//					summarypaymentlist.add(tdsTotalAmt+"");
//					summarypaymentlist.add(balancedAmount+"");
//					summarypaymentlist.add(totalWriteOffAmt+"");
//
//					sumCount +=paymentlist.size();
////					sumTotalAmt +=totalAmount;
//					sumTDSTotalAmt +=tdsTotalAmt;
//					sumAmtReceived +=amountReceived;
//					sumbalancedAmount +=balancedAmount;
//					/** date 03.03.2018 added by komal for revenue and tax calculation **/
//					sumReceivedRevenue += receivedRevenue;
//					sumReceivedTax += receivedTax;
//					/**
//					 * end komal
//					 */
//					/*** Date 27-09-2018 by Vijay for Cancelled total amount ***/ 
//					sumWriteOffAmt +=totalWriteOffAmt;
//					
//				}
//				
//				for(int i=0;i<summaryPaymentColumnCount;i++) {
//					summarypaymentlist.add("");
//				}
//				summarypaymentlist.add("TOTAL");
//				summarypaymentlist.add(sumCount+"");
//				summarypaymentlist.add(sumAmtReceived+"");
//				/** date 03.03.2018 added by komal for revenue and tax calculation **/
//				summarypaymentlist.add(sumReceivedRevenue+"");
//				summarypaymentlist.add(sumReceivedTax+"");
//				/**
//				 * end komal
//				 */
//				summarypaymentlist.add(sumTDSTotalAmt+"");
//				summarypaymentlist.add(sumbalancedAmount+"");
//				summarypaymentlist.add(sumWriteOffAmt+"");
//				
//				for(int i=0;i<summaryPaymentColumnCount;i++) {
//					summarypaymentlist.add(" ");
//				}		
//				
//				
//
//					/** payment list sorting by branch **/
//					Comparator<CustomerPayment> branchComparator = new Comparator<CustomerPayment>() {
//						public int compare(CustomerPayment s1, CustomerPayment s2) {
//						
//						String branch1 = s1.getBranch();
//						String branch2 = s2.getBranch();
//						
//						//ascending order
//						return branch1.compareTo(branch2);
//						}
//						
//						};
//						Collections.sort(paylist, branchComparator);
//					
//				/**
//				 * ends here
//				 */
//				
//				Date today = new Date();
//				/**
//				 *  Added By Priyanka - 18-08-2021
//				 *  
//				 */
//				Long companyId = paylist.get(0).getCompanyId();
//				
//				ArrayList<String> paymentlist = new ArrayList<String>();
//				paymentlist.add("BRANCH");
//				paymentlist.add("PAYMENT ID");
//				paymentlist.add("INVOICE ID");
//				paymentlist.add("INVOICE DATE");
//				paymentlist.add("PAYMENT DATE");
//				paymentlist.add("BP ID");
//				paymentlist.add("BP NAME");
//				paymentlist.add("CELL NUMBER");
//				paymentlist.add("ORDER ID");
//				paymentlist.add("ORDER START DATE");
//				paymentlist.add("ORDER END DATE");
//				paymentlist.add("ACCOUNT TYPE");
//				paymentlist.add("ORDER AMOUNT");
//				paymentlist.add("INVOICE AMOUNT");
//				paymentlist.add("INVOICE TYPE");
//				paymentlist.add("PAYMENT METHOD");
//				/** date 21.02.2018 added by komal for revenue and tax calcualtion **/
//				paymentlist.add("AMOUNT RECEIVED/PAID");
//				/**
//				 * Date 24-4-2018
//				 * By jayshree
//				 * Des.add sales person name
//				 */
//				paymentlist.add("SALES PERSON NAME");
//				paymentlist.add("ADJUSTED BASE");
//				paymentlist.add("ADJUSTED TAX");
//				/** 
//				 * end komal
//				 */
//				/*
//				 *  Added By Priyanka - 18/08/2021
//		         *  Des : Innovative : Rename TDS with WHT Requirmnet raised by Rahul
//				 */
//				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CustomerPayment", "RenameTDSWithWHT")){
//					paymentlist.add("WHT %");
//
//				}else{
//					paymentlist.add("TDS %");
//
//				}
//				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CustomerPayment", "RenameTDSWithWHT")){
//					paymentlist.add("WHT AMOUNT");
//				}else{
//					paymentlist.add("TDS AMOUNT");
//				}
//				/**
//				 *  End
//				 */
//				paymentlist.add("NET RECEIVED");
//				/** date 21.02.2018 added by komal for revenue and tax calcualtion **/
//				paymentlist.add("GROSS AMOUNT");
//				paymentlist.add("BASE AMOUNT");
//				paymentlist.add("TAX AMOUNT");
//				paymentlist.add("BALANCE AMOUNT");
//				paymentlist.add("STATUS");
//				paymentlist.add("SEGMENT");
//				/*
//				 *  1-07-2017
//				 *   nidhi
//				 */
//				paymentlist.add("SERVICE ID");
//				/*
//				 *  end
//				 */
//				
//				/** 08-11-2017 sagar sore**/
//				paymentlist.add("Aging");
//				paymentlist.add("TDS ID");
//				paymentlist.add("TDS Date");
//				paymentlist.add("COMMENT");
//				/**
//				 * Date 2-5-2018
//				 * by jayshree
//				 * add cheque date,number ,bank name requrment of rahul pawar sir
//				 */
//				paymentlist.add("CHEQUE DATE");
//				paymentlist.add("CHEQUE NUMBER");
//				paymentlist.add("CHEQUE BANK NAME");
//				/* end**/
//				
//				/**
//				 * Date 10-05-2018
//				 * Developer : Vijay
//				 */
//				paymentlist.add("Follow-Up Date");
//				/**
//				 * ends here
//				 */
//				paymentlist.add("Ref No.2");
//
//		        /**@author Sheetal : 09-06-2022
//				 * Des : Adding Payment Received Date and Digital-Reference No. ,requirement by ankita pest control**/
//				paymentlist.add("Payment Received Date");
//				paymentlist.add("Digital-Reference No");
//				
//				int paymentColumnCount = paymentlist.size();
//				
//				for(CustomerPayment payment:paylist)
//				{
//					
//					paymentlist.add(payment.getBranch());
//					paymentlist.add(payment.getCount()+"");
//					paymentlist.add(payment.getInvoiceCount()+"");
//
//					if(payment.getInvoiceDate()!=null){
//						paymentlist.add(AppUtility.parseDate(payment.getInvoiceDate(),"dd-MMM-yyyy"));
//					}
//					if(payment.getPaymentDate()!=null){
//						paymentlist.add(AppUtility.parseDate(payment.getPaymentDate(),"dd-MMM-yyyy"));
//					}else{
//						paymentlist.add("");
//
//					}
//					paymentlist.add(payment.getPersonInfo().getCount()+"");
//					paymentlist.add(payment.getPersonInfo().getFullName());
//					paymentlist.add(payment.getPersonInfo().getCellNumber()+"");
//					paymentlist.add(payment.getContractCount()+"");
//
//					/**
//					 * rohan 
//					 */
//					
//					if(payment.getContractStartDate()!=null){
//						paymentlist.add(AppUtility.parseDate(payment.getContractStartDate(),"dd-MMM-yyyy"));
//					}
//					else{
//						paymentlist.add("N.A");
//					}
//					if(payment.getContractEndDate()!=null){
//						paymentlist.add(AppUtility.parseDate(payment.getContractEndDate(),"dd-MMM-yyyy"));
//					}
//					else{
//						paymentlist.add("N.A");
//					}
//					paymentlist.add(payment.getAccountType());
//					paymentlist.add(payment.getTotalSalesAmount()+"");
//					paymentlist.add(payment.getInvoiceAmount()+"");
//					paymentlist.add(payment.getInvoiceType());
//					paymentlist.add(payment.getPaymentMethod());
//					paymentlist.add(payment.getPaymentReceived()+"");
//					/** Date 24-4-2018 By jayshree add sales person name**/
//					paymentlist.add(payment.getEmployee());
//					/** date 21.02.2018 added by komal for revenue and tax calcualtion **/
//					paymentlist.add(payment.getReceivedRevenue()+"");
//					paymentlist.add(payment.getReceivedTax()+"");
//					/** 
//					 * end komal
//					 */
//				
//					//Ashwini Patil Date:28-06-2022 TDS percentage were not getting printed on worksheet because of wrong condition. Corrected the condition
//					if(payment.getTdsPercentage()!=null && !payment.getTdsPercentage().equals("") )
//					{
//						paymentlist.add(payment.getTdsPercentage()+"");
//					}
//					else
//					{
//						paymentlist.add("0");
//					}
//					if(payment.getTdsTaxValue()!=0){
//						paymentlist.add(payment.getTdsTaxValue()+"");
//					}
//					else
//					{
//						paymentlist.add("0");
//					}
//					if(payment.getNetPay()!=0){
//						paymentlist.add(payment.getNetPay()+"");
//					}else
//					{
//						paymentlist.add("");
//					}
//					/** date 21.02.2018 added by komal for revenue and tax calcualtion **/
//					paymentlist.add(payment.getPaymentAmt()+"");
//					paymentlist.add(payment.getBalanceRevenue()+"");
//					paymentlist.add(payment.getBalanceTax()+"");
//					paymentlist.add(payment.getPaymentAmt()-payment.getPaymentReceived()+"");
//					/** 
//					 * end komal
//					 */
//					paymentlist.add(payment.getStatus());
//
//					
//				
//						/*
//						 *  nidhi
//						 *  1-07-2017
//						 */
//						if(payment.getSegment()!=null){
//							paymentlist.add(payment.getSegment()+"");
//						}else{
//							paymentlist.add("");
//						}
//						/*
//						 *  end
//						 */
//								
//						/**
//						 * Date : 17-07-2017 Nidhi
//						 */
//						if(payment.getRateContractServiceId()!=0){
//							paymentlist.add(payment.getRateContractServiceId()+"");
//						}
//						else{
//							paymentlist.add("");
//						}
//						/**
//						 * End
//						 */
//						
//						/**8-11-2017 sagar sore**/
//						if(payment.getPaymentDate()!=null)
//						{
//							long diff = today.getTime() - payment.getPaymentDate().getTime();
//							long diffDays = diff / (24 * 60 * 60 * 1000);
//							paymentlist.add(diffDays+"");
//
//						}
//						else
//							paymentlist.add("");
//						
//						
//						/******** Date 21-12-2017 added by vijay for TDS ID and TDS Date *********/
//						if(payment.getTdsId()!=0){
//							paymentlist.add(payment.getTdsId()+"");
//						}else{
//							paymentlist.add("");
//						}
//						
//						if(payment.getTdsDate()!=null){
//							paymentlist.add(AppUtility.parseDate(payment.getTdsDate(),"dd-MMM-yyyy"));
//						}else{
//							paymentlist.add("");
//						}
//						/** date 15.02.2018 added by komal for hvac **/
//						if(payment.getComment()!=null){
//							paymentlist.add(payment.getComment());
//						}else{
//							paymentlist.add("");
//						}
//						
//						/**
//						 * Date 2-5-2018
//						 * By jayshree
//						 * Des.add the cheque number ,date ,bank name
//						 */
//						
//						if(payment.getChequeDate()!=null){
//							paymentlist.add(AppUtility.parseDate(payment.getChequeDate(),"dd-MMM-yyyy"));
//						}else{
//							paymentlist.add("");
//						}
//						
//						if(payment.getChequeNo()!=null){
//							paymentlist.add(payment.getChequeNo()+"");
//						}else{
//							paymentlist.add("");
//						}
//						
//						if(payment.getBankName()!=null){
//							paymentlist.add(payment.getBankName()+"");
//						}else{
//							paymentlist.add("");
//						}
//						
//						/**
//						 * Date 10-05-2018
//						 * Developer : Vijay
//						 */
//						if(payment.getFollowUpDate()!=null){
//							paymentlist.add(AppUtility.parseDate(payment.getFollowUpDate(),"dd-MMM-yyyy"));
//						}else{
//							paymentlist.add("");
//						}
//						/**
//						 * ends here
//						 */
//						
//						/**End **/
//						if(payment.getRefNumberTwo()!=null){
//							paymentlist.add(payment.getRefNumberTwo());
//						}else{
//							paymentlist.add("");
//						}
//						
//						/**@author Sheetal : 09-06-2022
//						 * Des : Adding Payment Received Date and Digital-Reference No. ,requirement by ankita pest control**/
//						
//						if(payment.getAmountTransferDate()!=null) {
//							paymentlist.add(AppUtility.parseDate(payment.getAmountTransferDate(),"dd-MMM-yyyy"));
//
//						}else {
//							paymentlist.add("");
//						}
//						
//						if(payment.getReferenceNo()!=null) {
//							paymentlist.add(payment.getReferenceNo());
//						}else {
//							paymentlist.add("");
//						}
//						/**end**/
//					
//						
//					}
//				
//				csvservice.setExcelData(paymentlist, paymentColumnCount, AppConstants.PAYMENTDETAILS, new AsyncCallback<String>() {
//					
//					@Override
//					public void onSuccess(String result) {
//						// TODO Auto-generated method stub
//						
//						String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
//						final String url=gwt + "CreateXLXSServlet"+"?type="+16;
//						Window.open(url, "test", "enabled");
//					}
//					
//					@Override
//					public void onFailure(Throwable caught) {
//						// TODO Auto-generated method stub
//						
//					}
//				});
//						
//				
			}
		});
	}
	

	@Override
	protected void makeNewModel() {
		model=new CustomerPayment();
	}
	
	public static PaymentDetailsForm initalize()
	{
		//AppMemory.getAppMemory().currentState=ScreeenState.NEW;
		PaymentDetailsForm form=new  PaymentDetailsForm();
		PaymentDetailsTableProxy gentable=new PaymentDetailsTableProxy();
		gentable.setView(form);
		gentable.applySelectionModle();
		PaymentDetailsSearchProxy.staticSuperTable=gentable;
		PaymentDetailsSearchProxy searchpopup=new PaymentDetailsSearchProxy();
		form.setSearchpopupscreen(searchpopup);

		PaymentDetailsPresenter presenter=new PaymentDetailsPresenter(form,new CustomerPayment());
		/**
		 * Date : 29-07-2017 BY ANIL
		 * This line was added to update the name of screen loaded from any other screen or by clicking on payment details on account model
		 */
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Accounts/Payment Details",Screen.PAYMENTDETAILS);
		/**
		 * END
		 */
//		form.setToNewState();
		AppMemory.getAppMemory().stickPnel(form);
		
		return form;
	}
	
	
	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.salesprocess.CustomerPayment")
	public static  class PaymentDetailsPresenterSearch extends SearchPopUpScreen<CustomerPayment>{

		@Override
		public MyQuerry getQuerry() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public boolean validate() {
			// TODO Auto-generated method stub
			return true;
		}};

		@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.salesprocess.CustomerPayment")
		public static class PaymentDetailsPresenterTable extends SuperTable<CustomerPayment> implements GeneratedVariableRefrence{

			@Override
			public Object getVarRef(String varName) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void createTable() {

			}

			@Override
			protected void initializekeyprovider() {
				// TODO Auto-generated method stub

			}

			@Override
			public void addFieldUpdater() {
				// TODO Auto-generated method stub

			}

			@Override
			public void setEnable(boolean state) {

			}

			@Override
			public void applyStyle() {

			}};
			
			
			@Override
			public void onClick(ClickEvent event) {
				
				super.onClick(event);

				if(event.getSource().equals(conditionPopupforPreprint.getBtnOne()))
				{
					
					if(cnt >0){
						System.out.println("in side one yes");
						
						
						final String url = GWT.getModuleBaseURL() + "customerpayslip"+"?Id="+model.getId()+"&"+"preprint="+"yes";
						Window.open(url, "test", "enabled");
						 
						 panel.hide();
					}
					
					else if(hygeia >0)
					{
						final String url = GWT.getModuleBaseURL()+ "pdfreceipt" + "?Id=" + model.getId()+"&"+"preprint="+"yes";
						Window.open(url, "test", "enabled");
						
						 panel.hide();
					}
					else if(pecopp>0){
						Console.log("inside pecopp");
						final String url = GWT.getModuleBaseURL() + "custompayslip"+"?Id="+model.getId()+"&"+"preprint="+"yes";
						Window.open(url, "test", "enabled");
					}
				}
				
				
				if(event.getSource().equals(conditionPopupforPreprint.getBtnTwo()))
				{
					if(cnt >0){					
						
						final String url = GWT.getModuleBaseURL() + "customerpayslip"+"?Id="+model.getId()+"&"+"preprint="+"no";
						Window.open(url, "test", "enabled");
						 
							panel.hide();
					}
					
					else if(hygeia >0)
					{
						final String url = GWT.getModuleBaseURL()+ "pdfreceipt" + "?Id=" + model.getId()+"&"+"preprint="+"no";
						Window.open(url, "test", "enabled");
						
						panel.hide();
					}
					else if(pecopp>0){
						Console.log("inside pecopp");
						final String url = GWT.getModuleBaseURL() + "custompayslip"+"?Id="+model.getId()+"&"+"preprint="+"no";
						Window.open(url, "test", "enabled");
					}
				}
				
				
		if (event.getSource() == popup.getBtnOk()) {
			// panel.hide();
			//
			// if(model.getStatus().equalsIgnoreCase("Created"))
			// {
			// changeStatusOnCancellationInCreatedState();
			// }
			// else if(model.getStatus().equalsIgnoreCase("Closed"))
			// {
			// changeStatusOnCancellation();
			// }
			if (popup.getRemark().getValue().trim() != null
					&& !popup.getRemark().getValue().trim().equals("")) {
				panel.hide();
				if (model.getStatus().equalsIgnoreCase("Created")) {
					changeStatusOnCancellationInCreatedState();
				} else if (model.getStatus().equalsIgnoreCase("Closed")) {
					changeStatusOnCancellation();
				}
			} else {
				form.showDialogMessage("Please enter valid remark to cancel Payment..!");
			}

		}
				
				else if(event.getSource()==popup.getBtnCancel()){
					panel.hide();
					popup.remark.setValue("");
				}
				
				if (event.getSource() == form.getCbtdsApplicable()) {
					
					if (form.getIbNetPay().getValue()!=null && !form.getIbNetPay().equals("")) {
		
						if (form.getCbtdsApplicable().getValue().equals(true)) {
							form.getTbtdsPerValue().setEnabled(true);
							form.getDbtdsValue().setEnabled(true);
							form.getDbamtreceived().setEnabled(false);
						}
		
						
						if (form.getCbtdsApplicable().getValue().equals(false)) {
							form.getTbtdsPerValue().setEnabled(false);
							form.getDbtdsValue().setEnabled(false);
//							form.getDbamtreceived().setEnabled(false);
							form.getDbReceivedTax().setValue(0.0);
							form.dbReceivedTax.setEnabled(false);
							form.getDbReceivedRevenue().setValue(0.0);
							form.dbReceivedRevenue.setEnabled(false);
							form.getDbamtreceived().setValue(0.0);
							form.dbamtreceived.setEnabled(false);
							form.getTbtdsPerValue().setValue(0.0);
							form.getDbtdsValue().setValue(0.0);
							form.getIbNetPay().setValue(0.0);
//							form.getIbNetPay().setValue(form.getDbamtreceived().getValue()+"");
							/** date 21.02.2018 added by komal for revenue and tax calculation **/
							double amountReceived = form.getDbamtreceived().getValue();
							if(form.getIbpaymentAmt().isVisible()) {
							
							form.getDbBalanceRevenue().setValue(form.getIbpaymentAmt().getValue() - form.getDbBalanceTax().getValue());
							if(amountReceived == form.getIbpaymentAmt().getValue()){
								form.getDbReceivedRevenue().setValue(form.getDbBalanceRevenue().getValue());
								form.getDbReceivedTax().setValue(form.getDbBalanceTax().getValue());
							}
							if(amountReceived < form.getIbpaymentAmt().getValue()){
								double tax = 0;
								if(form.getDbBalanceTax().getValue() != null){
									tax = form.getDbBalanceTax().getValue();
								}
								if(amountReceived < tax){
									form.getDbReceivedRevenue().setValue(null);
									form.getDbReceivedTax().setValue(amountReceived);
								}
								if(amountReceived == tax){
									form.getDbReceivedRevenue().setValue(null);
									form.getDbReceivedTax().setValue(tax);
								}
								if(amountReceived > tax){
									form.getDbReceivedRevenue().setValue(amountReceived - tax);
									form.getDbReceivedTax().setValue(tax);
								}
							}
							}else {
								
								form.getDbBalanceRevenue().setValue(form.getDbpaymentAmt().getValue() - form.getDbBalanceTax().getValue());
								if(amountReceived == form.getDbpaymentAmt().getValue()){
									form.getDbReceivedRevenue().setValue(form.getDbBalanceRevenue().getValue());
									form.getDbReceivedTax().setValue(form.getDbBalanceTax().getValue());
								}
								if(amountReceived < form.getDbpaymentAmt().getValue()){
									double tax = 0;
									if(form.getDbBalanceTax().getValue() != null){
										tax = form.getDbBalanceTax().getValue();
									}
									if(amountReceived < tax){
										form.getDbReceivedRevenue().setValue(null);
										form.getDbReceivedTax().setValue(amountReceived);
									}
									if(amountReceived == tax){
										form.getDbReceivedRevenue().setValue(null);
										form.getDbReceivedTax().setValue(tax);
									}
									if(amountReceived > tax){
										form.getDbReceivedRevenue().setValue(amountReceived - tax);
										form.getDbReceivedTax().setValue(tax);
									}
								}
								
							}
						}
					} else {
						
						form.showDialogMessage("Please Enter Net Received Amount.");
						form.getCbtdsApplicable().setValue(false);
					}
				}
				
				/**
				 * Date 3 March 2017 added by vijay for payment entry in petty cash
				 * for only cash
				 */
				if(event.getSource() == form.cbaddintoPettyCash){
					if(form.cbaddintoPettyCash.getValue()){
						form.olbpettyCashName.setEnabled(true);
						form.olbpettyCashName.setSelectedIndex(0);
					}else{
						form.olbpettyCashName.setEnabled(false);
						form.olbpettyCashName.setSelectedIndex(0);
					}
				}
				
				/**
				 * ends here
				 */
				
				
				
				/**
				 * Date 3 March 2017 added by vijay for payment entry in petty cash
				 * for only cash
				 */
				if(event.getSource() == form.cbaddintoPettyCash){
					if(form.cbaddintoPettyCash.getValue()){
						form.olbpettyCashName.setEnabled(true);
						form.olbpettyCashName.setSelectedIndex(0);
					}else{
						form.olbpettyCashName.setEnabled(false);
						form.olbpettyCashName.setSelectedIndex(0);
					}
				}
				
				/**
				 * ends here
				 */
				
				/**
				 * Date 21-12-2017 added by vijay
				 * for TDS Update popup Update and cancel 
				 */
				
				if(event.getSource() instanceof InlineLabel){
					
					InlineLabel lbl = (InlineLabel) event.getSource();

					if(lbl.getText().equals("Update")){
						if(!form.tbstatus.getValue().equalsIgnoreCase("Cancelled")){
							tdsUpdatePopup.getLblOk().setText("Save TDS");
							tdsUpdatePopup.setEnable(true);
						}
						
					}
					else if(lbl.getText().equals("Save TDS")){
						if(validateTDSPopup()){
							saveTdsInfo();
						}
					}
					if(lbl.getText().equals("Cancel")){
						System.out.println("On cancel popup");
						tdsUpdatePopup.hidePopUp();
					}
					
				}
				/**
				 * ends here
				 */
				
			
				/**
				 * Date 09-05-2018
				 * Developer : Vijay
				 * Des : for Communication Log
				 */
				if(event.getSource() == CommunicationLogPopUp.getBtnOk()){
					form.showWaitSymbol();
				    List<InteractionType> list = CommunicationLogPopUp.getCommunicationLogTable().getDataprovider().getList();
				   
				    ArrayList<InteractionType> interactionlist = new ArrayList<InteractionType>();
				    interactionlist.addAll(list);
				    
				    boolean checkNewInteraction = AppUtility.checkNewInteractionAdded(interactionlist);
				    
				    if(checkNewInteraction==false){
				    	form.showDialogMessage("Please add new interaction details");
				    	form.hideWaitSymbol();
				    }	
				    else{	
				    	/**
				    	 * Date : 08-11-2017 KOMAL
				    	 */
				    	final Date followUpDate = interactionlist.get(interactionlist.size()-1).getInteractionDueDate();
				    	 
				    	/**
				    	 * @author Anil,Date : 06-02-2019
				    	 * Saving follow up date in payment details
				    	 * raised by Rahul Tiwari for Perfect pest control
				    	 */
				    	model.setFollowUpDate(followUpDate);
				    	service.save(model, new AsyncCallback<ReturnFromServer>() {
							@Override
							public void onFailure(Throwable caught) {
							}
							@Override
							public void onSuccess(ReturnFromServer result) {
								form.getDbFollowUpDate().setValue(followUpDate);
							}
						});
				    	/**
				    	 * 
				    	 */
				    	communicationService.saveCommunicationLog(interactionlist, new AsyncCallback<Void>() {

								@Override
								public void onFailure(Throwable caught) {
									form.showDialogMessage("Unexpected Error");
									form.hideWaitSymbol();
									LoginPresenter.communicationLogPanel.hide();
								}

								@Override
								public void onSuccess(Void result) {
									form.showDialogMessage("Data Save Successfully");
									form.hideWaitSymbol();
									model.setFollowUpDate(followUpDate);
									form.getDbFollowUpDate().setValue(followUpDate);
									LoginPresenter.communicationLogPanel.hide();
								}
							});
				    }
				   
				}
				if(event.getSource() == CommunicationLogPopUp.getBtnCancel()){
					LoginPresenter.communicationLogPanel.hide();
				}
				if(event.getSource() == CommunicationLogPopUp.getBtnAdd()){
						form.showWaitSymbol();
						String remark = CommunicationLogPopUp.getRemark().getValue();
						Date dueDate = CommunicationLogPopUp.getDueDate().getValue();
						String interactiongGroup =null;
						if(CommunicationLogPopUp.getOblinteractionGroup().getSelectedIndex()!=0)
							interactiongGroup=CommunicationLogPopUp.getOblinteractionGroup().getValue(CommunicationLogPopUp.getOblinteractionGroup().getSelectedIndex());
						boolean validationFlag = AppUtility.validateCommunicationlog(remark,dueDate);
						if(validationFlag){
							InteractionType communicationLog =  AppUtility.getCommunicationLog(AppConstants.ACCOUNTMODULE,AppConstants.PAYMENTDETAILS,model.getCount(),model.getEmployee(),remark,dueDate,model.getPersonInfo(),null,interactiongGroup, model.getBranch(),"");
							CommunicationLogPopUp.getCommunicationLogTable().getDataprovider().getList().add(communicationLog);
							CommunicationLogPopUp.getRemark().setValue("");
							CommunicationLogPopUp.getDueDate().setValue(null);
							CommunicationLogPopUp.getOblinteractionGroup().setSelectedIndex(0);
						}
						form.hideWaitSymbol();
				}
				/**
				 *ends here
				 */
				
				
			}

			/**
			 * Date 21-12-2017 added by vijay 
			 * for TDS popup updated information Saving to database
			 */

			private void saveTdsInfo() {
				if(tdsUpdatePopup.getIbTdsID().getValue()!=null){
					model.setTdsId(tdsUpdatePopup.getIbTdsID().getValue());
				}
				if(tdsUpdatePopup.getDbTDSDate().getValue()!=null){
					model.setTdsDate(tdsUpdatePopup.getDbTDSDate().getValue());
				}
				
				if(tdsUpdatePopup.getUcUploadTDSCs()!=null){
					model.setTdsCertificate(tdsUpdatePopup.getUcUploadTDSCs().getValue());
				}
				model.setTdsCertificateReceived(tdsUpdatePopup.getCbIsTDSCertificate().getValue());
				
				System.out.println("TDS infor");
				System.out.println(model.getTdsId());
				System.out.println(model.getTdsDate());
				System.out.println(model.getTdsCertificate());
				System.out.println(model.isTdsCertificateReceived());

				async.save(model, new AsyncCallback<ReturnFromServer>() {
					
					@Override
					public void onSuccess(ReturnFromServer result) {
						// TODO Auto-generated method stub
						form.showDialogMessage("TDS Updated Successfully");
						tdsUpdatePopup.hidePopUp();
//						setModel(model);
					}
					
					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						form.showDialogMessage("Unexpected Error Occurred");
						tdsUpdatePopup.hidePopUp();
					}
				});
			}

			private boolean validateTDSPopup() {
				if(tdsUpdatePopup.getDbTDSDate().getValue()==null){
					form.showDialogMessage("TDS Date can not be blank !");
					return false;
				}
				return true;
			}
			
			/**
			 * ends here
			 */


//			private void calclulateBaseAmmount(){
//				
//				
//				System.out.println("rohan inside method="+form.getIbamtreceived().getValue());
//				
//				Vector<Filter> filtrvec = new Vector<Filter>();
//				Filter filtr = null;
//				
//				filtr = new Filter();
//				filtr.setLongValue(model.getCompanyId());
//				filtr.setQuerryString("companyId");
//				filtrvec.add(filtr);
//				
//				filtr = new Filter();
//				filtr.setIntValue(Integer.parseInt(form.getIbinvoiceid().getValue().trim()));
//				filtr.setQuerryString("count");
//				filtrvec.add(filtr);
//				
//				MyQuerry querry = new MyQuerry();
//				querry.setFilters(filtrvec);
//				querry.setQuerryObject(new Invoice());
////				form.showWaitSymbol();
//				
//				async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
//					
//					@Override
//					public void onSuccess(ArrayList<SuperModel> result) {
//						System.out.println("Rohan in side success "+result.size());
//						if(result.size()!=0){
//							for(SuperModel model:result){
//								
//								Invoice invoice = (Invoice) model;
//								
//								double amtReceived = form.getIbamtreceived().getValue();
//								double vatTaxAmt=0;
//								double serviceTaxAmt=0;
//								double taxPercent = 0;
//								for (int i = 0; i < invoice.getSalesOrderProducts().size(); i++) {
//									
//									taxPercent = invoice.getSalesOrderProducts().get(i).getServiceTax().getPercentage();
//									if(i==0){
//									 if((invoice.getSalesOrderProducts().get(i).getVatTax().getPercentage()!=0 ))
//									 {
//										 vatTaxAmt = vatTaxAmt + (amtReceived *(invoice.getSalesOrderProducts().get(i).getVatTax().getPercentage())/(100+invoice.getSalesOrderProducts().get(i).getVatTax().getPercentage()));
//										 System.out.println("Amt Afetr vat tax calculation:"+vatTaxAmt);
//									 }
//									
//									 if((invoice.getSalesOrderProducts().get(i).getServiceTax().getPercentage()!=0 ))
//									 {
//										 
//										 serviceTaxAmt = serviceTaxAmt +((amtReceived - vatTaxAmt)*(invoice.getSalesOrderProducts().get(i).getServiceTax().getPercentage())/(100+invoice.getSalesOrderProducts().get(i).getServiceTax().getPercentage()));
//										 System.out.println("service tax Amt in case of vat/ vat=0 :"+serviceTaxAmt);
//									 }
//									}
//									else
//									{
//										if(taxPercent != invoice.getSalesOrderProducts().get(i).getServiceTax().getPercentage())
//										{
//											 serviceTaxAmt = serviceTaxAmt +((amtReceived - vatTaxAmt)*(invoice.getSalesOrderProducts().get(i).getServiceTax().getPercentage())/(100+invoice.getSalesOrderProducts().get(i).getServiceTax().getPercentage()));
//											 System.out.println("service tax Amt in case of vat/ vat=0 :"+serviceTaxAmt);
//										}
//										
//									}
//								}
//								double roundedBasePayAmt = Math.round(amtReceived - serviceTaxAmt);
//								form.getBasePayable().setValue(roundedBasePayAmt+"");
//								payableAmt = (int) (amtReceived - serviceTaxAmt);
//								finalTaxAmt = serviceTaxAmt;
//								}
//						}
//					}
//
//					@Override
//					public void onFailure(Throwable caught) {
//						
//					}
//					});
//				
//			}
			
			

			private void reactOnSubmit()
			{
				if(form.getOlbPaymentMethod().getSelectedIndex()==0)
				{
					form.showDialogMessage("Payment Method is Mandatory!");
					form.hideWaitSymbol();//Vijay
				}
				if(form.getDbamtreceived().getValue()==null || form.getDbamtreceived().getValue()<0)  //Ashwini Patil Date: 11-03-2022 Description: amount received can be zero
				{
					form.showDialogMessage("Amount Received is Mandatory!");
					form.hideWaitSymbol();//Vijay
				}
				if(form.getOlbPaymentMethod().getSelectedIndex()!=0&&form.getDbamtreceived().getValue()>0)
				{
					chkPaymentReceived();
				}
			}
			
		
			
			private void chkPaymentReceived()
			{	double payableAmt;
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CONTRACT", "PC_NO_ROUNDOFF_INVOICE_CONTRACT")){
					
					payableAmt=form.getDbpaymentAmt().getValue();
				}else
					payableAmt=form.getIbpaymentAmt().getValue();
				
				double payreceived=form.getDbamtreceived().getValue();
				
				Console.log("payableAmt="+payableAmt+" payreceived="+payreceived);
				if(payreceived>payableAmt)
				{
					form.showDialogMessage("Amount receive should not be greater than Payable amount");
					form.hideWaitSymbol();
					return;
				}
				
				if(payableAmt==payreceived)
				{
					changeStatusOnSubmit();
				}
				
				if(payreceived<payableAmt&&form.getDbamtreceived().getValue()!=0)
				{
					createNewAndChangeStatus();
				}
				
				
				/**
				 * Date 1 march 2017 added by vijay 
				 * for cash payment enrty into petty cash  if process configuration is active.
				 * requirement for pestcure 
				 */
				if(form.getDbamtreceived().getValue()!=0){
					if(form.cbaddintoPettyCash.getValue() && form.olbpettyCashName.getSelectedIndex()!=0){
						AddPaymentEntryIntoPettyCash(payreceived);
					}
				}
				/**
				 * 
				 */
			}
			
			private void changeStatusOnSubmit()
			{
				model.setStatus(CustomerPayment.PAYMENTCLOSED);
//				form.showWaitSymbol();//Vijay
				Timer timer=new Timer() 
	        	 {
	 				@Override
	 				public void run() {
	 					async.save(model,new AsyncCallback<ReturnFromServer>() {
	 						
							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected Error occured !");
								form.hideWaitSymbol();//Vijay
							}
		
							@Override
							public void onSuccess(ReturnFromServer result) {
								form.setAppHeaderBarAsPerStatus();
								form.setToViewState();
								form.tbstatus.setText(CustomerPayment.PAYMENTCLOSED);
								checkSendSmsStatus();
								form.hideWaitSymbol();
								form.showDialogMessage("Payment Submitted..!",GWTCAlert.OPTION_ROUNDED_BLUE,GWTCAlert.OPTION_ANIMATION);
							}
	 					});
	 						
	 					}
		 			};
		            timer.schedule(3000);
			}
			
			
			private void createNewAndChangeStatus()
			{
				model.setStatus(CustomerPayment.PAYMENTCLOSED);
//				model.setPaymentAmt(form.getIbamtreceived().getValue());
//				form.showWaitSymbol();//vijay
				Timer timer=new Timer() 
	        	 {
	 				@Override
	 				public void run() {
	 					async.save(model,new AsyncCallback<ReturnFromServer>() {
	 						
							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected Error occured !");
								form.hideWaitSymbol();
							}
		
							@Override
							public void onSuccess(ReturnFromServer result) {
								form.setAppHeaderBarAsPerStatus();
								form.setToViewState();
								form.tbstatus.setText(CustomerPayment.PAYMENTCLOSED);
								form.showDialogMessage("Payment Submitted..!",GWTCAlert.OPTION_ROUNDED_BLUE,GWTCAlert.OPTION_ANIMATION);
								form.hideWaitSymbol();//Vijay
								CustomerPayment paydtls=new CustomerPayment();
								
								PersonInfo pinfo=new PersonInfo();
								pinfo.setCount(Integer.parseInt(form.getTbpersoncount().getValue()));
								pinfo.setFullName(form.getTbpersonname().getValue());
								pinfo.setCellNumber(Long.parseLong(form.getTbpersoncell().getValue()));
								pinfo.setPocName(form.getTbpocName().getValue());
								if(pinfo!=null){
									paydtls.setPersonInfo(pinfo);
								}
								if(form.getIbcontractid().getValue()!=null)
									paydtls.setContractCount(Integer.parseInt(form.getIbcontractid().getValue()));
								if(form.getDosalesamount().getValue()!=null)
									paydtls.setTotalSalesAmount(form.getDosalesamount().getValue());
								List<BillingDocumentDetails> billdoclis=form.getBillingDoc().getDataprovider().getList();
								ArrayList<BillingDocumentDetails> billdocarr=new ArrayList<BillingDocumentDetails>();
								billdocarr.addAll(billdoclis);
								if(billdocarr.size()!=0)
									paydtls.setArrayBillingDocument(billdocarr);
								if(form.getDototalbillamount().getValue()!=null)
									paydtls.setTotalBillingAmount(form.getDototalbillamount().getValue());
								if(form.getIbinvoiceid().getValue()!=null)
									paydtls.setInvoiceCount(Integer.parseInt(form.getIbinvoiceid().getValue()));
								if(form.getDbinvoicedate().getValue()!=null)
									paydtls.setInvoiceDate(form.getDbinvoicedate().getValue());
								if(form.getTbinvoicetype().getValue()!=null)
									paydtls.setInvoiceType(form.getTbinvoicetype().getValue());
								if(form.getOlbPaymentMethod().getValue()!=null)
									paydtls.setPaymentMethod(form.getOlbPaymentMethod().getValue());
								if(model.getContractStartDate()!=null){
									paydtls.setContractStartDate(model.getContractStartDate());
								}
								if(model.getContractEndDate()!=null){
									paydtls.setContractEndDate(model.getContractEndDate());
								}
								
								if(form.getDoinvoiceamt().getValue()!=null){
									paydtls.setInvoiceAmount(form.getDoinvoiceamt().getValue());
									
								}
								
								if(form.getOlbEmployee().getValue()!=null){
								paydtls.setEmployee(form.getOlbEmployee().getValue());//Ashiwni Patil Date:16-08-2022
								}
								double payableAmt=form.getIbpaymentAmt().getValue();
								double payreceived=form.getDbamtreceived().getValue();
								int payAmt=(int) (payableAmt-payreceived);
								paydtls.setPaymentAmt(payAmt);
								
								//Ashwini Patil
								double payableAmtInDecimal=form.getDbpaymentAmt().getValue();
								double payAmtInDecimal=payableAmtInDecimal-payreceived;
								paydtls.setPaymentAmtInDecimal(payAmtInDecimal);
								
								if(form.getDbpaymentdate().getValue()!=null)
									paydtls.setPaymentDate(form.getDbpaymentdate().getValue());
								
								paydtls.setTypeOfOrder(model.getTypeOfOrder());
								
								paydtls.setOrderCreationDate(model.getOrderCreationDate());
								if(model.getOrderCformStatus()!=null){
									paydtls.setOrderCformStatus(model.getOrderCformStatus());
								}
								if(model.getOrderCformPercent()!=-1){
									paydtls.setOrderCformPercent(model.getOrderCformPercent());
								}
								paydtls.setOrderCreationDate(model.getOrderCreationDate());
								
								paydtls.setAccountType(model.getAccountType());
								
								paydtls.setStatus(CustomerPayment.CREATED);
								
								///   rohan added code for new payment should search with branch level restriction
								//    Date : 8/2/2017 
								paydtls.setBranch(model.getBranch());
								
								/**
								 * added by vijay on Date 3 march 2017
								 */
								paydtls.setAddintoPettyCash(model.isAddintoPettyCash());
								if(model.getPettyCashName()!=null)
								paydtls.setPettyCashName(model.getPettyCashName());
								
								/**
								 * ends here
								 */
								
								/**
								 * Date:25-11-2017 BY ANIL
								 * setting number range to newly generated payment document
								 */
								if(model.getNumberRange()!=null){
									paydtls.setNumberRange(model.getNumberRange());
								}
								/**
								 * End
								 */
								if(model.getRefNumber()!=null){
									paydtls.setRefNumber(model.getRefNumber());
								}
								
								if(model.getInvoiceConfigType()!=null){
									paydtls.setInvoiceConfigType(model.getInvoiceConfigType());
								}
								
								/** date 21/02/2018 added by komal for actual revenue and tax **/
								if(form.getIbpaymentAmt().isVisible()) {//Ashwini Patil
									
								if(payreceived < form.getIbpaymentAmt().getValue()){
									double tax = 0;
									if(form.getDbBalanceTax().getValue() != null){
										tax = form.getDbBalanceTax().getValue();
									}
									if(payreceived < tax){
										paydtls.setBalanceRevenue(Math.round(form.getDbBalanceRevenue().getValue() - model.getReceivedRevenue()));
										paydtls.setBalanceTax(Math.round(tax - model.getReceivedTax()));
									}
									if(payreceived == tax){
										paydtls.setBalanceRevenue(Math.round(model.getBalanceRevenue()));
										paydtls.setBalanceTax(0);
									}
									if(payreceived > tax){
										paydtls.setBalanceRevenue(Math.round(model.getBalanceRevenue()-model.getReceivedRevenue()));
										paydtls.setBalanceTax(0);
									}
									
									/*** Date 07-08-2020 Bug :- partial payment tax amount not showing and TDS set default as 0 **/
									paydtls.setTdsPercentage(0+"");
									double balanceTax = (paydtls.getPaymentAmt() - paydtls.getBalanceRevenue());
									paydtls.setBalanceTax(balanceTax);
								}
								}else {
								
									if(payreceived < form.getDbpaymentAmt().getValue()){
									double tax = 0;
									if(form.getDbBalanceTax().getValue() != null){
										tax = form.getDbBalanceTax().getValue();
									}
									if(payreceived < tax){
										paydtls.setBalanceRevenue(form.getDbBalanceRevenue().getValue() - model.getReceivedRevenue());
										paydtls.setBalanceTax(tax - model.getReceivedTax());
									}
									if(payreceived == tax){
										paydtls.setBalanceRevenue(model.getBalanceRevenue());
										paydtls.setBalanceTax(0);
									}
									if(payreceived > tax){
										paydtls.setBalanceRevenue(model.getBalanceRevenue()-model.getReceivedRevenue());
										paydtls.setBalanceTax(0);
									}
									}
									
									/*** Date 07-08-2020 Bug :- partial payment tax amount not showing and TDS set default as 0 **/
									paydtls.setTdsPercentage(0+"");
									double balanceTax = (paydtls.getPaymentAmtInDecimal() - paydtls.getBalanceRevenue());
									paydtls.setBalanceTax(balanceTax);
									
								}
								
								
								
								async.save(paydtls,new AsyncCallback<ReturnFromServer>() { 

									@Override
									public void onFailure(Throwable caught) {
										form.showDialogMessage("An Unexpected Error occured !");
									}
									@Override
									public void onSuccess(ReturnFromServer result) {
										
										checkSendSmsStatus();
									}
								});
							}
	 					});
	 						form.hideWaitSymbol();
	 					}
		 			};
		            timer.schedule(3000);
			}
			
			
			private void changeStatusOnCancellationInCreatedState() 
			{
				model.setStatus(CustomerPayment.CANCELLED);
				/**
				 * Updated By:Viraj
				 * Date: 15-06-2019
				 * Description: To set remark on cancellation of invoice
				 */
				model.setComment("Payment Id ="+model.getCount()+" "+"Payment Status ="+model.getStatus()+"\n"
						+"has been cancelled by "+LoginPresenter.loggedInUser.trim()
						+" with remark"+"\n"+"Remark ="+popup.getRemark().getValue());
				/** Ends **/
				form.showWaitSymbol();
				Timer timer=new Timer() 
	        	 {
	 				@Override
	 				public void run() {
	 					async.save(model,new AsyncCallback<ReturnFromServer>() {
	 						
							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected Error occured !");
							}
		
							@Override
							public void onSuccess(ReturnFromServer result) {
								form.setAppHeaderBarAsPerStatus();
								form.setToViewState();
								form.tbstatus.setText(CustomerPayment.CANCELLED);
								form.taComment.setText(model.getComment());			//Updated By: Viraj To set remark on cancellation of invoice
							
								createCancelledEntryInAccountingInterface(model);
							}

							
	 					});
	 						form.hideWaitSymbol();
	 					}
		 			};
		            timer.schedule(3000);
			}
			
			
			private void changeStatusOnCancellation() 
			{
				
//				MyQuerry querry = new MyQuerry();
//			  	Company c = new Company();
//			  	Vector<Filter> filtervec=new Vector<Filter>();
//			  	Filter filter = null;
//			  	filter = new Filter();
//			  	filter.setQuerryString("companyId");
//				filter.setLongValue(c.getCompanyId());
//				filtervec.add(filter);
//				filter = new Filter();
//				filter.setQuerryString("documentID");
//				filter.setIntValue(model.getCount());
//				filtervec.add(filter);
//				
//				filter = new Filter();
//				filter.setQuerryString("documentType");
//				filter.setStringValue("Customer Payment");
//				filtervec.add(filter);
//				
//				querry.setFilters(filtervec);
//				querry.setQuerryObject(new AccountingInterface());
//				async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
//		  			
//		  			@Override
//		  			public void onFailure(Throwable caught) {
//		  			}
//
//		  			@Override
//					public void onSuccess(ArrayList<SuperModel> result) {
//		  				
//		  				for(SuperModel smodel:result)
//							{
//		  					AccountingInterface accntInt = (AccountingInterface)smodel;
//		  					
//		  					if(accntInt.getStatus().equalsIgnoreCase(AccountingInterface.TALLYSYNCED)){
//		  						
//		  						accntInt.setStatus(AccountingInterface.DELETEFROMTALLY);
//			  					accntInt.setRemark("Payment Id ="+model.getCount()+" "+"Payment Status ="+model.getStatus()+"\n"
//									+"has been cancelled by "+LoginPresenter.loggedInUser.trim()
//									+" with remark"+"\n"+"Remark ="+popup.getRemark().getValue());
//									
//
//		  					}
//		  					else{
//
//		  						accntInt.setStatus(AccountingInterface.CANCELLED);
//			  					accntInt.setRemark("Invoice Id ="+model.getCount()+" "+"Invoice Status ="+model.getStatus()+"\n"
//									+"has been cancelled by "+LoginPresenter.loggedInUser.trim()
//									+" with remark"+"\n"+"Remark ="+popup.getRemark().getValue());
//									
//
//		  					}
//		  							
//		  					async.save(accntInt,new AsyncCallback<ReturnFromServer>() {
//									@Override
//									public void onFailure(Throwable caught) {
//										form.showDialogMessage("An Unexpected Error occured !");
////										
//									}
//									@Override
//									public void onSuccess(ReturnFromServer result) {
//									}
//								});
//							}
//		  				
//		  			}
//				
//				});

				
				
				model.setStatus(CustomerPayment.CREATED);
				/**
				 * Date 14-07-2018 By Vijay
				 * Des :- when payment doc cancelled then i have updated gross amount and tax amount for proper
				 * payment calculations 
				 */
				model.setPaymentAmt((int)model.getPaymentReceived());
				model.setPaymentAmtInDecimal(model.getPaymentReceived());//15-12-2023 as on cancellation of payment this value was not getting set and for noroundoff clients like innovative would face issue
				if(model.getRefNumber()!=null)
					model.setRefNumber(model.getRefNumber());//Ashwini Patil Date:2-1-2024 Orion requirement
				model.setBalanceRevenue(model.getReceivedRevenue());
				model.setBalanceTax(model.getReceivedTax());
				
				final int paymentReceived =(int) model.getPaymentReceived();
				final double paymentReceivedInDecimal =model.getPaymentReceived();
				final double balanceRevenue = model.getReceivedRevenue();
				final double balanceTax = model.getReceivedTax();
				model.setPaymentReceived(0);
				model.setReceivedRevenue(0);
				model.setReceivedTax(0);
				/**
				 * ends here
				 */
				form.showWaitSymbol();
				Timer timer=new Timer() 
	        	 {
	 				@Override
	 				public void run() {
	 					async.save(model,new AsyncCallback<ReturnFromServer>() {
	 						
							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected Error occured !");
							}
		
							@Override
							public void onSuccess(ReturnFromServer result) {
								form.setAppHeaderBarAsPerStatus();
								form.setToViewState();
//								form.ibpaymentAmt.setValue(model.getPaymentReceived());
								form.tbstatus.setText(CustomerPayment.CREATED);
								
								/**
								 * Date 14-07-2018 By Vijay
								 * Des :- when payment doc cancelled then i have updated gross amount and tax amount for proper
								 * payment calculations 
								 */
									form.ibpaymentAmt.setValue(paymentReceived);
									form.dbpaymentAmt.setValue(paymentReceivedInDecimal);
									form.dbBalanceRevenue.setValue(balanceRevenue);
									form.dbBalanceTax.setValue(balanceTax);
									form.dbamtreceived.setValue(null);
									form.dbReceivedRevenue.setValue(null);
									form.dbReceivedTax.setValue(null);
								/**
								 * ends here
								 */
									
									commonservice.deleteEntryFromAccountingInterface(model, new AsyncCallback<String>() {
										
										@Override
										public void onSuccess(String result) {
											// TODO Auto-generated method stub
											Console.log("delete entry in accounting interface"+result);
										}
										
										@Override
										public void onFailure(Throwable caught) {
											// TODO Auto-generated method stub
											
										}
									});
								
							}
	 					});
	 						form.hideWaitSymbol();
	 					}
		 			};
		            timer.schedule(3000);
			}
				
	/***********************************************Sms Method***************************************/
			
			
			private void checkSendSmsStatus() {
//				Vector<Filter> filtrvec = new Vector<Filter>();
//				Filter filtr = null;
//				
//				filtr = new Filter();
//				filtr.setLongValue(model.getCompanyId());
//				filtr.setQuerryString("companyId");
//				filtrvec.add(filtr);
//				
//				filtr = new Filter();
//				filtr.setBooleanvalue(true);
//				filtr.setQuerryString("status");
//				filtrvec.add(filtr);
//				
//				MyQuerry querry = new MyQuerry();
//				querry.setFilters(filtrvec);
//				querry.setQuerryObject(new SmsConfiguration());
//				
//				async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
//					
//					@Override
//					public void onSuccess(ArrayList<SuperModel> result) {
//
//						if(result.size()!=0){
//							for(SuperModel model:result){
//								SmsConfiguration smsconfig = (SmsConfiguration) model;
//								boolean smsStatus = smsconfig.getStatus();
//								String accountsid = smsconfig.getAccountSID();
//								String authotoken = smsconfig.getAuthToken();
//								String fromnumber = smsconfig.getPassword();
//								if(smsStatus==true){
////								 sendSMS(accountsid, authotoken, fromnumber);
									/**
									 * @author Vijay Date :- 09-08-2021
									 * Des :- added below method to check customer DND status if customer DND status is Active 
									 * then SMS will not send to customer
									 */
								 validateCustomerDNDStatus();
								
//								}
//							}
//						}
//					}
//					@Override
//					public void onFailure(Throwable caught) {
//						
//					}
//				});
			}		
			
			
			

			private void sendSMS() {
				
				if(model.getAccountType().equals(AppConstants.BILLINGACCOUNTTYPEAR)){
					
				Vector<Filter> filtrvec = new Vector<Filter>();
				Filter filtr = null;
				
				filtr = new Filter();
				filtr.setLongValue(model.getCompanyId());
				filtr.setQuerryString("companyId");
				filtrvec.add(filtr);
				
				filtr = new Filter();
				filtr.setStringValue("Payment Received");
				filtr.setQuerryString("event");
				filtrvec.add(filtr);
				
				filtr = new Filter();
				filtr.setBooleanvalue(true);
				filtr.setQuerryString("status");
				filtrvec.add(filtr);
				
				MyQuerry myquerry = new MyQuerry();
				myquerry.setFilters(filtrvec);
				myquerry.setQuerryObject(new SmsTemplate());
				
				async.getSearchResult(myquerry, new AsyncCallback<ArrayList<SuperModel>>() {
					
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						
						if(result.size()!=0){
							for(SuperModel model: result){
								SmsTemplate sms = (SmsTemplate) model;
								
								String templateMsgWithBraces = new String(sms.getMessage());
								String amtRecieved = form.getDbamtreceived().getValue()+"";
								String contarctNo = form.ibcontractid.getValue();
								
								String invoiceno =form.ibinvoiceid.getValue();
								//String invoiceno = form.getIbinvoiceid().getValue()+"";
								Date invcdate = form.getDbinvoicedate().getValue();
								String frminvoiceDate = AppUtility.parseDate(invcdate);	
								
								Date paymentdate = form.getDbpaymentdate().getValue();
								String frmpaymentDate = AppUtility.parseDate(paymentdate);	
								String receivedDate=AppUtility.parseDate(form.dbtransferDate.getValue());	
								
								cellNo = Long.parseLong(form.getTbpersoncell().getValue());

								
								Date todayDate = new Date();
								String strDate = AppUtility.parseDate(todayDate);
								
								String amountRecieved = templateMsgWithBraces.replace("{AmountRecieved}", amtRecieved);
								amountRecieved = amountRecieved.replace("{ReceivedDate}", receivedDate);//Ashwini Patil Date:1-03-2024
								
								//Date 22-08-2017 if condn added by vijay for bhash sms eco friendly  and else for normal standrd 
								if(LoginPresenter.bhashSMSFlag){
									String paymentDate= amountRecieved.replace("{Date}",strDate);
									companyName=Slick_Erp.businessUnitName;
									actualmsg = paymentDate.replace("{companyName}", companyName);

								
								}else {
//									String contractNumber = amountRecieved.replace("{OrderId}", contarctNo);
//									String paymentDate= contractNumber.replace("{Date}",strDate);
//									companyName=Slick_Erp.businessUnitName;
//									actualmsg = paymentDate.replace("{companyName}", companyName);
									/**
									 *   Added By Priyanka Date : 04-04-2021
									 *   Des - change in standard sms template requirement by Nitin Sir and Ashwini.
									 */
									String paymentDate= amountRecieved.replace("{Date}",frmpaymentDate);
									companyName = Slick_Erp.businessUnitName;
									String companyname =paymentDate.replace("{CompanyName}", companyName);
									String invoiceNo = companyname.replace("{InvoiceNumber}", invoiceno); // //
									actualmsg = invoiceNo.replace("{InvoiceDate}", frminvoiceDate);
								}
								
								/**
								 *@author Anil @since 04-08-2021
								 *Need add order id in template
								 *Raised by Rahul Tiwari for Aknamous
								 */
								actualmsg=actualmsg.replace("{OrderId}", contarctNo);
								System.out.println("Actual Msg:"+actualmsg);

								
//								smsserviceAsync.sendSmsToClient(actualmsg, cellNo, accountsid, authotoken, fromnumber,model.getCompanyId(),  new AsyncCallback<Integer>(){
//
//									@Override
//									public void onFailure(Throwable caught) {
//									}
//
//									@Override
//									public void onSuccess(Integer result) {
//										form.hideWaitSymbol();
//										if(result==1)
//										{
//											form.showDialogMessage("SMS Sent Successfully!");
//											savesmshistory();
//										}
//									}} );
								
								smsserviceAsync.sendMessage(AppConstants.ACCOUNTMODULE, AppConstants.PAYMENTDETAILS, sms.getEvent(), model.getCompanyId(), actualmsg, cellNo,false, new AsyncCallback<String>() {
									
									@Override
									public void onSuccess(String result) {
										// TODO Auto-generated method stub
										form.hideWaitSymbol();
//										form.showDialogMessage("Message Sent Successfully!");

									}
									
									@Override
									public void onFailure(Throwable caught) {
										// TODO Auto-generated method stub
										form.hideWaitSymbol();
									}
								});

							}
						}
					}
					@Override
					public void onFailure(Throwable caught) {
						System.out.println("Unable to send SMS");
					}
				});
				
				}////end of AR
				
				
				if(model.getAccountType().equals(AppConstants.BILLINGACCOUNTTYPEAP)){
				
				Vector<Filter> filtrvec = new Vector<Filter>();
				Filter filtr = null;
				filtr = new Filter();
				filtr.setLongValue(model.getCompanyId());
				filtr.setQuerryString("companyId");
				filtrvec.add(filtr);
				
				filtr = new Filter();
				filtr.setStringValue("Amount Payable");
				filtr.setQuerryString("event");
				filtrvec.add(filtr);
				
				filtr = new Filter();
				filtr.setBooleanvalue(true);
				filtr.setQuerryString("status");
				filtrvec.add(filtr);
				
				MyQuerry querry = new MyQuerry();
				querry.setFilters(filtrvec);
				querry.setQuerryObject(new SmsTemplate());
				
				async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
					
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						if(result.size()!=0){
							for(SuperModel model:result){
								SmsTemplate sms = (SmsTemplate) model;
								
								String templateMsgwithbraces = sms.getMessage();
								String amtPayable = form.getDbamtreceived().getValue()+"";
								String invoiceno = form.ibinvoiceid.getValue()+"";
								Date invcdate = form.dbinvoicedate.getValue();
								String frminvoiceDate = AppUtility.parseDate(invcdate);
								String paymentmethod = form.olbPaymentMethod.getValue();
								
								cellNo = Long.parseLong(form.tbpersoncell.getValue());
								
								String amountPayable = templateMsgwithbraces.replace("{PayableAmt}",amtPayable);
								String invoiceNo = amountPayable.replace("{InvoiceNo}", invoiceno);
								String invoiceDate = invoiceNo.replace("{InvoiceDate}", frminvoiceDate);
								 actualmsg = invoiceDate.replace("{PaymentMethod}", paymentmethod);
								companyName = Slick_Erp.businessUnitName;
								actualmsg = actualmsg.replace("{CompanyName}", companyName);
								
//								smsserviceAsync.sendSmsToClient(actualmsg+"\n"+companyName, cellNo, accountsid, authotoken, fromnumber, model.getCompanyId(), new AsyncCallback<Integer>() {
//									
//									@Override
//									public void onSuccess(Integer result) {
//										form.hideWaitSymbol();
//										if(result==1)
//										{
//											form.showDialogMessage("SMS Sent Successfully!");
//											savesmshistory();
//										}
//									}
//									
//									@Override
//									public void onFailure(Throwable caught) {
//										
//									}
//								});
								
								smsserviceAsync.sendMessage(AppConstants.ACCOUNTMODULE, AppConstants.VENDORPAYMENTDETAILS, sms.getEvent(), model.getCompanyId(), actualmsg, cellNo,false, new AsyncCallback<String>() {
									
									@Override
									public void onSuccess(String result) {
										// TODO Auto-generated method stub
										form.hideWaitSymbol();
//										form.showDialogMessage("Message Sent Successfully!");

									}
									
									@Override
									public void onFailure(Throwable caught) {
										// TODO Auto-generated method stub
										form.hideWaitSymbol();
									}
								});

							}
						}
					}
					
					@Override
					public void onFailure(Throwable caught) {
					}
				});
				}//end of AP if
			}
			
			private void savesmshistory() {
				SmsHistory smshistory = new SmsHistory();
				smshistory.setDocumentId(model.getCount());
				smshistory.setDocumentType("Payment");
				smshistory.setName(form.getTbpersonname().getValue());
				smshistory.setCellNo(cellNo);
				smshistory.setSmsText(actualmsg);
				smshistory.setUserId(LoginPresenter.loggedInUser);
				
				async.save(smshistory, new AsyncCallback<ReturnFromServer>() {
					
					@Override
					public void onSuccess(ReturnFromServer result) {
						System.out.println("Data Saved Successfully");
					}
					
					@Override
					public void onFailure(Throwable caught) {
						System.out.println("Data Save Unsuccessfull");
					}
				});
				
			}
			
			//*****************rohan added this *************************
			
			@Override
			public void onChange(ChangeEvent event) {
//				if(event.getSource()== form.getOlbtdsValue()){
//					System.out.println("in side on change method ");
//					getPayableValue();
//				}
				
				
				if(event.getSource() == form.olbPaymentMethod){
					if(form.olbPaymentMethod.getSelectedIndex()!=0){
						String selectedPaymentMethodValue = form.olbPaymentMethod.getValue(form.olbPaymentMethod.getSelectedIndex());
						if(selectedPaymentMethodValue.equalsIgnoreCase("Cash")){
							reactonPaymentMethod();
						}
						else{
							form.cbaddintoPettyCash.setValue(false);
							form.olbpettyCashName.setSelectedIndex(0);
							processConfigAddtoPettyCashEntry = false;
						}
					}else{
						form.cbaddintoPettyCash.setValue(false);
						form.olbpettyCashName.setSelectedIndex(0);
						processConfigAddtoPettyCashEntry=false;
					}
					
				}
				
			}
			
			
			
			private void reactonPaymentMethod() {

				boolean processConfigvalue = AppUtility.checkForProcessConfigurartionIsActiveOrNot("CustomerPayment","PaymentAddIntoPettyCash");
				
				if(processConfigvalue){
					processConfigAddtoPettyCashEntry = processConfigvalue;
					form.cbaddintoPettyCash.setValue(true);
					int pettycahnamesize = form.olbpettyCashName.getItemCount();
					System.out.println("Size =="+pettycahnamesize);
					if(pettycahnamesize==2){
						form.olbpettyCashName.setSelectedIndex(1);	
					}
				}
				
			}

			@Override
			public void onValueChange(ValueChangeEvent<Double> event) {
				System.out.println("in side value change ");
				totalNoOfTaxes =0;
				if(event.getSource().equals(form.getDbtdsValue())){
					getTotalNoOfTaxesFromInvoices(false,false);
				}
				if(event.getSource().equals(form.getTbtdsPerValue())){
					getTotalNoOfTaxesFromInvoices(true,false);
				}
				if(event.getSource().equals(form.getIbNetPay())){
					getTotalNoOfTaxesFromInvoices(false,true);
				}
				
				
				
				
//				getTotalNoOfTaxesFromInvoices();
//				if(event.getSource()== form.getIbNetPay()){
//					if(form.getTbtdsPerValue().getValue()!=null&&form.getCbtdsApplicable().getValue()==true){
//						getTotalNoOfTaxesFromInvoices();
////						form.getDbtdsValue().setEnabled(false);
////						Base Amt = (Net Receivable/((Tax % +1)-(TDS(%)/100))   -----> No Round-off
////
////					    Tax Amt = Base Amt x Tax(%)  ----> No Round-off
////
////						Gross Amt = Base Amt + Tax Amt   ----> Round-off
//						
//						double amountReceived=0;
//						double netReceivedAmt=0;
//						double tax=0;
//						double tdsPerValue=0;
//						tdsPerValue=form.getTbtdsPerValue().getValue();
//						
//						netReceivedAmt=(form.getIbNetPay().getValue());
//						tax=(totalNoOfTaxes/100)+1;
//						
//						amountReceived=(netReceivedAmt/tax)+(tdsPerValue/100);
//						form.getDbReceivedRevenue().setValue(amountReceived);	
//						
//						
//						
//						double adjtax=0;
//						double tax1=0;
//						tax1=form.getDbBalanceTax().getValue();
//						adjtax= (amountReceived*tax1);
//						
//						form.getDbReceivedTax().setValue(adjtax);
//						double grossAmt1=0;
//						grossAmt1=amountReceived+tax1;
//						form.getDbamtreceived().setValue(grossAmt1);
//						
//						
//					
//					}
//					
//					
//					
//				}
				
				if(event.getSource()== form.getDbamtreceived()){
					System.out.println("in side conditions ");
					if(form.getDbamtreceived().getValue()==0)
					{
						
						System.out.println("inside if condition");
						form.getIbNetPay().setValue(0.0);
						form.getDbtdsValue().setValue(0.0);
						form.dbtdsValue.setEnabled(false);
						form.getTbtdsPerValue().setValue(0.0);
						form.tbtdsPerValue.setEnabled(false);
						form.getCbtdsApplicable().setValue(false);
					}
					else
					{
						
							form.getIbNetPay().setValue(form.getDbamtreceived().getValue());
						
						
//							Amount Received/Paid = getDbamtreceived
//							Gross Amount = getIbpaymentAmt()
						
						/** date 21.02.2018 added by komal for revenue and tax calculation **/
						double amountReceived = form.getDbamtreceived().getValue();
						
						if(form.getIbpaymentAmt().isVisible())
						{
							
					
						if(amountReceived == form.getIbpaymentAmt().getValue()){
							form.getDbReceivedRevenue().setValue(form.getDbBalanceRevenue().getValue());
							form.getDbReceivedTax().setValue(form.getDbBalanceTax().getValue());
						}
						if(amountReceived < form.getIbpaymentAmt().getValue()){
							double tax = 0;
							if(form.getDbBalanceTax().getValue() != null){
								tax = form.getDbBalanceTax().getValue();
							}
							if(amountReceived < tax){
								form.getDbReceivedRevenue().setValue(null);
								form.getDbReceivedTax().setValue(amountReceived);
							}
							if(amountReceived == tax){
								form.getDbReceivedRevenue().setValue(null);
								form.getDbReceivedTax().setValue(tax);
							}
							if(amountReceived > tax){
								form.getDbReceivedRevenue().setValue(amountReceived - tax);
								form.getDbReceivedTax().setValue(tax);
							}
						}
						}else {
							
							if(amountReceived == form.getDbpaymentAmt().getValue()){
								form.getDbReceivedRevenue().setValue(form.getDbBalanceRevenue().getValue());
								form.getDbReceivedTax().setValue(form.getDbBalanceTax().getValue());
							}
							if(amountReceived < form.getDbpaymentAmt().getValue()){
								double tax = 0;
								if(form.getDbBalanceTax().getValue() != null){
									tax = form.getDbBalanceTax().getValue();
								}
								if(amountReceived < tax){
									form.getDbReceivedRevenue().setValue(null);
									form.getDbReceivedTax().setValue(amountReceived);
								}
								if(amountReceived == tax){
									form.getDbReceivedRevenue().setValue(null);
									form.getDbReceivedTax().setValue(tax);
								}
								if(amountReceived > tax){
									form.getDbReceivedRevenue().setValue(amountReceived - tax);
									form.getDbReceivedTax().setValue(tax);
								}
							}
						}
					}
				}
				/**Date 20-5-2020 by Amol Net Receivable Amount should also calculate on TDS Percentage raised by Rahul Tiwari**/
//				if(event.getSource()==form.getTbtdsPerValue()){
//					Console.log("Inside my tds percent");
//					if(form.getTbtdsPerValue()!=null&&form.getTbtdsPerValue().getValue()!=0){
//						
//						form.getDbtdsValue().setEnabled(false);
//					
////						Base Amt = (Net Receivable/((Tax % +1)-(TDS(%)/100))   -----> No Round-off
////
////					    Tax Amt = Base Amt x Tax(%)  ----> No Round-off
////
////						Gross Amt = Base Amt + Tax Amt   ----> Round-off
//						
//						double amountReceived=0;
//						double netReceivedAmt=0;
//						double tax=0;
//						double tdsPerValue=0;
//						
//						Console.log("totalno of invoice taxes "+totalNoOfTaxes);
//						
//						tdsPerValue=form.getTbtdsPerValue().getValue();
//						
//						netReceivedAmt=form.getIbNetPay().getValue();
//						tax=(totalNoOfTaxes/100);
//						
//						Console.log("total taxes "+tax);
//						Console.log("net recieved amt "+netReceivedAmt);
//						Console.log("tds percentage "+tdsPerValue);
//						
//						
//						
//						amountReceived=(netReceivedAmt/(tax+1))+(tdsPerValue/100);
//						Console.log("Adjusted base Amount "+amountReceived);
//						form.getDbReceivedRevenue().setValue(amountReceived);	
//						
//						
//						
//						double adjtax=0;
//						double tax1=0;
//						tax1=(totalNoOfTaxes/100);
//						
//						adjtax= (amountReceived*tax1);
//						Console.log(" Adjusted Tax Amount "+adjtax);
//						form.getDbReceivedTax().setValue(adjtax);
//						
//						double grossAmt1=0;
//						grossAmt1=amountReceived+adjtax;
//						Console.log(" Amount Received/Paid "+grossAmt1);
//						form.getDbamtreceived().setValue(grossAmt1);
//						
//						
//					}
//				}
				
				if(event.getSource()== form.getDbtdsValue())
				{
					if(form.getDbtdsValue().getValue()!=null && form.getDbtdsValue().getValue()!=0)
					{
						
//						System.out.println("amt received ="+form.getDbamtreceived().getValue());
//						System.out.println("tds amt ="+form.getDbtdsValue().getValue());
////						getTotalNoOfTaxesFromInvoices();
//						form.getTbtdsPerValue().setEnabled(false);
////						Gross Amt = Net Receivable + TDS Amt
//	//
////						Base Amt = Gross Amt/(Tax% + 1)
//	//
////						Tax Amt = Gross Amt - Base Amt
//						double tax2=0;
//						double netReceivedAmt=0;
//						double tdsAmt=0;
//						double grossAmt=0;
//						double baseAmt=0;
//						netReceivedAmt=(form.getIbNetPay().getValue());
//						tdsAmt=form.getDbtdsValue().getValue();
//						grossAmt=netReceivedAmt+	tdsAmt;
//						
//						form.getDbamtreceived().setValue(grossAmt);
//						
//						tax2=(totalNoOfTaxes/100)+1;
//						baseAmt=(grossAmt/tax2);
//						form.getDbReceivedRevenue().setValue(baseAmt);
//						
//						form.getDbReceivedTax().setValue(grossAmt-baseAmt);
						
//						form.getIbNetPay().setValue(form.getDbamtreceived().getValue() - form.getDbtdsValue().getValue()+"");
//						double amountReceived = Double.parseDouble(form.getIbNetPay().getValue());
//						form.getDbBalanceRevenue().setValue(form.getDbBalanceRevenue().getValue() - form.getDbtdsValue().getValue());
//						if(amountReceived == form.getIbpaymentAmt().getValue()){
//							form.getDbReceivedRevenue().setValue(form.getDbBalanceRevenue().getValue());
//							form.getDbReceivedTax().setValue(form.getDbBalanceTax().getValue());
//						}
//						if(amountReceived < form.getIbpaymentAmt().getValue()){
//							double tax = 0;
//							if(form.getDbBalanceTax().getValue() != null){
//								tax = form.getDbBalanceTax().getValue();
//							}
//							if(amountReceived < tax){
//								form.getDbReceivedRevenue().setValue(null);
//								form.getDbReceivedTax().setValue(amountReceived);
//							}
//							if(amountReceived == tax){
//								form.getDbReceivedRevenue().setValue(null);
//								form.getDbReceivedTax().setValue(tax);
//							}
//							if(amountReceived > tax){
//								form.getDbReceivedRevenue().setValue(amountReceived - tax);
//								form.getDbReceivedTax().setValue(tax);
//							}
//						}
					}
					
					/**
					 * Date 12-02-2018 By vijay for TDS Issue if tds amt removed but netpayable showing the deducted amt
					 */
					if(form.getDbtdsValue().getValue()==null){
						form.getIbNetPay().setValue(form.getDbamtreceived().getValue());
					}
					/**
					 * ends here
					 */
				}
				
				
				
			}
			
			
	private void AddPaymentEntryIntoPettyCash(double payreceived) {
				

				
				/**
				 * Date 24-10-2018 By Vijay
				 * Des :- below old commented and added new code for petty cash Name
				 */
//				PettyCash pettybal=form.olbpettyCashName.getSelectedItem();
				String pettyCashName = model.getPettyCashName();
				
				//Get current date and time
				Date depositDate=new Date();
				DateTimeFormat dateFormat=DateTimeFormat.getFormat(PredefinedFormat.DATE_SHORT);
				String strDate=dateFormat.format(depositDate);
				Date saveDate=DateTimeFormat.getShortDateFormat().parse(strDate);
				String saveTime=DateTimeFormat.getShortTimeFormat().format(depositDate);
				String personpResponsible ="";
				if(model.getEmployee().equals("")){
					personpResponsible=null;
				}else{
					personpResponsible = model.getEmployee();
				}
				
				updateAsync.depositepaymentAmtIntoPettyCash(model.getCount(), model.getCompanyId(), personpResponsible, saveDate, saveTime, payreceived,pettyCashName, new AsyncCallback<Void>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void onSuccess(Void result) {
						// TODO Auto-generated method stub
						System.out.println("Successed");
					}
				});
				
				
			}
			
			
//			private void getPayableValue() {
////				if(!form.getBasePayable().getValue().equals(""))
////				{
////					System.out.println("in side on change method base value "+form.getBasePayable().getValue());
////					payableAmt = Integer.parseInt(form.getBasePayable().getValue());
////				}
//					if(form.getCbtdsApplicable().getValue().equals(true))
//					{
//						
//						if(form.getOlbtdsValue().getValue(form.getOlbtdsValue().getSelectedIndex())!=null && form.getOlbtdsValue().getSelectedIndex()!=0)
//						{
//							double TdsValue = (payableAmt * Double.parseDouble(form.getOlbtdsValue().getValue(form.getOlbtdsValue().getSelectedIndex())))/ 100 ;
//							System.out.println("");
//							form.getIbtdsValue().setValue(TdsValue+"");
//							form.getIbNetPay().setValue(payableAmt - TdsValue+(int)finalTaxAmt+"");
//						}
//					}
//					else
//					{
//						form.getIbNetPay().setValue(payableAmt+(int)finalTaxAmt+"");
//					}
//		
//				}
	
	
	/**
	 * Date 09-05-2018
	 * Developer : Vijay
	 * Des : for Communication Log
	 */
		@Override
		public void reactOnCommunicationLog() {
			
			form.showWaitSymbol();
			MyQuerry querry = AppUtility.communicationLogQuerry(model.getCount(), model.getCompanyId(),AppConstants.ACCOUNTMODULE,AppConstants.PAYMENTDETAILS);
			
			async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					
					ArrayList<InteractionType> list = new ArrayList<InteractionType>();
					
					for(SuperModel model : result){
						
						InteractionType interactionType = (InteractionType) model;
						list.add(interactionType);
						
					}
					/**
					 * Date : 08-11-2017 BY Komal
					 */
					Comparator<InteractionType> comp=new Comparator<InteractionType>() {
						@Override
						public int compare(InteractionType e1, InteractionType e2) {
							if (e1.getCount() == e2.getCount()) {
								return 0;
							}
							if (e1.getCount() > e2.getCount()) {
								return 1;
							} else {
								return -1;
							}
						}
					};
					Collections.sort(list,comp);

					form.hideWaitSymbol();
					CommunicationLogPopUp.getRemark().setValue("");
					CommunicationLogPopUp.getDueDate().setValue(null);
					CommunicationLogPopUp.getOblinteractionGroup().setSelectedIndex(0);
					CommunicationLogPopUp.getCommunicationLogTable().getDataprovider().setList(list);
					LoginPresenter.communicationLogPanel = new PopupPanel(true);
					LoginPresenter.communicationLogPanel.add(CommunicationLogPopUp);
					LoginPresenter.communicationLogPanel.show();
					LoginPresenter.communicationLogPanel.center();
				}
				
				@Override
				public void onFailure(Throwable caught) {
					form.hideWaitSymbol();
				}
			});
			
		}
		/**
		 * ends here
		 */
		/** date 25.6.2018 added by komal for vendor form**/
		public static PaymentDetailsForm initalize(boolean flag)
		{
			//AppMemory.getAppMemory().currentState=ScreeenState.NEW;
			PaymentDetailsForm form=new  PaymentDetailsForm();
			PaymentDetailsTableProxy gentable=new PaymentDetailsTableProxy();
			gentable.setView(form);
			gentable.applySelectionModle();
			PaymentDetailsSearchProxy.staticSuperTable=gentable;
			PaymentDetailsSearchProxy searchpopup=new PaymentDetailsSearchProxy(AppConstants.BILLINGACCOUNTTYPEAP);
			form.setSearchpopupscreen(searchpopup);

			PaymentDetailsPresenter presenter=new PaymentDetailsPresenter(form,new CustomerPayment());
			form.setToNewState();
			/**
			 * Date : 29-07-2017 BY ANIL
			 * This line was added to update the name of screen loaded from any other screen or by clicking on payment details on account model
			 */
			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Accounts/Vendor Payment Details",Screen.VENDORPAYMENTDETAILS);
			/**
			 * END
			 */
			AppMemory.getAppMemory().stickPnel(form);
			return form;
		}
		
		
		private void setUpdatedValueAdustedBaseAndTaxAmount(double grossAmt, double baseAmt, double tdsAmount) {

			double taxAmount = 0;
			if(form.getDbBalanceTax().getValue()!=null){
				taxAmount = form.getDbBalanceTax().getValue();
			}
			System.out.println("taxAmount =="+taxAmount);
			double adujustedTaxAmt = 0;
			
			if(grossAmt>taxAmount){
				adujustedTaxAmt = taxAmount;
				baseAmt = grossAmt - taxAmount;
			}
			else if(grossAmt<taxAmount){
				adujustedTaxAmt = grossAmt;
			}
			else{
				adujustedTaxAmt = taxAmount;

			}
			if(tdsAmount>0){
				baseAmt += tdsAmount;
			}
			
			form.getDbReceivedRevenue().setValue(baseAmt);
			form.getDbReceivedTax().setValue(adujustedTaxAmt);
		

			
		}
		

		private void setEmailPopUpData() {
			
			
			if(AppConstants.ORDERTYPEPURCHASE.equals(model.getTypeOfOrder().trim())) {
				
				form.showWaitSymbol();

				String branchName = model.getBranch();
				String fromEmailId = AppUtility.getFromEmailAddress(branchName,model.getEmployee());
				List<VendorDetails> vendorlist = new ArrayList<VendorDetails>();
				VendorDetails vendor  = new VendorDetails();
				vendor.setVendorId(model.getPersonInfo().getCount());
				vendor.setVendorName(model.getPersonInfo().getFullName());
				vendor.setStatus(true);
				vendorlist.add(vendor);
				
				List<ContactPersonIdentification> vendorDetailslist = AppUtility.getvenodrEmailId(vendorlist,true);
				System.out.println("vendorDetailslist "+vendorDetailslist);
				label : AppUtility.loadContactPersonlist(emailpopup.olbContactlistEmailId,"Vendor",-1,"","",vendorDetailslist);
				Screen screenName=(Screen) AppMemory.getAppMemory().currentScreen;
				Console.log("screenName "+screenName);
				AppUtility.makeLiveSmsTemplateConfig(emailpopup.oblTemplateName,AppConstants.ACCOUNTMODULE,screenName);
				emailpopup.taFromEmailId.setValue(fromEmailId);
				emailpopup.smodel = model;
				form.hideWaitSymbol();
				
			}
			else {
				form.showWaitSymbol();

				String customerEmail = "";

				String branchName = model.getBranch();
				String fromEmailId = AppUtility.getFromEmailAddress(branchName,model.getEmployee());
				customerEmail = AppUtility.getCustomerEmailid(model.getPersonInfo().getCount());
				
				String customerName = model.getPersonInfo().getFullName();
				if(!customerName.equals("") && !customerEmail.equals("")){
					label : AppUtility.loadContactPersonlist(emailpopup.olbContactlistEmailId,"Customer",model.getPersonInfo().getCount(),customerName,customerEmail,null);
					emailpopup.taToEmailId.setValue(customerEmail);
				}
				else{
			
					MyQuerry querry = AppUtility.getcustomerQuerry(model.getPersonInfo().getCount());
					async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
						
						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							// TODO Auto-generated method stub
							for(SuperModel smodel : result){
								Customer custEntity  = (Customer) smodel;
								System.out.println("customer Name = =="+custEntity.getFullname());
								System.out.println("Customer Email = == "+custEntity.getEmail());
								
								if(custEntity.getEmail()!=null){
									label : AppUtility.loadContactPersonlist(emailpopup.olbContactlistEmailId,"Customer",model.getPersonInfo().getCount(),custEntity.getFullname(),custEntity.getEmail(),null);
									emailpopup.taToEmailId.setValue(custEntity.getEmail());
								}
								else{
									label : AppUtility.loadContactPersonlist(emailpopup.olbContactlistEmailId,"Customer",model.getPersonInfo().getCount(),custEntity.getFullname(),custEntity.getEmail(),null);
								}
								break;
							}
						}
						
						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
						}
					});
				}
				
				Screen screenName=(Screen) AppMemory.getAppMemory().currentScreen;
				Console.log("screenName "+screenName);
			
				AppUtility.makeLiveSmsTemplateConfig(emailpopup.oblTemplateName,"Accounts",screenName);
				
				emailpopup.taFromEmailId.setValue(fromEmailId);
				emailpopup.smodel = model;
				form.hideWaitSymbol();
			}
			
			
		}
		
		protected void validateCustomerDNDStatus() {
			Vector<Filter> filtrvect = new Vector<Filter>();
			Filter filter = null;
			
			filter = new Filter();
			filter.setIntValue(model.getPersonInfo().getCount());
			filter.setQuerryString("count");
			filtrvect.add(filter);
			
			MyQuerry querry = new MyQuerry();
			querry.setFilters(filtrvect);
			querry.setQuerryObject(new Customer());
			
			async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					// TODO Auto-generated method stub
					for(SuperModel model : result){
						Customer customer = (Customer) model;
						if(!customer.isSmsDNDStatus()){
							 sendSMS();
						}
						Console.log("customer.isSmsDNDStatus() "+customer.isSmsDNDStatus());
						break;
					}
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					
				}
			});
			
		}
          private boolean checkBillSubType(String billType){
			
			switch(billType){
				case AppConstants.NATIONALHOLIDAY:
				case AppConstants.OVERTIME:
				case AppConstants.STAFFINGANDRENTAL:
				case AppConstants.OTHERSERVICES:
				case AppConstants.CONSUMABLES:
				case AppConstants.FIXEDCONSUMABLES :
				case AppConstants.EQUIPMENTRENTAL:
				case AppConstants.STAFFING:
				case AppConstants.CONSOLIDATEBILL:	
				case AppConstants.ARREARSBILL:	
				case AppConstants.PUBLICHOLIDAY:
					return true;
			}
			return false;		
			}

          
          
          @Override
  		public void reactOnSMS() {
  			smspopup.showPopUp();
  			loadSMSPopUpData();
  		}

  		private void loadSMSPopUpData() {
  			form.showWaitSymbol();
  			String customerCellNo = model.getPersonInfo().getCellNumber()+"";

  			String customerName = model.getPersonInfo().getFullName();
  			if(!customerName.equals("") && !customerCellNo.equals("")){
  				AppUtility.loadContactPersonlist(smspopup.olbContactlistEmailId,"Customer",model.getPersonInfo().getCount(),customerName,customerCellNo,null,true);
  			}
  			Screen screenName=(Screen) AppMemory.getAppMemory().currentScreen;
  			Console.log("screenName "+screenName);
  			AppUtility.makeLiveSmsTemplateConfig(smspopup.oblsmsTemplate,LoginPresenter.currentModule.trim(),screenName,true,AppConstants.SMS);
  			smspopup.getRbSMS().setValue(true);
  			smspopup.smodel = model;
  			form.hideWaitSymbol();
  		}
  		
  		private void createCancelledEntryInAccountingInterface(
				CustomerPayment model) {

  			Console.log("Payment status"+model.getStatus());
			commonservice.createSpecificDocumentAccountingInterface(model, new AsyncCallback<String>() {
				
				@Override
				public void onSuccess(String result) {
					// TODO Auto-generated method stub
					Console.log(result);

				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
				}
			});
		}
  		
  		
  		private void reactonupdateAccountingInterface() {
  		
  			form.showWaitSymbol();
  			commonservice.updateAccountingInterface(model, new AsyncCallback<String>() {
				
				@Override
				public void onSuccess(String result) {
					// TODO Auto-generated method stub
					Console.log("result =="+result);
					form.showDialogMessage(result);
					form.hideWaitSymbol();
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					form.hideWaitSymbol();
				}
			});
  		}
}
