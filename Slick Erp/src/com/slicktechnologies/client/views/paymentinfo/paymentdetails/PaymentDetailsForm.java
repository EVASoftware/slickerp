package com.slicktechnologies.client.views.paymentinfo.paymentdetails;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.UploadComposite;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.BillingDocumentTable;
import com.slicktechnologies.shared.PettyCash;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.salesprocess.BillingDocumentDetails;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;

public class PaymentDetailsForm extends FormScreen<CustomerPayment> implements ChangeHandler, ValueChangeHandler<Date> {
	
	TextBox ibinvoiceid;
	TextBox ibpaymentid;
	TextBox ibcontractid;
	TextBox tbpersoncount;
	TextBox tbpersonname;
	TextBox tbpersoncell;
	TextBox tbpocName,ibchequeNo;
	DateBox dbinvoicedate,dbpaymentdate;
	DoubleBox dosalesamount;
	DoubleBox dototalbillamount,doinvoiceamt,doinvoiceamtcopy;
	TextBox tbstatus;
	ObjectListBox<Employee> olbEmployee,olbApproverName;
	ObjectListBox<Config> olbPaymentMethod,olbPaymentTerms;
	TextBox tbbankAccNo,tbbankName,tbbankBranch;
	DateBox dbChequeDate,dbtransferDate;
	
	BillingDocumentTable billingDoc;
	TextBox tbinvoicetype,tbreferenceno;
	TextBox tbaccounttype;
	IntegerBox ibpaymentAmt;
	DoubleBox dbpaymentAmt;
	

	TextArea taComment;
	CheckBox cbCformStatus;
	
	String cformStatus="";
	boolean cformFlag=false;
	
	TextBox tbChequeIssuedBy;
	FormField fgroupingCustomerInformation;
	
	//  rohan commented this code and make it as Textbox
//	ObjectListBox<Config> olbtdsValue;
//	IntegerBox ibamtreceived;
//	TextBox basePayable;
	
	DoubleBox dbamtreceived;
	DoubleBox tbtdsPerValue;
	DoubleBox dbtdsValue;
	DoubleBox ibNetPay;
	CheckBox cbtdsApplicable;
	
	/**
	 * Date 14 Feb 2017
	 * by Vijay for billing peroid from date and todate
	 */
	
	DateBox dbbillingperiodFromDate;
	DateBox dbbillingperiodToDate;
	
	/**
	 * End here
	 */
	
	/**
	 * Date 1 march 2017 added by vijay 
	 * for add cash payment into petty cash entry if process configuration is active.
	 * requirement for pestcure 
	 */
	CheckBox cbaddintoPettyCash;
	ObjectListBox<PettyCash> olbpettyCashName;
	/**
	 * ends here
	 */
	
	
	TextBox tbTicketId;
	
	/*
	 *  nidhi
	 *  1-07-2017
	 */
	TextBox tbSegment;
	
	/*
	 * end
	 */
	
	/**
	 * Date 18-07-2017
	 * added by  nidhi for displaying rate contract service ID
	 */
	TextBox tbrateContractServiceid;
	/**
	 * End here
	 */
	
	 /**
	  *  nidhi
	  *  24-07-2017
	  *   this feild ad for customer ref no which comes from customer details
	  *   we can change ref so this field kept in editable.
	  */
	 TextBox tbReferenceNumber;
	 TextBox tbInvoiceRefNumber;//Ashwini Patil Date:23-09-2022
	 /**
	  *  end
	  */
	CustomerPayment custPaymentObj;
	
	/** date 21.02.2018 added by komal for revenue amd tax calculation **/
	 DoubleBox  dbBalanceRevenue ;
	 DoubleBox dbBalanceTax;
	 
	 DoubleBox  dbReceivedRevenue ;
	 DoubleBox dbReceivedTax;
	/**
	 * end komal
	 */
	 
	/** Date 09-05-2018 added by Vijay for followup date **/
	DateBox dbFollowUpDate;
	
	/** date 20.10.2018 added by komal for type of order and sub bill type **/
	TextBox tbTypeOfOrder;
	TextBox tbSubBillType;
	
	/**
	 * @author Anil , Date : 31-01-2019
	 * Adding company payment mode option
	 * this will reflect in tally accounting interface download
	 * For Pepcopp Raised By Sonu
	 */
	ObjectListBox<CompanyPayment> oblPaymentModeList;
	TextBox tbCompBankName;
	TextBox tbCompBankAccountNo;
	TextBox tbCompBankBranch;
	TextBox tbCompPayableAt;
    /**Date 26-11-2019 by Amol Added a Ref no2 field**/
	TextBox tbReferenceNoTwo;
	
	/**
	 * @author Priyanka 05-08-2021
	 * Des :- as per rahuls requirement added 1 upload option.
	 */
	UploadComposite upload1;
	
	public PaymentDetailsForm() {
		super();
		createGui();
		billingDoc.connectToLocal();
		
		tbCompBankName.setEnabled(false);
		tbCompBankAccountNo.setEnabled(false);
		tbCompBankBranch.setEnabled(false);
		tbCompPayableAt.setEnabled(false);
	}
	
	public PaymentDetailsForm  (String[] processlevel, FormField[][] fields,
			FormStyle formstyle) 
	{
		super(processlevel, fields, formstyle);
		createGui();
		
		tbCompBankName.setEnabled(false);
		tbCompBankAccountNo.setEnabled(false);
		tbCompBankBranch.setEnabled(false);
		tbCompPayableAt.setEnabled(false);
		dbamtreceived.setEnabled(false);
	}
	
	private void initalizeWidget()
	{
		ibpaymentid=new TextBox();
		ibpaymentid.setEnabled(false);
		ibinvoiceid=new TextBox();
		ibinvoiceid.setEnabled(false);
		ibcontractid=new TextBox();
		ibcontractid.setEnabled(false);
		tbpersoncount=new TextBox();
		tbpersonname=new TextBox();
		tbpersoncell=new TextBox();
		tbpersoncount.setEnabled(false);
		tbpersonname.setEnabled(false);
		tbpersoncell.setEnabled(false);
		tbpocName=new TextBox();
		tbpocName.setEnabled(false);
		dbinvoicedate=new DateBoxWithYearSelector();
		dbinvoicedate.setEnabled(false);
		doinvoiceamt=new DoubleBox();
		doinvoiceamt.setEnabled(false);
		doinvoiceamtcopy=new DoubleBox();
		doinvoiceamtcopy.setEnabled(false);
		billingDoc=new BillingDocumentTable();
		dosalesamount=new DoubleBox();
		dosalesamount.setEnabled(false);
		dototalbillamount=new DoubleBox();
		dototalbillamount.setEnabled(false);
		tbstatus=new TextBox();
		tbstatus.setEnabled(false);
		olbApproverName=new ObjectListBox<Employee>();
		AppUtility.makeSalesPersonListBoxLive(olbApproverName);
		olbPaymentTerms=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbPaymentTerms, Screen.PAYMENTTERMS);
		olbPaymentMethod=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbPaymentMethod, Screen.PAYMENTMETHODS);
		olbPaymentMethod.addChangeHandler(this);
		olbEmployee=new ObjectListBox<Employee>();
		AppUtility.makeSalesPersonListBoxLive(olbEmployee);
		tbbankAccNo=new TextBox();
		tbbankAccNo.setEnabled(false);
		tbbankName=new TextBox();
		tbbankName.setEnabled(false);
		tbbankBranch=new TextBox();
		tbbankBranch.setEnabled(false);
		ibchequeNo=new TextBox(); 
		ibchequeNo.setEnabled(false);
		dbChequeDate=new DateBoxWithYearSelector();
		dbChequeDate.setEnabled(false);
		dbamtreceived=new DoubleBox();
		dbamtreceived.setEnabled(false);
		dbpaymentdate=new DateBoxWithYearSelector();
		dbpaymentdate.setEnabled(false);
		tbinvoicetype=new TextBox();
		tbinvoicetype.setEnabled(false);
		tbreferenceno=new TextBox();
		tbreferenceno.setEnabled(false);
		dbtransferDate=new DateBoxWithYearSelector();
		dbtransferDate.setEnabled(false);
		dbtransferDate.addValueChangeHandler(this);
		tbaccounttype=new TextBox();
		tbaccounttype.setEnabled(false);
		taComment=new TextArea();
		taComment.setEnabled(false);
		ibpaymentAmt=new IntegerBox();
		dbpaymentAmt=new DoubleBox();
		ibpaymentAmt.setEnabled(false);
		dbpaymentAmt.setEnabled(false);
		cbCformStatus=new CheckBox();
		cbCformStatus.setEnabled(false);
		cbCformStatus.setValue(false);
		
		tbChequeIssuedBy=new TextBox();
		
		
		cbtdsApplicable = new CheckBox();
		cbtdsApplicable.setValue(false);
		
		// rohan has changes this code for Ultra as per requirement 
		tbtdsPerValue = new DoubleBox();
		tbtdsPerValue.setEnabled(false);
		
		dbtdsValue = new DoubleBox();
		dbtdsValue.setEnabled(false);
		//  ends here 
		
		
		ibNetPay = new DoubleBox();
//		ibNetPay.setEnabled(false);
		
		
//		olbtdsValue=new ObjectListBox<Config>();
//		AppUtility.MakeLiveConfig(olbtdsValue, Screen.TDSPERCENTAGE);
//		olbtdsValue.addChangeHandler(this);
//		olbtdsValue.setEnabled(false);
		
		tbTicketId = new TextBox();
		tbTicketId.setEnabled(false);
		
//		basePayable = new TextBox();
//		basePayable.setEnabled(false);
		
		dbbillingperiodFromDate = new DateBoxWithYearSelector();
		dbbillingperiodToDate = new DateBoxWithYearSelector();
		
		cbaddintoPettyCash = new CheckBox();
		cbaddintoPettyCash.setValue(false);
		olbpettyCashName=new ObjectListBox<PettyCash>();
		olbpettyCashName.MakeLive(new MyQuerry(new Vector<Filter>(), new PettyCash()));
		olbpettyCashName.setEnabled(false);
		
		/*
		 *  nidhi	1-07-2017
		 */
		
		tbSegment = new TextBox();
		tbSegment.setEnabled(false);
		
		/*
		 *  end
		 */
		
		/*
		 *  nidhi
		 *  	18-07-2017
		 *  
		 */
		
		tbrateContractServiceid = new TextBox();
		tbrateContractServiceid.setEnabled(false);
		/*
		 *  end
		 */
		
		
		/**
		 *  nidhi
		 *  24-07-2017
		 *   tbReferenceNumber
		 */
		tbReferenceNumber=new TextBox();
		/**
		 *  end
		 */
		
		tbInvoiceRefNumber=new TextBox();
		/** date 21.02.2018 added by komal for revenue amd tax calculation **/
		 dbBalanceRevenue = new DoubleBox();
		 dbBalanceRevenue.setEnabled(false);
		 dbBalanceTax = new DoubleBox();
		 dbBalanceTax.setEnabled(false);
		 dbReceivedRevenue = new DoubleBox();
		 dbReceivedRevenue.setEnabled(false);
		 dbReceivedTax = new DoubleBox();
		 dbReceivedTax.setEnabled(false);
	/**
	 * end komal
	 */
	
		 /** Date 09-05-2018 added by Vijay for followup date **/
		 dbFollowUpDate = new DateBoxWithYearSelector();	
		 /** date 20.10.2018 added by komal for type of order and sub bill type **/
		tbTypeOfOrder = new TextBox();
		tbTypeOfOrder.setEnabled(false);
		tbSubBillType = new TextBox();
		tbSubBillType.setEnabled(false);
		
		oblPaymentModeList=new ObjectListBox<CompanyPayment>();
		
		MyQuerry q=new MyQuerry();
		Filter temp=new Filter();
		temp.setQuerryString("paymentStatus");
		temp.setBooleanvalue(true);
		q.getFilters().add(temp);
		q.setQuerryObject(new CompanyPayment());
		oblPaymentModeList.MakeLive(q);
		oblPaymentModeList.addChangeHandler(this);
		tbCompBankName=new TextBox();
		tbCompBankAccountNo=new TextBox();
		tbCompBankBranch=new TextBox();
		tbCompPayableAt=new TextBox();
		
		tbCompBankName.setEnabled(false);
		tbCompBankAccountNo.setEnabled(false);
		tbCompBankBranch.setEnabled(false);
		tbCompPayableAt.setEnabled(false);
		tbReferenceNoTwo=new TextBox();
		upload1=new UploadComposite();
	}


	@Override
	public void createScreen() {
		initalizeWidget();

		//Token to initialize the processlevel menus.
		
		if(isPopUpAppMenubar()){
			Console.log("Inside isPopUpAppMenubar in payment details");
			this.processlevelBarNames=new String[]{AppConstants.SUBMIT,AppConstants.CANCELPAYMENT,"Update TDS"};
		}else{
			/**
			 * Date 22-12-2017 added if condition by vijay for TDS payment document updation data with indexes process bar button w
			 */
			
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CustomerPayment", "UpdateDataTDSFlagIndex")){
				
				this.processlevelBarNames=new String[]{AppConstants.SUBMIT,AppConstants.CANCELPAYMENT,"Update TDS",AppConstants.VIEWINVOICE,"Update All Data"};

			}else{
				
					/**
					 * Date : 10-10-2017 BY ANIL
					 * Added View Invoice button for going back to invoice screen.
					 */
				/**@Sheetal : 15-02-2022
				 * Added View Order and View Billing button,requirement by nitin sir***/
					this.processlevelBarNames=new String[]{AppConstants.SUBMIT,AppConstants.CANCELPAYMENT,AppConstants.VIEWINVOICE,AppConstants.VIEWORDER,AppConstants.VIEWBILL,"Update TDS",AppConstants.PAYMENTUPLOAD,AppConstants.TALLYPAYMENTUPLOAD,"Update Accounting Interface"};
				
			}	
		}
		
		
		
		
		
		
		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////
		//Token to initialize formfield
		String mainScreenLabel="Payment Details";
		if(custPaymentObj!=null&&custPaymentObj.getCount()!=0){
			mainScreenLabel=custPaymentObj.getCount()+"/"+custPaymentObj.getStatus()+"/"+AppUtility.parseDate(custPaymentObj.getPaymentDate());
		}
		
//		fgroupingCustomerInformation.setLabel(mainScreenLabel);
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		//FormField fgroupingCustomerInformation=fbuilder.setlabel("Customer Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).setKeyField(true).build();
		fgroupingCustomerInformation=fbuilder.setlabel(mainScreenLabel).widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).setKeyField(true).build();
		
		//FormFieldBuilder fbuilder
		//fbuilder = new FormFieldBuilder();
		//FormField fgroupingCustomerInformation=fbuilder.setlabel("Payment Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).setKeyField(true).build();
		fbuilder = new FormFieldBuilder("* Customer ID",tbpersoncount);
		FormField ftbpersoncount= fbuilder.setMandatory(true).setMandatoryMsg("BP ID is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Customer Name",tbpersonname);
		FormField ftbpersonname= fbuilder.setMandatory(true).setMandatoryMsg("BP Name is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Cell Number",tbpersoncell);
		FormField ftbpersoncell= fbuilder.setMandatory(true).setMandatoryMsg("Cell Number is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("POC Name",tbpocName);
		FormField ftbpocName= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingSalesOrderInfo=fbuilder.setlabel("Classification").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();		
		fbuilder = new FormFieldBuilder("Order ID",ibcontractid);
		FormField fibcontractid=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Total Order Amount",dosalesamount);
		FormField fdoamount=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Total Billing Amount",dototalbillamount);
		FormField fdototalbillamount=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingBilling=fbuilder.setlabel("Billing Document").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("",billingDoc.getTable());
		FormField ftablebilling=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingInvoiceInfo=fbuilder.setlabel("Reference/General").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Invoice ID",ibinvoiceid);
		FormField fibinvoiceid=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Invoice Date",dbinvoicedate);
		FormField fdbinvoicedate=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Invoice Amount",doinvoiceamt);
		FormField fdoinvoiceamt=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Invoice Amount",doinvoiceamtcopy);
		FormField fdoinvoiceamtcopy=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Account Type",tbaccounttype);
		FormField ftbaccounttype=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Invoice Type",tbinvoicetype);
		FormField ftbinvoicetype=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Person Responsible",olbEmployee);
		FormField folbEmployee=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
//		fbuilder = new FormFieldBuilder("Payment Date",dbpaymentdate);
//		FormField fdbpaymentdate=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/**
		 * Date 24-01-2018 By vijay
		 * for Payment date Mondatory with process config and show created status payment date field blank
		 */
		
		FormField fdbpaymentdate = null;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CustomerPayment", "PaymentDateMandatory")){
			fbuilder = new FormFieldBuilder("* Payment Due Date",dbpaymentdate); //Ashwini Patil changed label
			fdbpaymentdate=fbuilder.setMandatory(true).setMandatoryMsg("Payment Date is Mandatory!").setRowSpan(0).setColSpan(0).build();
		}else{
			fbuilder = new FormFieldBuilder("Payment Due Date",dbpaymentdate); //Ashwini Patil changed label
			 fdbpaymentdate=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
		/**
		 * ends here
		 */
		fbuilder = new FormFieldBuilder();
		/**add here **/
		FormField fgroupingBankInfo=fbuilder.setlabel("Bank Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingPaymentInfo=fbuilder.setlabel("Payment Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Payment ID",ibpaymentid);
		FormField fibpaymentid=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Payment Method",olbPaymentMethod);
		FormField folbPaymentMethod=fbuilder.setMandatory(true).setMandatoryMsg("Payment Method is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Payment Terms",olbPaymentTerms);
		FormField folbPaymentTerms=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Bank Account No.",tbbankAccNo);
		FormField ftbbankAccNo=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Bank Name",tbbankName);
		FormField ftbbankName=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Bank Branch",tbbankBranch);
		FormField ftbbankBranch=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Cheque No",ibchequeNo);
		FormField fibchequeNo=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Cheque Date",dbChequeDate);
		FormField fdbChequeDate=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		FormField fibamtreceived;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CustomerPayment", "RenameTDSWithWHT")){
		fbuilder = new FormFieldBuilder("Amount Including WHT",dbamtreceived);
		fibamtreceived=fbuilder.setMandatory(true).setMandatoryMsg("Amount Received is Mandatory!").setRowSpan(0).setColSpan(0).build();
		}else{
		fbuilder = new FormFieldBuilder("Due Amount Including TDS",dbamtreceived);
		fibamtreceived=fbuilder.setMandatory(true).setMandatoryMsg("Amount Received is Mandatory!").setRowSpan(0).setColSpan(0).build();	
		}
		
		
		fbuilder = new FormFieldBuilder("Digital-Reference No.",tbreferenceno);
		FormField ftbreferenceno=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		FormField fdbtransferDate;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CustomerPayment", "Paymentreceiveddatemandatory")){
			
			fbuilder = new FormFieldBuilder("* Payment Received Date",dbtransferDate);  //Ashwini Patil changed label
			fdbtransferDate=fbuilder.setMandatory(true).setMandatoryMsg("Payment Received Date is Mandatory!").setRowSpan(0).setColSpan(0).build();
		}else{
			fbuilder = new FormFieldBuilder("Payment Received Date",dbtransferDate);  //Ashwini Patil changed label
			fdbtransferDate=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
		
		fbuilder = new FormFieldBuilder("Status",tbstatus);
		FormField ftbstatus=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder();
		FormField fblankgroup=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingCommentInfo=fbuilder.setlabel("Comments").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Description (Max 500 characters)",taComment);
		FormField ftaComment=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		FormField fibpaymentAmt;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CONTRACT", "PC_NO_ROUNDOFF_INVOICE_CONTRACT")){
			
			fbuilder = new FormFieldBuilder("Gross Amount",dbpaymentAmt);
			fibpaymentAmt=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			ibpaymentAmt.setVisible(false);
			dbpaymentAmt.setVisible(true);
		}else {
			fbuilder = new FormFieldBuilder("Gross Amount",ibpaymentAmt);
			fibpaymentAmt=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			ibpaymentAmt.setVisible(true);
			dbpaymentAmt.setVisible(false);
		}
		
		
		fbuilder = new FormFieldBuilder("Is Cform Recieved",cbCformStatus);
		FormField fcbCformStatus=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Cheque Issued By",tbChequeIssuedBy);
		FormField ftbChequeIssuedBy=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
        /*   
         *    Added By Priyanka - 18/08/2021
         *    Des : Innovative : Rename TDS with WHT Requirmnet raised by Rahul
         */
		
		FormField ftdsValue;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CustomerPayment", "RenameTDSWithWHT")){
		fbuilder = new FormFieldBuilder("WHT Percentage",tbtdsPerValue);
		ftdsValue=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}else{
		fbuilder = new FormFieldBuilder("TDS Percentage",tbtdsPerValue);
		ftdsValue=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
		
		FormField fcbtdsApplicable;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CustomerPayment", "RenameTDSWithWHT")){
		fbuilder = new FormFieldBuilder("Is WHT Applicable",cbtdsApplicable);
		fcbtdsApplicable=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}else{
		fbuilder = new FormFieldBuilder("Is TDS Applicable",cbtdsApplicable);
		fcbtdsApplicable=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();	
		}
		
		FormField fibtdsValue;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CustomerPayment", "RenameTDSWithWHT")){
		fbuilder = new FormFieldBuilder("WHT Amount",dbtdsValue);
		fibtdsValue=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}else{
		fbuilder = new FormFieldBuilder("TDS Amount",dbtdsValue);
		fibtdsValue=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();	
		}
		/*
		 * End
		 */
		
		
		
		FormField fibNetPay;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CustomerPayment", "RenameTDSWithWHT")){
		fbuilder = new FormFieldBuilder(" * Amount Received Excluding WHT",ibNetPay);
		fibNetPay=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}else{
			fbuilder = new FormFieldBuilder(" * Due Amount Excluding TDS",ibNetPay);
			fibNetPay=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();	
		}
		
		fbuilder = new FormFieldBuilder("Ticket Id",tbTicketId);
		FormField ftbTicketId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		FormField fgroupingBillingPeroid=fbuilder.setlabel("Billing Period Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();

		fbuilder = new FormFieldBuilder("Billing From Date",dbbillingperiodFromDate);
		FormField fdbbillingperiodFromDate=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Billing To Date",dbbillingperiodToDate);
		FormField fdbbillingperiodToDate=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

//		fbuilder = new FormFieldBuilder("Base Amount",basePayable);
//		FormField fbasePayable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		FormField fgroupingPettyCashInfo=fbuilder.setlabel("Petty Cash").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();

		fbuilder = new FormFieldBuilder("Add into petty cash",cbaddintoPettyCash);
		FormField fcbaddintoPettyCash=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Petty Cash Name",olbpettyCashName);
		FormField ftbpettycashname= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/*
		 * nidhi
		 * 30-06-2017
		 */
		fbuilder = new FormFieldBuilder("Segment",tbSegment);
		FormField ftbSegment=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		/*
		 * End
		 */
		
		/*
		 * nidhi
		 * 18-07-2017
		 * search field add in form
		 */
		fbuilder = new FormFieldBuilder("Service Id",tbrateContractServiceid);
		FormField ftbrateContractServiceid=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		/*
		 *  end
		 */
		/**
		 * nidhi
		 * 30-06-2017
		 */
		
		/**
		 * Date : 18-11-2017 BY ANIL
		 * making reference number 1 mandatory through process configuration
		 */
		FormField ftbReferenceNumber;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CustomerPayment", "MakeReferenceNumberMandatory")){
			fbuilder = new FormFieldBuilder("* Reference Number ",tbReferenceNumber);
			ftbReferenceNumber= fbuilder.setMandatory(true).setMandatoryMsg("Reference Number is mandatory.").setRowSpan(0).setColSpan(0).build();
		}else{
			fbuilder = new FormFieldBuilder("Reference Number ",tbReferenceNumber);
			ftbReferenceNumber= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
		
		
		fbuilder = new FormFieldBuilder("Invoice Ref. No",tbInvoiceRefNumber);
		FormField ftbInvoiceRefNumber= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		/**
		 * End
		 */
		/** date 21.02.2018 added by komal for revenue amd tax calculation **/
		fbuilder = new FormFieldBuilder("Base Amount",dbBalanceRevenue);
		FormField fdbBalanceRevenue=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Tax Amount",dbBalanceTax);
		FormField fdbBalanceTax=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Adjusted base",dbReceivedRevenue);
		FormField fdbReceivedRevenue=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Adjusted Tax",dbReceivedTax);
		FormField fdbReceivedTax=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		/**
		 * end komal
		 */
		 /** Date 09-05-2018 added by Vijay for followup date **/
		fbuilder = new FormFieldBuilder("Follow-Up Date", dbFollowUpDate);
		FormField fdbfollowUpDate = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/** date 20.10.2018 added by komal for type of order and sub bill type **/	
		fbuilder = new FormFieldBuilder("Type Of Order", tbTypeOfOrder);
		FormField ftbTypeOfOrder= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Sub Bill Type",tbSubBillType);
		FormField ftbSubBillType= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Payment Mode",oblPaymentModeList);
		FormField foblPaymentModeList= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Bank Name",tbCompBankName);
		FormField ftbCompBankName= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Account Number",tbCompBankAccountNo);
		FormField ftbCompBankAccountNo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Bank Branch",tbCompBankBranch);
		FormField ftbCompBankBranch= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Payable At",tbCompPayableAt);
		FormField ftbCompPayableAt= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder=new FormFieldBuilder("Reference Number 2",tbReferenceNoTwo);
		FormField ftbReferenceNo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/**
	 * @author Priyanka 05-08-2021
	 * Des :- as per rahuls requirement added 1 upload option.
	 */
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingAttachment=fbuilder.setlabel("Attachment").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Upload 1",upload1); //fgroupingAttachment  fupload1
		FormField fupload1= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		FormField[][] formfield = { 
//				{fgroupingCustomerInformation},
//				{ftbpersoncount,ftbpersonname,ftbpersoncell,ftbpocName},
//				{fgroupingSalesOrderInfo},
//				{fibcontractid,fdoamount,ftbaccounttype,ftbTicketId},
//				{ftbSegment,ftbrateContractServiceid,ftbReferenceNumber,ftbReferenceNo},
//				{fgroupinginvoiceinfo},
//				{ftablebilling},
//				{fblankgroup,fdototalbillamount},
//				{fgroupingInvoiceInfo},
//				{fibinvoiceid,fdbinvoicedate,ftbinvoicetype,fdoinvoiceamt},
//				{fgroupingPaymentInfo},{fibpaymentid,fdbpaymentdate,folbPaymentMethod,folbEmployee},
//				/** date 21.02.2018 commented by komal */
////				/**Date :16-11-2017 By:Manisha
////				 * Commented C form label from screen..fcbCformStatus!!
////				 */
////				{ftbstatus,fibpaymentAmt,fibamtreceived/*fcbCformStatus*/},
////				/**
////				 * Ends For Manisha
////				 */
//				/** date 21.02.2018 added by komal for revenue and tax **/
//				{fibpaymentAmt ,fdbBalanceRevenue ,fdbBalanceTax , ftbstatus},
//				
//				
//				{fibNetPay,fcbtdsApplicable,ftdsValue,fibtdsValue},
//				{fibamtreceived ,fdbReceivedRevenue , fdbReceivedTax,fdbfollowUpDate},
//				{ftbbankAccNo,ftbbankName,ftbbankBranch,fibchequeNo},
//				{fdbChequeDate,fdbtransferDate,ftbreferenceno,ftbChequeIssuedBy},
//				{ftbTypeOfOrder , ftbSubBillType,foblPaymentModeList},	/** date 20.10.2018 added by komal **/
//				{ftbCompBankName,ftbCompBankAccountNo,ftbCompBankBranch,ftbCompPayableAt},
//				{fgroupingCommentInfo},
//				{ftaComment},
//				{fgroupingBillingPeroid},
//				{fdbbillingperiodFromDate,fdbbillingperiodToDate},
//				{fgroupingPettyCashInfo},
//				{fcbaddintoPettyCash,ftbpettycashname}
//				
//		};
				
				/**PAYMENT DETAILS**/
				{fgroupingCustomerInformation},
				{ftbpersoncount,ftbpersonname,ftbpersoncell,ftbpocName},
				{fibNetPay,fcbtdsApplicable,ftdsValue,fibtdsValue},
				{fibamtreceived,fdbpaymentdate,folbPaymentMethod,fdbtransferDate}, //Ashwini Patil Date:17-03-2022 Added Payment Received date next to payment method
				
				/**PAYMENT INFORMATION**/
				{fgroupingPaymentInfo},
				{fibpaymentAmt,fdbBalanceRevenue,fdbBalanceTax,fdbReceivedRevenue},
				{fdbReceivedTax,fdoinvoiceamtcopy},
				
				/**BANK INFO DETAILS**/
				{fgroupingBankInfo},
				{fibchequeNo,fdbChequeDate,ftbChequeIssuedBy,foblPaymentModeList},
				{ftbbankAccNo,ftbbankName,ftbbankBranch,ftbCompPayableAt},
				{ftbreferenceno}, //Ashwini Patil Date:17-03-2022 removed fdbtransferDate from here and added next to payment mode
			
				/**Billing Information**/
				{fgroupingBilling},
				{fdbbillingperiodFromDate,fdbbillingperiodToDate},
				{ftablebilling},
				{fblankgroup,fdototalbillamount},
				/**PETTY CASH INFO DETAILS**/
				{fgroupingPettyCashInfo},
				{fcbaddintoPettyCash,ftbpettycashname},
				{ftbCompBankName,ftbCompBankAccountNo},
				/**CLASSIFICATION INFO DETAILS**/
				{fgroupingSalesOrderInfo},
				{ftbSegment,ftbTypeOfOrder,ftbSubBillType},
				/**GENERAL INFO DETAILS**/
				{fgroupingInvoiceInfo},
				{fibcontractid,fdoamount,ftbrateContractServiceid,ftbaccounttype},
				{fibinvoiceid,fdbinvoicedate,ftbinvoicetype,fdoinvoiceamt},
				{fdbfollowUpDate,folbEmployee,ftbReferenceNumber,ftbReferenceNo},
				{ftbTicketId,ftbInvoiceRefNumber},
				{ftaComment},
				/**Attachment Tab**/
				{fgroupingAttachment},
				{fupload1},
				
		};


		this.fields=formfield;
	}

	@Override
	public void updateModel(CustomerPayment model) {
		
		PersonInfo personInfo=new PersonInfo();
		if(!tbpersoncount.getValue().equals("")){
			personInfo.setCount(Integer.parseInt(tbpersoncount.getValue()));
		}
		if(tbpersonname.getValue()!=null){
			personInfo.setFullName(tbpersonname.getValue());
		}
		if(!tbpersoncell.getValue().equals("")){
			personInfo.setCellNumber(Long.parseLong(tbpersoncell.getValue()));
		}
		if(tbpocName.getValue()!=null){
			personInfo.setPocName(tbpocName.getValue());
		}
		
		if(personInfo!=null){
			model.setPersonInfo(personInfo);
		}
		
		if(olbEmployee.getValue()!=null){
			model.setEmployee(olbEmployee.getValue());
		}
		
		if(!ibcontractid.getValue().equals(""))
			model.setContractCount(Integer.parseInt(ibcontractid.getValue()));
		if(dosalesamount.getValue()!=null)
			model.setTotalSalesAmount(dosalesamount.getValue());
		List<BillingDocumentDetails> billdoclis=this.billingDoc.getDataprovider().getList();
		ArrayList<BillingDocumentDetails> billtablearr=new ArrayList<BillingDocumentDetails>();
		billtablearr.addAll(billdoclis);
			model.setArrayBillingDocument(billtablearr);
		if(dototalbillamount.getValue()!=null)
			model.setTotalBillingAmount(dototalbillamount.getValue());
		if(dbinvoicedate.getValue()!=null)
			model.setInvoiceDate(dbinvoicedate.getValue());
		if(tbinvoicetype.getValue()!=null)
			model.setInvoiceType(tbinvoicetype.getValue());
		if(doinvoiceamt.getValue()!=null)
			model.setInvoiceAmount(doinvoiceamt.getValue());
		if(dbpaymentdate.getValue()!=null)
			model.setPaymentDate(dbpaymentdate.getValue());
		if(tbstatus.getValue()!=null)
			model.setStatus(tbstatus.getValue());
		if(olbPaymentMethod.getSelectedIndex()!=0)
			model.setPaymentMethod(olbPaymentMethod.getValue(olbPaymentMethod.getSelectedIndex()));
		if(dbamtreceived.getValue()!=null)
			model.setPaymentReceived(dbamtreceived.getValue());
		if(tbbankAccNo.getValue()!=null)
			model.setBankAccNo(tbbankAccNo.getValue());
		if(tbbankName.getValue()!=null)
			model.setBankName(tbbankName.getValue());
		if(tbbankBranch.getValue()!=null)
			model.setBankBranch(tbbankBranch.getValue());
		if(ibchequeNo.getValue()!=null)
			model.setChequeNo(ibchequeNo.getValue());
		if(dbChequeDate.getValue()!=null)
			model.setChequeDate(dbChequeDate.getValue());
		if(tbreferenceno.getValue()!=null)
			model.setReferenceNo(tbreferenceno.getValue());
		if(dbtransferDate.getValue()!=null){
			model.setAmountTransferDate(dbtransferDate.getValue());
		}
		else{
			model.setAmountTransferDate(null);
		}
		if(tbaccounttype.getValue()!=null)
			model.setAccountType(tbaccounttype.getValue());
		if(taComment.getValue()!=null){
			model.setComment(taComment.getValue());
		}
		if(ibpaymentAmt.getValue()!=null){
			model.setPaymentAmt(ibpaymentAmt.getValue());
		}
		if(dbpaymentAmt.getValue()!=null){
			model.setPaymentAmtInDecimal(dbpaymentAmt.getValue());
		}
		
		if(!tbChequeIssuedBy.getValue().equals("")){
			model.setChequeIssuedBy(tbChequeIssuedBy.getValue());
		}
		
		if(!ibNetPay.getValue().equals("")){
			model.setNetPay(ibNetPay.getValue());
		}
		
		if(dbtdsValue.getValue()!=null){
			model.setTdsTaxValue(dbtdsValue.getValue());
		}
		
		if(tbtdsPerValue.getValue()!=null){
			model.setTdsPercentage(tbtdsPerValue.getValue()+"");
		}
//		if(cbtdsApplicable.getValue()!=null){
			model.setTdsApplicable(cbtdsApplicable.getValue());
//		}
		
			if(!tbTicketId.getValue().equals("")){
				model.setTicketId(Integer.parseInt(tbTicketId.getValue()));
				}
			
//		if(!basePayable.getValue().trim().equals(""))
//		{
//			model.setBaseAmmount(Double.parseDouble(basePayable.getValue().trim()));
//		}	
			
		if (dbbillingperiodFromDate.getValue() != null) {
			model.setBillingPeroidFromDate(dbbillingperiodFromDate.getValue());
		}
		if (dbbillingperiodToDate.getValue() != null) {
			model.setBillingPeroidToDate(dbbillingperiodToDate.getValue());
		}
		
		model.setAddintoPettyCash(cbaddintoPettyCash.getValue());
		if(olbpettyCashName.getSelectedIndex()!=0)
			model.setPettyCashName(olbpettyCashName.getValue(olbpettyCashName.getSelectedIndex()));
			
		/*
		 *  nidhi
		 *  1-07-2017
		 */
		
		if(tbSegment.getValue()!=null)
			model.setSegment(tbSegment.getValue());
		
		/*
		 *  end
		 */
		
		
		/*
		 *  nidhi
		 *  17-07-2017
		 *  
		 */
		if(!tbrateContractServiceid.getValue().equals("")){
			model.setRateContractServiceId(Integer.parseInt(tbrateContractServiceid.getValue()));
		}
		/*
		 *  end
		 */
		
		/**
		 *  nidhi
		 *  24-07-2017
		 *  
		 */
		if(tbReferenceNumber.getValue()!=null)
		{
			model.setRefNumber(tbReferenceNumber.getValue());
			
		}
		else
		{
			model.setRefNumber(0+"");
		}
		
		if(tbInvoiceRefNumber.getValue()!=null)
		{
			model.setInvRefNumber(tbInvoiceRefNumber.getValue());			
		}
		else
		{
			model.setInvRefNumber("");
		}
		
		/**
		 *  end
		 */
		/** date 21.02.2018 added by komal for revenue and tax **/
		if(dbBalanceRevenue.getValue()!=null){
			model.setBalanceRevenue(dbBalanceRevenue.getValue());
		}
		if(dbBalanceTax.getValue()!=null){
			model.setBalanceTax(dbBalanceTax.getValue());
		}
		if(dbReceivedRevenue.getValue()!=null){
			model.setReceivedRevenue(dbReceivedRevenue.getValue());
		}
		if(dbReceivedTax.getValue()!=null){
			model.setReceivedTax(dbReceivedTax.getValue());
		}
		/** 
		 * end komal
		 */
		
		 /** Date 09-05-2018 added by Vijay for followup date **/
		if(dbFollowUpDate.getValue() == null){
			model.setFollowUpDate(dbpaymentdate.getValue());
		}else{
			model.setFollowUpDate(dbFollowUpDate.getValue());
		}
		
		if(oblPaymentModeList.getValue()!=null){
			model.setPaymentMode(oblPaymentModeList.getValue());
		}else{
			model.setPaymentMode("");
		}
		
		if(tbReferenceNoTwo.getValue()!=null){
			model.setRefNumberTwo(tbReferenceNoTwo.getValue());
		}else{
			model.setRefNumberTwo("");
		}
		
		if(upload1.getValue()!=null) {
			model.setDocUpload1(upload1.getValue());
		}
		
		custPaymentObj=model;
		presenter.setModel(model);
		
	}

	@Override
	public void updateView(CustomerPayment view) {
		
		custPaymentObj=view;
		
		ibpaymentid.setValue(view.getCount()+"");
		ibinvoiceid.setValue(view.getInvoiceCount()+"");
		
		if(view.getPersonInfo()!=null){
			tbpersoncount.setValue(view.getPersonInfo().getCount()+"");
			tbpersonname.setValue(view.getPersonInfo().getFullName());
			tbpersoncell.setValue(view.getPersonInfo().getCellNumber()+"");
			tbpocName.setValue(view.getPersonInfo().getPocName());
		}
		if(view.getContractCount()!=null)
			ibcontractid.setValue(view.getContractCount()+"");
		if(view.getEmployee()!=null)
			olbEmployee.setValue(view.getEmployee());
		if(view.getTotalSalesAmount()!=null)
			dosalesamount.setValue(view.getTotalSalesAmount());
		billingDoc.setValue(view.getArrayBillingDocument());
		if(view.getTotalBillingAmount()!=null)
			dototalbillamount.setValue(view.getTotalBillingAmount());
		if(view.getInvoiceAmount()!=null){
			doinvoiceamt.setValue(view.getInvoiceAmount());
			doinvoiceamtcopy.setValue(view.getInvoiceAmount());
		}
		if(view.getInvoiceDate()!=null)
			dbinvoicedate.setValue(view.getInvoiceDate());
		if(view.getInvoiceType()!=null)
			tbinvoicetype.setValue(view.getInvoiceType());
		
		/**
		 * Date 24-01-2018 By vijay
		 * for Payment date show blank for created status only with process config
		 */
		//Ashwini patil commented below code
//		if(view.getStatus().equalsIgnoreCase("Created") && (AppUtility.checkForProcessConfigurartionIsActiveOrNot("CustomerPayment", "PaymentDateMandatory"))){
//				dbpaymentdate.setValue(null);
//		}else{
		
			if(view.getPaymentDate()!=null)
				dbpaymentdate.setValue(view.getPaymentDate());
//		}
		/**
		 * ends here
		 */
		
		if(view.getPaymentMethod()!=null)
			olbPaymentMethod.setValue(view.getPaymentMethod());
		if(view.getPaymentReceived()!=0)
			dbamtreceived.setValue(view.getPaymentReceived());
		if(view.getBankAccNo()!=null)
			tbbankAccNo.setValue(view.getBankAccNo());
		if(view.getBankName()!=null)
			tbbankName.setValue(view.getBankName());
		if(view.getBankBranch()!=null)
			tbbankBranch.setValue(view.getBankBranch());
		if(view.getChequeNo()!=null)
			ibchequeNo.setValue(view.getChequeNo());
		if(view.getChequeDate()!=null)
			dbChequeDate.setValue(view.getChequeDate());
		if(view.getReferenceNo()!=null)
			tbreferenceno.setValue(view.getReferenceNo());
		if(view.getAmountTransferDate()!=null)
			dbtransferDate.setValue(view.getAmountTransferDate());
		if(view.getStatus()!=null)
			tbstatus.setValue(view.getStatus());
		if(view.getAccountType()!=null)
			tbaccounttype.setValue(view.getAccountType());
		
		if(view.getComment()!=null){
			taComment.setValue(view.getComment());
		}
		if(view.getPaymentAmt()!=0){
			ibpaymentAmt.setValue(view.getPaymentAmt());
		}
		if(view.getPaymentAmtInDecimal()!=0){
			dbpaymentAmt.setValue(view.getPaymentAmtInDecimal());
		}else {
			if(view.getPaymentAmt()!=0){
				dbpaymentAmt.setValue((double)view.getPaymentAmt());
			}
		}
		
		if(ibpaymentAmt.isVisible()) {
			
			if(view.getNetPay()>=0&&view.getNetPay()<=ibpaymentAmt.getValue()){ //Ashwini Patil Date: 11-03-2022 Description: amount received can be zero and it should be less than or equal to gross amount
				ibNetPay.setValue(view.getNetPay());
			}
		}else {
			
			if(view.getNetPay()>=0&&view.getNetPay()<=dbpaymentAmt.getValue()){ //Ashwini Patil Date: 11-03-2022 Description: amount received can be zero and it should be less than or equal to gross amount
				ibNetPay.setValue(view.getNetPay());
			}
		}
		
		//if(view.getTdsTaxValue()!=0){
			dbtdsValue.setValue(view.getTdsTaxValue());
		//}
		
		if(view.getTdsPercentage()!=null && ! view.getTdsPercentage().equals("")){
			tbtdsPerValue.setValue(Double.parseDouble(view.getTdsPercentage()));
		}
		
		if(view.getBillingPeroidFromDate()!=null){
			dbbillingperiodFromDate.setValue(view.getBillingPeroidFromDate());
		}
		if(view.getBillingPeroidToDate()!=null){
			dbbillingperiodToDate.setValue(view.getBillingPeroidToDate());
		}
//		if(view.getBaseAmmount()!=0)
//		{
//			basePayable.setValue(view.getBaseAmmount()+"");
//		}	
		
//		if(view.isTdsApplicable()== true){
			
//			System.out.println("ROHAN "+cbtdsApplicable.getValue());
//		}
		
			cbaddintoPettyCash.setValue(view.isAddintoPettyCash());
			if(view.getPettyCashName()!=null){
				olbpettyCashName.setValue(view.getPettyCashName());
			}
		
		if(view.getChequeIssuedBy()!=null){
			tbChequeIssuedBy.setValue(view.getChequeIssuedBy());
		}
		
		
		this.tbTicketId.setValue(view.getTicketId()+"");
		
		cformStatus=view.getOrderCformStatus();
		Console.log("Log For Cform"+cformStatus);
		
		/**
		 * rohan added this code on 12-06-2017
		 */	
		
		if(view.getPaymentMethod()!=null && !view.getPaymentMethod().equals(""))
		{//Ashwini Patil added {
		
		if(view.getPaymentMethod().equalsIgnoreCase("Cheque"))
		{
			/**
			 * Date : 19-07-2017 By ANIl
			 * Commented clearFields() because it erases the data map at the time of view
			 */
//			clearFields();
			this.tbbankAccNo.setEnabled(true);
			this.tbbankName.setEnabled(true);
			this.tbbankBranch.setEnabled(true);
			this.ibchequeNo.setEnabled(true);
			this.dbChequeDate.setEnabled(true);
			this.tbreferenceno.setEnabled(false);
			this.dbtransferDate.setEnabled(true); //Ashwini Patil set this to true
		}
		else if(view.getPaymentMethod().equalsIgnoreCase("ECS"))
		{
//			clearFields();
			this.tbreferenceno.setEnabled(true);
			this.dbtransferDate.setEnabled(true);
			this.tbbankAccNo.setEnabled(true);
			this.tbbankName.setEnabled(true);
			this.tbbankBranch.setEnabled(true);
			this.ibchequeNo.setEnabled(false);
			this.dbChequeDate.setEnabled(false);
		}
		else if(view.getPaymentMethod().equalsIgnoreCase("NEFT"))
		{
//			clearFields();
			this.tbbankAccNo.setEnabled(false);
			this.tbbankName.setEnabled(false);
			this.tbbankBranch.setEnabled(false);
			this.ibchequeNo.setEnabled(false);
			this.dbChequeDate.setEnabled(false);
			this.tbreferenceno.setEnabled(true);
			this.dbtransferDate.setEnabled(true);
		}
		/**
		 *  Added By Priyanka - 18-08-2021
		 *  Des : General : Payment Details -Digital-Transfer Date and Digital-Reference
		 *   No Field should be editable for all payment method except Cash and Cheque
		 *   Raised By Rahul.
		 */
		else if(view.getPaymentMethod().equalsIgnoreCase("Digital Wallet"))
		{
//			clearFields();
			this.tbbankAccNo.setEnabled(false);
			this.tbbankName.setEnabled(false);
			this.tbbankBranch.setEnabled(false);
			this.ibchequeNo.setEnabled(false);
			this.dbChequeDate.setEnabled(false);
			this.tbreferenceno.setEnabled(true);
			this.dbtransferDate.setEnabled(true);
		}
		else if(view.getPaymentMethod().equalsIgnoreCase("GPay"))
		{
//			clearFields();
			this.tbbankAccNo.setEnabled(false);
			this.tbbankName.setEnabled(false);
			this.tbbankBranch.setEnabled(false);
			this.ibchequeNo.setEnabled(false);
			this.dbChequeDate.setEnabled(false);
			this.tbreferenceno.setEnabled(true);
			this.dbtransferDate.setEnabled(true);
		}
		else if(view.getPaymentMethod().equalsIgnoreCase("PayZapp"))
		{
//			clearFields();
			this.tbbankAccNo.setEnabled(false);
			this.tbbankName.setEnabled(false);
			this.tbbankBranch.setEnabled(false);
			this.ibchequeNo.setEnabled(false);
			this.dbChequeDate.setEnabled(false);
			this.tbreferenceno.setEnabled(true);
			this.dbtransferDate.setEnabled(true);
		}
		else if(view.getPaymentMethod().equalsIgnoreCase("PhonePay"))  //  Digital Wallet  GPay  PayZapp PhonePay UPI
		{
//			clearFields();
			this.tbbankAccNo.setEnabled(false);
			this.tbbankName.setEnabled(false);
			this.tbbankBranch.setEnabled(false);
			this.ibchequeNo.setEnabled(false);
			this.dbChequeDate.setEnabled(false);
			this.tbreferenceno.setEnabled(true);
			this.dbtransferDate.setEnabled(true);
		}
		else if(view.getPaymentMethod().equalsIgnoreCase("UPI"))
		{
//			clearFields();
			this.tbbankAccNo.setEnabled(false);
			this.tbbankName.setEnabled(false);
			this.tbbankBranch.setEnabled(false);
			this.ibchequeNo.setEnabled(false);
			this.dbChequeDate.setEnabled(false);
			this.tbreferenceno.setEnabled(true);
			this.dbtransferDate.setEnabled(true);
		}
		/**
		 * Added by Sheetal : 11-11-2021
		 * Des: General-Payment Details -Digital-Transfer Date and Digital-Reference No.
		 *  Field should be editable for all payment method except Cash and Cheque  
		 */
		
		else if(view.getPaymentMethod().equalsIgnoreCase("Paytm"))
		{
//			clearFields();
			this.tbbankAccNo.setEnabled(false);
			this.tbbankName.setEnabled(false);
			this.tbbankBranch.setEnabled(false);
			this.ibchequeNo.setEnabled(false);
			this.dbChequeDate.setEnabled(false);
			this.tbreferenceno.setEnabled(true);
			this.dbtransferDate.setEnabled(true);
		}
        else
		{
			this.tbbankAccNo.setEnabled(false);
			this.tbbankName.setEnabled(false);
			this.tbbankBranch.setEnabled(false);
			this.ibchequeNo.setEnabled(false);
			this.dbChequeDate.setEnabled(false);
			this.tbreferenceno.setEnabled(false);
			this.dbtransferDate.setEnabled(true);//Ashwini Patil set this to true
		}
		}//Ashwini Patil
		/**
		 * ends here
		 */
		
		/*
		 *  nidhi
		 *  1-07-2017
		 */
		
		if(view.getSegment()!=null){
			tbSegment.setValue(view.getSegment());
		}
		
		/*
		 *  end
		 */
		
		/*
		 *  nidhi
		 *  1-07-2017
		 */
		if(view.getRateContractServiceId()!=0){
			tbrateContractServiceid.setValue(view.getRateContractServiceId()+"");
		}
		
		/*
		 *  end
		 *  
		 */
		/**
		 *  nidhi
		 *  24-07-2017
		 */
			if(view.getRefNumber()!=null)
				tbReferenceNumber.setValue(view.getRefNumber());
		/**
		 * end
		 */
			
			if(view.getInvRefNumber()!=null)
				tbInvoiceRefNumber.setValue(view.getInvRefNumber());
		
			/** date 21.02.2018 added by komal for revenue and tax **/
			if(view.getBalanceRevenue()!=0){
				dbBalanceRevenue.setValue(view.getBalanceRevenue());
			}
			if(view.getBalanceTax()!=0){
				dbBalanceTax.setValue(view.getBalanceTax());
			}
			if(view.getReceivedRevenue()!=0){
				dbReceivedRevenue.setValue(view.getReceivedRevenue());
			}
			if(view.getReceivedTax()!=0){
				dbReceivedTax.setValue(view.getReceivedTax());
			}
			/**
			 *  end komal
			 */

		 /** Date 09-05-2018 added by Vijay for followup date **/
		if(view.getFollowUpDate()!= null)
			dbFollowUpDate.setValue(view.getFollowUpDate());	
		/** date 20.10.2018 added by komal **/
		if(view.getTypeOfOrder()!=null){
			tbTypeOfOrder.setValue(view.getTypeOfOrder());
		}
		if(view.getSubBillType()!=null){
			tbSubBillType.setValue(view.getSubBillType());
		}
		
		if(view.getRefNumberTwo()!=null){
			tbReferenceNoTwo.setValue(view.getRefNumberTwo());
		}
		
		
		
		/**
		 * @author Anil,Date : 13-02-2018
		 * While viewing payment details from quick contract,screen got hang due payment mode drop down is not loaded
		 */
		if(view.getPaymentMode()!=null&&!view.getPaymentMode().equals("")){
			oblPaymentModeList.setValue(view.getPaymentMode());
			if(oblPaymentModeList.getValue()!=null&&oblPaymentModeList.getSelectedIndex()!=0){
				CompanyPayment compPay=oblPaymentModeList.getSelectedItem();
				tbCompBankName.setValue(compPay.getPaymentBankName());
				tbCompBankAccountNo.setValue(compPay.getPaymentAccountNo());
				tbCompBankBranch.setValue(compPay.getPaymentBranch());
				tbCompPayableAt.setValue(compPay.getPaymentPayableAt());
			}
		}
			
		
		if(view.getDocUpload1()!=null) {
			upload1.setValue(view.getDocUpload1());
		}
		if(presenter!=null)
		presenter.setModel(view);
		
	}

	@Override
	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Save")||text.contains("Discard")||text.contains("Search")||text.contains(AppConstants.NAVIGATION) || text.contains(AppConstants.MESSAGE))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
				
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			
			/**
			 * Date 30 july 2017 if condtion added by vijay for GENERAL POPUP menus 
			 * and old code added to else block for normal save menus
			 */
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();

			}
			
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Save")||text.contains("Discard")||text.contains(AppConstants.NAVIGATION)||text.contains(AppConstants.COMMUNICATIONLOG))
					menus[k].setVisible(true); 
				
				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			
			/**
			 * Date 30 july 2017 if condtion added by vijay for GENERAL POPUP menus 
			 * and old code added to else block for normal save menus
			 */
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();

			}
			
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Discard")||text.contains("Search")||text.contains("Edit")||text.contains("Print")||text.contains(AppConstants.NAVIGATION)||text.contains(AppConstants.COMMUNICATIONLOG)|| text.contains(AppConstants.MESSAGE))
						menus[k].setVisible(true); 
				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.PAYMENTDETAILS,LoginPresenter.currentModule.trim());

	}
	
	
	@Override
	public void setCount(int count)
	{
		ibpaymentid.setValue(count+"");
	}

	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		ibpaymentid.setEnabled(false);
		ibinvoiceid.setEnabled(false);
		tbstatus.setEnabled(false);
		ibcontractid.setEnabled(false);
		tbpersoncount.setEnabled(false);
		tbpersonname.setEnabled(false);
		tbpersoncell.setEnabled(false);
		dbinvoicedate.setEnabled(false);
		doinvoiceamt.setEnabled(false);
		doinvoiceamtcopy.setEnabled(false);
		dosalesamount.setEnabled(false);
		dototalbillamount.setEnabled(false);
		tbbankAccNo.setEnabled(false);
		tbbankName.setEnabled(false);
		tbbankBranch.setEnabled(false);
		ibchequeNo.setEnabled(false);
		tbreferenceno.setEnabled(false);
		tbinvoicetype.setEnabled(false);
		dbChequeDate.setEnabled(false);
		dbtransferDate.setEnabled(false);
		tbaccounttype.setEnabled(false);
		taComment.setEnabled(false);
		ibpaymentAmt.setEnabled(false);
		dbpaymentAmt.setEnabled(false);
		cbCformStatus.setEnabled(false);
		tbpocName.setEnabled(false);
		
//		ibNetPay.setEnabled(false);
		tbTicketId.setEnabled(false);
//		basePayable.setEnabled(false);
		
		dbbillingperiodFromDate.setEnabled(state);
		dbbillingperiodToDate.setEnabled(state);
		
		olbpettyCashName.setEnabled(false);	
		
		tbrateContractServiceid.setEnabled(false);
		/**
		 *  nidhi
		 *  24-07-2017
		 *   
		 */
		tbReferenceNumber.setEnabled(state);
		/**
		 *  end
		 */
		/**
		 *  nidhi
		 *  2-08-2017
		 *  for set edit mode depends on tds checkbox selected
		 */
		tbtdsPerValue.setEnabled(false);
		dbtdsValue.setEnabled(false);
		if(state && cbtdsApplicable.getValue()){
			tbtdsPerValue.setEnabled(state);
			dbtdsValue.setEnabled(state);

		}
		/*
		 * end
		 */
		/** date 21.02.2018 added by komal for revenue and tax **/
		dbBalanceRevenue.setEnabled(false);
		dbBalanceTax.setEnabled(false);
		dbReceivedRevenue.setEnabled(false);
		dbReceivedTax.setEnabled(false);
		/**
		 * end komal
		 */
		/** date 20.10.2018 added by komal **/
		tbTypeOfOrder.setEnabled(false);
		tbSubBillType.setEnabled(false);

		tbCompBankName.setEnabled(false);
		tbCompBankAccountNo.setEnabled(false);
		tbCompBankBranch.setEnabled(false);
		tbCompPayableAt.setEnabled(false);
		upload1.setEnable(state);
	}
	
	@Override
	public void setToEditState() {
		super.setToEditState();
		this.processLevelBar.setVisibleFalse(false);
		paymentField();
		checkCformLogic();
		dbamtreceived.setEnabled(false);
	}
	
	@Override
	public void setToViewState() {
		super.setToViewState();
		this.setAppHeaderBarAsPerStatus();
		
		
		SuperModel model=new CustomerPayment();
		model=custPaymentObj;
		
		//   rohan added this code for tds 
		if(custPaymentObj.isTdsApplicable()==true)
		{
			System.out.println("Rohanin side if");
			cbtdsApplicable.setValue(true);
		}
		else
		{
			System.out.println("Rohan inside else ");
			cbtdsApplicable.setValue(false);
		}
		
		AppUtility.addDocumentToHistoryTable(AppConstants.ACCOUNTMODULE,AppConstants.PAYMENTDETAILS, custPaymentObj.getCount(),custPaymentObj.getPersonInfo().getCount(),custPaymentObj.getPersonInfo().getFullName(),custPaymentObj.getPersonInfo().getCellNumber(), false, model, null);
	
	
		String mainScreenLabel="Payment Details";
		if(custPaymentObj!=null&&custPaymentObj.getCount()!=0){
			mainScreenLabel=custPaymentObj.getCount()+"/"+custPaymentObj.getStatus()+"/"+AppUtility.parseDate(custPaymentObj.getPaymentDate());
		}
		
		fgroupingCustomerInformation.getHeaderLabel().setText(mainScreenLabel);
		
	}

	
	public void setAppHeaderBarAsPerStatus()
	{
		this.toggleHeaderBar();
		this.changeProcessLevel();
	}

	public void toggleHeaderBar()
	{
		CustomerPayment entity=(CustomerPayment) presenter.getModel();
		String status=entity.getStatus();
		
		if(status.equals(CustomerPayment.PAYMENTCLOSED)||status.equals(CustomerPayment.PAYMENTCLOSED))
		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			
			/**
			 * Date 30 july 2017 if condtion added by vijay for GENERAL POPUP menus 
			 * and old code added to else block for normal save menus
			 */
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();

			}
			
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Discard")||text.contains("Search")||text.contains("Print")||text.contains("Email")||text.contains(AppConstants.NAVIGATION)||text.contains(AppConstants.MESSAGE))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
			}
		}
		
		if(status.equals(CustomerPayment.CANCELLED)||status.equals(CustomerPayment.CANCELLED))
		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			
			/**
			 * Date 30 july 2017 if condtion added by vijay for GENERAL POPUP menus 
			 * and old code added to else block for normal save menus
			 */
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();

			}
			
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Discard")||text.contains("Search")||text.contains(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		 
			}
		}
		
	//*************************rohan added this for removing print button in created state ***	 
//		if(status.equals(CustomerPayment.CREATED)||status.equals(CustomerPayment.CREATED))
//		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
//			for(int k=0;k<menus.length;k++)
//			{
//				String text=menus[k].getText();
//				if(text.contains("Discard")||text.contains("Search"))
//				{
//					menus[k].setVisible(true); 
//				}
//				else
//					menus[k].setVisible(false);  		   
//			}
//		}
	//***************************changes ends here ***********************************	
		////Ashwini Patil Date:28-09-2022
		if(status.equals(CustomerPayment.CREATED)||status.equals(CustomerPayment.CREATED))
		{
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			}
			for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
						if(text.contains("Discard")||text.contains("Search")||text.contains("Edit")||text.contains("Print")||text.contains(AppConstants.NAVIGATION)||text.contains(AppConstants.COMMUNICATIONLOG)|| text.contains(AppConstants.MESSAGE)||text.contains("Email"))
								menus[k].setVisible(true); 
						else
								menus[k].setVisible(false);  
				}
			
		}
	}
	
	public void paymentField()
	{
		if(this.getOlbPaymentMethod().getSelectedIndex()!=0&&this.getOlbPaymentMethod().getValue(getOlbPaymentMethod().getSelectedIndex()).equalsIgnoreCase("Cheque")){
			this.tbbankAccNo.setEnabled(true);
			this.tbbankName.setEnabled(true);
			this.tbbankBranch.setEnabled(true);
			this.ibchequeNo.setEnabled(true);
			this.dbChequeDate.setEnabled(true);
			this.tbreferenceno.setEnabled(false);
			this.dbtransferDate.setEnabled(true);//Ashwini Patil set it true 
			this.taComment.setEnabled(true);
		}
		else if(this.getOlbPaymentMethod().getSelectedIndex()!=0&&this.getOlbPaymentMethod().getValue(getOlbPaymentMethod().getSelectedIndex()).equalsIgnoreCase("Cash")){
			this.tbbankAccNo.setEnabled(false);
			this.tbbankName.setEnabled(false);
			this.tbbankBranch.setEnabled(false);
			this.ibchequeNo.setEnabled(false);
			this.dbChequeDate.setEnabled(false);
			this.tbreferenceno.setEnabled(false);
			this.dbtransferDate.setEnabled(true);//Ashwini Patil set it true 
			this.taComment.setEnabled(true);
		}
		else if(this.getOlbPaymentMethod().getSelectedIndex()!=0&&this.getOlbPaymentMethod().getValue(getOlbPaymentMethod().getSelectedIndex()).equalsIgnoreCase("ECS")){
			System.out.println("Edit ");
			this.tbreferenceno.setEnabled(true);
			this.dbtransferDate.setEnabled(true);
			this.tbbankAccNo.setEnabled(true);
			this.tbbankName.setEnabled(true);
			this.tbbankBranch.setEnabled(true);
			this.ibchequeNo.setEnabled(false);
			this.dbChequeDate.setEnabled(false);
			this.taComment.setEnabled(true);
		}
		/** date 14/10/2017 below else if added by  for NEFT payment method(setting reference number and transfer date enable **/
		else if(this.getOlbPaymentMethod().getSelectedIndex()!=0&&this.getOlbPaymentMethod().getValue(getOlbPaymentMethod().getSelectedIndex()).equalsIgnoreCase("NEFT")){
			System.out.println("Edit ");
			this.tbbankAccNo.setEnabled(false);
			this.tbbankName.setEnabled(false);
			this.tbbankBranch.setEnabled(false);
			this.ibchequeNo.setEnabled(false);
			this.dbChequeDate.setEnabled(false);
			this.tbreferenceno.setEnabled(true);
			this.dbtransferDate.setEnabled(true);
			this.taComment.setEnabled(true);  //Digital Wallet  GPay  PayZapp PhonePay UPI
		}
		else if(this.getOlbPaymentMethod().getSelectedIndex()!=0&&this.getOlbPaymentMethod().getValue(getOlbPaymentMethod().getSelectedIndex()).equalsIgnoreCase("Digital Wallet")){
			System.out.println("Edit ");
			this.tbbankAccNo.setEnabled(false);
			this.tbbankName.setEnabled(false);
			this.tbbankBranch.setEnabled(false);
			this.ibchequeNo.setEnabled(false);
			this.dbChequeDate.setEnabled(false);
			this.tbreferenceno.setEnabled(true);
			this.dbtransferDate.setEnabled(true);
			this.taComment.setEnabled(true);  //   PayZapp PhonePay UPI
		}
		else if(this.getOlbPaymentMethod().getSelectedIndex()!=0&&this.getOlbPaymentMethod().getValue(getOlbPaymentMethod().getSelectedIndex()).equalsIgnoreCase("GPay")){
			System.out.println("Edit ");
			this.tbbankAccNo.setEnabled(false);
			this.tbbankName.setEnabled(false);
			this.tbbankBranch.setEnabled(false);
			this.ibchequeNo.setEnabled(false);
			this.dbChequeDate.setEnabled(false);
			this.tbreferenceno.setEnabled(true);
			this.dbtransferDate.setEnabled(true);
			this.taComment.setEnabled(true);  //Digital Wallet  GPay  PayZapp PhonePay UPI
		}
		else if(this.getOlbPaymentMethod().getSelectedIndex()!=0&&this.getOlbPaymentMethod().getValue(getOlbPaymentMethod().getSelectedIndex()).equalsIgnoreCase("PayZapp")){
			System.out.println("Edit ");
			this.tbbankAccNo.setEnabled(false);
			this.tbbankName.setEnabled(false);
			this.tbbankBranch.setEnabled(false);
			this.ibchequeNo.setEnabled(false);
			this.dbChequeDate.setEnabled(false);
			this.tbreferenceno.setEnabled(true);
			this.dbtransferDate.setEnabled(true);
			this.taComment.setEnabled(true);  //Digital Wallet  GPay  PayZapp PhonePay UPI
		}
		else if(this.getOlbPaymentMethod().getSelectedIndex()!=0&&this.getOlbPaymentMethod().getValue(getOlbPaymentMethod().getSelectedIndex()).equalsIgnoreCase("PhonePay")){
			System.out.println("Edit ");
			this.tbbankAccNo.setEnabled(false);
			this.tbbankName.setEnabled(false);
			this.tbbankBranch.setEnabled(false);
			this.ibchequeNo.setEnabled(false);
			this.dbChequeDate.setEnabled(false);
			this.tbreferenceno.setEnabled(true);
			this.dbtransferDate.setEnabled(true);
			this.taComment.setEnabled(true);  //Digital Wallet  GPay  PayZapp PhonePay UPI
		}
		else if(this.getOlbPaymentMethod().getSelectedIndex()!=0&&this.getOlbPaymentMethod().getValue(getOlbPaymentMethod().getSelectedIndex()).equalsIgnoreCase("UPI")){
			System.out.println("Edit ");
			this.tbbankAccNo.setEnabled(false);
			this.tbbankName.setEnabled(false);
			this.tbbankBranch.setEnabled(false);
			this.ibchequeNo.setEnabled(false);
			this.dbChequeDate.setEnabled(false);
			this.tbreferenceno.setEnabled(true);
			this.dbtransferDate.setEnabled(true);
			this.taComment.setEnabled(true);  //Digital Wallet  GPay  PayZapp PhonePay UPI
		}
		/** Added by sheetal: 11-11-2021 for Paytm**/
		else if(this.getOlbPaymentMethod().getSelectedIndex()!=0&&this.getOlbPaymentMethod().getValue(getOlbPaymentMethod().getSelectedIndex()).equalsIgnoreCase("Paytm")){
			System.out.println("Edit ");
			this.tbbankAccNo.setEnabled(false);
			this.tbbankName.setEnabled(false);
			this.tbbankBranch.setEnabled(false);
			this.ibchequeNo.setEnabled(false);
			this.dbChequeDate.setEnabled(false);
			this.tbreferenceno.setEnabled(true);
			this.dbtransferDate.setEnabled(true);
			this.taComment.setEnabled(true);  //Digital Wallet  GPay  PayZapp PhonePay UPI Paytm
		}
		
		else
		{
			this.tbbankAccNo.setEnabled(false);
			this.tbbankName.setEnabled(false);
			this.tbbankBranch.setEnabled(false);
			this.ibchequeNo.setEnabled(false);
			this.dbChequeDate.setEnabled(false);
			this.tbreferenceno.setEnabled(false);
			this.dbtransferDate.setEnabled(true);//Ashwini Patil set it to true
			this.taComment.setEnabled(true);
		}
	}
	
	protected void checkCformLogic()
	{
		if(!cformStatus.equals(""))
		{
			if(cformStatus.trim().equals(AppConstants.YES))
			{
				this.getCbCformStatus().setEnabled(true);
				cformFlag=true;
			}
			
			if(cformStatus.trim().equals(AppConstants.NO))
			{
				cformFlag=false;
				this.getCbCformStatus().setEnabled(false);
			}
		}
	}
	
	public void changeProcessLevel()
	{
		CustomerPayment entity=(CustomerPayment) presenter.getModel();
		String status=entity.getStatus();
		boolean renewStatusFlag=entity.isRenewFlag();
		
		for(int i=0;i<getProcesslevelBarNames().length;i++)
		{
			InlineLabel label=getProcessLevelBar().btnLabels[i];
			String text=label.getText().trim();
				
			if(status.equals(CustomerPayment.CREATED))
			{
				if(text.equals(AppConstants.SUBMIT)){
					label.setVisible(true);
				}
				if(text.equals(AppConstants.CANCELPAYMENT))
				{
					label.setVisible(false);
				}
				if(text.equals(AppConstants.CANCELPAYMENT))
				{
					label.setVisible(true);
				}
				/**
				 * Date : 10-10-2017 BY ANIL
				 */
				if(text.equals(AppConstants.VIEWINVOICE))
				{
					label.setVisible(true);
				}
				/**
				 * End
				 */
				/**Added by Sheetal : 15-02-2022**/
				if(text.equals(AppConstants.VIEWORDER))
				{
					label.setVisible(true);
				}
				
				if(text.equals(AppConstants.VIEWBILL))
				{
					label.setVisible(true);
				}
				/**end**/
			
				if(text.equals("Update TDS")){
					label.setVisible(true);
				}
				
				if(text.equals(AppConstants.PAYMENTUPLOAD))
				{
					label.setVisible(false);
				}
				
				if(text.equals(AppConstants.TALLYPAYMENTUPLOAD))
				{
					label.setVisible(false);
				}
				
				if(text.equals("Update Accounting Interface")){
					label.setVisible(false);
				}
				
			}
			if(status.equals(CustomerPayment.PAYMENTCLOSED)&&renewStatusFlag==true)
			{
				if(text.equals(AppConstants.SUBMIT)){
					label.setVisible(false);
				}
				if(text.equals(AppConstants.CANCELPAYMENT))
				{
					label.setVisible(false);
				}
				/**
				 * Date : 10-10-2017 BY ANIL
				 */
				if(text.equals(AppConstants.VIEWINVOICE))
				{
					label.setVisible(true);
				}
				/**
				 * End
				 */
				/**Added by Sheetal : 15-02-2022**/
				if(text.equals(AppConstants.VIEWORDER))
				{
					label.setVisible(true);
				}
				
				if(text.equals(AppConstants.VIEWBILL))
				{
					label.setVisible(true);
				}
				/**end**/
				if(text.equals("Update TDS")){
					label.setVisible(true);
				}
				
				if(text.equals(AppConstants.PAYMENTUPLOAD))
				{
					label.setVisible(false);
				}
				
				if(text.equals(AppConstants.TALLYPAYMENTUPLOAD))
				{
					label.setVisible(false);
				}
				if(LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN")||LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("Zonal Coordinator")){
					if(text.equals("Update Accounting Interface"))
						label.setVisible(true);
				}
				else{
					if(text.equals("Update Accounting Interface"))
						label.setVisible(false);
				}

			}
			
			if(status.equals(CustomerPayment.PAYMENTCLOSED)&&renewStatusFlag==false)
			{
				if(text.equals(AppConstants.SUBMIT)){
					label.setVisible(false);
				}
				
				
				/**
				 * rohan added this code for NBHC Changes ADMIN role should  
				 */
				
				/** Addded By Priyanka - 28-07-2021
				 *  Des : Zonal Coordinator role should have same acces as per admin.
				 */
				if(LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN")||LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("Zonal Coordinator")){
					if(text.equals(AppConstants.CANCELPAYMENT))
						label.setVisible(true);
				}else{
					if(text.equals(AppConstants.CANCELPAYMENT))
					label.setVisible(false);
				}
				
				/**
				 * Date : 10-10-2017 BY ANIL
				 */
				if(text.equals(AppConstants.VIEWINVOICE))
				{
					label.setVisible(true);
				}
				/**
				 * End
				 */
				/**Added by Sheetal : 15-02-2022**/
				if(text.equals(AppConstants.VIEWORDER))
				{
					label.setVisible(true);
				}
				
				if(text.equals(AppConstants.VIEWBILL))
				{
					label.setVisible(true);
				}
				/**end**/
				if(text.equals("Update TDS")){
					label.setVisible(true);
				}
				
				
				if(text.equals(AppConstants.PAYMENTUPLOAD))
				{
					label.setVisible(false);
				}
				
				if(text.equals(AppConstants.TALLYPAYMENTUPLOAD))
				{
					label.setVisible(false);
				}
				
				if(LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN")||LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("Zonal Coordinator")){
					if(text.equals("Update Accounting Interface"))
						label.setVisible(true);
				}
				else{
					if(text.equals("Update Accounting Interface"))
						label.setVisible(false);
				}
			}
			
			if(status.equals(CustomerPayment.CANCELLED)){
				
				if(text.equals(AppConstants.SUBMIT)){
					label.setVisible(false);
				}
				if(text.equals(AppConstants.CANCELPAYMENT))
				{
					label.setVisible(false);
				}
				/**
				 * Date : 10-10-2017 BY ANIL
				 */
				if(text.equals(AppConstants.VIEWINVOICE))
				{
					label.setVisible(true);
				}
				/**
				 * End
				 */
				/**Added by Sheetal : 15-02-2022**/
				if(text.equals(AppConstants.VIEWORDER))
				{
					label.setVisible(true);
				}
				
				if(text.equals(AppConstants.VIEWBILL))
				{
					label.setVisible(true);
				}
				/**end**/
				if(text.equals("Update TDS")){
					label.setVisible(false);
				}
				
				if(text.equals(AppConstants.PAYMENTUPLOAD))
				{
					label.setVisible(false);
				}
				
				if(text.equals(AppConstants.TALLYPAYMENTUPLOAD))
				{
					label.setVisible(false);
				}
			}
			
			if(isPopUpAppMenubar()){
				/**
				 * Date : 10-10-2017 By ANIL
				 */
				if(text.equals(AppConstants.VIEWINVOICE)){
					label.setVisible(false);
				}
				/**
				 * End
				 */
			}
			//Ashwini Patil Date:20-09-2022
			if(isHideNavigationButtons()) {
				Console.log("in changeProcessLevel isHideNavigationButtons");
				if(text.equalsIgnoreCase(AppConstants.VIEWBILL)||text.equalsIgnoreCase(AppConstants.VIEWINVOICE)||text.equalsIgnoreCase(AppConstants.VIEWORDER))
					label.setVisible(false);	
			}
		}
		
		
		
	}
	
	@Override
	public boolean validate() {
		System.out.println("Validate Method==");
		boolean superRes=super.validate();
		boolean validatePaymentMethods=true;
		boolean amtValidation=true;
		boolean cformValidation=true;
		String payMethod=this.olbPaymentMethod.getValue(getOlbPaymentMethod().getSelectedIndex()).trim();
		
		System.out.println("Validate Method="+payMethod);
	
		if(payMethod.equalsIgnoreCase("Cheque"))
		{
			//  rohan commented this code to remove Accnt No mandetory
			//  date : 17/11/2016
//			if(this.tbbankAccNo.getValue()==null||this.tbbankAccNo.getValue().equals(""))
//			{
//				validatePaymentMethods=false;
//				this.showDialogMessage("Bank Account is Mandatory!");
//			}
//			if(this.tbbankName.getValue()==null||this.tbbankName.getValue().equals("")&&!tbbankAccNo.getValue().equals(""))
//			{
//				validatePaymentMethods=false;
//				this.showDialogMessage("Bank Name is Mandatory!");
//			}
//			if(this.tbbankBranch.getValue()==null||this.tbbankBranch.getValue().equals(""))
//			{
//				validatePaymentMethods=false;
//				this.showDialogMessage("Bank Branch is Mandatory!");
//			}
			if(this.ibchequeNo.getValue()==null && this.ibchequeNo.getValue().equals(""))
			{
				validatePaymentMethods=false;
				this.showDialogMessage("Cheque No is Mandatory!");
			}
			if(this.dbChequeDate.getValue()==null )
			{
				validatePaymentMethods=false;
				this.showDialogMessage("Cheque Date is Mandatory!");
			}
		}
		else if(payMethod.equalsIgnoreCase("ECS"))
		{
//			if(this.tbbankAccNo.getValue()==null||this.tbbankAccNo.getValue().equals(""))
//			{
//				validatePaymentMethods=false;
//				this.showDialogMessage("Bank Account is Mandatory!");
//			}
//			if(this.tbbankName.getValue()==null||this.tbbankName.getValue().equals("")&&!tbbankAccNo.getValue().equals(""))
//			{
//				validatePaymentMethods=false;
//				this.showDialogMessage("Bank Name is Mandatory!");
//			}
			if(this.tbbankBranch.getValue()==null||this.tbbankBranch.getValue().equals(""))
			{
				validatePaymentMethods=false;
				this.showDialogMessage("Bank Branch is Mandatory!");
			}
//			if(this.tbreferenceno.getValue()==null||this.tbreferenceno.getValue().equals("")&&!tbbankBranch.getValue().equals(""))
//			{
//				validatePaymentMethods=false;
//				this.showDialogMessage("Reference No is Mandatory!");
//			}
			if(this.dbtransferDate.getValue()==null||this.dbtransferDate.getValue().equals(""))
			{
				validatePaymentMethods=false;
				this.showDialogMessage("Transfer Date is Mandatory!");
			}
		}
			System.out.println("Find");
			
			
			double payreceived=this.getDbamtreceived().getValue();
			double invoiceAmt=this.doinvoiceamt.getValue();
			//Ashwini Patil Date:27-07-2022 Innovative requirement
//			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CONTRACT", "PC_NO_ROUNDOFF_INVOICE_CONTRACT")){
//				Console.log("PC_NO_ROUNDOFF_INVOICE_CONTRACT is active -from payment details");
			if(ibpaymentAmt.isVisible()) {
				
				double payableAmt=this.getIbpaymentAmt().getValue();
				if(payreceived>invoiceAmt) {
					showDialogMessage("Amount receive should not be greater than Invoice amount");
					amtValidation=false;
				}
			}else 
			{	
				double payableAmt=this.getDbpaymentAmt().getValue()+1;
				if(payreceived>payableAmt) {
					showDialogMessage("Amount receive should not be greater than Payable amount");
					amtValidation=false;
				}
			}
			
			if(cformFlag==true&&cbCformStatus.getValue()==false)
			{
				showDialogMessage("Is Cform received mandatory!");
				cformValidation=false;
			}
			
			/**
			 * Date 1 march 2017
			 * added by vijay 
			 * for payment cash entry in peetycash validation in only process config is active
			 */
			if(PaymentDetailsPresenter.processConfigAddtoPettyCashEntry){
				if(cbaddintoPettyCash.getValue()==false || olbpettyCashName.getSelectedIndex()==0){
					showDialogMessage("Please select petty cash information");
					superRes= false;
				}
			}
			/**
			 * ends here
			 */
			

		return superRes&&validatePaymentMethods&&amtValidation&&cformValidation;
	}
	
	@Override
	public void onChange(ChangeEvent event) {
		
		/**
		 * @author Anil,Date : 31-01-2019
		 */
		if(event.getSource().equals(oblPaymentModeList)){
			if(oblPaymentModeList.getSelectedIndex()!=0){
				CompanyPayment compPay=oblPaymentModeList.getSelectedItem();
				tbCompBankName.setValue(compPay.getPaymentBankName());
				tbCompBankAccountNo.setValue(compPay.getPaymentAccountNo());
				tbCompBankBranch.setValue(compPay.getPaymentBranch());
				tbCompPayableAt.setValue(compPay.getPaymentPayableAt());
			}else{
				tbCompBankAccountNo.setValue("");
				tbCompBankBranch.setValue("");
				tbCompBankName.setValue("");
				tbCompPayableAt.setValue("");
			}
			return;
		}
       
		String payMethodVal=null;
		if(this.olbPaymentMethod.getSelectedIndex()!=0)
		{
			payMethodVal=olbPaymentMethod.getValue(olbPaymentMethod.getSelectedIndex()).trim();
			if(payMethodVal.equalsIgnoreCase("Cash"))
			{
				clearFields();
				this.tbbankAccNo.setEnabled(false);
				this.tbbankName.setEnabled(false);
				this.tbbankBranch.setEnabled(false);
				this.ibchequeNo.setEnabled(false);
				this.dbChequeDate.setEnabled(false);
				this.tbreferenceno.setEnabled(false);
				this.dbtransferDate.setEnabled(true);//Ashwini Patil set it to true
				// Date 24-04-2018 By vijay
				tbChequeIssuedBy.setEnabled(false);
			}
			else if(payMethodVal.equalsIgnoreCase("Cheque"))
			{
				clearFields();
				this.tbbankAccNo.setEnabled(true);
				this.tbbankName.setEnabled(true);
				this.tbbankBranch.setEnabled(true);
				this.ibchequeNo.setEnabled(true);
				this.dbChequeDate.setEnabled(true);
				this.tbreferenceno.setEnabled(false);
				this.dbtransferDate.setEnabled(true);//Ashwini Patil set it to true
				// Date 24-04-2018 By vijay
				tbChequeIssuedBy.setEnabled(true);
			}
			else if(payMethodVal.equalsIgnoreCase("ECS"))
			{
				clearFields();
				this.tbreferenceno.setEnabled(true);
				this.dbtransferDate.setEnabled(true);
				this.tbbankAccNo.setEnabled(true);
				this.tbbankName.setEnabled(true);
				this.tbbankBranch.setEnabled(true);
				this.ibchequeNo.setEnabled(false);
				this.dbChequeDate.setEnabled(false);
				// Date 24-04-2018 By vijay
				tbChequeIssuedBy.setEnabled(true);
			}
			else if(payMethodVal.equalsIgnoreCase("NEFT"))
			{
				clearFields();
				this.tbbankAccNo.setEnabled(false);
				this.tbbankName.setEnabled(false);
				this.tbbankBranch.setEnabled(false);
				this.ibchequeNo.setEnabled(false);
				this.dbChequeDate.setEnabled(false);
				this.tbreferenceno.setEnabled(true);
				this.dbtransferDate.setEnabled(true);
				// Date 24-04-2018 By vijay
				tbChequeIssuedBy.setEnabled(false);
			}
			else if(payMethodVal.equalsIgnoreCase("Digital Wallet"))
			{
				clearFields();
				this.tbbankAccNo.setEnabled(false);
				this.tbbankName.setEnabled(false);
				this.tbbankBranch.setEnabled(false);
				this.ibchequeNo.setEnabled(false);
				this.dbChequeDate.setEnabled(false);
				this.tbreferenceno.setEnabled(true);
				this.dbtransferDate.setEnabled(true);
				// Date 24-04-2018 By vijay
				tbChequeIssuedBy.setEnabled(false);
			}
			else if(payMethodVal.equalsIgnoreCase("GPay"))
			{
				clearFields();
				this.tbbankAccNo.setEnabled(false);
				this.tbbankName.setEnabled(false);
				this.tbbankBranch.setEnabled(false);
				this.ibchequeNo.setEnabled(false);
				this.dbChequeDate.setEnabled(false);
				this.tbreferenceno.setEnabled(true);
				this.dbtransferDate.setEnabled(true);
				// Date 24-04-2018 By vijay
				tbChequeIssuedBy.setEnabled(false);
			}
			else if(payMethodVal.equalsIgnoreCase("PayZapp")) //Digital Wallet  GPay  PayZapp PhonePay UPI
			{
				clearFields();
				this.tbbankAccNo.setEnabled(false);
				this.tbbankName.setEnabled(false);
				this.tbbankBranch.setEnabled(false);
				this.ibchequeNo.setEnabled(false);
				this.dbChequeDate.setEnabled(false);
				this.tbreferenceno.setEnabled(true);
				this.dbtransferDate.setEnabled(true);
				// Date 24-04-2018 By vijay
				tbChequeIssuedBy.setEnabled(false);
			}
			else if(payMethodVal.equalsIgnoreCase("PhonePay"))
			{
				clearFields();
				this.tbbankAccNo.setEnabled(false);
				this.tbbankName.setEnabled(false);
				this.tbbankBranch.setEnabled(false);
				this.ibchequeNo.setEnabled(false);
				this.dbChequeDate.setEnabled(false);
				this.tbreferenceno.setEnabled(true);
				this.dbtransferDate.setEnabled(true);
				// Date 24-04-2018 By vijay
				tbChequeIssuedBy.setEnabled(false);
			}else if(payMethodVal.equalsIgnoreCase("UPI"))
			{
				clearFields();
				this.tbbankAccNo.setEnabled(false);
				this.tbbankName.setEnabled(false);
				this.tbbankBranch.setEnabled(false);
				this.ibchequeNo.setEnabled(false);
				this.dbChequeDate.setEnabled(false);
				this.tbreferenceno.setEnabled(true);
				this.dbtransferDate.setEnabled(true);
				// Date 24-04-2018 By vijay
				tbChequeIssuedBy.setEnabled(false);
			}
			//added by sheetal on 11-11-2021
			else if(payMethodVal.equalsIgnoreCase("Paytm"))//Digital Wallet  GPay  PayZapp PhonePay UPI Paytm
			{
				clearFields();
				this.tbbankAccNo.setEnabled(false);
				this.tbbankName.setEnabled(false);
				this.tbbankBranch.setEnabled(false);
				this.ibchequeNo.setEnabled(false);
				this.dbChequeDate.setEnabled(false);
				this.tbreferenceno.setEnabled(true);
				this.dbtransferDate.setEnabled(true);
				// Date 24-04-2018 By vijay
				tbChequeIssuedBy.setEnabled(false);
			}
				
		else
		{
			clearFields();
			this.tbbankAccNo.setEnabled(false);
			this.tbbankName.setEnabled(false);
			this.tbbankBranch.setEnabled(false);
			this.ibchequeNo.setEnabled(false);
			this.dbChequeDate.setEnabled(false);
			this.tbreferenceno.setEnabled(false);
			this.dbtransferDate.setEnabled(true);//Ashwini Patil set it to true
			// Date 24-04-2018 By vijay
			tbChequeIssuedBy.setEnabled(false);
		}
		}
	}
	
	private void clearFields()
	{
		this.tbbankAccNo.setValue("");
		this.tbbankName.setValue("");
		this.tbbankBranch.setValue("");
		this.ibchequeNo.setValue(null);
		this.dbChequeDate.setValue(null);
		this.dbtransferDate.setValue(null);
		this.tbreferenceno.setValue("");
	}

	
	/*************************************Getters And Setters******************************************/
	
	
	public TextBox getIbpaymentid() {
		return ibpaymentid;
	}

	public void setIbpaymentid(TextBox ibpaymentid) {
		this.ibpaymentid = ibpaymentid;
	}
	

	public TextBox getIbinvoiceid() {
		return ibinvoiceid;
	}

	public void setIbinvoiceid(TextBox ibinvoiceid) {
		this.ibinvoiceid = ibinvoiceid;
	}

	public TextBox getIbcontractid() {
		return ibcontractid;
	}

	public void setIbcontractid(TextBox ibcontractid) {
		this.ibcontractid = ibcontractid;
	}

	public TextBox getTbpersoncount() {
		return tbpersoncount;
	}

	public void setTbpersoncount(TextBox tbpersoncount) {
		this.tbpersoncount = tbpersoncount;
	}

	public TextBox getTbpersonname() {
		return tbpersonname;
	}

	public void setTbpersonname(TextBox tbpersonname) {
		this.tbpersonname = tbpersonname;
	}

	public TextBox getTbpersoncell() {
		return tbpersoncell;
	}

	public void setTbpersoncell(TextBox tbpersoncell) {
		this.tbpersoncell = tbpersoncell;
	}

	public TextBox getTbaccounttype() {
		return tbaccounttype;
	}

	public void setTbaccounttype(TextBox tbaccounttype) {
		this.tbaccounttype = tbaccounttype;
	}

	public DateBox getDbinvoicedate() {
		return dbinvoicedate;
	}

	public void setDbinvoicedate(DateBox dbinvoicedate) {
		this.dbinvoicedate = dbinvoicedate;
	}

	public DateBox getDbpaymentdate() {
		return dbpaymentdate;
	}

	public void setDbpaymentdate(DateBox dbpaymentdate) {
		this.dbpaymentdate = dbpaymentdate;
	}

	public DoubleBox getDosalesamount() {
		return dosalesamount;
	}

	public void setDosalesamount(DoubleBox dosalesamount) {
		this.dosalesamount = dosalesamount;
	}

	public DoubleBox getDototalbillamount() {
		return dototalbillamount;
	}

	public void setDototalbillamount(DoubleBox dototalbillamount) {
		this.dototalbillamount = dototalbillamount;
	}

	public TextBox getTbstatus() {
		return tbstatus;
	}

	public void setTbstatus(TextBox tbstatus) {
		this.tbstatus = tbstatus;
	}

	public ObjectListBox<Employee> getOlbEmployee() {
		return olbEmployee;
	}

	public void setOlbEmployee(ObjectListBox<Employee> olbEmployee) {
		this.olbEmployee = olbEmployee;
	}

	public ObjectListBox<Employee> getOlbApproverName() {
		return olbApproverName;
	}

	public void setOlbApproverName(ObjectListBox<Employee> olbApproverName) {
		this.olbApproverName = olbApproverName;
	}

	public ObjectListBox<Config> getOlbPaymentMethod() {
		return olbPaymentMethod;
	}

	public void setOlbPaymentMethod(ObjectListBox<Config> olbPaymentMethod) {
		this.olbPaymentMethod = olbPaymentMethod;
	}

	public ObjectListBox<Config> getOlbPaymentTerms() {
		return olbPaymentTerms;
	}

	public void setOlbPaymentTerms(ObjectListBox<Config> olbPaymentTerms) {
		this.olbPaymentTerms = olbPaymentTerms;
	}

	public TextBox getTbbankAccNo() {
		return tbbankAccNo;
	}

	public void setTbbankAccNo(TextBox tbbankAccNo) {
		this.tbbankAccNo = tbbankAccNo;
	}

	public TextBox getTbbankName() {
		return tbbankName;
	}

	public void setTbbankName(TextBox tbbankName) {
		this.tbbankName = tbbankName;
	}

	public TextBox getTbbankBranch() {
		return tbbankBranch;
	}

	public void setTbbankBranch(TextBox tbbankBranch) {
		this.tbbankBranch = tbbankBranch;
	}

	
//	public IntegerBox getIbchequeNo() {
//		return ibchequeNo;
//	}
//
//	public void setIbchequeNo(IntegerBox ibchequeNo) {
//		this.ibchequeNo = ibchequeNo;
//	}
	
	public TextBox getIbchequeNo() {
		return ibchequeNo;
	}

	public void setIbchequeNo(TextBox ibchequeNo) {
		this.ibchequeNo = ibchequeNo;
	}
	

	public DateBox getDbChequeDate() {
		return dbChequeDate;
	}


	public void setDbChequeDate(DateBox dbChequeDate) {
		this.dbChequeDate = dbChequeDate;
	}

//	public IntegerBox getIbamtreceived() {
//		return ibamtreceived;
//	}
//
//	public void setIbamtreceived(IntegerBox ibamtreceived) {
//		this.ibamtreceived = ibamtreceived;
//	}

	public TextBox getTbinvoicetype() {
		return tbinvoicetype;
	}

	public void setTbinvoicetype(TextBox tbinvoicetype) {
		this.tbinvoicetype = tbinvoicetype;
	}

	public BillingDocumentTable getBillingDoc() {
		return billingDoc;
	}

	public void setBillingDoc(BillingDocumentTable billingDoc) {
		this.billingDoc = billingDoc;
	}

	public DateBox getDbtransferDate() {
		return dbtransferDate;
	}

	public void setDbtransferDate(DateBox dbtransferDate) {
		this.dbtransferDate = dbtransferDate;
	}

	public TextBox getTbreferenceno() {
		return tbreferenceno;
	}

	public void setTbreferenceno(TextBox tbreferenceno) {
		this.tbreferenceno = tbreferenceno;
	}

	public DoubleBox getDoinvoiceamt() {
		return doinvoiceamt;
	}

	public void setDoinvoiceamt(DoubleBox doinvoiceamt) {
		this.doinvoiceamt = doinvoiceamt;
	}

	public IntegerBox getIbpaymentAmt() {
		return ibpaymentAmt;
	}

	public void setIbpaymentAmt(IntegerBox ibpaymentAmt) {
		this.ibpaymentAmt = ibpaymentAmt;
	}

	public DoubleBox getDbpaymentAmt() {
		return dbpaymentAmt;
	}

	public void setDbpaymentAmt(DoubleBox dbpaymentAmt) {
		this.dbpaymentAmt = dbpaymentAmt;
	}
	public TextArea getTaComment() {
		return taComment;
	}

	public void setTaComment(TextArea taComment) {
		this.taComment = taComment;
	}

	public CheckBox getCbCformStatus() {
		return cbCformStatus;
	}

	public void setCbCformStatus(CheckBox cbCformStatus) {
		this.cbCformStatus = cbCformStatus;
	}

	public TextBox getTbpocName() {
		return tbpocName;
	}

	public void setTbpocName(TextBox tbpocName) {
		this.tbpocName = tbpocName;
	}

	public TextBox getTbChequeIssuedBy() {
		return tbChequeIssuedBy;
	}

	public void setTbChequeIssuedBy(TextBox tbChequeIssuedBy) {
		this.tbChequeIssuedBy = tbChequeIssuedBy;
	}

	public DoubleBox getIbNetPay() {
		return ibNetPay;
	}

	public void setIbNetPay(DoubleBox ibNetPay) {
		this.ibNetPay = ibNetPay;
	}

	public CheckBox getCbtdsApplicable() {
		return cbtdsApplicable;
	}

	public void setCbtdsApplicable(CheckBox cbtdsApplicable) {
		this.cbtdsApplicable = cbtdsApplicable;
	}

	

	public DoubleBox getDbtdsValue() {
		return dbtdsValue;
	}

	public void setDbtdsValue(DoubleBox dbtdsValue) {
		this.dbtdsValue = dbtdsValue;
	}

	public DoubleBox getDbamtreceived() {
		return dbamtreceived;
	}

	public void setDbamtreceived(DoubleBox dbamtreceived) {
		this.dbamtreceived = dbamtreceived;
	}

	public DoubleBox getTbtdsPerValue() {
		return tbtdsPerValue;
	}

	public void setTbtdsPerValue(DoubleBox tbtdsPerValue) {
		this.tbtdsPerValue = tbtdsPerValue;
	}
	public TextBox getTbSegment() {
		return tbSegment;
	}

	public void setTbSegment(TextBox tbSegment) {
		this.tbSegment = tbSegment;
	}
	public TextBox getTbReferenceNumber() {
		return tbReferenceNumber;
	}

	public void setTbReferenceNumber(TextBox tbReferenceNumber) {
		this.tbReferenceNumber = tbReferenceNumber;
	}

	public DoubleBox getDbBalanceRevenue() {
		return dbBalanceRevenue;
	}

	public void setDbBalanceRevenue(DoubleBox dbBalanceRevenue) {
		this.dbBalanceRevenue = dbBalanceRevenue;
	}

	public DoubleBox getDbBalanceTax() {
		return dbBalanceTax;
	}

	public void setDbBalanceTax(DoubleBox dbBalanceTax) {
		this.dbBalanceTax = dbBalanceTax;
	}

	public DoubleBox getDbReceivedRevenue() {
		return dbReceivedRevenue;
	}

	public void setDbReceivedRevenue(DoubleBox dbReceivedRevenue) {
		this.dbReceivedRevenue = dbReceivedRevenue;
	}

	public DoubleBox getDbReceivedTax() {
		return dbReceivedTax;
	}

	public void setDbReceivedTax(DoubleBox dbReceivedTax) {
		this.dbReceivedTax = dbReceivedTax;
	}

	public DateBox getDbFollowUpDate() {
		return dbFollowUpDate;
	}

	public void setDbFollowUpDate(DateBox dbFollowUpDate) {
		this.dbFollowUpDate = dbFollowUpDate;
	}
	
	

//	/**
//	 * @author Vijay Date :- 14-10-2020
//	 * Des :- Payment upload process bar button should be visible at new Mode
//	 */
//	@Override
//	public void setToNewState() {
//		// TODO Auto-generated method stub
//		super.setToNewState();
//		if(processLevelBar!=null){
//			if(processLevelBar!=null)
//			    processLevelBar.setVisibleFalse(true);
//		}
//		
//		CustomerPayment entity=(CustomerPayment) presenter.getModel();
//		String status=entity.getStatus();
//		Console.log("status"+status);
//		for(int i=0;i<getProcesslevelBarNames().length;i++)
//		{
//			InlineLabel label=getProcessLevelBar().btnLabels[i];
//			String text=label.getText().trim();
//				
//			if(status.equals(CustomerPayment.CREATED))
//			{
//				if(text.equals(AppConstants.SUBMIT)){
//					label.setVisible(false);
//				}
//				if(text.equals(AppConstants.CANCELPAYMENT))
//				{
//					label.setVisible(false);
//				}
//				if(text.equals(AppConstants.CANCELPAYMENT))
//				{
//					label.setVisible(false);
//				}
//				/**
//				 * Date : 10-10-2017 BY ANIL
//				 */
//				if(text.equals(AppConstants.VIEWINVOICE))
//				{
//					label.setVisible(false);
//				}
//				/**
//				 * End
//				 */
//			
//				if(text.equals("Update TDS")){
//					label.setVisible(false);
//				}
//				
//				if(text.equals(AppConstants.PAYMENTUPLOAD))
//				{
//					label.setVisible(true);
//				}
//				if(text.equals(AppConstants.TALLYPAYMENTUPLOAD))
//				{
//					label.setVisible(true);
//				}
//				
//			}
//		}	
//			
//		
//	}

	@Override
	public void refreshTableData() {
		// TODO Auto-generated method stub
		super.refreshTableData();
		billingDoc.getTable().redraw();
	}

	@Override
	public void onValueChange(ValueChangeEvent<Date> event) {

		if(event.getSource().equals(dbtransferDate)){
			if(dbtransferDate.getValue()!=null && AppUtility.checkForProcessConfigurartionIsActiveOrNot("CustomerPayment", AppConstants.PC_SETPAYMENTRECEIVEDATETOPAYMENTDUEDATE)){
				dbpaymentdate.setValue(dbtransferDate.getValue());
			}
		}
	}
	
	
	
	
}
