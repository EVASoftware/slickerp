package com.slicktechnologies.client.views.paymentinfo.invoicelist;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCGlassPanel;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.view.client.NoSelectionModel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreen;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreenPresenter;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.UpdateService;
import com.slicktechnologies.client.services.UpdateServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.accountinginterface.IntegrateSyncService;
import com.slicktechnologies.client.views.accountinginterface.IntegrateSyncServiceAsync;
import com.slicktechnologies.client.views.contract.ContractService;
import com.slicktechnologies.client.views.contract.ContractServiceAsync;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.FromAndToDateBoxPopup;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.SalesOrderProductTable;
import com.slicktechnologies.client.views.paymentinfo.billinglist.ProductDetailsPopup;
import com.slicktechnologies.client.views.paymentinfo.invoicedetails.InvoiceDetailsForm;
import com.slicktechnologies.client.views.paymentinfo.invoicedetails.InvoiceDetailsPresenter;
import com.slicktechnologies.client.views.paymentinfo.invoicedetails.InvoiceDetailsSearchPopUp;
import com.slicktechnologies.client.views.paymentinfo.invoicedetails.SalesRegisterPopUp;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

public class InvoiceListPresenter extends TableScreenPresenter<Invoice> {
	
	TableScreen<Invoice>form;
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	final ContractServiceAsync async=GWT.create(ContractService.class);
	static GWTCGlassPanel glassPanel=new GWTCGlassPanel();
	
	/*
	 *  nidhi
	 *  	1-07-2017
	 */
	FromAndToDateBoxPopup frmAndToPop = new FromAndToDateBoxPopup();
	PopupPanel panel ;
	/*
	 *  end
	 */
	/*
	 *  nidhi
	 *  	10-07-2017
	 */
		Boolean flagforrepot ;
	/*
	 *  end
	 */
		/*
		 * Added by Sheetal : 25-10-2021
		 */
//		GeneralViewDocumentPopup genralInvoicePopup;
		public static PopupPanel generalPanel;
		/*
		 * End
		 */
		/**24-10-2017 sagar sore [ For adding sales register popup]**/
		SalesRegisterPopUp salesRegisterPopUp;
		/**
		 * 	Created By : nidhi
		 *  Date : 18-11-2017
		 *  Discription : add for download fumigation value and contract revanue details 
		 *  
		 *  
		 */
		UpdateServiceAsync updateSer = GWT.create(UpdateService.class);
		/**
		 *  nidhi
		 *  28-01-2018
		 *  for fumigation report opt popup
		 *  
		 * @param view
		 * @param model
		 */
		FumigationOptPopup fumOptPopup;
		boolean serviceRevanue = false;
		/**
		 * end
		 * @param view
		 * @param model
		 */
		
		 final NoSelectionModel<Invoice> selectionModelMyObj = new NoSelectionModel<Invoice>();
		 GeneralViewDocumentPopup genralInvoicePopup = new GeneralViewDocumentPopup();
		 GenricServiceAsync service=GWT.create(GenricService.class);
		 public static boolean viewDocumentFlag=true;
		 boolean bulkEinvoiceflag=false;
		 
		 PopupPanel productDetailsPanel;
		 ProductDetailsPopup productPopup=new ProductDetailsPopup();
		 public static boolean productdetailFlag=false;//Ashwini Patil Date:1-07-2024
		 Invoice invoiceObj=null;
		 
	public InvoiceListPresenter(TableScreen<Invoice> view, Invoice model) {
		super(view, model);
		form=view;
		/**
		 *  nidhi
		 *  24-07-2017
		 *  this method stop calling for not display invoice list on page load
		 *  
		 */
		//view.retriveTable(getInvoiceListQuery());
		
		/**
		 *  end
		 */
		setTableSelectionOnService();
		
		/*
		 *  nidhi
		 *  	1-07-2017
		 */
		
		frmAndToPop.btnOne.addClickHandler(this);
		frmAndToPop.btnTwo.addClickHandler(this);
		/*
		 *  end
		 */
		/**
		 * nidhi
		 * 
		 */
		fumOptPopup = new FumigationOptPopup();
		fumOptPopup.getLblOk().addClickHandler(this);
		fumOptPopup.getLblCancel().addClickHandler(this);
		/**
		 * end
		 */
		
		genralInvoicePopup.getBtnClose().addClickHandler(this);
		
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.INVOICELIST,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
		productPopup.getBtnCancel().addClickHandler(this);
	}
	
	public InvoiceListPresenter(TableScreen<Invoice> view,
			Invoice model,MyQuerry querry) {
		super(view, model);
		form=view;
		view.retriveTable(querry);
		
		setTableSelectionOnService();
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.INVOICELIST,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
		
		genralInvoicePopup.getBtnClose().addClickHandler(this);
		productPopup.getBtnCancel().addClickHandler(this);
	}

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void reactOnDownload() {
		
		final ArrayList<Invoice> invoicingarray=new ArrayList<Invoice>();
		List<Invoice> listbill=(List<Invoice>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
		invoicingarray.addAll(listbill); 
		csvservice.setinvoicelist(invoicingarray, new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);
				
			}

			@Override
			public void onSuccess(Void result) {
				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "csvservlet"+"?type="+15;
				Window.open(url, "test", "enabled");
				
//				final String url=gwt + "CreateXLXSServlet"+"?type="+15;
//				Window.open(url, "test", "enabled");
				
				
//				ArrayList<SuperModel> superlist = new ArrayList<SuperModel>();
//				superlist.addAll(invoicingarray);
//				csvservice.getData(superlist, AppConstants.INVOICEDETAILS, new AsyncCallback<ArrayList<SuperModel>>() {
//
//					@Override
//					public void onFailure(Throwable caught) {
//						// TODO Auto-generated method stub
//						
//					}
//
//					@Override
//					public void onSuccess(ArrayList<SuperModel> result) {
//						// TODO Auto-generated method stub
//						ArrayList<Customer> customerlist = new ArrayList<Customer>();
//						for(SuperModel model : result){
//							Customer customer = (Customer) model;
//							customerlist.add(customer);
//						}
//						
//						ArrayList<String> invoicelist = new ArrayList<String>();
//						invoicelist.add("INVOICE ID");
//						invoicelist.add("INVOICE DATE");
//						invoicelist.add("INVOICE GROUP");
//						invoicelist.add("INVOICE DOCUMENT TYPE");
//						invoicelist.add("INVOICE CATEGORY");
//						invoicelist.add("INVOICE TYPE");
//						invoicelist.add("BP ID");
//						invoicelist.add("BP NAME");
//						invoicelist.add("Customer Branch");
//						invoicelist.add("CELL NUMBER");
//						
//						/**Date 11-9-2019 by Amol for ultrapest **/
//						invoicelist.add("EMAIL ID");
//						invoicelist.add("POC NAME");
//						invoicelist.add("REF NUMBER");
//						invoicelist.add("ORDER ID");
//						invoicelist.add("ORDER START DATE");
//						invoicelist.add("ORDER END DATE");
//						
//						/**@Sheetal : 02-03-2022
//						 *  Des  : Adding Billing Period Start date and End date, requirement by pest-o-sheild**/
//						invoicelist.add("BILLING PERIOD START DATE");
//						invoicelist.add("BILLING PERIOD END DATE");
//						/**end**/
//						/***Date 6-5-2020 by Amol added  invoice refno and reference Date***/
//						invoicelist.add("INVOICE REF. NO.");
//						invoicelist.add("REFERENCE DATE");
//						invoicelist.add("ACCOUNT TYPE");
//						invoicelist.add("ORDER AMOUNT");
//						invoicelist.add("PAYMENT DATE");
//						invoicelist.add("APPROVER NAME");
//						//***********************rohan remove this as per ultra pest control requirement ******
//						invoicelist.add("PRODUCT ID");
//						invoicelist.add("PRODUCT CODE");
//						invoicelist.add("PRODUCT NAME");
//						invoicelist.add("PRODUCT QUANTITY");
//						invoicelist.add("PRODUCT UOM");
//						invoicelist.add("PRODUCT HSN CODE");
//						invoicelist.add("PRODUCT RATE");
//						invoicelist.add("GROSS VALUE");
//						invoicelist.add("OTHER CHARGES");
//						invoicelist.add("TAX 1 DETAILS");
//						invoicelist.add("TAX 1 %");
//						invoicelist.add("TAX 1 AMOUNT");
//						invoicelist.add("TAX 2 DETAILS");
//						invoicelist.add("TAX 2 %");
//						invoicelist.add("TAX 2 AMOUNT");
//						invoicelist.add("SWACHH BHARAT CESS %");
//						invoicelist.add("TAX AMOUNT");
//						invoicelist.add("KRISHI KALYAN CESS %");
//						invoicelist.add("TAX AMOUNT");
//						invoicelist.add("PRODUCT FLAT DISCOUNT AMOUNT");
//						invoicelist.add("PRODUCT TOTAL AMOUNT");
//						invoicelist.add("TOTAL AMOUNT");
//						invoicelist.add("DISCOUNT AMOUNT");
//						invoicelist.add("AFTER DISCOUNT TOTAL AMOUNT");
//						invoicelist.add("AFTER TAXES TOTAL AMOUNT");
//						invoicelist.add("TOTAL BILLING AMOUNT");
//						invoicelist.add("ROUND OFF");
//						invoicelist.add("INVOICE AMOUNT");
//						invoicelist.add("STATUS");
//						invoicelist.add("SALES PERSON");
//						invoicelist.add("SEGMENT");
//						invoicelist.add("SERVICE ID");
//						invoicelist.add("GSTIN NUMBER");
//						invoicelist.add("QUANTITY");
//						invoicelist.add("UOM");
//						/**
//						 *  NIdhi
//						 *  24-07-2017
//						 */
//						invoicelist.add("REFERANCE NO.");
//						/**
//						 *  end
//						 */
//						/**
//						 * Rahul Verma Added these reference details generated by Interface
//						 */
//						invoicelist.add("Reference Invoice ID");
//						invoicelist.add("Reference Net Value");
//						invoicelist.add("Reference Tax Amount");
//						invoicelist.add("Reference Total Amount");
//						/**
//						 * ENDS for Rahul Verma
//						 */
//						invoicelist.add("Contract Status");
//					
//						int columnCount = 65;
//						
//						NumberFormat df=NumberFormat.getFormat("0.00");
//
//						for(Invoice invoice:invoicingarray)
//						{
//							int noofproduct=0;
//							
//							for(int d=0;d<invoice.getSalesOrderProducts().size();d++)
//							{
//								invoicelist.add(invoice.getCount()+"");
//
//								if(invoice.getInvoiceDate()!=null)
//									invoicelist.add(AppUtility.parseDate(invoice.getInvoiceDate(),"dd-MMM-yyyy"));
//								if(invoice.getInvoiceGroup()!=null){
//									invoicelist.add(invoice.getInvoiceGroup());
//								}
//								else{
//									invoicelist.add("");
//								}
//								invoicelist.add(invoice.getInvoiceType());
//								
//								if(invoice.getInvoiceCategory()!=null){
//									invoicelist.add(invoice.getInvoiceCategory());
//								}
//								else{
//									invoicelist.add("");
//								}
//								if(invoice.getInvoiceConfigType()!=null){
//									invoicelist.add(invoice.getInvoiceConfigType());
//								}
//								else{
//									invoicelist.add("");
//								}
//								invoicelist.add(invoice.getPersonInfo().getCount()+"");
//								/**Date 26-3-2020 by Amol to remove comma from string for om Pest***/
//								invoicelist.add(invoice.getPersonInfo().getFullName());
//								/***14-12-2019 Deepak Salve added this code for Customer Branch Download ***/
//								if(invoice.getCustomerBranch()!=null){
//									invoicelist.add(invoice.getCustomerBranch());
//								}else{
//									invoicelist.add("");
//								}
//								/***End***/
//								invoicelist.add(invoice.getPersonInfo().getCellNumber()+"");
//
//								if(invoice.getPersonInfo().getEmail()!=null){
//									invoicelist.add(invoice.getPersonInfo().getEmail());
//								}else{
//									invoicelist.add("");
//								}
//								if(invoice.getPersonInfo().getPocName()!=null){
//									invoicelist.add(invoice.getPersonInfo().getPocName());
//								}
//								else{
//									invoicelist.add("");
//								}
//								if(invoice.getRefNumber()!=null)
//								{
//									invoicelist.add(invoice.getRefNumber());
//								}
//								else
//								{
//									invoicelist.add("N.A");
//								}
//								invoicelist.add(invoice.getContractCount()+"");
//								if(invoice.getContractStartDate()!=null){
//									invoicelist.add(AppUtility.parseDate(invoice.getContractStartDate(),"dd-MMM-yyyy"));
//								}
//								else{
//									invoicelist.add("N.A");
//								}
//								if(invoice.getContractEndDate()!=null){
//									invoicelist.add(AppUtility.parseDate(invoice.getContractEndDate(),"dd-MMM-yyyy"));
//								}
//								else{
//									invoicelist.add("N.A");
//								}
//								/**@Sheetal : 02-03-2022 
//								 * Des  : Adding Billing Period Start date and End date, requirement by pest-o-sheild**/
//								if(invoice.getBillingPeroidFromDate()!=null){
//									invoicelist.add(AppUtility.parseDate(invoice.getBillingPeroidFromDate(),"dd-MMM-yyyy"));
//								}
//								else{
//									invoicelist.add("N.A");
//								}
//								if(invoice.getBillingPeroidToDate()!=null){
//									invoicelist.add(AppUtility.parseDate(invoice.getBillingPeroidToDate(),"dd-MMM-yyyy"));
//								}
//								else{
//									invoicelist.add("N.A");
//								}
//								/**
//								 * @author Priyanka Bhagwat Date : 04-02-2021
//								 * Downloaded excel data mismatch issue raised by Ashwini for ultra pest control.
//								 */
//								
//								if(invoice.getInvRefNumber()!=null){
//									invoicelist.add(invoice.getInvRefNumber());
//								}
//								else{
//									invoicelist.add("N.A");
//								}
//								if(invoice.getReferenceDate()!=null){
//									invoicelist.add(AppUtility.parseDate(invoice.getReferenceDate(),"dd-MMM-yyyy"));
//								}
//								else{
//									invoicelist.add("N.A");
//								}
//								/**
//								 *  End
//								 */
//								invoicelist.add(invoice.getAccountType());
//								invoicelist.add(invoice.getTotalSalesAmount()+"");
//
//								if(invoice.getPaymentDate()!=null)
//									invoicelist.add(AppUtility.parseDate(invoice.getPaymentDate(),"dd-MMM-yyyy"));
//								if(invoice.getApproverName()!=null)
//									invoicelist.add(invoice.getApproverName());
//
//								invoicelist.add(invoice.getSalesOrderProducts().get(d).getProdId()+"");
//								invoicelist.add(invoice.getSalesOrderProducts().get(d).getProdCode()+"");
//								invoicelist.add(invoice.getSalesOrderProducts().get(d).getProdName()+"");
//								invoicelist.add(invoice.getSalesOrderProducts().get(d).getQuantity()+"");
//
//								if(invoice.getSalesOrderProducts().get(d).getUnitOfMeasurement()!=null){
//									invoicelist.add(invoice.getSalesOrderProducts().get(d).getUnitOfMeasurement()+"");
//
//								}else{
//									invoicelist.add("");
//								}
//								
//								if(invoice.getSalesOrderProducts().get(d).getHsnCode()!=null){
//									invoicelist.add(invoice.getSalesOrderProducts().get(d).getHsnCode()+"");
//								}else{
//									invoicelist.add("");
//								}
//								
//								if(invoice.getSalesOrderProducts().get(d).getPrice()!=0){
//									invoicelist.add(invoice.getSalesOrderProducts().get(d).getPrice()+"");
//								}else{
//									invoicelist.add("");
//								}
//								
//					//************************rohan added here for tax field in download(ultra changes ) 
//								//*************calculation for tax amt ******************8
//								double convertval=Double.parseDouble(invoice.getSalesOrderProducts().get(d).getPaymentPercent()+"");
//								System.out.println("convertval==="+convertval);
//								double taxValue=Double.parseDouble(invoice.getSalesOrderProducts().get(d).getServiceTax().getPercentage()+"");
//								System.out.println("taxValue===="+taxValue);
//								double grossValueAsPerTaxpercent=0;
//								if(convertval==100){
//									System.out.println("tax %====="+invoice.getTaxPercent());
//									System.out.println("invoicelist.getSalesOrderProducts().get(d).getBaseBillingAmount()"+invoice.getSalesOrderProducts().get(d).getBaseBillingAmount());
//								 grossValueAsPerTaxpercent=(invoice.getSalesOrderProducts().get(d).getBaseBillingAmount());
//								}
//								else{
//									grossValueAsPerTaxpercent=(invoice.getSalesOrderProducts().get(d).getBaseBillingAmount()*convertval)/100;
//								}
//								
//								if(grossValueAsPerTaxpercent!=0){
//									invoicelist.add(grossValueAsPerTaxpercent+"");
//								}
//								else{
//									invoicelist.add("");
//								}
//								
//								double otherchargesTaxvalue=0.0;
//								double totalTax1=0.0;
//								double totalTax2=0.0;
//								if(invoice.getOtherCharges()!=null)
//								{	
//								ArrayList<OtherCharges>	 othercharge=new ArrayList<OtherCharges>();
//								double value=0;
//								double tax1=0.0;
//								double tax2=0.0;
//								othercharge=invoice.getOtherCharges();
//								for(OtherCharges otherCharge:othercharge){
////									writer.append(otherCharge.getAmount()+"");
//									if(noofproduct==0){
//									value+=otherCharge.getAmount();
//									}
//									if(otherCharge.getTax1().getPercentage()!=0){
//										tax1=otherCharge.getTax1().getPercentage();
//									}
//									if(otherCharge.getTax1().getPercentage()!=0){
//										tax2=otherCharge.getTax2().getPercentage();
//									}
//									System.out.println(" Other Charges Tax Amount :-"+otherCharge.getTax1().getPercentage());
//								}
//								double totalTax=tax1+tax2;
//								System.out.println(" Total Tax :-"+totalTax);
//								otherchargesTaxvalue=value*totalTax/100;
//								totalTax1=value*tax1/100;
//								totalTax2=value*tax2/100;
//								System.out.println(" Total Tax After Cal:-"+otherchargesTaxvalue+" Value "+value);
//								otherchargesTaxvalue=value-otherchargesTaxvalue;
//								System.out.println(" Total Tax :-"+totalTax);
//								System.out.println(" Other Charges Tax Amount 2 :-"+otherchargesTaxvalue);
//								invoicelist.add(value+"");
//								}else{
//									invoicelist.add(0+"");
//								}
//								/**
//								 * this code is for vat tax
//								 */
//								double vattaxAmount =0;
//								if(invoice.getSalesOrderProducts().get(d).getVatTax().getPercentage()!=0)
//								{
//									double taxVa = invoice.getSalesOrderProducts().get(d).getVatTax().getPercentage();
//									double vattaxAmt  = grossValueAsPerTaxpercent*taxVa /100;
//									
//									vattaxAmount = vattaxAmt+otherchargesTaxvalue;
//									//  new codeby rohan for gst 
//									if(invoice.getSalesOrderProducts().get(d).getVatTax().getTaxPrintName()!=null
//											&& !invoice.getSalesOrderProducts().get(d).getVatTax().equals("")){
//										invoicelist.add(invoice.getSalesOrderProducts().get(d).getVatTax().getTaxPrintName());
//									}
//									else{
//										invoicelist.add(invoice.getSalesOrderProducts().get(d).getVatTax().getTaxConfigName());
//									}
//									invoicelist.add(taxVa+"");
//									invoicelist.add((vattaxAmt+totalTax1)+"");
//								}
//								else
//								{
//									invoicelist.add("NA");
//									invoicelist.add("NA");
//									invoicelist.add("NA");
//
//								}
//								
//								
//								/**
//								 * this code is for serrvice tax 
//								 */
//								if(taxValue!=0)
//								{
//									if(invoice.getSalesOrderProducts().get(d).getServiceTax().getTaxPrintName()!=null
//											&& !invoice.getSalesOrderProducts().get(d).getServiceTax().equals(""))
//									{
//										double taxAmt  =grossValueAsPerTaxpercent*taxValue /100;
//										
//										double productTotalAmt = grossValueAsPerTaxpercent + vattaxAmount + taxAmt;
//
//										invoicelist.add(invoice.getSalesOrderProducts().get(d).getServiceTax().getTaxPrintName());
//										invoicelist.add(taxValue+"");
//										invoicelist.add((taxAmt+totalTax1)+"");
//										invoicelist.add("NA");
//										invoicelist.add("NA");
//										invoicelist.add("NA");
//										invoicelist.add("NA");
//										invoicelist.add(invoice.getSalesOrderProducts().get(d).getFlatDiscount()+"");
//										invoicelist.add(productTotalAmt+"");
//									}
//									else{
//										
//										double taxAmt123=0;
//										
//										double taxAmt= grossValueAsPerTaxpercent*taxValue;
//										taxAmt123=taxAmt/100;
//										invoicelist.add(invoice.getSalesOrderProducts().get(d).getServiceTax().getTaxConfigName());
//										
//										if(taxValue==15)
//										{
//											double cess = 0; 
//											double kks = 0; 
//											double taxVal=0;
//											taxVal =taxValue-1;
//											cess = 0.5; 
//											kks =0.5;
//											
//											double taxAmt1= (grossValueAsPerTaxpercent*taxVal)/100;
//											double cessVal= (grossValueAsPerTaxpercent*cess)/100;
//											double kksVal= (grossValueAsPerTaxpercent*kks)/100;
//											
//											invoicelist.add(df.format(taxVal));
//											invoicelist.add(df.format(taxAmt1));
//											invoicelist.add(cess+"");
//											invoicelist.add(df.format(cessVal));
//											invoicelist.add(kks+"");
//											invoicelist.add(df.format(kksVal));
//											invoicelist.add(invoice.getSalesOrderProducts().get(d).getFlatDiscount()+"");
//											invoicelist.add("NA");
//
//										}
//										else if(taxValue==14.5)
//										{
//											
//											double cess = 0; 
//											double taxVal=0;
//											taxVal =taxValue-0.5;
//											cess = 0.5; 
//											
//											double taxAmt1= (grossValueAsPerTaxpercent*taxVal)/100;
//											double cessVal= (grossValueAsPerTaxpercent*cess)/100;
//											
//											invoicelist.add(df.format(taxVal));
//											invoicelist.add(df.format(taxAmt1));
//											invoicelist.add(cess+"");
//											invoicelist.add(df.format(cessVal));
//											invoicelist.add("NA");
//											invoicelist.add("NA");
//											invoicelist.add(invoice.getSalesOrderProducts().get(d).getFlatDiscount()+"");
//											invoicelist.add("NA");
//
//										}
//										else
//										{
//											
//											invoicelist.add(invoice.getSalesOrderProducts().get(d).getServiceTax().getPercentage()+"");
//											invoicelist.add(df.format(taxAmt123));
//											invoicelist.add("NA");
//											invoicelist.add("NA");
//											invoicelist.add("NA");
//											invoicelist.add("NA");
//											invoicelist.add(invoice.getSalesOrderProducts().get(d).getFlatDiscount()+"");
//											invoicelist.add("NA");
//											
//										}
//									}
//								}
//								else
//								{
//									
//									invoicelist.add("NA");
//									invoicelist.add("NA");
//									invoicelist.add("NA");
//									invoicelist.add("NA");
//									invoicelist.add("NA");
//									invoicelist.add("NA");
//									invoicelist.add("NA");
//									invoicelist.add(invoice.getSalesOrderProducts().get(d).getFlatDiscount()+"");
//									invoicelist.add("NA");
//								}
//								
//								/*** Date 11-09-2017 added by vijay for discount amt round off  ***********/
//
//								invoicelist.add(invoice.getTotalAmtExcludingTax()+"");
//								invoicelist.add(invoice.getDiscountAmt()+"");
//								invoicelist.add(invoice.getFinalTotalAmt()+"");
//								invoicelist.add(invoice.getTotalAmtIncludingTax()+"");
//								invoicelist.add(invoice.getTotalBillingAmount()+"");
//								invoicelist.add(invoice.getDiscount()+"");
//								invoicelist.add(invoice.getInvoiceAmount()+"");
//								invoicelist.add(invoice.getStatus()+"");
//								invoicelist.add(invoice.getEmployee()+"");
//
//							
//								/**
//								 * Date : 11-07-2017 Nidhi
//								 */
//								if(invoice.getSegment()!=null){
//									invoicelist.add(invoice.getSegment());
//								}
//								else{
//									invoicelist.add("");
//								}
//								/**
//								 * End
//								 */
//								
//								/**
//								 * Date : 17-07-2017 Nidhi
//								 */
//								if(invoice.getRateContractServiceId()!=0){
//									invoicelist.add(invoice.getRateContractServiceId()+"");
//								}
//								else{
//									invoicelist.add("");
//								}
//								/**
//								 * End
//								 */
//								
//								/**
//								 * Date : 17-07-2017 Nidhi
//								 */
//								if(invoice.getGstinNumber()!=null && !invoice.getGstinNumber().equals("") ){
//									invoicelist.add(invoice.getGstinNumber());
//								}
//								else{
//									String gstnumber = getcustomerGSTInNumber(invoice.getPersonInfo().getCount(), customerlist);
//									invoicelist.add(gstnumber);
//
//								}
//								/**
//								 * End
//								 */
//								/*
//								 *  NIDHI
//								 *  20-07-2017
//								 *  
//								 */
//								if (invoice.getQuantity() != 0) {
//									invoicelist.add(invoice.getQuantity()+"");
//
//								} else {
//									invoicelist.add("");
//								}
//								if (invoice.getUom() != null) {
//									invoicelist.add(invoice.getQuantity()+"");
//								} else {
//									invoicelist.add("");
//								}
//								/*
//								 *  end
//								 */
//								
//								/**
//								 *  nidhi
//								 *  24-07-2017
//								 *  
//								 */
//								if (invoice.getRefNumber() != null) {
//									invoicelist.add(invoice.getRefNumber());
//								} else {
//									invoicelist.add("");
//								}
//								
//								/*
//								 *  end
//								 */
//								/**
//								 * Rahul Verma Added this code for Invoice Download
//								 */
//								if (invoice.getSapInvoiceId() != null) {
//									invoicelist.add(invoice.getSapInvoiceId());
//								} else {
//									invoicelist.add("");
//								}
//								
//								if (invoice.getSAPNetValue() != 0) {
//									invoicelist.add(invoice.getSAPNetValue()+"");
//								} else {
//									invoicelist.add("");
//								}
//								if (invoice.getSAPTaxAmount() != 0) {
//									invoicelist.add(invoice.getSAPTaxAmount()+"");
//
//								} else {
//									invoicelist.add("");
//								}
//								
//								if (invoice.getSAPTotalAmount() != 0) {
//									invoicelist.add(invoice.getSAPTotalAmount()+"");
//								} else {
//									invoicelist.add("");
//								}
//								/**
//								 * End
//								 */
//								/**
//								 * nidhi
//								 */
//								if(invoice.isRenewContractFlag()){
//									invoicelist.add("Renew");
//								}else{
//									invoicelist.add("new");
//								}
//								
//								noofproduct++;
//								}
//							}
//					
//						
//						csvservice.setExcelData(invoicelist, columnCount, AppConstants.INVOICEDETAILS, new AsyncCallback<String>() {
//							
//							@Override
//							public void onSuccess(String result) {
//								// TODO Auto-generated method stub
//								
//								String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
//								final String url=gwt + "CreateXLXSServlet"+"?type="+15;
//								Window.open(url, "test", "enabled");
//							}
//							
//							@Override
//							public void onFailure(Throwable caught) {
//								// TODO Auto-generated method stub
//								
//							}
//						});
//						
//
//					}
//
//					
//				});
				
			}
		});
	}

	
	/*
	 * Method template to make a new model
	 */
	@Override
	protected void makeNewModel() {
		
		model=new Invoice();
	}
	
	public static void initalize()
	{
		
//		glassPanel.show();
//		Timer timer=new Timer() 
//   	 	{
//			@Override
//			public void run() 
//			{
				InvoiceListTable table=new InvoiceListTable();
				InvoiceListForm form=new InvoiceListForm(table);
				//InvoiceDetailsForm billingDtlsForm=new InvoiceDetailsForm();
				InvoiceDetailsSearchPopUp.staticSuperTable=table;
				InvoiceDetailsSearchPopUp searchPopUp=new InvoiceDetailsSearchPopUp(false);
				searchPopUp.getDwnload().setVisible(false);
				form.setSearchpopupscreen(searchPopUp);
				/**Added by sheetal:13-01-2022
				 * Invoice list window to occupy full screen
				 */
				int height = Window.getClientHeight();
				Console.log("Window Height : " + height);
				height = height * 80 / 100;
				Console.log("Updated Height : " + height);
				searchPopUp.getSupertable().getTable().setHeight(height+"px");
				/**end**/
				InvoiceListPresenter presenter=new InvoiceListPresenter(form, new Invoice());
				form.setToViewState();
				/**
				 * Date : 29-07-2017 BY ANIL
				 * This line was added to update the name of screen loaded from any other screen or by clicking on billing details on account model
				 */
				AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Accounts/Invoice List",Screen.INVOICELIST);
				/**
				 * END
				 */
				AppMemory.getAppMemory().stickPnel(form);
//				glassPanel.hide();
//			}
//		};
//		timer.schedule(3000); 
	}
	
	public static void initalize(MyQuerry querry)
	{
		//AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Invoice Details",Screen.INVOICELIST);
		InvoiceListTable table=new InvoiceListTable();
		InvoiceListForm form=new InvoiceListForm(table);
		InvoiceListSearchPopUp.staticSuperTable=table;
		InvoiceListSearchPopUp searchPopUp=new InvoiceListSearchPopUp(false);
		form.setSearchpopupscreen(searchPopUp);
		/**Added by sheetal:13-01-2022
		 * Invoice list window to occupy full screen
		 */
		int height = Window.getClientHeight();
		Console.log("Window Height : " + height);
		height = height * 80 / 100;
		Console.log("Updated Height : " + height);
		searchPopUp.getSupertable().getTable().setHeight(height+"px");
		/**end**/
		InvoiceListPresenter presenter=new InvoiceListPresenter(form, new Invoice(),querry);
		form.setToViewState();
		/**
		 * Date : 29-07-2017 BY ANIL
		 * This line was added to update the name of screen loaded from any other screen or by clicking on billing details on account model
		 */
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Accounts/Invoice List",Screen.INVOICELIST);
		/**
		 * END
		 */
		AppMemory.getAppMemory().stickPnel(form);
	}
	
	/*
	 * Method template to set the query object in specific to search criteria and return the same
	 * @return MyQuerry object
	 */
	public MyQuerry getInvoiceListQuery()
	{
		MyQuerry quer=new MyQuerry();
		
		Vector<Filter> filtervec=new Vector<Filter>();
		List<String> statusList=new ArrayList<String>();
		statusList.add(Invoice.CREATED);
		statusList.add(Invoice.REQUESTED);
		
		Filter temp=null;
		
		Date invDate=new Date();
		CalendarUtil.addDaysToDate(invDate, 1);
		
		temp=new Filter();
		temp.setLongValue(model.getCompanyId());
		temp.setQuerryString("companyId");
		filtervec.add(temp);
		
		temp=new Filter();
		temp.setList(statusList);
		temp.setQuerryString("status IN");
		filtervec.add(temp);
		
		
		/**
		 * Developed by : Rohan added this branch  
		 * 
		 */
		
		List<String> branchList ;
		branchList= new ArrayList<String>();
		System.out.println("item count in branch "+LoginPresenter.globalBranch.size());
		for (int i = 0; i < LoginPresenter.globalBranch.size(); i++) {
			branchList.add(LoginPresenter.globalBranch.get(i).getBusinessUnitName());
		}
		System.out.println("branchList list "+branchList.size());
		
		temp = new Filter();
		temp.setQuerryString("branch IN");
		temp.setList(branchList);
		filtervec.add(temp);
		/**
		 * ends here 
		 */
		
		temp=new Filter();
		temp.setDateValue(invDate);
		temp.setQuerryString("invoiceDate <=");
		filtervec.add(temp);
		
		quer.setFilters(filtervec);
		quer.setQuerryObject(new Invoice());
		
		
		return quer;
	}
	
	 public void setTableSelectionOnService()
	 {
//		 final NoSelectionModel<Invoice> selectionModelMyObj = new NoSelectionModel<Invoice>();
	     
	     SelectionChangeEvent.Handler  tableHandler = new SelectionChangeEvent.Handler() 
	     {
	         @Override
	         public void onSelectionChange(SelectionChangeEvent event) 
	         {
//        	 final Invoice entity=selectionModelMyObj.getLastSelectedObject();
//	        	 AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
//	        	 InvoiceDetailsForm.flag=false;
//	        	
//        		
//	        	 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Invoice Details",Screen.INVOICEDETAILS);
//				 AppMemory.getAppMemory().stickPnel(form);
//				 
//	        	 final InvoiceDetailsForm form = InvoiceDetailsPresenter.initalize();
//
//				 Timer timer=new Timer() 
//	        	 {
//	 				@Override
//	 				public void run() {
//	 					System.out.println("Rohan in side invoice list ");
//					     form.updateView(entity);
//					     form.setToViewState();
//					     InvoiceDetailsPresenter presenter=(InvoiceDetailsPresenter) form.getPresenter();
//					     if(!entity.getStatus().equals(Invoice.CREATED)){
//					    	 presenter.form.setAppHeaderBarAsPerStatus();
//					     }
//	 			             
//	 				}
//	 			};
	            
//	             timer.schedule(3000); 
	        	 
	        	 /*
	        	  * Added by Sheetal : 25-10-2021 for opening popup window 
	        	  */
	        	 Console.log("in table selection viewdocumentflag="+viewDocumentFlag);
	        	 if(viewDocumentFlag){
	        	    Invoice invoiceEntity = selectionModelMyObj.getLastSelectedObject();
	        	    System.out.println("invoiceEntity.getSalesOrderProducts().size()"+invoiceEntity.getSalesOrderProducts().size());
//			        AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Accounts/Invoice Details",Screen.INVOICEDETAILS);
					AppMemory.getAppMemory().currentScreen = Screen.INVOICEDETAILS;

//					genralInvoicePopup = new GeneralViewDocumentPopup();
					AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Invoice Details",Screen.INVOICEDETAILS);
					genralInvoicePopup.setModel(AppConstants.PROCESSCONFIGINV,invoiceEntity);
					generalPanel = new PopupPanel();
					generalPanel.add(genralInvoicePopup);
					generalPanel.show();
					generalPanel.center();
	        	 }else {
	        		 	if(productdetailFlag)
						{
							
							productdetailFlag=false;
							final Invoice entity = selectionModelMyObj.getLastSelectedObject();
							invoiceObj=entity;
							showProductDetails(entity);									
						}
	        		 viewDocumentFlag = true;
	        	 }
	          }
	     };
	     // Add the handler to the selection model
	     selectionModelMyObj.addSelectionChangeHandler( tableHandler );
	     // Add the selection model to the table
	     form.getSuperTable().getTable().setSelectionModel(selectionModelMyObj);
	     
		 
		 
	 }

	
	 @Override
		public void reactToProcessBarEvents(ClickEvent e) {
//			super.reactToProcessBarEvents(e);
			
			InlineLabel label = (InlineLabel) e.getSource();
			String text = label.getText().trim();
			Console.log("in reactToProcessBarEvents text="+text);

			if(text.equals("Invoice Acknowledgment")) {
				final String url = GWT.getModuleBaseURL()+"pdfprintservice"+"?Id="+UserConfiguration.getCompanyId()
						+"&"+"type="+"IA";
				Window.open(url, "test", "enabled");
			}else if(text.equals("Revenue Report")){
				flagforrepot=true;
				panel = new PopupPanel(true);
				panel.add(frmAndToPop);
				panel.show();
				panel.center();
			}else if(text.equals("Technician Revenue Report")){
				flagforrepot=false;
				panel = new PopupPanel(true);
				panel.add(frmAndToPop);
				panel.show();
				panel.center();
			}
			/**24-10-2017 sagar sore [Action for sales register search popup]**/
			if (text.equals(AppConstants.SALESREGISTER))
			{
				salesRegisterPopUp = new SalesRegisterPopUp();
				salesRegisterPopUp.ultimaTallyExportFlag=false;
				salesRegisterPopUp.showPopUp();
				salesRegisterPopUp.getLblCancel().setVisible(false);
				salesRegisterPopUp.getLblOk().setText("Go");
			} 
			/**
			 *  nidhi
			 *  10-11-2017
			 *  for Daily fumigation report 
			 *  
			 */
			if(text.equals(AppConstants.SERVICEFUMIGATIONREPORT)){
//				updateSer.dailyFumigationReportDetails(model.getCompanyId(), null, new AsyncCallback<Void>() {
//					
//					@Override
//					public void onSuccess(Void result) {
//
//						String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
//						final String url=gwt + "CreateXLXSServlet"+"?type="+1;
//						Window.open(url, "test", "enabled");
//					}
//					
//					@Override
//					public void onFailure(Throwable caught) {
//						// TODO Auto-generated method stub
//						
//					}
//				});
				serviceRevanue = false;
				fumOptPopup.showPopUp();
				fumOptPopup.getFgroupingExpenseDetails().getHeaderLabel().setText(AppConstants.SERVICEFUMIGATIONREPORT);
			}
			
			
			/**
			 *  nidhi
			 *  16-11-2017
			 *  for Daily fumigation value report 
			 *  
			 */
			if(text.equals(AppConstants.SERVICEFUMIGATIONVALUEREPORT)){
				
				serviceRevanue = true;
				fumOptPopup.showPopUp();
				fumOptPopup.getFgroupingExpenseDetails().getHeaderLabel().setText(AppConstants.SERVICEFUMIGATIONVALUEREPORT);
			}
			if(text.equals(AppConstants.BulkeInvoiceGeneration)){
				Console.log("in BulkeInvoiceGeneration");
				bulkEinvoiceflag=true;
				ArrayList<Invoice> selectedlist = getAllSelectedRecords();
				if(selectedlist!=null&&selectedlist.size()>0) {
					Console.log("selected invoice list="+selectedlist.size());
					for(Invoice inv:selectedlist) {
						if(!inv.getStatus().equals(Invoice.APPROVED))
						{
							form.showDialogMessage("Please select only Approved Tax Invoices!");
							return;
						}
						if(!inv.getInvoiceType().equals("Tax Invoice")) {
							form.showDialogMessage("Please select only Tax Invoices!");
							return;
						}
					}
					
					IntegrateSyncServiceAsync integrateService=GWT.create(IntegrateSyncService.class);
					integrateService.generateEinvoicesInBulk(model.getCompanyId(), null, null,selectedlist,LoginPresenter.loggedInUser, new  AsyncCallback<String>() {

						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							form.showDialogMessage("Process failed!");
						}

						@Override
						public void onSuccess(String result) {
							// TODO Auto-generated method stub
							form.showDialogMessage("Process has started! Please check after 15 minutes");
						}
					});
				}
				else {
					Console.log("selected invoice list empty");
					panel = new PopupPanel(true);
					panel.add(frmAndToPop);
					panel.show();
					panel.center();
				}
			}
			
			if (text.equals(AppConstants.TallyExportForUltima))
			{
				salesRegisterPopUp = new SalesRegisterPopUp();
				salesRegisterPopUp.ultimaTallyExportFlag=true;
				salesRegisterPopUp.showPopUp();
				salesRegisterPopUp.getLblCancel().setVisible(false);
				salesRegisterPopUp.getLblOk().setText("Go");
			} 
		}
	 
	 @Override
		public void onClick(ClickEvent event) {
		 	super.onClick(event);
		 try {
			 	if(event.getSource()==frmAndToPop.btnOne){
			 		Console.log("ok button clicked on date popup");
			 		if(frmAndToPop.getFromDate().getValue()==null&&frmAndToPop.getToDate().getValue()==null){
			 			form.showDialogMessage("Please select from and to date.");
			 			return;
			 		}
			 		if(frmAndToPop.getFromDate().getValue()==null){
			 			form.showDialogMessage("Please select from date.");
			 			return;
			 		}
			 		if(frmAndToPop.getToDate().getValue()==null){
			 			form.showDialogMessage("Please select to date.");
			 			return;
			 		}
			 		if(frmAndToPop.getToDate().getValue().before(frmAndToPop.getFromDate().getValue())){
			 			form.showDialogMessage("To date should be greater than from date.");
			 			return;
			 		}
			 		form.showWaitSymbol();
			 		Console.log("flagforrepot="+flagforrepot+" bulkEinvoiceflag= "+bulkEinvoiceflag);
			 		if(bulkEinvoiceflag) {

			 			Console.log("in bulk einvoice date popup");
						Date fromdate=null;
						Date todate=null;
						if(frmAndToPop.getFromDate().getValue()!=null&&frmAndToPop.getToDate().getValue()!=null){
							
							Console.log("fromdate="+frmAndToPop.getFromDate().getValue()+" todate="+frmAndToPop.getToDate().getValue());					
							Date formDate;
							Date toDate;
							formDate=frmAndToPop.getFromDate().getValue();
							toDate=frmAndToPop.getToDate().getValue();
							
							int diffdays = AppUtility.getDifferenceDays(formDate, toDate);
							Console.log("diffdays "+diffdays);
							if(diffdays>1){
								form.showDialogMessage("Please select from date and to date range within 1 day");
								form.hideWaitSymbol();
								return;
							}
						}else{
							form.showDialogMessage("Please select date");
							form.hideWaitSymbol();
							return;
						}
						
						if(frmAndToPop.getFromDate().getValue()!=null) {
							fromdate=frmAndToPop.getFromDate().getValue();
							CalendarUtil.addDaysToDate(fromdate, 1);
						}
						if(frmAndToPop.getToDate().getValue()!=null) {
							todate=frmAndToPop.getToDate().getValue();
							CalendarUtil.addDaysToDate(todate, 1);
						}
						Console.log("fromdate="+fromdate+" todate:"+todate);
						IntegrateSyncServiceAsync integrateService=GWT.create(IntegrateSyncService.class);
						integrateService.generateEinvoicesInBulk(model.getCompanyId(), fromdate, todate,null,LoginPresenter.loggedInUser, new  AsyncCallback<String>() {

							@Override
							public void onFailure(Throwable caught) {
								// TODO Auto-generated method stub
								form.showDialogMessage("Process failed!");
								form.hideWaitSymbol();
							}

							@Override
							public void onSuccess(String result) {
								// TODO Auto-generated method stub
								form.showDialogMessage("Process has started! Please check after 15 minutes");
								form.hideWaitSymbol();
							}
						});
						
						panel.hide();
			 			bulkEinvoiceflag=false;
			 		
			 		}
			 		else if(flagforrepot){
			 			Console.log("in serviceRevanueReport date popup");
			 			async.serviceRevanueReport(model.getCompanyId(), frmAndToPop.getFromDate().getValue(), frmAndToPop.getToDate().getValue(),"", new AsyncCallback<String>(){

							@Override
							public void onFailure(Throwable caught) {
								// TODO Auto-generated method stub
								form.hideWaitSymbol();
							}

							@Override
							public void onSuccess(String result) {
								// TODO Auto-generated method stub
								form.hideWaitSymbol();
								if(result.equals("Success")){
									String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
									final String url = gwt + "csvservlet" + "?type=" + 123;
									Window.open(url, "test", "enabled");
								}else{
									form.showDialogMessage(result);
								}
								
							}
				 			
				 		});
				 		panel.hide();
			 		}else{
			 			Console.log("in technicianRevanueReport date popup");
			 			async.technicianRevanueReport(model.getCompanyId(), frmAndToPop.getFromDate().getValue(), frmAndToPop.getToDate().getValue(), "",new AsyncCallback<String>(){

							@Override
							public void onFailure(Throwable caught) {
								// TODO Auto-generated method stub
								form.hideWaitSymbol();
							}

							@Override
							public void onSuccess(String result) {
								// TODO Auto-generated method stub
								form.hideWaitSymbol();
								if(result.equals("Success")){
									String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
									final String url = gwt + "csvservlet" + "?type=" + 124;
									Window.open(url, "test", "enabled");
								}else{
									form.showDialogMessage(result);
								}
								
							}
				 			
				 		});
				 		panel.hide();
			 		}
			 		
			 		frmAndToPop.getFromDate().setValue(null);
			 		frmAndToPop.getToDate().setValue(null);			 		
				}
				
				if(event.getSource()==frmAndToPop.btnTwo){
					panel.hide();
				}
				 if(event.getSource() == fumOptPopup.getLblOk()){
					 if(fumOptPopup.getDbfromDate().getValue()!=null && fumOptPopup.getDbToDate().getValue()!=null){
						 if(fumOptPopup.getDbToDate().getValue().before(fumOptPopup.getDbfromDate().getValue())){
					 			form.showDialogMessage("To date should be greater than from date.");
					 			return;
					 		}
					 }
					
					 Date toDate=null,fromDate = null;
					 if(fumOptPopup.getDbfromDate().getValue() == null && fumOptPopup.getDbToDate().getValue() == null){
						 toDate = new Date();
						 fromDate = null;
					 }else if(fumOptPopup.getDbfromDate().getValue() == null || fumOptPopup.getDbToDate().getValue() == null){
						 if(fumOptPopup.getDbfromDate().getValue() != null){
							 toDate = fumOptPopup.dbfromDate.getValue();
						 }else if(fumOptPopup.getDbToDate().getValue() != null){
							 toDate = fumOptPopup.getDbToDate().getValue();
						 }
					 }else{
						 fromDate = fumOptPopup.getDbfromDate().getValue();
						 toDate = fumOptPopup.getDbToDate().getValue();
					 }
					 ArrayList<String> brName = new ArrayList<String>();
					 if(fumOptPopup.getOlBranch().getSelectedIndex() == 0){
						
						 for(int i=1;i<fumOptPopup.getOlBranch().getItemCount() ; i++){
							 brName.add(fumOptPopup.getOlBranch().getValue(i).trim());
						 }
						
					 }else{
						 brName.add(fumOptPopup.getOlBranch().getValue(fumOptPopup.getOlBranch().getSelectedIndex()).trim());
					 }
//					 form.showDialogMessage(brName.toString());
					 System.out.println("selected branch - service value report-" + brName.toString());
					 if(serviceRevanue){
						 
						 updateSer.dailyFumigationValueReportDetails(model.getCompanyId(), fromDate,toDate ,brName , new AsyncCallback<Void>() {
								
								@Override
								public void onSuccess(Void result) {
			
									String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
									final String url=gwt + "CreateXLXSServlet"+"?type="+2;
									Window.open(url, "test", "enabled");
									fumOptPopup.hidePopUp();
								}
								
								@Override
								public void onFailure(Throwable caught) {
									// TODO Auto-generated method stub
									
								}
							});
					 }else{
						 System.out.println("selected branch - fumigation report-" + brName.toString());
						 updateSer.dailyFumigationReportDetails(model.getCompanyId(),fromDate,toDate,brName, new AsyncCallback<Void>() {
								
								@Override
								public void onSuccess(Void result) {
			
									String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
									final String url=gwt + "CreateXLXSServlet"+"?type="+1;
									Window.open(url, "test", "enabled");
									fumOptPopup.hidePopUp();
								}
								
								@Override
								public void onFailure(Throwable caught) {
									// TODO Auto-generated method stub
									
								}
							});
					 }
						
				 } 
				 if(event.getSource() == fumOptPopup.getLblCancel()){
					 fumOptPopup.hidePopUp();
				 }
				 
				 if(event.getSource() == genralInvoicePopup.getBtnClose()){
					 
					 Invoice invoiceEntity = selectionModelMyObj.getLastSelectedObject();

					 if(invoiceEntity!=null && invoiceEntity.getTypeOfOrder().equals(AppConstants.SALESORDER)){
						 reactonClosePopupButton(invoiceEntity);
					 }
					 else{
							List<Invoice> invoicelist =  form.getSuperTable().getListDataProvider().getList();
							 form.getSuperTable().getDataprovider().setList(invoicelist);
						
						 refreshscreenMenu();
						 
					 }
					
				 }
				 
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
	 	
		 
		 	if(event.getSource()==productPopup.getBtnCancel()) {
				Console.log("productPopup.getBtnCancel() clicked");
				productDetailsPanel.hide();
			}
	 	}

	

	/*
	  *   end
	  */

	 
	private void reactonClosePopupButton(final Invoice invoiceEntity) {

		final MyQuerry querry=new MyQuerry();
		Vector<Filter> temp=new Vector<Filter>();
		Filter filter=null;
		
		filter=new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(invoiceEntity.getCount());
		temp.add(filter);
		
		filter=new Filter();
		filter.setQuerryString("contractCount");
		filter.setIntValue(invoiceEntity.getContractCount());
		temp.add(filter);
		
		filter=new Filter();
		filter.setQuerryString("typeOfOrder");
		filter.setStringValue(AppConstants.SALESORDER);
		temp.add(filter);
		
		querry.setFilters(temp);
		querry.setQuerryObject(new Invoice());
		
//		form.showWaitSymbol();
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
//				form.hideWaitSymbol();
				Console.log("invoice result size"+result.size());
				
				System.out.println("on close in service presenter");
				Console.log("on close in invoice list presenter");
				List<Invoice> invoicelist =  form.getSuperTable().getListDataProvider().getList();
				
				
				
				for(SuperModel model : result){
					Invoice invoice = (Invoice) model;
					if(invoice.getCount() == invoiceEntity.getCount() && invoice.getId().equals(invoiceEntity.getId())){
						invoicelist.remove(invoiceEntity);
						invoicelist.add(invoice);
					}
					break;
				}
				
				Comparator<Invoice> Invoice =  new Comparator<Invoice>() {
					
					@Override
					public int compare(Invoice p1, Invoice p2) {
						
						return p2.getInvoiceDate().compareTo(p1.getInvoiceDate());
					}
				};
				Collections.sort(invoicelist ,Invoice);
				
				form.getSuperTable().getDataprovider().setList(invoicelist);
				 
				refreshscreenMenu();
				    
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
//				form.hideWaitSymbol();
				 generalPanel.hide();

			}
		});
	}
	 
	 private void refreshscreenMenu() {


			genralInvoicePopup.viewDocumentPanel.hide();
			
			generalPanel.hide();
			
			AppMemory.getAppMemory().currentScreen = Screen.INVOICELIST;
			
			form.initializeScreen();
			form.intializeScreenMenus();
			form.addClickEventOnScreenMenus();
			form.addClickEventOnActionAndNavigationMenus();
//			setEventHandeling();
			for (int k = 0; k < mem.skeleton.menuLabels.length; k++) {
				if (mem.skeleton.registration[k] != null)
					mem.skeleton.registration[k].removeHandler();
			}

			for (int k = 0; k < mem.skeleton.menuLabels.length; k++)
				mem.skeleton.registration[k] = mem.skeleton.menuLabels[k].addClickHandler(this);
			form.setToViewState();
	}
	 
	 private ArrayList<Invoice> getAllSelectedRecords() 
		
		{
			List<Invoice> lisservicetable=form.getSuperTable().getDataprovider().getList();
			Console.log("table===="+lisservicetable.size());
			 ArrayList<Invoice> selectedlist=new ArrayList<Invoice>();
				 for(Invoice inv:lisservicetable)
				 {
					 if(inv.getRecordSelect()!=null&&inv.getRecordSelect()==true){
						 selectedlist.add(inv);
					 }
				 }
				 
				 Console.log("selectedlist===="+selectedlist.size());
				 return selectedlist;
		}
	 private void showProductDetails(Invoice bill) {
			Console.log("product details clicked");	
			if(bill.getAccountType().equals("AR")) {

				 MyQuerry querry=new MyQuerry();
				 Vector<Filter> filtrvec=new Vector<Filter>();
				 Filter filterval=null;
				 
				 filterval=new Filter();
				 filterval.setQuerryString("count");
				 filterval.setIntValue(bill.getContractCount());
				 filtrvec.add(filterval);
				 
				 filterval=new Filter();
				 filterval.setQuerryString("companyId");
				 filterval.setLongValue(bill.getCompanyId());
				 filtrvec.add(filterval);
				 
				 querry.setFilters(filtrvec);
				 querry.setQuerryObject(new Contract());
				
				service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
					
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						if(result!=null&&result.size()>0) {
							Contract con=(Contract)result.get(0);
							productDetailsPanel = new PopupPanel(true);
							productPopup.getproductTable().getDataprovider().setList(con.getItems());						
							
							productDetailsPanel.add(productPopup);
							productDetailsPanel.show();
							productDetailsPanel.center();
						
						}else
							form.showDialogMessage("Failed");
						
					}
					
					@Override
					public void onFailure(Throwable caught) {
						form.showDialogMessage("Failed to load details");
						
					}
				});
				
				
				
			}else
				form.showDialogMessage("Applicable only for Contract invoices");
			
		}
		
}
