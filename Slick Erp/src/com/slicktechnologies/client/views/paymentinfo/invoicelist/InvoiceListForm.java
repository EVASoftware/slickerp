package com.slicktechnologies.client.views.paymentinfo.invoicelist;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreen;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

public class InvoiceListForm extends TableScreen<Invoice> {
	
	//Token to add variable declaration
		//
		
		public InvoiceListForm(SuperTable<Invoice> superTable) {
			super(superTable);
			createGui();
		}
		
		@SuppressWarnings("unused")
		private void initalizeWidget()
		{
			//<ReplasbleVariableInitalizerBlock>
		}
		
		@Override
		public void createScreen() 
		{
			/**
			 * Date : 17-03-2017 
			 * added by Anil
			 * NBHC CCPM
			 */
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice","OnlyForNBHC"))
			{
//				this.processlevelBarNames = new String[] {"Invoice Acknowledgment","Revenue Report","Technician Revenue Report", AppConstants.SERVICEFUMIGATIONREPORT/*nidhi 18-11-2017 for display fumigation report*/, AppConstants.SERVICEFUMIGATIONVALUEREPORT/**
//						nidhi
//						18-11-2017
//						for contract revanue details report
//						**/};
////				,"Technician Revenue Report"
				
				/**
				 * Date 05-07-2018 by Vijay
				 * Des :- NBHC they need only Invoice Acknowledgment button for branch user other buttons not required so removed from here and above old code commented
				 * and admin able to view all menus
				 */
				if(LoginPresenter.myUserEntity.getRole().getRoleName().equals("ADMIN")){
					this.processlevelBarNames = new String[] {"Invoice Acknowledgment","Revenue Report","Technician Revenue Report", AppConstants.SERVICEFUMIGATIONREPORT/*nidhi 18-11-2017 for display fumigation report*/, AppConstants.SERVICEFUMIGATIONVALUEREPORT,AppConstants.BulkeInvoiceGeneration/**
							nidhi
							18-11-2017
							for contract revanue details report
							**/};
				}else{
					this.processlevelBarNames = new String[] {"Invoice Acknowledgment",AppConstants.BulkeInvoiceGeneration};

				}
				/**
				 * ends here
				 */
				
			}else if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice","EnableUltimaTallyExportOnInvoiceList")){
					this.processlevelBarNames = new String[] {AppConstants.SALESREGISTER,AppConstants.BulkeInvoiceGeneration,AppConstants.TallyExportForUltima};

			}else {
				/**27-10-2017 sagar sore [sales register button added in invoice list screen]**/
				/** Added by sheetal:07-12-2021 
				 * Des:Making sales register option from the menu of the invoice list visible to only admin role 
				 */
				if(LoginPresenter.myUserEntity.getRole().getRoleName().equals("ADMIN")){
					this.processlevelBarNames = new String[] {AppConstants.SALESREGISTER,AppConstants.BulkeInvoiceGeneration};
				}
				
			}
		}
		
		@Override
		public void setEnable(boolean state) {
			super.setEnable(state);
		}

		/**
		 * method template to update the model with token entity name
		 */
		@Override
		public void updateModel(Invoice model)
		{
			//<ReplasableUpdateModelFromViewCode>
		}

		


		/**
		 * method template to update the view with token entity name
		 */
		@Override
		public void updateView(Invoice view) 
		{
			//<ReplasableUpdateViewFromModelCode>
		}
		
		/**
		 * Toggles the app header bar menus as per screen state
		 */
		public void toggleAppHeaderBarMenu() {
			if(superTable!=null&&superTable.getListDataProvider().getList()!=null){
				Console.log("in toggleAppHeaderBarMenu count="+superTable.getListDataProvider().getList().size());
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.contains("Rows:")){
						menus[k].setText("Rows:"+superTable.getListDataProvider().getList().size());	
						menus[k].getElement().setId("addlabel2");
					}
				}
			}
			if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.equals("Discard")||text.equals("Download")||text.equals("Search")||text.equals(AppConstants.NAVIGATION)||text.contains("Rows:"))
					{
						menus[k].setVisible(true); 
					}
					else
						menus[k].setVisible(false);  		  		
				}
			}
			
			else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION)||text.contains("Rows:"))
						menus[k].setVisible(true); 
					
					else
						menus[k].setVisible(false);  		   
				}
			}
			
			else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.equals("Discard")||text.equals("Download")||text.equals("Search")||text.equals(AppConstants.NAVIGATION)||text.contains("Rows:"))
							menus[k].setVisible(true); 
					
					else
						menus[k].setVisible(false);  		   
				}
			}
			AuthorizationHelper.setAsPerAuthorization(Screen.INVOICELIST,LoginPresenter.currentModule.trim());
		}
		
		
		@Override
		public void retriveTable(MyQuerry querry) {
			superTable.connectToLocal();
			service.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					for(SuperModel model:result)
						superTable.getListDataProvider().getList().add((Invoice) model);
//					superTable.getTable().setVisibleRange(0,result.size());
					
					
				}
				
				@Override
				public void onFailure(Throwable caught) {
				caught.printStackTrace();
					
				}
			});
			
		}
		
		

}
