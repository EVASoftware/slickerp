package com.slicktechnologies.client.views.paymentinfo.invoicelist;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;

public class FumigationOptPopup extends PopupScreen{

	DateBox dbfromDate;
	DateBox dbToDate;
	ObjectListBox<Branch> olBranch;
	FormField fgroupingExpenseDetails;
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
	}

	private void initilizewidget() {
		
		
		dbfromDate=new DateBox();
		dbToDate=new DateBox();
		
		olBranch=new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olBranch);
//		setBranchList();
	}
	
	public void setBranchList(){
//		removeAllItems();
		
		olBranch.addItem("--SELECT--");
//		app
//		List<Branch> items=LoginPresenter.globalBranch;
//		System.out.println("get branch list size --" + LoginPresenter.globalBranch.size());
//		Comparator<Branch> dropDownComparator = new Comparator<Branch>() {
//			public int compare(Branch c1,Branch c2) {
//			String configName1=c1.toString();
//			String configName2=c2.toString();
//			
//			return configName1.compareTo(configName2);
//			}
//		};
//		Collections.sort(items,dropDownComparator);
//		
//		
//		for(int i=0;i<items.size();i++)
//		{
//			{
//				olBranch.addItem(items.get(i).toString());
////				olBranch.addItem(items.get(i).getBusinessUnitName(), items.get(i).getBusinessUnitName());
//				System.out.println("get old size --" + olBranch.getItemCount());
//			}
//		}
	}
	
	public FormField getFgroupingExpenseDetails() {
		return fgroupingExpenseDetails;
	}

	public void setFgroupingExpenseDetails(FormField fgroupingExpenseDetails) {
		this.fgroupingExpenseDetails = fgroupingExpenseDetails;
	}

	@Override
	public void createScreen() {
		// TODO Auto-generated method stub
		initilizewidget();
		

		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		 fgroupingExpenseDetails=fbuilder.setlabel("Revenue Report Options").widgetType(FieldType.GREYGROUPING).setMandatory(false).setColSpan(5).build();
		fbuilder = new FormFieldBuilder("From Date",dbfromDate);
		FormField fdbfromDate= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("To Date",dbToDate);
		FormField fdbToDate= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Branch",olBranch);
		FormField folBranch= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		
		FormField[][] formfield = {
				{fgroupingExpenseDetails},
				{fdbfromDate,fdbToDate,folBranch}
		};


		this.fields=formfield;
	}

	
	public DateBox getDbfromDate() {
		return dbfromDate;
	}

	public void setDbfromDate(DateBox dbfromDate) {
		this.dbfromDate = dbfromDate;
	}

	public DateBox getDbToDate() {
		return dbToDate;
	}

	public void setDbToDate(DateBox dbToDate) {
		this.dbToDate = dbToDate;
	}

	public ObjectListBox<Branch> getOlBranch() {
		return olBranch;
	}

	public void setOlBranch(ObjectListBox<Branch> olBranch) {
		this.olBranch = olBranch;
	}

	public FumigationOptPopup(){
		super();
		createGui();
	}
	
}
