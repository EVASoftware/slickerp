package com.slicktechnologies.client.views.paymentinfo.invoicelist;

import java.util.Comparator;
import java.util.List;

import com.google.code.p.gwtchismes.client.GWTCGlassPanel;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.ValueUpdater;
import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.Header;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.client.Timer;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.paymentinfo.billinglist.BillingListPresenter;
import com.slicktechnologies.client.views.service.ServicePresenter;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

public class InvoiceListTable extends SuperTable<Invoice> {
	
	TextColumn<Invoice> getCountColumn;
	TextColumn<Invoice> invoiceDateColumn;
	TextColumn<Invoice> invoiceTypeColumn;
	TextColumn<Invoice> customerIdColumn;
	TextColumn<Invoice> customerNameColumn;
	TextColumn<Invoice> customerCellColumn;
	TextColumn<Invoice> contractCountColumn;
	TextColumn<Invoice> salesAmtColumn;
	TextColumn<Invoice> invoiceAmtColumn;
	TextColumn<Invoice> approverNameColumn;
	TextColumn<Invoice> accountTypeColumn;
	TextColumn<Invoice> statusColumn;
	TextColumn<Invoice> branchColumn;
	
	
	/*
	 *  nidhi
	 *   1-07-2017
	 */
	TextColumn<Invoice> segmentColumn;
	/*
	 *  end
	 */
	
	/*
	 *  nidhi
	 *   17-07-2017 
	 *   display service Id 
	 */
	TextColumn<Invoice> serviceIdColumn;
	/*
	 *  end
	 */

	/**
	 *  nidhi
	 *  20-07-2017
	 *   for display service quantity and unit
	 */
	TextColumn<Invoice> quantityColumn;
	TextColumn<Invoice> uomColumn;
	/*
	 *  end
	 */
	
	/**
	 *  nidhi
	 *  24-07-2017
	 *   for display service quantity and unit
	 */
	TextColumn<Invoice> refColumn;
	/*
	 *  end
	 */
	
	/** 03-10-2017 sagar sore [To add Number range column in Invoice list ]**/
	TextColumn<Invoice> numberRangeColumn;
	/** date 1.11.2018 added by komal for type of order and sub bill type **/
	TextColumn<Invoice> getColTypeOfOrder;
	TextColumn<Invoice> getColSubBillType;
	
	
	//Ashwini Patil Date:12-08-2023
	TextColumn<Invoice> IRNNumberColumn;
	TextColumn<Invoice> ackNoColumn;
	TextColumn<Invoice> ackDateColumn;
	TextColumn<Invoice> IRNStatusColumn;
	TextColumn<Invoice> taskIdColumn;
	TextColumn<Invoice> einvoiceResult;
	TextColumn<Invoice> cancellationDateColumn;
	Column<Invoice, Boolean> checkColumn;
	Header<Boolean> selectAllHeader;
	protected GWTCGlassPanel glassPanel;
	Column<Invoice, String> productDetailsButton; //Ashwini Patil Date:3-07-2024 for ultima search
	
	public InvoiceListTable() {
		super();
	}

	
	
	@Override
	public void createTable() {
//		/**03-10-2017 sagar sore [adding number range column]**/
//		addColumnCount();
//		addColumnInvoiceDate();
//		addColumnInvoiceType();
//		addColumnCustomerId();		
//		addColumnNumberRange();
//		addColumnCustomerName();
//		addColumnCustomerPhone();
//		addColumnAccountType();
//		addColumnContractCount();
//		addColumnSalesAmount();
//		addColumnInvoiceBranch();
//		addColumnInvoiceAmount();
//		addColumnApproverName();
//		addColumnStatus();
//		/*
//		 *  nidhi
//		 */
//		addColumnServiceId();
//		/*
//		 *  end
//		 */
//		
//		/*
//		 *  nidhi
//		 */
//		addColumnInvoiceSegment();
//		/*
//		 *  end
//		 */
//
//		/**
//		 *  nidhi
//		 *  for quantity and uom display
//		 */
//		addColumnQuantity();
//		addColumnUom();
//		/**
//		 *  end
//		 */
//		/**
//		 *  nidhi
//		 *  for quantity and uom display
//		 *  24-07-2017
//		 */
//		addColumnRefNo();
//		/**
//		 *  end
//		 */
//		getColTypeOfOrder();
//		getColSubBillType();
        
		
		/**
         * Added by Sheetal -> 18-10-2021
         * Des: Invoice list Column realignment
         */
		addColumnCheckBox();//Ashwini Patil Date:31-08-2023
		addColumnCount();
		addColumnInvoiceDate();
		addColumnStatus();
		addColumnCustomerName();
		addColumnCustomerPhone();
		createColumnProductDetails();
		addColumnInvoiceAmount();
		addColumnNumberRange();
		addColumnApproverName();
		addColumnRefNo();
		addColumnInvoiceType();
		addColumnSalesAmount();
		addColumnInvoiceBranch();
		addColumnContractCount();
		addColumnCustomerId();	
		getColTypeOfOrder();
		getColSubBillType();
		addColumnServiceId();
		addColumnInvoiceSegment();
		addColumnQuantity();
		addColumnAccountType();
		addColumnIRNNumber();
		addColumnAckNumber();
		addColumnAckDate();
		addColumnIRNStatus();
		addColumnCancellationDate();
		addColumnEInvoiceResult();
		addColumnTaskId();
		
		 /**
		  *  End
		  */
		 
	    table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
	}
	
	public void addColumnSorting()
	{
		addSortinggetInvoiceCount();
		addSortinggetInvoiceDate();
		addSortinggetInvoiceType();
		addSortinggetCustomerId();
		addSortinggetCustomerFullName();
		addSortinggetCustomerCellNumber();
		addSortingAccountType();
		addSortinggetContractCount();
		addSortinggetSalesAmount();
		addSortinggetInvoiceAmount();
		addSortinggetApproverName();
		addSortinggetStatus();
		addSortinggetBranch();
		/*
		 *  nidhi
		 */
		addSortinggetSegment();
		/*
		 * end
		 */
		addSortinggetServiceId();
		/*
		 *  nidhi
		 *  20-09-2017
		 *  
		 */
//		addSortingUom();
		addSortingQuantity();
		/**
		 *  end
		 */
		/*
		 *  nidhi
		 *  24-07-2017
		 *  
		 */
		addSortingRefNo();
		/**
		 *  end
		 */
		
		/**03-10-2017 sagar sore [Sorting for number range column]**/
		addSortinggetNumberRange();
		addSortingTypeOfOrder();
		addSortingSubBillType();
	}
	
	public void addColumnServiceId()
	{
		serviceIdColumn =new TextColumn<Invoice>() {

			@Override
			public String getValue(Invoice object) {
				return object.getRateContractServiceId()+"";
			}
		};
		
		table.addColumn(serviceIdColumn,"Service Id");
		table.setColumnWidth(serviceIdColumn, 100, Unit.PX);
		serviceIdColumn.setSortable(true);
	}	
	
	public void addColumnInvoiceSegment()
	{
		segmentColumn=new TextColumn<Invoice>() {

			@Override
			public String getValue(Invoice object) {
				return object.getSegment();
			}
		};
		
		table.addColumn(segmentColumn,"Segment");
		table.setColumnWidth(segmentColumn, 100, Unit.PX);
		segmentColumn.setSortable(true);
	}	
	
	protected void addSortinggetServiceId()
	{
		List<Invoice> list=getDataprovider().getList();
		columnSort=new ListHandler<Invoice>(list);
		columnSort.setComparator(serviceIdColumn, new Comparator<Invoice>()
				{
			@Override
			public int compare(Invoice e1,Invoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getRateContractServiceId()== e2.getRateContractServiceId()){
						return 0;}
					if(e1.getRateContractServiceId()> e2.getRateContractServiceId()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	private void addSortingQuantity() {
		List<Invoice> list=getDataprovider().getList();
		columnSort=new ListHandler<Invoice>(list);
		columnSort.setComparator(quantityColumn, new Comparator<Invoice>()
				{
			@Override
			public int compare(Invoice e1,Invoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getQuantity()== e2.getQuantity()){
						return 0;}
					if(e1.getQuantity()> e2.getQuantity()){
						return 1;}
					else{
						return -1;
					}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortingRefNo()
	{
		List<Invoice> list=getDataprovider().getList();
		columnSort=new ListHandler<Invoice>(list);
		columnSort.setComparator(refColumn, new Comparator<Invoice>()
				{
			@Override
			public int compare(Invoice e1,Invoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getRefNumber()!=null && e2.getRefNumber()!=null){
						return e1.getRefNumber().compareTo(e2.getRefNumber());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortingUom()
	{
		List<Invoice> list=getDataprovider().getList();
		columnSort=new ListHandler<Invoice>(list);
		columnSort.setComparator(uomColumn, new Comparator<Invoice>()
				{
			@Override
			public int compare(Invoice e1,Invoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getUom()!=null && e2.getUom()!=null){
						return e1.getUom().compareTo(e2.getUom());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
private void addColumnQuantity() {
		
		quantityColumn = new TextColumn<Invoice>() {
			
			@Override
			public String getValue(Invoice object) {
				if(object.getQuantity()!=0)
					return object.getQuantity() +"";
				else 
					return "";
			}
		};
		
		table.addColumn(quantityColumn,"Quantity");
		table.setColumnWidth(quantityColumn, 100, Unit.PX);
		quantityColumn.setSortable(true);
		
	}
	
//	public void addColumnUom()
//	{
//		uomColumn=new TextColumn<Invoice>() {
//
//			@Override
//			public String getValue(Invoice object) {
//				return object.getUom();
//			}
//		};
//		
//		table.addColumn(uomColumn,"Status");
//		table.setColumnWidth(uomColumn, 100, Unit.PX);
//		uomColumn.setSortable(true);
//	}	
	
	public void addColumnRefNo()
	{
		refColumn=new TextColumn<Invoice>() {

			@Override
			public String getValue(Invoice object) {
				return object.getRefNumber();
			}
		};
		
		table.addColumn(refColumn,"Reference");
		table.setColumnWidth(refColumn, 100, Unit.PX);
		refColumn.setSortable(true);
	}	
	
	protected void addSortinggetSegment()
	{
		List<Invoice> list=getDataprovider().getList();
		columnSort=new ListHandler<Invoice>(list);
		columnSort.setComparator(segmentColumn, new Comparator<Invoice>()
				{
			@Override
			public int compare(Invoice e1,Invoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getSegment()!=null && e2.getSegment()!=null){
						return e1.getSegment().compareTo(e2.getSegment());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetBranch()
	{
		List<Invoice> list=getDataprovider().getList();
		columnSort=new ListHandler<Invoice>(list);
		columnSort.setComparator(branchColumn, new Comparator<Invoice>()
				{
			@Override
			public int compare(Invoice e1,Invoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBranch()!=null && e2.getBranch()!=null){
						return e1.getBranch().compareTo(e2.getBranch());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetInvoiceCount()
	{
		List<Invoice> list=getDataprovider().getList();
		columnSort=new ListHandler<Invoice>(list);
		columnSort.setComparator(getCountColumn, new Comparator<Invoice>()
				{
			@Override
			public int compare(Invoice e1,Invoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	public void addColumnCount()
	{
		getCountColumn=new TextColumn<Invoice>() {
			@Override
			public String getCellStyleNames(Context context, Invoice object) {
				if((object.getStatus().equals("Cancelled"))){
					 return "red";
				 }
				else 
				{
					return "black";
				}
			}

			@Override
			public String getValue(Invoice object) {
				return object.getCount()+"";
			}
		};
		table.addColumn(getCountColumn,"Invoice ID");
		table.setColumnWidth(getCountColumn, 100, Unit.PX);
		getCountColumn.setSortable(true);
	}
	
	protected void addSortinggetInvoiceDate()
	{
		List<Invoice> list=getDataprovider().getList();
		columnSort=new ListHandler<Invoice>(list);
		columnSort.setComparator(invoiceDateColumn, new Comparator<Invoice>()
				{
			@Override
			public int compare(Invoice e1,Invoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getInvoiceDate()!=null && e2.getInvoiceDate()!=null){
						return e1.getInvoiceDate().compareTo(e2.getInvoiceDate());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	public void addColumnInvoiceDate()
	{
		invoiceDateColumn=new TextColumn<Invoice>() {

			@Override
			public String getValue(Invoice object) {
				if(object.getInvoiceDate()!=null)
				{
				    String serDate=AppUtility.parseDate(object.getInvoiceDate());
				    return serDate;
				}
				return "Not Set";
			}
		};
		
		table.addColumn(invoiceDateColumn,"Invoice Date");
		table.setColumnWidth(invoiceDateColumn, 100, Unit.PX);
		invoiceDateColumn.setSortable(true);
	}
	
	protected void addSortinggetInvoiceType()
	{
		List<Invoice> list=getDataprovider().getList();
		columnSort=new ListHandler<Invoice>(list);
		columnSort.setComparator(invoiceTypeColumn, new Comparator<Invoice>()
				{
			@Override
			public int compare(Invoice e1,Invoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getInvoiceType()!=null && e2.getInvoiceType()!=null){
						return e1.getInvoiceType().compareTo(e2.getInvoiceType());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}

	
	private void addColumnInvoiceType() {
		invoiceTypeColumn=new TextColumn<Invoice>() {

			@Override
			public String getValue(Invoice object) {
				return object.getInvoiceType();
			}
		};
		
		table.addColumn(invoiceTypeColumn,"Invoice Type");
		table.setColumnWidth(invoiceTypeColumn, 120, Unit.PX);
		invoiceTypeColumn.setSortable(true);
		
	}

	protected void addSortinggetCustomerId()
	{
		List<Invoice> list=getDataprovider().getList();
		columnSort=new ListHandler<Invoice>(list);
		columnSort.setComparator(customerIdColumn, new Comparator<Invoice>()
				{
			@Override
			public int compare(Invoice e1,Invoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getPersonInfo().getCount()== e2.getPersonInfo().getCount()){
						return 0;}
					if(e1.getPersonInfo().getCount()> e2.getPersonInfo().getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	private void addColumnCustomerId() {
		customerIdColumn=new TextColumn<Invoice>() {

			@Override
			public String getValue(Invoice object) {
				return object.getCustomerCount()+"";
			}
		};
		
		table.addColumn(customerIdColumn,"Customer ID");
		table.setColumnWidth(customerIdColumn, 100, Unit.PX);
		customerIdColumn.setSortable(true);
		
	}
	
	protected void addSortinggetCustomerFullName()
	{
		List<Invoice> list=getDataprovider().getList();
		columnSort=new ListHandler<Invoice>(list);
		columnSort.setComparator(customerNameColumn, new Comparator<Invoice>()
				{
			@Override
			public int compare(Invoice e1,Invoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getPersonInfo().getFullName()!=null && e2.getPersonInfo().getFullName()!=null){
						return e1.getPersonInfo().getFullName().compareTo(e2.getPersonInfo().getFullName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}

	
	public void addColumnCustomerName()
	{
		customerNameColumn=new TextColumn<Invoice>() {

			@Override
			public String getValue(Invoice object) {
				return object.getName();
			}
		};
		
		table.addColumn(customerNameColumn,"Customer");
		table.setColumnWidth(customerNameColumn, 100, Unit.PX);
		customerNameColumn.setSortable(true);
	}
	
	protected void addSortinggetCustomerCellNumber()
	{
		List<Invoice> list=getDataprovider().getList();
		columnSort=new ListHandler<Invoice>(list);
		columnSort.setComparator(customerCellColumn, new Comparator<Invoice>()
				{
			@Override
			public int compare(Invoice e1,Invoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getPersonInfo().getCellNumber()== e2.getPersonInfo().getCellNumber()){
						return 0;}
					if(e1.getPersonInfo().getCellNumber()> e2.getPersonInfo().getCellNumber()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	public void addColumnCustomerPhone()
	{
		customerCellColumn=new TextColumn<Invoice>() {

			@Override
			public String getValue(Invoice object) {
				return object.getCellNumber()+"";
			}
		};
		
		table.addColumn(customerCellColumn,"Mobile");
		table.setColumnWidth(customerCellColumn, 100, Unit.PX);
		customerCellColumn.setSortable(true);
	}
	
	protected void addSortingAccountType()
	{
		List<Invoice> list=getDataprovider().getList();
		columnSort=new ListHandler<Invoice>(list);
		columnSort.setComparator(accountTypeColumn, new Comparator<Invoice>()
				{
			@Override
			public int compare(Invoice e1,Invoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getAccountType()!=null && e2.getAccountType()!=null){
						return e1.getAccountType().compareTo(e2.getAccountType());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}

	public void addColumnAccountType()
	{
		accountTypeColumn=new TextColumn<Invoice>() {

			@Override
			public String getValue(Invoice object) {
				return object.getAccountType();
			}
		};
		
		table.addColumn(accountTypeColumn,"Acc. Type");
		table.setColumnWidth(accountTypeColumn, 90, Unit.PX);
		accountTypeColumn.setSortable(true);
	}
	
	protected void addSortinggetContractCount()
	{
		List<Invoice> list=getDataprovider().getList();
		columnSort=new ListHandler<Invoice>(list);
		columnSort.setComparator(contractCountColumn, new Comparator<Invoice>()
				{
			@Override
			public int compare(Invoice e1,Invoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getContractCount()== e2.getContractCount()){
						return 0;}
					if(e1.getContractCount()> e2.getContractCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	public void addColumnContractCount()
	{
		contractCountColumn=new TextColumn<Invoice>() {

			@Override
			public String getValue(Invoice object) {
				    return object.getContractCount()+"";
			}
		};
		
		table.addColumn(contractCountColumn,"Order ID");
		table.setColumnWidth(contractCountColumn, 100, Unit.PX);
		contractCountColumn.setSortable(true);
	}
	
	protected void addSortinggetSalesAmount()
	{
		List<Invoice> list=getDataprovider().getList();
		columnSort=new ListHandler<Invoice>(list);
		columnSort.setComparator(salesAmtColumn, new Comparator<Invoice>()
				{
			@Override
			public int compare(Invoice e1,Invoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getTotalSalesAmount()== e2.getTotalSalesAmount()){
						return 0;}
					if(e1.getTotalSalesAmount()> e2.getTotalSalesAmount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	public void addColumnSalesAmount()
	{
		salesAmtColumn=new TextColumn<Invoice>() {

			@Override
			public String getValue(Invoice object) {
				return object.getTotalSalesAmount()+"";
			}
		};
		
		table.addColumn(salesAmtColumn,"Order Net Payable");
		table.setColumnWidth(salesAmtColumn, 100, Unit.PX);
		salesAmtColumn.setSortable(true);
	}
	
	protected void addSortinggetInvoiceAmount()
	{
		List<Invoice> list=getDataprovider().getList();
		columnSort=new ListHandler<Invoice>(list);
		columnSort.setComparator(invoiceAmtColumn, new Comparator<Invoice>()
				{
			@Override
			public int compare(Invoice e1,Invoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getInvoiceAmount()== e2.getInvoiceAmount()){
						return 0;}
					if(e1.getInvoiceAmount()> e2.getInvoiceAmount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}

	public void addColumnInvoiceBranch()
	{
		branchColumn=new TextColumn<Invoice>() {

			@Override
			public String getValue(Invoice object) {
				return object.getBranch();
			}
		};
		
		table.addColumn(branchColumn,"Branch");
		table.setColumnWidth(branchColumn, 100, Unit.PX);
		branchColumn.setSortable(true);
	}	
	public void addColumnInvoiceAmount()
	{
		invoiceAmtColumn=new TextColumn<Invoice>() {

			@Override
			public String getValue(Invoice object) {
				return object.getInvoiceAmount()+"";
			}
		};
		
		table.addColumn(invoiceAmtColumn,"Net Payable");
		table.setColumnWidth(invoiceAmtColumn, 100, Unit.PX);
		invoiceAmtColumn.setSortable(true);
	}
	
	protected void addSortinggetApproverName()
	{
		List<Invoice> list=getDataprovider().getList();
		columnSort=new ListHandler<Invoice>(list);
		columnSort.setComparator(approverNameColumn, new Comparator<Invoice>()
				{
			@Override
			public int compare(Invoice e1,Invoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getApproverName()!=null && e2.getApproverName()!=null){
						return e1.getApproverName().compareTo(e2.getApproverName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	public void addColumnApproverName()
	{
		approverNameColumn=new TextColumn<Invoice>() {

			@Override
			public String getValue(Invoice object) {
				return object.getApproverName();
			}
		};
		
		table.addColumn(approverNameColumn,"Approver");
		table.setColumnWidth(approverNameColumn, 120, Unit.PX);
		approverNameColumn.setSortable(true);
	}		
	
	protected void addSortinggetStatus()
	{
		List<Invoice> list=getDataprovider().getList();
		columnSort=new ListHandler<Invoice>(list);
		columnSort.setComparator(statusColumn, new Comparator<Invoice>()
				{
			@Override
			public int compare(Invoice e1,Invoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getStatus()!=null && e2.getStatus()!=null){
						return e1.getStatus().compareTo(e2.getStatus());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	public void addColumnStatus()
	{
		statusColumn=new TextColumn<Invoice>() {

			@Override
			public String getValue(Invoice object) {
				return object.getStatus();
			}
		};
		
		table.addColumn(statusColumn,"Status");
		table.setColumnWidth(statusColumn, 100, Unit.PX);
		statusColumn.setSortable(true);
	}		
	
	/**03-10-2017 sagar sore [Creating number range column and sorting for that column]**/
	public void addColumnNumberRange() {
		numberRangeColumn = new TextColumn<Invoice>() {

			@Override
			public String getValue(Invoice object) {
				return object.getNumberRange();
			}
		};

		table.addColumn(numberRangeColumn, "Number Range");
		table.setColumnWidth(numberRangeColumn, 120, Unit.PX);
		numberRangeColumn.setSortable(true);
	}

	protected void addSortinggetNumberRange() {
		List<Invoice> list = getDataprovider().getList();
		columnSort = new ListHandler<Invoice>(list);
		columnSort.setComparator(numberRangeColumn, new Comparator<Invoice>() {
			@Override
			public int compare(Invoice e1, Invoice e2) {
				if (e1 != null && e2 != null) {
					
					if (e1.getNumberRange() == null && e2.getNumberRange() == null){
						return 0;
					}
					if(e1.getNumberRange() == null) return 1;							
					if(e2.getNumberRange() == null) return -1;							
					return e1.getNumberRange().compareTo(e2.getNumberRange());

				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	/**end**/

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
	/** date 1.11.2018 added by komal for type of order and sub bill type **/
	public void getColTypeOfOrder()
	{
		getColTypeOfOrder=new TextColumn<Invoice>() {

			@Override
			public String getValue(Invoice object) {
				return object.getTypeOfOrder();
			}
		};
		
		table.addColumn(getColTypeOfOrder,"Order Type");
		table.setColumnWidth(getColTypeOfOrder, 120, Unit.PX);
		getColTypeOfOrder.setSortable(true);
	}	
	
	protected void addSortingTypeOfOrder()
	{
		List<Invoice> list=getDataprovider().getList();
		columnSort=new ListHandler<Invoice>(list);
		columnSort.setComparator(getColTypeOfOrder, new Comparator<Invoice>()
				{
			@Override
			public int compare(Invoice e1,Invoice e2)
			{
				if (e1 != null && e2 != null) {
					
					if (e1.getNumberRange() == null && e2.getNumberRange() == null){
						return 0;
					}
					if(e1.getNumberRange() == null) return 1;							
					if(e2.getNumberRange() == null) return -1;							
					return e1.getNumberRange().compareTo(e2.getNumberRange());

				}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	public void getColSubBillType()
	{
		getColSubBillType=new TextColumn<Invoice>() {

			@Override
			public String getValue(Invoice object) {
				return object.getSubBillType();
			}
		};
		
		table.addColumn(getColSubBillType,"Sub Bill Type");
		table.setColumnWidth(getColSubBillType, 120, Unit.PX);
		getColSubBillType.setSortable(true);
	}	
	
	protected void addSortingSubBillType()
	{
		List<Invoice> list=getDataprovider().getList();
		columnSort=new ListHandler<Invoice>(list);
		columnSort.setComparator(getColSubBillType, new Comparator<Invoice>()
				{
			@Override
			public int compare(Invoice e1,Invoice e2)
			{
				if (e1 != null && e2 != null) {
					
					if (e1.getNumberRange() == null && e2.getNumberRange() == null){
						return 0;
					}
					if(e1.getNumberRange() == null) return 1;							
					if(e2.getNumberRange() == null) return -1;							
					return e1.getNumberRange().compareTo(e2.getNumberRange());

				}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	
	public void addColumnIRNNumber()
	{
		IRNNumberColumn=new TextColumn<Invoice>() {

			@Override
			public String getValue(Invoice object) {
				if(object.getIRN()!=null)
					return object.getIRN();
				else
					return "";
			}
		};
		
		table.addColumn(IRNNumberColumn,"IRN Number");
		table.setColumnWidth(IRNNumberColumn, 90, Unit.PX);
		IRNNumberColumn.setSortable(true);
	}
	
	public void addColumnAckNumber()
	{
		ackNoColumn=new TextColumn<Invoice>() {

			@Override
			public String getValue(Invoice object) {
				if(object.getIrnAckNo()!=null)
					return object.getIrnAckNo();
				else
					return "";
			}
		};
		
		table.addColumn(ackNoColumn,"Acknowledgement Number");
		table.setColumnWidth(ackNoColumn, 90, Unit.PX);
		ackNoColumn.setSortable(true);
	}
	
	public void addColumnAckDate()
	{
		ackDateColumn=new TextColumn<Invoice>() {

			@Override
			public String getValue(Invoice object) {
				if(object.getIrnAckDate()!=null)
					return object.getIrnAckDate();
				else
					return "";
			}
		};
		
		table.addColumn(ackDateColumn,"Acknowledgement Date");
		table.setColumnWidth(ackDateColumn, 90, Unit.PX);
		ackDateColumn.setSortable(true);
	}
	
	public void addColumnIRNStatus()
	{
		IRNStatusColumn=new TextColumn<Invoice>() {

			@Override
			public String getValue(Invoice object) {
				if(object.getIrnStatus()!=null)
					return object.getIrnStatus();
				else
					return "";
			}
		};
		
		table.addColumn(IRNStatusColumn,"IRN Status");
		table.setColumnWidth(IRNStatusColumn, 90, Unit.PX);
		IRNStatusColumn.setSortable(true);
	}
	
	public void addColumnCancellationDate()
	{
		cancellationDateColumn=new TextColumn<Invoice>() {

			@Override
			public String getValue(Invoice object) {
				if(object.getCancellationDate()!=null)
					return AppUtility.parseDate(object.getCancellationDate());
				else
					return "";
			}
		};
		
		table.addColumn(cancellationDateColumn,"Cancellation Date");
		table.setColumnWidth(cancellationDateColumn, 90, Unit.PX);
		cancellationDateColumn.setSortable(true);
	}

	public void addColumnTaskId()
	{
		taskIdColumn=new TextColumn<Invoice>() {

			@Override
			public String getValue(Invoice object) {
				if(object.getBulkEinvoiceTaskID()!=null)
					return object.getBulkEinvoiceTaskID();
				else
					return "";
			}
		};
		
		table.addColumn(taskIdColumn,"Task ID");
		table.setColumnWidth(taskIdColumn, 90, Unit.PX);
	}
	
	public void addColumnEInvoiceResult()
	{
		einvoiceResult=new TextColumn<Invoice>() {

			@Override
			public String getValue(Invoice object) {
				if(object.geteInvoiceResult()!=null)
					return object.geteInvoiceResult();
				else
					return "";
			}
		};
		
		table.addColumn(einvoiceResult,"eInvoice Result");
		table.setColumnWidth(einvoiceResult, 200, Unit.PX);
		einvoiceResult.setSortable(true);
	}
	/**end**/
	
	private void addColumnCheckBox() {
		InvoiceListPresenter.viewDocumentFlag=false;

		checkColumn=new Column<Invoice, Boolean>(new CheckboxCell()) {
			@Override
			public Boolean getValue(Invoice object) {
				return object.getRecordSelect();
			}
		};
		
		
		checkColumn.setFieldUpdater(new FieldUpdater<Invoice, Boolean>() {
			@Override
			public void update(int index, Invoice object, Boolean value) {
				System.out.println("Check Column ....");
				
				InvoiceListPresenter.viewDocumentFlag=false;

				object.setRecordSelect(value);
				table.redrawRow(index);
				selectAllHeader.getValue();
				table.redrawHeaders();
				
				glassPanel = new GWTCGlassPanel();
				glassPanel.show();
				Timer timer = new Timer() {
					@Override
					public void run() {
						glassPanel.hide();
					}
				};
				timer.schedule(1000); 
			}
		});
		
		
		selectAllHeader =new Header<Boolean>(new CheckboxCell()) {
			@Override
			public Boolean getValue() {
				if(getDataprovider().getList().size()!=0){
					
				}
				return false;
			}
		};
		
		
		selectAllHeader.setUpdater(new ValueUpdater<Boolean>() {
			@Override
			public void update(Boolean value) {
				List<Invoice> list=getDataprovider().getList();
				for(Invoice object:list){
					object.setRecordSelect(value);
				}
				getDataprovider().setList(list);
				table.redraw();
				addColumnSorting();
			}
		});
		table.addColumn(checkColumn,selectAllHeader);
		table.setColumnWidth(checkColumn, 65, Unit.PX);
		checkColumn.setCellStyleNames("checkboxWidth");
		selectAllHeader.setHeaderStyleNames("checkboxWidth");
	}

	private void createColumnProductDetails() {

		ButtonCell btnCell = new ButtonCell();
		productDetailsButton = new Column<Invoice, String>(btnCell) {

			@Override
			public String getValue(Invoice object) {
				return "Product Details";
			}
		};

		table.addColumn(productDetailsButton, "");
		table.setColumnWidth(productDetailsButton, 110, Unit.PX);
		
		productDetailsButton.setFieldUpdater(new FieldUpdater<Invoice, String>() {
			
			@Override
			public void update(int index, Invoice object, String value) {
				
				InvoiceListPresenter.viewDocumentFlag = false;
				InvoiceListPresenter.productdetailFlag = true;				
			}
				
		});
		
	}
	

}
