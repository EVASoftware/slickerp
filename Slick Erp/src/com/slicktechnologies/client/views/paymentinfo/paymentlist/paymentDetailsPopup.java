package com.slicktechnologies.client.views.paymentinfo.paymentlist;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.helperlayer.Config;

public class paymentDetailsPopup extends AbsolutePanel implements ChangeHandler{

	ObjectListBox<Config> olbPaymentMethod;
	DoubleBox dbAmountPayable,dbAmountReceived;
	
	TextBox tbbankAccNo,tbbankName,tbbankBranch;
	DateBox dbChequeDate,dbtransferDate;
	Button btnCancel,btnOk;
	TextBox tbreferenceno,tbchequeNo,tbchequeissuedBy;
	
	public paymentDetailsPopup() {
		
		
		InlineLabel heading = new InlineLabel("Payment Details");
		add(heading,300,10);
		heading.getElement().getStyle().setFontSize(20, Unit.PX);
		
		
		InlineLabel pableAmt = new InlineLabel("Payable Amount");
		add(pableAmt,10,50);
		dbAmountPayable= new DoubleBox();
		double paybelAmt= calculateTotalAmountPaybel();
		dbAmountPayable.setValue(paybelAmt);
		dbAmountPayable.setEnabled(false);
		add(dbAmountPayable,10,70);
		
		InlineLabel receivedAmt = new InlineLabel("Received Amount");
		add(receivedAmt,200,50);
		dbAmountReceived= new DoubleBox();
		add(dbAmountReceived,200,70);
		
		
		InlineLabel paymetMethod = new InlineLabel("Payment Method");
		add(paymetMethod,390,50);
		olbPaymentMethod= new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbPaymentMethod, Screen.PAYMENTMETHODS);
		olbPaymentMethod.addChangeHandler(this);
		add(olbPaymentMethod,390,70);
		
		
		
		InlineLabel accntNumber = new InlineLabel("Cheque-Bank Account No.");
		add(accntNumber,10,150);
		tbbankAccNo= new TextBox();
		add(tbbankAccNo,10,170);
//		tbbankAccNo.setSize("110px", "18px");
		tbbankAccNo.setEnabled(false);
		
		
		InlineLabel accntbankName = new InlineLabel("Cheque-Bank Name");
		add(accntbankName,200,150);
		tbbankName= new TextBox();
		add(tbbankName,200,170);
//		tbbankName.setSize("110px", "18px");
		tbbankName.setEnabled(false);

		
		InlineLabel branchName = new InlineLabel("Cheque-Bank Branch");
		add(branchName,390,150);
		tbbankBranch= new TextBox();
		add(tbbankBranch,390,170);
//		tbbankBranch.setSize("110px", "18px");
		tbbankBranch.setEnabled(false);
		
		
		InlineLabel chequeNumber = new InlineLabel("Cheque No");
		add(chequeNumber,580,150);
		tbchequeNo= new TextBox();
		add(tbchequeNo,580,170);
//		tbchequeNo.setSize("110px", "18px");
		tbchequeNo.setEnabled(false);
		
		
		
		InlineLabel chqueDate = new InlineLabel("Cheque Date");
		add(chqueDate,10,250);
//		dbChequeDate= new DateBox();
		dbChequeDate = new DateBoxWithYearSelector();

		add(dbChequeDate,10,270);
//		dbChequeDate.setSize("110px", "18px");
		dbChequeDate.setEnabled(false);
		
		
		InlineLabel transferDate = new InlineLabel("Transfer Date");
		add(transferDate,200,250);
//		dbtransferDate= new DateBox();
		dbtransferDate = new DateBoxWithYearSelector();

		add(dbtransferDate,200,270);
//		dbtransferDate.setSize("110px", "18px");
		dbtransferDate.setEnabled(false);

		
		InlineLabel refNumber = new InlineLabel("Reference No.");
		add(refNumber,390,250);
		tbreferenceno= new TextBox();
		add(tbreferenceno,390,270);
//		tbreferenceno.setSize("110px", "18px");
		tbreferenceno.setEnabled(false);
		
		
		InlineLabel issueBy = new InlineLabel("Cheque Issued By");
		add(issueBy,580,250);
		tbchequeissuedBy= new TextBox();
		add(tbchequeissuedBy,580,270);
//		tbchequeissuedBy.setSize("110px", "18px");
		tbchequeissuedBy.setEnabled(false);
		
		btnCancel= new Button("Cancel");
		btnOk= new Button("Ok");
		
		add(btnOk,250,360);
		add(btnCancel,450,360);
		setSize("800px", "400px");
	}

	private double calculateTotalAmountPaybel() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void onChange(ChangeEvent event) {
		
		String payMethodVal=null;
		if(this.olbPaymentMethod.getSelectedIndex()!=0)
		{
			payMethodVal=olbPaymentMethod.getValue(olbPaymentMethod.getSelectedIndex()).trim();
			if(payMethodVal.equalsIgnoreCase("Cash"))
			{
				clearFields();
				this.tbbankAccNo.setEnabled(false);
				this.tbbankName.setEnabled(false);
				this.tbbankBranch.setEnabled(false);
				this.tbchequeNo.setEnabled(false);
				this.dbChequeDate.setEnabled(false);
				this.tbreferenceno.setEnabled(false);
				this.dbtransferDate.setEnabled(false);
			}
			else if(payMethodVal.equalsIgnoreCase("Cheque"))
			{
				clearFields();
				this.tbbankAccNo.setEnabled(true);
				this.tbbankName.setEnabled(true);
				this.tbbankBranch.setEnabled(true);
				this.tbchequeNo.setEnabled(true);
				this.dbChequeDate.setEnabled(true);
				this.tbreferenceno.setEnabled(false);
				this.dbtransferDate.setEnabled(false);
			}
			else if(payMethodVal.equalsIgnoreCase("ECS"))
			{
				clearFields();
				this.tbreferenceno.setEnabled(true);
				this.dbtransferDate.setEnabled(true);
				this.tbbankAccNo.setEnabled(true);
				this.tbbankName.setEnabled(true);
				this.tbbankBranch.setEnabled(true);
				this.tbchequeNo.setEnabled(false);
				this.dbChequeDate.setEnabled(false);
			}
			else if(payMethodVal.equalsIgnoreCase("NEFT"))
			{
				clearFields();
				this.tbbankAccNo.setEnabled(false);
				this.tbbankName.setEnabled(false);
				this.tbbankBranch.setEnabled(false);
				this.tbchequeNo.setEnabled(false);
				this.dbChequeDate.setEnabled(false);
				this.tbreferenceno.setEnabled(true);
				this.dbtransferDate.setEnabled(true);
			}
		}		
		else
		{
			clearFields();
			this.tbbankAccNo.setEnabled(false);
			this.tbbankName.setEnabled(false);
			this.tbbankBranch.setEnabled(false);
			this.tbchequeNo.setEnabled(false);
			this.dbChequeDate.setEnabled(false);
			this.tbreferenceno.setEnabled(false);
			this.dbtransferDate.setEnabled(false);
		}
	}
	
	private void clearFields()
	{
		this.tbbankAccNo.setValue("");
		this.tbbankName.setValue("");
		this.tbbankBranch.setValue("");
		this.tbchequeNo.setValue(null);
		this.dbChequeDate.setValue(null);
		this.dbtransferDate.setValue(null);
		this.tbreferenceno.setValue("");
	}

	
/******************************************getters and setters ***************************************/
	
	
	public ObjectListBox<Config> getOlbPaymentMethod() {
		return olbPaymentMethod;
	}

	public void setOlbPaymentMethod(ObjectListBox<Config> olbPaymentMethod) {
		this.olbPaymentMethod = olbPaymentMethod;
	}

	public DoubleBox getDbAmountPayable() {
		return dbAmountPayable;
	}

	public void setDbAmountPayable(DoubleBox dbAmountPayable) {
		this.dbAmountPayable = dbAmountPayable;
	}

	public DoubleBox getDbAmountReceived() {
		return dbAmountReceived;
	}

	public void setDbAmountReceived(DoubleBox dbAmountReceived) {
		this.dbAmountReceived = dbAmountReceived;
	}

	public TextBox getTbbankAccNo() {
		return tbbankAccNo;
	}

	public void setTbbankAccNo(TextBox tbbankAccNo) {
		this.tbbankAccNo = tbbankAccNo;
	}

	public TextBox getTbbankName() {
		return tbbankName;
	}

	public void setTbbankName(TextBox tbbankName) {
		this.tbbankName = tbbankName;
	}

	public TextBox getTbbankBranch() {
		return tbbankBranch;
	}

	public void setTbbankBranch(TextBox tbbankBranch) {
		this.tbbankBranch = tbbankBranch;
	}

	public DateBox getDbChequeDate() {
		return dbChequeDate;
	}

	public void setDbChequeDate(DateBox dbChequeDate) {
		this.dbChequeDate = dbChequeDate;
	}

	public DateBox getDbtransferDate() {
		return dbtransferDate;
	}

	public void setDbtransferDate(DateBox dbtransferDate) {
		this.dbtransferDate = dbtransferDate;
	}

	public Button getBtnCancel() {
		return btnCancel;
	}

	public void setBtnCancel(Button btnCancel) {
		this.btnCancel = btnCancel;
	}

	public Button getBtnOk() {
		return btnOk;
	}

	public void setBtnOk(Button btnOk) {
		this.btnOk = btnOk;
	}

	public TextBox getTbreferenceno() {
		return tbreferenceno;
	}

	public void setTbreferenceno(TextBox tbreferenceno) {
		this.tbreferenceno = tbreferenceno;
	}

	public TextBox getTbchequeNo() {
		return tbchequeNo;
	}

	public void setTbchequeNo(TextBox tbchequeNo) {
		this.tbchequeNo = tbchequeNo;
	}

	public TextBox getTbchequeissuedBy() {
		return tbchequeissuedBy;
	}

	public void setTbchequeissuedBy(TextBox tbchequeissuedBy) {
		this.tbchequeissuedBy = tbchequeissuedBy;
	}
	
}
