package com.slicktechnologies.client.views.paymentinfo.paymentlist;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCGlassPanel;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.view.client.NoSelectionModel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreen;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreenPresenter;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.UpdateService;
import com.slicktechnologies.client.services.UpdateServiceAsync;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.documentcancellation.DocumentCancellationPopUp;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.client.views.paymentinfo.invoicedetails.InvoiceDetailsForm;
import com.slicktechnologies.client.views.paymentinfo.paymentdetails.PaymentDetailsForm;
import com.slicktechnologies.client.views.paymentinfo.paymentdetails.PaymentDetailsPresenter;
import com.slicktechnologies.client.views.paymentinfo.paymentdetails.PaymentDetailsSearchProxy;
import com.slicktechnologies.client.views.paymentinfo.vendorinvoicedetails.VendorInvoiceDetailsForm;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

public class PaymentListPresenter<genralInvoicePopup> extends TableScreenPresenter<CustomerPayment> implements ClickHandler{
	
	TableScreen<CustomerPayment>form;
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	static GWTCGlassPanel glassPanel=new GWTCGlassPanel();
	
	UpdateServiceAsync updateservice=GWT.create(UpdateService.class);
	
	paymentDetailsPopup popup = new paymentDetailsPopup();
	PopupPanel popupPanel;
	/**
	 * Date : 25-10-2017 BY ANIL
	 * On tick check box cell table selection code get executed to avoid that we have to set the value of 
	 * view document flag to false on cells updater event
	 */
	public static boolean viewDocumentFlag=true;
	/**
	 * End
	 *
	 */
	
	/*
	 * Added by Sheetal
	 */
	GeneralViewDocumentPopup genralInvoicePopup;
	public static PopupPanel generalPanel;
	/*
	 * End
	 */
	public PaymentListPresenter(TableScreen<CustomerPayment> view, CustomerPayment model) {
		super(view, model);
		form=view;
		/**
		 *  nidhi
		 *  24-07-2017
		 *  this method stop calling for not display payment list on page load
		 *  
		 */
		//view.retriveTable(getPaymentListQuery());
		
		/**
		 *  end
		 */
		
		setTableSelectionOnService();
		
		popup.getBtnOk().addClickHandler(this);
		popup.getBtnCancel().addClickHandler(this);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.PAYMENTLIST,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}
	
	public PaymentListPresenter(TableScreen<CustomerPayment> view,
			CustomerPayment model,MyQuerry querry) {
		super(view, model);
		form=view;
		view.retriveTable(querry);
		setTableSelectionOnService();
		
		popup.getBtnOk().addClickHandler(this);
		popup.getBtnCancel().addClickHandler(this);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.PAYMENTLIST,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}
	
	
	
	

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();
		
		if(text.equals("Process Payment Document")){
			reactOnPaymentProcess();
		}
	}

	private void reactOnPaymentProcess() {
		
		List<CustomerPayment>selectedPaymentList= getAllSelectedRecords();
		System.out.println("selectedPaymentList list size"+selectedPaymentList.size());
		if(selectedPaymentList.size()!=0){
			if(validateCustomerName(selectedPaymentList)){
				if(validatePaymentStatus(selectedPaymentList)){
				double payableAmt = getPayableAmtFromList(selectedPaymentList);	
				popup.getDbAmountPayable().setValue(payableAmt);
				popupPanel = new PopupPanel(true);
				popupPanel.add(popup);
				popupPanel.show();
				popupPanel.center();
				}else{
					form.showDialogMessage("Please select paymnet documents in created status only..!");
				}
			}else{
				form.showDialogMessage("Please select single customer details to proceed..!");
			}
		}
		else{
			form.showDialogMessage("Please select atleast one record to proceed..!");
		}
	}

	private boolean validatePaymentStatus(List<CustomerPayment> selectedPaymentList) {
		
		boolean flag=true;
		for (int i = 0; i < selectedPaymentList.size(); i++) {
			if(selectedPaymentList.get(i).getStatus().equalsIgnoreCase("Created")){
				flag=true;
			}
			else{
				flag=false;
				break;
			}
		}
		if(flag)
			return true;
		else
		    return false;
	}

	/**
	 * This method is added by rohan to calculate net payment amount
	 * @param selectedPaymentList 
	 * @return
	 */
	private double getPayableAmtFromList(List<CustomerPayment> selectedPaymentList) {
		double payableAmt= 0;
		
		for (int i = 0; i < selectedPaymentList.size(); i++) {
			payableAmt=payableAmt+selectedPaymentList.get(i).getPaymentAmt();
		}
		return payableAmt;
	}

	private boolean validateCustomerName(List<CustomerPayment> selectedPaymentList) {
		int customerId= selectedPaymentList.get(0).getCustomerId();
		
		boolean flag=true;
		
		for(int i=0;i<selectedPaymentList.size();i++){
			
			if(customerId==selectedPaymentList.get(i).getCustomerId()){
				flag=true;
			}else{
				flag=false;
				break;
				
			}
			
		}
		
		if(flag)
			return true;
		else
			return false;
		
	}

	private List<CustomerPayment> getAllSelectedRecords() {

		 List<CustomerPayment> lisbillingtable=form.getSuperTable().getDataprovider().getList();
		 ArrayList<CustomerPayment> selectedlist=new ArrayList<CustomerPayment>();
		 for(CustomerPayment doc:lisbillingtable)
		 {
			 if(doc.getRecordSelect()==true)
				 selectedlist.add(doc);
		 }
		 return selectedlist;
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void reactOnDownload() {
		
		final ArrayList<CustomerPayment> paylist=new ArrayList<CustomerPayment>();
		List<CustomerPayment> listbill=(List<CustomerPayment>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
		paylist.addAll(listbill); 
		csvservice.setpaymentlist(paylist, new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);
				
			}

			@Override
			public void onSuccess(Void result) {

				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "csvservlet"+"?type="+16;
				Window.open(url, "test", "enabled");
				
////				final String url=gwt + "CreateXLXSServlet"+"?type="+16;
////				Window.open(url, "test", "enabled");
//				
//				
//				ArrayList<String> summarypaymentlist = new ArrayList<String>();
//				summarypaymentlist.add("BRANCH");
//				summarypaymentlist.add("COUNT");
//				summarypaymentlist.add("AMOUNT RECEIVED");
//				/** date 03.03.2018 added by komal for revenue and tax calculation **/
//				summarypaymentlist.add("BASE");
//				summarypaymentlist.add("TAX");
//				summarypaymentlist.add("TDS");
//				summarypaymentlist.add("OUTSTANDING");
//				summarypaymentlist.add("Write OFF");
//
//				int summaryPaymentColumnCount = summarypaymentlist.size();
//				
//				HashMap<String, ArrayList<CustomerPayment>> paymnethashmaplist = new HashMap<String, ArrayList<CustomerPayment>>();
//				
//				for(int i=0;i<paylist.size();i++){
//					
//					ArrayList<CustomerPayment> list = new ArrayList<CustomerPayment>();
//					if(paymnethashmaplist.containsKey(paylist.get(i).getBranch())){
//						list = paymnethashmaplist.get(paylist.get(i).getBranch());
//						list.add(paylist.get(i));
//					}else{
//						list.add(paylist.get(i));
//					}
//					paymnethashmaplist.put(paylist.get(i).getBranch(), list);
//				}
//				
//				Set s = paymnethashmaplist.entrySet();
//				Iterator iterator = s.iterator();
//				
//				double sumTotalAmt=0;
//				double sumTDSTotalAmt=0;
//				double sumAmtReceived=0;
//				double sumbalancedAmount=0;
//				double sumCount =0;
//				/** date 03.03.2018 added by komal for revenue and tax calculation **/
//				double sumReceivedRevenue = 0;
//				double sumReceivedTax = 0;		
//				/**
//				 * end komal
//				 */
//
//				/**
//				 * Date 27-09-2018 By Vijay
//				 * Des Write off Amt
//				 */
//				double sumWriteOffAmt=0;
//				
//				while (iterator.hasNext()) {	
//					ArrayList<CustomerPayment> paymentlist  = new ArrayList<CustomerPayment>();
//					Map.Entry<String, ArrayList<CustomerPayment>> specificBranchlist = (Map.Entry<String, ArrayList<CustomerPayment>>)iterator.next();
//					paymentlist.addAll(specificBranchlist.getValue());
//					
//					double totalAmount = 0;
//					double tdsTotalAmt = 0;
//					double amountReceived = 0;
//					double balancedAmount = 0;
//					/** date 03.03.2018 added by komal for revenue and tax calculation **/
//					double receivedRevenue = 0;
//					double receivedTax = 0;		
//					/**
//					 * end komal
//					 */
//					/**
//					 * Date 27-09-2018 By Vijay
//					 * Des Write off Amt
//					 */
//					double totalWriteOffAmt=0;
//					
//					summarypaymentlist.add(paymentlist.get(0).getBranch());
//					summarypaymentlist.add(paymentlist.size()+"");
//					
//					HashSet<Integer> customerPayment = new HashSet<Integer>();
//					
//					for(int j=0;j<paymentlist.size();j++){
//						
//						if(!paymentlist.get(j).getStatus().equals("Cancelled")){
//
//						customerPayment.add(paymentlist.get(j).getInvoiceCount());
//						
//						
//						if(paymentlist.get(j).getStatus().equals("Closed")){
//							amountReceived += paymentlist.get(j).getPaymentReceived();
//							/** date 03.03.2018 added by komal for revenue and tax calculation **/
//							receivedRevenue += paymentlist.get(j).getReceivedRevenue();
//							receivedTax += paymentlist.get(j).getReceivedTax();		
//							/**
//							 * end komal
//							 */
//							 /**
//							 * Date 25-10-2018 By Vijay showing as per the amount recieved for TDS
//							 */
//							if(paymentlist.get(j).isTdsApplicable()){
//								tdsTotalAmt += paymentlist.get(j).getTdsTaxValue();
//							}
//						}
//						if(paymentlist.get(j).getStatus().equals("Created")){
//							balancedAmount += paymentlist.get(j).getPaymentAmt();
//						}
//						
//						}
//						if(paymentlist.get(j).getStatus().equals("Cancelled")){
//							totalWriteOffAmt += paymentlist.get(j).getPaymentAmt();
//						}
//						
//					}
//					
//					
//					summarypaymentlist.add(amountReceived+"");
//					/** date 03.03.2018 added by komal for revenue and tax calculation **/
//					summarypaymentlist.add(receivedRevenue+"");
//					summarypaymentlist.add(receivedTax+"");
//					/**
//					 * end komal
//					 */
//					summarypaymentlist.add(tdsTotalAmt+"");
//					summarypaymentlist.add(balancedAmount+"");
//					summarypaymentlist.add(totalWriteOffAmt+"");
//
//					sumCount +=paymentlist.size();
////					sumTotalAmt +=totalAmount;
//					sumTDSTotalAmt +=tdsTotalAmt;
//					sumAmtReceived +=amountReceived;
//					sumbalancedAmount +=balancedAmount;
//					/** date 03.03.2018 added by komal for revenue and tax calculation **/
//					sumReceivedRevenue += receivedRevenue;
//					sumReceivedTax += receivedTax;
//					/**
//					 * end komal
//					 */
//					/*** Date 27-09-2018 by Vijay for Cancelled total amount ***/ 
//					sumWriteOffAmt +=totalWriteOffAmt;
//					
//				}
//				
//				for(int i=0;i<summaryPaymentColumnCount;i++) {
//					summarypaymentlist.add("");
//				}
//				summarypaymentlist.add("TOTAL");
//				summarypaymentlist.add(sumCount+"");
//				summarypaymentlist.add(sumAmtReceived+"");
//				/** date 03.03.2018 added by komal for revenue and tax calculation **/
//				summarypaymentlist.add(sumReceivedRevenue+"");
//				summarypaymentlist.add(sumReceivedTax+"");
//				/**
//				 * end komal
//				 */
//				summarypaymentlist.add(sumTDSTotalAmt+"");
//				summarypaymentlist.add(sumbalancedAmount+"");
//				summarypaymentlist.add(sumWriteOffAmt+"");
//				
//				for(int i=0;i<summaryPaymentColumnCount;i++) {
//					summarypaymentlist.add(" ");
//				}		
//				
//				
//
//					/** payment list sorting by branch **/
//					Comparator<CustomerPayment> branchComparator = new Comparator<CustomerPayment>() {
//						public int compare(CustomerPayment s1, CustomerPayment s2) {
//						
//						String branch1 = s1.getBranch();
//						String branch2 = s2.getBranch();
//						
//						//ascending order
//						return branch1.compareTo(branch2);
//						}
//						
//						};
//						Collections.sort(paylist, branchComparator);
//					
//				/**
//				 * ends here
//				 */
//				
//				Date today = new Date();
//				/**
//				 *  Added By Priyanka - 18-08-2021
//				 *  
//				 */
//				Long companyId = paylist.get(0).getCompanyId();
//				
//				ArrayList<String> paymentlist = new ArrayList<String>();
//				paymentlist.add("BRANCH");
//				paymentlist.add("PAYMENT ID");
//				paymentlist.add("INVOICE ID");
//				paymentlist.add("INVOICE DATE");
//				paymentlist.add("PAYMENT DATE");
//				paymentlist.add("BP ID");
//				paymentlist.add("BP NAME");
//				paymentlist.add("CELL NUMBER");
//				paymentlist.add("ORDER ID");
//				paymentlist.add("ORDER START DATE");
//				paymentlist.add("ORDER END DATE");
//				paymentlist.add("ACCOUNT TYPE");
//				paymentlist.add("ORDER AMOUNT");
//				paymentlist.add("INVOICE AMOUNT");
//				paymentlist.add("INVOICE TYPE");
//				paymentlist.add("PAYMENT METHOD");
//				/** date 21.02.2018 added by komal for revenue and tax calcualtion **/
//				paymentlist.add("AMOUNT RECEIVED/PAID");
//				/**
//				 * Date 24-4-2018
//				 * By jayshree
//				 * Des.add sales person name
//				 */
//				paymentlist.add("SALES PERSON NAME");
//				paymentlist.add("ADJUSTED BASE");
//				paymentlist.add("ADJUSTED TAX");
//				/** 
//				 * end komal
//				 */
//				/*
//				 *  Added By Priyanka - 18/08/2021
//		         *  Des : Innovative : Rename TDS with WHT Requirmnet raised by Rahul
//				 */
//				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CustomerPayment", "RenameTDSWithWHT")){
//					paymentlist.add("WHT %");
//
//				}else{
//					paymentlist.add("TDS %");
//
//				}
//				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CustomerPayment", "RenameTDSWithWHT")){
//					paymentlist.add("WHT AMOUNT");
//				}else{
//					paymentlist.add("TDS AMOUNT");
//				}
//				/**
//				 *  End
//				 */
//				paymentlist.add("NET RECEIVED");
//				/** date 21.02.2018 added by komal for revenue and tax calcualtion **/
//				paymentlist.add("GROSS AMOUNT");
//				paymentlist.add("BASE AMOUNT");
//				paymentlist.add("TAX AMOUNT");
//				paymentlist.add("BALANCE AMOUNT");
//				paymentlist.add("STATUS");
//				paymentlist.add("SEGMENT");
//				/*
//				 *  1-07-2017
//				 *   nidhi
//				 */
//				paymentlist.add("SERVICE ID");
//				/*
//				 *  end
//				 */
//				
//				/** 08-11-2017 sagar sore**/
//				paymentlist.add("Aging");
//				paymentlist.add("TDS ID");
//				paymentlist.add("TDS Date");
//				paymentlist.add("COMMENT");
//				/**
//				 * Date 2-5-2018
//				 * by jayshree
//				 * add cheque date,number ,bank name requrment of rahul pawar sir
//				 */
//				paymentlist.add("CHEQUE DATE");
//				paymentlist.add("CHEQUE NUMBER");
//				paymentlist.add("CHEQUE BANK NAME");
//				/* end**/
//				
//				/**
//				 * Date 10-05-2018
//				 * Developer : Vijay
//				 */
//				paymentlist.add("Follow-Up Date");
//				/**
//				 * ends here
//				 */
//				paymentlist.add("Ref No.2");
//
//		        /**@author Sheetal : 09-06-2022
//				 * Des : Adding Payment Received Date and Digital-Reference No. ,requirement by ankita pest control**/
//				paymentlist.add("Payment Received Date");
//				paymentlist.add("Digital-Reference No");
//				
//				int paymentColumnCount = paymentlist.size();
//				
//				for(CustomerPayment payment:paylist)
//				{
//					
//					paymentlist.add(payment.getBranch());
//					paymentlist.add(payment.getCount()+"");
//					paymentlist.add(payment.getInvoiceCount()+"");
//
//					if(payment.getInvoiceDate()!=null){
//						paymentlist.add(AppUtility.parseDate(payment.getInvoiceDate(),"dd-MMM-yyyy"));
//					}
//					if(payment.getPaymentDate()!=null){
//						paymentlist.add(AppUtility.parseDate(payment.getPaymentDate(),"dd-MMM-yyyy"));
//					}else{
//						paymentlist.add("");
//
//					}
//					paymentlist.add(payment.getPersonInfo().getCount()+"");
//					paymentlist.add(payment.getPersonInfo().getFullName());
//					paymentlist.add(payment.getPersonInfo().getCellNumber()+"");
//					paymentlist.add(payment.getContractCount()+"");
//
//					/**
//					 * rohan 
//					 */
//					
//					if(payment.getContractStartDate()!=null){
//						paymentlist.add(AppUtility.parseDate(payment.getContractStartDate(),"dd-MMM-yyyy"));
//					}
//					else{
//						paymentlist.add("N.A");
//					}
//					if(payment.getContractEndDate()!=null){
//						paymentlist.add(AppUtility.parseDate(payment.getContractEndDate(),"dd-MMM-yyyy"));
//					}
//					else{
//						paymentlist.add("N.A");
//					}
//					paymentlist.add(payment.getAccountType());
//					paymentlist.add(payment.getTotalSalesAmount()+"");
//					paymentlist.add(payment.getInvoiceAmount()+"");
//					paymentlist.add(payment.getInvoiceType());
//					paymentlist.add(payment.getPaymentMethod());
//					paymentlist.add(payment.getPaymentReceived()+"");
//					/** Date 24-4-2018 By jayshree add sales person name**/
//					paymentlist.add(payment.getEmployee());
//					/** date 21.02.2018 added by komal for revenue and tax calcualtion **/
//					paymentlist.add(payment.getReceivedRevenue()+"");
//					paymentlist.add(payment.getReceivedTax()+"");
//					/** 
//					 * end komal
//					 */
//				
//					//Ashwini Patil Date:28-06-2022 TDS percentage were not getting printed on worksheet because of wrong condition. Corrected the condition
//					if(payment.getTdsPercentage()!=null && !payment.getTdsPercentage().equals("") )
//					{
//						paymentlist.add(payment.getTdsPercentage()+"");
//					}
//					else
//					{
//						paymentlist.add("0");
//					}
//					if(payment.getTdsTaxValue()!=0){
//						paymentlist.add(payment.getTdsTaxValue()+"");
//					}
//					else
//					{
//						paymentlist.add("0");
//					}
//					if(payment.getNetPay()!=0){
//						paymentlist.add(payment.getNetPay()+"");
//					}else
//					{
//						paymentlist.add("");
//					}
//					/** date 21.02.2018 added by komal for revenue and tax calcualtion **/
//					paymentlist.add(payment.getPaymentAmt()+"");
//					paymentlist.add(payment.getBalanceRevenue()+"");
//					paymentlist.add(payment.getBalanceTax()+"");
//					paymentlist.add(payment.getPaymentAmt()-payment.getPaymentReceived()+"");
//					/** 
//					 * end komal
//					 */
//					paymentlist.add(payment.getStatus());
//
//					
//				
//						/*
//						 *  nidhi
//						 *  1-07-2017
//						 */
//						if(payment.getSegment()!=null){
//							paymentlist.add(payment.getSegment()+"");
//						}else{
//							paymentlist.add("");
//						}
//						/*
//						 *  end
//						 */
//								
//						/**
//						 * Date : 17-07-2017 Nidhi
//						 */
//						if(payment.getRateContractServiceId()!=0){
//							paymentlist.add(payment.getRateContractServiceId()+"");
//						}
//						else{
//							paymentlist.add("");
//						}
//						/**
//						 * End
//						 */
//						
//						/**8-11-2017 sagar sore**/
//						if(payment.getPaymentDate()!=null)
//						{
//							long diff = today.getTime() - payment.getPaymentDate().getTime();
//							long diffDays = diff / (24 * 60 * 60 * 1000);
//							paymentlist.add(diffDays+"");
//
//						}
//						else
//							paymentlist.add("");
//						
//						
//						/******** Date 21-12-2017 added by vijay for TDS ID and TDS Date *********/
//						if(payment.getTdsId()!=0){
//							paymentlist.add(payment.getTdsId()+"");
//						}else{
//							paymentlist.add("");
//						}
//						
//						if(payment.getTdsDate()!=null){
//							paymentlist.add(AppUtility.parseDate(payment.getTdsDate(),"dd-MMM-yyyy"));
//						}else{
//							paymentlist.add("");
//						}
//						/** date 15.02.2018 added by komal for hvac **/
//						if(payment.getComment()!=null){
//							paymentlist.add(payment.getComment());
//						}else{
//							paymentlist.add("");
//						}
//						
//						/**
//						 * Date 2-5-2018
//						 * By jayshree
//						 * Des.add the cheque number ,date ,bank name
//						 */
//						
//						if(payment.getChequeDate()!=null){
//							paymentlist.add(AppUtility.parseDate(payment.getChequeDate(),"dd-MMM-yyyy"));
//						}else{
//							paymentlist.add("");
//						}
//						
//						if(payment.getChequeNo()!=null){
//							paymentlist.add(payment.getChequeNo()+"");
//						}else{
//							paymentlist.add("");
//						}
//						
//						if(payment.getBankName()!=null){
//							paymentlist.add(payment.getBankName()+"");
//						}else{
//							paymentlist.add("");
//						}
//						
//						/**
//						 * Date 10-05-2018
//						 * Developer : Vijay
//						 */
//						if(payment.getFollowUpDate()!=null){
//							paymentlist.add(AppUtility.parseDate(payment.getFollowUpDate(),"dd-MMM-yyyy"));
//						}else{
//							paymentlist.add("");
//						}
//						/**
//						 * ends here
//						 */
//						
//						/**End **/
//						if(payment.getRefNumberTwo()!=null){
//							paymentlist.add(payment.getRefNumberTwo());
//						}else{
//							paymentlist.add("");
//						}
//						
//						/**@author Sheetal : 09-06-2022
//						 * Des : Adding Payment Received Date and Digital-Reference No. ,requirement by ankita pest control**/
//						
//						if(payment.getAmountTransferDate()!=null) {
//							paymentlist.add(AppUtility.parseDate(payment.getAmountTransferDate(),"dd-MMM-yyyy"));
//
//						}else {
//							paymentlist.add("");
//						}
//						
//						if(payment.getReferenceNo()!=null) {
//							paymentlist.add(payment.getReferenceNo());
//						}else {
//							paymentlist.add("");
//						}
//						/**end**/
//					
//						
//					}
//				
//				csvservice.setExcelData(paymentlist, paymentColumnCount, AppConstants.PAYMENTDETAILS, new AsyncCallback<String>() {
//					
//					@Override
//					public void onSuccess(String result) {
//						// TODO Auto-generated method stub
//						
//						String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
//						final String url=gwt + "CreateXLXSServlet"+"?type="+16;
//						Window.open(url, "test", "enabled");
//					}
//					
//					@Override
//					public void onFailure(Throwable caught) {
//						// TODO Auto-generated method stub
//						
//					}
//				});
						
			}
		});
	}
	
	/*
	 * Method template to make a new model
	 */
	@Override
	protected void makeNewModel() {
		
		model=new CustomerPayment();
	}
	
	public static void initalize()
	{
//		glassPanel.show();
//		Timer timer=new Timer() 
//   	 	{
//			@Override
//			public void run() 
//			{
				PaymentListTable table=new PaymentListTable();
				PaymentListForm form=new PaymentListForm(table);
				
				PaymentDetailsSearchProxy.staticSuperTable=table;
				PaymentDetailsSearchProxy searchPopUp=new PaymentDetailsSearchProxy(false);
				searchPopUp.getDwnload().setVisible(false);
				form.setSearchpopupscreen(searchPopUp);
				/**Added by sheetal:13-01-2022
				 * Payment list window to occupy full screen
				 */
				int height = Window.getClientHeight();
				Console.log("Window Height : " + height);
				height = height * 80 / 100;
				Console.log("Updated Height : " + height);
				searchPopUp.getSupertable().getTable().setHeight(height+"px");
				/**end**/
				PaymentListPresenter presenter=new PaymentListPresenter(form, new CustomerPayment());
				form.setToViewState();
				/**
				 * Date : 29-07-2017 BY ANIL
				 * This line was added to update the name of screen loaded from any other screen or by clicking on payment details on account model
				 */
				AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Accounts/Payment List",Screen.PAYMENTLIST);
				/**
				 * END
				 */
				AppMemory.getAppMemory().stickPnel(form);
//				glassPanel.hide();
//			}
//		};
//		timer.schedule(3000); 
	}
	
	public static void initalize(MyQuerry querry)
	{
		PaymentListTable table=new PaymentListTable();
		PaymentListForm form=new PaymentListForm(table);
		PaymentListSearchPopUp.staticSuperTable=table;
		PaymentListSearchPopUp searchPopUp=new PaymentListSearchPopUp(false);
		form.setSearchpopupscreen(searchPopUp);
		/**Added by sheetal:13-01-2022
		 * Payment list window to occupy full screen
		 */
		int height = Window.getClientHeight();
		Console.log("Window Height : " + height);
		height = height * 80 / 100;
		Console.log("Updated Height : " + height);
		searchPopUp.getSupertable().getTable().setHeight(height+"px");
		/**end**/
		PaymentListPresenter presenter=new PaymentListPresenter(form, new CustomerPayment(),querry);
		form.setToNewState();
		/**
		 * Date : 29-07-2017 BY ANIL
		 * This line was added to update the name of screen loaded from any other screen or by clicking on payment details on account model
		 */
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Accounts/Payment List",Screen.PAYMENTLIST);
		/**
		 * END
		 */
		AppMemory.getAppMemory().stickPnel(form);
	}
	
	/*
	 * Method template to set the query object in specific to search criteria and return the same
	 * @return MyQuerry object
	 */
	public MyQuerry getPaymentListQuery()
	{
		MyQuerry quer=new MyQuerry();
		Vector<Filter> filtervec=new Vector<Filter>();
		List<String> statusList=new ArrayList<String>();
		statusList.add(CustomerPayment.CREATED);
		
		Filter temp=null;
		
		Date payDate=new Date();
		CalendarUtil.addDaysToDate(payDate, 1);
		
		temp=new Filter();
		temp.setLongValue(model.getCompanyId());
		temp.setQuerryString("companyId");
		filtervec.add(temp);
		
		temp=new Filter();
		temp.setList(statusList);
		temp.setQuerryString("status IN");
		filtervec.add(temp);
		
		/**
		 * Developed by : Rohan added this branch  
		 * 
		 */
		
		List<String> branchList ;
		branchList= new ArrayList<String>();
		System.out.println("item count in branch "+LoginPresenter.globalBranch.size());
		for (int i = 0; i < LoginPresenter.globalBranch.size(); i++) {
			branchList.add(LoginPresenter.globalBranch.get(i).getBusinessUnitName());
		}
		System.out.println("branchList list "+branchList.size());
		
		temp = new Filter();
		temp.setQuerryString("branch IN");
		temp.setList(branchList);
		filtervec.add(temp);
		/**
		 * ends here 
		 */
		
		temp=new Filter();
		temp.setDateValue(payDate);
		temp.setQuerryString("paymentDate <=");
		filtervec.add(temp);
		
		quer.setFilters(filtervec);
		quer.setQuerryObject(new CustomerPayment());
		
		
		return quer;
	}
	
	 public void setTableSelectionOnService()
	 {
		 final NoSelectionModel<CustomerPayment> selectionModelMyObj = new NoSelectionModel<CustomerPayment>();
	  
	     SelectionChangeEvent.Handler  tableHandler = new SelectionChangeEvent.Handler() 
	     {
	         @Override
	         public void onSelectionChange(SelectionChangeEvent event) 
	         {
				Timer timer1 = new Timer() {
					@Override
					public void run() {
						if(viewDocumentFlag){
//				        	final CustomerPayment entity=selectionModelMyObj.getLastSelectedObject();
//				        	AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
//				        	/** date 26.6.2018 added by komal for vendor payment**/
//				        	final PaymentDetailsForm form;
//				        	if(entity.getAccountType().equalsIgnoreCase(AppConstants.BILLINGACCOUNTTYPEAP)){
//				        		VendorInvoiceDetailsForm.flag=false;
//				        		form = PaymentDetailsPresenter.initalize(true);
//					        	AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Vendor Payment Details",Screen.VENDORPAYMENTDETAILS);
//				        	}else{
//					        	InvoiceDetailsForm.flag=false;
//					        	form = PaymentDetailsPresenter.initalize();
//					        	AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Payment Details",Screen.PAYMENTDETAILS);
//								
//				        	}
//				        	AppMemory.getAppMemory().stickPnel(form);
//			
//							Timer timer = new Timer() {
//								@Override
//								public void run() {
//									form.updateView(entity);
//									form.setToViewState();
//									PaymentDetailsPresenter presenter = (PaymentDetailsPresenter) form.getPresenter();
//									presenter.form.setAppHeaderBarAsPerStatus();
//								}
//							};
//				            timer.schedule(3000);  
							
							/*
							 * Added by Sheetal : 23-10-2021 for opening popup window
							 */
							
							CustomerPayment paymentEntity = selectionModelMyObj.getLastSelectedObject();
//						    AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Accounts/Payment Details",Screen.PAYMENTDETAILS);
							AppMemory.getAppMemory().currentScreen = Screen.PAYMENTDETAILS;

							genralInvoicePopup = new GeneralViewDocumentPopup();
							if(paymentEntity.getAccountType().equalsIgnoreCase(AppConstants.BILLINGACCOUNTTYPEAP)){
//				        		VendorInvoiceDetailsForm.flag=false;
//				        		form = PaymentDetailsPresenter.initalize(true);
					        	AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Vendor Payment Details",Screen.VENDORPAYMENTDETAILS);
				        	}else{
//					        	InvoiceDetailsForm.flag=false;
//					        	form = PaymentDetailsPresenter.initalize();
					        	AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Payment Details",Screen.PAYMENTDETAILS);
				        	}
			        	    genralInvoicePopup.setModel(AppConstants.PAYMENT,paymentEntity);
							generalPanel = new PopupPanel();
							generalPanel.add(genralInvoicePopup);
							generalPanel.show();
							generalPanel.center();
				        	
							viewDocumentFlag = true;
						    }		
					}
				};
				timer1.schedule(1000); 
	          }
	     };
	     // Add the handler to the selection model
	     selectionModelMyObj.addSelectionChangeHandler( tableHandler );
	     // Add the selection model to the table
	     form.getSuperTable().getTable().setSelectionModel(selectionModelMyObj);
	     
	 }

	 @Override
		public void onClick(ClickEvent event) {
			super.onClick(event);
			
			if (event.getSource() == popup.btnOk) {
				
				System.out.println("payable -"+popup.getDbAmountPayable().getValue());
				System.out.println("received -"+popup.getDbAmountReceived().getValue());
				if(popup.getDbAmountPayable().getValue().equals(popup.getDbAmountReceived().getValue())){
					if(popup.getOlbPaymentMethod().getSelectedIndex()!=0){
						if(popup.getDbAmountReceived().getValue()!=0){
							if(validateMethod()){
								closedPaymentsWithReceivedAmount(popup.getTbbankAccNo().getValue(),popup.getTbbankName().getValue(),popup.getTbbankBranch().getValue()
										,popup.getTbchequeNo().getValue(),popup.getDbChequeDate().getValue(),popup.getDbtransferDate().getValue()
										,popup.getTbreferenceno().getValue(),popup.getTbchequeissuedBy().getValue(),popup.getOlbPaymentMethod().getValue(popup.getOlbPaymentMethod().getSelectedIndex()));
								popupPanel.hide();
							}
							
						}else{
							form.showDialogMessage("Amount received can not be zero..!");
						}
					}else{
						form.showDialogMessage("Please select payment method..!");
					}
				}
				else{
					form.showDialogMessage("Amount received must be equal to amount payable..!");
				}
				
			}
			
			if (event.getSource() == popup.btnCancel) {
				popupPanel.hide();
			}
	 }

	private boolean validateMethod() {
		boolean validatePaymentMethods=true;
		if(popup.getOlbPaymentMethod().getValue( ).equalsIgnoreCase("Cheque"))
		{
			if(this.popup.getTbchequeNo().getValue()==null || this.popup.getTbchequeNo().getValue().equals(""))
			{
				validatePaymentMethods=false;
				form.showDialogMessage("Cheque No is Mandatory!");
			}
			if(popup.getDbChequeDate().getValue()==null )
			{
				validatePaymentMethods=false;
				form.showDialogMessage("Cheque Date is Mandatory!");
			}
		}
		else if(this.popup.getOlbPaymentMethod().getValue().equalsIgnoreCase("ECS"))
		{
//			if(this.tbbankAccNo.getValue()==null||this.tbbankAccNo.getValue().equals(""))
//			{
//				validatePaymentMethods=false;
//				this.showDialogMessage("Bank Account is Mandatory!");
//			}
//			if(this.tbbankName.getValue()==null||this.tbbankName.getValue().equals("")&&!tbbankAccNo.getValue().equals(""))
//			{
//				validatePaymentMethods=false;
//				this.showDialogMessage("Bank Name is Mandatory!");
//			}
			if(popup.getTbbankBranch().getValue()==null||popup.getTbbankBranch().getValue().equals(""))
			{
				validatePaymentMethods=false;
				form.showDialogMessage("Bank Branch is Mandatory!");
			}
			if(popup.getTbreferenceno().getValue()==null||popup.getTbreferenceno().getValue().equals(""))
			{
				validatePaymentMethods=false;
				form.showDialogMessage("Reference No is Mandatory!");
			}
			if(popup.getDbtransferDate().getValue()==null||popup.getDbtransferDate().getValue().equals(""))
			{
				validatePaymentMethods=false;
				form.showDialogMessage("Transfer Date is Mandatory!");
			}
		}
		
		if(validatePaymentMethods)
			return true;
		else
		return false;
	}

	private void closedPaymentsWithReceivedAmount(String accntNumber, String bankName,String branchName,String chequeNumber, Date chequeDt, Date transferDt, String refNumber, String issueBy,String paymentMethod) {
		form.showWaitSymbol();
		List<CustomerPayment>selectedPaymentList= getAllSelectedRecords();
		updateservice.saveMultiplePaymentDetails(selectedPaymentList,accntNumber,bankName,branchName,chequeNumber,chequeDt,transferDt,refNumber,issueBy,paymentMethod,new AsyncCallback<String>() {
			
			@Override
			public void onSuccess(String result) {
				form.hideWaitSymbol();
				form.showDialogMessage("Payment submited successfully");
			}
			
			@Override
			public void onFailure(Throwable caught) {
				
				form.showDialogMessage("An-expected error occured..!");
			}
		});
	}
	/** date 25.6.2018 added by komal for vendor list **/
	public static void initalize(boolean flag)
	{
				PaymentListTable table=new PaymentListTable();
				PaymentListForm form=new PaymentListForm(table);
				
				PaymentDetailsSearchProxy.staticSuperTable=table;
				PaymentDetailsSearchProxy searchPopUp=new PaymentDetailsSearchProxy(AppConstants.BILLINGACCOUNTTYPEAP , false);
				searchPopUp.getDwnload().setVisible(false);
				form.setSearchpopupscreen(searchPopUp);
				PaymentListPresenter presenter=new PaymentListPresenter(form, new CustomerPayment());
				form.setToViewState();
				/**
				 * Date : 29-07-2017 BY ANIL
				 * This line was added to update the name of screen loaded from any other screen or by clicking on payment details on account model
				 */
				AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Accounts/Vendor Payment List",Screen.VENDORPAYMENTLIST);
				/**
				 * END
				 */
				AppMemory.getAppMemory().stickPnel(form); 
	}
	
	public static void initalize(MyQuerry querry , boolean flag)
	{
		PaymentListTable table=new PaymentListTable();
		PaymentListForm form=new PaymentListForm(table);
		PaymentListSearchPopUp.staticSuperTable=table;
		PaymentListSearchPopUp searchPopUp=new PaymentListSearchPopUp(AppConstants.BILLINGACCOUNTTYPEAP , false);
		form.setSearchpopupscreen(searchPopUp);
		PaymentListPresenter presenter=new PaymentListPresenter(form, new CustomerPayment(),querry);
		form.setToNewState();
		/**
		 * Date : 29-07-2017 BY ANIL
		 * This line was added to update the name of screen loaded from any other screen or by clicking on payment details on account model
		 */
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Accounts/Vendor Payment List",Screen.VENDORPAYMENTLIST);
		/**
		 * END
		 */
		AppMemory.getAppMemory().stickPnel(form);
	}

	



}
