package com.slicktechnologies.client.views.paymentinfo.paymentlist;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

public class PaymentListTable extends SuperTable<CustomerPayment> {
	
	TextColumn<CustomerPayment> getCountColumn;
	TextColumn<CustomerPayment> invoiceIdColumn;
	TextColumn<CustomerPayment> invoiceTypeColumn;
	TextColumn<CustomerPayment> customerIdColumn;
	TextColumn<CustomerPayment> customerNameColumn;
	TextColumn<CustomerPayment> customerCellColumn;
	TextColumn<CustomerPayment> contractCountColumn;
	TextColumn<CustomerPayment> contractSalesAmount;
	TextColumn<CustomerPayment> invoiceAmtColumn;
	TextColumn<CustomerPayment> billingPeriodColumn;//Ashwini Patil Date:14-02-2023
	TextColumn<CustomerPayment> paymentMethodColumn;
	TextColumn<CustomerPayment> paymentReceivedAmtColumn;
	TextColumn<CustomerPayment> payableAmountColumn;
	TextColumn<CustomerPayment> accountTypeColumn;
	TextColumn<CustomerPayment> statusColumn;
	TextColumn<CustomerPayment> branchColumn;
	/**17-10-2017 sagar sore**/
	TextColumn<CustomerPayment> paymentDateColumn;
	/*
	 *  nidhi
	 *   1-07-2017
	 */
	TextColumn<CustomerPayment> segmentColumn;
	/*
	 *  end
	 */
	
	/*
	 *  nidhi
	 *   18-07-2017 
	 *   display service Id 
	 */
	TextColumn<CustomerPayment> serviceIdColumn;
	/*
	 *  end
	 */
	
	// rohan added this code for adding selection in payment list
	private Column<CustomerPayment, Boolean> checkRecord;

	/**
	 *  nidhi
	 *  24-07-2017
	 *   for display service quantity and unit
	 */
	TextColumn<CustomerPayment> refColumn;
	/*
	 *  end
	 */
	
	/** 03-10-2017 sagar sore [adding Number range column in Invoice list ]**/
	TextColumn<CustomerPayment> numberRangeColumn;
	/** date 1.11.2018 added by komal for type of order and sub bill type **/
	TextColumn<CustomerPayment> getColTypeOfOrder;
	TextColumn<CustomerPayment> getColSubBillType;
	TextColumn<CustomerPayment> invoiceDateColumn;
	NumberFormat df=NumberFormat.getFormat("0.00");
	public PaymentListTable() {
		super();
	}
	
	@Override
	public void createTable() {
		
/**03-10-2017 sagar sore [number range column added and addColumnCustomerId moved to right]**/
		/**old code**/
//		addColumnCheckBox();
//		addColumnCount();
//		addColumnInvoiceCount();
//		addColumnInvoiceType();
//		addColumnCustomerId();
//		addColumnCustomerName();
//		addColumnCustomerPhone();
//		addColumnAccountType();
//		addColumnContractCount();
//		addColumnContractSalesAmount();
//		addColumnInvoiceAmount();
//		addColumnPayableAmount();
//		addColumnBranch();
//		addColumnPaymentMethod();
//		addColumnPaymentReceived();
//		addColumnStatus();

		addColumnCheckBox();
//		addColumnCount();
//		/**17-10-2017 sagar sore [To add payment date column]**/
//		addColumnPaymentDate();
//		addColumnInvoiceCount();	
//		addColumnInvoiceType();
//		addColumnCustomerName();
//		addColumnCustomerPhone();
//		addColumnAccountType();
//		addColumnNumberRange();
//		addColumnContractCount();
//		addColumnContractSalesAmount();
//		addColumnInvoiceAmount();
//		addColumnPayableAmount();
//		addColumnBranch();
//		addColumnCustomerId();
//		addColumnPaymentMethod();
//		addColumnPaymentReceived();
//		addColumnStatus();
//		/*
//		 *  nidhi
//		 */
//		addColumnServiceId();
//		/*
//		 *  end
//		 */
//		/*
//		 *  nidhi
//		 */
//		addColumnSegment();
//		/*
//		 *  end
//		 */
//		/**
//		 *  nidhi
//		 *  for quantity and uom display
//		 *  24-07-2017
//		 */
//		addColumnRefNo();
//		/**
///		 *  end
//		 */
//		getColTypeOfOrder();
//		getColSubBillType();
		
		/**
		 * Added by Sheetal -> 18-10-2021
         * Des: Payment list Column realignment
		 */
		addColumnCount();
		addColumnPaymentDate();
		addColumnStatus();
		addColumnCustomerName();
		addColumnCustomerPhone();
		addColumnPayableAmount();
		addColumnPaymentReceived();
		addColumnInvoiceAmount();
		addColumnBillingPeriod();
		addColumnContractSalesAmount();
		addColumnPaymentMethod();
		getColSubBillType();
		addColumnRefNo();
		addColumnNumberRange();
		addColumnBranch();
		addColumnInvoiceCount();
		addColumnInvoiceDate();
		addColumnInvoiceType();
		addColumnContractCount();
		addColumnCustomerId();
		addColumnServiceId();
		addColumnSegment();
		getColTypeOfOrder();
		addColumnAccountType();
		/**
		  *  End
		  */
		 
		
		setFieldUpdateOnCheckbox();
	}
	

	public void addColumnServiceId()
	{
		serviceIdColumn =new TextColumn<CustomerPayment>() {

			@Override
			public String getValue(CustomerPayment object) {
				return object.getRateContractServiceId()+"";
			}
		};
		
		table.addColumn(serviceIdColumn,"Service Id");
		table.setColumnWidth(serviceIdColumn, 100, Unit.PX);
		serviceIdColumn.setSortable(true);
	}	
	
	public void addColumnSegment()
	{
		segmentColumn=new TextColumn<CustomerPayment>() {

			@Override
			public String getValue(CustomerPayment object) {
				return object.getSegment();
			}
		};
		
		table.addColumn(segmentColumn,"Segment");
		table.setColumnWidth(segmentColumn, 100, Unit.PX);
		segmentColumn.setSortable(true);
	}	
	
	public void addColumnRefNo()
	{
		refColumn=new TextColumn<CustomerPayment>() {

			@Override
			public String getValue(CustomerPayment object) {
				return object.getRefNumber();
			}
		};
		
		table.addColumn(refColumn,"REFERANCE NO.");
		table.setColumnWidth(refColumn, 100, Unit.PX);
		refColumn.setSortable(true);
	}
	
	private void setFieldUpdateOnCheckbox() 

	{
		System.out.println("In Field Updator");
		checkRecord.setFieldUpdater(new FieldUpdater<CustomerPayment, Boolean>() {
		
		@Override
		public void update(int index, CustomerPayment object, Boolean value) {
			try{
				Boolean val1=(value);
				object.setRecordSelect(val1);
				/**
				 * Date :25-10-2017 By ANIL
				 */
				PaymentListPresenter.viewDocumentFlag=false;
				/**
				 * End
				 */
			}
			catch(Exception e)
			{
			}
			table.redrawRow(index);
		
		}
	   });
	}
	
	
	private void addColumnCheckBox()
	{
		CheckboxCell checkCell= new CheckboxCell();
		checkRecord = new Column<CustomerPayment, Boolean>(checkCell) {

			@Override
			public Boolean getValue(CustomerPayment object) {
				
				return object.getRecordSelect();
			}
		};
		table.addColumn(checkRecord,"Select");
		table.setColumnWidth(checkRecord, 60, Unit.PX);
	}
	
	public void addColumnCount()
	{
		getCountColumn=new TextColumn<CustomerPayment>() {

			@Override
			public String getValue(CustomerPayment object) {
				return object.getCount()+"";
			}
		};
		table.addColumn(getCountColumn,"Payment ID");
		table.setColumnWidth(getCountColumn, 100, Unit.PX);
		getCountColumn.setSortable(true);
	}

	public void addColumnBranch()
	{
		branchColumn=new TextColumn<CustomerPayment>() {

			@Override
			public String getValue(CustomerPayment object) {
				return object.getBranch();
			}
		};
		
		table.addColumn(branchColumn,"Branch");
		table.setColumnWidth(branchColumn, 90, Unit.PX);
		branchColumn.setSortable(true);
	}		
	
	public void addColumnInvoiceCount()
	{
		
		invoiceIdColumn=new TextColumn<CustomerPayment>() {

			@Override
			public String getValue(CustomerPayment object) {
				return object.getInvoiceCount()+"";
			}
		};
		table.addColumn(invoiceIdColumn,"Inv. ID");
		table.setColumnWidth(invoiceIdColumn, 100, Unit.PX);
		invoiceIdColumn.setSortable(true);
	}
	
	public void addColumnInvoiceType()
	{
		
		invoiceTypeColumn=new TextColumn<CustomerPayment>() {

			@Override
			public String getValue(CustomerPayment object) {
				return object.getInvoiceType();
			}
		};
		table.addColumn(invoiceTypeColumn,"Invoice Doc Type");
		table.setColumnWidth(invoiceTypeColumn, 120, Unit.PX);
		invoiceTypeColumn.setSortable(true);
	}

	private void addColumnCustomerId() {
		customerIdColumn=new TextColumn<CustomerPayment>() {

			@Override
			public String getValue(CustomerPayment object) {
				return object.getCustomerCount()+"";
			}
		};
		
		table.addColumn(customerIdColumn,"Customer ID");
		table.setColumnWidth(customerIdColumn, 100, Unit.PX);
		customerIdColumn.setSortable(true);
		
	}
	
	public void addColumnCustomerName()
	{
		customerNameColumn=new TextColumn<CustomerPayment>() {

			@Override
			public String getValue(CustomerPayment object) {
				return object.getName();
			}
		};
		
		table.addColumn(customerNameColumn,"Customer");
		table.setColumnWidth(customerNameColumn, 120, Unit.PX);
		customerNameColumn.setSortable(true);
	}
	
	public void addColumnCustomerPhone()
	{
		customerCellColumn=new TextColumn<CustomerPayment>() {

			@Override
			public String getValue(CustomerPayment object) {
				return object.getCellNumber()+"";
			}
		};
		
		table.addColumn(customerCellColumn,"Mobile");
		table.setColumnWidth(customerCellColumn, 120, Unit.PX);
		customerCellColumn.setSortable(true);
	}
	
	public void addColumnContractCount()
	{
		contractCountColumn=new TextColumn<CustomerPayment>() {

			@Override
			public String getValue(CustomerPayment object) {
				    return object.getContractCount()+"";
			}
		};
		
		table.addColumn(contractCountColumn,"Order ID");
		table.setColumnWidth(contractCountColumn, 100, Unit.PX);
		contractCountColumn.setSortable(true);
	}
	
	public void addColumnContractSalesAmount()
	{
		contractSalesAmount=new TextColumn<CustomerPayment>() {

			@Override
			public String getValue(CustomerPayment object) {
				
				return object.getTotalSalesAmount()+"";
			}
		};
		
		table.addColumn(contractSalesAmount,"Order Amount");
		table.setColumnWidth(contractSalesAmount, 100, Unit.PX);
		contractSalesAmount.setSortable(true);
	}

	public void addColumnInvoiceAmount()
	{
		invoiceAmtColumn=new TextColumn<CustomerPayment>() {

			@Override
			public String getValue(CustomerPayment object) {
				return object.getInvoiceAmount()+"";
			}
		};
		
		table.addColumn(invoiceAmtColumn,"Inv. Amt");
		table.setColumnWidth(invoiceAmtColumn, 100, Unit.PX);
		invoiceAmtColumn.setSortable(true);
	}
	
	//Ashwini Patil Date:14-02-2023
	public void addColumnBillingPeriod()
	{
		billingPeriodColumn=new TextColumn<CustomerPayment>() {

			@Override
			public String getValue(CustomerPayment object) {
				if(object.getBillingPeroidFromDate()!=null&&object.getBillingPeroidToDate()!=null)
					return AppUtility.parseDate(object.getBillingPeroidFromDate())+" - "+AppUtility.parseDate(object.getBillingPeroidToDate());
				else
					return "";
			}
		};
		
		table.addColumn(billingPeriodColumn,"Billing Period");
		table.setColumnWidth(billingPeriodColumn, 100, Unit.PX);
	}
	
	public void addColumnPayableAmount()
	{
		payableAmountColumn=new TextColumn<CustomerPayment>() {

			@Override
			public String getValue(CustomerPayment object) {
				//Ashwini Patil Date:25-09-2023 On payment details screen amount is getting displayed in decimals and on payment list screen it is showing rounded amount
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CONTRACT", "PC_NO_ROUNDOFF_INVOICE_CONTRACT")){
					return df.format(object.getPaymentAmtInDecimal())+"";					
				}else
					return object.getPaymentAmt()+"";
			}
		};
		
		table.addColumn(payableAmountColumn,"Payable Amt");
		table.setColumnWidth(payableAmountColumn, 120, Unit.PX);
		payableAmountColumn.setSortable(true);
	}

	public void addColumnPaymentMethod()
	{
		paymentMethodColumn=new TextColumn<CustomerPayment>() {

			@Override
			public String getValue(CustomerPayment object) {
				return object.getPaymentMethod();
			}
		};
		
		table.addColumn(paymentMethodColumn,"Payment Method");
		table.setColumnWidth(paymentMethodColumn, 120, Unit.PX);
		paymentMethodColumn.setSortable(true);
	}		
	
	public void addColumnPaymentReceived()
	{
		paymentReceivedAmtColumn=new TextColumn<CustomerPayment>() {

			@Override
			public String getValue(CustomerPayment object) {
				return object.getPaymentReceived()+"";
			}
		};
		
		table.addColumn(paymentReceivedAmtColumn,"Paid Amt");
		table.setColumnWidth(paymentReceivedAmtColumn, 100, Unit.PX);
		paymentReceivedAmtColumn.setSortable(true);
	}		
	
	public void addColumnAccountType()
	{
		accountTypeColumn=new TextColumn<CustomerPayment>() {

			@Override
			public String getValue(CustomerPayment object) {
				return object.getAccountType();
			}
		};
		
		table.addColumn(accountTypeColumn,"Acc. Type");
		table.setColumnWidth(accountTypeColumn, 90, Unit.PX);
		accountTypeColumn.setSortable(true);
	}
	
	
	public void addColumnStatus()
	{
		statusColumn=new TextColumn<CustomerPayment>() {

			@Override
			public String getValue(CustomerPayment object) {
				return object.getStatus();
			}
		};
		
		table.addColumn(statusColumn,"Status");
		table.setColumnWidth(statusColumn, 100, Unit.PX);
		statusColumn.setSortable(true);
	}	
	
	public void addColumnSorting(){
		addSortinggetCount();
		addSortinggetInvoiceCount();
		addSortinggetInvoiceType();
		addSortinggetCustomerId();
		addSortinggetCustomerFullName();
		addSortinggetCustomerCellNumber();
		addSortingAccountType();
		addSortinggetContractCount();
		addSortinggetSalesAmount();
		addSortinggetInvoiceAmount();
		addSortinggetPaymentMethod();
		addSortinggetReceivedAmount();
		addSortinggetStatus();
		addSortinggetBranch();
		
		/*
		 *  nidhi
		 */
		addSortinggetSegment();
		/*
		 * end
		 */
		addSortinggetServiceId();
		
		/*
		 *  nidhi
		 *  24-07-2017
		 *  
		 */
		addSortingRefNo();
		/**
		 *  end
		 */
		/**03-10-2017 sagar sore [adding number range column]**/
		addSortinggetNumberRange();
		/**17-10-2017 sagar sore[ to add payment date column sorting]**/
        addSortinggetPaymentDate();
        addSortingTypeOfOrder();
		addSortingSubBillType();
		
		addSortingOnInvoiceDate();
	}
	
	protected void addSortinggetServiceId()
	{
		List<CustomerPayment> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerPayment>(list);
		columnSort.setComparator(serviceIdColumn, new Comparator<CustomerPayment>()
				{
			@Override
			public int compare(CustomerPayment e1,CustomerPayment e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetSegment()
	{
		List<CustomerPayment> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerPayment>(list);
		columnSort.setComparator(segmentColumn, new Comparator<CustomerPayment>()
				{
			@Override
			public int compare(CustomerPayment e1,CustomerPayment e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getSegment()!=null && e2.getSegment()!=null){
						return e1.getSegment().compareTo(e2.getSegment());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addSortinggetBranch()
	{
		List<CustomerPayment> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerPayment>(list);
		columnSort.setComparator(branchColumn, new Comparator<CustomerPayment>()
				{
			@Override
			public int compare(CustomerPayment e1,CustomerPayment e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBranch()!=null && e2.getBranch()!=null){
						return e1.getBranch().compareTo(e2.getBranch());
					}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetCount()
	{
		List<CustomerPayment> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerPayment>(list);
		columnSort.setComparator(getCountColumn, new Comparator<CustomerPayment>()
				{
			@Override
			public int compare(CustomerPayment e1,CustomerPayment e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetInvoiceCount()
	{
		List<CustomerPayment> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerPayment>(list);
		columnSort.setComparator(invoiceIdColumn, new Comparator<CustomerPayment>()
				{
			@Override
			public int compare(CustomerPayment e1,CustomerPayment e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getInvoiceCount()== e2.getInvoiceCount()){
						return 0;}
					if(e1.getInvoiceCount()> e2.getInvoiceCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetInvoiceType()
	{
		List<CustomerPayment> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerPayment>(list);
		columnSort.setComparator(invoiceTypeColumn, new Comparator<CustomerPayment>()
				{
			@Override
			public int compare(CustomerPayment e1,CustomerPayment e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getInvoiceType()!=null && e2.getInvoiceType()!=null){
						return e1.getInvoiceType().compareTo(e2.getInvoiceType());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortingRefNo()
	{
		List<CustomerPayment> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerPayment>(list);
		columnSort.setComparator(refColumn, new Comparator<CustomerPayment>()
				{
			@Override
			public int compare(CustomerPayment e1,CustomerPayment e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getRefNumber()!=null && e2.getRefNumber()!=null){
						return e1.getRefNumber().compareTo(e2.getRefNumber());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addSortingAccountType()
	{
		List<CustomerPayment> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerPayment>(list);
		columnSort.setComparator(accountTypeColumn, new Comparator<CustomerPayment>()
				{
			@Override
			public int compare(CustomerPayment e1,CustomerPayment e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getAccountType()!=null && e2.getAccountType()!=null){
						return e1.getAccountType().compareTo(e2.getAccountType());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}

	
	protected void addSortinggetCustomerId()
	{
		List<CustomerPayment> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerPayment>(list);
		columnSort.setComparator(customerIdColumn, new Comparator<CustomerPayment>()
				{
			@Override
			public int compare(CustomerPayment e1,CustomerPayment e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getPersonInfo().getCount()== e2.getPersonInfo().getCount()){
						return 0;}
					if(e1.getPersonInfo().getCount()> e2.getPersonInfo().getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetCustomerFullName()
	{
		List<CustomerPayment> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerPayment>(list);
		columnSort.setComparator(customerNameColumn, new Comparator<CustomerPayment>()
				{
			@Override
			public int compare(CustomerPayment e1,CustomerPayment e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getPersonInfo().getFullName()!=null && e2.getPersonInfo().getFullName()!=null){
						return e1.getPersonInfo().getFullName().compareTo(e2.getPersonInfo().getFullName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetCustomerCellNumber()
	{
		List<CustomerPayment> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerPayment>(list);
		columnSort.setComparator(customerCellColumn, new Comparator<CustomerPayment>()
				{
			@Override
			public int compare(CustomerPayment e1,CustomerPayment e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getPersonInfo().getCellNumber()== e2.getPersonInfo().getCellNumber()){
						return 0;}
					if(e1.getPersonInfo().getCellNumber()> e2.getPersonInfo().getCellNumber()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetContractCount()
	{
		List<CustomerPayment> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerPayment>(list);
		columnSort.setComparator(contractCountColumn, new Comparator<CustomerPayment>()
				{
			@Override
			public int compare(CustomerPayment e1,CustomerPayment e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getContractCount()== e2.getContractCount()){
						return 0;}
					if(e1.getContractCount()> e2.getContractCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetSalesAmount()
	{
		List<CustomerPayment> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerPayment>(list);
		columnSort.setComparator(contractSalesAmount, new Comparator<CustomerPayment>()
				{
			@Override
			public int compare(CustomerPayment e1,CustomerPayment e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getTotalSalesAmount()== e2.getTotalSalesAmount()){
						return 0;}
					if(e1.getTotalSalesAmount()> e2.getTotalSalesAmount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetInvoiceAmount()
	{
		List<CustomerPayment> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerPayment>(list);
		columnSort.setComparator(invoiceAmtColumn, new Comparator<CustomerPayment>()
				{
			@Override
			public int compare(CustomerPayment e1,CustomerPayment e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getInvoiceAmount()== e2.getInvoiceAmount()){
						return 0;}
					if(e1.getInvoiceAmount()> e2.getInvoiceAmount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetPaymentMethod()
	{
		List<CustomerPayment> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerPayment>(list);
		columnSort.setComparator(paymentMethodColumn, new Comparator<CustomerPayment>()
				{
			@Override
			public int compare(CustomerPayment e1,CustomerPayment e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getPaymentMethod()!=null && e2.getPaymentMethod()!=null){
						return e1.getPaymentMethod().compareTo(e2.getPaymentMethod());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetReceivedAmount()
	{
		List<CustomerPayment> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerPayment>(list);
		columnSort.setComparator(paymentReceivedAmtColumn, new Comparator<CustomerPayment>()
				{
			@Override
			public int compare(CustomerPayment e1,CustomerPayment e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getPaymentReceived()== e2.getPaymentReceived()){
						return 0;}
					if(e1.getPaymentReceived()> e2.getPaymentReceived()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetStatus()
	{
		List<CustomerPayment> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerPayment>(list);
		columnSort.setComparator(statusColumn, new Comparator<CustomerPayment>()
				{
			@Override
			public int compare(CustomerPayment e1,CustomerPayment e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getStatus()!=null && e2.getStatus()!=null){
						return e1.getStatus().compareTo(e2.getStatus());
					}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	/**03-10-2017 sagar sore [To create number range column and sorting it]**/
	public void addColumnNumberRange()
	{
		numberRangeColumn=new TextColumn<CustomerPayment>() {

			@Override
			public String getValue(CustomerPayment object) {
				return object.getNumberRange();
			}
		};
		
		table.addColumn(numberRangeColumn,"Number Range");
		table.setColumnWidth(numberRangeColumn, 120, Unit.PX);
		numberRangeColumn.setSortable(true);
	}	
	
	protected void addSortinggetNumberRange()
	{
		List<CustomerPayment> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerPayment>(list);
		columnSort.setComparator(numberRangeColumn, new Comparator<CustomerPayment>()
				{
			@Override
			public int compare(CustomerPayment e1,CustomerPayment e2)
			{
				if (e1 != null && e2 != null) {
					
					if (e1.getNumberRange() == null && e2.getNumberRange() == null){
						return 0;
					}
					if(e1.getNumberRange() == null) return 1;							
					if(e2.getNumberRange() == null) return -1;							
					return e1.getNumberRange().compareTo(e2.getNumberRange());

				}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	/**end**/	

	/**17-07-2017 sagar sore {To add payment date column and column sorting}**/
	public void addColumnPaymentDate()
	{
		paymentDateColumn=new TextColumn<CustomerPayment>() {

			@Override
			public String getValue(CustomerPayment object) {
				if(object.getPaymentDate()!=null)
				{
				    String serDate=AppUtility.parseDate(object.getPaymentDate());
				    return serDate;
				}
				return "Not Set";
			}
		};
		
		table.addColumn(paymentDateColumn,"Payment Date");
		table.setColumnWidth(paymentDateColumn, 100, Unit.PX);
        paymentDateColumn.setSortable(true);
		
			}	
	
	protected void addSortinggetPaymentDate()
	{
		List<CustomerPayment> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerPayment>(list);
		columnSort.setComparator(paymentDateColumn, new Comparator<CustomerPayment>()
				{
			@Override
			public int compare(CustomerPayment e1,CustomerPayment e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getPaymentDate()!=null && e2.getPaymentDate()!=null){
						return e1.getPaymentDate().compareTo(e2.getPaymentDate());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);


	}
	/**end**/	

	
	@Override
	protected void initializekeyprovider() {
		
	}

	@Override
	public void addFieldUpdater() {
		
	}

	@Override
	public void setEnable(boolean state) {
		
	}

	@Override
	public void applyStyle() {
		
	}
	/** date 1.11.2018 added by komal for type of order and sub bill type **/
	public void getColTypeOfOrder()
	{
		getColTypeOfOrder=new TextColumn<CustomerPayment>() {

			@Override
			public String getValue(CustomerPayment object) {
				return object.getTypeOfOrder();
			}
		};
		
		table.addColumn(getColTypeOfOrder,"Type Of Order");
		table.setColumnWidth(getColTypeOfOrder, 120, Unit.PX);
		getColTypeOfOrder.setSortable(true);
	}	
	
	protected void addSortingTypeOfOrder()
	{
		List<CustomerPayment> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerPayment>(list);
		columnSort.setComparator(getColTypeOfOrder, new Comparator<CustomerPayment>()
				{
			@Override
			public int compare(CustomerPayment e1,CustomerPayment e2)
			{
				if (e1 != null && e2 != null) {
					
					if (e1.getNumberRange() == null && e2.getNumberRange() == null){
						return 0;
					}
					if(e1.getNumberRange() == null) return 1;							
					if(e2.getNumberRange() == null) return -1;							
					return e1.getNumberRange().compareTo(e2.getNumberRange());

				}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	public void getColSubBillType()
	{
		getColSubBillType=new TextColumn<CustomerPayment>() {

			@Override
			public String getValue(CustomerPayment object) {
				return object.getSubBillType();
			}
		};
		
		table.addColumn(getColSubBillType,"Sub Bill Type");
		table.setColumnWidth(getColSubBillType, 120, Unit.PX);
		getColSubBillType.setSortable(true);
	}	
	
	protected void addSortingSubBillType()
	{
		List<CustomerPayment> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerPayment>(list);
		columnSort.setComparator(getColSubBillType, new Comparator<CustomerPayment>()
				{
			@Override
			public int compare(CustomerPayment e1,CustomerPayment e2)
			{
				if (e1 != null && e2 != null) {
					
					if (e1.getNumberRange() == null && e2.getNumberRange() == null){
						return 0;
					}
					if(e1.getNumberRange() == null) return 1;							
					if(e2.getNumberRange() == null) return -1;							
					return e1.getNumberRange().compareTo(e2.getNumberRange());

				}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}

	/**end**/

	
	private void addColumnInvoiceDate() {

		invoiceDateColumn=new TextColumn<CustomerPayment>() {

			@Override
			public String getValue(CustomerPayment object) {
				if(object.getInvoiceDate()!=null)
				{
				    String invoiceDate=AppUtility.parseDate(object.getInvoiceDate());
				    return invoiceDate;
				}
				return "";
			}
		};
		
		table.addColumn(invoiceDateColumn,"Invoice Date");
		table.setColumnWidth(invoiceDateColumn, 100, Unit.PX);
		invoiceDateColumn.setSortable(true);
	}
	
	protected void addSortingOnInvoiceDate()
	{
		List<CustomerPayment> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerPayment>(list);
		columnSort.setComparator(invoiceDateColumn, new Comparator<CustomerPayment>()
				{
			@Override
			public int compare(CustomerPayment e1,CustomerPayment e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getInvoiceDate()!=null && e2.getInvoiceDate()!=null){
						return e1.getInvoiceDate().compareTo(e2.getInvoiceDate());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);


	}

}
