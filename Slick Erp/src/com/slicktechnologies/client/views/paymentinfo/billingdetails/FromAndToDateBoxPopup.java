package com.slicktechnologies.client.views.paymentinfo.billingdetails;

import java.util.Date;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.complain.TimeFormatBox;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;

public class FromAndToDateBoxPopup extends AbsolutePanel{
	public Button btnOne;
	public Button btnTwo;
	public DateBox fromDate;
	public DateBox toDate;
	public DateBox day1;
	public DateBox day2;
	/**
	 * Date : 10-09-2019 By ANIL
	 */
	public DateBox effectiveFromDate;
	/**
	 * End
	 */
	/** date 30.11.2018 added by komal f]to make 2 seperate bills for staffing and equipment rental **/
	public CheckBox cbFlag;
	
	/**Date 9-8-2019 added by Amol for effectiveToDate**/
	public DateBox effectiveToDate;
		PersonInfoComposite composite;
	ObjectListBox<Branch> olbBranch;
	
	/**
	 * @author Anil ,Date : 09-11-2019
	 */
	IntegerBox ibServiceId;
	IntegerBox ibContractId;//Ashwini Patil Date:7-06-2023 for psipl
	IntegerBox ibFromServiceId;//Ashwini Patil Date:7-06-2023 for psipl
	IntegerBox ibToServiceId;//Ashwini Patil Date:7-06-2023 for psipl
	
	/***
	 * @author Anil @since 15-12-2021
	 * Adding textarea widget for capturing remark for closing assement task
	 */
	public TextArea taRemark;
	
	public TimeFormatBox tmbServiceTime;
	
	public FromAndToDateBoxPopup() {
		DateTimeFormat dateFormat = DateTimeFormat.getFormat("dd/MM/yyyy"); 
		
		btnOne=new Button("Ok");
		btnTwo=new Button("Cancel");
		
		InlineLabel displayMessage = new InlineLabel("From Date");
		add(displayMessage,10,10);
		
		fromDate=new DateBoxWithYearSelector();
		add(fromDate,10,30);
		fromDate.setFormat(new DateBox.DefaultFormat(dateFormat));
		
		InlineLabel displayMessage1 = new InlineLabel("To Date");
		add(displayMessage1,200,10);
		
		toDate=new DateBoxWithYearSelector();
		add(toDate,200,30);
		toDate.setFormat(new DateBox.DefaultFormat(dateFormat));
		
		
		add(btnOne,120,100);
		add(btnTwo,180,100); 
		setSize("400px", "200px");
		this.getElement().setId("form");
		
	}
	
	public FromAndToDateBoxPopup(boolean isThreeDatebox) {
		DateTimeFormat dateFormat = DateTimeFormat.getFormat("dd/MM/yyyy"); 
		
		btnOne=new Button("Ok");
		btnTwo=new Button("Cancel");
		
		InlineLabel displayMessage = new InlineLabel("From Date");
		add(displayMessage,10,10);
		
		fromDate=new DateBoxWithYearSelector();
		add(fromDate,10,30);
		fromDate.setFormat(new DateBox.DefaultFormat(dateFormat));
		
		InlineLabel displayMessage1 = new InlineLabel("To Date");
		add(displayMessage1,200,10);
		
		toDate=new DateBoxWithYearSelector();
		add(toDate,200,30);
		toDate.setFormat(new DateBox.DefaultFormat(dateFormat));
		
		
		InlineLabel displayMessage2 = new InlineLabel("Effective From");
		add(displayMessage2,10,60);
		
		effectiveFromDate=new DateBoxWithYearSelector();
		add(effectiveFromDate,10,80);
		effectiveFromDate.setFormat(new DateBox.DefaultFormat(dateFormat));
		
		
		InlineLabel displayMessage3 = new InlineLabel("Effective To");
		add(displayMessage3,200,60);
		
		effectiveToDate=new DateBoxWithYearSelector();
		add(effectiveToDate,200,80);
		effectiveToDate.setFormat(new DateBox.DefaultFormat(dateFormat));
		
		
		
		add(btnOne,30,150);
		add(btnTwo,120,150); 
		setSize("400px", "200px");
		this.getElement().setId("form");
		
	}

	public FromAndToDateBoxPopup(String header) {
		DateTimeFormat dateFormat = DateTimeFormat.getFormat("dd/MM/yyyy"); 
		
		btnOne=new Button("Ok");
		btnTwo=new Button("Cancel");
		
		InlineLabel displayMessage = new InlineLabel("From Date");
		add(displayMessage,10,10);
		
		fromDate=new DateBoxWithYearSelector();
		add(fromDate,10,30);
		fromDate.setFormat(new DateBox.DefaultFormat(dateFormat));
		
		InlineLabel displayMessage1 = new InlineLabel("To Date");
		add(displayMessage1,200,10);
		
		toDate=new DateBoxWithYearSelector();
		add(toDate,200,30);
		toDate.setFormat(new DateBox.DefaultFormat(dateFormat));
		
		
		InlineLabel displayMessage2 = new InlineLabel(header);
		add(displayMessage2,10,60);
		
		InlineLabel displayMessage3 = new InlineLabel(header);
		add(displayMessage3,200,60);
		
		
		
		cbFlag=new CheckBox();
		add(cbFlag,10,80);
		
		
		add(btnOne,30,150);
		add(btnTwo,120,150); 
		setSize("400px", "200px");
		this.getElement().setId("form");
		
	}
	
	public FromAndToDateBoxPopup(PersonInfoComposite personInfo) {
		DateTimeFormat dateFormat = DateTimeFormat.getFormat("dd/MM/yyyy"); 
		
		btnOne=new Button("Ok");
		btnTwo=new Button("Cancel");
		
		InlineLabel displayMessage = new InlineLabel("From Date");
		add(displayMessage,10,10);
		
		fromDate=new DateBoxWithYearSelector();
		add(fromDate,10,30);
		fromDate.setFormat(new DateBox.DefaultFormat(dateFormat));
		
		InlineLabel displayMessage1 = new InlineLabel("To Date");
		add(displayMessage1,200,10);
		
		toDate=new DateBoxWithYearSelector();
		add(toDate,200,30);
		toDate.setFormat(new DateBox.DefaultFormat(dateFormat));
		
		composite=AppUtility.customerInfoComposite(new Customer());
		composite.getCustomerId().getHeaderLabel().setText("Customer ID");
		composite.getCustomerName().getHeaderLabel().setText("Customer Name");
		composite.getCustomerCell().getHeaderLabel().setText("Customer Cell");
		
		add(composite,10,80);
		
		InlineLabel displayMessage2 = new InlineLabel("Branch");
		add(displayMessage2,10,120);
		
		olbBranch=new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbBranch);
		add(olbBranch,10,140);
		
//		cbFlag=new CheckBox();
//		add(cbFlag,10,80);
		
		
		add(btnOne,200,250);
		add(btnTwo,300,250); 
		setSize("500px", "300px");
		this.getElement().setId("form");
		
	}
	
	public FromAndToDateBoxPopup(boolean idPopUp,String idHeader) {
		DateTimeFormat dateFormat = DateTimeFormat.getFormat("dd/MM/yyyy"); 
		
		btnOne=new Button("Ok");
		btnTwo=new Button("Cancel");
		
		InlineLabel displayMessage = new InlineLabel(idHeader);
		add(displayMessage,10,10);
		
		if(idPopUp){
			ibServiceId=new IntegerBox();
			add(ibServiceId,10,30);
		}
		
		
		//-------------Ashwini Patil Date:7-06-2023------------
		
		InlineLabel ormsg = new InlineLabel("OR");
		add(ormsg,180,70);
		
		InlineLabel contractLbl = new InlineLabel("Contract ID");
		add(contractLbl,10,100);
		
		ibContractId=new IntegerBox();
		add(ibContractId,10,130);
		
		//-------------Ashwini Patil Date:16-01-2023------------
		
		InlineLabel ormsg1 = new InlineLabel("OR");
		add(ormsg1,180,170);
		
		InlineLabel displayMessage1 = new InlineLabel("From Date");
		add(displayMessage1,10,200);
		
		fromDate=new DateBoxWithYearSelector();
		add(fromDate,10,230);
		fromDate.setFormat(new DateBox.DefaultFormat(dateFormat));
		
		InlineLabel displayMessage2 = new InlineLabel("To Date");
		add(displayMessage2,200,200);
		
		toDate=new DateBoxWithYearSelector();
		add(toDate,200,230);
		
		toDate.setFormat(new DateBox.DefaultFormat(dateFormat));
		//-------------------------
		
		//-------------Ashwini Patil Date:4-05-2023------------
		InlineLabel ormsg2 = new InlineLabel("OR");
		add(ormsg2,180,270);
		
		InlineLabel displayMessage3 = new InlineLabel("Day 1");
		add(displayMessage3,10,300);
		day1=new DateBoxWithYearSelector();
		add(day1,10,330);
		day1.setFormat(new DateBox.DefaultFormat(dateFormat));
		
		InlineLabel displayMessage4 = new InlineLabel("Day 2");
		add(displayMessage4,200,300);
		day2=new DateBoxWithYearSelector();
		add(day2,200,330);
		day2.setFormat(new DateBox.DefaultFormat(dateFormat));
		
		
		//-------------Ashwini Patil Date:7-06-2023------------
		InlineLabel ormsg3 = new InlineLabel("OR");
		add(ormsg3,180,370);
				
		InlineLabel displayMessage5 = new InlineLabel("From ServiceID");
		add(displayMessage5,10,400);
		ibFromServiceId=new IntegerBox();
		add(ibFromServiceId,10,430);
				
		InlineLabel displayMessage6 = new InlineLabel("To ServiceID");
		add(displayMessage6,200,400);
		ibToServiceId=new IntegerBox();
		add(ibToServiceId,200,430);
		
		add(btnOne,150,470);
		add(btnTwo,220,470); 
		setSize("400px", "500px");
		this.getElement().setId("form");
		
	}
	
	public FromAndToDateBoxPopup(String popupType,String idHeader) {
		DateTimeFormat dateFormat = DateTimeFormat.getFormat("dd/MM/yyyy"); 
		
		btnOne=new Button("Ok");
		btnTwo=new Button("Cancel");
		
		
		if(popupType!=null&&popupType.equals("Close Task")){
			InlineLabel displayMessage = new InlineLabel(idHeader);
			displayMessage.getElement().getStyle().setFontSize(16, Unit.PX);
			add(displayMessage,130,10-5);
			
			InlineLabel displayMessage1 = new InlineLabel("Closure Date");
			add(displayMessage1,30,10+20);
			
			fromDate=new DateBoxWithYearSelector();
			add(fromDate,30,30+20);
			fromDate.setFormat(new DateBox.DefaultFormat(dateFormat));
			
			InlineLabel remarkLbl = new InlineLabel("Remark");
			add(remarkLbl,30,70+20);
			
			taRemark=new TextArea();
			taRemark.getElement().getStyle().setWidth(300, Unit.PX);
			add(taRemark,30,90+20);
			
		}else if(popupType!=null&&popupType.equals("Schedule Assessment")){
			InlineLabel displayMessage = new InlineLabel(idHeader);
			displayMessage.getElement().getStyle().setFontSize(16, Unit.PX);
			add(displayMessage,130,10-5);
			
			InlineLabel displayMessage1 = new InlineLabel("Date");
			add(displayMessage1,30,10+20);
			
			fromDate=new DateBoxWithYearSelector();
			add(fromDate,30,30+20);
			fromDate.setFormat(new DateBox.DefaultFormat(dateFormat));
			
			InlineLabel displayMessage2 = new InlineLabel("Time");
			add(displayMessage2,220,10+20);
			
			tmbServiceTime=new TimeFormatBox();
	        tmbServiceTime.setValue(getCreationTime(new Date()));
			add(tmbServiceTime.getTimePanel(),220,30+20);
			
			InlineLabel remarkLbl = new InlineLabel("Remark");
			add(remarkLbl,30,70+20);
			
			taRemark=new TextArea();
			taRemark.getElement().getStyle().setWidth(300, Unit.PX);
			add(taRemark,30,90+20);
		}else if(popupType!=null&&popupType.equals("rescheduleServicesInDuration")){//Ashwini Patil Date:4-10-2023 for orion
			InlineLabel displayMessage = new InlineLabel(idHeader);
			displayMessage.getElement().getStyle().setFontSize(16, Unit.PX);
			add(displayMessage,30,10-5);
			
			InlineLabel displayMessage1 = new InlineLabel("From Date");
			add(displayMessage1,30,700);
			
			fromDate=new DateBoxWithYearSelector();
			add(fromDate,30,90);
			fromDate.setFormat(new DateBox.DefaultFormat(dateFormat));
			
			InlineLabel displayMessage2 = new InlineLabel("To Date");
			add(displayMessage2,220,70);
			
			toDate=new DateBoxWithYearSelector();
			add(toDate,220,90);
			toDate.setFormat(new DateBox.DefaultFormat(dateFormat));
			
		}

		add(btnOne,30,150+10);
		add(btnTwo,120,150+10); 
		setSize("430px", "220px");
		this.getElement().setId("form");
		
	}
	
	public String convert24Hrsto12HrsFormat(String timeIn24){
		String timeIn12 = null;
		DateTimeFormat fmt24Hrs= DateTimeFormat.getFormat("HH:mm");
		DateTimeFormat fmt12Hrs= DateTimeFormat.getFormat("hh:mm a");
		timeIn12=fmt12Hrs.format(fmt24Hrs.parse(timeIn24));
		timeIn12 = timeIn12.replaceAll("\\s","");
		return timeIn12;
	}
	public String getCreationTime(Date creationDate) {
		String time="";
		if(creationDate!=null) {
			time=creationDate.getHours()+":"+creationDate.getMinutes();
		}
		return time;
	}

	//**************getters and setters ********************
	
	public Button getBtnOne() {
		return btnOne;
	}

	public void setBtnOne(Button btnOne) {
		this.btnOne = btnOne;
	}

	public Button getBtnTwo() {
		return btnTwo;
	}

	public void setBtnTwo(Button btnTwo) {
		this.btnTwo = btnTwo;
	}

	public DateBox getFromDate() {
		return fromDate;
	}

	public DateBox getDay1() {
		return day1;
	}

	public void setDay1(DateBox day1) {
		this.day1 = day1;
	}

	public DateBox getDay2() {
		return day2;
	}

	public void setDay2(DateBox day2) {
		this.day2 = day2;
	}

	public void setFromDate(DateBox fromDate) {
		this.fromDate = fromDate;
	}

	public DateBox getToDate() {
		return toDate;
	}

	public void setToDate(DateBox toDate) {
		this.toDate = toDate;
	}

	public CheckBox getCbFlag() {
		return cbFlag;
	}

	public void setCbFlag(CheckBox cbFlag) {
		this.cbFlag = cbFlag;
	}

	public PersonInfoComposite getComposite() {
		return composite;
	}

	public void setComposite(PersonInfoComposite composite) {
		this.composite = composite;
	}

	public ObjectListBox<Branch> getOlbBranch() {
		return olbBranch;
	}

	public void setOlbBranch(ObjectListBox<Branch> olbBranch) {
		this.olbBranch = olbBranch;
	}

	public IntegerBox getIbServiceId() {
		return ibServiceId;
	}

	public void setIbServiceId(IntegerBox ibServiceId) {
		this.ibServiceId = ibServiceId;
	}
	
	public IntegerBox getIbContractId() {
		return ibContractId;
	}

	public void setIbContractId(IntegerBox ibContractId) {
		this.ibContractId = ibContractId;
	}

	public IntegerBox getIbFromServiceId() {
		return ibFromServiceId;
	}

	public void setIbFromServiceId(IntegerBox ibFromServiceId) {
		this.ibFromServiceId = ibFromServiceId;
	}

	public IntegerBox getIbToServiceId() {
		return ibToServiceId;
	}

	public void setIbToServiceId(IntegerBox ibToServiceId) {
		this.ibToServiceId = ibToServiceId;
	}

	
	
	
}
