package com.slicktechnologies.client.views.paymentinfo.billingdetails;

import java.util.List;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;

public class BillingProductTaxesTable extends SuperTable<ContractCharges> {
	
	TextColumn<ContractCharges> billingProductTaxNameColumn;
	TextColumn<ContractCharges> billingProductTaxPercentColumn;
	TextColumn<ContractCharges> billingAssessAmountColumn;
	TextColumn<ContractCharges> calculatedTaxColumn;
	NumberFormat nf=NumberFormat.getFormat("0.00");
	
	AppUtility appUtility = new AppUtility();
	/**
	 * @author Anil 
	 * @since 10-02-2022
	 * Innovative wanted to alter the calculated tax amount so they wanted amount column editable
	 * Raised by Nitin sir and Nithila
	 */
	Column<ContractCharges,String> editableCalculatedTaxColumn;
	Company comp=LoginPresenter.company;
	boolean thaiClientFlag=false;
	
	public BillingProductTaxesTable() {
		super();
		if(comp!=null&&(comp.getAddress().getCountry().trim().equalsIgnoreCase("Thailand")||comp.getAddress().getCountry().trim().equalsIgnoreCase("?????????"))){
			thaiClientFlag=true;
		}
	}
	
	@Override
	public void createTable() {
		
		if(comp!=null&&(comp.getAddress().getCountry().trim().equalsIgnoreCase("Thailand")||comp.getAddress().getCountry().trim().equalsIgnoreCase("?????????"))){
			thaiClientFlag=true;
		}
		createColumnBillingTaxName();
		createColumnBillingTaxPercent();
		createColumnBillingAssessAmount();
		
		if(thaiClientFlag){
			editableCalculatedTaxColumn();
		}else{
			createColumnCalcBillingTaxAmount();
		}
	    table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
	}
	
	
	private void editableCalculatedTaxColumn() {
		EditTextCell cell=new EditTextCell();
		editableCalculatedTaxColumn=new Column<ContractCharges,String>(cell) {
			@Override
			public String getValue(ContractCharges object) {
				
				if(object.getPayableAmt()!=0){
					return object.getPayableAmt()+"";
				}else{
					double totalCalcAmt=0;
					totalCalcAmt=object.getTaxChargeAssesVal()*object.getTaxChargePercent()/100;
					totalCalcAmt=Double.parseDouble(nf.format(totalCalcAmt));
					object.setPayableAmt(totalCalcAmt);
					return nf.format(object.getPayableAmt());
				}
			}
		};
		table.addColumn(editableCalculatedTaxColumn,"#Amount");
		
		editableCalculatedTaxColumn.setFieldUpdater(new FieldUpdater<TaxesAndCharges.ContractCharges, String>() {
			@Override
			public void update(int index, ContractCharges object, String value) {
				try{
					double amount=Double.parseDouble(value);
					object.setPayableAmt(amount);
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				}catch(Exception e){
					GWTCAlert alert=new GWTCAlert();
					alert.alert("Please enter numeric value only!");
				}
				
				table.redrawRow(index);
			}
		});
	}

	public void createColumnBillingTaxName()
	{
		billingProductTaxNameColumn=new TextColumn<ContractCharges>() {

			@Override
			public String getValue(ContractCharges object) {	
				return object.getTaxChargeName().trim();
			}
		};
		table.addColumn(billingProductTaxNameColumn,"Tax Name");
	}
	
	public void createColumnBillingTaxPercent()
	{
		billingProductTaxPercentColumn=new TextColumn<ContractCharges>() {
			@Override
			public String getValue(ContractCharges object) {
				return object.getTaxChargePercent()+"";
			}
		};
		table.addColumn(billingProductTaxPercentColumn,"%");
	}
	
	public void createColumnBillingAssessAmount()
	{
		billingAssessAmountColumn=new TextColumn<ContractCharges>() {
			@Override
			public String getValue(ContractCharges object) {
				return nf.format(object.getTaxChargeAssesVal());
			}
		};
		table.addColumn(billingAssessAmountColumn,"Asses Value");
		
	}
	
	public void createColumnCalcBillingTaxAmount()
	{
		calculatedTaxColumn=new TextColumn<ContractCharges>() {
			@Override
			public String getValue(ContractCharges object) {
				
				if(thaiClientFlag){
					if(object.getPayableAmt()!=0){
						return object.getPayableAmt()+"";
					}
				}
				double totalCalcAmt=0;
				totalCalcAmt=object.getTaxChargeAssesVal()*object.getTaxChargePercent()/100;
				
				
				if(LoginPresenter.PC_NO_ROUNDOFF_INVOICE_CONTRACTFlag){
					double amt = appUtility.getLastTwoDecimalOnly(totalCalcAmt);
					object.setPayableAmt(amt);
					Console.log("tax Amt "+amt);
					return amt+"";
				}
				else{
					totalCalcAmt=Double.parseDouble(nf.format(totalCalcAmt));
					object.setPayableAmt(totalCalcAmt);
					return nf.format(object.getPayableAmt());
				}
				
			}
		};
		table.addColumn(calculatedTaxColumn,"Amount");
	}

	@Override
	protected void initializekeyprovider() {
		
	}

	@Override
	public void addFieldUpdater() {
	}
	
	@Override
	public void setEnable(boolean state) {
		if(comp!=null&&(comp.getAddress().getCountry().trim().equalsIgnoreCase("Thailand")||comp.getAddress().getCountry().trim().equalsIgnoreCase("?????????"))){
			thaiClientFlag=true;
		}
		
		for (int i = table.getColumnCount() - 1; i > -1; i--)
			table.removeColumn(i);

		if (state == true) {
			createColumnBillingTaxName();
			createColumnBillingTaxPercent();
			createColumnBillingAssessAmount();
			
			if(thaiClientFlag){
				editableCalculatedTaxColumn();
			}else{
				createColumnCalcBillingTaxAmount();
			}
		}else{
			createColumnBillingTaxName();
			createColumnBillingTaxPercent();
			createColumnBillingAssessAmount();
			createColumnCalcBillingTaxAmount();
		}
		table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
		
	}

	@Override
	public void applyStyle() {
		
	}

	
	public double calculateTotalTaxes()
	{
		List<ContractCharges>list=getDataprovider().getList();
		double sum=0;
		for(int i=0;i<list.size();i++)
		{
			ContractCharges entity=list.get(i);
			sum=sum+(entity.getTaxChargeAssesVal()*entity.getTaxChargePercent()/100);
		}
		System.out.println("Sum Taxes"+sum);
		return sum;
	}
	
	public double calculateTotalTaxAmount()
	{
		List<ContractCharges>list=getDataprovider().getList();
		double sum=0;
		for(int i=0;i<list.size();i++)
		{
			ContractCharges entity=list.get(i);
			sum=sum+entity.getPayableAmt();
		}
		System.out.println("Sum Taxes"+sum);
		return sum;
	}
	
	


	

}
