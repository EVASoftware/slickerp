package com.slicktechnologies.client.views.paymentinfo.billingdetails;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.salesprocess.BillingDocumentDetails;

public class BillingDocumentTable extends SuperTable<BillingDocumentDetails> {

	TextColumn<BillingDocumentDetails> billingOrderIdColumn;
	TextColumn<BillingDocumentDetails> billingIdColumn;
	TextColumn<BillingDocumentDetails> billingDateColumn;
	TextColumn<BillingDocumentDetails> billingSalesAmtColumn;
	TextColumn<BillingDocumentDetails> billingAmtColumn;
	TextColumn<BillingDocumentDetails> billingCategoryColumn;
	TextColumn<BillingDocumentDetails> billingTypeColumn;
	TextColumn<BillingDocumentDetails> billingGroupColumn;
	TextColumn<BillingDocumentDetails> statusColumn;
	NumberFormat nf=NumberFormat.getFormat(".##");
	
	public BillingDocumentTable() {
		super();
	}

	@Override
	public void createTable() {
		addColumnBillingOrderId();
		addColumnBillingId();
		addColumnBillingDate();
		addColumnBillingAmount();
		addColumnBillingCategory();
		addColumnBillingType();
		addColumnBillingGroup();
		
		/**
		 * Date 27-02-2018 By vijay no need this column as discussed with nitin sir
		 */
//		addColumnBillingStatus();
		/**
		 * ends here
		 */
		table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
	}
	
	public void addColumnBillingOrderId()
	{
		billingOrderIdColumn=new TextColumn<BillingDocumentDetails>() {

			@Override
			public String getValue(BillingDocumentDetails object) {
				if(object.getOrderId()!=0){
					return object.getOrderId()+"";
				}else{
					return "";
				}
			}
		};
		table.addColumn(billingOrderIdColumn,"Order ID");
		table.setColumnWidth(billingOrderIdColumn, 22, Unit.PX);
		billingOrderIdColumn.setSortable(true);
	}
	
	
	public void addColumnBillingId()
	{
		billingIdColumn=new TextColumn<BillingDocumentDetails>() {

			@Override
			public String getValue(BillingDocumentDetails object) {
				return object.getBillId()+"";
			}
		};
		table.addColumn(billingIdColumn,"Billing ID");
		table.setColumnWidth(billingIdColumn, 22, Unit.PX);
		billingIdColumn.setSortable(true);
	}

	public void addColumnBillingDate()
	{
		billingDateColumn=new TextColumn<BillingDocumentDetails>()
	{
	   @Override
	   public String getValue(BillingDocumentDetails object){
		   if(object.getBillingDate()!=null)
		   {
			   return AppUtility.parseDate(object.getBillingDate());}
		   else
			   return "";
	}
	};
		table.addColumn(billingDateColumn,"Billing Date");
		table.setColumnWidth(billingDateColumn, 25, Unit.PX);
		billingDateColumn.setSortable(true);
	}

	public void addColumnBillingAmount()
	{
		billingAmtColumn=new TextColumn<BillingDocumentDetails>() {

			@Override
			public String getValue(BillingDocumentDetails object) {
				return nf.format(object.getBillAmount());
			}
		};
		
		table.addColumn(billingAmtColumn,"Billing Amount");
		table.setColumnWidth(billingAmtColumn, 23, Unit.PX);
		billingAmtColumn.setSortable(true);
	}

	public void addColumnBillingCategory()
	{
		billingCategoryColumn=new TextColumn<BillingDocumentDetails>() {

			@Override
			public String getValue(BillingDocumentDetails object) {	
				
				return object.getBillingCategory();
			}
		};
		table.addColumn(billingCategoryColumn,"Billing Category");
		table.setColumnWidth(billingCategoryColumn, 34, Unit.PX);
		billingCategoryColumn.setSortable(true);
	}	

	public void addColumnBillingType()
	{
		billingTypeColumn=new TextColumn<BillingDocumentDetails>() {

			@Override
			public String getValue(BillingDocumentDetails object) {	
				
				return object.getBillingType();
			}
		};
		table.addColumn(billingTypeColumn,"Billing Type");
		table.setColumnWidth(billingTypeColumn, 34, Unit.PX);
		billingTypeColumn.setSortable(true);
	}
	
	public void addColumnBillingGroup()
	{
		billingGroupColumn=new TextColumn<BillingDocumentDetails>() {

			@Override
			public String getValue(BillingDocumentDetails object) {	
				return object.getBillingGroup();
			}
		};
		table.addColumn(billingGroupColumn,"Billing Group");
		table.setColumnWidth(billingGroupColumn, 34, Unit.PX);
		billingGroupColumn.setSortable(true);
	}	
	
	
	public void addColumnBillingStatus()
	{
		statusColumn=new TextColumn<BillingDocumentDetails>() {

			@Override
			public String getValue(BillingDocumentDetails object) {
				return object.getBillstatus();
			}
		};
		
		table.addColumn(statusColumn,"Status");
		table.setColumnWidth(statusColumn, 25, Unit.PX);
		statusColumn.setSortable(true);
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}	
	
	
}
