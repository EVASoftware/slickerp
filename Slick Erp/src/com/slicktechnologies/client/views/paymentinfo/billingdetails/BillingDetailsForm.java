package com.slicktechnologies.client.views.paymentinfo.billingdetails;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.TextAlign;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.approvalutility.ApprovableFormScreen;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.config.CategoryTypeFactory;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.contract.OtherChargesTable;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.server.compositeimplementor.PersonInfoCompositeImplementor;
import com.slicktechnologies.shared.BranchWiseScheduling;
import com.slicktechnologies.shared.ProductOtherCharges;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productlayer.Tax;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.salesorder.OtherCharges;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.BillingDocumentDetails;
import com.slicktechnologies.shared.common.salesprocess.SalesOrderProductLineItem;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;

public class BillingDetailsForm extends ApprovableFormScreen<BillingDocument> implements ChangeHandler, ClickHandler {
	
	TextBox ibbillingid;
	IntegerBox ibpaytermsdays;
	TextBox ibcontractid;
//	PersonInfoComposite personComposite;
	TextBox tbpersonCount;
	TextBox tbpersonName;
	TextBox tbpersonCell;
	TextBox tbpocName;
	DateBox dbstartDate,dbendDate;
	DateBox dbbillingdate;
	DateBox dbcreationdate;
	ObjectListBox<ConfigCategory> olbbillingcategory;
	public ObjectListBox<Type> olbbillingtype;
	public ObjectListBox<Config> olbbillinggroup;
	DoubleBox dosalesamount;
	TextArea tacomment;
	TextBox tbstatus,tbpaytermscomment;
	ObjectListBox<Employee> olbEmployee,olbApproverName;
	ObjectListBox<Branch> olbbranch;
	DoubleBox dototalbillamt,dopaytermspercent;
	BillingDocumentTable billingDocumentTable;
	TextBox ibinvoiceid;
	DateBox dbinvoicedate,dbpaymentdate;
	SalesOrderProductTable salesProductTable;
	BillingProductTaxesTable billingTaxTable;
	BillingProductChargesTable billingChargesTable;
	TextBox tbaccounttype;
	TextBox tbremark;
	FormField fgroupingCustomerInformation;
	//FormField fpic;
	public static boolean multiContractFlag=false;
	

	public static boolean flag=false;
	public static boolean flag1=true;
	public static boolean flag2=true;
	
	Double totalAmount;
	Double toatalAmountIncludingTax;
	Double netPayableAmount;
	
	ListBox lbcustomerBranch;
	
	BillingDocument billingDocObj;
	
	/**
	 * Date 13 Feb 2017
	 * by Vijay for billing peroid from date and todate
	 */
	
	DateBox dbbillingperiodFromDate;
	DateBox dbbillingperiodToDate;
	
	/**
	 * End here
	 */
	
	/**
	 * Date 15 Feb 2017
	 * added by vijay
	 * for displaying rate contract service ID
	 */
	TextBox tbrateContractServiceid;
	/**
	 * End here
	 */
	
	/*
	 *  nidhi
	 *  30-06-2017
	 */
	TextBox tbSegment;
	
	/*
	 * end
	 */
	
	/**
	  * Added by Nidhi
	  * Two field named quantity and UOM
	  */
	 DoubleBox quantity;
	 ObjectListBox<Config> olbUOM;
	 /**
	  * 	Nidhi changes ends here
	  */
	 
	 /**
	  *  nidhi
	  *  24-07-2017
	  *   this feild ad for customer ref no which comes from customer details
	  *   we can change ref so this field kept in editable.
	  */
	 TextBox tbReferenceNumber;
	 /**
	  *  end
	  */
	 
 	/**
	 * Date 1-07-2017 added by vijay for Tax validation
	 */
	Date date = DateTimeFormat.getFormat("yyyy-MM-dd HH:mm:ss").parse("2017-07-01 00:00:00");
	final DateTimeFormat fmt = DateTimeFormat.getFormat("dd/MM/yyyy");

	/**
	 * ends here
	 */
	
	/** Date 03-09-2017 added by vijay for discount on total and round off **/
	DoubleBox dbDiscountAmt;
	DoubleBox dbFinalTotalAmt;
	TextBox tbroundOffAmt;
	DoubleBox dbtotalAmt;
	NumberFormat nf = NumberFormat.getFormat("0.00");
	DoubleBox dbtotalAmtIncludingTax;
	DoubleBox dbGrandTotalAmt;
	
	
	/**
	 * Date : 20-09-2017 BY ANIL
	 * Adding other charges table
	 */
	OtherChargesTable tblOtherCharges;
	Button addOthrChargesBtn;
	DoubleBox dbOtherChargesTotal;
	/**
	 * End
	 */
	/**
	 * nidhi 13-12-2018
	 */
//	TextArea taCancleAmtRemark;
	boolean parcialBillDisableFlag = false;
	DoubleBox dbCancelAmt ;
	/*** Date 17-04-2019 by Vijay for NBHC CCPM ***/
	ObjectListBox<Config> olbCancellationRemark;
	
	
	/**
	 * @author Anil , Date : 06-05-2019
	 * Adding Invoice creation flag
	 * Multiple invoice against single Bill
	 */
	
	public boolean invCreationFlag=false;
	
	/**
	 * @author Vijay Date - 11-08-2020
	 * Des :- Project Name from CNC for sasha with process config
	 */
	TextBox tbprojectName;
	IntegerBox ibcreditPeriod;
	TextBox tbContractNumber;

	public BillingDetailsForm() {
		super();
		createGui();
		billingDocumentTable.connectToLocal();
		salesProductTable.connectToLocal();
		//taxchargetable.connectToLocal();
		billingChargesTable.connectToLocal();
		billingTaxTable.connectToLocal();
		//************************************************************************
//		processLevelBar.btnLabels[5].setVisible(true);
		
		/** Date 03-09-2017 added by vijay for discount amount change handler ***/
		dbDiscountAmt.addChangeHandler(this);
		tbroundOffAmt.addChangeHandler(this);
		
		/**
		 * Date :20-09-2017 By ANIL
		 */
		tblOtherCharges.connectToLocal();
		dbOtherChargesTotal.setEnabled(false);
		/**
		 * ENd
		 */
	}
	
	public BillingDetailsForm  (String[] processlevel, FormField[][] fields,
			FormStyle formstyle) 
	{
		super(processlevel, fields, formstyle);
		createGui();
	}
	
	private void initalizeWidget()
	{
		ibbillingid=new TextBox();
		ibbillingid.setEnabled(false);
		ibcontractid=new TextBox();
		ibcontractid.setEnabled(false);
//		dbbillingdate=new DateBox();
		dbbillingdate = new DateBoxWithYearSelector();

//		dbcreationdate=new DateBox();
		dbcreationdate = new DateBoxWithYearSelector();

		dbcreationdate.setEnabled(false);
		olbbillingcategory= new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(olbbillingcategory, Screen.BILLINGCATEGORY);
		olbbillingcategory.addChangeHandler(this);
		olbbillingtype=new ObjectListBox<Type>();
		AppUtility.makeTypeListBoxLive(olbbillingtype, Screen.BILLINGTYPE);
//		setOneToManyCombo();
		olbbillinggroup=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbbillinggroup, Screen.BILLINGGROUP);
		
		tbpersonCount=new TextBox();
		tbpersonName=new TextBox();
		tbpersonCell=new TextBox();
		tbpersonCount.setEnabled(false);
		tbpersonName.setEnabled(false);
		tbpersonCell.setEnabled(false);
		tbpocName=new TextBox();
		tbpocName.setEnabled(false);
		dosalesamount=new DoubleBox();
		dosalesamount.setEnabled(false);
		tacomment=new TextArea();
		tbstatus=new TextBox();
		tbstatus.setEnabled(false);
		this.tbstatus.setText(BillingDocument.CREATED);
		olbApproverName=new ObjectListBox<Employee>();
		AppUtility.makeApproverListBoxLive(olbApproverName, "Billing Details");
		olbEmployee=new ObjectListBox<Employee>();
		AppUtility.makeSalesPersonListBoxLive(olbEmployee);
		olbbranch=new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbbranch);
		olbbranch.addChangeHandler(this);
		billingDocumentTable=new BillingDocumentTable();
		dototalbillamt=new DoubleBox();
		dototalbillamt.setEnabled(false);
		ibinvoiceid=new TextBox();
		ibinvoiceid.setEnabled(false);
//		dbinvoicedate=new DateBox();
		dbinvoicedate = new DateBoxWithYearSelector();

//		dbpaymentdate=new DateBox();
		dbpaymentdate = new DateBoxWithYearSelector();

		salesProductTable=new SalesOrderProductTable();
		ibpaytermsdays=new IntegerBox();
		ibpaytermsdays.setEnabled(false);
		dopaytermspercent=new DoubleBox();
		dopaytermspercent.setEnabled(false);
		tbpaytermscomment=new TextBox();
		tbpaytermscomment.setEnabled(false);
		billingTaxTable=new BillingProductTaxesTable();
		billingChargesTable=new BillingProductChargesTable();
		tbaccounttype=new TextBox();
		tbaccounttype.setEnabled(false);
		AppUtility.vendorInfoComposite(new Vendor());
		
		tbremark=new TextBox();
		tbremark.setEnabled(false);
		
		lbcustomerBranch = new ListBox();
		lbcustomerBranch.addItem("--SELECT--");
		
//		dbbillingperiodFromDate = new DateBox();
		dbbillingperiodFromDate = new DateBoxWithYearSelector();

//		dbbillingperiodToDate = new DateBox();
		dbbillingperiodToDate = new DateBoxWithYearSelector();

		
		tbrateContractServiceid = new TextBox();
		tbrateContractServiceid.setEnabled(false);
		
		/*
		 * nidhi
		 */
		tbSegment = new TextBox();
		tbSegment.setEnabled(false);
		/*
		 * end
		 */
		
		/**
		 * 	nidhi
		 * 	20-07-2017
		 *  for show quantity and measurement area of service 
		 */
		quantity=new DoubleBox();
		quantity.setEnabled(true);
		
		olbUOM=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbUOM, Screen.SERVICEUOM);
		
		/*
		 *  end
		 */
		
		/**
		 *  nidhi
		 *  24-07-2017
		 *   tbReferenceNumber
		 */
		tbReferenceNumber=new TextBox();
		/**
		 *  end
		 */
		
		/** Date 03-09-2017 added by vijay for discount amt round off total amt***/
		dbDiscountAmt = new DoubleBox();
		dbFinalTotalAmt = new DoubleBox();
		dbFinalTotalAmt.setEnabled(false);
		tbroundOffAmt = new TextBox();	
		dbtotalAmt = new DoubleBox();
		dbtotalAmt.setEnabled(false);
		dbtotalAmtIncludingTax = new DoubleBox();
		dbtotalAmtIncludingTax.setEnabled(false);
		dbGrandTotalAmt = new DoubleBox();
		dbGrandTotalAmt.setEnabled(false);
		
		/**
		 * Date : 20-09-2017 By ANIL
		 */
		tblOtherCharges=new OtherChargesTable();
		addOthrChargesBtn=new Button("+");
		addOthrChargesBtn.addClickHandler(this);
		dbOtherChargesTotal=new DoubleBox();
		dbOtherChargesTotal.setEnabled(false);
		/**
		 * End
		 */
		/**
		 * nidhi 14-12-2018
		 */
		dbCancelAmt = new DoubleBox();
		dbCancelAmt.setVisible(false);
		
		/** Date 17-04-2019 by Vijay for NBHC CCPM billing amount reducing Remark ***/
		olbCancellationRemark = new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbCancellationRemark, Screen.CANCELLATIONREMARK);
		
		parcialBillDisableFlag = AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "DisableParticalBillDocument");
		if(!parcialBillDisableFlag){
			olbCancellationRemark.setVisible(false);
		}
		
		tbprojectName = new TextBox();
		tbprojectName.setEnabled(false);
		
		ibcreditPeriod = new IntegerBox();
		tbContractNumber = new TextBox();
		tbContractNumber.setEnabled(false);
		
		this.changeWidgets();
	}


	/**
	 * Date 03-09-2017 added by vijay
	 * This method represents changing the widgets style.
	 */
	
	private void changeWidgets()
	{
		//  For changing the text alignments.
		this.changeTextAlignment(dbDiscountAmt);
		this.changeTextAlignment(dbFinalTotalAmt);
		this.changeTextAlignment(tbroundOffAmt);
		this.changeTextAlignment(dbtotalAmt);
		this.changeTextAlignment(dbtotalAmtIncludingTax);
		this.changeTextAlignment(dbGrandTotalAmt);
		this.changeTextAlignment(dototalbillamt);

		/**
		 * Date : 21-09-2017 BY ANIL
		 * please update roundoff textbox width
		 */
		this.changeTextAlignment(dototalbillamt);
		this.changeWidth(dototalbillamt, 200, Unit.PX);
		
		this.changeTextAlignment(dbOtherChargesTotal);
		this.changeWidth(dbOtherChargesTotal, 200, Unit.PX);
		/**
		 * End
		 */
		
		this.changeWidth(dbDiscountAmt, 200, Unit.PX);
		this.changeWidth(dbFinalTotalAmt, 200, Unit.PX);
		this.changeWidth(tbroundOffAmt, 200, Unit.PX);
		this.changeWidth(dbtotalAmt, 200, Unit.PX);
		this.changeWidth(dbtotalAmtIncludingTax,200,Unit.PX);
		this.changeWidth(dbGrandTotalAmt,200,Unit.PX);
		this.changeWidth(dototalbillamt,200,Unit.PX);

	}
	
	/*******************************Text Alignment Method******************************************/
	
	private void changeTextAlignment(Widget widg)
	{
		widg.getElement().getStyle().setTextAlign(TextAlign.CENTER);
	}
	
	/*****************************Change Width Method**************************************/
	private void changeWidth(Widget widg,double size,Unit unit)
	{
		widg.getElement().getStyle().setWidth(size, unit);
	}
	
	/**
	 * ends here
	 */

	@Override
	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){//18-10-2022
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			}
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Save")||text.contains("Discard")||text.contains("Search")||text.contains(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			}
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Save")||text.contains("Discard")||text.contains(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 
				
				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			}
			
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Discard")||text.contains("Search")||text.contains("Edit")||text.contains(AppConstants.NAVIGATION))
						menus[k].setVisible(true); 
				
				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.BILLINGDETAILS,"Billing Details");//LoginPresenter.currentModule.trim()
	}

	@Override
	public void createScreen() {
		initalizeWidget();

		
		if(isPopUpAppMenubar()){
			Console.log("Inside isPopUpAppMenubar in billing");
			//this.processlevelBarNames=new String[]{AppConstants.CREATEPROFORMAINVOICE,AppConstants.CREATETAXINVOICE,ManageApprovals.APPROVALREQUEST,ManageApprovals.CANCELAPPROVALREQUEST,AppConstants.CANCELBILLING,ManageApprovals.SUBMIT,AppConstants.SUBMITINVOICE,AppConstants.CHANGESTATUSTOCREATED};	
			/**
			 * Date : 28-12-2020 By Priyanka
			 * Des : Change Navigation flow as per requirement of simplification Project raised by Nitin Sir.
			 */
			this.processlevelBarNames=new String[]{ManageApprovals.SUBMIT,AppConstants.CANCELBILLING,AppConstants.CREATETAXINVOICE,AppConstants.CREATEPROFORMAINVOICE,ManageApprovals.APPROVALREQUEST,ManageApprovals.CANCELAPPROVALREQUEST,AppConstants.SUBMITINVOICE,AppConstants.CHANGESTATUSTOCREATED};	

		}else{
			/**
			 * Date : 23-05-2017 By ANIL
			 * Added submit button for self approval.
			 *  * Date : 29-07-2017 By ANIL
			 * Added View Invoice button on process level bar
			 * Date : 31-10-2017 By ANIL
			 * added view service button
			 */
			/**@Sheetal : 14-02-2022
			 * Added View payment button**/
			//Token to initialize the processlevel menus.
			//this.processlevelBarNames=new String[]{AppConstants.CREATEPROFORMAINVOICE,AppConstants.CREATETAXINVOICE,ManageApprovals.APPROVALREQUEST,ManageApprovals.CANCELAPPROVALREQUEST,AppConstants.CANCELBILLING,ManageApprovals.SUBMIT,AppConstants.VIEWORDER,AppConstants.VIEWINVOICE,AppConstants.VIEWSERVICE,AppConstants.VIEWGRN,AppConstants.SUBMITINVOICE,AppConstants.CHANGESTATUSTOCREATED};
			/**
			 * Date : 28-12-2020 By Priyanka
			 * Des : Change Navigation flow as per requirement of simplification Project raised by Nitin Sir.
			 */
			this.processlevelBarNames=new String[]{ManageApprovals.SUBMIT,AppConstants.CANCELBILLING,AppConstants.CREATETAXINVOICE,AppConstants.CREATEPROFORMAINVOICE,AppConstants.VIEWINVOICE,AppConstants.VIEWORDER,AppConstants.VIEWSERVICE,AppConstants.VIEWGRN,AppConstants.VIEWPAYMENT,AppConstants.SUBMITINVOICE,AppConstants.CHANGESTATUSTOCREATED,ManageApprovals.APPROVALREQUEST,ManageApprovals.CANCELAPPROVALREQUEST};	
		}
		
//		this.processlevelBarNames=new String[]{ManageApprovals.APPROVALREQUEST,ManageApprovals.CANCELAPPROVALREQUEST,AppConstants.CANCLECONTRACT,
//		AppConstants.VIEWSERVICES,AppConstants.CANCELLATIONSUMMARY, AppConstants.NEW,AppConstants.CONTRACTRENEWAL,AppConstants.WORKORDER,AppConstants.CUSTOMERINTEREST,AppConstants.MATERIALREQUIREDREPORT,"Update Taxes"};
//+=============================,"Update Taxes"
		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////
		//Token to initialize formfield
		String mainScreenLabel="Billing Details";
		if(billingDocObj!=null&&billingDocObj.getCount()!=0){
			mainScreenLabel=billingDocObj.getCount()+" "+"/"+" "+billingDocObj.getStatus()+" "+"/"+" "+AppUtility.parseDate(billingDocObj.getCreationDate());
		}
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		fgroupingCustomerInformation=fbuilder.setlabel(mainScreenLabel).widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).setKeyField(true).build(); 
		//FormFieldBuilder fbuilder;
		//fbuilder = new FormFieldBuilder();
		//FormField fgroupingCustomerInformation=fbuilder.setlabel("Billing Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).setKeyField(true).build();
		fbuilder = new FormFieldBuilder("* Customer ID",tbpersonCount);
		FormField ftbpersonCount=fbuilder.setMandatory(true).setMandatoryMsg("ID is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Customer Name",tbpersonName);
		FormField ftbpersonName=fbuilder.setMandatory(true).setMandatoryMsg("Name is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Cell Number",tbpersonCell);
		FormField ftbpersonCell=fbuilder.setMandatory(true).setMandatoryMsg("Cell Number is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("POC Name",tbpocName);
		FormField ftbpocName=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingSalesOrderInfo=fbuilder.setlabel("Reference/General").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();		
		fbuilder = new FormFieldBuilder("Order ID",ibcontractid);
		FormField fibcontractid=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Contract Start Date",dbstartDate);
		FormField fdbstartdate=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Contract End Date",dbendDate);
		FormField fdbenddate=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Total Order Amount",dosalesamount);
		FormField fdoamount=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingBillingInfo=fbuilder.setlabel("Classification").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Billing ID",ibbillingid);
		FormField fibbillingid=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Billing Date",dbbillingdate);
		FormField fdbpostingdate=fbuilder.setMandatory(true).setMandatoryMsg("Billing Date is Mandatory").setRowSpan(0).setColSpan(0).build();
		
		/**
		 * Date 27/2/2018 
		 * By Jayshree
		 * Des.to make the field Mandatory
		 */
		FormField folbbillingcategory;
		FormField folbbillingtype;
		FormField folbbillinggroup;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument","MakeBillingCategoryMandatory" )){
			fbuilder = new FormFieldBuilder("* Billing Category",olbbillingcategory);
			 folbbillingcategory=fbuilder.setMandatory(true).setMandatoryMsg("Billing Category is Madatory").setRowSpan(0).setColSpan(0).build();
		}
		else{
			fbuilder = new FormFieldBuilder("Billing Category",olbbillingcategory);
			 folbbillingcategory=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument","MakeBillingTypeMandatory" )){
			fbuilder = new FormFieldBuilder("* Billing Type",olbbillingtype);
			 folbbillingtype=fbuilder.setMandatory(true).setMandatoryMsg("Billing Type is Mandatory !").setRowSpan(0).setColSpan(0).build();
		}
		else{
			fbuilder = new FormFieldBuilder("Billing Type",olbbillingtype);
			 folbbillingtype=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument","MakeBillingGroupMandatory" )){
			fbuilder = new FormFieldBuilder("* Billing Group",olbbillinggroup);
			 folbbillinggroup=fbuilder.setMandatory(true).setMandatoryMsg("Billing Group is Mandatory !").setRowSpan(0).setColSpan(0).build();
		}
		else{
			fbuilder = new FormFieldBuilder("Billing Group",olbbillinggroup);
			 folbbillinggroup=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
		
		//End By Jayshree
		
		fbuilder = new FormFieldBuilder("* Approver Name",olbApproverName);
		FormField folbApproverName= fbuilder.setMandatory(true).setMandatoryMsg("Approver Name  is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Person Responsible",olbEmployee);
		FormField folbEmployee= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Branch",olbbranch);
		FormField folbbranch= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Status",tbstatus);
		FormField ftbstatus=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Invoice ID",ibinvoiceid);
		FormField fibinvoiceid=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Invoice Date",dbinvoicedate);
		FormField fdbinvoicedate=fbuilder.setMandatory(true).setMandatoryMsg("Invoice Date is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Payment Date",dbpaymentdate);
		FormField fdbpaymentdate=fbuilder.setMandatory(true).setMandatoryMsg("Payment Date is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Description (Max 500 characters) ",tacomment);
		FormField ftacomment=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Account Type",tbaccounttype);
		FormField ftbaccounttype=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingBillingDocumentInfo=fbuilder.setlabel("Billing Document").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		fbuilder = new FormFieldBuilder("",billingDocumentTable.getTable());
		FormField fbillingtable=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Total Billing Amount",dototalbillamt);
		FormField fdototalbillamt=fbuilder.setMandatory(true).setMandatoryMsg("Total Billing Amount is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder();
		FormField fblankgroup=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingProductTable=fbuilder.setlabel("Product").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("",salesProductTable.getTable());
		FormField fsalesProducttbl=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder();
		FormField fblank=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		fbuilder = new FormFieldBuilder("",billingTaxTable.getTable());
		FormField fbillingTaxTable=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		fbuilder = new FormFieldBuilder("",billingChargesTable.getTable());
		FormField fbillingChargesTable=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingtaxinfo=fbuilder.setlabel("Taxes And Charges").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(3).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingPaymentTerms=fbuilder.setlabel("Payment Terms").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Days",ibpaytermsdays);
		FormField fibpaytermsdays=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();		
		fbuilder = new FormFieldBuilder("Percent",dopaytermspercent);
		FormField fdopaytermspercent=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Comment",tbpaytermscomment);
		FormField ftbpaytermscomment=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Remark",tbremark);
		FormField ftbremark=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Customer Branch",lbcustomerBranch);
		FormField folbCustomerBranch=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		FormField fgroupingBillingPeroid=fbuilder.setlabel("Billing Period Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();

		fbuilder = new FormFieldBuilder("Billing From Date",dbbillingperiodFromDate);
		FormField fdbbillingperiodFromDate=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Billing To Date",dbbillingperiodToDate);
		FormField fdbbillingperiodToDate=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Service Id",tbrateContractServiceid);
		FormField ftbrateContractServiceid=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/*
		 * nidhi
		 * 30-06-2017
		 */
		fbuilder = new FormFieldBuilder("Segment",tbSegment);
		FormField ftbSegment=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		/*
		 * End
		 */
		
		/*
		 * nidhi
		 * 30-06-2017
		 */
		fbuilder = new FormFieldBuilder("Quantity",quantity);
		FormField ftbQuantity=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("UOM",olbUOM);
		FormField folbUom=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		/*
		 * End
		 */
		
		/**
		 * nidhi
		 * 30-06-2017
		 */
		/**
		 * Date : 18-11-2017 BY ANIL
		 * making reference number 1 mandatory through process configuration
		 */
		FormField ftbReferenceNumber;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "MakeReferenceNumberMandatory")){
			fbuilder = new FormFieldBuilder("* Reference Number",tbReferenceNumber);
			ftbReferenceNumber= fbuilder.setMandatory(true).setMandatoryMsg("Reference Number is mandatory.").setRowSpan(0).setColSpan(0).build();
		}else{
			fbuilder = new FormFieldBuilder("Reference Number",tbReferenceNumber);
			ftbReferenceNumber= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
		/**
		 * End
		 */
		
		/**
		 * end
		 */
		
		
		/** Date 04-09-2017 added by vijay for discount amount final total amount and round off ***/
		fbuilder = new FormFieldBuilder("Total Amount", dbtotalAmt);
		FormField fdbtotalAmt = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("Discount Amount", dbDiscountAmt);
		FormField fdbDiscountAmt = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Total Amount", dbFinalTotalAmt);
		FormField fdbFinalTotalAmt = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Round Off", tbroundOffAmt);
		FormField fdbroundOffAmt = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Total Amount", dbtotalAmtIncludingTax);
		FormField fdbtotalAmtIncludingTax = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Total Amount", dbGrandTotalAmt);
		FormField fdbGrandTotalAmt = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",tblOtherCharges.getTable());
		FormField fothChgTbl= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("",addOthrChargesBtn);
		FormField faddOthrChargesBtn= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Total Amount",dbOtherChargesTotal);
		FormField fdbOtherChargesTotal= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		/**
		 * nidhi 14-12-2018 Updated with dropdown by vijay on 17-04-2019 NBHC CCPM
		 */ 
		fbuilder = new FormFieldBuilder("Cancel Amount Remark",olbCancellationRemark);
		FormField ftaCancleAmtRemark=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		/**
		 * @author Vijay Chougule Date:- 11-08-2020
		 * Des :- TO show CNC project Name
		 */
		fbuilder = new FormFieldBuilder("Project Name",tbprojectName);
		FormField ftbprojectName=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		ftbprojectName.setVisible(false);
		
		
		/**
		 * @author Vijay Chougule Date:- 12-08-2020
		 * Des :- TO show Credit Period from CNC and Sales Order to caluclate Payment Date
		 */
		fbuilder = new FormFieldBuilder("Credit Period",ibcreditPeriod);
		FormField fibcreditPeriod=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fibcreditPeriod.setVisible(false);
		
		/**
		 * @author Vijay Chougule Date:- 14-08-2020
		 * Des :- TO show CNC Contract number
		 */
		fbuilder = new FormFieldBuilder("Contract Number",tbContractNumber);
		FormField ftbContractNumber=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		ftbContractNumber.setVisible(false);
		
		
//		FormField[][] formfield = {   
//				{fgroupingCustomerInformation},
//				{ftbpersonCount,ftbpersonName,ftbpersonCell,ftbpocName},
//				{fgroupingSalesOrderInfo},
//				{fibcontractid,fdoamount,ftbaccounttype,ftbReferenceNumber},
//				{ftbprojectName,ftbContractNumber},
//				{fgroupingBillingDocumentInfo},
//				{fbillingtable},
//				{fgroupingProductTable},
//				{fsalesProducttbl},
//				{fblankgroup,fdbtotalAmt},
//				{fblankgroup,fdbDiscountAmt},
//				{fblankgroup,fdbFinalTotalAmt},
//				
//				{fblank,faddOthrChargesBtn},
//				{fblank,fothChgTbl},/** Date : 20-09-2017 BY ANIL**/
//				{fblankgroup,fdbOtherChargesTotal},
//				
//				{fblank,fbillingTaxTable},
//				{fblankgroup,fdbtotalAmtIncludingTax},
//				
////				{fblank,fbillingChargesTable},/**Date : 21-09-2017 BY ANIL **/
////				{fblankgroup,fdbGrandTotalAmt},
//				
//				{fblankgroup,fdbroundOffAmt},
//				{fblankgroup,fdototalbillamt},
//				{fgroupingPaymentTerms},
//				{fibpaytermsdays,fdopaytermspercent,ftbpaytermscomment,fibcreditPeriod},
//				{fgroupingBillingInfo},
//				{fibbillingid,fdbpostingdate,fdbinvoicedate,fdbpaymentdate},
//				{ftbstatus,folbbillingcategory,folbbillingtype,folbbillinggroup},
//				{fibinvoiceid,folbApproverName,folbEmployee,folbbranch},
//				{ftbremark,folbCustomerBranch,ftbrateContractServiceid,ftbSegment},
//				{ftbQuantity,folbUom},
//				{ftacomment},
//				{ftaCancleAmtRemark},
//				{fgroupingBillingPeroid},
//				{fdbbillingperiodFromDate,fdbbillingperiodToDate}
//		};
		
		FormField[][] formfield = {  
				/** Billing Details**/
				{fgroupingCustomerInformation},
				{ftbpersonCount,ftbpersonName,ftbpersonCell,ftbpocName},
				{fdbinvoicedate,fdbpaymentdate,folbbranch,folbApproverName},
				{ftbremark},
				/**Product Details**/
				{fgroupingProductTable},
				{fsalesProducttbl},
				{fblankgroup,fdbtotalAmt},
				{fblankgroup,fdbDiscountAmt},
				{fblankgroup,fdbFinalTotalAmt},
				
				{fblank,faddOthrChargesBtn},
				{fblank,fothChgTbl},/** Date : 20-09-2017 BY ANIL**/
				{fblankgroup,fdbOtherChargesTotal},
				
				{fblank,fbillingTaxTable},
				{fblankgroup,fdbtotalAmtIncludingTax},
				
//				{fblank,fbillingChargesTable},/**Date : 21-09-2017 BY ANIL **/
//				{fblankgroup,fdbGrandTotalAmt},
				
				{fblankgroup,fdbroundOffAmt},
				{fblankgroup,fdototalbillamt},
				/**Billing Info**/
				{fgroupingBillingDocumentInfo},
				{fdbpostingdate,fdbbillingperiodFromDate,fdbbillingperiodToDate},
				{fbillingtable},
				/**Payment Terms Details**/
				{fgroupingPaymentTerms},
				{fibpaytermsdays,fdopaytermspercent,ftbpaytermscomment},
				/**classification Details**/
				{fgroupingBillingInfo},
				{folbbillinggroup,folbbillingcategory,folbbillingtype,ftbSegment},
				/**General info Details**/
				{fgroupingSalesOrderInfo},
				{fibinvoiceid,fibcontractid,fdoamount,ftbrateContractServiceid},
				{ftbReferenceNumber,folbCustomerBranch,ftbQuantity,folbUom},
				{folbEmployee,ftbaccounttype,ftbprojectName},// Added by Priyanka
				{ftaCancleAmtRemark},
				{ftacomment}
				
		};


		/**
		 * @author Vijay Chougule Date:- 11-08-2020
		 * Des :- CNC project Name will display when below process config is active 
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "AddProjectName")){
			ftbprojectName.setVisible(true);
			ftbContractNumber.setVisible(true);
		}
		else{
			ftbprojectName.setVisible(false);
			ftbContractNumber.setVisible(false);

		}
		/**
		 * ends here
		 */
		
		/**
		 * @author Vijay Chougule Date:- 12-08-2020
		 * Des :- TO show Credit Period from CNC and Sales Order to caluclate Payment Date
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "AddCreditPeriodField")){
			fibcreditPeriod.setVisible(true);
		}
		else{
			fibcreditPeriod.setVisible(false);
		}
		/**
		 * ends here
		 */
		
		
		this.fields=formfield;

		/**
		 * Date 15-09-2018 By Vijay
		 * Des :- Bug :- getting null pionter exception so added timer to delay execute
		 */
		Timer timer = new Timer() {
			@Override
			public void run() {
				if(!parcialBillDisableFlag){
					olbCancellationRemark.setVisible(false);
					olbCancellationRemark.setVisible(false);
				}
			}
		};
		timer.schedule(2000);
	
	}

	@Override
	public void updateModel(BillingDocument model) {
		
		PersonInfo personInfo=new PersonInfo();
		personInfo.setCount(Integer.parseInt(tbpersonCount.getValue()));
		personInfo.setFullName(tbpersonName.getValue());
		personInfo.setCellNumber(Long.parseLong(tbpersonCell.getValue()));
		personInfo.setPocName(tbpocName.getValue());
		if(tbpersonCount.getValue()!=null){
			model.setPersonInfo(personInfo);
		}
		if(!ibcontractid.getValue().equals(""))
			model.setContractCount(Integer.parseInt(ibcontractid.getValue()));
		if(dosalesamount.getValue()!=null)
			model.setTotalSalesAmount(dosalesamount.getValue());
		if(dototalbillamt.getValue()!=null)
			model.setTotalBillingAmount(dototalbillamt.getValue());
		if(olbApproverName.getValue()!=null)
			model.setApproverName(olbApproverName.getValue());
		if(olbEmployee.getValue()!=null)
			model.setEmployee(olbEmployee.getValue());
		
		if(olbbranch.getSelectedIndex()!=0)
			model.setBranch(olbbranch.getValue(olbbranch.getSelectedIndex()));
		
		if(dbinvoicedate.getValue()!=null)
			model.setInvoiceDate(dbinvoicedate.getValue());
		if(dbpaymentdate.getValue()!=null)
			model.setPaymentDate(dbpaymentdate.getValue());
		if(tbstatus.getValue()!=null)
			model.setStatus(tbstatus.getValue());
		if(tacomment.getValue()!=null)
			model.setComment(tacomment.getValue());
		if(!ibinvoiceid.getValue().equals(""))
			model.setInvoiceCount(Integer.parseInt(ibinvoiceid.getValue()));
		if(dbbillingdate.getValue()!=null)
			model.setBillingDate(dbbillingdate.getValue());
		if(olbbillingcategory.getSelectedIndex()!=0)
			model.setBillingCategory(olbbillingcategory.getValue(olbbillingcategory.getSelectedIndex()));
		
		if(olbbillingtype.getSelectedIndex()!=0)
			model.setBillingType(olbbillingtype.getValue(olbbillingtype.getSelectedIndex()));
	
		if(olbbillinggroup.getSelectedIndex()!=0)
			model.setBillingGroup(olbbillinggroup.getValue(olbbillinggroup.getSelectedIndex()));
	
		if(tbaccounttype.getValue()!=null)
			model.setAccountType(tbaccounttype.getValue());
//		if(tbremark.getValue()!=null){
//			model.setre(tbremark.getValue());
//		}
		
		List<SalesOrderProductLineItem> salesProductlis=this.salesProductTable.getDataprovider().getList();
		ArrayList<SalesOrderProductLineItem> arrSalesProduct=new ArrayList<SalesOrderProductLineItem>();
		arrSalesProduct.addAll(salesProductlis);
		model.setSalesOrderProducts(arrSalesProduct);
		
		List<PaymentTerms> payTrmLis=this.savePaymentTerms();
		ArrayList<PaymentTerms> payTrmsArray=new ArrayList<PaymentTerms>();
		payTrmsArray.addAll(payTrmLis);
		model.setArrPayTerms(payTrmsArray);
		
		List<ContractCharges> taxBillingLis=this.billingTaxTable.getDataprovider().getList();
		ArrayList<ContractCharges> taxBillingArr=new ArrayList<ContractCharges>();
		taxBillingArr.addAll(taxBillingLis);
		model.setBillingTaxes(taxBillingArr);
		
		/**
		 * Date : 21-09-2017 BY ANIL
		 */
		List<OtherCharges> otherChargesList=this.tblOtherCharges.getDataprovider().getList();
		ArrayList<OtherCharges> otherChargesArr=new ArrayList<OtherCharges>();
		otherChargesArr.addAll(otherChargesList);
		model.setOtherCharges(otherChargesArr);
		
		if(dbOtherChargesTotal.getValue()!=null){
		model.setTotalOtherCharges(dbOtherChargesTotal.getValue());
		}
		
		/**
		 * End
		 */
		
		List<ContractCharges> chrgBillingLis=this.billingChargesTable.getDataprovider().getList();
		ArrayList<ContractCharges> chrgBillingArr=new ArrayList<ContractCharges>();
		chrgBillingArr.addAll(chrgBillingLis);
		model.setBillingOtherCharges(chrgBillingArr);
		
//		if(flatDiscStatus()==true){
		if(billingDocumentTable.getValue()!=null){
			model.setArrayBillingDocument(billingDocumentTable.getValue());
		}
		fillBillingDocumentTable();
//		}
		
		
		if(lbcustomerBranch.getSelectedIndex()!=0){
			model.setCustomerBranch(lbcustomerBranch.getValue(lbcustomerBranch.getSelectedIndex()));
		}else{
			model.setCustomerBranch("");
		}
		
		/**
		 * @author Abhinav Bihade
		 * @since 25/12/2019
		 * As per Vishnavi Pawar's Requirement ISPC - Even if remove from date and to date from billing period of billing screen 
		 * and invoice screen
		 */
		if(dbbillingperiodFromDate.getValue()!=null){
			model.setBillingPeroidFromDate(dbbillingperiodFromDate.getValue());
		}
		else{
			model.setBillingPeroidFromDate(null);
			}
		if(dbbillingperiodToDate.getValue()!=null){
			model.setBillingPeroidToDate(dbbillingperiodToDate.getValue());
		}
		else{
			model.setBillingPeroidToDate(null);
		}
		
		if(!tbrateContractServiceid.getValue().equals("")){
			model.setRateContractServiceId(Integer.parseInt(tbrateContractServiceid.getValue()));
		}
		
		/*
		 *  nidhi 30-06-2017
		 */
		
		if(tbSegment.getValue()!=null)
			model.setSegment(tbSegment.getValue());
		/*
		 * end
		 *
		 */
		
		/**
		 *  nidhi
		 *  20-07-2017
		 *    for save values
		 */
		if(quantity.getValue()!=null){
				model.setQuantity(quantity.getValue());
		}
		
		if(olbUOM.getValue()!=null){
			model.setUom(olbUOM.getValue().trim());
		}
		/**
		 * end
		 *
		 */
		
		/**
		 *  nidhi
		 *  24-07-2017
		 *  
		 */
		if(tbReferenceNumber.getValue()!=null)
		{
			model.setRefNumber(tbReferenceNumber.getValue());
			
		}
		else
		{
			model.setRefNumber(0+"");
		}
		/**
		 *  end
		 */
		
		/** Date 03-09-2017 added by vijay for discount amount final total amount and round off ***/
		if(dbDiscountAmt.getValue()!=null)
			model.setDiscountAmt(dbDiscountAmt.getValue());
		if(dbFinalTotalAmt.getValue() !=null)
			model.setFinalTotalAmt(dbFinalTotalAmt.getValue());
		if(tbroundOffAmt.getValue()!=null && !tbroundOffAmt.getValue().equals(""))
			model.setRoundOffAmt(Double.parseDouble(tbroundOffAmt.getValue()));
		if(dbtotalAmt.getValue()!=null)
			model.setTotalAmount(dbtotalAmt.getValue());
		if(dbtotalAmtIncludingTax.getValue()!=null)
			model.setTotalAmtIncludingTax(dbtotalAmtIncludingTax.getValue());
		if(dbGrandTotalAmt.getValue()!=null)
			model.setGrandTotalAmount(dbGrandTotalAmt.getValue());
		
		/**
		 * nidhi 14-12-2018
		 */
		if(olbCancellationRemark.getSelectedIndex()!=0)
			model.setCancleAmtRemark(olbCancellationRemark.getValue());
//		if(taCancleAmtRemark.getValue() != null &&taCancleAmtRemark.getValue().trim().length()>0){
//			model.setCancleAmtRemark(taCancleAmtRemark.getValue());
//		}

		
		if(dbCancelAmt.getValue()!=null){
			model.setCancleAmt(dbCancelAmt.getValue());
		}
		
		if(tbprojectName.getValue()!=null){
			model.setProjectName(tbprojectName.getValue());
		}
		
		if(ibcreditPeriod.getValue()!=null){
			model.setCreditPeriod(ibcreditPeriod.getValue());
		}
		if(tbContractNumber.getValue()!=null){
			model.setCncContractNumber(tbContractNumber.getValue());
		}
		
		billingDocObj=model;
		presenter.setModel(model);
		
	}

	@Override
	public void updateView(BillingDocument view) {

		billingDocObj=view;
		System.out.println("Flag"+flag);
		System.out.println("Flag 1"+flag1);
		System.out.println("Flag 2"+flag2);
		
		
		
		ibbillingid.setValue(view.getCount()+"");
		
		tbpersonCount.setValue(view.getPersonInfo().getCount()+"");
		tbpersonName.setValue(view.getPersonInfo().getFullName());
		tbpersonCell.setValue(view.getPersonInfo().getCellNumber()+"");
		tbpocName.setValue(view.getPersonInfo().getPocName());
		
		ibinvoiceid.setValue(view.getInvoiceCount()+"");
		
		if(view.getContractCount()!=null && view.getContractCount().toString().trim().length()>0) // nidhi 7-12-2018 for stop black id for invoice creation
			ibcontractid.setValue(view.getContractCount()+"");
		
		if(view.getApproverName()!=null)
			olbApproverName.setValue(view.getApproverName());
		if(view.getEmployee()!=null)
			olbEmployee.setValue(view.getEmployee());
		if(view.getBranch()!=null && !view.getBranch().equals(""))
			olbbranch.setValue(view.getBranch());
		if(view.getInvoiceDate()!=null)
			dbinvoicedate.setValue(view.getInvoiceDate());
		if(view.getPaymentDate()!=null)
			dbpaymentdate.setValue(view.getPaymentDate());
		if(view.getStatus()!=null)
			tbstatus.setValue(view.getStatus());
		if(view.getComment()!=null)
			tacomment.setValue(view.getComment());
		if(view.getTotalSalesAmount()!=0)
			dosalesamount.setValue(view.getTotalSalesAmount());
		if(view.getTotalBillingAmount()!=null)
			dototalbillamt.setValue(view.getTotalBillingAmount());
		if(view.getInvoiceCount()!=0)
			ibinvoiceid.setValue(view.getInvoiceCount()+"");
		if(view.getAccountType()!=null)
			tbaccounttype.setValue(view.getAccountType());
		
		//************//
		if(view.getBillingDate()!=null)
			dbbillingdate.setValue(view.getBillingDate());
		if(view.getBillingCategory()!=null)
			olbbillingcategory.setValue(view.getBillingCategory());
		if(view.getBillingType()!=null)
			olbbillingtype.setValue(view.getBillingType());
		if(view.getBillingGroup()!=null)
			olbbillinggroup.setValue(view.getBillingGroup());
		
		if(view.getArrPayTerms()!=null)
		{
			ibpaytermsdays.setValue(view.getArrPayTerms().get(0).getPayTermDays());
			dopaytermspercent.setValue(view.getArrPayTerms().get(0).getPayTermPercent());
			tbpaytermscomment.setValue(view.getArrPayTerms().get(0).getPayTermComment());
		}
		
		if(view.getRemark()!=null){
			tbremark.setValue(view.getRemark());
		}
		
		
		/** Date 03-09-2017 added by vijay for discount amount final total amount and round off ***/
		dbDiscountAmt.setValue(view.getDiscountAmt());
		dbFinalTotalAmt.setValue(view.getFinalTotalAmt());
		tbroundOffAmt.setValue(view.getRoundOffAmt()+"");
		dbtotalAmt.setValue(view.getTotalAmount());
		dbtotalAmtIncludingTax.setValue(view.getTotalAmtIncludingTax());
		System.out.println("view.getTotalAmtIncludingTax() "+view.getTotalAmtIncludingTax());
		dbGrandTotalAmt.setValue(view.getGrandTotalAmount());
		
		/**
		 * @author Anil @since 07-10-2021
		 * Moved product, tax and other charges code below
		 * this was cuasing issue when we navigate from invoice to bill or bill list to billing details form
		 * Raised by Rutuza for doormojo
		 */
		salesProductTable.setValue(view.getSalesOrderProducts());
		
		/**
		 * Date : 21-09-2017 BY ANIL
		 */
		tblOtherCharges.setValue(view.getOtherCharges());
		dbOtherChargesTotal.setValue(view.getTotalOtherCharges());
		/**
		 * End
		 */
		
		billingTaxTable.setValue(view.getBillingTaxes());
		billingChargesTable.setValue(view.getBillingOtherCharges());
		
		System.out.println("In view");
		if(flag==false&&flag1==true){
			System.out.println("Entered");
			fillBillingDocumentTable();
		}
		
		if(flag==false&&flag1==false&&flag2==false){
			System.out.println("Entered from list");
			fillBillingDocumentTable();
		}
			
		if(flag==true&&flag1==true){
			retrieveTotalBillingAmt();
		}
		
		if(multiContractFlag==false){
		
			if(view.getBillingOtherCharges().size()==0){
				retrieveBillingCharges(view.getCompanyId(),false,view.getBillingOtherCharges());
			}
			if(view.getBillingOtherCharges().size()!=0){
				retrieveBillingCharges(view.getCompanyId(),true,view.getBillingOtherCharges());
			}
		}
		
		if(view.getCustomerBranch()!=null){
			/***14-12-2019 Deepak Salve added this line for set Customer branch in Drop down list***/
			lbcustomerBranch.setValue(0,view.getCustomerBranch());
			/***End***/
			makelivecustomerBranchList(view.getPersonInfo().getCount(),view.getCustomerBranch());
		}
		
		
		if(view.getBillingPeroidFromDate()!=null){
			dbbillingperiodFromDate.setValue(view.getBillingPeroidFromDate());
		}
		if(view.getBillingPeroidToDate()!=null){
			dbbillingperiodToDate.setValue(view.getBillingPeroidToDate());
		}
			
		
		if(view.getRateContractServiceId()!=0){
			tbrateContractServiceid.setValue(view.getRateContractServiceId()+"");
		}
		/*
		 *  nidhi 30-06-2017
		 */
		
		if(view.getSegment()!= null){
			tbSegment.setValue(view.getSegment());
		}
		/*
		 * end
		 *
		 */
		/**
		 *  nidhi
		 *  20-07-2017
		 *    for save values
		 */
		if(view.getUom()!=null&&!view.getUom().trim().equals("")){
			olbUOM.setValue(view.getUom().trim());
		}
		if(view.getQuantity()!=0){
			quantity.setValue(view.getQuantity());
		}
		/**
		 * end
		 *
		 */
		/**
		 *  nidhi
		 *  24-07-2017
		 */
		if(view.getRefNumber()!=null)
			tbReferenceNumber.setValue(view.getRefNumber());
		/**
		 * end
		 */
			
			
		/**
		 * nidhi
		 * 5-12-2017
		 * 
		 */
		valideCofigInfoDetails( view);
		/**
		 *  end
		 */
		/**
		 * nidhi
		 * 29-08-2018
		 */
		if(LoginPresenter.mapModelSerialNoFlag){

			if(view.getTypeOfOrder().equals(AppConstants.SALESORDER) || view.getTypeOfOrder().equals(AppConstants.PURCHASEORDER)){
				salesProductTable.setFlagforserialView(true);
			}else{
				salesProductTable.setFlagforserialView(false);
			}
			if(AppMemory.getAppMemory().currentState == ScreeenState.EDIT || AppMemory.getAppMemory().currentState  == ScreeenState.NEW){
				salesProductTable.setEnable(true);
			}else{
				salesProductTable.setEnable(false);
			}
		}
		/**
		 * nidhi 14-12-2018
		 */
		if(view.getCancleAmtRemark()!=null){
			olbCancellationRemark.setValue(view.getCancleAmtRemark());
		}
		if(view.getCancleAmt()!=0){
			dbCancelAmt.setValue(view.getCancleAmt());
		}
		
		if(view.getProjectName()!=null){
			tbprojectName.setValue(view.getProjectName());
		}
		
		ibcreditPeriod.setValue(view.getCreditPeriod());
		
		if(view.getCncContractNumber()!=null){
			tbContractNumber.setValue(view.getCncContractNumber());
		}
		
		/* 
		 * for approval process
		 *  nidhi
		 *  5-07-2017
		 */
		if(presenter != null){
			presenter.setModel(view);
		}
		/*
		 *  end
		 */
	}
	

	void valideCofigInfoDetails(final BillingDocument view){
		boolean flagCate = false;
		
	if(view.getBillingCategory()!=null){
		
		for(ConfigCategory congi : LoginPresenter.globalCategory){
			if(congi.getCategoryName().equalsIgnoreCase(view.getBillingCategory()) && congi.getInternalType() == 8){
				flagCate = true;
				break;
			}
		}
		
		if (!flagCate) {
			String configType=AppConstants.GLOBALRETRIEVALCATEGORY+"-"+view.getCompanyId();
			int i =AppUtility.globalMakeLiveCategory(configType);
		}
		
		olbbillingcategory.removeAllItems();
		AppUtility.MakeLiveCategoryConfig(olbbillingcategory, Screen.BILLINGCATEGORY);
	}
		

	
	 flagCate = false;
		
	 if(view.getBillingType()!=null)
	 {
		 for(Type congi : LoginPresenter.globalType){
				if(congi.getCategoryName().equalsIgnoreCase(view.getBillingCategory()) && congi.getTypeName().equalsIgnoreCase(view.getBillingType()) && congi.getInternalType() == 8){
					flagCate = true;
					break;
				}
			}
		 
		
		
		if (!flagCate) {
			String configType=AppConstants.GLOBALRETRIEVALTYPE+"-"+view.getCompanyId();
			int i =AppUtility.globalMakeLiveType(configType);
		}
		
		 olbbillingtype.removeAllItems();
		 AppUtility.makeTypeListBoxLive(olbbillingtype, Screen.BILLINGTYPE);
	 }
		
	 flagCate = false;
	 
	 if(view.getBillingGroup()!=null){
		 for(Config congi : LoginPresenter.globalConfig){
				if(congi.getName().equalsIgnoreCase(view.getBillingGroup()) && congi.getType() == 46){
					flagCate = true;
					break;
				}
			}
			
			if (!flagCate) {
				String configType=AppConstants.GLOBALRETRIEVALCONFIG+"-"+view.getCompanyId();
				int i =AppUtility.globalMakeLiveConfig(configType);
			}
			
			
			
			olbbillinggroup.removeAllItems();
			
			
			AppUtility.MakeLiveConfig(olbbillinggroup, Screen.BILLINGGROUP);
	 }
		
		
		Timer timer=new Timer() 
	   	 {
			@Override
			public void run() 
				{
				if(view.getBillingCategory()!=null)
					olbbillingcategory.setValue(view.getBillingCategory());
				if(view.getBillingType()!=null)
					olbbillingtype.setValue(view.getBillingType());
				if(view.getBillingGroup()!=null)
					olbbillinggroup.setValue(view.getBillingGroup());
				
				}
	   	 };
	   	timer.schedule(5000); 
	
}
	
	public TextBox getTbSegment() {
		return tbSegment;
	}

	public void setTbSegment(TextBox tbSegment) {
		this.tbSegment = tbSegment;
	}

	private void makelivecustomerBranchList(int count, final String customerBranch) {



		GenricServiceAsync service=GWT.create(GenricService.class);

		System.out.println("hi");
		System.out.println("Value cinfo=="+count);
		
		MyQuerry querry = new MyQuerry();
		
	  	Company c = new Company();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
	  
		filter = new Filter();
	  	filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("cinfo.count");
		filter.setIntValue(count);
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new CustomerBranchDetails());
		
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				
				System.out.println("RESULT=="+result.size());
				
				lbcustomerBranch.clear();
				lbcustomerBranch.addItem("--SELECT--");

				System.out.println("result"+result.size());

				/***14-12-2019 Deepak Salve added this line for set Customer branch in Drop down list***/
				if(result.size()!=0)
				{
				for(SuperModel model:result){
					CustomerBranchDetails  customerbranch = (CustomerBranchDetails)model;
					lbcustomerBranch.addItem(customerbranch.getBusinessUnitName());
					/***14-12-2019 Deepak Salve added this line for set Customer branch in Drop down list***/
					lbcustomerBranch.setValue(0,customerbranch.getBusinessUnitName());
					/***End***/
				}
					int count = lbcustomerBranch.getItemCount();
					System.out.println("listbox current ==="+count);
					String item= customerBranch;
					System.out.println("Item==="+item);
					for(int i=0;i<count;i++)
					{
						if(item.trim().equals(lbcustomerBranch.getItemText(i).trim()))
						{
							System.out.println("hiiiiiiiiiiii");
							lbcustomerBranch.setSelectedIndex(i);
							break;
						}
					}
				}
				/***14-12-2019 Deepak Salve added this line for set Customer branch in Drop down list***/
				lbcustomerBranch.setValue(0, customerBranch);
				/***End***/
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				showDialogMessage("An Unexpected Error occured !");
			}
		});
		
		
	
	}
	
	@Override
	public void setCount(int count)
	{
		ibbillingid.setValue(count+"");
	}

	
	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		/**
		 * @author Anil,Date:17-04-2019
		 * renaming default column no of branch to Qty and hiding column area for order type sales and purchase
		 */
		if(billingDocObj!=null){
			String orderType=billingDocObj.getTypeOfOrder();
			System.out.println("Order type : "+orderType);
			salesProductTable.orderType=orderType;
			if(billingDocObj.getTypeOfOrder().equalsIgnoreCase(AppConstants.ORDERTYPEPURCHASE) || billingDocObj.getTypeOfOrder().equalsIgnoreCase(AppConstants.ORDERTYPESALES)){
				salesProductTable.qtyHeader="Qty";
			}else{
				salesProductTable.qtyHeader=null;
			}
		}
		/**
		 * End
		 */
		
		dbcreationdate.setEnabled(false);
		tbpersonCount.setEnabled(false);
		tbpersonName.setEnabled(false);
		tbpersonCell.setEnabled(false);
		tbpocName.setEnabled(false);
		ibbillingid.setEnabled(false);
		tbstatus.setEnabled(false);
		ibinvoiceid.setEnabled(false);
		billingDocumentTable.setEnable(state);
		System.out.println("rohan in side enable "+state);
		salesProductTable.setEnable(state);
		dosalesamount.setEnabled(false);
		dototalbillamt.setEnabled(false);
		billingTaxTable.setEnable(state);
		billingChargesTable.setEnable(state);
		ibpaytermsdays.setEnabled(false);
		dopaytermspercent.setEnabled(false);
		tbpaytermscomment.setEnabled(state);
		tbaccounttype.setEnabled(false);
		tbremark.setEnabled(false);
		
		dbbillingperiodFromDate.setEnabled(state);
		dbbillingperiodToDate.setEnabled(state);
		tbrateContractServiceid.setEnabled(false);
		
		ibcontractid.setEnabled(false);
		
		/**
		 * Date : 11-07-2017 By NIDHI
		 */
		
		tbSegment.setEnabled(false);
		/**
		 * End
		 */
		/*
		 *  nidhi
		 *  20-07-2017
		 *   set display status
		 */
		olbUOM.setEnabled(state);
		quantity.setEnabled(state);
		/*
		 *  end
		 */
		/**
		 *  nidhi
		 *  24-07-2017
		 *   
		 */
		tbReferenceNumber.setEnabled(state);
		/**
		 *  end
		 */
		
		//Date 03-09-2017 added by vijay for total amount discount and roundoff
		this.dbtotalAmt.setEnabled(false);
		this.dbDiscountAmt.setEnabled(state);
		this.dbFinalTotalAmt.setEnabled(false);
		this.tbroundOffAmt.setEnabled(state);
		this.dbtotalAmtIncludingTax.setEnabled(false);
		this.dbGrandTotalAmt.setEnabled(false);
		
		/**
		 * Date : 21-09-2017 BY ANIL
		 */
		tblOtherCharges.setEnable(state);
		dbOtherChargesTotal.setEnabled(false);
		/**
		 * End
		 */
		
		/**
		 * Date 06-07-2018
		 * Developer :- Vijay
		 * Des :- This is for NBHC Billing Date should not be editable for Branch user and 
		 * Admin user can able to edit billing date
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "OnlyForNBHC")){
			if(!(LoginPresenter.myUserEntity.getRole().getRoleName().trim().equalsIgnoreCase("ADMIN"))){
				dbbillingdate.setEnabled(false);
			}else{
				dbbillingdate.setEnabled(state);
			}
		}
		/**
		 * ends here
		 */
		
		tbprojectName.setEnabled(false);
		ibcreditPeriod.setEnabled(state);
		tbContractNumber.setEnabled(false);

	}

	public void setAppHeaderBarAsPerStatus()
	{
		BillingDocument entity=(BillingDocument) presenter.getModel();
		String status=entity.getStatus();
		
		if(status.equals(BillingDocument.REQUESTED)||status.equals(BillingDocument.REQUESTED))
		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			}
			
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Discard")||text.contains("Search")||text.contains(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
			}
		}
		
		if(status.equals(BillingDocument.APPROVED)||status.equals(BillingDocument.APPROVED))
		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			}
			
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Discard")||text.contains("Search")||text.contains(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
			}
		}
		
		if(status.equals(BillingDocument.BILLINGINVOICED)||status.equals(BillingDocument.BILLINGINVOICED))
		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			}
			
			
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Discard")||text.contains("Search")||text.contains(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
			}
		}
		
		if(status.equals(BillingDocument.CANCELLED)||status.equals(BillingDocument.CANCELLED))
		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			}
			
			
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Discard")||text.contains("Search")||text.contains(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
			}
		}
	}

	
	

	
	public void changeProcessLevel()
	{
		BillingDocument entity=(BillingDocument) presenter.getModel();
		String status=entity.getStatus();
		String orderType=entity.getTypeOfOrder();
		System.out.println("typeoforder"+entity.getTypeOfOrder());
		
		boolean renewFlagStatus=entity.isRenewFlag();
		
		for(int i=0;i<getProcesslevelBarNames().length;i++)
		{
			InlineLabel label=getProcessLevelBar().btnLabels[i];
			String text=label.getText().trim();
			if(status.equals(BillingDocument.CREATED))
			{
				/**
				 * Date : 23-05-2017 By ANIL
				 * Checking self approval
				 */
				if(getManageapproval().isSelfApproval()){
					if(text.equals(AppConstants.CREATEPROFORMAINVOICE))
						label.setVisible(false);
					if(text.equals(AppConstants.CREATETAXINVOICE))
						label.setVisible(false);
					if(text.equals(ManageApprovals.APPROVALREQUEST))
						label.setVisible(false);
					if(text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
						label.setVisible(false);
					if(text.equals(AppConstants.CANCELBILLING))
						label.setVisible(true);
					if(text.equals(ManageApprovals.SUBMIT))
						label.setVisible(true);
				}else{
					if(text.equals(AppConstants.CREATEPROFORMAINVOICE))
						label.setVisible(false);
					if(text.equals(AppConstants.CREATETAXINVOICE))
						label.setVisible(false);
					if(text.equals(ManageApprovals.APPROVALREQUEST))
						label.setVisible(true);
					if(text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
						label.setVisible(false);
					if(text.equals(AppConstants.CANCELBILLING))
						label.setVisible(true);
					if(text.equals(ManageApprovals.SUBMIT))
						label.setVisible(false);
				}
				
				/**
				 * Date : 29-07-2017 By ANIL
				 */
				if(text.equals(AppConstants.VIEWINVOICE)){
					label.setVisible(false);
				}
				/**
				 * End
				 */
				
				/**
				 * Date : 11-10-2017 By ANIL
				 */
				if(text.equals(AppConstants.VIEWORDER)){
					label.setVisible(true);
				}
				/**
				 * End
				 */
				/**
				 * Date : 31-10-2017 By ANIL
				 */
				if(text.equals(AppConstants.VIEWSERVICE)){
					label.setVisible(true);
				}
				/**
				 * End
				 */
				/**@Sheetal : 14-02-2022**/
				if(text.equals(AppConstants.VIEWPAYMENT)){
					label.setVisible(true);
				}
			if(orderType.equals(AppConstants.ORDERTYPEPURCHASE)&&text.equals(AppConstants.VIEWGRN)){
					label.setVisible(true);
				
				}
			if(!orderType.equals(AppConstants.ORDERTYPEPURCHASE)&&text.equals(AppConstants.VIEWGRN)){
				label.setVisible(false);
			}
			
			/**
			 * Date :- 10-04-2019 by Vijay
			 */
			if(text.equals(AppConstants.SUBMITINVOICE)){
				label.setVisible(false);
			}
			/**
			 * Date :- 20-04-2019 by Vijay
			 */
			if(text.equals(AppConstants.CHANGESTATUSTOCREATED)){
				label.setVisible(false);
			}

			}
			if(status.equals(BillingDocument.REQUESTED))
			{
				if(text.equals(AppConstants.CREATEPROFORMAINVOICE))
					label.setVisible(false);
				if(text.equals(AppConstants.CREATETAXINVOICE))
					label.setVisible(false);
				if(text.equals(ManageApprovals.APPROVALREQUEST))
					label.setVisible(false);
				if(text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
					label.setVisible(true);
				if(text.equals(AppConstants.CANCELBILLING))
					label.setVisible(false);
				if(text.equals(ManageApprovals.SUBMIT))
					label.setVisible(false);
				/**
				 * Date : 29-07-2017 By ANIL
				 */
				if(text.equals(AppConstants.VIEWINVOICE)){
					label.setVisible(false);
				}
				/**
				 * End
				 */
				/**
				 * Date : 11-11-2017 By ANIL
				 */
				if(text.equals(AppConstants.VIEWORDER)){
					label.setVisible(true);
				}
				/**
				 * End
				 */
				/**
				 * Date : 31-10-2017 By ANIL
				 */
				if(text.equals(AppConstants.VIEWSERVICE)){
					label.setVisible(true);
				}
				/**
				 * End
				 */
				/**@Sheetal : 14-02-2022**/
				if(text.equals(AppConstants.VIEWPAYMENT)){
					label.setVisible(true);
				}
				if(orderType.equals(AppConstants.ORDERTYPEPURCHASE)&&text.equals(AppConstants.VIEWGRN)){
					label.setVisible(true);
				
				}
				if(!orderType.equals(AppConstants.ORDERTYPEPURCHASE)&&text.equals(AppConstants.VIEWGRN)){
					label.setVisible(false);
				}
				
				/**
				 * Date :- 10-04-2019 by Vijay
				 */
				if(text.equals(AppConstants.SUBMITINVOICE)){
					label.setVisible(false);
				}
				/**
				 * Date :- 20-04-2019 by Vijay
				 */
				if(text.equals(AppConstants.CHANGESTATUSTOCREATED)){
					label.setVisible(false);
				}
			}
			
			if(status.equals(BillingDocument.REJECTED))
			{
				if(text.equals(AppConstants.CREATEPROFORMAINVOICE))
					label.setVisible(false);
				if(text.equals(AppConstants.CREATETAXINVOICE))
					label.setVisible(false);
				if(text.equals(ManageApprovals.APPROVALREQUEST))
					label.setVisible(true);
				if(text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
					label.setVisible(false);
				if(text.equals(AppConstants.CANCELBILLING))
					label.setVisible(false);
				if(text.equals(ManageApprovals.SUBMIT))
					label.setVisible(false);
				/**
				 * Date : 29-07-2017 By ANIL
				 */
				if(text.equals(AppConstants.VIEWINVOICE)){
					label.setVisible(false);
				}
				/**
				 * End
				 */
				/**
				 * Date : 11-11-2017 By ANIL
				 */
				if(text.equals(AppConstants.VIEWORDER)){
					label.setVisible(true);
				}
				/**
				 * End
				 */
				/**
				 * Date : 31-10-2017 By ANIL
				 */
				if(text.equals(AppConstants.VIEWSERVICE)){
					label.setVisible(true);
				}
				/**
				 * End
				 */
				/**@Sheetal : 14-02-2022**/
				if(text.equals(AppConstants.VIEWPAYMENT)){
					label.setVisible(true);
				}
				if(orderType.equals(AppConstants.ORDERTYPEPURCHASE)&&text.equals(AppConstants.VIEWGRN)){
					label.setVisible(true);
				
				}
				if(!orderType.equals(AppConstants.ORDERTYPEPURCHASE)&&text.equals(AppConstants.VIEWGRN)){
					label.setVisible(false);
				}
				/**
				 * Date :- 10-04-2019 by Vijay
				 */
				if(text.equals(AppConstants.SUBMITINVOICE)){
					label.setVisible(false);
				}
				/**
				 * Date :- 20-04-2019 by Vijay
				 */
				if(text.equals(AppConstants.CHANGESTATUSTOCREATED)){
					label.setVisible(false);
				}
			}
			
			if(status.equals(BillingDocument.APPROVED))
			{
				/**
				 * Date 16-08-2018 By Vijay
				 * Des :- profroma invoice button should not display to branch user 
				 * for NBHC CCPM
				 */
				 //Ashwini Patil Date:28-03-2023
				if(entity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE)){
					if(text.equals(AppConstants.CREATEPROFORMAINVOICE))
						label.setVisible(false);
				}else{
					if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "OnlyForNBHC") &&
							!(LoginPresenter.myUserEntity.getRole().getRoleName().trim().equalsIgnoreCase("ADMIN"))){
							if(text.equals(AppConstants.CREATEPROFORMAINVOICE))
								label.setVisible(false);
						}else{					
							if(text.equals(AppConstants.CREATEPROFORMAINVOICE))
								label.setVisible(true);
						}				
				
				}
				/**
				 * Date 10-04-2019 by vijay for NBHC CCPM Auto Invoice then tax invoice should not display
				 */
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "EnableAutoInvoice")){
					if(text.equals(AppConstants.SUBMITINVOICE)){
						label.setVisible(true);
					}
					if(text.equals(AppConstants.CREATETAXINVOICE))
						label.setVisible(false);
				}
				else{
					if(text.equals(AppConstants.CREATETAXINVOICE))
						label.setVisible(true);
					if(text.equals(AppConstants.SUBMITINVOICE)){
						label.setVisible(false);
					}
				}
				
				if(text.equals(ManageApprovals.APPROVALREQUEST))
					label.setVisible(false);
				if(text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
					label.setVisible(false);
				
				/**
				 * rohan added this code for NBHC Changes ADMIN role should  
				 */
				
				/**Added by priyanka - 28-07-2021
				 * Des : zonal coordinator role should have acess like admin role.
				 */
				if(LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN")||LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("Zonal Coordinator")){
				if(text.equals(AppConstants.CANCELBILLING))
					label.setVisible(true);
				}
				
				
				
				if(text.equals(ManageApprovals.SUBMIT))
					label.setVisible(false);
				
				/**
				 * Date : 29-07-2017 By ANIL
				 */
				if(text.equals(AppConstants.VIEWINVOICE)){
					label.setVisible(false);
				}
				/**
				 * End
				 */
				/**
				 * Date : 11-11-2017 By ANIL
				 */
				if(text.equals(AppConstants.VIEWORDER)){
					label.setVisible(true);
				}
				/**
				 * End
				 */
				/**
				 * Date : 31-10-2017 By ANIL
				 */
				if(text.equals(AppConstants.VIEWSERVICE)){
					label.setVisible(true);
				}
				/**
				 * End
				 */
				/**@Sheetal : 14-02-2022**/
					if(text.equals(AppConstants.VIEWPAYMENT)){
						label.setVisible(true);
					}
				if(orderType.equals(AppConstants.ORDERTYPEPURCHASE)&&text.equals(AppConstants.VIEWGRN)){
					label.setVisible(true);
				
				}
				if(!orderType.equals(AppConstants.ORDERTYPEPURCHASE)&&text.equals(AppConstants.VIEWGRN)){
					label.setVisible(false);
				}
				/**
				 * Date :- 20-04-2019 by Vijay
				 */
				if(text.equals(AppConstants.CHANGESTATUSTOCREATED)){
					label.setVisible(false);
				}
			}
			if(status.equals(BillingDocument.BILLINGINVOICED)&&renewFlagStatus==false)
			{
				if(text.equals(AppConstants.CREATEPROFORMAINVOICE))
					label.setVisible(false);
				if(text.equals(AppConstants.CREATETAXINVOICE))
					label.setVisible(false);
				if(text.equals(ManageApprovals.APPROVALREQUEST))
					label.setVisible(false);
				if(text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
					label.setVisible(false);
				/**
				 * rohan added this code for NBHC Changes ADMIN role should  
				 */
				/**
				 *  Added by Priyanka 
				 *  Des : Zonal coprdinator role should have same access like admin role.
				 */
				if(LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN")||LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("Zonal Coordinator")){
				if(text.equals(AppConstants.CANCELBILLING))
					label.setVisible(true);
				}
				else
				{
					if(text.equals(AppConstants.CANCELBILLING))
						label.setVisible(false);
				}
				if(text.equals(ManageApprovals.SUBMIT))
					label.setVisible(false);
				
				/** Date 15-08-2017 if condition added by  vijay
				 * and old code added in else block 
				 */
				
				if(isPopUpAppMenubar() && !isSummaryPopupFlag()){//Ashwini Patil added && !isSummaryPopupFlag()
					
					if(text.equals(AppConstants.VIEWINVOICE)){
						label.setVisible(false);
					}
					
					/**
					 * Date : 11-11-2017 By ANIL
					 */
					if(text.equals(AppConstants.VIEWORDER)){
						label.setVisible(false);
					}
					/**
					 * End
					 */
					/**
					 * Date : 31-10-2017 By ANIL
					 */
					if(text.equals(AppConstants.VIEWSERVICE)){
						label.setVisible(false);
					}
					if(orderType.equals(AppConstants.ORDERTYPEPURCHASE)&&text.equals(AppConstants.VIEWGRN)){
						label.setVisible(true);
					
					}
					if(!orderType.equals(AppConstants.ORDERTYPEPURCHASE)&&text.equals(AppConstants.VIEWGRN)){
						label.setVisible(false);
					}
					/**
					 * End
					 */
					
				}else{
					/**
					 * Date : 29-07-2017 By ANIL
					 */
					
					if(text.equals(AppConstants.VIEWINVOICE)){
						label.setVisible(true);
					}
					
					/**
					 * Date : 11-11-2017 By ANIL
					 */
					if(text.equals(AppConstants.VIEWORDER)){
						label.setVisible(true);
					}
					/**
					 * End
					 */
					/**
					 * Date : 31-10-2017 By ANIL
					 */
					if(text.equals(AppConstants.VIEWSERVICE)){
						label.setVisible(true);
					}
					/**@Sheetal : 14-02-2022**/
					if(text.equals(AppConstants.VIEWPAYMENT)){
						label.setVisible(true);
					}
					
					if(orderType.equals(AppConstants.ORDERTYPEPURCHASE)&&text.equals(AppConstants.VIEWGRN)){
						label.setVisible(true);
					
					}
					if(!orderType.equals(AppConstants.ORDERTYPEPURCHASE)&&text.equals(AppConstants.VIEWGRN)){
						label.setVisible(false);
					}
					/**
					 * End
					 */
				}
				/**
				 * End
				 */
				/**
				 * Date :- 10-04-2019 by Vijay
				 */
				if(text.equals(AppConstants.SUBMITINVOICE)){
					label.setVisible(false);
				}
				/**
				 * Date :- 20-04-2019 by Vijay
				 */
				if(text.equals(AppConstants.CHANGESTATUSTOCREATED)){
					label.setVisible(false);
				}
			}
			
			if(status.equals(BillingDocument.BILLINGINVOICED)&&renewFlagStatus==true)
			{
				if(text.equals(AppConstants.CREATEPROFORMAINVOICE))
					label.setVisible(false);
				if(text.equals(AppConstants.CREATETAXINVOICE))
					label.setVisible(false);
				if(text.equals(ManageApprovals.APPROVALREQUEST))
					label.setVisible(false);
				if(text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
					label.setVisible(false);
				if(text.equals(AppConstants.CANCELBILLING))
					label.setVisible(false);
				if(text.equals(ManageApprovals.SUBMIT))
					label.setVisible(false);
				/**
				 * Date : 29-07-2017 By ANIL
				 */
				if(text.equals(AppConstants.VIEWINVOICE)){
					label.setVisible(true);
				}
				/**
				 * End
				 */
				/**
				 * Date : 11-11-2017 By ANIL
				 */
				if(text.equals(AppConstants.VIEWORDER)){
					label.setVisible(true);
				}
				/**
				 * End
				 */
				/**
				 * Date : 31-10-2017 By ANIL
				 */
				if(text.equals(AppConstants.VIEWSERVICE)){
					label.setVisible(true);
				}
				/**
				 * End
				 */
				/**@Sheetal : 14-02-2022**/
				if(text.equals(AppConstants.VIEWPAYMENT)){
					label.setVisible(true);
				}
				if(orderType.equals(AppConstants.ORDERTYPEPURCHASE)&&text.equals(AppConstants.VIEWGRN)){
					label.setVisible(true);
				
				}
				if(!orderType.equals(AppConstants.ORDERTYPEPURCHASE)&&text.equals(AppConstants.VIEWGRN)){
					label.setVisible(false);
				}
				/**
				 * Date :- 10-04-2019 by Vijay
				 */
				if(text.equals(AppConstants.SUBMITINVOICE)){
					label.setVisible(false);
				}
				/**
				 * Date :- 20-04-2019 by Vijay
				 */
				if(text.equals(AppConstants.CHANGESTATUSTOCREATED)){
					label.setVisible(false);
				}
			}
			
			if(status.equals(BillingDocument.CANCELLED))
			{
				if(text.equals(AppConstants.CREATEPROFORMAINVOICE))
					label.setVisible(false);
				if(text.equals(AppConstants.CREATETAXINVOICE))
					label.setVisible(false);
				if(text.equals(ManageApprovals.APPROVALREQUEST))
					label.setVisible(false);
				if(text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
					label.setVisible(false);
				if(text.equals(AppConstants.CANCELBILLING))
					label.setVisible(false);
				if(text.equals(ManageApprovals.SUBMIT))
					label.setVisible(false);
				/**
				 * Date : 29-07-2017 By ANIL
				 */
				if(text.equals(AppConstants.VIEWINVOICE)){
					label.setVisible(false);
				}
				/**
				 * End
				 */
				
				/**
				 * Date : 11-11-2017 By ANIL
				 */
				if(text.equals(AppConstants.VIEWORDER)){
					label.setVisible(true);
				}
				/**
				 * End
				 */
				/**
				 * Date : 31-10-2017 By ANIL
				 */
				if(text.equals(AppConstants.VIEWSERVICE)){
					label.setVisible(true);
				}
				/**@Sheetal : 14-02-2022**/
				if(text.equals(AppConstants.VIEWPAYMENT)){
					label.setVisible(true);
				}
				if(orderType.equals(AppConstants.ORDERTYPEPURCHASE)&&text.equals(AppConstants.VIEWGRN)){
					label.setVisible(true);
				}
				if(!orderType.equals(AppConstants.ORDERTYPEPURCHASE)&&text.equals(AppConstants.VIEWGRN)){
					label.setVisible(false);
				}
				/**
				 * End
				 */
				/**
				 * Date :- 10-04-2019 by Vijay
				 */
				if(text.equals(AppConstants.SUBMITINVOICE)){
					label.setVisible(false);
				}
				/**
				 * Date :- 20-04-2019 by Vijay
				 */
				 
				 /**
				 * Date :- 4-08-2021 by Priyanka
				 */
				if(LoginPresenter.myUserEntity.getRole().getRoleName().equalsIgnoreCase("Admin")|| LoginPresenter.myUserEntity.getRole().getRoleName().equalsIgnoreCase("Zonal Coordinator")){
					if(text.equals(AppConstants.CHANGESTATUSTOCREATED))
					label.setVisible(true);
				}
				else{
					if(text.equals(AppConstants.CHANGESTATUSTOCREATED))
					label.setVisible(false);
				}
			}
			if(isPopUpAppMenubar()&& !isSummaryPopupFlag()){ //Ashwini Patil added && !isSummaryPopupFlag()
			if(text.equals(AppConstants.VIEWORDER)){
				label.setVisible(false);
			}
			}
			if(isPopUpAppMenubar()&& !isSummaryPopupFlag()){ //Ashwini Patil added && !isSummaryPopupFlag()
			if(text.equals(AppConstants.VIEWSERVICE)){
				label.setVisible(false);
			}
			}
			
			//Ashwini Patil Date:20-09-2022
			if(isHideNavigationButtons()) {
				Console.log("in changeProcessLevel isHideNavigationButtons");
				if(text.equalsIgnoreCase(AppConstants.VIEWSERVICE)||text.equalsIgnoreCase(AppConstants.VIEWORDER)||text.equalsIgnoreCase(AppConstants.VIEWINVOICE)||text.equalsIgnoreCase(AppConstants.VIEWGRN)||text.equalsIgnoreCase(AppConstants.VIEWPAYMENT))
					label.setVisible(false);
			}
			
		}
		
	}
	
	@Override
	public void setToViewState() {
		super.setToViewState();
		invCreationFlag=false;
		Console.log("INVOICE CREATION FLAG : "+invCreationFlag);
		this.setMenus();
		if(billingDocObj!=null){
		SuperModel model=new BillingDocument();
		model=billingDocObj;
		AppUtility.addDocumentToHistoryTable(AppConstants.ACCOUNTMODULE,AppConstants.BILLINGDETAILS, billingDocObj.getCount(), billingDocObj.getPersonInfo().getCount(),billingDocObj.getPersonInfo().getFullName(),billingDocObj.getPersonInfo().getCellNumber(), false, model, null);
		/**
		 * Date 31-Oct-2018 By Vijay
		 * Des :- Bug :- can not merge two billing document
		 */
		// DATE 14.7.2018 ADDED BY KOMAL
		if(billingDocObj.getTypeOfOrder().equalsIgnoreCase(AppConstants.ORDERTYPEPURCHASE) || billingDocObj.getTypeOfOrder().equalsIgnoreCase(AppConstants.ORDERTYPESALES)){
			salesProductTable.qtyHeader = "Qty";
			salesProductTable.setEnable(false);
		}
		
		
		}
		String mainScreenLabel="Billing Details";
		if(billingDocObj!=null&&billingDocObj.getCount()!=0){
			mainScreenLabel=billingDocObj.getCount()+" "+"/"+" "+billingDocObj.getStatus()+" "+"/"+" "+AppUtility.parseDate(billingDocObj.getCreationDate());
		}
		fgroupingCustomerInformation.getHeaderLabel().setText(mainScreenLabel);
		
		
	}
	
	@Override
	public void setToEditState() {
		super.setToEditState();
		this.processLevelBar.setVisibleFalse(false);
		
		changeStatusToCreated();
		
		String mainScreenLabel="Billing Details";
		if(billingDocObj!=null&&billingDocObj.getCount()!=0){
			mainScreenLabel=billingDocObj.getCount()+" "+"/"+" "+billingDocObj.getStatus()+" "+"/"+" "+AppUtility.parseDate(billingDocObj.getCreationDate());
		}
		fgroupingCustomerInformation.getHeaderLabel().setText(mainScreenLabel);
	}
	
	
	public void changeStatusToCreated()
	{
		BillingDocument entity=(BillingDocument) presenter.getModel();
		String status=entity.getStatus();
		
		if(BillingDocument.REJECTED.equals(status.trim()))
		{
			this.tbstatus.setValue(BillingDocument.CREATED);
		}
		//DATE 14.7.2018 ADDED BY KOMAL
		if(billingDocObj.getTypeOfOrder().equalsIgnoreCase(AppConstants.ORDERTYPEPURCHASE) || billingDocObj.getTypeOfOrder().equalsIgnoreCase(AppConstants.ORDERTYPESALES)){
			salesProductTable.qtyHeader = "Qty";
			salesProductTable.setEnable(true);
		}
	}
	
	
	

	public void setMenus()
	{
		this.changeProcessLevel();
		this.setAppHeaderBarAsPerStatus();
	}
	
	@Override
	public TextBox getstatustextbox() {
		return this.tbstatus;
	}
	
	public void fillBillingDocumentTable()
	{
		
		
			System.out.println("Entering....");
//			if(flatDiscStatus()==true){
				Console.log("inside flat disc not zero condition");
			fillTotalBillingAmount();
//			}
			BillingDocumentDetails billingDocDetails=new BillingDocumentDetails();
			
			if(ibcontractid.getValue()!=null){
				billingDocDetails.setOrderId(Integer.parseInt(ibcontractid.getValue()));
			}
			
			if(ibbillingid.getValue()!=null)
				billingDocDetails.setBillId(Integer.parseInt(ibbillingid.getValue()));
			if(dbbillingdate.getValue()!=null)
				billingDocDetails.setBillingDate(dbbillingdate.getValue());
			if(dototalbillamt.getValue()!=null)
				billingDocDetails.setBillAmount(dototalbillamt.getValue());
			if(olbbillingcategory.getValue()!=null)
				billingDocDetails.setBillingCategory(olbbillingcategory.getValue());
			if(olbbillingtype.getValue()!=null)
				billingDocDetails.setBillingType(olbbillingtype.getValue());
			if(olbbillinggroup.getValue()!=null)
				billingDocDetails.setBillingGroup(olbbillinggroup.getValue());
			if(tbstatus.getValue()!=null)
				billingDocDetails.setBillstatus(tbstatus.getValue());
			billingDocumentTable.connectToLocal();		
			billingDocumentTable.getDataprovider().getList().add(billingDocDetails);
	}
	
	
	public boolean flatDiscStatus()
	{
		for(int i=0;i<this.getSalesProductTable().getDataprovider().getList().size();i++)
		{
			if(this.getSalesProductTable().getDataprovider().getList().get(i).getFlatDiscount()!=0)
			{
				return true;
			}
		}
		
		return false;
	}
	
	public void fillTotalBillingAmount()
	{
			
//			double payableamt=0;
//			payableamt=this.getSalesProductTable().calculateTotalBillAmt()+this.getBillingTaxTable().calculateTotalTaxes()+this.getBillingChargesTable().calculateTotalBillingCharges();
//			payableamt=Math.round(payableamt);
//			this.dototalbillamt.setValue(payableamt);

			/** date 07-09-2017 added by vijay for new logic deiscount amt round off calculation  above old commented**/
			double payableamt=0;
			if(this.getDbDiscountAmt().getValue()!=null){
				payableamt=this.getSalesProductTable().calculateTotalBillAmt()-this.getDbDiscountAmt().getValue();
			}else{
				payableamt=this.getSalesProductTable().calculateTotalBillAmt();
			}
			Console.log("payableamt "+payableamt);
			if(tacomment.getValue()!=null && !tacomment.equals("")){
				if(tacomment.getValue().contains("This data uploded through upload program") && dbtotalAmt.getValue()!=null){
					payableamt = dbtotalAmt.getValue();
				}
			}
			/**
			 * Date : 21-09-2017 BY ANIL
			 */
			if(dbOtherChargesTotal.getValue()!=null){
				payableamt =payableamt+dbOtherChargesTotal.getValue()+this.getBillingTaxTable().calculateTotalTaxes()+this.getBillingChargesTable().calculateTotalBillingCharges();
				
			}else{
				payableamt =payableamt+this.getBillingTaxTable().calculateTotalTaxes()+this.getBillingChargesTable().calculateTotalBillingCharges();
			}
			Console.log("payableamt ="+payableamt);

//			payableamt =payableamt+this.getBillingTaxTable().calculateTotalTaxes()+this.getBillingChargesTable().calculateTotalBillingCharges();
			if(this.getTbroundOffAmt().getValue()!=null && !this.getTbroundOffAmt().getValue().equals("")){
				payableamt =  AppUtility.calculateRoundOff(this.getTbroundOffAmt().getValue(),payableamt);
			}
			Console.log("payableamt =="+payableamt);
			if(LoginPresenter.PC_NO_ROUNDOFF_INVOICE_CONTRACTFlag){
				NumberFormat nf = NumberFormat.getFormat("0.00");
				payableamt = getLastTwoDecimalOnly(payableamt);
				this.dototalbillamt.setValue(payableamt);
			}
			else{
				payableamt=Math.round(payableamt);
				this.dototalbillamt.setValue(payableamt);
			}
			
	}
	
	

	public void retrieveTotalBillingAmt()
	{
		if(flag==true&&flag1==true)
		{
			this.fillTotalBillingAmount();
		}
	}
	
	public void retrieveBillingCharges(Long companyIdVal,final boolean flag,final List<ContractCharges> billCharges)
	{
		int orderId=0;
		
		final GenricServiceAsync async=GWT.create(GenricService.class);
		System.out.println("Company Id In Retrieval"+companyIdVal);
		System.out.println("Flag  "+flag);
		System.out.println("Size List "+billCharges.size());
		MyQuerry querry=new MyQuerry();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter=null;
		filter=new Filter();
		filter.setQuerryString("contractId");
		filter.setIntValue(Integer.parseInt(getIbcontractid().getValue()));
		filtervec.add(filter);
		filter=new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(companyIdVal);
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new TaxesAndCharges());
		async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {

			@Override
			public void onFailure(Throwable caught) {
				showDialogMessage("An Unexpected Error occured !");
			}
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				final ArrayList<ContractCharges> arrConCharges=new ArrayList<ContractCharges>();
				for(SuperModel bmodel:result)
				{
					TaxesAndCharges tncentity =(TaxesAndCharges)bmodel;
					List<ContractCharges> lisConCharges=tncentity.getTaxesChargesList();
					System.out.println(" List Size :: "+tncentity.getTaxesChargesList().size());
					for(int i=0;i<lisConCharges.size();i++){
						lisConCharges.get(i).setBillingId(Integer.parseInt(ibbillingid.getValue()));
					}
					
					arrConCharges.addAll(lisConCharges);
					System.out.println("arrConCharges List Size :: "+arrConCharges.size());
				}
				if(flag==true){
					List<ContractCharges> updatedbalanceLis=updateChargesBalance(arrConCharges,billCharges);
					ArrayList<ContractCharges> updatedBalanceArr=new ArrayList<ContractCharges>();
					updatedBalanceArr.addAll(updatedbalanceLis);
					getBillingChargesTable().setValue(updatedBalanceArr);
				}
				if(flag==false){
					getBillingChargesTable().setValue(arrConCharges);
				}
				
			}
		});
	}
	
	 public List<ContractCharges> updateChargesBalance(List<ContractCharges> chargeLis,List<ContractCharges> formChargeLis)
	 {
			ArrayList<ContractCharges> updatedBalArr=new ArrayList<ContractCharges>();
			for(int i=0;i<formChargeLis.size();i++)
			{
				 ContractCharges ccEntity=new ContractCharges();
				 
				 ccEntity.setOrderId(formChargeLis.get(i).getOrderId());
				 ccEntity.setBillingId(formChargeLis.get(i).getBillingId());
				 ccEntity.setTaxChargeName(formChargeLis.get(i).getTaxChargeName());
				 ccEntity.setTaxChargePercent(formChargeLis.get(i).getTaxChargePercent());
				 ccEntity.setTaxChargeAbsVal(formChargeLis.get(i).getTaxChargeAbsVal());
				 ccEntity.setTaxChargeAssesVal(formChargeLis.get(i).getTaxChargeAssesVal());
				 if(chargeLis.get(i).getChargesBalanceAmt()!=0){
					 System.out.println("INSIDE CHECKING CHARGES BALANCE......!!!");
					 ccEntity.setChargesBalanceAmt(chargeLis.get(i).getChargesBalanceAmt());
				 }else{
					 ccEntity.setChargesBalanceAmt(formChargeLis.get(i).getChargesBalanceAmt()); 
				 }
				 ccEntity.setPaypercent(formChargeLis.get(i).getPaypercent());
				 ccEntity.setPayableAmt(formChargeLis.get(i).getPayableAmt());
				 
				 updatedBalArr.add(ccEntity);
			}
			System.out.println("New...........");
			return updatedBalArr;
	 }
	
	/**
	 * This method is used for saving payment terms for current billing document.
	 * @return
	 */
	public List<PaymentTerms> savePaymentTerms()
	{
		ArrayList<PaymentTerms> arrPaymentTerms=new ArrayList<PaymentTerms>();
		PaymentTerms billPayTrms=new PaymentTerms();
		if(ibpaytermsdays.getValue()==0||ibpaytermsdays.getValue()!=0)
			billPayTrms.setPayTermDays(ibpaytermsdays.getValue());
		if(dopaytermspercent.getValue()!=0)
			billPayTrms.setPayTermPercent(dopaytermspercent.getValue());
		if(tbpaytermscomment.getValue()!=null)
			billPayTrms.setPayTermComment(tbpaytermscomment.getValue());
		
		arrPaymentTerms.add(billPayTrms);
		
		return arrPaymentTerms;
	}
	

	@Override
	public boolean validate() {
		boolean superRes=super.validate();
		boolean validateBillingAmt=true;
		boolean billDateVal=true;
		boolean invDateVal=true;
		boolean tableValidate=true;
		boolean taxValidate = true;
		
//		if(this.dototalbillamt.getValue()>this.dosalesamount.getValue())
//		{
//			validateBillingAmt=false;
//			this.showDialogMessage("Total Billing Amount cannot be greater than Total Sales Amount");
//		}
		
		if(this.getDbpaymentdate().getValue()!=null&&this.getDbinvoicedate().getValue()!=null)
		{
			Date billDate=this.getDbbillingdate().getValue();
			Date invDate=this.getDbinvoicedate().getValue();
			Date payDate=this.getDbpaymentdate().getValue();
			
			
			
			if(billDate.after(invDate))
			{
				showDialogMessage("Invoice Date should be greater than Billing Date!");
				billDateVal=false;
			}
			
			/**Added by sheetal:19-01-2022
			   Des:Commented below code for removing validation Payment Date should be 
			   greater than Billing Date and invoice date, requirement raised by Nitin sir**/
//			if(billDateVal==true&&billDate.after(payDate))
//			{
//				showDialogMessage("Payment Date should be greater than Billing Date!");
//				billDateVal=false;
//			}
			
//			if(billDateVal==true&&invDate.after(payDate))
//			{
//				showDialogMessage("Payment Date should be greater than Invoice Date!");
//				invDateVal=false;
//			}
			/**end**/
		}
		
		if(this.billingDocumentTable.getDataprovider().getList().size()==0)
		{
			showDialogMessage("Billing Document Table cannot be empty!");
			tableValidate=false;
		}
		
		 /**
		 * Date 03-07-2017 added by vijay tax validation
		 */
		List<ContractCharges> prodtaxlist = this.billingTaxTable.getDataprovider().getList();
		for(int i=0;i<prodtaxlist.size();i++){
			
			if(dbbillingdate.getValue()!=null && dbbillingdate.getValue().before(date)){
				if(prodtaxlist.get(i).getTaxChargeName().equalsIgnoreCase("CGST") || prodtaxlist.get(i).getTaxChargeName().equalsIgnoreCase("SGST")
						|| prodtaxlist.get(i).getTaxChargeName().equalsIgnoreCase("IGST")){
					showDialogMessage("As Billing date "+fmt.format(dbbillingdate.getValue())+" GST Tax is not applicable");
					taxValidate = false;
				}
			}
			/**
			 * @author Anil @since 21-07-2021
			 * Issue raised by Rahul for Innovative Pest(Thailand) 
			 * Vat is applicable there so system should not through validation for VAT
			 */
//			else if(dbbillingdate.getValue()!=null && dbbillingdate.getValue().equals(date) || dbbillingdate.getValue().after(date)){
//				if(prodtaxlist.get(i).getTaxChargeName().equalsIgnoreCase("CST") || prodtaxlist.get(i).getTaxChargeName().equalsIgnoreCase("Service Tax")
//						|| prodtaxlist.get(i).getTaxChargeName().equalsIgnoreCase("VAT")){
//					showDialogMessage("As Billing date "+fmt.format(dbbillingdate.getValue())+" VAT/CST/Service Tax is not applicable");
//					taxValidate = false;
//				}
//			}
		}
		/**
		 * ends here
		 */
		/**
		 *  nidhi
		 *  30-03-2018
		 *  for make unit and UOm is mandatory
		 */
		boolean unitFlag = validateunitandUOM();
		/**
		 * end
		 */
		
		
		/**
		 * Date 06-07-2018 By Vijay
		 * Des :- for billing date should not be greater than contract end date validation
		 * Requirement :- NBHC CCPM
		 */	
		
//		boolean billingDateflag= true;
//		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "OnlyForNBHC")){
//		if(this.getPresenter().getModel()!=null){
//			BillingDocument model = (BillingDocument) this.getPresenter().getModel();
//		if(model.getContractEndDate()!=null){
//			if(this.dbbillingdate.getValue().after(model.getContractEndDate())){
//				this.showDialogMessage("Billing Date should not be greater than contract end date!");
//				billingDateflag = false;
//			}
//		}
//		if(model.getContractStartDate()!=null){
//			if(this.dbbillingdate.getValue().before(model.getContractStartDate())){
//				this.showDialogMessage("Billing Date should not be less than contract start date!");
//				billingDateflag = false;
//			}
//		}
//		
//		}
//	  }   
		/**
		 * ends here
		 */
		boolean validateParcialAmt = checkPayableAmount();
		
		validateParcialAmt  = (validateParcialAmt && parcialBillDisableFlag) ? true : false;
		
		if(validateParcialAmt){
			if(olbCancellationRemark.getSelectedIndex()==0){
				showDialogMessage("Please select Cancel Amount Remark.");
				return false;
			}
		}
		/***16-12-2019 Deepak Salve added code for select customer branches option diseble for sales invoice***/ 
		BillingDocument currentObj = (BillingDocument) this.getPresenter().getModel();
		System.out.println("Model Value "+currentObj.getTypeOfOrder().equals(AppConstants.ORDERTYPEFORSERVICE));
		/** date 15.4.2019 added by komal for nbhc service branches button and popup **/
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "EnableServiceBranch") && (currentObj!=null && currentObj.getTypeOfOrder().equals(AppConstants.ORDERTYPEFORSERVICE))){
			if(!LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN")){
			NumberFormat nf=NumberFormat.getFormat("0.00");
			for(SalesOrderProductLineItem item:salesProductTable.getDataprovider().getList()){
			double totalAmount = 0;
			if(item.getServiceBranchesInfo()!=null && item.getServiceBranchesInfo().size()>0){
			ArrayList<BranchWiseScheduling> list = item.getServiceBranchesInfo().get(item.getProductSrNumber());
			if(list != null && list.size() > 0){
			 for(BranchWiseScheduling branchWiseScheduling : list){
				if(branchWiseScheduling.isCheck()){
					totalAmount += branchWiseScheduling.getArea();
				}
			 }
			}
//			if(totalAmount !=  item.getBasePaymentAmount()){
//				showDialogMessage("Service branches total amount should be equal to product payable amount.");
//				return false;
//			}
		 }
			
			if(totalAmount !=  Double.parseDouble(nf.format(item.getBasePaymentAmount()))){
				showDialogMessage("Service branches total amount should be equal to product payable amount.");
				return false;
			}	
		}
	   }
	  }
		/**
		 * end komal
		 */
		return superRes&&validateBillingAmt&&billDateVal&&invDateVal&&tableValidate&&taxValidate&&unitFlag;
	}
	

	public  boolean checkPayableAmount()
	{
		double baseBilling=0,payableAmt=0;
		List<SalesOrderProductLineItem> prodlist = this.salesProductTable.getDataprovider().getList();
		for(int i=0;i<prodlist.size();i++)
		{
			baseBilling=baseBilling+prodlist.get(i).getBaseBillingAmount() - prodlist.get(i).getFlatDiscount();
			payableAmt=payableAmt+((prodlist.get(i).getBaseBillingAmount()- prodlist.get(i).getFlatDiscount())*prodlist.get(i).getPaymentPercent())/100;
		}
		if(payableAmt<baseBilling)
		{
			dbCancelAmt.setValue(baseBilling - payableAmt);
			return true;
		}
		return false;
	}
	
	public void setOneToManyCombo()
	{
		CategoryTypeFactory.initiateOneManyFunctionality(this.olbbillingcategory,this.olbbillingtype);
		AppUtility.makeTypeListBoxLive(this.olbbillingtype,Screen.BILLINGCATEGORY);
	} 
	
	
	/**************************************Type Drop Down Logic****************************************/

	@Override
	public void onChange(ChangeEvent event) {
		
		if(event.getSource().equals(olbbillingcategory)){
			
			if(olbbillingcategory.getSelectedIndex()!=0){
				
				ConfigCategory cat=olbbillingcategory.getSelectedItem();
				if(cat!=null){
					AppUtility.makeLiveTypeDropDown(olbbillingtype, cat.getCategoryName(), cat.getCategoryCode(), cat.getInternalType());
					
				}
			}
		}
		
		/**
	     * Date 03-09-2017 added by vijay for final total amount and net payable after discount
	     */
		
		if(event.getSource().equals(dbDiscountAmt)){
			System.out.println("on change amt ==");

			if(this.getDbDiscountAmt().getValue()==null){
				this.getDbFinalTotalAmt().setValue(this.getDbtotalAmt().getValue());
				
				this.billingTaxTable.connectToLocal();
				try {
					this.addProdTaxes();
					/**
					 * Date : 21-09-2017 BY ANIL
					 */
					this.addOtherChargesInTaxTbl();
					dbOtherChargesTotal.setValue(tblOtherCharges.calculateOtherChargesSum());
					/**
					 * End
					 */
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				double totalIncludingTax=this.getDbFinalTotalAmt().getValue()+this.getBillingTaxTable().calculateTotalTaxes();
				/**
				 * Date : 22-09-2017 BY  ANIL
				 */
				if(dbOtherChargesTotal.getValue()!=null){
					totalIncludingTax=totalIncludingTax+dbOtherChargesTotal.getValue();
				}
				/**
				 * END
				 */
				double netPay = Double.parseDouble(nf.format(totalIncludingTax));
				netPay = Math.round(netPay);
				
				System.out.println("NET Payable ==="+netPay);
				
				
				this.getDbtotalAmtIncludingTax().setValue(totalIncludingTax);
				
				if(LoginPresenter.PC_NO_ROUNDOFF_INVOICE_CONTRACTFlag){
					//this is for other charges
					netPay = netPay + this.getBillingChargesTable().calculateNetPayable();
				}
				else{
					netPay = Math.round(netPay + this.getBillingChargesTable().calculateNetPayable());
				}

				
				this.getDbGrandTotalAmt().setValue(netPay);

				if(!tbroundOffAmt.getValue().equals("") && tbroundOffAmt.getValue()!=null){
					String roundOff = tbroundOffAmt.getValue();
					double roundoffAmt =  AppUtility.calculateRoundOff(roundOff, netPay);
					System.out.println("roundoffAmt"+roundoffAmt);
					if(roundoffAmt!=0){
						this.getDototalbillamt().setValue(roundoffAmt);
					}else{
						this.getDototalbillamt().setValue(netPay);
					}
				}
				else{
					this.getDototalbillamt().setValue(netPay);
				}


			}
			else if(this.getDbDiscountAmt().getValue() > this.getDbtotalAmt().getValue()){
				showDialogMessage("Discount Amount can not be greater than total amount");
				this.getDbDiscountAmt().setValue(0d);
			}else{
				reactOnDiscountAmtChange();
			}
			
		}
		/**
		 * ends here
		 */
		
		/**
		 * Date 03-09-2017 added by vijay for roundoff and managing net payable
		 */
		if(event.getSource().equals(tbroundOffAmt)){

			double totalIncludingTax=this.getDbFinalTotalAmt().getValue()+this.getBillingTaxTable().calculateTotalTaxes();
			/**
			 * Date : 10-10-2017 BY ANIL
			 */
			if(dbOtherChargesTotal.getValue()!=null){
				totalIncludingTax=totalIncludingTax+dbOtherChargesTotal.getValue();
			}
			/**
			 * end
			 */
			double netPay = Double.parseDouble(nf.format(totalIncludingTax));
			
			
			if(LoginPresenter.PC_NO_ROUNDOFF_INVOICE_CONTRACTFlag){
				//this is for if there in any other charges
				netPay =netPay + this.getBillingChargesTable().calculateNetPayable();
			}
			else{
				//this is for if there in any other charges
				netPay = Math.round(netPay + this.getBillingChargesTable().calculateNetPayable());
			}
			
			if(tbroundOffAmt.getValue().equals("") || tbroundOffAmt.getValue()==null){
				this.getDototalbillamt().setValue(netPay);
			}else{

				if(!tbroundOffAmt.getValue().equals("")&& tbroundOffAmt.getValue()!=null){
					String roundOff = tbroundOffAmt.getValue();
					double roundoffAmt =  AppUtility.calculateRoundOff(roundOff, netPay);
					System.out.println("roundoffAmt"+roundoffAmt);
					if(roundoffAmt!=0){
						this.getDototalbillamt().setValue(roundoffAmt);
					}else{
						this.getDototalbillamt().setValue(netPay);
						this.getTbroundOffAmt().setValue("");
					}
				}
			}
		}
		/**
		 * ends here
		 */
		
		if(event.getSource().equals(olbbranch)){
			getcustomerBillingAddress();
		}
	}
	
	
	

	

	/**
     * Date 03-09-2017 added by vijay for final total amount and net payable after discount
     */
	
	private void reactOnDiscountAmtChange() {
				showWaitSymbol();
				NumberFormat nf=NumberFormat.getFormat("0.00");
				double discountAmt = this.getDbDiscountAmt().getValue();
				double finalAmt = this.getDbtotalAmt().getValue()-discountAmt;
			    this.getDbFinalTotalAmt().setValue(finalAmt);
			    
				try {
					this.billingTaxTable.connectToLocal();
					this.addProdTaxes();
					/**
					 * Date : 21-09-2017 BY ANIL
					 */
					this.addOtherChargesInTaxTbl();
					dbOtherChargesTotal.setValue(tblOtherCharges.calculateOtherChargesSum());
					/**
					 * End
					 */
					double totalIncludingTax=this.getDbFinalTotalAmt().getValue()+this.getBillingTaxTable().calculateTotalTaxes();
					/**
					 * Date : 22-09-2017 BY  ANIL
					 */
					if(dbOtherChargesTotal.getValue()!=null){
						totalIncludingTax=totalIncludingTax+dbOtherChargesTotal.getValue();
					}
					/**
					 * END
					 */
					double netPay = Double.parseDouble(nf.format(totalIncludingTax));
					netPay = Math.round(netPay);
					
					this.getDbtotalAmtIncludingTax().setValue(totalIncludingTax);
					
					if(LoginPresenter.PC_NO_ROUNDOFF_INVOICE_CONTRACTFlag){
						//this is for other charges
						netPay = netPay + this.getBillingChargesTable().calculateNetPayable();
					}
					else{
						//this is for other charges
						netPay = Math.round(netPay + this.getBillingChargesTable().calculateNetPayable());
					}
					this.getDbGrandTotalAmt().setValue(netPay);

					if(!tbroundOffAmt.getValue().equals("") && tbroundOffAmt.getValue()!=null){
						String roundOff = tbroundOffAmt.getValue();
						double roundoffAmt =  AppUtility.calculateRoundOff(roundOff, netPay);
						System.out.println("roundoffAmt"+roundoffAmt);
						if(roundoffAmt!=0){
							this.getDototalbillamt().setValue(roundoffAmt);
						}else{
							this.getDototalbillamt().setValue(netPay);
							this.getTbroundOffAmt().setValue("");
						}
					}
					else{
						this.getDototalbillamt().setValue(netPay);

					}
					
				} catch (Exception e) {
					e.printStackTrace();
				}
				this.hideWaitSymbol();
	}

	/**
	 * ends here
	 */
	

	public void calculateNetPayable(){
		double amtval=0;
		
		if(getBillingChargesTable().getDataprovider().getList().size()!=0)
		{
			amtval=getDototalbillamt().getValue();
			amtval=amtval+getBillingChargesTable().calculateTotalBillingCharges();
		}
		
		if(!LoginPresenter.PC_NO_ROUNDOFF_INVOICE_CONTRACTFlag){
			amtval=Math.round(amtval);
			int retAmtVal=(int) amtval;
			amtval=retAmtVal;
		}
		getDototalbillamt().setValue(amtval);;
		
	}
	
	/**
	 * Date :23-09-2017 BY ANIL
	 * This method checks whether we should apply discount or not
	 * if taxes applied on all products then we can give discount
	 */
	public boolean isValidForDiscount(){
		String tax1="",tax2="";
		if(salesProductTable.getDataprovider().getList().size()==0){
			return false;
		}
		tax1=salesProductTable.getDataprovider().getList().get(0).getVatTax().getTaxConfigName().trim();
		tax2=salesProductTable.getDataprovider().getList().get(0).getServiceTax().getTaxConfigName().trim();
		for(SalesOrderProductLineItem obj:salesProductTable.getDataprovider().getList()){
			if(!obj.getVatTax().getTaxConfigName().trim().equals(tax1)
					||!obj.getServiceTax().getTaxConfigName().trim().equals(tax2)){
				showDialogMessage("Discount is applicable only if same taxes are applied to all products.");
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * End
	 */
	
/*****************************************Add Product Taxes Method***********************************/
	
	/**
	 * This method is called when a product is added to SalesLineItem table.
	 * Taxes related to corresponding products will be added in ProductTaxesTable.
	 * @throws Exception
	 */
	public void addProdTaxes() throws Exception
	{
		System.out.println();
		System.out.println("INSIDE addProdTaxes() METHOD.....");
		List<SalesOrderProductLineItem> salesLineItemLis=this.salesProductTable.getDataprovider().getList();
		
		System.out.println("PRODUCT TABLE SIZE "+salesLineItemLis.size());
		List<ContractCharges> taxList=this.billingTaxTable.getDataprovider().getList();
		System.out.println("PRODUCT TAX TABLE SIZE "+taxList.size());
		
		NumberFormat nf=NumberFormat.getFormat("#.00");
		for(int i=0;i<salesLineItemLis.size();i++)
		{
			
			
			double priceqty=0,taxPrice=0;
//			if(salesLineItemLis.get(i).getProdPercDiscount()==null){
//				taxPrice=this.salesProductTable.removeAllTaxes(salesLineItemLis.get(i).getPrduct(),salesLineItemLis.get(i).getBaseBillingAmount());
//				priceqty=(salesLineItemLis.get(i).getBaseBillingAmount()-taxPrice)*salesLineItemLis.get(i).getQuantity();
//				priceqty=Double.parseDouble(nf.format(priceqty));
//			}
//			if(salesLineItemLis.get(i).getProdPercDiscount()!=null){
//				taxPrice=this.salesProductTable.removeAllTaxes(salesLineItemLis.get(i).getPrduct(),salesLineItemLis.get(i).getBaseBillingAmount());
//				priceqty=(salesLineItemLis.get(i).getBaseBillingAmount()-taxPrice);
//				priceqty=priceqty-(priceqty*salesLineItemLis.get(i).getProdPercDiscount()/100);
//				priceqty=priceqty*salesLineItemLis.get(i).getQuantity();
//				priceqty=Double.parseDouble(nf.format(priceqty));
//			}
			
			if(salesLineItemLis.get(i).getFlatDiscount()!=0)
			{
				/**
				 * Date : 31-07-2017 by ANIL
				 * Removed below line from each conditional statement and commented
				 */
//				taxPrice=this.salesProductTable.removeAllTaxes(salesLineItemLis.get(i).getPrduct(),salesLineItemLis.get(i).getBaseBillingAmount());
				priceqty=(salesLineItemLis.get(i).getBaseBillingAmount()-taxPrice)-salesLineItemLis.get(i).getFlatDiscount();
				
				//  rohan commented this code for NBHC  on Date : 7/2/2016
//				priceqty=priceqty*salesLineItemLis.get(i).getQuantity();
				
				/**
				 * rohan added this code for billing %  
				 * Date : 10/10/2016
				 */
				
				priceqty =  priceqty * salesLineItemLis.get(i).getPaymentPercent()/100;
				/**
				 * ends here 
				 */
				
				System.out.println("00000000000000000"+priceqty);
			}
			
			else
			{
			
			//****************************************************************************
			if((salesLineItemLis.get(i).getProdPercDiscount()==null && salesLineItemLis.get(i).getProdPercDiscount()==0) && (salesLineItemLis.get(i).getDiscountAmt()==0)){
				
//				System.out.println("inside both 0 condition");
//				
//				priceVal=entity.getBaseBillingAmount()-taxAmt;
//				priceVal=priceVal*entity.getQuantity();
//				sum=sum+priceVal;
//				System.out.println("RRRRRRRRRRRRRR sum"+sum);
	
//					taxPrice=this.salesProductTable.removeAllTaxes(salesLineItemLis.get(i).getPrduct(),salesLineItemLis.get(i).getBaseBillingAmount());
					
					//  rohan commented this code for NBHC  on Date : 7/2/2016
//					priceqty=(salesLineItemLis.get(i).getBaseBillingAmount()-taxPrice)*salesLineItemLis.get(i).getQuantity();
					priceqty=(salesLineItemLis.get(i).getBaseBillingAmount()-taxPrice);
							//   ends here 
					/**
					 * rohan added this code for billing %  
					 * Date : 10/10/2016
					 */
					
						priceqty =  priceqty * salesLineItemLis.get(i).getPaymentPercent()/100;
					/**
					 * ends here 
					 */
					
					priceqty=Double.parseDouble(nf.format(priceqty));
					
					System.out.println("111111111"+priceqty);
			}
			
			else if((salesLineItemLis.get(i).getProdPercDiscount()!=null && salesLineItemLis.get(i).getProdPercDiscount()!=0)&& (salesLineItemLis.get(i).getDiscountAmt()!=0)){
				
//				taxPrice=this.salesProductTable.removeAllTaxes(salesLineItemLis.get(i).getPrduct(),salesLineItemLis.get(i).getBaseBillingAmount());
				System.out.println("inside both not null condition");
//				
				priceqty=salesLineItemLis.get(i).getBaseBillingAmount()-taxPrice;
				
//				priceVal=priceVal-(priceVal*entity.getProdPercDiscount()/100);
//				priceVal=priceVal-entity.getDiscountAmt();
				
				//  rohan commented this code for NBHC  on Date : 7/2/2016
//				priceqty=priceqty*salesLineItemLis.get(i).getQuantity();
				//   ends here 
				
//				sum=sum+priceVal;
//				System.out.println("RRRRRRRRRRRRRR sum"+sum);
				
				
//				taxPrice=this.salesProductTable.removeAllTaxes(salesLineItemLis.get(i).getPrduct(),salesLineItemLis.get(i).getBaseBillingAmount());
//				priceqty=(salesLineItemLis.get(i).getBaseBillingAmount()-taxPrice);
////				priceqty=priceqty-(priceqty*salesLineItemLis.get(i).getProdPercDiscount()/100);
////				priceqty=priceqty-salesLineItemLis.get(i).getDiscountAmt();
//				priceqty=priceqty*salesLineItemLis.get(i).getQuantity();
				
				/**
				 * rohan added this code for billing %  
				 * Date : 10/10/2016
				 */
				
				priceqty =  priceqty * salesLineItemLis.get(i).getPaymentPercent()/100;
				/**
				 * ends here 
				 */
				priceqty=Double.parseDouble(nf.format(priceqty));
				System.out.println("111111111"+priceqty);
			}
			else 
			{
				System.out.println("inside oneof the null condition");
				
				
//				taxPrice=this.salesProductTable.removeAllTaxes(salesLineItemLis.get(i).getPrduct(),salesLineItemLis.get(i).getBaseBillingAmount());
			if(salesLineItemLis.get(i).getProdPercDiscount()!=null && salesLineItemLis.get(i).getProdPercDiscount()!=0){	
				priceqty=(salesLineItemLis.get(i).getBaseBillingAmount()-taxPrice);
//				priceqty=priceqty-(priceqty*salesLineItemLis.get(i).getProdPercDiscount()/100);
				
				/**
				 * rohan added this code for billing %  
				 * Date : 10/10/2016
				 */
				
				priceqty =  priceqty * salesLineItemLis.get(i).getPaymentPercent()/100;
				/**
				 * ends here 
				 */
				
				//  rohan commented this code for NBHC  on Date : 7/2/2016
//				priceqty=priceqty*salesLineItemLis.get(i).getQuantity();
				//   ends here 
				priceqty=Double.parseDouble(nf.format(priceqty));
				
			}
			else 
			{
				priceqty=(salesLineItemLis.get(i).getBaseBillingAmount()-taxPrice);
//				priceqty=priceqty-salesLineItemLis.get(i).getDiscountAmt();
				
				//  rohan commented this code for NBHC  on Date : 7/2/2016
//				priceqty=priceqty*salesLineItemLis.get(i).getQuantity();
				//  ends here 
				
				/**
				 * rohan added this code for billing %  
				 * Date : 10/10/2016
				 */
				
				priceqty =  priceqty * salesLineItemLis.get(i).getPaymentPercent()/100;
				/**
				 * ends here 
				 */
				priceqty=Double.parseDouble(nf.format(priceqty));
			}
//					priceVal=entity.getBaseBillingAmount()-taxAmt;
//					if(entity.getProdPercDiscount()!=null && entity.getProdPercDiscount()!=0){
//						System.out.println("inside getPercentageDiscount oneof the null condition");
//					priceVal=priceVal-(priceVal*entity.getProdPercDiscount()/100);
//					}
//					else 
//					{
//						System.out.println("inside getDiscountAmt oneof the null condition");
//					priceVal=priceVal-entity.getDiscountAmt();
//					System.out.println("price value"+priceVal);
//					}
//						//  rohan commented this code for NBHC  on Date : 7/2/2016
//					priceqty=priceqty*salesLineItemLis.get(i).getQuantity();
					//  ends here 
					
					
//					System.out.println("sum value "+sum);
//					System.out.println("price out side for loop  value"+priceVal);
//					sum=sum+priceVal;
//					System.out.println("RRRRRRRRRRRRRR sum"+sum);
			}

				System.out.println("2222222222222222222222222222222"+priceqty);

			}
			
			
			
			
			
			/**
			 * Date 03-07-2017 below code commented by vijay 
			 * for this below code by default showing vat and service tax percent 0 no nedd this
			 */
			//***************************************************************************
			
//			if(salesLineItemLis.get(i).getVatTax().getPercentage()==0){
//				
//				ContractCharges pocentity=new ContractCharges();
//				pocentity.setTaxChargeName("VAT");
//				pocentity.setTaxChargePercent(salesLineItemLis.get(i).getVatTax().getPercentage());
//				pocentity.setIdentifyTaxCharge(i+1);
//				
//				int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getVatTax().getPercentage(),"VAT");
//				
//				if(indexValue!=-1){
//					pocentity.setTaxChargeAssesVal(0);
//					this.billingTaxTable.getDataprovider().getList().remove(indexValue);
//				}
//				
//				if(indexValue==-1){
//					pocentity.setTaxChargeAssesVal(0);
//				}
//				this.billingTaxTable.getDataprovider().getList().add(pocentity);
//				
//			}
//			
//			if(salesLineItemLis.get(i).getServiceTax().getPercentage()==0){
//				ContractCharges pocentity=new ContractCharges();
//				pocentity.setTaxChargeName("Service Tax");
//				pocentity.setTaxChargePercent(salesLineItemLis.get(i).getServiceTax().getPercentage());
//				pocentity.setIdentifyTaxCharge(i+1);
//				int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getServiceTax().getPercentage(),"Service");
//				if(indexValue!=-1){
//					pocentity.setTaxChargeAssesVal(0);
//					this.billingTaxTable.getDataprovider().getList().remove(indexValue);
//				}
//				
//				if(indexValue==-1){
//					pocentity.setTaxChargeAssesVal(0);
//				}
//				this.billingTaxTable.getDataprovider().getList().add(pocentity);
//			}

			/**
			 * ends here
			 */
			
			/**
		     * Date 03-09-2017 added by vijay for if discount amount is there then taxes calculation based on after
		     * discount Total amount
		     */
			if(dbDiscountAmt.getValue()!=null && dbDiscountAmt.getValue()!=0){
				/**
				 * Old Code
				 */
//				System.out.println("hi vijay inside discount amount");
//				priceqty = dbFinalTotalAmt.getValue();
				
				/**
				 * Date : 23-09-2017 BY ANIL
				 * 
				 */
				if(isValidForDiscount()){
					priceqty = dbFinalTotalAmt.getValue()/salesProductTable.getDataprovider().getList().size();
					System.out.println("hi vijay inside discount amount "+priceqty);
				}else{
					dbDiscountAmt.setValue(0.0);
					dbFinalTotalAmt.setValue(dbtotalAmt.getValue());
				}
			}
			
			/**
			 * ends here
			 */
			
		
			if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
				//  rohan added this conditions
				if(salesLineItemLis.get(i).getVatTax().getTaxPrintName()!=null && !salesLineItemLis.get(i).getVatTax().getTaxPrintName().equals(""))
				{
					System.out.println("Inside vat GST");
					ContractCharges pocentity=new ContractCharges();
					//   old code commented by rohan on date : 22-6-2017
	//				pocentity.setChargeName("VAT");
					//   new code by rohan Date 22-6-2017 
					System.out.println("salesLineItemLis.get(i).getVatTax().getTaxName()"+salesLineItemLis.get(i).getVatTax().getTaxPrintName());
					pocentity.setTaxChargeName(salesLineItemLis.get(i).getVatTax().getTaxPrintName());
					pocentity.setTaxChargePercent(salesLineItemLis.get(i).getVatTax().getPercentage());
					pocentity.setIdentifyTaxCharge(i+1);
					int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getVatTax().getPercentage(),salesLineItemLis.get(i).getVatTax().getTaxPrintName());
					
					if(indexValue!=-1){
						System.out.println("Index As Not -1");
						pocentity.setTaxChargeAssesVal(priceqty+taxList.get(indexValue).getTaxChargeAssesVal());
//						pocentity.setTaxChargeAssesVal(priceqty);
						this.billingTaxTable.getDataprovider().getList().remove(indexValue);
					}
					if(indexValue==-1){
						pocentity.setTaxChargeAssesVal(priceqty);
					}
					this.billingTaxTable.getDataprovider().getList().add(pocentity);
				}
				else
				{
					System.out.println("Vattx");
					ContractCharges pocentity=new ContractCharges();
					pocentity.setTaxChargeName("VAT");
					pocentity.setTaxChargePercent(salesLineItemLis.get(i).getVatTax().getPercentage());
					pocentity.setIdentifyTaxCharge(i+1);
					int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getVatTax().getPercentage(),"VAT");
					
					if(indexValue!=-1){
						System.out.println("Index As Not -1");
						
						pocentity.setTaxChargeAssesVal(priceqty+taxList.get(indexValue).getTaxChargeAssesVal());
						this.billingTaxTable.getDataprovider().getList().remove(indexValue);
					}
					if(indexValue==-1){
						pocentity.setTaxChargeAssesVal(priceqty);
					}
					this.billingTaxTable.getDataprovider().getList().add(pocentity);
				}
			}
			
			if(salesLineItemLis.get(i).getServiceTax().getPercentage()!=0){
			//  rohan added this conditions
			if(salesLineItemLis.get(i).getServiceTax().getTaxPrintName()!=null && !salesLineItemLis.get(i).getServiceTax().getTaxPrintName().equals(""))
			{
				System.out.println("Inside vat GST");
				ContractCharges pocentity=new ContractCharges();
				//   old code commented by rohan on date : 22-6-2017
//				pocentity.setChargeName("VAT");
				//   new code by rohan Date 22-6-2017 
				System.out.println("salesLineItemLis.get(i).getVatTax().getTaxName()"+salesLineItemLis.get(i).getServiceTax().getTaxPrintName());
				pocentity.setTaxChargeName(salesLineItemLis.get(i).getServiceTax().getTaxPrintName());
				pocentity.setTaxChargePercent(salesLineItemLis.get(i).getServiceTax().getPercentage());
				pocentity.setIdentifyTaxCharge(i+1);
				int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getServiceTax().getPercentage(),salesLineItemLis.get(i).getServiceTax().getTaxPrintName());
				
				if(indexValue!=-1){
					System.out.println("Index As Not -1");
					pocentity.setTaxChargeAssesVal(priceqty+taxList.get(indexValue).getTaxChargeAssesVal());
//					pocentity.setTaxChargeAssesVal(priceqty);
					this.billingTaxTable.getDataprovider().getList().remove(indexValue);
				}
				if(indexValue==-1){
					pocentity.setTaxChargeAssesVal(priceqty);
				}
				this.billingTaxTable.getDataprovider().getList().add(pocentity);
			}
			else
			{
				System.out.println("ServiceTx");
				ContractCharges pocentity=new ContractCharges();
				pocentity.setTaxChargeName("Service Tax");
				pocentity.setTaxChargePercent(salesLineItemLis.get(i).getServiceTax().getPercentage());
				pocentity.setIdentifyTaxCharge(i+1);
				int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getServiceTax().getPercentage(),"Service");
				if(indexValue!=-1){
					System.out.println("Index As Not -1");
					double assessValue=0;
					if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
						assessValue=priceqty+(priceqty*salesLineItemLis.get(i).getVatTax().getPercentage()/100);
						pocentity.setTaxChargeAssesVal(assessValue+taxList.get(indexValue).getTaxChargeAssesVal());
						this.billingTaxTable.getDataprovider().getList().remove(indexValue);
					}
					if(salesLineItemLis.get(i).getVatTax().getPercentage()==0){
						pocentity.setTaxChargeAssesVal(priceqty+taxList.get(indexValue).getTaxChargeAssesVal());
						this.billingTaxTable.getDataprovider().getList().remove(indexValue);
					}
					
				}
				if(indexValue==-1){
					System.out.println("Index As -1");
					double assessVal=0;
					if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
						assessVal=priceqty+(priceqty*salesLineItemLis.get(i).getVatTax().getPercentage()/100);
						pocentity.setTaxChargeAssesVal(assessVal);
					}
					if(salesLineItemLis.get(i).getVatTax().getPercentage()==0){
						pocentity.setTaxChargeAssesVal(priceqty);
					}
					
				}
				this.billingTaxTable.getDataprovider().getList().add(pocentity);
			}
			}
		}
		
	}
	
	
	/*********************************Check Tax Percent*****************************************/
	/**
	 * This method returns 
	 * @param taxValue
	 * @param taxName
	 * @return
	 */
	
	public int checkTaxPercent(double taxValue,String taxName)
	{
		List<ContractCharges> taxesList=this.billingTaxTable.getDataprovider().getList();
		for(int i=0;i<taxesList.size();i++)
		{
			double listval=taxesList.get(i).getTaxChargePercent();
			int taxval=(int)listval;
			
			if(taxName.equals("Service")){
				if(taxValue==taxesList.get(i).getTaxChargePercent()&&taxval!=0&&taxesList.get(i).getTaxChargeName().trim().equals("Service Tax")){
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getTaxChargeName().trim().equals("Service Tax")){
					return i;
				}
			}
			if(taxName.equals("VAT")){
				if(taxValue==taxesList.get(i).getTaxChargePercent()&&taxval!=0&&taxesList.get(i).getTaxChargeName().trim().equals("VAT")){
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getTaxChargeName().trim().equals("VAT")){
					return i;
				}
			}
			
		//  rohan added this for GST
					if(taxName.equals("CGST")){
						if(taxValue==taxesList.get(i).getTaxChargePercent()&&taxval!=0&&taxesList.get(i).getTaxChargeName().trim().equals("CGST")){
							return i;
						}
						if(taxval==0&&taxValue==0&&taxesList.get(i).getTaxChargeName().trim().equals("CGST")){
							return i;
						}
					}
					
				//  rohan added this for GST
					if(taxName.equals("SGST")){
						if(taxValue==taxesList.get(i).getTaxChargePercent()&&taxval!=0&&taxesList.get(i).getTaxChargeName().trim().equals("SGST")){
							return i;
						}
						if(taxval==0&&taxValue==0&&taxesList.get(i).getTaxChargeName().trim().equals("SGST")){
							return i;
						}
					}
					
				//  rohan added this for GST
					if(taxName.equals("IGST")){
						if(taxValue==taxesList.get(i).getTaxChargePercent()&&taxval!=0&&taxesList.get(i).getTaxChargeName().trim().equals("IGST")){
							return i;
						}
						if(taxval==0&&taxValue==0&&taxesList.get(i).getTaxChargeName().trim().equals("IGST")){
							return i;
						}
					}
		}
		return -1;
	}
	
	
	
//	public void calculateNetPayable(){
//		double amtval=0;
//		
//		if(getBillingChargesTable().getDataprovider().getList().size()!=0)
//		{
//			amtval=getDototalbillamt().getValue();
//			amtval=amtval+getBillingChargesTable().calculateTotalBillingCharges();
//		}
//		amtval=Math.round(amtval);
//		int retAmtVal=(int) amtval;
//		amtval=retAmtVal;
//		
//		getDototalbillamt().setValue(amtval);;
//		
//	}
	
	
	
	
	
	/************************Getters And Setters************************************/
	

	
	public TextBox getIbbillingid() {
		return ibbillingid;
	}

	public void setIbbillingid(TextBox ibbillingid) {
		this.ibbillingid = ibbillingid;
	}
	
	
	public DateBox getDbstartDate() {
		return dbstartDate;
	}

	public void setDbstartDate(DateBox dbstartDate) {
		this.dbstartDate = dbstartDate;
	}

	public DateBox getDbendDate() {
		return dbendDate;
	}

	public void setDbendDate(DateBox dbendDate) {
		this.dbendDate = dbendDate;
	}

	public DateBox getDbbillingdate() {
		return dbbillingdate;
	}

	public void setDbbillingdate(DateBox dbbillingdate) {
		this.dbbillingdate = dbbillingdate;
	}

	public DateBox getDbcreationdate() {
		return dbcreationdate;
	}

	public void setDbcreationdate(DateBox dbcreationdate) {
		this.dbcreationdate = dbcreationdate;
	}

	public ObjectListBox<ConfigCategory> getOlbbillingcategory() {
		return olbbillingcategory;
	}

	public void setOlbbillingcategory(
			ObjectListBox<ConfigCategory> olbbillingcategory) {
		this.olbbillingcategory = olbbillingcategory;
	}

	public ObjectListBox<Type> getOlbbillingtype() {
		return olbbillingtype;
	}

	public void setOlbbillingtype(ObjectListBox<Type> olbbillingtype) {
		this.olbbillingtype = olbbillingtype;
	}

	public ObjectListBox<Config> getOlbbillinggroup() {
		return olbbillinggroup;
	}

	public void setOlbbillinggroup(ObjectListBox<Config> olbbillinggroup) {
		this.olbbillinggroup = olbbillinggroup;
	}

	
	public DoubleBox getDosalesamount() {
		return dosalesamount;
	}

	public void setDosalesamount(DoubleBox dosalesamount) {
		this.dosalesamount = dosalesamount;
	}

	public ObjectListBox<Branch> getOlbbranch() {
		return olbbranch;
	}

	public void setOlbbranch(ObjectListBox<Branch> olbbranch) {
		this.olbbranch = olbbranch;
	}

	public ObjectListBox<Employee> getOlbEmployee() {
		return olbEmployee;
	}

	public void setOlbEmployee(ObjectListBox<Employee> olbEmployee) {
		this.olbEmployee = olbEmployee;
	}

	public ObjectListBox<Employee> getOlbApproverName() {
		return olbApproverName;
	}

	public void setOlbApproverName(ObjectListBox<Employee> olbApproverName) {
		this.olbApproverName = olbApproverName;
	}

	public TextArea getTacomment() {
		return tacomment;
	}

	public void setTacomment(TextArea tacomment) {
		this.tacomment = tacomment;
	}
	
	public TextBox getTbstatus() {
		return tbstatus;
	}

	public void setTbstatus(TextBox tbstatus) {
		this.tbstatus = tbstatus;
	}

	public BillingDocumentTable getBillingDocumentTable() {
		return billingDocumentTable;
	}

	public void setBillingDocumentTable(BillingDocumentTable billingDocumentTable) {
		this.billingDocumentTable = billingDocumentTable;
	}

	public DoubleBox getDototalbillamt() {
		return dototalbillamt;
	}

	public void setDototalbillamt(DoubleBox dototalbillamt) {
		this.dototalbillamt = dototalbillamt;
	}

	public TextBox getIbcontractid() {
		return ibcontractid;
	}

	public void setIbcontractid(TextBox ibcontractid) {
		this.ibcontractid = ibcontractid;
	}

	public TextBox getIbinvoiceid() {
		return ibinvoiceid;
	}

	public void setIbinvoiceid(TextBox ibinvoiceid) {
		this.ibinvoiceid = ibinvoiceid;
	}

	public DateBox getDbinvoicedate() {
		return dbinvoicedate;
	}

	public void setDbinvoicedate(DateBox dbinvoicedate) {
		this.dbinvoicedate = dbinvoicedate;
	}

	public DateBox getDbpaymentdate() {
		return dbpaymentdate;
	}

	public void setDbpaymentdate(DateBox dbpaymentdate) {
		this.dbpaymentdate = dbpaymentdate;
	}

	public SalesOrderProductTable getSalesProductTable() {
		return salesProductTable;
	}

	public void setSalesProductTable(SalesOrderProductTable salesProductTable) {
		this.salesProductTable = salesProductTable;
	}

	public IntegerBox getIbpaytermsdays() {
		return ibpaytermsdays;
	}

	public void setIbpaytermsdays(IntegerBox ibpaytermsdays) {
		this.ibpaytermsdays = ibpaytermsdays;
	}

	public TextBox getTbpaytermscomment() {
		return tbpaytermscomment;
	}

	public void setTbpaytermscomment(TextBox tbpaytermscomment) {
		this.tbpaytermscomment = tbpaytermscomment;
	}

	public DoubleBox getDopaytermspercent() {
		return dopaytermspercent;
	}

	public void setDopaytermspercent(DoubleBox dopaytermspercent) {
		this.dopaytermspercent = dopaytermspercent;
	}

	public BillingProductTaxesTable getBillingTaxTable() {
		return billingTaxTable;
	}

	public void setBillingTaxTable(BillingProductTaxesTable billingTaxTable) {
		this.billingTaxTable = billingTaxTable;
	}

	public BillingProductChargesTable getBillingChargesTable() {
		return billingChargesTable;
	}

	public void setBillingChargesTable(
			BillingProductChargesTable billingChargesTable) {
		this.billingChargesTable = billingChargesTable;
	}

	public TextBox getTbpersonCount() {
		return tbpersonCount;
	}

	public void setTbpersonCount(TextBox tbpersonCount) {
		this.tbpersonCount = tbpersonCount;
	}

	public TextBox getTbpersonName() {
		return tbpersonName;
	}

	public void setTbpersonName(TextBox tbpersonName) {
		this.tbpersonName = tbpersonName;
	}

	public TextBox getTbpersonCell() {
		return tbpersonCell;
	}

	public void setTbpersonCell(TextBox tbpersonCell) {
		this.tbpersonCell = tbpersonCell;
	}

	public TextBox getTbaccounttype() {
		return tbaccounttype;
	}

	public void setTbaccounttype(TextBox tbaccounttype) {
		this.tbaccounttype = tbaccounttype;
	}

	public TextBox getTbremark() {
		return tbremark;
	}

	public void setTbremark(TextBox tbremark) {
		this.tbremark = tbremark;
	}

	public TextBox getTbpocName() {
		return tbpocName;
	}

	public void setTbpocName(TextBox tbpocName) {
		this.tbpocName = tbpocName;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Double getToatalAmountIncludingTax() {
		return toatalAmountIncludingTax;
	}

	public void setToatalAmountIncludingTax(Double toatalAmountIncludingTax) {
		this.toatalAmountIncludingTax = toatalAmountIncludingTax;
	}

	public Double getNetPayableAmount() {
		return netPayableAmount;
	}

	public void setNetPayableAmount(Double netPayableAmount) {
		this.netPayableAmount = netPayableAmount;
	}

	public DateBox getDbbillingperiodFromDate() {
		return dbbillingperiodFromDate;
	}

	public void setDbbillingperiodFromDate(DateBox dbbillingperiodFromDate) {
		this.dbbillingperiodFromDate = dbbillingperiodFromDate;
	}

	public DateBox getDbbillingperiodToDate() {
		return dbbillingperiodToDate;
	}

	public void setDbbillingperiodToDate(DateBox dbbillingperiodToDate) {
		this.dbbillingperiodToDate = dbbillingperiodToDate;
	}

	public TextBox getTbrateContractServiceid() {
		return tbrateContractServiceid;
	}

	public void setTbrateContractServiceid(TextBox tbrateContractServiceid) {
		this.tbrateContractServiceid = tbrateContractServiceid;
	}
	
	/*
	 *  nidhi
	 *  20-07-2017
	 *  
	 */
	public DoubleBox getQuantity() {
		return quantity;
	}

	public void setQuantity(DoubleBox quantity) {
		this.quantity = quantity;
	}

	public ObjectListBox<Config> getOlbUOM() {
		return olbUOM;
	}

	public void setOlbUOM(ObjectListBox<Config> olbUOM) {
		this.olbUOM = olbUOM;
	}
	

	public TextBox getTbReferenceNumber() {
		return tbReferenceNumber;
	}

	public void setTbReferenceNumber(TextBox tbReferenceNumber) {
		this.tbReferenceNumber = tbReferenceNumber;
	}
	/**end
	 * 
	 */
	
	
	public DoubleBox getDbDiscountAmt() {
		return dbDiscountAmt;
	}
	
	public void setDbDiscountAmt(DoubleBox dbDiscountAmt) {
		this.dbDiscountAmt = dbDiscountAmt;
	}


	public DoubleBox getDbFinalTotalAmt() {
		return dbFinalTotalAmt;
	}


	public void setDbFinalTotalAmt(DoubleBox dbFinalTotalAmt) {
		this.dbFinalTotalAmt = dbFinalTotalAmt;
	}

	public DoubleBox getDbtotalAmt() {
		return dbtotalAmt;
	}

	public void setDbtotalAmt(DoubleBox dbtotalAmt) {
		this.dbtotalAmt = dbtotalAmt;
	}

	public DoubleBox getDbtotalAmtIncludingTax() {
		return dbtotalAmtIncludingTax;
	}

	public void setDbtotalAmtIncludingTax(DoubleBox dbtotalAmtIncludingTax) {
		this.dbtotalAmtIncludingTax = dbtotalAmtIncludingTax;
	}

	public DoubleBox getDbGrandTotalAmt() {
		return dbGrandTotalAmt;
	}

	public void setDbGrandTotalAmt(DoubleBox dbGrandTotalAmt) {
		this.dbGrandTotalAmt = dbGrandTotalAmt;
	}
	
	public TextBox getTbroundOffAmt() {
		return tbroundOffAmt;
	}
	public void setTbroundOffAmt(TextBox tbroundOffAmt) {
		this.tbroundOffAmt = tbroundOffAmt;
	}

	

	@Override
	public void onClick(ClickEvent event) {
		/**
		 * Date : 21-09-2017 By ANIL
		 */
			
		if(event.getSource().equals(addOthrChargesBtn)){
			Tax tax1=new Tax();
			Tax tax2=new Tax();
			OtherCharges prodCharges=new OtherCharges();
			prodCharges.setOtherChargeName("");
			prodCharges.setAmount(0);
			prodCharges.setTax1(tax1);
			prodCharges.setTax2(tax2);
			this.tblOtherCharges.getDataprovider().getList().add(prodCharges);
		}
		/**
		 * End
		 */
	}
	
	/**
	 * Date : 15-09-2017 BY ANIL
	 * Adding calculation part for other charges
	 */
	public void addOtherChargesInTaxTbl(){
		List<ContractCharges> taxList=this.billingTaxTable.getDataprovider().getList();
		for(OtherCharges otherCharges:tblOtherCharges.getDataprovider().getList()){
			/**
			 * If GST tax is not applicable then we will consider tax1 as vat tax field and tax2 as service tax field.
			 */
			if(otherCharges.getTax1().getTaxPrintName()!=null&&!otherCharges.getTax1().getTaxPrintName().equals("")){
				boolean updateTaxFlag=true;
				if(otherCharges.getTax1().getTaxPrintName().equalsIgnoreCase("SELECT")){
					updateTaxFlag=false;
//					return;
				}
				if(otherCharges.getTax1().getPercentage()==0){
					updateTaxFlag=false;
//					return;
				}
				System.out.println("INSIDE GST TAX1 PRINT NAME : "+otherCharges.getTax1().getTaxPrintName());
				if(updateTaxFlag){
					ContractCharges pocentity=new ContractCharges();
					pocentity.setTaxChargeName(otherCharges.getTax1().getTaxPrintName());
					pocentity.setTaxChargePercent(otherCharges.getTax1().getPercentage());
	//				pocentity.setIndexCheck(i+1);
					int indexValue=this.checkTaxPercent(otherCharges.getTax1().getPercentage(),otherCharges.getTax1().getTaxPrintName());
					if(indexValue!=-1){
						pocentity.setTaxChargeAssesVal(otherCharges.getAmount()+taxList.get(indexValue).getTaxChargeAssesVal());
						this.billingTaxTable.getDataprovider().getList().remove(indexValue);
					}
					if(indexValue==-1){
						pocentity.setTaxChargeAssesVal(otherCharges.getAmount());
					}
					this.billingTaxTable.getDataprovider().getList().add(pocentity);
				}
			}else{
				boolean updateTaxFlag=true;
				if(otherCharges.getTax1().getPercentage()==0){
					updateTaxFlag=false;
//					return;
				}
				if(updateTaxFlag){
					ContractCharges pocentity=new ContractCharges();
					pocentity.setTaxChargeName("VAT");
					pocentity.setTaxChargePercent(otherCharges.getTax1().getPercentage());
	//				pocentity.setIndexCheck(i+1);
					int indexValue=this.checkTaxPercent(otherCharges.getTax1().getPercentage(),"VAT");
					if(indexValue!=-1){
						pocentity.setTaxChargeAssesVal(otherCharges.getAmount()+taxList.get(indexValue).getTaxChargeAssesVal());
						this.billingTaxTable.getDataprovider().getList().remove(indexValue);
					}
					if(indexValue==-1){
						pocentity.setTaxChargeAssesVal(otherCharges.getAmount());
					}
					this.billingTaxTable.getDataprovider().getList().add(pocentity);
				}
			}
			
			if(otherCharges.getTax2().getTaxPrintName()!=null&&!otherCharges.getTax2().getTaxPrintName().equals("")){
				System.out.println("INSIDE GST TAX2 PRINT NAME : "+otherCharges.getTax2().getTaxPrintName());
				boolean updateTaxFlag=true;
				if(otherCharges.getTax2().getTaxPrintName().equalsIgnoreCase("SELECT")){
					updateTaxFlag=false;
//					return;
				}
				if(otherCharges.getTax2().getPercentage()==0){
					updateTaxFlag=false;
//					return;
				}
				if(updateTaxFlag){
				ContractCharges pocentity=new ContractCharges();
				pocentity.setTaxChargeName(otherCharges.getTax2().getTaxPrintName());
				pocentity.setTaxChargePercent(otherCharges.getTax2().getPercentage());
//				pocentity.setIndexCheck(i+1);
				int indexValue=this.checkTaxPercent(otherCharges.getTax2().getPercentage(),otherCharges.getTax2().getTaxPrintName());
				if(indexValue!=-1){
					pocentity.setTaxChargeAssesVal(otherCharges.getAmount()+taxList.get(indexValue).getTaxChargeAssesVal());
					this.billingTaxTable.getDataprovider().getList().remove(indexValue);
				}
				if(indexValue==-1){
					pocentity.setTaxChargeAssesVal(otherCharges.getAmount());
				}
				this.billingTaxTable.getDataprovider().getList().add(pocentity);
				}
			}else{
				boolean updateTaxFlag=true;
				System.out.println("ST OR NON GST");
				if(otherCharges.getTax2().getPercentage()==0){
					updateTaxFlag=false;
//					return;
				}
				if(otherCharges.getTax1().getTaxPrintName()!=null&&!otherCharges.getTax1().getTaxPrintName().equals("")){
					updateTaxFlag=false;
//					return;
				}
				if(updateTaxFlag){
				ContractCharges pocentity=new ContractCharges();
				pocentity.setTaxChargeName("Service Tax");
				pocentity.setTaxChargePercent(otherCharges.getTax2().getPercentage());
//				pocentity.setIndexCheck(i+1);
				int indexValue=this.checkTaxPercent(otherCharges.getTax2().getPercentage(),"Service");
				if(indexValue!=-1){
				 	double assessValue=otherCharges.getAmount()+(otherCharges.getAmount()*otherCharges.getTax1().getPercentage()/100);
					pocentity.setTaxChargeAssesVal(assessValue+taxList.get(indexValue).getTaxChargeAssesVal());
					this.billingTaxTable.getDataprovider().getList().remove(indexValue);
				}
				if(indexValue==-1){
					double assessValue=otherCharges.getAmount()+(otherCharges.getAmount()*otherCharges.getTax1().getPercentage()/100);
					pocentity.setTaxChargeAssesVal(assessValue);
				}
				this.billingTaxTable.getDataprovider().getList().add(pocentity);
				}
			}
		}
	}
	
	public  boolean validateunitandUOM(){
		/**
		 *  nidhi
		 *  30-03-2018
		 *  for make unit and UOm is mandatory
		 */
		boolean validateFlag = AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "MakeQty&UomMandatory");
		boolean unitFlag = true;
		if(validateFlag){
			if(tbrateContractServiceid.getValue() != null && tbrateContractServiceid.getValue().trim().matches("[0-9]*")  && !tbrateContractServiceid.getValue().trim().equals("")){
				if((quantity.getValue()==null || quantity.getValue()<=0) && olbUOM.getSelectedIndex()==0){
					showDialogMessage("Quantity and UOM is Mandatory!");	
					unitFlag = false;
				}else if(quantity.getValue()==null || quantity.getValue()<=0){
					showDialogMessage("Quantity is Mandatory!");	
					unitFlag = false;
				}else if(olbUOM.getSelectedIndex()==0){
					showDialogMessage("UOM is Mandatory!");	
					unitFlag = false;
				}
			}
		}
		/**
		 * end
		 */
		
		/**
		 * Date 12-10-2018 By Vijay
		 * Des :- Validation :- Billing Category Type Group if Mandatory with process config then must check when we sending Request For Approval
		 */
		boolean validateCategoryFlag = AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "MakeBillingCategoryMandatory");
		boolean validateTypeFlag = AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "MakeBillingTypeMandatory");
		boolean validateGroupFlag = AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "MakeBillingGroupMandatory");

		System.out.println("olbbillingcategory.getSelectedIndex()==0 ==="+olbbillingcategory.getSelectedIndex());
		System.out.println("olbbillingcategory.getSelectedIndex()==0 ==="+olbbillingtype.getSelectedIndex());

		if(validateCategoryFlag){
			if(olbbillingcategory.getSelectedIndex()==0){
				showDialogMessage("Category is Mandatory!");	
				unitFlag = false;
			}
		}
		else if(validateTypeFlag){
			if(olbbillingtype.getSelectedIndex()==0){
				showDialogMessage("Type is Mandatory!");	
				unitFlag = false;
			}
		}
		else{
			if(validateGroupFlag){
				if(olbbillinggroup.getSelectedIndex()==0){
					showDialogMessage("Group is Mandatory!");	
					unitFlag = false;
				}
			}
		}
		/**
		 * ends here
		 */
		
		
		return unitFlag;
	}
	/**
	 * End
	 */

	public OtherChargesTable getTblOtherCharges() {
		return tblOtherCharges;
	}

	public void setTblOtherCharges(OtherChargesTable tblOtherCharges) {
		this.tblOtherCharges = tblOtherCharges;
	}

	@Override
	public void refreshTableData() {
		// TODO Auto-generated method stub
		super.refreshTableData();
		
		tblOtherCharges.getTable().redraw();
		billingDocumentTable.getTable().redraw();
		salesProductTable.getTable().redraw();
		billingTaxTable.getTable().redraw();
		billingChargesTable.getTable().redraw();
	}

	public DoubleBox getDbOtherChargesTotal() {
		return dbOtherChargesTotal;
	}

	public void setDbOtherChargesTotal(DoubleBox dbOtherChargesTotal) {
		this.dbOtherChargesTotal = dbOtherChargesTotal;
	}

	
	private void getcustomerBillingAddress() {

		GenricServiceAsync service=GWT.create(GenricService.class);

		MyQuerry querry = new MyQuerry();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
	  
		int customerid = Integer.parseInt(tbpersonCount.getValue());
		
		filter = new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(customerid);
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Customer());
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				for(SuperModel model  : result){
					Customer customerEntity = (Customer) model;
					reactonUpdateTaxeDetails(customerEntity.getAdress().getState());
					break;
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
	}
	
	private void reactonUpdateTaxeDetails(String customerBillingAddressState) {

		Console.log("inside update tax method");
		List<SalesOrderProductLineItem> prodlist = salesProductTable.getDataprovider().getList();
		if(prodlist.size()>0){
			Branch branchEntity =olbbranch.getSelectedItem();
			
			boolean gstApplicable = false;
			BillingDocument billingEntity = (BillingDocument) this.getPresenter().getModel();
			String numberRange="";
			if(billingEntity!=null && billingEntity.getNumberRange()!=null && !billingEntity.getNumberRange().equals("")){
				numberRange = billingEntity.getNumberRange();
			}
			else{
				gstApplicable = true;
			}
			if(!numberRange.equals("")){
				ArrayList<Config> listConfig=LoginPresenter.globalConfig;
				for(Config confiEntity : listConfig){
					if(confiEntity.getType()==91 && confiEntity.getName().trim().equals(numberRange)){
						gstApplicable = confiEntity.isGstApplicable();
					}
				}
			}
			System.out.println("gstApplicable"+gstApplicable);
				System.out.println("customer onselection");
				Console.log("update taxes method");
				 List<SalesOrderProductLineItem> productlist = AppUtility.updateBillingTaxesDetails(prodlist, customerBillingAddressState, branchEntity,gstApplicable);
				 salesProductTable.getDataprovider().setList(productlist);
				 salesProductTable.getTable().redraw();
				 RowCountChangeEvent.fire(salesProductTable.getTable(), salesProductTable.getDataprovider().getList().size(), true);
				 setEnable(true);
		}
	
		
	}
	
	private double getLastTwoDecimalOnly(double AmountIncludingmoredecimal) {

		String strtotalamtincludingtax = AmountIncludingmoredecimal+"";
		if(strtotalamtincludingtax.contains(".")){
			String [] includingTaxAmtString = strtotalamtincludingtax.split("\\.");
			String firstDecimal = includingTaxAmtString[0];
			String secondDecimal = includingTaxAmtString[1];
			String twoDecimal = null;
			for(int p=0;p<secondDecimal.length();p++){
				if(p==0){
					twoDecimal = secondDecimal.charAt(p)+"";
				}
				else{
					twoDecimal += secondDecimal.charAt(p)+"";
				}
				if(p==1){
					break;
				}
			}
			System.out.println("twoDecimal "+twoDecimal);
			if(twoDecimal!=null){
				firstDecimal = includingTaxAmtString[0] +"."+twoDecimal;
				return Double.parseDouble(firstDecimal);
			}
			else{
				return AmountIncludingmoredecimal;
			}
		}
		return AmountIncludingmoredecimal;

	}
}
