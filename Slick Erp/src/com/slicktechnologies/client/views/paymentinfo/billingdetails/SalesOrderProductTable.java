package com.slicktechnologies.client.views.paymentinfo.billingdetails;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.SelectionCell;
import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.contract.ServiceBranchesPopup;
import com.slicktechnologies.client.views.popups.ProductSerailNumberPopup;
import com.slicktechnologies.client.views.salesorder.ItemProductDetailsPopUp;
import com.slicktechnologies.shared.BranchWiseScheduling;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.ProductSerialNoMapping;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.TaxDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.productlayer.Tax;
import com.slicktechnologies.shared.common.salesprocess.SalesOrderProductLineItem;

public class SalesOrderProductTable extends SuperTable<SalesOrderProductLineItem> implements ClickHandler {	
	TextColumn<SalesOrderProductLineItem> vatColumn,serviceColumn,totalColumn;
	TextColumn<SalesOrderProductLineItem> prodCategoryColumn,prodCodeColumn,prodNameColumn;
	TextColumn<SalesOrderProductLineItem> dateColumn;
	
	TextColumn<SalesOrderProductLineItem> priceColumn;

	
	TextColumn<SalesOrderProductLineItem> prodQtyColumn;
	
	Column<SalesOrderProductLineItem,String> paymentPercentColumn;
	Column<SalesOrderProductLineItem, String> payableBillingAmt;
	
	//  rohan remove this code and make base billing field editable on Date : 3/2/2017
//	TextColumn<SalesOrderProductLineItem> baseBillingAmt;
	Column<SalesOrderProductLineItem,String> baseBillingAmt;
	
	
	TextColumn<SalesOrderProductLineItem> viewServiceTaxColumn;
	TextColumn<SalesOrderProductLineItem> viewVatTaxColumn;
	
	/**
	 * rohan added this area field for making calculations easy and understandable
	 */
	/**
	 * rohan added this code for adding HSN code in prouct
	 */
	
	Column<SalesOrderProductLineItem,String> hsnCodeColumn;
	/**
	 * ends here  
	 */
	
	TextColumn<SalesOrderProductLineItem> prodAreaColumn;
	
	/**
	 * ends here 
	 */
	
	/**
	 * rohan added this code for adding HSN code in prouct
	 */

	TextColumn<SalesOrderProductLineItem> viewHsnCodeColumn;
	/**
	 * ends here  
	 */
	//***************rohan changes here ****************
	
	
	TextColumn<SalesOrderProductLineItem> discountAmt;
	TextColumn<SalesOrderProductLineItem> percDiscountColumn;
	
	Column<SalesOrderProductLineItem, String> flatDiscountAmt;
//	Column<SalesOrderProductLineItem, String> discountAmt,percDiscountColumn;
	Column<SalesOrderProductLineItem, String> viewdiscountAmt,viewpercDiscountColumn;
	
	ArrayList<TaxDetails> vattaxlist;
	ArrayList<TaxDetails> servicetaxlist;
	ArrayList<String> list;
	ArrayList<String> vatlist;
	
	Column<SalesOrderProductLineItem,String> editVatColumn,editServiceColumn;
	NumberFormat nf=NumberFormat.getFormat("0.00");
	
	Column<SalesOrderProductLineItem,String> deleteColumn;

	public static boolean newModeFlag=false;
	
	/**
	 * Date : 18-11-2017 BY ANIL
	 * made area , qty and price column editable for updating bill 
	 * 
	 * @author Anil , Date : 17-04-2019
	 * taking variable order type
	 * if order type sales then change column name from number of branch to qty
	 * and hide column area
	 * for roopson raised by Sonu
	 */
	Column<SalesOrderProductLineItem,String> editAreaColumn,editPriceColumn,editQtyColumn;
	public String orderType;
	/**
	 * End
	 */
	
	
	/**
	 * Date : 01-12-2017 BY ANIL
	 * 
	 */
	Column<SalesOrderProductLineItem,String> getEditableProductNameCol;
	/**
	 * End
	 */
	
	/**
	 * nidhi
	 * 9-06-2018
	 * for productDetail
	 */
	Column<SalesOrderProductLineItem, String> getDetailsButtonColumn;
	ItemProductDetailsPopUp itemDtPopUp = new ItemProductDetailsPopUp();
	int rowIndex;
	/**
	 * end
	 */
	/**
	 * nidhi
	 * 8-08-2018
	 * for map pro model , serialno
	 */
	Column<SalesOrderProductLineItem, String> getProModelNo;
	Column<SalesOrderProductLineItem, String> getProSerialNo;
	TextColumn<SalesOrderProductLineItem> getViewProModelNo, getViewProSerialNo;
	/**
	 * ends here
	 */
	/** date 14.7.2018 added by komal for purchase and sales order qty **/
	public String qtyHeader = "No of Branches";
	
	/**
	 * nidhi
	 * 29-08-2018
	 * 
	 */
	boolean flagforserialView = false;
	Column<SalesOrderProductLineItem, String> addProSerialNo;
	Column<SalesOrderProductLineItem, String> viewProSerialNo;
	boolean tableState = false;
	ProductSerailNumberPopup prodSerialPopup = new ProductSerailNumberPopup(true,false);
	HashMap<Integer, ArrayList<ProductSerialNoMapping>> proSerialList  = new HashMap<Integer, ArrayList<ProductSerialNoMapping>>();
	/**
	 * end
	 */
	public boolean salesBillDisable = false; // Nidhi
	/** date 15.4.2019 added by komal for nbhc service branches button and popup **/
	Column<SalesOrderProductLineItem, String> getServiceBranchesButton;
	ServiceBranchesPopup serviceBranchesPopup = new ServiceBranchesPopup();
	Column<SalesOrderProductLineItem, String> getEditableProductSrNoCol;
	TextColumn<SalesOrderProductLineItem> getViewProductSrNoCol;
	
	Column<SalesOrderProductLineItem, String> manpowerColumn;
	TextColumn<SalesOrderProductLineItem> viewmanpowercolumn;
	
	/**
	 * @author Anil @since 26-03-2021
	 */
	TextColumn<SalesOrderProductLineItem> billingTypeCol;
	public boolean showBillType=false;
	boolean enableSiteLocation=false;
	
	/**Added by Sheetal : 21-03-2022
	 * Des : Adding Unit of Measurement column at product level,requirement by Nitin sir**/
	ArrayList<String> UOM = new ArrayList<String>();
	Column<SalesOrderProductLineItem,String> UOMcolumn;
	TextColumn<SalesOrderProductLineItem> viewUOMcolumn;
	
	public SalesOrderProductTable()
	{
		super();
		itemDtPopUp.getLblCancel().addClickHandler(this);
		itemDtPopUp.getLblOk().addClickHandler(this);
		
		/**
		 * nidhi
		 * 22-08-2018
		 */
		prodSerialPopup.getLblOk().addClickHandler(this);
		prodSerialPopup.getLblCancel().addClickHandler(this);
		/** date 15.4.2019 added by komal for nbhc service branches button and popup **/
		serviceBranchesPopup.getLblOk().addClickHandler(this);
		serviceBranchesPopup.getLblCancel().addClickHandler(this);
		
		table.setHeight("300px");
		enableSiteLocation=AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "EnableSiteLocation");
	}
	public SalesOrderProductTable(UiScreen<SalesOrderProductLineItem> view)
	{
		super(view);	
		itemDtPopUp.getLblCancel().addClickHandler(this);
		itemDtPopUp.getLblOk().addClickHandler(this);
		
		/**
		 * nidhi
		 * 22-08-2018
		 */
		prodSerialPopup.getLblOk().addClickHandler(this);
		prodSerialPopup.getLblCancel().addClickHandler(this);
		
		/** date 15.4.2019 added by komal for nbhc service branches button and popup **/
		serviceBranchesPopup.getLblOk().addClickHandler(this);
		serviceBranchesPopup.getLblCancel().addClickHandler(this);
		
		table.setHeight("300px");
		enableSiteLocation=AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "EnableSiteLocation");
	}

	public void createTable()
	{
		enableSiteLocation=AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "EnableSiteLocation");
		if(newModeFlag==false){
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "ShowProductSerialNumber") &&
				LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN")){
			getEditableProductSrNoCol();
		}
//		createColumnprodHSNCodeColumn();
//		createColumnprodCategoryColumn();
//		createColumnprodCodeColumn();
		/**
		 * Date : 01-12-2017
		 * Editable Product name column
		 */
		getEditableProductNameCol();
//		createColumnprodNameColumn();
		/**
		 * End
		 */
		
		/** 
		 * @author Vijay  Date :- 14-08-2020
		 * Des :- for shasha if below process congi is active then No of branches column will hide
		 * and Display No of Man power. requirement raised by Rahul.
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "AddManpowercolumn")){
			createColumnManPower();
		}
		else{
			createColumnprodQtyColumn();
		}
		createColumnUOM();
		/**
		 * Date- 20-11-2017 By ANIL
		 */
//		createColumnprodAreaColumn();
//		createColumnpriceColumn();
		createEditableColumnArea();
		/**
		 * nidhi
		 * 8-08-2018
		 * for map model no and serialno
		 */
		if(LoginPresenter.mapModelSerialNoFlag){
			createColumnProdModelNo();
			createColumnProdSerialNo();
		}
		createEditableColumnPrice();
		/**
		 * End
		 */
//		createColumnvatColumn();
//		createColumnserviceColumn();
		
		
		retriveServiceTax();
		/**
		 *  Date : 01/03/2021 Added by - Priyanka 
		 *  Des : Table fields mismatch issue raised by Nitin Sir.
		 */
//		createColumnprodHSNCodeColumn();
//		createColumnprodCategoryColumn();
//		createColumnprodCodeColumn();
		
		
//		createEditColumnvatColumn();
//		createEditColumnserviceColumn();
//		createColumnPercDiscount();
//		createColumntotalAmt();
//		createColumnBaseBillingAmount();
//		createColumnPaymentPercent();
//		createColumnPayableAmt();
//		addFieldUpdater();
		table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);	
		}
		
		table.setWidth("auto");
		}
	
	
	protected void billingTypeCol(){
		billingTypeCol = new TextColumn<SalesOrderProductLineItem>() {
			@Override
			public String getValue(SalesOrderProductLineItem object) {
				if(object.getBillType()!=null){
					return object.getBillType();
				}
				return "";
			}
		};
		table.addColumn(billingTypeCol,"Billing Type");
		table.setColumnWidth(billingTypeCol, 90, Unit.PX);
	}
	
	
	
	private void createColumnprodHSNCodeColumn() {
//		viewHsnCodeColumn=new TextColumn<SalesOrderProductLineItem>()
//				{
//			@Override public String getValue(SalesOrderProductLineItem object){
//				return object.getHsnCode();
//			}
//		};
//		table.addColumn(viewHsnCodeColumn,"HSN Code");
//		table.setColumnWidth(viewHsnCodeColumn, 25, Unit.PX);
		
		
		EditTextCell editCell=new EditTextCell();
		hsnCodeColumn=new Column<SalesOrderProductLineItem,String>(editCell)
		{
			@Override
			public String getValue(SalesOrderProductLineItem object)
			{
				
				if(object.getHsnCode()!=null && !object.getHsnCode().equals("")){
					return object.getHsnCode();
				}
				else{
					return "";
				}
			}
		};
		table.addColumn(hsnCodeColumn,"#HSN/SAC Code");
		table.setColumnWidth(hsnCodeColumn, 90,Unit.PX);
		
	}
	
private void createColumnViewHSNCodeColumn() {
		
		viewHsnCodeColumn=new TextColumn<SalesOrderProductLineItem>()
			{
				@Override
				public String getValue(SalesOrderProductLineItem object)
				{
					if(object.getHsnCode()!=null && !object.getHsnCode().equals(""))
					return object.getHsnCode();
					return "";
				}
			};
			table.addColumn(viewHsnCodeColumn,"#HSN/SAC Code");
			table.setColumnWidth(viewHsnCodeColumn, 90,Unit.PX);
	}
	
	protected void createViewServiceTaxColumn(){
		
		viewServiceTaxColumn=new TextColumn<SalesOrderProductLineItem>() {
			public String getValue(SalesOrderProductLineItem object)
			{
				return object.getServiceTax().getTaxConfigName();
			}
		};
		table.addColumn(viewServiceTaxColumn,"# Tax 2" );
		table.setColumnWidth(viewServiceTaxColumn, 90,Unit.PX);
	}
	
	
	protected void createViewVatTaxColumn(){
		
		viewVatTaxColumn=new TextColumn<SalesOrderProductLineItem>() {
			public String getValue(SalesOrderProductLineItem object)
			{
				return object.getVatTax().getTaxConfigName();
			}
		};
		table.addColumn(viewVatTaxColumn,"# Tax 1" );
		table.setColumnWidth(viewVatTaxColumn, 90,Unit.PX);
	}
	
	private void retriveServiceTax() {
		final GenricServiceAsync service = GWT.create(GenricService.class);
		MyQuerry query = new MyQuerry();
		Company c=new Company();
		System.out.println("Company Id :: "+c.getCompanyId());
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		query.setFilters(filtervec);
		query.setQuerryObject(new TaxDetails());
		
		service.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
			}

		
			public void onSuccess(ArrayList<SuperModel> result) {
				
				System.out.println("result size of tax "+result);
				list=new ArrayList<String>();
				servicetaxlist=new ArrayList<TaxDetails>();
				vattaxlist = new ArrayList<TaxDetails>();
				vatlist=new ArrayList<String>();
				List<TaxDetails> backlist=new ArrayList<TaxDetails>();
				
				for (SuperModel model : result) {
					TaxDetails entity = (TaxDetails) model;
					
					backlist.add(entity);
					servicetaxlist.add(entity);
					vattaxlist.add(entity);
				}
				
				for(int i=0;i<backlist.size();i++){
//					if(backlist.get(i).getServiceTax().equals(true)){
						list.add(backlist.get(i).getTaxChargeName());
						System.out.println("list size"+list.size());
//					}
//					else if(backlist.get(i).getVatTax().equals(true)){
						vatlist.add(backlist.get(i).getTaxChargeName());
						System.out.println("valist size"+vatlist.size());
//					}
					}
				
				
				
				addEditColumnVatTax();
				addEditColumnServiceTax();
//				createColumnPercDiscount();
//				createColumntotalColumn();
//				createColumndeleteColumn();
//				addFieldUpdater();
				
				
				
				
				
//				createEditColumnvatColumn();
//				createEditColumnserviceColumn();
			
				
				//*************rohan added this for discount amt *************8
				createColumntotalAmt();
				createColumnPercDiscount();
				createColumnDiscountAmt();
				createViewColumnBaseBillingAmount();
				createColumnFlatDiscount();
				createColumnPaymentPercent();
				createColumnPayableAmt();
				/**
				 * nidhi
				 * 21-08-2018
				 * for add serial number
				 */
				if(LoginPresenter.mapModelSerialNoFlag && flagforserialView){
					createColumnAddProSerialNoColumn();
				}
				/**
				 * nidhi
				 * 9-06-2018
				 * 
				 */
				/** date 11.12.2018 added by komal for nbhc service branches button and popup **/
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "EnableServiceBranch")){
					getServiceBranchesButton();
				}
				
				/**
				 *  Date : 01/03/2021 Added by - Priyanka 
				 *  Des : Table fields mismatch issue raised by Nitin Sir.
				 */
				createColumnprodHSNCodeColumn();
				createColumnprodCategoryColumn();
				createColumnprodCodeColumn();
				/**
				 *  end
				 */
				getDetailsButtonColumn();
				
				Screen screenName=(Screen) AppMemory.getAppMemory().currentScreen;
				if(screenName.toString().trim().equals("INVOICEDETAILS"))
				{
					createColumndeleteColumn();
				}
				
				
				
				addFieldUpdater();
				
				
			}
			
		});
	}
	
	protected void createColumndeleteColumn() {
		ButtonCell btnCell= new ButtonCell();
		deleteColumn=new Column<SalesOrderProductLineItem,String>(btnCell)
				{
			@Override
			public String getValue(SalesOrderProductLineItem object)
			{
				return  "Delete" ;
			}
				};
				table.addColumn(deleteColumn,"Delete");
				table.setColumnWidth(deleteColumn, 100,Unit.PX);
				
	}
	protected void createViewFlatDiscount()
	{
		flatDiscountAmt=new TextColumn<SalesOrderProductLineItem>()
				{
			@Override
			public String getValue(SalesOrderProductLineItem object)
			{
				return nf.format(object.getFlatDiscount());
			}
		};
		table.addColumn(flatDiscountAmt,"#Flat Disc");
		table.setColumnWidth(flatDiscountAmt, 90, Unit.PX);
	}
	
	private void createColumnFlatDiscount() {
		
		EditTextCell editCell=new EditTextCell();
		flatDiscountAmt=new Column<SalesOrderProductLineItem,String>(editCell)
				{
			@Override
			public String getValue(SalesOrderProductLineItem object)
			{
				if(object.getFlatDiscount()==0){
					return 0+"";}
				else{
					return object.getFlatDiscount()+"";}
			}
				};
				table.addColumn(flatDiscountAmt,"#Flat Disc");
				table.setColumnWidth(flatDiscountAmt, 90, Unit.PX);
	}
	
	
	public void addEditColumnVatTax(){
		SelectionCell employeeselection= new SelectionCell(vatlist);
		editVatColumn = new Column<SalesOrderProductLineItem, String>(employeeselection) {
			@Override
			public String getValue(SalesOrderProductLineItem object) {
				
				System.out.println("getVatTaxEdit in biling "+object.getVatTaxEdit()+object.getVatTax().getTaxConfigName());
				
				if (object.getVatTax().getTaxConfigName()!= null && !object.getVatTax().getTaxConfigName().equals("")) {
					Console.log("Vat tax value "+object.getVatTax().getTaxConfigName());
					Console.log("Vat tax value "+object.getVatTax().getTaxConfigName()+"-"+object.getVatTax().getTaxName()+"-"+object.getVatTax().getPercentage());
					return object.getVatTax().getTaxConfigName();
				} else
					Console.log("Vat tax edit null value ");
					return "NA";
			}
		};
		table.addColumn(editVatColumn, "#Tax 1");
		table.setColumnWidth(editVatColumn,90,Unit.PX);
	}	
	
	
	//***************discount Amt column *************** 
		private void createColumnDiscountAmt() {
			
			discountAmt=new TextColumn<SalesOrderProductLineItem>()
					{
				@Override
				public String getValue(SalesOrderProductLineItem object)
				{
					if(object.getDiscountAmt()==0){
						return 0+"";}
					else{
						return object.getDiscountAmt()+"";}
				}
			};
			table.addColumn(discountAmt,"Disc Amt");
			table.setColumnWidth(discountAmt, 90, Unit.PX);
			
//			EditTextCell editCell=new EditTextCell();
//			discountAmt=new Column<SalesOrderProductLineItem,String>(editCell)
//					{
//				@Override
//				public String getValue(SalesOrderProductLineItem object)
//				{
//					if(object.getDiscountAmt()==0){
//						return 0+"";}
//					else{
//						return object.getDiscountAmt()+"";}
//				}
//					};
//					table.addColumn(discountAmt,"Disc Amt");
//					table.setColumnWidth(discountAmt, 25, Unit.PX);
		}
		
		protected void createViewDiscountAmt()
		{
			viewdiscountAmt=new TextColumn<SalesOrderProductLineItem>()
					{
				@Override
				public String getValue(SalesOrderProductLineItem object)
				{
					if(object.getDiscountAmt()==0){
						return 0+"";}
					else{
						return object.getDiscountAmt()+"";}
				}
					};
					table.addColumn(viewdiscountAmt,"Disc Amt");
					table.setColumnWidth(viewdiscountAmt, 90, Unit.PX);
		}
	
	
	public void updateVatTaxColumn()
	{

		editVatColumn.setFieldUpdater(new FieldUpdater<SalesOrderProductLineItem, String>() {
			
			@Override
			public void update(int index, SalesOrderProductLineItem object, String value) {
				try {
					String val1 = (value.trim());
					object.setVatTaxEdit(val1);
					Tax tax = object.getVatTax();
					for(int i=0;i<vattaxlist.size();i++)
					{
						if(val1.trim().equals(vattaxlist.get(i).getTaxChargeName())){
							tax.setTaxName(val1);
							tax.setPercentage(vattaxlist.get(i).getTaxChargePercent());
							/**
							 * rohan added this for GST implementation Date :23-06-2017
							 */
							tax.setTaxPrintName(vattaxlist.get(i).getTaxPrintName());
							tax.setTaxConfigName(val1);
							/**
							 * nidhi
							 * 8-06-2018
							 * for tax selection option
							 */
//							checkTaxValue(tax,object);
							Tax taxDt = AppUtility.checkTaxValue(tax, object.getServiceTax(), vattaxlist);
							if(taxDt != null){
								object.setServiceTax(taxDt);
							}
							/**
							 * end
							 */
							

							/**
							 * Date 28-07-2018 By Vijay
							 * Des :- If we select IGST then second tax box will be disable.if we select other than IGST then second tax box will be visible
							 */
							if(vattaxlist.get(i).getTaxPrintName().equals("IGST") || vattaxlist.get(i).getTaxPrintName().equals("IGST@18") || vattaxlist.get(i).getTaxPrintName().equals("SEZ")){
								editServiceColumn.setCellStyleNames("hideVisibility");
							}else{
								editServiceColumn.setCellStyleNames("showVisibility");
							}
							/**
							 * ends here
							 */
							
							break;
						}
					}
					object.setVatTax(tax);
					
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
					
				} catch (NumberFormatException e) {
				}
				table.redrawRow(index);
			}
		});
	}
	
	
	
//	service tax Selection List Column
	public void addEditColumnServiceTax() {
		SelectionCell employeeselection= new SelectionCell(list);
		editServiceColumn = new Column<SalesOrderProductLineItem, String>(employeeselection) {
			@Override
			public String getValue(SalesOrderProductLineItem object) {
				if (object.getServiceTax().getTaxConfigName()!= null && !object.getServiceTax().getTaxConfigName().equals("")) {
					Console.log("ST tax value "+object.getServiceTax().getTaxConfigName());
					Console.log("ST tax value "+object.getServiceTax().getTaxConfigName()+"-"+object.getServiceTax().getTaxName()+"-"+object.getServiceTax().getPercentage());
					return object.getServiceTax().getTaxConfigName();
				} else
					Console.log("ST tax value in side else  ");
					return "NA";
			}

			
		};
		table.addColumn(editServiceColumn, "#Tax 2");
		table.setColumnWidth(editServiceColumn,90,Unit.PX);
	}
	
	
	public void updateServiceTaxColumn()
	{

		editServiceColumn.setFieldUpdater(new FieldUpdater<SalesOrderProductLineItem, String>() {
			
			@Override
			public void update(int index, SalesOrderProductLineItem object, String value) {
				try {
					String val1 = (value.trim());
					object.setServiceTaxEdit(val1);
					Tax tax = object.getServiceTax();
					for(int i=0;i<servicetaxlist.size();i++)
					{
						if(val1.trim().equals(servicetaxlist.get(i).getTaxChargeName())){
							tax.setTaxName(val1);
							tax.setPercentage(servicetaxlist.get(i).getTaxChargePercent());
							/**
							 * rohan added this for GST implementation Date :23-06-2017
							 */
							tax.setTaxPrintName(servicetaxlist.get(i).getTaxPrintName());
							tax.setTaxConfigName(val1);
							/**
							 * nidhi
							 * 8-06-2018
							 * 
							 */
							//checkServiceTaxValue(tax, object);
//							Tax vatTax =AppUtility.checkse
							Tax vatTax = AppUtility.checkTaxValue(tax, object.getVatTax(), vattaxlist);
							if(vatTax!=null){
								object.setVatTax(vatTax);
							}
							/**
							 * end
							 */
							/**
							 * Date 28-07-2018 By Vijay
							 * Des :- If we select IGST then second tax box will be disable.if we select other than IGST then second tax box will be visible
							 */
							
							if(servicetaxlist.get(i).getTaxPrintName().equals("IGST") || servicetaxlist.get(i).getTaxPrintName().equals("IGST@18") || servicetaxlist.get(i).getTaxPrintName().equals("SEZ")){
								vatColumn.setCellStyleNames("hideVisibility");
							}else{
								vatColumn.setCellStyleNames("showVisibility");
							}
							/**
							 * ends here
							 */
							break;
						}
					}
					object.setServiceTax(tax);
					
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
					
				} catch (NumberFormatException e) {
				}
				table.redrawRow(index);
			}
		});
	}
	
	protected void createEditColumnvatColumn() {
		EditTextCell editCell = new EditTextCell();
		editVatColumn = new Column<SalesOrderProductLineItem, String>(editCell) {
			@Override
			public String getValue(SalesOrderProductLineItem object) {
				if (object.getVatTax() != null) {
					return object.getVatTax().getPercentage() + "";
				} else {
					return "N.A.";
				}
			}
		};
		table.addColumn(editVatColumn, "#Tax 1");
		table.setColumnWidth(editVatColumn, 90, Unit.PX);
	}

	protected void createEditColumnserviceColumn() {
		EditTextCell editCell = new EditTextCell();
		editServiceColumn = new Column<SalesOrderProductLineItem, String>(editCell) {
			@Override
			public String getValue(SalesOrderProductLineItem object) {
				// SuperProduct prod=object.getPrduct();
				if (object.getServiceTax() != null) {
					return object.getServiceTax().getPercentage() + "";
				} else {
					return "N.A.";
				}
			}
		};
		table.addColumn(editServiceColumn, "#Tax 2");
		table.setColumnWidth(editServiceColumn, 90, Unit.PX);
	}
	
	

	protected void createColumnprodCategoryColumn()
	{
		prodCategoryColumn=new TextColumn<SalesOrderProductLineItem>()
				{
			@Override public String getValue(SalesOrderProductLineItem object){
				return object.getProdCategory();
			}
		};
		table.addColumn(prodCategoryColumn,"Category");
		table.setColumnWidth(prodCategoryColumn, 90, Unit.PX);

	}
	protected void createColumnprodCodeColumn()
	{
		prodCodeColumn=new TextColumn<SalesOrderProductLineItem>()
				{
			@Override public String getValue(SalesOrderProductLineItem object){
				return object.getProdCode();
			}
		};
		table.addColumn(prodCodeColumn,"PCode");
		table.setColumnWidth(prodCodeColumn, 90, Unit.PX);
	}
	protected void createColumnprodNameColumn()
	{
		prodNameColumn=new TextColumn<SalesOrderProductLineItem>()
				{
			@Override public String getValue(SalesOrderProductLineItem object){
				return object.getProdName();
			}
		};
		table.addColumn(prodNameColumn,"Name");
		table.setColumnWidth(prodNameColumn, 100, Unit.PX);
	}

	protected void createColumnprodQtyColumn() {
		prodQtyColumn = new TextColumn<SalesOrderProductLineItem>() {
			@Override
			public String getValue(SalesOrderProductLineItem object) {
				if (object.getQuantity() == 0) {
					return 0 + "";
				} else {
					return object.getQuantity() + "";
				}
			}
		};
		if (qtyHeader == null || qtyHeader.equals(""))
			qtyHeader = "No of Branches";

		table.addColumn(prodQtyColumn, qtyHeader);
		table.setColumnWidth(prodQtyColumn, 70, Unit.PX);
	}
	
	
	
	protected void createColumnprodAreaColumn()
	{
		prodAreaColumn=new TextColumn<SalesOrderProductLineItem>()
				{
			@Override
			public String getValue(SalesOrderProductLineItem object)
			{
				if(object.getArea()!=null){
					return object.getArea();
				}else{
					return "";
				}
			}
				};
				table.addColumn(prodAreaColumn,"Area");
				table.setColumnWidth(prodAreaColumn, 90, Unit.PX);
	}
	
	protected void createColumnpriceColumn()
	{
	//   old code commented by rohan and added new code
		
		priceColumn=new TextColumn<SalesOrderProductLineItem>()
				{
			@Override
			public String getValue(SalesOrderProductLineItem object)
			{
				return nf.format(object.getPrice());
			}
		};
		table.addColumn(priceColumn,"Price");
		table.setColumnWidth(priceColumn, 90, Unit.PX);
		
		
//		EditTextCell editCell=new EditTextCell();
//		priceColumn=new Column<SalesOrderProductLineItem,String>(editCell)
//		{
//			@Override
//			public String getValue(SalesOrderProductLineItem object)
//			{
//					return nf.format(object.getPrice());
//			}
//		};
//				table.addColumn(priceColumn,"#Price");
//				table.setColumnWidth(priceColumn, 25, Unit.PX);
		}
	
	
	
	protected void createColumnvatColumn()
	{
		vatColumn=new TextColumn<SalesOrderProductLineItem>()
				{
			@Override
			public String getValue(SalesOrderProductLineItem object)
			{
				if(object.getVatTax()!=null)
				{
					return object.getVatTax()+"";
				}
				else{
					return "N.A.";
				}
				
			}
		};
		table.addColumn(vatColumn,"Tax 1");
		table.setColumnWidth(vatColumn, 90, Unit.PX);
	}
	
	protected void createColumnserviceColumn()
	{
		serviceColumn=new TextColumn<SalesOrderProductLineItem>()
				{
			@Override
			public String getValue(SalesOrderProductLineItem object)
			{
				if(object.getServiceTax()!=null){
				return object.getServiceTax()+"";}
				else{
					return "N.A.";
				}
			}
		};
		table.addColumn(serviceColumn,"Tax 2");
		table.setColumnWidth(serviceColumn, 90, Unit.PX);
	}
	
	protected void createColumntotalAmt()
	{
		totalColumn=new TextColumn<SalesOrderProductLineItem>()
				{
			@Override
			public String getValue(SalesOrderProductLineItem object)
			{
				return nf.format(object.getTotalAmount());
			}
		};
		table.addColumn(totalColumn,"Total");
		table.setColumnWidth(totalColumn, 90, Unit.PX);
	}
	
	
	protected void createColumnPercDiscount()
	{
		
		percDiscountColumn=new TextColumn<SalesOrderProductLineItem>()
				{
			@Override
			public String getValue(SalesOrderProductLineItem object)
			{
				if(object.getProdPercDiscount()==0||object.getProdPercDiscount()==null){
					return 0+"";}
				else{
					return object.getProdPercDiscount()+"";}
			}
		};
		table.addColumn(percDiscountColumn,"% Disc");
		table.setColumnWidth(percDiscountColumn, 70, Unit.PX);
		
//		EditTextCell editCell=new EditTextCell();
//		percDiscountColumn=new Column<SalesOrderProductLineItem,String>(editCell)
//				{
//			@Override
//			public String getValue(SalesOrderProductLineItem object)
//			{
//				if(object.getProdPercDiscount()==0||object.getProdPercDiscount()==null){
//					return 0+"";}
//				else{
//					return object.getProdPercDiscount()+"";}
//			}
//				};
//				table.addColumn(percDiscountColumn,"% Disc");
//				table.setColumnWidth(percDiscountColumn, 25, Unit.PX);
	}
	
	protected void createViewColumnBaseBillingAmount()
	{
	baseBillingAmt=new TextColumn<SalesOrderProductLineItem>()
	{
	@Override
	public String getValue(SalesOrderProductLineItem object)
		{
			
			return nf.format(object.getBaseBillingAmount());
		}
	};
	table.addColumn(baseBillingAmt,"Base Billing Amt");
	table.setColumnWidth(baseBillingAmt, 90, Unit.PX);
	}
	
	protected void createColumnBaseBillingAmount()
	{
		EditTextCell editCell=new EditTextCell();
		baseBillingAmt=new Column<SalesOrderProductLineItem,String>(editCell)
				{
			@Override
			public String getValue(SalesOrderProductLineItem object)
			{
					return nf.format(object.getBaseBillingAmount());
			}
				};
				table.addColumn(baseBillingAmt,"#Base Billing Amt");
				table.setColumnWidth(baseBillingAmt, 90, Unit.PX);
	}
	
	protected void createColumnPaymentPercent()
	{
	EditTextCell editCell=new EditTextCell();
	paymentPercentColumn=new Column<SalesOrderProductLineItem,String>(editCell)
	{
	@Override
	public String getValue(SalesOrderProductLineItem object)
	{
		if(object.getPaymentPercent()==0)
		{
			return 100+"";
		}
		else
		{
			return object.getPaymentPercent()+"";
		}
	}
//	@Override
//	public void onBrowserEvent(Context context, Element elem,
//			SalesOrderProductLineItem object, NativeEvent event) {
//		// TODO Auto-generated method stub
//	if(salesBillDisable){
//		
//	}else{
//		super.onBrowserEvent(context, elem, object, event);
//	}
//	
//	}
	};
	table.addColumn(paymentPercentColumn,"Payment Percent");
	table.setColumnWidth(paymentPercentColumn, 90, Unit.PX);
	}
	
	protected void createViewPaymentPercent()
	{
		paymentPercentColumn=new TextColumn<SalesOrderProductLineItem>()
				{
			@Override
			public String getValue(SalesOrderProductLineItem object)
			{
				if(object.getPaymentPercent()==0){
					return 0+"";}
				else{
					return object.getPaymentPercent()+"";}
			}
				};
				table.addColumn(paymentPercentColumn,"Payment Percent");
				table.setColumnWidth(paymentPercentColumn, 90, Unit.PX);
	}
	
	protected void createViewColumnPayableAmt(){
		payableBillingAmt=new TextColumn<SalesOrderProductLineItem>()
				{
			@Override
			public String getValue(SalesOrderProductLineItem object)
			{
				if(object.getBasePaymentAmount()==0){
					return 0+"";
				}
				else{
					return nf.format(object.getBasePaymentAmount())+"";
					}
			}
//			@Override
//			public void onBrowserEvent(Context context, Element elem,
//					SalesOrderProductLineItem object, NativeEvent event) {
//				// TODO Auto-generated method stub
//			if(salesBillDisable){
//				
//			}else{
//				super.onBrowserEvent(context, elem, object, event);
//			}
//			
//			}
				};
				table.addColumn(payableBillingAmt,"Payable Amt");
				table.setColumnWidth(payableBillingAmt, 90, Unit.PX);
	}
	

	protected void createColumnPayableAmt()
	{
		EditTextCell editCell=new EditTextCell();
		payableBillingAmt=new Column<SalesOrderProductLineItem,String>(editCell)
				{
			@Override
			public String getValue(SalesOrderProductLineItem object)
			{
				
				if(object.getBasePaymentAmount()!=0 || object.isPayableamtupdateFlag()){
					return nf.format(object.getBasePaymentAmount());
				}
				else
				{
				
				String paypercent=null;
				double convertval=0,baseBilling=0;
				
				baseBilling=object.getBaseBillingAmount();
				
				baseBilling=baseBilling - object.getFlatDiscount();
				
				paypercent=object.getPaymentPercent()+"";
				convertval=Double.parseDouble(paypercent);
				double totalpayamt=(baseBilling*convertval)/100;
				System.out.println("rohan total amt"+totalpayamt);
				object.setBasePaymentAmount(totalpayamt);
				return nf.format(totalpayamt);
			}
			}
				};
				table.addColumn(payableBillingAmt,"#Payable Amt");
				table.setColumnWidth(payableBillingAmt, 90, Unit.PX);
		
		
//		TextCell editCell=new TextCell();
//		payableBillingAmt=new Column<SalesOrderProductLineItem,String>(editCell)
//				{
//			@Override
//			public String getValue(SalesOrderProductLineItem object)
//			{
//				
//				String paypercent=null;
//				double convertval=0,baseBilling=0;
//				
//				baseBilling=object.getBaseBillingAmount();
//				
//				baseBilling=baseBilling - object.getFlatDiscount();
//				
//				paypercent=object.getPaymentPercent()+"";
//				convertval=Double.parseDouble(paypercent);
//				double totalpayamt=(baseBilling*convertval)/100;
//				
//				return nf.format(totalpayamt);
//				
//				
//			}
//				};
//				table.addColumn(payableBillingAmt,"Payable Amt");
//				table.setColumnWidth(payableBillingAmt, 30, Unit.PX);
	}
	
	@Override 
	public void addFieldUpdater() 
	{
		/**
		 * nidhi
		 * 9-08-2018
		 * for map model and serial no
		 */
		if(LoginPresenter.mapModelSerialNoFlag){
			createFieldUpdaterProModelNoColumn();
			createFieldUpdateProSerailNo();
		}
		/**
		 * end
		 */
		createFieldUpdaterPaymentPercent();
//		createFieldUpdaterServiceTaxColumn();
//		createFieldUpdaterVatColumn();
		updateVatTaxColumn();
		updateServiceTaxColumn();
		
		createFieldUpdaterFlatDiscountColumn();
		createFieldUpdaterForPayableAmtColumn();
		
		//Date 17-08-2017 added by vijay for hsn code
		updateHSNCode();
		/**
		 * nidhi
		 * 9-06-2018
		 */
		createFieldUpdaterOnBranchesColumn();
		/**
		 * ends here
		 */
		/**
		 * nidhi
		 * 21-08-2018
		 */
		if(LoginPresenter.mapModelSerialNoFlag && flagforserialView){
			if(addProSerialNo!=null)
			createFieldUpdaterAddSerialNo();
			if(viewProSerialNo!=null)
			createFieldUpdaterViewSerialNo();
		}
		/** date 11.12.2018 added by komal for nbhc service branches button and popup **/
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "EnableServiceBranch")){
			addFieldUpdaterOnServiceBranchesButton();
		}
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "AddManpowercolumn")){
			addFieldUpdaterManPower();
		}
		UpdateUOMColumn();
		
		Screen screenName=(Screen) AppMemory.getAppMemory().currentScreen;
		Console.log("screenName "+screenName);
		if(screenName.toString().trim().equals("INVOICEDETAILS"))
		{
			createFieldUpdaterdeleteColumn();
		}
	}
	
	private void updateHSNCode() {
		hsnCodeColumn.setFieldUpdater(new FieldUpdater<SalesOrderProductLineItem, String>() {
			
			@Override
			public void update(int index, SalesOrderProductLineItem object, String value) {
				// TODO Auto-generated method stub
				object.setHsnCode(value);
				table.redrawRow(index);
			}
		});
	}
	
	private void createFieldUpdaterForPayableAmtColumn(){

		payableBillingAmt.setFieldUpdater(new FieldUpdater<SalesOrderProductLineItem, String>()
				{
			@Override
			public void update(int index,SalesOrderProductLineItem object,String value)
			{

				try{
					Double val1=Double.parseDouble(value.trim());
					object.setBasePaymentAmount(val1);
					
					//  calculation payment  percents 
					double baseBilling=0;
					
					object.setFlatDiscount(0);
					
					baseBilling=object.getBaseBillingAmount();  
					double paypercent =(val1*100)/baseBilling;
				
					System.out.println("rohan payment percent"+paypercent);
					object.setPaymentPercent(paypercent);
					
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				}
				catch (NumberFormatException e)
				{

				}
				setEnable(true);
				table.redrawRow(index);
			}
				});

	}
	
	private void createFieldUpdaterFlatDiscountColumn() {
		
		flatDiscountAmt.setFieldUpdater(new FieldUpdater<SalesOrderProductLineItem, String>()
				{
			@Override

			public void update(int index,SalesOrderProductLineItem object,String value)
			{

				try{
					Double val1=Double.parseDouble(value.trim());
					object.setFlatDiscount(val1);
					
					
				//  calculation payment  percents 
					 double baseBilling=object.getBaseBillingAmount();
					 baseBilling = baseBilling -object.getFlatDiscount();
					 
					 double payableAmt =(baseBilling * object.getPaymentPercent())/100;
//					 payableAmt = payableAmt - object.getFlatDiscount();
					 
					 object.setBasePaymentAmount(payableAmt);
					 
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);

				}
				catch (NumberFormatException e)
				{

				}
				setEnable(true);
				table.redrawRow(index);
			}
				});
	}
//	protected void createFieldUpdaterPercDiscColumn()
//	{
//		percDiscountColumn.setFieldUpdater(new FieldUpdater<SalesOrderProductLineItem, String>()
//				{
//			@Override
//
//			public void update(int index,SalesOrderProductLineItem object,String value)
//			{
//
//				try{
//					Double val1=Double.parseDouble(value.trim());
//					object.setProdPercDiscount(val1);
//					
//					double baseAMt= object.getBaseBillingAmount()-(object.getBaseBillingAmount()*val1)/100;
//					
//					System.out.println("AAAAAAAAAAAAA"+baseAMt);
//					object.setTotalAmount(baseAMt);
//					
//					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
//
//				}
//				catch (NumberFormatException e)
//				{
//
//				}
//				table.redrawRow(index);
//			}
//				});
//	}
//	
//	protected void createFieldUpdaterDiscAmtColumn()
//	{
//		discountAmt.setFieldUpdater(new FieldUpdater<SalesOrderProductLineItem, String>()
//				{
//			@Override
//
//			public void update(int index,SalesOrderProductLineItem object,String value)
//			{
//
//				try{
//					Double val1=Double.parseDouble(value.trim());
//					object.setDiscountAmt(val1);
//					
//					double baseAMt= object.getBaseBillingAmount()-val1;
//					
//					System.out.println("AAAAAAAAAAAAA"+baseAMt);
//					object.setTotalAmount(baseAMt);
//					
//					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
//
//				}
//				catch (NumberFormatException e)
//				{
//
//				}
//				table.redrawRow(index);
//			}
//				});
//	}
	
	protected void createFieldUpdaterServiceTaxColumn(){
		editServiceColumn.setFieldUpdater(new FieldUpdater<SalesOrderProductLineItem, String>() {
			@Override
			public void update(int index, SalesOrderProductLineItem object, String value) {
				try {
					Double val1 = Double.parseDouble(value.trim());

//					object.setServiceTax(serviceTax);;
					object.getServiceTax().setPercentage(val1);
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);

				} catch (Exception e) {
				}
				table.redrawRow(index);
			}
		});
	}
	
	protected void createFieldUpdaterVatColumn(){
		editVatColumn.setFieldUpdater(new FieldUpdater<SalesOrderProductLineItem, String>() {
			@Override
			public void update(int index, SalesOrderProductLineItem object, String value) {
				try {
					Double val1 = Double.parseDouble(value.trim());

//					object.setServiceTax(serviceTax);;
					object.getVatTax().setPercentage(val1);
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);

				} catch (Exception e) {
				}
				table.redrawRow(index);
			}
		});
	}
	
	protected void createFieldUpdaterPaymentPercent()
	{
		paymentPercentColumn.setFieldUpdater(new FieldUpdater<SalesOrderProductLineItem, String>()
		{
		@Override
		public void update(int index,SalesOrderProductLineItem object,String value)
		{
			try{
				Double val1=Double.parseDouble(value.trim());
				object.setPaymentPercent(val1);
				
//				double baseBilling=0;
//				
//				baseBilling=object.getBaseBillingAmount();  
//				double payableAMt =(baseBilling*val1)/100;
//				payableAMt= payableAMt- object.getFlatDiscount();
//			
//				System.out.println("rohan payment percent"+payableAMt);
//				object.setBasePaymentAmount(payableAMt);
				
				
				double baseBilling=0;
				
				baseBilling=object.getBaseBillingAmount();
				baseBilling=baseBilling-object.getFlatDiscount();
				
				double payableAMt =(baseBilling*val1)/100;
//				payableAMt= payableAMt- object.getFlatDiscount();
			
				System.out.println("rohan payment percent"+payableAMt);
				object.setBasePaymentAmount(payableAMt);
				
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
			}
			catch (NumberFormatException e)
			{
				e.printStackTrace();
			}
			setEnable(true);
			table.redrawRow(index);
		}
		});
	}
	
	@Override
	public void setEnable(boolean state){
		for (int i = table.getColumnCount() - 1; i > -1; i--)
			table.removeColumn(i);

		if (state == true) {
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "ShowProductSerialNumber")
					&& LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN")){
				getEditableProductSrNoCol();
			}
//			createColumnprodHSNCodeColumn();
//			createColumnprodCategoryColumn();
//			createColumnprodCodeColumn();
			if(showBillType){
				billingTypeCol();
			}
			/**
			 * Date : 01-12-2017 Editable Product name column
			 */
			getEditableProductNameCol();
			// createColumnprodNameColumn();
			/**
			 * End
			 */
			
			/**
			 * @author Anil , Date : 30-07-2019
			 * for purchase we are storing quantity in area field , so for purchase type bill this column should not be visible
			 */
			if(orderType != null && orderType.equals(AppConstants.ORDERTYPESALES)){
				createEditableColumnQty();
			}else if(orderType!=null&&!orderType.equals(AppConstants.ORDERTYPEFORPURCHASE)){
//				createColumnprodQtyColumn();
				/** 
				 * @author Vijay  Date :- 14-08-2020
				 * Des :- for shasha if below process config is active then No of branches column will hide
				 * and Display No of Man power. requirement raised by Rahul.
				 */
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "AddManpowercolumn")){
					createColumnManPower();
				}
				else{
					createColumnprodQtyColumn();
				}
				
			}
			createColumnUOM();
			/**
			 * Date- 20-11-2017 By ANIL
			 */
			// createColumnprodAreaColumn();
			// createColumnpriceColumn();
			/**
			 * @author Anil , Date : 17-04-2019
			 * hiding area/qty column for order type sale and purchase
			 * raised by sonu for roopson
			 * @author Anil , Date : 30-07-2019
			 * for purchase we are storing quantity in area field , so for purchase type bill this column should be visible
			 */
			if(orderType!=null&&!orderType.equals(AppConstants.ORDERTYPESALES)){
				createEditableColumnArea();
			}
			/**
			 * nidhi 8-08-2018 for map serial no
			 */
			if (LoginPresenter.mapModelSerialNoFlag) {
				createColumnProdModelNo();
				createColumnProdSerialNo();
			}
			createEditableColumnPrice();
			/**
			 * End
			 */
			// createColumnvatColumn();
			// createColumnserviceColumn();
			// createEditColumnvatColumn();
			// createEditColumnserviceColumn();

			// createColumnPercDiscount();
			// createColumntotalAmt();
			// createColumnBaseBillingAmount();
			// createColumnPaymentPercent();
			// createColumnPayableAmt();
			// addFieldUpdater();
			retriveServiceTax();
//			createColumnprodHSNCodeColumn();
//			createColumnprodCategoryColumn();
//			createColumnprodCodeColumn();
		} else {
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "ShowProductSerialNumber")
					&& LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN")){
				createColumnViewProductSrNoCol();
			}
//			createColumnViewHSNCodeColumn();
//			createColumnprodCategoryColumn();
//			createColumnprodCodeColumn();
			if(showBillType){
				billingTypeCol();
			}
			createColumnprodNameColumn();
			
			/**
			 * @author Anil , Date : 30-07-2019
			 * for purchase we are storing quantity in area field , so for purchase type bill this column should not be visible
			 */
			if(orderType != null && orderType.equals(AppConstants.ORDERTYPESALES)){
				createEditableColumnQty();
			}else if(orderType!=null&&!orderType.equals(AppConstants.ORDERTYPEFORPURCHASE)){
//				createColumnprodQtyColumn();
				/** 
				 * @author Vijay  Date :- 14-08-2020
				 * Des :- for shasha if below process config is active then No of branches column will hide
				 * and Display No of Man power. requirement raised by Rahul.
				 */
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "AddManpowercolumn")){
					viewColumnManPower();
				}
				else{
					createColumnprodQtyColumn();
				}
				
			}
			viewColumnUOM();
			/**
			 * @author Anil , Date : 17-04-2019
			 * hiding area/qty column for order type sale and purchase
			 * raised by sonu for roopson
			 * @author Anil , Date : 30-07-2019
			 * for purchase we are storing quantity in area field , so for purchase type bill this column should be visible
			 */
			if(orderType!=null&&!orderType.equals(AppConstants.ORDERTYPESALES)){
				createEditableColumnArea();
			}
			/**
			 * nidhi 8-08-2018 for map serial no
			 */
			if (LoginPresenter.mapModelSerialNoFlag) {
				createColumnViewProdModelNo();
				createColumnViewProdSerialNo();
			}
			createColumnpriceColumn();

			// createColumnvatColumn();
			// createColumnserviceColumn();
			createViewVatTaxColumn();
			createViewServiceTaxColumn();
			createColumnPercDiscount();
			createViewDiscountAmt();
			createColumntotalAmt();
			createViewColumnBaseBillingAmount();
			createViewFlatDiscount();
			createViewPaymentPercent();
			// createColumnPayableAmt();
			/*
			 * nidhi 18-07-2017 for set field view only
			 */
			createViewColumnPayableAmt();
			/*
			 * end
			 */
			
			/**
			 *  Date : 01/03/2021 Added by - Priyanka 
			 *  Des : Table fields mismatch issue raised by Nitin Sir.
			 */
			createColumnViewHSNCodeColumn();
			createColumnprodCategoryColumn();
			createColumnprodCodeColumn();
			/**
			 * End
			 */
		}
		table.setWidth("auto");
	}

	@Override
	public void applyStyle()
	{

	}
	
	@Override
	protected void initializekeyprovider() {
		
	}

	public double calculateTotalBillAmt()
	{
		System.out.println("Entered in table calculate");
		List<SalesOrderProductLineItem>list=getDataprovider().getList();

		double caltotalamt=0;
		for(int i=0;i<list.size();i++)
		{
			SalesOrderProductLineItem entity=list.get(i);
			
			//**************rohan changes here ***************
			if(entity.getFlatDiscount()==0)
			{
				caltotalamt=caltotalamt+(entity.getBaseBillingAmount()*entity.getPaymentPercent())/100;
			}
			else
			{
				caltotalamt=caltotalamt+((entity.getBaseBillingAmount()-entity.getFlatDiscount())*entity.getPaymentPercent())/100;
			}
		}
		return caltotalamt;

	}
	
	
	
	public double calculateTotalExcludingTax()
	{
		System.out.println("INSIDE CALCULATE TOTAL EXCLUDING TAX ");
		List<SalesOrderProductLineItem>list=getDataprovider().getList();
		double sum=0,priceVal=0;
		
		for(int i=0;i<list.size();i++)
		{
			SalesOrderProductLineItem entity=list.get(i);
			
			System.out.println();
			System.out.println("Product Code     :: "+entity.getProdCode());
			System.out.println("Product Name     :: "+entity.getProdName());
			System.out.println("Product VAT      :: "+entity.getPrduct().getVatTax().getTaxConfigName()+" "+entity.getPrduct().getVatTax().getPercentage()+" IsInclusive "+entity.getPrduct().getVatTax().isInclusive());
			System.out.println("Product S.T.     :: "+entity.getPrduct().getServiceTax().getTaxConfigName()+" "+entity.getPrduct().getServiceTax().getPercentage()+" IsInclusive "+entity.getPrduct().getServiceTax().isInclusive() );
			System.out.println("Product Price    :: "+entity.getBaseBillingAmount());
			System.out.println("Product Discount :: "+entity.getProdPercDiscount());
			System.out.println("Product Disc AMT :: "+entity.getDiscountAmt());
			
			SuperProduct prod=entity.getPrduct();
			/**
			 * Date : 31-07-2017 By Anil
			 * this line is commented because under this condition we calculating inclusive tax on already inclusive calculated amount
			 */
//			double taxAmt=removeAllTaxes(prod,entity.getBaseBillingAmount());
			/**
			 * End
			 */
			double taxAmt=0;
			
			System.out.println("REMOVE ALL TAXES : TAX AMOUNT - "+taxAmt);
			
//			if(entity.getProdPercDiscount()==null){
//				System.out.println("DISCOUNT 0 ");
//				priceVal=entity.getBaseBillingAmount()-taxAmt;
//				System.out.println("SINGLE QTY PRICE : "+priceVal);
//				priceVal=priceVal*entity.getQuantity();
//				System.out.println("QTY MULTIPLIED PRICE : "+priceVal +"  QTY "+entity.getQuantity());
//				sum=sum+priceVal;
//				System.out.println("SUM OF ALL PRODUCT PRICE WITH QTY : "+sum);
//				
//			}
//			if(entity.getProdPercDiscount()!=null){
//				System.out.println("DISCOUNT NOT 0 ");
//				priceVal=entity.getBaseBillingAmount()-taxAmt;
//				System.out.println("SINGLE QTY PRICE : "+priceVal);
//				priceVal=priceVal-(priceVal*entity.getProdPercDiscount()/100);
//				System.out.println("DISCOUNT PRICE : "+priceVal);
//				priceVal=priceVal*entity.getQuantity();
//				System.out.println("QTY MULTIPLIED PRICE : "+priceVal+"  QTY "+entity.getQuantity());
//				sum=sum+priceVal;
//				System.out.println("SUM OF ALL PRODUCT PRICE WITH QTY : "+sum);
//			}
			
	//*****************new code for discount by rohan ***************** 
			if(entity.getFlatDiscount()!=0)
			{
				priceVal=entity.getBaseBillingAmount()-taxAmt;
				priceVal=priceVal-entity.getFlatDiscount();
				
				/**
				 * rohan added this code for billing %  
				 * Date : 10/10/2016
				 */
				
				priceVal =  priceVal * entity.getPaymentPercent()/100;
				/**
				 * ends here 
				 */
				
				sum=sum+priceVal;
				System.out.println("RRRRRRRRRRRRRR flat sum"+sum);
			}
			else
			{
			
				if((entity.getProdPercDiscount()==null && entity.getProdPercDiscount()==0) && (entity.getDiscountAmt()==0)){
				System.out.println("inside both 0 condition");
				priceVal=entity.getBaseBillingAmount()-taxAmt;
				/**
				 * rohan remove following below line because we have remove product qty from price
				 */
//				priceVal=priceVal*entity.getQuantity();
				/**
				 * rohan added this code for billing %  
				 * Date : 10/10/2016
				 */
				
				priceVal = priceVal * entity.getPaymentPercent()/100;
				/**
				 * ends here 
				 */
				sum=sum+priceVal;
				System.out.println("RRRRRRRRRRRRRR sum"+sum);
			}
			
			else if((entity.getProdPercDiscount()!=null && entity.getProdPercDiscount()!=0)&& (entity.getDiscountAmt()!=0)){
				
				System.out.println("inside both not null condition");
				
				priceVal=entity.getBaseBillingAmount()-taxAmt;
//				priceVal=priceVal-(priceVal*entity.getProdPercDiscount()/100);
//				priceVal=priceVal-entity.getDiscountAmt();
				/**
				 * rohan remove following below line because we have remove product qty from price
				 */
//				priceVal=priceVal*entity.getQuantity();
				/**
				 * rohan added this code for billing %  
				 * Date : 10/10/2016
				 */
				
				priceVal =  priceVal * entity.getPaymentPercent()/100;
				/**
				 * ends here 
				 */
				sum=sum+priceVal;
				System.out.println("RRRRRRRRRRRRRR sum"+sum);
			}
			else 
			{
				System.out.println("inside oneof the null condition");
				
					priceVal=entity.getBaseBillingAmount()-taxAmt;
//					if(entity.getProdPercDiscount()!=null && entity.getProdPercDiscount()!=0){
//						System.out.println("inside getPercentageDiscount oneof the null condition");
//					priceVal=priceVal-(priceVal*entity.getProdPercDiscount()/100);
//					}
//					else 
//					{
//						System.out.println("inside getDiscountAmt oneof the null condition");
//					priceVal=priceVal-entity.getDiscountAmt();
//					System.out.println("price value"+priceVal);
//					}
					
					
					/**
					 * rohan remove following below line because we have remove product qty from price
					 */
//					priceVal=priceVal*entity.getQuantity();
					/**
					 * rohan added this code for billing %  
					 * Date : 10/10/2016
					 */
					
					priceVal =  priceVal * entity.getPaymentPercent()/100;
					/**
					 * ends here 
					 */
					System.out.println("sum value "+sum);
					System.out.println("price out side for loop  value"+priceVal);
					sum=sum+priceVal;
					System.out.println("RRRRRRRRRRRRRR sum"+sum);
			}
			}

		}
		return sum;
	}
	
	public double removeAllTaxes(SuperProduct entity,double baseBillPrice)
	{
		double vat = 0,service = 0;
		double tax=0,retrVat=0,retrServ=0;
		if(entity instanceof ServiceProduct)
		{
			System.out.println("");
			System.out.println("*** SERVICE PRODUCT ***");
			ServiceProduct prod=(ServiceProduct) entity;
			if(prod.getServiceTax()!=null&&prod.getServiceTax().isInclusive()==true){
				service=prod.getServiceTax().getPercentage();
				
				System.out.println("Service Tax INCLUSIVE "+service);
			}
			if(prod.getVatTax()!=null&&prod.getVatTax().isInclusive()==true){
				vat=prod.getVatTax().getPercentage();
				System.out.println("Vat Tax INCLUSIVE "+vat);
			}
		}

		if(entity instanceof ItemProduct)
		{
			System.out.println("");
			System.out.println("*** ITEM PRODUCT ***");
			ItemProduct prod=(ItemProduct) entity;
			if(prod.getVatTax()!=null&&prod.getVatTax().isInclusive()==true){
				vat=prod.getVatTax().getPercentage();
				System.out.println("Vat Tax INCLUSIVE "+vat);
			}
			if(prod.getServiceTax()!=null&&prod.getServiceTax().isInclusive()==true){
				service=prod.getServiceTax().getPercentage();
				System.out.println("Service Tax INCLUSIVE "+service);
			}
		}
		
		if(vat!=0&&service==0){
			System.out.println();
			System.out.println("INSIDE VAT NOT 0 & ST 0");
			
			retrVat=baseBillPrice/(1+(vat/100));
			System.out.println("Retrieve VAT : "+retrVat);
			retrVat=baseBillPrice-retrVat;
			
			System.out.println("Retrieve VAT 1: "+retrVat);
		}
		if(service!=0&&vat==0){
			System.out.println();
			System.out.println("INSIDE VAT 0 & ST NOT 0");
			retrServ=baseBillPrice/(1+service/100);
			System.out.println("Retrieve ST : "+retrServ);
			retrServ=baseBillPrice-retrServ;
			
			System.out.println("Retrieve ST 1: "+retrServ);
		}
		if(service!=0&&vat!=0){
			// Here if both are inclusive then first remove service tax and then on that amount
			// calculate vat.
			System.out.println();
			System.out.println("INSIDE VAT NOT 0 & ST NOT 0");
			double removeServiceTax=(baseBillPrice/(1+service/100));
			System.out.println("First remove ST from price : "+removeServiceTax);		
			//double taxPerc=service+vat;
			//retrServ=(entity.getPrice()/(1+taxPerc/100)); //line changed below
			retrServ=(removeServiceTax/(1+vat/100));
			System.out.println("Calculate VAT on Amount :"+retrServ);
			retrServ=baseBillPrice-retrServ;
			
			System.out.println("Deducting VAT on Amount :"+retrServ);
		}
		tax=retrVat+retrServ;
		System.out.println();
		System.out.println("TAX -- "+tax);
		return tax;
	}
	
	/**
	 * Date : 20-11-2017 By ANIL
	 * made price and area column editable
	 */
	
	protected void createEditableColumnArea(){
		EditTextCell editCell=new EditTextCell();
		editAreaColumn=new Column<SalesOrderProductLineItem,String>(editCell){
			@Override
			public String getValue(SalesOrderProductLineItem object){
				if(object.getArea()!=null&&!object.getArea().equals("")){
					if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "AddManpowercolumn")){
						if(object.getArea().equalsIgnoreCase("na")){
							return 0+"";
						}
					}
					
					return object.getArea();
					
				}else{
					if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "AddManpowercolumn")){
						return 0+"";
					}
					else{
						return "NA";
					}
				}
			}
			
			/**
			 * @author Anil @since 27-03-2021
			 */
			@Override
			public void onBrowserEvent(Context context, Element elem,SalesOrderProductLineItem object, NativeEvent event) {
				if(enableSiteLocation&&object.getBillType()!=null&&!object.getBillType().equals("")
						&&(object.getBillType().equals("Staffing")||object.getBillType().equals("Overtime")
						||object.getBillType().equals("Labour")||object.getBillType().equals("Public Holiday")
						||object.getBillType().equals("National Holiday")||object.getBillType().equals("Holiday"))){
//					super.onBrowserEvent(context, elem, object, event);
				}else{
					super.onBrowserEvent(context, elem, object, event);
				}
			}
		};
		
		
		
//		/** 
//		 * @author Vijay  Date :- 14-08-2020
//		 * Des :- for shasha if below process config is active then Qty column Name replaced with Man Days.
//		 */
//		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "AddManpowercolumn")){
//			table.addColumn(editAreaColumn,"#Man Days");  
//		}
//		else{
//			table.addColumn(editAreaColumn,"#Qty");  //Added by Ashwini
//		}
			
		table.addColumn(editAreaColumn,"#Qty");  //Added by Ashwini
		table.setColumnWidth(editAreaColumn, 90, Unit.PX);
		
		editAreaColumn.setFieldUpdater(new FieldUpdater<SalesOrderProductLineItem, String>() {
			@Override
			public void update(int index, SalesOrderProductLineItem object,String value) {
				try{
					if(value.equals("NA") ){
						object.setArea(value);
					}else{
						Double val1=Double.parseDouble(value.trim());
						object.setArea(value);
						/**
						 * nidhi
						 * 24-09-2018
						 */
						double totalPrice=object.getPrice();
						
						if(!LoginPresenter.areaWiseCalRestricFlg)
						{
							totalPrice=val1*object.getPrice();
						}
						/**
						 * end
						 */
						object.setBaseBillingAmount(totalPrice);
						object.setTotalAmount(totalPrice);
						object.setBasePaymentAmount(totalPrice-object.getFlatDiscount());
						
					}
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				}
				catch (NumberFormatException e){
					e.printStackTrace();
					GWTCAlert alert=new GWTCAlert();
					object.setArea("NA");
					alert.alert("Only numeric value is allowed!");
				}
				table.redrawRow(index);
			}
		});
	}
	
	protected void createEditableColumnPrice(){
		EditTextCell editCell=new EditTextCell();
		editPriceColumn=new Column<SalesOrderProductLineItem,String>(editCell){
			@Override
			public String getValue(SalesOrderProductLineItem object){
				return nf.format(object.getPrice());
			}
			
			@Override
			public void onBrowserEvent(Context context, Element elem,SalesOrderProductLineItem object, NativeEvent event) {
				if(enableSiteLocation&&object.getBillType()!=null&&!object.getBillType().equals("")
						&&(object.getBillType().equals("Staffing")||object.getBillType().equals("Overtime")
						||object.getBillType().equals("Labour")||object.getBillType().equals("Public Holiday")
						||object.getBillType().equals("National Holiday")||object.getBillType().equals("Holiday"))){
//					super.onBrowserEvent(context, elem, object, event);
				}else{
					super.onBrowserEvent(context, elem, object, event);
				}
			}
		};
		table.addColumn(editPriceColumn,"#Price");
		table.setColumnWidth(editPriceColumn, 90, Unit.PX);
		
		editPriceColumn.setFieldUpdater(new FieldUpdater<SalesOrderProductLineItem, String>() {
			@Override
			public void update(int index, SalesOrderProductLineItem object,String value) {
				try{
					Double val1=Double.parseDouble(value.trim());
					object.setPrice(val1);
					double totalPrice=val1;
					/**
					 * nidhi
					 * 21-09-2018
					 */
					if(!object.getArea().equalsIgnoreCase("NA") && !LoginPresenter.areaWiseCalRestricFlg){
						double area=Double.parseDouble(object.getArea().trim());
						totalPrice=val1*area;
					}else{
						double qty= object.getQuantity();
						totalPrice = qty * val1;
					}
					
					object.setBaseBillingAmount(totalPrice);
					object.setTotalAmount(totalPrice);
					object.setBasePaymentAmount(totalPrice-object.getFlatDiscount());
					
					
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				}
				catch (NumberFormatException e){
					e.printStackTrace();
					GWTCAlert alert=new GWTCAlert();
					object.setArea("NA");
					alert.alert("Only numeric value is allowed!");
				}
				table.redrawRow(index);
			}
		});
	}
	
	protected void createEditableColumnQty(){
		EditTextCell editCell=new EditTextCell();
		editQtyColumn=new Column<SalesOrderProductLineItem,String>(editCell){
			@Override
			public String getValue(SalesOrderProductLineItem object){
				return nf.format(object.getQuantity());
			}
			
			/**
			 * @author Anil @since 27-03-2021
			 */
			@Override
			public void onBrowserEvent(Context context, Element elem,SalesOrderProductLineItem object, NativeEvent event) {
				if(enableSiteLocation&&object.getBillType()!=null&&!object.getBillType().equals("")
						&&(object.getBillType().equals("Staffing")||object.getBillType().equals("Overtime")
						||object.getBillType().equals("Labour")||object.getBillType().equals("Public Holiday")
						||object.getBillType().equals("National Holiday")||object.getBillType().equals("Holiday"))){
//					super.onBrowserEvent(context, elem, object, event);
				}else{
					super.onBrowserEvent(context, elem, object, event);
				}
			}
		};
//		/** 
//		 * @author Vijay  Date :- 14-08-2020
//		 * Des :- for shasha if below process config is active then Qty column Name replaced with Man Days.
//		 */
//		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "AddManpowercolumn")){
//			table.addColumn(editQtyColumn,"#Man Days");  
//		}
//		else{
			table.addColumn(editQtyColumn,"#Qty");
//		}
		table.setColumnWidth(editQtyColumn, 100, Unit.PX);
		
		editQtyColumn.setFieldUpdater(new FieldUpdater<SalesOrderProductLineItem, String>() {
			@Override
			public void update(int index, SalesOrderProductLineItem object,String value) {
				try{
					Double qty=Double.parseDouble(value.trim());
					if(orderType.equals(AppConstants.ORDERTYPESERVICE)){
						GWTCAlert alert=new GWTCAlert();
						alert.alert("This functionality is not available for service order bill.");
					}else if(orderType.equals(AppConstants.ORDERTYPESALES)){
						object.setQuantity(qty);
						double totalPrice = object.getQuantity() * object.getPrice();
						object.setBaseBillingAmount(totalPrice);
						object.setTotalAmount(totalPrice);
						object.setBasePaymentAmount(totalPrice-object.getFlatDiscount());
					}else if(orderType.equals(AppConstants.ORDERTYPEPURCHASE)){
						object.setQuantity(qty);
					}
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				}
				catch (NumberFormatException e){
					e.printStackTrace();
					GWTCAlert alert=new GWTCAlert();
					object.setArea("NA");
					alert.alert("Only numeric value is allowed!");
				}
				table.redrawRow(index);
			}
		});
	}
	
	private void getEditableProductNameCol() {
		EditTextCell editCell=new EditTextCell();
		getEditableProductNameCol=new Column<SalesOrderProductLineItem,String>(editCell){
			@Override
			public String getValue(SalesOrderProductLineItem object)
			{
				
				if(object.getProdName()!=null && !object.getProdName().equals("")){
					return object.getProdName();
				}
				else{
					return "";
				}
			}
		};
		table.addColumn(getEditableProductNameCol,"#Name");
		table.setColumnWidth(getEditableProductNameCol, 100,Unit.PX);
		
		getEditableProductNameCol.setFieldUpdater(new FieldUpdater<SalesOrderProductLineItem, String>() {

			@Override
			public void update(int index, SalesOrderProductLineItem object,String value) {
				object.setProdName(value);
				table.redraw();
			}
		});
	}
	
	
	/**
	 * nidhi
	 * 9-06-2018
	 * for details
	 */
	
	protected void getDetailsButtonColumn() {
		ButtonCell btnCell = new ButtonCell();
		getDetailsButtonColumn = new Column<SalesOrderProductLineItem, String>(btnCell) {
			@Override
			public String getValue(SalesOrderProductLineItem object) {
				return "Product Details";
			}
		};
		table.addColumn(getDetailsButtonColumn, "Product Details");
		table.setColumnWidth(getDetailsButtonColumn, 100, Unit.PX);
	}

	protected void createFieldUpdaterOnBranchesColumn() {
		getDetailsButtonColumn.setFieldUpdater(new FieldUpdater<SalesOrderProductLineItem, String>() {
			@Override
			public void update(int index, SalesOrderProductLineItem object,String value) {
				rowIndex = index;
				itemDtPopUp.showPopUp();//(itemDtPopUp.getTaDetails().getValue() != null) ? itemDtPopUp.getTaDetails().getValue() : itemDtPopUp.getTaDetails().getValue());
				itemDtPopUp.getTaDetails().setValue(object.getProdDesc1());
				itemDtPopUp.getTaDetails1().setValue(object.getProdDesc2());
				
			}
		});
	}
	@Override
	public void onClick(ClickEvent event) {


		if(event.getSource().equals(itemDtPopUp.getLblOk())){//(itemDtPopUp.getTaDetails().getValue()) ? itemDtPopUp.getTaDetails().getValue() : "");
			getDataprovider().getList().get(rowIndex).setProdDesc1((itemDtPopUp.getTaDetails().getValue()!= null) ? itemDtPopUp.getTaDetails().getValue() : "");
			getDataprovider().getList().get(rowIndex).setProdDesc2((itemDtPopUp.getTaDetails1().getValue()!= null) ? itemDtPopUp.getTaDetails1().getValue() : "");
			itemDtPopUp.hidePopUp();
		}else if(event.getSource().equals(itemDtPopUp.getLblCancel())){
			itemDtPopUp.hidePopUp();
		}
		
		/**
		 * nidhi
		 * 22-08-2018
		 */
		if(event.getSource() == prodSerialPopup.getLblOk()){
			ArrayList<ProductSerialNoMapping> proList = new ArrayList<ProductSerialNoMapping>();
//			proList.addAll(prodSerialPopup.getProSerNoTable().getDataprovider().getList());
			List<ProductSerialNoMapping> mainproList = prodSerialPopup.getProSerNoTable().getDataprovider().getList();
			for(int i =0 ; i <mainproList.size();i++){
				System.out.println(mainproList.get(i).getProSerialNo() + "  --- get serial number -- ");
				if(mainproList.get(i).getProSerialNo().trim().length()>0 && mainproList.get(i).isStatus()){
					proList.add(mainproList.get(i));
				}
			}
			
			ArrayList<SalesOrderProductLineItem> list=new ArrayList<SalesOrderProductLineItem>();
			list.addAll(getDataprovider().getList());
			
			list.get(rowIndex).getProSerialNoDetails().clear();
			list.get(rowIndex).getProSerialNoDetails().put(0, proList);
			getDataprovider().getList().clear();
			getDataprovider().getList().addAll(list);
			prodSerialPopup.hidePopUp();
		}
		if(event.getSource()== prodSerialPopup.getLblCancel()){
			prodSerialPopup.hidePopUp();
		}
		/** date 15.4.2019 added by komal  to save amount service branchwise **/ 
		if(event.getSource().equals(serviceBranchesPopup.getLblOk())){
			List<BranchWiseScheduling> brancheslist = serviceBranchesPopup.getServiceBranchesTable().getDataprovider().getList();
			ArrayList<BranchWiseScheduling> customerBrancheslist = new ArrayList<BranchWiseScheduling>();
			customerBrancheslist.addAll(brancheslist);
			Console.log("branch list size :" + customerBrancheslist.size());
			double totalAmount = 0;
			boolean flag = true;
			Console.log("sr number 1 567:"+ getDataprovider().getList().get(rowIndex).getProductSrNumber());
			if(customerBrancheslist!=null && customerBrancheslist.size()>0){
				for(BranchWiseScheduling branchWiseScheduling : customerBrancheslist){
					if(branchWiseScheduling.isCheck()){
						totalAmount += branchWiseScheduling.getArea();
					}
				}
			String value =  nf.format(getDataprovider().getList().get(rowIndex).getBasePaymentAmount());	
				Console.log("value :"+value);
				Console.log("value 1 :"+ Double.parseDouble(value));
				Console.log("total :" + totalAmount);
				if(totalAmount !=   Double.parseDouble(value)){
					GWTCAlert alert = new GWTCAlert();
					alert.alert("Service branches total amount should be equal to product payable amount.");
					flag = false;
				}
			}
			Console.log("flag :"+ flag + " "+getDataprovider().getList().get(rowIndex).getServiceBranchesInfo());
			if(flag){
			  if(getDataprovider().getList().get(rowIndex).getServiceBranchesInfo()!=null){
				if(getDataprovider().getList().get(rowIndex).getServiceBranchesInfo().containsKey(getDataprovider().getList().get(rowIndex).getProductSrNumber())){
					Integer productSrNo = getDataprovider().getList().get(rowIndex).getProductSrNumber();
					Console.log("sr number 1 :"+ productSrNo);
					getDataprovider().getList().get(rowIndex).getServiceBranchesInfo().put(productSrNo, customerBrancheslist);
				}else{
					Integer productSrNo = getDataprovider().getList().get(rowIndex).getProductSrNumber();
					Console.log("sr number 2 :"+ productSrNo);
					getDataprovider().getList().get(rowIndex).getServiceBranchesInfo().put(productSrNo, customerBrancheslist);
				}
			  }
			}else{
				ArrayList<BranchWiseScheduling> customerBrancheslist1 = new ArrayList<BranchWiseScheduling>();
				for(BranchWiseScheduling branch : customerBrancheslist){
					branch.setArea(0);
					branch.setCheck(false);
					customerBrancheslist1.add(branch);
				}
				if(getDataprovider().getList().get(rowIndex).getServiceBranchesInfo()!=null){
					if(getDataprovider().getList().get(rowIndex).getServiceBranchesInfo().containsKey(getDataprovider().getList().get(rowIndex).getProductSrNumber())){
						Integer productSrNo = getDataprovider().getList().get(rowIndex).getProductSrNumber();
						getDataprovider().getList().get(rowIndex).getServiceBranchesInfo().put(productSrNo, customerBrancheslist1);
					}
				  }
			}
			
			/**
			 * ends here
			 */
			
			table.redraw();
	
			serviceBranchesPopup.hidePopUp();
		}
		if(event.getSource().equals(serviceBranchesPopup.getLblCancel())){
			serviceBranchesPopup.hidePopUp();
		}
	}

	

	private void createColumnProdModelNo() {
		EditTextCell editCell=new EditTextCell();
		getProModelNo=new Column<SalesOrderProductLineItem,String>(editCell)
				{
			@Override
			public String getValue(SalesOrderProductLineItem object)
			{
				if(object.getProModelNo()!=null){
					return object.getProModelNo()+"";}
				else{
					return "";
					}
			}
			};
			table.addColumn(getProModelNo,"#Model No");	
			table.setColumnWidth(getProModelNo, 100,Unit.PX);
	}
	
	private void createColumnProdSerialNo() {
		EditTextCell editCell=new EditTextCell();
		getProSerialNo=new Column<SalesOrderProductLineItem,String>(editCell)
				{
			@Override
			public String getValue(SalesOrderProductLineItem object)
			{
				if(object.getProSerialNo()!=null){
					return object.getProSerialNo()+"";}
				else{
					return "";
					}
			}
			};
			table.addColumn(getProSerialNo,"#Serial No");	
			table.setColumnWidth(getProSerialNo, 100,Unit.PX);
	}
	
	private void createColumnViewProdModelNo() {
		getViewProModelNo=new TextColumn<SalesOrderProductLineItem>()
				{
			@Override
			public String getValue(SalesOrderProductLineItem object)
			{
				if(object.getProModelNo()!=null){
					return object.getProModelNo();
					}
				else{
									
					return "";
					}
			}
				};
				table.addColumn(getViewProModelNo,"#Model No");	
				table.setColumnWidth(getViewProModelNo, 100,Unit.PX);
		
		}
	
	private void createColumnViewProdSerialNo() {
		getViewProSerialNo=new TextColumn<SalesOrderProductLineItem>()
				{
			@Override
			public String getValue(SalesOrderProductLineItem object)
			{
				if(object.getProSerialNo()!=null){
					return object.getProSerialNo();
					}
				else{
					return "";
					}
			}
				};
				table.addColumn(getViewProSerialNo,"#Serial No");	
				table.setColumnWidth(getViewProSerialNo, 100,Unit.PX);
		
		}


	private void createFieldUpdaterProModelNoColumn() {
		getProModelNo.setFieldUpdater(new FieldUpdater<SalesOrderProductLineItem, String>()
				{
			@Override

			public void update(int index,SalesOrderProductLineItem object,String value)
			{

				try{
					System.out.println("value"+value);
					object.setProModelNo(value);
				}
				catch (Exception e)
				{

				}
				table.redrawRow(index);
			}
				});
	}
	
	
	private void createFieldUpdateProSerailNo() {
		getProSerialNo.setFieldUpdater(new FieldUpdater<SalesOrderProductLineItem, String>()
				{
			@Override

			public void update(int index,SalesOrderProductLineItem object,String value)
			{

				try{
					System.out.println("value"+value);
					object.setProSerialNo(value);
				}
				catch (Exception e)
				{

				}
				table.redrawRow(index);
			}
				});
	}
	
	/**
	 * nidhi
	 * 29-08-2018
	 * @return
	 */
	public boolean isFlagforserialView() {
		return flagforserialView;
	}
	public void setFlagforserialView(boolean flagforserialView) {
		this.flagforserialView = flagforserialView;
	}
	public void createColumnAddProSerialNoColumn() {
		ButtonCell btnCell = new ButtonCell();
		addProSerialNo = new Column<SalesOrderProductLineItem, String>(btnCell) {
			@Override
			public String getValue(SalesOrderProductLineItem object) {
				return "Product Serial Nos";
			}
		};
		table.addColumn(addProSerialNo, "");
		table.setColumnWidth(addProSerialNo,180,Unit.PX);

	}
	public void createFieldUpdaterAddSerialNo() {
		addProSerialNo.setFieldUpdater(new FieldUpdater<SalesOrderProductLineItem, String>() {
			@Override
			public void update(int index, SalesOrderProductLineItem object, String value) {
				rowIndex = index;
				prodSerialPopup.setProSerialenble(true,true);
				prodSerialPopup.getPopup().setSize("50%", "40%");
				/**
				 * nidhi
				 * 23-08-2018
				 */
				ArrayList<ProductSerialNoMapping> serNo = new ArrayList<ProductSerialNoMapping>();
				System.out.println("size  -- " + serNo.size());
					
					if(object.getProSerialNoDetails().size()>0 ){
						
						if(object.getProSerialNoDetails().containsKey(0)){
							serNo.addAll(object.getProSerialNoDetails().get(0));
						}
						int countpr = 0 ;
						if(serNo != null){
							 countpr = serNo.size();
						}
						
				
						if(countpr< object.getQuantity()){
							
							if(!proSerialList.containsKey(0) || proSerialList.keySet().size() == 0 || proSerialList.get(0) == null){
								proSerialList.put(0,new ArrayList<ProductSerialNoMapping>());
							}else{
								System.out.println("pro called -- " + proSerialList.keySet());
							}
							
								int count = (int) (object.getQuantity()- countpr);
								for(int i = 0 ; i < count ; i ++ ){
									ProductSerialNoMapping pro = new ProductSerialNoMapping();
									pro.setAvailableStatus(false);
									pro.setStatus(false);
									serNo.add(pro);
								}
							
						}
						
					}
					if(object.getProSerialNoDetails().size()==0){
						if(!object.getProSerialNoDetails().containsKey(0)){
							object.getProSerialNoDetails().put(0, new ArrayList<ProductSerialNoMapping>());
						}
						
						for(int i = 0 ; i < object.getQuantity(); i ++ ){
							ProductSerialNoMapping pro = new ProductSerialNoMapping();
							pro.setProSerialNo("");
							pro.setAvailableStatus(false);
							pro.setNewAddNo(true);
							serNo.add(pro);
						}
					}
					prodSerialPopup.getProSerNoTable().getDataprovider().getList().clear();
					prodSerialPopup.getProSerNoTable().getDataprovider().getList().addAll(serNo);
					prodSerialPopup.getProSerNoTable().getTable().redraw();
				prodSerialPopup.showPopUp();
			
			}
		});
	}
	
	public void createFieldUpdaterViewSerialNo() {
		viewProSerialNo.setFieldUpdater(new FieldUpdater<SalesOrderProductLineItem, String>() {
			@Override
			public void update(int index, SalesOrderProductLineItem object, String value) {
				prodSerialPopup.setProSerialenble(false,false);
				prodSerialPopup.getPopup().setSize("50%", "40%");
				rowIndex  = index;
				if(object.getProSerialNoDetails().size()>0){
					
					ArrayList<ProductSerialNoMapping> serNo = new ArrayList<ProductSerialNoMapping>();
					System.out.println("size  -- " + serNo.size());
					
					if(object.getProSerialNoDetails().containsKey(0)){
						serNo.addAll(object.getProSerialNoDetails().get(0));
					}
					
					prodSerialPopup.getProSerNoTable().getDataprovider().getList().clear();
					prodSerialPopup.getProSerNoTable().getDataprovider().getList().addAll(serNo);
					prodSerialPopup.getProSerNoTable().getTable().redraw();
				}
				prodSerialPopup.showPopUp();
			
			}
		});
	}
	
	public void createViewColumnAddProSerialNoColumn() {
		ButtonCell btnCell = new ButtonCell();
		viewProSerialNo = new Column<SalesOrderProductLineItem, String>(btnCell) {
			@Override
			public String getValue(SalesOrderProductLineItem object) {
				return "View Product Serial Nos";
			}
		};
		table.addColumn(viewProSerialNo, "");
		table.setColumnWidth(viewProSerialNo,180,Unit.PX);

	}
	/** date 15.4.2019 added by komal for nbhc service branches button and popup **/
	protected void getServiceBranchesButton() {
		ButtonCell btnCell = new ButtonCell();
		getServiceBranchesButton = new Column<SalesOrderProductLineItem, String>(btnCell) {
			@Override
			public String getValue(SalesOrderProductLineItem object) {
				return "Service Branches";
			}
		};
		table.addColumn(getServiceBranchesButton, "Service Branches");
		table.setColumnWidth(getServiceBranchesButton, 100, Unit.PX);
	}
	
	protected void addFieldUpdaterOnServiceBranchesButton() {
		
		getServiceBranchesButton.setFieldUpdater(new FieldUpdater<SalesOrderProductLineItem, String>() {
			@Override
			public void update(final int index, final SalesOrderProductLineItem object,final String value) {
				ArrayList<BranchWiseScheduling> branchList = new ArrayList<BranchWiseScheduling>();
				HashSet<BranchWiseScheduling> branchSet = new HashSet<BranchWiseScheduling>();
				serviceBranchesPopup.showPopUp();
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "ShowAllServiceBranches") ||
						LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN")){
					Map<String, BranchWiseScheduling> branchMap = new HashMap<String, BranchWiseScheduling>();
					HashMap<Integer, ArrayList<BranchWiseScheduling>> map = new HashMap<Integer, ArrayList<BranchWiseScheduling>>();
					if(object.getServiceBranchesInfo() != null && object.getServiceBranchesInfo().size() >0){
						Console.log("serial number :" + object.getProductSrNumber());
						branchList = object.getServiceBranchesInfo().get(object.getProductSrNumber());
						
					}
					for(BranchWiseScheduling b : branchList){
						branchMap.put(b.getServicingBranch(), b);
						branchSet.add(b);
					}
					for(Branch branch : LoginPresenter.customerScreenBranchList){
						BranchWiseScheduling branchWiseScheduling = new BranchWiseScheduling();
						branchWiseScheduling.setBranchName(branch.getBusinessUnitName());
						branchWiseScheduling.setArea(0);
						branchWiseScheduling.setCheck(false);
						if(branchMap.containsKey(branch.getBusinessUnitName())){
							//branchSet.add(branchMap.get(branch.getBusinessUnitName()));
						}else{
							branchSet.add(branchWiseScheduling);
						}
					}
					branchList = new ArrayList<BranchWiseScheduling>(branchSet);
					map = new HashMap<Integer, ArrayList<BranchWiseScheduling>>();
					map.put(object.getProductSrNumber(), branchList);
					object.setServiceBranchesInfo(map);
					Comparator<BranchWiseScheduling> compare = new  Comparator<BranchWiseScheduling>() {
						
						@Override
						public int compare(BranchWiseScheduling e1, BranchWiseScheduling e2) {
							// TODO Auto-generated method stub

							if(e1!=null && e2!=null)
							{
								if( e1.getBranchName()!=null && e2.getBranchName()!=null){
									return e1.getBranchName().compareTo(e2.getBranchName());}
							}
							else{
								return 0;}
							return 0;
						
						}
					};
					Collections.sort(branchList,compare);
					serviceBranchesPopup.getServiceBranchesTable().getDataprovider().setList(branchList);
					rowIndex=index;
				
				}else{
				
				if(object.getServiceBranchesInfo() != null && object.getServiceBranchesInfo().size() >0){
					Console.log("serial number :" + object.getProductSrNumber());
					branchList = object.getServiceBranchesInfo().get(object.getProductSrNumber());
				}
				
				
				if(branchList != null && branchList.size() > 0){
					HashMap<Integer, ArrayList<BranchWiseScheduling>> map = new HashMap<Integer, ArrayList<BranchWiseScheduling>>();
					ArrayList<BranchWiseScheduling> branchList1 = new ArrayList<BranchWiseScheduling>();
					if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "ServiceBranchFromSchedulePopup")){
					//	branchList1.addAll(branchList);
						for(BranchWiseScheduling sch : branchList){
							Console.log("dfg 11" + sch.getServicingBranch());
							if(sch.getBranchName().equalsIgnoreCase("Service Address")){
								sch.setBranchName(sch.getServicingBranch());
								branchSet.add(sch);
								Console.log("dfg 21" + sch.getServicingBranch());
							}else{
								branchSet.add(sch);
								Console.log("dfg 31" + sch.getServicingBranch());
							}
						}
						Console.log("dfg 14");
					}else{
						for(BranchWiseScheduling sch : branchList){
							Console.log("dfg 1" + sch.getBranchName());
							if(sch.getBranchName().equalsIgnoreCase("Service Address")){
								Console.log("dfg 2" + sch.getServicingBranch());
							}else{
								branchSet.add(sch);
								Console.log("dfg 3" + sch.getServicingBranch());
							}
						}
					}
					branchList1 = new ArrayList<BranchWiseScheduling>(branchSet);
					map.put(object.getProductSrNumber(), branchList1);
					object.setServiceBranchesInfo(map);
					Comparator<BranchWiseScheduling> compare = new  Comparator<BranchWiseScheduling>() {
						
						@Override
						public int compare(BranchWiseScheduling e1, BranchWiseScheduling e2) {
							// TODO Auto-generated method stub

							if(e1!=null && e2!=null)
							{
								if( e1.getBranchName()!=null && e2.getBranchName()!=null){
									return e1.getBranchName().compareTo(e2.getBranchName());}
							}
							else{
								return 0;}
							return 0;
						
						}
					};
					Collections.sort(branchList1,compare);
					serviceBranchesPopup.getServiceBranchesTable().getDataprovider().setList(branchList1);
					rowIndex=index;
				}else{
					GenricServiceAsync service = GWT.create(GenricService.class);
					MyQuerry query = new MyQuerry();
					Filter filter = new Filter();
					filter.setQuerryString("count");
					filter.setIntValue(object.getOrderId());
					query.getFilters().add(filter);
					query.setQuerryObject(new Contract());
					service.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {

						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							
						}

						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							// TODO Auto-generated method stub
							if(result != null){
								
							Contract contract = (Contract) result.get(0);
							
//							Contract contract =ofy().load().type(Contract.class).filter("companyId", UserConfiguration.getCompanyId()).filter("count", this.getContractCount()).first().now();
							HashMap<Integer, ArrayList<BranchWiseScheduling>> map= new HashMap<Integer, ArrayList<BranchWiseScheduling>>();
							Set<BranchWiseScheduling> branchSet = new HashSet<BranchWiseScheduling>();
							ArrayList<BranchWiseScheduling> branchList = new ArrayList<BranchWiseScheduling>();
							if(contract != null && contract.getItems() != null){
								for(SalesLineItem item :contract.getItems()){
									if(item.getProductSrNo() == object.getProductSrNumber()){
									
									branchSet = new HashSet<BranchWiseScheduling>();
									if(item.getCustomerBranchSchedulingInfo() != null && item.getCustomerBranchSchedulingInfo().size() > 0){
										for(BranchWiseScheduling branch : item.getCustomerBranchSchedulingInfo().get(item.getProductSrNo())){
											if(branch.isCheck()){
												Console.log("dfg 311" + branch.getBranchName());
												if(!branch.getBranchName().equals("Service Address")){
													BranchWiseScheduling branchWiseScheduling = new BranchWiseScheduling();
													branchWiseScheduling.setBranchName(branch.getServicingBranch());
													branchWiseScheduling.setArea(0);
													branchWiseScheduling.setCheck(false);
													branchSet.add(branchWiseScheduling);
												
												}else{						
//														BranchWiseScheduling branchWiseScheduling = new BranchWiseScheduling();
//														branchWiseScheduling.setBranchName(contract.getBranch());
//														branchWiseScheduling.setArea(0);
//														branchWiseScheduling.setCheck(false);
//														branchSet.add(branchWiseScheduling);
													if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "ServiceBranchFromSchedulePopup")){
														BranchWiseScheduling branchWiseScheduling1 = new BranchWiseScheduling();
														branchWiseScheduling1.setBranchName(branch.getServicingBranch());
														branchWiseScheduling1.setArea(0);
														branchWiseScheduling1.setCheck(false);
														branchSet.add(branchWiseScheduling1);
													}
												}
											}
											
										}
									}
									branchList = new ArrayList<BranchWiseScheduling>(branchSet);
									if(branchList != null && branchList.size() > 0){
										map.put(item.getProductSrNo(), branchList);
									}
									break;
								}
								}
								
							}
							
							if(map.size() > 0 && map.containsKey(object.getProductSrNumber())){
								object.setServiceBranchesInfo(map);
							}else{
								for(Branch branch : LoginPresenter.customerScreenBranchList){
									BranchWiseScheduling branchWiseScheduling = new BranchWiseScheduling();
									branchWiseScheduling.setBranchName(branch.getBusinessUnitName());
									branchWiseScheduling.setArea(0);
									branchWiseScheduling.setCheck(false);
									branchList.add(branchWiseScheduling);
								}
								map = new HashMap<Integer, ArrayList<BranchWiseScheduling>>();
								map.put(object.getProductSrNumber(), branchList);
								object.setServiceBranchesInfo(map);
							}
						
								Comparator<BranchWiseScheduling> compare = new  Comparator<BranchWiseScheduling>() {
									
									@Override
									public int compare(BranchWiseScheduling e1, BranchWiseScheduling e2) {
										// TODO Auto-generated method stub

										if(e1!=null && e2!=null)
										{
											if( e1.getBranchName()!=null && e2.getBranchName()!=null){
												return e1.getBranchName().compareTo(e2.getBranchName());}
										}
										else{
											return 0;}
										return 0;
									
									}
								};
								Collections.sort(branchList,compare);
								serviceBranchesPopup.getServiceBranchesTable().getDataprovider().setList(branchList);
								rowIndex=index;
							
						}
					}
						
					});
				}
				
				
			}
		}
		});
	
	}
	
	private void getEditableProductSrNoCol() {
		EditTextCell editCell=new EditTextCell();
		getEditableProductSrNoCol=new Column<SalesOrderProductLineItem,String>(editCell){
			@Override
			public String getValue(SalesOrderProductLineItem object)
			{
				return object.getProductSrNumber()+"";
			}
		};
		table.addColumn(getEditableProductSrNoCol,"#Sr No");
		table.setColumnWidth(getEditableProductSrNoCol, 100,Unit.PX);
		
		getEditableProductSrNoCol.setFieldUpdater(new FieldUpdater<SalesOrderProductLineItem, String>() {
 
			@Override
			public void update(int index, SalesOrderProductLineItem object,String value) {
				int srNumber = 0;
				try{
					srNumber = Integer.parseInt(value);
				}catch(Exception e){
					
				}
				object.setProductSrNumber(srNumber);
				table.redraw();
			}
		});
	}
	private void createColumnViewProductSrNoCol() {
		getViewProductSrNoCol=new TextColumn<SalesOrderProductLineItem>()
				{
			@Override
			public String getValue(SalesOrderProductLineItem object)
			{
				
					return object.getProductSrNumber()+"";
				
			}
				};
				table.addColumn(getViewProductSrNoCol,"Sr No");	
				table.setColumnWidth(getViewProductSrNoCol, 100,Unit.PX);
		
		}
	
	
	private void createColumnManPower() {
		EditTextCell editcell = new EditTextCell();
		manpowerColumn = new Column<SalesOrderProductLineItem, String>(editcell) {
			
			@Override
			public String getValue(SalesOrderProductLineItem object) {
				if(object.getManpower()!=null && !object.getManpower().equals("")){
					return object.getManpower();
				}
				return 0+"";
			}
		};
		table.addColumn(manpowerColumn, "#No.of Staff");
		table.setColumnWidth(manpowerColumn, 90, Unit.PX);
	
	}
	private void viewColumnManPower() {
		viewmanpowercolumn = new TextColumn<SalesOrderProductLineItem>() {
			
			@Override
			public String getValue(SalesOrderProductLineItem object) {
				if(object.getManpower()!=null && !object.getManpower().equals("")){
					return object.getManpower();
				}
				return 0+"";
			}
		};
		
		table.addColumn(viewmanpowercolumn, "#No.of staff");
		table.setColumnWidth(viewmanpowercolumn, 90, Unit.PX);
	
	}
	

	private void addFieldUpdaterManPower() {
		System.out.println("ok here");
		manpowerColumn.setFieldUpdater(new FieldUpdater<SalesOrderProductLineItem, String>() {
			
			@Override
			public void update(int index, SalesOrderProductLineItem object, String value) {
				// TODO Auto-generated method stub
				try{
					System.out.println("in man power"+value);
					object.setManpower(value);
				}
				catch (Exception e)
				{

				}
				table.redrawRow(index);
			}
		});
		
	}
	/**Sheetal : 23-03-2022, Adding Unit of measurement column in product level,requirement by nitin sir**/
	
	private void createColumnUOM() {
		if(UOM != null && UOM.size()>0)
			UOM.clear();
		else{
			UOM = new ArrayList<String>();
		}
		
		for(Config cfg : LoginPresenter.unitForProduct){
			UOM.add(cfg.getName());
		}
		SelectionCell UOMselection = new SelectionCell(UOM);
		UOMcolumn = new Column<SalesOrderProductLineItem, String>(UOMselection){

			@Override
			public String getValue(SalesOrderProductLineItem object) {
				if(object.getUnitOfMeasurement()!=null&&!object.getUnitOfMeasurement().equals("")){
					return object.getUnitOfMeasurement();
				}else{
					return "NA";
				}
			}
		};
		table.addColumn(UOMcolumn, "#UOM");
		table.setColumnWidth(UOMcolumn, 90, Unit.PX);
		
	}
	
	private void viewColumnUOM() {
		viewUOMcolumn=new  TextColumn<SalesOrderProductLineItem>(){

			@Override
			public String getValue(SalesOrderProductLineItem object) 
			    {
				return object.getUnitOfMeasurement();
				}
			};
			table.addColumn(viewUOMcolumn,"#UOM");
			table.setColumnWidth(viewUOMcolumn, 90,Unit.PX);
		
	}
	
	private void UpdateUOMColumn() {
		UOMcolumn.setFieldUpdater(new FieldUpdater<SalesOrderProductLineItem, String>()
				{

					@Override
					public void update(int index, SalesOrderProductLineItem object, String value) {
						try{
							String val1 =  (value.trim());
							object.setUnitOfMeasurement(val1);
						}catch(Exception e){
							
						}
						table.redrawRow(index);
					}
				});
		
	}
	/**end**/
	
	protected void createFieldUpdaterdeleteColumn()
	{
		deleteColumn.setFieldUpdater(new FieldUpdater<SalesOrderProductLineItem,String>()
				{
			@Override
			public void update(int index,SalesOrderProductLineItem object,String value)
			{
				
				boolean addDeleteProductInInvoiceFlag = AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", AppConstants.PC_ENABLEADDDELETESALESORDERPRODUCTSONINVOICE);
				Console.log("addDeleteProductInInvoiceFlag "+addDeleteProductInInvoiceFlag);
				Console.log("object.getTypeOfOrder() "+object.getTypeOfOrder());
				if(addDeleteProductInInvoiceFlag && object.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESALES)){
					getDataprovider().getList().remove(object);
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				}
				else{
					
					try {
					 	object.setBasePaymentAmount(0);
					 	object.setPayableamtupdateFlag(true);
						
						double paypercent =0;
					
						object.setPaymentPercent(paypercent);
						
						GWTCAlert alert = new GWTCAlert(); 
						alert.alert("New billing document will create amount of "+object.getBaseBillingAmount() + " when you approve the invoice");
						object.setFlatDiscount(0);

						RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
						
					} catch (Exception e) {
						// TODO: handle exception
					}
				}
				table.redrawRow(index);
				setEnable(true);

				
			}
			
			
		});
	}
	
	}
