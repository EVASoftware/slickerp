package com.slicktechnologies.client.views.paymentinfo.billingdetails;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.view.client.NoSelectionModel;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.approvalutility.ApprovableFormScreenPresenter;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.AutoInvoiceService;
import com.slicktechnologies.client.services.AutoInvoiceServiceAsync;
import com.slicktechnologies.client.services.CommunicationLogService;
import com.slicktechnologies.client.services.CommunicationLogServiceAsync;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.DocumentUploadService;
import com.slicktechnologies.client.services.DocumentUploadServiceAsync;
import com.slicktechnologies.client.services.EmailService;
import com.slicktechnologies.client.services.EmailServiceAsync;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.services.ServiceListService;
import com.slicktechnologies.client.services.ServiceListServiceAsync;
import com.slicktechnologies.client.services.TaxInvoiceService;
import com.slicktechnologies.client.services.TaxInvoiceServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.ApproverFactory;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.cnc.CNCForm;
import com.slicktechnologies.client.views.cnc.CNCPresenter;
import com.slicktechnologies.client.views.contract.ContractForm;
import com.slicktechnologies.client.views.contract.ContractPresenter;
import com.slicktechnologies.client.views.device.DeviceForm;
import com.slicktechnologies.client.views.device.DevicePresenter;
import com.slicktechnologies.client.views.documentcancellation.DocumentCancellationPopUp;
import com.slicktechnologies.client.views.inventory.grnlist.GRNListPresenter;
import com.slicktechnologies.client.views.inventory.recievingnote.GRNForm;
import com.slicktechnologies.client.views.inventory.recievingnote.GRNPresenter;
import com.slicktechnologies.client.views.paymentinfo.billinglist.BillingListPresenter;
import com.slicktechnologies.client.views.paymentinfo.invoicedetails.InvoiceDetailsForm;
import com.slicktechnologies.client.views.paymentinfo.invoicedetails.InvoiceDetailsPresenter;
import com.slicktechnologies.client.views.paymentinfo.invoicelist.InvoiceListPresenter;
import com.slicktechnologies.client.views.paymentinfo.paymentdetails.PaymentDetailsForm;
import com.slicktechnologies.client.views.paymentinfo.paymentdetails.PaymentDetailsPresenter;
import com.slicktechnologies.client.views.paymentinfo.paymentlist.PaymentListPresenter;
import com.slicktechnologies.client.views.paymentinfo.vendorinvoicedetails.VendorInvoiceDetailsForm;
import com.slicktechnologies.client.views.paymentinfo.vendorinvoicedetails.VendorInvoiceDetailsPresenter;
import com.slicktechnologies.client.views.paymentinfo.vendorinvoicelist.VendorInvoiceListPresenter;
import com.slicktechnologies.client.views.popups.InvoiceDetailsPopup;
import com.slicktechnologies.client.views.purchase.purchaseorder.PurchaseOrderForm;
import com.slicktechnologies.client.views.purchase.purchaseorder.PurchaseOrderPresenter;
import com.slicktechnologies.client.views.purchase.servicepo.ServicePoForm;
import com.slicktechnologies.client.views.purchase.servicepo.ServicePoPresenter;
import com.slicktechnologies.client.views.salesorder.SalesOrderForm;
import com.slicktechnologies.client.views.salesorder.SalesOrderPresenter;
import com.slicktechnologies.client.views.service.ServiceForm;
import com.slicktechnologies.client.views.service.ServicePresenter;
import com.slicktechnologies.server.addhocdownload.XlsxWriter;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.OtherTaxCharges;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.TaxDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.ConcreteBusinessProcess;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessprocesslayer.ServicePo;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cnc.CNC;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;
import com.slicktechnologies.shared.common.inventory.GRN;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.productlayer.Tax;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.role.ScreenAuthorization;
import com.slicktechnologies.shared.common.salesorder.OtherCharges;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.BillingDocumentDetails;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.SalesAnnexureType;
import com.slicktechnologies.shared.common.salesprocess.SalesOrderProductLineItem;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;
import com.slicktechnologies.shared.common.salesprocess.VendorInvoice;

public class BillingDetailsPresenter extends ApprovableFormScreenPresenter<BillingDocument> implements RowCountChangeEvent.Handler {

	public BillingDetailsForm form;
	final GenricServiceAsync async=GWT.create(GenricService.class);
	final CsvServiceAsync csvservice=GWT.create(CsvService.class);
	String invoiceType="";
	EmailServiceAsync emailService=GWT.create(EmailService.class);
	NumberFormat nf=NumberFormat.getFormat("0.00");
	DocumentCancellationPopUp popup = new DocumentCancellationPopUp();
	PopupPanel panel ;
	public static boolean multipleContractBillingInvoiceFlag = false;
	int counter=0;
	
	
	FromAndToDateBoxPopup frmAndToPop=new FromAndToDateBoxPopup();
	ArrayList<TaxDetails> vattaxlist;
	ArrayList<TaxDetails> servicetaxlist;	
	int billcounter=0;
	private boolean serviceCompleted;
	ServiceListServiceAsync serviceList = GWT.create(ServiceListService.class);
	GeneralServiceAsync generalAsync = GWT.create(GeneralService.class);
	
	/**
	 * @author Anil ,Date : 30-03-2019
	 */
	InlineLabel lblTaxInvoice;
	
	/*** Date 04-04-2019 by Vijay for NBHC CCPM AutoInvoice ******/
	InvoiceDetailsPopup autoInvoicePopup = new InvoiceDetailsPopup();
	
	TaxInvoiceServiceAsync taxInvoiceService = GWT.create(TaxInvoiceService.class);

	
	public BillingDetailsPresenter(FormScreen<BillingDocument> view, BillingDocument model) {
		super(view, model);
		form=(BillingDetailsForm) view;
		/**
		 * nidhi 14-12-2018 
		 */
		form.parcialBillDisableFlag = AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "DisableParticalBillDocument");
		/** end
		 */
		form.setPresenter(this);
		/**
		 *  below line commented by vijay on 14 Feb 2017 because when multiple billing combine to one billing then when we click on
		 * billing table that details map on form and wrong invoice amount will create when we create its invoice.
		 */
//		setTableSelectionOnBillingTable();
		/**
		 * End here
		 */
		form.getSalesProductTable().getTable().addRowCountChangeHandler(this);
		form.getBillingChargesTable().getTable().addRowCountChangeHandler(this);
		popup.getBtnOk().addClickHandler(this);
		popup.getBtnCancel().addClickHandler(this);
		
		frmAndToPop.btnOne.addClickHandler(this);
		frmAndToPop.btnTwo.addClickHandler(this);
		
		/**
		 * Date : 21-09-2017 BY ANIL
		 * Adding row count change handler on other charges table
		 */
		form.tblOtherCharges.getTable().addRowCountChangeHandler(this);
		/**
		 * End
		 */
		
		autoInvoicePopup.getLblCancel().addClickHandler(this);
		autoInvoicePopup.getLblOk().addClickHandler(this);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.BILLINGDETAILS,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();
		
		if(text.equals(AppConstants.CREATEPROFORMAINVOICE)){
			/**
			 * @author Komal,Date : 11.04.2019
			 * Multiple invoices were created against single bill
			 * This was happening because of double click on button
			 * raised by Rohan for pecopp
			 */
				lblTaxInvoice=label;
				lblTaxInvoice.setText("Processing...");
				lblTaxInvoice.setVisible(false);
				form.showWaitSymbol();
				reactOnProformaInvoice();
		}
		if (text.equals(AppConstants.CREATETAXINVOICE)) {
			/**
			 * @author Anil,Date : 30-03-2019
			 * Multiple invoices were created against single bill
			 * This was happening because of double click on button
			 * raised by Rohan for pecopp
			 */
			lblTaxInvoice=label;
			lblTaxInvoice.setText("Processing...");
			lblTaxInvoice.setVisible(false);
			
			reactOnInvoice(AppConstants.CREATETAXINVOICE,null,null,null,null);
			
			
//			/**
//			 * Date 13-09-2018 By Vijay
//			 * Des :- added showWaitSymbol for blue screen for user dont allow to click to times on button
//			 */
//			form.showWaitSymbol();
//			/**
//			 * ends here
//			 */
//			serviceCompleted = false;
//			
//			/**
//			 * Date 11-04-2018 By vijay
//			 * For EVA Rate Contract service completion validation message not required As per Nitin sir Instruction
//			 */
//			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "OnlyForEva")){
//				model.setRateContractServiceId(0);
//			}
//			/**
//			 * ends here
//			 */
//			
//			/**
//			 * Date 13-08-2018 Temporary commented by Vijay as per vaishali mam
//			 */
////			/**
////			 * Date : 06-07-2018
////			 * Developer : Vijay
////			 * Des :- For NBHC before to last months billing date not allowed to create Tax invoice branch user
////			 * admin can create 
////			 */
////			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "OnlyForNBHC")){
////				if(!(LoginPresenter.myUserEntity.getRole().getRoleName().trim().equalsIgnoreCase("ADMIN"))){
////					if(form.getDbbillingdate().getValue()!=null){
////						Date lastToLastMonthEndDate= new Date();
////						CalendarUtil.addMonthsToDate(lastToLastMonthEndDate, -1);
////						CalendarUtil.setToFirstDayOfMonth(lastToLastMonthEndDate);
////						CalendarUtil.addDaysToDate(lastToLastMonthEndDate, -1);
////						if(form.getDbbillingdate().getValue().before(lastToLastMonthEndDate) || form.getDbbillingdate().getValue().equals(lastToLastMonthEndDate)){
////							form.showDialogMessage("Can not create invoice Before last month billing Documents!");
////							return;
////						}
////						/**
////						 * Date 10-07-2018 By Vijay 
////						 * Des :- contract end is expired then dont allow related billing documents for approvals
////						 */
////						if(model.getContractEndDate()!=null){
////							Date todayDate = new Date();
////							if(todayDate.after(model.getContractEndDate())){
////								form.showDialogMessage("Contract expired can not proceed");
////								return;
////							}
////						}
////						/**
////						 * ends here
////						 */
////					}
////				}
////			}
//			/**
//			 * ends here
//			 */
//			
//			
//			Console.log("Rate contract Service id =="+model.getRateContractServiceId());
//			try {
//				if (model.getRateContractServiceId() != 0) {
//					final MyQuerry querry = new MyQuerry();
//					Vector<Filter> temp = new Vector<Filter>();
//					Filter filter = null;
//
//					filter = new Filter();
//					filter.setQuerryString("count");
//					filter.setIntValue(model.getRateContractServiceId());
//					temp.add(filter);
//
//					querry.setFilters(temp);
//					querry.setQuerryObject(new Service());
////					form.showWaitSymbol(); //vijay
//					service.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
//
//						@Override
//						public void onFailure(Throwable caught) {
//							// TODO Auto-generated method stub
//							form.hideWaitSymbol();
//							
//							/**
//							 * @author Anil,Date : 30-03-2019
//							 * Multiple invoices were created against single bill
//							 * This was happening because of double click on button
//							 * raised by Rohan for pecopp
//							 */
////									lblTaxInvoice.setVisible(true);
//							lblTaxInvoice.setText("Tax Invoice");
//							
//							/**
//							 * End
//							 */
//						}
//
//						@Override
//						public void onSuccess(ArrayList<SuperModel> result) {
//							
//							// TODO Auto-generated method stub
////									form.hideWaitSymbol(); //vijay
//							if (result.size() != 0) {
//								final Service service = (Service) result.get(0);
//								if (service.getStatus().equalsIgnoreCase(Service.SERVICESTATUSCOMPLETED)) {
//									serviceCompleted = true;
//								}
//								if (serviceCompleted) {
//									reactOnTaxInvoice();
//								}else{
//									form.hideWaitSymbol(); //vijay
//									form.showDialogMessage("Service for this bill is not completed. Please complete it and try again!!");
//								}
//								
//							}else{
//								reactOnTaxInvoice();
//							}
//						}
//
//					});
//
//				}
//				/***
//				 * Date 27-03-2019 by Vijay 
//				 * Des :- NBHC CCPM validation code moved into one method called at billing submit self approval
//				 */
////				/** date 15.03.2018 added by komal for nbhc (if services before billing date are not completed then do not allow to create tax invoice)**/
////				else if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "MakeServiceCompletionMandatoryForAMCContract")){					
////					
////					/***
////					 * Date 27-03-2019 by Vijay 
////					 * Des :- NBHC CCPM validation code moved into one method
////					 */
////					validateTaxInvoice();
////              }
//				else{
//					Console.log("inside else 1234");
//					System.out.println("billing date "  +" "+"Contract id");
//					reactOnTaxInvoice();
//				}
//			} catch (Exception e1) {
//				e1.printStackTrace();
//			}
			
			
			
		}
		if(text.equals(AppConstants.CANCELBILLING)){
			
			popup.getPaymentDate().setValue(new Date()+"");
			popup.getPaymentID().setValue(form.getIbinvoiceid().getValue().trim());
			popup.getPaymentStatus().setValue(form.getstatustextbox().getValue().trim());
			panel=new PopupPanel(true);
			panel.add(popup);
			panel.show();
			panel.center();
		}
		if(text.equals("Request For Approval")){
			/*** Date 13-10-2018 By Vijay For dont allow user to click twice ***/
			form.showWaitSymbol(); 
			/**
			 *  nidhi
			 *  03-march-2018
			 *  for stop allow approval with filled mandatory fields
			 */
			if(form.validateunitandUOM()){
				CheckRateContractServiceStatus("Request For Approval");
			}
			else{
				form.hideWaitSymbol();//Vijay
			}
			/**
			 *  end
			 */
		}
		if(text.equals("Cancel Approval Request")){
			reactOnCancelApproval();
		}
		if(text.equals("Update Taxes")){
			
			panel = new PopupPanel(true);
			panel.add(frmAndToPop);
			panel.show();
			panel.center();
		}
		
		if(text.equals(ManageApprovals.SUBMIT)){
			/*** Date 13-10-2018 By Vijay For dont allow user to click twice ***/
			form.showWaitSymbol();
			/**
			 *  nidhi
			 *  03-march-2018
			 *  for stop allow approval with filled mandatory fields
			 */
			if(form.validateunitandUOM()){
				CheckRateContractServiceStatus(ManageApprovals.SUBMIT);
			}
			else{
				form.hideWaitSymbol();
			}
			/**
			 *  end
			 */
		}
		
		/**
		 * Date : 29-07-2017 By ANIL
		 * 
		 */
		if (text.equals(AppConstants.VIEWINVOICE)) {
			if(form.isPopUpAppMenubar()){
				Console.log("Bill POPUP : View Invoice clicked!!");
				return;
			}
			viewInvoiceDocuments();
		}
		
		/**
		 * Date : 11-10-2017 By ANIL
		 * 
		 */
		if (text.equals(AppConstants.VIEWORDER)) {
			if(form.isPopUpAppMenubar()){
				Console.log("Bill POPUP : View Order clicked!!");
				return;
			}
			viewOrderDocuments();
		}
		
		/**
		 * Date :31-10-2017 BY ANIL
		 * 
		 */
		if(text.equals(AppConstants.VIEWSERVICE)){
			if(form.isPopUpAppMenubar()){
				Console.log("Bill POPUP : View Service clicked!!");
				return;
			}
			reactOnViewService();
		}
		/**
		 * End
		 */
		if(text.equals(AppConstants.VIEWGRN)){
			if(form.isPopUpAppMenubar()){
				Console.log("Billing Details POPUP : View GRN clicked!!");
				return;
			}
			reactOnViewGrn();
		}
		/**
		 * Date 10-04-2019 by Vijay for NBHC CCPM Auto Invoice
		 */
		if(text.equals(AppConstants.SUBMITINVOICE)){
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "EnableAutoInvoice")){
				
				/*** Date 28-03-2019 by Vijay Des :- this validation was at tax invoice now moved here because for NBHC
				 * when billing document submit Invoice then Invoice must create 
				 */
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "MakeServiceCompletionMandatoryForAMCContract")){
					form.hideWaitSymbol();
					reactonValidateService();
				}
			}
		}
		/**
		 * ends here
		 */
		/** 
		 * Date 20-04-2019 by Vijay for NBHC CCPM with process config
		 */
		 /** 
		 * Date 04-08-2021 by Priyanka 
		 */
		if(text.equals(AppConstants.CHANGESTATUSTOCREATED)){
			if(LoginPresenter.myUserEntity.getRole().getRoleName().equalsIgnoreCase("Admin")||LoginPresenter.myUserEntity.getRole().getRoleName().equalsIgnoreCase("Zonal Coordinator")){
				reactonChangeStatus();
			}
		}
		/**@sheetal:14-02-2022
		    Des : Adding view payment button for navigation , requirement raised by nitin sir**/
		if(text.equals(AppConstants.VIEWPAYMENT)){
			
			//Ashwini Patil Date:8-06-2023 to restrict access to payment as per user authorization			
			List<ScreenAuthorization> moduleValidation = null;
			boolean accessPayments=false;
			if(UserConfiguration.getRole()!=null){
				moduleValidation=UserConfiguration.getRole().getAuthorization();
			}
			for(int f=0;f<moduleValidation.size();f++)
			{
				if(moduleValidation.get(f).getScreens().trim().equals("Payment Details")||moduleValidation.get(f).getScreens().trim().equals("Payment List")||moduleValidation.get(f).getScreens().trim().equals("PAYMENTDETAILS")||moduleValidation.get(f).getScreens().trim().equals("PAYMENTLIST")){
					accessPayments=true;
					break;
				}
			}
			if(accessPayments) {
				if(form.isPopUpAppMenubar()){
					Console.log("Bill POPUP : View Payment clicked!!");
					return;
				}
				reactOnViewPayment();			
			}else {
				form.showDialogMessage("You are not authorized to view this screen!");
			}
		}
	}
	
	/**@sheetal:14-02-2022
    Des : Adding view payment button for navigation , requirement raised by nitin sir**/

	private void reactOnViewPayment() {
		final MyQuerry querry=new MyQuerry();
		Vector<Filter> temp=new Vector<Filter>();
		Filter filter=null;
		
		filter=new Filter();
		filter.setQuerryString("invoiceCount");
		filter.setIntValue(model.getInvoiceCount());
		temp.add(filter);
		
		filter=new Filter();
		filter.setQuerryString("accountType");
		filter.setStringValue("AR");
		temp.add(filter);
		
		
		querry.setFilters(temp);
		querry.setQuerryObject(new CustomerPayment());
		form.showWaitSymbol();
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
			}
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				form.hideWaitSymbol();
				if(result.size()==0){
					form.showDialogMessage("No payment document found.");
					return;
				}
				if(result.size()==1){
					final CustomerPayment billDocument=(CustomerPayment) result.get(0);
					final PaymentDetailsForm form=PaymentDetailsPresenter.initalize();
					Timer timer=new Timer() {
						@Override
						public void run() {
							form.updateView(billDocument);
							form.setToViewState();
						}
					};
					timer.schedule(1000);
					Console.log("view payment sheetal 11");
				}else{
					PaymentListPresenter.initalize(querry);
					
					Console.log("view payment sheetal 22");
				}
				Console.log("view payment sheetal 33");
			}
		});
		}

	private void reactOnViewGrn() {


		final MyQuerry querry=new MyQuerry();
		Vector<Filter> temp=new Vector<Filter>();
		Filter filter=null;
		
		filter=new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(Integer.parseInt(model.getRefNumber()));
		temp.add(filter);
	
		
		querry.setFilters(temp);
		querry.setQuerryObject(new GRN());
		form.showWaitSymbol();
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				form.hideWaitSymbol();
				if(result.size()==0){
					form.showDialogMessage("No GRN found.");
					return;
				}
				if(result.size()==1){
					final GRN grn=(GRN) result.get(0);
					final GRNForm form=GRNPresenter.initalize();
					Timer timer=new Timer() {
						@Override
						public void run() {
							form.updateView(grn);
							form.setToViewState();
						}
					};
					timer.schedule(1000);
				}else{
					GRNListPresenter.initalize(querry);
				}
				
			}
		});
		
	
		
	
	}

	private void reactForMultipleContract(final Date serDate){
		
		
		String contractId = form.getIbcontractid().getValue();
		Date billingDate = form.getDbbillingdate().getValue();
		/**
		 *  nidhi
		 *  3-march-2018
		 *  for process configration remaining service
		 */
//						final Date serValidDate = AppUtility.getForProcessConfigurartionIsActiveOrNot("AMCBillDateRestriction");
		
		
		final Date date = CalendarUtil.copyDate(billingDate);
		/** 
		 * end
		 * 
		 */

		
		
		ArrayList<Integer> contractCount = new ArrayList<Integer>();
		HashSet<Integer> countSet = new HashSet<Integer>();
		
		for(BillingDocumentDetails billDt : form.getBillingDocumentTable().getDataprovider().getList()){
			countSet.add(billDt.getOrderId());
		}
		contractCount.addAll(countSet);
		

		if(billingDate != null){
			
			if(serDate==null){
				CalendarUtil.addDaysToDate(billingDate,1);
			}
		}
		
		
			serviceList.checkScheduleServices(contractCount, serDate, billingDate, new AsyncCallback<Integer>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(Integer result) {
				// TODO Auto-generated method stub

					// TODO Auto-generated method stub
					Console.log("result :" + result);
					form.hideWaitSymbol();
					if (result == 0) {
						reactOnTaxInvoice();
					} else {/*
						form.showDialogMessage("All services before billing date "+AppUtility.parseDate(date) +" are not completed. Please complete it and try again!!");
					*/

					/**
					 *  nidhi
					 *   2-04-2018	
					 */
						if(serDate==null){
							form.showDialogMessage("All services before billing date "+AppUtility.parseDate(date) +" are not completed. Please complete it and try again!!");
						}else{
							form.showDialogMessage("All services after "+ AppUtility.parseDate(serDate)+" and  before billing date "+AppUtility.parseDate(date) +" are not completed. Please complete it and try again!!");
						}
						
						
					}								
				
			}
		});
		
		
	
}
	
	private void reactOnViewService() {
		final MyQuerry querry=new MyQuerry();
		Vector<Filter> temp=new Vector<Filter>();
		Filter filter=null;
		
		filter=new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(model.getRateContractServiceId());
		temp.add(filter);
		
		querry.setFilters(temp);
		querry.setQuerryObject(new Service());
		form.showWaitSymbol();
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				form.hideWaitSymbol();
				if(result.size()==0){
					form.showDialogMessage("No service document found.");
					return;
				}
				if(result.size()==1){
					final Service service=(Service) result.get(0);
					final DeviceForm form=DevicePresenter.initalize();
					Timer timer=new Timer() {
						@Override
						public void run() {
							form.updateView(service);
							form.setToViewState();
						}
					};
					timer.schedule(1000);
				}else{
					ServicePresenter.initalize(querry);
				}
				
			}
		});
		
	}

	private void viewOrderDocuments() {
		final MyQuerry querry=new MyQuerry();
		Vector<Filter> temp=new Vector<Filter>();
		Filter filter=null;
		
		filter=new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(model.getContractCount());
		temp.add(filter);
		
		querry.setFilters(temp);
		if(model.getTypeOfOrder().equals(AppConstants.ORDERTYPESERVICE)){
			querry.setQuerryObject(new Contract());
		}else if(model.getTypeOfOrder().equals(AppConstants.ORDERTYPESALES)){
			querry.setQuerryObject(new SalesOrder());
		}else if(model.getTypeOfOrder().equals(AppConstants.ORDERTYPEPURCHASE)){
			querry.setQuerryObject(new PurchaseOrder());
		}else if(model.getTypeOfOrder().equals(AppConstants.ORDERTYPESERVICEPO)){
			querry.setQuerryObject(new ServicePo());
		}
		
		/** date 26.10.2018 added by komal **/
		if(model.getSubBillType() != null && !model.getSubBillType().equals("")){
			if(checkBillSubType(model.getSubBillType())){
				querry.setQuerryObject(new CNC());
			}
		}
		form.showWaitSymbol();
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				form.hideWaitSymbol();
				if(result.size()==0){
					form.showDialogMessage("No order document found.");
					return;
				}
				
				if(model.getTypeOfOrder().equals(AppConstants.ORDERTYPESERVICE)){
					if(model.getSubBillType() != null && !model.getSubBillType().equals("")){
						if(checkBillSubType(model.getSubBillType())){
							final CNC billDocument=(CNC) result.get(0);
							final CNCForm form=CNCPresenter.initalize();
							Timer timer=new Timer() {
								@Override
								public void run() {
									form.updateView(billDocument);
									form.setToViewState();
								}
							};
							timer.schedule(4000);
						}
					}else{
					final Contract billDocument=(Contract) result.get(0);
					final ContractForm form=ContractPresenter.initalize();
					Timer timer=new Timer() {
						@Override
						public void run() {
							form.updateView(billDocument);
							form.setToViewState();
						}
					};
					timer.schedule(1000);
					}
				}else if(model.getTypeOfOrder().equals(AppConstants.ORDERTYPESALES)){
					final SalesOrder billDocument=(SalesOrder) result.get(0);
					final SalesOrderForm form=SalesOrderPresenter.initalize();
					Timer timer=new Timer() {
						@Override
						public void run() {
							form.updateView(billDocument);
							form.setToViewState();
						}
					};
					timer.schedule(1000);
				}else if(model.getTypeOfOrder().equals(AppConstants.ORDERTYPEPURCHASE)){
					final PurchaseOrder billDocument=(PurchaseOrder) result.get(0);
					final PurchaseOrderForm form=PurchaseOrderPresenter.initalize();
					Timer timer=new Timer() {
						@Override
						public void run() {
							form.updateView(billDocument);
							form.setToViewState();
						}
					};
					timer.schedule(1000);
				}/***11-1-2019 added by amol***/
				else if (model.getTypeOfOrder().equals(AppConstants.ORDERTYPESERVICEPO)){
					final ServicePo billDocument=(ServicePo) result.get(0);
					final ServicePoForm form=ServicePoPresenter.initialize();
					Timer timer=new Timer() {
						@Override
						public void run() {
							form.updateView(billDocument);
							form.setToViewState();
						}
					};
					timer.schedule(1000);
				
					
				}
				
				
			}
		});
	}

	private void viewInvoiceDocuments() {
		final MyQuerry querry=new MyQuerry();
		Vector<Filter> temp=new Vector<Filter>();
		Filter filter=null;
		
		filter=new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(model.getInvoiceCount());
		temp.add(filter);
		
		querry.setFilters(temp);
		if(model.getAccountType().equalsIgnoreCase(AppConstants.BILLINGACCOUNTTYPEAP)){
			querry.setQuerryObject(new VendorInvoice());
		}else{
			querry.setQuerryObject(new Invoice());
		}
		form.showWaitSymbol();
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				form.hideWaitSymbol();
				if(result.size()==0){
					form.showDialogMessage("No invoice document found.");
					return;
				}
				if(result.size()==1){
					/** date 25.6.2018 added by komal for vendor invoice form **/
					if(model.getAccountType().equalsIgnoreCase(AppConstants.ACCOUNTTYPEAP)){
						final VendorInvoice billDocument=(VendorInvoice) result.get(0);
						final VendorInvoiceDetailsForm form=VendorInvoiceDetailsPresenter.initalize();
						Timer timer=new Timer() {
							@Override
							public void run() {
								form.updateView(billDocument);
								form.setToViewState();
							}
						};
						timer.schedule(1000);
					}else{
					final Invoice billDocument=(Invoice) result.get(0);
					final InvoiceDetailsForm form=InvoiceDetailsPresenter.initalize();
					Timer timer=new Timer() {
						@Override
						public void run() {
							form.updateView(billDocument);
							form.setToViewState();
						}
					};
					timer.schedule(5000);
					}
				}else{
					/** date 25.6.2018 added by komal for vendor invoice list **/
					if(model.getAccountType().equalsIgnoreCase(AppConstants.ACCOUNTTYPEAP)){
						VendorInvoiceListPresenter.initalize(querry);
					}else{
						InvoiceListPresenter.initalize(querry);
					}
				}
				
			}
		});
		
	}
	private void CheckRateContractServiceStatus(final String btnLabel) {

		String serviceId = form.getTbrateContractServiceid().getValue();
		System.out.println("serviceId =="+serviceId);
		/**
		 * Date:19-07-2017 BY ANIL
		 * added process configuration for making service completion mandatory before approving any bill created for rate contract
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "OnlyForNBHC")
				&&serviceId!=null && !serviceId.equals("")){
			
			MyQuerry querry = new MyQuerry();
			Company c = new Company();
			
			Vector<Filter> filtervec = new Vector<Filter>();
			Filter filter = null;
			
			/**
			 * @author Anil
			 * @since 17-06-2020
			 * For NBHC
			 * for some bills service search query gets failed/return zero result and screen got hang
			 * thats why instead of parsing string value to integer directly passing integer value 
			 */
//			filter = new Filter();
//			filter.setQuerryString("companyId");
//			filter.setLongValue(c.getCompanyId());
//			filtervec.add(filter);
//			
//			filter = new Filter();
//			filter.setQuerryString("count");
//			filter.setIntValue(Integer.parseInt(serviceId));
//			filtervec.add(filter);
			
			filter=new Filter();
			filter.setQuerryString("count");
			filter.setIntValue(model.getRateContractServiceId());
			filtervec.add(filter);

			querry.setFilters(filtervec);
			querry.setQuerryObject(new Service());
			
			async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					Service serviceEntity = (Service) result.get(0);
					String serviceStatus = serviceEntity.getStatus();
					System.out.println("serviceStatus ==="+serviceStatus);
					if(!serviceStatus.equals("Cancelled") && !serviceStatus.equals("Completed")){
						form.showDialogMessage("Please Complete Service first! Service Status is "+serviceStatus);
						form.hideWaitSymbol();
					}else{
						reactOnValidateReqApproval(btnLabel);
					}
						
				}
				
				@Override
				public void onFailure(Throwable caught) {
					form.showDialogMessage("Unexpected Error");
					form.hideWaitSymbol();
				}
			});
			
		}
		else{
			System.out.println("inside else for no service Id");
			reactOnValidateReqApproval(btnLabel);
		}
		
	 
	}
	
	private void getTaxesDetails() {
		final GenricServiceAsync service = GWT.create(GenricService.class);
		MyQuerry query = new MyQuerry();
		Company c=new Company();
		System.out.println("Company Id :: "+c.getCompanyId());
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		query.setFilters(filtervec);
		query.setQuerryObject(new TaxDetails());
		form.showWaitSymbol();
		service.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				
			}
		
			public void onSuccess(ArrayList<SuperModel> result) {
				
				System.out.println("result size of tax "+result);
				servicetaxlist=new ArrayList<TaxDetails>();
				vattaxlist = new ArrayList<TaxDetails>();
				List<TaxDetails> backlist=new ArrayList<TaxDetails>();
				
				for (SuperModel model : result) {
					TaxDetails entity = (TaxDetails) model;
					backlist.add(entity);
					if(entity.getServiceTax().equals(true)){
						servicetaxlist.add(entity);
					}
					if(entity.getVatTax().equals(true)){
						vattaxlist.add(entity);
					}
				}
				System.out.println();
				System.out.println();
				for(int i=0;i<servicetaxlist.size();i++){
					System.out.println(servicetaxlist.get(i).getTaxChargeName()+" "+servicetaxlist.get(i).getTaxChargePercent());
				}
				System.out.println();
				System.out.println();
				for(int i=0;i<vattaxlist.size();i++){
					System.out.println(vattaxlist.get(i).getTaxChargeName()+" "+vattaxlist.get(i).getTaxChargePercent());
				}
				System.out.println();
				System.out.println();
				
				updateTaxes();
				
			}
		});
	}

	private void updateTaxes() {
		Vector<Filter> filtervec=new Vector<Filter>();
		
		MyQuerry querry = new MyQuerry();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(UserConfiguration.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("billingDocumentInfo.billingDate >=");
		filter.setDateValue(frmAndToPop.fromDate.getValue());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("billingDocumentInfo.billingDate <=");
		filter.setDateValue(frmAndToPop.toDate.getValue());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("status");
		filter.setStringValue(BillingDocument.CREATED);
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new BillingDocument());
//		form.showWaitSymbol();
		async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				if (result.size() != 0) {
					billcounter=0;
					for(SuperModel model:result){
						BillingDocument con=(BillingDocument) model;
						
						for(int i=0;i<con.getSalesOrderProducts().size();i++){
							
							if(con.getSalesOrderProducts().get(i).getServiceTax()!=null){
								for(int j=0;j<servicetaxlist.size();j++){
									if(con.getSalesOrderProducts().get(i).getServiceTax().getPercentage()==servicetaxlist.get(j).getTaxChargePercent()){
										con.getSalesOrderProducts().get(i).setServiceTaxEdit(servicetaxlist.get(j).getTaxChargeName());
									}
								}
							}
							
							if(con.getSalesOrderProducts().get(i).getVatTax()!=null){
								for(int j=0;j<vattaxlist.size();j++){
									if(con.getSalesOrderProducts().get(i).getVatTax().getPercentage()==vattaxlist.get(j).getTaxChargePercent()){
										con.getSalesOrderProducts().get(i).setVatTaxEdit(vattaxlist.get(j).getTaxChargeName());
									}
								}
							}
						}
						
						async.save(con,new AsyncCallback<ReturnFromServer>() {
							@Override
							public void onFailure(Throwable caught) {
								form.hideWaitSymbol();
							}
							@Override
							public void onSuccess(ReturnFromServer result) {
								billcounter++;
								form.hideWaitSymbol();
								form.showDialogMessage("Entity updated successfully!");
							}
						});
					}
				}
			}

			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
				form.showDialogMessage("Unexpected Error!");
			}
		});
	}
	
	

	@Override
	public void reactOnPrint() {
		
	}

	@Override
	public void reactOnEmail() {
		
	}
	
	@Override
	public void reactOnDownload() {
		
		final ArrayList<BillingDocument> conbillingarray=new ArrayList<BillingDocument>();
		 List<BillingDocument> listbill=(List<BillingDocument>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
		conbillingarray.addAll(listbill); 
		csvservice.setconbillinglist(conbillingarray, new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);
			}

			@Override
			public void onSuccess(Void result) {
				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "csvservlet"+"?type="+14;
				Window.open(url, "test", "enabled");
				
////				final String url=gwt + "CreateXLXSServlet"+"?type="+14;
////				Window.open(url, "test", "enabled");
//				
//				boolean addOrderNoAndGrnFlag = false;
//				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "AddOrderNoAndGrnNo")){
//					addOrderNoAndGrnFlag = true;
//				}
//				if(addOrderNoAndGrnFlag){
//					ArrayList<SuperModel> superlist = new ArrayList<SuperModel>();
//					superlist.addAll(conbillingarray);
//					csvservice.getData(superlist, AppConstants.BILLINGDETAILS, new AsyncCallback<ArrayList<SuperModel>>() {
//
//						@Override
//						public void onFailure(Throwable caught) {
//							// TODO Auto-generated method stub
//							
//						}
//
//						@Override
//						public void onSuccess(ArrayList<SuperModel> result) {
//							// TODO Auto-generated method stub
//							ArrayList<PurchaseOrder> polist = new ArrayList<PurchaseOrder>();
//							ArrayList<GRN> grnlist = new ArrayList<GRN>();
//
//							for(SuperModel model : result){
//								if(model.getDocumentName().equals(AppConstants.PURCHASEORDER)){
//									PurchaseOrder po = (PurchaseOrder) model;
//									polist.add(po);
//								}
//								else if(model.getDocumentName().equals(AppConstants.GRN)){
//									GRN grn = (GRN) model;
//									grnlist.add(grn);
//								}
//							}
//							downloadData(true,conbillingarray,polist,grnlist);
//
//						}
//					});
//					
//				}
//				else{
//					downloadData(false,conbillingarray,null,null);
//				}
				
			}
		});
	}

	@Override
	protected void makeNewModel() {
		model=new BillingDocument();
	}
	
	public static BillingDetailsForm initalize()
	{
		//AppMemory.getAppMemory().currentState=ScreeenState.NEW;
		BillingDetailsForm form=new  BillingDetailsForm();
		BillingDetailsTableProxy gentable=new BillingDetailsTableProxy();
		gentable.setView(form);
		gentable.applySelectionModle();
		BillingDetailsPresenterSearch.staticSuperTable=gentable;
		BillingDetailsSearchPopUp searchpopup=new BillingDetailsSearchPopUp();
		form.setSearchpopupscreen(searchpopup);
		BillingDetailsPresenter presenter=new BillingDetailsPresenter(form,new BillingDocument());
//		form.setToNewState();
		/**
		 * Date : 29-07-2017 BY ANIL
		 * This line was added to update the name of screen loaded from any other screen or by clicking on billing details on account model
		 */
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Accounts/Billing Details",Screen.BILLINGDETAILS);
		/**
		 * END
		 */
		AppMemory.getAppMemory().stickPnel(form);
		/**
		 * nidhi 14-12-2018 
		 */
		form.parcialBillDisableFlag = AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "DisableParticalBillDocument");
		/** end
		 */
		return form;
	}
	
	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.salesprocess.BillingDocument")
	public static  class BillingDetailsPresenterSearch extends SearchPopUpScreen<BillingDocument>{

		@Override
		public MyQuerry getQuerry() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public boolean validate() {
			// TODO Auto-generated method stub
			return true;
		}};

		@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.salesprocess.BillingDocument")
		public static class BillingDetailsPresenterTable extends SuperTable<BillingDocument> implements GeneratedVariableRefrence{

			@Override
			public Object getVarRef(String varName) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void createTable() {
				// TODO Auto-generated method stub

			}

			@Override
			protected void initializekeyprovider() {
				// TODO Auto-generated method stub
			}

			@Override
			public void addFieldUpdater() {
				// TODO Auto-generated method stub

			}

			@Override
			public void setEnable(boolean state) {
				// TODO Auto-generated method stub

			}

			@Override
			public void applyStyle() {
				// TODO Auto-generated method stub

			}} ;

			@Override
			protected void reactOnGo() {
				super.reactOnGo();
				form.getBillingDocumentTable().connectToLocal();
			}
			

			/**
			 * if proforma invoice is clicked. change status to invoiced and new invoice is created with
			 * invoice type as Proforma Invoice 
			 */
			
			private void reactOnProformaInvoice()
			{
				invoiceType=AppConstants.CREATEPROFORMAINVOICE;
				try {
					createNewInvoice(invoiceType);
				} catch (Exception e) {
					e.printStackTrace();
				}
				/**
				 *  nidhi
				 *  13-01-2018
				 *  stop calling this method from here and traspered it tp create invoice mehtod
				 */
//				changeBillingStatusToInvoiced();
				
				/**
				 *  end
				 */
				
				form.setMenus();
			}
			
			/**
			 * if tax invoice is clicked. change status to invoiced and new invoice is created with
			 * invoice type as Tax Invoice 
			 */
			private void reactOnTaxInvoice()
			{
				invoiceType=AppConstants.CREATETAXINVOICE;
				
//    note  : rohan added invoice type parameter in both method to seperate the two invoices as tax and proforma 
				try {
					createNewInvoice(invoiceType);
				} catch (Exception e) {
					e.printStackTrace();
				}
				/**
				 *  nidhi
				 *  13-01-2018
				 *  stop calling this method from here and traspered it tp create invoice mehtod
				 */
//				changeBillingStatusToInvoiced();
				
				/**
				 *  end
				 */
				
				form.setMenus();
			}

			/**
			 * Creation of New Invoice
			 * @throws Exception
			 */
			
			// this method checks whether billing document made is of multiple orders 
			
			public boolean isMultipleContractBilling(){
				System.out.println("INSIDE CHECKING MULTIPLE CONTRACT BILLING......");
				List<BillingDocumentDetails> billtablelis = form.billingDocumentTable.getDataprovider().getList();
				
				int contractId=billtablelis.get(0).getOrderId();
				for(int i=0;i<billtablelis.size();i++){
					if(billtablelis.get(i).getOrderId()!=contractId){
						return true;
					}
				}
				return false;
			}
			
			
			
			
			
			public void createNewInvoice(final String invoiceType)
			{
				Console.log("BF INVOICE CREATION FLAG : "+form.invCreationFlag);
				/**
				 * @author Anil , Date : 06-09-2019
				 * Checking Invoice Creation Flag to avoid multiple creation of invoice against single bill
				 */
				if(form.invCreationFlag==true){
					lblTaxInvoice.setText("Tax Invoice");
					form.hideWaitSymbol();
					return;
				}
				form.invCreationFlag=true;
				Console.log("AF INVOICE CREATION FLAG : "+form.invCreationFlag);
				
//				form.showWaitSymbol(); //vijay
				
				 /**
				  *  nidhi
				  *  Date : 4-12-2017
				  *  Check process configration for 
				  */
				 boolean confiFlag  =  AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "ContractCategoryAsServiceType");
				 
				 /**
				  * end
				  */
				 
				//final Invoice inventity = new Invoice();
				 /** date25.6.2018 added by komal for vendor invoice**/
				 final Invoice inventity;
				 if(model.getAccountType().equalsIgnoreCase(AppConstants.ACCOUNTTYPEAP)){
					 inventity = new VendorInvoice();
				 }else{
					 inventity = new Invoice();
				 }
				 /** end komal**/

				PersonInfo pinfo = new PersonInfo();
				if (form.getTbpersonCount().getValue() != null) {
					pinfo.setCount(Integer.parseInt(form.getTbpersonCount().getValue()));
				}
				if (form.getTbpersonName().getValue() != null) {
					pinfo.setFullName(form.getTbpersonName().getValue());
				}
				if (form.getTbpersonCell().getValue() != null) {
					pinfo.setCellNumber(Long.parseLong(form.getTbpersonCell().getValue()));
				}
				if (form.getTbpocName().getValue() != null) {
					pinfo.setPocName(form.getTbpocName().getValue());
				}
				if(model.getPersonInfo()!=null&&model.getPersonInfo().getEmail()!=null){
					pinfo.setEmail(model.getPersonInfo().getEmail());
				}

				// ************************rohan made changes here for setting
				// fields()**************

				if (model.getGrossValue() != 0) {
					inventity.setGrossValue(model.getGrossValue());
				}

				if (form.getDopaytermspercent().getValue() != 0) {

					inventity.setTaxPercent(form.getDopaytermspercent().getValue());
				}

				// ***********************************changes ends here
				// *****************************

				if (pinfo != null){
					inventity.setPersonInfo(pinfo);
				}

				/**
				 * nidhi 7-12-2018 for stop null contract id at invoice creation from 
				 * multiple billing
				 */
				if(form.getIbcontractid().getValue()==null || form.getIbcontractid().getValue().toString().trim().length()==0){
					form.showDialogMessage("Order Id should not be blank.!!");
					return ;
				}
				/**
				 * @author Anil , Date : 02-08-2019
				 * Contract count was getting set
				 */
				if (form.getIbcontractid().getValue()!=null&&!form.getIbcontractid().getValue().equals("")){  
					inventity.setContractCount(Integer.parseInt(form.getIbcontractid().getValue().trim()));
				}
				
				if(inventity.getContractCount()==0){
					inventity.setContractCount(model.getContractCount());
				}
				/**
				 * End
				 */
				
				if (model.getContractStartDate() != null) {
					inventity.setContractStartDate(model.getContractStartDate());
				}
				if (model.getContractEndDate() != null) {
					inventity.setContractEndDate(model.getContractEndDate());
				}
				if (form.getDosalesamount().getValue()!=null)
					inventity.setTotalSalesAmount(form.getDosalesamount().getValue());
				
				List<BillingDocumentDetails> billtablelis = form.billingDocumentTable.getDataprovider().getList();
				ArrayList<BillingDocumentDetails> billtablearr = new ArrayList<BillingDocumentDetails>();
				billtablearr.addAll(billtablelis);
				inventity.setArrayBillingDocument(billtablearr);
				
//				System.out.println("MULTIPLE CONTRAT STATUS ::: "+isMultipleContractBilling());
				if(isMultipleContractBilling()){
					inventity.setMultipleOrderBilling(true);
					/**
					 * nidhi
					 * 11*-4-2018
					 * for stop null pointer error
					 */
					/**
					 * @author Anil
					 * @since 13-08-2020
					 * Earlier for multicontract bill we were setting contract count as zero which causes error at the 
					 * time of invoice printing
					 * So On Approval of Nitin sir and vaishali commenting this code
					 */
//					if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "OnlyForOrionPestSolutions")==false){
//						inventity.setContractCount(0);
//					}
					inventity.setRemark(form.getTbremark().getValue());
				}
				
				if (form.getDototalbillamt().getValue() != null)
					inventity.setTotalBillingAmount(form.getDototalbillamt().getValue());
				
				//  rohan added this code for setting disc = 0 and net payable =setTotalBillingAmount
				
				if (form.getDototalbillamt().getValue() != null)
					inventity.setNetPayable(form.getDototalbillamt().getValue());
				
					inventity.setDiscount(0);
				//   ends here 
				
					/**
					 * Date 06-07-2018
					 * Developer :- Vijay
					 * Des :- for NBHC when creating invoice, invoice date must be today Date.
					 * if condition for NHBC and else block for all standard
					 *  
					 */
					if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "OnlyForNBHC")){
						inventity.setInvoiceDate(new Date());
					}else{
						if (form.getDbinvoicedate().getValue() != null)
							inventity.setInvoiceDate(form.getDbinvoicedate().getValue());
					}
					/**
					 * ends here
					 */
					
//				if (form.getDbinvoicedate().getValue() != null)
//					inventity.setInvoiceDate(form.getDbinvoicedate().getValue());
				if (form.getDbpaymentdate().getValue() != null)
					inventity.setPaymentDate(form.getDbpaymentdate().getValue());
				if (form.getOlbApproverName().getValue() != null)
					inventity.setApproverName(form.getOlbApproverName().getValue());
				if (form.getOlbEmployee().getValue() != null)
					inventity.setEmployee(form.getOlbEmployee().getValue());
				if (form.getOlbbranch().getValue() != null)
					inventity.setBranch(form.getOlbbranch().getValue());
				
				inventity.setOrderCreationDate(model.getOrderCreationDate());
				inventity.setCompanyId(model.getCompanyId());
				inventity.setInvoiceAmount(form.getDototalbillamt().getValue());
				inventity.setInvoiceType(invoiceType);
				inventity.setPaymentMethod(model.getPaymentMethod());
				inventity.setAccountType(model.getAccountType());
				inventity.setTypeOfOrder(model.getTypeOfOrder());
				
				
				if(invoiceType.trim().equalsIgnoreCase(AppConstants.CREATETAXINVOICE.trim()))
				{
					/**** Date 04-04-2019 by Vijay for NBHC CCPM AutoInvoice   *******/
					if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "EnableAutoInvoice")){
						inventity.setStatus(Invoice.REQUESTED);
					}
					else{
						inventity.setStatus(Invoice.CREATED);
					}
					
					System.out.println("Rohan in side tax invoice "+AppConstants.CREATETAXINVOICE);
				}
				else
				{
				//   rohan added this status
					System.out.println("Rohan in side proform invoice "+AppConstants.CREATETAXINVOICE);
					inventity.setStatus(BillingDocument.PROFORMAINVOICE);
				}
				
				
				
				
				List<SalesOrderProductLineItem>productlist=form.getSalesProductTable().getDataprovider().getList();
				ArrayList<SalesOrderProductLineItem>prodList=new ArrayList<SalesOrderProductLineItem>();
				prodList.addAll(productlist);
				System.out.println();
				System.out.println("SALES PRODUCT TABLE SIZE ::: "+prodList.size());
				
				/**
				 * Date :22-09-2017 BY ANIL
				 */
				
				List<OtherCharges>OthChargeTbl=form.tblOtherCharges.getDataprovider().getList();
				ArrayList<OtherCharges>ocList=new ArrayList<OtherCharges>();
				ocList.addAll(OthChargeTbl);
				System.out.println("OTHER TAXES TABLE SIZE ::: "+ocList.size());
				
				/**
				 * End
				 */
				
				List<ContractCharges>taxestable=form.getBillingTaxTable().getDataprovider().getList();
				ArrayList<ContractCharges>taxesList=new ArrayList<ContractCharges>();
				taxesList.addAll(taxestable);
				System.out.println("TAXES TABLE SIZE ::: "+taxesList.size());
				
				List<ContractCharges>otherChargesable=form.getBillingChargesTable().getDataprovider().getList();
				ArrayList<ContractCharges>otherchargesList=new ArrayList<ContractCharges>();
				otherchargesList.addAll(otherChargesable);
				System.out.println("OTHER CHARGES TABLE SIZE ::: "+otherchargesList.size());
				/**
				 * Date : 09 Mar 2018
				 * Dev : Rahul Verma
				 * Description : When Partial billing is generated it used to add same table as of billing. Because of partial invoice was creating invoice 
				 */
				ArrayList<SalesOrderProductLineItem> updatedProdList=new ArrayList<SalesOrderProductLineItem>();
				for (SalesOrderProductLineItem prod : prodList) {
					/***
					 * Date 09-04-2019 by Vijay for NBHC CCPM billing base amount storing in invoice with new filed
					 * for revenue loss report
					 */
					if(prod.getBillingDocBaseAmount()==0||prod.getBillingDocBaseAmount()==0.0){
						prod.setBillingDocBaseAmount(prod.getBaseBillingAmount());
					}/*** ends here ****/
					
					prod.setBaseBillingAmount(prod.getBasePaymentAmount());
					prod.setPaymentPercent(100);
					prod.setBasePaymentAmount(prod.getBasePaymentAmount());
					
					updatedProdList.add(prod);
					
				}
				inventity.setSalesOrderProductFromBilling(updatedProdList);
				/**
				 * Ends for Rahul
				 */
//				inventity.setSalesOrderProductFromBilling(prodList);
//				inventity.setSalesOrderProducts(prodList);
				
				/**
				 * Date : 22-09-2017 By ANIL
				 */
				inventity.setOtherCharges(ocList);
				if(form.dbOtherChargesTotal.getValue()!=null){
					inventity.setTotalOtherCharges(form.dbOtherChargesTotal.getValue());
				}
				/**
				 * ENd
				 */
				
				
				inventity.setBillingTaxes(taxesList);
				inventity.setBillingOtherCharges(otherchargesList);
				
				inventity.setCustomerBranch(model.getCustomerBranch());

				inventity.setAccountType(model.getAccountType());
				inventity.setArrPayTerms(model.getArrPayTerms());
				
				if (form.getOlbbillinggroup().getValue() != null) {
					inventity.setInvoiceGroup(form.getOlbbillinggroup().getValue());
				}
				if (model.getOrderCformStatus() != null) {
					inventity.setOrderCformStatus(model.getOrderCformStatus());
				}
				if (model.getOrderCformPercent() != 0&& model.getOrderCformPercent() != -1) {
					inventity.setOrderCformPercent(model.getOrderCformPercent());
				} else {
					inventity.setOrderCformPercent(-1);
				}
				
				/***************** vijay number range *************************/
				if(model.getNumberRange()!=null)
					inventity.setNumberRange(model.getNumberRange());
				/****************************************************/
				
				/**
				 * date 14 Feb 2017
				 * added by vijay for Setting billing period from date and To date in invoice
				 */
				if(form.getDbbillingperiodFromDate().getValue()!=null)
					inventity.setBillingPeroidFromDate(form.getDbbillingperiodFromDate().getValue());
				if(form.getDbbillingperiodToDate().getValue()!=null)
					inventity.setBillingPeroidToDate(form.getDbbillingperiodToDate().getValue());
				/**
				 * End here
				 */
				
				/*
				 *  nidhi
				 *   1-07-2017
				 */
				if (form.getTbSegment().getValue() != null)
					inventity.setSegment(form.getTbSegment().getValue());
				/*
				 *  end
				 */
				
				
				/*
				 *  nidhi
				 *   1-07-2017
				 */
				if (!form.getTbrateContractServiceid().getValue().equals(""))
					inventity.setRateContractServiceId(Integer.parseInt(form.getTbrateContractServiceid().getValue()));
				/*
				 *  end
				 */
				
				/**
				 *  nidhi
				 *  20-07-2017
				 *   quantity and mesurement transfer to the invoice entity 
				 */
				if(form.getQuantity().getValue() !=null && form.getQuantity().getValue() > 0){
					inventity.setQuantity(form.getQuantity().getValue());
				}
				
				if(form.getOlbUOM().getValue()!=null){
					inventity.setUom(form.getOlbUOM().getValue().trim());
				}
				/*
				 * end   
				 */
				/*
				 * 	nidhi
				 * 	24-07-2017
				 *  save customer ref no to invoice bean 	
				 *  
				 */
				if(form.getTbReferenceNumber().getValue()!=null){
					inventity.setRefNumber(form.getTbReferenceNumber().getValue().trim());
				}
				/**
				 *  end
				 */
				
				/** Date 06-09-2017 added by vijay for total amt roundoff *******/
				
				inventity.setTotalAmtExcludingTax(form.getDbtotalAmt().getValue());
				inventity.setDiscountAmt(form.dbDiscountAmt.getValue());
				inventity.setFinalTotalAmt(form.getDbFinalTotalAmt().getValue());
				inventity.setTotalAmtIncludingTax(form.getDbtotalAmtIncludingTax().getValue());
				inventity.setTotalBillingAmount(form.getDbGrandTotalAmt().getValue());
				if(form.getTbroundOffAmt().getValue()!=null&&!form.getTbroundOffAmt().getValue().equals("")){
					inventity.setDiscount(Double.parseDouble(form.getTbroundOffAmt().getValue()));
				}
				
				/**
				 * Date : 06-11-2017 BY ANIL
				 * setting billing comment to invoice
				 * for NBHC
				 */
				if(form.getTacomment().getValue()!=null){
					inventity.setComment(form.getTacomment().getValue());
				}
				/**
				 * End
				 */
				
				/**
				 *  nidhi
				 *  Date : 4-12-2017
				 *  For copy configration details to bill
				 */
				if(confiFlag){
					inventity.setInvoiceCategory(form.getOlbbillingcategory().getValue(form.getOlbbillingcategory().getSelectedIndex()));
					inventity.setInvoiceConfigType(form.getOlbbillingtype().getValue(form.getOlbbillingtype().getSelectedIndex()));
					inventity.setInvoiceGroup(form.getOlbbillinggroup().getValue(form.getOlbbillinggroup().getSelectedIndex()));
				}
				/**
				 *  end
				 */
				 /** Date 06/2/2018 added by komal for consolidate price checkbox **/
				inventity.setConsolidatePrice(model.isConsolidatePrice());
				/**
				 * end komal
				 */
				/**
				 * nidhi
				 * 28-04-2018
				 */
				inventity.setRenewContractFlag(model.isRenewContractFlag());
				/**
				 * end
				 */
				/** ends here **********************/
				/** date 15.10.2018 added by komal for subbilltye **/
				if(model.getSubBillType()!= null && !model.getSubBillType().equals("")){
					inventity.setSubBillType(model.getSubBillType());
					if(model.getSubBillType().equalsIgnoreCase(AppConstants.ARREARSBILL)){
						inventity.setCncBillAnnexureId(model.getCncBillAnnexureId());
					}
				}
				
				
				/**
				 * @author Anil,Date : 06-03-2019
				 * setting service id from bill document to invoice
				 * for service invoice mapping report
				 */
				if(model.getServiceId()!=null){
					inventity.setServiceId(model.getServiceId());
				}
				
				/*** Date 04-04-2019 by Vijay for NBHC CCPM AutoInvoice invoice details from popup ****/
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "EnableAutoInvoice")){
					inventity.setInvoiceCategory(autoInvoicePopup.getOlbInvoiceCategory().getValue(autoInvoicePopup.getOlbInvoiceCategory().getSelectedIndex()));
					inventity.setInvoiceConfigType(autoInvoicePopup.getOlbInvoiceConfigType().getValue(autoInvoicePopup.getOlbInvoiceConfigType().getSelectedIndex()));
					inventity.setInvoiceGroup(autoInvoicePopup.getOlbinvoicegroup().getValue(autoInvoicePopup.getOlbinvoicegroup().getSelectedIndex()));
					inventity.setApproverName(autoInvoicePopup.getOlbApproverName().getValue(autoInvoicePopup.getOlbApproverName().getSelectedIndex()));
				}
				
				/*** Date 17-04-2019 by Vijay for NBHC CCPM Billing Amount reduced storing in invoice to show in sales register revenue loss ****/
				if(model.getCancleAmtRemark()!=null && !model.getCancleAmtRemark().equals("")){
					inventity.setCancleAmtRemark(model.getCancleAmtRemark());
				}
				/** Date 16-09-2020 set Sub bill type for CNC invoices ***/
				if(model.getSubBillType()!=null){
					inventity.setSubBillType(model.getSubBillType());
				}
				
				/** Date 09-10-2020 by Vijay set value of created by requirement raised by Rahul Tiwari ***/
				if(LoginPresenter.loggedInUser!=null){
					inventity.setCreatedBy(LoginPresenter.loggedInUser);
				}
				
				/**
				 * @author Anil @since 05-04-2021
				 * For Alkosh raised by Rahul Tiwari
				 */
				inventity.setManagementFeesPercent(model.getManagementFeesPercent());
				
				if(model.getPaymentMode()!=null)
					inventity.setPaymentMode(model.getPaymentMode());
				
//				Timer t = new Timer() {
//				      @Override
//				      public void run() {
				
				/**
				 * @author Vijay 28-03-2022 for do not print service address flag updating to invoice
				 */
				inventity.setDonotprintServiceAddress(model.isDonotprintServiceAddress());

				async.save(inventity, new AsyncCallback<ReturnFromServer>() {

					@Override
					public void onFailure(Throwable caught) {
						form.hideWaitSymbol();
						form.showDialogMessage("An unexpected error occured");
						
						/**
						 * @author Anil,Date : 30-03-2019
						 * Multiple invoices were created against single bill
						 * This was happening because of double click on button
						 * raised by Rohan for pecopp
						 */
						if(lblTaxInvoice!=null){ //For auto Invoice getting null exception so added if condition by Vijay
						lblTaxInvoice.setVisible(true);
						lblTaxInvoice.setText("Tax Invoice");
						}
						/**
						 * End
						 */
					}

					@Override
					public void onSuccess(ReturnFromServer result) {	
						/**
						 * @author Anil,Date : 30-03-2019
						 * Multiple invoices were created against single bill
						 * This was happening because of double click on button
						 * raised by Rohan for pecopp
						 */
						if(lblTaxInvoice!=null){ //For auto Invoice getting null exception so added if condition by Vijay
//						lblTaxInvoice.setVisible(true);
						lblTaxInvoice.setText("Tax Invoice");
						}
						
						/**
						 * End
						 */
						/**
						 *  nidhi
						 *  13-1-2018
						 *  for single invoice to mutiple invoice creation 
						 *  commented this old code.
						 */
//						model.setInvoiceCount(result.count);
//						model.setStatus(BillingDocument.BILLINGINVOICED);
//						async.save(model, new AsyncCallback<ReturnFromServer>() {
//							@Override
//							public void onFailure(Throwable caught) {
//								form.hideWaitSymbol();
//								form.showDialogMessage("An unexpected error occured");
//							}
//							@Override
//							public void onSuccess(ReturnFromServer result) {
//								form.getIbinvoiceid().setValue(model.getInvoiceCount() + "");
//								form.showDialogMessage("Billing Document Invoiced!",GWTCAlert.OPTION_ROUNDED_BLUE,GWTCAlert.OPTION_ANIMATION);
//								form.hideWaitSymbol();
//							}
//						});
						/**
						 *  end
						 */
//						form.showDialogMessage("Invoice detail --" +result);
						/**
						 * @author Vijay Chougule Date 12-09-2019
						 * Des :- NBHC CCPM for auto Invoice sometimes invoice does not showing in approval Q
						 * So first invoice i will send the approval request in approval then i will call below method
						 * And normal for normal flow i called below method in else block  
						 */
//						changeBillingStatusToInvoiced(result.count,inventity);

						if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "EnableAutoInvoice")){
							reactonSendApprovalRequest(inventity, result.count);
						}
						else{
							changeBillingStatusToInvoiced(result.count,inventity);
						}
						
					}
				});
				      
//				      }
//				};
//			    t.schedule(2000);
			    
			    /**
				  *  nidhi
				  *  Date : 4-12-2017
				  *  Check process configration for 
				  */
				if(confiFlag){
					boolean updateFlag = false;
					updateFlag = AppUtility.checkContractCategoryAvailableOrNot(model.getBillingCategory(), model.getCompanyId(), model.getApproverName(), 24);
					updateFlag = AppUtility.checkContractTypeAvailableOrNot(model.getBillingType(),model.getBillingCategory(), model.getCompanyId(), model.getApproverName(), 24);
					updateFlag = AppUtility.checkServiceTypeAvailableOrNot(model.getBillingGroup(), model.getCompanyId(), model.getApproverName(), 77);
				}
				/**
				 * end
				 */
				}
			
			/**
			 * Change the billing document status to invoiced when either of proforma or tax invoice is clicked.
			 */
			/**
			 *  nidhi
			 *  13-1-2018
			 *  add invoice id perameter for save invoice id in 
			 *  every billing document
			 * @param inventity 
			 */
			private void changeBillingStatusToInvoiced(final int invoiceCount, Invoice inventity)
			{
				List<BillingDocumentDetails> billdoclis=form.getBillingDocumentTable().getDataprovider().getList();
				ArrayList<BillingDocumentDetails> billdocarr=new ArrayList<BillingDocumentDetails>();
				billdocarr.addAll(billdoclis);
				form.getBillingDocumentTable().connectToLocal();
				
				/*
				 *  nidhi
				 *  contract count
				 *  15-01-2018
				 */
				int billingCount=0,billingContract = 0,billCount = 0;
				
//				/**** Date 08-05-2019 by Vijay for NBHC CCPM AutoInvoice Sending Request for Approval  Some times its not sending request so added here *******/
////				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "EnableAutoInvoice")){
//				if(LoginPresenter.autoInvoiceFlag){
////					reactonSendApprovalRequest(inventity,invoiceCount);
//					Console.log("Before Sending Request for Approval");
//					try {
//						
//					
//					String documentCreatedBy="";
//					if(inventity.getCreatedBy()!=null){
//						documentCreatedBy=inventity.getCreatedBy();
//					}
//					ApproverFactory appFactory=new ApproverFactory();
//					Approvals approval=appFactory.getApprovals(inventity.getEmployee(), inventity.getApproverName()
//							, inventity.getBranch(), invoiceCount,documentCreatedBy,inventity.getCustomerId()+"",inventity.getPersonInfo().getFullName());
//					approval.setBusinessprocesstype(ApproverFactory.INVOICEDETAILS);
//					Console.log("Before Sending Request for Approval 1");
//
//					saveApprovalReq(approval,ConcreteBusinessProcess.REQUESTED,"Request Sent!",false);
//					Console.log("After Sending Request for Approval 1");
//
//					reactOnApprovalEmail(approval);
//					System.out.println("SENT");
//					} catch (Exception e) {
//						// TODO: handle exception
//						Console.log("Exception =="+e.getLocalizedMessage());
//					}
//				
//				}
				
				for(int i=0;i<billdocarr.size();i++)
				{
					BillingDocumentDetails billdocinfo=new BillingDocumentDetails();
					billdocinfo.setOrderId(billdocarr.get(i).getOrderId());
					billdocinfo.setBillId(billdocarr.get(i).getBillId());
					billdocinfo.setBillingDate(billdocarr.get(i).getBillingDate());
					billdocinfo.setBillAmount(billdocarr.get(i).getBillAmount());
					billdocinfo.setBillingCategory(billdocarr.get(i).getBillingCategory());
					billdocinfo.setBillingType(billdocarr.get(i).getBillingType());
					billdocinfo.setBillingGroup(billdocarr.get(i).getBillingGroup());
					billdocinfo.setBillstatus(BillingDocument.BILLINGINVOICED);
					form.getBillingDocumentTable().getDataprovider().getList().add(billdocinfo);
					billingCount=billdocarr.get(i).getBillId();
					
					MyQuerry querry=new MyQuerry();
					Vector<Filter> filtervec=new Vector<Filter>();
					Filter filtercount=null;
					System.out.println("One Two Three");
					filtercount=new Filter();
					filtercount.setQuerryString("count");
					filtercount.setIntValue(billingCount);
					filtervec.add(filtercount);
					/**
					 *  nidhi
					 *   13-1-2018
					 *   add contract id filter
					 *   for multiple billing document to single invoice
					 */
					billingContract = billdocarr.get(i).getOrderId();
					filtercount=new Filter();
					filtercount.setQuerryString("contractCount");
					filtercount.setIntValue(billingContract);
					filtervec.add(filtercount);
					
					/**
					 *  end
					 */
					filtercount=new Filter();
					filtercount.setQuerryString("companyId");
					filtercount.setLongValue(model.getCompanyId());
					filtervec.add(filtercount);
					
					filtercount=new Filter();
					filtercount.setQuerryString("typeOfOrder");
					filtercount.setStringValue(model.getTypeOfOrder());
					filtervec.add(filtercount);
					
					querry.setFilters(filtervec);
					querry.setQuerryObject(new BillingDocument());
					async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {

						@Override
						public void onFailure(Throwable caught) {
							form.showDialogMessage("An Unexpected Error occured !");
							
						}
						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							Console.log("billing detail count -- " +result.size());
						ArrayList<BillingDocument> resultDetail = new ArrayList<BillingDocument>();
//						resultDetail = (BillingDocument) result;
						
						for(int i =0 ;i<result.size();i++){
							resultDetail.add((BillingDocument)result.get(i));
							resultDetail.get(i).setStatus(BillingDocument.BILLINGINVOICED);
							resultDetail.get(i).setInvoiceCount(invoiceCount);
						}
						
						for(BillingDocument bill : resultDetail){
							Console.log("Billing document --" + bill.getCount() + " invoice id == " + invoiceCount);
						}
						
						ArrayList<SuperModel> resultList = new ArrayList<SuperModel>();
						
						resultList.addAll(resultDetail);
						
						
						async.save(resultList, new AsyncCallback<ArrayList<ReturnFromServer>>() {
							
							@Override
							public void onSuccess(ArrayList<ReturnFromServer> result) {
								// TODO Auto-generated method stub
								
								model.setStatus(BillingDocument.BILLINGINVOICED);
								form.tbstatus.setText(BillingDocument.BILLINGINVOICED);
								form.getIbinvoiceid().setValue(invoiceCount + "");
								
								/**
								 * @author Anil
								 * @since 06-01-2021
								 */
								form.billingDocObj.setStatus(model.getStatus());
								
								String mainScreenLabel="Billing Details";
								if(form.billingDocObj!=null&&form.billingDocObj.getCount()!=0){
									mainScreenLabel=form.billingDocObj.getCount()+" "+"/"+" "+form.billingDocObj.getStatus()+" "+"/"+" "+AppUtility.parseDate(form.billingDocObj.getCreationDate());
								}
								form.fgroupingCustomerInformation.getHeaderLabel().setText(mainScreenLabel);
								
								model.setInvoiceCount(invoiceCount);
								form.hideWaitSymbol();
								form.changeProcessLevel();
								
//							
							}
							
							@Override
							public void onFailure(Throwable caught) {
								// TODO Auto-generated method stub
								
							}
						});
						
						/**
						 *  nidhi
						 *  13-1-2018
						 *  commented for single invoice from billing document
						 */
					
//						async.save(resultList,new AsyncCallback<ReturnFromServer>() {
//
//							@Override
//							public void onFailure(Throwable caught) {
//								form.showDialogMessage("An Unexpected Error occured !");
//								
//							}
//							@Override
//							public void onSuccess(ReturnFromServer result) {
////								form.showDialogMessage("Billing Document Invoiced!",GWTCAlert.OPTION_ROUNDED_BLUE,GWTCAlert.OPTION_ANIMATION);
//								model.setStatus(BillingDocument.BILLINGINVOICED);
//								form.tbstatus.setText(BillingDocument.BILLINGINVOICED);
//								form.setToViewState();
//							}
//						});
						/**
						 *  end
						 */}
					});
				}
				
				/**** Date 04-04-2019 by Vijay for NBHC CCPM AutoInvoice not required to show this message   *******/
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "EnableAutoInvoice")){
					form.showDialogMessage("Billing Document Invoiced!",GWTCAlert.OPTION_ROUNDED_BLUE,GWTCAlert.OPTION_ANIMATION);
				}
				else{
					form.showDialogMessage("Billing Document Invoiced!",GWTCAlert.OPTION_ROUNDED_BLUE,GWTCAlert.OPTION_ANIMATION);
				}

				
				form.hideWaitSymbol(); //vijay
			}
			
			/*
			 * This method is used for checking payable amount. If billing document created is not 100perc
			 * of the calculated billing amount as per payment terms.
			 * If the calculated payable amount is less then base billing then it returns true.  
			 */
			private boolean chkPayableAmount()
			{
				double baseBilling=0,payableAmt=0;
				List<SalesOrderProductLineItem> chkPayable=form.getSalesProductTable().getDataprovider().getList();
				for(int i=0;i<chkPayable.size();i++)
				{
					baseBilling=baseBilling+chkPayable.get(i).getBaseBillingAmount();
					payableAmt=payableAmt+(chkPayable.get(i).getBaseBillingAmount()*chkPayable.get(i).getPaymentPercent())/100;
				}
				if(payableAmt<baseBilling)
				{
					return true;
				}
				return false;
			}
			
			/**
			 * Method to update product info if billing document is cancelled
			 * @return
			 */
			private List<SalesOrderProductLineItem> updateProductTableOnCancel()
			{
				List<SalesOrderProductLineItem> prodLst=form.getSalesProductTable().getDataprovider().getList();
				ArrayList<SalesOrderProductLineItem> arrSalesProd=new ArrayList<SalesOrderProductLineItem>();
				double calPayableAmt=0,calBaseBillAmt;
				for(int i=0;i<prodLst.size();i++)
				{
					SalesOrderProductLineItem  sopl=new SalesOrderProductLineItem();
					sopl.setProdCategory(prodLst.get(i).getProdCategory());
					sopl.setProdCode(prodLst.get(i).getProdCode());
					sopl.setProdName(prodLst.get(i).getProdName());
					sopl.setQuantity(prodLst.get(i).getQuantity());
					sopl.setPrice(prodLst.get(i).getPrice());
					sopl.setVatTax(prodLst.get(i).getVatTax());
					sopl.setServiceTax(prodLst.get(i).getServiceTax());
					sopl.setProdPercDiscount(prodLst.get(i).getProdPercDiscount());
					sopl.setTotalAmount(prodLst.get(i).getTotalAmount());
					calPayableAmt=(prodLst.get(i).getBaseBillingAmount()*prodLst.get(i).getPaymentPercent())/100;
					calBaseBillAmt=prodLst.get(i).getBaseBillingAmount()-calPayableAmt;
					if(prodLst.get(i).getPaymentPercent()==100){
						sopl.setBaseBillingAmount(prodLst.get(i).getBaseBillingAmount());
					}
					else{
						sopl.setBaseBillingAmount(calBaseBillAmt);
					}

					sopl.setPaymentPercent(100.0);
					
					arrSalesProd.add(sopl);
				}
				return arrSalesProd;
			}
			
			/**
			 * Method returns the list of taxes when billing document is cancelled. 
			 * The assess value is updated as per base billing amount
			 */
			private List<ContractCharges> updateTaxesTableOnCancel(List<SalesOrderProductLineItem> lisproducts)
			{
				double updatedbillamt=0;
				List<ContractCharges> formtaxlis=form.getBillingTaxTable().getDataprovider().getList();
				ArrayList<ContractCharges> arrcc=new ArrayList<ContractCharges>();
				
				for(int i=0;i<formtaxlis.size();i++)
				{
					ContractCharges billTaxEntity=new ContractCharges();
					billTaxEntity.setTaxChargeName(formtaxlis.get(i).getTaxChargeName());
					billTaxEntity.setTaxChargePercent(formtaxlis.get(i).getTaxChargePercent());
					updatedbillamt=this.updatedBillAmt(lisproducts,formtaxlis.get(i).getIdentifyTaxCharge());
					billTaxEntity.setTaxChargeAssesVal(updatedbillamt);
					arrcc.add(billTaxEntity);
				}
				return arrcc;
			}
		
			/**
			 * To calculate the assess val of tax table based on index value and base billing amount
			 * The index val is the products index and its related taxes
			 */
			private double updatedBillAmt(List<SalesOrderProductLineItem> lisprod,int indexVal)
			{
				double billVal=0;
				for(int i=0;i<lisprod.size();i++)
				{
					if(lisprod.get(i).getIndexVal()==indexVal){
						billVal=lisprod.get(i).getBaseBillingAmount();
					}
				}
				return billVal;
			}
			
			
			//Billing document table selection code. On select of record fill fields
			 public void setTableSelectionOnBillingTable()
			 {
				 final NoSelectionModel<BillingDocumentDetails> selectionModelMyObj = new NoSelectionModel<BillingDocumentDetails>();
			     SelectionChangeEvent.Handler  tableHandler = new SelectionChangeEvent.Handler() 
			     {
			         @Override
			         public void onSelectionChange(SelectionChangeEvent event) 
			         {
			        	 final BillingDocumentDetails entity=selectionModelMyObj.getLastSelectedObject();
			        	 
			        	 final int billingCount=entity.getBillId();
			        	 form.ibbillingid.setValue(entity.getBillId()+"");
			        	 if(entity.getBillingDate()!=null)
			        		 form.dbbillingdate.setValue(entity.getBillingDate());
			        	 if(entity.getBillingCategory()!=null)
			        		 form.olbbillingcategory.setValue(entity.getBillingCategory());
			        	 if(entity.getBillingType()!=null)
			        		 form.olbbillingtype.setValue(entity.getBillingType());
			        	 if(entity.getBillingGroup()!=null)
			        		 form.olbbillinggroup.setValue(entity.getBillingGroup());
			        	 
			        	 MyQuerry querry=new MyQuerry();
			        	 Vector<Filter> filtervec=new Vector<Filter>();
			        	 
			        	 Filter billCountFilter=null;
			        	 billCountFilter=new Filter();
			        	 billCountFilter.setQuerryString("count");
			        	 billCountFilter.setIntValue(billingCount);
			        	 filtervec.add(billCountFilter);
			        	 
			        	 billCountFilter=new Filter();
			        	 billCountFilter.setQuerryString("typeOfOrder");
			        	 billCountFilter.setStringValue(model.getTypeOfOrder());
			        	 filtervec.add(billCountFilter);

			        	 billCountFilter=new Filter();
			        	 billCountFilter.setQuerryString("companyId");
			        	 billCountFilter.setLongValue(model.getCompanyId());
			        	 filtervec.add(billCountFilter);
			        	 
			        	 querry.setFilters(filtervec);
			        	 querry.setQuerryObject(new BillingDocument());
			        		async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {

								@Override
								public void onFailure(Throwable caught) {
									form.showDialogMessage("An Unexpected Error occured !");
									
								}
								@Override
								public void onSuccess(ArrayList<SuperModel> result) {
									for(SuperModel smodel:result)
									{
										BillingDocument billentity = (BillingDocument)smodel;
										if(billentity.getCount()==billingCount)
										{
											form.getSalesProductTable().setValue(billentity.getSalesOrderProducts());
											form.getBillingTaxTable().setValue(billentity.getBillingTaxes());
											form.getBillingChargesTable().setValue(billentity.getBillingOtherCharges());
											if(billentity.getArrPayTerms()!=null)
											{
												form.getIbpaytermsdays().setValue(billentity.getArrPayTerms().get(0).getPayTermDays());
												form.getDopaytermspercent().setValue(billentity.getArrPayTerms().get(0).getPayTermPercent());
												form.getTbpaytermscomment().setValue(billentity.getArrPayTerms().get(0).getPayTermComment());
											}
											break;
										}
									}
								}
								});
			        	}
			         };
			         // Add the handler to the selection model
			         selectionModelMyObj.addSelectionChangeHandler( tableHandler );
			     	// Add the selection model to the table
			         form.billingDocumentTable.getTable().setSelectionModel(selectionModelMyObj);
			     }
			
			 @Override
			public void onRowCountChange(RowCountChangeEvent event) 
			{	
				
				System.out.println("INSIDE RoW COUNT CHANGE METHOD....!!!");
				Console.log("INSIDE RoW COUNT CHANGE METHOD....!!!");
			
			if (event.getSource() == form.getSalesProductTable().getTable()) {
				
				if(form.getSalesProductTable().getDataprovider().getList().size()!=0)
				{
					Console.log("INSIDE IF CONDITION....!!!");
				double retrieveBillAmt = form.getSalesProductTable().calculateTotalBillAmt();
				// form.getDototalbillamt().setValue(retrieveBillAmt);

				billingTaxTableOnRowChange();
				retrieveBillAmt = retrieveBillAmt+ form.getBillingTaxTable().calculateTotalTaxes();

//				if (multipleContractBillingInvoiceFlag == false) {
//					Console.log("PRODUCT FALSE....!!!");
//					System.out.println("PRODUCT FALSE....!!!");
//					billingTableOnRowChange(retrieveBillAmt);
//				}
//
//				if (multipleContractBillingInvoiceFlag == true) {
//					System.out.println("PRODUCT TRUE....!!!");
//					Console.log("PRODUCT TRUE....!!!");
//					reactOnProductsChange();
//				}

				if(checkproductstatus()){
					System.out.println("PRODUCT TRUE....!!!");
					reactOnProductsChange();
				}else{
					System.out.println("Inside else=================");
					retrieveproductinfomation();
				}
				
				}
				
				/**
				 * @author Anil @since 16-11-2021
				 * Updating price and tax details on sales oreder summary
				 * Sunrise SCM
				 */
				try{
					updateSalesOrderSummary();
				}catch(Exception e){
					
				}
			}
             
			double totalOfProductPrice=0;
			if (event.getSource().equals(form.tblOtherCharges.getTable())) {
				Console.log("inside onrowcountchange other charges");
				if(form.tblOtherCharges.getValue().size()!=0){
					totalOfProductPrice= form.getDbFinalTotalAmt().getValue();
					for(OtherCharges obj:form.getTblOtherCharges().getValue()){
						double otherChargAmt=0;
						OtherTaxCharges otherTax=form.tblOtherCharges.getOtherChargesDetails(obj.getOtherChargeName());
						if(otherTax!=null){
							if(otherTax.getOtherChargePercent()!=0){
								otherChargAmt=((totalOfProductPrice*otherTax.getOtherChargePercent())/100);
								obj.setAmount(otherChargAmt);
							}else if(otherTax.getOtherChargeAbsValue()!=null){
//								otherChargAmt=otherTax.getOtherChargeAbsValue();
//								obj.setAmount(otherChargAmt);
								if(obj.getAmount()==0){
									otherChargAmt=otherTax.getOtherChargeAbsValue();
									obj.setAmount(otherChargAmt);
								}
							}
						}
					}
				}
				
				
				form.getBillingTaxTable().connectToLocal();
				try {
					form.addProdTaxes();
					form.addOtherChargesInTaxTbl();
					/** Date 26-11-2018 By Vijay For Other Charges***/
					form.getDbOtherChargesTotal().setValue(form.tblOtherCharges.calculateOtherChargesSum());
				} catch (Exception e) {
					e.printStackTrace();
				}
				/**
				 *  Date 26-11-2018 By Vijay 
				 *  Des :- As per the Other Charges And Taxes Code updated
				 */
					double totalIncludingTax=form.getDbFinalTotalAmt().getValue()+form.getBillingTaxTable().calculateTotalTaxes();
					double newnetpay = totalIncludingTax+ form.tblOtherCharges.calculateOtherChargesSum();
					
					/**
					 * @author Vijay Date :- 19-01-2021
					 * Des :- Round up logic not required for innovative so if below process config is active then amount
					 * will not round. Below is standard code execute for other clients
					 */
					if(!LoginPresenter.PC_NO_ROUNDOFF_INVOICE_CONTRACTFlag){
						newnetpay = Math.round(newnetpay);
						int netPayAmt = (int) newnetpay;
						newnetpay = netPayAmt;
					}
					
					/**
					 * Date 08/06/2018
					 * Developer :- Vijay
					 * Des :- for final total amt Round off amt calculations
					 */
					form.getDbtotalAmtIncludingTax().setValue(Double.parseDouble(nf.format(newnetpay)));
					
					if(form.getTbroundOffAmt().getValue()!=null && !form.getTbroundOffAmt().getValue().equals("")){
						String roundOff = form.getTbroundOffAmt().getValue();
						double roundoffAmt =  AppUtility.calculateRoundOff(roundOff, newnetpay);
						System.out.println("roundoffAmt =="+roundoffAmt);
						if(roundoffAmt!=0){
							form.getDototalbillamt().setValue(roundoffAmt);
						}else{
							form.getDototalbillamt().setValue(Double.parseDouble(nf.format(newnetpay)));
							form.getDototalbillamt().setValue(0d);
						}
					}else{
						form.getDototalbillamt().setValue(Double.parseDouble(nf.format(newnetpay)));

					}
					/**
					 * ends here
					 */
					form.getDototalbillamt().setValue(Double.parseDouble(nf.format(newnetpay)));
					
			}
			
			
			
			
			
			if (event.getSource() == form.getBillingChargesTable().getTable()) {
				
				if (multipleContractBillingInvoiceFlag == false) {
					System.out.println("OTHER CHARGES FALSE....!!!");
					Console.log("OTHER CHARGES FALSE....!!!");
					double totalAmt = form.getSalesProductTable().calculateTotalBillAmt();
					totalAmt = totalAmt+ form.getBillingTaxTable().calculateTotalTaxes();
					totalAmt = totalAmt+ form.getBillingChargesTable().calculateTotalBillingCharges();
					if(!LoginPresenter.PC_NO_ROUNDOFF_INVOICE_CONTRACTFlag){
						totalAmt = Math.round(totalAmt);
					}
					form.getDototalbillamt().setValue(totalAmt);
					
					billingTableOnRowChange(totalAmt);
				}
				
			}
			
			/**
			 * Date : 21-09-2017 By ANIL
			 */
			double totalOfProductPric=0;
			if(event.getSource().equals(form.tblOtherCharges.getTable())){
				
				if(form.tblOtherCharges.getValue().size()!=0){
						totalOfProductPric= form.getDbtotalAmt().getValue();	
					
				
				for(OtherCharges obj:form.getTblOtherCharges().getValue()){
					double otherChargAmt=0;
					OtherTaxCharges otherTax=form.tblOtherCharges.getOtherChargesDetails(obj.getOtherChargeName());
					if(otherTax!=null){
						if(otherTax.getOtherChargePercent()!=0){
							otherChargAmt=((totalOfProductPric*otherTax.getOtherChargePercent())/100);
							obj.setAmount(otherChargAmt);
						}else if(otherTax.getOtherChargeAbsValue()!=null){
							if(obj.getAmount()==0){
							otherChargAmt=otherTax.getOtherChargeAbsValue();
							obj.setAmount(otherChargAmt);
							}
						}
					}
					
					
				}
				}
				
				form.billingTaxTable.connectToLocal();
				try {
					form.addProdTaxes();
					form.addOtherChargesInTaxTbl();
					form.dbOtherChargesTotal.setValue(form.tblOtherCharges.calculateOtherChargesSum());
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				double totalIncludingTax=0;
				if(form.getDbFinalTotalAmt().getValue()!=null){
					totalIncludingTax = form.getDbFinalTotalAmt().getValue()+ form.billingTaxTable.calculateTotalTaxes();
				}else{
					totalIncludingTax = form.billingTaxTable.calculateTotalTaxes();
				}
				if(form.dbOtherChargesTotal.getValue()!=null){
					totalIncludingTax=totalIncludingTax+form.dbOtherChargesTotal.getValue();
				}
				form.getDbtotalAmtIncludingTax().setValue(totalIncludingTax);
				form.setToatalAmountIncludingTax(Double.parseDouble(nf.format(totalIncludingTax)));
				
				double netPay = fillNetPayable(form.getToatalAmountIncludingTax());
				
				if(!LoginPresenter.PC_NO_ROUNDOFF_INVOICE_CONTRACTFlag){
					netPay = Math.round(netPay);
					int netPayable = (int) netPay;
					netPay = netPayable;
				}
				
				form.getDbGrandTotalAmt().setValue(netPay);
				if(form.getTbroundOffAmt().getValue()!=null && !form.getTbroundOffAmt().getValue().equals("") ){
					String roundOff = form.getTbroundOffAmt().getValue();
					double roundoffAmt =  AppUtility.calculateRoundOff(roundOff, netPay);
					if(roundoffAmt!=0){
						form.getDototalbillamt().setValue(roundoffAmt);
					}else{
						form.getDototalbillamt().setValue(roundoffAmt);
						form.getTbroundOffAmt().setValue("");
					}
				}
				else{
					form.getDototalbillamt().setValue(netPay);
				}

			}
				
			/**
			 * End
			 */
			
			}

			private void billingTableOnRowChange(double billamt)
			{
				List<BillingDocumentDetails> billdoclis=form.getBillingDocumentTable().getDataprovider().getList();
				ArrayList<BillingDocumentDetails> arrbilldoc=new ArrayList<BillingDocumentDetails>();
				arrbilldoc.addAll(billdoclis);
				Console.log("inside billingTableOnRowChange 1111111111111111");
				if(arrbilldoc.size()!=0)
				{
					Console.log("inside billingTableOnRowChange  222222222222222");
					BillingDocumentDetails billingdoc=new BillingDocumentDetails();
					//***************rohan added this line (missing while merging)
					billingdoc.setOrderId(arrbilldoc.get(0).getOrderId());
					
					billingdoc.setBillId(arrbilldoc.get(0).getBillId());
					billingdoc.setBillingDate(arrbilldoc.get(0).getBillingDate());
					billingdoc.setBillAmount(billamt);
					billingdoc.setBillingCategory(arrbilldoc.get(0).getBillingCategory());
					billingdoc.setBillingType(arrbilldoc.get(0).getBillingType());
					billingdoc.setBillingGroup(arrbilldoc.get(0).getBillingGroup());
					billingdoc.setBillstatus(arrbilldoc.get(0).getBillstatus());
					Console.log("inside billingTableOnRowChange  333333333333333333333333");
					form.getBillingDocumentTable().connectToLocal();
					form.getBillingDocumentTable().getDataprovider().getList().add(billingdoc);
				}
			}
			
			private void billingTaxTableOnRowChange()
			{
				List<SalesOrderProductLineItem> salesProdlis=form.getSalesProductTable().getDataprovider().getList();
				for(int i=0;i<salesProdlis.size();i++)
				{
					List<ContractCharges> updateBillLis=updateTaxesTable(salesProdlis.get(i).getPaymentPercent(),salesProdlis.get(i).getIndexVal());
					form.getBillingTaxTable().setValue(updateBillLis);
				}
				double retrieveAmt=form.getSalesProductTable().calculateTotalBillAmt()+form.getBillingTaxTable().calculateTotalTaxes()+form.getBillingChargesTable().calculateTotalBillingCharges();
				if(LoginPresenter.PC_NO_ROUNDOFF_INVOICE_CONTRACTFlag){
					form.getDototalbillamt().setValue(retrieveAmt);
				}
				else{
					retrieveAmt=Math.round(retrieveAmt);
					form.getDototalbillamt().setValue(retrieveAmt);
				}
				
			}
			
			private List<ContractCharges> updateTaxesTable(double payPercentValue,int indexValue)
			{
				List<ContractCharges> taxList=form.getBillingTaxTable().getDataprovider().getList();
				ArrayList<ContractCharges> taxArr=new ArrayList<ContractCharges>();
				double updatedAssess=0;
				for(int i=0;i<taxList.size();i++)
				{
					ContractCharges entity=new ContractCharges();
					entity.setTaxChargeName(taxList.get(i).getTaxChargeName());
					entity.setTaxChargePercent(taxList.get(i).getTaxChargePercent());
					
					System.out.println("index value "+indexValue);
					System.out.println("identify tax charge "+taxList.get(i).getIdentifyTaxCharge());
					if(taxList.get(i).getIdentifyTaxCharge()==indexValue){
						updatedAssess=taxList.get(i).getTaxChargeAssesVal()*payPercentValue/100;
						updatedAssess=Double.parseDouble(nf.format(updatedAssess));
						entity.setTaxChargeAssesVal(updatedAssess);
					}
					else{
						entity.setTaxChargeAssesVal(taxList.get(i).getTaxChargeAssesVal());
					}
					taxArr.add(entity);
					}
				return taxArr;
			}
			
			private List<ContractCharges> updateChargesBalance(List<ContractCharges> lisofcharges)
			{
				System.out.println("Hi I Entered");
				List<ContractCharges> updateBalanceLis=form.getBillingChargesTable().getDataprovider().getList();
				final ArrayList<ContractCharges> updateBalanceArr=new ArrayList<ContractCharges>();
				double updateBalance=0,totalPayable=0,paypercval=0;
				for(int i=0;i<updateBalanceLis.size();i++)
				{
					ContractCharges ccObj=new ContractCharges();
					ccObj.setTaxChargeName(updateBalanceLis.get(i).getTaxChargeName());
					ccObj.setTaxChargePercent(updateBalanceLis.get(i).getTaxChargePercent());
					ccObj.setTaxChargeAbsVal(updateBalanceLis.get(i).getTaxChargeAbsVal());
					ccObj.setTaxChargeAssesVal(updateBalanceLis.get(i).getTaxChargeAssesVal());
					if(updateBalanceLis.get(i).getTaxChargeAbsVal()!=0){
						totalPayable=updateBalanceLis.get(i).getTaxChargeAbsVal();
					}
					if(updateBalanceLis.get(i).getTaxChargePercent()!=0){
						totalPayable=updateBalanceLis.get(i).getTaxChargePercent()*updateBalanceLis.get(i).getTaxChargeAssesVal()/100;
					}
					System.out.println("Calc"+totalPayable+"   "+updateBalanceLis.get(i).getPayableAmt());
					paypercval=updateBalanceLis.get(i).getPaypercent()+lisofcharges.get(i).getPaypercent();
					paypercval=100-paypercval;
					ccObj.setPaypercent(paypercval);
					updateBalance=totalPayable-(updateBalanceLis.get(i).getPayableAmt()+lisofcharges.get(i).getPayableAmt());
					System.out.println("Update Balance"+updateBalance);
					ccObj.setChargesBalanceAmt(updateBalance);
					ccObj.setPayableAmt(updateBalanceLis.get(i).getPayableAmt()+lisofcharges.get(i).getPayableAmt());
					updateBalanceArr.add(ccObj);
				}
				
				return updateBalanceArr;
			}
			
			public double removeAllTaxes(SuperProduct entity)
			{
				double vat = 0,service = 0;
				double tax=0,retrVat=0,retrServ=0;
				if(entity instanceof ServiceProduct)
				{
					ServiceProduct prod=(ServiceProduct) entity;
					if(prod.getServiceTax()!=null&&prod.getServiceTax().isInclusive()==true){
						service=prod.getServiceTax().getPercentage();
					}
					if(prod.getVatTax()!=null&&prod.getVatTax().isInclusive()==true){
						vat=prod.getVatTax().getPercentage();
					}
				}

				if(entity instanceof ItemProduct)
				{
					ItemProduct prod=(ItemProduct) entity;
					if(prod.getVatTax()!=null&&prod.getVatTax().isInclusive()==true){
						vat=prod.getVatTax().getPercentage();
					}
					if(prod.getServiceTax()!=null&&prod.getServiceTax().isInclusive()==true){
						service=prod.getServiceTax().getPercentage();
					}
				}
				
				if(vat!=0&&service==0){
					retrVat=entity.getPrice()/(1+(vat/100));
					retrVat=entity.getPrice()-retrVat;
				}
				if(service!=0&&vat==0){
					retrServ=entity.getPrice()/(1+service/100);
					retrServ=entity.getPrice()-retrServ;
				}
				if(service!=0&&vat!=0){
					double removeServiceTax=(entity.getPrice()/(1+service/100));
					retrServ=(removeServiceTax/(1+vat/100));
					retrServ=entity.getPrice()-retrServ;
				}
				tax=retrVat+retrServ;
				return tax;
			}
			
	/**********************************React On Request For Approval***************************************/		
	
			private void reactOnValidateReqApproval(final String btnLabel)
			{
				if(form.getDbinvoicedate().getValue()==null){
					form.showDialogMessage("Invoice Date is Mandatory!");
					form.hideWaitSymbol();//Vijay
				}
				if(form.getDbpaymentdate().getValue()==null){
					form.showDialogMessage("Payment Date is Mandatory!");
					form.hideWaitSymbol();//Vijay
				}
				
//				/**
//				 * Date : 06-07-2018
//				 * Developer : Vijay
//				 * Des :- For NBHC before to last months billing date not allowed to request for approval branch user
//				 * and allowed for admin only 
//				 */
//				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "OnlyForNBHC")){
//					if(!(LoginPresenter.myUserEntity.getRole().getRoleName().trim().equalsIgnoreCase("ADMIN"))){
//						if(form.getDbbillingdate().getValue()!=null){
//							Date lastToLastMonthEndDate= new Date();
//							CalendarUtil.addMonthsToDate(lastToLastMonthEndDate, -1);
//							CalendarUtil.setToFirstDayOfMonth(lastToLastMonthEndDate);
//							CalendarUtil.addDaysToDate(lastToLastMonthEndDate, -1);
//							if(form.getDbbillingdate().getValue().before(lastToLastMonthEndDate) || form.getDbbillingdate().getValue().equals(lastToLastMonthEndDate)){
//								form.showDialogMessage("Can not create invoice Before last month billing Documents!");
//								return;
//							}
//							
//							/**
//							 * Date 10-07-2018 By Vijay 
//							 * Des :- contract end is expired then dont allow related billing documents for approvals
//							 */
//							if(model.getContractEndDate()!=null){
//								Date todayDate = new Date();
//								if(todayDate.after(model.getContractEndDate())){
//									form.showDialogMessage("Contract expired can not proceed");
//									return;
//								}
//							}
//							/**
//							 * ends here
//							 */
//						}
//					}
//				}
//				/**
//				 * ends here
//				 */
				
				if(form.getDbinvoicedate().getValue()!=null&&form.getDbpaymentdate().getValue()!=null){
					MyQuerry querry=new MyQuerry();
					Vector<Filter> filtervec=new Vector<Filter>();
					Filter temp=null;
					
					temp=new Filter();
					temp.setLongValue(model.getCompanyId());
					temp.setQuerryString("companyId");
					filtervec.add(temp);
					
					temp=new Filter();
					temp.setStringValue(model.getTypeOfOrder());
					temp.setQuerryString("typeOfOrder");
					filtervec.add(temp);
					
					temp=new Filter();
					temp.setIntValue(Integer.parseInt(form.getIbcontractid().getValue()));
					temp.setQuerryString("contractCount");
					filtervec.add(temp);
					querry.setFilters(filtervec);
					querry.setQuerryObject(new BillingDocument());
					
					async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>(){

						@Override
						public void onFailure(Throwable caught) {
							form.showDialogMessage("An Unexpected Error Occurred!");
							form.showWaitSymbol();
						}

						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							System.out.println("inn"+result.size());
							if(result.size()==1){
								/**
								 * rohan remove this validation for creating partial payment in 100% payment document
								 * earlier it was not allowing to do so.
								 * Rohan remove validateProductBilling() this method form following all if conditions 
								 * Date : 2/7/2017 
								 */
								
//								if(!validateProductBilling()){
//									form.showDialogMessage("Check Product Table. Payment Percent is not 100%!");
//								}
								/*
								 * nidhi
								 * 8-10-2018 
								 *  commented this validation bcoz it is now dynamic and tex included.
								 * 
								if(!validateChargesPercent()){
									form.showDialogMessage("Check Other Charges. Payment Percent is not 100%!");
								}
								if(validateChargesPercent()) */{
									/**
									 * Date : 23-05-2017 by ANil
									 */
									if(btnLabel.equals(ManageApprovals.SUBMIT)){
//										form.getManageapproval().reactToSubmit();
											reactOnSubmit();
									}else{
										reactOnReqApproval();
									}
								}
							}
							
							if(result.size()>1){
								boolean flag=true;
								int count = 0;
								for(SuperModel valmodel:result)
								{
									BillingDocument billValidation=(BillingDocument)valmodel;
									/**
									 * Date : 01-11-2017 BY ANIL
									 * Removed this validation on request of NBHC
									 */
//									if(billValidation.getStatus().equals(BillingDocument.REQUESTED)){
//										form.showDialogMessage("Already one billing document is requested for approval. Please approve it first!");
//										flag=false;
//									}
									if(flag==true&&!billValidation.getStatus().equals(BillingDocument.REQUESTED)&&billValidation.getStatus().equals(BillingDocument.APPROVED)){
										count++;
									}
								}
								Console.log("count="+count+" result="+result.size()+" flag="+flag);
								
								 /** nidhi
								 * 8-10-2018 
								 *  commented this validation bcoz it is now dynamic and tex included.
								 * validateChargesPercent()*/
//								if(count==result.size()-1&&validateProductBilling() && validateChargesPercent()){
									if(count==result.size()-1&&validateProductBilling()){

									/**
									 * Date : 23-05-2017 by ANil
									 */
									if(btnLabel.equals(ManageApprovals.SUBMIT)){
//										form.getManageapproval().reactToSubmit();
											reactOnSubmit();
									}else{
										reactOnReqApproval();
									}
								}
								 /** nidhi
								 * 8-10-2018 
								 *  commented this validation bcoz it is now dynamic and tex included.
								 * validateChargesPercent()*/
//								else if(count==result.size()-1&&!validateProductBilling() && validateChargesPercent() ){

									
									//Ashwini Patil Date:11-07-2024 commented payment term validation on request of ultima and with permission of nitin sir.
									//Since they uploaded contracts,only single bill got created. ultima wants to create all future bills at once as per payment terms. 
//								else if(count==result.size()-1&&!validateProductBilling() ){
//	
//									form.showDialogMessage("Check Product Table. Payment Percent is not 100%!");
//									form.hideWaitSymbol(); //vijay
//								}
//									/** nidhi
//									 * 8-10-2018 
//									 *  commented this validation bcoz it is now dynamic and tex included.
//									 * validateChargesPercent()*/		
////								else if(count==result.size()-1&&validateProductBilling() &&!validateProductBilling() ){
//								else if(count==result.size()-1&&validateProductBilling() ){
//									form.showDialogMessage("Check Other Charges. Payment Percent is not 100%!");
//									form.hideWaitSymbol(); // vijay
//								}else{
//									form.hideWaitSymbol();
//								}
//								if(count!=result.size()-1&&flag==true){
									/**
									 * Date : 23-05-2017 by ANil
									 */
									if(btnLabel.equals(ManageApprovals.SUBMIT)){
//										form.getManageapproval().reactToSubmit();
											reactOnSubmit();
									}else{
										reactOnReqApproval();
									}
//								}
							}
						}
					});
					
				}
			}
			
			/**
			 * Adde by rahul for self approval only
			 * Added on 12 june 2017
			 * 
			 */
			private void reactOnSubmit() {
				// TODO Auto-generated method stub
				Console.log("inside reactOnSubmit "+new Date());
				boolean validatePayment = this.checkPayableAmount();

				/**
				 * nidhi 14-12-2018
				 */
				boolean parcialBillFlag =  AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "DisableParticalBillDocument");
				
				if(parcialBillFlag){
					validatePayment = false;
				}
				/**
				 * end
				 */
				Console.log("get parciaalflag --  " + validatePayment);
				if (validatePayment == true) {
					createNewBillingDocumentAgainstPayable();
				}
				
//				GenricServiceAsync async=GWT.create(GenricService.class);
				model.setStatus(BillingDocument.APPROVED);
				model.setApprovedDate(new Date());
				model.setSelfApprovalFlag(true);
				
				/**
				 * @author Anil
				 * @since 30-12-2020
				 * updating screen heading and status
				 */
				String mainScreenLabel="Billing Details";
				if(form.billingDocObj!=null&&form.billingDocObj.getCount()!=0){
					mainScreenLabel=form.billingDocObj.getCount()+" "+"/"+" "+form.billingDocObj.getStatus()+" "+"/"+" "+AppUtility.parseDate(form.billingDocObj.getCreationDate());
				}
				form.fgroupingCustomerInformation.getHeaderLabel().setText(mainScreenLabel);
				
				updateBillingChargeBalance();
				

			}
			
			
			
			/**
			 * Added by Rahul Verma
			 * Date : 12 June 2017
			 * for updateBillingChargeBalance
			 * return: void
			 */
			private void updateBillingChargeBalance() {
				// TODO Auto-generated method stub

				if(model.getBillingOtherCharges().size()!=0){
					Console.log("Inside model.getBillingOtherCharges().size()!=0");
					String typeFlow="";
					if(model.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESALES)){
						typeFlow=AppConstants.BILLINGSALESFLOW;
					}
					if(model.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE)){
						typeFlow=AppConstants.BILLINGPURCHASEFLOW;
					}
					if(model.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESERVICE)){
						typeFlow=AppConstants.BILLINGSERVICEFLOW;
					}
					
					MyQuerry querry = new MyQuerry();
				  	Company c = new Company();
				  	Vector<Filter> filtervec=new Vector<Filter>();
				  	Filter filter = null;
				  	filter = new Filter();
				  	filter.setQuerryString("companyId");
					filter.setLongValue(model.getCompanyId());
					filtervec.add(filter);
					filter = new Filter();
					filter.setQuerryString("contractId");
					filter.setIntValue(model.getContractCount());
					filtervec.add(filter);
					filter = new Filter();
					filter.setQuerryString("identifyOrder");
					filter.setStringValue(typeFlow.trim());
					filtervec.add(filter);
					querry.setFilters(filtervec);
					querry.setQuerryObject(new TaxesAndCharges());
					async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {

						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							form.hideWaitSymbol(); //vijay
						}

						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							// TODO Auto-generated method stub
							if(result.size()!=0){
								TaxesAndCharges billBalance=(TaxesAndCharges) result.get(0);
								if(billBalance!=null){
									List<ContractCharges> saveBalance=updateChargesBalance();
									ArrayList<ContractCharges> saveBalArr=new ArrayList<ContractCharges>();
									saveBalArr.addAll(saveBalance);
									billBalance.setTaxesChargesList(saveBalArr);
									async.save(billBalance, new AsyncCallback<ReturnFromServer>() {
										
										@Override
										public void onSuccess(ReturnFromServer result) {
											// TODO Auto-generated method stub
											async.save(model, new AsyncCallback<ReturnFromServer>() {
												
												@Override
												public void onSuccess(ReturnFromServer result) {
													// TODO Auto-generated method stub
													form.tbstatus.setValue(BillingDocument.APPROVED);
													form.changeProcessLevel();
													form.showDialogMessage("Successfully Submitted");
													form.hideWaitSymbol(); //vijay
													
													/*** Date 13-11-2019 by Vijay for NBHC CCPM ***/
													updateRateContractServiceValue();
												}
												
												

												@Override
												public void onFailure(Throwable caught) {
													// TODO Auto-generated method stub
													form.hideWaitSymbol(); //vijay
												}
											});
											
										}
										
										
										@Override
										public void onFailure(Throwable caught) {
											// TODO Auto-generated method stub
											form.hideWaitSymbol(); //vijay
										}
									});
									
									
									
								}
							}
							
							
						}
						
					});
					
				}
				else{
					String typeFlow="";
					if(model.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESALES)){
						typeFlow=AppConstants.BILLINGSALESFLOW;
					}
					if(model.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE)){
						typeFlow=AppConstants.BILLINGPURCHASEFLOW;
					}
					if(model.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESERVICE)){
						typeFlow=AppConstants.BILLINGSERVICEFLOW;
					}
					
					MyQuerry querry = new MyQuerry();
				  	Company c = new Company();
				  	Vector<Filter> filtervec=new Vector<Filter>();
				  	Filter filter = null;
				  	filter = new Filter();
				  	filter.setQuerryString("companyId");
					filter.setLongValue(model.getCompanyId());
					filtervec.add(filter);
					filter = new Filter();
					filter.setQuerryString("contractId");
					filter.setIntValue(model.getContractCount());
					filtervec.add(filter);
					filter = new Filter();
					filter.setQuerryString("identifyOrder");
					filter.setStringValue(typeFlow.trim());
					filtervec.add(filter);
					querry.setFilters(filtervec);
					querry.setQuerryObject(new TaxesAndCharges());
					async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {

						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							form.hideWaitSymbol();//vijay
						}

						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							// TODO Auto-generated method stub
							if(result.size()!=0){
								TaxesAndCharges billBalance=(TaxesAndCharges) result.get(0);
								
									if(billBalance!=null){
										List<ContractCharges> saveBalance=billBalance.getTaxesChargesList();
										for(int i=0;i<saveBalance.size();i++){
											saveBalance.get(i).setBillingId(model.getCount());
										}
										ArrayList<ContractCharges> saveBalArr=new ArrayList<ContractCharges>();
										saveBalArr.addAll(saveBalance);
										
										
										model.setBillingOtherCharges(saveBalArr);
										/**
										 * nidhi
										 * 18-11-2017
										 * this changes done bcoz when contract and bill created using upload process
										 * we not assign any charges and text 
										 * 
										 */
//										async.save(model, new AsyncCallback<ReturnFromServer>() {
//											
//											@Override
//											public void onSuccess(ReturnFromServer result) {
//												// TODO Auto-generated method stub
//												form.tbstatus.setValue(BillingDocument.APPROVED);
//												form.changeProcessLevel();
//												form.showDialogMessage("Successfully Submitted");
//												Console.log("Successfully Submitted::----"+new Date());
//											}
//											
//											
//											@Override
//											public void onFailure(Throwable caught) {
//												// TODO Auto-generated method stub
//												
//											}
//										});
										
									}
								
							}
							/**
							 * nidhi
							 * 18-11-2017
							 * this changes done bcoz when contract and bill created using upload process
							 * we not assign any charges and text 
							 * 
							 */
							async.save(model, new AsyncCallback<ReturnFromServer>() {
								
								@Override
								public void onSuccess(ReturnFromServer result) {
									// TODO Auto-generated method stub
									form.tbstatus.setValue(BillingDocument.APPROVED);
									form.changeProcessLevel();
									form.showDialogMessage("Successfully Submitted");
									Console.log("Successfully Submitted::----"+new Date());
									form.hideWaitSymbol();//vijay
									
									/*** Date 13-11-2019 by Vijay for NBHC CCPM ***/
									updateRateContractServiceValue();
								}
								
								
								@Override
								public void onFailure(Throwable caught) {
									// TODO Auto-generated method stub
									form.hideWaitSymbol(); //vijay
								}
							});
							
						}
						
					});
					
				}
			
			}
			
	private List<ContractCharges> updateChargesBalance() {
		System.out.println("Updating Balance Code");
		List<ContractCharges> updateBalanceLis = model.getBillingOtherCharges();
		final ArrayList<ContractCharges> updateBalanceArr = new ArrayList<ContractCharges>();
		double balVal = 0;
		for (int i = 0; i < updateBalanceLis.size(); i++) {
			ContractCharges ccObj = new ContractCharges();
			ccObj.setTaxChargeName(updateBalanceLis.get(i).getTaxChargeName());
			ccObj.setTaxChargePercent(updateBalanceLis.get(i)
					.getTaxChargePercent());
			ccObj.setTaxChargeAbsVal(updateBalanceLis.get(i)
					.getTaxChargeAbsVal());
			ccObj.setTaxChargeAssesVal(updateBalanceLis.get(i)
					.getTaxChargeAssesVal());
			if (updateBalanceLis.get(i).getPaypercent() != 100
					&& updateBalanceLis.get(i).getPaypercent() != 0) {
				balVal = updateBalanceLis.get(i).getPaypercent()
						* updateBalanceLis.get(i).getChargesBalanceAmt() / 100;
				balVal = updateBalanceLis.get(i).getChargesBalanceAmt()
						- balVal;
			}
			if (updateBalanceLis.get(i).getPaypercent() == 0
					&& updateBalanceLis.get(i).getChargesBalanceAmt() != 0) {
				balVal = updateBalanceLis.get(i).getChargesBalanceAmt();
			}
			if (updateBalanceLis.get(i).getPaypercent() == 100) {
				balVal = 0;
			}
			ccObj.setChargesBalanceAmt(balVal);
			updateBalanceArr.add(ccObj);
		}

		return updateBalanceArr;
	}		
			
	/**
	 * Added by Rahul on 12 June 2017
	 * This will create new billing document if payable amount is less than the billing amount
	 * 
	 * @return void
	 */
	private void createNewBillingDocumentAgainstPayable() {

		GenricServiceAsync async = GWT.create(GenricService.class);
		List<SalesOrderProductLineItem> salesProductsLis = updateProductTable();
		ArrayList<SalesOrderProductLineItem> salesProductsArr = new ArrayList<SalesOrderProductLineItem>();
		salesProductsArr.addAll(salesProductsLis);
		BillingDocument billdetails = new BillingDocument();
		billdetails.setPersonInfo(model.getPersonInfo());
		billdetails.setContractCount(model.getContractCount());
		billdetails.setTotalSalesAmount(model.getTotalSalesAmount());
		billdetails.setSalesOrderProducts(salesProductsArr);
		billdetails.setBillingDate(model.getBillingDate());
		billdetails.setArrPayTerms(model.getArrPayTerms());
		billdetails.setContractStartDate(model.getContractStartDate());
		billdetails.setContractEndDate(model.getContractEndDate());
		List<ContractCharges> taxesLis = updateTaxesTable(salesProductsLis);
		ArrayList<ContractCharges> arrcc = new ArrayList<ContractCharges>();
		arrcc.addAll(taxesLis);
		billdetails.setBillingTaxes(arrcc);
		if (model.getCompanyId() != null)
			billdetails.setCompanyId(model.getCompanyId());
		if (model.getBillingCategory() != null)
			billdetails.setBillingCategory(model.getBillingCategory());
		if (model.getBillingType() != null)
			billdetails.setBillingType(model.getBillingType());
		if (model.getBillingGroup() != null)
			billdetails.setBillingGroup(model.getBillingGroup());
		billdetails.setApproverName(model.getApproverName());
		billdetails.setEmployee(model.getEmployee());
		billdetails.setBranch(model.getBranch());
		if (model.getInvoiceDate() != null)
			billdetails.setInvoiceDate(model.getInvoiceDate());
		if (model.getPaymentDate() != null)
			billdetails.setPaymentDate(model.getPaymentDate());

		billdetails.setStatus(BillingDocument.CREATED);
		billdetails.setOrderCformStatus(model.getOrderCformStatus());
		billdetails.setOrderCformPercent(model.getOrderCformPercent());
		billdetails.setOrderCreationDate(model.getOrderCreationDate());
		billdetails.setTypeOfOrder(model.getTypeOfOrder());
		billdetails.setAccountType(model.getAccountType());
		/**
		 * Date : 22-11-2017 BY ANIL
		 */
		System.out.println("OLD NUM RANGE : "+model.getNumberRange());
		if(model.getNumberRange()!=null){
			billdetails.setNumberRange(model.getNumberRange());
		}
		System.out.println("NEW NUM RANGE : "+billdetails.getNumberRange());
		if(model.getSegment()!=null){
			billdetails.setSegment(model.getSegment());
		}
		if(model.getRefNumber()!=null){
			billdetails.setRefNumber(model.getRefNumber());
		}
		double totBillAmt=0;
		double taxAmt=0;
		for(SalesOrderProductLineItem item:billdetails.getSalesOrderProducts()){
			totBillAmt=totBillAmt+item.getBaseBillingAmount();
		}
		for(ContractCharges chrg:billdetails.getBillingTaxes()){
			taxAmt=taxAmt+chrg.getPayableAmt();
		}
		billdetails.setTotalAmount(totBillAmt);
		billdetails.setFinalTotalAmt(totBillAmt);
		double totalamtincludingtax=totBillAmt+taxAmt;
		billdetails.setTotalAmtIncludingTax(totalamtincludingtax);
		billdetails.setGrandTotalAmount(totalamtincludingtax);
		billdetails.setTotalBillingAmount(totalamtincludingtax);
		
		billdetails.setComment(model.getComment());
		billdetails.setBillingPeroidFromDate(model.getBillingPeroidFromDate());
		billdetails.setBillingPeroidToDate(model.getBillingPeroidToDate());
		/**
		 * End
		 */

		/** Date 11-08-2020 by Vijay for Project Name mapping ***/
		if(model.getProjectName()!=null){
			billdetails.setProjectName(model.getProjectName());
		}
		if(model.getSubBillType()!=null){
			billdetails.setSubBillType(model.getSubBillType());
		}
		/** Date 14-08-2020 by Vijay  CNC contract number mapping in biling document ***/
		if(model.getCncContractNumber()!=null){
			billdetails.setCncContractNumber(model.getCncContractNumber());
		}
		billdetails.setCreditPeriod(model.getCreditPeriod());
		
		async.save(billdetails, new AsyncCallback<ReturnFromServer>() {

			@Override
			public void onFailure(Throwable caught) {
				form.showDialogMessage("Unable to save new bill");
			}

			@Override
			public void onSuccess(ReturnFromServer result) {
			}
		});
	}
	
	/**
	 * Added by Rahul Verma 
	 * Date : 12 June 2017
	 * @param salesProductsLis
	 * @return list of ContractCharges
	 */
	private List<ContractCharges> updateTaxesTable(
			List<SalesOrderProductLineItem> salesProductsLis) {
		
		ArrayList<ContractCharges> arrConCharges=new ArrayList<ContractCharges>();
		double totalAmouunt=0;
		double totalAmount1=0;   
		
		for(SalesOrderProductLineItem item:salesProductsLis){
			totalAmouunt=totalAmouunt+item.getBaseBillingAmount();
		}
		
		for(ContractCharges tax:model.getBillingTaxes()){
			if(tax.getTaxChargeName().trim().equalsIgnoreCase("VAT") ||tax.getTaxChargeName().trim().equalsIgnoreCase("CST")){
				ContractCharges ccent=new ContractCharges();
				ccent.setTaxChargeName(tax.getTaxChargeName());
				ccent.setTaxChargePercent(tax.getTaxChargePercent());
				ccent.setTaxChargeAssesVal(totalAmouunt);
				
				double totalCalcAmt=tax.getTaxChargeAssesVal()*tax.getTaxChargePercent()/100;
				ccent.setPayableAmt(totalCalcAmt);
				arrConCharges.add(ccent);
				
				totalAmount1=totalAmount1+totalAmouunt+totalCalcAmt;
			}
			/**
			 * Date : 22-11-2017 BY ANIL
			 * updating tax table according to gst
			 */
			else if(tax.getTaxChargeName().trim().equalsIgnoreCase("CGST") ||tax.getTaxChargeName().trim().equalsIgnoreCase("SGST")||tax.getTaxChargeName().trim().equalsIgnoreCase("IGST")){
				ContractCharges ccent=new ContractCharges();
				ccent.setTaxChargeName(tax.getTaxChargeName());
				ccent.setTaxChargePercent(tax.getTaxChargePercent());
				ccent.setTaxChargeAssesVal(totalAmouunt);
				
				double totalCalcAmt=tax.getTaxChargeAssesVal()*tax.getTaxChargePercent()/100;
				ccent.setPayableAmt(totalCalcAmt);
				arrConCharges.add(ccent);
			}
		}
	
	
	for(ContractCharges tax:model.getBillingTaxes()){
		if(tax.getTaxChargeName().trim().equalsIgnoreCase("Service Tax")){
			ContractCharges ccent=new ContractCharges();
			ccent.setTaxChargeName(tax.getTaxChargeName());
			ccent.setTaxChargePercent(tax.getTaxChargePercent());
			ccent.setTaxChargeAssesVal(totalAmount1);
			double totalCalcAmt=tax.getTaxChargeAssesVal()*tax.getTaxChargePercent()/100;
			ccent.setPayableAmt(totalCalcAmt);
			arrConCharges.add(ccent);
		}
	}
	
	return arrConCharges;
	
	}
	
	/**
	 * added by Rahul Verma on 12 June 2017
	 * updateProductTable
	 * @return list of SalesOrderProductLineItem
	 */
	private List<SalesOrderProductLineItem> updateProductTable() {
		ArrayList<SalesOrderProductLineItem> arrSalesProducts=new ArrayList<SalesOrderProductLineItem>();
		double calPayableAmt=0,calBaseBillAmt;
		for(int i=0;i<model.getSalesOrderProducts().size();i++)
		{
			SalesOrderProductLineItem salesOrder=new SalesOrderProductLineItem();
			salesOrder.setProdCategory(model.getSalesOrderProducts().get(i).getProdCategory());
			salesOrder.setProdCode(model.getSalesOrderProducts().get(i).getProdCode());
			salesOrder.setProdName(model.getSalesOrderProducts().get(i).getProdName());
			salesOrder.setQuantity(model.getSalesOrderProducts().get(i).getQuantity());
			salesOrder.setPrice(model.getSalesOrderProducts().get(i).getPrice());
			
			salesOrder.setVatTaxEdit(model.getSalesOrderProducts().get(i).getVatTaxEdit());
			salesOrder.setServiceTaxEdit(model.getSalesOrderProducts().get(i).getServiceTaxEdit());
			salesOrder.setVatTax(model.getSalesOrderProducts().get(i).getVatTax());
			salesOrder.setServiceTax(model.getSalesOrderProducts().get(i).getServiceTax());
			
			
			salesOrder.setTotalAmount(model.getSalesOrderProducts().get(i).getTotalAmount());
			salesOrder.setProdPercDiscount(model.getSalesOrderProducts().get(i).getProdPercDiscount());
		
			
			
			//  rohan added this code
			salesOrder.setPrduct(model.getSalesOrderProducts().get(i).getPrduct());
			
			calPayableAmt=((model.getSalesOrderProducts().get(i).getBaseBillingAmount()-model.getSalesOrderProducts().get(i).getFlatDiscount())*model.getSalesOrderProducts().get(i).getPaymentPercent())/100;
			calBaseBillAmt=model.getSalesOrderProducts().get(i).getBaseBillingAmount()-calPayableAmt;
			salesOrder.setBaseBillingAmount(calBaseBillAmt);
			if(model.getSalesOrderProducts().get(i).getPaymentPercent()==100){
				salesOrder.setPaymentPercent(0.0);
			}
			else{
				salesOrder.setPaymentPercent(100.0);
			}
			
			salesOrder.setIndexVal(model.getSalesOrderProducts().get(i).getIndexVal());
			/**
			 * nidhi
			 * 20-06-2018
			 */
			salesOrder.setProductSrNumber(model.getSalesOrderProducts().get(i).getProductSrNumber());
			/** Date 09-02-2018 By vijay for partial billing document setting base billing amt **/
			salesOrder.setBasePaymentAmount(calBaseBillAmt);
			/** Date 15-02-2018 By vijay partial biling setting HSN Code **/
			if(model.getSalesOrderProducts().get(i).getHsnCode()!=null && !model.getSalesOrderProducts().get(i).getHsnCode().equals(""))
			salesOrder.setHsnCode(model.getSalesOrderProducts().get(i).getHsnCode());
			/*** Date 13-08-2020 by Vijay for sasha man power for partila billing ***/
			if(model.getSalesOrderProducts().get(i).getManpower()!=null && !model.getSalesOrderProducts().get(i).getManpower().equals("")){
				salesOrder.setManpower(model.getSalesOrderProducts().get(i).getManpower());
			}
			if(model.getSalesOrderProducts().get(i).getArea()!=null && !model.getSalesOrderProducts().get(i).getArea().equals("")){
				salesOrder.setArea(model.getSalesOrderProducts().get(i).getArea());
			}
			if(model.getSalesOrderProducts().get(i).getBillType()!=null && !model.getSalesOrderProducts().get(i).getBillType().equals("") ){
				salesOrder.setBillType(model.getSalesOrderProducts().get(i).getBillType());
			}
			
			/*** Date 16-06-2020 added by Vijay when CNC partial bill raise **/
			if(model.getSalesOrderProducts().get(i).getCtcAmount()!=0){
				salesOrder.setCtcAmount(model.getSalesOrderProducts().get(i).getCtcAmount());
			}
			if(model.getSalesOrderProducts().get(i).getManagementFees()!=0){
				salesOrder.setManagementFees(model.getSalesOrderProducts().get(i).getManagementFees());
			}
			
			/**
			 * @author Anil , @since 07-07-2021
			 * in case of partial bill no. of services was coming zero on pdf beacuse data was not mapped on updated bill
			 * issue raised by Ashwini for Ompest
			 */
			salesOrder.setOrderServices(model.getSalesOrderProducts().get(i).getOrderServices());
			arrSalesProducts.add(salesOrder);
		}
		return arrSalesProducts;
	}
	
			
			/**
			 * Added by Rahul on 12 June 2017
			 * This will check payable amount is valid
			 * 
			 * @return boolean
			 */
			private boolean checkPayableAmount() {
				double baseBilling=0,payableAmt=0;
				for(int i=0;i<model.getSalesOrderProducts().size();i++)
				{
					baseBilling=baseBilling+model.getSalesOrderProducts().get(i).getBaseBillingAmount() - model.getSalesOrderProducts().get(i).getFlatDiscount();
					payableAmt=payableAmt+((model.getSalesOrderProducts().get(i).getBaseBillingAmount()- model.getSalesOrderProducts().get(i).getFlatDiscount())*model.getSalesOrderProducts().get(i).getPaymentPercent())/100;
				}
				if(payableAmt<baseBilling)
				{
					return true;
				}
				return false;
			}
			
			/**
			 * This method is used for validating payment percent in product table in billing screen.
			 * If no of billing documents is equal to one and payment percent in product table is less
			 * then 100 % then returns false else return true
			 */
			
			private boolean validateProductBilling()
			{
				List<SalesOrderProductLineItem> formProductLis=form.getSalesProductTable().getDataprovider().getList();
				for(int i=0;i<formProductLis.size();i++)
				{
					if(formProductLis.get(i).getPaymentPercent()!=100){
						return false;
					}
				}
				return true;
			}
			
			/**
			 * This method is used for validating payment percent in charges table in billing screen.
			 * If no of billing documents is equal to one and payment percent in product table is 100%
			 *  and payment percent in charges table is less than 100 % then returns false else return true
			 */
			
			private boolean validateChargesPercent()
			{
				List<ContractCharges> billChargeLis=form.getBillingChargesTable().getDataprovider().getList();
				for(int i=0;i<billChargeLis.size();i++)
				{
					if(billChargeLis.get(i).getPaypercent()!=100&&billChargeLis.get(i).getChargesBalanceAmt()!=0){
						return false;
					}
				}
				return true;
			}
			
			
			@Override
			public void onClick(ClickEvent event) {
				super.onClick(event);
				
				if(event.getSource()==frmAndToPop.btnOne){
					getTaxesDetails();
					panel.hide();
				}
				
				if(event.getSource()==frmAndToPop.btnTwo){
					panel.hide();
				}
				if(event.getSource()==popup.getBtnOk()){
//					panel.hide();
//					
//					if(form.getstatustextbox().getValue().equals(BillingDocument.APPROVED)){
//						createNewBillingDocument();
//						form.showDialogMessage("Billing Document Cancelled!");
//					}else if(form.getstatustextbox().getValue().equals(BillingDocument.BILLINGINVOICED)){
//						statusOfInvoiceFrombillingID();
//					}else if(form.getstatustextbox().getValue().equals(BillingDocument.CREATED)){
//						cancelBilligDocument();
//					}
					if(popup.getRemark().getValue().trim()!= null && !popup.getRemark().getValue().trim().equals("")){
						panel.hide();
						
						if(form.getstatustextbox().getValue().equals(BillingDocument.APPROVED)){
							createNewBillingDocument();
							form.showDialogMessage("Billing Document Cancelled!");
						}else if(form.getstatustextbox().getValue().equals(BillingDocument.BILLINGINVOICED)){
							statusOfInvoiceFrombillingID();
						}else if(form.getstatustextbox().getValue().equals(BillingDocument.CREATED)){
							cancelBilligDocument();
						}

					}
					else{
						form.showDialogMessage("Please enter valid remark to cancel Billing Document..");
					}

				}
				else if(event.getSource()==popup.getBtnCancel()){
					panel.hide();
					popup.remark.setValue("");
				}
				
				//Date 31 july 2017 added by vijay popup here getting null pointer exception
				if(form.getSearchpopupscreen()!=null){
				if(event.getSource()==form.getSearchpopupscreen().getGolbl()){
					if(BillingDetailsForm.flag==false){
						if(BillingDetailsForm.flag1==false){
							BillingDetailsForm.flag2=false;
						}
					}
				}
				
				}
				
				/***
				 * Date 04-04-2019 by Vijay for NBHC CCPM AutoInvoice when submit Tax invoice bill then
				 * directly invoice will created and send request for Approval
				 */
				if(event.getSource()==autoInvoicePopup.getLblOk()){
					/**
					 * @author Anil
					 * @since 09-07-2020
					 * hiding ok button after click to avoid multiple invoice creation
					 */
					autoInvoicePopup.getLblOk().setVisible(false);
					autoInvoicePopup.getLblOk().setText("Processing");
					if(autoInvoicePopup.validate()){
//							model.setStatus(BillingDocument.BILLINGINVOICED);
//							form.tbstatus.setValue(BillingDocument.BILLINGINVOICED);
//							reactOnTaxInvoice();
							String invoiceCategory = autoInvoicePopup.getOlbInvoiceCategory().getValue(autoInvoicePopup.getOlbInvoiceCategory().getSelectedIndex());
							String invoiceType = autoInvoicePopup.getOlbInvoiceConfigType().getValue(autoInvoicePopup.getOlbInvoiceConfigType().getSelectedIndex());
							String invoiceGruop = autoInvoicePopup.getOlbinvoicegroup().getValue(autoInvoicePopup.getOlbinvoicegroup().getSelectedIndex());
							String approverName = autoInvoicePopup.getOlbApproverName().getValue(autoInvoicePopup.getOlbApproverName().getSelectedIndex());
							
//							reactOnInvoice(AppConstants.CREATETAXINVOICE,invoiceCategory,invoiceType,invoiceGruop,approverName);
							reactonAutoInvoice(AppConstants.CREATETAXINVOICE,invoiceCategory,invoiceType,invoiceGruop,approverName);
							
							
							autoInvoicePopup.hidePopUp();
					}
					else{
						autoInvoicePopup.getLblOk().setText("OK");
					}
				}
				if(event.getSource()==autoInvoicePopup.getLblCancel()){
					autoInvoicePopup.hidePopUp();
				}

			}
			
			

			private void cancelBilligDocument() {
				
				 model.setStatus(BillingDocument.CANCELLED);
				 /**
				  * Updated By: Viraj
				  * Date: 15-06-2019
				  * Description: To set remark when document is cancelled
				  */
				 Console.log("remark: " + popup.getRemark().getValue().trim());
				 model.setRemark("This document was cancelled with remark" + popup.getRemark().getValue().trim());
				 
				 model.setComment("Billing Id ="+model.getCount()+" "+"Billing Status ="+model.getStatus()+"\n"
							+"has been cancelled by "+LoginPresenter.loggedInUser.trim()
							+" with remark"+"\n"+"Remark ="+popup.getRemark().getValue());
				 /** Ends **/
			     async.save(model,new AsyncCallback<ReturnFromServer>() {
			    	 @Override
						public void onFailure(Throwable caught) {
							form.showDialogMessage("An Unexpected Error occured !");
						}
						@Override
						public void onSuccess(ReturnFromServer result) {
							 form.setToViewState();
							 form.setAppHeaderBarAsPerStatus();
							 form.getTbstatus().setValue(model.getStatus());
							 form.getTbremark().setValue(model.getRemark());  		//Updated By:Viraj Date: 15-06-2019 Description: to show remark when screen loads
							 form.getTacomment().setValue(model.getComment()); 		//Updated By:Viraj Date: 05-07-2019 Description: to show remark when screen loads
						}
					});	  
			}

			private void statusOfInvoiceFrombillingID() {
				
				final int invoiceID=Integer.parseInt(form.getIbinvoiceid().getValue().trim());
				
				MyQuerry querry = new MyQuerry();
			  	Company c = new Company();
			  	Vector<Filter> filtervec=new Vector<Filter>();
			  	Filter filter = null;
			  	filter = new Filter();
			  	filter.setQuerryString("companyId");
				filter.setLongValue(c.getCompanyId());
				filtervec.add(filter);
				filter = new Filter();
				filter.setQuerryString("count");
				filter.setIntValue(invoiceID);
				filtervec.add(filter);
				filter = new Filter();
				filter.setQuerryString("typeOfOrder");
				filter.setStringValue(model.getTypeOfOrder().trim());
				filtervec.add(filter);
				/**
				 * Updated By: Viraj
				 * Date: 12-02-2019
				 * Decription: To cancel vendor invoice.
				 */
				if(model.getAccountType().equalsIgnoreCase(AppConstants.BILLINGACCOUNTTYPEAP)) {
					querry.setFilters(filtervec);
					querry.setQuerryObject(new VendorInvoice());
				} else {
					querry.setFilters(filtervec);
					querry.setQuerryObject(new Invoice());
				}
				/** Ends **/
				async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
		  			
		  			@Override
		  			public void onFailure(Throwable caught) {
		  			}

		  			@Override
					public void onSuccess(ArrayList<SuperModel> result) {
		  				int cnt=0;
		  				int requested=0;
		  				int pic=0;
		  				int invoice=0;
		  				int pinvoice=0; 
		  				ArrayList<Invoice> slist=new ArrayList<Invoice>();

		  				for(SuperModel model:result)
						{
		  					Invoice invoiceEntity= (Invoice)model;
		  					invoiceEntity.setStatus(invoiceEntity.getStatus());
							invoiceEntity.setInvoiceCount(invoiceEntity.getCount());
							slist.add(invoiceEntity);
						}
		  				
		  				
		  				for(int i=0;i<slist.size();i++)
		  				{
							if(invoiceID==slist.get(i).getCount());
							{
								if(slist.get(i).getStatus().equals(Invoice.CREATED)){
									 pic=pic+1;
								}
								
								if(slist.get(i).getStatus().equals(Invoice.APPROVED)){
									cnt=cnt+1;
								}
								
								if(slist.get(i).getStatus().equals(Invoice.REQUESTED)){
									    requested=requested+1;
								}
								
								if(slist.get(i).getStatus().equals(Invoice.REJECTED))
								{
								 	pic=pic+1;
								}
								if(slist.get(i).getStatus().equals(BillingDocument.BILLINGINVOICED))
								{
								 	invoice=invoice+1;
								}
								
								if(slist.get(i).getStatus().equals(BillingDocument.PROFORMAINVOICE))
								{
								 	pinvoice=pinvoice+1;
								}
								
								/**
								 * Date 06-02-2018 by vijay for Billing document cancel issue and set to created status
								 */
								if(slist.get(i).getStatus().equals(BillingDocument.CANCELLED))
								{
								 	createNewBillingDocument();
								}
								/**
								 * ends here
								 */
								
							}
						} 
		  				
						if(cnt>0){
							form.showDialogMessage("Invoice status is approved. Cancel invoice first!");
//							MyQuerry querry = new MyQuerry();
//						  	Company c = new Company();
//						  	Vector<Filter> filtervec=new Vector<Filter>();
//						  	Filter filter = null;
//						  	filter = new Filter();
//						  	filter.setQuerryString("companyId");
//							filter.setLongValue(c.getCompanyId());
//							filtervec.add(filter);
//							filter = new Filter();
//							filter.setQuerryString("invoiceCount");
//							filter.setIntValue(invoiceID);
//							filtervec.add(filter);
//							querry.setFilters(filtervec);
//							querry.setQuerryObject(new CustomerPayment());
//							async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
//					  			
//					  			@Override
//					  			public void onFailure(Throwable caught) {
//					  			}
//
//					  			@Override
//								public void onSuccess(ArrayList<SuperModel> result) {
//					  				int custPayCnt=0;
//					  				
//					  				ArrayList<CustomerPayment> slist=new ArrayList<CustomerPayment>();
//					  				
//									for(SuperModel model:result)
//									{
//										CustomerPayment custPaymentEntity= (CustomerPayment)model;
//										custPaymentEntity.setStatus(custPaymentEntity.getStatus());
//										custPaymentEntity.setInvoiceCount(custPaymentEntity.getInvoiceCount());
//										slist.add(custPaymentEntity);
//									}
//						
//									for(int i=0;i<slist.size();i++){
//										
//									if(form.getIbinvoiceid().getValue().trim().equals(slist.get(i).getInvoiceCount()));
//									{
//										if(slist.get(i).getStatus().equals(CustomerPayment.PAYMENTCLOSED)){
//											custPayCnt=custPayCnt+1;
//										}
//									}
//									} 
//									if(custPayCnt>0){
//										form.showDialogMessage("Document cannot be cancelled because payment status is closed.");
//									}
//									else{
//										changeCustomerPaymentStatus();
//										changeInvoiceStatus();
//										createNewBillingDocument();
//										form.showDialogMessage("Billing Document Cancelled and corresponding Invoice and Payment details also get cancelled");
//									}
//									}
//							});
//							
//						//************************************************************
						}
						else if(invoice > 0)
						{
							form.showDialogMessage("Invoice status is Invoice. Cancel Proforma Invoice first!");
						}
						else {
							if(pic>0){
								changeInvoiceStatus();
								createNewBillingDocument();
								
							}
							if(requested>0)
							{
								changeInvoiceStatus();
								createNewBillingDocument();
							}
							
							if(pinvoice > 0)
							{
								changeInvoiceStatus();
								createNewBillingDocument();
							}
						}
		  				
		  			}
				});
			}
			
			

			public void changeCustomerPaymentStatus()
			{
				MyQuerry querry = new MyQuerry();
			  	Company c = new Company();
			  	Vector<Filter> filtervec=new Vector<Filter>();
			  	Filter filter = null;
			  	filter = new Filter();
			  	filter.setQuerryString("companyId");
				filter.setLongValue(c.getCompanyId());
				filtervec.add(filter);
				filter = new Filter();
				filter.setQuerryString("invoiceCount");
				filter.setIntValue(model.getInvoiceCount());
				filtervec.add(filter);
				querry.setFilters(filtervec);
				querry.setQuerryObject(new CustomerPayment());
				async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
		  			
		  			@Override
		  			public void onFailure(Throwable caught) {
		  			}

		  			@Override
					public void onSuccess(ArrayList<SuperModel> result) {
		  				
		  				for(SuperModel smodel:result)
							{
		  					CustomerPayment custpay = (CustomerPayment)smodel;
								custpay.setStatus(CustomerPayment.CANCELLED);
								custpay.setComment("Billing Id ="+model.getCount()+" "+"Billing Status ="+model.getStatus()+"\n"
								+"has been cancelled by "+LoginPresenter.loggedInUser.trim()
								+" with remark "+"\n"+"Remark ="+popup.getRemark().getValue());
								async.save(custpay,new AsyncCallback<ReturnFromServer>() {
		//
									@Override
									public void onFailure(Throwable caught) {
										form.showDialogMessage("An Unexpected Error occured !");
//										
									}
									@Override
									public void onSuccess(ReturnFromServer result) {
									}
								});
							}
		  				
		  			}
				
				});
			}
				
			
			private void changeInvoiceStatus()
			{
				final int billingId=Integer.parseInt(form.getIbbillingid().getValue());
				final String billingStatus=form.getstatustextbox().getValue();
				
				MyQuerry querry = new MyQuerry();
			  	Company c = new Company();
			  	Vector<Filter> filtervec=new Vector<Filter>();
			  	Filter filter = null;
			  	filter = new Filter();
			  	filter.setQuerryString("companyId");
				filter.setLongValue(c.getCompanyId());
				filtervec.add(filter);
				filter = new Filter();
				filter.setQuerryString("count");
				filter.setIntValue(Integer.parseInt(form.getIbinvoiceid().getValue().trim()));
				filtervec.add(filter);
				querry.setFilters(filtervec);
				querry.setQuerryObject(new Invoice());
				async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
		  			
		  			@Override
		  			public void onFailure(Throwable caught) {
		  			}

		  			@Override
					public void onSuccess(ArrayList<SuperModel> result) {
		  				
		  				for(SuperModel smodel:result)
						{
		  					Invoice invoicepay = (Invoice)smodel;
		  					if(invoicepay.getStatus().equals(Invoice.CREATED))
		  					{
		  					invoicepay.setStatus(Invoice.CANCELLED);
		  					invoicepay.setComment("Billing Id ="+billingId+" "+billingStatus+"\n"+
							"has been cancelled by "+LoginPresenter.loggedInUser+" with remark."+"\n"+"Remark ="+popup.getRemark().getValue());
								
		  					
		  					async.save(invoicepay,new AsyncCallback<ReturnFromServer>() {
									
									@Override
									public void onFailure(Throwable caught) {
										form.showDialogMessage("An Unexpected Error occured !");
//										
									}
									@Override
									public void onSuccess(ReturnFromServer result) {
									}
								});
							}
		  					
		  					if(invoicepay.getStatus().equals(Invoice.APPROVED))
		  					{
		  						
		  					invoicepay.setStatus(Invoice.CANCELLED);
		  					invoicepay.setComment("Billing Id ="+billingId+" "+billingStatus+"\n"+
							"has been cancelled by "+LoginPresenter.loggedInUser+" with remark."+"\n"+"Remark ="+popup.getRemark().getValue());
								
		  					async.save(invoicepay,new AsyncCallback<ReturnFromServer>() {
									
									@Override
									public void onFailure(Throwable caught) {
										form.showDialogMessage("An Unexpected Error occured !");
//										
									}
									@Override
									public void onSuccess(ReturnFromServer result) {
									}
								});
							}
		  					
		  					if(invoicepay.getStatus().equals(Invoice.REJECTED))
		  					{
		  					invoicepay.setStatus(Invoice.CANCELLED);
		  					invoicepay.setComment("Billing Id ="+billingId+" "+billingStatus+"\n"+
							"has been cancelled by "+LoginPresenter.loggedInUser+" with remark."+"\n"+"Remark ="+popup.getRemark().getValue());
								
		  					async.save(invoicepay,new AsyncCallback<ReturnFromServer>() {
									
									@Override
									public void onFailure(Throwable caught) {
										form.showDialogMessage("An Unexpected Error occured !");
										
									}
									@Override
									public void onSuccess(ReturnFromServer result) {
									}
								});
							}
		  					
		  					
		  					//    rohan added this for cancellation of proforma invoice
		  					
		  					if(invoicepay.getStatus().equals(BillingDocument.PROFORMAINVOICE))
		  					{
		  					invoicepay.setStatus(Invoice.CANCELLED);
		  					invoicepay.setComment("Billing Id ="+billingId+" "+billingStatus+"\n"+
							"has been cancelled by "+LoginPresenter.loggedInUser+" with remark."+"\n"+"Remark ="+popup.getRemark().getValue());
								
		  					async.save(invoicepay,new AsyncCallback<ReturnFromServer>() {
									
									@Override
									public void onFailure(Throwable caught) {
										form.showDialogMessage("An Unexpected Error occured !");
										
									}
									@Override
									public void onSuccess(ReturnFromServer result) {
									}
								});
							}
		  					
		  					if(invoicepay.getStatus().equals(Invoice.REQUESTED))
		  					{
		  					invoicepay.setStatus(Invoice.CANCELLED);
		  					invoicepay.setComment("Billing Id ="+billingId+" "+billingStatus+"\n"+
							"has been cancelled by "+LoginPresenter.loggedInUser+" with remark."+"\n"+"Remark ="+popup.getRemark().getValue());
								
		  					async.save(invoicepay,new AsyncCallback<ReturnFromServer>() {
									
									@Override
									public void onFailure(Throwable caught) {
										form.showDialogMessage("An Unexpected Error occured !");
//										
									}
									@Override
									public void onSuccess(ReturnFromServer result) {
									}
								});
								
		  						MyQuerry querry = new MyQuerry();
							  	Company c = new Company();
							  	Vector<Filter> filtervec=new Vector<Filter>();
							  	Filter filter = null;
							  	filter = new Filter();
							  	filter.setQuerryString("companyId");
								filter.setLongValue(c.getCompanyId());
								filtervec.add(filter);
								filter = new Filter();
								filter.setQuerryString("businessprocessId");
								filter.setIntValue(invoicepay.getCount());
								filtervec.add(filter);
								querry.setFilters(filtervec);
								querry.setQuerryObject(new Approvals());
								async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
						  			
						  			@Override
						  			public void onFailure(Throwable caught) {
						  			}

						  			@Override
									public void onSuccess(ArrayList<SuperModel> result) {
						  				
						  				for(SuperModel smodel:result)
										{
						  					Approvals approval = (Approvals)smodel;
						  					approval.setStatus(Approvals.CANCELLED);
						  					
						  					approval.setRemark("Billing Id ="+billingId+" "+billingStatus+"\n"+
						  							"has been cancelled by "+LoginPresenter.loggedInUser+" with remark."+"\n"+"Remark ="+popup.getRemark().getValue());
											
						  					async.save(approval,new AsyncCallback<ReturnFromServer>() {
												
												@Override
												public void onFailure(Throwable caught) {
													form.showDialogMessage("An Unexpected Error occured !");
												}
												@Override
												public void onSuccess(ReturnFromServer result) {
												
												}
											});
										}
						  			}
						  			});
							}
		  					
							}
		  			}
				});
			}
			
			
			
			private void createNewBillingDocument()
			{

		    	  model.setStatus(BillingDocument.CREATED);
			     async.save(model,new AsyncCallback<ReturnFromServer>() {
			    	 @Override
						public void onFailure(Throwable caught) {
							form.showDialogMessage("An Unexpected Error occured !");
						}
						@Override
						public void onSuccess(ReturnFromServer result) {
							 form.setToViewState();
							 form.setAppHeaderBarAsPerStatus();
							 form.getTbstatus().setValue(model.getStatus());
						}
					});	  
		      }
			
			
		/**************************************Approval Logic*************************************/
			
			
			private void reactOnReqApproval()
			{
				
				/**
				 * Updated By Anil Date : 14-10-2016
				 * Release : 30 Sept 2016 
				 * Project : PURCHASE MODIFICATION(NBHC)
				 */
				String documentCreatedBy="";
				if(model.getCreatedBy()!=null){
					documentCreatedBy=model.getCreatedBy();
				}
				
//				form.showWaitSymbol();//vijay
				ApproverFactory appFactory=new ApproverFactory();
//				Approvals approval=appFactory.getApprovals(model.getEmployee(), model.getApproverName()
//						, model.getBranch(), model.getCount(),documentCreatedBy);
				/**
				 *  nidhi
				 *  7-10-2017
				 *  method change for display name and id
				 */
				Approvals approval=appFactory.getApprovals(model.getEmployee(), model.getApproverName()
						, model.getBranch(), model.getCount(),documentCreatedBy,model.getCustomerId()+"",model.getPersonInfo().getFullName());
				
				saveApprovalReq(approval,ConcreteBusinessProcess.REQUESTED,"Request Sent!",true);
				
				reactOnApprovalEmail(approval);
				System.out.println("SENT");
				
				
			}
			
			private void saveApprovalReq(Approvals approval,final String status,final String dialogmsg,final boolean autoInvoiceFlag)
			{
				service.save(approval, new AsyncCallback<ReturnFromServer>() {
					
					@Override
					public void onSuccess(ReturnFromServer result) 
					{
//						/*** Date 04-04-2019 by Vijay 
//						 *  Des :- for NBHC CCPM For autoInvoice not required to update model and for normal flow it will execute
//						 **/
//						if(autoInvoiceFlag){
							model.setStatus(status);
							service.save(model, new AsyncCallback<ReturnFromServer>() {

								@Override
								public void onFailure(Throwable caught) {
									form.hideWaitSymbol();
								}

								@Override
								public void onSuccess(ReturnFromServer result) {
									form.getTbstatus().setValue(status);
									form.setToViewState();
									form.showDialogMessage(dialogmsg);
									form.hideWaitSymbol();
								}
							});
//						}
						
					}
					
					@Override
					public void onFailure(Throwable caught) {
						form.showDialogMessage("Request Failed! Try Again!");
						form.hideWaitSymbol();
						
					}
				});
			}

			
			
			private void reactOnCancelApproval()
			{
		  		form.showWaitSymbol(); 
		  		ApproverFactory approverfactory=new ApproverFactory();
				  System.out.println("EEEEE"+approverfactory.getBuisnessProcessType());
				  Filter filter=new Filter();
				  filter.setStringValue(approverfactory.getBuisnessProcessType());
				  filter.setQuerryString("businessprocesstype");
				  Filter filterOnid=new Filter();
				  filterOnid.setIntValue(model.getCount());
				  filterOnid.setQuerryString("businessprocessId");
				  Vector<Filter> vecfilter=new Vector<Filter>();
				  vecfilter.add(filter);
				  vecfilter.add(filterOnid);
				  MyQuerry querry=new MyQuerry(vecfilter, new Approvals());
				  final ArrayList<SuperModel> array = new ArrayList<SuperModel>();
				  service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {

					@Override
					public void onFailure(Throwable caught) {
						caught.printStackTrace();
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						array.addAll(result);
						Approvals approval=(Approvals) array.get(0);
						approval.setStatus(Approvals.CANCELLED);
						 
						saveApprovalReq(approval, ConcreteBusinessProcess.CREATED, "Approval Request Cancelled!",true);
					}
				});
			  
			}
			
			
			public void reactOnApprovalEmail(Approvals approvalEntity)
			{
				//Quotation Entity patch patch patch
				emailService.initiateApprovalEmail(approvalEntity,new AsyncCallback<Void>() {

					@Override
					public void onFailure(Throwable caught) {
						Window.alert("Unable To Send Email");
						caught.printStackTrace();
					}

					@Override
					public void onSuccess(Void result) {
//						Window.alert("Email Sent Sucessfully !");
					}
				});
			}
			
			public void reactOnProductsChange() {
				
				form.showWaitSymbol();
				Timer timer = new Timer() {
					@Override
					public void run() {
						NumberFormat nf = NumberFormat.getFormat("0.00");
						double totalExcludingTax = form.getSalesProductTable().calculateTotalExcludingTax();
						totalExcludingTax = Double.parseDouble(nf.format(totalExcludingTax));
						form.setTotalAmount(totalExcludingTax);

						// Date 03-09-2017 added by vijay
						form.getDbtotalAmt().setValue(totalExcludingTax);
						System.out.println("TOTAL AMOUNT EXCLUDING :: "+ form.getTotalAmount());
						
						 /**
					     * Date 03-09-2017 added by vijay for final total amount after discount
					     */
					    if(form.getDbDiscountAmt().getValue()!=null && form.getDbDiscountAmt().getValue()!=0){
				    		if(form.getDbDiscountAmt().getValue()>totalExcludingTax){
					    		form.getDbDiscountAmt().setValue(0d);
				    		}
				    		else if(form.getDbDiscountAmt().getValue()<totalExcludingTax){
						    	form.getDbFinalTotalAmt().setValue(totalExcludingTax - form.getDbDiscountAmt().getValue());
				    		}
				    		else{
					    		form.getDbFinalTotalAmt().setValue(totalExcludingTax);
				    		}
				    	}
					    else{
					    	form.getDbFinalTotalAmt().setValue(totalExcludingTax);
				    	}
					    
					    /**
					     * ends here
					     */

						try {
							System.out.println();
							System.out.println("PRODUCT TAX TABLE CALCULATION BEGIN.....");
							form.billingTaxTable.connectToLocal();
							form.addProdTaxes();
							/**
							 * Date : 21-09-2017 BY ANIL
							 */
							form.addOtherChargesInTaxTbl();
							/**
							 * End
							 */
							
//							double totalIncludingTax = form.getTotalAmount()+ form.billingTaxTable.calculateTotalTaxes();
							
							// Date 03-09-2017 added by vijay old line above commented
							double totalIncludingTax = form.getDbFinalTotalAmt().getValue()+ form.billingTaxTable.calculateTotalTaxes();
							/**
							 * Date : 21-09-2017 BY ANIL
							 */
							if(form.dbOtherChargesTotal.getValue()!=null){
								totalIncludingTax=totalIncludingTax+form.dbOtherChargesTotal.getValue();
							}
							/**
							 * End
							 */
							
							form.getDbtotalAmtIncludingTax().setValue(totalIncludingTax);
							
							System.out.println("total"+totalIncludingTax);
							form.setToatalAmountIncludingTax(Double.parseDouble(nf.format(totalIncludingTax)));
							
							
//							form.getBillingChargesTable().connectToLocal();
//							form.updateChargesTable();
						} catch (Exception e) {
							e.printStackTrace();
						}
						double netPay = fillNetPayable(form.getToatalAmountIncludingTax());
						
						System.out.println("net pay"+netPay);
						/**
						 * @author Vijay Date :- 19-01-2021
						 * Des :- Round up logic not required for innovative so if below process config is active then amount
						 * will not round. Below is standard code execute for other clients
						 */
						if(!LoginPresenter.PC_NO_ROUNDOFF_INVOICE_CONTRACTFlag){
							netPay = Math.round(netPay);
							int netPayable = (int) netPay;
							netPay = netPayable;
						}

						
//						form.getDototalbillamt().setValue(Double.parseDouble(nf.format(netPay)));
						
						// above commented by vijay one line	
						/** Date 01-09-2017 added by vijay for final netpayable amount after discount **/
						form.getDbGrandTotalAmt().setValue(netPay);
						if(form.getTbroundOffAmt().getValue()!=null && !form.getTbroundOffAmt().getValue().equals("") ){
							String roundOff = form.getTbroundOffAmt().getValue();
							double roundoffAmt =  AppUtility.calculateRoundOff(roundOff, netPay);
							if(roundoffAmt!=0){
								form.getDototalbillamt().setValue(roundoffAmt);

							}else{
								form.getDototalbillamt().setValue(roundoffAmt);
								form.getTbroundOffAmt().setValue("");
							}
						}
						else{
							form.getDototalbillamt().setValue(netPay);
						}
						
						/** ends here *******/
						
						if(multipleContractBillingInvoiceFlag==false){
							Console.log("INSIDE multipleContractBillingInvoiceFlag....!!!");
							billingTableOnRowChange(Double.parseDouble(nf.format(netPay)));
						}
						form.hideWaitSymbol();
					}
				};
				timer.schedule(3000);
			}	
			

			private double fillNetPayable(double amtincltax)
			{
				double amtval=0;
				if(form.getBillingChargesTable().getDataprovider().getList().size()==0)
				{
					amtval=amtincltax;
				}
				if(form.getBillingChargesTable().getDataprovider().getList().size()!=0)
				{
					amtval=amtincltax+form.getBillingChargesTable().calculateTotalBillingCharges();
				}
				if(!LoginPresenter.PC_NO_ROUNDOFF_INVOICE_CONTRACTFlag){
					amtval=Math.round(amtval);
					int retAmtVal=(int) amtval;
					amtval=retAmtVal;
				}
				
				return amtval;
			}
//***********************mearging of tax editing in billing details ***************8888
			
			
			public boolean checkproductstatus() {
				if (form.salesProductTable.getDataprovider().getList().get(0).getPrduct() != null) {
					return true;
				}
				return false;
			}

			public void retrieveproductinfomation() {
				final ArrayList<SuperProduct> productlist = new ArrayList<SuperProduct>();

				for (int k = 0; k < form.salesProductTable.getDataprovider().getList().size(); k++) {
//					List<Integer> productIdlist = new ArrayList<Integer>();
					int prodid = form.salesProductTable.getDataprovider().getList().get(k).getProdId();

					System.out.println("Product id ::::: " + prodid);
					MyQuerry querry = new MyQuerry();
					Vector<Filter> filtervec = new Vector<Filter>();
					Filter filter = null;

					filter = new Filter();
					filter.setQuerryString("count");
					filter.setIntValue(prodid);
					filtervec.add(filter);

					filter = new Filter();
					filter.setLongValue(model.getCompanyId());
					filter.setQuerryString("companyId");
					filtervec.add(filter);

					querry.setFilters(filtervec);
					querry.setQuerryObject(new SuperProduct());

					async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							System.out.println("RESULT LIST SIZE :: "+ result.size());
							counter++;
							for (SuperModel model : result) {
								SuperProduct entity = (SuperProduct) model;
								productlist.add(entity);
							}
							if (counter == form.salesProductTable.getDataprovider().getList().size()) {
								System.out.println("Product List SIZE ::: "+ productlist.size());
								for (int i = 0; i < form.salesProductTable.getDataprovider().getList().size(); i++) {
									for (int j = 0; j < productlist.size(); j++) {
										if (form.salesProductTable.getDataprovider().getList().get(i).getProdId() == productlist.get(j).getCount()) {
											form.salesProductTable.getDataprovider().getList().get(i).setPrduct(productlist.get(j));
										}
									}
								}
								reactOnProductsChange();
							}
						}
						@Override
						public void onFailure(Throwable caught) {
							form.showDialogMessage("An Unexpected Error occured !");
						}
					});
				}
			}
			
			
			/***
			 * Date 27-03-2019 by Vijay 
			 * Des :- NBHC CCPM validation code moved into this method method
			 * @param btnLabel 
			 */
			private void reactonValidateService() {
//				final Date serDate = AppUtility.getForProcessConfigurartionIsActiveOrNot("AMCBillDateRestriction"); /** nidhi for service date validation */
				if(!(LoginPresenter.myUserEntity.getRole().getRoleName().trim().equalsIgnoreCase("ADMIN"))){
//					String dateser =  AppUtility.getForProcessConfigurartionIsActiveOrNot("AMCBillDateRestriction"); /** nidhi for service date validation */;
//					DateTimeFormat smpDate = DateTimeFormat.getFormat("dd/MM/yyyy");
//					System.out.println("get smp Date --" +smpDate);
//					final Date serDate = (dateser != null) ? smpDate.parse(dateser) : null;
//					if(form.multiContractFlag){
//							reactForMultipleContract(serDate);
//					}else{
//					
//					String contractId = form.getIbcontractid().getValue();
//					Date billingDate = form.getDbbillingdate().getValue();
//					/**
//					 *  nidhi
//					 *  3-march-2018
//					 *  for process configration remaining service
//					 */
////					final Date serValidDate = AppUtility.getForProcessConfigurartionIsActiveOrNot("AMCBillDateRestriction");
//					
//					
//					final Date date = CalendarUtil.copyDate(billingDate);
//					/** end
//					 * 
//					 */
//					
//					
//				
////					CalendarUtil.addDaysToDate(billingDate,1);
//	                List<String> statusList = new ArrayList<String>();
//	                statusList.add(Service.SERVICESTATUSSCHEDULE);
//	                statusList.add(Service.SERVICESTATUSRESCHEDULE);
//					MyQuerry querry = new MyQuerry();
//					Vector<Filter> temp = new Vector<Filter>();
//					Filter filter = null;
	//
//					filter = new Filter();
//					filter.setQuerryString("contractCount");
//					filter.setIntValue(Integer.parseInt(contractId));
//					temp.add(filter);
//					
//					/**
//					 *  nidhi
//					 *  3-march-2018
//					 *  for process configration remaining service
//					 */
//					if(serDate!=null){
//						filter = new Filter();
//						filter.setQuerryString("serviceDate >=");
//						filter.setDateValue(serDate);
//						temp.add(filter);
//					}
//					
//					if(billingDate != null){
//						
//						if(serDate==null){
//							CalendarUtil.addDaysToDate(billingDate,1);
//						}
//						filter = new Filter();
//						if(serDate==null){
//							filter.setQuerryString("serviceDate <");
//						}else{
//							filter.setQuerryString("serviceDate <=");
//						}
//						
//						filter.setDateValue(billingDate);
//						temp.add(filter);
//					}
//					
//					/***
//					 * end
//					 */
//					filter = new Filter();
//					filter.setQuerryString("status IN");
//					filter.setList(statusList);
//					temp.add(filter);
//					
//					querry.setFilters(temp);
//					querry.setQuerryObject(new Service());
////					form.showWaitSymbol();
//					service.getSearchResult(querry,
//							new AsyncCallback<ArrayList<SuperModel>>() {
	//
//								@Override
//								public void onFailure(Throwable caught) {
//									// TODO Auto-generated method stub
//									form.hideWaitSymbol();
//								}
	//
//								@Override
//								public void onSuccess(
//										ArrayList<SuperModel> result) {
//									// TODO Auto-generated method stub
//									Console.log("result :" + result.size());
//									form.hideWaitSymbol();
//									if (result.size() == 0) {
//										reactOnTaxInvoice();
										
								/**
								 * Date 08-03-2019 by Vijay for with process config  for service more than two days pending
								 * or 3 days pending value comes from process config for Tax invoice restriction
								 * if services Pending with specified days then tax invoice not allowed
								 */
					
								   DateTimeFormat smpDate = DateTimeFormat.getFormat("dd/MM/yyyy");
							       String strstartDate =  AppUtility.getForProcessConfigurartionIsActiveOrNot("AMCBillDateRestriction");
							       final Date startDate = smpDate.parse(strstartDate);
							       Console.log("startDate"+startDate);
							       
							       final Date dueDate = new Date();
									String servicePendingDays =  AppUtility.getForProcessConfigurartionIsActiveOrNot("EnableInvoiceRestrictionForPendingServices");
									int dueServiceDays = Integer.parseInt(servicePendingDays);
									CalendarUtil.addDaysToDate(dueDate, -dueServiceDays);
										
									 final ArrayList<String> branchlist = new ArrayList<String>();
									for(int i=0;i<LoginPresenter.globalBranch.size();i++){
										branchlist.add(LoginPresenter.globalBranch.get(i).getBusinessUnitName().trim());
									}
									
									
//									/** Date 11-06-2019 by Vijay for NBHC CCPM Validation with contract wise ****/
									ArrayList<Integer> contractId = new ArrayList<Integer>();
									/**
									 * Date:- 10-07-2019 By Vijay As per vaishali mam
									 * T+72 validation not required with contract wise so below code commented
									 * and added with T+72 Validation without contract
									 */
//									if(model.getContractCount()==0){
//										for(int i=0;i<model.getArrayBillingDocument().size();i++){
//											contractId.add(model.getArrayBillingDocument().get(i).getOrderId());
//										}
//									}else{
//										contractId.add(model.getContractCount());
//									}
//									/*** ends here ***/
									
									GeneralServiceAsync generalnewAsync = GWT.create(GeneralService.class);

									generalnewAsync.validateDueService(model.getCompanyId(), startDate, dueDate, branchlist,contractId, new AsyncCallback<String>() {

										@Override
										public void onSuccess(String result) {
											// TODO Auto-generated method stub
											Console.log("rpc success =="+result);
											System.out.println("rpc success =="+result);
											if(result.equals("Services Pending")){
//												form.showDialogMessage("There are services pending in the system unless you complete it in the system you would not be able to generate any invoice");
												String strDate = AppUtility.parseDate(dueDate);
												form.showDialogMessage("Kindly complete on pending services till  Date "+strDate+" for this contract and raise invoice.");
												form.hideWaitSymbol();
											}else{
												if(result.equals("Success")){
//													reactOnValidateReqApproval(btnLabel); 
													autoInvoicePopup.showPopUp();
													form.hideWaitSymbol();
												}	
											}
										}
										
										@Override
										public void onFailure(Throwable caught) {
											// TODO Auto-generated method stub
											form.hideWaitSymbol();
											Console.log("Failed =="+caught);
										}
										
									});
										
										
//									} else {/*
//										form.showDialogMessage("All services before billing date "+AppUtility.parseDate(date) +" are not completed. Please complete it and try again!!");
//									*/
	//
//									/**
//									 *  nidhi
//									 *   2-04-2018	
//									 */
//										if(serDate==null){
//											form.showDialogMessage("All services before billing date "+AppUtility.parseDate(date) +" are not completed. Please complete it and try again!!");
//										}else{
//											form.showDialogMessage("All services after "+ AppUtility.parseDate(serDate)+" and  before billing date "+AppUtility.parseDate(date) +" are not completed. Please complete it and try again!!");
//										}
//										form.hideWaitSymbol();
//										
//									}								
//								}
//							});					
//				
//				}
				
				}else{
					Console.log("inside else");
//					reactOnValidateReqApproval(btnLabel);
					autoInvoicePopup.showPopUp();
					form.hideWaitSymbol();
				}
			}

			
					
			/** 
			 * Date 20-04-2019 by Vijay for NBHC CCPM with process config
			 */
			private void reactonChangeStatus() {
				model.setStatus(BillingDocument.CREATED);
				model.setRemark("");
				model.setComment("");
				form.showWaitSymbol();
				async.save(model, new  AsyncCallback<ReturnFromServer>() {
					
					@Override
					public void onSuccess(ReturnFromServer result) {
						// TODO Auto-generated method stub
						form.getTbstatus().setValue(BillingDocument.CREATED);
						form.setToViewState();
						form.hideWaitSymbol();
						form.showDialogMessage("Billing Status Updated Sucessfully");
					}
					
					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						form.hideWaitSymbol();
					}
				});
			}
			
			
			/*** Date 12-09-2019 by Vijay for NBHC CCPM Auto invoice send request for Approval entry and then invoice status
			 ** changed to invoiced
			 **/
			private void reactonSendApprovalRequest(final Invoice inventity, final int invoiceCount) {
				final Approvals approval = new Approvals();
				approval.setApproverName(inventity.getApproverName());
				approval.setBranchname(inventity.getBranch());
				approval.setBusinessprocessId(invoiceCount);
				if(UserConfiguration.getInfo()!=null && !UserConfiguration.getInfo().getFullName().equals("")){
					approval.setRequestedBy(UserConfiguration.getInfo().getFullName());
				}
				approval.setPersonResponsible(inventity.getEmployee());
				if(inventity.getCreatedBy()!=null){
					approval.setDocumentCreatedBy(inventity.getCreatedBy());
				}
				approval.setApprovalLevel(1);
				approval.setStatus(Approvals.PENDING);
				approval.setBusinessprocesstype(ApproverFactory.INVOICEDETAILS);
				approval.setBpId(inventity.getPersonInfo().getCount()+"");
				approval.setBpName(inventity.getPersonInfo().getFullName());
				approval.setDocumentValidation(false);
				
				async.save(approval, new AsyncCallback<ReturnFromServer>() {
					
					@Override
					public void onSuccess(ReturnFromServer result) {
						// TODO Auto-generated method stub
						Console.log("Approval Sent");
						changeBillingStatusToInvoiced(invoiceCount,inventity);
						reactOnApprovalEmail(approval);
					}
					
					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						Console.log("Approval Sent failed");
					}
				});
				
			}
			
			public void reactOnInvoice(String invoiceType, String invoiceCategory, String invoiceType2, String invoiceGruop, String approverName){
				
//				form.showWaitSymbol();
//				Console.log("BF INVOICE CREATION FLAG : "+form.invCreationFlag);
//				if(form.invCreationFlag==true){
//					lblTaxInvoice.setText("Tax Invoice");
//					form.hideWaitSymbol();
//					return;
//				}
//				form.invCreationFlag=true;
//				Console.log("AF INVOICE CREATION FLAG : "+form.invCreationFlag);
//				
//				form.updateModel(model);
//				generalAsync.createTaxInvoice(model, invoiceType,invoiceCategory,invoiceType2,invoiceGruop,approverName,LoginPresenter.loggedInUser, false, new AsyncCallback<String>() {
//
//					@Override
//					public void onFailure(Throwable caught) {
//						// TODO Auto-generated method stub
//						form.hideWaitSymbol();
//						if(lblTaxInvoice!=null){ //For auto Invoice getting null exception so added if condition by Vijay
//							lblTaxInvoice.setVisible(true);
//							lblTaxInvoice.setText("Tax Invoice");
//						}
//					}
//
//					@Override
//					public void onSuccess(String result) {
//						form.hideWaitSymbol();
//						int invoiceCount=0;
//						if(result!=null){
//							Console.log("RESULT : "+result);
//							if(result.contains("-")){
//								String[] arr=result.split("-");
//								Console.log("RESULT : "+arr.length+"  "+arr[0]);
//								try{
//									invoiceCount=Integer.parseInt(arr[0]);
//								}catch(Exception e){
//									
//								}
//							}else{
//								form.showDialogMessage(result);
//								form.changeProcessLevel();
//								return;
//							}
//						}
//						
//						if(lblTaxInvoice!=null){
//							lblTaxInvoice.setText("Tax Invoice");
//						}
//						
//						model.setStatus(BillingDocument.BILLINGINVOICED);
//						form.tbstatus.setText(BillingDocument.BILLINGINVOICED);
//						form.showDialogMessage("Billing Document Invoiced!",GWTCAlert.OPTION_ROUNDED_BLUE,GWTCAlert.OPTION_ANIMATION);
//						form.getIbinvoiceid().setValue(invoiceCount + "");
//						model.setInvoiceCount(invoiceCount);
//						form.changeProcessLevel();
//					}
//				});
			
			
			/** 
			 * @author Vijay Date :- 04-12-2020
			 * Des :- Orion getting double invoice generation exception raising in general service impl in different operation on same time
			 * so called different RPC to avoid duplicate invoice
			 * 
			 */
				form.showWaitSymbol();
				Console.log("BF INVOICE CREATION FLAG : "+form.invCreationFlag);
				if(form.invCreationFlag==true){
					lblTaxInvoice.setText("Tax Invoice");
					form.hideWaitSymbol();
					return;
				}
				form.invCreationFlag=true;
				Console.log("AF INVOICE CREATION FLAG : "+form.invCreationFlag);
				
				form.updateModel(model);
				taxInvoiceService.createTaxInvoice(model, invoiceType, invoiceCategory, invoiceType2, invoiceGruop, approverName, LoginPresenter.loggedInUser, false, new AsyncCallback<String>() {
					
					@Override
					public void onSuccess(String result) {
						// TODO Auto-generated method stub
						form.hideWaitSymbol();
						int invoiceCount=0;
						if(result!=null){
							Console.log("RESULT : "+result);
							if(result.contains("-")){
								String[] arr=result.split("-");
								Console.log("RESULT : "+arr.length+"  "+arr[0]);
								try{
									invoiceCount=Integer.parseInt(arr[0]);
								}catch(Exception e){
									
								}
							}else{
								if(lblTaxInvoice!=null){
									lblTaxInvoice.setText("Tax Invoice");
								}
								form.invCreationFlag=false;

								form.showDialogMessage(result);
								form.changeProcessLevel();
								return;
							}
						}
						
						if(lblTaxInvoice!=null){
							lblTaxInvoice.setText("Tax Invoice");
						}
						
						model.setStatus(BillingDocument.BILLINGINVOICED);
						form.tbstatus.setText(BillingDocument.BILLINGINVOICED);
						
						/**
						 * @author Anil
						 * @since 06-01-2021
						 */
						form.billingDocObj.setStatus(model.getStatus());
						
						String mainScreenLabel="Billing Details";
						if(form.billingDocObj!=null&&form.billingDocObj.getCount()!=0){
							mainScreenLabel=form.billingDocObj.getCount()+" "+"/"+" "+form.billingDocObj.getStatus()+" "+"/"+" "+AppUtility.parseDate(form.billingDocObj.getCreationDate());
						}
						form.fgroupingCustomerInformation.getHeaderLabel().setText(mainScreenLabel);
						
						
						form.showDialogMessage("Billing Document Invoiced!",GWTCAlert.OPTION_ROUNDED_BLUE,GWTCAlert.OPTION_ANIMATION);
						form.getIbinvoiceid().setValue(invoiceCount + "");
						model.setInvoiceCount(invoiceCount);
						form.changeProcessLevel();
					}
					
					@Override
					public void onFailure(Throwable arg0) {
						// TODO Auto-generated method stub
						form.hideWaitSymbol();
						if(lblTaxInvoice!=null){ //For auto Invoice getting null exception so added if condition by Vijay
							lblTaxInvoice.setVisible(true);
							lblTaxInvoice.setText("Tax Invoice");
						}
					}
				});
			}
			
			
	/**
	 * @author Vijay Chougule
	 * Date :- 03-10-2019 for NBHC CCPM double invoice generation
	 * 
	 */
	private void reactonAutoInvoice(String createtaxinvoice, String invoiceCategory, String invoiceType2,
			String invoiceGruop, String approverName) {
		
			AutoInvoiceServiceAsync autoinvoice = GWT.create(AutoInvoiceService.class);
			final DocumentUploadServiceAsync sendRequestforApproval  = GWT.create(DocumentUploadService.class);
			
			form.showWaitSymbol();
			if (form.invCreationFlag == true) {
				lblTaxInvoice.setText("Tax Invoice");
				form.hideWaitSymbol();
				autoInvoicePopup.getLblOk().setVisible(true);
				return;
			}
			form.invCreationFlag = true;
			Console.log("AF INVOICE CREATION FLAG : " + form.invCreationFlag);

			form.updateModel(model);

			boolean confiFlag = AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service",
					"ContractCategoryAsServiceType");
			/***
			 * Date 26-09-2019 by Vijay for NBHC CCPM Auto invoice process added
			 * here
			 ***/
			boolean autoinvoiceflag = AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument",
					"EnableAutoInvoice");

			final Invoice inventity;
			if (model.getAccountType().equalsIgnoreCase(AppConstants.ACCOUNTTYPEAP)) {
				inventity = new VendorInvoice();
			} else {
				inventity = new Invoice();
			}

			PersonInfo pinfo = new PersonInfo();
			pinfo.setCount(model.getPersonInfo().getCount());
			pinfo.setFullName(model.getPersonInfo().getFullName());
			pinfo.setCellNumber(model.getPersonInfo().getCellNumber());
			pinfo.setPocName(model.getPersonInfo().getPocName());
			pinfo.setEmail(model.getPersonInfo().getEmail());
			if (model.getGrossValue() != 0) {
				inventity.setGrossValue(model.getGrossValue());
			}

			inventity.setTaxPercent(model.getArrPayTerms().get(0).getPayTermPercent());

			if (pinfo != null) {
				inventity.setPersonInfo(pinfo);
			}

			inventity.setContractCount(model.getContractCount());

			if (model.getContractStartDate() != null) {
				inventity.setContractStartDate(model.getContractStartDate());
			}
			if (model.getContractEndDate() != null) {
				inventity.setContractEndDate(model.getContractEndDate());
			}
			inventity.setTotalSalesAmount(model.getTotalSalesAmount());

			inventity.setArrayBillingDocument(model.getArrayBillingDocument());
			Console.log("HI Console 7");
//			if (isMultipleContractBilling(model.getArrayBillingDocument())) {
			if (isMultipleContractBilling()) {
				inventity.setMultipleOrderBilling(true);
				if (AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "OnlyForOrionPestSolutions") == false) {
					inventity.setContractCount(0);
				}
				inventity.setRemark(model.getRemark());
			}

			inventity.setTotalBillingAmount(model.getTotalBillingAmount());

			inventity.setNetPayable(model.getTotalBillingAmount());

			inventity.setDiscount(0);

			if (AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "OnlyForNBHC")) {
				inventity.setInvoiceDate(new Date());
			} else {
				inventity.setInvoiceDate(model.getInvoiceDate());
			}

			inventity.setPaymentDate(model.getPaymentDate());
			inventity.setApproverName(model.getApproverName());
			inventity.setEmployee(model.getEmployee());
			inventity.setBranch(model.getBranch());

			inventity.setOrderCreationDate(model.getOrderCreationDate());
			inventity.setCompanyId(model.getCompanyId());
			inventity.setInvoiceAmount(model.getTotalBillingAmount());
			inventity.setInvoiceType(createtaxinvoice);
			inventity.setPaymentMethod(model.getPaymentMethod());
			inventity.setAccountType(model.getAccountType());
			inventity.setTypeOfOrder(model.getTypeOfOrder());

			if (createtaxinvoice.trim().equalsIgnoreCase(AppConstants.CREATETAXINVOICE.trim())) {
				if (autoinvoiceflag) {
					inventity.setStatus(Invoice.REQUESTED);
				} else {
					inventity.setStatus(Invoice.CREATED);
				}
			} else {
				inventity.setStatus(BillingDocument.PROFORMAINVOICE);
			}

			List<SalesOrderProductLineItem> productlist = model.getSalesOrderProducts();
			ArrayList<SalesOrderProductLineItem> prodList = new ArrayList<SalesOrderProductLineItem>();
			prodList.addAll(productlist);
			System.out.println("SALES PRODUCT TABLE SIZE ::: " + prodList.size());

			List<OtherCharges> OthChargeTbl = model.getOtherCharges();
			ArrayList<OtherCharges> ocList = new ArrayList<OtherCharges>();
			ocList.addAll(OthChargeTbl);
			System.out.println("OTHER TAXES TABLE SIZE ::: " + ocList.size());

			List<ContractCharges> taxestable = model.getBillingTaxes();
			ArrayList<ContractCharges> taxesList = new ArrayList<ContractCharges>();
			taxesList.addAll(taxestable);
			System.out.println("TAXES TABLE SIZE ::: " + taxesList.size());

			List<ContractCharges> otherChargesable = model.getBillingOtherCharges();
			ArrayList<ContractCharges> otherchargesList = new ArrayList<ContractCharges>();
			otherchargesList.addAll(otherChargesable);
			System.out.println("OTHER CHARGES TABLE SIZE ::: " + otherchargesList.size());

			ArrayList<SalesOrderProductLineItem> updatedProdList = new ArrayList<SalesOrderProductLineItem>();
			for (SalesOrderProductLineItem prod : prodList) {

				if (prod.getBillingDocBaseAmount() == 0 || prod.getBillingDocBaseAmount() == 0.0) {
					prod.setBillingDocBaseAmount(prod.getBaseBillingAmount());
				}
				prod.setBaseBillingAmount(prod.getBasePaymentAmount());
				prod.setPaymentPercent(100);
				prod.setBasePaymentAmount(prod.getBasePaymentAmount());

				updatedProdList.add(prod);

			}
			inventity.setSalesOrderProductFromBilling(updatedProdList);

			inventity.setOtherCharges(ocList);
			inventity.setTotalOtherCharges(model.getTotalOtherCharges());

			inventity.setBillingTaxes(taxesList);
			inventity.setBillingOtherCharges(otherchargesList);

			inventity.setCustomerBranch(model.getCustomerBranch());

			inventity.setAccountType(model.getAccountType());
			inventity.setArrPayTerms(model.getArrPayTerms());

			inventity.setInvoiceGroup(model.getGroup());
			if (model.getOrderCformStatus() != null) {
				inventity.setOrderCformStatus(model.getOrderCformStatus());
			}
			if (model.getOrderCformPercent() != 0 && model.getOrderCformPercent() != -1) {
				inventity.setOrderCformPercent(model.getOrderCformPercent());
			} else {
				inventity.setOrderCformPercent(-1);
			}

			if (model.getNumberRange() != null)
				inventity.setNumberRange(model.getNumberRange());

			inventity.setBillingPeroidFromDate(model.getBillingPeroidFromDate());
			inventity.setBillingPeroidToDate(model.getBillingPeroidToDate());

			inventity.setSegment(model.getSegment());

			inventity.setRateContractServiceId(model.getRateContractServiceId());

			inventity.setQuantity(model.getQuantity());
			inventity.setUom(model.getUom());
			inventity.setRefNumber(model.getRefNumber());

			inventity.setTotalAmtExcludingTax(model.getTotalAmount());
			inventity.setDiscountAmt(model.getDiscountAmt());
			inventity.setFinalTotalAmt(model.getFinalTotalAmt());
			inventity.setTotalAmtIncludingTax(model.getTotalAmtIncludingTax());
			inventity.setTotalBillingAmount(model.getTotalBillingAmount());
			inventity.setDiscount(model.getRoundOffAmt());

			/**
			 * Date : 06-11-2017 BY ANIL setting billing comment to invoice for
			 * NBHC
			 */
			// if(form.getTacomment().getValue()!=null){
			inventity.setComment(model.getComment());
			// }

			if (confiFlag) {
				inventity.setInvoiceCategory(model.getCategory());
				inventity.setInvoiceConfigType(model.getType());
				inventity.setInvoiceGroup(model.getGroup());
			}

			inventity.setConsolidatePrice(model.isConsolidatePrice());

			inventity.setRenewContractFlag(model.isRenewContractFlag());

			if (model.getSubBillType() != null && !model.getSubBillType().equals("")) {
				inventity.setSubBillType(model.getSubBillType());
			}

			if (model.getServiceId() != null) {
				inventity.setServiceId(model.getServiceId());
			}

			if (model.getCancleAmtRemark() != null && !model.getCancleAmtRemark().equals("")) {
				inventity.setCancleAmtRemark(model.getCancleAmtRemark());
			}

			/***
			 * Date 26-09-2019 by Vijay for NBHC CCPM AutoInvoice invoice
			 * details from popup
			 ****/
			if (autoinvoiceflag) {
				inventity.setInvoiceCategory(invoiceCategory);
				inventity.setInvoiceConfigType(invoiceType2);
				inventity.setInvoiceGroup(invoiceGruop);
				inventity.setApproverName(approverName);

			}
			Console.log("HI Console 30");
			ArrayList<Integer> billIdList = new ArrayList<Integer>();
			ArrayList<Integer> billOrderIdList = new ArrayList<Integer>();
			List<BillingDocumentDetails> billtablelis = form.billingDocumentTable.getDataprovider().getList();
			for (int i = 0; i < billtablelis.size(); i++) {
				billIdList.add(billtablelis.get(i).getBillId());
				billOrderIdList.add(billtablelis.get(i).getOrderId());
			}
			Console.log("HI Console 31");
			
			/** Date 09-10-2020 by Vijay set value of created by requirement raised by Rahul Tiwari ***/
			if(LoginPresenter.loggedInUser!=null){
				inventity.setCreatedBy(LoginPresenter.loggedInUser);
			}
			
			autoinvoice.createTaxInvoice(model,inventity,LoginPresenter.loggedInUser,autoinvoiceflag,billIdList,billOrderIdList, new AsyncCallback<String>() {
				
				@Override
				public void onSuccess(String result) {
					// TODO Auto-generated method stub
					Console.log("HI Console 33");
					autoInvoicePopup.getLblOk().setVisible(true);
					autoInvoicePopup.getLblOk().setText("OK");
					int invoiceCount=0;
					if(result!=null){
						Console.log("RESULT : "+result);
						if(result.contains("-")){
							String[] arr=result.split("-");
							Console.log("RESULT : "+arr.length+"  "+arr[0]);
							try{
								invoiceCount=Integer.parseInt(arr[0]);
							}catch(Exception e){
								
							}
							
							if(arr[1].trim().equals("success")){
								form.hideWaitSymbol();
								form.getIbinvoiceid().setValue(invoiceCount + "");
								model.setInvoiceCount(invoiceCount);
								model.setStatus(BillingDocument.BILLINGINVOICED);
								form.tbstatus.setText(BillingDocument.BILLINGINVOICED);
								form.showDialogMessage("Billing Document Invoiced!",GWTCAlert.OPTION_ROUNDED_BLUE,GWTCAlert.OPTION_ANIMATION);
								form.changeProcessLevel();
							}
							
						}else{
							form.showDialogMessage(result);
							form.changeProcessLevel();
							return;
						}
					}
					
					if(lblTaxInvoice!=null){
						lblTaxInvoice.setText("Tax Invoice");
					}
					
					/**
					 * @author Anil , Date : 15-11-2019
					 * As per the disscusion with Nitin sir,commented second rpc call to send approval request
					 */
					
//					final int invoiceId = invoiceCount;
//					sendRequestforApproval.sendApprovalRequest(inventity, LoginPresenter.loggedInUser, invoiceCount, new AsyncCallback<String>() {
//						@Override
//						public void onSuccess(String result) {
//							// TODO Auto-generated method stub
//							if(result.trim().equals("success")){
//								form.hideWaitSymbol();
//								form.getIbinvoiceid().setValue(invoiceId + "");
//								model.setInvoiceCount(invoiceId);
//								model.setStatus(BillingDocument.BILLINGINVOICED);
//								form.tbstatus.setText(BillingDocument.BILLINGINVOICED);
//								form.showDialogMessage("Billing Document Invoiced!",GWTCAlert.OPTION_ROUNDED_BLUE,GWTCAlert.OPTION_ANIMATION);
//								form.changeProcessLevel();
//							}else{
//								/**
//								 * Date 06-11-2019 
//								 * @author Vijay Chougule.
//								 * Des :- as per nitin sir instruction IF RPC failed then same rpc will call and retry 3 times 
//								 * if any exception raised then it will show the message
//								 */
//								form.showDialogMessage(result);
//								int retryCount =2;
//								retryApprovalRequest(inventity,LoginPresenter.loggedInUser,invoiceId,retryCount);
//							}
//						}
//						@Override
//						public void onFailure(Throwable caught) {
//							// TODO Auto-generated method stub
//							form.showDialogMessage(caught.getMessage());
//							Console.log("Failed 1st Time");
//							int retryCount =2;
//							retryApprovalRequest(inventity,LoginPresenter.loggedInUser,invoiceId,retryCount);
//						}
//					});
					
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					Console.log("HI Console 34");
					form.hideWaitSymbol();
					form.showDialogMessage("ERROR : "+caught.getMessage());
					autoInvoicePopup.getLblOk().setVisible(true);
					autoInvoicePopup.getLblOk().setText("OK");
				}
			});
				
			}
			
			public boolean isMultipleContractBilling(List<BillingDocumentDetails> billtablelis){
				int contractId=billtablelis.get(0).getOrderId();
				for(int i=0;i<billtablelis.size();i++){
					if(billtablelis.get(i).getOrderId()!=contractId){
						return true;
					}
				}
				return false;
			}
	private boolean checkBillSubType(String billType){
				
				switch(billType){
					case AppConstants.NATIONALHOLIDAY:
					case AppConstants.OVERTIME:
					case AppConstants.STAFFINGANDRENTAL:
					case AppConstants.OTHERSERVICES:
					case AppConstants.CONSUMABLES:
					case AppConstants.FIXEDCONSUMABLES :
					case AppConstants.EQUIPMENTRENTAL:
					case AppConstants.STAFFING:
					case AppConstants.CONSOLIDATEBILL:	
					case AppConstants.ARREARSBILL:	
					case AppConstants.PUBLICHOLIDAY:	
						return true;
				}
				return false;		
				}		
	
	/**
	 * Date 06-11-2019 
	 * @author Vijay Chougule.
	 * Des :- as per nitin sir instruction IF RPC failed then same rpc will call and retry 3 times 
	 * if any exception raised then it will show the message
	 */
	
	private void retryApprovalRequest(final Invoice inventity, String loggedInUser, final int invoiceId, final int retryCount) {
		DocumentUploadServiceAsync sendRequestforApproval  = GWT.create(DocumentUploadService.class);
		if(retryCount<=3){
			
			sendRequestforApproval.sendApprovalRequest(inventity, LoginPresenter.loggedInUser, invoiceId, new AsyncCallback<String>() {
				
				@Override
				public void onSuccess(String result) {
					// TODO Auto-generated method stub
					form.hideWaitSymbol();
					form.getIbinvoiceid().setValue(invoiceId + "");
					model.setInvoiceCount(invoiceId);
					model.setStatus(BillingDocument.BILLINGINVOICED);
					form.tbstatus.setText(BillingDocument.BILLINGINVOICED);
					form.showDialogMessage("Billing Document Invoiced!",GWTCAlert.OPTION_ROUNDED_BLUE,GWTCAlert.OPTION_ANIMATION);
					form.changeProcessLevel();
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					Console.log("2nd Time Failed");
					form.showDialogMessage(caught.getMessage());
					int count =retryCount;
					count++;
					if(count<=3){
						retryApprovalRequest(inventity,LoginPresenter.loggedInUser,invoiceId,count);
					}
					else{
						form.hideWaitSymbol();
					}
					
				}
				
			});
		}
		
		
		
	}
	
	

	/** @author Vijay Chougule
	 * Des :- NBHC CCPM for Rate Contract Billing document Net amount will update 
	 * as Service value in Service
	 */
	private void updateRateContractServiceValue() {
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument","EnableUpdateServiceValueAsBillingTotalAmt") && model.getRateContractServiceId()!=0){
			CommunicationLogServiceAsync serviceValueUpdation = GWT.create(CommunicationLogService.class);
			serviceValueUpdation.updateRateContractServiceValue(model, new AsyncCallback<String>() {
				
				@Override
				public void onSuccess(String result) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					
				}
			});
		}
	}
	
	private void updateSalesOrderSummary() {
		for(SalesOrderProductLineItem product:form.getSalesProductTable().getValue()){
			if(product.getSalesAnnexureMap()!=null&&product.getSalesAnnexureMap().size()!=0){
				ArrayList<SalesAnnexureType> salesAnnexureList=new ArrayList<SalesAnnexureType>();
				salesAnnexureList.addAll(product.getSalesAnnexureMap().get(product.getBillProductName()));
				
				for(SalesAnnexureType type:salesAnnexureList){
					double sgstAmount=0;
					double cgstAmount=0;
					double igstAmount=0;
					double utgstAmount=0;
					double baseAmount=type.getPayableAmount();
					double totalNetAmt=0;
					
					double sgstPer=0;
					double cgstPer=0;
					double igstPer=0;
					double utgstper=0;
					
					try{
						if(product.getVatTax()!=null){
							if(product.getVatTax().getTaxConfigName().contains("cgst")||product.getVatTax().getTaxConfigName().contains("CGST")
									||product.getVatTax().getTaxName().contains("cgst")||product.getVatTax().getTaxName().contains("CGST")
									||product.getVatTax().getTaxPrintName().contains("cgst")||product.getVatTax().getTaxPrintName().contains("CGST")){
								cgstPer=product.getVatTax().getPercentage();
								cgstAmount=(baseAmount*cgstPer)/100;
							}
							
							if(product.getVatTax().getTaxConfigName().contains("sgst")||product.getVatTax().getTaxConfigName().contains("SGST")
									||product.getVatTax().getTaxName().contains("sgst")||product.getVatTax().getTaxName().contains("SGST")
									||product.getVatTax().getTaxPrintName().contains("sgst")||product.getVatTax().getTaxPrintName().contains("SGST")){
								sgstPer=product.getVatTax().getPercentage();
								sgstAmount=(baseAmount*sgstPer)/100;
							}
							
							if(product.getVatTax().getTaxConfigName().contains("igst")||product.getVatTax().getTaxConfigName().contains("IGST")
									||product.getVatTax().getTaxName().contains("igst")||product.getVatTax().getTaxName().contains("IGST")
									||product.getVatTax().getTaxPrintName().contains("igst")||product.getVatTax().getTaxPrintName().contains("IGST")){
								igstPer=product.getVatTax().getPercentage();
								igstAmount=(baseAmount*igstPer)/100;
							}
							
							if(product.getVatTax().getTaxConfigName().contains("utgst")||product.getVatTax().getTaxConfigName().contains("UTGST")
									||product.getVatTax().getTaxName().contains("utgst")||product.getVatTax().getTaxName().contains("UTGST")
									||product.getVatTax().getTaxPrintName().contains("utgst")||product.getVatTax().getTaxPrintName().contains("UTGST")){
								utgstper=product.getVatTax().getPercentage();
								utgstAmount=(baseAmount*utgstper)/100;
							}
						}
						
						
						if(product.getServiceTax()!=null){
							if(product.getServiceTax().getTaxConfigName().contains("cgst")||product.getServiceTax().getTaxConfigName().contains("CGST")
									||product.getServiceTax().getTaxName().contains("cgst")||product.getServiceTax().getTaxName().contains("CGST")
									||product.getServiceTax().getTaxPrintName().contains("cgst")||product.getServiceTax().getTaxPrintName().contains("CGST")){
								cgstPer=product.getServiceTax().getPercentage();
								cgstAmount=(baseAmount*cgstPer)/100;
							}
							
							if(product.getServiceTax().getTaxConfigName().contains("sgst")||product.getServiceTax().getTaxConfigName().contains("SGST")
									||product.getServiceTax().getTaxName().contains("sgst")||product.getServiceTax().getTaxName().contains("SGST")
									||product.getServiceTax().getTaxPrintName().contains("sgst")||product.getServiceTax().getTaxPrintName().contains("SGST")){
								sgstPer=product.getServiceTax().getPercentage();
								sgstAmount=(baseAmount*sgstPer)/100;
							}
							
							if(product.getServiceTax().getTaxConfigName().contains("igst")||product.getServiceTax().getTaxConfigName().contains("IGST")
									||product.getServiceTax().getTaxName().contains("igst")||product.getServiceTax().getTaxName().contains("IGST")
									||product.getServiceTax().getTaxPrintName().contains("igst")||product.getServiceTax().getTaxPrintName().contains("IGST")){
								igstPer=product.getServiceTax().getPercentage();
								igstAmount=(baseAmount*igstPer)/100;
							}
							
							if(product.getServiceTax().getTaxConfigName().contains("utgst")||product.getServiceTax().getTaxConfigName().contains("UTGST")
									||product.getServiceTax().getTaxName().contains("utgst")||product.getServiceTax().getTaxName().contains("UTGST")
									||product.getServiceTax().getTaxPrintName().contains("utgst")||product.getServiceTax().getTaxPrintName().contains("UTGST")){
								utgstper=product.getServiceTax().getPercentage();
								utgstAmount=(baseAmount*utgstper)/100;
							}
						}
					}catch(Exception e){
						e.printStackTrace();
					}
					totalNetAmt=baseAmount+cgstAmount+sgstAmount+igstAmount+utgstAmount;
					type.setCgstAmt(cgstAmount);
					type.setSgstAmt(sgstAmount);
					type.setUtgstAmt(utgstAmount);
					type.setIgstAmt(igstAmount);
					
					type.setCgstPer(cgstPer);
					type.setSgstPer(sgstPer);
					type.setUtgstPer(utgstper);
					type.setIgstPer(igstPer);
					type.setGrandTotal(totalNetAmt);
				}
				
				product.getSalesAnnexureMap().put(product.getBillProductName(), salesAnnexureList);
			}
		}
		form.getSalesProductTable().getTable().redraw();
		
	}
			
	
	private void downloadData(boolean addOrderNoAndGrnFlag, List<BillingDocument> listbill, ArrayList<PurchaseOrder> polist, ArrayList<GRN> grnlist) {

		int columnCount = 37;

		if(addOrderNoAndGrnFlag){
			columnCount +=2;	
		}
		
		ArrayList<String> billingList = new ArrayList<String>();
		billingList.add("BILLING ID");
		billingList.add("BILLING DATE");
		billingList.add("BP ID");
		billingList.add("BP NAME");
		/*** 13-12-2019 Deepak Salve added this code for Customer Branch Download***/
		billingList.add("Customer Branch");
		/***End***/
		billingList.add("PERSON CELL");
		billingList.add("ORDER ID");
		
		if(addOrderNoAndGrnFlag){
			billingList.add("ORDER NO");
			billingList.add("GRN NO");
		}
		billingList.add("ORDER START DATE");
		billingList.add("ORDER END DATE");
		/**@Sheetal : 02-03-2022
		 *  Des  : Adding Billing Period Start date and End date, requirement by pest-o-sheild**/
		billingList.add("BILLING PERIOD START DATE");
		billingList.add("BILLING PERIOD END DATE");
		/**end**/
		billingList.add("ACCOUNT TYPE");
		billingList.add("ORDER AMOUNT");
		billingList.add("BILLING AMOUNT");
		billingList.add("BILLING CATEGORY");
		billingList.add("BILLING TYPE");
		billingList.add("BILLING GROUP");
		billingList.add("INVOICE DATE");
		billingList.add("PAYMENT DATE");
		billingList.add("APPROVER NAME");
		billingList.add("BRANCH");
		billingList.add("COMMENT");
		billingList.add("STATUS");
		billingList.add("SEGMENT");
		billingList.add("SERVICE ID");
		billingList.add("QUANTITY");
		billingList.add("UOM");
		/**
		 *  NIdhi
		 *  24-07-2017
		 */
		billingList.add("REFERANCE NO.");
		/**
		 *  end
		 */
		
		/*** Date 11-09-2017 added by vijay for discount amt round off  ***********/
		billingList.add("TOTAL AMOUNT");
		billingList.add("DISCOUNT AMOUNT");
		billingList.add("TOTAL AMOUNT");
		billingList.add("TOTAL AMOUNT");
		billingList.add("TOTAL AMOUNT");
		billingList.add("ROUND OFF");
		billingList.add("TOTAL BILLING AMOUNT");
		/**
		 * Date 14-5-2018
		 * By jayshree add sale person 
		 */
		billingList.add("SALES PERSON");
		
		/**
		 * Date 06-07-2018 By Vijay
		 * Des :- For NBHC
		 */
		billingList.add("INVOICE ID");
		/**
		 * ends here
		 */
		
		

		for(BillingDocument billinglis:listbill)
		{
			
			billingList.add(billinglis.getCount()+"");

			if(billinglis.getBillingDate()!=null){
				billingList.add(AppUtility.parseDate(billinglis.getBillingDate(),"dd-MMM-yyyy"));
			}
			else{
				billingList.add("");
			}
			billingList.add(billinglis.getPersonInfo().getCount()+"");
			billingList.add(billinglis.getPersonInfo().getFullName());

			/***Deepak Salve add this code for Customer Branch Download***/
			System.out.println("Customer Branch "+billinglis.getCustomerBranch());
			if(billinglis.getCustomerBranch()!=null&&!billinglis.getCustomerBranch().equals("")){
				billingList.add(billinglis.getCustomerBranch());
			}
			else{
				billingList.add("");
			}
			/***End***/
			billingList.add(billinglis.getPersonInfo().getCellNumber()+"");
			billingList.add(billinglis.getContractCount()+"");

			/** Date 12-08-2020 by Vijay for sasha Supplier's Ref./Order No and Receipt Note No ***/
			if(addOrderNoAndGrnFlag){
				if(billinglis.getReferenceNumer()!=null && !billinglis.getReferenceNumer().equals("")){
					billingList.add(billinglis.getReferenceNumer());
				}
				else{
					billingList.add("");
				}
				if(billinglis.getReferenceNumber2()!=null && !billinglis.getReferenceNumber2().equals("")){
					billingList.add(billinglis.getReferenceNumber2());
				}
				else{
					billingList.add("");
				}
			}

			/**
			 * rohan added this for pest terminators 
			 */
			if(billinglis.getContractStartDate()!=null){
				billingList.add(AppUtility.parseDate(billinglis.getContractStartDate(),"dd-MMM-yyyy"));
			}
			else{
				billingList.add("N.A");
			}
			if(billinglis.getContractEndDate()!=null){
				billingList.add(AppUtility.parseDate(billinglis.getContractEndDate(),"dd-MMM-yyyy"));
			}
			else{
				billingList.add("N.A");
			}
			/**
			 * ends here 
			 */
			/**@Sheetal : 02-03-2022
			 *  Des  : Adding Billing Period Start date and End date, requirement by pest-o-sheild**/
			if(billinglis.getBillingPeroidFromDate()!=null){
				billingList.add(AppUtility.parseDate(billinglis.getBillingPeroidFromDate(),"dd-MMM-yyyy"));
			}
			else{
				billingList.add("N.A");
			}
			
			if(billinglis.getBillingPeroidToDate()!=null){
				billingList.add(AppUtility.parseDate(billinglis.getBillingPeroidToDate(),"dd-MMM-yyyy"));
			}
			else{
				billingList.add("N.A");
			}
			/**end**/
			billingList.add(billinglis.getAccountType());
			billingList.add(billinglis.getTotalSalesAmount()+"");
			if(billinglis.getTotalBillingAmount()!=null){
				billingList.add(billinglis.getTotalBillingAmount()+"");
			}else{
				billingList.add("");
			}
			
			if(billinglis.getBillingCategory()!=null){
				billingList.add(billinglis.getBillingCategory());
			}
			else{
				billingList.add("");
			}
			
			if (billinglis.getBillingType() != null) {
				billingList.add(billinglis.getBillingType());
			} else {
				billingList.add("");
			}
			if (billinglis.getBillingGroup() != null) {
				billingList.add(billinglis.getBillingGroup());
			} else {
				billingList.add("");
			}
			if (billinglis.getInvoiceDate() != null) {
				billingList.add(AppUtility.parseDate(billinglis.getInvoiceDate(),"dd-MMM-yyyy"));
			} else {
				billingList.add("");
			}
			if (billinglis.getPaymentDate() != null) {
				billingList.add(AppUtility.parseDate(billinglis.getPaymentDate(),"dd-MMM-yyyy"));
			} else {
				billingList.add("");
			}
			if (billinglis.getApproverName() != null) {
				billingList.add(billinglis.getApproverName());
			} else {
				billingList.add("");
			}
			//  rohan added here branch for NBHC 
			if (billinglis.getBranch() != null) {
				billingList.add(billinglis.getBranch());
			} else {
				billingList.add("");
			}
			//  ends here 	
				
			if(billinglis.getComment()!=null && !billinglis.getComment().equals("")){
				/**
				 * Date 11/10/2018 By Vijay
				 * Des :- Download getting issue due to comment contains comma so i have commneted old code and added below code
				 */
				billingList.add(billinglis.getComment());
				/**
				 * ends here
				 */
			}else{
				billingList.add("");
			}
			billingList.add(billinglis.getStatus());
			
			
		if(billinglis.getSegment()!=null){
			billingList.add(billinglis.getSegment());
		}
		else{
			billingList.add("");
		}
		/**
		 * Date : 17-07-2017 Nidhi
		 */
		if (billinglis.getRateContractServiceId() != 0) {
			billingList.add(billinglis.getRateContractServiceId() + "");
		} else {
			billingList.add("");
		}
		/**
		 * End
		 */
			
		/*
		 *  NIDHI
		 *  20-07-2017
		 *  
		 */
		if (billinglis.getQuantity() != 0) {
			billingList.add(billinglis.getQuantity()+"");
		} else {
			billingList.add("");
		}
		
		if (billinglis.getUom() != null) {
			billingList.add(billinglis.getUom());
		} else {
			billingList.add("");
		}
		/*
		 *  end
		 */
		
		/**
		 *  nidhi
		 *  24-07-2017
		 *  
		 */
		
		if (billinglis.getRefNumber() != null) {
			billingList.add(billinglis.getRefNumber());
		} else {
			billingList.add("");
		}
		/*
		 *  end
		 */
		
		/*** Date 11-09-2017 added by vijay for discount amt round off  ***********/
		billingList.add(billinglis.getTotalAmount()+"");
		billingList.add(billinglis.getDiscountAmt()+"");
		billingList.add(billinglis.getFinalTotalAmt()+"");
		billingList.add(billinglis.getTotalAmtIncludingTax()+"");
		billingList.add(billinglis.getGrandTotalAmount()+"");
		billingList.add(billinglis.getRoundOffAmt()+"");
		billingList.add(billinglis.getTotalBillingAmount()+"");
		/**
		 * Date 14-5-2018
		 * by jayshree
		 * add sales person name
		 */
		billingList.add(billinglis.getEmployee());
		//End by jayshree
		/**
		 * Date 06-07-2018 By Vijay
		 * Des :- invoice Id. Requirement from NBHC 
		 */
		if(billinglis.getInvoiceCount()!=0){
			billingList.add(billinglis.getInvoiceCount()+"");
		}else{
			billingList.add("");
		}
		/**
		 * ends here
		 */
		}
			

		csvservice.setExcelData(billingList, columnCount, AppConstants.BILLINGDETAILS, new AsyncCallback<String>() {
			
			@Override
			public void onSuccess(String result) {
				// TODO Auto-generated method stub
				
				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "CreateXLXSServlet"+"?type="+14;
				Window.open(url, "test", "enabled");
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});

	}
	
	
	private String getGRNReceiptNoteNo(List<GRN> grnlist, int GRNId) {

		for(GRN grnEntity : grnlist){
			if(grnEntity.getCount()==GRNId){
				if(grnEntity.getRefNo()!=null && !grnEntity.getRefNo().equals("")){
					return grnEntity.getRefNo();
				}
			}
		}
		
		return "";
	}



	private String getRefOrderNo(List<PurchaseOrder> polist,Integer contractCount) {
	
		for(PurchaseOrder purchase : polist){
			if(purchase.getCount()==contractCount){
				if(purchase.getRefOrderNO()!=null){
					return purchase.getRefOrderNO();
				}
			}
		}
		
		return "";
	}
}
