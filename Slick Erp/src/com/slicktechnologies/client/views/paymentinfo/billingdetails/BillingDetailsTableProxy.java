package com.slicktechnologies.client.views.paymentinfo.billingdetails;

import java.util.Comparator;
import java.util.List;


import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;

public class BillingDetailsTableProxy extends SuperTable<BillingDocument> {
	
	TextColumn<BillingDocument> billingIdColumn;
	TextColumn<BillingDocument> billingDateColumn;
	TextColumn<BillingDocument> customerIdColumn;
	TextColumn<BillingDocument> customerNameColumn;
	TextColumn<BillingDocument> customerCellColumn;
	TextColumn<BillingDocument> accountTypeColumn;
	TextColumn<BillingDocument> contractIdColumn;
	TextColumn<BillingDocument> billingAmtColumn;
	TextColumn<BillingDocument> billingTaxColumn;
	TextColumn<BillingDocument> billingCategoryColumn;
	TextColumn<BillingDocument> billingTypeColumn;
	TextColumn<BillingDocument> billingGroupColumn;
	TextColumn<BillingDocument> approverNameColumn;
	TextColumn<BillingDocument> statusColumn;
	TextColumn<BillingDocument> branchColumn;
	
	TextColumn<BillingDocument> rateContractServiceId;
	
	/*
	 * nidhi
	 */
	TextColumn<BillingDocument> segmentcolumn;
	/*
	 * end
	 */
	/** 03-10-2017 sagar sore [To add Number range column in Invoice list search pop up result]**/
	TextColumn<BillingDocument> numberRangeColumn;
	
	/**
	 * Date : 18-11-2017 BY ANIL
	 */
	TextColumn<BillingDocument> getColRefNum;
	/** date 1.11.2018 added by komal for type of order and sub bill type **/
	TextColumn<BillingDocument> getColTypeOfOrder;
	TextColumn<BillingDocument> getColSubBillType;
	/**
	 * nidhi 14-12-2018
	 */
	TextColumn<BillingDocument> getColCancleAmt;
	TextColumn<BillingDocument> getColCancleRemark;
	boolean parcialBillDisableFlag = AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "DisableParticalBillDocument");
	
	public BillingDetailsTableProxy() {
		super();
		 parcialBillDisableFlag = AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "DisableParticalBillDocument");
	}

	@Override
	public void createTable() {
		 parcialBillDisableFlag = AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "DisableParticalBillDocument");
		/**03-10-2017 sagar sore [number range column added and order of columns changed]**/
		/**old code**/
//		addColumnBillingId();
//		addColumnBillingDate();
//		addColumnBillingCustomerId();
//		addColumnCustomerName();
//		addColumnCustomerPhone();
//		addColumnAccountType();
//		addColumnContractId();
//		addColumnBillingAmount();
//		addColumnBillingCategory();
//		addColumnBillingType();
//		addColumnBillingGroup();
//		addColumnApproverName();
//		addColumnBillingStatus();
//		addColumnBranch();
//		addColumnRateContractServiceId();
//		/*
//		 * nidhi
//		 *  1-07-2017
//		 */
//		addColumnSegment();
//		/*
//		 *  end
//		 */
//		
//	    //table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
//		
/** updated order**/
		addColumnBillingId();
		addColumnBillingDate();
		addColumnBillingCustomerId();
	
		addColumnCustomerName();
		addColumnCustomerPhone();
		addColumnAccountType();
		addColumnBillingStatus();
		addColumnBranch();
	
		addColumnNumberRange();
		addColumnContractId();
		addColumnBillingAmount();
		addColumnBillingCategory();
		addColumnBillingType();
		addColumnBillingGroup();
		addColumnApproverName();		
		addColumnRateContractServiceId();
		/*
		 * nidhi
		 *  1-07-2017
		 */
		addColumnSegment();
		/*
		 *  end
		 */
		getColRefNum();
		/** date 1.11.2018 added by komal for type of order and sub bill type **/
		getColTypeOfOrder();
		getColSubBillType();
		if(parcialBillDisableFlag){
			getColCancleAmt();
			getColCancleAmtRemark();
			
		}
	}
	
	private void getColRefNum() {
		getColRefNum = new TextColumn<BillingDocument>() {
			
			@Override
			public String getValue(BillingDocument object) {
				if(object.getRefNumber()!= null)
					return object.getRefNumber() +"";
				else 
					return "";
			}
		};
		table.addColumn(getColRefNum,"Reference Number");
		table.setColumnWidth(getColRefNum, 100, Unit.PX);
		getColRefNum.setSortable(true);
	}

	private void addColumnSegment() {
		
		segmentcolumn = new TextColumn<BillingDocument>() {
			
			@Override
			public String getValue(BillingDocument object) {
				if(object.getSegment()!= null)
					return object.getSegment() +"";
				else 
					return "";
			}
		};
		
		table.addColumn(segmentcolumn,"Segment");
		table.setColumnWidth(segmentcolumn, 100, Unit.PX);
		segmentcolumn.setSortable(true);
		
	}
	
	
	private void addColumnRateContractServiceId() {
		
		rateContractServiceId = new TextColumn<BillingDocument>() {
			
			@Override
			public String getValue(BillingDocument object) {
				if(object.getRateContractServiceId()!=0)
					return object.getRateContractServiceId() +"";
				else 
					return "";
			}
		};
		
		table.addColumn(rateContractServiceId,"Service Id");
		table.setColumnWidth(rateContractServiceId, 100, Unit.PX);
		rateContractServiceId.setSortable(true);
		
	}
	
	public void addColumnBillingId()
	{
		
		billingIdColumn=new TextColumn<BillingDocument>() {

			@Override
			public String getValue(BillingDocument object) {
				return object.getCount()+"";
			}
		};
		table.addColumn(billingIdColumn,"Billing ID");
		table.setColumnWidth(billingIdColumn, 100, Unit.PX);
		billingIdColumn.setSortable(true);
	}

	public void addColumnBillingDate()
	{
		billingDateColumn=new TextColumn<BillingDocument>() {

			@Override
			public String getValue(BillingDocument object) {
				if(object.getBillingDate()!=null)
				{
				    String serDate=AppUtility.parseDate(object.getBillingDate());
				    return serDate;
				}
				return "Not Set";
			}
		};
		
		table.addColumn(billingDateColumn,"Billing Date");
		table.setColumnWidth(billingDateColumn, 100, Unit.PX);
        billingDateColumn.setSortable(true);
	}

	
	private void addColumnBillingCustomerId() {
		customerIdColumn=new TextColumn<BillingDocument>() {

			@Override
			public String getValue(BillingDocument object) {
				return object.getCustomerCount()+"";
			}
		};
		
		table.addColumn(customerIdColumn,"BP ID");
		table.setColumnWidth(customerIdColumn, 100, Unit.PX);
		customerIdColumn.setSortable(true);
		
	}
	
	public void addColumnCustomerName()
	{
		customerNameColumn=new TextColumn<BillingDocument>() {

			@Override
			public String getValue(BillingDocument object) {
				return object.getName();
			}
		};
		
		table.addColumn(customerNameColumn,"BP Name");
		table.setColumnWidth(customerNameColumn, 100, Unit.PX);
		customerNameColumn.setSortable(true);
	}
	
	public void addColumnCustomerPhone()
	{
		customerCellColumn=new TextColumn<BillingDocument>() {

			@Override
			public String getValue(BillingDocument object) {
				return object.getCellNumber()+"";
			}
		};
		
		table.addColumn(customerCellColumn,"Cell Number");
		table.setColumnWidth(customerCellColumn, 100, Unit.PX);
		customerCellColumn.setSortable(true);
	}
	
	public void addColumnAccountType()
	{
		accountTypeColumn=new TextColumn<BillingDocument>() {

			@Override
			public String getValue(BillingDocument object) {
				return object.getAccountType();
			}
		};
		
		table.addColumn(accountTypeColumn,"Acc. Type");
		table.setColumnWidth(accountTypeColumn, 100, Unit.PX);
		accountTypeColumn.setSortable(true);
	}
	
	private void addColumnContractId() {
		contractIdColumn=new TextColumn<BillingDocument>() {

			@Override
			public String getValue(BillingDocument object) {
				return object.getContractCount()+"";
			}
		};
		
		table.addColumn(contractIdColumn,"Order ID");
		table.setColumnWidth(contractIdColumn, 100, Unit.PX);
		contractIdColumn.setSortable(true);
		
	}

	public void addColumnBillingAmount()
	{
		billingAmtColumn=new TextColumn<BillingDocument>() {

			@Override
			public String getValue(BillingDocument object) {
				return object.getTotalSalesAmount()+"";
			}
		};
		
		table.addColumn(billingAmtColumn,"Amount");
		table.setColumnWidth(billingAmtColumn, 100, Unit.PX);
		billingAmtColumn.setSortable(true);
	}

	public void addColumnBillingCategory()
	{
		billingCategoryColumn=new TextColumn<BillingDocument>() {

			@Override
			public String getValue(BillingDocument object) {
				return object.getBillingCategory();
			}
		};
		
		table.addColumn(billingCategoryColumn,"Billing Category");
		table.setColumnWidth(billingCategoryColumn, 100, Unit.PX);
		billingCategoryColumn.setSortable(true);
	}	

	public void addColumnBillingType()
	{
		billingTypeColumn=new TextColumn<BillingDocument>() {

			@Override
			public String getValue(BillingDocument object) {
				return object.getBillingType();
			}
		};
		
		table.addColumn(billingTypeColumn,"Billing Type");
		table.setColumnWidth(billingTypeColumn, 100, Unit.PX);
		billingTypeColumn.setSortable(true);
	}	
	
	public void addColumnBillingGroup()
	{
		billingGroupColumn=new TextColumn<BillingDocument>() {

			@Override
			public String getValue(BillingDocument object) {
				return object.getBillingGroup();
			}
		};
		
		table.addColumn(billingGroupColumn,"Billing Group");
		table.setColumnWidth(billingGroupColumn, 100, Unit.PX);
		billingGroupColumn.setSortable(true);
	}		
	
	public void addColumnApproverName()
	{
		approverNameColumn=new TextColumn<BillingDocument>() {

			@Override
			public String getValue(BillingDocument object) {
				return object.getApproverName();
			}
		};
		
		table.addColumn(approverNameColumn,"Approver Name");
		table.setColumnWidth(approverNameColumn, 100, Unit.PX);
		approverNameColumn.setSortable(true);
	}	
	
	public void addColumnBillingStatus()
	{
		statusColumn=new TextColumn<BillingDocument>() {

			@Override
			public String getValue(BillingDocument object) {
				return object.getStatus();
			}
		};
		
		table.addColumn(statusColumn,"Status");
		table.setColumnWidth(statusColumn, 100, Unit.PX);
		statusColumn.setSortable(true);
	}	
	
	public void addColumnBranch()
	{
		branchColumn=new TextColumn<BillingDocument>() {

			@Override
			public String getValue(BillingDocument object) {
				return object.getBranch();
			}
		};
		
		table.addColumn(branchColumn,"Branch");
		table.setColumnWidth(branchColumn, 100, Unit.PX);
		branchColumn.setSortable(true);
	}	
	
	
	
	public void addColumnSorting(){
		addSortinggetBillingCount();
		addSortinggetBillingDate();
		addSortinggetCustomerId();
		addSortinggetCustomerFullName();
		addSortinggetCustomerCellNumber();
		addSortingAccountType();
		addSortinggetContractCount();
		addSortinggetBillingAmount();
		addSortinggetBillingCategory();
		addSortinggetBillingType();
		addSortinggetBillingGroup();
		addSortinggetApproverName();
		addSortinggetStatus();
		addSortinggetBranch();
		
		addSortingRateContractServiceId();
		
		/*
		 *  nidhi
		 *  1-07-2017
		 *  sort method for segment
		 */
		addSortingSegment();
		/*
		 * end
		 */
		
		/**03-10-2017 sagar sore [adding number range column]**/
		addSortinggetNumberRange();
		
		addSortingOnRefNum();
		addSortingTypeOfOrder();
		addSortingSubBillType();
		if(parcialBillDisableFlag){
			addSortingCancleAmt();
			addSortingCancleAmtRemark();
			
		}
	}
	private void addSortingOnRefNum() {
		List<BillingDocument> list = getDataprovider().getList();
		columnSort = new ListHandler<BillingDocument>(list);
		columnSort.setComparator(getColRefNum,new Comparator<BillingDocument>() {
			@Override
			public int compare(BillingDocument e1, BillingDocument e2) {
				if (e1 != null && e2 != null) {
					if (e1.getRefNumber() != null&& e2.getRefNumber() != null) {
						return e1.getRefNumber().compareTo(e2.getRefNumber());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	protected void addSortingSegment()
	{
		List<BillingDocument> list=getDataprovider().getList();
		columnSort=new ListHandler<BillingDocument>(list);
		columnSort.setComparator(segmentcolumn, new Comparator<BillingDocument>()
				{
			@Override
			public int compare(BillingDocument e1,BillingDocument e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBranch()!=null && e2.getBranch()!=null){
						return e1.getBranch().compareTo(e2.getBranch());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	private void addSortingRateContractServiceId() {
		List<BillingDocument> list=getDataprovider().getList();
		columnSort=new ListHandler<BillingDocument>(list);
		columnSort.setComparator(rateContractServiceId, new Comparator<BillingDocument>()
				{
			@Override
			public int compare(BillingDocument e1,BillingDocument e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getRateContractServiceId()== e2.getRateContractServiceId()){
						return 0;}
					if(e1.getRateContractServiceId()> e2.getRateContractServiceId()){
						return 1;}
					else{
						return -1;
					}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addSortinggetBillingCount()
	{
		List<BillingDocument> list=getDataprovider().getList();
		columnSort=new ListHandler<BillingDocument>(list);
		columnSort.setComparator(billingIdColumn, new Comparator<BillingDocument>()
				{
			@Override
			public int compare(BillingDocument e1,BillingDocument e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetBillingDate()
	{
		List<BillingDocument> list=getDataprovider().getList();
		columnSort=new ListHandler<BillingDocument>(list);
		columnSort.setComparator(billingDateColumn, new Comparator<BillingDocument>()
				{
			@Override
			public int compare(BillingDocument e1,BillingDocument e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBillingDate()!=null && e2.getBillingDate()!=null){
						return e1.getBillingDate().compareTo(e2.getBillingDate());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetCustomerId()
	{
		List<BillingDocument> list=getDataprovider().getList();
		columnSort=new ListHandler<BillingDocument>(list);
		columnSort.setComparator(customerIdColumn, new Comparator<BillingDocument>()
				{
			@Override
			public int compare(BillingDocument e1,BillingDocument e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getPersonInfo().getCount()== e2.getPersonInfo().getCount()){
						return 0;}
					if(e1.getPersonInfo().getCount()> e2.getPersonInfo().getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetCustomerFullName()
	{
		List<BillingDocument> list=getDataprovider().getList();
		columnSort=new ListHandler<BillingDocument>(list);
		columnSort.setComparator(customerNameColumn, new Comparator<BillingDocument>()
				{
			@Override
			public int compare(BillingDocument e1,BillingDocument e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getPersonInfo().getFullName()!=null && e2.getPersonInfo().getFullName()!=null){
						return e1.getPersonInfo().getFullName().compareTo(e2.getPersonInfo().getFullName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetCustomerCellNumber()
	{
		List<BillingDocument> list=getDataprovider().getList();
		columnSort=new ListHandler<BillingDocument>(list);
		columnSort.setComparator(customerCellColumn, new Comparator<BillingDocument>()
				{
			@Override
			public int compare(BillingDocument e1,BillingDocument e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getPersonInfo().getCellNumber()== e2.getPersonInfo().getCellNumber()){
						return 0;}
					if(e1.getPersonInfo().getCellNumber()> e2.getPersonInfo().getCellNumber()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortingAccountType()
	{
		List<BillingDocument> list=getDataprovider().getList();
		columnSort=new ListHandler<BillingDocument>(list);
		columnSort.setComparator(accountTypeColumn, new Comparator<BillingDocument>()
				{
			@Override
			public int compare(BillingDocument e1,BillingDocument e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getAccountType()!=null && e2.getAccountType()!=null){
						return e1.getAccountType().compareTo(e2.getAccountType());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetContractCount()
	{
		List<BillingDocument> list=getDataprovider().getList();
		columnSort=new ListHandler<BillingDocument>(list);
		columnSort.setComparator(contractIdColumn, new Comparator<BillingDocument>()
				{
			@Override
			public int compare(BillingDocument e1,BillingDocument e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getContractCount()== e2.getContractCount()){
						return 0;}
					if(e1.getContractCount()> e2.getContractCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetBillingAmount()
	{
		List<BillingDocument> list=getDataprovider().getList();
		columnSort=new ListHandler<BillingDocument>(list);
		columnSort.setComparator(billingAmtColumn, new Comparator<BillingDocument>()
				{
			@Override
			public int compare(BillingDocument e1,BillingDocument e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getTotalBillingAmount()== e2.getTotalBillingAmount()){
						return 0;}
					if(e1.getTotalBillingAmount()> e2.getTotalBillingAmount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetBillingCategory()
	{
		List<BillingDocument> list=getDataprovider().getList();
		columnSort=new ListHandler<BillingDocument>(list);
		columnSort.setComparator(billingCategoryColumn, new Comparator<BillingDocument>()
				{
			@Override
			public int compare(BillingDocument e1,BillingDocument e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBillingCategory()!=null && e2.getBillingCategory()!=null){
						return e1.getBillingCategory().compareTo(e2.getBillingCategory());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetBillingType()
	{
		List<BillingDocument> list=getDataprovider().getList();
		columnSort=new ListHandler<BillingDocument>(list);
		columnSort.setComparator(billingTypeColumn, new Comparator<BillingDocument>()
				{
			@Override
			public int compare(BillingDocument e1,BillingDocument e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBillingType()!=null && e2.getBillingType()!=null){
						return e1.getBillingType().compareTo(e2.getBillingType());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetBillingGroup()
	{
		List<BillingDocument> list=getDataprovider().getList();
		columnSort=new ListHandler<BillingDocument>(list);
		columnSort.setComparator(billingGroupColumn, new Comparator<BillingDocument>()
				{
			@Override
			public int compare(BillingDocument e1,BillingDocument e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBillingGroup()!=null && e2.getBillingGroup()!=null){
						return e1.getBillingGroup().compareTo(e2.getBillingGroup());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetApproverName()
	{
		List<BillingDocument> list=getDataprovider().getList();
		columnSort=new ListHandler<BillingDocument>(list);
		columnSort.setComparator(approverNameColumn, new Comparator<BillingDocument>()
				{
			@Override
			public int compare(BillingDocument e1,BillingDocument e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getApproverName()!=null && e2.getApproverName()!=null){
						return e1.getApproverName().compareTo(e2.getApproverName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetStatus()
	{
		List<BillingDocument> list=getDataprovider().getList();
		columnSort=new ListHandler<BillingDocument>(list);
		columnSort.setComparator(statusColumn, new Comparator<BillingDocument>()
				{
			@Override
			public int compare(BillingDocument e1,BillingDocument e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getStatus()!=null && e2.getStatus()!=null){
						return e1.getStatus().compareTo(e2.getStatus());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	
	protected void addSortinggetBranch()
	{
		List<BillingDocument> list=getDataprovider().getList();
		columnSort=new ListHandler<BillingDocument>(list);
		columnSort.setComparator(branchColumn, new Comparator<BillingDocument>()
				{
			@Override
			public int compare(BillingDocument e1,BillingDocument e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBranch()!=null && e2.getBranch()!=null){
						return e1.getBranch().compareTo(e2.getBranch());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	/**03-10-2017 sagar sore [To create number range column and to sort it]**/
	public void addColumnNumberRange()
	{
		numberRangeColumn=new TextColumn<BillingDocument>() {

			@Override
			public String getValue(BillingDocument object) {
				return object.getNumberRange();
			}
		};
		
		table.addColumn(numberRangeColumn,"Number Range");
		table.setColumnWidth(numberRangeColumn, 120, Unit.PX);
		numberRangeColumn.setSortable(true);
	}	
	
	protected void addSortinggetNumberRange()
	{
		List<BillingDocument> list=getDataprovider().getList();
		columnSort=new ListHandler<BillingDocument>(list);
		columnSort.setComparator(numberRangeColumn, new Comparator<BillingDocument>()
				{
			@Override
			public int compare(BillingDocument e1,BillingDocument e2)
			{
				if (e1 != null && e2 != null) {
					
					if (e1.getNumberRange() == null && e2.getNumberRange() == null){
						return 0;
					}
					if(e1.getNumberRange() == null) return 1;							
					if(e2.getNumberRange() == null) return -1;							
					return e1.getNumberRange().compareTo(e2.getNumberRange());

				}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	/**end**/
	
	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
	/** date 1.11.2018 added by komal for type of order and sub bill type **/
	public void getColTypeOfOrder()
	{
		getColTypeOfOrder=new TextColumn<BillingDocument>() {

			@Override
			public String getValue(BillingDocument object) {
				return object.getTypeOfOrder();
			}
		};
		
		table.addColumn(getColTypeOfOrder,"Type Of Order");
		table.setColumnWidth(getColTypeOfOrder, 120, Unit.PX);
		getColTypeOfOrder.setSortable(true);
	}	
	
	protected void addSortingTypeOfOrder()
	{
		List<BillingDocument> list=getDataprovider().getList();
		columnSort=new ListHandler<BillingDocument>(list);
		columnSort.setComparator(getColTypeOfOrder, new Comparator<BillingDocument>()
				{
			@Override
			public int compare(BillingDocument e1,BillingDocument e2)
			{
				if (e1 != null && e2 != null) {
					
					if (e1.getNumberRange() == null && e2.getNumberRange() == null){
						return 0;
					}
					if(e1.getNumberRange() == null) return 1;							
					if(e2.getNumberRange() == null) return -1;							
					return e1.getNumberRange().compareTo(e2.getNumberRange());

				}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	public void getColSubBillType()
	{
		getColSubBillType=new TextColumn<BillingDocument>() {

			@Override
			public String getValue(BillingDocument object) {
				return object.getSubBillType();
			}
		};
		
		table.addColumn(getColSubBillType,"Sub Bill Type");
		table.setColumnWidth(getColSubBillType, 120, Unit.PX);
		getColSubBillType.setSortable(true);
	}	
	
	protected void addSortingSubBillType()
	{
		List<BillingDocument> list=getDataprovider().getList();
		columnSort=new ListHandler<BillingDocument>(list);
		columnSort.setComparator(getColSubBillType, new Comparator<BillingDocument>()
				{
			@Override
			public int compare(BillingDocument e1,BillingDocument e2)
			{
				if (e1 != null && e2 != null) {
					
					if (e1.getNumberRange() == null && e2.getNumberRange() == null){
						return 0;
					}
					if(e1.getNumberRange() == null) return 1;							
					if(e2.getNumberRange() == null) return -1;							
					return e1.getNumberRange().compareTo(e2.getNumberRange());

				}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}

	/**end**/
	public void getColCancleAmt()
	{
		getColCancleAmt=new TextColumn<BillingDocument>() {

			@Override
			public String getValue(BillingDocument object) {
				return object.getCancleAmt()+"";
			}
		};
		
		table.addColumn(getColCancleAmt,"Cancel Amount");
		table.setColumnWidth(getColCancleAmt, 120, Unit.PX);
		getColCancleAmt.setSortable(true);
	}	
	
	public void getColCancleAmtRemark()
	{
		getColCancleRemark=new TextColumn<BillingDocument>() {

			@Override
			public String getValue(BillingDocument object) {
				if(object.getCancleAmtRemark()!=null)
					return object.getCancleAmtRemark()+"";
				else{
					return "";
				}
			}
		};
		
		table.addColumn(getColCancleRemark,"Cancel Amount Remark");
		table.setColumnWidth(getColCancleRemark, 120, Unit.PX);
		getColCancleRemark.setSortable(true);
	}	
	
	protected void addSortingCancleAmt()
	{
		List<BillingDocument> list=getDataprovider().getList();
		columnSort=new ListHandler<BillingDocument>(list);
		columnSort.setComparator(getColCancleAmt, new Comparator<BillingDocument>()
				{
			@Override
			public int compare(BillingDocument e1,BillingDocument e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCancleAmt()== e2.getCancleAmt()){
						return 0;}
					if(e1.getCancleAmt()> e2.getCancleAmt()){
						return 1;}
					else{
						return -1;
					}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	

	protected void addSortingCancleAmtRemark()
	{
		List<BillingDocument> list=getDataprovider().getList();
		columnSort=new ListHandler<BillingDocument>(list);
		columnSort.setComparator(getColCancleRemark, new Comparator<BillingDocument>()
				{
			@Override
			public int compare(BillingDocument e1,BillingDocument e2)
			{
				if (e1 != null && e2 != null) {
					
					if (e1.getCancleAmtRemark() == null && e2.getCancleAmtRemark() == null){
						return 0;
					}
					if(e1.getCancleAmtRemark() == null) return 1;							
					if(e2.getCancleAmtRemark() == null) return -1;							
					return e1.getCancleAmtRemark().compareTo(e2.getCancleAmtRemark());

				}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}

}
