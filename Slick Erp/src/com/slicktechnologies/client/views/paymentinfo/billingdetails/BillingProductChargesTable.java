package com.slicktechnologies.client.views.paymentinfo.billingdetails;

import java.util.List;

import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;

public class BillingProductChargesTable extends SuperTable<ContractCharges> {

	TextColumn<ContractCharges> orderId;
	TextColumn<ContractCharges> billingId;
	TextColumn<ContractCharges> billingChargeNameColumn;
	TextColumn<ContractCharges> billingChargePercentColumn;
	TextColumn<ContractCharges> billingChargeAbsValColumn;
	TextColumn<ContractCharges> billingChargeAssessAmtColumn;
	TextColumn<ContractCharges> billingTotalPayableColumn;
	TextColumn<ContractCharges> balanceAmountColumn;
	Column<ContractCharges, String> payablePercentColumn;
	TextColumn<ContractCharges> payableAmountColumn;
	TextColumn<ContractCharges> viewpayablePercentColumn;
	NumberFormat nf = NumberFormat.getFormat("0.00");

	public BillingProductChargesTable() {
		super();
	}

	/****************************************************************************/

	/********** Create Table Method ***********/
	@Override
	public void createTable() {
		createColumnOrderId();
		createColumnBillingId();
		createColumnChargeName();
		createColumnChargePercent();
		createColumnChargeAbsVal();
		createColumnAssessableAmount();
		createColumnTotalPayable();
		createColumnBalanceAmount();
		createColumnPayablePercent();
		createColumnPayableAmount();
		addFieldUpdater();
		table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
	}

	
	
	
	public void createColumnBillingId() {
		billingId = new TextColumn<ContractCharges>() {

			@Override
			public String getValue(ContractCharges object) {
				if (object.getBillingId() != 0) {
					return object.getBillingId() + "";
				}
				return "";
			}
		};
		table.addColumn(billingId, "Billing Id");
		table.setColumnWidth(billingId, "90px");
	}
	
	public void createColumnOrderId() {
		orderId = new TextColumn<ContractCharges>() {

			@Override
			public String getValue(ContractCharges object) {
				if (object.getOrderId() != 0) {
					return object.getOrderId() + "";
				}
				return "";
			}
		};
		table.addColumn(orderId, "Order Id");
		table.setColumnWidth(orderId, "90px");
	}

	public void createColumnChargeName() {
		billingChargeNameColumn = new TextColumn<ContractCharges>() {

			@Override
			public String getValue(ContractCharges object) {
				return object.getTaxChargeName().trim();
			}
		};
		table.addColumn(billingChargeNameColumn, "Charge Name");
		table.setColumnWidth(billingChargeNameColumn, "110px");
	}

	public void createColumnChargePercent() {
		billingChargePercentColumn = new TextColumn<ContractCharges>() {
			@Override
			public String getValue(ContractCharges object) {
				if (object.getTaxChargePercent() == 0) {
					return "N.A";
				} else {
					return object.getTaxChargePercent() + "";
				}
			}
		};
		table.addColumn(billingChargePercentColumn, "%");
		table.setColumnWidth(billingChargePercentColumn, "40px");
	}

	public void createColumnChargeAbsVal() {
		billingChargeAbsValColumn = new TextColumn<ContractCharges>() {
			@Override
			public String getValue(ContractCharges object) {
				if (object.getTaxChargeAbsVal() == 0) {
					return "N.A";
				} else {
					return object.getTaxChargeAbsVal() + "";
				}

			}
		};
		table.addColumn(billingChargeAbsValColumn, "Abs Val");
		table.setColumnWidth(billingChargeAbsValColumn, "72px");
	}

	public void createColumnAssessableAmount() {
		billingChargeAssessAmtColumn = new TextColumn<ContractCharges>() {
			@Override
			public String getValue(ContractCharges object) {
				if (object.getTaxChargeAssesVal() == 0) {
					return "N.A";
				} else {
					return nf.format(object.getTaxChargeAssesVal());
				}
			}
		};
		table.addColumn(billingChargeAssessAmtColumn, "Asses Val");
		table.setColumnWidth(billingChargeAssessAmtColumn, "90px");

	}

	public void createColumnTotalPayable() {
		billingTotalPayableColumn = new TextColumn<ContractCharges>() {
			@Override
			public String getValue(ContractCharges object) {
				double totalPayble = 0;
				if (object.getTaxChargeAbsVal() != 0) {
					totalPayble = object.getTaxChargeAbsVal();
				}
				if (object.getTaxChargePercent() != 0) {
					totalPayble = object.getTaxChargePercent()* object.getTaxChargeAssesVal() / 100;
				}
				return nf.format(totalPayble);
			}
		};
		table.addColumn(billingTotalPayableColumn, "Total Payable");
		table.setColumnWidth(billingTotalPayableColumn, "130px");

	}

	public void createColumnBalanceAmount() {
		balanceAmountColumn = new TextColumn<ContractCharges>() {
			@Override
			public String getValue(ContractCharges object) {
				if (object.getChargesBalanceAmt() == 0) {
					return 0 + "";
				} else {
					return nf.format(object.getChargesBalanceAmt());
				}

			}
		};
		table.addColumn(balanceAmountColumn, "Bal. Amt");
		table.setColumnWidth(balanceAmountColumn, "100px");
	}

	protected void createColumnPayableAmount() {
		payableAmountColumn = new TextColumn<ContractCharges>() {
			@Override
			public String getValue(ContractCharges object) {
				double payAmtVal = 0;
				// double totalPayble=0;
				// if(object.getTaxChargeAbsVal()!=0){
				// totalPayble=object.getTaxChargeAbsVal();
				// }
				// if(object.getTaxChargePercent()!=0){
				// totalPayble=object.getTaxChargePercent()*object.getTaxChargeAssesVal()/100;
				// }
				// if(object.getChargesBalanceAmt()!=0){
				// payAmtVal=Double.parseDouble(nf.format(object.getPaypercent()*totalPayble/100));
				// }
//				if (object.getChargesBalanceAmt() != 0) {
//					payAmtVal = object.getPaypercent()* object.getChargesBalanceAmt() / 100;
//				}
//
//				object.setPayableAmt(payAmtVal);
				return nf.format(object.getPayableAmt());
			}
		};
		table.addColumn(payableAmountColumn, "Payable Amt");
		table.setColumnWidth(payableAmountColumn, "100px");
	}

	protected void createColumnPayablePercent() {
		EditTextCell editCell = new EditTextCell();
		payablePercentColumn = new Column<ContractCharges, String>(editCell) {
			@Override
			public String getValue(ContractCharges object) {
				return object.getPaypercent() + "";
			}
		};
		table.addColumn(payablePercentColumn, "#Pay %");
		table.setColumnWidth(payablePercentColumn, "72px");
	}

	protected void createColumnViewPayablePercent() {
		viewpayablePercentColumn = new TextColumn<ContractCharges>() {
			@Override
			public String getValue(ContractCharges object) {
				return object.getPaypercent() + "";
			}
		};
		table.addColumn(viewpayablePercentColumn, "#Pay %");
		table.setColumnWidth(viewpayablePercentColumn, "72px");
	}

	protected void createFieldUpdaterPayablePercent() {
		payablePercentColumn.setFieldUpdater(new FieldUpdater<ContractCharges, String>() {
			@Override
			public void update(int index, ContractCharges object,String value) {
				try {
					 double payAmtVal=0;
					Integer val1 = Integer.parseInt(value.trim());
					object.setPaypercent(val1);
					
					payAmtVal = object.getPaypercent()* object.getChargesBalanceAmt() / 100;
					object.setPayableAmt(payAmtVal);
					
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				} catch (NumberFormatException e) {
				}
				table.redrawRow(index);
			}
		});
	}

	@Override
	protected void initializekeyprovider() {

	}

	@Override
	public void addFieldUpdater() {
		createFieldUpdaterPayablePercent();
	}

	private void addeditColumn() {
		createColumnOrderId();
		createColumnBillingId();
		createColumnChargeName();
		createColumnChargePercent();
		createColumnChargeAbsVal();
		createColumnAssessableAmount();
		createColumnTotalPayable();
		createColumnBalanceAmount();
		createColumnPayablePercent();
		createColumnPayableAmount();
		addFieldUpdater();
	}

	private void addViewColumn() {
		createColumnOrderId();
		createColumnBillingId();
		createColumnChargeName();
		createColumnChargePercent();
		createColumnChargeAbsVal();
		createColumnAssessableAmount();
		createColumnTotalPayable();
		createColumnBalanceAmount();
		createColumnViewPayablePercent();
		createColumnPayableAmount();
	}

	@Override
	public void setEnable(boolean state) {
		int tablecolcount = this.table.getColumnCount();
		for (int i = tablecolcount - 1; i > -1; i--)
			table.removeColumn(i);
		if (state == true) {
			addeditColumn();
		}
		if (state == false) {
			addViewColumn();
		}
	}

	@Override
	public void applyStyle() {

	}

	public double calculateTotalBillingCharges() {
		List<ContractCharges> list = getDataprovider().getList();
		double sum = 0;
		for (int i = 0; i < list.size(); i++) {
			ContractCharges entity = list.get(i);
			sum = sum+ (entity.getChargesBalanceAmt() * entity.getPaypercent() / 100);
		}
		return sum;
	}

	/************************* Calculate Net Payable Method ******************************************/
	/**
	 * This method calculates the total charges amount i.e surcharge amount. The
	 * amount is calculated and fetched in Presenter and the value is set to Net
	 * Payable field in Form.
	 * 
	 * @return
	 */

	public double calculateNetPayable() {
		List<ContractCharges> list = getDataprovider().getList();

		double sum = 0, calcTotal = 0;
		for (int i = 0; i < list.size(); i++) {
			ContractCharges entity = list.get(i);
			if (entity.getPaypercent() != 0) {
				calcTotal = entity.getPaypercent()* entity.getChargesBalanceAmt() / 100;
			}
			if (entity.getTaxChargeAbsVal() != 0) {
				calcTotal = entity.getTaxChargeAbsVal();
			}
			sum = sum + calcTotal;
		}

		return sum;
	}
}
