package com.slicktechnologies.client.views.paymentinfo.billingdetails;

import java.util.Vector;

import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.common.ProjectAllocation;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

public class BillingDetailsSearchPopUp extends SearchPopUpScreen<BillingDocument> implements SelectionHandler<Suggestion> {
	
	public IntegerBox ibbillingid;
	public IntegerBox ibcontractid;
	public PersonInfoComposite personInfo;
	public PersonInfoComposite vendorInfo;
	public DateComparator dateComparator;
	public ObjectListBox<ConfigCategory> olbbillingcategory;
	public ObjectListBox<Type> olbbillingtype;
	public ObjectListBox<Config> olbbillinggroup;
	public ListBox lbstatus;
	public ObjectListBox<Employee> olbApproverName;
	public ListBox lbaccountType;
	
	//***************rohan added this for branch wise search ******
	public ObjectListBox<Branch> olbBranch;
	
	//  vijay added this on 15/2/2017 for service id search
	public IntegerBox ibrateContractServiceId;
	
	/*
	 * nidhi
	 * 30-06-2017
	 *  customer category search
	 */
	public ObjectListBox<ConfigCategory> olbCustomerCategory;
	public TextBox tbSegment;
	/*
	 * end
	 */
	/**03-10-2017 sagar sore [ To add field for number range in Search pop up]***/
	public ObjectListBox<Config> olbcNumberRange;
	
	/**
	 * Date : 18-11-2017 BY ANIL
	 */
	TextBox tbReferenceNumber;
	/** date 20.10.2018 added by komal**/
	ObjectListBox<String> olbTypeOforder;
	ObjectListBox<String> olbSubBillType;
	
	/**
	 * @author Anil @since 12-11-2021
	 */
//	TextBox tbProjectName;
	ObjectListBox<ProjectAllocation> project;
	
	
	public BillingDetailsSearchPopUp() {
		super();
		createGui();
	}
	
	public BillingDetailsSearchPopUp(boolean b) {
		super(b);
		createGui();
	}
	
	public void initWidget()
	{
		ibbillingid= new IntegerBox();
		ibcontractid=new IntegerBox();
		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new Customer());
		personInfo=new PersonInfoComposite(querry,false);
		
		MyQuerry vendorquerry=new MyQuerry();
		vendorquerry.setQuerryObject(new Vendor());
		vendorInfo=new PersonInfoComposite(vendorquerry,false);
		
		//**************rohan made changes here ***************
		
		olbBranch= new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbBranch);
		
		//******************changes ends here *****************
		
		dateComparator=new DateComparator("billingDocumentInfo.billingDate",new BillingDocument());
		olbbillingcategory=new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(olbbillingcategory, Screen.BILLINGCATEGORY);
		olbbillingtype=new ObjectListBox<Type>();
		AppUtility.makeTypeListBoxLive(olbbillingtype, Screen.BILLINGTYPE);
		olbbillinggroup=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbbillinggroup, Screen.BILLINGGROUP);
		lbstatus= new ListBox();
		AppUtility.setStatusListBox(lbstatus,BillingDocument.getStatusList());
		olbApproverName=new ObjectListBox<Employee>();
		AppUtility.makeSalesPersonListBoxLive(olbApproverName);
		
		lbaccountType=new ListBox();
		lbaccountType.addItem("SELECT");
		lbaccountType.addItem(AppConstants.BILLINGACCOUNTTYPEAP);
		lbaccountType.addItem(AppConstants.BILLINGACCOUNTTYPEAR);
		
		personInfo.getId().addSelectionHandler(this);
		vendorInfo.getId().addSelectionHandler(this);
		
		personInfo.getCustomerId().getHeaderLabel().setText("Customer ID");
		personInfo.getCustomerName().getHeaderLabel().setText("Customer Name");
		personInfo.getCustomerCell().getHeaderLabel().setText("Customer Cell");
		
		vendorInfo.getCustomerId().getHeaderLabel().setText("Vendor ID");
		vendorInfo.getCustomerName().getHeaderLabel().setText("Vendor Name");
		vendorInfo.getCustomerCell().getHeaderLabel().setText("Vendor Cell");
		
		ibrateContractServiceId = new IntegerBox();
		
		/*
		 * 	nidhi
		 *  30-06-2017
		 *  for search
		 */
		olbCustomerCategory= new ObjectListBox<ConfigCategory>();
//		AppUtility.makeTypeListBoxLive(olbCustomerCategory, Screen.CONTRACTTYPE);
		AppUtility.MakeLiveCategoryConfig(olbCustomerCategory, Screen.CUSTOMERCATEGORY);
		tbSegment = new TextBox();
		/* 
		 * 
		 *   end
		 */
		/**03-10-2017 sagar sore[ olbcNumberRange initialization, Displaying values added from process configuration]***/
		olbcNumberRange= new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbcNumberRange,Screen.NUMBERRANGE);
		
		tbReferenceNumber=new TextBox();
		/** date 20.10.2018 added by komal**/
		olbTypeOforder = new ObjectListBox<String>();
		olbTypeOforder.addItem("--SELECT--");
		olbTypeOforder.addItem(AppConstants.ORDERTYPESALES);
		olbTypeOforder.addItem(AppConstants.ORDERTYPESERVICE);
		olbTypeOforder.addItem(AppConstants.ORDERTYPEPURCHASE);
		
		olbSubBillType = new ObjectListBox<String>();
		AppUtility.setStatusListBox(olbSubBillType, Invoice.getSubBillTypeList());
//		olbSubBillType.addItem("--SELECT--");
//		olbSubBillType.addItem(AppConstants.STAFFINGANDRENTAL);
//		olbSubBillType.addItem(AppConstants.CONSUMABLES);
//		olbSubBillType.addItem(AppConstants.NATIONALHOLIDAY);
//		olbSubBillType.addItem(AppConstants.OVERTIME);
//		olbSubBillType.addItem(AppConstants.OTHERSERVICES);
		
		project = new ObjectListBox<ProjectAllocation>();
		MyQuerry q1=new MyQuerry();
		Filter temp = new Filter();
		temp.setBooleanvalue(true);
		temp.setQuerryString("projectStatus");
		q1.getFilters().add(temp);
		q1.setQuerryObject(new ProjectAllocation());
		project.MakeLive(q1);
	}
	
	public void createScreen()
	{
		initWidget();
		FormFieldBuilder builder;
		builder = new FormFieldBuilder("Billing ID",ibbillingid);
		FormField fibbillingid= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("",personInfo);
		FormField fPersonInfo= builder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		builder = new FormFieldBuilder("",vendorInfo);
		FormField fvendorInfo= builder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		builder = new FormFieldBuilder("Order ID",ibcontractid);
		FormField fibcontractid= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("From Date (Billing Date)",dateComparator.getFromDate());
		FormField fdateComparatorfrom= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("To Date (Billing Date)",dateComparator.getToDate());
		FormField fdateComparatorto= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Billing Category",olbbillingcategory);
		FormField folbbillingcategory= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Billing Type",olbbillingtype);
		FormField folbbillingtype= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Billing Group",olbbillinggroup);
		FormField folbbillinggroup= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Status",lbstatus);
		FormField flbstatus=builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Approver Name",olbApproverName);
		FormField folbApproverName= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Account Type",lbaccountType);
		FormField flbaccountType= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Branch",olbBranch);
		FormField folbBranch= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Service id",ibrateContractServiceId);
		FormField fibrateContractServiceId= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/*
		 * 	nidhi
		 * 	30-6-2017
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "OnlyForNBHC"))
		{
			builder = new FormFieldBuilder("Segment Type",olbCustomerCategory);
			
		}
		else{
			builder = new FormFieldBuilder("Segment Type",tbSegment);
		}
		FormField ftbSagment= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/**03-10-2017 sagar sore[ adding search by number range]**/
		FormField folbcNumberRange;	
		builder = new FormFieldBuilder("Number Range",olbcNumberRange);
		folbcNumberRange= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/**
		 * Date : 18-11-2017 BY ANIL
		 */
		builder = new FormFieldBuilder("Reference Number",tbReferenceNumber);
		FormField ftbReferenceNumber= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/**
		 * Date 25-01-2018 By Vijay 
		 * differentiated date combination search and other filter search
		 */
		builder = new FormFieldBuilder();
		FormField fgroupingDateFilterInformation=builder.setlabel("Date Filter Combination").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();

		builder = new FormFieldBuilder();
		FormField fgroupingOtherFilterInformation=builder.setlabel("Other Filter Combination").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		/**
		 * ends here
		 */
		
		/** date 20.10.2018 added by komal**/
		builder = new FormFieldBuilder("Type Of Order",olbTypeOforder);
		FormField folbTypeOforder= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Sub Bill Type",olbSubBillType);
		FormField folbSubBillType= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Project Name",project);
		FormField ftbProjectName= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/**
		 * Date 12-07-2018 By Vijay
		 * Des :- for NBHC CCPM need only limited search fields and else for all standard
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "OnlyForNBHC")){
			this.fields=new FormField[][]{
					{fgroupingDateFilterInformation},
					{fdateComparatorfrom,fdateComparatorto,folbBranch,flbstatus},
					{fgroupingOtherFilterInformation},
					{folbbillingcategory,folbbillingtype,folbbillinggroup,flbaccountType}, //folbcNumberRange
					{fibbillingid,fibcontractid,fibrateContractServiceId},
					{fPersonInfo},
			};
		}
		else{
		
			this.fields=new FormField[][]{
					/****03-10-2017 sagar sore [Number range column added and fields rearranged]*******/
					/**old code**/
	//				{fibbillingid,fdateComparatorfrom,fdateComparatorto,flbstatus},
	//				{folbApproverName,folbbillingcategory,folbbillingtype,folbbillinggroup},
	//				{fibcontractid,flbaccountType,folbBranch,fibrateContractServiceId},
	//				{fPersonInfo},
	//				{fvendorInfo},
	//				{ftbSagment}
				
					/**updated code**/
					{fgroupingDateFilterInformation},
					{fdateComparatorfrom,fdateComparatorto,folbBranch,flbstatus},
					{ftbProjectName},
					{fgroupingOtherFilterInformation},
					{folbcNumberRange,folbbillingcategory,folbbillingtype,folbbillinggroup},
					{fibbillingid,fibcontractid,flbaccountType,fibrateContractServiceId},
					{fPersonInfo},
					{fvendorInfo},
					{ftbSagment,folbApproverName,ftbReferenceNumber},
					{folbTypeOforder , folbSubBillType }/** date 20.10.2018 added by komal**/
			};
			
		}
	}
	

	public MyQuerry getQuerry()
	{
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		
		if(dateComparator.getValue()!=null)
			filtervec.addAll(dateComparator.getValue());
		
		if(olbbillingcategory.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbbillingcategory.getValue().trim());
			temp.setQuerryString("billingDocumentInfo.billingCategory");
			filtervec.add(temp);
		}
		
		if(olbbillingtype.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbbillingtype.getValue().trim());
			temp.setQuerryString("billingDocumentInfo.billingType");
			filtervec.add(temp);
		}
		
		if(olbbillinggroup.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbbillinggroup.getValue().trim());
			temp.setQuerryString("billingDocumentInfo.billingGroup");
			filtervec.add(temp);
		}

		if(ibbillingid.getValue()!=null){
			temp=new Filter();
			temp.setIntValue(ibbillingid.getValue());
			temp.setQuerryString("count");
			filtervec.add(temp);
		}
		
		if(ibcontractid.getValue()!=null){
			temp=new Filter();
			temp.setIntValue(ibcontractid.getValue());
			temp.setQuerryString("contractCount");
			filtervec.add(temp);
		}
		
		if(lbaccountType.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(lbaccountType.getItemText(lbaccountType.getSelectedIndex()).trim());
			temp.setQuerryString("accountType");
			filtervec.add(temp);
		}
		
		if(personInfo.getIdValue()!=-1)
		  {
		  temp=new Filter();
		  temp.setIntValue(personInfo.getIdValue());
		  temp.setQuerryString("personInfo.count");
		  filtervec.add(temp);
		  }
		  
		  if(!(personInfo.getFullNameValue().equals("")))
		  {
		  temp=new Filter();
		  temp.setStringValue(personInfo.getFullNameValue());
		  temp.setQuerryString("personInfo.fullName");
		  filtervec.add(temp);
		  }
		  if(personInfo.getCellValue()!=-1l)
		  {
		  temp=new Filter();
		  temp.setLongValue(personInfo.getCellValue());
		  temp.setQuerryString("personInfo.cellNumber");
		  filtervec.add(temp);
		  }
		  
		  
		  if(vendorInfo.getIdValue()!=-1)
		  {
		  temp=new Filter();
		  temp.setIntValue(vendorInfo.getIdValue());
		  temp.setQuerryString("personInfo.count");
		  filtervec.add(temp);
		  }
		  
		  if(!(vendorInfo.getFullNameValue().equals("")))
		  {
		  temp=new Filter();
		  temp.setStringValue(vendorInfo.getFullNameValue());
		  temp.setQuerryString("personInfo.fullName");
		  filtervec.add(temp);
		  }
		  if(vendorInfo.getCellValue()!=-1l)
		  {
		  temp=new Filter();
		  temp.setLongValue(vendorInfo.getCellValue());
		  temp.setQuerryString("personInfo.cellNumber");
		  filtervec.add(temp);
		  }
		  
		  
		  if(lbstatus.getSelectedIndex()!=0){
				temp=new Filter();
				temp.setStringValue(lbstatus.getItemText(lbstatus.getSelectedIndex()));
				filtervec.add(temp);
				temp.setQuerryString("status");
				filtervec.add(temp);
			}
		  
		  if(olbApproverName.getSelectedIndex()!=0){
				temp=new Filter();temp.setStringValue(olbApproverName.getValue().trim());
				temp.setQuerryString("approverName");
				filtervec.add(temp);
			}
		 
		  
		  if(LoginPresenter.branchRestrictionFlag==true && olbBranch.getSelectedIndex()==0)
			{
				temp=new Filter();
				temp.setList(AppUtility.getbranchlist());
				temp.setQuerryString("branch IN");
				filtervec.add(temp);
			}
			else{
				if(olbBranch.getSelectedIndex()!=0){
					temp=new Filter();
					temp.setStringValue(olbBranch.getValue().trim());
					temp.setQuerryString("branch");
					filtervec.add(temp);
				}
			}
		
		
		if(ibrateContractServiceId.getValue()!=null){
			temp=new Filter();
			temp.setIntValue(ibrateContractServiceId.getValue());
			temp.setQuerryString("rateContractServiceId");
			filtervec.add(temp);
		}
		
		/*
		 * nidhi
		 * 30-06-017
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "OnlyForNBHC"))
		{
			if(olbCustomerCategory.getSelectedIndex()!=0){
				temp=new Filter();
				temp.setStringValue(olbCustomerCategory.getValue().trim());
				temp.setQuerryString("segment");
				filtervec.add(temp);
			}
		}else{
			if(!tbSegment.getValue().equals("")){
				
				temp=new Filter();
				temp.setStringValue(tbSegment.getValue().trim());
				temp.setQuerryString("segment");
				filtervec.add(temp);
			}
		}
		/*
		 * end
		 * 
		 */
		/**03-10-2017 sagar sore[ filter for search by number range i.e. Billing and Non-billing]**/
		System.out.println("BillingListSearchPopup :  olbcNumber range get selected "+olbcNumberRange.getSelectedIndex());
		if(olbcNumberRange.getSelectedIndex()!=0){
			System.out.println("   olbc number range : "+olbcNumberRange.getValue().trim()+" :: "+olbcNumberRange.getTitle());
			temp=new Filter();
			temp.setStringValue(olbcNumberRange.getValue().trim());
			temp.setQuerryString("numberRange");
			filtervec.add(temp);
		}
		
		/**
		 * Date : 18-11-2017 BY ANIL
		 */
		if(tbReferenceNumber.getValue()!=null&&!tbReferenceNumber.getValue().equals("")){
			temp=new Filter();
			temp.setStringValue(tbReferenceNumber.getValue());
			temp.setQuerryString("refNumber");
			filtervec.add(temp);
		}
		/** date 20.10.2018 added by komal**/
		if(olbTypeOforder.getSelectedIndex() != 0){
			temp=new Filter();
			temp.setStringValue(olbTypeOforder.getValue(olbTypeOforder.getSelectedIndex()));
			temp.setQuerryString("typeOfOrder");
			filtervec.add(temp);
		}
		
		if(olbSubBillType.getSelectedIndex() != 0){
			temp=new Filter();
			temp.setStringValue(olbSubBillType.getValue(olbSubBillType.getSelectedIndex()));
			temp.setQuerryString("subBillType");
			filtervec.add(temp);
		}
		
		/**
		 * @author Anil @since 12-11-2021
		 * Sunrise SCM
		 */
		if (project.getValue() != null) {
			temp = new Filter();
			temp.setStringValue(project.getValue());
			temp.setQuerryString("projectName");
			filtervec.add(temp);
		}

		MyQuerry querry= new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new BillingDocument());
		return querry;
	}

	@Override
	public void onSelection(SelectionEvent<Suggestion> event)
	{
		if(event.getSource().equals(personInfo.getId())||event.getSource().equals(personInfo.getName())||event.getSource().equals(personInfo.getPhone()))
		{
				vendorInfo.clear();
		}
		if(event.getSource().equals(personInfo.getName()))
		{
				vendorInfo.clear();
		}
		if(event.getSource().equals(personInfo.getPhone()))
		{
				vendorInfo.clear();
		}
		
		if(event.getSource().equals(vendorInfo.getId()))
		{
				personInfo.clear();
		}
		if(event.getSource().equals(vendorInfo.getName()))
		{
				personInfo.clear();
		}
		if(event.getSource().equals(vendorInfo.getPhone()))
		{
				personInfo.clear();
		}
	}

	@Override
	public boolean validate() {
		Console.log("in validate of billingDetailsSearchpopup");
//		if(LoginPresenter.branchRestrictionFlag)
//		{
//		if(olbBranch.getSelectedIndex()==0)
//		{
//			showDialogMessage("Select Branch");
//			return false;
//		}
//		}
		/**
		 * Date 25-01-2018 By Vijay 
		 * for Date combination validation
		 */
		String msg = "Wrong filter selection with From Date and To Date ! Please select proper filters with Date";
		if(dateComparator.getFromDate().getValue()!=null || dateComparator.getToDate().getValue()!=null ){
			if(olbbillingcategory.getSelectedIndex()!=0 || olbbillingtype.getSelectedIndex()!=0 ||
			   olbbillinggroup.getSelectedIndex()!=0 || ibbillingid.getValue()!=null || ibcontractid.getValue()!=null ||	
			   lbaccountType.getSelectedIndex()!=0 || personInfo.getIdValue()!=-1 ||!(personInfo.getFullNameValue().equals(""))  || 
			   personInfo.getCellValue()!=-1l || vendorInfo.getIdValue()!=-1 || !(vendorInfo.getFullNameValue().equals("")) ||
			   vendorInfo.getCellValue()!=-1l || olbApproverName.getSelectedIndex()!=0 || 
			   ibrateContractServiceId.getValue()!=null || olbCustomerCategory.getSelectedIndex()!=0 ||
			   !tbSegment.getValue().equals("") || olbcNumberRange.getSelectedIndex()!=0 || tbReferenceNumber.getValue()!=null&&!tbReferenceNumber.getValue().equals("") ){
				showDialogMessage(msg);
				return false;
			}
		}
		/**
		 * ends here
		 */
		boolean filterAppliedFlag=false;
		if((dateComparator.getFromDate().getValue()!=null && dateComparator.getToDate().getValue()!=null)||olbBranch.getSelectedIndex()!=0||lbstatus.getSelectedIndex()!=0||project.getSelectedIndex()!=0|| olbbillingcategory.getSelectedIndex()!=0 || olbbillingtype.getSelectedIndex()!=0 ||
				   olbbillinggroup.getSelectedIndex()!=0 || ibbillingid.getValue()!=null || ibcontractid.getValue()!=null ||	
				   lbaccountType.getSelectedIndex()!=0 || personInfo.getIdValue()!=-1 ||!(personInfo.getFullNameValue().equals(""))  || 
				   personInfo.getCellValue()!=-1l || vendorInfo.getIdValue()!=-1 || !(vendorInfo.getFullNameValue().equals("")) ||
				   vendorInfo.getCellValue()!=-1l || olbApproverName.getSelectedIndex()!=0 || 
				   ibrateContractServiceId.getValue()!=null || olbCustomerCategory.getSelectedIndex()!=0 ||
				   !tbSegment.getValue().equals("") || olbcNumberRange.getSelectedIndex()!=0 || tbReferenceNumber.getValue()!=null&&!tbReferenceNumber.getValue().equals("") ){
					filterAppliedFlag=true;
				}
		
		if(!filterAppliedFlag){
			showDialogMessage("Please apply at least one filter!");
			return false;
		}
		
		
		//Ashwini Patil Date:27-09-2023 
		if((dateComparator.getFromDate().getValue()!=null && dateComparator.getToDate().getValue()!=null)){
			int diffdays = AppUtility.getDifferenceDays(dateComparator.getFromDate().getValue(), dateComparator.getToDate().getValue());
			Console.log("diffdays "+diffdays);
			if(diffdays>31){
				showDialogMessage("Please select from date and to date range within 30 days");
				return false;
			}
		}
				
		return true;
	}
	


}
