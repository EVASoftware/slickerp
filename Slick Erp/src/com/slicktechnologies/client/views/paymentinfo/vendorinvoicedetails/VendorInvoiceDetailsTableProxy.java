package com.slicktechnologies.client.views.paymentinfo.vendorinvoicedetails;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.salesprocess.VendorInvoice;

public class VendorInvoiceDetailsTableProxy extends SuperTable<VendorInvoice>{

	
	TextColumn<VendorInvoice> invoiceIdColumn;
	TextColumn<VendorInvoice> invoiceDateColumn;
	TextColumn<VendorInvoice> invoiceTypeColumn;
	TextColumn<VendorInvoice> customerIdColumn;
	TextColumn<VendorInvoice> customerNameColumn;
	TextColumn<VendorInvoice> customerCellColumn;
	TextColumn<VendorInvoice> contractIdColumn;
	TextColumn<VendorInvoice> salesAmtColumn;
	TextColumn<VendorInvoice> invoiceAmtColumn;
	TextColumn<VendorInvoice> approverNameColumn;
	TextColumn<VendorInvoice> accountTypeColumn;
	TextColumn<VendorInvoice> statusColumn;
	TextColumn<VendorInvoice> branchColumn;
	
	/*
	 *  nidhi
	 *   1-07-2017
	 */
	TextColumn<VendorInvoice> segmentColumn;
	/*
	 *  end
	 */
	
	/*
	 *  nidhi
	 *   17-07-2017 
	 *   display service Id 
	 */
	TextColumn<VendorInvoice> serviceIdColumn;
	/*
	 *  end
	 */
	/** 03-10-2017 sagar sore [adding Number range column in VendorInvoice list of search pop up]**/
	TextColumn<VendorInvoice> numberRangeColumn;
	
	/**
	 * Date : 18-11-2017 BY ANIL
	 */
	TextColumn<VendorInvoice> getColRefNum;
	/** date 13.03.2018 added by komal for nbhc **/
	TextColumn<VendorInvoice> getColumnInvoiceGroup;
	TextColumn<VendorInvoice> getColumnInvoiceCategory;
	TextColumn<VendorInvoice> getColumnInvoiceConfigType;
	/**
	 * end komal
	 */
	
	public VendorInvoiceDetailsTableProxy() {
		super();
	}

	@Override
	public void createTable() {
		/**03-10-2017 sagar sore [To add number range column in search pop up result]**/
		addColumnInvoiceId();
		addColumnInvoiceDate();
		addColumnInvoiceType();
		addColumnNumberRange();
		addColumnCustomerId();
		addColumnCustomerName();
		addColumnCustomerPhone();
		addColumnAccountType();
		addColumnContractId();
		addColumnSalesAmount();
		addColumnInvoiceAmount();
		addColumnApproverName();
		addColumnInvoiceStatus();
		addColumnInvoiceBranch();
		
		/*
		 *  nidhi
		 */
		addColumnServiceId();
		/*
		 *  end
		 */
		
		/*
		 *  nidhi
		 */
		addColumnInvoiceSegment();
		/*
		 *  end
		 */
		getColRefNum();
		/** date 13.03.2018 added by komal for nbhc **/
		addColumnInvoiceGroup();
		addColumnInvoiceCategory();
		addColumnInvoiceConfigType();
		/**
		 * end komal
		 */
		
		
	}
	
	private void getColRefNum() {
		getColRefNum = new TextColumn<VendorInvoice>() {
			
			@Override
			public String getValue(VendorInvoice object) {
				if(object.getRefNumber()!= null)
					return object.getRefNumber() +"";
				else 
					return "";
			}
		};
		table.addColumn(getColRefNum,"Reference Number");
		table.setColumnWidth(getColRefNum, 100, Unit.PX);
		getColRefNum.setSortable(true);
	}
	
	private void addSortingOnRefNum() {
		List<VendorInvoice> list = getDataprovider().getList();
		columnSort = new ListHandler<VendorInvoice>(list);
		columnSort.setComparator(getColRefNum,new Comparator<VendorInvoice>() {
			@Override
			public int compare(VendorInvoice e1, VendorInvoice e2) {
				if (e1 != null && e2 != null) {
					if (e1.getRefNumber() != null&& e2.getRefNumber() != null) {
						return e1.getRefNumber().compareTo(e2.getRefNumber());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	public void addColumnServiceId()
	{
		serviceIdColumn =new TextColumn<VendorInvoice>() {

			@Override
			public String getValue(VendorInvoice object) {
				return object.getRateContractServiceId()+"";
			}
		};
		
		table.addColumn(serviceIdColumn,"Service Id");
		table.setColumnWidth(serviceIdColumn, 100, Unit.PX);
		serviceIdColumn.setSortable(true);
	}	
	
	public void addColumnInvoiceSegment()
	{
		segmentColumn=new TextColumn<VendorInvoice>() {

			@Override
			public String getValue(VendorInvoice object) {
				return object.getSegment();
			}
		};
		
		table.addColumn(segmentColumn,"Segment");
		table.setColumnWidth(segmentColumn, 100, Unit.PX);
		segmentColumn.setSortable(true);
	}	
	
	public void addColumnInvoiceId()
	{
		invoiceIdColumn=new TextColumn<VendorInvoice>() {
		@Override
		public String getCellStyleNames(Context context, VendorInvoice object) {
			
			if((object.getStatus().equals("Invoiced"))){
				 return "blue";
			}
			else if((object.getStatus().equals("Cancelled"))){
				 return "red";
			 }
			else 
			{
				return "black";
			}
		}

			@Override
			public String getValue(VendorInvoice object) {
				return object.getCount()+"";
			}
		};
		table.addColumn(invoiceIdColumn,"VendorInvoice ID");
		table.setColumnWidth(invoiceIdColumn, 100, Unit.PX);
		invoiceIdColumn.setSortable(true);
	}

	public void addColumnInvoiceDate()
	{
		invoiceDateColumn=new TextColumn<VendorInvoice>() {

			@Override
			public String getValue(VendorInvoice object) {
				if(object.getInvoiceDate()!=null)
				{
				    String serDate=AppUtility.parseDate(object.getInvoiceDate());
				    return serDate;
				}
				return "Not Set";
			}
		};
		
		table.addColumn(invoiceDateColumn,"VendorInvoice Date");
		table.setColumnWidth(invoiceDateColumn, 120, Unit.PX);
		invoiceDateColumn.setSortable(true);
	}

	public void addColumnInvoiceType()
	{
		invoiceTypeColumn=new TextColumn<VendorInvoice>() {

			@Override
			public String getValue(VendorInvoice object) {
				return object.getInvoiceType();
			}
		};
		
		table.addColumn(invoiceTypeColumn,"VendorInvoice Document Type");
		table.setColumnWidth(invoiceTypeColumn, 140, Unit.PX);
		invoiceTypeColumn.setSortable(true);
	}
	
	private void addColumnCustomerId() {
		customerIdColumn=new TextColumn<VendorInvoice>() {

			@Override
			public String getValue(VendorInvoice object) {
				return object.getCustomerCount()+"";
			}
		};
		
		table.addColumn(customerIdColumn,"BP ID");
		table.setColumnWidth(customerIdColumn, 120, Unit.PX);
		customerIdColumn.setSortable(true);
		
	}
	
	public void addColumnCustomerName()
	{
		customerNameColumn=new TextColumn<VendorInvoice>() {

			@Override
			public String getValue(VendorInvoice object) {
				return object.getName();
			}
		};
		
		table.addColumn(customerNameColumn,"BP Name");
		table.setColumnWidth(customerNameColumn, 120, Unit.PX);
		customerNameColumn.setSortable(true);
	}
	
	public void addColumnCustomerPhone()
	{
		customerCellColumn=new TextColumn<VendorInvoice>() {

			@Override
			public String getValue(VendorInvoice object) {
				return object.getCellNumber()+"";
			}
		};
		
		table.addColumn(customerCellColumn,"Cell Number");
		table.setColumnWidth(customerCellColumn, 120, Unit.PX);
		customerCellColumn.setSortable(true);
	}
	
	
	public void addColumnAccountType()
	{
		accountTypeColumn=new TextColumn<VendorInvoice>() {

			@Override
			public String getValue(VendorInvoice object) {
				return object.getAccountType();
			}
		};
		
		table.addColumn(accountTypeColumn,"Acc. Type");
		table.setColumnWidth(accountTypeColumn, 90, Unit.PX);
		accountTypeColumn.setSortable(true);
	}
	
	
	private void addColumnContractId() {
		contractIdColumn=new TextColumn<VendorInvoice>() {

			@Override
			public String getValue(VendorInvoice object) {
				return object.getContractCount()+"";
			}
		};
		
		table.addColumn(contractIdColumn,"Order ID");
		table.setColumnWidth(contractIdColumn, 100, Unit.PX);
		contractIdColumn.setSortable(true);
		
	}

	public void addColumnSalesAmount()
	{
		salesAmtColumn=new TextColumn<VendorInvoice>() {

			@Override
			public String getValue(VendorInvoice object) {
				return object.getTotalSalesAmount()+"";
			}
		};
		
		table.addColumn(salesAmtColumn,"Sales Amt");
		table.setColumnWidth(salesAmtColumn, 110, Unit.PX);
		salesAmtColumn.setSortable(true);
	}
	
	public void addColumnInvoiceAmount()
	{
		invoiceAmtColumn=new TextColumn<VendorInvoice>() {

			@Override
			public String getValue(VendorInvoice object) {
				return object.getInvoiceAmount()+"";
			}
		};
		
		table.addColumn(invoiceAmtColumn,"VendorInvoice Amt");
		table.setColumnWidth(invoiceAmtColumn, 110, Unit.PX);
		invoiceAmtColumn.setSortable(true);
	}
	
	public void addColumnApproverName()
	{
		approverNameColumn=new TextColumn<VendorInvoice>() {

			@Override
			public String getValue(VendorInvoice object) {
				return object.getApproverName();
			}
		};
		
		table.addColumn(approverNameColumn,"Approver Name");
		table.setColumnWidth(approverNameColumn, 120, Unit.PX);
		approverNameColumn.setSortable(true);
	}	
		
	public void addColumnInvoiceStatus()
	{
		statusColumn=new TextColumn<VendorInvoice>() {

			@Override
			public String getValue(VendorInvoice object) {
				return object.getStatus();
			}
		};
		
		table.addColumn(statusColumn,"Status");
		table.setColumnWidth(statusColumn, 100, Unit.PX);
		statusColumn.setSortable(true);
	}	
	
	public void addColumnInvoiceBranch()
	{
		branchColumn=new TextColumn<VendorInvoice>() {

			@Override
			public String getValue(VendorInvoice object) {
				return object.getBranch();
			}
		};
		
		table.addColumn(branchColumn,"Branch");
		table.setColumnWidth(branchColumn, 100, Unit.PX);
		branchColumn.setSortable(true);
	}	
	
	
	
	public void addColumnSorting()
	{
		addSortinggetInvoiceCount();
		addSortinggetInvoiceDate();
		addSortinggetInvoiceType();
		addSortinggetCustomerId();
		addSortinggetCustomerFullName();
		addSortinggetCustomerCellNumber();
		addSortingAccountType();
		addSortinggetContractCount();
		addSortinggetSalesAmount();
		addSortinggetInvoiceAmount();
		addSortinggetApproverName();
		addSortinggetStatus();
		addSortinggetBranch();
		/*
		 *  nidhi
		 */
		addSortinggetSegment();
		/*
		 * end
		 */
		addSortinggetServiceId();
		/**03-10-2017 sagar sore [adding number range column]**/
		addSortinggetNumberRange();
		addSortingOnRefNum();
		/** date 13.03.2018 added by komal for VendorInvoice category and type**/
		addSortinggetInvoiceGroup();
		addSortinggetInvoiceCategory();
		addSortinggetInvoiceConfigType();
		/**
		 * end komal
		 */
	}
	
	protected void addSortinggetServiceId()
	{
		List<VendorInvoice> list=getDataprovider().getList();
		columnSort=new ListHandler<VendorInvoice>(list);
		columnSort.setComparator(serviceIdColumn, new Comparator<VendorInvoice>()
				{
			@Override
			public int compare(VendorInvoice e1,VendorInvoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getRateContractServiceId()== e2.getRateContractServiceId()){
						return 0;}
					if(e1.getRateContractServiceId()> e2.getRateContractServiceId()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addSortinggetSegment()
	{
		List<VendorInvoice> list=getDataprovider().getList();
		columnSort=new ListHandler<VendorInvoice>(list);
		columnSort.setComparator(segmentColumn, new Comparator<VendorInvoice>()
				{
			@Override
			public int compare(VendorInvoice e1,VendorInvoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getSegment()!=null && e2.getSegment()!=null){
						return e1.getSegment().compareTo(e2.getSegment());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}

	
	protected void addSortinggetInvoiceCount()
	{
		List<VendorInvoice> list=getDataprovider().getList();
		columnSort=new ListHandler<VendorInvoice>(list);
		columnSort.setComparator(invoiceIdColumn, new Comparator<VendorInvoice>()
				{
			@Override
			public int compare(VendorInvoice e1,VendorInvoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetInvoiceDate()
	{
		List<VendorInvoice> list=getDataprovider().getList();
		columnSort=new ListHandler<VendorInvoice>(list);
		columnSort.setComparator(invoiceDateColumn, new Comparator<VendorInvoice>()
				{
			@Override
			public int compare(VendorInvoice e1,VendorInvoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getInvoiceDate()!=null && e2.getInvoiceDate()!=null){
						return e1.getInvoiceDate().compareTo(e2.getInvoiceDate());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortingAccountType()
	{
		List<VendorInvoice> list=getDataprovider().getList();
		columnSort=new ListHandler<VendorInvoice>(list);
		columnSort.setComparator(accountTypeColumn, new Comparator<VendorInvoice>()
				{
			@Override
			public int compare(VendorInvoice e1,VendorInvoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getAccountType()!=null && e2.getAccountType()!=null){
						return e1.getAccountType().compareTo(e2.getAccountType());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetInvoiceType()
	{
		List<VendorInvoice> list=getDataprovider().getList();
		columnSort=new ListHandler<VendorInvoice>(list);
		columnSort.setComparator(invoiceTypeColumn, new Comparator<VendorInvoice>()
				{
			@Override
			public int compare(VendorInvoice e1,VendorInvoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getInvoiceType()!=null && e2.getInvoiceType()!=null){
						return e1.getInvoiceType().compareTo(e2.getInvoiceType());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetCustomerId()
	{
		List<VendorInvoice> list=getDataprovider().getList();
		columnSort=new ListHandler<VendorInvoice>(list);
		columnSort.setComparator(customerIdColumn, new Comparator<VendorInvoice>()
				{
			@Override
			public int compare(VendorInvoice e1,VendorInvoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getPersonInfo().getCount()== e2.getPersonInfo().getCount()){
						return 0;}
					if(e1.getPersonInfo().getCount()> e2.getPersonInfo().getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetCustomerFullName()
	{
		List<VendorInvoice> list=getDataprovider().getList();
		columnSort=new ListHandler<VendorInvoice>(list);
		columnSort.setComparator(customerNameColumn, new Comparator<VendorInvoice>()
				{
			@Override
			public int compare(VendorInvoice e1,VendorInvoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getPersonInfo().getFullName()!=null && e2.getPersonInfo().getFullName()!=null){
						return e1.getPersonInfo().getFullName().compareTo(e2.getPersonInfo().getFullName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addSortinggetCustomerCellNumber()
	{
		List<VendorInvoice> list=getDataprovider().getList();
		columnSort=new ListHandler<VendorInvoice>(list);
		columnSort.setComparator(customerCellColumn, new Comparator<VendorInvoice>()
				{
			@Override
			public int compare(VendorInvoice e1,VendorInvoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getPersonInfo().getCellNumber()== e2.getPersonInfo().getCellNumber()){
						return 0;}
					if(e1.getPersonInfo().getCellNumber()> e2.getPersonInfo().getCellNumber()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetContractCount()
	{
		List<VendorInvoice> list=getDataprovider().getList();
		columnSort=new ListHandler<VendorInvoice>(list);
		columnSort.setComparator(contractIdColumn, new Comparator<VendorInvoice>()
				{
			@Override
			public int compare(VendorInvoice e1,VendorInvoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getContractCount()== e2.getContractCount()){
						return 0;}
					if(e1.getContractCount()> e2.getContractCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetSalesAmount()
	{
		List<VendorInvoice> list=getDataprovider().getList();
		columnSort=new ListHandler<VendorInvoice>(list);
		columnSort.setComparator(salesAmtColumn, new Comparator<VendorInvoice>()
				{
			@Override
			public int compare(VendorInvoice e1,VendorInvoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getTotalSalesAmount()== e2.getTotalSalesAmount()){
						return 0;}
					if(e1.getTotalSalesAmount()> e2.getTotalSalesAmount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetInvoiceAmount()
	{
		List<VendorInvoice> list=getDataprovider().getList();
		columnSort=new ListHandler<VendorInvoice>(list);
		columnSort.setComparator(invoiceAmtColumn, new Comparator<VendorInvoice>()
				{
			@Override
			public int compare(VendorInvoice e1,VendorInvoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getInvoiceAmount()== e2.getInvoiceAmount()){
						return 0;}
					if(e1.getInvoiceAmount()> e2.getInvoiceAmount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetApproverName()
	{
		List<VendorInvoice> list=getDataprovider().getList();
		columnSort=new ListHandler<VendorInvoice>(list);
		columnSort.setComparator(approverNameColumn, new Comparator<VendorInvoice>()
				{
			@Override
			public int compare(VendorInvoice e1,VendorInvoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getApproverName()!=null && e2.getApproverName()!=null){
						return e1.getApproverName().compareTo(e2.getApproverName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetStatus()
	{
		List<VendorInvoice> list=getDataprovider().getList();
		columnSort=new ListHandler<VendorInvoice>(list);
		columnSort.setComparator(statusColumn, new Comparator<VendorInvoice>()
				{
			@Override
			public int compare(VendorInvoice e1,VendorInvoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getStatus()!=null && e2.getStatus()!=null){
						return e1.getStatus().compareTo(e2.getStatus());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	
	protected void addSortinggetBranch()
	{
		List<VendorInvoice> list=getDataprovider().getList();
		columnSort=new ListHandler<VendorInvoice>(list);
		columnSort.setComparator(branchColumn, new Comparator<VendorInvoice>()
				{
			@Override
			public int compare(VendorInvoice e1,VendorInvoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBranch()!=null && e2.getBranch()!=null){
						return e1.getBranch().compareTo(e2.getBranch());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	/**03-10-2017 sagar sore [adding number range column]**/
	public void addColumnNumberRange() {
		numberRangeColumn = new TextColumn<VendorInvoice>() {
			@Override
			public String getValue(VendorInvoice object) {
				return object.getNumberRange();
			}
		};
		table.addColumn(numberRangeColumn, "Number Range");
		table.setColumnWidth(numberRangeColumn, 120, Unit.PX);
		numberRangeColumn.setSortable(true);
	}

	protected void addSortinggetNumberRange() {
		List<VendorInvoice> list = getDataprovider().getList();
		columnSort = new ListHandler<VendorInvoice>(list);
		columnSort.setComparator(numberRangeColumn, new Comparator<VendorInvoice>() {
			@Override
			public int compare(VendorInvoice e1, VendorInvoice e2) {
				if (e1 != null && e2 != null) {
					
					if (e1.getNumberRange() == null && e2.getNumberRange() == null){
						return 0;
					}
					if(e1.getNumberRange() == null) return 1;							
					if(e2.getNumberRange() == null) return -1;							
					return e1.getNumberRange().compareTo(e2.getNumberRange());

				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	/**end**/
	/** date 13.03.2018 added by komal for nbhc **/
	private void addColumnInvoiceCategory(){
		getColumnInvoiceCategory =new TextColumn<VendorInvoice>() {

			@Override
			public String getValue(VendorInvoice object) {
				return object.getInvoiceCategory()+"";
			}
		};
		
		table.addColumn(getColumnInvoiceCategory,"VendorInvoice Category");
		table.setColumnWidth(getColumnInvoiceCategory, 100, Unit.PX);
		getColumnInvoiceCategory.setSortable(true);

	}
	private void addColumnInvoiceConfigType(){
		getColumnInvoiceConfigType =new TextColumn<VendorInvoice>() {

			@Override
			public String getValue(VendorInvoice object) {
				return object.getInvoiceConfigType()+"";
			}
		};
		
		table.addColumn(getColumnInvoiceConfigType,"VendorInvoice Config Type");
		table.setColumnWidth(getColumnInvoiceConfigType, 100, Unit.PX);
		serviceIdColumn.setSortable(true);

	}
	protected void addSortinggetInvoiceCategory()
	{
		List<VendorInvoice> list=getDataprovider().getList();
		columnSort=new ListHandler<VendorInvoice>(list);
		columnSort.setComparator(getColumnInvoiceCategory, new Comparator<VendorInvoice>()
				{
			@Override
			public int compare(VendorInvoice e1,VendorInvoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getInvoiceCategory()!=null && e2.getInvoiceCategory()!=null){
						return e1.getInvoiceCategory().compareTo(e2.getInvoiceCategory());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addSortinggetInvoiceConfigType()
	{
		List<VendorInvoice> list=getDataprovider().getList();
		columnSort=new ListHandler<VendorInvoice>(list);
		columnSort.setComparator(getColumnInvoiceConfigType, new Comparator<VendorInvoice>()
				{
			@Override
			public int compare(VendorInvoice e1,VendorInvoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getInvoiceConfigType()!=null && e2.getInvoiceConfigType()!=null){
						return e1.getInvoiceConfigType().compareTo(e2.getInvoiceConfigType());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnInvoiceGroup(){
		getColumnInvoiceGroup =new TextColumn<VendorInvoice>() {

			@Override
			public String getValue(VendorInvoice object) {
				return object.getInvoiceGroup()+"";
			}
		};
		
		table.addColumn(getColumnInvoiceGroup,"VendorInvoice Group");
		table.setColumnWidth(getColumnInvoiceGroup, 100, Unit.PX);
		getColumnInvoiceGroup.setSortable(true);

	}
	protected void addSortinggetInvoiceGroup()
	{
		List<VendorInvoice> list=getDataprovider().getList();
		columnSort=new ListHandler<VendorInvoice>(list);
		columnSort.setComparator(getColumnInvoiceGroup, new Comparator<VendorInvoice>()
				{
			@Override
			public int compare(VendorInvoice e1,VendorInvoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getInvoiceGroup()!=null && e2.getInvoiceGroup()!=null){
						return e1.getInvoiceGroup().compareTo(e2.getInvoiceGroup());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}

	/**
	 * end komal
	 */


	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
	


}
