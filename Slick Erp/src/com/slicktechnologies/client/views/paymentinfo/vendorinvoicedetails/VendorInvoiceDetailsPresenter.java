package com.slicktechnologies.client.views.paymentinfo.vendorinvoicedetails;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.approvalutility.ApprovableFormScreenPresenter;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.reusabledata.ConditionDialogBox;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.EmailService;
import com.slicktechnologies.client.services.EmailServiceAsync;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.ApproverFactory;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.documentcancellation.DocumentCancellationPopUp;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.BillingDetailsForm;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.BillingDetailsPresenter;
import com.slicktechnologies.client.views.paymentinfo.billinglist.BillingListPresenter;
import com.slicktechnologies.client.views.paymentinfo.invoicedetails.InvoiceDetailsForm;
import com.slicktechnologies.client.views.paymentinfo.invoicedetails.InvoiceDetailsPresenter;
import com.slicktechnologies.client.views.paymentinfo.invoicedetails.InvoiceDetailsSearchPopUp;
import com.slicktechnologies.client.views.paymentinfo.invoicedetails.InvoiceDetailsTableProxy;
import com.slicktechnologies.client.views.paymentinfo.invoicedetails.ReferenceDocDetailsPopUp;
import com.slicktechnologies.client.views.paymentinfo.invoicedetails.InvoiceDetailsPresenter.InvoiceDetailsPresenterSearch;
import com.slicktechnologies.client.views.paymentinfo.invoicelist.InvoiceListPresenter;
import com.slicktechnologies.client.views.paymentinfo.paymentdetails.PaymentDetailsForm;
import com.slicktechnologies.client.views.paymentinfo.paymentdetails.PaymentDetailsPresenter;
import com.slicktechnologies.client.views.paymentinfo.paymentlist.PaymentListPresenter;
import com.slicktechnologies.client.views.paymentinfo.vendorinvoicelist.VendorInvoiceListPresenter;
import com.slicktechnologies.client.views.popups.NewEmailPopUp;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.OtherTaxCharges;
import com.slicktechnologies.shared.common.businessprocesslayer.ConcreteBusinessProcess;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.VendorDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.contactidentification.ContactPersonIdentification;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.salesorder.OtherCharges;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.BillingDocumentDetails;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.SalesOrderProductLineItem;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;
import com.slicktechnologies.shared.common.salesprocess.VendorInvoice;
import com.slicktechnologies.shared.common.tallyaccounts.AccountingInterface;

public class VendorInvoiceDetailsPresenter extends ApprovableFormScreenPresenter<VendorInvoice> implements RowCountChangeEvent.Handler, ChangeHandler{
	
	public static VendorInvoiceDetailsForm form;
	final static GenricServiceAsync async=GWT.create(GenricService.class);
	final CsvServiceAsync csvservice=GWT.create(CsvService.class);
	EmailServiceAsync emailService=GWT.create(EmailService.class);
	
	DocumentCancellationPopUp popup = new DocumentCancellationPopUp();
	PopupPanel panel ;
	int cnt=0;
	int pestoIndia=0;
	int vCare=0;
	int omPestInvoice=0;
	int reddysPestControl=0;
	int counter=0;
	int hygeia=0;
	int pecopp = 0;
	int orionPestControl=0;
	//************changes changes here ****************
		ConditionDialogBox conditionPopup=new ConditionDialogBox("Do you want to print on preprinted Stationery",AppConstants.YES,AppConstants.NO);
		
		/*
		 * Added by Rahul
		 */
		ReferenceDocDetailsPopUp  refDocPopUp=new ReferenceDocDetailsPopUp();
		PopupPanel refDocPanel;	
	//****************ends here ***********	
	
	 	NewEmailPopUp emailpopup = new NewEmailPopUp();

	public VendorInvoiceDetailsPresenter(FormScreen<VendorInvoice> view, VendorInvoice model) {
		super(view, model);
		form=(VendorInvoiceDetailsForm) view;
		form.setPresenter(this);
		popup.getBtnOk().addClickHandler(this);
		popup.getBtnCancel().addClickHandler(this);
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.VENDORINVOICEDETAILS,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
		
		
		
		//**********rohan ************************
				conditionPopup.getBtnOne().addClickHandler(this);
				conditionPopup.getBtnTwo().addClickHandler(this);
				//Date 04-09-2017 commented by vijay handled in form with new code amt roundoff discount
//				form.getDiscount().addValueChangeHandler(this);
				
				form.getSalesProductTable().getTable().addRowCountChangeHandler(this);
				form.getInvoiceChargesTable().getTable().addRowCountChangeHandler(this);
				
				form.btReferenceDetails.addClickHandler(this);
				refDocPopUp.getBtnOk().addClickHandler(this);
				form.getObjPaymentMode().addChangeHandler(this);
		/**
		 * Date : 21-09-2017 BY ANIL
		 * Adding row count change handler on other charges table
		 */
		form.tblOtherCharges.getTable().addRowCountChangeHandler(this);
		/**
		 * End
		 */
		/** date 24.10.2018 added by komal for tds **/
		form.cbtdsApplicable.addClickHandler(this);
		form.getDbtdsValue().addChangeHandler(this);
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel label=(InlineLabel) e.getSource();
		
		String text=label.getText().trim();
		if(text.equals(ManageApprovals.SUBMIT)){
			/**
			 * Date : 09-11-2017 By ANIL
			 */
			/**
			 *  nidhi
			 *  03-04-2018
			 *  for check unit and uom validation
			 */
			boolean unitFlag = form.validateunitandUOM();
			if(isRefNumIsMandatory()==false || unitFlag == false){
				return;
			}
			
			/**
			 * Date 12-10-2018 
			 * Developer : Vijay
			 * Des : if multiple billing mergred into billing document and its billing documents branch is diffrent then branch will blank
			 * then must add branch in invoice so before self approve branch validation 
			 */
			if(form.getOlbbranch().getSelectedIndex()==0){
				form.showDialogMessage("Please add branch!");
				return;
			}
			/**
			 * ends here
			 */
			
			form.getManageapproval().reactToSubmit();
		}
		
		if(text.equals(ManageApprovals.APPROVALREQUEST)){
			/**
			 * Date : 09-11-2017 By ANIL
			 */
			/**
			 *  nidhi
			 *  03-04-2018
			 *  for check unit and uom validation
			 */
			boolean unitFlag = form.validateunitandUOM();
			if(isRefNumIsMandatory()==false || unitFlag == false){
				return;
			}
			
			/**
			 * Date 12-10-2018 
			 * Developer : Vijay
			 * Des : if multiple billing mergred into billing document and its billing documents branch is diffrent then branch will blank
			 * then must add branch in invoice so before self approve branch validation 
			 */
			if(form.getOlbbranch().getSelectedIndex()==0){
				form.showDialogMessage("Please add branch!");
				return;
			}
			/**
			 * ends here
			 */
			
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "OnlyForNBHC"))
			{
				MyQuerry querry = AppUtility.checkRefNUmberInCustomerByCustomerId(model.getCustomerId(),model.getCompanyId());

				service.getSearchResult(querry,	new AsyncCallback<ArrayList<SuperModel>>() {
					
					@Override
					public void onFailure(Throwable caught) {
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						String refNumber="";
						if (result.size() == 0) {

						}
						else {

							for (SuperModel model : result) {
								Customer customer = (Customer) model;
								 refNumber=customer.getRefrNumber1();
							}
						}
						
						if(refNumber.equals("")){
							form.showDialogMessage("Please fill Customer reference number 1 in customer screen..!");
						}
						else{
							reactOnApprovalVal();
						}
					}
				});
				}
			else{
				reactOnApprovalVal();
			}
		}
		if(text.equals(ManageApprovals.CANCELAPPROVALREQUEST)){
			reactOnCancelApproval();
		}
		if(text.equals("Email")){
			reactOnEmail();
		}
		
		if(text.equals("Tax Invoice")){
			createNewTaxInvoice();
			changeBillingStatusToTInvoiced();
		}
		
		if(text.equals(AppConstants.CANCELINVOICE)){
			popup.getPaymentDate().setValue(new Date()+"");
			popup.getPaymentID().setValue(form.getIbinvoiceid().getValue());
			popup.getPaymentStatus().setValue(form.getTbstatus().getValue());
			popup.getRemark().setValue("");
			panel=new PopupPanel(true);
			panel.add(popup);
			panel.show();
			panel.center();
		}
		
		/**
		 * Date : 29-07-2017 By ANIL
		 * 
		 */
		if (text.equals(AppConstants.VIEWPAYMENT)) {
			viewPaymentDocuments();
		}
		
		/**
		 * Date : 10-10-2017 By ANIL
		 * 
		 */
		if (text.equals(AppConstants.VIEWBILL)) {
			viewBillingDocuments();
		}
		
		/**
		 * Date 22/2/2018 
		 * By Jayshree 
		 * Des.Add New Button View tax invoice Button
		 */
		
		if(text.equals(AppConstants.VIEWTAXINVOICE)){
			viewTaxInvoiceDocuments();
		}
		//End by jayshree
		/** Date 13.12.2018 added by komal for edit accounting interface **/
		if(text.equalsIgnoreCase(AppConstants.UPDATEACCOUNTINGINTERFACE)){
			updateAccountingInterface();
		}
	}
	
	
	
	private void viewBillingDocuments() {
		final MyQuerry querry=new MyQuerry();
		Vector<Filter> temp=new Vector<Filter>();
		Filter filter=null;
		
		filter=new Filter();
		filter.setQuerryString("invoiceCount");
		filter.setIntValue(model.getCount());
		temp.add(filter);
		
		/**Date 30-1-2020 by Amol added a Type Of Order Querry**/
		filter=new Filter();
		filter.setQuerryString("typeOfOrder");
		filter.setStringValue(AppConstants.ORDERTYPEFORPURCHASE);
		temp.add(filter);
		
		
		
		
		querry.setFilters(temp);
		querry.setQuerryObject(new BillingDocument());
		form.showWaitSymbol();
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				form.hideWaitSymbol();
				if(result.size()==0){
					form.showDialogMessage("No billing document found.");
					return;
				}
				if(result.size()==1){
					final BillingDocument billDocument=(BillingDocument) result.get(0);
					final BillingDetailsForm form=BillingDetailsPresenter.initalize();
					Timer timer=new Timer() {
						@Override
						public void run() {
							form.updateView(billDocument);
							form.setToViewState();
						}
					};
					timer.schedule(1000);
				}else{
					BillingListPresenter.initalize(querry);
				}
				
			}
		});
		
	}

	private void viewPaymentDocuments() {
		final MyQuerry querry=new MyQuerry();
		Vector<Filter> temp=new Vector<Filter>();
		Filter filter=null;
		
		filter=new Filter();
		filter.setQuerryString("invoiceCount");
		filter.setIntValue(model.getCount());
		temp.add(filter);
		
		/**Date 30-1-2020 by Amol added a Type Of Order Querry**/
		filter=new Filter();
		filter.setQuerryString("typeOfOrder");
		/**
		 * @author Anil
		 * @since 04-01-2020
		 */
//		filter.setStringValue(AppConstants.ORDERTYPEFORPURCHASE);
		filter.setStringValue(model.getTypeOfOrder());
		temp.add(filter);
		
		querry.setFilters(temp);
		querry.setQuerryObject(new CustomerPayment());
		form.showWaitSymbol();
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				form.hideWaitSymbol();
				if(result.size()==0){
					form.showDialogMessage("No payment document found.");
					return;
				}
				if(result.size()==1){
					final CustomerPayment billDocument=(CustomerPayment) result.get(0);
					/** date 25.6.2018 changed by koaml **/
					final PaymentDetailsForm form=PaymentDetailsPresenter.initalize(true);
					Timer timer=new Timer() {
						@Override
						public void run() {
							form.updateView(billDocument);
							form.setToViewState();
						}
					};
					timer.schedule(1000);
				}else{
					PaymentListPresenter.initalize(querry,true);
				}
				
			}
		});
		
	}

	private void changeBillingStatusToTInvoiced() {
		
		model.setStatus(BillingDocument.BILLINGINVOICED);
		
		async.save(model,new AsyncCallback<ReturnFromServer>() {
			@Override
			public void onFailure(Throwable caught) {
			}
			@Override
			public void onSuccess(ReturnFromServer result) {
				form.tbstatus.setText(model.getStatus());
				form.setAppHeaderBarAsPerStatus();
			}
		});
	}

	private void createNewTaxInvoice(){
		
		System.out.println("Inside tax invoice method ");
		
			form.showWaitSymbol();
			final VendorInvoice inventity = new VendorInvoice();
			PersonInfo pinfo = new PersonInfo();
			if (form.getTbpersoncount().getValue() != null) {
				pinfo.setCount(Integer.parseInt(form.getTbpersoncount().getValue()));
			}
			if (form.getTbpersonname().getValue() != null) {
				pinfo.setFullName(form.getTbpersonname().getValue());
			}
			if (form.getTbpersoncell().getValue() != null) {
				pinfo.setCellNumber(Long.parseLong(form.getTbpersoncell().getValue()));
			}
			if (form.getTbpocname().getValue() != null) {
				pinfo.setPocName(form.getTbpocname().getValue());
			}

			// ************************rohan made changes here for setting
			// fields()**************

			if (model.getGrossValue() != 0) {
				inventity.setGrossValue(model.getGrossValue());
			}

//			if (form.getDopaytermspercent().getValue() != 0) {
//
//				inventity.setTaxPercent(form.getDopaytermspercent().getValue());
//			}

			// ***********************************changes ends here
			// *****************************

			if (pinfo != null){
				inventity.setPersonInfo(pinfo);
			}

			if (!form.getIbcontractid().getValue().equals(""))
				inventity.setContractCount(Integer.parseInt(form.getIbcontractid().getValue()));
			if (model.getContractStartDate() != null) {
				inventity.setContractStartDate(model.getContractStartDate());
			}
			if (model.getContractEndDate() != null) {
				inventity.setContractEndDate(model.getContractEndDate());
			}
			if (form.getDosalesamount().getValue()!=null)
				inventity.setTotalSalesAmount(form.getDosalesamount().getValue());
			
			List<BillingDocumentDetails> billtablelis = form.billingdoctable.getDataprovider().getList();
			ArrayList<BillingDocumentDetails> billtablearr = new ArrayList<BillingDocumentDetails>();
			billtablearr.addAll(billtablelis);
			inventity.setArrayBillingDocument(billtablearr);
			
//			System.out.println("MULTIPLE CONTRAT STATUS ::: "+isMultipleContractBilling());
//			if(isMultipleContractBilling()){
//				inventity.setMultipleOrderBilling(true);
//			}
			
			
			//  rohan added this code for setting disc = 0 and net payable =setTotalBillingAmount
			
			if (form.getDototalbillamt().getValue() != null)
				inventity.setNetPayable(form.getDototalbillamt().getValue());
			
			if (form.getTbroundOffAmt().getValue() !=null && !form.getTbroundOffAmt().getValue().equals(""))
				inventity.setDiscount(Double.parseDouble(form.getTbroundOffAmt().getValue()));
			else{
				inventity.setDiscount(0.0);
			}
			//   ends here 
			
			
			if (form.getDototalbillamt().getValue() != null)
				inventity.setTotalBillingAmount(form.getDototalbillamt().getValue());
			if (form.getDbinvoicedate().getValue() != null)
				inventity.setInvoiceDate(form.getDbinvoicedate().getValue());
			if (form.getDbpaymentdate().getValue() != null)
				inventity.setPaymentDate(form.getDbpaymentdate().getValue());
			if (form.getOlbApproverName().getValue() != null)
				inventity.setApproverName(form.getOlbApproverName().getValue());
			if (form.getOlbEmployee().getValue() != null)
				inventity.setEmployee(form.getOlbEmployee().getValue());
			if (form.getOlbbranch().getValue() != null)
				inventity.setBranch(form.getOlbbranch().getValue());
			
			/*
			 *  nidhi
			 *  1-07-20170
			 */
			
			if (form.getTbSegment().getValue() != null)
				inventity.setSegment(form.getTbSegment().getValue());
			
				inventity.setRateContractServiceId(model.getRateContractServiceId());
			
			/*
			 *  end
			 */
			
			inventity.setOrderCreationDate(model.getOrderCreationDate());
			inventity.setCompanyId(model.getCompanyId());
			inventity.setInvoiceAmount(form.getDototalbillamt().getValue());
			inventity.setInvoiceType("Tax Invoice");
			inventity.setPaymentMethod(model.getPaymentMethod());
			inventity.setAccountType(model.getAccountType());
			inventity.setTypeOfOrder(model.getTypeOfOrder());
			inventity.setStatus(Invoice.CREATED);
				
			
			List<SalesOrderProductLineItem>productlist=form.getSalesProductTable().getDataprovider().getList();
			ArrayList<SalesOrderProductLineItem>prodList=new ArrayList<SalesOrderProductLineItem>();
			prodList.addAll(productlist);
			System.out.println();
			System.out.println("SALES PRODUCT TABLE SIZE ::: "+prodList.size());
			
			/**
			 * Date :22-09-2017 BY ANIL
			 */
			
			List<OtherCharges>OthChargeTbl=form.tblOtherCharges.getDataprovider().getList();
			ArrayList<OtherCharges>ocList=new ArrayList<OtherCharges>();
			ocList.addAll(OthChargeTbl);
			System.out.println("OTHER TAXES TABLE SIZE ::: "+ocList.size());
			
			inventity.setOtherCharges(ocList);
			if(form.dbOtherChargesTotal.getValue()!=null){
				inventity.setTotalOtherCharges(form.dbOtherChargesTotal.getValue());
			}
			
			/**
			 * End
			 */
			
			List<ContractCharges>taxestable=form.getInvoiceTaxTable().getDataprovider().getList();
			ArrayList<ContractCharges>taxesList=new ArrayList<ContractCharges>();
			taxesList.addAll(taxestable);
			System.out.println("TAXES TABLE SIZE ::: "+taxesList.size());
			
			List<ContractCharges>otherChargesable=form.getInvoiceChargesTable().getDataprovider().getList();
			ArrayList<ContractCharges>otherchargesList=new ArrayList<ContractCharges>();
			otherchargesList.addAll(otherChargesable);
			System.out.println("OTHER CHARGES TABLE SIZE ::: "+otherchargesList.size());
			inventity.setSalesOrderProductFromBilling(prodList);
//			inventity.setSalesOrderProducts(prodList);
			inventity.setBillingTaxes(taxesList);
			inventity.setBillingOtherCharges(otherchargesList);
			
			inventity.setCustomerBranch(model.getCustomerBranch());

			inventity.setAccountType(model.getAccountType());
			inventity.setArrPayTerms(model.getArrPayTerms());
			
			if (form.getOlbinvoicegroup().getValue() != null) {
				inventity.setInvoiceGroup(form.getOlbinvoicegroup().getValue());
			}
			if (model.getOrderCformStatus() != null) {
				inventity.setOrderCformStatus(model.getOrderCformStatus());
			}
			if (model.getOrderCformPercent() != 0&& model.getOrderCformPercent() != -1) {
				inventity.setOrderCformPercent(model.getOrderCformPercent());
			} else {
				inventity.setOrderCformPercent(-1);
			}
			
			if (model.getDiscount() != 0) {
				inventity.setDiscount(model.getDiscount());
			}
			
			if (model.getNetPayable() != 0) {
				inventity.setNetPayable(model.getNetPayable());
			}
			
			/**
			 * Old code commented by ANIl on 23-10-2017
			 */
//			if (model.getInvoiceAmount() != 0) {
//				inventity.setNetPayable(model.getInvoiceAmount());
//			}
			/**
			 * New code 
			 */
			if (model.getInvoiceAmount() != 0) {
				inventity.setInvoiceAmount(model.getInvoiceAmount());
			}
			
			/**
			 * rohan added this code for coping details from pro-forma to tax invoice Date : 15-05-2017
			 */
			if (model.getComment() != null) {
				inventity.setComment(model.getComment());
			}
			
			if(model.getCategory() !=null){
				inventity.setInvoiceCategory(model.getInvoiceCategory());
			}
			
			if(model.getGroup() !=null){
				inventity.setInvoiceGroup(model.getInvoiceGroup());
			}
			
			if(model.getType() !=null){
				inventity.setInvoiceConfigType(model.getInvoiceConfigType());
			}
			
			/**
			 * ends here  
			 */
			/**
			 *  nidhi
			 *  20-07-2017
			 *   quantity and mesurement transfer to the invoice entity 
			 */
			if(form.getQuantity().getValue() !=null && form.getQuantity().getValue() > 0){
				inventity.setQuantity(form.getQuantity().getValue());
			}
			
			if(form.getOlbUOM().getValue()!=null){
				inventity.setUom(form.getOlbUOM().getValue().trim());
			}
			/*
			 * end   
			 */
			
			
			/***************** vijay number range *************************/
			if(model.getNumberRange()!=null)
				inventity.setNumberRange(model.getNumberRange());
			/****************************************************/
			
			
			/**
			 * date 14 Feb 2017
			 * added by vijay for Setting billing period from date and To date in invoice
			 */
			if(form.getDbbillingperiodFromDate().getValue()!=null)
				inventity.setBillingPeroidFromDate(form.getDbbillingperiodFromDate().getValue());
			if(form.getDbbillingperiodToDate().getValue()!=null)
				inventity.setBillingPeroidToDate(form.getDbbillingperiodToDate().getValue());
			/**
			 * End here
			 */
			
			/** Date 06-09-2017 added by vijay for total amt roundoff *******/
			
			inventity.setTotalAmtExcludingTax(form.getDbtotalAmt().getValue());
			inventity.setDiscountAmt(form.dbDiscountAmt.getValue());
			inventity.setFinalTotalAmt(form.getDbFinalTotalAmt().getValue());
			inventity.setTotalAmtIncludingTax(form.getDbtotalAmtIncludingTax().getValue());
			/**
			 * Old COde commented by ANIL on 23-10-2017
			 */
//			inventity.setTotalBillingAmount(form.getDototalbillamt().getValue());
			/**
			 * New Code 
			 */
			inventity.setTotalBillingAmount(form.getDbtotalAmtIncludingTax().getValue());
			/**
			 * End
			 */
			if(form.getTbroundOffAmt().getValue()!=null&&!form.getTbroundOffAmt().getValue().equals("")){
				inventity.setDiscount(Double.parseDouble(form.getTbroundOffAmt().getValue()));
			}
			
			/**
			 * Date : 06-11-2017 BY ANIL
			 * setting billing comment to invoice
			 * for NBHC
			 */
			if(form.getTacomment().getValue()!=null){
				inventity.setComment(form.getTacomment().getValue());
			}
			/**
			 * End
			 */
			
				
			Timer t = new Timer() {
			      @Override
			      public void run() {

			async.save(inventity, new AsyncCallback<ReturnFromServer>() {

				@Override
				public void onFailure(Throwable caught) {
					form.hideWaitSymbol();
					form.showDialogMessage("An unexpected error occured");
				}

				@Override
				public void onSuccess(ReturnFromServer result) {
					
					model.setProformaCount(result.count);
						
					async.save(model, new AsyncCallback<ReturnFromServer>() {
						@Override
						public void onFailure(Throwable caught) {
							form.hideWaitSymbol();
							form.showDialogMessage("An unexpected error occured");
						}
						@Override
						public void onSuccess(ReturnFromServer result) {
							System.out.println("RRRRRRRRRRRRRRR"+model.getProformaCount());
							form.proformaRefId.setValue(model.getProformaCount()+"");
							form.showDialogMessage("Document Tax Invoiced!",GWTCAlert.OPTION_ROUNDED_BLUE,GWTCAlert.OPTION_ANIMATION);
							form.hideWaitSymbol();
						}
					});
					
					
							
				}
			});
			      }
			};
		    t.schedule(2000);
	}

	@Override
	public void reactOnPrint() {
		
		if(model.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE))
		{
			
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("VendorInvoice", "CompanyAsLetterHead"))
			{
				System.out.println("in side react on prinnt");
				panel=new PopupPanel(true);
				panel.add(conditionPopup);
				panel.setGlassEnabled(true);
				panel.show();
				panel.center();
			}
			else
			{
				final String url = GWT.getModuleBaseURL() + "purchaseInvoice"+"?Id="+model.getId()+"&"+"preprint="+"plane"+"&"+"typeOfOrder="+model.getTypeOfOrder();
				Window.open(url, "test", "enabled");
			}
		}
		
		if(model.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESERVICE)){
			
			//*************rohan changes ********************
			
			MyQuerry querry = new MyQuerry();
		  	Company c = new Company();
		  	Vector<Filter> filtervec=new Vector<Filter>();
		  	Filter filter = null;
		  	filter = new Filter();
		  	filter.setQuerryString("companyId");
			filter.setLongValue(c.getCompanyId());
			filtervec.add(filter);
			filter = new Filter();
			filter.setQuerryString("processName");
			filter.setStringValue("Invoice");
			filtervec.add(filter);
			
			filter = new Filter();
			filter.setQuerryString("processList.status");
			filter.setBooleanvalue(true);
			filtervec.add(filter);
			
			querry.setFilters(filtervec);
			querry.setQuerryObject(new ProcessConfiguration());
			
			service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onFailure(Throwable caught) {
				}			

				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					System.out.println(" result set size +++++++"+result.size());
					
					List<ProcessTypeDetails> processList =new ArrayList<ProcessTypeDetails>();
					
					if(result.size()==0){
//						final String url = GWT.getModuleBaseURL() + "serviceinvoiceprint"+"?Id="+model.getId()+"&"+"preprint="+"plane";
//						Window.open(url, "test", "enabled");
						
						
						/**
						 * Date 28-3-2018
						 * By jayshree
						 * Des.to call the multiple billing invoice
						 */
						
						if(model.isMultipleOrderBilling()==true){
							final String url2 = GWT.getModuleBaseURL() + "serviceinvoiceprint"+"?Id="+model.getId()+"&"+"preprint="+"plane"+"&"+"type="+"MultipleBilling";
							Window.open(url2, "test", "enabled");
						}else{
							final String url = GWT.getModuleBaseURL() + "serviceinvoiceprint"+"?Id="+model.getId()+"&"+"preprint="+"plane"+"&"+"type="+"SingleBilling";
							Window.open(url, "test", "enabled");
							
						}
					
						
					}
					else{
					
						for(SuperModel model:result)
						{
							ProcessConfiguration processConfig=(ProcessConfiguration)model;
							processList.addAll(processConfig.getProcessList());
							
						}
					
					for(int k=0;k<processList.size();k++){	
					if(processList.get(k).getProcessType().trim().equalsIgnoreCase("CompanyAsLetterHead")&&processList.get(k).isStatus()==true){
						
						cnt=cnt+1;
					
					}
					if(processList.get(k).getProcessType().trim().equalsIgnoreCase("PestoIndiaQuotations")&&processList.get(k).isStatus()==true){
						
						pestoIndia=pestoIndia+1;
					
					}
					if(processList.get(k).getProcessType().trim().equalsIgnoreCase("VCareCustomization")&&processList.get(k).isStatus()==true){
						
						vCare=vCare+1;
					
					}
					
					if(processList.get(k).getProcessType().trim().equalsIgnoreCase("InvoiceAndRefDocCustmization")&&processList.get(k).isStatus()==true){
						
						omPestInvoice=omPestInvoice+1;
					
					}
					
					
					if (processList.get(k).getProcessType().trim().equalsIgnoreCase("HygeiaPestManagement")&& processList.get(k).isStatus() == true) {

						hygeia = hygeia + 1;

					}

					if (processList.get(k).getProcessType().trim().equalsIgnoreCase("OnlyForOrionPestSolutions")&& processList.get(k).isStatus() == true) {

						orionPestControl = orionPestControl + 1;

					}
					
					
					if (processList.get(k).getProcessType().trim().equalsIgnoreCase("PCAMBCustomization")&& processList.get(k).isStatus() == true) {

						final String url = GWT.getModuleBaseURL() + "pdfpcambinvoice"+"?Id="+model.getId();
						Window.open(url, "test", "enabled");
						return;

					}
					
					
					System.out.println("ROHAN   =====  "+processList.get(k).getProcessType().trim());
					if(processList.get(k).getProcessType().trim().equalsIgnoreCase("ReddysPestControlQuotations")&&processList.get(k).isStatus()==true){
					
						reddysPestControl=reddysPestControl+1;
						
					}
					
					if(processList.get(k).getProcessType().trim().equalsIgnoreCase("OnlyForPecopp")&&processList.get(k).isStatus()==true){
						pecopp=pecopp+1;
						
					}
					
					}
					
					System.out.println("rpc"+reddysPestControl+"cnt"+cnt+"pesto"+pestoIndia+"v care"+vCare+"ompest"+omPestInvoice);
					
					if(cnt>0){
						System.out.println("in side react on prinnt");
						panel=new PopupPanel(true);
						panel.add(conditionPopup);
						panel.setGlassEnabled(true);
						panel.show();
						panel.center();
					}
					else if(pestoIndia > 0)
					{
						
						System.out.println("in side react on prinnt");
						panel=new PopupPanel(true);
						panel.add(conditionPopup);
						panel.setGlassEnabled(true);
						panel.show();
						panel.center();
						
					}
					else if(vCare > 0)
					{
						
						System.out.println("in side react on prinnt");
						panel=new PopupPanel(true);
						panel.add(conditionPopup);
						panel.setGlassEnabled(true);
						panel.show();
						panel.center();
						
					}
					else if(omPestInvoice > 0)
					{
						
						System.out.println("in side react on prinnt");
						panel=new PopupPanel(true);
						panel.add(conditionPopup);
						panel.setGlassEnabled(true);
						panel.show();
						panel.center();
						
					}
					else if (hygeia > 0) {
						
						final String url = GWT.getModuleBaseURL()+ "pdfinvoice" + "?Id="+ model.getId();
						Window.open(url, "test", "enabled");

					} 
					else if(reddysPestControl > 0){
						
						final String url = GWT.getModuleBaseURL() + "pdfrinvoice"+"?Id="+model.getId();
						Window.open(url, "test", "enabled");
					}else if(pecopp>0){
						final String url = GWT.getModuleBaseURL()+"pecopppdfservlet"+"?Id="+model.getId()+"&"+"type="+"Invoice";
						Window.open(url, "test", "enabled");
						
					}else if(orionPestControl > 0){
						final String url = GWT.getModuleBaseURL() + "pdfOrionInvoice"+"?Id="+model.getId();
						Window.open(url, "test", "enabled");
					}
					else
					{
//						final String url = GWT.getModuleBaseURL() + "serviceinvoiceprint"+"?Id="+model.getId()+"&"+"preprint="+"plane";
//						Window.open(url, "test", "enabled");
						
						

						/**
						 * Date 28-3-2018
						 * By jayshree
						 * Des.to call the multiple billing invoice and Single invoice billing
						 */
						
						if(model.isMultipleOrderBilling()==true){
							final String url2 = GWT.getModuleBaseURL() + "serviceinvoiceprint"+"?Id="+model.getId()+"&"+"preprint="+"plane"+"&"+"type="+"MultipleBilling";
							Window.open(url2, "test", "enabled");
						}else{
							
							final String url = GWT.getModuleBaseURL() + "serviceinvoiceprint"+"?Id="+model.getId()+"&"+"preprint="+"plane"+"&"+"type="+"SingleBilling";
							Window.open(url, "test", "enabled");
							
						}
						
					
						
					}
				}
				}
			});
			//**************************changes ends here ********************************
		}
		
//		final String url = GWT.getModuleBaseURL() + "pdfrinvoice"+"?Id="+model.getId();
//		Window.open(url, "test", "enabled");
		
	}

	@Override
	public void reactOnEmail() {
//		boolean conf = Window.confirm("Do you really want to send email?");
//		if (conf == true) {
//		emailService.initiateInvoiceEmail(model,new AsyncCallback<Void>() {
//
//			@Override
//			public void onFailure(Throwable caught) {
//				Window.alert("Unable To Send Email");
//				caught.printStackTrace();
//			}
//
//			@Override
//			public void onSuccess(Void result) {
//				Window.alert("Email Sent Sucessfully !");
//			}
//		});
//		}
	
		

		/**
		 * @author Vijay Date 19-03-2021
		 * Des :- above old code commented 
		 * Added New Email Popup Functionality
		 */
		emailpopup.showPopUp();
		setEmailPopUpData();
		
		/**
		 * ends here
		 */
		
	}
	
	@Override
	public void reactOnDownload() {
		
		ArrayList<VendorInvoice> invoicingarray=new ArrayList<VendorInvoice>();
		List<VendorInvoice> listbill=(List<VendorInvoice>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
		invoicingarray.addAll(listbill); 
		csvservice.setVendorinvoicelist(invoicingarray, new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);
				
			}

			@Override
			public void onSuccess(Void result) {
				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "csvservlet"+"?type="+142;
				Window.open(url, "test", "enabled");
			}
		});
	}


	@Override
	protected void makeNewModel() {
		model=new VendorInvoice();
	}
	
	public static VendorInvoiceDetailsForm initalize()
	{
		//AppMemory.getAppMemory().currentState=ScreeenState.NEW;
		VendorInvoiceDetailsForm form=new  VendorInvoiceDetailsForm();
		VendorInvoiceDetailsTableProxy gentable=new VendorInvoiceDetailsTableProxy();
		gentable.setView(form);
		gentable.applySelectionModle();
		VendorInvoiceDetailsPresenterSearch.staticSuperTable=gentable;
		VendorInvoiceDetailsSerachProxy searchpopup=new VendorInvoiceDetailsSerachProxy();
		form.setSearchpopupscreen(searchpopup);

		VendorInvoiceDetailsPresenter presenter=new VendorInvoiceDetailsPresenter(form,new VendorInvoice());
		/**
		 * Date : 29-07-2017 BY ANIL
		 * This line was added to update the name of screen loaded from any other screen or by clicking on billing details on account model
		 */
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Accounts/Vendor Invoice Details",Screen.VENDORINVOICEDETAILS);
		/**
		 * END
		 */
		AppMemory.getAppMemory().stickPnel(form);
		return form;
	}
	
	
	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.salesprocess.Invoice")
	public static  class VendorInvoiceDetailsPresenterSearch extends SearchPopUpScreen<VendorInvoice>{

		@Override
		public MyQuerry getQuerry() {
			return null;
		}

		@Override
		public boolean validate() {
			// TODO Auto-generated method stub
			return true;
		}};

		@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.salesprocess.Invoice")
		public static class VendorInvoiceDetailsPresenterTable extends SuperTable<VendorInvoice> implements GeneratedVariableRefrence{

			@Override
			public Object getVarRef(String varName) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void createTable() {
				// TODO Auto-generated method stub

			}

			@Override
			protected void initializekeyprovider() {
				// TODO Auto-generated method stub

			}

			@Override
			public void addFieldUpdater() {
				// TODO Auto-generated method stub

			}

			@Override
			public void setEnable(boolean state) {
				// TODO Auto-generated method stub

			}

			@Override
			public void applyStyle() {
				// TODO Auto-generated method stub

			}} ;
			
			
			
			@Override
			public void onClick(ClickEvent event) {
				
				super.onClick(event);
				

				if(event.getSource().equals(conditionPopup.getBtnOne()))
				{
					
					if(model.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESALES)
							||model.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE))
					{
						final String url = GWT.getModuleBaseURL() + "purchaseInvoice"+"?Id="+model.getId()+"&"+"preprint="+"yes"+"&"+"typeOfOrder="+model.getTypeOfOrder();
						Window.open(url, "test", "enabled");
						panel.hide();
					}

					
					
					if(cnt >0)
					{
						/**
						 * Date 28-3-2018
						 * By jayshree
						 * Des.to call the multiple billing invoice
						 */
						
						if(model.isMultipleOrderBilling()==true){
							final String url2 = GWT.getModuleBaseURL() + "serviceinvoiceprint"+"?Id="+model.getId()+"&"+"preprint="+"yes"+"&"+"type="+"MultipleBilling";
							Window.open(url2, "test", "enabled");
						}else{
							final String url = GWT.getModuleBaseURL() + "serviceinvoiceprint"+"?Id="+model.getId()+"&"+"preprint="+"yes"+"&"+"type="+"SingleBilling";
							Window.open(url, "test", "enabled");
						}
						panel.hide();
					}
					
					else if(pestoIndia > 0)
					{
						System.out.println("Pesto india quotation");
						final String url = GWT.getModuleBaseURL() + "pdfpestoIndiaservice"+"?Id="+model.getId()+"&"+"type="+"i"+"&"+"preprint="+"yes"; 
						Window.open(url, "test", "enabled");
						
						panel.hide();
						
					}
					
					else if(vCare > 0)
					{
						System.out.println("V care quotation");
						
						final String url = GWT.getModuleBaseURL() + "Invoice"+"?Id="+model.getId()+"&"+"preprint="+"yes";
						Window.open(url, "test", "enabled");
						
						panel.hide();
						
					}
					else if(omPestInvoice > 0)
					{
						final String url = GWT.getModuleBaseURL() + "invoicepdf"+"?Id="+model.getId()+"&"+"preprint="+"yes";
						Window.open(url, "test", "enabled");
						panel.hide();
					}
					
					
				}
				
				if(event.getSource().equals(conditionPopup.getBtnTwo()))
				{
					
					if(model.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESALES)
							||model.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE)
							||model.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESERVICEPO))
					{
						final String url = GWT.getModuleBaseURL() + "salesinvoiceprint"+"?Id="+model.getId()+"&"+"preprint="+"no"+"&"+"typeOfOrder="+model.getTypeOfOrder();;
						Window.open(url, "test", "enabled");
						panel.hide();
					}

					
					if(cnt >0)
					{
						
//						System.out.println("inside two no");
//						final String url = GWT.getModuleBaseURL() + "serviceinvoiceprint"+"?Id="+model.getId()+"&"+"preprint="+"no"+"&"+"type="+"SingleBilling";
//						Window.open(url, "test", "enabled");
//						panel.hide();
						

						/**
						 * Date 28-3-2018
						 * By jayshree
						 * Des.to call the multiple billing invoice
						 */
						
						if(model.isMultipleOrderBilling()==true){
							final String url2 = GWT.getModuleBaseURL() + "serviceinvoiceprint"+"?Id="+model.getId()+"&"+"preprint="+"no"+"&"+"type="+"MultipleBilling";
							Window.open(url2, "test", "enabled");
						}else{
							final String url = GWT.getModuleBaseURL() + "serviceinvoiceprint"+"?Id="+model.getId()+"&"+"preprint="+"no"+"&"+"type="+"SingleBilling";
							Window.open(url, "test", "enabled");
							
						}
						panel.hide();
						
					}
					
					else if(pestoIndia > 0)
					{
						System.out.println("Pesto india quotation");
						final String url = GWT.getModuleBaseURL() + "pdfpestoIndiaservice"+"?Id="+model.getId()+"&"+"type="+"i"+"&"+"preprint="+"no"; //+"&"+"group="+model.getGroup();//1st PDF
						Window.open(url, "test", "enabled");
						panel.hide();
					
					}
					else if(vCare > 0)
					{
						System.out.println("V care quotation");
						final String url = GWT.getModuleBaseURL() + "Invoice"+"?Id="+model.getId()+"&"+"preprint="+"no";
						Window.open(url, "test", "enabled");
						
						panel.hide();
						
					}
					
					else if(omPestInvoice > 0)
					{
						final String url = GWT.getModuleBaseURL() + "invoicepdf"+"?Id="+model.getId()+"&"+"preprint="+"no";
						Window.open(url, "test", "enabled");
						panel.hide();
					}
					
				}
				
				
				

				if(event.getSource()==popup.getBtnOk()){
//					panel.hide();
//					/**
//					 * Date:28-11-2017 BY ANIL
//					 * if invoice in created state we will cancelled invoice directly else need to cancel its corresponding documents.
//					 */
//					if(model.getStatus().equals(Invoice.APPROVED)){
//						statusOfPaymentFromInvoiceID();
//					}else{
//						createNewInvoice();
//					}
//					popup.getRemark().setValue("");
			if (popup.getRemark().getValue().trim() != null
					&& !popup.getRemark().getValue().trim().equals("")) {
				panel.hide();
				
				/**
				 * Date 14-02-2018 By vijay added if condition for while Canceling Proforma invoice must cancel first tax invoice  
				 */
				if(model.getInvoiceType().equals("Proforma Invoice")){
					if(form.proformaRefId.getValue()!=null && !form.proformaRefId.getValue().equals("")){
						checkTaxInvoiceStatus(form.proformaRefId.getValue());
					}else {/** date 30/03/2018 added by komal for performa invoice cancellation **/
						createNewInvoice();
					}
				}else{
					
					/**
					 * Date:28-11-2017 BY ANIL if invoice in created state we will
					 * cancelled invoice directly else need to cancel its
					 * corresponding documents.
					 */
					if (model.getStatus().equals(Invoice.APPROVED)) {
						statusOfPaymentFromInvoiceID();
					} else {
						createNewInvoice();
					}
				}
				
				//popup.getRemark().setValue("");
			} else {
				form.showDialogMessage("Please enter valid remark to cancel Invoice..");
			}
					
				}
				else if(event.getSource()==popup.getBtnCancel()){
					panel.hide();
					popup.getRemark().setValue("");
				}
				
				if (event.getSource()==form.btReferenceDetails) {
					reactOnReferenceDetailsPopUp();
					refDocPanel = new PopupPanel(true);
					refDocPanel.add(refDocPopUp);
					refDocPanel.show();
					refDocPanel.center();
				}
				if(event.getSource()==refDocPopUp.getBtnOk()){
					refDocPanel.hide();
				}
				/** date 24.10.2018 added by komal for tds **/
				if(event.getSource().equals(form.cbtdsApplicable)){
					if (event.getSource() == form.getCbtdsApplicable()) {
							if (form.getCbtdsApplicable().getValue().equals(true)) {
								form.getTbtdsPerValue().setEnabled(true);
								form.getDbtdsValue().setEnabled(true);
							}
			
							
							if (form.getCbtdsApplicable().getValue().equals(false)) {
								form.getTbtdsPerValue().setEnabled(false);
								form.getDbtdsValue().setEnabled(false);
								form.getTbtdsPerValue().setValue(0.0);
								form.getDbtdsValue().setValue(0.0);
								System.out.println("tds amt ="+form.getDbtdsValue().getValue());
								double totalValue = 0 , roundOff = 0 , total = 0;;
								if(form.dbtotalAmtIncludingTax.getValue()!=null){
									totalValue = form.dbtotalAmtIncludingTax.getValue();
								}
								if(form.tbroundOffAmt.getValue()!=null && !form.tbroundOffAmt.getValue().equals("")){
									roundOff = Double.parseDouble(form.tbroundOffAmt.getValue());
								}
								total = Math.round(totalValue + roundOff);
									if(form.getDbtdsValue().getValue()!=null && form.getDbtdsValue().getValue()!=0)
									{
										double tds = form.getDbtdsValue().getValue();
										form.netPayable.setValue(total - tds);					
									}else{
										form.netPayable.setValue(total);
									}		
						}
						
					}
				}
			}
			
			
			/**
			 * Date 14-02-2018 By vijay added if condition for while Canceling Proforma invoice must cancel first tax invoice  
			 */
			
			private void checkTaxInvoiceStatus(String value) {
				int taxInvoiceId = Integer.parseInt(value.trim());
				Vector<Filter> filterVec = new Vector<Filter>();
				Filter filter = null;
				
				MyQuerry querry = new MyQuerry();
				filter = new Filter();
				filter.setQuerryString("count");
				filter.setIntValue(taxInvoiceId);
				filterVec.add(filter);
				
				filter = new Filter();
				filter.setQuerryString("invoiceType");
				filter.setStringValue(AppConstants.CREATETAXINVOICE);
				filterVec.add(filter);
				
				querry.setFilters(filterVec);
				querry.setQuerryObject(new VendorInvoice());
				async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
					
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						// TODO Auto-generated method stub
						if(result.size()!=0){
							
							for(SuperModel model:result){
								VendorInvoice invoice = (VendorInvoice) model;
								if(!invoice.getStatus().equals("Cancelled")){
									form.showDialogMessage("Please cancel Tax Invoice first");
									break;
								}else{
									createNewInvoice();
									break;
								}
							}
						}
						
					}
					
					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						
					}
				});
				
				
			}
			/**
			 * ends here
			 */
			
			
			/*
			 * Only in View State
			 */
			private void reactOnReferenceDetailsPopUp() {
				// TODO Auto-generated method stub
				if(model.getSapInvoiceId()!=null)
				refDocPopUp.getTbRefInvoiceId().setValue(model.getSapInvoiceId());
				if(model.getSAPNetValue()!=0)
				refDocPopUp.getDbNetValue().setValue(model.getSAPNetValue());
				if(model.getSAPTaxAmount()!=0)
				refDocPopUp.getDbTaxAmount().setValue(model.getSAPTaxAmount());
				if(model.getSAPTotalAmount()!=0)
				refDocPopUp.getDbTotalAmount().setValue(model.getSAPTotalAmount());
				
			}
			/********************************Cancellation Logic**********************************/
			
			
			private void statusOfPaymentFromInvoiceID()
			{
				if(form.getTbstatus().getValue().trim().equals(Invoice.APPROVED))
				{
				
					final int invoiceid=Integer.parseInt(form.getIbinvoiceid().getValue().trim());
					MyQuerry querry = new MyQuerry();
				  	Company c = new Company();
				  	Vector<Filter> filtervec=new Vector<Filter>();
				  	Filter filter = null;
				  	filter = new Filter();
				  	filter.setQuerryString("companyId");
					filter.setLongValue(c.getCompanyId());
					filtervec.add(filter);
					filter = new Filter();
					filter.setQuerryString("invoiceCount");
					filter.setIntValue(invoiceid);
					filtervec.add(filter);
					querry.setFilters(filtervec);
					querry.setQuerryObject(new CustomerPayment());
					async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			  			
			  			@Override
			  			public void onFailure(Throwable caught) {
			  			}
	
			  			@Override
						public void onSuccess(ArrayList<SuperModel> result) {
			  				int cnt=0;
			  			
			  				ArrayList<CustomerPayment> slist=new ArrayList<CustomerPayment>();
			  				
							for(SuperModel model:result)
							{
								CustomerPayment custPaymentEntity= (CustomerPayment)model;
								custPaymentEntity.setStatus(custPaymentEntity.getStatus());
								custPaymentEntity.setInvoiceCount(custPaymentEntity.getInvoiceCount());
								slist.add(custPaymentEntity);
							}
							for(int i=0;i<slist.size();i++)
							{
								if(invoiceid==slist.get(i).getInvoiceCount());
								{
									if(slist.get(i).getStatus().equals(Invoice.PAYMENTCLOSED))
									{
										cnt=cnt+1;
									}
								}
							} 
							if(cnt>0){
								form.showDialogMessage("Document cannot be cancelled because payment document status is closed!");
							}
							else
							{
								changeStatus();
								createNewInvoice();
							}
							}
					});
				}
				//   rohan added this condition for proforma invoice cancel 
				else if(form.getTbstatus().getValue().trim().equals(BillingDocument.BILLINGINVOICED))
				{
					model.setStatus(BillingDocument.PROFORMAINVOICE);
					
					async.save(model,new AsyncCallback<ReturnFromServer>() {
						@Override
						public void onFailure(Throwable caught) {
							form.showDialogMessage("An Unexpected Error occured !");
//							
						}
						@Override
						public void onSuccess(ReturnFromServer result) {
							form.setAppHeaderBarAsPerStatus();
							form.setToViewState();
							form.getTbstatus().setValue(model.getStatus());
							form.showDialogMessage("Document Cancelled...!!");
						}
					});
				}
		}
			
			
			
			public void changeStatus()
			{
				final int invoiceid=Integer.parseInt(form.getIbinvoiceid().getValue().trim());
				final String invoiceStatus=form.getTbstatus().getValue().trim();
				
				MyQuerry querry = new MyQuerry();
			  	Company c = new Company();
			  	Vector<Filter> filtervec=new Vector<Filter>();
			  	Filter filter = null;
			  	filter = new Filter();
			  	filter.setQuerryString("companyId");
				filter.setLongValue(c.getCompanyId());
				filtervec.add(filter);
				filter = new Filter();
				filter.setQuerryString("invoiceCount");
				filter.setIntValue(invoiceid);
				filtervec.add(filter);
				querry.setFilters(filtervec);
				querry.setQuerryObject(new CustomerPayment());
				async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
		  			
		  			@Override
		  			public void onFailure(Throwable caught) {
		  			}

		  			@Override
					public void onSuccess(ArrayList<SuperModel> result) {
		  				
		  				for(SuperModel smodel:result)
							{
		  					CustomerPayment custpay = (CustomerPayment)smodel;
								custpay.setStatus(CustomerPayment.CANCELLED);
								custpay.setComment("Invoice Id ="+invoiceid+" "+"Invoice Status ="+invoiceStatus+"\n"
								+"has been cancelled by "+LoginPresenter.loggedInUser.trim()
								+" with remark"+"\n"+"Remark ="+popup.getRemark().getValue());
								async.save(custpay,new AsyncCallback<ReturnFromServer>() {
		//
									@Override
									public void onFailure(Throwable caught) {
										form.showDialogMessage("An Unexpected Error occured !");
//										
									}
									@Override
									public void onSuccess(ReturnFromServer result) {
									}
								});
							}
		  				
		  			}
				
				});
			}
			
			protected void createNewInvoice()
			{
				/**
				 * Date : 28-11-2017 By ANIL
				 * updated invoice cancellation as if invoice is in approved state we need to check accounting interface and update accourdingly
				 * else cancel invoice only
				 */
				if(model.getStatus().equals(Invoice.APPROVED)){
					final int invoiceid=Integer.parseInt(form.getIbinvoiceid().getValue().trim());
					final String invoiceStatus=form.getTbstatus().getValue().trim();
					
					MyQuerry querry = new MyQuerry();
				  	Company c = new Company();
				  	Vector<Filter> filtervec=new Vector<Filter>();
				  	Filter filter = null;
				  	filter = new Filter();
				  	filter.setQuerryString("companyId");
					filter.setLongValue(c.getCompanyId());
					filtervec.add(filter);
					filter = new Filter();
					filter.setQuerryString("documentID");
					filter.setIntValue(invoiceid);
					filtervec.add(filter);
					
					filter = new Filter();
					filter.setQuerryString("documentType");
					filter.setStringValue("Invoice");
					filtervec.add(filter);
					
					querry.setFilters(filtervec);
					querry.setQuerryObject(new AccountingInterface());
					async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			  			
			  			@Override
			  			public void onFailure(Throwable caught) {
			  			}
	
			  			@Override
						public void onSuccess(ArrayList<SuperModel> result) {
			  				
			  				for(SuperModel smodel:result)
								{
			  					AccountingInterface accntInt = (AccountingInterface)smodel;
			  					
			  					if(accntInt.getStatus().equalsIgnoreCase(AccountingInterface.TALLYSYNCED)){
			  						accntInt.setStatus(AccountingInterface.DELETEFROMTALLY);
				  					accntInt.setRemark("Invoice Id ="+invoiceid+" "+"Invoice Status ="+invoiceStatus+"\n"
										+"has been cancelled by "+LoginPresenter.loggedInUser.trim()
										+" with remark"+"\n"+"Remark ="+popup.getRemark().getValue());
			  																																}
			  					else{
			  						accntInt.setStatus(AccountingInterface.CANCELLED);
				  					accntInt.setRemark("Invoice Id ="+invoiceid+" "+"Invoice Status ="+invoiceStatus+"\n"
										+"has been cancelled by "+LoginPresenter.loggedInUser.trim()
										+" with remark"+"\n"+"Remark ="+popup.getRemark().getValue());
			  					}
			  					async.save(accntInt,new AsyncCallback<ReturnFromServer>() {
										@Override
										public void onFailure(Throwable caught) {
											form.showDialogMessage("An Unexpected Error occured !");
										}
										@Override
										public void onSuccess(ReturnFromServer result) {
										}
									});
								}
			  			}
					
					});
				}
				/**
				 * Date : 28-11-2017 BY ANIL
				 */
				if(model.getStatus().equals(Invoice.APPROVED)){
					model.setStatus(Invoice.CREATED);
				}else{
					model.setStatus(Invoice.CANCELLED);
					if(model.getComment()!=null){
					model.setComment(model.getComment()+"\n"+"Invoice Id ="+model.getCount()+" "+"Invoice Status ="+model.getStatus()+"\n"
										+"has been cancelled by "+LoginPresenter.loggedInUser.trim()
										+" with remark"+"\n"+"Remark ="+popup.getRemark().getValue());
					}else{
						model.setComment("Invoice Id ="+model.getCount()+" "+"Invoice Status ="+model.getStatus()+"\n"
								+"has been cancelled by "+LoginPresenter.loggedInUser.trim()
								+" with remark"+"\n"+"Remark ="+popup.getRemark().getValue());
					}
				}
				
				
				async.save(model,new AsyncCallback<ReturnFromServer>() {
					@Override
					public void onFailure(Throwable caught) {
						form.showDialogMessage("An Unexpected Error occured !");
//						
					}
					@Override
					public void onSuccess(ReturnFromServer result) {
						form.setAppHeaderBarAsPerStatus();
						form.setToViewState();
						form.getTbstatus().setValue(model.getStatus());
					}
				});
			}
			
			/*********************************Approval Logic*********************************/
			
			
			private void reactOnApprovalVal()
			{
				boolean groupValidation=form.checkForInvGroup();
				if(groupValidation==false&&form.getOlbinvoicegroup().getSelectedIndex()==0){
					form.showDialogMessage("Invoice Group is Mandatory!");
				}
				else{
					
					/**
					 * Rohan added this code Only for NBHC 
					 * Date : 17-04-2017
					 * This is used to validate invoice date is not after todays date 
					 */
					
					if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "OnlyForNBHC")){
						Date todaysDate = new Date();
						Date invDate = model.getInvoiceDate();
							if(invDate.after(todaysDate)){
								form.showDialogMessage("Invoice Date should not be greater than todays date..!");
							}
							else{
								reactOnReqApproval();
							}
					}
					else{
						reactOnReqApproval();
					}
					
					/**
					 * ends here 
					 */
					
				}
			}
			
			private void reactOnReqApproval()
			{
				
				/**
				 * Updated By Anil Date : 14-10-2016
				 * Release : 30 Sept 2016 
				 * Project : PURCHASE MODIFICATION(NBHC)
				 */
				String documentCreatedBy="";
				if(model.getCreatedBy()!=null){
					documentCreatedBy=model.getCreatedBy();
				}
				
				
				if(model.getCount()!=0)
				{
					form.showWaitSymbol();
					ApproverFactory appFactory=new ApproverFactory();
					/**
					 *  nidhi
					 *  7-10-2017
					 *  method change for display name and id
					 */
					Approvals approval=appFactory.getApprovals(model.getEmployee(), model.getApproverName()
							, model.getBranch(), model.getCount(),documentCreatedBy,model.getCustomerId()+"",model.getPersonInfo().getFullName());
					saveApprovalReq(approval,ConcreteBusinessProcess.REQUESTED,"Request Sent!");
					reactOnApprovalEmail(approval);
				}
			}
			
			private void saveApprovalReq(Approvals approval,final String status,final String dialogmsg)
			{
				service.save(approval, new AsyncCallback<ReturnFromServer>() {
					
					@Override
					public void onSuccess(ReturnFromServer result) 
					{
						model.setStatus(status);
						service.save(model, new AsyncCallback<ReturnFromServer>() {

							@Override
							public void onFailure(Throwable caught) {
								form.hideWaitSymbol();
							}

							@Override
							public void onSuccess(ReturnFromServer result) {
								form.getTbstatus().setValue(status);
								form.setToViewState();
								form.showDialogMessage(dialogmsg);
								form.hideWaitSymbol();
							}
						});
					}
					
					@Override
					public void onFailure(Throwable caught) {
						form.showDialogMessage("Request Failed! Try Again!");
						form.hideWaitSymbol();
						
					}
				});
			}
			

			private void reactOnCancelApproval()
			{
		  		form.showWaitSymbol(); 
		  		ApproverFactory approverfactory=new ApproverFactory();
				  Filter filter=new Filter();
				  filter.setStringValue(approverfactory.getBuisnessProcessType());
				  filter.setQuerryString("businessprocesstype");
				  Filter filterOnid=new Filter();
				  filterOnid.setIntValue(model.getCount());
				  filterOnid.setQuerryString("businessprocessId");
				  Vector<Filter> vecfilter=new Vector<Filter>();
				  vecfilter.add(filter);
				  vecfilter.add(filterOnid);
				  MyQuerry querry=new MyQuerry(vecfilter, new Approvals());
				  final ArrayList<SuperModel> array = new ArrayList<SuperModel>();
				  service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {

					@Override
					public void onFailure(Throwable caught) {
						caught.printStackTrace();
						
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						array.addAll(result);
						Approvals approval=(Approvals) array.get(0);
						approval.setStatus(Approvals.CANCELLED);
						 
						saveApprovalReq(approval, ConcreteBusinessProcess.CREATED, "Approval Request Cancelled!");
					}
				});
			  
			}

			public void reactOnApprovalEmail(Approvals approvalEntity)
			{
				//Quotation Entity patch patch patch
				emailService.initiateApprovalEmail(approvalEntity,new AsyncCallback<Void>() {

					@Override
					public void onFailure(Throwable caught) {
						Window.alert("Unable To Send Email");
						caught.printStackTrace();
					}

					@Override
					public void onSuccess(Void result) {
//						Window.alert("Email Sent Sucessfully !");
					}
				});
			}

		
//     rohan added this code for invoice editing ************* 			
			
			
			@Override
			public void onRowCountChange(RowCountChangeEvent event) {

				System.out.println("INSIDE RoW COUNT CHANGE METHOD....!!!");
				Console.log("INSIDE RoW COUNT CHANGE METHOD....!!!");
			
			if (event.getSource() == form.getSalesProductTable().getTable()) {
				
				if(form.getSalesProductTable().getDataprovider().getList().size()!=0)
				{
					Console.log("INSIDE IF CONDITION....!!!");
				double retrieveBillAmt = form.getSalesProductTable().calculateTotalBillAmt();
				// form.getDototalbillamt().setValue(retrieveBillAmt);

				billingTaxTableOnRowChange();
				retrieveBillAmt = retrieveBillAmt+ form.getInvoiceTaxTable().calculateTotalTaxes();

//				if (multipleContractBillingInvoiceFlag == false) {
//					Console.log("PRODUCT FALSE....!!!");
//					System.out.println("PRODUCT FALSE....!!!");
//					billingTableOnRowChange(retrieveBillAmt);
//				}
//
//				if (multipleContractBillingInvoiceFlag == true) {
//					System.out.println("PRODUCT TRUE....!!!");
//					Console.log("PRODUCT TRUE....!!!");
//					reactOnProductsChange();
//				}

				if(checkproductstatus()){
					System.out.println("PRODUCT TRUE....!!!");
					reactOnProductsChange();
				}else{
					System.out.println("Inside else=================");
					retrieveproductinfomation();
				}
				
				}
				
				
				System.out.println("Size in side row count change 123"+form.getSalesProductTable().getDataprovider().getList().size());
			}
			
			/**
			 * Date : 21-09-2017 By ANIL
			 */
			double totalOfProductPrice=0;
			if(event.getSource().equals(form.tblOtherCharges.getTable())){
				NumberFormat nf = NumberFormat.getFormat("0.00");
				if(form.tblOtherCharges.getValue().size()!=0){
				
                   totalOfProductPrice= form.getDbtotalAmt().getValue();
				
				for(OtherCharges obj:form.getTblOtherCharges().getValue()){
					double otherChargAmt=0;
					OtherTaxCharges otherTax=form.tblOtherCharges.getOtherChargesDetails(obj.getOtherChargeName());
					if(otherTax!=null){
						if(otherTax.getOtherChargePercent()!=0){
							otherChargAmt=((totalOfProductPrice*otherTax.getOtherChargePercent())/100);
							obj.setAmount(otherChargAmt);
						}else if(otherTax.getOtherChargeAbsValue()!=null){
							if(obj.getAmount()==0){
							otherChargAmt=otherTax.getOtherChargeAbsValue();
							obj.setAmount(otherChargAmt);
							}
						}
					}
					
					
				}
				}
				form.invoiceTaxTable.connectToLocal();
				try {
					form.addProdTaxes();
					form.addOtherChargesInTaxTbl();
					form.dbOtherChargesTotal.setValue(form.tblOtherCharges.calculateOtherChargesSum());
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				double totalIncludingTax=0;
				if(form.getDbFinalTotalAmt().getValue()!=null){
					totalIncludingTax = form.getDbFinalTotalAmt().getValue()+ form.invoiceTaxTable.calculateTotalTaxes();
				}else{
					totalIncludingTax = form.invoiceTaxTable.calculateTotalTaxes();
				}
				if(form.dbOtherChargesTotal.getValue()!=null){
					totalIncludingTax=totalIncludingTax+form.dbOtherChargesTotal.getValue();
				}
				form.getDbtotalAmtIncludingTax().setValue(totalIncludingTax);
				form.setToatalAmountIncludingTax(Double.parseDouble(nf.format(totalIncludingTax)));
				
				double netPay = fillNetPayable(form.getToatalAmountIncludingTax());
				netPay = Math.round(netPay);
				int netPayable = (int) netPay;
				netPay = netPayable;
				form.getDototalbillamt().setValue(Double.parseDouble(nf.format(netPay)));

				if(!form.getTbroundOffAmt().getValue().equals("") && form.getTbroundOffAmt().getValue()!=null){
					String roundOff = form.getTbroundOffAmt().getValue();
					double roundoffAmt =  AppUtility.calculateRoundOff(roundOff, netPay);
					if(roundoffAmt!=0){
						form.getNetPayable().setValue(roundoffAmt);
						form.getDoinvoiceamount().setValue(roundoffAmt);

					}else{
						form.getNetPayable().setValue(roundoffAmt);
						form.getTbroundOffAmt().setValue("");
						form.getDoinvoiceamount().setValue(roundoffAmt);
					}
				}
				else{
					form.getNetPayable().setValue(netPay);
					form.getDoinvoiceamount().setValue(Double.parseDouble(nf.format(netPay)));
				}

			}
				
			/**
			 * End
			 */
			
			System.out.println("Size in side row count change 456"+form.getSalesProductTable().getDataprovider().getList().size());
			}
			
			
			private void billingTaxTableOnRowChange()
			{
				List<SalesOrderProductLineItem> salesProdlis=form.getSalesProductTable().getDataprovider().getList();
				for(int i=0;i<salesProdlis.size();i++)
				{
					List<ContractCharges> updateBillLis=updateTaxesTable(salesProdlis.get(i).getPaymentPercent(),salesProdlis.get(i).getIndexVal());
					form.getInvoiceTaxTable().setValue(updateBillLis);
				}
				double retrieveAmt=form.getSalesProductTable().calculateTotalBillAmt()+form.getInvoiceTaxTable().calculateTotalTaxes()+form.getInvoiceChargesTable().calculateTotalBillingCharges();
				retrieveAmt=Math.round(retrieveAmt);
				form.getDototalbillamt().setValue(retrieveAmt);
			}
			
			private List<ContractCharges> updateTaxesTable(double payPercentValue,int indexValue)
			{
				NumberFormat nf=NumberFormat.getFormat("0.00");
				
				List<ContractCharges> taxList=form.getInvoiceTaxTable().getDataprovider().getList();
				ArrayList<ContractCharges> taxArr=new ArrayList<ContractCharges>();
				double updatedAssess=0;
				for(int i=0;i<taxList.size();i++)
				{
					ContractCharges entity=new ContractCharges();
					entity.setTaxChargeName(taxList.get(i).getTaxChargeName());
					entity.setTaxChargePercent(taxList.get(i).getTaxChargePercent());
					if(taxList.get(i).getIdentifyTaxCharge()==indexValue){
						updatedAssess=taxList.get(i).getTaxChargeAssesVal()*payPercentValue/100;
						updatedAssess=Double.parseDouble(nf.format(updatedAssess));
						entity.setTaxChargeAssesVal(updatedAssess);
					}
					else{
						entity.setTaxChargeAssesVal(taxList.get(i).getTaxChargeAssesVal());
					}
					
					taxArr.add(entity);
				}
				return taxArr;
			}
			
			
			public boolean checkproductstatus() {
				if (form.salesProductTable.getDataprovider().getList().get(0).getPrduct() != null) {
					return true;
				}
				return false;
			}

			public void retrieveproductinfomation() {
				final ArrayList<SuperProduct> productlist = new ArrayList<SuperProduct>();

				for (int k = 0; k < form.salesProductTable.getDataprovider().getList().size(); k++) {
//					List<Integer> productIdlist = new ArrayList<Integer>();
					int prodid = form.salesProductTable.getDataprovider().getList().get(k).getProdId();

					System.out.println("Product id ::::: " + prodid);
					MyQuerry querry = new MyQuerry();
					Vector<Filter> filtervec = new Vector<Filter>();
					Filter filter = null;

					filter = new Filter();
					filter.setQuerryString("count");
					filter.setIntValue(prodid);
					filtervec.add(filter);

					filter = new Filter();
					filter.setLongValue(model.getCompanyId());
					filter.setQuerryString("companyId");
					filtervec.add(filter);

					querry.setFilters(filtervec);
					querry.setQuerryObject(new SuperProduct());

					async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							System.out.println("RESULT LIST SIZE :: "+ result.size());
							counter++;
							for (SuperModel model : result) {
								SuperProduct entity = (SuperProduct) model;
								productlist.add(entity);
							}
							if (counter == form.salesProductTable.getDataprovider().getList().size()) {
								System.out.println("Product List SIZE ::: "+ productlist.size());
								for (int i = 0; i < form.salesProductTable.getDataprovider().getList().size(); i++) {
									for (int j = 0; j < productlist.size(); j++) {
										if (form.salesProductTable.getDataprovider().getList().get(i).getProdId() == productlist.get(j).getCount()) {
											form.salesProductTable.getDataprovider().getList().get(i).setPrduct(productlist.get(j));
										}
									}
								}
								reactOnProductsChange();
							}
						}
						@Override
						public void onFailure(Throwable caught) {
							form.showDialogMessage("An Unexpected Error occured !");
						}
					});
				}
			}

			
			
		private void reactOnProductsChange() {
				
				form.showWaitSymbol();
				Timer timer = new Timer() {
					@Override
					public void run() {
						NumberFormat nf = NumberFormat.getFormat("0.00");
						double totalExcludingTax = form.getSalesProductTable().calculateTotalExcludingTax();
						totalExcludingTax = Double.parseDouble(nf.format(totalExcludingTax));
						form.setTotalAmount(totalExcludingTax);
						
						// Date 04-09-2017 added by vijay
						form.getDbtotalAmt().setValue(totalExcludingTax);
						
						System.out.println("TOTAL AMOUNT EXCLUDING :: "+ form.getTotalAmount());
						

						 /**
					     * Date 03-09-2017 added by vijay for final total amount after discount
					     */
					    if(form.getDbDiscountAmt().getValue()!=null && form.getDbDiscountAmt().getValue()!=0){
				    		if(form.getDbDiscountAmt().getValue()>=totalExcludingTax){
					    		form.getDbDiscountAmt().setValue(0d);
				    		}
				    		else if(form.getDbDiscountAmt().getValue()<=totalExcludingTax){
						    	form.getDbFinalTotalAmt().setValue(totalExcludingTax - form.getDbDiscountAmt().getValue());
				    		}
				    		else{
					    		form.getDbFinalTotalAmt().setValue(totalExcludingTax);
				    		}
				    	}
					    else{
					    	form.getDbFinalTotalAmt().setValue(totalExcludingTax);
				    	}
					    
					    /**
					     * ends here
					     */

						try {
							System.out.println();
							System.out.println("PRODUCT TAX TABLE CALCULATION BEGIN.....");
							form.invoiceTaxTable.connectToLocal();
							form.addProdTaxes();
							
							/**
							 * Date : 21-09-2017 By ANIL
							 * Calculating tax On Other charges 
							 */
							form.addOtherChargesInTaxTbl();
							form.dbOtherChargesTotal.setValue(form.tblOtherCharges.calculateOtherChargesSum());
							
							/**
							 * End
							 */
//							double totalIncludingTax = form.getTotalAmount()+ form.invoiceTaxTable.calculateTotalTaxes();
							
							// Date 03-09-2017 added by vijay old line above commented
							double totalIncludingTax = form.getDbFinalTotalAmt().getValue()+ form.invoiceTaxTable.calculateTotalTaxes();
							form.getDbtotalAmtIncludingTax().setValue(totalIncludingTax);
							
							System.out.println("total"+totalIncludingTax);
							
							form.setToatalAmountIncludingTax(Double.parseDouble(nf.format(totalIncludingTax)));
							
							
//							form.getBillingChargesTable().connectToLocal();
//							form.updateChargesTable();
						} catch (Exception e) {
							e.printStackTrace();
						}
						double netPay = fillNetPayable(form.getToatalAmountIncludingTax());
						
						System.out.println("net pay"+netPay);
						
						netPay = Math.round(netPay);
						int netPayable = (int) netPay;
						netPay = netPayable;
						form.getDototalbillamt().setValue(Double.parseDouble(nf.format(netPay)));
						
						//  rohan added following for discount on product change
//						//      **********start ****************
//					
//						System.out.println("Discount value "+form.getDiscount().getValue());
//						if(form.getDiscount().getValue()!=null)
//						{
//							System.out.println("1111111"+netPay);
//							int pay =0;
//							pay =(int) (netPay - form.getDiscount().getValue());
//							pay = Math.round(pay);
//							
//							form.getNetPayable().setValue(Double.parseDouble(nf.format(pay)));
//							form.getDoinvoiceamount().setValue(Double.parseDouble(nf.format(pay)));
//						}
//						else{
//							form.getDiscount().setValue(0.0);
//							form.getNetPayable().setValue(Double.parseDouble(nf.format(netPay)));
//							form.getDoinvoiceamount().setValue(Double.parseDouble(nf.format(netPay)));
//						}
//						
						// ***************ends here ****************
						
						// above commented by vijay 	
						/** Date 01-09-2017 added by vijay for final netpayable amount after discount **/

						if(!form.getTbroundOffAmt().getValue().equals("") && form.getTbroundOffAmt().getValue()!=null){
							String roundOff = form.getTbroundOffAmt().getValue();
							double roundoffAmt =  AppUtility.calculateRoundOff(roundOff, netPay);
							if(roundoffAmt!=0){
								form.getNetPayable().setValue(roundoffAmt);
								form.getDoinvoiceamount().setValue(roundoffAmt);

							}else{
								form.getNetPayable().setValue(roundoffAmt);
								form.getTbroundOffAmt().setValue("");
								form.getDoinvoiceamount().setValue(roundoffAmt);
							}
						}
						else{
							form.getNetPayable().setValue(netPay);
							form.getDoinvoiceamount().setValue(Double.parseDouble(nf.format(netPay)));
						}
						/** ends here *******/
						
//						if(multipleContractBillingInvoiceFlag==false){
//							Console.log("INSIDE multipleContractBillingInvoiceFlag....!!!");
//							billingTableOnRowChange(Double.parseDouble(nf.format(netPay)));
//						}
						form.hideWaitSymbol();
					}
				};
				timer.schedule(3000);
			}	
		
		private double fillNetPayable(double amtincltax)
		{
			double amtval=0;
			if(form.getInvoiceChargesTable().getDataprovider().getList().size()==0)
			{
				amtval=amtincltax;
			}
			if(form.getInvoiceChargesTable().getDataprovider().getList().size()!=0)
			{
				amtval=amtincltax+form.getInvoiceChargesTable().calculateTotalBillingCharges();
			}
			amtval=Math.round(amtval);
			int retAmtVal=(int) amtval;
			amtval=retAmtVal;
			System.out.println("rohan in side net payble "+amtval);
			return amtval;
			
		}

		//Date 04-09-2017 commented by vijay handled in form with new code
		
//		@Override
//		public void onValueChange(ValueChangeEvent<Double> event) {
//			System.out.println("rohna in side value change handler");
//			NumberFormat nf = NumberFormat.getFormat("0.00");
//			if(event.getSource()== form.getDiscount()){
//				if(form.getDiscount()!=null)
//				{
//					System.out.println("rohna in side event");
//					int netPayable =0;
//					netPayable= (int) (form.getDototalbillamt().getValue()-form.getDiscount().getValue());
//					
//					form.getNetPayable().setValue(Double.parseDouble(nf.format(netPayable)));
//					form.getDoinvoiceamount().setValue(Double.parseDouble(nf.format(netPayable)));
//				}
//			}
//		}
		
		/**
		 * Date : 09-11-2017 BY ANIL
		 * if the process configuration is active then reference number is mandatory 
		 * at the time of sending approval or submitting the invoice
		 */
		private boolean isRefNumIsMandatory(){
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "MakeReferenceNumberMandatory")){
				if(model.getRefNumber()!=null&&!model.getRefNumber().equals("")){
					return true;
				}else{
					form.showDialogMessage("Reference number is manadatory!");
					return false;
				}
			}else{
				return true;
			}
		}
		
		
		/**
		 * Date : 21-11-2017 By ANIL
		 *
		 */
		@Override
		public void reactOnSave() {
			super.reactOnSave();
			if(form.updatePaymentFlag){
				VendorInvoice invoice=model;
				if(form.custPayment!=null){
					updateExistingPayment(invoice,form.custPayment,true);
				}else{
					updateExistingPayment(invoice,form.custPayment,false);
//					invoice.createPaymentDetails();
				}
			}
		}

		private void updateExistingPayment(VendorInvoice invoice,CustomerPayment paydetails,boolean updateFlag) {
			if(!updateFlag){
				paydetails=new CustomerPayment();
			}
			paydetails.setPersonInfo(invoice.getPersonInfo());
			paydetails.setContractCount(invoice.getContractCount());
			if(invoice.getContractStartDate()!=null){
				paydetails.setContractStartDate(invoice.getContractStartDate());
			}
			if(invoice.getContractEndDate()!=null){
				paydetails.setContractEndDate(invoice.getContractEndDate());
			}
			paydetails.setTotalSalesAmount(invoice.getTotalSalesAmount());
			paydetails.setArrayBillingDocument(invoice.getArrayBillingDocument());
			paydetails.setTotalBillingAmount(invoice.getInvoiceAmount());
			paydetails.setInvoiceAmount(invoice.getInvoiceAmount());
			paydetails.setInvoiceCount(invoice.getCount());
			paydetails.setInvoiceDate(invoice.getInvoiceDate());
			paydetails.setInvoiceType(invoice.getInvoiceType());
			paydetails.setPaymentDate(invoice.getPaymentDate());
			double invoiceamt=invoice.getInvoiceAmount();
			int payAmt=(int)(invoiceamt);
			paydetails.setPaymentAmt(payAmt);
			paydetails.setPaymentMethod(invoice.getPaymentMethod());
			paydetails.setTypeOfOrder(invoice.getTypeOfOrder());
			paydetails.setStatus(CustomerPayment.CREATED);
			paydetails.setOrderCreationDate(invoice.getOrderCreationDate());
			paydetails.setBranch(invoice.getBranch());
			paydetails.setAccountType(invoice.getAccountType());
			paydetails.setEmployee(invoice.getEmployee());
			paydetails.setCompanyId(invoice.getCompanyId());
			paydetails.setOrderCformStatus(invoice.getOrderCformStatus());
			paydetails.setOrderCformPercent(invoice.getOrderCformPercent());
			if(invoice.getPaymentMethod()!= null && !invoice.getPaymentMethod().equals("")){
				if(invoice.getPaymentMethod().equalsIgnoreCase("Cheque")){
					paydetails.setChequeIssuedBy(invoice.getPersonInfo().getFullName());
				}else{
					paydetails.setChequeIssuedBy("");
				}
			}else{
				paydetails.setChequeIssuedBy("");
			}
			if(invoice.getNumberRange()!=null){
				paydetails.setNumberRange(invoice.getNumberRange());
			}
			if(invoice.getBillingPeroidFromDate()!=null)
				paydetails.setBillingPeroidFromDate(invoice.getBillingPeroidFromDate());
			if(invoice.getBillingPeroidToDate()!=null)
				paydetails.setBillingPeroidToDate(invoice.getBillingPeroidToDate());
			if(invoice.getSegment()!=null){
				paydetails.setSegment(invoice.getSegment());
			}
			if(invoice.getRateContractServiceId()!=0){
				paydetails.setRateContractServiceId(invoice.getRateContractServiceId());
			}
			if(invoice.getQuantity() > 0){
				paydetails.setQuantity(invoice.getQuantity());
			}
			if(invoice.getUom()!=null){
				paydetails.setUom(invoice.getUom());
			}
			if(invoice.getRefNumber()!=null){
				paydetails.setRefNumber(invoice.getRefNumber());
			}
			paydetails.setNetPay(0);
			paydetails.setPaymentReceived(0);
			
			async.save(paydetails, new AsyncCallback<ReturnFromServer>( ) {
				@Override
				public void onFailure(Throwable caught) {
					
				}
				@Override
				public void onSuccess(ReturnFromServer result) {
					
				}
			});
			
		}
		
		/**
		 * Date 22/2/2018 
		 * By Jayshree 
		 * Des.Add New Button View tax invoice Button
		 */
		
		
		private void viewTaxInvoiceDocuments() {
			final MyQuerry querry=new MyQuerry();
			Vector<Filter> temp=new Vector<Filter>();
			Filter filter=null;
			
			filter=new Filter();
			filter.setQuerryString("count");
			filter.setIntValue(model.getProformaCount());
			temp.add(filter);
			
			querry.setFilters(temp);
			querry.setQuerryObject(new VendorInvoice());
			form.showWaitSymbol();
			service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>(){

				@Override
				public void onFailure(Throwable caught) {
					form.hideWaitSymbol();
				}

				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					form.hideWaitSymbol();
					if(result.size()==0){
						form.showDialogMessage("No invoice document found. ");
						return;
					}


				
					if(result.size()==1){
						final VendorInvoice billDocument=(VendorInvoice) result.get(0);
						final VendorInvoiceDetailsForm form=VendorInvoiceDetailsPresenter.initalize();
						Timer timer=new Timer() {
							
							@Override
							public void run() {
								form.updateView(billDocument);
								form.setToViewState();
								
								// Date 02-04-2018 By vijay
								form.setAppHeaderBarAsPerStatus();
								
							}
						};
						timer.schedule(1000);
					}
					else{
						VendorInvoiceListPresenter.initalize(querry);//should be changed for vendor list
					}
				}
				
			});
		}

		@Override
		public void onChange(ChangeEvent event) {
			if(event.getSource().equals(form.getObjPaymentMode())){
				form.setBankSelectedDetails();
			}
			/** date 24.10.2018 added by komal for tds **/
			if(event.getSource()== form.getDbtdsValue())
			{
				System.out.println("tds amt ="+form.getDbtdsValue().getValue());
				double totalValue = 0 , roundOff = 0 , total = 0;;
				if(form.dbtotalAmtIncludingTax.getValue()!=null){
					totalValue = form.dbtotalAmtIncludingTax.getValue();
				}
				if(form.tbroundOffAmt.getValue()!=null && !form.tbroundOffAmt.getValue().equals("")){
					roundOff = Double.parseDouble(form.tbroundOffAmt.getValue());
				}
				total = Math.round(totalValue + roundOff);
					if(form.getDbtdsValue().getValue()!=null && form.getDbtdsValue().getValue()!=0)
					{
						double tds = form.getDbtdsValue().getValue();
						form.netPayable.setValue(total - tds);					
					}else{
						form.netPayable.setValue(total);
					}
								
				}
		}

		//End By Jayshree
		/** Date 13.12.2018 added by komal for update accounting interface **/
		private void updateAccountingInterface(){
			GeneralServiceAsync generalAsyn = GWT.create(GeneralService.class);
			generalAsyn.updateAccountingInterface(model, new AsyncCallback<String>() {
				
				@Override
				public void onSuccess(String result) {
					// TODO Auto-generated method stub
					form.showDialogMessage(result);
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					
				}
			});
		}
		
		
		private void setEmailPopUpData() {
			form.showWaitSymbol();
			String branchName = form.olbbranch.getValue();
			String fromEmailId = AppUtility.getFromEmailAddress(branchName,model.getEmployee());
			List<ContactPersonIdentification> vendorDetailslist = AppUtility.getvenodrEmailId(model.getPersonInfo().getCount());
			System.out.println("vendorDetailslist "+vendorDetailslist);
			label : AppUtility.loadContactPersonlist(emailpopup.olbContactlistEmailId,"Vendor",-1,"","",vendorDetailslist);
			Screen screenName=(Screen) AppMemory.getAppMemory().currentScreen;
			Console.log("screenName "+screenName);
			AppUtility.makeLiveSmsTemplateConfig(emailpopup.oblTemplateName,AppConstants.ACCOUNTMODULE.trim(),screenName);
			emailpopup.taFromEmailId.setValue(fromEmailId);
			emailpopup.smodel = model;
			form.hideWaitSymbol();
		
			
		}
}
