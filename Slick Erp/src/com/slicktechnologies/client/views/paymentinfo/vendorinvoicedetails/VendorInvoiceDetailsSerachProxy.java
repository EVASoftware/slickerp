package com.slicktechnologies.client.views.paymentinfo.vendorinvoicedetails;

import java.util.Vector;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.VendorInvoice;

public class VendorInvoiceDetailsSerachProxy extends SearchPopUpScreen<VendorInvoice> implements SelectionHandler<Suggestion> , ChangeHandler{
	
	public IntegerBox ibinvoiceid;
	public IntegerBox ibcontractid;
	public PersonInfoComposite personInfo;
	public PersonInfoComposite vendorInfo;
	public DateComparator dateComparator;
	public ObjectListBox<ConfigCategory> olbbillingcategory;
	public ObjectListBox<Type> olbbillingtype;
	public ObjectListBox<Config> olbbillinggroup;
	public ListBox lbstatus,lbinvoicetype;
	public ObjectListBox<Employee> olbApproverName;
	public ListBox lbaccountType;
	
	//*************rohan added here for search by branch ********8888
	public ObjectListBox<Branch> olbBranch;
	
	//**************changes ends here ***************
	
	/*
	 * nidhi
	 * 30-06-2017
	 *  customer category search
	 */
	public ObjectListBox<ConfigCategory> olbCustomerCategory;
	public TextBox tbSegment;
	/*
	 * end
	 */
	
	
	/*
	 *  nidhi
	 *  17-07-2017
	 *  for rate contract service id search
	 */
	public IntegerBox ibrateContractServiceId;
	
	/*
	 *   end
	 */
	
	/**
	 * Date : 04-08-2017 By ANIL
	 */
	ObjectListBox<Config> olbinvoicegroup;
	/**
	 * ENd
	 */
	
	/**03-10-2017 sagar sore [ adding Number range field in Search pop up ]***/
	public ObjectListBox<Config> olbcNumberRange;
	
	/**
	 * Date : 18-11-2017 BY ANIL
	 */
	TextBox tbReferenceNumber;
	
	/** date 13.03.2018 added by komal for nbhc **/
	ObjectListBox<ConfigCategory> olbInvoiceCategory;
	ObjectListBox<Type> olbInvoiceConfigType;
    /**
     * end komal
     */
	
	public VendorInvoiceDetailsSerachProxy() {
		super();
		createGui();
	}
	
	public VendorInvoiceDetailsSerachProxy(boolean b) {
		super(b);
		createGui();
	}
	
	public void initWidget()
	{
		ibinvoiceid=new IntegerBox();
		ibcontractid=new IntegerBox();
		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new Customer());
		personInfo=new PersonInfoComposite(querry,false);
		
		//***********rohan changes here *******************
		
		olbBranch= new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbBranch);
		
		//*****************changes ends here ************
		
		MyQuerry vendorquerry=new MyQuerry();
		vendorquerry.setQuerryObject(new Vendor());
		vendorInfo=new PersonInfoComposite(vendorquerry,false);
		
		
		dateComparator=new DateComparator("invoiceDate",new VendorInvoice());
		lbstatus= new ListBox();
		AppUtility.setStatusListBox(lbstatus,Invoice.getStatusList());
		lbinvoicetype=new ListBox();
		lbinvoicetype.addItem("SELECT");
		lbinvoicetype.addItem(AppConstants.CREATEPROFORMAINVOICE);
		lbinvoicetype.addItem(AppConstants.CREATETAXINVOICE);
		olbApproverName=new ObjectListBox<Employee>();
		AppUtility.makeSalesPersonListBoxLive(olbApproverName);
		
		lbaccountType=new ListBox();
		lbaccountType.addItem("SELECT");
		lbaccountType.addItem(AppConstants.BILLINGACCOUNTTYPEAP);
		lbaccountType.addItem(AppConstants.BILLINGACCOUNTTYPEAR);
		
		
		personInfo.getId().addSelectionHandler(this);
		vendorInfo.getId().addSelectionHandler(this);
		
		personInfo.getCustomerId().getHeaderLabel().setText("Customer ID");
		personInfo.getCustomerName().getHeaderLabel().setText("Customer Name");
		personInfo.getCustomerCell().getHeaderLabel().setText("Customer Cell");
		
		vendorInfo.getCustomerId().getHeaderLabel().setText("Vendor ID");
		vendorInfo.getCustomerName().getHeaderLabel().setText("Vendor Name");
		vendorInfo.getCustomerCell().getHeaderLabel().setText("Vendor Cell");
		
		/*
		 * 	nidhi
		 *  30-06-2017
		 *  for search
		 */		
		olbCustomerCategory= new ObjectListBox<ConfigCategory>();
//		AppUtility.makeTypeListBoxLive(olbCustomerCategory, Screen.CONTRACTTYPE);
		AppUtility.MakeLiveCategoryConfig(olbCustomerCategory, Screen.CUSTOMERCATEGORY);
				
		tbSegment = new TextBox();
		/*   end  */
		
		
		/*  nidhi  */
		ibrateContractServiceId = new IntegerBox();
		/*  */
		
		
		olbinvoicegroup=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbinvoicegroup, Screen.INVOICEGROUP);
		
		/**03-10-2017 sagar sore[ olbcNumberRange initialization, Displaying values entered from process configuration]***/
		olbcNumberRange= new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbcNumberRange,Screen.NUMBERRANGE);
		
		tbReferenceNumber=new TextBox();
		
		/** Date 13.03.2018 added by komal for nbhc **/
		olbInvoiceCategory=new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(olbInvoiceCategory, Screen.INVOICECATEGORY);
		olbInvoiceCategory.addChangeHandler(this);
		olbInvoiceConfigType=new ObjectListBox<Type>();
		AppUtility.makeTypeListBoxLive(olbInvoiceConfigType, Screen.INVOICECONFIGTYPE);
        /** 
         * end komal
         */
	}
	
	public void createScreen()
	{
		initWidget();
		FormFieldBuilder builder;
		builder = new FormFieldBuilder("Invoice ID",ibinvoiceid);
		FormField fibinvoiceid= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("",personInfo);
		FormField fPersonInfo= builder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		builder = new FormFieldBuilder("",vendorInfo);
		FormField fvendorInfo= builder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		builder = new FormFieldBuilder("Order ID",ibcontractid);
		FormField fibcontractid= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("From Date (Invoice Date)",dateComparator.getFromDate());
		FormField fdateComparatorfrom= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("To Date (Invoice Date)",dateComparator.getToDate());
		FormField fdateComparatorto= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Invoice Document Type",lbinvoicetype);
		FormField flbinvoicetype= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Status",lbstatus);
		FormField flbstatus=builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Approver Name",olbApproverName);
		FormField folbApproverName= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Account Type",lbaccountType);
		FormField flbaccountType= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Branch",olbBranch);
		FormField folbBranch= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/*
		 * 	nidhi
		 * 	30-6-2017
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "OnlyForNBHC"))
		{
			builder = new FormFieldBuilder("Segment Type",olbCustomerCategory);
			
		}
		else{
			builder = new FormFieldBuilder("Segment Type",tbSegment);
		}
		FormField ftbSagment= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/*
		 *  nidhi
		 *  17-07-2017
		 *  Service Id serach
		 */
		
		builder = new FormFieldBuilder("Service ID",ibrateContractServiceId);
		FormField fibserviceid= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		/*
		 *  end
		 */
		
		builder = new FormFieldBuilder("Invoice Group",olbinvoicegroup);
		FormField folbinvoicegroup=builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/**03-10-2017 sagar sore[ adding search by number range]**/
		FormField folbcNumberRange;	
			builder = new FormFieldBuilder("Number Range",olbcNumberRange);
			folbcNumberRange= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			
		/**
		 * Date : 18-11-2017 BY ANIL
		 */
		builder = new FormFieldBuilder("Reference Number",tbReferenceNumber);
		FormField ftbReferenceNumber= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		/**
		 * Date 25-01-2018 By Vijay 
		 * differentiated date combination search and other filter search
		 */
		builder = new FormFieldBuilder();
		FormField fgroupingDateFilterInformation=builder.setlabel("Date Filter Combination").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();

		builder = new FormFieldBuilder();
		FormField fgroupingOtherFilterInformation=builder.setlabel("Other Filter Combination").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		/**
		 * ends here
		 */
		/** date 13.03.2018 added by komal for nbhc**/
		builder = new FormFieldBuilder("Invoice Category",olbInvoiceCategory);
		FormField folbInvoiceCategory=builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Invoice Type",olbInvoiceConfigType);
		FormField folbInvoiceConfigType=builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		this.fields=new FormField[][]{
				/**03-10-2017 sagar sore [folbcNumberRange added and order of fields changed]***/
				/**old code**/
//				{fdateComparatorfrom,fdateComparatorto,folbBranch},
//				{fibinvoiceid,flbinvoicetype,fibcontractid},
//				{flbstatus,folbApproverName,flbaccountType},
//				{fPersonInfo},
//				{fvendorInfo},
//				{ftbSagment,fibserviceid,folbinvoicegroup}
				 /**updated order of fields**/	/** date 13.03.2018 order changed by komal **/		
				{fgroupingDateFilterInformation},
				{fdateComparatorfrom,fdateComparatorto,folbBranch,flbstatus},
			//	{flbaccountType},
				{fgroupingOtherFilterInformation},
				{fibinvoiceid,folbinvoicegroup,folbInvoiceCategory , folbInvoiceConfigType},
				{flbinvoicetype ,folbcNumberRange , folbApproverName,fibcontractid},
				{ftbReferenceNumber , ftbSagment,fibserviceid },
				{fPersonInfo},
				{fvendorInfo}
		};
	}
	

	@Override
	public MyQuerry getQuerry() {
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		
		if(dateComparator.getValue()!=null){
			filtervec.addAll(dateComparator.getValue());
		}
		
		if(ibinvoiceid.getValue()!=null){
			temp=new Filter();
			temp.setIntValue(ibinvoiceid.getValue());
			filtervec.add(temp);
			temp.setQuerryString("count");
			filtervec.add(temp);
		}
		
		if(ibcontractid.getValue()!=null){
			temp=new Filter();
			temp.setIntValue(ibcontractid.getValue());
			filtervec.add(temp);
			temp.setQuerryString("contractCount");
			filtervec.add(temp);
		}
		
		//if(lbaccountType.getSelectedIndex()!=0){
		/** date 25.6.2018 added by komal for - by default loading of AR **/
		temp=new Filter();
		temp.setStringValue(AppConstants.BILLINGACCOUNTTYPEAP);
		temp.setQuerryString("accountType");
		filtervec.add(temp);
		//}
		
		if(lbinvoicetype.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(lbinvoicetype.getItemText(lbinvoicetype.getSelectedIndex()));
			filtervec.add(temp);
			temp.setQuerryString("invoiceType");
			filtervec.add(temp);
		}
		
		if(personInfo.getIdValue()!=-1)
		  {
		  temp=new Filter();
		  temp.setIntValue(personInfo.getIdValue());
		  temp.setQuerryString("personInfo.count");
		  filtervec.add(temp);
		  }
		  
//		  if(!(personInfo.getFullNameValue().equals("")))
//		  {
//		  temp=new Filter();
//		  temp.setStringValue(personInfo.getFullNameValue());
//		  temp.setQuerryString("personInfo.fullName");
//		  filtervec.add(temp);
//		  }
//		  if(personInfo.getCellValue()!=-1l)
//		  {
//		  temp=new Filter();
//		  temp.setLongValue(personInfo.getCellValue());
//		  temp.setQuerryString("personInfo.cellNumber");
//		  filtervec.add(temp);
//		  }
		  
		  if(vendorInfo.getIdValue()!=-1)
		  {
		  temp=new Filter();
		  temp.setIntValue(vendorInfo.getIdValue());
		  temp.setQuerryString("personInfo.count");
		  filtervec.add(temp);
		  }
		  
		  
		  //Ashwini Patil Date:3-10-2022 commented below code as if name or cell number changes old records are not getting searched. So search will be based only on id now.
//		  if(!(vendorInfo.getFullNameValue().equals("")))
//		  {
//		  temp=new Filter();
//		  temp.setStringValue(vendorInfo.getFullNameValue());
//		  temp.setQuerryString("personInfo.fullName");
//		  filtervec.add(temp);
//		  }
//		  if(vendorInfo.getCellValue()!=-1l)
//		  {
//		  temp=new Filter();
//		  temp.setLongValue(vendorInfo.getCellValue());
//		  temp.setQuerryString("personInfo.cellNumber");
//		  filtervec.add(temp);
//		  }
		  
		  
		  
		  if(lbstatus.getSelectedIndex()!=0){
				temp=new Filter();
				temp.setStringValue(lbstatus.getItemText(lbstatus.getSelectedIndex()));
				filtervec.add(temp);
				temp.setQuerryString("status");
				filtervec.add(temp);
			}
		  if(olbApproverName.getSelectedIndex()!=0){
				temp=new Filter();temp.setStringValue(olbApproverName.getValue().trim());
				temp.setQuerryString("approverName");
				filtervec.add(temp);
			}
		
		  
			//   rohan added this for branch level restriction
		
				if(olbBranch.getSelectedIndex()!=0){
					temp=new Filter();
					temp.setStringValue(olbBranch.getValue().trim());
					temp.setQuerryString("branch");
					filtervec.add(temp);
				}
				
				/*
				 *  nidhi 
				 *  1-07-2017
				 *  
				 */
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "OnlyForNBHC"))
				{
					if(olbCustomerCategory.getSelectedIndex()!=0){
						temp=new Filter();
						temp.setStringValue(olbCustomerCategory.getValue().trim());
						temp.setQuerryString("segment");
						filtervec.add(temp);
					}
				}else{
						
					if(!tbSegment.getValue().equals("")){
						
						temp=new Filter();
						temp.setStringValue(tbSegment.getValue().trim());
						temp.setQuerryString("segment");
						filtervec.add(temp);
					}
				}
				/*
				 *  end
				 */
				
				/*
				 *  nidhi
				 *   17-07-2017
				 *    service ID serach
				 */
				
				if(ibrateContractServiceId.getValue()!=null){
					temp=new Filter();
					temp.setIntValue(ibrateContractServiceId.getValue());
					temp.setQuerryString("rateContractServiceId");
					filtervec.add(temp);
				}	
				/*
				 *  end
				 */
				
				
				if(olbinvoicegroup.getSelectedIndex()!=0){
					temp=new Filter();
					temp.setStringValue(olbinvoicegroup.getItemText(olbinvoicegroup.getSelectedIndex()));
					temp.setQuerryString("invoiceGroup");
					filtervec.add(temp);
				}
				/**03-10-2017 sagar sore[ filter for search by number range i.e. Billing and Non-billing]**/
				if(olbcNumberRange.getSelectedIndex()!=0){
					temp=new Filter();
					temp.setStringValue(olbcNumberRange.getValue().trim());
					temp.setQuerryString("numberRange");
					filtervec.add(temp);
				}
				
		/**
		 * Date : 18-11-2017 BY ANIL
		 */
		if(tbReferenceNumber.getValue()!=null&&!tbReferenceNumber.getValue().equals("")){
			temp=new Filter();
			temp.setStringValue(tbReferenceNumber.getValue());
			temp.setQuerryString("refNumber");
			filtervec.add(temp);
		}
		
		/** date 13.03.2018 added by komal for nbhc**/
		if(olbInvoiceCategory.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(olbInvoiceCategory.getItemText(olbInvoiceCategory.getSelectedIndex()));
			temp.setQuerryString("invoiceCategory");
			filtervec.add(temp);
		}
		if(olbInvoiceConfigType.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(olbInvoiceConfigType.getItemText(olbInvoiceConfigType.getSelectedIndex()));
			temp.setQuerryString("invoiceConfigType");
			filtervec.add(temp);
		}
		/**
		 * END KOMAL
		 */
		MyQuerry querry= new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new VendorInvoice());
		return querry;
	}
	
	
	
	@Override
	public void onSelection(SelectionEvent<Suggestion> event) 
	{
		if(event.getSource().equals(personInfo.getId())||event.getSource().equals(personInfo.getName())||event.getSource().equals(personInfo.getPhone()))
		{
			if(!vendorInfo.getId().getValue().equals("")||!vendorInfo.getName().getValue().equals("")||!vendorInfo.getPhone().getValue().equals(""))
			{
				vendorInfo.clear();
			}
		}
		
		if(event.getSource().equals(vendorInfo.getId())||event.getSource().equals(vendorInfo.getName())||event.getSource().equals(vendorInfo.getPhone()))
		{
			if(!personInfo.getId().getValue().equals("")&&!personInfo.getName().getValue().equals("")&&!personInfo.getPhone().getValue().equals(""))
			{
				personInfo.clear();
			}
		}
	}

	@Override
	public boolean validate() {
		if(LoginPresenter.branchRestrictionFlag)
		{
		if(olbBranch.getSelectedIndex()==0)
		{
			showDialogMessage("Select Branch");
			return false;
		}
		}
		
		/**
		 * Date 25-01-2018 By Vijay 
		 * for Date combination validation
		 */
		String msg = "Wrong filter selection with From Date and To Date ! Please select proper filters with Date";
		if(dateComparator.getFromDate().getValue()!=null || dateComparator.getToDate().getValue()!=null ){
			if(ibinvoiceid.getValue()!=null || ibcontractid.getValue()!=null ||
				lbinvoicetype.getSelectedIndex()!=0 || vendorInfo.getIdValue()!=-1 || vendorInfo.getIdValue()!=-1 ||	
			    !(vendorInfo.getFullNameValue().equals("")) || !(vendorInfo.getFullNameValue().equals("")) ||
		       olbApproverName.getSelectedIndex()!=0 || olbCustomerCategory.getSelectedIndex()!=0 ||
  			   !tbSegment.getValue().equals("") || ibrateContractServiceId.getValue()!=null ||olbinvoicegroup.getSelectedIndex()!=0 ||
			   olbcNumberRange.getSelectedIndex()!=0 || tbReferenceNumber.getValue()!=null&&!tbReferenceNumber.getValue().equals("") ){
				showDialogMessage(msg);
				return false;
			}
		}
		/**
		 * ends here
		 */
		return true;
	}

	/** date 13.03.2018 added by komal for nbhc ***/
	@Override
	public void onChange(ChangeEvent event) {
		// TODO Auto-generated method stub
		if(event.getSource().equals(olbInvoiceCategory))
		{
			if(olbInvoiceCategory.getSelectedIndex()!=0){
				ConfigCategory cat=olbInvoiceCategory.getSelectedItem();
				if(cat!=null){
					AppUtility.makeLiveTypeDropDown(olbInvoiceConfigType, cat.getCategoryName(), cat.getCategoryCode(), cat.getInternalType());
				}
			}
		}
	}
	/**@Sheetal:23-03-2022,Increasing size of Vendor Invoice Details search popup for Samyak Exim**/
	@Override
	public void applyStyle() {
//		super.applyStyle();
		content.getElement().setId("formcontent");
		form.getElement().setId("form");
		horizontal.getElement().addClassName("centering");
		dwnload.getElement().setId("addbutton");
		golbl.getElement().setId("addbutton");
		
		popup.getElement().setId("searchpopup");
		/**
		 *  nidhi
		 *  5-08-2017
		 *  for make screen responsive class 
		 *  simpleERP.css
		 */
		popup.getElement().addClassName("searchPopupofVendorInvoiceDetails");
		/**
		 * end
		 */
		supertable.getTable().setHeight("400px");
		scrollpanel.setHeight("500px");
		popup.center();
	}
	
	


}
