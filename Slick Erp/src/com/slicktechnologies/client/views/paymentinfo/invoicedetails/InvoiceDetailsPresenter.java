package com.slicktechnologies.client.views.paymentinfo.invoicedetails;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.approvalutility.ApprovableFormScreenPresenter;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.reusabledata.ConditionDialogBox;
import com.slicktechnologies.client.services.CommonService;
import com.slicktechnologies.client.services.CommonServiceAsync;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.EmailService;
import com.slicktechnologies.client.services.EmailServiceAsync;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.services.TaxInvoiceService;
import com.slicktechnologies.client.services.TaxInvoiceServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.ApproverFactory;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.accountinginterface.IntegrateSyncService;
import com.slicktechnologies.client.views.accountinginterface.IntegrateSyncServiceAsync;
import com.slicktechnologies.client.views.cnc.CNCForm;
import com.slicktechnologies.client.views.cnc.CNCPresenter;
import com.slicktechnologies.client.views.contract.ContractForm;
import com.slicktechnologies.client.views.contract.ContractPresenter;
import com.slicktechnologies.client.views.documentcancellation.DocumentCancellationPopUp;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.BillingDetailsForm;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.BillingDetailsPresenter;
import com.slicktechnologies.client.views.paymentinfo.billinglist.BillingListPresenter;
import com.slicktechnologies.client.views.paymentinfo.invoicelist.InvoiceListPresenter;
import com.slicktechnologies.client.views.paymentinfo.paymentdetails.PaymentDetailsForm;
import com.slicktechnologies.client.views.paymentinfo.paymentdetails.PaymentDetailsPresenter;
import com.slicktechnologies.client.views.paymentinfo.paymentlist.PaymentListPresenter;
import com.slicktechnologies.client.views.popups.NewEmailPopUp;
import com.slicktechnologies.client.views.popups.ResetAppidPopup;
import com.slicktechnologies.client.views.popups.SMSPopUp;
import com.slicktechnologies.client.views.popups.UploadFilePopup;
import com.slicktechnologies.client.views.purchase.purchaseorder.PurchaseOrderForm;
import com.slicktechnologies.client.views.purchase.purchaseorder.PurchaseOrderPresenter;
import com.slicktechnologies.client.views.purchase.servicepo.ServicePoForm;
import com.slicktechnologies.client.views.purchase.servicepo.ServicePoPresenter;
import com.slicktechnologies.client.views.salesorder.SalesOrderForm;
import com.slicktechnologies.client.views.salesorder.SalesOrderPresenter;
import com.slicktechnologies.server.addhocdownload.XlsxWriter;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.OtherTaxCharges;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.Sales;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessprocesslayer.ConcreteBusinessProcess;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessprocesslayer.ServicePo;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cnc.CNC;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.role.ScreenAuthorization;
import com.slicktechnologies.shared.common.salesorder.OtherCharges;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesprocess.AnnexureType;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.BillingDocumentDetails;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.SalesAnnexureType;
import com.slicktechnologies.shared.common.salesprocess.SalesOrderProductLineItem;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;
import com.slicktechnologies.shared.common.tallyaccounts.AccountingInterface;

public class InvoiceDetailsPresenter extends ApprovableFormScreenPresenter<Invoice> implements RowCountChangeEvent.Handler, ChangeHandler{
	
	public static InvoiceDetailsForm form;
	final static GenricServiceAsync async=GWT.create(GenricService.class);
	final CsvServiceAsync csvservice=GWT.create(CsvService.class);
	EmailServiceAsync emailService=GWT.create(EmailService.class);
	
	DocumentCancellationPopUp popup = new DocumentCancellationPopUp();
	PopupPanel panel ;
	int cnt=0;
	int pestoIndia=0;
	int vCare=0;
	int omPestInvoice=0;
	int reddysPestControl=0;
	int counter=0;
	int hygeia=0;
	int pecopp = 0;
	int orionPestControl=0;
	int newInvoiceFormat = 0;
	int tabularGstServiceInvoice=0;//Ashwini Patil Date: 1-11-2022
	boolean newinvoiceformatflag = false;
	//************changes changes here ****************
		ConditionDialogBox conditionPopup=new ConditionDialogBox("Do you want to print on preprinted Stationery",AppConstants.YES,AppConstants.NO);
		
		/*
		 * Added by Rahul
		 */
		ReferenceDocDetailsPopUp  refDocPopUp=new ReferenceDocDetailsPopUp();
		PopupPanel refDocPanel;	
	//****************ends here ***********	
		//By Priyanka
		CRMTallySearchPopUp  crmTallyPopUp;
	
     GeneralServiceAsync generalAsyn = GWT.create(GeneralService.class);	
     
     /**
      * @author Anil @since 26-03-2021
      * Adding annexure popup for sunrise raised by Rahul Tiwari
      */
     AnnexureTypePopup annexurePopup=new AnnexureTypePopup();
	 
     NewEmailPopUp emailpopup = new NewEmailPopUp();
 	SMSPopUp smspopup = new SMSPopUp();
 	
 	/**
 	 * @author Anil @since 15-11-2021
 	 */
 	 SalesAnnexureTypePopup salesAnnexurePopup=new SalesAnnexureTypePopup();
 	 
 	 AppUtility apputility = new AppUtility();
 	 
 	ConditionDialogBox conditionPopupforInvoiceReset=new ConditionDialogBox("This action will delete all the payment document of this invoice and reset Invoice to Created state. Do you want to continue?",AppConstants.YES,AppConstants.NO);
	

 	ConditionDialogBox conditionPopupForEinvoiceCancellation=new ConditionDialogBox("eInvoice has been generated for this document. Do you want to cancel both Invoice and eInvoice?",AppConstants.YES,AppConstants.NO);
	
    CommonServiceAsync commonservice = GWT.create(CommonService.class);
    
    DescriptionPopup descriptionPopup=new DescriptionPopup(); //Ashwini Patil Date:21-08-2023
    
    ConditionDialogBox conditionPopupForOnlyEvaInvoiceCancellation=new ConditionDialogBox("The allowed einvoice cancellation time limit is crossed, you cannot cancel the IRN. Do you want to cancel only EVA invoice?",AppConstants.YES,AppConstants.NO);//Ashwini Patil Date:21-08-2023
	
    TaxInvoiceServiceAsync taxAsync = GWT.create(TaxInvoiceService.class);	
    UploadFilePopup uploadFilePopup=new UploadFilePopup();
	PopupPanel uploadFilePopupPanel=new PopupPanel();

	public InvoiceDetailsPresenter(FormScreen<Invoice> view, Invoice model) {
		super(view, model);
		form=(InvoiceDetailsForm) view;
		form.setPresenter(this);
		popup.getBtnOk().addClickHandler(this);
		popup.getBtnCancel().addClickHandler(this);
		conditionPopupforInvoiceReset.getBtnOne().addClickHandler(this);
		conditionPopupforInvoiceReset.getBtnTwo().addClickHandler(this);
		conditionPopupForEinvoiceCancellation.getBtnOne().addClickHandler(this);
		conditionPopupForEinvoiceCancellation.getBtnTwo().addClickHandler(this);
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.INVOICEDETAILS,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
		
		
		
		//**********rohan ************************
				conditionPopup.getBtnOne().addClickHandler(this);
				conditionPopup.getBtnTwo().addClickHandler(this);
				//Date 04-09-2017 commented by vijay handled in form with new code amt roundoff discount
//				form.getDiscount().addValueChangeHandler(this);
				
				form.getSalesProductTable().getTable().addRowCountChangeHandler(this);
				form.getInvoiceChargesTable().getTable().addRowCountChangeHandler(this);
				
				form.btReferenceDetails.addClickHandler(this);
				refDocPopUp.getBtnOk().addClickHandler(this);
				form.getObjPaymentMode().addChangeHandler(this);
		/**
		 * Date : 21-09-2017 BY ANIL
		 * Adding row count change handler on other charges table
		 */
		form.tblOtherCharges.getTable().addRowCountChangeHandler(this);
		/**
		 * End
		 */
		/** date 27.10.2018 added by komal for tds **/
		form.cbtdsApplicable.addClickHandler(this);
		form.dbtdsValue.addChangeHandler(this);
		
		annexurePopup.getLblOk().addClickHandler(this);
		annexurePopup.getLblCancel().addClickHandler(this);
		
		salesAnnexurePopup.getLblOk().addClickHandler(this);
		salesAnnexurePopup.getLblCancel().addClickHandler(this);
		
		form.getInvoiceTaxTable().getTable().addRowCountChangeHandler(this);
		form.btnCopyContractDescription.addClickHandler(this);
		descriptionPopup.getLblOk().addClickHandler(this);
		descriptionPopup.getLblCancel().addClickHandler(this);
		conditionPopupForOnlyEvaInvoiceCancellation.getBtnOne().addClickHandler(this);
		conditionPopupForOnlyEvaInvoiceCancellation.getBtnTwo().addClickHandler(this);
		uploadFilePopup.getBtnOk().addClickHandler(this);
		uploadFilePopup.getBtnCancel().addClickHandler(this);
		
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel label=(InlineLabel) e.getSource();
		
		String text=label.getText().trim();
		if(text.equals(ManageApprovals.SUBMIT)){
			/*** Date 13-10-2018 By Vijay For dont allow user to click twice ***/
			form.showWaitSymbol();
			/**
			 * Date : 09-11-2017 By ANIL
			 */
			/**
			 *  nidhi
			 *  03-04-2018
			 *  for check unit and uom validation
			 */
			boolean unitFlag = form.validateunitandUOM();
			if(isRefNumIsMandatory()==false || unitFlag == false){
				form.hideWaitSymbol();//vijay
				return;
			}
			
			/**
			 * Date 12-10-2018 
			 * Developer : Vijay
			 * Des : if multiple billing mergred into billing document and its billing documents branch is diffrent then branch will blank
			 * then must add branch in invoice so before self approve branch validation 
			 */
			if(form.getOlbbranch().getSelectedIndex()==0){
				form.showDialogMessage("Please add branch!");
				form.hideWaitSymbol();
				return;
			}
			/**
			 * ends here
			 */
			
			if(form.getDoinvoiceamount().getValue()==0){ // //Ashwini Patil Date:12-03-2022 Description: Nitin sir given requirement- Do not create payment document if invoice amount zero and show this popup
				form.showDialogMessage("Since invoice amount is zero, no payment document will be created");

			}
			form.hideWaitSymbol();//Vijay
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "EnableInvoiceIntegrationWithZohoBooks")) {
				taxAsync.validateInvoiceForZohoIntegration(model, new AsyncCallback<String>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void onSuccess(String result) {
						if(result!=null) {
							if(result.equalsIgnoreCase("Success"))
								form.getManageapproval().reactToSubmit();
							else
								form.showDialogMessage(result);						
						}else
							form.showDialogMessage("Failed");	
					}
				});
				
			}else
				form.getManageapproval().reactToSubmit();
			
		}
		
		if(text.equals(ManageApprovals.APPROVALREQUEST)){
			/*** Date 13-10-2018 By Vijay For dont allow user to click twice ***/
			form.showWaitSymbol();
			/**
			 * Date : 09-11-2017 By ANIL
			 */
			/**
			 *  nidhi
			 *  03-04-2018
			 *  for check unit and uom validation
			 */
			boolean unitFlag = form.validateunitandUOM();
			if(isRefNumIsMandatory()==false || unitFlag == false){
				form.hideWaitSymbol();//Vijay
				return;
			}
			
			/**
			 * Date 12-10-2018 
			 * Developer : Vijay
			 * Des : if multiple billing mergred into billing document and its billing documents branch is diffrent then branch will blank
			 * then must add branch in invoice so before self approve branch validation 
			 */
			if(form.getOlbbranch().getSelectedIndex()==0){
				form.showDialogMessage("Please add branch!");
				form.hideWaitSymbol();
				return;
			}
			/**
			 * ends here
			 */
			
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "OnlyForNBHC"))
			{
				MyQuerry querry = AppUtility.checkRefNUmberInCustomerByCustomerId(model.getCustomerId(),model.getCompanyId());

				service.getSearchResult(querry,	new AsyncCallback<ArrayList<SuperModel>>() {
					
					@Override
					public void onFailure(Throwable caught) {
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						String refNumber="";
						if (result.size() == 0) {

						}
						else {

							for (SuperModel model : result) {
								Customer customer = (Customer) model;
								 refNumber=customer.getRefrNumber1();
							}
						}
						
						if(refNumber.equals("")){
							form.showDialogMessage("Please fill Customer reference number 1 in customer screen..!");
							form.hideWaitSymbol();
						}
						else{
							reactOnApprovalVal();
						}
					}
				});
				}
			else{
				reactOnApprovalVal();
			}
		}
		if(text.equals(ManageApprovals.CANCELAPPROVALREQUEST)){
			reactOnCancelApproval();
		}
		if(text.equals("Email")){
			reactOnEmail();
		}
		
		if(text.equals("Tax Invoice")){
			
			/**
			 * @author Anil
			 * @since 26-05-2020
			 * if model object has null value then we return
			 * if model is null then process created additional invoice
			 * @author Anil
			 * @since 10-10-2020
			 * As observed we have initialized model thats why its default value is not null
			 * thats why also checking invoice count
			 */
			if(model==null||model.getCount()==0){
				return;
			}
			/**
			 * @author Anil , Date : 20-09-2019
			 * Multiple invoices created against single bill
			 * this happens when we navigate from bill to invoice and if tax invoice buttons get clicked then second invoice gets created
			 * @author Anil;
			 * @since 10-10-2020
			 * As observed duplicate invoice or multiple invoices against single bill is not created from bill screen 
			 * also observed that it is created from client side code but type of order is null so checking those conditions to avoid creating
			 * extra invoices
			 */
			if(model!=null&&model.getInvoiceType().equals(null)){
				return;
			}
			if(model!=null&&(model.getTypeOfOrder().equals(null)||model.getTypeOfOrder()==null || model.getTypeOfOrder().equals(""))){
				return;
			}
			if(model!=null&&(model.getInvoiceType().equals("Tax Invoice")||model.getInvoiceType().equals(""))){
				return;
			}
			createNewTaxInvoice();
			changeBillingStatusToTInvoiced();
		}
		
		if(text.equals(AppConstants.CANCELINVOICE)){
			Console.log("cancel invoice clicked");
			
			/*
			 * Ashwini Patil 
			 * Date:8-10-2024
			 * Ultra does not want to allow non admin user to cancel last month/old invoices after specific deadline
			 * deadline is defined in company -company info -Invoice deadline field
			 */
			if(!LoginPresenter.myUserEntity.getRole().getRoleName().equals("ADMIN")) {
				boolean validDate=AppUtility.validateInvoiceEditDate(form.getDbinvoicedate().getValue());
				Console.log("validDate="+validDate);
				if(!validDate) {
					form.showDialogMessage("You cannot cancel old Invoice now!");
					return;
				}			
			}
			
			if(model.getIrnStatus()!=null&&!model.getIrnStatus().equals("")) {
				Console.log("checking irn status");
				if(model.getIrnStatus().equals("Active")) {
					panel=new PopupPanel(true);
					panel.add(conditionPopupForEinvoiceCancellation);
					panel.setGlassEnabled(true);
					panel.show();
					panel.center();
				}else {
					popup.getPaymentDate().setValue(new Date()+"");
					popup.getPaymentID().setValue(form.getIbinvoiceid().getValue());
					popup.getPaymentStatus().setValue(form.getTbstatus().getValue());
					popup.getRemark().setValue("");
					panel=new PopupPanel(true);
					panel.add(popup);
					panel.show();
					panel.center();	
				}
			}else {
				popup.getPaymentDate().setValue(new Date()+"");
				popup.getPaymentID().setValue(form.getIbinvoiceid().getValue());
				popup.getPaymentStatus().setValue(form.getTbstatus().getValue());
				popup.getRemark().setValue("");
				panel=new PopupPanel(true);
				panel.add(popup);
				panel.show();
				panel.center();				
			}
		}
		
		/**
		 * Date : 29-07-2017 By ANIL
		 * 
		 */
		if (text.equals(AppConstants.VIEWPAYMENT)) {
			//Ashwini Patil Date:8-06-2023 to restrict access to payment as per user authorization			
			List<ScreenAuthorization> moduleValidation = null;
			boolean accessPayments=false;
			if(UserConfiguration.getRole()!=null){
				moduleValidation=UserConfiguration.getRole().getAuthorization();
			}
			for(int f=0;f<moduleValidation.size();f++)
			{
				if(moduleValidation.get(f).getScreens().trim().equals("Payment Details")||moduleValidation.get(f).getScreens().trim().equals("Payment List")||moduleValidation.get(f).getScreens().trim().equals("PAYMENTDETAILS")||moduleValidation.get(f).getScreens().trim().equals("PAYMENTLIST")){
					accessPayments=true;
					break;
				}
			}
			if(accessPayments) {
				if(form.isPopUpAppMenubar()){
					Console.log("Accounts Invoice POPUP : View Payment clicked!!");
					return;
				}
				viewPaymentDocuments();			
			}else {
				form.showDialogMessage("You are not authorized to view this screen!");
			}
			
			
		}
		
		/**
		 * Date : 10-10-2017 By ANIL
		 * 
		 */
		if (text.equals(AppConstants.VIEWBILL)) {
			if(form.isPopUpAppMenubar()){
				Console.log("Accounts Bill-Invoice POPUP : View Bill clicked!!");
				return;
			}
			viewBillingDocuments();
		}
		/**@Sheetal : 15-02-2022**/
		if(text.equals(AppConstants.VIEWORDER)){
			if(form.isPopUpAppMenubar()){
				Console.log("Accounts Invoice POPUP : View Order clicked!!");
				return;
			}
			viewOrderDocuments();
		}
		/**
		 * Date 22/2/2018 
		 * By Jayshree 
		 * Des.Add New Button View tax invoice Button
		 */
		
		if(text.equals(AppConstants.VIEWTAXINVOICE)){
			
			if(form.isPopUpAppMenubar()){
				Console.log("Accounts Bill-Invoice POPUP : View Tax Invoice clicked!!");
				return;
			}
			viewTaxInvoiceDocuments();
		}
		//End by jayshree
		/** Date 13.12.2018 added by komal for edit accounting interface **/
		if(text.equalsIgnoreCase(AppConstants.UPDATEACCOUNTINGINTERFACE)){
			updateAccountingInterface();
		}
		
		/**
		 * @author Anil @since 25-03-2021
		 */
		if(text.equalsIgnoreCase("Attendance Summary")){
			annexurePopup.setModel(model);
			annexurePopup.initializeAnnexureDetails();
		}
		
		if (text.equals(AppConstants.CRMTALLYREPORT))
		{
			crmTallyPopUp = new CRMTallySearchPopUp();
			crmTallyPopUp.showPopUp();
			crmTallyPopUp.getLblCancel().setVisible(false);
			crmTallyPopUp.getLblOk().setText("Download");
		}
		
		/**
		 * @author Anil @since 15-11-2021
		 */
		if(text.equalsIgnoreCase("Sales Order Summary")){
			salesAnnexurePopup.setModel(model);
			salesAnnexurePopup.initializeAnnexureDetails();
		}
		
		if(text.equalsIgnoreCase("Invoice Reset")){
			Console.log("Invoice Reset clicked");
			
			/*
			 * Ashwini Patil 
			 * Date:8-10-2024
			 * Ultra does not want to allow non admin user to reset last month/old invoices after specific deadline
			 * deadline is defined in company -company info -Invoice deadline field
			 */
			if(!LoginPresenter.myUserEntity.getRole().getRoleName().equals("ADMIN")) {
				boolean validDate=AppUtility.validateInvoiceEditDate(form.getDbinvoicedate().getValue());
				Console.log("validDate="+validDate);
				if(!validDate) {
					form.showDialogMessage("You cannot reset old Invoice now!");
					return;
				}			
			}
			if(model.getIRN()!=null&&!model.getIRN().equals("")){
				if(model.getIrnStatus()!=null&&model.getIrnStatus().equals("Cancelled")){
					form.showDialogMessage("Cannot reset Invoice once eInvoice is cancelled!");
				}else									
					form.showDialogMessage("Cancel e-Invoice first!");
				return;
			}

			panel=new PopupPanel(true);
			panel.add(conditionPopupforInvoiceReset);
			panel.setGlassEnabled(true);
			panel.show();
			panel.center();
		}
		
		if(text.equalsIgnoreCase("Generate e-Invoice number")){
			Console.log("Generate e-Invoice number clicked");			
			form.showWaitSymbol();
//			form.showDialogMessage("IRN generation process started!");
			IntegrateSyncServiceAsync integrateService=GWT.create(IntegrateSyncService.class);
			integrateService.getIRN(model,LoginPresenter.loggedInUser,null,false, new AsyncCallback<String>() {

				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					Console.log("Failed"+caught.getMessage());
					form.showDialogMessage("IRN generation failed");
					form.hideWaitSymbol();
				}

				@Override
				public void onSuccess(String result) {
					boolean irnsuccessFlag = false;
					// TODO Auto-generated method stub
					if(result.equalsIgnoreCase("success")) {
						Console.log("IRN generated successfully");	
						form.showDialogMessage("IRN generated successfully");	
						irnsuccessFlag = true;
					}else {
						Console.log(result);
						form.showDialogMessage(result);
					}
					updateInvoiceView(irnsuccessFlag,false,false);
					form.hideWaitSymbol();
				}
			});
		}	
		
		
		if(text.equals(AppConstants.UPDATESALESORDERINVOICE)){
			if(model.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESALES)){
				reactonUpdateSalesOrderInvoice();
			}
			else{
				form.showDialogMessage("This is applicable only for sales order invoice");
			}
		}
		
		if(text.equalsIgnoreCase(AppConstants.COPY)){
			Console.log("In copy invoice");
			reactOnCopy();
		}
		
		if(text.equalsIgnoreCase("Update Description")){
			Console.log("In Update Description");
			descriptionPopup.showPopUp();
			if(model.getComment()!=null) {
				Console.log("model.getComment()="+model.getComment());
				descriptionPopup.gettaDescription().setText(model.getComment());
			
			}else {
				Console.log("IN else of model.getComment()");
				descriptionPopup.gettaDescription().setText("");
			}
			
		}
		
		if(text.equals(AppConstants.generatePayment)) {
			final MyQuerry querry=new MyQuerry();
			Vector<Filter> temp=new Vector<Filter>();
			Filter filter=null;
			
			filter=new Filter();
			filter.setQuerryString("invoiceCount");
			filter.setIntValue(model.getCount());
			temp.add(filter);
			
			filter=new Filter();
			filter.setQuerryString("accountType");
			filter.setStringValue("AR");
			temp.add(filter);
			
			querry.setFilters(temp);
			querry.setQuerryObject(new CustomerPayment());
			
			
			form.showWaitSymbol();
			service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				@Override
				public void onFailure(Throwable caught) {
					form.hideWaitSymbol();
				}

				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					
					if(result==null||result.size()==0){
						taxAsync.createPayment(model, new AsyncCallback<String>() {
							
							@Override
							public void onSuccess(String result) {
								// TODO Auto-generated method stub
								if(result!=null&&result.equalsIgnoreCase("success")) {
									form.showDialogMessage("Payment Created successfully");
									form.hideWaitSymbol();
									return;
								}else
								{
									form.showDialogMessage("Failed to create payment.");
									form.hideWaitSymbol();
									return;
								}
							}
							
							@Override
							public void onFailure(Throwable caught) {
								// TODO Auto-generated method stub
								form.showDialogMessage("Failed to create payment.");
								form.hideWaitSymbol();
								return;
							}
						});
						
					}else {
						form.showDialogMessage("Payment already created!");
						form.hideWaitSymbol();
						return;
					}
					
				}
			});
		}
		if(text.equals(AppConstants.uploadDoc)) {
			Console.log(AppConstants.uploadDoc + "clicked");
			uploadFilePopup.clear();
			if(model.getDocUpload1()!=null)
				uploadFilePopup.getUpload().setValue(model.getDocUpload1());
			else
				uploadFilePopup.getUpload().setValue(null);
			
			uploadFilePopupPanel=new PopupPanel(true);
			uploadFilePopupPanel.add(uploadFilePopup);
			uploadFilePopupPanel.center();
			uploadFilePopupPanel.show();
		}
		if(text.equals(AppConstants.syncWithZohoBooks)) {
			taxAsync.validateInvoiceForZohoIntegration(model, new AsyncCallback<String>() {

				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void onSuccess(String result) {
					if(result!=null) {
						if(result.equalsIgnoreCase("Success")) {
							if(model.getZohoInvoiceID()!=null&&!model.getZohoInvoiceID().equals("")) {
								taxAsync.updateInvoiceInZohoBooks(model.getCount(), model.getCompanyId(),LoginPresenter.loggedInUser,new AsyncCallback<String>() {

									@Override
									public void onFailure(Throwable caught) {
										form.showDialogMessage("Failed to update invoice in zoho books. Please sync manually.");
									}

									@Override
									public void onSuccess(String result) {
										if(result!=null) {
											if(result.equalsIgnoreCase("Success"))
												form.showDialogMessage("Invoice updated in Zoho Books successfully");
											else
												form.showDialogMessage("Failed to update invoice in zoho books. API result : "+result+"\n Contact EVA Support");
										}else
											form.showDialogMessage("Failed to update invoice in zoho books! Please sync manually.");
										
									}
								});
							}else {
								
								taxAsync.createInvoiceInZohoBooks(model.getCount(), model.getCompanyId(),LoginPresenter.loggedInUser,new AsyncCallback<String>() {

									@Override
									public void onFailure(Throwable caught) {
										form.showDialogMessage("Failed to create invoice in zoho books. Please sync manually.");
									}

									@Override
									public void onSuccess(String result) {
										if(result!=null) {
											if(result.equalsIgnoreCase("Success"))
												form.showDialogMessage("Invoice created in Zoho Books successfully");
											else
												form.showDialogMessage("Failed to create invoice in zoho books. API result : "+result+"\n Contact EVA Support");
										}else
											form.showDialogMessage("Failed to create invoice in zoho books! Please sync manually.");
										
									}
								});
							}
						}else
							form.showDialogMessage(result);						
					}else
						form.showDialogMessage("Failed");	
				}
			});
			
		}
	}
	
	
	

	private void viewOrderDocuments() {
		final MyQuerry querry=new MyQuerry();
		Vector<Filter> temp=new Vector<Filter>();
		Filter filter=null;
		
		filter=new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(model.getContractCount());
		temp.add(filter);
		
		querry.setFilters(temp);
		if(model.getTypeOfOrder().equals(AppConstants.ORDERTYPESERVICE)){
			querry.setQuerryObject(new Contract());
		}else if(model.getTypeOfOrder().equals(AppConstants.ORDERTYPESALES)){
			querry.setQuerryObject(new SalesOrder());
		}else if(model.getTypeOfOrder().equals(AppConstants.ORDERTYPEPURCHASE)){
			querry.setQuerryObject(new PurchaseOrder());
		}else if(model.getTypeOfOrder().equals(AppConstants.ORDERTYPESERVICEPO)){
			querry.setQuerryObject(new ServicePo());
		}
		
		/** date 26.10.2018 added by komal **/
		if(model.getSubBillType() != null && !model.getSubBillType().equals("")){
			if(checkBillSubType(model.getSubBillType())){
				querry.setQuerryObject(new CNC());
			}
		}
		form.showWaitSymbol();
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				form.hideWaitSymbol();
				if(result.size()==0){
					form.showDialogMessage("No order document found.");
					return;
				}
				
				if(model.getTypeOfOrder().equals(AppConstants.ORDERTYPESERVICE)){
					if(model.getSubBillType() != null && !model.getSubBillType().equals("")){
						if(checkBillSubType(model.getSubBillType())){
							final CNC billDocument=(CNC) result.get(0);
							final CNCForm form=CNCPresenter.initalize();
							Timer timer=new Timer() {
								@Override
								public void run() {
									form.updateView(billDocument);
									form.setToViewState();
								}
							};
							timer.schedule(4000);
						}
					}else{
					final Contract billDocument=(Contract) result.get(0);
					final ContractForm form=ContractPresenter.initalize();
					Timer timer=new Timer() {
						@Override
						public void run() {
							form.updateView(billDocument);
							form.setToViewState();
						}
					};
					timer.schedule(1000);
					}
				}else if(model.getTypeOfOrder().equals(AppConstants.ORDERTYPESALES)){
					final SalesOrder billDocument=(SalesOrder) result.get(0);
					final SalesOrderForm form=SalesOrderPresenter.initalize();
					Timer timer=new Timer() {
						@Override
						public void run() {
							form.updateView(billDocument);
							form.setToViewState();
						}
					};
					timer.schedule(1000);
				}else if(model.getTypeOfOrder().equals(AppConstants.ORDERTYPEPURCHASE)){
					final PurchaseOrder billDocument=(PurchaseOrder) result.get(0);
					final PurchaseOrderForm form=PurchaseOrderPresenter.initalize();
					Timer timer=new Timer() {
						@Override
						public void run() {
							form.updateView(billDocument);
							form.setToViewState();
						}
					};
					timer.schedule(1000);
				}/***11-1-2019 added by amol***/
				else if (model.getTypeOfOrder().equals(AppConstants.ORDERTYPESERVICEPO)){
					final ServicePo billDocument=(ServicePo) result.get(0);
					final ServicePoForm form=ServicePoPresenter.initialize();
					Timer timer=new Timer() {
						@Override
						public void run() {
							form.updateView(billDocument);
							form.setToViewState();
						}
					};
					timer.schedule(1000);
				
					
				}
				
				
			}
		});
		
	}

	private void viewBillingDocuments() {
		
		/**
		 * @author Anil
		 * @since 14-01-2021
		 * If invoice is made from proforma then navigation was getting failed because we were storing 
		 * proforma invoice id in billing
		 */
		int invoiceId=0;
		if(model.getProformaCount()!=null&&model.getProformaCount()!=0&&!model.getInvoiceType().equals("Proforma Invoice")){
			invoiceId=model.getProformaCount();
		}else{
			invoiceId=model.getCount();
		}
		
		
		final MyQuerry querry=new MyQuerry();
		Vector<Filter> temp=new Vector<Filter>();
		Filter filter=null;
		
		filter=new Filter();
		filter.setQuerryString("invoiceCount");
//		filter.setIntValue(model.getCount());
		filter.setIntValue(invoiceId);
		temp.add(filter);
		
		querry.setFilters(temp);
		querry.setQuerryObject(new BillingDocument());
		form.showWaitSymbol();
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				form.hideWaitSymbol();
				if(result.size()==0){
					form.showDialogMessage("No billing document found.");
					return;
				}
				if(result.size()==1){
					final BillingDocument billDocument=(BillingDocument) result.get(0);
					final BillingDetailsForm form=BillingDetailsPresenter.initalize();
					Timer timer=new Timer() {
						@Override
						public void run() {
							form.updateView(billDocument);
							form.setToViewState();
						}
					};
					timer.schedule(1000);
				}else{
					BillingListPresenter.initalize(querry);
				}
				
			}
		});
		
	}

	private void viewPaymentDocuments() {
		final MyQuerry querry=new MyQuerry();
		Vector<Filter> temp=new Vector<Filter>();
		Filter filter=null;
		
		filter=new Filter();
		filter.setQuerryString("invoiceCount");
		filter.setIntValue(model.getCount());
		temp.add(filter);
		
		/**Date 26-9-2020 by Amol added a Acc Type Querry**/
		filter=new Filter();
		filter.setQuerryString("accountType");
		filter.setStringValue("AR");
		temp.add(filter);
		
		
		querry.setFilters(temp);
		querry.setQuerryObject(new CustomerPayment());
		form.showWaitSymbol();
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				form.hideWaitSymbol();
				if(result.size()==0){
					form.showDialogMessage("No payment document found.");
					return;
				}
				if(result.size()==1){
					final CustomerPayment billDocument=(CustomerPayment) result.get(0);
					final PaymentDetailsForm form=PaymentDetailsPresenter.initalize();
					Timer timer=new Timer() {
						@Override
						public void run() {
							form.updateView(billDocument);
							form.setToViewState();
						}
					};
					timer.schedule(1000);
				}else{
					PaymentListPresenter.initalize(querry);
				}
				
			}
		});
		
	}

	private void changeBillingStatusToTInvoiced() {
		
		model.setStatus(BillingDocument.BILLINGINVOICED);
		
		async.save(model,new AsyncCallback<ReturnFromServer>() {
			@Override
			public void onFailure(Throwable caught) {
			}
			@Override
			public void onSuccess(ReturnFromServer result) {
				form.tbstatus.setText(model.getStatus());
				form.setAppHeaderBarAsPerStatus();
			}
		});
	}

	private void createNewTaxInvoice(){
		
		System.out.println("Inside tax invoice method ");
		
			form.showWaitSymbol();
			final Invoice inventity = new Invoice();
//			PersonInfo pinfo = new PersonInfo();
//			if (form.getTbpersoncount().getValue() != null) {
//				pinfo.setCount(Integer.parseInt(form.getTbpersoncount().getValue()));
//			}
//			if (form.getTbpersonname().getValue() != null) {
//				pinfo.setFullName(form.getTbpersonname().getValue());
//			}
//			if (form.getTbpersoncell().getValue() != null) {
//				pinfo.setCellNumber(Long.parseLong(form.getTbpersoncell().getValue()));
//			}
//			if (form.getTbpocname().getValue() != null) {
//				pinfo.setPocName(form.getTbpocname().getValue());
//			}

			// ************************rohan made changes here for setting
			// fields()**************

			if (model.getGrossValue() != 0) {
				inventity.setGrossValue(model.getGrossValue());
			}

//			if (form.getDopaytermspercent().getValue() != 0) {
//
//				inventity.setTaxPercent(form.getDopaytermspercent().getValue());
//			}

			// ***********************************changes ends here
			// *****************************

//			if (pinfo != null){
//				inventity.setPersonInfo(pinfo);
//			}
			
			if(form.personInfoComposite.getValue()!=null){
				inventity.setPersonInfo(form.personInfoComposite.getValue());
			}

			if (!form.getIbcontractid().getValue().equals(""))
				inventity.setContractCount(Integer.parseInt(form.getIbcontractid().getValue()));
			if (model.getContractStartDate() != null) {
				inventity.setContractStartDate(model.getContractStartDate());
			}
			if (model.getContractEndDate() != null) {
				inventity.setContractEndDate(model.getContractEndDate());
			}
			if (form.getDosalesamount().getValue()!=null)
				inventity.setTotalSalesAmount(form.getDosalesamount().getValue());
			
			List<BillingDocumentDetails> billtablelis = form.billingdoctable.getDataprovider().getList();
			ArrayList<BillingDocumentDetails> billtablearr = new ArrayList<BillingDocumentDetails>();
			billtablearr.addAll(billtablelis);
			inventity.setArrayBillingDocument(billtablearr);
			
//			System.out.println("MULTIPLE CONTRAT STATUS ::: "+isMultipleContractBilling());
//			if(isMultipleContractBilling()){
//				inventity.setMultipleOrderBilling(true);
//			}
			
			
			//  rohan added this code for setting disc = 0 and net payable =setTotalBillingAmount
			
			if (form.getDototalbillamt().getValue() != null)
				inventity.setNetPayable(form.getDototalbillamt().getValue());
			
			if (form.getTbroundOffAmt().getValue() !=null && !form.getTbroundOffAmt().getValue().equals(""))
				inventity.setDiscount(Double.parseDouble(form.getTbroundOffAmt().getValue()));
			else{
				inventity.setDiscount(0.0);
			}
			//   ends here 
			
			
			if (form.getDototalbillamt().getValue() != null)
				inventity.setTotalBillingAmount(form.getDototalbillamt().getValue());
			if (form.getDbinvoicedate().getValue() != null)
				inventity.setInvoiceDate(form.getDbinvoicedate().getValue());
			if (form.getDbpaymentdate().getValue() != null)
				inventity.setPaymentDate(form.getDbpaymentdate().getValue());
			if (form.getOlbApproverName().getValue() != null)
				inventity.setApproverName(form.getOlbApproverName().getValue());
			if (form.getOlbEmployee().getValue() != null)
				inventity.setEmployee(form.getOlbEmployee().getValue());
			if (form.getOlbbranch().getValue() != null)
				inventity.setBranch(form.getOlbbranch().getValue());
			
			/*
			 *  nidhi
			 *  1-07-20170
			 */
			
			if (form.getTbSegment().getValue() != null)
				inventity.setSegment(form.getTbSegment().getValue());
			
				inventity.setRateContractServiceId(model.getRateContractServiceId());
			
			/*
			 *  end
			 */
			
			inventity.setOrderCreationDate(model.getOrderCreationDate());
			inventity.setCompanyId(model.getCompanyId());
			inventity.setInvoiceAmount(form.getDototalbillamt().getValue());
			inventity.setInvoiceType("Tax Invoice");
			inventity.setPaymentMethod(model.getPaymentMethod());
			inventity.setAccountType(model.getAccountType());
			inventity.setTypeOfOrder(model.getTypeOfOrder());
			inventity.setStatus(Invoice.CREATED);
				
			
			List<SalesOrderProductLineItem>productlist=form.getSalesProductTable().getDataprovider().getList();
			ArrayList<SalesOrderProductLineItem>prodList=new ArrayList<SalesOrderProductLineItem>();
			prodList.addAll(productlist);
			System.out.println();
			System.out.println("SALES PRODUCT TABLE SIZE ::: "+prodList.size());
			
			/**
			 * Date :22-09-2017 BY ANIL
			 */
			
			List<OtherCharges>OthChargeTbl=form.tblOtherCharges.getDataprovider().getList();
			ArrayList<OtherCharges>ocList=new ArrayList<OtherCharges>();
			ocList.addAll(OthChargeTbl);
			System.out.println("OTHER TAXES TABLE SIZE ::: "+ocList.size());
			
			inventity.setOtherCharges(ocList);
			if(form.dbOtherChargesTotal.getValue()!=null){
				inventity.setTotalOtherCharges(form.dbOtherChargesTotal.getValue());
			}
			
			/**
			 * End
			 */
			
			List<ContractCharges>taxestable=form.getInvoiceTaxTable().getDataprovider().getList();
			ArrayList<ContractCharges>taxesList=new ArrayList<ContractCharges>();
			taxesList.addAll(taxestable);
			System.out.println("TAXES TABLE SIZE ::: "+taxesList.size());
			
			List<ContractCharges>otherChargesable=form.getInvoiceChargesTable().getDataprovider().getList();
			ArrayList<ContractCharges>otherchargesList=new ArrayList<ContractCharges>();
			otherchargesList.addAll(otherChargesable);
			System.out.println("OTHER CHARGES TABLE SIZE ::: "+otherchargesList.size());
			inventity.setSalesOrderProductFromBilling(prodList);
//			inventity.setSalesOrderProducts(prodList);
			inventity.setBillingTaxes(taxesList);
			inventity.setBillingOtherCharges(otherchargesList);
			
			inventity.setCustomerBranch(model.getCustomerBranch());

			inventity.setAccountType(model.getAccountType());
			inventity.setArrPayTerms(model.getArrPayTerms());
			
			if (form.getOlbinvoicegroup().getValue() != null) {
				inventity.setInvoiceGroup(form.getOlbinvoicegroup().getValue());
			}
			if (model.getOrderCformStatus() != null) {
				inventity.setOrderCformStatus(model.getOrderCformStatus());
			}
			if (model.getOrderCformPercent() != 0&& model.getOrderCformPercent() != -1) {
				inventity.setOrderCformPercent(model.getOrderCformPercent());
			} else {
				inventity.setOrderCformPercent(-1);
			}
			
			if (model.getDiscount() != 0) {
				inventity.setDiscount(model.getDiscount());
			}
			
			if (model.getNetPayable() != 0) {
				inventity.setNetPayable(model.getNetPayable());
			}
			
			/**
			 * Old code commented by ANIl on 23-10-2017
			 */
//			if (model.getInvoiceAmount() != 0) {
//				inventity.setNetPayable(model.getInvoiceAmount());
//			}
			/**
			 * New code 
			 */
			if (model.getInvoiceAmount() != 0) {
				inventity.setInvoiceAmount(model.getInvoiceAmount());
			}
			
			/**
			 * rohan added this code for coping details from pro-forma to tax invoice Date : 15-05-2017
			 */
			if (model.getComment() != null) {
				inventity.setComment(model.getComment());
			}
			
			if(model.getCategory() !=null){
				inventity.setInvoiceCategory(model.getInvoiceCategory());
			}
			
			if(model.getGroup() !=null){
				inventity.setInvoiceGroup(model.getInvoiceGroup());
			}
			
			if(model.getType() !=null){
				inventity.setInvoiceConfigType(model.getInvoiceConfigType());
			}
			
			/**
			 * ends here  
			 */
			/**
			 *  nidhi
			 *  20-07-2017
			 *   quantity and mesurement transfer to the invoice entity 
			 */
			if(form.getQuantity().getValue() !=null && form.getQuantity().getValue() > 0){
				inventity.setQuantity(form.getQuantity().getValue());
			}
			
			if(form.getOlbUOM().getValue()!=null){
				inventity.setUom(form.getOlbUOM().getValue().trim());
			}
			/*
			 * end   
			 */
			
			
			/***************** vijay number range *************************/
			if(model.getNumberRange()!=null)
				inventity.setNumberRange(model.getNumberRange());
			/****************************************************/
			
			
			/**
			 * date 14 Feb 2017
			 * added by vijay for Setting billing period from date and To date in invoice
			 */
			if(form.getDbbillingperiodFromDate().getValue()!=null)
				inventity.setBillingPeroidFromDate(form.getDbbillingperiodFromDate().getValue());
			if(form.getDbbillingperiodToDate().getValue()!=null)
				inventity.setBillingPeroidToDate(form.getDbbillingperiodToDate().getValue());
			/**
			 * End here
			 */
			
			/** Date 06-09-2017 added by vijay for total amt roundoff *******/
			
			inventity.setTotalAmtExcludingTax(form.getDbtotalAmt().getValue());
			inventity.setDiscountAmt(form.dbDiscountAmt.getValue());
			inventity.setFinalTotalAmt(form.getDbFinalTotalAmt().getValue());
			inventity.setTotalAmtIncludingTax(form.getDbtotalAmtIncludingTax().getValue());
			/**
			 * Old COde commented by ANIL on 23-10-2017
			 */
//			inventity.setTotalBillingAmount(form.getDototalbillamt().getValue());
			/**
			 * New Code 
			 */
			inventity.setTotalBillingAmount(form.getDbtotalAmtIncludingTax().getValue());
			/**
			 * End
			 */
			if(form.getTbroundOffAmt().getValue()!=null&&!form.getTbroundOffAmt().getValue().equals("")){
				inventity.setDiscount(Double.parseDouble(form.getTbroundOffAmt().getValue()));
			}
			
			/**
			 * Date : 06-11-2017 BY ANIL
			 * setting billing comment to invoice
			 * for NBHC
			 */
			if(form.getTacomment().getValue()!=null){
				inventity.setComment(form.getTacomment().getValue());
			}
			/**
			 * End
			 */
			/**
			 * Updated By: Viraj
			 * Date: 06-07-2019
			 * Description: To map gstin no to tax invoice
			 */
			if(form.tbGstinNumber.getValue() != null) {
				inventity.setGstinNumber(form.tbGstinNumber.getValue());
			}
			/** Ends **/
			
			if(model.getSubBillType()!=null){
				inventity.setSubBillType(model.getSubBillType());
			}
			/** Date 09-10-2020 by Vijay set value of created by requirement raised by Rahul Tiwari ***/
			if(LoginPresenter.loggedInUser!=null){
				inventity.setCreatedBy(LoginPresenter.loggedInUser);
			}
			
			/**
			 * @author Anil
			 * @since 06-01-2021
			 * Copying TDS details, ref num ,proforma inv number and Consolidated details from Proforma to Tax invoice
			 */
			inventity.setRefNumber(model.getRefNumber());
			inventity.setConsolidatePrice(model.isConsolidatePrice());
			inventity.setTdsApplicable(model.isTdsApplicable());
			inventity.setTdsPercentage(model.getTdsPercentage());
			inventity.setTdsTaxValue(model.getTdsTaxValue());
			inventity.setProformaCount(model.getCount());
			
			/**
			 * @author Anil @since 05-04-2021
			 * For Alkosh raised by Rahul Tiwari
			 */
			inventity.setManagementFeesPercent(model.getManagementFeesPercent());
			
			/**
			 * @author Vijay 28-03-2022 for do not print service address flag updating to invoice
			 */
			inventity.setDonotprintServiceAddress(model.isDonotprintServiceAddress());
			/**
			 * @author Vijay Date :- 25-01-2022
			 * Des :- to manage invoice pdf print on invoice
			 */
			if(model.getInvoicePdfNameToPrint()!=null){
				inventity.setInvoicePdfNameToPrint(model.getInvoicePdfNameToPrint());
			}
			/**
			 * ends here
			 */
			
			if(model.getPaymentMode()!=null)
				inventity.setPaymentMode(model.getPaymentMode());
			
			Timer t = new Timer() {
			      @Override
			      public void run() {

			async.save(inventity, new AsyncCallback<ReturnFromServer>() {

				@Override
				public void onFailure(Throwable caught) {
					form.hideWaitSymbol();
					form.showDialogMessage("An unexpected error occured");
				}

				@Override
				public void onSuccess(ReturnFromServer result) {
					
					model.setProformaCount(result.count);
						
					async.save(model, new AsyncCallback<ReturnFromServer>() {
						@Override
						public void onFailure(Throwable caught) {
							form.hideWaitSymbol();
							form.showDialogMessage("An unexpected error occured");
						}
						@Override
						public void onSuccess(ReturnFromServer result) {
							System.out.println("RRRRRRRRRRRRRRR"+model.getProformaCount());
							form.proformaRefId.setValue(model.getProformaCount()+"");
							form.showDialogMessage("Document Tax Invoiced!",GWTCAlert.OPTION_ROUNDED_BLUE,GWTCAlert.OPTION_ANIMATION);
							form.hideWaitSymbol();
						}
					});
					
					
							
				}
			});
			      }
			};
		    t.schedule(2000);
	}

	@Override
	public void reactOnPrint() {
		/** date 15.10.2018 added by komal for sasha cnc invoices **/
		boolean flag = false;
		if(model.getSubBillType() != null && !model.getSubBillType().equals("")){
			flag = checkBillSubType(model.getSubBillType());
		}
		if(flag){
			/**
			 * @author Vijay Chougule
			 * Des :- Custom CNC Invoice PDF for Sunrise for FM and else for standard CNC invoice PDF
			 */
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "CustomizedCNCInvoiceSun")){
				
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "CompanyAsLetterHead"))
				{
					System.out.println("in side react on prinnt");
					panel=new PopupPanel(true);
					panel.add(conditionPopup);
					panel.setGlassEnabled(true);
					panel.show();
					panel.center();
				}
				else
				{
					final String url = GWT.getModuleBaseURL() + "CustomCNCInvoice"+"?Id="+model.getId()+"&"+"preprint=plane";
					Window.open(url, "test", "enabled");
				}
				
			}else if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "CustomizedCNCInvoice")){
				/**
				 * @author Anil @since 03-04-2021
				 * Alkosh customized pdf
				 */
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "CompanyAsLetterHead")){
					System.out.println("in side react on prinnt");
					panel=new PopupPanel(true);
					panel.add(conditionPopup);
					panel.setGlassEnabled(true);
					panel.show();
					panel.center();
				}else{
					final String url = GWT.getModuleBaseURL() + "CustomCNCInvoice"+"?Id="+model.getId()+"&"+"preprint=plane"+"&"+"version=one";
					Window.open(url, "test", "enabled");
				}
			}
			else{
				final String url = GWT.getModuleBaseURL() + "CNCInvoice"+"?Id="+model.getId()+"&"+"type="+model.getSubBillType();
				Window.open(url, "test", "enabled");
			}
			
			
			
			
		}else{
			
		/**
		 * Date 21-01-2019 by Vijay
		 * when we merge two bills docs so here i am calling the pdf this will also useful
		 * when we merge service and sales bill documents	
		 */
//			if(model.isMultipleOrderBilling()==true){
//				final String url2 = GWT.getModuleBaseURL() + "serviceinvoiceprint"+"?Id="+model.getId()+"&"+"preprint="+"plane"+"&"+"type="+"MultipleBilling";
//				Window.open(url2, "test", "enabled");
//				return;
//			}
		/**
		 * ends here	
		 */
			
		if(model.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESALES)
				||model.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE)
				||model.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESERVICEPO))
		{
			
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "CompanyAsLetterHead"))
			{
				System.out.println("in side react on prinnt");
				panel=new PopupPanel(true);
				panel.add(conditionPopup);
				panel.setGlassEnabled(true);
				panel.show();
				panel.center();
			}
			else
			{
				final String url = GWT.getModuleBaseURL() + "salesinvoiceprint"+"?Id="+model.getId()+"&"+"preprint="+"plane"+"&"+"typeOfOrder="+model.getTypeOfOrder();
				Window.open(url, "test", "enabled");
			}
		}
		
		if(model.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESERVICE)){
			
			//*************rohan changes ********************
			
			MyQuerry querry = new MyQuerry();
		  	Company c = new Company();
		  	Vector<Filter> filtervec=new Vector<Filter>();
		  	Filter filter = null;
		  	filter = new Filter();
		  	filter.setQuerryString("companyId");
			filter.setLongValue(c.getCompanyId());
			filtervec.add(filter);
			filter = new Filter();
			filter.setQuerryString("processName");
			filter.setStringValue("Invoice");
			filtervec.add(filter);
			
			filter = new Filter();
			filter.setQuerryString("processList.status");
			filter.setBooleanvalue(true);
			filtervec.add(filter);
			
			querry.setFilters(filtervec);
			querry.setQuerryObject(new ProcessConfiguration());
			
			service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onFailure(Throwable caught) {
				}			

				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					System.out.println(" result set size +++++++"+result.size());
					
					List<ProcessTypeDetails> processList =new ArrayList<ProcessTypeDetails>();
					
					if(result.size()==0){
//						final String url = GWT.getModuleBaseURL() + "serviceinvoiceprint"+"?Id="+model.getId()+"&"+"preprint="+"plane";
//						Window.open(url, "test", "enabled");
						
						
						/**
						 * Date 28-3-2018
						 * By jayshree
						 * Des.to call the multiple billing invoice
						 */
						
						if(model.isMultipleOrderBilling()==true){
							final String url2 = GWT.getModuleBaseURL() + "serviceinvoiceprint"+"?Id="+model.getId()+"&"+"preprint="+"plane"+"&"+"type="+"MultipleBilling";
							Window.open(url2, "test", "enabled");
						}else{
							final String url = GWT.getModuleBaseURL() + "serviceinvoiceprint"+"?Id="+model.getId()+"&"+"preprint="+"plane"+"&"+"type="+"SingleBilling";
							Window.open(url, "test", "enabled");
							
						}
					
						
					}
					else{
					
						for(SuperModel model:result)
						{
							ProcessConfiguration processConfig=(ProcessConfiguration)model;
							processList.addAll(processConfig.getProcessList());
							
						}
					
					for(int k=0;k<processList.size();k++){	
					if(processList.get(k).getProcessType().trim().equalsIgnoreCase("CompanyAsLetterHead")&&processList.get(k).isStatus()==true){
						
						cnt=cnt+1;
					
					}
					if(processList.get(k).getProcessType().trim().equalsIgnoreCase("PestoIndiaQuotations")&&processList.get(k).isStatus()==true){
						
						pestoIndia=pestoIndia+1;
					
					}
					if(processList.get(k).getProcessType().trim().equalsIgnoreCase("VCareCustomization")&&processList.get(k).isStatus()==true){
						
						vCare=vCare+1;
					
					}
					
					if(processList.get(k).getProcessType().trim().equalsIgnoreCase("InvoiceAndRefDocCustmization")&&processList.get(k).isStatus()==true){
						
						omPestInvoice=omPestInvoice+1;
					
					}
					
					
					if (processList.get(k).getProcessType().trim().equalsIgnoreCase("HygeiaPestManagement")&& processList.get(k).isStatus() == true) {

						hygeia = hygeia + 1;

					}

					if (processList.get(k).getProcessType().trim().equalsIgnoreCase("OnlyForOrionPestSolutions")&& processList.get(k).isStatus() == true) {

						orionPestControl = orionPestControl + 1;

					}
					
					
					if (processList.get(k).getProcessType().trim().equalsIgnoreCase("PCAMBCustomization")&& processList.get(k).isStatus() == true) {

						final String url = GWT.getModuleBaseURL() + "pdfpcambinvoice"+"?Id="+model.getId();
						Window.open(url, "test", "enabled");
						return;

					}
					
					
					System.out.println("ROHAN   =====  "+processList.get(k).getProcessType().trim());
					if(processList.get(k).getProcessType().trim().equalsIgnoreCase("ReddysPestControlQuotations")&&processList.get(k).isStatus()==true){
					
						reddysPestControl=reddysPestControl+1;
						
					}
					
					if(processList.get(k).getProcessType().trim().equalsIgnoreCase("OnlyForPecopp")&&processList.get(k).isStatus()==true){
						pecopp=pecopp+1;
						
					}
					
					if(processList.get(k).getProcessType().trim().equalsIgnoreCase(AppConstants.PC_NEWINVOICEFORMAT)&&processList.get(k).isStatus()==true){
//						newInvoiceFormat=newInvoiceFormat+1;
//						cnt=0;
						newinvoiceformatflag = true;
					}
					
					if(processList.get(k).getProcessType().trim().equalsIgnoreCase(AppConstants.TabularGstServiceInvoice)&&processList.get(k).isStatus()==true){
						tabularGstServiceInvoice=tabularGstServiceInvoice+1;
						cnt=0;
					}
					
					
					}
					
					System.out.println("rpc"+reddysPestControl+"cnt"+cnt+"pesto"+pestoIndia+"v care"+vCare+"ompest"+omPestInvoice);
					
					if(cnt>0){
						System.out.println("in side react on prinnt");
						panel=new PopupPanel(true);
						panel.add(conditionPopup);
						panel.setGlassEnabled(true);
						panel.show();
						panel.center();
					}
					else if(pestoIndia > 0)
					{
						
						System.out.println("in side react on prinnt");
						panel=new PopupPanel(true);
						panel.add(conditionPopup);
						panel.setGlassEnabled(true);
						panel.show();
						panel.center();
						
					}
					else if(vCare > 0)
					{
						
						System.out.println("in side react on prinnt");
						panel=new PopupPanel(true);
						panel.add(conditionPopup);
						panel.setGlassEnabled(true);
						panel.show();
						panel.center();
						
					}
					else if(omPestInvoice > 0)
					{
						
						System.out.println("in side react on prinnt");
						panel=new PopupPanel(true);
						panel.add(conditionPopup);
						panel.setGlassEnabled(true);
						panel.show();
						panel.center();
						
					}
					else if (hygeia > 0) {
						
						final String url = GWT.getModuleBaseURL()+ "pdfinvoice" + "?Id="+ model.getId();
						Window.open(url, "test", "enabled");

					} 
					else if(reddysPestControl > 0){
						
						final String url = GWT.getModuleBaseURL() + "pdfrinvoice"+"?Id="+model.getId();
						Window.open(url, "test", "enabled");
					/**
					 *   Added by Priyanka : 30-01-2021
					 *   Des : Header footer not printing on Invoice issue raised by Ashwini.
					 */
					}else if(pecopp>0){
						final String url = GWT.getModuleBaseURL()+"contractserviceone"+"?Id="+model.getId()+"&"+"preprint=plane"+"&"+"type="+"Invoice";
						Window.open(url, "test", "enabled");
					/** 
					 *  End	
					 */
					}else if(orionPestControl > 0){
//						final String url = GWT.getModuleBaseURL() + "pdfOrionInvoice"+"?Id="+model.getId();
//						Window.open(url, "test", "enabled");
						System.out.println("in side react on prinnt");
						panel=new PopupPanel(true);
						panel.add(conditionPopup);
						panel.setGlassEnabled(true);
						panel.show();
						panel.center();
					
					
					}
//					else if(newInvoiceFormat>0){
//						if(model.isMultipleOrderBilling()==true){
//							final String url2 = GWT.getModuleBaseURL() + "serviceinvoiceprint"+"?Id="+model.getId()+"&"+"preprint="+"plane"+"&"+"type="+"MultipleBilling";
//							Window.open(url2, "test", "enabled");
//						}else{
//							
//							final String url = GWT.getModuleBaseURL() + "servicegstinvoice"+"?Id="+model.getId()+"&"+"type="+"SingleBilling";
//							Window.open(url, "test", "enabled");
//						}
//					}
					else if(tabularGstServiceInvoice>0){
						if(model.isMultipleOrderBilling()==true){
							final String url = GWT.getModuleBaseURL() + "servicegstinvoice"+"?Id="+model.getId()+"&"+"type="+"MultipleBilling";
							Window.open(url, "test", "enabled");
						}else{							
							final String url = GWT.getModuleBaseURL() + "servicegstinvoice"+"?Id="+model.getId()+"&"+"type="+"SingleBilling";
							Window.open(url, "test", "enabled");							
						}
					}
					else
					{
//						final String url = GWT.getModuleBaseURL() + "serviceinvoiceprint"+"?Id="+model.getId()+"&"+"preprint="+"plane";
//						Window.open(url, "test", "enabled");
						
						

						/**
						 * Date 28-3-2018
						 * By jayshree
						 * Des.to call the multiple billing invoice and Single invoice billing
						 */
						
						if(model.isMultipleOrderBilling()==true){
							final String url2 = GWT.getModuleBaseURL() + "serviceinvoiceprint"+"?Id="+model.getId()+"&"+"preprint="+"plane"+"&"+"type="+"MultipleBilling";
							Window.open(url2, "test", "enabled");
						}else{
							
							Console.log("model.getInvoicePdfNameToPrint() =="+model.getInvoicePdfNameToPrint());
							if((model.getInvoicePdfNameToPrint()!=null && !model.getInvoicePdfNameToPrint().equals("")&&
									model.getInvoicePdfNameToPrint().equals(AppConstants.NEWSTANDARDINVOICEFORMAT)) || 
									(model.getInvoicePdfNameToPrint()!=null && !model.getInvoicePdfNameToPrint().equals("")&&
											model.getInvoicePdfNameToPrint().equals(AppConstants.CUSTOMIZEDINVOICEFORMAT)) || newinvoiceformatflag){
								final String url = GWT.getModuleBaseURL() + "servicegstinvoice"+"?Id="+model.getId()+"&"+"type="+"SingleBilling"+"&"+"preprint="+"plane";
								Window.open(url, "test", "enabled");
							}
							else if((model.getInvoicePdfNameToPrint()!=null && !model.getInvoicePdfNameToPrint().equals("")&&
									model.getInvoicePdfNameToPrint().equals(AppConstants.STANDARDINVOICEFORMAT)) ){
								final String url = GWT.getModuleBaseURL() + "serviceinvoiceprint"+"?Id="+model.getId()+"&"+"preprint="+"plane"+"&"+"type="+"SingleBilling";
								Window.open(url, "test", "enabled");
							}
							else if(newinvoiceformatflag){
								final String url = GWT.getModuleBaseURL() + "servicegstinvoice"+"?Id="+model.getId()+"&"+"type="+"SingleBilling"+"&"+"preprint="+"plane";
								Window.open(url, "test", "enabled");
							}
							else{
								final String url = GWT.getModuleBaseURL() + "serviceinvoiceprint"+"?Id="+model.getId()+"&"+"preprint="+"plane"+"&"+"type="+"SingleBilling";
								Window.open(url, "test", "enabled");
							}
							
							
						}
						
					
						
					}
				}
				}
			});
		}
		
//		final String url = GWT.getModuleBaseURL() + "pdfrinvoice"+"?Id="+model.getId();
//		Window.open(url, "test", "enabled");
		}
	}

	@Override
	public void reactOnEmail() {
//		boolean conf = Window.confirm("Do you really want to send email?");
//		if (conf == true) {
//		emailService.initiateInvoiceEmail(model,new AsyncCallback<Void>() {
//
//			@Override
//			public void onFailure(Throwable caught) {
//				Window.alert("Unable To Send Email");
//				caught.printStackTrace();
//			}
//
//			@Override
//			public void onSuccess(Void result) {
//				Window.alert("Email Sent Sucessfully !");
//			}
//		});
//		}
		
		/**
		 * @author Vijay Date 19-03-2021
		 * Des :- above old code commented 
		 * Added New Email Popup Functionality
		 */
		emailpopup.showPopUp();
		setEmailPopUpData();
		
		/**
		 * ends here
		 */
	
	}
	
	@Override
	public void reactOnDownload() {
		
		final ArrayList<Invoice> invoicingarray=new ArrayList<Invoice>();
		List<Invoice> listbill=(List<Invoice>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
		invoicingarray.addAll(listbill); 
		csvservice.setinvoicelist(invoicingarray, new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);
				
			}

			@Override
			public void onSuccess(Void result) {
				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "csvservlet"+"?type="+15;
				Window.open(url, "test", "enabled");
				
//				final String url=gwt + "CreateXLXSServlet"+"?type="+15;
//				Window.open(url, "test", "enabled");
				
				
//				ArrayList<SuperModel> superlist = new ArrayList<SuperModel>();
//				superlist.addAll(invoicingarray);
//				csvservice.getData(superlist, AppConstants.INVOICEDETAILS, new AsyncCallback<ArrayList<SuperModel>>() {
//
//					@Override
//					public void onFailure(Throwable caught) {
//						// TODO Auto-generated method stub
//						
//					}
//
//					@Override
//					public void onSuccess(ArrayList<SuperModel> result) {
//						// TODO Auto-generated method stub
//						ArrayList<Customer> customerlist = new ArrayList<Customer>();
//						for(SuperModel model : result){
//							Customer customer = (Customer) model;
//							customerlist.add(customer);
//						}
//						
//						ArrayList<String> invoicelist = new ArrayList<String>();
//						invoicelist.add("INVOICE ID");
//						invoicelist.add("INVOICE DATE");
//						invoicelist.add("INVOICE GROUP");
//						invoicelist.add("INVOICE DOCUMENT TYPE");
//						invoicelist.add("INVOICE CATEGORY");
//						invoicelist.add("INVOICE TYPE");
//						invoicelist.add("BP ID");
//						invoicelist.add("BP NAME");
//						invoicelist.add("Customer Branch");
//						invoicelist.add("CELL NUMBER");
//						
//						/**Date 11-9-2019 by Amol for ultrapest **/
//						invoicelist.add("EMAIL ID");
//						invoicelist.add("POC NAME");
//						invoicelist.add("REF NUMBER");
//						invoicelist.add("ORDER ID");
//						invoicelist.add("ORDER START DATE");
//						invoicelist.add("ORDER END DATE");
//						
//						/**@Sheetal : 02-03-2022
//						 *  Des  : Adding Billing Period Start date and End date, requirement by pest-o-sheild**/
//						invoicelist.add("BILLING PERIOD START DATE");
//						invoicelist.add("BILLING PERIOD END DATE");
//						/**end**/
//						/***Date 6-5-2020 by Amol added  invoice refno and reference Date***/
//						invoicelist.add("INVOICE REF. NO.");
//						invoicelist.add("REFERENCE DATE");
//						invoicelist.add("ACCOUNT TYPE");
//						invoicelist.add("ORDER AMOUNT");
//						invoicelist.add("PAYMENT DATE");
//						invoicelist.add("APPROVER NAME");
//						//***********************rohan remove this as per ultra pest control requirement ******
//						invoicelist.add("PRODUCT ID");
//						invoicelist.add("PRODUCT CODE");
//						invoicelist.add("PRODUCT NAME");
//						invoicelist.add("PRODUCT QUANTITY");
//						invoicelist.add("PRODUCT UOM");
//						invoicelist.add("PRODUCT HSN CODE");
//						invoicelist.add("PRODUCT RATE");
//						invoicelist.add("GROSS VALUE");
//						invoicelist.add("OTHER CHARGES");
//						invoicelist.add("TAX 1 DETAILS");
//						invoicelist.add("TAX 1 %");
//						invoicelist.add("TAX 1 AMOUNT");
//						invoicelist.add("TAX 2 DETAILS");
//						invoicelist.add("TAX 2 %");
//						invoicelist.add("TAX 2 AMOUNT");
//						invoicelist.add("SWACHH BHARAT CESS %");
//						invoicelist.add("TAX AMOUNT");
//						invoicelist.add("KRISHI KALYAN CESS %");
//						invoicelist.add("TAX AMOUNT");
//						invoicelist.add("PRODUCT FLAT DISCOUNT AMOUNT");
//						invoicelist.add("PRODUCT TOTAL AMOUNT");
//						invoicelist.add("TOTAL AMOUNT");
//						invoicelist.add("DISCOUNT AMOUNT");
//						invoicelist.add("AFTER DISCOUNT TOTAL AMOUNT");
//						invoicelist.add("AFTER TAXES TOTAL AMOUNT");
//						invoicelist.add("TOTAL BILLING AMOUNT");
//						invoicelist.add("ROUND OFF");
//						invoicelist.add("INVOICE AMOUNT");
//						invoicelist.add("STATUS");
//						invoicelist.add("SALES PERSON");
//						invoicelist.add("SEGMENT");
//						invoicelist.add("SERVICE ID");
//						invoicelist.add("GSTIN NUMBER");
//						invoicelist.add("QUANTITY");
//						invoicelist.add("UOM");
//						/**
//						 *  NIdhi
//						 *  24-07-2017
//						 */
//						invoicelist.add("REFERANCE NO.");
//						/**
//						 *  end
//						 */
//						/**
//						 * Rahul Verma Added these reference details generated by Interface
//						 */
//						invoicelist.add("Reference Invoice ID");
//						invoicelist.add("Reference Net Value");
//						invoicelist.add("Reference Tax Amount");
//						invoicelist.add("Reference Total Amount");
//						/**
//						 * ENDS for Rahul Verma
//						 */
//						invoicelist.add("Contract Status");
//					
//						int columnCount = 65;
//						
//						NumberFormat df=NumberFormat.getFormat("0.00");
//
//						for(Invoice invoice:invoicingarray)
//						{
//							int noofproduct=0;
//							
//							for(int d=0;d<invoice.getSalesOrderProducts().size();d++)
//							{
//								invoicelist.add(invoice.getCount()+"");
//
//								if(invoice.getInvoiceDate()!=null)
//									invoicelist.add(AppUtility.parseDate(invoice.getInvoiceDate(),"dd-MMM-yyyy"));
//								if(invoice.getInvoiceGroup()!=null){
//									invoicelist.add(invoice.getInvoiceGroup());
//								}
//								else{
//									invoicelist.add("");
//								}
//								invoicelist.add(invoice.getInvoiceType());
//								
//								if(invoice.getInvoiceCategory()!=null){
//									invoicelist.add(invoice.getInvoiceCategory());
//								}
//								else{
//									invoicelist.add("");
//								}
//								if(invoice.getInvoiceConfigType()!=null){
//									invoicelist.add(invoice.getInvoiceConfigType());
//								}
//								else{
//									invoicelist.add("");
//								}
//								invoicelist.add(invoice.getPersonInfo().getCount()+"");
//								/**Date 26-3-2020 by Amol to remove comma from string for om Pest***/
//								invoicelist.add(invoice.getPersonInfo().getFullName());
//								/***14-12-2019 Deepak Salve added this code for Customer Branch Download ***/
//								if(invoice.getCustomerBranch()!=null){
//									invoicelist.add(invoice.getCustomerBranch());
//								}else{
//									invoicelist.add("");
//								}
//								/***End***/
//								invoicelist.add(invoice.getPersonInfo().getCellNumber()+"");
//
//								if(invoice.getPersonInfo().getEmail()!=null){
//									invoicelist.add(invoice.getPersonInfo().getEmail());
//								}else{
//									invoicelist.add("");
//								}
//								if(invoice.getPersonInfo().getPocName()!=null){
//									invoicelist.add(invoice.getPersonInfo().getPocName());
//								}
//								else{
//									invoicelist.add("");
//								}
//								if(invoice.getRefNumber()!=null)
//								{
//									invoicelist.add(invoice.getRefNumber());
//								}
//								else
//								{
//									invoicelist.add("N.A");
//								}
//								invoicelist.add(invoice.getContractCount()+"");
//								if(invoice.getContractStartDate()!=null){
//									invoicelist.add(AppUtility.parseDate(invoice.getContractStartDate(),"dd-MMM-yyyy"));
//								}
//								else{
//									invoicelist.add("N.A");
//								}
//								if(invoice.getContractEndDate()!=null){
//									invoicelist.add(AppUtility.parseDate(invoice.getContractEndDate(),"dd-MMM-yyyy"));
//								}
//								else{
//									invoicelist.add("N.A");
//								}
//								/**@Sheetal : 02-03-2022 
//								 * Des  : Adding Billing Period Start date and End date, requirement by pest-o-sheild**/
//								if(invoice.getBillingPeroidFromDate()!=null){
//									invoicelist.add(AppUtility.parseDate(invoice.getBillingPeroidFromDate(),"dd-MMM-yyyy"));
//								}
//								else{
//									invoicelist.add("N.A");
//								}
//								if(invoice.getBillingPeroidToDate()!=null){
//									invoicelist.add(AppUtility.parseDate(invoice.getBillingPeroidToDate(),"dd-MMM-yyyy"));
//								}
//								else{
//									invoicelist.add("N.A");
//								}
//								/**
//								 * @author Priyanka Bhagwat Date : 04-02-2021
//								 * Downloaded excel data mismatch issue raised by Ashwini for ultra pest control.
//								 */
//								
//								if(invoice.getInvRefNumber()!=null){
//									invoicelist.add(invoice.getInvRefNumber());
//								}
//								else{
//									invoicelist.add("N.A");
//								}
//								if(invoice.getReferenceDate()!=null){
//									invoicelist.add(AppUtility.parseDate(invoice.getReferenceDate(),"dd-MMM-yyyy"));
//								}
//								else{
//									invoicelist.add("N.A");
//								}
//								/**
//								 *  End
//								 */
//								invoicelist.add(invoice.getAccountType());
//								invoicelist.add(invoice.getTotalSalesAmount()+"");
//
//								if(invoice.getPaymentDate()!=null)
//									invoicelist.add(AppUtility.parseDate(invoice.getPaymentDate(),"dd-MMM-yyyy"));
//								if(invoice.getApproverName()!=null)
//									invoicelist.add(invoice.getApproverName());
//
//								invoicelist.add(invoice.getSalesOrderProducts().get(d).getProdId()+"");
//								invoicelist.add(invoice.getSalesOrderProducts().get(d).getProdCode()+"");
//								invoicelist.add(invoice.getSalesOrderProducts().get(d).getProdName()+"");
//								invoicelist.add(invoice.getSalesOrderProducts().get(d).getQuantity()+"");
//
//								if(invoice.getSalesOrderProducts().get(d).getUnitOfMeasurement()!=null){
//									invoicelist.add(invoice.getSalesOrderProducts().get(d).getUnitOfMeasurement()+"");
//
//								}else{
//									invoicelist.add("");
//								}
//								
//								if(invoice.getSalesOrderProducts().get(d).getHsnCode()!=null){
//									invoicelist.add(invoice.getSalesOrderProducts().get(d).getHsnCode()+"");
//								}else{
//									invoicelist.add("");
//								}
//								
//								if(invoice.getSalesOrderProducts().get(d).getPrice()!=0){
//									invoicelist.add(invoice.getSalesOrderProducts().get(d).getPrice()+"");
//								}else{
//									invoicelist.add("");
//								}
//								
//					//************************rohan added here for tax field in download(ultra changes ) 
//								//*************calculation for tax amt ******************8
//								double convertval=Double.parseDouble(invoice.getSalesOrderProducts().get(d).getPaymentPercent()+"");
//								System.out.println("convertval==="+convertval);
//								double taxValue=Double.parseDouble(invoice.getSalesOrderProducts().get(d).getServiceTax().getPercentage()+"");
//								System.out.println("taxValue===="+taxValue);
//								double grossValueAsPerTaxpercent=0;
//								if(convertval==100){
//									System.out.println("tax %====="+invoice.getTaxPercent());
//									System.out.println("invoicelist.getSalesOrderProducts().get(d).getBaseBillingAmount()"+invoice.getSalesOrderProducts().get(d).getBaseBillingAmount());
//								 grossValueAsPerTaxpercent=(invoice.getSalesOrderProducts().get(d).getBaseBillingAmount());
//								}
//								else{
//									grossValueAsPerTaxpercent=(invoice.getSalesOrderProducts().get(d).getBaseBillingAmount()*convertval)/100;
//								}
//								
//								if(grossValueAsPerTaxpercent!=0){
//									invoicelist.add(grossValueAsPerTaxpercent+"");
//								}
//								else{
//									invoicelist.add("");
//								}
//								
//								double otherchargesTaxvalue=0.0;
//								double totalTax1=0.0;
//								double totalTax2=0.0;
//								if(invoice.getOtherCharges()!=null)
//								{	
//								ArrayList<OtherCharges>	 othercharge=new ArrayList<OtherCharges>();
//								double value=0;
//								double tax1=0.0;
//								double tax2=0.0;
//								othercharge=invoice.getOtherCharges();
//								for(OtherCharges otherCharge:othercharge){
////									writer.append(otherCharge.getAmount()+"");
//									if(noofproduct==0){
//									value+=otherCharge.getAmount();
//									}
//									if(otherCharge.getTax1().getPercentage()!=0){
//										tax1=otherCharge.getTax1().getPercentage();
//									}
//									if(otherCharge.getTax1().getPercentage()!=0){
//										tax2=otherCharge.getTax2().getPercentage();
//									}
//									System.out.println(" Other Charges Tax Amount :-"+otherCharge.getTax1().getPercentage());
//								}
//								double totalTax=tax1+tax2;
//								System.out.println(" Total Tax :-"+totalTax);
//								otherchargesTaxvalue=value*totalTax/100;
//								totalTax1=value*tax1/100;
//								totalTax2=value*tax2/100;
//								System.out.println(" Total Tax After Cal:-"+otherchargesTaxvalue+" Value "+value);
//								otherchargesTaxvalue=value-otherchargesTaxvalue;
//								System.out.println(" Total Tax :-"+totalTax);
//								System.out.println(" Other Charges Tax Amount 2 :-"+otherchargesTaxvalue);
//								invoicelist.add(value+"");
//								}else{
//									invoicelist.add(0+"");
//								}
//								/**
//								 * this code is for vat tax
//								 */
//								double vattaxAmount =0;
//								if(invoice.getSalesOrderProducts().get(d).getVatTax().getPercentage()!=0)
//								{
//									double taxVa = invoice.getSalesOrderProducts().get(d).getVatTax().getPercentage();
//									double vattaxAmt  = grossValueAsPerTaxpercent*taxVa /100;
//									
//									vattaxAmount = vattaxAmt+otherchargesTaxvalue;
//									//  new codeby rohan for gst 
//									if(invoice.getSalesOrderProducts().get(d).getVatTax().getTaxPrintName()!=null
//											&& !invoice.getSalesOrderProducts().get(d).getVatTax().equals("")){
//										invoicelist.add(invoice.getSalesOrderProducts().get(d).getVatTax().getTaxPrintName());
//									}
//									else{
//										invoicelist.add(invoice.getSalesOrderProducts().get(d).getVatTax().getTaxConfigName());
//									}
//									invoicelist.add(taxVa+"");
//									invoicelist.add((vattaxAmt+totalTax1)+"");
//								}
//								else
//								{
//									invoicelist.add("NA");
//									invoicelist.add("NA");
//									invoicelist.add("NA");
//
//								}
//								
//								
//								/**
//								 * this code is for serrvice tax 
//								 */
//								if(taxValue!=0)
//								{
//									if(invoice.getSalesOrderProducts().get(d).getServiceTax().getTaxPrintName()!=null
//											&& !invoice.getSalesOrderProducts().get(d).getServiceTax().equals(""))
//									{
//										double taxAmt  =grossValueAsPerTaxpercent*taxValue /100;
//										
//										double productTotalAmt = grossValueAsPerTaxpercent + vattaxAmount + taxAmt;
//
//										invoicelist.add(invoice.getSalesOrderProducts().get(d).getServiceTax().getTaxPrintName());
//										invoicelist.add(taxValue+"");
//										invoicelist.add((taxAmt+totalTax1)+"");
//										invoicelist.add("NA");
//										invoicelist.add("NA");
//										invoicelist.add("NA");
//										invoicelist.add("NA");
//										invoicelist.add(invoice.getSalesOrderProducts().get(d).getFlatDiscount()+"");
//										invoicelist.add(productTotalAmt+"");
//									}
//									else{
//										
//										double taxAmt123=0;
//										
//										double taxAmt= grossValueAsPerTaxpercent*taxValue;
//										taxAmt123=taxAmt/100;
//										invoicelist.add(invoice.getSalesOrderProducts().get(d).getServiceTax().getTaxConfigName());
//										
//										if(taxValue==15)
//										{
//											double cess = 0; 
//											double kks = 0; 
//											double taxVal=0;
//											taxVal =taxValue-1;
//											cess = 0.5; 
//											kks =0.5;
//											
//											double taxAmt1= (grossValueAsPerTaxpercent*taxVal)/100;
//											double cessVal= (grossValueAsPerTaxpercent*cess)/100;
//											double kksVal= (grossValueAsPerTaxpercent*kks)/100;
//											
//											invoicelist.add(df.format(taxVal));
//											invoicelist.add(df.format(taxAmt1));
//											invoicelist.add(cess+"");
//											invoicelist.add(df.format(cessVal));
//											invoicelist.add(kks+"");
//											invoicelist.add(df.format(kksVal));
//											invoicelist.add(invoice.getSalesOrderProducts().get(d).getFlatDiscount()+"");
//											invoicelist.add("NA");
//
//										}
//										else if(taxValue==14.5)
//										{
//											
//											double cess = 0; 
//											double taxVal=0;
//											taxVal =taxValue-0.5;
//											cess = 0.5; 
//											
//											double taxAmt1= (grossValueAsPerTaxpercent*taxVal)/100;
//											double cessVal= (grossValueAsPerTaxpercent*cess)/100;
//											
//											invoicelist.add(df.format(taxVal));
//											invoicelist.add(df.format(taxAmt1));
//											invoicelist.add(cess+"");
//											invoicelist.add(df.format(cessVal));
//											invoicelist.add("NA");
//											invoicelist.add("NA");
//											invoicelist.add(invoice.getSalesOrderProducts().get(d).getFlatDiscount()+"");
//											invoicelist.add("NA");
//
//										}
//										else
//										{
//											
//											invoicelist.add(invoice.getSalesOrderProducts().get(d).getServiceTax().getPercentage()+"");
//											invoicelist.add(df.format(taxAmt123));
//											invoicelist.add("NA");
//											invoicelist.add("NA");
//											invoicelist.add("NA");
//											invoicelist.add("NA");
//											invoicelist.add(invoice.getSalesOrderProducts().get(d).getFlatDiscount()+"");
//											invoicelist.add("NA");
//											
//										}
//									}
//								}
//								else
//								{
//									
//									invoicelist.add("NA");
//									invoicelist.add("NA");
//									invoicelist.add("NA");
//									invoicelist.add("NA");
//									invoicelist.add("NA");
//									invoicelist.add("NA");
//									invoicelist.add("NA");
//									invoicelist.add(invoice.getSalesOrderProducts().get(d).getFlatDiscount()+"");
//									invoicelist.add("NA");
//								}
//								
//								/*** Date 11-09-2017 added by vijay for discount amt round off  ***********/
//
//								invoicelist.add(invoice.getTotalAmtExcludingTax()+"");
//								invoicelist.add(invoice.getDiscountAmt()+"");
//								invoicelist.add(invoice.getFinalTotalAmt()+"");
//								invoicelist.add(invoice.getTotalAmtIncludingTax()+"");
//								invoicelist.add(invoice.getTotalBillingAmount()+"");
//								invoicelist.add(invoice.getDiscount()+"");
//								invoicelist.add(invoice.getInvoiceAmount()+"");
//								invoicelist.add(invoice.getStatus()+"");
//								invoicelist.add(invoice.getEmployee()+"");
//
//							
//								/**
//								 * Date : 11-07-2017 Nidhi
//								 */
//								if(invoice.getSegment()!=null){
//									invoicelist.add(invoice.getSegment());
//								}
//								else{
//									invoicelist.add("");
//								}
//								/**
//								 * End
//								 */
//								
//								/**
//								 * Date : 17-07-2017 Nidhi
//								 */
//								if(invoice.getRateContractServiceId()!=0){
//									invoicelist.add(invoice.getRateContractServiceId()+"");
//								}
//								else{
//									invoicelist.add("");
//								}
//								/**
//								 * End
//								 */
//								
//								/**
//								 * Date : 17-07-2017 Nidhi
//								 */
//								if(invoice.getGstinNumber()!=null && !invoice.getGstinNumber().equals("") ){
//									invoicelist.add(invoice.getGstinNumber());
//								}
//								else{
//									String gstnumber = getcustomerGSTInNumber(invoice.getPersonInfo().getCount(), customerlist);
//									invoicelist.add(gstnumber);
//
//								}
//								/**
//								 * End
//								 */
//								/*
//								 *  NIDHI
//								 *  20-07-2017
//								 *  
//								 */
//								if (invoice.getQuantity() != 0) {
//									invoicelist.add(invoice.getQuantity()+"");
//
//								} else {
//									invoicelist.add("");
//								}
//								if (invoice.getUom() != null) {
//									invoicelist.add(invoice.getQuantity()+"");
//								} else {
//									invoicelist.add("");
//								}
//								/*
//								 *  end
//								 */
//								
//								/**
//								 *  nidhi
//								 *  24-07-2017
//								 *  
//								 */
//								if (invoice.getRefNumber() != null) {
//									invoicelist.add(invoice.getRefNumber());
//								} else {
//									invoicelist.add("");
//								}
//								
//								/*
//								 *  end
//								 */
//								/**
//								 * Rahul Verma Added this code for Invoice Download
//								 */
//								if (invoice.getSapInvoiceId() != null) {
//									invoicelist.add(invoice.getSapInvoiceId());
//								} else {
//									invoicelist.add("");
//								}
//								
//								if (invoice.getSAPNetValue() != 0) {
//									invoicelist.add(invoice.getSAPNetValue()+"");
//								} else {
//									invoicelist.add("");
//								}
//								if (invoice.getSAPTaxAmount() != 0) {
//									invoicelist.add(invoice.getSAPTaxAmount()+"");
//
//								} else {
//									invoicelist.add("");
//								}
//								
//								if (invoice.getSAPTotalAmount() != 0) {
//									invoicelist.add(invoice.getSAPTotalAmount()+"");
//								} else {
//									invoicelist.add("");
//								}
//								/**
//								 * End
//								 */
//								/**
//								 * nidhi
//								 */
//								if(invoice.isRenewContractFlag()){
//									invoicelist.add("Renew");
//								}else{
//									invoicelist.add("new");
//								}
//								
//								noofproduct++;
//								}
//							}
//					
//						
//						csvservice.setExcelData(invoicelist, columnCount, AppConstants.INVOICEDETAILS, new AsyncCallback<String>() {
//							
//							@Override
//							public void onSuccess(String result) {
//								// TODO Auto-generated method stub
//								
//								String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
//								final String url=gwt + "CreateXLXSServlet"+"?type="+15;
//								Window.open(url, "test", "enabled");
//							}
//							
//							@Override
//							public void onFailure(Throwable caught) {
//								// TODO Auto-generated method stub
//								
//							}
//						});
//						
//
//					}
//
//					
//				});
				
				
			}
		});
	}


	@Override
	protected void makeNewModel() {
		model=new Invoice();
	}
	
	public static InvoiceDetailsForm initalize()
	{
		//AppMemory.getAppMemory().currentState=ScreeenState.NEW;
		InvoiceDetailsForm form=new  InvoiceDetailsForm();
		InvoiceDetailsTableProxy gentable=new InvoiceDetailsTableProxy();
		gentable.setView(form);
		gentable.applySelectionModle();
		InvoiceDetailsPresenterSearch.staticSuperTable=gentable;
		InvoiceDetailsSearchPopUp searchpopup=new InvoiceDetailsSearchPopUp();
		form.setSearchpopupscreen(searchpopup);

		InvoiceDetailsPresenter presenter=new InvoiceDetailsPresenter(form,new Invoice());
//		form.setToNewState();
		/**
		 * Date : 29-07-2017 BY ANIL
		 * This line was added to update the name of screen loaded from any other screen or by clicking on billing details on account model
		 */
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Accounts/Invoice Details",Screen.INVOICEDETAILS);
		/**
		 * END
		 */
		AppMemory.getAppMemory().stickPnel(form);
		return form;
	}
	
	
	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.salesprocess.Invoice")
	public static  class InvoiceDetailsPresenterSearch extends SearchPopUpScreen<Invoice>{

		@Override
		public MyQuerry getQuerry() {
			return null;
		}

		@Override
		public boolean validate() {
			// TODO Auto-generated method stub
			return true;
		}};

		@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.salesprocess.Invoice")
		public static class InvoiceDetailsPresenterTable extends SuperTable<Invoice> implements GeneratedVariableRefrence{

			@Override
			public Object getVarRef(String varName) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void createTable() {
				// TODO Auto-generated method stub

			}

			@Override
			protected void initializekeyprovider() {
				// TODO Auto-generated method stub

			}

			@Override
			public void addFieldUpdater() {
				// TODO Auto-generated method stub

			}

			@Override
			public void setEnable(boolean state) {
				// TODO Auto-generated method stub

			}

			@Override
			public void applyStyle() {
				// TODO Auto-generated method stub

			}} ;
			
			
			
			@Override
			public void onClick(ClickEvent event) {
				
				super.onClick(event);
				

				boolean flag = false;
				if(model.getSubBillType() != null && !model.getSubBillType().equals("")){
					flag = checkBillSubType(model.getSubBillType());
				}
				

				if(event.getSource().equals(conditionPopup.getBtnOne()))
				{
					
					if(flag){
						/**
						 * @author Vijay Chougule
						 * Des :- Custom CNC Invoice PDF for Sunrise for FM and else for standard CNC invoice PDF
						 */
						if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "CustomizedCNCInvoiceSun")){

							final String url = GWT.getModuleBaseURL() + "CustomCNCInvoice"+"?Id="+model.getId()+"&"+"preprint=yes";
							Window.open(url, "test", "enabled");
							panel.hide();
							return;
						}
						else if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "CustomizedCNCInvoice")){
							/**
							 * @author Anil @since 03-04-2021
							 * Alkosh customized pdf
							 */
							
							final String url = GWT.getModuleBaseURL() + "CustomCNCInvoice"+"?Id="+model.getId()+"&"+"preprint=yes"+"&"+"version=one";
							Window.open(url, "test", "enabled");
							panel.hide();
							return;
						}
//						else{
//							final String url = GWT.getModuleBaseURL() + "CNCInvoice"+"?Id="+model.getId()+"&"+"type="+model.getSubBillType();
//							Window.open(url, "test", "enabled");
//						}
						
					}else if(model.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESALES)
							||model.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE)
							||model.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESERVICEPO)){
						final String url = GWT.getModuleBaseURL() + "salesinvoiceprint"+"?Id="+model.getId()+"&"+"preprint="+"yes"+"&"+"typeOfOrder="+model.getTypeOfOrder();
						Window.open(url, "test", "enabled");
						panel.hide();
					}

					
					
					if(cnt >0)
					{
						/**
						 * Date 28-3-2018
						 * By jayshree
						 * Des.to call the multiple billing invoice
						 */
						
						if(model.isMultipleOrderBilling()==true){
							final String url2 = GWT.getModuleBaseURL() + "serviceinvoiceprint"+"?Id="+model.getId()+"&"+"preprint="+"yes"+"&"+"type="+"MultipleBilling";
							Window.open(url2, "test", "enabled");
							/**
							 * @author Priyanka Bhagwat - 25-01-2021
							 * Des :- Old format print of PDF issue raised by Ashwini.
							 */
						}else if(pecopp>0){
							final String url = GWT.getModuleBaseURL() + "contractserviceone"+"?Id="+model.getId()+"&"+"preprint=yes"+"&"+"type="+"Invoice";
							Window.open(url, "test", "enabled");
							panel.hide();
						}else{
							
							/**
							 * @author Vijay Date 27-01-2022
							 * Des :- invoice format selection based on process config and dropdown value from contract
							 */
							Console.log("model.getInvoicePdfNameToPrint()"+model.getInvoicePdfNameToPrint());
							if((model.getInvoicePdfNameToPrint()!=null && !model.getInvoicePdfNameToPrint().equals("")&&
									model.getInvoicePdfNameToPrint().equals(AppConstants.NEWSTANDARDINVOICEFORMAT)) || 
									(model.getInvoicePdfNameToPrint()!=null && !model.getInvoicePdfNameToPrint().equals("")&&
											model.getInvoicePdfNameToPrint().equals(AppConstants.CUSTOMIZEDINVOICEFORMAT)) || newinvoiceformatflag){
								final String url = GWT.getModuleBaseURL() + "servicegstinvoice"+"?Id="+model.getId()+"&"+"type="+"SingleBilling"+"&"+"preprint="+"yes";
								Window.open(url, "test", "enabled");
							}
							else if((model.getInvoicePdfNameToPrint()!=null && !model.getInvoicePdfNameToPrint().equals("")&&
									model.getInvoicePdfNameToPrint().equals(AppConstants.STANDARDINVOICEFORMAT)) ){
								final String url = GWT.getModuleBaseURL() + "serviceinvoiceprint"+"?Id="+model.getId()+"&"+"preprint="+"yes"+"&"+"type="+"SingleBilling";
								Window.open(url, "test", "enabled");
							}
							else if(newinvoiceformatflag){
								final String url = GWT.getModuleBaseURL() + "servicegstinvoice"+"?Id="+model.getId()+"&"+"type="+"SingleBilling"+"&"+"preprint="+"yes";
								Window.open(url, "test", "enabled");
							}
							else{
								final String url = GWT.getModuleBaseURL() + "serviceinvoiceprint"+"?Id="+model.getId()+"&"+"preprint="+"yes"+"&"+"type="+"SingleBilling";
								Window.open(url, "test", "enabled");
							}
							
//							final String url = GWT.getModuleBaseURL() + "serviceinvoiceprint"+"?Id="+model.getId()+"&"+"preprint="+"yes"+"&"+"type="+"SingleBilling";
//							Window.open(url, "test", "enabled");
						}
						panel.hide();
					}
					
					else if(pestoIndia > 0)
					{
						System.out.println("Pesto india quotation");
						final String url = GWT.getModuleBaseURL() + "pdfpestoIndiaservice"+"?Id="+model.getId()+"&"+"type="+"i"+"&"+"preprint="+"yes"; 
						Window.open(url, "test", "enabled");
						
						panel.hide();
						
					}
					
					else if(vCare > 0)
					{
						System.out.println("V care quotation");
						
						final String url = GWT.getModuleBaseURL() + "Invoice"+"?Id="+model.getId()+"&"+"preprint="+"yes";
						Window.open(url, "test", "enabled");
						
						panel.hide();
						
					}
					else if(omPestInvoice > 0)
					{
						final String url = GWT.getModuleBaseURL() + "invoicepdf"+"?Id="+model.getId()+"&"+"preprint="+"yes";
						Window.open(url, "test", "enabled");
						panel.hide();
					}
					else if(orionPestControl>0){
						final String url = GWT.getModuleBaseURL() + "pdfOrionInvoice"+"?Id="+model.getId()+"&"+"preprint="+"yes";
						Window.open(url, "test", "enabled");
						panel.hide();
					/**
					 *  Added by Priyanka : 30-01-2021
					 *  Des : Header footer not printing on Invoice PDF and Old format print issue raised by Ashwini.
					 */
					}else if(pecopp>0){
						final String url = GWT.getModuleBaseURL() + "contractserviceone"+"?Id="+model.getId()+"&"+"preprint=yes"+"&"+"type="+"Invoice";
						Window.open(url, "test", "enabled");
						panel.hide();
					}
					
					
				}
				
				if(event.getSource().equals(conditionPopup.getBtnTwo()))
				{
					
					if(flag){
						/**
						 * @author Vijay Chougule
						 * Des :- Custom CNC Invoice PDF for Sunrise for FM and else for standard CNC invoice PDF
						 */
						if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "CustomizedCNCInvoiceSun")){
							
							final String url = GWT.getModuleBaseURL() + "CustomCNCInvoice"+"?Id="+model.getId()+"&"+"preprint=no";
							Window.open(url, "test", "enabled");
							panel.hide();
							return;
						}
						else if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "CustomizedCNCInvoice")){
							/**
							 * @author Anil @since 03-04-2021
							 * Alkosh customized pdf
							 */
							
							final String url = GWT.getModuleBaseURL() + "CustomCNCInvoice"+"?Id="+model.getId()+"&"+"preprint=no"+"&"+"version=one";
							Window.open(url, "test", "enabled");
							panel.hide();
							return;
						}
//						else{
//							final String url = GWT.getModuleBaseURL() + "CNCInvoice"+"?Id="+model.getId()+"&"+"type="+model.getSubBillType();
//							Window.open(url, "test", "enabled");
//						}
						
					}
					
						/**
						 * @author Priyanka Bhagwat - 25-01-2021
						 * Des :- Old format print of PDF issue raised by Ashwini.
						 */
//						
//						if((AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice",  "CompanyAsLetterHead"))){
//							final String url = GWT.getModuleBaseURL() + "pecopppdfservlet"+"?Id="+model.getId()+"&"+"preprint=no";
//							Window.open(url, "test", "enabled");
//							panel.hide();
//							
//						}
//					
					
					
					else if(model.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESALES)
							||model.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE)
							||model.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESERVICEPO)){
						final String url = GWT.getModuleBaseURL() + "salesinvoiceprint"+"?Id="+model.getId()+"&"+"preprint="+"no"+"&"+"typeOfOrder="+model.getTypeOfOrder();;
						Window.open(url, "test", "enabled");
						panel.hide();
					}

					
					if(cnt >0)
					{
						
//						System.out.println("inside two no");
//						final String url = GWT.getModuleBaseURL() + "serviceinvoiceprint"+"?Id="+model.getId()+"&"+"preprint="+"no"+"&"+"type="+"SingleBilling";
//						Window.open(url, "test", "enabled");
//						panel.hide();
						

						/**
						 * Date 28-3-2018
						 * By jayshree
						 * Des.to call the multiple billing invoice
						 */
						
						if(model.isMultipleOrderBilling()==true){
							final String url2 = GWT.getModuleBaseURL() + "serviceinvoiceprint"+"?Id="+model.getId()+"&"+"preprint="+"no"+"&"+"type="+"MultipleBilling";
							Window.open(url2, "test", "enabled");
						}else if(pecopp>0){
							final String url = GWT.getModuleBaseURL() + "contractserviceone"+"?Id="+model.getId()+"&"+"preprint=no"+"&"+"type="+"Invoice";;
							Window.open(url, "test", "enabled");
							panel.hide();
						}
						else
						{
//							final String url = GWT.getModuleBaseURL() + "serviceinvoiceprint"+"?Id="+model.getId()+"&"+"preprint="+"no"+"&"+"type="+"SingleBilling";
//							Window.open(url, "test", "enabled");
							
							/**
							 * @author Vijay Date 27-01-2022
							 * Des :- invoice format selection based on process config and dropdown value from contract
							 */
							Console.log("model.getInvoicePdfNameToPrint() "+model.getInvoicePdfNameToPrint());
							if((model.getInvoicePdfNameToPrint()!=null && !model.getInvoicePdfNameToPrint().equals("")&&
									model.getInvoicePdfNameToPrint().equals(AppConstants.NEWSTANDARDINVOICEFORMAT)) || 
									(model.getInvoicePdfNameToPrint()!=null && !model.getInvoicePdfNameToPrint().equals("")&&
											model.getInvoicePdfNameToPrint().equals(AppConstants.CUSTOMIZEDINVOICEFORMAT))){
								final String url = GWT.getModuleBaseURL() + "servicegstinvoice"+"?Id="+model.getId()+"&"+"type="+"SingleBilling"+"&"+"preprint="+"no";
								Window.open(url, "test", "enabled");
							}
							else if((model.getInvoicePdfNameToPrint()!=null && !model.getInvoicePdfNameToPrint().equals("")&&
									model.getInvoicePdfNameToPrint().equals(AppConstants.STANDARDINVOICEFORMAT)) ){
								final String url = GWT.getModuleBaseURL() + "serviceinvoiceprint"+"?Id="+model.getId()+"&"+"preprint="+"no"+"&"+"type="+"SingleBilling";
								Window.open(url, "test", "enabled");
							}
							else if(newinvoiceformatflag){
								final String url = GWT.getModuleBaseURL() + "servicegstinvoice"+"?Id="+model.getId()+"&"+"type="+"SingleBilling"+"&"+"preprint="+"no";
								Window.open(url, "test", "enabled");
							}
							else{
								final String url = GWT.getModuleBaseURL() + "serviceinvoiceprint"+"?Id="+model.getId()+"&"+"preprint="+"no"+"&"+"type="+"SingleBilling";
								Window.open(url, "test", "enabled");
							}
							
						}
						panel.hide();
						
					}
					
					else if(pestoIndia > 0)
					{
						System.out.println("Pesto india quotation");
						final String url = GWT.getModuleBaseURL() + "pdfpestoIndiaservice"+"?Id="+model.getId()+"&"+"type="+"i"+"&"+"preprint="+"no"; //+"&"+"group="+model.getGroup();//1st PDF
						Window.open(url, "test", "enabled");
						panel.hide();
					
					}
					else if(vCare > 0)
					{
						System.out.println("V care quotation");
						final String url = GWT.getModuleBaseURL() + "Invoice"+"?Id="+model.getId()+"&"+"preprint="+"no";
						Window.open(url, "test", "enabled");
						
						panel.hide();
						
					}
					
					else if(omPestInvoice > 0)
					{
						final String url = GWT.getModuleBaseURL() + "invoicepdf"+"?Id="+model.getId()+"&"+"preprint="+"no";
						Window.open(url, "test", "enabled");
						panel.hide();
					}
					else if(orionPestControl>0){
						final String url = GWT.getModuleBaseURL() + "pdfOrionInvoice"+"?Id="+model.getId()+"&"+"preprint="+"no";
						Window.open(url, "test", "enabled");
						panel.hide();
					}else if(pecopp>0){
						final String url = GWT.getModuleBaseURL() + "contractserviceone"+"?Id="+model.getId()+"&"+"preprint=no"+"&"+"type="+"Invoice";
						Window.open(url, "test", "enabled");
						panel.hide();
					}
					
				}
				
				
				

				if(event.getSource()==popup.getBtnOk()){
//					panel.hide();
//					/**
//					 * Date:28-11-2017 BY ANIL
//					 * if invoice in created state we will cancelled invoice directly else need to cancel its corresponding documents.
//					 */
//					if(model.getStatus().equals(Invoice.APPROVED)){
//						statusOfPaymentFromInvoiceID();
//					}else{
//						createNewInvoice();
//					}
//					popup.getRemark().setValue("");
			if (popup.getRemark().getValue().trim() != null
					&& !popup.getRemark().getValue().trim().equals("")) {
				panel.hide();
				
				/**
				 * Date 14-02-2018 By vijay added if condition for while Canceling Proforma invoice must cancel first tax invoice  
				 */
				if(model.getInvoiceType().equals("Proforma Invoice")){
					if(form.proformaRefId.getValue()!=null && !form.proformaRefId.getValue().equals("")){
						checkTaxInvoiceStatus(form.proformaRefId.getValue());
					}else {/** date 30/03/2018 added by komal for performa invoice cancellation **/
						createNewInvoice();
					}
				}else{
					
					/**
					 * Date:28-11-2017 BY ANIL if invoice in created state we will
					 * cancelled invoice directly else need to cancel its
					 * corresponding documents.
					 */
					if (model.getStatus().equals(Invoice.APPROVED)) {
						statusOfPaymentFromInvoiceID();
					} else {
						if(model.getIrnStatus()!=null && model.getIrnStatus().equals("Active")) {
								cancelEInvoice(true);
						}
						else {
							createNewInvoice();
						}
					}
				}
//				if(model.getIrnStatus()!=null) {
//					if(model.getIrnStatus().equals("Active"))
//						cancelEInvoice();
//				}
				
				//popup.getRemark().setValue("");
			} else {
				form.showDialogMessage("Please enter valid remark to cancel Invoice..");
			}
					
				}
				else if(event.getSource()==popup.getBtnCancel()){
					panel.hide();
					popup.getRemark().setValue("");
				}
				
				if (event.getSource()==form.btReferenceDetails) {
					reactOnReferenceDetailsPopUp();
					refDocPanel = new PopupPanel(true);
					refDocPanel.add(refDocPopUp);
					refDocPanel.show();
					refDocPanel.center();
				}
				if(event.getSource()==refDocPopUp.getBtnOk()){
					refDocPanel.hide();
				}
				/** date 24.10.2018 added by komal for tds **/
				if(event.getSource().equals(form.cbtdsApplicable)){
					if (event.getSource() == form.getCbtdsApplicable()) {
							if (form.getCbtdsApplicable().getValue().equals(true)) {
								form.getTbtdsPerValue().setEnabled(true);
								form.getDbtdsValue().setEnabled(true);
							}
			
							
							if (form.getCbtdsApplicable().getValue().equals(false)) {
								form.getTbtdsPerValue().setEnabled(false);
								form.getDbtdsValue().setEnabled(false);
								form.getTbtdsPerValue().setValue(0.0);
								form.getDbtdsValue().setValue(0.0);
								System.out.println("tds amt ="+form.getDbtdsValue().getValue());
								double totalValue = 0 , roundOff = 0 , total = 0;;
								if(form.dbtotalAmtIncludingTax.getValue()!=null){
									totalValue = form.dbtotalAmtIncludingTax.getValue();
								}
								if(form.tbroundOffAmt.getValue()!=null && !form.tbroundOffAmt.getValue().equals("")){
									roundOff = Double.parseDouble(form.tbroundOffAmt.getValue());
								}
								if(LoginPresenter.PC_NO_ROUNDOFF_INVOICE_CONTRACTFlag){
									total = totalValue + roundOff;
								}
								else{
									total = Math.round(totalValue + roundOff);

								}
									if(form.getDbtdsValue().getValue()!=null && form.getDbtdsValue().getValue()!=0)
									{
										double tds = form.getDbtdsValue().getValue();
										form.netPayable.setValue(total - tds);					
									}else{
										form.netPayable.setValue(total);
									}		
						}
						
					}
				}
				
				if(event.getSource().equals(annexurePopup.getLblOk())){
					
					updateBillAsPerTheAnnexureModification();
					
					
					annexurePopup.hidePopUp();
				}
				if(event.getSource().equals(annexurePopup.getLblCancel())){
					annexurePopup.hidePopUp();
				}
				
				if(event.getSource().equals(salesAnnexurePopup.getLblOk())){
					updateBillAsPerTheSalesAnnexureModification();
					salesAnnexurePopup.hidePopUp();
				}
				if(event.getSource().equals(salesAnnexurePopup.getLblCancel())){
					salesAnnexurePopup.hidePopUp();
				}
								
				if(event.getSource().equals(conditionPopupforInvoiceReset.getBtnOne())){
					Console.log("in reset");
					panel.hide();
					reactOnInvoiceReset();			
				}
				if(event.getSource().equals(conditionPopupforInvoiceReset.getBtnTwo())){
					Console.log("in cancel");
					panel.hide();
				}
				
				if(event.getSource().equals(conditionPopupForEinvoiceCancellation.getBtnOne())){
					Console.log("in einvoice cancellation");
					panel.hide();
					popup.getPaymentDate().setValue(new Date()+"");
					popup.getPaymentID().setValue(form.getIbinvoiceid().getValue());
					popup.getPaymentStatus().setValue(form.getTbstatus().getValue());
					popup.getRemark().setValue("");
					panel=new PopupPanel(true);
					panel.add(popup);
					panel.show();
					panel.center();				
				}
				if(event.getSource().equals(conditionPopupForEinvoiceCancellation.getBtnTwo())){
					Console.log("in cancel");
					panel.hide();
				}
				if(event.getSource().equals(form.btnCopyContractDescription)){
					Console.log("copy description button clicked");
					final MyQuerry querry=new MyQuerry();
					Vector<Filter> temp=new Vector<Filter>();
					Filter filter=null;
					
					filter=new Filter();
					filter.setQuerryString("count");
					filter.setIntValue(model.getContractCount());
					temp.add(filter);
					
					if(model.getTypeOfOrder().equals(AppConstants.ORDERTYPESERVICE)){
						querry.setFilters(temp);					
						querry.setQuerryObject(new Contract());					
					}else if(model.getTypeOfOrder().equals(AppConstants.ORDERTYPEFORSALES)){
						querry.setFilters(temp);					
						querry.setQuerryObject(new SalesOrder());
					}
					form.showWaitSymbol();
					service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
						@Override
						public void onFailure(Throwable caught) {
							form.hideWaitSymbol();
						}

						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							Console.log("Result size="+result.size());
							form.hideWaitSymbol();
							if(result.size()==0){
								form.showDialogMessage("No contract found.");
								return;
							}
							if(model.getTypeOfOrder().equals(AppConstants.ORDERTYPESERVICE)){
								final Contract con=(Contract) result.get(0);
								
								if(con.getDescription()!=null&&!con.getDescription().equals(""))
									form.tacomment.setText(con.getDescription());
								else
									form.showDialogMessage("No description found on Contract");
								
							}else if(model.getTypeOfOrder().equals(AppConstants.ORDERTYPESALES)){
								final SalesOrder so=(SalesOrder) result.get(0);
								String desc="";
								if(so.getDescription()!=null&&!so.getDescription().equals("")){
									desc+=so.getDescription();
								}
								if(so.getDescriptiontwo()!=null&&!so.getDescriptiontwo().equals("")){
									desc+=so.getDescriptiontwo();
								}								
								if(desc.equals(""))
									form.showDialogMessage("No description found on Sales Order");
								else
									form.tacomment.setText(desc);
								
							}
							else {
								form.showDialogMessage("This option is applicable only for service Invoice.");
							}
				
						}

				});
				}
				
				if(event.getSource().equals(descriptionPopup.getLblOk()))
				{
					Console.log("ok clicked on description popup");
					String desc="";
					if(descriptionPopup.gettaDescription().getText()!=null) {
						model.setComment(descriptionPopup.gettaDescription().getText());
					}else
						model.setComment("");
					
					async.save(model,new AsyncCallback<ReturnFromServer>() {
						@Override
						public void onFailure(Throwable caught) {
						}
						@Override
						public void onSuccess(ReturnFromServer result) {
							form.tacomment.setText(model.getComment());				
						}
					
					});
					
					descriptionPopup.hidePopUp();
				}
				if(event.getSource().equals(descriptionPopup.getLblCancel()))
				{
					Console.log("cancel clicked on description popup");
					descriptionPopup.hidePopUp();
				}
				
				if(event.getSource().equals(conditionPopupForOnlyEvaInvoiceCancellation.getBtnOne()))
				{
					Console.log("yes clicked on conditionPopupForOnlyEvaInvoiceCancellation popup");
					updateInvoiceView(false,true,true);
					panel.hide();
				}
				
				if(event.getSource().equals(conditionPopupForOnlyEvaInvoiceCancellation.getBtnTwo()))
				{
					Console.log("cancel clicked on conditionPopupForOnlyEvaInvoiceCancellation popup");
					panel.hide();
				}
				if(event.getSource().equals(uploadFilePopup.getBtnOk()))
				{
					Console.log("uploadFilePopup ok button clicked");
					if(uploadFilePopup.getUpload().getValue()!=null) {
						model.setDocUpload1(uploadFilePopup.getUpload().getValue());
						async.save(model,new AsyncCallback<ReturnFromServer>() {
							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected Error occured while uploading document!");
//								
							}
							@Override
							public void onSuccess(ReturnFromServer result) {
								form.upload1.setValue(uploadFilePopup.getUpload().getValue());
								form.showDialogMessage("Document uploaded successfully!");
								
								uploadFilePopupPanel.hide();
								
							}
						});
					}else {
						form.showDialogMessage("Upload file!");
						return;
					}
				}
				if(event.getSource().equals(uploadFilePopup.getBtnCancel()))
				{Console.log("uploadFilePopup cancel button clicked");
					
					uploadFilePopupPanel.hide();
				}
				
				
				
				
			}
			
			
			private void updateBillAsPerTheSalesAnnexureModification() {
				
				try{
					for(SalesOrderProductLineItem item:form.getSalesProductTable().getDataprovider().getList()){
						ArrayList<SalesAnnexureType> annexList=salesAnnexurePopup.getSalesAnnexureListProdWise(item.getBillProductName());
						item.getSalesAnnexureMap().put(item.getBillProductName(), annexList);
						double totalPrice=salesAnnexurePopup.getPriceFromSalesAnnexureList(annexList);
						item.setPrice(totalPrice);
						item.setBaseBillingAmount(totalPrice);
						item.setTotalAmount(totalPrice);
						item.setBasePaymentAmount(totalPrice-item.getFlatDiscount());
					}
					
					List<SalesOrderProductLineItem> salesProductlis=form.getSalesProductTable().getDataprovider().getList();
					ArrayList<SalesOrderProductLineItem> arrSalesProduct=new ArrayList<SalesOrderProductLineItem>();
					arrSalesProduct.addAll(salesProductlis);
					model.setSalesOrderProducts(arrSalesProduct);
					form.getSalesProductTable().getTable().redraw();
					
				}catch(Exception e){
					
				}
				
				
				
				
				if (form.getSalesProductTable().getDataprovider().getList().size() != 0) {
					Console.log("INSIDE IF CONDITION....!!!");
					double retrieveBillAmt = form.getSalesProductTable().calculateTotalBillAmt();
					billingTaxTableOnRowChange();
					retrieveBillAmt = retrieveBillAmt+ form.getInvoiceTaxTable().calculateTotalTaxes();
					if (checkproductstatus()) {
						Console.log("PRODUCT TRUE....!!!");
						reactOnProductsChange(true);
					} else {
						Console.log("Inside else=================");
						retrieveproductinfomation(true);
					}
				}
				
				
				
			}
			
			private void updateBillAsPerTheAnnexureModification() {
				model.setAnnexureMap(annexurePopup.getAnnexureDetailsInMap());
				for(SalesOrderProductLineItem item:form.getSalesProductTable().getDataprovider().getList()){
					double totalPrice=item.getPrice();
					double area=0;
					for(AnnexureType type:annexurePopup.getAnnexureTbl().getValue() ){
						
						if(annexurePopup.getAnnexureTbl().staffBillFlag&&
								type.getDesignation().equals(item.getProdName())
								&&item.getBillType().equals("Labour")){
							
							area=area+type.getNoOfDaysPresent();
							item.setArea(area+"");
							Console.log("staffBillFlag Labour "+item.getArea() + " / "+area);
							
							if(!LoginPresenter.areaWiseCalRestricFlg){
								totalPrice=area*item.getPrice();
							}
							
							item.setBaseBillingAmount(totalPrice);
							item.setTotalAmount(totalPrice);
							item.setBasePaymentAmount(totalPrice-item.getFlatDiscount());
						}
						if(annexurePopup.getAnnexureTbl().normalOtFlag&&
								type.getDesignation().equals(item.getProdName())
								&&item.getBillType().equals("Overtime")){
							
							area=area+type.getNormalDayOt();
							item.setArea(area+"");
							Console.log("Overtime "+item.getArea()+ " / "+area);
							
							if(!LoginPresenter.areaWiseCalRestricFlg){
								totalPrice=area*item.getPrice();
							}
							
							item.setBaseBillingAmount(totalPrice);
							item.setTotalAmount(totalPrice);
							item.setBasePaymentAmount(totalPrice-item.getFlatDiscount());
						}
						
						if(annexurePopup.getAnnexureTbl().nhOtFlag&&
								type.getDesignation().equals(item.getProdName())
								&&(item.getBillType().equals("Holiday")||item.getBillType().equals("National Holiday"))){
							
							area=area+type.getNhOt();
							item.setArea(area+"");
							Console.log("Holiday National Holiday"+item.getArea()+ " / "+area);
							
							if(!LoginPresenter.areaWiseCalRestricFlg){
								totalPrice=area*item.getPrice();
							}
							
							item.setBaseBillingAmount(totalPrice);
							item.setTotalAmount(totalPrice);
							item.setBasePaymentAmount(totalPrice-item.getFlatDiscount());
						}
						
						if(annexurePopup.getAnnexureTbl().phOtFlag&&
								type.getDesignation().equals(item.getProdName())
								&&item.getBillType().equals("Public Holiday")){
							
							area=area+type.getPhOt();
							item.setArea(area+"");
							Console.log("Public Holiday"+item.getArea()+ " / "+area);
							
							if(!LoginPresenter.areaWiseCalRestricFlg){
								totalPrice=area*item.getPrice();
							}
							
							item.setBaseBillingAmount(totalPrice);
							item.setTotalAmount(totalPrice);
							item.setBasePaymentAmount(totalPrice-item.getFlatDiscount());
						}
					}
					
					
					
					
					
				}
				
				form.getSalesProductTable().getTable().redraw();
				
				if (form.getSalesProductTable().getDataprovider().getList().size() != 0) {
					Console.log("INSIDE IF CONDITION....!!!");
					double retrieveBillAmt = form.getSalesProductTable().calculateTotalBillAmt();
		
					billingTaxTableOnRowChange();
					retrieveBillAmt = retrieveBillAmt+ form.getInvoiceTaxTable().calculateTotalTaxes();
		
					if (checkproductstatus()) {
						Console.log("PRODUCT TRUE....!!!");
						reactOnProductsChange(true);
					} else {
						Console.log("Inside else=================");
						retrieveproductinfomation(true);
					}
		
				}
				
			}

			/**
			 * Date 14-02-2018 By vijay added if condition for while Canceling Proforma invoice must cancel first tax invoice  
			 */
			
			private void checkTaxInvoiceStatus(String value) {
				int taxInvoiceId = Integer.parseInt(value.trim());
				Vector<Filter> filterVec = new Vector<Filter>();
				Filter filter = null;
				
				MyQuerry querry = new MyQuerry();
				filter = new Filter();
				filter.setQuerryString("count");
				filter.setIntValue(taxInvoiceId);
				filterVec.add(filter);
				
				filter = new Filter();
				filter.setQuerryString("invoiceType");
				filter.setStringValue(AppConstants.CREATETAXINVOICE);
				filterVec.add(filter);
				
				querry.setFilters(filterVec);
				querry.setQuerryObject(new Invoice());
				async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
					
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						// TODO Auto-generated method stub
						if(result.size()!=0){
							
							for(SuperModel model:result){
								Invoice invoice = (Invoice) model;
								if(!invoice.getStatus().equals("Cancelled")){
									form.showDialogMessage("Please cancel Tax Invoice first");
									break;
								}else{
									createNewInvoice();
									break;
								}
							}
						}
						
					}
					
					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						
					}
				});
				
				
			}
			/**
			 * ends here
			 */
			
			
			/*
			 * Only in View State
			 */
			private void reactOnReferenceDetailsPopUp() {
				// TODO Auto-generated method stub
				if(model.getSapInvoiceId()!=null)
				refDocPopUp.getTbRefInvoiceId().setValue(model.getSapInvoiceId());
				if(model.getSAPNetValue()!=0)
				refDocPopUp.getDbNetValue().setValue(model.getSAPNetValue());
				if(model.getSAPTaxAmount()!=0)
				refDocPopUp.getDbTaxAmount().setValue(model.getSAPTaxAmount());
				if(model.getSAPTotalAmount()!=0)
				refDocPopUp.getDbTotalAmount().setValue(model.getSAPTotalAmount());
				
			}
			/********************************Cancellation Logic**********************************/
			
			
			private void statusOfPaymentFromInvoiceID()
			{
				if(form.getTbstatus().getValue().trim().equals(Invoice.APPROVED))
				{
				
					final int invoiceid=Integer.parseInt(form.getIbinvoiceid().getValue().trim());
					MyQuerry querry = new MyQuerry();
				  	Company c = new Company();
				  	Vector<Filter> filtervec=new Vector<Filter>();
				  	Filter filter = null;
				  	filter = new Filter();
				  	filter.setQuerryString("companyId");
					filter.setLongValue(c.getCompanyId());
					filtervec.add(filter);
					filter = new Filter();
					filter.setQuerryString("invoiceCount");
					filter.setIntValue(invoiceid);
					filtervec.add(filter);
					querry.setFilters(filtervec);
					querry.setQuerryObject(new CustomerPayment());
					async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			  			
			  			@Override
			  			public void onFailure(Throwable caught) {
			  			}
	
			  			@Override
						public void onSuccess(ArrayList<SuperModel> result) {
			  				int cnt=0;
			  			
			  				ArrayList<CustomerPayment> slist=new ArrayList<CustomerPayment>();
			  				
							for(SuperModel model:result)
							{
								CustomerPayment custPaymentEntity= (CustomerPayment)model;
								custPaymentEntity.setStatus(custPaymentEntity.getStatus());
								custPaymentEntity.setInvoiceCount(custPaymentEntity.getInvoiceCount());
								slist.add(custPaymentEntity);
							}
							int openpaumentdocument =0;
							for(int i=0;i<slist.size();i++)
							{
								if(invoiceid==slist.get(i).getInvoiceCount());
								{
									if(slist.get(i).getStatus().equals(Invoice.PAYMENTCLOSED))
									{
										cnt=cnt+1;
									}
									if(slist.get(i).getStatus().equals(Invoice.PAYMENTCLOSED)) {
										openpaumentdocument = openpaumentdocument + 1;
									}
										

								}
							} 
							if(cnt>0){
								form.showDialogMessage("Document cannot be cancelled because payment document status is closed!");
							}
							else
							{
								
								if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CustomerPayment", "Accounting Interface") &&
										openpaumentdocument>0) {
									form.showDialogMessage("Please cancel payment document first!");
								}
								else {
									changeStatus();
									
									if(model.getIrnStatus()!=null && model.getIrnStatus().equals("Active")) {
										cancelEInvoice(true);
									}
									else {
										createNewInvoice();
									}
								}
								
								
								
							}
							}
					});
				}
				//   rohan added this condition for proforma invoice cancel 
				else if(form.getTbstatus().getValue().trim().equals(BillingDocument.BILLINGINVOICED))
				{
					model.setStatus(BillingDocument.PROFORMAINVOICE);
					
					async.save(model,new AsyncCallback<ReturnFromServer>() {
						@Override
						public void onFailure(Throwable caught) {
							form.showDialogMessage("An Unexpected Error occured !");
//							
						}
						@Override
						public void onSuccess(ReturnFromServer result) {
							form.setAppHeaderBarAsPerStatus();
							form.setToViewState();
							form.getTbstatus().setValue(model.getStatus());
							form.showDialogMessage("Document Cancelled...!!");
						}
					});
				}
		}
			
			
			
			public void changeStatus()
			{
				final int invoiceid=Integer.parseInt(form.getIbinvoiceid().getValue().trim());
				final String invoiceStatus=form.getTbstatus().getValue().trim();
				
				MyQuerry querry = new MyQuerry();
			  	Company c = new Company();
			  	Vector<Filter> filtervec=new Vector<Filter>();
			  	Filter filter = null;
			  	filter = new Filter();
			  	filter.setQuerryString("companyId");
				filter.setLongValue(c.getCompanyId());
				filtervec.add(filter);
				filter = new Filter();
				filter.setQuerryString("invoiceCount");
				filter.setIntValue(invoiceid);
				filtervec.add(filter);
				querry.setFilters(filtervec);
				querry.setQuerryObject(new CustomerPayment());
				async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
		  			
		  			@Override
		  			public void onFailure(Throwable caught) {
		  			}

		  			@Override
					public void onSuccess(ArrayList<SuperModel> result) {
		  				
		  				for(SuperModel smodel:result)
							{
		  					CustomerPayment custpay = (CustomerPayment)smodel;
								custpay.setStatus(CustomerPayment.CANCELLED);
								custpay.setComment("Invoice Id ="+invoiceid+" "+"Invoice Status ="+invoiceStatus+"\n"
								+"has been cancelled by "+LoginPresenter.loggedInUser.trim()
								+" with remark"+"\n"+"Remark ="+popup.getRemark().getValue());
								async.save(custpay,new AsyncCallback<ReturnFromServer>() {
		//
									@Override
									public void onFailure(Throwable caught) {
										form.showDialogMessage("An Unexpected Error occured !");
//										
									}
									@Override
									public void onSuccess(ReturnFromServer result) {
									}
								});
							}
		  				
		  			}
				
				});
			}
			
			protected void createNewInvoice()
			{
				/**
				 * Date : 28-11-2017 By ANIL
				 * updated invoice cancellation as if invoice is in approved state we need to check accounting interface and update accourdingly
				 * else cancel invoice only
				 */
//				if(model.getStatus().equals(Invoice.APPROVED)){
//				
//					final int invoiceid=Integer.parseInt(form.getIbinvoiceid().getValue().trim());
//					final String invoiceStatus=form.getTbstatus().getValue().trim();
//					
//					MyQuerry querry = new MyQuerry();
//				  	Company c = new Company();
//				  	Vector<Filter> filtervec=new Vector<Filter>();
//				  	Filter filter = null;
//				  	filter = new Filter();
//				  	filter.setQuerryString("companyId");
//					filter.setLongValue(c.getCompanyId());
//					filtervec.add(filter);
//					filter = new Filter();
//					filter.setQuerryString("documentID");
//					filter.setIntValue(invoiceid);
//					filtervec.add(filter);
//					
//					filter = new Filter();
//					filter.setQuerryString("documentType");
//					filter.setStringValue("Invoice");
//					filtervec.add(filter);
//					
//					querry.setFilters(filtervec);
//					querry.setQuerryObject(new AccountingInterface());
//					async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
//			  			
//			  			@Override
//			  			public void onFailure(Throwable caught) {
//			  			}
//	
//			  			@Override
//						public void onSuccess(ArrayList<SuperModel> result) {
//			  				
//			  				for(SuperModel smodel:result)
//								{
//			  					AccountingInterface accntInt = (AccountingInterface)smodel;
//			  					
//			  					if(accntInt.getStatus().equalsIgnoreCase(AccountingInterface.TALLYSYNCED)){
//			  						accntInt.setStatus(AccountingInterface.DELETEFROMTALLY);
//				  					accntInt.setRemark("Invoice Id ="+invoiceid+" "+"Invoice Status ="+invoiceStatus+"\n"
//										+"has been cancelled by "+LoginPresenter.loggedInUser.trim()
//										+" with remark"+"\n"+"Remark ="+popup.getRemark().getValue());
//			  																																}
//			  					else{
//			  						accntInt.setStatus(AccountingInterface.CANCELLED);
//				  					accntInt.setRemark("Invoice Id ="+invoiceid+" "+"Invoice Status ="+invoiceStatus+"\n"
//										+"has been cancelled by "+LoginPresenter.loggedInUser.trim()
//										+" with remark"+"\n"+"Remark ="+popup.getRemark().getValue());
//			  					}
//			  					async.save(accntInt,new AsyncCallback<ReturnFromServer>() {
//										@Override
//										public void onFailure(Throwable caught) {
//											form.showDialogMessage("An Unexpected Error occured !");
//										}
//										@Override
//										public void onSuccess(ReturnFromServer result) {
//										}
//									});
//								}
//			  			}
//					
//					});
//				}
				/**
				 * Date : 28-11-2017 BY ANIL
				 */
				Console.log("model.getStatus() ==========="+model.getStatus());
				if(model.getStatus().equals(Invoice.APPROVED)){
					
					if(model.getIrnStatus()!=null&&!model.getIrnStatus().equals("")) {		
						Console.log("model.getIrnStatus() ==="+model.getIrnStatus());

//						if(model.getIrnStatus().equals("Active")) {
							
							model.setStatus(Invoice.CANCELLED);
							/**
							 * Updated By:Viraj
							 * Date: 15-06-2019
							 * Description: To set remark on cancellation of invoice
							 */
							model.setRemark("This document was cancelled with remark "+popup.getRemark().getValue());
							/** Ends **/
							if(model.getComment()!=null){
							model.setComment(model.getComment()+"\n"+"Invoice Id ="+model.getCount()+" "+"Invoice Status ="+model.getStatus()+"\n"
												+"has been cancelled by "+LoginPresenter.loggedInUser.trim()
												+" with remark"+"\n"+"Remark ="+popup.getRemark().getValue());
							}else{
								model.setComment("Invoice Id ="+model.getCount()+" "+"Invoice Status ="+model.getStatus()+"\n"
										+"has been cancelled by "+LoginPresenter.loggedInUser.trim()
										+" with remark"+"\n"+"Remark ="+popup.getRemark().getValue());
							}
							model.setCancelledBy(LoginPresenter.loggedInUser.trim());		
							
							if(form.eInvoiceCancellationDate.getValue()!=null) {
								Console.log("form.eInvoiceCancellationDate.getValue() "+form.eInvoiceCancellationDate.getValue());
								model.setIrnCancellationDate(form.eInvoiceCancellationDate.getValue());
							}
							createNewEntryInAccountingInterfaceWithCancelledStatus(model);

//						}
					}else{
						model.setStatus(Invoice.CREATED);
						commonservice.deleteEntryFromAccountingInterface(model, new AsyncCallback<String>() {
							
							@Override
							public void onSuccess(String result) {
								// TODO Auto-generated method stub
								Console.log("delete entry in accounting interface"+result);
							}
							
							@Override
							public void onFailure(Throwable caught) {
								// TODO Auto-generated method stub
								
							}
						});
					}
				}else{
					
					model.setStatus(Invoice.CANCELLED);
					/**
					 * Updated By:Viraj
					 * Date: 15-06-2019
					 * Description: To set remark on cancellation of invoice
					 */
					model.setRemark("This document was cancelled with remark "+popup.getRemark().getValue());
					/** Ends **/
					if(model.getComment()!=null){
					model.setComment(model.getComment()+"\n"+"Invoice Id ="+model.getCount()+" "+"Invoice Status ="+model.getStatus()+"\n"
										+"has been cancelled by "+LoginPresenter.loggedInUser.trim()
										+" with remark"+"\n"+"Remark ="+popup.getRemark().getValue());
					}else{
						model.setComment("Invoice Id ="+model.getCount()+" "+"Invoice Status ="+model.getStatus()+"\n"
								+"has been cancelled by "+LoginPresenter.loggedInUser.trim()
								+" with remark"+"\n"+"Remark ="+popup.getRemark().getValue());
					}
					model.setCancelledBy(LoginPresenter.loggedInUser.trim());//15-09-2022
					
					if(model.getStatus().equals(Invoice.CANCELLED)){
						createNewEntryInAccountingInterfaceWithCancelledStatus(model);
					}

				}
				
				Console.log("invoice status for cancellation "+model.getStatus());
				async.save(model,new AsyncCallback<ReturnFromServer>() {
					@Override
					public void onFailure(Throwable caught) {
						form.showDialogMessage("An Unexpected Error occured !");
//						
					}
					@Override
					public void onSuccess(ReturnFromServer result) {
						Console.log("Invoice cancelled successfully");
						form.setAppHeaderBarAsPerStatus();
						form.setToViewState();
						form.getTbstatus().setValue(model.getStatus());
						
						/**
						 * Updated By:Viraj
						 * Date: 15-06-2019
						 * Description: To set remark on cancellation of invoice
						 */
						form.getTbremark().setValue(model.getRemark());
						/** Ends **/
					}
				});
			}
			
			/*********************************Approval Logic*********************************/
			
			
			private void reactOnApprovalVal()
			{
				boolean groupValidation=form.checkForInvGroup();
				if(groupValidation==false&&form.getOlbinvoicegroup().getSelectedIndex()==0){
					form.showDialogMessage("Invoice Group is Mandatory!");
					form.hideWaitSymbol();//vijay
				}
				else{
					
					/**
					 * Rohan added this code Only for NBHC 
					 * Date : 17-04-2017
					 * This is used to validate invoice date is not after todays date 
					 */
					
					if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "OnlyForNBHC")){
						Date todaysDate = new Date();
						Date invDate = model.getInvoiceDate();
							if(invDate.after(todaysDate)){
								form.showDialogMessage("Invoice Date should not be greater than todays date..!");
								form.hideWaitSymbol();
							}
							else{
								reactOnReqApproval();
							}
					}
					else{
						reactOnReqApproval();
					}
					
					/**
					 * ends here 
					 */
					
				}
			}
			
			private void reactOnReqApproval()
			{
				
				/**
				 * Updated By Anil Date : 14-10-2016
				 * Release : 30 Sept 2016 
				 * Project : PURCHASE MODIFICATION(NBHC)
				 */
				String documentCreatedBy="";
				if(model.getCreatedBy()!=null){
					documentCreatedBy=model.getCreatedBy();
				}
				
				
				if(model.getCount()!=0)
				{
//					form.showWaitSymbol(); //Vijay
					ApproverFactory appFactory=new ApproverFactory();
					/**
					 *  nidhi
					 *  7-10-2017
					 *  method change for display name and id
					 */
					Approvals approval=appFactory.getApprovals(model.getEmployee(), model.getApproverName()
							, model.getBranch(), model.getCount(),documentCreatedBy,model.getCustomerId()+"",model.getPersonInfo().getFullName());
					saveApprovalReq(approval,ConcreteBusinessProcess.REQUESTED,"Request Sent!");
					reactOnApprovalEmail(approval);
				}
			}
			
			private void saveApprovalReq(Approvals approval,final String status,final String dialogmsg)
			{
				service.save(approval, new AsyncCallback<ReturnFromServer>() {
					
					@Override
					public void onSuccess(ReturnFromServer result) 
					{
						model.setStatus(status);
						service.save(model, new AsyncCallback<ReturnFromServer>() {

							@Override
							public void onFailure(Throwable caught) {
								form.hideWaitSymbol();
							}

							@Override
							public void onSuccess(ReturnFromServer result) {
								form.getTbstatus().setValue(status);
								form.setToViewState();
								form.showDialogMessage(dialogmsg);
								form.hideWaitSymbol();
							}
						});
					}
					
					@Override
					public void onFailure(Throwable caught) {
						form.showDialogMessage("Request Failed! Try Again!");
						form.hideWaitSymbol();
						
					}
				});
			}
			

			private void reactOnCancelApproval()
			{
		  		form.showWaitSymbol(); 
		  		ApproverFactory approverfactory=new ApproverFactory();
				  Filter filter=new Filter();
				  filter.setStringValue(approverfactory.getBuisnessProcessType());
				  filter.setQuerryString("businessprocesstype");
				  Filter filterOnid=new Filter();
				  filterOnid.setIntValue(model.getCount());
				  filterOnid.setQuerryString("businessprocessId");
				  Vector<Filter> vecfilter=new Vector<Filter>();
				  vecfilter.add(filter);
				  vecfilter.add(filterOnid);
				  MyQuerry querry=new MyQuerry(vecfilter, new Approvals());
				  final ArrayList<SuperModel> array = new ArrayList<SuperModel>();
				  service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {

					@Override
					public void onFailure(Throwable caught) {
						caught.printStackTrace();
						
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						if(result == null || result.size() == 0){

							model.setStatus( ConcreteBusinessProcess.CREATED);
							service.save(model, new AsyncCallback<ReturnFromServer>() {

								@Override
								public void onFailure(Throwable caught) {
									form.hideWaitSymbol();
								}

								@Override
								public void onSuccess(ReturnFromServer result) {
									form.getTbstatus().setValue(ConcreteBusinessProcess.CREATED);
									form.setToViewState();
									form.showDialogMessage("Approval Request Cancelled.");
									form.hideWaitSymbol();
								}
							});
						
						}else{
							
						
						array.addAll(result);
						Approvals approval=(Approvals) array.get(0);
						approval.setStatus(Approvals.CANCELLED);
						 
						saveApprovalReq(approval, ConcreteBusinessProcess.CREATED, "Approval Request Cancelled!");
						}
					}
				});
			  
			}

			public void reactOnApprovalEmail(Approvals approvalEntity)
			{
				//Quotation Entity patch patch patch
				emailService.initiateApprovalEmail(approvalEntity,new AsyncCallback<Void>() {

					@Override
					public void onFailure(Throwable caught) {
						Window.alert("Unable To Send Email");
						caught.printStackTrace();
					}

					@Override
					public void onSuccess(Void result) {
//						Window.alert("Email Sent Sucessfully !");
					}
				});
			}

		
//     rohan added this code for invoice editing ************* 			
			
			
			@Override
			public void onRowCountChange(RowCountChangeEvent event) {

				System.out.println("INSIDE RoW COUNT CHANGE METHOD....!!!");
				Console.log("INSIDE RoW COUNT CHANGE METHOD....!!!");
			
			if (event.getSource() == form.getSalesProductTable().getTable()) {
				
				if(form.getSalesProductTable().getDataprovider().getList().size()!=0)
				{
					Console.log("INSIDE IF CONDITION....!!!");
				double retrieveBillAmt = form.getSalesProductTable().calculateTotalBillAmt();
				// form.getDototalbillamt().setValue(retrieveBillAmt);

				billingTaxTableOnRowChange();
				retrieveBillAmt = retrieveBillAmt+ form.getInvoiceTaxTable().calculateTotalTaxes();

//				if (multipleContractBillingInvoiceFlag == false) {
//					Console.log("PRODUCT FALSE....!!!");
//					System.out.println("PRODUCT FALSE....!!!");
//					billingTableOnRowChange(retrieveBillAmt);
//				}
//
//				if (multipleContractBillingInvoiceFlag == true) {
//					System.out.println("PRODUCT TRUE....!!!");
//					Console.log("PRODUCT TRUE....!!!");
//					reactOnProductsChange();
//				}

				if(checkproductstatus()){
					System.out.println("PRODUCT TRUE....!!!");
					reactOnProductsChange(false);
				}else{
					System.out.println("Inside else=================");
					retrieveproductinfomation(false);
				}
				
				}
				
				/**
				 * @author Anil @since 16-11-2021
				 * Updating price and tax details on sales oreder summary
				 * Sunrise SCM
				 */
				try{
					updateSalesOrderSummary();
				}catch(Exception e){
					
				}
				
				System.out.println("Size in side row count change 123"+form.getSalesProductTable().getDataprovider().getList().size());
			}
			
			/**
			 * Date : 21-09-2017 By ANIL
			 */
			double totalOfProductPrice=0;
			if(event.getSource().equals(form.tblOtherCharges.getTable())){
				NumberFormat nf = NumberFormat.getFormat("0.00");
				
				
				if(form.tblOtherCharges.getValue().size()!=0){
				totalOfProductPrice= form.getDbtotalAmt().getValue();
				for(OtherCharges obj:form.getTblOtherCharges().getValue()){
					double otherChargAmt=0;
					OtherTaxCharges otherTax=form.tblOtherCharges.getOtherChargesDetails(obj.getOtherChargeName());
					if(otherTax!=null){
						if(otherTax.getOtherChargePercent()!=0){
							otherChargAmt=((totalOfProductPrice*otherTax.getOtherChargePercent())/100);
							obj.setAmount(otherChargAmt);
						}else if(otherTax.getOtherChargeAbsValue()!=null){
							if(obj.getAmount()==0){
							otherChargAmt=otherTax.getOtherChargeAbsValue();
							obj.setAmount(otherChargAmt);
							}
						}
					}
					
					
				}
				
				}
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				form.invoiceTaxTable.connectToLocal();
				try {
					form.addProdTaxes();
					form.addOtherChargesInTaxTbl();
					form.dbOtherChargesTotal.setValue(form.tblOtherCharges.calculateOtherChargesSum());
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				double totalIncludingTax=0;
				if(form.getDbFinalTotalAmt().getValue()!=null){
					totalIncludingTax = form.getDbFinalTotalAmt().getValue()+ form.invoiceTaxTable.calculateTotalTaxes();
				}else{
					totalIncludingTax = form.invoiceTaxTable.calculateTotalTaxes();
				}
				if(form.dbOtherChargesTotal.getValue()!=null){
					totalIncludingTax=totalIncludingTax+form.dbOtherChargesTotal.getValue();
				}
				form.getDbtotalAmtIncludingTax().setValue(totalIncludingTax);
				form.setToatalAmountIncludingTax(Double.parseDouble(nf.format(totalIncludingTax)));
				
				double netPay = fillNetPayable(form.getToatalAmountIncludingTax());
				if(!LoginPresenter.PC_NO_ROUNDOFF_INVOICE_CONTRACTFlag){
					netPay = Math.round(netPay);
					int netPayable = (int) netPay;
					netPay = netPayable;
				}
				
				form.getDototalbillamt().setValue(Double.parseDouble(nf.format(netPay)));

				if(!form.getTbroundOffAmt().getValue().equals("") && form.getTbroundOffAmt().getValue()!=null){
					String roundOff = form.getTbroundOffAmt().getValue();
					double roundoffAmt =  AppUtility.calculateRoundOff(roundOff, netPay);
					if(roundoffAmt!=0){
						form.getNetPayable().setValue(roundoffAmt);
						form.getDoinvoiceamount().setValue(roundoffAmt);

					}else{
						form.getNetPayable().setValue(roundoffAmt);
						form.getTbroundOffAmt().setValue("");
						form.getDoinvoiceamount().setValue(roundoffAmt);
					}
				}
				else{
					form.getNetPayable().setValue(netPay);
					form.getDoinvoiceamount().setValue(Double.parseDouble(nf.format(netPay)));
				}

			}
				
			/**
			 * End
			 */
			
			System.out.println("Size in side row count change 456"+form.getSalesProductTable().getDataprovider().getList().size());
			
			
			/**
			 * @author Anil
			 * @since 10-02-2022
			 * For innovative we have made tax table total invoice amount column editable
			 * Raised by Nitin Sir and Nithila
			 */
			
			if(event.getSource()==form.invoiceTaxTable.getTable()){
				Company comp=LoginPresenter.company;
				boolean thaiClientFlag=false;
				if(comp!=null&&(comp.getAddress().getCountry().trim().equalsIgnoreCase("Thailand")||comp.getAddress().getCountry().trim().equalsIgnoreCase("ประเทศไทย"))){
					thaiClientFlag=true;
				}
				
				if(thaiClientFlag){
					double totalIncludingTax=0;
					if(form.getDbFinalTotalAmt().getValue()!=null){
						totalIncludingTax = form.getDbFinalTotalAmt().getValue()+ form.invoiceTaxTable.calculateTotalTaxAmount();
					}else{
						totalIncludingTax = form.invoiceTaxTable.calculateTotalTaxAmount();
					}
					if(form.dbOtherChargesTotal.getValue()!=null){
						totalIncludingTax=totalIncludingTax+form.dbOtherChargesTotal.getValue();
					}
					form.getDbtotalAmtIncludingTax().setValue(totalIncludingTax);
					form.setToatalAmountIncludingTax(totalIncludingTax);
					
//					double netPay = fillNetPayable(form.getToatalAmountIncludingTax());
					double netPay =0;
					if(form.getInvoiceChargesTable().getDataprovider().getList().size()==0){
						netPay=form.getToatalAmountIncludingTax();
					}
					if(form.getInvoiceChargesTable().getDataprovider().getList().size()!=0){
						netPay=form.getToatalAmountIncludingTax()+form.getInvoiceChargesTable().calculateTotalBillingCharges();
					}
					
//					netPay = Math.round(netPay);
//					int netPayable = (int) netPay;
//					netPay = netPayable;
					form.getDototalbillamt().setValue(netPay);

					if(!form.getTbroundOffAmt().getValue().equals("") && form.getTbroundOffAmt().getValue()!=null){
						String roundOff = form.getTbroundOffAmt().getValue();
						double roundoffAmt =  AppUtility.calculateRoundOff(roundOff, netPay);
						if(roundoffAmt!=0){
							form.getNetPayable().setValue(roundoffAmt);
							form.getDoinvoiceamount().setValue(roundoffAmt);

						}else{
							form.getNetPayable().setValue(roundoffAmt);
							form.getTbroundOffAmt().setValue("");
							form.getDoinvoiceamount().setValue(roundoffAmt);
						}
					}
					else{
						form.getNetPayable().setValue(netPay);
						form.getDoinvoiceamount().setValue(netPay);
					}
				}
				
			}
			
			}
			
			
			private void billingTaxTableOnRowChange()
			{
				List<SalesOrderProductLineItem> salesProdlis=form.getSalesProductTable().getDataprovider().getList();
				for(int i=0;i<salesProdlis.size();i++)
				{
					List<ContractCharges> updateBillLis=updateTaxesTable(salesProdlis.get(i).getPaymentPercent(),salesProdlis.get(i).getIndexVal());
					form.getInvoiceTaxTable().setValue(updateBillLis);
				}
				double retrieveAmt=form.getSalesProductTable().calculateTotalBillAmt()+form.getInvoiceTaxTable().calculateTotalTaxes()+form.getInvoiceChargesTable().calculateTotalBillingCharges();
				retrieveAmt=Math.round(retrieveAmt);
				form.getDototalbillamt().setValue(retrieveAmt);
			}
			
			private List<ContractCharges> updateTaxesTable(double payPercentValue,int indexValue)
			{
				NumberFormat nf=NumberFormat.getFormat("0.00");
				
				List<ContractCharges> taxList=form.getInvoiceTaxTable().getDataprovider().getList();
				ArrayList<ContractCharges> taxArr=new ArrayList<ContractCharges>();
				double updatedAssess=0;
				for(int i=0;i<taxList.size();i++)
				{
					ContractCharges entity=new ContractCharges();
					entity.setTaxChargeName(taxList.get(i).getTaxChargeName());
					entity.setTaxChargePercent(taxList.get(i).getTaxChargePercent());
					if(taxList.get(i).getIdentifyTaxCharge()==indexValue){
						updatedAssess=taxList.get(i).getTaxChargeAssesVal()*payPercentValue/100;
						updatedAssess=Double.parseDouble(nf.format(updatedAssess));
						entity.setTaxChargeAssesVal(updatedAssess);
					}
					else{
						entity.setTaxChargeAssesVal(taxList.get(i).getTaxChargeAssesVal());
					}
					
					taxArr.add(entity);
				}
				return taxArr;
			}
			
			
			public boolean checkproductstatus() {
				if (form.salesProductTable.getDataprovider().getList().get(0).getPrduct() != null) {
					return true;
				}
				return false;
			}

			public void retrieveproductinfomation(final boolean saveFlag) {
				final ArrayList<SuperProduct> productlist = new ArrayList<SuperProduct>();

				for (int k = 0; k < form.salesProductTable.getDataprovider().getList().size(); k++) {
//					List<Integer> productIdlist = new ArrayList<Integer>();
					int prodid = form.salesProductTable.getDataprovider().getList().get(k).getProdId();

					System.out.println("Product id ::::: " + prodid);
					MyQuerry querry = new MyQuerry();
					Vector<Filter> filtervec = new Vector<Filter>();
					Filter filter = null;

					filter = new Filter();
					filter.setQuerryString("count");
					filter.setIntValue(prodid);
					filtervec.add(filter);

					filter = new Filter();
					filter.setLongValue(model.getCompanyId());
					filter.setQuerryString("companyId");
					filtervec.add(filter);

					querry.setFilters(filtervec);
					querry.setQuerryObject(new SuperProduct());

					async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							System.out.println("RESULT LIST SIZE :: "+ result.size());
							counter++;
							for (SuperModel model : result) {
								SuperProduct entity = (SuperProduct) model;
								productlist.add(entity);
							}
							if (counter == form.salesProductTable.getDataprovider().getList().size()) {
								System.out.println("Product List SIZE ::: "+ productlist.size());
								for (int i = 0; i < form.salesProductTable.getDataprovider().getList().size(); i++) {
									for (int j = 0; j < productlist.size(); j++) {
										if (form.salesProductTable.getDataprovider().getList().get(i).getProdId() == productlist.get(j).getCount()) {
											form.salesProductTable.getDataprovider().getList().get(i).setPrduct(productlist.get(j));
										}
									}
								}
								reactOnProductsChange(saveFlag);
							}
						}
						@Override
						public void onFailure(Throwable caught) {
							form.showDialogMessage("An Unexpected Error occured !");
						}
					});
				}
			}

			
			
		private void reactOnProductsChange(final boolean saveFlag) {
				
//				form.showWaitSymbol();
				Timer timer = new Timer() {
					@Override
					public void run() {
						NumberFormat nf = NumberFormat.getFormat("0.00");
						double totalExcludingTax = form.getSalesProductTable().calculateTotalExcludingTax();
						totalExcludingTax = Double.parseDouble(nf.format(totalExcludingTax));
						form.setTotalAmount(totalExcludingTax);
						
						// Date 04-09-2017 added by vijay
						form.getDbtotalAmt().setValue(totalExcludingTax);
						
						System.out.println("TOTAL AMOUNT EXCLUDING :: "+ form.getTotalAmount());
						

						 /**
					     * Date 03-09-2017 added by vijay for final total amount after discount
					     */
					    if(form.getDbDiscountAmt().getValue()!=null && form.getDbDiscountAmt().getValue()!=0){
				    		if(form.getDbDiscountAmt().getValue()>=totalExcludingTax){
					    		form.getDbDiscountAmt().setValue(0d);
				    		}
				    		else if(form.getDbDiscountAmt().getValue()<=totalExcludingTax){
						    	form.getDbFinalTotalAmt().setValue(totalExcludingTax - form.getDbDiscountAmt().getValue());
				    		}
				    		else{
					    		form.getDbFinalTotalAmt().setValue(totalExcludingTax);
				    		}
				    	}
					    else{
					    	form.getDbFinalTotalAmt().setValue(totalExcludingTax);
				    	}
					    
					    /**
					     * ends here
					     */

						try {
							System.out.println();
							System.out.println("PRODUCT TAX TABLE CALCULATION BEGIN.....");
							form.invoiceTaxTable.connectToLocal();
							form.addProdTaxes();
							
							/**
							 * Date : 21-09-2017 By ANIL
							 * Calculating tax On Other charges 
							 */
							form.addOtherChargesInTaxTbl();
							form.dbOtherChargesTotal.setValue(form.tblOtherCharges.calculateOtherChargesSum());
							
							/**
							 * End
							 */
//							double totalIncludingTax = form.getTotalAmount()+ form.invoiceTaxTable.calculateTotalTaxes();
							
							// Date 03-09-2017 added by vijay old line above commented
							double totalIncludingTax = form.getDbFinalTotalAmt().getValue()+ form.invoiceTaxTable.calculateTotalTaxes();
							form.getDbtotalAmtIncludingTax().setValue(totalIncludingTax);
							Console.log("totalIncludingTax "+totalIncludingTax);
							if(LoginPresenter.PC_NO_ROUNDOFF_INVOICE_CONTRACTFlag){
								double updatedAmt = apputility.getLastTwoDecimalOnly(totalIncludingTax);
								Console.log("updatedAmt "+updatedAmt);
								form.setToatalAmountIncludingTax(updatedAmt);
								form.getDbtotalAmtIncludingTax().setValue(updatedAmt);
							}
							else {
								form.setToatalAmountIncludingTax(Double.parseDouble(nf.format(totalIncludingTax)));
							}

							System.out.println("total"+totalIncludingTax);
							
							
//							form.getBillingChargesTable().connectToLocal();
//							form.updateChargesTable();
						} catch (Exception e) {
							e.printStackTrace();
						}
						double netPay = fillNetPayable(form.getToatalAmountIncludingTax());
						
						System.out.println("net pay"+netPay);
						if(!LoginPresenter.PC_NO_ROUNDOFF_INVOICE_CONTRACTFlag){
							netPay = Math.round(netPay);
							int netPayable = (int) netPay;
							netPay = netPayable;
							form.getDototalbillamt().setValue(Double.parseDouble(nf.format(netPay)));
						}
						else{
							netPay = form.getDbtotalAmtIncludingTax().getValue();
							Console.log("net payable amt "+netPay);
							form.getDototalbillamt().setValue(netPay);
						}
						//  rohan added following for discount on product change
//						//      **********start ****************
//					
//						System.out.println("Discount value "+form.getDiscount().getValue());
//						if(form.getDiscount().getValue()!=null)
//						{
//							System.out.println("1111111"+netPay);
//							int pay =0;
//							pay =(int) (netPay - form.getDiscount().getValue());
//							pay = Math.round(pay);
//							
//							form.getNetPayable().setValue(Double.parseDouble(nf.format(pay)));
//							form.getDoinvoiceamount().setValue(Double.parseDouble(nf.format(pay)));
//						}
//						else{
//							form.getDiscount().setValue(0.0);
//							form.getNetPayable().setValue(Double.parseDouble(nf.format(netPay)));
//							form.getDoinvoiceamount().setValue(Double.parseDouble(nf.format(netPay)));
//						}
//						
						// ***************ends here ****************
						
						// above commented by vijay 	
						/** Date 01-09-2017 added by vijay for final netpayable amount after discount **/

						if(!form.getTbroundOffAmt().getValue().equals("") && form.getTbroundOffAmt().getValue()!=null){
							String roundOff = form.getTbroundOffAmt().getValue();
							double roundoffAmt =  AppUtility.calculateRoundOff(roundOff, netPay);
							if(roundoffAmt!=0){
								form.getNetPayable().setValue(roundoffAmt);
								form.getDoinvoiceamount().setValue(roundoffAmt);

							}else{
								form.getNetPayable().setValue(roundoffAmt);
								form.getTbroundOffAmt().setValue("");
								form.getDoinvoiceamount().setValue(roundoffAmt);
							}
						}
						else{
							form.getNetPayable().setValue(netPay);
							form.getDoinvoiceamount().setValue(Double.parseDouble(nf.format(netPay)));
						}
						/** ends here *******/
						
//						if(multipleContractBillingInvoiceFlag==false){
//							Console.log("INSIDE multipleContractBillingInvoiceFlag....!!!");
//							billingTableOnRowChange(Double.parseDouble(nf.format(netPay)));
//						}
//						try {
//							form.hideWaitSymbol();
//						} catch (Exception e) {
//						}
						
						if(saveFlag){
							Console.log("Saving....");
							reactOnSave();
						}
					}
				};
				timer.schedule(3000);
			}	
		
		private double fillNetPayable(double amtincltax)
		{
			double amtval=0;
			if(form.getInvoiceChargesTable().getDataprovider().getList().size()==0)
			{
				amtval=amtincltax;
			}
			if(form.getInvoiceChargesTable().getDataprovider().getList().size()!=0)
			{
				amtval=amtincltax+form.getInvoiceChargesTable().calculateTotalBillingCharges();
			}
			if(!LoginPresenter.PC_NO_ROUNDOFF_INVOICE_CONTRACTFlag){
				amtval=Math.round(amtval);
				int retAmtVal=(int) amtval;
				amtval=retAmtVal;
				System.out.println("rohan in side net payble "+amtval);
			}
			
			return amtval;
			
		}

		//Date 04-09-2017 commented by vijay handled in form with new code
		
//		@Override
//		public void onValueChange(ValueChangeEvent<Double> event) {
//			System.out.println("rohna in side value change handler");
//			NumberFormat nf = NumberFormat.getFormat("0.00");
//			if(event.getSource()== form.getDiscount()){
//				if(form.getDiscount()!=null)
//				{
//					System.out.println("rohna in side event");
//					int netPayable =0;
//					netPayable= (int) (form.getDototalbillamt().getValue()-form.getDiscount().getValue());
//					
//					form.getNetPayable().setValue(Double.parseDouble(nf.format(netPayable)));
//					form.getDoinvoiceamount().setValue(Double.parseDouble(nf.format(netPayable)));
//				}
//			}
//		}
		
		/**
		 * Date : 09-11-2017 BY ANIL
		 * if the process configuration is active then reference number is mandatory 
		 * at the time of sending approval or submitting the invoice
		 */
		private boolean isRefNumIsMandatory(){
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "MakeReferenceNumberMandatory")){
				if(model.getRefNumber()!=null&&!model.getRefNumber().equals("")){
					return true;
				}else{
					form.showDialogMessage("Reference number is manadatory!");
					return false;
				}
			}else{
				return true;
			}
		}
		
		
		/**
		 * Date : 21-11-2017 By ANIL
		 *
		 */
		@Override
		public void reactOnSave() {
			super.reactOnSave();
			Console.log("Invoice Presenter onsave called");
			
			if(form.updatePaymentFlag){
				Invoice invoice=model;
				if(form.custPayment!=null){
					updateExistingPayment(invoice,form.custPayment,true);
				}else{
					updateExistingPayment(invoice,form.custPayment,false);
//					invoice.createPaymentDetails();
				}
			}
			
			/**
			 * Date 26-07-2018 By Vijay
			 * Des :- For Pepcopp Invoice edited by other than Admin roll then email must send to their email Id if netpayable and invoice date changed
			 * this is customized 
			 */
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "EnableInvoiceEditableForAllRoles")){	
				if(model.getStatus().equals(Invoice.APPROVED)){
					if(!form.dbinvoicedate.getValue().equals(form.oldinvoiceDate) || form.netPayable.getValue()!=form.oldNetPayable && !LoginPresenter.myUserEntity.getRole().getRoleName().equals("ADMIN")){
					
						String customerName ="";
						if(model.getPersonInfo().getFullName()!=null){
							customerName = model.getPersonInfo().getFullName();
						}
						generalAsyn.sendInvoiceEditedEmail(model.getCount(), model.getBranch(), LoginPresenter.loggedInUser, form.oldNetPayable, form.netPayable.getValue(), form.oldinvoiceDate, form.dbinvoicedate.getValue(),model.getCompanyId(),customerName, new AsyncCallback<Boolean>() {
							@Override
							public void onSuccess(Boolean result) {
								form.oldinvoiceDate = form.dbinvoicedate.getValue();
								form.oldNetPayable = form.netPayable.getValue();
							}
							
							@Override
							public void onFailure(Throwable caught) {
							}
						});
						
					}
				}
		   }
			/**
			 * ends here
			 */
		}

		private void updateExistingPayment(Invoice invoice,CustomerPayment paydetails,boolean updateFlag) {
			if(!updateFlag){
				paydetails=new CustomerPayment();
			}
			paydetails.setPersonInfo(invoice.getPersonInfo());
			paydetails.setContractCount(invoice.getContractCount());
			if(invoice.getContractStartDate()!=null){
				paydetails.setContractStartDate(invoice.getContractStartDate());
			}
			if(invoice.getContractEndDate()!=null){
				paydetails.setContractEndDate(invoice.getContractEndDate());
			}
			paydetails.setTotalSalesAmount(invoice.getTotalSalesAmount());
			paydetails.setArrayBillingDocument(invoice.getArrayBillingDocument());
			paydetails.setTotalBillingAmount(invoice.getInvoiceAmount());
			paydetails.setInvoiceAmount(invoice.getInvoiceAmount());
			paydetails.setInvoiceCount(invoice.getCount());
			paydetails.setInvoiceDate(invoice.getInvoiceDate());
			paydetails.setInvoiceType(invoice.getInvoiceType());
			paydetails.setPaymentDate(invoice.getPaymentDate());
			double invoiceamt=invoice.getInvoiceAmount();
			int payAmt=(int)(invoiceamt);
			paydetails.setPaymentAmt(payAmt);
			paydetails.setPaymentMethod(invoice.getPaymentMethod());
			paydetails.setTypeOfOrder(invoice.getTypeOfOrder());
			paydetails.setStatus(CustomerPayment.CREATED);
			paydetails.setOrderCreationDate(invoice.getOrderCreationDate());
			paydetails.setBranch(invoice.getBranch());
			paydetails.setAccountType(invoice.getAccountType());
			paydetails.setEmployee(invoice.getEmployee());
			paydetails.setCompanyId(invoice.getCompanyId());
			paydetails.setOrderCformStatus(invoice.getOrderCformStatus());
			paydetails.setOrderCformPercent(invoice.getOrderCformPercent());
			if(invoice.getPaymentMethod()!= null && !invoice.getPaymentMethod().equals("")){
				if(invoice.getPaymentMethod().equalsIgnoreCase("Cheque")){
					paydetails.setChequeIssuedBy(invoice.getPersonInfo().getFullName());
				}else{
					paydetails.setChequeIssuedBy("");
				}
			}else{
				paydetails.setChequeIssuedBy("");
			}
			if(invoice.getNumberRange()!=null){
				paydetails.setNumberRange(invoice.getNumberRange());
			}
			if(invoice.getBillingPeroidFromDate()!=null)
				paydetails.setBillingPeroidFromDate(invoice.getBillingPeroidFromDate());
			if(invoice.getBillingPeroidToDate()!=null)
				paydetails.setBillingPeroidToDate(invoice.getBillingPeroidToDate());
			if(invoice.getSegment()!=null){
				paydetails.setSegment(invoice.getSegment());
			}
			if(invoice.getRateContractServiceId()!=0){
				paydetails.setRateContractServiceId(invoice.getRateContractServiceId());
			}
			if(invoice.getQuantity() > 0){
				paydetails.setQuantity(invoice.getQuantity());
			}
			if(invoice.getUom()!=null){
				paydetails.setUom(invoice.getUom());
			}
			if(invoice.getRefNumber()!=null){
				paydetails.setRefNumber(invoice.getRefNumber());
			}
			paydetails.setNetPay(0);
			paydetails.setPaymentReceived(0);
			
			/**
			 * Date 25-10-2018 By Vijay for updating balance tax and Revenue
			 */
			double taxAmount = 0;
			taxAmount = Math.round(invoice.getTotalAmtIncludingTax() - (invoice.getFinalTotalAmt() + invoice.getTotalOtherCharges()));
			paydetails.setBalanceTax(taxAmount);
			paydetails.setBalanceRevenue(invoice.getNetPayable() - taxAmount);
			/**
			 * ends here
			 */
			 
			async.save(paydetails, new AsyncCallback<ReturnFromServer>( ) {
				@Override
				public void onFailure(Throwable caught) {
					
				}
				@Override
				public void onSuccess(ReturnFromServer result) {
					
				}
			});
			
		}
		
		/**
		 * Date 22/2/2018 
		 * By Jayshree 
		 * Des.Add New Button View tax invoice Button
		 */
		
		
		private void viewTaxInvoiceDocuments() {
			final MyQuerry querry=new MyQuerry();
			Vector<Filter> temp=new Vector<Filter>();
			Filter filter=null;
			
			filter=new Filter();
			filter.setQuerryString("count");
			filter.setIntValue(model.getProformaCount());
			temp.add(filter);
			
			querry.setFilters(temp);
			querry.setQuerryObject(new Invoice());
			form.showWaitSymbol();
			service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>(){

				@Override
				public void onFailure(Throwable caught) {
					form.hideWaitSymbol();
				}

				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					form.hideWaitSymbol();
					if(result.size()==0){
						form.showDialogMessage("No invoice document found. ");
						return;
					}


				
					if(result.size()==1){
						final Invoice billDocument=(Invoice) result.get(0);
						final InvoiceDetailsForm form=InvoiceDetailsPresenter.initalize();
						Timer timer=new Timer() {
							
							@Override
							public void run() {
								form.updateView(billDocument);
								form.setToViewState();
								
								// Date 02-04-2018 By vijay
								form.setAppHeaderBarAsPerStatus();
								
							}
						};
						timer.schedule(1000);
					}
					else{
						InvoiceListPresenter.initalize(querry);
					}
				}
				
			});
		}

		@Override
		public void onChange(ChangeEvent event) {
			if(event.getSource().equals(form.getObjPaymentMode())){
				form.setBankSelectedDetails();
			}
			/** date 27.10.2018 added by komal for tds **/
			if(event.getSource()== form.getDbtdsValue())
			{
				System.out.println("tds amt ="+form.getDbtdsValue().getValue());
				double totalValue = 0 , roundOff = 0 , total = 0;;
				if(form.dbtotalAmtIncludingTax.getValue()!=null){
					totalValue = form.dbtotalAmtIncludingTax.getValue();
				}
				if(form.tbroundOffAmt.getValue()!=null && !form.tbroundOffAmt.getValue().equals("")){
					roundOff = Double.parseDouble(form.tbroundOffAmt.getValue());
				}
				total = Math.round(totalValue + roundOff);
					if(form.getDbtdsValue().getValue()!=null && form.getDbtdsValue().getValue()!=0)
					{
						double tds = form.getDbtdsValue().getValue();
						form.netPayable.setValue(total - tds);					
					}else{
						form.netPayable.setValue(total);
					}
								
				}

		}

		//End By Jayshree
		 /** date 15.10.2018 added by komal for sasha invoices **/
		private boolean checkBillSubType(String billType){
			
			switch(billType){
				case AppConstants.NATIONALHOLIDAY:
				case AppConstants.OVERTIME:
				case AppConstants.STAFFINGANDRENTAL:
				case AppConstants.OTHERSERVICES:
				case AppConstants.CONSUMABLES:
				case AppConstants.FIXEDCONSUMABLES :
				case AppConstants.EQUIPMENTRENTAL:
				case AppConstants.STAFFING:
				case AppConstants.CONSOLIDATEBILL:	
				case AppConstants.ARREARSBILL:	
				case AppConstants.PUBLICHOLIDAY:
					return true;
			}
			return false;		
			}
		/** Date 13.12.2018 added by komal for update accounting interface **/
		private void updateAccountingInterface(){
			generalAsyn.updateAccountingInterface(model, new AsyncCallback<String>() {
				
				@Override
				public void onSuccess(String result) {
					// TODO Auto-generated method stub
					form.showDialogMessage(result);
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					
				}
			});
		}
		
		
		private void setEmailPopUpData() {
			form.showWaitSymbol();
			String customerEmail = "";

			String branchName = form.olbbranch.getValue();
			String fromEmailId = AppUtility.getFromEmailAddress(branchName,model.getEmployee());
			customerEmail = AppUtility.getCustomerEmailid(model.getPersonInfo().getCount());
			
			String customerName = model.getPersonInfo().getFullName();
			if(!customerName.equals("") && !customerEmail.equals("")){
				label : AppUtility.loadContactPersonlist(emailpopup.olbContactlistEmailId,"Customer",model.getPersonInfo().getCount(),customerName,customerEmail,null);
				emailpopup.taToEmailId.setValue(customerEmail);
			}
			else{
		
				MyQuerry querry = AppUtility.getcustomerQuerry(model.getPersonInfo().getCount());
				async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
					
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						// TODO Auto-generated method stub
						for(SuperModel smodel : result){
							Customer custEntity  = (Customer) smodel;
							System.out.println("customer Name = =="+custEntity.getFullname());
							System.out.println("Customer Email = == "+custEntity.getEmail());
							
							if(custEntity.getEmail()!=null){
								label : AppUtility.loadContactPersonlist(emailpopup.olbContactlistEmailId,"Customer",model.getPersonInfo().getCount(),custEntity.getFullname(),custEntity.getEmail(),null);
								emailpopup.taToEmailId.setValue(custEntity.getEmail());
							}
							else{
								label : AppUtility.loadContactPersonlist(emailpopup.olbContactlistEmailId,"Customer",model.getPersonInfo().getCount(),custEntity.getFullname(),custEntity.getEmail(),null);
							}
							break;
						}
					}
					
					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
					}
				});
			}
			
			Screen screenName=(Screen) AppMemory.getAppMemory().currentScreen;
			Console.log("screenName "+screenName);
			AppUtility.makeLiveSmsTemplateConfig(emailpopup.oblTemplateName,AppConstants.ACCOUNTMODULE,screenName);
			emailpopup.taFromEmailId.setValue(fromEmailId);
			emailpopup.smodel = model;
			form.hideWaitSymbol();
			
		}
		
		@Override
		public void reactOnSMS() {
			smspopup.showPopUp();
			loadSMSPopUpData();
		}

		private void loadSMSPopUpData() {
			form.showWaitSymbol();
			String customerCellNo = model.getPersonInfo().getCellNumber()+"";

			String customerName = model.getPersonInfo().getFullName();
			if(!customerName.equals("") && !customerCellNo.equals("")){
				AppUtility.loadContactPersonlist(smspopup.olbContactlistEmailId,"Customer",model.getPersonInfo().getCount(),customerName,customerCellNo,null,true);
			}
			Screen screenName=(Screen) AppMemory.getAppMemory().currentScreen;
			Console.log("screenName "+screenName);
			AppUtility.makeLiveSmsTemplateConfig(smspopup.oblsmsTemplate,AppConstants.ACCOUNTMODULE,screenName,true,AppConstants.SMS);
			smspopup.getRbSMS().setValue(true);
			smspopup.smodel = model;
			form.hideWaitSymbol();
		}
		
		private void updateSalesOrderSummary() {
			Console.log("Inside updateSalesOrderSummary");
			
			for(SalesOrderProductLineItem product:form.getSalesProductTable().getValue()){
				if(product.getSalesAnnexureMap()!=null&&product.getSalesAnnexureMap().size()!=0){
					ArrayList<SalesAnnexureType> salesAnnexureList=new ArrayList<SalesAnnexureType>();
					salesAnnexureList.addAll(product.getSalesAnnexureMap().get(product.getBillProductName()));
					
					for(SalesAnnexureType type:salesAnnexureList){
						double sgstAmount=0;
						double cgstAmount=0;
						double igstAmount=0;
						double utgstAmount=0;
						double baseAmount=type.getPayableAmount();
						double totalNetAmt=0;
						
						double sgstPer=0;
						double cgstPer=0;
						double igstPer=0;
						double utgstper=0;
						
						try{
							if(product.getVatTax()!=null){
								if(product.getVatTax().getTaxConfigName().contains("cgst")||product.getVatTax().getTaxConfigName().contains("CGST")
										||product.getVatTax().getTaxName().contains("cgst")||product.getVatTax().getTaxName().contains("CGST")
										||product.getVatTax().getTaxPrintName().contains("cgst")||product.getVatTax().getTaxPrintName().contains("CGST")){
									cgstPer=product.getVatTax().getPercentage();
									cgstAmount=(baseAmount*cgstPer)/100;
								}
								
								if(product.getVatTax().getTaxConfigName().contains("sgst")||product.getVatTax().getTaxConfigName().contains("SGST")
										||product.getVatTax().getTaxName().contains("sgst")||product.getVatTax().getTaxName().contains("SGST")
										||product.getVatTax().getTaxPrintName().contains("sgst")||product.getVatTax().getTaxPrintName().contains("SGST")){
									sgstPer=product.getVatTax().getPercentage();
									sgstAmount=(baseAmount*sgstPer)/100;
								}
								
								if(product.getVatTax().getTaxConfigName().contains("igst")||product.getVatTax().getTaxConfigName().contains("IGST")
										||product.getVatTax().getTaxName().contains("igst")||product.getVatTax().getTaxName().contains("IGST")
										||product.getVatTax().getTaxPrintName().contains("igst")||product.getVatTax().getTaxPrintName().contains("IGST")){
									igstPer=product.getVatTax().getPercentage();
									igstAmount=(baseAmount*igstPer)/100;
								}
								
								if(product.getVatTax().getTaxConfigName().contains("utgst")||product.getVatTax().getTaxConfigName().contains("UTGST")
										||product.getVatTax().getTaxName().contains("utgst")||product.getVatTax().getTaxName().contains("UTGST")
										||product.getVatTax().getTaxPrintName().contains("utgst")||product.getVatTax().getTaxPrintName().contains("UTGST")){
									utgstper=product.getVatTax().getPercentage();
									utgstAmount=(baseAmount*utgstper)/100;
								}
							}
							
							
							if(product.getServiceTax()!=null){
								if(product.getServiceTax().getTaxConfigName().contains("cgst")||product.getServiceTax().getTaxConfigName().contains("CGST")
										||product.getServiceTax().getTaxName().contains("cgst")||product.getServiceTax().getTaxName().contains("CGST")
										||product.getServiceTax().getTaxPrintName().contains("cgst")||product.getServiceTax().getTaxPrintName().contains("CGST")){
									cgstPer=product.getServiceTax().getPercentage();
									cgstAmount=(baseAmount*cgstPer)/100;
								}
								
								if(product.getServiceTax().getTaxConfigName().contains("sgst")||product.getServiceTax().getTaxConfigName().contains("SGST")
										||product.getServiceTax().getTaxName().contains("sgst")||product.getServiceTax().getTaxName().contains("SGST")
										||product.getServiceTax().getTaxPrintName().contains("sgst")||product.getServiceTax().getTaxPrintName().contains("SGST")){
									sgstPer=product.getServiceTax().getPercentage();
									sgstAmount=(baseAmount*sgstPer)/100;
								}
								
								if(product.getServiceTax().getTaxConfigName().contains("igst")||product.getServiceTax().getTaxConfigName().contains("IGST")
										||product.getServiceTax().getTaxName().contains("igst")||product.getServiceTax().getTaxName().contains("IGST")
										||product.getServiceTax().getTaxPrintName().contains("igst")||product.getServiceTax().getTaxPrintName().contains("IGST")){
									igstPer=product.getServiceTax().getPercentage();
									igstAmount=(baseAmount*igstPer)/100;
								}
								
								if(product.getServiceTax().getTaxConfigName().contains("utgst")||product.getServiceTax().getTaxConfigName().contains("UTGST")
										||product.getServiceTax().getTaxName().contains("utgst")||product.getServiceTax().getTaxName().contains("UTGST")
										||product.getServiceTax().getTaxPrintName().contains("utgst")||product.getServiceTax().getTaxPrintName().contains("UTGST")){
									utgstper=product.getServiceTax().getPercentage();
									utgstAmount=(baseAmount*utgstper)/100;
								}
							}
						}catch(Exception e){
							e.printStackTrace();
						}
						totalNetAmt=baseAmount+cgstAmount+sgstAmount+igstAmount+utgstAmount;
						type.setCgstAmt(cgstAmount);
						type.setSgstAmt(sgstAmount);
						type.setUtgstAmt(utgstAmount);
						type.setIgstAmt(igstAmount);
						
						type.setCgstPer(cgstPer);
						type.setSgstPer(sgstPer);
						type.setUtgstPer(utgstper);
						type.setIgstPer(igstPer);
						type.setGrandTotal(totalNetAmt);
					}
					
					product.getSalesAnnexureMap().put(product.getBillProductName(), salesAnnexureList);
				}
			}
			form.getSalesProductTable().getTable().redraw();
			
		}
		
		private void reactOnInvoiceReset(){
			Console.log("In reactOnInvoiceReset");
			form.showWaitSymbol();
			CommonServiceAsync commonservice = GWT.create(CommonService.class);
			commonservice.deletePaymentDocumentsOnInvoiceReset(model.getCount(), model.getCompanyId(),LoginPresenter.loggedInUser, new AsyncCallback<String>() {

						@Override
						public void onFailure(Throwable caught) {
							form.hideWaitSymbol();
						}

						@Override
						public void onSuccess(String result) {
							
							if(result!=null && result.equalsIgnoreCase("success")){
								form.showDialogMessage("Invoice reset successfully");
								form.hideWaitSymbol();
								form.tbstatus.setValue(Invoice.CREATED);
								model.setStatus(Invoice.CREATED);
								form.setToViewState();
							}
						}
					});
		}
		private void updateInvoiceView(final boolean accountinginterfaceCreationflag, final boolean cancelleddocumentAccountingInterfaceFlag,final boolean cancelOnlyEVAInvoice) {
			Console.log("in updateInvoiceView accountinginterfaceCreationflag="+accountinginterfaceCreationflag+" cancelleddocumentAccountingInterfaceFlag="+cancelleddocumentAccountingInterfaceFlag+" cancelOnlyEVAInvoice="+cancelOnlyEVAInvoice );
			final MyQuerry querry=new MyQuerry();
			Vector<Filter> temp=new Vector<Filter>();
			Filter filter=null;
			
			filter=new Filter();
			filter.setQuerryString("count");
			filter.setIntValue(model.getCount());
			temp.add(filter);
			
			querry.setFilters(temp);
			querry.setQuerryObject(new Invoice());
			
			service.getSearchResult(querry,	new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onFailure(Throwable caught) {
					form.showDialogMessage("Failed to update view");
				}

				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					Console.log("in updateInvoiceView success" );
					String refNumber="";
					if (result.size() == 0) {
						form.showDialogMessage("Failed to update view");
					}
					else {
						Invoice invoice=(Invoice)result.get(0);
						if(invoice.getIRN()!=null){
							form.eInvoiceNo.setValue(invoice.getIRN());
							model.setIRN(invoice.getIRN());
						}
						if(invoice.getIrnAckNo()!=null){
							form.ackNo.setValue(invoice.getIrnAckNo());
							model.setIrnAckNo(invoice.getIrnAckNo());
						}
						if(invoice.getIrnAckDate()!=null){
							form.ackDate.setValue(invoice.getIrnAckDate());
							model.setIrnAckDate(invoice.getIrnAckDate());
						}
						if(invoice.getIrnQrCode()!=null){
							form.qrCode.setValue(invoice.getIrnQrCode());
							model.setIrnQrCode(invoice.getIrnQrCode());
						}
						if(invoice.getIrnStatus()!=null){
							Console.log("Setting IRN status");
							form.eInvoiceStatus.setValue(invoice.getIrnStatus());
							model.setIrnStatus(invoice.getIrnStatus());
						}
						if(invoice.getIrnCancellationDate()!=null){
							form.eInvoiceCancellationDate.setValue(invoice.getIrnCancellationDate());
							model.setIrnCancellationDate(invoice.getIrnCancellationDate());
						}
						if(invoice.getCancellationDate()!=null) {
							Console.log("invoice.getIrnCancellationDate()"+invoice.getIrnCancellationDate());
							form.tbCancelledOn.setValue(invoice.getCancellationDate().toString());
							model.setCancellationDate(invoice.getCancellationDate());
						}
						if(invoice.getCancelledBy()!=null) {
							form.tbCancelledBy.setValue(invoice.getCancelledBy());
							model.setCancelledBy(invoice.getCancelledBy());
						}
						if(invoice.getRemark()!=null) {
							form.tbremark.setValue(invoice.getRemark());
							model.setRemark(invoice.getRemark());
						}
						if(invoice.getLog()!=null) {
							form.tbLog.setValue(invoice.getLog());
							model.setLog(invoice.getLog());
						}
						if(accountinginterfaceCreationflag) {
							createAccountingInterface(model);
						}
						
						if(cancelleddocumentAccountingInterfaceFlag) {
							Console.log("invoice.getIrnStatus() "+invoice.getIrnStatus());
							if(invoice.getIrnStatus().equals(invoice.CANCELLED)||cancelOnlyEVAInvoice) {
								createNewInvoice();
							}
						}
					}
					
					
				}
			});
		}
		
		private void cancelEInvoice(final boolean accountingInterfaceFlag){
			Console.log("Cancel e-Invoice clicked");
			form.showWaitSymbol();

			IntegrateSyncServiceAsync integrateService=GWT.create(IntegrateSyncService.class);
			integrateService.cancelIRN(model, new AsyncCallback<String>() {

				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					Console.log("Failed"+caught.getMessage());
				}

				@Override
				public void onSuccess(String result) {
					// TODO Auto-generated method stub
					Console.log("cancel einvoice on success result="+result);
					boolean flag = accountingInterfaceFlag;
					
					if(result.equalsIgnoreCase("Success")) {
						Console.log("IRN Cancelled successfully");	
						form.showDialogMessage("IRN Cancelled successfully");
						updateInvoiceView(false,flag,false);
					}else {
						Console.log("result="+result);
						if(result.contains("The allowed cancellation time limit is crossed")) {
							Console.log("time exceeded error caught");
							
							panel=new PopupPanel(true);
							panel.add(conditionPopupForOnlyEvaInvoiceCancellation);
							panel.setGlassEnabled(true);
							panel.show();
							panel.center();
							
						
						}else{
							form.showDialogMessage(result);
						}
						
					
						flag = false;
					}

					form.hideWaitSymbol();
				}
			});
		}
		
		
		private String getcustomerGSTInNumber(int count,
				ArrayList<Customer> customerlist) {
			String artValue  = " ";
			
			for(Customer customer : customerlist){
				if(customer.getCount()==count){
					
					ArrayList<ArticleType> articleTypeDetails= customer.getArticleTypeDetails();
					for(ArticleType art : articleTypeDetails){
						if(art.getArticleTypeName().equalsIgnoreCase("gstIn")){
							artValue = art.getArticleTypeValue();
							return artValue;
						}
					}
				}
			}
			
			return artValue;
		}

//		@Override
//		public void onSelection(SelectionEvent<Suggestion> event) {
//
//			if(event.getSource().equals(form.getPersonInfoComposite().getId())||event.getSource().equals(form.getPersonInfoComposite().getName())||event.getSource().equals(form.getPersonInfoComposite().getPhone())){
//				form.loadCustomer(form.getPersonInfoComposite().getIdValue());
//			}
//			
//		}
		
		protected void createAccountingInterface(Invoice model) {
			Console.log("Invoice status"+model.getStatus());
			commonservice.createAccountingInterface(model, new AsyncCallback<String>() {
				
				@Override
				public void onSuccess(String result) {
					// TODO Auto-generated method stub
					Console.log(result);
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					
				}
			});
		}
		
		
		protected void createNewEntryInAccountingInterfaceWithCancelledStatus(Invoice model) {
			Console.log("Invoice status = "+model.getStatus());
			commonservice.createSpecificDocumentAccountingInterface(model, new AsyncCallback<String>() {
				
				@Override
				public void onSuccess(String result) {
					// TODO Auto-generated method stub
					Console.log(result);

				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
				}
			});
		}
		
		
		private void reactonUpdateSalesOrderInvoice() {
			form.showWaitSymbol();
			commonservice.updateInvoiceOrderTypeInProduct(model, new AsyncCallback<Invoice>() {
				
				@Override
				public void onSuccess(Invoice invoiceEntity) {
					// TODO Auto-generated method stub
					
					if(invoiceEntity!=null){
						form.salesProductTable.getDataprovider().setList(invoiceEntity.getSalesOrderProducts());
						form.showDialogMessage("Updated successfully");
					}
					form.hideWaitSymbol();
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					form.hideWaitSymbol();
				}
			});
			
		}
		
		private void reactOnCopy(){
			Console.log("in reactOnCopy");
			try {
				Console.log("get id : " +model.getCount());
				final Invoice object=new Invoice();
				object.setCount(0);
				object.setApprovalDate(null);
				object.setCreationDate(null);
				object.setStatus(Quotation.CREATED);
				object.setId(null);
				
				Console.log("after default");
				//Header
				object.setPersonInfo(model.getPersonInfo());
				object.setBranch(model.getBranch());
				object.setInvoiceAmount(model.getInvoiceAmount());				
				object.setInvoiceDate(model.getInvoiceDate());
				object.setPaymentDate(model.getPaymentDate());
				if(model.getEmployee()!=null)
					object.setEmployee(model.getEmployee());
				if(model.getNumberRange()!=null)
					object.setNumberRange(model.getNumberRange());
				object.setApproverName(model.getApproverName());
				
				Console.log("after header");
				//Product table
				object.setConsolidatePrice(model.isConsolidatePrice());
				Console.log("after consolidate");
				object.setQuantity(model.getQuantity());
				Console.log("after quantity");
				if(model.getUom()!=null)
					object.setUom(model.getUom());	
				Console.log("after UOM");
				if(model.getSalesOrderProducts()!=null&&model.getSalesOrderProducts().size()>0){
					Console.log("products not null");					
					object.setSalesOrderProductFromBilling(model.getSalesOrderProducts());
				}
//				
				Console.log("after product1");					
				object.setDiscountAmt(model.getDiscountAmt());
				object.setFinalTotalAmt(model.getFinalTotalAmt());
				object.setTotalAmtExcludingTax(model.getTotalAmtExcludingTax());				
				if(model.getOtherCharges()!=null)
					object.setOtherCharges(model.getOtherCharges());
				object.setTotalOtherCharges(model.getTotalOtherCharges());
				Console.log("after product2");	
				if(model.getBillingTaxes()!=null)
					object.setBillingTaxes(model.getBillingTaxes());
				object.setTotalAmtIncludingTax(model.getTotalAmtIncludingTax());
				object.setDiscount(model.getDiscount());
				object.setNetPayable(model.getNetPayable());
				
				Console.log("after products");
				//TDS
				object.setTdsApplicable(model.isTdsApplicable());
				if(model.getTdsPercentage()!=null)
					object.setTdsPercentage(model.getTdsPercentage());
				object.setTdsTaxValue(model.getTdsTaxValue());
				
				Console.log("after TDS");
				//Billing Details
				object.setTotalBillingAmount(model.getTotalBillingAmount());
				if(model.getSubBillType()!=null)
					object.setSubBillType(model.getSubBillType());
				if(model.getBillingPeroidFromDate()!=null)
					object.setBillingPeroidFromDate(model.getBillingPeroidFromDate());
				if(model.getBillingPeroidToDate()!=null)
					object.setBillingPeroidToDate(model.getBillingPeroidToDate());
				if(model.getGstinNumber()!=null)
					object.setGstinNumber(model.getGstinNumber());
				if(model.getProformaCount()!=null&&!model.getInvoiceType().equals(AppConstants.CREATEPROFORMAINVOICE)) 
					object.setProformaCount(model.getProformaCount());
				if(model.getArrayBillingDocument()!=null)
					object.setArrayBillingDocument(model.getArrayBillingDocument());
				
				Console.log("after bill");
				//payment terms
				object.setArrPayTerms(model.getArrPayTerms());
				object.setInvoiceType(model.getInvoiceType());						
				if(model.getPaymentMode()!=null)
					object.setPaymentMode(model.getPaymentMode());				
				object.setCreditPeriod(model.getCreditPeriod());
				
				Console.log("after payterms");
				//classification
				if(model.getInvoiceCategory()!=null){
					Console.log("category="+model.getInvoiceCategory());
					object.setInvoiceCategory(model.getInvoiceCategory());
				}
				if(model.getInvoiceGroup()!=null){
					Console.log("model.getGroup()="+model.getInvoiceGroup());
					object.setInvoiceGroup(model.getInvoiceGroup());
				}
				if(model.getInvoiceConfigType()!=null){
					Console.log("model.getType()="+model.getInvoiceConfigType());
					object.setInvoiceConfigType(model.getInvoiceConfigType());
				}
				if(model.getSegment()!=null){
					Console.log("model.getSegment()="+model.getSegment());
					object.setSegment(model.getSegment());
				}
				Console.log("after classification");
				//reference
				if(model.getContractCount()!=null)
					object.setContractCount(model.getContractCount());
				if(model.getTotalSalesAmount()!=null)
					object.setTotalSalesAmount(model.getTotalSalesAmount());
				if(model.getServiceId()!=null)
					object.setServiceId(model.getServiceId());
				if(model.getPoNumber()!=null)
					object.setPoNumber(model.getPoNumber());
				if(model.getWoNumber()!=null)
					object.setWoNumber(model.getWoNumber());
				if(model.getRefNumber()!=null)
					object.setRefNumber(model.getRefNumber());
				if(model.getContractNumber()!=null)
					object.setContractNumber(model.getContractNumber());
				if(model.getConStartDur()!=null)
					object.setConStartDur(model.getConStartDur());
				if(model.getConEndDur()!=null)
					object.setConEndDur(model.getConEndDur());
				if(model.getCustomerBranch()!=null)
					object.setCustomerBranch(model.getCustomerBranch());
				if(model.getTypeOfOrder()!=null)
					object.setTypeOfOrder(model.getTypeOfOrder());
				if(model.getEmployee()!=null)
					object.setEmployee(model.getEmployee());
				if(model.getProjectName()!=null)
					object.setProjectName(model.getProjectName());					
				if(model.getComment()!=null)
					object.setComment(model.getComment());
				if(model.getComment1()!=null)
					object.setComment1(model.getComment1());
				object.setDonotprintServiceAddress(model.isDonotprintServiceAddress());
				if(model.getDeclaration()!=null)
					object.setDeclaration(model.getDeclaration());
				if(model.getDocUpload1()!=null)
					object.setDocUpload1(model.getDocUpload1());
				if(model.getDocUpload2()!=null)
					object.setDocUpload2(model.getDocUpload2());
				if(model.getAccountType()!=null)
					object.setAccountType(model.getAccountType()); //11-09-2023
				Console.log("after reference");
				
				if(model.getLog()!=null&&!model.getLog().equals("")){
					object.setLog(model.getLog()+" Invoice is copied from invoice Id: "+model.getCount() +" by "+LoginPresenter.loggedInUser+" on "+new Date()+".");				
			
				}else{
					object.setLog("Invoice is copied from invoice Id: "+model.getCount() +" by "+LoginPresenter.loggedInUser+" on "+new Date()+".");				
						
				}
					
				
				//Mappings which are not visible on screen
				object.setMultipleOrderBilling(model.isMultipleOrderBilling());
				object.setOrderCreationDate(model.getOrderCreationDate());
				if(model.getOrderCformStatus()!=null)
					object.setOrderCformStatus(model.getOrderCformStatus());				
				object.setOrderCformPercent(model.getOrderCformPercent());
				if(model.getRateContractServiceId()>0)
					object.setRateContractServiceId(model.getRateContractServiceId());
				object.setRenewContractFlag(model.isRenewContractFlag());
				if(model.getCncBillAnnexureId()!=null)
					object.setCncBillAnnexureId(model.getCncBillAnnexureId());
				if(model.getCancleAmtRemark()!=null)
					object.setCancleAmtRemark(model.getCancleAmtRemark());

				System.out.println("get id after : " +model.getCount()+ " ID "+model.getId() + " get value model  : "+ model.getCount() +" model Id ;;;; -- "+model.getId());
				AppMemory.getAppMemory().currentState=ScreeenState.NEW;
				form.setToNewState();
				Console.log("after new state");
				Timer timer = new Timer() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						
						Console.log("after run");
						form.updateView(object);
						Console.log("after updateview");
						form.setToEditState();
						Console.log("after editstate");
						form.toggleAppHeaderBarMenu();
						Console.log("after toggleAppHeaderBarMenu");
						
									
					}
				};
				timer.schedule(2000);
				
//				form.tbQuotationStatus.setValue(Quotation.CREATED);
//				
			} 
			catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}

}
