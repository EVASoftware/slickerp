package com.slicktechnologies.client.views.paymentinfo.invoicedetails;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.TextAlign;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.composite.UploadComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.approvalutility.ApprovableFormScreen;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CustomerNameChangeService;
import com.slicktechnologies.client.services.CustomerNameChangeServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.contract.OtherChargesTable;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.BillingDocumentTable;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.BillingProductChargesTable;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.BillingProductTaxesTable;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.SalesOrderProductTable;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.shared.BranchWiseScheduling;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Declaration;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.productlayer.Tax;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.salesorder.OtherCharges;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.BillingDocumentDetails;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.SalesOrderProductLineItem;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;
import com.slicktechnologies.shared.common.tallyaccounts.AccountingInterface;
import com.smartgwt.client.util.DateUtil;


public class InvoiceDetailsForm extends ApprovableFormScreen<Invoice> implements ClickHandler, ChangeHandler, ValueChangeHandler<Date> {
	
	TextBox ibinvoiceid;
	TextBox proformaRefId;
	TextBox referenceId;
	IntegerBox ibbillingid;
	TextBox ibcontractid;
//	TextBox tbpersoncount;
//	TextBox tbpersonname;
//	TextBox tbpersoncell;
//	TextBox tbpocname;
	DateBox dbstartDate,dbendDate;
	DateBox dbbillingdate,dbinvoicedate;
	DoubleBox dosalesamount;
	TextArea tacomment;
	TextBox tbstatus;
	ObjectListBox<Employee> olbEmployee,olbApproverName;
	ObjectListBox<Branch> olbbranch;
	TextBox tbinvoiceType;
	BillingDocumentTable billingdoctable;
	DoubleBox dototalbillamt,doinvoiceamount;
	DateBox dbpaymentdate;
	TextBox tbaccounttype;
	TextBox tbremark;
	ObjectListBox<ConfigCategory> olbInvoiceCategory;
	ObjectListBox<Type> olbInvoiceConfigType;
	ObjectListBox<Config> olbinvoicegroup;
	FormField fgroupingCustomerInformation;
	ListBox lbcustomerbranch;	
	
	DateBox conStartDur,conEndDur;
	boolean multipleBillInvoice; 
	
	
	//  rohan added this for om pest requirement for discount on invoice  
//	DoubleBox discount ;
	
	// Date 04-09-2017 added by vijay changed discount to roundoff
	TextBox tbroundOffAmt ;

	DoubleBox netPayable ;
	
	
	//   rohan added this for editable invoice document ***********
	
	SalesOrderProductTable salesProductTable;
	BillingProductTaxesTable invoiceTaxTable;
	BillingProductChargesTable invoiceChargesTable;
	Double totalAmount;
	Double toatalAmountIncludingTax;
	Double netPayableAmount;
	
	IntegerBox ibpaytermsdays;
	DoubleBox dopaytermspercent;
	TextBox tbpaytermscomment;
	
	/**
	 * Date 14 Feb 2017
	 * by Vijay for billing peroid from date and todate
	 */
	
	DateBox dbbillingperiodFromDate;
	DateBox dbbillingperiodToDate;
	
	/**
	 * End here
	 */
	
	//Added by Rahul Verma on 15 March 2017
	Button btReferenceDetails;
	
	public static boolean flag=false;
	
	/*
	 *  nidhi
	 *  1-07-2017
	 */
	TextBox tbSegment;
	
	/*
	 * end
	 */
	
	/**
	 * Date 18-07-2017
	 * added by  nidhi for displaying rate contract service ID
	 */
	TextBox tbrateContractServiceid;
	/**
	 * End here
	 */
	
	/**
	 * Date : 19-07-2017 By ANIL
	 */
	TextBox tbGstinNumber;
	/**
	 * End
	 */
	/**
	  * Added by Nidhi
	  * Two field named quantity and UOM
	  */
	 DoubleBox quantity;
	 ObjectListBox<Config> olbUOM;
	 /**
	  * 	Nidhi changes ends here
	  */
	
    /**
	 * Date 1-07-2017 added by vijay for Tax validation
	 */
	Date date = DateTimeFormat.getFormat("yyyy-MM-dd HH:mm:ss").parse("2017-07-01 00:00:00");
	final DateTimeFormat fmt = DateTimeFormat.getFormat("dd/MM/yyyy");
	/**
	 * ends here
	 */
	
	/** Date 04-09-2017 added by vijay for discount on total and round off **/
	DoubleBox dbDiscountAmt;
	DoubleBox dbFinalTotalAmt;
	DoubleBox dbtotalAmtIncludingTax;
	DoubleBox dbtotalAmt;
	NumberFormat nf = NumberFormat.getFormat("0.00");
	
	/**
	 * Date : 21-09-2017 BY ANIL
	 * Adding other charges table
	 */
	OtherChargesTable tblOtherCharges;
	Button addOthrChargesBtn;
	DoubleBox dbOtherChargesTotal;
	/**
	 * End
	 */
	
	Invoice invoiceobj;
	
	/**
	 * Date : 21-11-2017 BY ANIL
	 * these variable is used when we are updating invoice after its approval.
	 */
	public CustomerPayment custPayment=null;
	public boolean updatePaymentFlag=false;
	/**
	 * End
	 */
	 /** Date 06/2/2018 added by komal for consolidate price checkbox **/
	 CheckBox cbConsolidatePrice;
    /**
	 * nidhi
	 * 8-06-2018
	 * nidhi
	 */
	 ObjectListBox<CompanyPayment> objPaymentMode;
	 TextBox tbPaymentName,tbPaymentBankAC,tbPaymentPayableAt ,tbPaymentAccountName;
	 ArrayList<CompanyPayment> paymentModeList = new ArrayList<CompanyPayment>();
  
	 /** Date 26-07-2018 By Vijay for Invoice Edited and changed the invoice date and net payable then Email for Pecopp Customization **/
	public double oldNetPayable;
	public Date oldinvoiceDate;
	/***
	 * ends here		
	 */
	/** date 20.10.2018 added by komal for type of order and sub bill type **/
	TextBox tbTypeOfOrder;
	TextBox tbSubBillType;
	/** date 27.10.2018 added by komal for tds **/
	DoubleBox tbtdsPerValue;
	DoubleBox dbtdsValue;
	CheckBox cbtdsApplicable;
	/** date 27.10.2018 added by komal to store client's PO number , WO number and cnc number **/ 
	TextBox tbPONumber;
	TextBox tbWONumber;
	TextBox tbContractNumber;
	/**Date 7-2-2019 
	 * @author AMOL
	 * added textbox for invoice ref no
	 */
	TextBox tbInvoiceRefNumber;
	boolean invoiceRefNo=false;
	boolean invPrefFlag=false;
	/**
	 * @author Anil , Date : 31-07-2019
	 * eway bill number 
	 * raised by sasha, nitin sir, sonu
	 */
	TextBox tbEwayBillNumber;
	
	/**
	 * @author Vijay Date - 11-08-2020
	 * Des :- Project Name from CNC for sasha with process config
	 */
	TextBox tbprojectName;
	
	/**
	 * @author Vijay Date - 11-08-2020
	 * Des :- CNC and Sales Order Credit Period to calculate payment for sasha with process config
	 * and Dispatch Date
	 */
	IntegerBox ibcreditPeriod;
	DateBox dbDispatchDate;
//	TextBox tbInvPrefix;
	TextArea taComment1;
	
	/**
	 * @author Vijay 19-01-2021
	 * Des :- as per rahuls requirement added 2 uploads
	 */
	UploadComposite upload1,upload2;
	
	CustomerNameChangeServiceAsync customernamechangeasync = GWT.create(CustomerNameChangeService.class);
	
	
	/**
	 * @author Anil @since 27-03-2021
	 * Adding site location flag
	 */
	boolean enableSiteLocation=false;
	
	/**
	 * @author Anil @since 15-11-2021
	 * Sunrise SCM
	 */
	boolean uploadConsumableFlag=false;
	
	/**
	 * @author Anil
	 * @since 19-01-2022
	 * adding declaration field which will be printed on invoice. raised by Nithila and Nitin Sir for Innovative
	 */
	ObjectListBox<Declaration> obTermsandCondition;
	TextArea taDeclaration;
	
	/**
	 * @author Vijay  Date :- 28-03-2022 
	 * Des :- When this checkbox is true then service address will not print in pdf
	 */
	CheckBox cbdonotprintserviceAddress;

	/**@author Sheetal : 28-05-2022
	 * Des : Adding new fields of oracle format,requirement by Envocare**/
	ListBox lbPaymentTerms, lbTransactionLineType,lbTaxExemptionFlag;
	ObjectListBox <Config> olbTaxClassificationCode;
	ObjectListBox <Vendor> obVendor;
	
	PersonInfoComposite personInfoComposite;
	
	
	TextBox tbCreatedBy, tbCreatedOn, tbUpdatedBy, tbUpdatedOn, tbApprovedBy, tbApprovedOn, tbCancelledBy, tbCancelledOn;
	TextArea tbLog;

	TextBox eInvoiceNo;
	TextBox ackNo;
	TextBox ackDate;
	TextArea qrCode;
	TextBox eInvoiceStatus;
	TextBox eInvoiceCancellationDate;
	Button btnCopyContractDescription;//Ashwini Patil Date:1-02-2022

	ObjectListBox<String> invoicepdfNametoprint;
	
	ProductInfoComposite prodInfoComposite;
	Button baddproducts;
//	public Customer cust;
	final static GenricServiceAsync async=GWT.create(GenricService.class);
	
	boolean addDeleteProductInInvoiceFlag = false;
	
	TextBox tbZohoInvoiceId,tbZohoInvoiceUniqueID,tbZohoSyncInfo;
	TextBox tbZohoSyncDate;

	public InvoiceDetailsForm() {
		super();
		createGui();
		salesProductTable.connectToLocal();
		invoiceChargesTable.connectToLocal();
		invoiceTaxTable.connectToLocal();
		billingdoctable.connectToLocal();
		
		/** Date 04-09-2017 added by vijay for discount amount change handler ***/
		dbDiscountAmt.addChangeHandler(this);
		tbroundOffAmt.addChangeHandler(this);
		
		/**
		 * Date :20-09-2017 By ANIL
		 */
		tblOtherCharges.connectToLocal();
		dbOtherChargesTotal.setEnabled(false);
		/**
		 * ENd
		 */
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "PC_PRINTINVOICENUMBERPREFIX")){
			invPrefFlag=true;
			tbInvoiceRefNumber.setEnabled(false);
		}
		enableSiteLocation=AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "EnableSiteLocation");
		uploadConsumableFlag=AppUtility.checkForProcessConfigurartionIsActiveOrNot("CNC", "PC_UploadLocationWiseBudget");
	}
	
//	public InvoiceDetailsForm  (String[] processlevel, FormField[][] fields,FormStyle formstyle) 
//	{
//		super(processlevel, fields, formstyle);
//		createGui();
//		salesProductTable.connectToLocal();
//		invoiceChargesTable.connectToLocal();
//		invoiceTaxTable.connectToLocal();
//		billingdoctable.connectToLocal();
//	}
	
	private void initalizeWidget()
	{
		ibinvoiceid=new TextBox();
		ibinvoiceid.setEnabled(false);
		ibbillingid=new IntegerBox();
		ibbillingid.setEnabled(false);
		ibcontractid=new TextBox();
		ibcontractid.setEnabled(false);
		
//		tbpersoncount=new TextBox();
//		tbpersonname=new TextBox();
//		tbpersoncell=new TextBox();
//		tbpersoncount.setEnabled(false);
//		tbpersonname.setEnabled(false);
//		tbpersoncell.setEnabled(false);
//		tbpocname=new TextBox();
//		tbpocname.setEnabled(false);
		
//		dbbillingdate=new DateBox();
		dbbillingdate = new DateBoxWithYearSelector();
		dbbillingdate.setEnabled(false);
		
//		dbinvoicedate=new DateBox();
		dbinvoicedate = new DateBoxWithYearSelector();
		dbinvoicedate.addValueChangeHandler(this);
		
		dosalesamount=new DoubleBox();
		dosalesamount.setEnabled(false);
		tacomment=new TextArea();
		tbstatus=new TextBox();
		tbstatus.setEnabled(false);
		this.tbstatus.setText(Invoice.CREATED);
		olbApproverName=new ObjectListBox<Employee>();
		AppUtility.makeApproverListBoxLive(olbApproverName,"Invoice Details");
		olbbranch=new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbbranch);
		olbbranch.addChangeHandler(this);
		olbEmployee=new ObjectListBox<Employee>();
		AppUtility.makeSalesPersonListBoxLive(olbEmployee);
		tbinvoiceType=new TextBox();
		tbinvoiceType.setEnabled(false);
		billingdoctable=new BillingDocumentTable();
		dototalbillamt=new DoubleBox();
		dototalbillamt.setEnabled(false);
		
//		dbpaymentdate=new DateBox();
		dbpaymentdate = new DateBoxWithYearSelector();

		doinvoiceamount=new DoubleBox();
		doinvoiceamount.setEnabled(false);
		tbaccounttype=new TextBox();
		tbaccounttype.setEnabled(false);
		tbremark=new TextBox();
		tbremark.setEnabled(false);
		olbInvoiceCategory=new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(olbInvoiceCategory, Screen.INVOICECATEGORY);
		olbInvoiceConfigType=new ObjectListBox<Type>();
		AppUtility.makeTypeListBoxLive(olbInvoiceConfigType, Screen.INVOICECONFIGTYPE);
		olbInvoiceCategory.addChangeHandler(this);
		olbinvoicegroup=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbinvoicegroup, Screen.INVOICEGROUP);
		
		
		
		/**
		 * nidhi
		 * 8-06-2018
		 * 
		 */
		objPaymentMode = new ObjectListBox<CompanyPayment>();
		loadPaymentMode();
		
//		 conStartDur= new DateBox();
		 conStartDur = new DateBoxWithYearSelector();
		 conStartDur.setEnabled(false);
//		 conEndDur =  new DateBox();
		 conEndDur = new DateBoxWithYearSelector();

		 conEndDur.setEnabled(false);
		 
		 
		 lbcustomerbranch = new ListBox();
		 lbcustomerbranch.addItem("--SELECT--");
	
		 referenceId = new TextBox();
		 
		 
		 //   rohan added this for editable invoice 
		 
		invoiceTaxTable=new BillingProductTaxesTable();
		invoiceChargesTable=new BillingProductChargesTable();
		salesProductTable=new SalesOrderProductTable();
		/**
		 * nidhi 17-12-2018  for stop partical Invoice
		 */
		boolean salesBillDisable = AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "NonEditableInvoicePrice");
		salesProductTable.salesBillDisable = salesBillDisable;
		
		salesProductTable.setEnable(true);
		
		
		proformaRefId = new TextBox();
		proformaRefId.setEnabled(false);
		
		ibpaytermsdays=new IntegerBox();
		ibpaytermsdays.setEnabled(false);
		dopaytermspercent=new DoubleBox();
		dopaytermspercent.setEnabled(false);
		tbpaytermscomment=new TextBox();
		tbpaytermscomment.setEnabled(false);
		
		tbroundOffAmt = new TextBox();
		netPayable=new DoubleBox();
		netPayable.setEnabled(false);
		
		dbbillingperiodFromDate = new DateBoxWithYearSelector();
		
		dbbillingperiodToDate = new DateBoxWithYearSelector();

		btReferenceDetails=new Button("Reference Details");
		this.btReferenceDetails.setEnabled(true);
		
		/* 
		 *  nidhi
		 *   1-07-2017
		 */
		tbSegment=new TextBox();
		tbSegment.setEnabled(false);
		
		/*
		 *  end
		 */
		
		/*
		 *  nidhi
		 *  	18-07-2017
		 *  
		 */
		
		tbrateContractServiceid = new TextBox();
		tbrateContractServiceid.setEnabled(false);
		/*
		 *  end
		 */
		
		/**
		 * Date :19-07-2017 BY ANil
		 */
		tbGstinNumber=new TextBox();
		tbGstinNumber.setEnabled(false);
		/**
		 * End
		 */
	
		/**
		 * 	nidhi
		 * 	20-07-2017
		 *  for show quantity and measurement area of service 
		 */
		quantity=new DoubleBox();
		quantity.setEnabled(true);
		
		olbUOM=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbUOM, Screen.SERVICEUOM);
		
		/*
		 *  end
		 */
		
		/** Date 04-09-2017 added by vijay for discount amt round off total amt***/
		dbDiscountAmt = new DoubleBox();
		dbFinalTotalAmt = new DoubleBox();
		dbFinalTotalAmt.setEnabled(false);
		dbtotalAmt = new DoubleBox();
		dbtotalAmt.setEnabled(false);
		dbtotalAmtIncludingTax = new DoubleBox();
		dbtotalAmtIncludingTax.setEnabled(false);
		
		
		/**
		 * Date : 20-09-2017 By ANIL
		 */
		tblOtherCharges=new OtherChargesTable();
		addOthrChargesBtn=new Button("+");
		addOthrChargesBtn.addClickHandler(this);
		dbOtherChargesTotal=new DoubleBox();
		dbOtherChargesTotal.setEnabled(false);
		/**
		 * End
		 */
		 /** Date 06/2/2018 added by komal for consolidate price checkbox **/
		 cbConsolidatePrice = new CheckBox();
		 if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice","ConsolidatePrice")){
			 cbConsolidatePrice.setValue(true);
		 }else{
			 cbConsolidatePrice.setValue(false);
		 }
		 /**
		  * end komal
		  */
		this.changeWidgets();
		this.changeWidgets();
		tbPaymentName = new TextBox();
		tbPaymentAccountName = new TextBox();
		tbPaymentBankAC = new TextBox();
		tbPaymentPayableAt = new TextBox();
		/** date 20.10.2018 added by komal for type of order and sub bill type **/
		tbTypeOfOrder = new TextBox();
		tbTypeOfOrder.setEnabled(false);
		tbSubBillType = new TextBox();
		tbSubBillType.setEnabled(false);
		/** date 27.10.2018 added by komal for tds **/
		cbtdsApplicable = new CheckBox();
		cbtdsApplicable.setValue(false);
		tbtdsPerValue = new DoubleBox();
		tbtdsPerValue.setEnabled(false);		
		dbtdsValue = new DoubleBox();
		dbtdsValue.setEnabled(false);
		/** date 27.10.2018 added by komal to store client's PO number , WO number and cnc number **/ 
		tbPONumber = new TextBox();
		tbWONumber = new TextBox();
		tbContractNumber = new TextBox();
		/**Date 7-2-2019
		 * @author Amol
		 * added textbox for invoice ref no
		 */
		tbInvoiceRefNumber=new TextBox();
		
		tbEwayBillNumber=new TextBox();
		
		tbprojectName = new TextBox();
		ibcreditPeriod = new IntegerBox();
		ibcreditPeriod.addChangeHandler(this);
		dbDispatchDate = new DateBoxWithYearSelector();
		dbDispatchDate.addValueChangeHandler(this);
		dbDispatchDate.getTextBox().addChangeHandler(this);
	//		tbInvPrefix=new TextBox();
	//		tbInvPrefix.setEnabled(false);
	
	     taComment1 = new TextArea();
	     
	 	upload1=new UploadComposite();
	 	upload2=new UploadComposite();
	 	
	 	obTermsandCondition = new ObjectListBox<Declaration>();
		MyQuerry querry123 = new MyQuerry();
		querry123.setQuerryObject(new Declaration());
		obTermsandCondition.MakeLive(querry123);
		obTermsandCondition.addChangeHandler(this);
		taDeclaration=new TextArea();
		
		cbdonotprintserviceAddress = new CheckBox();
		cbdonotprintserviceAddress.setValue(false);

		lbPaymentTerms = new ListBox();
		lbPaymentTerms.addItem("--SELECT--");
		lbPaymentTerms.addItem("30 Net");
		lbPaymentTerms.addItem("45 Net");
		lbPaymentTerms.addItem("60 Net");
		lbPaymentTerms.addItem("90 Net");
		lbPaymentTerms.addItem("120 Net");

		lbTransactionLineType = new ListBox();
		lbTransactionLineType.addItem("--SELECT--");
		lbTransactionLineType.addItem("LINE");
		lbTransactionLineType.addItem("TAX");
		lbTransactionLineType.addItem("FREIGHT");
		lbTransactionLineType.addItem("CHARGES");
		
		olbTaxClassificationCode=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbTaxClassificationCode, Screen.TAXCLASSIFICATIONCODE);
		
		lbTaxExemptionFlag = new ListBox();
		lbTaxExemptionFlag.addItem("--SELECT--");
		lbTaxExemptionFlag.addItem("R");
		lbTaxExemptionFlag.addItem("E");
		lbTaxExemptionFlag.addItem("S");

		obVendor=new ObjectListBox<Vendor>();
		AppUtility.makeVendorListBoxLive(obVendor);
		
		personInfoComposite=AppUtility.customerInfoComposite(new Customer());
		personInfoComposite.getCustomerId().getHeaderLabel().setText("* Customer ID");
		personInfoComposite.getCustomerName().getHeaderLabel().setText("* Customer Name");
		personInfoComposite.getCustomerCell().getHeaderLabel().setText("* Customer Cell");
		
		
		//for document history tab
		tbCreatedBy = new TextBox();
		tbCreatedBy.setEnabled(false);
		tbCreatedOn= new TextBox();
		tbCreatedOn.setEnabled(false);
		tbUpdatedBy= new TextBox();
		tbUpdatedBy.setEnabled(false);
		tbUpdatedOn= new TextBox();
		tbUpdatedOn.setEnabled(false);
		tbApprovedBy= new TextBox();
		tbApprovedBy.setEnabled(false);
		tbApprovedOn= new TextBox();
		tbApprovedOn.setEnabled(false);
		tbCancelledBy= new TextBox();
		tbCancelledBy.setEnabled(false);
		tbCancelledOn= new TextBox();
		tbCancelledOn.setEnabled(false);
		tbLog= new TextArea();
		tbLog.setEnabled(false);
		eInvoiceNo= new TextBox();
		eInvoiceNo.setEnabled(false);
		ackNo= new TextBox();
		ackNo.setEnabled(false);
		ackDate= new TextBox();
		ackDate.setEnabled(false);
		qrCode= new TextArea();
		qrCode.setEnabled(false);	
		eInvoiceStatus= new TextBox();
		eInvoiceStatus.setEnabled(false);
		eInvoiceCancellationDate= new TextBox();
		eInvoiceCancellationDate.setEnabled(false);
		
		btnCopyContractDescription=new Button("Copy Order Description");
        invoicepdfNametoprint = new ObjectListBox<String>();
		invoicepdfNametoprint.addItem("--SELECT--");
		invoicepdfNametoprint.addItem(AppConstants.STANDARDINVOICEFORMAT);
		invoicepdfNametoprint.addItem(AppConstants.NEWSTANDARDINVOICEFORMAT);
		invoicepdfNametoprint.addItem(AppConstants.CUSTOMIZEDINVOICEFORMAT);
		this.changeWidth(invoicepdfNametoprint, 200, Unit.PX);

		prodInfoComposite=AppUtility.initiateSalesProductComposite(new SuperProduct());
		baddproducts=new Button("ADD");
		baddproducts.addClickHandler(this);
		prodInfoComposite.setEnable(false);
		baddproducts.setEnabled(false);
		addDeleteProductInInvoiceFlag = AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", AppConstants.PC_ENABLEADDDELETESALESORDERPRODUCTSONINVOICE);

		if(addDeleteProductInInvoiceFlag){
			prodInfoComposite.setEnable(true);
			baddproducts.setEnabled(true);
		}
		else{
			prodInfoComposite.setEnable(false);
			baddproducts.setEnabled(false);
		}
		tbZohoInvoiceId=new TextBox();
		tbZohoInvoiceId.setEnabled(false);
		tbZohoInvoiceUniqueID=new TextBox();
		tbZohoInvoiceUniqueID.setEnabled(false);
		tbZohoSyncInfo=new TextBox();
		tbZohoSyncInfo.setEnabled(false);
		tbZohoSyncDate=new TextBox();
		tbZohoSyncDate.setEnabled(false);
	}

	
	/**
	 * Date 04-09-2017 added by vijay
	 * This method represents changing the widgets style.
	 */
	
	private void changeWidgets()
	{
		//  For changing the text alignments.
		this.changeTextAlignment(dbDiscountAmt);
		this.changeTextAlignment(dbFinalTotalAmt);
		this.changeTextAlignment(dbtotalAmt);
		this.changeTextAlignment(tbroundOffAmt);
		this.changeTextAlignment(netPayable);
		this.changeTextAlignment(dbtotalAmtIncludingTax);
		
		/**
		 * Date : 21-09-2017 BY ANIL
		 * please update roundoff textbox width
		 */
		this.changeTextAlignment(dbOtherChargesTotal);
		this.changeWidth(dbOtherChargesTotal, 200, Unit.PX);
		/**
		 * End
		 */
		
		this.changeWidth(dbDiscountAmt, 200, Unit.PX);
		this.changeWidth(dbFinalTotalAmt, 200, Unit.PX);
		this.changeWidth(dbtotalAmt, 200, Unit.PX);
		this.changeWidth(tbroundOffAmt, 200, Unit.PX);
		this.changeWidth(netPayable, 200, Unit.PX);
		this.changeWidth(dbtotalAmtIncludingTax, 200, Unit.PX);
		
		/**
		 * @author Anil
		 * @since 06-01-2021
		 * Commented property added to total billing amount
		 * raised by Vaishnavi
		 */
//		this.changeTextAlignment(dototalbillamt);
//		this.changeWidth(dototalbillamt, 200, Unit.PX);

	}
	
	/*******************************Text Alignment Method******************************************/
	
	private void changeTextAlignment(Widget widg)
	{
		widg.getElement().getStyle().setTextAlign(TextAlign.CENTER);
	}
	
	/*****************************Change Width Method**************************************/
	private void changeWidth(Widget widg,double size,Unit unit)
	{
		widg.getElement().getStyle().setWidth(size, unit);
	}
	
	/**
	 * ends here
	 */


	@Override
	public void toggleAppHeaderBarMenu() {
		
		System.out.println("STATE== toggle app header "+AppMemory.getAppMemory().currentState);

		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Save")||text.contains("Discard")||text.contains("Search")||text.contains(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
				
			}
			//  added here for Only view state
			btReferenceDetails.setVisible(false);
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			
			/**
			 * Date 30 july 2017 if condtion added by vijay for GENERAL POPUP menus 
			 * and old code added to else block
			 */
			
			System.out.println("presenter.isPopUpAppMenubar()==== EDIT "+isPopUpAppMenubar());
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();

			}
			
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Save")||text.contains("Discard")||text.contains(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 
				
				else
					menus[k].setVisible(false);  		   
			}
		//  added here for Only view state
					btReferenceDetails.setVisible(false);
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			
			/**
			 * Date 30 july 2017 if condtion added by vijay for GENERAL POPUP menus 
			 * and old code added to else block
			 */
			
			System.out.println("presenter.isPopUpAppMenubar()vieww ===="+isPopUpAppMenubar());
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;

			}else{
				 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();

			}
			
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				System.out.println("hi vijay text toggle app=="+text);
				if(text.contains("Discard")||text.contains("Search")||text.contains("Edit")||text.contains("Print")||text.contains(AppConstants.NAVIGATION))
				{
					System.out.println("trueee");
					menus[k].setVisible(true); 
				}
				
				else{
					System.out.println("Falseeeee");
					menus[k].setVisible(false);  		   

				}
			}
		//  added here for Only view state
					btReferenceDetails.setVisible(true);
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.INVOICEDETAILS,LoginPresenter.currentModule.trim());

	}

	@Override
	public void createScreen() {
		enableSiteLocation=AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "EnableSiteLocation");
		uploadConsumableFlag=AppUtility.checkForProcessConfigurartionIsActiveOrNot("CNC", "PC_UploadLocationWiseBudget");
		initalizeWidget();

		//Token to initialize the processlevel menus.
		
		if(isPopUpAppMenubar()){
			Console.log("Inside isPopUpAppMenubar in invoice");
			/**
			 * Date : 28-12-2020 By Priyanka
			 * Des : Change Navigation flow as per requirement of simplification Project raised by Nitin Sir.
			 */
			//this.processlevelBarNames=new String[]{ManageApprovals.APPROVALREQUEST,ManageApprovals.CANCELAPPROVALREQUEST,AppConstants.CANCELINVOICE,"Tax Invoice",ManageApprovals.SUBMIT,AppConstants.UPDATEACCOUNTINGINTERFACE};
			this.processlevelBarNames=new String[]{ManageApprovals.SUBMIT,"Tax Invoice",AppConstants.UPDATEACCOUNTINGINTERFACE,ManageApprovals.APPROVALREQUEST,ManageApprovals.CANCELAPPROVALREQUEST,"Generate e-Invoice number","Invoice Reset",AppConstants.CANCELINVOICE,AppConstants.UPDATESALESORDERINVOICE,AppConstants.COPY,"Update Description",AppConstants.generatePayment,AppConstants.uploadDoc,AppConstants.syncWithZohoBooks};
		}else{
			/**
			 * Date : 29-07-2017 By Anil
			 * added view payment button on process level bar 
			 * Date : 10-10-2017 BY ANIL
			 * added view bill button on process level bar
			 */
			 /**@Sheetal :15-02-2022
			  * Added view order button on process level bar**/
			
			//this.processlevelBarNames=new String[]{ManageApprovals.APPROVALREQUEST,ManageApprovals.CANCELAPPROVALREQUEST,AppConstants.CANCELINVOICE,"Tax Invoice",ManageApprovals.SUBMIT,AppConstants.VIEWBILL,AppConstants.VIEWPAYMENT,AppConstants.VIEWTAXINVOICE,AppConstants.UPDATEACCOUNTINGINTERFACE};
			this.processlevelBarNames=new String[]{AppConstants.CRMTALLYREPORT,ManageApprovals.SUBMIT,"Tax Invoice",AppConstants.VIEWBILL,AppConstants.VIEWPAYMENT,AppConstants.VIEWORDER,AppConstants.VIEWTAXINVOICE,AppConstants.UPDATEACCOUNTINGINTERFACE,ManageApprovals.APPROVALREQUEST,ManageApprovals.CANCELAPPROVALREQUEST,"Attendance Summary","Sales Order Summary","Generate e-Invoice number","Invoice Reset",AppConstants.CANCELINVOICE,AppConstants.UPDATESALESORDERINVOICE,AppConstants.COPY,"Update Description",AppConstants.generatePayment,AppConstants.uploadDoc,AppConstants.syncWithZohoBooks};
		}
		
		/**
		 *  End.
		 */
		
		
		
		
		
		
		
		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////
		//Token to initialize formfield
		String mainScreenLabel="Invoice Details";
		if(invoiceobj!=null&&invoiceobj.getCount()!=0){
			mainScreenLabel=invoiceobj.getCount()+" "+"/"+" "+invoiceobj.getStatus()+" "+"/"+" "+AppUtility.parseDate(invoiceobj.getCreationDate());
		}
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		fgroupingCustomerInformation=fbuilder.setlabel(mainScreenLabel).widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).setKeyField(true).build(); // added by priyanka.
//		fbuilder = new FormFieldBuilder("* Customer ID",tbpersoncount);
//		FormField ftbpersoncount= fbuilder.setMandatory(true).setMandatoryMsg("ID is Mandatory!").setRowSpan(0).setColSpan(0).build();
//		fbuilder = new FormFieldBuilder("* Customer Name",tbpersonname);
//		FormField ftbpersonname= fbuilder.setMandatory(true).setMandatoryMsg("Name is Mandatory!").setRowSpan(0).setColSpan(0).build();
//		fbuilder = new FormFieldBuilder("* Cell Number",tbpersoncell);
//		FormField ftbpersoncell= fbuilder.setMandatory(true).setMandatoryMsg("Cell Number is Mandatory!").setRowSpan(0).setColSpan(0).build();
//		fbuilder = new FormFieldBuilder("POC Name",tbpocname);
//		FormField ftbpocname= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingSalesOrderInfo=fbuilder.setlabel("Classification").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();		
		fbuilder = new FormFieldBuilder("Order ID",ibcontractid);
		FormField fibcontractid=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Contract Start Date",dbstartDate);
		FormField fdbstartdate=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Contract End Date",dbendDate);
		FormField fdbenddate=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Total Order Amount",dosalesamount);
		FormField fdoamount=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingBillingInfo=fbuilder.setlabel("Reference/General").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("",billingdoctable.getTable());
		FormField fbillingdoctable=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Total Billing Amount",dototalbillamt);
		FormField fdototalbillamt=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Account Type",tbaccounttype);
		FormField ftbaccounttype=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingInvoiceInfo=fbuilder.setlabel("Invoice Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Invoice ID",ibinvoiceid);
		FormField fibinvoiceid=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Invoice Date",dbinvoicedate);
		FormField fdbinvoicedate=fbuilder.setMandatory(true).setMandatoryMsg("Invoice Date is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Billing ID",ibbillingid);
		FormField fibbillingid=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Billing Date",dbbillingdate);
		FormField fdbbillingdate=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Status",tbstatus);
		FormField ftbstatus=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Approver Name",olbApproverName);
		FormField folbApproverName= fbuilder.setMandatory(true).setMandatoryMsg("Approver Name is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Person Responsible",olbEmployee);
		FormField folbEmployee= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Branch",olbbranch);
		FormField folbbranch= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Invoice Document Type",tbinvoiceType);
		FormField ftbinvoiceType=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Payment Date",dbpaymentdate);
		FormField fdbpaymentdate=fbuilder.setMandatory(true).setMandatoryMsg("Payment Date is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Invoice Amount",doinvoiceamount);
		FormField fdoinvoiceamount=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",btnCopyContractDescription);
		FormField fbtnCopyContractDescription=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("Description (Max 500 characters)",tacomment);
		FormField ftacomment=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Remark",tbremark);
		FormField ftbremark=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		fbuilder = new FormFieldBuilder();
		FormField fblankgroup=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		/**Date 7-2-2019 
		 * @author AMOL
		 * added textbox for invoice ref no
		 */
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "RefrenceOrderNumber")){
			invoiceRefNo=true;
		}
		FormField ftbInvoiceRefNumber=null;
		if(invoiceRefNo){
		fbuilder = new FormFieldBuilder("Invoice Ref. No",tbInvoiceRefNumber);
		 ftbInvoiceRefNumber=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}else{
			fbuilder = new FormFieldBuilder();
		 ftbInvoiceRefNumber=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
		
		/**
		 * Date : 27 Feb 2018
		 * Dev : Rahul Verma
		 * Description : Did mandatory for interface level.
		 */
		FormField folbinvoicegroup;
		FormField folbInvoiceCategory;
		FormField folbInvoiceConfigType;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice","MakeInvoiceGroupMandatory"))
		{
			fbuilder = new FormFieldBuilder("* Invoice Group",olbinvoicegroup);
			folbinvoicegroup=fbuilder.setMandatory(true).setMandatoryMsg("Invoice Group is Mandatory !").setRowSpan(0).setColSpan(0).build();

		
		}
		else
		{
			fbuilder = new FormFieldBuilder("Invoice Group",olbinvoicegroup);
			folbinvoicegroup =fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice","MakeInvoiceCategoryMandatory"))
		{
			fbuilder = new FormFieldBuilder("* Invoice Category",olbInvoiceCategory);
			folbInvoiceCategory=fbuilder.setMandatory(true).setMandatoryMsg("Invoice Category is Mandatory !").setRowSpan(0).setColSpan(0).build();
		}
		else
		{
			fbuilder = new FormFieldBuilder("Invoice Category",olbInvoiceCategory);
			folbInvoiceCategory =fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice","MakeInvoiceTypeMandatory"))
		{
			fbuilder = new FormFieldBuilder("* Invoice Type",olbInvoiceConfigType);
			folbInvoiceConfigType=fbuilder.setMandatory(true).setMandatoryMsg("Invoice Type is Mandatory !").setRowSpan(0).setColSpan(0).build();
			
		}
		else
		{
			
			fbuilder = new FormFieldBuilder("Invoice Type",olbInvoiceConfigType);
			folbInvoiceConfigType =fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
		/**
		 * Ends for Rahul Verma
		 */
		
		fbuilder = new FormFieldBuilder("Contract Start Date",conStartDur);
		FormField fconStartDur=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Contract End Date",conEndDur);
		FormField fconEndDur=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Customer Branch",lbcustomerbranch);
		FormField folbcustomerbranch=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		/**
		 * Date : 18-11-2017 BY ANIL
		 * making reference number 1 mandatory through process configuration
		 */
		FormField freferenceId;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "MakeReferenceNumberMandatory")){
			fbuilder = new FormFieldBuilder("* Reference Number",referenceId);
			freferenceId= fbuilder.setMandatory(true).setMandatoryMsg("Reference Number is mandatory.").setRowSpan(0).setColSpan(0).build();
		}else{
			fbuilder = new FormFieldBuilder("Reference Number",referenceId);
			freferenceId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
		/**
		 * End
		 */
		
		
		//   rohan added all this for editable invoice documet ************************
		fbuilder = new FormFieldBuilder();
		FormField fgroupingProductTable=fbuilder.setlabel("Product").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		
		fbuilder = new FormFieldBuilder();
		FormField fblank=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder();
		fbuilder = new FormFieldBuilder("",salesProductTable.getTable());
		FormField fsalesProducttbl=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder();
		fbuilder = new FormFieldBuilder("",invoiceTaxTable.getTable());
		FormField fbillingTaxTable=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder();
		fbuilder = new FormFieldBuilder("",invoiceChargesTable.getTable());
		FormField fbillingChargesTable=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder();
		fbuilder = new FormFieldBuilder("Proforma Invoice Ref Number",proformaRefId);
		FormField fproformaRefId=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();

		
		FormField fgroupingPaymentTerms=fbuilder.setlabel("Payment Terms").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Days",ibpaytermsdays);
		FormField fibpaytermsdays=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();		
		fbuilder = new FormFieldBuilder("Percent",dopaytermspercent);
		FormField fdopaytermspercent=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Comment",tbpaytermscomment);
		FormField ftbpaytermscomment=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Round Off",tbroundOffAmt);
		FormField fdiscount=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("NetPayable",netPayable);
		FormField fnetPayable=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		FormField fgroupingBillingPeroid=fbuilder.setlabel("Billing Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();

		fbuilder = new FormFieldBuilder("Billing From Date",dbbillingperiodFromDate);
		FormField fdbbillingperiodFromDate=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Billing To Date",dbbillingperiodToDate);
		FormField fdbbillingperiodToDate=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",btReferenceDetails);
		FormField fbtReferenceDetails= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/*
		 * nidhi
		 * 30-06-2017
		 */
		fbuilder = new FormFieldBuilder("Segment",tbSegment);
		FormField ftbSegment=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		/*
		 * End
		 */
		
		/*
		 * nidhi
		 * 18-07-2017
		 * search field add in form
		 */
		fbuilder = new FormFieldBuilder("Service Id",tbrateContractServiceid);
		FormField ftbrateContractServiceid=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		/*
		 *  end
		 */
		
		/**
		 * Date : 19-07-2017 BY ANIL
		 */
		fbuilder = new FormFieldBuilder("GSTIN Number",tbGstinNumber);
		FormField ftbGstinNumber=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		/**
		 *  end
		 */
		
		/*
		 * nidhi
		 * 30-06-2017
		 */
		fbuilder = new FormFieldBuilder("Quantity",quantity);
		FormField ftbQuantity=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("UOM",olbUOM);
		FormField folbUom=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		/*
		 * End
		 */
		
		
		/** Date 04-09-2017 added by vijay for discount amount final total amount and round off ***/
		fbuilder = new FormFieldBuilder("Total Amount", dbtotalAmt);
		FormField fdbtotalAmt = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Discount Amount", dbDiscountAmt);
		FormField fdbDiscountAmt = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Total Amount", dbFinalTotalAmt);
		FormField fdbFinalTotalAmt = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Total Amount", dbtotalAmtIncludingTax);
		FormField fdbtotalAmtIncludingTax = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("",tblOtherCharges.getTable());
		FormField fothChgTbl= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("",addOthrChargesBtn);
		FormField faddOthrChargesBtn= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Total Amount",dbOtherChargesTotal);
		FormField fdbOtherChargesTotal= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Consolidate Price",cbConsolidatePrice);
		FormField fcbConsolidatePrice= fbuilder.setMandatory(true).setRowSpan(0).setColSpan(0).build();
		/**
		 * nidhi
		 */
		
		fbuilder = new FormFieldBuilder("Bank Name",tbPaymentName);
		FormField ftbPaymentName= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Account Number",tbPaymentBankAC);
		FormField ftbPaymentBankAC= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Payable At",tbPaymentPayableAt);
		FormField ftbPaymentPayableAt= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Bank Branch",tbPaymentAccountName);
		FormField ftbPaymentAccountName= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Payment Mode",objPaymentMode);
		FormField fobjPaymentMode= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/** date 20.10.2018 added by komal for type of order and sub bill type **/	
		fbuilder = new FormFieldBuilder("Type Of Order", tbTypeOfOrder);
		FormField ftbTypeOfOrder= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Sub Bill Type",tbSubBillType);
		FormField ftbSubBillType= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		/** date 27.10.2018 added by komal for tds **/
		fbuilder = new FormFieldBuilder("TDS Percentage",tbtdsPerValue);
		FormField ftdsValue=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Is TDS Applicable",cbtdsApplicable);
		FormField fcbtdsApplicable=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("TDS Amount",dbtdsValue);
		FormField fibtdsValue=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Ref PO No.",tbPONumber);
		FormField ftbPONumber=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Ref WO No.",tbWONumber);
		FormField ftbWONumber=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Ref Contract No.", tbContractNumber);
		FormField ftbContractNumber=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		FormField fgroupingTDSFormation=fbuilder.setlabel("TDS").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();

		fbuilder = new FormFieldBuilder("Eway Bill Number",tbEwayBillNumber);
		FormField ftbEwayBillNumber= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/**
		 * @author Vijay Chougule Date:- 11-08-2020
		 * Des :- TO show CNC project Name
		 */
		fbuilder = new FormFieldBuilder("Project Name",tbprojectName);
		FormField ftbprojectName=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		ftbprojectName.setVisible(false);
		
		/**
		 * @author Vijay Chougule Date:- 12-08-2020
		 * Des :- TO show Credit Period from CNC and Sales Order to caluclate Payment Date
		 */
		fbuilder = new FormFieldBuilder("Credit Period",ibcreditPeriod);
		FormField fibcreditPeriod=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fibcreditPeriod.setVisible(false);
		
		fbuilder = new FormFieldBuilder("Dispatch Date",dbDispatchDate);
		FormField fdbDispatchDate=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fdbDispatchDate.setVisible(false);
		
		FormField ftbInvPrefix=null;
//		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "PC_PRINTINVOICENUMBERPREFIX")){
//			fbuilder = new FormFieldBuilder("Invoice Prefix",tbInvPrefix);
//			ftbInvPrefix= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
//		}else{
//			fbuilder = new FormFieldBuilder();
//			ftbInvPrefix= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
//		}
		
		/**
		 *  Date : 10/03/2021 By Priyanka.
		 *  Des : Add description1 box for sunrise req. raised by Rahul.
		 */
		fbuilder = new FormFieldBuilder("Description1 (Max 500 characters)",taComment1);
		FormField ftaComment1=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		ftaComment1.setVisible(false);
		
		fbuilder = new FormFieldBuilder("Upload 1",upload1);
		FormField fupload1= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Upload 2",upload2);
		FormField fupload2= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Template",obTermsandCondition);
		FormField fobTermsandCondition=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Declaration",taDeclaration);
		FormField ftaDeclaration=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
	
		fbuilder = new FormFieldBuilder("Do not print service address" , cbdonotprintserviceAddress);
		FormField fcbdonotprintserviceAddress = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		/**@author Sheetal : 28-05-2022
		 * Des : Adding new tab as Oracle, for Envocare only **/
		
		FormField fgroupingOracle=fbuilder.setlabel("Oracle").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("* Payment Terms",lbPaymentTerms);
		FormField flbPaymentTerms=fbuilder.setMandatory(true).setMandatoryMsg("Payment Terms is Mandatory !").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Transaction Line Type",lbTransactionLineType);
		FormField flbTransactionLineType=fbuilder.setMandatory(true).setMandatoryMsg("Transaction Line Type is Mandatory !").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Tax Classification Code",olbTaxClassificationCode);
		FormField folbTaxClassificationCode=fbuilder.setMandatory(true).setMandatoryMsg("Tax Classification Code is Mandatory !").setRowSpan(0).setColSpan(0).build();
	
		fbuilder = new FormFieldBuilder("* Tax Exemption Flag",lbTaxExemptionFlag);
		FormField flbTaxExemptionFlag=fbuilder.setMandatory(true).setMandatoryMsg("Tax Exemption Flag is Mandatory !").setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Sub Contractor Name",obVendor);
		FormField fobVendor=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("",personInfoComposite);
		FormField fpersonInfoComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		//Ashwini Patil Date:14-09-2022
		FormField fgroupingHistory=fbuilder.setlabel("Document History").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		Console.log("new form fields created");
		fbuilder = new FormFieldBuilder("Created By",tbCreatedBy);
		FormField ftbCreatedBy=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Created On",tbCreatedOn);
		FormField ftbCreatedOn=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Updated By",tbUpdatedBy);
		FormField ftbUpdatedBy=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Updated On",tbUpdatedOn);
		FormField ftbUpdatedOn=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("Approved By",tbApprovedBy);
		FormField ftbApprovedBy=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Approved On",tbApprovedOn);		
		FormField ftbApprovedOn=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Cancelled By",tbCancelledBy);		
		FormField ftbCancelledBy=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Cancelled On",tbCancelledOn);
		FormField ftbCancelledOn=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Log",tbLog);		
		FormField ftbLog=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		//Ashwini Patil Date:14-09-2022
		FormField fgroupingEinvoice=fbuilder.setlabel("e-Invoice").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		
		fbuilder = new FormFieldBuilder("eInvoice No",eInvoiceNo);
		FormField feInvoiceNo=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("Acknowledgement No",ackNo);
		FormField fackNo=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Acknowledgement Date",ackDate);
		FormField fackDate=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("QR Code String",qrCode);
		FormField fqrCode=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Status",eInvoiceStatus);
		FormField feInvoiceStatus=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Cancellation Date",eInvoiceCancellationDate);
		FormField feInvoiceCancellationDate=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingPrintOptions=fbuilder.setlabel("Print Options").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
	
		fbuilder = new FormFieldBuilder("Invoie Format" , invoicepdfNametoprint);
		FormField finvoicepdfNametoprint = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",prodInfoComposite);
		FormField fprodInfoComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder("",baddproducts);
		FormField fbaddproducts= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		//Ashwini Patil Date:9-05-2024
		FormField fgroupingZoho=fbuilder.setlabel("Zoho").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
				
		fbuilder = new FormFieldBuilder("Zoho Invoice ID",tbZohoInvoiceId);
		FormField ftbZohoInvoiceId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Unique Document ID",tbZohoInvoiceUniqueID);
		FormField ftbZohoInvoiceUniqueID= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Sync Information",tbZohoSyncInfo);
		FormField ftbZohoSyncInfo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Sync Date",tbZohoSyncDate);
		FormField ftbZohoSyncDate =fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
//		FormField[][] formfield = { 
//				{fgroupingCustomerInformation},
//				{ftbpersoncount,ftbpersonname,ftbpersoncell,ftbpocname},
//				{fgroupingSalesOrderInfo},
//				{fibcontractid,fdoamount,ftbaccounttype,freferenceId},
//				{ftbSegment,ftbrateContractServiceid,ftbGstinNumber ,ftbPONumber},
//				{ftbWONumber , ftbContractNumber,ftbprojectName,ftbInvoiceRefNumber },
//				{fgroupingBillingInfo},
//				{fbillingdoctable},
//				{fgroupingProductTable},
//				{fsalesProducttbl},
//				{fblankgroup,fdbtotalAmt},
//				{fblankgroup,fdbDiscountAmt},
//				{fblankgroup,fdbFinalTotalAmt},
//				
//				{fblank,faddOthrChargesBtn},
//				{fblank,fothChgTbl},/** Date : 20-09-2017 BY ANIL**/
//				{fblankgroup,fdbOtherChargesTotal},
//				
//				{fblank,fbillingTaxTable},
//				{fblankgroup,fdbtotalAmtIncludingTax},
//
////				{fblank,fbillingChargesTable},
////				{fblankgroup,fdototalbillamt},
//				
//				//   rohan added this for discount on invoice amt 
//				
//				{fblankgroup,fdiscount},
//				{fblankgroup,fnetPayable},
//				/** date 27.10.2018 added by komal for tds ***/
//				{fgroupingTDSFormation},
//				{fcbtdsApplicable ,ftdsValue ,fibtdsValue},
//				
//				{fgroupingPaymentTerms},
//				{fibpaytermsdays,fdopaytermspercent,ftbpaytermscomment,fcbConsolidatePrice},/** Date : 06-02-2018 BY komal**/
//				{fibcreditPeriod},
//				{fgroupingInvoiceInfo},
//				{fibinvoiceid,ftbstatus,ftbinvoiceType,fdoinvoiceamount},
//				{fdbinvoicedate,fdbDispatchDate,fdbpaymentdate,folbbranch},
//				{folbApproverName,folbinvoicegroup,folbInvoiceCategory,folbInvoiceConfigType},
//				{ftbremark,fconStartDur,fconEndDur,folbcustomerbranch},
//				{fproformaRefId,fbtReferenceDetails,ftbQuantity,folbUom},
//				{folbEmployee,ftbTypeOfOrder , ftbSubBillType,ftbEwayBillNumber},	/** date 20.10.2018 added by komal **/	
//				{fobjPaymentMode},
//				{ftbPaymentName,ftbPaymentBankAC,ftbPaymentAccountName,ftbPaymentPayableAt},
//				
//				{ftacomment},
//				{fgroupingBillingPeroid},
//				{fdbbillingperiodFromDate,fdbbillingperiodToDate}
//		};
		

		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice",AppConstants.PC_EnableOracletab)){
		FormField[][] formfield = {   {fgroupingCustomerInformation},

//				{ftbpersoncount,ftbpersonname,ftbpersoncell,ftbpocname},
				{fpersonInfoComposite},
				{folbbranch,fdoinvoiceamount,fdbinvoicedate,fdbpaymentdate},
				{folbApproverName,ftbremark},
				/**Production Information**/
				{fgroupingProductTable},
				{fcbConsolidatePrice,ftbQuantity,folbUom},
				{fprodInfoComposite,fbaddproducts},
				{fsalesProducttbl},
				{fblankgroup,fdbtotalAmt},
				{fblankgroup,fdbDiscountAmt},
				{fblankgroup,fdbFinalTotalAmt},
				
				{fblank,faddOthrChargesBtn},
				{fblank,fothChgTbl},/** Date : 20-09-2017 BY ANIL**/
				{fblankgroup,fdbOtherChargesTotal},
				
				{fblank,fbillingTaxTable},
				{fblankgroup,fdbtotalAmtIncludingTax},

////				{fblank,fbillingChargesTable},
////				{fblankgroup,fdototalbillamt},
				
//				//   rohan added this for discount on invoice amt 
				
				{fblankgroup,fdiscount},
				{fblankgroup,fnetPayable},
				/**TDS**/
				{fgroupingTDSFormation},
				{fcbtdsApplicable ,ftdsValue ,fibtdsValue},
				/**Billing information**/
				{fgroupingBillingPeroid},
				{fdototalbillamt,ftbSubBillType,fdbbillingperiodFromDate,fdbbillingperiodToDate},
				{ftbGstinNumber,fproformaRefId,},
				{fbillingdoctable},
				/**Payment terms Information**/
				{fgroupingPaymentTerms},

				{fibpaytermsdays,fdopaytermspercent,ftbpaytermscomment,ftbinvoiceType},
				{fobjPaymentMode,ftbPaymentName,ftbPaymentBankAC,ftbPaymentAccountName},
				{ftbPaymentPayableAt},
				/**Classification Information**/
				{fgroupingSalesOrderInfo},
				{folbinvoicegroup,folbInvoiceCategory,folbInvoiceConfigType,ftbSegment},
				
								
				/**Reference and genral information**/
				{fgroupingBillingInfo},
				{fibcontractid,fdoamount,ftbrateContractServiceid,ftbPONumber},
				{ftbWONumber,freferenceId,ftbContractNumber,ftbaccounttype},
				{ftbEwayBillNumber,fconStartDur,fconEndDur,folbcustomerbranch},
				{ftbTypeOfOrder,folbEmployee,ftbInvoiceRefNumber,ftbprojectName},// Added by Priyanka				
				{ftbZohoInvoiceId},
				{fbtnCopyContractDescription},
				{ftacomment},
				/**
				 *  Date : 10/03/2021 By Priyanka.
				 *  Des : Add description1 box for sunrise req. raised by Rahul.
				 */
				{ftaComment1},
				
				{fobTermsandCondition,fcbdonotprintserviceAddress},
				{ftaDeclaration},
				
				{fupload1,fupload2},
				
				/**Oracle**/ //Added by sheetal:29-05-2022, for Envocare
				{fgroupingOracle},
				{flbPaymentTerms,flbTransactionLineType,folbTaxClassificationCode,flbTaxExemptionFlag},
				{fobVendor},
				
				/**Document History**/
				{fgroupingHistory},
				{ftbCreatedBy,ftbCreatedOn},
				{ftbUpdatedBy,ftbUpdatedOn},
				{ftbApprovedBy,ftbApprovedOn},				
				{ftbCancelledBy,ftbCancelledOn},
				{ftbLog},				
				
				/** e-Invoice **/
				{fgroupingEinvoice},
				{feInvoiceStatus,feInvoiceCancellationDate},
				{feInvoiceNo},
				{fackNo},
				{fackDate},
				{fqrCode},
				{fgroupingPrintOptions},
				{finvoicepdfNametoprint}
				
				
	
		};

		/**
		 * @author Vijay Chougule Date:- 11-08-2020
		 * Des :- CNC project Name will display when below process config is active 
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "AddProjectName")){
			ftbprojectName.setVisible(true);
		}
		else{
			ftbprojectName.setVisible(false);
		}
		/**
		 * ends here
		 */
		/**
		 * @author Vijay Chougule Date:- 12-08-2020
		 * Des :- TO show Credit Period from CNC and Sales Order to caluclate Payment Date
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "AddCreditPeriodAndDispatchDate")){
			fibcreditPeriod.setVisible(true);
			fdbDispatchDate.setVisible(true);
		}
		else{
			fibcreditPeriod.setVisible(false);
			fdbDispatchDate.setVisible(false);

		}
		/**
		 * ends here
		 */
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "AddCommentBox1")){
			ftaComment1.setVisible(true);
		}
		else{
			ftaComment1.setVisible(false);
		}
		this.fields=formfield;
	}  
		else{

		FormField[][] formfield = {   {fgroupingCustomerInformation},

//				{ftbpersoncount,ftbpersonname,ftbpersoncell,ftbpocname},
				{fpersonInfoComposite},
				{folbbranch,fdoinvoiceamount,fdbinvoicedate,fdbpaymentdate},
				{folbApproverName,ftbremark},
				/**Production Information**/
				{fgroupingProductTable},
				{fcbConsolidatePrice,ftbQuantity,folbUom},
				{fprodInfoComposite,fbaddproducts},
				{fsalesProducttbl},
				{fblankgroup,fdbtotalAmt},
				{fblankgroup,fdbDiscountAmt},
				{fblankgroup,fdbFinalTotalAmt},
				
				{fblank,faddOthrChargesBtn},
				{fblank,fothChgTbl},/** Date : 20-09-2017 BY ANIL**/
				{fblankgroup,fdbOtherChargesTotal},
				
				{fblank,fbillingTaxTable},
				{fblankgroup,fdbtotalAmtIncludingTax},

////				{fblank,fbillingChargesTable},
////				{fblankgroup,fdototalbillamt},
				
//				//   rohan added this for discount on invoice amt 
				
				{fblankgroup,fdiscount},
				{fblankgroup,fnetPayable},
				/**TDS**/
				{fgroupingTDSFormation},
				{fcbtdsApplicable ,ftdsValue ,fibtdsValue},
				/**Billing information**/
				{fgroupingBillingPeroid},
				{fdototalbillamt,ftbSubBillType,fdbbillingperiodFromDate,fdbbillingperiodToDate},
				{ftbGstinNumber,fproformaRefId,},
				{fbillingdoctable},
				/**Payment terms Information**/
				{fgroupingPaymentTerms},

				{fibpaytermsdays,fdopaytermspercent,ftbpaytermscomment,ftbinvoiceType},
				{fobjPaymentMode,ftbPaymentName,ftbPaymentBankAC,ftbPaymentAccountName},
				{ftbPaymentPayableAt},
				/**Classification Information**/
				{fgroupingSalesOrderInfo},
				{folbinvoicegroup,folbInvoiceCategory,folbInvoiceConfigType,ftbSegment},
				
				
				/**Reference and genral information**/
				{fgroupingBillingInfo},
				{fibcontractid,fdoamount,ftbrateContractServiceid,ftbPONumber},
				{ftbWONumber,freferenceId,ftbContractNumber,ftbaccounttype},
				{ftbEwayBillNumber,fconStartDur,fconEndDur,folbcustomerbranch},
				{ftbTypeOfOrder,folbEmployee,ftbInvoiceRefNumber,ftbprojectName},// Added by Priyanka				
				{ftbZohoInvoiceId},
				{fbtnCopyContractDescription},
				{ftacomment},
				/**
				 *  Date : 10/03/2021 By Priyanka.
				 *  Des : Add description1 box for sunrise req. raised by Rahul.
				 */
				{ftaComment1},
				
				{fobTermsandCondition,fcbdonotprintserviceAddress},
				{ftaDeclaration},
				
				{fupload1,fupload2},
	
				/**Document History**/
				{fgroupingHistory},
				{ftbCreatedBy,ftbCreatedOn},
				{ftbUpdatedBy,ftbUpdatedOn},
				{ftbApprovedBy,ftbApprovedOn},				
				{ftbCancelledBy,ftbCancelledOn},
				{ftbLog},
				
				/** e-Invoice **/
				{fgroupingEinvoice},
				{feInvoiceStatus,feInvoiceCancellationDate},
				{feInvoiceNo},
				{fackNo},
				{fackDate},
				{fqrCode},
				
				{fgroupingPrintOptions},
				{finvoicepdfNametoprint},
				
				{fgroupingZoho},
				{ftbZohoInvoiceId,ftbZohoInvoiceUniqueID},
				{ftbZohoSyncDate,ftbZohoSyncInfo}
		};

		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "EnableInvoiceIntegrationWithZohoBooks")) {
			fgroupingZoho.setVisible(true);
			ftbZohoInvoiceId.setVisible(true);
			ftbZohoInvoiceUniqueID.setVisible(true);
			ftbZohoSyncDate.setVisible(true);
			ftbZohoSyncInfo.setVisible(true);

		}else {
			fgroupingZoho.setVisible(false);
			ftbZohoInvoiceId.setVisible(false);
			ftbZohoInvoiceUniqueID.setVisible(false);
			ftbZohoSyncDate.setVisible(false);
			ftbZohoSyncInfo.setVisible(false);
		}


		/**
		 * @author Vijay Chougule Date:- 11-08-2020
		 * Des :- CNC project Name will display when below process config is active 
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "AddProjectName")){
			ftbprojectName.setVisible(true);
		}
		else{
			ftbprojectName.setVisible(false);
		}
		/**
		 * ends here
		 */
		/**
		 * @author Vijay Chougule Date:- 12-08-2020
		 * Des :- TO show Credit Period from CNC and Sales Order to caluclate Payment Date
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "AddCreditPeriodAndDispatchDate")){
			fibcreditPeriod.setVisible(true);
			fdbDispatchDate.setVisible(true);
		}
		else{
			fibcreditPeriod.setVisible(false);
			fdbDispatchDate.setVisible(false);

		}
		/**
		 * ends here
		 */
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "AddCommentBox1")){
			ftaComment1.setVisible(true);
		}
		else{
			ftaComment1.setVisible(false);
		}
		this.fields=formfield;
	
	}

	}

	@Override
	public void updateModel(Invoice model) {
		
//		PersonInfo personInfo=new PersonInfo();
//		personInfo.setCount(Integer.parseInt(tbpersoncount.getValue()));
//		personInfo.setFullName(tbpersonname.getValue());
//		personInfo.setCellNumber(Long.parseLong(tbpersoncell.getValue()));
//		personInfo.setPocName(tbpocname.getValue());
//		model.setPersonInfo(personInfo);
		
		if(personInfoComposite.getValue()!=null)
			model.setPersonInfo(personInfoComposite.getValue());
		
		if(!ibcontractid.getValue().equals(""))
			model.setContractCount(Integer.parseInt(ibcontractid.getValue()));
		if(dosalesamount.getValue()!=null)
			model.setTotalSalesAmount(dosalesamount.getValue());
		List<BillingDocumentDetails> billdoclis=this.billingdoctable.getDataprovider().getList();
		ArrayList<BillingDocumentDetails> billtablearr=new ArrayList<BillingDocumentDetails>();
		billtablearr.addAll(billdoclis);
			model.setArrayBillingDocument(billtablearr);
			
		if(dototalbillamt.getValue()!=null)
			model.setTotalBillingAmount(dototalbillamt.getValue());
		if(dbinvoicedate.getValue()!=null)
			model.setInvoiceDate(dbinvoicedate.getValue());
		if(tbinvoiceType.getValue()!=null)
			model.setInvoiceType(tbinvoiceType.getValue());
		if(doinvoiceamount.getValue()!=null)
			model.setInvoiceAmount(doinvoiceamount.getValue());
		if(dbpaymentdate.getValue()!=null)
			model.setPaymentDate(dbpaymentdate.getValue());
		if(tbstatus.getValue()!=null)
			model.setStatus(tbstatus.getValue());
		if(olbApproverName.getValue()!=null)
			model.setApproverName(olbApproverName.getValue());
		if(olbEmployee.getValue()!=null)
			model.setEmployee(olbEmployee.getValue());
		if(olbbranch.getSelectedIndex()!=0)
			model.setBranch(olbbranch.getValue(olbbranch.getSelectedIndex()));
		if(tacomment.getValue()!=null)
			model.setComment(tacomment.getValue());
//		if(tbrejectreason.getValue()!=null){
//			model.setRejectReason(tbrejectreason.getValue());
//		}
			
		if(olbInvoiceCategory.getSelectedIndex()!=0){
			model.setInvoiceCategory(olbInvoiceCategory.getValue(olbInvoiceCategory.getSelectedIndex()));
		}
		if(olbInvoiceConfigType.getSelectedIndex()!=0){
			model.setInvoiceConfigType(olbInvoiceConfigType.getValue(olbInvoiceConfigType.getSelectedIndex()));
		}
		if(olbinvoicegroup.getSelectedIndex()!=0){
			model.setInvoiceGroup(olbinvoicegroup.getValue(olbinvoicegroup.getSelectedIndex()));
		}
		model.setInvoiceCount(Integer.parseInt(ibinvoiceid.getValue()));
		model.setAccountType(tbaccounttype.getValue());
		
		
		
		if(conStartDur.getValue()!=null)
			model.setConStartDur(conStartDur.getValue());
		
		if(conEndDur.getValue()!=null)
			model.setConEndDur(conEndDur.getValue());
		
		System.out.println("RRRRRR  "+referenceId.getText());
		if(referenceId.getValue()!=null)
			model.setRefNumber(referenceId.getText());
		
		
		if(lbcustomerbranch.getSelectedIndex()!=0){
			model.setCustomerBranch(lbcustomerbranch.getValue(lbcustomerbranch.getSelectedIndex()));
		}
		else
		{
			model.setCustomerBranch("");
		}
		
		//    rohan added this code for invoice editing ************ 
		
		
		List<SalesOrderProductLineItem> salesProductlis=this.salesProductTable.getDataprovider().getList();
		ArrayList<SalesOrderProductLineItem> arrSalesProduct=new ArrayList<SalesOrderProductLineItem>();
		arrSalesProduct.addAll(salesProductlis);
		model.setSalesOrderProducts(arrSalesProduct);
		
		List<PaymentTerms> payTrmLis=this.savePaymentTerms();
		ArrayList<PaymentTerms> payTrmsArray=new ArrayList<PaymentTerms>();
		payTrmsArray.addAll(payTrmLis);
		model.setArrPayTerms(payTrmsArray);
		
		List<ContractCharges> taxBillingLis=this.invoiceTaxTable.getDataprovider().getList();
		ArrayList<ContractCharges> taxBillingArr=new ArrayList<ContractCharges>();
		taxBillingArr.addAll(taxBillingLis);
		model.setBillingTaxes(taxBillingArr);
		
		/**
		 * Date : 21-09-2017 BY ANIL
		 */
		List<OtherCharges> otherChargesList=this.tblOtherCharges.getDataprovider().getList();
		ArrayList<OtherCharges> otherChargesArr=new ArrayList<OtherCharges>();
		otherChargesArr.addAll(otherChargesList);
		model.setOtherCharges(otherChargesArr);
		
		if(dbOtherChargesTotal.getValue()!=null){
		model.setTotalOtherCharges(dbOtherChargesTotal.getValue());
		}
		
		/**
		 * End
		 */
		
		List<ContractCharges> chrgBillingLis=this.invoiceChargesTable.getDataprovider().getList();
		ArrayList<ContractCharges> chrgBillingArr=new ArrayList<ContractCharges>();
		chrgBillingArr.addAll(chrgBillingLis);
		model.setBillingOtherCharges(chrgBillingArr);
		
		if(proformaRefId.getValue()!= null && !proformaRefId.getValue().equals(""))
			model.setProformaCount(Integer.parseInt(proformaRefId.getValue()));
		
		
		if(tbroundOffAmt.getValue()!=null && !tbroundOffAmt.getValue().equals(""))
			model.setDiscount(Double.parseDouble(tbroundOffAmt.getValue()));
		
		if(netPayable.getValue()!=null)
		{
			model.setNetPayable(netPayable.getValue());
		}
		
		/**
		 * @author Abhinav Bihade
		 * @since 24/12/2019
		 * As per Vishnavi Pawar's Requirement ISPC - Even if remove from date and to date from billing period of billing screen 
		 * and invoice screen
		 */
		if(dbbillingperiodFromDate.getValue()!=null){
			model.setBillingPeroidFromDate(dbbillingperiodFromDate.getValue());
		}
		else{
			model.setBillingPeroidFromDate(null);
			
		}
		if(dbbillingperiodToDate.getValue()!=null){
			model.setBillingPeroidToDate(dbbillingperiodToDate.getValue());
		}
		else{
			model.setBillingPeroidToDate(null);
			}
		
		/*
		 *  nidhi
		 *  1-07-2017
		 */
		
		if(tbSegment.getValue()!=null)
			model.setSegment(tbSegment.getValue());
		
		/*
		 *  end
		 */
		
		/*
		 *  nidhi
		 *  17-07-2017
		 *  
		 */
		if(!tbrateContractServiceid.getValue().equals("")){
			model.setRateContractServiceId(Integer.parseInt(tbrateContractServiceid.getValue()));
		}
		/*
		 *  end
		 */
		
		if(tbGstinNumber.getValue()!=null){
			model.setGstinNumber(tbGstinNumber.getValue());
		}
		
		/**
		 *  nidhi
		 *  20-07-2017
		 *    for save values 
		 */
		if(quantity.getValue()!=null){
				model.setQuantity(quantity.getValue());
		}
		
		if(olbUOM.getValue()!=null){
			model.setUom(olbUOM.getValue().trim());
		}
		/**
		 * end
		 *
		 */
		
		/** Date 04-09-2017 added by vijay for discount amount final total amount and round off ***/
		if(dbDiscountAmt.getValue()!=null)
			model.setDiscountAmt(dbDiscountAmt.getValue());
		if(dbFinalTotalAmt.getValue() !=null)
			model.setFinalTotalAmt(dbFinalTotalAmt.getValue());
		if(dbtotalAmt.getValue()!=null)
			model.setTotalAmtExcludingTax(dbtotalAmt.getValue());
		if(dbtotalAmtIncludingTax.getValue()!=null)
			model.setTotalAmtIncludingTax(dbtotalAmtIncludingTax.getValue());
		/** Date 06/2/2018 added by komal for consolidate price checkbox **/
		if(cbConsolidatePrice.getValue() !=null){
			model.setConsolidatePrice(cbConsolidatePrice.getValue());
		}
		/**
		 * end komal
		 */
		if(objPaymentMode.getSelectedIndex()!=0){
			model.setPaymentMode(objPaymentMode.getValue(objPaymentMode.getSelectedIndex()));
		}else{
			model.setPaymentMode("");
			
		}
		
		/** date 24.10.2018 added by komal for tds **/
		if(dbtdsValue.getValue()!=null){
			model.setTdsTaxValue(dbtdsValue.getValue());
		}
		
		if(tbtdsPerValue.getValue()!=null){
			model.setTdsPercentage(tbtdsPerValue.getValue()+"");
		}
		model.setTdsApplicable(cbtdsApplicable.getValue());
		if(tbPONumber.getValue() != null){
			model.setPoNumber(tbPONumber.getValue());
		}
		if(tbWONumber.getValue() != null){
			model.setWoNumber(tbWONumber.getValue());
		}
		if(tbContractNumber.getValue() != null){
			model.setContractNumber(tbContractNumber.getValue());
		}
		if(tbInvoiceRefNumber.getValue()!=null){
			model.setInvRefNumber(tbInvoiceRefNumber.getValue());
			}
		
		if(tbEwayBillNumber.getValue()!=null){
			model.setEwayBillNumber(tbEwayBillNumber.getValue());
		}
		
		if(tbprojectName.getValue()!=null){
			model.setProjectName(tbprojectName.getValue());
		}
		
		if(ibcreditPeriod.getValue()!=null){
			model.setCreditPeriod(ibcreditPeriod.getValue());
		}
		if(dbDispatchDate.getValue()!=null){
			model.setDispatchDate(dbDispatchDate.getValue());
		}
//		if(tbInvPrefix.getValue()!=null){
//			model.setInvPrefix(tbInvPrefix.getValue());
//		}

		if(taComment1.getValue()!=null){
			model.setComment1(taComment1.getValue());
		}
		if(upload1.getValue()!=null) {
			model.setDocUpload1(upload1.getValue());
		}
		if(upload2.getValue()!=null) {
			model.setDocUpload2(upload2.getValue());
		}
		
		if(taDeclaration.getValue()!=null){
			model.setDeclaration(taDeclaration.getValue());
		}
		model.setDonotprintServiceAddress(cbdonotprintserviceAddress.getValue());

		if(lbPaymentTerms.getSelectedIndex()!=0) {
			model.setPaymentTerms(lbPaymentTerms.getValue(lbPaymentTerms.getSelectedIndex()));
		}
		
		if(lbTransactionLineType.getSelectedIndex()!=0) {
			model.setTransactionLineType(lbTransactionLineType.getValue(lbTransactionLineType.getSelectedIndex()));
		}
		
		if(lbTaxExemptionFlag.getSelectedIndex()!=0) {
			model.setTaxExemptionCode(lbTaxExemptionFlag.getValue(lbTaxExemptionFlag.getSelectedIndex()));
		}
		
		if(olbTaxClassificationCode.getValue() != null){
			model.setTaxClassificationCode(olbTaxClassificationCode.getValue());
		}
		
		if(obVendor.getSelectedIndex()!=0){
			model.setVendorName(obVendor.getValue(obVendor.getSelectedIndex()));
		}
		if(invoicepdfNametoprint.getSelectedIndex()!=0){
			model.setInvoicePdfNameToPrint(invoicepdfNametoprint.getValue(invoicepdfNametoprint.getSelectedIndex()));
		}
		else{
			model.setInvoicePdfNameToPrint("");
		}
		if(tbZohoInvoiceId.getValue()!=null&&!tbZohoInvoiceId.getValue().equals("")){
			model.setZohoInvoiceID(tbZohoInvoiceId.getValue());
		}
		else{
			model.setZohoInvoiceID("");
		}
		
		invoiceobj=model;
		
		model.setUpdatedBy(LoginPresenter.loggedInUser);
		Date d=new Date();
		model.setUpdationDate(d);
		model.setUpdationTime(d.getTime()+"");
		
		presenter.setModel(model);
		
	}
	
	/**
	 * This method is used for saving payment terms for current billing document.
	 * @return
	 */
	public List<PaymentTerms> savePaymentTerms()
	{
		ArrayList<PaymentTerms> arrPaymentTerms=new ArrayList<PaymentTerms>();
		PaymentTerms billPayTrms=new PaymentTerms();
		if(ibpaytermsdays.getValue()==0||ibpaytermsdays.getValue()!=0)
			billPayTrms.setPayTermDays(ibpaytermsdays.getValue());
		if(dopaytermspercent.getValue()!=0)
			billPayTrms.setPayTermPercent(dopaytermspercent.getValue());
		if(tbpaytermscomment.getValue()!=null)
			billPayTrms.setPayTermComment(tbpaytermscomment.getValue());
		
		arrPaymentTerms.add(billPayTrms);
		
		return arrPaymentTerms;
	}

	@Override
	public void updateView(Invoice view) {
		
		System.out.println("Rohan in side update view from list");
		invoiceobj=view;
		
		 /**
		  *  nidhi
		  *  Date : 4-12-2017
		  *  Check process configration for rajsthan pest control contract group category and type map to service billing invoice payment
		  */
		 boolean confiFlag  =  AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "ContractCategoryAsServiceType");
		 if(confiFlag){
		    valideCofigInfoDetails(view);
		 }
		 /**
		  * end
		  */
		
		ibinvoiceid.setValue(view.getCount()+"");
		
//		if(view.getPersonInfo()!=null){
//			tbpersoncount.setValue(view.getPersonInfo().getCount()+"");
//			tbpersonname.setValue(view.getPersonInfo().getFullName());
//			tbpersoncell.setValue(view.getPersonInfo().getCellNumber()+"");
//			tbpocname.setValue(view.getPersonInfo().getPocName());
//		}
		
		if(view.getPersonInfo()!=null)
			personInfoComposite.setValue(view.getPersonInfo());
		
		if(view.getRemark()!=null){
			tbremark.setValue(view.getRemark());
		}
		if(view.getContractCount()!=null)
			ibcontractid.setValue(view.getContractCount()+"");
		if(view.getTotalSalesAmount()!=null)
			dosalesamount.setValue(view.getTotalSalesAmount());
		if(view.getInvoiceDate()!=null)
			dbinvoicedate.setValue(view.getInvoiceDate());
		if(view.getInvoiceType()!=null)
			tbinvoiceType.setValue(view.getInvoiceType());
		if(view.getPaymentDate()!=null)
			dbpaymentdate.setValue(view.getPaymentDate());
		if(view.getStatus()!=null)
			tbstatus.setValue(view.getStatus());
		if(view.getApproverName()!=null)
			olbApproverName.setValue(view.getApproverName());
		if(view.getEmployee()!=null)
			olbEmployee.setValue(view.getEmployee());
		if(view.getBranch()!=null)
			olbbranch.setValue(view.getBranch());
		billingdoctable.setValue(view.getArrayBillingDocument());
		if(view.getTotalBillingAmount()!=null)
			dototalbillamt.setValue(view.getTotalBillingAmount());
		if(view.getInvoiceAmount()!=null)
			doinvoiceamount.setValue(view.getInvoiceAmount());
		if(view.getComment()!=null)
			tacomment.setValue(view.getComment());
		if(view.getAccountType()!=null)
			tbaccounttype.setValue(view.getAccountType());
		
		if(view.getInvoiceCategory()!=null){
			AppMemory.setList(view.getInvoiceCategory(), olbInvoiceCategory);
			olbInvoiceCategory.setValue(view.getInvoiceCategory());
		}
		if(view.getInvoiceConfigType()!=null){
			olbInvoiceConfigType.setValue(view.getInvoiceConfigType());
		}
		if(view.getInvoiceGroup()!=null){
			olbinvoicegroup.setValue(view.getInvoiceGroup());
		}
		
		
		
		if(view.getConStartDur()!=null){
			conStartDur.setValue(view.getConStartDur());
		}
		
		if(view.getConEndDur()!=null){
			conEndDur.setValue(view.getConEndDur());
		}
		
		if(view.isMultipleOrderBilling()==true){
			
			multipleBillInvoice=true;
		}
		
		
		if(view.getRefNumber()!=null){
			referenceId.setValue(view.getRefNumber());
		}
		
		if(view.getCustomerBranch()!=null){
			/***14-12-2019 Deepak Salve added this line for set Customer branch in Drop down list***/
			 lbcustomerbranch.setValue(0,view.getCustomerBranch());
			 /***End***/
			makelivecustomerBranchList(view.getPersonInfo().getCount(),view.getCustomerBranch());
		}
		
		//    rohan added this code for invoice editing 
		
		System.out.println("product lis size before "+view.getSalesOrderProducts().size());
		
		salesProductTable.setValue(view.getSalesOrderProducts());
		
		System.out.println(" lis size after "+salesProductTable.getDataprovider().getList().size());
		
		
		/**
		 * Date : 21-09-2017 BY ANIL
		 */
		tblOtherCharges.setValue(view.getOtherCharges());
		dbOtherChargesTotal.setValue(view.getTotalOtherCharges());
		/**
		 * End
		 */
		
		
		
		
		invoiceTaxTable.setValue(view.getBillingTaxes());
		System.out.println(" lis size after 123  "+salesProductTable.getDataprovider().getList().size());
		
		
		
		invoiceChargesTable.setValue(view.getBillingOtherCharges());
		
		System.out.println(" lis size after 456  "+salesProductTable.getDataprovider().getList().size());
		
		if(view.getArrPayTerms()!=null)
		{
			ibpaytermsdays.setValue(view.getArrPayTerms().get(0).getPayTermDays());
			dopaytermspercent.setValue(view.getArrPayTerms().get(0).getPayTermPercent());
			tbpaytermscomment.setValue(view.getArrPayTerms().get(0).getPayTermComment());
		}

		/**
		 * @author Anil
		 * @since 06-01-2021
		 * reference field should show reference number from bill/contract
		 * we have sepratly stored proforma invoice number
		 * no need to override
		 * Raised by Vaishnavi
		 */
//		if(view.getProformaCount()!=null){
//			referenceId.setValue(view.getProformaCount()+"");
//		}
		
		if(view.getDiscount()!=0)
		{
			tbroundOffAmt.setValue(view.getDiscount()+"");
		}
		
		if(view.getNetPayable()!=0)
		{
			netPayable.setValue(view.getNetPayable());
		}
		
		
		if(view.getBillingPeroidFromDate()!=null){
			dbbillingperiodFromDate.setValue(view.getBillingPeroidFromDate());
		}
		if(view.getBillingPeroidToDate()!=null){
			dbbillingperiodToDate.setValue(view.getBillingPeroidToDate());
		}
		
		
		
		//  rohan added this code for setting tax invoice ref number in profarma
		
		if(view.getProformaCount()!=null)
		{
			proformaRefId.setValue(view.getProformaCount()+"");
		}
		
		//   changes ends here ******************
		
		
//		makelivecustomerBranchList(view.getPersonInfo().getCount());
//		
//		if(view.getCustomerBranch()!=null){
//			int count = lbcustomerbranch.getItemCount();
//			String item=view.getCustomerBranch().toString();
//			for(int i=0;i<count;i++)
//			{
//				if(item.trim().equals(lbcustomerbranch.getItemText(i).trim()))
//				{
//					lbcustomerbranch.setSelectedIndex(i);
//					break;
//				}
//			}
//		}
		/*
		 *  nidhi
		 *  1-07-2017
		 */
		
		if(view.getSegment()!=null){
			tbSegment.setValue(view.getSegment());
		}
		
		/*
		 *  end
		 */
		
		/*
		 *  nidhi
		 *  18-07-2017
		 */
		if(view.getRateContractServiceId()!=0){
			tbrateContractServiceid.setValue(view.getRateContractServiceId()+"");
		}
		
		/*
		 *  end
		 */
		
		if(view.getGstinNumber()!=null){
			tbGstinNumber.setValue(view.getGstinNumber());
		}
		/**
		 *  nidhi
		 *  20-07-2017
		 *    for save values
		 */
			if(view.getUom()!=null&&!view.getUom().trim().equals("")){
				olbUOM.setValue(view.getUom().trim());
			}
			if(view.getQuantity()!=0){
				quantity.setValue(view.getQuantity());
			}
		
			
			/** Date 04-09-2017 added by vijay for discount amount final total amount and round off ***/
			dbDiscountAmt.setValue(view.getDiscountAmt());
			dbFinalTotalAmt.setValue(view.getFinalTotalAmt());
			dbtotalAmt.setValue(view.getTotalAmtExcludingTax());
			dbtotalAmtIncludingTax.setValue(view.getTotalAmtIncludingTax());
			
			
			/**
			 * Date : 23-09-2017 BY ANIL 
			 */
			if(view.getInvoiceAmount()!=null){
				System.out.println("INVOICE AMOUNT "+view.getInvoiceAmount());
				doinvoiceamount.setValue(view.getInvoiceAmount());
			}
			/**
			 * ENd
			 */
			/** Date 06/2/2018 added by komal for consolidate price checkbox **/
			
			cbConsolidatePrice.setValue(view.isConsolidatePrice());
			/**
			 * end komal
			 */
			
			 /** Date 26-07-2018 By Vijay for Invoice Edited and changed the invoice date and net payable then Email for Pecopp Customization **/
			 oldinvoiceDate = view.getInvoiceDate();
			 oldNetPayable = view.getNetPayable();
			 
			/**
			 * nidhi
			 * 8-06-2018
			 */
			objPaymentMode.setValue(view.getPaymentMode());
			/** date 20.10.2018 added by komal **/
			if(view.getTypeOfOrder()!=null){
				tbTypeOfOrder.setValue(view.getTypeOfOrder());
			}
			if(view.getSubBillType()!=null){
				tbSubBillType.setValue(view.getSubBillType());
			}
			/** date 27.10.2018 added by komal for tds **/
			dbtdsValue.setValue(view.getTdsTaxValue());
			cbtdsApplicable.setValue(view.isTdsApplicable());
			if(view.getTdsPercentage()!=null && ! view.getTdsPercentage().equals("")){
				tbtdsPerValue.setValue(Double.parseDouble(view.getTdsPercentage()));
			}
			if(view.getPoNumber() != null){
				tbPONumber.setValue(view.getPoNumber());
			}
			if(view.getWoNumber() != null){
				tbWONumber.setValue(view.getWoNumber());
			}
			if(view.getContractNumber() != null){
				tbContractNumber.setValue(view.getContractNumber());
			}
			setBankSelectedDetails();
			if(view.getInvRefNumber()!=null){
				tbInvoiceRefNumber.setValue(view.getInvRefNumber());
			}
			
			
			/**
			 * nidhi
			 * 29-08-2018
			 */
			if(LoginPresenter.mapModelSerialNoFlag){

				if(view.getTypeOfOrder().equals(AppConstants.SALESORDER) || view.getTypeOfOrder().equals(AppConstants.PURCHASEORDER)){
					salesProductTable.setFlagforserialView(true);
				}else{
					salesProductTable.setFlagforserialView(false);
				}
				if(AppMemory.getAppMemory().currentState == ScreeenState.EDIT || AppMemory.getAppMemory().currentState  == ScreeenState.NEW){
					salesProductTable.setEnable(true);
				}else{
					salesProductTable.setEnable(false);
				}
			}
			
			if(view.getEwayBillNumber()!=null){
				tbEwayBillNumber.setValue(view.getEwayBillNumber());
			}
			
			if(view.getProjectName()!=null){
				tbprojectName.setValue(view.getProjectName());
			}
			
			ibcreditPeriod.setValue(view.getCreditPeriod());
			
			if(view.getDispatchDate()!=null){
				dbDispatchDate.setValue(view.getDispatchDate());
			}
//			if(view.getInvPrefix()!=null){
//				tbInvPrefix.setValue(view.getInvPrefix());
//			}

			if(view.getComment1()!=null){
				taComment1.setValue(view.getComment1());
			}
			
			if(view.getDocUpload1()!=null) {
				upload1.setValue(view.getDocUpload1());
			}
			if(view.getDocUpload2()!=null) {
				upload2.setValue(view.getDocUpload2());
			}
			
			if(view.getDeclaration()!=null){
				taDeclaration.setValue(view.getDeclaration());
			}
			
			cbdonotprintserviceAddress.setValue(view.isDonotprintServiceAddress());

			if(view.getPaymentTerms()!=null){
				lbPaymentTerms.setValue(0, view.getPaymentTerms());
			}
			
			if(view.getTransactionLineType()!=null){
				lbTransactionLineType.setValue(0, view.getTransactionLineType());
			}
			
			if(view.getTaxExemptionCode()!=null){
				lbTaxExemptionFlag.setValue(0, view.getTaxExemptionCode());
			}
			
			if(view.getTaxClassificationCode()!=null){
				olbTaxClassificationCode.setValue(view.getTaxClassificationCode());
			}
			
			if(view.getVendorName()!=null){
				obVendor.setValue(view.getVendorName());
			}
			
			//14-09-2022
			if(view.getCreatedBy()!=null){
				tbCreatedBy.setValue(view.getCreatedBy());
			}
			if(view.getCreationDate()!=null){
				tbCreatedOn.setValue(view.getCreationDate().toString());
			}
			if(view.getApproverName()!=null){
				tbApprovedBy.setValue(view.getApproverName());
			}
			if(view.getApprovalDate()!=null){
				tbApprovedOn.setValue(view.getApprovalDate().toString());
			}
			if(view.getCancellationDate()!=null){
				tbCancelledOn.setValue(view.getCancellationDate().toString());
			}
			if(view.getCancelledBy()!=null){
				tbCancelledBy.setValue(view.getCancelledBy());
			}
			if(view.getUpdatedBy()!=null){
				tbUpdatedBy.setValue(view.getUpdatedBy());
			}
			if(view.getUpdationDate()!=null){
				tbUpdatedOn.setValue(view.getUpdationDate().toString());
			}
			if(view.getLog()!=null){
				tbLog.setValue(view.getLog());
			}
			if(view.getIRN()!=null){
				eInvoiceNo.setValue(view.getIRN());
			}
			if(view.getIrnAckNo()!=null){
				ackNo.setValue(view.getIrnAckNo());
			}
			if(view.getIrnAckDate()!=null){
				ackDate.setValue(view.getIrnAckDate());
			}
			if(view.getIrnQrCode()!=null){
				qrCode.setValue(view.getIrnQrCode());
			}
			if(view.getIrnStatus()!=null){
				eInvoiceStatus.setValue(view.getIrnStatus());
			}
			if(view.getIrnCancellationDate()!=null){
				eInvoiceCancellationDate.setValue(view.getIrnCancellationDate());
			}
			
			if(view.getInvoicePdfNameToPrint()!=null && !view.getInvoicePdfNameToPrint().equals("")){
				invoicepdfNametoprint.setValue(view.getInvoicePdfNameToPrint());
			}
			
			if(view.getZohoInvoiceID()!=null)
				tbZohoInvoiceId.setValue(view.getZohoInvoiceID());
			
			if(view.getZohoInvoiceUniqueID()!=0)
				tbZohoInvoiceUniqueID.setValue(view.getZohoInvoiceUniqueID()+"");
			
			if(view.getZohoSyncDate()!=null)
				tbZohoSyncDate.setValue(view.getZohoSyncDate().toString());
			
			if(view.getZohoSyncDetails()!=null)
				tbZohoSyncInfo.setValue(view.getZohoSyncDetails());
	
		/* 
		 * for approval process
		 *  nidhi
		 *  5-07-2017
		 */
		if(presenter != null){
			presenter.setModel(view);
		}
		/*
		 *  end
		 */
				
		
		
	}

	
	void valideCofigInfoDetails(final Invoice view){
		boolean flagCate = false;
		
	for(ConfigCategory congi : LoginPresenter.globalCategory){
		if(congi.getCategoryName().equalsIgnoreCase(view.getInvoiceCategory()) && congi.getInternalType() == 24){
			flagCate = true;
			break;
		}
	}
	
	if (!flagCate) {
		String configType=AppConstants.GLOBALRETRIEVALCATEGORY+"-"+view.getCompanyId();
		AppUtility.globalMakeLiveCategory(configType);
	}
	
	 flagCate = false;
	 
	for(Type congi : LoginPresenter.globalType){
		if(congi.getCategoryName().equalsIgnoreCase(view.getInvoiceCategory()) && congi.getTypeName().equalsIgnoreCase(view.getInvoiceConfigType()) && congi.getInternalType() == 24){
			flagCate = true;
			break;
		}
	}
	
	if (!flagCate) {
		String configType=AppConstants.GLOBALRETRIEVALTYPE+"-"+view.getCompanyId();
		AppUtility.globalMakeLiveType(configType);
	}
	
	 flagCate = false;
	 
		for(Config congi : LoginPresenter.globalConfig){
			if(congi.getName().equalsIgnoreCase(view.getInvoiceGroup()) && congi.getType() == 77){
				flagCate = true;
				break;
			}
		}
		
		if (!flagCate) {
			String configType=AppConstants.GLOBALRETRIEVALCONFIG+"-"+view.getCompanyId();
			AppUtility.globalMakeLiveConfig(configType);
		}
		
		olbInvoiceCategory.removeAllItems();
		olbInvoiceConfigType.removeAllItems();
		olbinvoicegroup.removeAllItems();
		AppUtility.MakeLiveCategoryConfig(olbInvoiceCategory, Screen.INVOICECATEGORY);
		AppUtility.makeTypeListBoxLive(olbInvoiceConfigType, Screen.INVOICECONFIGTYPE);
		AppUtility.MakeLiveConfig(olbinvoicegroup, Screen.INVOICEGROUP);
		
		Timer timer=new Timer() 
	   	 {
			@Override
			public void run() 
				{
				if(view.getInvoiceCategory()!=null)
					olbInvoiceCategory.setValue(view.getInvoiceCategory());
				if(view.getInvoiceConfigType()!=null)
					olbInvoiceConfigType.setValue(view.getInvoiceConfigType());
				if(view.getInvoiceGroup()!=null)
					olbinvoicegroup.setValue(view.getInvoiceGroup());
				
				}
	   	 };
	   	timer.schedule(5000); 
	
}
	
	@Override
	public void setCount(int count)
	{
		ibinvoiceid.setValue(count+"");
	}
	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		
		/**
		 * @author Anil,Date:17-04-2019
		 * renaming default column no of branch to Qty and hiding column area for order type sales and purchase
		 */
		if(invoiceobj!=null){
			
			boolean addDeleteProductInInvoiceFlag = AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", AppConstants.PC_ENABLEADDDELETESALESORDERPRODUCTSONINVOICE);

			if(invoiceobj!=null && addDeleteProductInInvoiceFlag && invoiceobj.getTypeOfOrder().equals(AppConstants.ORDERTYPESALES)) {
				prodInfoComposite.setEnable(state);
				baddproducts.setEnabled(state);
			}
			else{
				prodInfoComposite.setEnable(false);
				baddproducts.setEnabled(false);
			}
			
			String orderType=invoiceobj.getTypeOfOrder();
			System.out.println("Order type : "+orderType);
			salesProductTable.orderType=orderType;
			if(invoiceobj.getTypeOfOrder().equalsIgnoreCase(AppConstants.ORDERTYPEPURCHASE) || invoiceobj.getTypeOfOrder().equalsIgnoreCase(AppConstants.ORDERTYPESALES)){
				salesProductTable.qtyHeader="Qty";
			}else{
				salesProductTable.qtyHeader=null;
				
				/**
				 * @author Anil @since 27-03-2021
				 * Showing bill type column if bill type is one of below
				 */
				
				if(invoiceobj.getSubBillType()!=null&&!invoiceobj.getSubBillType().equals("")){
					if(invoiceobj.getSubBillType().equals("Public Holiday")
							||invoiceobj.getSubBillType().equals("National Holiday")
							||invoiceobj.getSubBillType().equals("Overtime")
							||invoiceobj.getSubBillType().equals("Holiday")
							||invoiceobj.getSubBillType().equals("Staffing")
							||invoiceobj.getSubBillType().equals("Staffing And Rental")
							||invoiceobj.getSubBillType().equals("Labour")
							||invoiceobj.getSubBillType().equals("Consolidate Bill")){
						salesProductTable.showBillType=true;
					}
				}
				
			}
			
			Console.log("Inside setenable method");
			
			
			
		}
		/**
		 * End
		 */
		
		ibinvoiceid.setEnabled(false);
		ibbillingid.setEnabled(false);
		tbstatus.setEnabled(false);
		ibcontractid.setEnabled(false);
//		tbpersoncount.setEnabled(false);
//		tbpersonname.setEnabled(false);
//		tbpersoncell.setEnabled(false);
		dbbillingdate.setEnabled(false);
		dosalesamount.setEnabled(false);
		dototalbillamt.setEnabled(false);
		doinvoiceamount.setEnabled(false);
		tbinvoiceType.setEnabled(false);
		tbaccounttype.setEnabled(false);
		tbremark.setEnabled(false);
//		tbpocname.setEnabled(false);
		proformaRefId.setEnabled(false);
		netPayable.setEnabled(false);
		
		
		ibpaytermsdays.setEnabled(state);
		dopaytermspercent.setEnabled(state);
		tbpaytermscomment.setEnabled(state);
	 
		System.out.println("Rohan in side set enable"+state);
		/**
		 * nidhi 17-12-2018  for stop partical Invoice
		 */
		boolean salesBillDisable = AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "NonEditableInvoicePrice");
		salesProductTable.salesBillDisable = salesBillDisable;
		
		salesProductTable.setEnable(state);
		
//		if(multipleBillInvoice==true)
//		{
			conStartDur.setEnabled(state);
			conEndDur.setEnabled(state);
//		}
		
		dbbillingperiodFromDate.setEnabled(state);
		dbbillingperiodToDate.setEnabled(state);
		
		this.btReferenceDetails.setEnabled(true);
		
		/**
		 * Date : 11-07-2017 By NIDHI
		 */
		
		tbSegment.setEnabled(false);
		/**
		 * End
		 */
		

		/*
		 *  nidhi
		 *  20-07-2017
		 *   set display status
		 */
		olbUOM.setEnabled(state);
		quantity.setEnabled(state);
		/*
		 *  end
		 */
		
		
		tbrateContractServiceid.setEnabled(false);
		tbGstinNumber.setEnabled(state);
		
		//Date 04-09-2017 added by vijay for total amount discount and roundoff
		this.dbtotalAmt.setEnabled(false);
		this.dbDiscountAmt.setEnabled(state);
		this.dbFinalTotalAmt.setEnabled(false);
		this.dbtotalAmtIncludingTax.setEnabled(false);
		
		/**
		 * Date : 21-09-2017 BY ANIL
		 */
		tblOtherCharges.setEnable(state);
		dbOtherChargesTotal.setEnabled(false);
		/**
		 * End
		 */
		/**
		 * nidhi
		 * 8-06-2018
		 * 
		 */
		objPaymentMode.setEnabled(state);
		tbPaymentName.setEnabled(false);
		tbPaymentBankAC.setEnabled(false);
		tbPaymentPayableAt.setEnabled(false);
		tbPaymentAccountName.setEnabled(false);
		/**
		 * end
		 */
		
		/**
		 * Date 06-07-2018
		 * Developer :- Vijay
		 * Des :- This is for NBHC Invoice Date should not be editable for Branch user and 
		 * Admin user can able to edit billing date
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "OnlyForNBHC")){
			if(!(LoginPresenter.myUserEntity.getRole().getRoleName().trim().equalsIgnoreCase("ADMIN"))){
				dbinvoicedate.setEnabled(false);
			}else{
				dbinvoicedate.setEnabled(state);
			}
		}
		/**
		 * ends here
		 */
		/** date 20.10.2018 added by komal **/
		tbTypeOfOrder.setEnabled(false);
		tbSubBillType.setEnabled(false);
		/** date 24.10.2018 added by komal for tds **/
		tbtdsPerValue.setEnabled(false);
		dbtdsValue.setEnabled(false);
		if(state && cbtdsApplicable.getValue()){
			tbtdsPerValue.setEnabled(state);
			dbtdsValue.setEnabled(state);

		}

		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "AddProjectName")){
			tbprojectName.setEnabled(state);
		}
		
		ibcreditPeriod.setEnabled(state);
		dbDispatchDate.setEnabled(state);
		//tbInvPrefix.setEnabled(false);
		if(invPrefFlag){
			tbInvoiceRefNumber.setEnabled(false);
		}
		taComment1.setEnabled(state);
		/**
		 * @author Vijay Date :- 19-11-2020
		 * Des :- if below process config is active then invoice category is non ediatble. category will map from Billing document
		 */

		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", AppConstants.PC_DISABLEINVOICECATEGORY)){

			olbInvoiceCategory.setEnabled(false);

		}
		
		upload1.setEnable(state);
		upload2.setEnable(state);
		
		invoiceTaxTable.setEnable(state);
		
		invoicepdfNametoprint.setEnabled(state);
		
		
		
	}

	
	@Override
	public boolean validate() {
		Console.log("in invoice validate");
		boolean superRes=super.validate();
		boolean invDateVal=true,invgroup=true,durDt=true,taxValidate=true;
		
		Date invDate=dbinvoicedate.getValue();
		Date payDate=dbpaymentdate.getValue();
		HashSet<String> taxesAtProduct=new HashSet<String>();
		HashSet<String> taxesAtTaxTable=new HashSet<String>();
		
		
		/**
		 * Rohan added this code Only for NBHC 
		 * Date : 17-04-2017
		 * This is used to validate invoice date is not after todays date 
		 */
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "OnlyForNBHC")){
			Date todaysDate = new Date();
				if(invDate.after(todaysDate)){
					showDialogMessage("Invoice Date should not be greater than todays date..!");
					invDateVal=false;	
				}
		}
		
		/**
		 * ends here 
		 */
		/**Added by sheetal:20-01-2022
		   Des:Commented below code for removing validation Payment Date should be 
		   greater than Invoice Date, requirement raised by nitin sir**/		
		
		
//		if(invDate!=null&&payDate!=null)
//		{
//			if(invDate.after(payDate))
//			{
//				showDialogMessage("Payment Date should be greater than Invoice Date!");
//				invDateVal=false;
//			}
//		}
		/**end**/
		System.out.println("Validation");
			if(checkForInvGroup()==false&&this.olbinvoicegroup.getSelectedIndex()==0)
			{
				showDialogMessage("Invoice Group is Mandatory!");
				invgroup=false;
			}
		
			
		/*
		 * nidhi
		 * 21-06-2018
		 * if(multipleBillInvoice==true && conStartDur.getValue()==null && conEndDur.getValue()==null)
		{
			showDialogMessage("Contract Duration Start Date & Contract Duration Start Date is Mandatory !");
			
			*//**
			 * nidhi
			 * 10-04-2018
			 * for proper alert message
			 *//*
			showDialogMessage("Contract Duration Start Date & Contract Duration End Date is Mandatory !");
			durDt=false;
		}*/
		
		
		 /**
		 * Date 03-07-2017 added by vijay tax validation
		 */
		List<ContractCharges> prodtaxlist = this.invoiceTaxTable.getDataprovider().getList();
		for(int i=0;i<prodtaxlist.size();i++){
			
			if(prodtaxlist.get(i).getTaxChargeName()!=null&&!prodtaxlist.get(i).getTaxChargeName().equalsIgnoreCase("NA")) {
				taxesAtTaxTable.add(prodtaxlist.get(i).getTaxChargeName().toUpperCase());
			}
			if(dbinvoicedate.getValue()!=null && dbinvoicedate.getValue().before(date)){
				if(prodtaxlist.get(i).getTaxChargeName().equalsIgnoreCase("CGST") || prodtaxlist.get(i).getTaxChargeName().equalsIgnoreCase("SGST")
						|| prodtaxlist.get(i).getTaxChargeName().equalsIgnoreCase("IGST")){
					showDialogMessage("As Invoice date "+fmt.format(dbinvoicedate.getValue())+" GST Tax is not applicable");
					taxValidate = false;
				}
			}
			/**
			 * @author Anil @since 21-07-2021
			 * Issue raised by Rahul for Innovative Pest(Thailand) 
			 * Vat is applicable there so system should not through validation for VAT
			 */
//			else if(dbinvoicedate.getValue()!=null && dbinvoicedate.getValue().equals(date) || dbinvoicedate.getValue().after(date)){
//				if(prodtaxlist.get(i).getTaxChargeName().equalsIgnoreCase("CST") || prodtaxlist.get(i).getTaxChargeName().equalsIgnoreCase("Service Tax")
//						|| prodtaxlist.get(i).getTaxChargeName().equalsIgnoreCase("VAT")){
//					showDialogMessage("As Invoice date "+fmt.format(dbinvoicedate.getValue())+" VAT/CST/Service Tax is not applicable");
//					taxValidate = false;
//				}
//			}
		}
		/**
		 * ends here
		 */
		/**
		 *  nidhi
		 *  30-03-2018
		 *  for make unit and UOm is mandatory
		 */
		boolean unitFlag = validateunitandUOM();
		/**
		 * end
		 */
		/** date 15.4.2019 added by komal for nbhc service branches button and popup **/
		/***19-12-2019 Deepak Salve added this code for Customer Branch Disable for Sales Invoice***/ 
		Invoice currentObj = (Invoice) this.getPresenter().getModel();
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "EnableServiceBranch") && (currentObj!=null && currentObj.getTypeOfOrder().equals(AppConstants.ORDERTYPEFORSERVICE))){
		 if(!LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN")){
			NumberFormat nf=NumberFormat.getFormat("0.00");
			for(SalesOrderProductLineItem item:salesProductTable.getDataprovider().getList()){
				double totalAmount = 0;
				if(item.getServiceBranchesInfo()!=null && item.getServiceBranchesInfo().size()>0){
				ArrayList<BranchWiseScheduling> list = item.getServiceBranchesInfo().get(item.getProductSrNumber());
				if(list != null && list.size() > 0){
				 for(BranchWiseScheduling branchWiseScheduling : list){
					if(branchWiseScheduling.isCheck()){
						totalAmount += branchWiseScheduling.getArea();
					}
				 }
				}
//				if(totalAmount !=  item.getBasePaymentAmount()){
//					showDialogMessage("Service branches total amount should be equal to product payable amount.");
//					return false;
//				}
			 }
				if(totalAmount !=  Double.parseDouble(nf.format(item.getBasePaymentAmount()))){
					showDialogMessage("Service branches total amount should be equal to product payable amount.");
					return false;
				}
			}
		   }
		}
		/**
		 * end komal
		 */
		for(SalesOrderProductLineItem item:salesProductTable.getDataprovider().getList()){
			if(item.getServiceTax().getTaxPrintName()!=null&&!item.getServiceTax().getTaxPrintName().equalsIgnoreCase("NA")) {
				taxesAtProduct.add(item.getServiceTax().getTaxPrintName().toUpperCase());
				Console.log("getTaxName="+item.getServiceTax().getTaxName()+" print name="+item.getServiceTax().getTaxPrintName());
			}
			if(item.getVatTax().getTaxPrintName()!=null&&!item.getVatTax().getTaxPrintName().equalsIgnoreCase("NA")) {
				taxesAtProduct.add(item.getVatTax().getTaxPrintName().toUpperCase());
			}
		}
		if(taxesAtProduct.size()>0&&taxesAtTaxTable.size()>0) {
			Console.log("in if taxesAtProduct="+taxesAtProduct);
			Console.log("in if taxesAtTaxTable="+taxesAtTaxTable);
			
			if(!taxesAtProduct.containsAll(taxesAtTaxTable)||!taxesAtTaxTable.containsAll(taxesAtProduct)) {
				showDialogMessage("Taxes at product table and taxes at taxtable above the net payable are not matching");
				return false;
			}
		}else if(taxesAtProduct.size()>0||taxesAtTaxTable.size()>0) {
			Console.log("in else if");
			showDialogMessage("Taxes at product table and taxes at taxtable above the net payable are not matching");
			return false;
		}
		
		
		return superRes&&invDateVal&&invgroup&&durDt&&taxValidate&&unitFlag;
		
	}
	
	
	public boolean checkForInvGroup()
	{
		System.out.println("Data Validation");
		List<ProcessConfiguration> processLis=LoginPresenter.globalProcessConfig;
		System.out.println("Process Lis"+processLis.size());
		List<ProcessTypeDetails> processDetailsLis=new ArrayList<ProcessTypeDetails>();
		boolean flagForInvoice=true;
		for(int i=0;i<processLis.size();i++)
		{
			if(processLis.get(i).getProcessName().equals("Invoice"))
			{
				processDetailsLis=processLis.get(i).getProcessList();
			}
		}
		
		for(int d=0;d<processDetailsLis.size();d++)
		{
			if(processDetailsLis.get(d).getProcessType().trim().equalsIgnoreCase("IsInvoiceGroupMandatory")&&processDetailsLis.get(d).isStatus()==true)
			{
				flagForInvoice=false;
			}
		}
		
		return flagForInvoice;
	}

	public void setAppHeaderBarAsPerStatus()
	{
		this.toggleHeaderBar();
		this.changeProcessLevel();
	}

	public void toggleHeaderBar(){
		Invoice entity=(Invoice) presenter.getModel();
		String status=entity.getStatus();
		/**
		 * @author Anil @since 04-08-2021
		 * requirement raised by rutuza for Ultra 
		 * invoice should be editable for proforma invoice 
		 */
		if(status.equals(Invoice.APPROVED)||status.equals(Invoice.PROFORMAINVOICE)){
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			/**
			 * Date 30 july 2017 if condtion added by vijay for GENERAL POPUP menus 
			 * and old code added to else block for normal save menus
			 */
			System.out.println("APPROVED");
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			}
			for(int k=0;k<menus.length;k++){
				String text=menus[k].getText();
				System.out.println("STATUS APP MENU== =="+text);
				/**
				 * Date : 20-11-2017 By ANIL
				 * Providing invoice update option for Admin after approval of invoice 
				 */
				
				/**
				 * Date : 22-07-2021 By Priyanka
				 * Providing invoice update option for Zonal Coordinator after approval of invoice 
				 */
				if(UserConfiguration.getRole().getRoleName().equals("ADMIN")||UserConfiguration.getRole().getRoleName().equals("Zonal Coordinator")||AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "EnableInvoiceEditableForAllRoles")  ){
				
					/**
					 * Date 26-07-2018 By Vijay
					 * Des :- Providing Invoice Editing for All Roles with Process Configuration for Pecopp customization. 
					 * other than admin can edit if invoice date within current month only
					 */
					Date firstDateOfMonth = new Date();
					CalendarUtil.setToFirstDayOfMonth(firstDateOfMonth);
					Date lastDateofMonth = new Date();
					CalendarUtil.addMonthsToDate(lastDateofMonth, 1);
					CalendarUtil.setToFirstDayOfMonth(lastDateofMonth);
					CalendarUtil.addDaysToDate(lastDateofMonth, -1);
					
					if(UserConfiguration.getRole().getRoleName().equals("ADMIN") || (dbinvoicedate.getValue().after(firstDateOfMonth) && dbinvoicedate.getValue().before(lastDateofMonth) || dbinvoicedate.getValue().equals(firstDateOfMonth)||dbinvoicedate.getValue().equals(lastDateofMonth) )){
						if(text.contains("Discard")||text.contains("Search")||text.contains("Print")||text.contains("Email")||text.contains(AppConstants.NAVIGATION)||text.contains("Edit") || text.contains(AppConstants.MESSAGE)){
							menus[k].setVisible(true); 
						}else{
							menus[k].setVisible(false);
						}
					}else if(UserConfiguration.getRole().getRoleName().equals("Zonal Coordinator") || (dbinvoicedate.getValue().after(firstDateOfMonth) && dbinvoicedate.getValue().before(lastDateofMonth) || dbinvoicedate.getValue().equals(firstDateOfMonth)||dbinvoicedate.getValue().equals(lastDateofMonth) )){
						if(text.contains("Discard")||text.contains("Search")||text.contains("Print")||text.contains("Email")||text.contains(AppConstants.NAVIGATION)||text.contains("Edit") || text.contains(AppConstants.MESSAGE)){
							menus[k].setVisible(true); 
						}else{
							menus[k].setVisible(false);
						}
					}else{
						if(text.contains("Discard")||text.contains("Search")||text.contains("Print")||text.contains("Email")||text.contains(AppConstants.NAVIGATION) || text.contains(AppConstants.MESSAGE)){
							menus[k].setVisible(true); 
						}else{
							menus[k].setVisible(false);
						}
					}

					
				}else{
					Console.log("process config else block");

					if(text.contains("Discard")||text.contains("Search")||text.contains("Print")||text.contains("Email")||text.contains(AppConstants.NAVIGATION) || text.contains(AppConstants.MESSAGE)){
						menus[k].setVisible(true); 
					}else{
						menus[k].setVisible(false);
					}
				}
				
				/**
				 * Created By : Nidhi
				 * Date - 20-11-2017
				 * Hide Email Button As Process Configration
				 * 
				 */
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "HideEmailButton") && text.contains("Email")){
					menus[k].setVisible(false); 
				}
				/**
				 * end
				 */
			}
			
			/**
			 * Date : 21-11-2017 BY ANIL
			 * Providing edit option after approval of document
			 * Date :- 26-07-2018 By Vijay Providing Invoice Editing for All Roles with Process Configuration for Pecopp customization
			 */	
			if(UserConfiguration.getRole().getRoleName().equals("ADMIN")||UserConfiguration.getRole().getRoleName().equals("Zonal Coordinator") || AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "EnableInvoiceEditableForAllRoles")){
				if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT){
					for(int k=0;k<menus.length;k++){
						String text=menus[k].getText();
						if(text.contains("Discard")||text.contains("Save") || text.contains(AppConstants.MESSAGE)){
							menus[k].setVisible(true); 
						}else{
							menus[k].setVisible(false);
						}
					}
				}
			}
			
		}
		
		
		if(status.equals(Invoice.REQUESTED))
		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			System.out.println("REQUESTED");
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();

			}
			
			
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Discard")||text.contains("Search")||text.contains("Print")||text.contains(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
			}
		}
		
		if(status.equals(Invoice.CANCELLED))
		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			System.out.println("CANCELLED");

			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();

			}
			
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Discard")||text.contains("Search")||text.contains("Print")||text.contains(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
			}
		}
		
		if(status.equals(Invoice.PAYMENTCLOSED))
		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			
			System.out.println("PAYMENTCLOSED");

			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();

			}
			
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Discard")||text.contains("Search")||text.contains(AppConstants.NAVIGATION) || text.contains(AppConstants.MESSAGE))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
			}
		}
		
		
		
		// rohan added this code 
		System.out.println("Rohan In side methods"+status+"="+BillingDocument.BILLINGINVOICED);
		if(status.equals(BillingDocument.BILLINGINVOICED))
		{
			System.out.println("Rohan inside xyz condition");
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();

			}
			
			Date firstDateOfMonth = new Date();
			CalendarUtil.setToFirstDayOfMonth(firstDateOfMonth);
			Date lastDateofMonth = new Date();
			CalendarUtil.addMonthsToDate(lastDateofMonth, 1);
			CalendarUtil.setToFirstDayOfMonth(lastDateofMonth);
			CalendarUtil.addDaysToDate(lastDateofMonth, -1);
			
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(UserConfiguration.getRole().getRoleName().equals("ADMIN")||UserConfiguration.getRole().getRoleName().equals("Zonal Coordinator")||AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "EnableInvoiceEditableForAllRoles")  ){
					
					if(UserConfiguration.getRole().getRoleName().equals("ADMIN") || (dbinvoicedate.getValue().after(firstDateOfMonth) && dbinvoicedate.getValue().before(lastDateofMonth) || dbinvoicedate.getValue().equals(firstDateOfMonth)||dbinvoicedate.getValue().equals(lastDateofMonth) )){
						if(text.contains("Discard")||text.contains("Search")||text.contains("Print")||text.contains("Email")||text.contains(AppConstants.NAVIGATION)||text.contains("Edit") || text.contains(AppConstants.MESSAGE)){
							menus[k].setVisible(true); 
						}else{
							menus[k].setVisible(false);
						}
					}
				}
				else {
					if(text.contains("Discard")||text.contains("Email")||text.contains("Search")||text.contains("Print")||text.contains(AppConstants.NAVIGATION) || text.contains(AppConstants.MESSAGE))
					{
						menus[k].setVisible(true); 
					}
					else
						menus[k].setVisible(false);  		
				}
					
					
				   
			}
		}
		
		
		         /**
				 * Date : 22-07-2021 By Priyanka
				 * Providing Email option when invoice status is Pinvoice - Proforma Invoice 
				 */
		
		
		if(status.equals(BillingDocument.PROFORMAINVOICE))
		{
			System.out.println("Rohan inside xyz condition");
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();

			}
			
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				/**
				 * @author Vijay Date :- 26-09-2021
				 * Des :- updated code invoice edit button for Proforma invoice
				 */
				if(UserConfiguration.getRole().getRoleName().equals("ADMIN")||UserConfiguration.getRole().getRoleName().equals("Zonal Coordinator")||AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "EnableInvoiceEditableForAllRoles")  ){
					if(text.contains("Discard")||text.contains("Email")||text.contains("Search")||text.contains("Print")||text.contains(AppConstants.NAVIGATION) || text.contains(AppConstants.MESSAGE) || text.contains("Edit"))
					{
						menus[k].setVisible(true); 
					}
					else
						menus[k].setVisible(false);  
				}
				else {
					if(text.contains("Discard")||text.contains("Email")||text.contains("Search")||text.contains("Print")||text.contains(AppConstants.NAVIGATION) || text.contains(AppConstants.MESSAGE))
					{
						menus[k].setVisible(true); 
					}
					else
						menus[k].setVisible(false);  
				}

						   
			}
		}
	}
	
	public void changeProcessLevel()
	{
		Invoice entity=(Invoice) presenter.getModel();
		String status=entity.getStatus();
		boolean renewFlagStatus=entity.isRenewFlag();
		
		Console.log("in changeProcessLevel of invoice"); 
		
		boolean salesorderInvoiceUpdateFlag = false;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", AppConstants.PC_UPDATESALESORDERINVOICE)){
			salesorderInvoiceUpdateFlag = true;
		}
		
		for(int i=0;i<getProcesslevelBarNames().length;i++)
		{
			InlineLabel label=getProcessLevelBar().btnLabels[i];
			String text=label.getText().trim();
				
			if(status.equals(Invoice.CREATED))
			{
				
				/**
				 * Date : 23-05-2017 By ANIL
				 * Checking self approval
				 */
				
				System.out.println("Hi vijay === Self approvals"+getManageapproval().isSelfApproval());
				
				if(getManageapproval().isSelfApproval()){
					
					if(text.equals(ManageApprovals.SUBMIT))
						label.setVisible(true);
					if(text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
						label.setVisible(false);
					if(text.equals(AppConstants.CANCELINVOICE))
						label.setVisible(true);
					if(text.equals("Tax Invoice"))
						label.setVisible(false);
					if(text.equals(ManageApprovals.APPROVALREQUEST))
						label.setVisible(false);
				}else{
					
					if(text.equals(ManageApprovals.APPROVALREQUEST))
						label.setVisible(true);
					if(text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
						label.setVisible(false);
					if(text.equals(AppConstants.CANCELINVOICE))
						label.setVisible(true);
					if(text.equals("Tax Invoice"))
						label.setVisible(false);
					if(text.equals(ManageApprovals.SUBMIT))
						label.setVisible(false);
				}
				
				/**
				 * Date : 29-07-2017 By ANIL
				 */
				if(text.equals(AppConstants.VIEWPAYMENT)){
					label.setVisible(false);
				}
				/**
				 * End
				 */
				/**
				 * Date : 10-10-2017 By ANIL
				 */
				if(text.equals(AppConstants.VIEWBILL)){
					label.setVisible(true);
				}
				/**
				 * End
				 */
				/**@Sheetal : 15-02-2022**/
				if(text.equals(AppConstants.VIEWORDER)){
					label.setVisible(true);
				}
				/**
				 * Date : 02-03-2018 By vijay
				 */
				if(text.equals(AppConstants.VIEWTAXINVOICE)){
					label.setVisible(false);
				}
				/**
				 * ends here
				 */
				/** Date 13.12.2018 added by komal for update accounting interface **/
				if(text.equalsIgnoreCase(AppConstants.UPDATEACCOUNTINGINTERFACE)){
					label.setVisible(false);
				}
				
				if(text.equalsIgnoreCase("Attendance Summary")){
					if(enableSiteLocation){
						label.setVisible(true);
					}else{
						label.setVisible(false);
					}
				}
				
				if(text.equalsIgnoreCase("Sales Order Summary")){
					if(uploadConsumableFlag){
						label.setVisible(true);
					}else{
						label.setVisible(false);
					}
				}
				
				if(text.equalsIgnoreCase("Invoice Reset")){			
						label.setVisible(false);					
				}
				if(text.equalsIgnoreCase("Generate e-Invoice number")){			
					label.setVisible(false);					
				}
				
				if(text.equalsIgnoreCase(AppConstants.UPDATESALESORDERINVOICE)){			
					if(LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN") && salesorderInvoiceUpdateFlag){
						label.setVisible(true);					
					}
					else{
						label.setVisible(false);					
					}
				}
				
				if(text.equalsIgnoreCase(AppConstants.generatePayment)){			
					label.setVisible(false);					
				}
				if(text.equalsIgnoreCase(AppConstants.syncWithZohoBooks)){			
					label.setVisible(false);					
				}
				
			}
			if(status.equals("Requested"))
			{
				if(text.equals(ManageApprovals.APPROVALREQUEST))
					label.setVisible(false);
				if(text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
					label.setVisible(true);
				if(text.equals(AppConstants.CANCELINVOICE))
					label.setVisible(false);
				if(text.equals("Tax Invoice"))
					label.setVisible(false);
				if(text.equals(ManageApprovals.SUBMIT))
					label.setVisible(false);
				/**
				 * Date : 29-07-2017 By ANIL
				 */
				if(text.equals(AppConstants.VIEWPAYMENT)){
					label.setVisible(false);
				}
				/**
				 * End
				 */
				/**
				 * Date : 10-10-2017 By ANIL
				 */
				if(text.equals(AppConstants.VIEWBILL)){
					label.setVisible(true);
				}
				/**
				 * End
				 */
				/**@Sheetal : 15-02-2022**/
				if(text.equals(AppConstants.VIEWORDER)){
					label.setVisible(true);
				}
				/**
				 * Date : 02-03-2018 By vijay
				 */
				if(text.equals(AppConstants.VIEWTAXINVOICE)){
					label.setVisible(false);
				}
				/**
				 * ends here
				 */
				/** Date 13.12.2018 added by komal for update accounting interface **/
				if(text.equalsIgnoreCase(AppConstants.UPDATEACCOUNTINGINTERFACE)){
					label.setVisible(false);
				}
				
				if(text.equalsIgnoreCase("Attendance Summary")){
					label.setVisible(false);
				}
				
				if(text.equalsIgnoreCase("Invoice Reset")){			
					label.setVisible(false);					
				}
				if(text.equalsIgnoreCase("Generate e-Invoice number")){			
					label.setVisible(false);					
				}
				
				if(text.equalsIgnoreCase(AppConstants.UPDATESALESORDERINVOICE)){			
						label.setVisible(false);					
				}
				if(text.equalsIgnoreCase(AppConstants.generatePayment)){			
					label.setVisible(false);					
				}
				if(text.equalsIgnoreCase(AppConstants.syncWithZohoBooks)){			
					label.setVisible(false);					
				}
			}
			
			if(status.equals("Rejected"))
			{
				if(text.equals(ManageApprovals.APPROVALREQUEST))
					label.setVisible(true);
				if(text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
					label.setVisible(false);
				if(text.equals(AppConstants.CANCELINVOICE))
					label.setVisible(false);
				if(text.equals("Tax Invoice"))
					label.setVisible(false);
				if(text.equals(ManageApprovals.SUBMIT))
					label.setVisible(false);
				/**
				 * Date : 29-07-2017 By ANIL
				 */
				if(text.equals(AppConstants.VIEWPAYMENT)){
					label.setVisible(false);
				}
				/**
				 * End
				 */
				/**
				 * Date : 10-10-2017 By ANIL
				 */
				if(text.equals(AppConstants.VIEWBILL)){
					label.setVisible(true);
				}
				/**
				 * End
				 */
				/**@Sheetal : 15-02-2022**/
				if(text.equals(AppConstants.VIEWORDER)){
					label.setVisible(true);
				}
				/**
				 * Date : 02-03-2018 By vijay
				 */
				if(text.equals(AppConstants.VIEWTAXINVOICE)){
					label.setVisible(false);
				}
				/**
				 * ends here
				 */
				/** Date 13.12.2018 added by komal for update accounting interface **/
				if(text.equalsIgnoreCase(AppConstants.UPDATEACCOUNTINGINTERFACE)){
					label.setVisible(false);
				}
				if(text.equalsIgnoreCase("Attendance Summary")){
					label.setVisible(false);
				}
				if(text.equalsIgnoreCase("Invoice Reset")){			
					label.setVisible(false);					
				}
				if(text.equalsIgnoreCase("Generate e-Invoice number")){			
					label.setVisible(false);					
				}
				if(text.equalsIgnoreCase(AppConstants.UPDATESALESORDERINVOICE)){			
					label.setVisible(false);					
				}
				if(text.equalsIgnoreCase(AppConstants.generatePayment)){			
					label.setVisible(false);					
				}
				if(text.equalsIgnoreCase(AppConstants.syncWithZohoBooks)){			
					label.setVisible(false);					
				}
			}
			
			if(status.equals("Approved")&&renewFlagStatus==false)
			{
				if(text.equals(ManageApprovals.APPROVALREQUEST))
					label.setVisible(false);
				if(text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
					label.setVisible(false);
				if(text.equals("Tax Invoice"))
					label.setVisible(false);
				if(text.equals(ManageApprovals.SUBMIT))
					label.setVisible(false);
				
				/**
				 * rohan added this code for NBHC Changes ADMIN role should  
				 */
				if(LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN")){
					if(text.equals(AppConstants.CANCELINVOICE))
						label.setVisible(true);
					/**
					 *  Added By Priyanka - 22-07-2021
					 */
				}else if(LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("Zonal Coordinator")){
					if(text.equals(AppConstants.CANCELINVOICE))
						label.setVisible(true);
				}
				else
				{
					if(text.equals(AppConstants.CANCELINVOICE))
						label.setVisible(false);
				}
				
				/** Date 15-08-2017 if condition added by  vijay
				 * and old code added in else block 
				 */
				
				if(isPopUpAppMenubar()&& !isSummaryPopupFlag()){//Ashwini Patil added && !isSummaryPopupFlag()
					Console.log("in popup + !summarypopup");
					if(text.equals(AppConstants.VIEWPAYMENT)){
						label.setVisible(false);
					}
					/**
					 * Date : 10-10-2017 By ANIL
					 */
					if(text.equals(AppConstants.VIEWBILL)){
						label.setVisible(false);
					}
					/**
					 * End
					 */
					
				}else{
					Console.log("in popup + summarypopup");
				/**
				 * Date : 29-07-2017 By ANIL
				 */
				if(text.equals(AppConstants.VIEWPAYMENT)){
					label.setVisible(true);
				}
				/**
				 * End
				 */
				}
				
				/**
				 * Date : 10-10-2017 By ANIL
				 */
				if(text.equals(AppConstants.VIEWBILL)){
					label.setVisible(true);
				}
				/**
				 * End
				 */
				/**@Sheetal : 15-02-2022**/
				if(text.equals(AppConstants.VIEWORDER)){
					label.setVisible(true);
				}
				/**
				 * Date : 02-03-2018 By vijay
				 */
				if(text.equals(AppConstants.VIEWTAXINVOICE)){
					label.setVisible(false);
				}
				/**
				 * ends here
				 */
				/** Date 13.12.2018 added by komal for update accounting interface **/
				if(text.equalsIgnoreCase(AppConstants.UPDATEACCOUNTINGINTERFACE)){
					label.setVisible(true);
				}
				
				if(text.equalsIgnoreCase("Attendance Summary")){
					if(enableSiteLocation){
						label.setVisible(true);
					}else{
						label.setVisible(false);
					}
				}
				
				if(text.equalsIgnoreCase("Sales Order Summary")){
					if(uploadConsumableFlag){
						label.setVisible(true);
					}else{
						label.setVisible(false);
					}
				}
				
				if(text.equalsIgnoreCase("Invoice Reset")){			
					label.setVisible(true);					
				}
				if(text.equalsIgnoreCase("Generate e-Invoice number")){			
					label.setVisible(true);					
				}
				
				
				if(text.equalsIgnoreCase(AppConstants.UPDATESALESORDERINVOICE)){			
					if(LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN") && salesorderInvoiceUpdateFlag){
						label.setVisible(true);					
					}
					else{
						label.setVisible(false);					
					}
				}
				if(text.equalsIgnoreCase(AppConstants.generatePayment)){			
					label.setVisible(true);					
				}
				
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "EnableInvoiceIntegrationWithZohoBooks")) {
					if(text.equalsIgnoreCase(AppConstants.syncWithZohoBooks)){			
						label.setVisible(true);					
					}
				}
				
			}
			
			
			if(status.equals("Approved")&&renewFlagStatus==true)
			{
				if(text.equals(ManageApprovals.APPROVALREQUEST))
					label.setVisible(false);
				if(text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
					label.setVisible(false);
				if(text.equals(AppConstants.CANCELINVOICE))
					label.setVisible(false);
				if(text.equals("Tax Invoice"))
					label.setVisible(false);
				if(text.equals(ManageApprovals.SUBMIT))
					label.setVisible(false);
				
				/** Date 15-08-2017 if condition added by  vijay
				 * and old code added in else block 
				 */
				
				if(popUpAppMenubar){
					
					if(text.equals(AppConstants.VIEWPAYMENT)){
						label.setVisible(false);
					}
					
				}else{
					
				/**
				 * Date : 29-07-2017 By ANIL
				 */
				if(text.equals(AppConstants.VIEWPAYMENT)){
					label.setVisible(true);
				}
				/**
				 * End
				 */
				}
				/**
				 * Date : 10-10-2017 By ANIL
				 */
				if(text.equals(AppConstants.VIEWBILL)){
					label.setVisible(true);
				}
				/**
				 * End
				 */
				/**@Sheetal : 15-02-2022**/
				if(text.equals(AppConstants.VIEWORDER)){
					label.setVisible(true);
				}
				/**
				 * Date : 02-04-2018 By vijay
				 */
				if(text.equals(AppConstants.VIEWTAXINVOICE)){
					label.setVisible(false);
				}
				/**
				 * ends here
				 */
				/** Date 13.12.2018 added by komal for update accounting interface **/
				if(text.equalsIgnoreCase(AppConstants.UPDATEACCOUNTINGINTERFACE)){
					label.setVisible(true);
				}
				if(text.equalsIgnoreCase("Attendance Summary")){
					if(enableSiteLocation){
						label.setVisible(true);
					}else{
						label.setVisible(false);
					}
				}
				
				if(text.equalsIgnoreCase("Sales Order Summary")){
					if(uploadConsumableFlag){
						label.setVisible(true);
					}else{
						label.setVisible(false);
					}
				}
				
				if(text.equalsIgnoreCase("Invoice Reset")){			
					label.setVisible(true);					
				}
				if(text.equalsIgnoreCase("Generate e-Invoice number")){			
					label.setVisible(true);					
				}
				
				
				if(text.equalsIgnoreCase(AppConstants.UPDATESALESORDERINVOICE)){			
					if(LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN") && salesorderInvoiceUpdateFlag){
						label.setVisible(true);					
					}
					else{
						label.setVisible(false);					
					}
				}
				if(text.equalsIgnoreCase(AppConstants.generatePayment)){			
					label.setVisible(true);					
				}
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "EnableInvoiceIntegrationWithZohoBooks")) {
					if(text.equalsIgnoreCase(AppConstants.syncWithZohoBooks)){			
						label.setVisible(true);					
					}
				}
			}
			
			if(status.equals("Cancelled"))
			{
				if(text.equals(ManageApprovals.APPROVALREQUEST))
					label.setVisible(false);
				if(text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
					label.setVisible(false);
				if(text.equals(AppConstants.CANCELINVOICE))
					label.setVisible(false);
				if(text.equals("Tax Invoice"))
					label.setVisible(false);
				if(text.equals(ManageApprovals.SUBMIT))
					label.setVisible(false);
				/**
				 * Date : 29-07-2017 By ANIL
				 */
				if(text.equals(AppConstants.VIEWPAYMENT)){
					label.setVisible(false);
				}
				/**
				 * End
				 */
				/**
				 * Date : 10-10-2017 By ANIL
				 */
				if(text.equals(AppConstants.VIEWBILL)){
					label.setVisible(true);
				}
				/**
				 * End
				 */
				/**@Sheetal : 15-02-2022**/
				if(text.equals(AppConstants.VIEWORDER)){
					label.setVisible(true);
				}
				/**
				 * Date : 02-04-2018 By vijay
				 */
				if(text.equals(AppConstants.VIEWTAXINVOICE)){
					label.setVisible(false);
				}
				/**
				 * ends here
				 */
				/** Date 13.12.2018 added by komal for update accounting interface **/
				if(text.equalsIgnoreCase(AppConstants.UPDATEACCOUNTINGINTERFACE)){
					label.setVisible(false);
				}
				if(text.equalsIgnoreCase("Attendance Summary")){
					label.setVisible(false);
				}
				
				if(text.equalsIgnoreCase("Invoice Reset")){			
					label.setVisible(true);					
				}
				if(text.equalsIgnoreCase("Generate e-Invoice number")){			
					label.setVisible(false);					
				}
				
				if(text.equalsIgnoreCase(AppConstants.UPDATESALESORDERINVOICE)){			
					label.setVisible(false);					
				}
				if(text.equalsIgnoreCase(AppConstants.generatePayment)){			
					label.setVisible(false);					
				}
				if(text.equalsIgnoreCase(AppConstants.syncWithZohoBooks)){			
						label.setVisible(false);					
				}
				
			}
			
			if(status.equals("PInvoice"))
			{
				if(text.equals(ManageApprovals.APPROVALREQUEST))
					label.setVisible(false);
				if(text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
					label.setVisible(false);
				if(text.equals(AppConstants.CANCELINVOICE))
					label.setVisible(true);
				if(text.equals("Tax Invoice"))
					label.setVisible(true);
				if(text.equals(ManageApprovals.SUBMIT))
					label.setVisible(false);
				/**
				 * Date : 29-07-2017 By ANIL
				 */
				if(text.equals(AppConstants.VIEWPAYMENT)){
					label.setVisible(false);
				}
				/**
				 * End
				 */
				
				/**
				 * Date : 10-10-2017 By ANIL
				 */
				if(text.equals(AppConstants.VIEWBILL)){
					label.setVisible(true);
				}
				/**
				 * End
				 */
				/**@Sheetal : 15-02-2022**/
				if(text.equals(AppConstants.VIEWORDER)){
					label.setVisible(true);
				}
				/**
				 * Date : 02-04-2018 By vijay
				 */
				if(text.equals(AppConstants.VIEWTAXINVOICE)){
					label.setVisible(false);
				}
				/**
				 * ends here
				 */
				/** Date 13.12.2018 added by komal for update accounting interface **/
				if(text.equalsIgnoreCase(AppConstants.UPDATEACCOUNTINGINTERFACE)){
					label.setVisible(false);
				}
				if(text.equalsIgnoreCase("Attendance Summary")){
					label.setVisible(false);
				}
				if(text.equalsIgnoreCase("Invoice Reset")){			
					label.setVisible(false);					
				}
				if(text.equalsIgnoreCase("Generate e-Invoice number")){			
					label.setVisible(false);					
				}
				
				if(text.equalsIgnoreCase(AppConstants.UPDATESALESORDERINVOICE)){			
					if(LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN") && salesorderInvoiceUpdateFlag){
						label.setVisible(true);					
					}
					else{
						label.setVisible(false);					
					}
				}
				if(text.equalsIgnoreCase(AppConstants.generatePayment)){			
					label.setVisible(false);					
				}
				if(text.equalsIgnoreCase(AppConstants.syncWithZohoBooks)){			
					label.setVisible(false);					
				}
			}
			if(status.equals("Invoiced"))
			{
				if(text.equals(ManageApprovals.APPROVALREQUEST))
					label.setVisible(false);
				if(text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
					label.setVisible(false);
//				if(text.equals(AppConstants.CANCELINVOICE))
//					label.setVisible(true);
				if(text.equals("Tax Invoice"))
					label.setVisible(false);
				
				/**
				 * rohan added this code for NBHC Changes ADMIN role should  
				 */
				/*
				 * Added by sheetal :10-11-2021
				 * Des: making cancel invoice button visible to zonal coordinator as well
				 */
				if(LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN")
						|| LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("Zonal Coordinator")){
					if(text.equals(AppConstants.CANCELINVOICE))
						label.setVisible(true);
				}
				else
				{
					if(text.equals(AppConstants.CANCELINVOICE))
						label.setVisible(false);
				}
				if(text.equals(ManageApprovals.SUBMIT))
					label.setVisible(false);
				/**
				 * Date : 29-07-2017 By ANIL
				 */
				if(text.equals(AppConstants.VIEWPAYMENT)){
					label.setVisible(false);
				}
				/**
				 * End
				 */
				
				/**
				 * Date : 02-04-2018 By vijay
				 */
				if(text.equals(AppConstants.VIEWTAXINVOICE)){
					label.setVisible(true);
				}
				/**
				 * ends here
				 */
				/** Date 13.12.2018 added by komal for update accounting interface **/
				if(text.equalsIgnoreCase(AppConstants.UPDATEACCOUNTINGINTERFACE)){
					label.setVisible(false);
				}
				if(text.equalsIgnoreCase("Attendance Summary")){
					label.setVisible(false);
				}
				if(text.equalsIgnoreCase("Invoice Reset")){			
					label.setVisible(false);					
				}
				if(text.equalsIgnoreCase("Generate e-Invoice number")){			
					label.setVisible(false);					
				}
				
				if(text.equalsIgnoreCase(AppConstants.UPDATESALESORDERINVOICE)){			
					if(LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN") && salesorderInvoiceUpdateFlag){
						label.setVisible(true);					
					}
					else{
						label.setVisible(false);					
					}
				}
				if(text.equalsIgnoreCase(AppConstants.generatePayment)){			
					label.setVisible(false);					
				}
				if(text.equalsIgnoreCase(AppConstants.syncWithZohoBooks)){			
					label.setVisible(false);					
				}
			}
			if(isPopUpAppMenubar()&& !isSummaryPopupFlag()){ //Ashwini Patil added && !isSummaryPopupFlag()
				if(text.equals(AppConstants.VIEWBILL)){
					label.setVisible(false);
				}
			}
			if(isPopUpAppMenubar()&& !isSummaryPopupFlag()){ //Ashwini Patil added && !isSummaryPopupFlag()
				if(text.equals(AppConstants.VIEWTAXINVOICE)){
					label.setVisible(false);
				}
			}
			Console.log("isHideNavigationButtons="+isHideNavigationButtons()); 
			//Ashwini Patil Date:20-09-2022
			if(isHideNavigationButtons()) {
				Console.log("in changeProcessLevel isHideNavigationButtons");
				if(text.equalsIgnoreCase(AppConstants.VIEWBILL)||text.equalsIgnoreCase(AppConstants.VIEWPAYMENT)||text.equalsIgnoreCase(AppConstants.VIEWORDER)||text.equalsIgnoreCase(AppConstants.VIEWTAXINVOICE))
					label.setVisible(false);	
			}
		}
		
	}
	
	
	
	
	@Override
	public void setToNewState() {
		super.setToNewState();
		this.processLevelBar.setVisibleFalse(false);
	}

	@Override
	public void setToViewState() {
		super.setToViewState();
		this.setAppHeaderBarAsPerStatus();
		
		SuperModel model=new Invoice();
		model=invoiceobj;
		AppUtility.addDocumentToHistoryTable(AppConstants.ACCOUNTMODULE,AppConstants.INVOICEDETAILS, invoiceobj.getCount(), invoiceobj.getPersonInfo().getCount(),invoiceobj.getPersonInfo().getFullName(),invoiceobj.getPersonInfo().getCellNumber(), false, model, null);
	
		String mainScreenLabel="Invoice Details";
		if(invoiceobj!=null&&invoiceobj.getCount()!=0){
			mainScreenLabel=invoiceobj.getCount()+" "+"/"+" "+invoiceobj.getStatus()+" "+"/"+" "+AppUtility.parseDate(invoiceobj.getCreationDate());
		}
		fgroupingCustomerInformation.getHeaderLabel().setText(mainScreenLabel);
		
	}

	@Override
	public void setToEditState() {
		/**
		 * Old Code
		 */
//		super.setToEditState();
//		this.processLevelBar.setVisibleFalse(false);
//		
//		if(multipleBillInvoice==true)
//			{
//				conStartDur.setEnabled(true);
//				conEndDur.setEnabled(true);
//			}
		
		/*
		 * Ashwini Patil 
		 * Date:8-10-2024
		 * Ultra does not want to allow non admin user to edit last month/old invoices after specific deadline
		 * deadline is defined in company -company info -Invoice deadline field
		 */
		if(invoiceobj.getStatus().equals(Invoice.APPROVED)) {
			if(!LoginPresenter.myUserEntity.getRole().getRoleName().equals("ADMIN")) {
				boolean validDate=AppUtility.validateInvoiceEditDate(getDbinvoicedate().getValue());
				Console.log("validDate="+validDate);
				if(!validDate) {
					showDialogMessage("You cannot edit old Invoice now!");
					return;
				}			
			}		
		}
		
		if(invoiceobj!=null && addDeleteProductInInvoiceFlag && invoiceobj.getTypeOfOrder().equals(AppConstants.ORDERTYPESALES)){
			Console.log("invoiceobj.getTypeOfOrder() "+invoiceobj.getTypeOfOrder());
			prodInfoComposite.setEnable(true);
			baddproducts.setEnabled(true);
		}
		else{
			prodInfoComposite.setEnable(false);
			baddproducts.setEnabled(false);
		}
		
		/**
		 * Date : 20-11-2017 BY ANIL
		 * if invoice is approved then after it is allowed for updated then we check some validation
		 */
		if(invoiceobj!=null&&invoiceobj.getStatus().equals(Invoice.APPROVED)){
			if(eInvoiceNo.getText()!=null&&!eInvoiceNo.getText().equals("")){				
				showDialogMessage("Cancel e-Invoice first!");
				return;
			}

			checkForPaymentAndAccountingInterface(invoiceobj.getCount());
		} else {
			super.setToEditState();
			this.processLevelBar.setVisibleFalse(false);
			if (multipleBillInvoice == true) {
				conStartDur.setEnabled(true);
				conEndDur.setEnabled(true);
			}
		}
//		tbInvPrefix.setEnabled(false);
		
		String mainScreenLabel="Invoice Details";
		if(invoiceobj!=null&&invoiceobj.getCount()!=0){
			mainScreenLabel=invoiceobj.getCount()+" "+"/"+" "+invoiceobj.getStatus()+" "+"/"+" "+AppUtility.parseDate(invoiceobj.getCreationDate());
		}
		fgroupingCustomerInformation.getHeaderLabel().setText(mainScreenLabel);
		
		tbCreatedBy.setEnabled(false);		
		tbCreatedOn.setEnabled(false);		
		tbUpdatedBy.setEnabled(false);		
		tbUpdatedOn.setEnabled(false);		
		tbApprovedBy.setEnabled(false);		
		tbApprovedOn.setEnabled(false);		
		tbCancelledBy.setEnabled(false);		
		tbCancelledOn.setEnabled(false);		
		tbLog.setEnabled(false);
		eInvoiceNo.setEnabled(false);
		eInvoiceCancellationDate.setEnabled(false);
		eInvoiceStatus.setEnabled(false);
		ackDate.setEnabled(false);
		ackNo.setEnabled(false);
		qrCode.setEnabled(false);	
		
		//for pest mortem
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "MakeCustomerEditable"))
				personInfoComposite.setEnabled(true);
		else
			personInfoComposite.setEnabled(false);//Ashwini Patil Date:15-09-2023
		
	}

	@Override
	public TextBox getstatustextbox() {
		return this.tbstatus;
	}
	
	@Override
	public void onChange(ChangeEvent event) {
		System.out.println("on change");
		if(event.getSource().equals(olbInvoiceCategory))
		{
			if(olbInvoiceCategory.getSelectedIndex()!=0){
				ConfigCategory cat=olbInvoiceCategory.getSelectedItem();
				if(cat!=null){
					AppUtility.makeLiveTypeDropDown(olbInvoiceConfigType, cat.getCategoryName(), cat.getCategoryCode(), cat.getInternalType());
				}
			}
		}
		
		Console.log("valueee of invPrefFlag "+invPrefFlag);
		if(invPrefFlag){
			if (event.getSource().equals(olbbranch)) {
				Console.log("inside on change  "+invPrefFlag);
				if (olbbranch.getSelectedIndex() != 0) {
					getInvoicePrefixDetails(olbbranch.getValue());
				}
			}
		}
		
		
		
		/**
	     * Date 04-09-2017 added by vijay for final total amount and net payable after discount
	     */
		
		if(event.getSource().equals(dbDiscountAmt)){

			if(this.getDbDiscountAmt().getValue()==null){
				this.getDbFinalTotalAmt().setValue(this.getDbtotalAmt().getValue());
				
				this.invoiceTaxTable.connectToLocal();
				try {
					this.addProdTaxes();
					/**
					 * Date : 21-09-2017 BY ANIL
					 */
					this.addOtherChargesInTaxTbl();
					/**
					 * End
					 */
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				double totalIncludingTax=this.getDbFinalTotalAmt().getValue()+this.getInvoiceTaxTable().calculateTotalTaxes();

				/**
				 * Date : 22-09-2017 BY  ANIL
				 */
				if(dbOtherChargesTotal.getValue()!=null){
					totalIncludingTax=totalIncludingTax+dbOtherChargesTotal.getValue();
				}
				/**
				 * END
				 */
				
				double netPay = Double.parseDouble(nf.format(totalIncludingTax));
				netPay = Math.round(netPay);
				
				this.getDbtotalAmtIncludingTax().setValue(netPay);
				if(LoginPresenter.PC_NO_ROUNDOFF_INVOICE_CONTRACTFlag){
					//this is for if there in any other charges
					netPay =netPay + this.getInvoiceChargesTable().calculateNetPayable();

				}
				else{
					//this is for if there in any other charges
					netPay = Math.round(netPay + this.getInvoiceChargesTable().calculateNetPayable());

				}
				
				this.getDototalbillamt().setValue(netPay);
				
				if(!tbroundOffAmt.getValue().equals("") && tbroundOffAmt.getValue()!=null){
					String roundOff = tbroundOffAmt.getValue();
					double roundoffAmt =  AppUtility.calculateRoundOff(roundOff, netPay);
					if(roundoffAmt!=0){
						this.getNetPayable().setValue(roundoffAmt);
						this.getDoinvoiceamount().setValue(roundoffAmt);
					}else{
						this.getNetPayable().setValue(netPay);
						this.getDoinvoiceamount().setValue(netPay);
					}
				}
				else{
					this.getNetPayable().setValue(netPay);
					this.getDoinvoiceamount().setValue(netPay);

				}
			}
			else if(this.getDbDiscountAmt().getValue() > this.getDbtotalAmt().getValue()){
				showDialogMessage("Discount Amount can not be greater than total amount");
				this.getDbDiscountAmt().setValue(0d);
			}else{
				reactOnDiscountAmtChange();
			}
			
		}
		/**
		 * ends here
		 */
		
		/**
		 * Date 04-09-2017 added by vijay for roundoff and managing net payable
		 */
		if(event.getSource().equals(tbroundOffAmt)){

			double totalIncludingTax=this.getDbFinalTotalAmt().getValue()+this.getInvoiceTaxTable().calculateTotalTaxes();
			/**
			 * Date : 10-10-2017 BY ANIL
			 */
			if(dbOtherChargesTotal.getValue()!=null){
				totalIncludingTax=totalIncludingTax+dbOtherChargesTotal.getValue();
			}
			/**
			 * end
			 */
			double netPay = Double.parseDouble(nf.format(totalIncludingTax));
			if(LoginPresenter.PC_NO_ROUNDOFF_INVOICE_CONTRACTFlag){
				netPay = netPay + this.getInvoiceChargesTable().calculateNetPayable();
			}
			else{
				//this is for if there in any other charges
				netPay = Math.round(netPay + this.getInvoiceChargesTable().calculateNetPayable());
			}
			
			if(tbroundOffAmt.getValue().equals("")||tbroundOffAmt.getValue()==null){
				this.getNetPayable().setValue(netPay);
				this.getDoinvoiceamount().setValue(netPay);

			}else{
//				this.getNetPayable().setValue(netPay - dbroundOffAmt.getValue());
//				this.getDoinvoiceamount().setValue(netPay - dbroundOffAmt.getValue());
				
				if(!tbroundOffAmt.getValue().equals("")&& tbroundOffAmt.getValue()!=null){
					String roundOff = tbroundOffAmt.getValue();
					double roundoffAmt =  AppUtility.calculateRoundOff(roundOff, netPay);
					if(roundoffAmt!=0){
						this.getNetPayable().setValue(roundoffAmt);
						this.getDoinvoiceamount().setValue(roundoffAmt);
					}else{
						this.getNetPayable().setValue(netPay);
						this.getDoinvoiceamount().setValue(netPay);
						this.getTbroundOffAmt().setValue("");
					}
				}

			}
		}
		/**
		 * ends here
		 */
		
		if(event.getSource().equals(ibcreditPeriod)){
			reactonCreditPeriod();
		}
		if(event.getSource().equals(dbDispatchDate.getTextBox())){
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "AddCreditPeriodAndDispatchDate")){
				if(dbDispatchDate.getValue()==null){
					reactonChangeInvoiceDate();
				}
			}
			
		}
		
		if(event.getSource().equals(olbbranch)){
			getcustomerBillingAddress();
		}
		
		if(event.getSource().equals(obTermsandCondition)){
			if(obTermsandCondition.getSelectedIndex()!=0){
				Declaration obj = obTermsandCondition.getSelectedItem();
				String value =obj.getDeclaratiomMsg();
				if(taDeclaration.getValue()!=null && !taDeclaration.getValue().equals("")){
					taDeclaration.setValue(taDeclaration.getValue()+"\n"+value);
				}else{
					taDeclaration.setValue(value);
				}
			}
		}
	}

	
	private void reactonCreditPeriod() {
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "AddCreditPeriodAndDispatchDate")){
			if(ibcreditPeriod.getValue()!=null){
				int creditperiod = ibcreditPeriod.getValue();
				if(dbDispatchDate!=null){
					Date dispatchDate = dbDispatchDate.getValue();
					CalendarUtil.addDaysToDate(dispatchDate, creditperiod);
					dbpaymentdate.setValue(dispatchDate);
				}
				else{
					Date invoiceDate = dbinvoicedate.getValue();
					CalendarUtil.addDaysToDate(invoiceDate, creditperiod);
					dbpaymentdate.setValue(invoiceDate);
				}
			}

		}

	}

	/**
     * Date 04-09-2017 added by vijay for final total amount and net payable after discount
     */

	private void reactOnDiscountAmtChange() {
				showWaitSymbol();
				NumberFormat nf=NumberFormat.getFormat("0.00");
				double discountAmt = this.getDbDiscountAmt().getValue();
				double finalAmt = this.getDbtotalAmt().getValue()-discountAmt;
			    this.getDbFinalTotalAmt().setValue(finalAmt);
			    
				try {
					this.invoiceTaxTable.connectToLocal();
					this.addProdTaxes();
					
					/**
					 * Date : 21-09-2017 BY ANIL
					 */
					this.addOtherChargesInTaxTbl();
					/**
					 * End
					 */
					
					double totalIncludingTax=this.getDbFinalTotalAmt().getValue()+this.getInvoiceTaxTable().calculateTotalTaxes();
					
					/**
					 * Date : 22-09-2017 BY  ANIL
					 */
					if(dbOtherChargesTotal.getValue()!=null){
						totalIncludingTax=totalIncludingTax+dbOtherChargesTotal.getValue();
					}
					/**
					 * END
					 */
					double netPay = Double.parseDouble(nf.format(totalIncludingTax));
					
					if(!LoginPresenter.PC_NO_ROUNDOFF_INVOICE_CONTRACTFlag)
					netPay = Math.round(netPay);
					
					this.getDbtotalAmtIncludingTax().setValue(netPay);
					
					if(LoginPresenter.PC_NO_ROUNDOFF_INVOICE_CONTRACTFlag){
						//this is for if there in any other charges
						netPay = netPay + this.getInvoiceChargesTable().calculateNetPayable();
					}
					else{
						//this is for if there in any other charges
						netPay = Math.round(netPay + this.getInvoiceChargesTable().calculateNetPayable());
					}
					
					this.getDototalbillamt().setValue(netPay);
					
					if(!tbroundOffAmt.getValue().equals("") && tbroundOffAmt.getValue()!=null){
						String roundOff = tbroundOffAmt.getValue();
						double roundoffAmt =  AppUtility.calculateRoundOff(roundOff, netPay);
						if(roundoffAmt!=0){
							this.getNetPayable().setValue(roundoffAmt);
							this.getDoinvoiceamount().setValue(roundoffAmt);
						}else{
							this.getNetPayable().setValue(netPay);
							this.getTbroundOffAmt().setValue("");
							this.getDoinvoiceamount().setValue(netPay);
						}
					}
					else{
						this.getNetPayable().setValue(netPay);
						this.getDoinvoiceamount().setValue(netPay);
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
				this.hideWaitSymbol();
	}

	/**
	 * ends here
	 */
	
	/******************************Getters And Setters**********************************/
	
	
	public TextBox getIbinvoiceid() {
		return ibinvoiceid;
	}

	public void setIbinvoiceid(TextBox ibinvoiceid) {
		this.ibinvoiceid = ibinvoiceid;
	}

	public IntegerBox getIbbillingid() {
		return ibbillingid;
	}

	public void setIbbillingid(IntegerBox ibbillingid) {
		this.ibbillingid = ibbillingid;
	}

	public TextBox getIbcontractid() {
		return ibcontractid;
	}

	public void setIbcontractid(TextBox ibcontractid) {
		this.ibcontractid = ibcontractid;
	}

//	public TextBox getTbpersoncount() {
//		return tbpersoncount;
//	}
//
//	public void setTbpersoncount(TextBox tbpersoncount) {
//		this.tbpersoncount = tbpersoncount;
//	}
//
//	public TextBox getTbpersonname() {
//		return tbpersonname;
//	}
//
//	public void setTbpersonname(TextBox tbpersonname) {
//		this.tbpersonname = tbpersonname;
//	}
//
//	public TextBox getTbpersoncell() {
//		return tbpersoncell;
//	}
//
//	public void setTbpersoncell(TextBox tbpersoncell) {
//		this.tbpersoncell = tbpersoncell;
//	}

	public DateBox getDbstartDate() {
		return dbstartDate;
	}

	public void setDbstartDate(DateBox dbstartDate) {
		this.dbstartDate = dbstartDate;
	}

	public DateBox getDbendDate() {
		return dbendDate;
	}

	public void setDbendDate(DateBox dbendDate) {
		this.dbendDate = dbendDate;
	}

	public DateBox getDbbillingdate() {
		return dbbillingdate;
	}

	public void setDbbillingdate(DateBox dbbillingdate) {
		this.dbbillingdate = dbbillingdate;
	}


	public DoubleBox getDosalesamount() {
		return dosalesamount;
	}

	public void setDosalesamount(DoubleBox dosalesamount) {
		this.dosalesamount = dosalesamount;
	}

	public TextArea getTacomment() {
		return tacomment;
	}

	public void setTacomment(TextArea tacomment) {
		this.tacomment = tacomment;
	}

	public TextBox getTbstatus() {
		return tbstatus;
	}

	public void setTbstatus(TextBox tbstatus) {
		this.tbstatus = tbstatus;
	}

	public ObjectListBox<Employee> getOlbApproverName() {
		return olbApproverName;
	}

	public void setOlbApproverName(ObjectListBox<Employee> olbApproverName) {
		this.olbApproverName = olbApproverName;
	}
	
	public DateBox getDbinvoicedate() {
		return dbinvoicedate;
	}

	public void setDbinvoicedate(DateBox dbinvoicedate) {
		this.dbinvoicedate = dbinvoicedate;
	}

	public ObjectListBox<Employee> getOlbEmployee() {
		return olbEmployee;
	}

	public void setOlbEmployee(ObjectListBox<Employee> olbEmployee) {
		this.olbEmployee = olbEmployee;
	}

	public ObjectListBox<Branch> getOlbbranch() {
		return olbbranch;
	}

	public void setOlbbranch(ObjectListBox<Branch> olbbranch) {
		this.olbbranch = olbbranch;
	}

	public TextBox getTbinvoiceType() {
		return tbinvoiceType;
	}

	public void setTbinvoiceType(TextBox tbinvoiceType) {
		this.tbinvoiceType = tbinvoiceType;
	}

	public BillingDocumentTable getBillingdoctable() {
		return billingdoctable;
	}

	public void setBillingdoctable(BillingDocumentTable billingdoctable) {
		this.billingdoctable = billingdoctable;
	}

	public DoubleBox getDototalbillamt() {
		return dototalbillamt;
	}

	public void setDototalbillamt(DoubleBox dototalbillamt) {
		this.dototalbillamt = dototalbillamt;
	}

	public DateBox getDbpaymentdate() {
		return dbpaymentdate;
	}

	public void setDbpaymentdate(DateBox dbpaymentdate) {
		this.dbpaymentdate = dbpaymentdate;
	}

	public DoubleBox getDoinvoiceamount() {
		return doinvoiceamount;
	}

	public void setDoinvoiceamount(DoubleBox doinvoiceamount) {
		this.doinvoiceamount = doinvoiceamount;
	}

	public TextBox getTbremark() {
		return tbremark;
	}

	public void setTbremark(TextBox tbremark) {
		this.tbremark = tbremark;
	}

	public ObjectListBox<ConfigCategory> getOlbInvoiceCategory() {
		return olbInvoiceCategory;
	}

	public void setOlbInvoiceCategory(
			ObjectListBox<ConfigCategory> olbInvoiceCategory) {
		this.olbInvoiceCategory = olbInvoiceCategory;
	}

	public ObjectListBox<Type> getOlbInvoiceConfigType() {
		return olbInvoiceConfigType;
	}

	public void setOlbInvoiceConfigType(ObjectListBox<Type> olbInvoiceConfigType) {
		this.olbInvoiceConfigType = olbInvoiceConfigType;
	}

	public ObjectListBox<Config> getOlbinvoicegroup() {
		return olbinvoicegroup;
	}

	public void setOlbinvoicegroup(ObjectListBox<Config> olbinvoicegroup) {
		this.olbinvoicegroup = olbinvoicegroup;
	}

//	public TextBox getTbpocname() {
//		return tbpocname;
//	}
//
//	public void setTbpocname(TextBox tbpocname) {
//		this.tbpocname = tbpocname;
//	}
	
	public SalesOrderProductTable getSalesProductTable() {
		return salesProductTable;
	}

	public void setSalesProductTable(SalesOrderProductTable salesProductTable) {
		this.salesProductTable = salesProductTable;
	}

	public BillingProductTaxesTable getInvoiceTaxTable() {
		return invoiceTaxTable;
	}

	public void setInvoiceTaxTable(BillingProductTaxesTable invoiceTaxTable) {
		this.invoiceTaxTable = invoiceTaxTable;
	}

	public BillingProductChargesTable getInvoiceChargesTable() {
		return invoiceChargesTable;
	}

	public void setInvoiceChargesTable(
			BillingProductChargesTable invoiceChargesTable) {
		this.invoiceChargesTable = invoiceChargesTable;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Double getToatalAmountIncludingTax() {
		return toatalAmountIncludingTax;
	}

	public void setToatalAmountIncludingTax(Double toatalAmountIncludingTax) {
		this.toatalAmountIncludingTax = toatalAmountIncludingTax;
	}

	public Double getNetPayableAmount() {
		return netPayableAmount;
	}

	public void setNetPayableAmount(Double netPayableAmount) {
		this.netPayableAmount = netPayableAmount;
	}
	
	private void makelivecustomerBranchList(int count, final String customerbranch) {

		GenricServiceAsync service=GWT.create(GenricService.class);

		System.out.println("hi");
		System.out.println("Value cinfo=="+count);
		
		MyQuerry querry = new MyQuerry();
		
	  	Company c = new Company();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
	  
		filter = new Filter();
	  	filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("cinfo.count");
		filter.setIntValue(count);
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new CustomerBranchDetails());
		
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				
				System.out.println("RESULT=="+result.size());
				
				lbcustomerbranch.clear();
				lbcustomerbranch.addItem("--SELECT--");
                if(result.size()!=0){
				for(SuperModel model:result){
					
					CustomerBranchDetails  customerbranch = (CustomerBranchDetails)model;
					lbcustomerbranch.addItem(customerbranch.getBusinessUnitName());
					/***14-12-2019 Deepak Salve added this line for set Customer branch in Drop down list***/
					lbcustomerbranch.setValue(0,customerbranch.getBusinessUnitName());
					/***End***/

				}
				
				int count = lbcustomerbranch.getItemCount();
				String item=customerbranch;
				for(int i=0;i<count;i++)
				{
					if(item.trim().equals(lbcustomerbranch.getItemText(i).trim()))
					{
						lbcustomerbranch.setSelectedIndex(i);
						break;
					}
				}
			}
                /***14-12-2019 Deepak Salve added this line for set Customer branch in Drop down list***/
                lbcustomerbranch.setValue(0,customerbranch);
                /***End***/
				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				showDialogMessage("An Unexpected Error occured !");
			}
		});
		
	}

	
	//    rohan added this for invoice editing ****************
	
	public void addProdTaxes() throws Exception
	{
		System.out.println();
		System.out.println("INSIDE addProdTaxes() METHOD.....");
		List<SalesOrderProductLineItem> salesLineItemLis=this.salesProductTable.getDataprovider().getList();
		
		System.out.println("PRODUCT TABLE SIZE "+salesLineItemLis.size());
		List<ContractCharges> taxList=this.invoiceTaxTable.getDataprovider().getList();
		System.out.println("PRODUCT TAX TABLE SIZE "+taxList.size());
		
		NumberFormat nf=NumberFormat.getFormat("#.00");
		for(int i=0;i<salesLineItemLis.size();i++)
		{
			
			/**
			 * Date : 10-01-2018 BY ANIL
			 * Tax table or tax calculation is happening on base bill amount ideally it should be on base payable amount
			 */
			double billPayableAmount=0;
//			if(salesLineItemLis.get(i).getBasePaymentAmount()!=0){
				billPayableAmount=salesLineItemLis.get(i).getBasePaymentAmount();
//			}else{
//				billPayableAmount=salesLineItemLis.get(i).getBaseBillingAmount();
//			}
			
			System.out.println("billPayableAmount "+billPayableAmount);
			
			double priceqty=0,taxPrice=0;
//			if(salesLineItemLis.get(i).getProdPercDiscount()==null){
//				taxPrice=this.salesProductTable.removeAllTaxes(salesLineItemLis.get(i).getPrduct(),salesLineItemLis.get(i).getBaseBillingAmount());
//				priceqty=(salesLineItemLis.get(i).getBaseBillingAmount()-taxPrice)*salesLineItemLis.get(i).getQuantity();
//				priceqty=Double.parseDouble(nf.format(priceqty));
//			}
//			if(salesLineItemLis.get(i).getProdPercDiscount()!=null){
//				taxPrice=this.salesProductTable.removeAllTaxes(salesLineItemLis.get(i).getPrduct(),salesLineItemLis.get(i).getBaseBillingAmount());
//				priceqty=(salesLineItemLis.get(i).getBaseBillingAmount()-taxPrice);
//				priceqty=priceqty-(priceqty*salesLineItemLis.get(i).getProdPercDiscount()/100);
//				priceqty=priceqty*salesLineItemLis.get(i).getQuantity();
//				priceqty=Double.parseDouble(nf.format(priceqty));
//			}
			
			if(salesLineItemLis.get(i).getFlatDiscount()!=0)
			{
				
				
				/**
				 * Date: 31-07-2017 By Anil
				 * below line is commented for not calculating inclusive amount on already inclusive calculated amount from contract
				 */
//				taxPrice=this.salesProductTable.removeAllTaxes(salesLineItemLis.get(i).getPrduct(),salesLineItemLis.get(i).getBaseBillingAmount());
				/**
				 * @author Anil,Date:23-01-2018
				 * base payable amount is a amount after removing flat discount,so if base payable amount is not zero then we will not subract flat discount from it
				 * for Eco safe raised by Rahul tiwari(BUG)
				 */
				if(salesLineItemLis.get(i).getBasePaymentAmount()!=0){
					priceqty=(billPayableAmount-taxPrice);
				}else{
					priceqty=(billPayableAmount-taxPrice)-salesLineItemLis.get(i).getFlatDiscount();
				}
				/**
				 * rohan commented this below for removing qty from price calculations
				 */
//				priceqty=priceqty*salesLineItemLis.get(i).getQuantity();
				System.out.println("00000000000000000"+priceqty);
			}
			
			else
			{
			
			//****************************************************************************
				if((salesLineItemLis.get(i).getProdPercDiscount()==null && salesLineItemLis.get(i).getProdPercDiscount()==0) && (salesLineItemLis.get(i).getDiscountAmt()==0)){
				
//				System.out.println("inside both 0 condition");
//				
//				priceVal=entity.getBaseBillingAmount()-taxAmt;
//				priceVal=priceVal*entity.getQuantity();
//				sum=sum+priceVal;
//				System.out.println("RRRRRRRRRRRRRR sum"+sum);
	
//					taxPrice=this.salesProductTable.removeAllTaxes(salesLineItemLis.get(i).getPrduct(),salesLineItemLis.get(i).getBaseBillingAmount());
					
					/**
					 * rohan commented this below for removing qty from price calculations
					 */
					//  new code 
					priceqty=(billPayableAmount-taxPrice);
					// old code 				
//					priceqty=(salesLineItemLis.get(i).getBaseBillingAmount()-taxPrice)*salesLineItemLis.get(i).getQuantity();
					
					/**
					 * ends here ....
					 */
					
					
					priceqty=Double.parseDouble(nf.format(priceqty));
					
					System.out.println("111111111"+priceqty);
			}
			
			else if((salesLineItemLis.get(i).getProdPercDiscount()!=null && salesLineItemLis.get(i).getProdPercDiscount()!=0)&& (salesLineItemLis.get(i).getDiscountAmt()!=0)){
				
//				taxPrice=this.salesProductTable.removeAllTaxes(salesLineItemLis.get(i).getPrduct(),salesLineItemLis.get(i).getBaseBillingAmount());
				System.out.println("inside both not null condition");
//				
				priceqty=billPayableAmount-taxPrice;
				
//				priceVal=priceVal-(priceVal*entity.getProdPercDiscount()/100);
//				priceVal=priceVal-entity.getDiscountAmt();
				
				/**
				 * rohan commented this below for removing qty from price calculations
				 */
//				priceqty=priceqty*salesLineItemLis.get(i).getQuantity();
				/**
				 * endds here 
				 */
				
				
//				sum=sum+priceVal;
//				System.out.println("RRRRRRRRRRRRRR sum"+sum);
				
				
//				taxPrice=this.salesProductTable.removeAllTaxes(salesLineItemLis.get(i).getPrduct(),salesLineItemLis.get(i).getBaseBillingAmount());
				priceqty=(billPayableAmount-taxPrice);
//				priceqty=priceqty-(priceqty*salesLineItemLis.get(i).getProdPercDiscount()/100);
//				priceqty=priceqty-salesLineItemLis.get(i).getDiscountAmt();
				/**
				 * rohan commented this below for removing qty from price calculations
				 */
//				priceqty=priceqty*salesLineItemLis.get(i).getQuantity();
				/**
				 * ends here 
				 */
				priceqty=Double.parseDouble(nf.format(priceqty));
				System.out.println("111111111"+priceqty);
			}
			else 
			{
				System.out.println("inside oneof the null condition");
				
				
//				taxPrice=this.salesProductTable.removeAllTaxes(salesLineItemLis.get(i).getPrduct(),salesLineItemLis.get(i).getBaseBillingAmount());
			if(salesLineItemLis.get(i).getProdPercDiscount()!=null && salesLineItemLis.get(i).getProdPercDiscount()!=0){	
				priceqty=(billPayableAmount-taxPrice);
//				priceqty=priceqty-(priceqty*salesLineItemLis.get(i).getProdPercDiscount()/100);
				/**
				 * rohan commented this below for removing qty from price calculations
				 */
//				priceqty=priceqty*salesLineItemLis.get(i).getQuantity();
				/**
				 * endds here 
				 */
				priceqty=Double.parseDouble(nf.format(priceqty));
				
			}
			else 
			{
				priceqty=(billPayableAmount-taxPrice);
//				priceqty=priceqty-salesLineItemLis.get(i).getDiscountAmt();
				/**
				 * rohan commented this below for removing qty from price calculations
				 */
//				priceqty=priceqty*salesLineItemLis.get(i).getQuantity();
				/**
				 * ends here 
				 */
				priceqty=Double.parseDouble(nf.format(priceqty));
			}
//					priceVal=entity.getBaseBillingAmount()-taxAmt;
//					if(entity.getProdPercDiscount()!=null && entity.getProdPercDiscount()!=0){
//						System.out.println("inside getPercentageDiscount oneof the null condition");
//					priceVal=priceVal-(priceVal*entity.getProdPercDiscount()/100);
//					}
//					else 
//					{
//						System.out.println("inside getDiscountAmt oneof the null condition");
//					priceVal=priceVal-entity.getDiscountAmt();
//					System.out.println("price value"+priceVal);
//					}
//					
//					priceqty=priceqty*salesLineItemLis.get(i).getQuantity();
//					System.out.println("sum value "+sum);
//					System.out.println("price out side for loop  value"+priceVal);
//					sum=sum+priceVal;
//					System.out.println("RRRRRRRRRRRRRR sum"+sum);
			}

		System.out.println("2222222222222222222222222222222"+priceqty);

			}
			//***************************************************************************
			
			/**
			 * Date 03-07-2017 below code commented by vijay 
			 * for this below code by default showing vat and service tax percent 0 no nedd this
			 */
			
//			if(salesLineItemLis.get(i).getVatTax().getPercentage()==0){
//				
//				ContractCharges pocentity=new ContractCharges();
//				pocentity.setTaxChargeName("VAT");
//				pocentity.setTaxChargePercent(salesLineItemLis.get(i).getVatTax().getPercentage());
//				pocentity.setIdentifyTaxCharge(i+1);
//				
//				int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getVatTax().getPercentage(),"VAT");
//				
//				if(indexValue!=-1){
//					pocentity.setTaxChargeAssesVal(0);
//					this.invoiceTaxTable.getDataprovider().getList().remove(indexValue);
//				}
//				
//				if(indexValue==-1){
//					pocentity.setTaxChargeAssesVal(0);
//				}
//				this.invoiceTaxTable.getDataprovider().getList().add(pocentity);
//			}
//			
//			if(salesLineItemLis.get(i).getServiceTax().getPercentage()==0){
//				ContractCharges pocentity=new ContractCharges();
//				pocentity.setTaxChargeName("Service Tax");
//				pocentity.setTaxChargePercent(salesLineItemLis.get(i).getServiceTax().getPercentage());
//				pocentity.setIdentifyTaxCharge(i+1);
//				int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getServiceTax().getPercentage(),"Service");
//				if(indexValue!=-1){
//					pocentity.setTaxChargeAssesVal(0);
//					this.invoiceTaxTable.getDataprovider().getList().remove(indexValue);
//				}
//				
//				if(indexValue==-1){
//					pocentity.setTaxChargeAssesVal(0);
//				}
//				this.invoiceTaxTable.getDataprovider().getList().add(pocentity);
//			}

			/**
			 * ends here
			 */
			
			/**
		     * Date 04-09-2017 added by vijay for if discount amount is there then taxes calculation based on after
		     * discount Total amount
		     */
			if(dbDiscountAmt.getValue()!=null && dbDiscountAmt.getValue()!=0){
				/**
				 * Old Code
				 */
//				System.out.println("hi vijay inside discount amount");
//				priceqty = dbFinalTotalAmt.getValue();
				
				
				/**
				 * Date : 23-09-2017 BY ANIL
				 * 
				 */
				if(isValidForDiscount()){
					priceqty = dbFinalTotalAmt.getValue()/salesProductTable.getDataprovider().getList().size();
					System.out.println("hi vijay inside discount amount "+priceqty);
				}else{
					dbDiscountAmt.setValue(0.0);
					dbFinalTotalAmt.setValue(dbtotalAmt.getValue());
				}
			}
		
			if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
				//  rohan added this conditions
				if(salesLineItemLis.get(i).getVatTax().getTaxPrintName()!=null && !salesLineItemLis.get(i).getVatTax().getTaxPrintName().equals(""))
				{
					System.out.println("Inside vat GST");
					ContractCharges pocentity=new ContractCharges();
					//   old code commented by rohan on date : 22-6-2017
	//				pocentity.setChargeName("VAT");
					//   new code by rohan Date 22-6-2017 
					System.out.println("salesLineItemLis.get(i).getVatTax().getTaxName()"+salesLineItemLis.get(i).getVatTax().getTaxPrintName());
					pocentity.setTaxChargeName(salesLineItemLis.get(i).getVatTax().getTaxPrintName());
					pocentity.setTaxChargePercent(salesLineItemLis.get(i).getVatTax().getPercentage());
					pocentity.setIdentifyTaxCharge(i+1);
					int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getVatTax().getPercentage(),salesLineItemLis.get(i).getVatTax().getTaxPrintName());
					
					if(indexValue!=-1){
						System.out.println("Index As Not -1");
						pocentity.setTaxChargeAssesVal(priceqty+taxList.get(indexValue).getTaxChargeAssesVal());
//						pocentity.setTaxChargeAssesVal(priceqty);
						this.invoiceTaxTable.getDataprovider().getList().remove(indexValue);
					}
					if(indexValue==-1){
						pocentity.setTaxChargeAssesVal(priceqty);
					}
					this.invoiceTaxTable.getDataprovider().getList().add(pocentity);
				}
				else
				{
					System.out.println("Vattx");
					ContractCharges pocentity=new ContractCharges();
					pocentity.setTaxChargeName("VAT");
					pocentity.setTaxChargePercent(salesLineItemLis.get(i).getVatTax().getPercentage());
					pocentity.setIdentifyTaxCharge(i+1);
					int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getVatTax().getPercentage(),"VAT");
					
					if(indexValue!=-1){
						System.out.println("Index As Not -1");
						pocentity.setTaxChargeAssesVal(priceqty+taxList.get(indexValue).getTaxChargeAssesVal());
						this.invoiceTaxTable.getDataprovider().getList().remove(indexValue);
					}
					if(indexValue==-1){
						pocentity.setTaxChargeAssesVal(priceqty);
					}
					this.invoiceTaxTable.getDataprovider().getList().add(pocentity);
				}
			}
			
			if(salesLineItemLis.get(i).getServiceTax().getPercentage()!=0){
			//  rohan added this conditions
				if(salesLineItemLis.get(i).getServiceTax().getTaxPrintName()!=null && !salesLineItemLis.get(i).getServiceTax().getTaxPrintName().equals(""))
				{
					System.out.println("Inside vat GST");
					ContractCharges pocentity=new ContractCharges();
					//   old code commented by rohan on date : 22-6-2017
//					pocentity.setChargeName("VAT");
					//   new code by rohan Date 22-6-2017 
					System.out.println("salesLineItemLis.get(i).getVatTax().getTaxName()"+salesLineItemLis.get(i).getServiceTax().getTaxPrintName());
					pocentity.setTaxChargeName(salesLineItemLis.get(i).getServiceTax().getTaxPrintName());
					pocentity.setTaxChargePercent(salesLineItemLis.get(i).getServiceTax().getPercentage());
					pocentity.setIdentifyTaxCharge(i+1);
					int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getServiceTax().getPercentage(),salesLineItemLis.get(i).getServiceTax().getTaxPrintName());
					
					if(indexValue!=-1){
						System.out.println("Index As Not -1");
						pocentity.setTaxChargeAssesVal(priceqty+taxList.get(indexValue).getTaxChargeAssesVal());
//						pocentity.setTaxChargeAssesVal(priceqty);
						this.invoiceTaxTable.getDataprovider().getList().remove(indexValue);
					}
					if(indexValue==-1){
						pocentity.setTaxChargeAssesVal(priceqty);
					}
					this.invoiceTaxTable.getDataprovider().getList().add(pocentity);
				}
				else
				{
					System.out.println("ServiceTx");
					ContractCharges pocentity=new ContractCharges();
					pocentity.setTaxChargeName("Service Tax");
					pocentity.setTaxChargePercent(salesLineItemLis.get(i).getServiceTax().getPercentage());
					pocentity.setIdentifyTaxCharge(i+1);
					int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getServiceTax().getPercentage(),"Service");
					if(indexValue!=-1){
						System.out.println("Index As Not -1");
						double assessValue=0;
						if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
							assessValue=priceqty+(priceqty*salesLineItemLis.get(i).getVatTax().getPercentage()/100);
							pocentity.setTaxChargeAssesVal(assessValue+taxList.get(indexValue).getTaxChargeAssesVal());
							this.invoiceTaxTable.getDataprovider().getList().remove(indexValue);
						}
						if(salesLineItemLis.get(i).getVatTax().getPercentage()==0){
							pocentity.setTaxChargeAssesVal(priceqty+taxList.get(indexValue).getTaxChargeAssesVal());
							this.invoiceTaxTable.getDataprovider().getList().remove(indexValue);
						}
						
					}
					if(indexValue==-1){
						System.out.println("Index As -1");
						double assessVal=0;
						if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
							assessVal=priceqty+(priceqty*salesLineItemLis.get(i).getVatTax().getPercentage()/100);
							pocentity.setTaxChargeAssesVal(assessVal);
						}
						if(salesLineItemLis.get(i).getVatTax().getPercentage()==0){
							pocentity.setTaxChargeAssesVal(priceqty);
						}
						
					}
					this.invoiceTaxTable.getDataprovider().getList().add(pocentity);
				}
			}
		}
		
	}

	public int checkTaxPercent(double taxValue,String taxName)
	{
		List<ContractCharges> taxesList=this.invoiceTaxTable.getDataprovider().getList();
		for(int i=0;i<taxesList.size();i++)
		{
			double listval=taxesList.get(i).getTaxChargePercent();
			int taxval=(int)listval;
			
			if(taxName.equals("Service")){
				if(taxValue==taxesList.get(i).getTaxChargePercent()&&taxval!=0&&taxesList.get(i).getTaxChargeName().trim().equals("Service Tax")){
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getTaxChargeName().trim().equals("Service Tax")){
					return i;
				}
			}
			if(taxName.equals("VAT")){
				if(taxValue==taxesList.get(i).getTaxChargePercent()&&taxval!=0&&taxesList.get(i).getTaxChargeName().trim().equals("VAT")){
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getTaxChargeName().trim().equals("VAT")){
					return i;
				}
			}
			
		//  rohan added this for GST
			if(taxName.equals("CGST")){
				if(taxValue==taxesList.get(i).getTaxChargePercent()&&taxval!=0&&taxesList.get(i).getTaxChargeName().trim().equals("CGST")){
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getTaxChargeName().trim().equals("CGST")){
					return i;
				}
			}
			
		//  rohan added this for GST
			if(taxName.equals("SGST")){
				if(taxValue==taxesList.get(i).getTaxChargePercent()&&taxval!=0&&taxesList.get(i).getTaxChargeName().trim().equals("SGST")){
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getTaxChargeName().trim().equals("SGST")){
					return i;
				}
			}
			
		//  rohan added this for GST
			if(taxName.equals("IGST")){
				if(taxValue==taxesList.get(i).getTaxChargePercent()&&taxval!=0&&taxesList.get(i).getTaxChargeName().trim().equals("IGST")){
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getTaxChargeName().trim().equals("IGST")){
					return i;
				}
			}
		}
		return -1;
	}

//	public DoubleBox getDiscount() {
//		return discount;
//	}
//
//	public void setDiscount(DoubleBox discount) {
//		this.discount = discount;
//	}

	public DoubleBox getNetPayable() {
		return netPayable;
	}

	public void setNetPayable(DoubleBox netPayable) {
		this.netPayable = netPayable;
	}

	public DateBox getDbbillingperiodFromDate() {
		return dbbillingperiodFromDate;
	}

	public void setDbbillingperiodFromDate(DateBox dbbillingperiodFromDate) {
		this.dbbillingperiodFromDate = dbbillingperiodFromDate;
	}
	public DateBox getDbbillingperiodToDate() {
		return dbbillingperiodToDate;
	}

	public void setDbbillingperiodToDate(DateBox dbbillingperiodToDate) {
		this.dbbillingperiodToDate = dbbillingperiodToDate;
	}
	/*
	 *	nidhi
	 *	1-07-2017 
	 * 
	 */
	

	public TextBox getTbSegment() {
		return tbSegment;
	}

	public void setTbSegment(TextBox tbSegment) {
		this.tbSegment = tbSegment;
	}
	
	/*
	 *  end
	 */
	/*
	 *  nidhi
	 *  20-07-2017
	 *  
	 */
	
	public DoubleBox getQuantity() {
		return quantity;
	}

	public void setQuantity(DoubleBox quantity) {
		this.quantity = quantity;
	}

	public ObjectListBox<Config> getOlbUOM() {
		return olbUOM;
	}

	public void setOlbUOM(ObjectListBox<Config> olbUOM) {
		this.olbUOM = olbUOM;
	}
	
	/*
	 *  end
	 */
	
	public DoubleBox getDbDiscountAmt() {
		return dbDiscountAmt;
	}
	
	public void setDbDiscountAmt(DoubleBox dbDiscountAmt) {
		this.dbDiscountAmt = dbDiscountAmt;
	}


	public DoubleBox getDbFinalTotalAmt() {
		return dbFinalTotalAmt;
	}


	public void setDbFinalTotalAmt(DoubleBox dbFinalTotalAmt) {
		this.dbFinalTotalAmt = dbFinalTotalAmt;
	}

	public DoubleBox getDbtotalAmt() {
		return dbtotalAmt;
	}

	public void setDbtotalAmt(DoubleBox dbtotalAmt) {
		this.dbtotalAmt = dbtotalAmt;
	}

	
	public DoubleBox getDbtotalAmtIncludingTax() {
		return dbtotalAmtIncludingTax;
	}

	public void setDbtotalAmtIncludingTax(DoubleBox dbtotalAmtIncludingTax) {
		this.dbtotalAmtIncludingTax = dbtotalAmtIncludingTax;
	}
	
	
	public TextBox getTbroundOffAmt() {
		return tbroundOffAmt;
	}
	public void setTbroundOffAmt(TextBox tbroundOffAmt) {
		this.tbroundOffAmt = tbroundOffAmt;
	}
	
	
	
	
	public TextBox getTbpaytermscomment() {
		return tbpaytermscomment;
	}

	public void setTbpaytermscomment(TextBox tbpaytermscomment) {
		this.tbpaytermscomment = tbpaytermscomment;
	}

	public ObjectListBox<CompanyPayment> getObjPaymentMode() {
		return objPaymentMode;
	}

	public void setObjPaymentMode(ObjectListBox<CompanyPayment> objPaymentMode) {
		this.objPaymentMode = objPaymentMode;
	}

	public TextBox getTbPaymentName() {
		return tbPaymentName;
	}

	public void setTbPaymentName(TextBox tbPaymentName) {
		this.tbPaymentName = tbPaymentName;
	}

	public TextBox getTbPaymentBankAC() {
		return tbPaymentBankAC;
	}

	public void setTbPaymentBankAC(TextBox tbPaymentBankAC) {
		this.tbPaymentBankAC = tbPaymentBankAC;
	}

	public TextBox getTbPaymentPayableAt() {
		return tbPaymentPayableAt;
	}

	public void setTbPaymentPayableAt(TextBox tbPaymentPayableAt) {
		this.tbPaymentPayableAt = tbPaymentPayableAt;
	}

	public TextBox getTbPaymentAccountName() {
		return tbPaymentAccountName;
	}

	public void setTbPaymentAccountName(TextBox tbPaymentAccountName) {
		this.tbPaymentAccountName = tbPaymentAccountName;
	}

	@Override
	public void onClick(ClickEvent event) {
		/**
		 * Date : 21-09-2017 By ANIL
		 */
			
		if(event.getSource().equals(addOthrChargesBtn)){
			Tax tax1=new Tax();
			Tax tax2=new Tax();
			OtherCharges prodCharges=new OtherCharges();
			prodCharges.setOtherChargeName("");
			prodCharges.setAmount(0);
			prodCharges.setTax1(tax1);
			prodCharges.setTax2(tax2);
			this.tblOtherCharges.getDataprovider().getList().add(prodCharges);
		}
		/**
		 * End
		 */
		
		if(event.getSource().equals(baddproducts)){
			
			if(validateprod()){
				this.ProdctType(this.prodInfoComposite.getProdCode().getValue().trim(),this.prodInfoComposite.getProdID().getValue().trim());
				prodInfoComposite.clear();

			}
			
		}	
	}
	

	private boolean validateprod() {
		
		if(personInfoComposite.getId().getValue().equals("")){
			showDialogMessage("Please add customer information!");
			return false;
		}
		if(olbbranch.getSelectedIndex()==0){
			showDialogMessage("Branch is mandatory");
			return false;
		}
		if(olbApproverName.getSelectedIndex()==0){
			showDialogMessage("Approver Name is mandatory");
			return false;
		}
		if(dbinvoicedate.getValue()==null){
			showDialogMessage("Invoice Date is mandatory");
			return false;
		}
		if(dbpaymentdate.getValue()==null){
			showDialogMessage("Payment Date is mandatory");
			return false;
		}
		
		return true;
	}

	/**
	 * Date : 15-09-2017 BY ANIL
	 * Adding calculation part for other charges
	 */
	public void addOtherChargesInTaxTbl(){
		List<ContractCharges> taxList=this.invoiceTaxTable.getDataprovider().getList();
		for(OtherCharges otherCharges:tblOtherCharges.getDataprovider().getList()){
			/**
			 * If GST tax is not applicable then we will consider tax1 as vat tax field and tax2 as service tax field.
			 */
			if(otherCharges.getTax1().getTaxPrintName()!=null&&!otherCharges.getTax1().getTaxPrintName().equals("")){
				boolean updateTaxFlag=true;
				if(otherCharges.getTax1().getTaxPrintName().equalsIgnoreCase("SELECT")){
					updateTaxFlag=false;
//					return;
				}
				if(otherCharges.getTax1().getPercentage()==0){
					updateTaxFlag=false;
//					return;
				}
				
				if(updateTaxFlag){
				ContractCharges pocentity=new ContractCharges();
				pocentity.setTaxChargeName(otherCharges.getTax1().getTaxPrintName());
				pocentity.setTaxChargePercent(otherCharges.getTax1().getPercentage());
//				pocentity.setIndexCheck(i+1);
				int indexValue=this.checkTaxPercent(otherCharges.getTax1().getPercentage(),otherCharges.getTax1().getTaxPrintName());
				if(indexValue!=-1){
					pocentity.setTaxChargeAssesVal(otherCharges.getAmount()+taxList.get(indexValue).getTaxChargeAssesVal());
					this.invoiceTaxTable.getDataprovider().getList().remove(indexValue);
				}
				if(indexValue==-1){
					pocentity.setTaxChargeAssesVal(otherCharges.getAmount());
				}
				this.invoiceTaxTable.getDataprovider().getList().add(pocentity);
				}
			}else{
				boolean updateTaxFlag=true;
				if(otherCharges.getTax1().getPercentage()==0){
					updateTaxFlag=false;
//					return;
				}
				if(updateTaxFlag){
				ContractCharges pocentity=new ContractCharges();
				pocentity.setTaxChargeName("VAT");
				pocentity.setTaxChargePercent(otherCharges.getTax1().getPercentage());
//				pocentity.setIndexCheck(i+1);
				int indexValue=this.checkTaxPercent(otherCharges.getTax1().getPercentage(),"VAT");
				if(indexValue!=-1){
					pocentity.setTaxChargeAssesVal(otherCharges.getAmount()+taxList.get(indexValue).getTaxChargeAssesVal());
					this.invoiceTaxTable.getDataprovider().getList().remove(indexValue);
				}
				if(indexValue==-1){
					pocentity.setTaxChargeAssesVal(otherCharges.getAmount());
				}
				this.invoiceTaxTable.getDataprovider().getList().add(pocentity);
				}
			}
			
			if(otherCharges.getTax2().getTaxPrintName()!=null&&!otherCharges.getTax2().getTaxPrintName().equals("")){
				System.out.println("INSIDE GST TAX PRINT NAME : "+otherCharges.getTax2().getTaxPrintName());
				boolean updateTaxFlag=true;
				if(otherCharges.getTax2().getTaxPrintName().equalsIgnoreCase("SELECT")){
					updateTaxFlag=false;
//					return;
				}
				if(otherCharges.getTax2().getPercentage()==0){
					updateTaxFlag=false;
//					return;
				}
				if(updateTaxFlag){
				ContractCharges pocentity=new ContractCharges();
				pocentity.setTaxChargeName(otherCharges.getTax2().getTaxPrintName());
				pocentity.setTaxChargePercent(otherCharges.getTax2().getPercentage());
//				pocentity.setIndexCheck(i+1);
				int indexValue=this.checkTaxPercent(otherCharges.getTax2().getPercentage(),otherCharges.getTax2().getTaxPrintName());
				if(indexValue!=-1){
					pocentity.setTaxChargeAssesVal(otherCharges.getAmount()+taxList.get(indexValue).getTaxChargeAssesVal());
					this.invoiceTaxTable.getDataprovider().getList().remove(indexValue);
				}
				if(indexValue==-1){
					pocentity.setTaxChargeAssesVal(otherCharges.getAmount());
				}
				this.invoiceTaxTable.getDataprovider().getList().add(pocentity);
				}
			}else{
				boolean updateTaxFlag=true;
				System.out.println("ST OR NON GST");
				if(otherCharges.getTax2().getPercentage()==0){
					updateTaxFlag=false;
//					return;
				}
				if(otherCharges.getTax1().getTaxPrintName()!=null&&!otherCharges.getTax1().getTaxPrintName().equals("")){
					updateTaxFlag=false;
//					return;
				}
				if(updateTaxFlag){
				ContractCharges pocentity=new ContractCharges();
				pocentity.setTaxChargeName("Service Tax");
				pocentity.setTaxChargePercent(otherCharges.getTax2().getPercentage());
//				pocentity.setIndexCheck(i+1);
				int indexValue=this.checkTaxPercent(otherCharges.getTax2().getPercentage(),"Service");
				if(indexValue!=-1){
				 	double assessValue=otherCharges.getAmount()+(otherCharges.getAmount()*otherCharges.getTax1().getPercentage()/100);
					pocentity.setTaxChargeAssesVal(assessValue+taxList.get(indexValue).getTaxChargeAssesVal());
					this.invoiceTaxTable.getDataprovider().getList().remove(indexValue);
				}
				if(indexValue==-1){
					double assessValue=otherCharges.getAmount()+(otherCharges.getAmount()*otherCharges.getTax1().getPercentage()/100);
					pocentity.setTaxChargeAssesVal(assessValue);
				}
				this.invoiceTaxTable.getDataprovider().getList().add(pocentity);
				}
			}
		}
	}
	
	
	/**
	 * Date :23-09-2017 BY ANIL
	 * This method checks whether we should apply discount or not
	 * if taxes applied on all products then we can give discount
	 */
	public boolean isValidForDiscount(){
		String tax1="",tax2="";
		if(salesProductTable.getDataprovider().getList().size()==0){
			return false;
		}
		tax1=salesProductTable.getDataprovider().getList().get(0).getVatTax().getTaxConfigName().trim();
		tax2=salesProductTable.getDataprovider().getList().get(0).getServiceTax().getTaxConfigName().trim();
		for(SalesOrderProductLineItem obj:salesProductTable.getDataprovider().getList()){
			if(!obj.getVatTax().getTaxConfigName().trim().equals(tax1)
					||!obj.getServiceTax().getTaxConfigName().trim().equals(tax2)){
				showDialogMessage("Discount is applicable only if same taxes are applied to all products.");
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * End
	 */
	
	/**
	 * @param invoiceCount
	 * Date : 21-11-2017 BY ANIL
	 * This method is used to check the status of accounting interface and customer payment created against 
	 * the invoice while updating the invoice after its approval.
	 */
	private void checkForPaymentAndAccountingInterface(final int invoiceCount) {
		custPayment=null;
		updatePaymentFlag=false;
		
		GenricServiceAsync async=GWT.create(GenricService.class);
		MyQuerry query=new MyQuerry();
		Filter filter=null;
		
		filter=new Filter();
		filter.setQuerryString("documentID");
		filter.setIntValue(invoiceCount);
		query.getFilters().add(filter);
		
		filter=new Filter();
		filter.setQuerryString("documentType");
		filter.setStringValue("Invoice");
		query.getFilters().add(filter);
		
		
		
		
		
		query.setQuerryObject(new AccountingInterface());
		async.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				
			}
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				final int size = result.size();
//				if(result.size()!=0){
//					showDialogMessage("Please do changes in tally manually as accounting interface is already generated against this invoice.");
//				}
			//	else{
					/////////////////////////////
					
					GenricServiceAsync async=GWT.create(GenricService.class);
					MyQuerry query=new MyQuerry();
					Filter filter=null;
					
					filter=new Filter();
					filter.setQuerryString("invoiceCount");
					filter.setIntValue(invoiceCount);
					query.getFilters().add(filter);
					/**Date 26-9-2020 by Amol added a Acc Type Querry**/
					
					filter=new Filter();
					filter.setQuerryString("accountType");
					filter.setStringValue("AR");
					query.getFilters().add(filter);
					
					query.setQuerryObject(new CustomerPayment());
					async.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
						@Override
						public void onFailure(Throwable caught) {
							
						}
						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							Console.log("result size in customer oayment "+result.size());
							if(result.size()>1){
								for(SuperModel model:result){
									CustomerPayment payment=(CustomerPayment) model;
									if(payment.getStatus().equals(CustomerPayment.CREATED)||payment.getStatus().equals(CustomerPayment.CLOSED)){
										showDialogMessage("Please first cancel the payments created against the invoice.");
										return;
									}
								}
								custPayment=null;
								updatePaymentFlag=true;
								setFormToEditState();
								
							}else if(result.size()==1){
								CustomerPayment payment=(CustomerPayment) result.get(0);
								if(payment.getStatus().equals(CustomerPayment.CLOSED)){
									showDialogMessage("Please first cancel the payment created against the invoice.");
								    return;
								}else if(payment.getStatus().equals(CustomerPayment.CREATED)){
									custPayment=payment;
									updatePaymentFlag=true;
									setFormToEditState();
								}else if(payment.getStatus().equals(CustomerPayment.CANCELLED)){
									custPayment=null;
									updatePaymentFlag=true;
									setFormToEditState();
								}
							}
							if(size!=0){
								showDialogMessage("Please click on 'Update Accounting Interface' button to update accounting interface.");
							}
						}
						
					});
					
					/////////////////////////
			//	}
			}
		});
	}
	
	public void setFormToEditState(){
		super.setToEditState();
		this.processLevelBar.setVisibleFalse(false);
		toggleHeaderBar();
		if (multipleBillInvoice == true) {
			conStartDur.setEnabled(true);
			conEndDur.setEnabled(true);
		}
		if(invPrefFlag){
			tbInvoiceRefNumber.setEnabled(false);
		}
	}
	/**
	 * End
	 */
	public  boolean validateunitandUOM(){
		/**
		 *  nidhi
		 *  30-03-2018
		 *  for make unit and UOm is mandatory
		 */
		boolean validateFlag = AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "MakeQty&UomMandatory");
		boolean unitFlag = true;
		if(validateFlag){
			if(tbrateContractServiceid.getValue() != null && tbrateContractServiceid.getValue().trim().matches("[0-9]*")  && !tbrateContractServiceid.getValue().trim().equals("")){
				if((quantity.getValue()==null || quantity.getValue()<=0) && olbUOM.getSelectedIndex()==0){
					showDialogMessage("Quantity and UOM is Mandatory!");	
					unitFlag = false;
				}else if(quantity.getValue()==null || quantity.getValue()<=0){
					showDialogMessage("Quantity is Mandatory!");	
					unitFlag = false;
				}else if(olbUOM.getSelectedIndex()==0){
					showDialogMessage("UOM is Mandatory!");	
					unitFlag = false;
				}
			}
		}
		/**
		 * end
		 */
		
		/**
		 * Date 12-10-2018 By Vijay
		 * Des :- Validation :- Billing Category Type Group if Mandatory with process config then must check when we sending Request For Approval
		 */
		boolean validateCategoryFlag = AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "MakeInvoiceCategoryMandatory");
		boolean validateGroupFlag = AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "MakeInvoiceGroupMandatory");
		boolean validateTypeFlag = AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "MakeInvoiceTypeMandatory");

		if(validateCategoryFlag){
			if(olbInvoiceCategory.getSelectedIndex()==0){
				showDialogMessage("Category is Mandatory!");	
				unitFlag = false;
			}
		}
		else if(validateTypeFlag){
			if(olbInvoiceConfigType.getSelectedIndex()==0){
				showDialogMessage("Type is Mandatory!");	
				unitFlag = false;
			}
		}
		else{
			if(validateGroupFlag){
				if(olbinvoicegroup.getSelectedIndex()==0){
					showDialogMessage("Group is Mandatory!");	
					unitFlag = false;
				}
			}
		}
		/**
		 * ends here
		 */
		return unitFlag;
	}
	/**
	 * End
	 */
	void loadPaymentMode(){
		try {
			 GenricServiceAsync async =  GWT.create(GenricService.class);

			MyQuerry querry = new MyQuerry();
			
			
			Company c = new Company();
			Vector<Filter> filtervec = new Vector<Filter>();
			Filter filter = null;
			
			filter = new Filter();
			filter.setQuerryString("companyId");
			filter.setLongValue(c.getCompanyId());
			filtervec.add(filter);
			
			querry.setFilters(filtervec);
			
			
			querry.setQuerryObject(new CompanyPayment());

			async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					// TODO Auto-generated method stub

					System.out.println(" in side date load size"+result.size());
					Console.log("get result size of payment mode --" + result.size());
					objPaymentMode.clear();
					objPaymentMode.addItem("--SELECT--");
//					paymentModeList = new ArrayList<CompanyPayment>();
					if (result.size() == 0) {

					} else {
						for (SuperModel model : result) {
							CompanyPayment payList = (CompanyPayment) model;
							paymentModeList.add(payList);
						}
						
						if (paymentModeList.size() != 0) {
							objPaymentMode.setListItems(paymentModeList);
						}
						
						for(int i=1;i<objPaymentMode.getItemCount();i++){
							String paymentDet = objPaymentMode.getItemText(i);
							Console.log("SERVICE DETAILS : "+paymentDet);
						}	
					}
				
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					
				}
			});
			

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void setBankSelectedDetails(){
		if(getObjPaymentMode().getSelectedIndex()!=0){
			CompanyPayment pay = (CompanyPayment) getObjPaymentMode().getSelectedItem();
			getTbPaymentName().setValue(pay.getPaymentBankName());
			getTbPaymentBankAC().setValue(pay.getPaymentAccountNo());
			getTbPaymentPayableAt().setValue(pay.getPaymentPayableAt());
			getTbPaymentAccountName().setValue(pay.getPaymentBranch());
		}else{
			getTbPaymentName().setValue("");
			getTbPaymentBankAC().setValue("");
			getTbPaymentPayableAt().setValue("");
			getTbPaymentAccountName().setValue("");
		}
	}

	public DoubleBox getTbtdsPerValue() {
		return tbtdsPerValue;
	}

	public void setTbtdsPerValue(DoubleBox tbtdsPerValue) {
		this.tbtdsPerValue = tbtdsPerValue;
	}

	public DoubleBox getDbtdsValue() {
		return dbtdsValue;
	}

	public void setDbtdsValue(DoubleBox dbtdsValue) {
		this.dbtdsValue = dbtdsValue;
	}

	public CheckBox getCbtdsApplicable() {
		return cbtdsApplicable;
	}

	public void setCbtdsApplicable(CheckBox cbtdsApplicable) {
		this.cbtdsApplicable = cbtdsApplicable;
	}

	public TextBox getTbInvoiceRefNumber() {
		return tbInvoiceRefNumber;
	}

	public void setTbInvoiceRefNumber(TextBox tbInvoiceRefNumber) {
		this.tbInvoiceRefNumber = tbInvoiceRefNumber;
	}
	/**
	 * Updated By:Viraj
	 * Date: 06-07-2019
	 * Description: To map gstin no 
	 */

	public TextBox getTbGstinNumber() {
		return tbGstinNumber;
	}

	public void setTbGstinNumber(TextBox tbGstinNumber) {
		this.tbGstinNumber = tbGstinNumber;
	}
	/** Ends **/
		public OtherChargesTable getTblOtherCharges() {
		return tblOtherCharges;
	}

	public void setTblOtherCharges(OtherChargesTable tblOtherCharges) {
		this.tblOtherCharges = tblOtherCharges;
	}

	@Override
	public void onValueChange(ValueChangeEvent<Date> event) {
		// TODO Auto-generated method stub
		if(event.getSource().equals(dbinvoicedate)){
			reactonChangeInvoiceDate();
		}
		if(event.getSource().equals(dbDispatchDate)){
			if(dbDispatchDate.getValue()==null){
				reactonChangeInvoiceDate();
			}
			else{
				reactonChangeDispathcDate();
			}
		}

		
	}

	private void reactonChangeDispathcDate() {
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "AddCreditPeriodAndDispatchDate")){
			int creditPeriod = ibcreditPeriod.getValue();
			if(dbDispatchDate.getValue()!=null){
				Date dispatchDate = dbDispatchDate.getValue();
				CalendarUtil.addDaysToDate(dispatchDate, creditPeriod);
				dbpaymentdate.setValue(dispatchDate);
			}
			else{
				Date invoiceDate = dbinvoicedate.getValue();
				CalendarUtil.addDaysToDate(invoiceDate, creditPeriod);
				dbpaymentdate.setValue(invoiceDate);
			}
		}
		
		
	}

	private void reactonChangeInvoiceDate() {
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "AddCreditPeriodAndDispatchDate")){
			int creditPeriod = ibcreditPeriod.getValue();
			Date invoiceDate = dbinvoicedate.getValue();
			if(dbDispatchDate.getValue()==null){
				CalendarUtil.addDaysToDate(invoiceDate, creditPeriod);
				dbpaymentdate.setValue(invoiceDate);
			}
		}
	}
public void getInvoicePrefixDetails(String branchName) {
		Console.log("getInvoicePrefixDetails invPrefFlag "+invPrefFlag);
//		if(invPrefFlag){
		Console.log("getInvoicePrefixDetails");
		customernamechangeasync.getInvoicePrefixDetails(UserConfiguration.getCompanyId(), branchName,
			new AsyncCallback<String>() {

				@Override
				public void onSuccess(String result) {
					Console.log("in on success " + result);

					String str = ibinvoiceid.getValue();
					String invoiceNumber = "";
					Console.log("invoice id " + str);

					int count=0;
					for (int j = 4; j < str.length(); j++) {
					
						  if(str.charAt(j)=='0'){
						    
						  }else{
						    for(int k = j; k < str.length(); k++){
						      invoiceNumber+=str.charAt(k);
						      count=k;
						    }
						    j=count;
						  }
						}
					Console.log("invoiceNumber valuee " + invoiceNumber);
					String prefixValue = result + invoiceNumber;
					Console.log("Prefix valuee " + prefixValue);
					tbInvoiceRefNumber.setValue(prefixValue);

				}

				@Override
				public void onFailure(Throwable caught) {
					System.out.println("falied");
					showDialogMessage(caught.getMessage());
					hideWaitSymbol();
				}
			});
//		}
	}

@Override
public void refreshTableData() {
	// TODO Auto-generated method stub
	super.refreshTableData();
	
	tblOtherCharges.getTable().redraw();
	billingdoctable.getTable().redraw();
	salesProductTable.getTable().redraw();
	invoiceTaxTable.getTable().redraw();
	invoiceChargesTable.getTable().redraw();
}
	
		private void getcustomerBillingAddress() {
		
			showWaitSymbol();
			
			GenricServiceAsync service=GWT.create(GenricService.class);
		
			MyQuerry querry = new MyQuerry();
		  	Vector<Filter> filtervec=new Vector<Filter>();
		  	Filter filter = null;
		  
//			int customerid = Integer.parseInt(tbpersoncount.getValue());
			int customerid = personInfoComposite.getIdValue();

			filter = new Filter();
			filter.setQuerryString("count");
			filter.setIntValue(customerid);
			
			filtervec.add(filter);
			
			querry.setFilters(filtervec);
			querry.setQuerryObject(new Customer());
			service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					// TODO Auto-generated method stub
					for(SuperModel model  : result){
						Customer customerEntity = (Customer) model;
						reactonUpdateTaxeDetails(customerEntity.getAdress().getState());
						break;
					}
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					
				}
			});
		}

	private void reactonUpdateTaxeDetails(String customerBillingAddressState) {

		Console.log("inside update tax method");
		List<SalesOrderProductLineItem> prodlist = salesProductTable.getDataprovider().getList();
		if(prodlist.size()>0){
			Branch branchEntity =olbbranch.getSelectedItem();
			
			boolean gstApplicable = false;
			Invoice invoiceEntity = (Invoice) this.getPresenter().getModel();
			String numberRange="";
			if(invoiceEntity!=null && invoiceEntity.getNumberRange()!=null && !invoiceEntity.getNumberRange().equals("")){
				numberRange = invoiceEntity.getNumberRange();
			}
			else{
				gstApplicable = true;
			}
			if(!numberRange.equals("")){
				ArrayList<Config> listConfig=LoginPresenter.globalConfig;
				for(Config confiEntity : listConfig){
					if(confiEntity.getType()==91 && confiEntity.getName().trim().equals(numberRange)){
						gstApplicable = confiEntity.isGstApplicable();
					}
				}
			}
			

			
			System.out.println("gstApplicable"+gstApplicable);
				System.out.println("customer onselection");
				Console.log("update taxes method");
				 List<SalesOrderProductLineItem> productlist = AppUtility.updateBillingTaxesDetails(prodlist, customerBillingAddressState, branchEntity,gstApplicable);
				 salesProductTable.getDataprovider().setList(productlist);
				 salesProductTable.getTable().redraw();
				 RowCountChangeEvent.fire(salesProductTable.getTable(), salesProductTable.getDataprovider().getList().size(), true);
				 setEnable(true);
		}
		
		/**
		 * @author Vijay Date - 22-09-2023
		 * Des :- added to hold screen to update tax details properly
		 * Bug - Orion when they chnage branch and save invoice tax details not updating properly
		 */
		Timer timer = new Timer() {
			
			@Override
			public void run() {

				hideWaitSymbol();

			}
		};
		timer.schedule(3000);
		/**
		 * ends here
		 */
	}

	
	public void ProdctType(String pcode,String productId)
	{
		final GenricServiceAsync genasync=GWT.create(GenricService.class);
		
		int prodId=Integer.parseInt(productId);
		
		Vector<Filter> filtervec=new Vector<Filter>();
		final MyQuerry querry=new MyQuerry();
		
		Filter filter=new Filter();
		filter.setQuerryString("productCode");
		filter.setStringValue(pcode.trim());
		filtervec.add(filter);
		
		filter=new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(prodId);
		filtervec.add(filter);
		
		filter=new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new SuperProduct());
		showWaitSymbol();
		 Timer timer=new Timer() 
    	 {
				@Override
				public void run() {
			genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onFailure(Throwable caught) {
					showDialogMessage("An Unexpected error occurred!");
				}
	
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					
					if(result.size()==0)
					{
						showDialogMessage("Please check whether product status is active or not!");
					}
					
					int productsrNo = getproductSRNo(salesProductTable.getDataprovider().getList());
					
					for(SuperModel model:result)
					{
						productsrNo++;
						SuperProduct superProdEntity = (SuperProduct)model;
						SalesOrderProductLineItem productlineItem=new SalesOrderProductLineItem();
						productlineItem.setProdId(superProdEntity.getCount());
						productlineItem.setProdCategory(superProdEntity.getProductCategory());
						productlineItem.setProdCode(superProdEntity.getProductCode());
						productlineItem.setProdName(superProdEntity.getProductName());
						productlineItem.setQuantity(1d);
						productlineItem.setOrderDuration(0);
						productlineItem.setOrderServices(0);
						productlineItem.setPrduct(superProdEntity);
						productlineItem.setPrice(superProdEntity.getPrice());
						productlineItem.setTotalAmount(superProdEntity.getPrice());
						productlineItem.setVatTax(superProdEntity.getVatTax());
						productlineItem.setServiceTax(superProdEntity.getServiceTax());
						productlineItem.setUnitOfMeasurement(superProdEntity.getUnitOfMeasurement());
						productlineItem.setProdDesc1(superProdEntity.getCommentdesc1());
						productlineItem.setProdDesc2(superProdEntity.getCommentdesc2());
						productlineItem.setBaseBillingAmount(superProdEntity.getPrice());
						productlineItem.setHsnCode(superProdEntity.getHsnNumber());
						productlineItem.setPaymentPercent(100.0);
						productlineItem.setBasePaymentAmount(superProdEntity.getPrice());
						productlineItem.setProductSrNumber(productsrNo);
						productlineItem.setPrduct(superProdEntity);
						productlineItem.setTypeOfOrder(AppConstants.ORDERTYPESALES);
						if(superProdEntity.getHsnNumber()!=null)
						productlineItem.setHsnCode(superProdEntity.getHsnNumber());
						salesProductTable.getDataprovider().getList().add(productlineItem);

					}

					
					}

			
				 });
				hideWaitSymbol();
 				}
	 	  };
          timer.schedule(1000); 
	}

	
	private int getproductSRNo(List<SalesOrderProductLineItem> list) {
		int productSrNo = 0;
		for(SalesOrderProductLineItem prodctline : list){
		
			if(prodctline.getProductSrNumber()>productSrNo){
				productSrNo = prodctline.getProductSrNumber();
			}
		}
		
		return productSrNo;
	}
	
	public PersonInfoComposite getPersonInfoComposite() {
		return personInfoComposite;
	}

	public void setPersonInfoComposite(PersonInfoComposite personInfoComposite) {
		this.personInfoComposite = personInfoComposite;
	}

//	public void loadCustomer(int customerId) {
//		
//		MyQuerry querry=new MyQuerry();
//		Vector<Filter> filtervec=new Vector<Filter>();
//		Filter temp=null;
//		
//		temp=new Filter();
//		temp.setQuerryString("count");
//		temp.setIntValue(customerId);
//		filtervec.add(temp);
//		
//		querry.setFilters(filtervec);
//		querry.setQuerryObject(new Customer());
//		
//		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
//			
//			@Override
//			public void onFailure(Throwable caught) {
//				showDialogMessage("An Unexpected error occurred!");
//			}
//
//			@Override
//			public void onSuccess(ArrayList<SuperModel> result) {
//					for(SuperModel model:result)
//					{
//						Customer custEntity = (Customer)model;
//						cust = custEntity;
//						
//					}
//				}
//			 });
//	}
	
	
	
}
