package com.slicktechnologies.client.views.paymentinfo.invoicedetails;

import java.util.ArrayList;
import java.util.Vector;
import java.util.logging.Level;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

public class CRMTallySearchPopUp extends PopupScreen implements SelectionHandler<Suggestion> {
	
	/** New popup for crm tally report **/
	protected GenricServiceAsync service=GWT.create(GenricService.class);
	final CsvServiceAsync csvservice=GWT.create(CsvService.class);
	public IntegerBox ibinvoiceid;
	public IntegerBox ibcontractid;
	public PersonInfoComposite personInfo;
	public PersonInfoComposite vendorInfo;
	public DateComparator dateComparator;
	public ObjectListBox<ConfigCategory> olbbillingcategory;
	public ObjectListBox<Type> olbbillingtype;
	public ObjectListBox<Config> olbbillinggroup;
	public ListBox lbstatus,lbinvoicetype;
	public ObjectListBox<Employee> olbApproverName;
	public ListBox lbaccountType;
	
	//*************rohan added here for search by branch ********8888
	public ObjectListBox<Branch> olbBranch;
	
	
	public ObjectListBox<ConfigCategory> olbCustomerCategory;
	public TextBox tbSegment;
	
	public IntegerBox ibrateContractServiceId;
	
	/*
	 *   end
	 */
	
	/**
	 * Date : 04-08-2017 By ANIL
	 */
	ObjectListBox<Config> olbinvoicegroup;
	/**
	 * ENd
	 */
	
	/**03-10-2017 sagar sore [ adding Number range field in Search pop up ]***/
	public ObjectListBox<Config> olbcNumberRange;
	
	/**
	 * Date : 18-11-2017 BY ANIL
	 */
	TextBox tbReferenceNumber;
	
	/** date 13.03.2018 added by komal for nbhc **/
	ObjectListBox<ConfigCategory> olbInvoiceCategory;
	ObjectListBox<Type> olbInvoiceConfigType;
    /**
     * end komal
     */
	/** date 20.10.2018 added by komal**/
	ObjectListBox<String> olbTypeOforder;
	ObjectListBox<String> olbSubBillType;
	
	MyQuerry myQuerryStatic;
//	ArrayList<Invoice> invoices;
	
	 /***
	  * Date 03-04-2019 by Vijay
	  * Des :- Cancellation contract to search
	  */
	 public DateComparator cancellationDateComparator;		

	 protected GeneralServiceAsync general = GWT.create(GeneralService.class);
	
	 /**@author Sheetal: 25-05-2022
	  * Des : Adding radio buttons for downloading report in CRM or Oracle format**/
	 
	 public RadioButton rbCRM,rbOracleInCSV,rbOracleInExcel;
	 
	public CRMTallySearchPopUp() {
		super();
		createGui();
		rbCRM.setValue(true);
	}
	
//	public CRMTallySearchPopUp(boolean b) {
//		super(b);
//		createGui();
//	}
	
	public void initWidget()
	{
		ibinvoiceid=new IntegerBox();
		ibcontractid=new IntegerBox();
		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new Customer());
		personInfo=new PersonInfoComposite(querry,false);
		
		//***********rohan changes here *******************
		
		olbBranch= new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbBranch);
		
		//*****************changes ends here ************
		
		MyQuerry vendorquerry=new MyQuerry();
		vendorquerry.setQuerryObject(new Vendor());
		vendorInfo=new PersonInfoComposite(vendorquerry,false);
		
		
		dateComparator=new DateComparator("invoiceDate",new Invoice());
		lbstatus= new ListBox();
		AppUtility.setStatusListBox(lbstatus,Invoice.getStatusList());
		lbinvoicetype=new ListBox();
		lbinvoicetype.addItem("SELECT");
		lbinvoicetype.addItem(AppConstants.CREATEPROFORMAINVOICE);
		lbinvoicetype.addItem(AppConstants.CREATETAXINVOICE);
		olbApproverName=new ObjectListBox<Employee>();
		AppUtility.makeSalesPersonListBoxLive(olbApproverName);
		
		lbaccountType=new ListBox();
		lbaccountType.addItem("SELECT");
		lbaccountType.addItem(AppConstants.BILLINGACCOUNTTYPEAP);
		lbaccountType.addItem(AppConstants.BILLINGACCOUNTTYPEAR);
		
		
		personInfo.getId().addSelectionHandler(this);
		vendorInfo.getId().addSelectionHandler(this);
		
		personInfo.getCustomerId().getHeaderLabel().setText("Customer ID");
		personInfo.getCustomerName().getHeaderLabel().setText("Customer Name");
		personInfo.getCustomerCell().getHeaderLabel().setText("Customer Cell");
		
		vendorInfo.getCustomerId().getHeaderLabel().setText("Vendor ID");
		vendorInfo.getCustomerName().getHeaderLabel().setText("Vendor Name");
		vendorInfo.getCustomerCell().getHeaderLabel().setText("Vendor Cell");
		
		/*
		 * 	nidhi
		 *  30-06-2017
		 *  for search
		 */		
		olbCustomerCategory= new ObjectListBox<ConfigCategory>();
//		AppUtility.makeTypeListBoxLive(olbCustomerCategory, Screen.CONTRACTTYPE);
		AppUtility.MakeLiveCategoryConfig(olbCustomerCategory, Screen.CUSTOMERCATEGORY);
				
		tbSegment = new TextBox();
		/*   end  */
		
		
		/*  nidhi  */
		ibrateContractServiceId = new IntegerBox();
		/*  */
		
		
		olbinvoicegroup=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbinvoicegroup, Screen.INVOICEGROUP);
		
		/**03-10-2017 sagar sore[ olbcNumberRange initialization, Displaying values entered from process configuration]***/
		olbcNumberRange= new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbcNumberRange,Screen.NUMBERRANGE);
		
		tbReferenceNumber=new TextBox();
		
		/** Date 13.03.2018 added by komal for nbhc **/
		olbInvoiceCategory=new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(olbInvoiceCategory, Screen.INVOICECATEGORY);
	//	olbInvoiceCategory.addChangeHandler(this);
		olbInvoiceConfigType=new ObjectListBox<Type>();
		AppUtility.makeTypeListBoxLive(olbInvoiceConfigType, Screen.INVOICECONFIGTYPE);
        /** 
         * end komal
         */
		/** date 20.10.2018 added by komal**/
		olbTypeOforder = new ObjectListBox<String>();
		olbTypeOforder.addItem("--SELECT--");
		olbTypeOforder.addItem(AppConstants.ORDERTYPESALES);
		olbTypeOforder.addItem(AppConstants.ORDERTYPESERVICE);
		olbTypeOforder.addItem(AppConstants.ORDERTYPEPURCHASE);
		
		olbSubBillType = new ObjectListBox<String>();
		AppUtility.setStatusListBox(olbSubBillType, Invoice.getSubBillTypeList());
//		olbSubBillType.addItem("--SELECT--");
//		olbSubBillType.addItem(AppConstants.STAFFINGANDRENTAL);
//		olbSubBillType.addItem(AppConstants.CONSUMABLES);
//		olbSubBillType.addItem(AppConstants.NATIONALHOLIDAY);
//		olbSubBillType.addItem(AppConstants.OVERTIME);
//		olbSubBillType.addItem(AppConstants.OTHERSERVICES);
		
		/*** Date 03-04-2019 by Vijay for cancelled contract search filter ****/
		cancellationDateComparator = new DateComparator("cancellationDate", new Invoice());
		
		rbCRM=new RadioButton("CRM Format");
		rbCRM.setValue(true);
		rbCRM.addClickHandler(this);
		
		rbOracleInCSV=new RadioButton("Oracle Format in Csv");
		rbOracleInCSV.addClickHandler(this);
		
		rbOracleInExcel=new RadioButton("Oracle Format in Excel");
		rbOracleInExcel.addClickHandler(this);
		
	}
	
	public void createScreen()
	{
		initWidget();
		FormFieldBuilder builder;
		builder = new FormFieldBuilder("Invoice ID",ibinvoiceid);
		FormField fibinvoiceid= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("",personInfo);
		FormField fPersonInfo= builder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		builder = new FormFieldBuilder("",vendorInfo);
		FormField fvendorInfo= builder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		builder = new FormFieldBuilder("Order ID",ibcontractid);
		FormField fibcontractid= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("From Date (Invoice Date)",dateComparator.getFromDate());
		FormField fdateComparatorfrom= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("To Date (Invoice Date)",dateComparator.getToDate());
		FormField fdateComparatorto= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Invoice Document Type",lbinvoicetype);
		FormField flbinvoicetype= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Status",lbstatus);
		FormField flbstatus=builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Approver Name",olbApproverName);
		FormField folbApproverName= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Account Type",lbaccountType);
		FormField flbaccountType= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Branch",olbBranch);
		FormField folbBranch= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/*
		 * 	nidhi
		 * 	30-6-2017
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "OnlyForNBHC"))
		{
			builder = new FormFieldBuilder("Segment Type",olbCustomerCategory);
			
		}
		else{
			builder = new FormFieldBuilder("Segment Type",tbSegment);
		}
		FormField ftbSagment= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/*
		 *  nidhi
		 *  17-07-2017
		 *  Service Id serach
		 */
		
		builder = new FormFieldBuilder("Service ID",ibrateContractServiceId);
		FormField fibserviceid= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		/*
		 *  end
		 */
		
		builder = new FormFieldBuilder("Invoice Group",olbinvoicegroup);
		FormField folbinvoicegroup=builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/**03-10-2017 sagar sore[ adding search by number range]**/
		FormField folbcNumberRange;	
			builder = new FormFieldBuilder("Number Range",olbcNumberRange);
			folbcNumberRange= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			
		/**
		 * Date : 18-11-2017 BY ANIL
		 */
		builder = new FormFieldBuilder("Reference Number",tbReferenceNumber);
		FormField ftbReferenceNumber= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		/**
		 * Date 25-01-2018 By Vijay 
		 * differentiated date combination search and other filter search
		 */
		builder = new FormFieldBuilder();
		FormField fgroupingDateFilterInformation=builder.setlabel("Date Filter Combination").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();

		builder = new FormFieldBuilder();
		FormField fgroupingOtherFilterInformation=builder.setlabel("Other Filter Combination").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		/**
		 * ends here
		 */
		/** date 13.03.2018 added by komal for nbhc**/
		builder = new FormFieldBuilder("Invoice Category",olbInvoiceCategory);
		FormField folbInvoiceCategory=builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Invoice Type",olbInvoiceConfigType);
		FormField folbInvoiceConfigType=builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/** date 20.10.2018 added by komal**/
		builder = new FormFieldBuilder("Type Of Order",olbTypeOforder);
		FormField folbTypeOforder= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Sub Bill Type",olbSubBillType);
		FormField folbSubBillType= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/**
		 * Date 03-04-2019 by Vijay for Cancelled records search
		 */
		builder = new FormFieldBuilder("From Date (Cancelled Date)",cancellationDateComparator.getFromDate());
		FormField fcancellationFrmDateComparator= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("To Date (Cancelled Date)",cancellationDateComparator.getToDate());
		FormField fcancellationToDateComparator= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/**@author Sheetal : 25-05-2022
		 * Des : Adding these fields for downloading CRM or Oracle report**/
		builder = new FormFieldBuilder("CRM Format",rbCRM);
		FormField frbCRM= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Oracle Format in Csv",rbOracleInCSV);
		FormField frbOracleInCSV= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Oracle format in EXCEL",rbOracleInExcel);
		FormField frbOracleInExcel= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/**
		 * Date 12-07-2018 By Vijay
		 * Des :- for NBHC CCPM need only limited search fields and else for all standard
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "OnlyForNBHC")){
			this.fields=new FormField[][]{
					{fgroupingDateFilterInformation},
					{fdateComparatorfrom,fdateComparatorto,folbBranch,flbstatus},
					{fcancellationFrmDateComparator,fcancellationToDateComparator,flbaccountType},
					{fgroupingOtherFilterInformation},
					{fibinvoiceid,folbinvoicegroup,folbInvoiceCategory , folbInvoiceConfigType},
					{flbinvoicetype,fibcontractid,fibserviceid},
				
					{fPersonInfo},
			};
		}
		else{
			
		this.fields=new FormField[][]{	
				{fgroupingDateFilterInformation},
				{fdateComparatorfrom,fdateComparatorto,folbBranch,flbstatus},
				{fcancellationFrmDateComparator,fcancellationToDateComparator,flbinvoicetype,flbaccountType}, //Added by Ashwini
				{fgroupingOtherFilterInformation},
				{fibinvoiceid,folbinvoicegroup,folbInvoiceCategory , folbInvoiceConfigType},
				{folbcNumberRange , folbApproverName,fibcontractid},
				{ftbReferenceNumber , ftbSagment,fibserviceid },
				{folbTypeOforder , folbSubBillType },
				{fPersonInfo},
				{fvendorInfo},
				{frbCRM,frbOracleInCSV,frbOracleInExcel}
				
		};
		
	  }
	}
	

	
	public MyQuerry getQuerry() {
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		
		if(dateComparator.getValue()!=null){
			filtervec.addAll(dateComparator.getValue());
		}
		
		if(ibinvoiceid.getValue()!=null){
			temp=new Filter();
			temp.setIntValue(ibinvoiceid.getValue());
			filtervec.add(temp);
			temp.setQuerryString("count");
			filtervec.add(temp);
		}
		
		if(ibcontractid.getValue()!=null){
			temp=new Filter();
			temp.setIntValue(ibcontractid.getValue());
			filtervec.add(temp);
			temp.setQuerryString("contractCount");
			filtervec.add(temp);
		}
		
		if(lbaccountType.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(lbaccountType.getItemText(lbaccountType.getSelectedIndex()).trim());
			temp.setQuerryString("accountType");
			filtervec.add(temp);
		}
		
		if(lbinvoicetype.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(lbinvoicetype.getItemText(lbinvoicetype.getSelectedIndex()));
			filtervec.add(temp);
			temp.setQuerryString("invoiceType");
			filtervec.add(temp);
		}
		
		if(personInfo.getIdValue()!=-1)
		  {
		  temp=new Filter();
		  temp.setIntValue(personInfo.getIdValue());
		  temp.setQuerryString("personInfo.count");
		  filtervec.add(temp);
		  }
		  
//		  if(!(personInfo.getFullNameValue().equals("")))
//		  {
//		  temp=new Filter();
//		  temp.setStringValue(personInfo.getFullNameValue());
//		  temp.setQuerryString("personInfo.fullName");
//		  filtervec.add(temp);
//		  }
//		  if(personInfo.getCellValue()!=-1l)
//		  {
//		  temp=new Filter();
//		  temp.setLongValue(personInfo.getCellValue());
//		  temp.setQuerryString("personInfo.cellNumber");
//		  filtervec.add(temp);
//		  }
		  
		  if(vendorInfo.getIdValue()!=-1)
		  {
		  temp=new Filter();
		  temp.setIntValue(vendorInfo.getIdValue());
		  temp.setQuerryString("personInfo.count");
		  filtervec.add(temp);
		  }
		  
		  if(!(vendorInfo.getFullNameValue().equals("")))
		  {
		  temp=new Filter();
		  temp.setStringValue(vendorInfo.getFullNameValue());
		  temp.setQuerryString("personInfo.fullName");
		  filtervec.add(temp);
		  }
		  if(vendorInfo.getCellValue()!=-1l)
		  {
		  temp=new Filter();
		  temp.setLongValue(vendorInfo.getCellValue());
		  temp.setQuerryString("personInfo.cellNumber");
		  filtervec.add(temp);
		  }
		  
		  
		  
		  if(lbstatus.getSelectedIndex()!=0){
				temp=new Filter();
				temp.setStringValue(lbstatus.getItemText(lbstatus.getSelectedIndex()));
				filtervec.add(temp);
				temp.setQuerryString("status");
				filtervec.add(temp);
			}
		  if(olbApproverName.getSelectedIndex()!=0){
				temp=new Filter();temp.setStringValue(olbApproverName.getValue().trim());
				temp.setQuerryString("approverName");
				filtervec.add(temp);
			}
		
		  
		  if(LoginPresenter.branchRestrictionFlag==true && olbBranch.getSelectedIndex()==0)
			{
				temp=new Filter();
				temp.setList(AppUtility.getbranchlist());
				temp.setQuerryString("branch IN");
				filtervec.add(temp);
			}
			else{
			//   rohan added this for branch level restriction
				if(olbBranch.getSelectedIndex()!=0){
					temp=new Filter();
					temp.setStringValue(olbBranch.getValue().trim());
					temp.setQuerryString("branch");
					filtervec.add(temp);
				}
			}
			
				
				/*
				 *  nidhi 
				 *  1-07-2017
				 *  
				 */
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "OnlyForNBHC"))
				{
					if(olbCustomerCategory.getSelectedIndex()!=0){
						temp=new Filter();
						temp.setStringValue(olbCustomerCategory.getValue().trim());
						temp.setQuerryString("segment");
						filtervec.add(temp);
					}
				}else{
						
					if(!tbSegment.getValue().equals("")){
						
						temp=new Filter();
						temp.setStringValue(tbSegment.getValue().trim());
						temp.setQuerryString("segment");
						filtervec.add(temp);
					}
				}
				/*
				 *  end
				 */
				
				/*
				 *  nidhi
				 *   17-07-2017
				 *    service ID serach
				 */
				
				if(ibrateContractServiceId.getValue()!=null){
					temp=new Filter();
					temp.setIntValue(ibrateContractServiceId.getValue());
					temp.setQuerryString("rateContractServiceId");
					filtervec.add(temp);
				}	
				/*
				 *  end
				 */
				
				
				if(olbinvoicegroup.getSelectedIndex()!=0){
					temp=new Filter();
					temp.setStringValue(olbinvoicegroup.getItemText(olbinvoicegroup.getSelectedIndex()));
					temp.setQuerryString("invoiceGroup");
					filtervec.add(temp);
				}
				/**03-10-2017 sagar sore[ filter for search by number range i.e. Billing and Non-billing]**/
				if(olbcNumberRange.getSelectedIndex()!=0){
					temp=new Filter();
					temp.setStringValue(olbcNumberRange.getValue().trim());
					temp.setQuerryString("numberRange");
					filtervec.add(temp);
				}
				
		/**
		 * Date : 18-11-2017 BY ANIL
		 */
		if(tbReferenceNumber.getValue()!=null&&!tbReferenceNumber.getValue().equals("")){
			temp=new Filter();
			temp.setStringValue(tbReferenceNumber.getValue());
			temp.setQuerryString("refNumber");
			filtervec.add(temp);
		}
		
		/** date 13.03.2018 added by komal for nbhc**/
		if(olbInvoiceCategory.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(olbInvoiceCategory.getItemText(olbInvoiceCategory.getSelectedIndex()));
			temp.setQuerryString("invoiceCategory");
			filtervec.add(temp);
		}
		if(olbInvoiceConfigType.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(olbInvoiceConfigType.getItemText(olbInvoiceConfigType.getSelectedIndex()));
			temp.setQuerryString("invoiceConfigType");
			filtervec.add(temp);
		}
		/**
		 * END KOMAL
		 */
		/** date 20.10.2018 added by komal**/
		if(olbTypeOforder.getSelectedIndex() != 0){
			temp=new Filter();
			temp.setStringValue(olbTypeOforder.getValue(olbTypeOforder.getSelectedIndex()));
			temp.setQuerryString("typeOfOrder");
			filtervec.add(temp);
		}
		
		if(olbSubBillType.getSelectedIndex() != 0){
			temp=new Filter();
			temp.setStringValue(olbSubBillType.getValue(olbSubBillType.getSelectedIndex()));
			temp.setQuerryString("subBillType");
			filtervec.add(temp);
		}
		
		/**
		 * Date : 03-04-2019 by Vijay for cancelled Date search filter
		 */
		if(cancellationDateComparator.getValue()!=null){
			filtervec.addAll(cancellationDateComparator.getValue());
		}
		
		MyQuerry querry= new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Invoice());
		return querry;
	}
	
	
	
	@Override
	public void onSelection(SelectionEvent<Suggestion> event) 
	{
		if(event.getSource().equals(personInfo.getId())||event.getSource().equals(personInfo.getName())||event.getSource().equals(personInfo.getPhone()))
		{
			if(!vendorInfo.getId().getValue().equals("")||!vendorInfo.getName().getValue().equals("")||!vendorInfo.getPhone().getValue().equals(""))
			{
				vendorInfo.clear();
			}
		}
		
		if(event.getSource().equals(vendorInfo.getId())||event.getSource().equals(vendorInfo.getName())||event.getSource().equals(vendorInfo.getPhone()))
		{
			if(!personInfo.getId().getValue().equals("")&&!personInfo.getName().getValue().equals("")&&!personInfo.getPhone().getValue().equals(""))
			{
				personInfo.clear();
			}
		}
	}

	@Override
	public boolean validate() {

		String msg = "Wrong filter selection with From Date and To Date ! Please select proper filters with Date";
		
		
		if(dateComparator.getFromDate().getValue()!=null || dateComparator.getToDate().getValue()!=null ){
			if(ibinvoiceid.getValue()!=null || ibcontractid.getValue()!=null ||
				vendorInfo.getIdValue()!=-1 || vendorInfo.getIdValue()!=-1 ||	
			    !(vendorInfo.getFullNameValue().equals("")) || !(vendorInfo.getFullNameValue().equals("")) ||
		       olbApproverName.getSelectedIndex()!=0 || olbCustomerCategory.getSelectedIndex()!=0 ||
  			   !tbSegment.getValue().equals("") || ibrateContractServiceId.getValue()!=null ||olbinvoicegroup.getSelectedIndex()!=0 ||
			   olbcNumberRange.getSelectedIndex()!=0 || tbReferenceNumber.getValue()!=null&&!tbReferenceNumber.getValue().equals("") ){
				showDialogMessage(msg);
				return false;
			}
		}
		
		return true;
	}

	
	@Override
	public void onClick(ClickEvent event) {
		
		if(event.getSource().equals(getLblOk())){
		
		MyQuerry myQuerry = getQuerry();
		myQuerryStatic = myQuerry;

		service.getSearchResult(myQuerryStatic,new AsyncCallback<ArrayList<SuperModel>>() {
			@SuppressWarnings("unchecked")
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				ArrayList<Invoice> invoicelist = new ArrayList<Invoice>();
				//logger.log(Level.SEVERE, "size of result list "+ result.size());
				if (result.size() != 0) {
					System.out.println("Name of class "+ result.get(0).getClass().getName());
					for (SuperModel model : result) {
						Invoice invoice = (Invoice) model;
//						logger.log(Level.SEVERE, "name: "+ invoice.getName() + " :: "+ invoice.getCount());
						invoicelist.add(invoice);
					}
//					invoices.clear();
//					invoices.addAll(invoicelist);
					System.out.println("Size of invoice "+ invoicelist.size());
					csvDownload(invoicelist);	
					showWaitSymbol();

					
				} else {
					Window.alert("Data not Found");
					hideWaitSymbol();
				}
			}
			@Override
			public void onFailure(Throwable caught) {
				caught.printStackTrace();
				hideWaitSymbol();
			}
		});
		
		if(rbCRM.getValue()==false&&rbOracleInExcel.getValue()==false&&rbOracleInCSV.getValue()==false){
			showDialogMessage("Please select format to download report!!");
		}
		}
		
		if(event.getSource().equals(rbCRM)){
			if(rbCRM.getValue()==true){
				rbOracleInCSV.setValue(false);
				rbOracleInExcel.setValue(false);
			}
		}
		
		if(event.getSource().equals(rbOracleInCSV)){
			if(rbOracleInCSV.getValue()==true){
				rbCRM.setValue(false);
				rbOracleInExcel.setValue(false);
			}
		}
		
		if(event.getSource().equals(rbOracleInExcel)){
			if(rbOracleInExcel.getValue()==true){
				rbCRM.setValue(false);
				rbOracleInCSV.setValue(false);
			}
		}
		
		
		
	}
	
	
	
	private void csvDownload(ArrayList<Invoice> invoicelist) {
		
			csvservice.setcrminvoicelist(invoicelist,new AsyncCallback<Void>() {
//				csvservice.setinvoicelist(invoices,new AsyncCallback<Void>() {
					@SuppressWarnings("unchecked")
					@Override
					public void onFailure(Throwable caught) {
						System.out.println("RPC call Failed"+ caught);
						hideWaitSymbol();
					}

					@Override
					public void onSuccess(Void result) {
						Timer t = new Timer() {
							@Override
							public void run() {
					 /**@author Sheetal : 28-04-2022
					   * Des : CRM Tally oracle Report will now download in Excel format 
					   *        or in csv format as per user, requirement by Envocare**/
								
								if(rbCRM.getValue()==true){
								String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
								final String url = gwt+ "csvservlet"+ "?type=" + 203;
								Window.open(url, "test","enabled");
								}
								
								if(rbOracleInExcel.getValue()==true){
								        String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
								        final String url = gwt+ "CreateXLXSServlet"+ "?type=" + 9;
								        Window.open(url, "test","enabled");
								  
								}
								
								if(rbOracleInCSV.getValue()==true){
									Console.log("Log 1");

//                                     Timer timer3 = new Timer() {
//										
//										@Override
//										public void run() {

											String gwt4 = com.google.gwt.core.client.GWT.getModuleBaseURL();
											final String url4 = gwt4+ "csvservlet"+ "?type=" + 210;
											Window.open(url4, "test","enabled");
											Console.log("1 sheet url=="+url4);
											
//										}
//									};
//									timer3.schedule(3000);
									Console.log("Log 2");
                                    Timer timer2  = new Timer() {
										
										@Override
										public void run() {
											String gwt3 = com.google.gwt.core.client.GWT.getModuleBaseURL();
											final String url3 = gwt3+ "csvservlet"+ "?type=" + 209;
											Window.open(url3, "test","enabled");
											Console.log("2 sheet url=="+url3);
										}
									};
									timer2.schedule(2000);
									
									Console.log("Log 3");

                                     Timer timer = new Timer() {
										
										@Override
										public void run() {

											String gwt2 = com.google.gwt.core.client.GWT.getModuleBaseURL();
											final String url2 = gwt2+ "csvservlet"+ "?type=" + 208;
											Window.open(url2, "test","enabled");
											Console.log("3 sheet url=="+url2);
											
										}
									};
									timer.schedule(3000);
									Console.log("Log 4");

									Timer time = new Timer() {
										
										@Override
										public void run() {
											
											String gwt1 = com.google.gwt.core.client.GWT.getModuleBaseURL();
											final String url1 = gwt1+ "csvservlet"+ "?type=" + 207;
											Window.open(url1, "test","enabled");
											Console.log("4 sheet url=="+url1);
											
										}
									};
									time.schedule(4000);
									Console.log("Log 5");

									
							}
								
							}
						};
						t.schedule(2000);
						hideWaitSymbol();
					}
				});
		}

}
