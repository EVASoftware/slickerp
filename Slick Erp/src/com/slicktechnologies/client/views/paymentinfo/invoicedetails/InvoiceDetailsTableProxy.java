package com.slicktechnologies.client.views.paymentinfo.invoicedetails;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

public class InvoiceDetailsTableProxy extends SuperTable<Invoice> {
	
	TextColumn<Invoice> invoiceIdColumn;
	TextColumn<Invoice> invoiceDateColumn;
	TextColumn<Invoice> invoiceTypeColumn;
	TextColumn<Invoice> customerIdColumn;
	TextColumn<Invoice> customerNameColumn;
	TextColumn<Invoice> customerCellColumn;
	TextColumn<Invoice> contractIdColumn;
	TextColumn<Invoice> salesAmtColumn;
	TextColumn<Invoice> invoiceAmtColumn;
	TextColumn<Invoice> approverNameColumn;
	TextColumn<Invoice> accountTypeColumn;
	TextColumn<Invoice> statusColumn;
	TextColumn<Invoice> branchColumn;
	
	/*
	 *  nidhi
	 *   1-07-2017
	 */
	TextColumn<Invoice> segmentColumn;
	/*
	 *  end
	 */
	
	/*
	 *  nidhi
	 *   17-07-2017 
	 *   display service Id 
	 */
	TextColumn<Invoice> serviceIdColumn;
	/*
	 *  end
	 */
	/** 03-10-2017 sagar sore [adding Number range column in Invoice list of search pop up]**/
	TextColumn<Invoice> numberRangeColumn;
	
	/**
	 * Date : 18-11-2017 BY ANIL
	 */
	TextColumn<Invoice> getColRefNum;
	/** date 13.03.2018 added by komal for nbhc **/
	TextColumn<Invoice> getColumnInvoiceGroup;
	TextColumn<Invoice> getColumnInvoiceCategory;
	TextColumn<Invoice> getColumnInvoiceConfigType;
	/**
	 * end komal
	 */
	/** date 1.11.2018 added by komal for type of order and sub bill type **/
	TextColumn<Invoice> getColTypeOfOrder;
	TextColumn<Invoice> getColSubBillType;

	
	public InvoiceDetailsTableProxy() {
		super();
	}

	@Override
	public void createTable() {
		/**03-10-2017 sagar sore [To add number range column in search pop up result]**/
		addColumnInvoiceId();
		addColumnInvoiceDate();
		addColumnInvoiceType();
		addColumnNumberRange();
		addColumnCustomerId();
		addColumnCustomerName();
		addColumnCustomerPhone();
		addColumnAccountType();
		addColumnContractId();
		addColumnSalesAmount();
		addColumnInvoiceAmount();
		addColumnApproverName();
		addColumnInvoiceStatus();
		addColumnInvoiceBranch();
		
		/*
		 *  nidhi
		 */
		addColumnServiceId();
		/*
		 *  end
		 */
		
		/*
		 *  nidhi
		 */
		addColumnInvoiceSegment();
		/*
		 *  end
		 */
		getColRefNum();
		/** date 13.03.2018 added by komal for nbhc **/
		addColumnInvoiceGroup();
		addColumnInvoiceCategory();
		addColumnInvoiceConfigType();
		/**
		 * end komal
		 */
		getColTypeOfOrder();
		getColSubBillType();
		
	}
	
	private void getColRefNum() {
		getColRefNum = new TextColumn<Invoice>() {
			
			@Override
			public String getValue(Invoice object) {
				if(object.getRefNumber()!= null)
					return object.getRefNumber() +"";
				else 
					return "";
			}
		};
		table.addColumn(getColRefNum,"Reference Number");
		table.setColumnWidth(getColRefNum, 100, Unit.PX);
		getColRefNum.setSortable(true);
	}
	
	private void addSortingOnRefNum() {
		List<Invoice> list = getDataprovider().getList();
		columnSort = new ListHandler<Invoice>(list);
		columnSort.setComparator(getColRefNum,new Comparator<Invoice>() {
			@Override
			public int compare(Invoice e1, Invoice e2) {
				if (e1 != null && e2 != null) {
					if (e1.getRefNumber() != null&& e2.getRefNumber() != null) {
						return e1.getRefNumber().compareTo(e2.getRefNumber());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	public void addColumnServiceId()
	{
		serviceIdColumn =new TextColumn<Invoice>() {

			@Override
			public String getValue(Invoice object) {
				return object.getRateContractServiceId()+"";
			}
		};
		
		table.addColumn(serviceIdColumn,"Service Id");
		table.setColumnWidth(serviceIdColumn, 100, Unit.PX);
		serviceIdColumn.setSortable(true);
	}	
	
	public void addColumnInvoiceSegment()
	{
		segmentColumn=new TextColumn<Invoice>() {

			@Override
			public String getValue(Invoice object) {
				return object.getSegment();
			}
		};
		
		table.addColumn(segmentColumn,"Segment");
		table.setColumnWidth(segmentColumn, 100, Unit.PX);
		segmentColumn.setSortable(true);
	}	
	
	public void addColumnInvoiceId()
	{
		invoiceIdColumn=new TextColumn<Invoice>() {
		@Override
		public String getCellStyleNames(Context context, Invoice object) {
			
			if((object.getStatus().equals("Invoiced"))){
				 return "blue";
			}
			else if((object.getStatus().equals("Cancelled"))){
				 return "red";
			 }
			else 
			{
				return "black";
			}
		}

			@Override
			public String getValue(Invoice object) {
				return object.getCount()+"";
			}
		};
		table.addColumn(invoiceIdColumn,"Invoice ID");
		table.setColumnWidth(invoiceIdColumn, 100, Unit.PX);
		invoiceIdColumn.setSortable(true);
	}

	public void addColumnInvoiceDate()
	{
		invoiceDateColumn=new TextColumn<Invoice>() {

			@Override
			public String getValue(Invoice object) {
				if(object.getInvoiceDate()!=null)
				{
				    String serDate=AppUtility.parseDate(object.getInvoiceDate());
				    return serDate;
				}
				return "Not Set";
			}
		};
		
		table.addColumn(invoiceDateColumn,"Invoice Date");
		table.setColumnWidth(invoiceDateColumn, 120, Unit.PX);
		invoiceDateColumn.setSortable(true);
	}

	public void addColumnInvoiceType()
	{
		invoiceTypeColumn=new TextColumn<Invoice>() {

			@Override
			public String getValue(Invoice object) {
				return object.getInvoiceType();
			}
		};
		
		table.addColumn(invoiceTypeColumn,"Invoice Document Type");
		table.setColumnWidth(invoiceTypeColumn, 140, Unit.PX);
		invoiceTypeColumn.setSortable(true);
	}
	
	private void addColumnCustomerId() {
		customerIdColumn=new TextColumn<Invoice>() {

			@Override
			public String getValue(Invoice object) {
				return object.getCustomerCount()+"";
			}
		};
		
		table.addColumn(customerIdColumn,"BP ID");
		table.setColumnWidth(customerIdColumn, 120, Unit.PX);
		customerIdColumn.setSortable(true);
		
	}
	
	public void addColumnCustomerName()
	{
		customerNameColumn=new TextColumn<Invoice>() {

			@Override
			public String getValue(Invoice object) {
				return object.getName();
			}
		};
		
		table.addColumn(customerNameColumn,"BP Name");
		table.setColumnWidth(customerNameColumn, 120, Unit.PX);
		customerNameColumn.setSortable(true);
	}
	
	public void addColumnCustomerPhone()
	{
		customerCellColumn=new TextColumn<Invoice>() {

			@Override
			public String getValue(Invoice object) {
				return object.getCellNumber()+"";
			}
		};
		
		table.addColumn(customerCellColumn,"Cell Number");
		table.setColumnWidth(customerCellColumn, 120, Unit.PX);
		customerCellColumn.setSortable(true);
	}
	
	
	public void addColumnAccountType()
	{
		accountTypeColumn=new TextColumn<Invoice>() {

			@Override
			public String getValue(Invoice object) {
				return object.getAccountType();
			}
		};
		
		table.addColumn(accountTypeColumn,"Acc. Type");
		table.setColumnWidth(accountTypeColumn, 90, Unit.PX);
		accountTypeColumn.setSortable(true);
	}
	
	
	private void addColumnContractId() {
		contractIdColumn=new TextColumn<Invoice>() {

			@Override
			public String getValue(Invoice object) {
				return object.getContractCount()+"";
			}
		};
		
		table.addColumn(contractIdColumn,"Order ID");
		table.setColumnWidth(contractIdColumn, 100, Unit.PX);
		contractIdColumn.setSortable(true);
		
	}

	public void addColumnSalesAmount()
	{
		salesAmtColumn=new TextColumn<Invoice>() {

			@Override
			public String getValue(Invoice object) {
				return object.getTotalSalesAmount()+"";
			}
		};
		
		table.addColumn(salesAmtColumn,"Sales Amt");
		table.setColumnWidth(salesAmtColumn, 110, Unit.PX);
		salesAmtColumn.setSortable(true);
	}
	
	public void addColumnInvoiceAmount()
	{
		invoiceAmtColumn=new TextColumn<Invoice>() {

			@Override
			public String getValue(Invoice object) {
				return object.getInvoiceAmount()+"";
			}
		};
		
		table.addColumn(invoiceAmtColumn,"Invoice Amt");
		table.setColumnWidth(invoiceAmtColumn, 110, Unit.PX);
		invoiceAmtColumn.setSortable(true);
	}
	
	public void addColumnApproverName()
	{
		approverNameColumn=new TextColumn<Invoice>() {

			@Override
			public String getValue(Invoice object) {
				return object.getApproverName();
			}
		};
		
		table.addColumn(approverNameColumn,"Approver Name");
		table.setColumnWidth(approverNameColumn, 120, Unit.PX);
		approverNameColumn.setSortable(true);
	}	
		
	public void addColumnInvoiceStatus()
	{
		statusColumn=new TextColumn<Invoice>() {

			@Override
			public String getValue(Invoice object) {
				return object.getStatus();
			}
		};
		
		table.addColumn(statusColumn,"Status");
		table.setColumnWidth(statusColumn, 100, Unit.PX);
		statusColumn.setSortable(true);
	}	
	
	public void addColumnInvoiceBranch()
	{
		branchColumn=new TextColumn<Invoice>() {

			@Override
			public String getValue(Invoice object) {
				return object.getBranch();
			}
		};
		
		table.addColumn(branchColumn,"Branch");
		table.setColumnWidth(branchColumn, 100, Unit.PX);
		branchColumn.setSortable(true);
	}	
	
	
	
	public void addColumnSorting()
	{
		addSortinggetInvoiceCount();
		addSortinggetInvoiceDate();
		addSortinggetInvoiceType();
		addSortinggetCustomerId();
		addSortinggetCustomerFullName();
		addSortinggetCustomerCellNumber();
		addSortingAccountType();
		addSortinggetContractCount();
		addSortinggetSalesAmount();
		addSortinggetInvoiceAmount();
		addSortinggetApproverName();
		addSortinggetStatus();
		addSortinggetBranch();
		/*
		 *  nidhi
		 */
		addSortinggetSegment();
		/*
		 * end
		 */
		addSortinggetServiceId();
		/**03-10-2017 sagar sore [adding number range column]**/
		addSortinggetNumberRange();
		addSortingOnRefNum();
		/** date 13.03.2018 added by komal for invoice category and type**/
		addSortinggetInvoiceGroup();
		addSortinggetInvoiceCategory();
		addSortinggetInvoiceConfigType();
		/**
		 * end komal
		 */
		addSortingTypeOfOrder();
		addSortingSubBillType();
	}
	
	protected void addSortinggetServiceId()
	{
		List<Invoice> list=getDataprovider().getList();
		columnSort=new ListHandler<Invoice>(list);
		columnSort.setComparator(serviceIdColumn, new Comparator<Invoice>()
				{
			@Override
			public int compare(Invoice e1,Invoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getRateContractServiceId()== e2.getRateContractServiceId()){
						return 0;}
					if(e1.getRateContractServiceId()> e2.getRateContractServiceId()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addSortinggetSegment()
	{
		List<Invoice> list=getDataprovider().getList();
		columnSort=new ListHandler<Invoice>(list);
		columnSort.setComparator(segmentColumn, new Comparator<Invoice>()
				{
			@Override
			public int compare(Invoice e1,Invoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getSegment()!=null && e2.getSegment()!=null){
						return e1.getSegment().compareTo(e2.getSegment());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}

	
	protected void addSortinggetInvoiceCount()
	{
		List<Invoice> list=getDataprovider().getList();
		columnSort=new ListHandler<Invoice>(list);
		columnSort.setComparator(invoiceIdColumn, new Comparator<Invoice>()
				{
			@Override
			public int compare(Invoice e1,Invoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetInvoiceDate()
	{
		List<Invoice> list=getDataprovider().getList();
		columnSort=new ListHandler<Invoice>(list);
		columnSort.setComparator(invoiceDateColumn, new Comparator<Invoice>()
				{
			@Override
			public int compare(Invoice e1,Invoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getInvoiceDate()!=null && e2.getInvoiceDate()!=null){
						return e1.getInvoiceDate().compareTo(e2.getInvoiceDate());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortingAccountType()
	{
		List<Invoice> list=getDataprovider().getList();
		columnSort=new ListHandler<Invoice>(list);
		columnSort.setComparator(accountTypeColumn, new Comparator<Invoice>()
				{
			@Override
			public int compare(Invoice e1,Invoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getAccountType()!=null && e2.getAccountType()!=null){
						return e1.getAccountType().compareTo(e2.getAccountType());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetInvoiceType()
	{
		List<Invoice> list=getDataprovider().getList();
		columnSort=new ListHandler<Invoice>(list);
		columnSort.setComparator(invoiceTypeColumn, new Comparator<Invoice>()
				{
			@Override
			public int compare(Invoice e1,Invoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getInvoiceType()!=null && e2.getInvoiceType()!=null){
						return e1.getInvoiceType().compareTo(e2.getInvoiceType());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetCustomerId()
	{
		List<Invoice> list=getDataprovider().getList();
		columnSort=new ListHandler<Invoice>(list);
		columnSort.setComparator(customerIdColumn, new Comparator<Invoice>()
				{
			@Override
			public int compare(Invoice e1,Invoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getPersonInfo().getCount()== e2.getPersonInfo().getCount()){
						return 0;}
					if(e1.getPersonInfo().getCount()> e2.getPersonInfo().getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetCustomerFullName()
	{
		List<Invoice> list=getDataprovider().getList();
		columnSort=new ListHandler<Invoice>(list);
		columnSort.setComparator(customerNameColumn, new Comparator<Invoice>()
				{
			@Override
			public int compare(Invoice e1,Invoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getPersonInfo().getFullName()!=null && e2.getPersonInfo().getFullName()!=null){
						return e1.getPersonInfo().getFullName().compareTo(e2.getPersonInfo().getFullName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addSortinggetCustomerCellNumber()
	{
		List<Invoice> list=getDataprovider().getList();
		columnSort=new ListHandler<Invoice>(list);
		columnSort.setComparator(customerCellColumn, new Comparator<Invoice>()
				{
			@Override
			public int compare(Invoice e1,Invoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getPersonInfo().getCellNumber()== e2.getPersonInfo().getCellNumber()){
						return 0;}
					if(e1.getPersonInfo().getCellNumber()> e2.getPersonInfo().getCellNumber()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetContractCount()
	{
		List<Invoice> list=getDataprovider().getList();
		columnSort=new ListHandler<Invoice>(list);
		columnSort.setComparator(contractIdColumn, new Comparator<Invoice>()
				{
			@Override
			public int compare(Invoice e1,Invoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getContractCount()== e2.getContractCount()){
						return 0;}
					if(e1.getContractCount()> e2.getContractCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetSalesAmount()
	{
		List<Invoice> list=getDataprovider().getList();
		columnSort=new ListHandler<Invoice>(list);
		columnSort.setComparator(salesAmtColumn, new Comparator<Invoice>()
				{
			@Override
			public int compare(Invoice e1,Invoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getTotalSalesAmount()== e2.getTotalSalesAmount()){
						return 0;}
					if(e1.getTotalSalesAmount()> e2.getTotalSalesAmount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetInvoiceAmount()
	{
		List<Invoice> list=getDataprovider().getList();
		columnSort=new ListHandler<Invoice>(list);
		columnSort.setComparator(invoiceAmtColumn, new Comparator<Invoice>()
				{
			@Override
			public int compare(Invoice e1,Invoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getInvoiceAmount()== e2.getInvoiceAmount()){
						return 0;}
					if(e1.getInvoiceAmount()> e2.getInvoiceAmount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetApproverName()
	{
		List<Invoice> list=getDataprovider().getList();
		columnSort=new ListHandler<Invoice>(list);
		columnSort.setComparator(approverNameColumn, new Comparator<Invoice>()
				{
			@Override
			public int compare(Invoice e1,Invoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getApproverName()!=null && e2.getApproverName()!=null){
						return e1.getApproverName().compareTo(e2.getApproverName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetStatus()
	{
		List<Invoice> list=getDataprovider().getList();
		columnSort=new ListHandler<Invoice>(list);
		columnSort.setComparator(statusColumn, new Comparator<Invoice>()
				{
			@Override
			public int compare(Invoice e1,Invoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getStatus()!=null && e2.getStatus()!=null){
						return e1.getStatus().compareTo(e2.getStatus());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	
	protected void addSortinggetBranch()
	{
		List<Invoice> list=getDataprovider().getList();
		columnSort=new ListHandler<Invoice>(list);
		columnSort.setComparator(branchColumn, new Comparator<Invoice>()
				{
			@Override
			public int compare(Invoice e1,Invoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBranch()!=null && e2.getBranch()!=null){
						return e1.getBranch().compareTo(e2.getBranch());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	/**03-10-2017 sagar sore [adding number range column]**/
	public void addColumnNumberRange() {
		numberRangeColumn = new TextColumn<Invoice>() {
			@Override
			public String getValue(Invoice object) {
				return object.getNumberRange();
			}
		};
		table.addColumn(numberRangeColumn, "Number Range");
		table.setColumnWidth(numberRangeColumn, 120, Unit.PX);
		numberRangeColumn.setSortable(true);
	}

	protected void addSortinggetNumberRange() {
		List<Invoice> list = getDataprovider().getList();
		columnSort = new ListHandler<Invoice>(list);
		columnSort.setComparator(numberRangeColumn, new Comparator<Invoice>() {
			@Override
			public int compare(Invoice e1, Invoice e2) {
				if (e1 != null && e2 != null) {
					
					if (e1.getNumberRange() == null && e2.getNumberRange() == null){
						return 0;
					}
					if(e1.getNumberRange() == null) return 1;							
					if(e2.getNumberRange() == null) return -1;							
					return e1.getNumberRange().compareTo(e2.getNumberRange());

				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	/**end**/
	/** date 13.03.2018 added by komal for nbhc **/
	private void addColumnInvoiceCategory(){
		getColumnInvoiceCategory =new TextColumn<Invoice>() {

			@Override
			public String getValue(Invoice object) {
				return object.getInvoiceCategory()+"";
			}
		};
		
		table.addColumn(getColumnInvoiceCategory,"Invoice Category");
		table.setColumnWidth(getColumnInvoiceCategory, 100, Unit.PX);
		getColumnInvoiceCategory.setSortable(true);

	}
	private void addColumnInvoiceConfigType(){
		getColumnInvoiceConfigType =new TextColumn<Invoice>() {

			@Override
			public String getValue(Invoice object) {
				return object.getInvoiceConfigType()+"";
			}
		};
		
		table.addColumn(getColumnInvoiceConfigType,"Invoice Config Type");
		table.setColumnWidth(getColumnInvoiceConfigType, 100, Unit.PX);
		serviceIdColumn.setSortable(true);

	}
	protected void addSortinggetInvoiceCategory()
	{
		List<Invoice> list=getDataprovider().getList();
		columnSort=new ListHandler<Invoice>(list);
		columnSort.setComparator(getColumnInvoiceCategory, new Comparator<Invoice>()
				{
			@Override
			public int compare(Invoice e1,Invoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getInvoiceCategory()!=null && e2.getInvoiceCategory()!=null){
						return e1.getInvoiceCategory().compareTo(e2.getInvoiceCategory());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addSortinggetInvoiceConfigType()
	{
		List<Invoice> list=getDataprovider().getList();
		columnSort=new ListHandler<Invoice>(list);
		columnSort.setComparator(getColumnInvoiceConfigType, new Comparator<Invoice>()
				{
			@Override
			public int compare(Invoice e1,Invoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getInvoiceConfigType()!=null && e2.getInvoiceConfigType()!=null){
						return e1.getInvoiceConfigType().compareTo(e2.getInvoiceConfigType());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnInvoiceGroup(){
		getColumnInvoiceGroup =new TextColumn<Invoice>() {

			@Override
			public String getValue(Invoice object) {
				return object.getInvoiceGroup()+"";
			}
		};
		
		table.addColumn(getColumnInvoiceGroup,"Invoice Group");
		table.setColumnWidth(getColumnInvoiceGroup, 100, Unit.PX);
		getColumnInvoiceGroup.setSortable(true);

	}
	protected void addSortinggetInvoiceGroup()
	{
		List<Invoice> list=getDataprovider().getList();
		columnSort=new ListHandler<Invoice>(list);
		columnSort.setComparator(getColumnInvoiceGroup, new Comparator<Invoice>()
				{
			@Override
			public int compare(Invoice e1,Invoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getInvoiceGroup()!=null && e2.getInvoiceGroup()!=null){
						return e1.getInvoiceGroup().compareTo(e2.getInvoiceGroup());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}

	/**
	 * end komal
	 */


	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
	
	/** date 1.11.2018 added by komal for type of order and sub bill type **/
	public void getColTypeOfOrder()
	{
		getColTypeOfOrder=new TextColumn<Invoice>() {

			@Override
			public String getValue(Invoice object) {
				return object.getTypeOfOrder();
			}
		};
		
		table.addColumn(getColTypeOfOrder,"Type Of Order");
		table.setColumnWidth(getColTypeOfOrder, 120, Unit.PX);
		getColTypeOfOrder.setSortable(true);
	}	
	
	protected void addSortingTypeOfOrder()
	{
		List<Invoice> list=getDataprovider().getList();
		columnSort=new ListHandler<Invoice>(list);
		columnSort.setComparator(getColTypeOfOrder, new Comparator<Invoice>()
				{
			@Override
			public int compare(Invoice e1,Invoice e2)
			{
				if (e1 != null && e2 != null) {
					
					if (e1.getNumberRange() == null && e2.getNumberRange() == null){
						return 0;
					}
					if(e1.getNumberRange() == null) return 1;							
					if(e2.getNumberRange() == null) return -1;							
					return e1.getNumberRange().compareTo(e2.getNumberRange());

				}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	public void getColSubBillType()
	{
		getColSubBillType=new TextColumn<Invoice>() {

			@Override
			public String getValue(Invoice object) {
				return object.getSubBillType();
			}
		};
		
		table.addColumn(getColSubBillType,"Sub Bill Type");
		table.setColumnWidth(getColSubBillType, 120, Unit.PX);
		getColSubBillType.setSortable(true);
	}	
	
	protected void addSortingSubBillType()
	{
		List<Invoice> list=getDataprovider().getList();
		columnSort=new ListHandler<Invoice>(list);
		columnSort.setComparator(getColSubBillType, new Comparator<Invoice>()
				{
			@Override
			public int compare(Invoice e1,Invoice e2)
			{
				if (e1 != null && e2 != null) {
					
					if (e1.getNumberRange() == null && e2.getNumberRange() == null){
						return 0;
					}
					if(e1.getNumberRange() == null) return 1;							
					if(e2.getNumberRange() == null) return -1;							
					return e1.getNumberRange().compareTo(e2.getNumberRange());

				}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}

	/**end**/

}
