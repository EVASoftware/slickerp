package com.slicktechnologies.client.views.paymentinfo.invoicedetails;

import java.util.ArrayList;
import java.util.List;

import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;

public class InvoiceCharges {

	
	double payPercent;
	List<ContractCharges> chargesList = new ArrayList<ContractCharges>();
	List<ContractCharges> otherChargesList = new ArrayList<ContractCharges>();
	
	//****************getters and setters *****************************
	
	public List<ContractCharges> getOtherChargesList() {
		return otherChargesList;
	}

	public void setOtherChargesList(List<ContractCharges> otherChargesList) {
		this.otherChargesList = otherChargesList;
	}

	public double getPayPercent() {
		return payPercent;
	}

	public void setPayPercent(double payPercent) {
		this.payPercent = payPercent;
	}

	public List<ContractCharges> getChargesList() {
		return chargesList;
	}

	public void setChargesList(List<ContractCharges> chargesList) {
		this.chargesList = chargesList;
	}

	
	
}
