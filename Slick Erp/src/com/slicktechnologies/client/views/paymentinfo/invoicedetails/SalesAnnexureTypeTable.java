package com.slicktechnologies.client.views.paymentinfo.invoicedetails;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.salesprocess.SalesAnnexureType;

public class SalesAnnexureTypeTable  extends SuperTable<SalesAnnexureType> {

	TextColumn<SalesAnnexureType> soIdCol;
	TextColumn<SalesAnnexureType> soDateCol; 
	TextColumn<SalesAnnexureType> siteLocationCol;
	
	TextColumn<SalesAnnexureType> budgetCol;
	TextColumn<SalesAnnexureType> soAmountCol; 
	TextColumn<SalesAnnexureType> payableAmountCol;
	
	TextColumn<SalesAnnexureType> costCodeCol; 
	TextColumn<SalesAnnexureType> carpetAreaCol;
	
	
	Column<SalesAnnexureType,String> editBudgetCol;
	Column<SalesAnnexureType,String> editSoAmountCol;
	Column<SalesAnnexureType,String> editPayableAmountCol;
	
	Column<SalesAnnexureType,String> editCostCodeCol;
	Column<SalesAnnexureType,String> editCarpetAreaCol;
	
	
	GWTCAlert alert;
	NumberFormat nf=NumberFormat.getFormat("0.00");
	
	
	
	public SalesAnnexureTypeTable() {
		super();
		setHeight("400px");
	}
	
	@Override
	public void createTable() {
		alert=new GWTCAlert();
		
		soIdCol();
		soDateCol();
		siteLocationCol();
		
		editCostCodeCol();
		editCarpetAreaCol();
		
		editBudgetCol();
		editSoAmountCol();
		editPayableAmountCol();
	}

	private void soIdCol() {
		soIdCol=new TextColumn<SalesAnnexureType>() {
			@Override
			public String getValue(SalesAnnexureType object) {
				if(object.getSoId()!=0){
					return object.getSoId()+"";
				}
				return "";
			}
		};
		table.addColumn(soIdCol,"Sales Order Id");
	}

	private void soDateCol() {
		soDateCol=new TextColumn<SalesAnnexureType>() {
			@Override
			public String getValue(SalesAnnexureType object) {
				if(object.getSoDate()!=null){
					return AppUtility.parseDate(object.getSoDate());
				}
				return "";
			}
		};
		table.addColumn(soDateCol,"Sales Order Date");
	}

	private void siteLocationCol() {
		siteLocationCol=new TextColumn<SalesAnnexureType>() {
			@Override
			public String getValue(SalesAnnexureType object) {
				if(object.getSiteLocation()!=null){
					return object.getSiteLocation();
				}
				return "";
			}
		};
		table.addColumn(siteLocationCol,"Site Location");
	}
	
	private void costCodeCol() {
		costCodeCol=new TextColumn<SalesAnnexureType>() {
			@Override
			public String getValue(SalesAnnexureType object) {
				if(object.getCostCode()!=null){
					return object.getCostCode();
				}
				return "";
			}
		};
		table.addColumn(costCodeCol,"Cost Code");
	}
	
	private void carpetAreaCol() {
		carpetAreaCol=new TextColumn<SalesAnnexureType>() {
			@Override
			public String getValue(SalesAnnexureType object) {
				if(object.getCarpetArea()!=null){
					return object.getCarpetArea();
				}
				return "";
			}
		};
		table.addColumn(carpetAreaCol,"Carpet Area");
	}
	
	private void editCostCodeCol() {
		EditTextCell editCell=new EditTextCell();
		editCostCodeCol=new Column<SalesAnnexureType,String>(editCell) {
			@Override
			public String getValue(SalesAnnexureType object) {
				if(object.getCostCode()!=null){
					return object.getCostCode();
				}
				return "";
			}
		};
		table.addColumn(editCostCodeCol,"#Cost Code");
		
		editCostCodeCol.setFieldUpdater(new FieldUpdater<SalesAnnexureType, String>(){
			@Override
			public void update(int index, SalesAnnexureType object,String value) {
				try{
					if(value!=null){
						object.setCostCode(value);;
					}else{
						object.setCostCode("");
					}
				}
				catch (Exception e){
				}
				table.redrawRow(index);
			}
		});
	}
	
	private void editCarpetAreaCol() {
		EditTextCell editCell=new EditTextCell();
		editCarpetAreaCol=new Column<SalesAnnexureType,String>(editCell) {
			@Override
			public String getValue(SalesAnnexureType object) {
				if(object.getCarpetArea()!=null){
					return object.getCarpetArea();
				}
				return "";
			}
		};
		table.addColumn(editCarpetAreaCol,"#Carpet Area");
		
		editCarpetAreaCol.setFieldUpdater(new FieldUpdater<SalesAnnexureType, String>(){
			@Override
			public void update(int index, SalesAnnexureType object,String value) {
				try{
					if(value!=null){
						object.setCarpetArea(value);;
					}else{
						object.setCarpetArea("");
					}
				}
				catch (Exception e){
				}
				table.redrawRow(index);
			}
		});
	}

	
	private void editBudgetCol() {
		EditTextCell editCell=new EditTextCell();
		editBudgetCol=new Column<SalesAnnexureType,String>(editCell) {
			@Override
			public String getValue(SalesAnnexureType object) {
				if(object.getBudget()!=0){
					return object.getBudget()+"";
				}
				return "";
			}
		};
		table.addColumn(editBudgetCol,"#Budget");
		
		editBudgetCol.setFieldUpdater(new FieldUpdater<SalesAnnexureType, String>(){
			@Override
			public void update(int index, SalesAnnexureType object,String value) {
				try{
					if(value!=null){
						double price=Double.parseDouble(value);
						object.setBudget(price);
					}else{
						object.setBudget(0.0);
					}
				}
				catch (Exception e){
					alert.alert("Please enter numeric value only.");
				}
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				table.redrawRow(index);
			}
		});
	}

	private void editSoAmountCol() {
		EditTextCell editCell=new EditTextCell();
		editSoAmountCol=new Column<SalesAnnexureType,String>(editCell) {
			@Override
			public String getValue(SalesAnnexureType object) {
				return object.getSoAmount()+"";
			}
			
		};
		table.addColumn(editSoAmountCol,"#Sales Order Amount");
		
		editSoAmountCol.setFieldUpdater(new FieldUpdater<SalesAnnexureType, String>(){
			@Override
			public void update(int index, SalesAnnexureType object,String value) {
				try{
					if(value!=null){
						double price=Double.parseDouble(value);
						object.setSoAmount(price);;
					}else{
						object.setSoAmount(0.0);
					}
				}
				catch (Exception e){
					alert.alert("Please enter numeric value only.");
				}
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				table.redrawRow(index);
			}
		});
	}

	private void editPayableAmountCol() {
		EditTextCell editCell=new EditTextCell();
		editPayableAmountCol=new Column<SalesAnnexureType,String>(editCell) {
			@Override
			public String getValue(SalesAnnexureType object) {
				return object.getPayableAmount()+"";
			}
		};
		table.addColumn(editPayableAmountCol,"#Payable Amount");
		
		editPayableAmountCol.setFieldUpdater(new FieldUpdater<SalesAnnexureType, String>(){
			@Override
			public void update(int index, SalesAnnexureType object,String value) {
				try{
					if(value!=null){
						double price=Double.parseDouble(value);
						object.setPayableAmount(price);
					}else{
						object.setPayableAmount(0.0);
					}
				}
				catch (Exception e){
					alert.alert("Please enter numeric value only.");
				}
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				table.redrawRow(index);
			}
		});
	}
	
	private void payableAmountCol() {
		payableAmountCol=new TextColumn<SalesAnnexureType>() {
			@Override
			public String getValue(SalesAnnexureType object) {
				return object.getPayableAmount()+"";
			}
		};
		table.addColumn(payableAmountCol,"Payable Amount");
	}

	private void soAmountCol() {
		soAmountCol=new TextColumn<SalesAnnexureType>() {
			@Override
			public String getValue(SalesAnnexureType object) {
				return object.getSoAmount()+"";
			}
			
		};
		table.addColumn(soAmountCol,"Sales Order Amount");		
	}

	private void budgetCol() {
		budgetCol=new TextColumn<SalesAnnexureType>() {
			@Override
			public String getValue(SalesAnnexureType object) {
				if(object.getBudget()!=0){
					return object.getBudget()+"";
				}
				return "";
			}
		};
		table.addColumn(budgetCol,"Budget");
	}

	@Override
	protected void initializekeyprovider() {
		
	}

	@Override
	public void addFieldUpdater() {
		
	}

	@Override
	public void setEnable(boolean state) {
		for (int i = table.getColumnCount() - 1; i > -1; i--)
			table.removeColumn(i);
			
		if (state == true) {
			soIdCol();
			soDateCol();
			siteLocationCol();
			
			editCostCodeCol();
			editCarpetAreaCol();
			
			editBudgetCol();
			editSoAmountCol();
			editPayableAmountCol();
		}else{
			soIdCol();
			soDateCol();
			siteLocationCol();
			
			costCodeCol();
			carpetAreaCol();
			
			budgetCol();
			soAmountCol();
			payableAmountCol();
			
		}
		
	}

	@Override
	public void applyStyle() {
		
	}

}
