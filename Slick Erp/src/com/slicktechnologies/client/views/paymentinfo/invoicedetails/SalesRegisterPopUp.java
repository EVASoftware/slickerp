package com.slicktechnologies.client.views.paymentinfo.invoicedetails;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.services.CommonService;
import com.slicktechnologies.client.services.CommonServiceAsync;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.server.GeneralServiceImpl;
import com.slicktechnologies.server.addhocdownload.CsvWriter;
import com.slicktechnologies.shared.CreditNote;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;

/**27-10- 2017 sagar sore [to download csv of summary of invoice details i.e. by clicking sales register button in invoice list]**/
public class SalesRegisterPopUp extends PopupScreen{
	
	protected GenricServiceAsync service=GWT.create(GenricService.class);
	final CsvServiceAsync csvservice=GWT.create(CsvService.class);
	IntegerBox ibInvoiceId;
//	public ListBox lbStatus;
	public ListBox lbStatus ,lbinvoicetype ; //Added by Ashwini
	ObjectListBox<Branch> olbBranch;
	
	DateComparator dateComparator;
	public PersonInfoComposite personInfo;
	MyQuerry myQuerryStatic;
	ArrayList<Invoice> invoices;
	Logger logger = Logger.getLogger("SalesRegisterPopUp");
	
	/**
	 * Date 07/06/2018 By vijay
	 * Des :- search with number Range
	 * Requirements :- Neatedge Services
	 */
	ObjectListBox<Config> olbcNumberRange;
	/**
	 * ends here
	 */
	/**
	 * Updated By: Viraj
	 * Date: 15-05-2019
	 * Description: To add check box for distinguishing between  master franchise or franchise with process config
	 */
	public CheckBox cbReport;
	protected GeneralServiceAsync general = GWT.create(GeneralService.class);
	/** Ends **/
	
	DateComparator creditNotedateComparator;
	Vector<Filter> filtervec2;
	DateTimeFormat sdf = DateTimeFormat.getFormat("dd-MM-yyyy");
	public boolean ultimaTallyExportFlag=false;
	
	public SalesRegisterPopUp(){
		super();
		createGui();
		invoices = new ArrayList();
	}

	private void initilizewidget() {
		dateComparator = new DateComparator("invoiceDate",new Invoice());
		ibInvoiceId = new IntegerBox();
		olbBranch = new ObjectListBox<>();
		AppUtility.makeBranchListBoxLive(olbBranch);
		lbStatus= new ListBox();
		AppUtility.setStatusListBox(lbStatus,Invoice.getStatusList());	
		
		/*
		 * Date:25-10-2018
		 * Developer:Ashwini
		 * Des:To add invoice document type
		 */
		
		lbinvoicetype = new ListBox();
		AppUtility.setInvoiceDocumentType(lbinvoicetype);
		
		/*
		 * End by Ashwini
		 */
		
		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new Customer());
		personInfo=new PersonInfoComposite(querry,false,true,false);
		personInfo.getCustomerId().getHeaderLabel().setText("Customer ID");
		personInfo.getCustomerName().getHeaderLabel().setText("Customer Name");
		personInfo.getCustomerCell().getHeaderLabel().setText("Customer Cell");
		
		olbcNumberRange= new ObjectListBox<Config>();
	    AppUtility.MakeLiveConfig(olbcNumberRange,Screen.NUMBERRANGE);	
	    
	    cbReport = new CheckBox();
	    
	    creditNotedateComparator = new DateComparator("creditNoteDate",new CreditNote());
	}
	
	@Override
	public void createScreen() {
		initilizewidget();
		
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingInvoiceDetails=fbuilder.setlabel("Invoice Details").widgetType(FieldType.GREYGROUPING).setMandatory(false).setColSpan(5).build();
		
		fbuilder = new FormFieldBuilder("From Date",dateComparator.getFromDate());
		FormField fdbFromDate = fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("To Date",dateComparator.getToDate());
		FormField fdbToDate = fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Invoice Id",ibInvoiceId);
		FormField fibInvoiceId = fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Branch",olbBranch);
		FormField folbBranch = fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Status",lbStatus);
		FormField ftbStatus = fbuilder.setRowSpan(0).setColSpan(0).build();
		
		/*
		 * Date:22-10-2018
		 * Developer:Ashwini
		 * Des:To add Invoice Document Type ie.tax invoice or proforma invoice
		 */
		
		fbuilder = new FormFieldBuilder("Invoice Document Type",lbinvoicetype);
		FormField flbinvoicetype= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/*
		 * end by Ashwini
		 */
		
		fbuilder = new FormFieldBuilder("",personInfo);
		FormField fPersonInfo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		
		
		fbuilder = new FormFieldBuilder("Number Range",olbcNumberRange);
		FormField folbcNumberRange= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Report",cbReport);
		FormField fcbReport= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/**
		 * Updated By: Viraj
		 * Date: 15-05-2019
		 * Description: To add check box for distinguishing between  master franchise or franchise with process config
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("SalesRegister","Report")) {
			FormField[][] formfield = {
					{fdbFromDate,fdbToDate},
					{fibInvoiceId,folbBranch},
					{ftbStatus,folbcNumberRange},
					{flbinvoicetype,fcbReport},
	     			{fPersonInfo},   
//					{flbinvoicetype} //Added by Ashwini
			};
			
			this.fields=formfield;
		} 
		/** Ends **/
		else {
			FormField[][] formfield = {
					{fdbFromDate,fdbToDate},
					{fibInvoiceId,folbBranch},
					{ftbStatus,folbcNumberRange},
					{flbinvoicetype},
	     			{fPersonInfo},   
//					{flbinvoicetype} //Added by Ashwini
			};
			
			this.fields=formfield;
		}

	}


	public MyQuerry getQuerry() {
		Vector<Filter> filtervec=new Vector<Filter>();
		filtervec2=new Vector<Filter>();
		Filter temp=null;
		boolean personinfoFlag=false;
		
		if(ibInvoiceId.getValue()!=null){
			temp=new Filter();
			logger.log(Level.SEVERE," invoie id "+ibInvoiceId.getValue());
			temp.setIntValue(ibInvoiceId.getValue());
			filtervec.add(temp);
			temp.setQuerryString("count");
			filtervec.add(temp);
			MyQuerry querry= new MyQuerry();
			querry.setFilters(filtervec);
			querry.setQuerryObject(new Invoice());
			return querry;
		}
		
		if(personInfo.getIdValue()!=-1)
		{  
			temp=new Filter();
			temp.setIntValue(personInfo.getIdValue());
			logger.log(Level.SEVERE," cust id "+personInfo.getIdValue());
			filtervec.add(temp);
			temp.setQuerryString("personInfo.count");
			filtervec.add(temp);
			personinfoFlag=true;
		}

		if(!(personInfo.getFullNameValue().equals("")))
		{
			temp=new Filter();
			logger.log(Level.SEVERE," cust name "+personInfo.getFullNameValue());
			temp.setStringValue(personInfo.getFullNameValue());
			filtervec.add(temp);
			temp.setQuerryString("personInfo.fullName");
			filtervec.add(temp);
		}
		
		
		 if(personInfo.getCellValue()!=-1)
		  {
		  temp=new Filter();
		  temp.setLongValue(personInfo.getCellValue());
			filtervec.add(temp);
		  temp.setQuerryString("personInfo.cellNumber");
		  filtervec.add(temp);
		  }
		
		if(dateComparator.getValue()!=null){
			if(personinfoFlag)
				filtervec2.addAll(dateComparator.getValue());
			else
				filtervec.addAll(dateComparator.getValue());
		}
		if(olbBranch.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(olbBranch.getItemText(olbBranch.getSelectedIndex()));
			temp.setQuerryString("branch");
			if(personinfoFlag)
				filtervec2.add(temp);
			else
				filtervec.add(temp);
		}
		
		if(lbStatus.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(lbStatus.getItemText(lbStatus.getSelectedIndex()));
			
			temp.setQuerryString("status");
			
			if(personinfoFlag)
				filtervec2.add(temp);
			else
				filtervec.add(temp);
		}
			
		/*
		 * Added by Ashwini
		 */
		if(lbinvoicetype.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(lbinvoicetype.getItemText(lbinvoicetype.getSelectedIndex()));
			
			temp.setQuerryString("invoiceType");
			if(personinfoFlag)
				filtervec2.add(temp);
			else
				filtervec.add(temp);
		}  
		 
		/*
		 * end by Ashwini
		 */
		
		
		 
		 if(olbcNumberRange.getSelectedIndex()!=0){
				temp=new Filter();
				temp.setStringValue(olbcNumberRange.getValue().trim());
				temp.setQuerryString("numberRange");
				if(personinfoFlag)
					filtervec2.add(temp);
				else
					filtervec.add(temp);
			}
		 
		Console.log("filtervec size="+filtervec.size());
		Console.log("filtervec2 size="+filtervec2.size());
		MyQuerry querry= new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Invoice());
		return querry;
	}	

	@Override
	public void onClick(ClickEvent event) {
		MyQuerry myQuerry = getQuerry();
		myQuerryStatic = myQuerry;
	
		Console.log("in salesregisterpopup onclick");
		/**
		 * Updated By: Viraj
		 * Date: 15-05-2019
		 * Description: To add check box for distinguishing between  master franchise or franchise with process config
		 */
		if(cbReport.getValue().equals(true)) {
			Console.log("in salesregisterpopup condition 1");
			if(personInfo.getFullNameValue() == null || personInfo.getFullNameValue().equals("")) {
				Window.alert("Customer name is mandatory");
			} else if(dateComparator.getfromDateValue() == null || dateComparator.gettoDateValue() == null) {
				Window.alert("Date is mandatory");
			} else {
				general.fetchSalesRegisterList(UserConfiguration.getCompanyId(), personInfo.getIdValue(), dateComparator.getfromDateValue(), dateComparator.gettoDateValue(), new AsyncCallback<String>() {

					@Override
					public void onFailure(Throwable caught) {
						caught.printStackTrace();
					}

					@Override
					public void onSuccess(String result) {
						
						String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
						final String url = gwt+ "csvservlet"+ "?type=" + 172;
						Window.open(url, "test","enabled");
						
					}
				});
			}
		} else {
			
		/** Ends **/
			
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "EnableSalesRegisterWithBranchRevenueBifurcation")){
				Console.log("in salesregisterpopup condition 2");
				reactonDonwloadSalesRegisterWithCreditNoteDetails();
			}
			else if(ultimaTallyExportFlag){
				Console.log("in salesregisterpopup condition 3");
				showWaitSymbol();
				service.getSearchResult(myQuerryStatic,new AsyncCallback<ArrayList<SuperModel>>() {
					@SuppressWarnings("unchecked")
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						ArrayList<Invoice> invoicelist = new ArrayList<Invoice>();
						Console.log( "size of result list "+ result.size());
						if (result.size() != 0) {
							for (SuperModel model : result) {
								Invoice invoice = (Invoice) model;
//								logger.log(Level.SEVERE, "name: "+ invoice.getName() + " :: "+ invoice.getCount());
								invoicelist.add(invoice);
							}
							Console.log( "invoicelist fetched size="+ invoicelist.size());
							if(filtervec2.size()>0){
								invoicelist=applyInternalFilter(invoicelist, filtervec2);
								if(invoicelist!=null&&invoicelist.size()>0){
									invoices.clear();
									invoices.addAll(invoicelist);
									Console.log("Size of invoice "+ invoices.size());
									csvDownload(invoices);	
								}else{
									Window.alert("Data not Found");
									hideWaitSymbol();
								}
							}else{
								System.out.println("Name of class "+ result.get(0).getClass().getName());								
								invoices.clear();
								invoices.addAll(invoicelist);
								Console.log("Size of invoice "+ invoices.size());
								csvDownload(invoices);		//Updated By: Viraj Date: 16-05-2019								
							}

							
						} else {
							Window.alert("Data not Found");
							hideWaitSymbol();
						}
					}
					@Override
					public void onFailure(Throwable caught) {
						caught.printStackTrace();
						hideWaitSymbol();
					}
				});
				
			}else {

				Console.log("in salesregisterpopup condition 4");
				showWaitSymbol();
				service.getSearchResult(myQuerryStatic,new AsyncCallback<ArrayList<SuperModel>>() {
					@SuppressWarnings("unchecked")
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						ArrayList<Invoice> invoicelist = new ArrayList<Invoice>();
						Console.log( "size of result list "+ result.size());
						if (result.size() != 0) {
							for (SuperModel model : result) {
								Invoice invoice = (Invoice) model;
//								logger.log(Level.SEVERE, "name: "+ invoice.getName() + " :: "+ invoice.getCount());
								invoicelist.add(invoice);
							}
							Console.log( "invoicelist fetched size="+ invoicelist.size());
							if(filtervec2.size()>0){
								invoicelist=applyInternalFilter(invoicelist, filtervec2);
								if(invoicelist!=null&&invoicelist.size()>0){
									invoices.clear();
									invoices.addAll(invoicelist);
									Console.log("Size of invoice "+ invoices.size());
									csvDownload(invoices);	
								}else{
									Window.alert("Data not Found");
									hideWaitSymbol();
								}
							}else{
								System.out.println("Name of class "+ result.get(0).getClass().getName());								
								invoices.clear();
								invoices.addAll(invoicelist);
								Console.log("Size of invoice "+ invoices.size());
								csvDownload(invoices);		//Updated By: Viraj Date: 16-05-2019								
							}

							
						} else {
							Window.alert("Data not Found");
							hideWaitSymbol();
						}
					}
					@Override
					public void onFailure(Throwable caught) {
						caught.printStackTrace();
						hideWaitSymbol();
					}
				});
				
			
			}
		
	}
	this.hidePopUp();
	}
	
	

	

	private void reactonDonwloadSalesRegisterWithCreditNoteDetails() {
		Company company = new Company();
		int invoiceid = 0;
		if(ibInvoiceId.getValue()!=null){
			invoiceid = ibInvoiceId.getValue();

		}
		CommonServiceAsync commonservice = GWT.create(CommonService.class);
		showWaitSymbol();
		commonservice.getSalesRegisterData(myQuerryStatic, dateComparator.getfromDateValue(), dateComparator.gettoDateValue(), invoiceid, company.getCompanyId(), new AsyncCallback<ArrayList<SuperModel>>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				hideWaitSymbol();
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				if (result.size() != 0) {
					ArrayList<Invoice> invoicelist = new ArrayList<Invoice>();
					for (SuperModel model : result) {
						Invoice invoice = (Invoice) model;
						invoicelist.add(invoice);
					}
					if(filtervec2.size()>0){
						invoicelist=applyInternalFilter(invoicelist, filtervec2);
						if(invoicelist!=null&&invoicelist.size()>0){
							invoices.clear();
							invoices.addAll(invoicelist);
							Console.log("Size of invoice "+ invoices.size());
							csvDownload(invoices);	
						}else{
							Window.alert("Data not Found");
							hideWaitSymbol();
						}
					}else{
						invoices.clear();
						invoices.addAll(invoicelist);
						csvDownload(invoices);	
					}
				} else {
					Window.alert("Data not Found");
					hideWaitSymbol();
				}
			}
		});
	}

	/**
	 * Updated By: Viraj
	 * Date: 16-05-2019
	 * Description: made csv method common 
	 * @param invoices
	 */
	private void csvDownload(ArrayList<Invoice> invoices) {
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("SalesRegister","Report")){
			
			String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
			final String url = gwt+ "csvservlet"+ "?type=" + 128;
			Window.open(url, "test","enabled");
			hideWaitSymbol();
		}else if(ultimaTallyExportFlag) { 

			csvservice.setSalesList(invoices,new AsyncCallback<Void>() {
					@SuppressWarnings("unchecked")
					@Override
					public void onFailure(Throwable caught) {
						System.out.println("RPC call Failed"+ caught);
						hideWaitSymbol();
					}

					@Override
					public void onSuccess(Void result) {
						Timer t = new Timer() {
							@Override
							public void run() {
								String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
								final String url = gwt+ "csvservlet"+ "?type=" + 221;
								Window.open(url, "test","enabled");
								hideWaitSymbol();
							}
						};
						t.schedule(2000);
					}
				});
		
		}else {
			csvservice.setSalesList(invoices,new AsyncCallback<Void>() {
//				csvservice.setinvoicelist(invoices,new AsyncCallback<Void>() {
					@SuppressWarnings("unchecked")
					@Override
					public void onFailure(Throwable caught) {
						System.out.println("RPC call Failed"+ caught);
						hideWaitSymbol();
					}

					@Override
					public void onSuccess(Void result) {
						Timer t = new Timer() {
							@Override
							public void run() {
								String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
								final String url = gwt+ "csvservlet"+ "?type=" + 128;
								Window.open(url, "test","enabled");
								hideWaitSymbol();
							}
						};
						t.schedule(2000);
					}
				});
		}
	}
	/** Ends **/

	public static int ordinalIndexOf(String str, String substr, int n) {
	    int pos = str.indexOf(substr);
	    while (--n > 0 && pos != -1)
	        pos = str.indexOf(substr, pos + 1);
	    return pos;
	}

	protected MyQuerry getCreditNoteQuerry() {
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		
		if(creditNotedateComparator.getValue()!=null){
			filtervec.addAll(creditNotedateComparator.getValue());
		}
		
		if(ibInvoiceId.getValue()!=null){
			temp=new Filter();
			logger.log(Level.SEVERE," invoie id "+ibInvoiceId.getValue());
			temp.setIntValue(ibInvoiceId.getValue());
			filtervec.add(temp);
			temp.setQuerryString("invoiceId");
			filtervec.add(temp);
		}
		
		MyQuerry querry= new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new CreditNote());
		return querry;
		
	}

	//Ashwini Patil Date:26-07-2023 As per Nitin sir's requirement if customer name is there in sales register search then we need to fetch data based on customer and then internally apply remaining filters instead of adding datastore index.
	private ArrayList<Invoice> applyInternalFilter(ArrayList<Invoice> fetchedInvoicelist,Vector<Filter> filtervec){


		ArrayList<Invoice> invoicelist=new ArrayList<Invoice>();
		invoicelist=fetchedInvoicelist;
		for(Filter filter:filtervec2){
			ArrayList<Invoice> filteredInvoicelist = new ArrayList<Invoice>();
			
			String conditionField = filter.getQuerryString();
			Object value = filter.geFilterValue();
			Console.log("conditionField="+conditionField+" value="+value+ " filteredInvoicelist size="+filteredInvoicelist.size() );
			if(conditionField.equals("invoiceDate >=")){
				for(Invoice i:invoicelist){
					if(sdf.format(i.getInvoiceDate()).equals(sdf.format((Date) value))||i.getInvoiceDate().after((Date) value))
						filteredInvoicelist.add(i);
				}
				Console.log("filteredInvoicelist size after="+filteredInvoicelist.size());
				invoicelist=filteredInvoicelist;
			}
			if(conditionField.equals("invoiceDate <=")){
				Console.log("in invoiceDate <= invoicelist size="+invoicelist.size());
				
				for(Invoice i:invoicelist){
//					Console.log("i.getInvoiceDate()="+i.getInvoiceDate());
					if(i.getInvoiceDate().before((Date) value)||sdf.format(i.getInvoiceDate()).equals(sdf.format((Date) value))){
						filteredInvoicelist.add(i);
					}
					Console.log("after if");
				}
				Console.log("filteredInvoicelist size after="+filteredInvoicelist.size());
				invoicelist=filteredInvoicelist;
			}
			if(conditionField.equals("branch")){
				for(Invoice i:invoicelist){
					if(i.getBranch().equals(value))
						filteredInvoicelist.add(i);
				}
				Console.log("filteredInvoicelist size after="+filteredInvoicelist.size());
				invoicelist=filteredInvoicelist;
			}
			if(conditionField.equals("status")){
				for(Invoice i:invoicelist){
					if(i.getStatus().equals(value))
						filteredInvoicelist.add(i);
				}	
				Console.log("filteredInvoicelist size after="+filteredInvoicelist.size());
				invoicelist=filteredInvoicelist;
			}
			if(conditionField.equals("invoiceType")){
				for(Invoice i:invoicelist){
					if(i.getInvoiceType().equals(value))
						filteredInvoicelist.add(i);
				}
				Console.log("filteredInvoicelist size after="+filteredInvoicelist.size());
				invoicelist=filteredInvoicelist;
			}
			if(conditionField.equals("numberRange")){
				for(Invoice i:invoicelist){
					if(i.getNumberRange().equals(value))
						filteredInvoicelist.add(i);
				}
				Console.log("filteredInvoicelist size after="+filteredInvoicelist.size());
				invoicelist=filteredInvoicelist;
			}
		}
		
		Console.log("returning invoicelist size="+invoicelist.size());
		return invoicelist;

	}
}

