package com.slicktechnologies.client.views.paymentinfo.invoicedetails;

import java.util.ArrayList;

import com.google.gwt.dom.client.Style.BorderStyle;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.google.gwt.view.client.RowCountChangeEvent.Handler;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.SalesAnnexureType;
import com.slicktechnologies.shared.common.salesprocess.SalesOrderProductLineItem;

public class SalesAnnexureTypePopup extends PopupScreen implements Handler {

	
	SalesAnnexureTypeTable annexureTbl;
	Invoice model;
	
	public SalesAnnexureTypePopup() {
		super(FormStyle.DEFAULT);
		createGui();
		annexureTbl.connectToLocal();
	}
	
	@Override
	public void applyStyle() {
		content.getElement().getStyle().setWidth(800, Unit.PX);
		content.getElement().getStyle().setHeight(520, Unit.PX);
		content.getElement().getStyle().setBorderWidth(1, Unit.PX);
		content.getElement().getStyle().setBorderStyle(BorderStyle.SOLID);
		form.getElement().setId("form");
		horizontal.getElement().addClassName("popupcentering");
		lblOk.getElement().setId("addbutton");
		lblCancel.getElement().setId("addbutton");
		popup.center();
	}
	
	private void initializeWidget() {
		annexureTbl=new SalesAnnexureTypeTable();
		annexureTbl.getTable().addRowCountChangeHandler(this);
	}
	
	@Override
	public void onClick(ClickEvent event) {
		
	}

	@Override
	public void createScreen() {
		initializeWidget();
		
		FormFieldBuilder fbuilder;
		
		fbuilder = new FormFieldBuilder();
		FormField fVenGrping=fbuilder.setlabel("Annexure Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",annexureTbl.getTable());
		FormField fannexureTbl= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		FormField[][] formfield = {
				{fVenGrping},
				{fannexureTbl},
		};

		this.fields=formfield;
	}

	@Override
	public void onRowCountChange(RowCountChangeEvent event) {
		for(SalesAnnexureType type:annexureTbl.getValue()){
			double payableAmount = 0;
			payableAmount=type.getPayableAmount();
			
			type.setCgstAmt((payableAmount*type.getCgstPer())/100);
			type.setSgstAmt((payableAmount*type.getSgstPer())/100);
			type.setIgstAmt((payableAmount*type.getIgstPer())/100);
			type.setUtgstAmt((payableAmount*type.getUtgstPer())/100);
			
			type.setGrandTotal(type.getPayableAmount()+type.getCgstAmt()+type.getSgstAmt()+type.getIgstAmt()+type.getUtgstAmt());
			
		}
		
		annexureTbl.getTable().redraw();
	}
	
	
	public void initializeAnnexureDetails(){
		if(model!=null){
			if(model.getSalesOrderProducts()!=null){
				Console.log("Annexure Details Found!");
				initiateAnnexureDetails();
			}
		}else{
			showDialogMessage("Invoice details can not be null.");
		}
	}

	private void initiateAnnexureDetails() {
		ArrayList<SalesAnnexureType> annexureList=new ArrayList<SalesAnnexureType>();
		for(SalesOrderProductLineItem item:model.getSalesOrderProducts()){
			if(item.getSalesAnnexureMap()!=null&&item.getSalesAnnexureMap().size()!=0){
				annexureList.addAll(item.getSalesAnnexureMap().get(item.getBillProductName()));
			}
		}
		
		Console.log("Sales Annexure List "+annexureList.size());
		if(annexureList!=null&&annexureList.size()!=0){
			annexureTbl.getDataprovider().getList().addAll(annexureList);
			if(model.getStatus().equals(Invoice.CREATED)){
				annexureTbl.setEnable(true);
				lblOk.setVisible(true);
			}else{
				annexureTbl.setEnable(false);
				lblOk.setVisible(false);
			}
			showPopUp();
		}else {
			showDialogMessage("No annexure details found!");
		}
	}
	
	
	
	public SalesAnnexureTypeTable getSalesAnnexureTbl() {
		return annexureTbl;
	}

	public void setSalesAnnexureTbl(SalesAnnexureTypeTable annexureTbl) {
		this.annexureTbl = annexureTbl;
	}

	public Invoice getModel() {
		return model;
	}

	public void setModel(Invoice model) {
		annexureTbl.connectToLocal();
		this.model = model;
	}
	
	
	public ArrayList<SalesAnnexureType> getSalesAnnexureListProdWise(String billProdName){
		ArrayList<SalesAnnexureType> list=new ArrayList<SalesAnnexureType>();
		for(SalesAnnexureType type:annexureTbl.getValue()){
			if(type.getBillProductName().equals(billProdName)){
				list.add(type);
			}
		}
		return list;
	}
	
	public double getPriceFromSalesAnnexureList(ArrayList<SalesAnnexureType> list){
		double price=0;
		for(SalesAnnexureType type:list){
			price=price+type.getPayableAmount();
		}
		return price;
	}


}
