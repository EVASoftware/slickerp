package com.slicktechnologies.client.views.paymentinfo.invoicedetails;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.common.salesprocess.AnnexureType;
import com.slicktechnologies.shared.common.salesprocess.SalesOrderProductLineItem;

public class AnnexureTypeTable extends SuperTable<AnnexureType> {

	TextColumn<AnnexureType> sitelocationCol;
	TextColumn<AnnexureType> designationCol;
	
	TextColumn<AnnexureType> noOfStaffCol;
	TextColumn<AnnexureType> noOfPresentDaysCol;
	TextColumn<AnnexureType> noOfNormatOtDaysCol;
	TextColumn<AnnexureType> noOfNHOtDaysCol;
	TextColumn<AnnexureType> noOfPHOtDaysCol;
	
	Column<AnnexureType,String> editNoOfStaffCol;
	Column<AnnexureType,String> editNoOfPresentDaysCol;
	Column<AnnexureType,String> editNoOfNormatOtDaysCol;
	Column<AnnexureType,String> editNoOfNHOtDaysCol;
	Column<AnnexureType,String> editNoOfPHOtDaysCol;
	
	GWTCAlert alert;
	NumberFormat nf=NumberFormat.getFormat("0.00");
	
	public boolean staffBillFlag=false;
	public boolean normalOtFlag=false;
	public boolean phOtFlag=false;
	public boolean nhOtFlag=false;
	
//	public double width=0;
	
	public AnnexureTypeTable() {
		super();
		setHeight("400px");
	}
	
	@Override
	public void createTable() {
		// TODO Auto-generated method stub
		alert=new GWTCAlert();
//		width=100/7;
//		Console.log("Width : "+width);
		sitelocationCol();
		designationCol();
		
//		noOfStaffCol();
//		noOfPresentDaysCol();
//		noOfNormatOtDaysCol();
//		noOfNHOtDaysCol();
//		noOfPHOtDaysCol();
		
		editNoOfStaffCol();
		editNoOfPresentDaysCol();
		editNoOfNormatOtDaysCol();
		editNoOfNHOtDaysCol();
		editNoOfPHOtDaysCol();
	}

	private void sitelocationCol() {
		sitelocationCol=new TextColumn<AnnexureType>() {
			@Override
			public String getValue(AnnexureType object) {
				if(object.getLocationName()!=null){
					return object.getLocationName();
				}
				return "";
			}
		};
		table.addColumn(sitelocationCol,"Site Location");
//		table.setColumnWidth(sitelocationCol, width, Unit.PCT);
	}

	private void designationCol() {
		designationCol=new TextColumn<AnnexureType>() {
			@Override
			public String getValue(AnnexureType object) {
				if(object.getDesignation()!=null){
					return object.getDesignation();
				}
				return "";
			}
		};
		table.addColumn(designationCol,"Designation");
//		table.setColumnWidth(designationCol, width, Unit.PCT);
	}

	private void noOfStaffCol() {
		// TODO Auto-generated method stub
		noOfStaffCol=new TextColumn<AnnexureType>() {
			@Override
			public String getValue(AnnexureType object) {
				if(object.getNoOfStaff()!=null){
					return object.getNoOfStaff();
				}
				return "";
			}
		};
		table.addColumn(noOfStaffCol,"No. Of Staff");
//		table.setColumnWidth(noOfStaffCol, width, Unit.PCT);
	}

	private void noOfPresentDaysCol() {
		noOfPresentDaysCol=new TextColumn<AnnexureType>() {
			@Override
			public String getValue(AnnexureType object) {
				return nf.format(object.getNoOfDaysPresent())+"";
			}
		};
		table.addColumn(noOfPresentDaysCol,"No. Of Present Days");
//		table.setColumnWidth(noOfPresentDaysCol, width, Unit.PCT);
	}

	private void noOfNormatOtDaysCol() {
		noOfNormatOtDaysCol=new TextColumn<AnnexureType>() {
			@Override
			public String getValue(AnnexureType object) {
				return nf.format(object.getNormalDayOt());
			}
		};
		table.addColumn(noOfNormatOtDaysCol,"No. Of Normal OT Days");
//		table.setColumnWidth(noOfNormatOtDaysCol, width, Unit.PCT);
	}

	private void noOfNHOtDaysCol() {
		noOfNHOtDaysCol=new TextColumn<AnnexureType>() {
			@Override
			public String getValue(AnnexureType object) {
				return nf.format(object.getNhOt());
			}
		};
		table.addColumn(noOfNHOtDaysCol,"No. Of NH OT Days");
//		table.setColumnWidth(noOfNHOtDaysCol, width, Unit.PCT);
	}

	private void noOfPHOtDaysCol() {
		noOfPHOtDaysCol=new TextColumn<AnnexureType>() {
			@Override
			public String getValue(AnnexureType object) {
				return nf.format(object.getPhOt());
			}
		};
		table.addColumn(noOfPHOtDaysCol,"No. Of PH OT Days");
//		table.setColumnWidth(noOfPHOtDaysCol, width, Unit.PCT);
	}

	private void editNoOfStaffCol() {
		EditTextCell editCell=new EditTextCell();
		editNoOfStaffCol=new Column<AnnexureType,String>(editCell) {
			@Override
			public String getValue(AnnexureType object) {
				if(object.getNoOfStaff()!=null&&!object.getNoOfStaff().equals("")){
					return object.getNoOfStaff()+"";
				}
				return "";
			}
		};
		table.addColumn(editNoOfStaffCol,"#No. Of Staff");
//		table.setColumnWidth(editNoOfStaffCol, width, Unit.PCT);
		
		editNoOfStaffCol.setFieldUpdater(new FieldUpdater<AnnexureType, String>(){
			@Override
			public void update(int index, AnnexureType object,String value) {
				try{
					if(value!=null){
						double price=Double.parseDouble(value);
						object.setNoOfStaff(price+"");
					}else{
						object.setNoOfStaff(0.0+"");
					}
				}
				catch (Exception e){
					alert.alert("Please enter numeric value only.");
				}
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				table.redrawRow(index);
			}
		});
	}

	private void editNoOfPresentDaysCol() {
		EditTextCell editCell=new EditTextCell();
		editNoOfPresentDaysCol=new Column<AnnexureType,String>(editCell) {
			@Override
			public String getValue(AnnexureType object) {
				return nf.format(object.getNoOfDaysPresent())+"";
			}
			
//			@Override
//			public void onBrowserEvent(Context context, Element elem,AnnexureType object, NativeEvent event) {
//				if(object.getNoOfDaysPresent()>0){
//					super.onBrowserEvent(context, elem, object, event);
//				}
//			}
		};
		table.addColumn(editNoOfPresentDaysCol,"#No. Of Present Days");
//		table.setColumnWidth(editNoOfPresentDaysCol, width, Unit.PCT);
		
		editNoOfPresentDaysCol.setFieldUpdater(new FieldUpdater<AnnexureType, String>(){
			@Override
			public void update(int index, AnnexureType object,String value) {
				try{
					if(value!=null){
						double price=Double.parseDouble(value);
						object.setNoOfDaysPresent(price);
					}else{
						object.setNoOfDaysPresent(0.0);
					}
				}
				catch (Exception e){
					alert.alert("Please enter numeric value only.");
				}
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				table.redrawRow(index);
			}
		});
	}

	private void editNoOfNormatOtDaysCol() {
		EditTextCell editCell=new EditTextCell();
		editNoOfNormatOtDaysCol=new Column<AnnexureType,String>(editCell) {
			@Override
			public String getValue(AnnexureType object) {
				return nf.format(object.getNormalDayOt())+"";
			}
		};
		table.addColumn(editNoOfNormatOtDaysCol,"#No. Of Normal OT Days");
//		table.setColumnWidth(editNoOfNormatOtDaysCol, width, Unit.PCT);
		
		editNoOfNormatOtDaysCol.setFieldUpdater(new FieldUpdater<AnnexureType, String>(){
			@Override
			public void update(int index, AnnexureType object,String value) {
				try{
					if(value!=null){
						double price=Double.parseDouble(value);
						object.setNormalDayOt(price);
					}else{
						object.setNormalDayOt(0.0);
					}
				}
				catch (Exception e){
					alert.alert("Please enter numeric value only.");
				}
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				table.redrawRow(index);
			}
		});
	}

	private void editNoOfNHOtDaysCol() {
		EditTextCell editCell=new EditTextCell();
		editNoOfNHOtDaysCol=new Column<AnnexureType,String>(editCell) {
			@Override
			public String getValue(AnnexureType object) {
				return nf.format(object.getNhOt())+"";
			}
		};
		table.addColumn(editNoOfNHOtDaysCol,"#No. Of NH OT Days");
//		table.setColumnWidth(editNoOfNHOtDaysCol, width, Unit.PCT);
		
		editNoOfNHOtDaysCol.setFieldUpdater(new FieldUpdater<AnnexureType, String>(){
			@Override
			public void update(int index, AnnexureType object,String value) {
				try{
					if(value!=null){
						double price=Double.parseDouble(value);
						object.setNhOt(price);
					}else{
						object.setNhOt(0.0);
					}
				}
				catch (Exception e){
					alert.alert("Please enter numeric value only.");
				}
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				table.redrawRow(index);
			}
		});
	}

	private void editNoOfPHOtDaysCol() {
		EditTextCell editCell=new EditTextCell();
		editNoOfNHOtDaysCol=new Column<AnnexureType,String>(editCell) {
			@Override
			public String getValue(AnnexureType object) {
				return nf.format(object.getPhOt())+"";
			}
		};
		table.addColumn(editNoOfNHOtDaysCol,"#No. Of PH OT Days");
//		table.setColumnWidth(editNoOfNHOtDaysCol, width, Unit.PCT);
		
		editNoOfNHOtDaysCol.setFieldUpdater(new FieldUpdater<AnnexureType, String>(){
			@Override
			public void update(int index, AnnexureType object,String value) {
				try{
					if(value!=null){
						double price=Double.parseDouble(value);
						object.setPhOt(price);
					}else{
						object.setPhOt(0.0);
					}
				}
				catch (Exception e){
					alert.alert("Please enter numeric value only.");
				}
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				table.redrawRow(index);
			}
		});
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		for (int i = table.getColumnCount() - 1; i > -1; i--)
			table.removeColumn(i);
		
//		Console.log("setEnable Width : "+width);
		if (state == true) {
			sitelocationCol();
			designationCol();
			
			
			editNoOfStaffCol();
			
			if(staffBillFlag){
				editNoOfPresentDaysCol();
			}
			if(normalOtFlag){
				editNoOfNormatOtDaysCol();
			}
			if(nhOtFlag){
				editNoOfNHOtDaysCol();
			}
			if(phOtFlag){
				editNoOfPHOtDaysCol();
			}
		}else{
			sitelocationCol();
			designationCol();
			
			noOfStaffCol();
			if(staffBillFlag){
				noOfPresentDaysCol();
			}
			if(normalOtFlag){
				noOfNormatOtDaysCol();
			}
			if(nhOtFlag){
				noOfNHOtDaysCol();
			}
			if(phOtFlag){
				noOfPHOtDaysCol();
			}
		}
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
