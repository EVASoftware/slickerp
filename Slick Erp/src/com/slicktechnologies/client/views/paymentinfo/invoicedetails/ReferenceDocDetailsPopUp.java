package com.slicktechnologies.client.views.paymentinfo.invoicedetails;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.TextBox;

public class ReferenceDocDetailsPopUp extends AbsolutePanel implements ClickHandler {

	/**
	 * Added by Rahul Verma on 10 Feb 2017
	 *  This is needed by NBHC
	 *
	 */
	TextBox tbRefInvoiceId;
	DoubleBox dbNetValue,dbTotalAmount,dbTaxAmount;
	

	Button btnOk;
	// Done Adding by Rahul
	public InlineLabel  lRefInvoiceID,lNetValue,lTotalAmount,lTaxAmount;
	
	
	
	public ReferenceDocDetailsPopUp() {
		super();
		// TODO Auto-generated constructor stub
		InlineLabel remark1 = new InlineLabel("Reference Document Details");
		add(remark1, 150, 10);
		remark1.getElement().getStyle().setFontSize(20, Unit.PX);
		
		//Added by Rahul
		tbRefInvoiceId=new TextBox();
		tbRefInvoiceId.setEnabled(false);
		dbNetValue=new DoubleBox();
		dbNetValue.setEnabled(false);
		dbTaxAmount=new DoubleBox();
		dbTaxAmount.setEnabled(false);
		dbTotalAmount=new DoubleBox();
		dbTotalAmount.setEnabled(false);
		
		lRefInvoiceID= new InlineLabel("Reference Invoice ID");
		add(lRefInvoiceID, 30,30);
		add(tbRefInvoiceId, 30,50);
		
		lNetValue= new InlineLabel("Net Value");
		add(lNetValue, 230,30);
		add(dbNetValue, 230,50);
		
		lTaxAmount= new InlineLabel("Tax Amount");
		add(lTaxAmount, 30,130);
		add(dbTaxAmount, 30,150);
		
		lTotalAmount= new InlineLabel("Total Amount");
		add(lTotalAmount, 230,130);
		add(dbTotalAmount, 230,150);
		
				
		btnOk = new Button("Ok");
		add(btnOk, 250, 450);
		setSize("500px", "250px");
	}

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
	}

	public TextBox getTbRefInvoiceId() {
		return tbRefInvoiceId;
	}

	public void setTbRefInvoiceId(TextBox tbRefInvoiceId) {
		this.tbRefInvoiceId = tbRefInvoiceId;
	}

	public DoubleBox getDbNetValue() {
		return dbNetValue;
	}

	public void setDbNetValue(DoubleBox dbNetValue) {
		this.dbNetValue = dbNetValue;
	}

	public DoubleBox getDbTotalAmount() {
		return dbTotalAmount;
	}

	public void setDbTotalAmount(DoubleBox dbTotalAmount) {
		this.dbTotalAmount = dbTotalAmount;
	}

	public DoubleBox getDbTaxAmount() {
		return dbTaxAmount;
	}

	public void setDbTaxAmount(DoubleBox dbTaxAmount) {
		this.dbTaxAmount = dbTaxAmount;
	}

	public Button getBtnOk() {
		return btnOk;
	}

	public void setBtnOk(Button btnOk) {
		this.btnOk = btnOk;
	}

	public InlineLabel getlRefInvoiceID() {
		return lRefInvoiceID;
	}

	public void setlRefInvoiceID(InlineLabel lRefInvoiceID) {
		this.lRefInvoiceID = lRefInvoiceID;
	}

	public InlineLabel getlNetValue() {
		return lNetValue;
	}

	public void setlNetValue(InlineLabel lNetValue) {
		this.lNetValue = lNetValue;
	}

	public InlineLabel getlTotalAmount() {
		return lTotalAmount;
	}

	public void setlTotalAmount(InlineLabel lTotalAmount) {
		this.lTotalAmount = lTotalAmount;
	}

	public InlineLabel getlTaxAmount() {
		return lTaxAmount;
	}

	public void setlTaxAmount(InlineLabel lTaxAmount) {
		this.lTaxAmount = lTaxAmount;
	}
	
	
	

}
