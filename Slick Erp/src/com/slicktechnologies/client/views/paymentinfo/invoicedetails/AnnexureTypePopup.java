package com.slicktechnologies.client.views.paymentinfo.invoicedetails;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.BorderStyle;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.StatusCodeException;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.google.gwt.view.client.RowCountChangeEvent.Handler;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.cnc.CncBillService;
import com.slicktechnologies.client.views.cnc.CncBillServiceAsync;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.salesprocess.AnnexureType;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.SalesOrderProductLineItem;

public class AnnexureTypePopup extends PopupScreen implements Handler {
	
	AnnexureTypeTable annexureTbl;
	
	Invoice model;
	
	CncBillServiceAsync cncBillAsync = GWT.create(CncBillService.class);
	
	public AnnexureTypePopup() {
		super(FormStyle.DEFAULT);
		createGui();
		annexureTbl.connectToLocal();
	}
	
	@Override
	public void applyStyle() {
		content.getElement().getStyle().setWidth(800, Unit.PX);
		content.getElement().getStyle().setHeight(520, Unit.PX);
		content.getElement().getStyle().setBorderWidth(1, Unit.PX);
		content.getElement().getStyle().setBorderStyle(BorderStyle.SOLID);
		form.getElement().setId("form");
		horizontal.getElement().addClassName("popupcentering");
		lblOk.getElement().setId("addbutton");
		lblCancel.getElement().setId("addbutton");
		popup.center();
	}
	
	private void initializeWidget() {
		annexureTbl=new AnnexureTypeTable();
		annexureTbl.getTable().addRowCountChangeHandler(this);
	}
	
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void createScreen() {
		initializeWidget();
		
		FormFieldBuilder fbuilder;
		
		fbuilder = new FormFieldBuilder();
		FormField fVenGrping=fbuilder.setlabel("Annexure Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",annexureTbl.getTable());
		FormField fannexureTbl= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		FormField[][] formfield = {
				{fVenGrping},
				{fannexureTbl},
		};

		this.fields=formfield;
	}

	@Override
	public void onRowCountChange(RowCountChangeEvent event) {
		// TODO Auto-generated method stub
		for(AnnexureType type:annexureTbl.getValue()){
			double otAmount=0;
			double totalAmount = 0;
			
			totalAmount=totalAmount+(type.getBasePrice()*type.getNoOfDaysPresent());
			
			otAmount=otAmount+(type.getBasePriceNormalOt()*type.getNormalDayOt());
			otAmount=otAmount+(type.getBasePriceNhOt()*type.getNhOt());
			otAmount=otAmount+(type.getBasePricePhOt()*type.getPhOt());
			
			totalAmount=totalAmount+otAmount;
			
			type.setOtAmount(otAmount);
			type.setTotalAmount(totalAmount);
			
			type.setCgstAmt((totalAmount*type.getCgstPer())/100);
			type.setSgstAmt((totalAmount*type.getSgstPer())/100);
			type.setIgstAmt((totalAmount*type.getIgstPer())/100);
			
			type.setNetTotalAmt(type.getTotalAmount()+type.getCgstAmt()+type.getSgstAmt()+type.getIgstAmt());
			
			Console.log("Location: "+type.getLocationName()+" Designation: "+type.getDesignation()+" NOS: "+type.getNoOfStaff());
			Console.log("NOP: "+type.getNoOfDaysPresent()+" NONOT: "+type.getNormalDayOt()+" NONHOT: "+type.getNhOt()+" NOPHOT: "+type.getPhOt());
			Console.log("NOP: "+type.getBasePrice()+" NONOT: "+type.getBasePriceNormalOt()+" NONHOT: "+type.getBasePriceNhOt()+" NOPHOT: "+type.getBasePricePhOt());
			Console.log("OT AMOUNT: "+type.getOtAmount()+" Total Amount: "+type.getTotalAmount());
		}
		
		annexureTbl.getTable().redraw();
	}
	
	
	public void initializeAnnexureDetails(){
		if(model!=null){
			if(model.getAnnexureMap()!=null){
				Console.log("Annexure Details Found!");
				initiateAnnexureDetails(model.getAnnexureMap());
			}else{
				Console.log("Retrieving Annexure Details Found!");
				showWaitSymbol();
				cncBillAsync.getAnnexureDetailsForCNCBill(UserConfiguration.getCompanyId(), model, new AsyncCallback<HashMap<String,ArrayList<AnnexureType>>>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						hideWaitSymbol();
						if (caught instanceof StatusCodeException && ((StatusCodeException) caught).getStatusCode() == 0) {
							showDialogMessage("Server unreachable, Please check your internet connection!");
				        }
					}

					@Override
					public void onSuccess(HashMap<String,ArrayList<AnnexureType>> result) {
						// TODO Auto-generated method stub
						hideWaitSymbol();
						initiateAnnexureDetails(result);
					}
				});
			}
		}else{
			showDialogMessage("Invoice details can not be null.");
		}
	}

	private void initiateAnnexureDetails(HashMap<String, ArrayList<AnnexureType>> result) {
		ArrayList<AnnexureType> annexureList=new ArrayList<AnnexureType>();
		if(result!=null){
			for (Map.Entry<String, ArrayList<AnnexureType>> entry : result.entrySet()){
				annexureList.addAll(entry.getValue());
			}
			
			try{
				Comparator<AnnexureType> annexureComp =new Comparator<AnnexureType>() {
					@Override
					public int compare(AnnexureType o1, AnnexureType o2) {
						return o1.getLocationName().compareTo(o2.getLocationName());
					}
				};
				Collections.sort(annexureList , annexureComp);
			}catch(Exception e){
				
			}
			
			annexureTbl.getDataprovider().getList().addAll(annexureList);
			int counter=3;
			if(model!=null){
				
				if(model.getSubBillType()!=null&&!model.getSubBillType().equals("")){
					if(model.getSubBillType().equals("Public Holiday")
							||model.getSubBillType().equals("National Holiday")
							||model.getSubBillType().equals("Overtime")
							||model.getSubBillType().equals("Holiday")
							||model.getSubBillType().equals("Staffing")
							||model.getSubBillType().equals("Staffing And Rental")
							||model.getSubBillType().equals("Labour")){
						
						
						if(model.getSubBillType().equals("Staffing")
								||model.getSubBillType().equals("Staffing And Rental")
								||model.getSubBillType().equals("Labour")){
							annexureTbl.staffBillFlag=true;
							counter++;
						}
						if(model.getSubBillType().equals("Overtime")){
							annexureTbl.normalOtFlag=true;
							counter++;
						}
						if(model.getSubBillType().equals("National Holiday")||model.getSubBillType().equals("Holiday")){
							annexureTbl.nhOtFlag=true;
							counter++;
						}
						if(model.getSubBillType().equals("Public Holiday")){
							annexureTbl.phOtFlag=true;
							counter++;
						}
					}else if(model.getSubBillType().equals("Consolidate Bill")){
											
						for(SalesOrderProductLineItem product : model.getSalesOrderProducts()){
							if(product.getBillType()!=null && !product.getBillType().equals("")){
								if(product.getBillType().equals("Overtime")
										||product.getBillType().equals("Holiday")
										||product.getBillType().equals("National Holiday")
										||product.getBillType().equals("Public Holiday")
										||product.getBillType().equals("Staffing")
										||product.getBillType().equals("Staffing And Rental")
										||product.getBillType().equals("Labour")){
									
									
									if(product.getBillType().equals("Staffing")
											||product.getBillType().equals("Staffing And Rental")
											||product.getBillType().equals("Labour")){
										annexureTbl.staffBillFlag=true;
										counter++;
									}
									
									if(product.getBillType().equals("Overtime")){
										annexureTbl.normalOtFlag=true;
										counter++;
									}
									if(product.getBillType().equals("National Holiday")||product.getBillType().equals("Holiday")){
										annexureTbl.nhOtFlag=true;
										counter++;
									}
									
									if(product.getBillType().equals("Public Holiday")){
										annexureTbl.phOtFlag=true;
										counter++;
									}
									
								}
							}
						}
					}
				}
			}
			
//			annexureTbl.width=100/counter;
			if(model.getStatus().equals(Invoice.CREATED)){
				annexureTbl.setEnable(true);
				lblOk.setVisible(true);
			}else{
				annexureTbl.setEnable(false);
				lblOk.setVisible(false);
			}
			showPopUp();
			
		}else {
			showDialogMessage("No annexure details found!");
		}
	}
	
	public HashMap<String, ArrayList<AnnexureType>> getAnnexureDetailsInMap(){
		HashMap<String, ArrayList<AnnexureType>> annexureMap=new HashMap<String, ArrayList<AnnexureType>>();
		
		
		for(AnnexureType type:annexureTbl.getValue()){
			
			String key=type.getLocationName()+type.getDesignation()+type.getCtcAmount();
			if(annexureMap.size()==0){
				ArrayList<AnnexureType> value=new ArrayList<AnnexureType>();
				value.add(type);
				annexureMap.put(key, value);
			}else{
				if(annexureMap.containsKey(key)){
					ArrayList<AnnexureType> list=annexureMap.get(key);
					type.setNoOfDaysPresent(type.getNoOfDaysPresent()+list.get(0).getNoOfDaysPresent());
					type.setNormalDayOt(type.getNormalDayOt()+list.get(0).getNormalDayOt());
					type.setPhOt(type.getPhOt()+list.get(0).getPhOt());
					type.setNhOt(type.getNhOt()+list.get(0).getNhOt());
					type.setOtAmount(type.getOtAmount()+list.get(0).getOtAmount());
					type.setTotalAmount(type.getTotalAmount()+list.get(0).getTotalAmount());
					type.setCgstAmt(type.getCgstAmt()+list.get(0).getCgstAmt());
					type.setSgstAmt(type.getSgstAmt()+list.get(0).getSgstAmt());
					type.setIgstAmt(type.getIgstAmt()+list.get(0).getIgstAmt());
					type.setNetTotalAmt(type.getNetTotalAmt()+list.get(0).getNetTotalAmt());
					
					
					if(type.getBasePrice()!=0){
						type.setBasePrice(type.getBasePrice());
					}else{
						type.setBasePrice(list.get(0).getBasePrice());
					}
					
					if(type.getBasePriceNormalOt()!=0){
						type.setBasePriceNormalOt(type.getBasePriceNormalOt());
					}else{
						type.setBasePriceNormalOt(list.get(0).getBasePriceNormalOt());
					}
					
					if(type.getBasePriceNhOt()!=0){
						type.setBasePriceNhOt(type.getBasePriceNhOt());
					}else{
						type.setBasePriceNhOt(list.get(0).getBasePriceNhOt());
					}
					
					if(type.getBasePricePhOt()!=0){
						type.setBasePricePhOt(type.getBasePricePhOt());
					}else{
						type.setBasePricePhOt(list.get(0).getBasePricePhOt());
					}
					
					ArrayList<AnnexureType> value=new ArrayList<AnnexureType>();
					value.add(type);
					annexureMap.put(key, value);
					
				}else{
					ArrayList<AnnexureType> value=new ArrayList<AnnexureType>();
					value.add(type);
					annexureMap.put(key, value);
				}
			}
		}
		
		return annexureMap;
	}
	
	public AnnexureTypeTable getAnnexureTbl() {
		return annexureTbl;
	}

	public void setAnnexureTbl(AnnexureTypeTable annexureTbl) {
		this.annexureTbl = annexureTbl;
	}

	public Invoice getModel() {
		return model;
	}

	public void setModel(Invoice model) {
		annexureTbl.connectToLocal();
		this.model = model;
	}
	
	
	

}
