package com.slicktechnologies.client.views.paymentinfo.invoicedetails;

import com.google.gwt.user.client.ui.TextArea;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;

public class DescriptionPopup extends PopupScreen{

	
	TextArea taDescription;
	
	
	public DescriptionPopup(){
		super();
		createGui();
		
	}
	
	@Override
	public void createScreen() {

		taDescription = new TextArea();
		taDescription.setHeight("400px");
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField downalodFormatInfo = fbuilder.setlabel("Description").setColSpan(4).widgetType(FieldType.Grouping).build();
		
		
		fbuilder = new FormFieldBuilder("", taDescription);
		FormField ftaDescription = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		
		FormField[][] formfield = {
				{ downalodFormatInfo },
				{ftaDescription },
				};
		this.fields = formfield;
	}

	public TextArea gettaDescription() {
		return taDescription;
	}

	public void settaDescription(TextArea taDescription) {
		this.taDescription = taDescription;
	}

	
	
}

