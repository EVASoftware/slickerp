package com.slicktechnologies.client.views.paymentinfo.vendorinvoicelist;

import java.util.Vector;

import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.VendorInvoice;

public class VendorInvoiceListSearchProxy extends SearchPopUpScreen<VendorInvoice> implements SelectionHandler<Suggestion>{
	
	public IntegerBox ibinvoiceid;
	public IntegerBox ibcontractid;
	public PersonInfoComposite personInfo;
	public PersonInfoComposite vendorInfo;
	public DateComparator dateComparator;
	public ListBox lbstatus,lbinvoicetype;
	public ObjectListBox<Employee> olbApproverName;
	public TextBox tbinvoicetype;
	public ListBox lbaccountType;
	
	public ObjectListBox<Branch> olbBranch;
	
	/**03-10-2017 sagar sore [ To add field for number range in Invoice List search pop up]***/
	public ObjectListBox<Config> olbcNumberRange;
	
	public VendorInvoiceListSearchProxy() {
		super();
		createGui();
	}
	
	public VendorInvoiceListSearchProxy(boolean b) {
		super(b);
		createGui();
	}
	
	public void initWidget()
	{
		ibinvoiceid=new IntegerBox();
		ibcontractid=new IntegerBox();
		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new Customer());
		personInfo=new PersonInfoComposite(querry,false);
		
		
		MyQuerry vendorquerry=new MyQuerry();
		vendorquerry.setQuerryObject(new Vendor());
		vendorInfo=new PersonInfoComposite(vendorquerry,false);
		
		dateComparator=new DateComparator("invoiceDate",new Invoice());
		lbstatus= new ListBox();
		AppUtility.setStatusListBox(lbstatus,Invoice.getStatusList());
		lbinvoicetype=new ListBox();
		lbinvoicetype.addItem("SELECT");
		lbinvoicetype.addItem("Proforma Invoice");
		lbinvoicetype.addItem("Tax Invoice");
		olbApproverName=new ObjectListBox<Employee>();
		AppUtility.makeSalesPersonListBoxLive(olbApproverName);
		
		
		lbaccountType=new ListBox();
		lbaccountType.addItem("SELECT");
		lbaccountType.addItem(AppConstants.BILLINGACCOUNTTYPEAP);
		lbaccountType.addItem(AppConstants.BILLINGACCOUNTTYPEAR);
		
		personInfo.getId().addSelectionHandler(this);
		vendorInfo.getId().addSelectionHandler(this);
		
		personInfo.getCustomerId().getHeaderLabel().setText("Customer ID");
		personInfo.getCustomerName().getHeaderLabel().setText("Customer Name");
		personInfo.getCustomerCell().getHeaderLabel().setText("Customer Cell");
		
		vendorInfo.getCustomerId().getHeaderLabel().setText("Vendor ID");
		vendorInfo.getCustomerName().getHeaderLabel().setText("Vendor Name");
		vendorInfo.getCustomerCell().getHeaderLabel().setText("Vendor Cell");
		
	//***********rohan changes here *******************
		
		olbBranch= new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbBranch);
		
		/**03-10-2017 sagar sore[ olbcNumberRange initialization]***/
		olbcNumberRange= new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbcNumberRange,Screen.NUMBERRANGE);
	}
	
	public void createScreen()
	{
		initWidget();
		FormFieldBuilder builder;
		builder = new FormFieldBuilder("Invoice ID",ibinvoiceid);
		FormField fibinvoiceid= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("",personInfo);
		FormField fPersonInfo= builder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		builder = new FormFieldBuilder("",vendorInfo);
		FormField fvendorInfo= builder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		builder = new FormFieldBuilder("Order ID",ibcontractid);
		FormField fibcontractid= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("From Date (Invoice Date)",dateComparator.getFromDate());
		FormField fdateComparatorfrom= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("To Date (Invoice Date)",dateComparator.getToDate());
		FormField fdateComparatorto= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Invoice Document Type",lbinvoicetype);
		FormField flbinvoicetype= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Status",lbstatus);
		FormField flbstatus=builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Approver Name",olbApproverName);
		FormField folbApproverName= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Account Type",lbaccountType);
		FormField flbaccountType= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Branch",olbBranch);
		FormField folbBranch= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/**03-10-2017 sagar sore[ adding search by number range]**/
		FormField folbcNumberRange;	
		builder = new FormFieldBuilder("Number Range",olbcNumberRange);
		folbcNumberRange= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		this.fields=new FormField[][]{
				/**03-10-2017 sagar sore [ Number range field added and order of fields updated]**/
				/** old code **/
//				{fdateComparatorfrom,fdateComparatorto,folbBranch},
//				{fibinvoiceid,flbinvoicetype,fibcontractid},
//				{flbstatus,folbApproverName,flbaccountType},
//				{fPersonInfo},
//				{fvendorInfo}
				/** updated order **/
				{fdateComparatorfrom,fdateComparatorto,folbBranch},
				{fibinvoiceid,flbinvoicetype,folbcNumberRange},
				{flbstatus,fibcontractid,folbApproverName},//flbaccountType
				{fPersonInfo},
				{fvendorInfo}
		};
	}
	

	@Override
	public MyQuerry getQuerry() {
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		
		if(dateComparator.getValue()!=null)
			filtervec.addAll(dateComparator.getValue());
		
		if(ibinvoiceid.getValue()!=null){
			temp=new Filter();
			temp.setIntValue(ibinvoiceid.getValue());
			filtervec.add(temp);
			temp.setQuerryString("count");
			filtervec.add(temp);
		}
		
		if(ibcontractid.getValue()!=null){
			temp=new Filter();
			temp.setIntValue(ibcontractid.getValue());
			filtervec.add(temp);
			temp.setQuerryString("contractCount");
			filtervec.add(temp);
		}
		if(lbinvoicetype.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(lbinvoicetype.getItemText(lbinvoicetype.getSelectedIndex()));
			filtervec.add(temp);
			temp.setQuerryString("invoiceType");
			filtervec.add(temp);
		}
		
		
		//if(lbaccountType.getSelectedIndex()!=0){
		/** date 25.6.2018 added by komal for - by default loading of AR **/
		temp=new Filter();
		temp.setStringValue(AppConstants.BILLINGACCOUNTTYPEAP);
		temp.setQuerryString("accountType");
		filtervec.add(temp);
	//	}
		
		
		
		if(personInfo.getIdValue()!=-1)
		  {
		  temp=new Filter();
		  temp.setIntValue(personInfo.getIdValue());
		  temp.setQuerryString("personInfo.count");
		  filtervec.add(temp);
		  }
		  
		  if(!(personInfo.getFullNameValue().equals("")))
		  {
		  temp=new Filter();
		  temp.setStringValue(personInfo.getFullNameValue());
		  temp.setQuerryString("personInfo.fullName");
		  filtervec.add(temp);
		  }
		  if(personInfo.getCellValue()!=-1l)
		  {
		  temp=new Filter();
		  temp.setLongValue(personInfo.getCellValue());
		  temp.setQuerryString("personInfo.cellNumber");
		  filtervec.add(temp);
		  }
		  
		  if(vendorInfo.getIdValue()!=-1)
		  {
		  temp=new Filter();
		  temp.setIntValue(vendorInfo.getIdValue());
		  temp.setQuerryString("personInfo.count");
		  filtervec.add(temp);
		  }
		  
		  if(!(vendorInfo.getFullNameValue().equals("")))
		  {
		  temp=new Filter();
		  temp.setStringValue(vendorInfo.getFullNameValue());
		  temp.setQuerryString("personInfo.fullName");
		  filtervec.add(temp);
		  }
		  if(vendorInfo.getCellValue()!=-1l)
		  {
		  temp=new Filter();
		  temp.setLongValue(vendorInfo.getCellValue());
		  temp.setQuerryString("personInfo.cellNumber");
		  filtervec.add(temp);
		  }
		  

			if(olbBranch.getSelectedIndex()!=0){
				temp=new Filter();
				temp.setStringValue(olbBranch.getValue().trim());
				temp.setQuerryString("branch");
				filtervec.add(temp);
			}
		  
		  
		  
		  if(lbstatus.getSelectedIndex()!=0){
				temp=new Filter();
				temp.setStringValue(lbstatus.getItemText(lbstatus.getSelectedIndex()));
				filtervec.add(temp);
				temp.setQuerryString("status");
				filtervec.add(temp);
			}
		  if(olbApproverName.getSelectedIndex()!=0){
				temp=new Filter();temp.setStringValue(olbApproverName.getValue().trim());
				temp.setQuerryString("approverName");
				filtervec.add(temp);
			}
		  /**03-10-2017 sagar sore[ filter for search by number range i.e. Billing and Non-billing]**/
			if(olbcNumberRange.getSelectedIndex()!=0){
				temp=new Filter();
				temp.setStringValue(olbcNumberRange.getValue().trim());
				temp.setQuerryString("numberRange");
				filtervec.add(temp);
			}
		MyQuerry querry= new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new VendorInvoice());
		return querry;
	}
	
	
	
	@Override
	public void onSelection(SelectionEvent<Suggestion> event) 
	{
		if(event.getSource().equals(personInfo.getId())||event.getSource().equals(personInfo.getName())||event.getSource().equals(personInfo.getPhone()))
		{
			if(!vendorInfo.getId().getValue().equals("")||!vendorInfo.getName().getValue().equals("")||!vendorInfo.getPhone().getValue().equals(""))
			{
				vendorInfo.clear();
			}
		}
		
		if(event.getSource().equals(vendorInfo.getId())||event.getSource().equals(vendorInfo.getName())||event.getSource().equals(vendorInfo.getPhone()))
		{
			if(!personInfo.getId().getValue().equals("")&&!personInfo.getName().getValue().equals("")&&!personInfo.getPhone().getValue().equals(""))
			{
				personInfo.clear();
			}
		}
	}

	@Override
	public boolean validate() {
		if(LoginPresenter.branchRestrictionFlag)
		{
		if(olbBranch.getSelectedIndex()==0)
		{
			showDialogMessage("Select Branch");
			return false;
		}
		}
		return true;
	}
	
	
	

}
