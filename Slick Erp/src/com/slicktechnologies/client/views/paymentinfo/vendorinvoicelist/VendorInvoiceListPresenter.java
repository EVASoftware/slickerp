package com.slicktechnologies.client.views.paymentinfo.vendorinvoicelist;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;

import com.google.code.p.gwtchismes.client.GWTCGlassPanel;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.view.client.NoSelectionModel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreen;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreenPresenter;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.UpdateService;
import com.slicktechnologies.client.services.UpdateServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.contract.ContractService;
import com.slicktechnologies.client.views.contract.ContractServiceAsync;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.FromAndToDateBoxPopup;
import com.slicktechnologies.client.views.paymentinfo.invoicedetails.InvoiceDetailsForm;
import com.slicktechnologies.client.views.paymentinfo.invoicedetails.InvoiceDetailsPresenter;
import com.slicktechnologies.client.views.paymentinfo.invoicedetails.InvoiceDetailsSearchPopUp;
import com.slicktechnologies.client.views.paymentinfo.invoicedetails.SalesRegisterPopUp;
import com.slicktechnologies.client.views.paymentinfo.invoicelist.FumigationOptPopup;
import com.slicktechnologies.client.views.paymentinfo.invoicelist.InvoiceListForm;
import com.slicktechnologies.client.views.paymentinfo.invoicelist.InvoiceListPresenter;
import com.slicktechnologies.client.views.paymentinfo.invoicelist.InvoiceListSearchPopUp;
import com.slicktechnologies.client.views.paymentinfo.invoicelist.InvoiceListTable;
import com.slicktechnologies.client.views.paymentinfo.vendorinvoicedetails.VendorInvoiceDetailsForm;
import com.slicktechnologies.client.views.paymentinfo.vendorinvoicedetails.VendorInvoiceDetailsPresenter;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.VendorInvoice;

public class VendorInvoiceListPresenter extends TableScreenPresenter<VendorInvoice> {
	
	TableScreen<VendorInvoice>form;
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	final ContractServiceAsync async=GWT.create(ContractService.class);
	static GWTCGlassPanel glassPanel=new GWTCGlassPanel();
	
	/*
	 *  nidhi
	 *  	1-07-2017
	 */
	FromAndToDateBoxPopup frmAndToPop = new FromAndToDateBoxPopup();
	PopupPanel panel ;
	/*
	 *  end
	 */
	/*
	 *  nidhi
	 *  	10-07-2017
	 */
		Boolean flagforrepot ;
	/*
	 *  end
	 */

		/**24-10-2017 sagar sore [ For adding sales register popup]**/
		SalesRegisterPopUp salesRegisterPopUp;
		/**
		 * 	Created By : nidhi
		 *  Date : 18-11-2017
		 *  Discription : add for download fumigation value and contract revanue details 
		 *  
		 *  
		 */
		UpdateServiceAsync updateSer = GWT.create(UpdateService.class);
		/**
		 *  nidhi
		 *  28-01-2018
		 *  for fumigation report opt popup
		 *  
		 * @param view
		 * @param model
		 */
		FumigationOptPopup fumOptPopup;
		boolean serviceRevanue = false;
		/**
		 * end
		 * @param view
		 * @param model
		 */
	public VendorInvoiceListPresenter(TableScreen<VendorInvoice> view, VendorInvoice model) {
		super(view, model);
		form=view;
		/**
		 *  nidhi
		 *  24-07-2017
		 *  this method stop calling for not display invoice list on page load
		 *  
		 */
		//view.retriveTable(getInvoiceListQuery());
		
		/**
		 *  end
		 */
		setTableSelectionOnService();
		
		/*
		 *  nidhi
		 *  	1-07-2017
		 */
		
		frmAndToPop.btnOne.addClickHandler(this);
		frmAndToPop.btnTwo.addClickHandler(this);
		/*
		 *  end
		 */
		/**
		 * nidhi
		 * 
		 */
		fumOptPopup = new FumigationOptPopup();
		fumOptPopup.getLblOk().addClickHandler(this);
		fumOptPopup.getLblCancel().addClickHandler(this);
		/**
		 * end
		 */
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.VENDORINVOICELIST,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
		
	}
	
	public VendorInvoiceListPresenter(TableScreen<VendorInvoice> view,
			VendorInvoice model,MyQuerry querry) {
		super(view, model);
		form=view;
		view.retriveTable(querry);
		
		setTableSelectionOnService();
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.VENDORINVOICELIST,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void reactOnDownload() {
		
		ArrayList<VendorInvoice> invoicingarray=new ArrayList<VendorInvoice>();
		List<VendorInvoice> listbill=(List<VendorInvoice>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
		invoicingarray.addAll(listbill); 
		csvservice.setVendorinvoicelist(invoicingarray, new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);
				
			}

			@Override
			public void onSuccess(Void result) {
				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "csvservlet"+"?type="+142;
				Window.open(url, "test", "enabled");
			}
		});
	}

	
	/*
	 * Method template to make a new model
	 */
	@Override
	protected void makeNewModel() {
		
		model=new VendorInvoice();
	}
	
	public static void initalize()
	{
		
//		glassPanel.show();
//		Timer timer=new Timer() 
//   	 	{
//			@Override
//			public void run() 
//			{
				VendorInvoiceListTable table=new VendorInvoiceListTable();
				VendorInvoiceListForm form=new VendorInvoiceListForm(table);
				//InvoiceDetailsForm billingDtlsForm=new InvoiceDetailsForm();
				VendorInvoiceListSearchProxy.staticSuperTable=table;
				VendorInvoiceListSearchProxy searchPopUp=new VendorInvoiceListSearchProxy(false);
				searchPopUp.getDwnload().setVisible(false);
				form.setSearchpopupscreen(searchPopUp);
				VendorInvoiceListPresenter presenter=new VendorInvoiceListPresenter(form, new VendorInvoice());
				form.setToViewState();
				/**
				 * Date : 29-07-2017 BY ANIL
				 * This line was added to update the name of screen loaded from any other screen or by clicking on billing details on account model
				 */
				AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Accounts/Vendor Invoice List",Screen.VENDORINVOICELIST);
				/**
				 * END
				 */
				AppMemory.getAppMemory().stickPnel(form);
//				glassPanel.hide();
//			}
//		};
//		timer.schedule(3000); 
	}
	
	public static void initalize(MyQuerry querry)
	{
		//AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Invoice Details",Screen.INVOICELIST);
		InvoiceListTable table=new InvoiceListTable();
		InvoiceListForm form=new InvoiceListForm(table);
		InvoiceListSearchPopUp.staticSuperTable=table;
		InvoiceListSearchPopUp searchPopUp=new InvoiceListSearchPopUp(false);
		form.setSearchpopupscreen(searchPopUp);
		InvoiceListPresenter presenter=new InvoiceListPresenter(form, new Invoice(),querry);
		form.setToViewState();
		/**
		 * Date : 29-07-2017 BY ANIL
		 * This line was added to update the name of screen loaded from any other screen or by clicking on billing details on account model
		 */
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Accounts/Vendor Invoice List",Screen.VENDORINVOICELIST);
		/**
		 * END
		 */
		AppMemory.getAppMemory().stickPnel(form);
	}
	
	/*
	 * Method template to set the query object in specific to search criteria and return the same
	 * @return MyQuerry object
	 */
	public MyQuerry getInvoiceListQuery()
	{
		MyQuerry quer=new MyQuerry();
		
		Vector<Filter> filtervec=new Vector<Filter>();
		List<String> statusList=new ArrayList<String>();
		statusList.add(Invoice.CREATED);
		statusList.add(Invoice.REQUESTED);
		
		Filter temp=null;
		
		Date invDate=new Date();
		CalendarUtil.addDaysToDate(invDate, 1);
		
		temp=new Filter();
		temp.setLongValue(model.getCompanyId());
		temp.setQuerryString("companyId");
		filtervec.add(temp);
		
		temp=new Filter();
		temp.setList(statusList);
		temp.setQuerryString("status IN");
		filtervec.add(temp);
		
		
		/**
		 * Developed by : Rohan added this branch  
		 * 
		 */
		
		List<String> branchList ;
		branchList= new ArrayList<String>();
		System.out.println("item count in branch "+LoginPresenter.globalBranch.size());
		for (int i = 0; i < LoginPresenter.globalBranch.size(); i++) {
			branchList.add(LoginPresenter.globalBranch.get(i).getBusinessUnitName());
		}
		System.out.println("branchList list "+branchList.size());
		
		temp = new Filter();
		temp.setQuerryString("branch IN");
		temp.setList(branchList);
		filtervec.add(temp);
		/**
		 * ends here 
		 */
		
		temp=new Filter();
		temp.setDateValue(invDate);
		temp.setQuerryString("invoiceDate <=");
		filtervec.add(temp);
		
		quer.setFilters(filtervec);
		quer.setQuerryObject(new Invoice());
		
		
		return quer;
	}
	
	 public void setTableSelectionOnService()
	 {
		 final NoSelectionModel<VendorInvoice> selectionModelMyObj = new NoSelectionModel<VendorInvoice>();
	     
	     SelectionChangeEvent.Handler  tableHandler = new SelectionChangeEvent.Handler() 
	     {
	         @Override
	         public void onSelectionChange(SelectionChangeEvent event) 
	         {
	        	 final VendorInvoice entity=selectionModelMyObj.getLastSelectedObject();
	        	 AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
	        	 InvoiceDetailsForm.flag=false;
	        	
        		
	        	 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Vendor Invoice Details",Screen.VENDORINVOICEDETAILS);
				 AppMemory.getAppMemory().stickPnel(form);
				 
	        	 final VendorInvoiceDetailsForm form = VendorInvoiceDetailsPresenter.initalize();

				 Timer timer=new Timer() 
	        	 {
	 				@Override
	 				public void run() {
	 					System.out.println("Rohan in side invoice list ");
					     form.updateView(entity);
					     form.setToViewState();
					     VendorInvoiceDetailsPresenter presenter=(VendorInvoiceDetailsPresenter) form.getPresenter();
					     if(!entity.getStatus().equals(Invoice.CREATED)){
					    	 presenter.form.setAppHeaderBarAsPerStatus();
					     }
	 			             
	 				}
	 			};
	            
	             timer.schedule(3000); 
	          }
	     };
	     // Add the handler to the selection model
	     selectionModelMyObj.addSelectionChangeHandler( tableHandler );
	     // Add the selection model to the table
	     form.getSuperTable().getTable().setSelectionModel(selectionModelMyObj);
	     
		 
		 
	 }

	
	 @Override
		public void reactToProcessBarEvents(ClickEvent e) {
//			super.reactToProcessBarEvents(e);
			
			InlineLabel label = (InlineLabel) e.getSource();
			String text = label.getText().trim();

			if(text.equals("Invoice Acknowledgment")) {
				final String url = GWT.getModuleBaseURL()+"pdfprintservice"+"?Id="+UserConfiguration.getCompanyId()
						+"&"+"type="+"IA";
				Window.open(url, "test", "enabled");
			}else if(text.equals("Revenue Report")){
				flagforrepot=true;
				panel = new PopupPanel(true);
				panel.add(frmAndToPop);
				panel.show();
				panel.center();
			}else if(text.equals("Technician Revenue Report")){
				flagforrepot=false;
				panel = new PopupPanel(true);
				panel.add(frmAndToPop);
				panel.show();
				panel.center();
			}
			/**24-10-2017 sagar sore [Action for sales register search popup]**/
			if (text.equals(AppConstants.SALESREGISTER))
			{
				salesRegisterPopUp = new SalesRegisterPopUp();
				salesRegisterPopUp.showPopUp();
				salesRegisterPopUp.getLblCancel().setVisible(false);
				salesRegisterPopUp.getLblOk().setText("Go");
			} 
			/**
			 *  nidhi
			 *  10-11-2017
			 *  for Daily fumigation report 
			 *  
			 */
			if(text.equals(AppConstants.SERVICEFUMIGATIONREPORT)){
//				updateSer.dailyFumigationReportDetails(model.getCompanyId(), null, new AsyncCallback<Void>() {
//					
//					@Override
//					public void onSuccess(Void result) {
//
//						String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
//						final String url=gwt + "CreateXLXSServlet"+"?type="+1;
//						Window.open(url, "test", "enabled");
//					}
//					
//					@Override
//					public void onFailure(Throwable caught) {
//						// TODO Auto-generated method stub
//						
//					}
//				});
				serviceRevanue = false;
				fumOptPopup.showPopUp();
				fumOptPopup.getFgroupingExpenseDetails().getHeaderLabel().setText(AppConstants.SERVICEFUMIGATIONREPORT);
			}
			
			
			/**
			 *  nidhi
			 *  16-11-2017
			 *  for Daily fumigation value report 
			 *  
			 */
			if(text.equals(AppConstants.SERVICEFUMIGATIONVALUEREPORT)){
				
				serviceRevanue = true;
				fumOptPopup.showPopUp();
				fumOptPopup.getFgroupingExpenseDetails().getHeaderLabel().setText(AppConstants.SERVICEFUMIGATIONVALUEREPORT);
			}
		}
	 
	 @Override
			public void onClick(ClickEvent event) {
			 	super.onClick(event);
//			 try {
//				 	if(event.getSource()==frmAndToPop.btnOne){
//				 		if(frmAndToPop.getFromDate().getValue()==null&&frmAndToPop.getToDate().getValue()==null){
//				 			form.showDialogMessage("Please select from and to date.");
//				 			return;
//				 		}
//				 		if(frmAndToPop.getToDate().getValue().before(frmAndToPop.getFromDate().getValue())){
//				 			form.showDialogMessage("To date should be greater than from date.");
//				 			return;
//				 		}
//				 		form.showWaitSymbol();
//				 		if(flagforrepot){
//				 			async.serviceRevanueReport(model.getCompanyId(), frmAndToPop.getFromDate().getValue(), frmAndToPop.getToDate().getValue(),"", new AsyncCallback<String>(){
//	
//								@Override
//								public void onFailure(Throwable caught) {
//									// TODO Auto-generated method stub
//									form.hideWaitSymbol();
//								}
//	
//								@Override
//								public void onSuccess(String result) {
//									// TODO Auto-generated method stub
//									form.hideWaitSymbol();
//									if(result.equals("Success")){
//										String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
//										final String url = gwt + "csvservlet" + "?type=" + 123;
//										Window.open(url, "test", "enabled");
//									}else{
//										form.showDialogMessage(result);
//									}
//									
//								}
//					 			
//					 		});
//					 		panel.hide();
//				 		}else{
//				 			async.technicianRevanueReport(model.getCompanyId(), frmAndToPop.getFromDate().getValue(), frmAndToPop.getToDate().getValue(), "",new AsyncCallback<String>(){
//	
//								@Override
//								public void onFailure(Throwable caught) {
//									// TODO Auto-generated method stub
//									form.hideWaitSymbol();
//								}
//	
//								@Override
//								public void onSuccess(String result) {
//									// TODO Auto-generated method stub
//									form.hideWaitSymbol();
//									if(result.equals("Success")){
//										String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
//										final String url = gwt + "csvservlet" + "?type=" + 124;
//										Window.open(url, "test", "enabled");
//									}else{
//										form.showDialogMessage(result);
//									}
//									
//								}
//					 			
//					 		});
//					 		panel.hide();
//				 		}
//				 		
//					}
//					
//					if(event.getSource()==frmAndToPop.btnTwo){
//						panel.hide();
//					}
//					 if(event.getSource() == fumOptPopup.getLblOk()){
//						 if(fumOptPopup.getDbfromDate().getValue()!=null && fumOptPopup.getDbToDate().getValue()!=null){
//							 if(fumOptPopup.getDbToDate().getValue().before(fumOptPopup.getDbfromDate().getValue())){
//						 			form.showDialogMessage("To date should be greater than from date.");
//						 			return;
//						 		}
//						 }
//						
//						 Date toDate=null,fromDate = null;
//						 if(fumOptPopup.getDbfromDate().getValue() == null && fumOptPopup.getDbToDate().getValue() == null){
//							 toDate = new Date();
//							 fromDate = null;
//						 }else if(fumOptPopup.getDbfromDate().getValue() == null || fumOptPopup.getDbToDate().getValue() == null){
//							 if(fumOptPopup.getDbfromDate().getValue() != null){
//								 toDate = fumOptPopup.getDbfromDate().getValue();
//							 }else if(fumOptPopup.getDbToDate().getValue() != null){
//								 toDate = fumOptPopup.getDbToDate().getValue();
//							 }
//						 }else{
//							 fromDate = fumOptPopup.getDbfromDate().getValue();
//							 toDate = fumOptPopup.getDbToDate().getValue();
//						 }
//						 ArrayList<String> brName = new ArrayList<String>();
//						 if(fumOptPopup.getOlBranch().getSelectedIndex() == 0){
//							
//							 for(int i=1;i<fumOptPopup.getOlBranch().getItemCount() ; i++){
//								 brName.add(fumOptPopup.getOlBranch().getValue(i).trim());
//							 }
//							
//						 }else{
//							 brName.add(fumOptPopup.getOlBranch().getValue(fumOptPopup.getOlBranch().getSelectedIndex()).trim());
//						 }
//	//					 form.showDialogMessage(brName.toString());
//						 System.out.println("selected branch - service value report-" + brName.toString());
//						 if(serviceRevanue){
//							 
//							 updateSer.dailyFumigationValueReportDetails(model.getCompanyId(), fromDate,toDate ,brName , new AsyncCallback<Void>() {
//									
//									@Override
//									public void onSuccess(Void result) {
//				
//										String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
//										final String url=gwt + "CreateXLXSServlet"+"?type="+2;
//										Window.open(url, "test", "enabled");
//										fumOptPopup.hidePopUp();
//									}
//									
//									@Override
//									public void onFailure(Throwable caught) {
//										// TODO Auto-generated method stub
//										
//									}
//								});
//						 }else{
//							 System.out.println("selected branch - fumigation report-" + brName.toString());
//							 updateSer.dailyFumigationReportDetails(model.getCompanyId(),fromDate,toDate,brName, new AsyncCallback<Void>() {
//									
//									@Override
//									public void onSuccess(Void result) {
//				
//										String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
//										final String url=gwt + "CreateXLXSServlet"+"?type="+1;
//										Window.open(url, "test", "enabled");
//										fumOptPopup.hidePopUp();
//									}
//									
//									@Override
//									public void onFailure(Throwable caught) {
//										// TODO Auto-generated method stub
//										
//									}
//								});
//						 }
//							
//					 } 
//					 if(event.getSource() == fumOptPopup.getLblCancel()){
//						 fumOptPopup.hidePopUp();
//					 }
//				} catch (Exception e) {
//					System.out.println(e.getMessage());
//				}
	 }	
}
