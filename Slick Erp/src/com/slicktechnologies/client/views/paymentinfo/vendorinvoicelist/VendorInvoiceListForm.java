package com.slicktechnologies.client.views.paymentinfo.vendorinvoicelist;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreen;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.VendorInvoice;

public class VendorInvoiceListForm extends TableScreen<VendorInvoice>{
	
	//Token to add variable declaration
		//
		
		public VendorInvoiceListForm(SuperTable<VendorInvoice> superTable) {
			super(superTable);
			createGui();
		}
		
		@SuppressWarnings("unused")
		private void initalizeWidget()
		{
			//<ReplasbleVariableInitalizerBlock>
		}
		
		@Override
		public void createScreen() 
		{
			/**
			 * Date : 17-03-2017 
			 * added by Anil
			 * NBHC CCPM
			 */
//			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice","OnlyForNBHC"))
//			{
//				this.processlevelBarNames = new String[] {"Invoice Acknowledgment","Revenue Report","Technician Revenue Report", AppConstants.SERVICEFUMIGATIONREPORT/*nidhi 18-11-2017 for display fumigation report*/, AppConstants.SERVICEFUMIGATIONVALUEREPORT/**
//						nidhi
//						18-11-2017
//						for contract revanue details report
//						**/};
////				,"Technician Revenue Report"
//			}else{
				/**27-10-2017 sagar sore [sales register button added in invoice list screen]**/
//				this.processlevelBarNames = new String[] {};//AppConstants.SALESREGISTER
//			}
		}
		
		@Override
		public void setEnable(boolean state) {
			super.setEnable(state);
		}

		/**
		 * method template to update the model with token entity name
		 */
		@Override
		public void updateModel(VendorInvoice model)
		{
			//<ReplasableUpdateModelFromViewCode>
		}

		


		/**
		 * method template to update the view with token entity name
		 */
		@Override
		public void updateView(VendorInvoice view) 
		{
			//<ReplasableUpdateViewFromModelCode>
		}
		
		/**
		 * Toggles the app header bar menus as per screen state
		 */
		public void toggleAppHeaderBarMenu() {
			if(superTable!=null&&superTable.getListDataProvider().getList()!=null){
				Console.log("in toggleAppHeaderBarMenu count="+superTable.getListDataProvider().getList().size());
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.contains("Rows:")){
						menus[k].setText("Rows:"+superTable.getListDataProvider().getList().size());	
						menus[k].getElement().setId("addlabel2");
					}
				}
			}
			if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.equals("Discard")||text.equals("Download")||text.equals("Search")||text.equals(AppConstants.NAVIGATION)||text.contains("Rows:"))
					{
						menus[k].setVisible(true); 
					}
					else
						menus[k].setVisible(false);  		  		
				}
			}
			
			else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION)||text.contains("Rows:"))
						menus[k].setVisible(true); 
					
					else
						menus[k].setVisible(false);  		   
				}
			}
			
			else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.equals("Discard")||text.equals("Download")||text.equals("Search")||text.equals(AppConstants.NAVIGATION)||text.contains("Rows:"))
							menus[k].setVisible(true); 
					
					else
						menus[k].setVisible(false);  		   
				}
			}
			AuthorizationHelper.setAsPerAuthorization(Screen.INVOICELIST,LoginPresenter.currentModule.trim());
		}
		
		
		@Override
		public void retriveTable(MyQuerry querry) {
			superTable.connectToLocal();
			service.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					for(SuperModel model:result)
						superTable.getListDataProvider().getList().add((VendorInvoice) model);
//					superTable.getTable().setVisibleRange(0,result.size());
					
					
				}
				
				@Override
				public void onFailure(Throwable caught) {
				caught.printStackTrace();
					
				}
			});
			
		}
		
		

}
