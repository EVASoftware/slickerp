package com.slicktechnologies.client.views.paymentinfo.vendorinvoicelist;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.salesprocess.VendorInvoice;

public class VendorInvoiceListTable extends SuperTable<VendorInvoice>{
	
	TextColumn<VendorInvoice> getCountColumn;
	TextColumn<VendorInvoice> invoiceDateColumn;
	TextColumn<VendorInvoice> invoiceTypeColumn;
	TextColumn<VendorInvoice> customerIdColumn;
	TextColumn<VendorInvoice> customerNameColumn;
	TextColumn<VendorInvoice> customerCellColumn;
	TextColumn<VendorInvoice> contractCountColumn;
	TextColumn<VendorInvoice> salesAmtColumn;
	TextColumn<VendorInvoice> invoiceAmtColumn;
	TextColumn<VendorInvoice> approverNameColumn;
	TextColumn<VendorInvoice> accountTypeColumn;
	TextColumn<VendorInvoice> statusColumn;
	TextColumn<VendorInvoice> branchColumn;
	
	/*
	 *  nidhi
	 *   1-07-2017
	 */
	TextColumn<VendorInvoice> segmentColumn;
	/*
	 *  end
	 */
	
	/*
	 *  nidhi
	 *   17-07-2017 
	 *   display service Id 
	 */
	TextColumn<VendorInvoice> serviceIdColumn;
	/*
	 *  end
	 */

	/**
	 *  nidhi
	 *  20-07-2017
	 *   for display service quantity and unit
	 */
	TextColumn<VendorInvoice> quantityColumn;
	TextColumn<VendorInvoice> uomColumn;
	/*
	 *  end
	 */
	
	/** 
	 *  nidhi
	 *  24-07-2017
	 *   for display service quantity and unit
	 */
	TextColumn<VendorInvoice> refColumn;
	/*
	 *  end
	 */
	
	/** 03-10-2017 sagar sore [To add Number range column in VendorInvoice list ]**/
	TextColumn<VendorInvoice> numberRangeColumn;
	public VendorInvoiceListTable() {
		super();
	}

	
	
	@Override
	public void createTable() {
		/**03-10-2017 sagar sore [adding number range column]**/
		addColumnCount();
		addColumnInvoiceDate();
		addColumnInvoiceType();
		addColumnCustomerId();		
		addColumnNumberRange();
		addColumnCustomerName();
		addColumnCustomerPhone();
		addColumnAccountType();
		addColumnContractCount();
		addColumnSalesAmount();
		addColumnInvoiceBranch();
		addColumnInvoiceAmount();
		addColumnApproverName();
		addColumnStatus();
		/*
		 *  nidhi
		 */
		addColumnServiceId();
		/*
		 *  end
		 */
		
		/*
		 *  nidhi
		 */
		addColumnInvoiceSegment();
		/*
		 *  end
		 */

		/**
		 *  nidhi
		 *  for quantity and uom display
		 */
		addColumnQuantity();
		addColumnUom();
		/**
		 *  end
		 */
		/**
		 *  nidhi
		 *  for quantity and uom display
		 *  24-07-2017
		 */
		addColumnRefNo();
		/**
		 *  end
		 */
	    table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
	}
	
	public void addColumnSorting()
	{
		addSortinggetInvoiceCount();
		addSortinggetInvoiceDate();
		addSortinggetInvoiceType();
		addSortinggetCustomerId();
		addSortinggetCustomerFullName();
		addSortinggetCustomerCellNumber();
		addSortingAccountType();
		addSortinggetContractCount();
		addSortinggetSalesAmount();
		addSortinggetInvoiceAmount();
		addSortinggetApproverName();
		addSortinggetStatus();
		addSortinggetBranch();
		/*
		 *  nidhi
		 */
		addSortinggetSegment();
		/*
		 * end
		 */
		addSortinggetServiceId();
		/*
		 *  nidhi
		 *  20-09-2017
		 *  
		 */
		addSortingUom();
		addSortingQuantity();
		/**
		 *  end
		 */
		/*
		 *  nidhi
		 *  24-07-2017
		 *  
		 */
		addSortingRefNo();
		/**
		 *  end
		 */
		
		/**03-10-2017 sagar sore [Sorting for number range column]**/
		addSortinggetNumberRange();
	}
	
	public void addColumnServiceId()
	{
		serviceIdColumn =new TextColumn<VendorInvoice>() {

			@Override
			public String getValue(VendorInvoice object) {
				return object.getRateContractServiceId()+"";
			}
		};
		
		table.addColumn(serviceIdColumn,"Service Id");
		table.setColumnWidth(serviceIdColumn, 100, Unit.PX);
		serviceIdColumn.setSortable(true);
	}	
	
	public void addColumnInvoiceSegment()
	{
		segmentColumn=new TextColumn<VendorInvoice>() {

			@Override
			public String getValue(VendorInvoice object) {
				return object.getSegment();
			}
		};
		
		table.addColumn(segmentColumn,"Segment");
		table.setColumnWidth(segmentColumn, 100, Unit.PX);
		segmentColumn.setSortable(true);
	}	
	
	protected void addSortinggetServiceId()
	{
		List<VendorInvoice> list=getDataprovider().getList();
		columnSort=new ListHandler<VendorInvoice>(list);
		columnSort.setComparator(serviceIdColumn, new Comparator<VendorInvoice>()
				{
			@Override
			public int compare(VendorInvoice e1,VendorInvoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getRateContractServiceId()== e2.getRateContractServiceId()){
						return 0;}
					if(e1.getRateContractServiceId()> e2.getRateContractServiceId()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	private void addSortingQuantity() {
		List<VendorInvoice> list=getDataprovider().getList();
		columnSort=new ListHandler<VendorInvoice>(list);
		columnSort.setComparator(quantityColumn, new Comparator<VendorInvoice>()
				{
			@Override
			public int compare(VendorInvoice e1,VendorInvoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getQuantity()== e2.getQuantity()){
						return 0;}
					if(e1.getQuantity()> e2.getQuantity()){
						return 1;}
					else{
						return -1;
					}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortingRefNo()
	{
		List<VendorInvoice> list=getDataprovider().getList();
		columnSort=new ListHandler<VendorInvoice>(list);
		columnSort.setComparator(refColumn, new Comparator<VendorInvoice>()
				{
			@Override
			public int compare(VendorInvoice e1,VendorInvoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getRefNumber()!=null && e2.getRefNumber()!=null){
						return e1.getRefNumber().compareTo(e2.getRefNumber());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortingUom()
	{
		List<VendorInvoice> list=getDataprovider().getList();
		columnSort=new ListHandler<VendorInvoice>(list);
		columnSort.setComparator(uomColumn, new Comparator<VendorInvoice>()
				{
			@Override
			public int compare(VendorInvoice e1,VendorInvoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getUom()!=null && e2.getUom()!=null){
						return e1.getUom().compareTo(e2.getUom());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
private void addColumnQuantity() {
		
		quantityColumn = new TextColumn<VendorInvoice>() {
			
			@Override
			public String getValue(VendorInvoice object) {
				if(object.getQuantity()!=0)
					return object.getQuantity() +"";
				else 
					return "";
			}
		};
		
		table.addColumn(quantityColumn,"Quantity");
		table.setColumnWidth(quantityColumn, 100, Unit.PX);
		quantityColumn.setSortable(true);
		
	}
	
	public void addColumnUom()
	{
		uomColumn=new TextColumn<VendorInvoice>() {

			@Override
			public String getValue(VendorInvoice object) {
				return object.getUom();
			}
		};
		
		table.addColumn(uomColumn,"Status");
		table.setColumnWidth(uomColumn, 100, Unit.PX);
		uomColumn.setSortable(true);
	}	
	
	public void addColumnRefNo()
	{
		refColumn=new TextColumn<VendorInvoice>() {

			@Override
			public String getValue(VendorInvoice object) {
				return object.getRefNumber();
			}
		};
		
		table.addColumn(refColumn,"REFERANCE NO.");
		table.setColumnWidth(refColumn, 100, Unit.PX);
		refColumn.setSortable(true);
	}	
	
	protected void addSortinggetSegment()
	{
		List<VendorInvoice> list=getDataprovider().getList();
		columnSort=new ListHandler<VendorInvoice>(list);
		columnSort.setComparator(segmentColumn, new Comparator<VendorInvoice>()
				{
			@Override
			public int compare(VendorInvoice e1,VendorInvoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getSegment()!=null && e2.getSegment()!=null){
						return e1.getSegment().compareTo(e2.getSegment());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetBranch()
	{
		List<VendorInvoice> list=getDataprovider().getList();
		columnSort=new ListHandler<VendorInvoice>(list);
		columnSort.setComparator(branchColumn, new Comparator<VendorInvoice>()
				{
			@Override
			public int compare(VendorInvoice e1,VendorInvoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBranch()!=null && e2.getBranch()!=null){
						return e1.getBranch().compareTo(e2.getBranch());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetInvoiceCount()
	{
		List<VendorInvoice> list=getDataprovider().getList();
		columnSort=new ListHandler<VendorInvoice>(list);
		columnSort.setComparator(getCountColumn, new Comparator<VendorInvoice>()
				{
			@Override
			public int compare(VendorInvoice e1,VendorInvoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	public void addColumnCount()
	{
		getCountColumn=new TextColumn<VendorInvoice>() {
			@Override
			public String getCellStyleNames(Context context, VendorInvoice object) {
				if((object.getStatus().equals("Cancelled"))){
					 return "red";
				 }
				else 
				{
					return "black";
				}
			}

			@Override
			public String getValue(VendorInvoice object) {
				return object.getCount()+"";
			}
		};
		table.addColumn(getCountColumn,"VendorInvoice ID");
		table.setColumnWidth(getCountColumn, 100, Unit.PX);
		getCountColumn.setSortable(true);
	}
	
	protected void addSortinggetInvoiceDate()
	{
		List<VendorInvoice> list=getDataprovider().getList();
		columnSort=new ListHandler<VendorInvoice>(list);
		columnSort.setComparator(invoiceDateColumn, new Comparator<VendorInvoice>()
				{
			@Override
			public int compare(VendorInvoice e1,VendorInvoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getInvoiceDate()!=null && e2.getInvoiceDate()!=null){
						return e1.getInvoiceDate().compareTo(e2.getInvoiceDate());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	public void addColumnInvoiceDate()
	{
		invoiceDateColumn=new TextColumn<VendorInvoice>() {

			@Override
			public String getValue(VendorInvoice object) {
				if(object.getInvoiceDate()!=null)
				{
				    String serDate=AppUtility.parseDate(object.getInvoiceDate());
				    return serDate;
				}
				return "Not Set";
			}
		};
		
		table.addColumn(invoiceDateColumn,"VendorInvoice Date");
		table.setColumnWidth(invoiceDateColumn, 100, Unit.PX);
		invoiceDateColumn.setSortable(true);
	}
	
	protected void addSortinggetInvoiceType()
	{
		List<VendorInvoice> list=getDataprovider().getList();
		columnSort=new ListHandler<VendorInvoice>(list);
		columnSort.setComparator(invoiceTypeColumn, new Comparator<VendorInvoice>()
				{
			@Override
			public int compare(VendorInvoice e1,VendorInvoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getInvoiceType()!=null && e2.getInvoiceType()!=null){
						return e1.getInvoiceType().compareTo(e2.getInvoiceType());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}

	
	private void addColumnInvoiceType() {
		invoiceTypeColumn=new TextColumn<VendorInvoice>() {

			@Override
			public String getValue(VendorInvoice object) {
				return object.getInvoiceType();
			}
		};
		
		table.addColumn(invoiceTypeColumn,"VendorInvoice Doc Type");
		table.setColumnWidth(invoiceTypeColumn, 120, Unit.PX);
		invoiceTypeColumn.setSortable(true);
		
	}

	protected void addSortinggetCustomerId()
	{
		List<VendorInvoice> list=getDataprovider().getList();
		columnSort=new ListHandler<VendorInvoice>(list);
		columnSort.setComparator(customerIdColumn, new Comparator<VendorInvoice>()
				{
			@Override
			public int compare(VendorInvoice e1,VendorInvoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getPersonInfo().getCount()== e2.getPersonInfo().getCount()){
						return 0;}
					if(e1.getPersonInfo().getCount()> e2.getPersonInfo().getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	private void addColumnCustomerId() {
		customerIdColumn=new TextColumn<VendorInvoice>() {

			@Override
			public String getValue(VendorInvoice object) {
				return object.getCustomerCount()+"";
			}
		};
		
		table.addColumn(customerIdColumn,"BP ID");
		table.setColumnWidth(customerIdColumn, 100, Unit.PX);
		customerIdColumn.setSortable(true);
		
	}
	
	protected void addSortinggetCustomerFullName()
	{
		List<VendorInvoice> list=getDataprovider().getList();
		columnSort=new ListHandler<VendorInvoice>(list);
		columnSort.setComparator(customerNameColumn, new Comparator<VendorInvoice>()
				{
			@Override
			public int compare(VendorInvoice e1,VendorInvoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getPersonInfo().getFullName()!=null && e2.getPersonInfo().getFullName()!=null){
						return e1.getPersonInfo().getFullName().compareTo(e2.getPersonInfo().getFullName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}

	
	public void addColumnCustomerName()
	{
		customerNameColumn=new TextColumn<VendorInvoice>() {

			@Override
			public String getValue(VendorInvoice object) {
				return object.getName();
			}
		};
		
		table.addColumn(customerNameColumn,"BP Name");
		table.setColumnWidth(customerNameColumn, 100, Unit.PX);
		customerNameColumn.setSortable(true);
	}
	
	protected void addSortinggetCustomerCellNumber()
	{
		List<VendorInvoice> list=getDataprovider().getList();
		columnSort=new ListHandler<VendorInvoice>(list);
		columnSort.setComparator(customerCellColumn, new Comparator<VendorInvoice>()
				{
			@Override
			public int compare(VendorInvoice e1,VendorInvoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getPersonInfo().getCellNumber()== e2.getPersonInfo().getCellNumber()){
						return 0;}
					if(e1.getPersonInfo().getCellNumber()> e2.getPersonInfo().getCellNumber()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	public void addColumnCustomerPhone()
	{
		customerCellColumn=new TextColumn<VendorInvoice>() {

			@Override
			public String getValue(VendorInvoice object) {
				return object.getCellNumber()+"";
			}
		};
		
		table.addColumn(customerCellColumn,"Cell Number");
		table.setColumnWidth(customerCellColumn, 100, Unit.PX);
		customerCellColumn.setSortable(true);
	}
	
	protected void addSortingAccountType()
	{
		List<VendorInvoice> list=getDataprovider().getList();
		columnSort=new ListHandler<VendorInvoice>(list);
		columnSort.setComparator(accountTypeColumn, new Comparator<VendorInvoice>()
				{
			@Override
			public int compare(VendorInvoice e1,VendorInvoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getAccountType()!=null && e2.getAccountType()!=null){
						return e1.getAccountType().compareTo(e2.getAccountType());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}

	public void addColumnAccountType()
	{
		accountTypeColumn=new TextColumn<VendorInvoice>() {

			@Override
			public String getValue(VendorInvoice object) {
				return object.getAccountType();
			}
		};
		
		table.addColumn(accountTypeColumn,"Acc. Type");
		table.setColumnWidth(accountTypeColumn, 90, Unit.PX);
		accountTypeColumn.setSortable(true);
	}
	
	protected void addSortinggetContractCount()
	{
		List<VendorInvoice> list=getDataprovider().getList();
		columnSort=new ListHandler<VendorInvoice>(list);
		columnSort.setComparator(contractCountColumn, new Comparator<VendorInvoice>()
				{
			@Override
			public int compare(VendorInvoice e1,VendorInvoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getContractCount()== e2.getContractCount()){
						return 0;}
					if(e1.getContractCount()> e2.getContractCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	public void addColumnContractCount()
	{
		contractCountColumn=new TextColumn<VendorInvoice>() {

			@Override
			public String getValue(VendorInvoice object) {
				    return object.getContractCount()+"";
			}
		};
		
		table.addColumn(contractCountColumn,"Order ID");
		table.setColumnWidth(contractCountColumn, 100, Unit.PX);
		contractCountColumn.setSortable(true);
	}
	
	protected void addSortinggetSalesAmount()
	{
		List<VendorInvoice> list=getDataprovider().getList();
		columnSort=new ListHandler<VendorInvoice>(list);
		columnSort.setComparator(salesAmtColumn, new Comparator<VendorInvoice>()
				{
			@Override
			public int compare(VendorInvoice e1,VendorInvoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getTotalSalesAmount()== e2.getTotalSalesAmount()){
						return 0;}
					if(e1.getTotalSalesAmount()> e2.getTotalSalesAmount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	public void addColumnSalesAmount()
	{
		salesAmtColumn=new TextColumn<VendorInvoice>() {

			@Override
			public String getValue(VendorInvoice object) {
				return object.getTotalSalesAmount()+"";
			}
		};
		
		table.addColumn(salesAmtColumn,"Order Amt");
		table.setColumnWidth(salesAmtColumn, 100, Unit.PX);
		salesAmtColumn.setSortable(true);
	}
	
	protected void addSortinggetInvoiceAmount()
	{
		List<VendorInvoice> list=getDataprovider().getList();
		columnSort=new ListHandler<VendorInvoice>(list);
		columnSort.setComparator(invoiceAmtColumn, new Comparator<VendorInvoice>()
				{
			@Override
			public int compare(VendorInvoice e1,VendorInvoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getInvoiceAmount()== e2.getInvoiceAmount()){
						return 0;}
					if(e1.getInvoiceAmount()> e2.getInvoiceAmount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}

	public void addColumnInvoiceBranch()
	{
		branchColumn=new TextColumn<VendorInvoice>() {

			@Override
			public String getValue(VendorInvoice object) {
				return object.getBranch();
			}
		};
		
		table.addColumn(branchColumn,"Branch");
		table.setColumnWidth(branchColumn, 100, Unit.PX);
		branchColumn.setSortable(true);
	}	
	public void addColumnInvoiceAmount()
	{
		invoiceAmtColumn=new TextColumn<VendorInvoice>() {

			@Override
			public String getValue(VendorInvoice object) {
				return object.getInvoiceAmount()+"";
			}
		};
		
		table.addColumn(invoiceAmtColumn,"VendorInvoice Amt");
		table.setColumnWidth(invoiceAmtColumn, 100, Unit.PX);
		invoiceAmtColumn.setSortable(true);
	}
	
	protected void addSortinggetApproverName()
	{
		List<VendorInvoice> list=getDataprovider().getList();
		columnSort=new ListHandler<VendorInvoice>(list);
		columnSort.setComparator(approverNameColumn, new Comparator<VendorInvoice>()
				{
			@Override
			public int compare(VendorInvoice e1,VendorInvoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getApproverName()!=null && e2.getApproverName()!=null){
						return e1.getApproverName().compareTo(e2.getApproverName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	public void addColumnApproverName()
	{
		approverNameColumn=new TextColumn<VendorInvoice>() {

			@Override
			public String getValue(VendorInvoice object) {
				return object.getApproverName();
			}
		};
		
		table.addColumn(approverNameColumn,"Approver Name");
		table.setColumnWidth(approverNameColumn, 120, Unit.PX);
		approverNameColumn.setSortable(true);
	}		
	
	protected void addSortinggetStatus()
	{
		List<VendorInvoice> list=getDataprovider().getList();
		columnSort=new ListHandler<VendorInvoice>(list);
		columnSort.setComparator(statusColumn, new Comparator<VendorInvoice>()
				{
			@Override
			public int compare(VendorInvoice e1,VendorInvoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getStatus()!=null && e2.getStatus()!=null){
						return e1.getStatus().compareTo(e2.getStatus());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	public void addColumnStatus()
	{
		statusColumn=new TextColumn<VendorInvoice>() {

			@Override
			public String getValue(VendorInvoice object) {
				return object.getStatus();
			}
		};
		
		table.addColumn(statusColumn,"Status");
		table.setColumnWidth(statusColumn, 100, Unit.PX);
		statusColumn.setSortable(true);
	}		
	
	/**03-10-2017 sagar sore [Creating number range column and sorting for that column]**/
	public void addColumnNumberRange() {
		numberRangeColumn = new TextColumn<VendorInvoice>() {

			@Override
			public String getValue(VendorInvoice object) {
				return object.getNumberRange();
			}
		};

		table.addColumn(numberRangeColumn, "Number Range");
		table.setColumnWidth(numberRangeColumn, 120, Unit.PX);
		numberRangeColumn.setSortable(true);
	}

	protected void addSortinggetNumberRange() {
		List<VendorInvoice> list = getDataprovider().getList();
		columnSort = new ListHandler<VendorInvoice>(list);
		columnSort.setComparator(numberRangeColumn, new Comparator<VendorInvoice>() {
			@Override
			public int compare(VendorInvoice e1, VendorInvoice e2) {
				if (e1 != null && e2 != null) {
					
					if (e1.getNumberRange() == null && e2.getNumberRange() == null){
						return 0;
					}
					if(e1.getNumberRange() == null) return 1;							
					if(e2.getNumberRange() == null) return -1;							
					return e1.getNumberRange().compareTo(e2.getNumberRange());

				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	/**end**/

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
