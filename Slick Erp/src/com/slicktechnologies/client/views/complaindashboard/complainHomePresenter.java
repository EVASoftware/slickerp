package com.slicktechnologies.client.views.complaindashboard;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.view.client.NoSelectionModel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.complain.ComplainForm;
import com.slicktechnologies.client.views.complain.ComplainPresenter;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.client.views.home.HomeForm;
import com.slicktechnologies.client.views.home.HomePresenter;
import com.slicktechnologies.client.views.home.HomePresenterSearchProxy;
import com.slicktechnologies.client.views.lead.LeadForm;
import com.slicktechnologies.client.views.lead.LeadPresenter;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.WhatsNew;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.complain.Complain;

public class complainHomePresenter implements ClickHandler{
	
	public static complainHomeForm form;
	complainHomePresenterSearchProxy searchpopup;
	
	protected GenricServiceAsync service=GWT.create(GenricService.class);

	GeneralViewDocumentPopup genPopup=new GeneralViewDocumentPopup();
	public static PopupPanel generalPanel;
	
	
	public complainHomePresenter(complainHomeForm form, complainHomePresenterSearchProxy searchpopup){
		
		this.form=form;
		this.searchpopup=searchpopup;
		SetEventHandling();
		searchpopup.applyHandler(this);
		form.getBtnGo().addClickHandler(this);
		
		Date date=new Date();
		System.out.println("Today Date ::::::::: "+date);
		// **** Here We Add 7 Days to current date and pass this date to default search.
		CalendarUtil.addDaysToDate(date,7);
	    System.out.println("Changed Date + 7 ::::::::: "+date);
		
		
		Vector<Filter> filtervec=null;
		Filter filter = null;
		MyQuerry querry;
//		List<String> statusList;
		
		filtervec=new Vector<Filter>();
		
		
		/**
		 * Old COde
		 */
//		filter = new Filter();
//		filter.setQuerryString("compStatus");
//		filter.setStringValue("Created");
//		filtervec.add(filter);
		
		/**
		 * Date : 02-08-2017 By ANIL
		 * Loading all the status of lead except Successful,Unsuccessful,Cancelled,Cancel,Rejected and Closed.
		 */
		List<String> statusList=new ArrayList<String>();
		statusList=AppUtility.getConfigValue(23);
		System.out.println("STATUS LIST SIZE:  "+statusList.size());
		
		for(int i=0;i<statusList.size();i++){
			if(statusList.get(i).trim().equalsIgnoreCase(AppConstants.Sucessful)
					||statusList.get(i).trim().equalsIgnoreCase(AppConstants.Unsucessful)
					||statusList.get(i).trim().equalsIgnoreCase(AppConstants.CANCELLED)
					||statusList.get(i).trim().equalsIgnoreCase(AppConstants.CANCEL)
					||statusList.get(i).trim().equalsIgnoreCase(AppConstants.REJECTED)
					||statusList.get(i).trim().equalsIgnoreCase(AppConstants.CLOSED)
					||statusList.get(i).trim().equalsIgnoreCase("Completed")){
				statusList.remove(i);
				i--;
			}
		}
		System.out.println("AFTER STATUS LIST SIZE:  "+statusList.size());
		filter = new Filter();
		filter.setQuerryString("compStatus IN");
		filter.setList(statusList);
		filtervec.add(filter);
		/**
		 * End
		 */
		
		filter = new Filter();
		filter.setQuerryString("date <=");
		filter.setDateValue(date);
		filtervec.add(filter);
		
		querry=new MyQuerry();
		querry.setQuerryObject(new Complain());
		querry.setFilters(filtervec);
//		this.retriveTable(querry,form.getComplainTable()); //Ashwini Patil Date:23-08-2024 to stop auto loading
		
		setTableSelectionOnComplain();
		
		
		genPopup.getBtnClose().addClickHandler(this);
	}
	
	
	

	public static void initialize()
	{
//		getwhatnewdata();
		complainHomeForm homeForm = new complainHomeForm();
//		AppMemory.getAppMemory().currentScreen=Screen.COMPLAINDASHBOARD;
		AppMemory.getAppMemory().skeleton.getProcessName().setText("Dashboard/Home");
		complainHomePresenterSearchProxy searchpopup=new complainHomePresenterSearchProxy();
		complainHomePresenter presenter= new complainHomePresenter(homeForm,searchpopup);
		AppMemory.getAppMemory().stickPnel(homeForm);	
	}

	

//	private static void  getwhatnewdata() {
//		
//		
//		GenricServiceAsync service=GWT.create(GenricService.class);
//		
//		/**********************************************/
//		
//		MyQuerry querry = new MyQuerry();
//		Vector<Filter> filtervec = new Vector<Filter>();
//		Filter filter=null;
//		Company comp = new Company();
//		
//		filter = new Filter();
//		filter.setQuerryString("companyId");
//		filter.setLongValue(comp.getCompanyId());
//		filtervec.add(filter);
//		
//		querry.setFilters(filtervec);
//		querry.setQuerryObject(new WhatsNew());
//		
//		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
//			@Override
//			public void onSuccess(ArrayList<SuperModel> result) {
//				
//				System.out.println("REslut CRON JOB"+result.size());
//				WhatsNew whatnew = new WhatsNew();
//				
//				for(SuperModel model:result){
//					
//					whatnew =(WhatsNew) model;
//				}
//				
//				if(whatnew!=null)
//				{
//					form.updatewhatsnew(whatnew);
//				}
//			}
//			@Override
//			public void onFailure(Throwable caught) {
//			}
//		});
//	}
	
	
	
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
		if(event.getSource() instanceof InlineLabel)
		{
		InlineLabel lbl= (InlineLabel) event.getSource();
		String text= lbl.getText();
		if(text.equals("Search"))
			searchpopup.showPopUp();
		
		if(text.equals("Go"))
			reactOnGo();
		}
		
		System.out.println("form.getPersonInfo().getValue() "+form.getPersonInfo().getValue());
		
		if(event.getSource().equals(form.getBtnGo())){
			final GWTCAlert alert = new GWTCAlert();

			
			if(form.getOlbbranch().getSelectedIndex()==0&&
					form.getOlbassignTo().getSelectedIndex()==0&&
					form.getDbFromDate().getValue()==null&&form.getDbToDate().getValue()==null  &&
							(form.getPersonInfo().getValue()==null || form.getPersonInfo().getValue().equals(""))){
				
					alert.alert("Select atleast one field to search");
					return;
			}
			
			if(form.getPersonInfo().getValue()!=null && !form.getPersonInfo().getValue().equals("")){
			
				if(	form.getDbFromDate().getValue()!=null || form.getDbToDate().getValue()!=null	){
					alert.alert("Please select either customer or from date to date");
				}
				else{
					searchbyCustomerInfo();
				}
			}
			else{	
			
			if(form.getOlbassignTo().getSelectedIndex()!=0&&form.getOlbbranch().getSelectedIndex()==0
			   &&( form.getDbFromDate().getValue()==null&&form.getDbToDate()!=null)){
				
				System.out.println("Assign To ");
				searchByFromToDate();
				
			}
			else if(form.getOlbassignTo().getSelectedIndex()==0&&form.getOlbbranch().getSelectedIndex()!=0
					   &&( form.getDbFromDate().getValue()==null&&form.getDbToDate()!=null)){
				
				
				System.out.println("branch ");
				searchByFromToDate();
			}
			else if(form.getOlbassignTo().getSelectedIndex()!=0&&form.getOlbbranch().getSelectedIndex()!=0
					   &&( form.getDbFromDate().getValue()==null&&form.getDbToDate()!=null)){
				
				System.out.println("for both AssignTo  and branch");
				searchByFromToDate();
			}
			else{
			System.out.println("Remaining all situations ");
			
			if(form.getDbFromDate().getValue()==null||form.getDbToDate().getValue()==null){
//				final GWTCAlert alert = new GWTCAlert();
				alert.alert("Select From and To Date.");
			}
			 if(form.getDbFromDate().getValue()!=null&&form.getDbToDate()!=null){
			Date formDate=form.getDbFromDate().getValue();
			Date toDate=form.getDbToDate().getValue();
			if(toDate.equals(formDate)||toDate.after(formDate)){
				System.out.println("From Date ::::::::: "+formDate);
				CalendarUtil.addDaysToDate(formDate,7);
				System.out.println("Changed Date ::::::::: "+formDate);
				System.out.println("To Date ::::::::: "+toDate);
			
				if(toDate.before(formDate)){
					System.out.println("Inside Date Condition"); 
					searchByFromToDate();
				}
				else{
//					final GWTCAlert alert = new GWTCAlert();
					alert.alert("From Date and To Date Difference Can Not Be More Than 7 Days.");
				}
			}
			else{
//				final GWTCAlert alert = new GWTCAlert();
				alert.alert("To Date should be greater than From Date.");
			}
			}
			}
		}
		}
		
		
		if(event.getSource().equals(genPopup.getBtnClose())){
		
			
			genPopup.viewDocumentPanel.hide();
			generalPanel.hide();
			
			initialize();
		}
	}







	/**
	 * Sets the event handling.
	 */
	public void SetEventHandling()
	{
		 InlineLabel label[] =AppMemory.getAppMemory().skeleton.getMenuLabels();
		  for(int k=0;k<label.length;k++)
		  {
			 if( AppMemory.getAppMemory().skeleton.registration[k]!=null)
			    AppMemory.getAppMemory().skeleton.registration[k].removeHandler();
			 
				 
		  }
		 
		  for( int k=0;k<label.length;k++)
		  {
			  
			  AppMemory.getAppMemory().skeleton.registration[k]= label[k].addClickHandler(this);
		  }
	}

	private void reactOnGo() {
		
		if(!searchpopup.personInfo.getId().getValue().equals("")&&!searchpopup.personInfo.getName().getValue().equals("")&&!searchpopup.personInfo.getPhone().getValue().equals(""))
		{
			MyQuerry quer=createcomplainFilter(new Complain());
			 this.form.ComplainTable.connectToLocal();
			retriveTable(quer, form.getComplainTable());
			 
			 searchpopup.hidePopUp();
		}
		
		
		if(searchpopup.personInfo.getId().getValue().equals("")&&searchpopup.personInfo.getName().getValue().equals("")&&searchpopup.personInfo.getPhone().getValue().equals(""))
		{
			GWTCAlert alertMsg=new GWTCAlert();
			alertMsg.alert("Please enter customer information");
		}
	}


	private MyQuerry createcomplainFilter(Complain complain) {

		Vector<Filter>filterVec = new Vector<Filter>();
		 Filter temp;
		 if(searchpopup.personInfo.getId().getText().trim().equals("")==false)
			{
				temp=new Filter();
				temp.setLongValue(Long.parseLong(searchpopup.personInfo.getId().getValue()));
				temp.setQuerryString("personinfo.count");
				filterVec.add(temp);	
			}
			
			if((searchpopup.personInfo.getName().getText()).trim().equals("")==false)
			{
				temp=new Filter();
				temp.setStringValue(searchpopup.personInfo.getName().getValue());
				temp.setQuerryString("personinfo.fullName");
				filterVec.add(temp);	
			}
			
			if(searchpopup.personInfo.getPhone().getText().trim().equals("")==false)
			{
				temp=new Filter();
				temp.setLongValue(Long.parseLong(searchpopup.personInfo.getPhone().getValue()));
				temp.setQuerryString("personinfo.cellNumber");
				filterVec.add(temp);	
			}
			
			MyQuerry Complain=new MyQuerry();
			Complain.setFilters(filterVec);
			Complain.setQuerryObject(complain);
			
			return Complain;
	}
	
	private void setTableSelectionOnComplain() {

		System.out.println("rohan in side method");
		
		 final NoSelectionModel<Complain> selectionModelMyObj = new NoSelectionModel<Complain>();
		 
	     SelectionChangeEvent.Handler  tableHandler = new SelectionChangeEvent.Handler() 
	     {
	         @Override
	         public void onSelectionChange(SelectionChangeEvent event) 
	         {
	        	 
	        	   
//	        	 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Customer Support",Screen.COMPLAIN);
	        	 final Complain entity=selectionModelMyObj.getLastSelectedObject();
//	        	 final ComplainForm form = ComplainPresenter.initalize();
//	        	  
//					form.showWaitSymbol();
//					Timer timer=new Timer() {
//	 				
//	 				@Override
//	 				public void run() {
//	 					form.hideWaitSymbol();
//					    form.updateView(entity);
//					    form.setToViewState();
//					    form.getPic().setEnabled(false);
//					  
//	 				}
//	 			};
//	            
//	             timer.schedule(5000); 
	        	 
	     		
	     		AppMemory.getAppMemory().currentScreen = Screen.COMPLAIN;
	        	genPopup.setModel(AppConstants.COMPLAIN, entity);
				generalPanel = new PopupPanel(false,true);
				generalPanel.add(genPopup);
				generalPanel.show();
				generalPanel.center();
	             
	          }
	     };
	     // Add the handler to the selection model
	     selectionModelMyObj.addSelectionChangeHandler( tableHandler );
	     // Add the selection model to the table
	     form.ComplainTable.getTable().setSelectionModel(selectionModelMyObj);
		 
		
	}

	private void searchByFromToDate() {
		// TODO Auto-generated method stub
		System.out.println("Hiiiiiiiiiiii........");
		Long compId=UserConfiguration.getCompanyId();
		System.out.println("Company Id :: "+compId);
		    
		Vector<Filter> filtervec=null;
		Filter filter = null;
		MyQuerry querry;
		List<String> statusList;
		
		filtervec=new Vector<Filter>();
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(compId);
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("compStatus");
		filter.setStringValue("Created");
		filtervec.add(filter);
		
		if(form.getDbFromDate().getValue()!=null){
		filter = new Filter();
		filter.setQuerryString("date >=");
		filter.setDateValue(form.getDbFromDate().getValue());
		filtervec.add(filter);
		}
		
		if(form.getDbToDate().getValue()!=null){
			
		filter = new Filter();
		filter.setQuerryString("date <=");
		filter.setDateValue(form.getDbToDate().getValue());
		filtervec.add(filter);
		}
		
		if(form.getOlbbranch().getSelectedIndex()!=0){
		System.out.println("inside Complain***********");
			filter = new Filter();
			filter.setQuerryString("branch");
			filter.setStringValue(form.getOlbbranch().getValue());
			filtervec.add(filter);
			
		}
		
		if(form.getOlbassignTo().getSelectedIndex()!=0){
			filter = new Filter();
			filter.setQuerryString("assignto");
			filter.setStringValue(form.getOlbassignTo().getValue());
			filtervec.add(filter);
		}
		
		querry=new MyQuerry();
		querry.setQuerryObject(new Complain());
		querry.setFilters(filtervec);
		this.retriveTable(querry,form.getComplainTable());
	}




	public <T> void retriveTable(MyQuerry querry,final SuperTable<T>table)
	 {
		 
				table.connectToLocal();
				service.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
					
					@SuppressWarnings("unchecked")
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						System.out.println("Rsult=========="+result.size());
						for(SuperModel model:result)
						{
							System.out.println("model"+model.getCount());
							table.getListDataProvider().getList().add((T) model);
						}
						System.out.println("11111111");
					}
					
					@Override
					public void onFailure(Throwable caught) {
					caught.printStackTrace();
						
					}
				}); 
	 }
	
	
	private void searchbyCustomerInfo() {
		    
		Vector<Filter> filtervec=null;
		Filter filter = null;
		MyQuerry querry;
		
		filtervec=new Vector<Filter>();
		
		filter = new Filter();
		filter.setQuerryString("compStatus");
		filter.setStringValue("Created");
		filtervec.add(filter);
		
		if(form.getPersonInfo().getIdValue()!=-1)
		{
			filter=new Filter();
			filter.setIntValue(form.getPersonInfo().getIdValue());
			filter.setQuerryString("personinfo.count");
			filtervec.add(filter);
		}
		  
		if(!(form.getPersonInfo().getFullNameValue().equals("")))
		{
			filter=new Filter();
			filter.setStringValue(form.getPersonInfo().getFullNameValue());
			filter.setQuerryString("personinfo.fullName");
			filtervec.add(filter);
		}
		if(form.getPersonInfo().getCellValue()!=-1l)
		{
			filter=new Filter();
			filter.setLongValue(form.getPersonInfo().getCellValue());
			filter.setQuerryString("personinfo.cellNumber");
			filtervec.add(filter);
		}
		
		if(form.getOlbbranch().getSelectedIndex()!=0){
			filter = new Filter();
			filter.setQuerryString("branch");
			filter.setStringValue(form.getOlbbranch().getValue());
			filtervec.add(filter);
				
		}
			
		if(form.getOlbassignTo().getSelectedIndex()!=0){
			filter = new Filter();
			filter.setQuerryString("assignto");
			filter.setStringValue(form.getOlbassignTo().getValue());
			filtervec.add(filter);
		}
		
		querry=new MyQuerry();
		querry.setQuerryObject(new Complain());
		querry.setFilters(filtervec);
		this.retriveTable(querry,form.getComplainTable());
	
	}

}
