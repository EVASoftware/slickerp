package com.slicktechnologies.client.views.complaindashboard;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.FontStyle;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CaptionPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.ViewContainer;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.MyInlineLabel;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.complain.ComplainPresenter;
import com.slicktechnologies.client.views.contract.ContractPresenter;
import com.slicktechnologies.client.views.home.ContractPresenterTableProxy;
import com.slicktechnologies.client.views.home.LeadPresenterTableProxy;
import com.slicktechnologies.client.views.home.QuotationPresenterTableProxy;
import com.slicktechnologies.client.views.home.ServiceTable;
import com.slicktechnologies.client.views.lead.LeadPresenter;
import com.slicktechnologies.client.views.quotation.QuotationPresenter;
import com.slicktechnologies.shared.common.WhatsNew;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.complain.Complain;
import com.slicktechnologies.shared.common.personlayer.Employee;

public class complainHomeForm extends ViewContainer implements ClickHandler{

	
	CsvServiceAsync scvservice = GWT.create(CsvService.class);
	
	/** String array repersenting the table header menus*/
	String[] tableheaderbarnames={"New","Download"};
	
	/** Flow panel for holding the Table header menus */
	FlowPanel tableheader;
	
	/** Complain Table which can come on the form, this component can be made generic */
	public ComplainPresenterTableProxy ComplainTable;
	
	
	ObjectListBox<Branch> olbbranch;
	ObjectListBox<Employee> olbassignTo;
	
	DateBox dbFromDate;
	DateBox dbToDate;
	Button btnGo;
	
	public PersonInfoComposite personInfo;

	
	public complainHomeForm()
	{
		//Toggle the App header bar menu , In this case only search should come
		toggleAppHeaderBarMenu();
		//Create the Gui, actual Gui gets created here.
		createGui();
		//Sets the event handling
  }	
	
	

//	public void updatewhatsnew( WhatsNew whatnew2){
//		
//				
//		
//		if(whatnew2!=null){
//			
//				
//		FlowPanel panelwhatsnewTitle = new FlowPanel();
//		
//		panelwhatsnewTitle.getElement().getStyle().setMarginTop(40, Unit.PX);
//		panelwhatsnewTitle.getElement().getStyle().setMarginLeft(45, Unit.PX);
//		panelwhatsnewTitle.getElement().getStyle().setMarginRight(45, Unit.PX);
//
//		
//		InlineLabel maintitle = new InlineLabel(" ..................................................................................... Whats New ...........................................................................");
//			maintitle.getElement().getStyle().setFontSize(20, Unit.PX);
//			maintitle.getElement().getStyle().setBackgroundColor("purple");
//			maintitle.getElement().getStyle().setColor("white");
//			
//			panelwhatsnewTitle.add(maintitle);
//			
//			content.add(panelwhatsnewTitle);
//			
//			
//			
//			
//			FlowPanel paneluserguide = new FlowPanel();
//			
//			paneluserguide.getElement().getStyle().setMarginLeft(900, Unit.PX);
//			paneluserguide.getElement().getStyle().setMarginRight(45, Unit.PX);
//			paneluserguide.getElement().getStyle().setFontSize(15, Unit.PX);
//
//			InlineLabel userguide = new InlineLabel("User Guide");
//			userguide.getElement().getStyle().setColor("blue");
//
//			paneluserguide.add(userguide);
//			
//			InlineLabel clkhere = new InlineLabel("Click here");
//			clkhere.getElement().getStyle().setColor("orange");
//			clkhere.getElement().getStyle().setMarginLeft(10,Unit.PX);
//			paneluserguide.add(clkhere);
//			
//			final String urluserguide="https://drive.google.com/open?id=0BwBfozphkPy2WmFvWTJfdWZwZGc";
//			clkhere.addClickHandler(new ClickHandler() {
//				
//				@Override
//				public void onClick(ClickEvent event) {
//					
//		 			Window.open(urluserguide, "test", "enabled");
//
//				}
//			});
//				content.add(paneluserguide);
//			
//
//			FlowPanel titlePanel= new FlowPanel();
//
//			titlePanel.getElement().getStyle().setMarginLeft(45, Unit.PX);
//			titlePanel.getElement().getStyle().setMarginRight(45, Unit.PX);
//			titlePanel.getElement().getStyle().setFontSize(15, Unit.PX);
//
//				
//				InlineLabel name = new InlineLabel(whatnew2.getTitle());
//				name.getElement().getStyle().setMarginBottom(20, Unit.PX);
//				name.getElement().getStyle().setColor("blue");
//				name.getElement().getStyle().setFontSize(20, Unit.PX);
//
//				titlePanel.add(name);
//				
//				content.add(titlePanel);
//
//				
//				FlowPanel flsubject= new FlowPanel();
//
//				flsubject.getElement().getStyle().setMarginTop(10, Unit.PX);
//				flsubject.getElement().getStyle().setMarginLeft(45, Unit.PX);
//				flsubject.getElement().getStyle().setMarginRight(45, Unit.PX);
//				flsubject.getElement().getStyle().setFontSize(15, Unit.PX);
//
//					InlineLabel lbsubject = new InlineLabel(whatnew2.getSubject());
//					flsubject.add(lbsubject);
//					
//					content.add(flsubject);
//					
//					
//				FlowPanel flpath= new FlowPanel();
//
//				flpath.getElement().getStyle().setMarginTop(10, Unit.PX);
//				flpath.getElement().getStyle().setMarginLeft(45, Unit.PX);
//				flpath.getElement().getStyle().setMarginRight(45, Unit.PX);
//				flpath.getElement().getStyle().setFontSize(15, Unit.PX);
//
//					InlineLabel lbpath = new InlineLabel(whatnew2.getPath());
//					flpath.add(lbpath);
//						
//					content.add(flpath);
//				
//				FlowPanel fldecriptionone= new FlowPanel();
//
//				fldecriptionone.getElement().getStyle().setMarginTop(10, Unit.PX);
//				fldecriptionone.getElement().getStyle().setMarginLeft(55, Unit.PX);
//				fldecriptionone.getElement().getStyle().setMarginRight(45, Unit.PX);
//				fldecriptionone.getElement().getStyle().setFontSize(15, Unit.PX);
//
//					InlineLabel lbdescriptionone = new InlineLabel(whatnew2.getDescriptionone());
//					fldecriptionone.add(lbdescriptionone);
//					
//					content.add(fldecriptionone);
//				
//				
//					
//				FlowPanel fldecriptiontwo= new FlowPanel();
//
//				fldecriptiontwo.getElement().getStyle().setMarginTop(10, Unit.PX);
//				fldecriptiontwo.getElement().getStyle().setMarginLeft(55, Unit.PX);
//				fldecriptiontwo.getElement().getStyle().setMarginRight(45, Unit.PX);
//				fldecriptiontwo.getElement().getStyle().setFontSize(15, Unit.PX);
//
//					InlineLabel lbdecriptiontwo = new InlineLabel(whatnew2.getDescriptiontwo());
//					fldecriptiontwo.add(lbdecriptiontwo);
//						
//					content.add(fldecriptiontwo);
//					
//					
//				FlowPanel fldecriptionthree= new FlowPanel();
//
//				fldecriptionthree.getElement().getStyle().setMarginTop(10, Unit.PX);
//				fldecriptionthree.getElement().getStyle().setMarginLeft(55, Unit.PX);
//				fldecriptionthree.getElement().getStyle().setMarginRight(45, Unit.PX);
//				fldecriptionthree.getElement().getStyle().setFontSize(15, Unit.PX);
//
//					InlineLabel lbdecriptionthree = new InlineLabel(whatnew2.getDescriptionthree());
//					fldecriptionthree.add(lbdecriptionthree);
//						
//					content.add(fldecriptionthree);
//					
//					
//				FlowPanel fldecriptionfour= new FlowPanel();
//
//				fldecriptionfour.getElement().getStyle().setMarginTop(10, Unit.PX);
//				fldecriptionfour.getElement().getStyle().setMarginLeft(55, Unit.PX);
//				fldecriptionfour.getElement().getStyle().setMarginRight(45, Unit.PX);
//				fldecriptionfour.getElement().getStyle().setFontSize(15, Unit.PX);
//
//					InlineLabel lbdecriptionfour = new InlineLabel(whatnew2.getDescriptionfour());
//					fldecriptionfour.add(lbdecriptionfour);
//						
//					content.add(fldecriptionfour);
//				
////				FlowPanel fldecriptionfive= new FlowPanel();
////
////				fldecriptionfive.getElement().getStyle().setMarginTop(10, Unit.PX);
////				fldecriptionfive.getElement().getStyle().setMarginLeft(55, Unit.PX);
////				fldecriptionfive.getElement().getStyle().setMarginRight(45, Unit.PX);
////				fldecriptionfive.getElement().getStyle().setFontSize(15, Unit.PX);
////
////					InlineLabel lbdecriptionfive = new InlineLabel(whatnew2.getDescriptionfive());
////					fldecriptionfive.add(lbdecriptionfive);
////						
////					content.add(fldecriptionfive);
//					
//				
//				
//				FlowPanel panel= new FlowPanel();
//				
//				panel.getElement().getStyle().setMarginTop(05, Unit.PX);
//				panel.getElement().getStyle().setMarginLeft(45, Unit.PX);
//				panel.getElement().getStyle().setMarginRight(45, Unit.PX);
//				
//
//				FlowPanel innerpanel= new FlowPanel();
//				innerpanel.getElement().getStyle().setFontSize(15, Unit.PX);
//				
//				InlineLabel clickheredetails = new InlineLabel(whatnew2.getMoreDetails());
//				clickheredetails.getElement().getStyle().setMarginLeft(10,Unit.PX);
//				clickheredetails.getElement().getStyle().setColor("orange");
//				innerpanel.add(clickheredetails);
//				
//				final String url = whatnew2.getGoogleDriveLink();
//
//				clickheredetails.addClickHandler(new ClickHandler() {
//					
//					@Override
//					public void onClick(ClickEvent event) {
//						
//						System.out.println("Clicked");
//			 			Window.open(url, "test", "enabled");
//						
//					}
//				});
//
//				panel.add(innerpanel);
//				content.add(panel);
//
//				
//		}
//		
//	}
	
	
	private void initializeWidget(){
		
		olbbranch =new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbbranch);
				
		olbassignTo =new ObjectListBox<Employee>();
		AppUtility.makeSalesPersonListBoxLive(olbassignTo);
				
		
		dbFromDate=new DateBoxWithYearSelector();
		dbToDate=new DateBoxWithYearSelector();
		DateTimeFormat dateFormat = DateTimeFormat.getFormat("dd-MM-yyyy");
		dbFromDate.setFormat(new DateBox.DefaultFormat(dateFormat));
		dbToDate.setFormat(new DateBox.DefaultFormat(dateFormat));
		btnGo=new Button("GO");
		
		
		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new Customer());
		personInfo=new PersonInfoComposite(querry,false);
	}
	
	private void toggleAppHeaderBarMenu() {
		
		InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
		for(int k=0;k<menus.length;k++)
		{
			String text=menus[k].getText();
//			if(text.contains("Search"))
//			{
//				menus[k].setVisible(true); 
//			}
//			else
				menus[k].setVisible(false);  		   
			
		}
	}


	@Override
	protected void createGui() {
		// TODO Auto-generated method stub
		

		
		initializeWidget();
		
		FlowPanel panel= new FlowPanel();
		
		panel.getElement().getStyle().setMarginTop(10, Unit.PX);
		panel.getElement().getStyle().setMarginLeft(75, Unit.PX);
		panel.getElement().getStyle().setMarginRight(75, Unit.PX);
		
		FlowPanel innerpanel= new FlowPanel();
		
		InlineLabel blank = new InlineLabel("    ");
		
		
		//************rohan added here search by branch and sales person***************  
		
				InlineLabel branch = new InlineLabel("  Branch  ");
				innerpanel.add(branch);
				innerpanel.add(olbbranch);
		
				InlineLabel AssignTo = new InlineLabel(" Assign To  ");
				innerpanel.add(AssignTo);
				innerpanel.add(olbassignTo);
				
		//**************************changes ends here ********************************
		
		InlineLabel fromDate = new InlineLabel("  From Date  ");
		fromDate.setSize("90px", "20px");
		innerpanel.add(fromDate);
		innerpanel.add(dbFromDate);
		
		InlineLabel toDate = new InlineLabel("  To Date  ");
		innerpanel.add(toDate);
		innerpanel.add(dbToDate);
		innerpanel.add(blank);
		innerpanel.add(blank);
		innerpanel.add(blank);
		innerpanel.add(blank);
//		innerpanel.add(btnGo);
		
		
		FlowPanel panel2= new FlowPanel();
		
		panel2.getElement().getStyle().setMarginTop(10, Unit.PX);
		panel2.getElement().getStyle().setMarginLeft(75, Unit.PX);
		panel2.getElement().getStyle().setMarginRight(75, Unit.PX);
		
		personInfo.setSize("900px", "20px");
		
		HorizontalPanel innerpanel2 = new HorizontalPanel();
		innerpanel2.add(personInfo);
		HorizontalPanel innerpanel3 = new HorizontalPanel();
		innerpanel3.getElement().getStyle().setMarginTop(12, Unit.PX);
		innerpanel3.add(btnGo);
		
		HorizontalPanel hrinnerpanel = new HorizontalPanel();
		hrinnerpanel.add(innerpanel2);
		hrinnerpanel.add(innerpanel3);

		panel2.add(hrinnerpanel);

		panel.add(innerpanel);
		content.add(panel);
		content.add(panel2);

		
		
		//Create the Four Scroll Panel To hold four types of Screen
		ScrollPanel[] scrollPanes=new ScrollPanel[1];
		//String array to hold captions
		String[]captions={"Complain"};
		//Screen array to hold new Screen object for the purpose of Redirection
		Screen[]screens={Screen.COMPLAIN};	
		
		//Create  Flow Panel,Inside Flow Panel 
		for(int i=0;i<captions.length;i++)
		{
			FlowPanel holder= new FlowPanel();
			tableheader= new FlowPanel();
			tableheader.getElement().setId("tableheader");
			// Provides Table Header which is responsible for new or Download Buttons
			setTableHeaderBar(screens[i]);
			
			tableheader.getElement().getStyle().setMarginTop(10, Unit.PX);
			tableheader.getElement().getStyle().setMarginBottom(10, Unit.PX);
			
			scrollPanes[i]= new ScrollPanel();
			scrollPanes[i].setHeight("350px");
		
			holder.add(tableheader);
			holder.add(scrollPanes[i]);
			//Caption Panel inside which a Flow Panel is added
			CaptionPanel cap= new CaptionPanel(captions[i]);
			cap.add(holder);
			cap.getElement().setClassName("tablecaption");
			//Add caption Panel on Content
		    content.add(cap);
		}
		//Add Lead Table inside the Scroll Panel
		/**
		 * @author Abhinav
		 * @since 17/10/2019
		 */
		ComplainTable = new ComplainPresenterTableProxy();
		ComplainTable.getTable().setHeight("350px");
		scrollPanes[0].add(ComplainTable.content);
		
		content.getElement().setId("homeview");
	  
		
	}

	private void setTableHeaderBar(Screen screen) {
		// TODO Auto-generated method stub
		
		for(int j=0;j<getTableheaderbarnames().length;j++)
		{
			MyInlineLabel lbl;
			lbl=new MyInlineLabel(getTableheaderbarnames()[j],screen);
            getTableheader().add(lbl);
			lbl.getElement().setId("tableheadermenu");
			lbl.addClickHandler(this);
		}
		
	}
	
	/**
	 * Gets the tableheaderbarnames.
	 *
	 * @return the tableheaderbarnames
	 */

	public String[] getTableheaderbarnames() {
		return tableheaderbarnames;
	}

	/**
	 * Sets the tableheaderbarnames.
	 *
	 * @param tableheaderbarnames the new tableheaderbarnames
	 */
	
	public void setTableheaderbarnames(String[] tableheaderbarnames) {
		this.tableheaderbarnames = tableheaderbarnames;
	}
	
	/**
	 * Gets the tableheader.
	 *
	 * @return the tableheader
	 */
	public FlowPanel getTableheader() {
		return tableheader;
	}

	/**
	 * Sets the tableheader.
	 *
	 * @param tableheader the new tableheader
	 */
	public void setTableheader(FlowPanel tableheader) {
		this.tableheader = tableheader;
	}

	
	
	public ComplainPresenterTableProxy getComplainTable() {
		return ComplainTable;
	}

	public void setComplainTable(ComplainPresenterTableProxy complainTable) {
		ComplainTable = complainTable;
	}

	@Override
	public void onClick(ClickEvent event) {
		
		if(event.getSource() instanceof MyInlineLabel)
		{
			MyInlineLabel lbl=(MyInlineLabel) event.getSource();
			redirectOnScreens(lbl);
			
		}
	}
	
	private void redirectOnScreens(MyInlineLabel lbl) {
		

		Screen screen=(Screen) lbl.redirectScreen;
		String title=lbl.getText().trim();
		switch(screen)
		{
		  case COMPLAIN:
			  
				if(title.equals("Download"))
				{
					reactOnComplainDownLoad();
				}
				
				if(title.equals("New"))
				{
					ComplainPresenter.initalize();
				}
				
				break;
				
		default:
			break;
				
		}


		
	}

	private void reactOnComplainDownLoad() {
		
		
		ArrayList<Complain> custarray=new ArrayList<Complain>();
		List<Complain> list=(List<Complain>) ComplainTable.getListDataProvider().getList();
		
		
		custarray.addAll(list);
		
		scvservice.setComplainlist(custarray, new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed" + caught);
			}
			@Override
			public void onSuccess(Void result) {
				String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
				/**
				 * Date : 24-10-2017 BY ANIL
				 * For NBHC we are providing a separate download with less fields in csv
				 */
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Complain", "CustomerSupportReport")){
					final String url = gwt + "csvservlet" + "?type=" + 126;
					Window.open(url, "test", "enabled");
				}else{
					final String url = gwt + "csvservlet" + "?type=" + 95;
					Window.open(url, "test", "enabled");
				}
			}
		});
		 

		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

	public ObjectListBox<Branch> getOlbbranch() {
		return olbbranch;
	}

	public void setOlbbranch(ObjectListBox<Branch> olbbranch) {
		this.olbbranch = olbbranch;
	}


	public ObjectListBox<Employee> getOlbassignTo() {
		return olbassignTo;
	}

	public void setOlbassignTo(ObjectListBox<Employee> olbassignTo) {
		this.olbassignTo = olbassignTo;
	}

	public DateBox getDbFromDate() {
		return dbFromDate;
	}

	public void setDbFromDate(DateBox dbFromDate) {
		this.dbFromDate = dbFromDate;
	}

	public DateBox getDbToDate() {
		return dbToDate;
	}

	public void setDbToDate(DateBox dbToDate) {
		this.dbToDate = dbToDate;
	}

	public Button getBtnGo() {
		return btnGo;
	}

	public void setBtnGo(Button btnGo) {
		this.btnGo = btnGo;
	}



	public PersonInfoComposite getPersonInfo() {
		return personInfo;
	}



	public void setPersonInfo(PersonInfoComposite personInfo) {
		this.personInfo = personInfo;
	}

	
	
	
	
}
