package com.slicktechnologies.client.views.complaindashboard;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.complain.Complain;

public class ComplainPresenterTableProxy extends SuperTable<Complain>{
	
	TextColumn<Complain> getCountColumn;
	TextColumn<Complain> getCustomerIdColumn;
	TextColumn<Complain> getCustomerNameColumn;
	TextColumn<Complain> getCustomerCellColumn;
	TextColumn<Complain> getContractIdColumn;
	TextColumn<Complain> getServiceIdColumn;
	TextColumn<Complain> getTicketNoColumn;
	TextColumn<Complain> getTicketDateColumn;
	TextColumn<Complain> getCallerNameColumn;
	TextColumn<Complain> getComplaintStatusColumn;
	TextColumn<Complain> getBranchColumn;
	TextColumn<Complain> getPriorityColumn;
	TextColumn<Complain> getAssignedToColumn;
	TextColumn<Complain> getBillableColumn;
	TextColumn<Complain> getComplaintDateColumn;

	/**
	 * nidhi
	 * 
	 */
	TextColumn<Complain> getDueDateColumn;
	public ComplainPresenterTableProxy(){
	 	super();
	}


	@Override
	public void createTable() {
		
		addColumngetTicketNo();
		addColumngetComplaintDate();
		/**
		 * nidhi
		 * 8-06-2018
		 */
		addColumngetDueDate();
		addColumngetCustomerId();
		addColumngetCustomerName();
		addColumngetCustomerCell();
		addColumngetAssignedTo();
		addColumngetContractId();
		addColumngetServiceId();
		addColumngetTicketDate();
		addColumngetCallerName();
		addColumngetComplaintStatus();
		addColumngetBranch();
		addColumngetPriority();
		addColumngetBillable();
		
		addColumnSorting();
	}



	private void addColumngetCustomerId() {
		getCustomerIdColumn = new TextColumn<Complain>() {
			
			@Override
			public String getValue(Complain object) {
				
				if (object.getPersoninfo()!=null)
				return object.getPersoninfo().getCount() + "";
				else
					return "N.A";
			}
		};
		table.addColumn(getCustomerIdColumn, "Customer Id");
		table.setColumnWidth(getCustomerIdColumn, 100, Unit.PX);
		getCustomerIdColumn.setSortable(true);
	}


	private void addColumngetCustomerName() {
		getCustomerNameColumn = new TextColumn<Complain>() {
			
			@Override
			public String getValue(Complain object) {
				if (object.getPersoninfo()!=null)
					return object.getPersoninfo().getFullName() + "";
					else
						return "N.A";
			}
		};
		
		table.addColumn(getCustomerNameColumn, "Customer Name");
		table.setColumnWidth(getCustomerNameColumn, 100, Unit.PX);
		getCustomerNameColumn.setSortable(true);
	}


	private void addColumngetCustomerCell() {
		getCustomerCellColumn = new TextColumn<Complain>() {
			
			@Override
			public String getValue(Complain object) {
				if (object.getPersoninfo()!=null)
					return object.getPersoninfo().getCellNumber() + "";
					else
						return "N.A";
			}
		};
		table.addColumn(getCustomerCellColumn, "Customer Cell");
		table.setColumnWidth(getCustomerCellColumn, 100, Unit.PX);
		getCustomerCellColumn.setSortable(true);
	}


	private void addColumngetContractId() {
		getContractIdColumn = new TextColumn<Complain>() {
			
			@Override
			public String getValue(Complain object) {
				if (object.getContrtactId()!=null)
					return object.getContrtactId() + "";
				else
					return "N.A";
				
			}
		};
		table.addColumn(getContractIdColumn, "Contract Id");
		table.setColumnWidth(getContractIdColumn, 100, Unit.PX);
		getContractIdColumn.setSortable(true);
	}


	private void addColumngetServiceId() {
		getServiceIdColumn = new TextColumn<Complain>() {
			
			@Override
			public String getValue(Complain object) {
				if (object.getServiceId()!=null)
				return object.getServiceId() + "";
				else
					return "N.A";
			}
		};
		table.addColumn(getServiceIdColumn, "Service Id");
		table.setColumnWidth(getServiceIdColumn, 100, Unit.PX);
		getServiceIdColumn.setSortable(true);
	}


	private void addColumngetTicketNo() {
		getTicketNoColumn = new TextColumn<Complain>() {
			
			@Override
			public String getValue(Complain object) {
					return object.getCount() + "";
			}
		};
		table.addColumn(getTicketNoColumn, "Ticket No");
		table.setColumnWidth(getTicketNoColumn, 100, Unit.PX);
		getTicketNoColumn.setSortable(true);
	}


	private void addColumngetTicketDate() {
		getTicketDateColumn = new TextColumn<Complain>() {
			
			@Override
			public String getValue(Complain object) {
				if (object.getDate() != null)
					return AppUtility.parseDate(object.getDate());
				else
					return " ";
			}
		};
		table.addColumn(getTicketDateColumn, "Creation Date");
		table.setColumnWidth(getTicketDateColumn, 140, Unit.PX);
		getTicketDateColumn.setSortable(true);
	}


	private void addColumngetCallerName() {
		getCallerNameColumn = new TextColumn<Complain>() {
			
			@Override
			public String getValue(Complain object) {
				if(object.getCallerName()!=null)
					return object.getCallerName();
				return "";
			}
		};
		
		table.addColumn(getCallerNameColumn, "Caller Name");
		table.setColumnWidth(getCallerNameColumn, 100, Unit.PX);
		getCallerNameColumn.setSortable(true);
	}


	private void addColumngetComplaintStatus() {
		getComplaintStatusColumn = new TextColumn<Complain>() {
			
			@Override
			public String getValue(Complain object) {
				if(object.getCompStatus()!=null)
					return object.getCompStatus();
				return "";
			}
		};
		
		table.addColumn(getComplaintStatusColumn, "Status");
		table.setColumnWidth(getComplaintStatusColumn, 100, Unit.PX);
		getComplaintStatusColumn.setSortable(true);
		
	}


	private void addColumngetBranch() {
		getBranchColumn = new TextColumn<Complain>() {
			
			@Override
			public String getValue(Complain object) {
				if(object.getBranch()!=null)
					return object.getBranch();
				return "";
			}
		};
		
		table.addColumn(getBranchColumn, "Branch");
		table.setColumnWidth(getBranchColumn, 100, Unit.PX);
		getBranchColumn.setSortable(true);
		
	}


	private void addColumngetPriority() {
		getPriorityColumn = new TextColumn<Complain>() {
			
			@Override
			public String getValue(Complain object) {
				if(object.getPriority()!=null)
					return object.getPriority();
				return "";
			}
		};
		
		table.addColumn(getPriorityColumn, "Priority");
		table.setColumnWidth(getPriorityColumn, 100, Unit.PX);
		getPriorityColumn.setSortable(true);
	}


	private void addColumngetAssignedTo() {
		getAssignedToColumn = new TextColumn<Complain>() {
			
			@Override
			public String getValue(Complain object) {
				if(object.getAssignto()!=null)
					return object.getAssignto();
				else
					return "";
			}
		};
		
		table.addColumn(getAssignedToColumn, "Assign To");
		table.setColumnWidth(getAssignedToColumn, 100, Unit.PX);
		getAssignedToColumn.setSortable(true);
	}


	private void addColumngetBillable() {

		getBillableColumn = new TextColumn<Complain>() {
			
			@Override
			public String getValue(Complain object) {
				if(object.getBbillable()!=null){
					if(object.getBbillable()==true)
						return "Billable";
					else
						return "Non Billable";
				}
				else
					return " ";
			}
		};
		
		table.addColumn(getBillableColumn, "Billable");
		table.setColumnWidth(getBillableColumn, 100, Unit.PX);
		getBillableColumn.setSortable(true);
	}


	private void addColumngetComplaintDate() {
		
		getComplaintDateColumn = new TextColumn<Complain>() {
			
			@Override
			public String getValue(Complain object) {
				if (object.getComplainDate() != null)
					return AppUtility.parseDate(object.getComplainDate());
				else
					return " ";
			}
		};
		
		table.addColumn(getComplaintDateColumn, "Ticket Date");
		table.setColumnWidth(getComplaintDateColumn, 140, Unit.PX);
		getComplaintDateColumn.setSortable(true);
		
	}

	private void addColumngetDueDate() {
		
		getDueDateColumn = new TextColumn<Complain>() {
			
			@Override
			public String getValue(Complain object) {
				if (object.getDueDate() != null)
					return AppUtility.parseDate(object.getDueDate());
				else
					return " ";
			}
		};
		
		table.addColumn(getDueDateColumn, "Due Date");
		table.setColumnWidth(getDueDateColumn, 140, Unit.PX);
		getDueDateColumn.setSortable(true);
		
	}

	public void addColumnSorting(){
		
		addsortinggetTicketNo();
		addsortinggetCustomerId();
		addColumnggetCustomerName();
		addsortinggetCustomerCell();
		addsortinggetContractId();
		addsortinggetServiceId();
		addsortinggetTicketDate();
		addsortinggetCallerName();
		addsortinggetComplaintStatus();
		addsortinggetBranch();
		addsortinggetPriority();
		addsortinggetAssignedTo();
		addsortinggetBillable();
		addsortinggetComplaintDate();
		/**
		 * nidhi
		 * 8-06-2018
		 */
		addsortinggetDueDate();
		
		
	}
	
	private void addsortinggetComplaintDate() {
		
		List<Complain> list=getDataprovider().getList();
		  columnSort=new ListHandler<Complain>(list);
		  columnSort.setComparator(getComplaintDateColumn, new Comparator<Complain>()
		  {
		  @Override
		  public int compare(Complain e1,Complain e2)
		  {
		  if(e1!=null && e2!=null)
		  {
		  if( e1.getComplainDate()!=null && e2.getComplainDate()!=null){
		  return e1.getComplainDate().compareTo(e2.getComplainDate());}
		  }
		  else{
		  return 0;}
		  return 0;
		  }
		  });
		  table.addColumnSortHandler(columnSort);
	}

	private void addsortinggetDueDate() {
		
		List<Complain> list=getDataprovider().getList();
		  columnSort=new ListHandler<Complain>(list);
		  columnSort.setComparator(getDueDateColumn, new Comparator<Complain>()
		  {
		  @Override
		  public int compare(Complain e1,Complain e2)
		  {
		  if(e1!=null && e2!=null)
		  {
		  if( e1.getDueDate()!=null && e2.getDueDate()!=null){
		  return e1.getDueDate().compareTo(e2.getDueDate());}
		  }
		  else{
		  return 0;}
		  return 0;
		  }
		  });
		  table.addColumnSortHandler(columnSort);
	}


	private void addsortinggetBillable() {
		
		List<Complain> list=getDataprovider().getList();
		columnSort=new ListHandler<Complain>(list);
		columnSort.setComparator(getBillableColumn, new Comparator<Complain>()
				{
			@Override
			public int compare(Complain e1,Complain e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBbillable()!=null && e2.getBbillable()!=null){
						return e1.getBbillable().compareTo(e2.getBbillable());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}


	private void addsortinggetAssignedTo() {

		List<Complain> list=getDataprovider().getList();
		columnSort=new ListHandler<Complain>(list);
		columnSort.setComparator(getAssignedToColumn, new Comparator<Complain>()
				{
			@Override
			public int compare(Complain e1,Complain e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getAssignto()!=null && e2.getAssignto()!=null){
						return e1.getAssignto().compareTo(e2.getAssignto());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}


	private void addsortinggetPriority() {
		
		List<Complain> list=getDataprovider().getList();
		columnSort=new ListHandler<Complain>(list);
		columnSort.setComparator(getCallerNameColumn, new Comparator<Complain>()
				{
			@Override
			public int compare(Complain e1,Complain e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getPriority()!=null && e2.getPriority()!=null){
						return e1.getPriority().compareTo(e2.getPriority());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}


	private void addsortinggetBranch() {

		List<Complain> list=getDataprovider().getList();
		columnSort=new ListHandler<Complain>(list);
		columnSort.setComparator(getBranchColumn, new Comparator<Complain>()
				{
			@Override
			public int compare(Complain e1,Complain e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBranch()!=null && e2.getBranch()!=null){
						return e1.getBranch().compareTo(e2.getBranch());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}


	private void addsortinggetComplaintStatus() {

		List<Complain> list=getDataprovider().getList();
		columnSort=new ListHandler<Complain>(list);
		columnSort.setComparator(getComplaintStatusColumn, new Comparator<Complain>()
				{
			@Override
			public int compare(Complain e1,Complain e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getCompStatus()!=null && e2.getCompStatus()!=null){
						return e1.getCompStatus().compareTo(e2.getCompStatus());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}


	private void addsortinggetCallerName() {

		List<Complain> list=getDataprovider().getList();
		columnSort=new ListHandler<Complain>(list);
		columnSort.setComparator(getCallerNameColumn, new Comparator<Complain>()
				{
			@Override
			public int compare(Complain e1,Complain e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getCallerName()!=null && e2.getCallerName()!=null){
						return e1.getCallerName().compareTo(e2.getCallerName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}


	private void addsortinggetTicketDate() {

		List<Complain> list=getDataprovider().getList();
		  columnSort=new ListHandler<Complain>(list);
		  columnSort.setComparator(getTicketDateColumn, new Comparator<Complain>()
		  {
		  @Override
		  public int compare(Complain e1,Complain e2)
		  {
		  if(e1!=null && e2!=null)
		  {
		  if( e1.getDate()!=null && e2.getDate()!=null){
		  return e1.getDate().compareTo(e2.getDate());}
		  }
		  else{
		  return 0;}
		  return 0;
		  }
		  });
		  table.addColumnSortHandler(columnSort);
	}


	private void addsortinggetServiceId() {

		List<Complain> list=getDataprovider().getList();
		columnSort=new ListHandler<Complain>(list);
		columnSort.setComparator(getServiceIdColumn, new Comparator<Complain>()
				{
			@Override
			public int compare(Complain e1,Complain e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getServiceId()== e2.getServiceId()){
						return 0;}
					if(e1.getServiceId()> e2.getServiceId()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);		
	}


	private void addsortinggetContractId() {
		
		List<Complain> list=getDataprovider().getList();
		columnSort=new ListHandler<Complain>(list);
		columnSort.setComparator(getContractIdColumn, new Comparator<Complain>()
				{
			@Override
			public int compare(Complain e1,Complain e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getContrtactId()== e2.getContrtactId()){
						return 0;}
					if(e1.getContrtactId()> e2.getContrtactId()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);		
	}


	private void addsortinggetCustomerCell() {
		
		List<Complain> list=getDataprovider().getList();
		columnSort=new ListHandler<Complain>(list);
		columnSort.setComparator(getCustomerCellColumn, new Comparator<Complain>()
				{
			@Override
			public int compare(Complain e1,Complain e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getPersoninfo().getCellNumber()== e2.getPersoninfo().getCellNumber()){
						return 0;}
					if(e1.getPersoninfo().getCellNumber()> e2.getPersoninfo().getCellNumber()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);		
	}


	private void addColumnggetCustomerName() {
		
		List<Complain> list=getDataprovider().getList();
		columnSort=new ListHandler<Complain>(list);
		columnSort.setComparator(getCustomerNameColumn, new Comparator<Complain>()
				{
			@Override
			public int compare(Complain e1,Complain e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getPersoninfo().getFullName()!=null && e2.getPersoninfo().getFullName()!=null){
						return e1.getPersoninfo().getFullName().compareTo(e2.getPersoninfo().getFullName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}


	private void addsortinggetCustomerId() {
		List<Complain> list=getDataprovider().getList();
		columnSort=new ListHandler<Complain>(list);
		columnSort.setComparator(getCustomerIdColumn, new Comparator<Complain>()
				{
			@Override
			public int compare(Complain e1,Complain e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getPersoninfo().getCount()== e2.getPersoninfo().getCount()){
						return 0;}
					if(e1.getPersoninfo().getCount()> e2.getPersoninfo().getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);		
	}


	private void addsortinggetTicketNo() {

		List<Complain> list=getDataprovider().getList();
		columnSort=new ListHandler<Complain>(list);
		columnSort.setComparator(getTicketNoColumn, new Comparator<Complain>()
				{
			@Override
			public int compare(Complain e1,Complain e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}


	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
