package com.slicktechnologies.client.views.contactpersonidentification.contactpersonlist;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.view.client.NoSelectionModel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreen;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreenPresenter;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.contactpersonidentification.contactpersondetails.ContactPersonForm;
import com.slicktechnologies.client.views.contactpersonidentification.contactpersondetails.ContactPersonPresenter;
import com.slicktechnologies.client.views.contactpersonidentification.contactpersondetails.ContactPersonPresenterSearchProxy;
import com.slicktechnologies.client.views.contactpersonidentification.contactpersondetails.ContactPersonPresenterTableProxy;
import com.slicktechnologies.shared.common.contactidentification.ContactPersonIdentification;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class ContactListPresenter extends TableScreenPresenter<ContactPersonIdentification> {

	TableScreen<ContactPersonIdentification>form;
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	final GenricServiceAsync async=GWT.create(GenricService.class);
	
	
	public ContactListPresenter(TableScreen<ContactPersonIdentification> view, ContactPersonIdentification model) {
		// TODO Auto-generated constructor stub
		super(view, model);
		form=view;
//		view.retriveTable(getContactListQuery());
		form.getSuperTable().connectToLocal();
		setTableSelectionOnContactDocument();
		form.getSearchpopupscreen().getDwnload().setVisible(false);	
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.CONTACTPERSONLIST,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}
	
	
	public ContactListPresenter(TableScreen<ContactPersonIdentification> view, ContactPersonIdentification model,MyQuerry querry) {
		super(view, model);
		form=view;
		view.retriveTable(querry);
		setTableSelectionOnContactDocument();
	}
	
	private void setTableSelectionOnContactDocument() {

		 
 final NoSelectionModel<ContactPersonIdentification> selectionModelMyObj = new NoSelectionModel<ContactPersonIdentification>();
	     
	     SelectionChangeEvent.Handler  tableHandler = new SelectionChangeEvent.Handler() 
	     {
	         @Override
	         public void onSelectionChange(SelectionChangeEvent event) 
	         {
	        	 //setContractRedirection();
	        	 final ContactPersonIdentification entity=selectionModelMyObj.getLastSelectedObject();
	        	 AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
	        	 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Settings/Contact Person Details",Screen.CONTACTPERSONDETAILS);
	        	// DeviceForm.flag=false;
	        	 final ContactPersonForm form = ContactPersonPresenter.initalize();
	        	 form.showWaitSymbol();
					
				 AppMemory.getAppMemory().stickPnel(form);
	        	
				 Timer timer=new Timer() 
	        	 {
	 				@Override
	 				public void run() {
	 					 form.hideWaitSymbol();
					     form.updateView(entity);
					     form.setToViewState();
	 				}
	 			};
	            
	             timer.schedule(3000); 
	          }
	     };
	     
	     // Add the handler to the selection model
	     selectionModelMyObj.addSelectionChangeHandler( tableHandler );
	     // Add the selection model to the table
	     form.getSuperTable().getTable().setSelectionModel(selectionModelMyObj);
			 

		
	}

	

	public static void initialize()
	{
		ContactPersonPresenterTableProxy gentable=new ContactPersonPresenterTableProxy();
		ContactListForm form=new ContactListForm(gentable);
		
		ContactPersonPresenterSearchProxy.staticSuperTable=gentable;
		ContactPersonPresenterSearchProxy searchpopup=new ContactPersonPresenterSearchProxy(false);
		form.setSearchpopupscreen(searchpopup);
		
		ContactListPresenter presenter=new ContactListPresenter(form,new ContactPersonIdentification());
		form.setToViewState();
		AppMemory.getAppMemory().stickPnel(form);
	}
	
	public static ContactListForm initialize(MyQuerry querry)
	{
		ContactPersonPresenterTableProxy gentable=new ContactPersonPresenterTableProxy();
		ContactListForm form=new ContactListForm(gentable);
		
		ContactPersonPresenterSearchProxy.staticSuperTable=gentable;
		ContactPersonPresenterSearchProxy searchpopup=new ContactPersonPresenterSearchProxy(false);
		form.setSearchpopupscreen(searchpopup);
		
		ContactListPresenter presenter=new ContactListPresenter(form,new ContactPersonIdentification(),querry);
		form.setToViewState();
		AppMemory.getAppMemory().stickPnel(form);
		return form;
	}
	
	
	
	
	
	

	@Override
	public void reactOnDownload() {
		
		ArrayList<ContactPersonIdentification> contactpersonarray = new ArrayList<ContactPersonIdentification>();
		List<ContactPersonIdentification> list = (List<ContactPersonIdentification>)
				form.getSearchpopupscreen().getSupertable().getDataprovider().getList();

		contactpersonarray.addAll(list);

		csvservice.setContactPersonDetails(contactpersonarray,
				new AsyncCallback<Void>() {

					@Override
					public void onFailure(Throwable caught) {
						System.out.println("RPC call Failed" + caught);

					}

					@Override
					public void onSuccess(Void result) {

						String gwt = com.google.gwt.core.client.GWT
								.getModuleBaseURL();
						final String url = gwt + "csvservlet" + "?type=" + 67;
						Window.open(url, "test", "enabled");

					}
				});
	}

	
	
	public MyQuerry getContactListQuery()
	{
		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new ContactPersonIdentification());
		return quer;
	}
	
	


	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

	
	
}
