package com.slicktechnologies.client.views.contactpersonidentification.contactpersondetails;


import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.interaction.interactiondetails.InteractionForm;
import com.slicktechnologies.client.views.project.concproject.EmpolyeeTable;
import com.slicktechnologies.client.views.settings.branch.BranchForm;
import com.slicktechnologies.client.views.settings.employee.EmployeePresenter;
import com.slicktechnologies.client.views.settings.employee.EmployeePresenterSearchProxy;
import com.slicktechnologies.client.views.settings.employee.EmployeePresenterTableProxy;
import com.slicktechnologies.client.views.settings.employee.EmployeePresenter.EmployeePresenterSearch;
import com.slicktechnologies.client.views.settings.employee.EmployeePresenter.EmployeePresenterTable;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.contactidentification.ContactPersonIdentification;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class ContactPersonPresenter extends FormTableScreenPresenter<ContactPersonIdentification> implements SelectionHandler<Suggestion>{

	
	
	public static ContactPersonForm form;
	final GenricServiceAsync async = GWT.create(GenricService.class);
	CsvServiceAsync csvservice = GWT.create(CsvService.class);
	
	public ContactPersonPresenter(FormTableScreen<ContactPersonIdentification> view, ContactPersonIdentification model) {
		super(view, model);
		
		form=(ContactPersonForm) view;
//		form.getSupertable().connectToLocal();
		form.retriveTable(getContactPersonDetailsQuery());
		form.setPresenter(this);
		
		form.getPersonInfoComposite().getId().addSelectionHandler(this);
		form.getPersonInfoComposite().getName().addSelectionHandler(this);
		form.getPersonInfoComposite().getPhone().addSelectionHandler(this);
		
		form.btnCopyCompanyAddress.addClickHandler(this);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.CONTACTPERSONDETAILS,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
		
	}
	
	public ContactPersonPresenter(FormTableScreen<ContactPersonIdentification> view, ContactPersonIdentification model,MyQuerry querry) {
		super(view, model);
		
		form=(ContactPersonForm) view;
		form.retriveTable(querry);
		form.setPresenter(this);
		
		form.getPersonInfoComposite().getId().addSelectionHandler(this);
		form.getPersonInfoComposite().getName().addSelectionHandler(this);
		form.getPersonInfoComposite().getPhone().addSelectionHandler(this);
		
		form.btnCopyCompanyAddress.addClickHandler(this);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.CONTACTPERSONDETAILS,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
		
	}

	private void reactOnNew()
	  {
		  form.setToNewState();
		  this.initalize();
	  }

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void makeNewModel() {
		model=new ContactPersonIdentification();
	}

	@Override
	public void reactOnDownload() {
		ArrayList<ContactPersonIdentification> contactpersonarray = new ArrayList<ContactPersonIdentification>();
		List<ContactPersonIdentification> list = (List<ContactPersonIdentification>)
				form.getSearchpopupscreen().getSupertable().getDataprovider().getList();

		contactpersonarray.addAll(list);

		csvservice.setContactPersonDetails(contactpersonarray,
				new AsyncCallback<Void>() {

					@Override
					public void onFailure(Throwable caught) {
						System.out.println("RPC call Failed" + caught);

					}

					@Override
					public void onSuccess(Void result) {

						String gwt = com.google.gwt.core.client.GWT
								.getModuleBaseURL();
						final String url = gwt + "csvservlet" + "?type=" + 67;
						Window.open(url, "test", "enabled");

					}
				});
	}

	
	
	public static ContactPersonForm initalize(){
//		AppMemory.getAppMemory().currentScreen.equals(ScreeenState.NEW);
		//ContactPersonForm form=new ContactPersonForm();
		
		
		//ContactPersonPresenterTable gentableSearch=new ContactPersonPresenterTableProxy();
		/*
		 * added by sheetal - 11/10/2021
		 */
		ContactPersonPresenterTableProxy gentable=new ContactPersonPresenterTableProxy();
		ContactPersonForm form=new ContactPersonForm(gentable,FormTableScreen.UPPER_MODE,true);
		   gentable.setView(form);
			gentable.applySelectionModle();
//			ContactPersonPresenterSearchProxy.staticSuperTable=gentable;
//			ContactPersonPresenterSearchProxy searchpopup=new ContactPersonPresenterSearchProxy();
//		    form.setSearchpopupscreen(searchpopup);
		
		    ContactPersonPresenter  presenter=new ContactPersonPresenter(form,new ContactPersonIdentification());
		    form.setToNewState();
		    AppMemory.getAppMemory().stickPnel(form);
		return form;
		
	}
	
	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.contactidentification.ContactPersonIdentification")
	public static class  ContactPersonPresenterTable extends SuperTable<ContactPersonIdentification> implements GeneratedVariableRefrence{

		@Override
		public Object getVarRef(String varName) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void createTable() {
			// TODO Auto-generated method stub
			
		}

		@Override
		protected void initializekeyprovider() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void addFieldUpdater() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void setEnable(boolean state) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void applyStyle() {
			// TODO Auto-generated method stub
			
		}
		
	}
//	 public static class  ContactPersonPresenterSearch extends SearchPopUpScreen<ContactPersonIdentification>{
//
//		@Override
//		public MyQuerry getQuerry() {
//			// TODO Auto-generated method stub
//			return null;
//		}
//		 
//		 
//		}
	
	/**
	 *   Added by : Sheetal - 18-10-2021
	 *
	 */
	
	public MyQuerry getContactPersonDetailsQuery()
	{
		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new ContactPersonIdentification());
		return quer;
	}


	@Override
	public void onSelection(SelectionEvent<Suggestion> event) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onClick(ClickEvent event) {
		super.onClick(event);

		/**
		 *  Date 12 April 2017 added by vijay requirement from PCAMB
		 *  for copy address from company
		 */
		if(event.getSource()==form.btnCopyCompanyAddress){
			
			/**
			 * @author Anil
			 * @since 22-10-2020
			 * fetching address as per the entity type selected
			 */
			if(form.Entype.getSelectedIndex()!=0){
				if(form.Entype.getValue().trim().equals("Customer")){
					copyCustomerAddress();
				}else if(form.Entype.getValue().trim().equals("Employee")){
					copyEmployeeAddress();
				}else if(form.Entype.getValue().trim().equals("Vendor")){
					copyVendorAddress();
				}else{
					copyCompanyAddress();
				}
			}else{
				copyCompanyAddress();
			}
			System.out.println("Inside copy address");
		}
		/**
		 * ends here
		 */
		
	}

	private void copyCustomerAddress() {
		// TODO Auto-generated method stub
		if(form.personInfoComposite==null||form.personInfoComposite.getValue()==null){
			form.showDialogMessage("Please select customer.");
			return;
		}
		
		MyQuerry querry = new MyQuerry();
	  	Company c = new Company();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
	  	
	  	filter = new Filter();
	  	filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
	  	filter.setQuerryString("count");
		filter.setIntValue(form.personInfoComposite.getIdValue());
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Customer());
		
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				System.out.println(" result size "+result.size());
				
				for(SuperModel model:result){
					Customer comp=(Customer)model;
					form.acshippingcomposite.clear();
					form.acshippingcomposite.setValue(comp.getAdress());
				}
			}
		});
	}

	private void copyEmployeeAddress() {
		// TODO Auto-generated method stub
		if(form.employeeInfoComposite==null||form.employeeInfoComposite.getValue()==null){
			form.showDialogMessage("Please select employee.");
			return;
		}
		
		MyQuerry querry = new MyQuerry();
	  	Company c = new Company();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
	  	
	  	filter = new Filter();
	  	filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
	  	filter.setQuerryString("count");
		filter.setIntValue(form.employeeInfoComposite.getEmployeeId());
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Employee());
		
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				System.out.println(" result size "+result.size());
				
				for(SuperModel model:result){
					Employee comp=(Employee)model;
					form.acshippingcomposite.clear();
					form.acshippingcomposite.setValue(comp.getAddress());
				}
			}
		});
	}

	private void copyVendorAddress() {
		if(form.personInfoVendorComposite==null||form.personInfoVendorComposite.getValue()==null){
			form.showDialogMessage("Please select vendor.");
			return;
		}
		
		MyQuerry querry = new MyQuerry();
	  	Company c = new Company();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
	  	
	  	filter = new Filter();
	  	filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
	  	filter.setQuerryString("count");
		filter.setIntValue(form.personInfoVendorComposite.getIdValue());
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Vendor());
		
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				System.out.println(" result size "+result.size());
				
				for(SuperModel model:result){
					Vendor comp=(Vendor)model;
					form.acshippingcomposite.clear();
					form.acshippingcomposite.setValue(comp.getPrimaryAddress());
				}
			}
		});
	}

	private void copyCompanyAddress() {
		// TODO Auto-generated method stub
		MyQuerry querry = new MyQuerry();
	  	Company c = new Company();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
	  	filter = new Filter();
	  	filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Company());
		
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				System.out.println(" result size "+result.size());
				
				for(SuperModel model:result){
					Company comp=(Company)model;
					form.acshippingcomposite.clear();
					form.acshippingcomposite.setValue(comp.getAddress());
				}
			}
		});
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		// TODO Auto-generated method stub
		
	}  
	 
}
