package com.slicktechnologies.client.views.contactpersonidentification.contactpersondetails;

import java.util.Vector;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.AddressComposite;
import com.simplesoftwares.client.library.composite.EmployeeInfoComposite;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.mywidgets.MyLongBox;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.contactidentification.ContactPersonIdentification;
import com.slicktechnologies.shared.common.helperlayer.City;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.Locality;
import com.slicktechnologies.shared.common.helperlayer.State;
import com.slicktechnologies.shared.common.personlayer.Employee;

public class ContactPersonPresenterSearchProxy extends SearchPopUpScreen<ContactPersonIdentification> implements ChangeHandler {
	
	IntegerBox ibcountId;
	TextBox Name,Email;
	MyLongBox Cell1,Cell2,Landline;
	ObjectListBox<Config> Role;
	ObjectListBox<Config> Entype;
	ObjectListBox<State> State;
	ObjectListBox<City> City;
	ObjectListBox<Locality> Locality;
	PersonInfoComposite personInfoComposite;
	EmployeeInfoComposite employeeInfoComposite;
	PersonInfoComposite personInfoVendorComposite;
	AddressComposite acshippingcomposite;
	/**This can be Reqn i future****/
//	ObjectListBox<Config> modulename;;
//	ObjectListBox<Config> processname;
	
	
	public ContactPersonPresenterSearchProxy() {
	super();
	createGui();
	}
	public ContactPersonPresenterSearchProxy(boolean b) {
		super(b);
		createGui();
	}
	
	public void initWidget()
	{
		ibcountId=new IntegerBox();
		Name=new TextBox();
		Cell1=new MyLongBox();
		Cell2=new MyLongBox();
		Landline=new MyLongBox();
		Email=new TextBox();
		acshippingcomposite=new AddressComposite();
		/*****this can req in future************/	

//		DocId=new IntegerBox();
//		modulename=new ObjectListBox<>();
//		AppUtility.MakeLiveConfig(modulename, Screen.MODULENAME);
//		processname=new ObjectListBox<>();
//		AppUtility.MakeLiveConfig(processname, Screen.PROCESSNAME);
		
		Role=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(Role, Screen.CONTACTROLE);
		Entype=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(Entype, Screen.PERSONTYPE);
		Entype.addChangeHandler(this);

		
		State=new ObjectListBox<State>();
		initializeState();
		City=new ObjectListBox<City>();
		initializeCity();
		Locality=new ObjectListBox<Locality>();
		initializeLocality();
	
	
		MyQuerry custquerry = new MyQuerry();
		Filter custfilteer = new Filter();
		custfilteer.setQuerryString("status");
		custfilteer.setBooleanvalue(true);
		custquerry.setQuerryObject(new Customer());
		personInfoComposite = new PersonInfoComposite(custquerry, false);

		MyQuerry vendorquerry = new MyQuerry();
		Filter vendorfilteer = new Filter();
		vendorfilteer.setQuerryString("status");
		vendorfilteer.setBooleanvalue(true);
		vendorquerry.setQuerryObject(new Vendor());
		personInfoVendorComposite = new PersonInfoComposite(vendorquerry, false);

		MyQuerry emplquerry = new MyQuerry();
		Filter emplfilteer = new Filter();
		emplfilteer.setQuerryString("status");
		emplfilteer.setBooleanvalue(true);
		emplquerry.setQuerryObject(new Employee());
		employeeInfoComposite = new EmployeeInfoComposite(emplquerry, false);

		personInfoComposite.setEnabled(false);
		employeeInfoComposite.setEnable(false);
		personInfoVendorComposite.setEnabled(false);

		employeeInfoComposite.setVisible(false);
		personInfoComposite.setVisible(false);
		personInfoVendorComposite.setVisible(false);

		
	
	}

/***************************************Create Screen*********************************************************************/	
	
	public void createScreen() {
		initWidget();
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder("Name",Name);
		FormField name= fbuilder.setMandatoryMsg("Name is Mandatory").setRowSpan(0).setColSpan(0).build();	
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingContactPersonInformation=fbuilder.setlabel("Person Address").widgetType(FieldType.Grouping).setMandatory(true).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingEntityInformation=fbuilder.setlabel("Select Entity").widgetType(FieldType.Grouping).setMandatory(true).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingContactInformation=fbuilder.setlabel("Contact Person Informantion").widgetType(FieldType.Grouping).setMandatory(true).setColSpan(4).build();
		
//		fbuilder = new FormFieldBuilder("Process Name",processname);
//		FormField pname= fbuilder.setMandatory(true).setRowSpan(0).setColSpan(0).build();
//		
//		fbuilder = new FormFieldBuilder("Module Name",modulename);
//		FormField modulename= fbuilder.setMandatory(true).setRowSpan(0).setColSpan(0).build();
		
//		fbuilder = new FormFieldBuilder("ID",Id);
//		FormField id= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Role",Role);
		FormField role= fbuilder.setMandatory(true).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("ID",ibcountId);
		FormField fibcountId= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Entity Type",Entype);
		FormField Entype= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("State",State);
		FormField state= fbuilder.setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("City",City);
		FormField city= fbuilder.setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Locality",Locality);
		FormField locality= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Cell 1",Cell1);
		FormField cell1= fbuilder.setRowSpan(0).setColSpan(0).build();
//		fbuilder = new FormFieldBuilder("ID",ID);
//		FormField id= fbuilder.setRowSpan(0).setColSpan(0).build();
//		fbuilder = new FormFieldBuilder("LandLine",Landline);
//		FormField landline= fbuilder.setRowSpan(0).setColSpan(0).build();
//		fbuilder = new FormFieldBuilder("Email",Email);
//		FormField email= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("", personInfoComposite);
		FormField fpersonInfoComposite = fbuilder.setMandatory(true).setRowSpan(0).setColSpan(4).build();
				

		fbuilder = new FormFieldBuilder("", employeeInfoComposite);
		FormField femployeeInfoComposite = fbuilder.setMandatory(true).setRowSpan(0).setColSpan(4).build();
				

		fbuilder = new FormFieldBuilder("", personInfoVendorComposite);
		FormField fvendorInfoComposite = fbuilder.setMandatory(true).setRowSpan(0).setColSpan(4).build();

		
		
		
	//for adding on form...	
		
		FormField[][] formfield = {
				{fgroupingEntityInformation},
				{fibcountId,Entype},
				{fpersonInfoComposite},{fvendorInfoComposite},{femployeeInfoComposite},
				{fgroupingContactInformation},
				{role,name,cell1},
			//	{Landline,Fax,email1,email2},
				{fgroupingContactPersonInformation},
				{state,city,locality},
		};
		this.fields=formfield;
		}

	/**************************************************Filter Querry********************************************************/////////
	
	@Override
	public MyQuerry getQuerry() {
		
		 Vector<Filter> filtervec=new Vector<Filter>();
		  Filter temp=null;
		  
		  if(Entype.getSelectedIndex()!=0)
		  {
			  temp=new Filter();
			  temp.setStringValue(Entype.getValue().trim());
			  temp.setQuerryString("entype");
			  filtervec.add(temp);
		  }
		  
		  if(ibcountId.getValue()!=null)
		  {
			  temp=new Filter();
			  temp.setIntValue(ibcountId.getValue());
			  temp.setQuerryString("count");
			  filtervec.add(temp);
		  }
		  
		  if(Role.getSelectedIndex()!=0)
		  {
		  temp=new Filter();
		  temp.setStringValue(Role.getValue().trim());
		  temp.setQuerryString("role");
		  filtervec.add(temp);
		  }
		  
		  if(State.getSelectedIndex()!=0)
		  {
		  temp=new Filter();
		  temp.setStringValue(State.getValue().trim());
		  temp.setQuerryString("contact.state");
		  filtervec.add(temp);
		  }
		  
		  if(City.getSelectedIndex()!=0)
		  {
		  temp=new Filter();
		  temp.setStringValue(City.getValue().trim());
		  temp.setQuerryString("contact.city");
		  filtervec.add(temp);
		  }
		  
		  if(Locality.getSelectedIndex()!=0)
		  {
		  temp=new Filter();
		  temp.setStringValue(Locality.getValue().trim());
		  temp.setQuerryString("contact.locality");
		  filtervec.add(temp);
		  }
		  
		  
		  
		  if(!Name.getValue().equals(""))
		  {
		  temp=new Filter();
		  temp.setStringValue(Name.getValue().trim());
		  temp.setQuerryString("name");
		  filtervec.add(temp);
		  }
		  
		  
		 /**********************this can be use in future**************************/ 
		  
//		  if(processname.getSelectedIndex()!=0){
//		  temp=new Filter();
//		  temp.setStringValue(processname.getValue().trim());
//		  temp.setQuerryString("processName");
//		  filtervec.add(temp);
//		  }
//		  if(modulename.getSelectedIndex()!=0){
//		  temp=new Filter();
//		  temp.setStringValue(modulename.getValue().trim());
//		  temp.setQuerryString("modelname");
//		  filtervec.add(temp);
//		  }
		 
		  if(Cell1.getValue()!=null)
		  {
			  temp=new Filter();
			  temp.setLongValue(Cell1.getValue());
			  temp.setQuerryString("cell");
			  filtervec.add(temp);
		  }
		  
		  if(Cell2.getValue()!=null)
		  {
			  temp=new Filter();
			  temp.setLongValue(Cell2.getValue());
			  temp.setQuerryString("cell1");
			  filtervec.add(temp);
		  }
		  
		  if(Landline.getValue()!=null)
		  {
			  temp=new Filter();
			  temp.setLongValue(Landline.getValue());
			  temp.setQuerryString("landline");
			  filtervec.add(temp);
		  }
		  
		  if(!Email.getValue().equals(""))
		  {
			  temp=new Filter();
			  temp.setStringValue(Email.getValue().trim());
			  temp.setQuerryString("email");
			  filtervec.add(temp);
		  }
		  
		  if(!personInfoComposite.getId().getValue().equals(""))
		  {
			  temp=new Filter();
			  temp.setIntValue(Integer.parseInt(personInfoComposite.getId().getValue()));
			  temp.setQuerryString("personInfo.count");
			  filtervec.add(temp);
		  }
		  
		  if(!personInfoComposite.getName().getValue().equals(""))
		  {
			  temp=new Filter();
			  temp.setStringValue(personInfoComposite.getName().getValue().trim());
			  temp.setQuerryString("personInfo.fullName");
			  filtervec.add(temp);
		  }
		  
		  if(!personInfoComposite.getPhone().getValue().equals(""))
		  {
			  temp=new Filter();
			  temp.setLongValue(Long.parseLong(personInfoComposite.getPhone().getValue().trim()));
			  temp.setQuerryString("personInfo.cellNumber");
			  filtervec.add(temp);
		  }
		  
		  if(!personInfoVendorComposite.getId().getValue().equals(""))
		  {
			  temp=new Filter();
			  temp.setIntValue(Integer.parseInt(personInfoVendorComposite.getId().getValue()));
			  temp.setQuerryString("personVendorInfo.count");
			  filtervec.add(temp);
		  }
		  
		  if(!personInfoVendorComposite.getName().getValue().equals(""))
		  {
			  temp=new Filter();
			  temp.setStringValue(personInfoVendorComposite.getName().getValue().trim());
			  temp.setQuerryString("personVendorInfo.fullName");
			  filtervec.add(temp);
		  }
		  
		  if(!personInfoVendorComposite.getPhone().getValue().equals(""))
		  {
			  temp=new Filter();
			  temp.setLongValue(Long.parseLong(personInfoVendorComposite.getPhone().getValue().trim()));
			  temp.setQuerryString("personVendorInfo.cellNumber");
			  filtervec.add(temp);
		  }
		  
		  if(!employeeInfoComposite.getId().getValue().equals(""))
		  {
			  temp=new Filter();
			  temp.setIntValue(Integer.parseInt(employeeInfoComposite.getId().getValue()));
			  temp.setQuerryString("employeeInfo.count");
			  filtervec.add(temp);
		  }
		  
		  if(!employeeInfoComposite.getName().getValue().equals(""))
		  {
			  temp=new Filter();
			  temp.setStringValue(employeeInfoComposite.getName().getValue().trim());
			  temp.setQuerryString("employeeInfo.fullName");
			  filtervec.add(temp);
		  }
		  
		  if(!employeeInfoComposite.getPhone().getValue().equals(""))
		  {
			  temp=new Filter();
			  temp.setLongValue(Long.parseLong(employeeInfoComposite.getPhone().getValue().trim()));
			  temp.setQuerryString("employeeInfo.cellNumber");
			  filtervec.add(temp);
		  }
		  
		  
		  
		  
		  
		  MyQuerry querry= new MyQuerry();
		  querry.setFilters(filtervec);
		  querry.setQuerryObject(new ContactPersonIdentification());
		  return querry;
	}		
		
///***********************************************Querry initialize methode************************************************/////	
	
		protected void initializeState(){
			MyQuerry qur=new MyQuerry(new Vector<Filter>(), new State());
			State.MakeLive(qur);
		}
		protected void initializeCity(){
			MyQuerry qur=new MyQuerry(new Vector<Filter>(), new City());
			City.MakeLive(qur);
		}
		protected void initializeLocality(){
			MyQuerry qur=new MyQuerry(new Vector<Filter>(), new Locality());
			Locality.MakeLive(qur);
		}
//		protected void initializeCustName(){
//			MyQuerry qur=new MyQuerry(new Vector<Filter>(), new Locality());
//			Name.MakeLive(qur);
//		}
		@Override
		public void onChange(ChangeEvent event) {

			
			if (event.getSource() == Entype) {

				if (Entype.getSelectedIndex() == 0) {
					clearComposit();
					employeeInfoComposite.setVisible(false);
					personInfoComposite.setVisible(false);
					personInfoVendorComposite.setVisible(false);

					personInfoComposite.setEnabled(false);
					employeeInfoComposite.setEnable(false);
					personInfoVendorComposite.setEnabled(false);
				}
				if ( Entype.getValue().trim().equals("Customer")) {
					clearComposit();
					employeeInfoComposite.setVisible(false);
					personInfoComposite.setVisible(true);
					personInfoVendorComposite.setVisible(false);

					personInfoComposite.setEnabled(true);
					employeeInfoComposite.setEnable(false);
					personInfoVendorComposite.setEnabled(false);

				}
				if ( Entype.getValue().trim().equals("Employee")) {
					clearComposit();
					employeeInfoComposite.setVisible(true);
					personInfoComposite.setVisible(false);
					personInfoVendorComposite.setVisible(false);

					personInfoComposite.setEnabled(false);
					employeeInfoComposite.setEnable(true);
					personInfoVendorComposite.setEnabled(false);

				}
				if ( Entype.getValue().trim().equals("Vendor")) {
					clearComposit();
					employeeInfoComposite.setVisible(false);
					personInfoComposite.setVisible(false);
					personInfoVendorComposite.setVisible(true);

					personInfoComposite.setEnabled(false);
					employeeInfoComposite.setEnable(false);
					personInfoVendorComposite.setEnabled(true);
				}
			}

		}
private void clearComposit() {

	if (personInfoComposite.getValue() != null) {
		personInfoComposite.getId().setValue(null);
		personInfoComposite.getName().setValue(null);
		personInfoComposite.getPhone().setValue(null);
	}
	if (personInfoVendorComposite.getValue() != null) {
		personInfoVendorComposite.getId().setValue(null);
		personInfoVendorComposite.getName().setValue(null);
		personInfoVendorComposite.getPhone().setValue(null);
	}
	if (employeeInfoComposite.getValue() != null) {
		employeeInfoComposite.getId().setValue(null);
		employeeInfoComposite.getName().setValue(null);
		employeeInfoComposite.getPhone().setValue(null);
		employeeInfoComposite.setBranch("");
		employeeInfoComposite.setDepartment(null);
		
		employeeInfoComposite.setEmployeeType(null);
		employeeInfoComposite.setEmpDesignation(null);
		employeeInfoComposite.setEmpRole(null);
	}


}
@Override
public boolean validate() {
	return true;
}
	}
