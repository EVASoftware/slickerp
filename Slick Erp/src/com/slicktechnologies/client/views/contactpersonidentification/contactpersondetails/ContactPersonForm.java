package com.slicktechnologies.client.views.contactpersonidentification.contactpersondetails;

import java.util.ArrayList;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.AddressComposite;
import com.simplesoftwares.client.library.composite.EmployeeInfoComposite;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.composite.SocialInfoComposites;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.MyLongBox;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.customerbranchdetails.CustomerBranchPresenter.CustomerBranchPresenterTable;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.client.views.history.HistoryPopup;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.contactidentification.ContactPersonIdentification;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class ContactPersonForm extends FormTableScreen<ContactPersonIdentification> implements ChangeHandler{

	public AddressComposite acshippingcomposite;
	TextBox Name;
	TextBox tbcountid;
	MyLongBox Cell,Cell1,Landline,Fax;
	ObjectListBox<Config> Role;
	ObjectListBox<ConfigCategory> modulename;
	public ObjectListBox<Config> Entype;
	TextBox email,email1;
	IntegerBox DocId;
	ObjectListBox<Type> processname;
	SocialInfoComposites socialInfoComposite;
	public PersonInfoComposite personInfoComposite;
	public EmployeeInfoComposite employeeInfoComposite;
	public PersonInfoComposite personInfoVendorComposite;
	
	ContactPersonIdentification contactpersonobj;
	
	final GenricServiceAsync genasync=GWT.create(GenricService.class);
	
	// Date 12 April 2017 added by vijay requirement from PCAMB
	Button btnCopyCompanyAddress;
		
	/** date 17/10/2017 added by komal to add poc name in contact detail form **/
	TextBox pocName;
	
//	public ContactPersonForm() {
//		super();
//		createGui();
//	}
//	
//	public ContactPersonForm  (String[] processlevel, FormField[][] fields,
//			FormStyle formstyle) 
//	{
//		super(processlevel, fields, formstyle);
//		createGui();
//		
//		
//	}

	/**
	 *     Added by : Sheetal  11/10/2021
	 *     Des  : As per the Requirement-change contact person detail Screen to table screen like branch
	 */
	
	public ContactPersonForm  (SuperTable<ContactPersonIdentification> table, int mode,boolean captionmode) {
		super(table, mode, captionmode);
		createGui();
	}
	
	/**
	 * @author Anil @since 09-11-2021
	 * @param table
	 * @param mode
	 * @param captionmode
	 */
	public ContactPersonForm  (SuperTable<ContactPersonIdentification> table, int mode,boolean captionmode,boolean popupFlag) {
		super(table, mode, captionmode,popupFlag);
		createGui();
	}
	public ContactPersonForm (ContactPersonPresenterTableProxy gentable) {
		super(gentable);
		createGui();
	}
	private void initalizeWidget(){
		/**
		 * nidhi
		 * 13-06-2018
		 * for make email mandatory
		 */
		boolean validate = AppUtility.checkForProcessConfigurartionIsActiveOrNot("ContactPersonIdentification", "MakeAddressNonMandatory");
		/**
		 * end
		 */
		acshippingcomposite=new AddressComposite(validate);
		Name=new TextBox();
		Cell=new MyLongBox();
		email=new TextBox();
		email1=new TextBox();
		Cell1=new MyLongBox();
		Landline=new MyLongBox();
		Fax=new MyLongBox();
		socialInfoComposite=new SocialInfoComposites();
		tbcountid=new TextBox();
		tbcountid.setEnabled(false);
		Role=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(Role, Screen.CONTACTROLE);
		
		Entype=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(Entype, Screen.PERSONTYPE);
		Entype.addChangeHandler(this);
		
		/** date 17/10/2017 added by komal to add poc name in contact detail form **/
		pocName = new TextBox();
		
		/***************** Person,Vendor,Employee Composite DEPENDS ON PERSON TYPE **********/

		MyQuerry custquerry = new MyQuerry();
		Filter custfilteer = new Filter();
		custfilteer.setQuerryString("status");
		custfilteer.setBooleanvalue(true);
		custquerry.setQuerryObject(new Customer());
		personInfoComposite = new PersonInfoComposite(custquerry, false);

		MyQuerry vendorquerry = new MyQuerry();
		Filter vendorfilteer = new Filter();
		vendorfilteer.setQuerryString("status");
		vendorfilteer.setBooleanvalue(true);
		vendorquerry.setQuerryObject(new Vendor());
		personInfoVendorComposite = new PersonInfoComposite(vendorquerry, false);

		MyQuerry emplquerry = new MyQuerry();
		Filter emplfilteer = new Filter();
		emplfilteer.setQuerryString("status");
		emplfilteer.setBooleanvalue(true);
//		emplquerry.setQuerryObject(new Employee());
		/**
		 * Date : 28-10-2017 BY KOMAL
		 */
		emplquerry.setQuerryObject(new EmployeeInfo());
		/**
		 * End
		 */
		employeeInfoComposite = new EmployeeInfoComposite(emplquerry, false);

		personInfoComposite.setEnabled(false);
		employeeInfoComposite.setEnable(false);
		personInfoVendorComposite.setEnabled(false);

		employeeInfoComposite.setVisible(false);
		personInfoComposite.setVisible(false);
		personInfoVendorComposite.setVisible(false);
		
		btnCopyCompanyAddress = new Button("Copy Address");


	}

	
	@Override
	public void createScreen() {
		initalizeWidget();
	
		this.processlevelBarNames = new String[] { "New" };
		if(isPopUpAppMenubar()){
//			this.processlevelBarNames=new String[]{};
			this.processlevelBarNames=null;
			Console.log("POP up is true and process level bar is null");
		}
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingContactPersonInformation=fbuilder.setlabel("Person Address").widgetType(FieldType.Grouping).setMandatory(true).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("*  Name",Name);
		FormField name1= fbuilder.setMandatory(true).setMandatoryMsg("Name is Mandatory").setRowSpan(0).setColSpan(0).build();	
		fbuilder = new FormFieldBuilder("*  Cell 1",Cell);
		FormField Cell= fbuilder.setMandatory(true).setMandatoryMsg("Cell 1 is Mandatory").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("  Cell 2",Cell1);
		FormField Cell1= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder(" LandLine",Landline);
		FormField Landline= fbuilder.setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("  Fax No",Fax);
		FormField Fax= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("*  Role",Role);
		FormField role= fbuilder.setMandatory(true).setMandatoryMsg("Role is Mandatory").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("ID",tbcountid);
		FormField ftbcountid= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	/*	fbuilder = new FormFieldBuilder("*  Email 1",email);
		FormField femail1= fbuilder.setMandatory(true).setMandatoryMsg("Email 1 is Mandatory").setRowSpan(0).setColSpan(0).build();
*/
		/**
		 * nidhi
		 * 13-06-2018
		 * for make email mandatory
		 */
		boolean emailMandatory = AppUtility.checkForProcessConfigurartionIsActiveOrNot("ContactPersonIdentification", "MakeEmailMandatory");
		
		fbuilder = new FormFieldBuilder((emailMandatory) ? "* Email Id" : "Email Id",email);
		FormField femail1= fbuilder.setMandatory(emailMandatory).setMandatoryMsg("Email 1 is Mandatory").setRowSpan(0).setColSpan(0).build();

		/**
		 * end
		 */
		fbuilder = new FormFieldBuilder("*  Entity Type",Entype);
		FormField Entype= fbuilder.setMandatory(true).setMandatoryMsg("Name is Mandatory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder(" ",acshippingcomposite);
		FormField address= fbuilder.setMandatory(true).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Email 2",email1);
		FormField email2= fbuilder.setColSpan(0).build();

		fbuilder = new FormFieldBuilder();
		FormField fgroupingEntityInformation=fbuilder.setlabel("Select Entity").widgetType(FieldType.Grouping).setMandatory(true).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingContactInformation=fbuilder.setlabel("Contact Person Informantion").widgetType(FieldType.Grouping).setMandatory(true).setColSpan(4).build();

		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingSInformation=fbuilder.setlabel("Social Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("",socialInfoComposite);
		FormField fsocialInfoComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
	
		// FOR PERSON TYPE INFORMATION
				
			fbuilder = new FormFieldBuilder("", personInfoComposite);
				FormField fpersonInfoComposite = fbuilder.setMandatory(true).setRowSpan(0).setColSpan(4).build();
						

				fbuilder = new FormFieldBuilder("", employeeInfoComposite);
				FormField femployeeInfoComposite = fbuilder.setMandatory(true).setRowSpan(0).setColSpan(4).build();
						

				fbuilder = new FormFieldBuilder("", personInfoVendorComposite);
				FormField fvendorInfoComposite = fbuilder.setMandatory(true).setRowSpan(0).setColSpan(4).build();
				
				fbuilder = new FormFieldBuilder("",btnCopyCompanyAddress);
				FormField fbtnCopyCompanyAddress= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
					
                /** date 17/10/2017 added by komal to add poc name in contact detail form **/
				
				fbuilder = new FormFieldBuilder("POC Name", pocName);
				FormField ftbPOCName = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		FormField[][] formfield = {
				{fgroupingEntityInformation},
				{ftbcountid,Entype},
				{fpersonInfoComposite},{femployeeInfoComposite},{fvendorInfoComposite},
				{fgroupingContactInformation},
				{role,name1,Cell,Cell1},
				{Landline,Fax,femail1,email2},
				{ftbPOCName},
				{fgroupingContactPersonInformation},
				{fbtnCopyCompanyAddress},
				{address},
				{fgroupingSInformation},
				{fsocialInfoComposite}
		};
		
		this.fields=formfield;
		}

	@Override
	public void updateModel(ContactPersonIdentification model) {
		
		
		if(Entype.getValue()!=null)
			model.setEntype(Entype.getValue());
		if(socialInfoComposite.getValue()!=null)
			model.setSocialInfo(socialInfoComposite.getValue());
		if(Name.getValue()!=null)
			model.setName(Name.getValue());
		if(email.getValue()!=null)
			model.setEmail(email.getValue());
		if(email1.getValue()!=null)
			model.setEmail1(email1.getValue());
		if(Role.getValue()!=null)
			model.setRole(Role.getValue());
		if(Fax.getValue()!=null)
			model.setFax(Fax.getValue());
		if(Landline.getValue()!=null)
			model.setLandline(Landline.getValue());
		if(Cell.getValue()!=null)
			model.setCell(Cell.getValue());
		if(Cell1.getValue()!=null)
			model.setCell1(Cell1.getValue());
		if(acshippingcomposite.getValue()!=null)
			model.setAddressInfo(acshippingcomposite.getValue());
		if (personInfoComposite.getValue() != null) {
			model.setPersonInfo(personInfoComposite.getValue());
		}
		if (employeeInfoComposite.getValue() != null) {
				model.setEmployeeInfo(employeeInfoComposite.getValue());
			}
		if (personInfoVendorComposite.getValue() != null) {
			model.setPersonVendorInfo(personInfoVendorComposite.getValue());
		}
		/** date 17/10/2017 added by komal to add poc name in contact detail form **/
		if(pocName.getValue() != null){
			model.setPocName(pocName.getValue());
		}
		
		manageGlobalList(model);
		
		contactpersonobj=model;
		presenter.setModel(model);

	}
	
	@Override
	public void updateView(ContactPersonIdentification view) {
		
		contactpersonobj=view;
		
		tbcountid.setValue(view.getCount()+"");
		

    	this.checkCustomerStatus(view.getPersonInfo().getCount());
    	
		
		if(view.getName()!=null)
			Name.setValue(view.getName());
		if(view.getEmail()!=null)
			email.setValue(view.getEmail());
		if(view.getEmail1()!=null)
			email1.setValue(view.getEmail1());
		if(view.getRole()!=null)
			Role.setValue(view.getRole());
		if(view.getDocumentid()!=null)
			DocId.setValue(view.getDocumentid());
		if(view.getFax()!=null)
			Fax.setValue(view.getFax());
		if(view.getCell()!=null)
			Cell.setValue(view.getCell());
		if(view.getCell1()!=null)
			Cell1.setValue(view.getCell1());
		if(view.getAddressInfo()!=null)
			acshippingcomposite.setValue(view.getAddressInfo());
		if(view.getLandline()!=null)
			Landline.setValue(view.getLandline());
		if(view.getEntype()!=null)
			Entype.setValue(view.getEntype());
		if(view.getSocialInfo()!=null)
			socialInfoComposite.setValue(view.getSocialInfo());
		if (view.getPersonInfo() != null)
			personInfoComposite.setValue(view.getPersonInfo());
		if (view.getEmployeeInfo() != null) {
			employeeInfoComposite.setEmployeeId(view.getEmployeeInfo().getCount());
			employeeInfoComposite.setEmployeeName(view.getEmployeeInfo().getFullName());
			employeeInfoComposite.setCellNumber(view.getEmployeeInfo().getCellNumber());
			employeeInfoComposite.setBranch(view.getEmployeeInfo().getBranch());
			employeeInfoComposite.setDepartment(view.getEmployeeInfo().getDepartment());
			employeeInfoComposite.setEmployeeType(view.getEmployeeInfo().getEmployeeType());
			employeeInfoComposite.setEmpDesignation(view.getEmployeeInfo().getDesignation());
			employeeInfoComposite.setEmpRole(view.getEmployeeInfo().getEmployeerole());
		}
		if (view.getPersonVendorInfo() != null)
			personInfoVendorComposite.setValue(view.getPersonVendorInfo());
		
/** date 17/10/2017 added by komal to add poc name in contact detail form **/
		
		if(view.getPocName() != null){
			pocName.setValue(view.getPocName());
		}  

		setVisibleComposite();
		presenter.setModel(view);
	}
	

	@Override
	public void toggleAppHeaderBarMenu() {
		
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		
		if(isPopUpAppMenubar()){
			return;
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.CONTACTPERSONDETAILS,LoginPresenter.currentModule.trim());
	}
	
	@Override
	public void setCount(int count)
	{
		tbcountid.setValue(count+"");
	}
	
	
	@Override
	public void setToViewState() {
		// TODO Auto-generated method stub
		super.setToViewState();
		
		
		SuperModel model=new ContactPersonIdentification();
		model=contactpersonobj;
		if(HistoryPopup.historyObj!=null){
			AppUtility.addDocumentToHistoryTable(HistoryPopup.historyObj.getModuleName(),AppConstants.CONTACTPERSONDETAILS,contactpersonobj.getCount(), Integer.parseInt(personInfoComposite.getId().getValue()),personInfoComposite.getName().getValue(),Long.parseLong(personInfoComposite.getPhone().getValue()), false, model, null);
			HistoryPopup.historyObj=null;
		}else{
			AppUtility.addDocumentToHistoryTable(LoginPresenter.currentModule.trim(),AppConstants.CONTACTPERSONDETAILS,contactpersonobj.getCount(), Integer.parseInt(personInfoComposite.getId().getValue()),personInfoComposite.getName().getValue(),Long.parseLong(personInfoComposite.getPhone().getValue()), false, model, null);
		}
	}
	
	/*********************** ON PERSON TYPE CHANGE ******************************/

	public void setVisibleComposite() {
		if (personInfoComposite.getValue() != null)
			personInfoComposite.setVisible(true);
		if (employeeInfoComposite.getValue() != null)
			employeeInfoComposite.setVisible(true);
		if (personInfoVendorComposite.getValue() != null)
			personInfoVendorComposite.setVisible(true);
	}

	public void setVisiblePersonType() {
		if (Entype.getValue().equalsIgnoreCase("Customer"))
			personInfoComposite.setVisible(true);
		if (Entype.getValue().equalsIgnoreCase("Employee"))
			employeeInfoComposite.setVisible(true);
		if (Entype.getValue().equalsIgnoreCase("Vendor"))
			personInfoVendorComposite.setVisible(true);
	}

	public void clearComposit() {
		if (personInfoComposite.getValue() != null) {
			personInfoComposite.getId().setValue(null);
			personInfoComposite.getName().setValue(null);
			personInfoComposite.getPhone().setValue(null);
		}
		if (personInfoVendorComposite.getValue() != null) {
			personInfoVendorComposite.getId().setValue(null);
			personInfoVendorComposite.getName().setValue(null);
			personInfoVendorComposite.getPhone().setValue(null);
		}
		if (employeeInfoComposite.getValue() != null) {
			employeeInfoComposite.getId().setValue(null);
			employeeInfoComposite.getName().setValue(null);
			employeeInfoComposite.getPhone().setValue(null);
			employeeInfoComposite.setBranch("");
			employeeInfoComposite.setDepartment(null);
			
			employeeInfoComposite.setEmployeeType(null);
			employeeInfoComposite.setEmpDesignation(null);
			employeeInfoComposite.setEmpRole(null);
		}

	}

	
	@Override
	public boolean validate() {
		
		boolean superres=super.validate();
		boolean compositeValidation=true;
//		boolean nrValidate=true;
		if(Entype.getSelectedIndex()!=0&&Entype.getValue().equals("Customer")&&personInfoComposite.getValue()==null){
			compositeValidation=false;
			showDialogMessage("Customer Information is Mandatory!");
		}
		if(Entype.getSelectedIndex()!=0&&Entype.getValue().equals("Vendor")&&personInfoVendorComposite.getValue()==null){
			compositeValidation=false;
			showDialogMessage("Vendor Information is Mandatory!");
		}
		if(Entype.getSelectedIndex()!=0&&Entype.getValue().equals("Employee")&&employeeInfoComposite.getValue()==null){
			compositeValidation=false;
			showDialogMessage("Employee Information is Mandatory!");
		}
//		if(Cell1.getValue() > 1000000000){
//			showDialogMessage("Please enter Cell 2 number range in 9 digits");
//			nrValidate = false;
//		}
//		if(Cell1.getValue() < 99999999){
//			showDialogMessage("Please enter Cell 2 number range in 9 digits");
//			nrValidate = false;
//		}
//		if(Cell.getValue() > 1000000000){
//			
//			showDialogMessage("Please enter Cell 1 number range in 9 digits");
//			nrValidate = false;
//			
//		}
//		if(Cell.getValue() < 99999999){
//			
//			showDialogMessage("Please enter Cell 1 number range in 9 digits");
//			nrValidate = false;
//			
//		}
//		if(Landline.getValue()<1000000000){
//			
//			nrValidate = false;
//		}
//		if(Landline.getValue()<999999){
//			
//			nrValidate = false;
//			
//		}
		
		
		return superres&&compositeValidation;
	}
	
	

	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		
		tbcountid.setEnabled(false);
	}

	/*******************************************FORM FIEALD GETTER SETTER******************************************************/

	public ObjectListBox<Config> getOblPersonType() {
		return Entype;
	}

	public void setOblPersonType(ObjectListBox<Config> Entype) {
		this.Entype = Entype;
	}

	public PersonInfoComposite getPersonInfoComposite() {
		return personInfoComposite;
	}

	public void setPersonInfoComposite(PersonInfoComposite personInfoComposite) {
		this.personInfoComposite = personInfoComposite;
	}

	public PersonInfoComposite getPersonInfoVendorComposite() {
		return personInfoVendorComposite;
	}

	public void setPersonInfoVendorComposite(
			PersonInfoComposite personInfoVendorComposite) {
		this.personInfoVendorComposite = personInfoVendorComposite;
	}
	
	

	public TextBox getPocName() {
		return pocName;
	}

	public void setPocName(TextBox pocName) {
		this.pocName = pocName;
	}

	/************************************************for Person Type OnChange******************************************/////////////

	@Override
	public void onChange(ChangeEvent event) {
	

		if (event.getSource() == Entype) {

			if (Entype.getSelectedIndex() == 0) {
				clearComposit();
				employeeInfoComposite.setVisible(false);
				personInfoComposite.setVisible(false);
				personInfoVendorComposite.setVisible(false);

				personInfoComposite.setEnabled(false);
				employeeInfoComposite.setEnable(false);
				personInfoVendorComposite.setEnabled(false);
				
				acshippingcomposite.clear();
			}
			if ( Entype.getValue().trim().equals("Customer")) {
				clearComposit();
				employeeInfoComposite.setVisible(false);
				personInfoComposite.setVisible(true);
				personInfoVendorComposite.setVisible(false);

				personInfoComposite.setEnabled(true);
				employeeInfoComposite.setEnable(false);
				personInfoVendorComposite.setEnabled(false);
				acshippingcomposite.clear();
			}
			if ( Entype.getValue().trim().equals("Employee")) {
				clearComposit();
				employeeInfoComposite.setVisible(true);
				personInfoComposite.setVisible(false);
				personInfoVendorComposite.setVisible(false);

				personInfoComposite.setEnabled(false);
				employeeInfoComposite.setEnable(true);
				personInfoVendorComposite.setEnabled(false);
				acshippingcomposite.clear();
			}
			if ( Entype.getValue().trim().equals("Vendor")) {
				clearComposit();
				employeeInfoComposite.setVisible(false);
				personInfoComposite.setVisible(false);
				personInfoVendorComposite.setVisible(true);

				personInfoComposite.setEnabled(false);
				employeeInfoComposite.setEnable(false);
				personInfoVendorComposite.setEnabled(true);
				
				acshippingcomposite.clear();
			}
		}

	}
	
	
	protected void checkCustomerStatus(int cCount)
	{
		MyQuerry querry=new MyQuerry();
		Company c=new Company();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		
		temp=new Filter();
		temp.setQuerryString("companyId");
		temp.setLongValue(c.getCompanyId());
		filtervec.add(temp);
		
		temp=new Filter();
		temp.setQuerryString("count");
		temp.setIntValue(cCount);
		filtervec.add(temp);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Customer());
		
		genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onFailure(Throwable caught) {
				showDialogMessage("An Unexpected error occurred!");
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
					for(SuperModel model:result)
					{
						Customer custEntity = (Customer)model;
						if(AppConstants.ACTIVE.equals(custEntity.getStatus().trim())){
							 getPersonInfoComposite().getId().getElement().addClassName("personactive");
							 getPersonInfoComposite().getName().getElement().addClassName("personactive");
							 getPersonInfoComposite().getPhone().getElement().addClassName("personactive");
							 getPersonInfoComposite().getTbpocName().getElement().addClassName("personactive");
						}
					}
				}
			 });
		}

	
	public void manageGlobalList(ContactPersonIdentification contactPersonModel)
	{
		ScreeenState scrState=AppMemory.getAppMemory().currentState;
		
		if(scrState.equals(ScreeenState.NEW))
		{
			LoginPresenter.globalContactpersonlist.add(contactPersonModel);
		}
		if(scrState.equals(ScreeenState.EDIT)){
			for(int i=0;i<LoginPresenter.globalContactpersonlist.size();i++)
			{
				if(LoginPresenter.globalContactpersonlist.get(i).getId().equals(contactPersonModel.getId()))
				{
					LoginPresenter.globalContactpersonlist.add(contactPersonModel);
					LoginPresenter.globalContactpersonlist.remove(i);
				}
			}
		}
		
	}
}
