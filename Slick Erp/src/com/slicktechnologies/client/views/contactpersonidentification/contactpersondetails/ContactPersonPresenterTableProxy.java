package com.slicktechnologies.client.views.contactpersonidentification.contactpersondetails;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.views.contactpersonidentification.contactpersondetails.ContactPersonPresenter.ContactPersonPresenterTable;
import com.slicktechnologies.shared.common.contactidentification.ContactPersonIdentification;

public class ContactPersonPresenterTableProxy extends SuperTable<ContactPersonIdentification> {
	TextColumn<ContactPersonIdentification>getColumnConPerID;
	TextColumn<ContactPersonIdentification>getColumnConPerRole;
	TextColumn<ContactPersonIdentification>getColumnConPerName;
	TextColumn<ContactPersonIdentification>getColumnConPerEntype;
	TextColumn<ContactPersonIdentification>getColumnConPerCell1;
	TextColumn<ContactPersonIdentification>getColumnConPerCell2;
	TextColumn<ContactPersonIdentification>getColumnConPerLandline;
	TextColumn<ContactPersonIdentification>getColumnConPerEmail;
	TextColumn<ContactPersonIdentification>getColumnConPerState;
	TextColumn<ContactPersonIdentification>getColumnConPerCity;
	TextColumn<ContactPersonIdentification>getColumnConPerLocality;
	
	/** date 17/10/2017 added by komal to show pocname and bpname in table **/
	TextColumn<ContactPersonIdentification>getColumnConPerPOCName;
    TextColumn<ContactPersonIdentification>getColumnConPerBPName;
	
	public ContactPersonPresenterTableProxy(){
		super();
	}

	@Override 	
	public void createTable() {
		createColumnConPerID();
		createColumnConPerEntype();
		createColumnConPerName();
		createColumnConPerRole();
		createColumnConPerBPName();
//		createColumnConPerCell1();
//		createColumnConPerEmail();
//		createColumnConPerState();
//		createColumnConPerCity();
//		createColumnConPerLocality();
		createColumnConPerPOCName();
	}
	

	/******************************************** View Column ********************************************/

	private void createColumnConPerID() {

		getColumnConPerID=new TextColumn<ContactPersonIdentification>() {
			
			@Override
			public String getValue(ContactPersonIdentification object) {
				return object.getCount()+"";
			}
		};
		table.addColumn(getColumnConPerID, "ID");
		getColumnConPerID.setSortable(true);
	}

		
	
	
	private void createColumnConPerCell1() {

		getColumnConPerCell1=new TextColumn<ContactPersonIdentification>() {
			
			@Override
			public String getValue(ContactPersonIdentification object) {
				return object.getCell()+"";
			}
		};
		table.addColumn(getColumnConPerCell1, "Cell 1");
		getColumnConPerCell1.setSortable(true);
	}

	private void createColumnConPerEmail() {
		getColumnConPerEmail=new TextColumn<ContactPersonIdentification>() {
			
			@Override
			public String getValue(ContactPersonIdentification object) {
				if(object.getEmail()!=null){
					return object.getEmail();
				}else{
					
				return "NA";
			}
			}
		};
		table.addColumn(getColumnConPerEmail, "Email");
		getColumnConPerEmail.setSortable(true);		
	}

	private void createColumnConPerState() {

		getColumnConPerState=new TextColumn<ContactPersonIdentification>() {
			
			@Override
			public String getValue(ContactPersonIdentification object) {
				if(object.getAddressInfo().getState()!=null){
					return object.getAddressInfo().getState();
				}else{
				return "NA";
			}
			}
		};
		table.addColumn(getColumnConPerState, "State");
		getColumnConPerState.setSortable(true);
	}

	private void createColumnConPerCity() {

		getColumnConPerCity=new TextColumn<ContactPersonIdentification>() {
			
			@Override
			public String getValue(ContactPersonIdentification object) {
				if(object.getAddressInfo().getCity()!=null){
					return object.getAddressInfo().getCity();
				}else{
				return"NA";
			}
			}
		};
		table.addColumn(getColumnConPerCity, "City");
		getColumnConPerCity.setSortable(true);
	}

	private void createColumnConPerLocality() {
		
		getColumnConPerLocality=new TextColumn<ContactPersonIdentification>() {
			
			@Override
			public String getValue(ContactPersonIdentification object) {
				if(object.getAddressInfo().getLocality()!=null){
					return object.getAddressInfo().getLocality();
				}
					else{
				return "NA";
			
				}
			}	
		};
		table.addColumn(getColumnConPerLocality, "Locality");
		getColumnConPerLocality.setSortable(true);
	}

	

	private void createColumnConPerEntype() {
		getColumnConPerEntype=new TextColumn<ContactPersonIdentification>() {
			
			@Override
			public String getValue(ContactPersonIdentification object) {
				if(object.getEntype()!=null){
					return object.getEntype();
				}
				else{
					return "NA";
				}
			}
		};
		table.addColumn(getColumnConPerEntype, "Entity Type");
		getColumnConPerEntype.setSortable(true);
	}


	private void createColumnConPerName() {
		getColumnConPerName=new TextColumn<ContactPersonIdentification>() {
			
			@Override
			public String getValue(ContactPersonIdentification object) {
				if(object.getName()!=null){
					return object.getName();
				}
					else{
						return "NA";
					}
				}

			
		};
		table.addColumn(getColumnConPerName, "Name");
		getColumnConPerName.setSortable(true);
	}

	private void createColumnConPerRole() {

		getColumnConPerRole=new TextColumn<ContactPersonIdentification>() {

			@Override
			public String getValue(ContactPersonIdentification object) {
				if(object.getRole()!=null){
					return object.getRole();
				}
				else{
				return "NA";
			}
			}
		};
		table.addColumn(getColumnConPerRole, "Role");
		getColumnConPerRole.setSortable(true);
	}

	//*********use in future*************/
	
//	private void createColumnConPerID() {
//		getColumnConPerId=new TextColumn<ContactPersonIdentification>() {
//			@Override
//			public String getValue(ContactPersonIdentification object) {
//				return object.getCount()+"";
//			}
//		};
//		table.addColumn(getColumnConPerId, "ID");
//		getColumnConPerId.setSortable(true);
//	}
	
//	private void createColumnConPerDocID() {
//
//	getColumnConPerDocId=new TextColumn<ContactPersonIdentification>() {
//			
//			@Override
//			public String getValue(ContactPersonIdentification object) {
//				return object.getDocumentid()+"";
//			}
//		};
//		table.addColumn(getColumnConPerDocId, "Document Id");
//		getColumnConPerDocId.setSortable(true);
//	}

//	private void createColumnConPerProcessName() {
//	getColumnConPerProcessName=new TextColumn<ContactPersonIdentification>() {
//		
//		@Override
//		public String getValue(ContactPersonIdentification object) {
//			return object.getProcessName();
//		}
//	};
//	table.addColumn(getColumnConPerProcessName, "Process Name");
//	getColumnConPerProcessName.setSortable(true);
//}

//	private void createColumnConPerModuleName() {
//	getColumnConPerModuleName=new TextColumn<ContactPersonIdentification>() {
//		
//		@Override
//		public String getValue(ContactPersonIdentification object) {
//			return object.getModulename();
//		}
//	};
//	
//	table.addColumn(getColumnConPerModuleName, "Module Name");
//	getColumnConPerModuleName.setSortable(true);
//}

/*******************************************************************************************************************/
	

		
	/************************************** Column Sorting *****************************************/

	public void addColumnSorting() {
		addColumnEntityTypeSorting();
		addColumnIDSorting();
		addColumnNameSorting();
		addColumnRoleSorting();
		addColumnCell1Sorting();
		addColumnEmailSorting();
		addColumnStateSorting();
		addColumnCitySorting();
		addColumnLocalitySorting();
		addColumnLandlineSorting();
		addColumnPOCNameSorting();
		addColumnBPNameSorting();
		}

private void addColumnIDSorting() {

	List<ContactPersonIdentification> list = getDataprovider().getList();
	columnSort = new ListHandler<ContactPersonIdentification>(list);
	Comparator<ContactPersonIdentification> quote = new Comparator<ContactPersonIdentification>() {
		@Override
		public int compare(ContactPersonIdentification e1, ContactPersonIdentification e2) {
			if (e1 != null && e2 != null) {
				if(e1.getCount()== e2.getCount()){
				  return 0;}
			  if(e1.getCount()> e2.getCount()){
				  return 1;}
			  else{
				  return -1;}} else
				return 0;
		}
	};
	columnSort.setComparator(getColumnConPerID, quote);
	table.addColumnSortHandler(columnSort);
	
	}


	private void addColumnCell1Sorting() {
		List<ContactPersonIdentification> list = getDataprovider().getList();
		columnSort = new ListHandler<ContactPersonIdentification>(list);
		Comparator<ContactPersonIdentification> quote = new Comparator<ContactPersonIdentification>() {
			@Override
			public int compare(ContactPersonIdentification e1, ContactPersonIdentification e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCell() != null
							&& e2.getCell() != null)
						return e1.getCell().compareTo(
								e2.getCell());
					return 0;
				} else
					return 0;
			}
		};
		columnSort.setComparator(getColumnConPerCell1, quote);
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnEmailSorting() {
		List<ContactPersonIdentification> list = getDataprovider().getList();
		columnSort = new ListHandler<ContactPersonIdentification>(list);
		Comparator<ContactPersonIdentification> quote = new Comparator<ContactPersonIdentification>() {
			@Override
			public int compare(ContactPersonIdentification e1, ContactPersonIdentification e2) {
				if (e1 != null && e2 != null) {
					if (e1.getEmail() != null
							&& e2.getEmail() != null)
						return e1.getEmail().compareTo(
								e2.getEmail());
					return 0;
				} else
					return 0;
			}
		};
		columnSort.setComparator(getColumnConPerEmail, quote);
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnStateSorting() {
		List<ContactPersonIdentification> list = getDataprovider().getList();
		columnSort = new ListHandler<ContactPersonIdentification>(list);
		Comparator<ContactPersonIdentification> quote = new Comparator<ContactPersonIdentification>() {
			@Override
			public int compare(ContactPersonIdentification e1, ContactPersonIdentification e2) {
				if (e1 != null && e2 != null) {
					if (e1.getAddressInfo().getState() != null
							&& e2.getAddressInfo().getState() != null)
						return e1.getAddressInfo().getState().compareTo(
								e2.getAddressInfo().getState());
					return 0;
				} else
					return 0;
			}
		};
		columnSort.setComparator(getColumnConPerState, quote);
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnCitySorting() {
		List<ContactPersonIdentification> list = getDataprovider().getList();
		columnSort = new ListHandler<ContactPersonIdentification>(list);
		Comparator<ContactPersonIdentification> quote = new Comparator<ContactPersonIdentification>() {
			@Override
			public int compare(ContactPersonIdentification e1, ContactPersonIdentification e2) {
				if (e1 != null && e2 != null) {
					if (e1.getAddressInfo().getCity() != null
							&& e2.getAddressInfo().getCity() != null)
						return e1.getAddressInfo().getCity().compareTo(
								e2.getAddressInfo().getCity());
					return 0;
				} else
					return 0;
			}
		};
		columnSort.setComparator(getColumnConPerCity, quote);
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnLocalitySorting() {
		List<ContactPersonIdentification> list = getDataprovider().getList();
		columnSort = new ListHandler<ContactPersonIdentification>(list);
		Comparator<ContactPersonIdentification> quote = new Comparator<ContactPersonIdentification>() {
			@Override
			public int compare(ContactPersonIdentification e1, ContactPersonIdentification e2) {
				if (e1 != null && e2 != null) {
					if (e1.getAddressInfo().getLocality() != null
							&& e2.getAddressInfo().getLocality() != null)
						return e1.getAddressInfo().getLocality().compareTo(
								e2.getAddressInfo().getLocality());
					return 0;
				} else
					return 0;
			}
		};
		columnSort.setComparator(getColumnConPerLocality, quote);
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnLandlineSorting() {
		List<ContactPersonIdentification> list = getDataprovider().getList();
		columnSort = new ListHandler<ContactPersonIdentification>(list);
		Comparator<ContactPersonIdentification> quote = new Comparator<ContactPersonIdentification>() {
			@Override
			public int compare(ContactPersonIdentification e1, ContactPersonIdentification e2) {
				if (e1 != null && e2 != null) {
					if (e1.getLandline() != null
							&& e2.getLandline() != null)
						return e1.getLandline().compareTo(
								e2.getLandline());
					return 0;
				} else
					return 0;
			}
		};
		columnSort.setComparator(getColumnConPerEntype, quote);
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnEntityTypeSorting() {

		List<ContactPersonIdentification> list = getDataprovider().getList();
		columnSort = new ListHandler<ContactPersonIdentification>(list);
		Comparator<ContactPersonIdentification> quote = new Comparator<ContactPersonIdentification>() {
			@Override
			public int compare(ContactPersonIdentification e1, ContactPersonIdentification e2) {
				if (e1 != null && e2 != null) {
					if (e1.getEntype() != null
							&& e2.getEntype() != null)
						return e1.getEntype().compareTo(
								e2.getEntype());
					return 0;
				} else
					return 0;
			}
		};
		columnSort.setComparator(getColumnConPerEntype, quote);
		table.addColumnSortHandler(columnSort);
		
	}

//	private void addColumnModuleNameSorting() {
//
//		List<ContactPersonIdentification> list = getDataprovider().getList();
//		columnSort = new ListHandler<ContactPersonIdentification>(list);
//		Comparator<ContactPersonIdentification> quote = new Comparator<ContactPersonIdentification>() {
//			@Override
//			public int compare(ContactPersonIdentification e1, ContactPersonIdentification e2) {
//				if (e1 != null && e2 != null) {
//					if (e1.getModulename() != null
//							&& e2.getModulename() != null)
//						return e1.getModulename().compareTo(
//								e2.getModulename());
//					return 0;
//				} else
//					return 0;
//			}
//		};
//		columnSort.setComparator(getColumnConPerModuleName, quote);
//		table.addColumnSortHandler(columnSort);
//	}

	private void addColumnNameSorting() {
		
		List<ContactPersonIdentification> list = getDataprovider().getList();
		columnSort = new ListHandler<ContactPersonIdentification>(list);
		Comparator<ContactPersonIdentification> quote = new Comparator<ContactPersonIdentification>() {
			@Override
			public int compare(ContactPersonIdentification e1, ContactPersonIdentification e2) {
				if (e1 != null && e2 != null) {
					if (e1.getName() != null
							&& e2.getName() != null)
						return e1.getName().compareTo(
								e2.getName());
					return 0;
				} else
					return 0;
			}
		};
		columnSort.setComparator(getColumnConPerName, quote);
		table.addColumnSortHandler(columnSort);
	}

//	private void addColumnProcessNameSorting() {
//
//		List<ContactPersonIdentification> list = getDataprovider().getList();
//		columnSort = new ListHandler<ContactPersonIdentification>(list);
//		Comparator<ContactPersonIdentification> quote = new Comparator<ContactPersonIdentification>() {
//			@Override
//			public int compare(ContactPersonIdentification e1, ContactPersonIdentification e2) {
//				if (e1 != null && e2 != null) {
//					if (e1.getProcessName() != null
//							&& e2.getProcessName() != null)
//						return e1.getProcessName().compareTo(
//								e2.getProcessName());
//					return 0;
//				} else
//					return 0;
//			}
//		};
//		columnSort.setComparator(getColumnConPerProcessName, quote);
//		table.addColumnSortHandler(columnSort);
//	}

	private void addColumnRoleSorting() {

		List<ContactPersonIdentification> list = getDataprovider().getList();
		columnSort = new ListHandler<ContactPersonIdentification>(list);
		Comparator<ContactPersonIdentification> quote = new Comparator<ContactPersonIdentification>() {
			@Override
			public int compare(ContactPersonIdentification e1, ContactPersonIdentification e2) {
				if (e1 != null && e2 != null) {
					if (e1.getRole() != null
							&& e2.getRole() != null)
						return e1.getRole().compareTo(
								e2.getRole());
					return 0;
				} else
					return 0;
			}
		};
		columnSort.setComparator(getColumnConPerRole, quote);
		table.addColumnSortHandler(columnSort);
	}
	
/** date 17/10/2017 added by komal to add poc name in contact person table **/
	
	private void createColumnConPerPOCName() {

		getColumnConPerPOCName=new TextColumn<ContactPersonIdentification>() {
			
			@Override
			public String getValue(ContactPersonIdentification object) {
				return object.getPocName()+"";
				
			}
		};
		table.addColumn(getColumnConPerPOCName, "POC Name");
		getColumnConPerPOCName.setSortable(true);
	}
	
	private void addColumnPOCNameSorting() {

		List<ContactPersonIdentification> list = getDataprovider().getList();
		columnSort = new ListHandler<ContactPersonIdentification>(list);
		Comparator<ContactPersonIdentification> quote = new Comparator<ContactPersonIdentification>() {
			@Override
			public int compare(ContactPersonIdentification e1, ContactPersonIdentification e2) {
				if (e1 != null && e2 != null) {
					if (e1.getPocName() != null
							&& e2.getPocName() != null)
						return e1.getPocName().compareTo(
								e2.getPocName());
					return 0;
				} else
					return 0;
			}
		};
		columnSort.setComparator(getColumnConPerPOCName, quote);
		table.addColumnSortHandler(columnSort);
	}
	
	/** date 17/10/2017 added by komal to add Business person name in contact person table **/
	
	private void createColumnConPerBPName(){
		
       getColumnConPerBPName=new TextColumn<ContactPersonIdentification>() {
 			
			@Override
			public String getValue(ContactPersonIdentification object) {
				
				return object.getBpName();
				
			}
		};
		table.addColumn(getColumnConPerBPName, "BP Name");
		getColumnConPerBPName.setSortable(true);
	}
	
	private void addColumnBPNameSorting() {

		List<ContactPersonIdentification> list = getDataprovider().getList();
		columnSort = new ListHandler<ContactPersonIdentification>(list);
		Comparator<ContactPersonIdentification> quote = new Comparator<ContactPersonIdentification>() {
			@Override
			public int compare(ContactPersonIdentification e1, ContactPersonIdentification e2) {
				if (e1 != null && e2 != null) {
					
					if (e1.getBpName() != null
							&& e2.getBpName() != null)
						return e1.getBpName().compareTo(
								e2.getBpName());
					return 0;
				} else
					return 0;
			}
		};
		columnSort.setComparator(getColumnConPerBPName, quote);
		table.addColumnSortHandler(columnSort);
	}
	
	/**********************************************************************************************************/
	
	
	
	@Override
	protected void initializekeyprovider() {
	}

	@Override
	public void addFieldUpdater() {
	}

	@Override
	public void setEnable(boolean state) {
	}

	@Override
	public void applyStyle() {
	}
	
	
}
