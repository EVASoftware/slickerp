package com.slicktechnologies.client.views.technicianwarehousedetails;

import java.util.ArrayList;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.ListBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.technicianwarehousedetails.TechnicianWareHouseDetailsPresenter.TechnicianWareHouseDetailsSearch;
import com.slicktechnologies.shared.TechnicianWareHouseDetailsList;
import com.slicktechnologies.shared.common.businessunitlayer.Company;

import com.slicktechnologies.shared.common.inventory.StorageLocation;
import com.slicktechnologies.shared.common.inventory.Storagebin;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.personlayer.Employee;

public class TechnicianWareHouseDetailsSearchProxy extends TechnicianWareHouseDetailsSearch implements ChangeHandler{

	
	ObjectListBox<WareHouse> oblwarehouse;
	ListBox lsStorageLoc;
	ListBox lsStoragebin;
	ObjectListBox<WareHouse> parentOblwarehouse;
	ListBox parentLsStorageLoc;
	ListBox parentLstoragebin;
	
	final GenricServiceAsync async = GWT.create(GenricService.class);
	ObjectListBox<Employee> olbTechnicainName;
	
	public TechnicianWareHouseDetailsSearchProxy() {
		super();
		createGui();
	}
	
private void initalizeWidget() {
		
		parentOblwarehouse = new ObjectListBox<WareHouse>();
		parentOblwarehouse.addChangeHandler(this);	
		parentLsStorageLoc=new ListBox();
		parentLsStorageLoc.addChangeHandler(this);
		parentLsStorageLoc.addItem("--SELECT--");
		parentLstoragebin=new ListBox();
		parentLstoragebin.addItem("--SELECT--");
		
		oblwarehouse = new ObjectListBox<WareHouse>();
		oblwarehouse.addChangeHandler(this);	
		lsStorageLoc=new ListBox();
		lsStorageLoc.addChangeHandler(this);
		lsStorageLoc.addItem("--SELECT--");
		lsStoragebin=new ListBox();
		lsStoragebin.addItem("--SELECT--");
		
		AppUtility.initializeWarehouse(parentOblwarehouse);
		AppUtility.initializeWarehouse(oblwarehouse);
		
		olbTechnicainName = new ObjectListBox<Employee>();
		olbTechnicainName.makeEmployeeLive(AppConstants.SERVICEMODULE, AppConstants.CONTRACT, "Technician");

	}


	@Override
	public void createScreen() {
		initalizeWidget();
		
		FormFieldBuilder fbuilder;

		fbuilder = new FormFieldBuilder();
		FormField fgroupingTechnicianWareHouseInfo = fbuilder.setlabel("Techician WareHouse Selection").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();

		fbuilder = new FormFieldBuilder();
		FormField fgroupingParentWareHouse = fbuilder.setlabel("Parent Warehouse Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();

		fbuilder = new FormFieldBuilder("Warehouse", parentOblwarehouse);
		FormField fparentwarehouse = fbuilder.setMandatory(false).setMandatoryMsg("WareHouse Name Is Mandatory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Storage Location", parentLsStorageLoc);
		FormField fparentlocation = fbuilder.setMandatory(false).setMandatoryMsg("Storage Location Is Mandatory").setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Storage Bin", parentLstoragebin);
		FormField fparentbin = fbuilder.setMandatory(false).setMandatoryMsg("Storage Bin Is Mandatory").setRowSpan(0).setColSpan(0).build();
				
		fbuilder = new FormFieldBuilder("Warehouse", oblwarehouse);
		FormField fwarehouse = fbuilder.setMandatory(false).setMandatoryMsg("WareHouse Name Is Mandatory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Storage Location", lsStorageLoc);
		FormField flocation = fbuilder.setMandatory(false).setMandatoryMsg("Storage Location Is Mandatory").setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Storage Bin", lsStoragebin);
		FormField fbin = fbuilder.setMandatory(false).setMandatoryMsg("Storage Bin Is Mandatory").setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("Technician Name", olbTechnicainName);
		FormField folbTechnicainName = fbuilder.setMandatory(false).setMandatoryMsg("Technician Name Is Mandatory").setRowSpan(0).setColSpan(0).build();

		
		FormField[][] formfield = { 
				{ fgroupingParentWareHouse }, 
				{ fparentwarehouse , fparentlocation , fparentbin },
			//	{ fgroupingTechnicianWareHouseInfo},
			//	{ fwarehouse, flocation, fbin,folbTechnicainName}		
				
		};
		this.fields = formfield;
	}
	@Override
	public void onChange(ChangeEvent event) {
		// TODO Auto-generated method stub

		if(event.getSource().equals(oblwarehouse)){
			System.out.println("Inside Change Event.......");
				retriveStorageLocation(lsStorageLoc ,oblwarehouse);
		}
		if(event.getSource().equals(lsStorageLoc)){
				System.out.println("Inside Change Event.......");
				retriveStorageBin(lsStoragebin , lsStorageLoc);
		}
		if(event.getSource().equals(parentOblwarehouse)){
			System.out.println("Inside Change Event.......");
				retriveStorageLocation(parentLsStorageLoc , parentOblwarehouse);
		}
		if(event.getSource().equals(parentLsStorageLoc)){
				System.out.println("Inside Change Event.......");
				retriveStorageBin(parentLstoragebin , parentLsStorageLoc);
		}
	
	}

	@Override
	public MyQuerry getQuerry() {
		// TODO Auto-generated method stub
		Console.log("HEloo");
		MyQuerry query = new MyQuerry();
		Company c=new Company();
		System.out.println("Company Id :: "+c.getCompanyId());
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		if(parentOblwarehouse.getSelectedIndex() != 0){
			Console.log("HEloo");
		filter = new Filter();
		filter.setQuerryString("wareHouseList.parentWareHouse");
		filter.setStringValue(parentOblwarehouse.getValue());
		filtervec.add(filter);
		}
		
		if(parentLsStorageLoc.getSelectedIndex() != 0){
			Console.log("HEloo");
		filter = new Filter();
		filter.setQuerryString("wareHouseList.parentStorageLocation");
		filter.setStringValue(parentLsStorageLoc.getValue(parentLsStorageLoc.getSelectedIndex()));
		filtervec.add(filter);
		}
		
		if(parentLstoragebin.getSelectedIndex() != 0){
			Console.log("HEloo");
		filter = new Filter();
		filter.setQuerryString("wareHouseList.pearentStorageBin");
		filter.setStringValue(parentLstoragebin.getValue(parentLstoragebin.getSelectedIndex()));
		filtervec.add(filter);
		}
		
//		if(oblwarehouse.getSelectedIndex() != 0){
//		filter = new Filter();
//		filter.setQuerryString("wareHouseList.technicianWareHouse");
//		filter.setStringValue(oblwarehouse.getValue());
//		filtervec.add(filter);
//		}
//		
//		if(lsStorageLoc.getSelectedIndex() != 0){
//		filter = new Filter();
//		filter.setQuerryString("wareHouseList.technicianStorageLocation");
//		filter.setStringValue(lsStorageLoc.getValue(lsStorageLoc.getSelectedIndex()));
//		filtervec.add(filter);
//		}
//		
//		if(lsStoragebin.getSelectedIndex() != 0){
//		filter = new Filter();
//		filter.setQuerryString("wareHouseList.technicianStorageBin");
//		filter.setStringValue(lsStoragebin.getValue(lsStoragebin.getSelectedIndex()));
//		filtervec.add(filter);
//		}
//		
//		if(olbTechnicainName.getSelectedIndex() != 0){
//		filter=new Filter();
//		filter.setQuerryString("wareHouseList.technicianName");
//		filter.setStringValue(olbTechnicainName.getValue());
//		filtervec.add(filter);
//		}
		
		query.setFilters(filtervec);
		query.setQuerryObject(new TechnicianWareHouseDetailsList());
		Console.log("HEloo");
		return query;
	}

	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
		return true;
	}
	private void retriveStorageLocation(final ListBox lsStorageLoc , ObjectListBox oblwarehouse){
		MyQuerry query = new MyQuerry();
		Company c=new Company();
		System.out.println("Company Id :: "+c.getCompanyId());
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("warehouseName");
		filter.setStringValue(oblwarehouse.getValue());
		filtervec.add(filter);
		
		filter=new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);
		
		query.setFilters(filtervec);
		query.setQuerryObject(new StorageLocation());
		
		async.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				lsStorageLoc.clear();
				lsStoragebin.clear();
				lsStorageLoc.addItem("--SELECT--");
				lsStoragebin.addItem("--SELECT--");
				for (SuperModel model : result) {
					StorageLocation entity = (StorageLocation) model;
					lsStorageLoc.addItem(entity.getBusinessUnitName());
					System.out.println("Location :"+entity.getBusinessUnitName());
				}
				
			}
		});
	}
	
	// ****************************** Retrieving Storage Bin From Storage location **********************************
	
	private void retriveStorageBin(final ListBox lsStoragebin , ListBox lsStorageLoc){
		MyQuerry query = new MyQuerry();
		Company c=new Company();
		System.out.println("Company Id :: "+c.getCompanyId());
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("storagelocation");
		filter.setStringValue(lsStorageLoc.getValue(lsStorageLoc.getSelectedIndex()));
		filtervec.add(filter);
		
		filter=new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);
		
		query.setFilters(filtervec);
		query.setQuerryObject(new Storagebin());
		
		async.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				lsStoragebin.clear();
				lsStoragebin.addItem("--SELECT--");
				for (SuperModel model : result) {
					Storagebin entity = (Storagebin) model;
					lsStoragebin.addItem(entity.getBinName());
					System.out.println("BIN :"+entity.getBinName());
				}
			}
		});
	}

}
