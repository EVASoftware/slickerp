package com.slicktechnologies.client.views.technicianwarehousedetails;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.views.technicianwarehousedetails.TechnicianWareHouseDetailsPresenter.TechnicianWareHouseTable;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.TechnicianWarehouseDetails;

public class TechnicianWareHouseDetailsTable extends SuperTable<TechnicianWarehouseDetails>{

	Column<TechnicianWarehouseDetails,String> deleteColumn;
	TextColumn<TechnicianWarehouseDetails> getColumnParentWarehouse , getColumnParentStorageLocation , getColumnParentStorageBin;
	TextColumn<TechnicianWarehouseDetails> getColumnTechnicianWarehouse , getColumnTechnicianStorageLocation , getColumnTechnicianStorageBin;
	TextColumn<TechnicianWarehouseDetails> getColumnTechnicianName;
	
	@Override
	public void createTable() {
		// TODO Auto-generated method stub
		createColumnParentWarehouse();
		createColumnParentStorageLocation();
		createColumnParentStorageBin();
		createColumnTechnicianName();
		createColumnTechnicianWarehouse();
		createColumnTechnicianStorageLocation();
		createColumnTechnicianStorageBin();
		createColumndeleteColumn();
		addFieldUpdater();
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		createFieldUpdaterdeleteColumn();
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
	
	protected void createColumndeleteColumn()
	{
		ButtonCell btnCell= new ButtonCell();
		deleteColumn=new Column<TechnicianWarehouseDetails,String>(btnCell)
				{
			@Override
			public String getValue(TechnicianWarehouseDetails object)
			{
				return  "Delete" ;
			}
				};
				table.addColumn(deleteColumn,"Delete");
	}

	protected void createFieldUpdaterdeleteColumn()
	{
		deleteColumn.setFieldUpdater(new FieldUpdater<TechnicianWarehouseDetails,String>()
				{
			@Override
			public void update(int index,TechnicianWarehouseDetails object,String value)
			{
				getDataprovider().getList().remove(object);

				table.redrawRow(index);
			}
				});
	}
	
	protected void createColumnParentWarehouse()
	{
		getColumnParentWarehouse=new TextColumn<TechnicianWarehouseDetails>()
				{
			@Override public String getValue(TechnicianWarehouseDetails object){
				if(object.getParentWareHouse()!=null)
					return object.getParentWareHouse();
				else
					return "";
			}
		};
				table.addColumn(getColumnParentWarehouse,"Parent Warehouse");
	}
	
	protected void createColumnParentStorageBin()
	{
		getColumnParentStorageBin=new TextColumn<TechnicianWarehouseDetails>()
				{
			@Override public String getValue(TechnicianWarehouseDetails object){
				if(object.getPearentStorageBin()!=null)
					return object.getPearentStorageBin();
				else
					return "";
			}
		};
				table.addColumn(getColumnParentStorageBin,"Parent Storage Bin");
	}
	protected void createColumnParentStorageLocation()
	{
		getColumnParentStorageLocation=new TextColumn<TechnicianWarehouseDetails>()
				{
			@Override public String getValue(TechnicianWarehouseDetails object){
				if(object.getParentStorageLocation()!=null)
					return object.getParentStorageLocation();
				else
					return "";
			}
		};
				table.addColumn(getColumnParentStorageLocation,"Parent Storage Location");
	}
	protected void createColumnTechnicianWarehouse()
	{
		getColumnTechnicianName=new TextColumn<TechnicianWarehouseDetails>()
				{
			@Override public String getValue(TechnicianWarehouseDetails object){
				if(object.getTechnicianWareHouse()!=null)
					return object.getTechnicianWareHouse();
				else
					return "";
				
			}
		};
				table.addColumn(getColumnTechnicianName,"Technician Warehouse");
	}
	
	protected void createColumnTechnicianStorageBin()
	{
		getColumnTechnicianStorageBin=new TextColumn<TechnicianWarehouseDetails>()
				{
			@Override public String getValue(TechnicianWarehouseDetails object){
				if(object.getTechnicianStorageBin()!=null)
					return object.getTechnicianStorageBin();
				else
					return "";
				
			}
		};
				table.addColumn(getColumnTechnicianStorageBin,"Technician Storage Bin");
	}
	protected void createColumnTechnicianStorageLocation()
	{
		getColumnTechnicianStorageLocation=new TextColumn<TechnicianWarehouseDetails>()
				{
			@Override public String getValue(TechnicianWarehouseDetails object){
				
				if(object.getTechnicianStorageLocation()!=null)
					return object.getTechnicianStorageLocation();
				else
					return "";
				
			}
		};
				table.addColumn(getColumnTechnicianStorageLocation,"Technician Storage Location");
	}
	
	protected void createColumnTechnicianName()
	{
		getColumnTechnicianName=new TextColumn<TechnicianWarehouseDetails>()
				{
			@Override public String getValue(TechnicianWarehouseDetails object){
				if(object.getTechnicianName()!=null)
					return object.getTechnicianName();
				else
					return "";
			}
		};
				table.addColumn(getColumnTechnicianName,"Technician Name");
	}
}
