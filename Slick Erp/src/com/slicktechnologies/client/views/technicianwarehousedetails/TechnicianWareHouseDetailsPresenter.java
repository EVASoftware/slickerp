package com.slicktechnologies.client.views.technicianwarehousedetails;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.lead.LeadForm;
import com.slicktechnologies.client.views.lead.LeadPresenterSearchProxy;
import com.slicktechnologies.client.views.lead.LeadPresenterTableProxy;
import com.slicktechnologies.client.views.lead.LeadPresenter.LeadPresenterSearch;
import com.slicktechnologies.client.views.lead.LeadPresenter.LeadPresenterTable;
import com.slicktechnologies.shared.TechnicianWareHouseDetailsList;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class TechnicianWareHouseDetailsPresenter extends FormScreenPresenter<TechnicianWareHouseDetailsList>{

	TechnicianWareHouseDetailsForm form ;
	
	public TechnicianWareHouseDetailsPresenter(
			UiScreen<TechnicianWareHouseDetailsList> view,
			TechnicianWareHouseDetailsList model) {
		super(view, model);
		form=(TechnicianWareHouseDetailsForm) view;
		
		form.setPresenter(this);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.TECHNICIANWAREHOUSEDETAILS,AppConstants.INVENTORYMODULE);
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}
	
	public static TechnicianWareHouseDetailsForm initialize() {
		
//		
//		ProductInventoryViewSearchProxy.staticSuperTable=gentableScreen;
//		ProductInventoryViewSearchProxy searchpopup=new ProductInventoryViewSearchProxy();
//		form.setSearchpopupscreen(searchpopup);
		TechnicianWareHouseDetailsForm form = new TechnicianWareHouseDetailsForm();
		TechnicianWareHouseTable gentable=new TechnicianWarehouseDetailsListTableProxy();
		gentable.setView(form);
		gentable.applySelectionModle();
		TechnicianWareHouseDetailsSearch.staticSuperTable=gentable;
		TechnicianWareHouseDetailsSearch searchpopup=new TechnicianWareHouseDetailsSearchProxy();
		form.setSearchpopupscreen(searchpopup);
		
		TechnicianWareHouseDetailsPresenter presenter = new TechnicianWareHouseDetailsPresenter(form, new TechnicianWareHouseDetailsList());
		AppMemory.getAppMemory().stickPnel(form);
		return form;

	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		// TODO Auto-generated method stub
		InlineLabel label = (InlineLabel) e.getSource();
		String text = label.getText().trim();
		if (text.equals(AppConstants.NEW))
			reactToNew();
	}

	private void reactToNew() {
		form.setToNewState();
		initialize();
	}

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void makeNewModel() {
		// TODO Auto-generated method stub
		model = new TechnicianWareHouseDetailsList();
	}
	
	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.TechnicianWareHouseDetailsList")
	public static  class TechnicianWareHouseDetailsSearch extends SearchPopUpScreen<TechnicianWareHouseDetailsList>{

		@Override
		public MyQuerry getQuerry() {
			return null;
		}

		@Override
		public boolean validate() {
			// TODO Auto-generated method stub
			return true;
		}};

		@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.TechnicianWareHouseDetailsList")
		public static class TechnicianWareHouseTable extends SuperTable<TechnicianWareHouseDetailsList> implements GeneratedVariableRefrence{

			@Override
			public Object getVarRef(String varName) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void createTable() {
				// TODO Auto-generated method stub

			}

			@Override
			protected void initializekeyprovider() {
				// TODO Auto-generated method stub

			}

			@Override
			public void addFieldUpdater() {
				// TODO Auto-generated method stub

			}

			@Override
			public void setEnable(boolean state) {

			}

			@Override
			public void applyStyle() {

			}} ;

		
}
