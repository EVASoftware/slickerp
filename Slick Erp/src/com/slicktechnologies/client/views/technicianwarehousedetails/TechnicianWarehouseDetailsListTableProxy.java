package com.slicktechnologies.client.views.technicianwarehousedetails;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.views.technicianwarehousedetails.TechnicianWareHouseDetailsPresenter.TechnicianWareHouseTable;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.TechnicianWareHouseDetailsList;

public class TechnicianWarehouseDetailsListTableProxy extends TechnicianWareHouseTable{

	Column<TechnicianWareHouseDetailsList,String> deleteColumn;
	TextColumn<TechnicianWareHouseDetailsList> getColumnParentWarehouse , getColumnParentStorageLocation , getColumnParentStorageBin;
	TextColumn<TechnicianWareHouseDetailsList> getColumnTechnicianWarehouse , getColumnTechnicianStorageLocation , getColumnTechnicianStorageBin;
	TextColumn<TechnicianWareHouseDetailsList> getColumnTechnicianName;
	
	@Override
	public void createTable() {
		// TODO Auto-generated method stub
		createColumnParentWarehouse();
		createColumnParentStorageLocation();
		createColumnParentStorageBin();
		createColumnTechnicianName();
		createColumnTechnicianWarehouse();
		createColumnTechnicianStorageLocation();
		createColumnTechnicianStorageBin();
//		createColumndeleteColumn();
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
	//	createFieldUpdaterdeleteColumn();
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
	
	
	
	protected void createColumnParentWarehouse()
	{
		getColumnParentWarehouse=new TextColumn<TechnicianWareHouseDetailsList>()
				{
			@Override public String getValue(TechnicianWareHouseDetailsList object){
				if(object.getWareHouseList().get(0).getParentWareHouse()!=null)
					return object.getWareHouseList().get(0).getParentWareHouse();
				else
					return "";
			}
		};
				table.addColumn(getColumnParentWarehouse,"Parent Warehouse");
	}
	
	protected void createColumnParentStorageBin()
	{
		getColumnParentStorageBin=new TextColumn<TechnicianWareHouseDetailsList>()
				{
			@Override public String getValue(TechnicianWareHouseDetailsList object){
				if(object.getWareHouseList().get(0).getPearentStorageBin()!=null)
					return object.getWareHouseList().get(0).getPearentStorageBin();
				else
					return "";
			}
		};
				table.addColumn(getColumnParentStorageBin,"Parent Storage Bin");
	}
	protected void createColumnParentStorageLocation()
	{
		getColumnParentStorageLocation=new TextColumn<TechnicianWareHouseDetailsList>()
				{
			@Override public String getValue(TechnicianWareHouseDetailsList object){
				if(object.getWareHouseList().get(0).getParentStorageLocation()!=null)
					return object.getWareHouseList().get(0).getParentStorageLocation();
				else
					return "";
			}
		};
				table.addColumn(getColumnParentStorageLocation,"Parent Storage Location");
	}
	protected void createColumnTechnicianWarehouse()
	{
		getColumnTechnicianName=new TextColumn<TechnicianWareHouseDetailsList>()
				{
			@Override public String getValue(TechnicianWareHouseDetailsList object){
				if(object.getWareHouseList().get(0).getTechnicianWareHouse()!=null)
					return object.getWareHouseList().get(0).getTechnicianWareHouse();
				else
					return "";
			}
		};
				table.addColumn(getColumnTechnicianName,"Technician Warehouse");
	}
	
	protected void createColumnTechnicianStorageBin()
	{
		getColumnTechnicianStorageBin=new TextColumn<TechnicianWareHouseDetailsList>()
				{
			@Override public String getValue(TechnicianWareHouseDetailsList object){
				if(object.getWareHouseList().get(0).getTechnicianStorageBin()!=null)
					return object.getWareHouseList().get(0).getTechnicianStorageBin();
				else
					return "";
			}
		};
				table.addColumn(getColumnTechnicianStorageBin,"Technician Storage Bin");
	}
	protected void createColumnTechnicianStorageLocation()
	{
		getColumnTechnicianStorageLocation=new TextColumn<TechnicianWareHouseDetailsList>()
				{
			@Override public String getValue(TechnicianWareHouseDetailsList object){
				if(object.getWareHouseList().get(0).getTechnicianStorageLocation()!=null)
					return object.getWareHouseList().get(0).getTechnicianStorageLocation();
				else
					return "";
			}
		};
				table.addColumn(getColumnTechnicianStorageLocation,"Technician Storage Location");
	}
	
	protected void createColumnTechnicianName()
	{
		getColumnTechnicianName=new TextColumn<TechnicianWareHouseDetailsList>()
				{
			@Override public String getValue(TechnicianWareHouseDetailsList object){
				if(object.getWareHouseList().get(0).getTechnicianName()!=null)
					return object.getWareHouseList().get(0).getTechnicianName();
				else
					return "";
			}
		};
				table.addColumn(getColumnTechnicianName,"Technician Name");
	}
}
