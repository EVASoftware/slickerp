package com.slicktechnologies.client.views.technicianwarehousedetails;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextArea;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.inventory.productinventoryview.InvetoryViewDetailsTable;
import com.slicktechnologies.client.views.settings.employee.EmployeeBranch;
import com.slicktechnologies.shared.TechnicianWareHouseDetailsList;
import com.slicktechnologies.shared.TechnicianWarehouseDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.inventory.StorageLocation;
import com.slicktechnologies.shared.common.inventory.Storagebin;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.shippingpackingprocess.InspectionMethod;

public class TechnicianWareHouseDetailsForm extends FormScreen<TechnicianWareHouseDetailsList> implements ChangeHandler, ClickHandler{

	ObjectListBox<WareHouse> oblwarehouse;
	ListBox lsStorageLoc;
	ListBox lsStoragebin;
	ObjectListBox<WareHouse> parentOblwarehouse;
	ListBox parentLsStorageLoc;
	ListBox parentLstoragebin;
	Button addBtn;
	TechnicianWareHouseDetailsTable technicianWareHouseDetailsTable;
	ArrayList<String> storageList=new ArrayList<String>();
	ArrayList<WareHouse> wareHouseList;
	TechnicianWareHouseDetailsList technicianWareHouseDetailsList;
	GenricServiceAsync async = GWT.create(GenricService.class);
	ObjectListBox<Employee> olbTechnicainName;
	
	public TechnicianWareHouseDetailsForm  (String[] processlevel, FormField[][] fields,FormStyle formstyle) 
	{
		super(processlevel, fields, formstyle);
		createGui();
	}

	public TechnicianWareHouseDetailsForm() {
		super();
		createGui();
		technicianWareHouseDetailsTable.connectToLocal();
	}

	
	private void initalizeWidget() {
		
		parentOblwarehouse = new ObjectListBox<WareHouse>();
		parentOblwarehouse.addChangeHandler(this);	
		parentLsStorageLoc=new ListBox();
		parentLsStorageLoc.addChangeHandler(this);
		parentLsStorageLoc.addItem("--SELECT--");
		parentLstoragebin=new ListBox();
		parentLstoragebin.addItem("--SELECT--");
		parentLstoragebin.addChangeHandler(this);
		addBtn = new Button("Add");
		addBtn.addClickHandler(this);
		technicianWareHouseDetailsTable = new TechnicianWareHouseDetailsTable();
		
		oblwarehouse = new ObjectListBox<WareHouse>();
		oblwarehouse.addChangeHandler(this);	
		
		lsStorageLoc=new ListBox();
		lsStorageLoc.addItem("--SELECT--");
		lsStorageLoc.addChangeHandler(this);
		lsStoragebin=new ListBox();
		lsStoragebin.addItem("--SELECT--");
		
		AppUtility.initializeWarehouse(parentOblwarehouse);
		AppUtility.initializeWarehouse(oblwarehouse);
		
		olbTechnicainName = new ObjectListBox<Employee>();
		olbTechnicainName.makeEmployeeLive(AppConstants.SERVICEMODULE, AppConstants.CONTRACT, "Technician");

	}


	@Override
	public void createScreen() {
		initalizeWidget();
		this.processlevelBarNames = new String[] { "New" };

		FormFieldBuilder fbuilder;

		fbuilder = new FormFieldBuilder();
		FormField fgroupingTechnicianWareHouseInfo = fbuilder.setlabel("Techician WareHouse Selection").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();

		fbuilder = new FormFieldBuilder();
		FormField fgroupingParentWareHouse = fbuilder.setlabel("Parent Warehouse Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();

		fbuilder = new FormFieldBuilder("Warehouse", parentOblwarehouse);
		FormField fparentwarehouse = fbuilder.setMandatory(false).setMandatoryMsg("WareHouse Name Is Mandatory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Storage Location", parentLsStorageLoc);
		FormField fparentlocation = fbuilder.setMandatory(false).setMandatoryMsg("Storage Location Is Mandatory").setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Storage Bin", parentLstoragebin);
		FormField fparentbin = fbuilder.setMandatory(false).setMandatoryMsg("Storage Bin Is Mandatory").setRowSpan(0).setColSpan(0).build();
				
		fbuilder = new FormFieldBuilder("Warehouse", oblwarehouse);
		FormField fwarehouse = fbuilder.setMandatory(false).setMandatoryMsg("WareHouse Name Is Mandatory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Storage Location", lsStorageLoc);
		FormField flocation = fbuilder.setMandatory(false).setMandatoryMsg("Storage Location Is Mandatory").setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Storage Bin", lsStoragebin);
		FormField fbin = fbuilder.setMandatory(false).setMandatoryMsg("Storage Bin Is Mandatory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("", addBtn);
		FormField fadd = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("", technicianWareHouseDetailsTable.getTable());
		FormField ftable = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(5).build();
		
		fbuilder = new FormFieldBuilder("Technician Name", olbTechnicainName);
		FormField folbTechnicainName = fbuilder.setMandatory(false).setMandatoryMsg("Technician Name Is Mandatory").setRowSpan(0).setColSpan(0).build();

		
		FormField[][] formfield = { 
				{ fgroupingParentWareHouse }, 
				{ fparentwarehouse , fparentlocation , fparentbin },
				{ fgroupingTechnicianWareHouseInfo},
				{ fwarehouse, flocation, fbin,folbTechnicainName , fadd },			
				{ ftable }
		};
		this.fields = formfield;
	}

	@Override
	public void updateModel(TechnicianWareHouseDetailsList model) {
		// TODO Auto-generated method stub
		if(parentOblwarehouse.getSelectedIndex() != 0){
			model.setParentWareHouse(parentOblwarehouse.getValue(parentOblwarehouse.getSelectedIndex()));
		}
		if(parentLsStorageLoc.getSelectedIndex() != 0){
			model.setParentStorageLocation(parentLsStorageLoc.getValue(parentLsStorageLoc.getSelectedIndex()));
		}
		if(parentLstoragebin.getSelectedIndex() != 0){
			model.setPearentStorageBin(parentLstoragebin.getValue(parentLstoragebin.getSelectedIndex()));
		}
		if(technicianWareHouseDetailsTable.getDataprovider().getList().size() >0){
			model.setWareHouseList(technicianWareHouseDetailsTable.getDataprovider().getList());
		}
		technicianWareHouseDetailsList=model;
		presenter.setModel(model);
	}

	@Override
	public void updateView(TechnicianWareHouseDetailsList view) {
		// TODO Auto-generated method stub

		
		technicianWareHouseDetailsList=view;
		
		if(view.getParentWareHouse()!=null){
			parentOblwarehouse.setValue(view.getParentWareHouse());
		}
		if(view.getParentStorageLocation() != null){
			for(int i=0;i<parentLsStorageLoc.getItemCount();i++){
				String data=parentLsStorageLoc.getItemText(i);
				if(data.equals(view.getParentStorageLocation()))
				{
					parentLsStorageLoc.setSelectedIndex(i);
					break;
				}
			}
		}
		
		if(view.getPearentStorageBin() != null){
			for(int i=0;i<parentLstoragebin.getItemCount();i++){
				String data=parentLstoragebin.getItemText(i);
				if(data.equals(view.getPearentStorageBin()))
				{
					parentLstoragebin.setSelectedIndex(i);
					break;
				}
			}
		}
		if(view.getWareHouseList() !=null && view.getWareHouseList().size()>0){
			technicianWareHouseDetailsTable.getDataprovider().setList(view.getWareHouseList());
		}
		presenter.setModel(view);
	
	}
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		if (event.getSource().equals(addBtn)) {
			if (parentOblwarehouse.getSelectedIndex() == 0) {
				showDialogMessage("Please select parent warehouse.");
			} else if (parentLsStorageLoc.getSelectedIndex() == 0) {
				showDialogMessage("Please select parent storage location.");
			} else if (parentLstoragebin.getSelectedIndex() == 0) {
				showDialogMessage("Please select parent storage bin.");
			} else if (oblwarehouse.getSelectedIndex() == 0) {
				showDialogMessage("Please select technician warehouse.");
			} else if (lsStorageLoc.getSelectedIndex() == 0) {
				showDialogMessage("Please select technician storage location.");
			} else if (lsStoragebin.getSelectedIndex() == 0) {
				showDialogMessage("Please select technician storage bin.");
			} else if (olbTechnicainName.getSelectedIndex() == 0) {
				showDialogMessage("Please select technician name.");
			} else {
				if(validateTechnician()){
				 
				TechnicianWarehouseDetails technicianWarehouseDetails = new TechnicianWarehouseDetails();
				technicianWarehouseDetails
						.setParentWareHouse(parentOblwarehouse.getValue());
				technicianWarehouseDetails
						.setParentStorageLocation(parentLsStorageLoc
								.getValue(parentLsStorageLoc.getSelectedIndex()));
				technicianWarehouseDetails
						.setPearentStorageBin(parentLstoragebin
								.getValue(parentLstoragebin.getSelectedIndex()));
				technicianWarehouseDetails.setTechnicianWareHouse(oblwarehouse
						.getValue());
				technicianWarehouseDetails
						.setTechnicianStorageLocation(lsStorageLoc
								.getValue(lsStorageLoc.getSelectedIndex()));
				technicianWarehouseDetails.setTechnicianStorageBin(lsStoragebin
						.getValue(lsStoragebin.getSelectedIndex()));
				technicianWarehouseDetails.setTechnicianName(olbTechnicainName
						.getValue());
				
				technicianWareHouseDetailsTable.getDataprovider().getList()
						.add(technicianWarehouseDetails);
				}else{
					showDialogMessage("Technician has already asigned bin.");
				}
					
			}
		}
				
	}
	
	@Override
	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.equals("Save")||text.equals("Discard")||text.equals("Search")||text.equals(AppConstants.NAVIGATION))
					{
						menus[k].setVisible(true); 
					}
					else
						menus[k].setVisible(false);  		   
				}
			}
	
			else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
						menus[k].setVisible(true); 
	
					else
						menus[k].setVisible(false);  		   
				}
			}
	
			else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.equals("Discard")||text.equals("Edit")||text.equals("Search")||text.equals(AppConstants.NAVIGATION))
						menus[k].setVisible(true); 
	
					else
						menus[k].setVisible(false);  		   
				}
			}
			
			AuthorizationHelper.setAsPerAuthorization(Screen.TECHNICIANWAREHOUSEDETAILS,LoginPresenter.currentModule.trim());
	}
public void initializeWarehounse(final ObjectListBox<WareHouse> oblwarehouse2) {
		
		GenricServiceAsync async = GWT.create(GenricService.class);
		MyQuerry querry = new MyQuerry();
		Company c = new Company();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("processName");
		filter.setStringValue("Branch");
		filtervec.add(filter);

		filter = new Filter();
		filter.setQuerryString("processList.status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);

		querry.setFilters(filtervec);
		querry.setQuerryObject(new ProcessConfiguration());

		async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {

			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				List<ProcessTypeDetails> processList = new ArrayList<ProcessTypeDetails>();
				if (result.size() == 0) {
					MyQuerry querry = new MyQuerry(new Vector<Filter>(), new WareHouse());
					oblwarehouse2.MakeLive(querry);
				} else {
					for (SuperModel model : result) {
						ProcessConfiguration processConfig = (ProcessConfiguration) model;
						processList.addAll(processConfig.getProcessList());
					}

					for (int i = 0; i < processList.size(); i++) {
						if (processList.get(i).getProcessType().trim().equalsIgnoreCase("BranchLevelRestriction")
								&& processList.get(i).isStatus() == true) {
							
							System.out.println("user role "+LoginPresenter.myUserEntity.getRole().getRoleName().trim());
							if (LoginPresenter.myUserEntity.getRole().getRoleName().trim().equalsIgnoreCase("ADMIN")) {
								MyQuerry myquerry = new MyQuerry(new Vector<Filter>(),new WareHouse());
								oblwarehouse2.MakeLive(myquerry);
							
							} else {
								
								wareHouseList = new ArrayList<WareHouse>();
								ArrayList<EmployeeBranch> branchesOfEmployee = new ArrayList<EmployeeBranch>();
								// String fullname =
								// LoginPresenter.loggedInUser;
								for (Employee emp : LoginPresenter.globalEmployee) {
									if (emp.getCount() == UserConfiguration.getInfo().getEmpCount()) {
										if (emp.getEmpBranchList() != null&& emp.getEmpBranchList().size() != 0) {
											branchesOfEmployee.addAll(emp.getEmpBranchList());
											break;
										} else {
											EmployeeBranch emplBranch = new EmployeeBranch();
											emplBranch.setBranchName(emp.getBranchName());
											branchesOfEmployee.add(emplBranch);
											break;
										}
									}
								}
								System.out.println("branchesOfEmployee:::::::::::"+ branchesOfEmployee.size());
								ArrayList<String> branchNameList = new ArrayList<String>();
								branchNameList = getBranchNameList(branchesOfEmployee);
								if (getBranchWiseWareHouse(branchNameList)) {
									Timer timer = new Timer() {
										@Override
										public void run() {
											System.out.println("Warehouse List"+ wareHouseList.size());
											oblwarehouse2.setListItems(wareHouseList);
										}
									};
									timer.schedule(3000);
								}
							}
						}else{
							MyQuerry myquerry = new MyQuerry(new Vector<Filter>(),new WareHouse());
							oblwarehouse2.MakeLive(myquerry);
						}
					}
				}
			}
		});
	}

	

	/**
	 * 
	 * Description :@param branches assigned to Employee
	 * 
	 * @return branches Name Date: 14-Sept-2016 Project: EVA Erp Required for :
	 *         Everyone.(This has been developed for branch level restriction)
	 *         Created by: Rahul Verma.
	 */
	public ArrayList<String> getBranchNameList(ArrayList<EmployeeBranch> branchesOfEmployee) {
		ArrayList<String> list = new ArrayList<String>();
		for (int i = 0; i < branchesOfEmployee.size(); i++) {
			System.out.println("Branch Name::::::"+ branchesOfEmployee.get(i).getBranchName());
			list.add(branchesOfEmployee.get(i).getBranchName().trim());
		}
		return list;
	}

	/**
	 * END
	 */

	/**
	 * 
	 * Description : @param branches of Employee who is Logged In.
	 *
	 * @return This will return boolean value(if executed without exception)
	 *         Creation Date: 14-Sept-2016 Project : EVA ERP Required for:
	 *         Everyone.(This is created for branch level restriction. Created
	 *         by : Rahul Verma
	 */
	public boolean getBranchWiseWareHouse(ArrayList<String> branchNameList) {
		// TODO Auto-generated method stub

		GenricServiceAsync async = GWT.create(GenricService.class);

		// for (int i = 0; i < branchesOfEmployee.size(); i++) {
		MyQuerry querry = new MyQuerry();
		Company c = new Company();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = new Filter();
		filter.setLongValue(c.getCompanyId());
		filter.setQuerryString("companyId");
		filtervec.add(filter);

		filter = new Filter();
		filter.setQuerryString("branchdetails.branchName IN");
		filter.setList(branchNameList);
		filtervec.add(filter);

		querry.setFilters(filtervec);
		querry.setQuerryObject(new WareHouse());
		async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {

			@Override
			public void onFailure(Throwable caught) {

			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				System.out.println("Result Size:::::::::::::"+ result.size());
				if (result.size() != 0) {
					for (SuperModel model : result) {
						System.out.println("Inside On Success loop");
						WareHouse warehouseEntity = (WareHouse) model;
						wareHouseList.add(warehouseEntity);
						System.out.println("warehouselist after adding::::"+ wareHouseList.size());
					}
				}
			}
		});
		return true;
	}

	

	@Override
	public void onChange(ChangeEvent event) {
		if(event.getSource().equals(oblwarehouse)){
			System.out.println("Inside Change Event.......");
				retriveStorageLocation(lsStorageLoc ,oblwarehouse);
		}
		if(event.getSource().equals(lsStorageLoc)){
				System.out.println("Inside Change Event.......");
				retriveStorageBin(lsStoragebin , lsStorageLoc);
		}
		if(event.getSource().equals(parentOblwarehouse)){
			System.out.println("Inside Change Event.......");
				retriveStorageLocation(parentLsStorageLoc , parentOblwarehouse);
		}
		if(event.getSource().equals(parentLsStorageLoc)){
				System.out.println("Inside Change Event.......");
				retriveStorageBin(parentLstoragebin , parentLsStorageLoc);
		}
		if(event.getSource() == lsStoragebin){
			MyQuerry query = new MyQuerry();
			Company c=new Company();
			System.out.println("Company Id :: "+c.getCompanyId());
			
			Vector<Filter> filtervec=new Vector<Filter>();
			Filter filter = null;
			if(parentOblwarehouse.getSelectedIndex() != 0 && parentLsStorageLoc.getSelectedIndex() != 0 && parentLstoragebin.getSelectedIndex()!=0){
			filter = new Filter();
			filter.setQuerryString("companyId");
			filter.setLongValue(c.getCompanyId());
			filtervec.add(filter);
			
			filter = new Filter();
			filter.setQuerryString("wareHouseList.parentWareHouse");
			filter.setStringValue(parentOblwarehouse.getValue());
			filtervec.add(filter);
			
			filter = new Filter();
			filter.setQuerryString("wareHouseList.parentStorageLocation");
			filter.setStringValue(parentLsStorageLoc.getValue(parentLsStorageLoc.getSelectedIndex()));
			filtervec.add(filter);
			
			filter = new Filter();
			filter.setQuerryString("wareHouseList.pearentStorageBin");
			filter.setStringValue(parentLstoragebin.getValue(parentLstoragebin.getSelectedIndex()));
			filtervec.add(filter);
			
			query.setFilters(filtervec);
			query.setQuerryObject(new TechnicianWareHouseDetailsList());
			async.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					// TODO Auto-generated method stub
					if(result!= null && result.size() > 0){
						showDialogMessage("Parent Warehouse already exists.");
						parentOblwarehouse.setSelectedIndex(0);
						parentLsStorageLoc.setSelectedIndex(0);
						parentLstoragebin.setSelectedIndex(0);
					}
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					
				}
			});
		}
		}
	}
	
	// ******************************* Retrieving Storage location from ****************************************
	
	private void retriveStorageLocation(final ListBox lsStorageLoc , ObjectListBox oblwarehouse){
		MyQuerry query = new MyQuerry();
		Company c=new Company();
		System.out.println("Company Id :: "+c.getCompanyId());
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("warehouseName");
		filter.setStringValue(oblwarehouse.getValue());
		filtervec.add(filter);
		
		filter=new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);
		
		query.setFilters(filtervec);
		query.setQuerryObject(new StorageLocation());
		
		async.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				lsStorageLoc.clear();
				lsStoragebin.clear();
				lsStorageLoc.addItem("--SELECT--");
				lsStoragebin.addItem("--SELECT--");
				for (SuperModel model : result) {
					StorageLocation entity = (StorageLocation) model;
					storageList.add(entity.getBusinessUnitName());
					lsStorageLoc.addItem(entity.getBusinessUnitName());
					System.out.println("Location :"+entity.getBusinessUnitName());
				}
				
			}
		});
	}
	
	// ****************************** Retrieving Storage Bin From Storage location **********************************
	
	private void retriveStorageBin(final ListBox lsStoragebin , ListBox lsStorageLoc){
		MyQuerry query = new MyQuerry();
		Company c=new Company();
		System.out.println("Company Id :: "+c.getCompanyId());
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("storagelocation");
		filter.setStringValue(lsStorageLoc.getValue(lsStorageLoc.getSelectedIndex()));
		filtervec.add(filter);
		
		filter=new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);
		
		query.setFilters(filtervec);
		query.setQuerryObject(new Storagebin());
		
		async.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				lsStoragebin.clear();
				lsStoragebin.addItem("--SELECT--");
				for (SuperModel model : result) {
					Storagebin entity = (Storagebin) model;
					lsStoragebin.addItem(entity.getBinName());
					System.out.println("BIN :"+entity.getBinName());
				}
			}
		});
	}

private boolean validateTechnician(){
	List<TechnicianWarehouseDetails> list = technicianWareHouseDetailsTable.getDataprovider().getList();
			for(TechnicianWarehouseDetails technicianWarehouseDetails : list){
				if(technicianWarehouseDetails.getTechnicianName().equalsIgnoreCase(olbTechnicainName.getValue())){
					return false;
				}
			}
			
	return true;		
}

@Override
public boolean validate() {
	// TODO Auto-generated method stub
	return true;
}

@Override
public void refreshTableData() {
	// TODO Auto-generated method stub
	super.refreshTableData();
	technicianWareHouseDetailsTable.getTable().redraw();
	
}

}
