package com.slicktechnologies.client.views.technicianwarehousedetails;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.logging.Level;

import org.apache.poi.ss.formula.ptg.TblPtg;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.CompositeInterface;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.reusabledata.ConditionDialogBox;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.services.UpdateService;
import com.slicktechnologies.client.services.UpdateServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.device.ServiceMarkCompletePopUp;
import com.slicktechnologies.client.views.project.concproject.EmpolyeeTable;
import com.slicktechnologies.client.views.service.CustomerServiceMaterialTable;
import com.slicktechnologies.client.views.technicianschedule.MaterialProductTable;
import com.slicktechnologies.client.views.technicianschedule.ServiceCompletetionPopup;
import com.slicktechnologies.client.views.technicianschedule.TechnicianTable;
import com.slicktechnologies.client.views.technicianschedule.TechnicianToolTable;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.TechnicianWareHouseDetailsList;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerlog.CustomerLogDetails;
import com.slicktechnologies.shared.common.customerlog.MaterialRequired;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.inventory.MaterialMovementNote;
import com.slicktechnologies.shared.common.inventory.MaterialProduct;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;
import com.slicktechnologies.shared.common.inventory.StorageLocation;
import com.slicktechnologies.shared.common.inventory.Storagebin;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.scheduling.TeamManagement;
import com.slicktechnologies.shared.common.servicerelated.CompanyAsset;

public class TechnicianServiceSchedulePopup extends PopupScreen implements CompositeInterface , ChangeHandler{

	GenricServiceAsync genasync=GWT.create(GenricService.class);
	long companyId = UserConfiguration.getCompanyId();
	private DateBox dateBox;
	ObjectListBox<TeamManagement> olbTeam;
	private ListBox p_servicehours;
	private ListBox p_servicemin;
	private ListBox p_ampm;
	
	  ObjectListBox<Employee> olbEmployee;
	  Button btnEmployee;
	  EmpolyeeTable emptable;
		Button btnToolAdd;
		Button btnproductAdd;
		TechnicianToolTable toolTable;
		
		CustomerServiceMaterialTable materialProductTable;
		
		ObjectListBox<ItemProduct> olbitemProduct;
	    ObjectListBox<CompanyAsset> olbTool;
		ObjectListBox<WareHouse> olbWarehouseName;
		ObjectListBox<StorageLocation> olbStorageLocation;
		ObjectListBox<Storagebin> olbstorageBin;
		DoubleBox dbQuantity;
		ObjectListBox<Invoice> olbInvoiceId;
		ObjectListBox<Invoice> olbInvoiceDate;
		public InlineLabel lblUpdate = new InlineLabel("Complete service");
		ServiceMarkCompletePopUp markcompletepopup=new ServiceMarkCompletePopUp();
		PopupPanel panel = new PopupPanel();
		Service service = null;
		VerticalPanel vpanel1;
		VerticalPanel vpanel2;
		VerticalPanel vpanel3;
		VerticalPanel vpanel4;
		VerticalPanel vpanel5;

		HorizontalPanel hpanel1 = new HorizontalPanel();
	
		public static ListBox e_mondaytofriday;
		ObjectListBox<WareHouse> olbParentWarehouseName;
		ObjectListBox<StorageLocation> olbParentStorageLocation;
		ObjectListBox<Storagebin> olbParentstorageBin;
		boolean isCompleteButtonClick = false;
		public InlineLabel lblSchedule = new InlineLabel("Schedule");
		GeneralServiceAsync genService=GWT.create(GeneralService.class);
		ConditionDialogBox conditionDialogBox = new ConditionDialogBox("MMN is already created for this service. Do you want cancel previous MMN and create new?" , AppConstants.YES, AppConstants.NO);
		PopupPanel popupPanel = new PopupPanel();
		UpdateServiceAsync updateService=GWT.create(UpdateService.class);
		ArrayList<MaterialMovementNote> mmnList = null;
		public static String materialValue  ="";
		
		public TechnicianServiceSchedulePopup() {
		
		super();
		materialValue ="";
		createGui();
		btnToolAdd.addClickHandler(this);
		btnproductAdd.addClickHandler(this);
		lblUpdate.addClickHandler(this);
		lblSchedule.addClickHandler(this);

		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "HideCompleteServiceButton") && 
				!LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN") &&
				LoginPresenter.branchRestrictionFlag){
			lblUpdate.setVisible(false);
		}
		
	}
	
	
	public TechnicianServiceSchedulePopup(boolean flag) {
		
		super();
		materialValue ="";
		isCompleteButtonClick = flag;
		createGui();
		btnToolAdd.addClickHandler(this);
		btnproductAdd.addClickHandler(this);
		lblUpdate.addClickHandler(this);	
		lblSchedule.addClickHandler(this);
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "HideCompleteServiceButton") && 
				!LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN") &&
				LoginPresenter.branchRestrictionFlag){
			lblUpdate.setVisible(false);
		}else{
			if(!isCompleteButtonClick){
				lblUpdate.setVisible(false);
			}
		}
		if(isCompleteButtonClick){
			lblSchedule.setVisible(false);
			lblOk.setVisible(false);
			btnEmployee.setEnabled(false);
			btnproductAdd.setEnabled(false);
			btnToolAdd.setEnabled(false);
			
		}
		conditionDialogBox.getBtnOne().addClickHandler(this);
		conditionDialogBox.getBtnTwo().addClickHandler(this);
	}
	public TechnicianServiceSchedulePopup(String str) {
		
		super();
		materialValue = str;
		createGui();
		btnproductAdd.addClickHandler(this);
		
		lblUpdate.setVisible(false);
		lblSchedule.setVisible(false);
		
	}

	void initilizewidget() {
		InlineLabel nlnlblNewServiceDate = new InlineLabel(
				"Service Date        :");

//		dateBox = new DateBox();
		dateBox = new DateBoxWithYearSelector();

		dateBox.setEnabled(false);
		DateTimeFormat format = DateTimeFormat.getFormat("yyyy-MM-dd");
		DateBox.DefaultFormat defformat = new DateBox.DefaultFormat(format);
		dateBox.setFormat(defformat);

		InlineLabel nlnlblNewServicetme = new InlineLabel(
				"Service Time          :");

		InlineLabel hhlable = new InlineLabel(" HH ");

		p_servicehours = new ListBox();
		p_servicehours.setSize("60px", "24px");

		p_servicehours.insertItem("--", 0);
		p_servicehours.insertItem("01", 1);
		p_servicehours.insertItem("02", 2);
		p_servicehours.insertItem("03", 3);
		p_servicehours.insertItem("04", 4);
		p_servicehours.insertItem("05", 5);
		p_servicehours.insertItem("06", 6);
		p_servicehours.insertItem("07", 7);
		p_servicehours.insertItem("08", 8);
		p_servicehours.insertItem("09", 9);
		p_servicehours.insertItem("10", 10);
		p_servicehours.insertItem("11", 11);
		p_servicehours.insertItem("12", 12);

		InlineLabel mmlable = new InlineLabel(" MM ");

		p_servicemin = new ListBox();
		p_servicemin.setSize("60px", "24px");
		p_servicemin.insertItem("--", 0);
		p_servicemin.insertItem("00", 1);
		p_servicemin.insertItem("15", 2);
		p_servicemin.insertItem("30", 3);
		p_servicemin.insertItem("45", 4);

		InlineLabel sslable = new InlineLabel(" AM/PM ");

		p_ampm = new ListBox();
		p_ampm.setSize("60px", "24px");
		p_ampm.insertItem("--", 0);
		p_ampm.insertItem("AM", 1);
		p_ampm.insertItem("PM", 2);

		olbEmployee = new ObjectListBox<Employee>();

		btnEmployee = new Button("Add Employee");

		emptable = new EmpolyeeTable();
		emptable.connectToLocal();
		olbTeam = new ObjectListBox<TeamManagement>();
		this.initalizeTeamListBox();

		InlineLabel specificReasonlbl = new InlineLabel("Team               :");
		specificReasonlbl.setSize("200px", "18px");

		olbTeam.getElement().getStyle().setHeight(23, Unit.PX);

		InlineLabel serviceEnglbl = new InlineLabel(
				"Technician Engineer          :");

		emptable.getTable().setHeight("150px");

		btnToolAdd = new Button("ADD");
		btnproductAdd = new Button("ADD");
		toolTable = new TechnicianToolTable();
		materialProductTable = new CustomerServiceMaterialTable();
		materialProductTable.connectToLocal();
		olbitemProduct = new ObjectListBox<ItemProduct>();
		olbitemProduct.addItem("--SELECT--");
		initializeItemProdcut(companyId, olbitemProduct);
		olbitemProduct.addChangeHandler(this);

		olbTool = new ObjectListBox<CompanyAsset>();
		olbTool.addItem("--SELECT--");
		AppUtility.makeCompanyAssetListBoxLive(olbTool);

		olbWarehouseName = new ObjectListBox<WareHouse>();
		olbWarehouseName.addItem("--SELECT--");

		olbWarehouseName.addChangeHandler(this);

		olbStorageLocation = new ObjectListBox<StorageLocation>();
		olbStorageLocation.addItem("--SELECT--");

		olbStorageLocation.addChangeHandler(this);

		olbstorageBin = new ObjectListBox<Storagebin>();
		olbstorageBin.addItem("--SELECT--");
		
		
		olbParentWarehouseName = new ObjectListBox<WareHouse>();
		olbParentWarehouseName.addItem("--SELECT--");

		olbParentWarehouseName.addChangeHandler(this);

		olbParentStorageLocation = new ObjectListBox<StorageLocation>();
		olbParentStorageLocation.addItem("--SELECT--");

		olbParentStorageLocation.addChangeHandler(this);

		olbParentstorageBin = new ObjectListBox<Storagebin>();
		olbParentstorageBin.addItem("--SELECT--");

		dbQuantity = new DoubleBox();
		olbInvoiceId = new ObjectListBox<Invoice>();
		olbInvoiceId.addItem("--SELECT--");
		olbInvoiceId.addChangeHandler(this);
		olbInvoiceDate = new ObjectListBox<Invoice>();
		olbInvoiceDate.addItem("--SELECT--");
		olbInvoiceDate.addChangeHandler(this);

		vpanel1 = new VerticalPanel();
		vpanel1.getElement().addClassName("serVertical");
		vpanel2 = new VerticalPanel();
		vpanel2.getElement().addClassName("serVertical");
		vpanel3 = new VerticalPanel();
		vpanel3.getElement().addClassName("serVertical");
		vpanel4 = new VerticalPanel();
		vpanel4.getElement().addClassName("serVertical");
		vpanel5 = new VerticalPanel();
		vpanel5.getElement().addClassName("serVertical");
		hpanel1 = new HorizontalPanel();
		hpanel1.getElement().addClassName("serHor");
		Label lbl1 = new Label("HH :");
		lbl1.getElement().addClassName("lblSer");
		vpanel1.add(lbl1);
		vpanel1.add(p_servicehours);

		Label lbl2 = new Label("MM :");
		lbl2.getElement().addClassName("lblSer");

		vpanel2.add(lbl2);
		vpanel2.add(p_servicemin);

		Label lbl3 = new Label("AM/PM : ");
		lbl3.getElement().addClassName("lblSer");
		vpanel3.add(lbl3);
		vpanel3.add(p_ampm);

		Label lbl4 = new Label("Invoice Id");
		lbl4.getElement().addClassName("lblSer");

		vpanel4.add(lbl4);
		vpanel4.add(olbInvoiceId);

		Label lbl5 = new Label("Invoice Date");
		lbl5.getElement().addClassName("lblSer");

		vpanel5.add(lbl5);
		vpanel5.add(olbInvoiceDate);

		hpanel1.add(vpanel1);
		hpanel1.add(vpanel2);
		hpanel1.add(vpanel3);
		hpanel1.add(vpanel4);
		hpanel1.add(vpanel5);
		
		AppUtility.initializeWarehouse(olbWarehouseName);
		AppUtility.initializeWarehouse(olbParentWarehouseName);
		
	}

	@Override
	public void createScreen() {
		
		// TODO Auto-generated method stub
		initilizewidget();		
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingServiceScheduleDetails=fbuilder.setlabel("Service Schedule Details").widgetType(FieldType.GREYGROUPING).setMandatory(false).setColSpan(6).build();
		
		fbuilder = new FormFieldBuilder("Service Date :",dateBox);
		FormField fdbdateBox= fbuilder.setRowSpan(0).setColSpan(0).build();
	
		
		fbuilder = new FormFieldBuilder("HH : ",p_servicehours);
		FormField fdbserviceHours= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("MM : ",p_servicemin);
		FormField fdbeserviceMin= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("AM/PM : ",p_ampm);
		FormField fdbeserviceAmPm= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingTechnicianDetails=fbuilder.setlabel("Technician Details").widgetType(FieldType.GREYGROUPING).setMandatory(false).setColSpan(6).build();
		
		fbuilder = new FormFieldBuilder("Team :",olbTeam);
		FormField fobjectList= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Service Engineer :",olbEmployee);
		FormField fTechobjectList= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",btnEmployee);
		FormField fTechobjectBtn= fbuilder.setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder(" ",emptable.getTable());
		FormField fdbTechList= fbuilder.setRowSpan(0).setColSpan(6).build();
		fbuilder = new FormFieldBuilder();
		FormField fblankgroupthree=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/** date 27.02.2018 added by komal to change popup same as technician scheduling list plan popup **/
		fbuilder = new FormFieldBuilder("" , btnToolAdd);
		FormField fbtnToolAdd= fbuilder.setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("" , btnproductAdd);
		FormField fbtnproductAdd= fbuilder.setRowSpan(0).setColSpan(0).build();	
		
		fbuilder = new FormFieldBuilder("" , toolTable.getTable());
		FormField ftoolTable= fbuilder.setRowSpan(0).setColSpan(6).build();
		
		fbuilder = new FormFieldBuilder("" , materialProductTable.getTable());
		FormField fmaterialProductTable= fbuilder.setRowSpan(0).setColSpan(6).build();
				
		fbuilder = new FormFieldBuilder("Material Name" , olbitemProduct);
		FormField folbitemProduct= fbuilder.setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Tool" , olbTool);
		FormField folbTool= fbuilder.setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("WareHouse" , olbWarehouseName);
		FormField folbWarehouseName= fbuilder.setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Storage Location" , olbStorageLocation);
		FormField folbStorageLocation= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Storage Bin" , olbstorageBin);
		FormField folbstorageBin = fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Quantity" , dbQuantity);
		FormField fdbQuantity= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Invoice Id" , olbInvoiceId);
		FormField folbInvoiceId= fbuilder.setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Invoice Date" , olbInvoiceDate);
		FormField folbInvoiceDate= fbuilder.setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder();
		FormField fToolInformation =fbuilder.setlabel("Tool Information").widgetType(FieldType.GREYGROUPING).setMandatory(false).setColSpan(6).build();

		fbuilder = new FormFieldBuilder();
		FormField fMaterialInformation =fbuilder.setlabel("Material Information").widgetType(FieldType.GREYGROUPING).setMandatory(false).setColSpan(6).build();

		fbuilder = new FormFieldBuilder("" , hpanel1);
		FormField fhpanel1 =fbuilder.setRowSpan(0).setColSpan(5).build();

		fbuilder = new FormFieldBuilder();
		FormField fTechnicianWarehouseInformation =fbuilder.setlabel("Technician Warehouse Details").widgetType(FieldType.GREYGROUPING).setMandatory(false).setColSpan(6).build();

		fbuilder = new FormFieldBuilder();
		FormField fParentWarehouseInformation =fbuilder.setlabel("Parent Warehouse Details").widgetType(FieldType.GREYGROUPING).setMandatory(false).setColSpan(6).build();
		
		
		fbuilder = new FormFieldBuilder("WareHouse" , olbParentWarehouseName);
		FormField folbParentWarehouseName= fbuilder.setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Storage Location" , olbParentStorageLocation);
		FormField folbParentStorageLocation= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Storage Bin" , olbParentstorageBin);
		FormField folbParentstorageBin = fbuilder.setRowSpan(0).setColSpan(0).build();
		

		if(materialValue != null && materialValue.equalsIgnoreCase("Material")){
			FormField[][] formfield = {
					{fMaterialInformation },
					{folbitemProduct , fdbQuantity , fbtnproductAdd} ,//
					{fmaterialProductTable},
					{fParentWarehouseInformation},
					{folbParentWarehouseName ,folbParentStorageLocation , folbParentstorageBin   },
					{fTechnicianWarehouseInformation},
					{folbWarehouseName , folbStorageLocation , folbstorageBin },
					{fToolInformation},
					{folbTool , fbtnToolAdd},
					{ftoolTable},
			};
			this.fields=formfield;	
		}else{
			
			FormField[][] formfield = {
					{fgroupingServiceScheduleDetails},
					{fdbdateBox, fdbserviceHours , fdbeserviceMin , fdbeserviceAmPm ,folbInvoiceId ,folbInvoiceDate},
					{fgroupingTechnicianDetails},
					{fobjectList , fTechobjectList , fTechobjectBtn },
					{fdbTechList },
					
					{fMaterialInformation },
					{folbitemProduct , fdbQuantity , fbtnproductAdd} ,//
					{fmaterialProductTable},
					{fParentWarehouseInformation},
					{folbParentWarehouseName ,folbParentStorageLocation , folbParentstorageBin   },
					{fTechnicianWarehouseInformation},
					{folbWarehouseName , folbStorageLocation , folbstorageBin },
					{fToolInformation},
					{folbTool , fbtnToolAdd},
					{ftoolTable},
					
			};
			this.fields=formfield;	
		}

		
	}
	
	private void initalizeTeamListBox() {
		MyQuerry querry = new MyQuerry();
		querry.setQuerryObject(new TeamManagement());
		olbTeam.MakeLive(querry);
		
		/**
		 *  nidhi
		 *  12-10-2017
		 *  
		 */
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "LoadOperatorDropDown")){
			olbEmployee.makeEmployeeLive(AppConstants.SERVICEMODULE, AppConstants.CUSTOMERPROJECT, "Operator");
		}else{
			olbEmployee.makeEmployeeLive(AppConstants.SERVICEMODULE, AppConstants.CUSTOMERPROJECT, "Technician");
		}
		
		/**
		 *  end
		 */
	}
	@Override
	public void setEnable(boolean status) {
		super.setEnable(status);
		dateBox.setEnabled(false);
		
	}

	@Override
	public boolean validate() {
		return false;
	}

	public void sethrList(int i){
		this.p_servicehours.setSelectedIndex(i);
	}
	
	public void clear() {
		this.p_servicehours.setSelectedIndex(0);
		this.p_servicemin.setSelectedIndex(0);
		this.p_ampm.setSelectedIndex(0);
		this.olbTeam.setSelectedIndex(0);
		/**
		 *  nidhi
		 *  12-10-2017
		 *  
		 */
		this.emptable.getDataprovider().getList().clear();
		this.olbEmployee.setSelectedIndex(0);
	}
	
	

	public DateBox getDateBox() {
		return dateBox;
	}


	public void setDateBox(DateBox dateBox) {
		this.dateBox = dateBox;
	}


//	public Button getBtnOk() {
//		return btnOk;
//	}
//
//
//	public void setBtnOk(Button btnOk) {
//		this.btnOk = btnOk;
//	}
//
//
//	public Button getBtnCancel() {
//		return btnCancel;
//	}
//
//
//	public void setBtnCancel(Button btnCancel) {
//		this.btnCancel = btnCancel;
//	}


	public ObjectListBox<TeamManagement> getOlbTeam() {
		return olbTeam;
	}


	public void setOlbTeam(ObjectListBox<TeamManagement> olbTeam) {
		this.olbTeam = olbTeam;
	}


	public ListBox getP_servicehours() {
		return p_servicehours;
	}


	public void setP_servicehours(ListBox p_servicehours) {
		this.p_servicehours = p_servicehours;
	}


	public ListBox getP_servicemin() {
		return p_servicemin;
	}


	public void setP_servicemin(ListBox p_servicemin) {
		this.p_servicemin = p_servicemin;
	}


	public ListBox getP_ampm() {
		return p_ampm;
	}


	public void setP_ampm(ListBox p_ampm) {
		this.p_ampm = p_ampm;
	}


	public ObjectListBox<Employee> getOlbEmployee() {
		return olbEmployee;
	}


	public void setOlbEmployee(ObjectListBox<Employee> olbEmployee) {
		this.olbEmployee = olbEmployee;
	}


	public Button getBtnEmployee() {
		return btnEmployee;
	}


	public void setBtnEmployee(Button btnEmployee) {
		this.btnEmployee = btnEmployee;
	}


	public EmpolyeeTable getEmptable() {
		return emptable;
	}


	public void setEmptable(EmpolyeeTable emptable) {
		this.emptable = emptable;
	}


	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		if(event.getSource()==btnToolAdd){
			System.out.println("btnToolAdd ==");
			if(olbTool.getSelectedIndex()!=0){
				System.out.println("btnToolAdd == 222222222");

				CompanyAsset companyAsset = olbTool.getSelectedItem();
//				CompanyAsset techniciantoolinfo = new CompanyAsset();
//				techniciantoolinfo.setName(olbTool.getValue(olbTool.getSelectedIndex()));
//				techniciantoolinfo.setCount(companyAsset.getCount());
				toolTable.getDataprovider().getList().add(companyAsset);
				
			}else{
				System.out.println("btnToolAdd else");
				showDialogMessage("Please Select Tool");
			}
			
		} 
		
		if(event.getSource()==btnproductAdd){
			
			System.out.println("btnproductAdd ==");

			if(ValidateWarehouse()){
				System.out.println("btnproductAdd == 222222222222");
				
				ItemProduct product = olbitemProduct.getSelectedItem();
//				TechnicianMaterialInfo technicianMaterialIfno = new TechnicianMaterialInfo();
				ProductGroupList technicianMaterialIfno  = new ProductGroupList();
				technicianMaterialIfno.setName(olbitemProduct.getValue(olbitemProduct.getSelectedIndex()));
				if(dbQuantity.getValue()!=null && dbQuantity.getValue() != 0 ){
					technicianMaterialIfno.setQuantity(dbQuantity.getValue());
					technicianMaterialIfno.setPlannedQty(dbQuantity.getValue());
					technicianMaterialIfno.setProActualQty(dbQuantity.getValue());
				}
			//	if(olbWarehouseName.getSelectedIndex()!=0)
					technicianMaterialIfno.setWarehouse(getOlbWarehouseName().getValue(getOlbWarehouseName().getSelectedIndex()));
			//	if(olbStorageLocation.getSelectedIndex()!=0)
					technicianMaterialIfno.setStorageLocation(getOlbStorageLocation().getValue(getOlbStorageLocation().getSelectedIndex()));	
			//	if(olbstorageBin.getSelectedIndex()!=0)
					technicianMaterialIfno.setStorageBin(getOlbstorageBin().getValue(getOlbstorageBin().getSelectedIndex()));	

			//	if(olbParentWarehouseName.getSelectedIndex()!=0)
					technicianMaterialIfno.setParentWarentwarehouse(getOlbParentWarehouseName().getValue(getOlbParentWarehouseName().getSelectedIndex()));
			//	if(olbParentStorageLocation.getSelectedIndex()!=0)
					technicianMaterialIfno.setParentStorageLocation(getOlbParentStorageLocation().getValue(getOlbParentStorageLocation().getSelectedIndex()));	
			//	if(olbParentstorageBin.getSelectedIndex()!=0)
					technicianMaterialIfno.setParentStorageBin(getOlbParentstorageBin().getValue(getOlbParentstorageBin().getSelectedIndex()));	
				
				technicianMaterialIfno.setProduct_id(product.getCount());
				technicianMaterialIfno.setCode(product.getProductCode());
				technicianMaterialIfno.setUnit(product.getUnitOfMeasurement());
				/** date 22.03.2019 added by komal to store price in project **/
				if(product.getPurchasePrice() != 0){
					technicianMaterialIfno.setPrice(product.getPurchasePrice());
				}else{
					technicianMaterialIfno.setPrice(product.getPrice());
				}
				
				materialProductTable.getDataprovider().getList().add(technicianMaterialIfno);
			}
		}
		if(event.getSource().equals(lblUpdate)){
			GWTCAlert alert =  new GWTCAlert();
			//alert.alert("hello button");
			/**
			 * date 12.04.2018 added by komal for orion
			 */
			boolean configFlag = AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "RestrictServiceCompletionForServicewiseBilling");
			if(service.isServiceWiseBilling() && configFlag){    
				panel.hide();
				alert.alert("As it is service wise bill-service, you can not complete it from here.");
				
            }else{
			if(validateCompletion()){
				Date todayDate=new Date();
				if(todayDate.before(getDateBox().getValue())){
					System.out.println("Service Date Exceeds");
					alert.alert("Service Date exceeds current date. Please reschedule the date");
					
				}else{
					System.out.println("Pop Up Showing");
					panel=new PopupPanel(true);
					panel.add(markcompletepopup);
					panel.show();
					panel.center();
					
					addDefaultValueInMarkCompletePopUp(markcompletepopup);
				}
			}
           }
		
		}	
		if(event.getSource().equals(lblSchedule)){
			//create MNN CODE ..../
			lblUpdate.setText("Processing...");
			MyQuerry query = new MyQuerry();
			Company c=new Company();
			System.out.println("Company Id :: "+c.getCompanyId());
			
			Vector<Filter> filtervec=new Vector<Filter>();
			Filter filter = null;
			
			filter = new Filter();
			filter.setQuerryString("companyId");
			filter.setLongValue(c.getCompanyId());
			filtervec.add(filter);
			
			filter = new Filter();
			filter.setQuerryString("serviceId");
			filter.setIntValue(service.getCount());
			filtervec.add(filter);
			
			filter = new Filter();
			filter.setQuerryString("status");
			filter.setStringValue(MaterialMovementNote.APPROVED);
			filtervec.add(filter);
				
			query.setFilters(filtervec);
			query.setQuerryObject(new MaterialMovementNote());
			
			genasync.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {

				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					lblUpdate.setText("Schedule");
				}

				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					// TODO Auto-generated method stubgfg
					if(result != null && result.size() > 0){
						mmnList = new ArrayList<MaterialMovementNote>();
						Console.log( "mmn size :"+ result.size());
						for(SuperModel model : result){
							mmnList.add((MaterialMovementNote) model);
						}
						lblUpdate.setVisible(true);
						popupPanel=new PopupPanel(true);
						popupPanel.add(conditionDialogBox);
						popupPanel.show();
						popupPanel.center();
					}else{
						reactOnScheduleButton();
					}
				}
			});
			
			
		}
		if(event.getSource() == conditionDialogBox.getBtnOne()){
			popupPanel.hide();
			cancelMMN(mmnList);
			
		}
		if(event.getSource() == conditionDialogBox.getBtnTwo()){
			popupPanel.hide();
		}
	}

	private void initializeItemProdcut(Long companyId, final ObjectListBox<ItemProduct> olbitemProduct) {
		// TODO Auto-generated method stub
		System.out.println("Hi Company Id =="+companyId);
		
		GenricServiceAsync genasync=GWT.create(GenricService.class);

		Vector<Filter> filtervec=new Vector<Filter>();
		Company c  = new Company();

		Filter filter =null;
		GWTCAlert alert = new GWTCAlert();
		filter= new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		filter= new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);
		
		MyQuerry querry = new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new ItemProduct());

		genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				
				List<ItemProduct> list = new ArrayList<ItemProduct>();
				for(SuperModel model : result){
					ItemProduct item = (ItemProduct) model;
					
//					olbitemProduct.addItem(item.getProductName());
					list.add(item);
				}
				/**
				 * @author Vijay Updated code with sorting order
				 */
				Comparator<ItemProduct> supermodelcomparator = new Comparator<ItemProduct>() {
					public int compare(ItemProduct c1, ItemProduct c2) {
					
						return c1.getProductName().toLowerCase().compareTo(c2.getProductName().toLowerCase());
					}
				};
				Collections.sort(list,supermodelcomparator);
				
				for(ItemProduct itemprod : list){
					olbitemProduct.addItem(itemprod.getProductName());
				}
				olbitemProduct.setItems(list);
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
		
	}

	@Override
	public void onChange(ChangeEvent event) {
		// TODO Auto-generated method stub
//		if(event.getSource() != olbitemProduct){
//					if(olbitemProduct.getSelectedIndex()==0){
////				olbWarehouseName.setSelectedIndex(0);
////				olbStorageLocation.setSelectedIndex(0);
////				olbstorageBin.setSelectedIndex(0);
////			}else{
//				ItemProduct product = olbitemProduct.getSelectedItem();
//				initializeProductWarehouse(product.getCount());
//			}
//		}
		if(event.getSource()==olbWarehouseName){
			if(olbWarehouseName.getSelectedIndex()==0){
				olbStorageLocation.setSelectedIndex(0);
				olbstorageBin.setSelectedIndex(0);
			}else{
				initializeStoragelocation(olbWarehouseName.getValue(olbWarehouseName.getSelectedIndex()) , olbStorageLocation);
			}
		}
		if(event.getSource()==olbStorageLocation){
			initializeBin(olbStorageLocation.getValue(olbStorageLocation.getSelectedIndex()) , olbstorageBin);
		}
		
		if(event.getSource() == olbInvoiceId){
			setinvoiceDateDropDown();
		}
		if(event.getSource() == olbInvoiceDate){
			setInvoiceIdDroDown();
		}
		if(event.getSource()==olbParentWarehouseName){
			if(olbParentWarehouseName.getSelectedIndex()==0){
				olbParentStorageLocation.setSelectedIndex(0);
				olbParentstorageBin.setSelectedIndex(0);
			}else{
				initializeStoragelocation(olbParentWarehouseName.getValue(olbParentWarehouseName.getSelectedIndex()) ,olbParentStorageLocation );
			}
		}
		if(event.getSource()==olbParentStorageLocation){
			initializeBin(olbParentStorageLocation.getValue(olbParentStorageLocation.getSelectedIndex()) ,olbParentstorageBin );
		}
		
	}
	private void setInvoiceIdDroDown() {

		if(olbInvoiceDate.getSelectedIndex()!=0){
//			olbInvoiceId.removeAllItems();
//			olbInvoiceId.addItem("--SELECT--");
			Invoice invoiceEntity = olbInvoiceDate.getSelectedItem();
			olbInvoiceId.setValue(invoiceEntity.getCount()+"");
			
		}else{
			olbInvoiceId.setSelectedIndex(0);
		}
	}

	private void setinvoiceDateDropDown() {
		if(olbInvoiceId.getSelectedIndex()!=0){
//			olbInvoiceDate.removeAllItems();
//			olbInvoiceDate.addItem("--SELECT--");
			Invoice invoicentity = olbInvoiceId.getSelectedItem();
			olbInvoiceDate.setValue(DateTimeFormat.getShortDateFormat().format(invoicentity.getInvoiceDate()));
		}else{
			olbInvoiceDate.setSelectedIndex(0);
		}
	}

	private void initializeProductWarehouse(int prodId) {

		olbWarehouseName.removeAllItems();
		
		MyQuerry query = new MyQuerry();
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		

		filter = new Filter();
		filter.setQuerryString("productinfo.prodID");
		filter.setIntValue(prodId);
		filtervec.add(filter);
		
		query.setFilters(filtervec);
		query.setQuerryObject(new ProductInventoryView());
		
		olbWarehouseName.addItem("--SELECT--");
		
		genasync.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				System.out.println("Warehouse size as per product"+result.size());
				for(SuperModel model : result){
					ProductInventoryView product = (ProductInventoryView) model;
					for(int i=0;i<product.getDetails().size();i++){
						olbWarehouseName.addItem(product.getDetails().get(i).getWarehousename().trim());
						System.out.println("hiiiiiiiiiiiiiii");
					}
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
		
	}

	private void initializeBin(String storageLocation , final ObjectListBox olbstorageBin) {
		
		olbstorageBin.removeAllItems();

		MyQuerry query = new MyQuerry();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(companyId);
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("storagelocation");
		filter.setStringValue(storageLocation);
		filtervec.add(filter);
	
		query.setFilters(filtervec);
		query.setQuerryObject(new Storagebin());
		
		olbstorageBin.addItem("--SELECT--");
		genasync.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				System.out.println("result of storage bin =="+result.size());
				for(SuperModel model :result){
					Storagebin storagebin = (Storagebin) model;
					olbstorageBin.addItem(storagebin.getBinName());
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
	}

	private void initializeStoragelocation(String warehouseName , final ObjectListBox olbStorageLocation) {

		olbStorageLocation.removeAllItems();
		MyQuerry query = new MyQuerry();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(companyId);
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("warehouseName");
		filter.setStringValue(warehouseName);
		filtervec.add(filter);
	
		query.setFilters(filtervec);
		query.setQuerryObject(new StorageLocation());
		
		olbStorageLocation.addItem("--SELECT--");
		
		genasync.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				
				
				for(SuperModel model :result){
					StorageLocation storageLocation = (StorageLocation) model;
					olbStorageLocation.addItem(storageLocation.getBusinessUnitName());
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
		
	}

	private boolean ValidateWarehouse() {

		if (olbitemProduct.getSelectedIndex() == 0) {
			System.out.println("olbitemProduct");
			showDialogMessage("Please select Material information");
			return false;
		}
  if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "WarehouseDetailsMandatory")){
		if (olbWarehouseName.getSelectedIndex() == 0) {
			showDialogMessage("Please select Technician warehouse name");
			return false;
		}
		if (olbStorageLocation.getSelectedIndex() == 0) {
			showDialogMessage("Please select Technician storage location");
			return false;
		}
		if (olbstorageBin.getSelectedIndex() == 0) {
			showDialogMessage("Please select Technician storage Bin");
			return false;
		}

		if (olbParentWarehouseName.getSelectedIndex() == 0) {
			showDialogMessage("Please select Parent warehouse name");
			return false;
		}
		if (olbParentStorageLocation.getSelectedIndex() == 0) {
			showDialogMessage("Please select Parent storage location");
			return false;
		}
		if (olbParentstorageBin.getSelectedIndex() == 0) {
			showDialogMessage("Please select Parent storage Bin");
			return false;
		}
   }
		if (olbitemProduct.getSelectedIndex() != 0
				&& dbQuantity.getValue() == null) {
			showDialogMessage("Please add quantity");
			return false;
		}
		
		
		return true;
	}

	public TechnicianToolTable getToolTable() {
		return toolTable;
	}

	public void setToolTable(TechnicianToolTable toolTable) {
		this.toolTable = toolTable;
	}

	public CustomerServiceMaterialTable getMaterialProductTable() {
		return materialProductTable;
	}

	public void setMaterialProductTable(CustomerServiceMaterialTable materialProductTable) {
		this.materialProductTable = materialProductTable;
	}

	public ObjectListBox<ItemProduct> getOlbitemProduct() {
		return olbitemProduct;
	}

	public void setOlbitemProduct(ObjectListBox<ItemProduct> olbitemProduct) {
		this.olbitemProduct = olbitemProduct;
	}

	public ObjectListBox<CompanyAsset> getOlbTool() {
		return olbTool;
	}

	public void setOlbTool(ObjectListBox<CompanyAsset> olbTool) {
		this.olbTool = olbTool;
	}

	public ObjectListBox<WareHouse> getOlbWarehouseName() {
		return olbWarehouseName;
	}

	public void setOlbWarehouseName(ObjectListBox<WareHouse> olbWarehouseName) {
		this.olbWarehouseName = olbWarehouseName;
	}

	public ObjectListBox<StorageLocation> getOlbStorageLocation() {
		return olbStorageLocation;
	}

	public void setOlbStorageLocation(
			ObjectListBox<StorageLocation> olbStorageLocation) {
		this.olbStorageLocation = olbStorageLocation;
	}

	public ObjectListBox<Storagebin> getOlbstorageBin() {
		return olbstorageBin;
	}

	public void setOlbstorageBin(ObjectListBox<Storagebin> olbstorageBin) {
		this.olbstorageBin = olbstorageBin;
	}

	public DoubleBox getDbQuantity() {
		return dbQuantity;
	}

	public void setDbQuantity(DoubleBox dbQuantity) {
		this.dbQuantity = dbQuantity;
	}

	public ObjectListBox<Invoice> getOlbInvoiceId() {
		return olbInvoiceId;
	}

	public void setOlbInvoiceId(ObjectListBox<Invoice> olbInvoiceId) {
		this.olbInvoiceId = olbInvoiceId;
	}

	public ObjectListBox<Invoice> getOlbInvoiceDate() {
		return olbInvoiceDate;
	}

	public void setOlbInvoiceDate(ObjectListBox<Invoice> olbInvoiceDate) {
		this.olbInvoiceDate = olbInvoiceDate;
	}
	private boolean validateCompletion() {
		GWTCAlert alert = new GWTCAlert();
		if(this.getMaterialProductTable().getTable() != null && this
				.getMaterialProductTable().getDataprovider().getList().size() >0){
			for(ProductGroupList group : this.getMaterialProductTable().getDataprovider().getList()){
				if(group.getReturnQuantity() > group.getProActualQty()){
					alert.alert("Consumed quantity should not be more than required quantity.");
					return false;
				}
			}
		}
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "OnlyForNBHC")){
			System.out.println("Config true");
			if(!UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin")){
				System.out.println("Not an admin");
				DateTimeFormat format= DateTimeFormat.getFormat("dd/MM/yyyy");
				Date serviceDate=getDateBox().getValue();
				Date lastValidDate=CalendarUtil.copyDate(serviceDate);
				Console.log("Service Date : "+format.format(serviceDate));
				System.out.println("Service Date : "+format.format(serviceDate));
				if(serviceDate!=null){
					CalendarUtil.addMonthsToDate(lastValidDate, 1);
					lastValidDate.setDate(5);
					Console.log("Valid/Deadline date "+format.format(lastValidDate));
					System.out.println("Valid/Deadline date "+format.format(lastValidDate));
					Date todaysDate=new Date();
					lastValidDate=format.parse(format.format(lastValidDate));
					todaysDate=format.parse(format.format(todaysDate));
					Console.log("Todays Date : "+format.format(todaysDate));
					System.out.println("Todays Date : "+format.format(todaysDate));
					if(todaysDate.after(lastValidDate)){
						alert.alert("Service completion of previous month is not allowed..!");
						return false;
					}
				}else{
					alert.alert("Service date is null");
					return false;
				}
			}
			return true;
		}else{
			System.out.println("Config false");
			return true;
		}
	}
private void addDefaultValueInMarkCompletePopUp(ServiceMarkCompletePopUp markcompletepopup2) {
		
		Console.log("Inside addDefaultValue");
		Console.log("model.getServiceCompleteDuration() :::"+getService().getServiceCompleteDuration());
		Console.log("model.getServiceCompletionDate():::"+getService().getServiceCompletionDate());
		Console.log("model.getServiceCompleteRemark():::"+getService().getServiceCompleteRemark());

		if (getService().getServiceCompleteDuration() != 0)
			markcompletepopup2.getServiceTime().setValue(
					getService().getServiceCompleteDuration());

		if (getService().getServiceCompletionDate() != null) {
			DateTimeFormat sdf = DateTimeFormat.getFormat("dd/MM/yyyy");

			markcompletepopup2.getServiceCompletionDate().setValue(
					sdf.parse(sdf.format(getService().getServiceCompletionDate())));

		}

		if (getService().getServiceCompleteRemark() != null
				&& !getService().getServiceCompleteRemark().equalsIgnoreCase("")) {
			markcompletepopup2.getRemark().setValue(
					getService().getServiceCompleteRemark().trim());
		}

		if (getService().getCustomerFeedback() != null
				&& !getService().getCustomerFeedback().equalsIgnoreCase("")) {
			int index = 0;
			for (int i = 0; i < markcompletepopup2.getLbRating().getItemCount(); i++) {
				if (getService()
						.getCustomerFeedback()
						.trim()
						.equalsIgnoreCase(
								markcompletepopup2.getLbRating().getValue(i))) {
					index = i;
				}
			}
			
			Console.log("index:::"+index);
			markcompletepopup2.getLbRating().setSelectedIndex(index);
		}
		
	}

		public Service getService() {
			return service;
		}
		
		public void setService(Service service) {
			this.service = service;
		}

		public ServiceMarkCompletePopUp getMarkcompletepopup() {
			return markcompletepopup;
		}

		public void setMarkcompletepopup(ServiceMarkCompletePopUp markcompletepopup) {
			this.markcompletepopup = markcompletepopup;
		}

		public PopupPanel getPanel() {
			return panel;
		}

		public void setPanel(PopupPanel panel) {
			this.panel = panel;
		}


		public InlineLabel getLblUpdate() {
			return lblUpdate;
		}


		public void setLblUpdate(InlineLabel lblUpdate) {
			this.lblUpdate = lblUpdate;
		}


		public ObjectListBox<WareHouse> getOlbParentWarehouseName() {
			return olbParentWarehouseName;
		}


		public void setOlbParentWarehouseName(
				ObjectListBox<WareHouse> olbParentWarehouseName) {
			this.olbParentWarehouseName = olbParentWarehouseName;
		}


		public ObjectListBox<StorageLocation> getOlbParentStorageLocation() {
			return olbParentStorageLocation;
		}


		public void setOlbParentStorageLocation(
				ObjectListBox<StorageLocation> olbParentStorageLocation) {
			this.olbParentStorageLocation = olbParentStorageLocation;
		}


		public ObjectListBox<Storagebin> getOlbParentstorageBin() {
			return olbParentstorageBin;
		}


		public void setOlbParentstorageBin(ObjectListBox<Storagebin> olbParentstorageBin) {
			this.olbParentstorageBin = olbParentstorageBin;
		}


		public InlineLabel getLblSchedule() {
			return lblSchedule;
		}


		public void setLblSchedule(InlineLabel lblSchedule) {
			this.lblSchedule = lblSchedule;
		}

		private void createMMNForReMaterial(final TechnicianServiceSchedulePopup popup){
			int customerId;
			String customerName;
			customerId = popup.getService().getCustomerId();
			customerName = popup.getService().getCustomerName();
			final MaterialMovementNote mmn=new MaterialMovementNote();
			mmn.setServiceId(popup.getService().getCount());
			mmn.setCompanyId(popup.getService().getCompanyId());		
			mmn.setMmnDate(new Date());
		    mmn.setBranch(popup.getService().getBranch());
			mmn.setMmnTransactionType("TransferOUT");
			mmn.setTransDirection("TRANSFEROUT");
			mmn.setEmployee(popup.getService().getEmployee());
			mmn.setMmnPersonResposible(LoginPresenter.loggedInUser);
			mmn.setApproverName(LoginPresenter.loggedInUser);
			mmn.setCreatedBy(LoginPresenter.loggedInUser);
			mmn.setStatus(MaterialMovementNote.REQUESTED);
			mmn.setMmnTitle("Service Id : "+ popup.getService().getCount()  +"-"+popup.getService().getEmployee()+"");
			mmn.setCreationDate(new Date());
			System.out.println("TransToStorBin******************"+popup.getOlbstorageBin().getValue(popup.getOlbstorageBin().getSelectedIndex()));
			mmn.setTransToStorBin(popup.getOlbstorageBin().getValue(popup.getOlbstorageBin().getSelectedIndex()));
			System.out.println("TransToStorLoc*****************"+popup.getOlbStorageLocation().getValue(popup.getOlbStorageLocation().getSelectedIndex()));
			mmn.setTransToStorLoc(popup.getOlbStorageLocation().getValue(popup.getOlbStorageLocation().getSelectedIndex()));
			System.out.println("TransToWareHouse*************"+popup.getOlbWarehouseName().getValue());
			mmn.setTransToWareHouse(popup.getOlbWarehouseName().getValue());
			System.out.println("from popup product*************"+popup.getMaterialProductTable().getDataprovider().getList().toString());
		    ArrayList<MaterialProduct> list = new ArrayList<MaterialProduct>();
		    for(ProductGroupList m : popup.getMaterialProductTable().getDataprovider().getList()){
		    	MaterialProduct mp = new MaterialProduct();
		    	Company c = new Company();
		    	mp.setCompanyId(c.getCompanyId());
		    	mp.setMaterialProductId(m.getProduct_id());
		    	mp.setMaterialProductCode(m.getCode());
		    	mp.setMaterialProductName(m.getName());
		    
		    	mp.setMaterialProductStorageBin(m.getParentStorageBin());
		    	mp.setMaterialProductStorageLocation(m.getParentStorageLocation());
		    	mp.setMaterialProductWarehouse(m.getParentWarentwarehouse());
		    	mp.setMaterialProductUOM(m.getUnit());
		    	mp.setMaterialProductRequiredQuantity(m.getProActualQty());
		    
		    	list.add(mp);
		    }
		    System.out.println("product list*************"+list.toString());
			mmn.setSubProductTableMmn(list);
			List<Integer> serviceIdList = new ArrayList<Integer>();
			serviceIdList.add(popup.getService().getCount());
			mmn.setServiceIdList(serviceIdList);
			String id = customerId+"";
			genService.saveAndApproveDocument(mmn, id, customerName, AppConstants.MMN, new AsyncCallback<String>(){
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					popup.hidePopUp();
					GWTCAlert alert = new GWTCAlert();
					alert.alert("An Unexpected error occurred!");
				}
				@Override
				public void onSuccess(String result) {
					// TODO Auto-generated method stub
					popup.hidePopUp();
					GWTCAlert alert = new GWTCAlert();
					alert.alert("Service Scheduled Successfully.");
					System.out.println("Saved Successfully ");
					genasync.save(service, new AsyncCallback<ReturnFromServer>() {
						
						@Override
						public void onSuccess(ReturnFromServer result) {
							// TODO Auto-generated method stub
							System.out.println("service saved successfully.");
						}
						
						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							System.out.println("Failed.");
						}
					});
					//alert.alert("Saved successfully!");
				}			
			});
			
		  }	
		public boolean validateQuantity(double availableQty , double reqQty) {
		
				if (availableQty == 0) {
					return true;
				} else if ((availableQty - reqQty) < 0) {
					return true;
				}
			
			return false;
		 }
private void reactOnScheduleButton(){
	
	Company c = new Company();
	List<ProductGroupList> materilList = this.getMaterialProductTable().getDataprovider().getList();
	ArrayList<Integer> integerList = new ArrayList<Integer>();
	final Map<Integer , Double> map =  new HashMap<Integer , Double>();
	for(ProductGroupList prodId : materilList){
		integerList.add(prodId.getProduct_id());
		map.put(prodId.getProduct_id() , prodId.getProActualQty());
	};
	final TechnicianServiceSchedulePopup popup = this;
	genService.getProductInventoryDetails(integerList, c.getCompanyId(), new AsyncCallback<ArrayList<SuperModel>>() {
		
		@Override
		public void onSuccess(ArrayList<SuperModel> result) {
			// TODO Auto-generated method stub
			lblUpdate.setText("Schedule");
			GWTCAlert alert = new GWTCAlert();
			if(result!=null){
			String transferToWarehouseName = getOlbWarehouseName().getValue(getOlbWarehouseName().getSelectedIndex());
			String transferToLocationName = getOlbStorageLocation().getValue(getOlbStorageLocation().getSelectedIndex());
			String transferToBinName = getOlbstorageBin().getValue(getOlbstorageBin().getSelectedIndex());
			
			String parentWarehouseName = getOlbParentWarehouseName().getValue(getOlbParentWarehouseName().getSelectedIndex());
			String parentLocationName = getOlbParentStorageLocation().getValue(getOlbParentStorageLocation().getSelectedIndex());
			String parentBinName = getOlbParentstorageBin().getValue(getOlbParentstorageBin().getSelectedIndex());

			if(result.size()==0){
				alert.alert("Please Define Product Inventory Master First!");
				return;
			}
			
			double availableQuantity = 0;
			for(SuperModel model : result){
				boolean flag = false;
				boolean parentFlag = false;
				ProductInventoryView prodInventory = (ProductInventoryView) model;
				for(ProductInventoryViewDetails productDetails: prodInventory.getDetails()){
					if(productDetails.getWarehousename().trim().equals(transferToWarehouseName.trim()) &&
						productDetails.getStoragelocation().trim().equals(transferToLocationName.trim()) &&
						productDetails.getStoragebin().trim().equals(transferToBinName.trim()) ){
						flag =true;
						
					}
					if(productDetails.getWarehousename().trim().equals(parentWarehouseName.trim()) &&
							productDetails.getStoragelocation().trim().equals(parentLocationName.trim()) &&
							productDetails.getStoragebin().trim().equals(parentBinName.trim()) ){
						availableQuantity = productDetails.getAvailableqty();
						if(result.size()!=0){
							if (validateQuantity(availableQuantity , map.get(productDetails.getProdid()))) {										
								alert.alert("Insufficient available quantity in parent warehouse for product id : "+productDetails.getProdid());
								return;
							} 
						}
						parentFlag =true;
					}
				}
				if(flag==false){
					alert.alert("Please Define Product Inventory Master for TransferTo Warehouse!");
					return;
				}
				if(parentFlag==false){
					alert.alert("Please Define Product Inventory Master for Parent Warehouse!");
					return;
				}
			}
			
			
			
			service.setServiceScheduled(true);
			createMMNForReMaterial(popup);
			}else{
				alert.alert("Please Define Product Inventory Master for TransferTo Warehouse!");
				return;
			}
			
		}
		
		@Override
		public void onFailure(Throwable caught) {
			// TODO Auto-generated method stub
			lblUpdate.setText("Schedule");
		}
	});
}
private void cancelMMN(final ArrayList<MaterialMovementNote> mmnList) {
	final TechnicianServiceSchedulePopup popup1 = this;
	this.hidePopUp();
	Console.log( "mmn size :"+ mmnList.size());
	
	if(mmnList.size()>0){	
		mmnList.get(0).setMmnDescription("MMN ID ="+mmnList.get(0).getCount()+" "+"MMN status ="+mmnList.get(0).getStatus()+"\n"
			+"has been cancelled by "+LoginPresenter.loggedInUser+" with remark "+"\n"
			+"Remark ="+ " Technician MMN"+" "+"Cancellation Date ="+AppUtility.parseDate(new Date()));
		mmnList.get(0).setStatus(MaterialIssueNote.CANCELLED);
	}
	updateService.cancelDocument(mmnList.get(0), new AsyncCallback<String>() {
		@Override
		public void onFailure(Throwable caught) {
			
		}
		@Override
		public void onSuccess(String result) {
			Console.log( "mmn size :"+ mmnList.size());
			if(mmnList.size()>1){	
				mmnList.get(1).setMmnDescription("MMN ID ="+mmnList.get(1).getCount()+" "+"MMN status ="+mmnList.get(1).getStatus()+"\n"
					+"has been cancelled by "+LoginPresenter.loggedInUser+" with remark "+"\n"
					+"Remark ="+ " Technician MMN"+" "+"Cancellation Date ="+AppUtility.parseDate(new Date()));
				mmnList.get(1).setStatus(MaterialIssueNote.CANCELLED);
			
			updateService.cancelDocument(mmnList.get(1), new AsyncCallback<String>() {
				@Override
				public void onFailure(Throwable caught) {
					
				}
				@Override
				public void onSuccess(String result) {
					createMMNForReMaterial(popup1);
					
				}
			});
			}else{
				createMMNForReMaterial(popup1);
			}
		}
	});
}
@Override
public void hidePopUp() {
	// TODO Auto-generated method stub
	materialValue = "";
	super.hidePopUp();
}



	
}
