package com.slicktechnologies.client.views.composites;

import java.util.ArrayList;
import java.util.List;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.composite.CompositeInterface;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.views.contract.PaymentTermsTable;
import com.slicktechnologies.client.views.humanresource.calendar.HolidayTable;
import com.slicktechnologies.shared.common.paymentlayer.Payment;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;

public class PaymentTermsComposite extends Composite implements ClickHandler, HasValue<ArrayList<PaymentTerms>>,CompositeInterface {
	
	private FlowPanel content;
	private IntegerBox ibdays;
	private DoubleBox dopercent;
	private TextBox tbcomment;
	private Button addPaymentTerms;
	private PaymentTermsTable paymentTermsTable;
	
	public PaymentTermsComposite() {
	    content=new FlowPanel();
	    initWidget(content);
	    createTableStructure();
	    paymentTermsTable.connectToLocal();
	    //initializeHashMaps();
	    applyStyle();
}

private void createTableStructure()
{
    Grid prodgrid = new Grid(2,4);
	
	InlineLabel ptdays = new InlineLabel("Days");
	InlineLabel ptpercent = new InlineLabel("Percent");
	InlineLabel ptcomment = new InlineLabel("Comment");
	
	ibdays=new IntegerBox();
	dopercent=new DoubleBox();
	tbcomment=new TextBox();
	addPaymentTerms= new Button("Add");
	addPaymentTerms.addClickHandler(this);
	
	prodgrid.setWidget(0,0,ptdays);
	prodgrid.setWidget(1, 0,ibdays);
	prodgrid.setWidget(0,1,ptpercent);
	prodgrid.setWidget(1, 1,dopercent);
	prodgrid.setWidget(0,2,ptcomment);
	prodgrid.setWidget(1, 2,tbcomment);
	prodgrid.setWidget(0,3,new InlineLabel("Add"));
	prodgrid.setWidget(1, 3,addPaymentTerms);
	
	prodgrid.setCellPadding(3);
	prodgrid.setCellSpacing(3);
	content.add(prodgrid);
	
	paymentTermsTable =new PaymentTermsTable();
	content.add(paymentTermsTable.content);
}


	

	
	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public void onClick(ClickEvent event) {
		System.out.println("In method of payemtn terms");	
		int payTrmDays=0;
		double payTrmPercent=0;
		String payTrmComment="";
		boolean flag=true;
		
		if(tbcomment.getValue()==null||tbcomment.getValue().equals(""))
		{
			 final GWTCAlert alert = new GWTCAlert(); 
		     alert.alert("Comment cannot be empty!");
		}
		
		boolean checkTermDays=this.validatePaymentTrmDays(ibdays.getValue());
		if(checkTermDays==false)
		{
			final GWTCAlert alert = new GWTCAlert(); 
		    alert.alert("Days already defined.Please enter unique days!");
			clearPaymentTermsFields();
			flag=false;
		}
		
		
		if(ibdays.getValue()!=null&&ibdays.getValue()>=0){
			payTrmDays=ibdays.getValue();
		}
		else
		{
			final GWTCAlert alert = new GWTCAlert(); 
		    alert.alert("Days cannot be empty!");
			flag=false;
		}
		
		
		if(dopercent.getValue()!=null&&dopercent.getValue()>=0)
		{
			if(dopercent.getValue()>100){
				final GWTCAlert alert = new GWTCAlert(); 
			    alert.alert("Percent cannot be greater than 100");
				dopercent.setValue(null);
				flag=false;
			}
			else if(dopercent.getValue()==0){
				final GWTCAlert alert = new GWTCAlert(); 
			    alert.alert("Percent should be greater than 0!");
				dopercent.setValue(null);
			}
			else
			{
			payTrmPercent=dopercent.getValue();
			}
		}
		else{
			final GWTCAlert alert = new GWTCAlert(); 
		    alert.alert("Percent cannot be empty!");
		}
		
		if(tbcomment.getValue()!=null)
			payTrmComment=tbcomment.getValue();
		
		if(!payTrmComment.equals("")&&payTrmPercent!=0&&flag==true)
		{
			PaymentTerms payTerms=new PaymentTerms();
			payTerms.setPayTermDays(payTrmDays);
			payTerms.setPayTermPercent(payTrmPercent);
			payTerms.setPayTermComment(payTrmComment);
			paymentTermsTable.getDataprovider().getList().add(payTerms);
			clearPaymentTermsFields();
		}
	}
	
	public boolean validatePaymentTermPercent()
	{
		List<PaymentTerms> payTermsLis=this.paymentTermsTable.getDataprovider().getList();
		double totalPayPercent=0;
		for(int i=0;i<payTermsLis.size();i++)
		{
			totalPayPercent=totalPayPercent+payTermsLis.get(i).getPayTermPercent();
		}
		System.out.println("Validation of Payment Percent as 100"+totalPayPercent);
		
		if(totalPayPercent!=100)
		{
			return false;
		}
		
		return true;
	}
	
	public boolean validatePaymentTrmDays(int retrDays)
	{
		List<PaymentTerms> payTermsLis=this.paymentTermsTable.getDataprovider().getList();
		for(int i=0;i<payTermsLis.size();i++)
		{
			if(retrDays==payTermsLis.get(i).getPayTermDays())
			{
				return false;
			}
			if(retrDays<payTermsLis.get(i).getPayTermDays())
			{
				final GWTCAlert alert = new GWTCAlert(); 
			    alert.alert("Days should be defined in increasing order!");
				return false;
			}
		}
		return true;
	}
	
	public void clearPaymentTermsFields()
	{
		ibdays.setValue(null);
		dopercent.setValue(null);
		tbcomment.setValue("");
	}
	
	@Override
	public void setEnable(boolean status) {
		ibdays.setEnabled(status);
		dopercent.setEnabled(status);
		tbcomment.setEnabled(status);
		paymentTermsTable.setEnable(status);
	}
	
	@Override
	public ArrayList<PaymentTerms> getValue() {
		List<PaymentTerms>list=this.paymentTermsTable.getDataprovider().getList();
		ArrayList<PaymentTerms>vec=new ArrayList<PaymentTerms>();
		vec.addAll(list);
		return vec;
	}
	@Override
	public void setValue(ArrayList<PaymentTerms> value) {
		paymentTermsTable.connectToLocal();
		paymentTermsTable.getDataprovider().getList().addAll(value);
	}
	@Override
	public void setValue(ArrayList<PaymentTerms> value, boolean fireEvents) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<ArrayList<PaymentTerms>> handler) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void clear() {
		this.paymentTermsTable.connectToLocal();
	}

	public void applyStyle()
	{
		content.setWidth("98%");
	}

	
}
