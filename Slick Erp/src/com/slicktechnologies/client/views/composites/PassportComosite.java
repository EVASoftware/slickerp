package com.slicktechnologies.client.views.composites;

import java.util.Date;

import javax.management.InstanceNotFoundException;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;
import com.itextpdf.text.pdf.PdfStructTreeController.returnType;
import com.simplesoftwares.client.library.FlexForm;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.composite.CompositeInterface;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.slicktechnologies.shared.Device;
import com.slicktechnologies.shared.common.helperlayer.PassportInformation;

public class PassportComosite extends Composite implements CompositeInterface,HasValue<PassportInformation>
{

	public IntegerBox deviceId;
	public TextBox passportNumber,issuedAt;
	public DateBox issueDate,expiryDate;
    public FlexForm form ;
	
	  private FormField[][]ff;
	  private FlowPanel panel;
	  
	  
	  
	  public PassportComosite()
	  {
		  super();
			createForm();
			initWidget(panel);
	  }



	



	@Override
	public void clear() {
	    form.clear();
		
	}

	private void resetBorderColor(Widget widg)
	{
		 widg.getElement().getStyle().clearBorderColor();
	}


	@Override
	public boolean validate()
	{
		if(issueDate.getValue()!=null&&expiryDate!=null)
		{
			if(expiryDate.getValue().before(issueDate.getValue()))
			{
				
				return false;
			}
		}
		return true;
		
	}

	 /**
	  * Applies background color.Called when validation get failed
	  *
	  * @param widg the widg
	  * @param color the color
	  */
	 public void applyBorderColor(Widget widg,String color)
	 {
		 widg.getElement().getStyle().setBorderColor(color);
		 
	 }
	 

	public void applyBackgroundColor(Widget widg,String color)
	 {
		 widg.getElement().getStyle().setBackgroundColor(color);
	 }
	
	
	 public void clearBackgroundColor(Widget widg)
	 {
		 widg.getElement().getStyle().clearBackgroundColor();
	 }

	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<PassportInformation> handler)
	{
		
		return null;
	}



	@Override
	public PassportInformation getValue() 
	{
		PassportInformation information=new PassportInformation();
		information.expiryDate=expiryDate.getValue();
		information.issueDate=issueDate.getValue();
		information.passportNumber=passportNumber.getValue();
		information.issuedAt=issuedAt.getValue();
		return information;
	}



	@Override
	public void setValue(PassportInformation value)
	{
		if(value.getExpiryDate()!=null)
		  expiryDate.setValue(value.getExpiryDate());
		if(value.getIssueDate()!=null)
			issueDate.setValue(value.getIssueDate());
		issuedAt.setText(value.getIssuedAt());
		passportNumber.setText(value.passportNumber);
	}



	@Override
	public void setValue(PassportInformation value, boolean fireEvents) {
		// TODO Auto-generated method stub
		
	}
	
	private void initializewidgets()
	{
		
		passportNumber=new TextBox();
		issuedAt=new TextBox();
		issueDate=new DateBoxWithYearSelector();
		expiryDate=new DateBoxWithYearSelector();
	}	
	
	private void createForm()
	{
		initializewidgets();
		FormFieldBuilder builder;
	     FormField fpassPortNumber,fIssueAt,fIssueDate,fexpiryDate;
	     
	     builder= new FormFieldBuilder("Passport Number",passportNumber);
	     fpassPortNumber=builder.setMandatory(false).build();
	     builder=new FormFieldBuilder("Issued At",issuedAt);
	     fIssueAt=builder.setMandatory(false).build();
	     builder= new FormFieldBuilder("Issue Date",issueDate);
	     fIssueDate=builder.setMandatory(false).build();
	     builder=new FormFieldBuilder("Expiry Date",expiryDate);
	     fexpiryDate=builder.setMandatory(false).build();
	     ff=new FormField[][]
	   {
		{fpassPortNumber,fIssueAt,fIssueDate,fexpiryDate}
		};
	
	form = new FlexForm(ff,FormStyle.ROWFORM);
	panel=new FlowPanel();
	form.setWidth("98%");
	panel.add(form);
		    
   }
	
	
	
	
	@Override
	public void setEnable(boolean status) {
		form.setEnabled(status);
		}

	
	

}
