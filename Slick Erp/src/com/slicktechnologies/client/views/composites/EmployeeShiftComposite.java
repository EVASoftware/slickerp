package com.slicktechnologies.client.views.composites;

import java.util.ArrayList;

import com.google.appengine.api.datastore.KeyFactory;
import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.code.p.gwtchismes.client.GWTCGlassPanel;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RadioButton;
import com.googlecode.objectify.Key;
import com.itextpdf.text.log.SysoCounter;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FlexForm;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.CompositeInterface;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.libservice.EmployeeInfoService;
import com.simplesoftwares.client.library.libservice.EmployeeInfoServiceAsync;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.humanresource.allocation.leavecalendarallocation.AllocationResult;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.Country;
import com.slicktechnologies.shared.common.humanresourcelayer.Department;
import com.slicktechnologies.shared.common.humanresourcelayer.EmployeeGroup;
import com.slicktechnologies.shared.common.personlayer.Employee;

public class EmployeeShiftComposite extends Composite implements CompositeInterface,ChangeHandler
,HasValue<ArrayList<EmployeeInfo>>
{

	
	 private FormField[][]ff;
	 public FlexForm form ;
	  private FlowPanel panel;
	ObjectListBox<Department>olbDepartment;
	ObjectListBox<Country>olbCountry;
	ObjectListBox<Config>olbemploymentType;
	ObjectListBox<Branch>olbBranch;
	ObjectListBox<Config>olbDesignation;
	ObjectListBox<Config>olbRole;
	ObjectListBox<EmployeeGroup>olbEmployeeGroup;
	EmployeeInfoComposite employeeInformation;
	protected ArrayList<EmployeeInfo>employeeInfoKey;
	
	
	ListBox applicability;
	GenricServiceAsync impl=GWT.create(GenricService.class);
	
	
	public EmployeeShiftComposite()
	{
		initWidget();
		createForm();
		initWidget(panel);
		employeeInfoKey=new ArrayList<EmployeeInfo>();
		changeState(false);
	}
	
	public void initWidget()
	{
		
		olbCountry=new ObjectListBox<Country>();
		olbCountry.setVisibleItemCount(5);
		olbCountry.setHeight("100px");
		olbCountry.addChangeHandler(this);
		//olbCountry.setMultipleSelect(true);
		Country.makeOjbectListBoxLive(olbCountry);
	//*****************************************************************************************//
		olbBranch=new ObjectListBox<Branch>();
		olbBranch.setVisibleItemCount(5);
		olbBranch.setHeight("100px");
		olbBranch.addChangeHandler(this);
		//olbBranch.setMultipleSelect(true);
		AppUtility.makeBranchListBoxLive(olbBranch);
  //*****************************************************************************************//	
		olbDepartment=new ObjectListBox<Department>();
		olbDepartment.setVisibleItemCount(5);
		//olbDepartment.setMultipleSelect(true);
		olbDepartment.setHeight("100px");
		olbDepartment.addChangeHandler(this);
		Department.makeDepartMentListBoxLive(olbDepartment);
	//*****************************************************************************************//		
		
		olbDesignation=new ObjectListBox<Config>();
		olbDesignation.setVisibleItemCount(5);
		//olbDesignation.setMultipleSelect(true);
		olbDesignation.setHeight("100px");
		olbDesignation.addChangeHandler(this);
		AppUtility.MakeLiveConfig(olbDesignation,Screen.EMPLOYEEDESIGNATION);
	//*****************************************************************************************//		
		olbemploymentType=new ObjectListBox<Config>();
		olbemploymentType.setVisibleItemCount(5);
		//olbemploymentType.setMultipleSelect(true);
		olbemploymentType.setHeight("100px");
		olbemploymentType.addChangeHandler(this);
		AppUtility.MakeLiveConfig(olbemploymentType,Screen.EMPLOYEETYPE);
				

//**************************************************************************************//		

//*****************************************************************************************//
		olbRole=new ObjectListBox<Config>();
		olbRole.setVisibleItemCount(5);
		//olbRole.setMultipleSelect(true);
		olbRole.setHeight("100px");
		olbRole.addChangeHandler(this);
		AppUtility.MakeLiveConfig(olbRole,Screen.EMPLOYEEROLE);
//**************************************************************************************//	
		olbEmployeeGroup=new ObjectListBox<EmployeeGroup>();
		olbEmployeeGroup.setVisibleItemCount(5);
		//olbEmployeeGroup.setMultipleSelect(true);
		olbEmployeeGroup.setHeight("100px");
		olbEmployeeGroup.addChangeHandler(this);
		EmployeeGroup.makeObjectListBoxLive(olbEmployeeGroup);
//**************************************************************************************//	
		applicability=new ListBox();
		applicability.addItem("--SELECT--");
		applicability.addItem("All");
		applicability.addItem("Country");
		applicability.addItem("Department");
		applicability.addItem("Branch");
		applicability.addItem("Designation");
		applicability.addItem("Employment Type");
		//applicability.addItem("Employmee Group");
		applicability.addChangeHandler(this);
		
		
		//Making Department live
		
		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new Employee());
		employeeInformation=AppUtility.employeeInfoComposite(new EmployeeInfo(),true);
		
		
		
		
	}
	
	public void createForm()
	{
		FormFieldBuilder builder;
	    
	     
	     builder= new FormFieldBuilder("Aplicable To",applicability);
	     FormField faplicability=builder.setMandatory(false).build();
	     builder= new FormFieldBuilder("Branch",olbBranch);
	     FormField fbranch=builder.setMandatory(false).build();
		
	     builder=new FormFieldBuilder("Department",olbDepartment);
	     FormField fdepartment=builder.build();
	     
	     builder=new FormFieldBuilder("Designation",olbDesignation);
	     FormField fdesignation=builder.build();
	     
	     builder=new FormFieldBuilder("Employee Type",olbemploymentType);
	     FormField fempltype=builder.build();
	     
	     builder=new FormFieldBuilder("Country",olbCountry);
	     FormField fcountry=builder.build();
	     
	    
	     builder=new FormFieldBuilder("",employeeInformation);
	     FormField femployeeInformation=builder.setColSpan(4).build();
	     
	     FormField fEmployeeInformation=builder.setlabel("").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
	    
	     
	     
	     ff=new FormField[][]
	    			{
	    				{faplicability},
	    				{fcountry,fbranch,fdepartment},
	    				{fdesignation,fempltype},
	    				{fEmployeeInformation},
	    				
	    				
	    		    };
	    			
	    			form = new FlexForm(ff,FormStyle.ROWFORM);
	    			panel=new FlowPanel();
	    			form.setWidth("98%");
	    			panel.add(form);
	     
	     
	     
	}
	
	@Override
	public void setEnable(boolean status) {
		form.setEnabled(status);
		if(status==true)
		   setStateAsPerSelection();
		
	}

	@Override
	public void clear() {
		form.validate();
		
	}

	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public void onChange(ChangeEvent event) {
		//Clear Previous Values in Table
		
		employeeInfoKey=new ArrayList<EmployeeInfo>();
		if(event.getSource()==applicability)
		{
		    setStateAsPerSelection();
		    if(applicability.getSelectedIndex()==1)
		    	setEmployeesOnValueChange("", null);
		    
		}
		if(event.getSource()==olbDepartment)
		{
			String department=olbDepartment.getValue();
			
			setEmployeesOnValueChange("department", department);
		}
		if(event.getSource()==olbBranch)
			setEmployeesOnValueChange("branch", olbBranch.getValue());
		if(event.getSource()==olbCountry)
			setEmployeesOnValueChange("country", olbCountry.getValue());
		if(event.getSource()==olbDesignation)
			setEmployeesOnValueChange("designation", olbDesignation.getValue());
		if(event.getSource()==olbemploymentType)
			setEmployeesOnValueChange("employeeType", olbemploymentType.getValue());
		
	}
	
	private void changeState(boolean state)
	{
		
		olbDepartment.setEnabled(state);
		olbBranch.setEnabled(state);
		olbDesignation.setEnabled(state);
		olbCountry.setEnabled(state);
		olbemploymentType.setEnabled(state);
		olbRole.setEnabled(state);
		olbEmployeeGroup.setEnabled(state);
		employeeInformation.setEnable(state);
		employeeInformation.clear();
		olbDepartment.setSelectedIndex(0);
		olbBranch.setSelectedIndex(0);
		olbDesignation.setSelectedIndex(0);
		olbCountry.setSelectedIndex(0);
		olbemploymentType.setSelectedIndex(0);
		olbRole.setSelectedIndex(0);
		olbEmployeeGroup.setSelectedIndex(0);
	}
	
	private void setStateAsPerSelection()
	{
		int index=this.applicability.getSelectedIndex();
		if(index!=0)
		{
			
			int sel=this.applicability.getSelectedIndex();
			String selectedItem=this.applicability.getItemText(sel);
			System.out.println("Selected item----- "+selectedItem);
			//For Company
			if(sel==1)
			{
				changeState(false);
			}
			
			//For Branch
			if(selectedItem.equals("Country"))
			{
				changeState(false);
				olbCountry.setEnabled((true));
			}
			
			if(selectedItem.equals("Branch"))
			{
				changeState(false);
				olbBranch.setEnabled((true));
			}
			
			//For Department
			if(selectedItem.equals("Department")==true)
			{
				changeState(false);
				olbDepartment.setEnabled((true));
				
				
			}
			//For Designation
			if(selectedItem.equals("Designation")==true)
			{
				changeState(false);
				olbDesignation.setEnabled((true));
				
			}
			//For Type
			if(selectedItem.equals("Employment Type")==true)
			{
				changeState(false);
				olbemploymentType.setEnabled((true));
				
			}
			//For Group
			if(selectedItem.equals("Employment  Group")==true)
			{
				changeState(false);
				olbEmployeeGroup.setEnabled(true);
				
			}
			
			
				
		}
		
		else
		{
			
			changeState(false);
		}
	}

	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<ArrayList<EmployeeInfo>> handler) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<EmployeeInfo> getValue() {
		// TODO Auto-generated method stub
		return this.employeeInfoKey;
	}

	@Override
	public void setValue(ArrayList<EmployeeInfo> value) {
		if(value!=null)
			employeeInfoKey=value;
		
	}

	@Override
	public void setValue(ArrayList<EmployeeInfo> value, boolean fireEvents) {
		// TODO Auto-generated method stub
		
	}
	
	public void setEmployeesOnValueChange(String cond,String value)
	{
		MyQuerry myQuerry=new MyQuerry();
		employeeInfoKey=new ArrayList<EmployeeInfo>();
		myQuerry.setQuerryObject(new EmployeeInfo());
		if(value!=null)
		{
	    Filter filter=new Filter();
	    filter.setQuerryString(cond);
	    filter.setStringValue(value);
	    myQuerry.getFilters().add(filter);
	    }
	    final GWTCGlassPanel panel=new GWTCGlassPanel();
	    panel.show();
	    
	    impl.getSearchResult(myQuerry, new AsyncCallback<ArrayList<SuperModel>>() {

			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC failed!!"+caught);
				panel.hide();
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
								for(SuperModel temp:result)
				{
					EmployeeInfo empinfo=(EmployeeInfo) temp;
					AllocationResult res=new AllocationResult();
					res.setInfo(empinfo);
					employeeInfoKey.add(empinfo);
					
				}
				
				panel.hide();
			}
		});
		
	}

	public FormField[][] getFf() {
		return ff;
	}

	public void setFf(FormField[][] ff) {
		this.ff = ff;
	}

	public FlexForm getForm() {
		return form;
	}

	public void setForm(FlexForm form) {
		this.form = form;
	}

	public FlowPanel getPanel() {
		return panel;
	}

	public void setPanel(FlowPanel panel) {
		this.panel = panel;
	}

	public ObjectListBox<Department> getOlbDepartment() {
		return olbDepartment;
	}

	public void setOlbDepartment(ObjectListBox<Department> olbDepartment) {
		this.olbDepartment = olbDepartment;
	}

	public ObjectListBox<Country> getOlbCountry() {
		return olbCountry;
	}

	public void setOlbCountry(ObjectListBox<Country> olbCountry) {
		this.olbCountry = olbCountry;
	}

	public ObjectListBox<Config> getOlbemploymentType() {
		return olbemploymentType;
	}

	public void setOlbemploymentType(ObjectListBox<Config> olbemploymentType) {
		this.olbemploymentType = olbemploymentType;
	}

	public ObjectListBox<Branch> getOlbBranch() {
		return olbBranch;
	}

	public void setOlbBranch(ObjectListBox<Branch> olbBranch) {
		this.olbBranch = olbBranch;
	}

	public ObjectListBox<Config> getOlbDesignation() {
		return olbDesignation;
	}

	public void setOlbDesignation(ObjectListBox<Config> olbDesignation) {
		this.olbDesignation = olbDesignation;
	}

	public ObjectListBox<Config> getOlbRole() {
		return olbRole;
	}

	public void setOlbRole(ObjectListBox<Config> olbRole) {
		this.olbRole = olbRole;
	}

	public ObjectListBox<EmployeeGroup> getOlbEmployeeGroup() {
		return olbEmployeeGroup;
	}

	public void setOlbEmployeeGroup(ObjectListBox<EmployeeGroup> olbEmployeeGroup) {
		this.olbEmployeeGroup = olbEmployeeGroup;
	}

	public EmployeeInfoComposite getEmployeeInformation() {
		return employeeInformation;
	}

	public void setEmployeeInformation(EmployeeInfoComposite employeeInformation) {
		this.employeeInformation = employeeInformation;
	}

	public ArrayList<EmployeeInfo> getEmployeeInfoKey() {
		return employeeInfoKey;
	}

	public void setEmployeeInfoKey(ArrayList<EmployeeInfo> employeeInfoKey) {
		this.employeeInfoKey = employeeInfoKey;
	}

	public ListBox getApplicability() {
		return applicability;
	}

	public void setApplicability(ListBox applicability) {
		this.applicability = applicability;
	}

	public GenricServiceAsync getImpl() {
		return impl;
	}

	public void setImpl(GenricServiceAsync impl) {
		this.impl = impl;
	}
	
	public MyQuerry getSelectedQuerry()
	{
		MyQuerry querry=new MyQuerry();
		Filter filterStatus=new Filter();
		Filter filterName=new Filter();
		if(applicability.getSelectedIndex()!=0)
		{
			
			int index=applicability.getSelectedIndex();
			String selectedItem=applicability.getItemText(index);
			//For Branch
			if(selectedItem.equals("Country"))
			{
				
				filterStatus.setQuerryString("status");
				filterStatus.setBooleanvalue(true);
				
				if(olbCountry.getValue()!=null)
				{
				  
				String selValue=olbCountry.getValue();
				filterName.setQuerryString("countryName");
				filterName.setStringValue(selValue);
				querry.getFilters().add(filterName);
				
				}
				querry.setQuerryObject(new Country());
			}
			
			if(selectedItem.equals("Branch"))
			{
				filterStatus.setQuerryString("status");
				filterStatus.setBooleanvalue(true);
				querry.setQuerryObject(new Branch());
				if(olbBranch.getValue()!=null)
				{
				  
				String selValue=olbBranch.getValue();
				filterName.setQuerryString("buisnessUnitName");
				filterName.setStringValue(selValue);
				querry.getFilters().add(filterName);
				
				}
			}
			
			//For Department
			if(selectedItem.equals("Department")==true)
			{
				filterStatus.setQuerryString("status");
				filterStatus.setBooleanvalue(true);
				querry.setQuerryObject(new Department());
				if(olbDepartment.getValue()!=null)
				{
				  
				String selValue=olbDepartment.getValue();
				filterName.setQuerryString("deptName");
				filterName.setStringValue(selValue);
				querry.getFilters().add(filterName);
				
				}
				
				
			}
			
			if(selectedItem.equals("All"))
			{
				querry.setQuerryObject(new Company());
			}
			
			//querry.getFilters().add(filterStatus);	
			return querry;
		}
		
		
		
		return null;
		
	}

	
	

}
