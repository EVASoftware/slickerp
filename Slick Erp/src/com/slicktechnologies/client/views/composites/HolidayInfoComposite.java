package com.slicktechnologies.client.views.composites;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.CompositeInterface;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.views.humanresource.calendar.HolidayTable;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Holiday;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.HolidayType;

public class HolidayInfoComposite extends Composite implements ClickHandler, HasValue<ArrayList< Holiday>>,CompositeInterface {
	
private FlowPanel content;
	
	private Button addButton;
	private HolidayTable holidayTable;
	private TextBox tbholidayname;
	private ObjectListBox<HolidayType> olbHolidayType;
	private DateBox dbholidaydate;
	DateTimeFormat df =  DateTimeFormat.getFormat("dd/MM/yyyy");
	
	
	
	public HolidayInfoComposite() {
		    content=new FlowPanel();
		    initWidget(content);
		    
		    createTableStructure();
		    holidayTable.connectToLocal();
		    //initializeHashMaps();
		    applyStyle();
	}
	
	private void createTableStructure()
	{
        Grid prodgrid = new Grid(2,4);
		
		InlineLabel hname = new InlineLabel("Holiday Name");
		InlineLabel htype = new InlineLabel("Holiday Type");
		InlineLabel hdate = new InlineLabel("Holiday Date");
		
		tbholidayname=new TextBox();
		olbHolidayType=new ObjectListBox<HolidayType>();
		this.initalizeHolidayTypeListBox();
		dbholidaydate=new DateBox();
		dbholidaydate.setFormat(new DateBox.DefaultFormat(df));
		addButton= new Button("Add");
		addButton.addClickHandler(this);
		
		prodgrid.setWidget(0,0,hname);
		prodgrid.setWidget(1, 0,tbholidayname);
		prodgrid.setWidget(0,1,htype);
		prodgrid.setWidget(1, 1,olbHolidayType);
		prodgrid.setWidget(0,2,hdate);
		prodgrid.setWidget(1, 2,dbholidaydate);
		prodgrid.setWidget(0,3,new InlineLabel("Add"));
		prodgrid.setWidget(1, 3,addButton);
		
		prodgrid.setCellPadding(3);
		prodgrid.setCellSpacing(3);
		content.add(prodgrid);
		
		holidayTable =new HolidayTable();
		content.add(holidayTable.content);
	}

	@Override
	public void onClick(ClickEvent event) {
			
			 
		
		     Holiday cal=new Holiday();
			 String a=this.tbholidayname.getValue();
			 String b=this.olbHolidayType.getValue();
			 Date c=this.dbholidaydate.getValue();
			 
			 
			 Date convDate=df.parse(df.format(c));
			 cal.setHolidayname(a);
			 cal.setHolidayType(b);
			 cal.setHolidaydate(convDate);
			 System.out.println("Date val "+convDate);
			 
			 if(holidayTable.validate(cal)==false)
			 {
				 final GWTCAlert alert = new GWTCAlert(); 
			     alert.alert("Please add Valid Holiday\n.Holiday name and date should be unique!");
			     return;
			 }
			 
			 // To clear data from fields after Add button is pressed
			 tbholidayname.setText("");
			 dbholidaydate.setValue(null);
			 olbHolidayType.setSelectedIndex(0);
			 
			 //To check if any value is null then it should not be added to the table
			 if(a.equals("") || b.equals("") || c.equals(""))
			 {
				 System.out.println("dip");
			 }
			 else
			 {
   			 this.holidayTable.getDataprovider().getList().add(cal);
   			 
			 }		
	}
	
	@Override
	public void setEnable(boolean status) 
	{
		tbholidayname.setEnabled(status);
		olbHolidayType.setEnabled(status);
		dbholidaydate.setEnabled(status);
		holidayTable.setEnabled(status);
	}

	public void clear()
	{
		this.holidayTable.connectToLocal();
	}

	 
	public boolean validate()
	{
		if(this.holidayTable.getDataprovider().getList().size()==0)
			return false;
		else
			return true;
	}

	
	
	public void applyStyle()
	{
		content.setWidth("98%");
	}


	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<ArrayList< Holiday>> handler) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList< Holiday> getValue() {
		List<Holiday>list=this.holidayTable.getDataprovider().getList();
		ArrayList<Holiday>vec=new ArrayList<Holiday>();
		vec.addAll(list);
		return vec;
		
	}

	@Override
	public void setValue(ArrayList< Holiday> value) {
		holidayTable.connectToLocal();
		holidayTable.getDataprovider().getList().addAll(value);
		
	}

	@Override
	public void setValue(ArrayList< Holiday> value, boolean fireEvents) {
		// TODO Auto-generated method stub
		
	}
	
	protected void initalizeHolidayTypeListBox()
	{
		MyQuerry querry = new MyQuerry();
		querry.setQuerryObject(new HolidayType());
		olbHolidayType.MakeLive(querry);
	}
	

	/***********************************Getters and Setters**********************************************/
	
	
	public HolidayTable getHolidayTable() {
		return holidayTable;
	}

	public void setHolidayTable(HolidayTable holidayTable) {
		this.holidayTable = holidayTable;
	}

	public ObjectListBox<HolidayType> getOlbHolidayType() {
		return olbHolidayType;
	}

	public void setOlbHolidayType(ObjectListBox<HolidayType> olbHolidayType) {
		this.olbHolidayType = olbHolidayType;
	}

	public TextBox getTbholidayname() {
		return tbholidayname;
	}

	public void setTbholidayname(TextBox tbholidayname) {
		this.tbholidayname = tbholidayname;
	}

	
	public DateBox getDbholidaydate() {
		return dbholidaydate;
	}

	public void setDbholidaydate(DateBox dbholidaydate) {
		this.dbholidaydate = dbholidaydate;
	}

	
}
