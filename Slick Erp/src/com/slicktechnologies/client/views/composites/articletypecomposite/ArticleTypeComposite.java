package com.slicktechnologies.client.views.composites.articletypecomposite;

import java.util.ArrayList;
import java.util.List;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.composite.CompositeInterface;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.paymentmodes.companypayment.CompanyPaymentForm;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Type;

public class ArticleTypeComposite extends Composite implements ClickHandler, HasValue<ArrayList<ArticleType>>,CompositeInterface {

	
	private FlowPanel content;
	private Button addButton;
	private ArticleTypeTable articletypetable;
	private ObjectListBox<Config>olbarticletypename;
	private TextBox articletypevalue;
	private TextBox articleDescription;
	private ListBox articleListBox;
	
	private Object form;
	/**
	 * Date : 13-09-2017 By JAYSHREE
	 * Old COde
	 */
//	private ObjectListBox<Type> olbDocumentName;
	
	/**
	 * New COde
	 */
//	private ObjectListBox<String> olbDocumentName;
	
	ListBox lstDocumentName;
	
	/**
	 * End
	 */
	
	public ArticleTypeComposite() {
		
	    content=new FlowPanel();
	    initWidget(content);
	    createTableStructure();
	    articletypetable.connectToLocal();
	    applyStyle();
}
	
	private void createTableStructure()
	{
        Grid ArticleGrid = new Grid(2,6);
		
    	InlineLabel lbldocumentname = new InlineLabel("Document Name");
    	lbldocumentname.setTitle("Select the document i.e quotation, contract on which you would like to print the article i.e. GSTIN, PAN, CIN number");
		InlineLabel lblarticletype = new InlineLabel("Article Type");
		lblarticletype.setTitle("Type of article i.e. TIN,CIN, GSTIN, PAN, MSME");
		InlineLabel lblarticletypevalue = new InlineLabel("Article Number");
		lblarticletypevalue.setTitle("Article id");
		InlineLabel lblarticledescription = new InlineLabel("Description (Max 30 chs)");
		InlineLabel lblarticlelistbox = new InlineLabel("Print");
		lblarticlelistbox.setTitle("select this to print on that document");
		
		
		olbarticletypename=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbarticletypename, Screen.ARTICLETYPE);
		articletypevalue=new TextBox();
		articletypevalue.getElement().getStyle().setHeight(13, Unit.PX);
		articleDescription = new TextBox();
		articleDescription.getElement().getStyle().setHeight(13, Unit.PX);
		articleListBox = new ListBox();
		articleListBox=new ListBox();
		articleListBox.addItem("SELECT");
		articleListBox.addItem(AppConstants.NO);
		articleListBox.addItem(AppConstants.YES);
		olbarticletypename.setTitle("Type of article i.e. TIN,CIN, GSTIN, PAN, MSME");
		articletypevalue.setTitle("Article id");
		articleListBox.setTitle("select this to print on that document");
		
		
//		olbDocumentName=new ObjectListBox<Type>();
//		AppUtility.makeTypeListBoxLive(olbDocumentName, Screen.DOCUMENTNAME);
		/**
		 * Date : 28/8/2017 Added by jayshree to load hard Coded Value in
		 * document name drop down menu
		 */
		lstDocumentName = new ObjectListBox<String>();
		lstDocumentName.addItem("--SELECT--");
		lstDocumentName.addItem("Invoice Details");
		lstDocumentName.addItem("Contract");
		lstDocumentName.addItem("Service");
		lstDocumentName.addItem("Quotation");
		lstDocumentName.addItem("Sales Order");
		lstDocumentName.addItem("DeliveryNote");
		lstDocumentName.addItem("Work Order");
		lstDocumentName.addItem("Quick Contract");
		lstDocumentName.addItem("ServiceInvoice");
		lstDocumentName.addItem("SalesInvoice");
		lstDocumentName.addItem("printservice");
		lstDocumentName.addItem("SalesInvoice");
		lstDocumentName.addItem("Employee");
		lstDocumentName.addItem("Fumigation");
		lstDocumentName.addItem("Purchase Order");
		lstDocumentName.addItem("ContractRenewal");
		lstDocumentName.addItem("ServiceCertificate");
		/**Date 18-9-2020 by Amol added Salesquotation**/
		lstDocumentName.addItem("SalesQuotation");
		/**
		 * Date : 30-11-2018 BY ANIL
		 */
		lstDocumentName.addItem("Payslip");
		
		lstDocumentName.setTitle("Select the document i.e quotation, contract on which you would like to print the article i.e. GSTIN, PAN, CIN number");
		
		addButton= new Button("Add");
		addButton.addClickHandler(this);
		
		
		ArticleGrid.setWidget(0,0,lbldocumentname);
		ArticleGrid.setWidget(1, 0,lstDocumentName);
		ArticleGrid.setWidget(0,1,lblarticletype);
		ArticleGrid.setWidget(1, 1,olbarticletypename);
		ArticleGrid.setWidget(0,2,lblarticletypevalue);
		ArticleGrid.setWidget(1, 2,articletypevalue);
		ArticleGrid.setWidget(0,3,lblarticledescription);
		ArticleGrid.setWidget(1, 3,articleDescription);
		ArticleGrid.setWidget(0, 4, lblarticlelistbox);
		ArticleGrid.setWidget(1, 4, articleListBox);
		ArticleGrid.setWidget(0, 5,new InlineLabel("Add"));
		ArticleGrid.setWidget(1, 5,addButton);
		
		ArticleGrid.setCellPadding(5);
		ArticleGrid.setCellSpacing(5);
		content.add(ArticleGrid);
		CompanyPaymentForm.isCallFromPaymentMode=false;
		articletypetable =new ArticleTypeTable();
		content.add(articletypetable.content);
	}
	
	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<ArrayList<ArticleType>> handler) {
		return null;
	}

	@Override
	public void setEnable(boolean status) {
		lstDocumentName.setEnabled(status);
		olbarticletypename.setEnabled(status);
		articletypevalue.setEnabled(status);
		articleDescription.setEnabled(status);
		articleListBox.setEnabled(status);
		articletypetable.setEnabled(status);
		addButton.setEnabled(status);
		
	}

	@Override
	public void clear() {
		this.articletypetable.connectToLocal();
		this.articletypevalue.setText("");
		this.articleDescription.setText("");
		this.articleListBox.setSelectedIndex(0);
		this.olbarticletypename.setSelectedIndex(0);
		this.lstDocumentName.setSelectedIndex(0);
		
	}

	public void applyStyle()
	{
		content.setWidth("98%");
	}
	
	@Override
	public boolean validate() {
		return true;
	}

	@Override
	public ArrayList<ArticleType> getValue() {
		List<ArticleType>list=this.articletypetable.getDataprovider().getList();
		ArrayList<ArticleType>vec=new ArrayList<ArticleType>();
		vec.addAll(list);
		return vec;
	}

	@Override
	public void setValue(ArrayList<ArticleType> value) {
		articletypetable.connectToLocal();
		articletypetable.getDataprovider().getList().addAll(value);
		
	}

	@Override
	public void setValue(ArrayList<ArticleType> value, boolean fireEvents) {
		
	}

	@Override
	public void onClick(ClickEvent event) {
		
		 String ArticleType=olbarticletypename.getValue();
		 if(ArticleType==null)
		 {
			 final GWTCAlert alert = new GWTCAlert(); 
		     alert.alert("Article type Cannot be empty !");
		     return;  
		 }
		 
		 String articletypvalue=this.articletypevalue.getText().trim();
		 if(articletypvalue.equals(""))
		 {
			 final GWTCAlert alert = new GWTCAlert(); 
		     alert.alert("Article Type Value Cannot be empty !");
		     return; 
		 }
		 
		 if(lstDocumentName.getSelectedIndex()==0)
		 {
			 final GWTCAlert alert = new GWTCAlert(); 
		     alert.alert("Document Name is Mandatory!");
		     return; 
		 }
		
		 if(articleListBox.getSelectedIndex()==0)
		 {
			 final GWTCAlert alert = new GWTCAlert(); 
		     alert.alert("Print is Mandatory !");
		     return; 
		 }
		 
		 /**
		  * Date 26-07-2018 By Vijay
		  * Des:- GSTIN Number can not be greater than 15 characters Validation
		  */
		 if(ArticleType.equals("GSTIN")){
				 final GWTCAlert alert = new GWTCAlert(); 
				 if(articletypvalue.length()>15 || articletypvalue.length()<15){
					 alert.alert("GSTIN number must be 15 characters!");
					 return;  
				 }
		 }
		 /**
		  * ends here
		  */
		 
		 String articledecsription=this.articleDescription.getText().trim();
		 String ArticlePrint = articleListBox.getItemText(articleListBox.getSelectedIndex());
		 
		 
		 if(validateProcess()==true){
			 addToArticleTypeTable();
		}
	}
	

	
		protected void addToArticleTypeTable() {
			
			ArticleType Articletypedetails = new ArticleType();
		  
			 Articletypedetails.setDocumentName(lstDocumentName.getValue(lstDocumentName.getSelectedIndex()));
			 Articletypedetails.setArticleTypeName(olbarticletypename.getValue());
			 Articletypedetails.setArticleTypeValue(articletypevalue.getText().trim());
			 Articletypedetails.setArticleDescription(this.articleDescription.getText().trim());
			 Articletypedetails.setArticlePrint(articleListBox.getItemText(articleListBox.getSelectedIndex()));
				if(olbarticletypename.getValue()!=null && articletypevalue.getText().trim()!=null )
				{
					this.articletypetable.getDataprovider().getList().add(Articletypedetails);
					articletypevalue.setText("");
					articleDescription.setText("");
					olbarticletypename.setSelectedIndex(0);
					articleListBox.setSelectedIndex(0);
					lstDocumentName.setSelectedIndex(0);
				}
		}

		public boolean validateProcess() {
		List<ArticleType> ArticleLis=articletypetable.getDataprovider().getList();
		if(ArticleLis.size()!=0){
			for(int i=0;i<ArticleLis.size();i++)
			{
				if(ArticleLis.get(i).getArticleTypeName().trim().equals(olbarticletypename.getValue().trim())&&ArticleLis.get(i).getDocumentName().trim().equals(lstDocumentName.getValue(lstDocumentName.getSelectedIndex()).trim()))
				{
					if(ArticleLis.get(i).getArticleDescription()!=null && articletypevalue.getText()!=null )
					{
						if(!ArticleLis.get(i).getArticleDescription().equals(articleDescription.getText()) && !ArticleLis.get(i).getArticleTypeValue().equals(articletypevalue.getText())){
							
						 return true;
						
						}
					else{
						final GWTCAlert alert = new GWTCAlert(); 
					    alert.alert("Document Name and Article Type already added!");
						return false;
					}
				}		
					else{
					 final GWTCAlert alert = new GWTCAlert(); 
				     alert.alert("Document Name and Article Type already added!");
					 return false;
					}
					
				}
			}
		}
		return true;
	}

	public void showDialogMessage(String string) {
		
	}

	
	/**************************************Getters And Setters***************************************/
	
	
	public FlowPanel getContent() {
		return content;
	}

	public void setContent(FlowPanel content) {
		this.content = content;
	}

	public Button getAddButton() {
		return addButton;
	}

	public void setAddButton(Button addButton) {
		this.addButton = addButton;
	}

	public ArticleTypeTable getArticletypetable() {
		return articletypetable;
	}

	public void setArticletypetable(ArticleTypeTable articletypetable) {
		this.articletypetable = articletypetable;
	}

	public ObjectListBox<Config> getOlbarticletypename() {
		return olbarticletypename;
	}

	public void setOlbarticletypename(ObjectListBox<Config> olbarticletypename) {
		this.olbarticletypename = olbarticletypename;
	}

	public TextBox getArticletypevalue() {
		return articletypevalue;
	}

	public void setArticletypevalue(TextBox articletypevalue) {
		this.articletypevalue = articletypevalue;
	}

	public TextBox getArticleDescription() {
		return articleDescription;
	}

	public void setArticleDescription(TextBox articleDescription) {
		this.articleDescription = articleDescription;
	}

	public ListBox getArticleListBox() {
		return articleListBox;
	}

	public void setArticleListBox(ListBox articleListBox) {
		this.articleListBox = articleListBox;
	}

	public Object getForm() {
		return form;
	}

	public void setForm(Object form) {
		this.form = form;
	}

//	public ObjectListBox<Type> getOlbDocumentName() {
//		return olbDocumentName;
//	}
//
//	public void setOlbDocumentName(ObjectListBox<Type> olbDocumentName) {
//		this.olbDocumentName = olbDocumentName;
//	}
	
//	public ObjectListBox<String> getOlbDocumentName() {
//		return olbDocumentName;
//	}
//
//	public void setOlbDocumentName(ObjectListBox<String> olbDocumentName) {
//		this.olbDocumentName = olbDocumentName;
//	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
