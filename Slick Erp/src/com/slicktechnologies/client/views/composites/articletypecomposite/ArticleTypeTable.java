package com.slicktechnologies.client.views.composites.articletypecomposite;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.ProvidesKey;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.paymentmodes.companypayment.CompanyPaymentForm;
import com.slicktechnologies.shared.common.articletype.ArticleType;

public class ArticleTypeTable extends SuperTable<ArticleType>{

	TextColumn<ArticleType> ArticleTypeNameColumn;
	TextColumn<ArticleType> ArticleTypeValueColumn;
	TextColumn<ArticleType> ArticleDescriptionColumn;
	TextColumn<ArticleType> ArticlePrintColumn;
	TextColumn<ArticleType> ArticleDocumentColumn;
	private Column<ArticleType, String> delete;
	
	
	
	public ArticleTypeTable() {
		super();
	}
	@Override
	public void createTable() {
		Console.log("in create table isCallFromPaymentMode="+CompanyPaymentForm.isCallFromPaymentMode);
		if(!CompanyPaymentForm.isCallFromPaymentMode){
			addColumnArticleTypeDocumentName();
			addColumnArticleTypeName();
			addColumnArticleTypevalue();
			addColumnArticleTypeDescription();
			addColumnArticleTypePrint();
		}else{
			addColumnPaymentTypeName();
			addColumnPaymentDetailsvalue();
		}
		addColumnDelete();
		setFieldUpdaterOnDelete();
	    table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);

	}
	
	public void addColumnArticleTypeDocumentName()
	{
		ArticleDocumentColumn = new TextColumn<ArticleType>() {
			@Override
			public String getValue(ArticleType object) {
				return object.getDocumentName();
			}
		};
		table.addColumn(ArticleDocumentColumn,"Document Name");
	}
	
	public void addColumnArticleTypeName()
	{
		ArticleTypeNameColumn = new TextColumn<ArticleType>() {
			@Override
			public String getValue(ArticleType object) {
				return object.getArticleTypeName();
			}
		};
		table.addColumn(ArticleTypeNameColumn,"Article Type");
	}
	
	
	public void addColumnArticleTypevalue()
	{
		 ArticleTypeValueColumn = new TextColumn<ArticleType>() {
			@Override
			public String getValue(ArticleType object) {
				return object.getArticleTypeValue();
			}
		};
		table.addColumn( ArticleTypeValueColumn,"Article Value");
	}
	
	public void addColumnPaymentTypeName()
	{
		ArticleTypeNameColumn = new TextColumn<ArticleType>() {
			@Override
			public String getValue(ArticleType object) {
				return object.getArticleTypeName();
			}
		};
		table.addColumn(ArticleTypeNameColumn,"Payment Type");
	}
	
	
	public void addColumnPaymentDetailsvalue()
	{
		 ArticleTypeValueColumn = new TextColumn<ArticleType>() {
			@Override
			public String getValue(ArticleType object) {
				return object.getArticleTypeValue();
			}
		};
		table.addColumn( ArticleTypeValueColumn,"Payment details");
	}
	
	public void addColumnArticleTypeDescription()
	{
		ArticleDescriptionColumn = new TextColumn<ArticleType>() {
			@Override
			public String getValue(ArticleType object) {
				return object.getArticleDescription();
			}
		};
		table.addColumn(ArticleDescriptionColumn,"Description");
	}
	
	
	
	public void addColumnArticleTypePrint()
	{
		ArticlePrintColumn = new TextColumn<ArticleType>() {
			@Override
			public String getValue(ArticleType object) {
				return object.getArticlePrint();
			}
		};
		table.addColumn(ArticlePrintColumn,"Print");
	}

	
	
	private void addColumnDelete()
	{
		ButtonCell btnCell= new ButtonCell();
		delete = new Column<ArticleType, String>(btnCell) {

			@Override
			public String getValue(ArticleType object) {
				
				return "Delete";
			}
		};
		table.addColumn(delete,"Delete");
	}
	
	private void setFieldUpdaterOnDelete()
	{
		delete.setFieldUpdater(new FieldUpdater<ArticleType, String>() {
			
			@Override
			public void update(int index, ArticleType object, String value) {
				getDataprovider().getList().remove(index);
				table.redrawRow(index);
			}
		});
	}
	
	
	private void addeditColumn()
	{
		if(!CompanyPaymentForm.isCallFromPaymentMode){
			addColumnArticleTypeDocumentName();
			addColumnArticleTypeName();
			addColumnArticleTypevalue();
			addColumnArticleTypeDescription();
			addColumnArticleTypePrint();		
		}else{
			addColumnPaymentTypeName();
			addColumnPaymentDetailsvalue();
		}
		addColumnDelete();
		setFieldUpdaterOnDelete();
	}
	
	private void addViewColumn()
	{
		if(!CompanyPaymentForm.isCallFromPaymentMode){
			addColumnArticleTypeDocumentName();
			addColumnArticleTypeName();
			addColumnArticleTypevalue();
			addColumnArticleTypeDescription();	
			addColumnArticleTypePrint();
		}else{
			addColumnPaymentTypeName();
			addColumnPaymentDetailsvalue();
		}
			
	}
	
	public  void setEnabled(boolean state)
	{
		int tablecolcount=this.table.getColumnCount();
		for(int i=tablecolcount-1;i>-1;i--)
		  table.removeColumn(i);
		if(state ==true)
			addeditColumn();
		if(state==false)
			addViewColumn();
	}
		

	
	@Override
	protected void initializekeyprovider() {
		keyProvider = new ProvidesKey<ArticleType>() {
			@Override
			public Object getKey(ArticleType item) {
				if(item==null)
					return null;
				else
					return item.getId();
			}
		};
		
	}

	@Override
	public void addFieldUpdater() {
		
	}

	@Override
	public void setEnable(boolean state) {
		int tablecolcount=this.table.getColumnCount();
		for(int i=tablecolcount-1;i>-1;i--)
		  table.removeColumn(i);
		if(state ==true)
			addeditColumn();
		if(state==false)
			addViewColumn();
		
	}

	@Override
	public void applyStyle() {
		
	}

}
