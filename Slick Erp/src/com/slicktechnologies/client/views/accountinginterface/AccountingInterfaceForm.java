package com.slicktechnologies.client.views.accountinginterface;

import java.util.ArrayList;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreen;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.tallyaccounts.AccountingInterface;

public class AccountingInterfaceForm extends TableScreen<AccountingInterface> implements ClickHandler {

	
	public AccountingInterfaceForm(SuperTable<AccountingInterface> superTable){
		super(superTable);
		createGui();
	}
	
	@Override
	public void createScreen() {
		initializeWidget();
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("AccountingInterface", "OnlyForNBHC")){
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("AccountingInterface", "EnableShowButtonChangeStatusToCreated")){
				this.processlevelBarNames=new String[]{AppConstants.INTEGRATESYNC,AppConstants.CHANGESTATUSTOCREATED};
			}
			else{
				this.processlevelBarNames=new String[]{AppConstants.INTEGRATESYNC};
			}
		}
		else{
			this.processlevelBarNames=new String[]{"Sync","UpdateRecord","Cancel Records","Tally Export","Excel Export"}; //Ashwini Patil added "Tally Export","Excel Export" Date:16-08-2022 
		}
	}
	
	@Override
	public void updateModel(AccountingInterface model) {
		// TODO Auto-generated method stub
		super.updateModel(model);
	}
	
	public void initializeWidget(){}
	
	
	@Override
	public void updateView(AccountingInterface view) {
		// TODO Auto-generated method stub
		super.updateView(view);
	}
	
	

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
	}
	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Search")||text.equals("Download"))
				{
					/**
					 * Date 19-02-2018 By vijay added if condition for NBHC another branch data download issue we removed download from list and showing only in searchpopup
					 * and old code added in else block
					 */
					if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("AccountingInterface", "AccountingInterfaceReport")){
						if(text.equals("Download")){
							menus[k].setVisible(false);
						}else{
							menus[k].setVisible(true);
						}
					}
					else{
					
						menus[k].setVisible(true);
					}

					 
				}
				else
					menus[k].setVisible(false);  		  		
			}
		}
		
		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals("Download")){
					/**
					 * Date 19-02-2018 By vijay added if condition for NBHC another branch data download issue we removed download from list and showing only in searchpopup
					 * and old code added in else block
					 */
					if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("AccountingInterface", "AccountingInterfaceReport")){
						if(text.equals("Download")){
							menus[k].setVisible(false);
						}else{
							menus[k].setVisible(true);
						}
					}
					else{
					
						menus[k].setVisible(true);
					}
				}
				else
					menus[k].setVisible(false);  	
					
				
						   
			}
		}
		
		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Search")||text.equals("Download")){
					/**
					 * Date 19-02-2018 By vijay added if condition for NBHC another branch data download issue we removed download from list and showing only in searchpopup
					 * and old code added in else block
					 */
					if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("AccountingInterface", "AccountingInterfaceReport")){
						if(text.equals("Download")){
							menus[k].setVisible(false);
						}else{
							menus[k].setVisible(true);
						}
					}
					else{
					
						menus[k].setVisible(true);
					}

				}
				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.INTERFACETALLYLIST,LoginPresenter.currentModule.trim());
	}


	@Override
	public void retriveTable(MyQuerry querry) {
		superTable.connectToLocal();
		service.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				for(SuperModel model:result)
					superTable.getListDataProvider().getList().add((AccountingInterface) model);
//				superTable.getTable().setVisibleRange(0,result.size());
				
				
			}
			
			@Override
			public void onFailure(Throwable caught) {
			caught.printStackTrace();
				
			}
		});
		
	}

}
