package com.slicktechnologies.client.views.accountinginterface;
import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.LicenseDetails;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.tallyaccounts.AccountingInterface;

@RemoteServiceRelativePath("integrateSyncServiceImpl")
public interface IntegrateSyncService extends RemoteService{

	String callInterFace(String docType, AccountingInterface entityAI);
	
	/**
	 *Date : 09-04-2018 By Anil
	 * 
	 */
	String callLicenseUpdateInteface(Contract contract,String actionTask);
	String getIRN(Invoice invoice, String loggedInUser,String taskId,boolean bulkGenerationFlag);//Ashwini Patil Date:19-09-2022
	String cancelIRN(Invoice invoice);//Ashwini Patil Date:27-10-2022
	
	ArrayList<LicenseDetails> getLicenseInfo(Customer customer, String actionTask);
	
	String generateEinvoicesInBulk(long companyId,Date fromDate,Date toDate, ArrayList<Invoice> invoiceList,String loggedInUser);//Ashwini Patil Date:31-08-2023 for pecopp

}
