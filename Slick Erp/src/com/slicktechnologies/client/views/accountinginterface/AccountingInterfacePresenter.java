package com.slicktechnologies.client.views.accountinginterface;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.view.client.NoSelectionModel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreen;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreenPresenter;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.libservice.PdfService;
import com.simplesoftwares.client.library.libservice.PdfServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.reusabledata.ConditionDialogBox;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.UpdateService;
import com.slicktechnologies.client.services.UpdateServiceAsync;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.device.DeviceForm;
import com.slicktechnologies.client.views.device.DevicePresenter;
import com.slicktechnologies.client.views.service.ServiceForm;
import com.slicktechnologies.client.views.service.ServicePresenter;
import com.slicktechnologies.client.views.service.ServiceSearchPopUp;
import com.slicktechnologies.client.views.service.ServiceTable;
import com.slicktechnologies.shared.GeneralKeyValueBean;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.TallyInterfaceBean;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.tallyaccounts.AccountingInterface;

public class AccountingInterfacePresenter extends TableScreenPresenter<AccountingInterface> implements ClickHandler{

	TableScreen<AccountingInterface>form;
	final GenricServiceAsync async=GWT.create(GenricService.class);
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	String docType="";
	int docId=0;
	String customerRef1="";
	AccountingInterface entityAI;
	
//  rohan added this code for new synch logic on Date : 23/2/2017
	
//	AccountingInterfaceServiceAsync serviceAsync = GWT.create(AccountingInterfaceService.class);
	UpdateServiceAsync update = GWT.create(UpdateService.class);
	
	/** date 16.04.2018 added by komal for conditionpop on sync button**/
	ConditionDialogBox conditionPopup = new ConditionDialogBox("Do you really want to sync records?", AppConstants.YES, AppConstants.NO);
	PopupPanel panel;
	AccountingInterfaceDownloadPopup accDownloadPopup = new AccountingInterfaceDownloadPopup();
	
	/**
	 * @author Anil , Date : 30-05-2019
	 * flag that checks stock item wise tally entry
	 */
	boolean stockItemFlag=false;
	
	/**
	 * @author Anil , Date :04-07-2019
	 * this flag checks whether to show consolidated tax amount irrespective of tax percent
	 */
	boolean cumalativeTaxAmtFlag=false;
	
	/***
	 * Date 22-08-2019 by Vijay
	 * Des :- Vijay
	 */
	InlineLabel lblTaxInvoice;
	NumberFormat df=NumberFormat.getFormat("0.00");
	
	public AccountingInterfacePresenter(TableScreen<AccountingInterface> view,AccountingInterface model) {

		super(view, model);
		form=view;
		
		//view.retriveTable(getInterfaceTallyQuery());
//		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("AccountingInterface", "OnlyForNBHC") || AppUtility.checkForProcessConfigurartionIsActiveOrNot("AccountingInterface", "RestrictByDefaultLoadingOfList")){
//		}else{
//			view.retriveTable(getInterfaceTallyQuery());
//		}
		
		
		form.getSearchpopupscreen().getDwnload().setVisible(false);
		/**
		 * Date 19/02/2018 By vijay for NBHC download button show in searchpop to download data as per search filter selection
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("AccountingInterface", "AccountingInterfaceReport")){
			form.getSearchpopupscreen().getDwnload().setVisible(true);
		}
		/**
		 * ends here
		 */

		docType="";
		entityAI=null;
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.INTERFACETALLYLIST,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
		/** date 16.04.2018 added by komal for sync condition popup **/
		conditionPopup.getBtnOne().addClickHandler(this);
		conditionPopup.getBtnTwo().addClickHandler(this);
		accDownloadPopup.getLblOk().addClickHandler(this);
		accDownloadPopup.getLblCancel().addClickHandler(this);
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("AccountingInterface", "StockItemWiseTallyExport")){
			stockItemFlag=true;
		}
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("AccountingInterface", "ShowCumulativeTaxAmount")){
			cumalativeTaxAmtFlag=true;
		}
	}
	
	public AccountingInterfacePresenter(TableScreen<AccountingInterface> view,AccountingInterface model,MyQuerry querry) {
		super(view, model);
		form=view;
		view.retriveTable(querry);
		form.getSearchpopupscreen().getDwnload().setVisible(false);
		docType="";
		entityAI=null;
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.INTERFACETALLYLIST,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
		
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();
		
		if(text.equals(AppConstants.SYNC)){
			if(!AppUtility.checkForProcessConfigurartionIsActiveOrNot("AccountingInterface", "OnlyForNBHC")){
				System.out.println("in side SYNC record");
				/**
				 * Date : 11-04-2018 BY ANIL
				 * Checking tally licensee details
				 */
				if(AppUtility.checkLicenseStatus(AppConstants.LICENSETYPELIST.EVA_TALLY_LICENSE)){
					//reactonSync();
					System.out.println("in side SYNC record");
					/** date 16.04.2018 added by komal for sync condition popup **/
					panel = new PopupPanel(true);
					panel.add(conditionPopup);
					panel.show();
					panel.center();

				}
			}else{
				form.showDialogMessage("Please click on Integrate Sync!!");
			}
		}
		
		if(text.equals("UpdateRecord")){
			if(!AppUtility.checkForProcessConfigurartionIsActiveOrNot("AccountingInterface", "OnlyForNBHC")){
				System.out.println("in side update record");
				updateReactonSync();
			}else{
				form.showDialogMessage("Please click on Integrate Sync!!");
			}
		}
		
		if(text.equals("Cancel Delete From Tally Records")){
			if(!AppUtility.checkForProcessConfigurartionIsActiveOrNot("AccountingInterface", "OnlyForNBHC")){
				System.out.println("inside cancel records");
				cancelDeleteFromTallyRecord();
			}else{
				form.showDialogMessage("Please click on Integrate Sync!!");
			}
		}
		
		if(text.equals(AppConstants.INTEGRATESYNC)){
			/**
			 * Date 22-08-2019 by Vijay
			 * Des :- for avoiding double click
			 */
			lblTaxInvoice=label;
			lblTaxInvoice.setText("Processing...");
			lblTaxInvoice.setVisible(false);
			
			reactOnIntegrateSync();
		}
		/**
		 * Date 03-07-2019 by Vijay 
		 * Des :- for NBHC CCPM with process config enabled to change status to Created 
		 */
		if(text.equals(AppConstants.CHANGESTATUSTOCREATED)){
			reactOnChangeStatusToCreated();
		}
		/**
		 * @author 20-06-2023 
		 * Des :- if any chnages do in tally export file then do in both methods
		 * 1. setInterfaceValuesForUltra(interfacearray) and 2. setInterfaceValues(interfacearray)
		 */
		//Ashwini Patil Date:16-08-2022
		if(text.equals("Tally Export")){
			ArrayList<AccountingInterface> interfacearray=new ArrayList<AccountingInterface>();
			List<AccountingInterface> AIlist=(List<AccountingInterface>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
			
			interfacearray.addAll(AIlist);
			Console.log("searched records size="+interfacearray.size());
			ArrayList<TallyInterfaceBean> list=null;
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("AccountingInterface", "UltraTallyExport"))
			{
				list = setInterfaceValuesForUltra(interfacearray);		
			}else
				list = setInterfaceValues(interfacearray);	
			
			if(list!=null)
				Console.log("Sending list of "+list.size()+" documents for tally export");
             csvservice.setTallyInterfaceData(list, new AsyncCallback<Void>() {
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					System.out.println("RPC call Failed"+caught);
					accDownloadPopup.hidePopUp();
				}
				@Override
				public void onSuccess(Void result) {
					// TODO Auto-generated method stub
					accDownloadPopup.hidePopUp();
					String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
					if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("AccountingInterface", "UltraTallyExport"))
					{Console.log("in UltraTallyExport");
						final String url=gwt + "csvservlet"+"?type="+214;
						Window.open(url, "test", "enabled");						
					}else{
						Console.log("in default TallyExport");
						final String url=gwt + "csvservlet"+"?type="+141;
						Window.open(url, "test", "enabled");
					}

				}
			});
		}
		
		//Ashwini Patil Date:16-08-2022
		if(text.equals("Excel Export")){
			ArrayList<AccountingInterface> interfacearray=new ArrayList<AccountingInterface>();
			List<AccountingInterface> list=(List<AccountingInterface>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
			
			interfacearray.addAll(list);
			
			csvservice.setInterfaceTallyDetails(interfacearray, new AsyncCallback<Void>() {
				

				@Override
				public void onFailure(Throwable caught) {
					System.out.println("RPC call Failed"+caught);
				}

				@Override
				public void onSuccess(Void result) {
					String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url=gwt + "csvservlet"+"?type="+63;
					Window.open(url, "test", "enabled");
				}
			});
		}
	}
	
	
	/**
	 * Description : This is created by Rahul Verma and is only for NBHC. This Class will be called to update the WMS System.
	 * Created on :26-10-2016
	 * Created By : Rahul Verma
	 * Created For: NBHC CCPM 
	 * 
	 */
	private void reactOnIntegrateSync() {
		// TODO Auto-generated method stub
		System.out.println("Inside reactOnIntergrateSync");
		List<AccountingInterface> acclist = form.getSuperTable().getDataprovider().getList();
		int ctr = 0;
		if (acclist.size() != 0) {
			ArrayList<AccountingInterface> accInterfaceListb = new ArrayList<AccountingInterface>();
			accInterfaceListb = getSelectedRecords();
			if (accInterfaceListb.size() != 0) {
				if (accInterfaceListb.size() < 2) {

					System.out.println("Inside size 1");
					/*
					 * Since we will always take one selected records thats why
					 * we are assigning entityAI directly here
					 */
					for (AccountingInterface accountingInterface : accInterfaceListb) {
						if (AccountingInterface.TALLYSYNCED.equals(accountingInterface.getStatus())
								|| AccountingInterface.PENDING.equals(accountingInterface.getStatus())
								|| AccountingInterface.CANCELLED.equals(accountingInterface.getStatus())) {
							ctr = ctr + 1;
							form.showDialogMessage("Please select records with created or Failed status only");
							/**
							 * Date 22-08-2019 by Vijay
							 * Des :- for avoiding double click
							 */
							lblTaxInvoice.setText("Integrate Sync");
							lblTaxInvoice.setVisible(true);

							return;
						} else {
							entityAI = accountingInterface;
							docType = entityAI.getDocumentType().trim();
							docId = entityAI.getDocumentID();
							customerRef1 = entityAI.getCustomerRefId();
						}
					}
					if (ctr == 0) {
						if (!docType.trim().equals("GRN")) {
							if (customerRef1 == null) {
								form.showDialogMessage("Please update customer refrence number in customer screen and contact support  ");
								/**
								 * Date 22-08-2019 by Vijay
								 * Des :- for avoiding double click
								 */
								lblTaxInvoice.setText("Integrate Sync");
								lblTaxInvoice.setVisible(true);

							} else {
								if (customerRef1.equals("")) {
									form.showDialogMessage("Please update customer refrence number in customer screen and contact support");
									/**
									 * Date 22-08-2019 by Vijay
									 * Des :- for avoiding double click
									 */
									lblTaxInvoice.setText("Integrate Sync");
									lblTaxInvoice.setVisible(true);


								} else {
									reactonOtherStatus();
								}
							}
						} else {
							reactonOtherStatus();
						}

					}

				} else {
					form.showDialogMessage("Please select only one record");
					/**
					 * Date 22-08-2019 by Vijay
					 * Des :- for avoiding double click
					 */
					lblTaxInvoice.setText("Integrate Sync");
					lblTaxInvoice.setVisible(true);
				}
			} else {
				form.showDialogMessage("No Records selected");
				/**
				 * Date 22-08-2019 by Vijay
				 * Des :- for avoiding double click
				 */
				lblTaxInvoice.setText("Integrate Sync");
				lblTaxInvoice.setVisible(true);
			}

		} else {
			/**
			 * Date 22-08-2019 by Vijay
			 * Des :- for avoiding double click
			 */
			lblTaxInvoice.setText("Integrate Sync");
			lblTaxInvoice.setVisible(true);
			form.showDialogMessage("No Record available to sync");
		}
	}
	
	
	private ArrayList<AccountingInterface> getSelectedRecords() {
		// TODO Auto-generated method stub
		List<AccountingInterface> accInterfacelist = form.getSuperTable().getDataprovider().getList();
		ArrayList<AccountingInterface> selectedaccInterfaceList = new ArrayList<AccountingInterface>();
		for(AccountingInterface accInterface : accInterfacelist){
			
			if(accInterface.isSelectRecord()==true){
				selectedaccInterfaceList.add(accInterface);
			}
			
		}
		return selectedaccInterfaceList;
	}

	/**
	 * Added By Rahul Verma on 26-10-2016
	 * @param acclist :List loaded for other list
	 * 
	 */
	private void reactonOtherStatus() {
		
	// TODO Auto-generated method stub
		
		if(docType.trim().equals("")){
			form.showDialogMessage("Please select atleast one record!!");
		}else{
			final IntegrateSyncServiceAsync intAsync = GWT
					.create(IntegrateSyncService.class);
			intAsync.callInterFace(docType,entityAI,new AsyncCallback<String>() {

				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					form.showDialogMessage("Unexpected Error");
					/**
					 * Date 22-08-2019 by Vijay
					 * Des :- for avoiding double click
					 */
					lblTaxInvoice.setVisible(true);
					lblTaxInvoice.setText("Integrate Sync");
				}

				@Override
				public void onSuccess(final String result) {
					// TODO Auto-generated method stub
//					if(result.trim().contains("Added Successfully")){
//						entityAI.setStatus(AccountingInterface.INTEGRATESYNCED);

					// TODO Auto-generated method stub
//					 { "ID": 1, "Value" : "Added Successfully."}
//					 
//					 { "ID" = -1, "Value" = "Error while adding data."}
//					    
//					 { "ID" = 0, "Value" = "Error while adding data."}
					
					/**
					 * Date 22-08-2019 by Vijay
					 * Des :- for avoiding double click
					 */
					lblTaxInvoice.setVisible(true);
					lblTaxInvoice.setText("Integrate Sync");
					
					AccountingInterface accountingInterface;
					if(result!=null){
						
					for(int i=0;i<form.getSuperTable().getDataprovider().getList().size();i++){
						Boolean created=form.getSuperTable().getDataprovider().getList().get(i).getDocumentID()==entityAI.getDocumentID()&&form.getSuperTable().getDataprovider().getList().get(i).getStatus().trim().equalsIgnoreCase(AccountingInterface.TALLYCREATED);
						Boolean failed=form.getSuperTable().getDataprovider().getList().get(i).getDocumentID()==entityAI.getDocumentID()&&form.getSuperTable().getDataprovider().getList().get(i).getStatus().trim().equalsIgnoreCase(AccountingInterface.FAILED);
						Boolean cancelled=form.getSuperTable().getDataprovider().getList().get(i).getDocumentID()==entityAI.getDocumentID()&&form.getSuperTable().getDataprovider().getList().get(i).getDocumentType().equals(docType)&&form.getSuperTable().getDataprovider().getList().get(i).getStatus().trim().equalsIgnoreCase(AccountingInterface.CANCELLED);
						
						if(created||failed){
							accountingInterface=new AccountingInterface();
							accountingInterface=(AccountingInterface) form.getSuperTable().getDataprovider().getList().get(i);
							accountingInterface.setDateofSynch(new Date());
							accountingInterface.setSelectRecord(false);
							
							form.getSuperTable().getDataprovider().getList().get(i).setDateofSynch(new Date());
							form.getSuperTable().getDataprovider().getList().get(i).setSelectRecord(false);
							
							accountingInterface.setSynchedBy(LoginPresenter.loggedInUser);
							form.getSuperTable().getDataprovider().getList().get(i).setSynchedBy(LoginPresenter.loggedInUser);
							
							if(result.toString().contains("1")&&result.toString().contains("Added Successfully")){
								accountingInterface.setRemark("Added Successfully.");
								if(docType.trim().equalsIgnoreCase("Contract")){
									accountingInterface.setStatus(AccountingInterface.TALLYSYNCED);
									form.getSuperTable().getDataprovider().getList().get(i).setStatus(AccountingInterface.TALLYSYNCED);
								}else if(docType.trim().equalsIgnoreCase("Invoice")){
									accountingInterface.setStatus(AccountingInterface.PENDING);	
									form.getSuperTable().getDataprovider().getList().get(i).setStatus(AccountingInterface.PENDING);
								}else if(docType.trim().equalsIgnoreCase("GRN")){
									accountingInterface.setStatus(AccountingInterface.PENDING);	
									form.getSuperTable().getDataprovider().getList().get(i).setStatus(AccountingInterface.PENDING);
								}else if(docType.trim().equalsIgnoreCase("MMN")){
									accountingInterface.setStatus(AccountingInterface.PENDING);	
									form.getSuperTable().getDataprovider().getList().get(i).setStatus(AccountingInterface.PENDING);
								}
								form.getSuperTable().getDataprovider().getList().get(i).setRemark("Added Successfully.");
									
							}else if(result.toString().contains("1")&&result.toString().contains("Document No Already Exists in System.")){
								accountingInterface.setRemark("Document No Already Exists in System.");
								accountingInterface.setStatus(AccountingInterface.PENDING);
								form.getSuperTable().getDataprovider().getList().get(i).setRemark("Document No Already Exists in System.");
								form.getSuperTable().getDataprovider().getList().get(i).setStatus(AccountingInterface.PENDING);
							}else if(result.toString().contains("0")){
								accountingInterface.setRemark("Error while adding data.");
								accountingInterface.setStatus(AccountingInterface.FAILED);
								form.getSuperTable().getDataprovider().getList().get(i).setRemark("Error while adding data.");
								form.getSuperTable().getDataprovider().getList().get(i).setStatus(AccountingInterface.FAILED);
							}else if(result.toString().contains("-1")){
								accountingInterface.setRemark("Error while adding data.");
								accountingInterface.setStatus(AccountingInterface.FAILED);
								form.getSuperTable().getDataprovider().getList().get(i).setRemark("Error while adding data.");
								form.getSuperTable().getDataprovider().getList().get(i).setStatus(AccountingInterface.FAILED);
							}
							/*** Date 05-04-2019 by Vijay *****/
							else if(result.toString().contains("1")&&result.toString().contains("Cancelled Successfully")){
								accountingInterface.setRemark("Cancelled Successfully.");
								if(docType.trim().equalsIgnoreCase("GRN")){
									accountingInterface.setStatus(AccountingInterface.PENDING);	
									form.getSuperTable().getDataprovider().getList().get(i).setStatus(AccountingInterface.PENDING);
								}
								form.getSuperTable().getDataprovider().getList().get(i).setRemark("Cancelled Successfully.");
							}
							else{
								accountingInterface.setRemark("Unexpected Error");
								accountingInterface.setStatus(AccountingInterface.FAILED);
								form.getSuperTable().getDataprovider().getList().get(i).setRemark("Unexpected Error");
								form.getSuperTable().getDataprovider().getList().get(i).setStatus(AccountingInterface.FAILED);
							}
							
							async.save(accountingInterface, new AsyncCallback<ReturnFromServer>() {

								@Override
								public void onFailure(Throwable caught) {
									// TODO Auto-generated method stub
									form.showDialogMessage("An Unexpected Error occured while saving!");
								}

								@Override
								public void onSuccess(ReturnFromServer result1) {}
							});
						}else if(cancelled){
							accountingInterface=new AccountingInterface();
							accountingInterface=(AccountingInterface) form.getSuperTable().getDataprovider().getList().get(i);
							accountingInterface.setDateofSynch(new Date());
							accountingInterface.setSelectRecord(false);
							
							form.getSuperTable().getDataprovider().getList().get(i).setDateofSynch(new Date());
							form.getSuperTable().getDataprovider().getList().get(i).setSelectRecord(false);
							
							accountingInterface.setSynchedBy(LoginPresenter.loggedInUser);
							form.getSuperTable().getDataprovider().getList().get(i).setSynchedBy(LoginPresenter.loggedInUser);
							
							if(result.toString().contains("1")&&result.toString().contains("Added Successfully")){
								accountingInterface.setRemark("Added Successfully.");
								if(docType.trim().equalsIgnoreCase("Contract")){
									accountingInterface.setStatus(AccountingInterface.TALLYSYNCED);
									form.getSuperTable().getDataprovider().getList().get(i).setStatus(AccountingInterface.TALLYSYNCED);
								}else if(docType.trim().equalsIgnoreCase("Invoice")){
									accountingInterface.setStatus(AccountingInterface.PENDING);	
									form.getSuperTable().getDataprovider().getList().get(i).setStatus(AccountingInterface.PENDING);
								}else if(docType.trim().equalsIgnoreCase("GRN")){
									accountingInterface.setStatus(AccountingInterface.TALLYSYNCED);	
									form.getSuperTable().getDataprovider().getList().get(i).setStatus(AccountingInterface.TALLYSYNCED);
								}
								form.getSuperTable().getDataprovider().getList().get(i).setRemark("Added Successfully.");
									
							}else if(result.toString().contains("0")){
								accountingInterface.setRemark("Error while adding data.");
								accountingInterface.setStatus(AccountingInterface.FAILED);
								form.getSuperTable().getDataprovider().getList().get(i).setRemark("Error while adding data.");
								form.getSuperTable().getDataprovider().getList().get(i).setStatus(AccountingInterface.FAILED);
							}else if(result.toString().contains("-1")){
								accountingInterface.setRemark("Error while adding data.");
								accountingInterface.setStatus(AccountingInterface.FAILED);
								form.getSuperTable().getDataprovider().getList().get(i).setRemark("Error while adding data.");
								form.getSuperTable().getDataprovider().getList().get(i).setStatus(AccountingInterface.FAILED);
							}
							/*** Date 05-04-2019 by Vijay *****/
							else if(result.toString().contains("1")&&result.toString().contains("Cancelled Successfully")){
								accountingInterface.setRemark("Cancelled Successfully.");
								if(docType.trim().equalsIgnoreCase("GRN")){
									accountingInterface.setStatus(AccountingInterface.PENDING);	
									form.getSuperTable().getDataprovider().getList().get(i).setStatus(AccountingInterface.PENDING);
								}
								form.getSuperTable().getDataprovider().getList().get(i).setRemark("Cancelled Successfully.");
							}
							else{
								accountingInterface.setRemark("Unexpected Error");
								accountingInterface.setStatus(AccountingInterface.FAILED);
								form.getSuperTable().getDataprovider().getList().get(i).setRemark("Unexpected Error");
								form.getSuperTable().getDataprovider().getList().get(i).setStatus(AccountingInterface.FAILED);
							}
							
							async.save(accountingInterface, new AsyncCallback<ReturnFromServer>() {

								@Override
								public void onFailure(Throwable caught) {
									// TODO Auto-generated method stub
									form.showDialogMessage("An Unexpected Error occured while saving!");
								}

								@Override
								public void onSuccess(ReturnFromServer result1) {}
							});
						}
					}
					}else{
						for(int i=0;i<form.getSuperTable().getDataprovider().getList().size();i++){
							form.getSuperTable().getDataprovider().getList().get(i).setDateofSynch(new Date());
							form.getSuperTable().getDataprovider().getList().get(i).setSelectRecord(false);
							
							if(form.getSuperTable().getDataprovider().getList().get(i).getDocumentID()==entityAI.getDocumentID()){
								accountingInterface=new AccountingInterface();
								accountingInterface.setRemark("Server Response is blank");
								accountingInterface.setStatus(AccountingInterface.FAILED);
								accountingInterface.setSelectRecord(false);
								
								form.getSuperTable().getDataprovider().getList().get(i).setStatus(AccountingInterface.FAILED);
								form.getSuperTable().getDataprovider().getList().get(i).setRemark("Server Response is blank");
								async.save(accountingInterface, new AsyncCallback<ReturnFromServer>() {

									@Override
									public void onFailure(Throwable caught) {
										// TODO Auto-generated method stub
										form.showDialogMessage("An Unexpected Error occured while saving!");
									}

									@Override
									public void onSuccess(ReturnFromServer result1) {}
								});
							}
						}
					}
					
					form.getSuperTable().getTable().redraw();
				
						
//					}
					form.showDialogMessage(result);
				}
			});
		}
	}

	private void cancelDeleteFromTallyRecord() {
		
		List<AccountingInterface>acclist = form.getSuperTable().getDataprovider().getList();
		
		ArrayList<AccountingInterface> arrayList = new ArrayList<AccountingInterface>();
		arrayList.addAll(acclist);
		form.showWaitSymbol();
		update.cancelDeleteFromTallyRecord(arrayList, new AsyncCallback<Void>() {
			
			@Override
			public void onSuccess(Void result) {
				
				form.showDialogMessage("Update Successful.....!!");
				form.hideWaitSymbol();
			}
			
			@Override
			public void onFailure(Throwable caught) {
				form.showDialogMessage("Error.......!!");
				form.hideWaitSymbol();
			}
		});
	}

	private void updateReactonSync() {
		
		List<AccountingInterface>acclist = form.getSuperTable().getDataprovider().getList();
		
		ArrayList<AccountingInterface> arrayList = new ArrayList<AccountingInterface>();
		arrayList.addAll(acclist);
		form.showWaitSymbol();
		update.updateSynchDataWithSynchDate(arrayList, new AsyncCallback<Void>() {
			
			@Override
			public void onSuccess(Void result) {
				
				form.showDialogMessage("Update Successful.....!!");
				form.hideWaitSymbol();
			}
			
			@Override
			public void onFailure(Throwable caught) {
				form.showDialogMessage("Error.......!!");
				form.hideWaitSymbol();
			}
		});
	}

	private void reactonSync() {
	//	List<AccountingInterface>acclist = form.getSuperTable().getDataprovider().getList();
		/** date 28.7.2018 added by komal to sysnc only selected records **/
		//final List<AccountingInterface>acclist = form.getSuperTable().getDataprovider().getList();
		
		List<AccountingInterface> acclist = getSelectedRecords();
		if(acclist.size() == 0){
			form.showDialogMessage("Please select records to sync");
			return;
		}
		/**
		 * end komal
		 */
		
		/**
		 * @author Anil ,Date : 30-05-2019
		 * checking validation to segregate Service and sale, purchase invoices
		 * user can not synch service and stock item invoices together
		 * For Sasha 
		 */
		if(stockItemFlag&&!valideInvoiceSelection(acclist)){
			return;
		}
		
		
		int ctr  = 0;
		for(int i = 0; i < acclist.size(); i++)
		{
			if(AccountingInterface.TALLYSYNCED.equals(acclist.get(i).getStatus().trim())){
				ctr = ctr+1;
			}
			else if(AccountingInterface.CANCELLED.equals(acclist.get(i).getStatus().trim())){
				ctr = ctr+1;
			}
			else if(AccountingInterface.DELETEFROMTALLY.equals(acclist.get(i).getStatus().trim())){
				ctr = ctr+1;
			}
		}
		if(ctr==0)
		{
			reactonChangeStatus(acclist);
		}
		else if(ctr > 0 )
		{
			form.showDialogMessage("Please select records with created status only");
		}
	}
	
	 
	 
	private boolean valideInvoiceSelection(List<AccountingInterface> acclist) {
		// TODO Auto-generated method stub
		int totalSelectedRecord=acclist.size();
		int sumOfSaleAndPurchaseInv=0;
		int sumOfOtherDoc=0;
		for(AccountingInterface obj:acclist){
			if(obj.getAccountType().equalsIgnoreCase(AppConstants.ACCOUNTTYPEAP)){
				sumOfSaleAndPurchaseInv++;
			}else if(obj.getAccountType().equalsIgnoreCase(AppConstants.ACCOUNTTYPE)&&obj.getReferenceDocumentType1().contains("SalesOrder")){
				sumOfSaleAndPurchaseInv++;
			}else{
				sumOfOtherDoc++;
			}
		}
		
		if(sumOfSaleAndPurchaseInv!=0&&sumOfOtherDoc!=0){
			form.showDialogMessage("Please select either sales/purchase invoices or other documents.");
			return false;
		}
		
		return true;
	}

	private void reactonChangeStatus(List<AccountingInterface> acclist){

		//  new code for sync on date : 23/2/2017
		
		//final List<AccountingInterface>acclist = form.getSuperTable().getDataprovider().getList();
		
		ArrayList<AccountingInterface> arrayList = new ArrayList<AccountingInterface>();
		arrayList.addAll(acclist);
		
		System.out.println("in side reactonChangeStatus method list size"+acclist.size()+"arrrayList size"+arrayList.size());
		form.showWaitSymbol();
		System.out.println("in side reactonChangeStatus method list size"+arrayList.size()+AccountingInterface.TALLYSYNCED+LoginPresenter.loggedInUser);
	
		update.getAccountingInterfaceDataSynched(arrayList,AccountingInterface.TALLYSYNCED,LoginPresenter.loggedInUser,new AsyncCallback<ArrayList<AccountingInterface>>() {

			@Override
			public void onFailure(Throwable caught) {
				System.out.println("in side fail");
				form.hideWaitSymbol();
			}

			@Override
			public void onSuccess(ArrayList<AccountingInterface> updatedList) {
				System.out.println("in side success");
				form.hideWaitSymbol();
				reactonSyncDownload(updatedList);
			}
		});
			
		
//		rohan commented old code  on Date : 23/2/2017
		   
		
//		for(int i = 0;i<acclist.size();i++){
//		
//		final MyQuerry querry=new MyQuerry();
//		Filter filtercount=new Filter();
//		filtercount.setQuerryString("count");
//		filtercount.setIntValue(acclist.get(i).getCount());
//		querry.getFilters().add(filtercount);
//		querry.setQuerryObject(new AccountingInterface());
//		
//		
//		async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
//
//			@Override
//			public void onFailure(Throwable caught) {
//				form.showDialogMessage("An Unexpected Error occured !");
//				
//			}
//			@Override
//			public void onSuccess(ArrayList<SuperModel> result) {
//				for(SuperModel smodel:result)
//				{
//					final AccountingInterface bdentity = (AccountingInterface)smodel;
//					bdentity.setStatus(AccountingInterface.TALLYSYNCED);
//					bdentity.setSynchedBy(LoginPresenter.loggedInUser);
//					async.save(bdentity,new AsyncCallback<ReturnFromServer>() {
//
//						@Override
//						public void onFailure(Throwable caught) {
//							form.showDialogMessage("An Unexpected Error occured !");
//							
//						}
//						@Override
//						public void onSuccess(ReturnFromServer result) {
//								
//						}
//					});
//				}
//				
//				
//				
//			}
//		});
//		
//		
//		}
		
//		for(int k=0;k<acclist.size();k++)
//		{
//			acclist.get(k).setDateofSynch(new Date());
//			acclist.get(k).setStatus(AccountingInterface.TALLYSYNCED);
//			acclist.get(k).setSynchedBy(LoginPresenter.loggedInUser);
//		}
//		
//		form.getSuperTable().connectToLocal();
//		form.getSuperTable().getDataprovider().setList(acclist);
//		reactonSyncDownload(acclist);

	}

	@Override
	public void reactOnPrint() {}
	
	@Override
	public void reactOnDownload() {
		
		/**
		 * Date 19/02/2018 By vijay for NBHC download in searchpopup to download data as per search filter selection
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("AccountingInterface", "AccountingInterfaceReport")){
			
			StringBuilder filterValue = new StringBuilder();
			StringBuilder filterQuerry = new StringBuilder();
			
			AccountingInterfaceSearchPopUp searchProxy = (AccountingInterfaceSearchPopUp) form.getSearchpopupscreen();
			
			
			if(searchProxy.dateComparator.getfromDateValue()!=null){
				filterQuerry.append("dateofSynch >="+"~");
				filterValue.append(AppUtility.parseDate(searchProxy.dateComparator.getfromDateValue())+"~");
			}
			if(searchProxy.dateComparator.gettoDateValue()!=null){
				filterQuerry.append("dateofSynch <="+"~");
				filterValue.append(AppUtility.parseDate(searchProxy.dateComparator.gettoDateValue())+"~");
			}
				
			if(searchProxy.creationDateComparator.getfromDateValue()!=null){
				filterQuerry.append("accountingInterfaceCreationDate >="+"~");
				filterValue.append(AppUtility.parseDate(searchProxy.creationDateComparator.getfromDateValue())+"~");
			}
			if(searchProxy.creationDateComparator.gettoDateValue()!=null){
				filterQuerry.append("accountingInterfaceCreationDate <="+"~");
				filterValue.append(AppUtility.parseDate(searchProxy.creationDateComparator.gettoDateValue())+"~");
			}

			if(searchProxy.accountingInterfaceID.getValue()!=null){
				filterQuerry.append("count"+"~");
				filterValue.append(searchProxy.accountingInterfaceID.getValue()+"~");
			}
			if(searchProxy.accountingInterfaceCreatedby.getSelectedIndex()!=0){
				filterQuerry.append("accountingInterfaceCreatedBy"+"~");
				filterValue.append(searchProxy.accountingInterfaceCreatedby.getValue().trim()+"~");
			}
			if(searchProxy.documentID.getValue()!=null){
				filterQuerry.append("documentID"+"~");
				filterValue.append(searchProxy.documentID.getValue()+"~");
			}
			if(searchProxy.synchedBy.getSelectedIndex()!=0){
				filterQuerry.append("synchedBy"+"~");
				filterValue.append(searchProxy.synchedBy.getValue().trim()+"~");
			}
			if(searchProxy.documentType.getSelectedIndex()!=0){
				filterQuerry.append("documentType"+"~");
				filterValue.append(searchProxy.documentType.getValue().trim()+"~");
			}
			
			// rohan added this code for adding vendor search in accounting interface  
			if(searchProxy.vendor.getIdValue()!=-1)
			  {
				filterQuerry.append("vendorID"+"~");
				filterValue.append(searchProxy.vendor.getIdValue()+"~");
			  }
				
			  if(!(searchProxy.vendor.getFullNameValue().equals("")))
			  {
				  filterQuerry.append("vendorName"+"~");
				  filterValue.append(searchProxy.vendor.getFullNameValue()+"~");
			  }
			  if(searchProxy.vendor.getCellValue()!=-1l)
			  {
				  filterQuerry.append("vendorCell"+"~");
				  filterValue.append(searchProxy.vendor.getCellValue()+"~");
			  } 
			
			//   rohan added this code for adding customer search in Accounting Interface
			
			if(searchProxy.personInfo.getIdValue()!=-1)
			  {
				filterQuerry.append("cinfo.count"+"~");
				filterValue.append(searchProxy.personInfo.getIdValue()+"~");
			  }
			  
			  if(!(searchProxy.personInfo.getFullNameValue().equals("")))
			  {
				filterQuerry.append("cinfo.fullName"+"~");
				filterValue.append(searchProxy.personInfo.getFullNameValue()+"~");
			  }
			  if(searchProxy.personInfo.getCellValue()!=-1l)
			  {
				  filterQuerry.append("cinfo.fullName"+"~");
				  filterValue.append(searchProxy.personInfo.getFullNameValue()+"~");
			  }
			
			  //  ends here 

			if(searchProxy.status.getSelectedIndex()!=0){
				 filterQuerry.append("status"+"~");
				  filterValue.append(searchProxy.status.getItemText(searchProxy.status.getSelectedIndex()).trim()+"~");
				  
			}
				
			if(searchProxy.module.getSelectedIndex()!=0){
				filterQuerry.append("module"+"~");
				filterValue.append(searchProxy.module.getValue().trim()+"~");
			}
				
			if(searchProxy.Branch.getSelectedIndex()!=0){
				filterQuerry.append("branch"+"~");
				filterValue.append(searchProxy.Branch.getValue().trim()+"~");
			}

			filterQuerry.append("companyId");
	 		filterValue.append(model.getCompanyId());
	 		
	 		String querry = filterQuerry.toString();
	 		String value = filterValue.toString();
	 		
	 		String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
	 		
	 		final String url = gwt + "csvservlet" + "?type=" + 135+ "&" +"querry="+querry+ "&" +"value="+value;
			Window.open(url, "test", "enabled");
			
		}else if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("AccountingInterface", "NewTallyInterfaceDownload")){
			/** date 16.04.2018 added by komal for new tally interface download **/	
			accDownloadPopup.showPopUp();			
		}else{
			
			ArrayList<AccountingInterface> interfacearray=new ArrayList<AccountingInterface>();
			List<AccountingInterface> list=(List<AccountingInterface>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
			
			interfacearray.addAll(list);
			
			csvservice.setInterfaceTallyDetails(interfacearray, new AsyncCallback<Void>() {
				

				@Override
				public void onFailure(Throwable caught) {
					System.out.println("RPC call Failed"+caught);
				}

				@Override
				public void onSuccess(Void result) {
					String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url=gwt + "csvservlet"+"?type="+63;
					Window.open(url, "test", "enabled");
				}
			});
		}

	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void makeNewModel() {
		model=new AccountingInterface();		
	}
	public static void initalize()
	{
		AccountingInterfaceTable table=new AccountingInterfaceTable();
		AccountingInterfaceForm form=new AccountingInterfaceForm(table);
		
		AccountingInterfaceSearchPopUp.staticSuperTable=table;
		AccountingInterfaceSearchPopUp searchPopUp=new AccountingInterfaceSearchPopUp(false);
		form.setSearchpopupscreen(searchPopUp);
		
		int height = Window.getClientHeight();
		Console.log("Window Height : " + height);
		height = height * 80 / 100;
		Console.log("Updated Height : " + height);
		searchPopUp.getSupertable().getTable().setHeight(height+"px");
		
		AccountingInterfacePresenter presenter=new AccountingInterfacePresenter(form, new AccountingInterface());
		form.setToViewState();
		AppMemory.getAppMemory().stickPnel(form);
	}
	
	public static void initalize(MyQuerry querry)
	{
		

		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Accounts/Interface Tally List",Screen.INTERFACETALLYLIST);
		AccountingInterfaceTable table=new AccountingInterfaceTable();
		AccountingInterfaceForm form=new AccountingInterfaceForm(table);
		
		AccountingInterfaceSearchPopUp.staticSuperTable=table;
		AccountingInterfaceSearchPopUp searchPopUp=new AccountingInterfaceSearchPopUp(false);
		form.setSearchpopupscreen(searchPopUp);
		
		AccountingInterfacePresenter presenter=new AccountingInterfacePresenter(form, new AccountingInterface(),querry);
		form.setToNewState();
		AppMemory.getAppMemory().stickPnel(form);
	}
	
	
	public MyQuerry getInterfaceTallyQuery()
	{
		List<String > statusList=new ArrayList<>();
		statusList.add(AccountingInterface.TALLYCREATED);
		statusList.add(AccountingInterface.FAILED);
		statusList.add(AccountingInterface.PENDING);
		
		MyQuerry quer=new MyQuerry();
		Vector<Filter> filters = new Vector<Filter>();
		Filter temp = null;
		temp = new Filter();
		temp.setLongValue(model.getCompanyId());
		temp.setQuerryString("companyId");
		filters.add(temp);
		
		temp = new Filter();
		temp.setList(statusList);
		temp.setQuerryString("status IN");
		filters.add(temp);
		
		/**
		 * Developed by : Rohan added this branch  
		 */
			List<String> branchList ;
			branchList= new ArrayList<String>();
			System.out.println("item count in branch "+LoginPresenter.globalBranch.size());
			for (int i = 0; i < LoginPresenter.globalBranch.size(); i++) {
				branchList.add(LoginPresenter.globalBranch.get(i).getBusinessUnitName());
			}
			System.out.println("branchList list "+branchList.size());
		
			temp = new Filter();
			temp.setQuerryString("branch IN");
			temp.setList(branchList);
			filters.add(temp);
		/**
		 * ends here 
		 */
			/** date 04/04/2018 added by komal to load accounting interfaces of last one month**/
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("AccountingInterface", "OnlyForNBHC")){
				Date date=new Date();
				System.out.println("Today Date ::::::::: "+date);				
				CalendarUtil.addDaysToDate(date,-7);
				temp = new Filter();
				temp.setQuerryString("accountingInterfaceCreationDate >=");
				temp.setDateValue(date);
				filters.add(temp);
			}
		/**
		 * ends here 
		 
		**/
		quer.setFilters(filters);
		quer.setQuerryObject(new AccountingInterface());
		return quer;
	}

	  
	public void reactonSyncDownload(List<AccountingInterface> acccarray) {
		/** date 16.04.2018 added by komal for new tally interface download **/
		ArrayList<AccountingInterface> accountingarray = new ArrayList<AccountingInterface>();
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("AccountingInterface", "NewTallyInterfaceDownload")){
			for(AccountingInterface accInterface : acccarray){
				if(accInterface.getDocumentType().equalsIgnoreCase("Expense Management") 
						|| accInterface.getDocumentType().equalsIgnoreCase("Invoice")
						|| accInterface.getDocumentType().equalsIgnoreCase("Customer Payment")
						|| accInterface.getDocumentType().equalsIgnoreCase("Payroll"))
				accountingarray.add(accInterface);
			}
		}else{
			accountingarray.addAll(acccarray);
		}
		//komal
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("AccountingInterface", "NewTallyInterfaceDownload")){
			 ArrayList<TallyInterfaceBean> list = setInterfaceValues(accountingarray);			 
              csvservice.setTallyInterfaceData(list, new AsyncCallback<Void>() {
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					System.out.println("RPC call Failed"+caught);
				}
				@Override
				public void onSuccess(Void result) {
					// TODO Auto-generated method stub
					String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url=gwt + "csvservlet"+"?type="+141;
					Window.open(url, "test", "enabled");
				}
			});
		}else{
			csvservice.setInterfaceTallyDetails(accountingarray, new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);
			}

			@Override
			public void onSuccess(Void result) {
					String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url=gwt + "csvservlet"+"?type="+63;
					Window.open(url, "test", "enabled");
				
			}
		});
	  }
	}
	/*** 28.03.2018 added by komal for tally interface download **/
	private ArrayList<TallyInterfaceBean> setInterfaceValues(ArrayList<AccountingInterface> accountingarray){
		ArrayList<TallyInterfaceBean> tallyInterfaceList = new ArrayList<TallyInterfaceBean>();
		Comparator<AccountingInterface> comp=new Comparator<AccountingInterface>() {
			@Override
			public int compare(AccountingInterface e1, AccountingInterface e2) {

				if(e1!=null && e2!=null)
				{
					if( e1.getDocumentType()!=null && e2.getDocumentType()!=null){
						return e1.getDocumentType().compareTo(e2.getDocumentType());}
				}
				else{
					return 0;}
				return 0;

			}
		};
		Collections.sort(accountingarray,comp);

		Comparator<AccountingInterface> comp1=new Comparator<AccountingInterface>() {
			@Override
			public int compare(AccountingInterface e1, AccountingInterface e2) {

				if (e1.getDocumentID() == e2.getDocumentID()) {
					return 0;
				}
				if (e1.getDocumentID() > e2.getDocumentID()) {
					return 1;
				} else {
					return -1;
				}
			}
		};
		Collections.sort(accountingarray,comp1);
		
		
		
		
		HashMap<String , ArrayList<AccountingInterface>> interfaceMap = new HashMap<String , ArrayList<AccountingInterface>>();
		ArrayList<AccountingInterface> list;
		for(AccountingInterface it : accountingarray){

			if(it.getDocumentType().equalsIgnoreCase("Payroll")){
				it.setDocumentID(it.getCount());
			}
			if(interfaceMap.containsKey(it.getDocumentType()+"-"+it.getDocumentID()+"-"+it.getDocumentStatus())){
				list = interfaceMap.get(it.getDocumentType()+"-"+it.getDocumentID()+"-"+it.getDocumentStatus());
                list.add(it);
				interfaceMap.put(it.getDocumentType()+"-"+it.getDocumentID()+"-"+it.getDocumentStatus(), list);
			}else{
				list = new ArrayList<AccountingInterface>();
				list.add(it);
				interfaceMap.put(it.getDocumentType()+"-"+it.getDocumentID()+"-"+it.getDocumentStatus(), list);
			}
		}
		ArrayList<AccountingInterface> accInterfaceList;
		TallyInterfaceBean tallyInterfaceBean;
		List<String> ledgerNameList;
		List<String> groupNameList;
		List<String> hsnSacList;
		List<Double> amountList;
		List<Double> gstList;
		List<String> narrationList;
		
		/**
		 * @author Anil , Date : 30-05-2019
		 */
		List<String> stockItemNameList;
		List<String> stockGroupNameList;
		List<String> stockQtyList;
		List<String> stockRateList;
		/**
		 * @author Anil , Date : 04-07-2019
		 */
		List<String> godownList;
		
		double discount = 0.0 , indirectExpense  = 0.0;
		
		/**
		 * @author Vijay Date :- 20-05-2023
		 * Des :- As per nitin sir first all invoice record and then payment records should download in tally export
		 * so added below list to manage the same
		 */
		
		ArrayList<TallyInterfaceBean> invoicelist = new ArrayList<TallyInterfaceBean>();
		ArrayList<TallyInterfaceBean> paymentlist = new ArrayList<TallyInterfaceBean>();
		ArrayList<TallyInterfaceBean> otherlist = new ArrayList<TallyInterfaceBean>();

		ArrayList<TallyInterfaceBean> cancelledInvoicelist = new ArrayList<TallyInterfaceBean>();
		ArrayList<TallyInterfaceBean> cancelledPaymentlist = new ArrayList<TallyInterfaceBean>();

		/**
		 * ends here
		 */
		if(interfaceMap!=null)
			Console.log("interfaceMap size="+interfaceMap.size());
		for(Map.Entry<String, ArrayList<AccountingInterface>> entry : interfaceMap.entrySet())
		{
			
			accInterfaceList = entry.getValue();
			AccountingInterface accountingInterface = accInterfaceList.get(0);
			
			Console.log("processing accountingInterface doctype="+accountingInterface.getDocumentType()+" list size="+accInterfaceList.size());
			
			tallyInterfaceBean = new TallyInterfaceBean();
			ledgerNameList = new ArrayList<String>();
			groupNameList = new ArrayList<String>();
			hsnSacList = new ArrayList<String>();
			amountList = new ArrayList<Double>();
			gstList = new ArrayList<Double>();
			narrationList = new ArrayList<String>();
			
			stockItemNameList=new ArrayList<String>();
			stockGroupNameList=new ArrayList<String>();
			stockQtyList=new ArrayList<String>();
			stockRateList=new ArrayList<String>();
			
			godownList=new ArrayList<String>();
			System.out.println("in map"+accountingInterface.getDocumentType());
            if(accountingInterface.getDocumentType().equalsIgnoreCase("Invoice")){
            	int amtVar=1;
            	int invVar=1;
            	int qtyVar=1;
				if(accountingInterface.getDocumentType()!=null){
				tallyInterfaceBean.setDocumentName(accountingInterface.getDocumentType());
				
				}	
				/** date 28.07.2018 added by komal for accounting interface voucher name column **/
				if(accountingInterface.getAccountType().equalsIgnoreCase(AppConstants.ACCOUNTTYPEAP)){
					tallyInterfaceBean.setInvoiceType("Purchase Invoice");/** date 13.12.2018 changed by komal for sasha(purchase and sales invoice diff)**/
				//	tallyInterfaceBean.setInvoiceType(AppConstants.SALESINVOICE);
					amtVar=-1;
					invVar=-1;
					qtyVar=-1;
				}else if(accountingInterface.getAccountType().equalsIgnoreCase(AppConstants.ACCOUNTTYPE)){
					tallyInterfaceBean.setInvoiceType("Sales Invoice");
				//	tallyInterfaceBean.setInvoiceType(AppConstants.pro);
					amtVar=-1;
					invVar=-1;
					qtyVar=-1;
				}else{
					tallyInterfaceBean.setInvoiceType("Service Invoice");
					amtVar=-1;
					invVar=-1;
					qtyVar=-1;
					
					
					
				}
					
				/*
				 * Date:5/10/2018
				 * Developer:Ashwini
				 * Des:To add branch name as cost center
				 */
				if(accountingInterface.getCompanyId()!=null){
					tallyInterfaceBean.setCompanyId(accountingInterface.getCompanyId());
				}
				
				if(accountingInterface.getBranch()!=null){
					tallyInterfaceBean.setBranch(accountingInterface.getBranch());
				}
				
				/*
				 * end by Ashwini
				 */
					
				if(accountingInterface.getDocumentDate()!=null){
					tallyInterfaceBean.setInvoiceDate(accountingInterface.getDocumentDate());
				}
				
				/**
				 * @author Anil , Date : 05-07-2019
				 */
				if(stockItemFlag&&accountingInterface.getInvoiceRefNum()!=null&&!accountingInterface.getInvoiceRefNum().equals("")){
					tallyInterfaceBean.setSupplierInvoiceNumber(accountingInterface.getInvoiceRefNum());
				}else{
					if(accountingInterface.getReferenceDocumentNo1()!=null){
						tallyInterfaceBean.setSupplierInvoiceNumber(accountingInterface.getReferenceDocumentNo1());
					}
				}
				
				if(accountingInterface.getReferenceDocumentDate1()!=null){
					tallyInterfaceBean.setSupplierDate(accountingInterface.getReferenceDocumentDate1());
				}
				
				/**
				 * @author Anil , Date : 05-07-2019
				 */
				if(stockItemFlag&&accountingInterface.getInvoiceNum()!=null&&!accountingInterface.getInvoiceNum().equals("")){
					tallyInterfaceBean.setInvoiceNumber(accountingInterface.getInvoiceNum());
				}else{
		            if(accountingInterface.getDocumentID()!=0){
		            	tallyInterfaceBean.setInvoiceNumber(accountingInterface.getDocumentID()+"");
		            }
				}
	            if(accountingInterface.getGstn()!=null){
	            	tallyInterfaceBean.setGstn(accountingInterface.getGstn());
	            }
	            if(accountingInterface.getState()!=null){
	            	tallyInterfaceBean.setPlaceToSupply(accountingInterface.getState());
	            }
	           
	            tallyInterfaceBean.setTotalInvoiceAmount(accountingInterface.getNetPayable());
//	            if(stockItemFlag){
//	            	tallyInterfaceBean.setTotalInvoiceAmount(invVar*accountingInterface.getNetPayable());
//	            }
	            
	        	if(accountingInterface.getReferenceDocumentNo1()!=null){
					tallyInterfaceBean.setBillNumber(accountingInterface.getReferenceDocumentNo1());
				}
			
	        	if(accountingInterface.getCustomerName()!=null)
	        		ledgerNameList.add(accountingInterface.getCustomerName());
	        	else
	        		ledgerNameList.add("");
	            
				if(accountingInterface.getAccountType().equalsIgnoreCase(AppConstants.ACCOUNTTYPEAP)){
				groupNameList.add("Sundry Creditors");
				}else if(accountingInterface.getAccountType().equalsIgnoreCase(AppConstants.ACCOUNTTYPE)){
					groupNameList.add("Sundry Debtors");
				}else{
				//	Console.log("in else condition of accounttype");
					if(accountingInterface.getReferenceDocumentType1()!=null){
						if(accountingInterface.getReferenceDocumentType1().equalsIgnoreCase("Contract")||accountingInterface.getReferenceDocumentType1().equalsIgnoreCase("SalesOrder.Ref.Doc"))
							groupNameList.add("Sundry Debtors");
						else if(accountingInterface.getReferenceDocumentType1().equalsIgnoreCase("Purchase Order"))
							groupNameList.add("Sundry Creditors");
					}else
						groupNameList.add("ac type error");
				}
				hsnSacList.add("");
				amountList.add(accountingInterface.getNetPayable());
				gstList.add(0.0);
				narrationList.add("");
				
				/**
				 * @author Anil ,Date : 30-05-2019
				 */
				
				if(stockItemFlag){
					stockItemNameList.add("");
					stockGroupNameList.add("");
					stockQtyList.add("");
					stockRateList.add("");
					
					godownList.add("");
				}
	
				Map<Double , ArrayList<AccountingInterface>> combinedTaxMap = getTaxwiseMap(accInterfaceList); 
	
				for(Map.Entry<Double, ArrayList<AccountingInterface>> entry1 : combinedTaxMap.entrySet()){
					Console.log("entry1 key :" + entry1.getKey() + " entry value : "+ entry1.getValue());
					String taxName1 = "" , 		taxName2 = "" , groupName1 = "" , groupName2 = "";
					double totalTax1 = 0 ,totalTax2 = 0;
					for(AccountingInterface accInterface : entry1.getValue()){
						if(accInterface.getProductName() !=null){
							/**
							 * 
							 */
	//						ledgerNameList.add(accInterface.getProductName());
							if(stockItemFlag){
								ledgerNameList.add(accInterface.getProductCategory());
								stockItemNameList.add(accInterface.getProductName());
								stockGroupNameList.add(accInterface.getProductCategory());
								stockQtyList.add((qtyVar*accInterface.getQuantity())+"");
								stockRateList.add(accInterface.getProductPrice()+"");
								
								if(accInterface.getGodownName()!=null){
									godownList.add(accInterface.getGodownName());
								}else{
									godownList.add("");
								}
								
							}else{
								ledgerNameList.add(accInterface.getProductName());
							}
							/**
							 * 
							 */
						}
						if(accInterface.getTax1()!=null && !(accInterface.getTax1().equalsIgnoreCase(""))){
							taxName1 = accInterface.getTax1()+"@"+accInterface.getServiceTaxPercent();
							
							/**
							 * @author Anil ,Date :04-07-2019
							 * changing tax name to default value
							 */
							if(cumalativeTaxAmtFlag&&(accInterface.getTax1().contains("CGST")||accInterface.getTax1().contains("Cgst")||accInterface.getTax1().contains("cgst"))){
								taxName1="Input CGST";
							}
							if(cumalativeTaxAmtFlag&&(accInterface.getTax1().contains("SGST")||accInterface.getTax1().contains("Sgst")||accInterface.getTax1().contains("sgst"))){
								taxName1="Input SGST";
							}
							if(cumalativeTaxAmtFlag&&(accInterface.getTax1().contains("IGST")||accInterface.getTax1().contains("Igst")||accInterface.getTax1().contains("igst"))){
								taxName1="Input IGST";
							}
						}
						
						if(accInterface.getTax2()!=null && !(accInterface.getTax2().equalsIgnoreCase(""))){
							taxName2 = accInterface.getTax2()+"@"+accInterface.getVatPercent();
							
							/**
							 * @author Anil ,Date :04-07-2019
							 * changing tax name to default value
							 */
							if(cumalativeTaxAmtFlag&&(accInterface.getTax2().contains("CGST")||accInterface.getTax2().contains("Cgst")||accInterface.getTax2().contains("cgst"))){
								taxName2="Input CGST";
							}
							if(cumalativeTaxAmtFlag&&(accInterface.getTax2().contains("SGST")||accInterface.getTax2().contains("Sgst")||accInterface.getTax2().contains("sgst"))){
								taxName2="Input SGST";
							}
							if(cumalativeTaxAmtFlag&&(accInterface.getTax2().contains("IGST")||accInterface.getTax2().contains("Igst")||accInterface.getTax2().contains("igst"))){
								taxName2="Input IGST";
							}
						}
						if(accountingInterface.getAccountType().equalsIgnoreCase(AppConstants.ACCOUNTTYPEAP)){
							groupNameList.add("Purchase Accounts");
						}else if(accountingInterface.getAccountType().equalsIgnoreCase(AppConstants.ACCOUNTTYPE)){
							groupNameList.add("Sales Accounts");
						}else {
						//	Console.log("in else condition of accounttype2");
							if(accountingInterface.getReferenceDocumentType1()!=null){
								if(accountingInterface.getReferenceDocumentType1().equalsIgnoreCase("Contract")||accountingInterface.getReferenceDocumentType1().equalsIgnoreCase("SalesOrder.Ref.Doc"))
									groupNameList.add("Sales Accounts");
								else if(accountingInterface.getReferenceDocumentType1().equalsIgnoreCase("Purchase Order"))
									groupNameList.add("Purchase Accounts");
							}else
								groupNameList.add("ac type error2");
						}
						groupName1 = accInterface.getGroupName1();
						groupName2 = accInterface.getGroupName2();
						hsnSacList.add(accInterface.getHsnSac());
						
						/**
						 * 
						 */
	//					amountList.add(-accInterface.getTotalAmount());
						if(stockItemFlag){
							amountList.add(amtVar*accInterface.getTotalAmount());							
						}else{
							double val=Double.parseDouble(df.format(-accInterface.getTotalAmount()));
//							amountList.add(-accInterface.getTotalAmount());		
							amountList.add(val);
						}
						
						
						gstList.add(accInterface.getServiceTaxPercent()+accInterface.getVatPercent());
						
					    totalTax1 += accInterface.getServiceTaxAmount();
					    totalTax2 += accInterface.getVatAmount();
					    String narration = "";
					    /**
					     * Date : 20-12-2018 BY ANIL
					     * Process Name : Invoice
					     * Process Type : InvoiceCommentAsTallyNarration
					     * Ankita Pest Control raised By Sonu
					     * mapping invoice comment as tally narration
					     */
					    if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "InvoiceCommentAsTallyNarration")&&accInterface.getDocumentRemark()!=null){
					    	narration=accInterface.getDocumentRemark();
					    	/**
					    	 * If narration list size is 1 then only we will add narration in narration list
					    	 */
					    	if(narrationList.size()==1){
					    		narrationList.add(narration);
					    	}
					    }else{
							narration = "Interface id : " + accInterface.getCount() + " ";
							if (accInterface.getAccountingInterfaceCreationDate() != null) {
								narration += "Interface Date : " + AppUtility.parseDateTime(accInterface.getAccountingInterfaceCreationDate())+"";
							}
							narrationList.add(narration);
					    }
					    /**
					     * End
					     */
						discount  = accInterface.getDiscount();
						indirectExpense = accInterface.getIndirectExpenses();
					}
					ledgerNameList.add(taxName1);
					ledgerNameList.add(taxName2);
					narrationList.add("");
					narrationList.add("");
				
					hsnSacList.add("");
					hsnSacList.add("");
					
					gstList.add(0.0);
					gstList.add(0.0);
					
					groupNameList.add(groupName1);
					groupNameList.add(groupName2);
					
					/**
					 * 
					 */
	//				amountList.add(-totalTax1);
	//				amountList.add(-totalTax2);
					if(stockItemFlag){
						amountList.add(amtVar*totalTax1);
						amountList.add(amtVar*totalTax2);
					}else{
						
//						amountList.add(-totalTax1);
						if(totalTax1!=0){
							double val=Double.parseDouble(df.format(-totalTax1));
							amountList.add(val);
						}else{
							amountList.add(-totalTax1);
						}
//						amountList.add(-totalTax2);
						if(totalTax2!=0){
							double val=Double.parseDouble(df.format(-totalTax2));
							amountList.add(val);	
						}else{
							amountList.add(-totalTax2);
						}
					}
					
					/***
					 * 
					 */
					if(stockItemFlag){
						stockItemNameList.add("");
						stockGroupNameList.add("");
						stockQtyList.add("");
						stockRateList.add("");
						godownList.add("");
						
						stockItemNameList.add("");
						stockGroupNameList.add("");
						stockQtyList.add("");
						stockRateList.add("");
						godownList.add("");
					}
					/**
					 * 
					 */
					
				}
				ledgerNameList.add(AppConstants.DISCOUNT);
				ledgerNameList.add(AppConstants.DELIVERYCHARGES);
				/**Date 22-9-2020 by Amol added a roundoff row raised by Nittin Sir for pecopp**/
				ledgerNameList.add("Roundoff");
				
				
				if (accountingInterface.getAccountType().equalsIgnoreCase(AppConstants.ACCOUNTTYPEAP)) {
					groupNameList.add(AppConstants.INDIRECTEXPENSE);
					groupNameList.add(AppConstants.INDIRECTEXPENSE);
				} else if (accountingInterface.getAccountType().equalsIgnoreCase(AppConstants.ACCOUNTTYPE)) {
					groupNameList.add(AppConstants.INDIRECTINCOMES);
					groupNameList.add(AppConstants.INDIRECTINCOMES);
				}else {
					Console.log("in else condition of accounttype3");
					if(accountingInterface.getReferenceDocumentType1()!=null){
						if(accountingInterface.getReferenceDocumentType1().equalsIgnoreCase("Contract")||accountingInterface.getReferenceDocumentType1().equalsIgnoreCase("SalesOrder.Ref.Doc"))
							groupNameList.add(AppConstants.INDIRECTINCOMES);
						else if(accountingInterface.getReferenceDocumentType1().equalsIgnoreCase("Purchase Order"))
							groupNameList.add(AppConstants.INDIRECTEXPENSE);
					}else
						groupNameList.add("ac type error3");
				}
				groupNameList.add("");
				groupNameList.add("");
				
				groupNameList.add("");
				
				narrationList.add("");
				narrationList.add("");  
				narrationList.add("");  
				
				hsnSacList.add("");
				hsnSacList.add("");
				hsnSacList.add("");
				
//				amountList.add(discount);
//				amountList.add(indirectExpense);
				if(discount!=0){
					double val=Double.parseDouble(df.format(discount));
					amountList.add(val);
				}else{
					amountList.add(discount);
				}
				
				if(indirectExpense!=0){
					double val=Double.parseDouble(df.format(indirectExpense));
					amountList.add(val);
				}else{
					amountList.add(indirectExpense);
				}
				
				double roundOffAmt=0;
				for(double amt:amountList){
					Console.log("TOTAL AMT "+amt);
					roundOffAmt+=amt;
				}
				
				Console.log(" total amt11"+roundOffAmt);
				
				double totAmt=(roundOffAmt*(-1));
				Console.log(" total amt value "+totAmt);
				amountList.add(totAmt);
				
				gstList.add(0.0);
				gstList.add(0.0);
				gstList.add(0.0);
				/***
				 * 
				 */
				if(stockItemFlag){
					stockItemNameList.add("");
					stockGroupNameList.add("");
					stockQtyList.add("");
					stockRateList.add("");
					godownList.add("");
					
					stockItemNameList.add("");
					stockGroupNameList.add("");
					stockQtyList.add("");
					stockRateList.add("");
					godownList.add("");
					
					tallyInterfaceBean.setStockItemNameList(stockItemNameList);
					tallyInterfaceBean.setStockGroupNameList(stockGroupNameList);
					tallyInterfaceBean.setStockRateList(stockRateList);
					tallyInterfaceBean.setStockQtyList(stockQtyList);
					tallyInterfaceBean.setGodownList(godownList);
				}
				/**
				 * 
				 */
				
				
				
				
				
				/**
				 * 
				 */
				//Console.log("amountList size="+amountList.size());
				tallyInterfaceBean.setAmountList(amountList);
				tallyInterfaceBean.setGroupNameList(groupNameList);
				tallyInterfaceBean.setLedgerNameList(ledgerNameList);
				tallyInterfaceBean.setHsnSacList(hsnSacList);
				tallyInterfaceBean.setNarrationList(narrationList);
				tallyInterfaceBean.setGstList(gstList);
				tallyInterfaceBean.setsAddressLine1(accountingInterface.getAddress());
				tallyInterfaceBean.setsAddressLine2(accountingInterface.getAddress1());
				tallyInterfaceBean.setsLandMark(accountingInterface.getLandmark());
				tallyInterfaceBean.setsLocality(accountingInterface.getLocality());
				tallyInterfaceBean.setsPinNumber(accountingInterface.getPincode()+"");
				tallyInterfaceBean.setsCity(accountingInterface.getCity());
				tallyInterfaceBean.setsCountry(accountingInterface.getCountry());
				tallyInterfaceBean.setsState(accountingInterface.getState());
							
				tallyInterfaceBean.setbAddressLine1(accountingInterface.getSaddress());
				tallyInterfaceBean.setbAddressLine2(accountingInterface.getSaddress2());
				tallyInterfaceBean.setbLandMark(accountingInterface.getSlandmark());
				tallyInterfaceBean.setbLocality(accountingInterface.getSlocality());
				tallyInterfaceBean.setbPinNumber(accountingInterface.getSpincode()+"");
				tallyInterfaceBean.setbCity(accountingInterface.getScity());
				tallyInterfaceBean.setbCountry(accountingInterface.getScountry());
				tallyInterfaceBean.setbState(accountingInterface.getSstate());
				
			
			
				tallyInterfaceBean.setDocumentStatus(accountingInterface.getDocumentStatus());
				tallyInterfaceBean.setReferenceNumber(accountingInterface.getReferenceDocumentNo2());
				tallyInterfaceBean.setEinvoiceNo(accountingInterface.getEinvoiceNo());
				tallyInterfaceBean.setEinvoiceStatus(accountingInterface.getEinvoiceStatus());
				tallyInterfaceBean.setEinvoiceAckNo(accountingInterface.getEinvoiceAckNo());
				if(accountingInterface.getEinvoiceAckDate()!=null)
				tallyInterfaceBean.setEinvoiceAckDate(accountingInterface.getEinvoiceAckDate());
				tallyInterfaceBean.setEinvoiceQRCodeString(accountingInterface.getEinvoiceQRCodeString());
				tallyInterfaceBean.setEinvoiceCancellationDate(accountingInterface.getEinvoiceCancellationDate());
//				tallyInterfaceList.add(tallyInterfaceBean);
				if(accountingInterface.getNumberRange()!=null)
    				tallyInterfaceBean.setNumberRange(accountingInterface.getNumberRange());
				
				Console.log("invoice tallyInterfaceBean.getDocumentStatus()"+tallyInterfaceBean.getDocumentStatus());
				if(tallyInterfaceBean.getDocumentStatus().equals(Invoice.CANCELLED)) {
					Console.log("Cancelled Invoice");
					cancelledInvoicelist.add(tallyInterfaceBean);
				}
				else {
					invoicelist.add(tallyInterfaceBean);
				}
				
           } 
			else if(accountingInterface.getDocumentType().equalsIgnoreCase("Customer Payment")){
        		for(AccountingInterface accInterface : accInterfaceList){
        			 if(accInterface.getDocumentType()!=null){
        					tallyInterfaceBean.setDocumentName(accInterface.getDocumentType());
        			 }
        			// tallyInterfaceBean.setInvoiceType(AppConstants.PAYMENT1);
        				/** date 28.07.2018 added by komal for accounting interface voucher name column **/
        				tallyInterfaceBean.setInvoiceType(AppConstants.RECEIPT);
        				if(accountingInterface.getReferenceDocumentNo1()!=null){
        					tallyInterfaceBean.setSupplierInvoiceNumber(accountingInterface.getReferenceDocumentNo1());
        				}
        				
        				
        				/*
        				 * Added by Ashwini
        				 */
        				
        				if(accountingInterface.getCompanyId()!=null){
        					tallyInterfaceBean.setCompanyId(accInterface.getCompanyId());
        				}
        				
        				if(accountingInterface.getBranch()!=null){
        					tallyInterfaceBean.setBranch(accInterface.getBranch());
        				}
        				
        				/*
        				 * End by Ashwini
        				 */
        				
        				if(accountingInterface.getReferenceDocumentDate1()!=null){
        					tallyInterfaceBean.setSupplierDate(accountingInterface.getReferenceDocumentDate1());
        				}
        	            if(accountingInterface.getDocumentID()!=0){
        	            	tallyInterfaceBean.setInvoiceNumber(accountingInterface.getDocumentID()+"");
        	            }

        			 if(accInterface.getPaymentDate()!=null){
        				 tallyInterfaceBean.setInvoiceDate(accInterface.getPaymentDate());
        			 }
        			 if(accInterface.getBankName()!=null){
        				 tallyInterfaceBean.setBankName(accInterface.getBankName());
        			 }
        	         if(accInterface.getCustomerName()!=null)
        	            ledgerNameList.add(accInterface.getCustomerName());
        	         else
        	            ledgerNameList.add("");
        				
        	         if(accInterface.getDocumentRemark()!=null && !accInterface.getDocumentRemark().equals("")){
        	        	 ledgerNameList.add(accInterface.getDocumentRemark());
        	         }else{
        	        	 ledgerNameList.add("");
        	         }
        	         ledgerNameList.add(AppConstants.TDSFEES);
//        			if(accInterface.getBankAccount()!=null){
//        				tallyInterfaceBean.setBankAccountNo(accInterface.getBankAccount());
//        			}
        			if(accInterface.getChequeNumber()!=null){
        				tallyInterfaceBean.setChequeNumber(accInterface.getChequeNumber());
        			}
        			if(accInterface.getChequeDate()!=null){
        				tallyInterfaceBean.setChequeDate(accInterface.getChequeDate());
        			}
        			if(accInterface.getReferenceDocumentNo1()!=null){
        				tallyInterfaceBean.setBillNumber(accInterface.getReferenceDocumentNo1());
        			}
        			amountList.add(accInterface.getAmountRecieved());
        			amountList.add(-(accInterface.getAmountRecieved()-accInterface.getTdsAmount()));
        			amountList.add(-accInterface.getTdsAmount());
        			String narration = "";
//    				narration = "Interface id : " + accInterface.getCount() + " ";
//    				
//    				if (accInterface.getAccountingInterfaceCreationDate() != null) {
//    					narration += "Interface Date : " + AppUtility.parseDateTime(accInterface.getAccountingInterfaceCreationDate())+" ";
//    				}
        			/**
        			 * Date : 20-12-2018 By ANIL
        			 * mapped customer payment as tally narration if below process configuration is active
        			 * Process Name : CustomerPayment
        			 * Process Type : PaymentCommentAsTallyNarration
        			 * For Ankita Pest Control raised BY Sonu
        			 */
        			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CustomerPayment", "PaymentCommentAsTallyNarration")){
        				narration=accInterface.getConractCategory();
        			}else{
        				narration +=accInterface.getPaymentMethod();
        			}
        			/**
        			 * End
        			 */
    				narrationList.add(narration);
    				narrationList.add("");
    				narrationList.add("");
    				if(accountingInterface.getAccountType().equalsIgnoreCase(AppConstants.ACCOUNTTYPEAP)){
    					groupNameList.add("Sundry Creditors");
    					}else if(accountingInterface.getAccountType().equalsIgnoreCase(AppConstants.ACCOUNTTYPE)){
    						groupNameList.add("Sundry Debtors");
    					}else {
    					//	Console.log("in else condition of accounttype4");
    						if(accountingInterface.getReferenceDocumentType1()!=null){
    							if(accountingInterface.getReferenceDocumentType1().equalsIgnoreCase("Contract")||accountingInterface.getReferenceDocumentType1().equalsIgnoreCase("SalesOrder.Ref.Doc"))
    								groupNameList.add("Sundry Debtors");
    							else if(accountingInterface.getReferenceDocumentType1().equalsIgnoreCase("Purchase Order"))
    								groupNameList.add("Sundry Creditors");
    						}else
    							groupNameList.add("ac type error4");
    					}
    				groupNameList.add("Bank Account");
    				groupNameList.add(AppConstants.DIRECTEXPENSE);
    				tallyInterfaceBean.setGroupNameList(groupNameList);
    				tallyInterfaceBean.setAmountList(amountList);
    				tallyInterfaceBean.setLedgerNameList(ledgerNameList);
    				tallyInterfaceBean.setNarrationList(narrationList);
    				
    				tallyInterfaceBean.setDocumentStatus(accountingInterface.getDocumentStatus());
    				tallyInterfaceBean.setReferenceNumber(accountingInterface.getReferenceDocumentNo2());
    				
//    				tallyInterfaceList.add(tallyInterfaceBean);

    				if(accountingInterface.getNumberRange()!=null)
    				tallyInterfaceBean.setNumberRange(accountingInterface.getNumberRange());

    				if(tallyInterfaceBean.getDocumentStatus().equals(Invoice.CANCELLED)) {
    					Console.log("Cancelled Payment");
    					cancelledPaymentlist.add(tallyInterfaceBean);
    				}
    				else {
    					paymentlist.add(tallyInterfaceBean);

    				}
    				
        		}
           }else if(accountingInterface.getDocumentType().equalsIgnoreCase("Expense Management")){
        	   for(AccountingInterface accInterface : accInterfaceList){
        	   tallyInterfaceBean.setInvoiceType(AppConstants.JOURNAL);	   
        	   tallyInterfaceBean.setTotalInvoiceAmount(accInterface.getNetPayable());
        	   tallyInterfaceBean.setDocumentName(accountingInterface.getDocumentType());
        	   tallyInterfaceBean.setVoucherNumber(accInterface.getDocumentID()+"");
        	   tallyInterfaceBean.setExpenseDate(accInterface.getDocumentDate());
        	   ledgerNameList.add("Sundry Expenses");
        	   ledgerNameList.add(accInterface.getReferenceDocumentType2());
        	   narrationList.add(accInterface.getConractCategory());
        	   tallyInterfaceBean.setBillNumber(accInterface.getReferenceDocumentNo2());
        	   tallyInterfaceBean.setLedgerNameList(ledgerNameList);
        	   tallyInterfaceBean.setNarrationList(narrationList);
//        	   tallyInterfaceList.add(tallyInterfaceBean);
				tallyInterfaceBean.setDocumentStatus(accountingInterface.getDocumentStatus());

				otherlist.add(tallyInterfaceBean);

        	   }
           }else if(accountingInterface.getDocumentType().equalsIgnoreCase("Payroll")){
        	   for(AccountingInterface accInterface : accInterfaceList){
        		   tallyInterfaceBean.setInvoiceType(AppConstants.JOURNAL);	   
            	   tallyInterfaceBean.setDocumentName(accountingInterface.getDocumentType());
            	   tallyInterfaceBean.setVoucherNumber(accInterface.getCount()+"");
            	   tallyInterfaceBean.setInvoiceDate(accInterface.getDocumentDate());
            	   tallyInterfaceBean.setInvoiceNumber(accInterface.getDocumentGL());
            	   List<GeneralKeyValueBean> beanList = accInterface.getPayRollList();
//            	   Comparator<GeneralKeyValueBean> comp2=new Comparator<GeneralKeyValueBean>() {
//           			@Override
//           			public int compare(GeneralKeyValueBean e1, GeneralKeyValueBean e2) {
//
//           				if (e1.getValue() == e2.getValue()) {
//           					return 0;
//           				}
//           				if (e1.getValue() > e2.getValue()) {
//           					return 1;
//           				} else {
//           					return -1;
//           				} 
//           			}
//           		};
//           		Collections.sort(beanList,comp2);
           		ledgerNameList.add("Salaries and Wages -"+accInterface.getAccountType());
           		amountList.add(accInterface.getTotalAmount());
           		groupNameList.add(AppConstants.INDIRECTEXPENSE);
            	   for(GeneralKeyValueBean bean : beanList){
            		   if(bean.isStatus()){
            			   ledgerNameList.add(bean.getGroupName());
            			   amountList.add(bean.getValue());
            			   groupNameList.add(AppConstants.INDIRECTEXPENSE);
            		   }
            	   }
            	   ledgerNameList.add("Salaries  Payable -"+accInterface.getBranch());
            	   amountList.add(-accInterface.getNetPayable());
            	   groupNameList.add(AppConstants.INDIRECTEXPENSE);
            	   for(GeneralKeyValueBean bean : beanList){
            		   if(!bean.isStatus()){
            			   ledgerNameList.add(bean.getGroupName());
            			   amountList.add(bean.getValue());
            			   groupNameList.add(AppConstants.INDIRECTEXPENSE);
            		   }
            	   }
            	tallyInterfaceBean.setGroupNameList(groupNameList);
   				tallyInterfaceBean.setAmountList(amountList);
   				tallyInterfaceBean.setLedgerNameList(ledgerNameList);
   				tallyInterfaceBean.setBranch(accInterface.getBranch());
//   				tallyInterfaceList.add(tallyInterfaceBean);
				tallyInterfaceBean.setDocumentStatus(accountingInterface.getDocumentStatus());

				otherlist.add(tallyInterfaceBean);

        	   }
           }else
           {
        	   Console.log("Not matched to any type. In else condition. Doc type="+accountingInterface.getDocumentType());
           }
		}
		
		
		if(invoicelist.size()>0){
			tallyInterfaceList.addAll(invoicelist);
		}
		if(paymentlist.size()>0){
			tallyInterfaceList.addAll(paymentlist);
		}
		Console.log("invoicelist size="+invoicelist.size());
		Console.log("Cancelled Invoice size"+cancelledInvoicelist.size());
		if(cancelledInvoicelist.size()>0) {
			tallyInterfaceList.addAll(cancelledInvoicelist);
		}
		if(cancelledPaymentlist.size()>0) {
			tallyInterfaceList.addAll(cancelledPaymentlist);
		}
		Console.log("paymentlist size="+paymentlist.size());
		Console.log("Cancelled Payment size"+cancelledPaymentlist.size());

		Console.log("otherlist size="+otherlist.size());
		if(otherlist.size()>0){
			tallyInterfaceList.addAll(otherlist);
		}
		return tallyInterfaceList;
	}	
	
	@Override
	public void onClick(ClickEvent event) {
		
		super.onClick(event);
		/** date 16.4.2018 added by komal for new accounting interface**/

		if(event.getSource().equals(conditionPopup.getBtnOne())){
			panel.hide();
			reactonSync();
		}
		if(event.getSource().equals(conditionPopup.getBtnTwo())){
			panel.hide();
		}
		/** date 16.4.2018 added by komal for new accounting interface**/
		if(event.getSource().equals(accDownloadPopup.getLblOk())){
			GenricServiceAsync service = GWT.create(GenricService.class);
			//List<AccountingInterface> acccarray=(List<AccountingInterface>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
			MyQuerry querry = new MyQuerry();
			Company c = new Company();
			Vector<Filter> filtervec = new Vector<Filter>();
			Filter filter = null;
			filter = new Filter();
			filter.setQuerryString("companyId");
			filter.setLongValue(c.getCompanyId());
			filtervec.add(filter);
			
			filter = new Filter();
			filter.setQuerryString("documentUniqueId");
			filter.setStringValue(accDownloadPopup.getOlbUniqueId().getValue());
			filtervec.add(filter);

			querry.setFilters(filtervec);
			querry.setQuerryObject(new AccountingInterface());

			service.getSearchResult(querry,
					new AsyncCallback<ArrayList<SuperModel>>() {

						@Override
						public void onFailure(Throwable caught) {
							form.hideWaitSymbol();
							form.showDialogMessage("Unexpected Error Occured!");
						}

						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							ArrayList<AccountingInterface> accountingarray = new ArrayList<AccountingInterface>();
							for(SuperModel model : result){
								AccountingInterface accInterface = (AccountingInterface)model;
								if(accInterface.getDocumentType().equalsIgnoreCase("Expense Management") || accInterface.getDocumentType().equalsIgnoreCase("Invoice")
										|| accInterface.getDocumentType().equalsIgnoreCase("Customer Payment") 
										  || accInterface.getDocumentType().equalsIgnoreCase("Payroll"))
								accountingarray.add(accInterface);
							}
						 ArrayList<TallyInterfaceBean> list = setInterfaceValues(accountingarray);			 
			              csvservice.setTallyInterfaceData(list, new AsyncCallback<Void>() {
							@Override
							public void onFailure(Throwable caught) {
								// TODO Auto-generated method stub
								System.out.println("RPC call Failed"+caught);
								accDownloadPopup.hidePopUp();
							}
							@Override
							public void onSuccess(Void result) {
								// TODO Auto-generated method stub
								accDownloadPopup.hidePopUp();
								String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
								final String url=gwt + "csvservlet"+"?type="+141;
								Window.open(url, "test", "enabled");
							}
						});
						}
					});	
		}
		if(event.getSource().equals(accDownloadPopup.getLblCancel())){
			accDownloadPopup.hidePopUp();
		}
	}
	/** date 26.11.2018 added by komal for tax wise combined data **/
	private Map<Double , ArrayList<AccountingInterface>> getTaxwiseMap(ArrayList<AccountingInterface> accInterfaceList){
			
		Map<Double , ArrayList<AccountingInterface>> map  = new HashMap<Double , ArrayList<AccountingInterface>>();
		ArrayList<AccountingInterface> list = new ArrayList<AccountingInterface>();
		for(AccountingInterface accInterface  :accInterfaceList){
			double tax = 0;
			if(accInterface.getServiceTaxPercent()!=0){
				tax = accInterface.getServiceTaxPercent();
			}
			if(accInterface.getVatPercent()!=0){
				tax = accInterface.getVatPercent();
			}
			/**
			 * @author Anil , 04-07-2019
			 */
			if(cumalativeTaxAmtFlag){
				tax=0;
			}
			
			if(map.containsKey(tax)){
				list = map.get(tax);
				list.add(accInterface);
				map.put(tax, list);
			}else{
				list = new ArrayList<AccountingInterface>();
				list.add(accInterface);
				map.put(tax, list);
			}
		}
		return map;
	}
	
	
	private void reactOnChangeStatusToCreated() {
		List<AccountingInterface> acclist = form.getSuperTable().getDataprovider().getList();
		if (acclist.size() != 0) {
			ArrayList<AccountingInterface> accInterfaceListb = new ArrayList<AccountingInterface>();
			accInterfaceListb = getSelectedRecords();
			if(accInterfaceListb.size()>1){
				form.showDialogMessage("Please select only one record!");
				return;
			}
			if (accInterfaceListb.size() != 0) {
				for(AccountingInterface accountinginterface : accInterfaceListb){
					accountinginterface.setStatus(AccountingInterface.TALLYCREATED);
				}
				form.showWaitSymbol();
				async.save(accInterfaceListb.get(0), new AsyncCallback<ReturnFromServer>() {
					
					@Override
					public void onSuccess(ReturnFromServer result) {
						// TODO Auto-generated method stub
						form.hideWaitSymbol();
						form.showDialogMessage("Status Updated Successfully");
					}
					
					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						form.hideWaitSymbol();
					}
				});
			}
			else{
				form.showDialogMessage("No Records selected");
			}
		}	
	}
	
	private ArrayList<TallyInterfaceBean> setInterfaceValuesForUltra(ArrayList<AccountingInterface> accountingarray){
		String costCenter="";
		
		ArrayList<TallyInterfaceBean> tallyInterfaceList = new ArrayList<TallyInterfaceBean>();
		Comparator<AccountingInterface> comp=new Comparator<AccountingInterface>() {
			@Override
			public int compare(AccountingInterface e1, AccountingInterface e2) {

				if(e1!=null && e2!=null)
				{
					if( e1.getDocumentType()!=null && e2.getDocumentType()!=null){
						return e1.getDocumentType().compareTo(e2.getDocumentType());}
				}
				else{
					return 0;}
				return 0;

			}
		};
		Collections.sort(accountingarray,comp);

		Comparator<AccountingInterface> comp1=new Comparator<AccountingInterface>() {
			@Override
			public int compare(AccountingInterface e1, AccountingInterface e2) {

				if (e1.getDocumentID() == e2.getDocumentID()) {
					return 0;
				}
				if (e1.getDocumentID() > e2.getDocumentID()) {
					return 1;
				} else {
					return -1;
				}
			}
		};
		Collections.sort(accountingarray,comp1);
		
		
		
		
		HashMap<String , ArrayList<AccountingInterface>> interfaceMap = new HashMap<String , ArrayList<AccountingInterface>>();
		ArrayList<AccountingInterface> list;
		for(AccountingInterface it : accountingarray){
			if(it.getDocumentType().equalsIgnoreCase("Payroll")){
				it.setDocumentID(it.getCount());
			}
			if(interfaceMap.containsKey(it.getDocumentType()+"-"+it.getDocumentID()+"-"+it.getDocumentStatus())){
				list = interfaceMap.get(it.getDocumentType()+"-"+it.getDocumentID()+"-"+it.getDocumentStatus());
                list.add(it);
				interfaceMap.put(it.getDocumentType()+"-"+it.getDocumentID()+"-"+it.getDocumentStatus(), list);
			}else{
				list = new ArrayList<AccountingInterface>();
				list.add(it);
				interfaceMap.put(it.getDocumentType()+"-"+it.getDocumentID()+"-"+it.getDocumentStatus(), list);
			}
		}
		ArrayList<AccountingInterface> accInterfaceList;
		TallyInterfaceBean tallyInterfaceBean;
		List<String> ledgerNameList;
		List<String> groupNameList;
		List<String> hsnSacList;
		List<Double> amountList;
		List<Double> gstList;
		List<String> narrationList;
		
		/**
		 * @author Anil , Date : 30-05-2019
		 */
		List<String> stockItemNameList;
		List<String> stockGroupNameList;
		List<String> stockQtyList;
		List<String> stockRateList;
		/**
		 * @author Anil , Date : 04-07-2019
		 */
		List<String> godownList;
		
		double discount = 0.0 , indirectExpense  = 0.0;
		
		/**
		 * @author Vijay Date :- 20-05-2023
		 * Des :- As per nitin sir first all invoice record and then payment records should download in tally export
		 * so added below list to manage the same
		 */
		ArrayList<TallyInterfaceBean> invoicelist = new ArrayList<TallyInterfaceBean>();
		ArrayList<TallyInterfaceBean> paymentlist = new ArrayList<TallyInterfaceBean>();
		ArrayList<TallyInterfaceBean> otherlist = new ArrayList<TallyInterfaceBean>();

		ArrayList<TallyInterfaceBean> cancelledInvoicelist = new ArrayList<TallyInterfaceBean>();
		ArrayList<TallyInterfaceBean> cancelledPaymentlist = new ArrayList<TallyInterfaceBean>();


		/**
		 * ends here
		 */
		
		for(Map.Entry<String, ArrayList<AccountingInterface>> entry : interfaceMap.entrySet())
		{
			Console.log("key == "+entry.getKey());
			accInterfaceList = entry.getValue();
			AccountingInterface accountingInterface = accInterfaceList.get(0);
			if(accountingInterface.getBranch()!=null)
				costCenter=accountingInterface.getBranch();
			
			
			tallyInterfaceBean = new TallyInterfaceBean();
			ledgerNameList = new ArrayList<String>();
			groupNameList = new ArrayList<String>();
			hsnSacList = new ArrayList<String>();
			amountList = new ArrayList<Double>();
			gstList = new ArrayList<Double>();
			narrationList = new ArrayList<String>();
			
			stockItemNameList=new ArrayList<String>();
			stockGroupNameList=new ArrayList<String>();
			stockQtyList=new ArrayList<String>();
			stockRateList=new ArrayList<String>();
			
			godownList=new ArrayList<String>();
			
            if(accountingInterface.getDocumentType().equalsIgnoreCase("Invoice")){
            	int amtVar=1;
            	int invVar=1;
            	int qtyVar=1;
				if(accountingInterface.getDocumentType()!=null){
				tallyInterfaceBean.setDocumentName(accountingInterface.getDocumentType());
				
				}	
				/** date 28.07.2018 added by komal for accounting interface voucher name column **/
				if(accountingInterface.getAccountType().equalsIgnoreCase(AppConstants.ACCOUNTTYPEAP)){
					tallyInterfaceBean.setInvoiceType("Purchase Invoice");/** date 13.12.2018 changed by komal for sasha(purchase and sales invoice diff)**/
				//	tallyInterfaceBean.setInvoiceType(AppConstants.SALESINVOICE);
					amtVar=-1;
					invVar=-1;
					qtyVar=-1;
				}else if(accountingInterface.getAccountType().equalsIgnoreCase(AppConstants.ACCOUNTTYPE)){
					tallyInterfaceBean.setInvoiceType("Sales-"+costCenter);
				//	tallyInterfaceBean.setInvoiceType(AppConstants.pro);
					amtVar=-1;
					invVar=-1;
					qtyVar=-1;
				}else{
					tallyInterfaceBean.setInvoiceType("Service Invoice");
					amtVar=-1;
					invVar=-1;
					qtyVar=-1;
					
					
					
				}
					
				/*
				 * Date:5/10/2018
				 * Developer:Ashwini
				 * Des:To add branch name as cost center
				 */
				if(accountingInterface.getCompanyId()!=null){
					tallyInterfaceBean.setCompanyId(accountingInterface.getCompanyId());
				}
				
				if(accountingInterface.getBranch()!=null){
					tallyInterfaceBean.setBranch(accountingInterface.getBranch());
				}
				
				/*
				 * end by Ashwini
				 */
					
				if(accountingInterface.getDocumentDate()!=null){
					tallyInterfaceBean.setInvoiceDate(accountingInterface.getDocumentDate());
				}
				
				/**
				 * @author Anil , Date : 05-07-2019
				 */
				if(stockItemFlag&&accountingInterface.getInvoiceRefNum()!=null&&!accountingInterface.getInvoiceRefNum().equals("")){
					tallyInterfaceBean.setSupplierInvoiceNumber(accountingInterface.getInvoiceRefNum());
				}else{
					if(accountingInterface.getReferenceDocumentNo1()!=null){
						tallyInterfaceBean.setSupplierInvoiceNumber(accountingInterface.getReferenceDocumentNo1());
					}
				}
				
				if(accountingInterface.getReferenceDocumentDate1()!=null){
					tallyInterfaceBean.setSupplierDate(accountingInterface.getReferenceDocumentDate1());
				}
				
				/**
				 * @author Anil , Date : 05-07-2019
				 */
				if(stockItemFlag&&accountingInterface.getInvoiceNum()!=null&&!accountingInterface.getInvoiceNum().equals("")){
					tallyInterfaceBean.setInvoiceNumber(accountingInterface.getInvoiceNum());
				}else{
		            if(accountingInterface.getDocumentID()!=0){
		            	tallyInterfaceBean.setInvoiceNumber(accountingInterface.getDocumentID()+"");
		            }
				}
	            if(accountingInterface.getGstn()!=null){
	            	tallyInterfaceBean.setGstn(accountingInterface.getGstn());
	            }
	            if(accountingInterface.getState()!=null){
	            	tallyInterfaceBean.setPlaceToSupply(accountingInterface.getState());
	            }
	           
	            tallyInterfaceBean.setTotalInvoiceAmount(accountingInterface.getNetPayable());
//	            if(stockItemFlag){
//	            	tallyInterfaceBean.setTotalInvoiceAmount(invVar*accountingInterface.getNetPayable());
//	            }
	            
	        	if(accountingInterface.getReferenceDocumentNo1()!=null){
					tallyInterfaceBean.setBillNumber(accountingInterface.getReferenceDocumentNo1());
				}
			
	        	if(accountingInterface.getCustomerName()!=null){
	        		if(costCenter.length()>0) {
	        			if(costCenter.equals("BANDRA"))
	        				ledgerNameList.add(accountingInterface.getCustomerName()+"-BN");
	        			else if(costCenter.equals("TARDEO"))
	        				ledgerNameList.add(accountingInterface.getCustomerName()+"-TR");
	        			else
	        				ledgerNameList.add(accountingInterface.getCustomerName()+"-"+costCenter.charAt(0));
		        			        			
	        		}else
	        			ledgerNameList.add(accountingInterface.getCustomerName());	        		
	        	
	        	}else
	        		ledgerNameList.add("");
	            
				if(accountingInterface.getAccountType().equalsIgnoreCase(AppConstants.ACCOUNTTYPEAP)){
					groupNameList.add("Sundry Creditors ("+costCenter+")");
				}else if(accountingInterface.getAccountType().equalsIgnoreCase(AppConstants.ACCOUNTTYPE)){
					groupNameList.add("Sundry Debtors ("+costCenter+")");
				}
				hsnSacList.add("");
				amountList.add(accountingInterface.getNetPayable());
				gstList.add(0.0);
				narrationList.add("");
				
				/**
				 * @author Anil ,Date : 30-05-2019
				 */
				
				if(stockItemFlag){
					stockItemNameList.add("");
					stockGroupNameList.add("");
					stockQtyList.add("");
					stockRateList.add("");
					
					godownList.add("");
				}
	
				Map<Double , ArrayList<AccountingInterface>> combinedTaxMap = getTaxwiseMap(accInterfaceList); 
	
				for(Map.Entry<Double, ArrayList<AccountingInterface>> entry1 : combinedTaxMap.entrySet()){
					Console.log("entry1 key :" + entry1.getKey() + " entry value : "+ entry1.getValue());
					String taxName1 = "" , 		taxName2 = "" , groupName1 = "" , groupName2 = "";
					double totalTax1 = 0 ,totalTax2 = 0;
					for(AccountingInterface accInterface : entry1.getValue()){
						if(accInterface.getProductName() !=null){
							/**
							 * 
							 */
	//						ledgerNameList.add(accInterface.getProductName());
							if(stockItemFlag){
								ledgerNameList.add(accInterface.getProductCategory());
								stockItemNameList.add(accInterface.getProductName());
								stockGroupNameList.add(accInterface.getProductCategory());
								stockQtyList.add((qtyVar*accInterface.getQuantity())+"");
								stockRateList.add(accInterface.getProductPrice()+"");
								
								if(accInterface.getGodownName()!=null){
									godownList.add(accInterface.getGodownName());
								}else{
									godownList.add("");
								}
								
							}else{
								if(accountingInterface.getGstn()!=null&&!accountingInterface.getGstn().equals("")){
									if(costCenter.length()>0)
										ledgerNameList.add("Service Charges GST -"+costCenter);//accInterface.getProductName()
									else
										ledgerNameList.add("Service Charges GST");//accInterface.getProductName()
									
								}else{
									if(costCenter.length()>0)
										ledgerNameList.add("Service Charges Unregd -"+costCenter);//accInterface.getProductName()
									else
										ledgerNameList.add("Service Charges Unregd");//accInterface.getProductName()
									
								}
								
							}
							/**
							 * 
							 */
						}
						if(accInterface.getTax1()!=null && !(accInterface.getTax1().equalsIgnoreCase(""))){
							taxName1 = "Output "+accInterface.getTax1()+" @ "+accInterface.getServiceTaxPercent()+ " %";
							
							/**
							 * @author Anil ,Date :04-07-2019
							 * changing tax name to default value
							 */
							if(cumalativeTaxAmtFlag&&(accInterface.getTax1().contains("CGST")||accInterface.getTax1().contains("Cgst")||accInterface.getTax1().contains("cgst"))){
								taxName1="Input CGST";
							}
							if(cumalativeTaxAmtFlag&&(accInterface.getTax1().contains("SGST")||accInterface.getTax1().contains("Sgst")||accInterface.getTax1().contains("sgst"))){
								taxName1="Input SGST";
							}
							if(cumalativeTaxAmtFlag&&(accInterface.getTax1().contains("IGST")||accInterface.getTax1().contains("Igst")||accInterface.getTax1().contains("igst"))){
								taxName1="Input IGST";
							}
						}
						
						if(accInterface.getTax2()!=null && !(accInterface.getTax2().equalsIgnoreCase(""))){
							taxName2 = "Output "+accInterface.getTax2()+" @ "+accInterface.getVatPercent()+ " %";
							
							
							/**
							 * @author Anil ,Date :04-07-2019
							 * changing tax name to default value
							 */
							if(cumalativeTaxAmtFlag&&(accInterface.getTax2().contains("CGST")||accInterface.getTax2().contains("Cgst")||accInterface.getTax2().contains("cgst"))){
								taxName2="Input CGST";
							}
							if(cumalativeTaxAmtFlag&&(accInterface.getTax2().contains("SGST")||accInterface.getTax2().contains("Sgst")||accInterface.getTax2().contains("sgst"))){
								taxName2="Input SGST";
							}
							if(cumalativeTaxAmtFlag&&(accInterface.getTax2().contains("IGST")||accInterface.getTax2().contains("Igst")||accInterface.getTax2().contains("igst"))){
								taxName2="Input IGST";
							}
						}
						
						//old code
//						if(accountingInterface.getAccountType().equalsIgnoreCase(AppConstants.ACCOUNTTYPEAP)){
//							groupNameList.add("Purchase Accounts");
//						}else if(accountingInterface.getAccountType().equalsIgnoreCase(AppConstants.ACCOUNTTYPE)){
//							groupNameList.add("Sales Accounts");
//						}
						//set for ultra
						if(accountingInterface.getGstn()!=null&&!accountingInterface.getGstn().equals(""))
							groupNameList.add("Service Charge GST Recd"); 
						else
							groupNameList.add("Service Charge Unregd Recd");
						
						groupName1 = accInterface.getGroupName1();
						groupName2 = accInterface.getGroupName2();
						hsnSacList.add(accInterface.getHsnSac());
						
						/**
						 * 
						 */
	//					amountList.add(-accInterface.getTotalAmount());
						if(stockItemFlag){
							amountList.add(amtVar*accInterface.getTotalAmount());							
						}else{
							double val=Double.parseDouble(df.format(-accInterface.getTotalAmount()));
//							amountList.add(-accInterface.getTotalAmount());		
							amountList.add(val);
						}
						
						
						gstList.add(accInterface.getServiceTaxPercent()+accInterface.getVatPercent());
						
					    totalTax1 += accInterface.getServiceTaxAmount();
					    totalTax2 += accInterface.getVatAmount();
					    String narration = "";
					    /**
					     * Date : 20-12-2018 BY ANIL
					     * Process Name : Invoice
					     * Process Type : InvoiceCommentAsTallyNarration
					     * Ankita Pest Control raised By Sonu
					     * mapping invoice comment as tally narration
					     */
					    if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "InvoiceCommentAsTallyNarration")&&accInterface.getDocumentRemark()!=null){
					    	narration=accInterface.getDocumentRemark();
					    	/**
					    	 * If narration list size is 1 then only we will add narration in narration list
					    	 */
					    	if(narrationList.size()==1){
					    		narrationList.add(narration);
					    	}
					    }else{
							narration = "Interface id : " + accInterface.getCount() + " ";
							if (accInterface.getAccountingInterfaceCreationDate() != null) {
								narration += "Interface Date : " + AppUtility.parseDateTime(accInterface.getAccountingInterfaceCreationDate())+"";
							}
							narrationList.add(narration);
					    }
					    /**
					     * End
					     */
						discount  = accInterface.getDiscount();
						indirectExpense = accInterface.getIndirectExpenses();
					}
					ledgerNameList.add(taxName1);
					ledgerNameList.add(taxName2);
					narrationList.add("");
					narrationList.add("");
				
					hsnSacList.add("");
					hsnSacList.add("");
					
					gstList.add(0.0);
					gstList.add(0.0);
					
					groupNameList.add(groupName1);
					groupNameList.add(groupName2);
					
					/**
					 * 
					 */
	//				amountList.add(-totalTax1);
	//				amountList.add(-totalTax2);
					if(stockItemFlag){
						amountList.add(amtVar*totalTax1);
						Console.log("Ashwini4 amount added to list "+amtVar*totalTax1);
						amountList.add(amtVar*totalTax2);
						Console.log("Ashwini5 amount added to list "+amtVar*totalTax2);
					}else{
						
//						amountList.add(-totalTax1);
						if(totalTax1!=0){
							double val=Double.parseDouble(df.format(-totalTax1));
							amountList.add(val);
						}else{
							amountList.add(-totalTax1);
						}
//						amountList.add(-totalTax2);
						if(totalTax2!=0){
							double val=Double.parseDouble(df.format(-totalTax2));
							amountList.add(val);	
						}else{
							amountList.add(-totalTax2);
						}
					}
					
					/***
					 * 
					 */
					if(stockItemFlag){
						stockItemNameList.add("");
						stockGroupNameList.add("");
						stockQtyList.add("");
						stockRateList.add("");
						godownList.add("");
						
						stockItemNameList.add("");
						stockGroupNameList.add("");
						stockQtyList.add("");
						stockRateList.add("");
						godownList.add("");
					}
					/**
					 * 
					 */
					
				}
//				ledgerNameList.add(AppConstants.DISCOUNT);
//				ledgerNameList.add(AppConstants.DELIVERYCHARGES);
				/**Date 22-9-2020 by Amol added a roundoff row raised by Nittin Sir for pecopp**/
				ledgerNameList.add("Roundoff");
				
				
//				if (accountingInterface.getAccountType().equalsIgnoreCase(AppConstants.ACCOUNTTYPEAP)) {
//					groupNameList.add(AppConstants.INDIRECTEXPENSE);
//					groupNameList.add(AppConstants.INDIRECTEXPENSE);
//				} else if (accountingInterface.getAccountType().equalsIgnoreCase(AppConstants.ACCOUNTTYPE)) {
//					groupNameList.add(AppConstants.INDIRECTINCOMES);
//					groupNameList.add(AppConstants.INDIRECTINCOMES);
//				}
				groupNameList.add("");
				groupNameList.add("");
				
				groupNameList.add("");
				
				narrationList.add("");
				narrationList.add("");  
				narrationList.add("");  
				
				hsnSacList.add("");
				hsnSacList.add("");
				hsnSacList.add("");
				
//				amountList.add(discount);
//				amountList.add(indirectExpense);
//				if(discount!=0){
//					double val=Double.parseDouble(df.format(discount));
//					amountList.add(val);
//				}else{
//					amountList.add(discount);
//				}
//				
//				if(indirectExpense!=0){
//					double val=Double.parseDouble(df.format(indirectExpense));
//					amountList.add(val);
//				}else{
//					amountList.add(indirectExpense);
//				}
				
				double roundOffAmt=0;
				for(double amt:amountList){
					Console.log("TOTAL AMT "+amt);
					roundOffAmt+=amt;
				}
				
				Console.log(" total amt11"+roundOffAmt);
				
				double totAmt=(roundOffAmt*(-1));
				Console.log(" total amt value "+totAmt);
				amountList.add(totAmt);
				
				gstList.add(0.0);
				gstList.add(0.0);
				gstList.add(0.0);
				/***
				 * 
				 */
				if(stockItemFlag){
					stockItemNameList.add("");
					stockGroupNameList.add("");
					stockQtyList.add("");
					stockRateList.add("");
					godownList.add("");
					
					stockItemNameList.add("");
					stockGroupNameList.add("");
					stockQtyList.add("");
					stockRateList.add("");
					godownList.add("");
					
					tallyInterfaceBean.setStockItemNameList(stockItemNameList);
					tallyInterfaceBean.setStockGroupNameList(stockGroupNameList);
					tallyInterfaceBean.setStockRateList(stockRateList);
					tallyInterfaceBean.setStockQtyList(stockQtyList);
					tallyInterfaceBean.setGodownList(godownList);
				}
				/**
				 * 
				 */
				
				
				
				
				
				/**
				 * 
				 */
				Console.log("Ashwini amountList size="+amountList.size());
				tallyInterfaceBean.setAmountList(amountList);
				tallyInterfaceBean.setGroupNameList(groupNameList);
				tallyInterfaceBean.setLedgerNameList(ledgerNameList);
				tallyInterfaceBean.setHsnSacList(hsnSacList);
				tallyInterfaceBean.setNarrationList(narrationList);
				tallyInterfaceBean.setGstList(gstList);
				tallyInterfaceBean.setsAddressLine1(accountingInterface.getAddress());
				tallyInterfaceBean.setsAddressLine2(accountingInterface.getAddress1());
				tallyInterfaceBean.setsLandMark(accountingInterface.getLandmark());
				tallyInterfaceBean.setsLocality(accountingInterface.getLocality());
				tallyInterfaceBean.setsPinNumber(accountingInterface.getPincode()+"");
				tallyInterfaceBean.setsCity(accountingInterface.getCity());
				tallyInterfaceBean.setsCountry(accountingInterface.getCountry());
				tallyInterfaceBean.setsState(accountingInterface.getState());
							
				tallyInterfaceBean.setbAddressLine1(accountingInterface.getSaddress());
				tallyInterfaceBean.setbAddressLine2(accountingInterface.getSaddress2());
				tallyInterfaceBean.setbLandMark(accountingInterface.getSlandmark());
				tallyInterfaceBean.setbLocality(accountingInterface.getSlocality());
				tallyInterfaceBean.setbPinNumber(accountingInterface.getSpincode()+"");
				tallyInterfaceBean.setbCity(accountingInterface.getScity());
				tallyInterfaceBean.setbCountry(accountingInterface.getScountry());
				tallyInterfaceBean.setbState(accountingInterface.getSstate());
				
				tallyInterfaceBean.setDocumentStatus(accountingInterface.getDocumentStatus());
				tallyInterfaceBean.setReferenceNumber(accountingInterface.getReferenceDocumentNo2());
				tallyInterfaceBean.setEinvoiceNo(accountingInterface.getEinvoiceNo());
				tallyInterfaceBean.setEinvoiceStatus(accountingInterface.getEinvoiceStatus());
				tallyInterfaceBean.setEinvoiceAckNo(accountingInterface.getEinvoiceAckNo());
				if(accountingInterface.getEinvoiceAckDate()!=null)
				tallyInterfaceBean.setEinvoiceAckDate(accountingInterface.getEinvoiceAckDate());
				tallyInterfaceBean.setEinvoiceQRCodeString(accountingInterface.getEinvoiceQRCodeString());
				tallyInterfaceBean.setEinvoiceCancellationDate(accountingInterface.getEinvoiceCancellationDate());
				
				if(accountingInterface.getNumberRange()!=null){
					tallyInterfaceBean.setNumberRange(accountingInterface.getNumberRange());
				}
			
//				tallyInterfaceList.add(tallyInterfaceBean);

			
				if(tallyInterfaceBean.getDocumentStatus().equals(Invoice.CANCELLED)) {
					Console.log("Cancelled Invoice");
					cancelledInvoicelist.add(tallyInterfaceBean);
				}
				else {
					invoicelist.add(tallyInterfaceBean);
				}
				
			
			
           }else if(accountingInterface.getDocumentType().equalsIgnoreCase("Customer Payment")){
        		for(AccountingInterface accInterface : accInterfaceList){
        			 if(accInterface.getDocumentType()!=null){
        					tallyInterfaceBean.setDocumentName(accInterface.getDocumentType());
        			 }
        			// tallyInterfaceBean.setInvoiceType(AppConstants.PAYMENT1);
        				/** date 28.07.2018 added by komal for accounting interface voucher name column **/
        				tallyInterfaceBean.setInvoiceType(AppConstants.RECEIPT+" - "+costCenter);
        				if(accountingInterface.getReferenceDocumentNo1()!=null){
        					tallyInterfaceBean.setSupplierInvoiceNumber(accountingInterface.getReferenceDocumentNo1());
        				}
        				if(accountingInterface.getCompanyId()!=null){
        					tallyInterfaceBean.setCompanyId(accInterface.getCompanyId());
        				}
        				
        				if(accountingInterface.getBranch()!=null){
        					tallyInterfaceBean.setBranch(accInterface.getBranch());
        				}
        				if(accountingInterface.getReferenceDocumentDate1()!=null){
        					tallyInterfaceBean.setSupplierDate(accountingInterface.getReferenceDocumentDate1());
        				}
        	            if(accountingInterface.getDocumentID()!=0){
        	            	tallyInterfaceBean.setInvoiceNumber(accountingInterface.getDocumentID()+"");
        	            }

        			 if(accInterface.getPaymentDate()!=null){
        				 tallyInterfaceBean.setInvoiceDate(accInterface.getPaymentDate());
        			 }
        			 if(accInterface.getBankName()!=null){
        				 tallyInterfaceBean.setBankName(accInterface.getBankName());
        			 }
        	         if(accInterface.getCustomerName()!=null) {
        	        	 if(costCenter.equals("BANDRA"))
 	        				ledgerNameList.add(accInterface.getCustomerName()+"-BN");
 	        			else if(costCenter.equals("TARDEO"))
 	        				ledgerNameList.add(accInterface.getCustomerName()+"-TR");
 	        			else
 	        				ledgerNameList.add(accInterface.getCustomerName()+"-"+costCenter.charAt(0));
        	         }else
        	            ledgerNameList.add("");
        			
        	         if(accInterface.getPaymentMethod().equalsIgnoreCase("cash")){
        	        	 ledgerNameList.add("Cash - "+costCenter);
        	         }
        	         else if(accInterface.getDocumentRemark()!=null && !accInterface.getDocumentRemark().equals("")){
        	        	 ledgerNameList.add(accInterface.getDocumentRemark());
        	         }else{
        	        	 ledgerNameList.add("");
        	         }
        	         ledgerNameList.add(AppConstants.TDSFEES); //commented on 20-03-2023
//        			if(accInterface.getBankAccount()!=null){
//        				tallyInterfaceBean.setBankAccountNo(accInterface.getBankAccount());
//        			}
        			if(accInterface.getChequeNumber()!=null){
        				tallyInterfaceBean.setChequeNumber(accInterface.getChequeNumber());
        			}
        			if(accInterface.getChequeDate()!=null){
        				tallyInterfaceBean.setChequeDate(accInterface.getChequeDate());
        			}
        			if(accInterface.getReferenceDocumentNo1()!=null){
        				tallyInterfaceBean.setBillNumber(accInterface.getReferenceDocumentNo1());
        			}
        			amountList.add(accInterface.getAmountRecieved());
        			amountList.add(-(accInterface.getAmountRecieved()-accInterface.getTdsAmount()));
        			amountList.add(-accInterface.getTdsAmount()); //commented on 20-03-2023
        			String narration = "";
//    				narration = "Interface id : " + accInterface.getCount() + " ";
//    				
//    				if (accInterface.getAccountingInterfaceCreationDate() != null) {
//    					narration += "Interface Date : " + AppUtility.parseDateTime(accInterface.getAccountingInterfaceCreationDate())+" ";
//    				}
        			/**
        			 * Date : 20-12-2018 By ANIL
        			 * mapped customer payment as tally narration if below process configuration is active
        			 * Process Name : CustomerPayment
        			 * Process Type : PaymentCommentAsTallyNarration
        			 * For Ankita Pest Control raised BY Sonu
        			 */
        			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CustomerPayment", "PaymentCommentAsTallyNarration")){
        				narration=accInterface.getConractCategory();
        			}else{
        				narration +=accInterface.getPaymentMethod();
        			}
        			/**
        			 * End
        			 */
    				narrationList.add(narration);
    				narrationList.add("");
//    				narrationList.add("");//commented on 20-03-2023
    				if(accountingInterface.getAccountType().equalsIgnoreCase(AppConstants.ACCOUNTTYPEAP)){
    					groupNameList.add("Sundry Creditors ("+costCenter+")");
    					}else if(accountingInterface.getAccountType().equalsIgnoreCase(AppConstants.ACCOUNTTYPE)){
    						groupNameList.add("Sundry Debtors ("+costCenter+")");
    					}
    				if(accInterface.getPaymentMethod().equalsIgnoreCase("cash"))
    					groupNameList.add("Cash-in-Hand");
    				else
    					groupNameList.add("Bank Account");
    				groupNameList.add(AppConstants.DIRECTEXPENSE);//commented on 20-03-2023
    				tallyInterfaceBean.setGroupNameList(groupNameList);
    				tallyInterfaceBean.setAmountList(amountList);
    				tallyInterfaceBean.setLedgerNameList(ledgerNameList);
    				tallyInterfaceBean.setNarrationList(narrationList);
    				
    				tallyInterfaceBean.setDocumentStatus(accountingInterface.getDocumentStatus());
    				tallyInterfaceBean.setReferenceNumber(accountingInterface.getReferenceDocumentNo2());
    				
//    				tallyInterfaceList.add(tallyInterfaceBean);
    				
    				if(accountingInterface.getNumberRange()!=null){
    					tallyInterfaceBean.setNumberRange(accountingInterface.getNumberRange());
    				}
    				if(tallyInterfaceBean.getDocumentStatus().equals(Invoice.CANCELLED)) {
    					Console.log("Cancelled Payment");
    					cancelledPaymentlist.add(tallyInterfaceBean);
    				}
    				else {
    					paymentlist.add(tallyInterfaceBean);

    				}
    				
        		}
           }else if(accountingInterface.getDocumentType().equalsIgnoreCase("Expense Management")){
        	   for(AccountingInterface accInterface : accInterfaceList){
        	   tallyInterfaceBean.setInvoiceType(AppConstants.JOURNAL);	   
        	   tallyInterfaceBean.setTotalInvoiceAmount(accInterface.getNetPayable());
        	   tallyInterfaceBean.setDocumentName(accountingInterface.getDocumentType());
        	   tallyInterfaceBean.setVoucherNumber(accInterface.getDocumentID()+"");
        	   tallyInterfaceBean.setExpenseDate(accInterface.getDocumentDate());
        	   ledgerNameList.add("Sundry Expenses");
        	   ledgerNameList.add(accInterface.getReferenceDocumentType2());
        	   narrationList.add(accInterface.getConractCategory());
        	   tallyInterfaceBean.setBillNumber(accInterface.getReferenceDocumentNo2());
        	   tallyInterfaceBean.setLedgerNameList(ledgerNameList);
			   tallyInterfaceBean.setDocumentStatus(accountingInterface.getDocumentStatus());
        	   tallyInterfaceBean.setNarrationList(narrationList);
//        	   tallyInterfaceList.add(tallyInterfaceBean);
				otherlist.add(tallyInterfaceBean);

        	   }
           }else if(accountingInterface.getDocumentType().equalsIgnoreCase("Payroll")){
        	   for(AccountingInterface accInterface : accInterfaceList){
        		   tallyInterfaceBean.setInvoiceType(AppConstants.JOURNAL);	   
            	   tallyInterfaceBean.setDocumentName(accountingInterface.getDocumentType());
            	   tallyInterfaceBean.setVoucherNumber(accInterface.getCount()+"");
            	   tallyInterfaceBean.setInvoiceDate(accInterface.getDocumentDate());
            	   tallyInterfaceBean.setInvoiceNumber(accInterface.getDocumentGL());
            	   List<GeneralKeyValueBean> beanList = accInterface.getPayRollList();
//            	   Comparator<GeneralKeyValueBean> comp2=new Comparator<GeneralKeyValueBean>() {
//           			@Override
//           			public int compare(GeneralKeyValueBean e1, GeneralKeyValueBean e2) {
//
//           				if (e1.getValue() == e2.getValue()) {
//           					return 0;
//           				}
//           				if (e1.getValue() > e2.getValue()) {
//           					return 1;
//           				} else {
//           					return -1;
//           				} 
//           			}
//           		};
//           		Collections.sort(beanList,comp2);
           		ledgerNameList.add("Salaries and Wages -"+accInterface.getAccountType());
           		amountList.add(accInterface.getTotalAmount());
           		groupNameList.add(AppConstants.INDIRECTEXPENSE);
            	   for(GeneralKeyValueBean bean : beanList){
            		   if(bean.isStatus()){
            			   ledgerNameList.add(bean.getGroupName());
            			   amountList.add(bean.getValue());
            			   groupNameList.add(AppConstants.INDIRECTEXPENSE);
            		   }
            	   }
            	   ledgerNameList.add("Salaries  Payable -"+accInterface.getBranch());
            	   amountList.add(-accInterface.getNetPayable());
            	   groupNameList.add(AppConstants.INDIRECTEXPENSE);
            	   for(GeneralKeyValueBean bean : beanList){
            		   if(!bean.isStatus()){
            			   ledgerNameList.add(bean.getGroupName());
            			   amountList.add(bean.getValue());
            			   groupNameList.add(AppConstants.INDIRECTEXPENSE);
            		   }
            	   }
            	tallyInterfaceBean.setGroupNameList(groupNameList);
   				tallyInterfaceBean.setAmountList(amountList);
   				tallyInterfaceBean.setLedgerNameList(ledgerNameList);
   				tallyInterfaceBean.setBranch(accInterface.getBranch());
				tallyInterfaceBean.setDocumentStatus(accountingInterface.getDocumentStatus());
					
//   				tallyInterfaceList.add(tallyInterfaceBean);
   				
				otherlist.add(tallyInterfaceBean);

        	   }
           }
		}
//		return tallyInterfaceList;
		
		if(invoicelist.size()>0){
			tallyInterfaceList.addAll(invoicelist);
		}
		if(paymentlist.size()>0){
			tallyInterfaceList.addAll(paymentlist);
		}
		
		Console.log("Cancelled Invoice size"+cancelledInvoicelist.size());
		if(cancelledInvoicelist.size()>0) {
			tallyInterfaceList.addAll(cancelledInvoicelist);
		}
		if(cancelledPaymentlist.size()>0) {
			tallyInterfaceList.addAll(cancelledPaymentlist);
		}
		Console.log("Cancelled Payment size"+cancelledPaymentlist.size());

		
		if(otherlist.size()>0){
			tallyInterfaceList.addAll(otherlist);
		}
		
		return tallyInterfaceList;
		
	}	
	
}
