package com.slicktechnologies.client.views.accountinginterface;

import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.views.contract.CancelContractTable;
import com.slicktechnologies.shared.common.contactidentification.ContactPersonIdentification;
import com.slicktechnologies.shared.common.tallyaccounts.AccountingInterface;

public class AccountingInterfaceDownloadPopup extends PopupScreen  implements ValueChangeHandler<Date>{
	DateBox dbSyncDate;
	ObjectListBox<String> olbUniqueId;
	
	public AccountingInterfaceDownloadPopup(){
		super();
		getPopup().setWidth("400px");
		getPopup().setHeight("200px");
		dbSyncDate.addValueChangeHandler(this);
		createGui();
	
	}
	
	private void initializeWidget() {
		 dbSyncDate =  new DateBoxWithYearSelector();
		 olbUniqueId = new ObjectListBox<String>();

	}

	@Override
	public void createScreen() {
		// TODO Auto-generated method stub
		
		initializeWidget();
		
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingDownloadPopup=fbuilder.setlabel("Download Popup").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("* Sync. Date", dbSyncDate);
		FormField fdbSyncDate= fbuilder.setMandatory(true).setMandatoryMsg("Sync. Date is Mandatory").setRowSpan(0).setColSpan(1).build();
		
		
		fbuilder = new FormFieldBuilder("Document Unique Id", olbUniqueId);
		FormField folbUniqueId= fbuilder.setMandatory(true).setMandatoryMsg("Document Unique Id is Mandatory").setRowSpan(0).setColSpan(1).build();
		
		FormField[][] formfield = {  
				{fgroupingDownloadPopup},
				{fdbSyncDate , folbUniqueId}
			
		};
		this.fields=formfield;
	}

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
	}

	public DateBox getDbSyncDate() {
		return dbSyncDate;
	}

	public void setDbSyncDate(DateBox dbSyncDate) {
		this.dbSyncDate = dbSyncDate;
	}

	public ObjectListBox<String> getOlbUniqueId() {
		return olbUniqueId;
	}

	public void setOlbUniqueId(ObjectListBox<String> olbUniqueId) {
		this.olbUniqueId = olbUniqueId;
	}

	@Override
	public void onValueChange(ValueChangeEvent<Date> event) {
		// TODO Auto-generated method stub
		MyQuerry qu = new MyQuerry();
		Vector filtervec = new Vector();
		Filter filter= null;
//		filter = new Filter();
//		filter.setQuerryString("companyId");
//		filter.setLongValue(UserConfiguration.getCompanyId());
//		filtervec.add(filter);
//		
		if(dbSyncDate.getValue()!=null){
			filter = new Filter();
			filter.setQuerryString("dateofSynch >=");
			filter.setDateValue(dbSyncDate.getValue());
			filtervec.add(filter);


			filter = new Filter();
			filter.setQuerryString("dateofSynch <=");
			filter.setDateValue(dbSyncDate.getValue());
			filtervec.add(filter);
		}
		qu.setFilters(filtervec);
		System.out.println("Query string:" + qu);
		qu.setQuerryObject(new AccountingInterface());
		olbUniqueId.MakeLiveUniqueItems(qu);

	}	
}
