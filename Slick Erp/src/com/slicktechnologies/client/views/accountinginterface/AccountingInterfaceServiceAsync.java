package com.slicktechnologies.client.views.accountinginterface;

import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.slicktechnologies.shared.common.tallyaccounts.AccountingInterface;

public interface AccountingInterfaceServiceAsync {

	public void getAccountingInterfaceDataSynched(List<AccountingInterface> acclist, String syncStatus, String syncBy, AsyncCallback<Void> callback);
	
	public void updateSynchDataWithSynchDate(List<AccountingInterface> acclist, AsyncCallback<Void> callback);
}
