package com.slicktechnologies.client.views.accountinginterface;
import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.LicenseDetails;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.tallyaccounts.AccountingInterface;

public interface IntegrateSyncServiceAsync {
	
	void callInterFace(String docType, AccountingInterface entityAI,AsyncCallback<String> asyncCallback);
	
	/**
	 *Date : 09-04-2018 By Anil
	 * 
	 */
	void callLicenseUpdateInteface(Contract contract,String actionTask,AsyncCallback<String> asyncCallback);

	void getIRN(Invoice invoice,String loggedInUser,String taskId,boolean bulkGenerationFlag, AsyncCallback<String> callback);

	void cancelIRN(Invoice invoice, AsyncCallback<String> callback);
	
	void getLicenseInfo(Customer customer, String actionTask, AsyncCallback<ArrayList<LicenseDetails>> callback);
	
	void generateEinvoicesInBulk(long companyId,Date fromDate,Date toDate,ArrayList<Invoice> invoiceList,String loggedInUser,AsyncCallback<String> callback);//Ashwini Patil Date:31-08-2023 for pecopp

}
