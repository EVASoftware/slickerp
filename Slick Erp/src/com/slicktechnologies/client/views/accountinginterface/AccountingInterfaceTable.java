package com.slicktechnologies.client.views.accountinginterface;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.user.cellview.client.Header;
import com.google.code.p.gwtchismes.client.GWTCGlassPanel;
import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.ValueUpdater;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.client.Timer;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.tallyaccounts.AccountingInterface;

public class AccountingInterfaceTable extends SuperTable <AccountingInterface> {TextColumn<AccountingInterface>accountingInterfaceIDColumn;
				
/*
 * Added by Rahul on 9 feb 2017
 * 
 */
				private Column<AccountingInterface, Boolean> chkselectRecords;
				
				Header<Boolean> selectAllHeader;
				GWTCGlassPanel glassPanel;

				TextColumn<AccountingInterface>accountingInterfaceCreationDateColumn;
				TextColumn<AccountingInterface>accountingInterfaceCreatedByColumn;
				TextColumn<AccountingInterface>statusColumn;
				TextColumn<AccountingInterface>remarkColumn;
				TextColumn<AccountingInterface>dateofSyncColumn;
				TextColumn<AccountingInterface>synchedByColumn;
				TextColumn<AccountingInterface>moduleColumn;
				
				TextColumn<AccountingInterface>documentTypeColumn;
				TextColumn<AccountingInterface>documentIDColumn;
				TextColumn<AccountingInterface>documentTitleColumn;
				TextColumn<AccountingInterface>documentDateColumn;
				TextColumn<AccountingInterface>documentGlColumn;
				TextColumn<AccountingInterface>referenceDocumentNumber1Column;
				TextColumn<AccountingInterface>referenceDocumentDate1Column;
				TextColumn<AccountingInterface>referenceDocument1TypeColumn;
				TextColumn<AccountingInterface>referenceDocumentNumber2Column;
				TextColumn<AccountingInterface>referenceDocumentDate2Column;
				TextColumn<AccountingInterface>referenceDocument2TypeColumn;
				TextColumn<AccountingInterface>accountTypeColumn;
				
				TextColumn<AccountingInterface>customerIDColumn;
				TextColumn<AccountingInterface>customerNameColumn;
				TextColumn<AccountingInterface>customerCellColumn;
				TextColumn<AccountingInterface>vendorIDColumn;
				TextColumn<AccountingInterface>vendorNameColumn;
				TextColumn<AccountingInterface>vendorCellColumn;
				TextColumn<AccountingInterface>employeeIDColumn;
				TextColumn<AccountingInterface>employeeNameColumn;
				TextColumn<AccountingInterface>BranchColumn;
				TextColumn<AccountingInterface>personResponsibleColumn;
				TextColumn<AccountingInterface>requestedByColumn;
				TextColumn<AccountingInterface>approvedByColumn;
				TextColumn<AccountingInterface>paymentMethodColumn;
				TextColumn<AccountingInterface>paymentDateColumn;
				TextColumn<AccountingInterface>chequeNoColumn;
				TextColumn<AccountingInterface>chequeDateColumn;
				TextColumn<AccountingInterface>bankNameColumn;
				TextColumn<AccountingInterface>bankAccountnoColumn;
				TextColumn<AccountingInterface>transferReferenceNumberColumn;
				TextColumn<AccountingInterface>transactionDateColumn;
				TextColumn<AccountingInterface>contractStartDateColumn;
				TextColumn<AccountingInterface>contractEndDateColumn;
				TextColumn<AccountingInterface>productIDColumn;
				TextColumn<AccountingInterface>productCodeColumn;
				TextColumn<AccountingInterface>productNameColumn;
				TextColumn<AccountingInterface>quantityColumn;
				TextColumn<AccountingInterface>productDateColumn;
				TextColumn<AccountingInterface>durationColumn;
				TextColumn<AccountingInterface>servicesColumn;
				TextColumn<AccountingInterface>unitOfMeasurementColumn;
				TextColumn<AccountingInterface>productPriceColumn;
				TextColumn<AccountingInterface>totalAmountColumn;
				TextColumn<AccountingInterface>VATPercentColumn;
				TextColumn<AccountingInterface>VATamountColumn;
				TextColumn<AccountingInterface>VATglAccountColumn;
				TextColumn<AccountingInterface>serviceTaxPercentColumn;
				TextColumn<AccountingInterface>serviceTaxAmountColumn;
				TextColumn<AccountingInterface>serviceTaxGLaccountColumn;
				TextColumn<AccountingInterface>cFormColumn;
				TextColumn<AccountingInterface>cFormPercentColumn;
				TextColumn<AccountingInterface>cFormAmountColumn;
				TextColumn<AccountingInterface>cFormGlAccountColumn;
				
				
				TextColumn<AccountingInterface>netPayableColumn;
				
				//Added by Rahul on 11/12/2016 for NBHC which will be used for SAP integration o 
				TextColumn<AccountingInterface>contractCategoryColumn;
				//Done Adding by Rahul
				
				TextColumn<AccountingInterface>amountRecievedColumn;
				
				//  rohan added this 
				TextColumn<AccountingInterface>baseAmount;
				TextColumn<AccountingInterface>tdsPercentage;
				TextColumn<AccountingInterface>tdsAmount;
				
				
				TextColumn<AccountingInterface>otherCharges1Column;
				TextColumn<AccountingInterface>otherChargesAmount1Column;
				TextColumn<AccountingInterface>otherCharges2Column;
				TextColumn<AccountingInterface>otherChargesAmount2Column;
				TextColumn<AccountingInterface>otherCharges3Column;
				TextColumn<AccountingInterface>otherChargesAmount3Column;
				TextColumn<AccountingInterface>otherCharges4Column;
				TextColumn<AccountingInterface>otherChargesAmount4Column;
				TextColumn<AccountingInterface>otherCharges5Column;
				TextColumn<AccountingInterface>otherChargesAmount5Column;
				TextColumn<AccountingInterface>otherCharges6Column;
				TextColumn<AccountingInterface>otherChargesAmount6Column;
				TextColumn<AccountingInterface>otherCharges7Column;
				TextColumn<AccountingInterface>otherChargesAmount7Column;
				TextColumn<AccountingInterface>otherCharges8Column;
				TextColumn<AccountingInterface>otherChargesAmount8Column;
				TextColumn<AccountingInterface>otherCharges9Column;
				TextColumn<AccountingInterface>otherChargesAmount9Column;
				TextColumn<AccountingInterface>otherCharges10Column;
				TextColumn<AccountingInterface>otherChargesAmount10Column;
				TextColumn<AccountingInterface>otherCharges11Column;
				TextColumn<AccountingInterface>otherChargesAmount11Column;
				TextColumn<AccountingInterface>otherCharges12Column;
				TextColumn<AccountingInterface>otherChargesAmount12Column;
				TextColumn<AccountingInterface>addressColumn;
				TextColumn<AccountingInterface>localityColumn;
				TextColumn<AccountingInterface>landmarkColumn;
				TextColumn<AccountingInterface>countryColumn;
				TextColumn<AccountingInterface>stateColumn;
				TextColumn<AccountingInterface>cityColumn;
				TextColumn<AccountingInterface>pincodeColumn;
				TextColumn<AccountingInterface>documentStatusColumn;
				
				TextColumn<AccountingInterface>billingPeriodStartDateColumn;
				TextColumn<AccountingInterface>billingPeriodEndDateColumn;
				
				TextColumn<AccountingInterface>customerRefNumber1Column;
				
				/*
				 * Added by Rahul
				 */
				TextColumn<AccountingInterface> warehouseColumn;
				TextColumn<AccountingInterface> warehouseCodeColumn;
				TextColumn<AccountingInterface> productRefIdColumn;
				TextColumn<AccountingInterface> directionColumn;
				TextColumn<AccountingInterface> sourceSystemColumn;
				TextColumn<AccountingInterface> destinationSystemColumn;
				
				NumberFormat nf=NumberFormat.getFormat("0.00");
				
				
				TextColumn<AccountingInterface> einvoiceNoColumn;
				TextColumn<AccountingInterface> einvoiceStatus;
				TextColumn<AccountingInterface> einvoiceAcknowledgementNo;
				TextColumn<AccountingInterface> einvoiceAcknowledgementDate;
				TextColumn<AccountingInterface> einvoiceCancellationDate;
				TextColumn<AccountingInterface> einvoiceQRCodeString;
				
				TextColumn<AccountingInterface> referenceNumber;

				
				@Override
				public void createTable() {
					
					addColumnCheckBox();
					addcolumngetaccountingInterfaceID();
					addcolumngetaccountingInterfaceCreationDate();
					addcolumngetaccountingInterfaceCreatedBy();
					addColumnDocumentStatus();
					addColumngetstatus();
					addColumngetremark();
					addColumngetdateofSync();
					addColumngetsynchedBy();
					addColumngetmodule();
					addColumngetdocumentType();
					addColumngetdocumentID();
					
					/**
					 * Order updated for customer by Rahul as it was required by NBHC
					 * Date : 10 Feb 2017
					 */
					addColumngetcustomerID();
					addColumngetcustomerName();
					addColumngetcustomerCell();
					/**
					 * Done changes by Rahul till here
					 */
					
					addColumngetdocumentTitle();
					addColumngetdocumentDate();
					addColumngetdocumentGl();
					addColumngetreferenceDocumentNumber1();
					addColumngetreferenceDocumentDate1();
					addColumngetreferenceDocumentType1();
					addColumngetreferenceDocumentNumber2();
					addColumngetreferenceDocumentDate2();
					addColumngetreferenceDocumentType2();
					addColumngetaccountType();
//					addColumngetcustomerID();
//					addColumngetcustomerName();
//					addColumngetcustomerCell();
					addColumngetvendorID();
					addColumngetvendorName();
					addColumngetvendorCell();
					addColumngetemployeeID();
					addColumngetemployeeName();
					addColumngetBranch();
					addColumngetpersonResponsible();
					addColumngetrequestedBy();
					addColumngetapprovedBy();
					addColumngetpaymentMethod();
					addColumngetpaymentDate();
					addColumngetchequeNo();
					addColumngetchequeDate();
					addColumngetbankName();
					addColumngetbankAccountNo();
					addColumngettransferReferenceNumber();
					addColumngettransactionDate();
					addColumngetContractStartDate();
					addColumngetContractEndDate();
					addColumngetproductID();
					addColumngetproductCode();
					addColumngetproductName();
					addColumngetquantity();
					addColumngetproductDate();
					addColumngetduration();
					addColumngetservices();
					addColumngetunitOfMeasurement();
					addColumngetproductPrice();
					addColumngettotalAmount();
					addColumngetVATpercent();
					addColumngetVATamount();
					addColumngetVATglAccount();
					addColumngetserviceTaxPercent();
					addColumngetserviceTaxAmount();
					addColumngetserviceTaxGLAccount();
					addColumngetcForm();
					addColumngetcFormpercent();
					addColumngetcFormAmount();
					addColumngetcFormGlAccount();
					addColumngetnetPayable();
					
					addColumngetContractCategory();
					
					addColumngetamountRecieved();
					
					addColumngetBaseAmount();
					addColumngettdsPercentage();
					addColumngettdsAmount();
					
					addColumngetotherCharges1();
					addColumngetotherChargesAmount1();
					addColumngetotherCharges2();
					addColumngetotherChargesAmount2();
					addColumngetotherCharges3();
					addColumngetotherChargesAmount3();
					addColumngetotherCharges4();
					addColumngetotherChargesAmount4();
					addColumngetotherCharges5();
					addColumngetotherChargesAmount5();
					addColumngetotherCharges6();
					addColumngetotherChargesAmount6();
					addColumngetotherCharges7();
					addColumngetotherChargesAmount7();
					addColumngetotherCharges8();
					addColumngetotherChargesAmount8();
					addColumngetotherCharges9();
					addColumngetotherChargesAmount9();
					addColumngetotherCharges10();
					addColumngetotherChargesAmount10();
					addColumngetotherCharges11();
					addColumngetotherChargesAmount11();
					addColumngetotherCharges12();
					addColumngetotherChargesAmount12();
					addColumngetaddress();
					addColumngetlocality();
					addColumngetlandmark();
					addColumngetcountry();
					addColumngetstate();
					addColumngetcity();
					addColumngetpincode();
					// rohan added this code on Date 3/3/2017
					addColumnBillingPeriodStartDate();
					addColumnBillingPerioEndDate();
					
					//  rohan added this column for NBHC on date 17-04-2017
					addColumnCustmerRefNumber1();
					//  rahul added this code for NBHC on date : 24-may-2017
					addColumngetWarehouse();
					addColumngetWarehouseCode();
					addColumngetProductRef();
					addColumngetdirection();
					addColumngetsourceSystem();
					addColumngetDestinationSystem();
					
					addColumnEInvoiceNo();
					addColumnEinvoiceStatus();
					addColumnEinvoiceAckNo();
					addColumnEinvoiceAckDate();
					addColumnEinvoiceCancellationDate();
					addColumnInvoiceQrCodeColumn();
				}


				private void addColumnEInvoiceNo() {
					einvoiceNoColumn = new TextColumn<AccountingInterface>() {
						
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getEinvoiceNo()!=null){
								return object.getEinvoiceNo();
							}
							else{
								return "";
							}
						}
					};
					table.addColumn(einvoiceNoColumn, "EInvoice No");
					table.setColumnWidth(einvoiceNoColumn, 100, Unit.PX);
					einvoiceNoColumn.setSortable(true);
				}


				private void addColumnEinvoiceStatus() {

					einvoiceStatus = new TextColumn<AccountingInterface>() {
						
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getEinvoiceStatus()!=null){
								return object.getEinvoiceStatus();
							}
							else{
								return "";
							}
						}
					};
					table.addColumn(einvoiceStatus, "EInvoice Status");
					table.setColumnWidth(einvoiceStatus, 100, Unit.PX);
					einvoiceStatus.setSortable(true);
				}


				private void addColumnEinvoiceAckNo() {
					einvoiceAcknowledgementNo = new TextColumn<AccountingInterface>() {
						
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getEinvoiceAckNo()!=null){
								return object.getEinvoiceAckNo();
							}
							else{
								return "";
							}
						}
					};
					table.addColumn(einvoiceAcknowledgementNo, "EInvoice Acknowledgement No");
					table.setColumnWidth(einvoiceAcknowledgementNo, 100, Unit.PX);
					einvoiceAcknowledgementNo.setSortable(true);
				}


				private void addColumnEinvoiceAckDate() {
					einvoiceAcknowledgementDate = new TextColumn<AccountingInterface>() {
						
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getEinvoiceAckDate()!=null){
								return object.getEinvoiceAckDate();
							}
							else{
								return "";
							}
						}
					};
					table.addColumn(einvoiceAcknowledgementDate, "EInvoice Acknowledgement Date");
					table.setColumnWidth(einvoiceAcknowledgementDate, 100, Unit.PX);
					einvoiceAcknowledgementDate.setSortable(true);
				}


				private void addColumnEinvoiceCancellationDate() {

					einvoiceCancellationDate = new TextColumn<AccountingInterface>() {
						
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getEinvoiceCancellationDate()!=null){
								return object.getEinvoiceCancellationDate();
							}
							else{
								return "";
							}
						}
					};
					table.addColumn(einvoiceCancellationDate, "EInvoice Cancellation Date");
					table.setColumnWidth(einvoiceCancellationDate, 100, Unit.PX);
					einvoiceCancellationDate.setSortable(true);
				}


				private void addColumnInvoiceQrCodeColumn() {
					einvoiceQRCodeString = new TextColumn<AccountingInterface>() {
						
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getEinvoiceQRCodeString()!=null){
								return object.getEinvoiceQRCodeString();
							}
							else{
								return "";
							}
							
						}
					};
					table.addColumn(einvoiceQRCodeString, "EInvoice QR Code String");
					table.setColumnWidth(einvoiceQRCodeString, 100, Unit.PX);
					einvoiceQRCodeString.setSortable(true);
				}


				private void addColumngetDestinationSystem() {
					// TODO Auto-generated method stub
					destinationSystemColumn = new TextColumn<AccountingInterface>() {

						@Override
						public String getValue(AccountingInterface object) {
							if (object.getDestinationSystem() != null)
								return object.getDestinationSystem();
							else
								return "";
						}
					};
					table.addColumn(destinationSystemColumn, "Destination System");
					table.setColumnWidth(destinationSystemColumn, 100, Unit.PX);
					destinationSystemColumn.setSortable(true);
				}

				private void addColumngetsourceSystem() {
					// TODO Auto-generated method stub
					sourceSystemColumn = new TextColumn<AccountingInterface>() {

						@Override
						public String getValue(AccountingInterface object) {
							if (object.getSourceSystem() != null)
								return object.getSourceSystem();
							else
								return "";
						}
					};
					table.addColumn(sourceSystemColumn, "Source System");
					table.setColumnWidth(sourceSystemColumn, 100, Unit.PX);
					sourceSystemColumn.setSortable(true);
				}

				
				private void addColumngetdirection() {
					// TODO Auto-generated method stub
					directionColumn = new TextColumn<AccountingInterface>() {

						@Override
						public String getValue(AccountingInterface object) {
							if (object.getDirection() != null)
								return object.getDirection();
							else
								return "";
						}
					};
					table.addColumn(directionColumn, "Direction");
					table.setColumnWidth(directionColumn, 100, Unit.PX);
					directionColumn.setSortable(true);
				}

				private void addColumngetProductRef() {
					// TODO Auto-generated method stub
					productRefIdColumn = new TextColumn<AccountingInterface>() {

						@Override
						public String getValue(AccountingInterface object) {
							if (object.getProductRefId() != null)
								return object.getProductRefId();
							else
								return "";
						}
					};
					table.addColumn(productRefIdColumn, "Product Ref Id");
					table.setColumnWidth(productRefIdColumn, 100, Unit.PX);
					productRefIdColumn.setSortable(true);
				}
	
				private void addColumngetWarehouseCode() {
					// TODO Auto-generated method stub
					warehouseCodeColumn = new TextColumn<AccountingInterface>() {

						@Override
						public String getValue(AccountingInterface object) {
							// TODO Auto-generated method stub
							if (object.getWareHouseCode() != null)
								return object.getWareHouseCode();
							else
								return "";
						}
					};
					table.addColumn(warehouseCodeColumn, "Warehouse Code");
					table.setColumnWidth(warehouseCodeColumn, 100, Unit.PX);
					warehouseCodeColumn.setSortable(true);
				}

				private void addColumngetWarehouse() {
					// TODO Auto-generated method stub
					warehouseColumn = new TextColumn<AccountingInterface>() {

						@Override
						public String getValue(AccountingInterface object) {
							// TODO Auto-generated method stub
							if (object.getWareHouse() != null)
								return object.getWareHouse();
							else
								return "";
						}
					};
					table.addColumn(warehouseColumn, "Warehouse");
					table.setColumnWidth(warehouseColumn, 100, Unit.PX);
					warehouseColumn.setSortable(true);
				}
	
				
				private void addColumnCustmerRefNumber1() {
					
					customerRefNumber1Column=new TextColumn<AccountingInterface>() {
						
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getReferenceDocumentNo1()!=null){
								return object.getCustomerRefId();
							}
							else{
								return "";
							}
						}
					};
					
					table.addColumn(customerRefNumber1Column,"Cust.Ref.No.1");
					table.setColumnWidth(customerRefNumber1Column, 100, Unit.PX);
					customerRefNumber1Column.setSortable(true);
				}



				private void addColumnCheckBox() {


					/*
					 * Only For Records
					 */
					chkselectRecords = new Column<AccountingInterface, Boolean>(new CheckboxCell()) {
						
						@Override
						public Boolean getValue(AccountingInterface object) {
							return object.isSelectRecord();
						}
					};
					
					chkselectRecords.setFieldUpdater(new FieldUpdater<AccountingInterface, Boolean>() {
						
						@Override
						public void update(int index, AccountingInterface object, Boolean value) {

								object.setSelectRecord(value);
								table.redrawRow(index);
								
								selectAllHeader.getValue();
								table.redrawHeaders();
								
								glassPanel = new GWTCGlassPanel();
								glassPanel.show();
								Timer timer = new Timer() {
									
									@Override
									public void run() {
										// TODO Auto-generated method stub
										glassPanel.hide();
									}
								};
							timer.schedule(1000);
						}
					});
					

					selectAllHeader =new Header<Boolean>(new CheckboxCell()) {
						@Override
						public Boolean getValue() {
							if(getDataprovider().getList().size()!=0){
								
							}
							return false;
						}
					};
					
					selectAllHeader.setUpdater(new ValueUpdater<Boolean>() {
						
						@Override
						public void update(Boolean value) {
							// TODO Auto-generated method stub
							List<AccountingInterface> list = getDataprovider().getList();
							for(AccountingInterface object:list){
								object.setSelectRecord(value);
							}
							
							getDataprovider().setList(list);
							table.redraw();
							addColumnSorting();
						}
					});
					
					table.addColumn(chkselectRecords,selectAllHeader);
					table.setColumnWidth(chkselectRecords, 60, Unit.PX);
							
				}

				@Override
				public void addColumnSorting() {
					
					addSortinggetaccountingInterfaceID();
					addSortinggetaccountingInterfaceCreationDate();
					addSortinggetaccountingInterfaceCreatedBy();
					addSortinggetstatus();
					addSortinggetDocumentStatus();
					addSortinggetremark();
					addSortinggetdateofSync();
					addSortinggetsynchedBy();
					addSortinggetmodule();
					addSortinggetdocumentType();
					addSortingngetdocumentID();
					addSortinggetdocumentTitle();
					addSortinggetdocumentDate();
					addSortinggetdocumentGl();
					addSortinggetreferenceDocumentNumber1();
					addSortinggetreferenceDocumentDate1();
					addSortinggetreferenceDocumentType1();
					addSortinggetreferenceDocumentNumber2();
					addSortinggetreferenceDocumentDate2();
					addSortinggetreferenceDocumentType2();
					addSortinggetaccountType();
					addSortinggetcustomerID();
					addSortinggetcustomerName();
					addSortinggetcustomerCell();
					addSortinggetvendorID();
					addSortinggetvendorName();
					addSortinggetvendorCell();
					addSortinggetemployeeID();
					addSortinggetemployeeName();
					addSortinggetBranch();
					addSortinggetpersonResponsible();
					addSortinggetrequestedBy();
					addSortinggetapprovedBy();
					addSortinggetpaymentMethod();
					addSortinggetpaymentDate();
					addSortinggetchequeNo();
					addSortinggetchequeDate();
					addSortinggetbankName();
					addSortinggetbankAccountNo();
					addSortinggettransferReferenceNumber();
					addSortinggettransactionDate();
					addSortinggetContractStartDate();
					addSortinggetContractEndDate();
					addSortinggetproductID();
					addSortinggetproductCode();
					addSortinggetproductName();
					addSortinggetquantity();
					addSortinggetproductDate();
					addSortinggetduration();
					addSortinggetservices();
					addSortinggetunitOfMeasurement();
					addSortinggetproductPrice();
					addSortinggettotalAmount();
					addSortinggetVATpercent();
					addSortinggetVATamount();
					addSortinggetVATglAccount();
					addSortinggetserviceTaxPercent();
					addSortinggetserviceTaxAmount();
					addSortinggetserviceTaxGLAccount();
					addSortinggetcForm();
					addSortinggetcFormpercent();
					addSortinggetcFormAmount();
					addSortinggetcFormGlAccount();
				
					addSortinggetnetPayable();
					// rohan added this code for contract category sorting 
					addSortingContractCategory();
					addSortinggetamountRecieved();
					
					//   rohan added this code 
					addSortinggetBaseAmount();
					addSortinggettdsPercentage();
					addSortingtdsAmount();
					
					
					addSortinggetotherCharges1();
					addSortinggetotherChargesAmount1();
					addSortinggetotherCharges2();
					addSortinggetotherChargesAmount2();
					addSortinggetotherCharges3();
					addSortinggetotherChargesAmount3();
					addSortinggetotherCharges4();
					addSortinggetotherChargesAmount4();
					addSortinggetotherCharges5();
					addSortinggetotherChargesAmount5();
					addSortinggetotherCharges6();
					addSortinggetotherChargesAmount6();
					addSortinggetotherCharges7();
					addSortinggetotherChargesAmount7();
					addSortinggetotherCharges8();
					addSortinggetotherChargesAmount8();
					addSortinggetotherCharges9();
					addSortinggetotherChargesAmount9();
					addSortinggetotherCharges10();
					addSortinggetotherChargesAmount10();
					addSortinggetotherCharges11();
					addSortinggetotherChargesAmount11();
					addSortinggetotherCharges12();
					addSortinggetotherChargesAmount12();
					addSortinggetaddress();
					addSortinggetlocality();
					addSortinggetlandmark();
					addSortinggetcountry();
					addSortinggetstate();
					addSortinggetcity();
					addSortinggetpincode();
					
					//  rohan added this code sorting billing period start date 
					addSortingBillingPeriodStartDate();
				//  rohan added this code sorting billing period end date 
					addSortingBillingPeriodEndDate();
					//  rohan added this code for NBHC 
					addSortingCustomerRefNO1();
					}
				
				private void addSortingCustomerRefNO1() {
					
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(customerRefNumber1Column, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if( e1.getCustomerRefId()!=null && e2.getCustomerRefId()!=null){
									return e1.getCustomerRefId().compareTo(e2.getCustomerRefId());}
							}
							else{
								return 0;}
							return 0;
						}
							});
					table.addColumnSortHandler(columnSort);		
				}



				private void addSortingContractCategory() {
					
						List<AccountingInterface> list=getDataprovider().getList();
						columnSort=new ListHandler<AccountingInterface>(list);
						columnSort.setComparator(contractCategoryColumn, new Comparator<AccountingInterface>()
								{
							@Override
							public int compare(AccountingInterface e1,AccountingInterface e2)
							{
								if(e1!=null && e2!=null)
								{
									if( e1.getConractCategory()!=null && e2.getConractCategory()!=null){
										return e1.getConractCategory().compareTo(e2.getConractCategory());}
								}
								else{
									return 0;}
								return 0;
							}
								});
						table.addColumnSortHandler(columnSort);								
								
				}



				private void addSortingBillingPeriodStartDate() {
					
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(billingPeriodStartDateColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if( e1.getBillingPeroidFromDate()!=null && e2.getBillingPeroidFromDate()!=null){
									return e1.getBillingPeroidFromDate().compareTo(e2.getBillingPeroidFromDate());}
							}
							else{
								return 0;}
							return 0;
						}
							});
					table.addColumnSortHandler(columnSort);	
				}

				private void addSortingBillingPeriodEndDate() {
					
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(billingPeriodEndDateColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if( e1.getBillingPeroidToDate()!=null && e2.getBillingPeroidToDate()!=null){
									return e1.getBillingPeroidToDate().compareTo(e2.getBillingPeroidToDate());}
							}
							else{
								return 0;}
							return 0;
						}
							});
					table.addColumnSortHandler(columnSort);	
				}




				/***************************************Accounting Interface ID**********************************************/
				
				private void addcolumngetaccountingInterfaceID() {
					accountingInterfaceIDColumn =new TextColumn<AccountingInterface>() {
				
							@Override
							public String getValue(AccountingInterface object) {
								if(object.getCount()!=0)
									return object.getCount()+"";
								else
									return "";
							}
						};
						
						table.addColumn(accountingInterfaceIDColumn,"IF ID");
						table.setColumnWidth(accountingInterfaceIDColumn, 100, Unit.PX);
						accountingInterfaceIDColumn.setSortable(true);
								
				}
				
				private void addSortinggetaccountingInterfaceID() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(accountingInterfaceIDColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if(e1.getCount()== e2.getCount()){
									return 0;}
								if(e1.getCount()> e2.getCount()){
									return 1;}
								else{
									return -1;}
							}
							else{
								return 0;}
						}
							});
					table.addColumnSortHandler(columnSort);
					
				}
				
				
				private void addcolumngetaccountingInterfaceCreationDate() {
					accountingInterfaceCreationDateColumn=new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getAccountingInterfaceCreationDate()!=null)
							{
							    String Date=AppUtility.parseDate(object.getAccountingInterfaceCreationDate());
							    return Date;
							}
							return "";
						}
					};
					
					table.addColumn(accountingInterfaceCreationDateColumn,"Creation Dt");
					table.setColumnWidth(accountingInterfaceCreationDateColumn, 100, Unit.PX);
					accountingInterfaceCreationDateColumn.setSortable(true);		
				}
				private void addSortinggetaccountingInterfaceCreationDate() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(accountingInterfaceCreationDateColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if( e1.getAccountingInterfaceCreationDate()!=null && e2.getAccountingInterfaceCreationDate()!=null){
									return e1.getAccountingInterfaceCreationDate().compareTo(e2.getAccountingInterfaceCreationDate());}
							}
							else{
								return 0;}
							return 0;
						}
							});
					table.addColumnSortHandler(columnSort);	
				}
				
				
				private void addcolumngetaccountingInterfaceCreatedBy() {
					accountingInterfaceCreatedByColumn=new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getAccountingInterfaceCreatedBy()!=null)
							return object.getAccountingInterfaceCreatedBy();
							else
								return "";
						}
					};
					
					table.addColumn(accountingInterfaceCreatedByColumn,"Created By");
					table.setColumnWidth(accountingInterfaceCreatedByColumn, 100, Unit.PX);
					accountingInterfaceCreatedByColumn.setSortable(true);
							
				}
				private void addSortinggetaccountingInterfaceCreatedBy() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(accountingInterfaceCreatedByColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if( e1.getAccountingInterfaceCreatedBy()!=null && e2.getAccountingInterfaceCreatedBy()!=null){
									return e1.getAccountingInterfaceCreatedBy().compareTo(e2.getAccountingInterfaceCreatedBy());}
							}
							else{
								return 0;}
							return 0;
						}
							});
					table.addColumnSortHandler(columnSort);		
				}
				
				
				
				
				
				
				
			
				//  rohan added this code for billing period
				private void addColumnBillingPerioEndDate() {
					
					billingPeriodEndDateColumn=new TextColumn<AccountingInterface>() {
						
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getBillingPeroidToDate()!=null)
							{
							    String Date=AppUtility.parseDate(object.getBillingPeroidToDate());
							    return Date;
							}
							return "";
						}
					};
					
					table.addColumn(billingPeriodEndDateColumn,"Billing Period End Dt");
					table.setColumnWidth(billingPeriodEndDateColumn, 100, Unit.PX);
					billingPeriodEndDateColumn.setSortable(true);
				}

				private void addColumnBillingPeriodStartDate() {
					
					billingPeriodStartDateColumn=new TextColumn<AccountingInterface>() {
						
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getBillingPeroidFromDate()!=null)
							{
							    String Date=AppUtility.parseDate(object.getBillingPeroidFromDate());
							    return Date;
							}
							return "";
						}
					};
					
					table.addColumn(billingPeriodStartDateColumn,"Billing Period St Dt");
					table.setColumnWidth(billingPeriodStartDateColumn, 100, Unit.PX);
					billingPeriodStartDateColumn.setSortable(true);
				}



				
				
				
				
				
				
				
				/******************************************************************************************************/
				private void addColumngetpincode() {
					pincodeColumn=new TextColumn<AccountingInterface>() {
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getPincode()!=0)
								return object.getPincode()+"";
							else
								return "";
						}
					};
					table.addColumn(pincodeColumn,"Pincode");
					table.setColumnWidth(pincodeColumn, 70, Unit.PX);
					pincodeColumn.setSortable(true);
							
				}
				
				private void addSortinggetpincode() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(pincodeColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if(e1.getPincode()== e2.getPincode()){
									return 0;}
								if(e1.getPincode()> e2.getPincode()){
									return 1;}
								else{
									return -1;}
							}
							else{
								return 0;}
						}
							});
					table.addColumnSortHandler(columnSort);		
				}
				
				
				private void addColumngetcity() {
					cityColumn =new TextColumn <AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getCity()!=null)
							return object.getCity();
							else
								return "";
						}
					};
					
					table.addColumn(cityColumn,"City");
					table.setColumnWidth(cityColumn, 100, Unit.PX);
					cityColumn.setSortable(true);	
				}
				private void addSortinggetcity() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(cityColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if( e1.getCity()!=null && e2.getCity()!=null){
									return e1.getCity().compareTo(e2.getCity());}
							}
							else{
								return 0;}
							return 0;
						}
							});
					table.addColumnSortHandler(columnSort);		
				}
				
				
				
				private void addColumngetstate() {
					stateColumn =new TextColumn <AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							
							if(object.getState()!=null)
							return object.getState();
							
							else
								return "";
						}
					};
					
					table.addColumn(stateColumn,"State");
					table.setColumnWidth(stateColumn, 100, Unit.PX);
					stateColumn.setSortable(true);			
				}
				private void addSortinggetstate() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(stateColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if( e1.getState()!=null && e2.getState()!=null){
									return e1.getState().compareTo(e2.getState());}
							}
							else{
								return 0;}
							return 0;
						}
							});
					table.addColumnSortHandler(columnSort);				
				}
				
				
				
				private void addColumngetcountry() {
					countryColumn=new TextColumn <AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getCountry()!=null)
							return object.getCountry();
							else
								return "";
						}
					};
					
					table.addColumn(countryColumn,"Country");
					table.setColumnWidth(countryColumn, 100, Unit.PX);
					countryColumn.setSortable(true);					
				}
				
				private void addSortinggetcountry() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(countryColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if( e1.getCountry()!=null && e2.getCountry()!=null){
									return e1.getCountry().compareTo(e2.getCountry());}
							}
							else{
								return 0;}
							return 0;
						}
							});
					table.addColumnSortHandler(columnSort);				
				}
				
				
				
				
				private void addColumngetlandmark() {
					landmarkColumn=new TextColumn <AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getLandmark()!=null)
							return object.getLandmark();
							else
								return "";
						}
					};
					
					table.addColumn(landmarkColumn,"Landmark");
					table.setColumnWidth(landmarkColumn, 200, Unit.PX);
					landmarkColumn.setSortable(true);					
				}
								
				private void addSortinggetlandmark() {
				
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(landmarkColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if( e1.getLandmark()!=null && e2.getLandmark()!=null){
									return e1.getLandmark().compareTo(e2.getLandmark());}
							}
							else{
								return 0;}
							return 0;
						}
							});
					table.addColumnSortHandler(columnSort);			
				}
				
				
				
				
				private void addColumngetlocality() {
					localityColumn=new TextColumn <AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getLocality()!=null)
							return object.getLocality();
							else
								return "";
						}
					};
					
					table.addColumn(localityColumn,"Locality");
					table.setColumnWidth(localityColumn, 200, Unit.PX);
					localityColumn.setSortable(true);					
					
				}
				private void addSortinggetlocality() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(localityColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if( e1.getLocality()!=null && e2.getLocality()!=null){
									return e1.getLocality().compareTo(e2.getLocality());}
							}
							else{
								return 0;}
							return 0;
						}
							});
					table.addColumnSortHandler(columnSort);			
					
				}
				
				private void addColumngetaddress() {
					addressColumn=new TextColumn <AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getAddress()!=null)
							return object.getAddress();
							else
								return "";
						}
					};
					
					table.addColumn(addressColumn,"Address");
					table.setColumnWidth(addressColumn, 200, Unit.PX);
					addressColumn.setSortable(true);				
				}
				private void addSortinggetaddress() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(addressColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if( e1.getAddress()!=null && e2.getAddress()!=null){
									return e1.getAddress().compareTo(e2.getAddress());}
							}
							else{
								return 0;}
							return 0;
						}
							});
					table.addColumnSortHandler(columnSort);			
							
				}
				
				
				
				private void addColumngetotherChargesAmount12() {
					otherChargesAmount12Column=new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getOtherChargesAmount12()!=0)
							{
								return nf.format(object.getOtherChargesAmount12());
							}
							return null;
						}
					};
					
					table.addColumn(otherChargesAmount12Column,"OC Amt-12");
					table.setColumnWidth(otherChargesAmount12Column, 100, Unit.PX);
					otherChargesAmount12Column.setSortable(true);
				}
				
				private void addSortinggetotherChargesAmount12() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(otherChargesAmount12Column, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if(e1.getOtherChargesAmount12()== e2.getOtherChargesAmount12()){
									return 0;}
								if(e1.getOtherChargesAmount12()> e2.getOtherChargesAmount12()){
									return 1;}
								else{
									return -1;}
							}
							else{
								return 0;}
						}
							});
					table.addColumnSortHandler(columnSort);	
				}
				
				
				
				private void addColumngetotherCharges12() {
					otherCharges12Column=new TextColumn <AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getOtherCharges12()!=null)
							return object.getOtherCharges12();
							else
								return "";
						}
					};
					
					table.addColumn(otherCharges12Column,"O.C.-12");
					table.setColumnWidth(otherCharges12Column, 120, Unit.PX);
					otherCharges12Column.setSortable(true);				
						
				}
				private void addSortinggetotherCharges12() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(otherCharges12Column, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if( e1.getOtherCharges12()!=null && e2.getOtherCharges12()!=null){
									return e1.getOtherCharges12().compareTo(e2.getOtherCharges12());}
							}
							else{
								return 0;}
							return 0;
						}
							});
					table.addColumnSortHandler(columnSort);			
					
					
				}
				
				
				
				
				
				
				private void addColumngetotherChargesAmount11() {
					otherChargesAmount11Column=new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getOtherChargesAmount11()!=0)
							{
								return nf.format(object.getOtherChargesAmount11());
							}
							return null;
						}
					};
					
					table.addColumn(otherChargesAmount11Column,"OC Amt-11");
					table.setColumnWidth(otherChargesAmount11Column, 100, Unit.PX);
					otherChargesAmount11Column.setSortable(true);				
				}
				private void addSortinggetotherChargesAmount11() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(otherChargesAmount11Column, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if(e1.getOtherChargesAmount11()== e2.getOtherChargesAmount11()){
									return 0;}
								if(e1.getOtherChargesAmount11()> e2.getOtherChargesAmount11()){
									return 1;}
								else{
									return -1;}
							}
							else{
								return 0;}
						}
							});
					table.addColumnSortHandler(columnSort);			
				}
				
				private void addColumngetotherCharges11() {
					otherCharges11Column=new TextColumn <AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getOtherCharges11()!=null)
							return object.getOtherCharges11();
							else
								return "";
							
						}
					};
					
					table.addColumn(otherCharges11Column,"O.C.-11");
					table.setColumnWidth(otherCharges11Column, 120, Unit.PX);
					otherCharges11Column.setSortable(true);				
				}
				
				private void addSortinggetotherCharges11() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(otherCharges11Column, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if( e1.getOtherCharges11()!=null && e2.getOtherCharges11()!=null){
									return e1.getOtherCharges11().compareTo(e2.getOtherCharges11());}
							}
							else{
								return 0;}
							return 0;
						}
							});
					table.addColumnSortHandler(columnSort);					
				}
				
				private void addColumngetotherChargesAmount10() {
					otherChargesAmount10Column=new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getOtherChargesAmount10()!=0)
							{
								return nf.format(object.getOtherChargesAmount10());
							}
							return null;
						}
					};
					
					table.addColumn(otherChargesAmount10Column,"OC Amt-10");
					table.setColumnWidth(otherChargesAmount10Column, 100, Unit.PX);
					otherChargesAmount10Column.setSortable(true);					
				}
				private void addSortinggetotherChargesAmount10() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(otherChargesAmount10Column, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if(e1.getOtherChargesAmount10()== e2.getOtherChargesAmount10()){
									return 0;}
								if(e1.getOtherChargesAmount10()> e2.getOtherChargesAmount10()){
									return 1;}
								else{
									return -1;}
							}
							else{
								return 0;}
						}
							});
					table.addColumnSortHandler(columnSort);				
				}
				
				
				
				
				
				
				private void addColumngetotherCharges10() {
					otherCharges10Column=new TextColumn <AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getOtherCharges10()!=null)
							return object.getOtherCharges10();
							else
								return "";
						}
					};
					
					table.addColumn(otherCharges10Column,"O.C.-10");
					table.setColumnWidth(otherCharges10Column, 120, Unit.PX);
					otherCharges10Column.setSortable(true);				
				}
				private void addSortinggetotherCharges10() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(otherCharges10Column, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if( e1.getOtherCharges10()!=null && e2.getOtherCharges10()!=null){
									return e1.getOtherCharges10().compareTo(e2.getOtherCharges10());}
							}
							else{
								return 0;}
							return 0;
						}
							});
					table.addColumnSortHandler(columnSort);				
				}
				
				private void addColumngetotherChargesAmount9() {
					otherChargesAmount9Column=new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getOtherChargesAmount9()!=0)
							{
								return nf.format(object.getOtherChargesAmount9());
							}
							return null;
						}
					};
					
					table.addColumn(otherChargesAmount9Column,"OC Amt- 9");
					table.setColumnWidth(otherChargesAmount9Column, 100, Unit.PX);
					otherChargesAmount9Column.setSortable(true);			
				}
				
				
				private void addSortinggetotherChargesAmount9() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(otherChargesAmount9Column, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if(e1.getOtherChargesAmount9()== e2.getOtherChargesAmount9()){
									return 0;}
								if(e1.getOtherChargesAmount9()> e2.getOtherChargesAmount9()){
									return 1;}
								else{
									return -1;}
							}
							else{
								return 0;}
						}
							});
					table.addColumnSortHandler(columnSort);				
				}
				
				
				private void addColumngetotherCharges9() {
					otherCharges9Column=new TextColumn <AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getOtherCharges9()!=null)
							return object.getOtherCharges9();
							else
								return "";
						}
					};
					
					table.addColumn(otherCharges9Column,"O.C.-9");
					table.setColumnWidth(otherCharges9Column, 120, Unit.PX);
					otherCharges9Column.setSortable(true);				
				}
				private void addSortinggetotherCharges9() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(otherCharges9Column, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if( e1.getOtherCharges9()!=null && e2.getOtherCharges9()!=null){
									return e1.getOtherCharges9().compareTo(e2.getOtherCharges9());}
							}
							else{
								return 0;}
							return 0;
						}
							});
					table.addColumnSortHandler(columnSort);				
				}
				
				
				
				
				
				
				
				
				
				
				private void addColumngetotherChargesAmount8() {
					otherChargesAmount8Column=new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getOtherChargesAmount8()!=0)
							{
								return nf.format(object.getOtherChargesAmount8());
							}
							return null;
						}
					};
					
					table.addColumn(otherChargesAmount8Column,"OC Amt-8");
					table.setColumnWidth(otherChargesAmount8Column, 100, Unit.PX);
					otherChargesAmount8Column.setSortable(true);					
				}
				private void addSortinggetotherChargesAmount8() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(otherChargesAmount8Column, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if(e1.getOtherChargesAmount8()== e2.getOtherChargesAmount8()){
									return 0;}
								if(e1.getOtherChargesAmount8()> e2.getOtherChargesAmount8()){
									return 1;}
								else{
									return -1;}
							}
							else{
								return 0;}
						}
							});
					table.addColumnSortHandler(columnSort);				
				}
				
				
				private void addColumngetotherCharges8() {
					otherCharges8Column=new TextColumn <AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getOtherCharges8()!=null)
							return object.getOtherCharges8();
							else
								return "";
						}
					};
					
					table.addColumn(otherCharges8Column,"O.C.-8");
					table.setColumnWidth(otherCharges8Column, 120, Unit.PX);
					otherCharges8Column.setSortable(true);				
				}
				private void addSortinggetotherCharges8() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(otherCharges8Column, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if( e1.getOtherCharges8()!=null && e2.getOtherCharges8()!=null){
									return e1.getOtherCharges8().compareTo(e2.getOtherCharges8());}
							}
							else{
								return 0;}
							return 0;
						}
							});
					table.addColumnSortHandler(columnSort);			
				}
				
				private void addColumngetotherChargesAmount7() {
					otherChargesAmount7Column=new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getOtherChargesAmount7()!=0)
							{
								return nf.format(object.getOtherChargesAmount7());				}
							return null;
						}
					};
					
					table.addColumn(otherChargesAmount7Column,"OC Amt-7");
					table.setColumnWidth(otherChargesAmount7Column, 100, Unit.PX);
					otherChargesAmount7Column.setSortable(true);						
				}
				private void addSortinggetotherChargesAmount7() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(otherChargesAmount7Column, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if(e1.getOtherChargesAmount7()== e2.getOtherChargesAmount7()){
									return 0;}
								if(e1.getOtherChargesAmount7()> e2.getOtherChargesAmount7()){
									return 1;}
								else{
									return -1;}
							}
							else{
								return 0;}
						}
							});
					table.addColumnSortHandler(columnSort);				
				}
				
				private void addColumngetotherCharges7() {
					otherCharges7Column=new TextColumn <AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							
							if(object.getOtherCharges7()!=null)
							return object.getOtherCharges7();
							else
								return "";
						}
					};
					
					table.addColumn(otherCharges7Column,"O.C.-7");
					table.setColumnWidth(otherCharges7Column, 120, Unit.PX);
					otherCharges7Column.setSortable(true);				
				}
				
				private void addSortinggetotherCharges7() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(otherCharges7Column, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if( e1.getOtherCharges7()!=null && e2.getOtherCharges7()!=null){
									return e1.getOtherCharges7().compareTo(e2.getOtherCharges7());}
							}
							else{
								return 0;}
							return 0;
						}
							});
					table.addColumnSortHandler(columnSort);			
				}
				
				private void addColumngetotherChargesAmount6() {
					otherChargesAmount6Column=new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getOtherChargesAmount6()!=0)
							{
								return nf.format(object.getOtherChargesAmount6());
							}
							return null;
						}
					};
					
					table.addColumn(otherChargesAmount6Column,"OC Amt-6");
					table.setColumnWidth(otherChargesAmount6Column, 100, Unit.PX);
					otherChargesAmount6Column.setSortable(true);				
				}
				private void addSortinggetotherChargesAmount6() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(otherChargesAmount6Column, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if(e1.getOtherChargesAmount6()== e2.getOtherChargesAmount6()){
									return 0;}
								if(e1.getOtherChargesAmount6()> e2.getOtherChargesAmount6()){
									return 1;}
								else{
									return -1;}
							}
							else{
								return 0;}
						}
							});
					table.addColumnSortHandler(columnSort);				
				}
				
				
				
				
				private void addColumngetotherCharges6() {
					otherCharges6Column=new TextColumn <AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getOtherCharges6()!=null)
							return object.getOtherCharges6();
							else
								return "";
						}
					};
					
					table.addColumn(otherCharges6Column,"O.C.-6");
					table.setColumnWidth(otherCharges6Column, 120, Unit.PX);
					otherCharges6Column.setSortable(true);				
				}
				private void addSortinggetotherCharges6() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(otherCharges6Column, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if( e1.getOtherCharges6()!=null && e2.getOtherCharges6()!=null){
									return e1.getOtherCharges6().compareTo(e2.getOtherCharges6());}
							}
							else{
								return 0;}
							return 0;
						}
							});
					table.addColumnSortHandler(columnSort);			
				}
				
				
				
				private void addColumngetotherChargesAmount5() {
					otherChargesAmount5Column=new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getOtherChargesAmount5()!=0)
							{
								return nf.format(object.getOtherChargesAmount5());
							}
							return null;
						}
					};
					
					table.addColumn(otherChargesAmount5Column,"OC Amt-5");
					table.setColumnWidth(otherChargesAmount5Column, 100, Unit.PX);
					otherChargesAmount5Column.setSortable(true);				
				}
				private void addSortinggetotherChargesAmount5() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(otherChargesAmount5Column, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if(e1.getOtherChargesAmount5()== e2.getOtherChargesAmount5()){
									return 0;}
								if(e1.getOtherChargesAmount5()> e2.getOtherChargesAmount5()){
									return 1;}
								else{
									return -1;}
							}
							else{
								return 0;}
						}
							});
					table.addColumnSortHandler(columnSort);				
				}
				
				
				
				private void addColumngetotherCharges5() {
					otherCharges5Column=new TextColumn <AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getOtherCharges5()!=null)
							return object.getOtherCharges5();
							else
								return "";
						}
					};
					
					table.addColumn(otherCharges5Column,"O.C.-5");
					table.setColumnWidth(otherCharges5Column, 120, Unit.PX);
					otherCharges5Column.setSortable(true);				
				}
				
				private void addSortinggetotherCharges5() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(otherCharges5Column, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if( e1.getOtherCharges5()!=null && e2.getOtherCharges5()!=null){
									return e1.getOtherCharges5().compareTo(e2.getOtherCharges5());}
							}
							else{
								return 0;}
							return 0;
						}
							});
					table.addColumnSortHandler(columnSort);				
				}
				
				
				
				private void addColumngetotherChargesAmount4() {
					otherChargesAmount4Column=new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getOtherChargesAmount4()!=0)
							{
								return nf.format(object.getOtherChargesAmount4());
							}
							return null;
						}
					};
					
					table.addColumn(otherChargesAmount4Column,"OC Amt-4");
					table.setColumnWidth(otherChargesAmount4Column, 100, Unit.PX);
					otherChargesAmount4Column.setSortable(true);			
				}
				
				private void addSortinggetotherChargesAmount4() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(otherChargesAmount4Column, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if(e1.getOtherChargesAmount4()== e2.getOtherChargesAmount4()){
									return 0;}
								if(e1.getOtherChargesAmount4()> e2.getOtherChargesAmount4()){
									return 1;}
								else{
									return -1;}
							}
							else{
								return 0;}
						}
							});
					table.addColumnSortHandler(columnSort);				
				}
				
				
				
				
				private void addColumngetotherCharges4() {
					otherCharges4Column=new TextColumn <AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getOtherCharges4()!=null)
							return object.getOtherCharges4();
							else
								return "";
						}
					};
					
					table.addColumn(otherCharges4Column,"O.C.-4");
					table.setColumnWidth(otherCharges4Column, 120, Unit.PX);
					otherCharges4Column.setSortable(true);		
				}
				private void addSortinggetotherCharges4() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(otherCharges4Column, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if( e1.getOtherCharges4()!=null && e2.getOtherCharges4()!=null){
									return e1.getOtherCharges4().compareTo(e2.getOtherCharges4());}
							}
							else{
								return 0;}
							return 0;
						}
							});
					table.addColumnSortHandler(columnSort);				
				}
				
				private void addColumngetotherChargesAmount3() {
					otherChargesAmount3Column=new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getOtherChargesAmount3()!=0)
							{
								return nf.format(object.getOtherChargesAmount3());
							}
							return null;
						}
					};
					
					table.addColumn(otherChargesAmount3Column,"OC Amt-3");
					table.setColumnWidth(otherChargesAmount3Column, 100, Unit.PX);
					otherChargesAmount3Column.setSortable(true);			
				}
				private void addSortinggetotherChargesAmount3() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(otherChargesAmount3Column, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if(e1.getOtherChargesAmount3()== e2.getOtherChargesAmount3()){
									return 0;}
								if(e1.getOtherChargesAmount3()> e2.getOtherChargesAmount3()){
									return 1;}
								else{
									return -1;}
							}
							else{
								return 0;}
						}
							});
					table.addColumnSortHandler(columnSort);				
				}
				
				
				
				private void addColumngetotherCharges3() {
					otherCharges3Column=new TextColumn <AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getOtherCharges3()!=null)
							return object.getOtherCharges3();
							else
								return "";
						}
					};
					
					table.addColumn(otherCharges3Column,"O.C.-3");
					table.setColumnWidth(otherCharges3Column, 120, Unit.PX);
					otherCharges3Column.setSortable(true);		
				}
				private void addSortinggetotherCharges3() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(otherCharges3Column, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if( e1.getOtherCharges3()!=null && e2.getOtherCharges3()!=null){
									return e1.getOtherCharges3().compareTo(e2.getOtherCharges3());}
							}
							else{
								return 0;}
							return 0;
						}
							});
					table.addColumnSortHandler(columnSort);					
				}
				
				
				
				private void addColumngetotherChargesAmount2() {
					otherChargesAmount2Column=new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getOtherChargesAmount2()!=0)
							{
								return nf.format(object.getOtherChargesAmount2());
							}
							return null;
						}
					};
					
					table.addColumn(otherChargesAmount2Column,"OC Amt-2");
					table.setColumnWidth(otherChargesAmount2Column, 100, Unit.PX);
					otherChargesAmount2Column.setSortable(true);				
				}
				
				private void addSortinggetotherChargesAmount2() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(otherChargesAmount2Column, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if(e1.getOtherChargesAmount2()== e2.getOtherChargesAmount2()){
									return 0;}
								if(e1.getOtherChargesAmount2()> e2.getOtherChargesAmount2()){
									return 1;}
								else{
									return -1;}
							}
							else{
								return 0;}
						}
							});
					table.addColumnSortHandler(columnSort);				
				}
				
				
				
				
				
				
				
				
				
				
				private void addColumngetotherCharges2() {
					otherCharges2Column=new TextColumn <AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							
							if(object.getOtherCharges2()!=null)
							return object.getOtherCharges2();
							else
								return "";
						}
					};
					
					table.addColumn(otherCharges2Column,"O.C.-2");
					table.setColumnWidth(otherCharges2Column, 120, Unit.PX);
					otherCharges2Column.setSortable(true);				
				}
				private void addSortinggetotherCharges2() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(otherCharges2Column, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if( e1.getOtherCharges2()!=null && e2.getOtherCharges2()!=null){
									return e1.getOtherCharges2().compareTo(e2.getOtherCharges2());}
							}
							else{
								return 0;}
							return 0;
						}
							});
					table.addColumnSortHandler(columnSort);					
				}
				
				
				
				private void addColumngetotherChargesAmount1() {
					otherChargesAmount1Column=new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getOtherChargesAmount1()!=0)
							{
								return nf.format(object.getOtherChargesAmount1());
							}
							return null;
						}
					};
					
					table.addColumn(otherChargesAmount1Column,"OC Amt-1");
					table.setColumnWidth(otherChargesAmount1Column, 100, Unit.PX);
					otherChargesAmount1Column.setSortable(true);		
				}
				private void addSortinggetotherChargesAmount1() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(otherChargesAmount1Column, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if(e1.getOtherChargesAmount1()== e2.getOtherChargesAmount1()){
									return 0;}
								if(e1.getOtherChargesAmount1()> e2.getOtherChargesAmount1()){
									return 1;}
								else{
									return -1;}
							}
							else{
								return 0;}
						}
							});
					table.addColumnSortHandler(columnSort);						
				}
				
				
				
				private void addColumngetotherCharges1() {
					otherCharges1Column=new TextColumn <AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							return object.getOtherCharges1();
						}
					};
					
					table.addColumn(otherCharges1Column,"O.C.-1");
					table.setColumnWidth(otherCharges1Column, 120, Unit.PX);
					otherCharges1Column.setSortable(true);				
				}
				
				private void addSortinggetotherCharges1() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(otherCharges1Column, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if( e1.getOtherCharges1()!=null && e2.getOtherCharges1()!=null){
									return e1.getOtherCharges1().compareTo(e2.getOtherCharges1());}
							}
							else{
								return 0;}
							return 0;
						}
							});
					table.addColumnSortHandler(columnSort);				
				}
				
				private void addColumngetnetPayable() {
					netPayableColumn=new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getNetPayable()!=0)
							{
								return nf.format(object.getNetPayable());
							}
							return null;
						}
					};
					
					table.addColumn(netPayableColumn,"Net Pay.");
					table.setColumnWidth(netPayableColumn, 80, Unit.PX);
					netPayableColumn.setSortable(true);			
				}
				
				private void addColumngetContractCategory() {
					contractCategoryColumn=new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getConractCategory()!=null)
							{
								return object.getConractCategory();
							}
							return null;
						}
					};
					
					table.addColumn(contractCategoryColumn,"Contract Category");
					table.setColumnWidth(contractCategoryColumn, 80, Unit.PX);
					contractCategoryColumn.setSortable(false);			
				}
				
				private void addSortinggetnetPayable() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(netPayableColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if(e1.getNetPayable()== e2.getNetPayable()){
									return 0;}
								if(e1.getNetPayable()> e2.getNetPayable()){
									return 1;}
								else{
									return -1;}
							}
							else{
								return 0;}
						}
							});
					table.addColumnSortHandler(columnSort);					
				}
				
				
				
				private void addColumngetamountRecieved() {
					amountRecievedColumn=new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getAmountRecieved()!=0)
							{
								return nf.format(object.getAmountRecieved());
							}
							return null;
						}
					};
					
					table.addColumn(amountRecievedColumn,"Amt.Recieved");
					table.setColumnWidth(amountRecievedColumn, 130, Unit.PX);
					amountRecievedColumn.setSortable(true);		
				}
				
				
				
				
				
				private void addSortinggetamountRecieved() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(amountRecievedColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if(e1.getAmountRecieved()== e2.getAmountRecieved()){
									return 0;}
								if(e1.getAmountRecieved()> e2.getAmountRecieved()){
									return 1;}
								else{
									return -1;}
							}
							else{
								return 0;}
						}
							});
					table.addColumnSortHandler(columnSort);				
				}
				
				
				//   rohan added this for base Amount 
				
				private void addColumngetBaseAmount() {
					baseAmount=new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getBaseAmount()!=0)
							{
								return nf.format(object.getBaseAmount());
							}
							return null;
						}
					};
					
					table.addColumn(baseAmount,"Base Amount");
					table.setColumnWidth(baseAmount, 130, Unit.PX);
					baseAmount.setSortable(true);		
				}
				
				private void addSortinggetBaseAmount() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(baseAmount, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if(e1.getBaseAmount()== e2.getBaseAmount()){
									return 0;}
								if(e1.getBaseAmount()> e2.getBaseAmount()){
									return 1;}
								else{
									return -1;}
							}
							else{
								return 0;}
						}
							});
					table.addColumnSortHandler(columnSort);				
				}
				
				
				
				//   rohan added this
				private void addColumngettdsPercentage() {
					tdsPercentage=new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getTdsPercentage()!=0)
							{
								return nf.format(object.getTdsPercentage());
							}
							return null;
						}
					};
					
					table.addColumn(tdsPercentage,"TDS %");
					table.setColumnWidth(tdsPercentage, 130, Unit.PX);
					tdsPercentage.setSortable(true);		
				}
				
				
				
				
				
				private void addSortinggettdsPercentage() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(tdsPercentage, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if(e1.getTdsPercentage()== e2.getTdsPercentage()){
									return 0;}
								if(e1.getTdsPercentage()> e2.getTdsPercentage()){
									return 1;}
								else{
									return -1;}
							}
							else{
								return 0;}
						}
							});
					table.addColumnSortHandler(columnSort);				
				}
				
				private void addColumngettdsAmount() {
					tdsAmount=new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getTdsAmount()!=0)
							{
								return nf.format(object.getTdsAmount());
							}
							return null;
						}
					};
					
					table.addColumn(tdsAmount,"TDS Amt");
					table.setColumnWidth(tdsAmount, 130, Unit.PX);
					tdsAmount.setSortable(true);		
				}
				
				
				
				
				
				private void addSortingtdsAmount() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(tdsAmount, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if(e1.getTdsAmount()== e2.getTdsAmount()){
									return 0;}
								if(e1.getTdsAmount()> e2.getTdsAmount()){
									return 1;}
								else{
									return -1;}
							}
							else{
								return 0;}
						}
							});
					table.addColumnSortHandler(columnSort);				
				}
				
				///      changes ends here 
				
				
				private void addColumngettotalAmount() {
					totalAmountColumn=new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getTotalAmount()!=0)
							{
							return nf.format(object.getTotalAmount());
							}
							return null;
						}
					};
					
					table.addColumn(totalAmountColumn,"Total Amt.");
					table.setColumnWidth(totalAmountColumn, 100, Unit.PX);
					totalAmountColumn.setSortable(true);					
				}
				private void addSortinggettotalAmount() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(totalAmountColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if(e1.getTotalAmount()== e2.getTotalAmount()){
									return 0;}
								if(e1.getTotalAmount()> e2.getTotalAmount()){
									return 1;}
								else{
									return -1;}
							}
							else{
								return 0;}
						}
							});
					table.addColumnSortHandler(columnSort);						
				}
				
				
				private void addColumngetcFormGlAccount() {
					cFormGlAccountColumn=new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getcFormGlaccount()!=0)
							return object.getcFormGlaccount()+"";
							else
								return "";
						}
					};
					
					table.addColumn(cFormGlAccountColumn,"cForm Gl.Acc.");
					table.setColumnWidth(cFormGlAccountColumn, 150, Unit.PX);
					cFormGlAccountColumn.setSortable(true);
				}
				
				
				
				private void addSortinggetcFormGlAccount() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(cFormGlAccountColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if(e1.getcFormGlaccount()== e2.getcFormGlaccount()){
									return 0;}
								if(e1.getcFormGlaccount()> e2.getcFormGlaccount()){
									return 1;}
								else{
									return -1;}
							}
							else{
								return 0;}
						}
							});
					table.addColumnSortHandler(columnSort);			
				}
				
				
				private void addColumngetcFormAmount() {
					cFormAmountColumn=new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getcFormAmount()!=0)
							{
								return nf.format(object.getcFormAmount());
							}
							return null;
						}
					};
					
					table.addColumn(cFormAmountColumn,"cForm Amt.");
					table.setColumnWidth(cFormAmountColumn, 100, Unit.PX);
					cFormAmountColumn.setSortable(true);				
				}
				private void addSortinggetcFormAmount() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(cFormAmountColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if(e1.getcFormAmount()== e2.getcFormAmount()){
									return 0;}
								if(e1.getcFormAmount()> e2.getcFormAmount()){
									return 1;}
								else{
									return -1;}
							}
							else{
								return 0;}
						}
							});
					table.addColumnSortHandler(columnSort);			
				}
				
				
				private void addColumngetcFormpercent() {
					cFormPercentColumn=new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getcFormPercent()!=0)
							{
							return object.getcFormPercent()+"";
							}
							return null;
						}
					};
					
					table.addColumn(cFormPercentColumn,"cForm %");
					table.setColumnWidth(cFormPercentColumn, 80, Unit.PX);
					cFormPercentColumn.setSortable(true);			
				}
				private void addSortinggetcFormpercent() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(cFormPercentColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if(e1.getcFormPercent()== e2.getcFormPercent()){
									return 0;}
								if(e1.getcFormPercent()> e2.getcFormPercent()){
									return 1;}
								else{
									return -1;}
							}
							else{
								return 0;}
						}
							});
					table.addColumnSortHandler(columnSort);					
				}
				
				
				private void addColumngetserviceTaxGLAccount() {
					serviceTaxGLaccountColumn=new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getServiceTaxGlAccount()!=0)
							return object.getServiceTaxGlAccount()+"";
							else
								return "";
						}
					};
					
					table.addColumn(serviceTaxGLaccountColumn,"Tax 2 GL.Acc.");
					table.setColumnWidth(serviceTaxGLaccountColumn, 100, Unit.PX);
					serviceTaxGLaccountColumn.setSortable(true);
							
				}
				
				
				
				private void addSortinggetserviceTaxGLAccount(){
					
					
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(serviceTaxGLaccountColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if(e1.getServiceTaxGlAccount()== e2.getServiceTaxGlAccount()){
									return 0;}
								if(e1.getServiceTaxGlAccount()> e2.getServiceTaxGlAccount()){
									return 1;}
								else{
									return -1;}
							}
							else{
								return 0;}
						}
							});
					table.addColumnSortHandler(columnSort);		
				}	
				
				
					
				
				private void addColumngetserviceTaxAmount() {
					serviceTaxAmountColumn=new TextColumn<AccountingInterface>() {
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getServiceTaxAmount()!=0)
							{
							return nf.format(object.getServiceTaxAmount());
							}
							return null;
						}
					};
					
					table.addColumn(serviceTaxAmountColumn,"Tax 2 Amt.");
					table.setColumnWidth(serviceTaxAmountColumn, 80, Unit.PX);
					serviceTaxAmountColumn.setSortable(true);			
				}
					
				
				private void addSortinggetserviceTaxAmount() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(serviceTaxAmountColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if(e1.getServiceTaxAmount()== e2.getServiceTaxAmount()){
									return 0;}
								if(e1.getServiceTaxAmount()> e2.getServiceTaxAmount()){
									return 1;}
								else{
									return -1;}
							}
							else{
								return 0;}
						}
							});
					table.addColumnSortHandler(columnSort);					
				}
				
				
				private void addColumngetserviceTaxPercent() {
					serviceTaxPercentColumn=new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getServiceTaxPercent()!=0)
							{
							return object.getServiceTaxPercent()+"";
							}
							return null;
						}
					};
					
					table.addColumn(serviceTaxPercentColumn,"Tax 2 %");
					table.setColumnWidth(serviceTaxPercentColumn, 60, Unit.PX);
					serviceTaxPercentColumn.setSortable(true);		
				}
				
				private void addSortinggetserviceTaxPercent() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(serviceTaxPercentColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if(e1.getServiceTaxPercent()== e2.getServiceTaxPercent()){
									return 0;}
								if(e1.getServiceTaxPercent()> e2.getServiceTaxPercent()){
									return 1;}
								else{
									return -1;}
							}
							else{
								return 0;}
						}
							});
					table.addColumnSortHandler(columnSort);					
				}
				
				
				
				
				private void addColumngetVATglAccount() {
					VATglAccountColumn=new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getVatGlaccount()!=0)
							return object.getVatGlaccount()+"";
							else
								return "";
						}
					};
					
					table.addColumn(VATglAccountColumn,"Tax 1 .GL.Acc.");
					table.setColumnWidth(VATglAccountColumn, 100, Unit.PX);
					VATglAccountColumn.setSortable(true);
										
				}
							
				
				
				private void addSortinggetVATglAccount() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(VATglAccountColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if(e1.getVatGlaccount()== e2.getVatGlaccount()){
									return 0;}
								if(e1.getVatGlaccount()> e2.getVatGlaccount()){
									return 1;}
								else{
									return -1;}
							}
							else{
								return 0;}
						}
							});
					table.addColumnSortHandler(columnSort);			
				}
				
				
				
				
				
				
				
				
				private void addColumngetVATamount() {
					VATamountColumn=new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getVatAmount()!=0)
							{
							return nf.format(object.getVatAmount());
							}
							return null;
						}
					};
					
					table.addColumn(VATamountColumn,"Tax amt.");
					table.setColumnWidth(VATamountColumn, 80, Unit.PX);
					VATamountColumn.setSortable(true);				
				}
				private void addSortinggetVATamount() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(VATamountColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if(e1.getVatAmount()== e2.getVatAmount()){
									return 0;}
								if(e1.getVatAmount()> e2.getVatAmount()){
									return 1;}
								else{
									return -1;}
							}
							else{
								return 0;}
						}
							});
					table.addColumnSortHandler(columnSort);					
				}
				
				
				private void addColumngetVATpercent() {
					VATPercentColumn=new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getVatPercent()!=0)
							{
							return object.getVatPercent()+"";
							}
							return null;
						}
					};
					
					table.addColumn(VATPercentColumn,"Tax 1 %");
					table.setColumnWidth(VATPercentColumn, 60, Unit.PX);
					VATPercentColumn.setSortable(true);				
				}
				private void addSortinggetVATpercent() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(VATPercentColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if(e1.getVatPercent()== e2.getVatPercent()){
									return 0;}
								if(e1.getVatPercent()> e2.getVatPercent()){
									return 1;}
								else{
									return -1;}
							}
							else{
								return 0;}
						}
							});
					table.addColumnSortHandler(columnSort);					
				}
				
				
				private void addColumngetquantity() {
					quantityColumn=new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getQuantity()!=0)
							{
							return object.getQuantity()+"";
							}
							return null;
						}
					};
					
					table.addColumn(quantityColumn,"Quantity");
					table.setColumnWidth(quantityColumn, 80, Unit.PX);
					quantityColumn.setSortable(true);		
				}
				
				
				private void addSortinggetquantity() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(quantityColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if(e1.getQuantity()== e2.getQuantity()){
									return 0;}
								if(e1.getQuantity()> e2.getQuantity()){
									return 1;}
								else{
									return -1;}
							}
							else{
								return 0;}
						}
							});
					table.addColumnSortHandler(columnSort);		
				}
				
				
				private void addColumngetproductDate() {
					productDateColumn=new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getProductDate()!=null)
							{
							    String Date=AppUtility.parseDate(object.getProductDate());
							    return Date;
							}
							return "";
						}
					};
					
					table.addColumn(productDateColumn,"Product Date");
					table.setColumnWidth(productDateColumn, 100, Unit.PX);
					productDateColumn.setSortable(true);		
				}
				
				private void addSortinggetproductDate(){
					
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(productDateColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if( e1.getProductDate()!=null && e2.getProductDate()!=null){
									return e1.getProductDate().compareTo(e2.getProductDate());}
							}
							else{
								return 0;}
							return 0;
						}
							});
					table.addColumnSortHandler(columnSort);			
					
				}
				
				
				
				
				private void addSortinggetproductPrice() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(productPriceColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if(e1.getProductPrice()== e2.getProductPrice()){
									return 0;}
								if(e1.getProductPrice()> e2.getProductPrice()){
									return 1;}
								else{
									return -1;}
							}
							else{
								return 0;}
						}
							});
					table.addColumnSortHandler(columnSort);				
				}
				
				
				private void addColumngetproductPrice() {
				productPriceColumn=new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getProductPrice()!=0)
							{
								return nf.format(object.getProductPrice());
							}
							return null;
						}
					};
					
					table.addColumn(productPriceColumn,"Prod.Price");
					table.setColumnWidth(productPriceColumn, 100, Unit.PX);
					productPriceColumn.setSortable(true);		
				}
				
				
				
				
				
				
				
				
				
				private void addSortinggetunitOfMeasurement() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(unitOfMeasurementColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if( e1.getUnitOfMeasurement()!=null && e2.getUnitOfMeasurement()!=null){
									return e1.getUnitOfMeasurement().compareTo(e2.getUnitOfMeasurement());}
							}
							else{
								return 0;}
							return 0;
						}
							});
					table.addColumnSortHandler(columnSort);		
				}
				
				
				
				
				private void addColumngetunitOfMeasurement() {
					unitOfMeasurementColumn =new TextColumn <AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getUnitOfMeasurement()!=null)
							return object.getUnitOfMeasurement();
							else
								return "";
						}
					};
					
					table.addColumn(unitOfMeasurementColumn,"UOM");
					table.setColumnWidth(unitOfMeasurementColumn, 50, Unit.PX);
					unitOfMeasurementColumn.setSortable(true);		
				}
				
				
				
				
				
				
				
				
				
				private void addColumngetproductName(){
					
					productNameColumn =new TextColumn <AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getProductName()!=null)
							return object.getProductName();
							else
								return "";
						}
					};
					
					table.addColumn(productNameColumn,"Prod. Name");
					table.setColumnWidth(productNameColumn, 100, Unit.PX);
					productNameColumn.setSortable(true);
				}
				
				
				
				private void addSortinggetproductName(){
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(productNameColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if( e1.getProductName()!=null && e2.getProductName()!=null){
									return e1.getProductName().compareTo(e2.getProductName());}
							}
							else{
								return 0;}
							return 0;
						}
							});
					table.addColumnSortHandler(columnSort);
					
					
					
				}
				
				
				
				
				
				private void addColumngetproductCode() {
					productCodeColumn =new TextColumn <AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getProductCode()!=null)
							return object.getProductCode();
							else
								return "";
						}
					};
					
					table.addColumn(productCodeColumn,"Prod. Code");
					table.setColumnWidth(productCodeColumn, 100, Unit.PX);
					productCodeColumn.setSortable(true);		
				}
				private void addSortinggetproductCode() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(productCodeColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if( e1.getProductCode()!=null && e2.getProductCode()!=null){
									return e1.getProductCode().compareTo(e2.getProductCode());}
							}
							else{
								return 0;}
							return 0;
						}
							});
					table.addColumnSortHandler(columnSort);
							
				}
				
				
				
				private void addSortinggetproductID(){
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(productIDColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if(e1.getProductID()== e2.getProductID()){
									return 0;}
								if(e1.getProductID()> e2.getProductID()){
									return 1;}
								else{
									return -1;}
							}
							else{
								return 0;}
						}
							});
					table.addColumnSortHandler(columnSort);		
					
					
					
				}
				
				private void addColumngetproductID(){
					
					productIDColumn=new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getProductID()!=0)
							return object.getProductID()+"";
							else
								return "";
						}
					};
					
					table.addColumn(productIDColumn,"Product ID");
					table.setColumnWidth(productIDColumn, 100, Unit.PX);
					productIDColumn.setSortable(true);
							
				}
				
				
				
				
				
				
				
				
				
				
				
				
				private void addColumngettransactionDate() {
					transactionDateColumn=new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getTransactionDate()!=null)
							{
							    String Date=AppUtility.parseDate(object.getTransactionDate());
							    return Date;
							}
							return "";
						}
					};
					
					table.addColumn(transactionDateColumn,"Trans Date");
					table.setColumnWidth(transactionDateColumn, 100, Unit.PX);
					transactionDateColumn.setSortable(true);		
				}
				
				
				private void addSortinggettransactionDate() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(transactionDateColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if( e1.getTransactionDate()!=null && e2.getTransactionDate()!=null){
									return e1.getTransactionDate().compareTo(e2.getTransactionDate());}
							}
							else{
								return 0;}
							return 0;
						}
							});
					table.addColumnSortHandler(columnSort);					
				}
				
				private void addColumngetContractStartDate() {
					contractStartDateColumn=new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getContractStartDate()!=null)
							{
							    String Date=AppUtility.parseDate(object.getContractStartDate());
							    return Date;
							}
							else{
								return "";
							}
						}
					};
					
					table.addColumn(contractStartDateColumn,"Start Date");
					table.setColumnWidth(contractStartDateColumn, 100, Unit.PX);
					contractStartDateColumn.setSortable(true);		
				}
				
				
				private void addSortinggetContractStartDate() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(contractStartDateColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if( e1.getContractStartDate()!=null && e2.getContractStartDate()!=null){
									return e1.getContractStartDate().compareTo(e2.getContractStartDate());}
							}
							else{
								return 0;}
							return 0;
						}
							});
					table.addColumnSortHandler(columnSort);					
				}
				
				private void addColumngetContractEndDate() {
					contractEndDateColumn=new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getContractEndDate()!=null)
							{
							    String Date=AppUtility.parseDate(object.getContractEndDate());
							    return Date;
							}
							else{
								return "";
							}
						}
					};
					
					table.addColumn(contractEndDateColumn,"End Date");
					table.setColumnWidth(contractEndDateColumn, 100, Unit.PX);
					contractEndDateColumn.setSortable(true);		
				}
				
				
				private void addSortinggetContractEndDate() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(contractEndDateColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if( e1.getContractEndDate()!=null && e2.getContractEndDate()!=null){
									return e1.getContractEndDate().compareTo(e2.getContractEndDate());}
							}
							else{
								return 0;}
							return 0;
						}
							});
					table.addColumnSortHandler(columnSort);					
				}
				
				private void addColumngettransferReferenceNumber() {
					
					transferReferenceNumberColumn=new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getTransferReferenceNumber()!=0)
							return object.getTransferReferenceNumber()+"";
							else
								return "";
						}
					};
					
					table.addColumn(transferReferenceNumberColumn,"Traf.Ref.No.");
					table.setColumnWidth(transferReferenceNumberColumn, 150, Unit.PX);
					transferReferenceNumberColumn.setSortable(true);
									
				}
				private void addSortinggettransferReferenceNumber() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(transferReferenceNumberColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if(e1.getTransferReferenceNumber()== e2.getTransferReferenceNumber()){
									return 0;}
								if(e1.getTransferReferenceNumber()> e2.getTransferReferenceNumber()){
									return 1;}
								else{
									return -1;}
							}
							else{
								return 0;}
						}
							});
					table.addColumnSortHandler(columnSort);		
							
				}
				
				
				
				
				private void addColumngetduration() {
					durationColumn=new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getProdDuration()!=0)
							return object.getProdDuration()+"";
							else
								return "";
						}
					};
					
					table.addColumn(durationColumn,"Duration");
					table.setColumnWidth(durationColumn, 90, Unit.PX);
					durationColumn.setSortable(true);
				}
				
				private void addSortinggetduration(){
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(durationColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if(e1.getProdDuration()== e2.getProdDuration()){
									return 0;}
								if(e1.getProdDuration()> e2.getProdDuration()){
									return 1;}
								else{
									return -1;}
							}
							else{
								return 0;}
						}
							});
					table.addColumnSortHandler(columnSort);		
							
					
				}
				
				
				private void addColumngetservices() {
					servicesColumn=new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getProdServices()!=0)
							return object.getProdServices()+"";
							else
								return "";
						}
					};
					
					table.addColumn(servicesColumn,"Services");
					table.setColumnWidth(servicesColumn, 90, Unit.PX);
					servicesColumn.setSortable(true);
				}
				
				private void addSortinggetservices(){
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(servicesColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if(e1.getProdServices()== e2.getProdServices()){
									return 0;}
								if(e1.getProdServices()> e2.getProdServices()){
									return 1;}
								else{
									return -1;}
							}
							else{
								return 0;}
						}
							});
					table.addColumnSortHandler(columnSort);		
					
				}
				
				
				
				
				
				
				
				
				private void addSortinggetpersonResponsible(){
					
					 List<AccountingInterface> list=getDataprovider().getList();
						columnSort=new ListHandler<AccountingInterface>(list);
						columnSort.setComparator(personResponsibleColumn, new Comparator<AccountingInterface>()
								{
							@Override
							public int compare(AccountingInterface e1,AccountingInterface e2)
							{
								if(e1!=null && e2!=null)
								{
									if( e1.getPersonResponsible()!=null && e2.getPersonResponsible()!=null){
										return e1.getPersonResponsible().compareTo(e2.getPersonResponsible());}
								}
								else{
									return 0;}
								return 0;
							}
								});
						table.addColumnSortHandler(columnSort);
				
				}
				private void addColumngetpersonResponsible(){
					
					personResponsibleColumn=new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getPersonResponsible()!=null)
							return object.getPersonResponsible();
							else
								return "";
						}
					};
					
					table.addColumn(personResponsibleColumn,"Person Responsible");
					table.setColumnWidth(personResponsibleColumn,200, Unit.PX);
					personResponsibleColumn.setSortable(true);
					
				}
				
				
				private void addSortinggetBranch(){
					 List<AccountingInterface> list=getDataprovider().getList();
						columnSort=new ListHandler<AccountingInterface>(list);
						columnSort.setComparator(BranchColumn, new Comparator<AccountingInterface>()
								{
							@Override
							public int compare(AccountingInterface e1,AccountingInterface e2)
							{
								if(e1!=null && e2!=null)
								{
									if( e1.getBranch()!=null && e2.getBranch()!=null){
										return e1.getBranch().compareTo(e2.getBranch());}
								}
								else{
									return 0;}
								return 0;
							}
								});
						table.addColumnSortHandler(columnSort);
						
					
				}
				
				
				private void addColumngetBranch(){
					BranchColumn=new TextColumn<AccountingInterface>() {
				
					@Override
					public String getValue(AccountingInterface object) {
						if(object.getBranch()!=null)
						return object.getBranch();
						else
							return "";
					}
				};
				
				table.addColumn(BranchColumn,"Branch");
				table.setColumnWidth(BranchColumn,65 , Unit.PX);
				BranchColumn.setSortable(true);
				}
				
				private void addColumnDocumentStatus(){
					documentStatusColumn=new TextColumn<AccountingInterface>() {
				
					@Override
					public String getValue(AccountingInterface object) {
						if(object.getDocumentStatus()!=null)
						return object.getDocumentStatus();
						else
							return "";
					}
				};
				
				table.addColumn(documentStatusColumn,"Doc. Status");
				table.setColumnWidth(documentStatusColumn,90 , Unit.PX);
				documentStatusColumn.setSortable(true);
				}
				
				private void addSortinggetDocumentStatus(){
					
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(documentStatusColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if( e1.getDocumentStatus()!=null && e2.getDocumentStatus()!=null){
									return e1.getDocumentStatus().compareTo(e2.getDocumentStatus());}
							}
							else{
								return 0;}
							return 0;
						}
							});
					table.addColumnSortHandler(columnSort);
				}
				
				
				
				
				private void addSortinggetbankAccountNo(){
					
					 List<AccountingInterface> list=getDataprovider().getList();
						columnSort=new ListHandler<AccountingInterface>(list);
						columnSort.setComparator(bankAccountnoColumn, new Comparator<AccountingInterface>()
								{
							@Override
							public int compare(AccountingInterface e1,AccountingInterface e2)
							{
								if(e1!=null && e2!=null)
								{
									if( e1.getBankAccount()!=null && e2.getBankAccount()!=null){
										return e1.getBankAccount().compareTo(e2.getBankAccount());}
								}
								else{
									return 0;}
								return 0;
							}
								});
						table.addColumnSortHandler(columnSort);
				}
					
					
					private void addColumngetbankAccountNo(){
						bankAccountnoColumn=new TextColumn<AccountingInterface>() {
				
							@Override
							public String getValue(AccountingInterface object) {
								if(object.getBankAccount()!=null)
								return object.getBankAccount();
								else
									return "";
							}
						};
						
						table.addColumn(bankAccountnoColumn,"Bank Acc.");
						table.setColumnWidth(bankAccountnoColumn,110, Unit.PX);
						bankAccountnoColumn.setSortable(true);
					}
					
					
					
					
					private void addSortinggetbankName(){
						
						List<AccountingInterface> list=getDataprovider().getList();
						columnSort=new ListHandler<AccountingInterface>(list);
						columnSort.setComparator(bankNameColumn, new Comparator<AccountingInterface>()
								{
							@Override
							public int compare(AccountingInterface e1,AccountingInterface e2)
							{
								if(e1!=null && e2!=null)
								{
									if( e1.getBankName()!=null && e2.getBankName()!=null){
										return e1.getBankName().compareTo(e2.getBankName());}
								}
								else{
									return 0;}
								return 0;
							}
								});
						table.addColumnSortHandler(columnSort);
					}
						
						
						private void addColumngetbankName(){
							
							bankNameColumn=new TextColumn<AccountingInterface>() {
				
								@Override
								public String getValue(AccountingInterface object) {
									if(object.getBankName()!=null)
									return object.getBankName();
									else
										return "";
								}
							};
							
							table.addColumn(bankNameColumn,"Bank Name");
							table.setColumnWidth(bankNameColumn,200 , Unit.PX);
							bankNameColumn.setSortable(true);
						}
						
						
						
						private void addSortinggetchequeDate(){
							
				
							List<AccountingInterface> list=getDataprovider().getList();
							columnSort=new ListHandler<AccountingInterface>(list);
							columnSort.setComparator(chequeDateColumn, new Comparator<AccountingInterface>()
									{
								@Override
								public int compare(AccountingInterface e1,AccountingInterface e2)
								{
									if(e1!=null && e2!=null)
									{
										if( e1.getChequeDate()!=null && e2.getChequeDate()!=null){
											return e1.getChequeDate().compareTo(e2.getChequeDate());}
									}
									else{
										return 0;}
									return 0;
								}
									});
							table.addColumnSortHandler(columnSort);	
				
						}
							
							
							private void addColumngetchequeDate(){
								chequeDateColumn=new TextColumn<AccountingInterface>() {
				
									@Override
									public String getValue(AccountingInterface object) {
										if(object.getChequeDate()!=null)
										{
										    String Date=AppUtility.parseDate(object.getChequeDate());
										    return Date;
										}
										return "";
									}
								};
								
								table.addColumn(chequeDateColumn,"Cheque Dt");
								table.setColumnWidth(chequeDateColumn, 100, Unit.PX);
								chequeDateColumn.setSortable(true);		
							}
							
							
							
							
							private void addSortinggetchequeNo(){
								List<AccountingInterface> list=getDataprovider().getList();
								columnSort=new ListHandler<AccountingInterface>(list);
								columnSort.setComparator(chequeNoColumn, new Comparator<AccountingInterface>()
										{
									
									@Override
									public int compare(AccountingInterface e1,AccountingInterface e2)
									{
										if(e1!=null && e2!=null)
										{
											if( e1.getChequeNumber()!=null && e2.getChequeNumber()!=null){
												return e1.getChequeNumber().compareTo(e2.getChequeNumber());}
										}
										else{
											return 0;}
										return 0;
									}
										});
//									@Override
//									public int compare(AccountingInterface e1,AccountingInterface e2)
//									{
//										if(e1!=null && e2!=null)
//										{
//											if(e1.getChequeNumber().equals(e2.getChequeNumber())){
//												return 0;}
//											if(e1.getChequeNumber()> e2.getChequeNumber()){
//												return 1;}
//											else{
//												return -1;}
//										}
//										else{
//											return 0;}
//									}
//										});
								table.addColumnSortHandler(columnSort);
							}
								
								
								private void addColumngetchequeNo(){
									chequeNoColumn =new TextColumn<AccountingInterface>() {
				
										@Override
										public String getValue(AccountingInterface object) {
											if(object.getChequeNumber()!="")
											return object.getChequeNumber();
											else
												return "";
										}
									};
									
									table.addColumn(chequeNoColumn,"Cheque No.");
									table.setColumnWidth(chequeNoColumn, 100, Unit.PX);
									chequeNoColumn.setSortable(true);
								}
								
								
								
								
								private void addSortinggetpaymentDate(){
				
									List<AccountingInterface> list=getDataprovider().getList();
								columnSort=new ListHandler<AccountingInterface>(list);
								columnSort.setComparator(paymentDateColumn, new Comparator<AccountingInterface>()
										{
									@Override
									public int compare(AccountingInterface e1,AccountingInterface e2)
									{
										if(e1!=null && e2!=null)
										{
											if( e1.getPaymentDate()!=null && e2.getPaymentDate()!=null){
												return e1.getPaymentDate().compareTo(e2.getPaymentDate());}
										}
										else{
											return 0;}
										return 0;
									}
										});
								table.addColumnSortHandler(columnSort);	
								}
									
									
									private void addColumngetpaymentDate(){
										paymentDateColumn=new TextColumn<AccountingInterface>() {
				
											@Override
											public String getValue(AccountingInterface object) {
												if(object.getPaymentDate()!=null)
												{
												    String Date=AppUtility.parseDate(object.getPaymentDate());
												    return Date;
												}
												return "";
											}
										};
										
										table.addColumn(paymentDateColumn,"Pmt Date");
										table.setColumnWidth(paymentDateColumn, 100, Unit.PX);
										paymentDateColumn.setSortable(true);		
									}
									
									
									
									private void addSortinggetpaymentMethod(){
										
										List<AccountingInterface> list=getDataprovider().getList();
										columnSort=new ListHandler<AccountingInterface>(list);
										columnSort.setComparator(paymentMethodColumn, new Comparator<AccountingInterface>()
												{
											@Override
											public int compare(AccountingInterface e1,AccountingInterface e2)
											{
												if(e1!=null && e2!=null)
												{
													if( e1.getPaymentMethod()!=null && e2.getPaymentMethod()!=null){
														return e1.getPaymentMethod().compareTo(e2.getPaymentMethod());}
												}
												else{
													return 0;}
												return 0;
											}
												});
										table.addColumnSortHandler(columnSort);
										
									}
										
										
										private void addColumngetpaymentMethod(){
											paymentMethodColumn=new TextColumn<AccountingInterface>() {
				
												@Override
												public String getValue(AccountingInterface object) {
													if(object.getPaymentMethod()!=null)
													return object.getPaymentMethod();
													else
														return "";
												}
											};
											
											table.addColumn(paymentMethodColumn,"Pmt. Method");
											table.setColumnWidth(paymentMethodColumn,100 , Unit.PX);
											paymentMethodColumn.setSortable(true);
										}
				
				
				
				private void addColumngetapprovedBy() {
					approvedByColumn=new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getApprovedBy()!=null)
							return object.getApprovedBy();
							else
								return "";
						}
					};
					
					table.addColumn(approvedByColumn,"Approved By");
					table.setColumnWidth(approvedByColumn,200 , Unit.PX);
					approvedByColumn.setSortable(true);		
				}
				private void addSortinggetapprovedBy() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(approvedByColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if( e1.getApprovedBy()!=null && e2.getApprovedBy()!=null){
									return e1.getApprovedBy().compareTo(e2.getApprovedBy());}
							}
							else{
								return 0;}
							return 0;
						}
							});
					table.addColumnSortHandler(columnSort);		
				}
				
				
				
				
				
				
				private void addColumngetrequestedBy() {
					requestedByColumn=new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getRequestedBy()!=null)
							return object.getRequestedBy();
							else
								return "";
						}
					};
					
					table.addColumn(requestedByColumn,"Req By");
					table.setColumnWidth(requestedByColumn,200 , Unit.PX);
					requestedByColumn.setSortable(true);			
				}
				
				private void addSortinggetrequestedBy() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(requestedByColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if( e1.getRequestedBy()!=null && e2.getRequestedBy()!=null){
									return e1.getRequestedBy().compareTo(e2.getRequestedBy());}
							}
							else{
								return 0;}
							return 0;
						}
							});
					table.addColumnSortHandler(columnSort);				
				}
				
				private void addColumngetemployeeName() {
					employeeNameColumn=new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getEmployeeName()!=null)
							return object.getEmployeeName();
							else
								return "";
						}
					};
					
					table.addColumn(employeeNameColumn,"Emp. Name");
					table.setColumnWidth(employeeNameColumn,200 , Unit.PX);
					employeeNameColumn.setSortable(true);			
				}
				private void addSortinggetemployeeName() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(employeeNameColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if( e1.getEmployeeName()!=null && e2.getEmployeeName()!=null){
									return e1.getEmployeeName().compareTo(e2.getEmployeeName());}
							}
							else{
								return 0;}
							return 0;
						}
							});
					table.addColumnSortHandler(columnSort);						
				}
				
				
				private void addColumngetemployeeID() {
					employeeIDColumn =new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getEmployeeID()!=0)
							return object.getEmployeeID()+"";
							else
								return "";
						}
					};
					
					table.addColumn(employeeIDColumn," Emp. ID");
					table.setColumnWidth(employeeIDColumn, 100, Unit.PX);
					employeeIDColumn.setSortable(true);
				}				
				private void addSortinggetemployeeID() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(employeeIDColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if(e1.getEmployeeID()== e2.getEmployeeID()){
									return 0;}
								if(e1.getEmployeeID()> e2.getEmployeeID()){
									return 1;}
								else{
									return -1;}
							}
							else{
								return 0;}
						}
							});
					table.addColumnSortHandler(columnSort);
				}
					
				
				
				
				
				
				
				
				
				
				
				
				
				
				private void addColumngetvendorCell() {
					vendorCellColumn =new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getVendorCell()!=0)
							return object.getVendorCell()+"";
							else
								return "";
						}
					};
					
					table.addColumn(vendorCellColumn,"Vendor Cell");
					table.setColumnWidth(vendorCellColumn, 100, Unit.PX);
					vendorCellColumn.setSortable(true);
				}
				private void addSortinggetvendorCell() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(vendorCellColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if(e1.getVendorCell()== e2.getVendorCell()){
									return 0;}
								if(e1.getVendorCell()> e2.getVendorCell()){
									return 1;}
								else{
									return -1;}
							}
							else{
								return 0;}
						}
							});
					table.addColumnSortHandler(columnSort);
					
				}
					
				
				private void addColumngetvendorName() {
					vendorNameColumn=new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getVendorName()!=null)
							return object.getVendorName();
							else
								return "";
						}
					};
					
					table.addColumn(vendorNameColumn,"Vendor Name");
					table.setColumnWidth(vendorNameColumn,200 , Unit.PX);
					vendorNameColumn.setSortable(true);			
				}
					
				private void addSortinggetvendorName() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(vendorNameColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if( e1.getVendorName()!=null && e2.getVendorName()!=null){
									return e1.getVendorName().compareTo(e2.getVendorName());}
							}
							else{
								return 0;}
							return 0;
						}
							});
					table.addColumnSortHandler(columnSort);								
				}		
				
				
				
				
				private void addColumngetvendorID() {
					vendorIDColumn =new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getVendorID()!=0)
							return object.getVendorID()+"";
							else
								return "";
						}
					};
					
					table.addColumn(vendorIDColumn," Vendor ID");
					table.setColumnWidth(vendorIDColumn, 100, Unit.PX);
					vendorIDColumn.setSortable(true);
				}				
							
				private void addSortinggetvendorID() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(vendorIDColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if(e1.getVendorID()== e2.getVendorID()){
									return 0;}
								if(e1.getVendorID()> e2.getVendorID()){
									return 1;}
								else{
									return -1;}
							}
							else{
								return 0;}
						}
							});
					table.addColumnSortHandler(columnSort);		
				}
				
				
				private void addColumngetcustomerCell() {
					customerCellColumn =new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getCustomerCell()!=0)
							return object.getCustomerCell()+"";
							else
								return "";
						}
					};
					
					table.addColumn(customerCellColumn,"Customer Cell");
					table.setColumnWidth(customerCellColumn, 100, Unit.PX);
					customerCellColumn.setSortable(true);
				}
							
				private void addSortinggetcustomerCell() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(customerCellColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if(e1.getCustomerCell()== e2.getCustomerCell()){
									return 0;}
								if(e1.getCustomerCell()> e2.getCustomerCell()){
									return 1;}
								else{
									return -1;}
							}
							else{
								return 0;}
						}
							});
					table.addColumnSortHandler(columnSort);		
				}
				
				
				
				private void addColumngetcustomerName() {
					customerNameColumn=new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getCustomerName()!=null)
							return object.getCustomerName();
							else
								return "";
						}
					};
					
					table.addColumn(customerNameColumn,"Cust. Name");
					table.setColumnWidth(customerNameColumn,200 , Unit.PX);
					customerNameColumn.setSortable(true);			
				}
					
				private void addSortinggetcustomerName() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(customerNameColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if( e1.getCustomerName()!=null && e2.getCustomerName()!=null){
									return e1.getCustomerName().compareTo(e2.getCustomerName());}
							}
							else{
								return 0;}
							return 0;
						}
							});
					table.addColumnSortHandler(columnSort);								
							
				}
				
				private void addColumngetaccountType() {
					accountTypeColumn=new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getAccountType()!=null)
							return object.getAccountType();
							else
								return "";
						}
					};
					
					table.addColumn(accountTypeColumn,"Acc. Type");
					table.setColumnWidth(accountTypeColumn, 75, Unit.PX);
					accountTypeColumn.setSortable(true);
				}
				
				
				private void addSortinggetaccountType() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(accountTypeColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if( e1.getAccountType()!=null && e2.getAccountType()!=null){
									return e1.getAccountType().compareTo(e2.getAccountType());}
							}
							else{
								return 0;}
							return 0;
						}
							});
					table.addColumnSortHandler(columnSort);		
				}
				
				private void addColumngetcustomerID() {
					customerIDColumn =new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getCustomerID()!=0)
							return object.getCustomerID()+"";
							else
								return "";
						}
					};
					
					table.addColumn(customerIDColumn," Customer ID");
					table.setColumnWidth(customerIDColumn, 100, Unit.PX);
					customerIDColumn.setSortable(true);
				}				
						
				
				private void addSortinggetcustomerID() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(customerIDColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if(e1.getCustomerID()== e2.getCustomerID()){
									return 0;}
								if(e1.getCustomerID()> e2.getCustomerID()){
									return 1;}
								else{
									return -1;}
							}
							else{
								return 0;}
						}
							});
					table.addColumnSortHandler(columnSort);						
				}
				
				
				/************************************************************************************************************/
				
				
				
				
				
				private void addColumngetreferenceDocumentType2() {
					referenceDocument2TypeColumn=new TextColumn <AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getReferenceDocumentType2()!=null)
							return object.getReferenceDocumentType2();
							else
								return "";
						}
					};
					
					table.addColumn(referenceDocument2TypeColumn,"Ref.Doc.Type-2");
					table.setColumnWidth(referenceDocument2TypeColumn, 100, Unit.PX);
					referenceDocument1TypeColumn.setSortable(true);		
				}
				
				private void addSortinggetreferenceDocumentType2() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(referenceDocument2TypeColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if( e1.getReferenceDocumentType2()!=null && e2.getReferenceDocumentType2()!=null){
									return e1.getReferenceDocumentType2().compareTo(e2.getReferenceDocumentType2());}
							}
							else{
								return 0;}
							return 0;
						}
							});
					table.addColumnSortHandler(columnSort);			
							
				}
				
				private void addColumngetreferenceDocumentDate2() {
					referenceDocumentDate2Column=new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getReferenceDocumentDate2()!=null)
							{
							    String Date=AppUtility.parseDate(object.getReferenceDocumentDate2());
							    return Date;
							}
							return "";
						}
					};
					
					table.addColumn(referenceDocumentDate2Column,"Ref.Doc.Dt.2 ");
					table.setColumnWidth(referenceDocumentDate2Column, 100, Unit.PX);
					referenceDocumentDate2Column.setSortable(true);		
				}
				
				private void addSortinggetreferenceDocumentDate2() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(referenceDocumentDate2Column, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if( e1.getReferenceDocumentDate2()!=null && e2.getReferenceDocumentDate2()!=null){
									return e1.getReferenceDocumentDate2().compareTo(e2.getReferenceDocumentDate2());}
							}
							else{
								return 0;}
							return 0;
						}
							});
					table.addColumnSortHandler(columnSort);		
				}
				
				
				
				
				
				private void addColumngetreferenceDocumentNumber2() {
					referenceDocumentNumber2Column=new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getReferenceDocumentNo2()!=null)
							return object.getReferenceDocumentNo2();
							else
								return "";
						}
					};
					
					table.addColumn(referenceDocumentNumber2Column,"Ref.Doc.No.-2");
					table.setColumnWidth(referenceDocumentNumber2Column, 100, Unit.PX);
					referenceDocumentNumber2Column.setSortable(true);		
				}
				
				private void addSortinggetreferenceDocumentNumber2() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(referenceDocumentNumber2Column, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if( e1.getReferenceDocumentNo2()!=null && e2.getReferenceDocumentNo2()!=null){
									return e1.getReferenceDocumentNo2().compareTo(e2.getReferenceDocumentNo2());}
							}
							else{
								return 0;}
							return 0;
						}
							});
					table.addColumnSortHandler(columnSort);		
				}
				
				
				private void addColumngetreferenceDocumentDate1() {
					referenceDocumentDate1Column=new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getReferenceDocumentDate1()!=null)
							{
							    String Date=AppUtility.parseDate(object.getReferenceDocumentDate1());
							    return Date;
							}
							return "";
						}
					};
					
					table.addColumn(referenceDocumentDate1Column,"Ref.Doc.Dt.1");
					table.setColumnWidth(referenceDocumentDate1Column, 100, Unit.PX);
					referenceDocumentDate1Column.setSortable(true);		
					
				}
				
				private void addSortinggetreferenceDocumentDate1() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(referenceDocumentDate1Column, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if( e1.getReferenceDocumentDate1()!=null && e2.getReferenceDocumentDate1()!=null){
									return e1.getReferenceDocumentDate1().compareTo(e2.getReferenceDocumentDate1());}
							}
							else{
								return 0;}
							return 0;
						}
							});
					table.addColumnSortHandler(columnSort);		
				}
				
				
				
				private void addColumngetreferenceDocumentType1() {
					referenceDocument1TypeColumn=new TextColumn <AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getReferenceDocumentType1()!=null)
							return object.getReferenceDocumentType1();
							else
								return "";
						}
					};
					
					table.addColumn(referenceDocument1TypeColumn,"Ref.Doc. Type");
					table.setColumnWidth(referenceDocument1TypeColumn, 150, Unit.PX);
					referenceDocument1TypeColumn.setSortable(true);		
				}
				
				private void addSortinggetreferenceDocumentType1() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(referenceDocument1TypeColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if( e1.getReferenceDocumentType1()!=null && e2.getReferenceDocumentType1()!=null){
									return e1.getReferenceDocumentType1().compareTo(e2.getReferenceDocumentType1());}
							}
							else{
								return 0;}
							return 0;
						}
							});
					table.addColumnSortHandler(columnSort);			
				}
				
				
				private void addColumngetreferenceDocumentNumber1() {
					referenceDocumentNumber1Column=new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getReferenceDocumentNo1()!=null){
								return object.getReferenceDocumentNo1();
							}
							else{
								return "";
							}
						}
					};
					
					table.addColumn(referenceDocumentNumber1Column,"Ref.Doc.No.-1");
					table.setColumnWidth(referenceDocumentNumber1Column, 100, Unit.PX);
					referenceDocumentNumber1Column.setSortable(true);
				}
				
				private void addSortinggetreferenceDocumentNumber1() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(referenceDocumentNumber1Column, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if( e1.getReferenceDocumentNo1()!=null && e2.getReferenceDocumentNo1()!=null){
									return e1.getReferenceDocumentNo1().compareTo(e2.getReferenceDocumentNo1());}
							}
							else{
								return 0;}
							return 0;
						}
							});
					table.addColumnSortHandler(columnSort);		
				}
				
				
				private void addColumngetdocumentGl() {
					documentGlColumn=new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getDocumentGL()!=null)
							return object.getDocumentGL();
							else
								return "";
						}
					};
					
					table.addColumn(documentGlColumn,"Doc. GL");
					table.setColumnWidth(documentGlColumn, 100, Unit.PX);
					documentGlColumn.setSortable(true);
				}
				
				private void addSortinggetdocumentGl() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(documentGlColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if( e1.getDocumentGL()!=null && e2.getDocumentGL()!=null){
									return e1.getDocumentGL().compareTo(e2.getDocumentGL());}
							}
							else{
								return 0;}
							return 0;
						}
							});
					table.addColumnSortHandler(columnSort);		
				}
				
				
				
				private void addColumngetdocumentDate(){
					
					documentDateColumn=new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getDocumentDate()!=null)
							{
							    String Date=AppUtility.parseDate(object.getDocumentDate());
							    return Date;
							}
							return "";
						}
					};
					
					table.addColumn(documentDateColumn,"Doc.Date");
					table.setColumnWidth(documentDateColumn, 100, Unit.PX);
					documentDateColumn.setSortable(true);	
				}
				
				
				private void addSortinggetdocumentDate(){
					
					List<AccountingInterface> list=getDataprovider().getList();
				columnSort=new ListHandler<AccountingInterface>(list);
				columnSort.setComparator(documentDateColumn, new Comparator<AccountingInterface>()
						{
					@Override
					public int compare(AccountingInterface e1,AccountingInterface e2)
					{
						if(e1!=null && e2!=null)
						{
							if( e1.getDocumentDate()!=null && e2.getDocumentDate()!=null){
								return e1.getDocumentDate().compareTo(e2.getDocumentDate());}
						}
						else{
							return 0;}
						return 0;
					}
						});
				table.addColumnSortHandler(columnSort);		
					
					
				}
				
				
				private void addSortinggetdocumentTitle() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(documentTitleColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if( e1.getDocumentTitle()!=null && e2.getDocumentTitle()!=null){
									return e1.getDocumentTitle().compareTo(e2.getDocumentTitle());}
							}
							else{
								return 0;}
							return 0;
						}
							});
					table.addColumnSortHandler(columnSort);		
				}
				
				private void addColumngetdocumentTitle() {
				
					documentTitleColumn=new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getDocumentTitle()!=null)
							return object.getDocumentTitle();
							else
								return "";
						}
					};
					
					table.addColumn(documentTitleColumn,"Doc. Title");
					table.setColumnWidth(documentTitleColumn, 150, Unit.PX);
					documentTitleColumn.setSortable(true);		
				}
				
				
				private void addColumngetdocumentID() {
					documentIDColumn=new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getBranch()!=null)
							return object.getDocumentID()+"";
							else
								return "";
						}
					};
					
					table.addColumn(documentIDColumn,"Doc. ID");
					table.setColumnWidth(documentIDColumn, 100, Unit.PX);
					documentIDColumn.setSortable(true);
							
				}
				
				private void addSortingngetdocumentID() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(documentIDColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if(e1.getDocumentID()== e2.getDocumentID()){
									return 0;}
								if(e1.getDocumentID()> e2.getDocumentID()){
									return 1;}
								else{
									return -1;}
							}
							else{
								return 0;}
						}
							});
					table.addColumnSortHandler(columnSort);		
				}
				
				
				
				private void addColumngetdocumentType() {
					documentTypeColumn =new TextColumn <AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getDocumentType()!=null)
							return object.getDocumentType();
							else
								return "";
						}
					};
					
					table.addColumn(documentTypeColumn,"Doc. Type");
					table.setColumnWidth(documentTypeColumn, 150, Unit.PX);
					documentTypeColumn.setSortable(true);			
				}
				private void addSortinggetdocumentType() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(documentTypeColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if( e1.getDocumentType()!=null && e2.getDocumentType()!=null){
									return e1.getDocumentType().compareTo(e2.getDocumentType());}
							}
							else{
								return 0;}
							return 0;
						}
							});
					table.addColumnSortHandler(columnSort);		
				}
				
				
				
				
				
				private void addColumngetsynchedBy() {
					synchedByColumn =new TextColumn <AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getBranch()!=null)
							return object.getSynchedBy();
							else
								return "";
						}
					};
					
					table.addColumn(synchedByColumn,"Synched By");
					table.setColumnWidth(synchedByColumn, 120, Unit.PX);
					synchedByColumn.setSortable(true);		
				}
				
				private void addSortinggetsynchedBy() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(synchedByColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if( e1.getSynchedBy()!=null && e2.getSynchedBy()!=null){
									return e1.getSynchedBy().compareTo(e2.getSynchedBy());}
							}
							else{
								return 0;}
							return 0;
						}
							});
					table.addColumnSortHandler(columnSort);
				}
				
				
				
				private void addColumngetdateofSync() {
					
					dateofSyncColumn=new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getDateofSynch()!=null)
							{
							    String Date=AppUtility.parseDate(object.getDateofSynch());
							    return Date;
							}
							return "";
						}
					};
					
					table.addColumn(dateofSyncColumn,"Sync Date");
					table.setColumnWidth(dateofSyncColumn, 100, Unit.PX);
					dateofSyncColumn.setSortable(true);		
				}
				
				
				private void addSortinggetdateofSync() {
					
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(dateofSyncColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if( e1.getDateofSynch()!=null && e2.getDateofSynch()!=null){
									return e1.getDateofSynch().compareTo(e2.getDateofSynch());}
							}
							else{
								return 0;}
							return 0;
						}
							});
					table.addColumnSortHandler(columnSort);	
				}
					
					
					
				
				
				
				private void addColumngetremark() {
					remarkColumn =new TextColumn <AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getRemark()!=null)
							return object.getRemark();
							else
								return "";
						}
					};
					
					table.addColumn(remarkColumn,"Remark");
					table.setColumnWidth(remarkColumn, 150, Unit.PX);
					remarkColumn.setSortable(true);		
				}
				
				private void addSortinggetremark() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(remarkColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if( e1.getRemark()!=null && e2.getRemark()!=null){
									return e1.getRemark().compareTo(e2.getRemark());}
							}
							else{
								return 0;}
							return 0;
						}
							});
					table.addColumnSortHandler(columnSort);
					
				}
				
				
				
				
				
				
				
				
				
				
				
				
				/*******************************************************************************************************************************/
				
				
				private void addSortinggetstatus() {
					List<AccountingInterface> list = getDataprovider().getList();
					columnSort = new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(statusColumn,
							new Comparator<AccountingInterface>() {
								@Override
								public int compare(AccountingInterface e1, AccountingInterface e2) {
									if (e1 != null && e2 != null) {
										if (e1.getStatus() == e2.getStatus()) {
											return 0;
										} else {
											return -1;
										}
									} else {
										return 0;
									}
								}
							});
					table.addColumnSortHandler(columnSort);		
				}
				
				
				private void addColumngetstatus() {
					statusColumn =new TextColumn <AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getStatus()!=null)
							return object.getStatus();
							else
								return "";
						}
					};
					
					table.addColumn(statusColumn,"Status");
					table.setColumnWidth(statusColumn, 90, Unit.PX);
					statusColumn.setSortable(true);
				}		
				
				/**************************************************************/
				private void addSortinggetcForm(){
				
				 List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(cFormColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if( e1.getcForm()!=null && e2.getcForm()!=null){
									return e1.getcForm().compareTo(e2.getcForm());}
							}
							else{
								return 0;}
							return 0;
						}
							});
					table.addColumnSortHandler(columnSort);
				}
				
				
				private void addColumngetcForm(){
					
					cFormColumn=new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getcForm()!=null)
							return object.getcForm();
							else
								return "";
						}
					};
					
					table.addColumn(cFormColumn,"cForm");
					table.setColumnWidth(cFormColumn,60 , Unit.PX);
					cFormColumn.setSortable(true);
				}
				/*****************************************************************/
				private void addSortinggetdocumentID() {
				
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(documentIDColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if(e1.getDocumentID()== e2.getDocumentID()){
									return 0;}
								if(e1.getDocumentID()> e2.getDocumentID()){
									return 1;}
								else{
									return -1;}
							}
							else{
								return 0;}
						}
							});
					table.addColumnSortHandler(columnSort);		
				}
				
				
				
				
				/*******************************************************************************************/	
				
				private void addSortinggetmodule() {
					List<AccountingInterface> list=getDataprovider().getList();
					columnSort=new ListHandler<AccountingInterface>(list);
					columnSort.setComparator(moduleColumn, new Comparator<AccountingInterface>()
							{
						@Override
						public int compare(AccountingInterface e1,AccountingInterface e2)
						{
							if(e1!=null && e2!=null)
							{
								if( e1.getModule()!=null && e2.getModule()!=null){
									return e1.getModule().compareTo(e2.getModule());}
							}
							else{
								return 0;}
							return 0;
						}
							});
					table.addColumnSortHandler(columnSort);		
				}
				private void addColumngetmodule() {
					moduleColumn=new TextColumn<AccountingInterface>() {
				
						@Override
						public String getValue(AccountingInterface object) {
							if(object.getModule()!=null)
							return object.getModule();
							else
								return "";
						}
					};
					
					table.addColumn(moduleColumn,"Module");
					table.setColumnWidth(moduleColumn, 100, Unit.PX);
					moduleColumn.setSortable(true);
					
				}
				
				
				
				@Override
				protected void initializekeyprovider() {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void addFieldUpdater() {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void setEnable(boolean state) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void applyStyle() {
					// TODO Auto-generated method stub
					
				}


}
