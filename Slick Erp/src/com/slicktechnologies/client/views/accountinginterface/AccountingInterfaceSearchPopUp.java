package com.slicktechnologies.client.views.accountinginterface;

import java.util.Vector;

import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.config.CategoryTypeFactory;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.tallyaccounts.AccountingInterface;

public class AccountingInterfaceSearchPopUp extends SearchPopUpScreen<AccountingInterface>{

	IntegerBox accountingInterfaceID;
	ObjectListBox<Employee>accountingInterfaceCreatedby;
	ListBox status;
	ObjectListBox<Employee>synchedBy;
	DateComparator dateComparator;
	
	ObjectListBox<ConfigCategory> module;
	ObjectListBox<Type>	documentType;
	IntegerBox documentID;
	IntegerBox	referenceDocumentNo1;
	DateBox referenceDocumentDateNo;
	ObjectListBox<Type> referencedocumentType1;
	ObjectListBox<Branch>Branch;
	
	/**
	 * Rohan added this code for Ultra pest control for 
	 */
	
	public PersonInfoComposite personInfo;
	public PersonInfoComposite vendor;
	DateComparator creationDateComparator;
	
	/**
	 * ends here 
	 */
	/** date 18.12.2018 added by komal for doc date filter**/
	DateComparator documentDateComparator;
	
	public AccountingInterfaceSearchPopUp() {
		super();
		createGui();
	}
	
	public AccountingInterfaceSearchPopUp(boolean b) {
		super(b);
		createGui();
	}
	
	
	
	private void initializewidget() {
		
		
		
		accountingInterfaceID = new IntegerBox();
		accountingInterfaceCreatedby = new ObjectListBox<Employee>();
		AppUtility.makeSalesPersonListBoxLive(accountingInterfaceCreatedby);
		
		status = new ListBox();
		AppUtility.setStatusListBox(status, AccountingInterface.getStatusList());
		
		synchedBy = new ObjectListBox<Employee>();
		AppUtility.makeSalesPersonListBoxLive(synchedBy);
		
		dateComparator= new DateComparator("dateofSynch",new AccountingInterface());
		
		
		
		module= new ObjectListBox<ConfigCategory>();
		documentType=new ObjectListBox<Type>();
		CategoryTypeFactory.initiateOneManyFunctionality(module,documentType);
		AppUtility.makeTypeListBoxLive(documentType, Screen.MODULENAME);
		
		documentID = new IntegerBox();
		referenceDocumentNo1 = new IntegerBox();
		referenceDocumentDateNo=new DateBoxWithYearSelector();
		referencedocumentType1 = new ObjectListBox<Type>();
		AppUtility.makeTypeListBoxLive(referencedocumentType1, Screen.DOCUMENTNAME);
	
		//  rohan added this code for branch level restriction
		Branch= new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(Branch);
		
		//  rohan added customer search in Accounting interface
		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new Customer());
		personInfo=new PersonInfoComposite(querry,false);
		
		personInfo.getCustomerId().getHeaderLabel().setText("Customer ID");
		personInfo.getCustomerName().getHeaderLabel().setText("Customer Name");
		personInfo.getCustomerCell().getHeaderLabel().setText("Customer Cell");
		
		//  rohan added this code for purchase vendor search
		
		MyQuerry querry1=new MyQuerry();
		querry1.setQuerryObject(new Vendor());
		vendor=new PersonInfoComposite(querry1,false);
		
		vendor.getCustomerId().getHeaderLabel().setText("Vendor ID");
		vendor.getCustomerName().getHeaderLabel().setText("Vendor Name");
		vendor.getCustomerCell().getHeaderLabel().setText("Vendor Cell");
		
		//  for searching Data which is not synch
		creationDateComparator= new DateComparator("accountingInterfaceCreationDate",new AccountingInterface());
		
		/** date 18.12.2018 added by komal for doc date filter**/
		documentDateComparator =new DateComparator("documentDate",new AccountingInterface());
	}
	
	public void createScreen()
	{
		initializewidget();
		FormFieldBuilder builder;
		
		
		builder = new FormFieldBuilder("Acc.Int.ID",accountingInterfaceID);
		FormField accountingInterfaceID= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
//		builder = new FormFieldBuilder("Acc.Int.Create.Dt.",accountingInterfaceCreationDate);
//		FormField accountingInterfaceCreationDate= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Acc.Int.Created By",accountingInterfaceCreatedby);
		FormField accountingInterfaceCreatedby= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Status",status);
		FormField status= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
//		builder = new FormFieldBuilder("Date of Sync",dateofSync);
//		FormField dateofSync= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Synched By",synchedBy);
		FormField synchedBy= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("From Date(Date of Sync)",dateComparator.getFromDate());
		FormField fdateComparator= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("To Date(Date of Sync)",dateComparator.getToDate());
		FormField fdateComparator1= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
//		builder = new FormFieldBuilder("From Date(Creation Date)",creationDate);
//		FormField creationDate= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
//		
//		builder = new FormFieldBuilder("To Date(Creation date)",creationtoDate);
//		FormField toDate= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Module",module);
		FormField module= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Document Type",documentType);
		FormField documentType= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Doc. ID",documentID);
		FormField documentID= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Branch",Branch);
		FormField Branch= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("",personInfo);
		FormField fpersonInfo= builder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		builder = new FormFieldBuilder("From Date(Acc.Int.Creation Date)",creationDateComparator.getFromDate());
		FormField fcreationDateComparator= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("To Date(Acc.Int.Creation Date)",creationDateComparator.getToDate());
		FormField fcreationDateComparator1= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("",vendor);
		FormField fvendor= builder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		/** date 18.12.2018 added by komal for doc date filter**/
		builder = new FormFieldBuilder("From Date(Doc. Date)",documentDateComparator.getFromDate());
		FormField fdocumentDateComparator= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("To Date(Doc. Date)",documentDateComparator.getToDate());
		FormField fdocumentDateComparator1= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	
	this.fields=new FormField[][]{
			{fcreationDateComparator,fcreationDateComparator1,fdateComparator,fdateComparator1},
			{fdocumentDateComparator , fdocumentDateComparator1},/** date 18.12.2018 added by komal for doc date filter**/
			{accountingInterfaceID,accountingInterfaceCreatedby,synchedBy,status},
			{Branch,module,documentType,documentID},
			{fpersonInfo},
			{fvendor},
			
	};
	}
	
	
	@Override
	public MyQuerry getQuerry() {
		

		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		
		
		if(dateComparator.getValue()!=null)
		{
			filtervec.addAll(dateComparator.getValue());
		}
		
		if(creationDateComparator.getValue()!=null)
		{
			filtervec.addAll(creationDateComparator.getValue());
		}


		if(accountingInterfaceID.getValue()!=null)
		{  
			temp=new Filter();
			temp.setIntValue(accountingInterfaceID.getValue());
			temp.setQuerryString("count");
			filtervec.add(temp);
		}
		
		if(accountingInterfaceCreatedby.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(accountingInterfaceCreatedby.getValue().trim());
			temp.setQuerryString("accountingInterfaceCreatedBy");
			filtervec.add(temp);
			
		}
		
		if(documentID.getValue()!=null)
		{  
			temp=new Filter();
			temp.setIntValue(documentID.getValue());
			temp.setQuerryString("documentID");
			filtervec.add(temp);
			}
		
		
		if(synchedBy.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(synchedBy.getValue().trim());
			temp.setQuerryString("synchedBy");
			filtervec.add(temp);
			
		}
		
		
		if(documentType.getSelectedIndex()!=0){
			temp=new Filter();
			//Ashwini Patil Date:26-08-2022
			if(documentType.getValue().trim().equals("Invoice Details"))
				temp.setStringValue("Invoice");
			else if(documentType.getValue().trim().equals("Payment Details"))
				temp.setStringValue("Customer Payment");
			else
				temp.setStringValue(documentType.getValue().trim());
			temp.setQuerryString("documentType");
			filtervec.add(temp);
			
		}
		
		// rohan added this code for adding vendor search in accounting interface  
		
		if(vendor.getIdValue()!=-1)
		  {
		  temp=new Filter();
		  temp.setIntValue(vendor.getIdValue());
		  temp.setQuerryString("vendorID");
		  filtervec.add(temp);
		  }
			
		  if(!(vendor.getFullNameValue().equals("")))
		  {
		  temp=new Filter();
		  temp.setStringValue(vendor.getFullNameValue());
		  temp.setQuerryString("vendorName");
		  filtervec.add(temp);
		  }
		  if(vendor.getCellValue()!=-1l)
		  {
		  temp=new Filter();
		  temp.setLongValue(vendor.getCellValue());
		  temp.setQuerryString("vendorCell");
		  filtervec.add(temp);
		  } 
		
		//   rohan added this code for adding customer search in Accounting Interface
		
		
		if(personInfo.getIdValue()!=-1)
		  {
		  temp=new Filter();
		  temp.setIntValue(personInfo.getIdValue());
		  temp.setQuerryString("cinfo.count");
		  filtervec.add(temp);
		  }
		  
		  if(!(personInfo.getFullNameValue().equals("")))
		  {
		  temp=new Filter();
		  temp.setStringValue(personInfo.getFullNameValue());
		  temp.setQuerryString("cinfo.fullName");
		  filtervec.add(temp);
		  }
		  if(personInfo.getCellValue()!=-1l)
		  {
		  temp=new Filter();
		  temp.setLongValue(personInfo.getCellValue());
		  temp.setQuerryString("cinfo.cellNumber");
		  filtervec.add(temp);
		  }
		
		  //  ends here 

		if(status.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(status.getItemText(status.getSelectedIndex()).trim());
			temp.setQuerryString("status");
			filtervec.add(temp);
		}
			
		if(module.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(module.getValue().trim());
			temp.setQuerryString("module");
			filtervec.add(temp);
		}
			
		if(Branch.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(Branch.getValue().trim());
			temp.setQuerryString("branch");
			filtervec.add(temp);
		}
		
		/** date 18.12.2018 added by komal for doc date filter**/
		if(documentDateComparator.getValue()!=null)
		{
			filtervec.addAll(documentDateComparator.getValue());
		}
			
	

		
		MyQuerry querry= new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new AccountingInterface());					
		return querry;
		
		
	}

	@Override
	public boolean validate() {
		if(LoginPresenter.branchRestrictionFlag)
		{
		if(Branch.getSelectedIndex()==0)
		{
			showDialogMessage("Select Branch");
			return false;
		}
		}
		return true;
	}
	
	
}
