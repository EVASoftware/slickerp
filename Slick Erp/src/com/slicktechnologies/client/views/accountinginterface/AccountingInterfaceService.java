package com.slicktechnologies.client.views.accountinginterface;

import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.slicktechnologies.shared.common.tallyaccounts.AccountingInterface;

@RemoteServiceRelativePath("accountinginterfaceimpl")
public interface AccountingInterfaceService extends RemoteService{

	public void getAccountingInterfaceDataSynched(List<AccountingInterface> acclist, String syncStatus, String syncBy); 

	public void updateSynchDataWithSynchDate(List<AccountingInterface> acclist);

}
