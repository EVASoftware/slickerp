package com.slicktechnologies.client.views.deliverynote;

import java.util.List;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.salesprocess.BillingDocumentDetails;

public class PaymentTermsTable extends SuperTable<PaymentTerms> {
	TextColumn<PaymentTerms> daysColumn;
	TextColumn<PaymentTerms> percentColumn;
	TextColumn<PaymentTerms> commentColumn;
	private Column<PaymentTerms, String> delete;
	
	public PaymentTermsTable() {
		super();
	}

	@Override
	public void createTable() {
		addColumnDays();
		addColumnPercent();
		addColumnComment();
		addColumnDelete();
		setFieldUpdaterOnDelete();
		table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
	}
	
	public void addColumnDays()
	{
		daysColumn=new TextColumn<PaymentTerms>() {

			@Override
			public String getValue(PaymentTerms object) {
				return object.getPayTermDays()+"";
			}
		};
		
		table.addColumn(daysColumn,"Days");
		//table.setColumnWidth(daysColumn, 150, Unit.PX);
		daysColumn.setSortable(true);
	}
	
	public void addColumnPercent()
	{
		percentColumn=new TextColumn<PaymentTerms>() {

			@Override
			public String getValue(PaymentTerms object) {
				return object.getPayTermPercent()+"";
			}
		};
		
		table.addColumn(percentColumn,"Percent");
		//table.setColumnWidth(percentColumn, 150, Unit.PX);
		percentColumn.setSortable(true);
	}
	
	public void addColumnComment()
	{
		commentColumn=new TextColumn<PaymentTerms>() {

			@Override
			public String getValue(PaymentTerms object) {
				return object.getPayTermComment().trim();
			}
		};
		
		table.addColumn(commentColumn,"Comment");
		//table.setColumnWidth(commentColumn, 150, Unit.PX);
		commentColumn.setSortable(true);
	}
	
	private void addColumnDelete()
	{
		ButtonCell btnCell= new ButtonCell();
		delete = new Column<PaymentTerms, String>(btnCell) {

			@Override
			public String getValue(PaymentTerms object) {
				
				return "Delete";
			}
		};
		table.addColumn(delete,"Delete");
	}

	private void setFieldUpdaterOnDelete()
	{
		delete.setFieldUpdater(new FieldUpdater<PaymentTerms, String>() {
			
			@Override
			public void update(int index, PaymentTerms object, String value) {
				getDataprovider().getList().remove(index);
				table.redrawRow(index);
			
			}
		});
	}
	
	
	private void addeditColumn()
	{
		addColumnDays();
		addColumnPercent();
		addColumnComment();
		addColumnDelete();
		setFieldUpdaterOnDelete();
	}
	
	private void addViewColumn()
	{
		addColumnDays();
		addColumnPercent();
		addColumnComment();
	}
	
	
	public  void setEnabled(boolean state)
	{
		int tablecolcount=this.table.getColumnCount();
		for(int i=tablecolcount-1;i>-1;i--)
		  table.removeColumn(i);
		if(state ==true)
			addeditColumn();
		if(state==false)
			addViewColumn();
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}	

}
