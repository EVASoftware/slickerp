package com.slicktechnologies.client.views.deliverynote;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.TextAlign;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.LongBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.AddressComposite;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.composite.UploadComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.MyLongBox;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.approvalutility.ApprovableFormScreen;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.config.CategoryTypeFactory;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.client.views.inventory.recievingnote.GRNPresenter;
import com.slicktechnologies.client.views.inventory.recievingnote.ProductQty;
import com.slicktechnologies.shared.ProductOtherCharges;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.TaxDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.deliverynote.DeliveryLineItems;
import com.slicktechnologies.shared.common.deliverynote.DeliveryNote;
import com.slicktechnologies.shared.common.deliverynote.ShippingSteps;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.inventory.GRNDetails;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.shippingpackingprocess.PackingMethod;
import com.slicktechnologies.shared.common.shippingpackingprocess.ShippingMethod;

public class DeliveryNoteForm extends ApprovableFormScreen<DeliveryNote> implements ClickHandler, ChangeHandler {
	
	
	PersonInfoComposite personInfoComposite;
	
//	ListBox cbcformlis;
//	ObjectListBox<TaxDetails> olbcstpercent;
	
	TextBox tbSalesOrderId,tbLeadId,tbQuotatinId;
	DateBox dbReferenceDate;
	TextBox tbReferenceNumber;
	ObjectListBox<Employee> olbeSalesPerson;
	UploadComposite ucChooseFile;
	TextArea tadescription;
	TextBox ibDeliveryNoteId;
	TextBox tbStatus;
	ObjectListBox<Branch> olbbBranch;
	ObjectListBox<Config> olbdeliveryGroup;
	ObjectListBox<Type> olbdeliveryType;
	ObjectListBox<ConfigCategory> olbdeliveryCategory;
	DateBox dbdeliverydate;
	ObjectListBox<ShippingMethod> olbShippingMethod;
	ObjectListBox<PackingMethod> olbPackingMethod;
	ListBox olbshippingchecklist;
	ListBox olbpackingchecklist;
	ObjectListBox<Employee> olbApproverName;
	DeliverySalesLineItemTable deliveryLineItem;
	ShippingStepsTable shippingListTable;
	PackingStepsTable packingListTable;
	DoubleBox dototalamt,doamtincltax,donetpayamt;
	TextBox tbvehicleno;
	ProductChargesTable chargesTable;
	ProductTaxesTable prodTaxTable;
	AddressComposite acshippingcomposite;
	Button addShipping,addPacking;
	List<SalesLineItem> prodLis=null;
	static List<SalesLineItem> origProdLis=null;
	public static List<Double> qty=new ArrayList<Double>();
	ListBox lisInvoice;
	TextBox tbremark;
	DateBox dbcompletiondate;
	TextBox tbvehicleName,tbdriverName,tblrNumber,tbtrackingNumber,tbotherinfo1,tbotherinfo2;
	MyLongBox lbdriverPhone;
	ObjectListBox<Config>olbcNumberRange;//Added by Ashwini
	
	public static boolean flag=true;
	ArrayList<ProductQty>compList=new ArrayList<ProductQty>();
	final GenricServiceAsync genasync=GWT.create(GenricService.class);
	FormField fgroupingCustomerInformation;
	DeliveryNote deliverynoteobj;
	TextBox ibRefOrderNo;
	
	/**
	 * @author Anil , Date : 16-04-2019
	 * Added customer branch dropdown and poc name textbox
	 */
	ObjectListBox<CustomerBranchDetails> oblCustomerBranch;
	TextBox tbCustPocName;
	
	/**
	 * @author Anil , Date : 31-07-2019
	 * eway bill number 
	 * raised by sasha, nitin sir, sonu
	 */
	TextBox tbEwayBillNumber;
	
	/**
	 * @author Anil
	 * @since 21-07-2020
	 * Adding delivery status field
	 * for PTSPL raised by Rahul Tiwari
	 */
	TextBox tbDeliveryStatus;
	TextArea taDescrptionTwo;
	public DeliveryNoteForm() {
		super();
		createGui();
		this.tbStatus.setEnabled(false);
		this.tbStatus.setText(DeliveryNote.CREATED);
		chargesTable.connectToLocal();
		prodTaxTable.connectToLocal();
		deliveryLineItem.connectToLocal();
		shippingListTable.connectToLocal();
		packingListTable.connectToLocal();
		tbDeliveryStatus.setEnabled(false);
	}
	
	public DeliveryNoteForm  (String[] processlevel, FormField[][] fields,
			FormStyle formstyle) 
	{
		super(processlevel, fields, formstyle);
		createGui();
	}
	
	@SuppressWarnings("unused")
	private void initalizeWidget()
	{
		personInfoComposite=AppUtility.customerInfoComposite(new Customer());
		personInfoComposite.getCustomerId().getHeaderLabel().setText("* Customer ID");
		personInfoComposite.getCustomerName().getHeaderLabel().setText("* Customer Name");
		personInfoComposite.getCustomerCell().getHeaderLabel().setText("* Customer Cell");
		personInfoComposite.setEnable(false);
		
//		cbcformlis=new ListBox();
//		cbcformlis.addItem("SELECT");
//		cbcformlis.addItem(AppConstants.YES);
//		cbcformlis.addItem(AppConstants.NO);
//		cbcformlis.setEnabled(false);
//		olbcstpercent=new ObjectListBox<TaxDetails>();
//		olbcstpercent.setEnabled(false);
		
//		initializePercentCst();
		tbLeadId=new TextBox();
		tbQuotatinId=new TextBox();
		tbSalesOrderId=new TextBox();
		this.tbSalesOrderId.setEnabled(false);
		this.tbLeadId.setEnabled(false);
		this.tbQuotatinId.setEnabled(false);
		olbeSalesPerson=new ObjectListBox<Employee>();
//		AppUtility.makeSalesPersonListBoxLive(olbeSalesPerson);
		olbeSalesPerson.makeEmployeeLive(AppConstants.SALESMODULE, AppConstants.DELIVERYNOTE, "Sales Person");
		olbeSalesPerson.setEnabled(false);
		ucChooseFile=new UploadComposite();
		tbReferenceNumber=new TextBox();
		dbReferenceDate=new DateBoxWithYearSelector();
		ibDeliveryNoteId=new TextBox();
		ibDeliveryNoteId.setEnabled(false);
		tbStatus=new TextBox();
		tbStatus.setEnabled(false);
		olbbBranch=new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbbBranch);
		olbbBranch.setEnabled(false);
		olbdeliveryCategory=new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(olbdeliveryCategory, Screen.DELIVERYNOTECATEGORY);
		olbdeliveryCategory.addChangeHandler(this);
		olbdeliveryType=new ObjectListBox<Type>();
	    AppUtility.makeTypeListBoxLive(this.olbdeliveryType,Screen.DELIVERYNOTETYPE);
		olbdeliveryGroup=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbdeliveryGroup, Screen.DELIVERYNOTEGROUP);
//		dbdeliverydate=new DateBox();
		dbdeliverydate = new DateBoxWithYearSelector();

		olbShippingMethod=new ObjectListBox<ShippingMethod>();
		ShippingMethod.initializeShippingName(olbShippingMethod);
		olbPackingMethod=new ObjectListBox<PackingMethod>();
		PackingMethod.initializePackingName(olbPackingMethod);
		olbshippingchecklist=new ListBox();
		olbshippingchecklist.setEnabled(false);
		olbshippingchecklist.addItem("SELECT");
//		CheckListType.initializeTypeName(olbshippingchecklist);
		olbpackingchecklist=new ListBox();
		olbpackingchecklist.setEnabled(false);
		olbpackingchecklist.addItem("SELECT");
//		CheckListType.initializeTypeName(olbpackingchecklist);
		deliveryLineItem=new DeliverySalesLineItemTable();
		shippingListTable=new ShippingStepsTable();
		packingListTable=new PackingStepsTable();
		olbApproverName=new ObjectListBox<Employee>();
		AppUtility.makeApproverListBoxLive(olbApproverName, "Delivery Note");
		prodTaxTable=new ProductTaxesTable();
		chargesTable=new ProductChargesTable();
		dbcompletiondate=new DateBoxWithYearSelector();
		dbcompletiondate.setEnabled(false);
		tbvehicleno=new TextBox();
		tbotherinfo1=new TextBox();
		tbotherinfo2=new TextBox();
		
		tbremark=new TextBox();
		tbremark.setEnabled(false);
		
		dototalamt=new DoubleBox();
		dototalamt.setEnabled(false);
		doamtincltax=new DoubleBox();
		doamtincltax.setEnabled(false);
		donetpayamt=new DoubleBox();
		donetpayamt.setEnabled(false);
		this.changeWidgets();
		tadescription=new TextArea();
		acshippingcomposite=new AddressComposite();
		acshippingcomposite.setEnable(false);
		addShipping=new Button("ADD");
		addPacking=new Button("ADD");
		lisInvoice=new ListBox();
		
		tbvehicleName=new TextBox();
		tbdriverName=new TextBox();
		tblrNumber=new TextBox();
		tbtrackingNumber=new TextBox();
		lbdriverPhone=new MyLongBox();
		/***Date 2-2-2019 added by amol**/
		ibRefOrderNo = new TextBox();
		ibRefOrderNo.setEnabled(false);
		/*
		 * Date:23/07/2018
		 * Developer:Ashwini
		 * Des:To add number range coloumn in delivery note
		 */
		olbcNumberRange=new ObjectListBox();
		AppUtility.MakeLiveConfig(olbcNumberRange, Screen.NUMBERRANGE);
		
		/*
		 * End by Ashwini
		 */
		
		oblCustomerBranch=new ObjectListBox<CustomerBranchDetails>();
		oblCustomerBranch.addItem("--SELECT--");
		oblCustomerBranch.addChangeHandler(this);
		tbCustPocName=new TextBox();
		
		tbEwayBillNumber=new TextBox();
		taDescrptionTwo=new TextArea();

		tbDeliveryStatus=new TextBox();
		tbDeliveryStatus.setEnabled(false);
	}

	@Override
	public void createScreen() {


		initalizeWidget();

		//Token to initialize the processlevel menus.
		this.processlevelBarNames=new String[]{"Request For Approval","Cancel Approval Request",AppConstants.CANCLEDELIVERYNOTE,
				AppConstants.NEW,AppConstants.MARKCOMPLETED,ManageApprovals.SUBMIT,"View Sales Order"};
		
		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////
		//Token to initialize formfield
		String mainScreenLabel="Delivery Note";
		if(deliverynoteobj!=null&&deliverynoteobj.getCount()!=0){
			mainScreenLabel=deliverynoteobj.getCount()+"/"+deliverynoteobj.getStatus()+"/"+AppUtility.parseDate(deliverynoteobj.getCreationDate());
		}
			//fgroupingCustomerInformation.getHeaderLabel().setText(mainScreenLabel);
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		fgroupingCustomerInformation=fbuilder.setlabel(mainScreenLabel).widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).setKeyField(true).build();
		//FormFieldBuilder fbuilder;
		//fbuilder = new FormFieldBuilder();
		//FormField fgroupingCustomerInformation=fbuilder.setlabel("Customer Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("",personInfoComposite);
		FormField fpersonInfoComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Lead ID",tbLeadId);
		FormField ftbLeadId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Quotation ID",tbQuotatinId);
		FormField ftbQuotatinId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Sales Order ID",tbSalesOrderId);
		FormField ftbContractId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Reference Number",tbReferenceNumber);
		FormField ftbReferenceNumber= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Reference Date",dbReferenceDate);
		FormField fdbReferenceDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
//		fbuilder = new FormFieldBuilder("C Form",cbcformlis);
//		FormField fcbcform= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
//		fbuilder = new FormFieldBuilder("CST Percent",olbcstpercent);
//		FormField folbcstpercent= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		//Ashwini Patil Date:13-04-2022 Description": made this filed non mandatory as if we don't define sales person in sales order it will show blank on delivery note screen and it does not allow to save. Also it is non editable. 
		fbuilder = new FormFieldBuilder("Saleperson",olbeSalesPerson);
		FormField folbeSalesPerson= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingDocumentInformation=fbuilder.setlabel("Attachment").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Choose File",ucChooseFile);
		FormField fucChooseFile= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingDescInformation=fbuilder.setlabel("Reference/General").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Description 1",tadescription);
		FormField ftadescription= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingDeliveryInformation=fbuilder.setlabel("Classification").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("ID",ibDeliveryNoteId);
		FormField fibDeliveryNoteId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Status",tbStatus);
		FormField ftbStatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Branch",olbbBranch);
		FormField folbbBranch= fbuilder.setMandatory(true).setMandatoryMsg("Branch is Mandatory").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Delivery Category",olbdeliveryCategory);
		FormField folbdeliveryCategory= fbuilder.setMandatory(false).setMandatoryMsg("Delivery Category is Mandatory").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Delivery Type",olbdeliveryType);
		FormField folbdeliveryType= fbuilder.setMandatory(false).setMandatoryMsg("Delivery Type is Mandatory").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Delivery Group",olbdeliveryGroup);
		FormField folbdeliveryGroup= fbuilder.setMandatory(false).setMandatoryMsg("Delivery Group is Mandatory").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Approver Name",olbApproverName);
		FormField folbApproverName= fbuilder.setMandatory(true).setMandatoryMsg("Approver Name is Mandatory").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Delivery Date",dbdeliverydate);
		FormField fdbdeliverydate= fbuilder.setMandatory(true).setMandatoryMsg("Delivery Date is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Remark",tbremark);
		FormField ftbremark= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();	
		fbuilder = new FormFieldBuilder("Completion Date",dbcompletiondate);
		FormField fdbcompletiondate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();	
		fbuilder = new FormFieldBuilder();
		FormField fgroupingChecklistInformation=fbuilder.setlabel("Checklist").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder=new FormFieldBuilder("Shipping Method",olbShippingMethod);
		FormField folbShippingMethod= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();		
		fbuilder=new FormFieldBuilder("Packing Method",olbPackingMethod);
		FormField folbPackingMethod= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();		
		fbuilder=new FormFieldBuilder("Shipping Checklist",olbshippingchecklist);
		FormField folbshippingchecklist= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();	
		fbuilder=new FormFieldBuilder("Packing Checklist",olbpackingchecklist);
		FormField folbpackingchecklist= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();	
		fbuilder=new FormFieldBuilder("",shippingListTable.getTable());
		FormField fshippingListTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		fbuilder=new FormFieldBuilder("",packingListTable.getTable());
		FormField fpackingListTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingProducts=fbuilder.setlabel("Product").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("",deliveryLineItem.getTable());
		FormField fdeliveryLineItem= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder();
		FormField fblankgroup=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		fbuilder = new FormFieldBuilder();
		FormField fblankgroupthree=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		fbuilder = new FormFieldBuilder();
		FormField fblankgroupone=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",prodTaxTable.getTable());
		FormField fprodTaxTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		fbuilder = new FormFieldBuilder("Total Amount",doamtincltax);
		FormField fdoAmtIncludingTax= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Total Amount",dototalamt);
		FormField fpayCompTotalAmt= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",chargesTable.getTable());
		FormField fchargesTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		fbuilder = new FormFieldBuilder("Net Payable",donetpayamt);
		FormField fpayCompNetPay= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",addShipping);
		FormField faddShipping= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",addPacking);
		FormField faddPacking= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Invoice ID",lisInvoice);
		FormField flisInvoice= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingShippingInfo=fbuilder.setlabel("Shipping Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("",acshippingcomposite);
		FormField facshippingcomposite=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingVehicleInfo=fbuilder.setlabel("Tracking Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Vehicle No",tbvehicleno);
		FormField ftbvehicleno= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Vehicle Name",tbvehicleName);
		FormField ftbvehicleName=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Delivery Person Name",tbdriverName);
		FormField ftbdriverName=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Delivery Person Cell No.",lbdriverPhone);
		FormField flbdriverPhone=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("LR Number",tblrNumber);
		FormField ftblrNumber=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Tracking Number",tbtrackingNumber);
		FormField ftbtrackingNumber=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Other Info 1",tbotherinfo1);
		FormField ftbotherinfo1= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Other Info 2",tbotherinfo2);
		FormField ftbotherinfo2= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/*
		 * Date:23/07/2018
		 * Developer:Ashwini
		 */
		fbuilder =new FormFieldBuilder("Number Range",olbcNumberRange);
		FormField folbcNumberRange= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Supplier's Ref./Order No.", ibRefOrderNo);
		FormField fibRefOrderNo = fbuilder.setMandatory(false).setMandatoryMsg("").setRowSpan(0).setColSpan(0).build();
		
		/*
		 * End by Ashwini
		 */
		fbuilder = new FormFieldBuilder("Customer Branch",oblCustomerBranch);
		FormField foblCustomerBranch= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("POC Name",tbCustPocName);
		FormField ftbCustPocName= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Eway Bill Number",tbEwayBillNumber);
		FormField ftbEwayBillNumber= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Delivery Status",tbDeliveryStatus);
		FormField ftbDeliveryStatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
			fbuilder = new FormFieldBuilder("Description 2",taDescrptionTwo);
		FormField ftaDescrptionTwo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////


		FormField[][] formfield = { 
//				{fgroupingCustomerInformation},
//				{fpersonInfoComposite},
//				{ftbContractId,ftbQuotatinId,ftbLeadId,folbeSalesPerson},
//				{ftbReferenceNumber,fdbReferenceDate,fibRefOrderNo,folbcNumberRange},
//				{fgroupingDocumentInformation},{fucChooseFile},
//				{fgroupingDeliveryInformation},
//				{fibDeliveryNoteId,ftbStatus,folbbBranch,folbApproverName},
//				{folbdeliveryGroup,folbdeliveryCategory,folbdeliveryType,flisInvoice},
//				{ftbremark,fdbcompletiondate,ftbEwayBillNumber,ftbDeliveryStatus},
//				{fgroupingVehicleInfo},
//				{ftbvehicleno,ftbvehicleName,ftbdriverName,flbdriverPhone},
//				{ftblrNumber,ftbtrackingNumber,ftbotherinfo1,ftbotherinfo2},
//				{fgroupingDescInformation},
//				{ftadescription},
//				{ftaDescrptionTwo},
//				{fgroupingProducts},
//				{fdeliveryLineItem},
////				{fblankgroupthree,fpayCompTotalAmt},
////				{fblankgroupthree},
////				{fblankgroup,fprodTaxTable},
////				{fblankgroup,fblankgroupone,fdoAmtIncludingTax},
////				{fblankgroup,fchargesTable},
//				{fblankgroup,fblankgroupone,fpayCompNetPay},
//				
//				{fgroupingChecklistInformation},
//				{folbShippingMethod,folbshippingchecklist,faddShipping},
//				{fshippingListTable},
//				{folbPackingMethod,folbpackingchecklist,faddPacking},
//				{fpackingListTable},
//				
//				{fgroupingShippingInfo},{fdbdeliverydate,foblCustomerBranch,ftbCustPocName},
//				{facshippingcomposite}
				
				//Delivery Info//
				{fgroupingCustomerInformation},
				{fpersonInfoComposite},
				{folbbBranch,folbApproverName,fdbdeliverydate,fdbcompletiondate}, //Ashwini Patil Date: 14-04-2022 Mandatory filed moved together in 2nd row and non mandatory in third
				{ftbEwayBillNumber,folbeSalesPerson,ftbremark},
				//Product//
				{fgroupingProducts},
				{fdeliveryLineItem},
//				{fblankgroupthree,fpayCompTotalAmt},
//				{fblankgroupthree},
//				{fblankgroup,fprodTaxTable},
//				{fblankgroup,fblankgroupone,fdoAmtIncludingTax},
//				{fblankgroup,fchargesTable},
				{fblankgroup,fblankgroupone,fpayCompNetPay},
				//Shipping info//
				{fgroupingShippingInfo},
				{foblCustomerBranch,ftbCustPocName},
				{facshippingcomposite},
				//Vehicle Number//
				{fgroupingVehicleInfo},
				{ftbvehicleno,ftbvehicleName,ftbdriverName,flbdriverPhone},
				{ftblrNumber,ftbtrackingNumber},
				//checklist//
				{fgroupingChecklistInformation},
				{folbShippingMethod,folbshippingchecklist,faddShipping},
				{fshippingListTable},
				{folbPackingMethod,folbpackingchecklist,faddPacking},
				{fpackingListTable},
				//Classification//
				{fgroupingDeliveryInformation},
				{folbdeliveryGroup,folbdeliveryCategory,folbdeliveryType},
				//Reference and General
				{fgroupingDescInformation},
				{flisInvoice,ftbQuotatinId,ftbLeadId,ftbotherinfo1,},
				{ftbotherinfo2,ftbReferenceNumber,fdbReferenceDate,fibRefOrderNo,},
				{folbcNumberRange},
				{ftadescription},
				{ftaDescrptionTwo},
				//Attachment
				{fgroupingDocumentInformation},
				{fucChooseFile}
				
				
				
				
				
		};

		this.fields=formfield;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void updateModel(DeliveryNote model) 
	{
		if(personInfoComposite.getValue()!=null)
			model.setCinfo(personInfoComposite.getValue());
		if(!tbLeadId.getValue().equals(""))
			model.setLeadCount(Integer.parseInt(tbLeadId.getValue()));
		if(!tbSalesOrderId.getValue().equals(""))
			model.setSalesOrderCount(Integer.parseInt(tbSalesOrderId.getValue()));
		if(!tbQuotatinId.getValue().equals(""))
			model.setQuotationCount(Integer.parseInt(tbQuotatinId.getValue()));
		if(tbReferenceNumber.getValue()!=null)
			model.setReferenceNumber(tbReferenceNumber.getValue());
		if(dbReferenceDate.getValue()!=null)
			model.setReferenceDate(dbReferenceDate.getValue());
		if(olbeSalesPerson.getValue()!=null)
			model.setEmployee(olbeSalesPerson.getValue());
//		if(cbcformlis.getSelectedIndex()!=0){
//			model.setCformStatus(cbcformlis.getItemText(cbcformlis.getSelectedIndex()));
//		}
//		if(olbcstpercent.getValue()!=null){
//			TaxDetails taxperc=olbcstpercent.getSelectedItem();
//			model.setCstPercent(taxperc.getTaxChargePercent());
//			model.setCstName(olbcstpercent.getValue());
//		}
		if(ucChooseFile.getValue()!=null)
			model.setDeliveryDocument(ucChooseFile.getValue());
		if(tbStatus.getValue()!=null)
			model.setStatus(tbStatus.getValue());
		if(olbbBranch.getValue()!=null)
			model.setBranch(olbbBranch.getValue());
		if(dbcompletiondate.getValue()!=null){
			model.setCompletionDate(dbcompletiondate.getValue());
		}
		if(tbotherinfo1.getValue()!=null){
			model.setOtherInfo1(tbotherinfo1.getValue());
		}
		if(tbotherinfo2.getValue()!=null){
			model.setOtherInfo2(tbotherinfo2.getValue());
		}
		if(olbdeliveryCategory.getValue()!=null)
			model.setCategory(olbdeliveryCategory.getValue());
		if(olbdeliveryType.getValue()!=null)
			model.setType(olbdeliveryType.getValue(olbdeliveryType.getSelectedIndex()));
		if(olbdeliveryGroup.getValue()!=null)
			model.setGroup(olbdeliveryGroup.getValue());
		if(dbdeliverydate.getValue()!=null)
			model.setDeliveryDate(dbdeliverydate.getValue());
		if(olbShippingMethod.getValue()!=null)
			model.setShippingTypeName(olbShippingMethod.getValue());
		if(olbshippingchecklist.getSelectedIndex()!=0)
			model.setShipCheckList(olbshippingchecklist.getItemText(olbshippingchecklist.getSelectedIndex()));
		if(olbPackingMethod.getValue()!=null)
			model.setPackingTypeName(olbPackingMethod.getValue());
		if(olbpackingchecklist.getSelectedIndex()!=0)
			model.setPackCheckList(olbpackingchecklist.getItemText(olbpackingchecklist.getSelectedIndex()));
		
		if(olbApproverName.getValue()!=null)
			model.setApproverName(olbApproverName.getValue());
		if(dototalamt.getValue()!=null)
			model.setTotalAmount(dototalamt.getValue());
		if(donetpayamt.getValue()!=null)
			model.setNetpayable(donetpayamt.getValue());
		if(doamtincltax.getValue()!=null)
			model.setAmountInclTax(doamtincltax.getValue());
		if(tadescription.getValue()!=null)
			model.setDescription(tadescription.getValue());
		if(tbvehicleno.getValue()!=null)
			model.setVehicleNo(tbvehicleno.getValue());
		if(tbvehicleName.getValue()!=null)
			model.setVehicleName(tbvehicleName.getValue());
		if(tbdriverName.getValue()!=null)
			model.setDriverName(tbdriverName.getValue());
		if(lbdriverPhone.getValue()!=null)
			model.setDriverPhone(lbdriverPhone.getValue());
		if(tblrNumber.getValue()!=null)
			model.setLrNumber(tblrNumber.getValue());
		if(tbtrackingNumber.getValue()!=null)
			model.setTrackingNumber(tbtrackingNumber.getValue());
		
		if(lisInvoice.getSelectedIndex()!=0){
			int invIndex=lisInvoice.getSelectedIndex();
			model.setInvoiceId(Integer.parseInt(lisInvoice.getItemText(invIndex)));
		}
			
		
		
		List<DeliveryLineItems> saleslis=this.getDeliveryLineItem().getDataprovider().getList();
		ArrayList<DeliveryLineItems> arrItems=new ArrayList<DeliveryLineItems>();
		arrItems.addAll(saleslis);
		model.setDeliveryItems(arrItems);
		
		List<ProductOtherCharges> productTaxLis=this.prodTaxTable.getDataprovider().getList();
		ArrayList<ProductOtherCharges> productTaxArr=new ArrayList<ProductOtherCharges>();
		productTaxArr.addAll(productTaxLis);
		model.setProdTaxesLis(productTaxArr);
		
		List<ProductOtherCharges> productChargeLis=this.chargesTable.getDataprovider().getList();
		ArrayList<ProductOtherCharges> productChargeArr=new ArrayList<ProductOtherCharges>();
		productChargeArr.addAll(productChargeLis);
		model.setProdChargeLis(productChargeArr);
		
		List<ShippingSteps> shipStepsLis=this.shippingListTable.getDataprovider().getList();
		ArrayList<ShippingSteps> shipStepsArr=new ArrayList<ShippingSteps>();
		shipStepsArr.addAll(shipStepsLis);
		model.setShippingStepsLis(shipStepsArr);
		
		List<ShippingSteps> packStepsLis=this.packingListTable.getDataprovider().getList();
		ArrayList<ShippingSteps> packStepsArr=new ArrayList<ShippingSteps>();
		packStepsArr.addAll(packStepsLis);
		model.setPackingStepsLis(packStepsArr);
		
		if(dbdeliverydate.getValue()!=null)
			model.setDeliveryDate(dbdeliverydate.getValue());
		if(acshippingcomposite.getValue()!=null)
			model.setShippingAddress(acshippingcomposite.getValue());
		
		/*
		 * Date:23/07/2018
		 * Developer:Ashwini	
		 */
		
		if(olbcNumberRange.getValue()!=null){
			 model.setNumberRange(olbcNumberRange.getValue(olbcNumberRange.getSelectedIndex()));
		}
		
		/*
		 * End by Ashwini
		 */
		/***2-2-2019 added by amol***/
		if (ibRefOrderNo.getValue() != null)
			model.setRefOrderNO(ibRefOrderNo.getValue());
		
		if(oblCustomerBranch.getValue()!=null){
			model.setCustBranch(oblCustomerBranch.getValue());
		}else{
			model.setCustBranch("");
		}
		if(tbCustPocName.getValue()!=null){
			model.setCustPocName(tbCustPocName.getValue());
		}
		if(tbEwayBillNumber.getValue()!=null){
			model.setEwayBillNumber(tbEwayBillNumber.getValue());
		}
		
		if(tbDeliveryStatus.getValue()!=null){
			model.setDeliveryStatus(tbDeliveryStatus.getValue());
		}
       if(taDescrptionTwo.getValue()!=null){
			model.setDesriptionTwo(taDescrptionTwo.getValue());
		}
			/**Date 28-4-2019
		 * Commented this method by Amol raised this issue by Sonu 
		 * due to this method multiple delivery notes are created.
		 */
//		callOnSave1();
		deliverynoteobj=model;
		presenter.setModel(model);

	}

		@SuppressWarnings("unchecked")
		@Override
		public void updateView(DeliveryNote view) 
		{
			deliverynoteobj=view;
	        ibDeliveryNoteId.setValue(view.getCount()+"");
	        
	        /**
			 * Date 20-04-2018 By vijay for orion pest locality load  
			 */
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Locality", "DontLoadAllLocalityAndCity")){
				setAddressDropDown();
			}
			/**
			 * ends here
			 */
			
	        this.checkCustomerStatus(view.getCinfo().getCount());
	        
	        if(view.getInvoiceId()!=0)
	        {
	        	addInvoices(view.getSalesOrderCount(),view.getInvoiceId());
	        }
	        if(view.getInvoiceId()==0)
	        {
	        	addInvoices(view.getSalesOrderCount(),-1);
	        }
	        
	        if(view.getCinfo()!=null)
				personInfoComposite.setValue(view.getCinfo());
			if(view.getLeadCount()!=-1)
				tbLeadId.setValue(view.getLeadCount()+"");
			if(view.getQuotationCount()!=-1)
				tbQuotatinId.setValue(view.getQuotationCount()+"");
			if(view.getSalesOrderCount()!=null)
				tbSalesOrderId.setValue(view.getSalesOrderCount()+"");
			if(view.getReferenceNumber()!=null)
				tbReferenceNumber.setValue(view.getReferenceNumber());
			if(view.getReferenceDate()!=null)
				dbReferenceDate.setValue(view.getReferenceDate());
	        if(view.getVehicleNo()!=null)
	        	tbvehicleno.setValue(view.getVehicleNo());
	        
	        if(view.getSalesOrderCount()!=0)
	        {
	        	System.out.println("Retrieving Data");
	        	DeliveryNotePresenter.checkOrderQuantity(view.getSalesOrderCount(),view.getCount());
	        }
	        
	        if(view.getRemark()!=null){
	        	tbremark.setValue(view.getRemark());
	        }
	        if(view.getCompletionDate()!=null){
	        	dbcompletiondate.setValue(view.getCompletionDate());
	        }
	        
			
//			if(view.getCformStatus()!=null)
//			{
//				for(int i=0;i<cbcformlis.getItemCount();i++)
//				{
//					String data=cbcformlis.getItemText(i);
//					if(data.equals(view.getCformStatus()))
//					{
//						cbcformlis.setSelectedIndex(i);
//						break;
//					}
//				}
//			}
//			olbcstpercent.setValue(view.getCstName());
			
			if(view.getEmployee()!=null)
				olbeSalesPerson.setValue(view.getEmployee());
			if(view.getDeliveryDocument()!=null)
				ucChooseFile.setValue(view.getDeliveryDocument());
			if(view.getStatus()!=null)
				tbStatus.setValue(view.getStatus());
			if(view.getBranch()!=null)
				olbbBranch.setValue(view.getBranch());
			if(view.getCategory()!=null)
				olbdeliveryCategory.setValue(view.getCategory());
			if(view.getType()!=null)
				olbdeliveryType.setValue(view.getType());
			if(view.getGroup()!=null)
				olbdeliveryGroup.setValue(view.getGroup());
			if(view.getDeliveryDate()!=null)
				dbdeliverydate.setValue(view.getDeliveryDate());
			if(view.getShippingTypeName()!=null)
				olbShippingMethod.setValue(view.getShippingTypeName());
			
//			if(view.getShipCheckList()!=null)
//				olbshippingchecklist.setValue(view.getShipCheckList());
			if(view.getPackingTypeName()!=null)
				olbPackingMethod.setValue(view.getPackingTypeName());
			if(view.getApproverName()!=null)
				olbApproverName.setValue(view.getApproverName());
			if(view.getDescription()!=null)
				tadescription.setValue(view.getDescription());
			
			if(view.getDriverName()!=null)
				tbdriverName.setValue(view.getDriverName());
			if(view.getDriverPhone()!=0)
				lbdriverPhone.setValue(view.getDriverPhone());
			if(view.getVehicleName()!=null)
				tbvehicleName.setValue(view.getVehicleName());
			if(view.getLrNumber()!=null)
				tblrNumber.setValue(view.getLrNumber());
			if(view.getTrackingNumber()!=null)
				tbtrackingNumber.setValue(view.getTrackingNumber());
			if(view.getOtherInfo1()!=null)
				tbotherinfo1.setValue(view.getOtherInfo1());
			if(view.getOtherInfo2()!=null)
				tbotherinfo2.setValue(view.getOtherInfo2());
			
			/****2-2-2019 added by amol***/
			ibRefOrderNo.setValue(view.getRefOrderNO());
			
			/*
			 * Date:23/07/2018
			 * Developer:Ashwini
			 */
			if(view.getNumberRange()!=null)
				olbcNumberRange.setValue(view.getNumberRange());
			/*
			 * End by Ashwini
			 */
			
			if(view.getDeliveryDate()!=null)
				dbdeliverydate.setValue(view.getDeliveryDate());
			if(view.getShippingAddress()!=null)
				acshippingcomposite.setValue(view.getShippingAddress());
			shippingListTable.setValue(view.getShippingStepsLis());
			packingListTable.setValue(view.getPackingStepsLis());
			deliveryLineItem.setValue(view.getDeliveryItems());
			prodTaxTable.setValue(view.getProdTaxesLis());
			chargesTable.setValue(view.getProdChargeLis());
			dototalamt.setValue(view.getTotalAmount());
			doamtincltax.setValue(view.getAmountInclTax());
			donetpayamt.setValue(view.getNetpayable());
		
			loadCustomerBranch(view.getCustBranch(),view.getCinfo().getCount());
			if(view.getCustPocName()!=null){
				tbCustPocName.setValue(view.getCustPocName());
			}
			
			if(view.getEwayBillNumber()!=null){
				tbEwayBillNumber.setValue(view.getEwayBillNumber());
			}
			
			if(view.getDeliveryStatus()!=null){
				tbDeliveryStatus.setValue(view.getDeliveryStatus());
			}
			if(view.getDesriptionTwo()!=null){
				taDescrptionTwo.setValue(view.getDesriptionTwo());
			}
			
			/* 
			 * for approval process
			 *  nidhi
			 *  5-07-2017
			 */
			if(presenter != null){
				presenter.setModel(view);
			}
			/*
			 *  end
			 */
		}
		
		/**
		 * @author Anil , Date : 16-04-2019
		 * @param custBranch
		 * @param custId
		 * this method is used to load customer branches and set value if viewing the doc
		 */
		public void loadCustomerBranch(final String custBranch,int custId) {
			// TODO Auto-generated method stub
			GenricServiceAsync async=GWT.create(GenricService.class);
			MyQuerry querry=new MyQuerry();
			Filter temp=null;
			Vector<Filter> vecList=new Vector<Filter>();
			
			temp=new Filter();
			temp.setQuerryString("cinfo.count");
			temp.setIntValue(custId);
			vecList.add(temp);
			
			temp=new Filter();
			temp.setQuerryString("status");
			temp.setBooleanvalue(true);
			vecList.add(temp);
			
			querry.setFilters(vecList);
			querry.setQuerryObject(new CustomerBranchDetails());
			async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {

				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
				}

				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					// TODO Auto-generated method stub
					oblCustomerBranch.clear();
					oblCustomerBranch.addItem("--SELECT--");
//					tbCustPocName.setValue("");
					if(result!=null&&result.size()!=0){
						ArrayList<CustomerBranchDetails>custBranchList=new ArrayList<CustomerBranchDetails>();
						for(SuperModel model:result){
							CustomerBranchDetails obj=(CustomerBranchDetails) model;
							custBranchList.add(obj);
						}
						
						oblCustomerBranch.setListItems(custBranchList);
						if(custBranch!=null){
							oblCustomerBranch.setValue(custBranch);
						}
					}
				}
			});
			
		}

		 /**
		 * Date 20-04-2018 By vijay
		 * here i am refreshing address composite data if global list size greater than drop down list size
		 * because we are adding locality and city from an entity in view state to global list if doest not exist in global list 
		 */
		private void setAddressDropDown() {
			if(LoginPresenter.globalLocality.size()>acshippingcomposite.locality.getItems().size()){
				acshippingcomposite.locality.setListItems(LoginPresenter.globalLocality);
			}
		}
		/**
		 * ends here 
		 */
		
		/**
		 * Toggles the app header bar menus as per screen state
		 */

		public void toggleAppHeaderBarMenu() {
			if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.contains("Save")||text.contains("Discard")||text.contains("Search")||text.contains(AppConstants.NAVIGATION))
					{
						menus[k].setVisible(true); 
					}
					else
						menus[k].setVisible(false);  		   
				}
			}

			else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
			{
//				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				
				/**
				 * Date 30 july 2017 if condtion added by vijay for GENERAL POPUP menus 
				 * and old code added to else block
				 */
				
				InlineLabel[] menus;
				if(isPopUpAppMenubar()){
					 menus=GeneralViewDocumentPopup.applevelMenu;
				}else{
					 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();

				}
				
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.contains("Save")||text.contains("Discard")||text.contains(AppConstants.NAVIGATION))
						menus[k].setVisible(true); 
					
					else
						menus[k].setVisible(false);  		   
				}
			}

			else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
			{
//				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				
				/**
				 * Date 30 july 2017 if condtion added by vijay for GENERAL POPUP menus 
				 * and old code added to else block
				 */
				
				InlineLabel[] menus;
				if(isPopUpAppMenubar()){
					 menus=GeneralViewDocumentPopup.applevelMenu;
				}else{
					 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();

				}
				
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.contains("Edit")||text.contains("Discard")||text.contains("Search")||text.contains("Print")||text.contains(AppConstants.NAVIGATION))
							menus[k].setVisible(true); 
					
					else
						menus[k].setVisible(false);  		   
				}
			}
			AuthorizationHelper.setAsPerAuthorization(Screen.DELIVERYNOTE,LoginPresenter.currentModule.trim());
		}

		public void toggleProcessLevelMenu()
		{
			DeliveryNote entity=(DeliveryNote) presenter.getModel();
			
			String status=entity.getStatus();

			for(int i=0;i<getProcesslevelBarNames().length;i++)
			{
				InlineLabel label=getProcessLevelBar().btnLabels[i];
				String text=label.getText().trim();

				if(status.equals(DeliveryNote.CREATED))
				{
					if(getManageapproval().isSelfApproval())
					{  
						if(text.equals(ManageApprovals.APPROVALREQUEST))
							label.setVisible(false);
						if(text.equals(ManageApprovals.SUBMIT))
							label.setVisible(true);
					}
					else
					{  
						if(text.equals(ManageApprovals.APPROVALREQUEST))
							label.setVisible(true);
						if(text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
							label.setVisible(false);
						if(text.equals(ManageApprovals.SUBMIT))
							label.setVisible(false);
					}
						/**
						 * Updated By: Viraj
						 * Date: 01-07-2019
						 * Description: Commented since request for approval is visible even if self approve process config is on
						 */
//						if(text.contains(ManageApprovals.APPROVALREQUEST))
//							label.setVisible(false);
						/** Ends **/
						if(text.equals(AppConstants.CANCEL))
							label.setVisible(false);
						if(text.equals(AppConstants.MARKCOMPLETED))
							label.setVisible(false);
						if(text.equals(AppConstants.CANCLEDELIVERYNOTE))
							label.setVisible(false);
						
						/**
						 * Updated By: Viraj
						 * Date: 01-07-2019
						 * Description: Commented since request for approval is visible even if self approve process config is on
						 */
//						if(text.contains(ManageApprovals.APPROVALREQUEST))
//							label.setVisible(true);
						/** Ends **/
						if(text.equals(AppConstants.CANCEL))
							label.setVisible(false);
						if(text.equals(AppConstants.MARKCOMPLETED))
							label.setVisible(false);
						if(text.equals(AppConstants.CANCLEDELIVERYNOTE))
							label.setVisible(false);
				
				}
				if(status.equals(DeliveryNote.APPROVED))
				{
					if(text.equals(AppConstants.Approve))
						label.setVisible(false);	
					if(text.equals(AppConstants.MARKCOMPLETED))
						label.setVisible(true);
//					if(text.equals(AppConstants.CANCLEDELIVERYNOTE))
//						label.setVisible(true);

					/**
					 * rohan added this code for NBHC Changes ADMIN role should  
					 */
					if(LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN")){
						if((text.contains(AppConstants.CANCLESALESORDER)))
							label.setVisible(true);
					}
					else
					{
						if((text.contains(AppConstants.CANCLESALESORDER)))
							label.setVisible(false);
					}
					
					if(text.equals(ManageApprovals.SUBMIT))
						label.setVisible(false);
				
				}

				if(status.equals(DeliveryNote.DELIVERYNOTECANCEL))
				{

					if((text.contains(AppConstants.NEW)))
						label.setVisible(false);
					else
						label.setVisible(false);
					if(text.equals(AppConstants.CANCLEDELIVERYNOTE))
						label.setVisible(false);
					if(text.equals(ManageApprovals.SUBMIT))
						label.setVisible(false);
				
				}
				
				if(status.equals(DeliveryNote.REQUESTED))
				{
					if(text.contains(ManageApprovals.APPROVALREQUEST))
						label.setVisible(false);
					if(text.equals(AppConstants.MARKCOMPLETED))
						label.setVisible(false);
					if(text.equals(AppConstants.CANCLEDELIVERYNOTE))
						label.setVisible(false);
					if(text.equals(ManageApprovals.SUBMIT))
						label.setVisible(false);
				
				}
				
				if(status.equals(DeliveryNote.REJECTED))
				{
					if(text.equals(AppConstants.CANCEL))
						label.setVisible(false);
					if(text.equals(AppConstants.MARKCOMPLETED))
						label.setVisible(false);
					if(text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
						label.setVisible(false);
					if(text.contains(AppConstants.NEW))
						label.setVisible(true);
					if(text.equals(AppConstants.CANCLEDELIVERYNOTE))
						label.setVisible(false);
					if(text.equals(ManageApprovals.SUBMIT))
						label.setVisible(false);
				
				}
				if(status.equals(DeliveryNote.DELIVERYCOMPLETED))
				{
					if(text.contains(AppConstants.NEW))
						label.setVisible(true);
					if(text.contains(ManageApprovals.APPROVALREQUEST))
						label.setVisible(false);
					if(text.equals(AppConstants.CANCLEDELIVERYNOTE))
						label.setVisible(false);
					if(text.equals(AppConstants.MARKCOMPLETED))
						label.setVisible(false);
					if(text.contains(ManageApprovals.CANCELAPPROVALREQUEST))
						label.setVisible(false);
					if(text.equals(ManageApprovals.SUBMIT))
						label.setVisible(false);
				
				}
				
				if(text.equals("View Sales Order"))
					label.setVisible(true);
				
				
				
				//Ashwini Patil Date:20-09-2022
				if(isHideNavigationButtons()) {
					Console.log("in changeProcessLevel isHideNavigationButtons");
					if(text.equalsIgnoreCase(AppConstants.NEW)||text.equalsIgnoreCase("View Sales Order"))
						label.setVisible(false);	
				}
			}
		}
		/**
		 * Sets the Application Header Bar as Per Status
		 */

		public void setAppHeaderBarAsPerStatus()
		{
			DeliveryNote entity=(DeliveryNote) presenter.getModel();
			String status=entity.getStatus();
			
			if(status.equals(DeliveryNote.DELIVERYNOTECANCEL)||status.equals(DeliveryNote.REJECTED)||status.equals(DeliveryNote.APPROVED))
			{
//				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				
				/**
				 * Date 30 july 2017 if condtion added by vijay for GENERAL POPUP menus 
				 * and old code added to else block
				 */
				
				InlineLabel[] menus;
				if(isPopUpAppMenubar()){
					 menus=GeneralViewDocumentPopup.applevelMenu;
				}else{
					 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();

				}
				
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.contains("Discard")||text.contains("Search")
							||text.contains("Print")||text.contains(AppConstants.NAVIGATION))
					{
						menus[k].setVisible(true); 
					}
					else
					{
						menus[k].setVisible(false);  
					}
				}
			}

			if(status.equals(DeliveryNote.APPROVED))
			{
//				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				
				/**
				 * Date 30 july 2017 if condtion added by vijay for GENERAL POPUP menus 
				 * and old code added to else block
				 */
				
				InlineLabel[] menus;
				if(isPopUpAppMenubar()){
					 menus=GeneralViewDocumentPopup.applevelMenu;
				}else{
					 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();

				}
				
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					
					/**
					 * Date 21-5-2018 By Jayshree
					 * Des.add the to edit the delivery note status and user role is admin
					 */
						if(UserConfiguration.getRole().getRoleName().equals("ADMIN")){
								if(text.contains("Discard")||text.contains("Search")||text.contains("Email")
									||text.contains("Print")||text.contains(AppConstants.NAVIGATION)||text.contains("Edit")){
									menus[k].setVisible(true); 
									}
									else
									{
									menus[k].setVisible(false);  
									}
								
							
						}
						else{
							if(text.contains("Discard")||text.contains("Search")||text.contains("Email")
								||text.contains("Print")||text.contains(AppConstants.NAVIGATION)){
								menus[k].setVisible(true); 
								}
								else
								{
								menus[k].setVisible(false);  
								}
							}
				}
				
				/**
				 * Date 21-5-2018 By Jayshree
				 * Des.add the to edit the delivery note status and user role is admin
//				 */
				if(UserConfiguration.getRole().getRoleName().equals("ADMIN")){
					if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT){
						for(int k=0;k<menus.length;k++){
							String text=menus[k].getText();
							if(text.contains("Discard")||text.contains("Save")){
								menus[k].setVisible(true); 
							}else{
								menus[k].setVisible(false);
							}
						}
					}
				}
				
			}
			
//			if(status.equals(DeliveryNote.CREATED))
//			{
//				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
//				for(int k=0;k<menus.length;k++)
//				{
//					String text=menus[k].getText();
//					if(text.contains("Discard")||text.contains("Search")||text.contains("Print"))
//					{
//						menus[k].setVisible(true); 
//
//					}
//					else
//					{
//						menus[k].setVisible(false);  
//					}
//				}
//			}
			
			if(status.equals(DeliveryNote.REQUESTED))
			{
//				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				
				/**
				 * Date 30 july 2017 if condtion added by vijay for GENERAL POPUP menus 
				 * and old code added to else block
				 */
				
				InlineLabel[] menus;
				if(isPopUpAppMenubar()){
					 menus=GeneralViewDocumentPopup.applevelMenu;
				}else{
					 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();

				}
				
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.contains("Discard")||text.contains("Search")||text.contains("Print"))
					{
						menus[k].setVisible(true); 

					}
					else
					{
						menus[k].setVisible(false);  
					}
				}
			}
			
			if(status.equals(DeliveryNote.REJECTED))
			{
//				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				
				/**
				 * Date 30 july 2017 if condtion added by vijay for GENERAL POPUP menus 
				 * and old code added to else block
				 */
				
				InlineLabel[] menus;
				if(isPopUpAppMenubar()){
					 menus=GeneralViewDocumentPopup.applevelMenu;
				}else{
					 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();

				}
				
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.contains("Discard")||text.contains("Search")||text.contains("Print")||text.contains("Edit"))
					{
						menus[k].setVisible(true); 

					}
					else
					{
						menus[k].setVisible(false);  
					}
				}
			}
			
			if(status.equals(DeliveryNote.DELIVERYCOMPLETED))
			{
//				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				
				/**
				 * Date 30 july 2017 if condtion added by vijay for GENERAL POPUP menus 
				 * and old code added to else block
				 */
				
				InlineLabel[] menus;
				if(isPopUpAppMenubar()){
					 menus=GeneralViewDocumentPopup.applevelMenu;
				}else{
					 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				}
				
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.contains("Discard")||text.contains("Search")||text.contains("Print"))
					{
						menus[k].setVisible(true); 
					}
					else
					{
						menus[k].setVisible(false);  
					}
				}
			}
			

			/*else if(status.equals(Contract.CREATED))
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.contains("Discard")||text.contains("Search")
							||text.contains("Print")||text.contains("Save")||text.contains("Edit"))
					{
						menus[k].setVisible(true); 

					}
					else
					{
						menus[k].setVisible(false);  
					}
				}
			}*/
		}
		
		
		/**
		 * The method is responsible for changing Application state as per 
		 * status
		 */
		public void setMenuAsPerStatus()
		{  
//			this.changeProcessLevel();             //Ajinkya added this method on Datye : 01/08/2017
			this.setAppHeaderBarAsPerStatus();
			this.toggleProcessLevelMenu();
		} 
		
		
		  //////////////////// Ajinkya added this for self Approval      ////////////////////////
		
//		public void setMenus()
//		{
//			this.changeProcessLevel();
//			this.setAppHeaderBarAsPerStatus();
//		} 
		
		public void changeProcessLevel()
		{  
			
			DeliveryNote entity=(DeliveryNote) presenter.getModel();
			String status=entity.getStatus();
		
//			boolean renewFlagStatus=entity.isRenewFlag();
			
			for(int i=0;i<getProcesslevelBarNames().length;i++)
			{
				InlineLabel label=getProcessLevelBar().btnLabels[i];
				String text=label.getText().trim();
				if(status.equals(DeliveryNote.CREATED))
				{
					System.out.println("inside Created DeliveryNote ");
					/**
					 * Date : 23-05-2017 By ANIL
					 * Checking self approval           updated here by Ajinkya for contract 
					 */
					if(getManageapproval().isSelfApproval())
					{  
						System.out.println("inside getManageapproval method ");
						
						if(text.equals(ManageApprovals.APPROVALREQUEST))
							label.setVisible(false);
						if(text.equals(ManageApprovals.SUBMIT))
							label.setVisible(true);
					}
					else
					{  
						System.out.println("inside else getManageapproval method ");
						if(text.equals(ManageApprovals.APPROVALREQUEST))
							label.setVisible(true);
						if(text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
							label.setVisible(false);
						if(text.equals(ManageApprovals.SUBMIT))
							label.setVisible(false);
					}
					
					
				}
				if(status.equals(DeliveryNote.REQUESTED))
				{  
					System.out.println("inside  DeliveryNote REQUESTED ");
					if(text.equals(ManageApprovals.APPROVALREQUEST))
						label.setVisible(false);
					if(text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
						label.setVisible(true);
					if(text.equals(ManageApprovals.SUBMIT))
						label.setVisible(false);
				}
				
				if(status.equals(DeliveryNote.REJECTED))
				{
					System.out.println("inside  DeliveryNote REJECTED ");
					if(text.equals(ManageApprovals.APPROVALREQUEST))
						label.setVisible(true);
					if(text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
						label.setVisible(false);
					if(text.equals(ManageApprovals.SUBMIT))
						label.setVisible(false);
				}
				
				if(status.equals(DeliveryNote.APPROVED))
				{
					System.out.println("inside  DeliveryNote APPROVED ");
					if(text.equals(ManageApprovals.APPROVALREQUEST))
						label.setVisible(false);
					if(text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
						label.setVisible(false);
					if(text.equals(ManageApprovals.SUBMIT))
						label.setVisible(false);
				}
				
				
				if(status.equals(DeliveryNote.CANCELLED))
				{  
					System.out.println("inside  DeliveryNote CANCELLED ");
					
					if(text.equals(ManageApprovals.APPROVALREQUEST))
						label.setVisible(false);
					if(text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
						label.setVisible(false);
					if(text.equals(ManageApprovals.SUBMIT))
						label.setVisible(false);
				}
			}
			
		} 
		
		                  //////////////////////////// End Here //////////////////////////
		

		

		
		/**
		 * sets the id textbox with the passed count value. 
		 */
		@Override
		public void setCount(int count)
		{
			ibDeliveryNoteId.setValue(count+"");
		}
		
		/****************************************On Click Mehtod*******************************************/
		/**
		 * This method is called on click of any widget.
		 * 
		 * 	Involves Payment Terms Button  (ADD)
		 *  Involves Add Charges Button (Plus symbol)
		 * 	Involves OtmComposite i.e Products which are added
		 */
		
		
		
		@Override
		public void onClick(ClickEvent event) 
		{
		}

		
		@Override
		public TextBox getstatustextbox() {
			
			return this.tbStatus;
		}

		
		/*****************************************Add Product Taxes Method***********************************/
		
		/**
		 * This method is called when a product is added to SalesLineItem table.
		 * Taxes related to corresponding products will be added in ProductTaxesTable.
		 * @throws Exception
		 */
		public void addProdTaxes() throws Exception
		{
			System.out.println("Add Prod Taxes Method");
			List<DeliveryLineItems> salesLineItemLis=this.deliveryLineItem.getDataprovider().getList();
			List<ProductOtherCharges> taxList=this.prodTaxTable.getDataprovider().getList();
			NumberFormat nf=NumberFormat.getFormat("0.00");
			String cformval="";
//			int cformindex=this.getCbcformlis().getSelectedIndex();
//			if(cformindex!=0){
//				cformval=this.getCbcformlis().getItemText(cformindex);
//			}
			for(int i=0;i<salesLineItemLis.size();i++)
			{
				double priceqty=0;
				if(salesLineItemLis.get(i).getProdPercDiscount()==null){
					priceqty=salesLineItemLis.get(i).getPrice()*salesLineItemLis.get(i).getQuantity();
					priceqty=Double.parseDouble(nf.format(priceqty));
				}
				if(salesLineItemLis.get(i).getProdPercDiscount()!=null){
					priceqty=salesLineItemLis.get(i).getPrice()*salesLineItemLis.get(i).getProdPercDiscount()/100;
					priceqty=(salesLineItemLis.get(i).getPrice()-priceqty)*salesLineItemLis.get(i).getQuantity();
					priceqty=Double.parseDouble(nf.format(priceqty));
				}
				System.out.println("Total Prodcut"+priceqty);
				
				
//				if(salesLineItemLis.get(i).getVatTax().getPercentage()==0&&cbcformlis.getSelectedIndex()==0){
//					System.out.println("For vat Txas 0");
//					ProductOtherCharges pocentity=new ProductOtherCharges();
//					if(validateState()){
//						pocentity.setChargeName("VAT");
//					}
//					if(!validateState()){
//						pocentity.setChargeName("CST");
//					}
//					pocentity.setChargePercent(salesLineItemLis.get(i).getVatTax().getPercentage());
//					pocentity.setIndexCheck(i+1);
//					int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getVatTax().getPercentage(),"VAT");
//					if(indexValue!=-1){
////						pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
//						pocentity.setAssessableAmount(0);
//						this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
//					}
//					
//					if(indexValue==-1){
//						pocentity.setAssessableAmount(0);
////						pocentity.setAssessableAmount(priceqty);
//					}
//					this.prodTaxTable.getDataprovider().getList().add(pocentity);
//				}
				
				if(salesLineItemLis.get(i).getServiceTax().getPercentage()==0){
//					if(validateState()){
					ProductOtherCharges pocentity=new ProductOtherCharges();
					pocentity.setChargeName("Service Tax");
					pocentity.setChargePercent(salesLineItemLis.get(i).getServiceTax().getPercentage());
					pocentity.setIndexCheck(i+1);
					int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getServiceTax().getPercentage(),"Service");
					if(indexValue!=-1){
//						pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
						pocentity.setAssessableAmount(0);
						this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
					}
					
					if(indexValue==-1){
						pocentity.setAssessableAmount(0);
//						pocentity.setAssessableAmount(priceqty);
					}
					this.prodTaxTable.getDataprovider().getList().add(pocentity);
//					}
					
				}
			/**
			 * Adding Vat Tax to table if the company state and delivery state is equal
			 */
				
			if(validateState()){
				if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
					System.out.println("Vattx");
					ProductOtherCharges pocentity=new ProductOtherCharges();
					pocentity.setChargeName("VAT");
					pocentity.setChargePercent(salesLineItemLis.get(i).getVatTax().getPercentage());
					pocentity.setIndexCheck(i+1);
					int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getVatTax().getPercentage(),"VAT");
					
					if(indexValue!=-1){
						System.out.println("Index As Not -1");
						pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
						this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
					}
					if(indexValue==-1){
						System.out.println("VAt");
						pocentity.setAssessableAmount(priceqty);
					}
					this.prodTaxTable.getDataprovider().getList().add(pocentity);
				}
			}
				
			/**
			 * Adding Vat Tax as CST if company state and delivery state is not equal and C form is submitted.
			 */
//				if(this.getCbcformlis().getSelectedIndex()!=0&&AppConstants.YES.equals(cformval.trim())){
//					if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
//						System.out.println("Vattx");
//						ProductOtherCharges pocentity=new ProductOtherCharges();
//						pocentity.setChargeName("CST");
//						TaxDetails cstper1=olbcstpercent.getSelectedItem();
//						pocentity.setChargePercent(cstper1.getTaxChargePercent());
//						pocentity.setIndexCheck(i+1);
//						int indexValue=this.checkTaxPercent(cstper1.getTaxChargePercent(),"CST");
//						
//						if(indexValue!=-1){
//							System.out.println("Index As Not -1");
//							pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
//							this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
//						}
//						if(indexValue==-1){
//							pocentity.setAssessableAmount(priceqty);
//						}
//						this.prodTaxTable.getDataprovider().getList().add(pocentity);
//					}
//				}
				
				
				/**
				 * Adding Vat Tax as CST if company state and delivery state is not equal and C form is not submitted.
				 */
				
//				if(this.getCbcformlis().getSelectedIndex()!=0&&AppConstants.NO.equals(cformval.trim())){
//					if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
//						System.out.println("Vattx");
//						ProductOtherCharges pocentity=new ProductOtherCharges();
//						pocentity.setChargeName("CST");
//						pocentity.setChargePercent(salesLineItemLis.get(i).getVatTax().getPercentage());
//						pocentity.setIndexCheck(i+1);
//						int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getVatTax().getPercentage(),"VAT");
//						
//						if(indexValue!=-1){
//							System.out.println("vsctIndex As Not -1");
//							pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
//							this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
//						}
//						if(indexValue==-1){
//							System.out.println("vcst-1");
//							pocentity.setAssessableAmount(priceqty);
//						}
//						this.prodTaxTable.getDataprovider().getList().add(pocentity);
//					}
//				}
				
				/**
				 * Adding service tax to the table
				 */
				
				if(salesLineItemLis.get(i).getServiceTax().getPercentage()!=0){
					System.out.println("ServiceTx");
					ProductOtherCharges pocentity=new ProductOtherCharges();
					pocentity.setChargeName("Service Tax");
					pocentity.setChargePercent(salesLineItemLis.get(i).getServiceTax().getPercentage());
					pocentity.setIndexCheck(i+1);
					int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getServiceTax().getPercentage(),"Service");
					if(indexValue!=-1){
						System.out.println("Index As Not -1");
						
						/**
						 * If validate state method is true
						 */
						
						if(validateState()){
							double assessValue=0;
							if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
								assessValue=priceqty+(priceqty*salesLineItemLis.get(i).getVatTax().getPercentage()/100);
								pocentity.setAssessableAmount(assessValue+taxList.get(indexValue).getAssessableAmount());
								this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
							}
							if(salesLineItemLis.get(i).getVatTax().getPercentage()==0){
								pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
								this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
							}
						}
						
						/**
						 * If validate state method is true and cform is submittted.
						 */
						
//						if(!validateState()&&this.getCbcformlis().getItemText(getCbcformlis().getSelectedIndex()).equals(AppConstants.YES)){
//							double assessValue=0;
//							if(this.getOlbcstpercent().getSelectedIndex()!=0){
//								TaxDetails cstper2=olbcstpercent.getSelectedItem();
//								assessValue=priceqty+(priceqty*cstper2.getTaxChargePercent()/100);
//								pocentity.setAssessableAmount(assessValue+taxList.get(indexValue).getAssessableAmount());
//								this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
//							}
//						}
						
						/**
						 * If validate state method is true and cform is not submitted.
						 */
						
//						if(!validateState()&&this.getCbcformlis().getItemText(getCbcformlis().getSelectedIndex()).equals(AppConstants.NO)){
//							double assessValue=0;
//							assessValue=priceqty+(priceqty*salesLineItemLis.get(i).getVatTax().getPercentage()/100);
//							pocentity.setAssessableAmount(assessValue+taxList.get(indexValue).getAssessableAmount());
//							this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
//						}
					}
					if(indexValue==-1){
						System.out.println("Index As -1");
						
						if(validateState()){
							double assessVal=0;
							if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
								assessVal=priceqty+(priceqty*salesLineItemLis.get(i).getVatTax().getPercentage()/100);
								pocentity.setAssessableAmount(assessVal);
							}
							if(salesLineItemLis.get(i).getVatTax().getPercentage()==0){
								pocentity.setAssessableAmount(priceqty);
							}
						}
//						if(!validateState()&&this.getCbcformlis().getItemText(getCbcformlis().getSelectedIndex()).equals(AppConstants.NO)){
//							double assessVal=0;
//							if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
//								assessVal=priceqty+(priceqty*salesLineItemLis.get(i).getVatTax().getPercentage()/100);
//								pocentity.setAssessableAmount(assessVal);
//							}
//							if(salesLineItemLis.get(i).getVatTax().getPercentage()==0){
//								pocentity.setAssessableAmount(priceqty);
//							}
//						}
						
//						if(!validateState()&&this.getCbcformlis().getItemText(getCbcformlis().getSelectedIndex()).equals(AppConstants.YES)){
//							double assessVal=0;
//							if(this.getOlbcstpercent().getSelectedIndex()!=0){
//								TaxDetails cstper3=olbcstpercent.getSelectedItem();
//								assessVal=priceqty+(priceqty*cstper3.getTaxChargePercent()/100);
//								pocentity.setAssessableAmount(assessVal);
//							}
//						}
					}
					this.prodTaxTable.getDataprovider().getList().add(pocentity);
				}
			}
		}

		/*********************************Check Tax Percent*****************************************/
		/**
		 * This method returns 
		 * @param taxValue
		 * @param taxName
		 * @return
		 */
		
		public int checkTaxPercent(double taxValue,String taxName)
		{
			List<ProductOtherCharges> taxesList=this.prodTaxTable.getDataprovider().getList();
			
			for(int i=0;i<taxesList.size();i++)
			{
				double listval=taxesList.get(i).getChargePercent();
				int taxval=(int)listval;
				
				if(taxName.equals("Service")){
					if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("Service Tax")){
						return i;
					}
					if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("Service Tax")){
						return i;
					}
				}
				if(taxName.equals("VAT")){
					if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("VAT")){
						return i;
					}
					if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("VAT")){
						return i;
					}
				}
				if(taxName.equals("CST")){
					if(taxValue==taxesList.get(i).getChargePercent()&&taxesList.get(i).getChargeName().trim().equals("CST")){
						return i;
					}
				}
			}
			return -1;
		}
		
		
		
		/*************************Update Charges Table****************************************/
		/**
		 * This method updates the charges table.
		 * The table is updated when any row value is changed through field updater in table i.e
		 * row change event is fired
		 */
		
		public void updateChargesTable()
		{
			List<ProductOtherCharges> prodOtrLis=this.chargesTable.getDataprovider().getList();
			ArrayList<ProductOtherCharges> prodOtrArr=new ArrayList<ProductOtherCharges>();
			prodOtrArr.addAll(prodOtrLis);
			
			this.chargesTable.connectToLocal();
			System.out.println("prodo"+prodOtrArr.size());
			for(int i=0;i<prodOtrArr.size();i++)
			{
				ProductOtherCharges prodChargesEnt=new ProductOtherCharges();
				prodChargesEnt.setChargeName(prodOtrArr.get(i).getChargeName());
				prodChargesEnt.setChargePercent(prodOtrArr.get(i).getChargePercent());
				prodChargesEnt.setChargeAbsValue(prodOtrArr.get(i).getChargeAbsValue());
				//prodChargesEnt.setAssessableAmount(prodOtrArr.get(i).getAssessableAmount());
				prodChargesEnt.setFlagVal(prodOtrArr.get(i).getFlagVal());
				prodChargesEnt.setIndexCheck(prodOtrArr.get(i).getIndexCheck());
				if(prodOtrArr.get(i).getFlagVal()==false){
					prodChargesEnt.setAssessableAmount(this.getDoamtincltax().getValue());
				}
				if(prodOtrArr.get(i).getFlagVal()==true){
					double assesableVal=this.retrieveSurchargeAssessable(prodOtrArr.get(i).getIndexCheck());
					prodChargesEnt.setAssessableAmount(assesableVal);
				}
				
				this.chargesTable.getDataprovider().getList().add(prodChargesEnt);
			}
		}
		
		public double retrieveSurchargeAssessable(int indexValue)
		{
			List<ProductOtherCharges> otrChrgLis=this.chargesTable.getDataprovider().getList();
			ArrayList<ProductOtherCharges> otrChrgArr=new ArrayList<ProductOtherCharges>();
			otrChrgArr.addAll(otrChrgLis);
		
			double getAssessVal=0;
			for(int i=0;i<otrChrgArr.size();i++)
			{
				if(otrChrgArr.get(i).getFlagVal()==false&&otrChrgArr.get(i).getIndexCheck()==indexValue)
				{
					if(otrChrgArr.get(i).getChargePercent()!=0){
						getAssessVal=otrChrgArr.get(i).getAssessableAmount()*otrChrgArr.get(i).getChargePercent()/100;
					}
					if(otrChrgArr.get(i).getChargeAbsValue()!=0){
						getAssessVal=otrChrgArr.get(i).getChargeAbsValue();
					}
				}
			}
			return getAssessVal;
		}

		/************************************Overridden Methods*******************************************/


		@Override
		public void setEnable(boolean state) {
			super.setEnable(state);
			getstatustextbox().setEnabled(false);
			this.tbSalesOrderId.setEnabled(false);
			this.tbLeadId.setEnabled(false);
			this.tbQuotatinId.setEnabled(false);
			this.chargesTable.setEnable(false);
			this.prodTaxTable.setEnable(false);
			olbbBranch.setEnabled(false);
			personInfoComposite.setEnable(false);
			olbeSalesPerson.setEnabled(false);
//			olbcstpercent.setEnabled(false);
//			cbcformlis.setEnabled(false);
			ibDeliveryNoteId.setEnabled(false);
			/**
			 * Date 21-5-2018 by jayshree
			 * des.add this to make he address editable in status approved and user role is admin
			 */
			DeliveryNote entity=(DeliveryNote) presenter.getModel();
			String status=entity.getStatus();
			if(status.equals(DeliveryNote.APPROVED) && 
					LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN")&&AppMemory.getAppMemory().currentState==ScreeenState.EDIT ){
				acshippingcomposite.setEnable(true);
			}
			else{
				acshippingcomposite.setEnable(false);
			}
//			acshippingcomposite.setEnable(false);
			olbshippingchecklist.setEnabled(false);
			deliveryLineItem.setEnable(state);
			dototalamt.setEnabled(false);
			doamtincltax.setEnabled(false);
			donetpayamt.setEnabled(false);
			shippingListTable.setEnable(state);
			packingListTable.setEnable(state);
			tbremark.setEnabled(false);
			dbcompletiondate.setEnabled(false);
			
			tbDeliveryStatus.setEnabled(false);
		}

		@Override
		public void setToViewState() {
		
			super.setToViewState();
			setMenuAsPerStatus();
		
			SuperModel model=new DeliveryNote();
			model=deliverynoteobj;
			AppUtility.addDocumentToHistoryTable(AppConstants.SALESMODULE,AppConstants.DELIVERYNOTE, deliverynoteobj.getCount(), Integer.parseInt(personInfoComposite.getId().getValue()),personInfoComposite.getName().getValue(),Long.parseLong(personInfoComposite.getPhone().getValue()), false, model, null);
			
			String mainScreenLabel="Delivery Note";
			if(deliverynoteobj!=null&&deliverynoteobj.getCount()!=0){
				mainScreenLabel=deliverynoteobj.getCount()+"/"+deliverynoteobj.getStatus()+"/"+AppUtility.parseDate(deliverynoteobj.getCreationDate());
			}
				fgroupingCustomerInformation.getHeaderLabel().setText(mainScreenLabel);
		}
		@Override
		public void setToEditState() {
		
			super.setToEditState();
			this.processLevelBar.setVisibleFalse(false);
			/**
			 * Date 21-5-2018 by jayshree call this method to allowed the deliverry note in approved state
			 */
			this.setAppHeaderBarAsPerStatus();
			
			changeStatusToCreated();
			ibRefOrderNo.setEnabled(false);
			
			String mainScreenLabel="Delivery Note";
			if(deliverynoteobj!=null&&deliverynoteobj.getCount()!=0){
				mainScreenLabel=deliverynoteobj.getCount()+"/"+deliverynoteobj.getStatus()+"/"+AppUtility.parseDate(deliverynoteobj.getCreationDate());
			}
				fgroupingCustomerInformation.getHeaderLabel().setText(mainScreenLabel);
		}
		
		public void changeStatusToCreated()
		{
			DeliveryNote entity=(DeliveryNote) presenter.getModel();
			String status=entity.getStatus();
			
			if(DeliveryNote.REJECTED.equals(status.trim()))
			{
				this.tbStatus.setValue(DeliveryNote.CREATED);
			}
		}
		
		
		@Override
		public void clear() {
			super.clear();
			getstatustextbox().setText(DeliveryNote.CREATED);
		}
		
		public boolean validateWarehouse(){
			int ctr=0;
			for(int i=0;i<deliveryLineItem.getDataprovider().getList().size();i++){
				if(!deliveryLineItem.getDataprovider().getList().get(i).getWareHouseName().equals("")
						&&!deliveryLineItem.getDataprovider().getList().get(i).getStorageBinName().equals("")){
					ctr++;
				}
			}
			if(ctr==deliveryLineItem.getDataprovider().getList().size()){
				return true;
			}else{
				return false;
			}
		}
		
		
		/****************************************Validation Method(Overridden)**********************************/
		/**
		 * Validates form fields
		 */
		
		@Override
		 public boolean validate()
		 {
			boolean superRes= super.validate();
			boolean qtyValidation=true;
			boolean warehouseVal=true;
			
			if(deliveryLineItem.getDataprovider().getList().size() == 0)
	        {
	            showDialogMessage("Please Select at least one Product!");
	            return false;
	        }
			//Ashwini Patil Date:22-04-20222 Description: Product quantity should not be zero
			if(deliveryLineItem.getDataprovider().getList().size() > 0){
				List<DeliveryLineItems> saleslis=this.getDeliveryLineItem().getDataprovider().getList();
				for(DeliveryLineItems d:saleslis){	
					if(d.getQuantity()==0){
						 showDialogMessage("Please Enter Quantity");
						return false;
					}
				}
			}
			/**
			 * Date 1 august 2017 if condition added by vijay for quick sales no nedd this if needed then process config
			 */
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("DeliveryNote", "WarehouseMondatory")){
				if(validateWarehouse()==false){
					showDialogMessage("Please Select Warehouse Details.");
					warehouseVal=false;
				}
			}
			
			System.out.println("In Validate");
			
			if(validateProdQtyAgainstNote()==false){
				qtyValidation = false;
			}
			
			/**
			 * nidhi
			 * 20-09-2018
			 */
			if(LoginPresenter.mapModelSerialNoFlag ){
				if(!validateSerailNo()){
					showDialogMessage("Please verify Product Qty and serial number selection.");
					return false;
				}
			}
	        return superRes&&qtyValidation&&warehouseVal;
		 }

		boolean validateSerailNo(){
			boolean flag = true;
			

			for(int i=0;i<deliveryLineItem.getDataprovider().getList().size();i++){
				int qty = (int) deliveryLineItem.getDataprovider().getList().get(i).getQuantity();
				int serqty = 0;
				if(deliveryLineItem.getDataprovider().getList().get(i).getProSerialNoDetails()!=null  
						&& deliveryLineItem.getDataprovider().getList().get(i).getProSerialNoDetails().size()>0
						&& deliveryLineItem.getDataprovider().getList().get(i).getProSerialNoDetails().containsKey(0)){
					for(int j = 0;j<deliveryLineItem.getDataprovider().getList().get(i).getProSerialNoDetails().get(0).size() ; j++){
						if (deliveryLineItem.getDataprovider().getList().get(i).getProSerialNoDetails().get(0).get(j).isStatus()){
							serqty++;
						}
					}
					if(serqty>0 && serqty==qty){
						flag = true;
					}else{
						flag = false;
						break;
					}
				}
			}
			
			return flag; 
					
		}
		public boolean validateProdQtyAgainstNote(){
			boolean flage=true;
			NumberFormat nf=NumberFormat.getFormat("0.00");
			for(int i=0;i<deliveryLineItem.getDataprovider().getList().size();i++){
				
				if(!compList.isEmpty()){
					System.out.println("INSIDE Not null Condn.........");
					for(int h=0;h<compList.size();h++)
					{
						System.out.println("==================================================");
						System.out.println("Print1"+compList.get(h).getProdId());
						System.out.println("Print2"+compList.get(h).getProdQty());
						System.out.println("==================================================");
					}
						
					for(int j=0;j<compList.size();j++){
						if(deliveryLineItem.getDataprovider().getList().get(i).getProdId()== compList.get(j).getProdId()){
							if(deliveryLineItem.getDataprovider().getList().get(i).getQuantity()>compList.get(j).getProdQty()){
								double exceedQty=deliveryLineItem.getDataprovider().getList().get(i).getQuantity()-compList.get(j).getProdQty();
								System.out.println("Exceeds Qty"+exceedQty);
								flage=false;
								showDialogMessage("Product  "+deliveryLineItem.getDataprovider().getList().get(i).getProdName()+" exceeds quantity in Sales Order by "+nf.format(exceedQty)+".");
								break;
							}
						}
					}
				}
			}
			return flage;
		}
		
		
		
		/**********************************Change Widgets Method******************************************/
		/**
		 * This method represents changing the widgets style. Used for modifying payment composite
		 * widget fields
		 */
		
		private void changeWidgets()
		{
			//  For changing the text alignments.
			this.changeTextAlignment(dototalamt);
			this.changeTextAlignment(donetpayamt);
			this.changeTextAlignment(doamtincltax);
			
			//  For changing widget widths.
			this.changeWidth(dototalamt, 200, Unit.PX);
			this.changeWidth(donetpayamt, 200, Unit.PX);
			this.changeWidth(doamtincltax, 200, Unit.PX);
			
		}
		
		/*******************************Text Alignment Method******************************************/
		/**
		 * Method used for align of payment composite fields
		 * @param widg
		 */
		
		private void changeTextAlignment(Widget widg)
		{
			widg.getElement().getStyle().setTextAlign(TextAlign.CENTER);
		}
		
		/*****************************Change Width Method**************************************/
		/**
		 * Method used for changing width of fields
		 * @param widg
		 * @param size
		 * @param unit
		 */
		
		private void changeWidth(Widget widg,double size,Unit unit)
		{
			widg.getElement().getStyle().setWidth(size, unit);
		}
		
		public boolean validateState()
		{
			System.out.println("Presen"+DeliveryNotePresenter.companyStateVal);
			if(this.getAcshippingcomposite().getState().getValue().trim().equals(DeliveryNotePresenter.companyStateVal.trim())){
				return true;
			}
			return false;
		}
		

//		public void initializePercentCst()
//		{
//			MyQuerry querry=new MyQuerry();
//			Company comEntity=new Company();
//			Vector<Filter> filter=new Vector<Filter>();
//			Filter temp=null;
//			
//			temp=new Filter();
//			temp.setQuerryString("companyId");
//			temp.setLongValue(comEntity.getCompanyId());
//			filter.add(temp);
//			
//			temp=new Filter();
//			temp.setQuerryString("isCentralTax");
//			temp.setBooleanvalue(true);
//			filter.add(temp);
//			
//			temp=new Filter();
//			temp.setQuerryString("taxChargeStatus");
//			temp.setBooleanvalue(true);
//			filter.add(temp);
//			
//			querry.setFilters(filter);
//			querry.setQuerryObject(new TaxDetails());
//			olbcstpercent.MakeLive(querry);
//		}

	
		public void callOnSave1()
		{
		int counter=0;
		List<ProductQty> updatedQtyLis=new ArrayList<ProductQty>();
		double qty=0;
		for(int i=0;i<deliveryLineItem.getDataprovider().getList().size();i++){
			for(int j=0;j<compList.size();j++){
				if(deliveryLineItem.getDataprovider().getList().get(i).getProdId()== compList.get(j).getProdId()){
							
					if(deliveryLineItem.getDataprovider().getList().get(i).getQuantity()==compList.get(j).getProdQty()){
						counter++;
					}
					else{
						qty=compList.get(j).getProdQty()-deliveryLineItem.getDataprovider().getList().get(i).getQuantity();
							
						ProductQty pq=new ProductQty();
						pq.setProdId(deliveryLineItem.getDataprovider().getList().get(i).getProdId());
						pq.setProdQty(qty);
						updatedQtyLis.add(pq);
					}
				}
			}
		}
			
		if(counter!=deliveryLineItem.getDataprovider().getList().size()){
			DeliveryNotePresenter pres=(DeliveryNotePresenter)presenter;
			pres.createDeliveryNote(getUpdatedList1(updatedQtyLis));
		}
			
		}
		
		private List<DeliveryLineItems> getUpdatedList1(List<ProductQty> qtyLis)
		{
			ArrayList<DeliveryLineItems> arrGrn=new ArrayList<DeliveryLineItems>();
			
			for(int i=0;i<deliveryLineItem.getDataprovider().getList().size();i++)
			{
				for(int j=0;j<qtyLis.size();j++){
					if(deliveryLineItem.getDataprovider().getList().get(i).getProdId()==qtyLis.get(j).getProdId()){
						DeliveryLineItems dli=new DeliveryLineItems();
						dli.setProdId(deliveryLineItem.getDataprovider().getList().get(i).getProdId());
						dli.setProdCategory(deliveryLineItem.getDataprovider().getList().get(i).getProdCategory());
						dli.setProdCode(deliveryLineItem.getDataprovider().getList().get(i).getProdCode());
						dli.setProdName(deliveryLineItem.getDataprovider().getList().get(i).getProdName());
						dli.setQuantity(qtyLis.get(j).getProdQty());
						dli.setPrice(deliveryLineItem.getDataprovider().getList().get(i).getPrice());
						dli.setVatTax(deliveryLineItem.getDataprovider().getList().get(i).getVatTax());
						dli.setServiceTax(deliveryLineItem.getDataprovider().getList().get(i).getServiceTax());
						dli.setProdPercDiscount(deliveryLineItem.getDataprovider().getList().get(i).getProdPercDiscount());
						dli.setTotalAmount(deliveryLineItem.getDataprovider().getList().get(i).getTotalAmount());
						dli.setWareHouseName(deliveryLineItem.getDataprovider().getList().get(i).getWareHouseName());
						dli.setStorageLocName(deliveryLineItem.getDataprovider().getList().get(i).getStorageLocName());
						dli.setStorageBinName(deliveryLineItem.getDataprovider().getList().get(i).getStorageBinName());
						arrGrn.add(dli);
					}
				}
			}
			return arrGrn;
		}
		
		public void validateDeliveryNoteQty(ArrayList<ProductQty> orderList,ArrayList<ProductQty> noteList){
			compList=new ArrayList<ProductQty>();
			System.out.println("Validate"+orderList.size()+"   ==   "+noteList.size());
			double qty=0;
			if(noteList.size()!=0){
				for(int i=0;i<orderList.size();i++){
					for(int j=0;j<noteList.size();j++){
						if(orderList.get(i).getProdId()==noteList.get(j).getProdId()){
							qty=orderList.get(i).getProdQty()-noteList.get(j).getProdQty();
						
							ProductQty pq=new ProductQty();
							pq.setProdId(orderList.get(i).getProdId());
							pq.setProdQty(qty);
					System.out.println("Loop Running");
							compList.add(pq);
						}
					}
				}
			}
			else{
				compList.addAll(orderList);
			}
			System.out.println("Comp lIst Size"+compList.size());
		}
		
		public void addInvoices(int salesOrderId,final int invoiceCountId)
		{
			MyQuerry querry=new MyQuerry();
			Company c=new Company();
			Vector<Filter> filtervec=new Vector<Filter>();
			Filter temp=null;
			
			temp=new Filter();
			temp.setLongValue(c.getCompanyId());
			temp.setQuerryString("companyId");
			filtervec.add(temp);
			
			temp=new Filter();
			temp.setIntValue(salesOrderId);
			temp.setQuerryString("contractCount");
			filtervec.add(temp);
			
			temp=new Filter();
			temp.setStringValue(AppConstants.ORDERTYPEFORSALES);
			temp.setQuerryString("typeOfOrder");
			filtervec.add(temp);

			
			querry.setFilters(filtervec);
			querry.setQuerryObject(new Invoice());
			
			
			genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onFailure(Throwable caught) {
					showDialogMessage("An Unexpected error occurred!");
				}
	
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					if(result.size()==0)
					{
						lisInvoice.addItem("SELECT");
					}
					else{
						List<String> invoicesLis=new ArrayList<String>();
						invoicesLis.clear();
						invoicesLis.add("SELECT");
						for(SuperModel model:result)
						{
							Invoice inv=(Invoice)model;
							invoicesLis.add(inv.getCount()+"");
						}
						
						for(int d=0;d<invoicesLis.size();d++)
						{
							lisInvoice.addItem(invoicesLis.get(d));
						}
						
						if(invoiceCountId!=-1)
						{
							System.out.println("CountInv"+lisInvoice.getItemCount());
							for(int i=0;i<lisInvoice.getItemCount();i++)
							{
								if(!lisInvoice.getItemText(i).equals("SELECT")){
									int data=Integer.parseInt(lisInvoice.getItemText(i));
									System.out.println("ParseInt"+data);
									
									if(data==invoiceCountId)
									{
										System.out.println("Equalled");
										lisInvoice.setSelectedIndex(i);
										break;
									}
								}
							}
						}
						
					}
				        
				}
			});
			
			
		}
		
		
		protected void checkCustomerStatus(int cCount)
		{
			MyQuerry querry=new MyQuerry();
			Company c=new Company();
			Vector<Filter> filtervec=new Vector<Filter>();
			Filter temp=null;
			
			temp=new Filter();
			temp.setQuerryString("companyId");
			temp.setLongValue(c.getCompanyId());
			filtervec.add(temp);
			
			temp=new Filter();
			temp.setQuerryString("count");
			temp.setIntValue(cCount);
			filtervec.add(temp);
			
			querry.setFilters(filtervec);
			querry.setQuerryObject(new Customer());
			
			genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onFailure(Throwable caught) {
					showDialogMessage("An Unexpected error occurred!");
				}

				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
						for(SuperModel model:result)
						{
							Customer custEntity = (Customer)model;
							if(AppConstants.ACTIVE.equals(custEntity.getStatus().trim())){
								 getPersonInfoComposite().getId().getElement().addClassName("personactive");
								 getPersonInfoComposite().getName().getElement().addClassName("personactive");
								 getPersonInfoComposite().getPhone().getElement().addClassName("personactive");
								 getPersonInfoComposite().getTbpocName().getElement().addClassName("personactive");
							}
						}
					}
				 });
			}
		
		
		
		/*******************************************Type Drop Down Logic**************************************/
		
		@Override
		public void onChange(ChangeEvent event) {
			if(event.getSource().equals(oblCustomerBranch)){
				if(oblCustomerBranch.getSelectedIndex()!=0){
					CustomerBranchDetails obj=oblCustomerBranch.getSelectedItem();
					if(obj!=null){
						if(obj.getPocName()!=null){
							tbCustPocName.setValue(obj.getPocName());
							acshippingcomposite.setValue(obj.getAddress());
						}
					}
				}else{
					tbCustPocName.setValue("");
					acshippingcomposite.clear();
				}
			}
			
			if(event.getSource().equals(olbdeliveryCategory))
			{
				if(olbdeliveryCategory.getSelectedIndex()!=0){
					ConfigCategory cat=olbdeliveryCategory.getSelectedItem();
					if(cat!=null){
						AppUtility.makeLiveTypeDropDown(olbdeliveryType, cat.getCategoryName(), cat.getCategoryCode(), cat.getInternalType());
					}
				}
			}
			
		}


	/**************************************Getters And Setters**********************************************/
		
		
		public PersonInfoComposite getPersonInfoComposite() {
			return personInfoComposite;
		}

		public void setPersonInfoComposite(PersonInfoComposite personInfoComposite) {
			this.personInfoComposite = personInfoComposite;
		}

//		public ListBox getCbcformlis() {
//			return cbcformlis;
//		}
//
//		public void setCbcformlis(ListBox cbcformlis) {
//			this.cbcformlis = cbcformlis;
//		}

//		public ObjectListBox<TaxDetails> getOlbcstpercent() {
//			return olbcstpercent;
//		}
//
//		public void setOlbcstpercent(ObjectListBox<TaxDetails> olbcstpercent) {
//			this.olbcstpercent = olbcstpercent;
//		}

		public TextBox getTbSalesOrderId() {
			return tbSalesOrderId;
		}

		public void setTbSalesOrderId(TextBox tbSalesOrderId) {
			this.tbSalesOrderId = tbSalesOrderId;
		}

		public TextBox getTbLeadId() {
			return tbLeadId;
		}

		public void setTbLeadId(TextBox tbLeadId) {
			this.tbLeadId = tbLeadId;
		}

		public TextBox getTbQuotatinId() {
			return tbQuotatinId;
		}

		public void setTbQuotatinId(TextBox tbQuotatinId) {
			this.tbQuotatinId = tbQuotatinId;
		}

		public DateBox getDbReferenceDate() {
			return dbReferenceDate;
		}

		public void setDbReferenceDate(DateBox dbReferenceDate) {
			this.dbReferenceDate = dbReferenceDate;
		}

		public TextBox getTbReferenceNumber() {
			return tbReferenceNumber;
		}

		public void setTbReferenceNumber(TextBox tbReferenceNumber) {
			this.tbReferenceNumber = tbReferenceNumber;
		}

		public ObjectListBox<Employee> getOlbeSalesPerson() {
			return olbeSalesPerson;
		}

		public void setOlbeSalesPerson(ObjectListBox<Employee> olbeSalesPerson) {
			this.olbeSalesPerson = olbeSalesPerson;
		}

		public UploadComposite getUcChooseFile() {
			return ucChooseFile;
		}

		public void setUcChooseFile(UploadComposite ucChooseFile) {
			this.ucChooseFile = ucChooseFile;
		}

		
		public TextBox getIbDeliveryNoteId() {
			return ibDeliveryNoteId;
		}

		public void setIbDeliveryNoteId(TextBox ibDeliveryNoteId) {
			this.ibDeliveryNoteId = ibDeliveryNoteId;
		}

		public TextBox getTbStatus() {
			return tbStatus;
		}

		public void setTbStatus(TextBox tbStatus) {
			this.tbStatus = tbStatus;
		}

		public ObjectListBox<Branch> getOlbbBranch() {
			return olbbBranch;
		}

		public void setOlbbBranch(ObjectListBox<Branch> olbbBranch) {
			this.olbbBranch = olbbBranch;
		}

		public ObjectListBox<Config> getOlbdeliveryGroup() {
			return olbdeliveryGroup;
		}

		public void setOlbdeliveryGroup(ObjectListBox<Config> olbdeliveryGroup) {
			this.olbdeliveryGroup = olbdeliveryGroup;
		}

		public ObjectListBox<Type> getOlbdeliveryType() {
			return olbdeliveryType;
		}

		public void setOlbdeliveryType(ObjectListBox<Type> olbdeliveryType) {
			this.olbdeliveryType = olbdeliveryType;
		}

		public ObjectListBox<ConfigCategory> getOlbdeliveryCategory() {
			return olbdeliveryCategory;
		}

		public void setOlbdeliveryCategory(
				ObjectListBox<ConfigCategory> olbdeliveryCategory) {
			this.olbdeliveryCategory = olbdeliveryCategory;
		}

		public DateBox getDbdeliverydate() {
			return dbdeliverydate;
		}

		public void setDbdeliverydate(DateBox dbdeliverydate) {
			this.dbdeliverydate = dbdeliverydate;
		}

		public ObjectListBox<ShippingMethod> getOlbShippingMethod() {
			return olbShippingMethod;
		}

		public void setOlbShippingMethod(ObjectListBox<ShippingMethod> olbShippingMethod) {
			this.olbShippingMethod = olbShippingMethod;
		}

		public ObjectListBox<PackingMethod> getOlbPackingMethod() {
			return olbPackingMethod;
		}

		public void setOlbPackingMethod(ObjectListBox<PackingMethod> olbPackingMethod) {
			this.olbPackingMethod = olbPackingMethod;
		}

		public ListBox getOlbshippingchecklist() {
			return olbshippingchecklist;
		}

		public void setOlbshippingchecklist(ListBox olbshippingchecklist) {
			this.olbshippingchecklist = olbshippingchecklist;
		}

		public ListBox getOlbpackingchecklist() {
			return olbpackingchecklist;
		}

		public void setOlbpackingchecklist(ListBox olbpackingchecklist) {
			this.olbpackingchecklist = olbpackingchecklist;
		}

		public ObjectListBox<Employee> getOlbApproverName() {
			return olbApproverName;
		}

		public void setOlbApproverName(ObjectListBox<Employee> olbApproverName) {
			this.olbApproverName = olbApproverName;
		}

		public DeliverySalesLineItemTable getDeliveryLineItem() {
			return deliveryLineItem;
		}

		public void setDeliveryLineItem(DeliverySalesLineItemTable deliveryLineItem) {
			this.deliveryLineItem = deliveryLineItem;
		}

		public ShippingStepsTable getShippingListTable() {
			return shippingListTable;
		}

		public void setShippingListTable(ShippingStepsTable shippingListTable) {
			this.shippingListTable = shippingListTable;
		}

		public PackingStepsTable getPackingListTable() {
			return packingListTable;
		}

		public void setPackingListTable(PackingStepsTable packingListTable) {
			this.packingListTable = packingListTable;
		}

		public DoubleBox getDototalamt() {
			return dototalamt;
		}

		public void setDototalamt(DoubleBox dototalamt) {
			this.dototalamt = dototalamt;
		}

		public DoubleBox getDoamtincltax() {
			return doamtincltax;
		}

		public void setDoamtincltax(DoubleBox doamtincltax) {
			this.doamtincltax = doamtincltax;
		}

		public DoubleBox getDonetpayamt() {
			return donetpayamt;
		}

		public void setDonetpayamt(DoubleBox donetpayamt) {
			this.donetpayamt = donetpayamt;
		}

		public ProductChargesTable getChargesTable() {
			return chargesTable;
		}

		public void setChargesTable(ProductChargesTable chargesTable) {
			this.chargesTable = chargesTable;
		}

		public ProductTaxesTable getProdTaxTable() {
			return prodTaxTable;
		}

		public void setProdTaxTable(ProductTaxesTable prodTaxTable) {
			this.prodTaxTable = prodTaxTable;
		}

		public AddressComposite getAcshippingcomposite() {
			return acshippingcomposite;
		}

		public void setAcshippingcomposite(AddressComposite acshippingcomposite) {
			this.acshippingcomposite = acshippingcomposite;
		}

		public TextBox getTbvehicleno() {
			return tbvehicleno;
		}

		public void setTbvehicleno(TextBox tbvehicleno) {
			this.tbvehicleno = tbvehicleno;
		}

		public TextBox getTbremark() {
			return tbremark;
		}

		public void setTbremark(TextBox tbremark) {
			this.tbremark = tbremark;
		}

		public DateBox getDbcompletiondate() {
			return dbcompletiondate;
		}

		public void setDbcompletiondate(DateBox dbcompletiondate) {
			this.dbcompletiondate = dbcompletiondate;
		}
		
		public TextBox getTbvehicleName() {
			return tbvehicleName;
		}

		public void setTbvehicleName(TextBox tbvehicleName) {
			this.tbvehicleName = tbvehicleName;
		}

		public TextBox getTbdriverName() {
			return tbdriverName;
		}

		public void setTbdriverName(TextBox tbdriverName) {
			this.tbdriverName = tbdriverName;
		}

		public TextBox getTblrNumber() {
			return tblrNumber;
		}

		public void setTblrNumber(TextBox tblrNumber) {
			this.tblrNumber = tblrNumber;
		}

		public TextBox getTbtrackingNumber() {
			return tbtrackingNumber;
		}

		public void setTbtrackingNumber(TextBox tbtrackingNumber) {
			this.tbtrackingNumber = tbtrackingNumber;
		}

		public TextBox getTbotherinfo1() {
			return tbotherinfo1;
		}

		public void setTbotherinfo1(TextBox tbotherinfo1) {
			this.tbotherinfo1 = tbotherinfo1;
		}

		public TextBox getTbotherinfo2() {
			return tbotherinfo2;
		}

		public void setTbotherinfo2(TextBox tbotherinfo2) {
			this.tbotherinfo2 = tbotherinfo2;
		}

		public TextBox getIbRefOrderNo() {
			return ibRefOrderNo;
		}

		public void setIbRefOrderNo(TextBox ibRefOrderNo) {
			this.ibRefOrderNo = ibRefOrderNo;
		}

		
		
		@Override
		public void refreshTableData() {
			// TODO Auto-generated method stub
			super.refreshTableData();
			prodTaxTable.getTable().redraw();
			chargesTable.getTable().redraw();
			deliveryLineItem.getTable().redraw();
			shippingListTable.getTable().redraw();
			packingListTable.getTable().redraw();
			chargesTable.getTable().redraw();
			prodTaxTable.getTable().redraw();
		}

		

}
