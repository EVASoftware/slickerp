package com.slicktechnologies.client.views.deliverynote;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.view.client.ProvidesKey;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.views.popups.ProductSerailNumberPopup;
import com.slicktechnologies.shared.ProductSerialNoMapping;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.deliverynote.DeliveryLineItems;
import com.slicktechnologies.shared.common.inventory.StorageLocation;
import com.slicktechnologies.shared.common.inventory.Storagebin;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;

public class DeliverySalesLineItemTable extends SuperTable<DeliveryLineItems> implements ClickHandler {
	
	InventoryLocationPopUp invLoc=new InventoryLocationPopUp();
	PopupPanel panel=new PopupPanel(true);
	
	int rowIndex;
	
	TextColumn<DeliveryLineItems> prodCategoryColumn,prodCodeColumn,prodNameColumn;
	Column<DeliveryLineItems,String> prodQtyColumn;
	TextColumn<DeliveryLineItems> viewprodQtyColumn;
	TextColumn<DeliveryLineItems> priceColumn;
	TextColumn<DeliveryLineItems> vatColumn,serviceColumn,totalColumn;
	TextColumn<DeliveryLineItems>prodPercentDiscColumn;
	Column<DeliveryLineItems,String> deleteColumn;

	
	Column<DeliveryLineItems, String> warehouseColumn;
	TextColumn<DeliveryLineItems> warehouseViewColumn;
	
	Column<DeliveryLineItems, String> storagebin;
	Column<DeliveryLineItems, String> columnviewBin;
	
	Column<DeliveryLineItems, String> storageLoc;
	TextColumn<DeliveryLineItems> columnviewLoc;
	Column<DeliveryLineItems, String> addColumn;
	
	/*Rohan added this column for showing discount Amount In the table 
	 * dated : 12/9/2016
	 */
	TextColumn<DeliveryLineItems>prodPercentDiscAmountColumn;
	
	/*
	 * nidhi
	 * 24-06-217
	 * Show available Qty
	 */
	TextColumn<DeliveryLineItems> getColumnProductAvailableQty;
	/*
	 * end
	 */
	
	NumberFormat nf=NumberFormat.getFormat("0.00");
	
	TextColumn<DeliveryLineItems> viewHsnCodeColumn;
	/**
	 * nidhi
	 * 21-08-2018
	 * for map serial number
	 */
	Column<DeliveryLineItems, String> addProSerialNo;
	Column<DeliveryLineItems, String> viewProSerialNo;
	boolean tableState = false;
	ProductSerailNumberPopup prodSerialPopup = new ProductSerailNumberPopup(true,false);
	HashMap<Integer, ArrayList<ProductSerialNoMapping>> proSerialList  = new HashMap<Integer, ArrayList<ProductSerialNoMapping>>();
	
	public DeliverySalesLineItemTable()
	{
		super();
		invLoc.getAddButton().addClickHandler(this);
		invLoc.getCancelButton().addClickHandler(this);
		/**
		 * nidhi
		 * 22-08-2018
		 */
		prodSerialPopup.getLblOk().addClickHandler(this);
		prodSerialPopup.getLblCancel().addClickHandler(this);
	}
	public DeliverySalesLineItemTable(UiScreen<DeliveryLineItems> view)
	{
		super(view);	
		/**
		 * nidhi
		 * 22-08-2018
		 */
		prodSerialPopup.getLblOk().addClickHandler(this);
		prodSerialPopup.getLblCancel().addClickHandler(this);
	}
	
	
	public void createTable()
	{
//		createColumnprodCategoryColumn();
//		createColumnprodCodeColumn();
//		createColumnprodNameColumn();
//		createColumnprodQtyColumn();
//		/*
//		 * 24-06-2017
//		 * nidhi
//		 * 
//		 */
//		addColumnProductAvailableQty();
//		/*
//		 * end
//		 */
//		createColumnpriceColumn();
//		createColumnvatColumn();
//		createColumnPercDiscount();
//		createColumnDiscountAmount();
//		createColumntotalColumn();
//		
//		createColumnprodHSNCodeColumn();
//
//		viewWarehouse();
//		viewstarageLoc();
//		viewstorageBin();
//		
//		createColumnAddColumn();
//		/**
//		 * nidhi
//		 * 21-08-2018
//		 * for add serial number
//		 */
		
		createColumnprodNameColumn();
		createColumnprodQtyColumn();
		/*
		 * 24-06-2017
		 * nidhi
		 * 
		 */
		addColumnProductAvailableQty();
		/*
		 * end
		 */
		createColumnpriceColumn();
		createColumnPercDiscount();
		createColumnDiscountAmount();
		createColumntotalColumn();
		viewWarehouse();
		createColumnvatColumn();
		viewstarageLoc();
		viewstorageBin();
		createColumnprodCategoryColumn();
		createColumnprodCodeColumn();
		createColumnprodHSNCodeColumn();
		createColumnAddColumn();
		
		if(LoginPresenter.mapModelSerialNoFlag){
			createColumnAddProSerialNoColumn();
		}
//		createColumndeleteColumn();
		addFieldUpdater();
		table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);	
	}
	
	private void createColumnprodHSNCodeColumn() {
		viewHsnCodeColumn = new TextColumn<DeliveryLineItems>() {
			
			@Override
			public String getValue(DeliveryLineItems object) {
				if(object.getPrduct()!=null){
					if(object.getPrduct().getHsnNumber()!=null && !object.getPrduct().getHsnNumber().equals("")){
						return object.getPrduct().getHsnNumber();
					}
				}
				return "";
			}
		};
		table.addColumn(viewHsnCodeColumn,"#HSN Code");
		table.setColumnWidth(viewHsnCodeColumn, 100,Unit.PX);
	}
	
	private void addColumnProductAvailableQty() {
		getColumnProductAvailableQty=new TextColumn<DeliveryLineItems>() {
			@Override
			public String getValue(DeliveryLineItems object) {
				return nf.format(object.getProdAvailableQuantity());
			}
		};
		table.addColumn(getColumnProductAvailableQty, "Stock");
		table.setColumnWidth(getColumnProductAvailableQty,100,Unit.PX);
	}
	
	
	protected void createColumnprodCategoryColumn()
	{
		prodCategoryColumn=new TextColumn<DeliveryLineItems>()
				{
			@Override public String getValue(DeliveryLineItems object){
				return object.getProdCategory();
			}
				};
				table.addColumn(prodCategoryColumn,"Category");

	}
	protected void createColumnprodCodeColumn()
	{
		prodCodeColumn=new TextColumn<DeliveryLineItems>()
				{
			@Override public String getValue(DeliveryLineItems object){
				return object.getProdCode();
			}
				};
				table.addColumn(prodCodeColumn,"PCode");

	}
	protected void createColumnprodNameColumn()
	{
		prodNameColumn=new TextColumn<DeliveryLineItems>()
				{
			@Override public String getValue(DeliveryLineItems object){
				return object.getProdName();
			}
				};
				table.addColumn(prodNameColumn,"Name");

	}
	protected void createColumnprodQtyColumn()
	{
		EditTextCell editCell=new EditTextCell();
		prodQtyColumn=new Column<DeliveryLineItems,String>(editCell)
				{
			@Override
			public String getValue(DeliveryLineItems object)
			{
				if(object.getQuantity()==0){
					return 0+"";}
				else{
					return object.getQuantity()+"";}
			}
				};
				table.addColumn(prodQtyColumn,"#Qty");
		}
	
	
	protected void createViewQtyColumn()
	{
		
		viewprodQtyColumn=new TextColumn<DeliveryLineItems>()
				{
			@Override
			public String getValue(DeliveryLineItems object)
			{
				if(object.getQuantity()==0){
					return 0+"";}
				else{
					return object.getQuantity()+"";
				}
			}
				};
				table.addColumn(viewprodQtyColumn,"#Qty");
	}
	
	
	
	protected void createColumnDiscountAmount()
	{
		prodPercentDiscAmountColumn=new TextColumn<DeliveryLineItems>()
				{
			@Override
			public String getValue(DeliveryLineItems object)
			{
				if(object.getDiscountAmt()==0){
					return 0+"";}
				else{
					return object.getDiscountAmt()+"";}
			}
				};
				table.addColumn(prodPercentDiscAmountColumn,"Disc Amt");
               
	}
	
	protected void createColumnPercDiscount()
	{
		prodPercentDiscColumn=new TextColumn<DeliveryLineItems>()
				{
			@Override
			public String getValue(DeliveryLineItems object)
			{
				if(object.getProdPercDiscount()==0||object.getProdPercDiscount()==null){
					return 0+"";}
				else{
					return object.getProdPercDiscount()+"";}
			}
				};
				table.addColumn(prodPercentDiscColumn,"% Disc");
               
	}
	
	protected void createColumnpriceColumn()
	{
		priceColumn=new TextColumn<DeliveryLineItems>()
				{
			@Override
			public String getValue(DeliveryLineItems object)
			{
				return nf.format(object.getPrice());
			}
				};
			table.addColumn(priceColumn,"Price");
	}
	
	protected void createColumnvatColumn()
	{
		vatColumn=new TextColumn<DeliveryLineItems>()
				{
			@Override
			public String getValue(DeliveryLineItems object)
			{
				if(object.getVatTax()!=null){
					return object.getVatTax().getPercentage()+"";
				}
				else{ 
					return "N.A.";
				}
			}
				};
				table.addColumn(vatColumn,"TAX 1");
	}
	protected void createColumnserviceColumn()
	{
		serviceColumn=new TextColumn<DeliveryLineItems>()
		{
			@Override
			public String getValue(DeliveryLineItems object)
			{
				//SuperProduct prod=object.getPrduct();
				if(object.getServiceTax()!=null){
					return object.getServiceTax().getPercentage()+"";
				}
				else{ 
					return "N.A.";
				}
			}
				};
				table.addColumn(serviceColumn,"Ser. Tax");
	}
	protected void createColumntotalColumn()
	{
		totalColumn=new TextColumn<DeliveryLineItems>()
				{
			@Override
			public String getValue(DeliveryLineItems object)
			{
				double total=0;
				if(object.getProdPercDiscount()==null){
					total=object.getPrice()*object.getQuantity();
				}
				if(object.getProdPercDiscount()!=null){
					total=object.getPrice()*object.getProdPercDiscount()/100;
					total=(object.getPrice()-total)*object.getQuantity();
				}
				return nf.format(total);
			}
				};
				table.addColumn(totalColumn,"Total");
	}
	
	public void viewstorageBin() {
		columnviewBin = new TextColumn<DeliveryLineItems>() {
			@Override
			public String getValue(DeliveryLineItems object) {
				if(object.getStorageBinName()!=null){
					return object.getStorageBinName();
				}
				else{
					return "";
				}
			}
		};
		table.addColumn(columnviewBin,"Sto. Bin");
	}

	public void viewWarehouse() {
		warehouseViewColumn = new TextColumn<DeliveryLineItems>() {
			@Override
			public String getValue(DeliveryLineItems object) {
				if(object.getWareHouseName()!=null){
				return object.getWareHouseName();
				}
				else{
					return "";
				}
			}
		};
		table.addColumn(warehouseViewColumn, "WareHouse");
	}
	
	public void viewstarageLoc() {
		columnviewLoc = new TextColumn<DeliveryLineItems>() {
			@Override
			public String getValue(DeliveryLineItems object) {
				if(object.getStorageLocName()!=null){
					return object.getStorageLocName();
				}
				else{
					return "";
				}
			}
		};
		table.addColumn(columnviewLoc, "Sto. Loc.");
	}

	
	protected void createColumndeleteColumn()
	{
		ButtonCell btnCell= new ButtonCell();
		deleteColumn=new Column<DeliveryLineItems,String>(btnCell)
				{
			@Override
			public String getValue(DeliveryLineItems object)
			{
				return  "Delete" ;
			}
				};
				table.addColumn(deleteColumn,"Delete");
	}

	@Override public void addFieldUpdater() 
	{
		createFieldUpdaterprodQtyColumn();
		createFieldUpdaterAdd();
		/**
		 * nidhi
		 * 21-08-2018
		 */
		if(LoginPresenter.mapModelSerialNoFlag){
			if(addProSerialNo!=null)
			createFieldUpdaterAddSerialNo();
			if(viewProSerialNo!=null)
			createFieldUpdaterViewSerialNo();
		}
//		createFieldUpdaterdeleteColumn();
	}

	protected void createFieldUpdaterprodQtyColumn()
	{
		prodQtyColumn.setFieldUpdater(new FieldUpdater<DeliveryLineItems, String>()
				{
			@Override

			public void update(int index,DeliveryLineItems object,String value)
			{

				try{
					Double val1=Double.parseDouble(value.trim());
					object.setQuantity(val1);
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				}
				catch (NumberFormatException e)
				{
				}
				table.redrawRow(index);
			}
				});
	}
	
	protected void createFieldUpdaterdeleteColumn()
	{
		deleteColumn.setFieldUpdater(new FieldUpdater<DeliveryLineItems,String>()
				{
			@Override
			public void update(int index,DeliveryLineItems object,String value)
			{
				getDataprovider().getList().remove(object);
				table.redrawRow(index);
			}
				});
	}
	
	
	public void createColumnAddColumn() {
		ButtonCell btnCell = new ButtonCell();
		addColumn = new Column<DeliveryLineItems, String>(btnCell) {
			@Override
			public String getValue(DeliveryLineItems object) {
				return "Select Warehouse";
			}
		};
		table.addColumn(addColumn, "");

	}
	
	
	public void createFieldUpdaterAdd() {
		addColumn.setFieldUpdater(new FieldUpdater<DeliveryLineItems, String>() {
			@Override
			public void update(int index, DeliveryLineItems object, String value) {
				
				panel=new PopupPanel(true);
				panel.add(invLoc);
				
				InventoryLocationPopUp.initialzeWarehouse(object.getProdId());
				
				
				if(object.getWareHouseName()!=null&&object.getStorageLocName()!=null&&object.getStorageBinName()!=null){
					System.out.println("Inside table not null list settt...........");
					for(int i=0;i<invLoc.getLsWarehouse().getItemCount();i++)
					{
						String data=invLoc.lsWarehouse.getItemText(i);
						if(data.equals(object.getWareHouseName()))
						{
							invLoc.lsWarehouse.setSelectedIndex(i);
							break;
						}
					}
					
					invLoc.getLsStorageLoc().clear();
					invLoc.lsStorageLoc.addItem("--SELECT--");
					invLoc.lsStorageLoc.addItem(object.getStorageLocName());
					
					
					invLoc.getLsStoragebin().clear();
					invLoc.lsStoragebin.addItem("--SELECT--");
					
					for(int i=0;i<invLoc.arraylist2.size();i++){
						if(invLoc.arraylist2.get(i).getWarehouseName().equals(object.getWareHouseName())
								&&invLoc.arraylist2.get(i).getStorageLocation().equals(object.getStorageLocName())){
							invLoc.lsStoragebin.addItem(invLoc.arraylist2.get(i).getStorageBin());
						}
					}
					
					for(int i=0;i<invLoc.getLsStorageLoc().getItemCount();i++)
					{
						String data=invLoc.lsStorageLoc.getItemText(i);
						if(data.equals(object.getStorageLocName()))
						{
							invLoc.lsStorageLoc.setSelectedIndex(i);
							break;
						}
					}
					for(int i=0;i<invLoc.getLsStoragebin().getItemCount();i++)
					{
						String data=invLoc.lsStoragebin.getItemText(i);
						if(data.equals(object.getStorageBinName()))
						{
							invLoc.lsStoragebin.setSelectedIndex(i);
							break;
						}
					}
				}
				invLoc.formView();
				panel.show();
				
				
				panel.center();
				rowIndex=index;
			}
		});
	}

	@Override
	public void onClick(ClickEvent event) {
		if(event.getSource()==invLoc.getAddButton())
		{	
			if(invLoc.getLsWarehouse().getSelectedIndex()!=0&&invLoc.getLsStorageLoc().getSelectedIndex()!=0&&invLoc.getLsStoragebin().getSelectedIndex()!=0){
				ArrayList<DeliveryLineItems> list=new ArrayList<DeliveryLineItems>();
				if(getDataprovider().getList().size()!=0){
					list.addAll(getDataprovider().getList());
					for( int i=rowIndex;i<getDataprovider().getList().size();i++){
						list.get(rowIndex).setWareHouseName(invLoc.getLsWarehouse().getValue(invLoc.getLsWarehouse().getSelectedIndex()));
						list.get(rowIndex).setStorageLocName(invLoc.getLsStorageLoc().getValue(invLoc.getLsStorageLoc().getSelectedIndex()));
						list.get(rowIndex).setStorageBinName(invLoc.getLsStoragebin().getValue(invLoc.getLsStoragebin().getSelectedIndex()));
						/**
						 * Date : 11-07-2017 By ANIL
						 */
						list.get(rowIndex).setProdAvailableQuantity(invLoc.getAvailableQty());
						/**
						 * End
						 */
						
						getDataprovider().getList().clear();
						getDataprovider().getList().addAll(list);
					}
				}
				panel.hide();
			}
			if(invLoc.getLsWarehouse().getSelectedIndex()==0&&invLoc.getLsStorageLoc().getSelectedIndex()==0&&invLoc.getLsStoragebin().getSelectedIndex()==0){
				DeliveryNotePresenter.showMessage("Please Select Warehouse.");
			}
			else if(invLoc.getLsStorageLoc().getSelectedIndex()==0&&invLoc.getLsStoragebin().getSelectedIndex()==0){
				DeliveryNotePresenter.showMessage("Please Select Storage Location.");
			}
			else if(invLoc.getLsStoragebin().getSelectedIndex()==0){
				DeliveryNotePresenter.showMessage("Please Select Storage Bin.");
			}
			
		}
		if(event.getSource()==invLoc.getCancelButton())
		{	
			invLoc.clear();
			panel.hide();
		}
		
		/**
		 * nidhi
		 * 22-08-2018
		 */
		if(event.getSource() == prodSerialPopup.getLblOk()){
			ArrayList<ProductSerialNoMapping> proList = new ArrayList<ProductSerialNoMapping>();
//			proList.addAll(prodSerialPopup.getProSerNoTable().getDataprovider().getList());
			List<ProductSerialNoMapping> mainproList = prodSerialPopup.getProSerNoTable().getDataprovider().getList();
			int qtyCnt = 0;
			for(int i =0 ; i <mainproList.size();i++){
				System.out.println(mainproList.get(i).getProSerialNo() + "  --- get serial number -- ");
				if(mainproList.get(i).getProSerialNo().trim().length()>0 && mainproList.get(i).isStatus()){
					proList.add(mainproList.get(i));
					qtyCnt++;
				}
			}
			
			ArrayList<DeliveryLineItems> list=new ArrayList<DeliveryLineItems>();
			list.addAll(getDataprovider().getList());
			
			/*if(proList.size() !=0 && proList.size() < list.get(rowIndex).getQuantity() && mainproList.size()>=list.get(rowIndex).getQuantity() ){
				GWTCAlert alert = new GWTCAlert();
				alert.alert("Please select serial no atlist equal to product quantity");
				return;
			}*/
			/*if(proList.size() !=0 && proList.size() < list.get(rowIndex).getQuantity()){
				list.get(rowIndex).setQuantity((double) proList.size());
				
				table.redraw();
			}
			*/
			list.get(rowIndex).getProSerialNoDetails().clear();
			list.get(rowIndex).getProSerialNoDetails().put(0, proList);
			if(qtyCnt!=0 && list.get(rowIndex).getProSerialNoDetails()!=null){
				list.get(rowIndex).setQuantity((double)qtyCnt);
			}
			
			getDataprovider().getList().clear();
			getDataprovider().getList().addAll(list);
			RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
			
			prodSerialPopup.hidePopUp();
		}
		if(event.getSource()== prodSerialPopup.getLblCancel()){
			prodSerialPopup.hidePopUp();
		}
	}
	
	
	@Override
	public void applyStyle()
	{

	}
	@Override
	protected void initializekeyprovider() {
		keyProvider= new ProvidesKey<DeliveryLineItems>()
				{
			@Override
			public Object getKey(DeliveryLineItems item)
			{
				if(item==null)
				{
					return null;
				}
				else
					return new Date().getTime();
			}
				};
	}

	public double calculateTotalExcludingTax()
	{
		List<DeliveryLineItems>list=getDataprovider().getList();
		double sum=0,priceVal=0;
		for(int i=0;i<list.size();i++)
		{
			DeliveryLineItems entity=list.get(i);
			if(entity.getProdPercDiscount()==null){
				priceVal=entity.getPrice()*entity.getQuantity();
				sum=sum+priceVal;
			}
			if(entity.getProdPercDiscount()!=null){
				priceVal=entity.getPrice()*entity.getProdPercDiscount()/100;
				
				priceVal=(entity.getPrice()-priceVal)*entity.getQuantity();
				
				sum=sum+priceVal;
			}
			
	
		}
		return sum;
	}
	


	public double removeAllTaxes(SuperProduct entity)
	{
		double vat = 0,service = 0;
		double tax=0,retrVat=0,retrServ=0;
		if(entity instanceof ServiceProduct)
		{
			ServiceProduct prod=(ServiceProduct) entity;
			if(prod.getServiceTax()!=null&&prod.getServiceTax().isInclusive()==true){
				service=prod.getServiceTax().getPercentage();
			}
			if(prod.getVatTax()!=null&&prod.getVatTax().isInclusive()==true){
				vat=prod.getVatTax().getPercentage();
			}
		}

		if(entity instanceof ItemProduct)
		{
			ItemProduct prod=(ItemProduct) entity;
			if(prod.getVatTax()!=null&&prod.getVatTax().isInclusive()==true){
				vat=prod.getVatTax().getPercentage();
			}
			if(prod.getServiceTax()!=null&&prod.getServiceTax().isInclusive()==true){
				service=prod.getServiceTax().getPercentage();
			}
		}
		
		if(vat!=0&&service==0){
			retrVat=entity.getPrice()/(1+(vat/100));
			retrVat=entity.getPrice()-retrVat;
		}
		if(service!=0&&vat==0){
			retrServ=entity.getPrice()/(1+service/100);
			retrServ=entity.getPrice()-retrServ;
		}
		if(service!=0&&vat!=0){
			// Here if both are inclusive then first remove service tax and then on that amount
			// calculate vat.
			double removeServiceTax=(entity.getPrice()/(1+service/100));
						
			//double taxPerc=service+vat;
			//retrServ=(entity.getPrice()/(1+taxPerc/100)); //line changed below
			retrServ=(removeServiceTax/(1+vat/100));
			retrServ=entity.getPrice()-retrServ;
		}
		tax=retrVat+retrServ;
		System.out.println("Returning Tax"+tax);
		return tax;
	}
	
	
		public boolean removeChargesOnDelete(){
		
		int tableSize=getDataprovider().getList().size();
		System.out.println("Printing Here The Table Size"+tableSize);
		
		if(tableSize<1){
			return false;
		}
		return true;
	}
		
		
		@Override
		public void setEnable(boolean state) {
			 for(int i=table.getColumnCount()-1;i>-1;i--)
		    	  table.removeColumn(i); 
		        	  
		      
		          if(state==true)
		          {
		        	createColumnprodCategoryColumn();
		      		createColumnprodCodeColumn();
		      		createColumnprodNameColumn();
		      		createColumnprodQtyColumn();
		      		/*
		    		 * 24-06-2017
		    		 * nidhi
		    		 * 
		    		 */
		    		addColumnProductAvailableQty();
		    		/*
		    		 * end
		    		 */
		      		createColumnpriceColumn();
		      		createColumnvatColumn();
		      		createColumnPercDiscount();
		      		createColumnDiscountAmount();
		      		createColumntotalColumn();
		      		
		      		createColumnprodHSNCodeColumn();
		      		
		      		viewWarehouse();
		    		viewstarageLoc();
		    		viewstorageBin();
		    		
		    		createColumnAddColumn();
		    		/**
		    		 * nidhi
		    		 * 21-08-2018
		    		 * for add serial number
		    		 */
		    		if(LoginPresenter.mapModelSerialNoFlag){
		    			createColumnAddProSerialNoColumn();
		    		}
//		      		createColumndeleteColumn();
		      		addFieldUpdater();
		      		}
		 
		          else
		          {
		        	createColumnprodCategoryColumn();
		      		createColumnprodCodeColumn();
		      		createColumnprodNameColumn();
		      		createViewQtyColumn();
		      		/*
		    		 * 24-06-2017
		    		 * nidhi
		    		 * 
		    		 */
		    		addColumnProductAvailableQty();
		    		/*
		    		 * end
		    		 */
		      		createColumnpriceColumn();
		      		createColumnvatColumn();
		      		createColumnPercDiscount();
		      		createColumnDiscountAmount();
		      		createColumntotalColumn();
		      		
		      		createColumnprodHSNCodeColumn();
		      		
		      		viewWarehouse();
		    		viewstarageLoc();
		    		viewstorageBin();
		    		/**
		    		 * nidhi
		    		 * 21-08-2018
		    		 * for add serial number
		    		 */
		    		if(LoginPresenter.mapModelSerialNoFlag){
		    			createViewColumnAddProSerialNoColumn();
		    		}
		    		addFieldUpdater();
		          }
		}
	
		public void createColumnAddProSerialNoColumn() {
			ButtonCell btnCell = new ButtonCell();
			addProSerialNo = new Column<DeliveryLineItems, String>(btnCell) {
				@Override
				public String getValue(DeliveryLineItems object) {
					return "Product Serial Nos";
				}
			};
			table.addColumn(addProSerialNo, "");
			table.setColumnWidth(addProSerialNo,180,Unit.PX);

		}
		public void createFieldUpdaterAddSerialNo() {
			addProSerialNo.setFieldUpdater(new FieldUpdater<DeliveryLineItems, String>() {
				@Override
				public void update(final int index, final DeliveryLineItems object, String value) {
					InventoryLocationPopUp.initialzeWarehouse(object.getProdId());
					 Timer timer=new Timer() 
		        	 {
		 				@Override
		 				public void run() {
		 					
		 					rowIndex = index;
							prodSerialPopup.setProSerialenble(true,true);
							prodSerialPopup.getPopup().setSize("50%", "40%");
							
							proSerialList.clear();
							if(object.getWareHouseName()!=null && object.getStorageBinName()!=null  && object.getStorageLocName()!=null
									&& object.getWareHouseName().length()>0 && object.getStorageBinName().length()>0  && object.getStorageLocName().length()>0){
								ArrayList<DeliveryLineItems> list=new ArrayList<DeliveryLineItems>();
								/**
								 * nidhi
								 * 23-08-2018
								 */
								
								for(StorageLocationBin strBIn :InventoryLocationPopUp.strLocStrBin){
									System.out.println("wr = "+object.getWareHouseName() 
											+" bin =  " +object.getStorageBinName()
											+ " lc = " +object.getStorageLocName());
									if(strBIn.getWarehouseName().equals(object.getWareHouseName()) 
											&& strBIn.getStorageLocation().equals(object.getStorageLocName())
											&& strBIn.getStorageBin().equals(object.getStorageBinName()))
									{
										
										ArrayList<ProductSerialNoMapping> prMp= new ArrayList<ProductSerialNoMapping>();
										if(strBIn.getProdSerialNoList().get(0)!=null && strBIn.getProdSerialNoList().get(0).size()>0){
											
											for(ProductSerialNoMapping prmain : strBIn.getProdSerialNoList().get(0)){
												ProductSerialNoMapping pr1 = new ProductSerialNoMapping();
												pr1.setAvailableStatus(prmain.isAvailableStatus());
												pr1.setNewAddNo(prmain.isNewAddNo());
												pr1.setStatus(prmain.isStatus());
												pr1.setProSerialNo(prmain.getProSerialNo());
												prMp.add(pr1);
											}
											
										}
										proSerialList.put(0, prMp);
										System.out.println("get size -- " + proSerialList.get(0).toArray().toString());
//										proSerialList.putAll(strBIn.getProdSerialNoList());
										list.addAll(getDataprovider().getList());
										ArrayList<ProductSerialNoMapping> proserMap = list.get(rowIndex).getProSerialNoDetails().get(0);
									}
								
								}
							}
							
								
								if(object.getProSerialNoDetails()!=null &&
										object.getProSerialNoDetails().size()>0 || proSerialList.size()>0){
									
									ArrayList<ProductSerialNoMapping> serNo = new ArrayList<ProductSerialNoMapping>();
									System.out.println("size  -- " + serNo.size());
									if(object.getProSerialNoDetails().containsKey(0)){
										serNo.addAll(object.getProSerialNoDetails().get(0));
									}
									if(!proSerialList.containsKey(0)  || proSerialList.keySet().size() == 0 || proSerialList.get(0) == null){
										proSerialList.put(0,new ArrayList<ProductSerialNoMapping>());
									}else{
										System.out.println("pro called -- " + proSerialList.keySet());
									}
									int countpr = 0 ;
									if(proSerialList.get(0) != null){
										 countpr = proSerialList.get(0).size();
									}
									
									if(serNo.size()>0 || (proSerialList.size()>0)){
										boolean get= false;
										
										for(int i=0;i<serNo.size();i++){
											get =false;
											int j=0;
											if(proSerialList.get(0) !=null){
												for(j=0;j<proSerialList.get(0).size();j++){
													if(proSerialList.get(0).get(j).getProSerialNo().equals(serNo.get(i).getProSerialNo())){
														proSerialList.get(0).get(j).setAvailableStatus(true);
														proSerialList.get(0).get(j).setStatus(true);
														get= true;
														break;
													}
												}
											}
											
											if(!get){
												ProductSerialNoMapping prodt = new ProductSerialNoMapping();
												prodt.setProSerialNo(serNo.get(i).getProSerialNo());
												prodt.setAvailableStatus(false);
												prodt.setStatus(true);
												proSerialList.get(0).add(prodt);
											}
										}
										
									}
									if(countpr< object.getQuantity()){
										
										if(!proSerialList.containsKey(0) || proSerialList.keySet().size() == 0 || proSerialList.get(0) == null){
											proSerialList.put(0,new ArrayList<ProductSerialNoMapping>());
										}else{
											System.out.println("pro called -- " + proSerialList.keySet());
										}
										
											int count = (int) (object.getQuantity()- countpr);
											for(int i = 0 ; i < count ; i ++ ){
												ProductSerialNoMapping pro = new ProductSerialNoMapping();
												pro.setAvailableStatus(false);
												pro.setStatus(false);
												proSerialList.get(0).add(pro);
											}
											prodSerialPopup.getProSerNoTable().getDataproviderlist().addAll(proSerialList.get(0));
										
										
									}
									
								}
								if(proSerialList.size()==0){
									if(!proSerialList.containsKey(0)){
										proSerialList.put(0, new ArrayList<ProductSerialNoMapping>());
									}
									
									ArrayList<ProductSerialNoMapping> serNo = new ArrayList<ProductSerialNoMapping>();
									for(int i = 0 ; i < object.getQuantity(); i ++ ){
										ProductSerialNoMapping pro = new ProductSerialNoMapping();
										pro.setProSerialNo("");
										pro.setAvailableStatus(false);
										pro.setNewAddNo(true);
										serNo.add(pro);
									}
									proSerialList.put(0, serNo);
								}
								System.out.println("get ser no list  -- " +proSerialList.get(0).toString());
								prodSerialPopup.getProSerNoTable().getDataprovider().getList().clear();
								prodSerialPopup.getProSerNoTable().getDataprovider().getList().addAll(proSerialList.get(0));
								prodSerialPopup.getProSerNoTable().getTable().redraw();
							prodSerialPopup.showPopUp();
						
						}
		        	 };
		        	 timer.schedule(2000);
		        	 
				
				}
			});
		}
		
		public void createFieldUpdaterViewSerialNo() {
			viewProSerialNo.setFieldUpdater(new FieldUpdater<DeliveryLineItems, String>() {
				@Override
				public void update(final int index, final DeliveryLineItems object, String value) {
					InventoryLocationPopUp.initialzeWarehouse(object.getProdId());
					 Timer timer=new Timer() 
		        	 {
		 				@Override
		 				public void run() {
		 					prodSerialPopup.setProSerialenble(false,false);
							prodSerialPopup.getPopup().setSize("50%", "40%");
							rowIndex  = index;
							
							if(object.getWareHouseName()!=null && object.getStorageBinName()!=null  && object.getStorageLocName()!=null
									&& object.getWareHouseName().length()>0 && object.getStorageBinName().length()>0  && object.getStorageLocName().length()>0){
								ArrayList<DeliveryLineItems> list=new ArrayList<DeliveryLineItems>();
								/**
								 * nidhi
								 * 23-08-2018
								 */
								for(StorageLocationBin strBIn :InventoryLocationPopUp.strLocStrBin){
									System.out.println("wr = "+object.getWareHouseName() 
											+" bin =  " +object.getStorageBinName()
											+ " lc = " +object.getStorageLocName());
									if(strBIn.getWarehouseName().equals(object.getWareHouseName()) 
											&& strBIn.getStorageLocation().equals(object.getStorageLocName())
											&& strBIn.getStorageBin().equals(object.getStorageBinName()))
									{
										proSerialList.clear();
										proSerialList.putAll(strBIn.getProdSerialNoList());
										list.addAll(getDataprovider().getList());
										ArrayList<ProductSerialNoMapping> proserMap = list.get(rowIndex).getProSerialNoDetails().get(0);
									}
								}
							}
							if(object.getProSerialNoDetails().size()>0){
								
								ArrayList<ProductSerialNoMapping> serNo = new ArrayList<ProductSerialNoMapping>();
								System.out.println("size  -- " + serNo.size());
								
								if(object.getProSerialNoDetails().containsKey(0)){
									serNo.addAll(object.getProSerialNoDetails().get(0));
								}
								
								if(proSerialList != null && proSerialList.get(0)!=null && proSerialList.get(0).size()>0 && serNo.size()>0){
									boolean get= false;
									for(int i=0;i<serNo.size();i++){
										get =false;
										int j=0;
										for(j=0;j<proSerialList.get(0).size();j++){
											if(proSerialList.get(0).get(j).getProSerialNo().equals(serNo.get(i).getProSerialNo())){
												serNo.get(j).setAvailableStatus(true);
												serNo.get(j).setStatus(true);
												get= true;
												break;
											}
										}
										if(!get){
											serNo.get(i).setAvailableStatus(false);
											serNo.get(i).setStatus(true);
										}
									}
									
								}
								
								prodSerialPopup.getProSerNoTable().getDataprovider().getList().clear();
								prodSerialPopup.getProSerNoTable().getDataprovider().getList().addAll(serNo);
								prodSerialPopup.getProSerNoTable().getTable().redraw();
							}
							prodSerialPopup.showPopUp();
		 				}
		        	 };
			          timer.schedule(1500); 
					
				
				}
			});
		}
		
		public void createViewColumnAddProSerialNoColumn() {
			ButtonCell btnCell = new ButtonCell();
			viewProSerialNo = new Column<DeliveryLineItems, String>(btnCell) {
				@Override
				public String getValue(DeliveryLineItems object) {
					return "View Product Serial Nos";
				}
			};
			table.addColumn(viewProSerialNo, "");
			table.setColumnWidth(viewProSerialNo,180,Unit.PX);

		}



}
