package com.slicktechnologies.client.views.deliverynote;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;

import com.gargoylesoftware.htmlunit.WebConsole.Logger;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.approvalutility.ApprovableFormScreenPresenter;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.reusabledata.ConditionDialogBox;
import com.slicktechnologies.client.services.CommonService;
import com.slicktechnologies.client.services.CommonServiceAsync;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.EmailService;
import com.slicktechnologies.client.services.EmailServiceAsync;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.ApproverFactory;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.documentcancellation.DocumentCancellationPopUp;
import com.slicktechnologies.client.views.inventory.recievingnote.ProductQty;
import com.slicktechnologies.client.views.salesorder.SalesOrderForm;
import com.slicktechnologies.client.views.salesorder.SalesOrderPresenter;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.common.businessprocesslayer.ConcreteBusinessProcess;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.deliverynote.DeliveryLineItems;
import com.slicktechnologies.shared.common.deliverynote.DeliveryNote;
import com.slicktechnologies.shared.common.deliverynote.ShippingSteps;
import com.slicktechnologies.shared.common.inventory.MaterialMovementNote;
import com.slicktechnologies.shared.common.inventory.MaterialProduct;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.shippingpackingprocess.CheckListType;
import com.slicktechnologies.shared.common.shippingpackingprocess.PackingMethod;
import com.slicktechnologies.shared.common.shippingpackingprocess.ShippingMethod;

public class DeliveryNotePresenter extends ApprovableFormScreenPresenter<DeliveryNote> implements RowCountChangeEvent.Handler, ChangeHandler {
	
	public static DeliveryNoteForm form;
	EmailServiceAsync emailService = GWT.create(EmailService.class);
	final CsvServiceAsync csvservice = GWT.create(CsvService.class);
	final static GenricServiceAsync genasync=GWT.create(GenricService.class);
	NumberFormat nf=NumberFormat.getFormat("0.00");
	String retrCompnyState=retrieveCompanyState();
	static String companyStateVal="";
//	List<SalesLineItem> prodLis=retrieveProdLis();
//	static List<SalesLineItem> origProdLis=null;
	int rowIndex=0;
	
	int cnt=0;
	ConditionDialogBox conditionPopup=new ConditionDialogBox("Do you want to print on preprinted Stationery",AppConstants.YES,AppConstants.NO);
	
	static ArrayList<ProductQty> salesOrderList;
	static ArrayList<ProductQty> deliveryList;
	
	DocumentCancellationPopUp popup = new DocumentCancellationPopUp();
	PopupPanel panel , preprintPanel;
	
	CommonServiceAsync commonservice = GWT.create(CommonService.class);
	
	public DeliveryNotePresenter(FormScreen<DeliveryNote> view, DeliveryNote model) {
		super(view, model);
		form = (DeliveryNoteForm) view;
		form.setPresenter(this);
		form.getDeliveryLineItem().getTable().addRowCountChangeHandler(this);
		form.getOlbShippingMethod().addChangeHandler(this);
		form.getOlbPackingMethod().addChangeHandler(this);
		form.getOlbshippingchecklist().addChangeHandler(this);
		form.getOlbpackingchecklist().addChangeHandler(this);
		form.addShipping.addClickHandler(this);
		form.addPacking.addClickHandler(this);
		popup.getBtnOk().addClickHandler(this);
		popup.getBtnCancel().addClickHandler(this);
		
		conditionPopup.getBtnOne().addClickHandler(this);
		conditionPopup.getBtnTwo().addClickHandler(this);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.DELIVERYNOTE,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}
	
	public static void showMessage(String msg){
		form.showDialogMessage(msg);
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel label = (InlineLabel) e.getSource();
		String text = label.getText().trim();

		if (text.equals(AppConstants.NEW)) {
			if(form.isPopUpAppMenubar()){
				Console.log("Delivery Note POPUP : New clicked!");
				return;
			}
			reactToNew();
		}
		if(text.equals(AppConstants.CANCLEDELIVERYNOTE)){
			
			popup.getPaymentDate().setValue(new Date()+"");
			popup.getPaymentID().setValue(form.getIbDeliveryNoteId().getValue().trim());
			popup.getPaymentStatus().setValue(form.getstatustextbox().getValue().trim());
			panel=new PopupPanel(true);
			panel.add(popup);
			panel.show();
			panel.center();
			
//			reactToCancel();		
		}
		if(text.equals("Mark Completed")){
			form.showWaitSymbol();
			if(model.getSalesOrderCount()!=null&&model.getSalesOrderCount()!=0){
				updateDeliveryStatus();
			}else{
				reactOnCompleted();
			}
		}
		if(text.equals(ManageApprovals.APPROVALREQUEST)){
//			reactOnApprRequest();
			if(form.validate()){
				reactOnApprRequest();
			}
		}
		
		if(text.equals(ManageApprovals.CANCELAPPROVALREQUEST)){
//			reactOnCancelRequest();
			form.getManageapproval().reactToApprovalRequestCancel();
		}
		
		if(text.equals("Email")){
			reactOnEmail();
		}
		/**
		 * Date :01-08-2017 BY ANIL
		 */
		if(text.equals(ManageApprovals.SUBMIT)){
			
			if(form.validate()){
				form.getManageapproval().reactToSubmit();
			}
		}
		
		if (text.equals("View Sales Order")) {
			if(form.isPopUpAppMenubar()){
				Console.log("Delivery Note POPUP : View Sales Order clicked!");
				return;
			}
			reactOnViewSalesOrder();
		}
			

	}

	@Override
	public void reactOnPrint() {
		
		MyQuerry querry = new MyQuerry();
	  	Company c = new Company();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
	  	filter = new Filter();
	  	filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("processName");
		filter.setStringValue("DeliveryNote");
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("processList.status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new ProcessConfiguration());
		
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onFailure(Throwable caught) {
			}			

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				System.out.println(" result set size +++++++"+result.size());
				
				List<ProcessTypeDetails> processList =new ArrayList<ProcessTypeDetails>();
				
				if(result.size()==0){
					
					final String url = GWT.getModuleBaseURL() + "deliverynotepdf"+"?Id="+model.getId()+"&"+"preprint="+"plane"+"&"+"type=DeliveryNote";
					Window.open(url, "test", "enabled");
					
				}/**
				 * Add by jayshree date 18-7-2018 
				 */
				else if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("DeliveryNote", "POPDFV1"))
				{
					final String url = GWT.getModuleBaseURL() + "deliverynotepdf"+"?Id="+model.getId()+"&"+"preprint="+"plane"+"&"+"type=DeliveryNote"+"&"+"subtype=DELIVERYNOTEV1";
					Window.open(url, "test", "enabled");
				}/**
				 * Add by jayshree date 30-11-2018
				 */
				else if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("DeliveryNote", "DeliveryNoteUpdatedpdf"))
				{
					final String url = GWT.getModuleBaseURL() + "deliverynotepdf"+"?Id="+model.getId()+"&"+"preprint="+"plane"+"&"+"type=DeliveryNote"+"&"+"subtype=DeliveryNoteUpdated";
					Window.open(url, "test", "enabled");
				}
				else{
				
					for(SuperModel model:result)
					{
						ProcessConfiguration processConfig=(ProcessConfiguration)model;
						processList.addAll(processConfig.getProcessList());
						
					}
				
				for(int k=0;k<processList.size();k++){	
				if(processList.get(k).getProcessType().trim().equalsIgnoreCase("CompanyAsLetterHead")&&processList.get(k).isStatus()==true){
					
					cnt=cnt+1;
				
				}
				}

				if(cnt>0){
					System.out.println("in side react on prinnt cnt");
					preprintPanel=new PopupPanel(true);
					preprintPanel.add(conditionPopup);
					preprintPanel.setGlassEnabled(true);
					preprintPanel.show();
					preprintPanel.center();
				}
				else
				{
					final String url = GWT.getModuleBaseURL() + "deliverynotepdf"+"?Id="+model.getId()+"&"+"preprint="+"plane"+"&"+"type=DeliveryNote";;
					Window.open(url, "test", "enabled");
				}
				
			}
			}
		});
		
	}
	
	
	@Override
	public void reactOnDownload() {
		ArrayList<DeliveryNote> noterarray=new ArrayList<DeliveryNote>();
		List<DeliveryNote> list=(List<DeliveryNote>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
		
		noterarray.addAll(list);
		
		csvservice.setdeliverynotelist(noterarray, new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);
				Console.log("RPC call Failed"+caught);
			}

			@Override
			public void onSuccess(Void result) {
				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "csvservlet"+"?type="+62;
				Window.open(url, "test", "enabled");
			}
		});
	}
	

	public void reactOnEmail()
	{
		boolean conf = Window.confirm("Do you really want to send email?");
		if (conf == true) {
		emailService.initiateDeliveryNoteEmail(model,new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Unable To Send Email");
				caught.printStackTrace();
			}

			@Override
			public void onSuccess(Void result) {
				Window.alert("Email Sent Sucessfully !");
			}
		});
		}
	
	
	}

	/**
	 * Method token to make new model
	 */
	@Override
	protected void makeNewModel() {
		model=new DeliveryNote();
	}

	public static DeliveryNoteForm initalize()
	{
				DeliveryNoteForm form=new  DeliveryNoteForm();
				DeliveryNotePresenterTable gentable=new DeliveryNotePresenterTableProxy();
				gentable.setView(form);
				gentable.applySelectionModle();
				DeliveryNotePresenterSearch.staticSuperTable=gentable;
				DeliveryNotePresenterSearch searchpopup=new DeliveryNotePresenterSearchProxy();
				form.setSearchpopupscreen(searchpopup);
				DeliveryNotePresenter  presenter=new DeliveryNotePresenter(form,new DeliveryNote());
				AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Sales/Delivery Note",Screen.DELIVERYNOTE);
				AppMemory.getAppMemory().stickPnel(form);
		return form;
	}

	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.deliverynote.DeliveryNote")
	public static  class DeliveryNotePresenterSearch extends SearchPopUpScreen<DeliveryNote>{

		@Override
		public MyQuerry getQuerry() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public boolean validate() {
			// TODO Auto-generated method stub
			return true;
		}};

		@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.deliverynote.DeliveryNote")
		public static class DeliveryNotePresenterTable extends SuperTable<DeliveryNote> implements GeneratedVariableRefrence{

			@Override
			public Object getVarRef(String varName) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void createTable() {
				// TODO Auto-generated method stub

			}

			@Override
			protected void initializekeyprovider() {
				// TODO Auto-generated method stub

			}

			@Override
			public void addFieldUpdater() {
				// TODO Auto-generated method stub

			}

			@Override
			public void setEnable(boolean state) {
				// TODO Auto-generated method stub

			}

			@Override
			public void applyStyle() {
				// TODO Auto-generated method stub

			}} ;
			
			
			private void reactToCancel()
			{
			}
			
			private void reactOnCompleted()
			{
				
					
					model.setStatus(DeliveryNote.DELIVERYCOMPLETED);
					model.setCompletionDate(new Date());
					genasync.save(model,new AsyncCallback<ReturnFromServer>() {

						@Override
						public void onFailure(Throwable caught) {
							form.showDialogMessage("An Unexpected Error occured ! "+caught.getMessage());
							form.hideWaitSymbol();
						}

						@Override
						public void onSuccess(ReturnFromServer result) {
							form.hideWaitSymbol();
							model.setStatus(DeliveryNote.DELIVERYCOMPLETED);
							form.showDialogMessage("Delivery Successfully Completed!");
							form.setMenuAsPerStatus();
							form.setToViewState();
							form.getTbStatus().setText(DeliveryNote.DELIVERYCOMPLETED);
							form.getDbcompletiondate().setValue(model.getCompletionDate());
							
							/**
							 * @author Anil
							 * @since 21-07-2020
							 */
							if(model.getDeliveryStatus()!=null){
								form.tbDeliveryStatus.setValue(model.getDeliveryStatus());
							}
							/**
							 * @author Vijay Date :- 02-11-2020
							 * Des :- below stock deduction logic moved to mark completion
							 * when delivery note mark completed then its stock will deducted
							 * requirement raised by Rahul Tiwari
							 * 
							 */
							commonservice.updateStock(model, new AsyncCallback<String>() {
								
								@Override
								public void onSuccess(String result) {
									// TODO Auto-generated method stub
									
								}
								
								@Override
								public void onFailure(Throwable caught) {
									// TODO Auto-generated method stub
									form.showDialogMessage("Stock deduction failed please contact EVA Support");
								}
							});
						}
					});
			}
			
			private boolean validateShipTable()
			{
				List<ShippingSteps> shiplis=form.getShippingListTable().getDataprovider().getList();
				for(int i=0;i<shiplis.size();i++)
				{
					if(shiplis.get(i).getShippingStepMandatory().trim().equals(AppConstants.YES)&&shiplis.get(i).isCompletionStatus()==false){
						
						return false;
					}
				}
				return true;
			}
			private boolean validatePackTable()
			{
				List<ShippingSteps> packlis=form.getPackingListTable().getDataprovider().getList();
				for(int i=0;i<packlis.size();i++)
				{
					if(packlis.get(i).getShippingStepMandatory().trim().equals(AppConstants.YES)&&packlis.get(i).isCompletionStatus()==false){
						
						return false;
					}
				}
				return true;
			}
			
			private void reactToNew()
			{
				form.setToNewState();
				initalize();
			}
			
			
			@Override
			public void onRowCountChange(RowCountChangeEvent event) 
			{
				if(event.getSource().equals(form.getDeliveryLineItem().getTable()))
				{
					this.productsOnChange();
				}
			}
			
			
			private double fillNetPayable(double amtincltax)
			{
				double amtval=0;
				if(form.getChargesTable().getDataprovider().getList().size()==0)
				{
					amtval=amtincltax;
				}
				if(form.getChargesTable().getDataprovider().getList().size()!=0)
				{
					amtval=amtincltax+form.getChargesTable().calculateNetPayable();
				}
				amtval=Math.round(amtval);
				int retAmtVal=(int) amtval;
				amtval=retAmtVal;
				return amtval;
			}
			
			private void productsOnChange()
			{
				form.showWaitSymbol();
				Timer timer=new Timer() 
			     {
					@Override
					public void run() 
					{
				    double totalExcludingTax=form.getDeliveryLineItem().calculateTotalExcludingTax();
				    totalExcludingTax=Double.parseDouble(nf.format(totalExcludingTax));
				    form.getDototalamt().setValue(totalExcludingTax);
				    
					boolean chkSize=form.getDeliveryLineItem().removeChargesOnDelete();
					if(chkSize==false){
						form.getChargesTable().connectToLocal();
					}
					
					try {
						form.prodTaxTable.connectToLocal();
						form.addProdTaxes();
						double totalIncludingTax=form.getDototalamt().getValue()+form.getProdTaxTable().calculateTotalTaxes();
						form.getDoamtincltax().setValue(Double.parseDouble(nf.format(totalIncludingTax)));
						form.updateChargesTable();
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					double netPay=fillNetPayable(form.getDoamtincltax().getValue());
					netPay=Math.round(netPay);
					int netPayable=(int) netPay;
					netPay=netPayable;
					form.getDonetpayamt().setValue(Double.parseDouble(nf.format(netPay)));
					
					//form.getTotalQuantity();
					
					form.hideWaitSymbol();
					}
		    	 };
		    	 timer.schedule(3000);
		    
			}

			
			public String retrieveCompanyState()
			{
				final MyQuerry querry=new MyQuerry();
				Filter tempfilter=new Filter();
				tempfilter.setQuerryString("companyId");
				tempfilter.setLongValue(model.getCompanyId());
				querry.getFilters().add(tempfilter);
				querry.setQuerryObject(new Company());
				Timer timer=new Timer() 
			    {
					@Override
					public void run() 
					{
					genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
						
						@Override
						public void onFailure(Throwable caught) {
							form.showDialogMessage("An Unexpected error occurred!");
						}
			
						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							for(SuperModel model:result)
							{
								Company compentity = (Company)model;
								retrCompnyState=compentity.getAddress().getState();
								System.out.println("Retrieved "+retrCompnyState);
							}
							companyStateVal=retrCompnyState.trim();
							System.out.println("sysouStatc"+companyStateVal);
							}
						 });
					}
		    	 };
		    	 timer.schedule(3000);
		    	 
		    	return retrCompnyState;
			}

			/**
			 * This method is used for verifying whether the name of the state where delivery is to be done
			 * is same as that of company state.
			 * @return it returns true if company state is same as that of customer and false if it is different.
			 */
			public boolean checkCompanyState()
			{
				//String companyState=retrieveCompanyState().trim();
				System.out.println("Company State"+retrCompnyState);
				
				String custState="";
				if(form.getAcshippingcomposite().getState().getSelectedIndex()!=0){
					custState=form.getAcshippingcomposite().getState().getValue().trim();
					System.out.println("Customer State"+custState);
				}
				if(!custState.equals("")&&custState.trim().equals(retrCompnyState.trim())&&!retrCompnyState.equals("")){
					return true;
				}
				return false;
			}

			public void createDeliveryNote(List<DeliveryLineItems> updList)
			{
				System.out.println("Entity Enter Here");
				DeliveryNote deliveryEntity=new DeliveryNote();
				deliveryEntity.setCompanyId(model.getCompanyId());
				if(model.getLeadCount()!=0){
					deliveryEntity.setLeadCount(model.getLeadCount());
				}
				if(model.getQuotationCount()!=0){
					deliveryEntity.setQuotationCount(model.getQuotationCount());
				}
				if(model.getSalesOrderCount()!=0){
					deliveryEntity.setSalesOrderCount(model.getSalesOrderCount());
				}
				if(model.getReferenceNumber()!=null){
					deliveryEntity.setReferenceNumber(model.getReferenceNumber());
				}
				if(model.getReferenceDate()!=null){
					deliveryEntity.setReferenceDate(model.getReferenceDate());
				}
				if(model.getEmployee()!=null){
					deliveryEntity.setEmployee(model.getEmployee());
				}
				if(model.getCformStatus()!=null){
					deliveryEntity.setCformStatus(model.getCformStatus());
				}
				if(model.getCstPercent()!=0){
					deliveryEntity.setCstPercent(model.getCstPercent());
				}
				if(model.getDeliveryDocument()!=null){
					deliveryEntity.setDeliveryDocument(model.getDeliveryDocument());
				}
				deliveryEntity.setStatus(ConcreteBusinessProcess.CREATED);
				if(model.getBranch()!=null){
					deliveryEntity.setBranch(model.getBranch());
				}
				if(model.getCategory()!=null){
					deliveryEntity.setCategory(model.getCategory());
				}
				if(model.getType()!=null){
					deliveryEntity.setType(model.getType());
				}
				if(model.getGroup()!=null){
					deliveryEntity.setGroup(model.getGroup());
				}
				if(model.getDeliveryDate()!=null){
					deliveryEntity.setDeliveryDate(model.getDeliveryDate());
				}
				if(model.getShippingAddress()!=null){
					deliveryEntity.setShippingAddress(model.getShippingAddress());
				}
				if(model.getDeliveryItems().size()!=0){
					deliveryEntity.setDeliveryItems(updList);
				}
				if(model.getShippingTypeName()!=null){
					deliveryEntity.setShippingTypeName(model.getShippingTypeName());
				}
				if(model.getShipCheckList()!=null){
					deliveryEntity.setShipCheckList(model.getShipCheckList());
				}
				if(model.getPackingTypeName()!=null){
					deliveryEntity.setPackingTypeName(model.getPackingTypeName());
				}
				if(model.getPackCheckList()!=null){
					deliveryEntity.setPackCheckList(model.getPackCheckList());
				}
				
				if(model.getCinfo().getCount()!=0){
					deliveryEntity.getCinfo().setCount(model.getCinfo().getCount());
				}
				if(model.getCinfo().getFullName()!=null){
					deliveryEntity.getCinfo().setFullName(model.getCinfo().getFullName());
				}
				if(model.getCinfo().getCellNumber()!=0){
					deliveryEntity.getCinfo().setCellNumber(model.getCinfo().getCellNumber());
				}
				
				if(model.getApproverName()!=null){
					deliveryEntity.setApproverName(model.getApproverName());
				}
				if(model.getPackingStepsLis().size()!=0){
					deliveryEntity.setPackingStepsLis(model.getPackingStepsLis());
				}
				
				if(model.getProdChargeLis().size()!=0){
					deliveryEntity.setProdChargeLis(model.getProdChargeLis());
				}
				if(model.getProdTaxesLis().size()!=0){
					deliveryEntity.setProdTaxesLis(model.getProdTaxesLis());
				}
				if(model.getTotalAmount()!=0){
					deliveryEntity.setTotalAmount(model.getTotalAmount());
				}
				
				if(model.getShippingStepsLis().size()!=0){
					deliveryEntity.setShippingStepsLis(model.getShippingStepsLis());
				}
				
				
				
				genasync.save(deliveryEntity,new AsyncCallback<ReturnFromServer>() {

					@Override
					public void onFailure(Throwable caught) {
						System.out.println("Unsuccessful");
					}

					@Override
					public void onSuccess(ReturnFromServer result) {
						System.out.println("Inserted");
					}
				});
				
			}
			
			
			@Override
			public void onChange(ChangeEvent event) {
				if(event.getSource().equals(form.getOlbShippingMethod())){
					if(form.getOlbShippingMethod().getSelectedIndex()!=0){
						form.getOlbshippingchecklist().clear();
						form.getOlbshippingchecklist().addItem("SELECT");
						ShippingMethod shipEntity=form.getOlbShippingMethod().getSelectedItem();
						if(shipEntity.getCheckListName()!=null){
							form.getOlbshippingchecklist().setEnabled(true);
							
				 			form.getOlbshippingchecklist().addItem(shipEntity.getCheckListName());
						}
					}
					
					if(form.getOlbShippingMethod().getSelectedIndex()==0){
//						form.getOlbshippingchecklist().clear();
						
						form.getOlbshippingchecklist().setEnabled(false);
						form.getOlbshippingchecklist().setSelectedIndex(0);
					}
				}
				
				if(event.getSource().equals(form.getOlbshippingchecklist())){
					if(form.getOlbshippingchecklist().getSelectedIndex()!=0){
						form.addShipping.setEnabled(true);
					}
				}
				
				if(event.getSource().equals(form.getOlbPackingMethod())){
					if(form.getOlbPackingMethod().getSelectedIndex()!=0){
						form.getOlbpackingchecklist().clear();
						form.getOlbpackingchecklist().addItem("SELECT");
						PackingMethod packEntity=form.getOlbPackingMethod().getSelectedItem();
						if(packEntity.getCheckListName()!=null){
							form.getOlbpackingchecklist().setEnabled(true);
							
							form.getOlbpackingchecklist().addItem(packEntity.getCheckListName());
						}
					}
					
					if(form.getOlbPackingMethod().getSelectedIndex()==0){
//						form.getOlbpackingchecklist().clear();
						form.getOlbpackingchecklist().setEnabled(false);
						form.getOlbpackingchecklist().setSelectedIndex(0);
					}
				}
				
				if(event.getSource().equals(form.getOlbpackingchecklist())){
					if(form.getOlbpackingchecklist().getSelectedIndex()!=0){
						form.addShipping.setEnabled(true);
					}
				}
				
			}
			
			
			
			private void reactOnApprRequest()
			{
				rowIndex=0;
				List<DeliveryLineItems> lisDeliveryProd=form.getDeliveryLineItem().getDataprovider().getList();
				System.out.println("Size In Request"+lisDeliveryProd.size());
				int counter=0;
				for(int i=0;i<lisDeliveryProd.size();i++)
				{
					System.out.println("LOOP");
					if(lisDeliveryProd.get(i).getWareHouseName().trim().equals("")||lisDeliveryProd.get(i).getStorageLocName().trim().equals("")||lisDeliveryProd.get(i).getStorageBinName().trim().equals(""))
					{
						System.out.println("Counter Added");
						counter=counter+1;
					}
				}
				if(counter>0){
					form.showDialogMessage("Please select warehouse information of product!");
				}

				if(!validateShipTable()){
					form.showDialogMessage("Please select all mandatory steps in shipping table!");
				}
				
				if(validateShipTable()&&!validatePackTable()){
					form.showDialogMessage("Please select all mandatory steps in packing table!");
				}
				
				if(validateShipTable()&&validatePackTable()&&counter==0)
				{
					System.out.println("Counter Passes");
					final List<DeliveryLineItems> deliveryLis=form.deliveryLineItem.getDataprovider().getList();
					for(int i=0;i<deliveryLis.size();i++)
					{
						
						MyQuerry querry=new MyQuerry();
						Company compny=new Company();
						Vector<Filter> filtervec=new Vector<Filter>();
						Filter temp=null;
						
						temp=new Filter();
						temp.setLongValue(compny.getCompanyId());
						temp.setQuerryString("companyId");
						filtervec.add(temp);
						
						temp=new Filter();
						temp.setIntValue(deliveryLis.get(i).getProdId());
						temp.setQuerryString("productinfo.prodID");
						filtervec.add(temp);
						
						querry.setFilters(filtervec);
						querry.setQuerryObject(new ProductInventoryView());
						
						
						genasync.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {

	 						@Override
	 						public void onFailure(Throwable caught) {
	 							form.hideWaitSymbol();
	 							form.showDialogMessage("An Unexpected Error occured !");
	 						}
	 						
	 						@Override
	 						public void onSuccess(ArrayList<SuperModel> result) {
	 							int ctr=0;
	 						
	 							ArrayList<ProductInventoryViewDetails> arrInvView=new ArrayList<ProductInventoryViewDetails>();
	 							for(SuperModel bmodel:result)
	 							{
	 								ProductInventoryView pivEntity =(ProductInventoryView)bmodel;
	 								arrInvView.addAll(pivEntity.getDetails());
	 							}
	 							
	 							String whname="",sloc="",sbin="";
	 							double qty=0;
	 							
	 							for(int d=0;d<deliveryLis.size();d++)
	 							{
	 								whname=deliveryLis.get(d).getWareHouseName().trim();
	 								sloc=deliveryLis.get(d).getStorageLocName().trim();
	 								sbin=deliveryLis.get(d).getStorageBinName().trim();
	 								qty=deliveryLis.get(d).getQuantity();
	 								System.out.println("Whname"+whname);
	 								System.out.println("Sloc"+sloc);
	 								System.out.println("Sbin"+sbin);
	 								System.out.println("qty"+qty);
	 								System.out.println("Size Retrievd"+arrInvView.size());
	 								for(int k=0;k<arrInvView.size();k++)
	 								{
	 									
	 									if(arrInvView.get(k).getWarehousename().trim().equals(whname)&&arrInvView.get(k).getStoragelocation().trim().equals(sloc)&&arrInvView.get(k).getStoragebin().trim().equals(sbin))
	 									{
	 										if(arrInvView.get(k).getProdcode().trim().equalsIgnoreCase(deliveryLis.get(d).getProdCode().trim())){
		 										if(arrInvView.get(k).getAvailableqty()<qty)
		 										{
		 											ctr=ctr+1;
		 											form.showDialogMessage("Quantity for "+deliveryLis.get(k).getProdName()+" exceeds available stock!");
		 											break;
		 										}
	 										}
	 									}
	 								}
	 							}
	 							
	 							rowIndex=rowIndex+1;
	 							System.out.println("Row Index"+rowIndex+"    Counter"+ctr);
	 							if(rowIndex==deliveryLis.size()&&ctr==0){
//	 								reactOnReqApproval();
	 								/**
	 								 * nidhi
	 								 * 3-10-2018
	 								 * for parcial delivery note
	 								 */
	 								getValidateStockForOrder(ManageApprovals.APPROVALREQUEST);
	 							}
	 							
	 							}
//	 						}
	 					});
					}
				}  
			}  
			
			
			
			public void getValidateStockForOrder(final String statusupdate){
				boolean flag = false;
				Integer sorder = model.getSalesOrderCount();
				List<String> status= new ArrayList<String>();
				status.add(DeliveryNote.CREATED);
				status.add(DeliveryNote.APPROVED);
				status.add(DeliveryNote.REJECTED);
				status.add(DeliveryNote.CLOSED);
				status.add(DeliveryNote.REQUESTED);
				status.add(DeliveryNote.DELIVERYCOMPLETED);
				

				MyQuerry querry = new MyQuerry();
				Company c=new Company();
				Vector<Filter> filtervec=new Vector<Filter>();
				Filter filter = null;
				
				filter = new Filter();
				filter.setQuerryString("companyId");
				filter.setLongValue(c.getCompanyId());
				filtervec.add(filter);
				
				filter = new Filter();
				filter.setQuerryString("salesOrderCount");
				filter.setIntValue(model.getSalesOrderCount());
				filtervec.add(filter);
				
				filter = new Filter();
				filter.setQuerryString("status IN");
				filter.setList(status);
				filtervec.add(filter);
				
				querry.setFilters(filtervec);
				querry.setQuerryObject(new DeliveryNote());
				
				
				genasync.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void onSuccess(final ArrayList<SuperModel> result) {
						
						MyQuerry querry1 = new MyQuerry();
						Company c=new Company();
						Vector<Filter> filtervec=new Vector<Filter>();
						Filter filter = null;
						
						filter = new Filter();
						filter.setQuerryString("companyId");
						filter.setLongValue(c.getCompanyId());
						filtervec.add(filter);
						
						filter = new Filter();
						filter.setQuerryString("count");
						filter.setIntValue(model.getSalesOrderCount());
						filtervec.add(filter);
						
						querry1.setFilters(filtervec);
						querry1.setQuerryObject(new SalesOrder());
						
						
						genasync.getSearchResult(querry1,new AsyncCallback<ArrayList<SuperModel>>() {

							@Override
							public void onFailure(Throwable caught) {
								// TODO Auto-generated method stub
								
							}

							@Override
							public void onSuccess(ArrayList<SuperModel> result1) {
								// TODO Auto-generated method stub
								SalesOrder srord= (SalesOrder) result1.get(0);
								
								ArrayList<DeliveryNote> dtList = new ArrayList<DeliveryNote>();
								for(SuperModel sr: result){
									DeliveryNote dt = (DeliveryNote) sr;
									dtList.add(dt);
								}
								
								validateProdSerialQty(dtList,srord,statusupdate);
//								dtList.addAll(result);
							}
							
						});
						
					}
					
				});
				
			}
			
			private void reactOnReqApproval()
			{
				/**
				 * Updated By Anil Date : 14-10-2016
				 * Release : 30 Sept 2016 
				 * Project : PURCHASE MODIFICATION(NBHC)
				 */
				String documentCreatedBy="";
				if(model.getCreatedBy()!=null){
					documentCreatedBy=model.getCreatedBy();
				}
				
				ApproverFactory appFactory=new ApproverFactory();
//				Approvals approval=appFactory.getApprovals(model.getEmployee(), model.getApproverName()
//						, model.getBranch(), model.getCount(),documentCreatedBy);
				/**
				 *  nidhi
				 *  7-10-2017
				 *  method change for display name and id
				 */
				Approvals approval=appFactory.getApprovals(model.getEmployee(), model.getApproverName()
						, model.getBranch(), model.getCount(),documentCreatedBy,
						model.getCinfo().getCount()+"",model.getCinfo().getFullName());
				/**
				 * end
				 */
				saveApprovalReq(approval,DeliveryNote.REQUESTED,"Request Sent!");
				
				reactOnApprovalEmail(approval);
			}
			
			private void saveApprovalReq(Approvals approval,final String status,final String dialogmsg)
			{
				form.showWaitSymbol();
				service.save(approval, new AsyncCallback<ReturnFromServer>() {
					
					@Override
					public void onSuccess(ReturnFromServer result) 
					{
						model.setStatus(status);
						service.save(model, new AsyncCallback<ReturnFromServer>() {

							@Override
							public void onFailure(Throwable caught) {
								form.hideWaitSymbol();
							}

							@Override
							public void onSuccess(ReturnFromServer result) {
								form.getTbStatus().setValue(status);
								form.setToViewState();
								form.showDialogMessage(dialogmsg);
								form.hideWaitSymbol();
							}
						});
					}
					
					@Override
					public void onFailure(Throwable caught) {
						form.showDialogMessage("Request Failed! Try Again!");
						form.hideWaitSymbol();
					}
				});
			}
			
			private void reactOnCancelRequest()
			{
		  		ApproverFactory approverfactory=new ApproverFactory();
				  Filter filter=new Filter();
				  filter.setStringValue(approverfactory.getBuisnessProcessType());
				  filter.setQuerryString("businessprocesstype");
				  Filter filterOnid=new Filter();
				  filterOnid.setIntValue(model.getCount());
				  filterOnid.setQuerryString("businessprocessId");
				  Vector<Filter> vecfilter=new Vector<Filter>();
				  vecfilter.add(filter);
				  vecfilter.add(filterOnid);
				  MyQuerry querry=new MyQuerry(vecfilter, new Approvals());
				  final ArrayList<SuperModel> array = new ArrayList<SuperModel>();
				  
				  
				  service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {

					@Override
					public void onFailure(Throwable caught) {
						caught.printStackTrace();
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						array.addAll(result);
						Approvals approval=(Approvals) array.get(0);
						approval.setStatus(Approvals.CANCELLED);
						 
						saveApprovalReq(approval, ConcreteBusinessProcess.CREATED, "Approval Request Cancelled!");
					}
				});
			}
				
			

			
			@Override
			public void onClick(ClickEvent event) {
				super.onClick(event);
				
				
				
				if(event.getSource().equals(conditionPopup.getBtnOne()))
				{
					
					if(cnt >0){
						System.out.println("in side one yes");
						
						final String url = GWT.getModuleBaseURL() + "deliverynotepdf"+"?Id="+model.getId()+"&"+"preprint="+"yes"+"&"+"type=DeliveryNote";;
						Window.open(url, "test", "enabled");
						
						preprintPanel.hide();
					}
				}
				
				
				if(event.getSource().equals(conditionPopup.getBtnTwo()))
				{
					if(cnt >0){
						System.out.println("inside two no");
						
						final String url = GWT.getModuleBaseURL() + "deliverynotepdf"+"?Id="+model.getId()+"&"+"preprint="+"no"+"&"+"type=DeliveryNote";;
						Window.open(url, "test", "enabled");
					    
						preprintPanel.hide();
					}
				}
				
				
				if(event.getSource()==popup.getBtnOk()){
					System.out.println("in on click okkkkkkkk");
					panel.hide();
					System.out.println("pannnnellll hide");
					createNewMMNForSelectedWareHouse();
					createNewDeliveryNote();
				}
				else if(event.getSource()==popup.getBtnCancel()){
					panel.hide();
					popup.remark.setValue("");
				}
				
				if(event.getSource().equals(form.addShipping))
				{
					if(form.getOlbShippingMethod().getSelectedIndex()==0)
					{
						form.showDialogMessage("Please select Shipping Method!");
					}
					if(form.getOlbshippingchecklist().getSelectedIndex()==0&&form.getOlbShippingMethod().getSelectedIndex()!=0)
					{
						form.showDialogMessage("Please select Shipping Checklist!");
					}
					
					List<ShippingSteps> shipStepLis=form.getShippingListTable().getDataprovider().getList();
					System.out.println("Ship Steps Size"+shipStepLis.size());
					
					if(shipStepLis.size()>0)
					{
						form.showDialogMessage("Shipping Steps Already Added!");
					}
					
					if(form.getOlbshippingchecklist().getSelectedIndex()!=0&&form.getOlbShippingMethod().getSelectedIndex()!=0&&shipStepLis.size()==0)
					{
						MyQuerry querry=new MyQuerry();
						Vector<Filter> filtervec=new Vector<Filter>();
						Filter temp=null;
						
						temp=new Filter();
						temp.setQuerryString("companyId");
						temp.setLongValue(model.getCompanyId());
						filtervec.add(temp);
						
						temp=new Filter();
						temp.setStringValue(form.getOlbshippingchecklist().getItemText(form.getOlbshippingchecklist().getSelectedIndex()).trim());
						temp.setQuerryString("checkListName");
						filtervec.add(temp);
						
						temp=new Filter();
						temp.setQuerryString("status");
						temp.setBooleanvalue(true);
						filtervec.add(temp);
						
						querry.setFilters(filtervec);
						querry.setQuerryObject(new CheckListType());
						
						genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
							
							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected error occurred!");
							}
				
							@Override
							public void onSuccess(ArrayList<SuperModel> result) {
								
								if(result.size()>0)
								{
									for(SuperModel model:result){
										CheckListType chkType=(CheckListType)model;
										
										for(int i=0;i<chkType.getStepsInfo().size();i++)
										{
											ShippingSteps shipSteps=new ShippingSteps();
											shipSteps.setShippingStepNumber(chkType.getStepsInfo().get(i).getStepDetailsNumber());
											shipSteps.setShippingStepCode(chkType.getStepsInfo().get(i).getStepDetailsCode());
											shipSteps.setShippingStepName(chkType.getStepsInfo().get(i).getStepDetailsName());
											shipSteps.setShippingStepMandatory(chkType.getStepsInfo().get(i).getStepDetailsMandatory());
											
											form.getShippingListTable().getDataprovider().getList().add(shipSteps);
										}
									}
								}
								
								}
							 });
					}
				}
				
				if(event.getSource().equals(form.addPacking))
				{
					if(form.getOlbPackingMethod().getSelectedIndex()==0)
					{
						form.showDialogMessage("Please select Packing Method!");
					}
					if(form.getOlbpackingchecklist().getSelectedIndex()==0&&form.getOlbPackingMethod().getSelectedIndex()!=0)
					{
						form.showDialogMessage("Please select Packing Checklist!");
					}
					
					List<ShippingSteps> packStepLis=form.getPackingListTable().getDataprovider().getList();
					System.out.println("Pack Steps Size"+packStepLis.size());
					
					if(packStepLis.size()>0)
					{
						form.showDialogMessage("Packing Steps Already Added!");
					}
					
					if(form.getOlbpackingchecklist().getSelectedIndex()!=0&&form.getOlbPackingMethod().getSelectedIndex()!=0&&packStepLis.size()==0)
					{
						MyQuerry querry=new MyQuerry();
						Vector<Filter> filtervec=new Vector<Filter>();
						Filter temp=null;
						
						temp=new Filter();
						temp.setQuerryString("companyId");
						temp.setLongValue(model.getCompanyId());
						filtervec.add(temp);
						
						temp=new Filter();
						temp.setStringValue(form.getOlbpackingchecklist().getItemText(form.getOlbpackingchecklist().getSelectedIndex()).trim());
						temp.setQuerryString("checkListName");
						filtervec.add(temp);
						
						temp=new Filter();
						temp.setQuerryString("status");
						temp.setBooleanvalue(true);
						filtervec.add(temp);
						
						querry.setFilters(filtervec);
						querry.setQuerryObject(new CheckListType());
						
						genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
							
							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected error occurred!");
							}
				
							@Override
							public void onSuccess(ArrayList<SuperModel> result) {
								
								if(result.size()>0)
								{
									for(SuperModel model:result){
										CheckListType chkType=(CheckListType)model;
										
										for(int i=0;i<chkType.getStepsInfo().size();i++)
										{
											ShippingSteps shipSteps=new ShippingSteps();
											shipSteps.setShippingStepNumber(chkType.getStepsInfo().get(i).getStepDetailsNumber());
											shipSteps.setShippingStepCode(chkType.getStepsInfo().get(i).getStepDetailsCode());
											shipSteps.setShippingStepName(chkType.getStepsInfo().get(i).getStepDetailsName());
											shipSteps.setShippingStepMandatory(chkType.getStepsInfo().get(i).getStepDetailsMandatory());
											
											form.getPackingListTable().getDataprovider().getList().add(shipSteps);
										}
									}
								}
								
								}
							 });
					}
				}
				
				
			}
			
			public void reactOnApprovalEmail(Approvals approvalEntity)
			{
				//Quotation Entity patch patch patch
				emailService.initiateApprovalEmail(approvalEntity,new AsyncCallback<Void>() {

					@Override
					public void onFailure(Throwable caught) {
						Window.alert("Resource Quota Ended ");
						caught.printStackTrace();
					}

					@Override
					public void onSuccess(Void result) {
//						Window.alert("Email Sent Sucessfully !");
					}
				});
			}
			
			
			

			
			private void createNewDeliveryNote() 
			{
				 model.setStatus(DeliveryNote.CREATED);
		    	  genasync.save(model,new AsyncCallback<ReturnFromServer>() {
						
						@Override
						public void onFailure(Throwable caught) {
						form.showDialogMessage("An Unexpected Error occured !");
						
						}
						
						@Override
						public void onSuccess(ReturnFromServer result) {
							form.setAppHeaderBarAsPerStatus();
							form.setToViewState();
							form.getTbStatus().setValue(model.getStatus());
			    		}
					});
			}
			
			int mmnIndex=0;
			private void createNewMMNForSelectedWareHouse() {

				final List<DeliveryLineItems> dnlist= new ArrayList<DeliveryLineItems>(); 
				dnlist.addAll(model.getDeliveryItems());
				for(int i=0;i<dnlist.size();i++)
				{
					MyQuerry querry = new MyQuerry();
				  	Company c = new Company();
				  	Vector<Filter> filtervec=new Vector<Filter>();
				  	Filter filter = null;
				  	filter = new Filter();
				  	filter.setQuerryString("companyId");
					filter.setLongValue(c.getCompanyId());
					filtervec.add(filter);
					
					filter = new Filter();
					filter.setQuerryString("warehousename");
					filter.setStringValue(dnlist.get(i).getWareHouseName());
					filtervec.add(filter);
					
					filter = new Filter();
					filter.setQuerryString("storagelocation");
					filter.setStringValue(dnlist.get(i).getStorageLocName());
					filtervec.add(filter);
					
					filter = new Filter();
					filter.setQuerryString("storagebin");
					filter.setStringValue(dnlist.get(i).getStorageBinName());
					filtervec.add(filter);
					
					filter = new Filter();
					filter.setQuerryString("prodid");
					filter.setIntValue(dnlist.get(i).getProdId());
					filtervec.add(filter);
					
					querry.setFilters(filtervec);
					querry.setQuerryObject(new ProductInventoryViewDetails());
					genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
						
						@Override
						public void onFailure(Throwable caught) {
						}
	
						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							double avlQty = 0;
							if(mmnIndex!=0){
								mmnIndex=mmnIndex+1;
							}
							else{
								mmnIndex=0;
							}
							
							for(SuperModel model:result){
							
								ProductInventoryViewDetails prodInvViewEntity=(ProductInventoryViewDetails)model;
								avlQty=prodInvViewEntity.getAvailableqty();
							}
							
							final MaterialMovementNote mmn=new MaterialMovementNote();
							
							mmn.setMmnTitle("MMN Return for DL Note");
							mmn.setMmnDate(new Date());
							mmn.setMmnTransactionType("RETURN");
							mmn.setTransDirection("IN");
						    mmn.setMmnCategory("");
							mmn.setMmnType("");
							
							if(model.getBranch()!=null){
								mmn.setBranch(model.getBranch());
							}
							if(model.getEmployee()!=null){
								mmn.setMmnPersonResposible(model.getEmployee());
							}
							if(model.getApproverName()!=null){
								mmn.setApproverName(model.getApproverName());
							}
							if(model.getEmployee()!=null){
								mmn.setEmployee(model.getEmployee());
							}
							
							mmn.setStatus(MaterialMovementNote.CREATED);
							if(model.getDescription()!=null){
								mmn.setMmnDescription("Ref Delivery Note -: "+model.getCount()+"\n"+model.getDescription());
							}
							
							List<MaterialProduct> mmnlis= new ArrayList<MaterialProduct>();
								
								MaterialProduct mmnentity=new MaterialProduct();
								
								mmnentity.setMaterialProductId(dnlist.get(mmnIndex).getProdId());
								mmnentity.setMaterialProductCode(dnlist.get(mmnIndex).getProdCode());
								mmnentity.setMaterialProductName(dnlist.get(mmnIndex).getProdName());
								mmnentity.setMaterialProductRequiredQuantity(dnlist.get(mmnIndex).getQuantity());
								mmnentity.setMaterialProductAvailableQuantity(avlQty);
								mmnentity.setMaterialProductUOM(dnlist.get(mmnIndex).getUnitOfMeasurement());
								mmnentity.setMaterialProductRemarks("");
								mmnentity.setMaterialProductWarehouse(dnlist.get(mmnIndex).getWareHouseName());
								mmnentity.setMaterialProductStorageLocation(dnlist.get(mmnIndex).getStorageLocName());
								mmnentity.setMaterialProductStorageBin(dnlist.get(mmnIndex).getStorageBinName());
								/**
								 * nidhi
								 * 
								 */
								mmnentity.setProSerialNoDetails(dnlist.get(mmnIndex).getProSerialNoDetails());
								/**
								 * end
								 */
								mmnlis.add(mmnentity);
								mmn.setSubProductTableMmn(mmnlis);
								mmn.setCompanyId(model.getCompanyId());
						
							genasync.save(mmn,new AsyncCallback<ReturnFromServer>() {
									
								@Override
								public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected Error occured !");
								
								}
								
								@Override
								public void onSuccess(ReturnFromServer result) {
									
					    		}
							});
					
						}
					});
			}
				
			}
			
			
			
			public static void checkOrderQuantity(final int salesOrderCount,final int deliveryNoteCount){
				
				
				MyQuerry querry = new MyQuerry();
				Company c=new Company();
				Vector<Filter> filtervec=new Vector<Filter>();
				Filter filter = null;
				
				filter = new Filter();
				filter.setQuerryString("companyId");
				filter.setLongValue(c.getCompanyId());
				filtervec.add(filter);
				
				filter = new Filter();
				filter.setQuerryString("count");
				filter.setIntValue(salesOrderCount);
				filtervec.add(filter);
				
				querry.setFilters(filtervec);
				querry.setQuerryObject(new SalesOrder());
				
				genasync.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onFailure(Throwable caught) {
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						salesOrderList=new ArrayList<ProductQty>();
						for (SuperModel model : result) {
							if(result.size()!=0){
								SalesOrder entity = (SalesOrder) model;
								for(int i=0;i<entity.getItems().size();i++){
									
									ProductQty pq=new ProductQty();
									pq.setProdId(entity.getItems().get(i).getPrduct().getCount());
									pq.setProdQty(entity.getItems().get(i).getQty());
									
									salesOrderList.add(pq);
								}
							}
						}
						
						for(int i=0;i<salesOrderList.size();i++){
							System.out.println("------------------------ Result ----------------------------  ");
							System.out.println("PO List PO ID : "+salesOrderList.get(i).getProdId());
							System.out.println("PO List QTY : "+salesOrderList.get(i).getProdQty());
						}

						
						MyQuerry querey1 = new MyQuerry();
						Company c=new Company();
						System.out.println("Company Id :: "+c.getCompanyId());
						
						Vector<Filter> filtervec=new Vector<Filter>();
						Filter filter = null;
						
						filter = new Filter();
						filter.setQuerryString("companyId");
						filter.setLongValue(c.getCompanyId());
						filtervec.add(filter);
						
						filter = new Filter();
						filter.setQuerryString("salesOrderCount");
						filter.setIntValue(salesOrderCount);
						filtervec.add(filter);
						
						querey1.setFilters(filtervec);
						querey1.setQuerryObject(new DeliveryNote());
						
						genasync.getSearchResult(querey1,new AsyncCallback<ArrayList<SuperModel>>() {
							@Override
							public void onFailure(Throwable caught) {
								System.out.println("On Failure !!!");
							}
				
							@Override
							public void onSuccess(ArrayList<SuperModel> result) {
								deliveryList=new ArrayList<ProductQty>();
								double qty=0;
								boolean flage=false;
								for (SuperModel model1 : result) {
									if(result.size()!=0){
										DeliveryNote entity = (DeliveryNote) model1;
										if(entity.getCount()!=deliveryNoteCount){
											
											System.out.println("Delivery ID"+entity.getCount());
											for(int a=0;a<entity.getDeliveryItems().size();a++)
											{
												System.out.println("DF======"+entity.getDeliveryItems().get(a).getQuantity());
											}
										for(int i=0;i<entity.getDeliveryItems().size();i++){
											if(deliveryList.size()>0){
												for(int j=0;j<deliveryList.size();j++){
													if(deliveryList.get(j).getProdId()==entity.getDeliveryItems().get(i).getProdId()){
														qty=deliveryList.get(j).getProdQty();
														qty=qty+entity.getDeliveryItems().get(i).getQuantity();
														deliveryList.get(j).setProdQty(qty);
														flage=true;
													}
												}
												if(flage==false){
													ProductQty pq=new ProductQty();
													pq.setProdId(entity.getDeliveryItems().get(i).getProdId());
													pq.setProdQty(entity.getDeliveryItems().get(i).getQuantity());
												
													deliveryList.add(pq);
												}
											}
											else {
												ProductQty pq=new ProductQty();
												pq.setProdId(entity.getDeliveryItems().get(i).getProdId());
												pq.setProdQty(entity.getDeliveryItems().get(i).getQuantity());
											
												deliveryList.add(pq);
												System.out.println("Delivery List Size After"+deliveryList.size());
											}
										  }
										}
									}
									else{
										System.out.println("No Result Found !!!!");
									}
								}
								
								for(int i=0;i<deliveryList.size();i++){
									System.out.println("------------------------ Result ----------------------------  ");
									System.out.println("GRN List PO ID : "+deliveryList.get(i).getProdId());
									System.out.println("GRN List QTY : "+deliveryList.get(i).getProdQty());
								}
								System.out.println("EOF");
								form.validateDeliveryNoteQty(salesOrderList,deliveryList);
							}
						});
					}
				});
			}
			
	void validateProdSerialQty (ArrayList<DeliveryNote> dtnote ,SalesOrder srord,String statusupdate){
				
				int counter=0;
				HashMap<Integer, Double> proqty = new HashMap<Integer, Double>();
				for(int i = 0 ; i < srord.getItems().size() ; i++){
					for(DeliveryNote dtNote : dtnote){
						for(int j = 0 ;j<dtNote.getDeliveryItems().size();j++){
							if(dtNote.getDeliveryItems().get(j).getProdId() == srord.getItems().get(i).getPrduct().getCount()
								&& dtNote.getDeliveryItems().get(j).getProductSrNumber() == srord.getItems().get(i).getProductSrNo()	){
								if(proqty.containsKey(dtNote.getDeliveryItems().get(j).getProdId())){
									double qty = proqty.get(dtNote.getDeliveryItems().get(j).getProdId());
									qty = qty + dtNote.getDeliveryItems().get(j).getQuantity();
									proqty.put(dtNote.getDeliveryItems().get(j).getProdId(), qty);
								}else{
									double qty =  dtNote.getDeliveryItems().get(j).getQuantity();
									proqty.put(dtNote.getDeliveryItems().get(j).getProdId(), qty);
								}
							}
						}
					}
				}
				
				counter = 0;
				
				HashMap<Integer, Double> reProQty = new HashMap<Integer, Double>();
				HashMap<Integer, Double> lssProQty = new HashMap<Integer, Double>();
				for(Integer prID : proqty.keySet()){
					for(int i = 0 ; i < srord.getItems().size() ; i++){
						if(srord.getItems().get(i).getPrduct().getCount() == prID && srord.getItems().get(i).getQty() == proqty.get(prID)){
							counter ++;
							
						}else if(srord.getItems().get(i).getPrduct().getCount() == prID && srord.getItems().get(i).getQty() < proqty.get(prID)){
							reProQty.put(srord.getItems().get(i).getPrduct().getCount(), proqty.get(prID) - srord.getItems().get(i).getQty());
						}else if(srord.getItems().get(i).getPrduct().getCount() == prID && srord.getItems().get(i).getQty() > proqty.get(prID)){
							lssProQty.put(srord.getItems().get(i).getPrduct().getCount(), proqty.get(prID) - srord.getItems().get(i).getQty());
						}
					}
				}
				
				if(reProQty.size()>0){

					StringBuffer validateMsg = new StringBuffer();
						
					for(Integer prId : reProQty.keySet()){
						for(int d=0;d<form.deliverynoteobj.getDeliveryItems().size();d++)
						{
							if(form.deliverynoteobj.getDeliveryItems().get(d).getProdId() == prId){
								validateMsg.append("Product "+form.deliverynoteobj.getDeliveryItems().get(d).getProdName().trim()
										+"Quantity exced by " + reProQty.get(prId) + ".\n");
							}
							
						}
						
					}
					
					form.showDialogMessage(validateMsg.toString());
				
				}else{
					if(statusupdate.equalsIgnoreCase(AppConstants.SUBMIT)){
						form.getManageapproval().reactToSubmit();
					}else if(statusupdate.equalsIgnoreCase(ManageApprovals.APPROVALREQUEST)){
						reactOnReqApproval();
					}
				}
			}
			
			

	private void updateDeliveryStatus() {
		MyQuerry querry = new MyQuerry();
		Company c = new Company();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(model.getSalesOrderCount());
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new SalesOrder());
		service.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				form.showDialogMessage("Error : "+caught.getMessage());
				form.hideWaitSymbol();
			}
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				SalesOrder salesOrder=(SalesOrder) result.get(0);
				model.setCompletionDate(new Date());
				if(model.getCompletionDate().before(salesOrder.getDeliveryDate())||AppUtility.parseDate(model.getCompletionDate()).equals(AppUtility.parseDate(salesOrder.getDeliveryDate()))){
					model.setDeliveryStatus("On Time");
				}else{
					model.setDeliveryStatus("Delayed");
				}
				reactOnCompleted();
			}
		});
	}	
	
	private void reactOnViewSalesOrder() {


		MyQuerry querry=new MyQuerry();
		Vector<Filter> temp=new Vector<Filter>();
		Filter filter=null;
		
		filter=new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(model.getSalesOrderCount());
		temp.add(filter);
		
		querry.setFilters(temp);
		querry.setQuerryObject(new SalesOrder());
		form.showWaitSymbol();
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
			}
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				form.hideWaitSymbol();
				if(result.size()==0){
					form.showDialogMessage("Sales order not found.");
					return;
				}
				if(result.size()>0){
					final SalesOrder entity=(SalesOrder) result.get(0);
					final SalesOrderForm form=SalesOrderPresenter.initalize();
					Timer timer=new Timer() {
						@Override
						public void run() {
							form.updateView(entity);
							form.setToViewState();
						}
					};
					timer.schedule(3000);
				}
			}
		});
	
	}

}
