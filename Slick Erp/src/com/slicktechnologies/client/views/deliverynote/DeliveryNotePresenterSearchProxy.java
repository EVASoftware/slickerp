package com.slicktechnologies.client.views.deliverynote;

import java.util.Vector;

import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.config.CategoryTypeFactory;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.deliverynote.DeliveryNotePresenter.DeliveryNotePresenterSearch;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.deliverynote.DeliveryNote;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.personlayer.Employee;

public class DeliveryNotePresenterSearchProxy extends DeliveryNotePresenterSearch {
	
	public PersonInfoComposite personInfo;
	public IntegerBox tbSalesOrderId;
	public IntegerBox tbDeliveryNoteId;
	public ObjectListBox<Branch> olbBranch;
	public ObjectListBox<Employee> olbEmployee;
	public DateComparator dateComparator;

	public ObjectListBox<Config> olbDeliveryGroup;
	public ObjectListBox<ConfigCategory> olbDeliveryCategory;
	public ObjectListBox<Type> olbDeliveryType;
	public ListBox olbStatus;
	public ObjectListBox<Config> olbSalesOrderPriority;
	public ObjectListBox<Config>olbcNumberRange; //Added by Ashwini
	/**
	 * Updated By:Viraj
	 * Date: 27-06-2019
	 * Description: To add reference no 
	 */
	public TextBox refNo;
	/** Ends **/
	public Object getVarRef(String varName)
	{
		if(varName.equals("personInfo"))
			return this.personInfo;
		if(varName.equals("tbDeliveryNoteId"))
			return this.tbDeliveryNoteId;
		if(varName.equals("tbSalesOrderId"))
			return this.tbSalesOrderId;
		if(varName.equals("olbBranch"))
			return this.olbBranch;
		if(varName.equals("olbEmployee"))
			return this.olbEmployee;
		if(varName.equals("dateComparator"))
			return this.dateComparator;
		
		if(varName.equals("olbDeliveryGroup"))
			return this.olbDeliveryGroup;
					
		if(varName.equals("olbDeliveryCategory"))
			return this.olbDeliveryCategory;
					
		if(varName.equals("olbDeliveryType"))
			return this.olbDeliveryType;
					
		if(varName.equals("olbStatus"))
			return this.olbStatus;
		
		/*
		 * Date:23/07/2018
		 * Developer:Ashwini
		 * Des:To add number range in search popup of delivery note.
		 */
	
		if(varName.equals("olbcNumberRange"))
			return this.olbcNumberRange;
		/*
		 * end by Ashwini
		 */
		/**
		 * Updated By:Viraj
		 * Date: 27-06-2019
		 * Description: To add reference no 
		 */
		if(varName.equals("refNo"))
			return this.refNo;
		/** Ends **/
		return null ;
	}
	public DeliveryNotePresenterSearchProxy()
	{
		super();
		createGui();
	}
	public void initWidget()
	{
		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new Customer());
		personInfo=new PersonInfoComposite(querry,false);
		tbSalesOrderId= new IntegerBox();
		tbDeliveryNoteId=new IntegerBox();
		olbBranch= new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbBranch);
		olbEmployee= new ObjectListBox<Employee>();
		olbEmployee.makeEmployeeLive(AppConstants.SALESMODULE, AppConstants.DELIVERYNOTE, "Sales Person");
		
		AppUtility.makeSalesPersonListBoxLive(olbEmployee);
		dateComparator= new DateComparator("deliveryDate",new DeliveryNote());
		
		olbDeliveryGroup=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbDeliveryGroup, Screen.DELIVERYNOTEGROUP);
		
		olbDeliveryCategory=new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(olbDeliveryCategory, Screen.DELIVERYNOTECATEGORY);
		
		olbDeliveryType=new ObjectListBox<Type>();
		AppUtility.makeTypeListBoxLive(olbDeliveryType, Screen.DELIVERYNOTETYPE);
		
		/*
		 * Date:23/07/2018
		 * Developer:Ashwini
		 */
		olbcNumberRange = new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbcNumberRange, Screen.NUMBERRANGE);
		/*
		 * End by Ashwini
		 */
		
		olbStatus=new ListBox();
		AppUtility.setStatusListBox(olbStatus, DeliveryNote.getStatusList());
		CategoryTypeFactory.initiateOneManyFunctionality(olbDeliveryCategory,olbDeliveryType);
		
		personInfo.getCustomerId().getHeaderLabel().setText("Customer ID");
		personInfo.getCustomerName().getHeaderLabel().setText("Customer Name");
		personInfo.getCustomerCell().getHeaderLabel().setText("Customer Cell");
		
		/**
		 * Updated By:Viraj
		 * Date: 27-06-2019
		 * Description: To add reference no 
		 */
		refNo = new TextBox();
		/** Ends **/
	}
	
	
	public void createScreen()
	{
		initWidget();
		FormFieldBuilder builder;
		builder = new FormFieldBuilder("",personInfo);
		FormField fpersonInfo= builder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		builder = new FormFieldBuilder("Delivery Note ID",tbDeliveryNoteId);
		FormField ftbDeliveryNoteId= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Sales Order ID",tbSalesOrderId);
		FormField ftbSalesOrderId= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Branch",olbBranch);
		FormField folbBranch= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Sales Person",olbEmployee);
		FormField folbEmployee= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("From Date",dateComparator.getFromDate());
		FormField fdateComparator= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("To Date",dateComparator.getToDate());
		FormField fdateComparator1= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		builder = new FormFieldBuilder("Delivery Note Group",olbDeliveryGroup);
		FormField folbDeliveryGroup= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Delivery Note Category",olbDeliveryCategory);
		FormField folbDeliveryCategory= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Delivery Note Type",olbDeliveryType);
		FormField folbDeliveryType= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Delivery Note Status",olbStatus);
		FormField folbQuotationStatus= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/*
		 * Date:21/07/2018
		 * Developer:Ashwini
		 * Des:To add number range coloumn in sales order search popup.
		 */
		FormField folbcNumberRange;	
		builder = new FormFieldBuilder("Number Range",olbcNumberRange);
		folbcNumberRange= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/*
		 * End by Ashwini
		 */
		
		//builder = new FormFieldBuilder("Contract Priority",olbContractPriority);
		//FormField folbQuotationPriority= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		/**
		 * Updated By:Viraj
		 * Date: 27-06-2019
		 * Description: To add reference no 
		 */
		builder = new FormFieldBuilder("Refrence No.",refNo);
		FormField frefNo= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		/** Ends **/
		
		this.fields=new FormField[][]{
//				{fdateComparator,fdateComparator1},//Commented by Ashwini
				{fdateComparator,fdateComparator1,folbcNumberRange,frefNo},//Added by Ashwini
				{folbDeliveryGroup,folbDeliveryCategory,folbDeliveryType,folbQuotationStatus},
				{ftbDeliveryNoteId,ftbSalesOrderId,folbEmployee,folbBranch},
				{fpersonInfo}
		};
	}
	public MyQuerry getQuerry()
	{
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		if(dateComparator.getValue()!=null)
		{
			filtervec.addAll(dateComparator.getValue());
		}
		if(olbEmployee.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbEmployee.getValue().trim());
			temp.setQuerryString("employee");
			filtervec.add(temp);
		}
		if(olbBranch.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbBranch.getValue().trim());
			temp.setQuerryString("branch");
			filtervec.add(temp);
		}
		if(personInfo.getIdValue()!=-1)
		  {
		  temp=new Filter();
		  temp.setIntValue(personInfo.getIdValue());
		  temp.setQuerryString("cinfo.count");
		  filtervec.add(temp);
		  }
		  
		  if(!(personInfo.getFullNameValue().equals("")))
		  {
		  temp=new Filter();
		  temp.setStringValue(personInfo.getFullNameValue());
		  temp.setQuerryString("cinfo.fullName");
		  filtervec.add(temp);
		  }
		  if(personInfo.getCellValue()!=-1l)
		  {
		  temp=new Filter();
		  temp.setLongValue(personInfo.getCellValue());
		  temp.setQuerryString("cinfo.cellNumber");
		  filtervec.add(temp);
		  }

		  if(tbSalesOrderId.getValue()!=null){
				temp=new Filter();
				temp.setIntValue(tbSalesOrderId.getValue());
				filtervec.add(temp);
				temp.setQuerryString("salesOrderCount");
				filtervec.add(temp);
			}
		  
		  if(tbDeliveryNoteId.getValue()!=null){
				temp=new Filter();
				temp.setIntValue(tbDeliveryNoteId.getValue());
				filtervec.add(temp);
				temp.setQuerryString("count");
				filtervec.add(temp);
			}
			
		
		if(olbDeliveryGroup.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbDeliveryGroup.getValue().trim());
			temp.setQuerryString("group");
			filtervec.add(temp);
		}
		
		if(olbDeliveryCategory.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbDeliveryCategory.getValue().trim());
			temp.setQuerryString("category");
			filtervec.add(temp);
		}
		
		if(olbDeliveryType.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbDeliveryType.getValue().trim());
			temp.setQuerryString("type");
			filtervec.add(temp);
		}
		
		if(olbStatus.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbStatus.getValue(olbStatus.getSelectedIndex()).trim());
			temp.setQuerryString("status");
			filtervec.add(temp);
		}
		
		/*
		 * Date:23/07/2018
		 * Developer:Ashwini
		 */
		if(olbcNumberRange.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(olbcNumberRange.getValue(olbcNumberRange.getSelectedIndex()).trim());
			temp.setQuerryString("Number Range");
			filtervec.add(temp);
			}
		
		/*
		 * End by Ashwini
		 */
		/**
		 * Updated By:Viraj
		 * Date: 27-06-2019
		 * Description: To add reference no 
		 */

		if(refNo.getValue() != null && !refNo.getValue().equals("")) {

			temp=new Filter();
			temp.setStringValue(refNo.getValue().trim());
			temp.setQuerryString("referenceNumber");
			filtervec.add(temp);
		}
		/** Ends **/
		MyQuerry querry= new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new DeliveryNote());
		return querry;
	}
	@Override
	public boolean validate() {

		if(LoginPresenter.branchRestrictionFlag)
		{
		if(olbBranch.getSelectedIndex()==0)
		{
			showDialogMessage("Select Branch");
			return false;
		}
		}
		return super.validate();
	}
	
	

}
