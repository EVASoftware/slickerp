package com.slicktechnologies.client.views.deliverynote;

import java.text.DecimalFormat;
import java.util.List;

import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.ProductOtherCharges;

public class ProductTaxesTable extends SuperTable<ProductOtherCharges> {
	
	/******************************** Column declaration **************************/
	
	TextColumn<ProductOtherCharges> prodTaxNameColumn;
	TextColumn<ProductOtherCharges> prodTaxPercentColumn;
	TextColumn<ProductOtherCharges> assessableAmountColumn;
	TextColumn<ProductOtherCharges> calculatedTaxAmount;
	NumberFormat df = NumberFormat.getFormat("0.00");
	
	/****************************************************************************/
	
	/*********************************Constructor********************************/
	
	public ProductTaxesTable() {
		super();
	}
	
	/****************************************************************************/
	
	/********** Create Table Method ***********/
	@Override
	public void createTable() {
		createColumnTaxName();
		createColumnTaxPercent();
		createColumnAssessableAmount();
		createColumnCalcTaxAmount();
	    table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
	}
	
	
	/*********************************** Tax Name Column *****************************/
	public void createColumnTaxName()
	{
		prodTaxNameColumn=new TextColumn<ProductOtherCharges>() {

			@Override
			public String getValue(ProductOtherCharges object) {	
				return object.getChargeName().trim();
			}
		};
		table.addColumn(prodTaxNameColumn,"Tax");
	//	table.setColumnWidth(prodTaxNameColumn, 4, Unit.PX);
	}
	
	/*********************************** Tax Percent Column *****************************/
	public void createColumnTaxPercent()
	{
		prodTaxPercentColumn=new TextColumn<ProductOtherCharges>() {
			@Override
			public String getValue(ProductOtherCharges object) {
				return object.getChargePercent()+"";
			}
		};
		table.addColumn(prodTaxPercentColumn,"%");
	//	table.setColumnWidth(prodTaxPercentColumn, 4, Unit.PX);
	}
	
	/*********************************** Assessable Amount Column *****************************/
	/**
	 * This column represents the amount on which tax will be calculated. 
	 */
	
	public void createColumnAssessableAmount()
	{
		assessableAmountColumn=new TextColumn<ProductOtherCharges>() {
			@Override
			public String getValue(ProductOtherCharges object) {
				
				
				object.getAssessableAmount();
			
				return df.format(object.getAssessableAmount());
				}
		};
		table.addColumn(assessableAmountColumn,"Ass. Val.");
	//	table.setColumnWidth(assessableAmountColumn, 5, Unit.PX);
		
	}
	
	/************************************Calculated Tax Amount***********************************/
	public void createColumnCalcTaxAmount()
	{
		calculatedTaxAmount=new TextColumn<ProductOtherCharges>() {
			@Override
			public String getValue(ProductOtherCharges object) {
			//	DecimalFormat df = new DecimalFormat("#.00");
				double calcAmt=object.getAssessableAmount()*object.getChargePercent()/100;
				//return Math.round(calcAmt)+"";
				
				object.setChargePayable(calcAmt);
				
				return df.format(object.getChargePayable());
			}
		};
		table.addColumn(calculatedTaxAmount,"Amt");
	//	table.setColumnWidth(calculatedTaxAmount, 4, Unit.PX);
		
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
	
	
	public double calculateTotalTaxes()
	{
		List<ProductOtherCharges>list=getDataprovider().getList();

		double sum=0;
		for(int i=0;i<list.size();i++)
		{
			ProductOtherCharges entity=list.get(i);
			//double totalTaxAmt=Math.round(entity.getAssessableAmount()*entity.getChargePercent()/100);
			double totalTaxAmt=(entity.getAssessableAmount()*entity.getChargePercent()/100);
			sum=sum+totalTaxAmt;
			Double.parseDouble(df.format(sum));
		}
		
		return sum;

	}


	
}
