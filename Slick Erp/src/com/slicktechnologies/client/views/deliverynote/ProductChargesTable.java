package com.slicktechnologies.client.views.deliverynote;

import java.util.List;

import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.ProductOtherCharges;

public class ProductChargesTable extends SuperTable<ProductOtherCharges> {
	
	/******************************** Column declaration **************************/
	
	TextColumn<ProductOtherCharges> prodChargeNameColumn;
	TextColumn<ProductOtherCharges> prodChargePercentColumn;
	TextColumn<ProductOtherCharges> prodChargeAbsValColumn;
	TextColumn<ProductOtherCharges> assessmentAmtColumn;
	TextColumn<ProductOtherCharges> calculateChargeAmtColumn;
	NumberFormat nf=NumberFormat.getFormat("0.00");
	/****************************************************************************/
	
	/*********************************Constructor********************************/

	
	public ProductChargesTable() {
		super();
	}
	/****************************************************************************/
	
	/********** Create Table Method ***********/
	
	@Override
	public void createTable() {
		createColumnChargeName();
		createColumnPercent();
		createColumnAbsoluteValue();
		createColumnAssessmentAmt();
		createColumnChargeAmt();
	}
	
	/********************************Charge Name Column*******************************************/
	
	public void createColumnChargeName()
	{
		prodChargeNameColumn=new TextColumn<ProductOtherCharges>() {

			@Override
			public String getValue(ProductOtherCharges object) {
				return object.getChargeName();
				
			}
		};
		table.addColumn(prodChargeNameColumn,"Charge");
	}
	
	/************************************Percent Column********************************************/
	
	protected void createColumnPercent()
	{
	prodChargePercentColumn=new TextColumn<ProductOtherCharges>()
	{
	@Override
	public String getValue(ProductOtherCharges object)
	{
		if(object.getChargePercent()==0||object.getChargePercent()>100)
		{
			return "";
		}
		else
		{
			return object.getChargePercent()+"";
		}
	}
	};
	table.addColumn(prodChargePercentColumn,"%");
	}
	
	/**********************************Absolute Value Column*******************************************/
	
	protected void createColumnAbsoluteValue()
	{
	prodChargeAbsValColumn=new TextColumn<ProductOtherCharges>()
	{
	@Override
	public String getValue(ProductOtherCharges object)
	{
		if(object.getChargeAbsValue()==0){
			return "";
		}
		else{
			object.setAssessableAmount(0);
			return object.getChargeAbsValue()+"";
		}
	}
	};
	table.addColumn(prodChargeAbsValColumn,"Abs Val");
	}
	
	/************************************Assessment Amount Column**************************************/
	
	protected void createColumnAssessmentAmt()
	{
	assessmentAmtColumn=new TextColumn<ProductOtherCharges>()
	{
	@Override
	public String getValue(ProductOtherCharges object)
	{
		return object.getAssessableAmount()+"";
	}
	};
	table.addColumn(assessmentAmtColumn,"Ass. Val.");
	}
	
	/********************************Calculated Charge Amount Column****************************************/
	
	protected void createColumnChargeAmt()
	{
		calculateChargeAmtColumn=new TextColumn<ProductOtherCharges>()
		{
		@Override
		public String getValue(ProductOtherCharges object)
		{
			double chargePercAmt=0,totalCalcAmt=0;
			if(object.getChargePercent()!=0){
				chargePercAmt=object.getAssessableAmount()*object.getChargePercent()/100 ;
				totalCalcAmt=chargePercAmt;
			}
			if(object.getChargeAbsValue()!=0){
				totalCalcAmt=object.getChargeAbsValue();
			}
			totalCalcAmt=Double.parseDouble(nf.format(totalCalcAmt));
			
			object.setChargePayable(totalCalcAmt);
			
			return nf.format(object.getChargePayable());
		}
		};
		table.addColumn(calculateChargeAmtColumn,"Amt");
	}

			@Override
			protected void initializekeyprovider() {
				// TODO Auto-generated method stub
				
			}
			
	/*************************Field Updater Method (Overridden)*************************************/
			/**
			 * This method represents the columns which can be updated.
			 */

			@Override
			public void addFieldUpdater() {
			}

			
			@Override
			public void setEnable(boolean state) {
			}

			@Override
			public void applyStyle() {
				// TODO Auto-generated method stub
				
			}
	
	/*************************Calculate Net Payable Method******************************************/
			/**
			 * This method calculates the total charges amount i.e surcharge amount.
			 * The amount is calculated and fetched in Presenter and the value is set to
			 * Net Payable field in Form.
			 * @return
			 */
			
			public double calculateNetPayable()
			{
				List<ProductOtherCharges>list=getDataprovider().getList();

				double sum=0,calcTotal=0;
				for(int i=0;i<list.size();i++)
				{
					ProductOtherCharges entity=list.get(i);
					if(entity.getChargePercent()!=0){
						calcTotal=entity.getChargePercent()*entity.getAssessableAmount()/100;
					}
					if(entity.getChargeAbsValue()!=0){
						calcTotal=entity.getChargeAbsValue();
					}
					sum=sum+calcTotal;
				}
				
				return sum;
			}
			

}
