package com.slicktechnologies.client.views.purchase.vendor;
import com.google.gwt.event.dom.client.ClickEvent;

import java.util.*;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import com.simplesoftwares.client.library.appstructure.*;
import com.simplesoftwares.client.library.appstructure.search.*;
import com.simplesoftwares.client.library.appstructure.formscreen.*;
import com.slicktechnologies.server.addhocdownload.XlsxWriter;
import com.slicktechnologies.shared.*;
import com.slicktechnologies.shared.common.*;
import com.slicktechnologies.shared.common.businessprocesslayer.*;
import com.slicktechnologies.shared.common.businessunitlayer.*;
import com.slicktechnologies.shared.common.contactidentification.ContactPersonIdentification;
import com.slicktechnologies.shared.common.helperlayer.*;
import com.slicktechnologies.shared.common.personlayer.*;
import com.slicktechnologies.shared.common.productlayer.*;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.google.gwt.core.shared.GWT;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.CustomerNameChangeService;
import com.slicktechnologies.client.services.CustomerNameChangeServiceAsync;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.contactpersonidentification.contactpersonlist.ContactListPresenter;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.shared.common.paymentlayer.*;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.google.gwt.event.dom.client.*;
/**
 * FormScreen presenter template.
 */
public class VendorPresenter extends FormScreenPresenter<Vendor>{

	//Token to set the concrete FormScreen class name
	VendorForm form;
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	CustomerNameChangeServiceAsync customernamechangeasync = GWT.create(CustomerNameChangeService.class);

	
	public VendorPresenter  (FormScreen<Vendor> view, Vendor model) {
		super(view, model);
		form=(VendorForm) view;
		
		  boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.VENDOR,LoginPresenter.currentModule.trim());
		  if(isDownload==false){
			 form.getSearchpopupscreen().getDwnload().setVisible(false);
		  }
		}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
	InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();
		
		if(text.equals("New")){
			reactToNew();
		}
		if(text.equals("View Contact List")){
			reactOnContactList();
		}
		if(text.equals("Update Vendor Name")){
			reactOnUpdateVendor();
		}
	}
	


	

	private void reactToNew()
	{
		form.setToNewState();
		this.initalize();
		form.toggleAppHeaderBarMenu();
	}
	@Override
	public void reactOnPrint() {
		
		
	}
	
	

	@Override
	public void reactOnEmail() {
		
		
	}

	/**
	 * Method token to make new model
	 */
	@Override
	protected void makeNewModel() {
		model=new Vendor();
	}
	
	
	
	
	public static VendorForm initalize()
	{
		VendorForm  form=new  VendorForm();
		
		
		 VendorPresenterTable gentable=new VendorPresenterTableProxy();
		  gentable.setView(form);
		  gentable.applySelectionModle();
		 VendorPresenterSearch.staticSuperTable=gentable;
		 VendorPresenterSearch searchpopup=new VendorPresenterSearchProxy();
		form.setSearchpopupscreen(searchpopup);
		
		
		 VendorPresenter  presenter=new VendorPresenter(form,new Vendor());
		AppMemory.getAppMemory().stickPnel(form);
		return form;
	}
	
	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.businessprocesslayer.Vendor")
	 public static  class VendorPresenterSearch extends SearchPopUpScreen<Vendor>{

		@Override
		public MyQuerry getQuerry() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public boolean validate() {
			// TODO Auto-generated method stub
			return true;
		}};
		
		@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.businessprocesslayer.Vendor")
		 public static class VendorPresenterTable extends SuperTable<Vendor> implements GeneratedVariableRefrence{

			@Override
			public Object getVarRef(String varName) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void createTable() {
				// TODO Auto-generated method stub
				
			}

			@Override
			protected void initializekeyprovider() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void addFieldUpdater() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void setEnable(boolean state) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void applyStyle() {
				// TODO Auto-generated method stub
				
			}} ;
			
		  private void reactTo()
		  {
		  }
		  
		  protected void reactOnContactList()
		  {
				Screen s = (Screen) AppMemory.getAppMemory().currentScreen;
				String currentScrType=s.toString();
				System.out.println("Screen Type"+currentScrType);
				
				MyQuerry querry=new MyQuerry();
				Vector<Filter>filtervec=new Vector<Filter>();
				Filter temp=null;
				
				temp=new Filter();
				temp.setQuerryString("companyId");
				temp.setLongValue(model.getCompanyId());
				filtervec.add(temp);
				
				temp=new Filter();
				temp.setIntValue(model.getCount());
				temp.setQuerryString("personVendorInfo.count");
				filtervec.add(temp);
				
				querry.setFilters(filtervec);
				querry.setQuerryObject(new ContactPersonIdentification());
				
		    	 AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
		    	 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Purchase/Contact Person List",Screen.CONTACTPERSONLIST);
		    	 ContactListPresenter.initialize(querry);
			}
		  
		 
			@Override
			public void reactOnDownload() {
				
				
					ArrayList<Vendor> vendorarray=new ArrayList<Vendor>();
					List<Vendor> list=(List<Vendor>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
					
					vendorarray.addAll(list);
					
					csvservice.setVendor(vendorarray, new AsyncCallback<Void>() {
		
						@Override
						public void onFailure(Throwable caught) {
							System.out.println("RPC call Failed"+caught);
						}
						
						@Override
						public void onSuccess(Void result) {
				
							String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
							final String url=gwt + "csvservlet"+"?type="+41;
							Window.open(url, "test", "enabled");
							
//							final String url=gwt + "CreateXLXSServlet"+"?type="+19;
//							Window.open(url, "test", "enabled");
						}
					});
					
					
//					
//					ArrayList<String> vendordatalist = new ArrayList<String>();
//
//					vendordatalist.add("ID");
//					vendordatalist.add("NAME");
//					vendordatalist.add("POC FULL NAME");
//					vendordatalist.add("EMAIL");
//					vendordatalist.add("CELL NUMBER");
//					vendordatalist.add("GROUP");
//					vendordatalist.add("CATEGORY");
//					vendordatalist.add("TYPE");
//					vendordatalist.add("ADDRESS LINE 1");
//					vendordatalist.add("ADDRESS LINE 2");
//					vendordatalist.add("LANDMARK");
//					vendordatalist.add("COUNTRY");
//					vendordatalist.add("STATE");
//					vendordatalist.add("CITY");
//					vendordatalist.add("LOCALITY");
//					vendordatalist.add("PIN");
//					vendordatalist.add("GOOGLE PLUS ID");
//					vendordatalist.add("FACEBOOK ID");
//					vendordatalist.add("TWITTER ID");
//					vendordatalist.add("STATUS");
//
//					int columnCount = vendordatalist.size();
//					
//					for(Vendor vendor:vendorarray)
//					{
//						vendordatalist.add(vendor.getCount()+"");
//						vendordatalist.add(vendor.getVendorName());
//						vendordatalist.add(vendor.getfullName());
//						vendordatalist.add(vendor.getEmail());
//						vendordatalist.add(vendor.getCellNumber1()+"");
//
//						if(vendor.getGroup()!=null)
//							vendordatalist.add(vendor.getGroup());
//						else
//							vendordatalist.add("");
//
//						if(vendor.getCategory()!=null)
//							vendordatalist.add(vendor.getCategory());
//						else
//							vendordatalist.add("N.A");
//					
//						if(vendor.getType()!=null)
//							vendordatalist.add(vendor.getType());
//						else
//							vendordatalist.add("N.A");
//						
//						if(vendor.getPrimaryAddress().getAddrLine1()!=null)
//							vendordatalist.add(vendor.getPrimaryAddress().getAddrLine1());
//						else
//							vendordatalist.add("N.A");
//						
//						if(vendor.getPrimaryAddress().getAddrLine2()!=null)
//							vendordatalist.add(vendor.getPrimaryAddress().getAddrLine2());
//						else
//							vendordatalist.add("N.A");
//						
//						if(vendor.getPrimaryAddress().getLandmark()!=null)
//							vendordatalist.add(vendor.getPrimaryAddress().getLandmark());
//						else
//							vendordatalist.add("N.A");
//						
//						if(vendor.getPrimaryAddress().getCountry()!=null)
//							vendordatalist.add(vendor.getPrimaryAddress().getCountry());
//						else
//							vendordatalist.add("N.A");
//						
//						if(vendor.getPrimaryAddress().getState()!=null)
//							vendordatalist.add(vendor.getPrimaryAddress().getState());
//						else
//							vendordatalist.add("N.A");
//						
//						if(vendor.getPrimaryAddress().getCity()!=null)
//							vendordatalist.add(vendor.getPrimaryAddress().getCity());
//						else
//							vendordatalist.add("N.A");
//						
//						if(vendor.getPrimaryAddress().getLocality()!=null)
//							vendordatalist.add(vendor.getPrimaryAddress().getLocality());
//						else
//							vendordatalist.add("N.A");
//						
//						vendordatalist.add(vendor.getPrimaryAddress().getPin()+"");
//
//						if(vendor.getSocialInfo().getGooglePlusId()!=null)
//							vendordatalist.add(vendor.getSocialInfo().getGooglePlusId()+"");
//						else
//							vendordatalist.add("N.A");
//						
//						if(vendor.getSocialInfo().getFaceBookId()!=null)
//							vendordatalist.add(vendor.getSocialInfo().getFaceBookId());
//						else
//							vendordatalist.add("N.A");
//
//						
//						if(vendor.getSocialInfo().getTwitterId()!=null)
//							vendordatalist.add(vendor.getSocialInfo().getTwitterId());
//						else
//							vendordatalist.add("N.A");
//						
//						if(vendor.getStatus()!=null)
//							vendordatalist.add(vendor.getStatus());
//						else
//							vendordatalist.add("N.A");
//					}
//					
//					
//					csvservice.setExcelData(vendordatalist, columnCount, AppConstants.VENDOR, new AsyncCallback<String>() {
//						
//						@Override
//						public void onSuccess(String result) {
//							// TODO Auto-generated method stub
//							
//							String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
//							final String url=gwt + "CreateXLXSServlet"+"?type="+19;
//							Window.open(url, "test", "enabled");
//						}
//						
//						@Override
//						public void onFailure(Throwable caught) {
//							// TODO Auto-generated method stub
//							
//						}
//					});
					
			}
			
			 protected void reactOnUpdateVendor() {

					form.showWaitSymbol();
					customernamechangeasync.updateVendor(model,new AsyncCallback<ArrayList<Integer>>() {
						
						@Override
						public void onSuccess(ArrayList<Integer> result) {
							System.out.println("in on success ");
							form.showDialogMessage("Vendor Name and Cell Updation Process has started. Please check after 30 minutes");
							form.hideWaitSymbol();
							
						}
						
						@Override
						public void onFailure(Throwable caught) {
							System.out.println("falied");
							form.showDialogMessage("Unexpected Error");
							form.hideWaitSymbol();
						}
					});
					
				
				  
			  }
			
}
