package com.slicktechnologies.client.views.purchase.vendor;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;

import java.util.Comparator;

import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.google.gwt.cell.client.FieldUpdater;

import java.util.List;
import java.util.Date;

import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.cell.client.EditTextCell;
import  com.google.gwt.cell.client.NumberCell;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import  com.google.gwt.cell.client.DatePickerCell;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.client.views.purchase.vendor.VendorPresenter.VendorPresenterTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;

public class VendorPresenterTableProxy extends VendorPresenterTable {
	TextColumn<Vendor> getBranchColumn;
	TextColumn<Vendor> getCreationDateColumn;
	TextColumn<Vendor> getStatusColumn;
	TextColumn<Vendor> getVendorNameColumn;
	TextColumn<Vendor> getGroupColumn;
	TextColumn<Vendor> getCategoryColumn;
	TextColumn<Vendor> getTypeColumn;
	TextColumn<Vendor> getCountColumn;

	public Object getVarRef(String varName)
	{
		if(varName.equals("getBranchColumn"))
			return this.getBranchColumn;
		if(varName.equals("getCreationDateColumn"))
			return this.getCreationDateColumn;
		if(varName.equals("getStatusColumn"))
			return this.getStatusColumn;
		if(varName.equals("getVendorNameColumn"))
			return this.getVendorNameColumn;
		if(varName.equals("getGroupColumn"))
			return this.getGroupColumn;
		if(varName.equals("getCategoryColumn"))
			return this.getCategoryColumn;
		if(varName.equals("getTypeColumn"))
			return this.getTypeColumn;
		if(varName.equals("getCountColumn"))
			return this.getCountColumn;
		return null ;
	}
	public VendorPresenterTableProxy()
	{
		super();
	}
	@Override public void createTable() {
		addColumngetCount();
		addColumngetVendorName();
		//addColumngetCreationDate();
		//addColumngetBranch();
		
		addColumngetGroup();
		addColumngetCategory();
		addColumngetType();
		addColumngetStatus();
		
	}
	@Override
	protected void initializekeyprovider() {
		keyProvider= new ProvidesKey<Vendor>()
				{
			@Override
			public Object getKey(Vendor item)
			{
				if(item==null)
				{
					return null;
				}
				else
					return item.getId();
			}
				};
	}
	@Override
	public void setEnable(boolean state)
	{
	}
	@Override
	public void applyStyle()
	{
	}
	public void addColumnSorting(){
		addSortinggetCount();
		addSortinggetCreationDate();
		addSortinggetBranch();
		addSortinggetStatus();
		addSortinggetGroup();
		addSortinggetCategory();
		addSortinggetType();
		addSortinggetVendorName();
	}
	@Override public void addFieldUpdater() {
	}
	protected void addSortinggetCount()
	{
		List<Vendor> list=getDataprovider().getList();
		columnSort=new ListHandler<Vendor>(list);
		columnSort.setComparator(getCountColumn, new Comparator<Vendor>()
				{
			@Override
			public int compare(Vendor e1,Vendor e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetCount()
	{
		getCountColumn=new TextColumn<Vendor>()
				{
			@Override
			public String getValue(Vendor object)
			{
				if( object.getCount()==-1)
					return "N.A";
				else return object.getCount()+"";
			}
				};
				table.addColumn(getCountColumn,"Id");
				getCountColumn.setSortable(true);
	}
	protected void addSortinggetCreationDate()
	{
		List<Vendor> list=getDataprovider().getList();
		columnSort=new ListHandler<Vendor>(list);
		columnSort.setComparator(getCreationDateColumn, new Comparator<Vendor>()
				{
			@Override
			public int compare(Vendor e1,Vendor e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getCreationDate()!=null && e2.getCreationDate()!=null){
						return e1.getCreationDate().compareTo(e2.getCreationDate());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetCreationDate()
	{
		DateTimeFormat fmt = DateTimeFormat.getFormat(PredefinedFormat.DATE_SHORT);
		DatePickerCell date= new DatePickerCell(fmt);
		getCreationDateColumn=new TextColumn<Vendor>()
				{
			@Override
			public String getValue(Vendor object)
			{
					return AppUtility.parseDate(object.getCreationDate());
			}
				};
				table.addColumn(getCreationDateColumn,"Creation Date");
				getCreationDateColumn.setSortable(true);
	}
	/*protected void addFieldUpdatergetCreationDate()
	{
		getCreationDateColumn.setFieldUpdater(new FieldUpdater<Vendor>()
				{
			@Override
			public void update(int index,Vendor object,Date value)
			{
				object.setCreationDate(value);
				table.redrawRow(index);
			}
				});
	}*/
	protected void addSortinggetBranch()
	{
		List<Vendor> list=getDataprovider().getList();
		columnSort=new ListHandler<Vendor>(list);
		columnSort.setComparator(getBranchColumn, new Comparator<Vendor>()
				{
			@Override
			public int compare(Vendor e1,Vendor e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBranch()!=null && e2.getBranch()!=null){
						return e1.getBranch().compareTo(e2.getBranch());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetBranch()
	{
		getBranchColumn=new TextColumn<Vendor>()
				{
			@Override
			public String getValue(Vendor object)
			{
				return object.getBranch()+"";
			}
				};
				table.addColumn(getBranchColumn,"Branch");
				getBranchColumn.setSortable(true);
	}
	protected void addSortinggetStatus()
	{
		List<Vendor> list=getDataprovider().getList();
		columnSort=new ListHandler<Vendor>(list);
		columnSort.setComparator(getStatusColumn, new Comparator<Vendor>()
				{
			@Override
			public int compare(Vendor e1,Vendor e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getStatus()!=null && e2.getStatus()!=null){
						return e1.getStatus().compareTo(e2.getStatus());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetStatus()
	{
		getStatusColumn=new TextColumn<Vendor>()
				{
			@Override
			public String getValue(Vendor object)
			{
				if(object.isVendorStatus()==true)
					return "Active";
				else
					return "In Active";
			}
				};
				table.addColumn(getStatusColumn,"Status");
				getStatusColumn.setSortable(true);
	}
	protected void addSortinggetGroup()
	{
		List<Vendor> list=getDataprovider().getList();
		columnSort=new ListHandler<Vendor>(list);
		columnSort.setComparator(getGroupColumn, new Comparator<Vendor>()
				{
			@Override
			public int compare(Vendor e1,Vendor e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getGroup()!=null && e2.getGroup()!=null){
						return e1.getGroup().compareTo(e2.getGroup());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetGroup()
	{
		getGroupColumn=new TextColumn<Vendor>()
				{
			@Override
			public String getValue(Vendor object)
			{
				return object.getGroup()+"";
			}
				};
				table.addColumn(getGroupColumn,"Group");
				getGroupColumn.setSortable(true);
	}
	protected void addSortinggetCategory()
	{
		List<Vendor> list=getDataprovider().getList();
		columnSort=new ListHandler<Vendor>(list);
		columnSort.setComparator(getCategoryColumn, new Comparator<Vendor>()
				{
			@Override
			public int compare(Vendor e1,Vendor e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getCategory()!=null && e2.getCategory()!=null){
						return e1.getCategory().compareTo(e2.getCategory());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetCategory()
	{
		getCategoryColumn=new TextColumn<Vendor>()
				{
			@Override
			public String getValue(Vendor object)
			{
				return object.getCategory()+"";
			}
				};
				table.addColumn(getCategoryColumn,"Category");
				getCategoryColumn.setSortable(true);
	}
	protected void addSortinggetType()
	{
		List<Vendor> list=getDataprovider().getList();
		columnSort=new ListHandler<Vendor>(list);
		columnSort.setComparator(getTypeColumn, new Comparator<Vendor>()
				{
			@Override
			public int compare(Vendor e1,Vendor e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getType()!=null && e2.getType()!=null){
						return e1.getType().compareTo(e2.getType());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetType()
	{
		getTypeColumn=new TextColumn<Vendor>()
				{
			@Override
			public String getValue(Vendor object)
			{
				return object.getType()+"";
			}
				};
				table.addColumn(getTypeColumn,"Type");
				getTypeColumn.setSortable(true);
	}
	protected void addSortinggetVendorName()
	{
		List<Vendor> list=getDataprovider().getList();
		columnSort=new ListHandler<Vendor>(list);
		columnSort.setComparator(getVendorNameColumn, new Comparator<Vendor>()
				{
			@Override
			public int compare(Vendor e1,Vendor e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getVendorName()!=null && e2.getVendorName()!=null){
						return e1.getVendorName().compareTo(e2.getVendorName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetVendorName()
	{
		getVendorNameColumn=new TextColumn<Vendor>()
				{
			@Override
			public String getValue(Vendor object)
			{
				return object.getVendorName()+"";
			}
				};
				table.addColumn(getVendorNameColumn,"Vendor");
				getVendorNameColumn.setSortable(true);
	}
}
