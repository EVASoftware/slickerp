package com.slicktechnologies.client.views.purchase.vendor;

import com.google.gwt.user.client.ui.ListBox;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;

import java.util.Vector;

import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.shared.common.helperlayer.*;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.client.views.purchase.vendor.VendorPresenter.VendorPresenterSearch;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.slicktechnologies.client.config.CategoryTypeFactory;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;

public class VendorPresenterSearchProxy extends VendorPresenterSearch {
	public PersonInfoComposite personInfo;
	
	//public DateComparator dateComparator;
	  public ObjectListBox<ConfigCategory> olbVendorCategory;
	  public ObjectListBox<Config>olbVendorGroup;
	  public ObjectListBox<Type> olbVendorType;
	  public ListBox lbvendorStatus;
	  
	
	public Object getVarRef(String varName)
	{
		if(varName.equals("personInfo"))
			return this.personInfo;
	//	if(varName.equals("dateComparator"))
		//	  return this.dateComparator;
		if(varName.equals("olbVendorCategory"))
			  return this.olbVendorCategory;
		
		if(varName.equals("olbVendorType"))
			return this.olbVendorType;
		return null ;
	}
	public VendorPresenterSearchProxy()
	{
		super();
		createGui();
	}
	public void initWidget()
	{
		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new Vendor());
		personInfo=new PersonInfoComposite(querry,false);
		
		// dateComparator= new DateComparator("concreteBusinessProcess.creationDate",new Vendor());
		
		olbVendorCategory=new ObjectListBox<ConfigCategory>();
		
		
		olbVendorType= new ObjectListBox<Type>();
		  
		 
		  olbVendorGroup=new ObjectListBox<Config>();
		  AppUtility.MakeLiveConfig(olbVendorGroup,Screen.VENDORGROUP);
		  lbvendorStatus=new ListBox();
		  
		  lbvendorStatus.addItem("--Select--");
		  lbvendorStatus.addItem(AppConstants.ACTIVE);
		  lbvendorStatus.addItem(AppConstants.INACTIVE);
		  
		  //For giving type category functionality
		  CategoryTypeFactory.initiateOneManyFunctionality(this.olbVendorCategory,this.olbVendorType); 
		  AppUtility.makeTypeListBoxLive(olbVendorType,Screen.VENDORTYPE);
		  
	}
	public void createScreen()
	{
		initWidget();
		FormFieldBuilder builder;
		builder = new FormFieldBuilder("",personInfo);
		FormField fpersonInfo= builder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		
		//builder = new FormFieldBuilder("From Date",dateComparator.getFromDate());
		 // FormField fdateComparator= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		 // builder = new FormFieldBuilder("To Date",dateComparator.getToDate());
		 // FormField fdateComparator1= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		  
		  builder = new FormFieldBuilder("Group",olbVendorGroup);
		  FormField fgroup= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		  
		  builder = new FormFieldBuilder("Vendor Category",olbVendorCategory);
		  FormField folbVendorCategory= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		  
		  builder = new FormFieldBuilder("Vendor Type",olbVendorType);
		  FormField folbVendorType= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		  builder = new FormFieldBuilder("Vendor Status",lbvendorStatus);
		  FormField flbstatus= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		  
		this.fields=new FormField[][]
				{
				
				{fgroup,folbVendorCategory,folbVendorType,flbstatus},
				{fpersonInfo,}
		};
	}
	public MyQuerry getQuerry()
	{
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		if(personInfo.getIdValue()!=-1)
		  {
		  temp=new Filter();
		  temp.setIntValue(personInfo.getIdValue());
		  temp.setQuerryString("count");
		  filtervec.add(temp);
		  }
		  
		  if(!(personInfo.getFullNameValue().equals("")))
		  {
		  temp=new Filter();
		  temp.setStringValue(personInfo.getFullNameValue());
		  temp.setQuerryString("vendorName");
		  filtervec.add(temp);
		  }
		  if(personInfo.getCellValue()!=-1l)
		  {
		  temp=new Filter();
		  temp.setLongValue(personInfo.getCellValue());
		  temp.setQuerryString("contact.cellNo1");
		  filtervec.add(temp);
		  }
		
		/* if(dateComparator.getValue()!=null)
		  {
		  filtervec.addAll(dateComparator.getValue());
		  }*/
		 
		 if(olbVendorType.getSelectedIndex()!=0){
			  temp=new Filter();
			  temp.setStringValue(olbVendorType.getValue().trim());
			  temp.setQuerryString("type");
			  filtervec.add(temp);
			  }
			  
			  if(olbVendorGroup.getSelectedIndex()!=0){
				  temp=new Filter();
				  temp.setStringValue(olbVendorGroup.getValue().trim());
				  temp.setQuerryString("group");
				  filtervec.add(temp);
				  }
			  
			  
			  if(olbVendorCategory.getSelectedIndex()!=0){
			  temp=new Filter();
			  temp.setStringValue(olbVendorCategory.getValue().trim());
			  temp.setQuerryString("category");
			  filtervec.add(temp);
			  }
			  
			  if(this.lbvendorStatus.getSelectedIndex()!=0){
				  temp=new Filter();
				  int index=lbvendorStatus.getSelectedIndex();
				  String selectedItem=lbvendorStatus.getItemText(index);
				  temp.setQuerryString("vendorStatus");
				  if(selectedItem.equals(AppConstants.ACTIVE))
				  {
					  temp.setBooleanvalue(true);
				  }
				  if(selectedItem.equals(AppConstants.INACTIVE))
				  {
					  temp.setBooleanvalue(false);
					  
				  }
				  filtervec.add(temp);
				  }
		MyQuerry querry= new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Vendor());
		return querry;
	}
	
	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
		return super.validate();
	}
}
