package com.slicktechnologies.client.views.purchase.vendor;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.composite.AddressComposite;
import com.simplesoftwares.client.library.composite.SocialInfoComposites;
import com.simplesoftwares.client.library.composite.TaxInformationComposite;
import com.simplesoftwares.client.library.composite.UploadComposite;
import com.simplesoftwares.client.library.mywidgets.EmailTextBox;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.simplesoftwares.client.library.mywidgets.PhoneNumberBox;
import com.slicktechnologies.client.config.CategoryTypeFactory;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.composites.articletypecomposite.ArticleTypeComposite;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;


public class VendorForm extends FormScreen<Vendor>    implements ClickHandler, ChangeHandler, ValueChangeHandler<Long> {

	//***********************************Variable Declaration Starts********************************************//
	//Token to add the varialble declarations
	EmailTextBox etbEmail;
	AddressComposite PrimaryAddressComposite,SecondaryAddressComposite;
	CheckBox checkBoxStatus;
	SocialInfoComposites socialInfoComposite;
	TaxInformationComposite taxInfoComposite;
//	TextBox tbVendorName,tbFirstName,tbMiddleName,tbLastName;
	// vijay
		TextBox tbVendorName,tbfullName;
	
	TextBox tbVendorId;
	PhoneNumberBox pnbLandlineNo,pnbCellNo1,pnbCellNo2,pnbFaxNo;
	ObjectListBox<Branch>olbBranch;
	ObjectListBox<Config>olbVendorGroup;
	
	ObjectListBox<ConfigCategory>olbVendorCategory;
	ObjectListBox<Type>olbVendorType;

	Button btnsecaddress,btnprimaryaddress;
	UploadComposite upload;
	ArticleTypeComposite tbArticleTypeInfoComposite;
	FormField fgroupingVendorInformation;
	Vendor vendorobj;
	/*
	 * Added by Rahul Verma
	 * Ref id of vendor
	 */
	TextBox tbVendorRefNo;
	/** date 12.10.2018 added by komal for sasha to save credit days **/
	IntegerBox ibCreaditDate;
	/** date 06.12.2018 added by komal for sasha to save credit days comment**/
	TextBox tbCreditDaysComment;
	
	//***********************************Variable Declaration Ends********************************************//
	public  VendorForm() {
		super();
		createGui();
		tbVendorId.setEnabled(false);
		tbArticleTypeInfoComposite.setForm(this);
	}

	public VendorForm  (String[] processlevel, FormField[][] fields,
			FormStyle formstyle) 
	{
		super(processlevel, fields, formstyle);
		createGui();
		tbVendorId.setEnabled(false);
		

	}


	//***********************************Variable Initialization********************************************//
	/**
	 * Method template to initialize the declared variables.
	 */
	private void initalizeWidget()
	{

		etbEmail=new EmailTextBox();

		PrimaryAddressComposite=new AddressComposite();

		SecondaryAddressComposite=new AddressComposite();

		checkBoxStatus=new CheckBox();
		checkBoxStatus.setValue(true);

		socialInfoComposite=new SocialInfoComposites();

		taxInfoComposite=new TaxInformationComposite();

		tbVendorId=new TextBox();
		/**
		 * Date : 07-02-2017 By ANil
		 */
		tbVendorId.setEnabled(false);
		/*
		 * End
		 */

		tbVendorName=new TextBox();
//
//		tbFirstName=new TextBox();
//
//		tbMiddleName=new TextBox();
//
//		tbLastName=new TextBox();
		
		tbVendorRefNo=new TextBox();

		tbfullName = new TextBox();
		pnbLandlineNo=new PhoneNumberBox();

		pnbCellNo1=new PhoneNumberBox();

		pnbCellNo2=new PhoneNumberBox();
		tbArticleTypeInfoComposite = new ArticleTypeComposite();
		olbVendorGroup=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbVendorGroup, Screen.VENDORGROUP);
		
		olbVendorCategory= new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(olbVendorCategory, Screen.VENDORCATEGORY);
		olbVendorCategory.addChangeHandler(this);
		olbVendorType = new ObjectListBox<Type>();
		AppUtility.makeTypeListBoxLive(olbVendorType,Screen.VENDORTYPE);
		pnbFaxNo=new PhoneNumberBox();
		olbBranch=new ObjectListBox<Branch>();
		btnsecaddress=new Button("Copy Secondary Address");
		btnprimaryaddress=new Button("Copy Primary Address");
		btnsecaddress.addClickHandler(this);
		btnprimaryaddress.addClickHandler(this);
		
		upload = new UploadComposite();
		
		/**
		 * Date : 15-05-2018 BY ANIl
		 * added validation code for cell no 1 for sasha erp
		 */
		pnbCellNo1.addValueChangeHandler(this);
		/** date 12.10.2018 added by komal for sasha to save credit days **/
		ibCreaditDate = new IntegerBox();
		/** date 06.12.2018 added by komal for sasha to save credit days comment**/
		tbCreditDaysComment = new TextBox();

	}

	/**
	 * method template to create screen formfields
	 */
	@Override
	public void createScreen() {


		initalizeWidget();

		//Token to initialize the processlevel menus.



		this.processlevelBarNames=new String[]{"New","View Contact List","Update Vendor Name"};
		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////
		//Token to initialize formfield
		
		String mainScreenLabel="VENDOR";
		if(vendorobj!=null&&vendorobj.getCount()!=0){
			
			
			mainScreenLabel=vendorobj.getCount()+" "+"/"+" "+vendorobj.getStatus()+" "+"/"+" "+AppUtility.parseDate(vendorobj.getCreationDate());
		}
		//fgroupingVendorInformation.getHeaderLabel().setText(mainScreenLabel);
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		fgroupingVendorInformation=fbuilder.setlabel(mainScreenLabel).widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).setKeyField(true).build();
		
		
		//FormFieldBuilder fbuilder;
		//fbuilder = new FormFieldBuilder();
		//FormField fgroupingVendorInformation=fbuilder.setlabel("Vendor Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Vendor Id",tbVendorId);
		FormField ftbVendorId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Vendor Name",tbVendorName);
		FormField ftbVendorName= fbuilder.setMandatory(true).setMandatoryMsg("Vendor Name is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Status",checkBoxStatus);
		FormField fcheckBoxStatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		//   by vijay for vendor full name 
//		fbuilder = new FormFieldBuilder("* POC First Name",tbFirstName);
//		FormField ftbFirstName= fbuilder.setMandatory(true).setMandatoryMsg("First Name is Mandatory!").setRowSpan(0).setColSpan(0).build();
//		fbuilder = new FormFieldBuilder("POC Middle Name",tbMiddleName);
//		FormField ftbMiddleName= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
//		fbuilder = new FormFieldBuilder("* POC Last Name",tbLastName);
//		FormField ftbLastName= fbuilder.setMandatory(true).setMandatoryMsg("Last Name is Mandatory!").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* POC Full Name",tbfullName);
		FormField ftbfullName= fbuilder.setMandatory(true).setMandatoryMsg("Full Name is Mandatory!").setRowSpan(0).setColSpan(0).build();
		
		/**@author Sheetal : 28-05-2022 , Making Email and cell no non-mandatory for envocare**/
		FormField fetbEmail;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Vendor", AppConstants.PC_MakeEmailNonMandatoryOnVendor)){
		fbuilder = new FormFieldBuilder("Email",etbEmail);
		 fetbEmail= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}else{
			fbuilder = new FormFieldBuilder("* Email",etbEmail);
			 fetbEmail= fbuilder.setMandatory(true).setMandatoryMsg("Email is Mandatory!").setRowSpan(0).setColSpan(0).build();
		}
		fbuilder = new FormFieldBuilder("Landline No.",pnbLandlineNo);
		FormField fpnbLandlineNo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		FormField fpnbCellNo1;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Vendor", AppConstants.PC_MakeCellNumberNonMandatoryOnVendor)){
		fbuilder = new FormFieldBuilder("Cell No. 1",pnbCellNo1);
		 fpnbCellNo1= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}else{
			fbuilder = new FormFieldBuilder("* Cell No. 1",pnbCellNo1);
			 fpnbCellNo1= fbuilder.setMandatory(true).setMandatoryMsg("Cell No 1 is Mandatory!").setRowSpan(0).setColSpan(0).build();
		}
		fbuilder = new FormFieldBuilder("Cell No. 2",pnbCellNo2);
		FormField fpnbCellNo2= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Fax No.",pnbFaxNo);
		FormField fpnbFaxNo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingSocialInformation=fbuilder.setlabel("Social Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("",socialInfoComposite);
		FormField fsocialInfoComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingPrimaryAddress=fbuilder.setlabel("Billing Address").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder=new FormFieldBuilder("",btnsecaddress);
		FormField fsecaddressbtn=fbuilder.build();

		fbuilder = new FormFieldBuilder("",PrimaryAddressComposite);
		FormField fPrimaryAddressComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingSecondaryAddress=fbuilder.setlabel("Secondary Address").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder=new FormFieldBuilder("",btnprimaryaddress);
		FormField fprimaryadrsbtn=fbuilder.build();

		fbuilder = new FormFieldBuilder("",SecondaryAddressComposite);
		FormField fSecondaryAddressComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingVender1Information=fbuilder.setlabel("Reference/General").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		FormField fgroupingClassification=fbuilder.setlabel("Classification").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		FormField fgroupingAttachment=fbuilder.setlabel("Attachment").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		
		FormField fgroupingTaxInformation=fbuilder.setlabel("Article Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("",taxInfoComposite);
		FormField ftaxInfoComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Vendor Group",olbVendorGroup);
		FormField folbGroup=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder=new FormFieldBuilder("Vendor Category",olbVendorCategory);
		FormField folbCat=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).setMandatoryMsg("Category is Mandatory!").build();
		fbuilder=new FormFieldBuilder("Vendor Type",olbVendorType);
		FormField folbType=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).setMandatoryMsg("Type is Mandatory!").build();

		fbuilder = new FormFieldBuilder("Upload Document", upload);
		FormField fdoc = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("",tbArticleTypeInfoComposite);
	    FormField ftbTaxTypeInfoComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();

	    fbuilder = new FormFieldBuilder("Reference Id",tbVendorRefNo);
	    FormField ftbVendorRefNo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
	    
		 /** date 12.10.2018 added by komal for sasha to save credit days **/
		 fbuilder=new FormFieldBuilder("Credit Days", ibCreaditDate);
		 FormField fibCreaditDate=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();  
		 
		 /** date 06.12.2018 added by komal for sasha to save credit days **/
		 fbuilder=new FormFieldBuilder("Credit Days Comment", tbCreditDaysComment);
		 FormField ftbCreditDaysComment=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();  
		        
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////


			FormField[][] formfield = {   
//					{fgroupingVendorInformation},
//					{ftbVendorId,ftbVendorName,ftbfullName,fcheckBoxStatus},
////					{ftbFirstName,ftbMiddleName,ftbLastName,fetbEmail},
//					{fetbEmail,fpnbLandlineNo,fpnbCellNo1,fpnbCellNo2},
//					{fpnbFaxNo,folbGroup,folbCat,folbType},
//					{ftbVendorRefNo , fibCreaditDate ,ftbCreditDaysComment}, /** date 12.10.2018 added by komal for sasha to save credit days **/
//					{fdoc},
//					{fgroupingPrimaryAddress},
//					{fsecaddressbtn},
//					{fPrimaryAddressComposite},
//					{fgroupingSecondaryAddress},
//					{fprimaryadrsbtn},
//					{fSecondaryAddressComposite},
//					{fgroupingTaxInformation},
//					{ftbTaxTypeInfoComposite},
//					{fgroupingSocialInformation},
//					{fsocialInfoComposite}
//					
					
					{fgroupingVendorInformation},
					{ftbVendorId,ftbVendorName,ftbfullName,fcheckBoxStatus},
					{fetbEmail, fpnbCellNo1},
					/**Primary and SecondoryAddress**/
					{fgroupingPrimaryAddress},
					{fsecaddressbtn},
					{fPrimaryAddressComposite},
					{fgroupingSecondaryAddress},
					{fprimaryadrsbtn},
					{fSecondaryAddressComposite},
					/**Vendor info**/   
					{fgroupingVender1Information},
					{fpnbLandlineNo,fpnbCellNo2, fpnbFaxNo,ftbVendorRefNo},
					{ fibCreaditDate ,ftbCreditDaysComment},
					/**Classification**/
					{fgroupingClassification},
					{folbGroup,folbCat,folbType},
					/**Taxinformation**/
					{fgroupingTaxInformation},
					{ftbTaxTypeInfoComposite},
					/**Attachment**/
					{fgroupingAttachment},
					{fdoc},
					/**Social Info**/
					{fgroupingSocialInformation},
					{fsocialInfoComposite},
				
					
			};

			this.fields=formfield;
	}

	/**
	 * method template to update the model with token entity name
	 */
	@Override
	public void updateModel(Vendor model) 
	{

		if(tbVendorName.getValue()!=null)
			model.setVendorName(tbVendorName.getValue());
		if(checkBoxStatus.getValue()!=null)
			model.setVendorStatus(checkBoxStatus.getValue());
//		if(tbFirstName.getValue()!=null)
//			model.setFirstName(tbFirstName.getValue().trim());
//		if(tbMiddleName.getValue()!=null)
//			model.setMiddleName(tbMiddleName.getValue().trim());
//		if(tbLastName.getValue()!=null)
//			model.setLastName(tbLastName.getValue().trim());
		
		if(tbfullName.getValue()!=null){
			model.setfullName(tbfullName.getValue().trim().toUpperCase());
		}
		
		if(etbEmail.getValue()!=null)
			model.setEmail(etbEmail.getValue());
		if(pnbLandlineNo.getValue()!=null)
			model.setLandline(pnbLandlineNo.getValue());
		if(pnbCellNo1.getValue()!=null)
			model.setCellNumber1(pnbCellNo1.getValue());
		if(pnbCellNo2.getValue()!=null)
			model.setCellNumber2(pnbCellNo2.getValue());
		if(pnbFaxNo.getValue()!=null)
			model.setFaxNumber(pnbFaxNo.getValue());
		if(socialInfoComposite.getValue()!=null)
			model.setSocialInfo(socialInfoComposite.getValue());
		if(PrimaryAddressComposite.getValue()!=null){
			model.setPrimaryAddress(PrimaryAddressComposite.getValue());
			System.out.println("PrimaryAddressComposite "+PrimaryAddressComposite.getValue().getCompleteAddress());
		}
		if(SecondaryAddressComposite.getValue()!=null){
			model.setSecondaryAddress(SecondaryAddressComposite.getValue());
			System.out.println("SecondaryAddressComposite "+SecondaryAddressComposite.getValue().getCompleteAddress());
		}
		if(taxInfoComposite.getValue()!=null)
			model.setTaxInfo(taxInfoComposite.getValue());
		if(olbVendorGroup.getValue()!=null)
			model.setGroup(olbVendorGroup.getValue());
		if(olbVendorCategory.getValue()!=null)
			model.setCategory(olbVendorCategory.getValue());
		if(olbVendorType.getValue()!=null)
			model.setType(olbVendorType.getValue(olbVendorType.getSelectedIndex()));
		if (upload.getValue() != null)
			model.setUptestReport(upload.getValue());
		if(tbArticleTypeInfoComposite.getValue()!=null)
			model.setArticleTypeDetails(tbArticleTypeInfoComposite.getValue());
		if(tbVendorRefNo.getValue()!=null)
			model.setVendorRefNo(tbVendorRefNo.getValue());
		/** date 12.10.2018 added by komal for sasha to save credit days **/	
		if(ibCreaditDate.getValue()!=null){
			model.setCreditDays(ibCreaditDate.getValue());
		}
		/** date 06.12.2018 added by komal for sasha to save credit days comment **/
		if(tbCreditDaysComment.getValue()!=null){
			model.setCreditDaysComment(tbCreditDaysComment.getValue());
		}
		vendorobj=model;
		presenter.setModel(model);

	}

	/**
	 * method template to update the view with token entity name
	 */
	@Override
	public void updateView(Vendor view) 
	{

		vendorobj=view;
		
		/**
		 * Date 20-04-2018 By vijay for orion pest locality load  
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Locality", "DontLoadAllLocalityAndCity")){
			setAddressDropDown();
		}
		/**
		 * ends here
		 */
		
		if(view.getVendorName()!=null)
			tbVendorName.setValue(view.getVendorName());
		
			checkBoxStatus.setValue(view.isVendorStatus());
//		if(view.getFirstName()!=null)
//			tbFirstName.setValue(view.getFirstName());
//		if(view.getMiddleName()!=null)
//			tbMiddleName.setValue(view.getMiddleName());
//		if(view.getLastName()!=null)
//			tbLastName.setValue(view.getLastName());
			
			if(view.getfullName()!=null){
				tbfullName.setValue(view.getfullName());
			}
			
			
		if(view.getEmail()!=null)
			etbEmail.setValue(view.getEmail());
		if(view.getLandline()!=null)
			pnbLandlineNo.setValue(view.getLandline());
		if(view.getCellNumber1()!=null)
			pnbCellNo1.setValue(view.getCellNumber1());
		if(view.getCellNumber2()!=null)
			pnbCellNo2.setValue(view.getCellNumber2());
		if(view.getFaxNumber()!=null)
			pnbFaxNo.setValue(view.getFaxNumber());
		if(view.getSocialInfo()!=null)
			socialInfoComposite.setValue(view.getSocialInfo());
		if(view.getPrimaryAddress()!=null){
			PrimaryAddressComposite.setValue(view.getPrimaryAddress());
			System.out.println("view.getPrimaryAddress() "+view.getPrimaryAddress().getCompleteAddress());
		}
		if(view.getSecondaryAddress()!=null){
			SecondaryAddressComposite.setValue(view.getSecondaryAddress());
			System.out.println("view.getSecondaryAddress() "+view.getSecondaryAddress().getCompleteAddress());

		}
		if(view.getTaxInfo()!=null)
			taxInfoComposite.setValue(view.getTaxInfo());

		tbVendorId.setValue(view.getCount()+"");
		
		if(view.getArticleTypeDetails()!=null)
			tbArticleTypeInfoComposite.setValue(view.getArticleTypeDetails());
		
		if(view.getGroup()!=null)
			olbVendorGroup.setValue(view.getGroup());

		if(view.getCategory()!=null)
			olbVendorCategory.setValue(view.getCategory());
		if(view.getType()!=null)
			olbVendorType.setValue(view.getType());
		if (view.getUptestReport() != null)
			upload.setValue(view.getUptestReport());
		if(view.getVendorRefNo()!=null&&view.getVendorRefNo().trim().length()>0){
			tbVendorRefNo.setValue(view.getVendorRefNo()+"");
		}	
		/** date 12.10.2018 added by komal for sasha to save credit days **/	
		ibCreaditDate.setValue(view.getCreditDays());
		/** date 06.12.2018 added by komal for sasha to save credit days comment **/
		if(view.getCreditDaysComment() != null){
			tbCreditDaysComment.setValue(view.getCreditDaysComment());
		}
		presenter.setModel(view);
	}

	// Hand written code shift in presenter


	/**
	 * Date 20-04-2018 By vijay
	 * here i am refreshing address composite data if global list size greater than drop down list size
	 * because we are adding locality and city from an entity in view state to global list if doest not exist in global list 
	 */
	private void setAddressDropDown() {
		if(LoginPresenter.globalLocality.size()>PrimaryAddressComposite.locality.getItems().size()
			|| LoginPresenter.globalLocality.size()>SecondaryAddressComposite.locality.getItems().size()){
			PrimaryAddressComposite.locality.setListItems(LoginPresenter.globalLocality);
			SecondaryAddressComposite.locality.setListItems(LoginPresenter.globalLocality);
		}
	}
	/**
	 * ends here 
	 */
	
	/**
	 * Toggles the app header bar menus as per screen state
	 */

	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals("Search")||text.equals(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit")||text.equals("Search")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.VENDOR,LoginPresenter.currentModule.trim());
	}

	/**
	 * sets the id textbox with the passed count value. 
	 */
	@Override
	public void setCount(int count)
	{
		tbVendorId.setValue(count+"");
	}
	
	@Override
	public void setToEditState() {
		super.setToEditState();
		this.processLevelBar.setVisibleFalse(false);
		
		String mainScreenLabel="VENDOR";
		if(vendorobj!=null&&vendorobj.getCount()!=0){
			
			
			mainScreenLabel=vendorobj.getCount()+" "+"/"+" "+vendorobj.getStatus()+" "+"/"+" "+AppUtility.parseDate(vendorobj.getCreationDate());
		}
		fgroupingVendorInformation.getHeaderLabel().setText(mainScreenLabel);
	}


	@Override
	public void onClick(ClickEvent event) {
		Button butn=(Button) event.getSource();

		if(butn == btnsecaddress)
		{
			PrimaryAddressComposite.clear();
			PrimaryAddressComposite.setValue(SecondaryAddressComposite.getValue());
		}

		if(butn == btnprimaryaddress)
		{
			SecondaryAddressComposite.clear(); 
			SecondaryAddressComposite.setValue(PrimaryAddressComposite.getValue());	  
		}

	}

	
	  @Override
		public void setToViewState() {
			super.setToViewState();
			SuperModel model=new Vendor();
			model=vendorobj;
			AppUtility.addDocumentToHistoryTable(AppConstants.PURCHASEMODULE,AppConstants.VENDOR, vendorobj.getCount(), vendorobj.getCount(),vendorobj.getVendorName(),vendorobj.getCellNumber1(), false, model, null);
			
			String mainScreenLabel="VENDOR";
			if(vendorobj!=null&&vendorobj.getCount()!=0){
				
				
				mainScreenLabel=vendorobj.getCount()+" "+"/"+" "+vendorobj.getStatus()+" "+"/"+" "+AppUtility.parseDate(vendorobj.getCreationDate());
			}
			fgroupingVendorInformation.getHeaderLabel().setText(mainScreenLabel);
	  
	  }
	  
  @Override
  public void clear()
  {
	  super.clear();
	  checkBoxStatus.setEnabled(true);
  }
  
  public ArticleTypeComposite getTbArticleTypeInfoComposite() {
		return tbArticleTypeInfoComposite;
	}

	public void setTbArticleTypeInfoComposite(
			ArticleTypeComposite tbArticleTypeInfoComposite) {
		this.tbArticleTypeInfoComposite = tbArticleTypeInfoComposite;
	}

	/*******************************************Type Drop Down Logic**************************************/
	
	@Override
	public void onChange(ChangeEvent event) {
		if(event.getSource().equals(olbVendorCategory))
		{
			if(olbVendorCategory.getSelectedIndex()!=0){
				ConfigCategory cat=olbVendorCategory.getSelectedItem();
				if(cat!=null){
					AppUtility.makeLiveTypeDropDown(olbVendorType, cat.getCategoryName(), cat.getCategoryCode(), cat.getInternalType());
				}
			}
		}
		
	}
	
	
	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		tbVendorId.setEnabled(false);
	}
	
	@Override
	public boolean validate() {
	
		boolean superValidate= super.validate();
		if(superValidate==false)
			return false;
		
		boolean customerValid = AppUtility.checkForProcessConfigurartionIsActiveOrNot("Vendor","MakeArticleTypeMandatory");
		if(customerValid){
			if(tbArticleTypeInfoComposite.getArticletypetable().getDataprovider().getList().size() == 0){
				showDialogMessage("Article Information is Mandatory !");
				return false;
			}
		}
		
		/**
		 * Date : 15-05-2018 By ANIL
		 * Validated employee cell no 1
		 * for sasha erp
		 */
		int cellCurrLen = pnbCellNo1.getValue().toString().length();
		
		
		//Ashwini Patil Date:12-05-2022 If cell number is mandatory then only it should check for validation
		boolean disablecellNovalidation=false;
		boolean cellNoNonMandatory=false;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer", "DisableCellNumberValidation")){
			disablecellNovalidation=true;
		}
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Vendor", AppConstants.PC_MakeCellNumberNonMandatoryOnVendor)) {
			cellNoNonMandatory=true;
		}
		if(!disablecellNovalidation&&!cellNoNonMandatory){
			Console.log("validating cell number cellNoNonMandatory="+cellNoNonMandatory);
			if(AppUtility.validateFieldRange(cellCurrLen, 10, 11, "Cell No. 1 can not be less than 10 digit !", "Cell No. 1 can not be more than 11 digit !")==false){
				return false;
			}				
		}
		
		return true;
	}
	
	/**
	 * Date :  15-05-2018 By  ANIL
	 * 
	 */
	@Override
	public void onValueChange(ValueChangeEvent<Long> event) {
//		if (event.getSource() == pnbCellNo1) {
//			int cellCurrLen = pnbCellNo1.getValue().toString().length();
//			AppUtility.validateFieldRange(cellCurrLen, 10, 11, "Cell No. 1 can not be less than 10 digit !", "Cell No. 1 can not be more than 11 digit !");
//		
//		}
	}

	@Override
	public void refreshTableData() {
		// TODO Auto-generated method stub
		super.refreshTableData();
		tbArticleTypeInfoComposite.getArticletypetable().getTable().redraw();
	}

	/**
	 *     Added By : Priyanka Bhagwat
	 *     Date : 1/06/2021
	 *     Des : When Submit PO then SMS and EMAIL will be send Req by Pecopp raised By Ashwini.	
	 */
	public TextBox getTbVendorName() {
		return tbVendorName;
	}

	public void setTbVendorName(TextBox tbVendorName) {
		this.tbVendorName = tbVendorName;
	}
	
	
}
