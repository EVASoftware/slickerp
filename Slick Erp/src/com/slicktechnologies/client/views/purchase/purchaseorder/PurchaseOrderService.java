package com.slicktechnologies.client.views.purchase.purchaseorder;

import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;

@RemoteServiceRelativePath("purchaseorderservice")
public interface PurchaseOrderService {
	public void approvalTransaction(PurchaseOrder purchaseorder);
}
