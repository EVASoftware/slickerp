package com.slicktechnologies.client.views.purchase.purchaseorder;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.dom.client.Style.BorderStyle;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.salesquotation.OtherChargesMarginTable;
import com.slicktechnologies.client.views.salesquotation.VendorMarginTable;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetailsPO;
import com.slicktechnologies.shared.common.salesorder.OtherChargesMargin;
import com.slicktechnologies.shared.common.salesorder.VendorMargin;

public class VendorAndOcMarginPopupPO extends PopupScreen implements RowCountChangeEvent.Handler {


	public VendorMarginTable vendorMarginTbl;
	public OtherChargesMarginTable otherChargesMarginTbl;
	public DoubleBox dbTotalVendorMargin;
	public DoubleBox dbOtherChargesMargin;
	
	FormField fdbTotalVendorMargin;
	FormField fdbOtherChargesMargin;
	
	
	public TextBox tbCostType;
	public DoubleBox dbAmount;
	public Button btnAdd;
	
	DoubleBox dbTotalOcSales;
	DoubleBox dbTotalOcPO;
	
	
	public VendorAndOcMarginPopupPO() {
		super();
		createGui();
		
//		fdbTotalVendorMargin.getHeaderLabel().getElement().getStyle().setMarginLeft(350, Unit.PX);
//		fdbOtherChargesMargin.getHeaderLabel().getElement().getStyle().setMarginLeft(350, Unit.PX);
	
	}
	
	
	
	@Override
	public void applyStyle() {
//		super.applyStyle();
		
//		content.getElement().setId("popupformcontent");
		content.getElement().getStyle().setWidth(700, Unit.PX);
		content.getElement().getStyle().setBorderWidth(1, Unit.PX);
		content.getElement().getStyle().setBorderStyle(BorderStyle.SOLID);
		form.getElement().setId("form");
		horizontal.getElement().addClassName("popupcentering");
		lblOk.getElement().setId("addbutton");
		lblCancel.getElement().setId("addbutton");
		popup.center();
	}



	private void initializeWidget() {
		vendorMarginTbl=new VendorMarginTable(true);
		otherChargesMarginTbl=new OtherChargesMarginTable(true);
		
		vendorMarginTbl.getTable().addRowCountChangeHandler(this);
		otherChargesMarginTbl.getTable().addRowCountChangeHandler(this);
		dbTotalVendorMargin=new DoubleBox();
		dbTotalVendorMargin.setEnabled(false);
		dbOtherChargesMargin=new DoubleBox();
		dbOtherChargesMargin.setEnabled(false);
		
		tbCostType=new TextBox();
		dbAmount=new DoubleBox();
		btnAdd=new Button("Add");
		btnAdd.addClickHandler(this);
		
		dbTotalOcSales=new DoubleBox();
		dbTotalOcSales.setEnabled(false);
		
		dbTotalOcPO=new DoubleBox();
		dbTotalOcPO.setEnabled(false);
		
		
	}

	@Override
	public void createScreen() {
		// TODO Auto-generated method stub
		initializeWidget();
		
		FormFieldBuilder fbuilder;
		
		fbuilder = new FormFieldBuilder();
		FormField fVenGrping=fbuilder.setlabel("Vendor Price Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder("",vendorMarginTbl.getTable());
		FormField fvendorMarginTbl= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
	
		fbuilder = new FormFieldBuilder();
		FormField fOcGrping=fbuilder.setlabel("Other Charges Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder("",otherChargesMarginTbl.getTable());
		FormField fotherChargesMarginTbl= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		
//		dbTotalVendorMargin.getElement().getStyle().setWidth(40, Unit.PCT);
//		dbTotalVendorMargin.getElement().getStyle().setMarginLeft(350, Unit.PX);
		fbuilder = new FormFieldBuilder("Total Vendor Margin (Purchase)",dbTotalVendorMargin);
		fdbTotalVendorMargin= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
		
//		dbOtherChargesMargin.getElement().getStyle().setWidth(40, Unit.PCT);
//		dbOtherChargesMargin.getElement().getStyle().setMarginLeft(350, Unit.PX);
		fbuilder = new FormFieldBuilder("Other Charges (Margin)",dbOtherChargesMargin);
		fdbOtherChargesMargin= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		
		
		fbuilder = new FormFieldBuilder();
		FormField fblankgroupthree=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("Cost Type",tbCostType);
		FormField ftbCostType= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Amount",dbAmount);
		FormField fdbAmount= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",btnAdd);
		FormField fbtnAdd= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("Total Other Charges (Sales)",dbTotalOcSales);
		FormField fdbTotalOcSales= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Total Other Charges (PO)",dbTotalOcPO);
		FormField fdbTotalOcPO= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		FormField[][] formfield = {
				{fVenGrping},
				{fvendorMarginTbl},
				{fblankgroupthree,fdbTotalVendorMargin},
				{fOcGrping},
				{ftbCostType,fdbAmount,fbtnAdd},
				{fotherChargesMarginTbl},
				{fdbTotalOcSales,fdbTotalOcPO,fdbOtherChargesMargin}
		};

		this.fields=formfield;
		
	}
	
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		if(event.getSource()==btnAdd){
			if(tbCostType.getValue().equals("")){
				showDialogMessage("Please enter cost type.");
				return;
			}
			if(dbAmount.getValue()==null){
				showDialogMessage("Please enter amount.");
				return;
			}
			
			OtherChargesMargin obj=new OtherChargesMargin();
			obj.setCostType(tbCostType.getValue());
			obj.setOtherCharges(dbAmount.getValue());
			
			if(otherChargesMarginTbl.getValue()!=null&&otherChargesMarginTbl.getValue().size()!=0){
				for(OtherChargesMargin margin:otherChargesMarginTbl.getValue()){
					if(obj.getCostType().equals(margin.getCostType())){
						showDialogMessage("Cost Type already added.");
						return;
					}
				}
			}
			
			otherChargesMarginTbl.getDataprovider().getList().add(obj);
			
			tbCostType.setText("");
			dbAmount.setValue(null);
		}
	}
	
	
	public void clear() {
		// TODO Auto-generated method stub
		vendorMarginTbl.connectToLocal();
		otherChargesMarginTbl.connectToLocal();
		dbTotalVendorMargin.setValue(null);
		dbOtherChargesMargin.setValue(null);
		dbTotalOcPO.setValue(null);
		dbTotalOcSales.setValue(null);
	}



	@Override
	public void onRowCountChange(RowCountChangeEvent event) {
		if(event.getSource()==vendorMarginTbl.getTable()){
			double sum=0;
			for(VendorMargin venObj:vendorMarginTbl.getValue()){
				sum=sum+venObj.getPurchaseMargin();
			}
			dbTotalVendorMargin.setValue(sum);
		}
		
		if(event.getSource()==otherChargesMarginTbl.getTable()){
			double sum=0;
			for(OtherChargesMargin ocObj:otherChargesMarginTbl.getValue()){
				sum=sum+ocObj.getOtherCharges();
			}
			dbTotalOcPO.setValue(sum);
			
			if(dbTotalOcSales.getValue()!=null&&dbTotalOcSales.getValue()>0){
				dbOtherChargesMargin.setValue(dbTotalOcSales.getValue()-dbTotalOcPO.getValue());
			}
		}
	}
	
	public void reactOnCosting(List<ProductDetailsPO> itemList,List<VendorMargin> vendorMargins,List<OtherChargesMargin> otherChargesMargins) {
		if(itemList==null||itemList.size()==0){
			showDialogMessage("Please add product details first.");
			return;
		}
		clear();
		
		boolean updateFlag=true;
		if(vendorMargins!=null&&vendorMargins.size()!=0){
			updateFlag=false;
		}
		
		Double totalVendorMargin=0d;
		Double totalOcMargin=0d;
		Double ocSales=0d;
		Double ocPO = 0d;
		
		if(updateFlag){
			Console.log("New Entry in costing.....!!");
			ArrayList<VendorMargin> vendorMarginList=new ArrayList<VendorMargin>();
			for(ProductDetailsPO item:itemList){
				VendorMargin venObj=new VendorMargin();
				venObj.setProductId(item.getPrduct().getCount());
				venObj.setProductName(item.getProductName());
				venObj.setPurchasePrice(item.getTotal());
				vendorMarginList.add(venObj);
			}
			vendorMarginTbl.setValue(vendorMarginList);
			otherChargesMarginTbl.setValue(otherChargesMargins);
		}else{
			Console.log("Updating Costing ...!!");
			for(ProductDetailsPO item:itemList){
				boolean addFlag=true;
				for(VendorMargin venObj:vendorMargins){
					if(venObj.getProductId()==item.getPrduct().getCount()){
						addFlag=false;
						venObj.setProductName(item.getProductName());
						venObj.setPurchasePrice(item.getTotal());
						if(venObj.getVendorPrice()!=0&&venObj.getPurchasePrice()!=0){
							venObj.setPurchaseMargin(venObj.getVendorPrice()-venObj.getPurchasePrice());
						}else{
							venObj.setPurchaseMargin(0);
						}
						ocSales=ocSales+item.getOtherCharges();
					}
				}
				if(addFlag){
					VendorMargin venObj=new VendorMargin();
					venObj.setProductId(item.getPrduct().getCount());
					venObj.setProductName(item.getProductName());
					venObj.setPurchasePrice(item.getTotal());
					vendorMargins.add(venObj);
				}
			}
			
			for(VendorMargin venObj:vendorMargins){
				boolean deleteFlag=true;
				for(ProductDetailsPO item:itemList){
					if(venObj.getProductId()==item.getPrduct().getCount()){
						deleteFlag=false;
					}
				}
				if(deleteFlag){
					vendorMargins.remove(venObj);
				}
			}
			
			vendorMarginTbl.setValue(vendorMargins);
			otherChargesMarginTbl.setValue(otherChargesMargins);
			
			for(VendorMargin venObj:vendorMargins){
				totalVendorMargin=totalVendorMargin+venObj.getPurchaseMargin();
			}
			for(OtherChargesMargin ocObj:otherChargesMargins){
				ocPO=ocPO+ocObj.getOtherCharges();
			}
			
			if(ocSales!=null&&ocSales>0){
				totalOcMargin=ocSales-ocPO;
			}
		}
		showPopUp();
		if(totalVendorMargin!=null){
			dbTotalVendorMargin.setValue(totalVendorMargin);
		}
		if(totalOcMargin!=null){
			dbOtherChargesMargin.setValue(totalOcMargin);
		}
		
		if(ocSales!=null){
			dbTotalOcSales.setValue(ocSales);
		}
		if(ocPO!=null){
			dbTotalOcPO.setValue(ocPO);
		}
		
	}
	
	public void reactOnCostingSave(List<ProductDetailsPO> itemList,List<VendorMargin> vendorMargins,List<OtherChargesMargin> otherChargesMargins) {
		boolean updateFlag=true;
		if(vendorMargins!=null&&vendorMargins.size()!=0){
			updateFlag=false;
		}
		
		Double totalVendorMargin;
		Double totalOcMargin;
		Double ocSales=0d;
		Double ocPO;
		
		if(updateFlag){
			Console.log("SAVE New Entry in costing.....!!");
			for(ProductDetailsPO item:itemList){
				VendorMargin venObj=new VendorMargin();
				venObj.setProductId(item.getPrduct().getCount());
				venObj.setProductName(item.getProductName());
				venObj.setPurchasePrice(item.getTotal());
				vendorMargins.add(venObj);
			}
		}else{
			Console.log("SAVE Updating Costing ...!!");
			for(ProductDetailsPO item:itemList){
				boolean addFlag=true;
				for(VendorMargin venObj:vendorMargins){
					if(venObj.getProductId()==item.getPrduct().getCount()){
						addFlag=false;
						venObj.setProductName(item.getProductName());
						venObj.setPurchasePrice(item.getTotal());
						if(venObj.getVendorPrice()!=0&&venObj.getPurchasePrice()!=0){
							venObj.setPurchaseMargin(venObj.getVendorPrice()-venObj.getPurchasePrice());
						}else{
							venObj.setPurchaseMargin(0);
						}
						ocSales=ocSales+item.getOtherCharges();
					}
				}
				if(addFlag){
					VendorMargin venObj=new VendorMargin();
					venObj.setProductId(item.getPrduct().getCount());
					venObj.setProductName(item.getProductName());
					venObj.setPurchasePrice(item.getTotal());
					vendorMargins.add(venObj);
				}
			}
			
			for(VendorMargin venObj:vendorMargins){
				boolean deleteFlag=true;
				for(ProductDetailsPO item:itemList){
					if(venObj.getProductId()==item.getPrduct().getCount()){
						deleteFlag=false;
					}
				}
				if(deleteFlag){
					vendorMargins.remove(venObj);
				}
			}
			totalVendorMargin=0d;
			for(VendorMargin venObj:vendorMargins){
				totalVendorMargin=totalVendorMargin+venObj.getPurchaseMargin();
			}
			
			totalOcMargin=0d;
			ocPO=0d;
			for(OtherChargesMargin ocObj:otherChargesMargins){
				ocPO=ocPO+ocObj.getOtherCharges();
			}
			
			if(ocSales!=null&&ocSales>0){
				totalOcMargin=ocSales-ocPO;
			}
			
			
			dbTotalVendorMargin.setValue(totalVendorMargin);
			dbTotalOcSales.setValue(ocSales);
			dbTotalOcPO.setValue(ocPO);
			dbOtherChargesMargin.setValue(totalOcMargin);
		}
	}


}
