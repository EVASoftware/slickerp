package com.slicktechnologies.client.views.purchase.purchaseorder;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.businessprocesslayer.LetterOfIntent;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetailsPO;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessprocesslayer.VendorDetails;

public class PurchaseOrderPresenterTableProxy extends SuperTable<PurchaseOrder> {

	public PurchaseOrderPresenterTableProxy() {
		super();
	}

	TextColumn<PurchaseOrder> rfqIDColumn;
	TextColumn<PurchaseOrder> loiIDColumn;
	TextColumn<PurchaseOrder> poIDColumn;
	TextColumn<PurchaseOrder> potitleColumn;

	TextColumn<PurchaseOrder> RfqDateColumn;
	TextColumn<PurchaseOrder> dateColumn;
	TextColumn<PurchaseOrder> closureDateColumn;
	TextColumn<PurchaseOrder> responseDateColumn;
	TextColumn<PurchaseOrder> referenceOrderColumn;
	TextColumn<PurchaseOrder> employeeColumn;
	TextColumn<PurchaseOrder> loiGroupColumn;
	TextColumn<PurchaseOrder> loitypeColumn;
	TextColumn<PurchaseOrder> statusColumn;
	TextColumn<PurchaseOrder> ColumnProject;
	TextColumn<PurchaseOrder> ColumnPurchaseReqno;
	TextColumn<PurchaseOrder> branchColumn;
	
	TextColumn<PurchaseOrder> vendorNameColumn;
	TextColumn<PurchaseOrder> productNameColumn;
	TextColumn<PurchaseOrder> netPayableColumn;
	TextColumn<PurchaseOrder> deliveryDateColumn;
	TextColumn<PurchaseOrder> deliveryCityColumn;
	TextColumn<PurchaseOrder> poCreationDateColumn;
	TextColumn<PurchaseOrder> approverNameColumn;
	TextColumn<PurchaseOrder> categoryNameColumn;
	TextColumn<PurchaseOrder> typeColumn;

	@Override
	public void createTable() {
		
		addColumnPOid();
		addColumnPOTitle();
		
		addColumnPODate();
		addColumnVendorName();
		addColumnBranch();
		addColumnProductNames();
		addColumnEmployee();
		addColumnNetPayable();
		addColumnStatus();
		addColumnDeliveryDate();
		addColumnDeliveryCity();
		addColumnPurchaseReqno();
		addColumnReferenceOrderColumn();
		addColumnPOCreationDate();
		addColumnApproverName();
		addColumnCategory();
		addColumnType();
		
//		addColumnLoiType();
		addColumnProject();
		addColumnRfqID();
		addColumnLoiID();
		// addColumnPODate();
		// addColumnRfqDate();
//		addColumnClosureDateColumn();
		// addColumnResponseDateColumn();
	}

	private void addColumnType() {
		typeColumn = new TextColumn<PurchaseOrder>() {
			
			@Override
			public String getValue(PurchaseOrder object) {
				if(object.getType()!=null){
					return object.getType();
				}
				return "";
			}
		};
		table.addColumn(typeColumn, "Type");
		table.setColumnWidth(typeColumn, 80,Unit.PX);
		typeColumn.setSortable(true);
	}

	private void addColumnCategory() {
		categoryNameColumn = new TextColumn<PurchaseOrder>() {
			
			@Override
			public String getValue(PurchaseOrder object) {
				if(object.getCategory()!=null){
					return object.getCategory();
				}
				return "";
			}
		};
		table.addColumn(categoryNameColumn, "Category");
		table.setColumnWidth(categoryNameColumn, 80,Unit.PX);
		categoryNameColumn.setSortable(true);
	}

	private void addColumnApproverName() {
		approverNameColumn = new TextColumn<PurchaseOrder>() {
			
			@Override
			public String getValue(PurchaseOrder object) {
				if(object.getApproverName()!=null){
					return object.getApproverName();
				}
				return "";
			}
		};
		table.addColumn(approverNameColumn, "Approver Name");
		table.setColumnWidth(approverNameColumn, 80,Unit.PX);
		approverNameColumn.setSortable(true);
	}

	private void addColumnPOCreationDate() {
		poCreationDateColumn = new TextColumn<PurchaseOrder>() {
			
			@Override
			public String getValue(PurchaseOrder object) {
				if(object.getCreationDate()!=null){
					return AppUtility.parseDate(object.getCreationDate());
				}
				return "";
			}
		};
		table.addColumn(poCreationDateColumn, "PO Creation Date");
		table.setColumnWidth(poCreationDateColumn, 80,Unit.PX);
		poCreationDateColumn.setSortable(true);
	}

	private void addColumnDeliveryCity() {
		deliveryCityColumn = new TextColumn<PurchaseOrder>() {
			
			@Override
			public String getValue(PurchaseOrder object) {
				if(object.getAdress()!=null && object.getAdress().getCity()!=null){
					return object.getAdress().getCity();
				}
				return "";
			}
		};
		table.addColumn(deliveryCityColumn, "Delivery City");
		table.setColumnWidth(deliveryCityColumn, 80,Unit.PX);
		deliveryCityColumn.setSortable(true);
	}

	private void addColumnDeliveryDate() {
		deliveryDateColumn = new TextColumn<PurchaseOrder>() {
			
			@Override
			public String getValue(PurchaseOrder object) {
				if(object.getDeliveryDate()!=null){
					return AppUtility.parseDate(object.getDeliveryDate());
				}
				return "";
			}
		};
		table.addColumn(deliveryDateColumn, "Delivery Date");
		table.setColumnWidth(deliveryDateColumn, 90,Unit.PX);
		deliveryDateColumn.setSortable(true);
	}

	private void addColumnNetPayable() {

		netPayableColumn = new TextColumn<PurchaseOrder>() {
			
			@Override
			public String getValue(PurchaseOrder object) {
				return object.getNetpayble()+"";
			}
		};
		
		table.addColumn(netPayableColumn, "Net Payable");
		table.setColumnWidth(netPayableColumn, 80,Unit.PX);
		netPayableColumn.setSortable(true);
	}

	private void addColumnProductNames() {
		productNameColumn = new TextColumn<PurchaseOrder>() {
			
			@Override
			public String getValue(PurchaseOrder object) {
				String productName ="";
				
				for(int i=0;i<object.getProductDetails().size();i++){
					if(i==object.getProductDetails().size()-1){
						productName +=object.getProductDetails().get(i).getProductName();
					}
					else{
						productName +=object.getProductDetails().get(i).getProductName()+" / ";
					}
				}
				return productName;
			}
		};
		
		table.addColumn(productNameColumn, "Product Name");
		table.setColumnWidth(productNameColumn, 130,Unit.PX);
		productNameColumn.setSortable(true);
	}

	private void addColumnVendorName() {
		vendorNameColumn = new TextColumn<PurchaseOrder>() {
			
			@Override
			public String getValue(PurchaseOrder object) {
				if(object.getVendorDetails()!=null && object.getVendorDetails().size()!=0){
					for(VendorDetails vendordetails : object.getVendorDetails()){
						if(vendordetails.getStatus()){
							return vendordetails.getVendorName();
						}
					}
				}
				return "";
			}
		};
		
		table.addColumn(vendorNameColumn, "Vendor Name");
		table.setColumnWidth(vendorNameColumn, 120,Unit.PX);
		vendorNameColumn.setSortable(true);
	}
	
	public void addColumnSorting() {
		
		addColumnSortingPOid();
		addColumnSortingPOTitle();
		addColumnSortingPODate();
		addSortinggetBranch();
		addColumnSortingEmployee();
		addColumnSortingStatus();
		addColumnSortingDeliveryDate();
		addColumnSortingDeliveryCity();
		addSortingColumnPurchaseReqno();
		addColumnSortingReferenceOrderColumn();
		addColumnSortingPOCreationDateColumn();
		addColumnSortingApproverNameColumn();
		addColumnSortingCategory();
		addColumnSortingType();
		addSortingColumnProject();
		addColumnSortingRfqID();
		addColumnSortingLoiID();

		// addColumnRfqDate();
//		addColumnSortingClosureDateColumn();
//		addColumnSortingResponseDateColumn();
//		addColumnSortingLoiGroup();
//		addColumnSortingLoiType();

	}


	private void addColumnSortingType() {

		List<PurchaseOrder> list = getDataprovider().getList();
		columnSort = new ListHandler<PurchaseOrder>(list);
		columnSort.setComparator(typeColumn,
				new Comparator<PurchaseOrder>() {
					@Override
					public int compare(PurchaseOrder e1, PurchaseOrder e2) {
						if (e1 != null && e2 != null) {
							if (e1.getType() != null
									&& e2.getType() != null) {
								return e1.getType().compareTo(
										e2.getType());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	
	
	}

	private void addColumnSortingCategory() {
		List<PurchaseOrder> list = getDataprovider().getList();
		columnSort = new ListHandler<PurchaseOrder>(list);
		columnSort.setComparator(categoryNameColumn,
				new Comparator<PurchaseOrder>() {
					@Override
					public int compare(PurchaseOrder e1, PurchaseOrder e2) {
						if (e1 != null && e2 != null) {
							if (e1.getCategory() != null
									&& e2.getCategory() != null) {
								return e1.getCategory().compareTo(
										e2.getCategory());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	
	}

	private void addColumnSortingApproverNameColumn() {

		List<PurchaseOrder> list = getDataprovider().getList();
		columnSort = new ListHandler<PurchaseOrder>(list);
		columnSort.setComparator(approverNameColumn,
				new Comparator<PurchaseOrder>() {
					@Override
					public int compare(PurchaseOrder e1, PurchaseOrder e2) {
						if (e1 != null && e2 != null) {
							if (e1.getApproverName() != null
									&& e2.getApproverName() != null) {
								return e1.getApproverName().compareTo(
										e2.getApproverName());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);

	
	}

	private void addColumnSortingPOCreationDateColumn() {

		List<PurchaseOrder> list = getDataprovider().getList();
		columnSort = new ListHandler<PurchaseOrder>(list);
		columnSort.setComparator(poCreationDateColumn, new Comparator<PurchaseOrder>() {
			@Override
			public int compare(PurchaseOrder e1, PurchaseOrder e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCreatedDate() != null
							&& e2.getCreatedDate() != null) {
						return e1.getCreatedDate().compareTo(
								e2.getCreatedDate());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);

	
		
	}

	private void addColumnSortingDeliveryCity() {

		List<PurchaseOrder> list = getDataprovider().getList();
		columnSort = new ListHandler<PurchaseOrder>(list);
		columnSort.setComparator(deliveryCityColumn,
				new Comparator<PurchaseOrder>() {
					@Override
					public int compare(PurchaseOrder e1, PurchaseOrder e2) {
						if (e1 != null && e2 != null) {
							if (e1.getAdress() != null
									&& e2.getAdress() != null) {
								return e1.getAdress().getCity().compareTo(
										e2.getAdress().getCity());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);

	
	}

	private void addColumnSortingDeliveryDate() {
		List<PurchaseOrder> list = getDataprovider().getList();
		columnSort = new ListHandler<PurchaseOrder>(list);
		columnSort.setComparator(deliveryDateColumn, new Comparator<PurchaseOrder>() {
			@Override
			public int compare(PurchaseOrder e1, PurchaseOrder e2) {
				if (e1 != null && e2 != null) {
					if (e1.getDeliveryDate() != null
							&& e2.getDeliveryDate() != null) {
						return e1.getDeliveryDate().compareTo(
								e2.getDeliveryDate());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	/********************************** sorting column *************************/

	public void addSortingColumnPurchaseReqno() {
		List<PurchaseOrder> list = getDataprovider().getList();
		columnSort = new ListHandler<PurchaseOrder>(list);
		columnSort.setComparator(ColumnPurchaseReqno,
				new Comparator<PurchaseOrder>() {
					@Override
					public int compare(PurchaseOrder e1, PurchaseOrder e2) {
						if (e1 != null && e2 != null) {
							if (e1.getPurchaseReqNo() == e2.getPurchaseReqNo()) {
								return 0;
							}
							if (e1.getPurchaseReqNo() > e2.getPurchaseReqNo()) {
								return 1;
							} else {
								return -1;
							}
						} else {
							return 0;
						}
					}
				});
		table.addColumnSortHandler(columnSort);

	}

	public void addSortingColumnProject() {
		List<PurchaseOrder> list = getDataprovider().getList();
		columnSort = new ListHandler<PurchaseOrder>(list);
		columnSort.setComparator(ColumnProject,
				new Comparator<PurchaseOrder>() {
					@Override
					public int compare(PurchaseOrder e1, PurchaseOrder e2) {
						if (e1 != null && e2 != null) {
							if (e1.getProject() != null
									&& e2.getProject() != null) {
								return e1.getProject().compareTo(
										e2.getProject());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);

	}

	public void addColumnSortingPOid() {
		List<PurchaseOrder> list = getDataprovider().getList();
		columnSort = new ListHandler<PurchaseOrder>(list);
		columnSort.setComparator(poIDColumn, new Comparator<PurchaseOrder>() {
			@Override
			public int compare(PurchaseOrder e1, PurchaseOrder e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCount() == e2.getCount()) {
						return 0;
					}
					if (e1.getCount() > e2.getCount()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);

	}

	public void addColumnSortingPOTitle() {
		List<PurchaseOrder> list = getDataprovider().getList();
		columnSort = new ListHandler<PurchaseOrder>(list);
		columnSort.setComparator(potitleColumn,
				new Comparator<PurchaseOrder>() {
					@Override
					public int compare(PurchaseOrder e1, PurchaseOrder e2) {
						if (e1 != null && e2 != null) {
							if (e1.getPOName() != null
									&& e2.getPOName() != null) {
								return e1.getPOName().compareTo(e2.getPOName());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);

	}

	public void addColumnSortingLoiID() {
		List<PurchaseOrder> list = getDataprovider().getList();
		columnSort = new ListHandler<PurchaseOrder>(list);
		columnSort.setComparator(loiIDColumn, new Comparator<PurchaseOrder>() {
			@Override
			public int compare(PurchaseOrder e1, PurchaseOrder e2) {
				if (e1 != null && e2 != null) {
					if (e1.getLOIId() == e2.getLOIId()) {
						return 0;
					}
					if (e1.getLOIId() > e2.getLOIId()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);

	}

	public void addColumnSortingRfqID() {
		List<PurchaseOrder> list = getDataprovider().getList();
		columnSort = new ListHandler<PurchaseOrder>(list);
		columnSort.setComparator(rfqIDColumn, new Comparator<PurchaseOrder>() {
			@Override
			public int compare(PurchaseOrder e1, PurchaseOrder e2) {
				if (e1 != null && e2 != null) {
					if (e1.getRFQID() == e2.getRFQID()) {
						return 0;
					}
					if (e1.getRFQID() > e2.getRFQID()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);

	}

	public void addColumnSortingPODate() {
		List<PurchaseOrder> list = getDataprovider().getList();
		columnSort = new ListHandler<PurchaseOrder>(list);
		columnSort.setComparator(dateColumn, new Comparator<PurchaseOrder>() {
			@Override
			public int compare(PurchaseOrder e1, PurchaseOrder e2) {
				if (e1 != null && e2 != null) {
					if (e1.getPODate() != null
							&& e2.getPODate() != null) {
						return e1.getPODate().compareTo(
								e2.getPODate());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);

	}

	public void addColumnSortingClosureDateColumn() {
		List<PurchaseOrder> list = getDataprovider().getList();
		columnSort = new ListHandler<PurchaseOrder>(list);
		columnSort.setComparator(closureDateColumn,
				new Comparator<PurchaseOrder>() {
					@Override
					public int compare(PurchaseOrder e1, PurchaseOrder e2) {
						if (e1 != null && e2 != null) {
							if (e1.getExpectedClosureDate() != null
									&& e2.getExpectedClosureDate() != null) {
								return e1.getExpectedClosureDate().compareTo(
										e2.getExpectedClosureDate());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);

	}

	public void addColumnSortingResponseDateColumn() {
		List<PurchaseOrder> list = getDataprovider().getList();
		columnSort = new ListHandler<PurchaseOrder>(list);
		columnSort.setComparator(closureDateColumn,
				new Comparator<PurchaseOrder>() {
					@Override
					public int compare(PurchaseOrder e1, PurchaseOrder e2) {
						if (e1 != null && e2 != null) {
							if (e1.getResponseDate() != null
									&& e2.getResponseDate() != null) {
								return e1.getResponseDate().compareTo(
										e2.getResponseDate());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);

	}

	public void addColumnSortingReferenceOrderColumn() {
		List<PurchaseOrder> list = getDataprovider().getList();
		columnSort = new ListHandler<PurchaseOrder>(list);
		columnSort.setComparator(referenceOrderColumn,new Comparator<PurchaseOrder>() {
					@Override
					public int compare(PurchaseOrder e1, PurchaseOrder e2) {
						if (e1 != null && e2 != null) {
							if (e1.getRefOrderNO() != null && e2.getRefOrderNO() != null) {
								return e1.getRefOrderNO().compareTo(e2.getRefOrderNO());
						}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);

	}

	public void addColumnSortingEmployee() {
		List<PurchaseOrder> list = getDataprovider().getList();
		columnSort = new ListHandler<PurchaseOrder>(list);
		columnSort.setComparator(employeeColumn,
				new Comparator<PurchaseOrder>() {
					@Override
					public int compare(PurchaseOrder e1, PurchaseOrder e2) {
						if (e1 != null && e2 != null) {
							if (e1.getEmployee() != null
									&& e2.getEmployee() != null) {
								return e1.getEmployee().compareTo(
										e2.getEmployee());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);

	}

	public void addColumnSortingLoiGroup() {
		List<PurchaseOrder> list = getDataprovider().getList();
		columnSort = new ListHandler<PurchaseOrder>(list);
		columnSort.setComparator(loiGroupColumn,
				new Comparator<PurchaseOrder>() {
					@Override
					public int compare(PurchaseOrder e1, PurchaseOrder e2) {
						if (e1 != null && e2 != null) {
							if (e1.getLOIGroup() != null
									&& e2.getLOIGroup() != null) {
								return e1.getLOIGroup().compareTo(
										e2.getLOIGroup());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);

	}

	public void addColumnSortingLoiType() {
		List<PurchaseOrder> list = getDataprovider().getList();
		columnSort = new ListHandler<PurchaseOrder>(list);
		columnSort.setComparator(loitypeColumn,new Comparator<PurchaseOrder>() {
					@Override
					public int compare(PurchaseOrder e1, PurchaseOrder e2) {
						if (e1 != null && e2 != null) {
							if (e1.getLOItype() != null && e2.getLOItype() != null) {
								return e1.getLOItype().compareTo(e2.getLOItype());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);

	}

	public void addColumnSortingStatus() {
		List<PurchaseOrder> list = getDataprovider().getList();
		columnSort = new ListHandler<PurchaseOrder>(list);
		columnSort.setComparator(statusColumn, new Comparator<PurchaseOrder>() {
			@Override
			public int compare(PurchaseOrder e1, PurchaseOrder e2) {
				if (e1 != null && e2 != null) {
					if (e1.getStatus() != null && e2.getStatus() != null) {
						return e1.getStatus().compareTo(e2.getStatus());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);

	}
	
	protected void addSortinggetBranch()
	{
		List<PurchaseOrder> list=getDataprovider().getList();
		columnSort=new ListHandler<PurchaseOrder>(list);
		columnSort.setComparator(branchColumn, new Comparator<PurchaseOrder>()
				{
			@Override
			public int compare(PurchaseOrder e1,PurchaseOrder e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBranch()!=null && e2.getBranch()!=null){
						return e1.getBranch().compareTo(e2.getBranch());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	

	/********************************************************************************/

	public void addColumnPurchaseReqno() {
		ColumnPurchaseReqno = new TextColumn<PurchaseOrder>() {

			@Override
			public String getValue(PurchaseOrder object) {
				if(object.getPurchaseReqNo()!=0){
					return object.getPurchaseReqNo() + "";
				}
				return "";
			}
		};
		table.addColumn(ColumnPurchaseReqno, "PR Number");
		table.setColumnWidth(ColumnPurchaseReqno, 80,Unit.PX);
		ColumnPurchaseReqno.setSortable(true);
	}

	public void addColumnProject() {
		ColumnProject = new TextColumn<PurchaseOrder>() {

			@Override
			public String getValue(PurchaseOrder object) {
				if(object.getProject()!=null){
					return object.getProject();
				}
				return "";
			}
		};
		table.addColumn(ColumnProject, "Project");
		table.setColumnWidth(ColumnProject, 80,Unit.PX);
		ColumnProject.setSortable(true);
	}

	public void addColumnLoiID() {
		loiIDColumn = new TextColumn<PurchaseOrder>() {

			@Override
			public String getValue(PurchaseOrder object) {
				if(object.getLOIId()!=0){
					return object.getLOIId() + "";
				}
				return "";
			}
		};
		table.addColumn(loiIDColumn, "LOI ID");
		table.setColumnWidth(loiIDColumn, 80,Unit.PX);
		loiIDColumn.setSortable(true);
	}

	public void addColumnPOTitle() {
		potitleColumn = new TextColumn<PurchaseOrder>() {

			@Override
			public String getValue(PurchaseOrder object) {
				return object.getPOName();
			}
		};
		table.addColumn(potitleColumn, "Title");
		table.setColumnWidth(potitleColumn, 100,Unit.PX);
		potitleColumn.setSortable(true);
	}

	public void addColumnPODate() {
		dateColumn = new TextColumn<PurchaseOrder>() {

			@Override
			public String getValue(PurchaseOrder object) {
				return AppUtility.parseDate(object.getPODate());
			}
		};
		table.addColumn(dateColumn, "PO Date");
		table.setColumnWidth(dateColumn, 90,Unit.PX);
		dateColumn.setSortable(true);

	}

	public void addColumnStatus() {
		statusColumn = new TextColumn<PurchaseOrder>() {

			@Override
			public String getValue(PurchaseOrder object) {
				return object.getStatus();
			}
		};
		table.addColumn(statusColumn, "Status");
		table.setColumnWidth(statusColumn, 80,Unit.PX);
		statusColumn.setSortable(true);
	}
	public void addColumnBranch()
	{
		branchColumn=new TextColumn<PurchaseOrder>() {

			@Override
			public String getValue(PurchaseOrder object) {
				return object.getBranch();
			}
		};
		
		table.addColumn(branchColumn,"Branch");
		table.setColumnWidth(branchColumn, 100, Unit.PX);
		branchColumn.setSortable(true);
	}
	public void addColumnLoiType() {
		loitypeColumn = new TextColumn<PurchaseOrder>() {

			@Override
			public String getValue(PurchaseOrder object) {
				return object.getLOItype();
			}
		};
		table.addColumn(loitypeColumn, "Type");
		loitypeColumn.setSortable(true);
	}

	public void addColumnLoiGroup() {
		loiGroupColumn = new TextColumn<PurchaseOrder>() {

			@Override
			public String getValue(PurchaseOrder object) {
				return object.getLOIGroup();
			}
		};
		table.addColumn(loiGroupColumn, "Category");
		loiGroupColumn.setSortable(true);
	}

	public void addColumnEmployee() {
		employeeColumn = new TextColumn<PurchaseOrder>() {

			@Override
			public String getValue(PurchaseOrder object) {
				return object.getEmployee();
			}
		};
		table.addColumn(employeeColumn, "Purchase Engineer");
		table.setColumnWidth(employeeColumn, 80,Unit.PX);
		employeeColumn.setSortable(true);
	}

	public void addColumnReferenceOrderColumn() {
		referenceOrderColumn = new TextColumn<PurchaseOrder>() {

			@Override
			public String getValue(PurchaseOrder object) {
				return object.getRefOrderNO() + "";
			}
		};
		table.addColumn(referenceOrderColumn, "Reference order Number");
		table.setColumnWidth(referenceOrderColumn, 80,Unit.PX);
		referenceOrderColumn.setSortable(true);
	}

	public void addColumnResponseDateColumn() {
		responseDateColumn = new TextColumn<PurchaseOrder>() {

			@Override
			public String getValue(PurchaseOrder object) {
				return AppUtility.parseDate(object.getResponseDate());
			}
		};
		table.addColumn(responseDateColumn, "Resp.Date");
		responseDateColumn.setSortable(true);
	}

	public void addColumnClosureDateColumn() {
		closureDateColumn = new TextColumn<PurchaseOrder>() {

			@Override
			public String getValue(PurchaseOrder object) {
				if (object.getExpectedClosureDate() != null)
					return AppUtility.parseDate(object.getExpectedClosureDate());
				else
					return "";
			}
		};
		table.addColumn(closureDateColumn, "Closure Date");
		closureDateColumn.setSortable(true);
	}

	public void addColumnRfqDate() {
		RfqDateColumn = new TextColumn<PurchaseOrder>() {

			@Override
			public String getValue(PurchaseOrder object) {
				return AppUtility.parseDate(object.getResponseDate());
			}
		};
		table.addColumn(responseDateColumn, "RFQ Date");
		RfqDateColumn.setSortable(true);
	}

	public void addColumnPOid() {
		poIDColumn = new TextColumn<PurchaseOrder>() {

			@Override
			public String getValue(PurchaseOrder object) {
				return object.getCount() + "";
			}
		};
		table.addColumn(poIDColumn, "ID");
		table.setColumnWidth(poIDColumn, 90,Unit.PX);

		poIDColumn.setSortable(true);
		
	}

	public void addColumnRfqID() {
		rfqIDColumn = new TextColumn<PurchaseOrder>() {

			@Override
			public String getValue(PurchaseOrder object) {
				if(object.getRFQID()!=0){
					return object.getRFQID() + "";
				}
				return "";
			}
		};
		table.addColumn(rfqIDColumn, "RFQ ID");
		table.setColumnWidth(rfqIDColumn, 90,Unit.PX);
		rfqIDColumn.setSortable(true);
	}

	@Override
	protected void initializekeyprovider() {

	}

	@Override
	public void addFieldUpdater() {

	}

	@Override
	public void setEnable(boolean state) {

	}

	@Override
	public void applyStyle() {

	}
}
