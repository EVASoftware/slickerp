package com.slicktechnologies.client.views.purchase.purchaseorder;

import java.util.Vector;

import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.config.CategoryTypeFactory;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;

public class PuchaseOrderPresenterSearchProxy extends
		SearchPopUpScreen<PurchaseOrder> {

	IntegerBox ibReqQuotationID, ibLOIid;
	TextBox ibRefOrderNo;
	ObjectListBox<Employee> oblEmployee;

	ObjectListBox<ConfigCategory> oblPOCategory;
	ObjectListBox<Type> oblPOType;

	DateComparator dateComparator, creationdateComparator;
	// ObjectListBox<Branch> branch;
	PersonInfoComposite vic;
	ProductInfoComposite pic;

	ListBox status;

	TextBox tbpoTitle;
	IntegerBox ibpoID;
	ListBox lbstatus;

	ObjectListBox<HrProject> project;

	IntegerBox purchaseRequisiteNo;

	/**
	 * Date : 08-02-2017 By Anil Added Branch Search
	 */
	ObjectListBox<Branch> olbBranch;

	/**
	 * End
	 */

	public PuchaseOrderPresenterSearchProxy() {
		super();
		createGui();
	}

	public void initWidget() {
		ibReqQuotationID = new IntegerBox();
		ibRefOrderNo = new TextBox();
		ibLOIid = new IntegerBox();

		oblEmployee = new ObjectListBox<Employee>();
//		initalizEmployeeNameListBox();
		oblEmployee.makeEmployeeLive(AppConstants.PURCHASEMODULE, AppConstants.PURCHASEORDER, "Purchase Requisition");
		status = new ListBox();

		oblPOCategory = new ObjectListBox<ConfigCategory>();
		oblPOType = new ObjectListBox<Type>();
		CategoryTypeFactory.initiateOneManyFunctionality(oblPOCategory,
				oblPOType);
		AppUtility.makeTypeListBoxLive(oblPOType, Screen.PURCHASEORDERCATEGORY);

		dateComparator = new DateComparator("PODate", new PurchaseOrder());
		// branch = new ObjectListBox<Branch>();

		vic = AppUtility.vendorInfoComposite(new Vendor());
		pic = AppUtility.product(new SuperProduct(), false);

		tbpoTitle = new TextBox();
		ibpoID = new IntegerBox();
		lbstatus = new ListBox();
		AppUtility.setStatusListBox(lbstatus, PurchaseOrder.getStatusList());

		project = new ObjectListBox<HrProject>();
		HrProject.MakeProjectListBoxLive(project);
		purchaseRequisiteNo = new IntegerBox();
		vic.getCustomerId().getHeaderLabel().setText("Vendor Id");
		vic.getCustomerName().getHeaderLabel().setText("Vendor Name");
		vic.getCustomerCell().getHeaderLabel().setText("Vendor Cell");

		olbBranch = new ObjectListBox<Branch>();
		initalizEmployeeBranchListBox();
		
		/**Added by sheetal:31-12-2021
		   Des:Adding search fliter for PO Creation date requirement by Envocare**/
		creationdateComparator = new DateComparator("creationDate", new PurchaseOrder());//Ashwini Patil Date:5-07-2022 changed CreatedDate to creationDate
		/**end**/
	}

	protected void initalizEmployeeBranchListBox() {
		MyQuerry querry = new MyQuerry(new Vector<Filter>(), new Branch());
		olbBranch.MakeLive(querry);
	}

	public void createScreen() {

		initWidget();
		FormFieldBuilder fbuilder;

		fbuilder = new FormFieldBuilder("RFQ ID", ibReqQuotationID);
		FormField fibReqQuotationID = fbuilder.setMandatory(false)
				.setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("LOI ID", ibLOIid);
		FormField floiID = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("RefOdrNo.", ibRefOrderNo);
		FormField fibRefOrderNo = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Pur. Eng.", oblEmployee);
		FormField femployee = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("PO Category", oblPOCategory);
		FormField fLOIgrpup = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("PO Type", oblPOType);
		FormField fLOItype = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("PO From Date",
				dateComparator.getFromDate());
		FormField fcreationdate = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("PO To Date",
				dateComparator.getToDate());
		FormField fcreationtodate = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("PO Creation From Date",
				creationdateComparator.getFromDate());
		FormField fPOcreationFromdate = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("PO Creation To Date",
				creationdateComparator.getToDate());
		FormField fPOcreationTodate = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("", vic);
		FormField fvic = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(4).build();

		fbuilder = new FormFieldBuilder("", pic);
		FormField fpic = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(4).build();

		fbuilder = new FormFieldBuilder("PO Title", tbpoTitle);
		FormField ftitle = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("PO ID", ibpoID);
		FormField fpoid = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Status", lbstatus);
		FormField fstatus = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Project", project);
		FormField fproject = fbuilder.setMandatory(false)
				.setMandatoryMsg("Project is mandatory").setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder(" PR Number", purchaseRequisiteNo);
		FormField fpuchasereq = fbuilder.setMandatory(false)
				.setMandatoryMsg("Branch is mandatory").setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Branch", olbBranch);
		FormField folbBranch = fbuilder.setMandatory(false)
				.setMandatoryMsg("Branch is mandatory").setRowSpan(0)
				.setColSpan(0).build();

		FormField[][] formfield = {
				{ fpoid, folbBranch, fcreationdate, fcreationtodate },
				{ fLOIgrpup, fLOItype, femployee, fproject },
				{ floiID, fibReqQuotationID, fpuchasereq, fibRefOrderNo },
				{ ftitle, fstatus, fPOcreationFromdate, fPOcreationTodate }, { fvic }, { fpic } };

		this.fields = formfield;

	}

	public MyQuerry getQuerry() {
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter temp = null;

		if (project.getSelectedIndex() != 0) {
			temp = new Filter();
			int item = project.getSelectedIndex();
			String sel = project.getItemText(item);
			temp.setStringValue(sel);
			temp.setQuerryString("project");
			filtervec.add(temp);
		}

		if (purchaseRequisiteNo.getValue() != null) {
			temp = new Filter();
			temp.setIntValue(purchaseRequisiteNo.getValue());
			temp.setQuerryString("purchaseReqNo");
			filtervec.add(temp);

		}

		if (lbstatus.getSelectedIndex() != 0) {
			temp = new Filter();
			temp.setStringValue(lbstatus.getValue(lbstatus.getSelectedIndex()));
			temp.setQuerryString("status");
			filtervec.add(temp);

		}
		if (!tbpoTitle.getValue().equals("")) {
			temp = new Filter();
			temp.setStringValue(tbpoTitle.getValue());
			temp.setQuerryString("POName");
			filtervec.add(temp);
		}
		if (ibpoID.getValue() != null) {
			temp = new Filter();
			temp.setIntValue(ibpoID.getValue());
			temp.setQuerryString("count");
			filtervec.add(temp);
		}

		if (pic.getValue()!=null&&!pic.getProdName().getValue().equals("")) {
			temp = new Filter();
			temp.setStringValue(pic.getProdName().getValue());
			temp.setQuerryString("productDetails.productdetails.productName");
			filtervec.add(temp);
		}
		if (pic.getValue()!=null&&!pic.getProdCode().getValue().equals("")) {
			temp = new Filter();
			temp.setStringValue(pic.getProdCode().getValue());
			temp.setQuerryString("productDetails.productdetails.productCode");
			filtervec.add(temp);
		}
		if (pic.getValue()!=null&&!pic.getProdID().getValue().equals("")) {
			temp = new Filter();
			temp.setIntValue(Integer.parseInt(pic.getProdID().getValue()));
			temp.setQuerryString("productDetails.productdetails.productID");
			filtervec.add(temp);
		}

//Ashwini Patil Date:29-08-2022 commented below code because when vendor name changes search results were not showing old records. So applying only id filter.
//		if (vic.getValue()!=null&&!vic.getName().getValue().equals("")) {
//			temp = new Filter();
//			temp.setStringValue(vic.getName().getValue());
//			temp.setQuerryString("vendorDetails.vendorName");
//			filtervec.add(temp);
//			
//		}

		if (vic.getValue()!=null&&!vic.getId().getValue().equals("")) {
			temp = new Filter();
			temp.setIntValue(Integer.parseInt(vic.getId().getValue()));
			temp.setQuerryString("vendorDetails.vendorId");
			filtervec.add(temp);
			
		}

		if (ibReqQuotationID.getValue() != null) {
			temp = new Filter();
			temp.setIntValue(ibReqQuotationID.getValue());
			temp.setQuerryString("RFQID");
			filtervec.add(temp);
		}
		if (ibLOIid.getValue() != null) {
			temp = new Filter();
			temp.setIntValue(ibLOIid.getValue());
			temp.setQuerryString("LOIId");
			filtervec.add(temp);
		}
		if (!ibRefOrderNo.getValue().equals("")) {
			temp = new Filter();
			temp.setStringValue(ibRefOrderNo.getValue());
			temp.setQuerryString("refOrderNO");
			filtervec.add(temp);
		}
		if (oblEmployee.getValue() != null) {
			temp = new Filter();
			temp.setStringValue(oblEmployee.getValue());
			temp.setQuerryString("employee");
			filtervec.add(temp);
		}
		if (oblPOCategory.getValue() != null) {
			temp = new Filter();
			temp.setStringValue(oblPOCategory.getValue());
			temp.setQuerryString("LOIGroup");
			filtervec.add(temp);
		}
		if (oblPOType.getValue() != null) {
			temp = new Filter();
			temp.setStringValue(oblPOType.getValue());
			temp.setQuerryString("LOItype");
			filtervec.add(temp);
		}
		if (dateComparator.getValue() != null) {
			filtervec.addAll(dateComparator.getValue());
		}
		
		if (creationdateComparator.getValue() != null) {
			filtervec.addAll(creationdateComparator.getValue());
		}
		
		
		if (olbBranch.getSelectedIndex() != 0) {
			temp = new Filter();
			temp.setStringValue(olbBranch.getValue());
			temp.setQuerryString("branch");
			filtervec.add(temp);
		}

		MyQuerry querry = new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new PurchaseOrder());
		return querry;
	}

	protected void initalizEmployeeNameListBox() {
		MyQuerry querry = new MyQuerry(new Vector<Filter>(), new Employee());
		oblEmployee.MakeLive(querry);
	}

	@Override
	public boolean validate() {
		/**
		 * @author Anil @since 31-03-2021
		 */
		if (LoginPresenter.branchRestrictionFlag&&!UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin")) {
			if (olbBranch.getSelectedIndex() == 0) {
				showDialogMessage("Please select branch.");
				return false;
			}
		}

//		ProductInfoComposite pic;//
		//Ashwini Patil Date:25-01-2024 	
		boolean filterAppliedFlag=false;
		Console.log("filterAppliedFlag before="+filterAppliedFlag);
		if(dateComparator.getFromDate().getValue()!=null || dateComparator.getToDate().getValue()!=null||
				creationdateComparator.getFromDate().getValue()!=null || creationdateComparator.getToDate().getValue()!=null||
				olbBranch.getSelectedIndex()!=0 ||lbstatus.getSelectedIndex()!=0 ||
				oblEmployee.getSelectedIndex()!=0 || vic.getIdValue()!=-1	|| !(vic.getFullNameValue().equals("")) || vic.getCellValue()!=-1l ||
				ibReqQuotationID.getValue()!=null || ibLOIid.getValue()!=null ||
			    oblPOCategory.getSelectedIndex()!=0 ||oblPOType.getSelectedIndex()!=0 || 
			    project.getSelectedIndex()!=0 || (ibRefOrderNo.getValue()!=null && !ibRefOrderNo.getValue().equals("")) || 
			    ibpoID.getValue()!= null ||(tbpoTitle.getValue()!=null && !tbpoTitle.getValue().equals(""))  || purchaseRequisiteNo.getValue()!=null||
			    pic.getValue()!=null){
			
							filterAppliedFlag=true;
		}
		Console.log("filterAppliedFlag after="+filterAppliedFlag);				
		if(!filterAppliedFlag){
				showDialogMessage("Please apply at least one filter!");
				return false;
		}
					
		if((dateComparator.getFromDate().getValue()!=null && dateComparator.getToDate().getValue()!=null)){
				int diffdays = AppUtility.getDifferenceDays(dateComparator.getFromDate().getValue(), dateComparator.getToDate().getValue());
				Console.log("diffdays "+diffdays);
				if(diffdays>31){
						showDialogMessage("Please select from date and to date range within 30 days");
						return false;
				}
		}

		if((creationdateComparator.getFromDate().getValue()!=null && creationdateComparator.getToDate().getValue()!=null)){
			int diffdays = AppUtility.getDifferenceDays(creationdateComparator.getFromDate().getValue(), creationdateComparator.getToDate().getValue());
			Console.log("diffdays "+diffdays);
			if(diffdays>31){
					showDialogMessage("Please select from date and to date range within 30 days");
					return false;
			}
	}

		return true;
	}

	
}
