package com.slicktechnologies.client.views.purchase.purchaseorder;

public class PurchaseOrderVendors {
	
	protected int prodId;
	protected int vendorId;
	protected String vendorName;
	protected String vendorEmail;
	protected long vendorCellNumber;
	//vijay
	protected String vendorPocName;
	
	public int getProdId() {
		return prodId;
	}
	public void setProdId(int prodId) {
		this.prodId = prodId;
	}
	public int getVendorId() {
		return vendorId;
	}
	public void setVendorId(int vendorId) {
		this.vendorId = vendorId;
	}
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	public String getVendorEmail() {
		return vendorEmail;
	}
	public void setVendorEmail(String vendorEmail) {
		this.vendorEmail = vendorEmail;
	}
	public long getVendorCellNumber() {
		return vendorCellNumber;
	}
	public void setVendorCellNumber(long vendorCellNumber) {
		this.vendorCellNumber = vendorCellNumber;
	}
	public String getVendorPocName() {
		return vendorPocName;
	}
	public void setVendorPocName(String vendorPocName) {
		this.vendorPocName = vendorPocName;
	}
	
	
	
	
	
	
}
