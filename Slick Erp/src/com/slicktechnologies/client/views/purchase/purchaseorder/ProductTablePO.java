package com.slicktechnologies.client.views.purchase.purchaseorder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.code.p.gwtchismes.client.GWTCGlassPanel;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.SelectionCell;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.purchase.letterofintent.LetterOfIntentPresenter;
import com.slicktechnologies.client.views.purchase.letterofintent.ProductTableLOI;
import com.slicktechnologies.client.views.salesorder.ItemProductDetailsPopUp;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.TaxDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetailsPO;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.productlayer.Tax;

public class ProductTablePO extends SuperTable<ProductDetailsPO> implements ClickHandler {

	NumberFormat nf = NumberFormat.getFormat("0.00");

	TextColumn<ProductDetailsPO> columnProductID;
	TextColumn<ProductDetailsPO> columnProductName;
	TextColumn<ProductDetailsPO> columnProductCode;
	TextColumn<ProductDetailsPO> columnProductCategory;
	TextColumn<ProductDetailsPO> columnPriceColumn;
	TextColumn<ProductDetailsPO> columnProdQtyColumn;
	TextColumn<ProductDetailsPO> columnunitOfmeasurement;
	
	TextColumn<ProductDetailsPO> columnServiceTax;
	TextColumn<ProductDetailsPO> columnVat;
	TextColumn<ProductDetailsPO> columnDiscount;
	TextColumn<ProductDetailsPO> nonEditcolumnDiscount;

	Column<ProductDetailsPO, String> editprodname; //Ashiwni Patil Date:16-02-2022
	Column<ProductDetailsPO, String> columnProductPrice;
	Column<ProductDetailsPO, String> columnProductQuantity;
	Column<ProductDetailsPO, String> ecolumnDiscount;
	Column<ProductDetailsPO, String> columnTotal;
	Column<ProductDetailsPO, String> deleteColumn;
	
	Column<ProductDetailsPO, String> discountAmt;
	TextColumn<ProductDetailsPO> viewdiscountAmt;
	
	ArrayList<TaxDetails> vattaxlist;
	ArrayList<TaxDetails> servicetaxlist;
	ArrayList<String> list;
	ArrayList<String> vatlist;
	Column<ProductDetailsPO,String> editVatColumn,editServiceColumn;
	
	
	
	
	
	/**
	 * warehouse ,exp. delivery date and avilable stock coloumn is added.
	 * Date : 29-09-2016 By Anil
	 * Release :30 Sept 2016
	 * Project: PURCHASE MODIFICATION (NBHC)
	 */
	TextColumn<ProductDetailsPO> warehouseColumn;
	Column<ProductDetailsPO,String> warehouseListColumn;
	TextColumn<ProductDetailsPO> expDeliveryDateColumn;
	Column<ProductDetailsPO,Date> editableExpDeliveryDateColumn;
	TextColumn<ProductDetailsPO> availableStockColumn;
	ArrayList<String> warehouseList;
	static ArrayList<WareHouse> warehouseEntityList;
	protected GWTCGlassPanel glassPanel;
	public static boolean isValidWarehouse=true;
	
	/**
	 * End
	 */

	
	
	/**
	 * Vijay added this code for adding HSN code in prouct
	 */
	TextColumn<ProductDetailsPO> viewHsnCodeColumn;
	Column<ProductDetailsPO,String> hsnCodeColumn;
	/**
	 * ends here  
	 */
	
	/**
	 * Date 26-04-2017 by vijay
	 * Warehouse Address column
	 * Project : Deadstock (NBHC)
	 */
	TextColumn<ProductDetailsPO> warehouseAddressColumn; 
	/**
	 * ends here
	 */
	
	
	/**
	 * Date : 19-04-2018 BY ANIL
	 * added inclusive tax col for HVAC/ROHAN/NITIN SIR
	 */
	
	Column<ProductDetailsPO,Boolean> getColIsTaxInclusive;
	TextColumn<ProductDetailsPO> viewColIsTaxInclusive;
	
	TextColumn<ProductDetailsPO> getSalesPriceCol;
	
    int rowIndex;
    
    /**
     * @author Anil
     * @since 20-07-2020
     */
    boolean marginFlag=AppUtility.checkForProcessConfigurartionIsActiveOrNot("PurchaseOrder","CalculateVendorAndOtherChargesMargin");
    
	public ProductTablePO() {
		super();
		table.setHeight("300px");
		
	}

	public ProductTablePO(UiScreen<ProductDetailsPO> view) {
		super(view);
		table.setHeight("300px");
	
	}

	@Override
	public void createTable() {
		marginFlag=AppUtility.checkForProcessConfigurartionIsActiveOrNot("PurchaseOrder","CalculateVendorAndOtherChargesMargin");
		
		addColumnProductName();
		
		
		if(!marginFlag){
			addColumnProductCategory();
			getSalesPriceCol();
		}
		/** date 12.10.2018 added by komal to make price non-editable **/
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("PurchaseOrder", "MakePriceNonEditable")){
			addColumnViewProductPrice();
		}else{
			addColumnProductPrice();
		}
		
		retriveServiceTax();
		
	
		}
	
	

	
	private void getSalesPriceCol(){
		getSalesPriceCol=new TextColumn<ProductDetailsPO>() {
			
			@Override
			public String getValue(ProductDetailsPO object) {
				// TODO Auto-generated method stub
				return nf.format(object.getSalesPrice());
			}
		};
		table.addColumn(getSalesPriceCol, "Sales Price");
		table.setColumnWidth(getSalesPriceCol, 90, Unit.PX);
		getSalesPriceCol.setCellStyleNames("wordWrap");
	}
	
	private void getColIsTaxInclusive() {
		CheckboxCell cell=new CheckboxCell();
		getColIsTaxInclusive=new Column<ProductDetailsPO, Boolean>(cell) {
			@Override
			public Boolean getValue(ProductDetailsPO object) {
				
//				if(object.getVatTax()!=null&&object.getServiceTax()!=null){
//					if(object.getVatTax().isInclusive()==true&&object.getServiceTax().isInclusive()==true){
//						return true;
//					}else if(object.getVatTax().isInclusive()==false&&object.getServiceTax().isInclusive()==false){
//						return false;
//					}else{
//						return false;
//					}
//				}
//				return false;
				
				if(object.getPurchaseTax1()!=null&&object.getPurchaseTax2()!=null){
					if(object.getPurchaseTax1().isInclusive()==true&&object.getPurchaseTax2().isInclusive()==true){
						return true;
					}else if(object.getPurchaseTax1().isInclusive()==false&&object.getPurchaseTax2().isInclusive()==false){
						return false;
					}else{
						return false;
					}
				}
				return false;
			}
		};
		table.addColumn(getColIsTaxInclusive, "Is Inclusive");
		table.setColumnWidth(getColIsTaxInclusive,80,Unit.PX);
		
		getColIsTaxInclusive.setFieldUpdater(new FieldUpdater<ProductDetailsPO, Boolean>() {
			@Override
			public void update(int index, ProductDetailsPO object, Boolean value) {
//				object.setInclusive(value);
//				object.getServiceTax().setInclusive(value);
//				object.getVatTax().setInclusive(value);
				
				object.getPurchaseTax1().setInclusive(value);
				object.getPurchaseTax2().setInclusive(value);
				
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				table.redraw();
			}
		});
	}
	
	private void viewColIsTaxInclusive() {
		viewColIsTaxInclusive=new TextColumn<ProductDetailsPO>() {
			@Override
			public String getValue(ProductDetailsPO object) {
				if(object.getVatTax()!=null&&object.getServiceTax()!=null){
					if(object.getVatTax().isInclusive()==true&&object.getServiceTax().isInclusive()==true){
						return "Yes";
					}else if(object.getVatTax().isInclusive()==false&&object.getServiceTax().isInclusive()==false){
						return "No";
					}else{
						return "No";
					}
				}
				return "No";
			}
		};
		table.addColumn(viewColIsTaxInclusive, "Is Inclusive");
		table.setColumnWidth(viewColIsTaxInclusive,80,Unit.PX);
	}

	private void retriveServiceTax() {
		final GenricServiceAsync service = GWT.create(GenricService.class);
		MyQuerry query = new MyQuerry();
		Company c=new Company();
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		query.setFilters(filtervec);
		query.setQuerryObject(new TaxDetails());
		
		service.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
			}
			public void onSuccess(ArrayList<SuperModel> result) {
				list=new ArrayList<String>();
				servicetaxlist=new ArrayList<TaxDetails>();
				vattaxlist = new ArrayList<TaxDetails>();
				vatlist=new ArrayList<String>();
				List<TaxDetails> backlist=new ArrayList<TaxDetails>();
				for (SuperModel model : result) {
					TaxDetails entity = (TaxDetails) model;
					backlist.add(entity);
					servicetaxlist.add(entity);
					vattaxlist.add(entity);
				}
				for(int i=0;i<backlist.size();i++){
//					if(backlist.get(i).getServiceTax().equals(true)){
						list.add(backlist.get(i).getTaxChargeName());
//					}
//					else if(backlist.get(i).getVatTax().equals(true)){
						vatlist.add(backlist.get(i).getTaxChargeName());
//					}
				}
				retrieveWarehouseNameList();
			}
		});
	}

	public void addeditColumn() {
		addColumnProductID();
		addColumnProductCode();
		addColumnEditProductName();
		if(!marginFlag){
			addColumnProductCategory();
			getSalesPriceCol();
		}
		/** date 12.10.2018 added by komal for making price editable **/
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("PurchaseOrder", "MakePriceNonEditable")){
			addColumnViewProductPrice();
		}else{
			addColumnProductPrice();
		}

		retriveServiceTax();
		
		
	}

	public void addViewColumn() {
		addColumnProductID();
		addColumnProductName();
		addColumnProductCode();
		if(!marginFlag){
			addColumnProductCategory();
			getSalesPriceCol();
		}
		/*
		 *  nidhi 
		 *  18-07-2017
		 *   addColumnProductPrice method removed and in space of that addColumnViewProductPrice method 
		 *   add for make product price read only
		 */
		addColumnViewProductPrice();
		/*
		 *  
		 */
		addColumnViewQuantity();
		addColumnUnit();
		vatColumn();
		addColumnTax();
		viewColIsTaxInclusive();
		addColumnviewDiscount();
		addColumnviewDiscountAmt();
		addColumnTotal();
		
		createColumnViewHSNCodeColumn();
		
		addExpDeliveryDateColoumn();
		addWarehouseColoumn();
		addAvailableStockColoumn();
		
		addWarehouseAddressColumn();
		
	
		}


	

	/******************************************* Product Column View **********************************************/

	protected void createViewDiscountAmt() {
		viewdiscountAmt = new TextColumn<ProductDetailsPO>() {
			@Override
			public String getValue(ProductDetailsPO object) {
				if (object.getDiscountAmt() == 0) {
					return 0 + "";
				} else {
					return object.getDiscountAmt() + "";
				}
			}
		};
		table.addColumn(viewdiscountAmt, "#Disc Amt");
		table.setColumnWidth(viewdiscountAmt, 90, Unit.PX);
		viewdiscountAmt.setCellStyleNames("wordWrap");
	}
	
	
	public void addColumnProductID() {
		columnProductID = new TextColumn<ProductDetailsPO>() {

			@Override
			public String getValue(ProductDetailsPO object) {
				return object.getProductID() + "";
			}
		};
		table.addColumn(columnProductID, "Product ID");
		table.setColumnWidth(columnProductID, 90, Unit.PX);
		columnProductID.setCellStyleNames("wordWrap");
	}

	public void addColumnProductName() {
		columnProductName = new TextColumn<ProductDetailsPO>() {

			@Override
			public String getValue(ProductDetailsPO object) {
				return object.getProductName();
			}
		};
		table.addColumn(columnProductName, "#Name");
		table.setColumnWidth(columnProductName, 120, Unit.PX);
		columnProductName.setCellStyleNames("wordWrap");
	}

	public void addColumnEditProductName() {
		EditTextCell editCell = new EditTextCell();
		editprodname = new Column<ProductDetailsPO, String>(editCell) {

			@Override
			public String getValue(ProductDetailsPO object) {
				return object.getProductName();
			}
		};
		table.addColumn(editprodname, "#Name");
		table.setColumnWidth(editprodname, 120, Unit.PX);
		editprodname.setCellStyleNames("wordWrap");
		
		editprodname.setFieldUpdater(new FieldUpdater<ProductDetailsPO, String>() {
			
			@Override
			public void update(int index, ProductDetailsPO object, String value) {
				// TODO Auto-generated method stub
				object.setProductName(value);
				table.redraw();
			}
		});
}
	public void addColumnProductCode() {
		columnProductCode = new TextColumn<ProductDetailsPO>() {

			@Override
			public String getValue(ProductDetailsPO object) {
				return object.getProductCode();
			}
		};
		table.addColumn(columnProductCode, "Code");
		table.setColumnWidth(columnProductCode, 100, Unit.PX);
		columnProductCode.setCellStyleNames("wordWrap");
	}

	public void addColumnProductCategory() {
		columnProductCategory = new TextColumn<ProductDetailsPO>() {

			@Override
			public String getValue(ProductDetailsPO object) {
				return object.getProductCategory();
			}
		};
		table.addColumn(columnProductCategory, "Category");
		table.setColumnWidth(columnProductCategory, 100, Unit.PX);
		columnProductCategory.setCellStyleNames("wordWrap");
	}

	//   this is for vat tax 
	public void addEditColumnVatTax(){
		SelectionCell employeeselection= new SelectionCell(vatlist);
		editVatColumn = new Column<ProductDetailsPO, String>(employeeselection) {
			@Override
			public String getValue(ProductDetailsPO object) {
//				if (object.getVatTax().getTaxConfigName()!= null) {
//					/**
//					 * Date : 19-04-2018 BY ANIL
//					 */
//					object.setVatTaxEdit(object.getVatTax().getTaxPrintName());
//					return object.getVatTax().getTaxConfigName();
//				} else
//					return "N/A";
				
				/**
				 * @author Anil @since 27-05-2021
				 */
//				if (object.getPurchaseTax2().getTaxConfigName()!= null&&!object.getPurchaseTax2().getTaxConfigName().equals("")) {
//					object.setVatTaxEdit(object.getPurchaseTax2().getTaxConfigName());
//					return object.getPurchaseTax2().getTaxConfigName();
//				} else
//					return "NA";
				
				if (object.getPurchaseTax1().getTaxConfigName()!= null&&!object.getPurchaseTax1().getTaxConfigName().equals("")) {
					object.setServiceTaxEdit(object.getPurchaseTax1().getTaxConfigName());
					return object.getPurchaseTax1().getTaxConfigName();
				} else
					return "NA";
			}
		};
		table.addColumn(editVatColumn, "#TAX 1");
		table.setColumnWidth(editVatColumn, 100, Unit.PX);
		editVatColumn.setCellStyleNames("wordWrap");
	}

	///   service tax editable 
	public void addEditColumnServiceTax(){
		SelectionCell employeeselection= new SelectionCell(list);
		editServiceColumn = new Column<ProductDetailsPO, String>(employeeselection) {
			@Override
			public String getValue(ProductDetailsPO object) {
//				if (object.getServiceTax().getTaxConfigName()!= null) {
//					object.setServiceTaxEdit(object.getServiceTax().getTaxPrintName());
//					return object.getServiceTax().getTaxConfigName();
//				} else
//					return "N/A";
//				if (object.getPurchaseTax1().getTaxConfigName()!= null&&!object.getPurchaseTax1().getTaxConfigName().equals("")) {
//					object.setServiceTaxEdit(object.getPurchaseTax1().getTaxConfigName());
//					return object.getPurchaseTax1().getTaxConfigName();
//				} else
//					return "NA";
				
				/**
				 * @author Anil @since 27-05-2021
				 */
				if (object.getPurchaseTax2().getTaxConfigName()!= null&&!object.getPurchaseTax2().getTaxConfigName().equals("")) {
					object.setVatTaxEdit(object.getPurchaseTax2().getTaxConfigName());
					return object.getPurchaseTax2().getTaxConfigName();
				} else
					return "NA";
			}
		};
		table.addColumn(editServiceColumn, "#TAX 2");
		table.setColumnWidth(editServiceColumn, 100, Unit.PX);
		editServiceColumn.setCellStyleNames("wordWrap");
	}
	

	// Editable Product Price
	public void addColumnProductPrice() {
		EditTextCell prodprice = new EditTextCell();
		columnProductPrice = new Column<ProductDetailsPO, String>(prodprice) {

			@Override
			public String getValue(ProductDetailsPO object) {
				
				if(object.getProdPrice() == 0){
					return 0 + "";
				}else{
					SuperProduct product = object.getPrduct();
					double tax = removeAllTaxes(product);
					double origPrice = object.getProdPrice() - tax;

					return nf.format(origPrice) + "";
				}
			}
		};
		table.addColumn(columnProductPrice, "#Price");
		table.setColumnWidth(columnProductPrice, 90, Unit.PX);
		columnProductPrice.setCellStyleNames("wordWrap");
		columnProductPrice.setFieldUpdater(new FieldUpdater<ProductDetailsPO, String>() {

			@Override
			public void update(int index, ProductDetailsPO object,String value) {
				object.setProdPrice(Double.parseDouble(value));
				table.redraw();
			}
		});
	}

	// Read Only Product Price
	protected void addColumnViewPrice() {

		columnPriceColumn = new TextColumn<ProductDetailsPO>() {
			@Override
			public String getValue(ProductDetailsPO object) {
				if (object.getProdPrice() == 0) {
					return 0 + "";
				} else {
					SuperProduct product = object.getPrduct();
					double tax = removeAllTaxes(product);
					double origPrice = object.getProdPrice() - tax;
					return nf.format(origPrice);
				}
			}
		};
		table.addColumn(columnPriceColumn, "#Price");
		table.setColumnWidth(columnPriceColumn, 90, Unit.PX);
		columnPriceColumn.setCellStyleNames("wordWrap");
	}

	// Editable Product Quantity
	public void addColumnProductQuantity() {
		EditTextCell editCell = new EditTextCell();
		columnProductQuantity = new Column<ProductDetailsPO, String>(editCell) {

			@Override
			public String getValue(ProductDetailsPO object) {
				if (object.getProductQuantity() == 0) {
					object.setProductQuantity(1);
				}
				return nf.format(object.getProductQuantity()) + "";
			}
		};
		table.addColumn(columnProductQuantity, "#Quantity");
		table.setColumnWidth(columnProductQuantity, 90, Unit.PX);
		columnProductQuantity.setCellStyleNames("wordWrap");
		columnProductQuantity.setFieldUpdater(new FieldUpdater<ProductDetailsPO, String>() {

			@Override
			public void update(int index, ProductDetailsPO object,String value) {
				object.setProductQuantity(Double.parseDouble(value));
				table.redraw();
			}
		});
		
		
		
		
		
		
	}

	// Read Only Product Quantity
	public void addColumnViewQuantity() {
		columnProdQtyColumn = new TextColumn<ProductDetailsPO>() {
			@Override
			public String getValue(ProductDetailsPO object) {
				if (object.getProductQuantity() == 0) {
					object.setProductQuantity(1);
				}
				return object.getProductQuantity() + "";
			}
		};
		table.addColumn(columnProdQtyColumn, "#Quantity");
		table.setColumnWidth(columnProdQtyColumn, 90, Unit.PX);
		columnProdQtyColumn.setCellStyleNames("wordWrap");
	}
	
	/**
	 *  nidhi
	 *   18-07-2017
	 *    product price view only method
	 */
	public void addColumnViewProductPrice() {
		columnPriceColumn = new TextColumn<ProductDetailsPO>() {
			@Override
			public String getValue(ProductDetailsPO object) {
				return object.getProdPrice() + "";
			}
		};
		table.addColumn(columnPriceColumn, "Price");
		table.setColumnWidth(columnPriceColumn, 90, Unit.PX);
		columnPriceColumn.setCellStyleNames("wordWrap");
	}
	
	
	public void addColumnUnit() {
		columnunitOfmeasurement = new TextColumn<ProductDetailsPO>() {

			@Override
			public String getValue(ProductDetailsPO object) {
				return object.getUnitOfmeasurement();
			}
		};
		table.addColumn(columnunitOfmeasurement, "UOM");
		table.setColumnWidth(columnunitOfmeasurement, 70, Unit.PX);
		columnunitOfmeasurement.setCellStyleNames("wordWrap");
	}
	
	// Read Only Service Tax Column
		public void addColumnTax() {
			columnServiceTax = new TextColumn<ProductDetailsPO>() {
				@Override
				public String getValue(ProductDetailsPO object) {
//					if (object.getTax() != null) {
//						return object.getServiceTaxEdit() + "";
//					} else {
//						return "N.A";
//					}
					
//					if (object.getPurchaseTax1().getTaxConfigName() != null) {
//						return object.getPurchaseTax1().getTaxConfigName();
//					} else {
//						return "NA";
//					}
					
					if (object.getPurchaseTax2().getTaxConfigName() != null) {
						return object.getPurchaseTax2().getTaxConfigName();
					} else {
						return "NA";
					}
				}
			};
			table.addColumn(columnServiceTax, "TAX 2");
			table.setColumnWidth(columnServiceTax, 100, Unit.PX);
			columnServiceTax.setCellStyleNames("wordWrap");
		}
		
	// Read Only Vat Column
	public void vatColumn() {
		columnVat = new TextColumn<ProductDetailsPO>() {
			@Override
			public String getValue(ProductDetailsPO object) {
//				if (object.getVat() != null) {
//					return object.getVatTaxEdit() + "";
//				} else {
//					return "N.A";
//				}
				
				
//				if (object.getPurchaseTax2().getTaxConfigName() != null) {
//					return object.getPurchaseTax2().getTaxConfigName();
//				} else {
//					return "NA";
//				}
				
				if (object.getPurchaseTax1().getTaxConfigName() != null) {
					return object.getPurchaseTax1().getTaxConfigName();
				} else {
					return "NA";
				}
			}
		};
		table.addColumn(columnVat, "TAX 1");
		table.setColumnWidth(columnVat, 100, Unit.PX);
		columnVat.setCellStyleNames("wordWrap");
	}

	// Read Only Discount Column
	public void addColumnviewDiscount() {
		columnDiscount = new TextColumn<ProductDetailsPO>() {
			@Override
			public String getValue(ProductDetailsPO object) {
				if (object.getDiscount() == 0) {
					return 0 + "";
				} else {
					return object.getDiscount() + "";
				}
			}
		};
		table.addColumn(columnDiscount, "% Discount");
		table.setColumnWidth(columnDiscount, 90, Unit.PX);
		columnDiscount.setCellStyleNames("wordWrap");
	}

	// Editable Discount Column
	public void addColumnDiscount() {
		EditTextCell cell = new EditTextCell();
		ecolumnDiscount = new Column<ProductDetailsPO, String>(cell) {
			@Override
			public String getValue(ProductDetailsPO object) {
				if (object.getDiscount() <= 0 || object.getDiscount() > 100)
					object.setDiscount(0);
				return object.getDiscount() + "";
			}
		};
		table.addColumn(ecolumnDiscount, "% Discount");
		table.setColumnWidth(ecolumnDiscount, 90, Unit.PX);
		ecolumnDiscount.setCellStyleNames("wordWrap");
	}

	
	
	
	// Read Only Discount Column
		public void addColumnviewDiscountAmt() {
			nonEditcolumnDiscount = new TextColumn<ProductDetailsPO>() {
				@Override
				public String getValue(ProductDetailsPO object) {
					if (object.getDiscountAmt() == 0) {
						return 0 + "";
					} else {
						return object.getDiscountAmt() + "";
					}
				}
			};
			table.addColumn(nonEditcolumnDiscount, "#Disc Amt");
			table.setColumnWidth(nonEditcolumnDiscount, 90, Unit.PX);
			nonEditcolumnDiscount.setCellStyleNames("wordWrap");
		}
	//***************Editable discount Amt column *************** 
	private void createColumnDiscountAmt() {
		EditTextCell editCell = new EditTextCell();
		discountAmt = new Column<ProductDetailsPO, String>(editCell) {
			@Override
			public String getValue(ProductDetailsPO object) {
				if (object.getDiscountAmt() == 0) {
					return 0 + "";
				} else {
					return object.getDiscountAmt() + "";
				}
			}
		};
		table.addColumn(discountAmt, "#Disc Amt");
		table.setColumnWidth(discountAmt, 90, Unit.PX);
		discountAmt.setCellStyleNames("wordWrap");
	}

	public void addColumnTotal() {
		TextCell editCell = new TextCell();
		columnTotal = new Column<ProductDetailsPO, String>(editCell) {
			@Override
			public String getValue(ProductDetailsPO object) {
				double total = 0;
				SuperProduct product = object.getPrduct();
				double tax = removeAllTaxes(product);
				double origPrice = object.getProdPrice() - tax;

				// ****************new code for discount by rohan
				// ******************

				if ((object.getDiscount() == null || object.getDiscount() == 0)
						&& (object.getDiscountAmt() == 0)) {
					total = origPrice * object.getProductQuantity();
				} else if ((object.getDiscount() != null)
						&& (object.getDiscountAmt() != 0)) {
					total = origPrice
							- (origPrice * object.getDiscount() / 100);
					total = total - object.getDiscountAmt();
					total = total * object.getProductQuantity();
				} else {
					if (object.getDiscount() != null) {
						total = origPrice
								- (origPrice * object.getDiscount() / 100);
					} else {
						total = origPrice - object.getDiscountAmt();
					}
					total = total * object.getProductQuantity();
				}
				/**
				 * @author Anil
				 * @since 20-07-2020
				 */
				object.setTotal(nf.parse(nf.format(total)));
				return nf.format(total);
			}
		};
		table.addColumn(columnTotal, "Total");
		table.setColumnWidth(columnTotal, 90, Unit.PX);
		columnTotal.setCellStyleNames("wordWrap");
	}
	
	public void createColumndeleteColumn() {
		ButtonCell btnCell = new ButtonCell();
		deleteColumn = new Column<ProductDetailsPO, String>(btnCell) {
			@Override
			public String getValue(ProductDetailsPO object) {
				return "Delete";
			}
		};
		table.addColumn(deleteColumn, "Delete");
		table.setColumnWidth(deleteColumn, 90, Unit.PX);
		deleteColumn.setCellStyleNames("wordWrap");

	}
	/**************************************** Update product Column **********************************************/
	
	public void createFieldUpdaterDelete() {
		deleteColumn.setFieldUpdater(new FieldUpdater<ProductDetailsPO, String>() {
			@Override
			public void update(int index, ProductDetailsPO object,String value) {
//				System.out.println("PRODUCT LIST SIZE BEFOR : "+getDataprovider().getList().size());
//				getDataprovider().getList().remove(object);
//				table.redrawRow(index);
//				System.out.println("PRODUCT LIST SIZE AFTER : "+getDataprovider().getList().size());
//				PurchaseOrderPresenter.updateVendorTable();
				
				
				boolean updateVenTblFlag=checkSameProductExist(object.getProductID(),getDataprovider().getList());
				getDataprovider().getList().remove(object);
				
				if(updateVenTblFlag){
					PurchaseOrderPresenter.updateVendorTable();
				}
				/**
				 * @author Anil,Date : 11-01-2019
				 * If we remove duplicate product its warehouse allocation status should also be updated
				 */
				if(validWarehouseSelection(object.getProductID(), value)){
					isValidWarehouse=true;
				}else{
					isValidWarehouse=false;
				}
				
				table.redrawRow(index);
			}
			
		});
	}
	
	/**
	 * This method checks in product list ,whether the product which you want to delete has 2 or more entry ,
	 * in that case you dont need to update the vendor table.
	 * Date : 28-09-2016 By Anil 
	 * Release : 30 Sept 2016 
	 * Project : PURCHASE MODIFICATION(NBHC)
	 * I/P : Product Id and product list
	 * O/P: return false if product has multiple entry else true
	 */
	private boolean checkSameProductExist(int productID,List<ProductDetailsPO> list) {
		int count=0;
		for(ProductDetailsPO obj:list){
			if(obj.getProductID()==productID){
				count++;
			}
			if(count==2){
				return false;
			}
		}
		return true;
	}
	
	/**
	 * END
	 */
	//********Date 04/09/2017 by jayshree change datatype int to float
	protected void createFieldUpdaterprodQtyColumn() {
		columnProductQuantity.setFieldUpdater(new FieldUpdater<ProductDetailsPO, String>() {
			@Override
			public void update(int index, ProductDetailsPO object,String value) {
				try {
					/**
					 * Date : 13-09-2017 BY JAYSHREE
					 * Old COde
					 */
//					Integer val1;
//					if (Integer.parseInt(value.trim()) == 0) {
//						val1 = 1;
//						object.setProductQuantity(val1);
////						System.out.println("in 0" + val1);
//						table.redrawRow(index);
//					} else {
//						val1 = Integer.parseInt(value.trim());
//						object.setProductQuantity(val1);
//					}
					/**
					 * New Code
					 */
					Float val1;
					if (Float.parseFloat(value.trim()) == 0) {
						val1 =  1.00f;
						object.setProductQuantity(val1);
//						System.out.println("in 0" + val1);
						table.redrawRow(index);
					} else {
						val1 = Float.parseFloat(value.trim());
						object.setProductQuantity(val1);
					}
					
					/**
					 * End
					 */
					
//					if(PurchaseOrderPresenter.isVendorSelected()&&!PurchaseOrderPresenter.isCformCstApplicable()){
						RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
////						System.out.println("Field Updater Called QTy"+ val1);
//					}

				} catch (NumberFormatException e) {

				}
				table.redrawRow(index);
			}
		});
	}

	public void addColumnEditDiscount() {
		ecolumnDiscount.setFieldUpdater(new FieldUpdater<ProductDetailsPO, String>() {
			@Override
			public void update(int index, ProductDetailsPO object,String value) {
				try {
					Double val1 = Double.parseDouble(value.trim());
					object.setDiscount(val1);

//					if(PurchaseOrderPresenter.isVendorSelected()&&!PurchaseOrderPresenter.isCformCstApplicable()){
						RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
////						System.out.println("Field Updater Called price=="+ val1);
//					}

				} catch (NumberFormatException e) {

				}
				table.redrawRow(index);
			}
		});
	}

	protected void createFieldUpdaterprodPriceColumn() {
		columnProductPrice.setFieldUpdater(new FieldUpdater<ProductDetailsPO, String>() {
			@Override
			public void update(int index, ProductDetailsPO object,String value) {
				try {
					Double val1 = Double.parseDouble(value.trim());
					object.setProdPrice(val1);
//					if(PurchaseOrderPresenter.isVendorSelected()&&!PurchaseOrderPresenter.isCformCstApplicable()){
						RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
////						System.out.println("Field Updater Called price=="+ val1);
//					}

				} catch (NumberFormatException e) {

				}
				table.redrawRow(index);
			}
		});
	}

	
	

	protected void createFieldUpdaterDiscAmtColumn()
	{
		discountAmt.setFieldUpdater(new FieldUpdater<ProductDetailsPO, String>()
		{
		    @Override
			public void update(int index,ProductDetailsPO object,String value)
			{
				try{
					Double val1=Double.parseDouble(value.trim());
					object.setDiscountAmt(val1);
					
//					if(PurchaseOrderPresenter.isVendorSelected()&&!PurchaseOrderPresenter.isCformCstApplicable()){
						RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
//					}
				}
				catch (NumberFormatException e)
				{

				}
				table.redrawRow(index);
			}
		});
	}
	
	
	@Override
	protected void initializekeyprovider() {

	}

	@Override
	public void addFieldUpdater() {
		createFieldUpdaterprodQtyColumn();
		//createFieldUpdaterprodPriceColumn();
		/** date 12.10.2018 added by komal to make price non-editable **/
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("PurchaseOrder", "MakePriceNonEditable")){
		}else{
			createFieldUpdaterprodPriceColumn();
		}
		addColumnEditDiscount();
		createFieldUpdaterDiscAmtColumn();
		updateServiceTaxColumn();
		updateVatTaxColumn();
		
		createFieldUpdaterDelete();
		
		setFieldUpdatorOnDeliveryDate();
		setFieldUpdatorOnWarehouse();
	}
	
	/**
	 * @author Anil,Date : 10-01-2019
	 * Updated tax auto selection part and resolved calculation issue
	 */
	public void updateVatTaxColumn()
	{

		editVatColumn.setFieldUpdater(new FieldUpdater<ProductDetailsPO, String>() {
			
			@Override
			public void update(int index, ProductDetailsPO object, String value) {
				
//				try {
//					String val1 = (value.trim());
//					object.setVatTaxEdit(val1);
//					Tax tax = object.getPurchaseTax2();
//					for(int i=0;i<vattaxlist.size();i++)
//					{
//						if(val1.trim().equals(vattaxlist.get(i).getTaxChargeName())){
//							tax.setTaxName(val1);
//							tax.setPercentage(vattaxlist.get(i).getTaxChargePercent());
//							tax.setTaxPrintName(vattaxlist.get(i).getTaxPrintName());
//							tax.setTaxConfigName(val1);
//							
//							object.setVat(vattaxlist.get(i).getTaxChargePercent());
//							Tax taxDt = AppUtility.checkTaxValue(tax, object.getPurchaseTax1(), vattaxlist);
//							if(taxDt != null){
//								object.setPurchaseTax1(taxDt);
//								object.setTax(taxDt.getPercentage());
//							} 
//														
//							if(vattaxlist.get(i).getTaxPrintName().equals("IGST") || vattaxlist.get(i).getTaxPrintName().equals("IGST@18") || vattaxlist.get(i).getTaxPrintName().equals("SEZ")){
//								editServiceColumn.setCellStyleNames("hideVisibility");
//							}else{
//								editServiceColumn.setCellStyleNames("showVisibility");
//							}
//							
//							break;
//						}
//					}
//					object.setPurchaseTax2(tax);
//					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
//					
//				} catch (NumberFormatException e) {
//				}
				
				try {
					String val1 = (value.trim());
					object.setVatTaxEdit(val1);
					Tax tax = new Tax();
					if(object.getPurchaseTax1()!=null){
						tax=object.getPurchaseTax1();
					}
					for(int i=0;i<vattaxlist.size();i++)
					{
						if(val1.trim().equals(vattaxlist.get(i).getTaxChargeName())){
							tax.setTaxName(val1);
							tax.setPercentage(vattaxlist.get(i).getTaxChargePercent());
							tax.setTaxPrintName(vattaxlist.get(i).getTaxPrintName());
							tax.setTaxConfigName(val1);
//							object.setTax(vattaxlist.get(i).getTaxChargePercent());
							object.setVat(vattaxlist.get(i).getTaxChargePercent());
							Tax vatTax = AppUtility.checkTaxValue(tax, object.getPurchaseTax2(), vattaxlist);
							if(vatTax!=null){
								object.setPurchaseTax2(vatTax);
//								object.setVat(vatTax.getPercentage());
								object.setTax(vatTax.getPercentage());
							}
							if(vattaxlist.get(i).getTaxPrintName().equals("IGST") || vattaxlist.get(i).getTaxPrintName().equals("IGST@18") || vattaxlist.get(i).getTaxPrintName().equals("SEZ")){
								editServiceColumn.setCellStyleNames("hideVisibility");
							}else{
								editServiceColumn.setCellStyleNames("showVisibility");
							}
							
							break;
						}
					}
					object.setPurchaseTax1(tax);
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				} catch (NumberFormatException e) {
				}
				
				table.redrawRow(index);
			}
		});
	}
	
	/**
	 * @author Anil,Date : 10-01-2019
	 * Updated tax auto selection part and resolved calculation issue
	 */
	public void updateServiceTaxColumn()
	{
		editServiceColumn.setFieldUpdater(new FieldUpdater<ProductDetailsPO, String>() {
			@Override
			public void update(int index, ProductDetailsPO object, String value) {

//				try {
//					String val1 = (value.trim());
//					object.setServiceTaxEdit(val1);
//					Tax tax = new Tax();
//					if(object.getPurchaseTax1()!=null){
//						tax=object.getPurchaseTax1();
//					}
//					for(int i=0;i<servicetaxlist.size();i++)
//					{
//						if(val1.trim().equals(servicetaxlist.get(i).getTaxChargeName())){
//							tax.setTaxName(val1);
//							tax.setPercentage(servicetaxlist.get(i).getTaxChargePercent());
//							tax.setTaxPrintName(servicetaxlist.get(i).getTaxPrintName());
//							tax.setTaxConfigName(val1);
//							object.setTax(servicetaxlist.get(i).getTaxChargePercent());
//							
//							Tax vatTax = AppUtility.checkTaxValue(tax, object.getPurchaseTax2(), servicetaxlist);
//							if(vatTax!=null){
//								object.setPurchaseTax2(vatTax);
//								object.setVat(vatTax.getPercentage());
//							}
//							if(servicetaxlist.get(i).getTaxPrintName().equals("IGST") || servicetaxlist.get(i).getTaxPrintName().equals("IGST@18") || servicetaxlist.get(i).getTaxPrintName().equals("SEZ")){
//								editVatColumn.setCellStyleNames("hideVisibility");
//							}else{
//								editVatColumn.setCellStyleNames("showVisibility");
//							}
//							
//							break;
//						}
//					}
//					object.setPurchaseTax1(tax);
//					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
//				} catch (NumberFormatException e) {
//				}
				
				try {
					String val1 = (value.trim());
					object.setServiceTaxEdit(val1);
					Tax tax = object.getPurchaseTax2();
					for(int i=0;i<servicetaxlist.size();i++)
					{
						if(val1.trim().equals(servicetaxlist.get(i).getTaxChargeName())){
							tax.setTaxName(val1);
							tax.setPercentage(servicetaxlist.get(i).getTaxChargePercent());
							tax.setTaxPrintName(servicetaxlist.get(i).getTaxPrintName());
							tax.setTaxConfigName(val1);
							
//							object.setVat(servicetaxlist.get(i).getTaxChargePercent());
							object.setTax(servicetaxlist.get(i).getTaxChargePercent());
							Tax taxDt = AppUtility.checkTaxValue(tax, object.getPurchaseTax1(), servicetaxlist);
							if(taxDt != null){
								object.setPurchaseTax1(taxDt);
//								object.setTax(taxDt.getPercentage());
								object.setVat(taxDt.getPercentage());
							} 
														
							if(servicetaxlist.get(i).getTaxPrintName().equals("IGST") || servicetaxlist.get(i).getTaxPrintName().equals("IGST@18") || servicetaxlist.get(i).getTaxPrintName().equals("SEZ")){
								editVatColumn.setCellStyleNames("hideVisibility");
							}else{
								editVatColumn.setCellStyleNames("showVisibility");
							}
							
							break;
						}
					}
					object.setPurchaseTax2(tax);
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
					
				} catch (NumberFormatException e) {
				}
				table.redrawRow(index);
			}
		});
	}
	
	
	

	@Override
	public void setEnable(boolean state) {
		int tablecolcount = this.table.getColumnCount();
		for (int i = tablecolcount - 1; i > -1; i--)
			table.removeColumn(i);
		if (state == true)
			addeditColumn();
		if (state == false)
			addViewColumn();

	}

	@Override
	public void applyStyle() {

	}

	public double calculateTotal() {
		List<ProductDetailsPO> list = getDataprovider().getList();
		double total = 0;
		for (ProductDetailsPO object : list) {
			// double tax=object.getProdPrice()*(12.5/100);
			// double vat=object.getProdPrice()*(12.5/100);
			double discount = object.getProdPrice()* (object.getDiscount() / 100);
			total = object.getProdPrice() * object.getProductQuantity() + total
					- discount;
		}
		return total;
	}

	public double calculateTotalExcludingTax() {
		List<ProductDetailsPO> list = getDataprovider().getList();
		double sum = 0, priceVal = 0;
		
		for (int i = 0; i < list.size(); i++) {
			
			ProductDetailsPO entity = list.get(i);
			SuperProduct prod = entity.getPrduct();
			//return inclusive tax amount
			double taxAmt = removeAllTaxes(prod);
			
			if((entity.getDiscount()==null || entity.getDiscount()==0) && (entity.getDiscountAmt()==0) ){
//				System.out.println("inside both 0 condition");
				priceVal=entity.getProdPrice()-taxAmt;
				priceVal=priceVal*entity.getProductQuantity();
				sum=sum+priceVal;
//				System.out.println("RRRRRRRRRRRRRR sum"+sum);
			}
			else if((entity.getDiscount()!=null)&& (entity.getDiscountAmt()!=0)){
//				System.out.println("inside both not null condition");
				priceVal=entity.getProdPrice()-taxAmt;
				priceVal=priceVal-(priceVal*entity.getDiscount()/100);
				priceVal=priceVal-entity.getDiscountAmt();
				priceVal=priceVal*entity.getProductQuantity();
				sum=sum+priceVal;
//				System.out.println("RRRRRRRRRRRRRR sum"+sum);
			}
			else{
//				System.out.println("inside oneof the null condition");
				priceVal=entity.getProdPrice()-taxAmt;
				if(entity.getDiscount()!=null){
//					System.out.println("inside getPercentageDiscount oneof the null condition");
					priceVal=priceVal-(priceVal*entity.getDiscount()/100);
				}
				else{
//					System.out.println("inside getDiscountAmt oneof the null condition");
					priceVal=priceVal-entity.getDiscountAmt();
				}
				priceVal=priceVal*entity.getProductQuantity();
				sum=sum+priceVal;
//				System.out.println("RRRRRRRRRRRRRR sum"+sum);
			}
			
		}
		return sum;
	}

	/**
	 * Commented By Anil
	 * This method return inclusive tax amount 
	 * its returns amount only if product has inclusive tax 
	 * Date : 23-11-2016 By ANIL
	 * Project : DEADSTOCK(NBHC) 
	 */
	
public double removeAllTaxes(SuperProduct entity) {
		
		double vat = 0, service = 0;
		double tax = 0, retrVat = 0, retrServ = 0;
		
		if (entity instanceof ServiceProduct) {
			
			ServiceProduct prod = (ServiceProduct) entity;
			if (prod.getPurchaseTax1()!= null&& prod.getPurchaseTax1().isInclusive() == true) {
				service = prod.getPurchaseTax1().getPercentage();
			}
			if (prod.getPurchaseTax2() != null&& prod.getPurchaseTax2().isInclusive() == true) {
				vat = prod.getPurchaseTax2().getPercentage();
			}
		}

		if (entity instanceof ItemProduct) {
			ItemProduct prod = (ItemProduct) entity;
			if (prod.getPurchaseTax2() != null&& prod.getPurchaseTax2().isInclusive() == true) {
				vat = prod.getPurchaseTax2().getPercentage();
			}
			if (prod.getPurchaseTax1() != null&& prod.getPurchaseTax1().isInclusive() == true) {
				service = prod.getPurchaseTax1().getPercentage();
			}
		}

		if (vat != 0 && service == 0) {
			retrVat = (entity.getPurchasePrice() / (1 + (vat / 100)));
			retrVat = entity.getPurchasePrice() - retrVat;
		}
		if (service != 0 && vat == 0) {
			retrServ = (entity.getPurchasePrice() / (1 + service / 100));
			retrServ = entity.getPurchasePrice() - retrServ;
		}
		if (service != 0 && vat != 0) {


			/**if condition added by vijay and old code added in else block
			 * Date 2 August  2017 added by vijay for GST
			 */
			
			ItemProduct prod=(ItemProduct) entity;
			if(prod.getPurchaseTax1().getTaxPrintName()!=null && ! prod.getPurchaseTax1().getTaxPrintName().equals("")
			   && prod.getPurchaseTax2().getTaxPrintName()!=null && ! prod.getPurchaseTax2().getTaxPrintName().equals(""))
			{

				double dot = service + vat;
				retrServ=(entity.getPurchasePrice()/(1+dot/100));
				retrServ=entity.getPurchasePrice()-retrServ;
//				retrVat=0;

			}else{
				
			
			// Here if both are inclusive then first remove service tax and then
			// on that amount
			// calculate vat.
			double removeServiceTax = (entity.getPurchasePrice() / (1 + service / 100));

			// double taxPerc=service+vat;
			// retrServ=(entity.getPurchasePrice()/(1+taxPerc/100)); //line changed
			// below
			retrServ = (removeServiceTax / (1 + vat / 100));
			retrServ = entity.getPurchasePrice() - retrServ;
			
			}
		}
		tax = retrVat + retrServ;
		return tax;
	}
	
//	public double removeAllTaxes(SuperProduct entity) {
//		
//		double vat = 0, service = 0;
//		double tax = 0, retrVat = 0, retrServ = 0;
//		
//		if (entity instanceof ServiceProduct) {
//			
//			ServiceProduct prod = (ServiceProduct) entity;
//			if (prod.getServiceTax()!= null&& prod.getServiceTax().isInclusive() == true) {
//				service = prod.getServiceTax().getPercentage();
//			}
//			if (prod.getVatTax() != null&& prod.getVatTax().isInclusive() == true) {
//				vat = prod.getVatTax().getPercentage();
//			}
//		}
//
//		if (entity instanceof ItemProduct) {
//			ItemProduct prod = (ItemProduct) entity;
//			if (prod.getVatTax() != null&& prod.getVatTax().isInclusive() == true) {
//				vat = prod.getVatTax().getPercentage();
//			}
//			if (prod.getServiceTax() != null&& prod.getServiceTax().isInclusive() == true) {
//				service = prod.getServiceTax().getPercentage();
//			}
//		}
//
//		if (vat != 0 && service == 0) {
//			retrVat = (entity.getPrice() / (1 + (vat / 100)));
//			retrVat = entity.getPrice() - retrVat;
//		}
//		if (service != 0 && vat == 0) {
//			retrServ = (entity.getPrice() / (1 + service / 100));
//			retrServ = entity.getPrice() - retrServ;
//		}
//		if (service != 0 && vat != 0) {
//
//
//			/**if condition added by vijay and old code added in else block
//			 * Date 2 August  2017 added by vijay for GST
//			 */
//			
//			ItemProduct prod=(ItemProduct) entity;
//			if(prod.getServiceTax().getTaxPrintName()!=null && ! prod.getServiceTax().getTaxPrintName().equals("")
//			   && prod.getVatTax().getTaxPrintName()!=null && ! prod.getVatTax().getTaxPrintName().equals(""))
//			{
//
//				double dot = service + vat;
//				retrServ=(entity.getPrice()/(1+dot/100));
//				retrServ=entity.getPrice()-retrServ;
////				retrVat=0;
//
//			}else{
//				
//			
//			// Here if both are inclusive then first remove service tax and then
//			// on that amount
//			// calculate vat.
//			double removeServiceTax = (entity.getPrice() / (1 + service / 100));
//
//			// double taxPerc=service+vat;
//			// retrServ=(entity.getPrice()/(1+taxPerc/100)); //line changed
//			// below
//			retrServ = (removeServiceTax / (1 + vat / 100));
//			retrServ = entity.getPrice() - retrServ;
//			
//			}
//		}
//		tax = retrVat + retrServ;
//		return tax;
//	}
	
	

	public boolean removeChargesOnDelete() {

		int tableSize = getDataprovider().getList().size();

		if (tableSize < 1) {
			return false;
		}
		return true;
	}




	/**
	 * This method loads all the active warehouse in selection list.
	 * purpose of loading warehouse is that product should be delivered in that warehouse.
	 * Date:27-09-2016 By Anil
	 * Release :30 Sept 2016 
	 * Project : PURCHASE MODIFICATION(NBHC) 
	 */
	
	public void retrieveWarehouseNameList(){
		final GenricServiceAsync service = GWT.create(GenricService.class);
		MyQuerry query = new MyQuerry();
//		System.out.println("Company Id :: "+UserConfiguration.getCompanyId());
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);
		
		if(AppUtility.isBranchRestricted()){
			if (!LoginPresenter.myUserEntity.getRole().getRoleName().trim().equalsIgnoreCase("ADMIN")) {
				List<String> list=new ArrayList<String>();
				for(Branch branch:LoginPresenter.globalBranch){
					list.add(branch.getBusinessUnitName());
				}
				
				 filter=new Filter();
                 filter.setQuerryString("branchdetails.branchName IN");
                 filter.setList(list);
                 filtervec.add(filter);
			}
		}
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(UserConfiguration.getCompanyId());
		filtervec.add(filter);
		
		
		query.setFilters(filtervec);
		query.setQuerryObject(new WareHouse());
		
		service.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				warehouseList=new ArrayList<String>();
				warehouseEntityList=new ArrayList<WareHouse>();
				warehouseList.add("");
				if(result.size()!=0){
					for(SuperModel model:result){
						WareHouse entity=(WareHouse) model;
						warehouseList.add(entity.getBusinessUnitName());
						warehouseEntityList.add(entity);
					}
					Collections.sort(warehouseList);
				}
				
				
				
				addColumnUnit();
				addAvailableStockColoumn();
				addColumnProductQuantity();
				addColumnDiscount();
				createColumnDiscountAmt();
				addColumnTotal();
				
				getColIsTaxInclusive();
				addEditColumnVatTax();
				addEditColumnServiceTax();
				addEditableWarehouseColoumn();
				addEditableExpDeliveryDateColoumn();
				
				
				
				addWarehouseAddressColumn();
				
				addColumnProductCode();
				addColumnProductID();
				createColumnprodHSNCodeColumn();
				createColumndeleteColumn();
				addFieldUpdater();
				
				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				
			}
		});
	}
	
	
	protected void addWarehouseAddressColumn() {
		warehouseAddressColumn = new TextColumn<ProductDetailsPO>() {
			
			@Override
			public String getValue(ProductDetailsPO object) {
				return object.getWarehouseAddress();
			}
		};
		table.addColumn(warehouseAddressColumn, "Warehouse Address");
		table.setColumnWidth(warehouseAddressColumn, 200,Unit.PX);
	}
	
	
	private void createColumnprodHSNCodeColumn() {

		EditTextCell editCell=new EditTextCell();
		hsnCodeColumn = new Column<ProductDetailsPO, String>(editCell) {
			
			@Override
			public String getValue(ProductDetailsPO object) {
				if(object.getPrduct().getHsnNumber()!=null && !object.getPrduct().getHsnNumber().equals("")){
					return object.getPrduct().getHsnNumber();
				}
				else{
					return "";
				}
			}
		};
		table.addColumn(hsnCodeColumn,"#HSN Code");
		table.setColumnWidth(hsnCodeColumn, 100,Unit.PX);
		
		/** date 27.8.2018 added by komal for hsn code update**/
		hsnCodeColumn.setFieldUpdater(new FieldUpdater<ProductDetailsPO, String>() {
			@Override
			public void update(int index, ProductDetailsPO object, String value) {
				object.getPrduct().setHsnNumber(value);;
				table.redrawRow(index);				
			}
		});
	}
	
	private void createColumnViewHSNCodeColumn() {
		viewHsnCodeColumn = new TextColumn<ProductDetailsPO>() {
			
			@Override
			public String getValue(ProductDetailsPO object) {
				if(object.getPrduct().getHsnNumber()!=null && !object.getPrduct().getHsnNumber().equals(""))
				return object.getPrduct().getHsnNumber();
				
				return "";
			}
		};
		table.addColumn(viewHsnCodeColumn,"#HSN Code");
		table.setColumnWidth(viewHsnCodeColumn, 100,Unit.PX);
	}
	
	public void addWarehouseColoumn(){
		warehouseColumn=new TextColumn<ProductDetailsPO>() {
			@Override
			public String getValue(ProductDetailsPO object) {
				if(object.getWarehouseName()!=null){
					return object.getWarehouseName();
				}
				return "";
			}
		};
		table.addColumn(warehouseColumn, "#Warehouse");
		table.setColumnWidth(warehouseColumn, 150, Unit.PX);
		warehouseColumn.setCellStyleNames("wordWrap");
	}
	
	public void addEditableWarehouseColoumn(){
		SelectionCell selectionCell= new SelectionCell(warehouseList);
		warehouseListColumn=new Column<ProductDetailsPO, String>(selectionCell) {
			
			@Override
			public String getCellStyleNames(Context context, ProductDetailsPO object) {
				 
				if(object.getWarehouseName()==null){
					 return "red";
				 }
				return "";
			}
			@Override
			public String getValue(ProductDetailsPO object) {
				if(object.getWarehouseName()!=null){
					return object.getWarehouseName();
				}
				return "";
			}
		};
		table.addColumn(warehouseListColumn, "#Warehouse");
		table.setColumnWidth(warehouseListColumn, 150, Unit.PX);
		warehouseListColumn.setCellStyleNames("wordWrap");
		
		
	}
	
	public void addExpDeliveryDateColoumn(){
		expDeliveryDateColumn=new TextColumn<ProductDetailsPO>() {
			@Override
			public String getValue(ProductDetailsPO object) {
				if(object.getProdDate()!=null){
					return AppUtility.parseDate(object.getProdDate());
				}
				return "";
			}
		};
		table.addColumn(expDeliveryDateColumn, "#Delivery Date");
		table.setColumnWidth(expDeliveryDateColumn, 110, Unit.PX);
		expDeliveryDateColumn.setCellStyleNames("wordWrap");
	}
	public void addEditableExpDeliveryDateColoumn(){
		final DateTimeFormat fmt = DateTimeFormat.getFormat("dd/MM/yyyy");
		DatePickerCell date= new DatePickerCell(fmt);
		editableExpDeliveryDateColumn=new Column<ProductDetailsPO,Date>(date) {
			@Override
			public Date getValue(ProductDetailsPO object) {
				if(object.getProdDate()!=null){
					return fmt.parse(fmt.format(object.getProdDate()));
				}else{
					object.setProdDate(new Date());
					return fmt.parse(fmt.format(new Date()));
				}
			}
		};
		table.addColumn(editableExpDeliveryDateColumn, "#Delivery Date");
		table.setColumnWidth(editableExpDeliveryDateColumn, 110, Unit.PX);
		editableExpDeliveryDateColumn.setCellStyleNames("wordWrap");
	}
	
	public void addAvailableStockColoumn(){
		availableStockColumn=new TextColumn<ProductDetailsPO>() {
			@Override
			public String getValue(ProductDetailsPO object) {
	//			if(object.getAvailableStock()!=0){
					return object.getAvailableStock()+"";
	//			}
	//			return "";
			}
		};
		table.addColumn(availableStockColumn, "Stock");
		table.setColumnWidth(availableStockColumn, 90, Unit.PX);
		availableStockColumn.setCellStyleNames("wordWrap");
	}
	
	private void setFieldUpdatorOnWarehouse(){
		warehouseListColumn.setFieldUpdater(new FieldUpdater<ProductDetailsPO, String>() {
			@Override
			public void update(int index, ProductDetailsPO object, String value) {
				
				if(validWarehouseSelection(object.getProductID(), value)){
					isValidWarehouse=true;
				}else{
					isValidWarehouse=false;
				}
				setAvailableStock(index,object,value);
				/**
				 * @author Anil,Date : 12-01-2019
				 * if warehouse is selected as blank,then its address should also be blank
				 */
				if(value==null||value.equals("")){
					object.setWarehouseAddress("");
				}
				//Below Method added by vijay for setting warehouse address for Deadstock NBHC on 26-04-2017
				setwarehouseAddress(index,object,value);
			}
		});
	}
	
	private void setFieldUpdatorOnDeliveryDate(){
		editableExpDeliveryDateColumn.setFieldUpdater(new FieldUpdater<ProductDetailsPO, Date>() {
			@Override
			public void update(int index, ProductDetailsPO object, Date value) {
				object.setProdDate(value);
				table.redrawRow(index);				
			}
		});
	}
	
	/**
	 * Date 26-04-2017
	 * added by vijay Deadstock NBHC
	 * for setting warehouse address
	 */
	private void setwarehouseAddress(int index, ProductDetailsPO object, String warehouseName) {
		
		for(int i=0;i<warehouseEntityList.size();i++){
			if(warehouseEntityList.get(i).getBusinessUnitName().equals(warehouseName)){
				object.setWarehouseAddress(warehouseEntityList.get(i).getAddress().getCompleteAddress());
				break;
			}
		}
		table.redrawRow(index);
		
	}
	
	/**
	 * 
	 * @param productID
	 * @param value -warehouse name
	 * @return true/false
	 * 
	 * This method checks whether selected warehouse for given product id is already added.
	 * if added then return false else true.
	 * Date : 27-09-2016 By Anil
	 * Release : 30 Sept 2016
	 * Project: PURCHASE MODIFICATION(NBHC)
	 */
	private boolean validWarehouseSelection(int productID, String value) {
		List<ProductDetailsPO> list=getDataprovider().getList();
		for(ProductDetailsPO prod:list){
			if(productID==prod.getProductID()&&value.trim().equals(prod.getWarehouseName().trim())){
				GWTCAlert alert =new GWTCAlert();
				alert.alert("Warehouse is already selected for product !");
				return false;
			}
		}
		return true;
	}
	
	
	/**
	 * 
	 * @param index -selected row
	 * @param object - selected row data
	 * @param value - updated warehouse name
	 * 
	 * This method return the available Stock of product in selected warehouse.
	 * Date :27-09-2016 By Anil
	 * Release : 30 Sept 2016
	 * Project : PURCHASE MODIFICATION(NBHC)
	 */
	
	private void setAvailableStock(final int index, final ProductDetailsPO object, final String value) {
		final GenricServiceAsync service = GWT.create(GenricService.class);
		MyQuerry query = new MyQuerry();
//		System.out.println("Company Id :: "+UserConfiguration.getCompanyId()+" Product Id :: "+object.getProductID()+" Warehouse  :: "+value);
		
		
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(UserConfiguration.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("productinfo.prodID");
		filter.setIntValue(object.getProductID());
		filtervec.add(filter);
		
	//	filter = new Filter();
	//	filter.setQuerryString("details.warehousename");
	//	filter.setStringValue(value.trim());
	//	filtervec.add(filter);
		
		query.setFilters(filtervec);
		query.setQuerryObject(new ProductInventoryView());
		showWaitSymbol();
		service.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				hideWaitSymbol();
			}
	
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				hideWaitSymbol();
//				System.out.println("RESULT SIZE : "+result.size());
				double avilableStock=0;
				if(result.size()!=0){
					ProductInventoryView entity=(ProductInventoryView) result.get(0);
					for(ProductInventoryViewDetails prod:entity.getDetails()){
						if(prod.getWarehousename().trim().equals(value.trim())){
							avilableStock=avilableStock+prod.getAvailableqty();
						}
					}
				}
				object.setAvailableStock(avilableStock);
				object.setWarehouseName(value);
//				System.out.println("ROW INDEX "+index+ " WH "+value );
				if(isValidWarehouse){
					AppUtility.ApplyWhiteColoronRow(ProductTablePO.this, index);
				}else{
					AppUtility.ApplyRedColoronRow(ProductTablePO.this, index);
				}
				
				
				if(isValidWarehouse){
					List<ProductDetailsPO> list=getDataprovider().getList();
					Integer rowIndex=getRowCountOfWarehouse(list);
					if(rowIndex!=null){
						isValidWarehouse=false;
						AppUtility.ApplyRedColoronRow(ProductTablePO.this, rowIndex);
					}else{
						isValidWarehouse=true;
						AppUtility.ApplyWhiteColoronRow(ProductTablePO.this, index);
					}
				}
				
				
				table.redraw();
//				System.out.println("ROW UPDATED "+object.getWarehouseName()+" - "+object.getAvailableStock());
				
				PurchaseOrderPresenter.updateWarehouseAndCformStatus();
				
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
			}
	
			
		});
	}
	
	/**
	 * This method returns row index whose which has duplicate warehouse name for perticular product.
	 * Date : 28-09-2016 By Anil
	 * Release : 30 Sept 2016 
	 * Project : PURCHASE MODIFICATION(NBHC)
	 * 
	 */
	
	private Integer getRowCountOfWarehouse(List<ProductDetailsPO> list) {
		for(ProductDetailsPO prod1:list){
			int count=0;
			int index=0;
			for(ProductDetailsPO prod2:list){
				if(prod1.getProductID()==prod2.getProductID()&&prod1.getWarehouseName().equals(prod2.getWarehouseName())){
					count++;
				}
				if(count==2){
					return index;
				}
				index++;
			}
		}
		return null;
	}
	
	public void showWaitSymbol() {
		glassPanel = new GWTCGlassPanel();
		glassPanel.show();
		
		
	}
	public void hideWaitSymbol() {
	   glassPanel.hide();
		
	}
	
	
	/**
	 * This method returns the state name in which warehouse located from global warehouse list.
	 * Date : 03-10-2016 By Anil
	 * Release : 30 Sept 2016 
	 * Project : PURCHASE MODIFICATION(NBHC)
	 * I/P: Warehouse Name
	 * O/p: Warehouse State
	 */
	
	public static String getWarehouseState(String warehouseName){
		String state="";
		for(WareHouse obj:warehouseEntityList){
			if(obj.getBusinessUnitName().trim().equals(warehouseName.trim())){
				return obj.getAddress().getState();
			}
		}
		return "";
		
	}
	
	
	/**
	 * This method returns the address of  warehouse  from global warehouse list.
	 * Date : 16-11-2016 By Anil
	 * Release : 30 Sept 2016 
	 * Project : PURCHASE MODIFICATION(NBHC)
	 * I/P: Warehouse Name
	 * O/p: Warehouse Address
	 */
	
	public static Address getWarehouseAddress(String warehouseName){
		String state="";
		for(WareHouse obj:warehouseEntityList){
			if(obj.getBusinessUnitName().trim().equals(warehouseName.trim())){
				return obj.getAddress();
			}
		}
		return null;
		
	}

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
//		if(event.getSource().equals(itemDtPopUp.getLblOk())){
//			getDataprovider().getList().get(rowIndex).getPrduct().setComment((itemDtPopUp.getTaDetails().getValue()!=null)?itemDtPopUp.getTaDetails().getValue():"");
//			getDataprovider().getList().get(rowIndex).getPrduct().setCommentdesc((itemDtPopUp.getTaDetails1().getValue()!=null)?itemDtPopUp.getTaDetails1().getValue():"");
//			table.redraw();
//			itemDtPopUp.hidePopUp();
//		}else if(event.getSource().equals(itemDtPopUp.getLblCancel())){
//			itemDtPopUp.hidePopUp();
//		}
	}
	

		
	}