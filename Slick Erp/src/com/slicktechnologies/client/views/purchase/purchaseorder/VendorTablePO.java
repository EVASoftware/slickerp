package com.slicktechnologies.client.views.purchase.purchaseorder;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.businessprocesslayer.VendorDetails;

public class VendorTablePO extends SuperTable<VendorDetails>{

	TextColumn<VendorDetails> VendorID;
	TextColumn<VendorDetails> VendorName;
	Column<VendorDetails,String> deleteColumn;
	private Column<VendorDetails,Boolean> stattus;
	TextColumn<VendorDetails> status;
	TextColumn<VendorDetails> email;
	
	
	@Override
	public void createTable() {
		addColumnVendorId();
		addColumnVendorName();
		addColumnVendorviewEmailId();
		createColumndeleteColumn();
		addColumnSelect();
		addFieldUpdater();
	}
	
	
	
	public void addViewColumn()
	{
		addColumnVendorId();
		addColumnVendorName();
		addColumnVendorviewEmailId();
		addColumnstatus();
	
	}
	
	public void addeditColumn()
	{
		addColumnVendorId();
		addColumnVendorName();
		addColumnVendorviewEmailId();
		addColumnSelect();
		createColumndeleteColumn();
		addFieldUpdater();
	}
	
	
	public void addColumnVendorviewEmailId()
	{
		email=new TextColumn<VendorDetails>() {
			
			@Override
			public String getValue(VendorDetails object) {
				return object.getVendorEmailId()+"";
			}
		};table.addColumn(email,"Email");
	}
	
	public void addColumnstatus()
	{
		status=new TextColumn<VendorDetails>() {
			
			@Override
			public String getValue(VendorDetails object) {
				if(object.getStatus()==true)
				return "Selected";
				else 
					return "N/A";
			}
		};table.addColumn(status,"Status");
	}
	
	public void addColumnSelect()
	{
		CheckboxCell chkcell=new CheckboxCell();
		stattus=new Column<VendorDetails, Boolean>(chkcell) {
			
			@Override
			public Boolean getValue(VendorDetails object) {
				// TODO Auto-generated method stub
				return object.getStatus();
			}
		};table.addColumn(stattus,"Select Vendor");
	}

	public void addColumnVendorId()
	{
		VendorID=new TextColumn<VendorDetails>() {
			
			@Override
			public String getValue(VendorDetails object) {
				// TODO Auto-generated method stub
				return object.getVendorId()+"";
			}
		};table.addColumn(VendorID,"Vendor ID");
	}
	
	public void addColumnVendorName()
	{
		VendorName=new TextColumn<VendorDetails>() {
			
			@Override
			public String getValue(VendorDetails object) {
				// TODO Auto-generated method stub
				return object.getVendorName();
			}
		};table.addColumn(VendorName,"Vendor Name");
	}
	
	public void createColumndeleteColumn()
	{
		ButtonCell btnCell= new ButtonCell();
		deleteColumn=new Column<VendorDetails,String>(btnCell)
				{
			@Override
			public String getValue(VendorDetails object)
			{
				return  "Delete" ;
			}
				};
		table.addColumn(deleteColumn,"Delete");

	}

	public void createFieldUpdaterDelete()
	{
		deleteColumn.setFieldUpdater(new FieldUpdater<VendorDetails,String>()
				{
			@Override
			public void update(int index,VendorDetails object,String value)
			{
				getDataprovider().getList().remove(object);

				table.redrawRow(index);
			}
				});

	}
	
	
	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		createFieldUpdaterDelete();
		createFieldUpdaterStatus();
		
	}

	@Override
	public void setEnable(boolean state) {
		int tablecolcount=this.table.getColumnCount();
		for(int i=tablecolcount-1;i>-1;i--)
		  table.removeColumn(i);
		if(state ==true)
			addeditColumn();
		if(state==false)
			addViewColumn();
		
		
	}

	@Override
	public void applyStyle() {
		
	}
	protected void createFieldUpdaterStatus()
	{	
		stattus.setFieldUpdater(new FieldUpdater<VendorDetails, Boolean>()
				{
				@Override
				public void update(int index,VendorDetails object,Boolean value)
				{

					try{
						checkForSelectedRecords();
						Boolean val1=(value);
						object.setStatus(val1);
						
						PurchaseOrderPresenter.retVenStateFlage=true;
						RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
					}
					catch (NumberFormatException e)
					{
					}
				table.redrawRow(index);
				}
				});
	}
	
	/**
	 * This method uncheck other record if selected already
	 * while selecting vendor
	 * Date :20-09-2016 By Anil
	 * Release :30 Aug 2016
	 * Project :PURCHASE ERROR SOLVING(NBHC)
	 */
	private void checkForSelectedRecords(){
		List<VendorDetails>list=getDataprovider().getList();
		List<VendorDetails>newlist=new ArrayList<VendorDetails>();
		
		for(VendorDetails obj:list){
			if(obj.getStatus()){
				obj.setStatus(false);
			}
			newlist.add(obj);
		}
		getDataprovider().getList().clear();
		getDataprovider().getList().addAll(newlist);
		
	}

	/**
	 * ends here 
	 */

	
}
