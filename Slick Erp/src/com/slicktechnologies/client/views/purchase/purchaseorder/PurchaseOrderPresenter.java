package com.slicktechnologies.client.views.purchase.purchaseorder;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.TimeZone;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
























import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.i18n.shared.DateTimeFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.google.gwt.view.client.RowCountChangeEvent.Handler;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.Slick_Erp;
import com.slicktechnologies.client.approvalutility.ApprovableFormScreenPresenter;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.reusabledata.ConditionDialogBox;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.EmailService;
import com.slicktechnologies.client.services.EmailServiceAsync;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.services.SmsService;
import com.slicktechnologies.client.services.SmsServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.ApproverFactory;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.contract.CancellationPopUp;
import com.slicktechnologies.client.views.documentcancellation.CancellationSummary;
import com.slicktechnologies.client.views.documentcancellation.CancellationSummaryPopUp;
import com.slicktechnologies.client.views.documentcancellation.CancellationSummaryPopUpTable;
import com.slicktechnologies.client.views.documentcancellation.DocumentCancellationPopUp;
import com.slicktechnologies.client.views.inventory.grnlist.GRNListPresenter;
import com.slicktechnologies.client.views.inventory.recievingnote.GRNForm;
import com.slicktechnologies.client.views.inventory.recievingnote.GRNPresenter;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.BillingDetailsForm;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.BillingDetailsPresenter;
import com.slicktechnologies.client.views.paymentinfo.billinglist.BillingListPresenter;
import com.slicktechnologies.client.views.paymentinfo.invoicedetails.InvoiceDetailsForm;
import com.slicktechnologies.client.views.popups.NewEmailPopUp;
import com.slicktechnologies.client.views.popups.SMSPopUp;
import com.slicktechnologies.client.views.purchase.purchaserequisition.PurchaseRequitionPresenter;
import com.slicktechnologies.client.views.purchase.vendor.VendorForm;
import com.slicktechnologies.client.views.salesorder.SalesOrderForm;
import com.slicktechnologies.client.views.salesorder.SalesOrderPresenter;
import com.slicktechnologies.server.LoginServiceImpl;
import com.slicktechnologies.server.clientsms.SmsServiceImpl;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.OtherTaxCharges;
import com.slicktechnologies.shared.ProductOtherCharges;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.TaxDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.LetterOfIntent;
import com.slicktechnologies.shared.common.businessprocesslayer.PriceList;
import com.slicktechnologies.shared.common.businessprocesslayer.PriceListDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetailsPO;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseRequisition;
import com.slicktechnologies.shared.common.businessprocesslayer.RequsestForQuotation;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessprocesslayer.VendorDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cancelsummary.CancelSummary;
import com.slicktechnologies.shared.common.contactidentification.ContactPersonIdentification;
import com.slicktechnologies.shared.common.contractcancel.CancelContract;
import com.slicktechnologies.shared.common.email.EmailDetails;
import com.slicktechnologies.shared.common.email.EmailTemplate;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.helperlayer.SmsConfiguration;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
import com.slicktechnologies.shared.common.inventory.GRN;
import com.slicktechnologies.shared.common.inventory.GRNDetails;
import com.slicktechnologies.shared.common.inventory.Inspection;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.productlayer.Tax;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.salesorder.OtherCharges;
import com.slicktechnologies.shared.common.salesorder.OtherChargesMargin;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.sms.SMSDetails;
import com.slicktechnologies.shared.common.smshistory.SmsHistory;

public class PurchaseOrderPresenter extends ApprovableFormScreenPresenter<PurchaseOrder> implements Handler,ChangeHandler {

	public static PurchaseOrderForm form;
	public VendorForm Form;
	final GenricServiceAsync async = GWT.create(GenricService.class);
	EmailServiceAsync emailService = GWT.create(EmailService.class);
	Logger logger = Logger.getLogger("NameOfYourLogger");
	static List<CancellationSummary> cancellis = new ArrayList<CancellationSummary>();
	CsvServiceAsync csvservice = GWT.create(CsvService.class);
	CancellationSummaryPopUp summaryPopup = new CancellationSummaryPopUp();
	PopupPanel summaryPanel;
	DocumentCancellationPopUp cancelPopup = new DocumentCancellationPopUp();
	PopupPanel cancelpanel;
	public TextBox tbStatus;
	int hygeia = 0;

	NewEmailPopUp emailpopup = new NewEmailPopUp();
	
	String retrCompnyState = retrieveCompanyState();
	static String companyState = "";

	public static String vendorState = "";
	String retVenStateName = "";
	
	/**
	 *     Added By : Priyanka Bhagwat
	 *     Date : 1/06/2021
	 *     Des : When Submit PO then SMS and EMAIL will be send Req by Pecopp raised By Ashwini.	
	 */
	final SmsServiceAsync smsserviceAsync=GWT.create(SmsService.class);
	private String actualmsg;
	private long cellNo;
	private String companyName;

	NumberFormat nf = NumberFormat.getFormat("0.00");

	/**
	 * If this flag is true then it retrieve the state in which vendor is
	 * located we are doing it to know which tax is applicable in that scenario.
	 * Date :13-09-2016 By Anil Release :31 Aug 2016 Project : PURCHASE ERROR
	 * SOLVING (NBHC)
	 * 
	 */
	public static boolean retVenStateFlage = false;

	/**
	 * END
	 */
	
	/**
	 * Updated By: Viraj
	 * Date: 22-01-2019
	 * Description: To return list of billing, vendorInvoice and payment document in purchase order 
	 */
	GeneralServiceAsync gasync =  GWT.create(GeneralService.class);
	CancellationPopUp cancellationPopup = new CancellationPopUp();
	/** Ends **/
    /**
     *  By Priyanka
     *  Date : 29-05-2021 Add POPup when print pdf.
     */
	ConditionDialogBox conditionPopupforPreprint=new ConditionDialogBox("Do you want to print on preprinted Stationery ?",AppConstants.YES,AppConstants.NO);
	int cnt=0;
	
	SMSPopUp smspopup = new SMSPopUp();

	public PurchaseOrderPresenter(FormScreen<PurchaseOrder> view,PurchaseOrder model) {
		super(view, model);
		form = (PurchaseOrderForm) view;
		
		form.getProducttable().getTable().addRowCountChangeHandler(this);
//		form.getProductchargestable().getTable().addRowCountChangeHandler(this);
		/** Date 26-11-2018 By Vijay For Other Charges with taxes ****/
		form.getTblOtherCharges().getTable().addRowCountChangeHandler(this);

		form.getVendortbl().getTable().addRowCountChangeHandler(this);
		form.getIbLetterOfIntentID().addChangeHandler(this);
		form.getIbreqforQuotation().addChangeHandler(this);
		form.getPurchaseRequisiteNo().addChangeHandler(this);
		
		form.getCbadds().addClickHandler(this);
		form.addPaymentTerms.addClickHandler(this);
		form.addOtherCharges.addClickHandler(this);
		form.addproducts.addClickHandler(this);
		
//		form.getCbcformlis().addChangeHandler(this);
//		form.getOlbcstpercent().addChangeHandler(this);
		
		form.getDeliveryadd().getState().addChangeHandler(this);

		cancelPopup.getBtnOk().addClickHandler(this);
		cancelPopup.getBtnCancel().addClickHandler(this);
		summaryPopup.getBtnOk().addClickHandler(this);
		summaryPopup.getBtnDownload().addClickHandler(this);
		
		/**
		 * Updated By: Viraj
		 * Date: 22-01-2019
		 * Description: To add cancellation popup with all the related documents
		 */
		cancellationPopup.getLblOk().addClickHandler(this);
		cancellationPopup.getLblCancel().addClickHandler(this);
		/** Ends **/
		
		/**28-05-2021 by Priyanka for company as letter**/
		conditionPopupforPreprint.getBtnOne().addClickHandler(this);
		conditionPopupforPreprint.getBtnTwo().addClickHandler(this);
		
		/**29-12-2018 added by amol for po category mandatory*/
			form.oblPurchaseOrderGroup.addChangeHandler(this);
	
		

		boolean isDownload = AuthorizationHelper.getDownloadAuthorization(Screen.PURCHASEORDER, LoginPresenter.currentModule.trim());
		if (isDownload == false) {
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
		form.btnAddVendor.addClickHandler(this);

	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		/** date 25.8.2018 added by komal to excute submit logic**/
		super.reactToProcessBarEvents(e);

		InlineLabel label = (InlineLabel) e.getSource();
		String text = label.getText().trim();
		
		if(text.equals(AppConstants.SUBMIT)){
			/*** Date 13-10-2018 By Vijay For dont allow user to click twice ***/
			//form.showWaitSymbol();
			reactOnSubmit();
		}

//		if (text.equals(ManageApprovals.APPROVALREQUEST)) {
//			if(valideForReqForApproval()){
//				form.getManageapproval().reactToRequestForApproval();
//			}
//		}
//		if (text.equals(ManageApprovals.CANCELAPPROVALREQUEST)) {
//			form.getManageapproval().reactToApprovalRequestCancel();
//		}
		if (text.equals(AppConstants.VIEWGRN)) {
			viewGRNdocuments();
		}
		if (text.equals("New")) {
			form.setToNewState();
			initalize();
			tbStatus.setEnabled(false);
			tbStatus.setValue(PurchaseOrder.CREATED);
		}
		if (text.equals("Email")) {
			reactOnEmail();
		}
		if (text.equals(AppConstants.CANCELPURCHASEORDER)) {
			/**
			 * Updated By: Viraj
			 * Date: 22-01-2019
			 * Description: commented old code and added grn validation on cancel po click
			 */
//			cancelPopup.getPaymentDate().setValue(new Date() + "");
//			cancelPopup.getPaymentID().setValue(model.getCount() + "");
//			cancelPopup.getPaymentStatus().setValue(model.getStatus());
//			cancelpanel = new PopupPanel(true);
//			cancelpanel.add(cancelPopup);
//			cancelpanel.show();
//			cancelpanel.center();
			Console.log("Inside click of cancel PO");
			checkForGRNStatus();
			/** Ends **/
		}

		if (text.equals(AppConstants.CANCELLATIONSUMMARY)){
			getSummaryFromPurchaseOrderID();
		}
		
		
		if (text.equals(AppConstants.SENDMAILTOASSIGNPERSON)){
			
			/**
			 * Here we send mail to assign to 1&2 whom po is assigned for further process
			 * Date : 17-10-2016 By Anil
			 * Release : 30 Sept 2016
			 * Project : PURCHASE MODIFICATION (NBHC)
			 */
			
			System.out.println("Send Mail Button Clicked !!!");
			
			if(form.olbAssignedTo1.getSelectedIndex()==0&&form.olbAssignedTo2.getSelectedIndex()==0){
				form.showDialogMessage("Please select assign to 1 and/or assign to 2 !");
				return;
			}
			
			final ArrayList<String> toEmailList=new ArrayList<String>();
			final ArrayList<String> ccEmailList=new ArrayList<String>();
			
			System.out.println("SEND EMAIL FLAG TRUE");
			if(form.olbAssignedTo1.getSelectedIndex()!=0&&form.olbAssignedTo2.getSelectedIndex()!=0){
				
				if(form.olbAssignedTo1.getValue().equals(form.olbAssignedTo2.getValue())){
					Employee emp1=getEmployeeDetails(form.olbAssignedTo1.getValue());					
					if(emp1!=null){
						if(emp1.getEmail()!=null&&!emp1.getEmail().equals("")){
							toEmailList.add(emp1.getEmail());
						}
					}
					
					Employee emp2=getEmployeeDetails(form.olbAssignedTo2.getValue());
					if(emp2!=null){
						if(emp2.getEmail()!=null&&!emp2.getEmail().equals("")){
							toEmailList.add(emp2.getEmail());
						}
					}
							
				}else{
					Employee emp1=getEmployeeDetails(form.olbAssignedTo1.getValue());
					if(emp1!=null){
						if(emp1.getEmail()!=null&&!emp1.getEmail().equals("")){
							toEmailList.add(emp1.getEmail());
						}
					}
				}
				
			}else if(form.olbAssignedTo1.getSelectedIndex()!=0&&form.olbAssignedTo2.getSelectedIndex()==0){
				Employee emp1=getEmployeeDetails(form.olbAssignedTo1.getValue());
				if(emp1!=null){
					if(emp1.getEmail()!=null&&!emp1.getEmail().equals("")){
						toEmailList.add(emp1.getEmail());
					}
				}
				
			}else if(form.olbAssignedTo1.getSelectedIndex()==0&&form.olbAssignedTo2.getSelectedIndex()!=0){
				Employee emp2=getEmployeeDetails(form.olbAssignedTo2.getValue());
				if(emp2!=null){
					if(emp2.getEmail()!=null&&!emp2.getEmail().equals("")){
						toEmailList.add(emp2.getEmail());
					}
				}
			}
			
			System.out.println("To Email List Size : "+toEmailList.size());
			
			MyQuerry querry=new MyQuerry();
			Filter filter=new Filter();
			
			filter.setLongValue(UserConfiguration.getCompanyId());
			filter.setQuerryString("companyId");
			
			querry.getFilters().add(filter);
			querry.setQuerryObject(new Company());
			
			form.showWaitSymbol();
			service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {

				@Override
				public void onFailure(Throwable caught) {
					form.hideWaitSymbol();
				}

				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					String fromEmail=null;
					Company comp=(Company) result.get(0);
					if(comp!=null){
						if(comp.getEmail()!=null&&!comp.getEmail().equals("")){
							fromEmail=comp.getEmail();
						}
					}
					
					if(fromEmail!=null){
						Employee emp1=getEmployeeDetails(form.oblEmployee.getValue());
						if(emp1!=null){
							if(emp1.getEmail()!=null&&!emp1.getEmail().equals("")){
								ccEmailList.add(emp1.getEmail());
							}
						}
						
						String requester="";
						if(LoginPresenter.loggedInUser!=null){
							requester=LoginPresenter.loggedInUser;
						}
						System.out.println("Form Email Not Null");
						String mailSub="Process PO Id -"+form.ibPOId.getValue()+"";
						String mailMsg="Dear Sir/Madam"+","+"\n"+"\n"+"\n"
										+"PO Id "+form.ibPOId.getValue()+" is assigned to you please process."+"\n"+"\n"+"\n"
										+"Purchase Engineer : "+form.oblEmployee.getValue()+"\n"
										+"Requester : "+requester+"\n";
						
						
						emailService.sendEmail(mailSub, mailMsg, toEmailList, ccEmailList, fromEmail, new AsyncCallback<Void>() {
							@Override
							public void onFailure(Throwable caught) {
								form.hideWaitSymbol();
							}

							@Override
							public void onSuccess(Void result) {
								form.hideWaitSymbol();
								form.showDialogMessage("Email sent successfully");
							}
						});
					}
					
				}
			});
			
			
			
			
			
		}
		
		if (text.equals(AppConstants.VIEWSALESORDER)){
			viewSaleOrder();
		}
		if (text.equals(AppConstants.COPY)){
			reactOnCopy();
		}
	}

	
	/**
	 *     Added By : Priyanka Bhagwat
	 *     Date : 1/06/2021
	 *     Des : When Submit PO then SMS and EMAIL will be send Req by Pecopp raised By Ashwini.	
	 */
	
	private void reactOnEmailPO() {
		
		String toEmail="";
		String fromEmail="";
		int vendorId=0;

		String branchName = form.branch.getValue();
		fromEmail = AppUtility.getFromEmailAddress(branchName,model.getEmployee());
		//logger.log(Level.SEVERE,"Inside Email from mailId:" +fromEmail);
		if(fromEmail.equals("")){
			return;
		}
		
		if(model!=null){
			if(model.getVendorDetails()!=null&&model.getVendorDetails().size()!=0){
				for(VendorDetails vendor:model.getVendorDetails()){
					if(vendor.getStatus()){//
						if(vendor.getVendorEmailId()!=null&&!vendor.getVendorEmailId().equals("")){
							toEmail=vendor.getVendorEmailId();
						}
						vendorId=vendor.getVendorId();
					}
				}
				
				if(toEmail.equals("")){
					toEmail = AppUtility.getVendorEmailid(vendorId);
					logger.log(Level.SEVERE,"Inside Email to mailId:" +toEmail);
					//toEmail = AppUtility.getvenodrEmailId(vendorlist,false);
				}
			}
		}
		
		if(toEmail.equals("")){
			return;
		}
		
		
		EmailDetails email = new EmailDetails();
		email.setFromEmailid(fromEmail);
		ArrayList<String> toemailId = new ArrayList<String>();
//		ArrayList<String> toemailId = new ArrayList<String>();
//		toemailId = getEmailIdlist(taToEmailId);
		toemailId.add(toEmail);
		email.setToEmailId(toemailId);
		
		ArrayList<String> ccemailId = new ArrayList<String>();
		email.setCcEmailId(ccemailId);
		
		ArrayList<String> bccemailId = new ArrayList<String>();
		email.setBccEmailId(bccemailId);
		
		EmailTemplate emailTemp=null;
		if(LoginPresenter.globalEmailTemplatelist!=null&&LoginPresenter.globalEmailTemplatelist.size()!=0){
			for(EmailTemplate obj:LoginPresenter.globalEmailTemplatelist){
				if(obj.getTemplateName().equals("Email PO On Approval")&&obj.isTemplateStatus()){ // 
					emailTemp=obj;
					break;
				}
			}
		}
		
		String emailSubject="";
		String emailBody="";
		if(emailTemp==null){
			return;
		}
		
		emailSubject=emailTemp.getSubject();
		Console.log("emailTemp.getSubject()");
		emailSubject=emailSubject.replace("{PurchaseOrderNo}", model.getCount()+"");
		email.setSubject(emailSubject);
		
		
		
		
	
		
		
		emailBody=emailTemp.getEmailBody();
		emailBody=emailBody.replace("{PurchaseOrderNo}", model.getCount()+"");
		
		final String serDate = AppUtility.parseDate(model.getPODate());
		
		emailBody=emailBody.replace("{PODate}", serDate);//Slick_Erp.businessUnitName;
		emailBody=emailBody.replace("{companyName}",Slick_Erp.businessUnitName+"");
		
		emailBody = emailBody.replaceAll("[\n]", "<br>");
		emailBody = emailBody.replaceAll("[\\s]", "&nbsp;");
		email.setEmailBody(emailBody);
		
		email.setPdfAttachment(emailTemp.isPdfattachment());
		Console.log("emailTemp.isPdfattachment()"+emailTemp.isPdfattachment());
		
		Screen screenName=(Screen) AppMemory.getAppMemory().currentScreen;
		String EntityName =AppUtility.getEntityName(screenName);
		email.setEntityName(EntityName);
		Console.log("screenName"+screenName);
		Console.log("EntityName"+EntityName);
		if(model!=null){
			email.setModel(model);
		}
		
		EmailServiceAsync emailAsync = GWT.create(EmailService.class);
		emailAsync.sendEmail(email, model.getCompanyId(), new AsyncCallback<String>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onSuccess(String result) {
				// TODO Auto-generated method stub
			}
		});
		
	}

	
	/**
	 *     Added By : Priyanka Bhagwat
	 *     Date : 1/06/2021
	 *     Des : When Submit PO then SMS and EMAIL will be send Req by Pecopp raised By Ashwini.	
	 */
	private void reactOnSubmit() {
//		checkSendSmsStatus();
		sendSMS();
		reactOnEmailPO();
	
	}

	

	private void viewGRNdocuments() {


		final MyQuerry querry=new MyQuerry();
		Vector<Filter> temp=new Vector<Filter>();
		Filter filter=null;
		
		filter=new Filter();
		filter.setQuerryString("poNo");
		filter.setIntValue(model.getCount());
		temp.add(filter);
		
	
		
		querry.setFilters(temp);
		querry.setQuerryObject(new GRN());
		form.showWaitSymbol();
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				form.hideWaitSymbol();
				if(result.size()==0){
					form.showDialogMessage("No GRN found.");
					return;
				}
				if(result.size()==1){
					final GRN grn=(GRN) result.get(0);
					final GRNForm form=GRNPresenter.initalize();
					Timer timer=new Timer() {
						@Override
						public void run() {
							form.updateView(grn);
							form.setToViewState();
						}
					};
					timer.schedule(1000);
				}else{
					GRNListPresenter.initalize(querry);
				}
				
			}
		});
		
	
		
	
	}

	private void viewSaleOrder() {
		int salesCount;
		try{
			salesCount=Integer.parseInt(model.getRefOrderNO().trim());
		}catch(NumberFormatException e){
			form.showDialogMessage("No sales order found.");
			return;
		}
		
		
		
		MyQuerry querry=new MyQuerry();
		Vector<Filter> temp=new Vector<Filter>();
		Filter filter=null;
		
		filter=new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(salesCount);
		temp.add(filter);
		
		querry.setFilters(temp);
		querry.setQuerryObject(new SalesOrder());
		form.showWaitSymbol();
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
			}
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				form.hideWaitSymbol();
				if(result.size()==0){
					form.showDialogMessage("No sales order found.");
					return;
				}
				if(result.size()>0){
					final SalesOrder contract=(SalesOrder) result.get(0);
					final SalesOrderForm form=SalesOrderPresenter.initalize();
					Timer timer=new Timer() {
						@Override
						public void run() {
							form.updateView(contract);
							form.setToViewState();
						}
					};
					timer.schedule(1000);
				}
			}
		});
	
	}
	
	/**
	 * Return Employee details from global employee list
	 * Date : 17-10-2016 By Anil
	 * Release : 30 Sept 2016
	 * Project : PURCHASE MODIFICATION (NBHC)
	 */
	
	public Employee getEmployeeDetails(String fullname){
		Employee emp=null;
		for(Employee obj:LoginPresenter.globalEmployee){
			if(fullname.equals(obj.getFullname())){
				return obj;
			}
		}
		
		return emp;
	}
	
	
	
	
	/**
	 * This method validate the payment terms and vendor addition while requesting for approval.
	 * Date : 13-10-2016 By Anil
	 * Release :30 Sept 2016
	 * Projetc : PURCHASE MODIFICATION(NBHC)
	 */
	public boolean valideForReqForApproval(){
		if(form.paymentTermsTable.getDataprovider().getList().size() == 0
				&&form.vendorTablePO.getDataprovider().getList().size()==0){
			form.showDialogMessage("Please add payment Terms !"+"\n"+"Please add vendor !");
			return false;
		}
		
		if(form.paymentTermsTable.getDataprovider().getList().size() == 0) {
			form.showDialogMessage("Please add payment Terms !");
			return false;
		}
		
		if(form.vendorTablePO.getDataprovider().getList().size()==0){
			form.showDialogMessage("Please add vendor !");
			return false;
		}
		
		if(form.getHeader().getValue().equals("")){
			form.showDialogMessage("Please fill header section!");
			return false;
		}
		if(form.getFooter().getValue().equals("")){
			form.showDialogMessage("Please fill footer section!");
			return false;
		}
		
		return true;
	}

	@Override
	public void reactOnPrint() {

		MyQuerry querry = new MyQuerry();
		Company c = new Company();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("processName");
		filter.setStringValue("PurchaseOrder");
		filtervec.add(filter);

		filter = new Filter();
		filter.setQuerryString("processList.status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);

		querry.setFilters(filtervec);
		querry.setQuerryObject(new ProcessConfiguration());

		async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				System.out.println(" result set size +++++++"+ result.size());

				List<ProcessTypeDetails> processList = new ArrayList<ProcessTypeDetails>();

				if (result.size() == 0) {
					final String url = GWT.getModuleBaseURL()+ "pdfpurchase" + "?Id=" + model.getId()+"&"+"preprint="+"plane";
					Window.open(url, "test", "enabled");
				}
				/**
				 * Date 20-7-2018
				 */
				else if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("PurchaseOrder", "POPDFV1")){
					final String url = GWT.getModuleBaseURL()+ "pdfpurchase" + "?Id=" + model.getId()+"&"+"type="+"POPDFV1";
					Window.open(url, "test", "enabled");
				}/* nidhi for copy of shasha for other client*/
				else if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("PurchaseOrder", "PurchaseOrderUpdatedPdf")){
					final String url = GWT.getModuleBaseURL()+ "pdfpurchase" + "?Id=" + model.getId()+"&"+"type="+"PurchaseOrderUpdatedPdf";
					Window.open(url, "test", "enabled");
				}
				/* nidhi for copy of shasha for other client*/
				else if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("PurchaseOrder", "PurchaseOrderUpdatedPdf")){
					final String url = GWT.getModuleBaseURL()+ "pdfpurchase" + "?Id=" + model.getId()+"&"+"type="+"PurchaseOrderUpdatedPdf";
					Window.open(url, "test", "enabled");
				}
				else {
					for (SuperModel model : result) {
						ProcessConfiguration processConfig = (ProcessConfiguration) model;
						processList.addAll(processConfig.getProcessList());
					}
					for (int k = 0; k < processList.size(); k++) {
						if (processList.get(k).getProcessType().trim().equalsIgnoreCase("HygeiaPestManagement")
								&&processList.get(k).isStatus() == true) {
							hygeia = hygeia + 1;
						}
						
						if(processList.get(k).getProcessType().trim().equalsIgnoreCase("CompanyAsLetterHead")&&processList.get(k).isStatus()==true){
							
							cnt=cnt+1;
						
						}
						
					}
					if(cnt>0){
						System.out.println("in side react on prinnt cnt");
						summaryPanel=new PopupPanel(true);
						summaryPanel.add(conditionPopupforPreprint);
						summaryPanel.setGlassEnabled(true);
						summaryPanel.show();
						summaryPanel.center();
					}
					
					else if (hygeia > 0) {
						final String url = GWT.getModuleBaseURL()+ "pdfpurchaseorder" + "?Id="+ model.getId();
						Window.open(url, "test", "enabled");
					} else {
						final String url = GWT.getModuleBaseURL()+ "pdfpurchase" + "?Id="+ model.getId()+"&"+"preprint="+"plane";
						Window.open(url, "test", "enabled");
					}
				}
			}
		});
	}

	@Override
	public void reactOnEmail() {

//		boolean conf = Window.confirm("Do you really want to send email?");
//		if (conf == true) {
//			emailService.initiatePOEmail((PurchaseOrder) model,new AsyncCallback<Void>() {
//				@Override
//				public void onSuccess(Void result) {
//					Window.alert("Email Sent Sucessfully !");
//				}
//
//				@Override
//				public void onFailure(Throwable caught) {
//					Window.alert("Resource Quota Ended");
//					caught.printStackTrace();
//				}
//			});
//		}
		
		/**
		 * @author Vijay Date 19-03-2021
		 * Des :- above old code commented 
		 * Added New Email Popup Functionality
		 */
		emailpopup.showPopUp();
		setEmailPopUpData();
		
		/**
		 * ends here
		 */
	}

	@Override
	protected void makeNewModel() {
		model = new PurchaseOrder();

	}

	public static PurchaseOrderForm initalize() {
//		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Purchase Order", Screen.PURCHASEORDER);
		PurchaseOrderForm form = new PurchaseOrderForm();

		PurchaseOrderPresenterTableProxy gentable = new PurchaseOrderPresenterTableProxy();
		gentable.setView(form);
		gentable.applySelectionModle();

		PuchaseOrderPresenterSearchProxy.staticSuperTable = gentable;
		PuchaseOrderPresenterSearchProxy searchpopup = new PuchaseOrderPresenterSearchProxy();
		form.setSearchpopupscreen(searchpopup);
		PurchaseOrderPresenter presenter = new PurchaseOrderPresenter(form,new PurchaseOrder());
		/**
		 * Date : 11-10-2017 BY ANIL
		 * This line was added to update the name of screen loaded from any other screen or by clicking on billing details on account model
		 */
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Purchase/Purchase Order",Screen.PURCHASEORDER);
		/**
		 * END
		 */
		
		AppMemory.getAppMemory().stickPnel(form);
		return form;
	}

	@Override
	public void reactOnDownload() {
		CsvServiceAsync async = GWT.create(CsvService.class);
		ArrayList<PurchaseOrder> POArray = new ArrayList<PurchaseOrder>();
		List<PurchaseOrder> list = form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
		POArray.addAll(list);
		async.setPO(POArray, new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed" + caught);
			}

			@Override
			public void onSuccess(Void result) {
				String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url = gwt + "csvservlet" + "?type=" + 46;
				Window.open(url, "test", "enabled");
			}
		});
	}

	@Override
	public void onRowCountChange(RowCountChangeEvent event) {
		NumberFormat nf = NumberFormat.getFormat("0.00");

		if (event.getSource().equals(form.getProducttable().getTable())) {
			System.out.println("Row Count Change Called");
			this.productsOnChange();
		}
//		if (event.getSource().equals(form.getProductchargestable().getTable())) {
//			if (form.getDoamtincltax().getValue() != null) {
//				double newnetpay = form.getDoamtincltax().getValue()+ form.getProductchargestable().calculateNetPayable();
//				newnetpay = Math.round(newnetpay);
//				int netPayAmt = (int) newnetpay;
//				newnetpay = netPayAmt;
//				
//				/**
//				 * Date 08/06/2018
//				 * Developer :- Vijay
//				 * Des :- for final total amt Round off amt calculations
//				 */
//				form.getDbfinalTotalAmt().setValue(Double.parseDouble(nf.format(newnetpay)));
//				
//				if(form.getTbroundoffAmt().getValue()!=null && !form.getTbroundoffAmt().getValue().equals("")){
//					String roundOff = form.getTbroundoffAmt().getValue();
//					double roundoffAmt =  AppUtility.calculateRoundOff(roundOff, newnetpay);
//					System.out.println("roundoffAmt =="+roundoffAmt);
//					if(roundoffAmt!=0){
//						form.getDonetpayamt().setValue(roundoffAmt);
//					}else{
//						form.getDonetpayamt().setValue(Double.parseDouble(nf.format(newnetpay)));
//						form.getTbroundoffAmt().setValue("");
//					}
//				}else{
//					form.getDonetpayamt().setValue(Double.parseDouble(nf.format(newnetpay)));
//
//				}
//				/**
//				 * ends here
//				 */
//				form.getDonetpayamt().setValue(Double.parseDouble(nf.format(newnetpay)));
//			}
//		}

		/**
		 *  Date 26-11-2018 By Vijay 
		 *  Des :- As per the Other Charges updated table And Taxes Code updated
		 *  above old code commented
		 */
		
		
		/**
		 * Date=19-7-2019 
		 * @author Amol 
		 * Description= to update the other charges table
		 */
		
		
		double totalOfProductPrice=0;
		if (event.getSource().equals(form.tblOtherCharges.getTable())) {
			if(form.tblOtherCharges.getValue().size()!=0){
				totalOfProductPrice= form.getDbTotal().getValue();
				for(OtherCharges obj:form.getTblOtherCharges().getValue()){
					double otherChargAmt=0;
					OtherTaxCharges otherTax=form.tblOtherCharges.getOtherChargesDetails(obj.getOtherChargeName());
					if(otherTax!=null){
						if(otherTax.getOtherChargePercent()!=0){
							otherChargAmt=((totalOfProductPrice*otherTax.getOtherChargePercent())/100);
							obj.setAmount(otherChargAmt);
						}else if(otherTax.getOtherChargeAbsValue()!=null){
//							otherChargAmt=otherTax.getOtherChargeAbsValue();
//							obj.setAmount(otherChargAmt);
							if(obj.getAmount()==0){
								otherChargAmt=otherTax.getOtherChargeAbsValue();
								obj.setAmount(otherChargAmt);
							}
						}
					}
				}
				form.addPoOtherCharges();
//				/**
//				 * @author Anil
//				 * @since 20-07-2020
//				 */
//				OtherChargesMargin obj=new OtherChargesMargin();
//				obj.setCostType("PO(Other Charges)");
//				obj.setOtherCharges(sumOfOcPo);
//				obj.setReadOnly(true);
//				if(form.otherChargesMargins==null||form.otherChargesMargins.size()==0){
//					form.otherChargesMargins=new ArrayList<OtherChargesMargin>();
//					form.otherChargesMargins.add(obj);
//				}else{
//					boolean updateFlag=true;
//					for(OtherChargesMargin margin:form.otherChargesMargins){
//						if(margin.getCostType().equals("PO(Other Charges)")){
//							updateFlag=false;
//							margin.setOtherCharges(sumOfOcPo);
//							margin.setReadOnly(true);
//						}
//					}
//					if(updateFlag){
//						form.otherChargesMargins.add(obj);
//					}
//				}
			}
			
			
			form.getProducttaxtable().connectToLocal();
			try {
				form.addProdTaxes();
				form.addOtherChargesInTaxTbl();
				/** Date 26-11-2018 By Vijay For Other Charges***/
				form.getDoamtincltax().setValue(form.tblOtherCharges.calculateOtherChargesSum());
			} catch (Exception e) {
				e.printStackTrace();
			}
			/**
			 *  Date 26-11-2018 By Vijay 
			 *  Des :- As per the Other Charges And Taxes Code updated
			 */
				double totalIncludingTax=form.getDbTotal().getValue()+form.getProducttaxtable().calculateTotalTaxes();
				double newnetpay = totalIncludingTax+ form.tblOtherCharges.calculateOtherChargesSum();
				form.getDbfinalTotalAmtWithoutRoundoff().setValue(newnetpay);//Ashwini Patil Date:27-05-2023
				newnetpay = Math.round(newnetpay);
				int netPayAmt = (int) newnetpay;
				newnetpay = netPayAmt;
				
				/**
				 * Date 08/06/2018
				 * Developer :- Vijay
				 * Des :- for final total amt Round off amt calculations
				 */
				
				form.getDbfinalTotalAmt().setValue(Double.parseDouble(nf.format(newnetpay)));
				
				if(form.getTbroundoffAmt().getValue()!=null && !form.getTbroundoffAmt().getValue().equals("")){
					String roundOff = form.getTbroundoffAmt().getValue();
					double roundoffAmt =  AppUtility.calculateRoundOff(roundOff, newnetpay);
					System.out.println("roundoffAmt =="+roundoffAmt);
					if(roundoffAmt!=0){
						form.getDonetpayamt().setValue(roundoffAmt);
					}else{
						form.getDonetpayamt().setValue(Double.parseDouble(nf.format(newnetpay)));
						form.getTbroundoffAmt().setValue("");
					}
				}else{
					form.getDonetpayamt().setValue(Double.parseDouble(nf.format(newnetpay)));

				}
				/**
				 * ends here
				 */
				form.getDonetpayamt().setValue(Double.parseDouble(nf.format(newnetpay)));
		}
		if (event.getSource().equals(form.getVendortbl().getTable())) {

//			Timer timer = new Timer() {
//				@Override
//				public void run() {
					System.out.println("VENDOR ROW COUNT CHANGE .....");
					System.out.println("RET VENDOR STATE FLAG "+ retVenStateFlage);
					
					form.productTablePO.setEnable(true);

					if (retVenStateFlage) {
						if (form.validateVendorselect()) {
							int vendorCount = retrieveVendorCount();
							System.out.println("Vendor COUNT - " + vendorCount);
							retVenStateName = retVendorState(vendorCount);
						}else{
							vendorState="";
						}
						retVenStateFlage = false;
					}
//				}
//			};
//			timer.schedule(1000);
		}

	}


	protected boolean validateProductId(int productId,List<ProductDetailsPO> tableProducts) {
		int ctr = 0;
		boolean flagVal = false;

		for (int i = 0; i < tableProducts.size(); i++) {
			if (tableProducts.get(i).getProductID() == productId) {
				ctr = ctr + 1;
			}
		}

		if (ctr == 0) {
			flagVal = false;
		} else {
			flagVal = true;
		}
		return flagVal;
	}

	

	public List<VendorDetails> vendorListRetrieved(List<PurchaseOrderVendors> listOfVendors,
			List<VendorDetails> vendorList) {

		ArrayList<VendorDetails> vendorTableSet = new ArrayList<VendorDetails>();
		for (VendorDetails vd : vendorList) {
			int vendorCount = vd.getVendorId();
			for (int k = 0; k < listOfVendors.size(); k++) {
				if (listOfVendors.get(k).getVendorId() == vendorCount) {
					VendorDetails vdEntity = new VendorDetails();
					vdEntity.setVendorId(listOfVendors.get(k).getVendorId());
					vdEntity.setVendorName(listOfVendors.get(k).getVendorName());
					vdEntity.setVendorPhone(listOfVendors.get(k).getVendorCellNumber());
					vdEntity.setVendorEmailId(listOfVendors.get(k).getVendorEmail());

					// vijay
					vdEntity.setPocName(listOfVendors.get(k).getVendorPocName());

					vendorTableSet.add(vdEntity);
				}
			}
		}
		return vendorTableSet;

	}

	/**
	 * On Change Event This method handles change event for loi id,rfq id, pr
	 * id, warehouse
	 */

	@Override
	public void onChange(ChangeEvent event) {

		/************************************** LOI ID ************************************************/

		if (event.getSource().equals(form.getIbLetterOfIntentID())) {
			this.setLOIData();
		}
		if (event.getSource().equals(form.getIbreqforQuotation())) {
			this.setRfqData();
		}
		if (event.getSource().equals(form.getPurchaseRequisiteNo())) {
			this.setPRData();
		}
		
		/**29-12-2018 added by amol for po category and description madatory
		 * 
		 */
		if (event.getSource().equals(form.oblPurchaseOrderGroup)) {
			boolean checkFlag=AppUtility.checkForProcessConfigurartionIsActiveOrNot("PurchaseOrder", "PoDescriptionMandatory");
			if(form.oblPurchaseOrderGroup.getSelectedIndex()!=0&&checkFlag){
				ConfigCategory cat=form.oblPurchaseOrderGroup.getSelectedItem();
				if(cat.getDescription()==null||cat.getDescription().equals("")){
					form.showDialogMessage("Please add description in po category.");
					form.oblPurchaseOrderGroup.setSelectedIndex(0);
					return;
				}
			}
		}

		/**
		 * On change of warehouse
		 */
//		if (event.getSource().equals(form.getOblwarehouse())
//				&& form.getCbadds().getValue() == false) {
//
//			if (form.getOblwarehouse().getSelectedIndex() != 0) {
//				this.setDefaultAdd();
//				isCformCstApplicable();
//				productsOnChange();
//			}
//			if (form.getOblwarehouse().getSelectedIndex() == 0
//					&& form.getCbadds().getValue() == false) {
//				form.getCompanyAddress(model.getCompanyId());
//				form.getDeliveryadd().setEnable(false);
//			}
//
//		}

		/**
		 * On change of cform
		 */

//		if (event.getSource().equals(form.getCbcformlis())) {
//			if (form.cbcformlis.getSelectedIndex() != 0) {
//				int index = form.cbcformlis.getSelectedIndex();
//				if (AppConstants.YES.equals(form.cbcformlis.getItemText(index)
//						.trim())) {
//					form.getOlbcstpercent().setEnabled(true);
//				}
//				if (AppConstants.NO.equals(form.cbcformlis.getItemText(index)
//						.trim())) {
//					productsOnChange();
//					form.getOlbcstpercent().setEnabled(false);
//					form.getOlbcstpercent().setSelectedIndex(0);
//				}
//			}
//			if (form.getCbcformlis().getSelectedIndex() == 0) {
//				form.getOlbcstpercent().setSelectedIndex(0);
//				form.getOlbcstpercent().setEnabled(false);
//				productsOnChange();
//			}
//		}

		/**
		 * On change of CST PERCENT
		 */

//		if (event.getSource().equals(form.getOlbcstpercent())) {
//			if (form.getOlbcstpercent().getSelectedIndex() != 0) {
//				productsOnChange();
//			}
//		}

		/**
		 * On change of state of Delivery Address
		 */

		if (event.getSource().equals(form.getDeliveryadd().getState())) {
//			if (!checkVendorState()
//					&& form.getDeliveryadd().getState().getSelectedIndex() != 0) {
//				form.getCbcformlis().setEnabled(true);
//
//			}
//			if (checkVendorState()
//					&& form.getDeliveryadd().getState().getSelectedIndex() != 0) {
//				form.getCbcformlis().setEnabled(false);
//				productsOnChange();
//			}
//			if (form.getDeliveryadd().getState().getSelectedIndex() == 0) {
//				form.getCbcformlis().setEnabled(false);
//				form.getOlbcstpercent().setEnabled(false);
//				form.getCbcformlis().setSelectedIndex(0);
//				form.getOlbcstpercent().setValue(null);
//			}
		}

	}

	/**
	 * On click event This method handles click event for payment terms,
	 * delivery address checkbox, add other charges and addproducts
	 */

	@Override
	public void onClick(ClickEvent event) {
		super.onClick(event);
		
		/**
		 *  Added by Priyanka 
		 *  Date : 28-05-2021 
		 *  Des : Company As letter head active on PO.
		 */
		if(event.getSource().equals(conditionPopupforPreprint.getBtnOne()))
		{
			
			if(cnt >0){
				System.out.println("in side one yes");
				
				final String url = GWT.getModuleBaseURL()+ "pdfpurchase" + "?Id=" + model.getId()+"&"+"preprint="+"yes";
				Window.open(url, "test", "enabled");
				
				summaryPanel.hide();
			}
			
			
		}
		
		
		if(event.getSource().equals(conditionPopupforPreprint.getBtnTwo()))
		{
			if(cnt >0){					
				
				final String url = GWT.getModuleBaseURL()+ "pdfpurchase" + "?Id=" + model.getId()+"&"+"preprint="+"no";
				Window.open(url, "test", "enabled");
				 
				summaryPanel.hide();
			}
			
			
		}
		
		
		
		
		
		
		
		
		
		
		
		
		

		if (event.getSource().equals(form.addPaymentTerms)) {
			form.reactOnAddPaymentTerms();
		}
		if (event.getSource().equals(form.getCbadds())) {
			if (form.getCbadds().getValue() == true) {
				form.getDeliveryadd().clear();
				form.getDeliveryadd().setEnable(true);
			} 
		}

//		if (event.getSource().equals(form.addOtherCharges)) {
//			int size = form.productchargestable.getDataprovider().getList().size();
//			if (form.doamtincltax.getValue() != null) {
//				ProductOtherCharges prodCharges = new ProductOtherCharges();
//				prodCharges.setAssessableAmount(form.getDoamtincltax().getValue());
//				prodCharges.setFlagVal(false);
//				prodCharges.setIndexCheck(size + 1);
//				form.productchargestable.getDataprovider().getList().add(prodCharges);
//			}
//		}

		/**
		 * Date : 24-11-2018 By Vijay
		 * Des :- For Other Charges with Taxes Update and above old code commented
		 */
			
		if(event.getSource().equals(form.addOtherCharges)){
			Tax tax1=new Tax();
			Tax tax2=new Tax();
			OtherCharges prodCharges=new OtherCharges();
			prodCharges.setOtherChargeName("");
			prodCharges.setAmount(0);
			prodCharges.setTax1(tax1);
			prodCharges.setTax2(tax2);
			form.tblOtherCharges.getDataprovider().getList().add(prodCharges);
		}
		/**
		 * End
		 */
		
		/**
		 * @author Anil @since 31-01-2021
		 * This was throwing an alert message every time on click of search or go button
		 * raised by Rahul Tiwari
		 */
//		if(form.branch.getSelectedIndex()==0){
//			form.showDialogMessage("Please select branch!");
//			return;
//		}
		
		if (event.getSource().equals(form.addproducts)) {
			if (!form.prodInfoComposite.getProdCode().getValue().equals("")) {
				form.showWaitSymbol();
				
				form.addproducts.setEnabled(false);
				Console.log("PrintPCD"+ form.prodInfoComposite.getProdCode().getValue());
				boolean productValidation = form.validateProductDuplicate(form.prodInfoComposite.getProdCode().getValue().trim(),null);

				if (productValidation) {
					
					form.ProdctType(form.prodInfoComposite.getProdCode().getValue().trim(), form.prodInfoComposite.getProdID().getValue().trim());
					
					form.prodInfoComposite.clear(); //Ashwini Patil Date: 5-07-2022
				}else{
					form.addproducts.setEnabled(true);
					form.hideWaitSymbol();//Ashwini Patil Date:24-04-2023
				}
			}
		}

		if (event.getSource().equals(form.btnAddVendor)) {
			if (event.getSource().equals(form.btnAddVendor)
					&& !form.vendorComp.getName().getValue().equals("")) {
				if (form.validVendor()) {
					form.checkPriceListAndAddVendorToVendorTable();

				}
			}
		}
		/**
		 * Updated By: Viraj
		 * Date: 22-01-2019
		 * Description: To add cancellation popup with all the related documents
		 */
		if(event.getSource() == cancellationPopup.getLblOk()){
			/**
			 * @author Anil @since 06-03-2021
			 * Due to process configuration for remark drop down 
			 * system was throwing error by reading text field 
			 * this condition should be handle at all level or should written in a such a way that 
			 * it will not impact other functionality
			 * Raised by Ashwini for Pecopp
			 */
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "EnableCancellationRemark")){
				if(cancellationPopup.getOlbCancellationRemark().getValue().trim()!= null && !cancellationPopup.getOlbCancellationRemark().getValue().trim().equals("")){
					//  react on contract cancel
					reactOnCancelContractButton(cancellationPopup.getOlbCancellationRemark().getValue().trim());
					cancellationPopup.hidePopUp();
				}else{
					form.showDialogMessage("Please enter valid remark to cancel order..!");
				}
			}else{
			
				if(cancellationPopup.getRemark().getValue().trim()!= null && !cancellationPopup.getRemark().getValue().trim().equals("")){
					//  react on purchase order cancel
					reactOnCancelContractButton(cancellationPopup.getRemark().getValue().trim());
					cancellationPopup.hidePopUp();
				}
				else{
					form.showDialogMessage("Please enter valid remark to cancel order..!");
				}
			}
		}
        if(event.getSource() == cancellationPopup.getLblCancel()){
        	cancellationPopup.hidePopUp();
			cancellationPopup.remark.setValue("");
		}
		
//		if (event.getSource() == cancelPopup.getBtnOk()) {
//			cancelpanel.hide();
//			checkForGRNStatus();
//		}
//
//		if (event.getSource() == cancelPopup.getBtnCancel()) {
//			cancelpanel.hide();
//			cancelPopup.remark.setValue("");
//		}
        /** Ends **/
        
		if (event.getSource() == summaryPopup.getBtnOk()) {
			summaryPanel.hide();
		}

		if (event.getSource() == summaryPopup.getBtnDownload()) {
			summaryPanel.hide();
			reactOnCancelSummaryDownload();
		}

	}

	/**
	 * Updated By: Viraj
	 * Date: 23-01-2019
	 * Description: To add cancellation popup with all the related documents
	 */
	private void reactOnCancelContractButton(final String remark) {
		final String loginUser = LoginPresenter.loggedInUser.trim();
		List<CancelContract> cancelContractList = cancellationPopup.getAllContractDocumentTable().getDataprovider().getList();
		ArrayList<CancelContract> selectedCancelContractList = new ArrayList<CancelContract>();
		for(CancelContract canContract : cancelContractList){
			if(canContract.isRecordSelect()){
				selectedCancelContractList.add(canContract);
			}
		}
		System.out.println("Selected list size :" + selectedCancelContractList.size());
		
		gasync.savePOCancellationData(model, remark, loginUser, selectedCancelContractList, new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
				form.showDialogMessage("Please Try again...!");
				
			}

			@Override
			public void onSuccess(Void result) {
				
				if(!model.getStatus().equals(PurchaseOrder.CANCELLED)) {
					model.setStatus(PurchaseOrder.CANCELLED);
					form.getTbStatus().setValue(PurchaseOrder.CANCELLED);
					form.tadescription.setValue(model.getDescription() + "\n" + "Purchase Order Id ="
					+ model.getCount() + " "
					+ "Purchase Order Status = "+model.getStatus()
					+ "\n"
					+ "has been cancelled by " + loginUser
					+ " with remark" + "\n" + "Remark ="
					+ remark);
					
					form.hideWaitSymbol();
					form.showDialogMessage("Purchase Order Cancelled Successfully..!");
					form.setToViewState();
			}
				
			}
		});
	}
	/** Ends **/
	/********************************************************** LOI ID QUERRY ******************************************************/

	public void setLOIData() {
		MyQuerry querry = new MyQuerry();
		Filter filter = new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(Integer.parseInt(form.getIbLetterOfIntentID()
				.getValue()));
		querry.getFilters().add(filter);
		querry.setQuerryObject(new LetterOfIntent());
		form.showWaitSymbol();
		async.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						if (result.size() != 0) {
							List<ProductDetailsPO> list = new ArrayList<ProductDetailsPO>();
							LetterOfIntent loi = (LetterOfIntent) result.get(0);
							if (loi.getStatus().equals("Approved")) {
								form.getDbLOIDate().setValue(
										loi.getCreationDate());
								form.getDbLOIDate().setEnabled(false);

								form.getVendortbl().getDataprovider().setList(loi.getVendorinfo());
								
//								form.getCbcformlis().setEnabled(false);
//								form.getOlbcstpercent().setEnabled(false);
								
								
								List<ProductDetails> list1 = loi.getProductinfo();

								for (ProductDetails temp : list1) {
									ProductDetailsPO po = new ProductDetailsPO();

									po.setProdDate(temp.getProdDate());
									po.setProductID(temp.getProductID());
									po.setProductName(temp.getProductName());
									po.setProductCode(temp.getProductCode());
									po.setProductCategory(temp
											.getProductCategory());
									po.setProdPrice(temp.getProdPrice());
									po.setUnitOfmeasurement(temp
											.getUnitOfmeasurement());
									po.setProductQuantity(1);
									po.setTax(temp.getServiceTax());
									po.setVat(temp.getVat());

									// ////////
									po.setPrduct(temp.getPrduct());
									po.setWarehouseName(temp.getWarehouseName());
									po.setAvailableStock(temp
											.getAvailableStock());

									list.add(po);
								}

								form.getProducttable().getDataprovider()
										.setList(list);

								form.getIbreqforQuotation().setValue(
										loi.getReqforQuotationID() + "");

								form.getDbRfqDate().setValue(loi.getRfqDate());
								form.getDbRfqDate().setEnabled(false);

								form.getIbRefOrderNo().setValue(
										loi.getRefOrderNO());
								if (form.getIbRefOrderNo().getValue() != null) {
									form.getIbRefOrderNo().setEnabled(false);
								}

								form.getPurchaseRequisiteNo().setValue(
										loi.getPurchaseReqNo() + "");
								form.getPrDate().setValue(loi.getPrDate());

								if (loi.getProject() != null) {
									form.getProject()
											.setValue(loi.getProject());
									form.getProject().setEnabled(false);
								}

								form.getBranch().setValue(loi.getBranch());
								if (form.getBranch().getValue() != null) {
									form.getBranch().setEnabled(false);
								}

								form.getOblEmployee().setValue(
										loi.getEmployee());
								if (form.getOblEmployee().getValue() != null) {
									form.getOblEmployee().setEnabled(false);
								}

								form.getTbApporverName().setValue(
										loi.getApproverName());
								if (form.getTbApporverName().getValue() != null) {
									form.getTbApporverName().setEnabled(false);
								}

								form.getCbadds().setValue(false);
								double total = form.getProducttable()
										.calculateTotal();
								form.getDbTotal().setValue(total);
								System.out.println("Total" + total);
								// setAllTable();

								/**
								 * Copying some more info of PR to PO Date :
								 * 12-09-2016 By Anil Release : 31 Aug 2016
								 * Project : PURCHASE ERROR SOLVING (NBHC)
								 */

								form.tbpoName.setValue(loi.getTitle());
								form.dbPODate.setValue(new Date());
								form.dbExpectedResposeDate.setValue(loi
										.getResponseDate());
								form.dbExpdeliverydate.setValue(loi
										.getDeliveryDate());
								// form.dbExpectedClosureDate.setValue(loi.getExpectedClosureDate());
								form.header.setValue(loi.getHeader());
								form.footer.setValue(loi.getFooter());

								form.olbAssignedTo1.setValue(loi.getAssignTo1());
								form.olbAssignedTo2.setValue(loi.getAssignTo2());

								// ///////////////
								List<String> prodCodeList = new ArrayList<String>();
								HashSet<String> hashList = new HashSet<String>();
								for (ProductDetails prod : loi.getProductinfo()) {
									hashList.add(prod.getProductCode());
								}
								prodCodeList.addAll(hashList);
								form.loadVendorProductPriceList(prodCodeList);

								form.prodInfoComposite.setEnable(false);
								form.addproducts.setEnabled(false);
								
								if(form.isWarehouseSelected()){
									form.getDeliveryadd().clear();
									form.getDeliveryadd().setEnable(false);
									form.getCbadds().setEnabled(false);
								}
								
								

								/**
								 * END
								 */

							} else {
								form.showDialogMessage("LOI is Not Approved");
								form.setToNewState();
								form.getProducttable().clear();
								form.getVendortbl().clear();
								form.getProducttable().connectToLocal();
								form.getProductchargestable().connectToLocal();

							}
						} else {
							form.showDialogMessage("No LOI found");
							form.setToNewState();
							form.getVendortbl().clear();
							form.getProducttable().clear();
							form.getProducttaxtable().connectToLocal();
							form.getProductchargestable().connectToLocal();

						}
					}

					@Override
					public void onFailure(Throwable caught) {

					}
				});
		form.hideWaitSymbol();
	}

	/******************************************************** RFQ ID QUERRY *********************************************************/

	public void setRfqData() {
		MyQuerry querry = new MyQuerry();
		Filter filter = new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(Integer.parseInt(form.getIbreqforQuotation()
				.getValue()));
		querry.getFilters().add(filter);
		querry.setQuerryObject(new RequsestForQuotation());
		form.showWaitSymbol();
		async.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						if (result.size() != 0) {
							RequsestForQuotation rfq = (RequsestForQuotation) result
									.get(0);
							if (rfq.getStatus().equals("Approved")) {
								form.getVendortbl().getDataprovider()
										.setList(rfq.getVendorinfo());
								form.getIbLetterOfIntentID().setValue(null);
								form.getDbLOIDate().setValue(null);
								List<ProductDetails> list = rfq
										.getProductinfo();
								List<ProductDetailsPO> list1 = new ArrayList<ProductDetailsPO>();
								for (ProductDetails temp : list) {

									ProductDetailsPO po = new ProductDetailsPO();
									po.setProdDate(temp.getProdDate());
									po.setProductID(temp.getProductID());
									po.setProductName(temp.getProductName());
									po.setProductCode(temp.getProductCode());
									po.setProductCategory(temp
											.getProductCategory());
									po.setProdPrice(temp.getProdPrice());
									po.setUnitOfmeasurement(temp
											.getUnitOfmeasurement());
									po.setProductQuantity(1);
									po.setTax(temp.getServiceTax());
									po.setVat(temp.getVat());

									// ////////
									po.setPrduct(temp.getPrduct());
									po.setWarehouseName(temp.getWarehouseName());
									po.setAvailableStock(temp
											.getAvailableStock());
									list1.add(po);
								}

								form.getProducttable().getDataprovider()
										.setList(list1);

								form.getDbRfqDate().setValue(
										rfq.getCreationDate());
								form.getDbRfqDate().setEnabled(false);
								form.getIbRefOrderNo().setValue(
										rfq.getRefOrderNO());
								if (form.getIbRefOrderNo().getValue() != null) {
									form.getIbRefOrderNo().setEnabled(true);
								}
								form.getPurchaseRequisiteNo().setValue(
										rfq.getPurchaseReqNo() + "");
								form.getPrDate().setValue(rfq.getPrDate());
								if (rfq.getProject() != null) {
									form.getProject()
											.setValue(rfq.getProject());
									form.getProject().setEnabled(false);
								}

								form.getBranch().setValue(rfq.getBranch());
								form.getBranch().setEnabled(false);

								form.getOblEmployee().setValue(
										rfq.getEmployee());
								if (form.getOblEmployee().getValue() != null) {
									form.getOblEmployee().setEnabled(false);
								}

								form.getTbApporverName().setValue(
										rfq.getApproverName());
								if (form.getTbApporverName().getValue() != null) {
									form.getTbApporverName().setEnabled(false);
								}
								
//								form.getCbcformlis().setEnabled(false);
//								form.getOlbcstpercent().setEnabled(false);
								
								form.getCbadds().setValue(false);
								double total = form.getProducttable().calculateTotal();
								form.getDbTotal().setValue(total);
								System.out.println("Total" + total);
								// setAllTable();

								/**
								 * Copying some more info of PR to PO Date :
								 * 12-09-2016 By Anil Release : 31 Aug 2016
								 * Project : PURCHASE ERROR SOLVING (NBHC)
								 */

								form.tbpoName.setValue(rfq.getQuotationName());
								form.dbPODate.setValue(new Date());
								form.dbExpectedResposeDate.setValue(rfq
										.getResponseDate());
								form.dbExpdeliverydate.setValue(rfq
										.getDeliveryDate());
								// form.dbExpectedClosureDate.setValue(rfq.getExpectedClosureDate());
								form.header.setValue(rfq.getHeader());
								form.footer.setValue(rfq.getFooter());

								form.olbAssignedTo1.setValue(rfq.getAssignTo1());
								form.olbAssignedTo2.setValue(rfq.getAssignTo2());

								// ///////////////
								List<String> prodCodeList = new ArrayList<String>();
								HashSet<String> hashList = new HashSet<String>();
								for (ProductDetails prod : rfq.getProductinfo()) {
									hashList.add(prod.getProductCode());
								}
								prodCodeList.addAll(hashList);
								form.loadVendorProductPriceList(prodCodeList);

								form.prodInfoComposite.setEnable(false);
								form.addproducts.setEnabled(false);
								
								if(form.isWarehouseSelected()){
									form.getDeliveryadd().clear();
									form.getDeliveryadd().setEnable(false);
									form.getCbadds().setEnabled(false);
								}

								/**
								 * END
								 */

							} else {
								form.showDialogMessage("RFQ is Not Approved");
								form.setToNewState();
								form.getProducttable().connectToLocal();
								form.getProducttaxtable().connectToLocal();
								form.getProductchargestable().connectToLocal();
								form.getVendortbl().connectToLocal();
							}
						} else {
							form.showDialogMessage("No RFQ found");
							form.setToNewState();
							form.getProducttable().connectToLocal();
							form.getProducttaxtable().connectToLocal();
							form.getProductchargestable().connectToLocal();
							form.getVendortbl().connectToLocal();
						}
					}

					@Override
					public void onFailure(Throwable caught) {

					}
				});
		form.hideWaitSymbol();
	}

	// ****************************************Set date From PR ID QUERRY
	// ************************************************/

	public void setPRData() {
		System.out.println("PR No"+form.getPurchaseRequisiteNo().getValue());
		try {
			Integer.parseInt(form.getPurchaseRequisiteNo().getValue());
		} catch (Exception e) {
			return;
		}
		MyQuerry querry = new MyQuerry();
		Filter filter = new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(Integer.parseInt(form.getPurchaseRequisiteNo()
				.getValue()));
		querry.getFilters().add(filter);
		querry.setQuerryObject(new PurchaseRequisition());
		form.showWaitSymbol();
		async.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						if (result.size() != 0) {
							final PurchaseRequisition pr = (PurchaseRequisition) result.get(0);
							
							
							/**
							 * Date : 04-03-2017 By Anil
							 * Checks for normal PR 
							 */
							if(AppUtility.getPrType(pr.getPrproduct())!=null){
								if(!AppUtility.getPrType(pr.getPrproduct()).equals("Normal PR")){
									form.showDialogMessage("Selected PR is not a Normal PR.");
									form.setToNewState();
									form.getProducttable().connectToLocal();
									form.getProducttaxtable().connectToLocal();
									form.getProductchargestable().connectToLocal();
									form.getVendortbl().connectToLocal();
									return;
								}
							}else{
								form.showDialogMessage("Product information not found in selected PR,Please check.");
								form.setToNewState();
								form.getProducttable().connectToLocal();
								form.getProducttaxtable().connectToLocal();
								form.getProductchargestable().connectToLocal();
								form.getVendortbl().connectToLocal();
								return;
							}

							if (pr.getStatus().equals("Approved")) {

//								form.getIbLetterOfIntentID().setValue(null);
//								form.getDbLOIDate().setValue(null);
//								form.getIbreqforQuotation().setValue(null);
//								form.getDbRfqDate().setValue(null);

//								List<ProductDetails> prProductList = pr.getPrproduct();
								ArrayList<ProductDetails> prProductList = PurchaseRequitionPresenter.getUniqueProductDetailsList(pr.getPrproduct());
								
								List<ProductDetailsPO> poProductList = new ArrayList<ProductDetailsPO>();
								for (ProductDetails temp : prProductList) {
									ProductDetailsPO po = new ProductDetailsPO();

									po.setProdDate(temp.getProdDate());
									po.setProductID(temp.getProductID());
									po.setProductName(temp.getProductName());
									po.setProductCode(temp.getProductCode());
									po.setProductCategory(temp.getProductCategory());
									po.setProdPrice(temp.getProdPrice());
									po.setUnitOfmeasurement(temp.getUnitOfmeasurement());
									po.setProductQuantity(temp.getProductQuantity());
									po.setTax(temp.getServiceTax());
									po.setVat(temp.getVat());

									// ////////
									po.setPrduct(temp.getPrduct());
									po.setWarehouseName(temp.getWarehouseName());
									po.setAvailableStock(temp.getAvailableStock());

									poProductList.add(po);
								}
								form.getProducttable().getDataprovider()
										.setList(poProductList);
								// form.getVendortbl().getDataprovider().setList(pr.getVendlist());

								form.getPrDate().setValue(pr.getCreationDate());

								form.getIbRefOrderNo().setValue(
										pr.getRefOrderNO());
								if (form.getIbRefOrderNo().getValue() != null) {
									form.getIbRefOrderNo().setEnabled(true);
								}
								if (pr.getProject() != null) {
									form.getProject().setValue(pr.getProject());
									form.getProject().setEnabled(false);
								}
								form.getBranch().setValue(pr.getBranch());
								if (form.getBranch().getValue() != null) {
									form.getBranch().setEnabled(false);
								}

								form.getOblEmployee()
										.setValue(pr.getEmployee());
								if (form.getOblEmployee().getValue() != null) {
									form.getOblEmployee().setEnabled(false);
								}

								form.getTbApporverName().setValue(
										pr.getApproverName());
								if (form.getTbApporverName().getValue() != null) {
									form.getTbApporverName().setEnabled(false);
								}
								
								
//								form.getCbcformlis().setEnabled(false);
//								form.getOlbcstpercent().setEnabled(false);
								
								
								form.getCbadds().setValue(false);
								double total = form.getProducttable()
										.calculateTotal();
								form.getDbTotal().setValue(total);
								System.out.println("Total" + total);
								// setAllTable();

								/**
								 * Copying some more info of PR to PO Date :
								 * 12-09-2016 By Anil Release : 31 Aug 2016
								 * Project : PURCHASE ERROR SOLVING (NBHC)
								 */

								form.tbpoName.setValue(pr.getPrTitle());
								form.dbPODate.setValue(new Date());
								form.dbExpectedResposeDate.setValue(pr
										.getResponseDate());
								form.dbExpdeliverydate.setValue(pr
										.getExpectedDeliveryDate());
								// form.dbExpectedClosureDate.setValue(pr.getExpectedClosureDate());
								form.header.setValue(pr.getHeader());
								form.footer.setValue(pr.getFooter());

								// List<String> prodCodeList=new
								// ArrayList<String>();
								// for(ProductDetails prod:pr.getPrproduct()){
								// prodCodeList.add(prod.getProductCode());
								// }
								
								
								/**
								 * If the process configuration is true the PR's assign to 1 is the purchase engg of PO
								 * in case assign to 1 is not selected and assign to 2 is selected then he will be purchase engg
								 * for po.
								 * Date: 14-10-2016 by Anil
								 * Release : 30 Sept 2016
								 * Project : PURCHASE MODIFICATION(NBHC)
								 */
								
								if(AppUtility.isPrAssignToPO("PurchaseRequisition")){
									if(!pr.getAssignTo1().equals("")){
										form.getOblEmployee().setValue(pr.getAssignTo1());
									}else if(!pr.getAssignTo2().equals("")){
										form.getOblEmployee().setValue(pr.getAssignTo2());
									}
								}
								/**
								 * End
								 */
								

								// ////
								form.olbAssignedTo1.setValue(pr.getAssignTo1());
								form.olbAssignedTo2.setValue(pr.getAssignTo2());

								// ///////////////
								List<String> prodCodeList = new ArrayList<String>();
								HashSet<String> hashList = new HashSet<String>();
								for (ProductDetails prod : pr.getPrproduct()) {
									hashList.add(prod.getProductCode());
								}
								prodCodeList.addAll(hashList);
								form.loadVendorProductPriceList(prodCodeList);
								
								if(form.isWarehouseSelected()){
									form.getDeliveryadd().clear();
									form.getDeliveryadd().setEnable(false);
									form.getCbadds().setEnabled(false);
								}

								form.prodInfoComposite.setEnable(false);
								form.addproducts.setEnabled(false);
								
								form.vendorTablePO.connectToLocal();

								// ///////////////

								/**
								 * END
								 */
							}

							else {
								form.showDialogMessage("PR No. is Not Approved");
								form.setToNewState();
								form.getProducttable().connectToLocal();
								form.getProducttaxtable().connectToLocal();
								form.getProductchargestable().connectToLocal();
								form.getVendortbl().connectToLocal();
							}
						} else {
							form.showDialogMessage("No PR No. found");
							form.setToNewState();
							form.getProducttable().connectToLocal();
							form.getProducttaxtable().connectToLocal();
							form.getProductchargestable().connectToLocal();
							form.getVendortbl().connectToLocal();
						}
					}

					@Override
					public void onFailure(Throwable caught) {

					}
				});
		form.hideWaitSymbol();
	}

	/**
	 * Set Default Address. This method retrieves the address for warehouse.
	 * When warehouse is selected the address of warehouse is retrieved.
	 */

//	public void setDefaultAdd() {
//		if (form.getOblwarehouse().getValue() != null) {
//			WareHouse wareh = form.getOblwarehouse().getSelectedItem();
//			PurchaseOrder view = new PurchaseOrder();
//			view.setAdress(wareh.getAddress());
//			form.getDeliveryadd().setValue(view.getAdress());
//			form.getDeliveryadd().setEnable(false);
//		}
//	}

	/**
	 * On row change event of product table.
	 */
	private void productsOnChange() {
		System.out.println("Inside Product Change");
		form.showWaitSymbol();
//		Timer timer = new Timer() {
//			@Override
//			public void run() {
				double totalExcludingTax = form.getProducttable().calculateTotalExcludingTax();
				totalExcludingTax = Double.parseDouble(nf.format(totalExcludingTax));
				form.getDbTotal().setValue(totalExcludingTax);

				System.out.println("DONE PROD CHANG 4");
				boolean chkSize = form.getProducttable().removeChargesOnDelete();
				if (chkSize == false) {
					form.getProductchargestable().connectToLocal();
					form.getTblOtherCharges().connectToLocal();//vijay
				}
				System.out.println("DONE PROD CHANG 3");
				try {
					form.producttaxtable.connectToLocal();
					form.addProdTaxes();
					/**
					 *  Date 26-11-2018 By Vijay 
					 *  Des :- As per the Other Charges with Taxes Code updated
					 *  and old code commented
					 */
//					double totalIncludingTax = form.getDbTotal().getValue()+ form.getProducttaxtable().calculateTotalTaxes();
//					form.getDoamtincltax().setValue(Double.parseDouble(nf.format(totalIncludingTax)));
//					form.updateChargesTable();
					
					form.addOtherChargesInTaxTbl();
					form.getDoamtincltax().setValue(form.tblOtherCharges.calculateOtherChargesSum());
					/**
					 * ends here
					 */

				} catch (Exception e) {
					e.printStackTrace();
				}
				System.out.println("DONE PROD CHANG 2");
				/**
				 *  Date 26-11-2018 By Vijay 
				 *  Des :- As per the Other Charges with Taxes Code updated
				 *  and old code commented
				 */  
//				double netPay = fillNetPayable(form.getDoamtincltax().getValue());
				double totalIncludingTax = form.getDbTotal().getValue()+ form.getProducttaxtable().calculateTotalTaxes()+form.tblOtherCharges.calculateOtherChargesSum();
				double netPay = fillNetPayable(totalIncludingTax);
				/**
				 * ends here
				 */
				double amtval = 0;
				if (form.getProductchargestable().getDataprovider().getList().size() == 0) {
					amtval = totalIncludingTax;
				}
				if (form.getProductchargestable().getDataprovider().getList().size() != 0) {
					amtval = totalIncludingTax+ form.getProductchargestable().calculateNetPayable();
				}
				form.getDbfinalTotalAmtWithoutRoundoff().setValue(amtval);//Ashwini Patil Date:27-05-2023
				netPay = Math.round(netPay);
				int netPayable = (int) netPay;
				netPay = netPayable;
				
				/**
				 * Date 08/06/2018
				 * Developer :- Vijay
				 * Des :- for final total amt Round off amt calculation
				 */
				form.getDbfinalTotalAmt().setValue(Double.parseDouble(nf.format(netPay)));
				
				if(form.getTbroundoffAmt().getValue()!=null && !form.getTbroundoffAmt().getValue().equals("")){
					String roundOff = form.getTbroundoffAmt().getValue();
					double roundoffAmt =  AppUtility.calculateRoundOff(roundOff, netPay);
					System.out.println("roundoffAmt =="+roundoffAmt);
					if(roundoffAmt!=0){
						form.getDonetpayamt().setValue(roundoffAmt);
					}else{
						form.getDonetpayamt().setValue(Double.parseDouble(nf.format(netPay)));
						form.getTbroundoffAmt().setValue("");
					}
				}else{
					form.getDonetpayamt().setValue(Double.parseDouble(nf.format(netPay)));

				}
				/**
				 * ends here
				 */
				
				form.getDonetpayamt().setValue(Double.parseDouble(nf.format(netPay)));
				System.out.println("DONE PROD CHANG 1");
				String approverName = "";
				if (form.tbApporverName.getValue() != null) {
					approverName = form.tbApporverName.getValue();
				}
				AppUtility.getApproverListAsPerTotalAmount(form.tbApporverName,"Purchase Order", approverName, form.getDonetpayamt().getValue());
				form.hideWaitSymbol();
				System.out.println("DONE PROD CHANG");
//			}
//		};
//		timer.schedule(3000);
	}

	private void productsOnChangeStateNotSelected() {
		System.out.println("Inside Product Change State Not Selected");
		form.showWaitSymbol();
		Timer timer = new Timer() {
			@Override
			public void run() {
				double totalExcludingTax = form.getProducttable()
						.calculateTotalExcludingTax();
				totalExcludingTax = Double.parseDouble(nf
						.format(totalExcludingTax));
				form.getDbTotal().setValue(totalExcludingTax);

				boolean chkSize = form.getProducttable()
						.removeChargesOnDelete();
				if (chkSize == false) {
					form.getProductchargestable().connectToLocal();
					// form.getVendortbl().connectToLocal();
					// PurchaseOrderForm.povendors.clear();
				}

				try {
					form.producttaxtable.connectToLocal();
					form.addProdTaxesNoState();
					double totalIncludingTax = form.getDbTotal().getValue()
							+ form.getProducttaxtable().calculateTotalTaxes();
					form.getDoamtincltax().setValue(
							Double.parseDouble(nf.format(totalIncludingTax)));
					form.updateChargesTable();

				} catch (Exception e) {
					e.printStackTrace();
				}

				double netPay = fillNetPayable(form.getDoamtincltax()
						.getValue());
				netPay = Math.round(netPay);
				int netPayable = (int) netPay;
				netPay = netPayable;
				form.getDonetpayamt().setValue(
						Double.parseDouble(nf.format(netPay)));
				form.hideWaitSymbol();
			}
		};
		timer.schedule(3000);

	}

	private double fillNetPayable(double amtincltax) {
		double amtval = 0;
		if (form.getProductchargestable().getDataprovider().getList().size() == 0) {
			amtval = amtincltax;
		}
		if (form.getProductchargestable().getDataprovider().getList().size() != 0) {
			amtval = amtincltax
					+ form.getProductchargestable().calculateNetPayable();
		}
		amtval = Math.round(amtval);
		int retAmtVal = (int) amtval;
		amtval = retAmtVal;
		return amtval;
	}

	/**
	 * This method returns the name of state in which the company is located. It
	 * is retrieved form Company address in Company Entity
	 * 
	 * @return It returns company state
	 */
	// ********************************** Retrieve Company State
	// *********************************************
	public String retrieveCompanyState() {
		final MyQuerry querry = new MyQuerry();
		Filter tempfilter = new Filter();
		tempfilter.setQuerryString("companyId");
		tempfilter.setLongValue(model.getCompanyId());
		querry.getFilters().add(tempfilter);
		querry.setQuerryObject(new Company());
		Timer timer = new Timer() {
			@Override
			public void run() {
				async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onFailure(Throwable caught) {
						form.showDialogMessage("An Unexpected error occurred!");
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						for (SuperModel model : result) {
							Company compentity = (Company) model;
							retrCompnyState = compentity.getAddress().getState();
						}
						companyState = retrCompnyState.trim();
						System.out.println("sysouStatc" + companyState);
					}
				});
			}
		};
		timer.schedule(3000);
		return retrCompnyState;
	}

	// **************************** Retrieve Vendor State
	// ************************************

	public String retVendorState(final int venCount) {

		MyQuerry querry = new MyQuerry();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter tempfilter = null;

		tempfilter = new Filter();
		tempfilter.setQuerryString("companyId");
		tempfilter.setLongValue(model.getCompanyId());
		filtervec.add(tempfilter);

		tempfilter = new Filter();
		tempfilter.setQuerryString("count");
		tempfilter.setIntValue(venCount);
		filtervec.add(tempfilter);

		querry.getFilters().add(tempfilter);
		querry.setQuerryObject(new Vendor());
		form.showWaitSymbol();

		async.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onFailure(Throwable caught) {
						form.hideWaitSymbol();
						form.showDialogMessage("An Unexpected error occurred!");
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						form.hideWaitSymbol();
						for (SuperModel model : result) {
							Vendor vendorentity = (Vendor) model;
							retVenStateName = vendorentity.getPrimaryAddress().getState();
							vendorState = retVenStateName.trim();
							/** date 10.12.2018 added by komal**/
							form.ibcreditdays.setValue(vendorentity.getCreditDays());
							/** end komal **/
							/** date 06.12.2018 added by komal**/
							if(vendorentity.getCreditDaysComment() != null){
								form.tbCreditDaysComment.setValue(vendorentity.getCreditDaysComment());
							}
							/** end komal **/
							
							List<ProductDetailsPO> list = form.productTablePO.getDataprovider().getList();
							Branch branchEntity = form.branch.getSelectedItem();
							System.out.println("retVenStateName "+retVenStateName);
							updateTaxesDetails(list,branchEntity,retVenStateName);
							
							if(form.isWarehouseSelected()){
								updateWarehouseAndCformStatus();
								setVendorPriceToProduct(form.productTablePO.getValue(), venCount);
								productsOnChange();
							}else{

//								if (!vendorState.equals("")&& form.getDeliveryadd().getState().getSelectedIndex() != 0
//										&& !vendorState.trim().equals(form.getDeliveryadd().getState().getValue().trim())) {
//									//Date 4 -07-2017 added by vijay 
//									if(form.dbPODate.getValue()!=null && form.dbPODate.getValue().before(form.date)){
//										form.getCbcformlis().setEnabled(true);
//										form.validateCForm();
//									}
//								} else {
//									form.getCbcformlis().setSelectedIndex(0);
//									form.getOlbcstpercent().setSelectedIndex(0);
//									form.getCbcformlis().setEnabled(false);
//									form.getOlbcstpercent().setEnabled(false);
//								}
	
								setVendorPriceToProduct(form.productTablePO.getValue(), venCount);
								productsOnChange();
							}
						}
					}

				});
		return retVenStateName;
	}

	// *********************************** Retrieve Vendor Count
	// *************************************
	public int retrieveVendorCount() {
		List<VendorDetails> vendorLis = form.getVendortbl().getDataprovider().getList();
		int vendorCount = 0;
		for (int i = 0; i < vendorLis.size(); i++) {
			if (vendorLis.get(i).getStatus() == true) {
				vendorCount = vendorLis.get(i).getVendorId();
				return vendorCount;
			}
		}
		return vendorCount;
	}

	// ******************************** Check Vendor State
	// *******************************************

	public boolean checkVendorState() {
		System.out.println("Vendor State" + vendorState);

		String addCompState = "";
		if (form.getDeliveryadd().getState().getSelectedIndex() != 0) {
			int indexVal = form.getDeliveryadd().getState().getSelectedIndex();
			addCompState = form.getDeliveryadd().getState()
					.getItemText(indexVal);
			System.out.println("Vendor State" + addCompState);
		}
		if (!addCompState.equals("")
				&& addCompState.trim().equals(vendorState.trim())
				&& !vendorState.equals("")) {
			return true;
		}
		return false;
	}

	// ******************************** Retrieving Product Price List
	// ************************************

	private List<String> retriveProductPriceList() {
		final List<String> list = new ArrayList<String>();

		MyQuerry querry = new MyQuerry();
		Filter filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(model.getCompanyId());
		querry.getFilters().add(filter);
		querry.setQuerryObject(new PriceList());
		async.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						if (result.size() != 0) {
							for (SuperModel m : result) {
								PriceList pr = (PriceList) m;
								list.add(pr.getProdCode());
							}
							System.out.println("List Size Inside Serach Qur:"
									+ list.size());
						}
					}

					@Override
					public void onFailure(Throwable caught) {
					}
				});

		return list;
	}

	private void getSummaryFromPurchaseOrderID() {

		MyQuerry querry = new MyQuerry();
		Company c = new Company();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;

		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);

		filter = new Filter();
		filter.setQuerryString("docID");
		filter.setIntValue(Integer.parseInt(form.getIbPOId().getValue().trim()));
		filtervec.add(filter);

		filter = new Filter();
		filter.setQuerryString("docType");
		filter.setStringValue(AppConstants.APPROVALPURCHASEORDER);
		filtervec.add(filter);

		querry.setFilters(filtervec);
		querry.setQuerryObject(new CancelSummary());
		async.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {

					@Override
					public void onFailure(Throwable caught) {
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						for (SuperModel smodel : result) {
							CancelSummary cancelEntity = (CancelSummary) smodel;
							summaryPanel = new PopupPanel(true);
							summaryPanel.add(summaryPopup);
							summaryPanel.show();
							summaryPanel.center();
							summaryPopup.setSummaryTable(cancelEntity
									.getCancelLis());
						}
					}
				});
	}

	private void reactOnCancelSummaryDownload() {
		System.out.println("inside download");

		ArrayList<CancelSummary> cancelsummarylist = new ArrayList<CancelSummary>();
		System.out.println("cancel summary list size"
				+ cancelsummarylist.size());
		List<CancellationSummary> cancel = summaryPopup.getSummaryTable()
				.getDataprovider().getList();
		System.out.println("cancel list size" + cancel.size());
		CancelSummary cancelEntity = new CancelSummary();
		cancelEntity.setCancelLis(cancel);

		cancelsummarylist.add(cancelEntity);
		System.out.println("cancel summary list size after......."
				+ cancelsummarylist.size());
		csvservice.setCancelSummaryDetails(cancelsummarylist,
				new AsyncCallback<Void>() {

					@Override
					public void onFailure(Throwable caught) {
						form.showDialogMessage("An unexpected error occurred");
					}

					@Override
					public void onSuccess(Void result) {
						String gwt = com.google.gwt.core.client.GWT
								.getModuleBaseURL();
						final String url = gwt + "csvservlet" + "?type=" + 82;
						Window.open(url, "test", "enabled");
					}
				});

	}

	private void checkForGRNStatus() {

		Console.log("Inside check  grn status");
		final int purchaseorderID = model.getCount();
		final String purchaseorderStatus = model.getStatus();

		MyQuerry querry = new MyQuerry();
		Company c = new Company();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("poNo");
		filter.setIntValue(purchaseorderID);
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new GRN());

		async.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {

					@Override
					public void onFailure(Throwable caught) {
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						int pic = 0;
						int request = 0;
						int inspection = 0;
						int message = 0;
						ArrayList<GRN> slist = new ArrayList<GRN>();
						/**
						 * Updated By: Viraj
						 * Date: 22-01-2019
						 * Description: ArrayList to add grnlist
						 */
						final ArrayList<GRN> grnList = new ArrayList<GRN>();
						/** Ends **/

						for (SuperModel model : result) {
							GRN grnEntity = (GRN) model;
							grnEntity.setStatus(grnEntity.getStatus());
							grnEntity.setCount(grnEntity.getCount());
							slist.add(grnEntity);
							
							/**
							 * Updated By: Viraj
							 * Date: 22-01-2019
							 * Description: To add grn list which are other than cancelled and rejected
							 */
							if(!grnEntity.getStatus().equalsIgnoreCase(GRN.CANCELLED)
									&& !grnEntity.getStatus().equalsIgnoreCase(GRN.REJECTED)) {
								grnList.add(grnEntity);
							}
							/** Ends **/
							
//							CancellationSummary table1 = new CancellationSummary();
//							table1.setDocID(grnEntity.getCount());
//							table1.setDocType(AppConstants.APPROVALGRN);
//							table1.setRemark(cancelPopup.getRemark().getValue()
//									.trim());
//							table1.setLoggedInUser(LoginPresenter.loggedInUser);
//							table1.setOldStatus(grnEntity.getStatus());
//							table1.setNewStatus(BillingDocument.CANCELLED);
//							table1.setSrNo(1);
//
//							cancellis.add(table1);
						}
						/**
						 * Updated By: Viraj
						 * Date: 14-02-2019
						 * Description: To add GRN Cancellation in PO
						 */
//						for (int i = 0; i < slist.size(); i++) {
//
//							if (slist.get(i).getStatus().equals(GRN.APPROVED)) {
//								message = message + 1;
//							} else if (slist.get(i).getStatus()
//									.equals(GRN.CREATED)
//									|| slist.get(i).getStatus()
//											.equals(GRN.REJECTED)) {
//								pic = pic + 1;
//							} else if (slist.get(i).getStatus()
//									.equals(GRN.REQUESTED)) {
//								request = request + 1;
//							} else if (slist.get(i).getStatus()
//									.equals(GRN.INSPECTIONAPPROVED)
//									|| slist.get(i).getStatus()
//											.equals(GRN.INSPECTIONREQUESTED)) {
//								inspection = inspection + 1;
//							}
//
//						}

//						if (message > 0) {
//							form.showDialogMessage("GRN status is approved. Cancel GRN first");
//						} else {
							/**
							 * Update By: Viraj
							 * Date: 22-01-2019
							 * Description: to show all documents related to the purchase order in cancellation popup
							 */
							Company c = new Company();
							
								gasync.getCancellingDocumentPurchaseOrder(c.getCompanyId(), model.getCount(), new AsyncCallback<ArrayList<CancelContract>>() {

									@Override
									public void onFailure(Throwable caught) {
										System.out.println("cancellation document error: "+ caught);
									}

									@Override
									public void onSuccess(ArrayList<CancelContract> result) {
										cancellationPopup.getAllContractDocumentTable().getDataprovider().getList().clear();
										
										if(!model.getStatus().equalsIgnoreCase(PurchaseOrder.CANCELLED)) {
											CancelContract canContract = new CancelContract();
											canContract.setDocumnetId(model.getCount());
						            		canContract.setBranch(model.getBranch());
						            		if(model.getVendorDetails() != null) {
							            		for(int i=0;i<model.getVendorDetails().size();i++) {
							            			if(model.getVendorDetails().get(i).getStatus()) {
							            				canContract.setBpName(model.getVendorDetails().get(i).getVendorName());
							            			}
							            		}
						            		}
						            		canContract.setDocumentStatus(model.getStatus());
						            		canContract.setDocumnetName(AppConstants.PURCHASEORDER);
						            		canContract.setRecordSelect(true);
						            		cancellationPopup.getAllContractDocumentTable().getDataprovider().getList().add(canContract);
										}
										
											if(grnList.size() != 0) {
												for(int i=0; i<grnList.size(); i++) {
													CancelContract canContract = new CancelContract();
													canContract.setDocumnetId(grnList.get(i).getCount());
								            		canContract.setBranch(grnList.get(i).getBranch());
								            		canContract.setBpName(grnList.get(i).getVendorInfo().getFullName());
								            		canContract.setDocumentStatus(grnList.get(i).getStatus());
								            		canContract.setDocumnetName(AppConstants.GRN);
								            		canContract.setRecordSelect(true);
								            		cancellationPopup.getAllContractDocumentTable().getDataprovider().getList().add(canContract);
												}
											}
											
											cancellationPopup.getAllContractDocumentTable().getDataprovider().getList().addAll(result);
											cancellationPopup.getAllContractDocumentTable().getTable().redraw();
											cancellationPopup.showPopUp();
									}
										
								});
										
							/** Ends **/
//							System.out.println("Else SYSO");
//
//							if ((pic > 0) || (request > 0) || (inspection > 0)) {
//
//								if (inspection > 0) {
//									for (int i = 0; i < inspection; i++) {
//										MyQuerry querry = new MyQuerry();
//										Company c = new Company();
//										Vector<Filter> filtervec = new Vector<Filter>();
//										Filter filter = null;
//										filter = new Filter();
//										filter.setQuerryString("companyId");
//										filter.setLongValue(c.getCompanyId());
//										filtervec.add(filter);
//										filter = new Filter();
//										filter.setQuerryString("inspectionGrnId");
//										filter.setIntValue(slist.get(i)
//												.getCount());
//										filtervec.add(filter);
//										querry.setFilters(filtervec);
//										querry.setQuerryObject(new Inspection());
//										async.getSearchResult(
//												querry,
//												new AsyncCallback<ArrayList<SuperModel>>() {
//
//													@Override
//													public void onFailure(
//															Throwable caught) {
//													}
//
//													@Override
//													public void onSuccess(
//															ArrayList<SuperModel> result) {
//
//														for (SuperModel smodel : result) {
//															Inspection inspectionEntity = (Inspection) smodel;
//
//															CancellationSummary table1 = new CancellationSummary();
//															table1.setDocID(inspectionEntity
//																	.getCount());
//															table1.setDocType(AppConstants.APPROVALINSPECTION);
//															table1.setRemark(cancelPopup
//																	.getRemark()
//																	.getValue()
//																	.trim());
//															table1.setLoggedInUser(LoginPresenter.loggedInUser);
//															table1.setOldStatus(inspectionEntity
//																	.getStatus());
//															table1.setNewStatus(Inspection.CANCELLED);
//															table1.setSrNo(1);
//
//															cancellis
//																	.add(table1);
//
//															if (inspectionEntity
//																	.getStatus()
//																	.trim()
//																	.equals(Inspection.CREATED)) {
//
//																inspectionEntity
//																		.setStatus(Inspection.CANCELLED);
//																inspectionEntity
//																		.setInspectionDescription("Purchase Order ID ="
//																				+ purchaseorderID
//																				+ " "
//																				+ "purchase order status ="
//																				+ purchaseorderStatus
//																				+ "\n"
//																				+ "has been cancelled by "
//																				+ LoginPresenter.loggedInUser
//																				+ " with remark "
//																				+ "\n"
//																				+ "Remark ="
//																				+ cancelPopup
//																						.getRemark()
//																						.getValue()
//																						.trim());
//
//																async.save(
//																		inspectionEntity,
//																		new AsyncCallback<ReturnFromServer>() {
//
//																			@Override
//																			public void onFailure(
//																					Throwable caught) {
//																				form.showDialogMessage("An Unexpected Error occured !");
//																			}
//
//																			@Override
//																			public void onSuccess(
//																					ReturnFromServer result) {
//																			}
//																		});
//															}
//
//															if (inspectionEntity
//																	.getStatus()
//																	.trim()
//																	.equals(Inspection.REJECTED)) {
//
//																inspectionEntity
//																		.setStatus(Inspection.CANCELLED);
//																inspectionEntity
//																		.setInspectionDescription("Purchase Order ID ="
//																				+ purchaseorderID
//																				+ " "
//																				+ "purchase order status ="
//																				+ purchaseorderStatus
//																				+ "\n"
//																				+ "has been cancelled by "
//																				+ LoginPresenter.loggedInUser
//																				+ " with remark "
//																				+ "\n"
//																				+ "Remark ="
//																				+ cancelPopup
//																						.getRemark()
//																						.getValue()
//																						.trim());
//
//																async.save(
//																		inspectionEntity,
//																		new AsyncCallback<ReturnFromServer>() {
//
//																			@Override
//																			public void onFailure(
//																					Throwable caught) {
//																				form.showDialogMessage("An Unexpected Error occured !");
//																			}
//
//																			@Override
//																			public void onSuccess(
//																					ReturnFromServer result) {
//																			}
//																		});
//															}
//
//															if (inspectionEntity
//																	.getStatus()
//																	.trim()
//																	.equals(Inspection.APPROVED)) {
//
//																inspectionEntity
//																		.setStatus(Inspection.CANCELLED);
//																inspectionEntity
//																		.setInspectionDescription("Purchase Order ID ="
//																				+ purchaseorderID
//																				+ " "
//																				+ "purchase order status ="
//																				+ purchaseorderStatus
//																				+ "\n"
//																				+ "has been cancelled by "
//																				+ LoginPresenter.loggedInUser
//																				+ " with remark "
//																				+ "\n"
//																				+ "Remark ="
//																				+ cancelPopup
//																						.getRemark()
//																						.getValue()
//																						.trim());
//
//																async.save(
//																		inspectionEntity,
//																		new AsyncCallback<ReturnFromServer>() {
//
//																			@Override
//																			public void onFailure(
//																					Throwable caught) {
//																				form.showDialogMessage("An Unexpected Error occured !");
//																			}
//
//																			@Override
//																			public void onSuccess(
//																					ReturnFromServer result) {
//																			}
//																		});
//															}
//
//															if (inspectionEntity
//																	.getStatus()
//																	.trim()
//																	.equals(Inspection.REQUESTED)) {
//
//																inspectionEntity
//																		.setStatus(Inspection.CANCELLED);
//																inspectionEntity
//																		.setInspectionDescription("Purchase Order ID ="
//																				+ purchaseorderID
//																				+ " "
//																				+ "purchase order status ="
//																				+ purchaseorderStatus
//																				+ "\n"
//																				+ "has been cancelled by "
//																				+ LoginPresenter.loggedInUser
//																				+ " with remark "
//																				+ "\n"
//																				+ "Remark ="
//																				+ cancelPopup
//																						.getRemark()
//																						.getValue()
//																						.trim());
//
//																async.save(
//																		inspectionEntity,
//																		new AsyncCallback<ReturnFromServer>() {
//
//																			@Override
//																			public void onFailure(
//																					Throwable caught) {
//																				form.showDialogMessage("An Unexpected Error occured !");
//																			}
//
//																			@Override
//																			public void onSuccess(
//																					ReturnFromServer result) {
//																			}
//																		});
//
//																MyQuerry querry = new MyQuerry();
//																Company c = new Company();
//																Vector<Filter> filtervec = new Vector<Filter>();
//																Filter filter = null;
//																filter = new Filter();
//																filter.setQuerryString("companyId");
//																filter.setLongValue(c
//																		.getCompanyId());
//																filtervec
//																		.add(filter);
//																filter = new Filter();
//																filter.setQuerryString("businessprocessId");
//																filter.setIntValue(inspectionEntity
//																		.getCount());
//																filtervec
//																		.add(filter);
//																filter = new Filter();
//																filter.setQuerryString("businessprocesstype");
//																filter.setStringValue(ApproverFactory.INSPECTION);
//																filtervec
//																		.add(filter);
//																querry.setFilters(filtervec);
//																querry.setQuerryObject(new Approvals());
//																async.getSearchResult(
//																		querry,
//																		new AsyncCallback<ArrayList<SuperModel>>() {
//
//																			@Override
//																			public void onFailure(
//																					Throwable caught) {
//																			}
//
//																			@Override
//																			public void onSuccess(
//																					ArrayList<SuperModel> result) {
//
//																				for (SuperModel smodel : result) {
//																					Approvals approval = (Approvals) smodel;
//
//																					approval.setStatus(Approvals.CANCELLED);
//																					approval.setRemark("purchase order Id ="
//																							+ purchaseorderID
//																							+ " "
//																							+ "purchase order status ="
//																							+ purchaseorderStatus
//																							+ "\n"
//																							+ "has been cancelled by "
//																							+ LoginPresenter.loggedInUser
//																							+ " with remark "
//																							+ "\n"
//																							+ "Remark ="
//																							+ cancelPopup
//																									.getRemark()
//																									.getValue()
//																									.trim());
//
//																					async.save(
//																							approval,
//																							new AsyncCallback<ReturnFromServer>() {
//
//																								@Override
//																								public void onFailure(
//																										Throwable caught) {
//																									form.showDialogMessage("An Unexpected Error occured !");
//																								}
//
//																								@Override
//																								public void onSuccess(
//																										ReturnFromServer result) {
//																								}
//																							});
//																				}
//																			}
//																		});
//
//															}
//
//														}
//													}
//												});
//									}
//								}
//
//								if (request > 0) {
//									for (int i = 0; i < request; i++) {
//
//										MyQuerry querry = new MyQuerry();
//										Company c = new Company();
//										Vector<Filter> filtervec = new Vector<Filter>();
//										Filter filter = null;
//										filter = new Filter();
//										filter.setQuerryString("companyId");
//										filter.setLongValue(c.getCompanyId());
//										filtervec.add(filter);
//										filter = new Filter();
//										filter.setQuerryString("businessprocessId");
//										filter.setIntValue(slist.get(i)
//												.getCount());
//										filtervec.add(filter);
//										filter = new Filter();
//										filter.setQuerryString("businessprocesstype");
//										filter.setStringValue(ApproverFactory.GRN);
//										filtervec.add(filter);
//										querry.setFilters(filtervec);
//										querry.setQuerryObject(new Approvals());
//										async.getSearchResult(
//												querry,
//												new AsyncCallback<ArrayList<SuperModel>>() {
//
//													@Override
//													public void onFailure(
//															Throwable caught) {
//													}
//
//													@Override
//													public void onSuccess(
//															ArrayList<SuperModel> result) {
//
//														for (SuperModel smodel : result) {
//															Approvals approval = (Approvals) smodel;
//
//															approval.setStatus(Approvals.CANCELLED);
//															approval.setRemark("purchase order Id ="
//																	+ purchaseorderID
//																	+ " "
//																	+ "purchase order status ="
//																	+ purchaseorderStatus
//																	+ "\n"
//																	+ "has been cancelled by "
//																	+ LoginPresenter.loggedInUser
//																	+ " with remark "
//																	+ "\n"
//																	+ "Remark ="
//																	+ cancelPopup
//																			.getRemark()
//																			.getValue()
//																			.trim());
//
//															async.save(
//																	approval,
//																	new AsyncCallback<ReturnFromServer>() {
//
//																		@Override
//																		public void onFailure(
//																				Throwable caught) {
//																			form.showDialogMessage("An Unexpected Error occured !");
//																		}
//
//																		@Override
//																		public void onSuccess(
//																				ReturnFromServer result) {
//																		}
//																	});
//														}
//													}
//												});
//									}
//								}
//
//								MyQuerry querry = new MyQuerry();
//								Company c = new Company();
//								Vector<Filter> filtervec = new Vector<Filter>();
//								Filter filter = null;
//								filter = new Filter();
//								filter.setQuerryString("companyId");
//								filter.setLongValue(c.getCompanyId());
//								filtervec.add(filter);
//								filter = new Filter();
//								filter.setQuerryString("contractCount");
//								filter.setIntValue(purchaseorderID);
//								filtervec.add(filter);
//								filter = new Filter();
//								filter.setQuerryString("typeOfOrder");
//								filter.setStringValue(AppConstants.ORDERTYPEFORPURCHASE);
//								filtervec.add(filter);
//								querry.setFilters(filtervec);
//								querry.setQuerryObject(new BillingDocument());
//
//								async.getSearchResult(
//										querry,
//										new AsyncCallback<ArrayList<SuperModel>>() {
//
//											@Override
//											public void onFailure(
//													Throwable caught) {
//											}
//
//											@Override
//											public void onSuccess(
//													ArrayList<SuperModel> result) {
//												int req = 0;
//												ArrayList<BillingDocument> slist = new ArrayList<BillingDocument>();
//
//												for (SuperModel model : result) {
//													BillingDocument billingEntity = (BillingDocument) model;
//													billingEntity
//															.setStatus(billingEntity
//																	.getStatus());
//													slist.add(billingEntity);
//
//													if (billingEntity
//															.getStatus()
//															.equals(BillingDocument.BILLINGINVOICED)) {
//														CancellationSummary table1 = new CancellationSummary();
//														table1.setDocID(billingEntity
//																.getCount());
//														table1.setDocType(AppConstants.APPROVALBILLINGDETAILS);
//														table1.setRemark(cancelPopup
//																.getRemark()
//																.getValue()
//																.trim());
//														table1.setLoggedInUser(LoginPresenter.loggedInUser);
//														table1.setOldStatus(billingEntity
//																.getStatus());
//														table1.setNewStatus(BillingDocument.BILLINGINVOICED);
//														table1.setSrNo(1);
//
//														cancellis.add(table1);
//													} else {
//														CancellationSummary table1 = new CancellationSummary();
//														table1.setDocID(billingEntity
//																.getCount());
//														table1.setDocType(AppConstants.APPROVALBILLINGDETAILS);
//														table1.setRemark(cancelPopup
//																.getRemark()
//																.getValue()
//																.trim());
//														table1.setLoggedInUser(LoginPresenter.loggedInUser);
//														table1.setOldStatus(billingEntity
//																.getStatus());
//														table1.setNewStatus(BillingDocument.CANCELLED);
//														table1.setSrNo(1);
//
//														cancellis.add(table1);
//													}
//
//												}
//
//												int cnt = 0;
//												int flag = 0;
//												for (int i = 0; i < slist
//														.size(); i++) {
//
//													if (slist
//															.get(i)
//															.getStatus()
//															.equals(BillingDocument.APPROVED)) {
//														flag = flag + 1;
//													} else if (slist
//															.get(i)
//															.getStatus()
//															.equals(BillingDocument.BILLINGINVOICED)) {
//														cnt = cnt + 1;
//
//													} else if (slist
//															.get(i)
//															.getStatus()
//															.equals(BillingDocument.CREATED)) {
//														flag = flag + 1;
//													} else if (slist
//															.get(i)
//															.getStatus()
//															.equals(BillingDocument.REJECTED)) {
//														flag = flag + 1;
//													} else if (slist
//															.get(i)
//															.getStatus()
//															.equals(BillingDocument.REQUESTED)) {
//														req = req + 1;
//													}
//
//												}
//												if (cnt > 0) {
//
//													MyQuerry querry = new MyQuerry();
//													Company c = new Company();
//													Vector<Filter> filtervec = new Vector<Filter>();
//													Filter filter = null;
//													filter = new Filter();
//													filter.setQuerryString("companyId");
//													filter.setLongValue(c
//															.getCompanyId());
//													filtervec.add(filter);
//													filter = new Filter();
//													filter.setQuerryString("contractCount");
//													filter.setIntValue(purchaseorderID);
//													filtervec.add(filter);
//													filter = new Filter();
//													filter.setQuerryString("typeOfOrder");
//													filter.setStringValue(AppConstants.ORDERTYPEFORPURCHASE);
//													filtervec.add(filter);
//													querry.setFilters(filtervec);
//													querry.setQuerryObject(new Invoice());
//													async.getSearchResult(
//															querry,
//															new AsyncCallback<ArrayList<SuperModel>>() {
//
//																@Override
//																public void onFailure(
//																		Throwable caught) {
//																}
//
//																@Override
//																public void onSuccess(
//																		ArrayList<SuperModel> result) {
//																	int cnt = 0;
//																	int requested = 0;
//																	ArrayList<Invoice> slist = new ArrayList<Invoice>();
//
//																	for (SuperModel model : result) {
//																		Invoice invoiceEntity = (Invoice) model;
//
//																		invoiceEntity
//																				.setStatus(invoiceEntity
//																						.getStatus());
//																		invoiceEntity
//																				.setContractCount(invoiceEntity
//																						.getContractCount());
//																		invoiceEntity
//																				.setInvoiceCount(invoiceEntity
//																						.getCount());
//																		invoiceEntity
//																				.setStatus(invoiceEntity
//																						.getStatus());
//																		slist.add(invoiceEntity);
//
//																		if (invoiceEntity
//																				.getStatus()
//																				.equals(Invoice.APPROVED)) {
//																			CancellationSummary table2 = new CancellationSummary();
//
//																			table2.setDocID(invoiceEntity
//																					.getCount());
//																			table2.setDocType(AppConstants.APPROVALINVOICEDETAILS);
//																			table2.setRemark(cancelPopup
//																					.getRemark()
//																					.getValue()
//																					.trim());
//																			table2.setLoggedInUser(LoginPresenter.loggedInUser);
//																			table2.setOldStatus(invoiceEntity
//																					.getStatus());
//																			table2.setNewStatus(Invoice.APPROVED);
//																			table2.setSrNo(2);
//
//																			cancellis
//																					.add(table2);
//																		} else {
//																			CancellationSummary table2 = new CancellationSummary();
//
//																			table2.setDocID(invoiceEntity
//																					.getCount());
//																			table2.setDocType(AppConstants.APPROVALINVOICEDETAILS);
//																			table2.setRemark(cancelPopup
//																					.getRemark()
//																					.getValue()
//																					.trim());
//																			table2.setLoggedInUser(LoginPresenter.loggedInUser);
//																			table2.setOldStatus(invoiceEntity
//																					.getStatus());
//																			table2.setNewStatus(Invoice.CANCELLED);
//																			table2.setSrNo(2);
//
//																			cancellis
//																					.add(table2);
//																		}
//																	}
//
//																	for (int i = 0; i < slist
//																			.size(); i++) {
//
//																		if (purchaseorderID == slist
//																				.get(i)
//																				.getContractCount())
//																			;
//																		{
//																			if (slist
//																					.get(i)
//																					.getStatus()
//																					.equals(Invoice.CREATED)) {
//																			}
//
//																			else if (slist
//																					.get(i)
//																					.getStatus()
//																					.equals(Invoice.APPROVED)) {
//																				cnt = cnt + 1;
//																			} else if (slist
//																					.get(i)
//																					.getStatus()
//																					.equals(Invoice.REJECTED)) {
//																			} else if (slist
//																					.get(i)
//																					.getStatus()
//																					.equals(Invoice.REQUESTED)) {
//																				requested = requested + 1;
//																			}
//
//																		}
//
//																	}
//																	if (cnt > 0) {
//
//																		MyQuerry querry = new MyQuerry();
//																		Company c = new Company();
//																		Vector<Filter> filtervec = new Vector<Filter>();
//																		Filter filter = null;
//																		filter = new Filter();
//																		filter.setQuerryString("companyId");
//																		filter.setLongValue(c
//																				.getCompanyId());
//																		filtervec
//																				.add(filter);
//																		filter = new Filter();
//																		filter.setQuerryString("contractCount");
//																		filter.setIntValue(purchaseorderID);
//																		filtervec
//																				.add(filter);
//																		filter = new Filter();
//																		filter.setQuerryString("typeOfOrder");
//																		filter.setStringValue(AppConstants.ORDERTYPEFORPURCHASE);
//																		filtervec
//																				.add(filter);
//																		querry.setFilters(filtervec);
//																		querry.setQuerryObject(new CustomerPayment());
//																		async.getSearchResult(
//																				querry,
//																				new AsyncCallback<ArrayList<SuperModel>>() {
//
//																					@Override
//																					public void onFailure(
//																							Throwable caught) {
//																					}
//
//																					@Override
//																					public void onSuccess(
//																							ArrayList<SuperModel> result) {
//
//																						int cnt = 0;
//
//																						ArrayList<CustomerPayment> slist = new ArrayList<CustomerPayment>();
//
//																						for (SuperModel model : result) {
//																							CustomerPayment custPayEntity = (CustomerPayment) model;
//
//																							custPayEntity
//																									.setStatus(custPayEntity
//																											.getStatus());
//																							custPayEntity
//																									.setInvoiceCount(custPayEntity
//																											.getInvoiceCount());
//																							custPayEntity
//																									.setContractCount(custPayEntity
//																											.getContractCount());
//																							slist.add(custPayEntity);
//
//																							if (custPayEntity
//																									.getStatus()
//																									.equals(CustomerPayment.PAYMENTCLOSED)) {
//																								CancellationSummary table2 = new CancellationSummary();
//
//																								table2.setDocID(custPayEntity
//																										.getCount());
//																								table2.setDocType("Payment Details");
//																								table2.setRemark(cancelPopup
//																										.getRemark()
//																										.getValue()
//																										.trim());
//																								table2.setLoggedInUser(LoginPresenter.loggedInUser);
//																								table2.setOldStatus(custPayEntity
//																										.getStatus());
//																								table2.setNewStatus(CustomerPayment.PAYMENTCLOSED);
//																								table2.setSrNo(2);
//
//																								cancellis
//																										.add(table2);
//																							} else {
//																								CancellationSummary table2 = new CancellationSummary();
//
//																								table2.setDocID(custPayEntity
//																										.getCount());
//																								table2.setDocType("Payment Details");
//																								table2.setRemark(cancelPopup
//																										.getRemark()
//																										.getValue()
//																										.trim());
//																								table2.setLoggedInUser(LoginPresenter.loggedInUser);
//																								table2.setOldStatus(custPayEntity
//																										.getStatus());
//																								table2.setNewStatus(Invoice.CANCELLED);
//																								table2.setSrNo(2);
//
//																								cancellis
//																										.add(table2);
//																							}
//																						}
//
//																						for (int i = 0; i < slist
//																								.size(); i++) {
//
//																							if (purchaseorderID == slist
//																									.get(i)
//																									.getContractCount())
//																								;
//																							{
//																								if (slist
//																										.get(i)
//																										.getStatus()
//																										.equals("Created")) {
//																									cnt = cnt + 1;
//																								}
//
//																								else if (slist
//																										.get(i)
//																										.getStatus()
//																										.equals("Closed")) {
//
//																								}
//																							}
//																						}
//
//																						if (cnt > 0) {
//																							cancelGRNDetails();
//																							cancelBillingDetails();
//																							cancelInvoiceDetails();
//																							cancelPaymentDetails();
//																							changeStatus(PurchaseOrder.CANCELLED);
//																						} else {
//
//																							cancelGRNDetails();
//																							cancelBillingDetails();
//																							cancelInvoiceDetails();
//																							cancelPaymentDetails();
//																							changeStatus(PurchaseOrder.CANCELLED);
//																						}
//																					}
//																				});
//																	} else {
//																		if (requested > 0) {
//																			cancelGRNDetails();
//																			cancelBillingDetails();
//																			cancelInvoiceDetails();
//																			changeStatus(PurchaseOrder.CANCELLED);
//																		}
//																	}
//
//																}
//															});
//													// **************************************************************
//												} else {
//
//													if ((flag > 0) || (req > 0)) {
//														cancelGRNDetails();
//														cancelBillingDetails();
//														changeStatus(PurchaseOrder.CANCELLED);
//
//													}
//												}
//											}
//										});
//							}
//						}

					}
				});

	}

	public void changeStatus(final String status) {
		int purchaseOrderId = Integer.parseInt(form.getIbPOId().getValue());
		String purchaseOrderStatus = form.getTbStatus().getValue().trim();
		model.setStatus(status);
		model.setDescription(model.getDescription() + "\n"
				+ "Purchase Order Id =" + purchaseOrderId + " "
				+ "Purchase Order Status =" + purchaseOrderStatus + "\n"
				+ "has been cancelled by " + LoginPresenter.loggedInUser
				+ " With remark." + "\n" + "Remark ="
				+ cancelPopup.getRemark().getValue().trim());
		async.save(model, new AsyncCallback<ReturnFromServer>() {
			@Override
			public void onFailure(Throwable caught) {
				form.showDialogMessage("An Unexpected Error occured !");
			}

			@Override
			public void onSuccess(ReturnFromServer result) {

				form.setToViewState();
				form.tbStatus.setText(status);

				CancellationSummary table5 = new CancellationSummary();

				table5.setDocID(model.getCount());
				table5.setDocType(AppConstants.APPROVALPURCHASEORDER);
				table5.setRemark(cancelPopup.getRemark().getValue().trim());
				table5.setLoggedInUser(LoginPresenter.loggedInUser);
				table5.setOldStatus(PurchaseOrder.APPROVED);
				table5.setNewStatus(model.getStatus());
				table5.setSrNo(5);

				cancellis.add(table5);
				CancellationSummaryPopUpTable popupTable = new CancellationSummaryPopUpTable();
				popupTable.setValue(cancellis);

				List<CancellationSummary> cancellistsetting1 = new ArrayList<CancellationSummary>();
				for (int i = 0; i < cancellis.size(); i++) {
					CancellationSummary sum = new CancellationSummary();
					sum.setSrNo(i + 1);
					sum.setDocID(cancellis.get(i).getDocID());
					sum.setDocType(cancellis.get(i).getDocType());
					sum.setLoggedInUser(cancellis.get(i).getLoggedInUser());
					sum.setOldStatus(cancellis.get(i).getOldStatus());
					sum.setNewStatus(cancellis.get(i).getNewStatus());
					sum.setRemark(cancellis.get(i).getRemark());

					cancellistsetting1.add(sum);
				}
				summaryPanel = new PopupPanel(true);
				summaryPanel.add(summaryPopup);
				summaryPanel.show();
				summaryPanel.center();
				summaryPopup.setSummaryTable(cancellistsetting1);

				CancelSummary cancelEntity = new CancelSummary();

				cancelEntity.setCancelLis(cancellistsetting1);
				cancelEntity.setDocType(AppConstants.APPROVALPURCHASEORDER);
				cancelEntity.setDocID(model.getCount());

				try {
					async.save(cancelEntity,
							new AsyncCallback<ReturnFromServer>() {

								@Override
								public void onFailure(Throwable caught) {
									form.showDialogMessage("An Unexpected Error occured !");
									//
								}

								@Override
								public void onSuccess(ReturnFromServer result) {
								}
							});
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	private void cancelGRNDetails() {

		final int purchaseorderID = model.getCount();
		final String purchaseorderStatus = model.getStatus();

		MyQuerry querry = new MyQuerry();
		Company c = new Company();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("poNo");
		filter.setIntValue(purchaseorderID);
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new GRN());
		System.out.println("querry completed......");

		async.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {

					@Override
					public void onFailure(Throwable caught) {
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {

						for (SuperModel model : result) {
							GRN grnEntity = (GRN) model;

							if (grnEntity.getStatus().equals(GRN.CREATED)) {

								grnEntity.setStatus(GRN.CANCELLED);
								grnEntity.setDescription("purchase order Id ="
										+ purchaseorderID
										+ " "
										+ "purchase order status ="
										+ purchaseorderStatus
										+ "\n"
										+ "has been cancelled by "
										+ LoginPresenter.loggedInUser
										+ " with remark "
										+ "\n"
										+ "Remark ="
										+ cancelPopup.getRemark().getValue()
												.trim() + " "
										+ "Cancellation Date =" + new Date());
								async.save(grnEntity,
										new AsyncCallback<ReturnFromServer>() {

											@Override
											public void onFailure(
													Throwable caught) {
												form.showDialogMessage("An Unexpected Error occured !");

											}

											@Override
											public void onSuccess(
													ReturnFromServer result) {
											}
										});
							}

							if (grnEntity.getStatus().equals(
									GRN.INSPECTIONREQUESTED)) {

								grnEntity.setStatus(GRN.CANCELLED);
								grnEntity.setDescription("Purchase Order Id ="
										+ purchaseorderID
										+ ","
										+ "purchase order status ="
										+ purchaseorderStatus
										+ "\n"
										+ "has been cancelled by "
										+ LoginPresenter.loggedInUser
										+ " with remark "
										+ "\n"
										+ "Remark ="
										+ cancelPopup.getRemark().getValue()
												.trim() + " "
										+ "Cancellation Date =" + new Date());
								async.save(grnEntity,
										new AsyncCallback<ReturnFromServer>() {

											@Override
											public void onFailure(
													Throwable caught) {
												form.showDialogMessage("An Unexpected Error occured !");

											}

											@Override
											public void onSuccess(
													ReturnFromServer result) {
											}
										});
							}

							if (grnEntity.getStatus().equals(
									GRN.INSPECTIONAPPROVED)) {

								grnEntity.setStatus(GRN.CANCELLED);
								grnEntity.setDescription("Purchase Order Id ="
										+ purchaseorderID
										+ " "
										+ "purchase order status ="
										+ purchaseorderStatus
										+ "\n"
										+ "has been cancelled by "
										+ LoginPresenter.loggedInUser
										+ " with remark "
										+ "\n"
										+ "Remark ="
										+ cancelPopup.getRemark().getValue()
												.trim() + " "
										+ "Cancellation Date =" + new Date());
								async.save(grnEntity,
										new AsyncCallback<ReturnFromServer>() {

											@Override
											public void onFailure(
													Throwable caught) {
												form.showDialogMessage("An Unexpected Error occured !");

											}

											@Override
											public void onSuccess(
													ReturnFromServer result) {
											}
										});
							}

							if (grnEntity.getStatus().equals(GRN.REJECTED)) {

								grnEntity.setStatus(GRN.CANCELLED);
								grnEntity.setDescription("Purchase Order Id ="
										+ purchaseorderID
										+ " "
										+ "purchase order status ="
										+ purchaseorderStatus
										+ "\n"
										+ "has been cancelled by "
										+ LoginPresenter.loggedInUser
										+ " with remark "
										+ "\n"
										+ "Remark ="
										+ cancelPopup.getRemark().getValue()
												.trim() + " "
										+ "Cancellation Date =" + new Date());
								async.save(grnEntity,
										new AsyncCallback<ReturnFromServer>() {

											@Override
											public void onFailure(
													Throwable caught) {
												form.showDialogMessage("An Unexpected Error occured !");

											}

											@Override
											public void onSuccess(
													ReturnFromServer result) {
											}
										});
							}

							if (grnEntity.getStatus().equals(GRN.REQUESTED)) {

								grnEntity.setStatus(GRN.CANCELLED);
								grnEntity.setDescription("purchase order Id ="
										+ purchaseorderID
										+ " "
										+ "purchase order status ="
										+ purchaseorderStatus
										+ "\n"
										+ "has been cancelled by "
										+ LoginPresenter.loggedInUser
										+ " with remark "
										+ "\n"
										+ "Remark ="
										+ cancelPopup.getRemark().getValue()
												.trim() + " "
										+ "Cancellation Date =" + new Date());
								async.save(grnEntity,
										new AsyncCallback<ReturnFromServer>() {

											@Override
											public void onFailure(
													Throwable caught) {
												form.showDialogMessage("An Unexpected Error occured !");

											}

											@Override
											public void onSuccess(
													ReturnFromServer result) {
											}
										});

								MyQuerry querry = new MyQuerry();
								Company c = new Company();
								Vector<Filter> filtervec = new Vector<Filter>();
								Filter filter = null;
								filter = new Filter();
								filter.setQuerryString("companyId");
								filter.setLongValue(c.getCompanyId());
								filtervec.add(filter);
								filter = new Filter();
								filter.setQuerryString("businessprocessId");
								filter.setIntValue(grnEntity.getCount());
								filtervec.add(filter);
								filter = new Filter();
								filter.setQuerryString("businessprocesstype");
								filter.setStringValue(AppConstants.APPROVALGRN);
								filtervec.add(filter);
								querry.setFilters(filtervec);
								querry.setQuerryObject(new Approvals());
								async.getSearchResult(
										querry,
										new AsyncCallback<ArrayList<SuperModel>>() {

											@Override
											public void onFailure(
													Throwable caught) {
											}

											@Override
											public void onSuccess(
													ArrayList<SuperModel> result) {

												for (SuperModel smodel : result) {
													Approvals approval = (Approvals) smodel;

													approval.setStatus(Approvals.CANCELLED);
													approval.setRemark("Purchase Order Id ="
															+ purchaseorderID
															+ " "
															+ "purchase order status ="
															+ purchaseorderStatus
															+ "\n"
															+ "has been cancelled by "
															+ LoginPresenter.loggedInUser
															+ " with remark "
															+ "\n"
															+ "Remark ="
															+ cancelPopup
																	.getRemark()
																	.getValue()
																	.trim()
															+ " "
															+ "Cancellation Date ="
															+ new Date());

													async.save(
															approval,
															new AsyncCallback<ReturnFromServer>() {

																@Override
																public void onFailure(
																		Throwable caught) {
																	form.showDialogMessage("An Unexpected Error occured !");
																}

																@Override
																public void onSuccess(
																		ReturnFromServer result) {

																}
															});

												}
											}
										});
							}
						}
					}
				});
	}

	private void cancelBillingDetails() {

		final int purchaseorderID = model.getCount();
		final String purchaseorderStatus = model.getStatus();

		MyQuerry querry = new MyQuerry();
		Company c = new Company();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("contractCount");
		filter.setIntValue(purchaseorderID);
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("typeOfOrder");
		filter.setStringValue(AppConstants.ORDERTYPEFORPURCHASE);
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new BillingDocument());
		async.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {

					@Override
					public void onFailure(Throwable caught) {
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {

						for (SuperModel smodel : result) {
							BillingDocument biil = (BillingDocument) smodel;

							if (biil.getStatus()
									.equals(BillingDocument.CREATED)) {

								biil.setStatus(BillingDocument.CANCELLED);
								biil.setComment("Purchase Order Id ="
										+ purchaseorderID
										+ " "
										+ "purchase order status ="
										+ purchaseorderStatus
										+ "\n"
										+ "has been cancelled by "
										+ LoginPresenter.loggedInUser
										+ " with remark "
										+ "\n"
										+ "Remark ="
										+ cancelPopup.getRemark().getValue()
												.trim() + " "
										+ "Cancellation Date =" + new Date());
								async.save(biil,
										new AsyncCallback<ReturnFromServer>() {

											@Override
											public void onFailure(
													Throwable caught) {
												form.showDialogMessage("An Unexpected Error occured !");
												//
											}

											@Override
											public void onSuccess(
													ReturnFromServer result) {
											}
										});
							}

							if (biil.getStatus().equals(
									BillingDocument.APPROVED)) {

								biil.setStatus(BillingDocument.CANCELLED);
								biil.setComment("Purchase Order Id ="
										+ purchaseorderID
										+ " "
										+ "purchase order status ="
										+ purchaseorderStatus
										+ "\n"
										+ "has been cancelled by "
										+ LoginPresenter.loggedInUser
										+ " with remark "
										+ "\n"
										+ "Remark ="
										+ cancelPopup.getRemark().getValue()
												.trim() + " "
										+ "Cancellation Date =" + new Date());
								async.save(biil,
										new AsyncCallback<ReturnFromServer>() {

											@Override
											public void onFailure(
													Throwable caught) {
												form.showDialogMessage("An Unexpected Error occured !");
												//
											}

											@Override
											public void onSuccess(
													ReturnFromServer result) {
											}
										});
							}
							if (biil.getStatus().equals(
									BillingDocument.BILLINGINVOICED)) {
								biil.setRenewFlag(true);
								biil.setStatus(BillingDocument.BILLINGINVOICED);
								biil.setComment("purchase order Id ="
										+ purchaseorderID
										+ " "
										+ "purchase order status ="
										+ purchaseorderStatus
										+ "\n"
										+ "has been cancelled by "
										+ LoginPresenter.loggedInUser
										+ " with remark "
										+ "\n"
										+ "Remark ="
										+ cancelPopup.getRemark().getValue()
												.trim() + " "
										+ "Cancellation Date =" + new Date());
								async.save(biil,
										new AsyncCallback<ReturnFromServer>() {

											@Override
											public void onFailure(
													Throwable caught) {
												form.showDialogMessage("An Unexpected Error occured !");
												//
											}

											@Override
											public void onSuccess(
													ReturnFromServer result) {
											}
										});
							}

							if (biil.getStatus().equals(
									BillingDocument.REJECTED)) {

								biil.setStatus(BillingDocument.CANCELLED);
								biil.setComment("Purchase Order Id ="
										+ purchaseorderID
										+ ","
										+ "purchase order status ="
										+ purchaseorderStatus
										+ "\n"
										+ "has been cancelled by "
										+ LoginPresenter.loggedInUser
										+ " with remark "
										+ "\n"
										+ "Remark ="
										+ cancelPopup.getRemark().getValue()
												.trim() + " "
										+ "Cancellation Date =" + new Date());
								async.save(biil,
										new AsyncCallback<ReturnFromServer>() {

											@Override
											public void onFailure(
													Throwable caught) {
												form.showDialogMessage("An Unexpected Error occured !");
												//
											}

											@Override
											public void onSuccess(
													ReturnFromServer result) {
											}
										});
							}

							if (biil.getStatus().equals(
									BillingDocument.REQUESTED)) {

								biil.setStatus(BillingDocument.CANCELLED);
								biil.setComment("Purchase Order Id ="
										+ purchaseorderID
										+ " "
										+ "purchase order status ="
										+ purchaseorderStatus
										+ "\n"
										+ "has been cancelled by "
										+ LoginPresenter.loggedInUser
										+ " with remark "
										+ "\n"
										+ "Remark ="
										+ cancelPopup.getRemark().getValue()
												.trim() + " "
										+ "Cancellation Date =" + new Date());
								async.save(biil,
										new AsyncCallback<ReturnFromServer>() {

											@Override
											public void onFailure(
													Throwable caught) {
												form.showDialogMessage("An Unexpected Error occured !");
												//
											}

											@Override
											public void onSuccess(
													ReturnFromServer result) {
											}
										});

								MyQuerry querry = new MyQuerry();
								Company c = new Company();
								Vector<Filter> filtervec = new Vector<Filter>();
								Filter filter = null;
								filter = new Filter();
								filter.setQuerryString("companyId");
								filter.setLongValue(c.getCompanyId());
								filtervec.add(filter);
								filter = new Filter();
								filter.setQuerryString("businessprocessId");
								filter.setIntValue(biil.getCount());
								filtervec.add(filter);
								filter = new Filter();
								filter.setQuerryString("businessprocesstype");
								filter.setStringValue(AppConstants.APPROVALBILLINGDETAILS);
								filtervec.add(filter);
								querry.setFilters(filtervec);
								querry.setQuerryObject(new Approvals());
								async.getSearchResult(
										querry,
										new AsyncCallback<ArrayList<SuperModel>>() {

											@Override
											public void onFailure(
													Throwable caught) {
											}

											@Override
											public void onSuccess(
													ArrayList<SuperModel> result) {

												for (SuperModel smodel : result) {
													Approvals approval = (Approvals) smodel;

													approval.setStatus(Approvals.CANCELLED);
													approval.setRemark("Purchase Order Id ="
															+ purchaseorderID
															+ " "
															+ "purchase order status ="
															+ purchaseorderStatus
															+ "\n"
															+ "has been cancelled by "
															+ LoginPresenter.loggedInUser
															+ " with remark "
															+ "\n"
															+ "Remark ="
															+ cancelPopup
																	.getRemark()
																	.getValue()
																	.trim()
															+ " "
															+ "Cancellation Date ="
															+ new Date());
													async.save(
															approval,
															new AsyncCallback<ReturnFromServer>() {

																@Override
																public void onFailure(
																		Throwable caught) {
																	form.showDialogMessage("An Unexpected Error occured !");
																}

																@Override
																public void onSuccess(
																		ReturnFromServer result) {

																}
															});

												}
											}
										});

							}
						}
					}

				});

	}

	private void cancelPaymentDetails() {

		final int purchaseorderID = model.getCount();
		final String purchaseorderStatus = model.getStatus();

		MyQuerry querry = new MyQuerry();
		Company c = new Company();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("contractCount");
		filter.setIntValue(purchaseorderID);
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("typeOfOrder");
		filter.setStringValue(AppConstants.ORDERTYPEFORPURCHASE);
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new CustomerPayment());
		async.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {

					@Override
					public void onFailure(Throwable caught) {
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {

						for (SuperModel smodel : result) {

							CustomerPayment custpay = (CustomerPayment) smodel;
							if (custpay.getStatus().equals(
									CustomerPayment.CREATED)) {
								custpay.setStatus(CustomerPayment.CANCELLED);
								custpay.setComment("Purchase Order Id ="
										+ purchaseorderID
										+ " "
										+ "purchase order status ="
										+ purchaseorderStatus
										+ "\n"
										+ "has been cancelled by "
										+ LoginPresenter.loggedInUser
										+ " with remark "
										+ "\n"
										+ "Remark ="
										+ cancelPopup.getRemark().getValue()
												.trim() + " "
										+ "Cancellation Date =" + new Date());
								async.save(custpay,
										new AsyncCallback<ReturnFromServer>() {
											//
											@Override
											public void onFailure(
													Throwable caught) {
												form.showDialogMessage("An Unexpected Error occured !");
												//
											}

											@Override
											public void onSuccess(
													ReturnFromServer result) {
											}
										});
							}
							if (custpay.getStatus().equals(
									CustomerPayment.PAYMENTCLOSED)) {
								custpay.setRenewFlag(true);
								custpay.setStatus(CustomerPayment.PAYMENTCLOSED);
								custpay.setComment("Purchase Order Id ="
										+ purchaseorderID
										+ " "
										+ "purchase order status ="
										+ purchaseorderStatus
										+ "\n"
										+ "has been cancelled by "
										+ LoginPresenter.loggedInUser
										+ " with remark "
										+ "\n"
										+ "Remark ="
										+ cancelPopup.getRemark().getValue()
												.trim() + " "
										+ "Cancellation Date =" + new Date());
								async.save(custpay,
										new AsyncCallback<ReturnFromServer>() {
											//
											@Override
											public void onFailure(
													Throwable caught) {
												form.showDialogMessage("An Unexpected Error occured !");
												//
											}

											@Override
											public void onSuccess(
													ReturnFromServer result) {
											}
										});
							}
						}
					}
				});
	}

	private void cancelInvoiceDetails() {

		final int purchaseorderID = model.getCount();
		final String purchaseorderStatus = model.getStatus();

		MyQuerry querry = new MyQuerry();
		Company c = new Company();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("contractCount");
		filter.setIntValue(purchaseorderID);
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("typeOfOrder");
		filter.setStringValue(AppConstants.ORDERTYPEFORPURCHASE);
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Invoice());
		async.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {

					@Override
					public void onFailure(Throwable caught) {
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {

						for (SuperModel smodel : result) {
							// **************inset login user name in comment
							// *******************

							Invoice invoice = (Invoice) smodel;
							if (invoice.getStatus().equals(Invoice.CREATED)) {
								invoice.setStatus(Invoice.CANCELLED);
								invoice.setComment("Purchase Order Id ="
										+ purchaseorderID
										+ " "
										+ "purchase order status ="
										+ purchaseorderStatus
										+ "\n"
										+ "has been cancelled by "
										+ LoginPresenter.loggedInUser
										+ " with remark "
										+ "\n"
										+ "Remark ="
										+ cancelPopup.getRemark().getValue()
												.trim() + " "
										+ "Cancellation Date =" + new Date());
								async.save(invoice,
										new AsyncCallback<ReturnFromServer>() {
											@Override
											public void onFailure(
													Throwable caught) {
												form.showDialogMessage("An Unexpected Error occured !");
											}

											@Override
											public void onSuccess(
													ReturnFromServer result) {
											}
										});
							}

							if (invoice.getStatus().equals(Invoice.APPROVED)) {
								invoice.setRenewFlag(true);
								invoice.setStatus(Invoice.APPROVED);
								invoice.setComment("Purchase Order Id ="
										+ purchaseorderID
										+ " "
										+ "purchase order status ="
										+ purchaseorderStatus
										+ "\n"
										+ "has been cancelled by "
										+ LoginPresenter.loggedInUser
										+ " with remark "
										+ "\n"
										+ "Remark ="
										+ cancelPopup.getRemark().getValue()
												.trim() + " "
										+ "Cancellation Date =" + new Date());
								async.save(invoice,
										new AsyncCallback<ReturnFromServer>() {
											@Override
											public void onFailure(
													Throwable caught) {
												form.showDialogMessage("An Unexpected Error occured !");
											}

											@Override
											public void onSuccess(
													ReturnFromServer result) {
											}
										});
							}

							if (invoice.getStatus().equals(Invoice.REJECTED)) {
								invoice.setStatus(Invoice.CANCELLED);
								invoice.setComment("Purchase Order Id ="
										+ purchaseorderID
										+ " "
										+ "purchase order status ="
										+ purchaseorderStatus
										+ "\n"
										+ "has been cancelled by "
										+ LoginPresenter.loggedInUser
										+ " with remark "
										+ "\n"
										+ "Remark ="
										+ cancelPopup.getRemark().getValue()
												.trim() + " "
										+ "Cancellation Date =" + new Date());
								async.save(invoice,
										new AsyncCallback<ReturnFromServer>() {
											@Override
											public void onFailure(
													Throwable caught) {
												form.showDialogMessage("An Unexpected Error occured !");
											}

											@Override
											public void onSuccess(
													ReturnFromServer result) {
											}
										});
							}

							if (invoice.getStatus().equals(Invoice.REQUESTED)) {
								invoice.setStatus(Invoice.CANCELLED);
								invoice.setComment("Purchase Order Id ="
										+ purchaseorderID
										+ " "
										+ "purchase order status ="
										+ purchaseorderStatus
										+ "\n"
										+ "has been cancelled by "
										+ LoginPresenter.loggedInUser
										+ " with remark "
										+ "\n"
										+ "Remark ="
										+ cancelPopup.getRemark().getValue()
												.trim() + " "
										+ "Cancellation Date =" + new Date());
								async.save(invoice,
										new AsyncCallback<ReturnFromServer>() {
											@Override
											public void onFailure(
													Throwable caught) {
												form.showDialogMessage("An Unexpected Error occured !");
											}

											@Override
											public void onSuccess(
													ReturnFromServer result) {
											}
										});

								MyQuerry querry = new MyQuerry();
								Company c = new Company();
								Vector<Filter> filtervec = new Vector<Filter>();
								Filter filter = null;
								filter = new Filter();
								filter.setQuerryString("companyId");
								filter.setLongValue(c.getCompanyId());
								filtervec.add(filter);
								filter = new Filter();
								filter.setQuerryString("businessprocessId");
								filter.setIntValue(invoice.getCount());
								filtervec.add(filter);
								filter = new Filter();
								filter.setQuerryString("businessprocesstype");
								filter.setStringValue(AppConstants.APPROVALINVOICEDETAILS);
								filtervec.add(filter);

								querry.setFilters(filtervec);
								querry.setQuerryObject(new Approvals());
								System.out.println("querry completed......");
								async.getSearchResult(
										querry,
										new AsyncCallback<ArrayList<SuperModel>>() {

											@Override
											public void onFailure(
													Throwable caught) {
											}

											@Override
											public void onSuccess(
													ArrayList<SuperModel> result) {

												for (SuperModel smodel : result) {
													Approvals approval = (Approvals) smodel;

													approval.setStatus(Approvals.CANCELLED);
													approval.setRemark("Purchase Order Id ="
															+ purchaseorderID
															+ " "
															+ "purchase order status ="
															+ purchaseorderStatus
															+ "\n"
															+ "has been cancelled by "
															+ LoginPresenter.loggedInUser
															+ " with remark "
															+ "\n"
															+ "Remark ="
															+ cancelPopup
																	.getRemark()
																	.getValue()
																	.trim()
															+ " "
															+ "Cancellation Date ="
															+ new Date());

													async.save(
															approval,
															new AsyncCallback<ReturnFromServer>() {

																@Override
																public void onFailure(
																		Throwable caught) {
																	form.showDialogMessage("An Unexpected Error occured !");
																}

																@Override
																public void onSuccess(
																		ReturnFromServer result) {

																}
															});
												}
											}
										});
							}
						}
					}
				});
	}

	/************************* ON PRODUCT DELETE VENDOR UPDATION CODE *******************************/

	/**
	 * This method update vendor table when we delete product Date : 10-09-2016
	 * By Anil Release : 31 Aug 2016 Project : PURCHASE ERROR SOLVING (NBHC)
	 */

	public static void updateVendorTable() {
//		System.out.println("PRODUCT TBL SIZE : "+ form.productTablePO.getDataprovider().getList().size());
		if (form.productTablePO.getDataprovider().getList().size() == 0) {
			form.vendorTablePO.getDataprovider().getList().clear();
			form.vendorRateList.clear();
		} else {
			List<ProductDetailsPO> prodList = form.productTablePO.getValue();
//			System.out.println("GLOBAL VENDOR PRICE LIST SIZE BF : "+ form.vendorRateList.size());
			List<PriceListDetails> remProdComVenLis = getRemainingProductVendorPriceList(prodList, form.vendorRateList);
//			System.out.println("GLOBAL VENDOR PRICE LIST SIZE AF : "+ form.vendorRateList.size());
//			System.out.println("COMMON VENDOR PRICEL LIST SIZE : "+ remProdComVenLis.size());
//			System.out.println("EARLIER VEN TBL LIST SIZE :"+ form.vendorTablePO.getDataprovider().getList().size());
			List<VendorDetails> vendorList = getVendorListOfRemainingProduct(form.vendorTablePO.getDataprovider().getList(),remProdComVenLis);
//			System.out.println("NOW VEN TBL LIST SIZE : " + vendorList.size());
//			System.out.println("AFTER REMOVING : "+ form.vendorTablePO.getDataprovider().getList().size());
		}
	}

	/**
	 * This method returns the list of common vendor from global vendor list
	 * which is maintained at the time of product add.
	 * 
	 * Date : 10-09-2016 By Anil Release : 31 Aug 2016 Project : PURCHASE ERROR
	 * SOLVING (NBHC) I/P:existing vendor table list and global vendor list
	 * O/P:vendor list
	 */
	private static List<PriceListDetails> getRemainingProductVendorPriceList(
			List<ProductDetailsPO> prodList,
			ArrayList<PriceListDetails> vendorRateList) {

		ArrayList<PriceListDetails> vendorPriceList = new ArrayList<PriceListDetails>();
		List<PriceListDetails> list = new ArrayList<PriceListDetails>();

		for (ProductDetailsPO prodDet : prodList) {
			for (PriceListDetails priceLis : vendorRateList) {
				if (prodDet.getProductID() == priceLis.getProdID()) {
					vendorPriceList.add(priceLis);
				}
			}
		}

		form.vendorRateList.clear();
		form.vendorRateList.addAll(vendorPriceList);

		for (PriceListDetails pricelis : vendorPriceList) {
			if (list.size() == 0) {
				list.add(pricelis);
			} else {
				boolean isAlreadyAddedFlag = checkPriceListAlreadyAdded(list,
						pricelis);
				if (isAlreadyAddedFlag == false) {
					list.add(pricelis);
				}
			}
		}
		return list;
	}

	/**
	 * This method compare whether passed parameter 2nd is present in parameter
	 * 1st i.e. vendor price list Date 10-09-2016 By Anil Release : 31 Aug 2016
	 * Project: PURCHASE ERROR SOLVING(NBHC) I/P:vendor price list and vendor
	 * price O/P:True/False
	 */
	private static boolean checkPriceListAlreadyAdded(
			List<PriceListDetails> list, PriceListDetails pricelis) {
		for (PriceListDetails pricelist : list) {
			if (pricelis.getVendorID() == pricelist.getVendorID()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * This method returns common vendors of remaining products. Date 10-09-2016
	 * By Anil Release : 31 Aug 2016 Project: PURCHASE ERROR SOLVING(NBHC)
	 * I/P:existing vendor list and remaining products vendor list O/P:vendor
	 * list
	 */

	private static List<VendorDetails> getVendorListOfRemainingProduct(
			List<VendorDetails> vendorList, List<PriceListDetails> venRateLis) {
//		System.out.println("REMAINING PRODUCTS VENDOR LIST");

//		System.out.println("VEN LIS SIZE BF " + vendorList.size());
		for (PriceListDetails venRLis : venRateLis) {
			boolean vendorFlag = isVendorPresent(venRLis.getVendorID(),vendorList);
			if (vendorFlag == false) {
				List<PriceListDetails> vLis = new ArrayList<PriceListDetails>();
				vLis.add(venRLis);
				ArrayList<VendorDetails> vDeLis = form.getVendorList(vLis);
				vendorList.addAll(vDeLis);
			}
		}

//		System.out.println("VEN LIS SIZE AF " + vendorList.size());
		return vendorList;
	}

	/**
	 * This method returns true if passed vendor id is present in given vendor
	 * list. Date : 10-09-2016 By Anil Release : 31 Aug 2016 Project: PURCHASE
	 * ERROR SOLVING (NBHC) I/P:Vendor id and Vendor List O/P:True/False
	 */
	private static boolean isVendorPresent(int vendorID,
			List<VendorDetails> vendorList) {
		for (VendorDetails venDet : vendorList) {
			if (vendorID == venDet.getVendorId()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * This method return true if vendor is selected Date : 13-09-2016 By Anil
	 * Release 31 Aug 2016 Project: PURCHASE ERROR SOLVING (NBHC)
	 * 
	 */

	public static boolean isVendorSelected() {
		List<VendorDetails> list = form.vendorTablePO.getDataprovider()
				.getList();

		for (VendorDetails ven : list) {
			if (ven.getStatus() == true) {
				return true;
			}
		}

		form.showDialogMessage("Please select vendor!");
		return false;
	}

	/**
	 * This method return true if CST is applicable Date : 13-09-2016 By Anil
	 * Release 31 Aug 2016 Project: PURCHASE ERROR SOLVING (NBHC)
	 * 
	 */

//	public static boolean isCformCstApplicable() {
//
//		if (form.getDeliveryadd().getState().getSelectedIndex() != 0
//				&& !vendorState.equals("")
//				&& !form.getDeliveryadd().getState().getValue().trim()
//						.equals(vendorState.trim())) {
//
//			form.getCbcformlis().setEnabled(true);
//
//			if (form.getCbcformlis().getSelectedIndex() == 0) {
//				form.showDialogMessage("Please select C Form");
//				return true;
//			} else {
//				if (form.getCbcformlis()
//						.getItemText(form.getCbcformlis().getSelectedIndex())
//						.equals(AppConstants.YES)
//						&& form.getOlbcstpercent().getSelectedIndex() == 0) {
//					form.getOlbcstpercent().setEnabled(true);
//					form.showDialogMessage("Please select CST Percent");
//					return true;
//				}
//			}
//		} else {
////			System.out.println("SET CFORM CST BF");
//			form.getCbcformlis().setSelectedIndex(0);
//			form.getOlbcstpercent().setSelectedIndex(0);
//
////			System.out.println("SET CFORM CST AF");
//
//			form.getCbcformlis().setEnabled(false);
//			form.getOlbcstpercent().setEnabled(false);
//		}
//		return false;
//	}

	/**
	 * This method sets price mention in vendor price list to added products
	 * Date:13-09-2016 By Anil Release 31 Aug 2016 Project : PURCHASE ERROR
	 * SOLVING I/P: Product List and vendor id
	 */
	private void setVendorPriceToProduct(List<ProductDetailsPO> value,int venCount) {

		ArrayList<ProductDetailsPO> prodList = new ArrayList<ProductDetailsPO>();

//		System.out.println("PRICE EDITING ");
//		System.out.println("VENDOR LIST BF  - " + form.vendorRateList.size());
		ArrayList<PriceListDetails> selVenPricLis = new ArrayList<PriceListDetails>();
		for (PriceListDetails venlis : form.vendorRateList) {
			if (venlis.getVendorID() == venCount) {
				selVenPricLis.add(venlis);
			}
		}
//		System.out.println("VENDOR LIST AF- " + selVenPricLis.size());
		for (ProductDetailsPO prodpo : value) {
			for (PriceListDetails venlis : selVenPricLis) {
				if (prodpo.getProductID() == venlis.getProdID()) {

					prodpo.setProdPrice(venlis.getProductPrice());
					/**
					 * Date : 19-04-2018 By ANIL
					 */
					prodpo.getPrduct().setPrice(venlis.getProductPrice());
					/**
					 * End
					 */
					System.out.println(prodpo.getProductID() + " "+ prodpo.getProdPrice());
				}
			}
		}

		form.productTablePO.getTable().redraw();
	}
	
	
	
	/**
	 * This method make delivery address read only if warehouse is selected
	 * and also checks whether vendor state and warehouse state is different or not
	 * if vendor State is different then it ask for cform and cstpercent
	 * Date : 03-10-2016 By Anil
	 * Release : 30 Sept 2016 
	 * Project: PURCHASE MODIFICATION(NBHC) 
	 */
	
	public static void updateWarehouseAndCformStatus(){
		if(!vendorState.equals("")){
			List<ProductDetailsPO> list=form.productTablePO.getDataprovider().getList();
			
			boolean addressFlag=false;
			for(ProductDetailsPO obj:list){
				if(!obj.getWarehouseName().equals("")){
					form.deliveryadd.clear();
					form.deliveryadd.setEnable(false);
					form.cbadds.setValue(false);
					form.cbadds.setEnabled(false);
					addressFlag=true;
					break;
				}
			}
			
			if(!addressFlag){
				form.cbadds.setValue(true);
				form.deliveryadd.setEnable(true);
			}
			
			HashSet<String> addressSet=new HashSet<String>();
			for(ProductDetailsPO obj:list){
				if(!obj.getWarehouseName().equals("")){
					addressSet.add(obj.getWarehouseName());
				}
			}
//			System.out.println("ADDRESS SET SIZE :: "+addressSet.size());
			
			if(addressSet.size()!=0){
				if(addressSet.size()==1){
					ArrayList<String> addressList=new ArrayList<String>();
					addressList.addAll(addressSet);
//					System.out.println("Setting WH Address as delivery address "+addressSet.toString());
					form.deliveryadd.setValue(ProductTablePO.getWarehouseAddress(addressList.get(0)));
					form.deliveryadd.setEnable(false);
					form.cbadds.setValue(false);
					form.cbadds.setEnabled(false);
					form.deliveryaddress.setValue("Warehouse Address");
				}
			}
			
			boolean cformFlag=false;
			for(ProductDetailsPO obj:list){
				if(!obj.getWarehouseName().equals("")){
					String warehouseState=ProductTablePO.getWarehouseState(obj.getWarehouseName());
					if(!vendorState.trim().equals(warehouseState.trim())){
						cformFlag=true;
//						form.getCbcformlis().setEnabled(true);
//						if(form.getCbcformlis().getSelectedIndex()==0){
//							form.showDialogMessage("Please select C Form");
//						}
//						if(form.getCbcformlis().getItemText(form.getCbcformlis().getSelectedIndex()).equals(AppConstants.YES)){
//							if(form.getOlbcstpercent().getSelectedIndex()==0){
//								form.showDialogMessage("Please select CST Percent");
//							}
//						}
						break;
					}
				}
			}
			
//			if(!cformFlag){
//				form.getCbcformlis().setSelectedIndex(0);
//				form.getOlbcstpercent().setSelectedIndex(0);
//				form.getCbcformlis().setEnabled(false);
//				form.getOlbcstpercent().setEnabled(false);
//			}
		}else{
			List<ProductDetailsPO> list=form.productTablePO.getDataprovider().getList();
			
			HashSet<String> addressSet=new HashSet<String>();
			for(ProductDetailsPO obj:list){
				if(!obj.getWarehouseName().equals("")){
					addressSet.add(obj.getWarehouseName());
				}
			}
//			System.out.println("ADDRESS SET SIZE :: "+addressSet.size());
			
			if(addressSet.size()!=0){
				if(addressSet.size()==1){
					ArrayList<String> addressList=new ArrayList<String>();
					addressList.addAll(addressSet);
//					System.out.println("Setting WH Address as delivery address "+addressSet.toString());
					form.deliveryadd.setValue(ProductTablePO.getWarehouseAddress(addressList.get(0)));
					form.deliveryadd.setEnable(false);
					form.cbadds.setValue(false);
					form.cbadds.setEnabled(false);
					form.deliveryaddress.setValue("Warehouse Address");

				}else{
					form.deliveryadd.clear();
					form.deliveryadd.setEnable(false);
					form.cbadds.setValue(false);
					form.cbadds.setEnabled(false);
				}
			}else{
				form.deliveryadd.clear();
				form.deliveryadd.setEnable(false);
				form.cbadds.setValue(false);
				form.cbadds.setEnabled(false);	
			}
		}
	}
	
	
	
	/**
	 *     Added By : Priyanka Bhagwat
	 *     Date : 1/06/2021
	 *     Des : When Submit PO then SMS and EMAIL will be send Req by Pecopp raised By Ashwini.	
	 */
	/***********************************************Sms Method***************************************/
	
	
	private void checkSendSmsStatus() {
		
		Console.log("Inside check sms status");
		Vector<Filter> filtrvec = new Vector<Filter>();
		Filter filtr = null;
		
		filtr = new Filter();
		filtr.setLongValue(model.getCompanyId());
		filtr.setQuerryString("companyId");
		filtrvec.add(filtr);
		
		filtr = new Filter();
		filtr.setBooleanvalue(true);
		filtr.setQuerryString("status");
		filtrvec.add(filtr);
		
		MyQuerry querry = new MyQuerry();
		querry.setFilters(filtrvec);
		querry.setQuerryObject(new SmsConfiguration());
//		form.showWaitSymbol();
		Console.log("status of sms");
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				Console.log("Success of check sms status");
				if(result.size()!=0){
					Console.log("Result");
					for(SuperModel model:result){
						SmsConfiguration smsconfig = (SmsConfiguration) model;
						boolean smsStatus = smsconfig.getStatus();
						String accountsid = smsconfig.getAccountSID();
						String authotoken = smsconfig.getAuthToken();
						String fromnumber = smsconfig.getPassword();
						logger.log(Level.SEVERE,"Inside send sms Method:" +smsStatus);
						Console.log("Status"+smsStatus);
						if(smsStatus==true){
//						 sendSMS(accountsid, authotoken, fromnumber);
						}
					}
				}
			}
			@Override
			public void onFailure(Throwable caught) {
				
			}
		});
	}
	
	
	private void sendSMS() {
	
//		if(model.getStatus().equals(AppConstants.SUBMIT)){
		Vector<Filter> filtrvec = new Vector<Filter>();
		Filter filtr = null;
		filtr = new Filter();
		filtr.setLongValue(model.getCompanyId());
		filtr.setQuerryString("companyId");
		filtrvec.add(filtr);
		
		filtr = new Filter();
		filtr.setStringValue("Purchase Order");
		filtr.setQuerryString("event");
		filtrvec.add(filtr);
		
		
		
		filtr = new Filter();
		filtr.setBooleanvalue(true);
		filtr.setQuerryString("status");
		filtrvec.add(filtr);
		
		MyQuerry querry = new MyQuerry();
		querry.setFilters(filtrvec);
		querry.setQuerryObject(new SmsTemplate());
		
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				Console.log("Inside Success method of sms");
				if(result.size()!=0){
					Console.log("Result"+result.size());
					for(SuperModel model1:result){
						SmsTemplate sms = (SmsTemplate) model1;
//						String vendorName = "";
//						//String tocell="";
//						int vendorId=0;
//						if(model!=null){
//							if(model.getVendorDetails()!=null&&model.getVendorDetails().size()!=0){
//								for(VendorDetails vendor:model.getVendorDetails()){
//									if(vendor.getStatus()){//
//										//if(vendor.getVendorPhone()!=null&&!vendor.getVendorPhone().equals(""))) {
//										if(vendor.getVendorPhone()!=0) {
//											Console.log("Vendor contact"+vendor.getVendorPhone());
//											cellNo=vendor.getVendorPhone();
//										}else {
//											cellNo = AppUtility.getVendorCellNo(vendorId);
//										}
//										vendorId=vendor.getVendorId();
//										vendorName = vendor.getVendorName();
//									}
//								}
//							}
//						}
//						
//						
//						Console.log("sms getMsg"+sms.getMessage());
//						String templateMsgwithbraces = sms.getMessage();
//						String purchaseno = form.ibPOId.getValue()+"";
//						Date podate = form.dbPODate.getValue();
//						String frminvoiceDate = AppUtility.parseDate(podate);
//						
////						Console.log("Vendor contact"+vendor.getCellNumber1());
////						cellNo = vendor.getCellNumber1();
//						
//						String purchaseorderno = templateMsgwithbraces.replace("{PurchaseOrderNo}",purchaseno);
//						String purDate = purchaseorderno.replace("{PODate}", frminvoiceDate);
//						companyName=Slick_Erp.businessUnitName;
//						actualmsg = purDate.replace("{CompanyName}", companyName);
						
					
						 SMSDetails smsdetails = new SMSDetails();
						 smsdetails.setModel(model);
						 smsdetails.setSmsTemplate(sms);
						 smsdetails.setEntityName("PurchaseOrder");
						 smsdetails.setMobileNumber(cellNo+"");
						 smsdetails.setCommunicationChannel(AppConstants.SMS);
						 smsdetails.setModuleName(AppConstants.PURCHASEMODULE);
						 smsdetails.setDocumentName(AppConstants.PURCHASEORDER);
						 smsdetails.setPdfURL(LoginPresenter.URL);

						smsserviceAsync.validateAndSendSMS(smsdetails, new AsyncCallback<String>() {
							
							@Override
							public void onSuccess(String result) {
								// TODO Auto-generated method stub
								form.showDialogMessage("Message Sent Successfully!");

							}
							
							@Override
							public void onFailure(Throwable caught) {
								// TODO Auto-generated method stub
								
							}
						});

					}
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
			}
		});
//		}
	}
	
	
	private void savesmshistory() {
		SmsHistory smshistory = new SmsHistory();
		smshistory.setDocumentId(model.getCount());
		smshistory.setDocumentType("Purchase Order");
		smshistory.setName(Form.getTbVendorName().getValue());
		smshistory.setCellNo(cellNo);
		smshistory.setSmsText(actualmsg);
		smshistory.setUserId(LoginPresenter.loggedInUser);
		
		async.save(smshistory, new AsyncCallback<ReturnFromServer>() {
			
			@Override
			public void onSuccess(ReturnFromServer result) {
				System.out.println("Data Saved Successfully");
			}
			
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("Data Save Unsuccessfull");
			}
		});
		
	}
	
	/**
	 * 
	 * End
	 */

	public static void updateTaxesDetails(List<ProductDetailsPO> list,Branch branchEntity, String vendorState2) {
		
		
		try {
	          GWTCAlert gwtalert = new GWTCAlert();
	          
		for(ProductDetailsPO productDetails : list){
						
					System.out.println("LoginPresenter.globalTaxList"+LoginPresenter.globalTaxList.size());
					
					if(productDetails.getVatTax()!=null && productDetails.getVatTax().getPercentage()!=0){
						productDetails.setPurchaseTax1(productDetails.getVatTax());
					}
					if(productDetails.getServiceTax()!=null && productDetails.getServiceTax().getPercentage()!=0){
						productDetails.setPurchaseTax2(productDetails.getServiceTax());
					}
					
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany")){
					if(vendorState2!=null && !vendorState2.trim().equals("") 
							&& branchEntity.getAddress().getState()!=null && !branchEntity.getAddress().getState().equals("") ){
						if(!vendorState2.trim().equals(branchEntity.getAddress().getState().trim())){
							System.out.println("branch as company different state");
							
							if(productDetails.getPurchaseTax1()!=null && productDetails.getPurchaseTax1().getPercentage()!=0){
								Tax tax1 = new Tax();
								double tax1percentage = 0;
								if(productDetails.getPurchaseTax1()!=null && productDetails.getPurchaseTax1().getPercentage()!=0){
									 if(productDetails.getPurchaseTax1().getTaxPrintName().trim().equals("CGST") || productDetails.getPurchaseTax1().getTaxPrintName().trim().equals("SGST")){
										 if(productDetails.getServiceTax()!=null ){
											 tax1percentage = productDetails.getPurchaseTax1().getPercentage() + productDetails.getServiceTax().getPercentage();
										 }
										 else{
											 tax1percentage = productDetails.getPurchaseTax1().getPercentage();
										 }
									 }
									 else{
										 tax1percentage = productDetails.getPurchaseTax1().getPercentage();
									 }
									 tax1 =	 getTaxDetails("IGST",LoginPresenter.globalTaxList,tax1percentage);
								 }
								 else{
									 tax1 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
								 }
								 
								 if(tax1!=null){
									 productDetails.setPurchaseTax1(tax1);
									 productDetails.setVat(tax1.getPercentage());

								 }
								 else{
									 gwtalert.alert("IGST "+tax1percentage +" does not exist in Tax Master!");
									 tax1 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
									 productDetails.setPurchaseTax1(tax1);
								 }
								 Tax tax2 = new Tax();
								 tax2 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
								 productDetails.setPurchaseTax2(tax2);
								 productDetails.setTax(tax2.getPercentage());

							}
							 if(productDetails.getPurchaseTax2()!=null && productDetails.getPurchaseTax2().getPercentage()!=0){

							 double tax2percentage = 0;
							 Tax tax2 = new Tax();
							 if(productDetails.getPurchaseTax2()!=null && productDetails.getPurchaseTax2().getPercentage()!=0){
								 if(productDetails.getPurchaseTax1().getTaxPrintName().trim().equals("CGST") || productDetails.getPurchaseTax2().getTaxPrintName().trim().equals("SGST")){
									 if(productDetails.getPurchaseTax1()!=null ){
										 tax2percentage = productDetails.getPurchaseTax2().getPercentage() + productDetails.getPurchaseTax1().getPercentage();
									 }
									 else{
										 tax2percentage = productDetails.getPurchaseTax1().getPercentage();
									 }
								 }
								 else{
									 tax2percentage = productDetails.getPurchaseTax1().getPercentage();
								 }
								 tax2 =	 getTaxDetails("IGST",LoginPresenter.globalTaxList,tax2percentage);
							 }
							 else{
								 tax2 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
							 }
							 if(tax2!=null){
								 productDetails.setPurchaseTax2(tax2);
								 productDetails.setTax(tax2.getPercentage());
							 }
							 else{
								 gwtalert.alert("IGST "+tax2percentage +" does not exist in Tax Master!");
								 tax2 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
								 productDetails.setPurchaseTax2(tax2);

							 }
							 Tax tax = new Tax();
							 tax =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
							 productDetails.setPurchaseTax1(tax);
							 productDetails.setVat(tax.getPercentage());

					  	}	
							 
						}
						else{
							
							System.out.println("branch as company same state");
							 if(productDetails.getPurchaseTax1()!=null && productDetails.getPurchaseTax1().getPercentage()!=0){
								 Tax tax1 = new Tax();
								 double tax1percentage = 0;
								 if(productDetails.getPurchaseTax1()!=null && productDetails.getPurchaseTax1().getPercentage()!=0){
									 if(productDetails.getPurchaseTax1().getTaxPrintName().trim().equals("IGST")){
											 tax1percentage = productDetails.getPurchaseTax1().getPercentage()/2;
									 }
									 else{
										 tax1percentage = productDetails.getPurchaseTax1().getPercentage();
									 }
									 tax1 =	 getTaxDetails("CGST",LoginPresenter.globalTaxList,tax1percentage);
								 }
								 else{
									 tax1 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
								 }
								 if(tax1!=null){
									 if(productDetails.getPurchaseTax1().getTaxPrintName().trim().equals("IGST") && 
											 tax1.getTaxPrintName().trim().equals("CGST")){
										 Tax tax2 = new Tax();
										 tax2 =	 getTaxDetails("SGST",LoginPresenter.globalTaxList,tax1.getPercentage());
										 productDetails.setPurchaseTax2(tax2);

									 }
									 productDetails.setPurchaseTax1(tax1);
									 productDetails.setVat(tax1.getPercentage());

								 }
								 else{
									 gwtalert.alert("CGST "+tax1percentage +" does not exist in Tax Master!");
									 tax1 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
									 productDetails.setPurchaseTax1(tax1);
									 
									 Tax tax2 = new Tax();
									 tax2 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
									 productDetails.setPurchaseTax2(tax2);
									 productDetails.setTax(tax2.getPercentage());
								 }
								 
							 }
							 if(productDetails.getPurchaseTax2()!=null && productDetails.getPurchaseTax2().getPercentage()!=0){

								 double tax2percentage = 0;
								 Tax tax2 = new Tax();
								 if(productDetails.getPurchaseTax2()!=null && productDetails.getPurchaseTax2().getPercentage()!=0){
									 if(productDetails.getPurchaseTax2().getTaxPrintName().trim().equals("IGST")){
										 if(productDetails.getPurchaseTax2()!=null ){
											 tax2percentage = productDetails.getPurchaseTax2().getPercentage()/2;

										 }
									 }
									 else{
										 tax2percentage = productDetails.getPurchaseTax2().getPercentage();
									 }
									 tax2 =	 getTaxDetails("SGST",LoginPresenter.globalTaxList,tax2percentage);
								 }
								 else{
									 tax2 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
								 }
								 if(tax2!=null){
									 if(productDetails.getPurchaseTax2().getTaxPrintName().trim().equals("IGST") && 
											 tax2.getTaxPrintName().trim().equals("SGST")){
										 Tax tax1 = new Tax();
										 tax1 =	 getTaxDetails("CGST",LoginPresenter.globalTaxList,tax2.getPercentage());
										 productDetails.setPurchaseTax1(tax1);
										 productDetails.setVat(tax1.getPercentage());
									 }
									 productDetails.setPurchaseTax2(tax2);
									 productDetails.setTax(tax2.getPercentage());
								 }
								 else{
									 gwtalert.alert("SGST "+tax2percentage +"does not exist in Tax Master!");
									 tax2 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
									 productDetails.setPurchaseTax2(tax2);
									 
									 Tax tax = new Tax();
									 tax =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
									 productDetails.setPurchaseTax1(tax);
								 }
							 }
							 
						}
					}
				}
				else{
					if(vendorState2!=null && !vendorState2.equals("") 
							&& LoginPresenter.company!=null && LoginPresenter.company.getAddress().getState()!=null && !LoginPresenter.company.getAddress().getState().equals("") ){
						
						if(!vendorState2.trim().equals(LoginPresenter.company.getAddress().getState().trim())){
							
							if(productDetails.getPurchaseTax1()!=null && productDetails.getPurchaseTax1().getPercentage()!=0){
								Tax tax1 = new Tax();
								double tax1percentage = 0;
								if(productDetails.getPurchaseTax1()!=null && productDetails.getPurchaseTax1().getPercentage()!=0){
									 if(productDetails.getPurchaseTax1().getTaxPrintName().trim().equals("CGST") || productDetails.getPurchaseTax1().getTaxPrintName().trim().equals("SGST")){
										 if(productDetails.getServiceTax()!=null ){
											 tax1percentage = productDetails.getPurchaseTax1().getPercentage() + productDetails.getPurchaseTax2().getPercentage();
										 }
										 else{
											 tax1percentage = productDetails.getPurchaseTax1().getPercentage();
										 }
									 }
									 else{
										 tax1percentage = productDetails.getPurchaseTax1().getPercentage();
									 }
									 tax1 =	 getTaxDetails("IGST",LoginPresenter.globalTaxList,tax1percentage);
								 }
								 else{
									 tax1 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
								 }
								 
								 if(tax1!=null){
									 productDetails.setPurchaseTax1(tax1);
									 productDetails.setVat(tax1.getPercentage());
								 }
								 else{
									 gwtalert.alert("IGST "+tax1percentage +" does not exist in Tax Master!");
									 tax1 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
									 productDetails.setPurchaseTax1(tax1);
									 productDetails.setVat(tax1.getPercentage());
								 }
								 Tax tax2 = new Tax();
								 tax2 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
								 productDetails.setPurchaseTax2(tax2);
								 productDetails.setTax(tax2.getPercentage());
							}
							 if(productDetails.getPurchaseTax2()!=null && productDetails.getPurchaseTax2().getPercentage()!=0){

							 double tax2percentage = 0;
							 Tax tax2 = new Tax();
							 if(productDetails.getPurchaseTax2()!=null && productDetails.getPurchaseTax2().getPercentage()!=0){
								 if(productDetails.getPurchaseTax1().getTaxPrintName().trim().equals("CGST") || productDetails.getPurchaseTax2().getTaxPrintName().trim().equals("SGST")){
									 if(productDetails.getPurchaseTax1()!=null ){
										 tax2percentage = productDetails.getPurchaseTax2().getPercentage() + productDetails.getPurchaseTax1().getPercentage();
									 }
									 else{
										 tax2percentage = productDetails.getPurchaseTax1().getPercentage();
									 }
								 }
								 else{
									 tax2percentage = productDetails.getPurchaseTax1().getPercentage();
								 }
								 tax2 =	 getTaxDetails("IGST",LoginPresenter.globalTaxList,tax2percentage);
							 }
							 else{
								 tax2 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
							 }
							 if(tax2!=null){
								 productDetails.setPurchaseTax2(tax2);
								 productDetails.setTax(tax2.getPercentage());
							 }
							 else{
								 gwtalert.alert("IGST "+tax2percentage +" does not exist in Tax Master!");
								 tax2 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
								 productDetails.setPurchaseTax2(tax2);
								 productDetails.setTax(tax2.getPercentage());
							 }
							 Tax tax = new Tax();
							 tax =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
							 productDetails.setPurchaseTax1(tax);
							 productDetails.setVat(tax.getPercentage());
					  	}	
						
								 
						}
						else{
							 if(productDetails.getPurchaseTax1()!=null && productDetails.getPurchaseTax1().getPercentage()!=0){
								 Tax tax1 = new Tax();
								 double tax1percentage = 0;
								 if(productDetails.getPurchaseTax1()!=null && productDetails.getPurchaseTax1().getPercentage()!=0){
									 if(productDetails.getPurchaseTax1().getTaxPrintName().trim().equals("IGST")){
											 tax1percentage = productDetails.getPurchaseTax1().getPercentage()/2;
									 }
									 else{
										 tax1percentage = productDetails.getPurchaseTax1().getPercentage();
									 }
									 tax1 =	 getTaxDetails("CGST",LoginPresenter.globalTaxList,tax1percentage);
								 }
								 else{
									 tax1 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
								 }
								 if(tax1!=null){
									 if(productDetails.getPurchaseTax1().getTaxPrintName().trim().equals("IGST") && 
											 tax1.getTaxPrintName().trim().equals("CGST")){
										 Tax tax2 = new Tax();
										 Console.log("tax2.getPercentage() "+tax1.getPercentage());
										 tax2 =	 getTaxDetails("SGST",LoginPresenter.globalTaxList,tax1.getPercentage());
										 Console.log("tax2"+tax2);
										 productDetails.setPurchaseTax2(tax2);
										 productDetails.setTax(tax2.getPercentage());

									 }
									 productDetails.setPurchaseTax1(tax1);
									 productDetails.setVat(tax1.getPercentage());
								 }
								 else{
									 gwtalert.alert("CGST "+tax1percentage +" does not exist in Tax Master!");
									 tax1 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
									 productDetails.setPurchaseTax1(tax1);
									 productDetails.setVat(tax1.getPercentage());
									 
									 Tax tax2 = new Tax();
									 tax2 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
									 productDetails.setPurchaseTax2(tax2);
									 productDetails.setTax(tax2.getPercentage());
								 }
								 
							 }
							 if(productDetails.getPurchaseTax2()!=null && productDetails.getPurchaseTax2().getPercentage()!=0){

								 double tax2percentage = 0;
								 Tax tax2 = new Tax();
								 if(productDetails.getPurchaseTax2()!=null && productDetails.getPurchaseTax2().getPercentage()!=0){
									 if(productDetails.getPurchaseTax2().getTaxPrintName().trim().equals("IGST")){
										 if(productDetails.getPurchaseTax2()!=null ){
											 tax2percentage = productDetails.getPurchaseTax2().getPercentage()/2;

										 }
									 }
									 else{
										 tax2percentage = productDetails.getPurchaseTax2().getPercentage();
									 }
									 tax2 =	 getTaxDetails("SGST",LoginPresenter.globalTaxList,tax2percentage);
								 }
								 else{
									 tax2 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
								 }
								 if(tax2!=null){
									 if(productDetails.getPurchaseTax2().getTaxPrintName().trim().equals("IGST") && 
											 tax2.getTaxPrintName().trim().equals("SGST")){
										 Tax tax1 = new Tax();
										 tax1 =	 getTaxDetails("CGST",LoginPresenter.globalTaxList,tax2.getPercentage());
										 productDetails.setPurchaseTax1(tax1);
										 productDetails.setVat(tax1.getPercentage());

									 }
									 productDetails.setPurchaseTax2(tax2);
									 productDetails.setTax(tax2.getPercentage());
								 }
								 else{
									 gwtalert.alert("SGST "+tax2percentage +"does not exist in Tax Master!");
									 tax2 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
									 productDetails.setPurchaseTax2(tax2);
									 productDetails.setTax(tax2.getPercentage());
									 
									 Tax tax = new Tax();
									 tax =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
									 productDetails.setPurchaseTax2(tax);
									 productDetails.setTax(tax.getPercentage());
								 }
							 }
							 
						}
		
						
					}
				
				}
				
		}
		
		
		
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		form.productTablePO.getDataprovider().setList(list);
		form.productTablePO.getTable().redraw();
	
	
	
	}

	private static Tax getTaxDetails(String taxNamePrintName,ArrayList<TaxDetails> globalTaxList, double percentage) {

		
		try {
		
		for(TaxDetails taxDetails : globalTaxList){
			if(taxDetails.getTaxChargeStatus() && taxDetails.getTaxPrintName().trim().equalsIgnoreCase(taxNamePrintName.trim()) && percentage==taxDetails.getTaxChargePercent()){
				Tax tax = new Tax();
				tax.setPercentage(taxDetails.getTaxChargePercent());
				tax.setTaxConfigName(taxDetails.getTaxChargeName());
				tax.setTaxName(taxDetails.getTaxChargeName());
				tax.setTaxPrintName(taxDetails.getTaxPrintName());
				return tax;
			}
		}
		
		
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	
		return null;
	}


	private void setEmailPopUpData() {
		form.showWaitSymbol();

		String branchName = form.branch.getValue();
		String fromEmailId = AppUtility.getFromEmailAddress(branchName,model.getEmployee());
		List<VendorDetails> vendorlist = form.vendorTablePO.getDataprovider().getList();
		List<ContactPersonIdentification> vendorDetailslist = AppUtility.getvenodrEmailId(vendorlist,false);
		System.out.println("vendorDetailslist "+vendorDetailslist);
		label : AppUtility.loadContactPersonlist(emailpopup.olbContactlistEmailId,"Vendor",-1,"","",vendorDetailslist);
		Screen screenName=(Screen) AppMemory.getAppMemory().currentScreen;
		Console.log("screenName "+screenName);
		AppUtility.makeLiveSmsTemplateConfig(emailpopup.oblTemplateName,AppConstants.PURCHASEMODULE.trim(),screenName);
		emailpopup.taFromEmailId.setValue(fromEmailId);
		emailpopup.smodel = model;
		form.hideWaitSymbol();
	}
	
	@Override
	public void reactOnSMS() {
		smspopup.showPopUp();
		loadSMSPopUpData();
	}

	private void loadSMSPopUpData() {
		form.showWaitSymbol();
		
//		String customerCellNo = model.getCinfo().getCellNumber()+"";
//		String customerName = model.getCinfo().getFullName();
//		if(!customerName.equals("") && !customerCellNo.equals("")){
//			AppUtility.loadContactPersonlist(smspopup.olbContactlistEmailId,"Customer",model.getCinfo().getCount(),customerName,customerCellNo,null,true);
//		}
		
		List<VendorDetails> vendorlist = form.vendorTablePO.getDataprovider().getList();
		List<ContactPersonIdentification> vendorDetailslist = AppUtility.getvenodrEmailId(vendorlist,false);
		AppUtility.loadContactPersonlist(smspopup.olbContactlistEmailId,"Vendor",-1,"","",vendorDetailslist,true);

		Screen screenName=(Screen) AppMemory.getAppMemory().currentScreen;
		Console.log("screenName "+screenName);
		AppUtility.makeLiveSmsTemplateConfig(smspopup.oblsmsTemplate,LoginPresenter.currentModule.trim(),screenName,true,AppConstants.SMS);
		smspopup.getRbSMS().setValue(true);
		smspopup.smodel = model;
		form.hideWaitSymbol();
	}
	
	
	//Ashwini Patil Date:31-01-2023
	private void reactOnCopy(){
		try {
			Console.log("get id : " +model.getCount());
			final PurchaseOrder object=new PurchaseOrder();
			object.setCount(0);
			object.setApprovalDate(null);
			object.setCreationDate(null);
			object.setStatus(Quotation.CREATED);
			object.setId(null);
			
			//Header
			if(model.getPOName()!=null)
				object.setPOName(model.getPOName());			
			object.setPODate(model.getPODate());
			object.setDeliveryDate(model.getDeliveryDate());
			object.setBranch(model.getBranch());
			object.setEmployee(model.getEmployee());
			if(model.getAuthorisedBy()!=null)
			object.setAuthorisedBy(model.getAuthorisedBy());			
			object.setApproverName(model.getApproverName());
			if(model.getRemark()!=null)
			object.setRemark(model.getRemark());
			
			//Product table
			object.setProductDetails(model.getProductDetails());
			if (model.getTotalAmount() != 0)
				object.setTotalAmount(model.getTotalAmount());
			if (model.getOtherCharges() != null&&model.getOtherCharges().size()!=0){
				object.setOtherCharges(model.getOtherCharges());
			}
			object.setGtotal(model.getGtotal());			
			if (model.getProductTaxes() != null)
				object.setProductTaxes(model.getProductTaxes());							
			if (model.getProductCharges() != null)
				object.setProductCharges(model.getProductCharges());
			object.setTotal(model.getTotal());
			object.setNetpayble(model.getNetpayble());				
			
			if(model.getTotalFinalAmount()!=0){
				object.setTotalFinalAmount(model.getTotalFinalAmount());
			}
			if(model.getRoundOffAmount()!=0){
				object.setRoundOffAmount(model.getRoundOffAmount());
			}
			
			if(model.getVendorMargins()!=null){
				object.setVendorMargins(model.getVendorMargins());
			}
			if(model.getOtherChargesMargins()!=null){
				object.setOtherChargesMargins(model.getOtherChargesMargins());
			}
			
			//Vendor
			if(model.getVendorDetails()!=null)
				object.setVendorDetails(model.getVendorDetails());
			
			
			//Delivery
			if(model.getTermsOfDelivery()!=null)
				object.setTermsOfDelivery(model.getTermsOfDelivery());
			object.setDeliverAdd(model.isDeliverAdd());
			if(model.getPocName()!=null){
				object.setPocName(model.getPocName());
			}			
			if(model.getPocNumber()!=0){
				object.setPocNumber(model.getPocNumber());
			}
			object.setAdress(model.getAdress());
			
			//payment terms
			if(model.getPaymentMethod()!=null)
				object.setPaymentMethod(model.getPaymentMethod());
			if(model.getCreditDays()!=null){
				object.setCreditDays(model.getCreditDays());
			}
			if(model.getCreditDaysComment() != null){
				object.setCreditDaysComment(model.getCreditDaysComment());
			}
			if(model.getPaymentTermsList()!=null)
				object.setPaymentTermsList(model.getPaymentTermsList());
			
			//classification
			if(model.getLOIGroup()!=null)
				object.setLOIGroup(model.getLOIGroup());
			if(model.getLOItype()!=null)
				object.setLOItype(model.getLOItype());
			
			//Attachment
			if (model.getHeader() != null)
				object.setHeader(model.getHeader());
			if (model.getFooter() != null)
				object.setFooter(model.getFooter());
			if (model.getUptestReport() != null)
				object.setUptestReport(model.getUptestReport());
			
			
			//reference
			
			object.setLOIId(model.getLOIId());
			object.setRFQID(model.getRFQID());			
			if (model.getLOIDate() != null)
				object.setLOIDate(model.getLOIDate());
			if (model.getRFQDate() != null)
				object.setRFQDate(model.getRFQDate());			
			if(model.getRefOrderNO()!=null)
				object.setRefOrderNO(model.getRefOrderNO());			
			if (model.getResponseDate() != null)
				object.setResponseDate(model.getResponseDate());
			object.setPurchaseReqNo(model.getPurchaseReqNo());
			if(model.getPrDate()!=null)
				object.setPrDate(model.getPrDate());
			if (model.getReferenceDate()!= null)
				object.setReferenceDate(model.getReferenceDate());
			if (model.getDescription() != null)
				object.setDescription(model.getDescription());
			if (model.getProject() != null)
				object.setProject(model.getProject());
			if(model.getAssignTo1()!=null){
				object.setAssignTo1(model.getAssignTo1());
			}
			if(model.getAssignTo2()!=null){
				object.setAssignTo2(model.getAssignTo2());
			}
			
			
			
			
			AppMemory.getAppMemory().currentState=ScreeenState.NEW;
			form.setToNewState();
			
			Timer timer = new Timer() {
				
				@Override
				public void run() {
					// TODO Auto-generated method stub
					
					
					form.updateView(object);
					form.setToEditState();
					form.toggleAppHeaderBarMenu();
					
			
				}
			};
			timer.schedule(2000);
			
		} 
		catch (Exception e) {
			System.out.println(e.getMessage());
		}
}
}
