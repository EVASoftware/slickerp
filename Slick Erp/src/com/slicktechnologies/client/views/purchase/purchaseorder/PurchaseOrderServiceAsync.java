package com.slicktechnologies.client.views.purchase.purchaseorder;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;

public interface PurchaseOrderServiceAsync {
	void approvalTransaction(PurchaseOrder purchaseorder, AsyncCallback<Void> callback);
}
