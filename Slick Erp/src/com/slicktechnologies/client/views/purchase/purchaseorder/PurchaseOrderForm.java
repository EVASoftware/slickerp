package com.slicktechnologies.client.views.purchase.purchaseorder;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.LongBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.AddressComposite;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.composite.UploadComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.approvalutility.ApprovableFormScreen;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.contract.OtherChargesTable;
import com.slicktechnologies.client.views.contract.PaymentTermsTable;
import com.slicktechnologies.client.views.contract.ProductChargesTable;
import com.slicktechnologies.client.views.contract.ProductTaxesTable;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.client.views.purchase.letterofintent.ProductTableLOI;
import com.slicktechnologies.client.views.purchase.servicepo.PaymentTermsTbl;
import com.slicktechnologies.client.views.salesquotation.VendorAndOtherChargesMarginPopup;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.OtherTaxCharges;
import com.slicktechnologies.shared.ProductOtherCharges;
import com.slicktechnologies.shared.TaxDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.PriceList;
import com.slicktechnologies.shared.common.businessprocesslayer.PriceListDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetailsPO;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessprocesslayer.VendorDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.productlayer.Tax;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.salesorder.OtherCharges;
import com.slicktechnologies.shared.common.salesorder.OtherChargesMargin;
import com.slicktechnologies.shared.common.salesorder.VendorMargin;

public class PurchaseOrderForm extends ApprovableFormScreen<PurchaseOrder> implements ChangeHandler, ClickHandler {
	final GenricServiceAsync async = GWT.create(GenricService.class);

	public TextBox ibreqforQuotation, ibPOId;
	TextBox purchaseRequisiteNo;
	
	public TextBox ibLetterOfIntentID; 
	public DateBox dbRfqDate, dbExpectedResposeDate,dbLOIDate, dbPODate,dbrefDate;
	DateBox dbExpdeliverydate;
	DateBox prDate;
	TextBox ibRefOrderNo;
	TextArea tadescription;
	IntegerBox ibcreditdays;
	ObjectListBox<ConfigCategory> oblPurchaseOrderGroup;
	ObjectListBox<Type> oblPurchaseOrderType;

	public TextBox tbStatus;
	TextBox tbcommentForStatusChange;
	public TextBox tbpoName;
	public TextArea header, footer;
	
	public ProductTablePO productTablePO;
	VendorTablePO vendorTablePO;
	ObjectListBox<Config> olbcPaymentMethods;
	PaymentTermsTbl paymentTermsTable;
	Button addPaymentTerms;
	
	IntegerBox ibdays;
	DoubleBox dopercent;
	TextBox tbcomment;
	
	ObjectListBox<PriceList> veninfo;
	
	ObjectListBox<Employee> oblEmployee;
	ObjectListBox<Branch> branch;
	ObjectListBox<Employee> tbApporverName;
	ObjectListBox<HrProject> project;
	
	UploadComposite upload;
	AddressComposite deliveryadd;
	CheckBox cbadds;
	Label lbtotal;
	public DoubleBox dbTotal;

	ProductTaxesTable producttaxtable;
	ProductChargesTable productchargestable;
	
	Button addOtherCharges;
	TextBox tbblanck;
	FormField fgroupingLetterInformation;
	
	/**
	 * date 18-7-2018
	 * by jayshree
	 */
	
	TextArea tbtersOfDelivery;

	/***********************/

	public ProductInfoComposite prodInfoComposite;
	public Button addproducts;
	
	
	/**
	 * Date : 09-07-2018 BY ANIL
	 * commented cform and cst % logic
	 */
//	ObjectListBox<TaxDetails> olbcstpercent;
//	ListBox cbcformlis;
	
	DoubleBox dototalamt, doamtincltax, donetpayamt;
	PurchaseOrder purchaseOrderobj;
	
	
	/**
	 * This list provides the rate list of all vendor supplying the product .
	 * Date : 12-09-2016 By Anil
	 * Release 31 Aug 2016
	 * Project:PURCHASE ERROR SOLVING (NBHC)
	 */
	public ArrayList<PriceListDetails> vendorRateList=new ArrayList<PriceListDetails>();
	
	
	/**
	 * Purchase Engg. assigned PR to its team & vendor composite;
	 * Date : 29-09-2016 By Anil
	 * Release :30 Sept 2016
	 * Project : PURCHASE MODIFICATION (NBHC)
	 */
	ObjectListBox<Employee> olbAssignedTo1;
	ObjectListBox<Employee> olbAssignedTo2;
	PersonInfoComposite vendorComp;
	Button btnAddVendor;
	
	/**
	 * End
	 */
	
	/**
	 * Date 02 August 2017 added by vijay for Cform validation
	 */
	Date date = DateTimeFormat.getFormat("yyyy-MM-dd HH:mm:ss").parse("2017-07-01 00:00:00");
	final DateTimeFormat fmt = DateTimeFormat.getFormat("dd/MM/yyyy");

	/**
	 * ends here
	 */
	
	/**
	 * Date 08/06/2018
	 * Developer :- Vijay
	 * Des :- for final total amt Round off amt 
	 */
	DoubleBox dbfinalTotalAmt;
	DoubleBox dbfinalTotalAmtWithoutRoundoff;//Ashwini Patil Date:27-05-2023
	

	TextBox tbroundoffAmt;
	/**
	 * ends here
	 */
	/** date 13.10.2018 added by komal for sasha(Authorised by)**/
	ObjectListBox<Employee> olbAuthorisedBy;
	/** date 22.10.2018 added by komal **/
	ArrayList<WareHouse> globalWareHouseList =  new ArrayList<WareHouse>();
	
	/**
	 * Date : 24-11-2018 By Vijay
	 * Adding other charges table
	 */
	OtherChargesTable tblOtherCharges;
	/**
	 * End
	 */
	/** date 06.12.2018 added by komal **/
	TextBox tbCreditDaysComment;
	
	/***22-1-2019 added by amol for podate setenable true**/
	 boolean podateenabletrue=false ;
	 
	 /**Date 10-10-2019 by Amol added a two fields poc name and poc number**/
	 TextBox tbPocName;
	 LongBox iPocNumber;
	 
	 
	 /**
	 * @author Anil
	 * @since 20-07-2020
	 * For PTSPL raised by Rahul Tiwari
	 * calculating vendor and other charges margin
	 */
	public Button costingBtn;
	public boolean marginFlag;
	public List<VendorMargin> vendorMargins;
	public List<OtherChargesMargin> otherChargesMargins;
	public Double totalVendorMargin;
	public Double totalOcMargin;
	public VendorAndOcMarginPopupPO marginPopup=new VendorAndOcMarginPopupPO();
	Double totalOtherChargesSales,totalOtherChargesPO;
//	ListBox deliveryaddress;
	ObjectListBox<String> deliveryaddress;
	
	public PurchaseOrderForm() {
		super();
		createGui();
		productTablePO.connectToLocal();
		paymentTermsTable.connectToLocal();
		tbStatus.setValue(PurchaseOrder.CREATED);
		tbcommentForStatusChange.setEnabled(false);
		
//		olbcstpercent.setEnabled(false);
//		cbcformlis.setEnabled(false);
		
		tbblanck.setVisible(false);
		deliveryadd.setEnable(false);
		Company com = new Company();
		getCompanyAddress(com.getCompanyId());
		vendorTablePO.connectToLocal();
		/**22-1-2019 added by amol for po date enable true**/
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("PurchaseOrder", "PoDateEnableTrue")){
			podateenabletrue=true;
		}
		if(podateenabletrue){
			dbPODate.setEnabled(true);
		}else{
		dbPODate.setEnabled(false);
		}
		PurchaseOrderPresenter.vendorState="";
		/** date 22.10.2018 added by komal **/
		branch.addChangeHandler(this);
		
		Date date=new Date();
		dbPODate.setValue(date);
		
		marginFlag=AppUtility.checkForProcessConfigurartionIsActiveOrNot("PurchaseOrder","CalculateVendorAndOtherChargesMargin");
		marginPopup.getLblOk().addClickHandler(this);
		marginPopup.getLblCancel().addClickHandler(this);
	}

	
	public PurchaseOrderForm(String[] processlevel, FormField[][] fields,FormStyle formstyle) {
		super(processlevel, fields, formstyle);
		createGui();
		tbStatus.setValue(PurchaseOrder.CREATED);
		tbcommentForStatusChange.setEnabled(false);
		
//		olbcstpercent.setEnabled(false);
//		cbcformlis.setEnabled(false);
		
		
		deliveryadd.setEnable(false);
		Company com = new Company();
		getCompanyAddress(com.getCompanyId());
		if(podateenabletrue){
			dbPODate.setEnabled(true);
		}else{
		dbPODate.setEnabled(false);
		}
	}

	private void initalizeWidget() {

		ibPOId = new TextBox();
		ibPOId.setEnabled(false);
		ibLetterOfIntentID = new TextBox();
		ibLetterOfIntentID.addChangeHandler(this);
		ibreqforQuotation = new TextBox();
		ibreqforQuotation.addChangeHandler(this);
		purchaseRequisiteNo = new TextBox();
		purchaseRequisiteNo.addChangeHandler(this);
		ibRefOrderNo = new TextBox();
		tbpoName = new TextBox();
		header = new TextArea();
		footer = new TextArea();
		ibcreditdays=new IntegerBox();
		
		dbExpectedResposeDate = new DateBoxWithYearSelector();
		dbExpdeliverydate = new DateBoxWithYearSelector();
		dbRfqDate = new DateBoxWithYearSelector();
		dbRfqDate.setEnabled(false);
		dbLOIDate = new DateBoxWithYearSelector();
		dbLOIDate.setEnabled(false);
		prDate = new DateBoxWithYearSelector();
		prDate.setEnabled(false);
		
		dbPODate = new DateBoxWithYearSelector();
//		Date date = new Date();
//		dbPODate.setValue(date);
		
		if(podateenabletrue){
			dbPODate.setEnabled(true);
		}else{
		dbPODate.setEnabled(false);
		}
		
		dbrefDate=new DateBoxWithYearSelector();
		vendorTablePO = new VendorTablePO();
		productTablePO = new ProductTablePO();
		producttaxtable = new ProductTaxesTable();
		productchargestable = new ProductChargesTable();
		tadescription=new TextArea();
		oblEmployee = new ObjectListBox<Employee>();
		oblEmployee.makeEmployeeLive(AppConstants.PURCHASEMODULE, AppConstants.PURCHASEORDER, "Purchase Engineer");
//		initalizEmployeeNameListBox();
		
//		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Employee", "LoadEmployeeRoleWise")){
//			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Employee", "PurchaseEngineerPRPO")){
//				oblEmployee.setEmployeeToDropDown("PO Purchase Engineer");
//			}else{
//				oblEmployee.setEmployeeToDropDown("Purchase Engineer");
//			}
//		}else{
//			initalizEmployeeNameListBox();
//		}
		
		oblPurchaseOrderGroup = new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(oblPurchaseOrderGroup, Screen.PURCHASEORDERCATEGORY);
		oblPurchaseOrderGroup.addChangeHandler(this);
		oblPurchaseOrderType = new ObjectListBox<Type>();
		AppUtility.makeTypeListBoxLive(oblPurchaseOrderType,Screen.PURCHASEORDERTYPE);
		
		ibdays = new IntegerBox();
		dopercent = new DoubleBox();
		tbcomment = new TextBox();
		addPaymentTerms = new Button("ADD");
//		paymentTermsTable = new PaymentTermsTable();
		paymentTermsTable = new PaymentTermsTbl();
		
		branch = new ObjectListBox<Branch>();
		initalizEmployeeBranchListBox();
		tbApporverName = new ObjectListBox<Employee>();
		AppUtility.makeApproverListBoxLive(tbApporverName, "Purchase Order");
		
		project = new ObjectListBox<HrProject>();
		HrProject.MakeProjectListBoxLive(project);

		upload = new UploadComposite();
		olbcPaymentMethods=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbcPaymentMethods,Screen.PAYMENTMETHODS);

		lbtotal = new Label("Total");
		dbTotal = new DoubleBox();
		donetpayamt = new DoubleBox();
		addOtherCharges = new Button("+");

		doamtincltax = new DoubleBox();

		cbadds = new CheckBox();
		cbadds.setValue(false);
		deliveryadd = new AddressComposite(true);
		deliveryadd.setEnable(false);

//		cbcformlis = new ListBox();
//		cbcformlis.addItem("SELECT");
//		cbcformlis.addItem(AppConstants.YES);
//		cbcformlis.addItem(AppConstants.NO);
//		cbcformlis.setEnabled(false);
//		olbcstpercent = new ObjectListBox<TaxDetails>();
//		olbcstpercent.setEnabled(false);
//		initializePercentCst();
		
		
		addproducts = new Button("ADD");
		
		dototalamt=new DoubleBox();

		tbStatus = new TextBox();
		tbStatus.setEnabled(false);
		tbcommentForStatusChange=new TextBox();
		tbcommentForStatusChange.setEnabled(false);
		tbblanck = new TextBox();
		tbblanck.setVisible(false);
		/***Date-18-1-2019
		 @author Amol 
		 added salesproductcomposite for  adding only item product **/
		prodInfoComposite = AppUtility.initiateSalesProductComposite(new SuperProduct());
		
		olbAssignedTo1=new ObjectListBox<Employee>();
//		AppUtility.makeSalesPersonListBoxLive(olbAssignedTo1);
		olbAssignedTo1.makeEmployeeLive(AppConstants.PURCHASEMODULE, AppConstants.PURCHASEORDER, "Assign To 1");
		
		olbAssignedTo2=new ObjectListBox<Employee>();
//		AppUtility.makeSalesPersonListBoxLive(olbAssignedTo2);
		olbAssignedTo2.makeEmployeeLive(AppConstants.PURCHASEMODULE, AppConstants.PURCHASEORDER, "Assign To 2");
		
		MyQuerry querry = new MyQuerry();
		querry.setQuerryObject(new Vendor());
		Filter filter = new Filter();
		filter.setQuerryString("vendorStatus");
		filter.setBooleanvalue(true);
		querry.getFilters().add(filter);
		
		vendorComp = new PersonInfoComposite(querry, false);
		btnAddVendor = new Button("Add");
		
		/**
		 * Date 18-7-2018 by jayshree
		 */
		tbtersOfDelivery=new TextArea();
		
		dbfinalTotalAmt = new DoubleBox();
		dbfinalTotalAmt.setEnabled(false);
		dbfinalTotalAmtWithoutRoundoff=new DoubleBox();
		dbfinalTotalAmtWithoutRoundoff.setEnabled(false);
		
		tbroundoffAmt = new TextBox();
		tbroundoffAmt.addChangeHandler(this);
		/** date 13.10.2018 added by komal for sasha(Authorised by)**/
		olbAuthorisedBy = new ObjectListBox<Employee>();
		/**
		 * Date : 11-12-2018 BY ANIL
		 */
//		AppUtility.makeApproverListBoxLive(olbAuthorisedBy, "Purchase Order");
		olbAuthorisedBy.makeEmployeeLive(AppConstants.PURCHASEMODULE, AppConstants.PURCHASEORDER, "Authorised By");
		
		/**
		 * Date : 24-11-2018 By Vijay
		 * Adding other charges table
		 */
		tblOtherCharges=new OtherChargesTable();
		/**
		 * ends here
		 */
		/** date 06.12.2018 added by komal **/
		tbCreditDaysComment = new TextBox();
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("PurchaseOrder", "MonthwiseNumberGeneration")){
			ibRefOrderNo.setEnabled(false);
		}
		
		tbPocName=new TextBox();
		iPocNumber=new LongBox();
		
		costingBtn=new Button("Costing");
		costingBtn.addClickHandler(this);
		
		
		deliveryaddress = new ObjectListBox<String>();
		deliveryaddress.addChangeHandler(this);
		deliveryaddress.addItem("--SELECT--");
		deliveryaddress.addItem("Company Address");
		deliveryaddress.addItem("Branch Address");
		deliveryaddress.addItem("Warehouse Address");
		deliveryaddress.addItem("Other");

	}

	@Override
	public void createScreen() {
		marginFlag=AppUtility.checkForProcessConfigurartionIsActiveOrNot("PurchaseOrder","CalculateVendorAndOtherChargesMargin");
		
		initalizeWidget();
		/**
		 * Date : 19-04-2018 BY ANIL
		 * added view sales order and submit button
		 */
		/****Date23-1-2019 by amol for view GRN***/
		
		this.processlevelBarNames = new String[] {ManageApprovals.SUBMIT,
				ManageApprovals.APPROVALREQUEST,ManageApprovals.CANCELAPPROVALREQUEST, AppConstants.CANCELPURCHASEORDER,
				AppConstants.CANCELLATIONSUMMARY,"New" ,AppConstants.SENDMAILTOASSIGNPERSON,AppConstants.VIEWSALESORDER,AppConstants.VIEWGRN,AppConstants.COPY};
		
		
		String mainScreenLabel="Purchase Order";
		if(purchaseOrderobj!=null&&purchaseOrderobj.getCount()!=0){
			
			
			mainScreenLabel=purchaseOrderobj.getCount()+" "+"/"+" "+purchaseOrderobj.getStatus()+" "+"/"+" "+AppUtility.parseDate(purchaseOrderobj.getCreationDate());
		}
		 //fgroupingLetterInformation.getHeaderLabel().setText(mainScreenLabel);
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		fgroupingLetterInformation=fbuilder.setlabel(mainScreenLabel).widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).setKeyField(true).build();
		//FormField fgroupingLetterInformation = fbuilder.setlabel("Purchase Order").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();

		fbuilder = new FormFieldBuilder();
		FormField fgroupingReferenceDetails = fbuilder.setlabel("Reference/General").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();

		fbuilder = new FormFieldBuilder("LOI ID", ibLetterOfIntentID);
		FormField fibLetterOfIntentID = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("LOI Date.", dbLOIDate);
		FormField fdbLOIDate = fbuilder.setMandatory(false).setMandatoryMsg("LOI Date is mandatory").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("RFQ ID", ibreqforQuotation);
		FormField fibreqforQuotation = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("RFQ Date", dbRfqDate);
		FormField rfqdate = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("PR Number", purchaseRequisiteNo);
		FormField fpuchasereq = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("PR Date", prDate);
		FormField fpuchasereqdate = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/** date 12.10.2018 added by komal for header change **/
		String poId = "PO ID.";
		String refNo = "Reference Order No.";
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("PurchaseOrder", "POPDFV1")){
			poId = "Voucher No.";
			refNo = "Supplier's Ref./Order No.";
		}
		fbuilder = new FormFieldBuilder(refNo, ibRefOrderNo);
		FormField freferenceorderno = fbuilder.setMandatory(false).setMandatoryMsg("").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Project", project);
		FormField fproject = fbuilder.setMandatory(false).setMandatoryMsg("Project is mandatory").setRowSpan(0)
				.setColSpan(0).build();

		

		fbuilder = new FormFieldBuilder(poId, ibPOId);
		FormField fibPOId = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		fbuilder = new FormFieldBuilder("PO Title", tbpoName);
		FormField ftbpoName = fbuilder.setMandatory(false)
				.setMandatoryMsg("Title is mandatory").setRowSpan(0)
				.setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* PO Date", dbPODate);
		FormField fpodate = fbuilder.setMandatory(true)
				.setMandatoryMsg("PO Date is mandatory").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Credit Days", ibcreditdays);
		FormField fibcreditdays = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("Reference Date", dbrefDate);
		FormField fdbrefDate = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Expected Response Date",dbExpectedResposeDate);
		FormField fresponsedate = fbuilder.setMandatory(false)
				.setMandatoryMsg("Expected Response Date is mandatory")
				.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Expected Delivery Date",dbExpdeliverydate);
		FormField fexpdeldate = fbuilder.setMandatory(true)
				.setMandatoryMsg("Delivery date is mandatory").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("PO Type.", oblPurchaseOrderType);
		FormField floitype = fbuilder.setMandatory(false)
				.setMandatoryMsg("PO type is mandatory").setRowSpan(0)
				.setColSpan(0).build();
//		fbuilder = new FormFieldBuilder("PO Category.", oblPurchaseOrderGroup);
		
//		FormField floigroup = fbuilder.setMandatory(false)
//				.setMandatoryMsg("PO Category is mandatory").setRowSpan(0)
//				.setColSpan(0).build();
//		
//		
		FormField floigroup;
//		makeCategoryMandatory
		
		
		    /**29-12-2018 added by amol for po category mandatory**/
		
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("PurchaseOrder","makeCategoryMandatory" )){
			fbuilder = new FormFieldBuilder("* PO Category",oblPurchaseOrderGroup);
			floigroup=fbuilder.setMandatory(true).setMandatoryMsg("PO Category is Madatory").setRowSpan(0).setColSpan(0).build();
		}
		else{
			fbuilder = new FormFieldBuilder("PO Category ",oblPurchaseOrderGroup);
			floigroup=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
		
		
		
		
		
		
		
//		fbuilder = new FormFieldBuilder("* Branch", branch);
//		FormField fbranch = fbuilder.setMandatory(true)
//				.setMandatoryMsg("Branch is mandatory").setRowSpan(0)
//				.setColSpan(0).build();
		
		FormField fbranch=null;
		if(AppUtility.isBranchNonMandatory("PurchaseOrder")){
			fbuilder = new FormFieldBuilder("Branch", branch);
			fbranch = fbuilder.setMandatory(false).setMandatoryMsg("Branch is Mandatory!").setRowSpan(0)
					.setColSpan(0).build();
		}else{
			fbuilder = new FormFieldBuilder("* Branch", branch);
			fbranch = fbuilder.setMandatory(true).setMandatoryMsg("Branch is Mandatory!").setRowSpan(0)
					.setColSpan(0).build();
		}
		
		fbuilder = new FormFieldBuilder("* Purchase Engineer.", oblEmployee);
		FormField femployee = fbuilder.setMandatory(true)
				.setMandatoryMsg("Purchase Engineer is mandatory")
				.setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Approver Name", tbApporverName);
		FormField ftbApporverName = fbuilder.setMandatory(true)
				.setMandatoryMsg("Approver Name is mandatory").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Upload Document", upload);
		FormField fdoc = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(2).build();
		fbuilder = new FormFieldBuilder("Status", tbStatus);
		FormField ftbStatus = fbuilder.setMandatory(false)
				.setMandatoryMsg("Status is mandatory").setRowSpan(0)
				.setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Payment Method", olbcPaymentMethods);
		FormField folbcPaymentMethods = fbuilder.setMandatory(false)
				.setMandatoryMsg("Payment Method is mandatory").setRowSpan(0)
				.setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Remark", tbcommentForStatusChange);
		FormField ftbcommentForStatusChange = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		FormField fpaymenttemrms = fbuilder.setlabel("Payment Terms")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(5).build();
		
		FormField fClassification = fbuilder.setlabel("Classification")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(5).build();

		fbuilder = new FormFieldBuilder("Days", ibdays);
		FormField fibdays = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Percent", dopercent);
		FormField fdopercent = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Comment", tbcomment);
		FormField ftbcomment = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		fbuilder = new FormFieldBuilder("", addPaymentTerms);
		FormField faddpayment = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		fbuilder = new FormFieldBuilder("", paymentTermsTable.getTable());
		FormField fpaymenttermstable = fbuilder.setMandatory(true)
				.setMandatoryMsg("Payment Term  is mandatory").setRowSpan(0)
				.setColSpan(4).build();

		fbuilder = new FormFieldBuilder();
		FormField fproductInformation = fbuilder.setlabel("Product")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(5).build();

		fbuilder = new FormFieldBuilder("", productTablePO.getTable());
		FormField fproductTablePO = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(5).build();

		fbuilder = new FormFieldBuilder("Total", dbTotal);
		FormField fdbtl = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("", producttaxtable.getTable());
		FormField fproducttaxtable = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(3).build();

		fbuilder = new FormFieldBuilder("Total", doamtincltax);
		FormField fdoamtincltax = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("", productchargestable.getTable());
		FormField fchargestable = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(3).build();

		fbuilder = new FormFieldBuilder("", addOtherCharges);
		FormField faddOtherCharges = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Net Payable", donetpayamt);
		FormField fdonetpayamt = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("", vendorTablePO.getTable());
		FormField fvendortable = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(5).build();

		fbuilder = new FormFieldBuilder("", tbblanck);
		FormField fblanck = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("E-mail Header", header);
		FormField fheader = fbuilder.setMandatory(false)
				.setMandatoryMsg("Header is mandatory").setRowSpan(0)
				.setColSpan(5).build();

		fbuilder = new FormFieldBuilder("E-mail Footer", footer);
		FormField ffooter = fbuilder.setMandatory(false)
				.setMandatoryMsg("Footer is mandatory").setRowSpan(0)
				.setColSpan(5).build();

		fbuilder = new FormFieldBuilder("Is Delivery Address Different", cbadds);
		FormField fcbadds = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("", deliveryadd);
		FormField fdeliveryadd = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(5).build();

		fbuilder = new FormFieldBuilder("", prodInfoComposite);
		FormField fprodInfoComposite = fbuilder.setMandatory(false)
				.setRowSpan(0).setColSpan(3).build();
	
//		fbuilder = new FormFieldBuilder("C Form", cbcformlis);
//		FormField fcbcform = fbuilder.setMandatory(false).setRowSpan(0)
//				.setColSpan(0).build();
//		fbuilder = new FormFieldBuilder("CST Percent", olbcstpercent);
//		FormField folbcstpercent = fbuilder.setMandatory(false).setRowSpan(0)
//				.setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("", addproducts);
		FormField faddproducts = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingDescription = fbuilder.setlabel("Attachment").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		fbuilder = new FormFieldBuilder("Description (Max 500 characters)", tadescription);
		FormField ftadescription = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(5).build();
		
		
		fbuilder = new FormFieldBuilder();
		FormField fvendorTableGroup = fbuilder.setlabel("Vendor")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(5).build();
		
		fbuilder = new FormFieldBuilder("Assign To 1", olbAssignedTo1);
		FormField folbAssignedTo1 = fbuilder.setMandatory(false).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Assign To 2", olbAssignedTo2);
		FormField folbAssignedTo2 = fbuilder.setMandatory(false).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("", vendorComp);
		FormField fvendorComp = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("", btnAddVendor);
		FormField fbtaddVendor = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder();
		FormField fdeliveryDetails = fbuilder.setlabel("Delivery")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(5).build();
		
		/**
		 * Date 21-7-2018 by jayshree
		 */
		fbuilder = new FormFieldBuilder("Terms Of Delivery", tbtersOfDelivery);
		FormField ftbtersOfDelivery = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(5).build();
		
		
		
		 /* Ashwini Patil
		 * Date:26-05-2023
		 * Des :- for final Total amt without roundoff
		 */
		fbuilder = new FormFieldBuilder("Total Amount without round off",dbfinalTotalAmtWithoutRoundoff);
		FormField fdbfinalTotalAmtWithoutRoundoff = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		
		/**
		 * Date 08-06-2018 By vijay
		 * Des :- for final Total amt and roundoff amt
		 */
		fbuilder = new FormFieldBuilder("Total Amount",dbfinalTotalAmt);
		FormField fdbfinalTotalAmt = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Round Off",tbroundoffAmt);
		FormField ftbroundoffAmt = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		/**
		 * ends here
		 */
		/** date 13.10.2018 added by komal for sasha(Authorised by)**/
		fbuilder = new FormFieldBuilder("Authorised By",olbAuthorisedBy);
		FormField folbAuthorisedBy = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/**
		 * Date : 24-11-2018 By Vijay
		 * Adding other charges table
		 */
		fbuilder = new FormFieldBuilder("",tblOtherCharges.getTable());
		FormField fothChgTbl= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		/**
		 * ends here
		 */
		/*** date 06.12.2018 added by komal for credit days comment **/
		fbuilder = new FormFieldBuilder("Credit Days Comment", tbCreditDaysComment);
		FormField ftbCreditDaysComment= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder=new FormFieldBuilder("POC Name", tbPocName);
		FormField ftbPocName= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder=new FormFieldBuilder("POC Number", iPocNumber);
		FormField fiPocNumber= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("" , costingBtn);
		FormField fcostingBtn = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).setVisible(marginFlag).build();
		
		fbuilder = new FormFieldBuilder("Delivery Address", deliveryaddress);
		FormField fdeliveryaddress = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		FormField[][] formfield = {
//				{ fgroupingReferenceDetails },
//				{ fibLetterOfIntentID, fdbLOIDate, fibreqforQuotation, rfqdate,fpuchasereq },
//				{ fpuchasereqdate, freferenceorderno,fdbrefDate, fproject },
//				{ fgroupingLetterInformation },
//				{ fibPOId, ftbpoName, fpodate, fresponsedate,fexpdeldate },
//				{ floigroup, floitype,fbranch,femployee,ftbApporverName },
////				{ folbcPaymentMethods, fcbcform,folbcstpercent,ftbStatus,ftbcommentForStatusChange}, //vijay
//				{ folbAssignedTo1,folbAssignedTo2,folbcPaymentMethods,ftbStatus,ftbcommentForStatusChange}, //vijay
//				{ folbAuthorisedBy,fdoc },/** date 13.10.2018 added by komal for sasha(Authorised by)**/
//				{ fgroupingDescription},
//				
//				{ ftadescription},
//				{ fpaymenttemrms },
//				{ fibdays, fdopercent, ftbcomment, faddpayment },
//				{ fibcreditdays ,ftbCreditDaysComment},  //vijay
//				{ fpaymenttermstable },
//				{ fproductInformation }, {fcostingBtn},{ fprodInfoComposite, faddproducts },
//				{ fproductTablePO },
////				{ fblanck, fblanck, fblanck,fblanck, fdbtl },
////				{ fblanck, fblanck, fproducttaxtable },
////				{ fblanck, fblanck,fblanck, faddOtherCharges, fdoamtincltax },
////				{ fblanck, fblanck, fchargestable },
//				/** Date : 26-11-2018 By Vijay for Other Charges with taxes Update above old code commented**/
//				{ fblanck, fblanck, fblanck,faddOtherCharges, fdbtl },
//				{fblanck,fblanck,fothChgTbl},
//				{ fblanck, fblanck,fblanck,fblanck, fdoamtincltax },
//				{ fblanck, fblanck, fproducttaxtable },
//				/**
//				 * ends here
//				 */
//				{ fblanck, fblanck, fblanck,fblanck, fdbfinalTotalAmt }, 
//				{ fblanck, fblanck, fblanck,fblanck, ftbroundoffAmt }, 
//				{ fblanck, fblanck, fblanck,fblanck, fdonetpayamt }, 
//				{ fvendorTableGroup},
//				{ fvendorComp,fbtaddVendor},
//				{ fvendortable },
//				{ fheader }, { ffooter },
//				{fdeliveryDetails},
//				{ftbtersOfDelivery}, { fcbadds,ftbPocName,fiPocNumber }, { fdeliveryadd },	};
//
//		this.fields = formfield;
				/** Purchase Order **/
				{ fgroupingLetterInformation },
				{ ftbpoName, fpodate,fexpdeldate,fbranch},
				{ femployee,folbAuthorisedBy,ftbApporverName,ftbcommentForStatusChange},
				/** Product**/
				{ fproductInformation }, 
				{ fcostingBtn},
				{ fprodInfoComposite, faddproducts },
				{ fproductTablePO },
				{ fblanck, fblanck, fblanck,faddOtherCharges, fdbtl },
				{ fblanck,fblanck,fothChgTbl},
				{ fblanck, fblanck,fblanck,fblanck, fdoamtincltax },
				{ fblanck, fblanck, fproducttaxtable },
				{ fblanck, fblanck, fblanck,fblanck, fdbfinalTotalAmtWithoutRoundoff }, 
				{ fblanck, fblanck, fblanck,fblanck, fdbfinalTotalAmt }, 
				{ fblanck, fblanck, fblanck,fblanck, ftbroundoffAmt }, 
				{ fblanck, fblanck, fblanck,fblanck, fdonetpayamt }, 
				/** Vendor **/
				{ fvendorTableGroup},
				{ fvendorComp,fbtaddVendor},
				{ fvendortable },
				
				/**Delivery**/
				{ fdeliveryDetails},
				{ ftbtersOfDelivery},
//				{ fcbadds,ftbPocName,fiPocNumber },
				{ fdeliveryaddress,ftbPocName,fiPocNumber },
				{ fdeliveryadd },
				/**Payment Detail**/
				{ fpaymenttemrms },
				{ folbcPaymentMethods, fibcreditdays ,ftbCreditDaysComment},  //vijay
				{ fibdays, fdopercent, ftbcomment, faddpayment },
				{ fpaymenttermstable },
				/**Classification**/
				{ fClassification},
				{ floigroup, floitype},
				 //vijay
				/**Reference**/
				{ fgroupingReferenceDetails },
				{ fibLetterOfIntentID, fdbLOIDate, fibreqforQuotation, rfqdate },
				{ fpuchasereq,fpuchasereqdate, freferenceorderno,fdbrefDate},
				{ fproject,fresponsedate,folbAssignedTo1,folbAssignedTo2},
				{ ftadescription},
				/**Attachment**/
				{ fgroupingDescription},
				{ fheader },
				{ ffooter },
				{ fdoc }
		};

		this.fields = formfield;

	}

	
	@Override
	public void updateModel(PurchaseOrder model) {
		if (!ibreqforQuotation.getValue().equals(""))
			model.setRFQID(Integer.parseInt(ibreqforQuotation.getValue()));
		if (!ibLetterOfIntentID.getValue().equals(""))
			model.setLOIId(Integer.parseInt(ibLetterOfIntentID.getValue()));
		if (tbpoName.getValue() != null)
			model.setPOName(tbpoName.getValue());
		if (dbLOIDate.getValue() != null)
			model.setLOIDate(dbLOIDate.getValue());
		if (dbRfqDate.getValue() != null)
			model.setRFQDate(dbRfqDate.getValue());
		if (dbExpectedResposeDate.getValue() != null)
			model.setResponseDate(dbExpectedResposeDate.getValue());
		if (ibRefOrderNo.getValue() != null)
			model.setRefOrderNO(ibRefOrderNo.getValue());
		if (oblEmployee.getValue() != null)
			model.setEmployee(oblEmployee.getValue());
		if (oblPurchaseOrderGroup.getValue() != null)
			model.setLOIGroup(oblPurchaseOrderGroup.getValue());
		if (oblPurchaseOrderType.getValue() != null)
			model.setLOItype(oblPurchaseOrderType.getValue(oblPurchaseOrderType.getSelectedIndex()));
		if (productTablePO.getValue() != null)
			model.setProductDetails(productTablePO.getValue());
		if (vendorTablePO.getValue() != null)
			model.setVendorDetails(vendorTablePO.getValue());
		if (tbStatus.getValue() != null)
			model.setStatus(tbStatus.getValue());
		if (dbTotal.getValue() != 0)
			model.setTotalAmount(dbTotal.getValue());
		if (paymentTermsTable.getValue() != null)
			model.setPaymentTermsList(paymentTermsTable.getValue());
		if (dbPODate.getValue() != null)
			model.setPODate(dbPODate.getValue());
		if (branch.getValue() != null)
			model.setBranch(branch.getValue());
		if (header.getValue() != null)
			model.setHeader(header.getValue());
		if (footer.getValue() != null)
			model.setFooter(footer.getValue());
		if (tbApporverName.getValue() != null)
			model.setApproverName(tbApporverName.getValue());
		if (project.getValue() != null)
			model.setProject(project.getValue());
		if (!purchaseRequisiteNo.getValue().equals(""))
			model.setPurchaseReqNo(Integer.parseInt(purchaseRequisiteNo.getValue()));
		if (dbExpdeliverydate.getValue() != null)
			model.setDeliveryDate(dbExpdeliverydate.getValue());
		if (dbrefDate.getValue() != null)
			model.setReferenceDate(dbrefDate.getValue());
		if (upload.getValue() != null)
			model.setUptestReport(upload.getValue());
		if (deliveryadd.getValue() != null)
			model.setAdress(deliveryadd.getValue());
		if (producttaxtable.getValue() != null)
			model.setProductTaxes(producttaxtable.getValue());
		if (productchargestable.getValue() != null)
			model.setProductCharges(productchargestable.getValue());
		if (prDate.getValue() != null)
			model.setPrDate(prDate.getValue());
		
		if(tbPocName.getValue()!=null){
			model.setPocName(tbPocName.getValue());
		}
		
		if(iPocNumber.getValue()!=null){
			model.setPocNumber(iPocNumber.getValue());
		}else{
			model.setPocNumber(0);
		}
		Console.log("POC Number"+iPocNumber.getValue());
		
		
		model.setDeliverAdd(cbadds.getValue());
		
		
//		if (cbcformlis.getSelectedIndex()!=0){
//			model.setcForm(cbcformlis.getValue(cbcformlis.getSelectedIndex()));
//		}else{
//			model.setcForm("");
//		}
//		
//		if (olbcstpercent.getValue()!=null){
//			TaxDetails cstPerc=olbcstpercent.getSelectedItem();
//			model.setCstpercent(cstPerc.getTaxChargePercent());
//		}
//		if(olbcstpercent.getValue()!=null)
//		{
//			model.setCstName(olbcstpercent.getValue());
//		}
		
		if(ibcreditdays.getValue()!=null){
			model.setCreditDays(ibcreditdays.getValue());
		}else
			model.setCreditDays(0);
		
		if (tadescription.getValue() != null)
			model.setDescription(tadescription.getValue());
		
		
		
		if(olbcPaymentMethods.getValue()!=null)
			model.setPaymentMethod(olbcPaymentMethods.getValue());
		model.setTotal(dbTotal.getValue());
		model.setGtotal(doamtincltax.getValue());
		model.setNetpayble(donetpayamt.getValue());
		
		if(olbAssignedTo1.getValue()!=null){
			model.setAssignTo1(olbAssignedTo1.getValue());
		}
		
		if(olbAssignedTo2.getValue()!=null){
			model.setAssignTo2(olbAssignedTo2.getValue());
		}
		
		
		/**
		 * Date 18-7-2018 by jayshree
		 */
		
		if(tbtersOfDelivery.getValue()!=null){
			model.setTermsOfDelivery(tbtersOfDelivery.getValue());
		}
		
		/**
		 * Date 08/06/2018
		 * Developer :- Vijay
		 * Des :- for final total amt Round off amt 
		 */
		  if(dbfinalTotalAmt.getValue()!=null){
			  model.setTotalFinalAmount(dbfinalTotalAmt.getValue());;
		  }
		  if(tbroundoffAmt.getValue()!=null && !tbroundoffAmt.getValue().equals("")){
			  model.setRoundOffAmount(Double.parseDouble(tbroundoffAmt.getValue()));
		  }
		/**
		 * ends here
		 */
		  /** date 13.10.2018 added by komal for sasha(Authorised by)**/
		  if(olbAuthorisedBy.getSelectedIndex()!=0){
			  model.setAuthorisedBy(olbAuthorisedBy.getValue(olbAuthorisedBy.getSelectedIndex()));
		  }
		  
	    /**
		 * Date : 24-11-2017 BY Vijay
		 * Des :- for other charges with taxes
		 */
		List<OtherCharges> otherChargesList=this.tblOtherCharges.getDataprovider().getList();
		ArrayList<OtherCharges> otherChargesArr=new ArrayList<OtherCharges>();
		otherChargesArr.addAll(otherChargesList);
		model.setOtherCharges(otherChargesArr);
		
		/**
		 * End
		 */
		/*** date 06.12.2018 added by komal for credit days comment **/
		if(tbCreditDaysComment.getValue() != null){
			model.setCreditDaysComment(tbCreditDaysComment.getValue());
		}
		
		if(marginFlag){
			if(vendorMargins==null){
				vendorMargins=new ArrayList<VendorMargin>();
			}
			addPoOtherCharges();
			marginPopup.reactOnCostingSave(productTablePO.getValue(),vendorMargins,otherChargesMargins);
			totalVendorMargin=marginPopup.dbTotalVendorMargin.getValue();
			totalOcMargin=marginPopup.dbOtherChargesMargin.getValue();
			totalOtherChargesSales=marginPopup.dbTotalOcSales.getValue();
			totalOtherChargesPO=marginPopup.dbTotalOcPO.getValue();
			
			Console.log("Vendor "+vendorMargins.size());
			Console.log("Other "+otherChargesMargins.size());
		}
		if(vendorMargins!=null){
			model.setVendorMargins(vendorMargins);
		}
		if(otherChargesMargins!=null){
			model.setOtherChargesMargins(otherChargesMargins);
		}
		if(totalVendorMargin!=null){
			model.setTotalVendorMargin(totalVendorMargin);
		}
		if(totalOcMargin!=null){
			model.setTotalOtherChargesMargin(totalOcMargin);
		}
		
		if(totalOtherChargesSales!=null){
			model.setTotalOtherChargesSales(totalOtherChargesSales);
		}
		if(totalOtherChargesPO!=null){
			model.setTotalOtherChargesPO(totalOtherChargesPO);
		}
		if(deliveryaddress.getSelectedIndex()!=0){
			model.setDeliveryAddressName(deliveryaddress.getValue(deliveryaddress.getSelectedIndex()));
		}
		purchaseOrderobj=model;
	}

	@Override
	public void updateView(PurchaseOrder model) {
		
		purchaseOrderobj=model;
		ibPOId.setValue(model.getCount()+"");
		
		/**
		 * Date 20-04-2018 By vijay for orion pest locality load  
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Locality", "DontLoadAllLocalityAndCity")){
			setAddressDropDown();
		}
		/**
		 * ends here
		 */
		
		ibLetterOfIntentID.setValue(model.getLOIId()+"");
		ibreqforQuotation.setValue(model.getRFQID()+"");
		if (model.getLOIDate() != null)
			dbLOIDate.setValue(model.getLOIDate());
		if (model.getRFQDate() != null)
			dbRfqDate.setValue(model.getRFQDate());
		ibRefOrderNo.setValue(model.getRefOrderNO());
		if (model.getEmployee() != null)
			oblEmployee.setValue(model.getEmployee());
		if (model.getLOIGroup() != null)
			oblPurchaseOrderGroup.setValue(model.getLOIGroup());
		if (model.getLOItype() != null)
			oblPurchaseOrderType.setValue(model.getLOItype());
		
		if (model.getApproverName() != null)
			tbApporverName.setValue(model.getApproverName());
		Console.log("UPDATE VIEW LOG 2");
		if (model.getProductDetails() != null&&model.getProductDetails().size()!=0){
			productTablePO.setValue(model.getProductDetails());
			
			List<String> prodIdList=new ArrayList<String>();
			HashSet<String> uniqueCodeList=new HashSet<>();
			for(ProductDetailsPO prodDet:model.getProductDetails()){
				uniqueCodeList.add(prodDet.getProductCode());
			}
			prodIdList.addAll(uniqueCodeList);	
			loadVendorProductPriceList(prodIdList);
		}
		if (model.getVendorDetails() != null)
			vendorTablePO.setValue(model.getVendorDetails());
		if (model.getStatus() != null)
			tbStatus.setValue(model.getStatus());
		if (model.getRemark()!= null)
			tbcommentForStatusChange.setValue(model.getRemark());
		if (model.getResponseDate() != null)
			dbExpectedResposeDate.setValue(model.getResponseDate());
		if (model.getPOName() != null)
			tbpoName.setValue(model.getPOName());
		if (model.getTotalAmount() != 0)
			dbTotal.setValue(model.getTotalAmount());
		if (model.getPaymentTermsList() != null)
			paymentTermsTable.setValue(model.getPaymentTermsList());
		if (model.getPODate() != null)
			dbPODate.setValue(model.getPODate());
		if (model.getBranch() != null)
			branch.setValue(model.getBranch());
		if (model.getHeader() != null)
			header.setValue(model.getHeader());
		if (model.getFooter() != null)
			footer.setValue(model.getFooter());
	
		if (model.getProject() != null)
			project.setValue(model.getProject());
		purchaseRequisiteNo.setValue(model.getPurchaseReqNo()+"");
		if (model.getDeliveryDate() != null)
			dbExpdeliverydate.setValue(model.getDeliveryDate());
		if (model.getUptestReport() != null)
			upload.setValue(model.getUptestReport());
		if (model.getAdress() != null)
			deliveryadd.setValue(model.getAdress());
		
		
		/**
		 * Date : 24-11-2018 By Vijay
		 * for Other Charges
		 */
		if (model.getOtherCharges() != null&&model.getOtherCharges().size()!=0){
			tblOtherCharges.setValue(model.getOtherCharges());
		}
		/**
		 * End
		 */
		doamtincltax.setValue(model.getGtotal());
		
		if (model.getProductTaxes() != null)
			producttaxtable.setValue(model.getProductTaxes());
		
		
		
		
		if (model.getProductCharges() != null)
			productchargestable.setValue(model.getProductCharges());
		if (model.getPrDate() != null)
			prDate.setValue(model.getPrDate());
		if (model.getReferenceDate()!= null)
			dbrefDate.setValue(model.getReferenceDate());
		if(model.getPaymentMethod()!=null)
			olbcPaymentMethods.setValue(model.getPaymentMethod());
		cbadds.setValue(model.isDeliverAdd());
		
		if(model.getCreditDays()!=null){
			ibcreditdays.setValue(model.getCreditDays());
		}
		
		if (model.getDescription() != null)
			tadescription.setValue(model.getDescription());
		
		/**
		 * Date 18-7-2018 by jayshree
		 */
		if(model.getTermsOfDelivery()!=null)
		tbtersOfDelivery.setValue(model.getTermsOfDelivery());
		
		
//		olbcstpercent.setValue(model.getCstName());
//		if(model.getcForm()!=null){
//			for(int i=0;i<cbcformlis.getItemCount();i++)
//			{
//				String data=cbcformlis.getItemText(i);
//				if(data.equals(model.getcForm()))
//				{
//					cbcformlis.setSelectedIndex(i);
//					break;
//				}
//			}
//		}

		dbTotal.setValue(model.getTotal());
		
		donetpayamt.setValue(model.getNetpayble());
		
		if(model.getAssignTo1()!=null){
			olbAssignedTo1.setValue(model.getAssignTo1());
		}
		if(model.getAssignTo2()!=null){
			olbAssignedTo2.setValue(model.getAssignTo2());
		}
		
		if(this.validateVendorselect()){
			int vendorCount=retrieveVendorCount();
			retVendorState(vendorCount);
		}
		
		/**
		 * Date 08/06/2018
		 * Developer :- Vijay
		 * Des :- for final total amt Round off amt 
		 */
		
		if(model.getTotalFinalAmount()!=0){
			dbfinalTotalAmt.setValue(model.getTotalFinalAmount());
		}
		if(model.getRoundOffAmount()!=0){
			tbroundoffAmt.setValue(model.getRoundOffAmount()+"");
		}
		
		if(model.getPocName()!=null){
			tbPocName.setValue(model.getPocName());
		}
		
		if(model.getPocNumber()!=0){
			iPocNumber.setValue(model.getPocNumber());
		}
		
		
		/**
		 * ends here
		 */
		 /** date 13.10.2018 added by komal for sasha(Authorised by)**/
		if(model.getAuthorisedBy()!=null){
			olbAuthorisedBy.setValue(model.getAuthorisedBy());
		}
		
		
		
		/* 
		 * for approval process
		 *  nidhi
		 *  5-07-2017
		 */
		/*** date 06.12.2018 added by komal for credit days comment **/
		if(model.getCreditDaysComment() != null){
			tbCreditDaysComment.setValue(model.getCreditDaysComment());
		}
		
		if(model.getVendorMargins()!=null){
			vendorMargins=model.getVendorMargins();
		}
		if(model.getOtherChargesMargins()!=null){
			otherChargesMargins=model.getOtherChargesMargins();
		}
		
		
		totalVendorMargin=model.getTotalOtherChargesMargin();
		totalOcMargin=model.getTotalOtherChargesMargin();
		totalOtherChargesPO=model.getTotalOtherChargesPO();
		totalOtherChargesSales=model.getTotalOtherChargesSales();
		
		
		//Ashwini Patil Date:27-05-2023
		double totalIncludingTax = getDbTotal().getValue()+ getProducttaxtable().calculateTotalTaxes()+tblOtherCharges.calculateOtherChargesSum();
		double amtval = 0;
		if (getProductchargestable().getDataprovider().getList().size() == 0) {
			amtval = totalIncludingTax;
		}
		if (getProductchargestable().getDataprovider().getList().size() != 0) {
			amtval = totalIncludingTax+getProductchargestable().calculateNetPayable();
		}
		getDbfinalTotalAmtWithoutRoundoff().setValue(amtval);
		//end of comment
		
		
		if(model.getDeliveryAddressName()!=null && !model.getDeliveryAddressName().equals("")){
			deliveryaddress.setValue(model.getDeliveryAddressName());
		}
		
		if(presenter != null){
			presenter.setModel(model);
		}
		/*
		 *  end
		 */
	}
	
	   /**
		 * Date 20-04-2018 By vijay
		 * here i am refreshing address composite data if global list size greater than drop down list size
		 * because we are adding locality and city from an entity in view state to global list if doest not exist in global list 
		 */
		private void setAddressDropDown() {
			if(LoginPresenter.globalLocality.size()>deliveryadd.locality.getItems().size()	){
				deliveryadd.locality.setListItems(LoginPresenter.globalLocality);
			}
		}
		/**
		 * ends here 
		 */
	

	@Override
	public void toggleAppHeaderBarMenu() {
		if (AppMemory.getAppMemory().currentState == ScreeenState.NEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Save") || text.equals("Discard")
						|| text.equals("Search")|| text.equals(AppConstants.NAVIGATION)) {
					menus[k].setVisible(true);
				} else
					menus[k].setVisible(false);
			}
		}
		else if (AppMemory.getAppMemory().currentState == ScreeenState.EDIT) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Save") || text.equals("Discard")|| text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true);
				else
					menus[k].setVisible(false);
			}
		}

		else if (AppMemory.getAppMemory().currentState == ScreeenState.VIEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Discard") || text.equals("Edit")|| text.equals("Print") || text.equals("Search")|| text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true);
				else
					menus[k].setVisible(false);
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.PURCHASEORDER,LoginPresenter.currentModule.trim());
	}

	public void toggleProcessLevelMenu() {
		System.out.println("TOGGLE METHOD");
		PurchaseOrder entity = (PurchaseOrder) presenter.getModel();
		String status = entity.getStatus();
		System.out.println("STATUS "+status);

		for (int i = 0; i < getProcesslevelBarNames().length; i++) {
			InlineLabel label = getProcessLevelBar().btnLabels[i];
			String text = label.getText().trim();
			System.out.println("Text : "+text);
			if ((status.equals(PurchaseOrder.CREATED))) {
				System.out.println("CREATED ");
				/*****added by amol 25-1-2019***/
				if (text.equals(AppConstants.VIEWGRN))
					label.setVisible(false);
				if (text.equals(AppConstants.CANCELPURCHASEORDER))
					label.setVisible(false);
				if (text.equals(AppConstants.CANCELLATIONSUMMARY))
					label.setVisible(false);
				
				if(AppUtility.isPrAssignToPO("PurchaseRequisition")){
					System.out.println("PR CONFIG TRUE");
					if (text.equals(AppConstants.SENDMAILTOASSIGNPERSON)){
						System.out.println("OK");
						label.setVisible(true);
					}
				}else{
					System.out.println("CONFIG FALSE");
					if (text.equals(AppConstants.SENDMAILTOASSIGNPERSON)){
						label.setVisible(false);
					}
				}
				/**
				 * Date : 19-04-2018 By ANIL
				 */
				if (text.equals(AppConstants.VIEWSALESORDER)){
					label.setVisible(false);
				}
				
				if(getManageapproval().isSelfApproval()){  
					if(text.equals(ManageApprovals.APPROVALREQUEST))
						label.setVisible(false);
					if(text.equals(ManageApprovals.SUBMIT))
						label.setVisible(true);
					if(text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
						label.setVisible(false);
				}else{  
					if(text.equals(ManageApprovals.APPROVALREQUEST))
						label.setVisible(true);
					if(text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
						label.setVisible(false);
					if(text.equals(ManageApprovals.SUBMIT))
						label.setVisible(false);
				}

			}else if (status.equals(PurchaseOrder.APPROVED)) {
				if (text.equals(AppConstants.CANCELPURCHASEORDER))
					label.setVisible(true);
				if (text.equals(AppConstants.CANCELLATIONSUMMARY))
					label.setVisible(false);
				if (text.equals(AppConstants.SENDMAILTOASSIGNPERSON))
					label.setVisible(false);
				
				/**23-1-2019 added by amol for view GRN****/
				if (text.equals(AppConstants.VIEWGRN))
					label.setVisible(true);
				
				/**
				 * Date : 19-04-2018 By ANIL
				 */
				if (text.equals(AppConstants.VIEWSALESORDER)){
					label.setVisible(true);
				}
				if(text.equals(ManageApprovals.SUBMIT))
					label.setVisible(false);
			}
			/**25-1-2019 added by amol *****/
			else if(status.equals(PurchaseOrder.REQUESTED)){
				if (text.equals(AppConstants.VIEWGRN))
					label.setVisible(false);
				/**
				 * Updated By: Viraj
				 * Date: 15-02-2019
				 * Description: To remove submit,cancellation summary and cancel purchase order
				 */
				if(text.equals(ManageApprovals.SUBMIT))
					label.setVisible(false);
				if (text.equals(AppConstants.CANCELLATIONSUMMARY))
					label.setVisible(false);
				if (text.equals(AppConstants.CANCELPURCHASEORDER))
					label.setVisible(false);
				/** Ends **/
				
				}
			
			else if (status.equals(PurchaseOrder.CANCELLED)) {
				if (text.equals(AppConstants.CANCELPURCHASEORDER))
					label.setVisible(false);
				if (text.equals(AppConstants.CANCELLATIONSUMMARY))
					label.setVisible(true);
				if (text.equals(ManageApprovals.APPROVALREQUEST))
					label.setVisible(false);
				if (text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
					label.setVisible(false);
				if (text.equals(AppConstants.SENDMAILTOASSIGNPERSON))
					label.setVisible(false);
				/**23-1-2019 added by amol for view GRN****/
				if (text.equals(AppConstants.VIEWGRN))
					label.setVisible(true);
				
				/**
				 * Date : 19-04-2018 By ANIL
				 */
				if (text.equals(AppConstants.VIEWSALESORDER)){
					label.setVisible(false);
				}
				if(text.equals(ManageApprovals.SUBMIT))
					label.setVisible(false);
				
			} else if (status.equals(PurchaseOrder.REJECTED)) {
				if (text.equals(ManageApprovals.APPROVALREQUEST))
					label.setVisible(false);
				if (text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
					label.setVisible(false);
				if (text.equals(AppConstants.CANCELPURCHASEORDER))
					label.setVisible(false);
				if (text.equals(AppConstants.CANCELLATIONSUMMARY))
					label.setVisible(false);
				if (text.equals(AppConstants.SENDMAILTOASSIGNPERSON))
					label.setVisible(false);
				/**
				 * Date : 19-04-2018 By ANIL
				 */
				if (text.equals(AppConstants.VIEWSALESORDER)){
					label.setVisible(false);
				}
				/***25-1-2019 added by amol***/
				if (text.equals(AppConstants.VIEWGRN))
					label.setVisible(false);
				if(text.equals(ManageApprovals.SUBMIT))
					label.setVisible(false);
			} else {
				if (text.equals("Mark Close"))
					label.setVisible(false);
				if (text.equals("Create GRN"))
					label.setVisible(false);
				if (text.equals(AppConstants.CANCELPURCHASEORDER))
					label.setVisible(false);
				if (text.equals(AppConstants.CANCELLATIONSUMMARY))
					label.setVisible(false);
				if (text.equals(AppConstants.SENDMAILTOASSIGNPERSON))
					label.setVisible(false);
				/**
				 * Date : 19-04-2018 By ANIL
				 */
				if (text.equals(AppConstants.VIEWSALESORDER)){
					label.setVisible(false);
				}
				if(text.equals(ManageApprovals.SUBMIT))
					label.setVisible(false);
			}
			
			
			 
			//Ashwini Patil Date:20-09-2022
			if(isHideNavigationButtons()) {
				Console.log("in changeProcessLevel isHideNavigationButtons");
				if(text.equalsIgnoreCase("New")||text.equalsIgnoreCase(AppConstants.VIEWSALESORDER)||text.equalsIgnoreCase(AppConstants.VIEWGRN)||text.equalsIgnoreCase(AppConstants.COPY))
					label.setVisible(false);	
			}
		}
	}
	


	/******************************************** Set Enable Method **************************************************/

	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		dbLOIDate.setEnabled(false);
		dbRfqDate.setEnabled(false);
		productTablePO.setEnable(state);
		vendorTablePO.setEnable(state);
		ibPOId.setEnabled(false);
		tbStatus.setEnabled(false);
		dbTotal.setEnabled(false);
		doamtincltax.setEnabled(false);
		donetpayamt.setEnabled(false);
		tbcommentForStatusChange.setEnabled(false);
		
//		olbcstpercent.setEnabled(false);
//		cbcformlis.setEnabled(false);
		
		
		deliveryadd.setEnable(false);
	
		
		if(podateenabletrue){
			dbPODate.setEnabled(true);
		}else{
		dbPODate.setEnabled(false);
		}
		/**
		 *  nidhi
		 *  18-07-2017
		 *   make view state as required 
		 */
			productchargestable.setEnable(state);
		/**
		 *  end
		 */
		if((!purchaseRequisiteNo.getValue().equals("")&&!purchaseRequisiteNo.getValue().equals("0"))
				||(!ibreqforQuotation.getValue().equals("")&&!ibreqforQuotation.getValue().equals("0"))
					||(!ibLetterOfIntentID.getValue().equals("")&&!ibLetterOfIntentID.getValue().equals("0"))){
			//Ashwini Patil date:30-06-2023 commented as Atharva said we allow to add products even if po created from requision
			//prodInfoComposite.setEnable(false);
			//addproducts.setEnabled(false);
		}
		
		
		/**
		 * Date : 08-02-2017 by ANIL
		 */
		paymentTermsTable.setEnable(state);
		/**
		 * End
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("PurchaseOrder", "MonthwiseNumberGeneration")){
			ibRefOrderNo.setEnabled(false);
		}
		
		marginPopup.getLblOk().setVisible(state);
		marginPopup.vendorMarginTbl.setEnable(state);
		marginPopup.otherChargesMarginTbl.setEnable(state);
		marginPopup.btnAdd.setEnabled(state);
		marginPopup.tbCostType.setEnabled(state);
		marginPopup.dbAmount.setEnabled(state);
		tblOtherCharges.setEnable(state);
	}

	@Override
	public TextBox getstatustextbox() {
		return this.tbStatus;
	}

	/************************************** validate only vendor is selected ****************************************/

	public boolean validateVendorselect() {
		List<VendorDetails> list = vendorTablePO.getDataprovider().getList();
		if(list.size()!=0){
			for(VendorDetails venDet:list){
				if(venDet.getStatus()==true){
					return true;
				}
			}
		}else{
			return true;
		}
		return false;
	}

	/****************************************** Validation Method *********************************************************/
	@Override
	public boolean validate() {
		boolean a = super.validate();
		if (a == false)
			return false;

		if (dbPODate.getValue().after(dbExpdeliverydate.getValue())) {
			this.showDialogMessage("Expected Delivery Date Should Be Greater Than Creation Date.");
			return false;
		} else if (dbExpectedResposeDate.getValue() != null) {
			if (dbPODate.getValue().after(dbExpectedResposeDate.getValue())) {
				this.showDialogMessage("Expected Response Date Should Be Greater Than Creation Date.");
				return false;
			} else if (dbExpectedResposeDate.getValue().after(
					dbExpdeliverydate.getValue())) {
				this.showDialogMessage("Expected Deliver Date Should Be Greater Than Expected Response Date.");
				return false;
			} 
		} 
		
//		if (paymentTermsTable.getDataprovider().getList().size() == 0) {
//			this.showDialogMessage("Please add payment Terms");
//			return false;
//		}
		
		boolean payment = validatePaymentTermPercent();
		if (payment == false) {
			this.showDialogMessage("In Payment Terms total of percent column should be equal to 100");
			return false;
		}
		if (productTablePO.getDataprovider().getList().size() == 0) {
			this.showDialogMessage("Please add products");
			return false;
		}
		
		boolean b = validateVendorselect();
		if (b == false) {
			this.showDialogMessage("Please Select Vendor !");
			return false;
		}
		
//		if(deliveryadd.getState().getSelectedIndex()!=0){
//			String vendorState=PurchaseOrderPresenter.vendorState.trim();
//			System.out.println("Add. valdn VendorState::: "+vendorState);
//			
//			if(!vendorState.equals("")&&!getDeliveryadd().getState().getValue().trim().equals(vendorState)&&this.getCbcformlis().getSelectedIndex()==0){
//				showDialogMessage("C Form is Mandatroy");
//				return false;
//			}
//			if(getCbcformlis().getItemText(getCbcformlis().getSelectedIndex()).trim().equals(AppConstants.YES)&&getOlbcstpercent().getSelectedIndex()==0){
//				showDialogMessage("Please Select CST Percent !");
//				return false;
//			}
//		}
//		if(!validateCForm()){
//			return false;
//		}
		
		/**
		 *  nidhi
		 *  14-09-2017
		 *  for check product Price 
		 */
		if(!validatePRProdPrice()){
			showDialogMessage("Please enter appropriate Product Price required!");
			return false;
		}
		/**
		 * end
		 */
		if(ProductTablePO.isValidWarehouse==false){
			showDialogMessage("Warehouse is already selected for product, please change warehouse or delete row.");
			return false;
		}
		
		if(!isAllWarehouseIsSelectedOrDeselected()){
			return false;
		}
		
		 /**
		 * Date 03-07-2017 added by vijay tax validation
		 */
		List<ProductOtherCharges> prodtaxlist = this.producttaxtable.getDataprovider().getList();
		for(int i=0;i<prodtaxlist.size();i++){
			
			if(dbPODate.getValue()!=null && dbPODate.getValue().before(date)){
				if(prodtaxlist.get(i).getChargeName().equalsIgnoreCase("CGST") || prodtaxlist.get(i).getChargeName().equalsIgnoreCase("SGST")
						|| prodtaxlist.get(i).getChargeName().equalsIgnoreCase("IGST")){
					showDialogMessage("As Purchase Order date "+fmt.format(dbPODate.getValue())+" GST Tax is not applicable");
					return false;
				}
			}
			/**
			 * @author Anil @since 21-07-2021
			 * Issue raised by Rahul for Innovative Pest(Thailand) 
			 * Vat is applicable there so system should not through validation for VAT
			 */
//			else if(dbPODate.getValue()!=null && dbPODate.getValue().equals(date) || dbPODate.getValue().after(date)){
//				if(prodtaxlist.get(i).getChargeName().equalsIgnoreCase("CST") || prodtaxlist.get(i).getChargeName().equalsIgnoreCase("Service Tax")
//						|| prodtaxlist.get(i).getChargeName().equalsIgnoreCase("VAT")){
//					showDialogMessage("As Purchase Order date "+fmt.format(dbPODate.getValue())+" VAT/CST/Service Tax is not applicable");
//					return false;
//				}
//			}
		}
		/**
		 * ends here
		 */
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("PurchaseOrder", AppConstants.PC_ENABLETAXMANDATORY)){
			
			List<ProductDetailsPO> list = productTablePO.getDataprovider().getList();
			for(ProductDetailsPO product : list){
				if(product.getPurchaseTax1().getPercentage()==0 && product.getPurchaseTax2().getPercentage()==0){
					showDialogMessage("Tax is mandatory for products please add tax in products!");
					return false;
				}
			}
		}

			try {
				if(ibLetterOfIntentID.getValue()!=null && !ibLetterOfIntentID.getValue().equals(""))
				Integer.parseInt(ibLetterOfIntentID.getValue());
			} catch (Exception e) {
				showDialogMessage("Please add only number in LOI ID");
				return false;
			}
			try {
				if(ibreqforQuotation.getValue()!=null && !ibreqforQuotation.getValue().equals(""))
				Integer.parseInt(ibreqforQuotation.getValue());
			} catch (Exception e) {
				showDialogMessage("Please add only number in RFQ ID");
				return false;
			}
			try {
				if(purchaseRequisiteNo.getValue()!=null && !purchaseRequisiteNo.getValue().equals(""))
				Integer.parseInt(purchaseRequisiteNo.getValue());
			} catch (Exception e) {
				showDialogMessage("Please add only number in PR Number");
				return false;
			}
		
				if(deliveryaddress.getValue().equals("Other") && (deliveryadd.getValue()==null || deliveryadd.getValue().equals(""))){
					showDialogMessage("Please add delivery address");
					return false;
				}
				if(deliveryaddress.getSelectedIndex()==0 && (deliveryadd.getValue()==null || deliveryadd.getValue().equals(""))){
					showDialogMessage("Please add delivery address");
					return false;
				}
		
		return true;
	}
	

		// ***************************************** Validation For CForm **************************************
		
//		public boolean validateCForm()
//		{
//			if(!validateState()&&this.getCbcformlis().getSelectedIndex()==0){
//				showDialogMessage("Please select C Form");
//				return false;
//			}
//			if(!validateState()&&this.getCbcformlis().getItemText(getCbcformlis().getSelectedIndex()).equals(AppConstants.YES)){
//				if(this.getOlbcstpercent().getSelectedIndex()==0){
//					showDialogMessage("Please select CST Percent");
//					return false;
//				}
//				else{
//					return true;
//				}
//			}
//			return true;
//			
//		}
		
		/**
		 * This method is used to validate state from the delivery address and verify it with the selected vendor state.
		 * If state is not equal with the state in the delivery address the it returns false, else it returns true.
		 * @return
		 */
		
		// ******************************** validate Vendor Sate with Address Composite State **************************
		
//		public boolean validateState() {
//			
//			if(isWarehouseSelected()){
//				List<ProductDetailsPO> list=productTablePO.getDataprovider().getList();
//				for(ProductDetailsPO obj:list){
//					if(!obj.getWarehouseName().equals("")&&!PurchaseOrderPresenter.vendorState.equals("")){
//						String warehouseState=ProductTablePO.getWarehouseState(obj.getWarehouseName());
//						if(!PurchaseOrderPresenter.vendorState.trim().equals(warehouseState.trim())){
//							return false;
//						}
//					}
//				}
//				return true;
//			}else{
//				if(this.getDeliveryadd().getState().getSelectedIndex()!=0&&this.getDeliveryadd().getState().getValue().trim().equals(PurchaseOrderPresenter.vendorState.trim())){
//					return true; 
//				}
//			}
//			return false;
//		}
		
		// ****************************** Validation For Duplicate Product **************************************
			/**
			 * @author Anil,Date : 11-01-2019
			 * Added warehouse name as parameter to check duplication of product
			 */
			public boolean validateProductDuplicate(String pcode,String whName) {
				
				
				
				if(dbExpdeliverydate.getValue()==null){
					this.showDialogMessage("Please select delivery date !");
					return false;
				}
				
				if(ProductTablePO.isValidWarehouse==false){
					showDialogMessage("Warehouse is already selected for product, please change warehouse or delete row.");
					return false;
				}
				
				List<ProductDetailsPO> list = productTablePO.getDataprovider().getList();
				for (ProductDetailsPO temp : list) {
					if (temp.getProductCode().trim().equals(pcode.trim())) {
						boolean whFlag=false;
						if(temp.getWarehouseName()!=null&&whName!=null&&temp.getWarehouseName().equals(whName)){
							whFlag=true;
							this.showDialogMessage("Can not add Duplicate product");
							return false;
						}
						if(!whFlag){
							this.showDialogMessage("Can not add Duplicate product");
							return false;
						}
					}
				}
				return true;
				
			}
			
		
		// *********************** Row Count change Method **************************************************
		
//	public void onRowCountChange(RowCountChangeEvent event) {
//		if (productTablePO.getDataprovider().getList().size() == 0) {
//			productTablePO.connectToLocal();
//		} 
//	}
	/******************************************* React on Add payment terms ******************************************/
	public void reactOnAddPaymentTerms()
	{
		int payTrmDays=0;
		double payTrmPercent=0;
		String payTrmComment="";
		boolean flag=true;
		boolean percFlag=true;
		boolean payTermsFlag=true;
		
		if(ibdays.getValue()==null||ibdays.getValue().equals(""))
		{
			this.showDialogMessage("Days cannot be empty!");
			return;
		}
		
		
		if(dopercent.getValue()==null||dopercent.getValue().equals(""))
		{
			this.showDialogMessage("Percent cannot be empty!");
			return;
		}
		
		if(tbcomment.getValue()==null||tbcomment.getValue().equals(""))
		{
			this.showDialogMessage("Comment cannot be empty!");
			return;
		}
		
		boolean checkTermDays=this.validatePaymentTrmDays(ibdays.getValue());
		if(checkTermDays==false)
		{
			if(chk==0){
				showDialogMessage("Days already defined.Please enter unique days!");
			}
			flag=false;
		}
		
		
		if(ibdays.getValue()!=null&&ibdays.getValue()>=0){
			payTrmDays=ibdays.getValue();
		}
		else{
			showDialogMessage("Days cannot be empty!");
			flag=false;
		}
		
		
		if(dopercent.getValue()!=null&&dopercent.getValue()>=0){
			if(dopercent.getValue()>100){
				showDialogMessage("Percent cannot be greater than 100");
				percFlag=false;
			}
			else if(dopercent.getValue()==0){
				showDialogMessage("Percent should be greater than 0!");
			}
			else{
			payTrmPercent=dopercent.getValue();
			}
		}
		else{
			showDialogMessage("Percent cannot be empty!");
		}
		
		if(tbcomment.getValue()!=null){
			payTrmComment=tbcomment.getValue();
		}
		
		if(paymentTermsTable.getDataprovider().getList().size()>9)
		{
			showDialogMessage("Total number of payment terms allowed are 10");
			payTermsFlag=false;
		}
		
		if(!payTrmComment.equals("")&&payTrmPercent!=0&&flag==true&&percFlag==true&&payTermsFlag==true)
		{
			PaymentTerms payTerms=new PaymentTerms();
			payTerms.setPayTermDays(payTrmDays);
			payTerms.setPayTermPercent(payTrmPercent);
			payTerms.setPayTermComment(payTrmComment);
			paymentTermsTable.getDataprovider().getList().add(payTerms);
			clearPaymentTermsFields();
		}
	}

	/************************************* Clear Payment terms Fields *******************************************************/
	public void clearPaymentTermsFields() {
		ibdays.setValue(null);
		dopercent.setValue(null);
		tbcomment.setValue("");
	}

	/************************************* Validate Payment Terms PERCENT Fields *******************************************/
	public boolean validatePaymentTermPercent() {
		List<PaymentTerms> payTermsLis = this.paymentTermsTable.getDataprovider().getList();
		
		if(payTermsLis.size()!=0){
			double totalPayPercent = 0;
			for (int i = 0; i < payTermsLis.size(); i++) {
				totalPayPercent = totalPayPercent+ payTermsLis.get(i).getPayTermPercent();
			}
			System.out.println("Validation of Payment Percent as 100"+ totalPayPercent);
	
			if (totalPayPercent != 100) {
				return false;
			}
		}else{
			return true;
		}

		return true;
	}

	/************************************* Validate Payment Terms DAY Fields ************************************************/
	int chk=0;
	public boolean validatePaymentTrmDays(int retrDays)
	{
		List<PaymentTerms> payTermsLis=this.paymentTermsTable.getDataprovider().getList();
		for(int i=0;i<payTermsLis.size();i++)
		{
			if(retrDays==payTermsLis.get(i).getPayTermDays())
			{
				return false;
			}
			if(retrDays<payTermsLis.get(i).getPayTermDays())
			{
				showDialogMessage("Days should be defined in increasing order!");
				chk=1;
				return false;
			}
			else{
				chk=0;
			}
			if(retrDays>10000){
				showDialogMessage("Days cannot be greater than 4 digits!");
				return false;
			}
		}
		return true;
	}
	/************************* Initializing All Object List Element **********************************************/


	protected void initalizEmployeeNameListBox() {
		MyQuerry querry = new MyQuerry(new Vector<Filter>(), new Employee());
		oblEmployee.MakeLive(querry);
	}

	protected void initalizEmployeeBranchListBox() {
		MyQuerry querry = new MyQuerry(new Vector<Filter>(), new Branch());
		branch.MakeLive(querry);
	}

	/***************************************** set ID count ******************************************************/

	@Override
	public void setCount(int count) {
		ibPOId.setValue(count+"");
	}



	/******************************************** set FORM as per state *********************************************/

	@Override
	public void setToViewState() {
		super.setToViewState();
		setMenuAsPerStatus();
		
		SuperModel model=new PurchaseOrder();
		model=purchaseOrderobj;
		AppUtility.addDocumentToHistoryTable(AppConstants.PURCHASEMODULE,AppConstants.PURCHASEORDER, purchaseOrderobj.getCount(), null,null,null, false, model, null);
	
		if(marginFlag){
			costingBtn.setEnabled(true);
		}
		
		String mainScreenLabel="Purchase Order";
		if(purchaseOrderobj!=null&&purchaseOrderobj.getCount()!=0){
			
			
			mainScreenLabel=purchaseOrderobj.getCount()+" "+"/"+" "+purchaseOrderobj.getStatus()+" "+"/"+" "+AppUtility.parseDate(purchaseOrderobj.getCreationDate());
		}
		 fgroupingLetterInformation.getHeaderLabel().setText(mainScreenLabel);
	}

	@Override
	public void setToNewState() {
		super.setToNewState();
		Company com = new Company();
		getCompanyAddress(com.getCompanyId());
		/******30-1-2019 added by amol****/
	    tbStatus.setEnabled(false);

	    getstatustextbox().setValue(PurchaseOrder.CREATED);
	    tbStatus.setValue(PurchaseOrder.CREATED);
	    System.out.println("purchase order created");
		prDate.setEnabled(false);
		cbadds.setValue(false);
	}

	@Override
	public void setToEditState() {
		super.setToEditState();
		setMenuAsPerStatus();
		if (tbStatus.getValue().equals(PurchaseOrder.REJECTED)) {
			tbStatus.setValue(PurchaseOrder.CREATED);
		}
//		if(olbcstpercent.getValue()!=null){
//			olbcstpercent.setEnabled(true);
//		}
//		if(cbcformlis.getSelectedIndex()!=0){
//			cbcformlis.setEnabled(true);
//		}
		this.processLevelBar.setVisibleFalse(false);
		
		String mainScreenLabel="Purchase Order";
		if(purchaseOrderobj!=null&&purchaseOrderobj.getCount()!=0){
			
			
			mainScreenLabel=purchaseOrderobj.getCount()+" "+"/"+" "+purchaseOrderobj.getStatus()+" "+"/"+" "+AppUtility.parseDate(purchaseOrderobj.getCreationDate());
		}
		 fgroupingLetterInformation.getHeaderLabel().setText(mainScreenLabel);
	}

	/******************************* Set Menu Method for header and Process level menu **************************************/

	public void setMenuAsPerStatus() {
		this.setAppHeaderBarAsPerStatus();
		this.toggleProcessLevelMenu();
	}

	/************************************ Set App Header Bar As per Status ***********************************************/

	public void setAppHeaderBarAsPerStatus() {
		PurchaseOrder entity = (PurchaseOrder) presenter.getModel();
		String status = entity.getStatus();
		if (status.equals(PurchaseOrder.CLOSED)|| status.equals(PurchaseOrder.CANCELLED))
		{
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Discard") || text.contains("Search")|| text.contains(AppConstants.NAVIGATION)||text.equals(AppConstants.MESSAGE))
				{
					menus[k].setVisible(true);
				} else {
					menus[k].setVisible(false);
				}
			}
		}
		
		if (status.equals(PurchaseOrder.APPROVED))
		{
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Discard") || text.contains("Search")|| text.contains("Print")|| text.contains("Email")|| text.contains(AppConstants.NAVIGATION)||text.equals(AppConstants.MESSAGE))
				{
					menus[k].setVisible(true);
				} else {
					menus[k].setVisible(false);
				}
			}
		}
	}

	/************************************************* set Tax table **********************************************/
	
	
	/*****************************************Add Product Taxes Method***********************************/
	
	/**
	 * This method is called when a product is added to SalesLineItem table.
	 * Taxes related to corresponding products will be added in ProductTaxesTable.
	 * @throws Exception
	 */
//	public void addProdTaxes() throws Exception{
//		System.out.println();
//		System.out.println("INSIDE ADD PRODUCT TAXES METHOD !!");
//		
//		List<ProductDetailsPO> salesLineItemLis=this.productTablePO.getDataprovider().getList();
//		List<ProductOtherCharges> taxList=this.producttaxtable.getDataprovider().getList();
//		NumberFormat nf=NumberFormat.getFormat("0.00");
//		String cformval="";
//		int cformindex;
//		
////		int cformindex=this.getCbcformlis().getSelectedIndex();
////		if(cformindex!=0){
////			cformval=this.getCbcformlis().getItemText(cformindex);
////		}
//		
//		System.out.println("PRODUCT LIST SIZE : "+salesLineItemLis.size());
//		
//		for(int i=0;i<salesLineItemLis.size();i++){
//			double priceqty=0,taxPrice=0;
//			
//			if((salesLineItemLis.get(i).getDiscount()==null || salesLineItemLis.get(i).getDiscount()==0) && (salesLineItemLis.get(i).getDiscountAmt()==0) ){
////				System.out.println("inside both 0 condition");
//				System.out.println("NO DISCOUNT");
//				taxPrice=this.getProducttable().removeAllTaxes(salesLineItemLis.get(i).getPrduct());
//				priceqty=(salesLineItemLis.get(i).getProdPrice()-taxPrice)*salesLineItemLis.get(i).getProductQuantity();
//				priceqty=Double.parseDouble(nf.format(priceqty));
//			}else if(((salesLineItemLis.get(i).getDiscount()!=null)&&(salesLineItemLis.get(i).getDiscountAmt()!=0))){
////				System.out.println("inside both not null condition");
//				taxPrice=this.getProducttable().removeAllTaxes(salesLineItemLis.get(i).getPrduct());
//				priceqty=(salesLineItemLis.get(i).getProdPrice()-taxPrice);
//				priceqty=priceqty-(priceqty*salesLineItemLis.get(i).getDiscount()/100);
//				priceqty=priceqty-salesLineItemLis.get(i).getDiscountAmt();
//				priceqty=priceqty*salesLineItemLis.get(i).getProductQuantity();
//				priceqty=Double.parseDouble(nf.format(priceqty));
//			}else {
////				System.out.println("inside oneof the null condition");
//				taxPrice=this.getProducttable().removeAllTaxes(salesLineItemLis.get(i).getPrduct());
//				priceqty=(salesLineItemLis.get(i).getProdPrice()-taxPrice);
//				if(salesLineItemLis.get(i).getDiscount()!=null){
//					priceqty=priceqty-(priceqty*salesLineItemLis.get(i).getDiscount()/100);
//				}
//				else{
//					priceqty=priceqty-salesLineItemLis.get(i).getDiscountAmt();
//				}
//				priceqty=priceqty*salesLineItemLis.get(i).getProductQuantity();
//				priceqty=Double.parseDouble(nf.format(priceqty));
//			}
//			System.out.println();
//			System.out.println("Total Price Excluding Tax "+i+" - "+priceqty);
//			
//			
//			/**
//			 * Date 2 August 2017 below code commented by vijay 
//			 * for this below code by default showing vat and service tax percent 0 no nedd this if
//			 * product master does not have any taxes
//			 */
//			
//			/**
//			 * calculating in case of vat tax ==0 and cform not selected
//			 */
////			if(salesLineItemLis.get(i).getVat()==0&&cbcformlis.getSelectedIndex()==0){
////				System.out.println("CFORM NOT APPLICABLE && VAT == 0");
////				System.out.println("TAX TBL SIZE : "+getProducttaxtable().getDataprovider().getList().size());
////				ProductOtherCharges pocentity=new ProductOtherCharges();
////				
////				if(validateState()){
////					System.out.println("valid state VAT");
////					pocentity.setChargeName("VAT");
////				}
////				if(!validateState()){
////					System.out.println("!valid state CST");
////					pocentity.setChargeName("CST");
////				}
////				pocentity.setChargePercent(salesLineItemLis.get(i).getVat());
////				pocentity.setIndexCheck(i+1);
////				int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getVat(),"VAT");
////				System.out.println("INDEX VALUE : "+indexValue);
////				if(indexValue!=-1){
////					System.out.println("V SETTING AMT 0 AND REMOVING ROW "+indexValue);
//////					pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
////					pocentity.setAssessableAmount(0);
////					this.getProducttaxtable().getDataprovider().getList().remove(indexValue);
////					System.out.println("V AFTER REMOVEL TAX LIST SIZE : "+getProducttaxtable().getDataprovider().getList().size());
////				}
////				
////				if(indexValue==-1){
////					System.out.println("V SETTING AMOUNT 0");
////					pocentity.setAssessableAmount(0);
//////					pocentity.setAssessableAmount(priceqty);
////				}
////				this.producttaxtable.getDataprovider().getList().add(pocentity);
////				System.out.println("VAT TAX TBL LIST SIZE : "+this.producttaxtable.getDataprovider().getList().size());
////			}
////			
////			if(salesLineItemLis.get(i).getTax()==0){
////				System.out.println("INSIDE SERVICE TAX ==0 ");
//////				if(validateState()){
////				ProductOtherCharges pocentity=new ProductOtherCharges();
////				pocentity.setChargeName("Service Tax");
////				pocentity.setChargePercent(salesLineItemLis.get(i).getTax());
////				pocentity.setIndexCheck(i+1);
////				int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getTax(),"Service");
////				if(indexValue!=-1){
////					System.out.println("S SETTING AMT 0 AND REMOVING ROW "+indexValue);
//////					pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
////					pocentity.setAssessableAmount(0);
////					this.getProducttaxtable().getDataprovider().getList().remove(indexValue);
////					System.out.println("S AFTER REMOVEL TAX LIST SIZE : "+getProducttaxtable().getDataprovider().getList().size());
////				}
////				
////				if(indexValue==-1){
////					System.out.println("S SETTING AMOUNT 0");
////					pocentity.setAssessableAmount(0);
//////					pocentity.setAssessableAmount(priceqty);
////				}
////				this.producttaxtable.getDataprovider().getList().add(pocentity);
////				System.out.println("ST TAX TBL LIST SIZE : "+this.producttaxtable.getDataprovider().getList().size());
////			}
//			
//			/**
//			 * Ends here
//			 */
//			
//			
//			
//			/**
//			 * Date 2 August 2017 added by vijay for GST
//			 * below code is applicable only if PO date is less than 1 july 2017 for Cform
//			 * below if condition added by vijay
//			 */
//			
//			if(dbPODate.getValue()!=null && dbPODate.getValue().before(date)){
//			
//			
//			/**
//			 * Adding Vat Tax to table if the company state and delivery state is not equal but cform not selected  
//			 * Added on 24-11-2016 By Anil
//			 * Project : DEADSTOCK (NBHC)
//			 */
//				
////			if(!validateState()&&this.getCbcformlis().getSelectedIndex()==0){
////				System.out.println("Adding Vat Tax to table if the company state and delivery state is not equal but cform not selected  ");
////				if(salesLineItemLis.get(i).getVat()!=0){
////					System.out.println("Vattx");
////					ProductOtherCharges pocentity=new ProductOtherCharges();
////					pocentity.setChargeName("VAT");
////					pocentity.setChargePercent(salesLineItemLis.get(i).getVat());
////					pocentity.setIndexCheck(i+1);
////					int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getVat(),"VAT");
////					
////					if(indexValue!=-1){
////						System.out.println("Index As Not -1");
////						pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
////						this.getProducttaxtable().getDataprovider().getList().remove(indexValue);
////					}
////					if(indexValue==-1){
////						System.out.println("VAt");
////						pocentity.setAssessableAmount(priceqty);
////					}
////					this.producttaxtable.getDataprovider().getList().add(pocentity);
////				}
////			}
//			
//			}
//			/**
//			 * END
//			 */
//			
//		/**
//		 * Adding Vat Tax to table if the company state and delivery state is equal
//		 */
//			
//			System.out.println("HI VIJAY print name ==="+salesLineItemLis.get(i).getPurchaseTax1().getTaxPrintName());
////			System.out.println("Hi == validate state"+validateState());
//			//Date 03 august 2017 below if condition commented by vijay no need this if condition here
////		if(validateState()){
//			System.out.println("Adding Vat Tax to table if the company state and delivery state is equal");
//			if(salesLineItemLis.get(i).getVat()!=0){
//				
//				System.out.println("VAT !=0 -- ");
//				/**
//				 * Date 2 August 2017 below if condition added by vijay for GST old code added in else block
//				 */
//				if(salesLineItemLis.get(i).getPurchaseTax1().getTaxPrintName()!=null && !salesLineItemLis.get(i).getPurchaseTax1().getTaxPrintName().equals("")){
//					System.out.println("GST");
//					ProductOtherCharges pocentity=new ProductOtherCharges();
//					// below code added by vijay for GST
//					pocentity.setChargeName(salesLineItemLis.get(i).getPurchaseTax1().getTaxPrintName());
//					pocentity.setChargePercent(salesLineItemLis.get(i).getVat());
//					pocentity.setIndexCheck(i+1);
//					int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getVat(), salesLineItemLis.get(i).getPurchaseTax1().getTaxPrintName());
//					
//					if(indexValue!=-1){
//						System.out.println("Index As Not -1");
//						pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
//						this.getProducttaxtable().getDataprovider().getList().remove(indexValue);
//					}
//					if(indexValue==-1){
//						System.out.println("VAt");
//						pocentity.setAssessableAmount(priceqty);
//					}
//					this.producttaxtable.getDataprovider().getList().add(pocentity);
//					
//				}else{
//					System.out.println("Vattx");
//					ProductOtherCharges pocentity=new ProductOtherCharges();
//					pocentity.setChargeName("VAT");
//					pocentity.setChargePercent(salesLineItemLis.get(i).getVat());
//					pocentity.setIndexCheck(i+1);
//					int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getVat(),"VAT");
//					
//					if(indexValue!=-1){
//						System.out.println("Index As Not -1");
//						pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
//						this.getProducttaxtable().getDataprovider().getList().remove(indexValue);
//					}
//					if(indexValue==-1){
//						System.out.println("VAt");
//						pocentity.setAssessableAmount(priceqty);
//					}
//					this.producttaxtable.getDataprovider().getList().add(pocentity);
//					
//				}
//				
//			}
////		}
//			
//		
//		/**
//		 * Date 2 August 2017 added by vijay for GST
//		 * below code is applicable only if PO date is less than 1 july 2017 for Cform
//		 * below if condition added by vijay
//		 */
//		
//		if(dbPODate.getValue()!=null && dbPODate.getValue().before(date)){
//			
//		/**
//		 * Adding Vat Tax as CST if company state and delivery state is not equal and C form is submitted.
//		 */
////			if(this.getCbcformlis().getSelectedIndex()!=0&&AppConstants.YES.equals(cformval.trim())){
////				System.out.println("Adding Vat Tax as CST if company state and delivery state is not equal and C form is submitted.");
//////				if(salesLineItemLis.get(i).getVat()!=0){
////					System.out.println("Vattx");
////					ProductOtherCharges pocentity=new ProductOtherCharges();
////					pocentity.setChargeName("CST");
////					
////					TaxDetails cstperc=olbcstpercent.getSelectedItem();
////					pocentity.setChargePercent(cstperc.getTaxChargePercent());
////					pocentity.setIndexCheck(i+1);
////					int indexValue=this.checkTaxPercent(cstperc.getTaxChargePercent(),"CST");
////					
////					if(indexValue!=-1){
////						System.out.println("Index As Not -1");
////						pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
////						this.getProducttaxtable().getDataprovider().getList().remove(indexValue);
////					}
////					if(indexValue==-1){
////						pocentity.setAssessableAmount(priceqty);
////					}
////					this.producttaxtable.getDataprovider().getList().add(pocentity);
//////				}
////			}
//			
//		 }
//			
//		
//		
//		/**
//		 * Date 2 August 2017 added by vijay for GST
//		 * below code is applicable only if PO date is less than 1 july 2017 for Cform
//		 * below if condition added by vijay
//		 */
//		
//		if(dbPODate.getValue()!=null && dbPODate.getValue().before(date)){
//			
//			/**
//			 * Adding Vat Tax as CST if company state and delivery state is not equal and C form is not submitted.
//			 */
//			
////			if(this.getCbcformlis().getSelectedIndex()!=0&&AppConstants.NO.equals(cformval.trim())){
////				System.out.println("Adding Vat Tax as CST if company state and delivery state is not equal and C form is not submitted.");
////				if(salesLineItemLis.get(i).getVat()!=0){
////					System.out.println("Vattx");
////					ProductOtherCharges pocentity=new ProductOtherCharges();
////					pocentity.setChargeName("CST");
////					pocentity.setChargePercent(salesLineItemLis.get(i).getVat());
////					pocentity.setIndexCheck(i+1);
//////					int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getVat(),"VAT");
////					
////					/**
////					 * Updated By Anil Date: 24-11-2016
////					 */
////					int indexValue=this.checkCstTaxPercentForNoCform(salesLineItemLis.get(i).getVat(),"VAT");
////					
////					/**
////					 * End
////					 */
////					if(indexValue!=-1){
////						System.out.println("vsctIndex As Not -1");
////						pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
////						this.getProducttaxtable().getDataprovider().getList().remove(indexValue);
////					}
////					if(indexValue==-1){
////						System.out.println("vcst-1");
////						pocentity.setAssessableAmount(priceqty);
////					}
////					this.producttaxtable.getDataprovider().getList().add(pocentity);
////				}
////			}
//			
//		}
//			
//			/**
//			 * Adding service tax to the table
//			 */
//			
//			if(salesLineItemLis.get(i).getTax()!=0){
//				System.out.println("Adding service tax to the table");
//				System.out.println("ServiceTx");
//				ProductOtherCharges pocentity=new ProductOtherCharges();
////				pocentity.setChargeName("Service Tax");
//				
//				/**
//				 * Date 2 August 2017 added by vijay for GST and old code added in else block 
//				 */
//				if(salesLineItemLis.get(i).getPurchaseTax2().getTaxPrintName()!=null && !salesLineItemLis.get(i).getPurchaseTax2().getTaxPrintName().equals(""))
//				{
//					pocentity.setChargeName(salesLineItemLis.get(i).getPurchaseTax2().getTaxPrintName());
//				}else{
//					pocentity.setChargeName("Service Tax");
//				}
//				/**
//				 * ends here
//				 */
//				
//				
//				
//				pocentity.setChargePercent(salesLineItemLis.get(i).getTax());
//				pocentity.setIndexCheck(i+1);
////				int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getTax(),"Service");
//				
//				
//				int indexValue;
//				/**
//				 * Date 12 july 2017 if condition for old code and else block for GST added by vijay
//				 */
//				if(salesLineItemLis.get(i).getPurchaseTax2().getTaxPrintName()!=null && !salesLineItemLis.get(i).getPurchaseTax2().getTaxPrintName().equals(""))
//				{
//					indexValue =this.checkTaxPercent(salesLineItemLis.get(i).getPurchaseTax2().getPercentage(),salesLineItemLis.get(i).getPurchaseTax2().getTaxPrintName());
//					System.out.println("GST ===== --------");
//				}else{
//					indexValue =this.checkTaxPercent(salesLineItemLis.get(i).getPurchaseTax2().getPercentage(),"Service");
//					System.out.println("Service ===== --------");
//
//				}
//				/**
//				 * ends here
//				 */
//				
//				if(indexValue!=-1){
//					System.out.println("Index As Not -1");
//					/**
//					 * If validate state method is true
//					 */
//					
////					if(validateState()){
//						System.out.println("VAlidate true");
//						double assessValue=0;
//						if(salesLineItemLis.get(i).getVat()!=0){
//							
//							/**
//							 * Date 11 july 2017 if condition added by vijay for GST and old code added in else block 
//							 */
//							if(salesLineItemLis.get(i).getPurchaseTax2().getTaxPrintName()!=null && !salesLineItemLis.get(i).getPurchaseTax2().getTaxPrintName().equals("")){
//								System.out.println("For GST !! ");
//								assessValue=priceqty;
//								pocentity.setAssessableAmount(assessValue+taxList.get(indexValue).getAssessableAmount());
//								this.getProducttaxtable().getDataprovider().getList().remove(indexValue);
//							}else{
//								System.out.println("OLD Code");
//								assessValue=priceqty+(priceqty*salesLineItemLis.get(i).getVat()/100);
//								pocentity.setAssessableAmount(assessValue+taxList.get(indexValue).getAssessableAmount());
//								this.getProducttaxtable().getDataprovider().getList().remove(indexValue);
//							}
//						}
//						if(salesLineItemLis.get(i).getVat()==0){
//							pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
//							this.getProducttaxtable().getDataprovider().getList().remove(indexValue);
//						}
////					}
//					
//					
//					/**
//					 * Date 2 August 2017 added by vijay for GST
//					 * below code is applicable only if PO date is less than 1 july 2017 for Cform
//					 * below if condition added by vijay
//					 */
//					if(dbPODate.getValue()!=null && this.dbPODate.getValue().before(date)){	
//						
//						/**
//						 * If validate state method is true and cform is submittted.
//						 */
//					
////					if(!validateState()&&this.getCbcformlis().getItemText(getCbcformlis().getSelectedIndex()).equals(AppConstants.YES)){
////						double assessValue=0;
////						if(this.getOlbcstpercent().getSelectedIndex()!=0){
////							TaxDetails cstPerc1=olbcstpercent.getSelectedItem();
////							assessValue=priceqty+(priceqty*cstPerc1.getTaxChargePercent()/100);
////							pocentity.setAssessableAmount(assessValue+taxList.get(indexValue).getAssessableAmount());
////							this.getProducttaxtable().getDataprovider().getList().remove(indexValue);
////						}
////					}
//				   }
//					
//					/**
//					 * If validate state method is true and cform is not submitted.
//					 */
//					
//					/**
//					 * Date 2 August 2017 added by vijay for GST
//					 * below code is applicable only if PO date is less than 1 july 2017 for Cform
//					 * below if condition added by vijay
//					 */
//					if(dbPODate.getValue()!=null && this.dbPODate.getValue().before(date)){	
//						
//					
////					if(!validateState()&&this.getCbcformlis().getItemText(getCbcformlis().getSelectedIndex()).equals(AppConstants.NO)){
////						double assessValue=0;
////						assessValue=priceqty+(priceqty*salesLineItemLis.get(i).getVat()/100);
////						pocentity.setAssessableAmount(assessValue+taxList.get(indexValue).getAssessableAmount());
////						this.getProducttaxtable().getDataprovider().getList().remove(indexValue);
////					}
//					
//					}
//					
//					/**
//					 * 
//					 * Updated By Anil Date: 24-11-2016
//					 */
////					if(!validateState()&&this.getCbcformlis().getSelectedIndex()==0){
////					if(!validateState()&&this.getCbcformlis().getSelectedIndex()==0){
////						double assessValue=0;
////						if(salesLineItemLis.get(i).getVat()!=0){
////							
////							/**
////							 * Date 11 july 2017 if condition added by vijay for GST and old code added in else block 
////							 */
////							if(salesLineItemLis.get(i).getServiceTax().getTaxPrintName()!=null && !salesLineItemLis.get(i).getServiceTax().getTaxPrintName().equals("")){
////								System.out.println("For GST ++ +");
////								assessValue=priceqty;
////								pocentity.setAssessableAmount(assessValue+taxList.get(indexValue).getAssessableAmount());
////								this.getProducttaxtable().getDataprovider().getList().remove(indexValue);
////							}else{
////								assessValue=priceqty+(priceqty*salesLineItemLis.get(i).getVat()/100);
////								pocentity.setAssessableAmount(assessValue+taxList.get(indexValue).getAssessableAmount());
////								this.getProducttaxtable().getDataprovider().getList().remove(indexValue);
////							}
////							
////						}
////						if(salesLineItemLis.get(i).getVat()==0){
////							pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
////							this.getProducttaxtable().getDataprovider().getList().remove(indexValue);
////						}
////					}
//					
//					/**
//					 * END
//					 */
//				}
//				if(indexValue==-1){
//					System.out.println("Index As -1");
//					
////					if(validateState()){
//						double assessVal=0;
//						if(salesLineItemLis.get(i).getVat()!=0){
//							
//							/**
//							 * Date 11 july 2017 if condition added by vijay for GST and old code added in else block 
//							 */
//							if(salesLineItemLis.get(i).getPurchaseTax2().getTaxPrintName()!=null && !salesLineItemLis.get(i).getPurchaseTax2().getTaxPrintName().equals("")){
//								System.out.println("For GST -- = = ");
//								assessVal=priceqty;
////								pocentity.setAssessableAmount(assessVal+taxList.get(indexValue).getAssessableAmount());
//								pocentity.setAssessableAmount(assessVal);
//
//								System.out.println(assessVal);
//							}else{
//								System.out.println("OLD -- == ");
//								assessVal=priceqty+(priceqty*salesLineItemLis.get(i).getVat()/100);
//								pocentity.setAssessableAmount(assessVal);
//							}
//							 
//						}
//						if(salesLineItemLis.get(i).getVat()==0){
//							System.out.println("********");
//							pocentity.setAssessableAmount(priceqty);
//						}
////					}
//					
//					/**
//					 * Date 2 August 2017 added by vijay for GST
//					 * below code is applicable only if PO date is less than 1 july 2017 for Cform
//					 * below if condition added by vijay
//					 */
//					if(dbPODate.getValue()!=null && this.dbPODate.getValue().before(date)){	
//						
////					if(!validateState()&&this.getCbcformlis().getItemText(getCbcformlis().getSelectedIndex()).equals(AppConstants.NO)){
////						double assessVal=0;
////						if(salesLineItemLis.get(i).getVat()!=0){
////							assessVal=priceqty+(priceqty*salesLineItemLis.get(i).getVat()/100);
////							pocentity.setAssessableAmount(assessVal);
////						}
////						if(salesLineItemLis.get(i).getVat()==0){
////							pocentity.setAssessableAmount(priceqty);
////						}
////					}
//					
////					if(!validateState()&&this.getCbcformlis().getItemText(getCbcformlis().getSelectedIndex()).equals(AppConstants.YES)){
////						double assessVal=0;
////						if(this.getOlbcstpercent().getSelectedIndex()!=0){
////							TaxDetails cstperc2=olbcstpercent.getSelectedItem();
////							assessVal=priceqty+(priceqty*cstperc2.getTaxChargePercent()/100);
////							pocentity.setAssessableAmount(assessVal);
////						}
////					}
//					
//					}
//					
//					/**
//					 * ends here
//					 */
//					
//					/**
//					 * 
//					 * Updated By Anil Date: 24-11-2016
//					 */
////					if(!validateState()&&this.getCbcformlis().getSelectedIndex()==0){
////						double assessVal=0;
////						if(salesLineItemLis.get(i).getVat()!=0){
////							
////							 /** Date 11 july 2017 if condition added by vijay for GST and old code added in else block 
////							 */
////							if(salesLineItemLis.get(i).getServiceTax().getTaxPrintName()!=null && !salesLineItemLis.get(i).getServiceTax().getTaxPrintName().equals("")){
////								System.out.println("For GST 44 55 ");
////								assessVal=priceqty;
////								pocentity.setAssessableAmount(assessVal);
////							}else{
////							System.out.println("78888888888888");
////							assessVal=priceqty+(priceqty*salesLineItemLis.get(i).getVat()/100);
////							pocentity.setAssessableAmount(assessVal);
////							
////							}
////						}
////						if(salesLineItemLis.get(i).getVat()==0){
////							pocentity.setAssessableAmount(priceqty);
////						}
////					}
//					/**
//					 * END
//					 */
//				}
//				System.out.println("pocentity=="+pocentity.getChargeName());
//				System.out.println(pocentity.getChargePercent());
//				System.out.println(pocentity.getAssessableAmount());
//				this.producttaxtable.getDataprovider().getList().add(pocentity);
//			}
//		}
//	}
	
	
	
	public void addProdTaxesNoState() throws Exception
	{
		List<ProductDetailsPO> salesLineItemLis=this.productTablePO.getDataprovider().getList();
		List<ProductOtherCharges> taxList=this.producttaxtable.getDataprovider().getList();
		NumberFormat nf=NumberFormat.getFormat("0.00");
		String cformval="";
//		int cformindex=this.getCbcformlis().getSelectedIndex();
//		if(cformindex!=0){
//			cformval=this.getCbcformlis().getItemText(cformindex);
//		}
		for(int i=0;i<salesLineItemLis.size();i++)
		{
		
			double priceqty=0,taxPrice=0;
			
			if((salesLineItemLis.get(i).getDiscount()==null || salesLineItemLis.get(i).getDiscount()==0) && (salesLineItemLis.get(i).getDiscountAmt()==0) ){
				
				System.out.println("inside both 0 condition");
				
				taxPrice=this.getProducttable().removeAllTaxes(salesLineItemLis.get(i).getPrduct());
				priceqty=(salesLineItemLis.get(i).getProdPrice()-taxPrice)*salesLineItemLis.get(i).getProductQuantity();
				priceqty=Double.parseDouble(nf.format(priceqty));
			}
			
			else if(((salesLineItemLis.get(i).getDiscount()!=null)&& (salesLineItemLis.get(i).getDiscountAmt()!=0))){
				
				System.out.println("inside both not null condition");
				
				taxPrice=this.getProducttable().removeAllTaxes(salesLineItemLis.get(i).getPrduct());
				priceqty=(salesLineItemLis.get(i).getProdPrice()-taxPrice);
				priceqty=priceqty-(priceqty*salesLineItemLis.get(i).getDiscount()/100);
				
				priceqty=priceqty-salesLineItemLis.get(i).getDiscountAmt();
				
				priceqty=priceqty*salesLineItemLis.get(i).getProductQuantity();
				priceqty=Double.parseDouble(nf.format(priceqty));
			}
			else 
			{
				System.out.println("inside oneof the null condition");
				
				
				taxPrice=this.getProducttable().removeAllTaxes(salesLineItemLis.get(i).getPrduct());
				priceqty=(salesLineItemLis.get(i).getProdPrice()-taxPrice);
				
				if(salesLineItemLis.get(i).getDiscount()!=null){
				priceqty=priceqty-(priceqty*salesLineItemLis.get(i).getDiscount()/100);
				}
				else 
				{
				priceqty=priceqty-salesLineItemLis.get(i).getDiscountAmt();
				}
				priceqty=priceqty*salesLineItemLis.get(i).getProductQuantity();
				priceqty=Double.parseDouble(nf.format(priceqty));
			}
			
			System.out.println("Total Prodcut"+priceqty);
			
			
			if(salesLineItemLis.get(i).getVat()==0){
				ProductOtherCharges pocentity=new ProductOtherCharges();
				pocentity.setChargeName("VAT");
				pocentity.setIndexCheck(i+1);
				int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getVat(),"VAT");
				if(indexValue!=-1){
//					pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
					pocentity.setAssessableAmount(0);
					this.getProducttaxtable().getDataprovider().getList().remove(indexValue);
				}
				
				if(indexValue==-1){
					pocentity.setAssessableAmount(0);
//					pocentity.setAssessableAmount(priceqty);
				}
				this.producttaxtable.getDataprovider().getList().add(pocentity);
			}
			
			if(salesLineItemLis.get(i).getTax()==0){
//				if(validateState()){
				ProductOtherCharges pocentity=new ProductOtherCharges();
				pocentity.setChargeName("Service Tax");
				pocentity.setChargePercent(salesLineItemLis.get(i).getTax());
				pocentity.setIndexCheck(i+1);
				int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getTax(),"Service");
				if(indexValue!=-1){
//					pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
					pocentity.setAssessableAmount(0);
					this.getProducttaxtable().getDataprovider().getList().remove(indexValue);
				}
				
				if(indexValue==-1){
					pocentity.setAssessableAmount(0);
//					pocentity.setAssessableAmount(priceqty);
				}
				this.producttaxtable.getDataprovider().getList().add(pocentity);
			}
			
		/**
		 * Adding Vat Tax to table if the company state and delivery state is equal
		 */
			
			if(salesLineItemLis.get(i).getVat()!=0){
				System.out.println("Vattx");
				ProductOtherCharges pocentity=new ProductOtherCharges();
				pocentity.setChargeName("VAT");
				pocentity.setChargePercent(salesLineItemLis.get(i).getVat());
				pocentity.setIndexCheck(i+1);
				int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getVat(),"VAT");
				
				if(indexValue!=-1){
					System.out.println("Index As Not -1");
					pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
					this.getProducttaxtable().getDataprovider().getList().remove(indexValue);
				}
				if(indexValue==-1){
					System.out.println("VAt");
					pocentity.setAssessableAmount(priceqty);
				}
				this.producttaxtable.getDataprovider().getList().add(pocentity);
			}
			
			/**
			 * Adding service tax to the table
			 */
			
			if(salesLineItemLis.get(i).getTax()!=0){
				System.out.println("ServiceTx");
				ProductOtherCharges pocentity=new ProductOtherCharges();
				pocentity.setChargeName("Service Tax");
				pocentity.setChargePercent(salesLineItemLis.get(i).getTax());
				pocentity.setIndexCheck(i+1);
				int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getTax(),"Service");
				if(indexValue!=-1){
					System.out.println("Index As Not -1");
					
					/**
					 * If validate state method is true
					 */
					
						double assessValue=0;
						if(salesLineItemLis.get(i).getVat()!=0){
							assessValue=priceqty+(priceqty*salesLineItemLis.get(i).getVat()/100);
							pocentity.setAssessableAmount(assessValue+taxList.get(indexValue).getAssessableAmount());
							this.getProducttaxtable().getDataprovider().getList().remove(indexValue);
						}
						if(salesLineItemLis.get(i).getVat()==0){
							pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
							this.getProducttaxtable().getDataprovider().getList().remove(indexValue);
						}
					
					/**
					 * If validate state method is true and cform is submittted.
					 */
					
					
					/**
					 * If validate state method is true and cform is not submitted.
					 */
					
				}
				
				if(indexValue==-1){
					System.out.println("Index As -1");
					
						double assessVal=0;
						if(salesLineItemLis.get(i).getVat()!=0){
							assessVal=priceqty+(priceqty*salesLineItemLis.get(i).getVat()/100);
							pocentity.setAssessableAmount(assessVal);
						}
						if(salesLineItemLis.get(i).getVat()==0){
							pocentity.setAssessableAmount(priceqty);
						}
					
				}
				this.producttaxtable.getDataprovider().getList().add(pocentity);
			}
		}
	}
	
	
	
	
	
	
	/******************************************* Check Tax Percent ****************************************************/
	/**
	 * This method returns 
	 * @param taxValue
	 * @param taxName
	 * @return
	 */
	
	public int checkTaxPercent(double taxValue,String taxName)
	{
//		System.out.println();
//		System.out.println("CHECK TAX PERCENT");
//		System.out.println(taxName+" TAX Value "+taxValue);
		
		List<ProductOtherCharges> taxesList=this.producttaxtable.getDataprovider().getList();
//		System.out.println("TAX LIST SIZE : "+taxesList.size());
		
		
		for(int i=0;i<taxesList.size();i++)
		{
			double listval=taxesList.get(i).getChargePercent();
			int taxval=(int)listval;
			
			if(taxName.equals("Service")){
				if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("Service Tax")){
					System.out.println("ST NZ "+i);
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("Service Tax")){
					System.out.println("ST zero "+i);
					return i;
				}
			}
			if(taxName.equals("VAT")){
				if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("VAT")){
					System.out.println("V NZ "+i);
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("VAT")){
					System.out.println("V zero "+i);
					return i;
				}
			}
			if(taxName.equals("CST")){
				System.out.println("CST");
				if(taxValue==taxesList.get(i).getChargePercent()&&taxesList.get(i).getChargeName().trim().equals("CST")){
					System.out.println("CST "+i);
					return i;
				}
			}
			if(taxName.equals("CGST")){
				if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("CGST")){
					System.out.println("V NZ "+i);
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("CGST")){
					System.out.println("V zero "+i);
					return i;
				}
			}
			if(taxName.equals("SGST")){
				if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("SGST")){
					System.out.println("V NZ "+i);
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("SGST")){
					System.out.println("V zero "+i);
					return i;
				}
			}
			if(taxName.equals("IGST")){
				if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("IGST")){
					System.out.println("V NZ "+i);
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("IGST")){
					System.out.println("V zero "+i);
					return i;
				}
			}
		}
		return -1;
	}
	
	
	/**
	 * This method sets tax label from vat to cst in case cform is not applicable or selected as NO
	 * Date : 24-11-2016 By Anil
	 * Project : DEADSTOCK (NBHC)
	 */
	
	public int checkCstTaxPercentForNoCform(double taxValue,String taxName)
	{
		System.out.println();
		System.out.println("CHECK TAX PERCENT");
		System.out.println(taxName+" TAX Value "+taxValue);
		
		List<ProductOtherCharges> taxesList=this.producttaxtable.getDataprovider().getList();
		System.out.println("TAX LIST SIZE : "+taxesList.size());
		

		for(int i=0;i<taxesList.size();i++)
		{
			double listval=taxesList.get(i).getChargePercent();
			int taxval=(int)listval;
			
//			if(taxName.equals("CST")){
				if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("CST")){
					System.out.println("C NZ "+i);
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("CST")){
					System.out.println("c zero "+i);
					return i;
				}
//			}
		}
		
		for(int i=0;i<taxesList.size();i++)
		{
			double listval=taxesList.get(i).getChargePercent();
			int taxval=(int)listval;
			
			if(taxName.equals("VAT")){
				if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("VAT")){
					System.out.println("V NZ "+i);
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("VAT")){
					System.out.println("V zero "+i);
					return i;
				}
			}
		}
		
		
		return -1;
	}
	
	/**
	 * End
	 */
	
	
	
	
	/***************************************** Update Charges Table *******************************************/
	/**
	 * This method updates the charges table.
	 * The table is updated when any row value is changed through field updater in table i.e
	 * row change event is fired
	 */
	
	public void updateChargesTable()
	{
		List<ProductOtherCharges> prodOtrLis=this.productchargestable.getDataprovider().getList();
		ArrayList<ProductOtherCharges> prodOtrArr=new ArrayList<ProductOtherCharges>();
		prodOtrArr.addAll(prodOtrLis);
		
		this.productchargestable.connectToLocal();
		System.out.println("prodo"+prodOtrArr.size());
		for(int i=0;i<prodOtrArr.size();i++)
		{
			ProductOtherCharges prodChargesEnt=new ProductOtherCharges();
			prodChargesEnt.setChargeName(prodOtrArr.get(i).getChargeName());
			prodChargesEnt.setChargePercent(prodOtrArr.get(i).getChargePercent());
			prodChargesEnt.setChargeAbsValue(prodOtrArr.get(i).getChargeAbsValue());
			//prodChargesEnt.setAssessableAmount(prodOtrArr.get(i).getAssessableAmount());
			prodChargesEnt.setFlagVal(prodOtrArr.get(i).getFlagVal());
			prodChargesEnt.setIndexCheck(prodOtrArr.get(i).getIndexCheck());
			if(prodOtrArr.get(i).getFlagVal()==false){
				prodChargesEnt.setAssessableAmount(this.getDoamtincltax().getValue());
			}
			if(prodOtrArr.get(i).getFlagVal()==true){
				double assesableVal=this.retrieveSurchargeAssessable(prodOtrArr.get(i).getIndexCheck());
				prodChargesEnt.setAssessableAmount(assesableVal);
			}
			
			this.productchargestable.getDataprovider().getList().add(prodChargesEnt);
		}
	}
	
	public double retrieveSurchargeAssessable(int indexValue)
	{
		List<ProductOtherCharges> otrChrgLis=this.productchargestable.getDataprovider().getList();
		ArrayList<ProductOtherCharges> otrChrgArr=new ArrayList<ProductOtherCharges>();
		otrChrgArr.addAll(otrChrgLis);
	
		double getAssessVal=0;
		for(int i=0;i<otrChrgArr.size();i++)
		{
			if(otrChrgArr.get(i).getFlagVal()==false&&otrChrgArr.get(i).getIndexCheck()==indexValue)
			{
				if(otrChrgArr.get(i).getChargePercent()!=0){
					getAssessVal=otrChrgArr.get(i).getAssessableAmount()*otrChrgArr.get(i).getChargePercent()/100;
				}
				if(otrChrgArr.get(i).getChargeAbsValue()!=0){
					getAssessVal=otrChrgArr.get(i).getChargeAbsValue();
				}
			}
		}
		return getAssessVal;
	}

	
	/************************************* Set All Table *****************************************************/
	public void setAllTable() {
		if (productTablePO.getDataprovider().getList().size() != 0) {
			double total = productTablePO.calculateTotal();
			dbTotal.setValue(total);
			// setTaxTable();
		}
		if (producttaxtable.getDataprovider().getList().size() != 0) {
//			 setGrandTotal();
		}
		if (productchargestable.getDataprovider().getList().size() != 0) {
			System.out.println("change product charge table");
			List<ProductOtherCharges> list = productchargestable.getDataprovider().getList();
			for (ProductOtherCharges temp : list) {
				temp.setAssessableAmount(doamtincltax.getValue());
			}
			productchargestable.getTable().redraw();
		}

	}

	/**************************************** Get Company Address **************************************************/
	
	/**
	 * This method is used to set company address to address composite and called in the form constructor.
	 * @param count
	 */

	public void getCompanyAddress(Long count) {
		Filter temp = new Filter();
		temp.setQuerryString("companyId");
		temp.setLongValue(count);
		final MyQuerry querry = new MyQuerry();
		querry.getFilters().add(temp);
		querry.setQuerryObject(new Company());

		 Timer timer=new Timer() 
    	 {
			@Override
			public void run() 
			{
				async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onFailure(Throwable caught) {
						showDialogMessage("An Unexpected Error occured !");
					}
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						for (SuperModel smodel : result) {
							Company comentity = (Company) smodel;
							deliveryadd.setValue(comentity.getAddress());
							deliveryadd.setEnable(false);
							deliveryaddress.setValue("Company Address");
						}
					}
				});
			}
    	 };
    	 timer.schedule(3000); 
	}
	// *********************************** Checking Product************************************************ 
	public boolean checkProducts(String pcode) {
		List<ProductDetailsPO> salesitemlis = productTablePO.getDataprovider().getList();
		for (int i = 0; i < salesitemlis.size(); i++) {
			if (salesitemlis.get(i).getProductCode().equals(pcode)|| salesitemlis.get(i).getProductCode() == pcode) {
				return true;
			}
		}
		return false;
	}

	// ****************************** Method For Adding product Details With Price List In Table *********************
	
	public void ProdctType(String pcode,String productId) {
		System.out.println("Started");
	
		final GenricServiceAsync genasync = GWT.create(GenricService.class);
		final int prodId=Integer.parseInt(productId);
		
		final MyQuerry querry = new MyQuerry();
		Company c=new Company();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		filter = new Filter();
		filter.setQuerryString("productCode");
		filter.setStringValue(pcode.trim());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(prodId);
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);
		
		filter=new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new SuperProduct());
//		showWaitSymbol();
//		Timer timer = new Timer() {
//			@Override
//			public void run() {
				genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {

					@Override
					public void onFailure(Throwable caught) {
						hideWaitSymbol();
						showDialogMessage("An Unexpected error occurred!");
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						
						if(result.size()==0)
						{
							showDialogMessage("Please check whether product status is active or not!");
						}
							
						for (SuperModel model : result) {
							SuperProduct superProdEntity = (SuperProduct) model;
							final ProductDetailsPO lis = AppUtility.ReactOnAddPurchaseProductComposite(superProdEntity);
							lis.setProdDate(dbExpdeliverydate.getValue());
							/**
							 * Date : 11-12-2018 BY ANIL
							 * Setting default product quantity to 1
							 */
							lis.setProductQuantity(1);
							lis.setProdPrice(superProdEntity.getPrice());
							checkAndAddVendorToVendorTable(lis,false);
						}
						Console.log("Out Of Success");
						hideWaitSymbol();
					}
				});
		
//	}
//	};
//	timer.schedule(3000);
	}
	
	protected boolean checkForCommonVendor(List<PriceListDetails> vendorList,int vendorId)
	{
		boolean vendorCheckFlag=false;
		int ctr=0;
		for(int h=0;h<vendorList.size();h++)
		{
			if(vendorList.get(h).getVendorID()==vendorId){
				ctr=ctr+1;
			}
		}
		
		if(ctr>0){
			vendorCheckFlag=true;
		}
		
		return vendorCheckFlag;
	}
	
	
	// ************************************ Initializing Cst percent ***********************************************
	
//	public void initializePercentCst() {
//		Company comEntity=new Company();
//		MyQuerry querry = new MyQuerry();
//		
//		Vector<Filter> filter = new Vector<Filter>();
//		Filter temp = null;
//
//		temp = new Filter();
//		System.out.println("Cid"+comEntity.getCompanyId());
//		temp.setQuerryString("companyId");
//		temp.setLongValue(comEntity.getCompanyId());
//		filter.add(temp);
//
//		temp = new Filter();
//		temp.setQuerryString("isCentralTax");
//		temp.setBooleanvalue(true);
//		filter.add(temp);
//
//		temp = new Filter();
//		temp.setQuerryString("taxChargeStatus");
//		temp.setBooleanvalue(true);
//		filter.add(temp);
//
//		querry.setFilters(filter);
//		querry.setQuerryObject(new TaxDetails());
//		olbcstpercent.MakeLive(querry);
//	}

	
	/*******************************************Type Drop Down Logic**************************************/
	
	@Override
	public void onChange(ChangeEvent event) {
		if(event.getSource().equals(oblPurchaseOrderGroup))
		{
			if(oblPurchaseOrderGroup.getSelectedIndex()!=0){
				ConfigCategory cat=oblPurchaseOrderGroup.getSelectedItem();
				if(cat!=null){
					AppUtility.makeLiveTypeDropDown(oblPurchaseOrderType, cat.getCategoryName(), cat.getCategoryCode(), cat.getInternalType());
				}
			}
		}	
			/**
			 * Date 08/06/2018
			 * Developer :- Vijay
			 * Des :- for round off amount chnage calculation and change net payable accourdingly
			 */
			if(event.getSource().equals(tbroundoffAmt)){
				NumberFormat nf = NumberFormat.getFormat("0.00");
				double netPay=this.getDbfinalTotalAmt().getValue();
				this.getDbfinalTotalAmtWithoutRoundoff().setValue(netPay);//Ashwini Patil Date:27-05-2023
				this.getDbfinalTotalAmt().setValue(netPay);
				if(tbroundoffAmt.getValue().equals("") || tbroundoffAmt.getValue()==null){
					this.getDonetpayamt().setValue(netPay);
				}else{
					if(!tbroundoffAmt.getValue().equals("")&& tbroundoffAmt.getValue()!=null){
						String roundOff = tbroundoffAmt.getValue();
						double roundoffAmt =  AppUtility.calculateRoundOff(roundOff, netPay);
						if(roundoffAmt!=0){
							this.getDonetpayamt().setValue(roundoffAmt);
						}else{
							this.getDonetpayamt().setValue(netPay);
							this.getTbroundoffAmt().setValue("");
						}
					}
				}
			}
			/**
			 * ends here
			 */
			/** date 22.10.2018 added by komal to load branch warehouse **/
			if(event.getSource().equals(branch)){
				if(branch.getSelectedIndex()!= 0){
					loadWareHouseList(branch.getValue(branch.getSelectedIndex()));
					List<ProductDetailsPO> list = productTablePO.getDataprovider().getList();
					Branch branchEntity = branch.getSelectedItem();
					PurchaseOrderPresenter.updateTaxesDetails(list,branchEntity,PurchaseOrderPresenter.vendorState);
					RowCountChangeEvent.fire(productTablePO.getTable(), productTablePO.getDataprovider().getList().size(), true);
					setEnable(true);
				}
			}
			/**
			 * @author Vijay Date -19-08-2023
			 * Des - added validation
			 */
			if(event.getSource().equals(ibLetterOfIntentID)){
				try {
					if(ibLetterOfIntentID.getValue()!=null && !ibLetterOfIntentID.getValue().equals(""))
					Integer.parseInt(ibLetterOfIntentID.getValue());
				} catch (Exception e) {
					showDialogMessage("Please add only number");
				}
			}
			if(event.getSource().equals(ibreqforQuotation)){
				try {
					if(ibreqforQuotation.getValue()!=null && !ibreqforQuotation.getValue().equals(""))
					Integer.parseInt(ibreqforQuotation.getValue());
				} catch (Exception e) {
					showDialogMessage("Please add only number");
				}
			}
			if(event.getSource().equals(purchaseRequisiteNo)){
				try {
					if(purchaseRequisiteNo.getValue()!=null && !purchaseRequisiteNo.getValue().equals(""))
					Integer.parseInt(purchaseRequisiteNo.getValue());
				} catch (Exception e) {
					showDialogMessage("Please add only number");
				}
			}
			/**
			 * ends here
			 */
	
			if(event.getSource().equals(deliveryaddress)){
				if(deliveryaddress.getValue(deliveryaddress.getSelectedIndex()).equals("Company Address")){
					getAndSetCompanyAddress();
				}
				if(deliveryaddress.getValue(deliveryaddress.getSelectedIndex()).equals("Branch Address")){
					if(branch.getSelectedIndex()==0){
						showDialogMessage("Please select branch");
					}
					else{
						getAndSetBranchAddress();
					}
				}
				if(deliveryaddress.getValue(deliveryaddress.getSelectedIndex()).equals("Warehouse Address")){
					if(this.productTablePO.getDataprovider().getList().size()>0){
						if(this.productTablePO.getDataprovider().getList().get(0).getWarehouseName()!=null && 
								!this.productTablePO.getDataprovider().getList().get(0).getWarehouseName().equals("")){
							deliveryadd.setValue(ProductTablePO.getWarehouseAddress(this.productTablePO.getDataprovider().getList().get(0).getWarehouseName()));
							deliveryadd.setEnable(false);
						}
						else{
							showDialogMessage("Please select warehouse name");
						}
						
					}
					else{
						showDialogMessage("Please add product first!");
					}
					
				}
				if(deliveryaddress.getValue(deliveryaddress.getSelectedIndex()).equals("Other")){
					deliveryadd.clear();
					deliveryadd.setEnable(true);
				}

			}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/***************************** NEW VENDOR SELECTION LOGIC BY ANIL **********************************/
	
	
	
	
	

	





	/**
	 * This method takes product details which we are adding in product table as input and 
	 * checks whether vendor price list for that product is exist or not
	 * if exist then checks for common vendor and add product and vendor to their respective table.
	 * 
	 * Date : 10-09-2016 By Anil
	 * Release :31 Aug 2016
	 * Project:PURCHASE ERROR SOLVING (NBHC)
	 * I/P:Product of type ProductDetails
	 */
	
	
	public void checkAndAddVendorToVendorTable(final ProductDetailsPO prodDetails,final boolean isCallFromPurchaseRequision){
		
		System.out.println("One");
		
		MyQuerry querry=new MyQuerry();
		Filter filter = new Filter();
		filter.setQuerryString("prodCode");
//		filter.setStringValue(prodInfoComposite.getProdCode().getValue().trim());
		filter.setStringValue(prodDetails.getProductCode().trim());
		querry.getFilters().add(filter);
		querry.setQuerryObject(new PriceList());
		System.out.println("Two");
		
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				addproducts.setEnabled(true);
			}
			@Override
			public void onSuccess(ArrayList<SuperModel> vresult) {
				Console.log("VEN PRICE LIS SIZE : "+vresult.size());
				addproducts.setEnabled(true);
				if (vresult.size() != 0) {
					for(SuperModel smodel:vresult){
						System.out.println("Three");
						PriceList pentity=(PriceList) smodel;
//						if(isCommonVendor(pentity.getPriceInfo())){
							System.out.println("Four");
							
							vendorRateList.addAll(pentity.getPriceInfo());
							
							if(validateVendorselect()){
								int vendCount=retrieveVendorCount();
								for(PriceListDetails price:vendorRateList){
									if(price.getVendorID()==vendCount&&price.getProdID()==prodDetails.getProductID()){
										prodDetails.setProdPrice(price.getProductPrice());
									}
								}
							}
							/** date 22.10.2018 added by komal **/
							if(branch.getSelectedIndex() == 0){
								showDialogMessage("Please select branch.");
								return;
							}else{
								if(globalWareHouseList.size() == 1){
									byDefaultSelectionOfWareHouse(prodDetails,isCallFromPurchaseRequision);
								}else{
									if(!isCallFromPurchaseRequision)
										productTablePO.getDataprovider().getList().add(prodDetails);
								}
							}
							
//						}else{
//							System.out.println("Five");
//							showDialogMessage("No common vendor for this product !");
//							return;
//						}
						addUniqueVendorsToTable(pentity.getPriceInfo());
					}
				}else{
					System.out.println("Six");
//					showDialogMessage("Vendor Price List does not exists for product");
					
					/**
					 * @author Vijay Date :- 04-05-2023
					 * Des :- updated code if pricelist not defined allow to add product
					 */
					if(!isCallFromPurchaseRequision)
						productTablePO.getDataprovider().getList().add(prodDetails);
					
					/**
					 * ends here
					 */
				
				}
			}
		});		
	}
	
	/**
	 * This method checks whether vendors are common or not for added product if not then it will not allow you add product
	 * Date 10-09-2016 By Anil
	 * Release : 31 Aug 2016
	 * Project: PURCHASE ERROR SOLVING(NBHC)
	 * I/P:Current added products vendor price list and checks with  already added vendor list
	 * O/P: return true if at least one vendor common else false
	 */

	private boolean isCommonVendor(List<PriceListDetails>venRateLis) {
		System.out.println("CHECKING COMMON VENDOR ");
		System.out.println("VEN TBL SIZE "+vendorTablePO.getDataprovider().getList().size());
		if(vendorTablePO.getDataprovider().getList().size()==0){
			return true;
		}
		for(VendorDetails gblVenLis:vendorTablePO.getDataprovider().getList()){
			for(PriceListDetails currVenLis:venRateLis){
				System.out.println("VENDOR ID "+gblVenLis.getVendorId() +"    --   "+currVenLis.getVendorID());
				if(gblVenLis.getVendorId()==currVenLis.getVendorID()){
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * This method adds only common vendor to vendor table 
	 * Date 10-09-2016 By Anil
	 * Release : 31 Aug 2016
	 * Project: PURCHASE ERROR SOLVING(NBHC)
	 * I/P:Current added products vendor list and checks with already added vendor list,maintain only common vendors list else all removed 
	 */
	
	private void addUniqueVendorsToTable(List<PriceListDetails>venRateLis) {
		System.out.println("ADDING UNIQUE VENDOR");
		if(vendorTablePO.getDataprovider().getList().size()==0){
			System.out.println("ADDING FOR 1ST TIME");
			ArrayList<VendorDetails> venList=getVendorList(venRateLis);	
			vendorTablePO.getDataprovider().getList().addAll(venList);
		}else{
			System.out.println("ADDING ");
			ArrayList<VendorDetails> commonVenList=getCommonVendorList(vendorTablePO.getDataprovider().getList(),venRateLis);
			vendorTablePO.getDataprovider().getList().clear();
			vendorTablePO.getDataprovider().getList().addAll(commonVenList);
		}
		
		System.out.println("FINAL VENDOR LIST SIZE "+vendorTablePO.getDataprovider().getList().size());
		if(vendorTablePO.getDataprovider().getList().size()>0){
			List<VendorDetails> vendorlist = vendorTablePO.getDataprovider().getList();

			for(VendorDetails vendorDetails : vendorlist ){
				if(vendorDetails.getStatus()){
					vendorDetails.setStatus(false);
				}
			}
			vendorTablePO.getDataprovider().setList(vendorlist);
		}

		
	}
	
	/**
	 * This method returns common vendors list by comparing from existing and currently to be added vendor list.
	 * Date 10-09-2016 By Anil
	 * Release : 31 Aug 2016
	 * Project: PURCHASE ERROR SOLVING(NBHC)
	 * I/P:existing vendor list and currently added vendor price list
	 * O/P: common vendor list
	 */
	
	private ArrayList<VendorDetails> getCommonVendorList(List<VendorDetails> vendorList,List<PriceListDetails> venRateLis) {
		System.out.println("CHECKING ADDING COMMON VENDOR ONLY ");
		ArrayList<VendorDetails> vendList=new ArrayList<VendorDetails>();
		for(VendorDetails venDet:vendorList){
			for(PriceListDetails venRLis:venRateLis){
				if(venDet.getVendorId()==venRLis.getVendorID()){
					vendList.add(venDet);
				}
			}
		}
		System.out.println("VEN LIS SIZE "+vendList.size());
		return vendList;
	}
	
	
	
	/**
	 * This method convert the vendor list from  type PriceListDetails to type VendorDetails
	 * Date 10-09-2016 By Anil
	 * Release : 31 Aug 2016
	 * Project: PURCHASE ERROR SOLVING(NBHC)
	 * I/P:vendor list of type PriceListDetails
	 * O/P: vendor list of type VendorDetails 
	 */
	
	public ArrayList<VendorDetails> getVendorList(List<PriceListDetails> venRateLis) {
		System.out.println("CONVERTING LIST.....");
		ArrayList<VendorDetails> vendorList=new ArrayList<VendorDetails>();
		
		for (PriceListDetails temp : venRateLis) {
			VendorDetails ve = new VendorDetails();
			ve.setVendorEmailId(temp.getEmail());
			ve.setVendorId(temp.getVendorID());
			ve.setVendorName(temp.getFullName());
			ve.setVendorPhone(temp.getCellNumber());
			
			/**
			 * Date : 06-02-2017 By Anil
			 * Added POC Name 
			 */
			ve.setPocName(temp.getPocName());
			/**
			 * End
			 */
			vendorList.add(ve);
		}
		return vendorList;
	}
	
	
	
	

	/**
	 * This method load global vendor list when we are viewing the records by searching.
	 * Date: 10-09-2016
	 * Release : 31 Aug 2016
	 * Project: PURCHASE ERROR SOLVING(NBHC)
	 * I/P: List of product code
	 * 
	 */
	public void loadVendorProductPriceList(List<String> prodIdList) {
		
		
		for(String obj:prodIdList){
			System.out.println("CODE : "+obj);
		}
		MyQuerry querry=new MyQuerry();
		Filter filter=null;
		Vector<Filter>temp=new Vector<Filter>();
		
		filter=new Filter();
		filter.setList(prodIdList);
		filter.setQuerryString("prodCode IN");
		temp.add(filter);
		
		filter=new Filter();
		filter.setLongValue(UserConfiguration.getCompanyId());
		filter.setQuerryString("companyId");
		temp.add(filter);
		
		
		
		querry.setFilters(temp);
		querry.setQuerryObject(new PriceList());
		
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				System.out.println("LOAD RESULT "+result.size());
				vendorRateList=new ArrayList<PriceListDetails>();
				for(SuperModel model:result){
					PriceList entity=(PriceList) model;
					vendorRateList.addAll(entity.getPriceInfo());
				}
				System.out.println("GLOBAL VEN LIST SIZE IN LOAD "+vendorRateList.size());
			}
		});
		
		
		
	}
	
	
	
	
	
	/**
	 * This method is called when you are adding vendor 
	 * this method checks whether price list of vendor for added product is defined or not.
	 * Date : 29-09-2016 By Anil
	 * Release : 30 Sept 2016
	 * Project : PURCHASE MODIFICATION (NBHC)
	 */
	public void checkPriceListAndAddVendorToVendorTable() {
		System.out.println("INSIDE VENDOR ADD CODE ");
		List<ProductDetailsPO> list=productTablePO.getDataprovider().getList();
		HashSet<String> hashList=new HashSet<String>();
		final ArrayList<String> codeList=new ArrayList<String>();
		for(ProductDetailsPO obj:list){
			hashList.add(obj.getProductCode());
		}
		codeList.addAll(hashList);
		
		System.out.println("CODE LIST SIZE "+codeList.size());
		Vector<Filter>temp=new Vector<Filter>();
		MyQuerry querry=new MyQuerry();
		Filter filter =null;
		
		filter = new Filter();
		filter.setQuerryString("prodCode IN");
		filter.setList(codeList);
		temp.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("priceInfo.personinfo.count");
		filter.setIntValue(vendorComp.getIdValue());
		temp.add(filter);
		
		
		querry.setFilters(temp);
		querry.setQuerryObject(new PriceList());
		showWaitSymbol();
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				hideWaitSymbol();
			}
			@Override
			public void onSuccess(ArrayList<SuperModel> vresult) {
				hideWaitSymbol();
				Console.log("VEN PRICE LIS SIZE : "+vresult.size());
				if (vresult.size() != 0) {
					ArrayList<PriceList> priceList=new ArrayList<PriceList>();
					for(SuperModel smodel:vresult){
						PriceList pentity=(PriceList) smodel;
						priceList.add(pentity);
					}	
					
					if(priceList.size()==codeList.size()){
						VendorDetails ve = new VendorDetails();
						
						for(PriceList entity:priceList){
							vendorRateList.addAll(entity.getPriceInfo());
							
							for(PriceListDetails obj:entity.getPriceInfo()){
								if(obj.getVendorID()==vendorComp.getIdValue()){
									ve.setVendorEmailId(obj.getEmail());
									ve.setVendorId(obj.getVendorID());
									ve.setVendorName(obj.getFullName());
									ve.setVendorPhone(obj.getCellNumber());
									
									/**
									 * Date : 06-02-2017 By ANIL
									 * Added vendor POC Name
									 */
									ve.setPocName(obj.getPocName());
									/**
									 * End
									 */
									
									/**
									 * Updated on 24-11-2016 By Anil
									 */
									if(vendorTablePO.getDataprovider().getList().size()==0){
										ve.setStatus(true);
									}
									/**
									 * End
									 */
								}
							}
						}
						/**
						 * Updated on 24-11-2016 By Anil
						 */
						if(ve.getStatus()==true){
							PurchaseOrderPresenter.retVenStateFlage=true;
						}
						/**
						 * End
						 */
						
						vendorTablePO.getDataprovider().getList().add(ve);
						vendorComp.clear();
						
						
					}
					else{
						
						

						System.out.println("Six");
//						showDialogMessage("Vendor Price List does not exists.");
//						return;
						
						/**
						 * @author Vijay Date :- 04-05-2023
						 * Des :- updated code if pricelist not defined allow to add product
						 */
						
						MyQuerry querry = new MyQuerry();
						Vector<Filter> filtervec = new Vector<Filter>();
						Filter tempfilter = null;

					
						tempfilter = new Filter();
						tempfilter.setQuerryString("count");
						tempfilter.setIntValue(vendorComp.getIdValue());
						filtervec.add(tempfilter);

						querry.getFilters().add(tempfilter);
						querry.setQuerryObject(new Vendor());
						
						async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
							
							@Override
							public void onSuccess(ArrayList<SuperModel> result) {
								// TODO Auto-generated method stub
								
								for(SuperModel model : result){
									Vendor vendor = (Vendor) model;
									VendorDetails ve = new VendorDetails();

									if(vendor.getEmail()!=null)
									ve.setVendorEmailId(vendor.getEmail());
									ve.setVendorId(vendor.getCount());
//									ve.setVendorName(vendor.getfullName());
									ve.setVendorName(vendor.getVendorName());//Ashwini Patil Date:30-10-2023 vendor poc name was getting added to the vendor table. issue reported by many clients
									if(vendor.getCellNumber1()!=null)
									ve.setVendorPhone(vendor.getCellNumber1());
									
									/**
									 * Date : 06-02-2017 By ANIL
									 * Added vendor POC Name
									 */
									ve.setPocName(vendor.getPointOfContact().getFullname());
									/**
									 * End
									 */
									
									/**
									 * Updated on 24-11-2016 By Anil
									 */
									if(vendorTablePO.getDataprovider().getList().size()==0){
										ve.setStatus(true);
									}
									
									vendorTablePO.getDataprovider().getList().add(ve);
									vendorComp.clear();
								}
								
								
							}
							
							@Override
							public void onFailure(Throwable caught) {
								// TODO Auto-generated method stub
								
							}
						});
						
						/**
						 * ends here
						 */
					
//						
//						ArrayList<String> list=new ArrayList<String>();
//						for(String obj:codeList){
//							boolean flag=false;
//							for(PriceList lisObj:priceList){
//								if(lisObj.getProdCode().equals(obj)){
//									flag=true;
//									break;
//								}
//							}
//							if(!flag){
//								list.add(obj);
//							}
//						}
//						
//						String prodName="";
//						for(String obj:list){
//							prodName=prodName+getProductNameFromCode(obj)+"\n";
//						}
//						
//						showDialogMessage("Vendor price list does not exist for following products."+"\n"+prodName);
//						return;
						
					}
				}
				else{
					System.out.println("Six");
//					showDialogMessage("Vendor Price List does not exists.");
//					return;
					
					
					/**
					 * @author Vijay Date :- 04-05-2023
					 * Des :- updated code if pricelist not defined allow to add product
					 */
					
					
					MyQuerry querry = new MyQuerry();
					Vector<Filter> filtervec = new Vector<Filter>();
					Filter tempfilter = null;

				
					tempfilter = new Filter();
					tempfilter.setQuerryString("count");
					tempfilter.setIntValue(vendorComp.getIdValue());
					filtervec.add(tempfilter);

					querry.getFilters().add(tempfilter);
					querry.setQuerryObject(new Vendor());
					
					async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
						
						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							// TODO Auto-generated method stub
							
							for(SuperModel model : result){
								Vendor vendor = (Vendor) model;
								VendorDetails ve = new VendorDetails();

								if(vendor.getEmail()!=null)
								ve.setVendorEmailId(vendor.getEmail());
								ve.setVendorId(vendor.getCount());
//								ve.setVendorName(vendor.getfullName());
								ve.setVendorName(vendor.getVendorName());//Ashwini Patil Date:30-10-2023 vendor poc name was getting added to the vendor table. issue reported by many clients
								if(vendor.getCellNumber1()!=null)
								ve.setVendorPhone(vendor.getCellNumber1());
								
								/**
								 * Date : 06-02-2017 By ANIL
								 * Added vendor POC Name
								 */
								ve.setPocName(vendor.getPointOfContact().getFullname());
								/**
								 * End
								 */
								
								/**
								 * Updated on 24-11-2016 By Anil
								 */
								if(vendorTablePO.getDataprovider().getList().size()==0){
									ve.setStatus(true);
								}
								
								vendorTablePO.getDataprovider().getList().add(ve);
								vendorComp.clear();
							}
							
							
						}
						
						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							
						}
					});
					
					/**
					 * ends here
					 */
				}
				
			}
		});				
	}
	
	
	private String getProductNameFromCode(String code){
		List<ProductDetailsPO> list=productTablePO.getDataprovider().getList();
		for(ProductDetailsPO obj:list){
			if(obj.getProductCode().equals(code)){
				return obj.getProductName();
			}
		}
		return null;
	}

	/**
	 * This method checks whether vendor is already added or not
	 * if already added then returns false else true
	 * Date : 28-09-2016 By Anil
	 * Release : 30 Sept 2016
	 * Project : PURCHASE MODIFICATION(NBHC)
	 */

	public boolean validVendor() {
		
//		List<ProductDetailsPO> prodList=productTablePO.getDataprovider().getList();
//		if(prodList.size()==0){
//			showDialogMessage("Please add product first !");
//			return false;
//		}
		
		List<VendorDetails> list=vendorTablePO.getDataprovider().getList();
		int vendorId=vendorComp.getIdValue();
		
		for(VendorDetails obj:list){
			if(obj.getVendorId()==vendorId){
				showDialogMessage("Vendor already added !");
				return false;
			}
		}
		return true;
	}
	
	/**
	 * End
	 */
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	/******************************** Getter and Setter ************************************************/
	public CheckBox getCbadds() {
		return cbadds;
	}

	public void setCbadds(CheckBox cbadds) {
		this.cbadds = cbadds;
	}

	public AddressComposite getDeliveryadd() {
		return deliveryadd;
	}

	public void setDeliveryadd(AddressComposite deliveryadd) {
		this.deliveryadd = deliveryadd;
	}

	public TextBox getIbLetterOfIntentID() {
		return ibLetterOfIntentID;
	}


	public void setIbLetterOfIntentID(TextBox ibLetterOfIntentID) {
		this.ibLetterOfIntentID = ibLetterOfIntentID;
	}


	public TextBox getIbreqforQuotation() {
		return ibreqforQuotation;
	}


	public void setIbreqforQuotation(TextBox ibreqforQuotation) {
		this.ibreqforQuotation = ibreqforQuotation;
	}


	public TextBox getIbPOId() {
		return ibPOId;
	}


	public void setIbPOId(TextBox ibPOId) {
		this.ibPOId = ibPOId;
	}


//	public TextBox getTbName() {
//		return tbName;
//	}
//
//	public void setTbName(TextBox tbName) {
//		this.tbName = tbName;
//	}

	public ProductTablePO getProducttable() {
		return productTablePO;
	}

	public void setProducttable(ProductTablePO productTablePO) {
		this.productTablePO = productTablePO;
	}

	public VendorTablePO getVendortbl() {
		return vendorTablePO;
	}

	public void setVendortbl(VendorTablePO vendorTablePO) {
		this.vendorTablePO = vendorTablePO;
	}

	public DateBox getDbRfqDate() {
		return dbRfqDate;
	}

	public void setDbRfqDate(DateBox dbRfqDate) {
		this.dbRfqDate = dbRfqDate;
	}

//	public DateBox getDbCreationDate() {
//		return dbCreationDate;
//	}
//
//	public void setDbCreationDate(DateBox dbCreationDate) {
//		this.dbCreationDate = dbCreationDate;
//	}

	public DateBox getDbExpectedResposeDate() {
		return dbExpectedResposeDate;
	}

	public void setDbExpectedResposeDate(DateBox dbExpectedResposeDate) {
		this.dbExpectedResposeDate = dbExpectedResposeDate;
	}

//	public DateBox getDbExpectedClosureDate() {
//		return dbExpectedClosureDate;
//	}
//
//	public void setDbExpectedClosureDate(DateBox dbExpectedClosureDate) {
//		this.dbExpectedClosureDate = dbExpectedClosureDate;
//	}

	public DateBox getDbLOIDate() {
		return dbLOIDate;
	}

	public void setDbLOIDate(DateBox dbLOIDate) {
		this.dbLOIDate = dbLOIDate;
	}

	public DateBox getDbPODate() {
		return dbPODate;
	}

	public void setDbPODate(DateBox dbPODate) {
		this.dbPODate = dbPODate;
	}

	public TextBox getIbRefOrderNo() {
		return ibRefOrderNo;
	}

	public void setIbRefOrderNo(TextBox ibRefOrderNo) {
		this.ibRefOrderNo = ibRefOrderNo;
	}

	public ObjectListBox<Employee> getOblEmployee() {
		return oblEmployee;
	}

	public void setOblEmployee(ObjectListBox<Employee> oblEmployee) {
		this.oblEmployee = oblEmployee;
	}

	public ObjectListBox<ConfigCategory> getOblPurchaseOrderGroup() {
		return oblPurchaseOrderGroup;
	}

	public void setOblPurchaseOrderGroup(
			ObjectListBox<ConfigCategory> oblPurchaseOrderGroup) {
		this.oblPurchaseOrderGroup = oblPurchaseOrderGroup;
	}

	public ObjectListBox<Type> getOblPurchaseOrderType() {
		return oblPurchaseOrderType;
	}

	public void setOblPurchaseOrderType(ObjectListBox<Type> oblPurchaseOrderType) {
		this.oblPurchaseOrderType = oblPurchaseOrderType;
	}

	public TextBox getTbStatus() {
		return tbStatus;
	}

	public void setTbStatus(TextBox tbStatus) {
		this.tbStatus = tbStatus;
	}

	public TextBox getTbpoName() {
		return tbpoName;
	}

	public void setTbpoName(TextBox tbpoName) {
		this.tbpoName = tbpoName;
	}

	public TextArea getHeader() {
		return header;
	}

	public void setHeader(TextArea header) {
		this.header = header;
	}

	public TextArea getFooter() {
		return footer;
	}

	public void setFooter(TextArea footer) {
		this.footer = footer;
	}

	public PaymentTermsTbl getPaymentTermsTable() {
		return paymentTermsTable;
	}

	public void setPaymentTermsTable(PaymentTermsTbl paymentTermsTable) {
		this.paymentTermsTable = paymentTermsTable;
	}

	public IntegerBox getIbdays() {
		return ibdays;
	}

	public void setIbdays(IntegerBox ibdays) {
		this.ibdays = ibdays;
	}

	public DoubleBox getDopercent() {
		return dopercent;
	}

	public void setDopercent(DoubleBox dopercent) {
		this.dopercent = dopercent;
	}

	public TextBox getTbcomment() {
		return tbcomment;
	}

	public void setTbcomment(TextBox tbcomment) {
		this.tbcomment = tbcomment;
	}

	public ObjectListBox<Branch> getBranch() {
		return branch;
	}

	public void setBranch(ObjectListBox<Branch> branch) {
		this.branch = branch;
	}

	public ObjectListBox<Employee> getTbApporverName() {
		return tbApporverName;
	}

	public void setTbApporverName(ObjectListBox<Employee> tbApporverName) {
		this.tbApporverName = tbApporverName;
	}

	public ObjectListBox<HrProject> getProject() {
		return project;
	}

	public void setProject(ObjectListBox<HrProject> project) {
		this.project = project;
	}

	public TextBox getPurchaseRequisiteNo() {
		return purchaseRequisiteNo;
	}


	public void setPurchaseRequisiteNo(TextBox purchaseRequisiteNo) {
		this.purchaseRequisiteNo = purchaseRequisiteNo;
	}


	public DateBox getDbExpdeliverydate() {
		return dbExpdeliverydate;
	}

	public void setDbExpdeliverydate(DateBox dbExpdeliverydate) {
		this.dbExpdeliverydate = dbExpdeliverydate;
	}

//	public ObjectListBox<WareHouse> getOblwarehouse() {
//		return oblwarehouse;
//	}
//
//	public void setOblwarehouse(ObjectListBox<WareHouse> oblwarehouse) {
//		this.oblwarehouse = oblwarehouse;
//	}

	public Label getLbtotal() {
		return lbtotal;
	}

	public void setLbtotal(Label lbtotal) {
		this.lbtotal = lbtotal;
	}

	public DoubleBox getDbTotal() {
		return dbTotal;
	}

	public void setDbTotal(DoubleBox dbTotal) {
		this.dbTotal = dbTotal;
	}

	public ProductTaxesTable getProducttaxtable() {
		return producttaxtable;
	}

	public void setProducttaxtable(ProductTaxesTable producttaxtable) {
		this.producttaxtable = producttaxtable;
	}

	public ProductChargesTable getProductchargestable() {
		return productchargestable;
	}

	public void setProductchargestable(ProductChargesTable productchargestable) {
		this.productchargestable = productchargestable;
	}

	public TextBox getTbblanck() {
		return tbblanck;
	}

	public void setTbblanck(TextBox tbblanck) {
		this.tbblanck = tbblanck;
	}

	public DateBox getPrDate() {
		return prDate;
	}

	public void setPrDate(DateBox prDate) {
		this.prDate = prDate;
	}

	public ProductInfoComposite getProdInfoComposite() {
		return prodInfoComposite;
	}

	public void setProdInfoComposite(ProductInfoComposite prodInfoComposite) {
		this.prodInfoComposite = prodInfoComposite;
	}

//	public ObjectListBox<TaxDetails> getOlbcstpercent() {
//		return olbcstpercent;
//	}
//
//	public void setOlbcstpercent(ObjectListBox<TaxDetails> olbcstpercent) {
//		this.olbcstpercent = olbcstpercent;
//	}
//
//	public ListBox getCbcformlis() {
//		return cbcformlis;
//	}
//
//	public void setCbcformlis(ListBox cbcformlis) {
//		this.cbcformlis = cbcformlis;
//	}

	public DoubleBox getDototalamt() {
		return dototalamt;
	}

	public void setDototalamt(DoubleBox dototalamt) {
		this.dototalamt = dototalamt;
	}

	public DoubleBox getDoamtincltax() {
		return doamtincltax;
	}

	public void setDoamtincltax(DoubleBox doamtincltax) {
		this.doamtincltax = doamtincltax;
	}

	public DoubleBox getDonetpayamt() {
		return donetpayamt;
	}

	public void setDonetpayamt(DoubleBox donetpayamt) {
		this.donetpayamt = donetpayamt;
	}


	public DateBox getDbrefDate() {
		return dbrefDate;
	}


	public void setDbrefDate(DateBox dbrefDate) {
		this.dbrefDate = dbrefDate;
	}
	
	
	//   rohan 
	
	public int retrieveVendorCount(){
		List<VendorDetails> vendorLis=this.getVendortbl().getDataprovider().getList();
		int vendorCount=0;
		for(int i=0;i<vendorLis.size();i++)
		{
			if(vendorLis.get(i).getStatus()==true){
				vendorCount=vendorLis.get(i).getVendorId();
			}
		}
		return vendorCount;
	}
	
	
	
	private void retVendorState(int vendorCount) {
		
		final MyQuerry querry=new MyQuerry();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter tempfilter=null;
		
		tempfilter=new Filter();
		tempfilter.setQuerryString("companyId");
		tempfilter.setLongValue(UserConfiguration.getCompanyId());
		filtervec.add(tempfilter);
		
		tempfilter=new Filter();
		tempfilter.setQuerryString("count");
		tempfilter.setIntValue(vendorCount);
		filtervec.add(tempfilter);
		
		querry.getFilters().add(tempfilter);
		querry.setQuerryObject(new Vendor());
		Timer timer=new Timer() 
	    {
			@Override
			public void run() 
			{
				async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				@Override
				public void onFailure(Throwable caught) {
				}
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					for(SuperModel model:result)
					{
						Vendor vendorentity = (Vendor)model;
						String	retVenStateName=vendorentity.getPrimaryAddress().getState();
						PurchaseOrderPresenter.vendorState=retVenStateName.trim();
					}
				}
			 });
			}
    	 };
    	 timer.schedule(3000);
	}
	
	
	
	/**
	 * This methods returns true if warehouse is selected at product level.
	 * Date : 03-10-2016 By Anil
	 * Release : 30 Sept 2016 
	 * Project : PURCHASE MODIFICATION(NBHC)
	 */
	public boolean isWarehouseSelected(){
		List<ProductDetailsPO> list=productTablePO.getDataprovider().getList();
		for(ProductDetailsPO obj:list){
			if(!obj.getWarehouseName().equals("")){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * This method checks whether warehouse for all product is selected or not
	 * It returns true if all warehouses are selected or none of them are selected else return false 
	 * Date : 03-10-2016 By Anil
	 * Release : 30 Sept 2016 
	 * Project: PURCHASE MODIFICATION (NBHC)  
	 */
	public boolean isAllWarehouseIsSelectedOrDeselected(){
		int selCounter=0;
		int dselCounter=0;
		List<ProductDetailsPO> list=productTablePO.getDataprovider().getList();
		for(ProductDetailsPO obj:list){
			if(!obj.getWarehouseName().equals("")){
				selCounter++;
			}else{
				dselCounter++;
			}
		}
		
		if(selCounter==list.size()){
			int sameState=0;
			int diffState=0;
			String sameWhDet="";
			String diffWhDet="";
			
			
			for(ProductDetailsPO obj:list){
				String warehouseState=ProductTablePO.getWarehouseState(obj.getWarehouseName());
				if(warehouseState.trim().equals(PurchaseOrderPresenter.vendorState.trim())){
					sameState++;
					sameWhDet=sameWhDet+obj.getWarehouseName()+" : "+warehouseState+"\n"; 
				}else{
					diffState++;
					diffWhDet=diffWhDet+obj.getWarehouseName()+" : "+warehouseState+"\n"; 
				}
			}
			
			if(sameState==list.size()){
				return true;
			}else if(diffState==list.size()){
				return true;
			}else{
				showDialogMessage("Please check following"
						+"\n"+"VENDOR : "+getSelectedVendorName()+" : "+PurchaseOrderPresenter.vendorState
						+"\n\n"+"Warehouse with state name(same as vendor)"
						+"\n"+sameWhDet
						+"\n"+"Warehouse with state name(other than vendor)"
						+"\n"+diffWhDet
						+"\n"+"Please raise po for all same state or all other state!");
				return false;
			}
			
					
			
		}else if(dselCounter==list.size()){
			
			if(deliveryadd.getAdressline1().getValue().equals("")
					||deliveryadd.getCountry().getSelectedIndex()==0
					||deliveryadd.getState().getSelectedIndex()==0
					||deliveryadd.getCity().getSelectedIndex()==0
					||deliveryadd.getPin().getValue().equals("")){
				
				cbadds.setEnabled(true);
				deliveryadd.setEnable(true);
				showDialogMessage("Please add delivery address!");
				return false;
			}else{
				return true;
			}
			
		}else{
			showDialogMessage("Please select/unselect warehouse for all product !");
			return false;
		}
		
	}
	
	
	/**
	 * This method returns the name of selected Vendor
	 * Date : 03-10-2016 By Anil
	 * Release :30 Sept 2016
	 * Project : PURCHASE MODIFICATION (NBHC)
	 */
	
	public String getSelectedVendorName(){
		String venName="";
		List<VendorDetails> list=vendorTablePO.getDataprovider().getList();
		for(VendorDetails obj:list){
			if(obj.getStatus()==true){
				return obj.getVendorName();
			}
		}
		return "";
	}
	/**
	 *  nidhi
	 *  14-09-2017
	 *  
	 */
	public boolean validatePRProdPrice()
	{
		List<ProductDetailsPO> prProdLis=productTablePO.getDataprovider().getList();
		int ctr=0;
		for(int i=0;i<prProdLis.size();i++)
		{
			Console.log("validate product price "+prProdLis.get(i).getProdPrice());
			if(prProdLis.get(i).getProdPrice()==0)
			{
				ctr=ctr+1;
				Console.log("validate product 1");
			}
		}
		
		if(ctr==0){
			Console.log("validate product 2");
			return true;
			
		}
		else{
			Console.log("validate product 3");
			return false;
		}
	}
	
	/**
	 *  end
	 */
	
	public DoubleBox getDbfinalTotalAmt() {
		return dbfinalTotalAmt;
	}

	public void setDbfinalTotalAmt(DoubleBox dbfinalTotalAmt) {
		this.dbfinalTotalAmt = dbfinalTotalAmt;
	}

	public TextBox getTbroundoffAmt() {
		return tbroundoffAmt;
	}

	public void setTbroundoffAmt(TextBox tbroundoffAmt) {
		this.tbroundoffAmt = tbroundoffAmt;
	}
	
	public DoubleBox getDbfinalTotalAmtWithoutRoundoff() {
		return dbfinalTotalAmtWithoutRoundoff;
	}


	public void setDbfinalTotalAmtWithoutRoundoff(
			DoubleBox dbfinalTotalAmtWithoutRoundoff) {
		this.dbfinalTotalAmtWithoutRoundoff = dbfinalTotalAmtWithoutRoundoff;
	}
	
public void addProdTaxes() throws Exception{

	System.out.println();
	System.out.println("INSIDE ADD PRODUCT TAXES METHOD !!");
	
	List<ProductDetailsPO> salesLineItemLis=this.productTablePO.getDataprovider().getList();
	List<ProductOtherCharges> taxList=this.producttaxtable.getDataprovider().getList();
	NumberFormat nf=NumberFormat.getFormat("0.00");
	String cformval="";
	int cformindex;
	System.out.println("PRODUCT LIST SIZE : "+salesLineItemLis.size());
	
	for(int i=0;i<salesLineItemLis.size();i++){
		double priceqty=0,taxPrice=0;
		
		if((salesLineItemLis.get(i).getDiscount()==null || salesLineItemLis.get(i).getDiscount()==0) && (salesLineItemLis.get(i).getDiscountAmt()==0) ){
//			System.out.println("inside both 0 condition");
			System.out.println("NO DISCOUNT");
			taxPrice=this.getProducttable().removeAllTaxes(salesLineItemLis.get(i).getPrduct());
			priceqty=(salesLineItemLis.get(i).getProdPrice()-taxPrice)*salesLineItemLis.get(i).getProductQuantity();
			priceqty=Double.parseDouble(nf.format(priceqty));
		}else if(((salesLineItemLis.get(i).getDiscount()!=null)&&(salesLineItemLis.get(i).getDiscountAmt()!=0))){
//			System.out.println("inside both not null condition");
			taxPrice=this.getProducttable().removeAllTaxes(salesLineItemLis.get(i).getPrduct());
			priceqty=(salesLineItemLis.get(i).getProdPrice()-taxPrice);
			priceqty=priceqty-(priceqty*salesLineItemLis.get(i).getDiscount()/100);
			priceqty=priceqty-salesLineItemLis.get(i).getDiscountAmt();
			priceqty=priceqty*salesLineItemLis.get(i).getProductQuantity();
			priceqty=Double.parseDouble(nf.format(priceqty));
		}else {
//			System.out.println("inside oneof the null condition");
			taxPrice=this.getProducttable().removeAllTaxes(salesLineItemLis.get(i).getPrduct());
			priceqty=(salesLineItemLis.get(i).getProdPrice()-taxPrice);
			if(salesLineItemLis.get(i).getDiscount()!=null){
				priceqty=priceqty-(priceqty*salesLineItemLis.get(i).getDiscount()/100);
			}
			else{
				priceqty=priceqty-salesLineItemLis.get(i).getDiscountAmt();
			}
			priceqty=priceqty*salesLineItemLis.get(i).getProductQuantity();
			priceqty=Double.parseDouble(nf.format(priceqty));
		}
		System.out.println();
		System.out.println("Total Price Excluding Tax "+i+" - "+priceqty);
		
		
		
		System.out.println("HI VIJAY print name ==="+salesLineItemLis.get(i).getPurchaseTax1().getTaxPrintName());
		System.out.println("Adding Vat Tax to table if the company state and delivery state is equal");
		if(salesLineItemLis.get(i).getVat()!=0){
			
			System.out.println("VAT !=0 -- ");
			/**
			 * Date 2 August 2017 below if condition added by vijay for GST old code added in else block
			 */
			if(salesLineItemLis.get(i).getPurchaseTax1().getTaxPrintName()!=null && !salesLineItemLis.get(i).getPurchaseTax1().getTaxPrintName().equals("")){
				System.out.println("GST");
				ProductOtherCharges pocentity=new ProductOtherCharges();
				// below code added by vijay for GST
				System.out.println("salesLineItemLis.get(i).getPurchaseTax2().getTaxPrintName() "+salesLineItemLis.get(i).getPurchaseTax1().getTaxPrintName());
				pocentity.setChargeName(salesLineItemLis.get(i).getPurchaseTax1().getTaxPrintName());
				pocentity.setChargePercent(salesLineItemLis.get(i).getVat());
				pocentity.setIndexCheck(i+1);
				int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getVat(), salesLineItemLis.get(i).getPurchaseTax1().getTaxPrintName());
				
				if(indexValue!=-1){
					System.out.println("Index As Not -1");
					pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
					this.getProducttaxtable().getDataprovider().getList().remove(indexValue);
				}
				if(indexValue==-1){
					System.out.println("VAt");
					pocentity.setAssessableAmount(priceqty);
				}
				this.producttaxtable.getDataprovider().getList().add(pocentity);
				
			}else{
				System.out.println("Vattx");
				ProductOtherCharges pocentity=new ProductOtherCharges();
				pocentity.setChargeName("VAT");
				pocentity.setChargePercent(salesLineItemLis.get(i).getVat());
				pocentity.setIndexCheck(i+1);
				int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getVat(),"VAT");
				
				if(indexValue!=-1){
					System.out.println("Index As Not -1");
					pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
					this.getProducttaxtable().getDataprovider().getList().remove(indexValue);
				}
				if(indexValue==-1){
					System.out.println("VAt");
					pocentity.setAssessableAmount(priceqty);
				}
				this.producttaxtable.getDataprovider().getList().add(pocentity);
				
			}
			
		}
//	}
		
		if(salesLineItemLis.get(i).getTax()!=0){
		if(salesLineItemLis.get(i).getPurchaseTax2().getTaxPrintName()!=null && !salesLineItemLis.get(i).getPurchaseTax2().getTaxPrintName().equals("")){
			System.out.println("GST");
			ProductOtherCharges pocentity=new ProductOtherCharges();
			// below code added by vijay for GST
			pocentity.setChargeName(salesLineItemLis.get(i).getPurchaseTax2().getTaxPrintName());
			pocentity.setChargePercent(salesLineItemLis.get(i).getTax());
			pocentity.setIndexCheck(i+1);
			int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getTax(), salesLineItemLis.get(i).getPurchaseTax2().getTaxPrintName());
			
			if(indexValue!=-1){
				System.out.println("Index As Not -1");
				pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
				this.getProducttaxtable().getDataprovider().getList().remove(indexValue);
			}
			if(indexValue==-1){
				System.out.println("service");
				pocentity.setAssessableAmount(priceqty);
			}
			this.producttaxtable.getDataprovider().getList().add(pocentity);
			
		}else{
			System.out.println("service");
			ProductOtherCharges pocentity=new ProductOtherCharges();
			pocentity.setChargeName("Service");
			pocentity.setChargePercent(salesLineItemLis.get(i).getTax());
			pocentity.setIndexCheck(i+1);
			int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getTax(),"VAT");
			
			if(indexValue!=-1){
				System.out.println("Index As Not -1");
				pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
				this.getProducttaxtable().getDataprovider().getList().remove(indexValue);
			}
			if(indexValue==-1){
				System.out.println("VAt");
				pocentity.setAssessableAmount(priceqty);
			}
			this.producttaxtable.getDataprovider().getList().add(pocentity);
			
		}
		}
		
	}

}
/** date 22.10.2018 added by komal **/
public void byDefaultSelectionOfWareHouse(final ProductDetailsPO product,final boolean isCallFromPurchaseRequision) {
	
	MyQuerry querry = new MyQuerry();
	Company c=new Company();
	Vector<Filter> filtervec=new Vector<Filter>();
	Filter filter =null;
	
	filter = new Filter();
//	filter.setQuerryString("details.prodid");
	filter.setQuerryString("count"); //Ashiwni Patil Date:25-11-2022
//	filter.setIntValue(Integer.parseInt(prodInfoComposite.getProdID().getValue()));
	filter.setIntValue(product.getProductID());//Ashiwni Patil Date:25-11-2022
	filtervec.add(filter);
	
	
	filter=new Filter();
	filter.setLongValue(c.getCompanyId());
	filter.setQuerryString("companyId");
	filtervec.add(filter);
	
	querry.setFilters(filtervec);
	querry.setQuerryObject(new ProductInventoryView());
	
	 GenricServiceAsync genasync=GWT.create(GenricService.class);
	 genasync.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
		@Override
		public void onSuccess(ArrayList<SuperModel> result) {
			String wh = globalWareHouseList.get(0).getBusinessUnitName();
			String whAddress = globalWareHouseList.get(0).getAddress().getCompleteAddress();
			int count = 0 ;
			double stock = 0;
			for(SuperModel model: result){
				ProductInventoryView view = (ProductInventoryView) model;
				if(view.getDetails().size() > 0){
					for(ProductInventoryViewDetails list : view.getDetails()){
						if(list.getWarehousename().equalsIgnoreCase(wh)){
							stock = list.getAvailableqty();
							count++;
						}
					}
				}
			}
			if(count == 1 || count == 0){
				product.setWarehouseName(wh);
				product.setWarehouseAddress(whAddress);
				product.setAvailableStock(stock);
			}
			/**
			 * @author Anil, Date : 11-01-2019
			 * Checking product validation again after assigning it warehouse details
			 */
			if(validateProductDuplicate(product.getProductCode(),product.getWarehouseName())){
				if(!isCallFromPurchaseRequision)
					productTablePO.getDataprovider().getList().add(product);
			}
		}
		@Override
		public void onFailure(Throwable caught) {
			System.out.println("No Product Found");
			if(!isCallFromPurchaseRequision)
				productTablePO.getDataprovider().getList().add(product);
		}
	});
}
	public void loadWareHouseList(String branch){
		GenricServiceAsync service = GWT.create(GenricService.class);
		MyQuerry query = new MyQuerry();
//		System.out.println("Company Id :: "+UserConfiguration.getCompanyId());
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);
				
		filter=new Filter();
	    filter.setQuerryString("branchdetails.branchName");
	    filter.setStringValue(branch);
	    filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(UserConfiguration.getCompanyId());
		filtervec.add(filter);
		
		
		query.setFilters(filtervec);
		query.setQuerryObject(new WareHouse());
		service.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				globalWareHouseList = new ArrayList<WareHouse>();
				if(result.size()>0){
					for(SuperModel model : result){
						WareHouse wh = (WareHouse) model;
						globalWareHouseList.add(wh);
					}
				}
			}
		});
	}

	/**
	 *  Date 26-11-2018 By Vijay 
	 *  Des :- Adding calculation part for other charges
	 */  
	public void addOtherChargesInTaxTbl(){
		List<ProductOtherCharges> taxList=this.getProducttaxtable().getDataprovider().getList();
		for(OtherCharges otherCharges:tblOtherCharges.getDataprovider().getList()){
			/**
			 * If GST tax is not applicable then we will consider tax1 as vat tax field and tax2 as service tax field.
			 */
			if(otherCharges.getTax1().getTaxPrintName()!=null&&!otherCharges.getTax1().getTaxPrintName().equals("")){
				boolean updateTaxFlag=true;
				if(otherCharges.getTax1().getTaxPrintName().equalsIgnoreCase("SELECT")){
					updateTaxFlag=false;
//					return;
				}
				if(otherCharges.getTax1().getPercentage()==0){
					updateTaxFlag=false;
//					return;
				}
				if(updateTaxFlag){
					ProductOtherCharges pocentity=new ProductOtherCharges();
					pocentity.setChargeName(otherCharges.getTax1().getTaxPrintName());
					pocentity.setChargePercent(otherCharges.getTax1().getPercentage());
//					pocentity.setIndexCheck(i+1);
					int indexValue=this.checkTaxPercent(otherCharges.getTax1().getPercentage(),otherCharges.getTax1().getTaxPrintName());
					if(indexValue!=-1){
						pocentity.setAssessableAmount(otherCharges.getAmount()+taxList.get(indexValue).getAssessableAmount());
						this.getProducttaxtable().getDataprovider().getList().remove(indexValue);
					}
					if(indexValue==-1){
						pocentity.setAssessableAmount(otherCharges.getAmount());
					}
					this.getProducttaxtable().getDataprovider().getList().add(pocentity);
					
				}
			}else{
				boolean updateTaxFlag=true;
				if(otherCharges.getTax1().getPercentage()==0){
					updateTaxFlag=false;
//					return;
				}
				if(updateTaxFlag){
					ProductOtherCharges pocentity=new ProductOtherCharges();
					pocentity.setChargeName("VAT");
					pocentity.setChargePercent(otherCharges.getTax1().getPercentage());
//					pocentity.setIndexCheck(i+1);
					int indexValue=this.checkTaxPercent(otherCharges.getTax1().getPercentage(),"VAT");
					if(indexValue!=-1){
						pocentity.setAssessableAmount(otherCharges.getAmount()+taxList.get(indexValue).getAssessableAmount());
						this.getProducttaxtable().getDataprovider().getList().remove(indexValue);
					}
					if(indexValue==-1){
						pocentity.setAssessableAmount(otherCharges.getAmount());
					}
					this.getProducttaxtable().getDataprovider().getList().add(pocentity);
				}
			}
			
			if(otherCharges.getTax2().getTaxPrintName()!=null&&!otherCharges.getTax2().getTaxPrintName().equals("")){
				System.out.println("INSIDE GST TAX PRINT NAME : "+otherCharges.getTax2().getTaxPrintName());
				boolean updateTaxFlag=true;
				if(otherCharges.getTax2().getTaxPrintName().equalsIgnoreCase("SELECT")){
					updateTaxFlag=false;
//					return;
				}
				if(otherCharges.getTax2().getPercentage()==0){
					updateTaxFlag=false;
//					return;
				}
				if(updateTaxFlag){
					ProductOtherCharges pocentity=new ProductOtherCharges();
					pocentity.setChargeName(otherCharges.getTax2().getTaxPrintName());
					pocentity.setChargePercent(otherCharges.getTax2().getPercentage());
//					pocentity.setIndexCheck(i+1);
					int indexValue=this.checkTaxPercent(otherCharges.getTax2().getPercentage(),otherCharges.getTax2().getTaxPrintName());
					if(indexValue!=-1){
						pocentity.setAssessableAmount(otherCharges.getAmount()+taxList.get(indexValue).getAssessableAmount());
						this.getProducttaxtable().getDataprovider().getList().remove(indexValue);
					}
					if(indexValue==-1){
						pocentity.setAssessableAmount(otherCharges.getAmount());
					}
					this.getProducttaxtable().getDataprovider().getList().add(pocentity);
				}
			}else{
				System.out.println("ST OR NON GST");
				boolean updateTaxFlag=true;
				if(otherCharges.getTax2().getPercentage()==0){
					updateTaxFlag=false;
//					return;
				}
				if(otherCharges.getTax1().getTaxPrintName()!=null&&!otherCharges.getTax1().getTaxPrintName().equals("")){
					updateTaxFlag=false;
//					return;
				}
				if(updateTaxFlag){
					ProductOtherCharges pocentity=new ProductOtherCharges();
					pocentity.setChargeName("Service Tax");
					pocentity.setChargePercent(otherCharges.getTax2().getPercentage());
//					pocentity.setIndexCheck(i+1);
					int indexValue=this.checkTaxPercent(otherCharges.getTax2().getPercentage(),"Service");
					if(indexValue!=-1){
					 	double assessValue=otherCharges.getAmount()+(otherCharges.getAmount()*otherCharges.getTax1().getPercentage()/100);
						pocentity.setAssessableAmount(assessValue+taxList.get(indexValue).getAssessableAmount());
						this.getProducttaxtable().getDataprovider().getList().remove(indexValue);
					}
					if(indexValue==-1){
						double assessValue=otherCharges.getAmount()+(otherCharges.getAmount()*otherCharges.getTax1().getPercentage()/100);
						pocentity.setAssessableAmount(assessValue);
					}
					this.getProducttaxtable().getDataprovider().getList().add(pocentity);
				}
			}
		}
	}


	public OtherChargesTable getTblOtherCharges() {
		return tblOtherCharges;
	}


	public void setTblOtherCharges(OtherChargesTable tblOtherCharges) {
		this.tblOtherCharges = tblOtherCharges;
	}


	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		if(event.getSource().equals(costingBtn)){
			addPoOtherCharges();
			marginPopup.reactOnCosting(productTablePO.getValue(),vendorMargins,otherChargesMargins);
		}
		
		if(event.getSource().equals(marginPopup.getLblOk())){
			
			vendorMargins=marginPopup.vendorMarginTbl.getValue();
			otherChargesMargins=marginPopup.otherChargesMarginTbl.getValue();
			totalVendorMargin=marginPopup.dbTotalVendorMargin.getValue();
			totalOcMargin=marginPopup.dbOtherChargesMargin.getValue();
			totalOtherChargesPO=marginPopup.dbTotalOcPO.getValue();
			totalOtherChargesSales=marginPopup.dbTotalOcSales.getValue();
			marginPopup.hidePopUp();
		}
		
		if(event.getSource().equals(marginPopup.getLblCancel())){
			marginPopup.hidePopUp();
		}
	}


	@Override
	public void clear() {
		// TODO Auto-generated method stub
		super.clear();
		
		vendorMargins=null;
		otherChargesMargins=null;
		totalOcMargin=null;
		totalVendorMargin=null;
		totalOtherChargesPO=null;
		totalOtherChargesSales=null;
	}
	
	public void addPoOtherCharges(){
		if(otherChargesMargins==null){
			otherChargesMargins=new ArrayList<OtherChargesMargin>();
		}
		double sumOfOcPo=0;
		OtherChargesMargin obj=null;
		if(tblOtherCharges.getValue().size()!=0){
			for(OtherCharges object:tblOtherCharges.getValue()){
				sumOfOcPo=sumOfOcPo+object.getAmount();
			}
			
			obj=new OtherChargesMargin();
			obj.setCostType("PO(Other Charges)");
			obj.setOtherCharges(sumOfOcPo);
			obj.setReadOnly(true);
		}
		
		
		if(otherChargesMargins==null||otherChargesMargins.size()==0){
			if(obj!=null){
				otherChargesMargins.add(obj);
			}
		}else{
			boolean updateFlag=true;
			for(OtherChargesMargin margin:otherChargesMargins){
				if(margin.getCostType().equals("PO(Other Charges)")){
					updateFlag=false;
					margin.setOtherCharges(sumOfOcPo);
					margin.setReadOnly(true);
				}
			}
			if(updateFlag){
				if(obj!=null){
					otherChargesMargins.add(obj);
				}
			}
		}
	}
	
	/**
	 * @author Anil
	 * @since 30-07-2020
	 * adding vendor details when making PO from SO
	 * for PTSPL raised by Rahul Tiwari
	 * @param itemList
	 */
	public void addCommonVendorToVendorTable(final List<ProductDetailsPO> itemList){
		Console.log("Inside add common vendor...");
		List<String> prodCodeList=new ArrayList<String>();
		for(ProductDetailsPO obj:itemList){
			prodCodeList.add(obj.getProductCode());
		}
		MyQuerry querry=new MyQuerry();
		Filter filter = new Filter();
		filter.setQuerryString("prodCode IN");
		filter.setList(prodCodeList);
		querry.getFilters().add(filter);
		
		Filter filter1 = new Filter();
		filter1.setQuerryString("companyId");
		filter1.setLongValue(UserConfiguration.getCompanyId());
		querry.getFilters().add(filter1);
		
		querry.setQuerryObject(new PriceList());
		
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
			}
			@Override
			public void onSuccess(ArrayList<SuperModel> vresult) {
				Console.log("PriceList..."+vresult.size());
				vendorRateList=new ArrayList<PriceListDetails>();
				ArrayList<Integer> commonVendorIds=new ArrayList<Integer>();
				if (vresult.size() != 0) {
					for(SuperModel smodel:vresult){
						PriceList pentity=(PriceList) smodel;
						ArrayList<Integer>vendorIds=new ArrayList<Integer>();
						vendorRateList.addAll(pentity.getPriceInfo());
						
						for(PriceListDetails obj:pentity.getPriceInfo()){
							vendorIds.add(obj.getVendorID());
						}
						
						if(commonVendorIds.size()!=0){
							commonVendorIds.retainAll(vendorIds);
							if(commonVendorIds.size()==0){
								showDialogMessage("No common vendor found.");
								return;
							}
						}else{
							commonVendorIds.addAll(vendorIds);
						}
					}
					
					ArrayList<PriceListDetails> commonVendorList=new ArrayList<PriceListDetails>();
					ArrayList<PriceListDetails> vendorList=new ArrayList<PriceListDetails>();
					for(Integer vendorId:commonVendorIds){
						PriceListDetails price=new PriceListDetails();
						for(PriceListDetails obj:vendorRateList){
							if(obj.getVendorID()==vendorId){
								commonVendorList.add(obj);
								PersonInfo info=new PersonInfo();
								info.setVendor(true);
								info.setCount(obj.getVendorID());
								info.setFullName(obj.getFullName());
								info.setCellNumber(obj.getCellNumber());
								info.setEmail(obj.getEmail());
								price.setPersoninfo(info);
							}
						}
						vendorList.add(price);
					}
					
					for(ProductDetailsPO item:itemList){
						boolean productFlag=false;
						for(PriceListDetails obj:commonVendorList){
							if(obj.getProdID()==item.getProductID()){
								productFlag=true;
							}
						}
						if(productFlag==false){
							showDialogMessage("No vendor product price list found for "+item.getProductName());
							return;
						}
					}
					if(vendorList.size()>0){
						addUniqueVendorsToTable(vendorList);
					}
				}else{
					showDialogMessage("No vendor product price list found.");
				}
			}
		});		
	}
	
	@Override
	public void refreshTableData() {
		// TODO Auto-generated method stub
		super.refreshTableData();
		productTablePO.getTable().redraw();
		vendorTablePO.getTable().redraw();
		paymentTermsTable.getTable().redraw();
		producttaxtable.getTable().redraw();
		productchargestable.getTable().redraw();
		tblOtherCharges.getTable().redraw();
	}
	
	private void getAndSetBranchAddress() {

		final MyQuerry querry = new MyQuerry();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter=new Filter();
		filter.setQuerryString("buisnessUnitName");
		filter.setStringValue(branch.getValue());
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Branch());
		showWaitSymbol();

		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				hideWaitSymbol();
				for(SuperModel model: result){
					Branch branch = (Branch) model;
					deliveryadd.setValue(branch.getAddress());
				}
				deliveryadd.setEnable(false);

			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				hideWaitSymbol();

			}
		});
	}
	
	private void getAndSetCompanyAddress() {
		final MyQuerry querry = new MyQuerry();
		Company c=new Company();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter=new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Company());
		
		showWaitSymbol();
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				hideWaitSymbol();

				for(SuperModel model: result){
					Company company = (Company) model;
					deliveryadd.setValue(company.getAddress());
					deliveryadd.setEnable(false);
				}

			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				hideWaitSymbol();

			}
		});
	}
}
