package com.slicktechnologies.client.views.purchase.quickpurchase;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.LongBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.ValueBoxBase.TextAlignment;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.AddressComposite;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.composite.UploadComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.contract.OtherChargesTable;
import com.slicktechnologies.client.views.contract.ProductChargesTable;
import com.slicktechnologies.client.views.contract.ProductTaxesTable;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.client.views.purchase.purchaseorder.ProductTablePO;
import com.slicktechnologies.client.views.purchase.purchaseorder.PurchaseOrderPresenter;
import com.slicktechnologies.client.views.purchase.purchaseorder.VendorTablePO;
import com.slicktechnologies.client.views.purchase.servicepo.PaymentTermsTbl;
import com.slicktechnologies.shared.ProductOtherCharges;
import com.slicktechnologies.shared.TaxDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.PriceList;
import com.slicktechnologies.shared.common.businessprocesslayer.PriceListDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetailsPO;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessprocesslayer.VendorDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.productlayer.Tax;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.salesorder.OtherCharges;

public class QuickPurchaseOrderForm extends FormScreen<PurchaseOrder> implements ChangeHandler {

	
	final GenricServiceAsync async = GWT.create(GenricService.class);
	public TextBox ibPOId;
	public DateBox dbPODate,dbrefDate;
	TextBox ibRefOrderNo;
	DateBox dbExpdeliverydate;
	TextArea tadescription;
	ObjectListBox<ConfigCategory> oblPurchaseOrderCategory;
	ObjectListBox<Type> oblPurchaseOrderType;
	public TextBox tbStatus;
	
//	public ProductTablePO productTablePO;
	public QuickPurchaseProductTable productTablePO;
	
	VendorTablePO vendorTablePO;
	ObjectListBox<Config> olbcPaymentMethods;

	ObjectListBox<PriceList> veninfo;
	
	ObjectListBox<Employee> oblEmployee;
	ObjectListBox<Branch> branch;
	ObjectListBox<Employee> tbApporverName;
	
	ProductTaxesTable producttaxtable;
	ProductChargesTable productchargestable;
	FormField fgroupingLetterInformation;
	
	Button addOtherCharges;
	TextBox tbblanck;
	
	public ProductInfoComposite prodInfoComposite;
	public Button addproducts;
	
//	ObjectListBox<TaxDetails> olbcstpercent;
//	ListBox cbcformlis;
	DoubleBox dototalamt, doamtincltax, donetpayamt;
	PurchaseOrder purchaseOrderobj;
	
	public TextBox tbpoName;
	UploadComposite upload;
	
	Label lbtotal;
	public DoubleBox dbTotal;
	AddressComposite deliveryadd;
	PersonInfoComposite vendorComp;
	Button btnAddVendor;
	
	CheckBox cbadds;
	/**
	 * Date 02 August 2017 added by vijay for Cform validation
	 */
	Date date = DateTimeFormat.getFormat("yyyy-MM-dd HH:mm:ss").parse("2017-07-01 00:00:00");
	final DateTimeFormat fmt = DateTimeFormat.getFormat("dd/MM/yyyy");

	/**
	 * ends here
	 */
	
	Button btnNewVendor;
	DoubleBox dbpaymentrecieved,dbbalancepayment;
	
	NumberFormat numfmt = NumberFormat.getFormat("0.00");
	
	/**
	 * Date : 26-11-2018 By Vijay
	 * Adding other charges table with taxes
	 */
	OtherChargesTable tblOtherCharges;
	/**
	 * End
	 */
	
	/** Date 26-11-2018 added by Vijay **/
	ObjectListBox<Employee> olbAuthorisedBy;
	TextArea tbtersOfDelivery;
	 /**Date 10-10-2019 by Amol added a two fields poc name and poc number**/
	 TextBox tbPocName;
	 LongBox IbPocNumber;
	 
	public QuickPurchaseOrderForm() {
		super();
		createGui();
		productTablePO.connectToLocal();
		tbStatus.setValue(PurchaseOrder.CREATED);
//		olbcstpercent.setEnabled(false);
//		cbcformlis.setEnabled(false);
		tbblanck.setVisible(false);
		deliveryadd.setEnable(false);
		Company com = new Company();
		getCompanyAddress(com.getCompanyId());
		vendorTablePO.connectToLocal();
		
//		dbPODate.setEnabled(false);
		dbTotal.setEnabled(false);
		doamtincltax.setEnabled(false);
		donetpayamt.setEnabled(false);
		PurchaseOrderPresenter.vendorState="";
	}
	
	public QuickPurchaseOrderForm(String[] processlevel, FormField[][] fields,FormStyle formstyle) {
		super(processlevel, fields, formstyle);
		createGui();
		tbStatus.setValue(PurchaseOrder.CREATED);
		deliveryadd.setEnable(false);
		Company com = new Company();
		getCompanyAddress(com.getCompanyId());
//		dbPODate.setEnabled(false);
		dbTotal.setEnabled(false);
		doamtincltax.setEnabled(false);
		donetpayamt.setEnabled(false);

	}
	
	private void initalizeWidget() {

		ibPOId = new TextBox();
		ibPOId.setEnabled(false);
		ibRefOrderNo = new TextBox();
		tbpoName = new TextBox();
		dbExpdeliverydate = new DateBoxWithYearSelector();
		dbPODate = new DateBoxWithYearSelector();
		Date date = new Date();
		dbPODate.setValue(date);
//		dbPODate.setEnabled(false);
		
		dbrefDate=new DateBoxWithYearSelector();
		vendorTablePO = new VendorTablePO();
		productTablePO = new QuickPurchaseProductTable();
		producttaxtable = new ProductTaxesTable();
		productchargestable = new ProductChargesTable();
		tadescription=new TextArea();
		oblEmployee = new ObjectListBox<Employee>();
		oblEmployee.makeEmployeeLive(AppConstants.PURCHASEMODULE, AppConstants.PURCHASEORDER, "Purchase Engineer");
		oblPurchaseOrderCategory = new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(oblPurchaseOrderCategory, Screen.PURCHASEORDERCATEGORY);
		oblPurchaseOrderCategory.addChangeHandler(this);
		oblPurchaseOrderType = new ObjectListBox<Type>();
		AppUtility.makeTypeListBoxLive(oblPurchaseOrderType,Screen.PURCHASEORDERTYPE);
		
		
		branch = new ObjectListBox<Branch>();
		branch.addChangeHandler(this);
		initalizEmployeeBranchListBox();
		tbApporverName = new ObjectListBox<Employee>();
		AppUtility.makeApproverListBoxLive(tbApporverName, "Purchase Order");
		
//		project = new ObjectListBox<HrProject>();
//		HrProject.MakeProjectListBoxLive(project);

		upload = new UploadComposite();
		olbcPaymentMethods=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbcPaymentMethods,Screen.PAYMENTMETHODS);

		lbtotal = new Label("Total");
		dbTotal = new DoubleBox();
		dbTotal.setWidth("200px");
		dbTotal.setAlignment(TextAlignment.CENTER);
		
		donetpayamt = new DoubleBox();
		donetpayamt.setWidth("200px");
		donetpayamt.setAlignment(TextAlignment.CENTER);
		
		addOtherCharges = new Button("+");

		doamtincltax = new DoubleBox();
		doamtincltax.setWidth("200px");
		doamtincltax.setAlignment(TextAlignment.CENTER);

		cbadds = new CheckBox();
		cbadds.setValue(false);
		deliveryadd = new AddressComposite(true);
		deliveryadd.setEnable(false);

//		cbcformlis = new ListBox();
//		cbcformlis.addItem("SELECT");
//		cbcformlis.addItem(AppConstants.YES);
//		cbcformlis.addItem(AppConstants.NO);
//		cbcformlis.setEnabled(false);
//		olbcstpercent = new ObjectListBox<TaxDetails>();
//		olbcstpercent.setEnabled(false);
//		initializePercentCst();
		addproducts = new Button("ADD");
		
		dototalamt=new DoubleBox();

		tbStatus = new TextBox();
		tbStatus.setEnabled(false);
//		tbcommentForStatusChange=new TextBox();
//		tbcommentForStatusChange.setEnabled(false);
		tbblanck = new TextBox();
		tbblanck.setVisible(false);
//		prodInfoComposite = AppUtility.initiatePurchaseProductComposite(new SuperProduct());
		/** Date 10-08-2020 updated code composite should load only item product **/
		prodInfoComposite = AppUtility.initiateSalesProductComposite(new SuperProduct());

		
//		olbAssignedTo1=new ObjectListBox<Employee>();
////		AppUtility.makeSalesPersonListBoxLive(olbAssignedTo1);
//		olbAssignedTo1.makeEmployeeLive(AppConstants.PURCHASEMODULE, AppConstants.PURCHASEORDER, "Assign To 1");
//		
//		olbAssignedTo2=new ObjectListBox<Employee>();
////		AppUtility.makeSalesPersonListBoxLive(olbAssignedTo2);
//		olbAssignedTo2.makeEmployeeLive(AppConstants.PURCHASEMODULE, AppConstants.PURCHASEORDER, "Assign To 2");
//		
		MyQuerry querry = new MyQuerry();
		querry.setQuerryObject(new Vendor());
		Filter filter = new Filter();
		filter.setQuerryString("vendorStatus");
		filter.setBooleanvalue(true);
		querry.getFilters().add(filter);
		
		dbpaymentrecieved= new DoubleBox();
		dbpaymentrecieved.setWidth("200px");
		dbpaymentrecieved.setAlignment(TextAlignment.CENTER);
		
		dbbalancepayment = new DoubleBox();
		dbbalancepayment.setEnabled(false);
		dbbalancepayment.setWidth("200px");
		dbbalancepayment.setAlignment(TextAlignment.CENTER);
		
		vendorComp = new PersonInfoComposite(querry, false);
		btnAddVendor = new Button("Add");
		
		btnNewVendor = new Button("New Vendor");
		
		/**
		 * Date : 24-11-2018 By Vijay
		 * Adding other charges table
		 */
		tblOtherCharges=new OtherChargesTable();
		/**
		 * ends here
		 */
		
		/** date 26-11-2018 added by Vijay **/
		olbAuthorisedBy = new ObjectListBox<Employee>();
		AppUtility.makeApproverListBoxLive(olbAuthorisedBy, "Purchase Order");
		tbtersOfDelivery=new TextArea();
		
		tbPocName=new TextBox();
		IbPocNumber=new LongBox();
		
	}
	
	
	

	@Override
	public void createScreen() {
		initalizeWidget();

		this.processlevelBarNames = new String[] {
				 AppConstants.CANCELPURCHASEORDER,AppConstants.CANCELLATIONSUMMARY,"New", AppConstants.SUBMIT,AppConstants.VIEWINVOICEPAYMENT};
		
		String mainScreenLabel="Purchase Register";
		if(purchaseOrderobj!=null&&purchaseOrderobj.getCount()!=0){
			mainScreenLabel=purchaseOrderobj.getCount()+"/"+purchaseOrderobj.getStatus()+"/"+AppUtility.parseDate(purchaseOrderobj.getCreationDate());
		}
		//fgroupingLetterInformation.getHeaderLabel().setText(mainScreenLabel);
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		fgroupingLetterInformation=fbuilder.setlabel(mainScreenLabel).widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).setKeyField(true).build();
		
		FormField fgroupingReferenceDetails = fbuilder.setlabel("Reference Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		FormField fpuchasereqdate = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Referece Order No.", ibRefOrderNo);
		FormField freferenceorderno = fbuilder.setMandatory(false).setMandatoryMsg("").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder();
		//FormField fgroupingLetterInformation = fbuilder.setlabel("Purchase Order ").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();

		fbuilder = new FormFieldBuilder("PO ID.", ibPOId);
		FormField fibPOId = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		fbuilder = new FormFieldBuilder("PO Title", tbpoName);
		FormField ftbpoName = fbuilder.setMandatory(false)
				.setMandatoryMsg("Title is mandatory").setRowSpan(0)
				.setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* PO Date", dbPODate);
		FormField fpodate = fbuilder.setMandatory(true)
				.setMandatoryMsg("PO Date is mandatory").setRowSpan(0)
				.setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("Reference Date", dbrefDate);
		FormField fdbrefDate = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("Expected Delivery Date",dbExpdeliverydate);
		FormField fexpdeldate = fbuilder.setMandatory(false)
				.setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("PO Type.", oblPurchaseOrderType);
		FormField floitype = fbuilder.setMandatory(false)
				.setMandatoryMsg("PO type is mandatory").setRowSpan(0)
				.setColSpan(0).build();
		fbuilder = new FormFieldBuilder("PO Category.", oblPurchaseOrderCategory);
		FormField floigroup = fbuilder.setMandatory(false)
				.setMandatoryMsg("PO Category is mandatory").setRowSpan(0)
				.setColSpan(0).build();
		
		
		FormField fbranch=null;
		if(AppUtility.isBranchNonMandatory("PurchaseOrder")){
			fbuilder = new FormFieldBuilder("Branch", branch);
			fbranch = fbuilder.setMandatory(false).setMandatoryMsg("Branch is Mandatory!").setRowSpan(0)
					.setColSpan(0).build();
		}else{
			fbuilder = new FormFieldBuilder("* Branch", branch);
			fbranch = fbuilder.setMandatory(true).setMandatoryMsg("Branch is Mandatory!").setRowSpan(0)
					.setColSpan(0).build();
		}
		
		fbuilder = new FormFieldBuilder("* Purchase Engineer.", oblEmployee);
		FormField femployee = fbuilder.setMandatory(true)
				.setMandatoryMsg("Purchase Engineer is mandatory")
				.setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Approver Name", tbApporverName);
		FormField ftbApporverName = fbuilder.setMandatory(true)
				.setMandatoryMsg("Approver Name is mandatory").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Upload Document", upload);
		FormField fdoc = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Status", tbStatus);
		FormField ftbStatus = fbuilder.setMandatory(true)
				.setMandatoryMsg("Status is mandatory").setRowSpan(0)
				.setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Payment Method", olbcPaymentMethods);
		FormField folbcPaymentMethods = fbuilder.setMandatory(false)
				.setMandatoryMsg("Payment Method is mandatory").setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder();
		FormField fproductInformation = fbuilder.setlabel("Product")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(5).build();

		fbuilder = new FormFieldBuilder("", productTablePO.getTable());
		FormField fproductTablePO = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(5).build();

		fbuilder = new FormFieldBuilder("Total", dbTotal);
		FormField fdbtl = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("", producttaxtable.getTable());
		FormField fproducttaxtable = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(3).build();

		fbuilder = new FormFieldBuilder("Total", doamtincltax);
		FormField fdoamtincltax = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("", productchargestable.getTable());
		FormField fchargestable = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(3).build();

		fbuilder = new FormFieldBuilder("", addOtherCharges);
		FormField faddOtherCharges = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Net Payable", donetpayamt);
		FormField fdonetpayamt = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("", vendorTablePO.getTable());
		FormField fvendortable = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(5).build();

		fbuilder = new FormFieldBuilder("", tbblanck);
		FormField fblanck = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();


		fbuilder = new FormFieldBuilder("Is Delivery Address Different", cbadds);
		FormField fcbadds = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("", deliveryadd);
		FormField fdeliveryadd = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(5).build();

		fbuilder = new FormFieldBuilder("", prodInfoComposite);
		FormField fprodInfoComposite = fbuilder.setMandatory(false)
				.setRowSpan(0).setColSpan(3).build();
		fbuilder = new FormFieldBuilder("", addproducts);
		FormField faddproducts = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingAttachment = fbuilder.setlabel("Attachment").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		FormField fgroupingDescription = fbuilder.setlabel("Reference/General").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		fbuilder = new FormFieldBuilder("Description (Max 500 characters)", tadescription);
		FormField ftadescription = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(5).build();
		
		//
		fbuilder = new FormFieldBuilder();
		FormField fgroupingDelivery = fbuilder.setlabel("Delivery").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingClassification = fbuilder.setlabel("Classification").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fvendorTableGroup = fbuilder.setlabel("Vendor").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		
		fbuilder = new FormFieldBuilder("", vendorComp);
		FormField fvendorComp = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("", btnAddVendor);
		FormField fbtaddVendor = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		
		fbuilder = new FormFieldBuilder("Payment Paid",dbpaymentrecieved);
		FormField fpaymentrecieved= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Balance Payment",dbbalancepayment);
		FormField fbalancepayment= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		
		fbuilder = new FormFieldBuilder("", btnNewVendor);
		FormField fbtnNewVendor = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fblankgroup=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fblankgroupone=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
	
		/**
		 * Date : 24-11-2018 By Vijay
		 * Adding other charges table with taxes
		 */
		fbuilder = new FormFieldBuilder("",tblOtherCharges.getTable());
		FormField fothChgTbl= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		/**
		 * ends here
		 */
		
		/** Date 26-11-2018 added by Vijay**/
		fbuilder = new FormFieldBuilder("Authorised By",olbAuthorisedBy);
		FormField folbAuthorisedBy = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/**
		 * Date 21-7-2018 by jayshree
		 */
		fbuilder = new FormFieldBuilder("Terms Of Delivery", tbtersOfDelivery);
		FormField ftbtersOfDelivery = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder=new FormFieldBuilder("POC Name", tbPocName);
		FormField ftbPocName= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder=new FormFieldBuilder("POC Number", IbPocNumber);
		FormField fIbPocNumber= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		FormField[][] formfield = {
//				{ fgroupingLetterInformation },
//				{ fibPOId, ftbpoName, fpodate,folbcPaymentMethods ,fexpdeldate },
//				{ floigroup, floitype,fbranch,femployee,ftbApporverName },
//				{ ftbStatus,freferenceorderno,fdbrefDate,fdoc,folbAuthorisedBy}, //vijay
//				{ fgroupingDescription},
//				{ ftadescription},
//				{ fproductInformation }, { fprodInfoComposite, faddproducts },
//				{ fproductTablePO },
////				{ fblanck, fblanck, fblanck,fblanck, fdbtl },
////				{ fblanck, fblanck, fblanck, fproducttaxtable },
////				{ fblanck, fblanck,fblanck, faddOtherCharges, fdoamtincltax },
////				{ fblanck, fblanck,fblanck, fchargestable },
//				/** Date : 26-11-2018 By Vijay for Other Charges with taxes Update above old code commented**/
//				{ fblanck, fblanck, fblanck, faddOtherCharges,fdbtl },
//				{ fblanck, fblanck, fothChgTbl,  },
//				{ fblanck, fblanck, fblanck, fblanck,fdoamtincltax },
//				{ fblanck, fblanck, fproducttaxtable}, 
//				/** ends here ***/ 
//				
//				{ fblanck, fblanck, fblanck,fblanck, fdonetpayamt }, 
//				{fblankgroup,fblankgroupone,fpaymentrecieved},
//				{fblankgroup,fblankgroupone,fbalancepayment},
//				{ fvendorTableGroup},
//				{fbtnNewVendor},
//				{ fvendorComp,fbtaddVendor},
//				{ fvendortable },
//				{ fcbadds , ftbtersOfDelivery ,ftbPocName,fIbPocNumber}, { fdeliveryadd },
				
				{ fgroupingLetterInformation },
				{ ftbpoName, fpodate,folbcPaymentMethods,fbranch},
				{ femployee,folbAuthorisedBy,ftbApporverName},
				
				//Product Info
				{ fproductInformation },
				{ fprodInfoComposite, faddproducts },
				{ fproductTablePO },
				{ fblanck, fblanck, fblanck, faddOtherCharges,fdbtl },
				{ fblanck, fblanck, fothChgTbl,  },
				{ fblanck, fblanck, fblanck, fblanck,fdoamtincltax },
				{ fblanck, fblanck, fproducttaxtable}, 
				{ fblanck, fblanck, fblanck,fblanck, fdonetpayamt }, 
				{fblankgroup,fblankgroupone,fpaymentrecieved},
				{fblankgroup,fblankgroupone,fbalancepayment},
				
				//vendor
				{ fvendorTableGroup},
				{ fbtnNewVendor},
				{ fvendorComp,fbtaddVendor},
				{ fvendortable },
				
				//Delivery
				{fgroupingDelivery},
				{ fcbadds , ftbtersOfDelivery },
				{ftbPocName,fIbPocNumber},
				{ fdeliveryadd },	
				//clasification
				{fgroupingClassification},
				{ floigroup, floitype},
				//Reference and General
				{ fgroupingDescription},
				{ freferenceorderno,fdbrefDate,fexpdeldate},
				{ ftadescription},
				//Attachment
				{fgroupingAttachment},
				{fdoc}
				

		};

		this.fields = formfield;

	}
	
	

	@Override
	public void updateModel(PurchaseOrder model) {
		if (tbpoName.getValue() != null)
			model.setPOName(tbpoName.getValue());
		if (ibRefOrderNo.getValue() != null)
			model.setRefOrderNO(ibRefOrderNo.getValue());
		if (oblEmployee.getValue() != null)
			model.setEmployee(oblEmployee.getValue());
		if (oblPurchaseOrderCategory.getValue() != null)
			model.setLOIGroup(oblPurchaseOrderCategory.getValue());
		if (oblPurchaseOrderType.getValue() != null)
			model.setLOItype(oblPurchaseOrderType.getValue(oblPurchaseOrderType.getSelectedIndex()));
		if (productTablePO.getValue() != null)
			model.setProductDetails(productTablePO.getValue());
		if (vendorTablePO.getValue() != null)
			model.setVendorDetails(vendorTablePO.getValue());
		if (tbStatus.getValue() != null)
			model.setStatus(tbStatus.getValue());
		if (dbTotal.getValue() != 0)
			model.setTotalAmount(dbTotal.getValue());
		if (dbPODate.getValue() != null)
			model.setPODate(dbPODate.getValue());
		if (branch.getValue() != null)
			model.setBranch(branch.getValue());
		if (tbApporverName.getValue() != null)
			model.setApproverName(tbApporverName.getValue());
		if (dbExpdeliverydate.getValue() != null)
			model.setDeliveryDate(dbExpdeliverydate.getValue());
		if (dbrefDate.getValue() != null)
			model.setReferenceDate(dbrefDate.getValue());
		if (upload.getValue() != null)
			model.setUptestReport(upload.getValue());
		if (deliveryadd.getValue() != null)
			model.setAdress(deliveryadd.getValue());
		if (producttaxtable.getValue() != null)
			model.setProductTaxes(producttaxtable.getValue());
		if (productchargestable.getValue() != null)
			model.setProductCharges(productchargestable.getValue());
		
		ArrayList<PaymentTerms> PayTermslist=new ArrayList<PaymentTerms>();
		PayTermslist = getpaymentterms(donetpayamt.getValue(),dbpaymentrecieved.getValue());
		model.setPaymentTermsList(PayTermslist);
		
		
		model.setDeliverAdd(cbadds.getValue());
		
		if (tadescription.getValue() != null)
			model.setDescription(tadescription.getValue());
		
		if(olbcPaymentMethods.getValue()!=null)
			model.setPaymentMethod(olbcPaymentMethods.getValue());
		model.setTotal(dbTotal.getValue());
		model.setGtotal(doamtincltax.getValue());
		model.setNetpayble(donetpayamt.getValue());
		
		
		if(dbpaymentrecieved.getValue()!=null){
			model.setPaymentPaid(dbpaymentrecieved.getValue());
		}
		if(dbbalancepayment.getValue()!=null){
			model.setBalancePayment(dbbalancepayment.getValue());
		}
		
		 /**
		 * Date : 24-11-2017 BY Vijay
		 * Des :- for other charges with taxes
		 */
		List<OtherCharges> otherChargesList=this.tblOtherCharges.getDataprovider().getList();
		ArrayList<OtherCharges> otherChargesArr=new ArrayList<OtherCharges>();
		otherChargesArr.addAll(otherChargesList);
		model.setOtherCharges(otherChargesArr);
		
		/**
		 * End
		 */
		
		 /** Date 26-11-2018 added by Vijay *****/
		  if(olbAuthorisedBy.getSelectedIndex()!=0){
			  model.setAuthorisedBy(olbAuthorisedBy.getValue(olbAuthorisedBy.getSelectedIndex()));
		  }
		  if(tbtersOfDelivery.getValue()!=null){
				model.setTermsOfDelivery(tbtersOfDelivery.getValue());
		  }
		  
		  if(tbPocName.getValue()!=null){
				model.setPocName(tbPocName.getValue());
			}
		  if(IbPocNumber.getValue()!=null){
				model.setPocNumber(IbPocNumber.getValue());
			}else{
				model.setPocNumber(0);
			}
		  
		  
		  
		purchaseOrderobj=model;
	}

	@Override
	public void updateView(PurchaseOrder model) {
		
		purchaseOrderobj=model;
		ibPOId.setValue(model.getCount()+"");
		
		ibRefOrderNo.setValue(model.getRefOrderNO());
		if (model.getEmployee() != null)
			oblEmployee.setValue(model.getEmployee());
		if (model.getLOIGroup() != null)
			oblPurchaseOrderCategory.setValue(model.getLOIGroup());
		if (model.getLOItype() != null)
			oblPurchaseOrderType.setValue(model.getLOItype());
		
		if (model.getApproverName() != null)
			tbApporverName.setValue(model.getApproverName());
		
		if (model.getProductDetails() != null){
			productTablePO.setValue(model.getProductDetails());
			
			List<String> prodIdList=new ArrayList<String>();
			HashSet<String> uniqueCodeList=new HashSet<>();
			for(ProductDetailsPO prodDet:model.getProductDetails()){
				uniqueCodeList.add(prodDet.getProductCode());
			}
			prodIdList.addAll(uniqueCodeList);	
//			loadVendorProductPriceList(prodIdList);
		}
		if (model.getVendorDetails() != null)
			vendorTablePO.setValue(model.getVendorDetails());
		if (model.getStatus() != null)
			tbStatus.setValue(model.getStatus());
		if (model.getPOName() != null)
			tbpoName.setValue(model.getPOName());
		if (model.getTotalAmount() != 0)
			dbTotal.setValue(model.getTotalAmount());
//		if (model.getPaymentTermsList() != null)
//			paymentTermsTable.setValue(model.getPaymentTermsList());
		if (model.getPODate() != null)
			dbPODate.setValue(model.getPODate());
		if (model.getBranch() != null)
			branch.setValue(model.getBranch());
		if (model.getDeliveryDate() != null)
			dbExpdeliverydate.setValue(model.getDeliveryDate());
		if (model.getUptestReport() != null)
			upload.setValue(model.getUptestReport());
		if (model.getAdress() != null)
			deliveryadd.setValue(model.getAdress());
		if (model.getProductTaxes() != null)
			producttaxtable.setValue(model.getProductTaxes());
		if (model.getProductCharges() != null)
			productchargestable.setValue(model.getProductCharges());
		if (model.getReferenceDate()!= null)
			dbrefDate.setValue(model.getReferenceDate());
		if(model.getPaymentMethod()!=null)
			olbcPaymentMethods.setValue(model.getPaymentMethod());
		cbadds.setValue(model.isDeliverAdd());
		
		
		if (model.getDescription() != null)
			tadescription.setValue(model.getDescription());
			
		dbTotal.setValue(model.getTotal());
		doamtincltax.setValue(model.getGtotal());
		donetpayamt.setValue(model.getNetpayble());
		
		
		if(this.validateVendorselect()){
			int vendorCount=retrieveVendorCount();
			retVendorState(vendorCount);
		}
		
		/**
		 * Date : 26-11-2018 By Vijay
		 * for Other Charges
		 */
		tblOtherCharges.setValue(model.getOtherCharges());
		/**
		 * End
		 */
		
		
		if(model.getPaymentPaid()!=0){
			dbpaymentrecieved.setValue(model.getPaymentPaid());
		}
		
		if(model.getBalancePayment()!=0){
			dbbalancepayment.setValue(model.getBalancePayment());
			
		}
		
		 /** Date 26-11-2018 added by Vijay **/
		if(model.getAuthorisedBy()!=null){
			olbAuthorisedBy.setValue(model.getAuthorisedBy());
		}
		if(model.getTermsOfDelivery()!=null){
			tbtersOfDelivery.setValue(model.getTermsOfDelivery());
		}	
		
		if(model.getPocName()!=null){
			tbPocName.setValue(model.getPocName());
		}
		
		if(model.getPocNumber()!=0){
			IbPocNumber.setValue(model.getPocNumber());
		}
		
		
		
		/* 
		 * for approval process
		 *  nidhi
		 *  5-07-2017
		 */
		if(presenter != null){
			presenter.setModel(model);
		}
		/*
		 *  end
		 */
	}
	
	private ArrayList<PaymentTerms> getpaymentterms(Double netpayble, Double paymentrecieved) {

		double percentage = 0 ;

		System.out.println(" net payable =="+netpayble);
		System.out.println(" payment recived === "+paymentrecieved);
		if(paymentrecieved==null || paymentrecieved==0){
			percentage = 100;
		}else{
			percentage = getpercentage(netpayble,paymentrecieved);
		}
			
		System.out.println(" percentage  == "+percentage);
		ArrayList<PaymentTerms> billingPayTerms=new ArrayList<PaymentTerms>();
		
		if(100-percentage==0 || 100-percentage==100.0){
			
			if(paymentrecieved == null || paymentrecieved==0){
				
				PaymentTerms paymentTerms=new PaymentTerms();

				System.out.println(" for 100 % balance payment ");
				paymentTerms.setPayTermDays(0);
				
				String pay = numfmt.format(percentage);
				double paypercent= Double.parseDouble(pay);
				
				paymentTerms.setPayTermPercent(paypercent);
				paymentTerms.setPayTermComment("Balance Payment");
				billingPayTerms.add(paymentTerms);
				return billingPayTerms;
			}
			else{
				
				PaymentTerms paymentTerms=new PaymentTerms();

				System.out.println(" for 100 % payment recieved ");
				paymentTerms.setPayTermDays(0);
				String pay = numfmt.format(percentage);
				double paypercent= Double.parseDouble(pay);
				
				paymentTerms.setPayTermPercent(paypercent);
				paymentTerms.setPayTermComment("Payment Recieved");
				billingPayTerms.add(paymentTerms);
				return billingPayTerms;
			}
			
		}else{
			
			for(int i=0;i<2;i++){
				System.out.println(" for partial payment");
				if(i==1){
					double percent = 100-percentage;
					System.out.println("Percentage ==== $$$$$$$$$$$$$$$$$ "+percent);
					PaymentTerms paymentTerms=new PaymentTerms();
					paymentTerms.setPayTermDays(0);  
					double paypercent= Math.round(percent);
					paymentTerms.setPayTermPercent(paypercent);
					paymentTerms.setPayTermComment("Balance Payment");
					billingPayTerms.add(paymentTerms);
					
					return billingPayTerms;

				}else{
					PaymentTerms paymentTerms=new PaymentTerms();

					System.out.println("Payment recieved billing doc ================");
					paymentTerms.setPayTermDays(0);
					
					double paypercent= Math.round(percentage);

					paymentTerms.setPayTermPercent(paypercent);
					paymentTerms.setPayTermComment("Payment Recieved");
					billingPayTerms.add(paymentTerms);

				}
				
			}
		
		}
		
		return billingPayTerms;
	}
	
	
	private double getpercentage(Double netpayble, Double paymentrecieved) {
		double totalpercentage;
		totalpercentage= paymentrecieved/netpayble*100;
		return totalpercentage;
	}
	
	@Override
	public void toggleAppHeaderBarMenu() {
		if (AppMemory.getAppMemory().currentState == ScreeenState.NEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Save") || text.equals("Discard")
						|| text.equals("Search")|| text.equals(AppConstants.NAVIGATION)) {
					menus[k].setVisible(true);
				} else
					menus[k].setVisible(false);
			}
		}
		else if (AppMemory.getAppMemory().currentState == ScreeenState.EDIT) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Save") || text.equals("Discard")|| text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true);
				else
					menus[k].setVisible(false);
			}
		}

		else if (AppMemory.getAppMemory().currentState == ScreeenState.VIEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Discard") || text.equals("Edit")|| text.equals("Print") || text.equals("Search")|| text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true);
				else
					menus[k].setVisible(false);
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.QUICKPURCHASEORDER,LoginPresenter.currentModule.trim());
	}
	
	
	public void toggleProcessLevelMenu() {
		System.out.println("TOGGLE METHOD");
		PurchaseOrder entity = (PurchaseOrder) presenter.getModel();
		String status = entity.getStatus();
		System.out.println("STATUS "+status);

		for (int i = 0; i < getProcesslevelBarNames().length; i++) {
			InlineLabel label = getProcessLevelBar().btnLabels[i];
			String text = label.getText().trim();
			
			if ((status.equals(PurchaseOrder.CREATED))) {
				System.out.println("CREATED ");
				if (text.equals(AppConstants.CANCELPURCHASEORDER))
					label.setVisible(false);
				if (text.equals(AppConstants.CANCELLATIONSUMMARY))
					label.setVisible(false);
				
				if(AppUtility.isPrAssignToPO("PurchaseRequisition")){
					System.out.println("PR CONFIG TRUE");
					if (text.equals(AppConstants.SENDMAILTOASSIGNPERSON)){
						System.out.println("OK");
						label.setVisible(true);
					}
				}else{
					System.out.println("CONFIG FALSE");
					if (text.equals(AppConstants.SENDMAILTOASSIGNPERSON)){
						label.setVisible(false);
					}
				}
				
				if(text.contains(AppConstants.SUBMIT))
					label.setVisible(true);
				if(text.contains(AppConstants.VIEWINVOICEPAYMENT))
					label.setVisible(false);
				
				if (text.equals(AppConstants.CANCELPURCHASEORDER))
					label.setVisible(false);
				

			}else if (status.equals(PurchaseOrder.APPROVED)) {
				if (text.equals(AppConstants.CANCELPURCHASEORDER))
					label.setVisible(true);
				if (text.equals(AppConstants.CANCELLATIONSUMMARY))
					label.setVisible(false);
				if (text.equals(AppConstants.SENDMAILTOASSIGNPERSON))
					label.setVisible(false);
				if(text.contains(AppConstants.SUBMIT))
					label.setVisible(false);
				if(text.contains(AppConstants.VIEWINVOICEPAYMENT))
					label.setVisible(true);
				
				
			} 
			else if (status.equals(PurchaseOrder.CANCELLED)) {
				if (text.equals(AppConstants.CANCELPURCHASEORDER))
					label.setVisible(false);
				if (text.equals(AppConstants.CANCELLATIONSUMMARY))
					label.setVisible(true);
				if (text.equals(ManageApprovals.APPROVALREQUEST))
					label.setVisible(false);
				if (text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
					label.setVisible(false);
				if (text.equals(AppConstants.SENDMAILTOASSIGNPERSON))
					label.setVisible(false);
				if(text.contains(AppConstants.VIEWINVOICEPAYMENT))
					label.setVisible(false);
				
				if(text.equals(ManageApprovals.SUBMIT))
					label.setVisible(false);
				
			} else if (status.equals(PurchaseOrder.REJECTED)) {
				if (text.equals(ManageApprovals.APPROVALREQUEST))
					label.setVisible(false);
				if (text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
					label.setVisible(false);
				if (text.equals(AppConstants.CANCELPURCHASEORDER))
					label.setVisible(false);
				if (text.equals(AppConstants.CANCELLATIONSUMMARY))
					label.setVisible(false);
				if (text.equals(AppConstants.SENDMAILTOASSIGNPERSON))
					label.setVisible(false);
				if(text.contains(AppConstants.SUBMIT))
					label.setVisible(false);
				if(text.contains(AppConstants.VIEWINVOICEPAYMENT))
					label.setVisible(false);
				if (text.equals(AppConstants.CANCELPURCHASEORDER))
					label.setVisible(false);
				
			}
			
			
			
			else {
				if (text.equals("Mark Close"))
					label.setVisible(false);
				if (text.equals("Create GRN"))
					label.setVisible(false);
				if (text.equals(AppConstants.CANCELPURCHASEORDER))
					label.setVisible(false);
				if (text.equals(AppConstants.CANCELLATIONSUMMARY))
					label.setVisible(false);
				if (text.equals(AppConstants.SENDMAILTOASSIGNPERSON))
					label.setVisible(false);
				if(text.contains(AppConstants.SUBMIT))
					label.setVisible(false);
				if(text.contains(AppConstants.VIEWINVOICEPAYMENT))
					label.setVisible(false);
			}
			
			
			
		}
	}
	


	/******************************************** Set Enable Method **************************************************/

	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		productTablePO.setEnable(state);
		vendorTablePO.setEnable(state);
		ibPOId.setEnabled(false);
		tbStatus.setEnabled(false);
		dbTotal.setEnabled(false);
		doamtincltax.setEnabled(false);
		donetpayamt.setEnabled(false);
		deliveryadd.setEnable(false);
		dbPODate.setEnabled(false);
		/**
		 *  nidhi
		 *  18-07-2017
		 *   make view state as required 
		 */
			productchargestable.setEnable(state);
		/**
		 *  end
		 */
		dbbalancepayment.setEnabled(false);
		/*** Date 26-11-2018 By Vijay ****/
		tbtersOfDelivery.setEnabled(state);
		olbAuthorisedBy.setEnabled(state);
		tblOtherCharges.setEnable(state);
	}

//	@Override
//	public TextBox getstatustextbox() {
//		return this.tbStatus;
//	}
	public boolean validateWarehouse(){
		int ctr=0;
		for(int i=0;i<productTablePO.getDataprovider().getList().size();i++){
			if(!productTablePO.getDataprovider().getList().get(i).getItemProductStrorageLocation().equals("")
					&&!productTablePO.getDataprovider().getList().get(i).getItemProductStorageBin().equals("")
					&&!productTablePO.getDataprovider().getList().get(i).getItemProductWarehouseName().equals("")){
				ctr++;
			}
		}
		if(ctr==productTablePO.getDataprovider().getList().size()){
			return true;
		}else{
			return false;
		}
	}

	/************************************** validate only vendor is selected ****************************************/

	public boolean validateVendorselect() {
		List<VendorDetails> list = vendorTablePO.getDataprovider().getList();
		if(list.size()!=0){
			for(VendorDetails venDet:list){
				if(venDet.getStatus()==true){
					return true;
				}
			}
		}else{
			return true;
		}
		return false;
	}

	/****************************************** Validation Method *********************************************************/
	@Override
	public boolean validate() {
		boolean a = super.validate();
		if (a == false)
			return false;

		
		if(validateWarehouse()==false){
			showDialogMessage("Please Select Warehouse Details.");
			return false;
		}
		
		
		
//		if (dbPODate.getValue().after(dbExpdeliverydate.getValue())) {
//			this.showDialogMessage("Expected Delivery Date Should Be Greater Than Creation Date.");
//			return false;
//		} 
		
//		boolean payment = validatePaymentTermPercent();
//		if (payment == false) {
//			this.showDialogMessage("In Payment Terms total of percent column should be equal to 100");
//			return false;
//		}
		if (productTablePO.getDataprovider().getList().size() == 0) {
			this.showDialogMessage("Please add products");
			return false;
		}
		
		boolean b = validateVendorselect();
		if (b == false) {
			this.showDialogMessage("Please Select Vendor !");
			return false;
		}
		
//		if(deliveryadd.getState().getSelectedIndex()!=0){
//			String vendorState=PurchaseOrderPresenter.vendorState.trim();
//			System.out.println("Add. valdn VendorState::: "+vendorState);
//			
//			if(!vendorState.equals("")&&!getDeliveryadd().getState().getValue().trim().equals(vendorState)&&this.getCbcformlis().getSelectedIndex()==0){
//				showDialogMessage("C Form is Mandatroy");
//				return false;
//			}
//			if(getCbcformlis().getItemText(getCbcformlis().getSelectedIndex()).trim().equals(AppConstants.YES)&&getOlbcstpercent().getSelectedIndex()==0){
//				showDialogMessage("Please Select CST Percent !");
//				return false;
//			}
//		}
//		if(!validateCForm()){
//			return false;
//		}
		
//		if(ProductTablePO.isValidWarehouse==false){
//			showDialogMessage("Warehouse is already selected for product, please change warehouse or delete row.");
//			return false;
//		}
		
//		if(!isAllWarehouseIsSelectedOrDeselected()){
//			return false;
//		}
		
		 /**
		 * Date 03-07-2017 added by vijay tax validation
		 */
		List<ProductOtherCharges> prodtaxlist = this.producttaxtable.getDataprovider().getList();
		for(int i=0;i<prodtaxlist.size();i++){
			
			if(dbPODate.getValue()!=null && dbPODate.getValue().before(date)){
				if(prodtaxlist.get(i).getChargeName().equalsIgnoreCase("CGST") || prodtaxlist.get(i).getChargeName().equalsIgnoreCase("SGST")
						|| prodtaxlist.get(i).getChargeName().equalsIgnoreCase("IGST")){
					showDialogMessage("As Purchase Order date "+fmt.format(dbPODate.getValue())+" GST Tax is not applicable");
					return false;
				}
			}
			else if(dbPODate.getValue()!=null && dbPODate.getValue().equals(date) || dbPODate.getValue().after(date)){
				if(prodtaxlist.get(i).getChargeName().equalsIgnoreCase("CST") || prodtaxlist.get(i).getChargeName().equalsIgnoreCase("Service Tax")
						|| prodtaxlist.get(i).getChargeName().equalsIgnoreCase("VAT")){
					showDialogMessage("As Purchase Order date "+fmt.format(dbPODate.getValue())+" VAT/CST/Service Tax is not applicable");
					return false;
				}
			}
		}
		/**
		 * ends here
		 */
		
		if(this.vendorTablePO.getDataprovider().getList().size()==0){
			showDialogMessage("Please add vendor details");
			return false;
		}
		/**
		 * nidhi
		 * 20-09-2018
		 */
		if(LoginPresenter.mapModelSerialNoFlag ){
			if(!validateSerailNo()){
				showDialogMessage("Please verify Product Qty and serial number selection.");
				return false;
			}
		}
		return true;
	}
	
	boolean validateSerailNo(){
		boolean flag = true;
		
		for(int i=0;i<productTablePO.getDataprovider().getList().size();i++){
			int qty = (int) productTablePO.getDataprovider().getList().get(i).getProductQuantity();
			int serqty = 0;
			if(productTablePO.getDataprovider().getList().get(i).getProSerialNoDetails()!=null  
					&& productTablePO.getDataprovider().getList().get(i).getProSerialNoDetails().size()>0
					&& productTablePO.getDataprovider().getList().get(i).getProSerialNoDetails().containsKey(0)){
				for(int j = 0;j<productTablePO.getDataprovider().getList().get(i).getProSerialNoDetails().get(0).size() ; j++){
					if (productTablePO.getDataprovider().getList().get(i).getProSerialNoDetails().get(0).get(j).getProSerialNo().trim().length()>0){
						serqty++;
					}
				}
				if(serqty>0 && serqty==qty){
					flag = true;
				}else if(serqty!=0 && serqty!=qty){
					flag = false;
					break;
				}
			}
		}
		
		return flag; 
				
	}
		/**
		 * This method is used to validate state from the delivery address and verify it with the selected vendor state.
		 * If state is not equal with the state in the delivery address the it returns false, else it returns true.
		 * @return
		 */
		
		// ******************************** validate Vendor Sate with Address Composite State **************************
		
		public boolean validateState() {
			
			if(isWarehouseSelected()){
				List<ProductDetailsPO> list=productTablePO.getDataprovider().getList();
				for(ProductDetailsPO obj:list){
					if(!obj.getWarehouseName().equals("")&&!PurchaseOrderPresenter.vendorState.equals("")){
						String warehouseState=ProductTablePO.getWarehouseState(obj.getWarehouseName());
						if(!PurchaseOrderPresenter.vendorState.trim().equals(warehouseState.trim())){
							return false;
						}
					}
				}
				return true;
			}else{
				if(this.getDeliveryadd().getState().getSelectedIndex()!=0&&this.getDeliveryadd().getState().getValue().trim().equals(PurchaseOrderPresenter.vendorState.trim())){
					return true; 
				}
			}
			return false;
		}
		
		// ****************************** Validation For Duplicate Product **************************************
		
			public boolean validateProductDuplicate(String pcode) {
				
				
				
//				if(dbExpdeliverydate.getValue()==null){
//					this.showDialogMessage("Please select delivery date !");
//					return false;
//				}
				
				if(ProductTablePO.isValidWarehouse==false){
					showDialogMessage("Warehouse is already selected for product, please change warehouse or delete row.");
					return false;
				}
				
				List<ProductDetailsPO> list = productTablePO.getDataprovider().getList();
				for (ProductDetailsPO temp : list) {
					if (temp.getProductCode().trim().equals(pcode.trim())&&temp.getWarehouseName().equals("")) {
						this.showDialogMessage("Can not add Duplicate product");
						return false;
					}
				}
				return true;
				
			}
			
		
		// *********************** Row Count change Method **************************************************
		
	public void onRowCountChange(RowCountChangeEvent event) {
		if (productTablePO.getDataprovider().getList().size() == 0) {
			productTablePO.connectToLocal();
		} 
	}


	/************************* Initializing All Object List Element **********************************************/


	protected void initalizEmployeeNameListBox() {
		MyQuerry querry = new MyQuerry(new Vector<Filter>(), new Employee());
		oblEmployee.MakeLive(querry);
	}

	protected void initalizEmployeeBranchListBox() {
		MyQuerry querry = new MyQuerry(new Vector<Filter>(), new Branch());
		branch.MakeLive(querry);
	}

	/***************************************** set ID count ******************************************************/

	@Override
	public void setCount(int count) {
		ibPOId.setValue(count+"");
	}



	/******************************************** set FORM as per state *********************************************/

	@Override
	public void setToViewState() {
		super.setToViewState();
		setMenuAsPerStatus();
		
		SuperModel model=new PurchaseOrder();
		model=purchaseOrderobj;
		AppUtility.addDocumentToHistoryTable(AppConstants.PURCHASEMODULE,AppConstants.PURCHASEORDER, purchaseOrderobj.getCount(), null,null,null, false, model, null);
	
		String mainScreenLabel="Purchase Register";
		if(purchaseOrderobj!=null&&purchaseOrderobj.getCount()!=0){
			mainScreenLabel=purchaseOrderobj.getCount()+"/"+purchaseOrderobj.getStatus()+"/"+AppUtility.parseDate(purchaseOrderobj.getCreationDate());
		}
		fgroupingLetterInformation.getHeaderLabel().setText(mainScreenLabel);
	}

	@Override
	public void setToNewState() {
		super.setToNewState();
		Company com = new Company();
		getCompanyAddress(com.getCompanyId());
		tbStatus.setValue(PurchaseOrder.CREATED);
		cbadds.setValue(false);
	}

	@Override
	public void setToEditState() {
		super.setToEditState();
		setMenuAsPerStatus();
		if (tbStatus.getValue().equals(PurchaseOrder.REJECTED)) {
			tbStatus.setValue(PurchaseOrder.CREATED);
		}
		this.processLevelBar.setVisibleFalse(false);
		
		String mainScreenLabel="Purchase Register";
		if(purchaseOrderobj!=null&&purchaseOrderobj.getCount()!=0){
			mainScreenLabel=purchaseOrderobj.getCount()+"/"+purchaseOrderobj.getStatus()+"/"+AppUtility.parseDate(purchaseOrderobj.getCreationDate());
		}
		fgroupingLetterInformation.getHeaderLabel().setText(mainScreenLabel);
	}

	/******************************* Set Menu Method for header and Process level menu **************************************/

	public void setMenuAsPerStatus() {
		this.setAppHeaderBarAsPerStatus();
		this.toggleProcessLevelMenu();
	}

	/************************************ Set App Header Bar As per Status ***********************************************/

	public void setAppHeaderBarAsPerStatus() {
		PurchaseOrder entity = (PurchaseOrder) presenter.getModel();
		String status = entity.getStatus();
		if (status.equals(PurchaseOrder.CLOSED)|| status.equals(PurchaseOrder.CANCELLED))
		{
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Discard") || text.contains("Search")|| text.contains(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true);
				} else {
					menus[k].setVisible(false);
				}
			}
		}
		
		if (status.equals(PurchaseOrder.APPROVED))
		{
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Discard") || text.contains("Search")|| text.contains("Print")|| text.contains("Email")|| text.contains(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true);
				} else {
					menus[k].setVisible(false);
				}
			}
		}
	}

	/************************************************* set Tax table **********************************************/
	
	
	/*****************************************Add Product Taxes Method***********************************/
	
	/**
	 * This method is called when a product is added to SalesLineItem table.
	 * Taxes related to corresponding products will be added in ProductTaxesTable.
	 * @throws Exception
	 */
	public void addProdTaxes() throws Exception{
		System.out.println();
		System.out.println("INSIDE ADD PRODUCT TAXES METHOD !!");
		
		List<ProductDetailsPO> salesLineItemLis=this.productTablePO.getDataprovider().getList();
		List<ProductOtherCharges> taxList=this.producttaxtable.getDataprovider().getList();
		NumberFormat nf=NumberFormat.getFormat("0.00");
		String cformval="";
		
		System.out.println("PRODUCT LIST SIZE : "+salesLineItemLis.size());
		
		for(int i=0;i<salesLineItemLis.size();i++){
			double priceqty=0,taxPrice=0;
			
			if((salesLineItemLis.get(i).getDiscount()==null || salesLineItemLis.get(i).getDiscount()==0) && (salesLineItemLis.get(i).getDiscountAmt()==0) ){
//				System.out.println("inside both 0 condition");
				System.out.println("NO DISCOUNT");
				taxPrice=this.getProductTablePO().removeAllTaxes(salesLineItemLis.get(i).getPrduct());
				priceqty=(salesLineItemLis.get(i).getProdPrice()-taxPrice)*salesLineItemLis.get(i).getProductQuantity();
				priceqty=Double.parseDouble(nf.format(priceqty));
			}else if(((salesLineItemLis.get(i).getDiscount()!=null)&&(salesLineItemLis.get(i).getDiscountAmt()!=0))){
//				System.out.println("inside both not null condition");
				taxPrice=this.getProductTablePO().removeAllTaxes(salesLineItemLis.get(i).getPrduct());
				priceqty=(salesLineItemLis.get(i).getProdPrice()-taxPrice);
				priceqty=priceqty-(priceqty*salesLineItemLis.get(i).getDiscount()/100);
				priceqty=priceqty-salesLineItemLis.get(i).getDiscountAmt();
				priceqty=priceqty*salesLineItemLis.get(i).getProductQuantity();
				priceqty=Double.parseDouble(nf.format(priceqty));
			}else {
//				System.out.println("inside oneof the null condition");
				taxPrice=this.getProductTablePO().removeAllTaxes(salesLineItemLis.get(i).getPrduct());
				priceqty=(salesLineItemLis.get(i).getProdPrice()-taxPrice);
				if(salesLineItemLis.get(i).getDiscount()!=null){
					priceqty=priceqty-(priceqty*salesLineItemLis.get(i).getDiscount()/100);
				}
				else{
					priceqty=priceqty-salesLineItemLis.get(i).getDiscountAmt();
				}
				priceqty=priceqty*salesLineItemLis.get(i).getProductQuantity();
				priceqty=Double.parseDouble(nf.format(priceqty));
			}
			System.out.println();
			System.out.println("Total Price Excluding Tax "+i+" - "+priceqty);
			
			
			
			
		/**
		 * Adding Vat Tax to table if the company state and delivery state is equal
		 */
			
			//Date 03 august 2017 below if condition commented by vijay no need this if condition here
//		if(validateState()){
//			if(salesLineItemLis.get(i).getVat()!=0){
				if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
				
				System.out.println("VAT !=0 -- ");
				/**
				 * Date 2 August 2017 below if condition added by vijay for GST old code added in else block
				 */
				if(salesLineItemLis.get(i).getVatTax().getTaxPrintName()!=null && !salesLineItemLis.get(i).getVatTax().getTaxPrintName().equals("")){
					System.out.println("GST");
					ProductOtherCharges pocentity=new ProductOtherCharges();
					// below code added by vijay for GST
					pocentity.setChargeName(salesLineItemLis.get(i).getVatTax().getTaxPrintName());
//					pocentity.setChargePercent(salesLineItemLis.get(i).getVat());
					pocentity.setChargePercent(salesLineItemLis.get(i).getVatTax().getPercentage());

					pocentity.setIndexCheck(i+1);
					int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getVat(), salesLineItemLis.get(i).getVatTax().getTaxPrintName());
					
					if(indexValue!=-1){
						System.out.println("Index As Not -1");
						pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
						this.getProducttaxtable().getDataprovider().getList().remove(indexValue);
					}
					if(indexValue==-1){
						System.out.println("VAt");
						pocentity.setAssessableAmount(priceqty);
					}
					this.producttaxtable.getDataprovider().getList().add(pocentity);
					
				}else{
					System.out.println("Vattx");
					ProductOtherCharges pocentity=new ProductOtherCharges();
					pocentity.setChargeName("VAT");
//					pocentity.setChargePercent(salesLineItemLis.get(i).getVat());
					pocentity.setChargePercent(salesLineItemLis.get(i).getVatTax().getPercentage());
					pocentity.setIndexCheck(i+1);
					int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getVat(),"VAT");
					
					if(indexValue!=-1){
						System.out.println("Index As Not -1");
						pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
						this.getProducttaxtable().getDataprovider().getList().remove(indexValue);
					}
					if(indexValue==-1){
						System.out.println("VAt");
						pocentity.setAssessableAmount(priceqty);
					}
					this.producttaxtable.getDataprovider().getList().add(pocentity);
					
				}
				
			}
//		}
			
		
		
		
			
			/**
			 * Adding service tax to the table
			 */
			System.out.println(" salesLineItemLis.get(i).getTax() =="+salesLineItemLis.get(i).getTax());
//			if(salesLineItemLis.get(i).getTax()!=0){
				if(salesLineItemLis.get(i).getServiceTax().getPercentage()!=0){

				System.out.println("Adding service tax to the table");
				System.out.println("ServiceTx");
				ProductOtherCharges pocentity=new ProductOtherCharges();
//				pocentity.setChargeName("Service Tax");
				
				/**
				 * Date 2 August 2017 added by vijay for GST and old code added in else block 
				 */
				if(salesLineItemLis.get(i).getServiceTax().getTaxPrintName()!=null && !salesLineItemLis.get(i).getServiceTax().getTaxPrintName().equals(""))
				{
					pocentity.setChargeName(salesLineItemLis.get(i).getServiceTax().getTaxPrintName());
				}else{
					pocentity.setChargeName("Service Tax");
				}
				/**
				 * ends here
				 */
				
				
				
//				pocentity.setChargePercent(salesLineItemLis.get(i).getTax());
				pocentity.setChargePercent(salesLineItemLis.get(i).getServiceTax().getPercentage());

				pocentity.setIndexCheck(i+1);
//				int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getTax(),"Service");
				
				
				int indexValue;
				/**
				 * Date 12 july 2017 if condition for old code and else block for GST added by vijay
				 */
				if(salesLineItemLis.get(i).getServiceTax().getTaxPrintName()!=null && !salesLineItemLis.get(i).getServiceTax().getTaxPrintName().equals(""))
				{
					indexValue =this.checkTaxPercent(salesLineItemLis.get(i).getServiceTax().getPercentage(),salesLineItemLis.get(i).getServiceTax().getTaxPrintName());
				}else{
					indexValue =this.checkTaxPercent(salesLineItemLis.get(i).getServiceTax().getPercentage(),"Service");
				}
				/**
				 * ends here
				 */
				
				if(indexValue!=-1){
					System.out.println("Index As Not -1");
					/**
					 * If validate state method is true
					 */
					/**
					 * Date 26-11-2018 By Vijay
					 * Des :- if condition commented for Taxes map properly
					 */
//					if(validateState()){
						System.out.println("VAlidate true");
						double assessValue=0;
						if(salesLineItemLis.get(i).getVat()!=0){
							
							/**
							 * Date 11 july 2017 if condition added by vijay for GST and old code added in else block 
							 */
							if(salesLineItemLis.get(i).getServiceTax().getTaxPrintName()!=null && !salesLineItemLis.get(i).getServiceTax().getTaxPrintName().equals("")){
								System.out.println("For GST !! ");
								assessValue=priceqty;
								pocentity.setAssessableAmount(assessValue+taxList.get(indexValue).getAssessableAmount());
								this.getProducttaxtable().getDataprovider().getList().remove(indexValue);
							}else{
								System.out.println("OLD Code");
								assessValue=priceqty+(priceqty*salesLineItemLis.get(i).getVat()/100);
								pocentity.setAssessableAmount(assessValue+taxList.get(indexValue).getAssessableAmount());
								this.getProducttaxtable().getDataprovider().getList().remove(indexValue);
							}
						}
						if(salesLineItemLis.get(i).getVat()==0){
							pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
							this.getProducttaxtable().getDataprovider().getList().remove(indexValue);
						}
//					}
					
					
				}
				if(indexValue==-1){
					System.out.println("Index As -1");
					
					if(validateState()){
						double assessVal=0;
						if(salesLineItemLis.get(i).getVat()!=0){
							
							/**
							 * Date 11 july 2017 if condition added by vijay for GST and old code added in else block 
							 */
							if(salesLineItemLis.get(i).getServiceTax().getTaxPrintName()!=null && !salesLineItemLis.get(i).getServiceTax().getTaxPrintName().equals("")){
								assessVal=priceqty;
//								pocentity.setAssessableAmount(assessVal+taxList.get(indexValue).getAssessableAmount());
								pocentity.setAssessableAmount(assessVal);

								System.out.println(assessVal);
							}else{
								assessVal=priceqty+(priceqty*salesLineItemLis.get(i).getVat()/100);
								pocentity.setAssessableAmount(assessVal);
							}
							 
						}
						if(salesLineItemLis.get(i).getVat()==0){
							pocentity.setAssessableAmount(priceqty);
						}
					}
					
					/**
					 * 
					 * Updated By Anil Date: 24-11-2016
					 */
//					if(!validateState()&&this.getCbcformlis().getSelectedIndex()==0){
						double assessVal=0;
						if(salesLineItemLis.get(i).getVat()!=0){
							
							 /** Date 11 july 2017 if condition added by vijay for GST and old code added in else block 
							 */
							if(salesLineItemLis.get(i).getServiceTax().getTaxPrintName()!=null && !salesLineItemLis.get(i).getServiceTax().getTaxPrintName().equals("")){
								assessVal=priceqty;
								pocentity.setAssessableAmount(assessVal);
							}else{
							assessVal=priceqty+(priceqty*salesLineItemLis.get(i).getVat()/100);
							pocentity.setAssessableAmount(assessVal);
							
							}
						}
						if(salesLineItemLis.get(i).getVat()==0){
							pocentity.setAssessableAmount(priceqty);
						}
//					}
					/**
					 * END
					 */
				}
				this.producttaxtable.getDataprovider().getList().add(pocentity);
			}
			
			
		
//		}
			
		}
	}
	
	
	
	public void addProdTaxesNoState() throws Exception
	{
		List<ProductDetailsPO> salesLineItemLis=this.productTablePO.getDataprovider().getList();
		List<ProductOtherCharges> taxList=this.producttaxtable.getDataprovider().getList();
		NumberFormat nf=NumberFormat.getFormat("0.00");
		for(int i=0;i<salesLineItemLis.size();i++)
		{
		
			double priceqty=0,taxPrice=0;
			
			if((salesLineItemLis.get(i).getDiscount()==null || salesLineItemLis.get(i).getDiscount()==0) && (salesLineItemLis.get(i).getDiscountAmt()==0) ){
				
				System.out.println("inside both 0 condition");
				
				taxPrice=this.getProductTablePO().removeAllTaxes(salesLineItemLis.get(i).getPrduct());
				priceqty=(salesLineItemLis.get(i).getProdPrice()-taxPrice)*salesLineItemLis.get(i).getProductQuantity();
				priceqty=Double.parseDouble(nf.format(priceqty));
			}
			
			else if(((salesLineItemLis.get(i).getDiscount()!=null)&& (salesLineItemLis.get(i).getDiscountAmt()!=0))){
				
				System.out.println("inside both not null condition");
				
				taxPrice=this.getProductTablePO().removeAllTaxes(salesLineItemLis.get(i).getPrduct());
				priceqty=(salesLineItemLis.get(i).getProdPrice()-taxPrice);
				priceqty=priceqty-(priceqty*salesLineItemLis.get(i).getDiscount()/100);
				
				priceqty=priceqty-salesLineItemLis.get(i).getDiscountAmt();
				
				priceqty=priceqty*salesLineItemLis.get(i).getProductQuantity();
				priceqty=Double.parseDouble(nf.format(priceqty));
			}
			else 
			{
				System.out.println("inside oneof the null condition");
				
				
				taxPrice=this.getProductTablePO().removeAllTaxes(salesLineItemLis.get(i).getPrduct());
				priceqty=(salesLineItemLis.get(i).getProdPrice()-taxPrice);
				
				if(salesLineItemLis.get(i).getDiscount()!=null){
				priceqty=priceqty-(priceqty*salesLineItemLis.get(i).getDiscount()/100);
				}
				else 
				{
				priceqty=priceqty-salesLineItemLis.get(i).getDiscountAmt();
				}
				priceqty=priceqty*salesLineItemLis.get(i).getProductQuantity();
				priceqty=Double.parseDouble(nf.format(priceqty));
			}
			
			System.out.println("Total Prodcut"+priceqty);
			
			
			if(salesLineItemLis.get(i).getVat()==0){
				ProductOtherCharges pocentity=new ProductOtherCharges();
				pocentity.setChargeName("VAT");
				pocentity.setIndexCheck(i+1);
				int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getVat(),"VAT");
				if(indexValue!=-1){
//					pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
					pocentity.setAssessableAmount(0);
					this.getProducttaxtable().getDataprovider().getList().remove(indexValue);
				}
				
				if(indexValue==-1){
					pocentity.setAssessableAmount(0);
//					pocentity.setAssessableAmount(priceqty);
				}
				this.producttaxtable.getDataprovider().getList().add(pocentity);
			}
			
			if(salesLineItemLis.get(i).getTax()==0){
//				if(validateState()){
				ProductOtherCharges pocentity=new ProductOtherCharges();
				pocentity.setChargeName("Service Tax");
				pocentity.setChargePercent(salesLineItemLis.get(i).getTax());
				pocentity.setIndexCheck(i+1);
				int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getTax(),"Service");
				if(indexValue!=-1){
//					pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
					pocentity.setAssessableAmount(0);
					this.getProducttaxtable().getDataprovider().getList().remove(indexValue);
				}
				
				if(indexValue==-1){
					pocentity.setAssessableAmount(0);
//					pocentity.setAssessableAmount(priceqty);
				}
				this.producttaxtable.getDataprovider().getList().add(pocentity);
			}
			
		/**
		 * Adding Vat Tax to table if the company state and delivery state is equal
		 */
			
			if(salesLineItemLis.get(i).getVat()!=0){
				System.out.println("Vattx");
				ProductOtherCharges pocentity=new ProductOtherCharges();
				pocentity.setChargeName("VAT");
				pocentity.setChargePercent(salesLineItemLis.get(i).getVat());
				pocentity.setIndexCheck(i+1);
				int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getVat(),"VAT");
				
				if(indexValue!=-1){
					System.out.println("Index As Not -1");
					pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
					this.getProducttaxtable().getDataprovider().getList().remove(indexValue);
				}
				if(indexValue==-1){
					System.out.println("VAt");
					pocentity.setAssessableAmount(priceqty);
				}
				this.producttaxtable.getDataprovider().getList().add(pocentity);
			}
			
			/**
			 * Adding service tax to the table
			 */
			
			if(salesLineItemLis.get(i).getTax()!=0){
				System.out.println("ServiceTx");
				ProductOtherCharges pocentity=new ProductOtherCharges();
				pocentity.setChargeName("Service Tax");
				pocentity.setChargePercent(salesLineItemLis.get(i).getTax());
				pocentity.setIndexCheck(i+1);
				int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getTax(),"Service");
				if(indexValue!=-1){
					System.out.println("Index As Not -1");
					
					/**
					 * If validate state method is true
					 */
					
						double assessValue=0;
						if(salesLineItemLis.get(i).getVat()!=0){
							assessValue=priceqty+(priceqty*salesLineItemLis.get(i).getVat()/100);
							pocentity.setAssessableAmount(assessValue+taxList.get(indexValue).getAssessableAmount());
							this.getProducttaxtable().getDataprovider().getList().remove(indexValue);
						}
						if(salesLineItemLis.get(i).getVat()==0){
							pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
							this.getProducttaxtable().getDataprovider().getList().remove(indexValue);
						}
					
					/**
					 * If validate state method is true and cform is submittted.
					 */
					
					
					/**
					 * If validate state method is true and cform is not submitted.
					 */
					
				}
				
				if(indexValue==-1){
					System.out.println("Index As -1");
					
						double assessVal=0;
						if(salesLineItemLis.get(i).getVat()!=0){
							assessVal=priceqty+(priceqty*salesLineItemLis.get(i).getVat()/100);
							pocentity.setAssessableAmount(assessVal);
						}
						if(salesLineItemLis.get(i).getVat()==0){
							pocentity.setAssessableAmount(priceqty);
						}
					
				}
				this.producttaxtable.getDataprovider().getList().add(pocentity);
			}
		}
	}
	
	
	
	
	
	
	/******************************************* Check Tax Percent ****************************************************/
	/**
	 * This method returns 
	 * @param taxValue
	 * @param taxName
	 * @return
	 */
	
	public int checkTaxPercent(double taxValue,String taxName)
	{
		System.out.println();
		System.out.println("CHECK TAX PERCENT");
		System.out.println(taxName+" TAX Value "+taxValue);
		
		List<ProductOtherCharges> taxesList=this.producttaxtable.getDataprovider().getList();
		System.out.println("TAX LIST SIZE : "+taxesList.size());
		
		
		for(int i=0;i<taxesList.size();i++)
		{
			double listval=taxesList.get(i).getChargePercent();
			int taxval=(int)listval;
			
			if(taxName.equals("Service")){
				if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("Service Tax")){
					System.out.println("ST NZ "+i);
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("Service Tax")){
					System.out.println("ST zero "+i);
					return i;
				}
			}
			if(taxName.equals("VAT")){
				if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("VAT")){
					System.out.println("V NZ "+i);
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("VAT")){
					System.out.println("V zero "+i);
					return i;
				}
			}
			if(taxName.equals("CST")){
				System.out.println("CST");
				if(taxValue==taxesList.get(i).getChargePercent()&&taxesList.get(i).getChargeName().trim().equals("CST")){
					System.out.println("CST "+i);
					return i;
				}
			}
			
			/**
			 * Date 26-11-2018 By Vijay
			 * Des :- for GST Taxes map properly
			 */
			
			if(taxName.equals("CGST")){
				if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("CGST")){
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("CGST")){
					return i;
				}
			}
			
			if(taxName.equals("SGST")){
				if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("SGST")){
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("SGST")){
					return i;
				}
			}
			
			if(taxName.equals("IGST")){
				if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("IGST")){
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("IGST")){
					return i;
				}
			}
			/**
			 * ends here
			 */
		}
		return -1;
	}
	
	
	/**
	 * This method sets tax label from vat to cst in case cform is not applicable or selected as NO
	 * Date : 24-11-2016 By Anil
	 * Project : DEADSTOCK (NBHC)
	 */
	
	public int checkCstTaxPercentForNoCform(double taxValue,String taxName)
	{
		System.out.println();
		System.out.println("CHECK TAX PERCENT");
		System.out.println(taxName+" TAX Value "+taxValue);
		
		List<ProductOtherCharges> taxesList=this.producttaxtable.getDataprovider().getList();
		System.out.println("TAX LIST SIZE : "+taxesList.size());
		

		for(int i=0;i<taxesList.size();i++)
		{
			double listval=taxesList.get(i).getChargePercent();
			int taxval=(int)listval;
			
//			if(taxName.equals("CST")){
				if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("CST")){
					System.out.println("C NZ "+i);
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("CST")){
					System.out.println("c zero "+i);
					return i;
				}
//			}
		}
		
		for(int i=0;i<taxesList.size();i++)
		{
			double listval=taxesList.get(i).getChargePercent();
			int taxval=(int)listval;
			
			if(taxName.equals("VAT")){
				if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("VAT")){
					System.out.println("V NZ "+i);
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("VAT")){
					System.out.println("V zero "+i);
					return i;
				}
			}
		}
		
		
		return -1;
	}
	
	/**
	 * End
	 */
	
	
	
	
	/***************************************** Update Charges Table *******************************************/
	/**
	 * This method updates the charges table.
	 * The table is updated when any row value is changed through field updater in table i.e
	 * row change event is fired
	 */
	
	public void updateChargesTable()
	{
		List<ProductOtherCharges> prodOtrLis=this.productchargestable.getDataprovider().getList();
		ArrayList<ProductOtherCharges> prodOtrArr=new ArrayList<ProductOtherCharges>();
		prodOtrArr.addAll(prodOtrLis);
		
		this.productchargestable.connectToLocal();
		System.out.println("prodo"+prodOtrArr.size());
		for(int i=0;i<prodOtrArr.size();i++)
		{
			ProductOtherCharges prodChargesEnt=new ProductOtherCharges();
			prodChargesEnt.setChargeName(prodOtrArr.get(i).getChargeName());
			prodChargesEnt.setChargePercent(prodOtrArr.get(i).getChargePercent());
			prodChargesEnt.setChargeAbsValue(prodOtrArr.get(i).getChargeAbsValue());
			//prodChargesEnt.setAssessableAmount(prodOtrArr.get(i).getAssessableAmount());
			prodChargesEnt.setFlagVal(prodOtrArr.get(i).getFlagVal());
			prodChargesEnt.setIndexCheck(prodOtrArr.get(i).getIndexCheck());
			if(prodOtrArr.get(i).getFlagVal()==false){
				prodChargesEnt.setAssessableAmount(this.getDoamtincltax().getValue());
			}
			if(prodOtrArr.get(i).getFlagVal()==true){
				double assesableVal=this.retrieveSurchargeAssessable(prodOtrArr.get(i).getIndexCheck());
				prodChargesEnt.setAssessableAmount(assesableVal);
			}
			
			this.productchargestable.getDataprovider().getList().add(prodChargesEnt);
		}
	}
	
	public double retrieveSurchargeAssessable(int indexValue)
	{
		List<ProductOtherCharges> otrChrgLis=this.productchargestable.getDataprovider().getList();
		ArrayList<ProductOtherCharges> otrChrgArr=new ArrayList<ProductOtherCharges>();
		otrChrgArr.addAll(otrChrgLis);
	
		double getAssessVal=0;
		for(int i=0;i<otrChrgArr.size();i++)
		{
			if(otrChrgArr.get(i).getFlagVal()==false&&otrChrgArr.get(i).getIndexCheck()==indexValue)
			{
				if(otrChrgArr.get(i).getChargePercent()!=0){
					getAssessVal=otrChrgArr.get(i).getAssessableAmount()*otrChrgArr.get(i).getChargePercent()/100;
				}
				if(otrChrgArr.get(i).getChargeAbsValue()!=0){
					getAssessVal=otrChrgArr.get(i).getChargeAbsValue();
				}
			}
		}
		return getAssessVal;
	}

	
	/************************************* Set All Table *****************************************************/
	public void setAllTable() {
		if (productTablePO.getDataprovider().getList().size() != 0) {
			double total = productTablePO.calculateTotal();
			dbTotal.setValue(total);
			// setTaxTable();
		}
		if (producttaxtable.getDataprovider().getList().size() != 0) {
//			 setGrandTotal();
		}
		if (productchargestable.getDataprovider().getList().size() != 0) {
			System.out.println("change product charge table");
			List<ProductOtherCharges> list = productchargestable.getDataprovider().getList();
			for (ProductOtherCharges temp : list) {
				temp.setAssessableAmount(doamtincltax.getValue());
			}
			productchargestable.getTable().redraw();
		}

	}

	/**************************************** Get Company Address **************************************************/
	
	/**
	 * This method is used to set company address to address composite and called in the form constructor.
	 * @param count
	 */

	public void getCompanyAddress(Long count) {
		Filter temp = new Filter();
		temp.setQuerryString("companyId");
		temp.setLongValue(count);
		final MyQuerry querry = new MyQuerry();
		querry.getFilters().add(temp);
		querry.setQuerryObject(new Company());

		 Timer timer=new Timer() 
    	 {
			@Override
			public void run() 
			{
				async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onFailure(Throwable caught) {
						showDialogMessage("An Unexpected Error occured !");
					}
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						for (SuperModel smodel : result) {
							Company comentity = (Company) smodel;
							deliveryadd.setValue(comentity.getAddress());
							deliveryadd.setEnable(false);
						}
					}
				});
			}
    	 };
    	 timer.schedule(3000); 
	}
	// *********************************** Checking Product************************************************ 
	public boolean checkProducts(String pcode) {
		List<ProductDetailsPO> salesitemlis = productTablePO.getDataprovider().getList();
		for (int i = 0; i < salesitemlis.size(); i++) {
			if (salesitemlis.get(i).getProductCode().equals(pcode)|| salesitemlis.get(i).getProductCode() == pcode) {
				return true;
			}
		}
		return false;
	}

	// ****************************** Method For Adding product Details With Price List In Table *********************
	
	public void ProdctType(String pcode,String productId) {
		System.out.println("Started");
	
		final GenricServiceAsync genasync = GWT.create(GenricService.class);
		final int prodId=Integer.parseInt(productId);
		
		final MyQuerry querry = new MyQuerry();
		Company c=new Company();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		filter = new Filter();
		filter.setQuerryString("productCode");
		filter.setStringValue(pcode.trim());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(prodId);
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);
		
		filter=new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new SuperProduct());
		showWaitSymbol();
		Timer timer = new Timer() {
			@Override
			public void run() {
				genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {

					@Override
					public void onFailure(Throwable caught) {
						hideWaitSymbol();
						showDialogMessage("An Unexpected error occurred!");
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						
						if(result.size()==0)
						{
							showDialogMessage("Please check whether product status is active or not!");
						}
							
						for (SuperModel model : result) {
							SuperProduct superProdEntity = (SuperProduct) model;
							final ProductDetailsPO lis = AppUtility.ReactOnAddPurchaseProductComposite(superProdEntity);
							if(dbExpdeliverydate.getValue()==null){
								lis.setProdDate(new Date());

							}else{
								lis.setProdDate(dbExpdeliverydate.getValue());

							}
							productTablePO.getDataprovider().getList().add(lis);

//							checkAndAddVendorToVendorTable(lis);
						}
						System.out.println("Out Of Success");
					}
				});
		hideWaitSymbol();
	}
	};
	timer.schedule(1000);
	}
	
	protected boolean checkForCommonVendor(List<PriceListDetails> vendorList,int vendorId)
	{
		boolean vendorCheckFlag=false;
		int ctr=0;
		for(int h=0;h<vendorList.size();h++)
		{
			if(vendorList.get(h).getVendorID()==vendorId){
				ctr=ctr+1;
			}
		}
		
		if(ctr>0){
			vendorCheckFlag=true;
		}
		
		return vendorCheckFlag;
	}
	
	
	
	/*******************************************Type Drop Down Logic**************************************/
	
	@Override
	public void onChange(ChangeEvent event) {
		if(event.getSource().equals(oblPurchaseOrderCategory))
		{
			if(oblPurchaseOrderCategory.getSelectedIndex()!=0){
				ConfigCategory cat=oblPurchaseOrderCategory.getSelectedItem();
				if(cat!=null){
					AppUtility.makeLiveTypeDropDown(oblPurchaseOrderType, cat.getCategoryName(), cat.getCategoryCode(), cat.getInternalType());
				}
			}
		}
		
		if(event.getSource().equals(branch)){
			if(branch.getSelectedIndex()!= 0 && productTablePO.getDataprovider().getList().size()>0){
				List<ProductDetailsPO> list = productTablePO.getDataprovider().getList();
				Branch branchEntity = branch.getSelectedItem();
				QuickPurchaseOrderPreseneter.updateTaxesDetails(list,branchEntity,QuickPurchaseOrderPreseneter.vendorState);
//				List<ProductDetailsPO> updatedlist = PurchaseOrderPresenter.updateTaxesDetails(list,branchEntity,QuickPurchaseOrderPreseneter.vendorState,true);
//				productTablePO.getDataprovider().setList(updatedlist);
				RowCountChangeEvent.fire(productTablePO.getTable(), productTablePO.getDataprovider().getList().size(), true);
				setEnable(true);
			}
		}
		
	}
	
	
	
	/***************************** NEW VENDOR SELECTION LOGIC BY ANIL **********************************/
	

	/**
	 * This method takes product details which we are adding in product table as input and 
	 * checks whether vendor price list for that product is exist or not
	 * if exist then checks for common vendor and add product and vendor to their respective table.
	 * 
	 * Date : 10-09-2016 By Anil
	 * Release :31 Aug 2016
	 * Project:PURCHASE ERROR SOLVING (NBHC)
	 * I/P:Product of type ProductDetails
	 */
	
	
//	public void checkAndAddVendorToVendorTable(final ProductDetailsPO prodDetails){
//		
//		System.out.println("One");
//		
//		MyQuerry querry=new MyQuerry();
//		Filter filter = new Filter();
//		filter.setQuerryString("prodCode");
//		filter.setStringValue(prodInfoComposite.getProdCode().getValue().trim());
//		querry.getFilters().add(filter);
//		querry.setQuerryObject(new PriceList());
//		System.out.println("Two");
//		
//		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
//			@Override
//			public void onFailure(Throwable caught) {
//				
//			}
//			@Override
//			public void onSuccess(ArrayList<SuperModel> vresult) {
//				System.out.println("VEN PRICE LIS SIZE : "+vresult.size());
//				if (vresult.size() != 0) {
//					for(SuperModel smodel:vresult){
//						System.out.println("Three");
//						PriceList pentity=(PriceList) smodel;
//						if(isCommonVendor(pentity.getPriceInfo())){
//							System.out.println("Four");
//							
//							vendorRateList.addAll(pentity.getPriceInfo());
//							
//							if(validateVendorselect()){
//								int vendCount=retrieveVendorCount();
//								for(PriceListDetails price:vendorRateList){
//									if(price.getVendorID()==vendCount&&price.getProdID()==prodDetails.getProductID()){
//										prodDetails.setProdPrice(price.getProductPrice());
//									}
//								}
//							}
//							
//							productTablePO.getDataprovider().getList().add(prodDetails);
//							
//						}else{
//							System.out.println("Five");
//							showDialogMessage("No common vendor for this product !");
//							return;
//						}
//						addUniqueVendorsToTable(pentity.getPriceInfo());
//					}
//				}else{
//					System.out.println("Six");
//					showDialogMessage("Vendor Price List does not exists for product");
//				}
//			}
//		});		
//	}
	
	/**
	 * This method checks whether vendors are common or not for added product if not then it will not allow you add product
	 * Date 10-09-2016 By Anil
	 * Release : 31 Aug 2016
	 * Project: PURCHASE ERROR SOLVING(NBHC)
	 * I/P:Current added products vendor price list and checks with  already added vendor list
	 * O/P: return true if at least one vendor common else false
	 */

	private boolean isCommonVendor(List<PriceListDetails>venRateLis) {
		System.out.println("CHECKING COMMON VENDOR ");
		System.out.println("VEN TBL SIZE "+vendorTablePO.getDataprovider().getList().size());
		if(vendorTablePO.getDataprovider().getList().size()==0){
			return true;
		}
		for(VendorDetails gblVenLis:vendorTablePO.getDataprovider().getList()){
			for(PriceListDetails currVenLis:venRateLis){
				System.out.println("VENDOR ID "+gblVenLis.getVendorId() +"    --   "+currVenLis.getVendorID());
				if(gblVenLis.getVendorId()==currVenLis.getVendorID()){
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * This method adds only common vendor to vendor table 
	 * Date 10-09-2016 By Anil
	 * Release : 31 Aug 2016
	 * Project: PURCHASE ERROR SOLVING(NBHC)
	 * I/P:Current added products vendor list and checks with already added vendor list,maintain only common vendors list else all removed 
	 */
	
	private void addUniqueVendorsToTable(List<PriceListDetails>venRateLis) {
		System.out.println("ADDING UNIQUE VENDOR");
		if(vendorTablePO.getDataprovider().getList().size()==0){
			System.out.println("ADDING FOR 1ST TIME");
			ArrayList<VendorDetails> venList=getVendorList(venRateLis);	
			vendorTablePO.getDataprovider().getList().addAll(venList);
		}else{
			System.out.println("ADDING ");
			ArrayList<VendorDetails> commonVenList=getCommonVendorList(vendorTablePO.getDataprovider().getList(),venRateLis);
			vendorTablePO.getDataprovider().getList().clear();
			vendorTablePO.getDataprovider().getList().addAll(commonVenList);
		}
		
		System.out.println("FINAL VENDOR LIST SIZE "+vendorTablePO.getDataprovider().getList().size());
		
		
	}
	
	/**
	 * This method returns common vendors list by comparing from existing and currently to be added vendor list.
	 * Date 10-09-2016 By Anil
	 * Release : 31 Aug 2016
	 * Project: PURCHASE ERROR SOLVING(NBHC)
	 * I/P:existing vendor list and currently added vendor price list
	 * O/P: common vendor list
	 */
	
	private ArrayList<VendorDetails> getCommonVendorList(List<VendorDetails> vendorList,List<PriceListDetails> venRateLis) {
		System.out.println("CHECKING ADDING COMMON VENDOR ONLY ");
		ArrayList<VendorDetails> vendList=new ArrayList<VendorDetails>();
		for(VendorDetails venDet:vendorList){
			for(PriceListDetails venRLis:venRateLis){
				if(venDet.getVendorId()==venRLis.getVendorID()){
					vendList.add(venDet);
				}
			}
		}
		System.out.println("VEN LIS SIZE "+vendList.size());
		return vendList;
	}
	
	
	
	/**
	 * This method convert the vendor list from  type PriceListDetails to type VendorDetails
	 * Date 10-09-2016 By Anil
	 * Release : 31 Aug 2016
	 * Project: PURCHASE ERROR SOLVING(NBHC)
	 * I/P:vendor list of type PriceListDetails
	 * O/P: vendor list of type VendorDetails 
	 */
	
	public ArrayList<VendorDetails> getVendorList(List<PriceListDetails> venRateLis) {
		System.out.println("CONVERTING LIST.....");
		ArrayList<VendorDetails> vendorList=new ArrayList<VendorDetails>();
		
		for (PriceListDetails temp : venRateLis) {
			VendorDetails ve = new VendorDetails();
			ve.setVendorEmailId(temp.getEmail());
			ve.setVendorId(temp.getVendorID());
			ve.setVendorName(temp.getFullName());
			ve.setVendorPhone(temp.getCellNumber());
			
			/**
			 * Date : 06-02-2017 By Anil
			 * Added POC Name 
			 */
			ve.setPocName(temp.getPocName());
			/**
			 * End
			 */
			vendorList.add(ve);
		}
		return vendorList;
	}
	
	
	
	

	/**
	 * This method load global vendor list when we are viewing the records by searching.
	 * Date: 10-09-2016
	 * Release : 31 Aug 2016
	 * Project: PURCHASE ERROR SOLVING(NBHC)
	 * I/P: List of product code
	 * 
	 */
//	public void loadVendorProductPriceList(List<String> prodIdList) {
//		
//		
//		for(String obj:prodIdList){
//			System.out.println("CODE : "+obj);
//		}
//		MyQuerry querry=new MyQuerry();
//		Filter filter=null;
//		Vector<Filter>temp=new Vector<Filter>();
//		
//		filter=new Filter();
//		filter.setList(prodIdList);
//		filter.setQuerryString("prodCode IN");
//		temp.add(filter);
//		
//		filter=new Filter();
//		filter.setLongValue(UserConfiguration.getCompanyId());
//		filter.setQuerryString("companyId");
//		temp.add(filter);
//		
//		
//		
//		querry.setFilters(temp);
//		querry.setQuerryObject(new PriceList());
//		
//		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
//
//			@Override
//			public void onFailure(Throwable caught) {
//				// TODO Auto-generated method stub
//				
//			}
//
//			@Override
//			public void onSuccess(ArrayList<SuperModel> result) {
//				System.out.println("LOAD RESULT "+result.size());
//				vendorRateList=new ArrayList<PriceListDetails>();
//				for(SuperModel model:result){
//					PriceList entity=(PriceList) model;
//					vendorRateList.addAll(entity.getPriceInfo());
//				}
//				System.out.println("GLOBAL VEN LIST SIZE IN LOAD "+vendorRateList.size());
//			}
//		});
//		
//		
//		
//	}
	
	
	
	
	
	/**
	 * This method is called when you are adding vendor 
	 * this method checks whether price list of vendor for added product is defined or not.
	 * Date : 29-09-2016 By Anil
	 * Release : 30 Sept 2016
	 * Project : PURCHASE MODIFICATION (NBHC)
	 */
//	public void checkPriceListAndAddVendorToVendorTable() {
//		System.out.println("INSIDE VENDOR ADD CODE ");
//		List<ProductDetailsPO> list=productTablePO.getDataprovider().getList();
//		HashSet<String> hashList=new HashSet<String>();
//		final ArrayList<String> codeList=new ArrayList<String>();
//		for(ProductDetailsPO obj:list){
//			hashList.add(obj.getProductCode());
//		}
//		codeList.addAll(hashList);
//		
//		System.out.println("CODE LIST SIZE "+codeList.size());
//		Vector<Filter>temp=new Vector<Filter>();
//		MyQuerry querry=new MyQuerry();
//		Filter filter =null;
//		
//		filter = new Filter();
//		filter.setQuerryString("prodCode IN");
//		filter.setList(codeList);
//		temp.add(filter);
//		
//		filter = new Filter();
//		filter.setQuerryString("priceInfo.personinfo.count");
//		filter.setIntValue(vendorComp.getIdValue());
//		temp.add(filter);
//		
//		
//		querry.setFilters(temp);
//		querry.setQuerryObject(new PriceList());
//		showWaitSymbol();
//		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
//			@Override
//			public void onFailure(Throwable caught) {
//				hideWaitSymbol();
//			}
//			@Override
//			public void onSuccess(ArrayList<SuperModel> vresult) {
//				hideWaitSymbol();
//				System.out.println("VEN PRICE LIS SIZE : "+vresult.size());
//				if (vresult.size() != 0) {
//					ArrayList<PriceList> priceList=new ArrayList<PriceList>();
//					for(SuperModel smodel:vresult){
//						PriceList pentity=(PriceList) smodel;
//						priceList.add(pentity);
//					}	
//					
//					if(priceList.size()==codeList.size()){
//						VendorDetails ve = new VendorDetails();
//						
//						for(PriceList entity:priceList){
//							vendorRateList.addAll(entity.getPriceInfo());
//							
//							for(PriceListDetails obj:entity.getPriceInfo()){
//								if(obj.getVendorID()==vendorComp.getIdValue()){
//									ve.setVendorEmailId(obj.getEmail());
//									ve.setVendorId(obj.getVendorID());
//									ve.setVendorName(obj.getFullName());
//									ve.setVendorPhone(obj.getCellNumber());
//									
//									/**
//									 * Date : 06-02-2017 By ANIL
//									 * Added vendor POC Name
//									 */
//									ve.setPocName(obj.getPocName());
//									/**
//									 * End
//									 */
//									
//									/**
//									 * Updated on 24-11-2016 By Anil
//									 */
//									if(vendorTablePO.getDataprovider().getList().size()==0){
//										ve.setStatus(true);
//									}
//									/**
//									 * End
//									 */
//								}
//							}
//						}
//						/**
//						 * Updated on 24-11-2016 By Anil
//						 */
//						if(ve.getStatus()==true){
//							PurchaseOrderPresenter.retVenStateFlage=true;
//						}
//						/**
//						 * End
//						 */
//						
//						vendorTablePO.getDataprovider().getList().add(ve);
//						vendorComp.clear();
//						
//						
//					}else{
//						ArrayList<String> list=new ArrayList<String>();
//						for(String obj:codeList){
//							boolean flag=false;
//							for(PriceList lisObj:priceList){
//								if(lisObj.getProdCode().equals(obj)){
//									flag=true;
//									break;
//								}
//							}
//							if(!flag){
//								list.add(obj);
//							}
//						}
//						
//						String prodName="";
//						for(String obj:list){
//							prodName=prodName+getProductNameFromCode(obj)+"\n";
//						}
//						
//						showDialogMessage("Vendor price list does not exist for following products."+"\n"+prodName);
//						return;
//						
//					}
//				}else{
//					System.out.println("Six");
//					showDialogMessage("Vendor Price List does not exists.");
//				}
//			}
//		});				
//	}
	
	
	private String getProductNameFromCode(String code){
		List<ProductDetailsPO> list=productTablePO.getDataprovider().getList();
		for(ProductDetailsPO obj:list){
			if(obj.getProductCode().equals(code)){
				return obj.getProductName();
			}
		}
		return null;
	}

	/**
	 * This method checks whether vendor is already added or not
	 * if already added then returns false else true
	 * Date : 28-09-2016 By Anil
	 * Release : 30 Sept 2016
	 * Project : PURCHASE MODIFICATION(NBHC)
	 */

	public boolean validVendor() {
		
		List<ProductDetailsPO> prodList=productTablePO.getDataprovider().getList();
		if(prodList.size()==0){
			showDialogMessage("Please add product first !");
			return false;
		}
		
		List<VendorDetails> list=vendorTablePO.getDataprovider().getList();
		int vendorId=vendorComp.getIdValue();
		
		for(VendorDetails obj:list){
			if(obj.getVendorId()==vendorId){
				showDialogMessage("Vendor already added !");
				return false;
			}
		}
		return true;
	}
	
	/**
	 * End
	 */
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	/******************************** Getter and Setter ************************************************/
	public CheckBox getCbadds() {
		return cbadds;
	}

	public void setCbadds(CheckBox cbadds) {
		this.cbadds = cbadds;
	}

	public AddressComposite getDeliveryadd() {
		return deliveryadd;
	}

	public void setDeliveryadd(AddressComposite deliveryadd) {
		this.deliveryadd = deliveryadd;
	}


	public TextBox getIbPOId() {
		return ibPOId;
	}


	public void setIbPOId(TextBox ibPOId) {
		this.ibPOId = ibPOId;
	}


//	public TextBox getTbName() {
//		return tbName;
//	}
//
//	public void setTbName(TextBox tbName) {
//		this.tbName = tbName;
//	}

	
	

	public VendorTablePO getVendortbl() {
		return vendorTablePO;
	}

	public QuickPurchaseProductTable getProductTablePO() {
		return productTablePO;
	}

	public void setProductTablePO(QuickPurchaseProductTable productTablePO) {
		this.productTablePO = productTablePO;
	}

	public void setVendortbl(VendorTablePO vendorTablePO) {
		this.vendorTablePO = vendorTablePO;
	}


	public DateBox getDbPODate() {
		return dbPODate;
	}

	public void setDbPODate(DateBox dbPODate) {
		this.dbPODate = dbPODate;
	}

	public TextBox getIbRefOrderNo() {
		return ibRefOrderNo;
	}

	public void setIbRefOrderNo(TextBox ibRefOrderNo) {
		this.ibRefOrderNo = ibRefOrderNo;
	}

	public ObjectListBox<Employee> getOblEmployee() {
		return oblEmployee;
	}

	public void setOblEmployee(ObjectListBox<Employee> oblEmployee) {
		this.oblEmployee = oblEmployee;
	}


	public ObjectListBox<Type> getOblPurchaseOrderType() {
		return oblPurchaseOrderType;
	}

	public void setOblPurchaseOrderType(ObjectListBox<Type> oblPurchaseOrderType) {
		this.oblPurchaseOrderType = oblPurchaseOrderType;
	}

	public TextBox getTbStatus() {
		return tbStatus;
	}

	public void setTbStatus(TextBox tbStatus) {
		this.tbStatus = tbStatus;
	}

	public TextBox getTbpoName() {
		return tbpoName;
	}

	public void setTbpoName(TextBox tbpoName) {
		this.tbpoName = tbpoName;
	}


	public ObjectListBox<Branch> getBranch() {
		return branch;
	}

	public void setBranch(ObjectListBox<Branch> branch) {
		this.branch = branch;
	}

	public ObjectListBox<Employee> getTbApporverName() {
		return tbApporverName;
	}

	public void setTbApporverName(ObjectListBox<Employee> tbApporverName) {
		this.tbApporverName = tbApporverName;
	}

	public DateBox getDbExpdeliverydate() {
		return dbExpdeliverydate;
	}

	public void setDbExpdeliverydate(DateBox dbExpdeliverydate) {
		this.dbExpdeliverydate = dbExpdeliverydate;
	}

	public Label getLbtotal() {
		return lbtotal;
	}

	public void setLbtotal(Label lbtotal) {
		this.lbtotal = lbtotal;
	}

	public DoubleBox getDbTotal() {
		return dbTotal;
	}

	public void setDbTotal(DoubleBox dbTotal) {
		this.dbTotal = dbTotal;
	}

	public ProductTaxesTable getProducttaxtable() {
		return producttaxtable;
	}

	public void setProducttaxtable(ProductTaxesTable producttaxtable) {
		this.producttaxtable = producttaxtable;
	}

	public ProductChargesTable getProductchargestable() {
		return productchargestable;
	}

	public void setProductchargestable(ProductChargesTable productchargestable) {
		this.productchargestable = productchargestable;
	}

	public TextBox getTbblanck() {
		return tbblanck;
	}

	public void setTbblanck(TextBox tbblanck) {
		this.tbblanck = tbblanck;
	}

	public ProductInfoComposite getProdInfoComposite() {
		return prodInfoComposite;
	}

	public void setProdInfoComposite(ProductInfoComposite prodInfoComposite) {
		this.prodInfoComposite = prodInfoComposite;
	}


	public DoubleBox getDototalamt() {
		return dototalamt;
	}

	public void setDototalamt(DoubleBox dototalamt) {
		this.dototalamt = dototalamt;
	}

	public DoubleBox getDoamtincltax() {
		return doamtincltax;
	}

	public void setDoamtincltax(DoubleBox doamtincltax) {
		this.doamtincltax = doamtincltax;
	}

	public DoubleBox getDonetpayamt() {
		return donetpayamt;
	}

	public void setDonetpayamt(DoubleBox donetpayamt) {
		this.donetpayamt = donetpayamt;
	}


	public DateBox getDbrefDate() {
		return dbrefDate;
	}


	public void setDbrefDate(DateBox dbrefDate) {
		this.dbrefDate = dbrefDate;
	}
	
	
	//   rohan 
	
	public int retrieveVendorCount(){
		List<VendorDetails> vendorLis=this.getVendortbl().getDataprovider().getList();
		int vendorCount=0;
		for(int i=0;i<vendorLis.size();i++)
		{
			if(vendorLis.get(i).getStatus()==true){
				vendorCount=vendorLis.get(i).getVendorId();
			}
		}
		return vendorCount;
	}
	
	
	
	private void retVendorState(int vendorCount) {
		
		final MyQuerry querry=new MyQuerry();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter tempfilter=null;
		
		tempfilter=new Filter();
		tempfilter.setQuerryString("companyId");
		tempfilter.setLongValue(UserConfiguration.getCompanyId());
		filtervec.add(tempfilter);
		
		tempfilter=new Filter();
		tempfilter.setQuerryString("count");
		tempfilter.setIntValue(vendorCount);
		filtervec.add(tempfilter);
		
		querry.getFilters().add(tempfilter);
		querry.setQuerryObject(new Vendor());
		Timer timer=new Timer() 
	    {
			@Override
			public void run() 
			{
				async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				@Override
				public void onFailure(Throwable caught) {
				}
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					for(SuperModel model:result)
					{
						Vendor vendorentity = (Vendor)model;
						String	retVenStateName=vendorentity.getPrimaryAddress().getState();
						PurchaseOrderPresenter.vendorState=retVenStateName.trim();
					}
				}
			 });
			}
    	 };
    	 timer.schedule(3000);
	}
	
	
	
	/**
	 * This methods returns true if warehouse is selected at product level.
	 * Date : 03-10-2016 By Anil
	 * Release : 30 Sept 2016 
	 * Project : PURCHASE MODIFICATION(NBHC)
	 */
	public boolean isWarehouseSelected(){
		List<ProductDetailsPO> list=productTablePO.getDataprovider().getList();
		for(ProductDetailsPO obj:list){
			if(!obj.getWarehouseName().equals("")){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * This method checks whether warehouse for all product is selected or not
	 * It returns true if all warehouses are selected or none of them are selected else return false 
	 * Date : 03-10-2016 By Anil
	 * Release : 30 Sept 2016 
	 * Project: PURCHASE MODIFICATION (NBHC)  
	 */
	public boolean isAllWarehouseIsSelectedOrDeselected(){
		int selCounter=0;
		int dselCounter=0;
		List<ProductDetailsPO> list=productTablePO.getDataprovider().getList();
		for(ProductDetailsPO obj:list){
			if(!obj.getWarehouseName().equals("")){
				selCounter++;
			}else{
				dselCounter++;
			}
		}
		
		if(selCounter==list.size()){
			int sameState=0;
			int diffState=0;
			String sameWhDet="";
			String diffWhDet="";
			
			
			for(ProductDetailsPO obj:list){
				String warehouseState=ProductTablePO.getWarehouseState(obj.getWarehouseName());
				if(warehouseState.trim().equals(PurchaseOrderPresenter.vendorState.trim())){
					sameState++;
					sameWhDet=sameWhDet+obj.getWarehouseName()+" : "+warehouseState+"\n"; 
				}else{
					diffState++;
					diffWhDet=diffWhDet+obj.getWarehouseName()+" : "+warehouseState+"\n"; 
				}
			}
			
			if(sameState==list.size()){
				return true;
			}else if(diffState==list.size()){
				return true;
			}else{
				showDialogMessage("Please check following"
						+"\n"+"VENDOR : "+getSelectedVendorName()+" : "+PurchaseOrderPresenter.vendorState
						+"\n\n"+"Warehouse with state name(same as vendor)"
						+"\n"+sameWhDet
						+"\n"+"Warehouse with state name(other than vendor)"
						+"\n"+diffWhDet
						+"\n"+"Please raise po for all same state or all other state!");
				return false;
			}
			
					
			
		}else if(dselCounter==list.size()){
			
			if(deliveryadd.getAdressline1().getValue().equals("")
					||deliveryadd.getCountry().getSelectedIndex()==0
					||deliveryadd.getState().getSelectedIndex()==0
					||deliveryadd.getCity().getSelectedIndex()==0
					||deliveryadd.getPin().getValue().equals("")){
				
				cbadds.setEnabled(true);
				deliveryadd.setEnable(true);
				showDialogMessage("Please add delivery address!");
				return false;
			}else{
				return true;
			}
			
		}else{
			showDialogMessage("Please select/unselect warehouse for all product !");
			return false;
		}
		
	}
	
	
	/**
	 * This method returns the name of selected Vendor
	 * Date : 03-10-2016 By Anil
	 * Release :30 Sept 2016
	 * Project : PURCHASE MODIFICATION (NBHC)
	 */
	
	public String getSelectedVendorName(){
		String venName="";
		List<VendorDetails> list=vendorTablePO.getDataprovider().getList();
		for(VendorDetails obj:list){
			if(obj.getStatus()==true){
				return obj.getVendorName();
			}
		}
		return "";
	}

	public void addvendorDeatils() {
		// TODO Auto-generated method stub
//		vendorComp.getValue()
		Vector<Filter>temp=new Vector<Filter>();
		MyQuerry querry=new MyQuerry();
		Filter filter =null;
		
		filter = new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(vendorComp.getIdValue());
		temp.add(filter);
		
		querry.setFilters(temp);
		querry.setQuerryObject(new Vendor());
		
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
//				String retVenStateName = "";
				for(SuperModel model:result){
					Vendor ven = (Vendor) model;
//					retVenStateName = ven.getPrimaryAddress().getState();
					
					VendorDetails vendor = new VendorDetails();
					
					vendor.setVendorEmailId(ven.getEmail());
					vendor.setVendorId(ven.getCount());
					vendor.setVendorName(ven.getVendorName());
					vendor.setVendorPhone(ven.getCellNumber1());
					vendor.setPocName(ven.getfullName());
					if(vendorTablePO.getDataprovider().getList().size()==0){
						vendor.setStatus(true);
					}
					
					vendorTablePO.getDataprovider().getList().add(vendor);
					
					
				}

			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
	}

	
	public DoubleBox getDbpaymentrecieved() {
		return dbpaymentrecieved;
	}

	public void setDbpaymentrecieved(DoubleBox dbpaymentrecieved) {
		this.dbpaymentrecieved = dbpaymentrecieved;
	}

	public DoubleBox getDbbalancepayment() {
		return dbbalancepayment;
	}

	public void setDbbalancepayment(DoubleBox dbbalancepayment) {
		this.dbbalancepayment = dbbalancepayment;
	}

	
	/**
	 *  Date 26-11-2018 By Vijay 
	 *  Des :- Adding calculation part for other charges
	 */  
	public void addOtherChargesInTaxTbl(){
		List<ProductOtherCharges> taxList=this.getProducttaxtable().getDataprovider().getList();
		for(OtherCharges otherCharges:tblOtherCharges.getDataprovider().getList()){
			/**
			 * If GST tax is not applicable then we will consider tax1 as vat tax field and tax2 as service tax field.
			 */
			if(otherCharges.getTax1().getTaxPrintName()!=null&&!otherCharges.getTax1().getTaxPrintName().equals("")){
				boolean updateTaxFlag=true;
				if(otherCharges.getTax1().getTaxPrintName().equalsIgnoreCase("SELECT")){
					updateTaxFlag=false;
//					return;
				}
				if(otherCharges.getTax1().getPercentage()==0){
					updateTaxFlag=false;
//					return;
				}
				if(updateTaxFlag){
					ProductOtherCharges pocentity=new ProductOtherCharges();
					pocentity.setChargeName(otherCharges.getTax1().getTaxPrintName());
					pocentity.setChargePercent(otherCharges.getTax1().getPercentage());
//					pocentity.setIndexCheck(i+1);
					int indexValue=this.checkTaxPercent(otherCharges.getTax1().getPercentage(),otherCharges.getTax1().getTaxPrintName());
					if(indexValue!=-1){
						pocentity.setAssessableAmount(otherCharges.getAmount()+taxList.get(indexValue).getAssessableAmount());
						this.getProducttaxtable().getDataprovider().getList().remove(indexValue);
					}
					if(indexValue==-1){
						pocentity.setAssessableAmount(otherCharges.getAmount());
					}
					this.getProducttaxtable().getDataprovider().getList().add(pocentity);
					
				}
			}else{
				boolean updateTaxFlag=true;
				if(otherCharges.getTax1().getPercentage()==0){
					updateTaxFlag=false;
//					return;
				}
				if(updateTaxFlag){
					ProductOtherCharges pocentity=new ProductOtherCharges();
					pocentity.setChargeName("VAT");
					pocentity.setChargePercent(otherCharges.getTax1().getPercentage());
//					pocentity.setIndexCheck(i+1);
					int indexValue=this.checkTaxPercent(otherCharges.getTax1().getPercentage(),"VAT");
					if(indexValue!=-1){
						pocentity.setAssessableAmount(otherCharges.getAmount()+taxList.get(indexValue).getAssessableAmount());
						this.getProducttaxtable().getDataprovider().getList().remove(indexValue);
					}
					if(indexValue==-1){
						pocentity.setAssessableAmount(otherCharges.getAmount());
					}
					this.getProducttaxtable().getDataprovider().getList().add(pocentity);
				}
			}
			
			if(otherCharges.getTax2().getTaxPrintName()!=null&&!otherCharges.getTax2().getTaxPrintName().equals("")){
				System.out.println("INSIDE GST TAX PRINT NAME : "+otherCharges.getTax2().getTaxPrintName());
				boolean updateTaxFlag=true;
				if(otherCharges.getTax2().getTaxPrintName().equalsIgnoreCase("SELECT")){
					updateTaxFlag=false;
//					return;
				}
				if(otherCharges.getTax2().getPercentage()==0){
					updateTaxFlag=false;
//					return;
				}
				if(updateTaxFlag){
					ProductOtherCharges pocentity=new ProductOtherCharges();
					pocentity.setChargeName(otherCharges.getTax2().getTaxPrintName());
					pocentity.setChargePercent(otherCharges.getTax2().getPercentage());
//					pocentity.setIndexCheck(i+1);
					int indexValue=this.checkTaxPercent(otherCharges.getTax2().getPercentage(),otherCharges.getTax2().getTaxPrintName());
					if(indexValue!=-1){
						pocentity.setAssessableAmount(otherCharges.getAmount()+taxList.get(indexValue).getAssessableAmount());
						this.getProducttaxtable().getDataprovider().getList().remove(indexValue);
					}
					if(indexValue==-1){
						pocentity.setAssessableAmount(otherCharges.getAmount());
					}
					this.getProducttaxtable().getDataprovider().getList().add(pocentity);
				}
			}else{
				System.out.println("ST OR NON GST");
				boolean updateTaxFlag=true;
				if(otherCharges.getTax2().getPercentage()==0){
					updateTaxFlag=false;
//					return;
				}
				if(otherCharges.getTax1().getTaxPrintName()!=null&&!otherCharges.getTax1().getTaxPrintName().equals("")){
					updateTaxFlag=false;
//					return;
				}
				if(updateTaxFlag){
					ProductOtherCharges pocentity=new ProductOtherCharges();
					pocentity.setChargeName("Service Tax");
					pocentity.setChargePercent(otherCharges.getTax2().getPercentage());
//					pocentity.setIndexCheck(i+1);
					int indexValue=this.checkTaxPercent(otherCharges.getTax2().getPercentage(),"Service");
					if(indexValue!=-1){
					 	double assessValue=otherCharges.getAmount()+(otherCharges.getAmount()*otherCharges.getTax1().getPercentage()/100);
						pocentity.setAssessableAmount(assessValue+taxList.get(indexValue).getAssessableAmount());
						this.getProducttaxtable().getDataprovider().getList().remove(indexValue);
					}
					if(indexValue==-1){
						double assessValue=otherCharges.getAmount()+(otherCharges.getAmount()*otherCharges.getTax1().getPercentage()/100);
						pocentity.setAssessableAmount(assessValue);
					}
					this.getProducttaxtable().getDataprovider().getList().add(pocentity);
				}
			}
		}
	}


	public OtherChargesTable getTblOtherCharges() {
		return tblOtherCharges;
	}


	public void setTblOtherCharges(OtherChargesTable tblOtherCharges) {
		this.tblOtherCharges = tblOtherCharges;
	}
	
	/**
	 * ends here
	 */
	@Override
	public void refreshTableData() {
		// TODO Auto-generated method stub
		super.refreshTableData();
		productTablePO.getTable().redraw();
		vendorTablePO.getTable().redraw();
		producttaxtable.getTable().redraw();
		productchargestable.getTable().redraw();
		tblOtherCharges.getTable().redraw();
	}
	
	

	
}
