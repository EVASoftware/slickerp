package com.slicktechnologies.client.views.purchase.quickpurchase;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.composite.AddressComposite;
import com.simplesoftwares.client.library.mywidgets.EmailTextBox;
import com.simplesoftwares.client.library.mywidgets.PhoneNumberBox;

public class NewVendorPopUP extends PopupScreen{

	
	TextBox tbVendorName,tbPocName;
	EmailTextBox etbEmail;
	PhoneNumberBox pnbCellNumber;
	AddressComposite addressComposite;
	
	TextBox tbGSTnumber;
	
	
	public NewVendorPopUP(){
		super();
		createGui();
	}
	
	public AddressComposite getAddressComposite() {
		return addressComposite;
	}

	public void setAddressComposite(AddressComposite addressComposite) {
		this.addressComposite = addressComposite;
	}

	private void initilizeWidgets() {

		tbVendorName=new TextBox();
		tbPocName = new TextBox();
		etbEmail = new EmailTextBox();
		pnbCellNumber = new PhoneNumberBox();
		addressComposite = new AddressComposite();
		tbGSTnumber = new TextBox();
	}
	
	@Override
	public void createScreen() {
		
		initilizeWidgets();
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingVendorInformation=fbuilder.setlabel("Vendor Information").widgetType(FieldType.GREYGROUPING).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Vendor Name",tbVendorName);
		FormField ftbVendorName= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("POC Name",tbPocName);
		FormField ftbfullName= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Email",etbEmail);
		FormField fetbEmail= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Cell No",pnbCellNumber);
		FormField fpnbLandlineNo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder();
		FormField fgroupingPrimaryAddress=fbuilder.setlabel("Address Information").widgetType(FieldType.GREYGROUPING).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",addressComposite);
		FormField fPrimaryAddressComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("GST Number",tbGSTnumber);
		FormField ftbGSTnumber= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		

		FormField[][] formfield = {   {fgroupingVendorInformation},
				{ftbVendorName,ftbfullName,fetbEmail,fpnbLandlineNo},
				{ftbGSTnumber},
				{fgroupingPrimaryAddress},
				{fPrimaryAddressComposite}
		};

		this.fields=formfield;
	
	}
	
	

	public TextBox getTbGSTnumber() {
		return tbGSTnumber;
	}

	public void setTbGSTnumber(TextBox tbGSTnumber) {
		this.tbGSTnumber = tbGSTnumber;
	}

	@Override
	public void onClick(ClickEvent event) {
		
	}

	public TextBox getTbVendorName() {
		return tbVendorName;
	}

	public void setTbVendorName(TextBox tbVendorName) {
		this.tbVendorName = tbVendorName;
	}


	public TextBox getTbPocName() {
		return tbPocName;
	}

	public void setTbPocName(TextBox tbPocName) {
		this.tbPocName = tbPocName;
	}

	public EmailTextBox getEtbEmail() {
		return etbEmail;
	}

	public void setEtbEmail(EmailTextBox etbEmail) {
		this.etbEmail = etbEmail;
	}

	public PhoneNumberBox getPnbCellNumber() {
		return pnbCellNumber;
	}

	public void setPnbCellNumber(PhoneNumberBox pnbCellNumber) {
		this.pnbCellNumber = pnbCellNumber;
	}

	
	

}
