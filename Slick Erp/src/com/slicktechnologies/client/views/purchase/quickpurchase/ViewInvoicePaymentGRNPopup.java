package com.slicktechnologies.client.views.purchase.quickpurchase;

import java.util.ArrayList;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCGlassPanel;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.TabPanel;
import com.google.gwt.view.client.NoSelectionModel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.client.views.quicksalesorder.QuickSalesOrderBillingTable;
import com.slicktechnologies.client.views.quicksalesorder.QuickSalesOrderInvoiceTable;
import com.slicktechnologies.client.views.quicksalesorder.QuickSalesOrderPaymentTable;
import com.slicktechnologies.shared.common.inventory.GRN;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.VendorInvoice;

public class ViewInvoicePaymentGRNPopup extends AbsolutePanel{

	public static QuickSalesOrderBillingTable tabBillingTable;
	public static QuickSalesOrderInvoiceTable tabInvoiceTable;
	public static QuickSalesOrderPaymentTable tabPaymentTable;
	public static QuickPurchaseGRNTable tabGRNTable;
	

	public long companyId;
	public int orderId;
	
	final GenricServiceAsync genasync=GWT.create(GenricService.class);
	protected GWTCGlassPanel glassPanel;
	
	
	GeneralViewDocumentPopup genralInvoicePopup;
	public static PopupPanel generalPanel;
	
	FlowPanel btnClosePanel;
	Image btnClose;
	
	
	public ViewInvoicePaymentGRNPopup(){
		
		tabBillingTable = new QuickSalesOrderBillingTable();
		tabInvoiceTable = new QuickSalesOrderInvoiceTable();
		tabPaymentTable = new QuickSalesOrderPaymentTable();
		tabGRNTable = new QuickPurchaseGRNTable();
		
		TabPanel tabPanel = new TabPanel();
		
		btnClosePanel = new FlowPanel();
		
		btnClose = new Image("/images/closebtnred.png");
		btnClose.getElement().setClassName("topRight");
		btnClosePanel.add(btnClose);
		add(btnClosePanel);
		
		tabPanel.addSelectionHandler(new SelectionHandler<Integer>() {
			
			@Override
			public void onSelection(SelectionEvent<Integer> event) {
				int tabId = event.getSelectedItem();
				 System.out.println("TAB ID "+tabId);
				 
				 glassPanel = new GWTCGlassPanel();
				 
					
				 if(tabId==0){
					 tabBillingTable.getTable().redraw();
				 }
				 if(tabId==1){
					 tabInvoiceTable.getTable().redraw();
					 setInvoiceTableDate();
					 setTabelSelectionOnInvoiceTable();
				 }
				 else if(tabId==2){
					 tabPaymentTable.getTable().redraw();
					 setPaymentTableData();
					 setTableSelectionOnPaymentTableData();

					 
				 }else if(tabId==3){
					 tabGRNTable.getTable().redraw();
					 setGRNTableData();
					 setTableSelectionOnGRNTable();

				 }
				
			}
		});
		
		
		
		setTableSelectionOnBillingTable();
		
		FlowPanel flowPanel1=new FlowPanel();
		FlowPanel flowPanel2=new FlowPanel();
		FlowPanel flowPanel3=new FlowPanel();
		FlowPanel flowPanel4=new FlowPanel();
		
		// create titles for tabs
		String tabTitlebilling = AppConstants.BILLINGDETAILS;
		String tabTitleInvoice = AppConstants.PROCESSCONFIGINV;
		String tabTitlePayment = AppConstants.PAYMENT;
		String tabTitleGRN = AppConstants.GRN;
		
		flowPanel1.add(tabBillingTable.getTable());
		flowPanel2.add(tabInvoiceTable.getTable());
		flowPanel3.add(tabPaymentTable.getTable());
		flowPanel4.add(tabGRNTable.getTable());
		
		// create tabs
		tabPanel.add(flowPanel1, tabTitlebilling);
		tabPanel.add(flowPanel2, tabTitleInvoice);
		tabPanel.add(flowPanel3, tabTitlePayment);
		tabPanel.add(flowPanel4, tabTitleGRN);
		
		// select first tab
		tabPanel.selectTab(0);
		tabPanel.setSize("1107px", "430px");
		add(tabPanel, 10, 30);
		setSize("1127px", "450px");
		this.getElement().setId("form");
	}


	
	private void setTableSelectionOnBillingTable() {
		// TODO Auto-generated method stub
		final NoSelectionModel<BillingDocument> selectionModelInvoiceObj = new NoSelectionModel<BillingDocument>();
		SelectionChangeEvent.Handler tableselectionHandler= new SelectionChangeEvent.Handler() {
			
			@Override
			public void onSelectionChange(SelectionChangeEvent event) {
				// TODO Auto-generated method stub
				System.out.println("HELO Vijay Inside Billing Selection");
				BillingDocument billingEntity = selectionModelInvoiceObj.getLastSelectedObject();
				
				AppMemory.getAppMemory().currentScreen = Screen.BILLINGDETAILS;

				genralInvoicePopup = new GeneralViewDocumentPopup();

				genralInvoicePopup.setModel(AppConstants.BILLINGDETAILS,billingEntity);
				generalPanel = new PopupPanel();
				generalPanel.add(genralInvoicePopup);
				generalPanel.show();
				generalPanel.center();
				
			}
		};
		selectionModelInvoiceObj.addSelectionChangeHandler(tableselectionHandler);
		tabBillingTable.getTable().setSelectionModel(selectionModelInvoiceObj);
	
	}


	private void setTableSelectionOnGRNTable() {

		final NoSelectionModel<GRN> selectionModelInvoiceObj = new NoSelectionModel<GRN>();
		SelectionChangeEvent.Handler tableselectionHandler= new SelectionChangeEvent.Handler() {
			
			@Override
			public void onSelectionChange(SelectionChangeEvent event) {
				// TODO Auto-generated method stub
				System.out.println("HELO Vijay Inside GRN Selection");
				GRN GRNEntity = selectionModelInvoiceObj.getLastSelectedObject();
				AppMemory.getAppMemory().currentScreen = Screen.GRN;

				genralInvoicePopup = new GeneralViewDocumentPopup();

				genralInvoicePopup.setModel(AppConstants.GRN,GRNEntity);
				generalPanel = new PopupPanel();
				generalPanel.add(genralInvoicePopup);
				generalPanel.show();
				generalPanel.center();
			}
		};
		selectionModelInvoiceObj.addSelectionChangeHandler(tableselectionHandler);
		tabGRNTable.getTable().setSelectionModel(selectionModelInvoiceObj);
	}
	
	private void setTableSelectionOnPaymentTableData() {

		final NoSelectionModel<CustomerPayment> selectionModelInvoiceObj = new NoSelectionModel<CustomerPayment>();
		SelectionChangeEvent.Handler tableselectionHandler= new SelectionChangeEvent.Handler() {
			
			@Override
			public void onSelectionChange(SelectionChangeEvent event) {
				// TODO Auto-generated method stub
				System.out.println("HELO Vijay Inside PAyment Selection");
				CustomerPayment paymentEntity = selectionModelInvoiceObj.getLastSelectedObject();
				AppMemory.getAppMemory().currentScreen = Screen.VENDORPAYMENTDETAILS;

				genralInvoicePopup = new GeneralViewDocumentPopup();

				genralInvoicePopup.setModel(AppConstants.PROCESSCONFIGCPVENDOR,paymentEntity);
				generalPanel = new PopupPanel();
				generalPanel.add(genralInvoicePopup);
				generalPanel.show();
				generalPanel.center();
			}
		};
		selectionModelInvoiceObj.addSelectionChangeHandler(tableselectionHandler);
		tabPaymentTable.getTable().setSelectionModel(selectionModelInvoiceObj);
	}
	

	private void setTabelSelectionOnInvoiceTable() {

		final NoSelectionModel<Invoice> selectionModelInvoiceObj = new NoSelectionModel<Invoice>();
		SelectionChangeEvent.Handler tableselectionHandler= new SelectionChangeEvent.Handler() {
			
			@Override
			public void onSelectionChange(SelectionChangeEvent event) {
				// TODO Auto-generated method stub
				System.out.println("HELO Vijay Inside Invoice Selection");
				Invoice invoiceEntity = selectionModelInvoiceObj.getLastSelectedObject();
				AppMemory.getAppMemory().currentScreen = Screen.VENDORINVOICEDETAILS;

				genralInvoicePopup = new GeneralViewDocumentPopup();

				genralInvoicePopup.setModel(AppConstants.PROCESSCONFIGINVVENDOR,invoiceEntity);
				generalPanel = new PopupPanel();
				generalPanel.add(genralInvoicePopup);
				generalPanel.show();
				generalPanel.center();
			}
		};
		selectionModelInvoiceObj.addSelectionChangeHandler(tableselectionHandler);
		tabInvoiceTable.getTable().setSelectionModel(selectionModelInvoiceObj);
	}
	
	public QuickSalesOrderInvoiceTable getTabInvoiceTable() {
		return tabInvoiceTable;
	}


	public void setTabInvoiceTable(QuickSalesOrderInvoiceTable tabInvoiceTable) {
		this.tabInvoiceTable = tabInvoiceTable;
	}
	
	
	private void setGRNTableData() {
		
		glassPanel.show();
		
		
		MyQuerry querry = getGRNQuerry(this.companyId,this.orderId);
		
		genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				System.out.println("SIZE Delivery note =="+result.size());
				ArrayList<GRN> grnlist = new ArrayList<GRN>();
				for(SuperModel model : result){
					GRN grn = (GRN) model;
					grnlist.add(grn);
					
					
				}
				tabGRNTable.getDataprovider().setList(grnlist);
				glassPanel.hide();
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				glassPanel.hide();
			}
		});
	}
	
	


	private void setPaymentTableData() {

		glassPanel.show();
		
		MyQuerry querry = getPaymentQuerry(this.companyId,this.orderId);
		
		genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				ArrayList<CustomerPayment> paymentlist = new ArrayList<CustomerPayment>();
				
				for(SuperModel model : result){
					CustomerPayment payment = (CustomerPayment) model;
					paymentlist.add(payment);
				}
				tabPaymentTable.getDataprovider().setList(paymentlist);
				glassPanel.hide();
				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				glassPanel.hide();
			}
		});
	}

	
	private void setInvoiceTableDate() {
		// TODO Auto-generated method stub
		glassPanel.show();
		MyQuerry querry = getInvoiceQuerry(this.companyId,this.orderId);

		genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				
				ArrayList<Invoice> invoicelist = new ArrayList<Invoice>();
				
				for(SuperModel model : result){
					Invoice invoice = (Invoice) model;
					invoicelist.add(invoice);
				}
				tabInvoiceTable.getDataprovider().setList(invoicelist);
				glassPanel.hide();
				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
		
	}

	private MyQuerry getInvoiceQuerry(long companyId, int salesOrderId) {


		MyQuerry querry = new MyQuerry();
		
		Vector<Filter> filtervec = new Vector<Filter>();
		
		Filter filter = new Filter();
		filter.setIntValue(salesOrderId);
		filter.setQuerryString("contractCount");
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setLongValue(companyId);
		filter.setQuerryString("companyId");
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setStringValue("Purchase Order");
		filter.setQuerryString("typeOfOrder");
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new VendorInvoice());
		return querry;
	}



	private MyQuerry getPaymentQuerry(long companyId, int salesOrderId) {

		MyQuerry querry = new MyQuerry();
		
		Vector<Filter> filtervec = new Vector<Filter>();
		
		Filter filter = new Filter();
		filter.setIntValue(salesOrderId);
		filter.setQuerryString("contractCount");
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setLongValue(companyId);
		filter.setQuerryString("companyId");
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setStringValue("Purchase Order");
		filter.setQuerryString("typeOfOrder");
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new CustomerPayment());
		
		return querry;
	}

	
	private MyQuerry getGRNQuerry(long companyId, int salesOrderId) {

		MyQuerry querry = new MyQuerry();
		
		Vector<Filter> filtervec = new Vector<Filter>();
		
		Filter filter = new Filter();
		filter.setIntValue(salesOrderId);
		filter.setQuerryString("poNo");
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setLongValue(companyId);
		filter.setQuerryString("companyId");
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new GRN());
		
		return querry;
	}



	public QuickSalesOrderBillingTable getTabBillingTable() {
		return tabBillingTable;
	}



	public void setTabBillingTable(QuickSalesOrderBillingTable tabBillingTable) {
		this.tabBillingTable = tabBillingTable;
	}



	public Image getBtnClose() {
		return btnClose;
	}



	public void setBtnClose(Image btnClose) {
		this.btnClose = btnClose;
	}

	
	
	


}
