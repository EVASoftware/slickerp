package com.slicktechnologies.client.views.purchase.quickpurchase;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.google.gwt.view.client.RowCountChangeEvent.Handler;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.reusabledata.ConditionDialogBox;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.EmailService;
import com.slicktechnologies.client.services.EmailServiceAsync;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.ApproverFactory;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.contract.CancellationPopUp;
import com.slicktechnologies.client.views.documentcancellation.CancellationSummary;
import com.slicktechnologies.client.views.documentcancellation.CancellationSummaryPopUp;
import com.slicktechnologies.client.views.documentcancellation.CancellationSummaryPopUpTable;
import com.slicktechnologies.client.views.documentcancellation.DocumentCancellationPopUp;
import com.slicktechnologies.client.views.popups.NewEmailPopUp;
import com.slicktechnologies.client.views.purchase.purchaseorder.ProductTablePO;
import com.slicktechnologies.client.views.purchase.purchaseorder.PuchaseOrderPresenterSearchProxy;
import com.slicktechnologies.client.views.purchase.purchaseorder.PurchaseOrderForm;
import com.slicktechnologies.client.views.purchase.purchaseorder.PurchaseOrderPresenter;
import com.slicktechnologies.client.views.purchase.purchaseorder.PurchaseOrderPresenterTableProxy;
import com.slicktechnologies.client.views.purchase.purchaseorder.PurchaseOrderVendors;
import com.slicktechnologies.client.views.purchase.purchaserequisition.PurchaseRequitionPresenter;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.ProductOtherCharges;
import com.slicktechnologies.shared.TaxDetails;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessprocesslayer.LetterOfIntent;
import com.slicktechnologies.shared.common.businessprocesslayer.PriceList;
import com.slicktechnologies.shared.common.businessprocesslayer.PriceListDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetailsPO;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseRequisition;
import com.slicktechnologies.shared.common.businessprocesslayer.RequsestForQuotation;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessprocesslayer.VendorDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cancelsummary.CancelSummary;
import com.slicktechnologies.shared.common.contactidentification.ContactPersonIdentification;
import com.slicktechnologies.shared.common.contractcancel.CancelContract;
import com.slicktechnologies.shared.common.inventory.GRN;
import com.slicktechnologies.shared.common.inventory.Inspection;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.productlayer.Tax;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.salesorder.OtherCharges;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

public class QuickPurchaseOrderPreseneter extends FormScreenPresenter<PurchaseOrder> implements Handler,ChangeHandler {
	


	public static QuickPurchaseOrderForm form;
	final GenricServiceAsync async = GWT.create(GenricService.class);
	EmailServiceAsync emailService = GWT.create(EmailService.class);

	static List<CancellationSummary> cancellis = new ArrayList<CancellationSummary>();
	CsvServiceAsync csvservice = GWT.create(CsvService.class);
	CancellationSummaryPopUp summaryPopup = new CancellationSummaryPopUp();
	PopupPanel summaryPanel;
	DocumentCancellationPopUp cancelPopup = new DocumentCancellationPopUp();
	PopupPanel cancelpanel;

	int hygeia = 0;

	String retrCompnyState = retrieveCompanyState();
	static String companyState = "";

	public static String vendorState = "";
	String retVenStateName = "";

	NumberFormat nf = NumberFormat.getFormat("0.00");

	GeneralServiceAsync quickpurchaseasync = GWT.create(GeneralService.class);
	/**
	 * If this flag is true then it retrieve the state in which vendor is
	 * located we are doing it to know which tax is applicable in that scenario.
	 * Date :13-09-2016 By Anil Release :31 Aug 2016 Project : PURCHASE ERROR
	 * SOLVING (NBHC)
	 * 
	 */
	public static boolean retVenStateFlage = false;

	/**
	 * END
	 */
	
	NewVendorPopUP newVenderPupup = new NewVendorPopUP();
	
	ViewInvoicePaymentGRNPopup viewInvoicePaymentPopup = new ViewInvoicePaymentGRNPopup();
	PopupPanel invoicePaymentPanel;
	
	/**
	 * Date 24-09-2018 by Vijay
	 * Add the condition popup
	 */
	ConditionDialogBox conditionPopup=new ConditionDialogBox("Do you want to print on preprinted Stationery",AppConstants.YES,AppConstants.NO);
	PopupPanel panel ;
	int cnt=0;
	GeneralServiceAsync gasync =  GWT.create(GeneralService.class);
	CancellationPopUp cancellationPopup = new CancellationPopUp();
	
 	NewEmailPopUp emailpopup = new NewEmailPopUp();

	public QuickPurchaseOrderPreseneter(FormScreen<PurchaseOrder> view,PurchaseOrder model) {
		super(view, model);
		form = (QuickPurchaseOrderForm) view;
		
		form.getProductTablePO().getTable().addRowCountChangeHandler(this);
//		form.getProductchargestable().getTable().addRowCountChangeHandler(this);
		/** Date 26-11-2018 By Vijay For Other Charges with taxes ****/
		form.getTblOtherCharges().getTable().addRowCountChangeHandler(this);
		
		form.getVendortbl().getTable().addRowCountChangeHandler(this);
		
		form.getCbadds().addClickHandler(this);
//		form.addPaymentTerms.addClickHandler(this);
		form.addOtherCharges.addClickHandler(this);
		form.addproducts.addClickHandler(this);
		form.getDeliveryadd().getState().addChangeHandler(this);

		cancelPopup.getBtnOk().addClickHandler(this);
		cancelPopup.getBtnCancel().addClickHandler(this);
		summaryPopup.getBtnOk().addClickHandler(this);
		summaryPopup.getBtnDownload().addClickHandler(this);

		boolean isDownload = AuthorizationHelper.getDownloadAuthorization(Screen.QUICKPURCHASEORDER, LoginPresenter.currentModule.trim());
		if (isDownload == false) {
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
		form.btnAddVendor.addClickHandler(this);
		
		form.btnNewVendor.addClickHandler(this);
		
		newVenderPupup.getLblOk().addClickHandler(this);
		newVenderPupup.getLblCancel().addClickHandler(this);
		
		form.dbpaymentrecieved.addChangeHandler(this);

		viewInvoicePaymentPopup.btnClose.addClickHandler(this);
		
		/**
		 * Date 24-09-2018 By Vijay
		 * Des :- added the condition popup click handler
		 */
		conditionPopup.getBtnOne().addClickHandler(this);
		conditionPopup.getBtnTwo().addClickHandler(this);
		/**
		 * ends here
		 */
		cancellationPopup.getLblOk().addClickHandler(this);
		cancellationPopup.getLblCancel().addClickHandler(this);

	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel label = (InlineLabel) e.getSource();
		String text = label.getText().trim();

		if (text.equals("New")) {
			initalize();
		}
		if (text.equals("Email")) {
			reactOnEmail();
		}
//		if (text.equals(AppConstants.CANCELPURCHASEORDER)) {
//			cancelPopup.getPaymentDate().setValue(new Date() + "");
//			cancelPopup.getPaymentID().setValue(model.getCount() + "");
//			cancelPopup.getPaymentStatus().setValue(model.getStatus());
//			cancelpanel = new PopupPanel(true);
//			cancelpanel.add(cancelPopup);
//			cancelpanel.show();
//			cancelpanel.center();
//		}

		if (text.equals(AppConstants.CANCELLATIONSUMMARY)){
			getSummaryFromPurchaseOrderID();
		}
		
		if(text.equals(AppConstants.SUBMIT)){
			reactOnSubmit();
		}
		
		if(text.equals(AppConstants.VIEWINVOICEPAYMENT)){
			reatconViewInvoicePaymentAndGRN();
		}
		if (text.equals(AppConstants.CANCELPURCHASEORDER)) {
			checkForGRNStatus1();
		}
	}
	
	private void checkForGRNStatus1() {


		Console.log("Inside check  grn status");
		final int purchaseorderID = model.getCount();
		final String purchaseorderStatus = model.getStatus();

		MyQuerry querry = new MyQuerry();
		Company c = new Company();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("poNo");
		filter.setIntValue(purchaseorderID);
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new GRN());

		async.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {

					@Override
					public void onFailure(Throwable caught) {
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						int pic = 0;
						int request = 0;
						int inspection = 0;
						int message = 0;
						ArrayList<GRN> slist = new ArrayList<GRN>();
						/**
						 * Updated By: Viraj
						 * Date: 22-01-2019
						 * Description: ArrayList to add grnlist
						 */
						final ArrayList<GRN> grnList = new ArrayList<GRN>();
						/** Ends **/

						for (SuperModel model : result) {
							GRN grnEntity = (GRN) model;
							grnEntity.setStatus(grnEntity.getStatus());
							grnEntity.setCount(grnEntity.getCount());
							slist.add(grnEntity);
							
							/**
							 * Updated By: Viraj
							 * Date: 22-01-2019
							 * Description: To add grn list which are other than cancelled and rejected
							 */
							if(!grnEntity.getStatus().equalsIgnoreCase(GRN.CANCELLED)
									&& !grnEntity.getStatus().equalsIgnoreCase(GRN.REJECTED)) {
								grnList.add(grnEntity);
							}
							/** Ends **/
						}
							/**
							 * Update By: Viraj
							 * Date: 22-01-2019
							 * Description: to show all documents related to the purchase order in cancellation popup
							 */
							Company c = new Company();
							
								gasync.getCancellingDocumentPurchaseOrder(c.getCompanyId(), model.getCount(), new AsyncCallback<ArrayList<CancelContract>>() {

									@Override
									public void onFailure(Throwable caught) {
										System.out.println("cancellation document error: "+ caught);
									}

									@Override
									public void onSuccess(ArrayList<CancelContract> result) {
										cancellationPopup.getAllContractDocumentTable().getDataprovider().getList().clear();
										
										if(!model.getStatus().equalsIgnoreCase(PurchaseOrder.CANCELLED)) {
											CancelContract canContract = new CancelContract();
											canContract.setDocumnetId(model.getCount());
						            		canContract.setBranch(model.getBranch());
						            		if(model.getVendorDetails() != null) {
							            		for(int i=0;i<model.getVendorDetails().size();i++) {
							            			if(model.getVendorDetails().get(i).getStatus()) {
							            				canContract.setBpName(model.getVendorDetails().get(i).getVendorName());
							            			}
							            		}
						            		}
						            		canContract.setDocumentStatus(model.getStatus());
						            		canContract.setDocumnetName(AppConstants.PURCHASEORDER);
						            		canContract.setRecordSelect(true);
						            		cancellationPopup.getAllContractDocumentTable().getDataprovider().getList().add(canContract);
										}
										
											if(grnList.size() != 0) {
												for(int i=0; i<grnList.size(); i++) {
													CancelContract canContract = new CancelContract();
													canContract.setDocumnetId(grnList.get(i).getCount());
								            		canContract.setBranch(grnList.get(i).getBranch());
								            		canContract.setBpName(grnList.get(i).getVendorInfo().getFullName());
								            		canContract.setDocumentStatus(grnList.get(i).getStatus());
								            		canContract.setDocumnetName(AppConstants.GRN);
								            		canContract.setRecordSelect(true);
								            		cancellationPopup.getAllContractDocumentTable().getDataprovider().getList().add(canContract);
												}
											}
											
											cancellationPopup.getAllContractDocumentTable().getDataprovider().getList().addAll(result);
											cancellationPopup.getAllContractDocumentTable().getTable().redraw();
											cancellationPopup.showPopUp();
									}
										
								});
										

					}
				});

			
	}

	private void reatconViewInvoicePaymentAndGRN() {
		
		MyQuerry query = getQuery(model);
		query.setQuerryObject(new BillingDocument());

		async.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
			
				System.out.println("Result size=="+result.size());
				ArrayList<BillingDocument> billinglist = new ArrayList<BillingDocument>();
				for(SuperModel model : result){
					BillingDocument billingEntity = (BillingDocument) model;
					
					billinglist.add(billingEntity);
				}

				viewInvoicePaymentPopup.companyId=model.getCompanyId();
				viewInvoicePaymentPopup.orderId=model.getCount();
				viewInvoicePaymentPopup.getTabBillingTable().getDataprovider().setList(billinglist);
				viewInvoicePaymentPopup.getTabBillingTable().getTable().redraw();
				invoicePaymentPanel = new PopupPanel();
				
				invoicePaymentPanel.add(viewInvoicePaymentPopup);
				invoicePaymentPanel.center();
				invoicePaymentPanel.show();
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				form.showDialogMessage("Unexpected Error Occurred");

			}
		});
		
	}
	
	public static void updateVendorTable() {
		if (form.productTablePO.getDataprovider().getList().size() == 0) {
			form.vendorTablePO.getDataprovider().getList().clear();
		} 
	}
	
	private MyQuerry getQuery(PurchaseOrder model) {

		MyQuerry querry = new MyQuerry();
		
		Vector<Filter> filtervec = new Vector<Filter>();
		
		Filter filter = new Filter();
		filter.setIntValue(model.getCount());
		filter.setQuerryString("contractCount");
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setLongValue(model.getCompanyId());
		filter.setQuerryString("companyId");
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setStringValue("Purchase Order");
		filter.setQuerryString("typeOfOrder");
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		return querry;
	}


	private void reactOnSubmit() {

		form.showWaitSymbol();
		int count = 0; 
		/**
		 * Updated By:Viraj
		 * Date:11-02-2019
		 * Description:To set grn if warehouse present.
		 */
		if(model != null) {
			for(int i =0;i < model.getProductDetails().size(); i++) {
				if(model.getProductDetails().get(i).getItemProductWarehouseName() != null && model.getProductDetails().get(i).getItemProductWarehouseName().equals("")) {
					count += 1;
					System.out.println("count : "+count);
				}
			}
			if(count == 0) {
				quickpurchaseasync.quickPurchaseOrderSubmit(model, new AsyncCallback<String>() {
					
					@Override
					public void onSuccess(final String result) {
						// TODO Auto-generated method stub
						model.setStatus(PurchaseOrder.APPROVED);
						form.tbStatus.setValue(PurchaseOrder.APPROVED);
						
						String mainScreenLabel="Purchase Register";
						if(form.purchaseOrderobj!=null&&form.purchaseOrderobj.getCount()!=0){
							mainScreenLabel=form.purchaseOrderobj.getCount()+"/"+form.purchaseOrderobj.getStatus()+"/"+AppUtility.parseDate(form.purchaseOrderobj.getCreationDate());
						}
						form.fgroupingLetterInformation.getHeaderLabel().setText(mainScreenLabel);
						
						form.setMenuAsPerStatus();
						Timer time = new Timer() {
							@Override
							public void run() {
								// TODO Auto-generated method stub
									form.showDialogMessage(result);
									form.hideWaitSymbol();
							}
						};
						time.schedule(3000);
					}
					
					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						form.showDialogMessage("Unexpected Error");
						form.hideWaitSymbol();
					}
				});
			} else if(count == model.getProductDetails().size()){
				quickpurchaseasync.quickPurchaseOrderSubmit(model, new AsyncCallback<String>() {
					
					@Override
					public void onSuccess(final String result) {
						// TODO Auto-generated method stub
						model.setStatus(PurchaseOrder.APPROVED);
						form.tbStatus.setValue(PurchaseOrder.APPROVED);
						form.setMenuAsPerStatus();
						Timer time = new Timer() {
							@Override
							public void run() {
								// TODO Auto-generated method stub
									form.showDialogMessage(result);
									form.hideWaitSymbol();
							}
						};
						time.schedule(3000);
					}
					
					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						form.showDialogMessage("Unexpected Error");
						form.hideWaitSymbol();
					}
				});
			} else {
				form.showDialogMessage("Please assign warehouse to all products");
				form.hideWaitSymbol();
			}
		}
		/** Ends **/
	}

	/**
	 * Return Employee details from global employee list
	 * Date : 17-10-2016 By Anil
	 * Release : 30 Sept 2016
	 * Project : PURCHASE MODIFICATION (NBHC)
	 */
	
	public Employee getEmployeeDetails(String fullname){
		Employee emp=null;
		for(Employee obj:LoginPresenter.globalEmployee){
			if(fullname.equals(obj.getFullname())){
				return obj;
			}
		}
		
		return emp;
	}
	

	@Override
	public void reactOnPrint() {

		MyQuerry querry = new MyQuerry();
		Company c = new Company();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("processName");
		filter.setStringValue("PurchaseOrder");
		filtervec.add(filter);

		filter = new Filter();
		filter.setQuerryString("processList.status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);

		querry.setFilters(filtervec);
		querry.setQuerryObject(new ProcessConfiguration());

		async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				System.out.println(" result set size +++++++"+ result.size());

				List<ProcessTypeDetails> processList = new ArrayList<ProcessTypeDetails>();

				if (result.size() == 0) {
					final String url = GWT.getModuleBaseURL()+ "pdfpurchase" + "?Id=" + model.getId() + "&" + "preprint=" + "plane";
					Window.open(url, "test", "enabled");
					
					
				}
				/**
				 * Date 20-7-2018
				 */
				else if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("PurchaseOrder", "POPDFV1")){
					final String url = GWT.getModuleBaseURL()+ "pdfpurchase" + "?Id=" + model.getId()+"&"+"type="+"POPDFV1";
					Window.open(url, "test", "enabled");
				}/* nidhi for copy of shasha for other client*/
				else if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("PurchaseOrder", "PurchaseOrderUpdatedPdf")){
					final String url = GWT.getModuleBaseURL()+ "pdfpurchase" + "?Id=" + model.getId()+"&"+"type="+"PurchaseOrderUpdatedPdf";
					Window.open(url, "test", "enabled");
				}
				else {
					for (SuperModel model : result) {
						ProcessConfiguration processConfig = (ProcessConfiguration) model;
						processList.addAll(processConfig.getProcessList());
					}
					for (int k = 0; k < processList.size(); k++) {
						if (processList.get(k).getProcessType().trim().equalsIgnoreCase("HygeiaPestManagement")
								&&processList.get(k).isStatus() == true) {
							hygeia = hygeia + 1;
						}
					}
					if (hygeia > 0) {
						final String url = GWT.getModuleBaseURL()+ "pdfpurchaseorder" + "?Id="+ model.getId();
						Window.open(url, "test", "enabled");
					} else {
						final String url = GWT.getModuleBaseURL()+ "pdfpurchase" + "?Id="+ model.getId() + "&" + "preprint=" + "plane";;
						Window.open(url, "test", "enabled");
					}
				}
			}
		});
	}

	@Override
	public void reactOnEmail() {

//		boolean conf = Window.confirm("Do you really want to send email?");
//		if (conf == true) {
//			emailService.initiatePOEmail((PurchaseOrder) model,new AsyncCallback<Void>() {
//				@Override
//				public void onSuccess(Void result) {
//					Window.alert("Email Sent Sucessfully !");
//				}
//
//				@Override
//				public void onFailure(Throwable caught) {
//					Window.alert("Resource Quota Ended");
//					caught.printStackTrace();
//				}
//			});
//		}
		
		/**
		 * @author Vijay Date 19-03-2021
		 * Des :- above old code commented 
		 * Added New Email Popup Functionality
		 */
		emailpopup.showPopUp();
		setEmailPopUpData();
		
		/**
		 * ends here
		 */
	}

	@Override
	protected void makeNewModel() {
		model = new PurchaseOrder();

	}

	public static QuickPurchaseOrderForm initalize() {
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Purchase Register", Screen.QUICKPURCHASEORDER);
		QuickPurchaseOrderForm form = new QuickPurchaseOrderForm();

		PurchaseOrderPresenterTableProxy gentable = new PurchaseOrderPresenterTableProxy();
		gentable.setView(form);
		gentable.applySelectionModle();

		PuchaseOrderPresenterSearchProxy.staticSuperTable = gentable;
		PuchaseOrderPresenterSearchProxy searchpopup = new PuchaseOrderPresenterSearchProxy();
		form.setSearchpopupscreen(searchpopup);
		QuickPurchaseOrderPreseneter presenter = new QuickPurchaseOrderPreseneter(form,new PurchaseOrder());
		AppMemory.getAppMemory().stickPnel(form);
		return form;
	}

	@Override
	public void reactOnDownload() {
		CsvServiceAsync async = GWT.create(CsvService.class);
		ArrayList<PurchaseOrder> POArray = new ArrayList<PurchaseOrder>();
		List<PurchaseOrder> list = form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
		POArray.addAll(list);
		async.setPO(POArray, new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed" + caught);
			}

			@Override
			public void onSuccess(Void result) {
				String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url = gwt + "csvservlet" + "?type=" + 46;
				Window.open(url, "test", "enabled");
			}
		});
	}

	@Override
	public void onRowCountChange(RowCountChangeEvent event) {
		NumberFormat nf = NumberFormat.getFormat("0.00");

		if (event.getSource().equals(form.getProductTablePO().getTable())) {
			System.out.println("Row Count Change Called");
			this.productsOnChange();
		}
//		if (event.getSource().equals(form.getProductchargestable().getTable())) {
//			if (form.getDoamtincltax().getValue() != null) {
//				double newnetpay = form.getDoamtincltax().getValue()+ form.getProductchargestable().calculateNetPayable();
//				newnetpay = Math.round(newnetpay);
//				int netPayAmt = (int) newnetpay;
//				newnetpay = netPayAmt;
//				form.getDonetpayamt().setValue(Double.parseDouble(nf.format(newnetpay)));
//				
//				/******************** vijay  for quick Purchase on 18-08-2017 *******************/
//				if(form.getDbpaymentrecieved().getValue()!=null){
//					double balancePayment=newnetpay-form.getDbpaymentrecieved().getValue();
//					System.out.println("balance payment ===="+balancePayment);
//					form.getDbbalancepayment().setValue(Double.parseDouble(nf.format(balancePayment)));
//				}else{
//					form.getDbbalancepayment().setValue(Double.parseDouble(nf.format(newnetpay)));
//				}
//			}
//		}

		/**
		 *  Date 26-11-2018 By Vijay 
		 *  Des :- As per the Other Charges updated table And Taxes Code updated
		 *  above old code commented
		 */
		
		if (event.getSource().equals(form.tblOtherCharges.getTable())) {
				form.getProducttaxtable().connectToLocal();
				try {
					form.addProdTaxes();
					form.addOtherChargesInTaxTbl();
					/** Date 26-11-2018 By Vijay For Other Charges***/
					form.getDoamtincltax().setValue(form.tblOtherCharges.calculateOtherChargesSum());
				} catch (Exception e) {
					e.printStackTrace();
				}
				/**
				 *  Date 26-11-2018 By Vijay 
				 *  Des :- As per the Other Charges And Taxes Code updated
				 */
				double totalIncludingTax=form.getDbTotal().getValue()+form.getProducttaxtable().calculateTotalTaxes();
				double newnetpay = totalIncludingTax+ form.tblOtherCharges.calculateOtherChargesSum();
				
//				double newnetpay = form.getDoamtincltax().getValue()+ form.getProductchargestable().calculateNetPayable();
				newnetpay = Math.round(newnetpay);
				int netPayAmt = (int) newnetpay;
				newnetpay = netPayAmt;
				form.getDonetpayamt().setValue(Double.parseDouble(nf.format(newnetpay)));
				
				/******************** vijay  for quick Purchase on 18-08-2017 *******************/
				if(form.getDbpaymentrecieved().getValue()!=null){
					double balancePayment=newnetpay-form.getDbpaymentrecieved().getValue();
					System.out.println("balance payment ===="+balancePayment);
					form.getDbbalancepayment().setValue(Double.parseDouble(nf.format(balancePayment)));
				}else{
					form.getDbbalancepayment().setValue(Double.parseDouble(nf.format(newnetpay)));
				}
		}
		
		if (event.getSource().equals(form.getVendortbl().getTable())) {

			Timer timer = new Timer() {
				@Override
				public void run() {
					System.out.println("VENDOR ROW COUNT CHANGE .....");
					System.out.println("RET VENDOR STATE FLAG "+ retVenStateFlage);
//					if (retVenStateFlage) {
						if (form.validateVendorselect()) {
							int vendorCount = retrieveVendorCount();
							System.out.println("Vendor COUNT - " + vendorCount);
							retVenStateName = retVendorState(vendorCount);
						}else{
							vendorState="";
						}
//						retVenStateFlage = false;
//					}
				}
			};
			timer.schedule(1000);
		}

	}


	protected boolean validateProductId(int productId,List<ProductDetailsPO> tableProducts) {
		int ctr = 0;
		boolean flagVal = false;

		for (int i = 0; i < tableProducts.size(); i++) {
			if (tableProducts.get(i).getProductID() == productId) {
				ctr = ctr + 1;
			}
		}

		if (ctr == 0) {
			flagVal = false;
		} else {
			flagVal = true;
		}
		return flagVal;
	}

	

	public List<VendorDetails> vendorListRetrieved(List<PurchaseOrderVendors> listOfVendors,
			List<VendorDetails> vendorList) {

		ArrayList<VendorDetails> vendorTableSet = new ArrayList<VendorDetails>();
		for (VendorDetails vd : vendorList) {
			int vendorCount = vd.getVendorId();
			for (int k = 0; k < listOfVendors.size(); k++) {
				if (listOfVendors.get(k).getVendorId() == vendorCount) {
					VendorDetails vdEntity = new VendorDetails();
					vdEntity.setVendorId(listOfVendors.get(k).getVendorId());
					vdEntity.setVendorName(listOfVendors.get(k).getVendorName());
					vdEntity.setVendorPhone(listOfVendors.get(k).getVendorCellNumber());
					vdEntity.setVendorEmailId(listOfVendors.get(k).getVendorEmail());

					// vijay
					vdEntity.setPocName(listOfVendors.get(k).getVendorPocName());

					vendorTableSet.add(vdEntity);
				}
			}
		}
		return vendorTableSet;

	}

	/**
	 * On Change Event This method handles change event for loi id,rfq id, pr
	 * id, warehouse
	 */

	@Override
	public void onChange(ChangeEvent event) {

		/**
		 * On change of state of Delivery Address
		 */

		if (event.getSource().equals(form.getDeliveryadd().getState())) {
//				productsOnChange();
		}
		
		
		/**
		 * Date 18-08-2017 added by vijay for Quick Purchase Order
		 */
		
		if(event.getSource().equals(form.dbpaymentrecieved)){
			System.out.println("you are here on change");
			
			System.out.println(" net payment ==== "+form.donetpayamt.getValue());
			System.out.println("hi recievd == "+form.dbpaymentrecieved.getValue());
			System.out.println("balance =="+form.dbbalancepayment.getValue());
			if(form.dbpaymentrecieved.getValue()==null){
				form.dbpaymentrecieved.setValue(0d);
				form.dbbalancepayment.setValue(form.donetpayamt.getValue());
			}
				
			
			boolean valid =	validation(form.donetpayamt.getValue(),form.dbpaymentrecieved.getValue());
			if(valid == false){
				form.showDialogMessage("Payment recieved amount not greater than net payable amount");
				form.dbpaymentrecieved.setText(null);
				form.dbbalancepayment.setValue(form.donetpayamt.getValue());
			}
			if(valid){
				form.dbbalancepayment.setValue(form.donetpayamt.getValue()-form.dbpaymentrecieved.getValue());
				System.out.println(" if chnged value then == "+ form.dbbalancepayment.getValue());
			}
			
		}

	}
	
	
	private boolean validation(Double netpayable, Double recievedamt) {
		if(recievedamt>netpayable){
			return false;
	    }
	  return true;
    }
	

	/**
	 * On click event This method handles click event for payment terms,
	 * delivery address checkbox, add other charges and addproducts
	 */

	@Override
	public void onClick(ClickEvent event) {
		super.onClick(event);

		if (event.getSource().equals(form.getCbadds())) {
			if (form.getCbadds().getValue() == true) {
				form.getDeliveryadd().clear();
				form.getDeliveryadd().setEnable(true);
			} 
			if (form.getCbadds().getValue() == false) {
				form.getCompanyAddress(model.getCompanyId());
			}
		}

//		if (event.getSource().equals(form.addOtherCharges)) {
//			int size = form.productchargestable.getDataprovider().getList().size();
//			if (form.doamtincltax.getValue() != null) {
//				ProductOtherCharges prodCharges = new ProductOtherCharges();
//				prodCharges.setAssessableAmount(form.getDoamtincltax().getValue());
//				prodCharges.setFlagVal(false);
//				prodCharges.setIndexCheck(size + 1);
//				form.productchargestable.getDataprovider().getList().add(prodCharges);
//			}
//		}

		/**
		 * Date : 24-11-2018 By Vijay
		 * Des :- For Other Charges with Taxes Update and above old code commented
		 */
		if(event.getSource().equals(form.addOtherCharges)){
			Tax tax1=new Tax();
			Tax tax2=new Tax();
			OtherCharges prodCharges=new OtherCharges();
			prodCharges.setOtherChargeName("");
			prodCharges.setAmount(0);
			prodCharges.setTax1(tax1);
			prodCharges.setTax2(tax2);
			form.tblOtherCharges.getDataprovider().getList().add(prodCharges);
		}
		/**
		 * End
		 */
		
		if (event.getSource().equals(form.addproducts)) {
			if (!form.prodInfoComposite.getProdCode().getValue().equals("")) {
				System.out.println("PrintPCD"+ form.prodInfoComposite.getProdCode().getValue());
				boolean productValidation = form.validateProductDuplicate(form.prodInfoComposite.getProdCode().getValue().trim());

				if (productValidation) {
					form.ProdctType(form.prodInfoComposite.getProdCode().getValue().trim(), form.prodInfoComposite.getProdID().getValue().trim());
				}
			}
		}

		if (event.getSource().equals(form.btnAddVendor)) {
//			if (event.getSource().equals(form.btnAddVendor)
//					&& !form.vendorComp.getName().getValue().equals("")) {
				if (form.validVendor()) {
					form.addvendorDeatils();
				}
		}

		if (event.getSource() == cancelPopup.getBtnOk()) {
			cancelpanel.hide();
			checkForGRNStatus();
		}

		if (event.getSource() == cancelPopup.getBtnCancel()) {
			cancelpanel.hide();
			cancelPopup.remark.setValue("");
		}

		if (event.getSource() == summaryPopup.getBtnOk()) {
			summaryPanel.hide();
		}

		if (event.getSource() == summaryPopup.getBtnDownload()) {
			summaryPanel.hide();
			reactOnCancelSummaryDownload();
		}

		if (event.getSource().equals(form.btnNewVendor)) {
			newVenderPupup.showPopUp();			
		}
		
		
		if(event.getSource()==newVenderPupup.getLblOk()){
			if(ValidateVendorInfo()){
				System.out.println("hi vijay inside ok");
				saveNewVendorInfo();
			}
		}
		
		if(event.getSource()==newVenderPupup.getLblCancel()){
			newVenderPupup.getPopup().hide();
		}
		
		
		
//		if(event.getSource() instanceof InlineLabel)
//		{
//			InlineLabel lbl= (InlineLabel) event.getSource();
//			System.out.println(newVenderPupup.getLblOk().getText());
//			System.out.println(lbl.getText());
//			if(lbl.getText().equals(newVenderPupup.getLblOk().getText())){
//				if(ValidateVendorInfo()){
//					System.out.println("hi vijay inside ok");
//					saveNewVendorInfo();
//				}
//			}
//			if(lbl.getText().equals(newVenderPupup.getLblCancel().getText())){
//				newVenderPupup.getPopup().hide();
//			}
//		}
		
		if(event.getSource() == viewInvoicePaymentPopup.btnClose){
			this.initalize();
			Timer time = new Timer() {
				@Override
				public void run() {
					form.updateView(model);
		        	form.setToViewState();
					invoicePaymentPanel.hide();
				}
			};
			time.schedule(3000);
		}

		/**
		 * Date 24-09-2018 by Vijay
		 * Des :- print with company as letter head or not
		 */
		if(event.getSource().equals(conditionPopup.getBtnOne())){
			final String url = GWT.getModuleBaseURL()+ "pdfpurchase" + "?Id="+ model.getId()+"&"+"preprint="+"yes";
			Window.open(url, "test", "enabled");
			panel.hide();
		}

		if(event.getSource().equals(conditionPopup.getBtnTwo())){
			final String url = GWT.getModuleBaseURL()+ "pdfpurchase" + "?Id="+ model.getId()+"&"+"preprint="+"no";
			Window.open(url, "test", "enabled");
			panel.hide();
		}
		/**
		 * ends here
		 */
		if(event.getSource() == cancellationPopup.getLblOk()){
			if(cancellationPopup.getRemark().getValue().trim()!= null && !cancellationPopup.getRemark().getValue().trim().equals("")){
				//  react on purchase order cancel
				reactOnCancelContractButton(cancellationPopup.getRemark().getValue().trim());
				cancellationPopup.hidePopUp();
			}
			else{
				form.showDialogMessage("Please enter valid remark to cancel contract..!");
			}
		}
        if(event.getSource() == cancellationPopup.getLblCancel()){
        	cancellationPopup.hidePopUp();
			cancellationPopup.remark.setValue("");
		}
	}

	private void reactOnCancelContractButton(final String remark) {
		final String loginUser = LoginPresenter.loggedInUser.trim();
		List<CancelContract> cancelContractList = cancellationPopup.getAllContractDocumentTable().getDataprovider().getList();
		ArrayList<CancelContract> selectedCancelContractList = new ArrayList<CancelContract>();
		for(CancelContract canContract : cancelContractList){
			if(canContract.isRecordSelect()){
				selectedCancelContractList.add(canContract);
			}
		}
		System.out.println("Selected list size :" + selectedCancelContractList.size());
		
		gasync.savePOCancellationData(model, remark, loginUser, selectedCancelContractList, new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
				form.showDialogMessage("Please Try again...!");
				
			}

			@Override
			public void onSuccess(Void result) {
				
				if(!model.getStatus().equals(PurchaseOrder.CANCELLED)) {
					model.setStatus(PurchaseOrder.CANCELLED);
					form.getTbStatus().setValue(PurchaseOrder.CANCELLED);
					form.tadescription.setValue(model.getDescription() + "\n" + "Purchase Order Id ="
					+ model.getCount() + " "
					+ "Purchase Order Status = "+model.getStatus()
					+ "\n"
					+ "has been cancelled by " + loginUser
					+ " with remark" + "\n" + "Remark ="
					+ remark);
					
					form.hideWaitSymbol();
					form.showDialogMessage("Purchase Order Cancelled Successfully..!");
					form.setToViewState();
			}
				
			}
		});
	}

	private void saveNewVendorInfo() {
		form.showWaitSymbol();
		
		final Vendor vendor = new Vendor();
		vendor.setVendorName(newVenderPupup.getTbVendorName().getValue());
		vendor.setfullName(newVenderPupup.getTbPocName().getValue());
		vendor.setEmail(newVenderPupup.getEtbEmail().getValue());
		vendor.setCellNumber1(newVenderPupup.getPnbCellNumber().getValue());
		vendor.setPrimaryAddress(newVenderPupup.getAddressComposite().getValue());
		vendor.setSecondaryAddress(newVenderPupup.getAddressComposite().getValue());
		
		if(newVenderPupup.getTbGSTnumber().getValue()!=null&&!newVenderPupup.getTbGSTnumber().getValue().equals("")){
			ArticleType articalType=new ArticleType();
			articalType.setDocumentName("Invoice");
			articalType.setArticleTypeName("GSTIN");
			articalType.setArticleTypeValue(newVenderPupup.getTbGSTnumber().getValue());
			articalType.setArticlePrint("Yes");
			vendor.getArticleTypeDetails().add(articalType);
		}
		
		async.save(vendor, new AsyncCallback<ReturnFromServer>() {
			
			@Override
			public void onSuccess(final ReturnFromServer result) {
				// TODO Auto-generated method stub
				
				System.out.println("result ==="+result);

				Timer time = new Timer() {
					
					@Override
					public void run() {
						VendorDetails vendorInfo = new VendorDetails();
						vendorInfo.setVendorId(result.count);
						vendorInfo.setVendorName(vendor.getVendorName());
						vendorInfo.setPocName(vendor.getfullName());
						vendorInfo.setVendorPhone(vendor.getCellNumber1());
						vendorInfo.setVendorEmailId(vendor.getEmail());
						form.vendorTablePO.getDataprovider().getList().add(vendorInfo);
						form.hideWaitSymbol();
						newVenderPupup.getPopup().hide();
					}
					
				};
				
				time.schedule(2000);
				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				form.hideWaitSymbol();
			}
		});
	}

	private boolean ValidateVendorInfo() {
		System.out.println("vendor Name =="+newVenderPupup.getTbVendorName().getValue());
		if(newVenderPupup.getTbVendorName().getValue().equals("")){
			form.showDialogMessage("Please add Vendor Name");
			return false;
		}
		else if(newVenderPupup.getTbPocName().getValue().equals("")){
			form.showDialogMessage("Please add Vendor POC Name");
			return false;
		}
//		else if(newVenderPupup.getEtbEmail().getValue().equals("")){
//			form.showDialogMessage("Please add Vendor Email");
//			return false;
//		}
		else if(newVenderPupup.getPnbCellNumber().getValue()==null){
			form.showDialogMessage("Please add Cell Number");
			return false;
		}
		else if(newVenderPupup.addressComposite.getValue()==null || newVenderPupup.addressComposite.adressline1.getValue().equals("")
				|| newVenderPupup.addressComposite.country.getSelectedIndex()==0 || newVenderPupup.addressComposite.state.getSelectedIndex()==0
				|| newVenderPupup.addressComposite.city.getSelectedIndex()==0){
			
			form.showDialogMessage("Please add Complete Address");
			return false;
		}else{
			return true;
		}
	}
	
	/**
	 * Set Default Address. This method retrieves the address for warehouse.
	 * When warehouse is selected the address of warehouse is retrieved.
	 */

//	public void setDefaultAdd() {
//		if (form.getOblwarehouse().getValue() != null) {
//			WareHouse wareh = form.getOblwarehouse().getSelectedItem();
//			PurchaseOrder view = new PurchaseOrder();
//			view.setAdress(wareh.getAddress());
//			form.getDeliveryadd().setValue(view.getAdress());
//			form.getDeliveryadd().setEnable(false);
//		}
//	}

	

	/**
	 * On row change event of product table.
	 */
	private void productsOnChange() {
		System.out.println("Inside Product Change");
		form.showWaitSymbol();
		Timer timer = new Timer() {
			@Override
			public void run() {
				double totalExcludingTax = form.getProductTablePO().calculateTotalExcludingTax();
				totalExcludingTax = Double.parseDouble(nf.format(totalExcludingTax));
				form.getDbTotal().setValue(totalExcludingTax);

				System.out.println("DONE PROD CHANG 4");
				boolean chkSize = form.getProductTablePO().removeChargesOnDelete();
				if (chkSize == false) {
					form.getProductchargestable().connectToLocal();
				}
				System.out.println("DONE PROD CHANG 3");
				try {
					form.producttaxtable.connectToLocal();
					form.addProdTaxes();
					/**
					 *  Date 26-11-2018 By Vijay 
					 *  Des :- As per the Other Charges with Taxes Code updated
					 *  and old code commented
					 */
//					double totalIncludingTax = form.getDbTotal().getValue()+ form.getProducttaxtable().calculateTotalTaxes();
//					form.getDoamtincltax().setValue(Double.parseDouble(nf.format(totalIncludingTax)));
//					form.updateChargesTable();
					
					form.addOtherChargesInTaxTbl();
					form.getDoamtincltax().setValue(form.tblOtherCharges.calculateOtherChargesSum());
					/**
					 * ends here
					 */

				} catch (Exception e) {
					e.printStackTrace();
				}
				System.out.println("DONE PROD CHANG 2");
				/**
				 *  Date 26-11-2018 By Vijay 
				 *  Des :- As per the Other Charges with Taxes Code updated
				 *  and old code commented
				 */  
//				double netPay = fillNetPayable(form.getDoamtincltax().getValue());
				double totalIncludingTax = form.getDbTotal().getValue()+ form.getProducttaxtable().calculateTotalTaxes()+form.tblOtherCharges.calculateOtherChargesSum();
				double netPay = fillNetPayable(totalIncludingTax);
				/**
				 * ends here
				 */
				netPay = Math.round(netPay);
				int netPayable = (int) netPay;
				netPay = netPayable;
				form.getDonetpayamt().setValue(Double.parseDouble(nf.format(netPay)));
				System.out.println("DONE PROD CHANG 1");
				String approverName = "";
				if (form.tbApporverName.getValue() != null) {
					approverName = form.tbApporverName.getValue();
				}
				AppUtility.getApproverListAsPerTotalAmount(form.tbApporverName,"Purchase Order", approverName, form.getDonetpayamt().getValue());
				form.hideWaitSymbol();
				System.out.println("DONE PROD CHANG");
				
				/********  Date 21-08-2017 added by vijay for balance payment ********************/
				if(form.getDbpaymentrecieved().getValue()!=null){
					double balancePayment=netPay-form.getDbpaymentrecieved().getValue();
					System.out.println("balance payment ===="+balancePayment);
					form.getDbbalancepayment().setValue(Double.parseDouble(nf.format(balancePayment)));
				}else{
					form.getDbbalancepayment().setValue(Double.parseDouble(nf.format(netPay)));
				}
			}
		};
		timer.schedule(3000);
	}

	private void productsOnChangeStateNotSelected() {
		System.out.println("Inside Product Change State Not Selected");
		form.showWaitSymbol();
		Timer timer = new Timer() {
			@Override
			public void run() {
				double totalExcludingTax = form.getProductTablePO()
						.calculateTotalExcludingTax();
				totalExcludingTax = Double.parseDouble(nf
						.format(totalExcludingTax));
				form.getDbTotal().setValue(totalExcludingTax);

				boolean chkSize = form.getProductTablePO()
						.removeChargesOnDelete();
				if (chkSize == false) {
					form.getProductchargestable().connectToLocal();
					// form.getVendortbl().connectToLocal();
					// PurchaseOrderForm.povendors.clear();
				}

				try {
					form.producttaxtable.connectToLocal();
					form.addProdTaxesNoState();
					double totalIncludingTax = form.getDbTotal().getValue()
							+ form.getProducttaxtable().calculateTotalTaxes();
					form.getDoamtincltax().setValue(
							Double.parseDouble(nf.format(totalIncludingTax)));
					form.updateChargesTable();

				} catch (Exception e) {
					e.printStackTrace();
				}

				double netPay = fillNetPayable(form.getDoamtincltax()
						.getValue());
				netPay = Math.round(netPay);
				int netPayable = (int) netPay;
				netPay = netPayable;
				form.getDonetpayamt().setValue(
						Double.parseDouble(nf.format(netPay)));
				form.hideWaitSymbol();
			}
		};
		timer.schedule(3000);

	}

	private double fillNetPayable(double amtincltax) {
		double amtval = 0;
		if (form.getProductchargestable().getDataprovider().getList().size() == 0) {
			amtval = amtincltax;
		}
		if (form.getProductchargestable().getDataprovider().getList().size() != 0) {
			amtval = amtincltax
					+ form.getProductchargestable().calculateNetPayable();
		}
		amtval = Math.round(amtval);
		int retAmtVal = (int) amtval;
		amtval = retAmtVal;
		return amtval;
	}

	/**
	 * This method returns the name of state in which the company is located. It
	 * is retrieved form Company address in Company Entity
	 * 
	 * @return It returns company state
	 */
	// ********************************** Retrieve Company State
	// *********************************************
	public String retrieveCompanyState() {
		final MyQuerry querry = new MyQuerry();
		Filter tempfilter = new Filter();
		tempfilter.setQuerryString("companyId");
		tempfilter.setLongValue(model.getCompanyId());
		querry.getFilters().add(tempfilter);
		querry.setQuerryObject(new Company());
		Timer timer = new Timer() {
			@Override
			public void run() {
				async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onFailure(Throwable caught) {
						form.showDialogMessage("An Unexpected error occurred!");
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						for (SuperModel model : result) {
							Company compentity = (Company) model;
							retrCompnyState = compentity.getAddress().getState();
						}
						companyState = retrCompnyState.trim();
						System.out.println("sysouStatc" + companyState);
					}
				});
			}
		};
		timer.schedule(3000);
		return retrCompnyState;
	}

	// **************************** Retrieve Vendor State
	// ************************************

	public String retVendorState(final int venCount) {
		form.hideWaitSymbol();

		MyQuerry querry = new MyQuerry();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter tempfilter = null;

		tempfilter = new Filter();
		tempfilter.setQuerryString("companyId");
		tempfilter.setLongValue(model.getCompanyId());
		filtervec.add(tempfilter);

		tempfilter = new Filter();
		tempfilter.setQuerryString("count");
		tempfilter.setIntValue(venCount);
		filtervec.add(tempfilter);

		querry.getFilters().add(tempfilter);
		querry.setQuerryObject(new Vendor());
		form.showWaitSymbol();

		async.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onFailure(Throwable caught) {
						form.hideWaitSymbol();
						form.showDialogMessage("An Unexpected error occurred!");
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						form.hideWaitSymbol();
						for (SuperModel model : result) {
							Vendor vendorentity = (Vendor) model;
							retVenStateName = vendorentity.getPrimaryAddress().getState();
							vendorState = retVenStateName.trim();
							
							List<ProductDetailsPO> list = form.productTablePO.getDataprovider().getList();
							Branch branchEntity = form.branch.getSelectedItem();
							System.out.println("retVenStateName "+retVenStateName);
							updateTaxesDetails(list,branchEntity,retVenStateName);
//							List<ProductDetailsPO> updatedlist = PurchaseOrderPresenter.updateTaxesDetails(list,branchEntity,retVenStateName,true);
//							form.productTablePO.getDataprovider().setList(updatedlist);
							
							if(form.isWarehouseSelected()){
//								updateWarehouseAndCformStatus();
//								setVendorPriceToProduct(form.productTablePO.getValue(), venCount);
								productsOnChange();
							}else{

//								if (!vendorState.equals("")&& form.getDeliveryadd().getState().getSelectedIndex() != 0
//										&& !vendorState.trim().equals(form.getDeliveryadd().getState().getValue().trim())) {
//									//Date 4 -07-2017 added by vijay 
//									if(form.dbPODate.getValue()!=null && form.dbPODate.getValue().before(form.date)){
//										form.getCbcformlis().setEnabled(true);
//										form.validateCForm();
//									}
//								} else {
//									form.getCbcformlis().setSelectedIndex(0);
//									form.getOlbcstpercent().setSelectedIndex(0);
//									form.getCbcformlis().setEnabled(false);
//									form.getOlbcstpercent().setEnabled(false);
//								}
	
//								setVendorPriceToProduct(form.productTablePO.getValue(), venCount);
								productsOnChange();
							}
						}
					}

				});
		return retVenStateName;
	}

	// *********************************** Retrieve Vendor Count
	// *************************************
	public int retrieveVendorCount() {
		List<VendorDetails> vendorLis = form.getVendortbl().getDataprovider().getList();
		int vendorCount = 0;
		for (int i = 0; i < vendorLis.size(); i++) {
			if (vendorLis.get(i).getStatus() == true) {
				vendorCount = vendorLis.get(i).getVendorId();
				return vendorCount;
			}
		}
		return vendorCount;
	}

	// ******************************** Check Vendor State
	// *******************************************

	public boolean checkVendorState() {
		System.out.println("Vendor State" + vendorState);

		String addCompState = "";
		if (form.getDeliveryadd().getState().getSelectedIndex() != 0) {
			int indexVal = form.getDeliveryadd().getState().getSelectedIndex();
			addCompState = form.getDeliveryadd().getState()
					.getItemText(indexVal);
			System.out.println("Vendor State" + addCompState);
		}
		if (!addCompState.equals("")
				&& addCompState.trim().equals(vendorState.trim())
				&& !vendorState.equals("")) {
			return true;
		}
		return false;
	}

	// ******************************** Retrieving Product Price List
	// ************************************

	private List<String> retriveProductPriceList() {
		final List<String> list = new ArrayList<String>();

		MyQuerry querry = new MyQuerry();
		Filter filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(model.getCompanyId());
		querry.getFilters().add(filter);
		querry.setQuerryObject(new PriceList());
		async.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						if (result.size() != 0) {
							for (SuperModel m : result) {
								PriceList pr = (PriceList) m;
								list.add(pr.getProdCode());
							}
							System.out.println("List Size Inside Serach Qur:"
									+ list.size());
						}
					}

					@Override
					public void onFailure(Throwable caught) {
					}
				});

		return list;
	}

	private void getSummaryFromPurchaseOrderID() {

		MyQuerry querry = new MyQuerry();
		Company c = new Company();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;

		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);

		filter = new Filter();
		filter.setQuerryString("docID");
		filter.setIntValue(Integer.parseInt(form.getIbPOId().getValue().trim()));
		filtervec.add(filter);

		filter = new Filter();
		filter.setQuerryString("docType");
		filter.setStringValue(AppConstants.APPROVALPURCHASEORDER);
		filtervec.add(filter);

		querry.setFilters(filtervec);
		querry.setQuerryObject(new CancelSummary());
		async.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {

					@Override
					public void onFailure(Throwable caught) {
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						for (SuperModel smodel : result) {
							CancelSummary cancelEntity = (CancelSummary) smodel;
							summaryPanel = new PopupPanel(true);
							summaryPanel.add(summaryPopup);
							summaryPanel.show();
							summaryPanel.center();
							summaryPopup.setSummaryTable(cancelEntity
									.getCancelLis());
						}
					}
				});
	}

	private void reactOnCancelSummaryDownload() {
		System.out.println("inside download");

		ArrayList<CancelSummary> cancelsummarylist = new ArrayList<CancelSummary>();
		System.out.println("cancel summary list size"
				+ cancelsummarylist.size());
		List<CancellationSummary> cancel = summaryPopup.getSummaryTable()
				.getDataprovider().getList();
		System.out.println("cancel list size" + cancel.size());
		CancelSummary cancelEntity = new CancelSummary();
		cancelEntity.setCancelLis(cancel);

		cancelsummarylist.add(cancelEntity);
		System.out.println("cancel summary list size after......."
				+ cancelsummarylist.size());
		csvservice.setCancelSummaryDetails(cancelsummarylist,
				new AsyncCallback<Void>() {

					@Override
					public void onFailure(Throwable caught) {
						form.showDialogMessage("An unexpected error occurred");
					}

					@Override
					public void onSuccess(Void result) {
						String gwt = com.google.gwt.core.client.GWT
								.getModuleBaseURL();
						final String url = gwt + "csvservlet" + "?type=" + 82;
						Window.open(url, "test", "enabled");
					}
				});

	}

	private void checkForGRNStatus() {

		final int purchaseorderID = model.getCount();
		final String purchaseorderStatus = model.getStatus();

		MyQuerry querry = new MyQuerry();
		Company c = new Company();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("poNo");
		filter.setIntValue(purchaseorderID);
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new GRN());

		async.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {

					@Override
					public void onFailure(Throwable caught) {
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						int pic = 0;
						int request = 0;
						int inspection = 0;
						int message = 0;
						ArrayList<GRN> slist = new ArrayList<GRN>();

						for (SuperModel model : result) {
							GRN grnEntity = (GRN) model;
							grnEntity.setStatus(grnEntity.getStatus());
							grnEntity.setCount(grnEntity.getCount());
							slist.add(grnEntity);

							CancellationSummary table1 = new CancellationSummary();
							table1.setDocID(grnEntity.getCount());
							table1.setDocType(AppConstants.APPROVALGRN);
							table1.setRemark(cancelPopup.getRemark().getValue()
									.trim());
							table1.setLoggedInUser(LoginPresenter.loggedInUser);
							table1.setOldStatus(grnEntity.getStatus());
							table1.setNewStatus(BillingDocument.CANCELLED);
							table1.setSrNo(1);

							cancellis.add(table1);
						}
						for (int i = 0; i < slist.size(); i++) {

							if (slist.get(i).getStatus().equals(GRN.APPROVED)) {
								message = message + 1;
							} else if (slist.get(i).getStatus()
									.equals(GRN.CREATED)
									|| slist.get(i).getStatus()
											.equals(GRN.REJECTED)) {
								pic = pic + 1;
							} else if (slist.get(i).getStatus()
									.equals(GRN.REQUESTED)) {
								request = request + 1;
							} else if (slist.get(i).getStatus()
									.equals(GRN.INSPECTIONAPPROVED)
									|| slist.get(i).getStatus()
											.equals(GRN.INSPECTIONREQUESTED)) {
								inspection = inspection + 1;
							}

						}

						if (message > 0) {
							form.showDialogMessage("GRN status is approved. Cancel GRN first");
						} else {
							System.out.println("Else SYSO");

							if ((pic > 0) || (request > 0) || (inspection > 0)) {

								if (inspection > 0) {
									for (int i = 0; i < inspection; i++) {
										MyQuerry querry = new MyQuerry();
										Company c = new Company();
										Vector<Filter> filtervec = new Vector<Filter>();
										Filter filter = null;
										filter = new Filter();
										filter.setQuerryString("companyId");
										filter.setLongValue(c.getCompanyId());
										filtervec.add(filter);
										filter = new Filter();
										filter.setQuerryString("inspectionGrnId");
										filter.setIntValue(slist.get(i)
												.getCount());
										filtervec.add(filter);
										querry.setFilters(filtervec);
										querry.setQuerryObject(new Inspection());
										async.getSearchResult(
												querry,
												new AsyncCallback<ArrayList<SuperModel>>() {

													@Override
													public void onFailure(
															Throwable caught) {
													}

													@Override
													public void onSuccess(
															ArrayList<SuperModel> result) {

														for (SuperModel smodel : result) {
															Inspection inspectionEntity = (Inspection) smodel;

															CancellationSummary table1 = new CancellationSummary();
															table1.setDocID(inspectionEntity
																	.getCount());
															table1.setDocType(AppConstants.APPROVALINSPECTION);
															table1.setRemark(cancelPopup
																	.getRemark()
																	.getValue()
																	.trim());
															table1.setLoggedInUser(LoginPresenter.loggedInUser);
															table1.setOldStatus(inspectionEntity
																	.getStatus());
															table1.setNewStatus(Inspection.CANCELLED);
															table1.setSrNo(1);

															cancellis
																	.add(table1);

															if (inspectionEntity
																	.getStatus()
																	.trim()
																	.equals(Inspection.CREATED)) {

																inspectionEntity
																		.setStatus(Inspection.CANCELLED);
																inspectionEntity
																		.setInspectionDescription("Purchase Order ID ="
																				+ purchaseorderID
																				+ " "
																				+ "purchase order status ="
																				+ purchaseorderStatus
																				+ "\n"
																				+ "has been cancelled by "
																				+ LoginPresenter.loggedInUser
																				+ " with remark "
																				+ "\n"
																				+ "Remark ="
																				+ cancelPopup
																						.getRemark()
																						.getValue()
																						.trim());

																async.save(
																		inspectionEntity,
																		new AsyncCallback<ReturnFromServer>() {

																			@Override
																			public void onFailure(
																					Throwable caught) {
																				form.showDialogMessage("An Unexpected Error occured !");
																			}

																			@Override
																			public void onSuccess(
																					ReturnFromServer result) {
																			}
																		});
															}

															if (inspectionEntity
																	.getStatus()
																	.trim()
																	.equals(Inspection.REJECTED)) {

																inspectionEntity
																		.setStatus(Inspection.CANCELLED);
																inspectionEntity
																		.setInspectionDescription("Purchase Order ID ="
																				+ purchaseorderID
																				+ " "
																				+ "purchase order status ="
																				+ purchaseorderStatus
																				+ "\n"
																				+ "has been cancelled by "
																				+ LoginPresenter.loggedInUser
																				+ " with remark "
																				+ "\n"
																				+ "Remark ="
																				+ cancelPopup
																						.getRemark()
																						.getValue()
																						.trim());

																async.save(
																		inspectionEntity,
																		new AsyncCallback<ReturnFromServer>() {

																			@Override
																			public void onFailure(
																					Throwable caught) {
																				form.showDialogMessage("An Unexpected Error occured !");
																			}

																			@Override
																			public void onSuccess(
																					ReturnFromServer result) {
																			}
																		});
															}

															if (inspectionEntity
																	.getStatus()
																	.trim()
																	.equals(Inspection.APPROVED)) {

																inspectionEntity
																		.setStatus(Inspection.CANCELLED);
																inspectionEntity
																		.setInspectionDescription("Purchase Order ID ="
																				+ purchaseorderID
																				+ " "
																				+ "purchase order status ="
																				+ purchaseorderStatus
																				+ "\n"
																				+ "has been cancelled by "
																				+ LoginPresenter.loggedInUser
																				+ " with remark "
																				+ "\n"
																				+ "Remark ="
																				+ cancelPopup
																						.getRemark()
																						.getValue()
																						.trim());

																async.save(
																		inspectionEntity,
																		new AsyncCallback<ReturnFromServer>() {

																			@Override
																			public void onFailure(
																					Throwable caught) {
																				form.showDialogMessage("An Unexpected Error occured !");
																			}

																			@Override
																			public void onSuccess(
																					ReturnFromServer result) {
																			}
																		});
															}

															if (inspectionEntity
																	.getStatus()
																	.trim()
																	.equals(Inspection.REQUESTED)) {

																inspectionEntity
																		.setStatus(Inspection.CANCELLED);
																inspectionEntity
																		.setInspectionDescription("Purchase Order ID ="
																				+ purchaseorderID
																				+ " "
																				+ "purchase order status ="
																				+ purchaseorderStatus
																				+ "\n"
																				+ "has been cancelled by "
																				+ LoginPresenter.loggedInUser
																				+ " with remark "
																				+ "\n"
																				+ "Remark ="
																				+ cancelPopup
																						.getRemark()
																						.getValue()
																						.trim());

																async.save(
																		inspectionEntity,
																		new AsyncCallback<ReturnFromServer>() {

																			@Override
																			public void onFailure(
																					Throwable caught) {
																				form.showDialogMessage("An Unexpected Error occured !");
																			}

																			@Override
																			public void onSuccess(
																					ReturnFromServer result) {
																			}
																		});

																MyQuerry querry = new MyQuerry();
																Company c = new Company();
																Vector<Filter> filtervec = new Vector<Filter>();
																Filter filter = null;
																filter = new Filter();
																filter.setQuerryString("companyId");
																filter.setLongValue(c
																		.getCompanyId());
																filtervec
																		.add(filter);
																filter = new Filter();
																filter.setQuerryString("businessprocessId");
																filter.setIntValue(inspectionEntity
																		.getCount());
																filtervec
																		.add(filter);
																filter = new Filter();
																filter.setQuerryString("businessprocesstype");
																filter.setStringValue(ApproverFactory.INSPECTION);
																filtervec
																		.add(filter);
																querry.setFilters(filtervec);
																querry.setQuerryObject(new Approvals());
																async.getSearchResult(
																		querry,
																		new AsyncCallback<ArrayList<SuperModel>>() {

																			@Override
																			public void onFailure(
																					Throwable caught) {
																			}

																			@Override
																			public void onSuccess(
																					ArrayList<SuperModel> result) {

																				for (SuperModel smodel : result) {
																					Approvals approval = (Approvals) smodel;

																					approval.setStatus(Approvals.CANCELLED);
																					approval.setRemark("purchase order Id ="
																							+ purchaseorderID
																							+ " "
																							+ "purchase order status ="
																							+ purchaseorderStatus
																							+ "\n"
																							+ "has been cancelled by "
																							+ LoginPresenter.loggedInUser
																							+ " with remark "
																							+ "\n"
																							+ "Remark ="
																							+ cancelPopup
																									.getRemark()
																									.getValue()
																									.trim());

																					async.save(
																							approval,
																							new AsyncCallback<ReturnFromServer>() {

																								@Override
																								public void onFailure(
																										Throwable caught) {
																									form.showDialogMessage("An Unexpected Error occured !");
																								}

																								@Override
																								public void onSuccess(
																										ReturnFromServer result) {
																								}
																							});
																				}
																			}
																		});

															}

														}
													}
												});
									}
								}

								if (request > 0) {
									for (int i = 0; i < request; i++) {

										MyQuerry querry = new MyQuerry();
										Company c = new Company();
										Vector<Filter> filtervec = new Vector<Filter>();
										Filter filter = null;
										filter = new Filter();
										filter.setQuerryString("companyId");
										filter.setLongValue(c.getCompanyId());
										filtervec.add(filter);
										filter = new Filter();
										filter.setQuerryString("businessprocessId");
										filter.setIntValue(slist.get(i)
												.getCount());
										filtervec.add(filter);
										filter = new Filter();
										filter.setQuerryString("businessprocesstype");
										filter.setStringValue(ApproverFactory.GRN);
										filtervec.add(filter);
										querry.setFilters(filtervec);
										querry.setQuerryObject(new Approvals());
										async.getSearchResult(
												querry,
												new AsyncCallback<ArrayList<SuperModel>>() {

													@Override
													public void onFailure(
															Throwable caught) {
													}

													@Override
													public void onSuccess(
															ArrayList<SuperModel> result) {

														for (SuperModel smodel : result) {
															Approvals approval = (Approvals) smodel;

															approval.setStatus(Approvals.CANCELLED);
															approval.setRemark("purchase order Id ="
																	+ purchaseorderID
																	+ " "
																	+ "purchase order status ="
																	+ purchaseorderStatus
																	+ "\n"
																	+ "has been cancelled by "
																	+ LoginPresenter.loggedInUser
																	+ " with remark "
																	+ "\n"
																	+ "Remark ="
																	+ cancelPopup
																			.getRemark()
																			.getValue()
																			.trim());

															async.save(
																	approval,
																	new AsyncCallback<ReturnFromServer>() {

																		@Override
																		public void onFailure(
																				Throwable caught) {
																			form.showDialogMessage("An Unexpected Error occured !");
																		}

																		@Override
																		public void onSuccess(
																				ReturnFromServer result) {
																		}
																	});
														}
													}
												});
									}
								}

								MyQuerry querry = new MyQuerry();
								Company c = new Company();
								Vector<Filter> filtervec = new Vector<Filter>();
								Filter filter = null;
								filter = new Filter();
								filter.setQuerryString("companyId");
								filter.setLongValue(c.getCompanyId());
								filtervec.add(filter);
								filter = new Filter();
								filter.setQuerryString("contractCount");
								filter.setIntValue(purchaseorderID);
								filtervec.add(filter);
								filter = new Filter();
								filter.setQuerryString("typeOfOrder");
								filter.setStringValue(AppConstants.ORDERTYPEFORPURCHASE);
								filtervec.add(filter);
								querry.setFilters(filtervec);
								querry.setQuerryObject(new BillingDocument());

								async.getSearchResult(
										querry,
										new AsyncCallback<ArrayList<SuperModel>>() {

											@Override
											public void onFailure(
													Throwable caught) {
											}

											@Override
											public void onSuccess(
													ArrayList<SuperModel> result) {
												int req = 0;
												ArrayList<BillingDocument> slist = new ArrayList<BillingDocument>();

												for (SuperModel model : result) {
													BillingDocument billingEntity = (BillingDocument) model;
													billingEntity
															.setStatus(billingEntity
																	.getStatus());
													slist.add(billingEntity);

													if (billingEntity
															.getStatus()
															.equals(BillingDocument.BILLINGINVOICED)) {
														CancellationSummary table1 = new CancellationSummary();
														table1.setDocID(billingEntity
																.getCount());
														table1.setDocType(AppConstants.APPROVALBILLINGDETAILS);
														table1.setRemark(cancelPopup
																.getRemark()
																.getValue()
																.trim());
														table1.setLoggedInUser(LoginPresenter.loggedInUser);
														table1.setOldStatus(billingEntity
																.getStatus());
														table1.setNewStatus(BillingDocument.BILLINGINVOICED);
														table1.setSrNo(1);

														cancellis.add(table1);
													} else {
														CancellationSummary table1 = new CancellationSummary();
														table1.setDocID(billingEntity
																.getCount());
														table1.setDocType(AppConstants.APPROVALBILLINGDETAILS);
														table1.setRemark(cancelPopup
																.getRemark()
																.getValue()
																.trim());
														table1.setLoggedInUser(LoginPresenter.loggedInUser);
														table1.setOldStatus(billingEntity
																.getStatus());
														table1.setNewStatus(BillingDocument.CANCELLED);
														table1.setSrNo(1);

														cancellis.add(table1);
													}

												}

												int cnt = 0;
												int flag = 0;
												for (int i = 0; i < slist
														.size(); i++) {

													if (slist
															.get(i)
															.getStatus()
															.equals(BillingDocument.APPROVED)) {
														flag = flag + 1;
													} else if (slist
															.get(i)
															.getStatus()
															.equals(BillingDocument.BILLINGINVOICED)) {
														cnt = cnt + 1;

													} else if (slist
															.get(i)
															.getStatus()
															.equals(BillingDocument.CREATED)) {
														flag = flag + 1;
													} else if (slist
															.get(i)
															.getStatus()
															.equals(BillingDocument.REJECTED)) {
														flag = flag + 1;
													} else if (slist
															.get(i)
															.getStatus()
															.equals(BillingDocument.REQUESTED)) {
														req = req + 1;
													}

												}
												if (cnt > 0) {

													MyQuerry querry = new MyQuerry();
													Company c = new Company();
													Vector<Filter> filtervec = new Vector<Filter>();
													Filter filter = null;
													filter = new Filter();
													filter.setQuerryString("companyId");
													filter.setLongValue(c
															.getCompanyId());
													filtervec.add(filter);
													filter = new Filter();
													filter.setQuerryString("contractCount");
													filter.setIntValue(purchaseorderID);
													filtervec.add(filter);
													filter = new Filter();
													filter.setQuerryString("typeOfOrder");
													filter.setStringValue(AppConstants.ORDERTYPEFORPURCHASE);
													filtervec.add(filter);
													querry.setFilters(filtervec);
													querry.setQuerryObject(new Invoice());
													async.getSearchResult(
															querry,
															new AsyncCallback<ArrayList<SuperModel>>() {

																@Override
																public void onFailure(
																		Throwable caught) {
																}

																@Override
																public void onSuccess(
																		ArrayList<SuperModel> result) {
																	int cnt = 0;
																	int requested = 0;
																	ArrayList<Invoice> slist = new ArrayList<Invoice>();

																	for (SuperModel model : result) {
																		Invoice invoiceEntity = (Invoice) model;

																		invoiceEntity
																				.setStatus(invoiceEntity
																						.getStatus());
																		invoiceEntity
																				.setContractCount(invoiceEntity
																						.getContractCount());
																		invoiceEntity
																				.setInvoiceCount(invoiceEntity
																						.getCount());
																		invoiceEntity
																				.setStatus(invoiceEntity
																						.getStatus());
																		slist.add(invoiceEntity);

																		if (invoiceEntity
																				.getStatus()
																				.equals(Invoice.APPROVED)) {
																			CancellationSummary table2 = new CancellationSummary();

																			table2.setDocID(invoiceEntity
																					.getCount());
																			table2.setDocType(AppConstants.APPROVALINVOICEDETAILS);
																			table2.setRemark(cancelPopup
																					.getRemark()
																					.getValue()
																					.trim());
																			table2.setLoggedInUser(LoginPresenter.loggedInUser);
																			table2.setOldStatus(invoiceEntity
																					.getStatus());
																			table2.setNewStatus(Invoice.APPROVED);
																			table2.setSrNo(2);

																			cancellis
																					.add(table2);
																		} else {
																			CancellationSummary table2 = new CancellationSummary();

																			table2.setDocID(invoiceEntity
																					.getCount());
																			table2.setDocType(AppConstants.APPROVALINVOICEDETAILS);
																			table2.setRemark(cancelPopup
																					.getRemark()
																					.getValue()
																					.trim());
																			table2.setLoggedInUser(LoginPresenter.loggedInUser);
																			table2.setOldStatus(invoiceEntity
																					.getStatus());
																			table2.setNewStatus(Invoice.CANCELLED);
																			table2.setSrNo(2);

																			cancellis
																					.add(table2);
																		}
																	}

																	for (int i = 0; i < slist
																			.size(); i++) {

																		if (purchaseorderID == slist
																				.get(i)
																				.getContractCount())
																			;
																		{
																			if (slist
																					.get(i)
																					.getStatus()
																					.equals(Invoice.CREATED)) {
																			}

																			else if (slist
																					.get(i)
																					.getStatus()
																					.equals(Invoice.APPROVED)) {
																				cnt = cnt + 1;
																			} else if (slist
																					.get(i)
																					.getStatus()
																					.equals(Invoice.REJECTED)) {
																			} else if (slist
																					.get(i)
																					.getStatus()
																					.equals(Invoice.REQUESTED)) {
																				requested = requested + 1;
																			}

																		}

																	}
																	if (cnt > 0) {

																		MyQuerry querry = new MyQuerry();
																		Company c = new Company();
																		Vector<Filter> filtervec = new Vector<Filter>();
																		Filter filter = null;
																		filter = new Filter();
																		filter.setQuerryString("companyId");
																		filter.setLongValue(c
																				.getCompanyId());
																		filtervec
																				.add(filter);
																		filter = new Filter();
																		filter.setQuerryString("contractCount");
																		filter.setIntValue(purchaseorderID);
																		filtervec
																				.add(filter);
																		filter = new Filter();
																		filter.setQuerryString("typeOfOrder");
																		filter.setStringValue(AppConstants.ORDERTYPEFORPURCHASE);
																		filtervec
																				.add(filter);
																		querry.setFilters(filtervec);
																		querry.setQuerryObject(new CustomerPayment());
																		async.getSearchResult(
																				querry,
																				new AsyncCallback<ArrayList<SuperModel>>() {

																					@Override
																					public void onFailure(
																							Throwable caught) {
																					}

																					@Override
																					public void onSuccess(
																							ArrayList<SuperModel> result) {

																						int cnt = 0;

																						ArrayList<CustomerPayment> slist = new ArrayList<CustomerPayment>();

																						for (SuperModel model : result) {
																							CustomerPayment custPayEntity = (CustomerPayment) model;

																							custPayEntity
																									.setStatus(custPayEntity
																											.getStatus());
																							custPayEntity
																									.setInvoiceCount(custPayEntity
																											.getInvoiceCount());
																							custPayEntity
																									.setContractCount(custPayEntity
																											.getContractCount());
																							slist.add(custPayEntity);

																							if (custPayEntity
																									.getStatus()
																									.equals(CustomerPayment.PAYMENTCLOSED)) {
																								CancellationSummary table2 = new CancellationSummary();

																								table2.setDocID(custPayEntity
																										.getCount());
																								table2.setDocType("Payment Details");
																								table2.setRemark(cancelPopup
																										.getRemark()
																										.getValue()
																										.trim());
																								table2.setLoggedInUser(LoginPresenter.loggedInUser);
																								table2.setOldStatus(custPayEntity
																										.getStatus());
																								table2.setNewStatus(CustomerPayment.PAYMENTCLOSED);
																								table2.setSrNo(2);

																								cancellis
																										.add(table2);
																							} else {
																								CancellationSummary table2 = new CancellationSummary();

																								table2.setDocID(custPayEntity
																										.getCount());
																								table2.setDocType("Payment Details");
																								table2.setRemark(cancelPopup
																										.getRemark()
																										.getValue()
																										.trim());
																								table2.setLoggedInUser(LoginPresenter.loggedInUser);
																								table2.setOldStatus(custPayEntity
																										.getStatus());
																								table2.setNewStatus(Invoice.CANCELLED);
																								table2.setSrNo(2);

																								cancellis
																										.add(table2);
																							}
																						}

																						for (int i = 0; i < slist
																								.size(); i++) {

																							if (purchaseorderID == slist
																									.get(i)
																									.getContractCount())
																								;
																							{
																								if (slist
																										.get(i)
																										.getStatus()
																										.equals("Created")) {
																									cnt = cnt + 1;
																								}

																								else if (slist
																										.get(i)
																										.getStatus()
																										.equals("Closed")) {

																								}
																							}
																						}

																						if (cnt > 0) {
																							cancelGRNDetails();
																							cancelBillingDetails();
																							cancelInvoiceDetails();
																							cancelPaymentDetails();
																							changeStatus(PurchaseOrder.CANCELLED);
																						} else {

																							cancelGRNDetails();
																							cancelBillingDetails();
																							cancelInvoiceDetails();
																							cancelPaymentDetails();
																							changeStatus(PurchaseOrder.CANCELLED);
																						}
																					}
																				});
																	} else {
																		if (requested > 0) {
																			cancelGRNDetails();
																			cancelBillingDetails();
																			cancelInvoiceDetails();
																			changeStatus(PurchaseOrder.CANCELLED);
																		}
																	}

																}
															});
													// **************************************************************
												} else {

													if ((flag > 0) || (req > 0)) {
														cancelGRNDetails();
														cancelBillingDetails();
														changeStatus(PurchaseOrder.CANCELLED);

													}
												}
											}
										});
							}
						}

					}
				});

	}

	public void changeStatus(final String status) {
		int purchaseOrderId = Integer.parseInt(form.getIbPOId().getValue());
		String purchaseOrderStatus = form.getTbStatus().getValue().trim();
		model.setStatus(status);
		model.setDescription(model.getDescription() + "\n"
				+ "Purchase Order Id =" + purchaseOrderId + " "
				+ "Purchase Order Status =" + purchaseOrderStatus + "\n"
				+ "has been cancelled by " + LoginPresenter.loggedInUser
				+ " With remark." + "\n" + "Remark ="
				+ cancelPopup.getRemark().getValue().trim());
		async.save(model, new AsyncCallback<ReturnFromServer>() {
			@Override
			public void onFailure(Throwable caught) {
				form.showDialogMessage("An Unexpected Error occured !");
			}

			@Override
			public void onSuccess(ReturnFromServer result) {

				form.setToViewState();
				form.tbStatus.setText(status);

				CancellationSummary table5 = new CancellationSummary();

				table5.setDocID(model.getCount());
				table5.setDocType(AppConstants.APPROVALPURCHASEORDER);
				table5.setRemark(cancelPopup.getRemark().getValue().trim());
				table5.setLoggedInUser(LoginPresenter.loggedInUser);
				table5.setOldStatus(PurchaseOrder.APPROVED);
				table5.setNewStatus(model.getStatus());
				table5.setSrNo(5);

				cancellis.add(table5);
				CancellationSummaryPopUpTable popupTable = new CancellationSummaryPopUpTable();
				popupTable.setValue(cancellis);

				List<CancellationSummary> cancellistsetting1 = new ArrayList<CancellationSummary>();
				for (int i = 0; i < cancellis.size(); i++) {
					CancellationSummary sum = new CancellationSummary();
					sum.setSrNo(i + 1);
					sum.setDocID(cancellis.get(i).getDocID());
					sum.setDocType(cancellis.get(i).getDocType());
					sum.setLoggedInUser(cancellis.get(i).getLoggedInUser());
					sum.setOldStatus(cancellis.get(i).getOldStatus());
					sum.setNewStatus(cancellis.get(i).getNewStatus());
					sum.setRemark(cancellis.get(i).getRemark());

					cancellistsetting1.add(sum);
				}
				summaryPanel = new PopupPanel(true);
				summaryPanel.add(summaryPopup);
				summaryPanel.show();
				summaryPanel.center();
				summaryPopup.setSummaryTable(cancellistsetting1);

				CancelSummary cancelEntity = new CancelSummary();

				cancelEntity.setCancelLis(cancellistsetting1);
				cancelEntity.setDocType(AppConstants.APPROVALPURCHASEORDER);
				cancelEntity.setDocID(model.getCount());

				try {
					async.save(cancelEntity,
							new AsyncCallback<ReturnFromServer>() {

								@Override
								public void onFailure(Throwable caught) {
									form.showDialogMessage("An Unexpected Error occured !");
									//
								}

								@Override
								public void onSuccess(ReturnFromServer result) {
								}
							});
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	private void cancelGRNDetails() {

		final int purchaseorderID = model.getCount();
		final String purchaseorderStatus = model.getStatus();

		MyQuerry querry = new MyQuerry();
		Company c = new Company();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("poNo");
		filter.setIntValue(purchaseorderID);
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new GRN());
		System.out.println("querry completed......");

		async.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {

					@Override
					public void onFailure(Throwable caught) {
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {

						for (SuperModel model : result) {
							GRN grnEntity = (GRN) model;

							if (grnEntity.getStatus().equals(GRN.CREATED)) {

								grnEntity.setStatus(GRN.CANCELLED);
								grnEntity.setDescription("purchase order Id ="
										+ purchaseorderID
										+ " "
										+ "purchase order status ="
										+ purchaseorderStatus
										+ "\n"
										+ "has been cancelled by "
										+ LoginPresenter.loggedInUser
										+ " with remark "
										+ "\n"
										+ "Remark ="
										+ cancelPopup.getRemark().getValue()
												.trim() + " "
										+ "Cancellation Date =" + new Date());
								async.save(grnEntity,
										new AsyncCallback<ReturnFromServer>() {

											@Override
											public void onFailure(
													Throwable caught) {
												form.showDialogMessage("An Unexpected Error occured !");

											}

											@Override
											public void onSuccess(
													ReturnFromServer result) {
											}
										});
							}

							if (grnEntity.getStatus().equals(
									GRN.INSPECTIONREQUESTED)) {

								grnEntity.setStatus(GRN.CANCELLED);
								grnEntity.setDescription("Purchase Order Id ="
										+ purchaseorderID
										+ ","
										+ "purchase order status ="
										+ purchaseorderStatus
										+ "\n"
										+ "has been cancelled by "
										+ LoginPresenter.loggedInUser
										+ " with remark "
										+ "\n"
										+ "Remark ="
										+ cancelPopup.getRemark().getValue()
												.trim() + " "
										+ "Cancellation Date =" + new Date());
								async.save(grnEntity,
										new AsyncCallback<ReturnFromServer>() {

											@Override
											public void onFailure(
													Throwable caught) {
												form.showDialogMessage("An Unexpected Error occured !");

											}

											@Override
											public void onSuccess(
													ReturnFromServer result) {
											}
										});
							}

							if (grnEntity.getStatus().equals(
									GRN.INSPECTIONAPPROVED)) {

								grnEntity.setStatus(GRN.CANCELLED);
								grnEntity.setDescription("Purchase Order Id ="
										+ purchaseorderID
										+ " "
										+ "purchase order status ="
										+ purchaseorderStatus
										+ "\n"
										+ "has been cancelled by "
										+ LoginPresenter.loggedInUser
										+ " with remark "
										+ "\n"
										+ "Remark ="
										+ cancelPopup.getRemark().getValue()
												.trim() + " "
										+ "Cancellation Date =" + new Date());
								async.save(grnEntity,
										new AsyncCallback<ReturnFromServer>() {

											@Override
											public void onFailure(
													Throwable caught) {
												form.showDialogMessage("An Unexpected Error occured !");

											}

											@Override
											public void onSuccess(
													ReturnFromServer result) {
											}
										});
							}

							if (grnEntity.getStatus().equals(GRN.REJECTED)) {

								grnEntity.setStatus(GRN.CANCELLED);
								grnEntity.setDescription("Purchase Order Id ="
										+ purchaseorderID
										+ " "
										+ "purchase order status ="
										+ purchaseorderStatus
										+ "\n"
										+ "has been cancelled by "
										+ LoginPresenter.loggedInUser
										+ " with remark "
										+ "\n"
										+ "Remark ="
										+ cancelPopup.getRemark().getValue()
												.trim() + " "
										+ "Cancellation Date =" + new Date());
								async.save(grnEntity,
										new AsyncCallback<ReturnFromServer>() {

											@Override
											public void onFailure(
													Throwable caught) {
												form.showDialogMessage("An Unexpected Error occured !");

											}

											@Override
											public void onSuccess(
													ReturnFromServer result) {
											}
										});
							}

							if (grnEntity.getStatus().equals(GRN.REQUESTED)) {

								grnEntity.setStatus(GRN.CANCELLED);
								grnEntity.setDescription("purchase order Id ="
										+ purchaseorderID
										+ " "
										+ "purchase order status ="
										+ purchaseorderStatus
										+ "\n"
										+ "has been cancelled by "
										+ LoginPresenter.loggedInUser
										+ " with remark "
										+ "\n"
										+ "Remark ="
										+ cancelPopup.getRemark().getValue()
												.trim() + " "
										+ "Cancellation Date =" + new Date());
								async.save(grnEntity,
										new AsyncCallback<ReturnFromServer>() {

											@Override
											public void onFailure(
													Throwable caught) {
												form.showDialogMessage("An Unexpected Error occured !");

											}

											@Override
											public void onSuccess(
													ReturnFromServer result) {
											}
										});

								MyQuerry querry = new MyQuerry();
								Company c = new Company();
								Vector<Filter> filtervec = new Vector<Filter>();
								Filter filter = null;
								filter = new Filter();
								filter.setQuerryString("companyId");
								filter.setLongValue(c.getCompanyId());
								filtervec.add(filter);
								filter = new Filter();
								filter.setQuerryString("businessprocessId");
								filter.setIntValue(grnEntity.getCount());
								filtervec.add(filter);
								filter = new Filter();
								filter.setQuerryString("businessprocesstype");
								filter.setStringValue(AppConstants.APPROVALGRN);
								filtervec.add(filter);
								querry.setFilters(filtervec);
								querry.setQuerryObject(new Approvals());
								async.getSearchResult(
										querry,
										new AsyncCallback<ArrayList<SuperModel>>() {

											@Override
											public void onFailure(
													Throwable caught) {
											}

											@Override
											public void onSuccess(
													ArrayList<SuperModel> result) {

												for (SuperModel smodel : result) {
													Approvals approval = (Approvals) smodel;

													approval.setStatus(Approvals.CANCELLED);
													approval.setRemark("Purchase Order Id ="
															+ purchaseorderID
															+ " "
															+ "purchase order status ="
															+ purchaseorderStatus
															+ "\n"
															+ "has been cancelled by "
															+ LoginPresenter.loggedInUser
															+ " with remark "
															+ "\n"
															+ "Remark ="
															+ cancelPopup
																	.getRemark()
																	.getValue()
																	.trim()
															+ " "
															+ "Cancellation Date ="
															+ new Date());

													async.save(
															approval,
															new AsyncCallback<ReturnFromServer>() {

																@Override
																public void onFailure(
																		Throwable caught) {
																	form.showDialogMessage("An Unexpected Error occured !");
																}

																@Override
																public void onSuccess(
																		ReturnFromServer result) {

																}
															});

												}
											}
										});
							}
						}
					}
				});
	}

	private void cancelBillingDetails() {

		final int purchaseorderID = model.getCount();
		final String purchaseorderStatus = model.getStatus();

		MyQuerry querry = new MyQuerry();
		Company c = new Company();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("contractCount");
		filter.setIntValue(purchaseorderID);
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("typeOfOrder");
		filter.setStringValue(AppConstants.ORDERTYPEFORPURCHASE);
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new BillingDocument());
		async.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {

					@Override
					public void onFailure(Throwable caught) {
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {

						for (SuperModel smodel : result) {
							BillingDocument biil = (BillingDocument) smodel;

							if (biil.getStatus()
									.equals(BillingDocument.CREATED)) {

								biil.setStatus(BillingDocument.CANCELLED);
								biil.setComment("Purchase Order Id ="
										+ purchaseorderID
										+ " "
										+ "purchase order status ="
										+ purchaseorderStatus
										+ "\n"
										+ "has been cancelled by "
										+ LoginPresenter.loggedInUser
										+ " with remark "
										+ "\n"
										+ "Remark ="
										+ cancelPopup.getRemark().getValue()
												.trim() + " "
										+ "Cancellation Date =" + new Date());
								async.save(biil,
										new AsyncCallback<ReturnFromServer>() {

											@Override
											public void onFailure(
													Throwable caught) {
												form.showDialogMessage("An Unexpected Error occured !");
												//
											}

											@Override
											public void onSuccess(
													ReturnFromServer result) {
											}
										});
							}

							if (biil.getStatus().equals(
									BillingDocument.APPROVED)) {

								biil.setStatus(BillingDocument.CANCELLED);
								biil.setComment("Purchase Order Id ="
										+ purchaseorderID
										+ " "
										+ "purchase order status ="
										+ purchaseorderStatus
										+ "\n"
										+ "has been cancelled by "
										+ LoginPresenter.loggedInUser
										+ " with remark "
										+ "\n"
										+ "Remark ="
										+ cancelPopup.getRemark().getValue()
												.trim() + " "
										+ "Cancellation Date =" + new Date());
								async.save(biil,
										new AsyncCallback<ReturnFromServer>() {

											@Override
											public void onFailure(
													Throwable caught) {
												form.showDialogMessage("An Unexpected Error occured !");
												//
											}

											@Override
											public void onSuccess(
													ReturnFromServer result) {
											}
										});
							}
							if (biil.getStatus().equals(
									BillingDocument.BILLINGINVOICED)) {
								biil.setRenewFlag(true);
								biil.setStatus(BillingDocument.BILLINGINVOICED);
								biil.setComment("purchase order Id ="
										+ purchaseorderID
										+ " "
										+ "purchase order status ="
										+ purchaseorderStatus
										+ "\n"
										+ "has been cancelled by "
										+ LoginPresenter.loggedInUser
										+ " with remark "
										+ "\n"
										+ "Remark ="
										+ cancelPopup.getRemark().getValue()
												.trim() + " "
										+ "Cancellation Date =" + new Date());
								async.save(biil,
										new AsyncCallback<ReturnFromServer>() {

											@Override
											public void onFailure(
													Throwable caught) {
												form.showDialogMessage("An Unexpected Error occured !");
												//
											}

											@Override
											public void onSuccess(
													ReturnFromServer result) {
											}
										});
							}

							if (biil.getStatus().equals(
									BillingDocument.REJECTED)) {

								biil.setStatus(BillingDocument.CANCELLED);
								biil.setComment("Purchase Order Id ="
										+ purchaseorderID
										+ ","
										+ "purchase order status ="
										+ purchaseorderStatus
										+ "\n"
										+ "has been cancelled by "
										+ LoginPresenter.loggedInUser
										+ " with remark "
										+ "\n"
										+ "Remark ="
										+ cancelPopup.getRemark().getValue()
												.trim() + " "
										+ "Cancellation Date =" + new Date());
								async.save(biil,
										new AsyncCallback<ReturnFromServer>() {

											@Override
											public void onFailure(
													Throwable caught) {
												form.showDialogMessage("An Unexpected Error occured !");
												//
											}

											@Override
											public void onSuccess(
													ReturnFromServer result) {
											}
										});
							}

							if (biil.getStatus().equals(
									BillingDocument.REQUESTED)) {

								biil.setStatus(BillingDocument.CANCELLED);
								biil.setComment("Purchase Order Id ="
										+ purchaseorderID
										+ " "
										+ "purchase order status ="
										+ purchaseorderStatus
										+ "\n"
										+ "has been cancelled by "
										+ LoginPresenter.loggedInUser
										+ " with remark "
										+ "\n"
										+ "Remark ="
										+ cancelPopup.getRemark().getValue()
												.trim() + " "
										+ "Cancellation Date =" + new Date());
								async.save(biil,
										new AsyncCallback<ReturnFromServer>() {

											@Override
											public void onFailure(
													Throwable caught) {
												form.showDialogMessage("An Unexpected Error occured !");
												//
											}

											@Override
											public void onSuccess(
													ReturnFromServer result) {
											}
										});

								MyQuerry querry = new MyQuerry();
								Company c = new Company();
								Vector<Filter> filtervec = new Vector<Filter>();
								Filter filter = null;
								filter = new Filter();
								filter.setQuerryString("companyId");
								filter.setLongValue(c.getCompanyId());
								filtervec.add(filter);
								filter = new Filter();
								filter.setQuerryString("businessprocessId");
								filter.setIntValue(biil.getCount());
								filtervec.add(filter);
								filter = new Filter();
								filter.setQuerryString("businessprocesstype");
								filter.setStringValue(AppConstants.APPROVALBILLINGDETAILS);
								filtervec.add(filter);
								querry.setFilters(filtervec);
								querry.setQuerryObject(new Approvals());
								async.getSearchResult(
										querry,
										new AsyncCallback<ArrayList<SuperModel>>() {

											@Override
											public void onFailure(
													Throwable caught) {
											}

											@Override
											public void onSuccess(
													ArrayList<SuperModel> result) {

												for (SuperModel smodel : result) {
													Approvals approval = (Approvals) smodel;

													approval.setStatus(Approvals.CANCELLED);
													approval.setRemark("Purchase Order Id ="
															+ purchaseorderID
															+ " "
															+ "purchase order status ="
															+ purchaseorderStatus
															+ "\n"
															+ "has been cancelled by "
															+ LoginPresenter.loggedInUser
															+ " with remark "
															+ "\n"
															+ "Remark ="
															+ cancelPopup
																	.getRemark()
																	.getValue()
																	.trim()
															+ " "
															+ "Cancellation Date ="
															+ new Date());
													async.save(
															approval,
															new AsyncCallback<ReturnFromServer>() {

																@Override
																public void onFailure(
																		Throwable caught) {
																	form.showDialogMessage("An Unexpected Error occured !");
																}

																@Override
																public void onSuccess(
																		ReturnFromServer result) {

																}
															});

												}
											}
										});

							}
						}
					}

				});

	}

	private void cancelPaymentDetails() {

		final int purchaseorderID = model.getCount();
		final String purchaseorderStatus = model.getStatus();

		MyQuerry querry = new MyQuerry();
		Company c = new Company();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("contractCount");
		filter.setIntValue(purchaseorderID);
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("typeOfOrder");
		filter.setStringValue(AppConstants.ORDERTYPEFORPURCHASE);
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new CustomerPayment());
		async.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {

					@Override
					public void onFailure(Throwable caught) {
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {

						for (SuperModel smodel : result) {

							CustomerPayment custpay = (CustomerPayment) smodel;
							if (custpay.getStatus().equals(
									CustomerPayment.CREATED)) {
								custpay.setStatus(CustomerPayment.CANCELLED);
								custpay.setComment("Purchase Order Id ="
										+ purchaseorderID
										+ " "
										+ "purchase order status ="
										+ purchaseorderStatus
										+ "\n"
										+ "has been cancelled by "
										+ LoginPresenter.loggedInUser
										+ " with remark "
										+ "\n"
										+ "Remark ="
										+ cancelPopup.getRemark().getValue()
												.trim() + " "
										+ "Cancellation Date =" + new Date());
								async.save(custpay,
										new AsyncCallback<ReturnFromServer>() {
											//
											@Override
											public void onFailure(
													Throwable caught) {
												form.showDialogMessage("An Unexpected Error occured !");
												//
											}

											@Override
											public void onSuccess(
													ReturnFromServer result) {
											}
										});
							}
							if (custpay.getStatus().equals(
									CustomerPayment.PAYMENTCLOSED)) {
								custpay.setRenewFlag(true);
								custpay.setStatus(CustomerPayment.PAYMENTCLOSED);
								custpay.setComment("Purchase Order Id ="
										+ purchaseorderID
										+ " "
										+ "purchase order status ="
										+ purchaseorderStatus
										+ "\n"
										+ "has been cancelled by "
										+ LoginPresenter.loggedInUser
										+ " with remark "
										+ "\n"
										+ "Remark ="
										+ cancelPopup.getRemark().getValue()
												.trim() + " "
										+ "Cancellation Date =" + new Date());
								async.save(custpay,
										new AsyncCallback<ReturnFromServer>() {
											//
											@Override
											public void onFailure(
													Throwable caught) {
												form.showDialogMessage("An Unexpected Error occured !");
												//
											}

											@Override
											public void onSuccess(
													ReturnFromServer result) {
											}
										});
							}
						}
					}
				});
	}

	private void cancelInvoiceDetails() {

		final int purchaseorderID = model.getCount();
		final String purchaseorderStatus = model.getStatus();

		MyQuerry querry = new MyQuerry();
		Company c = new Company();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("contractCount");
		filter.setIntValue(purchaseorderID);
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("typeOfOrder");
		filter.setStringValue(AppConstants.ORDERTYPEFORPURCHASE);
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Invoice());
		async.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {

					@Override
					public void onFailure(Throwable caught) {
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {

						for (SuperModel smodel : result) {
							// **************inset login user name in comment
							// *******************

							Invoice invoice = (Invoice) smodel;
							if (invoice.getStatus().equals(Invoice.CREATED)) {
								invoice.setStatus(Invoice.CANCELLED);
								invoice.setComment("Purchase Order Id ="
										+ purchaseorderID
										+ " "
										+ "purchase order status ="
										+ purchaseorderStatus
										+ "\n"
										+ "has been cancelled by "
										+ LoginPresenter.loggedInUser
										+ " with remark "
										+ "\n"
										+ "Remark ="
										+ cancelPopup.getRemark().getValue()
												.trim() + " "
										+ "Cancellation Date =" + new Date());
								async.save(invoice,
										new AsyncCallback<ReturnFromServer>() {
											@Override
											public void onFailure(
													Throwable caught) {
												form.showDialogMessage("An Unexpected Error occured !");
											}

											@Override
											public void onSuccess(
													ReturnFromServer result) {
											}
										});
							}

							if (invoice.getStatus().equals(Invoice.APPROVED)) {
								invoice.setRenewFlag(true);
								invoice.setStatus(Invoice.APPROVED);
								invoice.setComment("Purchase Order Id ="
										+ purchaseorderID
										+ " "
										+ "purchase order status ="
										+ purchaseorderStatus
										+ "\n"
										+ "has been cancelled by "
										+ LoginPresenter.loggedInUser
										+ " with remark "
										+ "\n"
										+ "Remark ="
										+ cancelPopup.getRemark().getValue()
												.trim() + " "
										+ "Cancellation Date =" + new Date());
								async.save(invoice,
										new AsyncCallback<ReturnFromServer>() {
											@Override
											public void onFailure(
													Throwable caught) {
												form.showDialogMessage("An Unexpected Error occured !");
											}

											@Override
											public void onSuccess(
													ReturnFromServer result) {
											}
										});
							}

							if (invoice.getStatus().equals(Invoice.REJECTED)) {
								invoice.setStatus(Invoice.CANCELLED);
								invoice.setComment("Purchase Order Id ="
										+ purchaseorderID
										+ " "
										+ "purchase order status ="
										+ purchaseorderStatus
										+ "\n"
										+ "has been cancelled by "
										+ LoginPresenter.loggedInUser
										+ " with remark "
										+ "\n"
										+ "Remark ="
										+ cancelPopup.getRemark().getValue()
												.trim() + " "
										+ "Cancellation Date =" + new Date());
								async.save(invoice,
										new AsyncCallback<ReturnFromServer>() {
											@Override
											public void onFailure(
													Throwable caught) {
												form.showDialogMessage("An Unexpected Error occured !");
											}

											@Override
											public void onSuccess(
													ReturnFromServer result) {
											}
										});
							}

							if (invoice.getStatus().equals(Invoice.REQUESTED)) {
								invoice.setStatus(Invoice.CANCELLED);
								invoice.setComment("Purchase Order Id ="
										+ purchaseorderID
										+ " "
										+ "purchase order status ="
										+ purchaseorderStatus
										+ "\n"
										+ "has been cancelled by "
										+ LoginPresenter.loggedInUser
										+ " with remark "
										+ "\n"
										+ "Remark ="
										+ cancelPopup.getRemark().getValue()
												.trim() + " "
										+ "Cancellation Date =" + new Date());
								async.save(invoice,
										new AsyncCallback<ReturnFromServer>() {
											@Override
											public void onFailure(
													Throwable caught) {
												form.showDialogMessage("An Unexpected Error occured !");
											}

											@Override
											public void onSuccess(
													ReturnFromServer result) {
											}
										});

								MyQuerry querry = new MyQuerry();
								Company c = new Company();
								Vector<Filter> filtervec = new Vector<Filter>();
								Filter filter = null;
								filter = new Filter();
								filter.setQuerryString("companyId");
								filter.setLongValue(c.getCompanyId());
								filtervec.add(filter);
								filter = new Filter();
								filter.setQuerryString("businessprocessId");
								filter.setIntValue(invoice.getCount());
								filtervec.add(filter);
								filter = new Filter();
								filter.setQuerryString("businessprocesstype");
								filter.setStringValue(AppConstants.APPROVALINVOICEDETAILS);
								filtervec.add(filter);

								querry.setFilters(filtervec);
								querry.setQuerryObject(new Approvals());
								System.out.println("querry completed......");
								async.getSearchResult(
										querry,
										new AsyncCallback<ArrayList<SuperModel>>() {

											@Override
											public void onFailure(
													Throwable caught) {
											}

											@Override
											public void onSuccess(
													ArrayList<SuperModel> result) {

												for (SuperModel smodel : result) {
													Approvals approval = (Approvals) smodel;

													approval.setStatus(Approvals.CANCELLED);
													approval.setRemark("Purchase Order Id ="
															+ purchaseorderID
															+ " "
															+ "purchase order status ="
															+ purchaseorderStatus
															+ "\n"
															+ "has been cancelled by "
															+ LoginPresenter.loggedInUser
															+ " with remark "
															+ "\n"
															+ "Remark ="
															+ cancelPopup
																	.getRemark()
																	.getValue()
																	.trim()
															+ " "
															+ "Cancellation Date ="
															+ new Date());

													async.save(
															approval,
															new AsyncCallback<ReturnFromServer>() {

																@Override
																public void onFailure(
																		Throwable caught) {
																	form.showDialogMessage("An Unexpected Error occured !");
																}

																@Override
																public void onSuccess(
																		ReturnFromServer result) {

																}
															});
												}
											}
										});
							}
						}
					}
				});
	}

	/************************* ON PRODUCT DELETE VENDOR UPDATION CODE *******************************/

	/**
	 * This method update vendor table when we delete product Date : 10-09-2016
	 * By Anil Release : 31 Aug 2016 Project : PURCHASE ERROR SOLVING (NBHC)
	 */

//	public static void updateVendorTable() {
////		System.out.println("PRODUCT TBL SIZE : "+ form.productTablePO.getDataprovider().getList().size());
//		if (form.productTablePO.getDataprovider().getList().size() == 0) {
//			form.vendorTablePO.getDataprovider().getList().clear();
//			form.vendorRateList.clear();
//		} else {
//			List<ProductDetailsPO> prodList = form.productTablePO.getValue();
////			System.out.println("GLOBAL VENDOR PRICE LIST SIZE BF : "+ form.vendorRateList.size());
//			List<PriceListDetails> remProdComVenLis = getRemainingProductVendorPriceList(prodList, form.vendorRateList);
////			System.out.println("GLOBAL VENDOR PRICE LIST SIZE AF : "+ form.vendorRateList.size());
////			System.out.println("COMMON VENDOR PRICEL LIST SIZE : "+ remProdComVenLis.size());
////			System.out.println("EARLIER VEN TBL LIST SIZE :"+ form.vendorTablePO.getDataprovider().getList().size());
//			List<VendorDetails> vendorList = getVendorListOfRemainingProduct(form.vendorTablePO.getDataprovider().getList(),remProdComVenLis);
////			System.out.println("NOW VEN TBL LIST SIZE : " + vendorList.size());
////			System.out.println("AFTER REMOVING : "+ form.vendorTablePO.getDataprovider().getList().size());
//		}
//	}

	/**
	 * This method returns the list of common vendor from global vendor list
	 * which is maintained at the time of product add.
	 * 
	 * Date : 10-09-2016 By Anil Release : 31 Aug 2016 Project : PURCHASE ERROR
	 * SOLVING (NBHC) I/P:existing vendor table list and global vendor list
	 * O/P:vendor list
	 */
	private static List<PriceListDetails> getRemainingProductVendorPriceList(
			List<ProductDetailsPO> prodList,
			ArrayList<PriceListDetails> vendorRateList) {

		ArrayList<PriceListDetails> vendorPriceList = new ArrayList<PriceListDetails>();
		List<PriceListDetails> list = new ArrayList<PriceListDetails>();

		for (ProductDetailsPO prodDet : prodList) {
			for (PriceListDetails priceLis : vendorRateList) {
				if (prodDet.getProductID() == priceLis.getProdID()) {
					vendorPriceList.add(priceLis);
				}
			}
		}

//		form.vendorRateList.clear();
//		form.vendorRateList.addAll(vendorPriceList);

		for (PriceListDetails pricelis : vendorPriceList) {
			if (list.size() == 0) {
				list.add(pricelis);
			} else {
				boolean isAlreadyAddedFlag = checkPriceListAlreadyAdded(list,
						pricelis);
				if (isAlreadyAddedFlag == false) {
					list.add(pricelis);
				}
			}
		}
		return list;
	}

	/**
	 * This method compare whether passed parameter 2nd is present in parameter
	 * 1st i.e. vendor price list Date 10-09-2016 By Anil Release : 31 Aug 2016
	 * Project: PURCHASE ERROR SOLVING(NBHC) I/P:vendor price list and vendor
	 * price O/P:True/False
	 */
	private static boolean checkPriceListAlreadyAdded(
			List<PriceListDetails> list, PriceListDetails pricelis) {
		for (PriceListDetails pricelist : list) {
			if (pricelis.getVendorID() == pricelist.getVendorID()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * This method returns common vendors of remaining products. Date 10-09-2016
	 * By Anil Release : 31 Aug 2016 Project: PURCHASE ERROR SOLVING(NBHC)
	 * I/P:existing vendor list and remaining products vendor list O/P:vendor
	 * list
	 */

	private static List<VendorDetails> getVendorListOfRemainingProduct(
			List<VendorDetails> vendorList, List<PriceListDetails> venRateLis) {
//		System.out.println("REMAINING PRODUCTS VENDOR LIST");

//		System.out.println("VEN LIS SIZE BF " + vendorList.size());
		for (PriceListDetails venRLis : venRateLis) {
			boolean vendorFlag = isVendorPresent(venRLis.getVendorID(),vendorList);
			if (vendorFlag == false) {
				List<PriceListDetails> vLis = new ArrayList<PriceListDetails>();
				vLis.add(venRLis);
				ArrayList<VendorDetails> vDeLis = form.getVendorList(vLis);
				vendorList.addAll(vDeLis);
			}
		}

//		System.out.println("VEN LIS SIZE AF " + vendorList.size());
		return vendorList;
	}

	/**
	 * This method returns true if passed vendor id is present in given vendor
	 * list. Date : 10-09-2016 By Anil Release : 31 Aug 2016 Project: PURCHASE
	 * ERROR SOLVING (NBHC) I/P:Vendor id and Vendor List O/P:True/False
	 */
	private static boolean isVendorPresent(int vendorID,
			List<VendorDetails> vendorList) {
		for (VendorDetails venDet : vendorList) {
			if (vendorID == venDet.getVendorId()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * This method return true if vendor is selected Date : 13-09-2016 By Anil
	 * Release 31 Aug 2016 Project: PURCHASE ERROR SOLVING (NBHC)
	 * 
	 */

	public static boolean isVendorSelected() {
		List<VendorDetails> list = form.vendorTablePO.getDataprovider()
				.getList();

		for (VendorDetails ven : list) {
			if (ven.getStatus() == true) {
				return true;
			}
		}

		form.showDialogMessage("Please select vendor!");
		return false;
	}

	/**
	 * This method return true if CST is applicable Date : 13-09-2016 By Anil
	 * Release 31 Aug 2016 Project: PURCHASE ERROR SOLVING (NBHC)
	 * 
	 */

//	public static boolean isCformCstApplicable() {
//
//		if (form.getDeliveryadd().getState().getSelectedIndex() != 0
//				&& !vendorState.equals("")
//				&& !form.getDeliveryadd().getState().getValue().trim()
//						.equals(vendorState.trim())) {
//
//			form.getCbcformlis().setEnabled(true);
//
//			if (form.getCbcformlis().getSelectedIndex() == 0) {
//				form.showDialogMessage("Please select C Form");
//				return true;
//			} else {
//				if (form.getCbcformlis()
//						.getItemText(form.getCbcformlis().getSelectedIndex())
//						.equals(AppConstants.YES)
//						&& form.getOlbcstpercent().getSelectedIndex() == 0) {
//					form.getOlbcstpercent().setEnabled(true);
//					form.showDialogMessage("Please select CST Percent");
//					return true;
//				}
//			}
//		} else {
////			System.out.println("SET CFORM CST BF");
//			form.getCbcformlis().setSelectedIndex(0);
//			form.getOlbcstpercent().setSelectedIndex(0);
//
////			System.out.println("SET CFORM CST AF");
//
//			form.getCbcformlis().setEnabled(false);
//			form.getOlbcstpercent().setEnabled(false);
//		}
//		return false;
//	}

	/**
	 * This method sets price mention in vendor price list to added products
	 * Date:13-09-2016 By Anil Release 31 Aug 2016 Project : PURCHASE ERROR
	 * SOLVING I/P: Product List and vendor id
	 */
//	private void setVendorPriceToProduct(List<ProductDetailsPO> value,int venCount) {
//
//		ArrayList<ProductDetailsPO> prodList = new ArrayList<ProductDetailsPO>();
//
////		System.out.println("PRICE EDITING ");
////		System.out.println("VENDOR LIST BF  - " + form.vendorRateList.size());
//		ArrayList<PriceListDetails> selVenPricLis = new ArrayList<PriceListDetails>();
//		for (PriceListDetails venlis : form.vendorRateList) {
//			if (venlis.getVendorID() == venCount) {
//				selVenPricLis.add(venlis);
//			}
//		}
////		System.out.println("VENDOR LIST AF- " + selVenPricLis.size());
//		for (ProductDetailsPO prodpo : value) {
//			for (PriceListDetails venlis : selVenPricLis) {
//				if (prodpo.getProductID() == venlis.getProdID()) {
//
//					prodpo.setProdPrice(venlis.getProductPrice());
//					System.out.println(prodpo.getProductID() + " "
//							+ prodpo.getProdPrice());
//				}
//			}
//		}
//
//		form.productTablePO.getTable().redraw();
//	}
	
	
	
	/**
	 * This method make delivery address read only if warehouse is selected
	 * and also checks whether vendor state and warehouse state is different or not
	 * if vendor State is different then it ask for cform and cstpercent
	 * Date : 03-10-2016 By Anil
	 * Release : 30 Sept 2016 
	 * Project: PURCHASE MODIFICATION(NBHC) 
	 */
	
	public static void updateWarehouseAndCformStatus(){
		if(!vendorState.equals("")){
			List<ProductDetailsPO> list=form.productTablePO.getDataprovider().getList();
			
			boolean addressFlag=false;
			for(ProductDetailsPO obj:list){
				if(!obj.getWarehouseName().equals("")){
					form.deliveryadd.clear();
					form.deliveryadd.setEnable(false);
					form.cbadds.setValue(false);
					form.cbadds.setEnabled(false);
					addressFlag=true;
					break;
				}
			}
			
			if(!addressFlag){
				form.cbadds.setValue(true);
				form.deliveryadd.setEnable(true);
			}
			
			HashSet<String> addressSet=new HashSet<String>();
			for(ProductDetailsPO obj:list){
				if(!obj.getWarehouseName().equals("")){
					addressSet.add(obj.getWarehouseName());
				}
			}
//			System.out.println("ADDRESS SET SIZE :: "+addressSet.size());
			
			if(addressSet.size()!=0){
				if(addressSet.size()==1){
					ArrayList<String> addressList=new ArrayList<String>();
					addressList.addAll(addressSet);
//					System.out.println("Setting WH Address as delivery address "+addressSet.toString());
					form.deliveryadd.setValue(ProductTablePO.getWarehouseAddress(addressList.get(0)));
					form.deliveryadd.setEnable(false);
					form.cbadds.setValue(false);
					form.cbadds.setEnabled(false);
				}
			}
			
			boolean cformFlag=false;
			for(ProductDetailsPO obj:list){
				if(!obj.getWarehouseName().equals("")){
					String warehouseState=ProductTablePO.getWarehouseState(obj.getWarehouseName());
//					if(!vendorState.trim().equals(warehouseState.trim())){
//						cformFlag=true;
//						form.getCbcformlis().setEnabled(true);
//						if(form.getCbcformlis().getSelectedIndex()==0){
//							form.showDialogMessage("Please select C Form");
//						}
//						if(form.getCbcformlis().getItemText(form.getCbcformlis().getSelectedIndex()).equals(AppConstants.YES)){
//							if(form.getOlbcstpercent().getSelectedIndex()==0){
//								form.showDialogMessage("Please select CST Percent");
//							}
//						}
//						break;
//					}
				}
			}
			
//			if(!cformFlag){
//				form.getCbcformlis().setSelectedIndex(0);
//				form.getOlbcstpercent().setSelectedIndex(0);
//				form.getCbcformlis().setEnabled(false);
//				form.getOlbcstpercent().setEnabled(false);
//			}
		}else{
			List<ProductDetailsPO> list=form.productTablePO.getDataprovider().getList();
			
			HashSet<String> addressSet=new HashSet<String>();
			for(ProductDetailsPO obj:list){
				if(!obj.getWarehouseName().equals("")){
					addressSet.add(obj.getWarehouseName());
				}
			}
//			System.out.println("ADDRESS SET SIZE :: "+addressSet.size());
			
			if(addressSet.size()!=0){
				if(addressSet.size()==1){
					ArrayList<String> addressList=new ArrayList<String>();
					addressList.addAll(addressSet);
//					System.out.println("Setting WH Address as delivery address "+addressSet.toString());
					form.deliveryadd.setValue(ProductTablePO.getWarehouseAddress(addressList.get(0)));
					form.deliveryadd.setEnable(false);
					form.cbadds.setValue(false);
					form.cbadds.setEnabled(false);
				}else{
					form.deliveryadd.clear();
					form.deliveryadd.setEnable(false);
					form.cbadds.setValue(false);
					form.cbadds.setEnabled(false);
				}
			}else{
				form.deliveryadd.clear();
				form.deliveryadd.setEnable(false);
				form.cbadds.setValue(false);
				form.cbadds.setEnabled(false);	
			}
		}
	}
	
	
	
	
	
protected static void updateTaxesDetails(List<ProductDetailsPO> list,Branch branchEntity, String vendorState2) {
		

		try {
		          GWTCAlert gwtalert = new GWTCAlert();
		          
			for(ProductDetailsPO productDetails : list){
							
						System.out.println("LoginPresenter.globalTaxList"+LoginPresenter.globalTaxList.size());
					if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany")){
						if(vendorState2!=null && !vendorState2.trim().equals("") 
								&& branchEntity.getAddress().getState()!=null && !branchEntity.getAddress().getState().equals("") ){
							if(!vendorState2.trim().equals(branchEntity.getAddress().getState().trim())){
//								 Tax tax1 = new Tax();
//								 tax1 =	 getTaxDetails("IGST",LoginPresenter.globalTaxList,18);
//								 if(tax1!=null){
//									 productDetails.setVat(tax1.getPercentage());
//									 productDetails.setPurchaseTax1(tax1);
//								 }
//								 
//								 Tax tax2 = new Tax();
//								 tax2 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
//								 if(tax2!=null){
//									 productDetails.setTax(tax2.getPercentage());
//									 productDetails.setPurchaseTax2(tax2);
//								 }
								
								System.out.println("branch as company different state");
								
								if(productDetails.getVatTax()!=null && productDetails.getVatTax().getPercentage()!=0){
									Tax tax1 = new Tax();
									double tax1percentage = 0;
									if(productDetails.getVatTax()!=null && productDetails.getVatTax().getPercentage()!=0){
										 if(productDetails.getVatTax().getTaxPrintName().trim().equals("CGST") || productDetails.getVatTax().getTaxPrintName().trim().equals("SGST")){
											 if(productDetails.getServiceTax()!=null ){
												 tax1percentage = productDetails.getVatTax().getPercentage() + productDetails.getServiceTax().getPercentage();
											 }
											 else{
												 tax1percentage = productDetails.getVatTax().getPercentage();
											 }
											 System.out.println("tax1percentage == "+tax1percentage);
										 }
										 else{
											 tax1percentage = productDetails.getVatTax().getPercentage();
											 System.out.println("tax1percentage == $$ "+tax1percentage);
										 }
										 System.out.println("tax1percentage "+tax1percentage);
										 tax1 =	 getTaxDetails("IGST",LoginPresenter.globalTaxList,tax1percentage);
									 }
									 else{
										 tax1 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
									 }
									 
									 if(tax1!=null){
										 productDetails.setVatTax(tax1);
									 }
									 else{
										 gwtalert.alert("IGST "+tax1percentage +" does not exist in Tax Master!");
										 tax1 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
										 productDetails.setVatTax(tax1);
										 System.out.println("here igst found");
									 }
									 Tax tax2 = new Tax();
									 tax2 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
									 productDetails.setServiceTax(tax2);
								}
//								else{
								if(productDetails.getServiceTax()!=null && productDetails.getServiceTax().getPercentage()!=0){

								 System.out.println("tax 2 started here");
								 double tax2percentage = 0;
								 Tax tax2 = new Tax();
								 if(productDetails.getServiceTax()!=null && productDetails.getServiceTax().getPercentage()!=0){
									 if(productDetails.getVatTax().getTaxPrintName().trim().equals("CGST") || productDetails.getServiceTax().getTaxPrintName().trim().equals("SGST")){
										 if(productDetails.getVatTax()!=null ){
											 tax2percentage = productDetails.getServiceTax().getPercentage() + productDetails.getVatTax().getPercentage();
											 System.out.println("tax1percentage @@==@@ "+tax2percentage);
										 }
										 else{
											 tax2percentage = productDetails.getVatTax().getPercentage();
										 }
										 System.out.println("tax1percentage @@== "+tax2percentage);
									 }
									 else{
										 tax2percentage = productDetails.getVatTax().getPercentage();
										 System.out.println("tax1percentage @@ "+tax2percentage);
									 }
									 System.out.println("tax1percentage $$$ "+tax2percentage);
									 tax2 =	 getTaxDetails("IGST",LoginPresenter.globalTaxList,tax2percentage);
								 }
								 else{
									 tax2 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
								 }
								 if(tax2!=null){
									 productDetails.setServiceTax(tax2);
								 }
								 else{
									 System.out.println("tax 2 ");
									 gwtalert.alert("IGST "+tax2percentage +" does not exist in Tax Master!");
									 tax2 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
									 productDetails.setServiceTax(tax2);
								 }
								 Tax tax = new Tax();
								 tax =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
								 productDetails.setVatTax(tax);
						  	}	
								 
							}
							else{
								
								System.out.println("branch as company same state");
								 if(productDetails.getVatTax()!=null && productDetails.getVatTax().getPercentage()!=0){
									 Tax tax1 = new Tax();
									 double tax1percentage = 0;
									 if(productDetails.getVatTax()!=null && productDetails.getVatTax().getPercentage()!=0){
										 if(productDetails.getVatTax().getTaxPrintName().trim().equals("IGST")){
												 tax1percentage = productDetails.getVatTax().getPercentage()/2;
												 System.out.println("saleslineitem.getVatTax().getPercentage()"+productDetails.getVatTax().getPercentage());
										 }
										 else{
											 tax1percentage = productDetails.getVatTax().getPercentage();
											 System.out.println("tax1percentage &&* "+tax1percentage);
										 }
										 System.out.println("tax1percentage **&& "+tax1percentage);
										 tax1 =	 getTaxDetails("CGST",LoginPresenter.globalTaxList,tax1percentage);
									 }
									 else{
										 tax1 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
									 }
									 if(tax1!=null){
										 if(productDetails.getVatTax().getTaxPrintName().trim().equals("IGST") && 
												 tax1.getTaxPrintName().trim().equals("CGST")){
											 Tax tax2 = new Tax();
											 tax2 =	 getTaxDetails("SGST",LoginPresenter.globalTaxList,tax1.getPercentage());
											 productDetails.setServiceTax(tax2);

										 }
										 productDetails.setVatTax(tax1);
									 }
									 else{
										 System.out.println("here alert");
										 gwtalert.alert("CGST "+tax1percentage +" does not exist in Tax Master!");
										 tax1 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
										 productDetails.setVatTax(tax1);
										 
										 Tax tax2 = new Tax();
										 tax2 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
										 productDetails.setServiceTax(tax2);
									 }
									 
								 }
//								 else{
								 if(productDetails.getServiceTax()!=null && productDetails.getServiceTax().getPercentage()!=0){
 
									 System.out.println("else tax 2 == ");
									 double tax2percentage = 0;
									 Tax tax2 = new Tax();
									 if(productDetails.getServiceTax()!=null && productDetails.getServiceTax().getPercentage()!=0){
										 if(productDetails.getServiceTax().getTaxPrintName().trim().equals("IGST")){
											 if(productDetails.getServiceTax()!=null ){
												 tax2percentage = productDetails.getServiceTax().getPercentage()/2;
												 System.out.println("saleslineitem.getServiceTax().getPercentage()"+productDetails.getServiceTax().getPercentage());

											 }
										 }
										 else{
											 tax2percentage = productDetails.getServiceTax().getPercentage();
											 System.out.println("tax2percentage &&== * "+tax2percentage);
										 }
										 System.out.println("tax2percentage **&& "+tax2percentage);
										 tax2 =	 getTaxDetails("SGST",LoginPresenter.globalTaxList,tax2percentage);
									 }
									 else{
										 tax2 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
									 }
									 if(tax2!=null){
										 if(productDetails.getServiceTax().getTaxPrintName().trim().equals("IGST") && 
												 tax2.getTaxPrintName().trim().equals("SGST")){
											 Tax tax1 = new Tax();
											 tax1 =	 getTaxDetails("CGST",LoginPresenter.globalTaxList,tax2.getPercentage());
											 productDetails.setVatTax(tax1);

										 }
										 productDetails.setServiceTax(tax2);
									 }
									 else{
										 System.out.println("here alert ==");

										 gwtalert.alert("SGST "+tax2percentage +"does not exist in Tax Master!");
										 tax2 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
										 productDetails.setServiceTax(tax2);
										 
										 Tax tax = new Tax();
										 tax =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
										 productDetails.setServiceTax(tax);
									 }
								 }
								 
							}
						}
					}
					else{
						System.out.println("else block");
						if(vendorState2!=null && !vendorState2.equals("") 
								&& LoginPresenter.company!=null && LoginPresenter.company.getAddress().getState()!=null && !LoginPresenter.company.getAddress().getState().equals("") ){
							
							if(!vendorState2.trim().equals(LoginPresenter.company.getAddress().getState().trim())){
								
								System.out.println("else block 1");

								if(productDetails.getVatTax()!=null && productDetails.getVatTax().getPercentage()!=0){
									Tax tax1 = new Tax();
									double tax1percentage = 0;
									if(productDetails.getVatTax()!=null && productDetails.getVatTax().getPercentage()!=0){
										 if(productDetails.getVatTax().getTaxPrintName().trim().equals("CGST") || productDetails.getVatTax().getTaxPrintName().trim().equals("SGST")){
											 if(productDetails.getServiceTax()!=null ){
												 tax1percentage = productDetails.getVatTax().getPercentage() + productDetails.getServiceTax().getPercentage();
											 }
											 else{
												 tax1percentage = productDetails.getVatTax().getPercentage();
											 }
											 System.out.println("tax1percentage == "+tax1percentage);
										 }
										 else{
											 tax1percentage = productDetails.getVatTax().getPercentage();
											 System.out.println("tax1percentage == $$ "+tax1percentage);
										 }
										 System.out.println("tax1percentage "+tax1percentage);
										 tax1 =	 getTaxDetails("IGST",LoginPresenter.globalTaxList,tax1percentage);
									 }
									 else{
										 tax1 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
									 }
									 
									 if(tax1!=null){
										 productDetails.setVatTax(tax1);
									 }
									 else{
										 gwtalert.alert("IGST "+tax1percentage +" does not exist in Tax Master!");
										 tax1 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
										 productDetails.setVatTax(tax1);
										 System.out.println("here igst found");
									 }
									 Tax tax2 = new Tax();
									 tax2 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
									 productDetails.setServiceTax(tax2);
								}
//								else{
								if(productDetails.getServiceTax()!=null && productDetails.getServiceTax().getPercentage()!=0){
	
									
								 System.out.println("tax 2 started here");
								 double tax2percentage = 0;
								 Tax tax2 = new Tax();
								 if(productDetails.getServiceTax()!=null && productDetails.getServiceTax().getPercentage()!=0){
									 if(productDetails.getVatTax().getTaxPrintName().trim().equals("CGST") || productDetails.getServiceTax().getTaxPrintName().trim().equals("SGST")){
										 if(productDetails.getVatTax()!=null ){
											 tax2percentage = productDetails.getServiceTax().getPercentage() + productDetails.getVatTax().getPercentage();
											 System.out.println("tax1percentage @@==@@ "+tax2percentage);
										 }
										 else{
											 tax2percentage = productDetails.getVatTax().getPercentage();
										 }
										 System.out.println("tax1percentage @@== "+tax2percentage);
									 }
									 else{
										 tax2percentage = productDetails.getVatTax().getPercentage();
										 System.out.println("tax1percentage @@ "+tax2percentage);
									 }
									 System.out.println("tax1percentage $$$ "+tax2percentage);
									 tax2 =	 getTaxDetails("IGST",LoginPresenter.globalTaxList,tax2percentage);
								 }
								 else{
									 tax2 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
								 }
								 if(tax2!=null){
									 productDetails.setServiceTax(tax2);
								 }
								 else{
									 System.out.println("tax 2 ");
									 gwtalert.alert("IGST "+tax2percentage +" does not exist in Tax Master!");
									 tax2 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
									 productDetails.setServiceTax(tax2);
								 }
								 Tax tax = new Tax();
								 tax =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
								 productDetails.setVatTax(tax);
						  	}	
							
									 
							}
							else{
								 System.out.println("else block 2 else");


								 System.out.println("else block 2 else");

								 System.out.println("taxxxx");
								 if(productDetails.getVatTax()!=null && productDetails.getVatTax().getPercentage()!=0){
									 Tax tax1 = new Tax();
									 double tax1percentage = 0;
									 if(productDetails.getVatTax()!=null && productDetails.getVatTax().getPercentage()!=0){
										 if(productDetails.getVatTax().getTaxPrintName().trim().equals("IGST")){
												 tax1percentage = productDetails.getVatTax().getPercentage()/2;
												 System.out.println("saleslineitem.getVatTax().getPercentage()"+productDetails.getVatTax().getPercentage());
										 }
										 else{
											 tax1percentage = productDetails.getVatTax().getPercentage();
											 System.out.println("tax1percentage &&* "+tax1percentage);
										 }
										 System.out.println("tax1percentage **&& "+tax1percentage);
										 tax1 =	 getTaxDetails("CGST",LoginPresenter.globalTaxList,tax1percentage);
									 }
									 else{
										 tax1 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
									 }
									 if(tax1!=null){
										 if(productDetails.getVatTax().getTaxPrintName().trim().equals("IGST") && 
												 tax1.getTaxPrintName().trim().equals("CGST")){
											 Tax tax2 = new Tax();
											 tax2 =	 getTaxDetails("SGST",LoginPresenter.globalTaxList,tax2.getPercentage());
											 productDetails.setServiceTax(tax2);

										 }
										 productDetails.setVatTax(tax1);
									 }
									 else{
										 System.out.println("here alert");
										 gwtalert.alert("CGST "+tax1percentage +" does not exist in Tax Master!");
										 tax1 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
										 productDetails.setVatTax(tax1);
										 
										 Tax tax2 = new Tax();
										 tax2 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
										 productDetails.setServiceTax(tax2);
									 }
									 
								 }
//								 else{
								 if(productDetails.getServiceTax()!=null && productDetails.getServiceTax().getPercentage()!=0){
 
									 System.out.println("else tax 2 == ");
									 double tax2percentage = 0;
									 Tax tax2 = new Tax();
									 if(productDetails.getServiceTax()!=null && productDetails.getServiceTax().getPercentage()!=0){
										 if(productDetails.getServiceTax().getTaxPrintName().trim().equals("IGST")){
											 if(productDetails.getServiceTax()!=null ){
												 tax2percentage = productDetails.getServiceTax().getPercentage()/2;
												 System.out.println("saleslineitem.getServiceTax().getPercentage()"+productDetails.getServiceTax().getPercentage());

											 }
										 }
										 else{
											 tax2percentage = productDetails.getServiceTax().getPercentage();
											 System.out.println("tax2percentage &&== * "+tax2percentage);
										 }
										 System.out.println("tax2percentage **&& "+tax2percentage);
										 tax2 =	 getTaxDetails("SGST",LoginPresenter.globalTaxList,tax2percentage);
									 }
									 else{
										 tax2 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
									 }
									 if(tax2!=null){
										 if(productDetails.getServiceTax().getTaxPrintName().trim().equals("IGST") && 
												 tax2.getTaxPrintName().trim().equals("SGST")){
											 Tax tax1 = new Tax();
											 tax1 =	 getTaxDetails("CGST",LoginPresenter.globalTaxList,tax2.getPercentage());
											 productDetails.setVatTax(tax1);

										 }
										 productDetails.setServiceTax(tax2);
									 }
									 else{
										 System.out.println("here alert ==");

										 gwtalert.alert("SGST "+tax2percentage +"does not exist in Tax Master!");
										 tax2 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
										 productDetails.setServiceTax(tax2);
										 
										 Tax tax = new Tax();
										 tax =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
										 productDetails.setServiceTax(tax);
									 }
								 }
								 
							}
			
							
						}
					
					}
					
			}
			
			form.productTablePO.getDataprovider().setList(list);
			form.productTablePO.getTable().redraw();
			
	} catch (Exception e) {
		// TODO: handle exception
	}
		
	
	
	}

	private static Tax getTaxDetails(String taxNamePrintName,ArrayList<TaxDetails> globalTaxList, double percentage) {

		try {
		
		for(TaxDetails taxDetails : globalTaxList){
			if(taxDetails.getTaxChargeStatus() && taxDetails.getTaxPrintName().trim().equalsIgnoreCase(taxNamePrintName.trim()) && percentage==taxDetails.getTaxChargePercent()){
				Tax tax = new Tax();
				tax.setPercentage(taxDetails.getTaxChargePercent());
				tax.setTaxConfigName(taxDetails.getTaxChargeName());
				tax.setTaxName(taxDetails.getTaxChargeName());
				tax.setTaxPrintName(taxDetails.getTaxPrintName());
				return tax;
			}
		}
		
		
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	
		return null;
	
	}

	private void setEmailPopUpData() {
		form.showWaitSymbol();

		String branchName = form.branch.getValue();
		String fromEmailId = AppUtility.getFromEmailAddress(branchName,model.getEmployee());
		List<VendorDetails> vendorlist = form.vendorTablePO.getDataprovider().getList();
		List<ContactPersonIdentification> vendorDetailslist = AppUtility.getvenodrEmailId(vendorlist,false);
		System.out.println("vendorDetailslist "+vendorDetailslist);
		label : AppUtility.loadContactPersonlist(emailpopup.olbContactlistEmailId,"Vendor",-1,"","",vendorDetailslist);
		Screen screenName=(Screen) AppMemory.getAppMemory().currentScreen;
		Console.log("screenName "+screenName);
		AppUtility.makeLiveSmsTemplateConfig(emailpopup.oblTemplateName,AppConstants.PURCHASEMODULE.trim(),screenName);
		emailpopup.taFromEmailId.setValue(fromEmailId);
		emailpopup.smodel = model;
		form.hideWaitSymbol();
	}


}
