package com.slicktechnologies.client.views.purchase.quickpurchase;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.code.p.gwtchismes.client.GWTCGlassPanel;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.SelectionCell;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.inventory.recievingnote.InventoryLocationPopUp;
import com.slicktechnologies.client.views.popups.ProductSerailNumberPopup;
import com.slicktechnologies.client.views.purchase.purchaseorder.ProductTablePO;
import com.slicktechnologies.client.views.purchase.purchaseorder.PurchaseOrderPresenter;
import com.slicktechnologies.client.views.quickcontract.QuickContractPresentor;
import com.slicktechnologies.client.views.salesorder.SalesOrderPresenter;
import com.slicktechnologies.shared.ProductSerialNoMapping;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.TaxDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetailsPO;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;
import com.slicktechnologies.shared.common.inventory.Storagebin;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.productlayer.Tax;

public class QuickPurchaseProductTable extends SuperTable<ProductDetailsPO> implements ClickHandler {

	NumberFormat nf = NumberFormat.getFormat("0.00");

	Column<ProductDetailsPO,String> columnProdName;

	TextColumn<ProductDetailsPO> columnProductID;
	TextColumn<ProductDetailsPO> columnProductName;
	TextColumn<ProductDetailsPO> columnProductCode;
	TextColumn<ProductDetailsPO> columnProductCategory;
	TextColumn<ProductDetailsPO> columnPriceColumn;
	TextColumn<ProductDetailsPO> columnProdQtyColumn;
	TextColumn<ProductDetailsPO> columnunitOfmeasurement;
	
	TextColumn<ProductDetailsPO> columnServiceTax;
	TextColumn<ProductDetailsPO> columnVat;
	TextColumn<ProductDetailsPO> columnDiscount;
	TextColumn<ProductDetailsPO> nonEditcolumnDiscount;

	Column<ProductDetailsPO, String> columnProductPrice;
	Column<ProductDetailsPO, String> columnProductQuantity;
	Column<ProductDetailsPO, String> ecolumnDiscount;
	Column<ProductDetailsPO, String> columnTotal;
	Column<ProductDetailsPO, String> deleteColumn;
	
	Column<ProductDetailsPO, String> discountAmt;
	TextColumn<ProductDetailsPO> viewdiscountAmt;
	
	ArrayList<TaxDetails> vattaxlist;
	ArrayList<TaxDetails> servicetaxlist;
	ArrayList<String> list;
	ArrayList<String> vatlist;
	Column<ProductDetailsPO,String> editVatColumn,editServiceColumn;
	
	TextColumn<ProductDetailsPO> expDeliveryDateColumn;
	Column<ProductDetailsPO,Date> editableExpDeliveryDateColumn;
	protected GWTCGlassPanel glassPanel;

	/**
	 * Vijay added this code for adding HSN code in prouct
	 */
	
	TextColumn<ProductDetailsPO> viewHsnCodeColumn;
	Column<ProductDetailsPO,String> hsnCodeColumn;
	/**
	 * ends here  
	 */
	
	
	/**
	 * Date 17-08-2017 added by vijay 
	 */
	
	InventoryLocationPopUp invLoc=new InventoryLocationPopUp();
	PopupPanel panel=new PopupPanel(true);
	int rowIndex;
	
	Column<ProductDetailsPO, String> addwarehouseBtnColumn;
	TextColumn<ProductDetailsPO> getColumnProductAvailableQty;
	public  ArrayList<Storagebin> storagelocList1;
	
	Column<ProductDetailsPO, String> warehouseColumn;
	TextColumn<ProductDetailsPO> warehouseViewColumn;
	
	Column<ProductDetailsPO, String> storagebin;
	Column<ProductDetailsPO, String> columnviewBin;
	
	Column<ProductDetailsPO, String> storageLoc;
	TextColumn<ProductDetailsPO> columnviewLoc;
	
	
	/**
	 * ends here
	 */
	/**
	 * nidhi
	 * 21-08-2018
	 * for map serial number
	 */
	Column<ProductDetailsPO, String> addProSerialNo;
	Column<ProductDetailsPO, String> viewProSerialNo;
	boolean tableState = false;
	ProductSerailNumberPopup prodSerialPopup = new ProductSerailNumberPopup(true,false);
	public QuickPurchaseProductTable() {
		super();
		invLoc.getAddButton().addClickHandler(this);
		invLoc.getCancelButton().addClickHandler(this);
		/**
		 * nidhi
		 * 22-08-2018
		 */
		prodSerialPopup.getLblOk().addClickHandler(this);
		prodSerialPopup.getLblCancel().addClickHandler(this);
	}

	public QuickPurchaseProductTable(UiScreen<ProductDetailsPO> view) {
		super(view);
		/**
		 * nidhi
		 * 22-08-2018
		 */
		prodSerialPopup.getLblOk().addClickHandler(this);
		prodSerialPopup.getLblCancel().addClickHandler(this);
	}

	@Override
	public void createTable() {
		
		createColumnprodName();
		addColumnUnit();
		addColumnProductAvailableQty();
		addColumnProductQuantity();
		addColumnProductPrice();
		
		retriveServiceTax();
		
	
	}

	private void retriveServiceTax() {
		final GenricServiceAsync service = GWT.create(GenricService.class);
		MyQuerry query = new MyQuerry();
		Company c=new Company();
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		query.setFilters(filtervec);
		query.setQuerryObject(new TaxDetails());
		
		service.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
			}
			public void onSuccess(ArrayList<SuperModel> result) {
				list=new ArrayList<String>();
				servicetaxlist=new ArrayList<TaxDetails>();
				vattaxlist = new ArrayList<TaxDetails>();
				vatlist=new ArrayList<String>();
				List<TaxDetails> backlist=new ArrayList<TaxDetails>();
				for (SuperModel model : result) {
					TaxDetails entity = (TaxDetails) model;
					backlist.add(entity);
					servicetaxlist.add(entity);
					vattaxlist.add(entity);
				}
				for(int i=0;i<backlist.size();i++){
						list.add(backlist.get(i).getTaxChargeName());
						vatlist.add(backlist.get(i).getTaxChargeName());
				}
				
				
				addColumnDiscount();
				createColumnDiscountAmt();
				addColumnTotal();
				addEditColumnVatTax();
				addEditColumnServiceTax();
				addEditableExpDeliveryDateColoumn();
				viewWarehouse();
				viewstarageLoc();
				viewstorageBin();
				addColumnProductCategory();
				addColumnProductCode();
				addColumnProductID();
				createColumnprodHSNCodeColumn();
				createColumnAddColumn();
				/**
				 * nidhi
				 * 21-08-2018
				 * for add serial number
				 */
				if(LoginPresenter.mapModelSerialNoFlag){
					createColumnAddProSerialNoColumn();
				}
				createColumndeleteColumn();
				addFieldUpdater();
				
			}
		});
	}
	
	
	

	public void addeditColumn() {
		addColumnProductID();
		addColumnProductCode();
		createColumnprodName();
		addColumnProductCategory();
		addColumnProductPrice();
		retriveServiceTax();
	}

	public void addViewColumn() {
		addColumnProductID();
		addColumnviewProductName();
		addColumnProductCode();
		addColumnProductCategory();
		/*
		 *  nidhi 
		 *  18-07-2017
		 *   addColumnProductPrice method removed and in space of that addColumnViewProductPrice method 
		 *   add for make product price read only
		 */
		addColumnViewProductPrice();
		/*
		 *  
		 */
		addColumnViewQuantity();
		addColumnUnit();
		addColumnTax();
		vatColumn();
		addColumnviewDiscount();
		addColumnviewDiscountAmt();
		addColumnTotal();
		
		createColumnViewHSNCodeColumn();
		
		addColumnProductAvailableQty();
		viewWarehouse();
		viewstarageLoc();
		viewstorageBin();
		
		addExpDeliveryDateColoumn();
		/**
		 * nidhi
		 * 21-08-2018
		 * for add serial number
		 */
		if(LoginPresenter.mapModelSerialNoFlag){
			createViewColumnAddProSerialNoColumn();
		}
		addFieldUpdater();
		/**
		 * end
		 */
	}

	
	public void createColumnAddColumn() {
		ButtonCell btnCell = new ButtonCell();
		addwarehouseBtnColumn = new Column<ProductDetailsPO, String>(btnCell) {
			
			@Override
			public String getValue(ProductDetailsPO object) {
				return "Select Warehouse";
			}
		};
		table.addColumn(addwarehouseBtnColumn, "");
		table.setColumnWidth(addwarehouseBtnColumn,170,Unit.PX);

	}
	
	public void viewWarehouse() {
		warehouseViewColumn = new TextColumn<ProductDetailsPO>() {
			@Override
			public String getValue(ProductDetailsPO object) {
				return object.getItemProductWarehouseName();
			}
		};
		table.addColumn(warehouseViewColumn, "Warehouse");
		table.setColumnWidth(warehouseViewColumn,153,Unit.PX);
	}
	
	public void viewstarageLoc() {
		columnviewLoc = new TextColumn<ProductDetailsPO>() {
			@Override
			public String getValue(ProductDetailsPO object) {
				return object.getItemProductStrorageLocation();
			}
		};
		table.addColumn(columnviewLoc, "Storage Location");
		table.setColumnWidth(columnviewLoc,153,Unit.PX);
	}
	public void viewstorageBin() {
		columnviewBin = new TextColumn<ProductDetailsPO>() {
			@Override
			public String getValue(ProductDetailsPO object) {
				return object.getItemProductStorageBin();
			}
		};
		table.addColumn(columnviewBin, "Storage Bin");
		table.setColumnWidth(columnviewBin,153,Unit.PX);
	}
	
	private void addColumnProductAvailableQty() {
		getColumnProductAvailableQty = new TextColumn<ProductDetailsPO>() {
			
			@Override
			public String getValue(ProductDetailsPO object) {
				return nf.format(object.getItemProductAvailableQty());
			}
		};
		table.addColumn(getColumnProductAvailableQty, "Stock");
		table.setColumnWidth(getColumnProductAvailableQty,180,Unit.PX);
	}
	
	
	
	protected void createViewDiscountAmt() {
		viewdiscountAmt = new TextColumn<ProductDetailsPO>() {
			@Override
			public String getValue(ProductDetailsPO object) {
				if (object.getDiscountAmt() == 0) {
					return 0 + "";
				} else {
					return object.getDiscountAmt() + "";
				}
			}
		};
		table.addColumn(viewdiscountAmt, "#Disc Amt");
		table.setColumnWidth(viewdiscountAmt, 90, Unit.PX);
		viewdiscountAmt.setCellStyleNames("wordWrap");
	}
	
	
	public void addColumnProductID() {
		columnProductID = new TextColumn<ProductDetailsPO>() {

			@Override
			public String getValue(ProductDetailsPO object) {
				return object.getProductID() + "";
			}
		};
		table.addColumn(columnProductID, "Product ID");
		table.setColumnWidth(columnProductID, 90, Unit.PX);
		columnProductID.setCellStyleNames("wordWrap");
	}

	public void addColumnviewProductName() {
		columnProductName = new TextColumn<ProductDetailsPO>() {

			@Override
			public String getValue(ProductDetailsPO object) {
				return object.getProductName();
			}
		};
		table.addColumn(columnProductName, "Name");
		table.setColumnWidth(columnProductName, 120, Unit.PX);
		columnProductName.setCellStyleNames("wordWrap");
	}
	
	protected void createColumnprodName()
	{
		
		EditTextCell editCell=new EditTextCell();
		columnProdName=new Column<ProductDetailsPO,String>(editCell)
				{
			@Override
			public String getValue(ProductDetailsPO object)
			{
				return object.getProductName();
			}
				};
				table.addColumn(columnProdName,"#Name");
				table.setColumnWidth(columnProdName, 100,Unit.PX);
				columnProdName.setCellStyleNames("wordWrap");

	}

	public void addColumnProductCode() {
		columnProductCode = new TextColumn<ProductDetailsPO>() {

			@Override
			public String getValue(ProductDetailsPO object) {
				return object.getProductCode();
			}
		};
		table.addColumn(columnProductCode, "Code");
		table.setColumnWidth(columnProductCode, 100, Unit.PX);
		columnProductCode.setCellStyleNames("wordWrap");
	}

	public void addColumnProductCategory() {
		columnProductCategory = new TextColumn<ProductDetailsPO>() {

			@Override
			public String getValue(ProductDetailsPO object) {
				return object.getProductCategory();
			}
		};
		table.addColumn(columnProductCategory, "Category");
		table.setColumnWidth(columnProductCategory, 100, Unit.PX);
		columnProductCategory.setCellStyleNames("wordWrap");
	}

	//   this is for vat tax 
	public void addEditColumnVatTax(){
		SelectionCell employeeselection= new SelectionCell(vatlist);
		editVatColumn = new Column<ProductDetailsPO, String>(employeeselection) {
			@Override
			public String getValue(ProductDetailsPO object) {
				if (object.getVatTax().getTaxConfigName()!= null&&!object.getVatTax().getTaxConfigName().equals("")) {
					object.setVatTaxEdit(object.getVatTax().getTaxPrintName());
					return object.getVatTax().getTaxConfigName();
				} else
					return "NA";
			}
		};
		table.addColumn(editVatColumn, "#TAX 1");
		table.setColumnWidth(editVatColumn, 100, Unit.PX);
		editVatColumn.setCellStyleNames("wordWrap");
	}

	///   service tax editable 
	public void addEditColumnServiceTax(){
		SelectionCell employeeselection= new SelectionCell(list);
		editServiceColumn = new Column<ProductDetailsPO, String>(employeeselection) {
			@Override
			public String getValue(ProductDetailsPO object) {
				if (object.getServiceTax().getTaxConfigName()!= null&&!object.getServiceTax().getTaxConfigName().equals("")) {
					object.setServiceTaxEdit(object.getServiceTax().getTaxPrintName());
					return object.getServiceTax().getTaxConfigName();
				} else
					return "NA";
			}
		};
		table.addColumn(editServiceColumn, "#TAX 2");
		table.setColumnWidth(editServiceColumn, 100, Unit.PX);
		editServiceColumn.setCellStyleNames("wordWrap");
	}
	

	// Editable Product Price
	public void addColumnProductPrice() {
		EditTextCell prodprice = new EditTextCell();
		columnProductPrice = new Column<ProductDetailsPO, String>(prodprice) {

			@Override
			public String getValue(ProductDetailsPO object) {
				SuperProduct product = object.getPrduct();
				double tax = removeAllTaxes(product);
				double origPrice = object.getProdPrice() - tax;

				return nf.format(origPrice) + "";
			}
		};
		table.addColumn(columnProductPrice, "#Price");
		table.setColumnWidth(columnProductPrice, 90, Unit.PX);
		columnProductPrice.setCellStyleNames("wordWrap");
	}

	// Read Only Product Price
	protected void addColumnViewPrice() {

		columnPriceColumn = new TextColumn<ProductDetailsPO>() {
			@Override
			public String getValue(ProductDetailsPO object) {
				if (object.getProdPrice() == 0) {
					return 0 + "";
				} else {
					SuperProduct product = object.getPrduct();
					double tax = removeAllTaxes(product);
					double origPrice = object.getProdPrice() - tax;
					return nf.format(origPrice);
				}
			}
		};
		table.addColumn(columnPriceColumn, "#Price");
		table.setColumnWidth(columnPriceColumn, 90, Unit.PX);
		columnPriceColumn.setCellStyleNames("wordWrap");
	}

	// Editable Product Quantity
	public void addColumnProductQuantity() {
		EditTextCell editCell = new EditTextCell();
		columnProductQuantity = new Column<ProductDetailsPO, String>(editCell) {

			@Override
			public String getValue(ProductDetailsPO object) {
				if (object.getProductQuantity() == 0) {
					object.setProductQuantity(1);
				}
				return object.getProductQuantity() + "";
			}
		};
		table.addColumn(columnProductQuantity, "#Quantity");
		table.setColumnWidth(columnProductQuantity, 90, Unit.PX);
		columnProductQuantity.setCellStyleNames("wordWrap");
	}

	// Read Only Product Quantity
	public void addColumnViewQuantity() {
		columnProdQtyColumn = new TextColumn<ProductDetailsPO>() {
			@Override
			public String getValue(ProductDetailsPO object) {
				if (object.getProductQuantity() == 0) {
					object.setProductQuantity(1);
				}
				return object.getProductQuantity() + "";
			}
		};
		table.addColumn(columnProdQtyColumn, "#Quantity");
		table.setColumnWidth(columnProdQtyColumn, 90, Unit.PX);
		columnProdQtyColumn.setCellStyleNames("wordWrap");
	}
	
	/**
	 *  nidhi
	 *   18-07-2017
	 *    product price view only method
	 */
	public void addColumnViewProductPrice() {
		columnPriceColumn = new TextColumn<ProductDetailsPO>() {
			@Override
			public String getValue(ProductDetailsPO object) {
				return object.getProdPrice() + "";
			}
		};
		table.addColumn(columnPriceColumn, "#Price");
		table.setColumnWidth(columnPriceColumn, 90, Unit.PX);
		columnPriceColumn.setCellStyleNames("wordWrap");
	}
	
	
	
	public void addColumnUnit() {
		columnunitOfmeasurement = new TextColumn<ProductDetailsPO>() {

			@Override
			public String getValue(ProductDetailsPO object) {
				return object.getUnitOfmeasurement();
			}
		};
		table.addColumn(columnunitOfmeasurement, "UOM");
		table.setColumnWidth(columnunitOfmeasurement, 70, Unit.PX);
		columnunitOfmeasurement.setCellStyleNames("wordWrap");
	}
	
	// Read Only Service Tax Column
		public void addColumnTax() {
			columnServiceTax = new TextColumn<ProductDetailsPO>() {
				@Override
				public String getValue(ProductDetailsPO object) {
					if (object.getTax() != null) {
						return object.getServiceTaxEdit() + "";
					} else {
						return "N.A";
					}
				}
			};
			table.addColumn(columnServiceTax, "TAX 2");
			table.setColumnWidth(columnServiceTax, 100, Unit.PX);
			columnServiceTax.setCellStyleNames("wordWrap");
		}
		
	// Read Only Vat Column
	public void vatColumn() {
		columnVat = new TextColumn<ProductDetailsPO>() {
			@Override
			public String getValue(ProductDetailsPO object) {
				if (object.getVat() != null) {
					return object.getVatTaxEdit() + "";
				} else {
					return "N.A";
				}
			}
		};
		table.addColumn(columnVat, "TAX 1");
		table.setColumnWidth(columnVat, 100, Unit.PX);
		columnVat.setCellStyleNames("wordWrap");
	}

	// Read Only Discount Column
	public void addColumnviewDiscount() {
		columnDiscount = new TextColumn<ProductDetailsPO>() {
			@Override
			public String getValue(ProductDetailsPO object) {
				if (object.getDiscount() == 0) {
					return 0 + "";
				} else {
					return object.getDiscount() + "";
				}
			}
		};
		table.addColumn(columnDiscount, "% Discount");
		table.setColumnWidth(columnDiscount, 90, Unit.PX);
		columnDiscount.setCellStyleNames("wordWrap");
	}

	// Editable Discount Column
	public void addColumnDiscount() {
		EditTextCell cell = new EditTextCell();
		ecolumnDiscount = new Column<ProductDetailsPO, String>(cell) {
			@Override
			public String getValue(ProductDetailsPO object) {
				if (object.getDiscount() <= 0 || object.getDiscount() > 100)
					object.setDiscount(0);
				return object.getDiscount() + "";
			}
		};
		table.addColumn(ecolumnDiscount, "% Discount");
		table.setColumnWidth(ecolumnDiscount, 90, Unit.PX);
		ecolumnDiscount.setCellStyleNames("wordWrap");
	}

	
	
	
	// Read Only Discount Column
		public void addColumnviewDiscountAmt() {
			nonEditcolumnDiscount = new TextColumn<ProductDetailsPO>() {
				@Override
				public String getValue(ProductDetailsPO object) {
					if (object.getDiscountAmt() == 0) {
						return 0 + "";
					} else {
						return object.getDiscountAmt() + "";
					}
				}
			};
			table.addColumn(nonEditcolumnDiscount, "#Disc Amt");
			table.setColumnWidth(nonEditcolumnDiscount, 90, Unit.PX);
			nonEditcolumnDiscount.setCellStyleNames("wordWrap");
		}
	//***************Editable discount Amt column *************** 
	private void createColumnDiscountAmt() {
		EditTextCell editCell = new EditTextCell();
		discountAmt = new Column<ProductDetailsPO, String>(editCell) {
			@Override
			public String getValue(ProductDetailsPO object) {
				if (object.getDiscountAmt() == 0) {
					return 0 + "";
				} else {
					return object.getDiscountAmt() + "";
				}
			}
		};
		table.addColumn(discountAmt, "#Disc Amt");
		table.setColumnWidth(discountAmt, 90, Unit.PX);
		discountAmt.setCellStyleNames("wordWrap");
	}

	public void addColumnTotal() {
		TextCell editCell = new TextCell();
		columnTotal = new Column<ProductDetailsPO, String>(editCell) {
			@Override
			public String getValue(ProductDetailsPO object) {
				double total = 0;
				SuperProduct product = object.getPrduct();
				double tax = removeAllTaxes(product);
				double origPrice = object.getProdPrice() - tax;

				// ****************new code for discount by rohan
				// ******************

				if ((object.getDiscount() == null || object.getDiscount() == 0)
						&& (object.getDiscountAmt() == 0)) {
					total = origPrice * object.getProductQuantity();
				} else if ((object.getDiscount() != null)
						&& (object.getDiscountAmt() != 0)) {
					total = origPrice
							- (origPrice * object.getDiscount() / 100);
					total = total - object.getDiscountAmt();
					total = total * object.getProductQuantity();
				} else {
					if (object.getDiscount() != null) {
						total = origPrice
								- (origPrice * object.getDiscount() / 100);
					} else {
						total = origPrice - object.getDiscountAmt();
					}
					total = total * object.getProductQuantity();
				}
				return nf.format(total);
			}
		};
		table.addColumn(columnTotal, "Total");
		table.setColumnWidth(columnTotal, 90, Unit.PX);
		columnTotal.setCellStyleNames("wordWrap");
	}
	
	public void createColumndeleteColumn() {
		ButtonCell btnCell = new ButtonCell();
		deleteColumn = new Column<ProductDetailsPO, String>(btnCell) {
			@Override
			public String getValue(ProductDetailsPO object) {
				return "Delete";
			}
		};
		table.addColumn(deleteColumn, "Delete");
		table.setColumnWidth(deleteColumn, 90, Unit.PX);
		deleteColumn.setCellStyleNames("wordWrap");

	}
	/**************************************** Update product Column **********************************************/
	
	public void createFieldUpdaterDelete() {
		deleteColumn.setFieldUpdater(new FieldUpdater<ProductDetailsPO, String>() {
			@Override
			public void update(int index, ProductDetailsPO object,String value) {
//				System.out.println("PRODUCT LIST SIZE BEFOR : "+getDataprovider().getList().size());
//				getDataprovider().getList().remove(object);
//				table.redrawRow(index);
//				System.out.println("PRODUCT LIST SIZE AFTER : "+getDataprovider().getList().size());
//				PurchaseOrderPresenter.updateVendorTable();
				
				
				boolean updateVenTblFlag=checkSameProductExist(object.getProductID(),getDataprovider().getList());
				getDataprovider().getList().remove(object);
				table.redrawRow(index);
				if(updateVenTblFlag){
					QuickPurchaseOrderPreseneter.updateVendorTable();
				}
			}
			
		});
	}
	
	/**
	 * This method checks in product list ,whether the product which you want to delete has 2 or more entry ,
	 * in that case you dont need to update the vendor table.
	 * Date : 28-09-2016 By Anil 
	 * Release : 30 Sept 2016 
	 * Project : PURCHASE MODIFICATION(NBHC)
	 * I/P : Product Id and product list
	 * O/P: return false if product has multiple entry else true
	 */
	private boolean checkSameProductExist(int productID,List<ProductDetailsPO> list) {
		int count=0;
		for(ProductDetailsPO obj:list){
			if(obj.getProductID()==productID){
				count++;
			}
			if(count==2){
				return false;
			}
		}
		return true;
	}
	
	/**
	 * END
	 */
	
	protected void createFieldUpdaterprodQtyColumn() {
		columnProductQuantity.setFieldUpdater(new FieldUpdater<ProductDetailsPO, String>() {
			@Override
			public void update(int index, ProductDetailsPO object,String value) {
				try {
					double val1;
					if (Double.parseDouble(value.trim()) == 0) {
						val1 = 1;
						object.setProductQuantity(val1);
//						System.out.println("in 0" + val1);
						table.redrawRow(index);
					} else {
						val1 = Double.parseDouble(value.trim());
						object.setProductQuantity(val1);
					}
					
						RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);

				} catch (NumberFormatException e) {

				}
				table.redrawRow(index);
			}
		});
	}

	public void addColumnEditDiscount() {
		ecolumnDiscount.setFieldUpdater(new FieldUpdater<ProductDetailsPO, String>() {
			@Override
			public void update(int index, ProductDetailsPO object,String value) {
				try {
					Double val1 = Double.parseDouble(value.trim());
					object.setDiscount(val1);

						RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);

				} catch (NumberFormatException e) {

				}
				table.redrawRow(index);
			}
		});
	}

	protected void createFieldUpdaterprodPriceColumn() {
		columnProductPrice.setFieldUpdater(new FieldUpdater<ProductDetailsPO, String>() {
			@Override
			public void update(int index, ProductDetailsPO object,String value) {
				try {
					Double val1 = Double.parseDouble(value.trim());
					object.setProdPrice(val1);
						RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);

				} catch (NumberFormatException e) {

				}
				table.redrawRow(index);
			}
		});
	}

	
	

	protected void createFieldUpdaterDiscAmtColumn()
	{
		discountAmt.setFieldUpdater(new FieldUpdater<ProductDetailsPO, String>()
		{
		    @Override
			public void update(int index,ProductDetailsPO object,String value)
			{
				try{
					Double val1=Double.parseDouble(value.trim());
					object.setDiscountAmt(val1);
					
						RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				}
				catch (NumberFormatException e)
				{

				}
				table.redrawRow(index);
			}
		});
	}
	
	
	@Override
	protected void initializekeyprovider() {

	}

	@Override
	public void addFieldUpdater() {
		
		createFieldUpdaterproductNameColumn();

		
		createFieldUpdaterprodQtyColumn();
		createFieldUpdaterprodPriceColumn();
		addColumnEditDiscount();
		createFieldUpdaterDiscAmtColumn();
		updateServiceTaxColumn();
		updateVatTaxColumn();
		
		createFieldUpdaterDelete();
		
		setFieldUpdatorOnDeliveryDate();
		updateWarehouseBtn();
		
		/**
		 * nidhi 4-10-2018
		 * 
		 */
		if(addProSerialNo!=null){
			createFieldUpdaterAddSerialNo();
		}
		if(viewProSerialNo!=null){
			createFieldUpdaterViewSerialNo();
			
		}
	}
	
	
	protected void createFieldUpdaterproductNameColumn()
	{
		columnProdName.setFieldUpdater(new FieldUpdater<ProductDetailsPO, String>()
				{
			@Override

			public void update(int index,ProductDetailsPO object,String value)
			{

				try{
					String val1=value.trim();
					object.setProductName(val1);
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);

				}
				catch (NumberFormatException e)
				{

				}
				table.redrawRow(index);
			}
				});
	}
	
	private void updateWarehouseBtn() {
		
		addwarehouseBtnColumn.setFieldUpdater(new FieldUpdater<ProductDetailsPO, String>() {
			
			@Override
			public void update(int index, ProductDetailsPO object, String value) {
				
				panel=new PopupPanel(true);
				panel.add(invLoc);
				InventoryLocationPopUp.initialzeWarehouse(object.getPrduct().getCount());
				
				if(object.getItemProductWarehouseName()!=null&&object.getItemProductStrorageLocation()!=null
						&&object.getItemProductStorageBin()!=null){
					System.out.println("Inside table not null list settt...........");
					for(int i=0;i<InventoryLocationPopUp.getLsWarehouse().getItemCount();i++)
					{
						String data=InventoryLocationPopUp.lsWarehouse.getItemText(i);
						if(data.equals(object.getItemProductWarehouseName()))
						{
							InventoryLocationPopUp.lsWarehouse.setSelectedIndex(i);
							break;
						}
					}
					
					invLoc.getLsStorageLoc().clear();
					invLoc.lsStorageLoc.addItem("--SELECT--");
					invLoc.lsStorageLoc.addItem(object.getItemProductStrorageLocation());
					
					
					invLoc.getLsStoragebin().clear();
					invLoc.lsStoragebin.addItem("--SELECT--");
					
					for(int i=0;i<invLoc.arraylist2.size();i++){
						if(invLoc.arraylist2.get(i).getWarehouseName().equals(object.getItemProductWarehouseName())
								&&invLoc.arraylist2.get(i).getStorageLocation().equals(object.getItemProductStrorageLocation())){
							invLoc.lsStoragebin.addItem(invLoc.arraylist2.get(i).getStorageBin());
						}
					}
					
					for(int i=0;i<invLoc.getLsStorageLoc().getItemCount();i++)
					{
						String data=invLoc.lsStorageLoc.getItemText(i);
						if(data.equals(object.getItemProductStrorageLocation()))
						{
							invLoc.lsStorageLoc.setSelectedIndex(i);
							break;
						}
					}
					for(int i=0;i<invLoc.getLsStoragebin().getItemCount();i++)
					{
						String data=invLoc.lsStoragebin.getItemText(i);
						if(data.equals(object.getItemProductStrorageLocation()))
						{
							invLoc.lsStoragebin.setSelectedIndex(i);
							break;
						}
					}
				}
				
				
				
				invLoc.formView();
				panel.show();
				panel.center();
				rowIndex=index;
			}
		});
	}
	
	public void updateVatTaxColumn()
	{

		editVatColumn.setFieldUpdater(new FieldUpdater<ProductDetailsPO, String>() {
			
			@Override
			public void update(int index, ProductDetailsPO object, String value) {
				try {
					String val1 = (value.trim());
					object.setVatTaxEdit(val1);
					Tax tax = object.getVatTax();
					for(int i=0;i<vattaxlist.size();i++)
					{
						if(val1.trim().equals(vattaxlist.get(i).getTaxChargeName())){
							tax.setTaxName(val1);
							tax.setTaxConfigName(val1);
							tax.setPercentage(vattaxlist.get(i).getTaxChargePercent());
							object.setVat(vattaxlist.get(i).getTaxChargePercent());
							tax.setTaxPrintName(vattaxlist.get(i).getTaxPrintName());
							/**
							 * nidhi
							 * 8-06-2018
							 * for tax selection option
							 */
//							checkTaxValue(tax,object);
							Tax taxDt = AppUtility.checkTaxValue(tax, object.getServiceTax(), vattaxlist);
							if(taxDt != null){
								object.setServiceTax(taxDt);
								object.setVat(taxDt.getPercentage());
							}
							/**
							 * end
							 */
							
							break;
						}
					}
					object.setVatTax(tax);
						RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
					
				} catch (NumberFormatException e) {
				}
				table.redrawRow(index);
			}
		});
	}
	
	public void updateServiceTaxColumn()
	{
		editServiceColumn.setFieldUpdater(new FieldUpdater<ProductDetailsPO, String>() {
			@Override
			public void update(int index, ProductDetailsPO object, String value) {
				try {
//					System.out.println("Rohan 000000000000"+value);
					String val1 = (value.trim());
					object.setServiceTaxEdit(val1);
					Tax tax = new Tax();
					if(object.getServiceTax()!=null){
						tax=object.getServiceTax();
					}
					for(int i=0;i<servicetaxlist.size();i++)
					{
						if(val1.trim().equals(servicetaxlist.get(i).getTaxChargeName())){
//							System.out.println("Inside condition 111111111111111111"+servicetaxlist.get(i).getTaxChargeName());
							tax.setTaxName(val1);
							tax.setTaxConfigName(val1);
							tax.setPercentage(servicetaxlist.get(i).getTaxChargePercent());
							object.setTax(servicetaxlist.get(i).getTaxChargePercent());
							tax.setTaxPrintName(servicetaxlist.get(i).getTaxPrintName());
							/**
							 * nidhi
							 * 8-06-2018
							 * 
							 */
							//checkServiceTaxValue(tax, object);
//							Tax vatTax =AppUtility.checkse
							Tax vatTax = AppUtility.checkTaxValue(tax, object.getVatTax(), vattaxlist);
							if(vatTax!=null){
								object.setVatTax(vatTax);
								object.setTax(vatTax.getPercentage());
							}
							/**
							 * end
							 */
							
							
							break;
						}
					}
					object.setServiceTax(tax);
						RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				} catch (NumberFormatException e) {
				}
				table.redrawRow(index);
			}
		});
	}
	
	
	

	@Override
	public void setEnable(boolean state) {
		int tablecolcount = this.table.getColumnCount();
		for (int i = tablecolcount - 1; i > -1; i--)
			table.removeColumn(i);
		if (state == true)
			addeditColumn();
		if (state == false)
			addViewColumn();

	}

	@Override
	public void applyStyle() {

	}

	public double calculateTotal() {
		List<ProductDetailsPO> list = getDataprovider().getList();
		double total = 0;
		for (ProductDetailsPO object : list) {
			// double tax=object.getProdPrice()*(12.5/100);
			// double vat=object.getProdPrice()*(12.5/100);
			double discount = object.getProdPrice()* (object.getDiscount() / 100);
			total = object.getProdPrice() * object.getProductQuantity() + total
					- discount;
		}
		return total;
	}

	public double calculateTotalExcludingTax() {
		List<ProductDetailsPO> list = getDataprovider().getList();
		double sum = 0, priceVal = 0;
		
		for (int i = 0; i < list.size(); i++) {
			
			ProductDetailsPO entity = list.get(i);
			SuperProduct prod = entity.getPrduct();
			//return inclusive tax amount
			double taxAmt = removeAllTaxes(prod);
			
			if((entity.getDiscount()==null || entity.getDiscount()==0) && (entity.getDiscountAmt()==0) ){
//				System.out.println("inside both 0 condition");
				priceVal=entity.getProdPrice()-taxAmt;
				priceVal=priceVal*entity.getProductQuantity();
				sum=sum+priceVal;
//				System.out.println("RRRRRRRRRRRRRR sum"+sum);
			}
			else if((entity.getDiscount()!=null)&& (entity.getDiscountAmt()!=0)){
//				System.out.println("inside both not null condition");
				priceVal=entity.getProdPrice()-taxAmt;
				priceVal=priceVal-(priceVal*entity.getDiscount()/100);
				priceVal=priceVal-entity.getDiscountAmt();
				priceVal=priceVal*entity.getProductQuantity();
				sum=sum+priceVal;
//				System.out.println("RRRRRRRRRRRRRR sum"+sum);
			}
			else{
//				System.out.println("inside oneof the null condition");
				priceVal=entity.getProdPrice()-taxAmt;
				if(entity.getDiscount()!=null){
//					System.out.println("inside getPercentageDiscount oneof the null condition");
					priceVal=priceVal-(priceVal*entity.getDiscount()/100);
				}
				else{
//					System.out.println("inside getDiscountAmt oneof the null condition");
					priceVal=priceVal-entity.getDiscountAmt();
				}
				priceVal=priceVal*entity.getProductQuantity();
				sum=sum+priceVal;
//				System.out.println("RRRRRRRRRRRRRR sum"+sum);
			}
			
		}
		return sum;
	}

	/**
	 * Commented By Anil
	 * This method return inclusive tax amount 
	 * its returns amount only if product has inclusive tax 
	 * Date : 23-11-2016 By ANIL
	 * Project : DEADSTOCK(NBHC) 
	 */
	
	public double removeAllTaxes(SuperProduct entity) {
		
		double vat = 0, service = 0;
		double tax = 0, retrVat = 0, retrServ = 0;
		
		if (entity instanceof ServiceProduct) {
			
			ServiceProduct prod = (ServiceProduct) entity;
			if (prod.getServiceTax()!= null&& prod.getServiceTax().isInclusive() == true) {
				service = prod.getServiceTax().getPercentage();
			}
			if (prod.getVatTax() != null&& prod.getVatTax().isInclusive() == true) {
				vat = prod.getVatTax().getPercentage();
			}
		}

		if (entity instanceof ItemProduct) {
			ItemProduct prod = (ItemProduct) entity;
			if (prod.getVatTax() != null&& prod.getVatTax().isInclusive() == true) {
				vat = prod.getVatTax().getPercentage();
			}
			if (prod.getServiceTax() != null&& prod.getServiceTax().isInclusive() == true) {
				service = prod.getServiceTax().getPercentage();
			}
		}

		if (vat != 0 && service == 0) {
			retrVat = (entity.getPrice() / (1 + (vat / 100)));
			retrVat = entity.getPrice() - retrVat;
		}
		if (service != 0 && vat == 0) {
			retrServ = (entity.getPrice() / (1 + service / 100));
			retrServ = entity.getPrice() - retrServ;
		}
		if (service != 0 && vat != 0) {


			/**if condition added by vijay and old code added in else block
			 * Date 2 August  2017 added by vijay for GST
			 */
			
			ItemProduct prod=(ItemProduct) entity;
			if(prod.getServiceTax().getTaxPrintName()!=null && ! prod.getServiceTax().getTaxPrintName().equals("")
			   && prod.getVatTax().getTaxPrintName()!=null && ! prod.getVatTax().getTaxPrintName().equals(""))
			{

				double dot = service + vat;
				retrServ=(entity.getPrice()/(1+dot/100));
				retrServ=entity.getPrice()-retrServ;
//				retrVat=0;

			}else{
				
			
			// Here if both are inclusive then first remove service tax and then
			// on that amount
			// calculate vat.
			double removeServiceTax = (entity.getPrice() / (1 + service / 100));

			// double taxPerc=service+vat;
			// retrServ=(entity.getPrice()/(1+taxPerc/100)); //line changed
			// below
			retrServ = (removeServiceTax / (1 + vat / 100));
			retrServ = entity.getPrice() - retrServ;
			
			}
		}
		tax = retrVat + retrServ;
		return tax;
	}
	
	

	public boolean removeChargesOnDelete() {

		int tableSize = getDataprovider().getList().size();

		if (tableSize < 1) {
			return false;
		}
		return true;
	}


	
	private void createColumnprodHSNCodeColumn() {

		EditTextCell editCell=new EditTextCell();
		hsnCodeColumn = new Column<ProductDetailsPO, String>(editCell) {
			
			@Override
			public String getValue(ProductDetailsPO object) {
				if(object.getPrduct().getHsnNumber()!=null && !object.getPrduct().getHsnNumber().equals("")){
					return object.getPrduct().getHsnNumber();
				}
				else{
					return "";
				}
			}
		};
		table.addColumn(hsnCodeColumn,"#HSN Code");
		table.setColumnWidth(hsnCodeColumn, 100,Unit.PX);
	}
	
	private void createColumnViewHSNCodeColumn() {
		viewHsnCodeColumn = new TextColumn<ProductDetailsPO>() {
			
			@Override
			public String getValue(ProductDetailsPO object) {
				if(object.getPrduct().getHsnNumber()!=null && !object.getPrduct().getHsnNumber().equals(""))
				return object.getPrduct().getHsnNumber();
				
				return "";
			}
		};
		table.addColumn(viewHsnCodeColumn,"#HSN Code");
		table.setColumnWidth(viewHsnCodeColumn, 100,Unit.PX);
	}
	
	
	public void addExpDeliveryDateColoumn(){
		expDeliveryDateColumn=new TextColumn<ProductDetailsPO>() {
			@Override
			public String getValue(ProductDetailsPO object) {
				if(object.getProdDate()!=null){
					return AppUtility.parseDate(object.getProdDate());
				}
				return "";
			}
		};
		table.addColumn(expDeliveryDateColumn, "#Delivery Date");
		table.setColumnWidth(expDeliveryDateColumn, 110, Unit.PX);
		expDeliveryDateColumn.setCellStyleNames("wordWrap");
	}
	public void addEditableExpDeliveryDateColoumn(){
		final DateTimeFormat fmt = DateTimeFormat.getFormat("dd/MM/yyyy");
		DatePickerCell date= new DatePickerCell(fmt);
		editableExpDeliveryDateColumn=new Column<ProductDetailsPO,Date>(date) {
			@Override
			public Date getValue(ProductDetailsPO object) {
				if(object.getProdDate()!=null){
					return fmt.parse(fmt.format(object.getProdDate()));
				}else{
					object.setProdDate(new Date());
					return fmt.parse(fmt.format(new Date()));
				}
			}
		};
		table.addColumn(editableExpDeliveryDateColumn, "#Delivery Date");
		table.setColumnWidth(editableExpDeliveryDateColumn, 110, Unit.PX);
		editableExpDeliveryDateColumn.setCellStyleNames("wordWrap");
	}
	
	
	private void setFieldUpdatorOnDeliveryDate(){
		editableExpDeliveryDateColumn.setFieldUpdater(new FieldUpdater<ProductDetailsPO, Date>() {
			@Override
			public void update(int index, ProductDetailsPO object, Date value) {
				object.setProdDate(value);
				table.redrawRow(index);				
			}
		});
	}
	
	
	
	
	public void showWaitSymbol() {
		glassPanel = new GWTCGlassPanel();
		glassPanel.show();
		
		
	}
	public void hideWaitSymbol() {
	   glassPanel.hide();
		
	}
	
	
		@Override
		public void onClick(ClickEvent event) {
			if(event.getSource()==invLoc.getAddButton())
			{	
				if(invLoc.getLsWarehouse().getSelectedIndex()!=0&&invLoc.getLsStorageLoc().getSelectedIndex()!=0&&invLoc.getLsStoragebin().getSelectedIndex()!=0){
					ArrayList<ProductDetailsPO> list=new ArrayList<ProductDetailsPO>();
					if(getDataprovider().getList().size()!=0){
						list.addAll(getDataprovider().getList());
						for( int i=rowIndex;i<getDataprovider().getList().size();i++){
							list.get(rowIndex).setItemProductWarehouoseName(invLoc.getLsWarehouse().getValue(invLoc.getLsWarehouse().getSelectedIndex()));
							list.get(rowIndex).setItemProductStorageLocation(invLoc.getLsStorageLoc().getValue(invLoc.getLsStorageLoc().getSelectedIndex()));
							list.get(rowIndex).setItemStorageBin(invLoc.getLsStoragebin().getValue(invLoc.getLsStoragebin().getSelectedIndex()));
							list.get(rowIndex).setItemProductAvailableQty(invLoc.getAvailableQty());
							
							getDataprovider().getList().clear();
							getDataprovider().getList().addAll(list);
							
						}
					}
					panel.hide();
				}
				if(invLoc.getLsWarehouse().getSelectedIndex()==0&&invLoc.getLsStorageLoc().getSelectedIndex()==0&&invLoc.getLsStoragebin().getSelectedIndex()==0){
					SalesOrderPresenter.showMessage("Please Select Warehouse.");
				}
				else if(invLoc.getLsStorageLoc().getSelectedIndex()==0&&invLoc.getLsStoragebin().getSelectedIndex()==0){
					SalesOrderPresenter.showMessage("Please Select Storage Location.");
				}
				else if(invLoc.getLsStoragebin().getSelectedIndex()==0){
					SalesOrderPresenter.showMessage("Please Select Storage Bin.");
				}
				
			}
			if(event.getSource()==invLoc.getCancelButton())
			{	
				panel.hide();
			}
			
			/**
			 * nidhi
			 * 22-08-2018
			 */
			if(event.getSource() == prodSerialPopup.getLblOk()){
				ArrayList<ProductSerialNoMapping> proList = new ArrayList<ProductSerialNoMapping>();
//				proList.addAll(prodSerialPopup.getProSerNoTable().getDataprovider().getList());
				ArrayList<ProductDetailsPO> list=new ArrayList<ProductDetailsPO>();
				list.addAll(getDataprovider().getList());
				List<ProductSerialNoMapping> mainproList = prodSerialPopup.getProSerNoTable().getDataprovider().getList();
				int proser  = 0;
				
				for(int i =0 ; i <mainproList.size();i++){
					System.out.println(mainproList.get(i).getProSerialNo() + "  --- get serial number -- ");
					if(mainproList.get(i).getProSerialNo().trim().length()>0){
						proList.add(mainproList.get(i));
						proser++;
					}
				}
				if(proList != null  && proser>0 && proser!= (int) list.get(rowIndex).getProductQuantity())
				{
					final GWTCAlert alert = new GWTCAlert(); 
				     alert.alert("Please add serial number as per product quantity");
				     return ; 
				}
				if(list.get(rowIndex).getProSerialNoDetails()!=null){
					list.get(rowIndex).getProSerialNoDetails().clear();
					list.get(rowIndex).getProSerialNoDetails().put(0, proList);
					getDataprovider().getList().clear();
					getDataprovider().getList().addAll(list);
				}else{
					HashMap<Integer, ArrayList<ProductSerialNoMapping>> pro = new HashMap<Integer, ArrayList<ProductSerialNoMapping>>();
					pro.put(0, proList);
						list.get(rowIndex).setProSerialNoDetails(pro);
						getDataprovider().getList().clear();
						getDataprovider().getList().addAll(list);
				}
				prodSerialPopup.hidePopUp();
			}
			if(event.getSource()== prodSerialPopup.getLblCancel()){
				prodSerialPopup.hidePopUp();
			}
		}
		
		public void createColumnAddProSerialNoColumn() {
			ButtonCell btnCell = new ButtonCell();
			addProSerialNo = new Column<ProductDetailsPO, String>(btnCell) {
				@Override
				public String getValue(ProductDetailsPO object) {
					return "Product Serial Nos";
				}
			};
			table.addColumn(addProSerialNo, "");
			table.setColumnWidth(addProSerialNo,180,Unit.PX);

		}
		public void createFieldUpdaterAddSerialNo() {
			addProSerialNo.setFieldUpdater(new FieldUpdater<ProductDetailsPO, String>() {
				@Override
				public void update(int index, ProductDetailsPO object, String value) {
					rowIndex = index;
					prodSerialPopup.setProSerialenble(true,false);
					prodSerialPopup.getPopup().setSize("50%", "40%");
					prodSerialPopup.showPopUp();
					if(object.getProSerialNoDetails()!=null && object.getProSerialNoDetails().size()>0){
						ArrayList<ProductSerialNoMapping> serNo = object.getProSerialNoDetails().get(0);
						if(serNo.size() < object.getProductQuantity()){
							int count = (int) (object.getProductQuantity()- serNo.size());
							for(int i = 0 ; i < count ; i ++ ){
								ProductSerialNoMapping pro = new ProductSerialNoMapping();
								serNo.add(pro);
							}
							prodSerialPopup.getProSerNoTable().getDataprovider().getList().clear();
							prodSerialPopup.getProSerNoTable().getDataprovider().getList().addAll(serNo);
							prodSerialPopup.getProSerNoTable().getTable().redraw();
						}else{

							prodSerialPopup.getProSerNoTable().getDataprovider().getList().clear();
							prodSerialPopup.getProSerNoTable().getDataprovider().getList().addAll(serNo);
							prodSerialPopup.getProSerNoTable().getTable().redraw();
						}
					}else{

						ArrayList<ProductSerialNoMapping> serNo = new ArrayList<ProductSerialNoMapping>();
							for(int i = 0 ; i < object.getProductQuantity() ; i ++ ){
								ProductSerialNoMapping pro = new ProductSerialNoMapping();
								pro.setProSerialNo("");
								serNo.add(pro);
							}
							prodSerialPopup.getProSerNoTable().getDataprovider().getList().clear();
							prodSerialPopup.getProSerNoTable().getDataprovider().getList().addAll(serNo);
							prodSerialPopup.getProSerNoTable().getTable().redraw();
					}
					
				
				}
			});
		}
		
		public void createFieldUpdaterViewSerialNo() {
			viewProSerialNo.setFieldUpdater(new FieldUpdater<ProductDetailsPO, String>() {
				@Override
				public void update(int index, ProductDetailsPO object, String value) {
					prodSerialPopup.setProSerialenble(false,false);
					prodSerialPopup.getPopup().setSize("50%", "40%");
					if(object.getProSerialNoDetails()!=null && object.getProSerialNoDetails().size()>0){
						ArrayList<ProductSerialNoMapping> serNo = new ArrayList<ProductSerialNoMapping>();
//						for(int i = 0 ; i < object.getProSerialNoDetails().get(0).size() ; i ++ ){
//							ProductSerialNoMapping pro = new ProductSerialNoMapping();
					System.out.println("size  -- " + serNo.size());
							serNo.addAll(object.getProSerialNoDetails().get(0));
//						}
							prodSerialPopup.getProSerNoTable().getDataprovider().getList().clear();
						prodSerialPopup.getProSerNoTable().getDataprovider().getList().addAll(serNo);
						prodSerialPopup.getProSerNoTable().getTable().redraw();
					}
					prodSerialPopup.showPopUp();
				
				}
			});
		}
		
		public void createViewColumnAddProSerialNoColumn() {
			ButtonCell btnCell = new ButtonCell();
			viewProSerialNo = new Column<ProductDetailsPO, String>(btnCell) {
				@Override
				public String getValue(ProductDetailsPO object) {
					return "View Product Serial Nos";
				}
			};
			table.addColumn(viewProSerialNo, "");
			table.setColumnWidth(viewProSerialNo,180,Unit.PX);

		}
		
	}
