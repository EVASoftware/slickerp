package com.slicktechnologies.client.views.purchase.quickpurchase;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.inventory.GRN;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;

public class QuickPurchaseGRNTable extends SuperTable<GRN>{

	
	TextColumn<GRN> getColumnPurchaseOrderId;
	TextColumn<GRN> getColumnGRNId;
	TextColumn<GRN> getColumnVendorName;
	TextColumn<GRN> getColumnBranch;
	TextColumn<GRN> getColumnStatus;
	TextColumn<GRN> getColumnGRNDate;
	
	public QuickPurchaseGRNTable(){
		createGui();
		setHeight("350px");
	}
	
	@Override
	public void createTable() {

		createColumnPOId();
		createColumnGRNId();
		createColumnVendorName();
		createColumnBranch();
		createColumnStatus();
		createColumnGRNDate();
	}

	private void createColumnPOId() {

		getColumnPurchaseOrderId = new TextColumn<GRN>() {
			
			@Override
			public String getValue(GRN object) {
				// TODO Auto-generated method stub
				return object.getPoNo()+"";
			}
		};
		table.addColumn(getColumnPurchaseOrderId,"PO Id");
		table.setColumnWidth(getColumnPurchaseOrderId, 110,Unit.PX);
	}

	private void createColumnGRNId() {

		getColumnGRNId = new TextColumn<GRN>() {
			
			@Override
			public String getValue(GRN object) {
				// TODO Auto-generated method stub
				return object.getCount()+"";
			}
		};
		table.addColumn(getColumnGRNId,"GRN Id");
		table.setColumnWidth(getColumnGRNId, 110,Unit.PX);
	}

	private void createColumnVendorName() {

		getColumnVendorName = new TextColumn<GRN>() {
			
			@Override
			public String getValue(GRN object) {
				if(object.getVendorInfo()!=null)
					return object.getVendorInfo().getFullName();
				return "";
			}
		};
		table.addColumn(getColumnVendorName,"Vendor Name");
		table.setColumnWidth(getColumnVendorName, 110,Unit.PX);
	}

	private void createColumnBranch() {

		getColumnBranch = new TextColumn<GRN>() {
			
			@Override
			public String getValue(GRN object) {
				if(object.getBranch()!=null)
				return object.getBranch();
				return "";
			}
		};
		table.addColumn(getColumnBranch,"Branch");
		table.setColumnWidth(getColumnBranch, 110,Unit.PX);
	}

	private void createColumnStatus() {

		getColumnStatus = new TextColumn<GRN>() {
			
			@Override
			public String getValue(GRN object) {
				if(object.getStatus()!=null)
				return object.getStatus();
				return "";
			}
		};
		table.addColumn(getColumnStatus,"Status");
		table.setColumnWidth(getColumnStatus, 110,Unit.PX);
	}

	private void createColumnGRNDate() {

		getColumnGRNDate = new TextColumn<GRN>() {
			
			@Override
			public String getValue(GRN object) {
				if(object.getCreationDate()!=null){
					return AppUtility.parseDate(object.getCreationDate());
				}
				return "";
			}
		};
		table.addColumn(getColumnGRNDate,"GRN Date");
		table.setColumnWidth(getColumnGRNDate, 110,Unit.PX);
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
