package com.slicktechnologies.client.views.purchase.productrequisite;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.client.views.serviceliaison.liaisonsteps.ReponseStepInfoTable;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductRequisite;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;



public class ProductRequisiteForm extends FormTableScreen<ProductRequisite> implements ClickHandler{
	IntegerBox productRequisiteId;
	TextBox stepsName;
	TextBox tbresponsestep;
	TextArea description;
	CheckBox status,mandatory;
	
	IntegerBox days,ibresponseSLA;
	

	Button addResponse;
	CheckBox cbreponsemandatory;
	
	TextArea tadescriptionResponse;
	ProductITems table;
	InlineLabel label;
	ProductInfoComposite pic;
	ObjectListBox<ProductRequisite> subitems;
	
	
	public ProductRequisiteForm(SuperTable<ProductRequisite> table, int mode,
			boolean captionmode) {
		super(table, mode, captionmode);
		createGui();
		table.connectToLocal();
	}

	private void initalizeWidget()
	{
		productRequisiteId=new IntegerBox();
		productRequisiteId.setEnabled(false);
		stepsName=new TextBox();
		description=new TextArea();
		status=new CheckBox();
		status.setValue(true);
		days=new IntegerBox();
		mandatory=new CheckBox();
		mandatory.addClickHandler(this);
		
		tbresponsestep=new TextBox();
		ibresponseSLA=new IntegerBox();
		cbreponsemandatory=new CheckBox();
		
		tadescriptionResponse=new TextArea();
		addResponse=new Button("Add Items");
		addResponse.addClickHandler(this);
		table=new ProductITems();
		label=new InlineLabel("*For edit Product Items delete and add again");
		this.initalizeSteps();
		pic=AppUtility.product(new SuperProduct(),true);
		subitems=new ObjectListBox<ProductRequisite>();
		
	}

	/*
	 * Method template to create the formtable screen
	 */

	@Override
	public void createScreen() {

		//Token to initialize the processlevel menus.
		
		
		initalizeWidget();

		
		//////////////////////////////////Form Field Declaration/////////////////////////////////////////////////

		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////

		//Token to initialize formfield
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingStepsInformation=fbuilder.setlabel("Pruduct Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(3).build();
		fbuilder = new FormFieldBuilder("Status",status);
		FormField fstatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
					
		fbuilder = new FormFieldBuilder("Mandatory",mandatory);
		FormField fmandatory= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	
		FormField fgroupingResponseInformation=fbuilder.setlabel("Product Items Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(3).build();
		fbuilder = new FormFieldBuilder("",addResponse);
		FormField faddResponse= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		FormField fgroupingResponseStepsInformation=fbuilder.setlabel("Response steps").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(3).build();
		
		
		
		fbuilder = new FormFieldBuilder("",table.getTable());
		FormField ftable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();

		fbuilder = new FormFieldBuilder("",label);
		FormField flabel= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	
		fbuilder = new FormFieldBuilder("",pic);
		FormField fpic= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(6).build();
	
		fbuilder = new FormFieldBuilder("Items",subitems);
		FormField fitems= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	
		
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////


		FormField[][] formfield = {   {fgroupingStepsInformation},
				{fpic},
				{},{fstatus}
				,{fgroupingResponseInformation},
				{fitems,faddResponse},
				{flabel},{fgroupingResponseStepsInformation},
				{ftable}
		};


		this.fields=formfield;		
	}

	
	/**
	 * method template to update the model with token entity name
	 */
	@Override
	public void updateModel(ProductRequisite model) 
	{
		presenter.setModel(model);
	}

	/**
	 * method template to update the view with token entity name
	 */
	@Override
	public void updateView(ProductRequisite view) 
	{
		
		presenter.setModel(view);
	}

	/**
	 * Toggles the app header bar menus as per screen state
	 */

	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.PRODUCTREQUISITE,LoginPresenter.currentModule.trim());
	}

	/**
	 * sets the id textbox with the passed count value. 
	 */
	@Override
	public void setCount(int count)
	{
		this.productRequisiteId.setValue(presenter.getModel().getCount());
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		super.setEnable(state);
		productRequisiteId.setEnabled(false);
	}
	
	@Override
	public void onClick(ClickEvent event)
	{
		
		
		if(event.getSource()==this.addResponse)
		{
			if(validateSubstep()&&tbresponsestep.getValue()!=null)
			{
		
			
		/*	LiaisonSubstep info=new LiaisonSubstep();
			info.setReponseName(tbresponsestep.getValue());
			info.setReponseSLA(ibresponseSLA.getValue());
			info.setReponseMandatory(cbreponsemandatory.getValue());
			
			info.setReponseDescription(tadescriptionResponse.getValue());
			table.getDataprovider().getList().add(info);
			
			tbresponsestep.setValue("");
			ibresponseSLA.setValue(null);
			tadescriptionResponse.setValue("");
		*/
			}
			}
	
	}
	protected void initalizeSteps()
	{
		
		
	}
	
	@Override
	public boolean validate()
	{
		boolean superRes= super.validate();
		if(superRes==false)
			return false;
	
	if(table.getDataprovider().getList().isEmpty()==true)
	{
		this.showDialogMessage("Please Add Reponse Steps");
		return false;
	}
	
	
	if(stepsName.getValue().trim().equals((null)))
	{
		this.showDialogMessage("Request step and Parent step cant be same");
		return false;
	}
		return true;
	}
	

	
	
	
	@Override
	public void clear()
	{
		super.clear();
		table.clear();
	}
	
	public boolean validateSubstep()
	{
		boolean val=true;
		
		if(tbresponsestep.getValue().trim().equals(""))
		{
			this.showDialogMessage("Request Step name is mandatory");
			return false;
		}
		
		if(ibresponseSLA.getValue()==null)
		{
			this.showDialogMessage("Request step SLA is mandatory");
			return false;
		}
		if(tadescriptionResponse.getValue().equals(""))
		{
			this.showDialogMessage("Request step Description is mandatory");
			return false;
		}
		
		/*List<LiaisonSubstep> list=table.getDataprovider().getList();
		if(list.size()!=0)
		{
			
				String name=tbresponsestep.getValue();
				for(int i=0;i<list.size();i++)
				{
					if(name.equals(list.get(i).getReponseName()))
					{
						this.showDialogMessage("This Request step already exists in table");
						return false;
					}
				}
			
		}*/
		return true;
		
	}

	@Override
	public void setToViewState()
	{
		super.setToViewState();
		table.setEnable(false);
	}

	@Override
	public void setToEditState()
	{
		super.setToEditState();
		table.setEnable(true);
	}

	
}
