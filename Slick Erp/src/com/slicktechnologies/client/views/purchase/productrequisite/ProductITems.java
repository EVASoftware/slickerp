package com.slicktechnologies.client.views.purchase.productrequisite;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;



import com.slicktechnologies.shared.common.serviceLiaisonLayer.LiaisonSubstep;

public class ProductITems extends SuperTable<LiaisonSubstep>
{

	TextColumn<LiaisonSubstep> name;
	TextColumn<LiaisonSubstep> SLA;
	TextColumn<LiaisonSubstep> mandatory;
	
	TextColumn<LiaisonSubstep> description;
	private Column<LiaisonSubstep, String> delete;
	public ProductITems()
	{
		super();
	}
	
	@Override
	public void createTable() {
		addColumnName();
		addColumnSLA();
		
		addColumnMandatory();
		
		addColumnDescription();
		addColumnDelete();
		setFieldUpdaterOnDelete();
		
	}
	public void addColumnName()
	{
		name=new TextColumn<LiaisonSubstep>() {
			
			@Override
			public String getValue(LiaisonSubstep object) {
				// TODO Auto-generated method stub
				return object.getReponseName();
			}
		};table.addColumn(name,"ID");
	}
	
	public void addColumnSLA()
	{
		SLA=new TextColumn<LiaisonSubstep>() {
			
			@Override
			public String getValue(LiaisonSubstep object) {
				// TODO Auto-generated method stub
				return object.getReponseSLA()+"";
			}
		};table.addColumn(SLA,"Code");
	}
	
	public void addColumnMandatory()
	{
		mandatory=new TextColumn<LiaisonSubstep>() {
			
			@Override
			public String getValue(LiaisonSubstep object) {
				// TODO Auto-generated method stub
				return object.getReponseMandatory()+"";
			}
		};table.addColumn(mandatory,"Name");
	}
	
	
	
	public void addColumnDescription()
	{
		description=new TextColumn<LiaisonSubstep>() {
			
			@Override
			public String getValue(LiaisonSubstep object) {
				// TODO Auto-generated method stub
				return object.getReponseDescription();
			}
		};table.addColumn(description,"category");
	}
	
	public void addColumnDelete()
	{
		ButtonCell btnCell= new ButtonCell();
		delete=new Column<LiaisonSubstep, String>(btnCell) {
			
			@Override
			public String getValue(LiaisonSubstep object) {
				// TODO Auto-generated method stub
				return "delete";
			}
		};table.addColumn(delete,"Delete");
	}
	
	private void setFieldUpdaterOnDelete()
	{
		delete.setFieldUpdater(new FieldUpdater<LiaisonSubstep, String>() {
			
			@Override
			public void update(int index, LiaisonSubstep object, String value) {
				getDataprovider().getList().remove(index);
				table.redrawRow(index);
			
			}
		});
	}
	
	
	@Override
	protected void initializekeyprovider() {
		
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		int tablecolcount=this.table.getColumnCount();
		for(int i=tablecolcount-1;i>-1;i--)
		  table.removeColumn(i);
		if(state ==true)
			createTable();
		if(state==false)
			addViewColumn();
		
	}
	
	public void addViewColumn()
	{
		addColumnName();
		addColumnSLA();
		
		addColumnMandatory();
		
		addColumnDescription();

	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
	

}
