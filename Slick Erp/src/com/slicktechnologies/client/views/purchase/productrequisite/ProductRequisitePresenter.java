package com.slicktechnologies.client.views.purchase.productrequisite;

import com.google.gwt.event.dom.client.ClickEvent;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreenPresenter;


import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.views.serviceliaison.liaisonsteps.LiaisonStepPresenterTableProxy;
import com.slicktechnologies.client.views.serviceliaison.liaisonsteps.LiaisonStepsForm;
import com.slicktechnologies.client.views.serviceliaison.liaisonsteps.LiaisonStepsPresenter.LiaisonStepsPresenterTable;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductRequisite;
import com.slicktechnologies.shared.common.serviceLiaisonLayer.LiaisonStep;


public class ProductRequisitePresenter extends FormTableScreenPresenter<ProductRequisite>{

	public ProductRequisiteForm form;
	public ProductRequisitePresenter(FormTableScreen<ProductRequisite> view,
			ProductRequisite model) {
		super(view, model);
		form=(ProductRequisiteForm) view;
		form.setPresenter(this);
		
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}
	
	public static void initialize()
	{
	
		
		ProductRequisitePresenterTableProxy gentableScreen=new ProductRequisitePresenterTableProxy();
		ProductRequisiteForm form=new ProductRequisiteForm(gentableScreen, FormTableScreen.UPPER_MODE, true);
		gentableScreen.setView(form);
		gentableScreen.applySelectionModle();

		ProductRequisitePresenter  presenter=new  ProductRequisitePresenter(form,new ProductRequisite());
		AppMemory.getAppMemory().stickPnel(form);
		
		
	}

	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.serviceLiaisonLayer.ProductRequisite")
	 public static class  ProductRequisitePresenterTable extends SuperTable<LiaisonStep> implements GeneratedVariableRefrence{
		
			
		
	
	@Override
	public Object getVarRef(String varName) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void createTable() {
		// TODO Auto-generated method stub
		
	}
	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
		};
		
		@Override
		protected void makeNewModel() {
			model=new ProductRequisite();
			
		}

}
