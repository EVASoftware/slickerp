package com.slicktechnologies.client.views.purchase.vendorproductlist;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.view.client.NoSelectionModel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreen;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreenPresenter;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.inventory.productinventoryview.ProductInventoryViewForm;
import com.slicktechnologies.client.views.inventory.productinventoryview.ProductInventoryViewPresenter;
import com.slicktechnologies.client.views.purchase.vendorPriceList.VendorPriceListForm;
import com.slicktechnologies.client.views.purchase.vendorPriceList.VendorPriceListPresenter;
import com.slicktechnologies.client.views.purchase.vendorPriceList.VendorPriceListPresenterSearchProxy;
import com.slicktechnologies.client.views.purchase.vendorPriceList.VendorPriceListPresenter.VendorPricelistPresenterSearch;
import com.slicktechnologies.client.views.purchase.vendorPriceList.VendorPriceListPresenter.VendorPricelistPresenterTable;
import com.slicktechnologies.client.views.purchase.vendorPriceList.VendorPriceListPresenterTableProxy;
import com.slicktechnologies.shared.common.businessprocesslayer.PriceList;
import com.slicktechnologies.shared.common.businessprocesslayer.VendorPriceListDetails;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class VendorProductListPresenter extends TableScreenPresenter<VendorPriceListDetails>{

	TableScreen<VendorPriceListDetails>form;
	final GenricServiceAsync async=GWT.create(GenricService.class);
	public VendorProductListPresenter(TableScreen<VendorPriceListDetails> view,
			VendorPriceListDetails model) {
		super(view, model);
		form=view;
//		view.retriveTable(getVendorProductListQuery());//Ashwini Patil Date:23-08-2024 to stop auto loading
		setTableSelectionOnProducts();
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.VENDORPRODUCTLIST,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}

	}

	
	public VendorProductListPresenter(TableScreen<VendorPriceListDetails> view,
			VendorPriceListDetails model,MyQuerry querry) {
		super(view, model);
		form=view;
//		view.retriveTable(getVendorProductListQuery());//Ashwini Patil Date:23-08-2024 to stop auto loading
		setTableSelectionOnProducts();
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.VENDORPRODUCTLIST,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}

	}
	@Override
	public void reactOnPrint() {
		
	}

	@Override
	public void reactOnEmail() {
		
	}
	@Override
	protected void makeNewModel() {
		model=new VendorPriceListDetails();
	}
	
	public static void initalize()
	{
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Vendor Product Price List",Screen.VENDORPRODUCTLIST);
		VendorPriceListTableProxy gentable=new VendorPriceListTableProxy();
		VendorProductListForm form=new VendorProductListForm(gentable);
	
		
		VendorProductSearchPopUp.staticSuperTable=gentable;
		VendorProductSearchPopUp searchpopup=new VendorProductSearchPopUp(false);

		form.setSearchpopupscreen(searchpopup);
		VendorProductListPresenter presenter=new VendorProductListPresenter(form, new VendorPriceListDetails());
		form.setToNewState();
		AppMemory.getAppMemory().stickPnel(form);

		  
	}

	
	public static void initalize(MyQuerry querry)
	{
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Vendor Product Price List",Screen.VENDORPRODUCTLIST);
		VendorPriceListTableProxy gentable=new VendorPriceListTableProxy();
	
		VendorProductListForm form=new VendorProductListForm(gentable);
		VendorProductSearchPopUp.staticSuperTable=gentable;
		VendorProductSearchPopUp searchpopup=new VendorProductSearchPopUp(false);
		form.setSearchpopupscreen(searchpopup);
		
		VendorProductListPresenter presenter=new VendorProductListPresenter(form, new VendorPriceListDetails(),querry);
		form.setToNewState();
		AppMemory.getAppMemory().stickPnel(form);

		  
	}
	
	public MyQuerry getVendorProductListQuery()
	{
		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new VendorPriceListDetails());
		return quer;
	}
	
	
	public void setTableSelectionOnProducts()
	{
		 final NoSelectionModel<VendorPriceListDetails> selectionModelMyObj = new NoSelectionModel<VendorPriceListDetails>();
		 SelectionChangeEvent.Handler  tableHandler = new SelectionChangeEvent.Handler()
		 {
			@Override
			public void onSelectionChange(SelectionChangeEvent event) {
				final VendorPriceListDetails entity=selectionModelMyObj.getLastSelectedObject();
	        	AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
	     
	        	AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Purchase/Vendor Price List",Screen.VENDORPRICELIST);
	        	final VendorPriceListForm form = VendorPriceListPresenter.initalize();
	        
	        	AppMemory.getAppMemory().stickPnel(form);
	        	 
	        	final MyQuerry querry=new MyQuerry();
	        	Vector<Filter> filtervec=new Vector<Filter>();
	        	Filter temp=null;
	        	 
	        	temp=new Filter();
	        	temp.setLongValue(entity.getCompanyId());
	        	temp.setQuerryString("companyId");
	        	filtervec.add(temp);
	        	 

	        	temp=new Filter();
	        	temp.setIntValue(entity.getPriceListId());
	        	temp.setQuerryString("count");
	        	filtervec.add(temp);
	        	 
	        	querry.setFilters(filtervec);
	        	querry.setQuerryObject(new PriceList());
	        	 
	        	 
	        	form.showWaitSymbol();
					
	        	Timer timer=new Timer() 
	        	{
	 				@Override
	 				public void run() {
	 					
	 					async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {

	 						@Override
	 						public void onFailure(Throwable caught) {
	 							form.showDialogMessage("An Unexpected Error occured !");
	 							form.hideWaitSymbol();
	 						}
	 						@Override
	 						public void onSuccess(ArrayList<SuperModel> result) {
	 							
	 							
	 							for(SuperModel pmodel:result)
	 							{
	 								PriceList pgi=(PriceList)pmodel;
	 								form.updateView(pgi);
	 								form.setToViewState();
	 							}
	 							
	 						}
	 					});

	 					
	 					form.hideWaitSymbol();
					    
	 			             
	 				}
	 			};
	            
	             timer.schedule(3000); 
			}
		 };
		 selectionModelMyObj.addSelectionChangeHandler( tableHandler );
	     form.getSuperTable().getTable().setSelectionModel(selectionModelMyObj);
	}
	
	
	
	
	@Override
	public void reactOnDownload()
	{
		CsvServiceAsync async=GWT.create(CsvService.class);
		ArrayList<VendorPriceListDetails> vendorarray=new ArrayList<VendorPriceListDetails>();
		List<VendorPriceListDetails> list=form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
		
		vendorarray.addAll(list); 
		
		
		async.setVendorProductList(vendorarray, new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);
				
			}

			@Override
			public void onSuccess(Void result) {
				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "csvservlet"+"?type="+85;
				Window.open(url, "test", "enabled");
			}
		});

	}
	
	
	
	
	
	
	
	
	
	
}
