package com.slicktechnologies.client.views.purchase.vendorproductlist;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.businessprocesslayer.VendorPriceListDetails;

public class VendorPriceListTableProxy extends SuperTable<VendorPriceListDetails>{

	TextColumn<VendorPriceListDetails> columnPriceListCount;
	TextColumn<VendorPriceListDetails> columnPriceListTitle;
	TextColumn<VendorPriceListDetails> columnVendorId;
	TextColumn<VendorPriceListDetails> columnVendorTitle;
	TextColumn<VendorPriceListDetails> columnProductID;
	TextColumn<VendorPriceListDetails> columnProductName;
	TextColumn<VendorPriceListDetails> columnProductCode;
	TextColumn<VendorPriceListDetails> columnProductCategory;
	TextColumn<VendorPriceListDetails> columnProductPrice;
	
	@Override
	protected void initializekeyprovider() {
		keyProvider = new ProvidesKey<VendorPriceListDetails>() {
			@Override
			public Object getKey(VendorPriceListDetails item) {
				if (item == null) {
					return null;
				} else
					return item.getId();
			}
		};
	}

	public VendorPriceListTableProxy() {

	}

	@Override
	public void createTable() {
		addColumnPriceListCount();
		addColumnPriceListTitle();
		addColumnVendorId();
		addColumnVendorName();
		addColumnProductID();
		addColumnProductName();
		addColumnProductCode();
		addColumnProductCategory();
		addColumnProductPrice();

	}

	

	public void addColumnSorting() {
		addSortinggetCount();
		addSortinggetTitle();
		addSortinggetVendorId();
		addSortinggetVendorName();
		addSortinggetProductID();
		addSortinggetProductName();
		addSortinggetProductCode();
		addSortinggetProductCategory();
		addSortingProductPrice();
	}

	public Object getVarRef(String varName) {
		if (varName.equals("columnPriceListCount"))
			return this.columnPriceListCount;
		return null;
	}

	/******************** sorting columns *******************/
	public void addSortinggetTitle() {
		List<VendorPriceListDetails> list = getDataprovider().getList();
		columnSort = new ListHandler<VendorPriceListDetails>(list);
		columnSort.setComparator(columnPriceListTitle, new Comparator<VendorPriceListDetails>() {
			@Override
			public int compare(VendorPriceListDetails e1, VendorPriceListDetails e2) {
				if (e1 != null && e2 != null) {
					if (e1.getPriceListTitle() != null
							&& e2.getPriceListTitle() != null) {
						return e1.getPriceListTitle().compareTo(
								e2.getPriceListTitle());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	public void addSortinggetCount() {
		List<VendorPriceListDetails> list = getDataprovider().getList();
		columnSort = new ListHandler<VendorPriceListDetails>(list);
		columnSort.setComparator(columnPriceListCount, new Comparator<VendorPriceListDetails>() {
			@Override
			public int compare(VendorPriceListDetails e1, VendorPriceListDetails e2) {
				if (e1 != null && e2 != null) {
					if (e1.getPriceListId() == e2.getPriceListId()) {
						return 0;
					}
					if (e1.getPriceListId() > e2.getPriceListId()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	public void addSortinggetProductID() {
		List<VendorPriceListDetails> list = getDataprovider().getList();
		columnSort = new ListHandler<VendorPriceListDetails>(list);
		columnSort.setComparator(columnProductID, new Comparator<VendorPriceListDetails>() {
			@Override
			public int compare(VendorPriceListDetails e1, VendorPriceListDetails e2) {
				if (e1 != null && e2 != null) {
					if (e1.getProductId() == e2.getProductId()) {
						return 0;
					}
					if (e1.getProductId() > e2.getProductId()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	protected void addSortinggetProductName() {
		List<VendorPriceListDetails> list = getDataprovider().getList();
		columnSort = new ListHandler<VendorPriceListDetails>(list);
		columnSort.setComparator(columnProductName,
				new Comparator<VendorPriceListDetails>() {
					@Override
					public int compare(VendorPriceListDetails e1, VendorPriceListDetails e2) {
						if (e1 != null && e2 != null) {
							if (e1.getProductName() != null
									&& e2.getProductName() != null) {
								return e1.getProductName().compareTo(
										e2.getProductName());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	
	
	public void addSortinggetVendorId() {
		List<VendorPriceListDetails> list = getDataprovider().getList();
		columnSort = new ListHandler<VendorPriceListDetails>(list);
		columnSort.setComparator(columnVendorId, new Comparator<VendorPriceListDetails>() {
			@Override
			public int compare(VendorPriceListDetails e1, VendorPriceListDetails e2) {
				if (e1 != null && e2 != null) {
					if (e1.getVendorId() == e2.getVendorId()) {
						return 0;
					}
					if (e1.getVendorId() > e2.getVendorId()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	protected void addSortinggetVendorName() {
		List<VendorPriceListDetails> list = getDataprovider().getList();
		columnSort = new ListHandler<VendorPriceListDetails>(list);
		columnSort.setComparator(columnVendorTitle,
				new Comparator<VendorPriceListDetails>() {
					@Override
					public int compare(VendorPriceListDetails e1, VendorPriceListDetails e2) {
						if (e1 != null && e2 != null) {
							if (e1.getVendorName() != null
									&& e2.getVendorName() != null) {
								return e1.getVendorName().compareTo(
										e2.getVendorName());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}
	

	protected void addSortinggetProductCategory() {
		List<VendorPriceListDetails> list = getDataprovider().getList();
		columnSort = new ListHandler<VendorPriceListDetails>(list);
		columnSort.setComparator(columnProductCategory,
				new Comparator<VendorPriceListDetails>() {
					@Override
					public int compare(VendorPriceListDetails e1, VendorPriceListDetails e2) {
						if (e1 != null && e2 != null) {
							if (e1.getProductCategory() != null
									&& e2.getProductCategory() != null) {
								return e1.getProductCategory().compareTo(
										e2.getProductCategory());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addSortinggetProductCode() {
		List<VendorPriceListDetails> list = getDataprovider().getList();
		columnSort = new ListHandler<VendorPriceListDetails>(list);
		columnSort.setComparator(columnProductCode,
				new Comparator<VendorPriceListDetails>() {
					@Override
					public int compare(VendorPriceListDetails e1, VendorPriceListDetails e2) {
						if (e1 != null && e2 != null) {
							if (e1.getProductCode() != null
									&& e2.getProductCode() != null) {
								return e1.getProductCode().compareTo(
										e2.getProductCode());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	private void addSortingProductPrice() {
		List<VendorPriceListDetails> list = getDataprovider().getList();
		columnSort = new ListHandler<VendorPriceListDetails>(list);
		columnSort.setComparator(columnProductPrice, new Comparator<VendorPriceListDetails>() {
			@Override
			public int compare(VendorPriceListDetails e1, VendorPriceListDetails e2) {
				if (e1 != null && e2 != null) {
					if (e1.getProductPrice() == e2.getProductPrice()) {
						return 0;
					}
					if (e1.getProductPrice() > e2.getProductPrice()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	/************************************************************************************/
	public void addColumnPriceListTitle() {
		columnPriceListTitle = new TextColumn<VendorPriceListDetails>() {

			@Override
			public String getValue(VendorPriceListDetails object) {
				return object.getPriceListTitle();
			}
		};
		table.addColumn(columnPriceListTitle, "Title");
		columnPriceListTitle.setSortable(true);
	}

	public void addColumnPriceListCount() {
		columnPriceListCount = new TextColumn<VendorPriceListDetails>() {

			@Override
			public String getValue(VendorPriceListDetails object) {
				return object.getPriceListId() + "";
			}
		};
		table.addColumn(columnPriceListCount, "ID");
		columnPriceListCount.setSortable(true);
	}

	public void addColumnProductID() {
		columnProductID = new TextColumn<VendorPriceListDetails>() {

			@Override
			public String getValue(VendorPriceListDetails object) {
				return object.getProductId() + "";
			}
		};
		table.addColumn(columnProductID, "Product ID");
		columnProductID.setSortable(true);
	}

	public void addColumnProductName() {
		columnProductName = new TextColumn<VendorPriceListDetails>() {

			@Override
			public String getValue(VendorPriceListDetails object) {
				return object.getProductName();
			}
		};
		table.addColumn(columnProductName, "Name");
		columnProductName.setSortable(true);
	}
	
	
	public void addColumnVendorId() {
		columnVendorId = new TextColumn<VendorPriceListDetails>() {

			@Override
			public String getValue(VendorPriceListDetails object) {
				return object.getVendorId() + "";
			}
		};
		table.addColumn(columnVendorId, "Vendor ID");
		columnVendorId.setSortable(true);
	}

	public void addColumnVendorName() {
		columnVendorTitle = new TextColumn<VendorPriceListDetails>() {

			@Override
			public String getValue(VendorPriceListDetails object) {
				return object.getVendorName();
			}
		};
		table.addColumn(columnVendorTitle, "Vendor Name");
		columnVendorTitle.setSortable(true);
	}
	

	public void addColumnProductCode() {
		columnProductCode = new TextColumn<VendorPriceListDetails>() {

			@Override
			public String getValue(VendorPriceListDetails object) {
				return object.getProductCode();
			}
		};
		table.addColumn(columnProductCode, "Code");
		columnProductCode.setSortable(true);
	}

	public void addColumnProductCategory() {
		columnProductCategory = new TextColumn<VendorPriceListDetails>() {
			@Override
			public String getValue(VendorPriceListDetails object) {
				return object.getProductCategory();
			}
		};
		table.addColumn(columnProductCategory, "Category");
		columnProductCategory.setSortable(true);
	}
	
	private void addColumnProductPrice() {
		columnProductPrice = new TextColumn<VendorPriceListDetails>() {
			@Override
			public String getValue(VendorPriceListDetails object) {
				return object.getProductPrice()+"";
			}
		};
		table.addColumn(columnProductPrice, "Price");
		columnProductPrice.setSortable(true);
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
