package com.slicktechnologies.client.views.purchase.requestquotation;

import java.util.Vector;

import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.config.CategoryTypeFactory;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.client.views.purchase.requestquotation.RequestForQuotationPresenter.RequestForQuotationPresenterSearch;
import com.slicktechnologies.shared.common.businessprocesslayer.RequsestForQuotation;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;

public class RequestForQuotationPresenterSearchProxy extends
		RequestForQuotationPresenterSearch {

	IntegerBox ibReqQuotationID;
	TextBox tbquotationName;
	ListBox lbstatus;
	PersonInfoComposite vic;
	ProductInfoComposite pic;
	
	ObjectListBox<ConfigCategory> oblquotationCategory;
	ObjectListBox<Type> oblquotationType;
	ObjectListBox<Branch> olbBranch;
	DateBox dbCreationDate;
	
	DateComparator datecomparator;
	DateComparator datecomp;
	ObjectListBox<HrProject> project;
	IntegerBox purchaseRequisiteNo;

	public RequestForQuotationPresenterSearchProxy() {
		super();
		createGui();
		System.out.println("hello");
		pic.getProdPrice().setVisible(false);
		pic.getUnitOfmeasuerment().setVisible(false);

	}

	public Object getVarRef(String varName) {
		return null;
	}

	public void initWidget() {
		ibReqQuotationID = new IntegerBox();
		tbquotationName = new TextBox();
		lbstatus = new ListBox();
		AppUtility.setStatusListBox(lbstatus,RequsestForQuotation.getStatusList());

		vic = AppUtility.vendorInfoComposite(new Vendor());
		pic = AppUtility.product(new SuperProduct(), false);

		vic.getCustomerId().getHeaderLabel().setText("Vendor Id");
		vic.getCustomerName().getHeaderLabel().setText("Vendor Name");
		vic.getCustomerCell().getHeaderLabel().setText("Vendor Cell");
		

		oblquotationCategory= new ObjectListBox<ConfigCategory>();
		oblquotationType = new ObjectListBox<Type>();
		CategoryTypeFactory.initiateOneManyFunctionality(oblquotationCategory,oblquotationType);
		AppUtility.makeTypeListBoxLive(this.oblquotationType,Screen.RFQCATEGORY);
		dbCreationDate = new DateBoxWithYearSelector();
		datecomparator = new DateComparator("creationDate",new RequsestForQuotation());

		project = new ObjectListBox<HrProject>();
		HrProject.MakeProjectListBoxLive(project);

		purchaseRequisiteNo = new IntegerBox();
		
	//**************rohan made changes here ***************
		
		olbBranch= new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbBranch);

	}

	public void createScreen() {

		initWidget();
		FormFieldBuilder fbuilder;

		fbuilder = new FormFieldBuilder();
		fbuilder = new FormFieldBuilder("RFQ ID", ibReqQuotationID);
		FormField fibReqQuotationID = fbuilder.setMandatory(false)
				.setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("RFQ Title", tbquotationName);
		FormField ftbquotationName = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Branch",olbBranch);
		FormField folbBranch= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		

		fbuilder = new FormFieldBuilder("Status", lbstatus);
		FormField flbstatus = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("", pic);
		FormField fpic = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(4).build();

		fbuilder = new FormFieldBuilder("", vic);
		FormField fvic = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(4).build();

		fbuilder = new FormFieldBuilder("RFQ Type", oblquotationType);
		FormField foblquotationType = fbuilder.setMandatory(false)
				.setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("RFQ Category", oblquotationCategory);
		FormField foblquotationCategory = fbuilder.setMandatory(false)
				.setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("RFQ From Date",
				datecomparator.getFromDate());
		FormField fdbCreationDate = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("RFQ To Date",
				datecomparator.getToDate());
		FormField fresponseDate = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Project", project);
		FormField fproject = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("PR Number",
				purchaseRequisiteNo);
		FormField fpuchasereq = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		FormField[][] formfield = {
				{ fibReqQuotationID, ftbquotationName, fdbCreationDate,
						fresponseDate },
				{ foblquotationCategory, foblquotationType, fpuchasereq,
						fproject }, { flbstatus,folbBranch }, { fpic }, { fvic } };

		
		
		
		this.fields = formfield;

	}

	public MyQuerry getQuerry() {
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter temp = null;

		if (project.getSelectedIndex() != 0) {

			temp = new Filter();
			int item = project.getSelectedIndex();
			String sel = project.getItemText(item);
			temp.setStringValue(sel);
			temp.setQuerryString("project");
			filtervec.add(temp);

		}

		if (purchaseRequisiteNo.getValue() != null) {
			temp = new Filter();
			temp.setIntValue(purchaseRequisiteNo.getValue());
			temp.setQuerryString("purchaseReqNo");
			filtervec.add(temp);

		}

		if (ibReqQuotationID.getValue() != null) {
			temp = new Filter();
			temp.setIntValue(ibReqQuotationID.getValue());
			temp.setQuerryString("count");
			filtervec.add(temp);

		}

		if (!tbquotationName.getValue().equals("")) {
			temp = new Filter();
			temp.setStringValue(tbquotationName.getValue());
			temp.setQuerryString("quotationName");
			filtervec.add(temp);

		}
		

		if(olbBranch.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(olbBranch.getValue().trim());
			temp.setQuerryString("branch");
			filtervec.add(temp);
		} 

		if (lbstatus.getSelectedIndex() != 0) {
			System.out.println("inside status");
			temp = new Filter();
			int item = lbstatus.getSelectedIndex();
			String sel = lbstatus.getItemText(item);
			System.out.println(sel);
			temp.setStringValue(sel);
			temp.setQuerryString("status");
			filtervec.add(temp);

		}
		if (oblquotationCategory.getSelectedIndex() != 0) {
			temp = new Filter();
			temp.setStringValue(oblquotationCategory.getValue());
			temp.setQuerryString("quotationCategory");
			filtervec.add(temp);
		}
		if (oblquotationType.getSelectedIndex() != 0) {
			temp = new Filter();
			temp.setStringValue(oblquotationType.getValue());
			temp.setQuerryString("quotationType");
			filtervec.add(temp);
		}

		if (!pic.getProdName().getValue().equals("")) {
			temp = new Filter();
			temp.setStringValue(pic.getProdName().getValue());
			temp.setQuerryString("productDetails.productName");
			filtervec.add(temp);
		}
		if (datecomparator.getValue() != null) {
			filtervec.addAll(datecomparator.getValue());
		}

		if (!pic.getProdCode().getValue().equals("")) {
			temp = new Filter();
			temp.setStringValue(pic.getProdCode().getValue());
			temp.setQuerryString("productDetails.productCode");
			filtervec.add(temp);
		}

		if (!pic.getProdID().getValue().equals("")) {
			temp = new Filter();
			temp.setIntValue(Integer.parseInt(pic.getProdID().getValue()));
			temp.setQuerryString("productDetails.productID");
			filtervec.add(temp);
		}

		if (!vic.getName().getValue().equals("")) {
			temp = new Filter();
			temp.setStringValue(vic.getName().getValue());
			temp.setQuerryString("vendorDetails.vendorName");
			filtervec.add(temp);
		}

		if (!vic.getId().getValue().equals("")) {
			temp = new Filter();
			temp.setIntValue(Integer.parseInt(vic.getId().getValue()));
			temp.setQuerryString("vendorDetails.vendorId");
			filtervec.add(temp);
		}

		MyQuerry querry = new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new RequsestForQuotation());
		return querry;
	}

	@Override
	public boolean validate() {
		if(LoginPresenter.branchRestrictionFlag)
		{
		if(olbBranch.getSelectedIndex()==0)
		{
			showDialogMessage("Select Branch");
			return false;
		}
		}
		return true;
	}

}