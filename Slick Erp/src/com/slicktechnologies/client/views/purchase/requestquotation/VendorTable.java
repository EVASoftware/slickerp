package com.slicktechnologies.client.views.purchase.requestquotation;

import java.util.List;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.businessprocesslayer.PriceListDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.VendorDetails;

public class VendorTable extends SuperTable<VendorDetails>{

	TextColumn<VendorDetails> VendorID;
	TextColumn<VendorDetails> VendorName;
	Column<VendorDetails,String> VendorEmailId;
	TextColumn<VendorDetails> VendorEmailIdView;
	Column<VendorDetails,String> deleteColumn;
	
	
	@Override
	public void createTable() {
		addColumnVendorId();
		addColumnVendorName();
		addColumnVendorEmailId();
		createColumndeleteColumn();
		addFieldUpdater();
	}
	
	public void addViewColumn()
	{
		addColumnVendorId();
		addColumnVendorName();
		addColumnViewVendorEmailId();
	
	}
	
	public void addeditColumn()
	{
		addColumnVendorId();
		addColumnVendorName();
		addColumnVendorEmailId();
		createColumndeleteColumn();
		addFieldUpdater();
	}
	public void addColumnVendorId()
	{
		VendorID=new TextColumn<VendorDetails>() {
			
			@Override
			public String getValue(VendorDetails object) {
				// TODO Auto-generated method stub
				return object.getVendorId()+"";
			}
		};table.addColumn(VendorID,"Vendor ID");
	}
	
	public void addColumnVendorName()
	{
		VendorName=new TextColumn<VendorDetails>() {
			
			@Override
			public String getValue(VendorDetails object) {
				// TODO Auto-generated method stub
				return object.getVendorName();
			}
		};table.addColumn(VendorName,"Vendor Name");
	}
	
	public void addColumnVendorEmailId()
	{
		EditTextCell editCell=new EditTextCell();
		VendorEmailId=new Column<VendorDetails, String>(editCell) {
			
			@Override
			public String getValue(VendorDetails object) {
				// TODO Auto-generated method stub
				
					return object.getVendorEmailId();
				
			}
		};table.addColumn(VendorEmailId,"Email ID");
	}
	
	public void addColumnViewVendorEmailId()
	{
		VendorEmailIdView=new TextColumn<VendorDetails>() {
			
			@Override
			public String getValue(VendorDetails object) {
					return object.getVendorEmailId();
			}
		};table.addColumn(VendorEmailIdView,"Email ID");
	}

	public void createColumndeleteColumn()
	{
		ButtonCell btnCell= new ButtonCell();
		deleteColumn=new Column<VendorDetails,String>(btnCell)
				{
			@Override
			public String getValue(VendorDetails object)
			{
				return  "Delete" ;
			}
				};
		table.addColumn(deleteColumn,"Delete");

	}

	public void createFieldUpdaterDelete()
	{
		deleteColumn.setFieldUpdater(new FieldUpdater<VendorDetails,String>()
				{
			@Override
			public void update(int index,VendorDetails object,String value)
			{
				getDataprovider().getList().remove(object);

				table.redrawRow(index);
			}
				});

	}
	
	public void createFieldUpdateEmail()
	{
		VendorEmailId.setFieldUpdater(new FieldUpdater<VendorDetails, String>() {
			
			@Override
			public void update(int index, VendorDetails object, String value) {
				// TODO Auto-generated method stub
				try
				{
					String val=value;
					object.setVendorEmailId(val);
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
				table.redrawRow(index);

			}
		});
	}

	
	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		createFieldUpdaterDelete();
		createFieldUpdateEmail();
		
	}

	@Override
	public void setEnable(boolean state) {
		int tablecolcount=this.table.getColumnCount();
		for(int i=tablecolcount-1;i>-1;i--)
		  table.removeColumn(i);
		if(state ==true)
			addeditColumn();
		if(state==false)
			addViewColumn();
		
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
	/*
	public void RemoveDupVendor(){
		System.out.println("in dup vendor method");
		List<VendorDetails> list=getDataprovider().getList();
		System.out.println(list.size());
		List<VendorDetails> list1=getDataprovider().getList();
		for(int i=0;i<list.size();i++)
		{
			for(int k=i+1;k<list.size();k++)
			{
				VendorDetails vd=new VendorDetails();
				if(list.get(i).getVendorId()==list.get(k).getVendorId())
				{
				//table.removeFromParent();
				vd.setVendorId(list.get(i).getVendorId());
				vd.setVendorEmailId(list.get(i).getVendorEmailId());
				vd.setVendorName(list.get(i).getVendorName());
				list1.add(vd);
				}
				
			}
		}
		connectToLocal();
		getDataprovider().setList(list1);
		
	}*/

}
