package com.slicktechnologies.client.views.purchase.requestquotation;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.purchase.requestquotation.RequestForQuotationPresenter.RequestForPresenterTable;
import com.slicktechnologies.shared.common.businessprocesslayer.PriceList;
import com.slicktechnologies.shared.common.businessprocesslayer.RequsestForQuotation;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;

public class RequestForQuotationPresenterTableProxy extends RequestForPresenterTable{

	TextColumn<RequsestForQuotation> ColumnRfqID;
	TextColumn<RequsestForQuotation> ColumnRfqTitle;
	TextColumn<RequsestForQuotation> ColumnRfqCategory;
	TextColumn<RequsestForQuotation> ColumnRfqType;
	TextColumn<RequsestForQuotation> ColumnRfqResponsiblePerson;
	TextColumn<RequsestForQuotation> ColumnRfqStatus;
	TextColumn<RequsestForQuotation> ColumnRfqExptectedDate;
	TextColumn<RequsestForQuotation> ColumnProject;
	TextColumn<RequsestForQuotation> ColumnPurchaseReqno;
	TextColumn<RequsestForQuotation>ColumnRfqDate;
	TextColumn<RequsestForQuotation> branchColumn;
	
	@Override
	public void createTable()
	{
		addColumnRfqID();
		addColumnRfqTitle();
		addColumnRfqCategory();
		addColumnRfqType();
		addColumnProject();
		addColumnPurchaseReqno();
		addColumnRfqResponsiblePerson();
//		addColumnRfqDate();
		addColumnBranch();
		addColumnRfqExptectedDate();
		addColumnRfqStatus();
		
		
	}
	
	public void addColumnSorting(){
		addSortinggetCount();
		addSortinggetTitle();
		addSortColumnRfqCategory();
		addSortColumnRfqType();
		addSortingColumnProject();
		addSortingColumnPurchaseReqno();
	
		addSortColumnRfqResponsiblePerson();
		addSortinggetBranch();
		addSortColumnRfqDate();
		addSortColumnRfqExptectedDate();
		addSortColumnRfqStatus();
		
			}
	
	
	/********************************************sorted columns*******************/
	
	public void addSortingColumnPurchaseReqno()
	{
		List<RequsestForQuotation> list=getDataprovider().getList();
		columnSort=new ListHandler<RequsestForQuotation>(list);
		columnSort.setComparator(ColumnPurchaseReqno, new Comparator<RequsestForQuotation>()
				{
			@Override
			public int compare(RequsestForQuotation e1,RequsestForQuotation e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getPurchaseReqNo()== e2.getPurchaseReqNo()){
						return 0;}
					if(e1.getPurchaseReqNo()> e2.getPurchaseReqNo()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);

	}
	
	public void addSortingColumnProject()
	{
		List<RequsestForQuotation> list=getDataprovider().getList();
		columnSort=new ListHandler<RequsestForQuotation>(list);
		columnSort.setComparator(ColumnProject, new Comparator<RequsestForQuotation>()
				{
			@Override
			public int compare(RequsestForQuotation e1,RequsestForQuotation e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getProject()!=null && e2.getProject()!=null){
						return e1.getProject().compareTo(e2.getProject());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);

	}
	
	public void addSortColumnRfqExptectedDate()
	{
		List<RequsestForQuotation> list=getDataprovider().getList();
		columnSort=new ListHandler<RequsestForQuotation>(list);
		columnSort.setComparator(ColumnRfqExptectedDate, new Comparator<RequsestForQuotation>()
				{
			@Override
			public int compare(RequsestForQuotation e1,RequsestForQuotation e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getExpectedClosureDate()!=null && e2.getExpectedClosureDate()!=null){
						return e1.getExpectedClosureDate().compareTo(e2.getExpectedClosureDate());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);


	}
	
	protected void addSortinggetBranch()
	{
		List<RequsestForQuotation> list=getDataprovider().getList();
		columnSort=new ListHandler<RequsestForQuotation>(list);
		columnSort.setComparator(branchColumn, new Comparator<RequsestForQuotation>()
				{
			@Override
			public int compare(RequsestForQuotation e1,RequsestForQuotation e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBranch()!=null && e2.getBranch()!=null){
						return e1.getBranch().compareTo(e2.getBranch());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	public void addSortColumnRfqDate()
	{
		List<RequsestForQuotation> list=getDataprovider().getList();
		columnSort=new ListHandler<RequsestForQuotation>(list);
		columnSort.setComparator(ColumnRfqDate, new Comparator<RequsestForQuotation>()
				{
			@Override
			public int compare(RequsestForQuotation e1,RequsestForQuotation e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getCreationDate()!=null && e2.getCreationDate()!=null){
						return e1.getCreationDate().compareTo(e2.getCreationDate());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);


	}
	
	public void addSortColumnRfqStatus()
	{
		List<RequsestForQuotation> list=getDataprovider().getList();
		columnSort=new ListHandler<RequsestForQuotation>(list);
		columnSort.setComparator(ColumnRfqStatus, new Comparator<RequsestForQuotation>()
				{
			@Override
			public int compare(RequsestForQuotation e1,RequsestForQuotation e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getStatus()!=null && e2.getStatus()!=null){
						return e1.getStatus().compareTo(e2.getStatus());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);


	}
	
	public void addSortColumnRfqResponsiblePerson()
	{
		List<RequsestForQuotation> list=getDataprovider().getList();
		columnSort=new ListHandler<RequsestForQuotation>(list);
		columnSort.setComparator(ColumnRfqResponsiblePerson, new Comparator<RequsestForQuotation>()
				{
			@Override
			public int compare(RequsestForQuotation e1,RequsestForQuotation e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getEmployee()!=null && e2.getEmployee()!=null){
						return e1.getEmployee().compareTo(e2.getEmployee());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);


	}
	public void addSortColumnRfqType()
	{
		List<RequsestForQuotation> list=getDataprovider().getList();
		columnSort=new ListHandler<RequsestForQuotation>(list);
		columnSort.setComparator(ColumnRfqType, new Comparator<RequsestForQuotation>()
				{
			@Override
			public int compare(RequsestForQuotation e1,RequsestForQuotation e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getQuotationType()!=null && e2.getQuotationType()!=null){
						return e1.getQuotationType().compareTo(e2.getQuotationType());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);


	}
	
	public void addSortColumnRfqCategory()
	{
		List<RequsestForQuotation> list=getDataprovider().getList();
		columnSort=new ListHandler<RequsestForQuotation>(list);
		columnSort.setComparator(ColumnRfqCategory, new Comparator<RequsestForQuotation>()
				{
			@Override
			public int compare(RequsestForQuotation e1,RequsestForQuotation e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getQuotationCategory()!=null && e2.getQuotationCategory()!=null){
						return e1.getQuotationCategory().compareTo(e2.getQuotationCategory());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);

	}
	
	public void addSortinggetTitle()
	{
		List<RequsestForQuotation> list=getDataprovider().getList();
		columnSort=new ListHandler<RequsestForQuotation>(list);
		columnSort.setComparator(ColumnRfqTitle, new Comparator<RequsestForQuotation>()
				{
			@Override
			public int compare(RequsestForQuotation e1,RequsestForQuotation e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getQuotationName()!=null && e2.getQuotationName()!=null){
						return e1.getQuotationName().compareTo(e2.getQuotationName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}

	public void addSortinggetCount()
	{
		List<RequsestForQuotation> list=getDataprovider().getList();
		columnSort=new ListHandler<RequsestForQuotation>(list);
		columnSort.setComparator(ColumnRfqID, new Comparator<RequsestForQuotation>()
				{
			@Override
			public int compare(RequsestForQuotation e1,RequsestForQuotation e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}


	
	/****************************************************************************/
	
	public void addColumnPurchaseReqno()
	{
		ColumnPurchaseReqno=new TextColumn<RequsestForQuotation>() {
			
			@Override
			public String getValue(RequsestForQuotation object) {
				// TODO Auto-generated method stub
				return object.getPurchaseReqNo()+"";
			}
		};table.addColumn(ColumnPurchaseReqno,"PR No.");
		ColumnPurchaseReqno.setSortable(true);
	}
	
	public void addColumnProject()
	{
		ColumnProject=new TextColumn<RequsestForQuotation>() {
			
			@Override
			public String getValue(RequsestForQuotation object) {
				// TODO Auto-generated method stub
				return object.getProject();
			}
		};table.addColumn(ColumnProject,"Project");
		ColumnProject.setSortable(true);
	}
	
	public void addColumnRfqID()
	{
		ColumnRfqID=new TextColumn<RequsestForQuotation>() {
			
			@Override
			public String getValue(RequsestForQuotation object) {
				// TODO Auto-generated method stub
				return object.getCount()+"";
			}
		};table.addColumn(ColumnRfqID,"ID");
		ColumnRfqID.setSortable(true);
	}
	
	public void addColumnRfqTitle()
	{
		ColumnRfqTitle=new TextColumn<RequsestForQuotation>() {
			
			@Override
			public String getValue(RequsestForQuotation object) {
				// TODO Auto-generated method stub
				return object.getQuotationName();
			}
		};table.addColumn(ColumnRfqTitle,"Title");
		ColumnRfqTitle.setSortable(true);
	}
	
	public void addColumnRfqCategory()
	{
		ColumnRfqCategory=new TextColumn<RequsestForQuotation>() {
			
			@Override
			public String getValue(RequsestForQuotation object) {
				// TODO Auto-generated method stub
				return object.getQuotationCategory();
			}
		};table.addColumn(ColumnRfqCategory,"Category");
		ColumnRfqCategory.setSortable(true);
	}
	
	public void addColumnRfqType()
	{
		ColumnRfqType=new TextColumn<RequsestForQuotation>() {
			
			@Override
			public String getValue(RequsestForQuotation object) {
				// TODO Auto-generated method stub
				return object.getQuotationType();
			}
		};table.addColumn(ColumnRfqType,"Type");
		ColumnRfqType.setSortable(true);
	}
	
	public void addColumnRfqResponsiblePerson()
	{
		ColumnRfqResponsiblePerson=new TextColumn<RequsestForQuotation>() {
			
			@Override
			public String getValue(RequsestForQuotation object) {
				// TODO Auto-generated method stub
				return object.getApproverName();
			}
		};table.addColumn(ColumnRfqResponsiblePerson,"Pur Eng.");
		ColumnRfqResponsiblePerson.setSortable(true);
	}
	
	public void addColumnRfqStatus()
	{
		ColumnRfqStatus=new TextColumn<RequsestForQuotation>() {
			
			@Override
			public String getValue(RequsestForQuotation object) {
				// TODO Auto-generated method stub
				return object.getStatus();
			}
		};table.addColumn(ColumnRfqStatus,"Status");
		ColumnRfqStatus.setSortable(true);
	}
	
	public void addColumnBranch()
	{
		branchColumn=new TextColumn<RequsestForQuotation>() {

			@Override
			public String getValue(RequsestForQuotation object) {
				return object.getBranch();
			}
		};
		
		table.addColumn(branchColumn,"Branch");
		table.setColumnWidth(branchColumn, 100, Unit.PX);
		branchColumn.setSortable(true);
	}
	
	public void addColumnRfqExptectedDate()
	{
		ColumnRfqExptectedDate=new TextColumn<RequsestForQuotation>() {
			
			@Override
			public String getValue(RequsestForQuotation object) {
				if(object.getResponseDate()!=null)
					return AppUtility.parseDate(object.getResponseDate())+"";
				else
					return "";
			}
		};table.addColumn(ColumnRfqExptectedDate,"Resp. Date");
		ColumnRfqExptectedDate.setSortable(true);
	}
	
	public void addColumnRfqDate()
	{
		ColumnRfqDate=new TextColumn<RequsestForQuotation>() {
			
			@Override
			public String getValue(RequsestForQuotation object) {
				if(object.getCreationDate()!=null)
					return AppUtility.parseDate(object.getCreationDate())+"";
				else
					return "";
			}
		};table.addColumn(ColumnRfqDate,"Creation Date");
		ColumnRfqDate.setSortable(true);
	}
	
}
