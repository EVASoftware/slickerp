package com.slicktechnologies.client.views.purchase.requestquotation;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.google.gwt.view.client.RowCountChangeEvent.Handler;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.approvalutility.ApprovableFormScreenPresenter;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.EmailService;
import com.slicktechnologies.client.services.EmailServiceAsync;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.documentcancellation.DocumentCancellationPopUp;
import com.slicktechnologies.client.views.popups.NewEmailPopUp;
import com.slicktechnologies.client.views.purchase.letterofintent.LetterOfIntentForm;
import com.slicktechnologies.client.views.purchase.letterofintent.LetterOfIntentPresenter;
import com.slicktechnologies.client.views.purchase.purchaseorder.PurchaseOrderForm;
import com.slicktechnologies.client.views.purchase.purchaseorder.PurchaseOrderPresenter;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.LetterOfIntent;
import com.slicktechnologies.shared.common.businessprocesslayer.PriceListDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetailsPO;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessprocesslayer.RequsestForQuotation;
import com.slicktechnologies.shared.common.businessprocesslayer.VendorDetails;
import com.slicktechnologies.shared.common.contactidentification.ContactPersonIdentification;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class RequestForQuotationPresenter extends
		ApprovableFormScreenPresenter<RequsestForQuotation> implements Handler {

	final GenricServiceAsync async = GWT.create(GenricService.class);
	public static RequestForQuotationForm form;
	EmailServiceAsync emailService = GWT.create(EmailService.class);
	DocumentCancellationPopUp cancelPopup = new DocumentCancellationPopUp();
	PopupPanel cancelpanel;

 	NewEmailPopUp emailpopup = new NewEmailPopUp();

	public RequestForQuotationPresenter(FormScreen<RequsestForQuotation> view,
			RequsestForQuotation model) {
		super(view, model);
		form = (RequestForQuotationForm) view;
		// form.setPresenter(this);

		cancelPopup.getBtnOk().addClickHandler(this);
		cancelPopup.getBtnCancel().addClickHandler(this);
		form.productTableRFQ.getTable().addRowCountChangeHandler(this);

		boolean isDownload = AuthorizationHelper
				.getDownloadAuthorization(Screen.REQUESTFORQUOTATION,
						LoginPresenter.currentModule.trim());
		if (isDownload == false) {
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}

	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel label = (InlineLabel) e.getSource();
		String text = label.getText().trim();

		if (text.equals("Email"))
			reactOnEmail();

		if (text.equals("New")) {
			form.setToNewState();
			initalize();

		}
		if (text.equals("Mark Close")) {
			ChangeStatus(RequsestForQuotation.CLOSED);

		}
		if (text.equals(AppConstants.CANCELRFQ)) {

			cancelPopup.getPaymentDate().setValue(new Date() + "");
			cancelPopup.getPaymentID().setValue(model.getCount() + "");
			cancelPopup.getPaymentStatus().setValue(model.getStatus());
			cancelpanel = new PopupPanel(true);
			cancelpanel.add(cancelPopup);
			cancelpanel.show();
			cancelpanel.center();
		}
		if (text.equals(ManageApprovals.APPROVALREQUEST)) {
			form.getManageapproval().reactToRequestForApproval();
		}
		if (text.equals(ManageApprovals.CANCELAPPROVALREQUEST)) {
			form.getManageapproval().reactToApprovalRequestCancel();
		}
		if (text.equals(ManageApprovals.SUBMIT)) {
			form.getManageapproval().reactToSubmit();
		}
		if (text.equals("Create LOI")) {
			reactToLetterOFIntent();
		}
		if (text.equals("Create PO")) {
			reactToPurchaseOrder();
		}
		if (text.equals("Download")) {
			reactOnDownload();
		}

	}

	@Override
	public void reactOnPrint() {

	}

	@Override
	public void reactOnEmail() {
		System.out.println("inside react on email");

//		boolean conf = Window.confirm("Do you really want to send email?");
//		if (conf == true) {
//			emailService.initiateRFQEmail((RequsestForQuotation) model,
//					new AsyncCallback<Void>() {
//						@Override
//						public void onFailure(Throwable caught) {
//							Window.alert("Resource Quota Ended ");
//							caught.printStackTrace();
//						}
//
//						@Override
//						public void onSuccess(Void result) {
//							Window.alert("Email Sent Sucessfully !");
//						}
//					});
//		}

		/**
		 * @author Vijay Date 19-03-2021
		 * Des :- above old code commented 
		 * Added New Email Popup Functionality
		 */
		emailpopup.showPopUp();
		setEmailPopUpData();
		
		/**
		 * ends here
		 */
	}

	@Override
	protected void makeNewModel() {
		model = new RequsestForQuotation();

	}

	public static RequestForQuotationForm initialize() {
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("RFQ",
				Screen.REQUESTFORQUOTATION);
		form = new RequestForQuotationForm();

		RequestForPresenterTable gentable = new RequestForQuotationPresenterTableProxy();
		gentable.setView(form);
		gentable.applySelectionModle();

		RequestForQuotationPresenterSearch.staticSuperTable = gentable;
		RequestForQuotationPresenterSearch searchpopup = new RequestForQuotationPresenterSearchProxy();
		form.setSearchpopupscreen(searchpopup);

		RequestForQuotationPresenter presenter = new RequestForQuotationPresenter(
				form, new RequsestForQuotation());
		AppMemory.getAppMemory().stickPnel(form);
		return form;

	}

	public static RequestForQuotationForm initalize() {
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("RFQ",
				Screen.REQUESTFORQUOTATION);
		form = new RequestForQuotationForm();

		RequestForPresenterTable gentable = new RequestForQuotationPresenterTableProxy();
		gentable.setView(form);
		gentable.applySelectionModle();

		RequestForQuotationPresenterSearch.staticSuperTable = gentable;
		RequestForQuotationPresenterSearch searchpopup = new RequestForQuotationPresenterSearchProxy();
		form.setSearchpopupscreen(searchpopup);

		RequestForQuotationPresenter presenter = new RequestForQuotationPresenter(
				form, new RequsestForQuotation());
		AppMemory.getAppMemory().stickPnel(form);
		return form;

	}

	// public static RequestForQuotationForm initalize(MyQuerry querry) {
	// AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("RFQ",Screen.REQUESTFORQUOTATION);
	// form = new RequestForQuotationForm();
	//
	// RequestForPresenterTable gentable = new
	// RequestForQuotationPresenterTableProxy();
	// gentable.setView(form);
	// gentable.applySelectionModle();
	//
	// RequestForQuotationPresenterSearch.staticSuperTable = gentable;
	// RequestForQuotationPresenterSearch searchpopup = new
	// RequestForQuotationPresenterSearchProxy();
	// form.setSearchpopupscreen(searchpopup);
	//
	// RequestForQuotationPresenter presenter = new
	// RequestForQuotationPresenter(form, new RequsestForQuotation());
	// AppMemory.getAppMemory().stickPnel(form);
	//
	// return form;
	//
	// }

	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.businessprocesslayer.RequsestForQuotation")
	public static class RequestForPresenterTable extends
			SuperTable<RequsestForQuotation> implements
			GeneratedVariableRefrence {
		@Override
		public Object getVarRef(String varName) {
			return null;
		}

		@Override
		public void createTable() {

		}

		@Override
		protected void initializekeyprovider() {

		}

		@Override
		public void addFieldUpdater() {

		}

		@Override
		public void setEnable(boolean state) {

		}

		@Override
		public void applyStyle() {

		}
	};

	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.businessprocesslayer.RequsestForQuotation")
	public static class RequestForQuotationPresenterSearch extends
			SearchPopUpScreen<RequsestForQuotation> {
		@Override
		public MyQuerry getQuerry() {
			return null;
		}

		@Override
		public boolean validate() {
			// TODO Auto-generated method stub
			return true;
		}
	};

	/******************************************************* React On LOI *************************************************/

	public void reactToLetterOFIntent() {
		final LetterOfIntentForm form = LetterOfIntentPresenter.initalize();
		final LetterOfIntent letterof = new LetterOfIntent();

		letterof.setPurchaseReqNo(model.getPurchaseReqNo());
		letterof.setPrDate(model.getPrDate());
		letterof.setReqforQuotationID(model.getCount());
		letterof.setRfqDate(model.getCreationDate());
		letterof.setRefOrderNO(model.getRefOrderNO());
		letterof.setProject(model.getProject());
		letterof.setBranch(model.getBranch());
		letterof.setEmployee(model.getEmployee());
		letterof.setApproverName(model.getApproverName());
		// letterof.setProductinfo(model.getProductinfo());

		ArrayList<ProductDetails> loiProductList = model.getProductinfo();
		for (int i = 0; i < loiProductList.size(); i++) {
			System.out.println("PRODUCT NAME : "
					+ loiProductList.get(i).getProductName());
		}

		letterof.setProductinfo(loiProductList);

		ArrayList<ProductDetails> list1 = letterof.getProductinfo();
		for (int i = 0; i < list1.size(); i++) {
			System.out.println("PRODUCT NAME 1: "
					+ list1.get(i).getProductName());
		}

		// ArrayList<VendorDetails> vendorList =
		// setCommonVendors(model.getVendorinfo());
		// letterof.setVendorinfo(vendorList);
		letterof.setVendorinfo(model.getVendorinfo());

		/**
		 * Copying some more info of PR to LOI Date : 12-09-2016 By Anil Release
		 * : 31 Aug 2016 Project : PURCHASE ERROR SOLVING (NBHC)
		 */

		letterof.setTitle(model.getQuotationName());
		letterof.setResponseDate(model.getResponseDate());
		letterof.setDeliveryDate(model.getDeliveryDate());
		// letterof.setExpectedClosureDate(model.getExpectedClosureDate());
		letterof.setHeader(model.getHeader());
		letterof.setFooter(model.getFooter());

		/**
		 * END
		 */

		// /
		letterof.setAssignTo1(model.getAssignTo1());
		letterof.setAssignTo2(model.getAssignTo2());

		Timer t = new Timer() {
			@Override
			public void run() {
				form.setToNewState();
				form.updateView(letterof);
				form.ibreqforQuotation.setEnabled(false);
				form.dbRfqDate.setEnabled(false);
				form.ibLetterOfIntentID.setEnabled(false);
				form.ibLetterOfIntentID.setValue("");
				// form.getProductTableLOI().setValue(model.getProductinfo());
				form.setEnableFalse();
				form.getPurchaseRequisiteNo().setEnabled(false);
				form.getIbreqforQuotation().setEnabled(false);
				form.getIbLetterOfIntentID().setEnabled(false);
				form.getTbApporverName().setEnabled(true);
				form.getOblEmployee().setEnabled(true);
				form.getBranch().setEnabled(true);
				form.getbAddproducts().setEnabled(false);

				// /////////

				form.getDbCreationDate().setEnabled(true);
				form.getDbExpdeliverydate().setEnabled(true);

				form.getTbtitle().setEnabled(true);
				form.getDbExpectedResposeDate().setEnabled(true);
				// form.getDbExpectedClosureDate().setEnabled(true);
				form.getHeader().setEnabled(true);
				form.getFooter().setEnabled(true);

				form.getbAddproducts().setEnabled(false);
				form.productInfoComposite.setEnable(false);

				// ////////
			}
		};
		t.schedule(5000);

	}

	public void reactToPurchaseOrder() {
		final PurchaseOrderForm form = PurchaseOrderPresenter.initalize();
		final PurchaseOrder po = new PurchaseOrder();

		po.setPurchaseReqNo(model.getPurchaseReqNo());
		po.setPrDate(model.getPrDate());
		po.setRFQID(model.getCount());
		po.setRFQDate(model.getCreationDate());
		po.setRefOrderNO(model.getRefOrderNO());
		po.setProject(model.getProject());
		po.setEmployee(model.getEmployee());
		po.setBranch(model.getBranch());
		po.setApproverName(model.getApproverName());

		List<ProductDetailsPO> poProductList = new ArrayList<ProductDetailsPO>();
		List<ProductDetails> rfqProductList = model.getProductinfo();

		/******** convert one list to another ****/
		for (ProductDetails temp : rfqProductList) {
			ProductDetailsPO purch = new ProductDetailsPO();

			purch.setProdDate(temp.getProdDate());
			purch.setProductID(temp.getProductID());
			purch.setProductName(temp.getProductName());
			purch.setProductCode(temp.getProductCode());
			purch.setProductCategory(temp.getProductCategory());
			purch.setProdPrice(temp.getProdPrice());
			purch.setUnitOfmeasurement(temp.getUnitOfmeasurement());
			purch.setProductQuantity(temp.getProductQuantity());
			purch.setPrduct(temp.getPrduct());
			purch.setTax(temp.getServiceTax());
			purch.setVat(temp.getVat());

			// ////
			purch.setWarehouseName(temp.getWarehouseName());
			purch.setAvailableStock(temp.getAvailableStock());

			poProductList.add(purch);
		}
		po.setProductDetails(poProductList);
		// ArrayList<VendorDetails> vendorList =
		// setCommonVendors(model.getVendorinfo());
		// po.setVendorDetails(vendorList);

		po.setVendorDetails(model.getVendorinfo());

		// ///
		po.setAssignTo1(model.getAssignTo1());
		po.setAssignTo2(model.getAssignTo2());

		/**
		 * Copying some more info of PR to LOI Date : 12-09-2016 By Anil Release
		 * : 31 Aug 2016 Project : PURCHASE ERROR SOLVING (NBHC)
		 */
		po.setPODate(new Date());
		po.setPOName(model.getQuotationName());
		po.setResponseDate(model.getResponseDate());
		po.setDeliveryDate(model.getDeliveryDate());
		// po.setExpectedClosureDate(model.getExpectedClosureDate());
		po.setHeader(model.getHeader());
		po.setFooter(model.getFooter());

		/**
		 * END
		 */

		Timer t = new Timer() {
			@Override
			public void run() {
				form.setToNewState();
				form.updateView(po);
				form.ibreqforQuotation.setEnabled(false);
				form.dbRfqDate.setEnabled(false);
				form.ibLetterOfIntentID.setEnabled(false);
				form.getPurchaseRequisiteNo().setEnabled(false);
				form.dbLOIDate.setEnabled(false);
				form.getTbApporverName().setEnabled(true);
				form.getOblEmployee().setEnabled(true);
				form.getBranch().setEnabled(true);

				double total = form.productTablePO.calculateTotal();
				form.dbTotal.setValue(total);
				form.setAllTable();

				// /////////

				// form.getDbCreationDate().setEnabled(true);
				form.getDbExpdeliverydate().setEnabled(true);

				form.getTbpoName().setEnabled(true);
				form.getDbExpectedResposeDate().setEnabled(true);
				// form.getDbExpectedClosureDate().setEnabled(true);
				form.getHeader().setEnabled(true);
				form.getFooter().setEnabled(true);

				if (form.isWarehouseSelected()) {
					form.getDeliveryadd().clear();
					form.getDeliveryadd().setEnable(false);
					form.getCbadds().setEnabled(false);
				}

				// ////////

				// ////
				form.prodInfoComposite.setEnable(false);
				form.addproducts.setEnabled(false);

			}
		};
		t.schedule(5000);

	}

	/******************************************* react on download ***************************************************/

	public void reactOnDownload() {
		CsvServiceAsync async = GWT.create(CsvService.class);
		ArrayList<RequsestForQuotation> reqQuotationarray = new ArrayList<RequsestForQuotation>();
		List<RequsestForQuotation> list = form.getSearchpopupscreen()
				.getSupertable().getDataprovider().getList();

		reqQuotationarray.addAll(list);

		async.setRequestForQuotation(reqQuotationarray,
				new AsyncCallback<Void>() {

					@Override
					public void onFailure(Throwable caught) {
						System.out.println("RPC call Failed" + caught);

					}

					@Override
					public void onSuccess(Void result) {
						String gwt = com.google.gwt.core.client.GWT
								.getModuleBaseURL();
						final String url = gwt + "csvservlet" + "?type=" + 43;
						Window.open(url, "test", "enabled");
					}
				});

	}

	/**************************************** change status ************************************************************/

	public void ChangeStatus(final String status) {
		model.setStatus(status);
		async.save(model, new AsyncCallback<ReturnFromServer>() {
			@Override
			public void onFailure(Throwable caught) {
				form.showDialogMessage("An Unexpected Error occured !");
			}

			@Override
			public void onSuccess(ReturnFromServer result) {
				form.setToViewState();
				form.tbstatus.setText(status);
			}
		});
	}

	/****************************************** On Click Event ******************************************/

	@Override
	public void onClick(ClickEvent event) {
		super.onClick(event);

		if (event.getSource() == cancelPopup.getBtnOk()) {
			cancelpanel.hide();
			changeStatusOnCancelRFQ(RequsestForQuotation.CANCELLED);
		}

		if (event.getSource() == cancelPopup.getBtnCancel()) {
			cancelpanel.hide();
			cancelPopup.remark.setValue("");
		}

	}

	/****************************************** RFQ Cancellation *******************************************/

	public void changeStatusOnCancelRFQ(final String status) {
		model.setStatus(status);
		model.setDescription("RFQ Id = " + model.getCount() + " "
				+ "RFQ status =" + form.getstatustextbox().getValue().trim()
				+ "\n" + "has been cancelled by " + LoginPresenter.loggedInUser
				+ " with remark  " + "\n" + "Remark ="
				+ cancelPopup.getRemark().getValue().trim() + " "
				+ "Cancellation Date =" + new Date());
		async.save(model, new AsyncCallback<ReturnFromServer>() {
			@Override
			public void onFailure(Throwable caught) {
				form.showDialogMessage("An Unexpected Error occured !");
			}

			@Override
			public void onSuccess(ReturnFromServer result) {
				form.setToViewState();
				form.tbstatus.setText(status);
				form.taDescription.setValue(model.getDescription());
			}
		});
	}

	public ArrayList<VendorDetails> setCommonVendors(List<VendorDetails> list) {
		HashSet<VendorDetails> hset = new HashSet<VendorDetails>(list);
		ArrayList<VendorDetails> newvend = new ArrayList<VendorDetails>();
		newvend.addAll(hset);
		return newvend;
	}

	@Override
	public void onRowCountChange(RowCountChangeEvent event) {
		if (event.getSource().equals(form.productTableRFQ.getTable())) {
			Double amount = 0d;

			for (ProductDetails prod : form.productTableRFQ.getDataprovider()
					.getList()) {
				amount = amount + prod.getProdPrice();
			}

			String approverName = "";
			if (form.tbApporverName.getValue() != null) {
				approverName = form.tbApporverName.getValue();
			}
			AppUtility.getApproverListAsPerTotalAmount(form.tbApporverName,
					"Request For Quotation", approverName, amount);

		}
	}

	/**
	 * This method update vendor table when we delete product Date : 10-09-2016
	 * By Anil Release : 31 Aug 2016 Project : PURCHASE ERROR SOLVING (NBHC)
	 */

	public static void updateVendorTable() {

		System.out.println("PRODUCT TBL SIZE : "
				+ form.productTableRFQ.getDataprovider().getList().size());
		if (form.productTableRFQ.getDataprovider().getList().size() == 0) {
			form.vendorTableRFQ.getDataprovider().getList().clear();
			form.vendorRateList.clear();
		} else {
			List<ProductDetails> prodList = form.productTableRFQ.getValue();
			System.out.println("GLOBAL VENDOR PRICE LIST SIZE BF : "
					+ form.vendorRateList.size());
			List<PriceListDetails> remProdComVenLis = getRemainingProductVendorPriceList(
					prodList, form.vendorRateList);
			System.out.println("GLOBAL VENDOR PRICE LIST SIZE AF : "
					+ form.vendorRateList.size());
			System.out.println("COMMON VENDOR PRICEL LIST SIZE : "
					+ remProdComVenLis.size());
			System.out.println("EARLIER VEN TBL LIST SIZE :"
					+ form.vendorTableRFQ.getDataprovider().getList().size());
			List<VendorDetails> vendorList = getVendorListOfRemainingProduct(
					form.vendorTableRFQ.getDataprovider().getList(),
					remProdComVenLis);
			System.out.println("NOW VEN TBL LIST SIZE : " + vendorList.size());
			// form.vendorDetailList.clear();
			// form.vendorDetailList.addAll(vendorList);
			System.out.println("AFTER REMOVING : "
					+ form.vendorTableRFQ.getDataprovider().getList().size());
		}
	}

	/**
	 * This method returns the list of common vendor from global vendor list
	 * which is maintained at the time of product add.
	 * 
	 * Date : 10-09-2016 By Anil Release : 31 Aug 2016 Project : PURCHASE ERROR
	 * SOLVING (NBHC) I/P:existing vendor table list and global vendor list
	 * O/P:vendor list
	 */
	private static List<PriceListDetails> getRemainingProductVendorPriceList(
			List<ProductDetails> prodList,
			ArrayList<PriceListDetails> vendorRateList) {

		ArrayList<PriceListDetails> vendorPriceList = new ArrayList<PriceListDetails>();
		List<PriceListDetails> list = new ArrayList<PriceListDetails>();

		for (ProductDetails prodDet : prodList) {
			for (PriceListDetails priceLis : vendorRateList) {
				if (prodDet.getProductID() == priceLis.getProdID()) {
					vendorPriceList.add(priceLis);
				}
			}
		}

		form.vendorRateList.clear();
		form.vendorRateList.addAll(vendorPriceList);

		for (PriceListDetails pricelis : vendorPriceList) {
			if (list.size() == 0) {
				list.add(pricelis);
			} else {
				boolean isAlreadyAddedFlag = checkPriceListAlreadyAdded(list,
						pricelis);
				if (isAlreadyAddedFlag == false) {
					list.add(pricelis);
				}
			}
		}
		return list;
	}

	/**
	 * This method compare whether passed parameter 2nd is present in parameter
	 * 1st i.e. vendor price list Date 10-09-2016 By Anil Release : 31 Aug 2016
	 * Project: PURCHASE ERROR SOLVING(NBHC) I/P:vendor price list and vendor
	 * price O/P:True/False
	 */
	private static boolean checkPriceListAlreadyAdded(
			List<PriceListDetails> list, PriceListDetails pricelis) {
		for (PriceListDetails pricelist : list) {
			if (pricelis.getVendorID() == pricelist.getVendorID()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * This method returns common vendors of remaining products. Date 10-09-2016
	 * By Anil Release : 31 Aug 2016 Project: PURCHASE ERROR SOLVING(NBHC)
	 * I/P:existing vendor list and remaining products vendor list O/P:vendor
	 * list
	 */

	private static List<VendorDetails> getVendorListOfRemainingProduct(
			List<VendorDetails> vendorList, List<PriceListDetails> venRateLis) {
		System.out.println("REMAINING PRODUCTS VENDOR LIST");

		System.out.println("VEN LIS SIZE BF " + vendorList.size());
		for (PriceListDetails venRLis : venRateLis) {
			boolean vendorFlag = isVendorPresent(venRLis.getVendorID(),
					vendorList);
			if (vendorFlag == false) {
				List<PriceListDetails> vLis = new ArrayList<PriceListDetails>();
				vLis.add(venRLis);
				ArrayList<VendorDetails> vDeLis = form.getVendorList(vLis);
				vendorList.addAll(vDeLis);
			}
		}

		System.out.println("VEN LIS SIZE AF " + vendorList.size());
		return vendorList;
	}

	/**
	 * This method returns true if passed vendor id is present in given vendor
	 * list. Date : 10-09-2016 By Anil Release : 31 Aug 2016 Project: PURCHASE
	 * ERROR SOLVING (NBHC) I/P:Vendor id and Vendor List O/P:True/False
	 */
	private static boolean isVendorPresent(int vendorID,
			List<VendorDetails> vendorList) {
		for (VendorDetails venDet : vendorList) {
			if (vendorID == venDet.getVendorId()) {
				return true;
			}
		}
		return false;
	}
	
	private void setEmailPopUpData() {
		form.showWaitSymbol();

		String branchName = form.branch.getValue();
		String fromEmailId = AppUtility.getFromEmailAddress(branchName,model.getEmployee());
		List<VendorDetails> vendorlist = form.vendorTableRFQ.getDataprovider().getList();
		List<ContactPersonIdentification> vendorDetailslist = AppUtility.getvenodrEmailId(vendorlist,true);
		System.out.println("vendorDetailslist "+vendorDetailslist);
		label : AppUtility.loadContactPersonlist(emailpopup.olbContactlistEmailId,"Vendor",-1,"","",vendorDetailslist);
		Screen screenName=(Screen) AppMemory.getAppMemory().currentScreen;
		Console.log("screenName "+screenName);
		AppUtility.makeLiveSmsTemplateConfig(emailpopup.oblTemplateName,AppConstants.PURCHASEMODULE.trim(),screenName);
		emailpopup.taFromEmailId.setValue(fromEmailId);
		emailpopup.smodel = model;
		form.hideWaitSymbol();
		
	}

}
