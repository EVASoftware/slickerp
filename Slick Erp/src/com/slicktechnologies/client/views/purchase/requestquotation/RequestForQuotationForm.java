package com.slicktechnologies.client.views.purchase.requestquotation;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Vector;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.google.gwt.view.client.RowCountChangeEvent.Handler;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.composite.UploadComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.approvalutility.ApprovableFormScreen;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.client.views.purchase.purchaserequisition.PurchaseRequitionPresenter;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseRequisition;
import com.slicktechnologies.shared.common.businessprocesslayer.RequsestForQuotation;
import com.slicktechnologies.shared.common.businessprocesslayer.PriceListDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.PriceList;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessprocesslayer.VendorDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class RequestForQuotationForm extends
		ApprovableFormScreen<RequsestForQuotation> implements ClickHandler,
		ChangeHandler, Handler {
	final GenricServiceAsync async = GWT.create(GenricService.class);

	TextBox purchaseRequisiteNo;
	DateBox prDate;
	TextBox ibRefOrderNO;

	TextBox ibRequestQuotationID;
	TextBox tbName;
	DateBox dbCreationDate, dbResponseDate;
	DateBox dbExpdeliverydate;
	// DateBox dbExpectedClosureDate;

	ObjectListBox<ConfigCategory> oblquotationCategory;
	ObjectListBox<Type> oblquotationType;

	public ProductInfoComposite productInfoComposite;
	Button bAddproducts;

	public ProductTable productTableRFQ;
	public VendorTable vendorTableRFQ;
	TextArea taDescription;
	ObjectListBox<Employee> oblEmployee;
	ObjectListBox<Employee> tbApporverName;
	ObjectListBox<Branch> branch;
	ObjectListBox<HrProject> project;
	FormField fgroupingRFQDetails;
	// PersonInfoComposite vic;
	TextBox tbstatus;
	TextBox tbcommentForStatusChange;
	UploadComposite upload;
	TextArea header, footer;

	/**
	 * This list provides the rate list of all vendor supplying the product .
	 * Date : 10-09-2016 By Anil Release 31 Aug 2016 Project:PURCHASE ERROR
	 * SOLVING (NBHC)
	 */
	public ArrayList<PriceListDetails> vendorRateList = new ArrayList<PriceListDetails>();

	/**
	 * END
	 */

	/**
	 * Purchase Engg. assigned PR to its team & vendor composite; Date :
	 * 28-09-2016 By Anil Release :30 Sept 2016 Project : PURCHASE MODIFICATION
	 * (NBHC)
	 */
	ObjectListBox<Employee> olbAssignedTo1;
	ObjectListBox<Employee> olbAssignedTo2;
	PersonInfoComposite vendorComp;
	Button btnAddVendor;

	/**
	 * End
	 */

	RequsestForQuotation rfqobj;

	public RequestForQuotationForm() {
		super();
		createGui();
		productTableRFQ.connectToLocal();
		vendorTableRFQ.connectToLocal();
		tbstatus.setValue(RequsestForQuotation.CREATED);
		tbcommentForStatusChange.setEnabled(false);
		dbCreationDate.setEnabled(false);
	}

	public RequestForQuotationForm(String[] processlevel, FormField[][] fields,
			FormStyle formstyle) {
		super(processlevel, fields, formstyle);
		createGui();
		tbcommentForStatusChange.setEnabled(false);
		dbCreationDate.setEnabled(false);
	}

	private void initalizeWidget() {
		purchaseRequisiteNo = new TextBox();
		purchaseRequisiteNo.addChangeHandler(this);
		prDate = new DateBoxWithYearSelector();
		prDate.setEnabled(false);

		ibRequestQuotationID = new TextBox();
		ibRequestQuotationID.setEnabled(false);
		tbName = new TextBox();

		dbCreationDate = new DateBoxWithYearSelector();
		Date date = new Date();
		dbCreationDate.setValue(date);
		dbCreationDate.setEnabled(false);

		dbResponseDate = new DateBoxWithYearSelector();
		// dbExpectedClosureDate = new DateBoxWithYearSelector();
		dbExpdeliverydate = new DateBoxWithYearSelector();

		ibRefOrderNO = new TextBox();
		taDescription = new TextArea();
		oblquotationCategory = new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(oblquotationCategory,
				Screen.RFQCATEGORY);
		oblquotationCategory.addChangeHandler(this);
		oblquotationType = new ObjectListBox<Type>();
		AppUtility.makeTypeListBoxLive(oblquotationType, Screen.RFQTYPE);
		oblEmployee = new ObjectListBox<Employee>();
//		initalizEmployeeNameListBox();
		/**
		 * Date 14-02-2017 By Anil
		 */
		oblEmployee.makeEmployeeLive(AppConstants.PURCHASEMODULE, AppConstants.RFQ, "Purchase Engineer");
		/**
		 * End
		 */
		
		branch = new ObjectListBox<Branch>();
		initalizEmployeeBranchListBox();
		tbApporverName = new ObjectListBox<Employee>();
		AppUtility.makeApproverListBoxLive(tbApporverName,
				"Request For Quotation");
		project = new ObjectListBox<HrProject>();
		HrProject.MakeProjectListBoxLive(project);

		productInfoComposite = AppUtility
				.initiatePurchaseProductComposite(new ItemProduct());
		bAddproducts = new Button("Add Products");
		bAddproducts.addClickHandler(this);

		productTableRFQ = new ProductTable();
		productTableRFQ.getTable().addRowCountChangeHandler(this);

		vendorTableRFQ = new VendorTable();

		header = new TextArea();
		footer = new TextArea();
		tbstatus = new TextBox();
		tbstatus.setEnabled(false);
		tbcommentForStatusChange = new TextBox();
		tbcommentForStatusChange.setEnabled(false);
		upload = new UploadComposite();

		olbAssignedTo1 = new ObjectListBox<Employee>();
//		AppUtility.makeSalesPersonListBoxLive(olbAssignedTo1);
		/**
		 * Date 14-02-2017 By Anil
		 */
		olbAssignedTo1.makeEmployeeLive(AppConstants.PURCHASEMODULE, AppConstants.RFQ, "Assign To 1");
		/**
		 * End
		 */

		
		olbAssignedTo2 = new ObjectListBox<Employee>();
//		AppUtility.makeSalesPersonListBoxLive(olbAssignedTo2);
		/**
		 * Date 14-02-2017 By Anil
		 */
		olbAssignedTo2.makeEmployeeLive(AppConstants.PURCHASEMODULE, AppConstants.RFQ, "Assign To 2");
		/**
		 * End
		 */
		
		MyQuerry querry = new MyQuerry();
		querry.setQuerryObject(new Vendor());
		Filter filter = new Filter();
		filter.setQuerryString("vendorStatus");
		filter.setBooleanvalue(true);
		querry.getFilters().add(filter);

		vendorComp = new PersonInfoComposite(querry, false);
		btnAddVendor = new Button("Add");
		btnAddVendor.addClickHandler(this);

	}

	@Override
	public void createScreen() {
		initalizeWidget();

		this.processlevelBarNames = new String[] {ManageApprovals.SUBMIT,
				ManageApprovals.APPROVALREQUEST,
				ManageApprovals.CANCELAPPROVALREQUEST, "Create LOI",
				"Create PO", "Mark Close", AppConstants.CANCELRFQ, "New" };
		
		String mainScreenLabel="RFQ";
		if(rfqobj!=null&&rfqobj.getCount()!=0){
			
			
			mainScreenLabel=rfqobj.getCount()+" "+"/"+" "+rfqobj.getStatus()+" "+"/"+" "+AppUtility.parseDate(rfqobj.getCreationDate());
		}
		//fgroupingRFQDetails.getHeaderLabel().setText(mainScreenLabel);
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		fgroupingRFQDetails=fbuilder.setlabel(mainScreenLabel).widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).setKeyField(true).build();
	
		//FormField fgroupingRFQDetails = fbuilder.setlabel("Reference Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		
		
		fbuilder = new FormFieldBuilder("PR No.", purchaseRequisiteNo);
		FormField fpuchasereq = fbuilder.setMandatory(false).setRowSpan(0)                
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("PR Date", prDate);
		FormField fpuchasereqdate = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		FormField fgroupingProductInformation = fbuilder
				.setlabel("Product Information").widgetType(FieldType.Grouping)
				.setMandatory(false).setColSpan(5).build();

		fbuilder = new FormFieldBuilder("", bAddproducts);
		FormField fbAddproducts = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder();
		FormField fproductTableGroup = fbuilder.setlabel("Product")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(5).build();

		fbuilder = new FormFieldBuilder("", productTableRFQ.getTable());
		FormField fproductTableRFQ = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(5).build();

		fbuilder = new FormFieldBuilder();
		FormField fvendorTableGroup = fbuilder.setlabel("Vendor")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(5).build();

		fbuilder = new FormFieldBuilder("", vendorTableRFQ.getTable());
		FormField fvendorTableRFQ = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(5).build();

		fbuilder = new FormFieldBuilder("E-mail Header", header);
		FormField fheader = fbuilder.setMandatory(false)
				.setMandatoryMsg("Header is mandatory").setRowSpan(0)
				.setColSpan(5).build();

		fbuilder = new FormFieldBuilder("E-mail Footer ", footer);
		FormField ffooter = fbuilder.setMandatory(false)
				.setMandatoryMsg("Footer is mandatory").setRowSpan(0)
				.setColSpan(5).build();

		fbuilder = new FormFieldBuilder("RFQ ID", ibRequestQuotationID);
		FormField fibRequestQuotationID = fbuilder.setMandatory(false)
				.setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("RFQ Title", tbName);
		FormField ftbName = fbuilder.setMandatory(false)
				.setMandatoryMsg("Quotation Title is mandatory").setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Status", tbstatus);
		FormField ftbstatus = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Comment", tbcommentForStatusChange);
		FormField ftbcommentForStatusChange = fbuilder.setMandatory(false)
				.setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Purchase Engineer", oblEmployee);
		FormField foblEmployee = fbuilder.setMandatory(false)
				.setMandatoryMsg("Pur. Eng. is mandatory").setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("* RFQ Date", dbCreationDate);// creation
																		// Date
		FormField fdbCreationDate = fbuilder.setMandatory(true)
				.setMandatoryMsg("RFQ Date is mandatory").setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Expected Response Date",
				dbResponseDate);
		FormField fdbResponseDate = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("RFQ Type", oblquotationType);
		FormField foblquotationType = fbuilder.setMandatory(false)
				.setMandatoryMsg("RFQ Type is mandatory").setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("RFQ Category", oblquotationCategory);
		FormField foblquotationCategory = fbuilder.setMandatory(false)
				.setMandatoryMsg("RFQ Category is mandatory").setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("", productInfoComposite);
		FormField fproductPriceList = fbuilder.setMandatory(false)
				.setRowSpan(0).setColSpan(3).build();

		// fbuilder = new
		// FormFieldBuilder("* Expected Closure Date",dbExpectedClosureDate);
		// FormField fdbExpectedClosureDate =
		// fbuilder.setMandatory(true).setMandatoryMsg("Expected Closure Date is mandatory").setRowSpan(0)
		// .setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Reference Order NO", ibRefOrderNO);
		FormField freforderno = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("* Branch", branch);
		FormField fbranch = fbuilder.setMandatory(true)
				.setMandatoryMsg("Branch is mandatory").setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("* Approver Name", tbApporverName);
		FormField ftbApporverName = fbuilder.setMandatory(true)
				.setMandatoryMsg("Approver Name is mandatory").setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Project", project);
		FormField fproject = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("* Expected Delivery Date",
				dbExpdeliverydate);
		FormField fexpdeldate = fbuilder.setMandatory(true)
				.setMandatoryMsg("Expected Delivery date is mandatory")
				.setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Upload Document", upload);
		FormField fdoc = fbuilder.setMandatory(false)
				.setMandatoryMsg("Branch is mandatory").setRowSpan(0)
				.setColSpan(2).build();

		FormField fgroupingClassificationDetails = fbuilder.setlabel("Classification")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(5).build();
		
		FormField fgroupingReferenceDetails = fbuilder.setlabel("Reference/General")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(5).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingAttachmentDetails = fbuilder.setlabel("Attachment")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(5).build();

		FormField fgroupingDescription = fbuilder.setlabel("Description")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Description (Max 500 characters)", taDescription);
		FormField ftaDescription = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(5).build();

		fbuilder = new FormFieldBuilder("Assign To 1", olbAssignedTo1);
		FormField folbAssignedTo1 = fbuilder.setMandatory(false).setColSpan(0)
				.build();

		fbuilder = new FormFieldBuilder("Assign To 2", olbAssignedTo2);
		FormField folbAssignedTo2 = fbuilder.setMandatory(false).setColSpan(0)
				.build();

		fbuilder = new FormFieldBuilder("", vendorComp);
		FormField fvendorComp = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(3).build();

		fbuilder = new FormFieldBuilder("", btnAddVendor);
		FormField fbtaddVendor = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		FormField[][] formfield = {
//				{ fgroupingReferenceDetails },
//				{ fpuchasereq, fpuchasereqdate, freforderno, fproject },
//				{ fgroupingProductInformation },
//				{ fibRequestQuotationID, ftbName, fdbCreationDate,fdbResponseDate },
//				{ fexpdeldate, foblquotationCategory, foblquotationType,fbranch },
//				{ foblEmployee, ftbApporverName, ftbstatus,ftbcommentForStatusChange },
//				{ folbAssignedTo1, folbAssignedTo2, fdoc },
//				{ fgroupingDescription },
//				{ ftaDescription },
//				{ fproductTableGroup }, 
//				{ fproductPriceList, fbAddproducts },
//				{ fproductTableRFQ },
//				{ fvendorTableGroup },
//				{ fvendorComp, fbtaddVendor },
//				{ fvendorTableRFQ },
//				{ fheader },
//				{ ffooter }
//				};
				/**RFQ INFORMATION**/
				{ fgroupingRFQDetails },
				{ ftbName,fdbCreationDate,fexpdeldate,fbranch},
				{ foblEmployee,ftbApporverName,ftbcommentForStatusChange},
				/**Product**/
				{ fproductTableGroup }, 
				{ fproductPriceList, fbAddproducts },
				{ fproductTableRFQ },
				/**Vendor form**/
				{ fvendorTableGroup },
				{ fvendorComp, fbtaddVendor },
				{ fvendorTableRFQ },
				/**Classification**/
				{ fgroupingClassificationDetails },
				{ foblquotationCategory,foblquotationType},
				/**Refernces..**/
				{ fgroupingReferenceDetails },
				{ fpuchasereq, fpuchasereqdate, freforderno, fproject },
				{ fdbResponseDate,folbAssignedTo1,folbAssignedTo2},
				{ ftaDescription },
				/**Attachment**/
				{ fgroupingAttachmentDetails },
				{ fheader },
				{ ffooter },
				{ fdoc }
				
				
				
				
				};
		this.fields = formfield;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void updateModel(RequsestForQuotation model) {
		if (productTableRFQ.getValue() != null)
			model.setProductinfo(productTableRFQ.getValue());

		if (vendorTableRFQ.getValue() != null)
			model.setVendorinfo(vendorTableRFQ.getValue());

		if (header.getValue() != null)
			model.setHeader(header.getValue());
		if (footer.getValue() != null)
			model.setFooter(footer.getValue());

		if (tbName.getValue() != null)
			model.setQuotationName(tbName.getValue());
		if (tbstatus.getValue() != null)
			model.setStatus(tbstatus.getValue());
		// if (tbcommentForStatusChange.getValue() != null)
		// model.setCommentStatusChange(tbcommentForStatusChange.getValue());

		if (oblEmployee.getValue() != null)
			model.setEmployee(oblEmployee.getValue());
		if (prDate.getValue() != null)
			model.setPrDate(prDate.getValue());

		if (dbCreationDate.getValue() != null)
			model.setCreationDate(dbCreationDate.getValue());
		if (dbResponseDate.getValue() != null)
			model.setResponseDate(dbResponseDate.getValue());
		if (oblquotationCategory.getValue() != null)
			model.setQuotationCategory(oblquotationCategory.getValue());
		if (oblquotationType.getValue() != null)
			model.setQuotationType(oblquotationType.getValue(oblquotationType
					.getSelectedIndex()));
		// if (dbExpectedClosureDate.getValue() != null)
		// model.setExpectedClosureDate(dbExpectedClosureDate.getValue());
		if (ibRefOrderNO.getValue() != null)
			model.setRefOrderNO(ibRefOrderNO.getValue());
		if (branch.getValue() != null)
			model.setBranch(branch.getValue());

		if (tbApporverName.getValue() != null)
			model.setApproverName(tbApporverName.getValue());
		if (taDescription.getValue() != null) {
			model.setDescription(taDescription.getValue());
		}
		if (project.getValue() != null)
			model.setProject(project.getValue());
		if (!purchaseRequisiteNo.getValue().equals(""))
			model.setPurchaseReqNo(Integer.parseInt(purchaseRequisiteNo
					.getValue()));
		if (dbExpdeliverydate.getValue() != null)
			model.setDeliveryDate(dbExpdeliverydate.getValue());
		if (upload.getValue() != null)
			model.setUptestReport(upload.getValue());

		if (olbAssignedTo1.getValue() != null) {
			model.setAssignTo1(olbAssignedTo1.getValue());
		}

		if (olbAssignedTo2.getValue() != null) {
			model.setAssignTo2(olbAssignedTo2.getValue());
		}

		rfqobj = model;
		presenter.setModel(model);

	}

	@SuppressWarnings("unchecked")
	@Override
	public void updateView(RequsestForQuotation model) {

		rfqobj = model;
		ibRequestQuotationID.setValue(model.getCount() + "");
		if (model.getApproverName() != null)
			tbApporverName.setValue(model.getApproverName());
		if (model.getProductinfo() != null) {
			productTableRFQ.setValue(model.getProductinfo());

			List<String> prodIdList = new ArrayList<String>();
			HashSet<String> uniqueCodeList = new HashSet<>();
			for (ProductDetails prodDet : model.getProductinfo()) {
				uniqueCodeList.add(prodDet.getProductCode());
			}
			prodIdList.addAll(uniqueCodeList);
			loadVendorProductPriceList(prodIdList);
		}
		if (model.getVendorinfo() != null)
			vendorTableRFQ.setValue(model.getVendorinfo());
		if (model.getHeader() != null)
			header.setValue(model.getHeader());
		if (model.getFooter() != null)
			footer.setValue(model.getFooter());
		if (model.getPrDate() != null)
			prDate.setValue(model.getPrDate());
		if (model.getQuotationName() != null)
			tbName.setValue(model.getQuotationName());
		if (model.getStatus() != null)
			tbstatus.setValue(model.getStatus());
		if (model.getRemark() != null)
			tbcommentForStatusChange.setValue(model.getRemark());
		if (model.getEmployee() != null)
			oblEmployee.setValue(model.getEmployee());
		if (model.getCreationDate() != null)
			dbCreationDate.setValue(model.getCreationDate());
		if (model.getResponseDate() != null)
			dbResponseDate.setValue(model.getResponseDate());
		if (model.getQuotationCategory() != null)
			oblquotationCategory.setValue(model.getQuotationCategory());
		if (model.getQuotationType() != null)
			oblquotationType.setValue(model.getQuotationType());
		// if (model.getExpectedClosureDate() != null)
		// dbExpectedClosureDate.setValue(model.getExpectedClosureDate());
		if (model.getRefOrderNO() != null)
			ibRefOrderNO.setValue(model.getRefOrderNO());
		if (model.getBranch() != null)
			branch.setValue(model.getBranch());
		if (model.getProject() != null)
			project.setValue(model.getProject());
		if (model.getPurchaseReqNo() != 0)
			purchaseRequisiteNo.setValue(model.getPurchaseReqNo() + "");
		if (model.getDeliveryDate() != null)
			dbExpdeliverydate.setValue(model.getDeliveryDate());
		if (model.getUptestReport() != null)
			upload.setValue(model.getUptestReport());
		if (model.getDescription() != null) {
			taDescription.setValue(model.getDescription());
		}

		if (model.getAssignTo1() != null) {
			olbAssignedTo1.setValue(model.getAssignTo1());
		}
		if (model.getAssignTo2() != null) {
			olbAssignedTo2.setValue(model.getAssignTo2());
		}
		/* 
		 * for approval process
		 *  nidhi
		 *  5-07-2017
		 */
		if(presenter != null){
			presenter.setModel(model);
		}
		/*
		 *  end
		 */
	}

	@Override
	public void toggleAppHeaderBarMenu() {
		if (AppMemory.getAppMemory().currentState == ScreeenState.NEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Save") || text.equals("Discard")
						|| text.equals("Search")
						|| text.equals(AppConstants.NAVIGATION)) {
					menus[k].setVisible(true);
				} else
					menus[k].setVisible(false);
			}
		} else if (AppMemory.getAppMemory().currentState == ScreeenState.EDIT) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Save") || text.equals("Discard")
						|| text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true);
				else
					menus[k].setVisible(false);
			}
		} else if (AppMemory.getAppMemory().currentState == ScreeenState.VIEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Discard") || text.equals("Edit")
						|| text.equals("Search")
						|| text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true);
				else
					menus[k].setVisible(false);
			}

		}
		AuthorizationHelper.setAsPerAuthorization(Screen.REQUESTFORQUOTATION,
				LoginPresenter.currentModule.trim());

	}

	public void toggleProcessLevelMenu() {
		RequsestForQuotation entity = (RequsestForQuotation) presenter
				.getModel();
		String status = entity.getStatus();
		for (int i = 0; i < getProcesslevelBarNames().length; i++) {
			InlineLabel label = getProcessLevelBar().btnLabels[i];
			String text = label.getText().trim();

			if (status.equals(RequsestForQuotation.CLOSED)) {
				System.out.println("in toggler process level menu");
				if (text.equals("Create LOI"))
					label.setVisible(true);
				if (text.equals("Create PO"))
					label.setVisible(true);
				if (text.equals("Mark Close"))
					label.setVisible(false);
				if (text.equals(AppConstants.CANCELRFQ))
					label.setVisible(false);
				if (text.equals(ManageApprovals.APPROVALREQUEST))
					label.setVisible(false);
				if (text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
					label.setVisible(false);
				if(text.equals(ManageApprovals.SUBMIT))
					label.setVisible(false);

			} else if (status.equals(RequsestForQuotation.APPROVED)) {
				if (text.equals("Create LOI"))
					label.setVisible(true);
				if (text.equals("Create PO"))
					label.setVisible(true);
				if (text.equals("Mark Close"))
					label.setVisible(true);
				if (text.equals(AppConstants.CANCELRFQ))
					label.setVisible(true);
				if(text.equals(ManageApprovals.SUBMIT))
					label.setVisible(false);

			} else if (status.equals(RequsestForQuotation.CANCELLED)) {
				if (text.equals("Create LOI"))
					label.setVisible(false);
				if (text.equals("Create PO"))
					label.setVisible(false);
				if (text.equals("Mark Close"))
					label.setVisible(false);
				if (text.equals(AppConstants.CANCELRFQ))
					label.setVisible(false);
				if (text.equals(ManageApprovals.APPROVALREQUEST))
					label.setVisible(false);
				if (text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
					label.setVisible(false);
				if(text.equals(ManageApprovals.SUBMIT))
					label.setVisible(false);
			} else if (status.equals(RequsestForQuotation.REJECTED)) {
				if (text.equals("Mark Close"))
					label.setVisible(false);
				if (text.equals(ManageApprovals.APPROVALREQUEST))
					label.setVisible(false);
				if (text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
					label.setVisible(false);
				if (text.equals(AppConstants.CANCELRFQ))
					label.setVisible(false);
				if (text.equals("Create LOI"))
					label.setVisible(false);
				if (text.equals("Create PO"))
					label.setVisible(false);
				if(text.equals(ManageApprovals.SUBMIT))
					label.setVisible(false);
			} else if (status.equals(RequsestForQuotation.CREATED)) {
				if (text.equals("Mark Close"))
					label.setVisible(false);
				if (text.equals(ManageApprovals.APPROVALREQUEST))
					label.setVisible(true);
				if (text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
					label.setVisible(false);
				if (text.equals(AppConstants.CANCELRFQ))
					label.setVisible(true);
				if (text.equals("Create LOI"))
					label.setVisible(false);
				if (text.equals("Create PO"))
					label.setVisible(false);
				
				/**
				 * @author Anil
				 * @since 04-01-2021
				 */
				if(getManageapproval().isSelfApproval()){
					if(text.equals(ManageApprovals.SUBMIT))
						label.setVisible(true);
					if(text.equals(ManageApprovals.APPROVALREQUEST))
						label.setVisible(false);
				}else{
					if(text.equals(ManageApprovals.SUBMIT))
						label.setVisible(false);
					if(text.equals(ManageApprovals.APPROVALREQUEST))
						label.setVisible(true);
				}
				
			} else if (status.equals(RequsestForQuotation.REQUESTED)) {
				if (text.equals("Mark Close"))
					label.setVisible(false);
				if (text.equals(ManageApprovals.APPROVALREQUEST))
					label.setVisible(false);
				if (text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
					label.setVisible(true);
				if (text.equals(AppConstants.CANCELRFQ))
					label.setVisible(false);
				if (text.equals("Create LOI"))
					label.setVisible(false);
				if (text.equals("Create PO"))
					label.setVisible(false);
				if(text.equals(ManageApprovals.SUBMIT))
					label.setVisible(false);
			} else {
				if (text.equals("Create LOI"))
					label.setVisible(false);
				if (text.equals("Create PO"))
					label.setVisible(false);
				if (text.equals("Mark Close"))
					label.setVisible(false);
				if (text.equals(AppConstants.CANCELRFQ))
					label.setVisible(false);
				if(text.equals(ManageApprovals.SUBMIT))
					label.setVisible(false);
			}
		}
	}

	/**************************************** On Click Function ****************************************************/

	@Override
	public void onClick(ClickEvent event) {
		if (event.getSource().equals(bAddproducts)
				&& !productInfoComposite.getProdCode().getValue().equals("")) {
			if (validateProduct())
				prodctType(productInfoComposite.getProdCode().getValue());
		}

		if (event.getSource().equals(btnAddVendor)
				&& !vendorComp.getName().getValue().equals("")) {
			if (validVendor()) {
				checkPriceListAndAddVendorToVendorTable();

			}
		}

	}

	/**
	 * This method is called when you are adding vendor this method checks
	 * whether price list of vendor for added product is defined or not. Date :
	 * 28-09-2016 By Anil Release : 30 Sept 2016 Project : PURCHASE MODIFICATION
	 * (NBHC)
	 */
	private void checkPriceListAndAddVendorToVendorTable() {
		System.out.println("INSIDE VENDOR ADD CODE ");
		List<ProductDetails> list = productTableRFQ.getDataprovider().getList();
		HashSet<String> hashList = new HashSet<String>();
		final ArrayList<String> codeList = new ArrayList<String>();
		for (ProductDetails obj : list) {
			hashList.add(obj.getProductCode());
		}
		codeList.addAll(hashList);

		System.out.println("CODE LIST SIZE " + codeList.size());
		Vector<Filter> temp = new Vector<Filter>();
		MyQuerry querry = new MyQuerry();
		Filter filter = null;

		filter = new Filter();
		filter.setQuerryString("prodCode IN");
		filter.setList(codeList);
		temp.add(filter);

		filter = new Filter();
		filter.setQuerryString("priceInfo.personinfo.count");
		filter.setIntValue(vendorComp.getIdValue());
		temp.add(filter);

		querry.setFilters(temp);
		querry.setQuerryObject(new PriceList());
		showWaitSymbol();
		async.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onFailure(Throwable caught) {
						hideWaitSymbol();
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> vresult) {
						hideWaitSymbol();
						System.out.println("VEN PRICE LIS SIZE : "
								+ vresult.size());
						if (vresult.size() != 0) {
							ArrayList<PriceList> priceList = new ArrayList<PriceList>();
							for (SuperModel smodel : vresult) {
								PriceList pentity = (PriceList) smodel;
								priceList.add(pentity);
							}

							if (priceList.size() == codeList.size()) {
								VendorDetails ve = new VendorDetails();

								for (PriceList entity : priceList) {
									vendorRateList.addAll(entity.getPriceInfo());

									for (PriceListDetails obj : entity
											.getPriceInfo()) {
										if (obj.getVendorID() == vendorComp
												.getIdValue()) {
											ve.setVendorEmailId(obj.getEmail());
											ve.setVendorId(obj.getVendorID());
											ve.setVendorName(obj.getFullName());
											ve.setVendorPhone(obj
													.getCellNumber());
										}
									}
								}

								vendorTableRFQ.getDataprovider().getList()
										.add(ve);
								vendorComp.clear();

							} else {
								ArrayList<String> list = new ArrayList<String>();
								for (String obj : codeList) {
									boolean flag = false;
									for (PriceList lisObj : priceList) {
										if (lisObj.getProdCode().equals(obj)) {
											flag = true;
											break;
										}
									}
									if (!flag) {
										list.add(obj);
									}
								}

								String prodName = "";
								for (String obj : list) {
									prodName = prodName
											+ getProductNameFromCode(obj)
											+ "\n";
								}

								showDialogMessage("Vendor price list does not exist for following products."
										+ "\n" + prodName);
								return;

							}
						} else {
							System.out.println("Six");
							showDialogMessage("Vendor Price List does not exists.");
						}
					}
				});
	}

	private String getProductNameFromCode(String code) {
		List<ProductDetails> list = productTableRFQ.getDataprovider().getList();
		for (ProductDetails obj : list) {
			if (obj.getProductCode().equals(code)) {
				return obj.getProductName();
			}
		}
		return null;
	}

	/**
	 * This method checks whether vendor is already added or not if already
	 * added then returns false else true Date : 28-09-2016 By Anil Release : 30
	 * Sept 2016 Project : PURCHASE MODIFICATION(NBHC)
	 */

	private boolean validVendor() {

		List<ProductDetails> prodList = productTableRFQ.getDataprovider()
				.getList();
		if (prodList.size() == 0) {
			showDialogMessage("Please add product first !");
			return false;
		}

		List<VendorDetails> list = vendorTableRFQ.getDataprovider().getList();
		int vendorId = vendorComp.getIdValue();

		for (VendorDetails obj : list) {
			if (obj.getVendorId() == vendorId) {
				showDialogMessage("Vendor already added !");
				return false;
			}
		}
		return true;
	}

	/**
	 * End
	 */

	/********************************** Initialize ObjectList *********************************************/
	protected void initalizEmployeeNameListBox() {
		MyQuerry querry = new MyQuerry(new Vector<Filter>(), new Employee());
		oblEmployee.MakeLive(querry);
	}

	protected void initalizEmployeeBranchListBox() {
		MyQuerry querry = new MyQuerry(new Vector<Filter>(), new Branch());
		branch.MakeLive(querry);
	}

	/****************************** Validate Function *************************************************************/

	@Override
	public boolean validate() {
		boolean b = super.validate();
		if (b == false)
			return false;

		if (productTableRFQ.getDataprovider().getList().size() == 0) {
			this.showDialogMessage("Please add atleast one product");
			return false;
		}

		if (vendorTableRFQ.getDataprovider().getList().size() == 0) {
			this.showDialogMessage("Please add atleast one vendor");
			return false;
		}

		// if
		// (dbCreationDate.getValue().after(dbExpectedClosureDate.getValue())) {
		// this.showDialogMessage("Expected Closure Date Should Be Greater Than Creation Date.");
		// return false;
		// } else
		if (dbCreationDate.getValue().after(dbExpdeliverydate.getValue())) {
			this.showDialogMessage("Expected Delivery Date Should Be Greater Than Creation Date.");
			return false;
		} else if (dbResponseDate.getValue() != null) {
			if (dbCreationDate.getValue().after(dbResponseDate.getValue())) {
				this.showDialogMessage("Expected Response Date Should Be Greater Than Creation Date.");
				return false;
			} else if (dbResponseDate.getValue().after(
					dbExpdeliverydate.getValue())) {
				this.showDialogMessage("Expected Deliver Date Should Be Greater Than Expected Response Date.");
				return false;
			}
			// else if
			// (dbResponseDate.getValue().after(dbExpectedClosureDate.getValue()))
			// {
			// this.showDialogMessage("Expected Closure Date Should Be Greater Than Expected Response Date.");
			// return false;
			// }
		}
		// else if
		// (dbExpdeliverydate.getValue().after(dbExpectedClosureDate.getValue()))
		// {
		// this.showDialogMessage("Expected Closure Date Should Be Greater Than Expected Delivery Date.");
		// return false;
		// }

		if (!validateProductQuantity()) {
			showDialogMessage("Product Quantity Should Not Be Zero.");
			return false;
		}

		if (ProductTable.isValidWarehouse == false) {
			showDialogMessage("Warehouse is already selected for product, please change warehouse or delete row.");
			return false;
		}

		return true;
	}

	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		productTableRFQ.setEnable(state);
		vendorTableRFQ.setEnable(state);
		ibRequestQuotationID.setEnabled(false);
		tbstatus.setEnabled(false);
		tbcommentForStatusChange.setEnabled(false);
		dbCreationDate.setEnabled(false);

		if (!purchaseRequisiteNo.getValue().equals("")
				&& !purchaseRequisiteNo.getValue().equals("0")) {
			productInfoComposite.setEnable(false);
			bAddproducts.setEnabled(false);
		}
	}

	@Override
	public TextBox getstatustextbox() {
		return this.tbstatus;
	}

	@Override
	public void clear() {
		super.clear();
	}

	@Override
	public void onChange(ChangeEvent event) {
		if (event.getSource().equals(purchaseRequisiteNo)) {
			checkPrNumber();
		}

		/******************************************* Type Drop Down Logic **************************************/

		if (event.getSource().equals(oblquotationCategory)) {
			if (oblquotationCategory.getSelectedIndex() != 0) {
				ConfigCategory cat = oblquotationCategory.getSelectedItem();
				if (cat != null) {
					AppUtility.makeLiveTypeDropDown(oblquotationType,
							cat.getCategoryName(), cat.getCategoryCode(),
							cat.getInternalType());
				}
			}
		}
	}

	private void checkPrNumber() {
		if (purchaseRequisiteNo.getValue() != null) {
			int i = Integer.parseInt(purchaseRequisiteNo.getValue());
			MyQuerry querry = new MyQuerry();
			Filter filter = new Filter();
			filter.setQuerryString("count");
			filter.setIntValue(i);
			querry.getFilters().add(filter);
			querry.setQuerryObject(new PurchaseRequisition());

			async.getSearchResult(querry,
					new AsyncCallback<ArrayList<SuperModel>>() {
						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							if (result.size() != 0) {
								PurchaseRequisition pr = (PurchaseRequisition) result
										.get(0);
								if (pr.getStatus().equals("Approved")) {

									ArrayList<ProductDetails> prProductList = PurchaseRequitionPresenter
											.getUniqueProductDetailsList(pr
													.getPrproduct());
									productTableRFQ.getDataprovider().setList(
											prProductList);

									// productTableRFQ.getDataprovider().setList(pr.getPrproduct());

									prDate.setValue(pr.getCreationDate());
									ibRefOrderNO.setValue(pr.getRefOrderNO());
									if (ibRefOrderNO.getValue() != null) {
										ibRefOrderNO.setEnabled(false);
									}
									if (pr.getProject() != null) {
										project.setValue(pr.getProject());
										project.setEnabled(false);
									}
									branch.setValue(pr.getBranch());
									branch.setEnabled(false);
									oblEmployee.setValue(pr.getEmployee());
									if (oblEmployee.getValue() != null) {
										oblEmployee.setEnabled(false);
									}
									tbApporverName.setValue(pr
											.getApproverName());
									if (tbApporverName.getValue() != null) {
										tbApporverName.setEnabled(false);
									}

									/**
									 * Copying some more info of PR to RFQ Date
									 * : 09-09-2016 By Anil Release : 31 Aug
									 * 2016 Project : PURCHASE ERROR SOLVING
									 * (NBHC)
									 */

									tbName.setValue(pr.getPrTitle());
									dbResponseDate.setValue(pr
											.getResponseDate());
									dbExpdeliverydate.setValue(pr
											.getExpectedDeliveryDate());
									// dbExpectedClosureDate.setValue(pr.getExpectedClosureDate());
									header.setValue(pr.getHeader());
									footer.setValue(pr.getFooter());

									// ////
									olbAssignedTo1.setValue(pr.getAssignTo1());
									olbAssignedTo2.setValue(pr.getAssignTo2());

									// ///////////////
									List<String> prodCodeList = new ArrayList<String>();
									HashSet<String> hashList = new HashSet<String>();
									for (ProductDetails prod : pr
											.getPrproduct()) {
										hashList.add(prod.getProductCode());
									}
									prodCodeList.addAll(hashList);
									loadVendorProductPriceList(prodCodeList);

									// /////////////

									productInfoComposite.setEnable(false);
									bAddproducts.setEnabled(false);

									/**
									 * END
									 */

								} else {
									showDialogMessage("PR No. is Not Approved");
									setToNewState();
									productTableRFQ.connectToLocal();
									vendorTableRFQ.connectToLocal();
								}
							} else {
								showDialogMessage("No PR No. found");
								setToNewState();
								productTableRFQ.connectToLocal();
								vendorTableRFQ.connectToLocal();
							}
						}

						@Override
						public void onFailure(Throwable caught) {

						}
					});
		}
	}

	/********** validate product for duplicate entry ***************/

	public boolean validateProduct() {

		if (dbExpdeliverydate.getValue() == null) {
			this.showDialogMessage("Please select delivery date !");
			return false;
		}

		if (ProductTable.isValidWarehouse == false) {
			showDialogMessage("Warehouse is already selected for product, please change warehouse or delete row.");
			return false;
		}

		int prodid = productInfoComposite.getIdValue();
		List<ProductDetails> list = productTableRFQ.getDataprovider().getList();
		for (ProductDetails temp : list) {
			if (prodid == temp.getProductID()
					&& temp.getWarehouseName().equals("")) {
				this.showDialogMessage("Can not add Duplicate product");
				return false;
			}
		}
		return true;

		// String name = productInfoComposite.getProdName().getValue();
		// List<ProductDetails> list =
		// productTableRFQ.getDataprovider().getList();
		// for (ProductDetails temp : list) {
		// if (name.equals(temp.getProductName())) {
		// this.showDialogMessage("Can not add Duplicate product");
		// return false;
		// }
		// }
		// return true;
	}

	@Override
	public void onRowCountChange(RowCountChangeEvent event) {
		if (productTableRFQ.getDataprovider().getList().size() == 0) {
			vendorTableRFQ.connectToLocal();
		}
	}

	@Override
	public void setCount(int count) {
		ibRequestQuotationID.setValue(count + "");
	}

	@Override
	public void setToViewState() {
		super.setToViewState();
		setMenuAsPerStatus();

		SuperModel model = new RequsestForQuotation();
		model = rfqobj;
		AppUtility.addDocumentToHistoryTable(AppConstants.PURCHASEMODULE,
				AppConstants.RFQ, rfqobj.getCount(), null, null, null, false,
				model, null);
		
		String mainScreenLabel="RFQ";
		if(rfqobj!=null&&rfqobj.getCount()!=0){
			
			
			mainScreenLabel=rfqobj.getCount()+" "+"/"+" "+rfqobj.getStatus()+" "+"/"+" "+AppUtility.parseDate(rfqobj.getCreationDate());
		}
		fgroupingRFQDetails.getHeaderLabel().setText(mainScreenLabel);

	}

	@Override
	public void setToEditState() {
		super.setToEditState();
		if (tbstatus.getValue().equals(RequsestForQuotation.REJECTED)) {
			tbstatus.setValue(RequsestForQuotation.CREATED);
		}
		setMenuAsPerStatus();
		this.processLevelBar.setVisibleFalse(false);
		
		String mainScreenLabel="RFQ";
		if(rfqobj!=null&&rfqobj.getCount()!=0){
			
			
			mainScreenLabel=rfqobj.getCount()+" "+"/"+" "+rfqobj.getStatus()+" "+"/"+" "+AppUtility.parseDate(rfqobj.getCreationDate());
		}
		fgroupingRFQDetails.getHeaderLabel().setText(mainScreenLabel);
	}

	@Override
	public void setToNewState() {
		super.setToNewState();
		tbstatus.setValue(RequsestForQuotation.CREATED);
		prDate.setEnabled(false);
		Date data = new Date();
		dbCreationDate.setValue(data);
	}

	public void setMenuAsPerStatus() {
		this.setAppHeaderBarAsPerStatus();
		this.toggleProcessLevelMenu();
	}

	public void setAppHeaderBarAsPerStatus() {
		RequsestForQuotation entity = (RequsestForQuotation) presenter
				.getModel();
		String status = entity.getStatus();

		if (status.equals(RequsestForQuotation.CLOSED)
				|| status.equals(RequsestForQuotation.CANCELLED)) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Discard") || text.contains("Search")
						|| text.equals(AppConstants.NAVIGATION)) {
					menus[k].setVisible(true);
					System.out
							.println("Value of text is " + menus[k].getText());
				} else {
					menus[k].setVisible(false);
				}
			}
		}
		if (status.equals(RequsestForQuotation.APPROVED)) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Discard") || text.equals("Search")
						|| text.endsWith("Email")
						|| text.equals(AppConstants.NAVIGATION)) {
					menus[k].setVisible(true);
				} else {
					menus[k].setVisible(false);
				}
			}
		}
	}

	public boolean validateProductQuantity() {
		List<ProductDetails> list = productTableRFQ.getDataprovider().getList();
		for (ProductDetails temp : list) {
			if (temp.getProductQuantity() == 0) {
				return false;
			}
		}
		return true;
	}

	public void setEnableFalse() {
		int row = this.fields.length;
		for (int k = 0; k < row; k++) {
			int column = fields[k].length;
			for (int i = 0; i < column; i++) {
				Widget widg = fields[k][i].getWidget();
				if (widg instanceof TextBox) {
					TextBox sb = (TextBox) widg;
					if (!sb.getText().equals(""))
						sb.setEnabled(false);
				}
				if (widg instanceof IntegerBox) {
					IntegerBox sb = (IntegerBox) widg;
					if (sb.getValue() != 0)
						sb.setEnabled(false);
				}
				if (widg instanceof DateBox) {
					DateBox sb = (DateBox) widg;
					if (sb.getValue() != null)
						sb.setEnabled(false);
				}
				if (widg instanceof ListBox) {
					ListBox sb = (ListBox) widg;
					if (sb.getSelectedIndex() != 0)
						sb.setEnabled(false);
				}
			}
		}
	}

	/******************
	 * Setting Product Table RFQ and Vendor List From
	 * ProductInfoComposite,PriceList
	 *****************/

	public void prodctType(String pcode) {
		System.out.println("Started");
		final GenricServiceAsync genasync = GWT.create(GenricService.class);
		final MyQuerry querry = new MyQuerry();
		Company c = new Company();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;
		filter = new Filter();
		filter.setQuerryString("productCode");
		filter.setStringValue(pcode.trim());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new SuperProduct());
		showWaitSymbol();
		Timer timer = new Timer() {
			@Override
			public void run() {
				genasync.getSearchResult(querry,
						new AsyncCallback<ArrayList<SuperModel>>() {
							@Override
							public void onFailure(Throwable caught) {
								showDialogMessage("An Unexpected error occurred!");
							}

							@Override
							public void onSuccess(ArrayList<SuperModel> result) {
								for (SuperModel model : result) {
									SuperProduct superProdEntity = (SuperProduct) model;
									ProductDetails lis = AppUtility
											.ReactOnAddPurchaseReqProductComposite(superProdEntity);
									lis.setProdDate(dbExpdeliverydate
											.getValue());
									checkAndAddVendorToVendorTable(lis);
								}
							}
						});
				hideWaitSymbol();
			}
		};
		timer.schedule(3000);
	}

	/**
	 * This method takes product details which we are adding in product table as
	 * input and checks whether vendor price list for that product is exist or
	 * not if exist then checks for common vendor and add product and vendor to
	 * their respective table.
	 * 
	 * Date : 10-09-2016 By Anil Release :31 Aug 2016 Project:PURCHASE ERROR
	 * SOLVING (NBHC) I/P:Product of type ProductDetails
	 */

	public void checkAndAddVendorToVendorTable(final ProductDetails prodDetails) {

		System.out.println("One");

		MyQuerry querry = new MyQuerry();
		Filter filter = new Filter();
		filter.setQuerryString("prodCode");
		filter.setStringValue(productInfoComposite.getProdCode().getValue()
				.trim());
		querry.getFilters().add(filter);
		querry.setQuerryObject(new PriceList());
		System.out.println("Two");

		async.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onFailure(Throwable caught) {

					}

					@Override
					public void onSuccess(ArrayList<SuperModel> vresult) {
						System.out.println("VEN PRICE LIS SIZE : "
								+ vresult.size());
						if (vresult.size() != 0) {
							for (SuperModel smodel : vresult) {
								boolean priceListFlag = true;
								System.out.println("Three");
								PriceList pentity = (PriceList) smodel;
								if (isCommonVendor(pentity.getPriceInfo())) {
									System.out.println("Four");
									productTableRFQ.getDataprovider().getList()
											.add(prodDetails);

									// //////////////////

									if (vendorRateList.size() == 0) {
										vendorRateList.addAll(pentity
												.getPriceInfo());
									} else {
										for (PriceListDetails obj : pentity
												.getPriceInfo()) {
											priceListFlag = checkPriceListAlreadExist(obj);
											if (!priceListFlag) {
												vendorRateList.add(obj);
											}
										}
									}

									// ///////////////

								} else {
									System.out.println("Five");
									showDialogMessage("No common vendor for this product !");
									return;
								}
								addUniqueVendorsToTable(pentity.getPriceInfo());
							}
						} else {
							System.out.println("Six");
							showDialogMessage("Vendor Price List does not exists for product");
						}
					}

				});
	}

	/**
	 * This method checks whether price list already exist in global list or
	 * not. Date : 28-09-2016 By Anil Release :30 Sept 2016 Project : PURCHASE
	 * MODIFICATION(NBHC)
	 */
	private boolean checkPriceListAlreadExist(PriceListDetails obj) {
		for (PriceListDetails lisObj : vendorRateList) {
			if (obj.getProductCode().equals(lisObj.getProductCode())
					&& obj.getVendorID() == lisObj.getVendorID()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * This method checks whether vendors are common or not for added product if
	 * not then it will not allow you add product Date 10-09-2016 By Anil
	 * Release : 31 Aug 2016 Project: PURCHASE ERROR SOLVING(NBHC) I/P:Current
	 * added products vendor price list and checks with already added vendor
	 * list O/P: return true if at least one vendor common else false
	 */

	private boolean isCommonVendor(List<PriceListDetails> venRateLis) {
		System.out.println("CHECKING COMMON VENDOR ");
		System.out.println("VEN TBL SIZE "
				+ vendorTableRFQ.getDataprovider().getList().size());
		if (vendorTableRFQ.getDataprovider().getList().size() == 0) {
			return true;
		}
		for (VendorDetails gblVenLis : vendorTableRFQ.getDataprovider()
				.getList()) {
			for (PriceListDetails currVenLis : venRateLis) {
				System.out.println("VENDOR ID " + gblVenLis.getVendorId()
						+ "    --   " + currVenLis.getVendorID());
				if (gblVenLis.getVendorId() == currVenLis.getVendorID()) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * This method adds only common vendor to vendor table Date 10-09-2016 By
	 * Anil Release : 31 Aug 2016 Project: PURCHASE ERROR SOLVING(NBHC)
	 * I/P:Current added products vendor list and checks with already added
	 * vendor list,maintain only common vendors list else all removed
	 */

	private void addUniqueVendorsToTable(List<PriceListDetails> venRateLis) {
		System.out.println("ADDING UNIQUE VENDOR");
		if (vendorTableRFQ.getDataprovider().getList().size() == 0) {
			System.out.println("ADDING FOR 1ST TIME");
			ArrayList<VendorDetails> venList = getVendorList(venRateLis);
			vendorTableRFQ.getDataprovider().getList().addAll(venList);
		} else {
			System.out.println("ADDING ");
			ArrayList<VendorDetails> commonVenList = getCommonVendorList(
					vendorTableRFQ.getDataprovider().getList(), venRateLis);
			vendorTableRFQ.getDataprovider().getList().clear();
			vendorTableRFQ.getDataprovider().getList().addAll(commonVenList);
		}

		System.out.println("FINAL VENDOR LIST SIZE "
				+ vendorTableRFQ.getDataprovider().getList().size());

	}

	/**
	 * This method returns common vendors list by comparing from existing and
	 * currently to be added vendor list. Date 10-09-2016 By Anil Release : 31
	 * Aug 2016 Project: PURCHASE ERROR SOLVING(NBHC) I/P:existing vendor list
	 * and currently added vendor price list O/P: common vendor list
	 */

	private ArrayList<VendorDetails> getCommonVendorList(
			List<VendorDetails> vendorList, List<PriceListDetails> venRateLis) {
		System.out.println("CHECKING ADDING COMMON VENDOR ONLY ");
		ArrayList<VendorDetails> vendList = new ArrayList<VendorDetails>();
		for (VendorDetails venDet : vendorList) {
			for (PriceListDetails venRLis : venRateLis) {
				if (venDet.getVendorId() == venRLis.getVendorID()) {
					vendList.add(venDet);
				}
			}
		}
		System.out.println("VEN LIS SIZE " + vendList.size());
		return vendList;
	}

	/**
	 * This method convert the vendor list from type PriceListDetails to type
	 * VendorDetails Date 10-09-2016 By Anil Release : 31 Aug 2016 Project:
	 * PURCHASE ERROR SOLVING(NBHC) I/P:vendor list of type PriceListDetails
	 * O/P: vendor list of type VendorDetails
	 */

	public ArrayList<VendorDetails> getVendorList(
			List<PriceListDetails> venRateLis) {
		System.out.println("CONVERTING LIST.....");
		ArrayList<VendorDetails> vendorList = new ArrayList<VendorDetails>();

		for (PriceListDetails temp : venRateLis) {
			VendorDetails ve = new VendorDetails();
			ve.setVendorEmailId(temp.getEmail());
			ve.setVendorId(temp.getVendorID());
			ve.setVendorName(temp.getFullName());
			ve.setVendorPhone(temp.getCellNumber());
			vendorList.add(ve);
		}
		return vendorList;
	}

	/**
	 * This method load global vendor list when we are viewing the records by
	 * searching. Date: 10-09-2016 Release : 31 Aug 2016 Project: PURCHASE ERROR
	 * SOLVING(NBHC) I/P: List of product code
	 * 
	 */
	private void loadVendorProductPriceList(List<String> prodIdList) {

		for (String obj : prodIdList) {
			System.out.println("CODE : " + obj);
		}
		MyQuerry querry = new MyQuerry();
		Filter filter = null;
		Vector<Filter> temp = new Vector<Filter>();

		filter = new Filter();
		filter.setList(prodIdList);
		filter.setQuerryString("prodCode IN");
		temp.add(filter);

		filter = new Filter();
		filter.setLongValue(UserConfiguration.getCompanyId());
		filter.setQuerryString("companyId");
		temp.add(filter);

		querry.setFilters(temp);
		querry.setQuerryObject(new PriceList());

		async.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						System.out.println("LOAD RESULT " + result.size());
						vendorRateList = new ArrayList<PriceListDetails>();
						for (SuperModel model : result) {
							PriceList entity = (PriceList) model;
							vendorRateList.addAll(entity.getPriceInfo());
						}
						System.out.println("GLOBAL VEN LIST SIZE IN LOAD "
								+ vendorRateList.size());
					}
				});

	}

	/****************************************** Getter and Setter For Form Fields *************************************/

	// public List<PriceListDetails> getAdded() {
	// return added;
	// }
	//
	// public void setAdded(List<PriceListDetails> added) {
	// this.added = added;
	// }

	// public List<String> getName() {
	// return name;
	// }
	//
	// public void setName(List<String> name) {
	// this.name = name;
	// }

	public TextBox getPurchaseRequisiteNo() {
		return purchaseRequisiteNo;
	}

	public void setPurchaseRequisiteNo(TextBox purchaseRequisiteNo) {
		this.purchaseRequisiteNo = purchaseRequisiteNo;
	}

	public DateBox getPrDate() {
		return prDate;
	}

	public void setPrDate(DateBox prDate) {
		this.prDate = prDate;
	}

	public TextBox getIbRefOrderNO() {
		return ibRefOrderNO;
	}

	public void setIbRefOrderNO(TextBox ibRefOrderNO) {
		this.ibRefOrderNO = ibRefOrderNO;
	}

	public TextBox getIbRequestQuotationID() {
		return ibRequestQuotationID;
	}

	public void setIbRequestQuotationID(TextBox ibRequestQuotationID) {
		this.ibRequestQuotationID = ibRequestQuotationID;
	}

	public TextBox getTbName() {
		return tbName;
	}

	public void setTbName(TextBox tbName) {
		this.tbName = tbName;
	}

	public DateBox getDbCreationDate() {
		return dbCreationDate;
	}

	public void setDbCreationDate(DateBox dbCreationDate) {
		this.dbCreationDate = dbCreationDate;
	}

	public DateBox getDbResponseDate() {
		return dbResponseDate;
	}

	public void setDbResponseDate(DateBox dbResponseDate) {
		this.dbResponseDate = dbResponseDate;
	}

	public DateBox getDbExpdeliverydate() {
		return dbExpdeliverydate;
	}

	public void setDbExpdeliverydate(DateBox dbExpdeliverydate) {
		this.dbExpdeliverydate = dbExpdeliverydate;
	}

	// public DateBox getDbExpectedClosureDate() {
	// return dbExpectedClosureDate;
	// }
	//
	// public void setDbExpectedClosureDate(DateBox dbExpectedClosureDate) {
	// this.dbExpectedClosureDate = dbExpectedClosureDate;
	// }

	public ObjectListBox<ConfigCategory> getOblquotationCategory() {
		return oblquotationCategory;
	}

	public void setOblquotationCategory(
			ObjectListBox<ConfigCategory> oblquotationCategory) {
		this.oblquotationCategory = oblquotationCategory;
	}

	public ObjectListBox<Type> getOblquotationType() {
		return oblquotationType;
	}

	public void setOblquotationType(ObjectListBox<Type> oblquotationType) {
		this.oblquotationType = oblquotationType;
	}

	public Button getbAddproducts() {
		return bAddproducts;
	}

	public void setbAddproducts(Button bAddproducts) {
		this.bAddproducts = bAddproducts;
	}

	public ProductTable getProductTableRFQ() {
		return productTableRFQ;
	}

	public void setProductTableRFQ(ProductTable productTableRFQ) {
		this.productTableRFQ = productTableRFQ;
	}

	public VendorTable getVendorTableRFQ() {
		return vendorTableRFQ;
	}

	public void setVendorTableRFQ(VendorTable vendorTableRFQ) {
		this.vendorTableRFQ = vendorTableRFQ;
	}

	public ObjectListBox<Employee> getOblEmployee() {
		return oblEmployee;
	}

	public void setOblEmployee(ObjectListBox<Employee> oblEmployee) {
		this.oblEmployee = oblEmployee;
	}

	public ObjectListBox<Employee> getTbApporverName() {
		return tbApporverName;
	}

	public void setTbApporverName(ObjectListBox<Employee> tbApporverName) {
		this.tbApporverName = tbApporverName;
	}

	public ObjectListBox<Branch> getBranch() {
		return branch;
	}

	public void setBranch(ObjectListBox<Branch> branch) {
		this.branch = branch;
	}

	public ObjectListBox<HrProject> getProject() {
		return project;
	}

	public void setProject(ObjectListBox<HrProject> project) {
		this.project = project;
	}

	// public PersonInfoComposite getVic() {
	// return vic;
	// }
	//
	// public void setVic(PersonInfoComposite vic) {
	// this.vic = vic;
	// }

	public TextBox getTbstatus() {
		return tbstatus;
	}

	public void setTbstatus(TextBox tbstatus) {
		this.tbstatus = tbstatus;
	}

	public TextBox getTbcommentForStatusChange() {
		return tbcommentForStatusChange;
	}

	public void setTbcommentForStatusChange(TextBox tbcommentForStatusChange) {
		this.tbcommentForStatusChange = tbcommentForStatusChange;
	}

	public UploadComposite getUpload() {
		return upload;
	}

	public void setUpload(UploadComposite upload) {
		this.upload = upload;
	}

	public TextArea getHeader() {
		return header;
	}

	public void setHeader(TextArea header) {
		this.header = header;
	}

	public TextArea getFooter() {
		return footer;
	}

	public void setFooter(TextArea footer) {
		this.footer = footer;
	}
	
	@Override
	public void refreshTableData() {
		// TODO Auto-generated method stub
		super.refreshTableData();
		vendorTableRFQ.getTable().redraw();
		productTableRFQ.getTable().redraw();
	}

}
