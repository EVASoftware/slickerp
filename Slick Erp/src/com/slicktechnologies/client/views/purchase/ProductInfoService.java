package com.slicktechnologies.client.views.purchase;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.slicktechnologies.shared.common.helperlayer.ProductInfo;

@RemoteServiceRelativePath("../productinfoservice")
public interface ProductInfoService extends RemoteService{

	public ArrayList<ProductInfo> LoadVinfo(long companyId,String typeOfEntity);
	public ArrayList<ProductInfo> LoadProdinfo(long companyId,int latestCount,String typeOfEntity);
	public ArrayList<ProductInfo> LoadSuperProd(int itemLatestId,int serviceLatestId,long companyId);
//	public ArrayList<ProductInfo> LoadVinfo(MyQuerry querry);
	
	public ArrayList<ProductInfo> LoadAssetProd(long companyId);
	
}
