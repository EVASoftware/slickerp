package com.slicktechnologies.client.views.purchase.purchaselist;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.view.client.NoSelectionModel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreen;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreenPresenter;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.purchase.purchaserequisition.PurchaseRequisitionForm;
import com.slicktechnologies.client.views.purchase.purchaserequisition.PurchaseRequisitionPresenterSearch;
import com.slicktechnologies.client.views.purchase.purchaserequisition.PurchaseRequitionPresenter;
import com.slicktechnologies.client.views.purchase.vendorPriceList.VendorPriceListForm;
import com.slicktechnologies.client.views.purchase.vendorPriceList.VendorPriceListPresenter;
import com.slicktechnologies.client.views.purchase.vendorproductlist.VendorPriceListTableProxy;
import com.slicktechnologies.client.views.purchase.vendorproductlist.VendorProductListForm;
import com.slicktechnologies.client.views.purchase.vendorproductlist.VendorProductListPresenter;
import com.slicktechnologies.client.views.purchase.vendorproductlist.VendorProductSearchPopUp;
import com.slicktechnologies.shared.common.businessprocesslayer.PriceList;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseRequisition;
import com.slicktechnologies.shared.common.businessprocesslayer.VendorPriceListDetails;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;

public class PurchaseRequisitionListPresenter extends TableScreenPresenter<PurchaseRequisition> {

	TableScreen<PurchaseRequisition>form;
	public PurchaseRequisitionListPresenter(TableScreen<PurchaseRequisition> view,PurchaseRequisition model) {
		super(view, model);
		form=view;
//		view.retriveTable(getPRQuery());//Ashwini Patil Date:23-08-2024 to stop auto loading
//		setTableSelectionOnPRList();
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.PURCHASEREQUISITIONLIST,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}
	
	public static void initialize()
	{
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Purchase Requisition List",Screen.PURCHASEREQUISITIONLIST);
		PurchaseRequisitionTable gentable=new PurchaseRequisitionTable();
		PurchaseRequisitionListForm form=new PurchaseRequisitionListForm(gentable);
	
		
		PurchaseRequisitionPresenterSearch.staticSuperTable=gentable;
		PurchaseRequisitionPresenterSearch searchpopup=new PurchaseRequisitionPresenterSearch(false);
		form.setSearchpopupscreen(searchpopup);
		
		
		PurchaseRequisitionListPresenter presenter=new PurchaseRequisitionListPresenter(form, new PurchaseRequisition());
		form.setToViewState();
//		form.setToNewState();
		AppMemory.getAppMemory().stickPnel(form);

		  
	}
	
	

	
	
	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
//		super.reactToProcessBarEvents(e);
		InlineLabel label = (InlineLabel) e.getSource();
		String text = label.getText().trim();
		
		if (text.equals(AppConstants.CREATESINGLEPR)){
			System.out.println("PR BUTTON CLICKED");
			boolean validResult=validateAllSelectedPrDocument();
			if(validResult){
				createPR();
			}
		}
	}

	private void createPR() {
		List<PurchaseRequisition> prList=getAllSelectedRecords();
		
		Collections.sort(prList, new Comparator<PurchaseRequisition>() {
			@Override
			public int compare(PurchaseRequisition o1,PurchaseRequisition o2) {
				Integer count1=o1.getCount();
				Integer count2=o2.getCount();
				return count1.compareTo(count2);
			}
		});
		
		ArrayList<ProductDetails> combinePrProductList=new ArrayList<ProductDetails>();
		
		Date maxExpDelDate=null;
		for(PurchaseRequisition obj:prList){
			for(ProductDetails prodObj:obj.getPrproduct()){
				prodObj.setProductQuantity(prodObj.getAcceptedQty());
				
				combinePrProductList.add(prodObj);
				
				Date temp=prodObj.getProdDate();
				if(maxExpDelDate==null){
					maxExpDelDate=temp;
				}else{
					if(temp.after(maxExpDelDate)){
						maxExpDelDate=temp;
					}
				}
			}
		}
//		String purEngg=prList.get(0).getEmployee();
		String approver=prList.get(0).getApproverName();
		final PurchaseRequisitionForm form=PurchaseRequitionPresenter.initalize();
		form.setToNewState();
		form.setPrHeadAsDefualtPurchaseEngineer();
		final PurchaseRequisition entity=new PurchaseRequisition();
		entity.setPrproduct(combinePrProductList);
		entity.setExpectedDeliveryDate(maxExpDelDate);
//		entity.setEmployee(purEngg);
		entity.setApproverName(approver);
		Timer t = new Timer() {
			@Override
			public void run() {
				form.productTablePR.setEnable(false);
				form.bAddproducts.setEnabled(false);
				form.productInfoComposite.setEnable(false);
				
				form.updateView(entity);
			}
		};
		t.schedule(5000);
	}

	public MyQuerry getPRQuery()
	{
		MyQuerry querry=new MyQuerry();
		Vector<Filter> temp=new Vector<Filter>();
		Filter filter=null;
		
//		List<String> statusList=new ArrayList<String>();
//		statusList.add(PurchaseRequisition.CREATED);
//		statusList.add(PurchaseRequisition.REQUESTED);
//		
//		
//		filter=new Filter();
//		filter.setList(statusList);
//		filter.setQuerryString("purchasedetails.status IN");
//		temp.add(filter);
		
		filter=new Filter();
		filter.setStringValue(PurchaseRequisition.APPROVED);
		filter.setQuerryString("purchasedetails.status");
		temp.add(filter);
		
		if(!LoginPresenter.loggedInUser.equals("")){
			filter=new Filter();
			filter.setStringValue(LoginPresenter.loggedInUser);
			filter.setQuerryString("purchasedetails.approverName");
			temp.add(filter);
		}
		
		filter=new Filter();
		filter.setLongValue(UserConfiguration.getCompanyId());
		filter.setQuerryString("companyId");
		temp.add(filter);
		
		querry.setFilters(temp);
		querry.setQuerryObject(new PurchaseRequisition());
		
		return querry;
		
		
		
//		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new PurchaseRequisition());
//		return quer;
		
	}
	
	
	public void setTableSelectionOnPRList()
	{
		 final NoSelectionModel<PurchaseRequisition> selectionModelMyObj = new NoSelectionModel<PurchaseRequisition>();
		 SelectionChangeEvent.Handler  tableHandler = new SelectionChangeEvent.Handler()
		 {
			@Override
			public void onSelectionChange(SelectionChangeEvent event) {
				final PurchaseRequisition entity=selectionModelMyObj.getLastSelectedObject();
	        	AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
	     
//	        	AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Purchase/Vendor Price List",Screen.VENDORPRICELIST);
//	        	final VendorPriceListForm form = VendorPriceListPresenter.initalize();
//	        	AppMemory.getAppMemory().stickPnel(form);
	        	 
	        	
					
			}
		 };
		 selectionModelMyObj.addSelectionChangeHandler( tableHandler );
	     form.getSuperTable().getTable().setSelectionModel(selectionModelMyObj);
	}
	
	
	@Override
	protected void makeNewModel() {
		model=new PurchaseRequisition();
	}
	
	@Override
	public void reactOnDownload()
	{
		CsvServiceAsync async=GWT.create(CsvService.class);
		ArrayList<PurchaseRequisition> PRArray=new ArrayList<PurchaseRequisition>();
		List<PurchaseRequisition> list=form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
		PRArray.addAll(list); 
		async.setPR(PRArray, new AsyncCallback<Void>() {
		@Override
		public void onFailure(Throwable caught) {
			System.out.println("RPC call Failed" + caught);
		}

		@Override
		public void onSuccess(Void result) {
			String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
			final String url = gwt + "csvservlet" + "?type=" + 47;
			Window.open(url, "test", "enabled");
		}
	});

	}

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}
	
	
	
	 private List<PurchaseRequisition> getAllSelectedRecords()
	 {
		 List<PurchaseRequisition> lisbillingtable=form.getSuperTable().getDataprovider().getList();
		 ArrayList<PurchaseRequisition> selectedlist=new ArrayList<PurchaseRequisition>();
		 for(PurchaseRequisition doc:lisbillingtable)
		 {
			 if(doc.getRecordSelect()==true)
				 selectedlist.add(doc);
		 }
		 return selectedlist;
	 }
	 
	 private List<PurchaseRequisition> getAllUnselectedRecords()
	 {
		 List<PurchaseRequisition> lisbillingtable=form.getSuperTable().getDataprovider().getList();
		 ArrayList<PurchaseRequisition> selectedlist=new ArrayList<PurchaseRequisition>();
		 for(PurchaseRequisition doc:lisbillingtable)
		 {
			 if(doc.getRecordSelect()==false)
				 selectedlist.add(doc);
		 }
		 return selectedlist;
	 }
	
	
	 private boolean validateAllSelectedPrDocument()
	 {
		String msg="";
		boolean temp=true;
		List<PurchaseRequisition> prDocList= getAllSelectedRecords();
		if(prDocList.size()==0)
		{
			form.showDialogMessage("No PR Document selected. Please select atleast one PR Document!");
			return false;
		}
		
		boolean valApproverName=validateApproverName(prDocList);
		boolean valStatus=validateStatus(prDocList);
		
		
		if(valApproverName==false)
		{
			msg=msg+"Approver Name of all selected Billing Document should be same"+"\n";
			temp=false;
		}
		
		if(valStatus==false)
		{
			msg=msg+"Status of all selected Billing Document should be approved"+"\n";
			temp=false;
		}
		if(temp==false)
		{
			form.showDialogMessage(msg);
			return false;
		}
			
		return true;
	 }

	private boolean validateStatus(List<PurchaseRequisition> prDocList) {
//		int counter=0;
//		for(PurchaseRequisition obj:prDocList){
//			if(obj.getStatus().equals(PurchaseRequisition.CREATED)||obj.getStatus().equals(PurchaseRequisition.REQUESTED)){
//				counter++;
//			}else{
//				return false;
//			}
//		}
//		
//		if(prDocList.size()==counter){
//			return true;
//		}
//		return false;
		
		for(PurchaseRequisition obj:prDocList){
			if(!obj.getStatus().equals(PurchaseRequisition.APPROVED)){
				return false;
			}
		}
		return true;
		
	}

	private boolean validateApproverName(List<PurchaseRequisition> prDocList) {
		String approverName=prDocList.get(0).getApproverName().trim();
		for(PurchaseRequisition obj:prDocList){
			if(!approverName.equals(obj.getApproverName())){
				return false;
			}
		}
		return true;
	}
	
	
	
	

}
