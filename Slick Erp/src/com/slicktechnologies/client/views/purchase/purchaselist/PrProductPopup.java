package com.slicktechnologies.client.views.purchase.purchaselist;

import com.google.gwt.dom.client.Style.FontStyle;
import com.google.gwt.dom.client.Style.FontWeight;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;

public class PrProductPopup extends AbsolutePanel{
	PrProductTable prTable;
	Button btnOk,btnCancel;
	
	public void initializeWidget(){
		prTable=new PrProductTable();
		prTable.getTable().getElement().getStyle().setWidth(1050, Unit.PX);
		btnOk=new Button("Ok");
		btnOk.getElement().getStyle().setWidth(80, Unit.PX);
		btnCancel=new Button("Cancel");
		btnCancel.getElement().getStyle().setWidth(80, Unit.PX);
	}
	
	public PrProductPopup() {
		initializeWidget();
		InlineLabel label=new InlineLabel("Products");
		label.getElement().getStyle().setFontSize(20, Unit.PX);
		label.getElement().getStyle().setFontStyle(FontStyle.NORMAL);
		label.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		
		add(label,485,10);
		
		add(prTable.getTable(),10,40);
		
		add(btnOk, 880, 310);
		add(btnCancel,980,310);
		
		setSize("1070px","350px");
		this.getElement().setId("login-form");
	}
	
	
	public void clear(){
		prTable.connectToLocal();
		prTable.isValidWarehouse=true;
	}

	public PrProductTable getPrTable() {
		return prTable;
	}

	public void setPrTable(PrProductTable prTable) {
		this.prTable = prTable;
	}

	public Button getBtnOk() {
		return btnOk;
	}

	public void setBtnOk(Button btnOk) {
		this.btnOk = btnOk;
	}

	public Button getBtnCancel() {
		return btnCancel;
	}

	public void setBtnCancel(Button btnCancel) {
		this.btnCancel = btnCancel;
	}
	
	
	
	
	

}
