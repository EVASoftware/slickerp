package com.slicktechnologies.client.views.purchase.purchaselist;

import java.util.Comparator;
import java.util.List;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.code.p.gwtchismes.client.GWTCGlassPanel;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.ValueUpdater;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.Header;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.PopupPanel;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseRequisition;

public class PurchaseRequisitionTable extends SuperTable<PurchaseRequisition> implements ClickHandler {

	TextColumn<PurchaseRequisition> getPrCountColumn;
	TextColumn<PurchaseRequisition> getPrTitleColumn;
	TextColumn<PurchaseRequisition> getPrDateColumn;
	TextColumn<PurchaseRequisition> getPrExpDelDateColumn;
	TextColumn<PurchaseRequisition> getPrPurEnggColumn;
	TextColumn<PurchaseRequisition> getPrApproverColumn;
	TextColumn<PurchaseRequisition> getPrBranchColumn;
	TextColumn<PurchaseRequisition> getPrStatusColumn;
	
	TextColumn<PurchaseRequisition> getPrRemarkColumn;
	
	Column<PurchaseRequisition, Boolean> checkColumn;
	Header<Boolean> selectAllHeader;
	
	Column<PurchaseRequisition, String> getViewBtnColumn;
	
	PrProductPopup productPopup=new PrProductPopup();
	PopupPanel panel=new PopupPanel(true);
	
	protected GWTCGlassPanel glassPanel;
	
	int rowIndex;
	GenricServiceAsync service=GWT.create(GenricService.class);
	
	public PurchaseRequisitionTable() {
		productPopup.getBtnOk().addClickHandler(this);
		productPopup.getBtnCancel().addClickHandler(this);
	}
	
	@Override
	public void createTable() {
		addAnotherCheckBoxCell();
		getPrCountColumn();
		getPrTitleColumn();
		getPrDateColumn();
		getPrExpDelDateColumn();
		getPrPurEnggColumn();
		getPrApproverColumn();
		getPrBranchColumn();
		getPrStatusColumn();
		getPrRemarkColumn();
		getViewBtnColumn();
	}

	

	private void addAnotherCheckBoxCell() {
		checkColumn=new Column<PurchaseRequisition, Boolean>(new CheckboxCell()) {
			@Override
			public Boolean getValue(PurchaseRequisition object) {
				return object.getRecordSelect();
			}
		};
		checkColumn.setFieldUpdater(new FieldUpdater<PurchaseRequisition, Boolean>() {
			@Override
			public void update(int index, PurchaseRequisition object, Boolean value) {
				System.out.println("Check Column ....");
//				Console.log("INSIDE CHECKBOX UPDATOR METHOD - "+BillingListPresenter.viewDocumentFlag);
//				Console.log("INSIDE CHECKBOX UPDATOR METHOD AP - "+BillingListApPresenter.viewDocumentFlag);
//				BillingListPresenter.viewDocumentFlag=false;
//				BillingListApPresenter.viewDocumentFlag=false;
//				Console.log("AFTER - "+BillingListPresenter.viewDocumentFlag);
//				Console.log("AFTER AP - "+BillingListApPresenter.viewDocumentFlag);
				
				object.setRecordSelect(value);
				table.redrawRow(index);
				selectAllHeader.getValue();
				table.redrawHeaders();
				
//				glassPanel = new GWTCGlassPanel();
//				glassPanel.show();
//				showWaitSymbol();
//				Timer timer = new Timer() {
//					@Override
//					public void run() {
//						glassPanel.hide();
//						hideWaitSymbol();
//					}
//				};
//				timer.schedule(1000); 
			}
		});
		selectAllHeader =new Header<Boolean>(new CheckboxCell()) {
			@Override
			public Boolean getValue() {
				if(getDataprovider().getList().size()!=0){
					
				}
				return false;
			}
		};
		selectAllHeader.setUpdater(new ValueUpdater<Boolean>() {
			@Override
			public void update(Boolean value) {
				List<PurchaseRequisition> list=getDataprovider().getList();
				for(PurchaseRequisition object:list){
					object.setRecordSelect(value);
				}
				getDataprovider().setList(list);
				table.redraw();
				addColumnSorting();
			}
		});
		table.addColumn(checkColumn,selectAllHeader);
	}
	
	private void getPrCountColumn() {
		getPrCountColumn=new TextColumn<PurchaseRequisition>() {
			@Override
			public String getValue(PurchaseRequisition object) {
				return object.getCount()+"";
			}
		};
		table.addColumn(getPrCountColumn,"PR Id");
		getPrCountColumn.setSortable(true);
	}

	private void getPrTitleColumn() {
		getPrTitleColumn=new TextColumn<PurchaseRequisition>() {
			@Override
			public String getValue(PurchaseRequisition object) {
				if(object.getPrTitle()!=null){
					return object.getPrTitle()+"";
				}
				return "";
			}
		};
		table.addColumn(getPrTitleColumn,"PR Title");
		getPrTitleColumn.setSortable(true);
		
	}

	private void getPrDateColumn() {
		getPrDateColumn=new TextColumn<PurchaseRequisition>() {
			@Override
			public String getValue(PurchaseRequisition object) {
				if(object.getCreationDate()!=null){
					return AppUtility.parseDate(object.getCreationDate());
				}
				return "";
			}
		};
		table.addColumn(getPrDateColumn,"PR Date");
		getPrDateColumn.setSortable(true);
		
	}

	private void getPrExpDelDateColumn() {
		getPrExpDelDateColumn=new TextColumn<PurchaseRequisition>() {
			@Override
			public String getValue(PurchaseRequisition object) {
				if(object.getExpectedDeliveryDate()!=null){
					return AppUtility.parseDate(object.getExpectedDeliveryDate());
				}
				return "";
			}
		};
		table.addColumn(getPrExpDelDateColumn,"Exp. Del. Date");
		getPrExpDelDateColumn.setSortable(true);
		
	}

	private void getPrPurEnggColumn() {
		getPrPurEnggColumn=new TextColumn<PurchaseRequisition>() {
			@Override
			public String getValue(PurchaseRequisition object) {
				if(object.getEmployee()!=null){
					return object.getEmployee();
				}
				return "";
			}
		};
		table.addColumn(getPrPurEnggColumn,"Purchase Engg.");
		getPrPurEnggColumn.setSortable(true);
	}

	private void getPrApproverColumn() {
		getPrApproverColumn=new TextColumn<PurchaseRequisition>() {
			@Override
			public String getValue(PurchaseRequisition object) {
				if(object.getApproverName()!=null){
					return object.getApproverName();
				}
				return "";
			}
		};
		table.addColumn(getPrApproverColumn,"Approver");
		getPrApproverColumn.setSortable(true);
	}

	private void getPrBranchColumn() {
		getPrBranchColumn=new TextColumn<PurchaseRequisition>() {
			@Override
			public String getValue(PurchaseRequisition object) {
				if(object.getBranch()!=null){
					return object.getBranch();
				}
				return "";
			}
		};
		table.addColumn(getPrBranchColumn,"Branch");
		getPrBranchColumn.setSortable(true);
		
	}

	private void getPrStatusColumn() {
		getPrStatusColumn=new TextColumn<PurchaseRequisition>() {
			@Override
			public String getValue(PurchaseRequisition object) {
				if(object.getStatus()!=null){
					return object.getStatus();
				}
				return "";
			}
		};
		table.addColumn(getPrStatusColumn,"Status");
		getPrStatusColumn.setSortable(true);
		
	}
	
	private void getPrRemarkColumn() {
		getPrRemarkColumn=new TextColumn<PurchaseRequisition>() {
			@Override
			public String getValue(PurchaseRequisition object) {
				if(object.getRemark()!=null){
					return object.getRemark();
				}
				return "";
			}
		};
		table.addColumn(getPrRemarkColumn,"Remark");
		getPrRemarkColumn.setSortable(true);
	}
	
	private void getViewBtnColumn() {
		ButtonCell btnCell=new ButtonCell();
		getViewBtnColumn=new Column<PurchaseRequisition,String>(btnCell) {
			@Override
			public String getValue(PurchaseRequisition object) {
				return "View Product";
			}
		};
		table.addColumn(getViewBtnColumn,"");
		
		
		getViewBtnColumn.setFieldUpdater(new FieldUpdater<PurchaseRequisition, String>() {
			@Override
			public void update(int index, PurchaseRequisition object, String value) {
				System.out.println("View Product Button Clicked !!!");
				rowIndex=index;
//				if(object.getStatus().equals(PurchaseRequisition.CREATED)||object.getStatus().equals(PurchaseRequisition.REQUESTED)){
//					productPopup.getPrTable().setEnable(true);
//					productPopup.getBtnOk().setEnabled(true);
//				}else{
//					productPopup.getPrTable().setEnable(false);
//					productPopup.getBtnOk().setEnabled(false);
//				}
				
				if(object.getStatus().equals(PurchaseRequisition.APPROVED)){
					productPopup.getPrTable().setEnable(true);
					productPopup.getBtnOk().setEnabled(true);
				}else{
					productPopup.getPrTable().setEnable(false);
					productPopup.getBtnOk().setEnabled(false);
				}
				productPopup.clear();
				productPopup.getPrTable().setValue(object.getPrproduct());
				panel=new PopupPanel(true);
				panel.add(productPopup);
				panel.show();
				panel.center();
				
				
				
				
			}
		});
	}
	
	
	public void addColumnSorting() {
		addSortingToPrCountColumn();
		addSortingToPrTitleColumn();
		addSortingToPrDateColumn();
		addSortingToPrExpDelDateColumn();
		addSortingToPrPurEnggColumn();
		addSortingToPrApproverColumn();
		addSortingToPrBranchColumn();
		addSortingToPrStatusColumn();
		addSortingToPrRemarkColumn();
	}

	
	private void addSortingToPrCountColumn() {
		List<PurchaseRequisition> list = getDataprovider().getList();
		columnSort = new ListHandler<PurchaseRequisition>(list);
		columnSort.setComparator(getPrCountColumn, new Comparator<PurchaseRequisition>() {
			@Override
			public int compare(PurchaseRequisition e1, PurchaseRequisition e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCount() == e2.getCount()) {
						return 0;
					}
					if (e1.getCount() > e2.getCount()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);	
	}

	private void addSortingToPrTitleColumn() {
		List<PurchaseRequisition> list = getDataprovider().getList();
		columnSort = new ListHandler<PurchaseRequisition>(list);
		columnSort.setComparator(getPrTitleColumn,new Comparator<PurchaseRequisition>() {
			@Override
			public int compare(PurchaseRequisition e1, PurchaseRequisition e2) {
				if (e1 != null && e2 != null) {
					if (e1.getPrTitle() != null&& e2.getPrTitle() != null) {
						return e1.getPrTitle().compareTo(e2.getPrTitle());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);	
	}

	private void addSortingToPrDateColumn() {
		List<PurchaseRequisition> list = getDataprovider().getList();
		columnSort = new ListHandler<PurchaseRequisition>(list);
		columnSort.setComparator(getPrDateColumn,new Comparator<PurchaseRequisition>() {
			@Override
			public int compare(PurchaseRequisition e1, PurchaseRequisition e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCreationDate() != null&& e2.getCreationDate() != null) {
						return e1.getCreationDate().compareTo(e2.getCreationDate());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);		
	}

	private void addSortingToPrExpDelDateColumn() {
		List<PurchaseRequisition> list = getDataprovider().getList();
		columnSort = new ListHandler<PurchaseRequisition>(list);
		columnSort.setComparator(getPrExpDelDateColumn,new Comparator<PurchaseRequisition>() {
			@Override
			public int compare(PurchaseRequisition e1, PurchaseRequisition e2) {
				if (e1 != null && e2 != null) {
					if (e1.getExpectedDeliveryDate() != null&& e2.getExpectedDeliveryDate() != null) {
						return e1.getExpectedDeliveryDate().compareTo(e2.getExpectedDeliveryDate());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);		
	}

	private void addSortingToPrPurEnggColumn() {
		List<PurchaseRequisition> list = getDataprovider().getList();
		columnSort = new ListHandler<PurchaseRequisition>(list);
		columnSort.setComparator(getPrPurEnggColumn,new Comparator<PurchaseRequisition>() {
			@Override
			public int compare(PurchaseRequisition e1, PurchaseRequisition e2) {
				if (e1 != null && e2 != null) {
					if (e1.getEmployee() != null&& e2.getEmployee() != null) {
						return e1.getEmployee().compareTo(e2.getEmployee());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);		
	}

	private void addSortingToPrApproverColumn() {
		List<PurchaseRequisition> list = getDataprovider().getList();
		columnSort = new ListHandler<PurchaseRequisition>(list);
		columnSort.setComparator(getPrApproverColumn,new Comparator<PurchaseRequisition>() {
			@Override
			public int compare(PurchaseRequisition e1, PurchaseRequisition e2) {
				if (e1 != null && e2 != null) {
					if (e1.getApproverName() != null&& e2.getApproverName() != null) {
						return e1.getApproverName().compareTo(e2.getApproverName());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);		
	}

	private void addSortingToPrBranchColumn() {
		List<PurchaseRequisition> list = getDataprovider().getList();
		columnSort = new ListHandler<PurchaseRequisition>(list);
		columnSort.setComparator(getPrBranchColumn,new Comparator<PurchaseRequisition>() {
			@Override
			public int compare(PurchaseRequisition e1, PurchaseRequisition e2) {
				if (e1 != null && e2 != null) {
					if (e1.getBranch() != null&& e2.getBranch() != null) {
						return e1.getBranch().compareTo(e2.getBranch());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);		
	}

	private void addSortingToPrStatusColumn() {
		List<PurchaseRequisition> list = getDataprovider().getList();
		columnSort = new ListHandler<PurchaseRequisition>(list);
		columnSort.setComparator(getPrStatusColumn,new Comparator<PurchaseRequisition>() {
			@Override
			public int compare(PurchaseRequisition e1, PurchaseRequisition e2) {
				if (e1 != null && e2 != null) {
					if (e1.getStatus() != null&& e2.getStatus() != null) {
						return e1.getStatus().compareTo(e2.getStatus());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);		
	}
	
	private void addSortingToPrRemarkColumn() {
		List<PurchaseRequisition> list = getDataprovider().getList();
		columnSort = new ListHandler<PurchaseRequisition>(list);
		columnSort.setComparator(getPrRemarkColumn,new Comparator<PurchaseRequisition>() {
			@Override
			public int compare(PurchaseRequisition e1, PurchaseRequisition e2) {
				if (e1 != null && e2 != null) {
					if (e1.getRemark() != null&& e2.getRemark() != null) {
						return e1.getRemark().compareTo(e2.getRemark());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);		
		
	}

	
	@Override
	public void onClick(ClickEvent event) {
		if(event.getSource().equals(productPopup.getBtnOk())){
			System.out.println("OK CLICKED");
			if(validQuantity()){
			if(productPopup.getPrTable().isValidWarehouse){
				getDataprovider().getList().get(rowIndex).setPrproduct(productPopup.getPrTable().getValue());
				PurchaseRequisition entity=getDataprovider().getList().get(rowIndex);
				
				showWaitSymbol();
				service.save(entity, new AsyncCallback<ReturnFromServer>() {
					@Override
					public void onSuccess(ReturnFromServer result) {
						hideWaitSymbol();
					}
					
					@Override
					public void onFailure(Throwable caught) {
						hideWaitSymbol();
					}
				});
			
				panel.hide();
			}else{
				GWTCAlert alert=new GWTCAlert();
				alert.alert("Warehouse is already selected for product, please change warehouse or delete row.");
			}
			}
		}
		if(event.getSource().equals(productPopup.getBtnCancel())){
			System.out.println("CANCEL CLICKED");
//			table.redraw();
//			productPopup.getPrTable().getTable().redraw();
			panel.hide();
		}
	}

	public boolean validQuantity(){
		
		List<ProductDetails> list=productPopup.getPrTable().getValue();
		
		
		for(ProductDetails object:list){
			if(object.getAcceptedQty()>object.getProductQuantity()){
				GWTCAlert alert =new GWTCAlert();
				alert.alert("Accepted quantity can not be greater than requested quantity!");
				return false;
			}
		}
		
		for(ProductDetails object:list){
			double totalQty=object.getBalanceQty()+object.getAcceptedQty();
			if(totalQty>object.getProductQuantity()){
				GWTCAlert alert =new GWTCAlert();
				alert.alert("Sum of accepted quantity and balance quantity can not be greater than requested quantity!");
				return false;
			}
		}
		
		
		return true;
	}
	

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
	
	public void showWaitSymbol(){
		glassPanel=new GWTCGlassPanel();
		glassPanel.show();
	}
	
	public void hideWaitSymbol(){
		glassPanel.hide();
	}

	
}
