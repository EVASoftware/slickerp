package com.slicktechnologies.client.views.purchase.purchaselist;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.code.p.gwtchismes.client.GWTCGlassPanel;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.Cell;
import com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.SelectionCell;
import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseRequisition;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;
import com.slicktechnologies.shared.common.inventory.WareHouse;

public class PrProductTable extends SuperTable<ProductDetails>{
	NumberFormat nf=NumberFormat.getFormat("0.00");
	TextColumn<ProductDetails> prodID;
	TextColumn<ProductDetails> prodcode;
	TextColumn<ProductDetails> prodname;
	TextColumn<ProductDetails> prodcategory;
	
	TextColumn<ProductDetails> viewPriceColumn;
	Column<ProductDetails, String> prodprice;
	
	TextColumn<ProductDetails> viewprodQty;
	
	TextColumn<ProductDetails> viewAccpQty;
	Column<ProductDetails, String> accpQty;
//	Column<ProductDetails, Double> accpQty;
	
	TextColumn<ProductDetails> viewBalQty;
	Column<ProductDetails, String> balQty;
	
	Column<ProductDetails, String> deleteColumn;
	
	
	/**
	 * warehouse ,exp. delivery date and avilable stock coloumn is added.
	 * Date : 26-09-2016 By Anil
	 * Release :30 Sept 2016
	 * Project: PURCHASE MODIFICATION (NBHC)
	 */
	TextColumn<ProductDetails> warehouseColumn;
	Column<ProductDetails,String> warehouseListColumn;
	TextColumn<ProductDetails> expDeliveryDateColumn;
	Column<ProductDetails,Date> editableExpDeliveryDateColumn;
	TextColumn<ProductDetails> availableStockColumn;
	ArrayList<String> warehouseList;
	ArrayList<WareHouse> warehouseEntityList;
	protected GWTCGlassPanel glassPanel;
	public static boolean isValidWarehouse=true;
	
	/**
	 * End
	 */

	/**
	 * Date 23-05-2017 By Vijay
	 * Total Column And Warehouse Address column
	 * Project : Deadstock (NBHC)
	 */
	TextColumn<ProductDetails> totalColumn;
	TextColumn<ProductDetails> warehouseAddressColumn; 
	/**
	 * ends here
	 */
	
	public PrProductTable() {
		super();
		setHeight("250Px");
	}
	
	@Override
	public void createTable() {
		addColumnprodID();
		addColumnprodcode();
		addColumnprodname();
		addColumnprodcategory();
		addColumnVendorprodprice();
		addColumnviewQty();
		addColumnAccepQty();
		addColumnBalQty();
		addColumnTotal();
		addEditableExpDeliveryDateColoumn();
		retrieveWarehouseNameList();

	}

	public void addeditColumn() {
		addColumnprodID();
		addColumnprodcode();
		addColumnprodname();
		addColumnprodcategory();
		addColumnVendorprodprice();
		addColumnviewQty();
		addColumnAccepQty();
		addColumnBalQty();
		addColumnTotal();
		addEditableExpDeliveryDateColoumn();
		retrieveWarehouseNameList();
	}

	public void addViewColumn() {
		addColumnprodID();
		addColumnprodcode();
		addColumnprodname();
		addColumnprodcategory();
		addColumnViewPrice();
		addColumnviewQty();
		addViewColumnAccepQty();
		addViewColumnBalQty();
		addColumnTotal();
		addExpDeliveryDateColoumn();
		addWarehouseColoumn();
		addAvailableStockColoumn();
		addWarehouseAddressColumn();


	}

	@Override
	public void addFieldUpdater() {
		columnEditPrice();
		createFieldUpdaterDelete();
//		createFieldUpdaterprodQtyColumn();
		setFieldUpdatorOnDeliveryDate();
		setFieldUpdatorOnWarehouse();
		setFieldUpdatorOnAcceptedQty();
		setFieldUpdatorOnBalanceQty();
	}
	
	@Override
	public void setEnable(boolean state) {
		int tablecolcount = this.table.getColumnCount();
		for (int i = tablecolcount - 1; i > -1; i--)
			table.removeColumn(i);
		if (state == true)
			addeditColumn();
		if (state == false)
			addViewColumn();
	}
	
	/*********************************************Product View Column ********************************************/
	
	/**
	 * This method loads all the active warehouse in selection list.
	 * purpose of loading warehouse is that product should be delivered in that warehouse.
	 * Date:27-09-2016 By Anil
	 * Release :30 Sept 2016 
	 * Project : PURCHASE MODIFICATION(NBHC) 
	 */
	
	public void retrieveWarehouseNameList(){
		final GenricServiceAsync service = GWT.create(GenricService.class);
		MyQuerry query = new MyQuerry();
		System.out.println("Company Id :: "+UserConfiguration.getCompanyId());
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(UserConfiguration.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);
		
		query.setFilters(filtervec);
		query.setQuerryObject(new WareHouse());
		
		service.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				warehouseList=new ArrayList<String>();
				warehouseEntityList=new ArrayList<WareHouse>();
				warehouseList.add("");
				if(result.size()!=0){
					for(SuperModel model:result){
						WareHouse entity=(WareHouse) model;
						warehouseList.add(entity.getBusinessUnitName());
						warehouseEntityList.add(entity);
					}
					Collections.sort(warehouseList);
				}
				
				
				addEditableWarehouseColoumn();
				addAvailableStockColoumn();
				addWarehouseAddressColumn();
				createColumndeleteColumn();
				addFieldUpdater();
			}
			
			@Override
			public void onFailure(Throwable caught) {
				
			}
		});
	}
	
	/**
	 * Date 23-05-2017
	 * Total Column And Warehouse Address column
	 * Project : Deadstock (NBHC)
	 */
	
	private void addWarehouseAddressColumn() {

		warehouseAddressColumn = new TextColumn<ProductDetails>() {
			
			@Override
			public String getValue(ProductDetails object) {
				return object.getWarehouseAddress();
			}
		};
		table.addColumn(warehouseAddressColumn, "Warehouse Address");
		table.setColumnWidth(warehouseAddressColumn, 200,Unit.PX);
	}
	
	private void addColumnTotal() {
		totalColumn = new TextColumn<ProductDetails>() {
			
			@Override
			public String getValue(ProductDetails object) {
				double total = object.getProdPrice()*object.getAcceptedQty();
				object.setTotal(total);
				return nf.format(object.getTotal());
			}
		};
		table.addColumn(totalColumn,"Total");
		table.setColumnWidth(totalColumn, 90,Unit.PX);
	}
	
	/**
	 * ends here
	 */
	
	public void addWarehouseColoumn(){
		warehouseColumn=new TextColumn<ProductDetails>() {
			@Override
			public String getValue(ProductDetails object) {
				if(object.getWarehouseName()!=null){
					return object.getWarehouseName();
				}
				return "";
			}
		};
		table.addColumn(warehouseColumn, "#Warehouse");
		table.setColumnWidth(warehouseColumn, 130,Unit.PX);
	}
	
	public void addEditableWarehouseColoumn(){
		SelectionCell selectionCell= new SelectionCell(warehouseList);
		warehouseListColumn=new Column<ProductDetails, String>(selectionCell) {
			
			@Override
			public String getCellStyleNames(Context context, ProductDetails object) {
				 
				if(object.getWarehouseName()==null){
					 return "red";
				 }
				return "";
			}
			@Override
			public String getValue(ProductDetails object) {
				if(object.getWarehouseName()!=null){
					return object.getWarehouseName();
				}
				return "";
			}
		};
		table.addColumn(warehouseListColumn, "#Warehouse");
		table.setColumnWidth(warehouseListColumn, 130,Unit.PX);
		
	}
	
	public void addExpDeliveryDateColoumn(){
		expDeliveryDateColumn=new TextColumn<ProductDetails>() {
			@Override
			public String getValue(ProductDetails object) {
				if(object.getProdDate()!=null){
					return AppUtility.parseDate(object.getProdDate());
				}
				return "";
			}
		};
		table.addColumn(expDeliveryDateColumn, "#Delivery Date");
		table.setColumnWidth(expDeliveryDateColumn, 100,Unit.PX);
	}
	public void addEditableExpDeliveryDateColoumn(){
		final DateTimeFormat fmt = DateTimeFormat.getFormat("dd/MM/yyyy");
		DatePickerCell date= new DatePickerCell(fmt);
		editableExpDeliveryDateColumn=new Column<ProductDetails,Date>(date) {
			@Override
			public Date getValue(ProductDetails object) {
				if(object.getProdDate()!=null){
					return fmt.parse(fmt.format(object.getProdDate()));
				}else{
					object.setProdDate(new Date());
					return fmt.parse(fmt.format(new Date()));
				}
			}
		};
		table.addColumn(editableExpDeliveryDateColumn, "#Delivery Date");
		table.setColumnWidth(editableExpDeliveryDateColumn, 100,Unit.PX);
	}
	
	public void addAvailableStockColoumn(){
		availableStockColumn=new TextColumn<ProductDetails>() {
			@Override
			public String getValue(ProductDetails object) {
//				if(object.getAvailableStock()!=0){
					return object.getAvailableStock()+"";
//				}
//				return "";
			}
		};
		table.addColumn(availableStockColumn, "Available Stock");
		table.setColumnWidth(availableStockColumn, 90,Unit.PX);
	}
	
	private void setFieldUpdatorOnWarehouse(){
		warehouseListColumn.setFieldUpdater(new FieldUpdater<ProductDetails, String>() {
			@Override
			public void update(int index, ProductDetails object, String value) {
				
				if(validWarehouseSelection(object.getProductID(), value)){
					isValidWarehouse=true;
				}else{
					isValidWarehouse=false;
				}
				setAvailableStock(index,object,value);
				

				//Below Method added by vijay for setting warehouse address for Deadstock NBHC on 26-04-2017
				setwarehouseAddress(index,object,value);
			}
		});
	}
	
	/**
	 * Date 23-05-2017
	 * added by vijay Deadstock NBHC
	 * for setting warehouse address
	 */
	private void setwarehouseAddress(int index, ProductDetails object, String warehouseName) {
		
		for(int i=0;i<warehouseEntityList.size();i++){
			if(warehouseEntityList.get(i).getBusinessUnitName().equals(warehouseName)){
				object.setWarehouseAddress(warehouseEntityList.get(i).getAddress().getCompleteAddress());
				break;
			}
		}
		table.redrawRow(index);
		
	}
	
	private void setFieldUpdatorOnDeliveryDate(){
		editableExpDeliveryDateColumn.setFieldUpdater(new FieldUpdater<ProductDetails, Date>() {
			@Override
			public void update(int index, ProductDetails object, Date value) {
				object.setProdDate(value);
				table.redrawRow(index);				
			}
		});
	}
	
	
	/**
	 * 
	 * @param productID
	 * @param value -warehouse name
	 * @return true/false
	 * 
	 * This method checks whether selected warehouse for given product id is already added.
	 * if added then return false else true.
	 * Date : 27-09-2016 By Anil
	 * Release : 30 Sept 2016
	 * Project: PURCHASE MODIFICATION(NBHC)
	 */
	private boolean validWarehouseSelection(int productID, String value) {
		List<ProductDetails> list=getDataprovider().getList();
		for(ProductDetails prod:list){
			if(productID==prod.getProductID()&&value.trim().equals(prod.getWarehouseName().trim())){
				GWTCAlert alert =new GWTCAlert();
				alert.alert("Warehouse is already selected for product !");
				return false;
			}
		}
		return true;
	}
	
	
	/**
	 * 
	 * @param index -selected row
	 * @param object - selected row data
	 * @param value - updated warehouse name
	 * 
	 * This method return the available Stock of product in selected warehouse.
	 * Date :27-09-2016 By Anil
	 * Release : 30 Sept 2016
	 * Project : PURCHASE MODIFICATION(NBHC)
	 */
	
	private void setAvailableStock(final int index, final ProductDetails object, final String value) {
		final GenricServiceAsync service = GWT.create(GenricService.class);
		MyQuerry query = new MyQuerry();
		System.out.println("Company Id :: "+UserConfiguration.getCompanyId()+" Product Id :: "+object.getProductID()+" Warehouse  :: "+value);
		
		
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(UserConfiguration.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("productinfo.prodID");
		filter.setIntValue(object.getProductID());
		filtervec.add(filter);
		
//		filter = new Filter();
//		filter.setQuerryString("details.warehousename");
//		filter.setStringValue(value.trim());
//		filtervec.add(filter);
		
		query.setFilters(filtervec);
		query.setQuerryObject(new ProductInventoryView());
		showWaitSymbol();
		service.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				hideWaitSymbol();
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				hideWaitSymbol();
				System.out.println("RESULT SIZE : "+result.size());
				double avilableStock=0;
				if(result.size()!=0){
					ProductInventoryView entity=(ProductInventoryView) result.get(0);
					for(ProductInventoryViewDetails prod:entity.getDetails()){
						if(prod.getWarehousename().trim().equals(value.trim())){
							avilableStock=avilableStock+prod.getAvailableqty();
						}
					}
				}
				object.setAvailableStock(avilableStock);
				object.setWarehouseName(value);
				System.out.println("ROW INDEX "+index);
				if(isValidWarehouse){
					AppUtility.ApplyWhiteColoronRow(PrProductTable.this, index);
				}else{
					AppUtility.ApplyRedColoronRow(PrProductTable.this, index);
				}
				
				
				if(isValidWarehouse){
					List<ProductDetails> list=getDataprovider().getList();
					Integer rowIndex=getRowCountOfWarehouse(list);
					if(rowIndex!=null){
						isValidWarehouse=false;
						AppUtility.ApplyRedColoronRow(PrProductTable.this, rowIndex);
					}else{
						isValidWarehouse=true;
						AppUtility.ApplyWhiteColoronRow(PrProductTable.this, index);
					}
				}
				
				
				table.redraw();
				System.out.println("ROW UPDATED "+object.getWarehouseName()+" - "+object.getAvailableStock());
				
			}

			
		});
	}
	
	/**
	 * This method returns row index whose which has duplicate warehouse name for perticular product.
	 * Date : 28-09-2016 By Anil
	 * Release : 30 Sept 2016 
	 * Project : PURCHASE MODIFICATION(NBHC)
	 * 
	 */

	private Integer getRowCountOfWarehouse(List<ProductDetails> list) {
		for(ProductDetails prod1:list){
			int count=0;
			int index=0;
			for(ProductDetails prod2:list){
				if(prod1.getProductID()==prod2.getProductID()&&prod1.getWarehouseName().equals(prod2.getWarehouseName())){
					count++;
				}
				if(count==2){
					return index;
				}
				index++;
			}
		}
		return null;
	}
	

	public void addColumnprodID() {
		prodID = new TextColumn<ProductDetails>() {

			@Override
			public String getValue(ProductDetails object) {
				return object.getProductID() + "";
			}
		};
		table.addColumn(prodID, "ID");
		table.setColumnWidth(prodID, 90,Unit.PX);
		
	}
	
	public void addColumnprodcode() {
		prodcode = new TextColumn<ProductDetails>() {

			@Override
			public String getValue(ProductDetails object) {
				return object.getProductCode();
			}
		};
		table.addColumn(prodcode, "Code");
		table.setColumnWidth(prodcode, 100,Unit.PX);
	}

	public void addColumnprodname() {
		prodname = new TextColumn<ProductDetails>() {

			@Override
			public String getValue(ProductDetails object) {
				return object.getProductName();
			}
		};
		table.addColumn(prodname, "Name");
		table.setColumnWidth(prodname, 100,Unit.PX);
	}
	
	public void addColumnprodcategory() {
		prodcategory = new TextColumn<ProductDetails>() {

			@Override
			public String getValue(ProductDetails object) {
				return object.getProductCategory();
			}
		};
		table.addColumn(prodcategory, "Category");
		table.setColumnWidth(prodcategory, 100,Unit.PX);
	}
	
	//Read Only Price Column
	public void addColumnViewPrice() {
		viewPriceColumn = new TextColumn<ProductDetails>() {

			@Override
			public String getValue(ProductDetails object) {
				return object.getProdPrice() + "";
			}
		};
		table.addColumn(viewPriceColumn, "#Price");
		table.setColumnWidth(viewPriceColumn, 90,Unit.PX);
	}

	//Editable Price Column
	public void addColumnVendorprodprice() {
		EditTextCell editCell = new EditTextCell();
		prodprice = new Column<ProductDetails, String>(editCell) {

			@Override
			public String getValue(ProductDetails object) {
				return object.getProdPrice() + "";
			}
		};
		table.addColumn(prodprice, "#Price");
		table.setColumnWidth(prodprice, 90,Unit.PX);
	}
	//Read Only Quantity Column
	public void addColumnviewQty() {
		viewprodQty = new TextColumn<ProductDetails>() {

			@Override
			public String getValue(ProductDetails object) {
				return nf.format(object.getProductQuantity());
			}
		};
		table.addColumn(viewprodQty, "Requested Quantity");
		table.setColumnWidth(viewprodQty, 90,Unit.PX);
	}
	
	//Editable Quantity Column
	public void addColumnAccepQty() {
		EditTextCell editCell = new EditTextCell();
		accpQty = new Column<ProductDetails, String>(editCell) {

			@Override
			public String getValue(ProductDetails object) {
//				if(object.getAcceptedQty()!=0){
				System.out.println("@@@@@ACCPTED QTY :: "+object.getAcceptedQty());
					return nf.format(object.getAcceptedQty());
//				}
//				return "";
			}

			@Override
			public String getCellStyleNames(Context context,ProductDetails object) {
//				return super.getCellStyleNames(context, object);
				if(object.getAcceptedQty()>object.getProductQuantity()){
					return "red";
				}
				return "";
			}
		};
		table.addColumn(accpQty, "#Accepted Quantity");
		table.setColumnWidth(accpQty, 90,Unit.PX);
		
	}
	
	//Editable Quantity Column
	public void addColumnBalQty() {
		EditTextCell editCell = new EditTextCell();
		balQty = new Column<ProductDetails, String>(editCell) {

			@Override
			public String getValue(ProductDetails object) {
//					if(object.getBalanceQty()!=0){
					return nf.format(object.getBalanceQty());
//					}
//					return "";
			}
			
			
			@Override
			public String getCellStyleNames(Context context,ProductDetails object) {
//				return super.getCellStyleNames(context, object);
				if(object.getBalanceQty()!=0){
					double totalQty=object.getBalanceQty()+object.getAcceptedQty();
					if(totalQty>object.getProductQuantity()){
						return "red";
					}
				}
				return "";
			}
		};
		table.addColumn(balQty, "#Balance Quantity");
		table.setColumnWidth(balQty, 90,Unit.PX);
	}
		
	public void addViewColumnAccepQty() {
		viewAccpQty = new TextColumn<ProductDetails>() {

			@Override
			public String getValue(ProductDetails object) {
				return nf.format(object.getAcceptedQty());
			}
		};
		table.addColumn(viewAccpQty, "#Accepted Quantity");
		table.setColumnWidth(viewAccpQty, 90,Unit.PX);
	}
	
	public void addViewColumnBalQty() {
		viewBalQty = new TextColumn<ProductDetails>() {
			@Override
			public String getValue(ProductDetails object) {
				return nf.format(object.getBalanceQty());
			}
		};
		table.addColumn(viewBalQty, "#Balance Quantity");
		table.setColumnWidth(viewBalQty, 90,Unit.PX);
	}
	
	

	public void createColumndeleteColumn() {
		ButtonCell btnCell = new ButtonCell();
		deleteColumn = new Column<ProductDetails, String>(btnCell) {
			@Override
			public String getValue(ProductDetails object) {
				return "Delete";
			}
		};
		table.addColumn(deleteColumn, "");
		table.setColumnWidth(deleteColumn, 80,Unit.PX);

	}

	/****************************************Product Column Updater *******************************************/
	
	public void columnEditPrice() {
		prodprice.setFieldUpdater(new FieldUpdater<ProductDetails, String>() {
			@Override
			public void update(int index, ProductDetails object, String value) {
				try {
					Integer val1 = Integer.parseInt(value.trim());
					object.setProdPrice(val1);
					
					//total added by vijay for deadstock nbhc Date 23-05-2017
					object.setTotal(val1*object.getAcceptedQty());
					
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				} catch (NumberFormatException e) {
				}
				table.redrawRow(index);
			}
		});
	}

	public void createFieldUpdaterDelete() {
		deleteColumn.setFieldUpdater(new FieldUpdater<ProductDetails, String>() {
			@Override
			public void update(int index, ProductDetails object,String value) {
				System.out.println("PRODUCT LIST SIZE BEFOR : "+getDataprovider().getList().size());
				getDataprovider().getList().remove(object);
				table.redrawRow(index);
				System.out.println("PRODUCT LIST SIZE AFTER : "+getDataprovider().getList().size());
//				PurchaseRequitionPresenter.updateVendorTable();
				
			}
		});
	}

//	protected void createFieldUpdaterprodQtyColumn() {
//		prodQty.setFieldUpdater(new FieldUpdater<ProductDetails, String>() {
//			@Override
//			public void update(int index, ProductDetails object, String value) {
//				try {
//					double val1 = Double.parseDouble(value.trim());
//					object.setProductQuantity(val1);
//					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
//				} catch (NumberFormatException e) {
//
//				}
//				table.redrawRow(index);
//			}
//		});
//	}

	protected void setFieldUpdatorOnAcceptedQty() {
		accpQty.setFieldUpdater(new FieldUpdater<ProductDetails, String>() {
			@Override
			public void update(final int index, ProductDetails object, String value) {
				try {
					double val1 = Double.parseDouble(value.trim());
					if(val1>object.getProductQuantity()){
//						object.setAcceptedQty(object.getProductQuantity());
						object.setAcceptedQty(val1);
						object.setBalanceQty(0);
						GWTCAlert alert =new GWTCAlert();
						alert.alert("Accepted quantity can not be greater than requested quantity!");
						
//						System.out.println("REQUESTED QUANTITY : "+object.getProductQuantity());
//						System.out.println("ACCEPTED QUANTITY : "+object.getAcceptedQty());
//						System.out.println("GET DB LIST SIZE BF :: "+getDataprovider().getList().size());
//						final ArrayList<ProductDetails> list=new ArrayList<ProductDetails>();
//						list.addAll(getDataprovider().getList());
//						System.out.println("LIST SIZE AF :: "+list.size());
//						list.get(index).setAcceptedQty(object.getProductQuantity());
//						System.out.println("LIST ACCEPTED QTY :: "+list.get(index).getAcceptedQty());
//						getDataprovider().getList().clear();
//						System.out.println("GET DB LIST SIZE  :: "+getDataprovider().getList().size());
//						Timer t = new Timer() {
//							@Override
//							public void run() {
//								System.out.println("LIST ACCEPTED QTY 1:: "+list.get(index).getAcceptedQty());
//								getDataprovider().setList(list);
//								System.out.println("GET DB LIST SIZE AF :: "+getDataprovider().getList().size());
//							}
//						};
//						t.schedule(500);
						
						
						
					}else{
						System.out.println("ACCEPTED QTY : "+val1);
						double balQty=object.getProductQuantity()-val1;
						System.out.println("BALANCE QTY : "+balQty);
						object.setAcceptedQty(val1);
						object.setBalanceQty(balQty);
						
						//total added by vijay for deadstock nbhc Date 23-05-2017
						double total =val1*object.getProdPrice();
						object.setTotal(total);
					}
				} catch (NumberFormatException e) {

				}
				table.redrawRow(index);
			}
		});
	}
	
	protected void setFieldUpdatorOnBalanceQty() {
		balQty.setFieldUpdater(new FieldUpdater<ProductDetails, String>() {
			@Override
			public void update(int index, ProductDetails object, String value) {
				try {
					double val1 = Double.parseDouble(value.trim());
					double totalQty=val1+object.getAcceptedQty();
					if(totalQty>object.getProductQuantity()){
//						double balQty=object.getProductQuantity()-object.getAcceptedQty();
//						object.setBalanceQty(balQty);
						
						object.setBalanceQty(val1);
						
						
						GWTCAlert alert =new GWTCAlert();
						alert.alert("Sum of accepted quantity and balance quantity can not be greater than requested quantity!");
						
					}else{
						object.setBalanceQty(val1);
					}
				} catch (NumberFormatException e) {

				}
				table.redrawRow(index);
			}
		});
	}
	
	
	
	@Override
	protected void initializekeyprovider() {

	}
	
	@Override
	public void applyStyle() {

	}
	
	
	public void showWaitSymbol() {
		glassPanel = new GWTCGlassPanel();
		glassPanel.show();
		
		
	}
	public void hideWaitSymbol() {
	   glassPanel.hide();
		
	}

}
