package com.slicktechnologies.client.views.purchase.vendorPriceList;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;
import com.slicktechnologies.client.views.purchase.vendorPriceList.VendorPriceListPresenter.VendorPricelistPresenterTable;
import com.slicktechnologies.shared.common.businessprocesslayer.PriceList;

public class VendorPriceListPresenterTableProxy extends VendorPricelistPresenterTable {
	TextColumn<PriceList> columnCount;
	TextColumn<PriceList> columnTitle;
	TextColumn<PriceList> columnProductID;
	TextColumn<PriceList> columnProductName;
	TextColumn<PriceList> columnProductCode;
	TextColumn<PriceList> columnProductCategory;
	TextColumn<PriceList> columnProductPrice;

	@Override
	protected void initializekeyprovider() {
		keyProvider = new ProvidesKey<PriceList>() {
			@Override
			public Object getKey(PriceList item) {
				if (item == null) {
					return null;
				} else
					return item.getId();
			}
		};
	}

	public VendorPriceListPresenterTableProxy() {

	}

	@Override
	public void createTable() {
		addColumnCount();
		addColumnTitle();
		addColumnProductID();
		addColumnProductName();
		addColumnProductCode();
		addColumnProductCategory();
		addColumnProductPrice();

	}

	

	public void addColumnSorting() {
		addSortinggetCount();
		addSortinggetTitle();
		addSortinggetProductID();
		addSortinggetProductName();
		addSortinggetProductCode();
		addSortinggetProductCategory();
		addSortingProductPrice();
	}

	public Object getVarRef(String varName) {
		if (varName.equals("columnCount"))
			return this.columnCount;
		return null;
	}

	/******************** sorting columns *******************/
	public void addSortinggetTitle() {
		List<PriceList> list = getDataprovider().getList();
		columnSort = new ListHandler<PriceList>(list);
		columnSort.setComparator(columnTitle, new Comparator<PriceList>() {
			@Override
			public int compare(PriceList e1, PriceList e2) {
				if (e1 != null && e2 != null) {
					if (e1.getPricelistTitle() != null
							&& e2.getPricelistTitle() != null) {
						return e1.getPricelistTitle().compareTo(
								e2.getPricelistTitle());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	public void addSortinggetCount() {
		List<PriceList> list = getDataprovider().getList();
		columnSort = new ListHandler<PriceList>(list);
		columnSort.setComparator(columnCount, new Comparator<PriceList>() {
			@Override
			public int compare(PriceList e1, PriceList e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCount() == e2.getCount()) {
						return 0;
					}
					if (e1.getCount() > e2.getCount()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	public void addSortinggetProductID() {
		List<PriceList> list = getDataprovider().getList();
		columnSort = new ListHandler<PriceList>(list);
		columnSort.setComparator(columnProductID, new Comparator<PriceList>() {
			@Override
			public int compare(PriceList e1, PriceList e2) {
				if (e1 != null && e2 != null) {
					if (e1.getProdID() == e2.getProdID()) {
						return 0;
					}
					if (e1.getProdID() > e2.getProdID()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	protected void addSortinggetProductName() {
		List<PriceList> list = getDataprovider().getList();
		columnSort = new ListHandler<PriceList>(list);
		columnSort.setComparator(columnProductName,
				new Comparator<PriceList>() {
					@Override
					public int compare(PriceList e1, PriceList e2) {
						if (e1 != null && e2 != null) {
							if (e1.getProdName() != null
									&& e2.getProdName() != null) {
								return e1.getProdName().compareTo(
										e2.getProdName());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addSortinggetProductCategory() {
		List<PriceList> list = getDataprovider().getList();
		columnSort = new ListHandler<PriceList>(list);
		columnSort.setComparator(columnProductCategory,
				new Comparator<PriceList>() {
					@Override
					public int compare(PriceList e1, PriceList e2) {
						if (e1 != null && e2 != null) {
							if (e1.getProdCategory() != null
									&& e2.getProdCategory() != null) {
								return e1.getProdCategory().compareTo(
										e2.getProdCategory());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addSortinggetProductCode() {
		List<PriceList> list = getDataprovider().getList();
		columnSort = new ListHandler<PriceList>(list);
		columnSort.setComparator(columnProductCode,
				new Comparator<PriceList>() {
					@Override
					public int compare(PriceList e1, PriceList e2) {
						if (e1 != null && e2 != null) {
							if (e1.getProdCode() != null
									&& e2.getProdCode() != null) {
								return e1.getProdCode().compareTo(
										e2.getProdCode());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	private void addSortingProductPrice() {
		List<PriceList> list = getDataprovider().getList();
		columnSort = new ListHandler<PriceList>(list);
		columnSort.setComparator(columnProductPrice, new Comparator<PriceList>() {
			@Override
			public int compare(PriceList e1, PriceList e2) {
				if (e1 != null && e2 != null) {
					if (e1.getProdPrice() == e2.getProdPrice()) {
						return 0;
					}
					if (e1.getProdPrice() > e2.getProdPrice()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	/************************************************************************************/
	public void addColumnTitle() {
		columnTitle = new TextColumn<PriceList>() {

			@Override
			public String getValue(PriceList object) {
				return object.getPricelistTitle();
			}
		};
		table.addColumn(columnTitle, "Title");
		columnTitle.setSortable(true);
	}

	public void addColumnCount() {
		columnCount = new TextColumn<PriceList>() {

			@Override
			public String getValue(PriceList object) {
				return object.getCount() + "";
			}
		};
		table.addColumn(columnCount, "ID");
		columnCount.setSortable(true);
	}

	public void addColumnProductID() {
		columnProductID = new TextColumn<PriceList>() {

			@Override
			public String getValue(PriceList object) {
				return object.getProdID() + "";
			}
		};
		table.addColumn(columnProductID, "Product ID");
		columnProductID.setSortable(true);
	}

	public void addColumnProductName() {
		columnProductName = new TextColumn<PriceList>() {

			@Override
			public String getValue(PriceList object) {
				return object.getProdName();
			}
		};
		table.addColumn(columnProductName, "Name");
		columnProductName.setSortable(true);
	}

	public void addColumnProductCode() {
		columnProductCode = new TextColumn<PriceList>() {

			@Override
			public String getValue(PriceList object) {
				return object.getProdCode();
			}
		};
		table.addColumn(columnProductCode, "Code");
		columnProductCode.setSortable(true);
	}

	public void addColumnProductCategory() {
		columnProductCategory = new TextColumn<PriceList>() {
			@Override
			public String getValue(PriceList object) {
				return object.getProdCategory();
			}
		};
		table.addColumn(columnProductCategory, "Category");
		columnProductCategory.setSortable(true);
	}
	
	private void addColumnProductPrice() {
		columnProductPrice = new TextColumn<PriceList>() {
			@Override
			public String getValue(PriceList object) {
				return object.getProdPrice()+"";
			}
		};
		table.addColumn(columnProductPrice, "Price");
		columnProductPrice.setSortable(true);
	}

}