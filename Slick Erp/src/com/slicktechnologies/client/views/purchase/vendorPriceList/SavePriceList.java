package com.slicktechnologies.client.views.purchase.vendorPriceList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.simplesoftwares.client.library.ReturnFromServer;

import com.slicktechnologies.shared.common.businessprocesslayer.PriceList;

@RemoteServiceRelativePath("/savepricelist")
public interface SavePriceList extends RemoteService{
	
	public ReturnFromServer savePriceList(PriceList list);

}
