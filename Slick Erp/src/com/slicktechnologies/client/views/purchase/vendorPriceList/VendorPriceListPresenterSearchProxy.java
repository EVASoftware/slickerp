package com.slicktechnologies.client.views.purchase.vendorPriceList;

import java.util.Vector;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.client.views.purchase.vendorPriceList.VendorPriceListPresenter.VendorPricelistPresenterSearch;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessprocesslayer.PriceList;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;

public class VendorPriceListPresenterSearchProxy extends VendorPricelistPresenterSearch implements ClickHandler
{
	 	public ObjectListBox<PriceList> productName;
	 	IntegerBox ibproductID;
	 	PersonInfoComposite vic;
		ProductInfoComposite pic;
		TextBox title;


	public Object getVarRef(String varName)
	{
	  
		return null ;
	}



	public VendorPriceListPresenterSearchProxy()
	{
		super();
		createGui();
		
	}
	
	public VendorPriceListPresenterSearchProxy(boolean b)
	{
		super();
		createGui();
		
	}
	 public void initWidget()
	  {
		ibproductID=new IntegerBox();
		vic=AppUtility.vendorInfoComposite(new Vendor());
		vic.getCustomerId().getHeaderLabel().setText("Vendor Id");
		vic.getCustomerName().getHeaderLabel().setText("Vendor Name");
		vic.getCustomerCell().getHeaderLabel().setText("Vendor Cell");
		
//		pic=AppUtility.product(new SuperProduct(),false);
		pic=AppUtility.initiateSalesProductComposite(new SuperProduct());//Ashwini Patil Date:24-04-2024 New product was not getting loaded
		
		title=new TextBox();
	  }
	 
	 public void createScreen() {
			
			initWidget();
			FormFieldBuilder fbuilder;
			
			fbuilder = new FormFieldBuilder();
			
			fbuilder = new FormFieldBuilder("Product Price List ID",ibproductID);
			FormField fibproductID= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			
		
			fbuilder = new FormFieldBuilder("",pic);
			FormField fpic= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();

			fbuilder = new FormFieldBuilder("",vic);
			FormField fvic= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
			
//			fbuilder = new FormFieldBuilder();
//			FormField fgroupingProductInformation=fbuilder.setlabel("Product").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(2).build();
//			
//			fbuilder = new FormFieldBuilder();
//			FormField fgroupingVendorInformation=fbuilder.setlabel("Vendor").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(2).build();
			
			fbuilder = new FormFieldBuilder("Title",title);
			FormField ftitle= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			
			FormField[][] formfield = {  
					{fibproductID,ftitle},
//					{fgroupingVendorInformation},
					{fvic},
//					{fgroupingProductInformation},
					{fpic}
					
					
					
					
			};

			this.fields=formfield;

			
	 }
	 
	  public MyQuerry getQuerry()
	  {
	  Vector<Filter> filtervec=new Vector<Filter>();
	  Filter temp=null;
	  if(ibproductID.getValue()!=null)
	  {
		  temp=new Filter();
		  temp.setIntValue(ibproductID.getValue());
		  temp.setQuerryString("count");
		  filtervec.add(temp);

	  }
	 
			  if(!pic.getProdName().getValue().equals(""))
	  {
		  temp=new Filter();
		  temp.setStringValue(pic.getProdName().getValue());
		  temp.setQuerryString("prodName");
		  filtervec.add(temp);
	  }
			  
	if(!pic.getProdCode().getValue().equals(""))
	{
		temp=new Filter();
		temp.setStringValue(pic.getProdCode().getValue());
		temp.setQuerryString("prodCode");
		filtervec.add(temp);
	}
	if(!pic.getProdID().getValue().equals(""))
	{
		
		temp=new Filter();
		temp.setIntValue(Integer.parseInt(pic.getProdID().getValue()));
		temp.setQuerryString("prodID");
		filtervec.add(temp);
	}
			  
			  
			  if(!vic.getName().getValue().equals(""))
			  {
				  temp=new Filter();
				  temp.setStringValue(vic.getName().getValue());
				  temp.setQuerryString("priceInfo.personinfo.fullName");
				  filtervec.add(temp);
				  
			  }
			  if(!vic.getId().getValue().equals(""))
			  {
				  temp=new Filter();
				  temp.setIntValue(Integer.parseInt(vic.getId().getValue()));
				  temp.setQuerryString("priceInfo.personinfo.count");
				  filtervec.add(temp);
						  
			  }
				if(!title.getValue().equals(""))
				{
					
					temp=new Filter();
					temp.setStringValue(title.getValue());
					temp.setQuerryString("pricelistTitle");
					filtervec.add(temp);
				}
			
			
	  
	  MyQuerry querry= new MyQuerry();
	  querry.setFilters(filtervec);
	  querry.setQuerryObject(new PriceList());
	 
	  System.out.println(querry);
	  return querry;

	  }
	

		  protected void initalizeProductNameListBox()
		{
			MyQuerry querry = new MyQuerry(new Vector<Filter>(),new PriceList());

			productName.MakeLive(querry);
		}



	@Override
	public void onClick(ClickEvent event) {
		
	}



	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
		return super.validate();
	}

	
}
