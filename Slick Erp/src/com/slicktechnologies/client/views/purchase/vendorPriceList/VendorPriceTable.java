package com.slicktechnologies.client.views.purchase.vendorPriceList;

import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.ClickableTextCell;
import com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.complain.ServiceDateBoxPopup;
import com.slicktechnologies.shared.common.businessprocesslayer.PriceListDetails;

public class VendorPriceTable extends SuperTable<PriceListDetails> implements ClickHandler {
	
	
	//vijay
		PopupPanel Panel;
		ServiceDateBoxPopup datepopup = new ServiceDateBoxPopup("Date",null);
		int rowIndex;

	TextColumn<PriceListDetails> vendorID;
	TextColumn<PriceListDetails> vendorname;
	TextColumn<PriceListDetails> vendorCell;
	Column<PriceListDetails, String> prodprice;
	TextColumn<PriceListDetails> prodname;
	TextColumn<PriceListDetails> prodcategory;
	TextColumn<PriceListDetails> prodcode;
	Column<PriceListDetails, String> deleteColumn;
	Column<PriceListDetails, Date> dateColumn;
//	Column<PriceListDetails, Date> TodateColumn;
	TextColumn<PriceListDetails> vendorColumn;
	TextColumn<PriceListDetails> viewFromDateColumn;
	TextColumn<PriceListDetails> viewPriceColumn;
	TextColumn<PriceListDetails> viewTodateColumn;
	TextColumn<PriceListDetails> UnitColumn;

	Column<PriceListDetails, String> TodateColumn;
	
	VendorPriceTable(){
		super();
		datepopup.getBtnOne().addClickHandler(this);
		datepopup.getBtnTwo().addClickHandler(this);
	}

	
	@Override
	public void createTable() {
		addColumnVendorID();
		addColumnVendorname();
		addColumnVendorCell();
		addColumnprodcode();
		addColumnprodcategory();
		addColumnprodname();
		addColumnVendorprodprice();
		addColumnUnitOfMeasurement();
		addColumnFromDate();
		addColumnToDate();
		createColumndeleteColumn();
		addFieldUpdater();
		table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);

	}

	

	public void addeditColumn() {
		addColumnVendorID();
		addColumnVendorname();
		addColumnVendorCell();
		addColumnprodcode();
		addColumnprodcategory();
		addColumnprodname();
		addColumnVendorprodprice();
		addColumnUnitOfMeasurement();
		addColumnFromDate();
		addColumnToDate();
		createColumndeleteColumn();
		addFieldUpdater();
		table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);

	}

	public void addViewColumn() {
		addColumnVendorID();
		addColumnVendorname();
		addColumnVendorCell();
		addColumnprodcode();
		addColumnprodcategory();
		addColumnprodname();
		addColumnViewPrice();
		addColumnUnitOfMeasurement();
		addColumnViewFromDate();
		addColumnViewToDate();
	}

	@Override
	protected void initializekeyprovider() {

	}

	@Override
	public void addFieldUpdater() {
		columnEditPrice();
		createFieldUpdaterDelete();
		createFieldUpdaterdateColumn();
		createFieldUpdaterTodateColumn();

	}

	@Override
	public void applyStyle() {

	}

	public void addColumnViewPrice() {
		viewPriceColumn = new TextColumn<PriceListDetails>() {

			@Override
			public String getValue(PriceListDetails object) {
				return object.getProductPrice() + "";
			}
		};
		table.addColumn(viewPriceColumn, "#Price");
	}

	public void addColumnViewFromDate() {
		viewFromDateColumn = new TextColumn<PriceListDetails>() {

			@Override
			public String getValue(PriceListDetails object) {
				return AppUtility.parseDate(object.getFromdate()) + "";
			}
		};
		table.addColumn(viewFromDateColumn, "From Date");
	}

	public void addColumnViewToDate() {
		viewTodateColumn = new TextColumn<PriceListDetails>() {

			@Override
			public String getValue(PriceListDetails object) {
				// TODO Auto-generated method stub
				return AppUtility.parseDate(object.getTodate());
			}
		};
		table.addColumn(viewTodateColumn, "To Date");
	}

	public void addColumnVendorID() {
		vendorID = new TextColumn<PriceListDetails>() {
			@Override
			public String getValue(PriceListDetails object) {
				return object.getVendorID() + "";
			}
		};
		table.addColumn(vendorID, "Vendor ID");
	}

	public void addColumnVendorname() {
		vendorname = new TextColumn<PriceListDetails>() {
			@Override
			public String getValue(PriceListDetails object) {
				return object.getFullName();
			}
		};
		table.addColumn(vendorname, "Vendor Name");
	}

	public void addColumnVendorCell() {
		vendorCell = new TextColumn<PriceListDetails>() {
			@Override
			public String getValue(PriceListDetails object) {
				return object.getCellNumber()+"";
			}
		};
		table.addColumn(vendorCell, "Vendor Cell");
	}

	public void addColumnprodcategory() {
		prodcategory = new TextColumn<PriceListDetails>() {
			@Override
			public String getValue(PriceListDetails object) {
				return object.getProductCategory();
			}
		};
		table.addColumn(prodcategory, "Category");
	}

	public void addColumnprodcode() {
		prodcode = new TextColumn<PriceListDetails>() {
			@Override
			public String getValue(PriceListDetails object) {
				return object.getProductCode();
			}
		};
		table.addColumn(prodcode, "Pro. Code");
	}

	public void addColumnprodname() {
		prodname = new TextColumn<PriceListDetails>() {

			@Override
			public String getValue(PriceListDetails object) {
				// TODO Auto-generated method stub
				return object.getProductName();
			}
		};
		table.addColumn(prodname, "Name");
	}

	public void addColumnVendorprodprice() {
		EditTextCell editCell = new EditTextCell();
		prodprice = new Column<PriceListDetails, String>(editCell) {
			@Override
			public String getValue(PriceListDetails object) {
				return object.getProductPrice() + "";
			}
		};
		table.addColumn(prodprice, "#Price");
	}

	public void createColumndeleteColumn() {
		ButtonCell btnCell = new ButtonCell();
		deleteColumn = new Column<PriceListDetails, String>(btnCell) {
			@Override
			public String getValue(PriceListDetails object) {
				return "Delete";
			}
		};
		table.addColumn(deleteColumn, "Delete");

	}

	public void columnEditPrice() {
		prodprice.setFieldUpdater(new FieldUpdater<PriceListDetails, String>() {
			@Override
			public void update(int index, PriceListDetails object, String value) {

				try {
					Double val1 = Double.parseDouble(value.trim());
					object.setProductPrice(val1);
					RowCountChangeEvent.fire(table, getDataprovider().getList()
							.size(), true);

				} catch (NumberFormatException e) {

				}

				table.redrawRow(index);
			}
		});

	}

	public void createFieldUpdaterDelete() {
		deleteColumn.setFieldUpdater(new FieldUpdater<PriceListDetails, String>() {
					@Override
					public void update(int index, PriceListDetails object,String value) {
						getDataprovider().getList().remove(object);

						table.redrawRow(index);
					}
				});

	}

	public void addColumnFromDate() {
		DateTimeFormat fmt = DateTimeFormat
				.getFormat(PredefinedFormat.DATE_SHORT);
		DatePickerCell date = new DatePickerCell(fmt);
		dateColumn = new Column<PriceListDetails, Date>(date) {

			@Override
			public Date getValue(PriceListDetails object) {
				// TODO Auto-generated method stub
				if (object.getFromdate() != null) {
					return object.getFromdate();
				} else {
					object.setFromdate(new Date());
					return object.getFromdate();
				}
			}
		};
		table.addColumn(dateColumn, "From Date");

	}

	public void addColumnToDate() {
		DateTimeFormat fmt = DateTimeFormat
				.getFormat(PredefinedFormat.DATE_SHORT);
		DatePickerCell date = new DatePickerCell(fmt);
		// old commented by vijay
//		TodateColumn = new Column<PriceListDetails, Date>(date) {
//
//			@Override
//			public Date getValue(PriceListDetails object) {
//				if (object.getTodate() != null) {
//					return object.getTodate();
//				} else {
//					Date date = DateTimeFormat.getFormat("yyyy-MM-dd").parse(
//							"9999-12-31");
//
//					object.setTodate(date);
//					object.getTodate();
//				}
//				return object.getTodate();
//			}
//		};
		
		// new code added by vijay
		
		ClickableTextCell clickcell = new ClickableTextCell();
		
		TodateColumn = new Column<PriceListDetails, String>(clickcell) {
			
			@Override
			public String getValue(PriceListDetails object) {
				if (object.getTodate() != null) {
					return DateTimeFormat.getFormat("yyyy-MM-dd").format(object.getTodate());
				} else {
					Date date = DateTimeFormat.getFormat("yyyy-MM-dd").parse(
							"9999-12-31");

					object.setTodate(date);
					object.getTodate();
				}
				return DateTimeFormat.getFormat("yyyy-MM-dd").format(object.getTodate());
			}
		};
		table.addColumn(TodateColumn, "To Date");
	}

	protected void createFieldUpdaterdateColumn() {
		dateColumn.setFieldUpdater(new FieldUpdater<PriceListDetails, Date>() {
			@Override
			public void update(int index, PriceListDetails object, Date value) {
				object.setFromdate(value);
				table.redrawRow(index);
			}
		});
	}

	protected void createFieldUpdaterTodateColumn() {
		// old cold
//		TodateColumn.setFieldUpdater(new FieldUpdater<PriceListDetails, Date>() {
//			
//					@Override
//					public void update(int index, PriceListDetails object,Date value) {
//						object.setTodate(value);
//						table.redrawRow(index);
//					}
//				});
		
		
		// new code
		TodateColumn.setFieldUpdater(new FieldUpdater<PriceListDetails, String>() {
			
			@Override
			public void update(int index, PriceListDetails object, String value) {
				// TODO Auto-generated method stub
				datepopup.cleardate();
				Panel = new PopupPanel(true);
				Panel.add(datepopup);
				Panel.show();
				Panel.center();
				
				rowIndex=index;
			}
		});
	}

	protected void addColumnUnitOfMeasurement() {
		UnitColumn = new TextColumn<PriceListDetails>() {

			@Override
			public String getValue(PriceListDetails object) {
				if (object.getUnitofMeasure()!=null && object.getUnitofMeasure().equals(""))
					return "N/A";
				else
					return object.getUnitofMeasure();
			}
		};
		table.addColumn(UnitColumn, "UOM");
	}

	@Override
	public void setEnable(boolean state) {
		int tablecolcount = this.table.getColumnCount();
		for (int i = tablecolcount - 1; i > -1; i--)
			table.removeColumn(i);
		if (state == true)
			addeditColumn();
		if (state == false)
			addViewColumn();

	}


	@Override
	public void onClick(ClickEvent event) {
		
	if(event.getSource()==datepopup.getBtnOne()){
			
			ArrayList<PriceListDetails> list = new ArrayList<PriceListDetails>();
			list.addAll(getDataprovider().getList());
			list.get(rowIndex).setTodate(datepopup.getServiceDate().getValue());
			Panel.hide();
			
			getDataprovider().getList().clear();
			getDataprovider().getList().addAll(list);
			
		}
		
		if(event.getSource()==datepopup.getBtnTwo()){
			Panel.hide();
		}
	}

}
