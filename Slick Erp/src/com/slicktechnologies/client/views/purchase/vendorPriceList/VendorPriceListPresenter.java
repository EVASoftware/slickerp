package com.slicktechnologies.client.views.purchase.vendorPriceList;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.businessprocesslayer.PriceList;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;


public class VendorPriceListPresenter extends FormScreenPresenter<PriceList> implements SelectionHandler<Suggestion> 
{

	public static VendorPriceListForm form;
	
	public VendorPriceListPresenter(FormScreen<PriceList> view,PriceList model) {
		super(view, model);
		form.pic.getProdID().addSelectionHandler(this);
		form.pic.getProdName().addSelectionHandler(this);
		form.pic.getProdCode().addSelectionHandler(this);
		form.setPresenter(this);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.VENDORPRICELIST,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel label = (InlineLabel) e.getSource();
		String text = label.getText().trim();
		if (text.equals(AppConstants.NEW))
			reactToNew();
	}

	private void reactToNew() {
		form.setToNewState();
		initalize();
	}
	

	@Override
	public void reactOnPrint() {
		
	}
	
	@Override
	public void reactOnDownload()
	{
		CsvServiceAsync async=GWT.create(CsvService.class);
		ArrayList<PriceList> vendorarray=new ArrayList<PriceList>();
		List<PriceList> list=form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
		
		vendorarray.addAll(list); 
		
		
		async.setVendorProduct(vendorarray, new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);
				
			}

			@Override
			public void onSuccess(Void result) {
				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "csvservlet"+"?type="+42;
				Window.open(url, "test", "enabled");
			}
		});

	}

	@Override
	public void reactOnEmail() {
		
	}

	@Override
	protected void makeNewModel() {
		model=new PriceList();
		
	}

	public static VendorPriceListForm initalize()
	{
		
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Vendor Product Price",Screen.VENDORPRICELIST);
		
		  form=new  VendorPriceListForm();
	
		  VendorPricelistPresenterTable gentable=new VendorPriceListPresenterTableProxy();
		  gentable.setView(form);
		  gentable.applySelectionModle();
		  
		 VendorPricelistPresenterSearch.staticSuperTable=gentable;
		 VendorPricelistPresenterSearch searchpopup=new VendorPriceListPresenterSearchProxy();
		form.setSearchpopupscreen(searchpopup);
		
		  
		  
		 System.out.println("initalize");
		 
		
		 VendorPriceListPresenter  presenter=new VendorPriceListPresenter(form,new PriceList());
		AppMemory.getAppMemory().stickPnel(form);
		return form;
		}

	
	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.businessprocesslayer.PriceList")
	 public static class VendorPricelistPresenterTable extends SuperTable<PriceList> implements GeneratedVariableRefrence{

		@Override
		public Object getVarRef(String varName) {
			return null;
		}

		@Override
		public void createTable() {
		}

		@Override
		protected void initializekeyprovider() {
			
		}

		@Override
		public void addFieldUpdater() {
			
		}

		@Override
		public void setEnable(boolean state) {
			
		}

		@Override
		public void applyStyle() {
			
		}
	
		};
		
		@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.businessprocesslayer.PriceList")
		 public static  class VendorPricelistPresenterSearch extends SearchPopUpScreen<PriceList>{

			@Override
			public MyQuerry getQuerry() {
				return null;
			}

			@Override
			public boolean validate() {
				// TODO Auto-generated method stub
				return true;
			}
			
		}

		@Override
		public void onSelection(SelectionEvent<Suggestion> event) {
			if (event.getSource().equals(form.pic.getProdID())||event.getSource().equals(form.pic.getProdName())||event.getSource().equals(form.pic.getProdCode())) {
				form.retriveProduct();
			}
		};

}
