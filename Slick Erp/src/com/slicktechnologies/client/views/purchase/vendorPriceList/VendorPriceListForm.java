package com.slicktechnologies.client.views.purchase.vendorPriceList;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.google.gwt.view.client.RowCountChangeEvent.Handler;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.CompositeInterface;
import com.simplesoftwares.client.library.composite.OneToManyComposite;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.contract.SalesLineItemTable;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessprocesslayer.PriceListDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.PriceList;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class VendorPriceListForm extends FormScreen<PriceList> implements
		ClickHandler, Handler {

	final GenricServiceAsync async = GWT.create(GenricService.class);
	public static int temp = 0;
	TextBox ibproductID;
	TextBox tbPriceListTitle;
	VendorPriceTable vendorpricetable;
	
	/**
	 * This list is used for saving old vendor data for vendor product price change 
	 */
	
		List<PriceListDetails> oldVendorProductList = new ArrayList<PriceListDetails>();
	
		/**
	 * ends here 
	 */
		
	/**
	 * This VendorPriceTable() used for for saving history of vendor product price change
	 * 
	 */
		VendorPriceTable vendorProductPriceHistory;
		
	/**
	 *  ends here 
	 */
	
	Button btaddVendor;
	ProductInfoComposite pic;
	PersonInfoComposite vic;
	
	PriceList pricelistobj;
	
	public VendorPriceListForm() {
		super();
		createGui();
		ibproductID.getElement().getStyle().setWidth(200, Unit.PX);
		tbPriceListTitle.getElement().getStyle().setWidth(300, Unit.PX);
		vic.setMandatory(false);
		vendorpricetable.connectToLocal();
		

	}

	public VendorPriceListForm(String[] processlevel, FormField[][] fields,
			FormStyle formstyle) {
		super(processlevel, fields, formstyle);
		createGui();
	}

	private void initalizeWidget() {
		ibproductID = new TextBox();
		ibproductID.setEnabled(false);
		
		tbPriceListTitle = new TextBox();

//		pic = AppUtility.product(new SuperProduct(), true);
		pic=AppUtility.initiateSalesProductComposite(new SuperProduct());//Ashwini Patil Date:24-04-2024 New product was not getting loaded
		

		// Initialization of Person Info
		MyQuerry querry = new MyQuerry();
		querry.setQuerryObject(new Vendor());
		Filter filter = new Filter();
		filter.setQuerryString("vendorStatus");
		filter.setBooleanvalue(true);
		querry.getFilters().add(filter);
		
		vic = new PersonInfoComposite(querry, false);
		btaddVendor = new Button("Add");
		btaddVendor.addClickHandler(this);

		
		
		vendorpricetable = new VendorPriceTable();
		vendorpricetable.getTable().addRowCountChangeHandler(this);
		
		vendorProductPriceHistory = new VendorPriceTable();
		vendorProductPriceHistory.setEnable(false);
		
	}

	@Override
	public void toggleAppHeaderBarMenu() {
		if (AppMemory.getAppMemory().currentState == ScreeenState.NEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Save") || text.equals("Discard")
						|| text.equals("Search")|| text.equals(AppConstants.NAVIGATION)) {
					menus[k].setVisible(true);
				} else
					menus[k].setVisible(false);

			}
		}

		else if (AppMemory.getAppMemory().currentState == ScreeenState.EDIT) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Save") || text.equals("Discard")|| text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true);

				else
					menus[k].setVisible(false);
			}
		}

		else if (AppMemory.getAppMemory().currentState == ScreeenState.VIEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Discard") || text.equals("Edit")
						|| text.equals("Search")|| text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true);

				else
					menus[k].setVisible(false);
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.VENDORPRICELIST,LoginPresenter.currentModule.trim());

	}

	@Override
	public void createScreen() {
		initalizeWidget();
		this.processlevelBarNames = new String[] {AppConstants.NEW};
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingCustomerInformation = fbuilder
				.setlabel("Product Information").widgetType(FieldType.Grouping)
				.setMandatory(false).setColSpan(5).build();
		
		fbuilder = new FormFieldBuilder("Pricelist ID", ibproductID);
		FormField fibproductID = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Title", tbPriceListTitle);
		FormField ftbPriceListTitle = fbuilder.setMandatory(true)
				.setMandatoryMsg("Title is mandatory").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("", pic);
		FormField fpic = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(5).build();

		fbuilder = new FormFieldBuilder();
		FormField fvendorinformation = fbuilder.setlabel("Vendor Information")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(5).build();

		fbuilder = new FormFieldBuilder("", vic);
		FormField fvic = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("", btaddVendor);
		FormField fbtaddVendor = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("", vendorpricetable.getTable());
		FormField fvendorpricetable = fbuilder.setMandatory(false)
				.setRowSpan(0).setColSpan(5).build();
		
		
		fbuilder = new FormFieldBuilder();
		FormField fHistory = fbuilder.setlabel("Vendor Product Price History Details")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(5).build();

		
		fbuilder = new FormFieldBuilder("", vendorProductPriceHistory.getTable());
		FormField fHistoryTable = fbuilder.setMandatory(false)
				.setRowSpan(0).setColSpan(5).build();
		

		FormField[][] formfield = { 
				{ fgroupingCustomerInformation },
				{ fibproductID, ftbPriceListTitle },
				{ fpic },
				{ fvendorinformation }, 
				{ fvic }, { fbtaddVendor },
				{ fvendorpricetable },
				{fHistory},
				{fHistoryTable}

		};

		this.fields = formfield;

	}

	@Override
	public void updateModel(PriceList model) {

		if (tbPriceListTitle.getValue() != null)
			model.setPricelistTitle(tbPriceListTitle.getValue());

		if (pic.getValue() != null) {
			model.setProdID(Integer.parseInt(pic.getProdID().getValue()));
			model.setProdName(pic.getProdName().getValue());
			model.setProdCode(pic.getProdCode().getValue());
			model.setProdCategory(pic.getProdCategory().getValue());
			model.setProdPrice(pic.getProdPrice().getValue());
			model.setUnitOfmeasurement(pic.getUnitOfmeasuerment().getValue());

			if (pic.getPentity() != null) {
				model.setServiceTax(pic.getPentity().getServiceTax());
				model.setVat(pic.getPentity().getVat());
			}

		}
		
		List<PriceListDetails> priceTableLis = vendorpricetable.getDataprovider().getList();
		ArrayList<PriceListDetails> arrPricelis = new ArrayList<PriceListDetails>();
		arrPricelis.addAll(priceTableLis);
		model.setPriceInfo(arrPricelis);
		
		if(validateForVendorProductPriceChange()==true)
		{
			System.out.println("in side true condition in update model"); 
			
			List<PriceListDetails> historyPriceLis = vendorProductPriceHistory.getDataprovider().getList();
			ArrayList<PriceListDetails> historyProductPriceLis = new ArrayList<PriceListDetails>();
			historyProductPriceLis.addAll(historyPriceLis);
			model.setVendorPriceChangeHistory(historyProductPriceLis);
			
			
			oldVendorProductList = new ArrayList<PriceListDetails>();
			List<PriceListDetails> prodList = new ArrayList<PriceListDetails>();
			for (int i = 0; i < arrPricelis.size(); i++) {
				
				PriceListDetails prodDetails =new PriceListDetails();
				prodDetails.setCount(arrPricelis.get(i).getVendorID());
				prodDetails.setFullName(arrPricelis.get(i).getFullName());
				prodDetails.setCellNumber(arrPricelis.get(i).getCellNumber());
				prodDetails.setProductCode(arrPricelis.get(i).getProductCode());
				prodDetails.setProductCategory(arrPricelis.get(i).getProductCategory());
				prodDetails.setProductName(arrPricelis.get(i).getProductName());
				prodDetails.setProductPrice(arrPricelis.get(i).getProductPrice());
				prodDetails.setUnitofMeasure(arrPricelis.get(i).getUnitofMeasure());
				prodDetails.setFromdate(arrPricelis.get(i).getFromdate());
				prodDetails.setTodate(arrPricelis.get(i).getTodate());
				prodList.add(prodDetails);
			}
			oldVendorProductList.addAll(prodList);
		}
		
		pricelistobj=model;
		presenter.setModel(model);

	}

	
	/**
	 * this method is used to validate product price change 
	 * if product price is change then it will add that product in to history table  
	 * Date : 22-09-2016
	 * @return
	 */
	private boolean validateForVendorProductPriceChange() {
		int cnt =0;
		List<PriceListDetails> HistoryList = new ArrayList<PriceListDetails>();	
		List<PriceListDetails> historyTableList  =  vendorProductPriceHistory.getDataprovider().getList();
		List<PriceListDetails> CurrenttableList=vendorpricetable.getDataprovider().getList();
			for (int i = 0; i < oldVendorProductList.size(); i++) 
			{
				System.out.println("vendor old price price - vendor Id ="+oldVendorProductList.get(i).getProductPrice()+oldVendorProductList.get(i).getVendorID());
				for (int j = 0; j < CurrenttableList.size();j++) 
				{
					
					System.out.println("vendor new price - vendor id ="+CurrenttableList.get(j).getProductPrice()+CurrenttableList.get(j).getVendorID());
					if(oldVendorProductList.get(i).getVendorID()== CurrenttableList.get(j).getVendorID() && oldVendorProductList.get(i).getProductPrice() != CurrenttableList.get(j).getProductPrice())
					{
						System.out.println("inside vendor old price price "+oldVendorProductList.get(i).getProductPrice());
						System.out.println("inside vendor new price "+CurrenttableList.get(j).getProductPrice());
						PriceListDetails prod = new PriceListDetails();
						prod.setProdID(oldVendorProductList.get(i).getProdID());
						prod.setProductCategory(oldVendorProductList.get(i).getProductCategory());
						prod.setProductCode(oldVendorProductList.get(i).getProductCode());
						prod.setProductPrice(oldVendorProductList.get(i).getProductPrice());
						prod.setProductName(oldVendorProductList.get(i).getProductName());
						prod.setFromdate(oldVendorProductList.get(i).getFromdate());
						prod.setTodate(oldVendorProductList.get(i).getTodate());
						prod.setUnitofMeasure(oldVendorProductList.get(i).getUnitofMeasure());
						prod.setFullName(oldVendorProductList.get(i).getFullName());
						prod.setCount(oldVendorProductList.get(i).getVendorID());
						prod.setCellNumber(oldVendorProductList.get(i).getCellNumber());
						HistoryList.add(prod);
						cnt = cnt +1;
					}
				}
			}
			
			System.out.println("rohan size of history "+HistoryList.size());
			System.out.println("rohan size of history table size  "+historyTableList.size());
			
			HistoryList.addAll(historyTableList);
			System.out.println("rohan size of history after merging"+HistoryList.size());
			this.vendorProductPriceHistory.getDataprovider().setList(HistoryList);
		
		if(cnt > 0)
			return true;
		else
			return false;
		
	}

	@Override
	public void updateView(PriceList model) {
		
		pricelistobj=model;
		
		ibproductID.setValue(model.getCount()+"");
		
		if (model.getPricelistTitle() != null)
			tbPriceListTitle.setValue(model.getPricelistTitle());
		
		pic.getProdID().setValue(model.getProdID() + "");
		
		if (model.getProdName() != null)
			pic.getProdName().setText(model.getProdName());
		if (model.getProdCode() != null)
			pic.getProdCode().setText(model.getProdCode());
		if (model.getProdCategory() != null)
			pic.getProdCategory().setText(model.getProdCategory());
		
		pic.getProdPrice().setValue(model.getProdPrice());
		if(pic.getProdIDtoProdInfo().get(model.getProdID())!=null){
			pic.setPentity(pic.getProdIDtoProdInfo().get(model.getProdID()));
		}
		if (model.getUnitOfmeasurement() != null)
			pic.getUnitOfmeasuerment().setValue(model.getUnitOfmeasurement());

		vendorpricetable.setValue(model.getPriceInfo());
		
		retriveVendorPriceTable(model.getPriceInfo());
		
		vendorProductPriceHistory.setValue(model.getVendorPriceChangeHistory());
		
		for(int i=0;i<model.getPriceInfo().size();i++)
		{
			System.out.println("View"+model.getPriceInfo().get(i).getProductPrice());
		}
		presenter.setModel(model);

	}

	private void retriveVendorPriceTable(List<PriceListDetails> priceInfo) {
		
		List<PriceListDetails> list = new ArrayList<PriceListDetails>();
		for (int i = 0; i < priceInfo.size(); i++) 
		{
			PriceListDetails prod = new PriceListDetails();
			prod.setCount(priceInfo.get(i).getVendorID());
			prod.setCellNumber(priceInfo.get(i).getCellNumber());
			prod.setFullName(priceInfo.get(i).getFullName());
			prod.setProdID(priceInfo.get(i).getProdID());
			prod.setProductCode(priceInfo.get(i).getProductCode());
			prod.setProductName(priceInfo.get(i).getProductName());
			prod.setProductCategory(priceInfo.get(i).getProductCategory());
			prod.setProductPrice(priceInfo.get(i).getProductPrice());
			prod.setUnitofMeasure(priceInfo.get(i).getUnitofMeasure());
			prod.setFromdate(priceInfo.get(i).getFromdate());
			prod.setTodate(priceInfo.get(i).getTodate());
			
			list.add(prod);
		}
		
		
		System.out.println("Before update view "+oldVendorProductList.size());
		oldVendorProductList.addAll(list);
		System.out.println("After update view "+oldVendorProductList.size()+oldVendorProductList.get(0).getProductPrice() );
	}

	@Override
	public void setToEditState() {
		super.setToEditState();
		this.processLevelBar.setVisibleFalse(false);
	}

	@Override
	public void onClick(ClickEvent event) {

		if (event.getSource().equals(btaddVendor) && pic.getValue() != null&& validateVendor()) {
			addVendor();
		}
	}

	/******************** Add vendors ************************/

	public void addVendor() {
		if (pic.getValue() != null && vic.getValue() != null) {
			System.out.println("In Update iew");
			PriceListDetails info = new PriceListDetails();
			info.setCount(vic.getValue().getCount());
			info.setFullName(vic.getFullNameValue());
			info.setCellNumber(vic.getCellValue());
			info.setProductName(pic.getProdName().getText());
			info.setProductCode(pic.getProdCode().getText());
			info.setProductCategory(pic.getProdCategory().getValue());
			
			/**
			 * Date : 10-07-2018 By ANIL
			 */
//			info.setProductPrice(pic.getProdPrice().getValue());
			info.setProductPrice(pic.getPentity().getPurchasePrice());
			/**
			 * End
			 */
			
			info.setUnitofMeasure(pic.getUnitOfmeasuerment().getValue());
			
			//  anil added product id
			
			info.setProdID(pic.getIdValue());
			// vijay added poc 
			info.setPocName(vic.getValue().getPocName());
			System.out.println(" hi vijay poc name ==="+vic.getValue().getPocName());
			vendorpricetable.getDataprovider().getList().add(info);
			
			pic.setEnable(false);
//			vic.getName().setText("");
//			vic.getPhone().setText("");
//			vic.getId().setText("");
		//  rohan added this code to clear vendor composite on 12-06-2017
			vic.clear();
			
			
		} else if (pic.getValue() == null)
			this.showDialogMessage("Please Fill Product Information");
		else if (vic.getValue() == null)
			this.showDialogMessage("Please Fill Vendor Information");

	}

	/***************************************************/

	@Override
	public void setToViewState() {
		super.setToViewState();
		
		SuperModel model=new PriceList();
		model=pricelistobj;
		AppUtility.addDocumentToHistoryTable(AppConstants.PURCHASEMODULE,AppConstants.VENDORPRODUCTPRICE, pricelistobj.getCount(), null,null,null, false, model, null);
	
	}
	
	public ProductInfoComposite getPic() {
		return pic;
	}

	public void setPic(ProductInfoComposite pic) {
		this.pic = pic;
	}

	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		pic.setEnable(false);
		ibproductID.setEnabled(false);
		vendorpricetable.setEnable(state);

	}

	public boolean validateVendor() {
		List<PriceListDetails> list = vendorpricetable.getDataprovider().getList();

		for (PriceListDetails temp : list) {
			if (vic.getName().getValue().equals(temp.getFullName())) {
				this.showDialogMessage("Vendor already exist in table");
				vic.setValue(null);
				return false;
			}
		}
		return true;
	}

	@Override
	public boolean validate() {
		boolean b = super.validate();
		if (b == false)
			return false;

		if (vendorpricetable.getDataprovider().getList().size() == 0) {
			this.showDialogMessage("Please add atleast one vendor");
			return false;
		}
		boolean c = validateToDate();
		if (c == false)
			return false;
		

		return true;
	}

	@Override
	public void onRowCountChange(RowCountChangeEvent event) {
		if (vendorpricetable.getDataprovider().getList().size() == 0) {
			pic.setEnable(true);
		}
	}

//	
	public boolean validateToDate() {
		if (vendorpricetable.getDataprovider().getList().size() != 0) {
			List<PriceListDetails> list = vendorpricetable.getDataprovider().getList();
			for (int i = 0; i < list.size(); i++) {
				if (list.get(i).getFromdate().after(list.get(i).getTodate())) {
					this.showDialogMessage("From date is greater than To Date for Vendor"+ list.get(i).getFullName());
					return false;
				}
			}
		}
		return true;
	}

	@Override
	public void setCount(int count) {
		ibproductID.setValue(count+"");
	}

	public void retriveProduct() {
		MyQuerry querry = new MyQuerry();
		Company c=new Company();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		/**
		 * @author Anil , Date : 29-07-2019
		 * at the time of raising PO ,price list is checking validation with product code 
		 * but here it is checking with product name,this causes to add 
		 */
//		filter=new Filter();
//		filter.setQuerryString("prodName");
//		filter.setStringValue(pic.getProdName().getValue());
//		filtervec.add(filter);
		
		filter=new Filter();
		filter.setQuerryString("prodCode");
		filter.setStringValue(pic.getProdCode().getValue());
		filtervec.add(filter);
		
		/**
		 * End
		 */
		
		filter=new Filter();
		filter.setLongValue(c.getCompanyId());
		filter.setQuerryString("companyId");
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new PriceList());

		async.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						if (result.size() != 0) {
							showDialogMessage("Product Already Exist!");
							pic.clear();
						}
					}
					@Override
					public void onFailure(Throwable caught) {
						System.out.println("No Product Found");
					}
				});
	}

	/**
	 *  25-12-2020 Added by Priyanka issue raised by Ashwini as table not appear after click on save button
	 */
	@Override
	public void refreshTableData() {
		// TODO Auto-generated method stub
		super.refreshTableData();
		vendorpricetable.getTable().redraw();
		vendorProductPriceHistory.getTable().redraw();
	}
	
}