package com.slicktechnologies.client.views.purchase.vendorPriceList;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.slicktechnologies.shared.common.businessprocesslayer.PriceList;

public interface SavePriceListAsync {

	void savePriceList(PriceList list, AsyncCallback<ReturnFromServer> callback);

}
