package com.slicktechnologies.client.views.purchase.letterofintent;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.google.gwt.view.client.RowCountChangeEvent.Handler;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.approvalutility.ApprovableFormScreenPresenter;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.EmailService;
import com.slicktechnologies.client.services.EmailServiceAsync;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.documentcancellation.DocumentCancellationPopUp;
import com.slicktechnologies.client.views.popups.NewEmailPopUp;
import com.slicktechnologies.client.views.purchase.purchaseorder.PurchaseOrderForm;
import com.slicktechnologies.client.views.purchase.purchaseorder.PurchaseOrderPresenter;
import com.slicktechnologies.shared.common.businessprocesslayer.LetterOfIntent;
import com.slicktechnologies.shared.common.businessprocesslayer.PriceListDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetailsPO;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessprocesslayer.VendorDetails;
import com.slicktechnologies.shared.common.contactidentification.ContactPersonIdentification;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class LetterOfIntentPresenter extends
		ApprovableFormScreenPresenter<LetterOfIntent> implements Handler {

	public static LetterOfIntentForm form;
	final GenricServiceAsync async = GWT.create(GenricService.class);
	final CsvServiceAsync csvservice = GWT.create(CsvService.class);
	EmailServiceAsync emailService = GWT.create(EmailService.class);

	DocumentCancellationPopUp cancelPopup = new DocumentCancellationPopUp();
	PopupPanel cancelpanel;

 	NewEmailPopUp emailpopup = new NewEmailPopUp();

	public LetterOfIntentPresenter(FormScreen<LetterOfIntent> view,
			LetterOfIntent model) {
		super(view, model);
		form = (LetterOfIntentForm) view;
		// form.setPresenter(this);

		cancelPopup.getBtnOk().addClickHandler(this);
		cancelPopup.getBtnCancel().addClickHandler(this);
		form.productTableLOI.getTable().addRowCountChangeHandler(this);

		boolean isDownload = AuthorizationHelper.getDownloadAuthorization(
				Screen.LETTEROFINTENT, LoginPresenter.currentModule.trim());
		if (isDownload == false) {
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}

	}

	public LetterOfIntentPresenter(FormScreen<LetterOfIntent> view,
			LetterOfIntent model, MyQuerry querry) {
		super(view, model);
		form = (LetterOfIntentForm) view;
		// form.setPresenter(this);

		boolean isDownload = AuthorizationHelper.getDownloadAuthorization(
				Screen.LETTEROFINTENT, LoginPresenter.currentModule.trim());
		if (isDownload == false) {
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}

	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel label = (InlineLabel) e.getSource();
		String text = label.getText().trim();

		if (text.equals(ManageApprovals.APPROVALREQUEST)) {
			form.getManageapproval().reactToRequestForApproval();
		}
		if (text.equals(ManageApprovals.CANCELAPPROVALREQUEST)) {
			form.getManageapproval().reactToApprovalRequestCancel();
		}
		if (text.equals("Mark Close")) {
			ChangeStatus(LetterOfIntent.CLOSED);

		}
		if (text.equals(AppConstants.CANCELLOI)) {

			cancelPopup.getPaymentDate().setValue(new Date() + "");
			cancelPopup.getPaymentID().setValue(model.getCount() + "");
			cancelPopup.getPaymentStatus().setValue(model.getStatus());
			cancelpanel = new PopupPanel(true);
			cancelpanel.add(cancelPopup);
			cancelpanel.show();
			cancelpanel.center();
		}
		if (text.equals("New")) {
			initalize();
			form.setToNewState();
		}
		if (text.equals("Purchase Order")) {
			reactToPurchaseOrder();
		}
		if (text.equals("Email")) {
			reactOnEmail();
		}

	}

	@Override
	public void reactOnPrint() {

	}

	@Override
	public void reactOnEmail() {

//		boolean conf = Window.confirm("Do you really want to send email?");
//		if (conf == true) {
//			emailService.initiateLOIEmail((LetterOfIntent) model,
//					new AsyncCallback<Void>() {
//						@Override
//						public void onSuccess(Void result) {
//							Window.alert("Email Sent Sucessfully !");
//						}
//
//						@Override
//						public void onFailure(Throwable caught) {
//							Window.alert("Resource Quota Ended ");
//							caught.printStackTrace();
//						}
//					});
//		}
		
		/**
		 * @author Vijay Date 19-03-2021
		 * Des :- above old code commented 
		 * Added New Email Popup Functionality
		 */
		emailpopup.showPopUp();
		setEmailPopUpData();
		
		/**
		 * ends here
		 */
	}

	@Override
	protected void makeNewModel() {
		model = new LetterOfIntent();

	}

	public static LetterOfIntentForm initalize() {
		AppMemory.getAppMemory().currentState = ScreeenState.NEW;
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName(
				"Letter Of Intent(LOI)", Screen.LETTEROFINTENT);
		form = new LetterOfIntentForm();

		LetterOfIntentPresenterTableProxy gentable = new LetterOfIntentPresenterTableProxy();
		gentable.setView(form);
		gentable.applySelectionModle();

		LOIPresenterSearch.staticSuperTable = gentable;
		LetterOfIntentPresenterSearchProxy searchpopup = new LetterOfIntentPresenterSearchProxy();
		form.setSearchpopupscreen(searchpopup);

		LetterOfIntentPresenter presenter = new LetterOfIntentPresenter(form,
				new LetterOfIntent());
		AppMemory.getAppMemory().stickPnel(form);
		return form;
	}

	public static LetterOfIntentForm initalize(MyQuerry querry) {
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName(
				"Letter Of Intent(LOI)", Screen.LETTEROFINTENT);
		form = new LetterOfIntentForm();

		LetterOfIntentPresenterTableProxy gentable = new LetterOfIntentPresenterTableProxy();
		gentable.setView(form);
		gentable.applySelectionModle();

		LOIPresenterSearch.staticSuperTable = gentable;
		LOIPresenterSearch searchpopup = new LetterOfIntentPresenterSearchProxy();
		form.setSearchpopupscreen(searchpopup);

		LetterOfIntentPresenter presenter = new LetterOfIntentPresenter(form,
				new LetterOfIntent(), querry);
		AppMemory.getAppMemory().stickPnel(form);

		return form;

	}

	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.businessprocesslayer.LetterOfIntent")
	public static class LoiPresenterTable extends SuperTable<LetterOfIntent>
			implements GeneratedVariableRefrence {

		@Override
		public Object getVarRef(String varName) {
			return null;
		}

		@Override
		public void createTable() {

		}

		@Override
		protected void initializekeyprovider() {

		}

		@Override
		public void addFieldUpdater() {

		}

		@Override
		public void setEnable(boolean state) {

		}

		@Override
		public void applyStyle() {

		}

	};

	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.businessprocesslayer.LetterOfIntent")
	public static class LOIPresenterSearch extends
			SearchPopUpScreen<LetterOfIntent> {
		@Override
		public MyQuerry getQuerry() {
			return null;
		}

		@Override
		public boolean validate() {
			// TODO Auto-generated method stub
			return true;
		}

	};

	/************************************** On Click Event *******************************************/

	@Override
	public void onClick(ClickEvent event) {
		super.onClick(event);

		if (event.getSource() == cancelPopup.getBtnOk()) {
			cancelpanel.hide();
			changeStatusOnLOI(LetterOfIntent.CANCELLED);
		}

		if (event.getSource() == cancelPopup.getBtnCancel()) {
			cancelpanel.hide();
			cancelPopup.remark.setValue("");
		}
	}

	/*************************************************** React On PO ***************************************************/

	public void reactToPurchaseOrder() {
		final PurchaseOrderForm form = PurchaseOrderPresenter.initalize();
		final PurchaseOrder po = new PurchaseOrder();

		po.setPurchaseReqNo(model.getPurchaseReqNo());
		po.setPrDate(model.getPrDate());
		po.setRFQID(model.getReqforQuotationID());
		po.setRFQDate(model.getRfqDate());
		po.setLOIId(model.getCount());
		po.setLOIDate(model.getCreationDate());
		po.setProject(model.getProject());
		po.setRefOrderNO(model.getRefOrderNO());
		po.setBranch(model.getBranch());
		po.setEmployee(model.getEmployee());
		po.setApproverName(model.getApproverName());

		List<ProductDetails> loiProductList = model.getProductinfo();
		List<ProductDetailsPO> poProductList = new ArrayList<ProductDetailsPO>();
		/******** convert one list to another ****/
		for (ProductDetails temp : loiProductList) {
			ProductDetailsPO purch = new ProductDetailsPO();

			purch.setProdDate(temp.getProdDate());
			purch.setProductID(temp.getProductID());
			purch.setProductName(temp.getProductName());
			purch.setProductCode(temp.getProductCode());
			purch.setProductCategory(temp.getProductCategory());
			purch.setProdPrice(temp.getProdPrice());
			purch.setUnitOfmeasurement(temp.getUnitOfmeasurement());
			purch.setProductQuantity(temp.getProductQuantity());
			purch.setPrduct(temp.getPrduct());
			purch.setTax(temp.getServiceTax());
			purch.setVat(temp.getVat());

			// ////
			purch.setWarehouseName(temp.getWarehouseName());
			purch.setAvailableStock(temp.getAvailableStock());

			poProductList.add(purch);
		}
		po.setProductDetails(poProductList);
		// ArrayList<VendorDetails> vendorList =
		// setCommonVendors(model.getVendorinfo());
		// po.setVendorDetails(vendorList);

		po.setVendorDetails(model.getVendorinfo());

		/**
		 * Copying some more info of PR to LOI Date : 12-09-2016 By Anil Release
		 * : 31 Aug 2016 Project : PURCHASE ERROR SOLVING (NBHC)
		 */
		po.setPODate(new Date());
		po.setPOName(model.getTitle());
		po.setResponseDate(model.getResponseDate());
		po.setDeliveryDate(model.getDeliveryDate());
		// po.setExpectedClosureDate(model.getExpectedClosureDate());
		po.setHeader(model.getHeader());
		po.setFooter(model.getFooter());

		/**
		 * END
		 */

		// ///
		po.setAssignTo1(model.getAssignTo1());
		po.setAssignTo2(model.getAssignTo2());

		Timer t = new Timer() {
			@Override
			public void run() {

				form.setToNewState();
				form.updateView(po);
				form.ibreqforQuotation.setEnabled(false);
				form.dbRfqDate.setEnabled(false);
				form.getCbadds().setValue(false);
				;
				// form.setEnableFalse();
				form.getPurchaseRequisiteNo().setEnabled(false);
				form.getIbreqforQuotation().setEnabled(false);
				form.getIbLetterOfIntentID().setEnabled(false);
				form.getTbApporverName().setEnabled(true);
				form.getOblEmployee().setEnabled(true);
				form.getBranch().setEnabled(true);

				double total = form.productTablePO.calculateTotal();
				form.dbTotal.setValue(total);
				form.setAllTable();

				// /////////

				// form.getDbCreationDate().setEnabled(true);
				form.getDbExpdeliverydate().setEnabled(true);

				form.getTbpoName().setEnabled(true);
				form.getDbExpectedResposeDate().setEnabled(true);
				// form.getDbExpectedClosureDate().setEnabled(true);
				form.getHeader().setEnabled(true);
				form.getFooter().setEnabled(true);

				if (form.isWarehouseSelected()) {
					form.getDeliveryadd().clear();
					form.getDeliveryadd().setEnable(false);
					form.getCbadds().setEnabled(false);
				}

				// ////////

				// ////
				form.prodInfoComposite.setEnable(false);
				form.addproducts.setEnabled(false);
			}
		};
		t.schedule(5000);
	}

	/******************************************** react On Download ***********************************************/

	@Override
	public void reactOnDownload() {

		ArrayList<LetterOfIntent> LOIarray = new ArrayList<LetterOfIntent>();
		List<LetterOfIntent> list = form.getSearchpopupscreen().getSupertable()
				.getDataprovider().getList();

		LOIarray.addAll(list);

		csvservice.setLOI(LOIarray, new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed" + caught);

			}

			@Override
			public void onSuccess(Void result) {
				String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url = gwt + "csvservlet" + "?type=" + 45;
				Window.open(url, "test", "enabled");
			}
		});

	}

	/*********************************************** React On Status change *********************************************/

	public void ChangeStatus(final String status) {
		model.setStatus(status);
		async.save(model, new AsyncCallback<ReturnFromServer>() {
			@Override
			public void onFailure(Throwable caught) {
				form.showDialogMessage("An Unexpected Error occured !");
			}

			@Override
			public void onSuccess(ReturnFromServer result) {
				form.setToViewState();
				form.tbStatus.setText(status);
			}
		});
	}

	public ArrayList<VendorDetails> setCommonVendors(List<VendorDetails> list) {
		HashSet<VendorDetails> hset = new HashSet<VendorDetails>(list);
		ArrayList<VendorDetails> newvend = new ArrayList<VendorDetails>();
		newvend.addAll(hset);
		return newvend;
	}

	/********************************************* Cancellation Of Loi *******************************************/

	public void changeStatusOnLOI(final String status) {
		model.setStatus(status);
		model.setDescription("LOI Id = " + model.getCount() + " "
				+ "LOI status =" + form.getstatustextbox().getValue().trim()
				+ "\n" + "has been cancelled by " + LoginPresenter.loggedInUser
				+ " with remark " + "\n" + "Remark ="
				+ cancelPopup.getRemark().getValue().trim() + " "
				+ "Cancellation Date =" + new Date());
		async.save(model, new AsyncCallback<ReturnFromServer>() {
			@Override
			public void onFailure(Throwable caught) {
				form.showDialogMessage("An Unexpected Error occured !");
			}

			@Override
			public void onSuccess(ReturnFromServer result) {
				form.setToViewState();
				form.tbStatus.setText(status);
				form.taDescription.setValue(model.getDescription());
			}
		});
	}

	@Override
	public void onRowCountChange(RowCountChangeEvent event) {

		if (event.getSource().equals(form.productTableLOI.getTable())) {
			Double amount = 0d;

			for (ProductDetails prod : form.productTableLOI.getDataprovider()
					.getList()) {
				amount = amount + prod.getProdPrice();
			}

			String approverName = "";
			if (form.tbApporverName.getValue() != null) {
				approverName = form.tbApporverName.getValue();
			}
			AppUtility.getApproverListAsPerTotalAmount(form.tbApporverName,
					"Letter Of Intent(LOI)", approverName, amount);

		}
	}

	/**
	 * This method update vendor table when we delete product Date : 10-09-2016
	 * By Anil Release : 31 Aug 2016 Project : PURCHASE ERROR SOLVING (NBHC)
	 */

	public static void updateVendorTable() {

		// System.out.println("PRODUCT TBL SIZE : "+form.productTableLOI.getDataprovider().getList().size());
		if (form.productTableLOI.getDataprovider().getList().size() == 0) {
			form.vendorTableLOI.getDataprovider().getList().clear();
			form.vendorRateList.clear();
		} else {
			List<ProductDetails> prodList = form.productTableLOI.getValue();
			// System.out.println("GLOBAL VENDOR PRICE LIST SIZE BF : "+form.vendorRateList.size());
			List<PriceListDetails> remProdComVenLis = getRemainingProductVendorPriceList(
					prodList, form.vendorRateList);
			// System.out.println("GLOBAL VENDOR PRICE LIST SIZE AF : "+form.vendorRateList.size());
			// System.out.println("COMMON VENDOR PRICEL LIST SIZE : "+remProdComVenLis.size());
			// System.out.println("EARLIER VEN TBL LIST SIZE :"+form.vendorTableLOI.getDataprovider().getList().size());
			List<VendorDetails> vendorList = getVendorListOfRemainingProduct(
					form.vendorTableLOI.getDataprovider().getList(),
					remProdComVenLis);
			// System.out.println("NOW VEN TBL LIST SIZE : "+vendorList.size());
			// System.out.println("AFTER REMOVING : "+form.vendorTableLOI.getDataprovider().getList().size());
		}
	}

	/**
	 * This method returns the list of common vendor from global vendor list
	 * which is maintained at the time of product add.
	 * 
	 * Date : 10-09-2016 By Anil Release : 31 Aug 2016 Project : PURCHASE ERROR
	 * SOLVING (NBHC) I/P:existing vendor table list and global vendor list
	 * O/P:vendor list
	 */
	private static List<PriceListDetails> getRemainingProductVendorPriceList(
			List<ProductDetails> prodList,
			ArrayList<PriceListDetails> vendorRateList) {

		ArrayList<PriceListDetails> vendorPriceList = new ArrayList<PriceListDetails>();
		List<PriceListDetails> list = new ArrayList<PriceListDetails>();

		for (ProductDetails prodDet : prodList) {
			for (PriceListDetails priceLis : vendorRateList) {
				if (prodDet.getProductID() == priceLis.getProdID()) {
					vendorPriceList.add(priceLis);
				}
			}
		}

		form.vendorRateList.clear();
		form.vendorRateList.addAll(vendorPriceList);

		for (PriceListDetails pricelis : vendorPriceList) {
			if (list.size() == 0) {
				list.add(pricelis);
			} else {
				boolean isAlreadyAddedFlag = checkPriceListAlreadyAdded(list,
						pricelis);
				if (isAlreadyAddedFlag == false) {
					list.add(pricelis);
				}
			}
		}
		return list;
	}

	/**
	 * This method compare whether passed parameter 2nd is present in parameter
	 * 1st i.e. vendor price list Date 10-09-2016 By Anil Release : 31 Aug 2016
	 * Project: PURCHASE ERROR SOLVING(NBHC) I/P:vendor price list and vendor
	 * price O/P:True/False
	 */
	private static boolean checkPriceListAlreadyAdded(
			List<PriceListDetails> list, PriceListDetails pricelis) {
		for (PriceListDetails pricelist : list) {
			if (pricelis.getVendorID() == pricelist.getVendorID()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * This method returns common vendors of remaining products. Date 10-09-2016
	 * By Anil Release : 31 Aug 2016 Project: PURCHASE ERROR SOLVING(NBHC)
	 * I/P:existing vendor list and remaining products vendor list O/P:vendor
	 * list
	 */

	private static List<VendorDetails> getVendorListOfRemainingProduct(
			List<VendorDetails> vendorList, List<PriceListDetails> venRateLis) {
		System.out.println("REMAINING PRODUCTS VENDOR LIST");

		System.out.println("VEN LIS SIZE BF " + vendorList.size());
		for (PriceListDetails venRLis : venRateLis) {
			boolean vendorFlag = isVendorPresent(venRLis.getVendorID(),
					vendorList);
			if (vendorFlag == false) {
				List<PriceListDetails> vLis = new ArrayList<PriceListDetails>();
				vLis.add(venRLis);
				ArrayList<VendorDetails> vDeLis = form.getVendorList(vLis);
				vendorList.addAll(vDeLis);
			}
		}

		System.out.println("VEN LIS SIZE AF " + vendorList.size());
		return vendorList;
	}

	/**
	 * This method returns true if passed vendor id is present in given vendor
	 * list. Date : 10-09-2016 By Anil Release : 31 Aug 2016 Project: PURCHASE
	 * ERROR SOLVING (NBHC) I/P:Vendor id and Vendor List O/P:True/False
	 */
	private static boolean isVendorPresent(int vendorID,
			List<VendorDetails> vendorList) {
		for (VendorDetails venDet : vendorList) {
			if (vendorID == venDet.getVendorId()) {
				return true;
			}
		}
		return false;
	}
	
	private void setEmailPopUpData() {
		form.showWaitSymbol();

		String branchName = form.branch.getValue();
		String fromEmailId = AppUtility.getFromEmailAddress(branchName,model.getEmployee());
		List<VendorDetails> vendorlist = form.vendorTableLOI.getDataprovider().getList();
		List<ContactPersonIdentification> vendorDetailslist = AppUtility.getvenodrEmailId(vendorlist,true);
		System.out.println("vendorDetailslist "+vendorDetailslist);
		label : AppUtility.loadContactPersonlist(emailpopup.olbContactlistEmailId,"Vendor",-1,"","",vendorDetailslist);
		Screen screenName=(Screen) AppMemory.getAppMemory().currentScreen;
		Console.log("screenName "+screenName);
		AppUtility.makeLiveSmsTemplateConfig(emailpopup.oblTemplateName,AppConstants.PURCHASEMODULE.trim(),screenName);
		emailpopup.taFromEmailId.setValue(fromEmailId);
		emailpopup.smodel = model;
		form.hideWaitSymbol();
		
	}

}
