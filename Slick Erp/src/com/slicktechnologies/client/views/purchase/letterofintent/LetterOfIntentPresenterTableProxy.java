package com.slicktechnologies.client.views.purchase.letterofintent;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.purchase.letterofintent.LetterOfIntentPresenter.LoiPresenterTable;
import com.slicktechnologies.shared.common.businessprocesslayer.LetterOfIntent;

public class LetterOfIntentPresenterTableProxy extends LoiPresenterTable {

	public LetterOfIntentPresenterTableProxy() {
		super();
	}

	TextColumn<LetterOfIntent> rfqIDColumn;
	TextColumn<LetterOfIntent> LOIIDColumn;
	TextColumn<LetterOfIntent> RfqDateColumn;
	TextColumn<LetterOfIntent> dateColumn;
	TextColumn<LetterOfIntent> closureDateColumn;
	TextColumn<LetterOfIntent> responseDateColumn;
	TextColumn<LetterOfIntent> referenceOrderColumn;
	TextColumn<LetterOfIntent> employeeColumn;
	TextColumn<LetterOfIntent> loiGroupColumn;
	TextColumn<LetterOfIntent> loitypeColumn;
	TextColumn<LetterOfIntent> statusColumn;
	TextColumn<LetterOfIntent> ColumnPurchaseReqno;
	TextColumn<LetterOfIntent> ColumnRfqDate;
	TextColumn<LetterOfIntent> ColumnProject;
	TextColumn<LetterOfIntent> ColumnTitle;
	TextColumn<LetterOfIntent> branchColumn;

	@Override
	public void createTable() {
		addColumnLOIid();
		addColumnTitle();
		 addColumnLOIDate();
		addColumnLoiGroup();
		addColumnLoiType();
		addColumnBranch();
		addColumnProject();
		addColumnPurchaseReqno();
		addColumnEmployee();
		addColumnRfqID();
//		addColumnClosureDateColumn();
		 addColumnResponseDateColumn();
		addColumnReferenceOrderColumn();
		addColumnStatus();

	}

	public void addColumnSorting() {
		addSortinggetLOIID();
		addSortinggetTitle();
		addSortinggetLOIDate();
		addSortingColumnProject();
		addSortingColumnPurchaseReqno();
		addSortinggetClosureDate();
		addSortinggetResponseDate();
		addSortinggetReforderNo();
		addSortinggetEmployee();
		addSortinggetLOICategory();
		addSortinggetLOIType();
		addSortinggetRFQID();
		addSortinggetStatus();
		addSortinggetBranch();

	}

	/************************* sorting column *************************************************/

	public void addSortingColumnProject() {
		List<LetterOfIntent> list = getDataprovider().getList();
		columnSort = new ListHandler<LetterOfIntent>(list);
		columnSort.setComparator(ColumnProject,
				new Comparator<LetterOfIntent>() {
					@Override
					public int compare(LetterOfIntent e1, LetterOfIntent e2) {
						if (e1 != null && e2 != null) {
							if (e1.getProject() != null
									&& e2.getProject() != null) {
								return e1.getProject().compareTo(
										e2.getProject());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);

	}

	public void addSortingColumnPurchaseReqno() {
		List<LetterOfIntent> list = getDataprovider().getList();
		columnSort = new ListHandler<LetterOfIntent>(list);
		columnSort.setComparator(ColumnPurchaseReqno,
				new Comparator<LetterOfIntent>() {
					@Override
					public int compare(LetterOfIntent e1, LetterOfIntent e2) {
						if (e1 != null && e2 != null) {
							if (e1.getPurchaseReqNo() == e2.getPurchaseReqNo()) {
								return 0;
							}
							if (e1.getPurchaseReqNo() > e2.getPurchaseReqNo()) {
								return 1;
							} else {
								return -1;
							}
						} else {
							return 0;
						}
					}
				});
		table.addColumnSortHandler(columnSort);

	}
	protected void addSortinggetBranch()
	{
		List<LetterOfIntent> list=getDataprovider().getList();
		columnSort=new ListHandler<LetterOfIntent>(list);
		columnSort.setComparator(branchColumn, new Comparator<LetterOfIntent>()
				{
			@Override
			public int compare(LetterOfIntent e1,LetterOfIntent e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBranch()!=null && e2.getBranch()!=null){
						return e1.getBranch().compareTo(e2.getBranch());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	public void addSortinggetStatus() {
		List<LetterOfIntent> list = getDataprovider().getList();
		columnSort = new ListHandler<LetterOfIntent>(list);
		columnSort.setComparator(statusColumn,
				new Comparator<LetterOfIntent>() {
					@Override
					public int compare(LetterOfIntent e1, LetterOfIntent e2) {
						if (e1 != null && e2 != null) {
							if (e1.getStatus() != null
									&& e2.getStatus() != null) {
								return e1.getStatus().compareTo(e2.getStatus());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);

	}

	public void addSortinggetLOIType() {
		List<LetterOfIntent> list = getDataprovider().getList();
		columnSort = new ListHandler<LetterOfIntent>(list);
		columnSort.setComparator(loitypeColumn,
				new Comparator<LetterOfIntent>() {
					@Override
					public int compare(LetterOfIntent e1, LetterOfIntent e2) {
						if (e1 != null && e2 != null) {
							if (e1.getLoiType() != null
									&& e2.getLoiType() != null) {
								return e1.getLoiType().compareTo(
										e2.getLoiType());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);

	}

	public void addSortinggetLOICategory() {
		List<LetterOfIntent> list = getDataprovider().getList();
		columnSort = new ListHandler<LetterOfIntent>(list);
		columnSort.setComparator(loiGroupColumn,
				new Comparator<LetterOfIntent>() {
					@Override
					public int compare(LetterOfIntent e1, LetterOfIntent e2) {
						if (e1 != null && e2 != null) {
							if (e1.getLoiGroup() != null
									&& e2.getLoiGroup() != null) {
								return e1.getLoiGroup().compareTo(
										e2.getLoiGroup());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);

	}

	public void addSortinggetEmployee() {
		List<LetterOfIntent> list = getDataprovider().getList();
		columnSort = new ListHandler<LetterOfIntent>(list);
		columnSort.setComparator(employeeColumn,
				new Comparator<LetterOfIntent>() {
					@Override
					public int compare(LetterOfIntent e1, LetterOfIntent e2) {
						if (e1 != null && e2 != null) {
							if (e1.getEmployee() != null
									&& e2.getEmployee() != null) {
								return e1.getEmployee().compareTo(
										e2.getEmployee());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}
	public void addSortinggetTitle() {
		List<LetterOfIntent> list = getDataprovider().getList();
		columnSort = new ListHandler<LetterOfIntent>(list);
		columnSort.setComparator(ColumnTitle,new Comparator<LetterOfIntent>() {
					@Override
					public int compare(LetterOfIntent e1, LetterOfIntent e2) {
						if (e1 != null && e2 != null) {
							if (e1.getTitle() != null&& e2.getTitle() != null) {
								return e1.getTitle().compareTo(e2.getTitle());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	public void addSortinggetReforderNo() {
		List<LetterOfIntent> list = getDataprovider().getList();
		columnSort = new ListHandler<LetterOfIntent>(list);
		columnSort.setComparator(referenceOrderColumn,
				new Comparator<LetterOfIntent>() {
					@Override
					public int compare(LetterOfIntent e1, LetterOfIntent e2) {
						if (e1 != null && e2 != null) {
							if (e1.getRefOrderNO()!= null&& e2.getRefOrderNO() != null) {
								return e1.getRefOrderNO().compareTo(e2.getRefOrderNO());
							}if(e1.getRefOrderNO().equals("")&& e2.getRefOrderNO().equals("")){
								return 0;
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);

	}

	public void addSortinggetResponseDate() {
		List<LetterOfIntent> list = getDataprovider().getList();
		columnSort = new ListHandler<LetterOfIntent>(list);
		columnSort.setComparator(responseDateColumn,
				new Comparator<LetterOfIntent>() {
					@Override
					public int compare(LetterOfIntent e1, LetterOfIntent e2) {
						if (e1 != null && e2 != null) {
							if (e1.getResponseDate() != null
									&& e2.getResponseDate() != null) {
								return e1.getResponseDate().compareTo(
										e2.getResponseDate());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);

	}

	public void addSortinggetClosureDate() {
		List<LetterOfIntent> list = getDataprovider().getList();
		columnSort = new ListHandler<LetterOfIntent>(list);
		columnSort.setComparator(closureDateColumn,
				new Comparator<LetterOfIntent>() {
					@Override
					public int compare(LetterOfIntent e1, LetterOfIntent e2) {
						if (e1 != null && e2 != null) {
							if (e1.getExpectedClosureDate() != null
									&& e2.getExpectedClosureDate() != null) {
								return e1.getExpectedClosureDate().compareTo(
										e2.getExpectedClosureDate());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);

	}

	public void addSortinggetLOIDate() {
		List<LetterOfIntent> list = getDataprovider().getList();
		columnSort = new ListHandler<LetterOfIntent>(list);
		columnSort.setComparator(dateColumn, new Comparator<LetterOfIntent>() {
			@Override
			public int compare(LetterOfIntent e1, LetterOfIntent e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCreationDate() != null
							&& e2.getCreationDate() != null) {
						return e1.getCreationDate().compareTo(
								e2.getCreationDate());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);

	}

	public void addSortinggetLOIID() {
		List<LetterOfIntent> list = getDataprovider().getList();
		columnSort = new ListHandler<LetterOfIntent>(list);
		columnSort.setComparator(LOIIDColumn, new Comparator<LetterOfIntent>() {
			@Override
			public int compare(LetterOfIntent e1, LetterOfIntent e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCount() == e2.getCount()) {
						return 0;
					}
					if (e1.getCount() > e2.getCount()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);

	}

	public void addSortinggetRFQID() {
		List<LetterOfIntent> list = getDataprovider().getList();
		columnSort = new ListHandler<LetterOfIntent>(list);
		columnSort.setComparator(rfqIDColumn, new Comparator<LetterOfIntent>() {
			@Override
			public int compare(LetterOfIntent e1, LetterOfIntent e2) {
				if (e1 != null && e2 != null) {
					if (e1.getReqforQuotationID() == e2.getReqforQuotationID()) {
						return 0;
					}
					if (e1.getReqforQuotationID() > e2.getReqforQuotationID()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);

	}

	/***************************************************************************/

	public void addColumnTitle() {
		ColumnTitle = new TextColumn<LetterOfIntent>() {

			@Override
			public String getValue(LetterOfIntent object) {
				return object.getTitle() + "";
			}
		};
		table.addColumn(ColumnTitle, "Title");
		table.setColumnWidth(ColumnTitle, 100, Unit.PX);
		ColumnTitle.setSortable(true);
	}

	public void addColumnPurchaseReqno() {
		ColumnPurchaseReqno = new TextColumn<LetterOfIntent>() {

			@Override
			public String getValue(LetterOfIntent object) {
				// TODO Auto-generated method stub
				return object.getPurchaseReqNo() + "";
			}
		};
		table.addColumn(ColumnPurchaseReqno, "PR No.");
		table.setColumnWidth(ColumnPurchaseReqno, 90, Unit.PX);
		ColumnPurchaseReqno.setSortable(true);
	}

	public void addColumnProject() {
		ColumnProject = new TextColumn<LetterOfIntent>() {

			@Override
			public String getValue(LetterOfIntent object) {
				// TODO Auto-generated method stub
				return object.getProject();
			}
		};
		table.addColumn(ColumnProject, "Project");
		table.setColumnWidth(ColumnProject, 100, Unit.PX);
		ColumnProject.setSortable(true);
	}

	public void addColumnLOIDate() {
		dateColumn = new TextColumn<LetterOfIntent>() {

			@Override
			public String getValue(LetterOfIntent object) {
				// TODO Auto-generated method stub
				if (object.getLoiDate() != null)
					return AppUtility.parseDate(object.getLoiDate());
				else
					return "N/A";
			}
		};
		table.addColumn(dateColumn, "Creation Date");
		table.setColumnWidth(dateColumn, 100, Unit.PX);
		dateColumn.setSortable(true);

	}

	public void addColumnStatus() {
		statusColumn = new TextColumn<LetterOfIntent>() {

			@Override
			public String getValue(LetterOfIntent object) {
				// TODO Auto-generated method stub
				return object.getStatus();
			}
		};
		table.addColumn(statusColumn, "Status");
		table.setColumnWidth(statusColumn, 90, Unit.PX);
		statusColumn.setSortable(true);
	}

	public void addColumnLoiType() {
		loitypeColumn = new TextColumn<LetterOfIntent>() {

			@Override
			public String getValue(LetterOfIntent object) {
				// TODO Auto-generated method stub
				return object.getLoiType();
			}
		};
		table.addColumn(loitypeColumn, "Type");
		table.setColumnWidth(loitypeColumn, 100, Unit.PX);
		loitypeColumn.setSortable(true);
	}

	public void addColumnBranch()
	{
		branchColumn=new TextColumn<LetterOfIntent>() {

			@Override
			public String getValue(LetterOfIntent object) {
				return object.getBranch();
			}
		};
		
		table.addColumn(branchColumn,"Branch");
		table.setColumnWidth(branchColumn, 100, Unit.PX);
		branchColumn.setSortable(true);
	}
	
	public void addColumnLoiGroup() {
		loiGroupColumn = new TextColumn<LetterOfIntent>() {

			@Override
			public String getValue(LetterOfIntent object) {
				// TODO Auto-generated method stub
				return object.getLoiGroup();
			}
		};
		table.addColumn(loiGroupColumn, "Category");
		table.setColumnWidth(loiGroupColumn, 100, Unit.PX);
		loiGroupColumn.setSortable(true);
	}

	public void addColumnEmployee() {
		employeeColumn = new TextColumn<LetterOfIntent>() {

			@Override
			public String getValue(LetterOfIntent object) {
				return object.getEmployee();
			}
		};
		table.addColumn(employeeColumn, "Pur Eng.");
		table.setColumnWidth(employeeColumn, 100, Unit.PX);
		employeeColumn.setSortable(true);
	}

	public void addColumnReferenceOrderColumn() {
		referenceOrderColumn = new TextColumn<LetterOfIntent>() {

			@Override
			public String getValue(LetterOfIntent object) {
				if(object.getRefOrderNO()!=null){
					return object.getRefOrderNO() + "";
				}
				else{
					return "";
				}
			}
		};
		table.addColumn(referenceOrderColumn, "RefOrdNo.");
		table.setColumnWidth(referenceOrderColumn, 100, Unit.PX);
		referenceOrderColumn.setSortable(true);
	}

	public void addColumnResponseDateColumn() {
		responseDateColumn = new TextColumn<LetterOfIntent>() {

			@Override
			public String getValue(LetterOfIntent object) {

				if (object.getResponseDate() != null)
					return AppUtility.parseDate(object.getResponseDate());
				else
					return " ";
			}
		};
		table.addColumn(responseDateColumn, "Resp. Date");
		table.setColumnWidth(responseDateColumn, 100, Unit.PX);
		responseDateColumn.setSortable(true);
	}

	public void addColumnClosureDateColumn() {
		closureDateColumn = new TextColumn<LetterOfIntent>() {

			@Override
			public String getValue(LetterOfIntent object) {
				// TODO Auto-generated method stub
				if (object.getExpectedClosureDate() != null)
					return AppUtility
							.parseDate(object.getExpectedClosureDate());
				else
					return "N/A";
			}
		};
		table.addColumn(closureDateColumn, "Closure Date");
		table.setColumnWidth(closureDateColumn, 100, Unit.PX);
		closureDateColumn.setSortable(true);
	}

	public void addColumnRfqDate() {
		RfqDateColumn = new TextColumn<LetterOfIntent>() {

			@Override
			public String getValue(LetterOfIntent object) {

				if (object.getResponseDate() != null)
					return AppUtility.parseDate(object.getResponseDate());
				else
					return "N/A";
			}
		};
		table.addColumn(RfqDateColumn, "RFQ Date");
		table.setColumnWidth(RfqDateColumn, 100, Unit.PX);
		RfqDateColumn.setSortable(true);
	}

	public void addColumnLOIid() {
		LOIIDColumn = new TextColumn<LetterOfIntent>() {

			@Override
			public String getValue(LetterOfIntent object) {
				// TODO Auto-generated method stub
				return object.getCount() + "";
			}
		};
		table.addColumn(LOIIDColumn, "ID");
		table.setColumnWidth(LOIIDColumn, 90, Unit.PX);
		LOIIDColumn.setSortable(true);
	}

	public void addColumnRfqID() {
		rfqIDColumn = new TextColumn<LetterOfIntent>() {

			@Override
			public String getValue(LetterOfIntent object) {
				// TODO Auto-generated method stub
				return object.getReqforQuotationID() + "";
			}
		};
		table.addColumn(rfqIDColumn, "RFQ ID");
		table.setColumnWidth(rfqIDColumn, 90, Unit.PX);
		rfqIDColumn.setSortable(true);
	}
}
