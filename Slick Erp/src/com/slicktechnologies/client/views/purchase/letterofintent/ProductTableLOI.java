package com.slicktechnologies.client.views.purchase.letterofintent;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.code.p.gwtchismes.client.GWTCGlassPanel;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.SelectionCell;
import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetails;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;
import com.slicktechnologies.shared.common.inventory.WareHouse;

public class ProductTableLOI extends SuperTable<ProductDetails> {

	NumberFormat nf = NumberFormat.getFormat("0.00");
	TextColumn<ProductDetails> columnProductID;
	TextColumn<ProductDetails> columnProductName;
	TextColumn<ProductDetails> columnProductCode;
	TextColumn<ProductDetails> columnProductCategory;
	TextColumn<ProductDetails> columnProductPriceView;
	Column<ProductDetails, String> columnProductPrice;
	TextColumn<ProductDetails> columnProductQuantityView;
	Column<ProductDetails, String> columnProductQuantity;
	TextColumn<ProductDetails> columnunitOfmeasurement;
	Column<ProductDetails, String> deleteColumn;
	
	
	
	/**
	 * warehouse ,exp. delivery date and avilable stock coloumn is added.
	 * Date : 29-09-2016 By Anil
	 * Release :30 Sept 2016
	 * Project: PURCHASE MODIFICATION (NBHC)
	 */
	TextColumn<ProductDetails> warehouseColumn;
	Column<ProductDetails,String> warehouseListColumn;
	TextColumn<ProductDetails> expDeliveryDateColumn;
	Column<ProductDetails,Date> editableExpDeliveryDateColumn;
	TextColumn<ProductDetails> availableStockColumn;
	ArrayList<String> warehouseList;
	ArrayList<WareHouse> warehouseEntityList;
	protected GWTCGlassPanel glassPanel;
	public static boolean isValidWarehouse=true;
	
	/**
	 * End
	 */

	/**
	 * Date 12-06-2017
	 * Total Column And Warehouse Address column
	 */
	TextColumn<ProductDetails> totalColumn;
	TextColumn<ProductDetails> warehouseAddressColumn; 
	/**
	 * ends here
	 */
	
	
	@Override
	public void createTable() {
		addColumnProductID();
		addColumnProductName();
		addColumnProductCode();
		addColumnProductCategory();
		addColumnProductPrice();
		addColumnProductQuantity();
		addColumnUnit();
		addColumnTotal();

		addEditableExpDeliveryDateColoumn();
		retrieveWarehouseNameList();
		
//		createColumndeleteColumn();
//		addFieldUpdater();

	}

	public void addeditColumn() {
		addColumnProductID();
		addColumnProductName();
		addColumnProductCode();
		addColumnProductCategory();
		addColumnProductPrice();
		addColumnProductQuantity();
		addColumnUnit();
		addColumnTotal();

		
		addEditableExpDeliveryDateColoumn();
		retrieveWarehouseNameList();
		
//		createColumndeleteColumn();
//		addFieldUpdater();

	}

	public void addViewColumn() {
		addColumnProductID();
		addColumnProductName();
		addColumnProductCode();
		addColumnProductCategory();
		addColumnProductPriceView();
		addColumnProductQuantityView();
		addColumnUnit();
		addColumnTotal();

		
		addExpDeliveryDateColoumn();
		addWarehouseColoumn();
		addAvailableStockColoumn();
		addWarehouseAddressColumn();


	}
	
	
	
	
	/**
	 * This method loads all the active warehouse in selection list.
	 * purpose of loading warehouse is that product should be delivered in that warehouse.
	 * Date:27-09-2016 By Anil
	 * Release :30 Sept 2016 
	 * Project : PURCHASE MODIFICATION(NBHC) 
	 */
	
	public void retrieveWarehouseNameList(){
		final GenricServiceAsync service = GWT.create(GenricService.class);
		MyQuerry query = new MyQuerry();
		System.out.println("Company Id :: "+UserConfiguration.getCompanyId());
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(UserConfiguration.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);
		
		query.setFilters(filtervec);
		query.setQuerryObject(new WareHouse());
		
		service.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				warehouseList=new ArrayList<String>();
				warehouseEntityList=new ArrayList<WareHouse>();
				warehouseList.add("");
				if(result.size()!=0){
					for(SuperModel model:result){
						WareHouse entity=(WareHouse) model;
						warehouseList.add(entity.getBusinessUnitName());
						warehouseEntityList.add(entity);
					}
					Collections.sort(warehouseList);
				}
				
				
				addEditableWarehouseColoumn();
				addAvailableStockColoumn();
				addWarehouseAddressColumn();

				createColumndeleteColumn();
				addFieldUpdater();
			}
			
			@Override
			public void onFailure(Throwable caught) {
				
			}
		});
	}
	
	
	/**
	 * Date 12-06-2017
	 * Total Column And Warehouse Address column
	 */
	
	private void addWarehouseAddressColumn() {

		warehouseAddressColumn = new TextColumn<ProductDetails>() {
			
			@Override
			public String getValue(ProductDetails object) {
				return object.getWarehouseAddress();
			}
		};
		table.addColumn(warehouseAddressColumn, "Warehouse Address");
		table.setColumnWidth(warehouseAddressColumn, 200,Unit.PX);
	}
	
	private void addColumnTotal() {
		totalColumn = new TextColumn<ProductDetails>() {
			
			@Override
			public String getValue(ProductDetails object) {
				double total = object.getProdPrice()*object.getProductQuantity();
				object.setTotal(total);
				return nf.format(object.getTotal());
			}
		};
		table.addColumn(totalColumn,"Total");
		table.setColumnWidth(totalColumn, 90,Unit.PX);
	}
	
	/**
	 * ends here
	 */
	
	public void addWarehouseColoumn(){
		warehouseColumn=new TextColumn<ProductDetails>() {
			@Override
			public String getValue(ProductDetails object) {
				if(object.getWarehouseName()!=null){
					return object.getWarehouseName();
				}
				return "";
			}
		};
		table.addColumn(warehouseColumn, "#Warehouse");
	}
	
	public void addEditableWarehouseColoumn(){
		SelectionCell selectionCell= new SelectionCell(warehouseList);
		warehouseListColumn=new Column<ProductDetails, String>(selectionCell) {
			
			@Override
			public String getCellStyleNames(Context context, ProductDetails object) {
				 
				if(object.getWarehouseName()==null){
					 return "red";
				 }
				return "";
			}
			@Override
			public String getValue(ProductDetails object) {
				if(object.getWarehouseName()!=null){
					return object.getWarehouseName();
				}
				return "";
			}
		};
		table.addColumn(warehouseListColumn, "#Warehouse");
		
		
	}
	
	public void addExpDeliveryDateColoumn(){
		expDeliveryDateColumn=new TextColumn<ProductDetails>() {
			@Override
			public String getValue(ProductDetails object) {
				if(object.getProdDate()!=null){
					return AppUtility.parseDate(object.getProdDate());
				}
				return "";
			}
		};
		table.addColumn(expDeliveryDateColumn, "#Delivery Date");
	}
	public void addEditableExpDeliveryDateColoumn(){
		final DateTimeFormat fmt = DateTimeFormat.getFormat("dd/MM/yyyy");
		DatePickerCell date= new DatePickerCell(fmt);
		editableExpDeliveryDateColumn=new Column<ProductDetails,Date>(date) {
			@Override
			public Date getValue(ProductDetails object) {
				if(object.getProdDate()!=null){
					return fmt.parse(fmt.format(object.getProdDate()));
				}else{
					object.setProdDate(new Date());
					return fmt.parse(fmt.format(new Date()));
				}
			}
		};
		table.addColumn(editableExpDeliveryDateColumn, "#Delivery Date");
	}
	
	public void addAvailableStockColoumn(){
		availableStockColumn=new TextColumn<ProductDetails>() {
			@Override
			public String getValue(ProductDetails object) {
//				if(object.getAvailableStock()!=0){
					return object.getAvailableStock()+"";
//				}
//				return "";
			}
		};
		table.addColumn(availableStockColumn, "Available Stock");
	}
	
	private void setFieldUpdatorOnWarehouse(){
		warehouseListColumn.setFieldUpdater(new FieldUpdater<ProductDetails, String>() {
			@Override
			public void update(int index, ProductDetails object, String value) {
				
				if(validWarehouseSelection(object.getProductID(), value)){
					isValidWarehouse=true;
				}else{
					isValidWarehouse=false;
				}
				setAvailableStock(index,object,value);
				
				//Below Method added by vijay for setting warehouse address on 12-06-2017
				setwarehouseAddress(index,object,value);
			}
		});
	}
	
	/**
	 * Date 12-06-2017
	 * added by vijay
	 * for setting warehouse address
	 */
	private void setwarehouseAddress(int index, ProductDetails object, String warehouseName) {
		
		for(int i=0;i<warehouseEntityList.size();i++){
			if(warehouseEntityList.get(i).getBusinessUnitName().equals(warehouseName)){
				object.setWarehouseAddress(warehouseEntityList.get(i).getAddress().getCompleteAddress());
				break;
			}
		}
		table.redrawRow(index);
		
	}
	/**
	 * ends here
	 */
	
	
	private void setFieldUpdatorOnDeliveryDate(){
		editableExpDeliveryDateColumn.setFieldUpdater(new FieldUpdater<ProductDetails, Date>() {
			@Override
			public void update(int index, ProductDetails object, Date value) {
				object.setProdDate(value);
				table.redrawRow(index);				
			}
		});
	}
	
	
	/**
	 * 
	 * @param productID
	 * @param value -warehouse name
	 * @return true/false
	 * 
	 * This method checks whether selected warehouse for given product id is already added.
	 * if added then return false else true.
	 * Date : 27-09-2016 By Anil
	 * Release : 30 Sept 2016
	 * Project: PURCHASE MODIFICATION(NBHC)
	 */
	private boolean validWarehouseSelection(int productID, String value) {
		List<ProductDetails> list=getDataprovider().getList();
		for(ProductDetails prod:list){
			if(productID==prod.getProductID()&&value.trim().equals(prod.getWarehouseName().trim())){
				GWTCAlert alert =new GWTCAlert();
				alert.alert("Warehouse is already selected for product !");
				return false;
			}
		}
		return true;
	}
	
	
	/**
	 * 
	 * @param index -selected row
	 * @param object - selected row data
	 * @param value - updated warehouse name
	 * 
	 * This method return the available Stock of product in selected warehouse.
	 * Date :27-09-2016 By Anil
	 * Release : 30 Sept 2016
	 * Project : PURCHASE MODIFICATION(NBHC)
	 */
	
	private void setAvailableStock(final int index, final ProductDetails object, final String value) {
		final GenricServiceAsync service = GWT.create(GenricService.class);
		MyQuerry query = new MyQuerry();
		System.out.println("Company Id :: "+UserConfiguration.getCompanyId()+" Product Id :: "+object.getProductID()+" Warehouse  :: "+value);
		
		
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(UserConfiguration.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("productinfo.prodID");
		filter.setIntValue(object.getProductID());
		filtervec.add(filter);
		
//		filter = new Filter();
//		filter.setQuerryString("details.warehousename");
//		filter.setStringValue(value.trim());
//		filtervec.add(filter);
		
		query.setFilters(filtervec);
		query.setQuerryObject(new ProductInventoryView());
		showWaitSymbol();
		service.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				hideWaitSymbol();
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				hideWaitSymbol();
				System.out.println("RESULT SIZE : "+result.size());
				double avilableStock=0;
				if(result.size()!=0){
					ProductInventoryView entity=(ProductInventoryView) result.get(0);
					for(ProductInventoryViewDetails prod:entity.getDetails()){
						if(prod.getWarehousename().trim().equals(value.trim())){
							avilableStock=avilableStock+prod.getAvailableqty();
						}
					}
				}
				object.setAvailableStock(avilableStock);
				object.setWarehouseName(value);
				System.out.println("ROW INDEX "+index+ " WH "+value );
				if(isValidWarehouse){
					AppUtility.ApplyWhiteColoronRow(ProductTableLOI.this, index);
				}else{
					AppUtility.ApplyRedColoronRow(ProductTableLOI.this, index);
				}
				
				
				if(isValidWarehouse){
					List<ProductDetails> list=getDataprovider().getList();
					Integer rowIndex=getRowCountOfWarehouse(list);
					if(rowIndex!=null){
						isValidWarehouse=false;
						AppUtility.ApplyRedColoronRow(ProductTableLOI.this, rowIndex);
					}else{
						isValidWarehouse=true;
						AppUtility.ApplyWhiteColoronRow(ProductTableLOI.this, index);
					}
				}
				
				
				table.redraw();
				System.out.println("ROW UPDATED "+object.getWarehouseName()+" - "+object.getAvailableStock());
				
			}

			
		});
	}
	
	/**
	 * This method returns row index whose which has duplicate warehouse name for perticular product.
	 * Date : 28-09-2016 By Anil
	 * Release : 30 Sept 2016 
	 * Project : PURCHASE MODIFICATION(NBHC)
	 * 
	 */

	private Integer getRowCountOfWarehouse(List<ProductDetails> list) {
		for(ProductDetails prod1:list){
			int count=0;
			int index=0;
			for(ProductDetails prod2:list){
				if(prod1.getProductID()==prod2.getProductID()&&prod1.getWarehouseName().equals(prod2.getWarehouseName())){
					count++;
				}
				if(count==2){
					return index;
				}
				index++;
			}
		}
		return null;
	}
	

	/******************************************* Product Column View *************************************************/

	public void addColumnProductID() {
		columnProductID = new TextColumn<ProductDetails>() {

			@Override
			public String getValue(ProductDetails object) {
				return object.getProductID() + "";
			}
		};
		table.addColumn(columnProductID, "Product ID");
	}

	public void addColumnProductName() {
		columnProductName = new TextColumn<ProductDetails>() {

			@Override
			public String getValue(ProductDetails object) {
				return object.getProductName();
			}
		};
		table.addColumn(columnProductName, "Product Name");
	}

	public void addColumnProductCode() {
		columnProductCode = new TextColumn<ProductDetails>() {

			@Override
			public String getValue(ProductDetails object) {
				return object.getProductCode();
			}
		};
		table.addColumn(columnProductCode, "Product Code");
	}

	public void addColumnProductCategory() {
		columnProductCategory = new TextColumn<ProductDetails>() {

			@Override
			public String getValue(ProductDetails object) {
				return object.getProductCategory();
			}
		};
		table.addColumn(columnProductCategory, "Product Category");
	}

	// Read Only Product Price
	private void addColumnProductPriceView() {
		columnProductPriceView = new TextColumn<ProductDetails>() {

			@Override
			public String getValue(ProductDetails object) {
				return object.getProdPrice() + "";
			}
		};
		table.addColumn(columnProductPriceView, "#Product Price");
	}

	// Editable Product Price
	public void addColumnProductPrice() {
		EditTextCell prodprice = new EditTextCell();
		columnProductPrice = new Column<ProductDetails, String>(prodprice) {

			@Override
			public String getValue(ProductDetails object) {
				return object.getProdPrice() + "";
			}
		};
		table.addColumn(columnProductPrice, "#Product Price");
	}

	// Editable Product Quantity
	public void addColumnProductQuantity() {
		EditTextCell editCell = new EditTextCell();
		columnProductQuantity = new Column<ProductDetails, String>(editCell) {

			@Override
			public String getValue(ProductDetails object) {
				return nf.format(object.getProductQuantity());
			}
		};
		table.addColumn(columnProductQuantity, "#Product Quantity");
	}

	// Read Only Product Quantity
	private void addColumnProductQuantityView() {
		columnProductQuantityView = new TextColumn<ProductDetails>() {

			@Override
			public String getValue(ProductDetails object) {
				return nf.format(object.getProductQuantity());
			}
		};
		table.addColumn(columnProductQuantityView, "#Product Quantity");
	}

	public void addColumnUnit() {
		columnunitOfmeasurement = new TextColumn<ProductDetails>() {

			@Override
			public String getValue(ProductDetails object) {
				return object.getUnitOfmeasurement();
			}
		};
		table.addColumn(columnunitOfmeasurement, "UOM");
	}

	public void createColumndeleteColumn() {
		ButtonCell btnCell = new ButtonCell();
		deleteColumn = new Column<ProductDetails, String>(btnCell) {
			@Override
			public String getValue(ProductDetails object) {
				return "Delete";
			}
		};
		table.addColumn(deleteColumn, "Delete");
	}

	/***************************************** Update Product Column Method ************************************** */

	protected void createFieldUpdaterprodQtyColumn() {
		columnProductQuantity
				.setFieldUpdater(new FieldUpdater<ProductDetails, String>() {
					@Override
					public void update(int index, ProductDetails object,
							String value) {
						try {
							double val1 = Double.parseDouble(value.trim());
							object.setProductQuantity(val1);
							
							// Date 12 jun 2017 for total added by vijay
							object.setTotal(val1*object.getProdPrice());
							
							RowCountChangeEvent.fire(table, getDataprovider()
									.getList().size(), true);
							System.out.println("Field Updater Called");
						} catch (NumberFormatException e) {
						}
						table.redrawRow(index);
					}
				});
	}

	public void columnEditPrice() {
		columnProductPrice
				.setFieldUpdater(new FieldUpdater<ProductDetails, String>() {
					@Override
					public void update(int index, ProductDetails object,
							String value) {
						try {
							Integer val1 = Integer.parseInt(value.trim());
							object.setProdPrice(val1);
							
							// Date 12 jun 2017 for total added by vijay
							object.setTotal(val1*object.getProductQuantity());
							
							RowCountChangeEvent.fire(table, getDataprovider()
									.getList().size(), true);
						} catch (NumberFormatException e) {
						}
						table.redrawRow(index);
					}
				});
	}

	public void createFieldUpdaterDelete() {
		deleteColumn.setFieldUpdater(new FieldUpdater<ProductDetails, String>() {
			@Override
			public void update(int index, ProductDetails object,String value) {
//				System.out.println("PRODUCT LIST SIZE BEFOR : "+getDataprovider().getList().size());
//				getDataprovider().getList().remove(object);
//				table.redrawRow(index);
//				System.out.println("PRODUCT LIST SIZE AFTER : "+getDataprovider().getList().size());
//				LetterOfIntentPresenter.updateVendorTable();
				
				
				boolean updateVenTblFlag=checkSameProductExist(object.getProductID(),getDataprovider().getList());
				getDataprovider().getList().remove(object);
				table.redrawRow(index);
				if(updateVenTblFlag){
					LetterOfIntentPresenter.updateVendorTable();
				}
			}
			
		});
	}
	
	/**
	 * This method checks in product list ,whether the product which you want to delete has 2 or more entry ,
	 * in that case you dont need to update the vendor table.
	 * Date : 28-09-2016 By Anil 
	 * Release : 30 Sept 2016 
	 * Project : PURCHASE MODIFICATION(NBHC)
	 * I/P : Product Id and product list
	 * O/P: return false if product has multiple entry else true
	 */
	private boolean checkSameProductExist(int productID,List<ProductDetails> list) {
		int count=0;
		for(ProductDetails obj:list){
			if(obj.getProductID()==productID){
				count++;
			}
			if(count==2){
				return false;
			}
		}
		return true;
	}
	
	/**
	 * END
	 */

	@Override
	protected void initializekeyprovider() {

	}

	@Override
	public void addFieldUpdater() {
		columnEditPrice();
		createFieldUpdaterprodQtyColumn();
		createFieldUpdaterDelete();
		
		setFieldUpdatorOnDeliveryDate();
		setFieldUpdatorOnWarehouse();
	}

	@Override
	public void setEnable(boolean state) {
		int tablecolcount = this.table.getColumnCount();
		for (int i = tablecolcount - 1; i > -1; i--)
			table.removeColumn(i);
		if (state == true)
			addeditColumn();
		if (state == false)
			addViewColumn();
	}

	@Override
	public void applyStyle() {
	}
	
	public void showWaitSymbol() {
		glassPanel = new GWTCGlassPanel();
		glassPanel.show();
		
		
	}
	public void hideWaitSymbol() {
	   glassPanel.hide();
		
	}

}
