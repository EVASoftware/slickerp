package com.slicktechnologies.client.views.purchase.letterofintent;

import java.awt.Label;



import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.MouseDownHandler;
import com.google.gwt.event.dom.client.MouseMoveHandler;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.event.dom.client.MouseUpHandler;
import com.google.gwt.event.dom.client.MouseWheelHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.DialogBox.Caption;
import com.google.gwt.user.client.ui.DockPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.LayoutPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public class Newclass{



public static class MypopUp extends DialogBox 
{
	
	private final static MyCaptionImpl impl = new MyCaptionImpl();
//	private final DialogBox panel = new DialogBox(impl);
	private final LayoutPanel lp = new LayoutPanel();
	private final DockPanel dp = new DockPanel();

	public MypopUp(Image img) {
		this("50em", "20em");

		dp.add(img, DockPanel.WEST);
		dp.setCellVerticalAlignment(img, VerticalPanel.ALIGN_MIDDLE);
	}


	/*public MypopUp() {
		super(true);
		
		Button ok = new Button("OK");
        ok.addClickHandler(new ClickHandler() {
         
		@Override
		public void onClick(ClickEvent event) {
		
			MypopUp.this.hide();
			
		}
        });
		
	}*/
	
	public MypopUp(String width, String height) {
		
		super(impl);
		setWidget(new com.google.gwt.user.client.ui.Label("Box"));
		this.setAnimationEnabled(true);
		this.setGlassEnabled(true);
		
		getElement().setAttribute("background", "#FFFFFF");
		// this.setStylePrimaryName("backgroundwhite");
		lp.setSize(width, height);
		dp.add(lp, DockPanel.EAST);
		this.add(dp);
	}

	
	public void setText(String text) {
		setWidget(new com.google.gwt.user.client.ui.Label("Box"));
		com.google.gwt.user.client.ui.Label lbHeader = new com.google.gwt.user.client.ui.Label();
		lbHeader.setText(text);
		DOM.setStyleAttribute(lbHeader.getElement(), "fontFamily", "arial");
		DOM.setStyleAttribute(lbHeader.getElement(), "fontSize", "20px");
		DOM.setStyleAttribute(lbHeader.getElement(), "color", "#FFFFFF");
		impl.setText(lbHeader.getText());
	}

	public void setSize(String width, String height) {
		lp.setSize(width, height);
	}

	public void addWidget(Widget w) {
		// Clear the previous widgets before adding the new ones.
		for (int i = 0; i < lp.getWidgetCount(); i++) {
			lp.remove(i);
		}
		lp.add(w);
		

	}


	
	


private static class MyCaptionImpl extends Composite implements Caption {
	private DockPanel hp = new DockPanel();
	private HorizontalPanel empty = new HorizontalPanel();
	private DialogBox dlgBox;
	
	private com.google.gwt.user.client.ui.Label lbTitle=new com.google.gwt.user.client.ui.Label();

	public MyCaptionImpl() {
		super();

		hp = new DockPanel();
		hp.setWidth("100%");
		final Image img = new Image("delete_off.png");
		hp.add(empty, DockPanel.WEST);
		empty.add(lbTitle);
		hp.add(img, DockPanel.EAST);
		hp.setCellWidth(empty, "90%");
		hp.setSpacing(10);
		hp.setCellWidth(img, "10%");
		hp.setCellHorizontalAlignment(img, HorizontalPanel.ALIGN_RIGHT);
		DOM.setStyleAttribute(hp.getElement(), "background", "#0066CC");
		//hp.setStylePrimaryName("backgroundchildgrid");
		DOM.setStyleAttribute(lbTitle.getElement(), "color", "#FFFFFF");
		
		initWidget(hp);
	}

	@Override
	public HandlerRegistration addMouseDownHandler(MouseDownHandler handler) {
		// TODO Auto-generated method stub
		return addMouseDownHandler(handler);
	}

	@Override
	public void fireEvent(GwtEvent<?> event) {
		super.fireEvent(event);
		
	}

	@Override
	public HandlerRegistration addMouseUpHandler(MouseUpHandler handler) {
		// TODO Auto-generated method stub
		return addMouseUpHandler(handler);
	}

	@Override
	public HandlerRegistration addMouseOutHandler(MouseOutHandler handler) {
		// TODO Auto-generated method stub
		return addMouseOutHandler(handler);
	}

	@Override
	public HandlerRegistration addMouseOverHandler(MouseOverHandler handler) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HandlerRegistration addMouseMoveHandler(MouseMoveHandler handler) {
		// TODO Auto-generated method stub
		return addMouseMoveHandler(handler);
	}

	@Override
	public HandlerRegistration addMouseWheelHandler(MouseWheelHandler handler) {
		// TODO Auto-generated method stub
		return addMouseWheelHandler(handler);
	}

	@Override
	public String getHTML() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setHTML(String html) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getText() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setText(String text) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setHTML(SafeHtml html) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Widget asWidget() {
		// TODO Auto-generated method stub
		return this;
	}




	public DialogBox getDlgBox() {
		return dlgBox;
	}




	public void setDlgBox(DialogBox dlgBox) {
		this.dlgBox = dlgBox;
	}
	
	
}
}
}