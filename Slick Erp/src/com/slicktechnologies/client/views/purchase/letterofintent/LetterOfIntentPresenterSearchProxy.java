package com.slicktechnologies.client.views.purchase.letterofintent;

import java.util.Vector;

import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.config.CategoryTypeFactory;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.client.views.purchase.letterofintent.LetterOfIntentPresenter.LOIPresenterSearch;
import com.slicktechnologies.shared.common.businessprocesslayer.LetterOfIntent;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;

public class LetterOfIntentPresenterSearchProxy extends LOIPresenterSearch {

	IntegerBox ibReqQuotationID, ibLOIid;
	TextBox ibRefOrderNo;
	ObjectListBox<Employee> oblEmployee;
	ObjectListBox<ConfigCategory> oblLoiGroup;
	ObjectListBox<Type> oblLoiType;
	DateComparator dateComparator;
	
	ObjectListBox<Branch> olbBranch;
	PersonInfoComposite vic;
	ProductInfoComposite pic;
	DateComparator datacomp;
	ObjectListBox<HrProject> project;
	IntegerBox purchaseRequisiteNo;
	TextBox tbtitle;
	ListBox status;

	public LetterOfIntentPresenterSearchProxy() {
		super();
		createGui();
	}

	public void initWidget() {
		ibReqQuotationID = new IntegerBox();
		ibRefOrderNo = new TextBox();
		ibLOIid = new IntegerBox();
		oblEmployee = new ObjectListBox<Employee>();
		oblEmployee.makeEmployeeLive(AppConstants.PURCHASEMODULE, AppConstants.LETTEROFINTENT, "Purchase Requisition");
		status = new ListBox();
		AppUtility.setStatusListBox(status, LetterOfIntent.getStatusList());
		oblLoiGroup = new ObjectListBox<ConfigCategory>();
//		AppUtility.MakeLiveCategoryConfig(oblLoiGroup, Screen.LOICATEGORY);
		oblLoiType = new ObjectListBox<Type>();
		CategoryTypeFactory.initiateOneManyFunctionality(oblLoiGroup,oblLoiType);
		 AppUtility.makeTypeListBoxLive(this.oblLoiType,Screen.LOICATEGORY);
//		initalizEmployeeNameListBox();

		dateComparator = new DateComparator("LoiDate", new LetterOfIntent());
		vic = AppUtility.vendorInfoComposite(new Vendor());
		pic = AppUtility.product(new SuperProduct(), false);
		datacomp = new DateComparator("expectedClosureDate",new LetterOfIntent());
		project = new ObjectListBox<HrProject>();
		HrProject.MakeProjectListBoxLive(project);
		purchaseRequisiteNo = new IntegerBox();
		purchaseRequisiteNo = new IntegerBox();
		tbtitle = new TextBox();

		vic.getCustomerId().getHeaderLabel().setText("Vendor Id");
		vic.getCustomerName().getHeaderLabel().setText("Vendor Name");
		vic.getCustomerCell().getHeaderLabel().setText("Vendor Cell");

		//**************rohan made changes here ***************
		
				olbBranch= new ObjectListBox<Branch>();
				AppUtility.makeBranchListBoxLive(olbBranch);
		
	}

	public void createScreen() {

		initWidget();
		FormFieldBuilder fbuilder;

		fbuilder = new FormFieldBuilder("RFQ ID", ibReqQuotationID);
		FormField fibReqQuotationID = fbuilder.setMandatory(false)
				.setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("LOI ID", ibLOIid);
		FormField floiID = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("RefOdrNo.", ibRefOrderNo);
		FormField fibRefOrderNo = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Pur. Eng.", oblEmployee);
		FormField femployee = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("LOI Category", oblLoiGroup);
		FormField fLOIgrpup = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("LOI Type", oblLoiType);
		FormField fLOItype = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("LOI From Date",
				dateComparator.getFromDate());
		FormField fcreationdate = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("LOI To Date",
				dateComparator.getToDate());
		FormField fclosuredate = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("", vic);
		FormField fvic = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(5).build();

		fbuilder = new FormFieldBuilder("", pic);
		FormField fpic = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(5).build();

		fbuilder = new FormFieldBuilder("Project", project);
		FormField fproject = fbuilder.setMandatory(false)
				.setMandatoryMsg("Project is mandatory").setRowSpan(0)
				.setColSpan(0).build();
		
		

		fbuilder = new FormFieldBuilder("PR Number", purchaseRequisiteNo);
		FormField fpuchasereq = fbuilder.setMandatory(false)
				.setMandatoryMsg("Branch is mandatory").setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("LOI Title", tbtitle);
		FormField ftbtitle = fbuilder.setMandatory(false)
				.setMandatoryMsg("LOI is mandatory").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Branch",olbBranch);
		FormField folbBranch= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		FormField[][] formfield = {
				{ floiID, ftbtitle, fcreationdate, fclosuredate },
				{ fLOIgrpup, fLOItype, femployee, fproject },
				{ fibReqQuotationID, fpuchasereq, fibRefOrderNo, folbBranch}, 
				{ fvic },
				{ fpic } 
				};

		this.fields = formfield;

	}

	public MyQuerry getQuerry() {
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter temp = null;

		if (project.getSelectedIndex() != 0) {

			temp = new Filter();
			int item = project.getSelectedIndex();
			String sel = project.getItemText(item);
			temp.setStringValue(sel);
			temp.setQuerryString("project");
			filtervec.add(temp);

		}

		if (purchaseRequisiteNo.getValue() != null) {
			temp = new Filter();
			temp.setIntValue(purchaseRequisiteNo.getValue());
			temp.setQuerryString("purchaseReqNo");
			filtervec.add(temp);

		}
		
		if(olbBranch.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(olbBranch.getValue().trim());
			temp.setQuerryString("branch");
			filtervec.add(temp);
		} 

		if (ibReqQuotationID.getValue() != null) {
			temp = new Filter();
			temp.setIntValue(ibReqQuotationID.getValue());
			temp.setQuerryString("reqforQuotationID");
			filtervec.add(temp);
		}
		if (ibLOIid.getValue() != null) {
			temp = new Filter();
			temp.setIntValue(ibLOIid.getValue());
			temp.setQuerryString("count");
			filtervec.add(temp);
		}
		if (!ibRefOrderNo.getValue().equals("")) {
			temp = new Filter();
			temp.setStringValue(ibRefOrderNo.getValue());
			temp.setQuerryString("refOrderNO");
			filtervec.add(temp);
		}
		if (oblEmployee.getValue() != null) {
			temp = new Filter();
			temp.setStringValue(oblEmployee.getValue());
			temp.setQuerryString("employee");
			filtervec.add(temp);
		}
		if (oblLoiGroup.getValue() != null) {
			temp = new Filter();
			temp.setStringValue(oblLoiGroup.getValue());
			temp.setQuerryString("loiGroup");
			filtervec.add(temp);
		}
		if (oblLoiType.getValue() != null) {
			temp = new Filter();
			temp.setStringValue(oblLoiType.getValue());
			temp.setQuerryString("loiType");
			filtervec.add(temp);
		}
		if (dateComparator.getValue() != null) {
			filtervec.addAll(dateComparator.getValue());
		}

		if (!pic.getProdName().getValue().equals("")) {
			temp = new Filter();
			temp.setStringValue(pic.getProdName().getValue());
			temp.setQuerryString("productinfo.productName");
			filtervec.add(temp);
		}
		if (!tbtitle.getValue().equals("")) {
			temp = new Filter();
			temp.setStringValue(tbtitle.getValue());
			temp.setQuerryString("title");
			filtervec.add(temp);
		}

		if (!pic.getProdCode().getValue().equals("")) {
			temp = new Filter();
			temp.setStringValue(pic.getProdCode().getValue());
			temp.setQuerryString("productinfo.productCode");
			filtervec.add(temp);
		}
		if (!pic.getProdID().getValue().equals("")) {
			temp = new Filter();
			temp.setIntValue(Integer.parseInt(pic.getProdID().getValue()));
			temp.setQuerryString("productinfo.productID");
			filtervec.add(temp);
		}

		if (!vic.getName().getValue().equals("")) {
			temp = new Filter();
			temp.setStringValue(vic.getName().getValue());
			temp.setQuerryString("vendorinfo.vendorName");
			filtervec.add(temp);
		}
		
		if (!vic.getPhone().getValue().equals("")) {
			temp = new Filter();
			temp.setLongValue(Long.parseLong(vic.getPhone().getValue()));
			temp.setQuerryString("vendorinfo.vendorPhone");
			filtervec.add(temp);
		}

		if (!vic.getId().getValue().equals("")) {
			temp = new Filter();
			temp.setIntValue(Integer.parseInt(vic.getId().getValue()));
			temp.setQuerryString("vendorinfo.vendorId");
			filtervec.add(temp);
		}

		MyQuerry querry = new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new LetterOfIntent());
		return querry;
	}

	protected void initalizEmployeeNameListBox() {
		MyQuerry querry = new MyQuerry(new Vector<Filter>(), new Employee());
		oblEmployee.MakeLive(querry);
	}
	
	@Override
	public boolean validate() {
		if(LoginPresenter.branchRestrictionFlag)
		{
		if(olbBranch.getSelectedIndex()==0)
		{
			showDialogMessage("Select Branch");
			return false;
		}
		}
		return true;
	}

}
