package com.slicktechnologies.client.views.purchase;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.MultiWordSuggestOracle;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FlexForm;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.CompositeInterface;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.reusabledata.ConditionDialogBox;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.contactidentification.ContactPersonIdentification;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.helperlayer.ProductInfo;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.role.ScreenAuthorization;
import com.slicktechnologies.shared.common.role.UserRole;


public class ProductInfoComposite extends  Composite implements HasValue<ProductInfo>,
SelectionHandler<Suggestion>,Editor<ProductInfo>,CompositeInterface, ClickHandler
{
public boolean bool;
	
	
Logger logger=Logger.getLogger("Product info");
	
	private SuggestBox prodID;
	private SuggestBox prodCode;
	private SuggestBox prodName;
	TextBox prodCategory;
	DoubleBox prodPrice;
	TextBox unitOfmeasuerment;
	 FormField productCode,productName,productCategory,productPrice,productID,UOM;
	
	 public static ArrayList<ProductInfo> globalItemProdArray=new ArrayList<ProductInfo>();
	 public static ArrayList<ProductInfo> globalServiceProdArray=new ArrayList<ProductInfo>();
	 public static ArrayList<ProductInfo> globalSuperProdArray=new ArrayList<ProductInfo>();
	 
	 /**
	  * 
	  */
	 boolean loadAllProductFlag=false;
	 
	/**
     * @author Anil 
     * @since 08-02-2022
     * Adding new and edit button functionality on Product Composite
     */
    public HorizontalPanel hrButtonPanel;
    public Button btnNew;
    public Button btnEdit;
    public Button btnClr;
  
    FormField buttonPanel;
    public GeneralViewDocumentPopup genPopup=null;
    GWTCAlert alert =new GWTCAlert();
    GenricServiceAsync async=GWT.create(GenricService.class);
    
    ConditionDialogBox conditionalPopup = new ConditionDialogBox("Please select product type.", AppConstants.YES,AppConstants.NO);
    PopupPanel popUpPanel=new PopupPanel(true);
    
    String classNameValue="";
    
    ItemProduct itemProductObj=null;
    ServiceProduct serviceProductObj=null;
    
    boolean searchPopupScreenFlag=false;
    
	
    public MyQuerry getQuerry() {
		return querry;
	}

	
	public void setQuerry(MyQuerry querry) {
		this.querry = querry;
	}

	
	
	private ProductInfo pentity;
	
	 private   HashMap<String,ProductInfo>prodCodetoProdInfo = new HashMap<String,ProductInfo>();
	 
	 private   HashMap<String,ProductInfo>prodNametoProdInfo = new HashMap<String,ProductInfo>();
	 
	 private   HashMap<String,ProductInfo>prodCatetoProdInfo = new HashMap<String,ProductInfo>();
	 
	 private   HashMap<Double,ProductInfo>prodPricetoProdInfo = new HashMap<Double,ProductInfo>();
	 
	 private   HashMap<Integer,ProductInfo>prodIDtoProdInfo = new HashMap<Integer,ProductInfo>();
	 
	 final ProductInfoServiceAsync service=GWT.create(ProductInfoService.class);
		
	 
	 MyQuerry querry;
	
	 public ProductInfoServiceAsync getService() {
		return service;
	}


	
	/** The panel. */
	 private FlowPanel panel;
	 
	 /** The ff. */
	 private FormField[][]formfield;
	 
	 
	 /**
	  * @author Anil
	  * @since 09-02-2022
	  * @param querry
	  * @param bool
	  * @param searchScreen
	  * Added new constructor to hide new and edit button on search popup screen
	  */
	 public ProductInfoComposite(MyQuerry querry,boolean bool,String searchScreen){
	 	super();
	 	this.bool=bool;
//	 	createForm();
		this.querry=querry;
		loadAllProductFlag=false;
		searchPopupScreenFlag=true;
		
		String[] classType=querry.getQuerryObject().getClass().toString().trim().split("\\s+");
		String strcname=classType[1];
		String classNameArray[]=strcname.split("\\.");
		classNameValue=classNameArray[classNameArray.length-1];
		System.out.println("Person Composite Querry Class"+classNameValue);
		Console.log("Class Name Value Product Composte"+classNameValue);
		
		createForm();
		
		if(AppConstants.PRODUCTCOMPOSITEITEM.equals(classNameValue.trim()))
		{
			initalizeItemProductData();
		}
		if(AppConstants.PRODUCTCOMPOSITESERVICE.equals(classNameValue.trim()))
		{
			initalizeServiceProductData();
		}
		if(AppConstants.PRODUCTCOMPOSITESUPER.equals(classNameValue.trim()))
		{
			initalizeSuperProductData();
		}
		
		initWidget(panel);
		applyStyle();
	 }
 
 public ProductInfoComposite(MyQuerry querry,boolean bool)
 {
	 	super();
	 	this.bool=bool;
//	 	createForm();
		this.querry=querry;
		loadAllProductFlag=false;
		
		String[] classType=querry.getQuerryObject().getClass().toString().trim().split("\\s+");
		String strcname=classType[1];
		String classNameArray[]=strcname.split("\\.");
		classNameValue=classNameArray[classNameArray.length-1];
		System.out.println("Person Composite Querry Class"+classNameValue);
		Console.log("Class Name Value Product Composte"+classNameValue);
		
		createForm();
		
		if(AppConstants.PRODUCTCOMPOSITEITEM.equals(classNameValue.trim()))
		{
			initalizeItemProductData();
		}
		if(AppConstants.PRODUCTCOMPOSITESERVICE.equals(classNameValue.trim()))
		{
			initalizeServiceProductData();
		}
		if(AppConstants.PRODUCTCOMPOSITESUPER.equals(classNameValue.trim()))
		{
			initalizeSuperProductData();
		}
		
//		initalizeProductInfoHashMaps();
		initWidget(panel);
		
		applyStyle();
		
 }
 
 
 /**
  * 
  */
 
 
 public ProductInfoComposite(MyQuerry querry,boolean bool,boolean loadAllProductFlag)
 {
	 	super();
	 	this.bool=bool;
	 	createForm();
		this.querry=querry;
		/**
		 * 
		 */
		this.loadAllProductFlag=loadAllProductFlag;
		
		String[] classType=querry.getQuerryObject().getClass().toString().trim().split("\\s+");
		String strcname=classType[1];
		String classNameArray[]=strcname.split("\\.");
		classNameValue=classNameArray[classNameArray.length-1];
		System.out.println("Person Composite Querry Class"+classNameValue);
		Console.log("Class Name Value Product Composte"+classNameValue);
		
		if(AppConstants.PRODUCTCOMPOSITEITEM.equals(classNameValue.trim()))
		{
			initalizeItemProductData();
		}
		if(AppConstants.PRODUCTCOMPOSITESERVICE.equals(classNameValue.trim()))
		{
			initalizeServiceProductData();
		}
		if(AppConstants.PRODUCTCOMPOSITESUPER.equals(classNameValue.trim()))
		{
			initalizeSuperProductData();
		}
		
//		initalizeProductInfoHashMaps();
		initWidget(panel);
		
		applyStyle();
		
 }
 
 
 	/**
 	 * @author Anil , Date : 09-07-2019
 	 * Loading only those product whose group is marked as Asset
 	 * @param querry
 	 */
	 public ProductInfoComposite(MyQuerry querry) {
		// TODO Auto-generated constructor stub
		 
		super();
	 	createForm();
		this.querry=querry;
		
		String[] classType=querry.getQuerryObject().getClass().toString().trim().split("\\s+");
		String strcname=classType[1];
		String classNameArray[]=strcname.split("\\.");
		classNameValue=classNameArray[classNameArray.length-1];
		System.out.println("Person Composite Querry Class"+classNameValue);
		Console.log("Class Name Value Product Composte"+classNameValue);
		
		if(AppConstants.PRODUCTCOMPOSITEITEM.equals(classNameValue.trim())){
			initalizeItemProductGroupedAsAsset();
		}
		initWidget(panel);
		
		applyStyle();
	 }
     /**
      * Date : 27/02/2021 By Priyanka
      * Des : New added product not immediately reflected on search product composite issue raised by Nitin Sir.
      */
	 public void setProductInfoHashMap(ProductInfo person){
		try {
				
			    prodIDtoProdInfo.put(person.getCount(),person);
				prodCodetoProdInfo.put(person.getProductCode(),person);
				prodNametoProdInfo.put(person.getProductName(),person);
				prodCatetoProdInfo.put(person.getProductCategory(),person);
				prodPricetoProdInfo.put(person.getProductPrice(),person);
				initalizeNameOracle();
				initalizeIdOracle();
				initalizeCodeOracle();
				
				
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println(e.getMessage());
			}
		}
	 
	 private void initalizeItemProductGroupedAsAsset() {
		// TODO Auto-generated method stub
 		Console.log("initalizeItemProductGroupedAsAsset");
 		Console.log("Retrieving Data For Item Product"+globalItemProdArray.size());
 		ProductInfoServiceAsync  prodservice=GWT.create(ProductInfoService.class);
 		Company c=new Company();
 		prodservice.LoadAssetProd(c.getCompanyId(),new AsyncCallback<ArrayList<ProductInfo>>() {
			@Override
			public void onFailure(Throwable caught) {
				Console.log("In Failure For Composite In PInfo");
				caught.printStackTrace();
			}

			@Override
			public void onSuccess(ArrayList<ProductInfo> result) {
				Console.log("On Success Initializing Hash Map for Item Product");
				Console.log("Size Of Impl Retrieval"+result.size());
				if(result.size()>0){
					for(int i=0;i<result.size();i++){
						int id=result.get(i).getProdID();
						String prodname=result.get(i).getProductName();
						String prodcode=result.get(i).getProductCode();
						String prodcate=result.get(i).getProductCategory();
						double price=result.get(i).getProductPrice();
						
						prodIDtoProdInfo.put(id, result.get(i));
						prodCodetoProdInfo.put(prodcode, result.get(i));
						prodNametoProdInfo.put(prodname, result.get(i));
						prodCatetoProdInfo.put(prodcate, result.get(i));
						prodPricetoProdInfo.put(price, result.get(i));
						logger.log(Level.SEVERE,"inside load all product flaggggggg");
					}
					initalizeNameOracle();
					initalizeIdOracle();
					initalizeCodeOracle();
					prodCategory.setValue("");
					
				}
				if(result.size()==0){
					creatingHashMaps(AppConstants.PRODUCTCOMPOSITEITEM);
				}
			}
		});
		 
		 
		 
		 
	 }


/**
  * 
  */
	
	
 private void initializewidgets()
 {
	 prodID=new SuggestBox();
	 prodID.addSelectionHandler(this);
	 
	 prodCode=new SuggestBox();
	 prodCode.addSelectionHandler(this);
	 
	 prodName=new SuggestBox();
	 prodName.addSelectionHandler(this);
	 
	 prodCategory=new TextBox();
	 prodCategory.setEnabled(false);
	 prodPrice=new DoubleBox();
	 prodPrice.setEnabled(false);
	 unitOfmeasuerment=new TextBox();
	 unitOfmeasuerment.setEnabled(false);
	 
	 intializeButtonPanel();
 }
	
	 
	private void intializeButtonPanel() {
//		conditionalPopup.getBtnOne().setText("Service Product");
//		conditionalPopup.getBtnTwo().setText("Item Product");
//
//		conditionalPopup.getBtnOne().getElement().getStyle().setWidth(90, Unit.PX);
//		conditionalPopup.getBtnTwo().getElement().getStyle().setWidth(90, Unit.PX);
//
//		conditionalPopup.getBtnOne().getElement().getStyle().setLeft(230, Unit.PX);
//		conditionalPopup.getBtnTwo().getElement().getStyle().setLeft(130, Unit.PX);
//
//		conditionalPopup.getBtnOne().addClickHandler(this);
//		conditionalPopup.getBtnTwo().addClickHandler(this);

		hrButtonPanel = new HorizontalPanel();

		btnNew = new Button("<image src='/images/menubar/043-plus.png' width='25px' height='18px'/>");
		btnEdit = new Button("<image src='/images/menubar/162-edit.png' width='25px' height='18px'/>");
		btnClr = new Button("<image src='/images/menubar/137-eraser.png' width='25px' height='18px'/>");

		btnNew.getElement().addClassName("buttonIcon");
		btnEdit.getElement().addClassName("buttonIcon");
		btnClr.getElement().addClassName("buttonIcon");
		

		btnNew.addClickHandler(this);
		btnEdit.addClickHandler(this);
		btnClr.addClickHandler(this);
		

		boolean naf=false;
		if(classNameValue!=null&&classNameValue.equals(AppConstants.PRODUCTCOMPOSITEITEM)){
			naf = isAuthorized("Product", Screen.ITEMPRODUCT);
		}else if(classNameValue!=null&&classNameValue.equals(AppConstants.PRODUCTCOMPOSITESERVICE)){
			naf = isAuthorized("Product", Screen.SERVICEPRODUCT);
		}
		

		if (naf) {
			hrButtonPanel.add(btnNew);
			hrButtonPanel.add(btnEdit);
			hrButtonPanel.add(btnClr);
		}
		btnNew.setEnabled(naf);
		btnEdit.setEnabled(naf);

		if (!naf) {
			hrButtonPanel.setVisible(false);
		}
		hrButtonPanel.setSpacing(2);
	}
	
	public boolean isAuthorized(String moduleName,Screen screen){
		UserRole role = UserConfiguration.getRole();
		if (role == null)
			return false;
		
		for (ScreenAuthorization hr : role.getAuthorization()) {
			String screenName = AuthorizationHelper.checkScreenName(screen);
			if(AppConstants.ADMINUSER.equals(role.getRoleName().trim())){
				return true;
			}else{
				if(hr.getScreens().trim().equalsIgnoreCase(screenName.trim())&&hr.getModule().equalsIgnoreCase(moduleName.trim())) {
					return hr.isCreate();
				}
			}
		}
		return false;
	}


	public void createForm()
	{
		initializewidgets();
		FormFieldBuilder builder;
		
		builder= new FormFieldBuilder("Product Code",prodCode);
		productCode=builder.setMandatory(true).setColSpan(0).
				setMandatoryMsg("ID  is Mandatory!").build();
		
		builder= new FormFieldBuilder("Product Name",prodName);
		productName=builder.setMandatory(true).setColSpan(0).
				setMandatoryMsg("ID  is Mandatory!").build();
		
		builder= new FormFieldBuilder("Product Category",prodCategory);
		productCategory=builder.setMandatory(false).setColSpan(0).
				setMandatoryMsg("ID  is Mandatory!").build();
		
		builder= new FormFieldBuilder("Product Price",prodPrice);
		productPrice=builder.setMandatory(false).setColSpan(0).
				setMandatoryMsg("ID  is Mandatory!").build();
		
		builder= new FormFieldBuilder("Product ID",prodID);
		productID=builder.setMandatory(true).setColSpan(0).
				setMandatoryMsg("ID  is Mandatory!").build();
		
		builder= new FormFieldBuilder("Unit Of measurement",unitOfmeasuerment);
		UOM=builder.setMandatory(true).setColSpan(0).
				setMandatoryMsg("ID  is Mandatory!").build();
		/**
		 * @author Anil
		 * @since 08-02-2022
		 */
		builder=new FormFieldBuilder("",hrButtonPanel);
		buttonPanel=builder.setMandatory(false).build();
		
		if(bool==true){
		formfield=new FormField[][]
				{
					{buttonPanel, productID,productCode,productName,productCategory,productPrice },
					{UOM}
			    };
		}
		else
		{
			formfield=new FormField[][]
					{
						/**
						 * @author Anil
						 * @since 11-02-2022
						 * As rearranging field sequence 
						 * Requirement raised by Nitin
						 */
//						{ buttonPanel,productID,productCode,productName},
						{ buttonPanel,productName,productCode,productID},
						
				    };
		}
		
		
		FlexForm form = new FlexForm(formfield,FormStyle.ROWFORM);
		panel=new FlowPanel();
		form.setWidth("98%");
		panel.add(form);
		
		if(searchPopupScreenFlag||classNameValue==null||classNameValue.equals("")||classNameValue.equals(AppConstants.PRODUCTCOMPOSITESUPER)){
			buttonPanel.setVisible(false);
			hrButtonPanel.setVisible(false);
		}

	
	}
	
	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<ProductInfo> handler) {
		// TODO Auto-generated method stub
		return this.addValueChangeHandler(handler);
	}

	@Override
	public void setEnable(boolean status) {
		prodName.setEnabled(status);
		prodID.setEnabled(status);
		prodCode.setEnabled(status);
		prodCategory.setEnabled(false);
		prodPrice.setEnabled(false);
		unitOfmeasuerment.setEnabled(false);
		
		this.btnNew.setEnabled(true);
		this.btnEdit.setEnabled(true);
		this.btnClr.setEnabled(status);
	}

	@Override
	public void clear() {
		int row=this.formfield.length;
		for(int k=0;k<row;k++)
		{
			int column=formfield[k].length;
			for(int i=0;i<column;i++)
			{
				Widget widg=formfield[k][i].getWidget();
				if(widg instanceof SuggestBox)
				{
					SuggestBox sb=(SuggestBox) widg;
					resetBorderColor(sb);
					sb.setText("");
					
					
				}
				if(widg instanceof TextBox)
				{
					TextBox sb=(TextBox) widg;
					resetBorderColor(sb);
					sb.setText("");
					
					
				}
				if(widg instanceof DoubleBox)
				{
					DoubleBox sb=(DoubleBox) widg;
					resetBorderColor(sb);
					sb.setText(null);
				}
				
				if(formfield[k][i].getMandatoryMsgWidget()!=null)
					formfield[k][i].getMandatoryMsgWidget().setVisible(false);
				}
			}

		
	}

	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public void onSelection(SelectionEvent<Suggestion> event) {
		SuggestBox sb=(SuggestBox) event.getSource();
		String selectedItem=sb.getText().trim();
		resetBorderColor(prodID);
		resetBorderColor(prodName);
		resetBorderColor(prodCode);
		
		
		if(sb==this.prodID)
		{
			int count=Integer.parseInt(selectedItem);
			pentity=prodIDtoProdInfo.get(count);
			
			if(pentity==null)
				sb.setText("");
			else
			{	
				prodCode.setText(pentity.getProductCode());
				prodName.setText(pentity.getProductName());
				prodCategory.setText(pentity.getProductCategory());
				prodPrice.setText(pentity.getProductPrice()+"");
				/**
				 * nidhi
				 * 10-10-2018
				 */
				unitOfmeasuerment.setText(pentity.getUnitofMeasure());
				
			}
		}

		
		if(sb==this.prodName)
		{
			String[]xyz=selectedItem.split("-");
			
			if(xyz.length==0)
			{
				sb.setText("");
				return;	
			}
			
			else
			{
				pentity=prodNametoProdInfo.get(selectedItem);
				
				if(pentity==null)
					sb.setText("");
				
				else
				{
					prodCode.setText(pentity.getProductCode());
					prodID.setText(pentity.getProdID()+"");
					prodCategory.setText(pentity.getProductCategory());
					prodPrice.setText(pentity.getProductPrice()+"");
					unitOfmeasuerment.setText(pentity.getUnitofMeasure());
				}
			}
		}

		
		if(sb==this.prodCode)
		{
			String[]xyz=selectedItem.split("-");
			
			if(xyz.length==0)
			{
				sb.setText("");
				return;	
			}
			
			else
			{
				pentity=prodCodetoProdInfo.get(selectedItem);
				
				if(pentity==null)
					sb.setText("");
				
				else
				{
					prodName.setText(pentity.getProductName());
					prodID.setText(pentity.getProdID()+"");
					prodCategory.setText(pentity.getProductCategory());
					prodPrice.setText(pentity.getProductPrice()+"");
					unitOfmeasuerment.setText(pentity.getUnitofMeasure());
				}
			}
		}

		
	}
	
	
	

	@Override
	public ProductInfo getValue() {
		ProductInfo entity = null;
		    int id=-1;
		    if((this.prodID.getText().trim().equals(""))==false)
		       id=Integer.parseInt(this.getProdID().getText().trim());
		    
		    entity=prodIDtoProdInfo.get(id);
		    pentity=entity;
		    return entity;

	}
	public int getIdValue()
	{
		if(this.prodID!=null)
		{
			String count=prodID.getText().trim();
			try{
			Integer intCount=Integer.parseInt(count);
			return intCount;
			}
			catch(Exception e)
			{
				return -1;
			}
			
			
		}
		return -1;
	}
	
	public String getProdNameValue()
	{
		if(this.prodName!=null)
		{
			String name=this.prodName.getText().trim();
			return name;
		}
		return "";
			
	}

	public String getProdCodeValue()
	{
		if(this.prodCode!=null)
		{
			String code=this.prodCode.getText().trim();
			return code;
		}
		return "";
			
	}
	
	
	public Vector<Filter> getQuerryFilters()
	{
		
		  Filter temp;
		  Vector<Filter>filtervec=new Vector<Filter>();
		  if(getIdValue()!=-1)
		  {
		  temp=new Filter();
		  temp.setIntValue(getIdValue());
		  temp.setQuerryString("productInfo.prodID");
		  filtervec.add(temp);
		  }
		  
		  if(!(getProdNameValue().equals("")))
		  {
		  temp=new Filter();
		  temp.setStringValue(getProdNameValue());
		  temp.setQuerryString("productInfo.productName");
		  filtervec.add(temp);
		  }
		  if(!(getProdCodeValue().equals("")))
		  {
		  temp=new Filter();
		  temp.setStringValue(getProdCodeValue());
		  temp.setQuerryString("productInfo.productCode");
		  filtervec.add(temp);
		  }
		  
		  return filtervec;
	}
	
	
	
	@Override
	public void setValue(ProductInfo value) {
		prodID.setText(value.getProdID()+"");
		prodCode.setText(value.getProductCode());
		prodName.setText(value.getProductName());
		
		pentity=value;
		
	}
	
	
	public void setEnabled(boolean status)
 	{
		 this.prodID.setEnabled(status);
		 this.prodName.setEnabled(status);
		 this.prodCode.setEnabled(status);
		 this.prodCategory.setEnabled(status);
		 this.prodPrice.setEnabled(status);
		 this.unitOfmeasuerment.setEnabled(status);
 	}

	@Override
	public void setValue(ProductInfo value, boolean fireEvents) {
		
	}

	
	public void initializeFirstHashMap(final String type)
	{
 		ProductInfoServiceAsync  productservice=GWT.create(ProductInfoService.class);
 		System.out.println("product comp");
		System.out.println("prod"+productservice);
		
		Company c=new Company();
		
 		productservice.LoadVinfo(c.getCompanyId(),type, new AsyncCallback<ArrayList<ProductInfo>>() {
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("on failure");
			}

			@Override
			public void onSuccess(ArrayList<ProductInfo> result) {
			System.out.println("on success");
			
			if(AppConstants.PRODUCTCOMPOSITEITEM.equals(type))
			{
				for(ProductInfo info:result)
				{
					/**
					 * 
					 */
					if(loadAllProductFlag){
						globalItemProdArray.add(info);
						Console.log("Inside load all product flag");
					}else if(info.isStatus()){
						globalItemProdArray.add(info);
						Console.log("outside of all product flag");
					}
//					globalItemProdArray.add(info);
				}
			}
			
			if(AppConstants.PRODUCTCOMPOSITESERVICE.equals(type))
			{
				for(ProductInfo info:result)
				{
					globalServiceProdArray.add(info);
				}
			}
			
			if(AppConstants.PRODUCTCOMPOSITESUPER.equals(type))
			{
				for(ProductInfo info:result)
				{
					globalSuperProdArray.add(info);
				}
			}
			
			Console.log("Main hashmap method ServiceProduct Size"+globalServiceProdArray.size());
			Console.log("Main hashmap method ItemProduct Size"+globalItemProdArray.size());
			Console.log("Main hashmap method SuperProduct Size"+globalSuperProdArray.size());
			
			
				for(int i=0;i<result.size();i++)
				{
					/**
					 * 
					 */
					if(loadAllProductFlag){
						int id=result.get(i).getProdID();
						String prodname=result.get(i).getProductName();
						String prodcode=result.get(i).getProductCode();
						String prodcate=result.get(i).getProductCategory();
						double price=result.get(i).getProductPrice();
						
						prodIDtoProdInfo.put(id, result.get(i));
						prodCodetoProdInfo.put(prodcode, result.get(i));
						prodNametoProdInfo.put(prodname, result.get(i));
						prodCatetoProdInfo.put(prodcate, result.get(i));
						prodPricetoProdInfo.put(price, result.get(i));
						logger.log(Level.SEVERE,"inside load all product flaggggggg");
						
					}else if(result.get(i).isStatus()){
					int id=result.get(i).getProdID();
					String prodname=result.get(i).getProductName();
					String prodcode=result.get(i).getProductCode();
					String prodcate=result.get(i).getProductCategory();
					double price=result.get(i).getProductPrice();
					
					prodIDtoProdInfo.put(id, result.get(i));
					prodCodetoProdInfo.put(prodcode, result.get(i));
					prodNametoProdInfo.put(prodname, result.get(i));
					prodCatetoProdInfo.put(prodcate, result.get(i));
					prodPricetoProdInfo.put(price, result.get(i));
					logger.log(Level.SEVERE,"outside load all product flagggggg");
					
					}
				}
				initalizeNameOracle();
				initalizeIdOracle();
				initalizeCodeOracle();
				prodCategory.setValue("");
				//initalizeCateOracle();
				//initalizePrizeOracle();
			}
		});
	}
	
	public void initalizeItemProductData()
 	{
		Console.log("Item Product Data In Initialize"+globalItemProdArray.size());
 		if(globalItemProdArray.size()==0)
 		{
 			Console.log("Size Of Item Prodcut Is Zero");
 			initializeFirstHashMap(AppConstants.PRODUCTCOMPOSITEITEM);
 		}
 		if(globalItemProdArray.size()!=0)
 		{
 			Console.log("Size Of Item Prodcut Is Not Zero");
 			retrieveItemProductData();
 		}
 	}
	
	public void initalizeServiceProductData()
 	{
		Console.log("Service Product Data"+globalServiceProdArray.size());
 		if(globalServiceProdArray.size()==0)
 		{
 			Console.log("Size Of Service Prodcut Is Zero");
 			initializeFirstHashMap(AppConstants.PRODUCTCOMPOSITESERVICE);
 		}
 		if(globalServiceProdArray.size()!=0)
 		{
 			Console.log("Size Of Service Prodcut Is Not Zero");
 			retrieveServiceProductData();
 		}
 	}
	
	public void initalizeSuperProductData()
 	{
		Console.log("In Super Data");
		if(globalSuperProdArray.size()==0)
		{
			Console.log("Size All Zero");
			if(globalItemProdArray.size()!=0&&globalServiceProdArray.size()==0){
				Console.log("Item Prod Not Zero And Service Prod Zero");
				Collections.sort(globalItemProdArray, itemArrayComparator);
				int latestItemCount=globalItemProdArray.get(globalItemProdArray.size()-1).getProdID();
				
				retrieveSuperProductDataOne(latestItemCount,0,false);
			}
			if(globalItemProdArray.size()!=0&&globalServiceProdArray.size()!=0){
				Console.log("Item Prod Not Zero And Service Prod Not Zero");
				Collections.sort(globalItemProdArray, itemArrayComparator);
				Collections.sort(globalServiceProdArray, serviceArrayComparator);
				int latestItemCount=globalItemProdArray.get(globalItemProdArray.size()-1).getProdID();
				int latestServiceCount=globalServiceProdArray.get(globalServiceProdArray.size()-1).getProdID();
				
				retrieveSuperProductDataOne(latestItemCount,latestServiceCount,false);
			}
			if(globalItemProdArray.size()==0&&globalServiceProdArray.size()!=0){
				Console.log("Item Prod Zero And Service Prod Not Zero");
				Collections.sort(globalServiceProdArray, serviceArrayComparator);
				int latestServiceCount=globalServiceProdArray.get(globalServiceProdArray.size()-1).getProdID();
				
				retrieveSuperProductDataOne(0,latestServiceCount,false);
			}
			if(globalItemProdArray.size()==0&&globalServiceProdArray.size()==0){
				Console.log("Both Zero");
				retrieveSuperProductDataOne(0,0,false);
			}
		}
		
		if(globalSuperProdArray.size()!=0)
		{
			Console.log("Size Not Zero For Super Prod");
			Collections.sort(globalItemProdArray, itemArrayComparator);
			Collections.sort(globalServiceProdArray, serviceArrayComparator);
			int latestItemCount=0;
			int latestServiceCount=0;
			
			if(globalItemProdArray.size()!=0){
				latestItemCount=globalItemProdArray.get(globalItemProdArray.size()-1).getProdID();
			}
			if(globalServiceProdArray.size()!=0){
				latestServiceCount=globalServiceProdArray.get(globalServiceProdArray.size()-1).getProdID();
			}
			
			retrieveSuperProductDataOne(latestItemCount,latestServiceCount,true);
		}
		
		
//		Console.log("Super Product Data"+globalSuperProdArray.size());
// 		if(globalSuperProdArray.size()==0)
// 		{
// 			initializeFirstHashMap(AppConstants.PRODUCTCOMPOSITESUPER);
// 		}
// 		if(globalSuperProdArray.size()!=0)
// 		{
// 			retrieveSuperProductData();
// 		}
 	}
	
	
	
	public void retrieveItemProductData()
 	{
 		Console.log("Initializing Hash Map Before RPC Item Product");
 		Console.log("Retrieving Data For Item Product"+globalItemProdArray.size());
 		ProductInfoServiceAsync  prodservice=GWT.create(ProductInfoService.class);
 		Company c=new Company();
 		
		Collections.sort(globalItemProdArray,itemArrayComparator);
		Console.log("After Comparator Item Prod====");
		int latestItemId=globalItemProdArray.get(globalItemProdArray.size()-1).getProdID();
 		Console.log("Latest Item Product ID===="+latestItemId);
		
		
 		prodservice.LoadProdinfo(c.getCompanyId(), latestItemId, AppConstants.PRODUCTCOMPOSITEITEM, new AsyncCallback<ArrayList<ProductInfo>>() {
			@Override
			public void onFailure(Throwable caught) {
				Console.log("In Failure For Composite In PInfo");
				caught.printStackTrace();
			}

			@Override
			public void onSuccess(ArrayList<ProductInfo> result) {
				Console.log("On Success Initializing Hash Map for Item Product");
				Console.log("Size Of Impl Retrieval"+result.size());
				
				if(result.size()>0)
				{
					for(int i=0;i<result.size();i++)
					{
						globalItemProdArray.add(result.get(i));
					}
					creatingHashMaps(AppConstants.PRODUCTCOMPOSITEITEM);
				}
				if(result.size()==0)
				{
					creatingHashMaps(AppConstants.PRODUCTCOMPOSITEITEM);
				}
			}
		});
	 }
	
	
	public void retrieveServiceProductData()
 	{
 		Console.log("Initializing Hash Map Before RPCService");
 		
 		ProductInfoServiceAsync  prodservice=GWT.create(ProductInfoService.class);
 		Company c=new Company();
 		System.out.println("Company Id"+c.getCompanyId());
 		
		Collections.sort(globalServiceProdArray,serviceArrayComparator);
		Console.log("After Comparator");
		int latestCustomerId=globalServiceProdArray.get(globalServiceProdArray.size()-1).getProdID();
 		Console.log("Latest Service Product ID"+latestCustomerId);
		
		
 		prodservice.LoadProdinfo(c.getCompanyId(), latestCustomerId, AppConstants.PRODUCTCOMPOSITESERVICE, new AsyncCallback<ArrayList<ProductInfo>>() {
			@Override
			public void onFailure(Throwable caught) {
				Console.log("In Failure For Composite In PInfo");
				caught.printStackTrace();
			}

			@Override
			public void onSuccess(ArrayList<ProductInfo> result) {
				Console.log("On Success Initializing Hash Map for PInfo");
				Console.log("Size Of Impl Retrieval"+result.size());
				
				if(result.size()>0)
				{
					for(int i=0;i<result.size();i++)
					{
						globalServiceProdArray.add(result.get(i));
					}
					creatingHashMaps(AppConstants.PRODUCTCOMPOSITESERVICE);
				}
				if(result.size()==0)
				{
					creatingHashMaps(AppConstants.PRODUCTCOMPOSITESERVICE);
				}
			}
		});
	 }
	
	public void retrieveSuperProductDataOne(int itemid,int serviceid,boolean flagVal)
 	{
		
		ProductInfoServiceAsync  prodservice=GWT.create(ProductInfoService.class);
		Company c=new Company();
 		System.out.println("Company Id"+c.getCompanyId());
		Console.log("Flag Value For Super Prod"+flagVal);
		if(flagVal==false)
		{
			Console.log("Entered In false");
			prodservice.LoadSuperProd(itemid, serviceid,c.getCompanyId(), new AsyncCallback<ArrayList<ProductInfo>>() {
				@Override
				public void onFailure(Throwable caught) {
					Console.log("In Failure For Composite In PInfo");
					caught.printStackTrace();
				}

				@Override
				public void onSuccess(ArrayList<ProductInfo> result) {
					Console.log("On Success Super Prod One");
					Console.log("Size Of Impl Retrieval"+result.size());
					if(globalSuperProdArray.size()==0)
					{
						globalSuperProdArray.addAll(globalItemProdArray);
						globalSuperProdArray.addAll(globalServiceProdArray);
					}
					
					for(int i=0;i<result.size();i++)
					{
						globalSuperProdArray.add(result.get(i));
					}
					creatingHashMaps(AppConstants.PRODUCTCOMPOSITESUPER);
					Console.log("Size Of Result Added TO Global Array"+globalSuperProdArray.size());
				}
			});
		}
		
		if(flagVal==true)
		{
			Console.log("Entered In true");
			prodservice.LoadSuperProd(itemid, serviceid,c.getCompanyId(), new AsyncCallback<ArrayList<ProductInfo>>() {
				@Override
				public void onFailure(Throwable caught) {
					Console.log("In Failure For Composite In PInfo");
					caught.printStackTrace();
				}

				@Override
				public void onSuccess(ArrayList<ProductInfo> result) {
					
					if(result.size()>0)
					{
						for(int i=0;i<result.size();i++)
						{
							globalSuperProdArray.add(result.get(i));
						}
						creatingHashMaps(AppConstants.PRODUCTCOMPOSITESUPER);
					}
					
				}
			});
		}
 	}
	
//	public void retrieveSuperProductData()
// 	{
// 		Console.log("Initializing Hash Map Before RPC SuperProd");
// 		
// 		ProductInfoServiceAsync  prodservice=GWT.create(ProductInfoService.class);
// 		Company c=new Company();
// 		System.out.println("Company Id"+c.getCompanyId());
// 		
//		Collections.sort(globalSuperProdArray,superArrayComparator);
//		Console.log("After Comparator");
//		int latestCustomerId=globalSuperProdArray.get(globalSuperProdArray.size()-1).getCount();
// 		Console.log("Latest SuperProduct ID"+latestCustomerId);
//		
//		
// 		prodservice.LoadProdinfo(c.getCompanyId(), latestCustomerId, AppConstants.PRODUCTCOMPOSITESUPER, new AsyncCallback<ArrayList<ProductInfo>>() {
//			@Override
//			public void onFailure(Throwable caught) {
//				Console.log("In Failure For Composite In PInfo");
//				caught.printStackTrace();
//			}
//
//			@Override
//			public void onSuccess(ArrayList<ProductInfo> result) {
//				Console.log("On Success Initializing Hash Map for PInfo");
//				Console.log("Size Of Impl Retrieval"+result.size());
//				
//				if(result.size()>0)
//				{
//					for(int i=0;i<result.size();i++)
//					{
//						globalSuperProdArray.add(result.get(i));
//					}
//					creatingHashMaps(AppConstants.PRODUCTCOMPOSITESUPER);
//				}
//				if(result.size()==0)
//				{
//					creatingHashMaps(AppConstants.PRODUCTCOMPOSITESUPER);
//				}
//			}
//		});
//	 }
	
	
	
	public void creatingHashMaps(String type)
 	{
 		
 		if(AppConstants.PRODUCTCOMPOSITEITEM.equals(type.trim()))
 		{
 			for(int i=0;i<globalItemProdArray.size();i++)
 			{
 				/**
 				 * 
 				 */
 				if(loadAllProductFlag){
 					int id=globalItemProdArray.get(i).getProdID();
 					String prodname=globalItemProdArray.get(i).getProductName();
 					String prodcode=globalItemProdArray.get(i).getProductCode();
 					String prodcate=globalItemProdArray.get(i).getProductCategory();
 					double price=globalItemProdArray.get(i).getProductPrice();
 					
 					prodIDtoProdInfo.put(id, globalItemProdArray.get(i));
 					prodCodetoProdInfo.put(prodcode, globalItemProdArray.get(i));
 					prodNametoProdInfo.put(prodname, globalItemProdArray.get(i));
 					prodCatetoProdInfo.put(prodcate, globalItemProdArray.get(i));
 					prodPricetoProdInfo.put(price, globalItemProdArray.get(i));
 				}else if(globalItemProdArray.get(i).isStatus()){
 					
 			
 				int id=globalItemProdArray.get(i).getProdID();
				String prodname=globalItemProdArray.get(i).getProductName();
				String prodcode=globalItemProdArray.get(i).getProductCode();
				String prodcate=globalItemProdArray.get(i).getProductCategory();
				double price=globalItemProdArray.get(i).getProductPrice();
				
				prodIDtoProdInfo.put(id, globalItemProdArray.get(i));
				prodCodetoProdInfo.put(prodcode, globalItemProdArray.get(i));
				prodNametoProdInfo.put(prodname, globalItemProdArray.get(i));
				prodCatetoProdInfo.put(prodcate, globalItemProdArray.get(i));
				prodPricetoProdInfo.put(price, globalItemProdArray.get(i));
 				}
 			}
 		}
 		
 		if(AppConstants.PRODUCTCOMPOSITESERVICE.equals(type.trim()))
 		{
 			for(int i=0;i<globalServiceProdArray.size();i++)
 			{
 				int id=globalServiceProdArray.get(i).getProdID();
				String prodname=globalServiceProdArray.get(i).getProductName();
				String prodcode=globalServiceProdArray.get(i).getProductCode();
				String prodcate=globalServiceProdArray.get(i).getProductCategory();
				double price=globalServiceProdArray.get(i).getProductPrice();
				
				prodIDtoProdInfo.put(id, globalServiceProdArray.get(i));
				prodCodetoProdInfo.put(prodcode, globalServiceProdArray.get(i));
				prodNametoProdInfo.put(prodname, globalServiceProdArray.get(i));
				prodCatetoProdInfo.put(prodcate, globalServiceProdArray.get(i));
				prodPricetoProdInfo.put(price, globalServiceProdArray.get(i));
 			}
 		}
 		
 		if(AppConstants.PRODUCTCOMPOSITESUPER.equals(type.trim()))
 		{
 			for(int i=0;i<globalSuperProdArray.size();i++)
 			{
 				int id=globalSuperProdArray.get(i).getProdID();
				String prodname=globalSuperProdArray.get(i).getProductName();
				String prodcode=globalSuperProdArray.get(i).getProductCode();
				String prodcate=globalSuperProdArray.get(i).getProductCategory();
				double price=globalSuperProdArray.get(i).getProductPrice();
				
				prodIDtoProdInfo.put(id, globalSuperProdArray.get(i));
				prodCodetoProdInfo.put(prodcode, globalSuperProdArray.get(i));
				prodNametoProdInfo.put(prodname, globalSuperProdArray.get(i));
				prodCatetoProdInfo.put(prodcate, globalSuperProdArray.get(i));
				prodPricetoProdInfo.put(price, globalSuperProdArray.get(i));
 			}
 		}
 		
 		
		
		Console.log("Exited For Loop For Filling Hash Maps & Initializing Oracle");
		Console.log("Hash Map For Id After Loop"+prodIDtoProdInfo.size());
		Console.log("Hash Map For Code After Loop"+prodCodetoProdInfo.size());
		Console.log("Hash Map For Name After Loop"+prodNametoProdInfo.size());
		Console.log("Hash Map For Category After Loop"+prodCatetoProdInfo.size());
		Console.log("Hash Map For Price After Loop"+prodPricetoProdInfo.size());
		
		initalizeNameOracle();
		initalizeIdOracle();
		initalizeCodeOracle();
		prodCategory.setValue("");
 	}
	
	
	   public void applyStyle()
	    {
	    	panel.setWidth("98%");
	    }
	 
		public void initalizeNameOracle()
		{
			 MultiWordSuggestOracle orcl = (MultiWordSuggestOracle) this.prodName.getSuggestOracle();
			 System.out.println("size of name hashmap  --------------- ::"+prodNametoProdInfo.size());
		 
			 for(String name:prodNametoProdInfo.keySet())
			 {
				 System.out.println("Name IS ");
				 orcl.add(name);
			 }
		
		}

		private void initalizeIdOracle()
	 	{
			 MultiWordSuggestOracle orcl = (MultiWordSuggestOracle) this.prodID.getSuggestOracle();
			 for(int id:this.prodIDtoProdInfo.keySet())
				 orcl.add(id+"");	 
	 	}

public void initalizeCodeOracle()
{
	 MultiWordSuggestOracle orcl = (MultiWordSuggestOracle) this.prodCode.getSuggestOracle();
	 System.out.println("size of name hashmap  --------------- ::"+prodCodetoProdInfo.size());
 
	 for(String code:prodCodetoProdInfo.keySet())
	 {
		 System.out.println("Name IS ");
		 orcl.add(code);
	 }

}

/********************************************************************************************************/

	Comparator<ProductInfo> itemArrayComparator = new Comparator<ProductInfo>() {
		public int compare(ProductInfo c1, ProductInfo c2) {
		Integer custCount1=c1.getCount();
		Integer custCount2=c2.getCount();
		
		return custCount1.compareTo(custCount2);
		}
	};
	
	Comparator<ProductInfo> serviceArrayComparator = new Comparator<ProductInfo>() {
		public int compare(ProductInfo c1, ProductInfo c2) {
		Integer custCount1=c1.getCount();
		Integer custCount2=c2.getCount();
		
		return custCount1.compareTo(custCount2);
		}
	};
	
	Comparator<ProductInfo> superArrayComparator = new Comparator<ProductInfo>() {
		public int compare(ProductInfo c1, ProductInfo c2) {
		Integer custCount1=c1.getCount();
		Integer custCount2=c2.getCount();
		
		return custCount1.compareTo(custCount2);
		}
	};


	
	
	
/*****************************************************Getters And Setters*********************************/
	

	public SuggestBox getProdID() {
		return prodID;
	}


	public void setProdID(SuggestBox prodID) {
		this.prodID = prodID;
	}


	public SuggestBox getProdCode() {
		return prodCode;
	}


	public void setProdCode(SuggestBox prodCode) {
		this.prodCode = prodCode;
	}


	public SuggestBox getProdName() {
		return prodName;
	}


	public void setProdName(SuggestBox prodName) {
		this.prodName = prodName;
	}


	public ProductInfo getPentity() {
		return this.pentity;
	}


	public void setPentity(ProductInfo pentity) {
		this.pentity = pentity;
	}
	
	
	
	
	private void resetBorderColor(Widget widg)
	 {
		 widg.getElement().getStyle().clearBorderColor();
		
		 
	 }

	private void changeBorderColor(Widget widg,String color)
	 {
		 widg.getElement().getStyle().setBorderColor(color);
	 }


	public HashMap<String, ProductInfo> getProdCodetoProdInfo() {
		return prodCodetoProdInfo;
	}


	public void setProdCodetoProdInfo(
			HashMap<String, ProductInfo> prodCodetoProdInfo) {
		this.prodCodetoProdInfo = prodCodetoProdInfo;
	}


	public HashMap<String, ProductInfo> getProdNametoProdInfo() {
		return prodNametoProdInfo;
	}


	public void setProdNametoProdInfo(
			HashMap<String, ProductInfo> prodNametoProdInfo) {
		this.prodNametoProdInfo = prodNametoProdInfo;
	}


	public HashMap<String, ProductInfo> getProdCatetoProdInfo() {
		return prodCatetoProdInfo;
	}


	public void setProdCatetoProdInfo(
			HashMap<String, ProductInfo> prodCatetoProdInfo) {
		this.prodCatetoProdInfo = prodCatetoProdInfo;
	}


	public HashMap<Double, ProductInfo> getProdPricetoProdInfo() {
		return prodPricetoProdInfo;
	}


	public void setProdPricetoProdInfo(
			HashMap<Double, ProductInfo> prodPricetoProdInfo) {
		this.prodPricetoProdInfo = prodPricetoProdInfo;
	}


	public HashMap<Integer, ProductInfo> getProdIDtoProdInfo() {
		return prodIDtoProdInfo;
	}


	public void setProdIDtoProdInfo(HashMap<Integer, ProductInfo> prodIDtoProdInfo) {
		this.prodIDtoProdInfo = prodIDtoProdInfo;
	}


	public FormField getProductCategory() {
		return productCategory;
	}


	public void setProductCategory(FormField productCategory) {
		this.productCategory = productCategory;
	}


	public FormField getProductPrice() {
		return productPrice;
	}


	public void setProductPrice(FormField productPrice) {
		this.productPrice = productPrice;
	}


	public TextBox getProdCategory() {
		return prodCategory;
	}


	public void setProdCategory(TextBox prodCategory) {
		this.prodCategory = prodCategory;
	}


	public DoubleBox getProdPrice() {
		return prodPrice;
	}


	public void setProdPrice(DoubleBox prodPrice) {
		this.prodPrice = prodPrice;
	}


	public TextBox getUnitOfmeasuerment() {
		return unitOfmeasuerment;
	}


	public void setUnitOfmeasuerment(TextBox unitOfmeasuerment) {
		this.unitOfmeasuerment = unitOfmeasuerment;
	}


	public FormField getProductCode() {
		return productCode;
	}


	public void setProductCode(FormField productCode) {
		this.productCode = productCode;
	}


	public FormField getProductName() {
		return productName;
	}


	public void setProductName(FormField productName) {
		this.productName = productName;
	}


	public FormField getProductID() {
		return productID;
	}


	public void setProductID(FormField productID) {
		this.productID = productID;
	}


	@Override
	public void onClick(ClickEvent event) {
		Console.log("Person info clicked ...");
		if(event.getSource()==btnNew){
			Console.log("new button clicked ...");
			genPopup=new GeneralViewDocumentPopup();
			genPopup.getBtnClose().addClickHandler(this);
			genPopup.setProductCompositeScreen(classNameValue, null, pentity);
		}
		if(event.getSource()==btnEdit){
			
			if(prodID.getValue()==null||prodID.getValue().equals("")
					||prodCode.getValue()==null||prodCode.getValue().equals("")
					||prodName.getValue()==null||prodName.getValue().equals("")){
				
				pentity=null;
			}
			Console.log("edit button clicked ...");
			if(pentity!=null){
				loadProduct(pentity);
			}else{
				alert.alert("Please select product");
			}
			
		}
		
		if(event.getSource()==btnClr){
			prodID.setText("");
			prodCode.setText("");
			prodName.setText("");
			pentity=null;
		}
		try{
			if(event.getSource()==genPopup.getBtnClose()){
				Console.log("close button clicked ...");
				try{
					reactOnBtnClose();
				}catch(Exception e){
					alert.alert(e.getMessage());
				}
			}
		}catch(Exception e){
			
		}
	}
	
	private void loadProduct(ProductInfo info) {
		Console.log("Inside load method : product id - "+info.getProdID()+" / "+classNameValue);
		genPopup=new GeneralViewDocumentPopup();
		genPopup.getBtnClose().addClickHandler(this);
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter temp = null;
		
		temp=new Filter();
		temp.setQuerryString("companyId");
		temp.setLongValue(UserConfiguration.getCompanyId());
		filtervec.add(temp);
		
		temp = new Filter();
		temp.setIntValue(info.getProdID());
		temp.setQuerryString("count");
		filtervec.add(temp);
		
		MyQuerry querry = new MyQuerry();
		querry.setFilters(filtervec);
		
		if(classNameValue.equals(AppConstants.PRODUCTCOMPOSITEITEM)){
			querry.setQuerryObject(new ItemProduct());
		}else if(classNameValue.equals(AppConstants.PRODUCTCOMPOSITESERVICE)){
			querry.setQuerryObject(new ServiceProduct());
		}else{
			return;
		}
		
		
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				if(result!=null&&result.size()!=0){
					if(classNameValue.equals(AppConstants.PRODUCTCOMPOSITEITEM)){
						ItemProduct obj=(ItemProduct) result.get(0);
						genPopup.setProductCompositeScreen(classNameValue, obj,pentity);
					}else if(classNameValue.equals(AppConstants.PRODUCTCOMPOSITESERVICE)){
						ServiceProduct obj=(ServiceProduct) result.get(0);
						genPopup.setProductCompositeScreen(classNameValue, obj,pentity);
					}
				}
			}
		});
		
	}


	private void reactOnBtnClose() {
		// TODO Auto-generated method stub
		ScreeenState scrState =genPopup.screenState;
		Console.log("In Comp ScreeenState : "+scrState);
		if(genPopup!=null){
			if(genPopup.getPresenter()!=null){
				if(genPopup.getPresenter().getModel() instanceof ItemProduct){
					Console.log("Item Product instances "+AppMemory.getAppMemory().currentState);
					itemProductObj=(ItemProduct) genPopup.getPresenter().getModel();
					
					if(itemProductObj!=null&&itemProductObj.getCount()!=0){
						
						ProductInfo info=new ProductInfo();
						info.setProdID(itemProductObj.getCount());
						info.setProductName(itemProductObj.getProductName());
						info.setProductCode(itemProductObj.getProductCode());
						info.setProductCategory(itemProductObj.getProductCategory());
						info.setProductPrice(itemProductObj.getPrice());
						globalItemProdArray.add(info);
				    	
						int id=info.getProdID();
						String prodname=info.getProductName();
						String prodcode=info.getProductCode();
						String prodcate=info.getProductCategory();
						double price=info.getProductPrice();
						
						prodIDtoProdInfo.put(id,info);
						prodCodetoProdInfo.put(prodcode,info);
						prodNametoProdInfo.put(prodname,info);
						prodCatetoProdInfo.put(prodcate,info);
						prodPricetoProdInfo.put(price,info);
						
						
						initalizeNameOracle();
						initalizeIdOracle();
						initalizeCodeOracle();
						prodCategory.setValue("");
						
						if(scrState.equals(ScreeenState.NEW)||scrState.equals(ScreeenState.EDIT)){
							setValue(info);
							pentity=info;
						}
					}
				}else if(genPopup.getPresenter().getModel() instanceof ServiceProduct){
					Console.log("Service Product instances "+AppMemory.getAppMemory().currentState);
					serviceProductObj=(ServiceProduct) genPopup.getPresenter().getModel();
					
					if(serviceProductObj!=null&&serviceProductObj.getCount()!=0){
						
						ProductInfo info=new ProductInfo();
						info.setProdID(serviceProductObj.getCount());
						info.setProductName(serviceProductObj.getProductName());
						info.setProductCode(serviceProductObj.getProductCode());
						info.setProductCategory(serviceProductObj.getProductCategory());
						info.setProductPrice(serviceProductObj.getPrice());
						globalServiceProdArray.add(info);
				    	
						int id=info.getProdID();
						String prodname=info.getProductName();
						String prodcode=info.getProductCode();
						String prodcate=info.getProductCategory();
						double price=info.getProductPrice();
						
						prodIDtoProdInfo.put(id,info);
						prodCodetoProdInfo.put(prodcode,info);
						prodNametoProdInfo.put(prodname,info);
						prodCatetoProdInfo.put(prodcate,info);
						prodPricetoProdInfo.put(price,info);
						
						
						initalizeNameOracle();
						initalizeIdOracle();
						initalizeCodeOracle();
						prodCategory.setValue("");
						
						if(scrState.equals(ScreeenState.NEW)||scrState.equals(ScreeenState.EDIT)){
							setValue(info);
							pentity=info;
						}
					}
				}
			}
		}
		/**
		 * Need to update current screen's Current state
		 */
		AppMemory.getAppMemory().currentState=scrState;
	}
	
}
