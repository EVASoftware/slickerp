package com.slicktechnologies.client.views.purchase.purchaserequisition;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.code.p.gwtchismes.client.GWTCGlassPanel;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.SelectionCell;
import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.BrowserEvents;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.TaxDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetailsPO;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;
import com.slicktechnologies.shared.common.inventory.WareHouse;

public class ProductTablePR extends SuperTable<ProductDetails> {

	NumberFormat nf=NumberFormat.getFormat("0.00");
	TextColumn<ProductDetails> prodID;
	TextColumn<ProductDetails> prodcode;
	TextColumn<ProductDetails> prodname;
	TextColumn<ProductDetails> prodcategory;
	
	Column<ProductDetails, String> editprodname;//Ashwini Patil Date:16-02-2023
	TextColumn<ProductDetails> viewPriceColumn;
	Column<ProductDetails, String> prodprice;
	
	TextColumn<ProductDetails> viewprodQty;
	Column<ProductDetails, String> prodQty;
	
	TextColumn<ProductDetails> UnitColumn;
	
	Column<ProductDetails, String> deleteColumn;
	
	
	/**
	 * warehouse ,exp. delivery date and avilable stock coloumn is added.
	 * Date : 26-09-2016 By Anil
	 * Release :30 Sept 2016
	 * Project: PURCHASE MODIFICATION (NBHC)
	 */
	TextColumn<ProductDetails> warehouseColumn;
	Column<ProductDetails,String> warehouseListColumn;
	TextColumn<ProductDetails> expDeliveryDateColumn;
	Column<ProductDetails,Date> editableExpDeliveryDateColumn;
	TextColumn<ProductDetails> availableStockColumn;
	ArrayList<String> warehouseList;
	ArrayList<WareHouse> warehouseEntityList;
	protected GWTCGlassPanel glassPanel;
	public static boolean isValidWarehouse=true;
	
	/**
	 * End
	 */
	
	
	/**
	 * PR Id Column
	 * Date : 11-10-2016 By Anil
	 * Release : 30 sept 2016
	 * Project : PURCHASE MODIFICATION (NBHC)
	 */
	TextColumn<ProductDetails> getPrIdColumn;
	/**
	 * End
	 */

	/**
	 * Date 26-04-2017 By Vijay
	 * Total Column And Warehouse Address column
	 * Project : Deadstock (NBHC)
	 */
	TextColumn<ProductDetails> totalColumn;
	TextColumn<ProductDetails> warehouseAddressColumn; 
	/**
	 * ends here
	 */
	public ProductTablePR(){
		
		table.setWidth("100%");
		table.setHeight("350px");
	}
	
	
	
	@Override
	public void createTable() {
		System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@ 1");
		getPrIdColumn();

		addColumnEditProductName();
		addColumnUnitOfMeasurement();
		addColumnQty();
		addColumnVendorprodprice();
		addColumnTotal();
		/**
		 * Date 28-1-2019 by vijay for NBHC Inventory Management delivery date not required with process config
		 */
		if(!AppUtility.checkForProcessConfigurartionIsActiveOrNot("PurchaseRequisition", "MakeExpectedDeliveryDateNonMandatory")){
			addEditableExpDeliveryDateColoumn();
		}
		/**
		 * ends here
		 */
		retrieveWarehouseNameList();
//		createColumndeleteColumn();
//		addFieldUpdater();
		
		
//		table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);

	}

	

	public void addeditColumn() {
		
		getPrIdColumn();
		addColumnprodID();
		addColumnprodcode();

		addColumnEditProductName();
		addColumnprodcategory();
		addColumnVendorprodprice();
		addColumnQty();
		addColumnUnitOfMeasurement();
		addColumnTotal();
		/**
		 * Date 28-1-2019 by vijay for NBHC Inventory Management delivery date not required with process config
		 */
		if(!AppUtility.checkForProcessConfigurartionIsActiveOrNot("PurchaseRequisition", "MakeExpectedDeliveryDateNonMandatory")){
			addEditableExpDeliveryDateColoumn();
		}
		
		retrieveWarehouseNameList();
		addWarehouseAddressColumn();
		
//		createColumndeleteColumn();
//		addFieldUpdater();
		
		
//		table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);

	}

	public void addViewColumn() {
		getPrIdColumn();
		addColumnprodID();
		addColumnprodcode();
		addColumnprodname();
		addColumnprodcategory();
		addColumnViewPrice();
		addColumnviewQty();
		addColumnUnitOfMeasurement();
		addColumnTotal();
		/**
		 * Date 28-1-2019 by vijay for NBHC Inventory Management delivery date not required with process config
		 */
		if(!AppUtility.checkForProcessConfigurartionIsActiveOrNot("PurchaseRequisition", "MakeExpectedDeliveryDateNonMandatory")){
			addExpDeliveryDateColoumn();
		}
		addWarehouseColoumn();
		addAvailableStockColoumn();
		addWarehouseAddressColumn();

	}

	@Override
	public void addFieldUpdater() {
		columnEditPrice();
		createFieldUpdaterDelete();
		createFieldUpdaterprodQtyColumn();
		if(!AppUtility.checkForProcessConfigurartionIsActiveOrNot("PurchaseRequisition", "MakeExpectedDeliveryDateNonMandatory")){
			setFieldUpdatorOnDeliveryDate();
		}
		setFieldUpdatorOnWarehouse();
	}
	
	@Override
	public void setEnable(boolean state) {
		int tablecolcount = this.table.getColumnCount();
		for (int i = tablecolcount - 1; i > -1; i--){
			table.removeColumn(i);
		}
	
		if (state == true){
			System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@ 2");
			addeditColumn();
		}
		if (state == false){
			System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@ 3");
			addViewColumn();
		}
	}
	
	/**
	 * Date 26-04-2017 By Vijay
	 * Total Column 
	 * Project : Deadstock (NBHC)
	 */
	private void addColumnTotal() {
		totalColumn = new TextColumn<ProductDetails>() {
			
			@Override
			public String getValue(ProductDetails object) {
				double total = object.getProdPrice()*object.getProductQuantity();
				object.setTotal(total);
				return nf.format(object.getTotal());
			}
		};
		table.addColumn(totalColumn,"Total");
		table.setColumnWidth(totalColumn, 90,Unit.PX);
	}
	
	
	/*********************************************Product View Column ********************************************/
	
	/**
	 * This method loads all the active warehouse in selection list.
	 * purpose of loading warehouse is that product should be delivered in that warehouse.
	 * Date:27-09-2016 By Anil
	 * Release :30 Sept 2016 
	 * Project : PURCHASE MODIFICATION(NBHC) 
	 */
	
	public void retrieveWarehouseNameList(){
		final GenricServiceAsync service = GWT.create(GenricService.class);
		MyQuerry query = new MyQuerry();
		System.out.println("Company Id :: "+UserConfiguration.getCompanyId());
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);
		
		if(AppUtility.isBranchRestricted()){
			if (!LoginPresenter.myUserEntity.getRole().getRoleName().trim().equalsIgnoreCase("ADMIN")) {
				List<String> list=new ArrayList<String>();
				for(Branch branch:LoginPresenter.globalBranch){
					list.add(branch.getBusinessUnitName());
				}
				
				 filter=new Filter();
                 filter.setQuerryString("branchdetails.branchName IN");
                 filter.setList(list);
                 filtervec.add(filter);
			}
		}
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(UserConfiguration.getCompanyId());
		filtervec.add(filter);
		
		query.setFilters(filtervec);
		query.setQuerryObject(new WareHouse());
		
		service.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				warehouseList=new ArrayList<String>();
				warehouseEntityList=new ArrayList<WareHouse>();
				warehouseList.add("");
				if(result.size()!=0){
					for(SuperModel model:result){
						WareHouse entity=(WareHouse) model;
						warehouseList.add(entity.getBusinessUnitName());
						warehouseEntityList.add(entity);
					}
					Collections.sort(warehouseList);
				}
				
				
				addEditableWarehouseColoumn();
				addAvailableStockColoumn();
				
				addWarehouseAddressColumn();
				addColumnprodcategory();
				addColumnprodcode();
				addColumnprodID();
				createColumndeleteColumn();
				addFieldUpdater();
			}
			
			@Override
			public void onFailure(Throwable caught) {
				
			}
		});
	}
	
	/**
	 * Date 26-04-2017 By Vijay
	 * Warehouse Address column
	 * Project : Deadstock (NBHC)
	 */
	
	private void addWarehouseAddressColumn() {

		warehouseAddressColumn = new TextColumn<ProductDetails>() {
			
			@Override
			public String getValue(ProductDetails object) {
				return object.getWarehouseAddress();
			}
		};
		table.addColumn(warehouseAddressColumn, "Warehouse Address");
		table.setColumnWidth(warehouseAddressColumn, 200,Unit.PX);
	}
	
	public void addWarehouseColoumn(){
		warehouseColumn=new TextColumn<ProductDetails>() {
			@Override
			public String getValue(ProductDetails object) {
				if(object.getWarehouseName()!=null){
					return object.getWarehouseName();
				}
				return "";
			}
		};
		table.addColumn(warehouseColumn, "#Warehouse");
		table.setColumnWidth(warehouseColumn, 150,Unit.PX);
		warehouseColumn.setCellStyleNames("wordWrap");
	}
	
	public void addEditableWarehouseColoumn(){
		SelectionCell selectionCell= new SelectionCell(warehouseList);
		warehouseListColumn=new Column<ProductDetails, String>(selectionCell) {
			
			@Override
			public String getCellStyleNames(Context context, ProductDetails object) {
				 
				if(object.getWarehouseName()==null){
					 return "red";
				 }
				return "";
			}
			@Override
			public String getValue(ProductDetails object) {
				if(object.getWarehouseName()!=null){
					return object.getWarehouseName();
				}
				return "";
			}
		};
		table.addColumn(warehouseListColumn, "#Warehouse");
		table.setColumnWidth(warehouseListColumn, 150,Unit.PX);
		warehouseListColumn.setCellStyleNames("wordWrap");
		
	}
	
	public void addExpDeliveryDateColoumn(){
		expDeliveryDateColumn=new TextColumn<ProductDetails>() {
			@Override
			public String getValue(ProductDetails object) {
				if(object.getProdDate()!=null){
					return AppUtility.parseDate(object.getProdDate());
				}
				return "";
			}
		};
		table.addColumn(expDeliveryDateColumn, "#Delivery Date");
		table.setColumnWidth(expDeliveryDateColumn, 110,Unit.PX);
		expDeliveryDateColumn.setCellStyleNames("wordWrap");
	}
	public void addEditableExpDeliveryDateColoumn(){
		final DateTimeFormat fmt = DateTimeFormat.getFormat("dd/MM/yyyy");
		DatePickerCell date= new DatePickerCell(fmt);
		editableExpDeliveryDateColumn=new Column<ProductDetails,Date>(date) {
			@Override
			public Date getValue(ProductDetails object) {
				if(object.getProdDate()!=null){
					return fmt.parse(fmt.format(object.getProdDate()));
				}else{
					object.setProdDate(new Date());
					return fmt.parse(fmt.format(new Date()));
				}
			}
		};
		table.addColumn(editableExpDeliveryDateColumn, "#Delivery Date");
		table.setColumnWidth(editableExpDeliveryDateColumn, 110,Unit.PX);
		editableExpDeliveryDateColumn.setCellStyleNames("wordWrap");
	}
	
	public void addAvailableStockColoumn(){
		availableStockColumn=new TextColumn<ProductDetails>() {
			@Override
			public String getValue(ProductDetails object) {
//				if(object.getAvailableStock()!=0){
					return object.getAvailableStock()+"";
//				}
//				return "";
			}
		};
		table.addColumn(availableStockColumn, "Available Stock");
		table.setColumnWidth(availableStockColumn, 90,Unit.PX);
		availableStockColumn.setCellStyleNames("wordWrap");
	}
	
	private void setFieldUpdatorOnWarehouse(){
		warehouseListColumn.setFieldUpdater(new FieldUpdater<ProductDetails, String>() {
			@Override
			public void update(int index, ProductDetails object, String value) {
				
				if(validWarehouseSelection(object.getPrCount(),object.getProductID(), value)){
					isValidWarehouse=true;
				}else{
					isValidWarehouse=false;
				}
				setAvailableStock(index,object,value);
				
				//Below Method added by vijay for setting warehouse address for Deadstock NBHC on 26-04-2017
				setwarehouseAddress(index,object,value);
			}
		});
	}
	
	private void setFieldUpdatorOnDeliveryDate(){
		editableExpDeliveryDateColumn.setFieldUpdater(new FieldUpdater<ProductDetails, Date>() {
			@Override
			public void update(int index, ProductDetails object, Date value) {
				object.setProdDate(value);
				table.redrawRow(index);				
			}
		});
	}
	
	
	/**
	 * Date 26-04-2017
	 * added by vijay Deadstock NBHC
	 * for setting warehouse address
	 */
	private void setwarehouseAddress(int index, ProductDetails object, String warehouseName) {
		
		for(int i=0;i<warehouseEntityList.size();i++){
			if(warehouseEntityList.get(i).getBusinessUnitName().equals(warehouseName)){
				object.setWarehouseAddress(warehouseEntityList.get(i).getAddress().getCompleteAddress());
				break;
			}
		}
		table.redrawRow(index);
		
	}
	
	/**
	 * 
	 * @param productID
	 * @param value -warehouse name
	 * @return true/false
	 * 
	 * This method checks whether selected warehouse for given product id is already added.
	 * if added then return false else true.
	 * Date : 27-09-2016 By Anil
	 * Release : 30 Sept 2016
	 * Project: PURCHASE MODIFICATION(NBHC)
	 */
	private boolean validWarehouseSelection(int prCount,int productID, String value) {
		List<ProductDetails> list=getDataprovider().getList();
		for(ProductDetails prod:list){
			if(prCount==prod.getPrCount()&&productID==prod.getProductID()
					&&value.trim().equals(prod.getWarehouseName().trim())){
				GWTCAlert alert =new GWTCAlert();
				alert.alert("Warehouse is already selected for product !");
				return false;
			}
		}
		return true;
	}
	
	
	/**
	 * 
	 * @param index -selected row
	 * @param object - selected row data
	 * @param value - updated warehouse name
	 * 
	 * This method return the available Stock of product in selected warehouse.
	 * Date :27-09-2016 By Anil
	 * Release : 30 Sept 2016
	 * Project : PURCHASE MODIFICATION(NBHC)
	 */
	
	private void setAvailableStock(final int index, final ProductDetails object, final String value) {
		final GenricServiceAsync service = GWT.create(GenricService.class);
		MyQuerry query = new MyQuerry();
		System.out.println("Company Id :: "+UserConfiguration.getCompanyId()+" Product Id :: "+object.getProductID()+" Warehouse  :: "+value);
		
		
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(UserConfiguration.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("productinfo.prodID");
		filter.setIntValue(object.getProductID());
		filtervec.add(filter);
		
//		filter = new Filter();
//		filter.setQuerryString("details.warehousename");
//		filter.setStringValue(value.trim());
//		filtervec.add(filter);
		
		query.setFilters(filtervec);
		query.setQuerryObject(new ProductInventoryView());
		showWaitSymbol();
		service.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				hideWaitSymbol();
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				hideWaitSymbol();
				System.out.println("RESULT SIZE : "+result.size());
				double avilableStock=0;
				if(result.size()!=0){
					ProductInventoryView entity=(ProductInventoryView) result.get(0);
					for(ProductInventoryViewDetails prod:entity.getDetails()){
						if(prod.getWarehousename().trim().equals(value.trim())){
							avilableStock=avilableStock+prod.getAvailableqty();
						}
					}
				}
				object.setAvailableStock(avilableStock);
				object.setWarehouseName(value);
				System.out.println("ROW INDEX "+index);
				if(isValidWarehouse){
					AppUtility.ApplyWhiteColoronRow(ProductTablePR.this, index);
				}else{
					AppUtility.ApplyRedColoronRow(ProductTablePR.this, index);
				}
				
				
				if(isValidWarehouse){
					List<ProductDetails> list=getDataprovider().getList();
					Integer rowIndex=getRowCountOfWarehouse(list);
					if(rowIndex!=null){
						isValidWarehouse=false;
						AppUtility.ApplyRedColoronRow(ProductTablePR.this, rowIndex);
					}else{
						isValidWarehouse=true;
						AppUtility.ApplyWhiteColoronRow(ProductTablePR.this, index);
					}
				}
				
				
				table.redraw();
				System.out.println("ROW UPDATED "+object.getWarehouseName()+" - "+object.getAvailableStock());
				
			}

			
		});
	}
	
	/**
	 * This method returns row index whose which has duplicate warehouse name for perticular product.
	 * Date : 28-09-2016 By Anil
	 * Release : 30 Sept 2016 
	 * Project : PURCHASE MODIFICATION(NBHC)
	 * 
	 */

	private Integer getRowCountOfWarehouse(List<ProductDetails> list) {
		for(ProductDetails prod1:list){
			int count=0;
			int index=0;
			for(ProductDetails prod2:list){
				if(prod1.getPrCount()==prod2.getPrCount()
						&&prod1.getProductID()==prod2.getProductID()
						&&prod1.getWarehouseName().equals(prod2.getWarehouseName())){
					count++;
				}
				if(count==2){
					return index;
				}
				index++;
			}
		}
		return null;
	}
	
	
	private void getPrIdColumn() {
		getPrIdColumn = new TextColumn<ProductDetails>() {

			@Override
			public String getValue(ProductDetails object) {
				if(object.getPrCount()!=0){
					return object.getPrCount() + "";
				}
				return "";
			}
		};
		table.addColumn(getPrIdColumn, "PR ID");
		table.setColumnWidth(getPrIdColumn, 100,Unit.PX);
		getPrIdColumn.setCellStyleNames("wordWrap");
	}

	public void addColumnprodID() {
		prodID = new TextColumn<ProductDetails>() {

			@Override
			public String getValue(ProductDetails object) {
				return object.getProductID() + "";
			}
		};
		table.addColumn(prodID, "ID");
		table.setColumnWidth(prodID, 100,Unit.PX);
		prodID.setCellStyleNames("wordWrap");
	}
	
	public void addColumnprodcode() {
		prodcode = new TextColumn<ProductDetails>() {

			@Override
			public String getValue(ProductDetails object) {
				return object.getProductCode();
			}
		};
		table.addColumn(prodcode, "Code");
		table.setColumnWidth(prodcode, 100,Unit.PX);
		prodcode.setCellStyleNames("wordWrap");
	}

	public void addColumnprodname() {
		prodname = new TextColumn<ProductDetails>() {

			@Override
			public String getValue(ProductDetails object) {
				return object.getProductName();
			}
		};
		table.addColumn(prodname, "#Name");
		table.setColumnWidth(prodname, 120,Unit.PX);
		prodname.setCellStyleNames("wordWrap");
	}
	


	public void addColumnEditProductName() {
		EditTextCell editCell = new EditTextCell();
		editprodname = new Column<ProductDetails, String>(editCell) {

			@Override
			public String getValue(ProductDetails object) {
				return object.getProductName();
			}
		};
		table.addColumn(editprodname, "#Name");
		table.setColumnWidth(editprodname, 120, Unit.PX);
		editprodname.setCellStyleNames("wordWrap");
		
		editprodname.setFieldUpdater(new FieldUpdater<ProductDetails, String>() {
			
			@Override
			public void update(int index, ProductDetails object, String value) {
				// TODO Auto-generated method stub
				object.setProductName(value);
				table.redraw();
			}
		});
}
	
	public void addColumnprodcategory() {
		prodcategory = new TextColumn<ProductDetails>() {

			@Override
			public String getValue(ProductDetails object) {
				return object.getProductCategory();
			}
		};
		table.addColumn(prodcategory, "Category");
		table.setColumnWidth(prodcategory, 100,Unit.PX);
		prodcategory.setCellStyleNames("wordWrap");
	}
	
	//Read Only Price Column
	public void addColumnViewPrice() {
		viewPriceColumn = new TextColumn<ProductDetails>() {

			@Override
			public String getValue(ProductDetails object) {
				return object.getProdPrice() + "";
			}
		};
		table.addColumn(viewPriceColumn, "#Price");
		table.setColumnWidth(viewPriceColumn, 90,Unit.PX);
		viewPriceColumn.setCellStyleNames("wordWrap");
	}

	//Editable Price Column
	public void addColumnVendorprodprice() {
		EditTextCell editCell = new EditTextCell();
		prodprice = new Column<ProductDetails, String>(editCell) {

			@Override
			public String getValue(ProductDetails object) {
				return object.getProdPrice() + "";
			}
		};
		table.addColumn(prodprice, "#Price");
		table.setColumnWidth(prodprice, 90,Unit.PX);
		prodprice.setCellStyleNames("wordWrap");
	}
	//Read Only Quantity Column
	public void addColumnviewQty() {
		viewprodQty = new TextColumn<ProductDetails>() {

			@Override
			public String getValue(ProductDetails object) {
				return nf.format(object.getProductQuantity());
			}
		};
		table.addColumn(viewprodQty, "#Quantity");
		table.setColumnWidth(viewprodQty, 90,Unit.PX);
		viewprodQty.setCellStyleNames("wordWrap");
	}
	
	//Editable Quantity Column
	public void addColumnQty() {
		EditTextCell editCell = new EditTextCell();
		prodQty = new Column<ProductDetails, String>(editCell) {

			@Override
			public String getValue(ProductDetails object) {
				return nf.format(object.getProductQuantity());
			}
		};
		table.addColumn(prodQty, "#Quantity");
		table.setColumnWidth(prodQty, 90,Unit.PX);
		prodQty.setCellStyleNames("wordWrap");
	}
	
	protected void addColumnUnitOfMeasurement() {
		UnitColumn = new TextColumn<ProductDetails>() {

			@Override
			public String getValue(ProductDetails object) {
				if (object.getUnitOfmeasurement().equals(""))
					return "N/A";
				else
					return object.getUnitOfmeasurement();
			}
		};
		table.addColumn(UnitColumn, "UOM");
		table.setColumnWidth(UnitColumn, 70,Unit.PX);
		UnitColumn.setCellStyleNames("wordWrap");
	}

	public void createColumndeleteColumn() {
		ButtonCell btnCell = new ButtonCell();
		deleteColumn = new Column<ProductDetails, String>(btnCell) {
			@Override
			public String getValue(ProductDetails object) {
				return "Delete";
			}
		};
		table.addColumn(deleteColumn, "Delete");
		table.setColumnWidth(deleteColumn, 70,Unit.PX);

	}

	/****************************************Product Column Updater *******************************************/
	
	public void columnEditPrice() {
		prodprice.setFieldUpdater(new FieldUpdater<ProductDetails, String>() {
			@Override
			public void update(int index, ProductDetails object, String value) {
				try {
//					Integer val1 = Integer.parseInt(value.trim()); Deepak Salve Comment this code  
//					object.setProdPrice(val1); Deepak Comment this code
					
					// Date-09/12/2019 Deepak Salve add this code
					double val1=Double.parseDouble(value.trim());
					object.setProdPrice(val1);
					//total added by vijay for deadstock nbhc
					object.setTotal(val1*object.getProductQuantity());
					
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				} catch (NumberFormatException e) {
				}
				table.redrawRow(index);
			}
		});
	}

	public void createFieldUpdaterDelete() {
		deleteColumn.setFieldUpdater(new FieldUpdater<ProductDetails, String>() {
			@Override
			public void update(int index, ProductDetails object,String value) {
				System.out.println("PRODUCT LIST SIZE BEFOR : "+getDataprovider().getList().size());
				getDataprovider().getList().remove(object);
				table.redrawRow(index);
				System.out.println("PRODUCT LIST SIZE AFTER : "+getDataprovider().getList().size());
//				PurchaseRequitionPresenter.updateVendorTable();
				
			}
		});
	}

	protected void createFieldUpdaterprodQtyColumn() {
		prodQty.setFieldUpdater(new FieldUpdater<ProductDetails, String>() {
			@Override
			public void update(int index, ProductDetails object, String value) {

				try {
					double val1 = Double.parseDouble(value.trim());
					object.setProductQuantity(val1);
					object.setAcceptedQty(val1);
					//total added by vijay for deadstock nbhc
					object.setTotal(val1*object.getProdPrice());
					/*** Date 14-09-2019 by Vijay PR Qty set to PRGRNBalance Qty for NBHC Inventory management****/
					object.setGrnBalancedQty(val1);
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				} catch (NumberFormatException e) {

				}

				table.redrawRow(index);
			}
		});
	}

	
	
	
	@Override
	protected void initializekeyprovider() {

	}
	
	@Override
	public void applyStyle() {

	}
	
	
	public void showWaitSymbol() {
		glassPanel = new GWTCGlassPanel();
		glassPanel.show();
		
		
	}
	public void hideWaitSymbol() {
	   glassPanel.hide();
		
	}

}


