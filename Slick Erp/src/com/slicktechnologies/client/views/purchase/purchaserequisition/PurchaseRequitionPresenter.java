package com.slicktechnologies.client.views.purchase.purchaserequisition;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.google.gwt.view.client.RowCountChangeEvent.Handler;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.approvalutility.ApprovableFormScreenPresenter;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.EmailService;
import com.slicktechnologies.client.services.EmailServiceAsync;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.documentcancellation.DocumentCancellationPopUp;
import com.slicktechnologies.client.views.popups.NewEmailPopUp;
import com.slicktechnologies.client.views.purchase.letterofintent.LetterOfIntentForm;
import com.slicktechnologies.client.views.purchase.letterofintent.LetterOfIntentPresenter;
import com.slicktechnologies.client.views.purchase.purchaseorder.PurchaseOrderForm;
import com.slicktechnologies.client.views.purchase.purchaseorder.PurchaseOrderPresenter;
import com.slicktechnologies.client.views.purchase.requestquotation.RequestForQuotationForm;
import com.slicktechnologies.client.views.purchase.requestquotation.RequestForQuotationPresenter;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.LetterOfIntent;
import com.slicktechnologies.shared.common.businessprocesslayer.PriceList;
import com.slicktechnologies.shared.common.businessprocesslayer.PriceListDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetailsPO;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseRequisition;
import com.slicktechnologies.shared.common.businessprocesslayer.RequsestForQuotation;
import com.slicktechnologies.shared.common.businessprocesslayer.VendorDetails;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class PurchaseRequitionPresenter extends ApprovableFormScreenPresenter<PurchaseRequisition> implements Handler {

	static PurchaseRequisitionForm form;
	final GenricServiceAsync async = GWT.create(GenricService.class);
	EmailServiceAsync emailService = GWT.create(EmailService.class);
	
	DocumentCancellationPopUp cancelPopup = new DocumentCancellationPopUp();
	PopupPanel cancelpanel ;
	
	
	/**
	 * In this pop up we assign PR To Purchase Engg 
	 * so he can make PO against that pr
	 * Date : 15-10-2016 By Anil
	 * Release : 30 Sept 2016 
	 * Project : PURCHASE REQUISITION (NBHC)
	 */
	AssignPrToPoPurchaseEnggPopup assignPopup=new AssignPrToPoPurchaseEnggPopup();
	
 	NewEmailPopUp emailpopup = new NewEmailPopUp();


	public PurchaseRequitionPresenter(FormScreen<PurchaseRequisition> view,PurchaseRequisition model) {
		super(view, model);
		form = (PurchaseRequisitionForm) view;
		
		cancelPopup.getBtnOk().addClickHandler(this);
		cancelPopup.getBtnCancel().addClickHandler(this);
		form.productTablePR.getTable().addRowCountChangeHandler(this);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.PURCHASEREQUISITE,LoginPresenter.currentModule.trim());
		  if(isDownload==false){
			 form.getSearchpopupscreen().getDwnload().setVisible(false);
		  }
		  
		  
		  assignPopup.getBtnOk().addClickHandler(this);
		  assignPopup.getBtnCancel().addClickHandler(this);

	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		super.reactToProcessBarEvents(e);
		InlineLabel label = (InlineLabel) e.getSource();
		String text = label.getText().trim();

		if (text.equals("Email"))
			reactOnEmail();

		if (text.equals("New")) {
			form.setToNewState();
			initalize();
		}
		if (text.equals("Create RFQ")) {
			reactToRFQ();
		}
		if (text.equals("Create LOI")) {
			reactToLOI();
		}
		if (text.equals("Create PO")) {
			if(AppUtility.getPrType(model.getPrproduct())!=null){
				if(AppUtility.getPrType(model.getPrproduct()).equals("Normal PR")){
					System.out.println("Normal PR");
					reactToPO();
				}else {
					System.out.println("Service PR");
					reactToServicePO();
				}
			}else{
				form.showDialogMessage("Product information not found,Please check.");
			}
		}
		if (text.equals("Mark Close")) {
			ChangeStatus(PurchaseRequisition.CLOSE);
			form.toggleProcessLevelMenu();
		}
		if (text.equals(AppConstants.CANCELPR)) {
			
			cancelPopup.getPaymentDate().setValue(new Date()+"");
			cancelPopup.getPaymentID().setValue(model.getCount()+"");
			cancelPopup.getPaymentStatus().setValue(model.getStatus());
			cancelpanel=new PopupPanel(true);
			cancelpanel.add(cancelPopup);
			cancelpanel.show();
			cancelpanel.center();
		}
		
		if (text.equals(AppConstants.ASSIGNPRTOPOPURCHASEENGG)) {
			cancelpanel=new PopupPanel(true);
			assignPopup.clear();
			cancelpanel.add(assignPopup);
			cancelpanel.show();
			cancelpanel.center();
		}
	}

	
	@Override
	public void reactOnPrint() {

	}

	@Override
	public void reactOnEmail() {
		
//		boolean conf = Window.confirm("Do you really want to send email?");
//		if (conf == true) {
//			emailService.initiatePrEmail((PurchaseRequisition) model,new AsyncCallback<Void>() {
//				@Override
//				public void onFailure(Throwable caught) {
//					Window.alert("Resource Quota Ended ");
//					caught.printStackTrace();
//				}
//
//				@Override
//				public void onSuccess(Void result) {
//					Window.alert("Email Sent Sucessfully !");
//				}
//			});
//		}
		
		/**
		 * @author Vijay Date 19-03-2021
		 * Des :- above old code commented 
		 * Added New Email Popup Functionality
		 */
		emailpopup.showPopUp();
		setEmailPopUpData();
		
		/**
		 * ends here
		 */
	}

	@Override
	public void reactOnDownload() {
		
		CsvServiceAsync async = GWT.create(CsvService.class);
		ArrayList<PurchaseRequisition> PRArray = new ArrayList<PurchaseRequisition>();
		List<PurchaseRequisition> list = form.getSearchpopupscreen()
				.getSupertable().getDataprovider().getList();
		PRArray.addAll(list);
		/** date 02/02/2018 added by komal to download data with approvestatus column **/
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("PurchaseRequisition","AddApprovalStatusInPR"))
		{
			getApprovalStatus("Purchase Requisition", PRArray);
		}else{
			
		async.setPR(PRArray, new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed" + caught);
			}
			@Override
				public void onSuccess(Void result) {

					String gwt = com.google.gwt.core.client.GWT
							.getModuleBaseURL();
					final String url = gwt + "csvservlet" + "?type=" + 47;
					Window.open(url, "test", "enabled");
				}
			});
		}
	}

	@Override
	protected void makeNewModel() {
		model = new PurchaseRequisition();

	}

	public static PurchaseRequisitionForm initalize() {
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Purchase Requisition", Screen.PURCHASEREQUISITE);
		PurchaseRequisitionForm form = new PurchaseRequisitionForm();

		PurchaseRequisitionPresenterTable gentable = new PurchaseRequisitionPresenterTable();
		gentable.setView(form);
		gentable.applySelectionModle();

		PurchaseRequisitionPresenterSearch.staticSuperTable = gentable;
		PurchaseRequisitionPresenterSearch searchpopup = new PurchaseRequisitionPresenterSearch();
		form.setSearchpopupscreen(searchpopup);

		PurchaseRequitionPresenter presenter = new PurchaseRequitionPresenter(form, new PurchaseRequisition());
		AppMemory.getAppMemory().stickPnel(form);
		return form;

	}

	/******************************************** react to RFQ ***********************************************************/

	public void reactToRFQ() {
		final RequestForQuotationForm form = RequestForQuotationPresenter.initalize();
		final RequsestForQuotation rfq = new RequsestForQuotation();
		
		rfq.setPurchaseReqNo(model.getCount());
		rfq.setPrDate(model.getCreationDate());
		rfq.setRefOrderNO(model.getRefOrderNO());
		rfq.setProject(model.getProject());
		rfq.setDeliveryDate(model.getExpectedDeliveryDate());
		rfq.setBranch(model.getBranch());
		rfq.setEmployee(model.getEmployee());
		rfq.setApproverName(model.getApproverName());
		
//		ArrayList<ProductDetails> productList = model.getPrproduct();
		
		ArrayList<ProductDetails> productList = getUniqueProductDetailsList(model.getPrproduct());
		
		rfq.setProductinfo(productList);
		
//		ArrayList<VendorDetails> vendorList = setCommonVendors(model.getVendlist());
//		rfq.setVendorinfo(vendorList);
		
		
		/**
		 * Copying some more info of PR to RFQ
		 * Date : 09-09-2016 By Anil
		 * Release : 31 Aug 2016
		 * Project : PURCHASE ERROR SOLVING (NBHC)
		 */
		
//		form.vendorRateList=this.form.vendorRateList;
		rfq.setQuotationName(model.getPrTitle());
		rfq.setResponseDate(model.getResponseDate());
//		rfq.setExpectedClosureDate(model.getExpectedClosureDate());
		rfq.setHeader(model.getHeader());
		rfq.setFooter(model.getFooter());
		
		/**
		 * END
		 */
		
		
		///////
		rfq.setAssignTo1(model.getAssignTo1());
		rfq.setAssignTo2(model.getAssignTo2());
		
		//////
		
		

		Timer t = new Timer() {
			@Override
			public void run() {
				form.setToNewState();
				form.updateView(rfq);
				form.setEnableFalse();
				form.getPurchaseRequisiteNo().setEnabled(false);
				form.getIbRequestQuotationID().setEnabled(false);
				form.getPrDate().setEnabled(false);
				
				form.getBranch().setEnabled(true);
				form.getOblEmployee().setEnabled(true);
				form.getTbApporverName().setEnabled(true);
				
				
				///////////
				
				form.getDbCreationDate().setEnabled(true);
				form.getDbExpdeliverydate().setEnabled(true);
				
				form.getTbName().setEnabled(true);
				form.getDbResponseDate().setEnabled(true);
//				form.getDbExpectedClosureDate().setEnabled(true);
				form.getHeader().setEnabled(true);
				form.getFooter().setEnabled(true);
				
				form.getbAddproducts().setEnabled(false);
				form.productInfoComposite.setEnable(false);
				
				//////////
				
				
			}
		};
		t.schedule(5000);
	}

	/************************************************* react on loi ******************************************************/

	public void reactToLOI() {
		final LetterOfIntentForm form = LetterOfIntentPresenter.initalize();
		final LetterOfIntent loi = new LetterOfIntent();
		
		loi.setPurchaseReqNo(model.getCount());
		loi.setPrDate(model.getCreationDate());
		loi.setRefOrderNO(model.getRefOrderNO());
		loi.setProject(model.getProject());
		loi.setDeliveryDate(model.getExpectedDeliveryDate());
		loi.setBranch(model.getBranch());
		loi.setEmployee(model.getEmployee());
		loi.setApproverName(model.getApproverName());

//		ArrayList<ProductDetails> productList = model.getPrproduct();
		
		ArrayList<ProductDetails> productList = getUniqueProductDetailsList(model.getPrproduct());
		
		
		
		loi.setProductinfo(productList);
		
//		ArrayList<VendorDetails> vendorList = setCommonVendors(model.getVendlist());
//		loi.setVendorinfo(vendorList);
		
		/**
		 * Copying some more info of PR to LOI
		 * Date : 09-09-2016 By Anil
		 * Release : 31 Aug 2016
		 * Project : PURCHASE ERROR SOLVING (NBHC)
		 */
		
		loi.setTitle(model.getPrTitle());
		loi.setResponseDate(model.getResponseDate());
//		loi.setExpectedClosureDate(model.getExpectedClosureDate());
		loi.setHeader(model.getHeader());
		loi.setFooter(model.getFooter());
		
		/**
		 * END
		 */
		
		
		
		///////
		loi.setAssignTo1(model.getAssignTo1());
		loi.setAssignTo2(model.getAssignTo2());
		
		//////

		Timer t = new Timer() {
			@Override
			public void run() {
				form.setToNewState();
				form.updateView(loi);
				form.setEnableFalse();
				form.getBranch().setEnabled(true);
				form.getOblEmployee().setEnabled(true);
				form.getTbApporverName().setEnabled(true);
				form.getIbreqforQuotation().setEnabled(false);
				form.getPurchaseRequisiteNo().setEnabled(false);
				form.getIbLetterOfIntentID().setEnabled(false);

				
				
				///////////
				
				form.getDbCreationDate().setEnabled(true);
				form.getDbExpdeliverydate().setEnabled(true);
				
				form.getTbtitle().setEnabled(true);
				form.getDbExpectedResposeDate().setEnabled(true);
//				form.getDbExpectedClosureDate().setEnabled(true);
				form.getHeader().setEnabled(true);
				form.getFooter().setEnabled(true);
				
				
				form.getbAddproducts().setEnabled(false);
				form.productInfoComposite.setEnable(false);
				
				
				
				//////////
						
				
			}
		};
		t.schedule(5000);
	}

	/******************************************** react to po *******************************************************/

	public void reactToPO() {
		PurchaseOrderPresenter.vendorState="";
		final PurchaseOrderForm form = PurchaseOrderPresenter.initalize();
		final PurchaseOrder loi = new PurchaseOrder();
		
		loi.setPurchaseReqNo(model.getCount());
		loi.setPrDate(model.getCreationDate());
		loi.setRefOrderNO(model.getRefOrderNO());
		loi.setProject(model.getProject());
		loi.setDeliveryDate(model.getExpectedDeliveryDate());
		loi.setBranch(model.getBranch());
		loi.setEmployee(model.getEmployee());
		loi.setApproverName(model.getApproverName());
		form.ibLetterOfIntentID.setValue(null);
		form.ibreqforQuotation.setValue(null);
		form.ibPOId.setValue(null);
		form.getDbrefDate().setValue(form.getDbrefDate().getValue());

//		ArrayList<ProductDetails> prProductList = model.getPrproduct();
		
		ArrayList<ProductDetails> prProductList = getUniqueProductDetailsList(model.getPrproduct());

		List<ProductDetailsPO> poProductList = new ArrayList<ProductDetailsPO>();

		/******** convert one list to another ******/
		for (ProductDetails temp : prProductList) {
			ProductDetailsPO purch = new ProductDetailsPO();
			purch.setProdDate(temp.getProdDate());
			purch.setProductID(temp.getProductID());
			purch.setProductName(temp.getProductName());
			purch.setProductCode(temp.getProductCode());
			purch.setProductCategory(temp.getProductCategory());
			purch.setProdPrice(temp.getProdPrice());
			purch.setUnitOfmeasurement(temp.getUnitOfmeasurement());
			purch.setProductQuantity(temp.getProductQuantity());
			purch.setTax(temp.getServiceTax());
			purch.setVat(temp.getVat());
			
			purch.setPrduct(temp.getPrduct());
			
			//////
			purch.setWarehouseName(temp.getWarehouseName());
			purch.setAvailableStock(temp.getAvailableStock());			
			purch.setWarehouseAddress(temp.getWarehouseAddress());

			poProductList.add(purch);
		}

		loi.setProductDetails(poProductList);
		
//		ArrayList<VendorDetails> vendorList = setCommonVendors(model.getVendlist());
//		loi.setVendorDetails(vendorList);
		
		
		/**
		 * Copying some more info of PR to LOI
		 * Date : 09-09-2016 By Anil
		 * Release : 31 Aug 2016
		 * Project : PURCHASE ERROR SOLVING (NBHC)
		 */
		loi.setPODate(new Date());
		loi.setPOName(model.getPrTitle());
		loi.setResponseDate(model.getResponseDate());
//		loi.setExpectedClosureDate(model.getExpectedClosureDate());
		loi.setHeader(model.getHeader());
		loi.setFooter(model.getFooter());
		
		/**
		 * END
		 */
		
		
		/**
		 * If the process configuration is true the PR's assign to 1 is the purchase engg of PO
		 * in case assign to 1 is not selected and assign to 2 is selected then he will be purchase engg
		 * for po.
		 * Date: 14-10-2016 by Anil
		 * Release : 30 Sept 2016
		 * Project : PURCHASE MODIFICATION(NBHC)
		 */
		
		if(AppUtility.isPrAssignToPO("PurchaseRequisition")){
			if(!model.getAssignTo1().equals("")){
				loi.setEmployee(model.getAssignTo1());
			}else if(!model.getAssignTo2().equals("")){
				loi.setEmployee(model.getAssignTo2());
			}
		}
		/**
		 * End
		 */
		
		/////
		loi.setAssignTo1(model.getAssignTo1());
		loi.setAssignTo2(model.getAssignTo2());

		Timer t = new Timer() {
			@Override
			public void run() {
				form.setToNewState();
				form.updateView(loi);
				Double total = form.productTablePO.calculateTotal();
				form.getCbadds().setValue(false);
				form.dbTotal.setValue(total);
				form.setAllTable();
				form.getPurchaseRequisiteNo().setEnabled(false);
				form.getIbreqforQuotation().setEnabled(false);
				form.getIbLetterOfIntentID().setEnabled(false);
				form.getBranch().setEnabled(true);
				form.getOblEmployee().setEnabled(true);
				form.getTbApporverName().setEnabled(true);
				
				
				
				///////////
				
//				form.getDbCreationDate().setEnabled(true);
				form.getDbExpdeliverydate().setEnabled(true);
				
				
				
				form.getTbpoName().setEnabled(true);
				form.getDbExpectedResposeDate().setEnabled(true);
//				form.getDbExpectedClosureDate().setEnabled(true);
				form.getHeader().setEnabled(true);
				form.getFooter().setEnabled(true);
				
				if(form.isWarehouseSelected()){
					form.getDeliveryadd().clear();
					form.getDeliveryadd().setEnable(false);
					form.getCbadds().setEnabled(false);
				}
				
				//////////
						
				//////
				/**
				 * @author Vijay Date :- 21-07-2023
				 * Des :- commented below to allow to add products in PO
				 */
//				form.prodInfoComposite.setEnable(false);
//				form.addproducts.setEnabled(false);	
				/**
				 * ends here
				 */
				for(ProductDetailsPO prod: loi.getProductDetails()){
					form.checkAndAddVendorToVendorTable(prod,true);//Ashwini Patil Date:28-06-2023 Vendor was not getting added to vendor tab when po getting created from purchase requisition. 
				}
				
				
			}
		};
		t.schedule(5000);

	}
	
	/**
	 * This method sum up the quantity of  same product entered in PR
	 * Date : 17-10-2016 By Anil
	 * Release : 30 Sept 2016
	 * Project: PURCHASE MODIFICATION (NBHC)
	 */

	public static ArrayList<ProductDetails> getUniqueProductDetailsList(ArrayList<ProductDetails> prproduct) {
		ArrayList<ProductDetails> prproductList=new ArrayList<ProductDetails>();
		
		for(ProductDetails obj:prproduct){
			if(prproductList.size()==0){
				prproductList.add(obj);
			}else{
				boolean addProdFlag=false;
				for(ProductDetails obj1:prproductList){
					if(obj.getProductID()==obj1.getProductID()
							&&obj.getWarehouseName().equals(obj1.getWarehouseName())){
						
						double prodQty=obj1.getProductQuantity();
						prodQty=prodQty+obj.getProductQuantity();
						obj1.setProductQuantity(prodQty);
						addProdFlag=true;
						break;
					}
				}
				if(!addProdFlag){
					prproductList.add(obj);
				}
			}
		}
		
		return prproductList;
	}
	
	/**
	 * End
	 */

	/************************************************ change status ***********************************************/

	public void ChangeStatus(final String status) {
		model.setStatus(status);
		async.save(model, new AsyncCallback<ReturnFromServer>() {
			@Override
			public void onFailure(Throwable caught) {
				form.showDialogMessage("An Unexpected Error occured !");
			}
			@Override
			public void onSuccess(ReturnFromServer result) {
				form.setToViewState();
				form.tbStatus.setText(status);
			}
		});
	}

	
	/*************************************************On Click Event*******************************************/
	
	@Override
	public void onClick(ClickEvent event) {
			super.onClick(event);
			
			if(event.getSource()==cancelPopup.getBtnOk()){
					cancelpanel.hide();
					changeStatusOnCancelPR(PurchaseRequisition.CANCELLED);
				}
			
			if(event.getSource()==cancelPopup.getBtnCancel()){
					cancelpanel.hide();
					cancelPopup.remark.setValue("");
			}
			if(event.getSource().equals(assignPopup.getBtnOk())){
				if(assignPopup.validate()){
					if(assignPopup.getOlbAssignedTo1().getValue()!=null){
						model.setAssignTo1(assignPopup.getOlbAssignedTo1().getValue());
					}
					if(assignPopup.getOlbAssignedTo2().getValue()!=null){
						model.setAssignTo2(assignPopup.getOlbAssignedTo2().getValue());
					}
					form.showWaitSymbol();
					service.save(model, new AsyncCallback<ReturnFromServer>() {
						@Override
						public void onSuccess(ReturnFromServer result) {
							cancelpanel.hide();
							
							form.olbAssignedTo1.setSelectedIndex(0);
							form.olbAssignedTo2.setSelectedIndex(0);
							
							if(model.getAssignTo1()!=null&&!model.getAssignTo1().equals("")){
								form.olbAssignedTo1.setValue(model.getAssignTo1());
							}
							if(model.getAssignTo2()!=null&&!model.getAssignTo2().equals("")){
								form.olbAssignedTo2.setValue(model.getAssignTo2());
							}
							form.hideWaitSymbol();
							form.showDialogMessage("Assigned Successfully!");
							
						}
						@Override
						public void onFailure(Throwable caught) {
							cancelpanel.hide();
							form.hideWaitSymbol();
						}
					});
				}
			}
			if(event.getSource().equals(assignPopup.getBtnCancel())){
				cancelpanel.hide();
			}
		}

	
	/*********************************************** set common vendors Method*****************************************************/

	
	public void changeStatusOnCancelPR(final String status) {
		model.setStatus(status);
		model.setDescription("PR Id = "+model.getCount()+" "+"PR status ="+form.getstatustextbox().getValue().trim()+"\n"
		+"has been cancelled by "+LoginPresenter.loggedInUser+" with remark "+"\n"+"Remark ="+cancelPopup.getRemark().getValue().trim()
		+" "+"Cancellation Date ="+new Date());
		async.save(model, new AsyncCallback<ReturnFromServer>() {
			@Override
			public void onFailure(Throwable caught) {
				form.showDialogMessage("An Unexpected Error occured !");
			}
			@Override
			public void onSuccess(ReturnFromServer result) {
				form.setToViewState();
				form.tbStatus.setText(status);
				form.taDescription.setValue(model.getDescription());
			}
		});
	}

	
	public ArrayList<VendorDetails> setCommonVendors(ArrayList<VendorDetails> vend) {
		HashSet<VendorDetails> hset = new HashSet<VendorDetails>(vend);
		
		ArrayList<VendorDetails> newvend = new ArrayList<VendorDetails>();
		newvend.addAll(hset);
		return newvend;
	}

	@Override
	public void onRowCountChange(RowCountChangeEvent event) {
		
		if(event.getSource().equals(form.productTablePR.getTable())){
			Double amount=0d;
			
			for(ProductDetails prod:form.productTablePR.getDataprovider().getList()){
				amount=amount+prod.getProdPrice();
			}
			
			
			String approverName="";
			if(form.tbApporverName.getValue()!=null){
				approverName=form.tbApporverName.getValue();
			}
			AppUtility.getApproverListAsPerTotalAmount(form.tbApporverName, "Purchase Requisition", approverName, amount);
			
		}
	}
	
	
	/**
	 * This method update vendor table when we delete product 
	 * Date : 10-09-2016 By Anil
	 * Release : 31 Aug 2016
	 * Project : PURCHASE ERROR SOLVING (NBHC)
	 */
	
	public static void updateVendorTable(){
		
		System.out.println("PRODUCT TBL SIZE : "+form.productTablePR.getDataprovider().getList().size());
		if(form.productTablePR.getDataprovider().getList().size()==0){
			form.vendorDetailList.clear();
			form.vendorRateList.clear();
		}else{
			List<ProductDetails> prodList=form.productTablePR.getValue();
			System.out.println("GLOBAL VENDOR PRICE LIST SIZE BF : "+form.vendorRateList.size());
			List<PriceListDetails> remProdComVenLis=getRemainingProductVendorPriceList(prodList,form.vendorRateList);
			System.out.println("GLOBAL VENDOR PRICE LIST SIZE AF : "+form.vendorRateList.size());
			System.out.println("COMMON VENDOR PRICEL LIST SIZE : "+remProdComVenLis.size());
			System.out.println("EARLIER VEN TBL LIST SIZE :"+form.vendorDetailList.size());
			ArrayList<VendorDetails>vendorList=getVendorListOfRemainingProduct(form.vendorDetailList,remProdComVenLis);
			System.out.println("NOW VEN TBL LIST SIZE : "+vendorList.size());
//			form.vendorDetailList.clear();
//			form.vendorDetailList.addAll(vendorList);
			System.out.println("AFTER REMOVING : "+form.vendorDetailList.size());
		}
	}
	
	
	/**
	 * This method returns the list of common vendor from global vendor list which is maintained at the time of product add.
	 * 
	 * Date : 10-09-2016 By Anil
	 * Release : 31 Aug 2016
	 * Project : PURCHASE ERROR SOLVING (NBHC)
	 * I/P:existing vendor table list and global vendor list
	 * O/P:vendor list
	 */
	private static List<PriceListDetails> getRemainingProductVendorPriceList(List<ProductDetails> prodList,
			ArrayList<PriceListDetails> vendorRateList) {
		
		ArrayList<PriceListDetails> vendorPriceList=new ArrayList<PriceListDetails>();
		List<PriceListDetails> list=new ArrayList<PriceListDetails>();
		
		for(ProductDetails prodDet:prodList){
			for(PriceListDetails priceLis:vendorRateList){
				if(prodDet.getProductID()==priceLis.getProdID()){
					vendorPriceList.add(priceLis);
				}
			}
		}
		
		form.vendorRateList.clear();
		form.vendorRateList.addAll(vendorPriceList);
		
		for(PriceListDetails pricelis:vendorPriceList){
			if(list.size()==0){
				list.add(pricelis);
			}else{
				boolean isAlreadyAddedFlag=checkPriceListAlreadyAdded(list,pricelis);
				if(isAlreadyAddedFlag==false){
					list.add(pricelis);
				}
			}
		}
		return list;
	}

	
	/**
	 * This method compare whether passed parameter 2nd is present in parameter 1st i.e. vendor price list
	 * Date 10-09-2016 By Anil
	 * Release : 31 Aug 2016
	 * Project: PURCHASE ERROR SOLVING(NBHC)
	 * I/P:vendor price list and vendor price
	 * O/P:True/False
	 */
	private static boolean checkPriceListAlreadyAdded(List<PriceListDetails> list, PriceListDetails pricelis) {
		for(PriceListDetails pricelist:list){
			if(pricelis.getVendorID()==pricelist.getVendorID()){
				return true;
			}
		}
		return false;
	}

	/**
	 * This method returns common vendors of remaining products.
	 * Date 10-09-2016 By Anil
	 * Release : 31 Aug 2016
	 * Project: PURCHASE ERROR SOLVING(NBHC)
	 * I/P:existing vendor list and remaining products vendor list
	 * O/P:vendor list
	 */
	
	private static ArrayList<VendorDetails> getVendorListOfRemainingProduct(ArrayList<VendorDetails> vendorList,List<PriceListDetails> venRateLis) {
		System.out.println("REMAINING PRODUCTS VENDOR LIST");
		
		System.out.println("VEN LIS SIZE BF "+vendorList.size());
		for(PriceListDetails venRLis:venRateLis){
			boolean vendorFlag= isVendorPresent(venRLis.getVendorID(),vendorList);
			if(vendorFlag==false){
				List<PriceListDetails> vLis=new ArrayList<PriceListDetails>();
				vLis.add(venRLis);
				ArrayList<VendorDetails>vDeLis=form.getVendorList(vLis);
				vendorList.addAll(vDeLis);
			}
		}
		
		System.out.println("VEN LIS SIZE AF "+vendorList.size());
		return vendorList;
	}

	/**
	 * This method returns true if passed vendor id is present in given vendor list.
	 * Date : 10-09-2016 By Anil
	 * Release : 31 Aug 2016
	 * Project: PURCHASE ERROR SOLVING (NBHC)
	 * I/P:Vendor id and Vendor List
	 * O/P:True/False
	 */
	private static boolean isVendorPresent(int vendorID,ArrayList<VendorDetails> vendorList) {
		for(VendorDetails venDet:vendorList){
			if(vendorID==venDet.getVendorId()){
				return true;
			}
		}
		return false;
	}
	
	private void reactToServicePO() {
		
	}
/** date 02/02/2018 added by komal to get approval status in PR download **/
	public void getApprovalStatus(String processType , ArrayList<PurchaseRequisition> list){
		GeneralServiceAsync service = GWT.create(GeneralService.class);
		service.getPurchaseRequisitionDetails(UserConfiguration.getCompanyId(), processType, list, new AsyncCallback<ArrayList<PurchaseRequisition>>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				Console.log("erroe :" + caught);
			}

			@Override
			public void onSuccess(ArrayList<PurchaseRequisition> result) {
				// TODO Auto-generated method stub
				CsvServiceAsync async = GWT.create(CsvService.class);
				async.setPR(result, new AsyncCallback<Void>() {
					@Override
					public void onFailure(Throwable caught) {
						System.out.println("RPC call Failed" + caught);
					}

					@Override
					public void onSuccess(Void result) {
						
						String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
						final String url = gwt + "csvservlet" + "?type=" + 47;
						Window.open(url, "test", "enabled");
					}
				});
			}
		});
	}
	
	private void setEmailPopUpData() {
		form.showWaitSymbol();
		String branchName = form.branch.getValue();
		String fromEmailId = AppUtility.getFromEmailAddress(branchName,model.getEmployee());
		String purchaseEngineerEmailid="";
		if(form.oblEmployee.getSelectedIndex()!=0){
			Employee emp = form.oblEmployee.getSelectedItem();
			if(emp!=null && emp.getEmail()!=null && !emp.getEmail().equals("")){
				purchaseEngineerEmailid = emp.getEmail();
			}
		}
		if(!purchaseEngineerEmailid.equals("")){
			String emailId = AppUtility.getPurchaseEngineerEmailid(form.oblEmployee.getValue());
			if(emailId!=null){
				purchaseEngineerEmailid = emailId;
			}
		}
		
		String purcaseEngineerName = "";
		if(form.oblEmployee.getValue()!=null){
			purcaseEngineerName = form.oblEmployee.getValue();
		}
		if(purcaseEngineerName!=null && !purcaseEngineerName.equals("")){
			label : AppUtility.loadContactPersonlist(emailpopup.olbContactlistEmailId,"Vendor",-1,purcaseEngineerName,purchaseEngineerEmailid,null);
		}
		Screen screenName=(Screen) AppMemory.getAppMemory().currentScreen;
		Console.log("screenName "+screenName);
		AppUtility.makeLiveSmsTemplateConfig(emailpopup.oblTemplateName,AppConstants.PURCHASEMODULE.trim(),screenName);
		emailpopup.taFromEmailId.setValue(fromEmailId);
		emailpopup.smodel = model;
		form.hideWaitSymbol();
		
	}
	

}
