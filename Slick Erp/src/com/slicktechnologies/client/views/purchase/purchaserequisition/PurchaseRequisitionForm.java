package com.slicktechnologies.client.views.purchase.purchaserequisition;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.UploadComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.approvalutility.ApprovableFormScreen;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.config.CategoryTypeFactory;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.shared.common.businessprocesslayer.PriceList;
import com.slicktechnologies.shared.common.businessprocesslayer.PriceListDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetailsPO;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseRequisition;
import com.slicktechnologies.shared.common.businessprocesslayer.VendorDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.helperlayer.Type;

public class PurchaseRequisitionForm extends ApprovableFormScreen<PurchaseRequisition> implements ClickHandler, ChangeHandler {
	
	final GenricServiceAsync async = GWT.create(GenricService.class);
	public TextBox ibPRId;
	public TextBox tbprName;
	public DateBox  dbCreationDate, dbExpectedResposeDate,dbExpectedDeliveryDate, dbRefOrderDate;
	TextBox ibRefOrderNo;
	ObjectListBox<Config> oblPRGroup;
	ObjectListBox<ConfigCategory> oblPRCategory;
	ObjectListBox<Type> oblPRType;
	
	ObjectListBox<HrProject> project;
	ObjectListBox<Branch> branch;
	ObjectListBox<Employee> oblEmployee;
	ObjectListBox<Employee> tbApporverName;
	TextArea taDescription;
	UploadComposite upload;
	public TextBox tbStatus;
	TextArea header, footer;
	TextBox tbcommentForStatusChange;
	FormField fgroupingLetterInformation;
	public ProductInfoComposite productInfoComposite;
	public Button bAddproducts;
	
	public ProductTablePR productTablePR;
	
	public ArrayList<VendorDetails> vendorDetailList = new ArrayList<VendorDetails>();
	
	/**
	 * This list provides the rate list of all vendor supplying the product .
	 * Date : 10-09-2016 By Anil
	 * Release 31 Aug 2016
	 * Project:PURCHASE ERROR SOLVING (NBHC)
	 */
	public ArrayList<PriceListDetails> vendorRateList=new ArrayList<PriceListDetails>();
	
	/**
	 * END
	 */
	
	
	/**
	 * Purchase Engg. assigned PR to its team;
	 * Date : 26-09-2016 By Anil
	 * Release :30 Sept 2016
	 * Project : PURCHASE MODIFICATION (NBHC)
	 */
	ObjectListBox<Employee> olbAssignedTo1;
	ObjectListBox<Employee> olbAssignedTo2;
	
	/**
	 * End
	 */
	
	
	/**
	 * Date : 08-02-2017 By ANIL
	 * Added description 2
	 */
	TextArea taDescription2;
	/**
	 * End
	 */
	
	PurchaseRequisition purchaseRequisitionobj;
	
	
	
	
	public PurchaseRequisitionForm() {
		super();
		createGui();
		productTablePR.connectToLocal();
		tbStatus.setValue(PurchaseRequisition.CREATED);
		tbcommentForStatusChange.setEnabled(false);
		dbCreationDate.setEnabled(false);
		
		if(AppUtility.isPrHeadAsDefaultPurEngg("PurchaseRequisition")){
			setPrHeadAsDefualtPurchaseEngineer();
		}
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("PurchaseRequisition", "PurchaseEngineerReadOnly")){
			oblEmployee.setEnabled(false);
		}
		
//		if(AppUtility.isPoHeadAsDefaultPurEngg("PurchaseRequisition")){
//			setPoHeadAsDefualtPurchaseEngineer();
//		}
		
	}
	
	/**
	 * This method sets PR HEAD(Role of employee) as purchase engineer.
	 * Date : 23-11-2016 By Anil
	 * Project : DEADSTOCK (NBHC)
	 */
	public void setPrHeadAsDefualtPurchaseEngineer(){
		Timer timer=new Timer() {
			@Override
			public void run() {
				for(Employee object:LoginPresenter.globalEmployee){
					if(object.getDesignation()!=null&&object.getDesignation().equals("PR HEAD")){
						oblEmployee.setValue(object.getFullName());
						break;
					}
				}
			}
		};
		timer.schedule(4000);
	}
	
	/**
	 * This method sets PO HEAD(Role of employee) as purchase engineer of PO in assign 1 listbox.
	 * Date : 31-01-2017 By Anil
	 * Project : DEADSTOCK (NBHC)
	 */
	public void setPoHeadAsDefualtPurchaseEngineer(){
		Timer timer=new Timer() {
			@Override
			public void run() {
				for(Employee object:LoginPresenter.globalEmployee){
					if(object.getRoleName()!=null&&object.getRoleName().equals("PO HEAD")){
						olbAssignedTo1.setValue(object.getFullName());
						break;
					}
				}
			}
		};
		timer.schedule(4000);
	}
	

	public PurchaseRequisitionForm(String[] processlevel, FormField[][] fields,FormStyle formstyle) {
		super(processlevel, fields, formstyle);
		createGui();
		tbStatus.setValue(PurchaseRequisition.CREATED);
		tbcommentForStatusChange.setEnabled(false);
		dbCreationDate.setEnabled(false);
	}

	private void initalizeWidget() {
		ibPRId = new TextBox();
		ibPRId.setEnabled(false);
		tbprName = new TextBox();
		
		dbCreationDate = new DateBoxWithYearSelector();
		Date date = new Date();
		dbCreationDate.setValue(date);
		dbCreationDate.setEnabled(false);
		
		ibRefOrderNo = new TextBox();
		
		dbExpectedResposeDate = new DateBoxWithYearSelector();
		dbExpectedDeliveryDate = new DateBoxWithYearSelector();
		dbRefOrderDate = new DateBoxWithYearSelector();
		
		oblPRGroup = new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(oblPRGroup, Screen.PRGROUP);
		oblPRCategory = new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(oblPRCategory, Screen.PRCATEGORY);
		oblPRCategory.addChangeHandler(this);
		oblPRType = new ObjectListBox<Type>();
		AppUtility.makeTypeListBoxLive(oblPRType, Screen.PRTYPE);
		branch = new ObjectListBox<Branch>();
		initalizEmployeeBranchListBox();
		
		oblEmployee = new ObjectListBox<Employee>();
//		initalizEmployeeNameListBox();
		oblEmployee.makeEmployeeLive(AppConstants.PURCHASEMODULE, AppConstants.PURCHASEREQUISION, "Purchase Engineer");
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("PurchaseRequisition", "PurchaseEngineerReadOnly")){
			oblEmployee.setEnabled(false);
		}
		
		tbApporverName = new ObjectListBox<Employee>();
		AppUtility.makeApproverListBoxLive(tbApporverName, "Purchase Requisition");
		
		project = new ObjectListBox<HrProject>();
		HrProject.MakeProjectListBoxLive(project);
		
		productInfoComposite=AppUtility.initiatePurchaseProductComposite(new ItemProduct());
		bAddproducts = new Button("Add product");
		bAddproducts.addClickHandler(this);
		productTablePR = new ProductTablePR();
		

		taDescription = new TextArea();
		upload = new UploadComposite();
		tbStatus = new TextBox();
		tbStatus.setEnabled(false);
		tbcommentForStatusChange=new TextBox();
		tbcommentForStatusChange.setEnabled(false);
		
		header = new TextArea();
		footer = new TextArea();
		
		olbAssignedTo1=new ObjectListBox<Employee>();
		olbAssignedTo1.makeEmployeeLive(AppConstants.PURCHASEMODULE, AppConstants.PURCHASEREQUISION, "Assign To 1");
//		AppUtility.makeSalesPersonListBoxLive(olbAssignedTo1);
		
//		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Employee", "LoadEmployeeRoleWise")){
//			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Employee", "PurchaseEngineerPRPO")){
//				olbAssignedTo1.setEmployeeToDropDown("PO Purchase Engineer");
//			}else{
//				olbAssignedTo1.setEmployeeToDropDown("Purchase Engineer");
//			}
//		}else{
//			AppUtility.makeSalesPersonListBoxLive(olbAssignedTo1);
//		}
		
		
		olbAssignedTo2=new ObjectListBox<Employee>();
		olbAssignedTo2.makeEmployeeLive(AppConstants.PURCHASEMODULE, AppConstants.PURCHASEREQUISION, "Assign To 2");
//		AppUtility.makeSalesPersonListBoxLive(olbAssignedTo2);
		
//		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Employee", "LoadEmployeeRoleWise")){
//			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Employee", "PurchaseEngineerPRPO")){
//				olbAssignedTo2.setEmployeeToDropDown("PO Purchase Engineer");
//			}else{
//				olbAssignedTo2.setEmployeeToDropDown("Purchase Engineer");
//			}
//		}else{
//			AppUtility.makeSalesPersonListBoxLive(olbAssignedTo2);
//		}
		
		taDescription2=new TextArea();
	}

	@Override
	public void createScreen() {
		initalizeWidget();

		this.processlevelBarNames = new String[] {ManageApprovals.SUBMIT,
				ManageApprovals.APPROVALREQUEST,
				ManageApprovals.CANCELAPPROVALREQUEST, "Create RFQ",
				"Create LOI", "Create PO", "Mark Close",  AppConstants.CANCELPR, "New",AppConstants.ASSIGNPRTOPOPURCHASEENGG,ManageApprovals.APPROVALSTATUS };


		boolean catvalidFlag = false,groupvalidFlag = false , typevalidFlag = false;
		
		/**
		 *  nidhi
		 *  14-05-2018
		 *  for make field mandetory
		 */
		catvalidFlag = AppUtility.checkForProcessConfigurartionIsActiveOrNot("PurchaseRequisition", "makeCategoryMandatory");
		groupvalidFlag = AppUtility.checkForProcessConfigurartionIsActiveOrNot("PurchaseRequisition", "makeCategoryMandatory");
		typevalidFlag = AppUtility.checkForProcessConfigurartionIsActiveOrNot("PurchaseRequisition", "makeTypeMandatory");
		/**
		 * end
		 */
		String mainScreenLabel="Purchase Requisition";
		if(purchaseRequisitionobj!=null&&purchaseRequisitionobj.getCount()!=0){
			mainScreenLabel=purchaseRequisitionobj.getCount()+" "+"/"+" "+purchaseRequisitionobj.getStatus()+" "+"/"+" "+AppUtility.parseDate(purchaseRequisitionobj.getCreationDate());
		}
			//fgroupingLetterInformation.getHeaderLabel().setText(mainScreenLabel);
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		fgroupingLetterInformation=fbuilder.setlabel(mainScreenLabel).widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).setKeyField(true).build();
		
		
//		FormFieldBuilder fbuilder;
//		fbuilder = new FormFieldBuilder();
//		FormFieldfgroupingLetterInformation  = fbuilder.setlabel("Purchase Requisition Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();

		fbuilder = new FormFieldBuilder("PR ID", ibPRId);
		FormField fibLetterOfIntentID = fbuilder.setMandatory(false)
				.setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("PR Title", tbprName);
		FormField ftbpoName = fbuilder.setMandatory(false)
				.setMandatoryMsg("Title is mandatory").setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("* PR Date", dbCreationDate);
		FormField fdate = fbuilder.setMandatory(true)
				.setMandatoryMsg("Date is mandatory").setRowSpan(0)
				.setColSpan(0).build();

//		fbuilder = new FormFieldBuilder("* Expected Closure Date",dbExpectedClosureDate);
//		FormField fclosuredate = fbuilder.setMandatory(true)
//				.setMandatoryMsg("Expected Closure Date is mandatory")
//				.setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Expected Response Date",
				dbExpectedResposeDate);
		FormField fresponsedate = fbuilder.setMandatory(false)
				.setMandatoryMsg("Expected Response Date is mandatory")
				.setRowSpan(0).setColSpan(0).build();
		
		/**
		 * Date 28-01-2019 by Vijay
		 * Des :- Expected Delivery note non mandatory with process config
		 * for NBHC Inventory Management
		 */
		FormField fdeliverydate;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("PurchaseRequisition", "MakeExpectedDeliveryDateNonMandatory")){
			fbuilder = new FormFieldBuilder("Expected Delivery Date",dbExpectedDeliveryDate);
			fdeliverydate = fbuilder.setMandatory(false)
					.setMandatoryMsg("Expected Delivery Date is mandatory")
					.setRowSpan(0).setColSpan(0).build();
		}
		else{
			fbuilder = new FormFieldBuilder("* Expected Delivery Date",dbExpectedDeliveryDate);
			fdeliverydate = fbuilder.setMandatory(true)
					.setMandatoryMsg("Expected Delivery Date is mandatory")
					.setRowSpan(0).setColSpan(0).build();
		}
		/**
		 * ends here
		 */
		

		fbuilder = new FormFieldBuilder("Reference Order No.", ibRefOrderNo);
		FormField freferenceorderno = fbuilder.setMandatory(false)
				.setMandatoryMsg("").setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Purchase Engineer.", oblEmployee);
		FormField femployee = fbuilder.setMandatory(false)
				.setMandatoryMsg("Purchase Engineer is mandatory")
				.setRowSpan(0).setColSpan(0).build();

/*		fbuilder = new FormFieldBuilder("PR Type.", oblPRType);
		FormField floitype = fbuilder.setMandatory(false)
				.setMandatoryMsg("PR type is mandatory").setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("PR Group.", oblPRGroup);
		FormField floigroup = fbuilder.setMandatory(false)
				.setMandatoryMsg("PR Group is mandatory").setRowSpan(0)
				.setColSpan(0).build();

*/
		
		/**
		 *  nidhi
		 *  14-05-2018
		 *  for make field mandetory
		 */
		fbuilder = new FormFieldBuilder((typevalidFlag) ? "* PR Type" : "PR Type", oblPRType);
		FormField floitype = fbuilder.setMandatory(typevalidFlag)
				.setMandatoryMsg("PR type is mandatory").setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder((groupvalidFlag) ? "* PR Group" : "PR Group" , oblPRGroup);
		FormField floigroup = fbuilder.setMandatory(groupvalidFlag)
				.setMandatoryMsg("PR Group is mandatory").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder((catvalidFlag) ? "* PR Category" : "PR Category", oblPRCategory);
		FormField foblPRCategory = fbuilder.setMandatory(catvalidFlag)
				.setMandatoryMsg("PR Category is mandatory").setColSpan(0)
				.build();
		/**
		 * end
		 */

		fbuilder = new FormFieldBuilder("Status.", tbStatus);
		FormField ftbStatus = fbuilder.setMandatory(false)
				.setMandatoryMsg("LOI Group is mandatory").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Remark", tbcommentForStatusChange);
		FormField ftbcommentForStatusChange = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
//		fbuilder = new FormFieldBuilder("* Branch", branch);
//		FormField fbranch = fbuilder.setMandatory(true)
//				.setMandatoryMsg("Branch is mandatory").setRowSpan(0)
//				.setColSpan(0).build();
		
		
		FormField fbranch=null;
		if(AppUtility.isBranchNonMandatory("PurchaseRequisition")){
			fbuilder = new FormFieldBuilder("Branch", branch);
			fbranch = fbuilder.setMandatory(false).setMandatoryMsg("Branch is Mandatory!").setRowSpan(0)
					.setColSpan(0).build();
		}else{
			fbuilder = new FormFieldBuilder("* Branch", branch);
			fbranch = fbuilder.setMandatory(true).setMandatoryMsg("Branch is Mandatory!").setRowSpan(0)
					.setColSpan(0).build();
		}
		
		

		fbuilder = new FormFieldBuilder("* Approver Name", tbApporverName);
		FormField ftbApporverName = fbuilder.setMandatory(true)
				.setMandatoryMsg("Approver Name is mandatory").setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Project", project);
		FormField fproject = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("", productInfoComposite);
		FormField foblProductList = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(3).build();

		fbuilder = new FormFieldBuilder("", bAddproducts);
		FormField fbAddproducts = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		FormField fproductInformation = fbuilder
				.setlabel("Product").widgetType(FieldType.Grouping)
				.setMandatory(false).setColSpan(5).build();
		
		FormField fClassificationInformation = fbuilder
				.setlabel("Classification").widgetType(FieldType.Grouping)
				.setMandatory(false).setColSpan(4).build();
		
		FormField fReferenceInformation = fbuilder
				.setlabel("Reference/General").widgetType(FieldType.Grouping)
				.setMandatory(false).setColSpan(4).build();
		
		FormField fTimestampInformation = fbuilder                                     
				.setlabel("Timestamp").widgetType(FieldType.Grouping)
				.setMandatory(false).setColSpan(4).build();
		
		FormField fAttachInformation = fbuilder                                     
				.setlabel("Attachment").widgetType(FieldType.Grouping)
				.setMandatory(false).setColSpan(4).build();


		fbuilder = new FormFieldBuilder("", productTablePR.getTable());
		FormField fproductTablePR = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(5).build();

		fbuilder = new FormFieldBuilder("Description 1", taDescription);
		FormField fdescription = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(5).build();

/*		fbuilder = new FormFieldBuilder("PR Category", oblPRCategory);
		FormField foblPRCategory = fbuilder.setMandatory(false)
				.setMandatoryMsg("PR Category is mandatory").setColSpan(0)
				.build();
*/
		fbuilder = new FormFieldBuilder("Reference Order Date", dbRefOrderDate);
		FormField freforederDate = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Upload Document", upload);
		FormField fdoc = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(2).build();
		fbuilder = new FormFieldBuilder("E-mail Header", header);
		FormField fheader = fbuilder.setMandatory(false)
				.setMandatoryMsg("Header is mandatory").setRowSpan(0)
				.setColSpan(5).build();

		fbuilder = new FormFieldBuilder("E-mail Footer ", footer);
		FormField ffooter = fbuilder.setMandatory(false)
				.setMandatoryMsg("Footer is mandatory").setRowSpan(0)
				.setColSpan(5).build();
		
		fbuilder = new FormFieldBuilder("Assign To 1", olbAssignedTo1);
		FormField folbAssignedTo1 = fbuilder.setMandatory(false).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Assign To 2", olbAssignedTo2);
		FormField folbAssignedTo2 = fbuilder.setMandatory(false).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Description 2", taDescription2);
		FormField ftaDescription2 = fbuilder.setMandatory(false).setColSpan(5).build();
		
		
//		FormField[][] formfield = {
//				{ fgroupingLetterInformation },
//				{ fibLetterOfIntentID, ftbpoName, fdate, fresponsedate },
//				{ fdeliverydate,freferenceorderno,freforederDate,fproject },
//				{ floigroup, foblPRCategory, floitype,fbranch  },
//				{ femployee, ftbApporverName ,ftbStatus,ftbcommentForStatusChange},
//				{ folbAssignedTo1,folbAssignedTo2,fdoc },
//				{ fdescription }, 
//				{ ftaDescription2 }, 
//				{ fproductInformation },
//				{ foblProductList, fbAddproducts }, 
//				{ fproductTablePR },{fheader},{ffooter} };
		
		FormField[][] formfield = {
				{ fgroupingLetterInformation },
				{ ftbpoName, fdate, fdeliverydate,fbranch},
				{ femployee, ftbApporverName,ftbcommentForStatusChange},
				/**Product**/
				{ fproductInformation },
				{ foblProductList, fbAddproducts }, 
				{ fproductTablePR },
				/**Classification**/    
				{ fClassificationInformation },
				{ floigroup, foblPRCategory, floitype },
				/**Reference/Genreal**/
				{ fReferenceInformation},
				{ fresponsedate,freferenceorderno,freforederDate,fproject},
				{folbAssignedTo1,folbAssignedTo2 },
				{ fdescription }, 
				{ ftaDescription2 },
				/**Attachment**/
				{fAttachInformation},
				{fheader},
				{ffooter} ,
				{fdoc }
				
				
		};

		this.fields = formfield;
	}

	@Override
	public void updateModel(PurchaseRequisition model) {
		if (tbprName.getValue() != null)
			model.setPrTitle(tbprName.getValue());

		if (dbCreationDate.getValue() != null)
			model.setCreationDate(dbCreationDate.getValue());

//		if (dbExpectedClosureDate.getValue() != null)
//			model.setExpectedClosureDate(dbExpectedClosureDate.getValue());

		if (dbExpectedResposeDate.getValue() != null)
			model.setResponseDate(dbExpectedResposeDate.getValue());

		if (ibRefOrderNo.getValue() != null)
			model.setRefOrderNO(ibRefOrderNo.getValue());

		if (oblEmployee.getValue() != null)
			model.setEmployee(oblEmployee.getValue());

		if (tbStatus.getValue() != null)
			model.setStatus(tbStatus.getValue());
		
//		if (tbcommentForStatusChange.getValue() != null)
//			model.setCommentChangeStatus(tbcommentForStatusChange.getValue());

		if (branch.getValue() != null)
			model.setBranch(branch.getValue());

		if (tbApporverName.getValue() != null)
			model.setApproverName(tbApporverName.getValue());

		if (project.getValue() != null)
			model.setProject(project.getValue());

		if (productTablePR.getValue() != null)
			model.setPrproduct(productTablePR.getValue());

		if (taDescription.getValue() != null)
			model.setDescription(taDescription.getValue());

		if (oblPRCategory.getValue() != null)
			model.setPrCategory(oblPRCategory.getValue());

		if (oblPRGroup.getValue() != null)
			model.setPrGroup(oblPRGroup.getValue());

		if (oblPRType.getValue() != null)
			model.setPrType(oblPRType.getValue(oblPRType.getSelectedIndex()));
		
		if (header.getValue() != null)
			model.setHeader(header.getValue());
		if (footer.getValue() != null)
			model.setFooter(footer.getValue());
		
		if (dbExpectedDeliveryDate.getValue() != null)
			model.setExpectedDeliveryDate(dbExpectedDeliveryDate.getValue());

		if (dbRefOrderDate.getValue() != null)
			model.setRefOrderDate(dbRefOrderDate.getValue());

//		model.setVendlist(vendorDetailList);

		if (upload.getValue() != null)
			model.setUptestReport(upload.getValue());
		
		if(olbAssignedTo1.getValue()!=null){
			model.setAssignTo1(olbAssignedTo1.getValue());
		}
		
		if(olbAssignedTo2.getValue()!=null){
			model.setAssignTo2(olbAssignedTo2.getValue());
		}
		
		
		if(taDescription2.getValue()!=null){
			model.setDescription2(taDescription2.getValue());
		}
		
		purchaseRequisitionobj=model;
		
		presenter.setModel(model);
	}

	@Override
	public void updateView(PurchaseRequisition model) {
		Console.log("INSIDE PR UPDATE VIEW ");
		purchaseRequisitionobj=model;
		
		ibPRId.setValue(model.getCount()+"");

		if (model.getPrTitle() != null)
			tbprName.setValue(model.getPrTitle());

		if (model.getRemark()!= null)
			tbcommentForStatusChange.setValue(model.getRemark());
		
		if (model.getCreationDate() != null)
			dbCreationDate.setValue(model.getCreationDate());

//		if (model.getExpectedClosureDate() != null)
//			dbExpectedClosureDate.setValue(model.getExpectedClosureDate());
		if (model.getRefOrderNO() != null)
			ibRefOrderNo.setValue(model.getRefOrderNO());

		if (model.getEmployee() != null)
			oblEmployee.setValue(model.getEmployee());

		if (model.getStatus() != null)
			tbStatus.setValue(model.getStatus());
		
		
		
		if (model.getResponseDate() != null)
			dbExpectedResposeDate.setValue(model.getResponseDate());

		if (model.getBranch() != null)
			branch.setValue(model.getBranch());

		if (model.getApproverName() != null)
			tbApporverName.setValue(model.getApproverName());

		if (model.getProject() != null)
			project.setValue(model.getProject());

		if (model.getHeader() != null)
			header.setValue(model.getHeader());
		if (model.getFooter() != null)
			footer.setValue(model.getFooter());

		if (model.getPrproduct() != null){
			productTablePR.setValue(model.getPrproduct());
			System.out.println("PRODUCT LIST SIZE : "+model.getPrproduct().size());
			List<String> prodIdList=new ArrayList<String>();
			for(ProductDetails prodDet:model.getPrproduct()){
				System.out.println("PRODUCT ID : "+prodDet.getProductID());
				prodIdList.add(prodDet.getProductCode());	
			}
			System.out.println("PRODUCT INT LIST SIZE : "+prodIdList.size());
			
//			loadVendorProductPriceList(prodIdList);
		}
		
	
		
		

		if (model.getDescription() != null)
			taDescription.setValue(model.getDescription());
		if (model.getPrCategory() != null)
			oblPRCategory.setValue(model.getPrCategory());
		if (model.getPrGroup() != null)
			oblPRGroup.setValue(model.getPrGroup());
		if (model.getPrType() != null)
			oblPRType.setValue(model.getPrType());
		if (model.getExpectedDeliveryDate() != null)
			dbExpectedDeliveryDate.setValue(model.getExpectedDeliveryDate());
		if (model.getRefOrderDate() != null)
			dbRefOrderDate.setValue(model.getRefOrderDate());
		
//		vendorDetailList.addAll(model.getVendlist());
		
		if (model.getUptestReport() != null)
			upload.setValue(model.getUptestReport());
		
		if(model.getAssignTo1()!=null){
			olbAssignedTo1.setValue(model.getAssignTo1());
		}
		if(model.getAssignTo2()!=null){
			olbAssignedTo2.setValue(model.getAssignTo2());
		}
		
		if(model.getDescription2()!=null){
			taDescription2.setValue(model.getDescription2());
		}

		/* 
		 * for approval process
		 *  nidhi
		 *  5-07-2017
		 */
		if(presenter != null){
			presenter.setModel(model);
		}
		/*
		 *  end
		 */
	}



	@Override
	public void toggleAppHeaderBarMenu() {
		super.toggleAppHeaderBarMenu();
		if (AppMemory.getAppMemory().currentState == ScreeenState.NEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Save") || text.equals("Discard")|| text.equals("Search")||text.equals(AppConstants.NAVIGATION)) {
					menus[k].setVisible(true);
				} else
					menus[k].setVisible(false);
			}
		}
		else if (AppMemory.getAppMemory().currentState == ScreeenState.EDIT) {
			
			/**
			 * Date 01-02-2019 for NBHC Inventory mgmt if condtion added by vijay for GENERAL POPUP menus 
			 * and old code added to else block
			 */
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			}
			
//			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Save") || text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true);
				else
					menus[k].setVisible(false);
			}
		}
		else if (AppMemory.getAppMemory().currentState == ScreeenState.VIEW) {
			/**
			 * Date 01-02-2019 for NBHC Inventory mgmt if condtion added by vijay for GENERAL POPUP menus 
			 * and old code added to else block
			 */
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			}
//			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Discard") || text.equals("Edit")|| text.equals("Search")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true);
				else
					menus[k].setVisible(false);
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.PURCHASEREQUISITE,LoginPresenter.currentModule.trim());
		
	}

	public void toggleProcessLevelMenu() {
		PurchaseRequisition entity = (PurchaseRequisition) presenter.getModel();
		String status = entity.getStatus();

		for (int i = 0; i < getProcesslevelBarNames().length; i++) {
			InlineLabel label = getProcessLevelBar().btnLabels[i];
			String text = label.getText().trim();

			if (status.equals(PurchaseRequisition.APPROVED)) {
				if (text.equals("Create PO"))
					label.setVisible(true);
				if (text.equals("Create LOI"))
					label.setVisible(true);
				if (text.equals("Create RFQ"))
					label.setVisible(true);
				if (text.equals("Mark Close"))
					label.setVisible(true);
				if (text.equals(AppConstants.CANCELPR))
					label.setVisible(true);
				if (text.equals(AppConstants.ASSIGNPRTOPOPURCHASEENGG))
					label.setVisible(true);
				if (text.equals(ManageApprovals.APPROVALSTATUS))
					label.setVisible(true);
					// By Priyanka Req. raised by Vaishnavi mam - After approved PR submit status shown on menu bar.
				if(text.equals(ManageApprovals.SUBMIT)) 
					label.setVisible(false);

			} else if (status.equals(PurchaseRequisition.CLOSE)||status.equals(PurchaseRequisition.PROCESSED)) {
				if (text.equals("Create PO"))
					label.setVisible(false);
				if (text.equals("Create LOI"))
					label.setVisible(false);
				if (text.equals("Create RFQ"))
					label.setVisible(false);
				if (text.equals("Mark Close"))
					label.setVisible(false);
				if (text.equals(ManageApprovals.APPROVALREQUEST))
					label.setVisible(false);
				if (text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
					label.setVisible(false);
				if (text.equals(AppConstants.CANCELPR))
					label.setVisible(false);
				if (text.equals(AppConstants.ASSIGNPRTOPOPURCHASEENGG))
					label.setVisible(false);
				if (text.equals(ManageApprovals.APPROVALSTATUS))
					label.setVisible(true);
				if(text.equals(ManageApprovals.SUBMIT))
					label.setVisible(false);

			} else if (status.equals(PurchaseRequisition.CANCELLED)) {
				if (text.equals("Create PO"))
					label.setVisible(false);
				if (text.equals("Create LOI"))
					label.setVisible(false);
				if (text.equals("Create RFQ"))
					label.setVisible(false);
				if (text.equals("Mark Close"))
					label.setVisible(false);
				if (text.equals(ManageApprovals.APPROVALREQUEST))
					label.setVisible(false);
				if (text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
					label.setVisible(false);
				if (text.equals(AppConstants.CANCELPR))
					label.setVisible(false);
				if (text.equals(AppConstants.ASSIGNPRTOPOPURCHASEENGG))
					label.setVisible(false);
				if(text.equals(ManageApprovals.SUBMIT))
					label.setVisible(false);

			} else if (status.equals(PurchaseRequisition.REJECTED)) {
				if (text.equals("Create PO"))
					label.setVisible(false);
				if (text.equals("Create LOI"))
					label.setVisible(false);
				if (text.equals("Create RFQ"))
					label.setVisible(false);
				if (text.equals("Mark Close"))
					label.setVisible(false);
				if (text.equals(ManageApprovals.APPROVALREQUEST))
					label.setVisible(false);
				if (text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
					label.setVisible(false);
				if (text.equals(AppConstants.CANCELPR))
					label.setVisible(false);
				if (text.equals(AppConstants.ASSIGNPRTOPOPURCHASEENGG))
					label.setVisible(false);
				if(text.equals(ManageApprovals.SUBMIT))
					label.setVisible(false);

			}
			 else if (status.equals(PurchaseRequisition.CREATED)) {
					if (text.equals("Create PO"))
						label.setVisible(false);
					if (text.equals("Create LOI"))
						label.setVisible(false);
					if (text.equals("Create RFQ"))
						label.setVisible(false);
					if (text.equals("Mark Close"))
						label.setVisible(false);
					if (text.equals(ManageApprovals.APPROVALREQUEST))
						label.setVisible(true);
					if (text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
						label.setVisible(false);
					if (text.equals(AppConstants.CANCELPR))
						label.setVisible(false);
					if (text.equals(AppConstants.ASSIGNPRTOPOPURCHASEENGG))
						label.setVisible(false);
					
					/**
					 * @author Anil
					 * @since 04-01-2021
					 */
					if(getManageapproval().isSelfApproval()){
						if(text.equals(ManageApprovals.SUBMIT))
							label.setVisible(true);
						if(text.equals(ManageApprovals.APPROVALREQUEST))
							label.setVisible(false);
					}else{
						if(text.equals(ManageApprovals.SUBMIT))
							label.setVisible(false);
						if(text.equals(ManageApprovals.APPROVALREQUEST))
							label.setVisible(true);
					}
				}
			else if (status.equals(PurchaseRequisition.REQUESTED)){
				if (text.equals("Create PO"))
					label.setVisible(false);
				if (text.equals("Create LOI"))
					label.setVisible(false);
				if (text.equals("Create RFQ"))
					label.setVisible(false);
				if (text.equals("Mark Close"))
					label.setVisible(false);
				if(text.equals(ManageApprovals.SUBMIT))
					label.setVisible(false);
				if (text.equals(ManageApprovals.APPROVALREQUEST))
					label.setVisible(false);
				if (text.equals(AppConstants.CANCELPR))
					label.setVisible(false);
				if (text.equals(AppConstants.ASSIGNPRTOPOPURCHASEENGG))
					label.setVisible(false);
				if (text.equals(ManageApprovals.APPROVALSTATUS))
					label.setVisible(true);
			}
			else {
				if (text.equals("Create PO"))
					label.setVisible(false);
				if (text.equals("Create LOI"))
					label.setVisible(false);
				if (text.equals("Create RFQ"))
					label.setVisible(false);
				if (text.equals("Mark Close"))
					label.setVisible(false);
				if (text.equals(AppConstants.CANCELPR))
					label.setVisible(false);
				if (text.equals(AppConstants.ASSIGNPRTOPOPURCHASEENGG))
					label.setVisible(false);
				if (text.equals(ManageApprovals.APPROVALSTATUS))
					label.setVisible(true);
			}
		}
	}

	
	@Override
	public void onClick(ClickEvent event) {
		if (event.getSource().equals(bAddproducts)&& !productInfoComposite.getProdCode().getValue().equals("")) {
			if (validateProduct()) {
				prodctType(productInfoComposite.getProdCode().getValue());
			}
		}
	}

	/********************************************** initialize product *********************************************/
	
	protected void initalizEmployeeNameListBox() {
		MyQuerry querry = new MyQuerry(new Vector<Filter>(), new Employee());
		oblEmployee.MakeLive(querry);
	}

	protected void initalizEmployeeBranchListBox() {
		MyQuerry querry = new MyQuerry(new Vector<Filter>(), new Branch());
		branch.MakeLive(querry);
	}

	/************************************** validate Product for duplicate entry **********************************/

	public boolean validateProduct() {
		
		/**
		 * Date 16-06-2019 by Vijay
		 * Des :- Expected Delivery note non mandatory with process config
		 * for NBHC Inventory Management
		 */
		if(!AppUtility.checkForProcessConfigurartionIsActiveOrNot("PurchaseRequisition", "MakeExpectedDeliveryDateNonMandatory")){
			if(dbExpectedDeliveryDate.getValue()==null){
				this.showDialogMessage("Please select delivery date !");
				return false;
			}	
		}
		
		
		if(ProductTablePR.isValidWarehouse==false){
			showDialogMessage("Warehouse is already selected for product, please change warehouse or delete row.");
			return false;
		}
		
		int prodid = productInfoComposite.getIdValue();
		List<ProductDetails> list = productTablePR.getDataprovider().getList();
		
		/**
		 * Date : 03-03-2017 By ANil
		 */
		
		int currProdfd=firstDigit(prodid);
		for (ProductDetails temp : list) {
			if (prodid==temp.getProductID()&&temp.getWarehouseName().equals("")) {
				this.showDialogMessage("Can not add Duplicate product");
				return false;
			}
			int addedProdfd=firstDigit(temp.getProductID());
			System.out.println("i "+addedProdfd+" j "+currProdfd);
			if(currProdfd!=addedProdfd){
				this.showDialogMessage("You can either add item product or service product.");
				return false;
			}
		}
		return true;
	}

	/**
	 * Date : 03-03-2017 By ANil
	 */

	public int firstDigit(int x) {
	  if (x == 0) return 0;
	  x = Math.abs(x);
	  return (int) Math.floor(x / Math.pow(10, Math.floor(Math.log10(x))));
	}

	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		ibPRId.setEnabled(false);
		tbStatus.setEnabled(false);
		productTablePR.setEnable(state);
		tbcommentForStatusChange.setEnabled(false);
		dbCreationDate.setEnabled(false);

		/**
		 * Date : 09-02-2017 By Anil
		 * Made this Read only for NBHC DeadStock
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Employee", "PurchaseEngineerPRPO")){
			oblEmployee.setEnabled(false);
		}
		/**
		 * End
		 */
	}

	@Override
	public TextBox getstatustextbox() {
		return this.tbStatus;
	}

	/************************************************ validate *****************************************************/

	@Override
	public boolean validate() {
		boolean a = super.validate();
		if (a == false)
			return false;

//		if (dbCreationDate.getValue().after(dbExpectedClosureDate.getValue())) {
//			this.showDialogMessage("Expected Closure Date Should Be Greater Than Creation Date.");
//			return false;
//		} else 
			if (dbCreationDate.getValue().after(dbExpectedDeliveryDate.getValue())) {
				this.showDialogMessage("Expected Delivery Date Should Be Greater Than Creation Date.");
				return false;
			} else if (dbExpectedResposeDate.getValue() != null) {
			if (dbCreationDate.getValue().after(dbExpectedResposeDate.getValue())){
				this.showDialogMessage("Expected Response Date Should Be Greater Than Creation Date.");
				return false;
			}
			else if (dbExpectedResposeDate.getValue().after(dbExpectedDeliveryDate.getValue())) {
				this.showDialogMessage("Expected Deliver Date Should Be Greater Than Expected Response Date.");
				return false;
			}
//			else if (dbExpectedResposeDate.getValue().after(dbExpectedClosureDate.getValue())) {
//				this.showDialogMessage("Expected Closure Date Should Be Greater Than Expected Response Date.");
//				return false;
//			}
		}
//		else if (dbExpectedDeliveryDate.getValue().after(dbExpectedClosureDate.getValue())) {
//		this.showDialogMessage("Expected Closure Date Should Be Greater Than Expected Delivery Date.");
//		return false;
//		} 

		

		if (productTablePR.getDataprovider().getList().size() == 0) {
			this.showDialogMessage("Please add atleast one product");
			return false;
		}else{
			Console.log("in else");
			List<ProductDetails> productDetailsList=productTablePR.getDataprovider().getList();
			if(productDetailsList!=null){
				Console.log("checking product name");
				for(ProductDetails detail:productDetailsList){
					if(detail.getProductName()==null||detail.getProductName().equals("")){
						showDialogMessage("Product name should not be blank! Add product name");
						return false;
					}else if(detail.getProductName().contains("@")){
						showDialogMessage("Remove @ from Product Name");
						return false;
					}else if(detail.getProductName().contains("\"")){
						showDialogMessage("Remove double quote (\") from Product Name");
						return false;
					}else if(detail.getProductName().contains("$")){
						showDialogMessage("Remove Dollar symbol ($) from Product Name");
						return false;
					}else if(detail.getProductName().contains("'")){
						showDialogMessage("Remove single quote (') from Product Name");
						return false;
					}else if(detail.getProductName().contains(",")){
						showDialogMessage("Remove comma (,) from Product Name");
						return false;
					}
				}
			}
		}
		
		
		if(!validatePRProdQty()){
			showDialogMessage("Please enter appropriate quantity required!");
			return false;
		}
		
		
		if(ProductTablePR.isValidWarehouse==false){
			showDialogMessage("Warehouse is already selected for product, please change warehouse or delete row.");
			return false;
		}
//		List<ProductDetails> list =productTablePR.getDataprovider().getList();
//		for(ProductDetails prod:list){
//			if(prod.getWarehouseName()==null){
//				showDialogMessage("Warehouse is already selected for "+prod.getProductName()+"\n"+" please change warehouse or delete row.");
//				return false;
//			}
//		}

		/**
		 *  nidhi
		 *  14-09-2017
		 *  for check product Price 
		 */
		if(!validatePRProdPrice()){
			showDialogMessage("Please enter appropriate Product Price required!");
			return false;
		}
		/**
		 * end
		 */
		
		
		return true;
	}
	
	
	public boolean validatePRProdQty()
	{
		List<ProductDetails> prProdLis=productTablePR.getDataprovider().getList();
		int ctr=0;
		for(int i=0;i<prProdLis.size();i++)
		{
			if(prProdLis.get(i).getProductQuantity()==0)
			{
				ctr=ctr+1;
			}
		}
		
		if(ctr==0){
			return true;
		}
		else{
			return false;
		}
	}

	/*************************************************************************************************/

	

	@Override
	public void setCount(int count) {
		ibPRId.setValue(count+"");
		setCountInProductTable(count);
		
	}

	/** 
	 * This method sets the pr Id to product table Pr Id after click of save
	 * Date :12-10-2016 By Anil
	 * Release : 30 Sept 2016
	 * Project : PURCHASE MODIFICATION (NBHC)
	 */
	public void setCountInProductTable(int count){
		for(ProductDetails obj:productTablePR.getDataprovider().getList()){
			if(obj.getPrCount()==0){
				obj.setPrCount(count);
			}
		}
		productTablePR.getTable().redraw();
	}
	
	
	
	@Override
	public void setToViewState() {
		super.setToViewState();
		setMenuAsPerStatus();
		
		SuperModel model=new PurchaseRequisition();
		model=purchaseRequisitionobj;
		AppUtility.addDocumentToHistoryTable(AppConstants.PURCHASEMODULE,AppConstants.PURCHASEREQUISION, purchaseRequisitionobj.getCount(), null,null,null, false, model, null);
	
		String mainScreenLabel="Purchase Requisition";
		if(purchaseRequisitionobj!=null&&purchaseRequisitionobj.getCount()!=0){
			mainScreenLabel=purchaseRequisitionobj.getCount()+" "+"/"+" "+purchaseRequisitionobj.getStatus()+" "+"/"+" "+AppUtility.parseDate(purchaseRequisitionobj.getCreationDate());
		}
			fgroupingLetterInformation.getHeaderLabel().setText(mainScreenLabel);
	
	}


	@Override
	public void setToEditState() {
		super.setToEditState();
		if (tbStatus.getValue().equals(PurchaseRequisition.REJECTED)) {
			tbStatus.setValue(PurchaseRequisition.CREATED);
		}
		
		if(isConsolidatedPr()){
//			int tablecolcount = this.productTablePR.getTable().getColumnCount();
//			for (int i = tablecolcount - 1; i > -1; i--){
//				this.productTablePR.getTable().removeColumn(i);
//			}
//			productTablePR.addViewColumn();
			productTablePR.setEnable(false);
			bAddproducts.setEnabled(false);
			productInfoComposite.setEnable(false);
		}
		/** Date 31-08-2019 by Vijay for NBHC Inventory Management ****/
		if(isPopUpAppMenubar()&&!(AppUtility.checkForProcessConfigurartionIsActiveOrNot("PurchaseRequisition","PReditableonPRUpload"))){
			bAddproducts.setEnabled(false);
		}
		setMenuAsPerStatus();
		this.processLevelBar.setVisibleFalse(false);
		
		String mainScreenLabel="Purchase Requisition";
		if(purchaseRequisitionobj!=null&&purchaseRequisitionobj.getCount()!=0){
			mainScreenLabel=purchaseRequisitionobj.getCount()+" "+"/"+" "+purchaseRequisitionobj.getStatus()+" "+"/"+" "+AppUtility.parseDate(purchaseRequisitionobj.getCreationDate());
		}
			fgroupingLetterInformation.getHeaderLabel().setText(mainScreenLabel);
	}
	
	
	/**
	 * This method return true if its a consolidated PR 
	 * Date : 22-11-2016 By Anil
	 * Project: DEADSTOCK NBHC
	 * 
	 */
	
	public boolean isConsolidatedPr(){
		HashSet<Integer> prSet=new HashSet<Integer>();
		for(ProductDetails obj:productTablePR.getDataprovider().getList()){
			if(obj.getPrCount()!=0){
				prSet.add(obj.getPrCount());
			}
		}
		if(prSet.size()>1){
			return true;
		}
		return false;
	}
	
	
	public void setToNewState() {
		super.setToNewState();
		tbStatus.setValue(PurchaseRequisition.CREATED);
		
	}

	public void setMenuAsPerStatus() {
		this.setAppHeaderBarAsPerStatus();
		this.toggleProcessLevelMenu();

	}

	public void setAppHeaderBarAsPerStatus() {
		PurchaseRequisition entity = (PurchaseRequisition) presenter.getModel();
		String status = entity.getStatus();
		if (status.equals(PurchaseRequisition.CLOSE)|| status.equals(PurchaseRequisition.CANCELLED)|| status.equals(PurchaseRequisition.PROCESSED))
		{
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Discard") || text.contains("Search"))
				{
					menus[k].setVisible(true);
				}
				else {
					menus[k].setVisible(false);
				}
			}
		}
		
		if (status.equals(PurchaseRequisition.APPROVED))
		{
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Print"))
				{
					menus[k].setVisible(false);
				}
			}
		}
	}
	
	/****************** Setting Product Table PR and Vendor List From ProductInfoComposite,PriceList *****************/
	
	public void prodctType(String pcode) {
		System.out.println("Started");
		final GenricServiceAsync genasync = GWT.create(GenricService.class);
		final MyQuerry querry = new MyQuerry();
		Company c=new Company();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		filter = new Filter();
		filter.setQuerryString("productCode");
		filter.setStringValue(pcode.trim());
		filtervec.add(filter);
		filter=new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new SuperProduct());
		showWaitSymbol();
		Timer timer = new Timer() {
			@Override
			public void run() {
				genasync.getSearchResult(querry,
						new AsyncCallback<ArrayList<SuperModel>>() {

							@Override
							public void onFailure(Throwable caught) {
								showDialogMessage("An Unexpected error occurred!");
							}

							@Override
							public void onSuccess(ArrayList<SuperModel> result) {
								for (SuperModel model : result) {
									SuperProduct superProdEntity = (SuperProduct) model;
									ProductDetails lis = AppUtility.ReactOnAddPurchaseReqProductComposite(superProdEntity);
									if(!ibPRId.getValue().equals("")&&!ibPRId.getValue().equals("0")){
										lis.setPrCount(Integer.parseInt(ibPRId.getValue()));
									}
									lis.setProdDate(dbExpectedDeliveryDate.getValue());
									productTablePR.getDataprovider().getList().add(lis);
									
//									checkAndAddVendorToVendorTable(lis);
									
									
								}
							}
						});
				hideWaitSymbol();
			}
		};
		timer.schedule(3000);
	}
	
	
	
	
	/**
	 * This method takes product details which we are adding in product table as input and 
	 * checks whether vendor price list for that product is exist or not
	 * if exist then checks for common vendor and add product and vendor to their respective table.
	 * 
	 * Date : 10-09-2016 By Anil
	 * Release :31 Aug 2016
	 * Project:PURCHASE ERROR SOLVING (NBHC)
	 * I/P:Product of type ProductDetails
	 */
	
	
	public void checkAndAddVendorToVendorTable(final ProductDetails prodDetails){
		
		System.out.println("One");
		
		MyQuerry querry=new MyQuerry();
		Filter filter = new Filter();
		filter.setQuerryString("prodCode");
		filter.setStringValue(productInfoComposite.getProdCode().getValue().trim());
		querry.getFilters().add(filter);
		querry.setQuerryObject(new PriceList());
		System.out.println("Two");
		
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				
			}
			@Override
			public void onSuccess(ArrayList<SuperModel> vresult) {
				System.out.println("VEN PRICE LIS SIZE : "+vresult.size());
				if (vresult.size() != 0) {
					for(SuperModel smodel:vresult){
						System.out.println("Three");
						PriceList pentity=(PriceList) smodel;
						if(isCommonVendor(pentity.getPriceInfo())){
							System.out.println("Four");
							productTablePR.getDataprovider().getList().add(prodDetails);
							vendorRateList.addAll(pentity.getPriceInfo());
						}else{
							System.out.println("Five");
							showDialogMessage("No common vendor for this product !");
							return;
						}
						addUniqueVendorsToTable(pentity.getPriceInfo());
					}
				}else{
					System.out.println("Six");
					showDialogMessage("Vendor Price List does not exists for product");
				}
			}
		});		
	}
	
	/**
	 * This method checks whether vendors are common or not for added product if not then it will not allow you add product
	 * Date 10-09-2016 By Anil
	 * Release : 31 Aug 2016
	 * Project: PURCHASE ERROR SOLVING(NBHC)
	 * I/P:Current added products vendor price list and checks with  already added vendor list
	 * O/P: return true if at least one vendor common else false
	 */

	private boolean isCommonVendor(List<PriceListDetails>venRateLis) {
		System.out.println("CHECKING COMMON VENDOR ");
		System.out.println("VEN TBL SIZE "+vendorDetailList.size());
		if(vendorDetailList.size()==0){
			return true;
		}
		for(VendorDetails gblVenLis:vendorDetailList){
			for(PriceListDetails currVenLis:venRateLis){
				System.out.println("VENDOR ID "+gblVenLis.getVendorId() +"    --   "+currVenLis.getVendorID());
				if(gblVenLis.getVendorId()==currVenLis.getVendorID()){
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * This method adds only common vendor to vendor table 
	 * Date 10-09-2016 By Anil
	 * Release : 31 Aug 2016
	 * Project: PURCHASE ERROR SOLVING(NBHC)
	 * I/P:Current added products vendor list and checks with already added vendor list,maintain only common vendors list else all removed 
	 */
	
	private void addUniqueVendorsToTable(List<PriceListDetails>venRateLis) {
		System.out.println("ADDING UNIQUE VENDOR");
		if(vendorDetailList.size()==0){
			System.out.println("ADDING FOR 1ST TIME");
			ArrayList<VendorDetails> venList=getVendorList(venRateLis);	
			vendorDetailList.addAll(venList);
		}else{
			System.out.println("ADDING ");
			ArrayList<VendorDetails> commonVenList=getCommonVendorList(vendorDetailList,venRateLis);
			vendorDetailList.clear();
			vendorDetailList.addAll(commonVenList);
		}
		
		System.out.println("FINAL VENDOR LIST SIZE "+vendorDetailList.size());
		
		
	}
	
	/**
	 * This method returns common vendors list by comparing from existing and currently to be added vendor list.
	 * Date 10-09-2016 By Anil
	 * Release : 31 Aug 2016
	 * Project: PURCHASE ERROR SOLVING(NBHC)
	 * I/P:existing vendor list and currently added vendor price list
	 * O/P: common vendor list
	 */
	
	private ArrayList<VendorDetails> getCommonVendorList(ArrayList<VendorDetails> vendorList,List<PriceListDetails> venRateLis) {
		System.out.println("CHECKING ADDING COMMON VENDOR ONLY ");
		ArrayList<VendorDetails> vendList=new ArrayList<VendorDetails>();
		for(VendorDetails venDet:vendorList){
			for(PriceListDetails venRLis:venRateLis){
				if(venDet.getVendorId()==venRLis.getVendorID()){
					vendList.add(venDet);
				}
			}
		}
		System.out.println("VEN LIS SIZE "+vendList.size());
		return vendList;
	}
	
	
	
	/**
	 * This method convert the vendor list from  type PriceListDetails to type VendorDetails
	 * Date 10-09-2016 By Anil
	 * Release : 31 Aug 2016
	 * Project: PURCHASE ERROR SOLVING(NBHC)
	 * I/P:vendor list of type PriceListDetails
	 * O/P: vendor list of type VendorDetails 
	 */
	
	public ArrayList<VendorDetails> getVendorList(List<PriceListDetails> venRateLis) {
		System.out.println("CONVERTING LIST.....");
		ArrayList<VendorDetails> vendorList=new ArrayList<VendorDetails>();
		
		for (PriceListDetails temp : venRateLis) {
			VendorDetails ve = new VendorDetails();
			ve.setVendorEmailId(temp.getEmail());
			ve.setVendorId(temp.getVendorID());
			ve.setVendorName(temp.getFullName());
			ve.setVendorPhone(temp.getCellNumber());
			vendorList.add(ve);
		}
		return vendorList;
	}
	
	
	
	

	/**
	 * This method load global vendor list when we are viewing the records by searching.
	 * Date: 10-09-2016
	 * Release : 31 Aug 2016
	 * Project: PURCHASE ERROR SOLVING(NBHC)
	 * I/P: List of product code
	 * 
	 */
	private void loadVendorProductPriceList(List<String> prodIdList) {
		
		
		for(String obj:prodIdList){
			System.out.println("CODE : "+obj);
		}
		MyQuerry querry=new MyQuerry();
		Filter filter=null;
		Vector<Filter>temp=new Vector<Filter>();
		
		filter=new Filter();
		filter.setList(prodIdList);
		filter.setQuerryString("prodCode IN");
		temp.add(filter);
		
		filter=new Filter();
		filter.setLongValue(UserConfiguration.getCompanyId());
		filter.setQuerryString("companyId");
		temp.add(filter);
		
		
		
		querry.setFilters(temp);
		querry.setQuerryObject(new PriceList());
		
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				System.out.println("LOAD RESULT "+result.size());
				vendorRateList=new ArrayList<PriceListDetails>();
				for(SuperModel model:result){
					PriceList entity=(PriceList) model;
					vendorRateList.addAll(entity.getPriceInfo());
				}
				System.out.println("GLOBAL VEN LIST SIZE IN LOAD "+vendorRateList.size());
			}
		});
		
		
		
	}
	
	
	
	
	
	

	/*******************************************Type Drop Down Logic**************************************/
	
	@Override
	public void onChange(ChangeEvent event) {
		if(event.getSource().equals(oblPRCategory))
		{
			if(oblPRCategory.getSelectedIndex()!=0){
				ConfigCategory cat=oblPRCategory.getSelectedItem();
				if(cat!=null){
					AppUtility.makeLiveTypeDropDown(oblPRType, cat.getCategoryName(), cat.getCategoryCode(), cat.getInternalType());
				}
			}
		}
		
	}
	
	/**
	 *  nidhi
	 *  14-09-2017
	 *  
	 */
	public boolean validatePRProdPrice()
	{
		List<ProductDetails> prProdLis=productTablePR.getDataprovider().getList();
		int ctr=0;
		for(int i=0;i<prProdLis.size();i++)
		{
			if(prProdLis.get(i).getProdPrice()==0)
			{
				ctr=ctr+1;
			}
		}
		
		if(ctr==0){
			return true;
		}
		else{
			return false;
		}
	}

	@Override
	public void refreshTableData() {
		// TODO Auto-generated method stub
		super.refreshTableData();
		productTablePR.getTable().redraw();
	}
	
	/**
	 *  end
	 */
	
	
}
