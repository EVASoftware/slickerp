package com.slicktechnologies.client.views.purchase.purchaserequisition;

import java.util.Vector;

import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.config.CategoryTypeFactory;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.businessprocesslayer.PriceList;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseRequisition;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.personlayer.Employee;

public class PurchaseRequisitionPresenterSearch extends
		SearchPopUpScreen<PurchaseRequisition> {

	public DateBox dbCreationDate;
	TextBox ibRefOrderNo;
	ObjectListBox<Employee> oblEmployee;
	ObjectListBox<Config> oblPRGroup;
	public TextBox tbStatus;
	public TextBox tbprName;
	
	ObjectListBox<ConfigCategory> oblPRCategory;
	ObjectListBox<Type> oblPRType;
	
	public IntegerBox ibPRId;
	ObjectListBox<Branch> branch;
	ObjectListBox<Employee> tbApporverName;
	ObjectListBox<HrProject> project;
	ObjectListBox<PriceList> veninfo;
	ListBox lbstatus;
	DateComparator datecomp;

	public PurchaseRequisitionPresenterSearch() {
		super();
		createGui();
	}
	
	public PurchaseRequisitionPresenterSearch(boolean b) {
		super(b);
		createGui();
	}

	private void initalizeWidget() {

		dbCreationDate = new DateBoxWithYearSelector();

		oblEmployee = new ObjectListBox<Employee>();
		oblEmployee.makeEmployeeLive(AppConstants.PURCHASEMODULE, AppConstants.PURCHASEREQUISION, "Purchase Requisition");
		
		ibRefOrderNo = new TextBox();
		oblPRGroup = new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(oblPRGroup, Screen.PRGROUP);

		oblPRCategory = new ObjectListBox<ConfigCategory>();
		oblPRType = new ObjectListBox<Type>();
		CategoryTypeFactory.initiateOneManyFunctionality(oblPRCategory,oblPRType);
		AppUtility.makeTypeListBoxLive(this.oblPRType,Screen.PRCATEGORY);
		tbStatus = new TextBox();
		tbStatus.setEnabled(false);
		tbprName = new TextBox();
		veninfo = new ObjectListBox<PriceList>();
//		initalizEmployeeNameListBox();
		project = new ObjectListBox<HrProject>();
		HrProject.MakeProjectListBoxLive(project);
		ibPRId = new IntegerBox();
		tbApporverName = new ObjectListBox<Employee>();
		branch = new ObjectListBox<Branch>();
		lbstatus = new ListBox();
		AppUtility.setStatusListBox(lbstatus,PurchaseRequisition.getStatusList());

		datecomp = new DateComparator("purchasedetails.creationDate",new PurchaseRequisition());

		initalizEmployeeBranchListBox();
		initalizproductListBox();
		initalizAppNameListBox();
	}

	@Override
	public void createScreen() {
		initalizeWidget();

		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();

		fbuilder = new FormFieldBuilder("PR ID", ibPRId);
		FormField fibLetterOfIntentID = fbuilder.setMandatory(false)
				.setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("PR Title", tbprName);
		FormField ftbpoName = fbuilder.setMandatory(false)
				.setMandatoryMsg("Title is mandatory").setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("PR From Date", datecomp.getFromDate());
		FormField fdate = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("PR To Date", datecomp.getToDate());
		FormField ftodate = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Ref. Ord. Nr", ibRefOrderNo);
		FormField freferenceorderno = fbuilder.setMandatory(false)
				.setMandatoryMsg("").setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Pur. Eng.", oblEmployee);
		FormField femployee = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("PR Type", oblPRType);
		FormField floitype = fbuilder.setMandatory(false)
				.setMandatoryMsg("PO type is mandatory").setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("PR Group.", oblPRGroup);
		FormField floigroup = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Status", lbstatus);
		FormField ftbStatus = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Branch", branch);
		FormField fbranch = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Approver", tbApporverName);
		FormField ftbApporverName = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Project", project);
		FormField fproject = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("PR Category", oblPRCategory);
		FormField foblPRCategory = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		FormField[][] formfield = {
				{ fibLetterOfIntentID, ftbpoName,fdate,ftodate}, 
				{ freferenceorderno,floigroup,foblPRCategory, floitype},
				{ fbranch,femployee,ftbApporverName,fproject },
				{ftbStatus },
		};

		this.fields = formfield;

	}

	@Override
	public MyQuerry getQuerry() {
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter temp = null;

//		if (datecomparator.getValue() != null) {
//			filtervec.addAll(datecomparator.getValue());
//		}

		if (datecomp.getValue() != null) {
			filtervec.addAll(datecomp.getValue());
		}

		if (ibPRId.getValue() != null) {
			temp = new Filter();
			temp.setIntValue(ibPRId.getValue());
			temp.setQuerryString("count");
			filtervec.add(temp);

		}

		if (!tbprName.getValue().equals("")) {
			temp = new Filter();
			temp.setStringValue(tbprName.getValue());
			temp.setQuerryString("prTitle");
			filtervec.add(temp);

		}
		if (!ibRefOrderNo.getValue().equals("")) {
			temp = new Filter();
			temp.setStringValue(ibRefOrderNo.getValue());
			temp.setQuerryString("purchasedetails.refOrderNO");
			filtervec.add(temp);
		}

		if (oblEmployee.getValue() != null) {
			temp = new Filter();
			temp.setStringValue(oblEmployee.getValue());
			temp.setQuerryString("purchasedetails.employee");
			filtervec.add(temp);
		}

		if (branch.getValue() != null) {
			temp = new Filter();
			temp.setStringValue(branch.getValue());
			temp.setQuerryString("purchasedetails.branch");
			filtervec.add(temp);
		}

		if (tbApporverName.getValue() != null) {
			temp = new Filter();
			temp.setStringValue(tbApporverName.getValue());
			temp.setQuerryString("purchasedetails.approverName");
			filtervec.add(temp);
		}

		if (lbstatus.getSelectedIndex() != 0) {
			temp = new Filter();
			temp.setStringValue(lbstatus.getValue(lbstatus.getSelectedIndex()));
			temp.setQuerryString("purchasedetails.status");
			filtervec.add(temp);

		}

		if (project.getValue() != null) {

			temp = new Filter();
			int item = project.getSelectedIndex();
			String sel = project.getValue();
			temp.setStringValue(sel);
			temp.setQuerryString("purchasedetails.project");
			filtervec.add(temp);

		}

		if (oblPRCategory.getValue() != null) {
			temp = new Filter();
			temp.setStringValue(oblPRCategory.getValue());
			temp.setQuerryString("prCategory");
			filtervec.add(temp);
		}
		if (oblPRGroup.getValue() != null) {
			temp = new Filter();
			temp.setStringValue(oblPRGroup.getValue());
			temp.setQuerryString("prGroup");
			filtervec.add(temp);
		}

		if (oblPRType.getValue() != null) {
			temp = new Filter();
			temp.setStringValue(oblPRType.getValue());
			temp.setQuerryString("prType");
			filtervec.add(temp);
		}

		MyQuerry querry = new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new PurchaseRequisition());
		return querry;
	}

	protected void initalizEmployeeNameListBox() {
		MyQuerry querry = new MyQuerry(new Vector<Filter>(), new Employee());

		oblEmployee.MakeLive(querry);
	}

	protected void initalizEmployeeBranchListBox() {
		MyQuerry querry = new MyQuerry(new Vector<Filter>(), new Branch());

		branch.MakeLive(querry);
	}

	protected void initalizproductListBox() {
		MyQuerry querry = new MyQuerry(new Vector<Filter>(), new PriceList());

		veninfo.MakeLive(querry);
	}

	protected void initalizAppNameListBox() {
		MyQuerry querry = new MyQuerry(new Vector<Filter>(), new Employee());

		tbApporverName.MakeLive(querry);
	}

	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
		if(LoginPresenter.branchRestrictionFlag)
		{
			/**
			 * Date 27-04-2017 if condtion added by vijay
			 * for deadstock NBHC for user role PO Head allow to search without selecting BRANCH
			 */
		 if (!LoginPresenter.myUserEntity.getRole().getRoleName().trim().equalsIgnoreCase("PO Head")) {
			 
			if(branch.getSelectedIndex()==0)
			{
				showDialogMessage("Select Branch");
				return false;
			}
			
		   }
		}
		return true;
	}

}
