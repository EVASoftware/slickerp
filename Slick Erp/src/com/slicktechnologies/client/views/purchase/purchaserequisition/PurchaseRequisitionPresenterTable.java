package com.slicktechnologies.client.views.purchase.purchaserequisition;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseRequisition;

public class PurchaseRequisitionPresenterTable extends SuperTable<PurchaseRequisition> {

	TextColumn<PurchaseRequisition> Columncount;
	TextColumn<PurchaseRequisition> ColumnPRTitle;
	TextColumn<PurchaseRequisition> ColumnCreationDate;
	TextColumn<PurchaseRequisition> ColumnExptectedDeliveryDate;
	TextColumn<PurchaseRequisition> ColumnRefOrderDate;
	TextColumn<PurchaseRequisition> ColumnResponseDate;
	TextColumn<PurchaseRequisition> ColumnProject;
	TextColumn<PurchaseRequisition> ColumnReforderNo;
	TextColumn<PurchaseRequisition> ColumnPurchaseEngg;
	TextColumn<PurchaseRequisition> ColumnBranch;
	TextColumn<PurchaseRequisition> ColumnPRType;
	TextColumn<PurchaseRequisition> ColumnPRGrpup;
	TextColumn<PurchaseRequisition> ColumnPRCategory;
	TextColumn<PurchaseRequisition> ColumnAprroverName;
	TextColumn<PurchaseRequisition> ColumnStatus;

	@Override
	public void createTable() {
		createColumnCount();
		createColumnPRTitle();
		createColumnExpectedDelDate();
		createColumnPRCategory();
		createColumnPRType();
		createColumnPRGroup();
		createColumnProject();
		createColumnRefOrderNo();
		createColumnRefOrderDate();
		createColumnResponseDate();
		createColumnBranch();
		createColumnPurchaseEngg();
		createColumnPRApproverName();
		createColumnCreationDate();
		createColumnStatus();
	}

	public void addColumnSorting() {
		createColumnSortingCount();
		createColumnSortingPRTitle();
		createColumnSortingCreationDate();
		createColumnSortingExpectedDelDate();
		createColumnSortingRefOrderDate();
		createColumnSortingResponseDate();
		createColumnSortingProject();
		createColumnSortingRefOrderNo();
		createColumnSortingPurchaseEngg();
		createColumnSortingBranch();
		createColumnSortingPRType();
		createColumnSortingPRGroup();
		createColumnSortingPRCategory();
		createColumnSortingPRApproverName();
		createColumnSortingStatus();

	}

	

	/**************************** sorting columns *****************************************/

	
	public void createColumnSortingPRApproverName() {
		List<PurchaseRequisition> list = getDataprovider().getList();
		columnSort = new ListHandler<PurchaseRequisition>(list);
		columnSort.setComparator(ColumnAprroverName,
				new Comparator<PurchaseRequisition>() {
					@Override
					public int compare(PurchaseRequisition e1,
							PurchaseRequisition e2) {
						if (e1 != null && e2 != null) {
							if (e1.getApproverName() != null
									&& e2.getApproverName() != null) {
								return e1.getApproverName().compareTo(
										e2.getApproverName());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	public void createColumnSortingBranch() {
		List<PurchaseRequisition> list = getDataprovider().getList();
		columnSort = new ListHandler<PurchaseRequisition>(list);
		columnSort.setComparator(ColumnBranch,
				new Comparator<PurchaseRequisition>() {
					@Override
					public int compare(PurchaseRequisition e1,
							PurchaseRequisition e2) {
						if (e1 != null && e2 != null) {
							if (e1.getBranch() != null
									&& e2.getBranch() != null) {
								return e1.getBranch().compareTo(e2.getBranch());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	
	public void createColumnSortingCount() {
		List<PurchaseRequisition> list = getDataprovider().getList();
		columnSort = new ListHandler<PurchaseRequisition>(list);
		columnSort.setComparator(Columncount,new Comparator<PurchaseRequisition>() {
					@Override
					public int compare(PurchaseRequisition e1,
							PurchaseRequisition e2) {
						if (e1 != null && e2 != null) {
							if (e1.getCount() == e2.getCount()) {
								return 0;
							}
							if (e1.getCount() > e2.getCount()) {
								return 1;
							} else {
								return -1;
							}
						} else {
							return 0;
						}
					}
				});
		table.addColumnSortHandler(columnSort);

	}

	public void createColumnSortingPRTitle() {
		List<PurchaseRequisition> list = getDataprovider().getList();
		columnSort = new ListHandler<PurchaseRequisition>(list);
		columnSort.setComparator(ColumnPRTitle,
				new Comparator<PurchaseRequisition>() {
					@Override
					public int compare(PurchaseRequisition e1,
							PurchaseRequisition e2) {
						if (e1 != null && e2 != null) {
							if (e1.getPrTitle() != null
									&& e2.getPrTitle() != null) {
								return e1.getPrTitle().compareTo(
										e2.getPrTitle());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);

	}

	public void createColumnSortingCreationDate() {
		List<PurchaseRequisition> list = getDataprovider().getList();
		columnSort = new ListHandler<PurchaseRequisition>(list);
		columnSort.setComparator(ColumnCreationDate,
				new Comparator<PurchaseRequisition>() {
					@Override
					public int compare(PurchaseRequisition e1,
							PurchaseRequisition e2) {
						if (e1 != null && e2 != null) {
							if (e1.getCreationDate() != null
									&& e2.getCreationDate() != null) {
								return e1.getCreationDate().compareTo(
										e2.getCreationDate());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);

	}

	public void createColumnSortingExpectedDelDate() {
		List<PurchaseRequisition> list = getDataprovider().getList();
		columnSort = new ListHandler<PurchaseRequisition>(list);
		columnSort.setComparator(ColumnExptectedDeliveryDate,new Comparator<PurchaseRequisition>() {
					@Override
					public int compare(PurchaseRequisition e1,PurchaseRequisition e2) {
						if (e1 != null && e2 != null) {
							if (e1.getExpectedDeliveryDate() != null&& e2.getExpectedDeliveryDate() != null) {
								return e1.getExpectedDeliveryDate().compareTo(e2.getExpectedDeliveryDate());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);

	}

	public void createColumnSortingRefOrderDate() {
		List<PurchaseRequisition> list = getDataprovider().getList();
		columnSort = new ListHandler<PurchaseRequisition>(list);
		columnSort.setComparator(ColumnRefOrderDate,
				new Comparator<PurchaseRequisition>() {
					@Override
					public int compare(PurchaseRequisition e1,PurchaseRequisition e2) {
						if (e1 != null && e2 != null) {
							if (e1.getRefOrderDate() != null&& e2.getRefOrderDate() != null) {
								return e1.getRefOrderDate().compareTo(e2.getRefOrderDate());
							}
							if (e1.getRefOrderDate().equals("")&& e2.getRefOrderDate().equals("")) {
								return 0;
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);

	}

	public void createColumnSortingResponseDate() {
		List<PurchaseRequisition> list = getDataprovider().getList();
		columnSort = new ListHandler<PurchaseRequisition>(list);
		columnSort.setComparator(ColumnResponseDate,
				new Comparator<PurchaseRequisition>() {
					@Override
					public int compare(PurchaseRequisition e1,
							PurchaseRequisition e2) {
						if (e1 != null && e2 != null) {
							if (e1.getResponseDate() != null
									&& e2.getResponseDate() != null) {
								return e1.getResponseDate().compareTo(
										e2.getResponseDate());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);

	}

	public void createColumnSortingProject() {
		List<PurchaseRequisition> list = getDataprovider().getList();
		columnSort = new ListHandler<PurchaseRequisition>(list);
		columnSort.setComparator(ColumnProject,
				new Comparator<PurchaseRequisition>() {
					@Override
					public int compare(PurchaseRequisition e1,
							PurchaseRequisition e2) {
						if (e1 != null && e2 != null) {
							if (e1.getProject() != null
									&& e2.getProject() != null) {
								return e1.getProject().compareTo(
										e2.getProject());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);

	}

	public void createColumnSortingRefOrderNo() {
		List<PurchaseRequisition> list = getDataprovider().getList();
		columnSort = new ListHandler<PurchaseRequisition>(list);
		columnSort.setComparator(ColumnReforderNo,new Comparator<PurchaseRequisition>() {
					@Override
					public int compare(PurchaseRequisition e1,PurchaseRequisition e2) {
						if (e1 != null && e2 != null) {
							if (e1.getRefOrderNO() != null&& e2.getRefOrderNO() != null) {
								return e1.getRefOrderNO().compareTo(e2.getRefOrderNO());
							}
						} 
						else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);

	}

	public void createColumnSortingPurchaseEngg() {
		List<PurchaseRequisition> list = getDataprovider().getList();
		columnSort = new ListHandler<PurchaseRequisition>(list);
		columnSort.setComparator(ColumnPurchaseEngg,
				new Comparator<PurchaseRequisition>() {
					@Override
					public int compare(PurchaseRequisition e1,
							PurchaseRequisition e2) {
						if (e1 != null && e2 != null) {
							if (e1.getEmployee() != null
									&& e2.getEmployee() != null) {
								return e1.getEmployee().compareTo(
										e2.getEmployee());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);

	}

	public void createColumnSortingPRType() {
		List<PurchaseRequisition> list = getDataprovider().getList();
		columnSort = new ListHandler<PurchaseRequisition>(list);
		columnSort.setComparator(ColumnPRType,
				new Comparator<PurchaseRequisition>() {
					@Override
					public int compare(PurchaseRequisition e1,
							PurchaseRequisition e2) {
						if (e1 != null && e2 != null) {
							if (e1.getPrType() != null
									&& e2.getPrType() != null) {
								return e1.getPrType().compareTo(e2.getPrType());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);

	}

	public void createColumnSortingPRGroup() {
		List<PurchaseRequisition> list = getDataprovider().getList();
		columnSort = new ListHandler<PurchaseRequisition>(list);
		columnSort.setComparator(ColumnPRGrpup,
				new Comparator<PurchaseRequisition>() {
					@Override
					public int compare(PurchaseRequisition e1,PurchaseRequisition e2) {
						if (e1 != null && e2 != null) {
							if (e1.getPrGroup() != null&& e2.getPrGroup() != null) {
								return e1.getPrGroup().compareTo(e2.getPrGroup());
							}
							if (e1.getPrGroup().equals("")&& e2.getPrGroup().equals("")) {
								return e1.getPrGroup().compareTo(e2.getPrGroup());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);

	}

	public void createColumnSortingPRCategory() {
		List<PurchaseRequisition> list = getDataprovider().getList();
		columnSort = new ListHandler<PurchaseRequisition>(list);
		columnSort.setComparator(ColumnPRCategory,
				new Comparator<PurchaseRequisition>() {
					@Override
					public int compare(PurchaseRequisition e1,
							PurchaseRequisition e2) {
						if (e1 != null && e2 != null) {
							if (e1.getPrCategory() != null
									&& e2.getPrCategory() != null) {
								return e1.getPrCategory().compareTo(
										e2.getPrCategory());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);

	}

	public void createColumnSortingStatus() {
		List<PurchaseRequisition> list = getDataprovider().getList();
		columnSort = new ListHandler<PurchaseRequisition>(list);
		columnSort.setComparator(ColumnStatus,
				new Comparator<PurchaseRequisition>() {
					@Override
					public int compare(PurchaseRequisition e1,
							PurchaseRequisition e2) {
						if (e1 != null && e2 != null) {
							if (e1.getStatus() != null
									&& e2.getStatus() != null) {
								return e1.getStatus().compareTo(e2.getStatus());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);

	}

	/***********************************************************************************/

	public void createColumnCreationDate() {
		ColumnCreationDate = new TextColumn<PurchaseRequisition>() {

			@Override
			public String getValue(PurchaseRequisition object) {
				if (object.getCreationDate() != null)
					return AppUtility.parseDate(object.getCreationDate());
				else
					return "";
			}
		};
		table.addColumn(ColumnCreationDate, "Creation Date");
		table.setColumnWidth(ColumnCreationDate, 110, Unit.PX);
		ColumnCreationDate.setSortable(true);
	}

	public void createColumnExpectedDelDate() {
		ColumnExptectedDeliveryDate = new TextColumn<PurchaseRequisition>() {

			@Override
			public String getValue(PurchaseRequisition object) {
				if (object.getExpectedDeliveryDate() != null)
					return AppUtility.parseDate(object.getExpectedDeliveryDate());
				else
					return "";
			}
		};
		table.addColumn(ColumnExptectedDeliveryDate, "Exp.Del Date");
		table.setColumnWidth(ColumnExptectedDeliveryDate, 110, Unit.PX);
		ColumnExptectedDeliveryDate.setSortable(true);
	}

	public void createColumnRefOrderDate() {
		ColumnRefOrderDate = new TextColumn<PurchaseRequisition>() {

			@Override
			public String getValue(PurchaseRequisition object) {
				if (object.getRefOrderDate() != null)
					return AppUtility.parseDate(object.getRefOrderDate());
				else
					return "";
			}
		};
		table.addColumn(ColumnRefOrderDate, "Ref. Ord. Date");
		table.setColumnWidth(ColumnRefOrderDate, 110, Unit.PX);
		ColumnRefOrderDate.setSortable(true);
	}

	public void createColumnResponseDate() {
		ColumnResponseDate = new TextColumn<PurchaseRequisition>() {

			@Override
			public String getValue(PurchaseRequisition object) {
				if (object.getResponseDate() != null)
					return AppUtility.parseDate(object.getResponseDate());
				else
					return "";
			}
		};
		table.addColumn(ColumnResponseDate, "Res. Date");
		table.setColumnWidth(ColumnResponseDate, 100, Unit.PX);
		ColumnResponseDate.setSortable(true);
	}

	public void createColumnProject() {
		ColumnProject = new TextColumn<PurchaseRequisition>() {

			@Override
			public String getValue(PurchaseRequisition object) {
				return object.getProject();
			}
		};
		table.addColumn(ColumnProject, "Project");
		table.setColumnWidth(ColumnProject, 110, Unit.PX);
		ColumnProject.setSortable(true);
	}

	public void createColumnRefOrderNo() {
		ColumnReforderNo = new TextColumn<PurchaseRequisition>() {
			@Override
			public String getValue(PurchaseRequisition object) {
				return object.getRefOrderNO() + "";
			}
		};
		table.addColumn(ColumnReforderNo, "RefOdrNr");
		table.setColumnWidth(ColumnReforderNo, 100, Unit.PX);
		ColumnReforderNo.setSortable(true);
	}

	public void createColumnPurchaseEngg() {
		ColumnPurchaseEngg = new TextColumn<PurchaseRequisition>() {

			@Override
			public String getValue(PurchaseRequisition object) {
				// TODO Auto-generated method stub
				return object.getEmployee();
			}
		};
		table.addColumn(ColumnPurchaseEngg, "Pur Eng.");
		table.setColumnWidth(ColumnPurchaseEngg, 100, Unit.PX);
		ColumnPurchaseEngg.setSortable(true);
	}

	public void createColumnBranch() {
		ColumnBranch = new TextColumn<PurchaseRequisition>() {

			@Override
			public String getValue(PurchaseRequisition object) {
				return object.getBranch();
			}
		};
		table.addColumn(ColumnBranch, "Branch");
		table.setColumnWidth(ColumnBranch, 100, Unit.PX);
		ColumnBranch.setSortable(true);
	}

	public void createColumnPRType() {
		ColumnPRType = new TextColumn<PurchaseRequisition>() {

			@Override
			public String getValue(PurchaseRequisition object) {
				return object.getPrType();
			}
		};
		table.addColumn(ColumnPRType, "Type");
		table.setColumnWidth(ColumnPRType, 100, Unit.PX);
		ColumnPRType.setSortable(true);
	}

	public void createColumnPRGroup() {
		ColumnPRGrpup = new TextColumn<PurchaseRequisition>() {

			@Override
			public String getValue(PurchaseRequisition object) {
				return object.getPrGroup();
			}
		};
		table.addColumn(ColumnPRGrpup, "Group");
		table.setColumnWidth(ColumnPRGrpup, 100, Unit.PX);
		ColumnPRGrpup.setSortable(true);
	}

	public void createColumnPRCategory() {
		ColumnPRCategory = new TextColumn<PurchaseRequisition>() {

			@Override
			public String getValue(PurchaseRequisition object) {
				return object.getPrCategory();
			}
		};
		table.addColumn(ColumnPRCategory, "Category");
		table.setColumnWidth(ColumnPRCategory, 100, Unit.PX);
		ColumnPRCategory.setSortable(true);
	}

	public void createColumnStatus() {
		ColumnStatus = new TextColumn<PurchaseRequisition>() {

			@Override
			public String getValue(PurchaseRequisition object) {
				return object.getStatus();
			}
		};
		table.addColumn(ColumnStatus, "Status");
		table.setColumnWidth(ColumnStatus, 110, Unit.PX);
		ColumnStatus.setSortable(true);
	}

	public void createColumnCount() {
		Columncount = new TextColumn<PurchaseRequisition>() {

			@Override
			public String getValue(PurchaseRequisition object) {
				return object.getCount() + "";
			}
		};
		table.addColumn(Columncount, "ID");
		table.setColumnWidth(Columncount, 100, Unit.PX);
		Columncount.setSortable(true);
	}

	public void createColumnPRTitle() {
		ColumnPRTitle = new TextColumn<PurchaseRequisition>() {

			@Override
			public String getValue(PurchaseRequisition object) {
				return object.getPrTitle();
			}
		};
		table.addColumn(ColumnPRTitle, "Title");
		table.setColumnWidth(ColumnPRTitle, 110, Unit.PX);
		ColumnPRTitle.setSortable(true);
	}

	public void createColumnPRApproverName() {
		ColumnAprroverName = new TextColumn<PurchaseRequisition>() {

			@Override
			public String getValue(PurchaseRequisition object) {
				return object.getApproverName();
			}
		};
		table.addColumn(ColumnAprroverName, "Approver");
		table.setColumnWidth(ColumnAprroverName, 110, Unit.PX);
		ColumnAprroverName.setSortable(true);
	}

	
	
	
	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub

	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub

	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub

	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		table.setAlwaysShowScrollBars(true);

	}

}
