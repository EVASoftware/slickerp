package com.slicktechnologies.client.views.purchase.purchaserequisition;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.code.p.gwtchismes.client.GWTCTabPanel;
import com.google.gwt.dom.client.Style.FontStyle;
import com.google.gwt.dom.client.Style.FontWeight;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.personlayer.Employee;

public class AssignPrToPoPurchaseEnggPopup extends AbsolutePanel {

	ObjectListBox<Employee> olbAssignedTo1;
	ObjectListBox<Employee> olbAssignedTo2;
	Button btnOk,btnCancel;
	
	public void initializeWidget(){
		olbAssignedTo1=new ObjectListBox<Employee>();
		olbAssignedTo1.makeEmployeeLive(AppConstants.PURCHASEMODULE, AppConstants.PURCHASEREQUISION, "Assign To 1");
//		AppUtility.makeSalesPersonListBoxLive(olbAssignedTo1);
		
//		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Employee", "LoadEmployeeRoleWise")){
//			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Employee", "PurchaseEngineerPRPO")){
//				olbAssignedTo1.setEmployeeToDropDown("PO Purchase Engineer");
//			}else{
//				olbAssignedTo1.setEmployeeToDropDown("Purchase Engineer");
//			}
//		}else{
//			AppUtility.makeSalesPersonListBoxLive(olbAssignedTo1);
//		}
		
		olbAssignedTo2=new ObjectListBox<Employee>();
		olbAssignedTo2.makeEmployeeLive(AppConstants.PURCHASEMODULE, AppConstants.PURCHASEREQUISION, "Assign To 2");
//		AppUtility.makeSalesPersonListBoxLive(olbAssignedTo2);
		
//		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Employee", "LoadEmployeeRoleWise")){
//			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Employee", "PurchaseEngineerPRPO")){
//				olbAssignedTo2.setEmployeeToDropDown("PO Purchase Engineer");
//			}else{
//				olbAssignedTo2.setEmployeeToDropDown("Purchase Engineer");
//			}
//		}else{
//			AppUtility.makeSalesPersonListBoxLive(olbAssignedTo2);
//		}
		
		btnOk=new Button("Ok");
		btnOk.getElement().getStyle().setWidth(80, Unit.PX);
		btnCancel=new Button("Cancel");
		btnCancel.getElement().getStyle().setWidth(80, Unit.PX);
	}
	
	public AssignPrToPoPurchaseEnggPopup() {
		initializeWidget();
		
		InlineLabel label=new InlineLabel("Assign PR to PO Purchase Engineer");
		label.getElement().getStyle().setFontSize(15, Unit.PX);
		label.getElement().getStyle().setFontStyle(FontStyle.NORMAL);
		label.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		
		InlineLabel assignTo1Lbl=new InlineLabel("Assign To 1");
		InlineLabel assignTo1Lb2=new InlineLabel("Assign To 2");
		
		add(label,90,10);
		add(assignTo1Lbl,10,50);
		add(olbAssignedTo1,10,85);
		
		add(assignTo1Lb2,230,50);
		add(olbAssignedTo2,230,85);
		
		add(btnOk,110,130);
		add(btnCancel,240,130);
		
		setSize("440px","180px");
		this.getElement().setId("login-form");
		
	}
	
	
	public void clear(){
		olbAssignedTo1.setSelectedIndex(0);
		olbAssignedTo2.setSelectedIndex(0);
	}
	
	public boolean validate(){
		if(olbAssignedTo1.getSelectedIndex()==0&&olbAssignedTo2.getSelectedIndex()==0){
			GWTCAlert alert =new GWTCAlert();
			alert.alert("Please select assignt to !");
			return false;
		}
		return true;
	}

	public ObjectListBox<Employee> getOlbAssignedTo1() {
		return olbAssignedTo1;
	}

	public void setOlbAssignedTo1(ObjectListBox<Employee> olbAssignedTo1) {
		this.olbAssignedTo1 = olbAssignedTo1;
	}

	public ObjectListBox<Employee> getOlbAssignedTo2() {
		return olbAssignedTo2;
	}

	public void setOlbAssignedTo2(ObjectListBox<Employee> olbAssignedTo2) {
		this.olbAssignedTo2 = olbAssignedTo2;
	}

	public Button getBtnOk() {
		return btnOk;
	}

	public void setBtnOk(Button btnOk) {
		this.btnOk = btnOk;
	}

	public Button getBtnCancel() {
		return btnCancel;
	}

	public void setBtnCancel(Button btnCancel) {
		this.btnCancel = btnCancel;
	}
	
	
	
}
