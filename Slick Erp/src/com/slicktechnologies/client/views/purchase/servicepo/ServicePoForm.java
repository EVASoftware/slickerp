package com.slicktechnologies.client.views.purchase.servicepo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.AddressComposite;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.composite.UploadComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.approvalutility.ApprovableFormScreen;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.contract.PaymentTermsTable;
import com.slicktechnologies.client.views.contract.ProductChargesTable;
import com.slicktechnologies.client.views.contract.ProductTaxesTable;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.ProductOtherCharges;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.businessprocesslayer.PriceList;
import com.slicktechnologies.shared.common.businessprocesslayer.PriceListDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessprocesslayer.ServicePo;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class ServicePoForm extends ApprovableFormScreen<ServicePo> implements ChangeHandler, ClickHandler {
	
	PersonInfoComposite vendorComp;
	TextBox tbServicePoId;
	TextBox tbServicePoTitle;
	DateBox dbServicePoDate;
	DateBox dbServiceExpDelDate;
	
	TextBox tbReferenceNum;
	DateBox dbReferenceDate;
	
	
	ObjectListBox<Employee> olbPurchaseEngg;
	ObjectListBox<Branch> olbBranch;
	ObjectListBox<Employee> olbApporverName;
	ObjectListBox<HrProject> olbProject;
	ObjectListBox<Config> olbcPaymentMethods;
	
	ObjectListBox<ConfigCategory> olbServicePoCategory;
	ObjectListBox<Type> olbServicePoType;
	
	
	IntegerBox ibCreditDays;
	IntegerBox ibNoOfDays;
	DoubleBox dbPercent;
	TextBox tbComment;
	PaymentTermsTbl paymentTermsTable;
	Button addPaymentTerms;
	
	ObjectListBox<ConfigCategory> olbPaymentTerms;
	Button addPayTerms;
	CheckBox cbStartOfPeriod;
	NumberFormat nf = NumberFormat.getFormat("0.00");
	
	ObjectListBox<Employee> olbAssignedTo1;
	ObjectListBox<Employee> olbAssignedTo2;
	
	UploadComposite upload;
	AddressComposite deliveryadd;
	CheckBox cbadds;
	
	TextBox tbStatus;
	TextBox tbRemark;
	TextArea tadescription;
	FormField fGroupingSerPoInfo;
	
	ServicePoLineItemTable saleslineitemtable;
	ProductChargesTable chargesTable;
	Button addOtherCharges;
	ProductTaxesTable prodTaxTable;
	ProductInfoComposite prodInfoComposite;
	Button baddproducts;
	
	DoubleBox dototalamt,doamtincltax,donetpayamt;
	
	/**
	 * This object is used to maintain navigation history
	 * Date : 18-10-2016 By Anil
	 * 
	 */
	ServicePo servicePoObj;
	final GenricServiceAsync async = GWT.create(GenricService.class);
	
	public ServicePoForm() {
		super();
		createGui();
		tbStatus.setValue(PurchaseOrder.CREATED);
		tbStatus.setEnabled(false);
		tbRemark.setEnabled(false);
		tbServicePoId.setEnabled(false);
		dbServicePoDate.setEnabled(false);
		
		saleslineitemtable.connectToLocal();
		paymentTermsTable.connectToLocal();
		chargesTable.connectToLocal();
		prodTaxTable.connectToLocal();
	}
	
	
	private void initializeWidget(){
		
		MyQuerry querry = new MyQuerry();
		querry.setQuerryObject(new Vendor());
		Filter filter = new Filter();
		filter.setQuerryString("vendorStatus");
		filter.setBooleanvalue(true);
		querry.getFilters().add(filter);
		vendorComp = new PersonInfoComposite(querry, false);
		
		tbServicePoId=new TextBox();
		tbServicePoTitle=new TextBox();
		dbServicePoDate=new DateBoxWithYearSelector();
		Date date=new Date();
		dbServicePoDate.setValue(date);
		dbServicePoDate.setEnabled(false);
		
		dbServiceExpDelDate=new DateBoxWithYearSelector();
		
		tbReferenceNum=new TextBox();
		dbReferenceDate=new DateBoxWithYearSelector();
		
		olbPurchaseEngg=new ObjectListBox<Employee>();
//		AppUtility.makeSalesPersonListBoxLive(olbPurchaseEngg);
		/**
		 * Date 14-02-2017 By Anil
		 */
		olbPurchaseEngg.makeEmployeeLive(AppConstants.PURCHASEMODULE, AppConstants.SERVICEPO, "Purchase Engineer");
		/**
		 * End
		 */
		
		olbApporverName = new ObjectListBox<Employee>();
		AppUtility.makeApproverListBoxLive(olbApporverName, "Service PO");
		
		olbBranch=new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbBranch);
		
		olbProject = new ObjectListBox<HrProject>();
		HrProject.MakeProjectListBoxLive(olbProject);
		
		olbcPaymentMethods=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbcPaymentMethods,Screen.PAYMENTMETHODS);
		
		olbServicePoCategory = new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(olbServicePoCategory, Screen.SERVICEPOCATEGORY);
		olbServicePoCategory.addChangeHandler(this);
		
		olbServicePoType = new ObjectListBox<Type>();
		AppUtility.makeTypeListBoxLive(olbServicePoType,Screen.SERVICEPOTYPE);
		
		ibCreditDays=new IntegerBox();
		
		ibNoOfDays = new IntegerBox();
		dbPercent = new DoubleBox();
		tbComment = new TextBox();
		addPaymentTerms = new Button("ADD");
		addPaymentTerms.addClickHandler(this);
		paymentTermsTable = new PaymentTermsTbl();
		
		olbAssignedTo1=new ObjectListBox<Employee>();
//		AppUtility.makeSalesPersonListBoxLive(olbAssignedTo1);
		/**
		 * Date 14-02-2017 By Anil
		 */
		olbAssignedTo1.makeEmployeeLive(AppConstants.PURCHASEMODULE, AppConstants.SERVICEPO, "Assign To 1");
		/**
		 * End
		 */
		
		olbAssignedTo2=new ObjectListBox<Employee>();
//		AppUtility.makeSalesPersonListBoxLive(olbAssignedTo2);
		/**
		 * Date 14-02-2017 By Anil
		 */
		olbAssignedTo2.makeEmployeeLive(AppConstants.PURCHASEMODULE, AppConstants.SERVICEPO, "Assign To 2");
		/**
		 * End
		 */
		
		upload = new UploadComposite();
		
		cbadds = new CheckBox();
		cbadds.setValue(false);
		deliveryadd = new AddressComposite(true);
		deliveryadd.setEnable(false);
		
		tbStatus=new TextBox();
		tbRemark=new TextBox();
		tadescription=new TextArea();
		
		olbPaymentTerms = new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(olbPaymentTerms,Screen.ADDPAYMENTTERMS);

		addPayTerms = new Button("ADD");
		addPayTerms.addClickHandler(this);

		cbStartOfPeriod = new CheckBox();
		cbStartOfPeriod.addClickHandler(this);
		cbStartOfPeriod.setValue(false);
		
		saleslineitemtable=new ServicePoLineItemTable();
		prodInfoComposite=AppUtility.initiateServiceProductComposite(new SuperProduct());
		
		chargesTable=new ProductChargesTable();
		addOtherCharges=new Button("+");
		addOtherCharges.addClickHandler(this);
		prodTaxTable=new ProductTaxesTable();
		
		baddproducts=new Button("ADD");
		baddproducts.addClickHandler(this);
		
		dototalamt=new DoubleBox();
		dototalamt.setEnabled(false);
		doamtincltax=new DoubleBox();
		doamtincltax.setEnabled(false);
		donetpayamt=new DoubleBox();
		donetpayamt.setEnabled(false);
	}

	


	@Override
	public void createScreen() {
		initializeWidget();
		
		/**@author Amol  11-1-2019
		   added a "view Bill" button in Service PO Screen
		 */

		this.processlevelBarNames = new String[] {ManageApprovals.SUBMIT,ManageApprovals.APPROVALREQUEST,
				ManageApprovals.CANCELAPPROVALREQUEST,"New",AppConstants.VIEWBILL};
		
		
	
		String mainScreenLabel="Service PO ";
		if(servicePoObj!=null&&servicePoObj.getCount()!=0){
			
			
			mainScreenLabel=servicePoObj.getCount()+" "+"/"+" "+servicePoObj.getStatus()+" "+"/"+" "+AppUtility.parseDate(servicePoObj.getCreationDate());
		}
		//fGroupingSerPoInfo.getHeaderLabel().setText(mainScreenLabel);
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		fGroupingSerPoInfo=fbuilder.setlabel(mainScreenLabel).widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).setKeyField(true).build();
		
//		FormFieldBuilder fbuilder;
//		fbuilder = new FormFieldBuilder();
//		FormField fGroupingSerPoInfo = fbuilder.setlabel("Service PO Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
//		
		fbuilder = new FormFieldBuilder();
		FormField fGroupingVendInfo = fbuilder.setlabel("Classification").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();

		fbuilder = new FormFieldBuilder("", vendorComp);
		FormField fvendorComp = fbuilder.setMandatory(true).setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Service PO Id", tbServicePoId);
		FormField ftbServicePoId = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Service PO Title", tbServicePoTitle);
		FormField ftbServicePoTitle = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Service PO Date", dbServicePoDate);
		FormField fdbServicePoDate = fbuilder.setMandatory(true).setMandatoryMsg("Service PO Date is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Expected Delivery Date", dbServiceExpDelDate);
		FormField fdbServiceExpDelDate = fbuilder.setMandatory(true).setMandatoryMsg("Expected Delivery Date is Mandatory!").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Reference Number", tbReferenceNum);
		FormField ftbReferenceNum = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Reference Date", dbReferenceDate);
		FormField fdbReferenceDate = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Project", olbProject);
		FormField folbProject = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
//		fbuilder = new FormFieldBuilder("* Branch", olbBranch);
//		FormField folbBranch = fbuilder.setMandatory(true).setMandatoryMsg("Branch is Mandatory!").setRowSpan(0).setColSpan(0).build();
		
		FormField folbBranch=null;
		if(AppUtility.isBranchNonMandatory("ServicePo")){
			fbuilder = new FormFieldBuilder("Branch", olbBranch);
			folbBranch = fbuilder.setMandatory(false).setMandatoryMsg("Branch is Mandatory!").setRowSpan(0)
					.setColSpan(0).build();
		}else{
			fbuilder = new FormFieldBuilder("* Branch", olbBranch);
			folbBranch = fbuilder.setMandatory(true).setMandatoryMsg("Branch is Mandatory!").setRowSpan(0)
					.setColSpan(0).build();
		}
		
		
		fbuilder = new FormFieldBuilder("Service PO Category", olbServicePoCategory);
		FormField folbServicePoCategory = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Service PO Type", olbServicePoType);
		FormField folbServicePoType = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Purchase Engineer", olbPurchaseEngg);
		FormField folbPurchaseEngg = fbuilder.setMandatory(true).setMandatoryMsg("Purchase Engineer is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Approver", olbApporverName);
		FormField folbApporverName = fbuilder.setMandatory(true).setMandatoryMsg("Approver is Mandatory!").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Assign To 1", olbAssignedTo1);
		FormField folbAssignedTo1 = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Assign To 2", olbAssignedTo2);
		FormField folbAssignedTo2 = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Payment Method", olbcPaymentMethods);
		FormField folbcPaymentMethods = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Credit Days", ibCreditDays);
		FormField fdbCreditDays = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Status", tbStatus);
		FormField tbStatus = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Remark", tbRemark);
		FormField ftbRemark = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Upload Documents", upload);
		FormField fupload = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fGroupingDescription = fbuilder.setlabel("Description").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Description(Max 500 charcters)", tadescription);
		FormField ftadescription = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fGroupingReference = fbuilder.setlabel("Reference/General").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fGroupingTimestamp = fbuilder.setlabel("Timestamp").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fGroupingAttachment = fbuilder.setlabel("Attachment").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();

		
		fbuilder = new FormFieldBuilder();
		FormField fGroupingPaymentTerms = fbuilder.setlabel("Payment Terms").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();

		fbuilder = new FormFieldBuilder("Days", ibNoOfDays);
		FormField fibNoOfDays = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Percent", dbPercent);
		FormField fdbPercent = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Comment", tbComment);
		FormField ftbComment = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",addPaymentTerms );
		FormField faddPaymentTerms = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Payment Terms", olbPaymentTerms);
		FormField folbPaymentTerms = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Start Of Period", cbStartOfPeriod);
		FormField fcbStartOfPeriod = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("", addPayTerms);
		FormField faddPayTerms = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",paymentTermsTable.getTable() );
		FormField fpaymentTermsTable = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingProducts=fbuilder.setlabel("Product").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",prodInfoComposite);
		FormField fprodInfoComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		fbuilder = new FormFieldBuilder("",baddproducts);
		FormField fbaddproducts= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",saleslineitemtable.getTable());
		FormField fsaleslineitemtable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("",prodTaxTable.getTable());
		FormField fprodTaxTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("Total Amount",dototalamt);
		FormField fpayCompTotalAmt= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("",addOtherCharges);
		FormField faddOtherCharges= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",chargesTable.getTable());
		FormField fchargesTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		fbuilder = new FormFieldBuilder("Net Payable",donetpayamt);
		FormField fpayCompNetPay= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Total Amount",doamtincltax);
		FormField fdoAmtIncludingTax= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fblankgroupthree=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		fbuilder = new FormFieldBuilder();
		FormField fblankgroup=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		fbuilder = new FormFieldBuilder();
		FormField fblankgroupone=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Is Delivery Address Different", cbadds);
		FormField fcbadds = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("", deliveryadd);
		FormField fdeliveryadd = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(5).build();
		
		
//		FormField[][] formfield = {
//				{ fGroupingVendInfo },
//				{ fvendorComp},
//				{ fGroupingSerPoInfo},
//				{ ftbServicePoId, ftbServicePoTitle, fdbServicePoDate, fdbServiceExpDelDate},
//				{ ftbReferenceNum, fdbReferenceDate, folbProject, folbBranch},
//				{ folbServicePoCategory, folbServicePoType, folbPurchaseEngg, folbApporverName},
//				{ folbAssignedTo1, folbAssignedTo2, folbcPaymentMethods, fdbCreditDays},
//				{ tbStatus,ftbRemark,fupload},
//				{ fGroupingDescription},
//				{ ftadescription},
//				{ fGroupingPaymentTerms },
//				{ fibNoOfDays, fdbPercent, ftbComment, faddPaymentTerms },
//				{ folbPaymentTerms,fcbStartOfPeriod,faddPayTerms},
//				{ fpaymentTermsTable},
//				
//				
//				{fgroupingProducts},
//				{fprodInfoComposite,fbaddproducts},
//				{fsaleslineitemtable},
//				{fblankgroupthree,fpayCompTotalAmt},
//				{fblankgroupthree},
//				{fblankgroup,fprodTaxTable},
//				{fblankgroup,faddOtherCharges,fdoAmtIncludingTax},
//				{fblankgroup,fchargesTable},
//				{fblankgroup,fblankgroupone,fpayCompNetPay},
////				{ fcbadds }, { fdeliveryadd },
//				
		
		
		FormField[][] formfield = {
				{ fGroupingSerPoInfo},
				{ fvendorComp},
				{ ftbServicePoTitle, fdbServicePoDate, fdbServiceExpDelDate, folbBranch},
				{folbPurchaseEngg, folbApporverName,ftbRemark},
				//Product
				{fgroupingProducts},
				{fprodInfoComposite,fbaddproducts},
				{fsaleslineitemtable},
				{fblankgroupthree,fpayCompTotalAmt},
				{fblankgroupthree},
				{fblankgroup,fprodTaxTable},
				{fblankgroup,faddOtherCharges,fdoAmtIncludingTax},
				{fblankgroup,fchargesTable},
				{fblankgroup,fblankgroupone,fpayCompNetPay},
				//Payment term
				{ fGroupingPaymentTerms },
				{ folbcPaymentMethods, fdbCreditDays},
				{folbPaymentTerms,fcbStartOfPeriod,faddPayTerms},
				{ fibNoOfDays, fdbPercent, ftbComment, faddPaymentTerms },
				{ fpaymentTermsTable},
				//classification
				{ fGroupingVendInfo },
				{ folbServicePoCategory, folbServicePoType}, 
				//Reference/General
				{ fGroupingReference },  
				{ ftbReferenceNum, fdbReferenceDate, folbProject,folbAssignedTo1 },
				{  folbAssignedTo2 },
				{ ftadescription},
				//Attachment
				{ fGroupingAttachment},
				{ fupload },
				
			
			
				

		};

		this.fields = formfield;
	}


	@Override
	public void updateModel(ServicePo model) {
		
		if(vendorComp.getValue()!=null){
			model.setVinfo(vendorComp.getValue());
		}
		
		if(tbServicePoTitle.getValue()!=null){
			model.setServicePoTitle(tbServicePoTitle.getValue());
		}
		if(dbServicePoDate.getValue()!=null){
			model.setServicePoDate(dbServicePoDate.getValue());
		}
		if(dbServiceExpDelDate.getValue()!=null){
			model.setExpDeliveryDate(dbServiceExpDelDate.getValue());
		}
		if(tbReferenceNum.getValue()!=null){
			model.setRefNum(tbReferenceNum.getValue());
		}
		if(dbReferenceDate.getValue()!=null){
			model.setRefDate(dbReferenceDate.getValue());
		}
		if(olbProject.getValue()!=null){
			model.setProject(olbProject.getValue());
		}
		if(olbBranch.getValue()!=null){
			model.setBranch(olbBranch.getValue());
		}
		if(olbServicePoCategory.getValue()!=null){
			model.setCategory(olbServicePoCategory.getValue());
		}
		if(olbServicePoType.getValue()!=null){
			model.setType(olbServicePoType.getValue());
		}
		
		if(olbPurchaseEngg.getValue()!=null){
			model.setEmployee(olbPurchaseEngg.getValue());
		}
		if(olbApporverName.getValue()!=null){
			model.setApproverName(olbApporverName.getValue());
		}
		if(olbAssignedTo1.getValue()!=null){
			model.setAssignTo1(olbAssignedTo1.getValue());
		}
		if(olbAssignedTo2.getValue()!=null){
			model.setAssignTo2(olbAssignedTo2.getValue());
		}
		if(olbcPaymentMethods.getValue()!=null){
			model.setPaymentMethod(olbcPaymentMethods.getValue());
		}
		if(ibCreditDays.getValue()!=null){
			model.setCreditDays(ibCreditDays.getValue());
		}
		if(tbStatus.getValue()!=null){
			model.setStatus(tbStatus.getValue());
		}
		if(tbRemark.getValue()!=null){
			model.setRemark(tbRemark.getValue());
		}
		if(upload.getValue()!=null){
			model.setUpload(upload.getValue());
		}
		if(tadescription.getValue()!=null){
			model.setDescription(tadescription.getValue());
		}
		if(paymentTermsTable.getValue()!=null){
			model.setPaymentTermsList(paymentTermsTable.getValue());
		}
		if(saleslineitemtable.getValue()!=null){
			model.setItems(saleslineitemtable.getValue());
		}
		if(dototalamt.getValue()!=null){
			model.setTotalAmount1(dototalamt.getValue());
		}
		if(prodTaxTable.getValue()!=null){
			model.setProductTaxes(prodTaxTable.getValue());
		}
		if(doamtincltax.getValue()!=null){
			model.setTotalAmount2(doamtincltax.getValue());
		}
		if(chargesTable.getValue()!=null){
			model.setProductCharges(chargesTable.getValue());
		}
		if(donetpayamt.getValue()!=null){
			model.setNetPayable(donetpayamt.getValue());
		}
		servicePoObj=model;
		presenter.setModel(model);
	}

	@Override
	public void updateView(ServicePo view) {
		
		servicePoObj=view;
		
		tbServicePoId.setValue(view.getCount()+"");
		if(view.getVinfo()!=null){
			vendorComp.setValue(view.getVinfo());
		}
		if(view.getServicePoTitle()!=null){
			tbServicePoTitle.setValue(view.getServicePoTitle());
		}
		if(view.getServicePoDate()!=null){
			dbServicePoDate.setValue(view.getServicePoDate());
		}
		if(view.getExpDeliveryDate()!=null){
			dbServiceExpDelDate.setValue(view.getExpDeliveryDate());
		}
		if(view.getRefNum()!=null){
			tbReferenceNum.setValue(view.getRefNum());
		}
		if(view.getRefDate()!=null){
			dbReferenceDate.setValue(view.getRefDate());
		}
		if(view.getProject()!=null){
			olbProject.setValue(view.getProject());
		}
		if(view.getBranch()!=null){
			olbBranch.setValue(view.getBranch());
		}
		if(view.getCategory()!=null){
			olbServicePoCategory.setValue(view.getCategory());
		}
		if(view.getType()!=null){
			olbServicePoType.setValue(view.getType());
		}
		
		if(view.getEmployee()!=null){
			olbPurchaseEngg.setValue(view.getEmployee());
		}
		if(view.getApproverName()!=null){
			olbApporverName.setValue(view.getApproverName());
		}
		if(view.getAssignTo1()!=null){
			olbAssignedTo1.setValue(view.getAssignTo1());
		}
		if(view.getAssignTo2()!=null){
			olbAssignedTo2.setValue(view.getAssignTo2());
		}
		if(view.getPaymentMethod()!=null){
			olbcPaymentMethods.setValue(view.getPaymentMethod());
		}
		if(view.getCreditDays()!=0){
			ibCreditDays.setValue(view.getCreditDays());
		}
		if(view.getStatus()!=null){
			tbStatus.setValue(view.getStatus());
		}
		if(view.getRemark()!=null){
			tbRemark.setValue(view.getRemark());
		}
		if(view.getUpload()!=null){
			upload.setValue(view.getUpload());
		}
		if(view.getDescription()!=null){
			tadescription.setValue(view.getDescription());
		}
		if(view.getPaymentTermsList()!=null){
			paymentTermsTable.setValue(view.getPaymentTermsList());
		}
		if(view.getItems()!=null){
			saleslineitemtable.setValue(view.getItems());
		}
		if(view.getTotalAmount1()!=0){
			dototalamt.setValue(view.getTotalAmount1());
		}
		if(view.getProductTaxes()!=null){
			prodTaxTable.setValue(view.getProductTaxes());
		}
		if(view.getTotalAmount2()!=0){
			doamtincltax.setValue(view.getTotalAmount2());
		}
		if(view.getProductCharges()!=null){
			chargesTable.setValue(view.getProductCharges());
		}
		if(view.getNetPayable()!=0){
			donetpayamt.setValue(view.getNetPayable());
		}
		/* 
		 * for approval process
		 *  nidhi
		 *  5-07-2017
		 */
		if(presenter != null){
			presenter.setModel(view);
		}
		/*
		 *  end
		 */
	}

	@Override
	public void toggleAppHeaderBarMenu() {
		
		if (AppMemory.getAppMemory().currentState == ScreeenState.NEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Save") || text.equals("Discard")
						|| text.equals("Search")|| text.equals(AppConstants.NAVIGATION)) {
					menus[k].setVisible(true);
				} else
					menus[k].setVisible(false);
			}
		}
		else if (AppMemory.getAppMemory().currentState == ScreeenState.EDIT) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Save") || text.equals("Discard")|| text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true);
				else
					menus[k].setVisible(false);
			}
		}

		else if (AppMemory.getAppMemory().currentState == ScreeenState.VIEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Discard") || text.equals("Edit")|| text.equals("Print") || text.equals("Search")|| text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true);
				else
					menus[k].setVisible(false);
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.SERVICEPO,LoginPresenter.currentModule.trim());
		
	}

	@Override
	public boolean validate() {
		boolean supervalidate=super.validate();
		if(!supervalidate){
			return false;
		}
		if (dbServicePoDate.getValue().after(dbServiceExpDelDate.getValue())) {
			this.showDialogMessage("Expected Delivery Date Should Be Greater Than Creation Date.");
			return false;
		}
		if(saleslineitemtable.getDataprovider().getList().size() == 0)
        {
            showDialogMessage("Please Select at least one Product!");
            return false;
        }
		
		if(paymentTermsTable.getDataprovider().getList().size()==0){
			this.showDialogMessage("Please add payment terms!");
			return false;
		}
        
		boolean payment = validatePaymentTermPercent();
		if (payment == false) {
			this.showDialogMessage("In Payment Terms total of percent column should be equal to 100");
			return false;
		}
		
		
		return true;
	}
	
	public boolean validatePaymentTermPercent() {
		List<PaymentTerms> payTermsLis = this.paymentTermsTable.getDataprovider().getList();
		
		if(payTermsLis.size()!=0){
			double totalPayPercent = 0;
			for (int i = 0; i < payTermsLis.size(); i++) {
				totalPayPercent = totalPayPercent+ payTermsLis.get(i).getPayTermPercent();
			}
			System.out.println("Validation of Payment Percent as 100"+ totalPayPercent);
	
			if (totalPayPercent != 100) {
				return false;
			}
		}else{
			return true;
		}

		return true;
	}
	
	/**@author Amol  11-1-2019
	this method is  added for a"view Bill" button in Service PO Screen
	 */
	public void toggleProcessLevelMenu()
	{
		ServicePo entity=(ServicePo) presenter.getModel();
		
		String status=entity.getStatus();
		
		for(int i=0;i<getProcesslevelBarNames().length;i++)
		{
			InlineLabel label=getProcessLevelBar().btnLabels[i];
			String text=label.getText().trim();
			if(status.equals(Contract.CREATED))
			{    
				if(getManageapproval().isSelfApproval()){
			
					if(text.equals(ManageApprovals.SUBMIT))
						label.setVisible(true);
					if(text.equals(ManageApprovals.APPROVALREQUEST))
						label.setVisible(false);
					if(text.equals(AppConstants.NEW)){
						label.setVisible(true);
					}
				
				}else{
				
					if(text.equals(ManageApprovals.SUBMIT))
						label.setVisible(false);
					if(text.equals(ManageApprovals.APPROVALREQUEST))
						label.setVisible(true);
					if(text.equals(AppConstants.NEW)){
						label.setVisible(true);
					}
					
				}
			}
			else if(status.equals(Contract.APPROVED))
			{
				if(text.equals(AppConstants.VIEWBILL)){
					label.setVisible(true);
				}
				if(text.equals(ManageApprovals.SUBMIT))
					label.setVisible(false);
				if(text.equals(AppConstants.NEW)){
					label.setVisible(true);
				}
				
			}else{
				if(text.equals(ManageApprovals.SUBMIT))
					label.setVisible(false);
			}
			}
		}
	
	public void setMenuAsPerStatus()
	{    
	
		this.toggleProcessLevelMenu();
		
	}



	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		tbServicePoId.setEnabled(false);
		tbStatus.setEnabled(false);
		tbRemark.setEnabled(false);
		dbServicePoDate.setEnabled(false);
		
		saleslineitemtable.setEnable(state);
		paymentTermsTable.setEnable(state);
		prodTaxTable.setEnable(state);
		chargesTable.setEnable(state);
		
		
	}


	@Override
	public void setCount(int count) {
		tbServicePoId.setValue(count+"");
	}
	

	@Override
	public void setToViewState() {
		super.setToViewState();
		
		SuperModel model=new ServicePo();
		model=servicePoObj;
		AppUtility.addDocumentToHistoryTable(AppConstants.PURCHASEMODULE,"Service PO", servicePoObj.getCount(), servicePoObj.getVinfo().getCount(),servicePoObj.getVinfo().getFullName(),servicePoObj.getVinfo().getCellNumber(), false, model, null);
	
		String mainScreenLabel="Service PO ";
		if(servicePoObj!=null&&servicePoObj.getCount()!=0){
			mainScreenLabel=servicePoObj.getCount()+" "+"/"+" "+servicePoObj.getStatus()+" "+"/"+" "+AppUtility.parseDate(servicePoObj.getCreationDate());
		}
		fGroupingSerPoInfo.getHeaderLabel().setText(mainScreenLabel);
		
		toggleProcessLevelMenu();
		
	}
	
	
	@Override
	public void setToEditState() {
		super.setToEditState();
		
		processLevelBar.setVisibleFalse(false);
		
		String mainScreenLabel="Service PO ";
		if(servicePoObj!=null&&servicePoObj.getCount()!=0){
			mainScreenLabel=servicePoObj.getCount()+" "+"/"+" "+servicePoObj.getStatus()+" "+"/"+" "+AppUtility.parseDate(servicePoObj.getCreationDate());
		}
		fGroupingSerPoInfo.getHeaderLabel().setText(mainScreenLabel);
		
	}


	@Override
	public TextBox getstatustextbox() {
		return this.tbStatus;
	}



	@Override
	public void onChange(ChangeEvent event) {
		if(event.getSource().equals(olbServicePoCategory))
		{
			loadServicePoType();
		}
		
	}
	
	
	private void loadServicePoType(){
		if(olbServicePoCategory.getSelectedIndex()!=0){
			ConfigCategory cat=olbServicePoCategory.getSelectedItem();
			if(cat!=null){
				AppUtility.makeLiveTypeDropDown(olbServicePoType, cat.getCategoryName(), cat.getCategoryCode(), cat.getInternalType());
			}
		}
	}


	@Override
	public void onClick(ClickEvent event) {
		System.out.println("CLICK EVENT");
		if(event.getSource().equals(addPaymentTerms)){
			System.out.println("Add PT");
			reactOnAddPaymentTerms();
		}
		if (event.getSource().equals(addPayTerms)) {
			System.out.println("Add PT 1");
			reactOnAddPayTerms();
		}
		if(event.getSource().equals(addOtherCharges)){
			int size=chargesTable.getDataprovider().getList().size();
			if(this.doamtincltax.getValue()!=null){
				ProductOtherCharges prodCharges=new ProductOtherCharges();
				prodCharges.setAssessableAmount(this.getDoamtincltax().getValue());
				prodCharges.setFlagVal(false);
				prodCharges.setIndexCheck(size+1);
				this.chargesTable.getDataprovider().getList().add(prodCharges);
			}
		}
		
		if(event.getSource().equals(baddproducts)){
			if(vendorComp.getId().getValue().equals("")){
				showDialogMessage("Please add vendor information!");
			}
			else{
				if(!prodInfoComposite.getProdCode().getValue().equals("")){
//					this.prodctType(this.prodInfoComposite.getProdCode().getValue().trim(),this.prodInfoComposite.getProdID().getValue().trim());
					checkVendorPriceList();
				}
			}
		}
	}
	
	
	public void prodctType(String pcode,String productId,final double vendPrice)
	{
		final GenricServiceAsync genasync=GWT.create(GenricService.class);
		
		Vector<Filter> filtervec=new Vector<Filter>();
		int prodId=Integer.parseInt(productId);
		
		final MyQuerry querry=new MyQuerry();
		Filter filter=new Filter();
		filter.setQuerryString("productCode");
		filter.setStringValue(pcode.trim());
		filtervec.add(filter);
		
		filter=new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(prodId);
		filtervec.add(filter);
		
		filter=new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);
		
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new SuperProduct());
//		showWaitSymbol();
		Timer timer=new Timer() 
    	{
			@Override
			public void run() {
				genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onFailure(Throwable caught) {
						hideWaitSymbol();
						showDialogMessage("An Unexpected error occurred!");
					}
			
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						if(result.size()==0)
						{
							showDialogMessage("Please check whether product status is active or not!");
						}
						SalesLineItem lis=null;
						
						for(SuperModel model:result)
						{
							SuperProduct superProdEntity = (SuperProduct)model;
							lis=AppUtility.ReactOnAddProductComposite(superProdEntity);
							lis.setQuantity(1.0);
							lis.setPrice(vendPrice);
							
						    getSaleslineitemtable().getDataprovider().getList().add(lis);
						}
					}
				});
				hideWaitSymbol();
			}
	 	  };
          timer.schedule(3000); 
	}
	
	
	public void checkVendorPriceList(){
		MyQuerry querry=new MyQuerry();
		
		Filter filter = new Filter();
		filter.setQuerryString("prodID");
		filter.setIntValue(prodInfoComposite.getIdValue());
		querry.getFilters().add(filter);
		
		Filter filter1 = new Filter();
		filter1.setQuerryString("prodCode");
		filter1.setStringValue(prodInfoComposite.getProdCode().getValue().trim());
		querry.getFilters().add(filter1);
		
		
		querry.setQuerryObject(new PriceList());
		System.out.println("Two");
		showWaitSymbol();
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				hideWaitSymbol();
			}
			@Override
			public void onSuccess(ArrayList<SuperModel> vresult) {
//				
				System.out.println("VEN PRICE LIS SIZE : "+vresult.size());
				if (vresult.size() != 0) {
					double venProdPrice=0;
					boolean venPriceFlag=false;
					PriceList entity=(PriceList) vresult.get(0);
					for(PriceListDetails obj:entity.getPriceInfo()){
						if(obj.getVendorID()==vendorComp.getIdValue()){
							venPriceFlag=true;
							venProdPrice=obj.getProductPrice();
							prodctType(prodInfoComposite.getProdCode().getValue().trim(), prodInfoComposite.getProdID().getValue(), venProdPrice);
							break;
						}
					}
					
					if(!venPriceFlag){
						hideWaitSymbol();
						showDialogMessage("Vendor price list is not defined!");
					}
				}else{
					hideWaitSymbol();
					showDialogMessage("Vendor price list is not defined!");
				}
			}
		});	
	}
	
	private void reactOnAddPayTerms() {

		if (olbPaymentTerms.getSelectedIndex() == 0) {
			showDialogMessage("Please select payment terms");
			return;
		}

		if (olbPaymentTerms.getSelectedIndex() != 0) {

			ConfigCategory conCategory = olbPaymentTerms.getSelectedItem();
			if (conCategory != null) {

				String str = conCategory.getDescription();
				if (str.equals("")) {
					showDialogMessage("Please add Payment Days.");
					return;
				}

				int days = Integer.parseInt(str);
				System.out.println("Payment terms Days::::::::::::::" + days);

				if (saleslineitemtable.getDataprovider().getList().size() == 0) {
					showDialogMessage("Please add Product");
					return;
				}

				int duration = getDurationFromProductTable();
				System.out.println("Duration::::::::: " + duration);

				if (duration == 0) {
					showDialogMessage("Please add duration!");
					return;
				}

				int noOfPayTerms = duration / days;
				System.out.println("No. of payment terms::::::::: "
						+ noOfPayTerms);

				if (noOfPayTerms == 0) {
					noOfPayTerms = 1;
				}

				Float no = (float) noOfPayTerms;
				float percent1 = 100 / no;
				System.out.println("Percent1::::::::: " + percent1);

				Double percent = (double) percent1;
				percent = Double.parseDouble(nf.format(percent));
				System.out.println("Percent2:::::::::  " + percent);

				int day = 0;
				double totalPer = 0;

				if (cbStartOfPeriod.getValue() == true) {
					day = 0;
				}

				if (cbStartOfPeriod.getValue() == false) {
					day = days;
				}

				ArrayList<PaymentTerms> ptermsList = new ArrayList<PaymentTerms>();

				for (int i = 1; i <= noOfPayTerms; i++) {
					totalPer = totalPer + percent;

					if (i == noOfPayTerms) {

						if (totalPer == 100) {
							PaymentTerms pterm = new PaymentTerms();
							pterm.setPayTermDays(day);
							pterm.setPayTermPercent(percent);
							pterm.setPayTermComment(i + " Payment");
							ptermsList.add(pterm);
						} else {
							double diff = 0;
							if (totalPer < 100) {
								diff = 100 - totalPer;
								percent = percent + diff;
								percent = Double.parseDouble(nf.format(percent));

								PaymentTerms pterm = new PaymentTerms();
								pterm.setPayTermDays(day);
								pterm.setPayTermPercent(percent);
								pterm.setPayTermComment(i + " Payment");
								ptermsList.add(pterm);
							} else {
								diff = totalPer - 100;
								percent = percent - diff;
								percent = Double.parseDouble(nf.format(percent));

								PaymentTerms pterm = new PaymentTerms();
								pterm.setPayTermDays(day);
								pterm.setPayTermPercent(percent);
								pterm.setPayTermComment(i + " Payment");
								ptermsList.add(pterm);
							}
						}
					} else {
						PaymentTerms pterm = new PaymentTerms();
						pterm.setPayTermDays(day);
						pterm.setPayTermPercent(percent);
						pterm.setPayTermComment(i + " Payment");
						ptermsList.add(pterm);
					}
					day = day + days;
				}
				paymentTermsTable.getDataprovider().setList(ptermsList);
			}
		}
	}
	
	
	private int getDurationFromProductTable() {
		int maxDur = 0;
		int currDur = 0;
		for (int i = 0; i < saleslineitemtable.getDataprovider().getList().size(); i++) {
			currDur = saleslineitemtable.getDataprovider().getList().get(i).getDuration();
			if (maxDur <= currDur) {
				maxDur = currDur;
			}
		}
		return maxDur;
	}
	
	/**
	 * This method represents the validation of payment terms days
	 * @param retrDays
	 * @return
	 */
	
	int chk=0;
	public boolean validatePaymentTrmDays(int retrDays)
	{
		List<PaymentTerms> payTermsLis=this.paymentTermsTable.getDataprovider().getList();
		for(int i=0;i<payTermsLis.size();i++)
		{
			if(retrDays==payTermsLis.get(i).getPayTermDays())
			{
				return false;
			}
			if(retrDays<payTermsLis.get(i).getPayTermDays())
			{
				showDialogMessage("Days should be defined in increasing order!");
				chk=1;
				return false;
			}
			else{
				chk=0;
			}
			if(retrDays>10000){
				showDialogMessage("Days cannot be greater than 4 digits!");
				return false;
			}
		}
		return true;
	}
	
	public void reactOnAddPaymentTerms()
	{
		System.out.println("In method of payemtn terms");	
		int payTrmDays=0;
		double payTrmPercent=0;
		String payTrmComment="";
		boolean flag=true;
		
		if(tbComment.getValue()==null||tbComment.getValue().equals(""))
		{
			this.showDialogMessage("Comment cannot be empty!");
		}
		
		boolean checkTermDays=this.validatePaymentTrmDays(ibNoOfDays.getValue());
		if(checkTermDays==false)
		{
			if(chk==0){
				showDialogMessage("Days already defined.Please enter unique days!");
			}
			flag=false;
		}
		
		
		if(ibNoOfDays.getValue()!=null&&ibNoOfDays.getValue()>=0){
			payTrmDays=ibNoOfDays.getValue();
		}
		else{
			showDialogMessage("Days cannot be empty!");
			flag=false;
		}
		
		
		if(dbPercent.getValue()!=null&&dbPercent.getValue()>=0)
		{
			if(dbPercent.getValue()>100){
				showDialogMessage("Percent cannot be greater than 100");
				dbPercent.setValue(null);
				flag=false;
			}
			else if(dbPercent.getValue()==0){
				showDialogMessage("Percent should be greater than 0!");
				dbPercent.setValue(null);
			}
			else{
			payTrmPercent=dbPercent.getValue();
			}
		}
		else{
			showDialogMessage("Percent cannot be empty!");
		}
		
		if(tbComment.getValue()!=null){
			payTrmComment=tbComment.getValue();
		}
		
		if(!payTrmComment.equals("")&&payTrmPercent!=0&&flag==true)
		{
			PaymentTerms payTerms=new PaymentTerms();
			payTerms.setPayTermDays(payTrmDays);
			payTerms.setPayTermPercent(payTrmPercent);
			payTerms.setPayTermComment(payTrmComment);
			payTerms.setBranch("");
			paymentTermsTable.getDataprovider().getList().add(payTerms);
			clearPaymentTermsFields();
		}
	}
	public void clearPaymentTermsFields()
	{
		ibNoOfDays.setValue(null);
		dbPercent.setValue(null);
		tbComment.setValue("");
	}

	
	
	
	
/*****************************************Add Product Taxes Method***********************************/
	
	/**
	 * This method is called when a product is added to SalesLineItem table.
	 * Taxes related to corresponding products will be added in ProductTaxesTable.
	 * @throws Exception
	 */
	public void addProdTaxes() throws Exception{
		List<SalesLineItem> salesLineItemLis=this.saleslineitemtable.getDataprovider().getList();
		List<ProductOtherCharges> taxList=this.prodTaxTable.getDataprovider().getList();
		NumberFormat nf=NumberFormat.getFormat("#.00");
		boolean areaWiseflg  = LoginPresenter.areaWiseCalRestricFlg;
		for(int i=0;i<salesLineItemLis.size();i++){
			double priceqty=0,taxPrice=0,origPrice=0;
			if((salesLineItemLis.get(i).getPercentageDiscount()==null 
					&& salesLineItemLis.get(i).getPercentageDiscount()==0) 
					&& (salesLineItemLis.get(i).getDiscountAmt()==0) ){
				System.out.println("inside both 0 condition");
				
				taxPrice=this.getSaleslineitemtable().removeAllTaxes(salesLineItemLis.get(i).getPrduct());
				origPrice=(salesLineItemLis.get(i).getPrice()-taxPrice);
				if(!salesLineItemLis.get(i).getArea().equalsIgnoreCase("NA") && !areaWiseflg){
						double squareArea = Double.parseDouble(salesLineItemLis.get(i).getArea());
						priceqty=origPrice*squareArea;
						priceqty=Double.parseDouble(nf.format(priceqty));
				}else{
					priceqty=(origPrice-taxPrice);
					priceqty=Double.parseDouble(nf.format(priceqty));
				}
			}else if(((salesLineItemLis.get(i).getPercentageDiscount()!=null)
					&& (salesLineItemLis.get(i).getDiscountAmt()!=0))){
				System.out.println("inside both not null condition");
				taxPrice=this.getSaleslineitemtable().removeAllTaxes(salesLineItemLis.get(i).getPrduct());
				origPrice=(salesLineItemLis.get(i).getPrice()-taxPrice);
//				priceqty=(salesLineItemLis.get(i).getPrice()-taxPrice);
				if(!salesLineItemLis.get(i).getArea().equalsIgnoreCase("NA")  && !areaWiseflg){
					double squareArea = Double.parseDouble(salesLineItemLis.get(i).getArea());
					priceqty=origPrice*squareArea;
					priceqty= priceqty-(priceqty*salesLineItemLis.get(i).getPercentageDiscount()/100);
					priceqty=priceqty-salesLineItemLis.get(i).getDiscountAmt();
					priceqty=Double.parseDouble(nf.format(priceqty));
				}else{
					priceqty=origPrice*salesLineItemLis.get(i).getQty();
					priceqty=priceqty-(priceqty*salesLineItemLis.get(i).getPercentageDiscount()/100);
					priceqty=priceqty-salesLineItemLis.get(i).getDiscountAmt();
					priceqty=Double.parseDouble(nf.format(priceqty));
					
				}
			}else{
				System.out.println("inside oneof the null condition");
				taxPrice=this.getSaleslineitemtable().removeAllTaxes(salesLineItemLis.get(i).getPrduct());
				origPrice=(salesLineItemLis.get(i).getPrice()-taxPrice);
//				priceqty=(salesLineItemLis.get(i).getPrice()-taxPrice);
				if(salesLineItemLis.get(i).getPercentageDiscount()!=null   && !areaWiseflg){
					if(!salesLineItemLis.get(i).getArea().equalsIgnoreCase("NA")   && !areaWiseflg){
						Double squareArea = Double.parseDouble(salesLineItemLis.get(i).getArea());
						priceqty = origPrice*squareArea;
						priceqty=priceqty-(priceqty*salesLineItemLis.get(i).getPercentageDiscount()/100);
					}else{
						priceqty=origPrice*salesLineItemLis.get(i).getQty();
						priceqty=priceqty-(priceqty*salesLineItemLis.get(i).getPercentageDiscount()/100);
					}
					
				}else {
					if(!salesLineItemLis.get(i).getArea().equals("NA")   && !areaWiseflg){
						Double squareArea = Double.parseDouble(salesLineItemLis.get(i).getArea());
						priceqty = origPrice*squareArea;
						priceqty=priceqty-salesLineItemLis.get(i).getDiscountAmt();
					}else{
						priceqty=origPrice*salesLineItemLis.get(i).getQty();
						priceqty=priceqty-salesLineItemLis.get(i).getDiscountAmt();
					}
				}
				priceqty=Double.parseDouble(nf.format(priceqty));
				System.out.println("Price ====== "+priceqty);
			}
			//****************changes ends here *********************************
			
			if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
				System.out.println("hi vijay vat=="+salesLineItemLis.get(i).getVatTax().getTaxPrintName());
				if(salesLineItemLis.get(i).getVatTax().getTaxPrintName()!=null 
						&& !salesLineItemLis.get(i).getVatTax().getTaxPrintName().equals("")){
					System.out.println("Inside vat GST");
					ProductOtherCharges pocentity=new ProductOtherCharges();
					pocentity.setChargeName(salesLineItemLis.get(i).getVatTax().getTaxPrintName());
					pocentity.setChargePercent(salesLineItemLis.get(i).getVatTax().getPercentage());
					pocentity.setIndexCheck(i+1);
					int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getVatTax().getPercentage(),salesLineItemLis.get(i).getVatTax().getTaxPrintName());
					
					if(indexValue!=-1){
						System.out.println("Index As Not -1");
						pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
						this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
					}
					if(indexValue==-1){
						pocentity.setAssessableAmount(priceqty);
					}
					this.prodTaxTable.getDataprovider().getList().add(pocentity);
				}else{
					System.out.println("Vattx");
					ProductOtherCharges pocentity=new ProductOtherCharges();
					pocentity.setChargeName("VAT");
					pocentity.setChargePercent(salesLineItemLis.get(i).getVatTax().getPercentage());
					pocentity.setIndexCheck(i+1);
					int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getVatTax().getPercentage(),"VAT");
					
					if(indexValue!=-1){
						System.out.println("Index As Not -1");
						pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
						this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
					}
					if(indexValue==-1){
						pocentity.setAssessableAmount(priceqty);
					}
					this.prodTaxTable.getDataprovider().getList().add(pocentity);
				}
			}
			
			if(salesLineItemLis.get(i).getServiceTax().getPercentage()!=0){
				System.out.println(" HI Vijay Service =="+salesLineItemLis.get(i).getServiceTax().getTaxPrintName());
				if(salesLineItemLis.get(i).getServiceTax().getTaxPrintName()!=null 
						&& !salesLineItemLis.get(i).getServiceTax().getTaxPrintName().equals("")){
					System.out.println("Inside service GST");
					ProductOtherCharges pocentity=new ProductOtherCharges();
					pocentity.setChargeName(salesLineItemLis.get(i).getServiceTax().getTaxPrintName());
					pocentity.setChargePercent(salesLineItemLis.get(i).getServiceTax().getPercentage());
					pocentity.setIndexCheck(i+1);
					int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getServiceTax().getPercentage(),salesLineItemLis.get(i).getServiceTax().getTaxPrintName());
					if(indexValue!=-1){
						System.out.println("Index As Not -1");
						pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
						this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
					}
					if(indexValue==-1){
						pocentity.setAssessableAmount(priceqty);
					}
					this.prodTaxTable.getDataprovider().getList().add(pocentity);
				}else{
					System.out.println("ServiceTx");
					ProductOtherCharges pocentity=new ProductOtherCharges();
					pocentity.setChargeName("Service");
					pocentity.setChargePercent(salesLineItemLis.get(i).getServiceTax().getPercentage());
					pocentity.setIndexCheck(i+1);
					int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getServiceTax().getPercentage(),"Service");
					if(indexValue!=-1){
						System.out.println("Index As Not -1");
						double assessValue=0;
						if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
							assessValue=priceqty+(priceqty*salesLineItemLis.get(i).getVatTax().getPercentage()/100);
							pocentity.setAssessableAmount(assessValue+taxList.get(indexValue).getAssessableAmount());
							this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
						}
						if(salesLineItemLis.get(i).getVatTax().getPercentage()==0){
							pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
							this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
						}
					}
					if(indexValue==-1){
						System.out.println("Index As -1");
						double assessVal=0;
						if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
							assessVal=priceqty+(priceqty*salesLineItemLis.get(i).getVatTax().getPercentage()/100);
							pocentity.setAssessableAmount(assessVal);
						}
						if(salesLineItemLis.get(i).getVatTax().getPercentage()==0){
							pocentity.setAssessableAmount(priceqty);
						}
						
					}
					this.prodTaxTable.getDataprovider().getList().add(pocentity);
					System.out.println("DONE ....");
				}
			}
		}
	}

	/*********************************Check Tax Percent*****************************************/
	/**
	 * This method returns 
	 * @param taxValue
	 * @param taxName
	 * @return
	 */
	public int checkTaxPercent(double taxValue,String taxName){
		List<ProductOtherCharges> taxesList=this.prodTaxTable.getDataprovider().getList();
		for(int i=0;i<taxesList.size();i++){
			double listval=taxesList.get(i).getChargePercent();
			int taxval=(int)listval;
			
			if(taxName.equals("Service")){
				if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("Service Tax")){
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("Service Tax")){
					return i;
				}
			}
			if(taxName.equals("VAT")){
				if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("VAT")){
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("VAT")){
					return i;
				}
			}
			if(taxName.equals("CGST")){
				if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("CGST")){
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("CGST")){
					return i;
				}
			}
			if(taxName.equals("SGST")){
				if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("SGST")){
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("SGST")){
					return i;
				}
			}
			if(taxName.equals("IGST")){
				if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("IGST")){
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("IGST")){
					return i;
				}
			}
			
		}
		return -1;
	}
	
	/*************************Update Charges Table****************************************/
	/**
	 * This method updates the charges table.
	 * The table is updated when any row value is changed through field updater in table i.e
	 * row change event is fired
	 */
	public void updateChargesTable(){
		List<ProductOtherCharges> prodOtrLis=this.chargesTable.getDataprovider().getList();
		ArrayList<ProductOtherCharges> prodOtrArr=new ArrayList<ProductOtherCharges>();
		prodOtrArr.addAll(prodOtrLis);
		
		this.chargesTable.connectToLocal();
		for(int i=0;i<prodOtrArr.size();i++){
			ProductOtherCharges prodChargesEnt=new ProductOtherCharges();
			prodChargesEnt.setChargeName(prodOtrArr.get(i).getChargeName());
			prodChargesEnt.setChargePercent(prodOtrArr.get(i).getChargePercent());
			prodChargesEnt.setChargeAbsValue(prodOtrArr.get(i).getChargeAbsValue());
			//prodChargesEnt.setAssessableAmount(prodOtrArr.get(i).getAssessableAmount());
			prodChargesEnt.setFlagVal(prodOtrArr.get(i).getFlagVal());
			prodChargesEnt.setIndexCheck(prodOtrArr.get(i).getIndexCheck());
			if(prodOtrArr.get(i).getFlagVal()==false){
				prodChargesEnt.setAssessableAmount(this.getDoamtincltax().getValue());
			}
			if(prodOtrArr.get(i).getFlagVal()==true){
				double assesableVal=this.retrieveSurchargeAssessable(prodOtrArr.get(i).getIndexCheck());
				prodChargesEnt.setAssessableAmount(assesableVal);
			}
			
			this.chargesTable.getDataprovider().getList().add(prodChargesEnt);
		}
	}
	
	public double retrieveSurchargeAssessable(int indexValue)
	{
		List<ProductOtherCharges> otrChrgLis=this.chargesTable.getDataprovider().getList();
		ArrayList<ProductOtherCharges> otrChrgArr=new ArrayList<ProductOtherCharges>();
		otrChrgArr.addAll(otrChrgLis);
	
		double getAssessVal=0;
		for(int i=0;i<otrChrgArr.size();i++)
		{
			if(otrChrgArr.get(i).getFlagVal()==false&&otrChrgArr.get(i).getIndexCheck()==indexValue)
			{
				if(otrChrgArr.get(i).getChargePercent()!=0){
					getAssessVal=otrChrgArr.get(i).getAssessableAmount()*otrChrgArr.get(i).getChargePercent()/100;
				}
				if(otrChrgArr.get(i).getChargeAbsValue()!=0){
					getAssessVal=otrChrgArr.get(i).getChargeAbsValue();
				}
			}
		}
		return getAssessVal;
	}

	
	/*********************************** Getter And Setter ***********************************/
	
	public PersonInfoComposite getVendorComp() {
		return vendorComp;
	}


	public void setVendorComp(PersonInfoComposite vendorComp) {
		this.vendorComp = vendorComp;
	}


	public TextBox getTbServicePoId() {
		return tbServicePoId;
	}


	public void setTbServicePoId(TextBox tbServicePoId) {
		this.tbServicePoId = tbServicePoId;
	}


	public TextBox getTbServicePoTitle() {
		return tbServicePoTitle;
	}


	public void setTbServicePoTitle(TextBox tbServicePoTitle) {
		this.tbServicePoTitle = tbServicePoTitle;
	}


	public DateBox getDbServicePoDate() {
		return dbServicePoDate;
	}


	public void setDbServicePoDate(DateBox dbServicePoDate) {
		this.dbServicePoDate = dbServicePoDate;
	}


	public DateBox getDbServiceExpDelDate() {
		return dbServiceExpDelDate;
	}


	public void setDbServiceExpDelDate(DateBox dbServiceExpDelDate) {
		this.dbServiceExpDelDate = dbServiceExpDelDate;
	}


	public TextBox getTbReferenceNum() {
		return tbReferenceNum;
	}


	public void setTbReferenceNum(TextBox tbReferenceNum) {
		this.tbReferenceNum = tbReferenceNum;
	}


	public DateBox getDbReferenceDate() {
		return dbReferenceDate;
	}


	public void setDbReferenceDate(DateBox dbReferenceDate) {
		this.dbReferenceDate = dbReferenceDate;
	}


	public ObjectListBox<Employee> getOlbPurchaseEngg() {
		return olbPurchaseEngg;
	}


	public void setOlbPurchaseEngg(ObjectListBox<Employee> olbPurchaseEngg) {
		this.olbPurchaseEngg = olbPurchaseEngg;
	}


	public ObjectListBox<Branch> getOlbBranch() {
		return olbBranch;
	}


	public void setOlbBranch(ObjectListBox<Branch> olbBranch) {
		this.olbBranch = olbBranch;
	}


	public ObjectListBox<Employee> getOlbApporverName() {
		return olbApporverName;
	}


	public void setOlbApporverName(ObjectListBox<Employee> olbApporverName) {
		this.olbApporverName = olbApporverName;
	}


	public ObjectListBox<HrProject> getOlbProject() {
		return olbProject;
	}


	public void setOlbProject(ObjectListBox<HrProject> olbProject) {
		this.olbProject = olbProject;
	}


	public ObjectListBox<Config> getOlbcPaymentMethods() {
		return olbcPaymentMethods;
	}


	public void setOlbcPaymentMethods(ObjectListBox<Config> olbcPaymentMethods) {
		this.olbcPaymentMethods = olbcPaymentMethods;
	}


	public ObjectListBox<ConfigCategory> getOlbServicePoCategory() {
		return olbServicePoCategory;
	}


	public void setOlbServicePoCategory(
			ObjectListBox<ConfigCategory> olbServicePoCategory) {
		this.olbServicePoCategory = olbServicePoCategory;
	}


	public ObjectListBox<Type> getOlbServicePoType() {
		return olbServicePoType;
	}


	public void setOlbServicePoType(ObjectListBox<Type> olbServicePoType) {
		this.olbServicePoType = olbServicePoType;
	}


	public IntegerBox getIbCreditDays() {
		return ibCreditDays;
	}


	public void setIbCreditDays(IntegerBox ibCreditDays) {
		this.ibCreditDays = ibCreditDays;
	}


	public IntegerBox getIbNoOfDays() {
		return ibNoOfDays;
	}


	public void setIbNoOfDays(IntegerBox ibNoOfDays) {
		this.ibNoOfDays = ibNoOfDays;
	}


	public DoubleBox getDbPercent() {
		return dbPercent;
	}


	public void setDbPercent(DoubleBox dbPercent) {
		this.dbPercent = dbPercent;
	}


	public TextBox getTbComment() {
		return tbComment;
	}


	public void setTbComment(TextBox tbComment) {
		this.tbComment = tbComment;
	}


	public PaymentTermsTbl getPaymentTermsTable() {
		return paymentTermsTable;
	}


	public void setPaymentTermsTable(PaymentTermsTbl paymentTermsTable) {
		this.paymentTermsTable = paymentTermsTable;
	}


	public Button getAddPaymentTerms() {
		return addPaymentTerms;
	}


	public void setAddPaymentTerms(Button addPaymentTerms) {
		this.addPaymentTerms = addPaymentTerms;
	}


	public ObjectListBox<ConfigCategory> getOlbPaymentTerms() {
		return olbPaymentTerms;
	}


	public void setOlbPaymentTerms(ObjectListBox<ConfigCategory> olbPaymentTerms) {
		this.olbPaymentTerms = olbPaymentTerms;
	}


	public Button getAddPayTerms() {
		return addPayTerms;
	}


	public void setAddPayTerms(Button addPayTerms) {
		this.addPayTerms = addPayTerms;
	}


	public CheckBox getCbStartOfPeriod() {
		return cbStartOfPeriod;
	}


	public void setCbStartOfPeriod(CheckBox cbStartOfPeriod) {
		this.cbStartOfPeriod = cbStartOfPeriod;
	}


	public NumberFormat getNf() {
		return nf;
	}


	public void setNf(NumberFormat nf) {
		this.nf = nf;
	}


	public ObjectListBox<Employee> getOlbAssignedTo1() {
		return olbAssignedTo1;
	}


	public void setOlbAssignedTo1(ObjectListBox<Employee> olbAssignedTo1) {
		this.olbAssignedTo1 = olbAssignedTo1;
	}


	public ObjectListBox<Employee> getOlbAssignedTo2() {
		return olbAssignedTo2;
	}


	public void setOlbAssignedTo2(ObjectListBox<Employee> olbAssignedTo2) {
		this.olbAssignedTo2 = olbAssignedTo2;
	}


	public UploadComposite getUpload() {
		return upload;
	}


	public void setUpload(UploadComposite upload) {
		this.upload = upload;
	}


	public AddressComposite getDeliveryadd() {
		return deliveryadd;
	}


	public void setDeliveryadd(AddressComposite deliveryadd) {
		this.deliveryadd = deliveryadd;
	}


	public CheckBox getCbadds() {
		return cbadds;
	}


	public void setCbadds(CheckBox cbadds) {
		this.cbadds = cbadds;
	}


	public TextBox getTbStatus() {
		return tbStatus;
	}


	public void setTbStatus(TextBox tbStatus) {
		this.tbStatus = tbStatus;
	}


	public TextBox getTbRemark() {
		return tbRemark;
	}


	public void setTbRemark(TextBox tbRemark) {
		this.tbRemark = tbRemark;
	}


	public TextArea getTadescription() {
		return tadescription;
	}


	public void setTadescription(TextArea tadescription) {
		this.tadescription = tadescription;
	}


	public ServicePoLineItemTable getSaleslineitemtable() {
		return saleslineitemtable;
	}


	public void setSaleslineitemtable(ServicePoLineItemTable saleslineitemtable) {
		this.saleslineitemtable = saleslineitemtable;
	}


	public ProductChargesTable getChargesTable() {
		return chargesTable;
	}


	public void setChargesTable(ProductChargesTable chargesTable) {
		this.chargesTable = chargesTable;
	}


	public Button getAddOtherCharges() {
		return addOtherCharges;
	}


	public void setAddOtherCharges(Button addOtherCharges) {
		this.addOtherCharges = addOtherCharges;
	}


	public ProductTaxesTable getProdTaxTable() {
		return prodTaxTable;
	}


	public void setProdTaxTable(ProductTaxesTable prodTaxTable) {
		this.prodTaxTable = prodTaxTable;
	}


	public ProductInfoComposite getProdInfoComposite() {
		return prodInfoComposite;
	}


	public void setProdInfoComposite(ProductInfoComposite prodInfoComposite) {
		this.prodInfoComposite = prodInfoComposite;
	}


	public Button getBaddproducts() {
		return baddproducts;
	}


	public void setBaddproducts(Button baddproducts) {
		this.baddproducts = baddproducts;
	}


	public DoubleBox getDototalamt() {
		return dototalamt;
	}


	public void setDototalamt(DoubleBox dototalamt) {
		this.dototalamt = dototalamt;
	}


	public DoubleBox getDoamtincltax() {
		return doamtincltax;
	}


	public void setDoamtincltax(DoubleBox doamtincltax) {
		this.doamtincltax = doamtincltax;
	}


	public DoubleBox getDonetpayamt() {
		return donetpayamt;
	}


	public void setDonetpayamt(DoubleBox donetpayamt) {
		this.donetpayamt = donetpayamt;
	}


	public int getChk() {
		return chk;
	}


	public void setChk(int chk) {
		this.chk = chk;
	}
	
	@Override
	public void refreshTableData() {
		// TODO Auto-generated method stub
		super.refreshTableData();
		saleslineitemtable.getTable().redraw();
		paymentTermsTable.getTable().redraw();
		chargesTable.getTable().redraw();
		prodTaxTable.getTable().redraw();
	}
	
}
