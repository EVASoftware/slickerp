package com.slicktechnologies.client.views.purchase.servicepo;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.google.gwt.view.client.RowCountChangeEvent.Handler;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.client.approvalutility.ApprovableFormScreenPresenter;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.EmailService;
import com.slicktechnologies.client.services.EmailServiceAsync;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.BillingDetailsForm;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.BillingDetailsPresenter;
import com.slicktechnologies.client.views.paymentinfo.billinglist.BillingListPresenter;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessprocesslayer.ServicePo;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;

public class ServicePoPresenter extends ApprovableFormScreenPresenter<ServicePo> implements Handler {

	ServicePoForm form;
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	EmailServiceAsync emailService=GWT.create(EmailService.class);
	
	public ServicePoPresenter(FormScreen<ServicePo> view, ServicePo model) {
		super(view, model);
		form=(ServicePoForm) view;
		form.setPresenter(this);
		form.getSaleslineitemtable().getTable().addRowCountChangeHandler(this);
		form.getChargesTable().getTable().addRowCountChangeHandler(this);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.SERVICEPO,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}
	
	
	public static ServicePoForm initialize()
	{
		ServicePoForm form=new  ServicePoForm();
		
		ServicePoPresenterTableProxy gentable=new ServicePoPresenterTableProxy();
		gentable.setView(form);
		gentable.applySelectionModle();
		
		ServicePoPresenterSearchProxy.staticSuperTable=gentable;
		ServicePoPresenterSearchProxy searchpopup=new ServicePoPresenterSearchProxy();
		form.setSearchpopupscreen(searchpopup);
		
		ServicePoPresenter  presenter=new ServicePoPresenter(form,new ServicePo());
		/**12-1-2019 added by amol 
		 This line was added to update the name of screen loaded from any other screen or by clicking on view order button in billing details scrren
		 **/
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Purchase/Service PO",Screen.SERVICEPO);
		AppMemory.getAppMemory().stickPnel(form);
		return form;
	}
	
	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		super.reactToProcessBarEvents(e);
		InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();

		if(text.equals(AppConstants.NEW)){
			initialize();
			form.setToNewState();
		}
		if (text.equals("Email")) {
			reactOnEmail();
		}
		/**@author Amol  11-1-2019
		   added a "view Bill button in Service PO Screen"
		 */
		if (text.equals(AppConstants.VIEWBILL)) {
			viewBillingDocuments();
		}
	}
	
	


	private void viewBillingDocuments() {

		final MyQuerry querry=new MyQuerry();
		Vector<Filter> temp=new Vector<Filter>();
		Filter filter=null;
		
		filter=new Filter();
		filter.setQuerryString("contractCount");
		filter.setIntValue(model.getCount());
		temp.add(filter);
		
		filter=new Filter();
		filter.setStringValue(AppConstants.ACCOUNTTYPEAP);
		filter.setQuerryString("accountType");
		temp.add(filter);
//		
		filter=new Filter();
		filter.setStringValue(AppConstants.ORDERTYPESERVICEPO);
		filter.setQuerryString("typeOfOrder");
		temp.add(filter);
		
		querry.setFilters(temp);
		querry.setQuerryObject(new BillingDocument());
		form.showWaitSymbol();
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				form.hideWaitSymbol();
				if(result.size()==0){
					form.showDialogMessage("No billing document found.");
					return;
				}
				if(result.size()==1){
					final BillingDocument billDocument=(BillingDocument) result.get(0);
					final BillingDetailsForm form=BillingDetailsPresenter.initalize();
					Timer timer=new Timer() {
						@Override
						public void run() {
							form.updateView(billDocument);
							form.setToViewState();
						}
					};
					timer.schedule(1000);
				}else{
					BillingListPresenter.initalize(querry);
				}
				
			}
		});
		
	
		
	}


	@Override
	public void reactOnPrint() {
		final String url = GWT.getModuleBaseURL()+ "servicePoPdf" + "?Id="+ model.getId();
		Window.open(url, "test", "enabled");
		
	}
	
	@Override
	public void reactOnEmail() {
		System.out.println("REACT ON EMAIL");
		boolean conf = Window.confirm("Do you really want to send email?");
		if (conf == true) {
			emailService.initiateServicePoEmail((ServicePo) model,new AsyncCallback<Void>() {
				@Override
				public void onSuccess(Void result) {
					Window.alert("Email Sent Sucessfully !");
				}

				@Override
				public void onFailure(Throwable caught) {
					Window.alert("Resource Quota Ended");
					caught.printStackTrace();
				}
			});
		}
		
	}
	@Override
	public void reactOnDownload() {
		ArrayList<ServicePo> custarray=new ArrayList<ServicePo>();
		List<ServicePo> list=(List<ServicePo>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
		custarray.addAll(list);
		csvservice.setServicePoList(custarray, new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);
			}
			@Override
			public void onSuccess(Void result) {
				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "csvservlet"+"?type="+112;
				Window.open(url, "test", "enabled");
			}
		});
	}


	@Override
	protected void makeNewModel() {
		model=new ServicePo();
	}


	@Override
	public void onRowCountChange(RowCountChangeEvent event) {
		NumberFormat nf=NumberFormat.getFormat("0.00");
		if(event.getSource().equals(form.getSaleslineitemtable().getTable())){
			this.reactOnProductsChange();
		}
		
		if(event.getSource().equals(form.getChargesTable().getTable())){
			if(form.getDoamtincltax().getValue()!=null){
				double newnetpay=form.getDoamtincltax().getValue()+form.getChargesTable().calculateNetPayable();
				newnetpay=Math.round(newnetpay);
				int netPayAmt=(int) newnetpay;
				newnetpay=netPayAmt;
				form.getDonetpayamt().setValue(Double.parseDouble(nf.format(newnetpay)));
			}
		}
		
	}
	
	
	private void reactOnProductsChange()
	{
		form.showWaitSymbol();
//		Timer timer=new Timer() 
//	     {
//			@Override
//			public void run() 
//			{
			 NumberFormat nf=NumberFormat.getFormat("0.00");
			 double totalExcludingTax=form.getSaleslineitemtable().calculateTotalExcludingTax();
			    totalExcludingTax=Double.parseDouble(nf.format(totalExcludingTax));
			    form.getDototalamt().setValue(totalExcludingTax);
			    
				boolean chkSize=form.getSaleslineitemtable().removeChargesOnDelete();
				if(chkSize==false){
					form.getChargesTable().connectToLocal();
				}
				
				try {
					form.prodTaxTable.connectToLocal();
					form.addProdTaxes();
					double totalIncludingTax=form.getDototalamt().getValue()+form.getProdTaxTable().calculateTotalTaxes();
					form.getDoamtincltax().setValue(Double.parseDouble(nf.format(totalIncludingTax)));
					form.updateChargesTable();
				} catch (Exception e) {
					e.printStackTrace();
				}
				double netPay=fillNetPayable(form.getDoamtincltax().getValue());
				netPay=Math.round(netPay);
				int netPayable=(int) netPay;
				netPay=netPayable;
				form.getDonetpayamt().setValue(Double.parseDouble(nf.format(netPay)));
				
				
				String approverName="";
				if(form.olbApporverName.getValue()!=null){
					approverName=form.olbApporverName.getValue();
				}
				AppUtility.getApproverListAsPerTotalAmount(form.olbApporverName, "ServicePo", approverName, form.getDonetpayamt().getValue());
				form.hideWaitSymbol();
//			}
//	     };
//    	 timer.schedule(3000);
	}
	
	private double fillNetPayable(double amtincltax)
	{
		double amtval=0;
		if(form.getChargesTable().getDataprovider().getList().size()==0)
		{
			amtval=amtincltax;
		}
		if(form.getChargesTable().getDataprovider().getList().size()!=0)
		{
			amtval=amtincltax+form.getChargesTable().calculateNetPayable();
		}
		amtval=Math.round(amtval);
		int retAmtVal=(int) amtval;
		amtval=retAmtVal;
		return amtval;
	}
	
}
