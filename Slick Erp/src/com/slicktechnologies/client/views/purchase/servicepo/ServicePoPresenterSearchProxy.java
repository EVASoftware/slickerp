package com.slicktechnologies.client.views.purchase.servicepo;

import java.util.Vector;

import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessprocesslayer.ServicePo;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;

public class ServicePoPresenterSearchProxy extends SearchPopUpScreen<ServicePo> {

	IntegerBox  ibPoId;
	TextBox tbPoTitle;
	DateComparator dateComparator;
	
	ObjectListBox<Employee> olbPurchaseEngg;
	ObjectListBox<Employee> olbApprover;
	ObjectListBox<Branch> olbBranch;
	ListBox lbstatus;
	
	PersonInfoComposite vic;
	ProductInfoComposite pic;
	
	public ServicePoPresenterSearchProxy() {
		super();
		createGui();
	}
	
	private void initializeWidget(){
		ibPoId=new IntegerBox();
		tbPoTitle=new TextBox();
		
		dateComparator = new DateComparator("servicePoDate", new ServicePo());
		olbPurchaseEngg=new ObjectListBox<Employee>();
//		AppUtility.makeSalesPersonListBoxLive(olbPurchaseEngg);
		olbPurchaseEngg.makeEmployeeLive(AppConstants.PURCHASEMODULE, AppConstants.SERVICEPO, "Purchase Requisition");
		
		olbApprover=new ObjectListBox<Employee>();
		AppUtility.makeApproverListBoxLive(olbApprover, "Service PO");
		
		olbBranch=new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbBranch);
		
		
		vic = AppUtility.vendorInfoComposite(new Vendor());
		pic = AppUtility.product(new SuperProduct(), false);
		
		lbstatus = new ListBox();
		AppUtility.setStatusListBox(lbstatus, ServicePo.getStatusList());
		
		vic.getCustomerId().getHeaderLabel().setText("Vendor Id");
		vic.getCustomerName().getHeaderLabel().setText("Vendor Name");
		vic.getCustomerCell().getHeaderLabel().setText("Vendor Cell");
	}
	
	
	
	
	
	
	
	@Override
	public void createScreen() {
		initializeWidget();
		
		FormFieldBuilder fbuilder;
		
		fbuilder = new FormFieldBuilder("Service PO Id", ibPoId);
		FormField ftbServicePoId = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Service PO Title", tbPoTitle);
		FormField ftbServicePoTitle = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("From Date", dateComparator.getFromDate());
		FormField fdbServicePoDate = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("To Date", dateComparator.getToDate());
		FormField fdbServicePoDate1 = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Purchase Engineer", olbPurchaseEngg);
		FormField folbPurchaseEngg = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Approver", olbApprover);
		FormField folbApprover = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Branch", olbBranch);
		FormField folbBranch = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Status", lbstatus);
		FormField flbstatus = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("", vic);
		FormField fvic = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("", pic);
		FormField fpic = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		
		
		FormField[][] formfield = {
				{ ftbServicePoId, ftbServicePoTitle, fdbServicePoDate, fdbServicePoDate1 },
				{ folbPurchaseEngg, folbApprover, folbBranch, flbstatus },
				{ fvic }, 
				{ fpic } };

		this.fields = formfield;
		
	}







	@Override
	public MyQuerry getQuerry() {
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		
		if(ibPoId.getValue()!=null){
			temp = new Filter();
			temp.setIntValue(ibPoId.getValue());
			temp.setQuerryString("count");
			filtervec.add(temp);
		}
		if(!tbPoTitle.getValue().equals("")){
			temp = new Filter();
			temp.setStringValue(tbPoTitle.getValue());
			temp.setQuerryString("servicePoTitle");
			filtervec.add(temp);
		}
		
		if(olbPurchaseEngg.getValue()!=null){
			temp = new Filter();
			temp.setStringValue(olbPurchaseEngg.getValue());
			temp.setQuerryString("employee");
			filtervec.add(temp);
		}
		
		if(olbApprover.getValue()!=null){
			temp = new Filter();
			temp.setStringValue(olbApprover.getValue());
			temp.setQuerryString("approverName");
			filtervec.add(temp);
		}
		
		if(olbBranch.getValue()!=null){
			temp = new Filter();
			temp.setStringValue(olbBranch.getValue());
			temp.setQuerryString("branch");
			filtervec.add(temp);
		}
		
		if(lbstatus.getSelectedIndex()!=0){
			temp = new Filter();
			temp.setStringValue(lbstatus.getValue(lbstatus.getSelectedIndex()).trim());
			temp.setQuerryString("status");
			filtervec.add(temp);
		}
		
		if (!pic.getProdName().getValue().equals("")) {
			temp = new Filter();
			temp.setStringValue(pic.getProdName().getValue());
			temp.setQuerryString("items.serviceProduct.productName");
			filtervec.add(temp);
		}
		if (!pic.getProdCode().getValue().equals("")) {
			temp = new Filter();
			temp.setStringValue(pic.getProdCode().getValue());
			temp.setQuerryString("items.serviceProduct.productCode");
			filtervec.add(temp);
		}
		if (!pic.getProdID().getValue().equals("")) {
			temp = new Filter();
			temp.setIntValue(Integer.parseInt(pic.getProdID().getValue()));
			temp.setQuerryString("items.serviceProduct.count");
			filtervec.add(temp);
		}

		if (!vic.getName().getValue().equals("")) {
			temp = new Filter();
			temp.setStringValue(vic.getName().getValue());
			temp.setQuerryString("vinfo.fullName");
			filtervec.add(temp);
		}

		if (!vic.getId().getValue().equals("")) {
			temp = new Filter();
			temp.setIntValue(Integer.parseInt(vic.getId().getValue()));
			temp.setQuerryString("vinfo.count");
			filtervec.add(temp);
		}
		
		if (dateComparator.getValue() != null) {
			filtervec.addAll(dateComparator.getValue());
		}
		
		
		MyQuerry querry=new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new ServicePo());
		
		return querry;
	}

	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
		return true;
	}

}
