package com.slicktechnologies.client.views.purchase.servicepo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.code.p.gwtchismes.client.GWTCGlassPanel;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.SelectionCell;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.view.client.ProvidesKey;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.TaxDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.productlayer.Tax;

public class ServicePoLineItemTable extends SuperTable<SalesLineItem> {

	TextColumn<SalesLineItem> prodCategoryColumn, prodCodeColumn;
	Column<SalesLineItem, String> prodQtyColumn, prodNameColumn;
	TextColumn<SalesLineItem> viewprodQtyColumn;
	Column<SalesLineItem, Date> dateColumn;
	TextColumn<SalesLineItem> viewdColumn;
	Column<SalesLineItem, String> durationColumn, noServicesColumn,
			discountAmt;
	TextColumn<SalesLineItem> viewdurationColumn, viewnoServicesColumn,
			viewServicingTime, viewdiscountAmt;
	Column<SalesLineItem, String> priceColumn;
	TextColumn<SalesLineItem> viewPriceColumn;

	Column<SalesLineItem, String> vatColumn, serviceColumn;
	TextColumn<SalesLineItem> viewServiceTaxColumn;
	TextColumn<SalesLineItem> viewVatTaxColumn;

	public ArrayList<TaxDetails> vattaxlist;
	public ArrayList<TaxDetails> servicetaxlist;
	public ArrayList<String> list;
	public ArrayList<String> vatlist;

	Column<SalesLineItem, String> prodPercentDiscColumn;
	TextColumn<SalesLineItem> viewprodPercDiscColumn;
	Column<SalesLineItem, String> deleteColumn;
	Column<SalesLineItem, String> totalColumn;

	NumberFormat nf = NumberFormat.getFormat("0.00");

	int productIdCount = 0;
	int productSrNoCount = 0;
	public static boolean newModeFlag = false;

	TextColumn<SalesLineItem> warehouseColumn;
	Column<SalesLineItem, String> warehouseListColumn;
	ArrayList<String> warehouseList;
	static ArrayList<WareHouse> warehouseEntityList;
	protected GWTCGlassPanel glassPanel;
	public static boolean isValidWarehouse = true;

	public ServicePoLineItemTable() {
		super();
	}

	public ServicePoLineItemTable(UiScreen<SalesLineItem> view) {
		super(view);
	}

	public void createTable() {
		if (newModeFlag == false) {
			
			createColumnprodNameColumn();
			createColumndateColumn();
			createColumndurationColumn();
			createColumnnoServicesColumn();
			createColumnprodQtyColumn();
			createColumnpriceColumn();
			retriveServiceTax();
			table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
		}
	}
	
	@Override
	public void setEnable(boolean state) {
		for (int i = table.getColumnCount() - 1; i > -1; i--) {
			table.removeColumn(i);
		}
		if (state == true) {
			createColumnprodCodeColumn();
			createColumnprodNameColumn();
			createColumnprodQtyColumn();
			createColumnpriceColumn();
			createColumndateColumn();
			createColumndurationColumn();
			createColumnnoServicesColumn();
			retriveServiceTax();
		} else {
			createColumnprodCodeColumn();
			createColumnprodNameColumn();
			createViewQtyColumn();
			createViewColumnpriceColumn();
			createViewColumndateColumn();
			createColumnViewDuration();
			createViewColumnnoServicesColumn();
			createViewColumnServicingTimeColumn();
			createViewVatTaxColumn();
			createViewServiceTaxColumn();
			createViewPercDiscount();
			createViewDiscountAmt();
			createColumntotalColumn();
			addWarehouseColoumn();
		}
	}

	private void createViewColumnServicingTimeColumn() {
		viewServicingTime = new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				if (object.getServicingTIme() == 0) {
					return "N.A" + "";
				} else {
					return object.getServicingTIme() + "";
				}
			}
		};
		table.addColumn(viewServicingTime, "Ser.Time");
		table.setColumnWidth(viewServicingTime, 100, Unit.PX);
	}

	/**
	 * @author Anil,Date : 08-01-2019
	 * Changed label from service  to Tax 2
	 */
	protected void createViewServiceTaxColumn() {

		viewServiceTaxColumn = new TextColumn<SalesLineItem>() {
			public String getValue(SalesLineItem object) {
				return object.getServiceTax().getTaxConfigName();
			}
		};
		table.addColumn(viewServiceTaxColumn, "# Tax 2");
		table.setColumnWidth(viewServiceTaxColumn, 100, Unit.PX);
	}

	/**
	 * @author Anil,Date : 08-01-2019
	 * Changed label from Vat to Tax 1
	 */
	protected void createViewVatTaxColumn() {

		viewVatTaxColumn = new TextColumn<SalesLineItem>() {
			public String getValue(SalesLineItem object) {
				return object.getVatTax().getTaxConfigName();
			}
		};
		table.addColumn(viewVatTaxColumn, "# Tax 1");
		table.setColumnWidth(viewVatTaxColumn, 100, Unit.PX);
	}

	private void retriveServiceTax() {
		final GenricServiceAsync service = GWT.create(GenricService.class);
		MyQuerry query = new MyQuerry();
		Company c = new Company();
		System.out.println("Company Id :: " + c.getCompanyId());

		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;

		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);

		query.setFilters(filtervec);
		query.setQuerryObject(new TaxDetails());

		service.getSearchResult(query,
				new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onFailure(Throwable caught) {
					}

					public void onSuccess(ArrayList<SuperModel> result) {
						list = new ArrayList<String>();
						servicetaxlist = new ArrayList<TaxDetails>();
						vattaxlist = new ArrayList<TaxDetails>();
						vatlist = new ArrayList<String>();
						List<TaxDetails> backlist = new ArrayList<TaxDetails>();

						for (SuperModel model : result) {
							TaxDetails entity = (TaxDetails) model;
							backlist.add(entity);
							servicetaxlist.add(entity);
							vattaxlist.add(entity);
						}
						/**
						 * @author Anil,Date : 08-01-2019
						 * Earlier added vat tax in vat list and service tax in list
						 * now will add all taxes on both list
						 */
						for (int i = 0; i < backlist.size(); i++) {
							list.add(backlist.get(i).getTaxChargeName());
							vatlist.add(backlist.get(i).getTaxChargeName());
						}

						retrieveWarehouseNameList();
						// addEditColumnVatTax();
						// addEditColumnServiceTax();
						// createColumnPercDiscount();
						// createColumnDiscountAmt();
						// createColumntotalColumn();
						// createColumndeleteColumn();
						// addFieldUpdater();
					}

				});
	}

	public void retrieveWarehouseNameList() {
		final GenricServiceAsync service = GWT.create(GenricService.class);
		MyQuerry query = new MyQuerry();
		System.out.println("Company Id :: " + UserConfiguration.getCompanyId());

		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;

		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(UserConfiguration.getCompanyId());
		filtervec.add(filter);

		filter = new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);

		query.setFilters(filtervec);
		query.setQuerryObject(new WareHouse());

		service.getSearchResult(query,
				new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						warehouseList = new ArrayList<String>();
						warehouseEntityList = new ArrayList<WareHouse>();
						warehouseList.add("");
						if (result.size() != 0) {
							for (SuperModel model : result) {
								WareHouse entity = (WareHouse) model;
								warehouseList.add(entity.getBusinessUnitName());
								warehouseEntityList.add(entity);
							}
							Collections.sort(warehouseList);
						}

						addEditColumnVatTax();
						addEditColumnServiceTax();
						createColumnPercDiscount();
						createColumnDiscountAmt();
						createColumntotalColumn();
						addEditableWarehouseColoumn();
						createColumnprodCodeColumn();
						createColumndeleteColumn();
						addFieldUpdater();
					}
					@Override
					public void onFailure(Throwable caught) {
					}
				});
	}
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public void addWarehouseColoumn() {
		warehouseColumn = new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				if (object.getWarehouseName() != null) {
					return object.getWarehouseName();
				}
				return "";
			}
		};
		table.addColumn(warehouseColumn, "#Warehouse");
		table.setColumnWidth(warehouseColumn, 150, Unit.PX);
	}

	public void addEditableWarehouseColoumn() {
		SelectionCell selectionCell = new SelectionCell(warehouseList);
		warehouseListColumn = new Column<SalesLineItem, String>(selectionCell) {
			@Override
			public String getValue(SalesLineItem object) {
				if (object.getWarehouseName() != null) {
					return object.getWarehouseName();
				}
				return "";
			}
		};
		table.addColumn(warehouseListColumn, "#Warehouse");
		table.setColumnWidth(warehouseListColumn, 150, Unit.PX);

	}

	private void setFieldUpdatorOnWarehouse() {
		warehouseListColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {
			@Override
			public void update(int index, SalesLineItem object,String value) {

				if (validWarehouseSelection(object.getPrduct().getCount(), value)) {
					isValidWarehouse = true;
				} else {
					isValidWarehouse = false;
				}
				object.setWarehouseName(value);

				if (isValidWarehouse) {
					AppUtility.ApplyWhiteColoronRow(ServicePoLineItemTable.this, index);
				} else {
					AppUtility.ApplyRedColoronRow(ServicePoLineItemTable.this, index);
				}
				if (isValidWarehouse) {
					List<SalesLineItem> list = getDataprovider().getList();
					Integer rowIndex = getRowCountOfWarehouse(list);
					if (rowIndex != null) {
						isValidWarehouse = false;
						AppUtility.ApplyRedColoronRow(ServicePoLineItemTable.this, rowIndex);
					} else {
						isValidWarehouse = true;
						AppUtility.ApplyWhiteColoronRow(ServicePoLineItemTable.this, index);
					}
				}
				table.redraw();
				System.out.println("ROW UPDATED "+ object.getWarehouseName());
			}
		});
	}

	private Integer getRowCountOfWarehouse(List<SalesLineItem> list) {
		for (SalesLineItem prod1 : list) {
			int count = 0;
			int index = 0;
			for (SalesLineItem prod2 : list) {
				if (prod1.getPrduct().getCount() == prod2.getPrduct().getCount()
						&& prod1.getWarehouseName().equals(prod2.getWarehouseName())) {
					count++;
				}
				if (count == 2) {
					return index;
				}
				index++;
			}
		}
		return null;
	}

	public void showWaitSymbol() {
		glassPanel = new GWTCGlassPanel();
		glassPanel.show();

	}

	public void hideWaitSymbol() {
		glassPanel.hide();

	}

	private boolean validWarehouseSelection(int productID, String value) {
		List<SalesLineItem> list = getDataprovider().getList();
		for (SalesLineItem prod : list) {
			if (productID == prod.getPrduct().getCount()
					&& value.trim().equals(prod.getWarehouseName().trim())) {
				GWTCAlert alert = new GWTCAlert();
				alert.alert("Warehouse is already selected for product !");
				return false;
			}
		}
		return true;
	}
	
	public static String getWarehouseState(String warehouseName){
		String state="";
		for(WareHouse obj:warehouseEntityList){
			if(obj.getBusinessUnitName().trim().equals(warehouseName.trim())){
				return obj.getAddress().getState();
			}
		}
		return "";
		
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// ***************discount Amt column ***************
	private void createColumnDiscountAmt() {
		EditTextCell editCell = new EditTextCell();
		discountAmt = new Column<SalesLineItem, String>(editCell) {
			@Override
			public String getValue(SalesLineItem object) {
				if (object.getDiscountAmt() == 0) {
					return 0 + "";
				} else {
					return object.getDiscountAmt() + "";
				}
			}
		};
		table.addColumn(discountAmt, "#Disc Amt");
		table.setColumnWidth(discountAmt, 100, Unit.PX);
	}

	protected void createViewDiscountAmt() {
		viewdiscountAmt = new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				if (object.getDiscountAmt() == 0) {
					return 0 + "";
				} else {
					return object.getDiscountAmt() + "";
				}
			}
		};
		table.addColumn(viewdiscountAmt, "#Disc Amt");
		table.setColumnWidth(viewdiscountAmt, 100, Unit.PX);
	}
	/**
	 * @author Anil,Date : 08-01-2019
	 * Changed tax label from vat tax to tax 1
	 */
	// vat tax Selection List Column
	public void addEditColumnVatTax() {
		SelectionCell employeeselection = new SelectionCell(vatlist);
		vatColumn = new Column<SalesLineItem, String>(employeeselection) {
			@Override
			public String getValue(SalesLineItem object) {
				if (object.getVatTax().getTaxConfigName() != null&&!object.getVatTax().getTaxConfigName().equals("")) {
					return object.getVatTax().getTaxConfigName();
				} else
					return "NA";
			}
		};
		table.addColumn(vatColumn, "#Tax 1");
		table.setColumnWidth(vatColumn, 130, Unit.PX);

	}

	/**
	 *@author Anil,Date : 08-01-2019
	 *Updated Tax 1 updater code same as it was in contract
	 */
	public void updateVatTaxColumn() {

		vatColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {

			@Override
			public void update(int index, SalesLineItem object, String value) {
//				try {
//					String val1 = (value.trim());
//					object.setVatTaxEdit(val1);
//					Tax tax = object.getVatTax();
//					for (int i = 0; i < vattaxlist.size(); i++) {
//						if (val1.trim().equals(vattaxlist.get(i).getTaxChargeName())) {
//							tax.setTaxName(val1);
//							tax.setPercentage(vattaxlist.get(i).getTaxChargePercent());
//							break;
//						}
//					}
//					object.setVatTax(tax);
//					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
//
//				} catch (NumberFormatException e) {
//				}
//				table.redrawRow(index);
				
				try {
					String val1 = (value.trim());
					object.setVatTaxEdit(val1);
					Tax tax = object.getVatTax();
					for(int i=0;i<vattaxlist.size();i++){
						if(val1.trim().equals(vattaxlist.get(i).getTaxChargeName())){
							tax.setTaxName(val1);
							tax.setPercentage(vattaxlist.get(i).getTaxChargePercent());
							tax.setTaxPrintName(vattaxlist.get(i).getTaxPrintName());
							tax.setTaxConfigName(val1);
							Tax taxDt = AppUtility.checkTaxValue(tax, object.getServiceTax(), vattaxlist);
							if(taxDt != null){
								object.setServiceTax(taxDt);
							}
							if(vattaxlist.get(i).getTaxPrintName().equals("IGST") || vattaxlist.get(i).getTaxPrintName().equals("IGST@18") || vattaxlist.get(i).getTaxPrintName().equals("SEZ")){
								serviceColumn.setCellStyleNames("hideVisibility");
							}else{
								serviceColumn.setCellStyleNames("showVisibility");
							}
							break;
						}
					}
					object.setVatTax(tax);
					productIdCount=object.getPrduct().getCount();
					productSrNoCount = object.getProductSrNo();
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
					
				} catch (Exception e) {
				}
				table.redrawRow(index);
			}
		});
	}

	/**
	 * @author Anil,Date : Changed label from Service tax to tax 2
	 */
	// service tax Selection List Column
	public void addEditColumnServiceTax() {
		SelectionCell employeeselection = new SelectionCell(list);
		serviceColumn = new Column<SalesLineItem, String>(employeeselection) {
			@Override
			public String getValue(SalesLineItem object) {
				if (object.getServiceTax().getTaxConfigName() != null&&!object.getServiceTax().getTaxConfigName().equals("")) {
					return object.getServiceTax().getTaxConfigName();
				} else
					return "NA";
			}

		};
		table.addColumn(serviceColumn, "#Tax 2");
		table.setColumnWidth(serviceColumn, 130, Unit.PX);
	}
	/**
	 *@author Anil,Date : 08-01-2019
	 *Updated Tax 1 updater code same as it was in contract
	 */
	public void updateServiceTaxColumn() {

		serviceColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {
			@Override
			public void update(int index, SalesLineItem object,String value) {
//				try {
//					String val1 = (value.trim());
//					object.setServiceTaxEdit(val1);
//					Tax tax = object.getServiceTax();
//					for (int i = 0; i < servicetaxlist.size(); i++) {
//						if (val1.trim().equals(servicetaxlist.get(i).getTaxChargeName())) {
//							tax.setTaxName(val1);
//							tax.setPercentage(servicetaxlist.get(i).getTaxChargePercent());
//							break;
//						}
//					}
//					object.setServiceTax(tax);
//					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
//				} catch (NumberFormatException e) {
//				}
//				table.redrawRow(index);
				try {
					String val1 = (value.trim());
					object.setServiceTaxEdit(val1);
					Tax tax = object.getServiceTax();
					for(int i=0;i<servicetaxlist.size();i++){
						if(val1.trim().equals(servicetaxlist.get(i).getTaxChargeName())){
							tax.setTaxName(val1);
							tax.setPercentage(servicetaxlist.get(i).getTaxChargePercent());
							tax.setTaxPrintName(servicetaxlist.get(i).getTaxPrintName());
							tax.setTaxConfigName(val1);
							Tax vatTax = AppUtility.checkTaxValue(tax, object.getVatTax(), vattaxlist);
							if(vatTax!=null){
								object.setVatTax(vatTax);
							}
							if(servicetaxlist.get(i).getTaxPrintName().equals("IGST") || servicetaxlist.get(i).getTaxPrintName().equals("IGST@18") || servicetaxlist.get(i).getTaxPrintName().equals("SEZ")){
								vatColumn.setCellStyleNames("hideVisibility");
							}else{
								vatColumn.setCellStyleNames("showVisibility");
							}
							break;
						}
					}
					object.setServiceTax(tax);
					productIdCount=object.getPrduct().getCount();
					productSrNoCount = object.getProductSrNo();
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
					
				} catch (Exception e) {
				}
				table.redrawRow(index);
			}
		});
	}

	// ******************************changes ends here
	// ***************************************
	protected void createColumnprodCategoryColumn() {
		prodCategoryColumn = new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				return object.getProductCategory();
			}
		};
		table.addColumn(prodCategoryColumn, "Category");
		table.setColumnWidth(prodCategoryColumn, 100, Unit.PX);

	}

	protected void createColumnprodCodeColumn() {
		prodCodeColumn = new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				return object.getProductCode();
			}
		};
		table.addColumn(prodCodeColumn, "PCode");
		table.setColumnWidth(prodCodeColumn, 100, Unit.PX);

	}

	protected void createColumnprodNameColumn() {
		EditTextCell editCell = new EditTextCell();
		prodNameColumn = new Column<SalesLineItem, String>(editCell) {
			@Override
			public String getValue(SalesLineItem object) {
				return object.getProductName();
			}
		};
		table.addColumn(prodNameColumn, "Name");
		table.setColumnWidth(prodNameColumn, 100, Unit.PX);
	}

	protected void createColumnprodQtyColumn() {
		EditTextCell editCell = new EditTextCell();
		prodQtyColumn = new Column<SalesLineItem, String>(editCell) {
			@Override
			public String getValue(SalesLineItem object) {
				if (object.getQty() == 0) {
					return 0 + "";
				} else {
					return object.getQty() + "";
				}
			}
		};
		table.addColumn(prodQtyColumn, "#Qty");
		table.setColumnWidth(prodQtyColumn, 100, Unit.PX);
	}

	protected void createViewQtyColumn() {
		viewprodQtyColumn = new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				if (object.getQty() == 0) {
					return 0 + "";
				} else {
					return object.getQty() + "";
				}
			}
		};
		table.addColumn(viewprodQtyColumn, "#Qty");
		table.setColumnWidth(viewprodQtyColumn, 100, Unit.PX);
	}

	protected void createColumnPercDiscount() {
		EditTextCell editCell = new EditTextCell();
		prodPercentDiscColumn = new Column<SalesLineItem, String>(editCell) {
			@Override
			public String getValue(SalesLineItem object) {
				if (object.getPercentageDiscount() == 0
						|| object.getPercentageDiscount() == null) {
					return 0 + "";
				} else {
					return object.getPercentageDiscount() + "";
				}
			}
		};
		table.addColumn(prodPercentDiscColumn, "#% Disc");
		table.setColumnWidth(prodPercentDiscColumn, 100, Unit.PX);

	}

	protected void createViewPercDiscount() {
		viewprodPercDiscColumn = new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				if (object.getPercentageDiscount() == 0) {
					return 0 + "";
				} else {
					return object.getPercentageDiscount() + "";
				}
			}
		};
		table.addColumn(viewprodPercDiscColumn, "#% Disc");
		table.setColumnWidth(viewprodPercDiscColumn, 100, Unit.PX);
	}

	protected void createColumndateColumn() {
		final DateTimeFormat fmt = DateTimeFormat.getFormat("dd/MM/yyyy");
		DatePickerCell date = new DatePickerCell(fmt);
		dateColumn = new Column<SalesLineItem, Date>(date) {
			@Override
			public Date getValue(SalesLineItem object) {
				if (object.getStartDate() != null) {
					return fmt.parse(fmt.format(object.getStartDate()));
				} else {
					object.setStartDate(new Date());
					return fmt.parse(fmt.format(new Date()));
				}
			}
		};
		table.setColumnWidth(dateColumn, 90, Unit.PX);
		table.addColumn(dateColumn, "#Date");
		table.setColumnWidth(dateColumn, 100, Unit.PX);
	}

	protected void createViewColumndateColumn() {
		viewdColumn = new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				if (object.getStartDate() != null) {

					return AppUtility.parseDate(object.getStartDate());
				} else {
					return "N.A.";
				}
			}
		};
		table.addColumn(viewdColumn, "Date");
		table.setColumnWidth(viewdColumn, 100, Unit.PX);
	}

	protected void createColumndurationColumn() {
		EditTextCell editCell = new EditTextCell();
		durationColumn = new Column<SalesLineItem, String>(editCell) {
			@Override
			public String getValue(SalesLineItem object) {
				if (object.getDuration() == 0) {
					return 0 + "";
				} else {
					return object.getDuration() + "";
				}
			}
		};
		table.addColumn(durationColumn, "#Duration");
		table.setColumnWidth(durationColumn, 100, Unit.PX);
	}

	protected void createColumnViewDuration() {
		viewdurationColumn = new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				if (object.getDuration() == 0)
					return "N.A";
				else {
					return object.getDuration() + "";
				}
			}
		};
		table.addColumn(viewdurationColumn, "Duration");
		table.setColumnWidth(viewdurationColumn, 100, Unit.PX);
	}

	protected void createColumnnoServicesColumn() {
		EditTextCell editCell = new EditTextCell();
		noServicesColumn = new Column<SalesLineItem, String>(editCell) {
			@Override
			public String getValue(SalesLineItem object) {
				if (object.getNumberOfServices() == 0) {
					return 0 + "";
				} else {
					return object.getNumberOfServices() + "";
				}
			}
		};
		table.addColumn(noServicesColumn, "#Services");
		table.setColumnWidth(noServicesColumn, 100, Unit.PX);
	}

	protected void createViewColumnnoServicesColumn() {
		viewnoServicesColumn = new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				if (object.getNumberOfServices() == 0) {
					return "N.A" + "";
				} else {
					return object.getNumberOfServices() + "";
				}
			}
		};
		table.addColumn(viewnoServicesColumn, "Services");
		table.setColumnWidth(viewnoServicesColumn, 100, Unit.PX);
	}

	protected void createColumnpriceColumn() {
		EditTextCell editCell = new EditTextCell();
		priceColumn = new Column<SalesLineItem, String>(editCell) {
			@Override
			public String getValue(SalesLineItem object) {
				SuperProduct product = object.getPrduct();
				double tax = removeAllTaxes(product);
				double origPrice = object.getPrice() - tax;

				return nf.format(origPrice);
			}
		};
		table.addColumn(priceColumn, "#Price");
		table.setColumnWidth(priceColumn, 100, Unit.PX);
	}

	protected void createViewColumnpriceColumn() {
		viewPriceColumn = new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				if (object.getPrice() == 0) {
					return 0 + "";
				} else {
					SuperProduct product = object.getPrduct();
					double tax = removeAllTaxes(product);
					double origPrice = object.getPrice() - tax;

					return nf.format(origPrice);
				}
			}
		};
		table.addColumn(viewPriceColumn, "#Price");
		table.setColumnWidth(viewPriceColumn, 100, Unit.PX);
	}

	protected void createColumntotalColumn() {
		TextCell editCell = new TextCell();
		totalColumn = new Column<SalesLineItem, String>(editCell) {
			@Override
			public String getValue(SalesLineItem object) {
				double total = 0;
				SuperProduct product = object.getPrduct();
				double tax = removeAllTaxes(product);
				double origPrice = object.getPrice() - tax;

				// ****************old code

				// if(object.getPercentageDiscount()==null){
				// total=origPrice*object.getQty();
				// }
				// if(object.getPercentageDiscount()!=null){
				// total=origPrice-(origPrice*object.getPercentageDiscount()/100);
				// total=total*object.getQty();
				// }
				// ****************new code for discount by rohan
				// ******************
				// ********** this code is commented by vijay for area + disc on
				// total *************
				// if((object.getPercentageDiscount()==null &&
				// object.getPercentageDiscount()==0) &&
				// (object.getDiscountAmt()==0) ){
				//
				// System.out.println("inside both 0 condition");
				// total=origPrice*object.getQty();
				// }
				//
				// else if((object.getPercentageDiscount()!=null)&&
				// (object.getDiscountAmt()!=0)){
				//
				// System.out.println("inside both not null condition");
				//
				// total=origPrice-(origPrice*object.getPercentageDiscount()/100);
				// total=total-object.getDiscountAmt();
				// total=total*object.getQty();
				// }
				// else
				// {
				// System.out.println("inside oneof the null condition");
				//
				// if(object.getPercentageDiscount()!=null){
				// System.out.println("inside getPercentageDiscount oneof the null condition");
				// total=origPrice-(origPrice*object.getPercentageDiscount()/100);
				// }
				// else
				// {
				// System.out.println("inside getDiscountAmt oneof the null condition");
				// total=origPrice-object.getDiscountAmt();
				// }
				//
				// total=total*object.getQty();
				//
				// }
				//
				//
				// return nf.format(total);
				// }
				// };
				// table.addColumn(totalColumn,"Total");

				// new code by vijay add code for area calculation

				System.out.println("Get value from area ==" + object.getArea());
				if (object.getArea().equals("") || object.getArea().equals("0")) {
					System.out.println(" vijay mmmmmmm ");
					object.setArea("NA");
				}

				System.out.println("area == " + object.getArea());

				if ((object.getPercentageDiscount() == null && object
						.getPercentageDiscount() == 0)
						&& (object.getDiscountAmt() == 0)) {

					System.out.println("inside both 0 condition");

					/********************
					 * Square Area Calculation code added in if condition and
					 * without square area calculation code in else block
					 ***************************/
					System.out.println("Get value from area =="
							+ object.getArea());
					System.out
							.println("total amount before area calculation =="
									+ total);
					if (!object.getArea().equalsIgnoreCase("NA")) {
						double area = Double.parseDouble(object.getArea());
						total = origPrice * object.getQty() * area;
						System.out
								.println(" Final TOTAL if no discount per & no discount Amt ==="
										+ total);
					} else {
						total = total * object.getQty();
					}

				}
				/********************
				 * Square Area Calculation code added in if condition and
				 * without square area calculation code in else block
				 ***************************/
				else if ((object.getPercentageDiscount() != null)
						&& (object.getDiscountAmt() != 0)) {

					System.out.println("inside both not null condition");
					System.out
							.println("total amount before area calculation =="
									+ total);
					if (!object.getArea().equalsIgnoreCase("NA")) {
						double area = Double.parseDouble(object.getArea());
						total = origPrice * object.getQty() * area;
						System.out.println("total before discount per ===="
								+ total);
						total = total
								- (total * object.getPercentageDiscount() / 100);
						System.out.println("after discount per total === "
								+ total);
						total = total - object.getDiscountAmt();
						System.out.println("after discount AMT total === "
								+ total);

						System.out
								.println(" Final TOTAL   discount per &  discount Amt ==="
										+ total);
					} else {
						System.out.println(" normal === total " + total);
						total = origPrice * object.getQty();
						total = total
								- (total * object.getPercentageDiscount() / 100);
						total = total - object.getDiscountAmt();
					}
				} else {
					System.out.println("inside oneof the null condition");
					if (object.getPercentageDiscount() != null
							|| object.getPercentageDiscount() != 0) {
						System.out
								.println("inside getPercentageDiscount oneof the null condition");
						/********************
						 * Square Area Calculation code added in if condition
						 * and without square area calculation code in else
						 * block
						 ***************************/
						System.out.println("tatal $$$$$$$$$$ ====" + total);
						if (!object.getArea().equalsIgnoreCase("NA")) {
							double area = Double.parseDouble(object.getArea());
							total = origPrice * object.getQty() * area;
							System.out.println("total before discount per ===="
									+ total);
							total = total
									- (total * object.getPercentageDiscount() / 100);
							System.out.println("after discount per total === "
									+ total);

						} else {
							System.out.println("old code");
							total = origPrice * object.getQty();
							total = total
									- (total * object.getPercentageDiscount() / 100);
						}
					} else {
						System.out
								.println("inside getDiscountAmt oneof the null condition");
						/********
						 * vijay added code for square area wise calculation new
						 * code added in if condition and old code added in else
						 ***************/
						System.out.println("tatal $$$$$$$$$$ ====" + total);
						if (!object.getArea().equalsIgnoreCase("NA")) {
							double area = Double.parseDouble(object.getArea());
							total = origPrice * object.getQty() * area;
							System.out.println("total before discount amt ===="
									+ total);
							total = total - object.getDiscountAmt();
							System.out.println("after discount amt total === "
									+ total);

						} else {
							total = total * object.getQty();
							total = total - object.getDiscountAmt();
						}

					}
					System.out.println("6666666666666");
				}

				return nf.format(total);
			}
		};
		table.addColumn(totalColumn, "Total");
		table.setColumnWidth(totalColumn, 100, Unit.PX);
	}

	protected void createColumndeleteColumn() {
		ButtonCell btnCell = new ButtonCell();
		deleteColumn = new Column<SalesLineItem, String>(btnCell) {
			@Override
			public String getValue(SalesLineItem object) {
				return "Delete";
			}
		};
		table.addColumn(deleteColumn, "Delete");
		table.setColumnWidth(deleteColumn, 100, Unit.PX);
	}

	@Override
	public void addFieldUpdater() {
		createFieldUpdaterproductNameColumn();
		createFieldUpdaterprodQtyColumn();
		createFieldUpdaterdateColumn();
		createFieldUpdaterpriceColumn();
		createFieldUpdaterprodDurationColumn();
		createFieldUpdaterprodServices();
		createFieldUpdaterPercDiscColumn();
		createFieldUpdaterDiscAmtColumn();
		createFieldUpdaterdeleteColumn();
		setFieldUpdatorOnWarehouse();
		updateServiceTaxColumn();
		updateVatTaxColumn();

	}

	protected void createFieldUpdaterprodQtyColumn() {
		prodQtyColumn
				.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {
					@Override
					public void update(int index, SalesLineItem object,
							String value) {

						try {
							Double val1 = Double.parseDouble(value.trim());
							object.setQuantity(val1);
							productIdCount = object.getPrduct().getCount();
							productSrNoCount = object.getProductSrNo();
							RowCountChangeEvent.fire(table, getDataprovider()
									.getList().size(), true);

						} catch (NumberFormatException e) {

						}

						table.redrawRow(index);
					}
				});
	}

	protected void createFieldUpdaterDiscAmtColumn() {
		discountAmt.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {
			@Override
			public void update(int index, SalesLineItem object, String value) {

				try {
					Double val1 = Double.parseDouble(value.trim());
					object.setDiscountAmt(val1);
					RowCountChangeEvent.fire(table, getDataprovider().getList()
							.size(), true);

				} catch (NumberFormatException e) {

				}
				table.redrawRow(index);
			}
		});
	}

	protected void createFieldUpdaterPercDiscColumn() {
		prodPercentDiscColumn
				.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {
					@Override
					public void update(int index, SalesLineItem object,
							String value) {

						try {
							Double val1 = Double.parseDouble(value.trim());
							object.setPercentageDiscount(val1);
							RowCountChangeEvent.fire(table, getDataprovider()
									.getList().size(), true);

						} catch (NumberFormatException e) {

						}
						table.redrawRow(index);
					}
				});
	}

	protected void createFieldUpdaterdateColumn() {
		dateColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, Date>() {
			@Override
			public void update(int index, SalesLineItem object, Date value) {
				object.setStartDate(value);
				productIdCount = object.getPrduct().getCount();
				productSrNoCount = object.getProductSrNo();
				RowCountChangeEvent.fire(table, getDataprovider().getList()
						.size(), true);
				table.redrawRow(index);
			}
		});
	}

	protected void createFieldUpdaterprodDurationColumn() {
		durationColumn
				.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {
					@Override
					public void update(int index, SalesLineItem object,
							String value) {

						try {
							Integer val1 = Integer.parseInt(value.trim());
							object.setDuration(val1);
							productIdCount = object.getPrduct().getCount();
							productSrNoCount = object.getProductSrNo();
							RowCountChangeEvent.fire(table, getDataprovider()
									.getList().size(), true);

						} catch (NumberFormatException e) {
						}
						table.redrawRow(index);
					}
				});
	}

	protected void createFieldUpdaterprodServices() {
		noServicesColumn
				.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {
					@Override
					public void update(int index, SalesLineItem object,
							String value) {

						try {
							Integer val1 = Integer.parseInt(value.trim());
							object.setNumberOfService(val1);
							productIdCount = object.getPrduct().getCount();
							productSrNoCount = object.getProductSrNo();
							RowCountChangeEvent.fire(table, getDataprovider()
									.getList().size(), true);

						} catch (NumberFormatException e) {
						}
						table.redrawRow(index);
					}
				});
	}

	protected void createFieldUpdaterproductNameColumn() {
		prodNameColumn
				.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {
					@Override
					public void update(int index, SalesLineItem object,
							String value) {

						try {
							String val1 = value.trim();
							object.setProductName(val1);
							RowCountChangeEvent.fire(table, getDataprovider()
									.getList().size(), true);

						} catch (NumberFormatException e) {

						}
						table.redrawRow(index);
					}
				});
	}

	protected void createFieldUpdaterpriceColumn() {
		priceColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {
			@Override
			public void update(int index, SalesLineItem object, String value) {
				try {
					Double val1 = Double.parseDouble(value.trim());

					object.setPrice(val1);
					RowCountChangeEvent.fire(table, getDataprovider().getList()
							.size(), true);

				} catch (Exception e) {
				}
				table.redrawRow(index);
			}
		});
	}

	protected void createFieldUpdaterdeleteColumn() {
		deleteColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {
			@Override
			public void update(int index, SalesLineItem object, String value) {
				getDataprovider().getList().remove(object);
				productIdCount = 0;
				table.redrawRow(index);
			}
		});
	}

	

	@Override
	public void applyStyle() {

	}

	@Override
	protected void initializekeyprovider() {
		keyProvider = new ProvidesKey<SalesLineItem>() {
			@Override
			public Object getKey(SalesLineItem item) {
				if (item == null) {
					return null;
				} else
					return item.getId();
			}
		};
	}
	
	public double calculateTotalExcludingTax() {
		List<SalesLineItem> list = getDataprovider().getList();
		double sum = 0, priceVal = 0;
		for (int i = 0; i < list.size(); i++) {
			SalesLineItem entity = list.get(i);
			SuperProduct prod = entity.getPrduct();
			double taxAmt = removeAllTaxes(prod);

			// ******************* new code by vijay (rohan) for sq area + disc
			// on total ********************************

			System.out.println("Square Area === " + entity.getArea());

			if ((entity.getPercentageDiscount() == null && entity
					.getPercentageDiscount() == 0)
					&& (entity.getDiscountAmt() == 0)) {

				System.out.println("inside both 0 condition");
				/********************
				 * Square Area Calculation code added in if condition and
				 * without square area calculation code in else block
				 ***************************/
				if (!entity.getArea().equalsIgnoreCase("NA")) {
					double squareArea = Double.parseDouble(entity.getArea());
					priceVal = entity.getPrice() - taxAmt;
					priceVal = priceVal * entity.getQty() * squareArea;
					sum = sum + priceVal;
					System.out.println("RRRRRRRRRRRRRR sum" + sum);
				} else {
					System.out.println("Old code");
					priceVal = entity.getPrice() - taxAmt;
					priceVal = priceVal * entity.getQty();
					sum = sum + priceVal;
					System.out.println("RRRRRRRRRRRRRR sum" + sum);
				}

				/******************** vijay ***************************/

			}

			else if ((entity.getPercentageDiscount() != null)
					&& (entity.getDiscountAmt() != 0)) {

				System.out.println("inside both not null condition");

				/********************
				 * Square Area Calculation code added in if condition and
				 * without square area calculation code in else block
				 ***************************/
				if (!entity.getArea().equalsIgnoreCase("NA")) {
					priceVal = entity.getPrice() - taxAmt;
					double squareArea = Double.parseDouble(entity.getArea());
					priceVal = priceVal * entity.getQty() * squareArea;
					priceVal = priceVal
							- (priceVal * entity.getPercentageDiscount() / 100);
					priceVal = priceVal - entity.getDiscountAmt();
					sum = sum + priceVal;
					System.out.println("RRRRRRRRRRRRRR sum" + sum);

				} else {
					System.out.println("Old code ===");
					priceVal = entity.getPrice() - taxAmt;
					priceVal = priceVal * entity.getQty();
					priceVal = priceVal
							- (priceVal * entity.getPercentageDiscount() / 100);
					priceVal = priceVal - entity.getDiscountAmt();
					sum = sum + priceVal;
					System.out.println("RRRRRRRRRRRRRR sum" + sum);
				}

				/********************************************************/

			} else {
				System.out.println("inside oneof the null condition");

				priceVal = entity.getPrice() - taxAmt;
				if (entity.getPercentageDiscount() != null) {
					System.out
							.println("inside getPercentageDiscount oneof the null condition");
					/********************
					 * Square Area Calculation code added in if condition and
					 * without square area calculation code in else block
					 ***************************/
					if (!entity.getArea().equalsIgnoreCase("NA")) {
						double squareArea = Double
								.parseDouble(entity.getArea());
						priceVal = priceVal * entity.getQty() * squareArea;
						priceVal = priceVal
								- (priceVal * entity.getPercentageDiscount() / 100);
						sum = sum + priceVal;
						System.out.println("RRRRRRRRRRRRRR sum" + sum);
					} else {
						System.out.println("old code");
						priceVal = priceVal * entity.getQty();
						sum = sum
								+ priceVal
								- (priceVal * entity.getPercentageDiscount() / 100);

					}
					/****************************************************************************************/
				} else {
					System.out
							.println("inside getDiscountAmt oneof the null condition");
					/********************
					 * Square Area Calculation code added in if condition and
					 * without square area code calculation in else block
					 ***************************/
					if (!entity.getArea().equalsIgnoreCase("NA")) {
						double squareArea = Double
								.parseDouble(entity.getArea());
						priceVal = priceVal * entity.getQty() * squareArea;
						priceVal = priceVal - entity.getDiscountAmt();
						sum = sum + priceVal;
						System.out.println("RRRRRRRRRRRRRR sum" + sum);
					} else {
						System.out.println("old code");
						priceVal = priceVal * entity.getQty();
						sum = sum + priceVal - entity.getDiscountAmt();

					}
					/******************** new code end here ************************************************/
				}

				System.out.println("RRRRRRRRRRRRRR sum" + sum);
			}

		}
		return sum;
	}
	
	/**
	 * 
	 
	public double calculateTotalExcludingTax(){
//		List<SalesLineItem>list=getDataprovider().getList();
//		double sum=0,priceVal=0;
//		for(int i=0;i<list.size();i++){
//			SalesLineItem entity=list.get(i);
//			SuperProduct prod=entity.getPrduct();
//			double taxAmt=removeAllTaxes(prod);
//			if((entity.getPercentageDiscount()==null && entity.getPercentageDiscount()==0) 
//					&& (entity.getDiscountAmt()==0) ){
//				System.out.println("inside both 0 condition");
//				if(!entity.getArea().equalsIgnoreCase("NA") && !LoginPresenter.areaWiseCalRestricFlg){
//					double squareArea = Double.parseDouble(entity.getArea());
//					priceVal=entity.getPrice()-taxAmt;
//					priceVal=priceVal*squareArea;
//					sum=sum+priceVal;
//				}else{
//					System.out.println("Old code");
//					priceVal=entity.getPrice()-taxAmt;
//					sum=sum+priceVal;
//				}
//			}else if((entity.getPercentageDiscount()!=null)&& (entity.getDiscountAmt()!=0)){
//				if(!entity.getArea().equalsIgnoreCase("NA") && !LoginPresenter.areaWiseCalRestricFlg){
//					priceVal=entity.getPrice()-taxAmt;
//					double squareArea = Double.parseDouble(entity.getArea());
//					priceVal=priceVal*squareArea;
//					priceVal=priceVal-(priceVal*entity.getPercentageDiscount()/100);
//					priceVal=priceVal-entity.getDiscountAmt();
//					sum=sum+priceVal;
//				}else{
//					System.out.println("Old code ===");
//					priceVal=entity.getPrice()-taxAmt;
//					priceVal=priceVal-(priceVal*entity.getPercentageDiscount()/100);
//					priceVal=priceVal-entity.getDiscountAmt();
//					sum=sum+priceVal;
//				}
//			}else{
//				System.out.println("inside oneof the null condition");
//				priceVal=entity.getPrice()-taxAmt;
//				if(entity.getPercentageDiscount()!=null){
//					if(!entity.getArea().equalsIgnoreCase("NA") && !LoginPresenter.areaWiseCalRestricFlg){
//						double squareArea = Double.parseDouble(entity.getArea());
//						priceVal=priceVal*squareArea;
//						priceVal=priceVal-(priceVal*entity.getPercentageDiscount()/100);
//						sum=sum+priceVal;
//					}else{
//						sum=sum+priceVal-(priceVal*entity.getPercentageDiscount()/100);
//					}
//				}else{
//					System.out.println("inside getDiscountAmt oneof the null condition");
//					if(!entity.getArea().equalsIgnoreCase("NA") && !LoginPresenter.areaWiseCalRestricFlg){
//						double squareArea = Double.parseDouble(entity.getArea());
//						priceVal=priceVal*squareArea;
//						priceVal=priceVal-entity.getDiscountAmt();
//						sum=sum+priceVal;
//					}else{
//						sum=sum+priceVal-entity.getDiscountAmt();
//					}
//				}
//			}
//		}
//		return sum;	
		

		List<SalesLineItem>list=getDataprovider().getList();
//		double sum=0;
//		priceVal=0;
		double total=0,origPrice=0;
		for(int i=0;i<list.size();i++){
			SalesLineItem entity=list.get(i);
			SuperProduct prod=entity.getPrduct();
			double taxAmt=removeAllTaxes(prod);
			origPrice = entity.getPrice()-taxAmt;
			if((entity.getPercentageDiscount()==null 
					&& entity.getPercentageDiscount()==0) 
					&& (entity.getDiscountAmt()==0) ){
				if(!entity.getArea().equalsIgnoreCase("NA")){
					double squareArea = Double.parseDouble(entity.getArea());
					origPrice=origPrice*squareArea;
					total+=origPrice;
				}else{
					origPrice = origPrice*entity.getQty();
					total+=origPrice;
				}
			}else if((entity.getPercentageDiscount()!=null)&& (entity.getDiscountAmt()!=0)){
				if(!entity.getArea().equalsIgnoreCase("NA")){
					double squareArea = Double.parseDouble(entity.getArea());
					origPrice=origPrice*squareArea;
					origPrice=origPrice-(origPrice*entity.getPercentageDiscount()/100);
					origPrice=origPrice-entity.getDiscountAmt();
					total+=origPrice;
				}else{
					origPrice=origPrice*entity.getQty();
					origPrice=origPrice-(origPrice*entity.getPercentageDiscount()/100);
					origPrice=origPrice-entity.getDiscountAmt();
					total +=origPrice;
				}
			}else{
				if(entity.getPercentageDiscount()!=null){
					if(!entity.getArea().equalsIgnoreCase("NA")){
						double squareArea = Double.parseDouble(entity.getArea());
						origPrice=origPrice*squareArea;
						origPrice=origPrice-(origPrice*entity.getPercentageDiscount()/100);
						total +=origPrice;
					}else{
						origPrice=origPrice*entity.getQty();
						origPrice=origPrice-(origPrice*entity.getPercentageDiscount()/100);
						total +=origPrice;
					}	
				}else {
					if(!entity.getArea().equalsIgnoreCase("NA")){
						double squareArea = Double.parseDouble(entity.getArea());
						origPrice=origPrice*squareArea;
						origPrice=origPrice*entity.getQty();
						origPrice=origPrice-entity.getDiscountAmt();
						total +=origPrice;
					}else{
						origPrice=origPrice*entity.getQty();
						origPrice=origPrice-entity.getDiscountAmt();
						total +=origPrice;
					}
				}
			}
		}
		return total;
	}
	**/


	public double removeAllTaxes(SuperProduct entity){
		double vat = 0,service = 0;
		double tax=0,retrVat=0,retrServ=0;
		if(entity instanceof ServiceProduct){
			ServiceProduct prod=(ServiceProduct) entity;
			if(prod.getServiceTax()!=null&&prod.getServiceTax().isInclusive()==true){
				service=prod.getServiceTax().getPercentage();
			}
			if(prod.getVatTax()!=null&&prod.getVatTax().isInclusive()==true){
				vat=prod.getVatTax().getPercentage();
			}
		}

		if(entity instanceof ItemProduct){
			ItemProduct prod=(ItemProduct) entity;
			if(prod.getVatTax()!=null&&prod.getVatTax().isInclusive()==true){
				vat=prod.getVatTax().getPercentage();
			}
			if(prod.getServiceTax()!=null&&prod.getServiceTax().isInclusive()==true){
				service=prod.getServiceTax().getPercentage();
			}
		}
		
		if(vat!=0&&service==0){
			retrVat=entity.getPrice()/(1+(vat/100));
			retrVat=entity.getPrice()-retrVat;
		}
		if(service!=0&&vat==0){
			retrServ=entity.getPrice()/(1+service/100);
			retrServ=entity.getPrice()-retrServ;
		}
		if(service!=0&&vat!=0){
			if(entity instanceof ItemProduct){
				ItemProduct prod=(ItemProduct) entity;
				if(prod.getServiceTax().getTaxPrintName()!=null && ! prod.getServiceTax().getTaxPrintName().equals("")
				   && prod.getVatTax().getTaxPrintName()!=null && ! prod.getVatTax().getTaxPrintName().equals("")){
					double dot = service + vat;
					retrServ=(entity.getPrice()/(1+dot/100));
					retrServ=entity.getPrice()-retrServ;
				}else{
					double removeServiceTax=(entity.getPrice()/(1+service/100));
					retrServ=(removeServiceTax/(1+vat/100));
					retrServ=entity.getPrice()-retrServ;
				}
			}
			
			if(entity instanceof ServiceProduct){
				ServiceProduct prod=(ServiceProduct) entity;
				if(prod.getServiceTax().getTaxPrintName()!=null && ! prod.getServiceTax().getTaxPrintName().equals("")
				   && prod.getVatTax().getTaxPrintName()!=null && ! prod.getVatTax().getTaxPrintName().equals("")){
					double dot = service + vat;
					retrServ=(entity.getPrice()/(1+dot/100));
					retrServ=entity.getPrice()-retrServ;
				}else{
					double removeServiceTax=(entity.getPrice()/(1+service/100));
					retrServ=(removeServiceTax/(1+vat/100));
					retrServ=entity.getPrice()-retrServ;
				}
			}
		}
		tax=retrVat+retrServ;
		return tax;
	}
	
	
	public boolean removeChargesOnDelete() {
		int tableSize = getDataprovider().getList().size();
		if (tableSize < 1) {
			return false;
		}
		return true;
	}
		
	public int identifyProductId() {
		return productIdCount;
	}

	public int identifySrNo() {
		return productSrNoCount;
	}

}
