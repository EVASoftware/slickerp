package com.slicktechnologies.client.views.purchase.servicepo;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessprocesslayer.ServicePo;

public class ServicePoPresenterTableProxy extends SuperTable<ServicePo> {
	
	TextColumn<ServicePo> getSerPoIdCol;
	TextColumn<ServicePo> getSerPoNameCol;
	TextColumn<ServicePo> getSerPoDateCol;
	TextColumn<ServicePo> getExpDelDateCol;
	TextColumn<ServicePo> getVendorIdCol;
	TextColumn<ServicePo> getVendorNameCol;
	TextColumn<ServicePo> getVendorCellCol;
	TextColumn<ServicePo> getPurchaseEnggCol;
	TextColumn<ServicePo> getApproverCol;
	TextColumn<ServicePo> getBranchCol;
	TextColumn<ServicePo> getStatusCol;
	
	

	public ServicePoPresenterTableProxy() {
		super();
	}
	
	

	@Override
	public void createTable() {
		getSerPoIdCol();
		getSerPoNameCol();
		getSerPoDateCol();
		getExpDelDateCol();
		getVendorIdCol();
		getVendorNameCol();
		getVendorCellCol();
		getPurchaseEnggCol();
		getApproverCol();
		getBranchCol();
		getStatusCol();
	}
	
	private void getSerPoIdCol() {
		getSerPoIdCol=new TextColumn<ServicePo>() {
			@Override
			public String getValue(ServicePo object) {
				return object.getCount()+"";
			}
		};
		table.addColumn(getSerPoIdCol, "Po Id");
		getSerPoIdCol.setSortable(true);
	}

	private void getSerPoNameCol() {
		getSerPoNameCol=new TextColumn<ServicePo>() {
			@Override
			public String getValue(ServicePo object) {
				return object.getServicePoTitle()+"";
			}
		};
		table.addColumn(getSerPoNameCol, "Po Title");
		getSerPoNameCol.setSortable(true);
		
	}

	private void getSerPoDateCol() {
		getSerPoDateCol=new TextColumn<ServicePo>() {
			@Override
			public String getValue(ServicePo object) {
				if(object.getServicePoDate()!=null){
					return AppUtility.parseDate(object.getServicePoDate());
				}
				return "";
			}
		};
		table.addColumn(getSerPoDateCol, "Po Date");
		getSerPoDateCol.setSortable(true);
		
	}

	private void getExpDelDateCol() {
		getExpDelDateCol=new TextColumn<ServicePo>() {
			@Override
			public String getValue(ServicePo object) {
				if(object.getExpDeliveryDate()!=null){
					return AppUtility.parseDate(object.getExpDeliveryDate());
				}
				return "";
			}
		};
		table.addColumn(getExpDelDateCol, "Exp. Del. Date");
		getExpDelDateCol.setSortable(true);
		
	}

	private void getVendorIdCol() {
		getVendorIdCol=new TextColumn<ServicePo>() {
			@Override
			public String getValue(ServicePo object) {
				if(object.getVinfo().getCount()!=0){
					return object.getVinfo().getCount()+"";
				}
				return "";
			}
		};
		table.addColumn(getVendorIdCol, "Vendor Id");
		getVendorIdCol.setSortable(true);
		
	}

	private void getVendorNameCol() {
		getVendorNameCol=new TextColumn<ServicePo>() {
			@Override
			public String getValue(ServicePo object) {
				if(object.getVinfo().getFullName()!=null){
					return object.getVinfo().getFullName();
				}
				return "";
			}
		};
		table.addColumn(getVendorNameCol, "Vendor Name");
		getVendorNameCol.setSortable(true);
		
	}

	private void getVendorCellCol() {
		getVendorCellCol=new TextColumn<ServicePo>() {
			@Override
			public String getValue(ServicePo object) {
				if(object.getVinfo().getCellNumber()!=null){
					return object.getVinfo().getCellNumber()+"";
				}
				return "";
			}
		};
		table.addColumn(getVendorCellCol, "Vendor Cell");
		getVendorCellCol.setSortable(true);
		
	}

	private void getPurchaseEnggCol() {
		getPurchaseEnggCol=new TextColumn<ServicePo>() {
			@Override
			public String getValue(ServicePo object) {
				if(object.getEmployee()!=null){
					return object.getEmployee()+"";
				}
				return "";
			}
		};
		table.addColumn(getPurchaseEnggCol, "Purchase Engineer");
		getPurchaseEnggCol.setSortable(true);
		
	}

	private void getApproverCol() {
		getApproverCol=new TextColumn<ServicePo>() {
			@Override
			public String getValue(ServicePo object) {
				if(object.getApproverName()!=null){
					return object.getApproverName()+"";
				}
				return "";
			}
		};
		table.addColumn(getApproverCol, "Approver");
		getApproverCol.setSortable(true);
		
	}

	private void getBranchCol() {
		getBranchCol=new TextColumn<ServicePo>() {
			@Override
			public String getValue(ServicePo object) {
				if(object.getBranch()!=null){
					return object.getBranch()+"";
				}
				return "";
			}
		};
		table.addColumn(getBranchCol, "Branch");
		getBranchCol.setSortable(true);
		
	}

	private void getStatusCol() {
		getStatusCol=new TextColumn<ServicePo>() {
			@Override
			public String getValue(ServicePo object) {
				if(object.getStatus()!=null){
					return object.getStatus()+"";
				}
				return "";
			}
		};
		table.addColumn(getStatusCol, "Status");
		getStatusCol.setSortable(true);
		
	}
	
	
	
	
	

	@Override
	public void addColumnSorting() {
		addSortingToIdCol();
		addSortingToTitleCol();
		addSortingToDateCol();
		addSortingToExpDelDateCol();
		addSortingToVenIdCol();
		addSortingToVenNameCol();
		addSortingToVenCellCol();
		addSortingToPurchaseEnggCol();
		addSortingToApproverCol();
		addSortingToBranchCol();
		addSortingToStatusCol();
	}



	private void addSortingToIdCol() {
		List<ServicePo> list = getDataprovider().getList();
		columnSort = new ListHandler<ServicePo>(list);
		columnSort.setComparator(getSerPoIdCol, new Comparator<ServicePo>() {
			@Override
			public int compare(ServicePo e1, ServicePo e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCount() == e2.getCount()) {
						return 0;
					}
					if (e1.getCount() > e2.getCount()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}



	private void addSortingToTitleCol() {
		List<ServicePo> list = getDataprovider().getList();
		columnSort = new ListHandler<ServicePo>(list);
		columnSort.setComparator(getSerPoNameCol,new Comparator<ServicePo>() {
			@Override
			public int compare(ServicePo e1, ServicePo e2) {
				if (e1 != null && e2 != null) {
					if (e1.getServicePoTitle() != null&& e2.getServicePoTitle() != null) {
						return e1.getServicePoTitle().compareTo(e2.getServicePoTitle());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}



	private void addSortingToDateCol() {
		List<ServicePo> list = getDataprovider().getList();
		columnSort = new ListHandler<ServicePo>(list);
		columnSort.setComparator(getSerPoNameCol,new Comparator<ServicePo>() {
			@Override
			public int compare(ServicePo e1, ServicePo e2) {
				if (e1 != null && e2 != null) {
					if (e1.getServicePoTitle() != null&& e2.getServicePoTitle() != null) {
						return e1.getServicePoTitle().compareTo(e2.getServicePoTitle());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}



	private void addSortingToExpDelDateCol() {
		List<ServicePo> list = getDataprovider().getList();
		columnSort = new ListHandler<ServicePo>(list);
		columnSort.setComparator(getSerPoNameCol,new Comparator<ServicePo>() {
			@Override
			public int compare(ServicePo e1, ServicePo e2) {
				if (e1 != null && e2 != null) {
					if (e1.getServicePoTitle() != null&& e2.getServicePoTitle() != null) {
						return e1.getServicePoTitle().compareTo(e2.getServicePoTitle());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}



	private void addSortingToVenIdCol() {
		List<ServicePo> list = getDataprovider().getList();
		columnSort = new ListHandler<ServicePo>(list);
		columnSort.setComparator(getSerPoIdCol, new Comparator<ServicePo>() {
			@Override
			public int compare(ServicePo e1, ServicePo e2) {
				if (e1 != null && e2 != null) {
					if (e1.getVinfo().getCellNumber() == e2.getVinfo().getCellNumber()) {
						return 0;
					}
					if (e1.getVinfo().getCellNumber() > e2.getVinfo().getCellNumber()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}



	private void addSortingToVenNameCol() {
		List<ServicePo> list = getDataprovider().getList();
		columnSort = new ListHandler<ServicePo>(list);
		columnSort.setComparator(getVendorNameCol,new Comparator<ServicePo>() {
			@Override
			public int compare(ServicePo e1, ServicePo e2) {
				if (e1 != null && e2 != null) {
					if (e1.getVinfo().getFullName() != null&& e2.getVinfo().getFullName() != null) {
						return e1.getVinfo().getFullName().compareTo(e2.getVinfo().getFullName());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}



	private void addSortingToVenCellCol() {
		List<ServicePo> list = getDataprovider().getList();
		columnSort = new ListHandler<ServicePo>(list);
		columnSort.setComparator(getVendorCellCol, new Comparator<ServicePo>() {
			@Override
			public int compare(ServicePo e1, ServicePo e2) {
				if (e1 != null && e2 != null) {
					if (e1.getVinfo().getCount() == e2.getVinfo().getCount()) {
						return 0;
					}
					if (e1.getVinfo().getCount() > e2.getVinfo().getCount()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}



	private void addSortingToPurchaseEnggCol() {
		List<ServicePo> list = getDataprovider().getList();
		columnSort = new ListHandler<ServicePo>(list);
		columnSort.setComparator(getPurchaseEnggCol,new Comparator<ServicePo>() {
			@Override
			public int compare(ServicePo e1, ServicePo e2) {
				if (e1 != null && e2 != null) {
					if (e1.getEmployee() != null&& e2.getEmployee() != null) {
						return e1.getEmployee().compareTo(e2.getEmployee());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}



	private void addSortingToApproverCol() {
		List<ServicePo> list = getDataprovider().getList();
		columnSort = new ListHandler<ServicePo>(list);
		columnSort.setComparator(getApproverCol,new Comparator<ServicePo>() {
			@Override
			public int compare(ServicePo e1, ServicePo e2) {
				if (e1 != null && e2 != null) {
					if (e1.getApproverName() != null&& e2.getApproverName() != null) {
						return e1.getApproverName().compareTo(e2.getApproverName());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}



	private void addSortingToBranchCol() {
		List<ServicePo> list = getDataprovider().getList();
		columnSort = new ListHandler<ServicePo>(list);
		columnSort.setComparator(getBranchCol,new Comparator<ServicePo>() {
			@Override
			public int compare(ServicePo e1, ServicePo e2) {
				if (e1 != null && e2 != null) {
					if (e1.getBranch() != null&& e2.getBranch() != null) {
						return e1.getBranch().compareTo(e2.getBranch());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}



	private void addSortingToStatusCol() {
		List<ServicePo> list = getDataprovider().getList();
		columnSort = new ListHandler<ServicePo>(list);
		columnSort.setComparator(getStatusCol,new Comparator<ServicePo>() {
			@Override
			public int compare(ServicePo e1, ServicePo e2) {
				if (e1 != null && e2 != null) {
					if (e1.getStatus() != null&& e2.getStatus() != null) {
						return e1.getStatus().compareTo(e2.getStatus());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}



	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
