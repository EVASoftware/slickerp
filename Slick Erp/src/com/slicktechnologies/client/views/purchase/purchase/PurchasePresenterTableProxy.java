package com.slicktechnologies.client.views.purchase.purchase;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;
import java.util.Comparator;
import com.google.gwt.cell.client.FieldUpdater;
import java.util.List;
import java.util.Date;
import com.google.gwt.user.cellview.client.Column;
import  com.google.gwt.cell.client.NumberCell;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import  com.google.gwt.cell.client.DatePickerCell;
import com.slicktechnologies.shared.Purchase;
import com.slicktechnologies.client.views.purchase.purchase.PurchasePresenter.PurchasePresenterTable;

public class PurchasePresenterTableProxy extends PurchasePresenterTable {
	Column<Purchase,Date> getPoDateColumn;
	TextColumn<Purchase> getPoNrColumn;
	Column<Purchase,Number> getPoValueColumn;
	TextColumn<Purchase> getVendorFullNameColumn;
	TextColumn<Purchase> getBranchColumn;
	TextColumn<Purchase> getEmployeeColumn;
	TextColumn<Purchase> getStatusColumn;
	Column<Purchase,Date> getCreationDateColumn;
	TextColumn<Purchase> getGroupColumn;
	TextColumn<Purchase> getCategoryColumn;
	TextColumn<Purchase> getTypeColumn;
	TextColumn<Purchase> getCountColumn;
	
	public Object getVarRef(String varName)
	{
		if(varName.equals("getPoDateColumn"))
			return this.getPoDateColumn;
		if(varName.equals("getPoNrColumn"))
			return this.getPoNrColumn;
		if(varName.equals("getPoValueColumn"))
			return this.getPoValueColumn;
		if(varName.equals("getVendorFullNameColumn"))
			return this.getVendorFullNameColumn;
		if(varName.equals("getBranchColumn"))
			return this.getBranchColumn;
		if(varName.equals("getEmployeeColumn"))
			return this.getEmployeeColumn;
		if(varName.equals("getStatusColumn"))
			return this.getStatusColumn;
		if(varName.equals("getCreationDateColumn"))
			return this.getCreationDateColumn;
		if(varName.equals("getGroupColumn"))
			return this.getGroupColumn;
		if(varName.equals("getCategoryColumn"))
			return this.getCategoryColumn;
		if(varName.equals("getTypeColumn"))
			return this.getTypeColumn;
		if(varName.equals("getCountColumn"))
			return this.getCountColumn;
		return null ;
	}
	public PurchasePresenterTableProxy()
	{
		super();
	}
	@Override public void createTable() {
		addColumngetCount();
		addColumngetCreationDate();
		addColumngetBranch();
		addColumngetEmployee();
		addColumngetVendorFullName();
		addColumngetStatus();
		addColumngetPoNr();
		//addColumngetGroup();
		addColumngetPoValue();
		//addColumngetCategory();
		addColumngetPoDate();
		//addColumngetType();
	}
	@Override
	protected void initializekeyprovider() {
		keyProvider= new ProvidesKey<Purchase>()
				{
			@Override
			public Object getKey(Purchase item)
			{
				if(item==null)
				{
					return null;
				}
				else
					return item.getId();
			}
				};
	}
	@Override
	public void setEnable(boolean state)
	{
	}
	@Override
	public void applyStyle()
	{
	}
	public void addColumnSorting(){
		addSortinggetBranch();
		addSortinggetEmployee();
		addSortinggetVendorFullName();
		addSortinggetStatus();
		addSortinggetPoNr();
		addSortinggetGroup();
		addSortinggetPoValue();
		addSortinggetCategory();
		addSortinggetPoDate();
		addSortinggetCreationDate();
		addSortinggetType();
	}
	@Override public void addFieldUpdater() {
	}
	protected void addColumngetCount()
	{
		getCountColumn=new TextColumn<Purchase>()
				{
			@Override
			public String getValue(Purchase object)
			{
				if( object.getCount()==-1)
					return "N.A";
				else return object.getCount()+"";
			}
				};
				table.addColumn(getCountColumn,"Id");
				getCountColumn.setSortable(true);
	}
	protected void addColumngetCreationDate()
	{
		DateTimeFormat fmt = DateTimeFormat.getFormat(PredefinedFormat.DATE_SHORT);
		DatePickerCell date= new DatePickerCell(fmt);
		getCreationDateColumn=new Column<Purchase,Date>(date)
				{
			@Override
			public Date getValue(Purchase object)
			{
				if(object.getCreationDate()!=null){
					return object.getCreationDate();}
				else{
					object.setCreationDate(new Date());
					return new Date();
				}
			}
				};
				table.addColumn(getCreationDateColumn,"Creation Date");
				getCreationDateColumn.setSortable(true);
	}
	protected void addFieldUpdatergetCreationDate()
	{
		getCreationDateColumn.setFieldUpdater(new FieldUpdater<Purchase, Date>()
				{
			@Override
			public void update(int index,Purchase object,Date value)
			{
				object.setCreationDate(value);
				table.redrawRow(index);
			}
				});
	}
	protected void addSortinggetBranch()
	{
		List<Purchase> list=getDataprovider().getList();
		columnSort=new ListHandler<Purchase>(list);
		columnSort.setComparator(getBranchColumn, new Comparator<Purchase>()
				{
			@Override
			public int compare(Purchase e1,Purchase e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBranch()!=null && e2.getBranch()!=null){
						return e1.getBranch().compareTo(e2.getBranch());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetBranch()
	{
		getBranchColumn=new TextColumn<Purchase>()
				{
			@Override
			public String getValue(Purchase object)
			{
				return object.getBranch()+"";
			}
				};
				table.addColumn(getBranchColumn,"Branch");
				getBranchColumn.setSortable(true);
	}
	protected void addSortinggetEmployee()
	{
		List<Purchase> list=getDataprovider().getList();
		columnSort=new ListHandler<Purchase>(list);
		columnSort.setComparator(getEmployeeColumn, new Comparator<Purchase>()
				{
			@Override
			public int compare(Purchase e1,Purchase e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getEmployee()!=null && e2.getEmployee()!=null){
						return e1.getEmployee().compareTo(e2.getEmployee());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetEmployee()
	{
		getEmployeeColumn=new TextColumn<Purchase>()
				{
			@Override
			public String getValue(Purchase object)
			{
				return object.getEmployee()+"";
			}
				};
				table.addColumn(getEmployeeColumn,"Sales Person");
				getEmployeeColumn.setSortable(true);
	}
	protected void addSortinggetVendorFullName()
	{
		List<Purchase> list=getDataprovider().getList();
		columnSort=new ListHandler<Purchase>(list);
		columnSort.setComparator(getVendorFullNameColumn, new Comparator<Purchase>()
				{
			@Override
			public int compare(Purchase e1,Purchase e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getVendorFullName()!=null && e2.getVendorFullName()!=null){
						return e1.getVendorFullName().compareTo(e2.getVendorFullName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetVendorFullName()
	{
		getVendorFullNameColumn=new TextColumn<Purchase>()
				{
			@Override
			public String getValue(Purchase object)
			{
				return object.getVendorFullName()+"";
			}
				};
				table.addColumn(getVendorFullNameColumn,"Vendor");
				getVendorFullNameColumn.setSortable(true);
	}
	protected void addSortinggetStatus()
	{
		List<Purchase> list=getDataprovider().getList();
		columnSort=new ListHandler<Purchase>(list);
		columnSort.setComparator(getStatusColumn, new Comparator<Purchase>()
				{
			@Override
			public int compare(Purchase e1,Purchase e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getStatus()!=null && e2.getStatus()!=null){
						return e1.getStatus().compareTo(e2.getStatus());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetStatus()
	{
		getStatusColumn=new TextColumn<Purchase>()
				{
			@Override
			public String getValue(Purchase object)
			{
				return object.getStatus()+"";
			}
				};
				table.addColumn(getStatusColumn,"Status");
				getStatusColumn.setSortable(true);
	}
	protected void addSortinggetPoNr()
	{
		List<Purchase> list=getDataprovider().getList();
		columnSort=new ListHandler<Purchase>(list);
		columnSort.setComparator(getPoNrColumn, new Comparator<Purchase>()
				{
			@Override
			public int compare(Purchase e1,Purchase e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getPoNr()!=null && e2.getPoNr()!=null){
						return e1.getPoNr().compareTo(e2.getPoNr());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetPoNr()
	{
		getPoNrColumn=new TextColumn<Purchase>()
				{
			@Override
			public String getValue(Purchase object)
			{
				return object.getPoNr()+"";
			}
				};
				table.addColumn(getPoNrColumn,"PO Nr");
				getPoNrColumn.setSortable(true);
	}
	protected void addSortinggetGroup()
	{
		List<Purchase> list=getDataprovider().getList();
		columnSort=new ListHandler<Purchase>(list);
		columnSort.setComparator(getGroupColumn, new Comparator<Purchase>()
				{
			@Override
			public int compare(Purchase e1,Purchase e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getGroup()!=null && e2.getGroup()!=null){
						return e1.getGroup().compareTo(e2.getGroup());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetGroup()
	{
		getGroupColumn=new TextColumn<Purchase>()
				{
			@Override
			public String getValue(Purchase object)
			{
				return object.getGroup()+"";
			}
				};
				table.addColumn(getGroupColumn,"Group");
				getGroupColumn.setSortable(true);
	}
	protected void addSortinggetPoValue()
	{
		List<Purchase> list=getDataprovider().getList();
		columnSort=new ListHandler<Purchase>(list);
		columnSort.setComparator(getPoValueColumn, new Comparator<Purchase>()
				{
			@Override
			public int compare(Purchase e1,Purchase e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getPoValue()== e2.getPoValue()){
						return 0;}
					if(e1.getPoValue()> e2.getPoValue()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetPoValue()
	{
		NumberCell editCell=new NumberCell();
		getPoValueColumn=new Column<Purchase,Number>(editCell)
				{
			@Override
			public Number getValue(Purchase object)
			{
				if(object.getPoValue()==0){
					return 0;}
				else{
					return object.getPurchasedProducts().get(0).getPrice();}
			}
				};
				table.addColumn(getPoValueColumn,"PO Value");
				getPoValueColumn.setSortable(true);
	}
	protected void addFieldUpdatergetPoValue()
	{
		getPoValueColumn.setFieldUpdater(new FieldUpdater<Purchase, Number>()
				{
			@Override
			public void update(int index,Purchase object,Number value)
			{
				Double val1=(Double) value;
				object.setPoValue(val1);
				table.redrawRow(index);
			}
				});
	}
	protected void addSortinggetCategory()
	{
		List<Purchase> list=getDataprovider().getList();
		columnSort=new ListHandler<Purchase>(list);
		columnSort.setComparator(getCategoryColumn, new Comparator<Purchase>()
				{
			@Override
			public int compare(Purchase e1,Purchase e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getCategory()!=null && e2.getCategory()!=null){
						return e1.getCategory().compareTo(e2.getCategory());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetCategory()
	{
		getCategoryColumn=new TextColumn<Purchase>()
				{
			@Override
			public String getValue(Purchase object)
			{
				return object.getCategory()+"";
			}
				};
				table.addColumn(getCategoryColumn,"Category");
				getCategoryColumn.setSortable(true);
	}
	protected void addSortinggetPoDate()
	{
		List<Purchase> list=getDataprovider().getList();
		columnSort=new ListHandler<Purchase>(list);
		columnSort.setComparator(getPoDateColumn, new Comparator<Purchase>()
				{
			@Override
			public int compare(Purchase e1,Purchase e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getPoDate()!=null && e2.getPoDate()!=null){
						return e1.getPoDate().compareTo(e2.getPoDate());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	
	protected void addSortinggetCreationDate()
	{
		List<Purchase> list=getDataprovider().getList();
		columnSort=new ListHandler<Purchase>(list);
		columnSort.setComparator(getCreationDateColumn, new Comparator<Purchase>()
				{
			@Override
			public int compare(Purchase e1,Purchase e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getPoDate()!=null && e2.getPoDate()!=null){
						return e1.getCreationDate().compareTo(e2.getCreationDate());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addColumngetPoDate()
	{
		DateTimeFormat fmt = DateTimeFormat.getFormat(PredefinedFormat.DATE_SHORT);
		DatePickerCell date= new DatePickerCell(fmt);
		getPoDateColumn=new Column<Purchase,Date>(date)
				{
			@Override
			public Date getValue(Purchase object)
			{
				if(object.getCreationDate()!=null){
					return object.getPoDate();}
				else{
					object.setPoDate(new Date());
					return new Date();
				}
			}
				};
				table.addColumn(getPoDateColumn,"PO Date");
				getPoDateColumn.setSortable(true);
	}
	protected void addSortinggetType()
	{
		List<Purchase> list=getDataprovider().getList();
		columnSort=new ListHandler<Purchase>(list);
		columnSort.setComparator(getTypeColumn, new Comparator<Purchase>()
				{
			@Override
			public int compare(Purchase e1,Purchase e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getType()!=null && e2.getType()!=null){
						return e1.getType().compareTo(e2.getType());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetType()
	{
		getTypeColumn=new TextColumn<Purchase>()
				{
			@Override
			public String getValue(Purchase object)
			{
				return object.getType()+"";
			}
				};
				table.addColumn(getTypeColumn,"Type");
				getTypeColumn.setSortable(true);
	}
	
	
}
