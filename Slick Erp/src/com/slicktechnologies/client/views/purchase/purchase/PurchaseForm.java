package com.slicktechnologies.client.views.purchase.purchase;
import java.util.Vector;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.OneToManyComposite;
import com.simplesoftwares.client.library.composite.PaymentComposite;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.Purchase;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productlayer.Category;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;


public class PurchaseForm extends FormScreen<Purchase> implements ClickHandler {

	//***********************************Variable Declaration Starts********************************************//
	//Token to add the varialble declarations
	OneToManyComposite<ConfigCategory,ItemProduct,SalesLineItem> otmComposite;
	DateBox dbPurchaseOrderDate;
	ObjectListBox<Branch> olbbBranch;
	TextBox tbPurchaseOrderNo;
	TextArea taDescription;
	PersonInfoComposite picVendor;
	ObjectListBox<Employee> olbePurchaseEngineer;

	SalesLineItemTable saleslineitempurchasetable;

	PaymentComposite paymentComposite;
	//***********************************Variable Declaration Ends********************************************//

	public  PurchaseForm() {
		super();
		createGui();
		this.setDisabled(paymentComposite);
		this.paymentComposite.setWidth("200px");
		this.paymentComposite.getElement().addClassName("footer-grid");
	}

	public PurchaseForm  (String[] processlevel, FormField[][] fields,
			FormStyle formstyle) 
	{
		super(processlevel, fields, formstyle);
		createGui();


	}

	
	//***********************************Variable Initialization********************************************//

	/**
	 * Method template to initialize the declared variables.
	 */
	private void initalizeWidget()
	{
		saleslineitempurchasetable=new SalesLineItemTable();
		otmComposite=new OneToManyComposite<ConfigCategory,ItemProduct,SalesLineItem>(saleslineitempurchasetable);
		//AppUtility.<ConfigCategory,ItemProduct,SalesLineItem>initiateOneToManyComposite(otmComposite,this);
		initalizeCategoryComboBox();
		dbPurchaseOrderDate=new DateBoxWithYearSelector();


		olbbBranch=new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbbBranch);

		tbPurchaseOrderNo=new TextBox();

		taDescription=new TextArea();

		
		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new Vendor());
		picVendor=new PersonInfoComposite(querry);
		
		olbePurchaseEngineer=new ObjectListBox<Employee>();
		AppUtility.makeSalesPersonListBoxLive(olbePurchaseEngineer);

		paymentComposite=new PaymentComposite();

	}

	/**
	 * method template to create screen formfields
	 */
	@Override
	public void createScreen() {


		initalizeWidget();

		//Token to initialize the processlevel menus.


		this.processlevelBarNames=new String[]{"New"};

		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////
		//Token to initialize formfield
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingPurchaseInformation=fbuilder.setlabel("Purchase Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("",picVendor);
		FormField fpicVendor= fbuilder.setMandatory(true).setMandatoryMsg("Vendor Id is Mandatory!").setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Purchase Order No",tbPurchaseOrderNo);
		FormField ftbPurchaseOrderNo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Purchase Order Date",dbPurchaseOrderDate);
		FormField fdbPurchaseOrderDate= fbuilder.setMandatory(true).setMandatoryMsg("Purchase Order Date is Mandatory!").setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("* Branch",olbbBranch);
		FormField folbbBranch= fbuilder.setMandatory(true).setMandatoryMsg("Branch is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Purchase Engineer",olbePurchaseEngineer);
		FormField folbePurchaseEngineer= fbuilder.setMandatory(true).setMandatoryMsg("Purchase Engineer is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingDescription=fbuilder.setlabel("Description").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Description",taDescription);
		FormField ftaDescription= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingItemInformation=fbuilder.setlabel("Item Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("",otmComposite);
		FormField fotmComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();

		fbuilder = new FormFieldBuilder("",paymentComposite);
		FormField fpaymentComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////


		FormField[][] formfield = {   {fgroupingPurchaseInformation},
				{fpicVendor},
				{ftbPurchaseOrderNo,fdbPurchaseOrderDate,folbbBranch,folbePurchaseEngineer},
				{fgroupingDescription},
				{ftaDescription},
				{fgroupingItemInformation},
				{fotmComposite},
				{fpaymentComposite}
		};

		this.fields=formfield;

	}

	/**
	 * method template to update the model with token entity name
	 */
	@Override
	public void updateModel(Purchase model) 
	{

		if(picVendor.getValue()!=null)
			model.setVendorInfo(picVendor.getValue());
		if(tbPurchaseOrderNo.getValue()!=null)
			model.setPoNr(tbPurchaseOrderNo.getValue());
		if(dbPurchaseOrderDate.getValue()!=null)
			model.setPoDate(dbPurchaseOrderDate.getValue());
		if(olbbBranch.getValue()!=null)
			model.setBranch(olbbBranch.getValue());
		if(olbePurchaseEngineer.getValue()!=null)
			model.setEmployee(olbePurchaseEngineer.getValue());
		if(taDescription.getValue()!=null)
			model.setDescription(taDescription.getValue());
		if(otmComposite.getValue()!=null)
			model.setPurchasedProducts(otmComposite.getValue());
		if(paymentComposite.getTotalAmt().getValue()!=null)
			model.setPoValue(paymentComposite.getTotalAmt().getValue());

		presenter.setModel(model);

	}

	/**
	 * method template to update the view with token entity name
	 */
	@Override
	public void updateView(Purchase view) 
	{


		if(view.getVendorInfo()!=null)
			picVendor.setValue(view.getVendorInfo());
		if(view.getPoNr()!=null)
			tbPurchaseOrderNo.setValue(view.getPoNr());
		if(view.getPoDate()!=null)
			dbPurchaseOrderDate.setValue(view.getPoDate());
		if(view.getBranch()!=null)
			olbbBranch.setValue(view.getBranch());
		if(view.getEmployee()!=null)
			olbePurchaseEngineer.setValue(view.getEmployee());
		if(view.getDescription()!=null)
			taDescription.setValue(view.getDescription());
		if(view.getPurchasedProducts()!=null)
			otmComposite.setValue(view.getPurchasedProducts());
		
		paymentComposite.getTotalAmt().setValue(view.getPoValue());
		presenter.setModel(view);
	}

	// Hand written code shift in presenter


	/**
	 * Toggles the app header bar menus as per screen state
	 */

	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals("Search"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit")||text.equals("Search"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.PURCHASE,LoginPresenter.currentModule.trim());
	}

	/**
	 * sets the id textbox with the passed count value. 
	 */
	@Override
	public void setCount(int count)
	{

	}


	@Override
	public void onClick(ClickEvent event) {
		AppUtility.ReactOnAddForOneToManyComposite(otmComposite);

	}


	public void setDisabled(PaymentComposite payComposite)
	{
		paymentComposite.advance.setVisible(false);
		paymentComposite.credidamt.setVisible(false);
		paymentComposite.balance.setVisible(false);
		paymentComposite.bankbranch.setVisible(false);
		paymentComposite.bankname.setVisible(false);
		paymentComposite.cashamt.setVisible(false);
		paymentComposite.cheqamt.setVisible(false);
		paymentComposite.chequeDate.setVisible(false);
		paymentComposite.chequenumber.setVisible(false);
		paymentComposite.credidamt.setVisible(false);
		paymentComposite.totalAmt.setEnabled(false);
		paymentComposite.netPayable.setVisible(false);
		paymentComposite.flatdiscount.setVisible(false);
		paymentComposite.percentageDiscount.setVisible(false);
		paymentComposite.check(paymentComposite.form);


		for(int i=0;i<paymentComposite.form.getRowCount();i++)
		{
			int  colCount=paymentComposite.form.getCellCount(i);
			for(int j=0;j<colCount;j++)
			{

				String requiredWidget=paymentComposite.form.getWidget(i, j).toString();
				if(requiredWidget.contains("Total Amount"))
				{
					paymentComposite.form.getWidget(i, j).setVisible(true);
				}
			}
		}
	}

	public SalesLineItemTable getSaleslineitempurchasetable() {
		return saleslineitempurchasetable;
	}

	public void setSaleslineitempurchasetable(
			SalesLineItemTable saleslineitempurchasetable) {
		this.saleslineitempurchasetable = saleslineitempurchasetable;
	}

	public PaymentComposite getPaymentComposite() {
		return paymentComposite;
	}

	public void setPaymentComposite(PaymentComposite paymentComposite) {
		this.paymentComposite = paymentComposite;
	}



	public void initalizeCategoryComboBox()
	{
		//System.out.println("Calllllllllllllllll");
		
		Filter categoryFilter = new Filter();
		categoryFilter.setQuerryString("isSellCategory");
		categoryFilter.setBooleanvalue(true);
		Vector<Filter> catvecfilter=new Vector<Filter>();
		catvecfilter.add(categoryFilter);
		MyQuerry querry=new MyQuerry(catvecfilter, new Category());
		
		   Filter filtervalue=new Filter();
		   String querrycon="categoryKey";
		   filtervalue.setQuerryString(querrycon);
		   Vector<Filter> manyvecfilter=new Vector<Filter>();
		   manyvecfilter.add(filtervalue);
		   MyQuerry manyquerry=new MyQuerry(manyvecfilter, new SuperProduct());
		  
		   otmComposite.setOneQuerry(querry);
		   otmComposite.setManyQuerry(manyquerry);
		 otmComposite.initializeHashMaps();
		 otmComposite.addClickHandler(this);

	}
	
	@Override
	 public boolean validate()
	    {
	       
	        
	        if(saleslineitempurchasetable.getDataprovider().getList().size() == 0)
	        {
	            showDialogMessage("Please Select at least one Product!");
	            return false;
	        }
	        boolean superValidate = super.validate();
	        if(superValidate==false)
	        {
	        	System.out.println("9999999999999999");
	        	return false;
	        }
	       
	        return true;
	    }
}
