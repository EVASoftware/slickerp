package com.slicktechnologies.client.views.purchase.purchase;

import java.util.Vector;

import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.purchase.purchase.PurchasePresenter.PurchasePresenterSearch;
import com.slicktechnologies.shared.Purchase;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.personlayer.Employee;

public class PurchasePresenterSearchProxy extends PurchasePresenterSearch {
	
	public DateComparator dateBoxPODate;
	public TextBox tbPONr;
	public ObjectListBox<Employee> olbPurchaseEngg;
	public ObjectListBox<Branch> olbBranch;
	public ObjectListBox<Employee> olbEmployee;
	public PersonInfoComposite vendor;
	//public DateComparator dateComparator;
	public Object getVarRef(String varName)
	{
		if(varName.equals("dateBoxPODate"))
			return this.dateBoxPODate;
		if(varName.equals("tbPONr"))
			return this.tbPONr;
		if(varName.equals("olbPurchaseEngg"))
			return this.olbPurchaseEngg;
		if(varName.equals("olbBranch"))
			return this.olbBranch;
		if(varName.equals("olbEmployee"))
			return this.olbEmployee;
		
		return null ;
	}
	public PurchasePresenterSearchProxy()
	{
		super();
		createGui();
	}
	public void initWidget()
	{
		dateBoxPODate= new DateComparator("poDate",new Purchase());
		tbPONr= new TextBox();
		olbPurchaseEngg= new ObjectListBox<Employee>();
		AppUtility.makeSalesPersonListBoxLive(olbPurchaseEngg);
		olbBranch= new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbBranch);
		olbEmployee= new ObjectListBox<Employee>();
		AppUtility.makeSalesPersonListBoxLive(olbEmployee);
		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new Vendor());
		vendor=new PersonInfoComposite(querry,false);
		
	}
	public void createScreen()
	{
		initWidget();
		FormFieldBuilder builder;
		builder = new FormFieldBuilder("From Date(PO Date)",dateBoxPODate.getFromDate());
		FormField ffpoDate= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("To Date(PO Date)",dateBoxPODate.getToDate());
		FormField ftpoDate= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("PO Nr",tbPONr);
		FormField ftbPONr= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Purchase Engineer",olbPurchaseEngg);
		FormField folbPurchaseEngg= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Branch",olbBranch);
		FormField folbBranch= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Employee",olbEmployee);
		FormField folbEmployee= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
	
		this.fields=new FormField[][]{
				{ffpoDate,ftpoDate,ftbPONr,},
				{folbEmployee,folbPurchaseEngg,folbBranch}
		};
	}
	public MyQuerry getQuerry()
	{
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		if(dateBoxPODate.getValue()!=null)
		{
			filtervec.addAll(dateBoxPODate.getValue());
		}
		
		if(olbEmployee.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbEmployee.getValue().trim());
			temp.setQuerryString("employee");
			filtervec.add(temp);
		}
		if(olbPurchaseEngg.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbPurchaseEngg.getValue().trim());
			temp.setQuerryString("employee");
			filtervec.add(temp);
		}
		if(olbBranch.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbBranch.getValue().trim());
			temp.setQuerryString("branch");
			filtervec.add(temp);
		}
		if(tbPONr.getValue().trim().equals("")==false){
			temp=new Filter();temp.setStringValue(tbPONr.getValue().trim());
			filtervec.add(temp);
			temp.setQuerryString("poNr");
			filtervec.add(temp);
		}
		if(vendor.getIdValue()!=-1)
		  {
		  temp=new Filter();
		  temp.setIntValue(vendor.getIdValue());
		  temp.setQuerryString("vendorInfo.count");
		  filtervec.add(temp);
		  }
		  
		  if(!(vendor.getFullNameValue().equals("")))
		  {
		  temp=new Filter();
		  temp.setStringValue(vendor.getFullNameValue());
		  temp.setQuerryString("vendorInfo.fullName");
		  filtervec.add(temp);
		  }
		  if(vendor.getCellValue()!=-1l)
		  {
		  temp=new Filter();
		  temp.setLongValue(vendor.getCellValue());
		  temp.setQuerryString("vendorInfo.cellNumber");
		  filtervec.add(temp);
		  }
		
		MyQuerry querry= new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Purchase());
		return querry;
	}
	
	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
		if(LoginPresenter.branchRestrictionFlag)
		{
		if(olbBranch.getSelectedIndex()==0)
		{
			showDialogMessage("Select Branch");
			return false;
		}
		}
		return super.validate();
	}
}
