package com.slicktechnologies.client.views.purchase.purchase;
import com.google.gwt.event.dom.client.ClickEvent;

import java.util.*;

import com.google.gwt.user.client.ui.*;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appstructure.*;
import com.simplesoftwares.client.library.appstructure.search.*;
import com.simplesoftwares.client.library.appstructure.formscreen.*;
import com.slicktechnologies.shared.*;
import com.slicktechnologies.shared.common.*;
import com.slicktechnologies.shared.common.businessprocesslayer.*;
import com.slicktechnologies.shared.common.businessunitlayer.*;
import com.slicktechnologies.shared.common.helperlayer.*;
import com.slicktechnologies.shared.common.personlayer.*;
import com.slicktechnologies.shared.common.productlayer.*;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.google.gwt.core.shared.GWT;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.shared.common.paymentlayer.*;
import com.google.gwt.event.dom.client.*;
/**
 * FormScreen presenter template.
 */
public class PurchasePresenter extends FormScreenPresenter<Purchase>  implements RowCountChangeEvent.Handler{

	//Token to set the concrete FormScreen class name
	PurchaseForm form;
	
	public PurchasePresenter  (FormScreen<Purchase> view, Purchase model) {
		super(view, model);
		form=(PurchaseForm) view;
		form.getSaleslineitempurchasetable().getTable().addRowCountChangeHandler(this);
		}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
	InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();
		
		if(text.equals("New"))
			reactToNew();
		

	}
	
	private void reactToNew()
	{
		form.setToNewState();
		this.initalize();
		form.toggleAppHeaderBarMenu();
	}

	@Override
	public void reactOnPrint() {
		
		
	}
	
	@Override
	public void reactOnDownload() {
		
		
	}

	@Override
	public void reactOnEmail() {
		
		
	}

	/**
	 * Method token to make new model
	 */
	@Override
	protected void makeNewModel() {
		model=new Purchase();
	}
	
	
	
	
	public static void initalize()
	{
		PurchaseForm  form=new  PurchaseForm();
		
		
		 PurchasePresenterTable gentable=new PurchasePresenterTableProxy();
		  gentable.setView(form);
		  gentable.applySelectionModle();
		 PurchasePresenterSearch.staticSuperTable=gentable;
		 PurchasePresenterSearch searchpopup=new PurchasePresenterSearchProxy();
		form.setSearchpopupscreen(searchpopup);
		
		
		 PurchasePresenter  presenter=new PurchasePresenter(form,new Purchase());
		AppMemory.getAppMemory().stickPnel(form);
	}
	
	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.Purchase")
	 public static  class PurchasePresenterSearch extends SearchPopUpScreen<Purchase>{

		@Override
		public MyQuerry getQuerry() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public boolean validate() {
			// TODO Auto-generated method stub
			return true;
		}};
		
		@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.Purchase")
		 public static class PurchasePresenterTable extends SuperTable<Purchase> implements GeneratedVariableRefrence{

			@Override
			public Object getVarRef(String varName) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void createTable() {
				// TODO Auto-generated method stub
				
			}

			@Override
			protected void initializekeyprovider() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void addFieldUpdater() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void setEnable(boolean state) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void applyStyle() {
				// TODO Auto-generated method stub
				
			}} ;
			
		  private void reactTo()
  {
}
		  
		  @Override
			public void onRowCountChange(RowCountChangeEvent event) 
			{
			    double total=form.getSaleslineitempurchasetable().calculateTotal();
			   
              
			   // ValueChangeEvent.fire(form.getPaymentComposite().getTotalAmt(),total);
				form.getPaymentComposite().getTotalAmt().setValue(total);
				form.getPaymentComposite().setAmount();
			    
			}
	
	
}
