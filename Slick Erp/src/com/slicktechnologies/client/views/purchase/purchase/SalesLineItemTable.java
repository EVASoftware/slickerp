package com.slicktechnologies.client.views.purchase.purchase;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.ProvidesKey;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;


public class SalesLineItemTable extends SuperTable<SalesLineItem>  
{
	Column<SalesLineItem,String> deleteColumn;
	Column<SalesLineItem,String> lbtColumn,vatColumn,serviceColumn,totalColumn;
	TextColumn<SalesLineItem> prodCategoryColumn,prodCodeColumn,prodNameColumn;
	Column<SalesLineItem,Date> dateColumn;
	Column<SalesLineItem,String> priceColumn;
	Column<SalesLineItem,String> prodQtyColumn,durationColumn,noServicesColumn;
	
	TextColumn<SalesLineItem> viewprodQtyColumn,viewdurationColumn,viewnoServicesColumn,viewdColumn,
	viewPriceColumn;



	public SalesLineItemTable()
	{
		super();
	}
	public SalesLineItemTable(UiScreen<SalesLineItem> view)
	{
		super(view);	
	}

	public void createTable()
	{
		createColumnprodCategoryColumn();
		createColumnprodCodeColumn();
		createColumnprodNameColumn();
		createColumnprodQtyColumn();
		createColumndateColumn();
		createColumndurationColumn();
		createColumnnoServicesColumn();
		createColumnpriceColumn();
		createColumnlbtColumn();
		createColumnvatColumn();
		createColumnserviceColumn();
		createColumntotalColumn();
		createColumndeleteColumn();
		addFieldUpdater();
		table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);	
		}
	
	
	

	protected void createColumnprodCategoryColumn()
	{
		prodCategoryColumn=new TextColumn<SalesLineItem>()
				{
			@Override public String getValue(SalesLineItem object){
				return object.getProductCategory();
			}
				};
				table.addColumn(prodCategoryColumn,"Category");

	}
	protected void createColumnprodCodeColumn()
	{
		prodCodeColumn=new TextColumn<SalesLineItem>()
				{
			@Override public String getValue(SalesLineItem object){
				return object.getProductCode();
			}
				};
				table.addColumn(prodCodeColumn,"PCode");

	}
	protected void createColumnprodNameColumn()
	{
		prodNameColumn=new TextColumn<SalesLineItem>()
				{
			@Override public String getValue(SalesLineItem object){
				return object.getProductName();
			}
				};
				table.addColumn(prodNameColumn,"Name");

	}
	protected void createColumnprodQtyColumn()
	{
		EditTextCell editCell=new EditTextCell();
		prodQtyColumn=new Column<SalesLineItem,String>(editCell)
				{
			@Override
			public String getValue(SalesLineItem object)
			{
				if(object.getQty()==0){
					return 0+"";}
				else{
					return object.getQty()+"";}
			}
				};
				table.addColumn(prodQtyColumn,"#Qty");
               
	}
	
	
	protected void createViewQtyColumn()
	{
		
		viewprodQtyColumn=new TextColumn<SalesLineItem>()
				{
			@Override
			public String getValue(SalesLineItem object)
			{
				if(object.getQty()==0){
					return 0+"";}
				else{
					return object.getQty()+"";}
			}
				};
				table.addColumn(viewprodQtyColumn,"#Qty");
	}
	
	
	protected void createColumndateColumn()
	{
		DateTimeFormat fmt = DateTimeFormat.getFormat(PredefinedFormat.DATE_SHORT);
		DatePickerCell date= new DatePickerCell(fmt);
		dateColumn=new Column<SalesLineItem,Date>(date)
				{
			@Override
			public Date getValue(SalesLineItem object)
			{
				if(object.getStartDate()!=null){
					return null;}
				else{object.setStartDate(new Date());
				return new Date();
				}
			}
				};
				table.addColumn(dateColumn,"Date");
	}
	
	protected void createViewColumndateColumn()
	{
		viewdColumn=new TextColumn<SalesLineItem>()
				{
			@Override
			public String getValue(SalesLineItem object)
			{
				if(object.getStartDate()!=null){
					
					return AppUtility.parseDate(object.getStartDate());
					}
				else{
				return "N.A";
				}
			}
				};
				table.addColumn(viewdColumn,"Date");
	}
	
	
	
	
	
	
	protected void createColumndurationColumn()
	{
		TextCell editCell=new TextCell();
		durationColumn=new Column<SalesLineItem,String>(editCell)
				{
			@Override
			public String getValue(SalesLineItem object)
			{
				if(object.getPrduct() instanceof ItemProduct)
					return "N.A";
				else{
					return object.getDuration()+"";}
			}
				};
				table.addColumn(durationColumn,"#Duration");
	}
	
	protected void createColumnViewDuration()
	{
		
		viewdurationColumn=new TextColumn<SalesLineItem>()
				{
			@Override
			public String getValue(SalesLineItem object)
			{
				if(object.getPrduct() instanceof ItemProduct)
					return "N.A";
				else{
					return object.getDuration()+"";}
			}
				};
				table.addColumn(viewdurationColumn,"#Duration");
	}
	
	
	
	
	
	
	
	protected void createColumnnoServicesColumn()
	{
		EditTextCell editCell=new EditTextCell();
		noServicesColumn=new Column<SalesLineItem,String>(editCell)
				{
			@Override
			public String getValue(SalesLineItem object)
			{
				if(object.getPrduct() instanceof ItemProduct){
					return "N.A"+"";}
				else{
					return object.getNumberOfServices()+"";}
			}
				};
				table.addColumn(noServicesColumn,"#Services");
	}
	
	
	protected void createViewColumnnoServicesColumn()
	{
		
		viewnoServicesColumn=new TextColumn<SalesLineItem>()
				{
			@Override
			public String getValue(SalesLineItem object)
			{
				if(object.getPrduct() instanceof ItemProduct){
					return "N.A"+"";}
				else{
					return object.getNumberOfServices()+"";}
			}
				};
				table.addColumn(viewnoServicesColumn,"#Services");
	}
	
	
	
	protected void createColumnpriceColumn()
	{
		EditTextCell editCell=new EditTextCell();
		priceColumn=new Column<SalesLineItem,String>(editCell)
				{
			@Override
			public String getValue(SalesLineItem object)
			{
				return object.getPrice()+"";
			}
				};
				table.addColumn(priceColumn,"#Price");
	}
	
	protected void createViewColumnpriceColumn()
	{
		
		viewPriceColumn=new TextColumn<SalesLineItem>()
				{
			@Override
			public String getValue(SalesLineItem object)
			{
				if(object.getPrice()==0){
					return 0+"";}
				else{
					return object.getPrice()+"";}
			}
				};
				table.addColumn(viewPriceColumn,"#Price");
	}
	
	
	
	
	
	
	
	protected void createColumnlbtColumn()
	{
		TextCell editCell=new TextCell();
		lbtColumn=new Column<SalesLineItem,String>(editCell)
				{
			@Override
			public String getValue(SalesLineItem object)
			{
				return "";
			}
				};
				table.addColumn(lbtColumn,"#Lbt");
	}
	protected void createColumnvatColumn()
	{
		TextCell editCell=new TextCell();
		vatColumn=new Column<SalesLineItem,String>(editCell)
				{
			@Override
			public String getValue(SalesLineItem object)
			{
				SuperProduct product=object.getPrduct();
				if(product instanceof ItemProduct)
					return object.getVatTax().getPercentage()+"";
				else 
					return "N.A.";
			}
				};
				table.addColumn(vatColumn,"#Vat");
	}
	protected void createColumnserviceColumn()
	{
		TextCell editCell=new TextCell();
		serviceColumn=new Column<SalesLineItem,String>(editCell)
				{
			@Override
			public String getValue(SalesLineItem object)
			{
				SuperProduct prod=object.getPrduct();
				if(prod instanceof ServiceProduct)
					return object.getServiceTax().getPercentage()+"";
				else 
					return "N.A.";
			}
				};
				table.addColumn(serviceColumn,"#Service");
	}
	protected void createColumntotalColumn()
	{
		TextCell editCell=new TextCell();
		totalColumn=new Column<SalesLineItem,String>(editCell)
				{
			@Override
			public String getValue(SalesLineItem object)
			{
				SuperProduct product=object.getPrduct();
				double tax=calculateTax(product);
				double total=(tax+object.getPrice())*object.getQty();
				System.out.println("Total Quantity "+object.getQty());
				System.out.println("Total Price "+object.getPrice());
				
				
				
				return total+"";
			}
				};
				table.addColumn(totalColumn,"Total");
				table.setColumnWidth(totalColumn,"90px");
	}
	protected void createColumndeleteColumn()
	{
		ButtonCell btnCell= new ButtonCell();
		deleteColumn=new Column<SalesLineItem,String>(btnCell)
				{
			@Override
			public String getValue(SalesLineItem object)
			{
				return  "Delete" ;
			}
				};
				table.addColumn(deleteColumn,"Delete");
	}



	@Override public void addFieldUpdater() 
	{
		createFieldUpdaterprodQtyColumn();
		createFieldUpdaterdateColumn();
		createFieldUpdaterpriceColumn();
		createFieldUpdaterprodDurationColumn();
		createFieldUpdaterdeleteColumn();


	}

	protected void createFieldUpdaterprodQtyColumn()
	{
		prodQtyColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, String>()
				{
			@Override

			public void update(int index,SalesLineItem object,String value)
			{

				try{
					Double val1=Double.parseDouble(value.trim());
					object.setQuantity(val1);
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				}
				catch (NumberFormatException e)
				{

				}


				table.redrawRow(index);
			}
				});
	}
	protected void createFieldUpdaterdateColumn()
	{
		dateColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, Date>()
				{
			@Override
			public void update(int index,SalesLineItem object,Date value)
			{
				object.setStartDate(value);
				table.redrawRow(index);
			}
				});
	}

	protected void createFieldUpdaterprodDurationColumn()
	{
		this.durationColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, String>()
				{
			@Override

			public void update(int index,SalesLineItem object,String value)
			{

				try{
					Integer val1=Integer.parseInt(value.trim());
					object.setDuration(val1);

				}
				catch (NumberFormatException e)
				{

				}


				table.redrawRow(index);
			}
				});
	}


	protected void createFieldUpdaterNoServicesColumn()
	{
		this.durationColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, String>()
				{
			@Override

			public void update(int index,SalesLineItem object,String value)
			{

				try{
					Integer val1=Integer.parseInt(value.trim());
					object.setDuration(val1);

				}
				catch (NumberFormatException e)
				{

				}


				table.redrawRow(index);
			}
				});
	}




	protected void createFieldUpdaterpriceColumn()
	{
		priceColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, String>()
				{
			@Override

			public void update(int index,SalesLineItem object,String value)
			{
				try
				{
					
				Double val1=Double.parseDouble(value.trim());
				System.out.println("val1 "+val1);
				object.setPrice(val1);
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
				table.redrawRow(index);
			}
				});
	}
	protected void createFieldUpdaterdeleteColumn()
	{
		deleteColumn.setFieldUpdater(new FieldUpdater<SalesLineItem,String>()
				{
			@Override
			public void update(int index,SalesLineItem object,String value)
			{
				getDataprovider().getList().remove(object);

				table.redrawRow(index);
			}
				});
	}


	public void addColumnSorting(){

		//createSortinglbtColumn();

	}
/*
	protected void createSortinglbtColumn(){
		List<SalesLineItem> list=getDataprovider().getList();
		columnSort=new ListHandler<SalesLineItem>(list);
		columnSort.setComparator(lbtColumn, new Comparator<SalesLineItem>()
				{
			@Override
			public int compare(SalesLineItem e1,SalesLineItem e2)
			{
				if(e1!=null && e2!=null)
				{if( e1.getLbtTax()!=null && e2.getLbtTax()!=null){
					return e1.getLbtTax().compareTo(e2.getLbtTax());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}*/


	@Override
	public void setEnable(boolean state)
	{
          
		
        for(int i=table.getColumnCount()-1;i>-1;i--)
    	  table.removeColumn(i); 
        	  
      
          if(state==true)
          {
        	  createColumnprodCategoryColumn();
      		createColumnprodCodeColumn();
      		createColumnprodNameColumn();
      		createColumnprodQtyColumn();
      		createColumndateColumn();
      		createColumndurationColumn();
      		createColumnnoServicesColumn();
      		createColumnpriceColumn();
      		createColumnlbtColumn();
      		createColumnvatColumn();
      		createColumnserviceColumn();
      		createColumntotalColumn();
      		createColumndeleteColumn();
      		addFieldUpdater();
      		
          }
          
          
          else
          {
        	  
        	  
        	createColumnprodCategoryColumn();
      		createColumnprodCodeColumn();
      		createColumnprodNameColumn();
      		createViewQtyColumn();
      		createViewColumndateColumn();
      		createColumndurationColumn();
      		createViewColumnnoServicesColumn();
      		createViewColumnpriceColumn();
      		createColumnlbtColumn();
      		createColumnvatColumn();
      		createColumnserviceColumn();
      		createColumntotalColumn();
      		addFieldUpdater();
      	
          }
	}

	@Override
	public void applyStyle()
	{

	}
	@Override
	protected void initializekeyprovider() {
		keyProvider= new ProvidesKey<SalesLineItem>()
				{
			@Override
			public Object getKey(SalesLineItem item)
			{
				if(item==null)
				{
					return null;
				}
				else
					return item.getId();
			}
				};
	}


	private double calculateTax(SuperProduct entity)
	{
		double lbt = 0,vat = 0,service = 0;
		double tax=0;
		double prodprice=entity.getPrice();
		if(entity instanceof ServiceProduct)
		{
			ServiceProduct prod=(ServiceProduct) entity;
			if(prod.getServiceTax()!=null&&prod.getServiceTax().isInclusive()==false)
				service=prod.getServiceTax().getPercentage();

		}

		if(entity instanceof ItemProduct)
		{
			ItemProduct prod=(ItemProduct) entity;
			if(prod.getVatTax()!=null&&prod.getVatTax().isInclusive()==false)
				vat=prod.getVatTax().getPercentage();
		}
		if(entity.getVatTax()!=null&&entity.getVatTax().isInclusive()==false)
			lbt=entity.getVatTax().getPercentage();

		tax=(lbt+vat+service)*prodprice/100;
		return tax;


	}

	public double calculateTotal()
	{
		List<SalesLineItem>list=getDataprovider().getList();

		double sum=0;
		for(int i=0;i<list.size();i++)
		{
			SalesLineItem entity=list.get(i);
			SuperProduct prod=entity.getPrduct();
			double tax=calculateTax(prod);
			sum=sum+(entity.getPrice()+tax)*entity.getQty();
	
		}
		
		return sum;

	}
	
	
}
