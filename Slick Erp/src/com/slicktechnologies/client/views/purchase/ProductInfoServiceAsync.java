package com.slicktechnologies.client.views.purchase;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.slicktechnologies.shared.common.helperlayer.ProductInfo;

public interface ProductInfoServiceAsync {

	public void LoadVinfo(long companyId,String typeOfEntity,AsyncCallback<ArrayList<ProductInfo>>callback);
	public void LoadProdinfo(long companyId,int latestCount,String typeOfEntity,AsyncCallback<ArrayList<ProductInfo>>callback);
	public void LoadSuperProd(int itemLatestId,int serviceLatestId,long companyId,AsyncCallback<ArrayList<ProductInfo>>callback);
//	public void LoadVinfo(MyQuerry querry,AsyncCallback<ArrayList<ProductInfo>>callback);
	
	public void LoadAssetProd(long companyId,AsyncCallback<ArrayList<ProductInfo>>callback);
}
