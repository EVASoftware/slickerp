package com.slicktechnologies.client.views.multipleserviceschedule;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.Button;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.views.project.concproject.EmpolyeeTable;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.scheduling.TeamManagement;

public class TeamPopUp extends PopupScreen{

	ObjectListBox<TeamManagement> olbTeam;
	Button btnAdd;
	EmpolyeeTable emptable;
	
	ObjectListBox<Employee> olbEmployee;
	 
	public TeamPopUp(){
		super();
		createGui();
		getPopup().getElement().addClassName("setWidth");
	}
	
	@Override
	public void createScreen() {
		// TODO Auto-generated method stub
		
		initializeWidgets();
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fTeamgrouping = fbuilder.setlabel("Team").widgetType(FieldType.Grouping).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Team",  olbTeam);
		FormField folbTeam = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Add",  btnAdd);
		FormField fbtnAdd = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Technician  (First technician in list will be marked as technician & he will receive the service on his technician mobile app)",  olbEmployee);
		FormField folbEmployee = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",  emptable.getTable());
		FormField femptable = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
			
	
		FormField [][]formfield = {
				{fTeamgrouping},
				{folbTeam,folbEmployee,fbtnAdd},
				{femptable},
		};
		this.fields=formfield;
		
	}

	private void initializeWidgets() {

		olbTeam = new ObjectListBox<TeamManagement>();
		this.initalizeTeamListBox();
		
		btnAdd = new Button("Add");
		btnAdd.addClickHandler(this);
		
		emptable = new EmpolyeeTable();
		emptable.connectToLocal();
		
		olbEmployee = new ObjectListBox<Employee>();
		olbEmployee.makeEmployeeLive(AppConstants.SERVICEMODULE, AppConstants.CUSTOMERPROJECT, "Technician");

	}

	private void initalizeTeamListBox() {
		MyQuerry querry = new MyQuerry();
		querry.setQuerryObject(new TeamManagement());
		olbTeam.MakeLive(querry);
		
	}

	@Override
	public void onClick(ClickEvent event) {
		super.onClick(event);
		
		if(event.getSource()==btnAdd){
			if(olbTeam.getSelectedIndex()!=0){
				
				ArrayList<EmployeeInfo> list = new ArrayList<EmployeeInfo>();
				TeamManagement teamManangementEntity = olbTeam.getSelectedItem();
				for(int i=0;i<teamManangementEntity.getTeamList().size();i++){
					
					EmployeeInfo empInfoEntity = new EmployeeInfo();
			  		empInfoEntity.setFullName(teamManangementEntity.getTeamList().get(i).getEmployeeName());
			        empInfoEntity.setCellNumber(teamManangementEntity.getTeamList().get(i).getPhoneNumber());
			        empInfoEntity.setEmpCount(teamManangementEntity.getTeamList().get(i).getCustomerCount());
			        if(teamManangementEntity.getTeamList().get(i).getEmpDesignation()!=null)
			        empInfoEntity.setDesignation(teamManangementEntity.getTeamList().get(i).getEmpDesignation());
			      	list.add(empInfoEntity);
				}
				
				this.emptable.getDataprovider().setList(list);
				
			}
			

			if(olbEmployee.getSelectedIndex()!=0){
				Employee empEntity=olbEmployee.getSelectedItem();

			    List<EmployeeInfo> empList = this.emptable.getDataprovider().getList();
			    for(EmployeeInfo empInfo : empList){
			    	if(empEntity.getCount() == empInfo.getEmpCount()){
			    		showDialogMessage("Technician is already present in Table..Please select different one");
			    		return;
			    	}
			    }
		  		EmployeeInfo empInfoEntity = new EmployeeInfo();
		  		
		  		empInfoEntity.setEmpCount(empEntity.getCount());
		  		empInfoEntity.setFullName(empEntity.getFullName());
		      	empInfoEntity.setCellNumber(empEntity.getCellNumber1());
		      	empInfoEntity.setDesignation(empEntity.getDesignation());
		      	empInfoEntity.setDepartment(empEntity.getDepartMent());
		      	empInfoEntity.setEmployeeType(empEntity.getEmployeeType());
		      	empInfoEntity.setEmployeerole(empEntity.getRoleName());
		      	empInfoEntity.setBranch(empEntity.getBranchName());
		      	empInfoEntity.setCountry(empEntity.getCountry());
		      	
				this.emptable.getDataprovider().getList().add(empInfoEntity);

			}
		}
		
		if(event.getSource()==this.getLblCancel()){
			hidePopUp();
		}
	}

	public EmpolyeeTable getEmptable() {
		return emptable;
	}

	public void setEmptable(EmpolyeeTable emptable) {
		this.emptable = emptable;
	}
	
	
}
