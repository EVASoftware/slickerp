package com.slicktechnologies.client.views.multipleserviceschedule;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.Cell;
import com.google.gwt.cell.client.ClickableTextCell;
import com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.SelectionCell;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.notification.NotificationService;
import com.slicktechnologies.client.notification.NotificationServiceAsync;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.contract.ContractForm;
import com.slicktechnologies.client.views.device.SaveService;
import com.slicktechnologies.client.views.device.SaveServiceAsync;
import com.slicktechnologies.client.views.device.ServicReschdulePopUp;
import com.slicktechnologies.client.views.popups.RescheduleHistoryPopup;
import com.slicktechnologies.client.views.scheduleservice.ServiceSchedulePopUp;
import com.slicktechnologies.client.views.service.CustomerServiceMaterialTable;
import com.slicktechnologies.client.views.service.ServiceSchedulePopup;
import com.slicktechnologies.client.views.service.ServiceTable;
import com.slicktechnologies.client.views.technicianwarehousedetails.TechnicianServiceSchedulePopup;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.TechnicianWareHouseDetailsList;
import com.slicktechnologies.shared.TechnicianWarehouseDetails;
import com.slicktechnologies.shared.common.ModificationHistory;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
import com.slicktechnologies.shared.common.productgroup.ServiceProductGroupList;
import com.slicktechnologies.shared.common.servicerelated.CompanyAsset;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;
import com.google.gwt.cell.client.Cell.Context;

public class MultipleServicesScheduleTable extends SuperTable<Service> implements ClickHandler{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8618637134226391633L;
//	Logger logger = Logger.getLogger("table");

	TextColumn<Service> getColumnServiceId;
	TextColumn<Service> getColumnServiceName;
	TextColumn<Service> getColumnServiceDate;
	Column<Service , Date> geteditServiceDate;
	Column<Service , String> geteditColumnTechnician;
	TextColumn<Service> getColumnTechnician;
	TextColumn<Service> getColumnServiceTime;
	Column<Service , String> materialColumn;
	Column<Service, String> deleteColumn;
	ArrayList<String> hourList;
	ArrayList<String> minuteList;
	ArrayList<String> timeTypeList;
	Column<Service , String> geteditColumnHour;
	Column<Service , String> geteditColumnMinute;
	Column<Service , String> geteditColumnAMPM;
	ArrayList<String> empList;
	Map<Integer , Employee> employeeMap = new HashMap<Integer, Employee>();
	
	TechnicianServiceSchedulePopup materialPopup = new  TechnicianServiceSchedulePopup("Material");
	
	ServiceTable serviceTable = new ServiceTable();
	GeneralServiceAsync genService = GWT.create(GeneralService.class);
	GenricServiceAsync service = GWT.create(GenricService.class);
	int rowIndex;
	Service serviceObject;
	GWTCAlert alert = new GWTCAlert();
	
	ServiceSchedulePopup serviceSchedulePopUp = new ServiceSchedulePopup("Material");
	Column<Service, String> dateChangeButton;
	public ServicReschdulePopUp dateChangePopup = new ServicReschdulePopUp();
	PopupPanel popup = new PopupPanel();
	RescheduleHistoryPopup rescheduleHistoryPopup = new RescheduleHistoryPopup();
	Column<Service, String> communicationLogButton;
	/** Added By Priyanka **/
	Column<Service , String> DescriptionColumn;
	DescriptionPopup desPopup = new DescriptionPopup();
	
	Column<Service , String> getColumnTeam;
	TeamPopUp teampopup = new TeamPopUp();
	Service teamPopupServiceObj;
	
	TextColumn<Service> getColumnCustomerName;
	TextColumn<Service> getColumnServiceBranch;
	Column<Service , String> srDownloadColumn;

	
	public MultipleServicesScheduleTable(){
		super();
		TechnicianServiceSchedulePopup.materialValue = "Material";
		materialPopup = new  TechnicianServiceSchedulePopup("Material");
		materialPopup.getLblOk().addClickHandler(this);
		materialPopup.getLblCancel().addClickHandler(this);
		
		ServiceSchedulePopup.materialValue = "Material";
		serviceSchedulePopUp = new  ServiceSchedulePopup("Material");
		serviceSchedulePopUp.getLblOk().addClickHandler(this);
		serviceSchedulePopUp.getLblCancel().addClickHandler(this);
		dateChangePopup.getBtnReschedule().addClickHandler(this);
		dateChangePopup.getBtnCancel().addClickHandler(this);
		
		desPopup = new DescriptionPopup();
		
		desPopup.getLblOk().addClickHandler(this);
		desPopup.getLblCancel().addClickHandler(this);
		
		teampopup.getLblOk().addClickHandler(this);
		
	}
	
	@Override
	public void createTable() {
		// TODO Auto-generated method stub
		setListValues();
		addColumngetColumnServiceId();
		/**
		 * @author Vijay Date :- 17-01-2023
		 * Des :- added customer name and service branch column
		 */
		addColumnCustomerName();
		addColumnServiceBranch();
		/**
		 * ends here
		 */
		addColumngetColumnServiceName();
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "ServiceStatusAsPlanned")
				|| AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "RescheduleHistoryInPopup")){
			createColumnCommunicationLog();
		}
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "ServiceStatusAsPlanned")
				|| AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "RescheduleInPopup")){
			createColumnReschedule();
		}
//		addColumngetColumnServiceDate(); 
		addColumngetEditColumnServiceDate();//Ashwini patil added this and commented above
		addColumnHour();
		addColumnMinute();
		addColumnAMPM();
		addColumngeteditColumnTechnician();
		addColumnMaterial();
		
		addColumnTeam();

		addColumnDescription();
		addSRDownload();
		addColumnDelete();
		addFieldUpdater();
		
	}

	

	

	private void addColumnServiceBranch() {
		getColumnServiceBranch = new TextColumn<Service>() {
			
			@Override
			public String getValue(Service object) {
				return object.getServiceBranch();
			}
		};
		table.addColumn(getColumnServiceBranch, "Service Branch");
		table.setColumnWidth(getColumnServiceBranch, 100, Unit.PX);
	}

	private void addColumnCustomerName() {

		getColumnCustomerName = new TextColumn<Service>() {
			@Override
			public String getValue(Service object) {
				if(object.getPersonInfo()!=null && object.getPersonInfo().getFullName()!=null)
					return object.getPersonInfo().getFullName()+"";
				else
					return "";
			}
		};
		
		table.addColumn(getColumnCustomerName, "Customer Name");
		table.setColumnWidth(getColumnCustomerName, 100, Unit.PX);
	
		
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		createFieldUpdaterMaterialColumn();
		createFieldUpdateraddColumnDescription();
		createFieldUpdaterdeleteColumn();
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "ServiceStatusAsPlanned")
				|| AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "RescheduleInPopup") ){
			updateColumnReschedule();
		}
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "ServiceStatusAsPlanned")
				|| AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "RescheduleHistoryInPopup")){
			updateColumnCommunicationLogButton();
		}
		
		createFieldUpdaterTeam();
		
	}

	

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
	private void addColumngetColumnServiceId() {
		getColumnServiceId = new TextColumn<Service>() {
			
			@Override
			public String getValue(Service object) {
				return object.getCount()+"";
			}
		};
		table.addColumn(getColumnServiceId, "Service Id");
		table.setColumnWidth(getColumnServiceId, 100, Unit.PX);
	}
	
	private void addColumngetColumnServiceDate() {
		getColumnServiceDate = new TextColumn<Service>() {
			
			@Override
			public String getValue(Service object) {
				return AppUtility.parseDate(object.getServiceDate())+"";
			}
		};
		table.addColumn(getColumnServiceDate, "Service Date");
		table.setColumnWidth(getColumnServiceDate, 100, Unit.PX);
	}
	
	public void addColumngetEditColumnServiceDate() {
		DateTimeFormat fmt = DateTimeFormat
				.getFormat(PredefinedFormat.DATE_SHORT);

		DatePickerCell startdate = new DatePickerCell(fmt);
		geteditServiceDate = new Column<Service, Date>(startdate) {

			@Override
			public Date getValue(Service object) {
					return object.getServiceDate();
			}

		};
		table.addColumn(geteditServiceDate, "Service Date");
		table.setColumnWidth(geteditServiceDate, 120, Unit.PX);
		geteditServiceDate.setSortable(true);
		
		geteditServiceDate.setFieldUpdater(new FieldUpdater<Service, Date>() {
			@Override
			public void update(int index, Service object, Date value) {
				object.setServiceDate(value);
				object.setServiceDay(ContractForm.serviceDay(value));//10-02-2023
				final ArrayList<Service> list = new ArrayList<Service>();
				list.addAll(getDataprovider().getList());
				//Ashwini Patil Date:03-05-2023 Orion requirement is when they schedule multiple services and select date in one service, same date should get selected in other services.					
				for(Service s:list){
					s.setServiceDate(value);
					s.setServiceDay(ContractForm.serviceDay(value));//10-02-2023
				}
				table.redraw();
//				table.redrawRow(index);
			
			}
		});

	}
	
	private void addColumngetColumnServiceTime() {
		getColumnServiceTime= new TextColumn<Service>() {
			
			@Override
			public String getValue(Service object) {
				if(object.getServiceTime() != null && !object.getServiceTime().equals("")){
					return object.getServiceTime();			
				}else{
					return "Flexible";
				}
			}
		};
		table.addColumn(getColumnServiceTime, "Service Id");
		table.setColumnWidth(getColumnServiceTime, 100, Unit.PX);
	}
	
	private void addColumnHour() {
		SelectionCell hourCell= new SelectionCell(hourList);
		geteditColumnHour = new Column<Service, String>(hourCell) {
			@Override
			public String getValue(Service object) {
				if(object.getServiceTime()!= null  && !object.getServiceTime().equals("")&&!object.getServiceTime().equals("Flexible")){
					
					String[] time=object.getServiceTime().split(":");
					System.out.println(" 0 "+time[0]);
					System.out.println(" 1 "+time[1]);
					String hrs=time[0];
					String min;
					String ampm;
					if(time[1].contains("PM")){
						min=time[1].replace("PM", "");
						ampm="PM";
					}else{
						min=time[1].replace("AM", "");
						ampm="AM";
					}
					object.setHours(hrs);
					return hrs;
				}
				object.setHours("--");
				return "--";
			}
		};
		table.addColumn(geteditColumnHour, "HH");
		table.setColumnWidth(geteditColumnHour, 60, Unit.PX);
		
		
		geteditColumnHour.setFieldUpdater(new FieldUpdater<Service, String>() {
				@Override
				public void update(int index, Service object, String value) {
					
					object.setHours(value.trim());
					
					String serviceTime = "";
					if(value.equals("--")){
						object.setHours("12");
						value = "12";
					}
					serviceTime += value.trim()+":";
					if(object.getMinutes() != null && !object.getMinutes().equals("--")){
						serviceTime += object.getMinutes();
					}else{
						serviceTime += "00";
					}
					
					if(object.getAmpm() != null && !object.getMinutes().equals("--")){
						serviceTime += object.getAmpm();
					}else{
						serviceTime += "AM";
					}
					object.setServiceTime(serviceTime);

					table.redrawRow(index);
				
				}
			});
	}
	private void addColumnMinute() {
		SelectionCell hourCell= new SelectionCell(minuteList);
		geteditColumnMinute = new Column<Service, String>(hourCell) {
			@Override
			public String getValue(Service object) {
				if(object.getServiceTime()!= null  && !object.getServiceTime().equals("")&&!object.getServiceTime().equals("Flexible")){
					
					String[] time=object.getServiceTime().split(":");
					System.out.println(" 0 "+time[0]);
					System.out.println(" 1 "+time[1]);
					String hrs=time[0];
					String min;
					String ampm;
					if(time[1].contains("PM")){
						min=time[1].replace("PM", "");
						ampm="PM";
					}else{
						min=time[1].replace("AM", "");
						ampm="AM";
					}
					object.setMinutes(min);
					return min;
				}
				object.setMinutes("--");
				return "--";
			}
		};
		table.addColumn(geteditColumnMinute, "MM");
		table.setColumnWidth(geteditColumnMinute, 60, Unit.PX);
		
		geteditColumnMinute.setFieldUpdater(new FieldUpdater<Service, String>() {
			@Override
			public void update(int index, Service object, String value) {			
				object.setMinutes(value.trim());	
				String serviceTime = "";
				if(value.equals("--")){
					object.setMinutes("00");	
					value = "00";
				}
				if(object.getHours() != null && !object.getHours().equals("--")){
					serviceTime += object.getHours()+":";
				}else{
					serviceTime += "12:";
				}
				serviceTime += value.trim();
				if(object.getAmpm() != null && !object.getAmpm().equals("--")){
					serviceTime += object.getAmpm();
				}else{
					serviceTime += "AM";
				}
				object.setServiceTime(serviceTime);
				table.redrawRow(index);
			}
		});
	}
	private void addColumnAMPM() {
		SelectionCell hourCell= new SelectionCell(timeTypeList);
		geteditColumnAMPM = new Column<Service, String>(hourCell) {
			@Override
			public String getValue(Service object) {
				if(object.getServiceTime()!= null  && !object.getServiceTime().equals("")&&!object.getServiceTime().equals("Flexible")){
					
					String[] time=object.getServiceTime().split(":");
					System.out.println(" 0 "+time[0]);
					System.out.println(" 1 "+time[1]);
					String hrs=time[0];
					String min;
					String ampm;
					if(time[1].contains("PM")){
						min=time[1].replace("PM", "");
						ampm="PM";
					}else{
						min=time[1].replace("AM", "");
						ampm="AM";
					}
					object.setAmpm(ampm);
					return ampm;
				}
				object.setAmpm("--");
				return "--";
			}
		};
		table.addColumn(geteditColumnAMPM, "AM/PM");
		table.setColumnWidth(geteditColumnAMPM, 60, Unit.PX);
		
		geteditColumnAMPM.setFieldUpdater(new FieldUpdater<Service, String>() {
			@Override
			public void update(int index, Service object, String value) {
				object.setAmpm(value.trim());
				String serviceTime = "";
				
				if(object.getHours() != null && !object.getHours().equals("--")){
					serviceTime += object.getHours()+":";
				}else{
					serviceTime += "12:";
				}
				if(object.getMinutes() != null && !object.getMinutes().equals("--")){
					serviceTime += object.getMinutes();
				}else{
					serviceTime += "00";
				}
				if(value.equals("--")) {
					value = "AM";
				}
				
				serviceTime += value.trim();
				object.setServiceTime(serviceTime);
				table.redrawRow(index);
			}
		});
	}
	
	
	private void addColumngetColumnTechnician() {
		getColumnTechnician = new TextColumn<Service>() {
			
			@Override
			public String getValue(Service object) {
				return object.getEmployee();
			}
		};
//		table.addColumn(getColumnTechnician, "Service Engineer");
		/**@Sheetal:17-03-2022 
		 *  Des : Renaming Service Engineer to Technician, requirement by nitin sir**/
		table.addColumn(getColumnTechnician, "Technician");
		table.setColumnWidth(getColumnTechnician, 100, Unit.PX);
	}
	private void addColumngeteditColumnTechnician() {
		
		ObjectListBox<Employee> olbEmployee = new ObjectListBox<Employee>();
		olbEmployee.makeEmployeeLive(AppConstants.SERVICEMODULE, AppConstants.CUSTOMERSERVICELIST, "Technician");
		empList = new ArrayList<String>();
		Set<String> empSet = new HashSet<String>();
		empList.add("--SELECT--");
		employeeMap = new HashMap<Integer, Employee>();
		for(Employee emp : olbEmployee.getItems()){
			empSet.add(emp.getFullName());
		}
		empList.addAll(empSet);
		Console.log("empList size"+empList.size());
		SelectionCell empCell= new SelectionCell(empList);
		geteditColumnTechnician = new Column<Service, String>(empCell) {
			@Override
			public String getValue(Service object) {
				return object.getEmployee();
			}
		};
//		table.addColumn(geteditColumnTechnician, "Service Engineer");
		/**@Sheetal:17-03-2022 
		 *  Des : Renaming Service Engineer to Technician, requirement by nitin sir**/
		table.addColumn(geteditColumnTechnician, "Technician");
		table.setColumnWidth(geteditColumnTechnician, 100, Unit.PX);
		
		geteditColumnTechnician.setFieldUpdater(new FieldUpdater<Service, String>() {
			@Override
			public void update(int index, Service object, String value) {
				if(object.getTechnicians().size()>0){
					object.setEmployee(object.getTechnicians().get(0).getFullName());
				}
//				else{
				final ArrayList<Service> list = new ArrayList<Service>();
				list.addAll(getDataprovider().getList());
				
					if(value.equalsIgnoreCase("--SELECT--")){
//						object.setEmployee("");
						//Ashwini Patil Date:26-04-2023 Orion requirement is when they schedule multiple services and select technician in one service, same technician should get selected in other services.					
						for(Service s:list){
							s.setEmployee("");
						}
					}else{
						String oldEmp=object.getEmployee();
//						object.setEmployee(value);
						
						
						Employee empEntity=new Employee();
						ObjectListBox<Employee> olbEmployee = new ObjectListBox<Employee>();
						olbEmployee.makeEmployeeLive(AppConstants.SERVICEMODULE, AppConstants.CUSTOMERSERVICELIST, "Technician");
						for(Employee emp : olbEmployee.getItems()){
							if(emp.getFullName().equals(value)){
								empEntity=emp;	
								break;
							}

						}
						

						
						//Ashwini Patil
//						Employee empEntity=new Employee();
//						ArrayList<Employee> allEmployeeList=LoginPresenter.globalEmployee;
//						Console.log("allEmployeeList size="+allEmployeeList.size()+"value="+value);
//						for(Employee e:allEmployeeList){
//							Console.log("e.getFullName()="+e.getFullName());
//							if(e.getFullName().equals(value)){
//								Console.log("employee matched with allEmployeeList");
//								empEntity=e;	
//								break;
//							}	
//							
//						}
						EmployeeInfo empInfoEntity = new EmployeeInfo();
				  		
				  		empInfoEntity.setEmpCount(empEntity.getCount());
				  		empInfoEntity.setFullName(empEntity.getFullName());
				      	empInfoEntity.setCellNumber(empEntity.getCellNumber1());
				      	empInfoEntity.setDesignation(empEntity.getDesignation());
				      	empInfoEntity.setDepartment(empEntity.getDepartMent());
				      	empInfoEntity.setEmployeeType(empEntity.getEmployeeType());
				      	empInfoEntity.setEmployeerole(empEntity.getRoleName());
				      	empInfoEntity.setBranch(empEntity.getBranchName());
				      	empInfoEntity.setCountry(empEntity.getCountry());
						
//						final ArrayList<Service> list = new ArrayList<Service>();
//						list.addAll(getDataprovider().getList());
//						for(int i=0; i<list.size();i++){
//							Service serObj = list.get(i);
							
//							EmployeeInfo empInfoEntity = new EmployeeInfo();
//					  		empInfoEntity.setFullName(value);
						List<EmployeeInfo> empInfoList=new ArrayList<EmployeeInfo>();
						empInfoList.add(empInfoEntity);
						if(object.getTechnicians().size()>0){
								for(int i=0;i<object.getTechnicians().size();i++){
									
									if(i==0&&oldEmp!=null&&!oldEmp.equals("")){
										if(object.getTechnicians().get(0).getFullName().equals(oldEmp))
											continue;
									}
									empInfoList.add(object.getTechnicians().get(i));
								}
						}
//							teampopup.getEmptable().getDataprovider().getList().add(empInfoEntity);
							object.setTechnicians(empInfoList);
							Console.log("empinfo set to team list");

							//Ashwini Patil Date:14-04-2023 Orion requirement is when they schedule multiple services and select technician in one service, same technician should get selected in other services.
							
						
							for(Service s:list){
								Console.log("serviceid="+s.getCount());
								s.setEmployee(value);
								s.setTechnicians(empInfoList);
							}
							
							getDataprovider().setList(list);
							Console.log("technician name set to all list items");
							
//						}
					}
//				}
				
//				table.redrawRow(index);//Commented on 27-04-2023
				table.redraw();
			}
		});
	}
	private void addColumnMaterial() {
		ButtonCell btnCell = new ButtonCell();
		materialColumn = new Column<Service, String>(btnCell) {
			@Override
			public String getValue(Service object) {
				return "Material";
			}
		};
		table.addColumn(materialColumn, "Material");
		table.setColumnWidth(materialColumn, 100, Unit.PX);
	}
	
	/** Added By Priyanka **/
	private void addColumnDescription() {
		// TODO Auto-generated method stub
		
		ButtonCell btnCell = new ButtonCell();
		DescriptionColumn = new Column<Service, String>(btnCell) {
			@Override
			public String getValue(Service object) {
				return "Description";
			}
		};
		table.addColumn(DescriptionColumn, "Description");
		table.setColumnWidth(DescriptionColumn, 100, Unit.PX);
		
	}
	
	//Ashwini Patil Date:1-03-2023
	private  void addSRDownload(){
//		ButtonCell imagelink = new ButtonCell();
		
		srDownloadColumn=new Column<Service, String>(new ClickableTextCell()) {
			@Override
			public String getCellStyleNames(Context context, Service object) {
					return "blue";
			}
			
			@Override
			public String getValue(Service object) {
				// TODO Auto-generated method stub
				if(object.getUptestReport()!=null&&object.getUptestReport().getName()!=null)
					return object.getUptestReport().getName();
				else
					return "no image";
			}
		};
		
		
		table.addColumn(srDownloadColumn, "Service Report1");
		table.setColumnWidth(srDownloadColumn, 100, Unit.PX);
		
		srDownloadColumn.setFieldUpdater(new FieldUpdater<Service, String>() {
			
			@Override
			public void update(int index, Service object, String value) {
				// TODO Auto-generated method stub
				if(object.getUptestReport()!=null){
					String url=object.getUptestReport().getUrl()+"&filename="+object.getUptestReport().getName();
					Window.open(url, "_blank", "");
					//https://my-dot-evaerp282915.appspot.com/slick_erp/downloadMapImage?blobkey=AMIfv96FnGlPnJLuG2pQhkTEEhVGA1IZ2bGIy4sYJwksLeBPv7gLMQ4OYV4DAgfd-Fww-6NtSea2VfIrVkLbH-YRW3Y8kIBBVCbMZoxvF6M719z59cJ0irfqudoKvEBw3kanmirEHPyLSmH9Jd_EWJ57J978p84T5FGx5blptAeuiGrw8IAr82SteALS8eH9dUhIA5fA8UTy3qciVYJNQfXyWg397_u4xKy8DAi0ZImUU_akGhveEQGvExv0FeM3Mb3LF3wE9h-lsPAwKZ9TbRYGf2JfIknBAg&filename=abc.jpg
				}else{
					Window.alert("No image found");
				}
			}
		});
	}
	
	protected void createFieldUpdateraddColumnDescription() {
		DescriptionColumn.setFieldUpdater(new FieldUpdater<Service, String>() {
			@Override
			public void update(int index, Service object, String value) {
				
				
				rowIndex = index;
				serviceObject = object;
				desPopup.getPopup().getElement()
				.addClassName("servicepopup");
				desPopup.getPopup().center();
				desPopup.getPopup().setHeight("300px");
				desPopup.getPopup().setWidth("300px");
				desPopup.showPopUp();
				//serviceTable.getProjectDetails(serviceObject , desPopup , true);
				//getServiceDescription(serviceObject ,desPopup );
				desPopup.serviceObj=object;
				
				desPopup.taDescription.setValue(object.getDescription());
				//desPopup.taDescription.setValue(object.getd);
				table.redrawRow(index);
			}
		});
	}
	
//	protected void getServiceDescription(Service serviceObject2,DescriptionPopup desPopup2) {
//		
//		
//		
//		
//	}

	protected void createFieldUpdaterMaterialColumn() {
		materialColumn.setFieldUpdater(new FieldUpdater<Service, String>() {
			@Override
			public void update(int index, Service object, String value) {
		//		serviceTable.getProjectAndBOMDetails(object, materialPopup, false);
				
				
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "TechnicianwiseMaterialPopup")){
				TechnicianServiceSchedulePopup.materialValue = "Material";			
				rowIndex = index;
				serviceObject = object;
				materialPopup.getPopup().getElement()
				.addClassName("servicepopup");
				materialPopup.getPopup().center();
				materialPopup.getPopup().setHeight("580px");
				materialPopup.showPopUp();
				
				getProjectAndBOMDetails(serviceObject , materialPopup);
				}else{
					ServiceSchedulePopup.materialValue = "Material";			
					rowIndex = index;
					serviceObject = object;
					serviceSchedulePopUp.getPopup().getElement()
					.addClassName("servicepopup");
					serviceSchedulePopUp.getPopup().center();
					serviceSchedulePopUp.getPopup().setHeight("580px");
					serviceSchedulePopUp.showPopUp();
					serviceTable.getProjectDetails(serviceObject , serviceSchedulePopUp , true);
				}
				table.redrawRow(index);
			}
		});
	}
	private void addColumnDelete() {
		ButtonCell btnCell = new ButtonCell();
		deleteColumn = new Column<Service, String>(btnCell) {
			@Override
			public String getValue(Service object) {
				return "Delete";
			}
		};
		table.addColumn(deleteColumn, "Delete");
		table.setColumnWidth(deleteColumn, 100, Unit.PX);
		
		
	}
	
	protected void createFieldUpdaterdeleteColumn() {
		deleteColumn.setFieldUpdater(new FieldUpdater<Service, String>() {
			@Override
			public void update(int index, Service object, String value) {
				getDataprovider().getList().remove(object);
				table.redrawRow(index);
			}
		});
	}
	private void setListValues(){
		hourList   = new  ArrayList<String>();
		minuteList = new  ArrayList<String>();
		timeTypeList = new ArrayList<String>();
		
		hourList.add("--");
		hourList.add("01");
		hourList.add("02");
		hourList.add("03");
		hourList.add("04");
		hourList.add("05");
		hourList.add("06");
		hourList.add("07");
		hourList.add("08");
		hourList.add("09");
		hourList.add("10");
		hourList.add("11");
		hourList.add("12");
		
		minuteList.add("--");
		minuteList.add("00");
		minuteList.add("15");
		minuteList.add("30");
		minuteList.add("45");

		timeTypeList.add("--");
		timeTypeList.add("AM");
		timeTypeList.add("PM");
		ObjectListBox<Employee> olbEmployee = new ObjectListBox<Employee>();
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "LoadOperatorDropDown")){
			olbEmployee.makeEmployeeLive(AppConstants.SERVICEMODULE, AppConstants.CUSTOMERPROJECT, "Operator");
		}else{
			olbEmployee.makeEmployeeLive(AppConstants.SERVICEMODULE, AppConstants.CUSTOMERPROJECT, "Technician");
		}
		empList = new ArrayList<String>();
		Set<String> empSet = new HashSet<String>();
		empList.add("--SELECT--");
		employeeMap = new HashMap<Integer, Employee>();
//		for(int i=0;i<LoginPresenter.globalEmployee.size() ; i++ ){
//			Employee emp = LoginPresenter.globalEmployee.get(i);
//			if(emp != null){
//				if(emp.getRoleName().equalsIgnoreCase("Operator")){
//					employeeMap.put(emp.getCount(), emp);
//					empSet.add(emp.getFullname());
//				}
//				//if(emp.gete)
//				
//			}
//			
//		}
		Console.log(" s");
		for(Employee emp : olbEmployee.getItems()){
			empSet.add(emp.getFullName());
		}
		empList.addAll(empSet);
		}
	/** date 27.02.2018 added by komal for customer service list new changes **/
	public void getProjectAndBOMDetails(final Service model ,final TechnicianServiceSchedulePopup schedule ) {

		final int contractId = model.getContractCount();
		
		 MyQuerry querry = new MyQuerry();
		 Vector<Filter> filterVec = new Vector<Filter>();
		 
		 Filter filter =null;
		 filter=new Filter();
		 filter.setIntValue(model.getCount());
		 filter.setQuerryString("serviceId");
		 filterVec.add(filter);
		 System.out.println("proj =="+model.getProjectId());
		 int projectId = model.getProjectId();
		 if(projectId!=0){
			 System.out.println("PROJECTttt");
			 filter=new Filter();
			 filter.setIntValue(projectId);
			 filter.setQuerryString("count");
			 filterVec.add(filter);
		 }
		 
		 querry.setFilters(filterVec);
		 querry.setQuerryObject(new ServiceProject());
		 
		 service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(final ArrayList<SuperModel> result1) {
				// TODO Auto-generated method stub
				System.out.println("result ="+result1.size());
				
				MyQuerry querry1 = new MyQuerry();
				 Vector<Filter> filterVec = new Vector<Filter>();
				 
				 Filter filter =null;
				 filter=new Filter();
				 filter.setStringValue(model.getEmployee());
				 filter.setQuerryString("wareHouseList.technicianName");
				 filterVec.add(filter);
				 
				 querry1.setFilters(filterVec);
				 querry1.setQuerryObject(new TechnicianWareHouseDetailsList());
				
				 service.getSearchResult(querry1, new AsyncCallback<ArrayList<SuperModel>>() {
					
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						// TODO Auto-generated method stub
						TechnicianWareHouseDetailsList technicianDetails = null;
						for(SuperModel model : result){
							technicianDetails = (TechnicianWareHouseDetailsList) model;
							break;
						}
						String warehouseName = "" ,storageLocation = "", storageBin = "",
								parentWareHouse = "" , parentStorageLocation = "" , parentStorageBin = "";
						
						if(technicianDetails != null){
							
							for(TechnicianWarehouseDetails details : technicianDetails.getWareHouseList()){
								if(details.getTechnicianName().equalsIgnoreCase(model.getEmployee())){
									warehouseName = details.getTechnicianWareHouse();
									storageLocation = details.getTechnicianStorageLocation();
									schedule.getOlbStorageLocation().addItem(storageLocation);
									storageBin = details.getTechnicianStorageBin();
									schedule.getOlbstorageBin().addItem(storageBin);
									parentWareHouse = details.getParentWareHouse();
									parentStorageLocation = details.getParentStorageLocation();
									schedule.getOlbParentStorageLocation().addItem(parentStorageLocation);
									parentStorageBin = details.getPearentStorageBin();
									schedule.getOlbParentstorageBin().addItem(parentStorageBin);
									break;
								}
							}
						}
						schedule.showPopUp();
						schedule.clear();
						
						schedule.lblSchedule.getElement().setId("addbutton");
						schedule.getHorizontal().add(schedule.lblSchedule);
						schedule.getMaterialProductTable().setEnable(true);
					
					
						
						schedule.getOlbParentstorageBin().setValue(parentStorageBin);
						schedule.getOlbParentWarehouseName().setValue(parentWareHouse);
						schedule.getOlbParentStorageLocation().setValue(parentStorageLocation);
						schedule.getOlbStorageLocation().setValue(storageLocation);
						schedule.getOlbstorageBin().setValue(storageBin);
						schedule.getOlbWarehouseName().setValue(warehouseName);
						
						schedule.getEmptable().getDataprovider().getList().clear();
						schedule.getToolTable().getDataprovider().getList().clear();
						schedule.getMaterialProductTable().getDataprovider().getList().clear();
						schedule.getDbQuantity().setValue(0d);
//						if(model.getStatus().equals("Completed")){
//							schedule.setEnable(false);
//							schedule.getMaterialProductTable().setEnable(false);
//						}else{
//							schedule.setEnable(true);
//							schedule.getMaterialProductTable().setEnable(true);
//						}
						if(model.getEmployee() != null && !(model.getEmployee().equals(""))){
							schedule.getOlbEmployee().setValue(model.getEmployee());
							List<Employee> empList = LoginPresenter.globalEmployee;
							Set<Employee> empSet = new HashSet<Employee>(empList);
							List<EmployeeInfo> empInfoList = new ArrayList<EmployeeInfo>(); 
							for(Employee empEntity : empSet){
							if(empEntity.getFullname().equals(model.getEmployee())){
								empInfoList = new ArrayList<EmployeeInfo>();
								EmployeeInfo empInfoEntity = new EmployeeInfo();

								empInfoEntity.setEmpCount(empEntity.getCount());
						  		empInfoEntity.setFullName(empEntity.getFullName());
						      	empInfoEntity.setCellNumber(empEntity.getCellNumber1());
						      	empInfoEntity.setDesignation(empEntity.getDesignation());
						      	empInfoEntity.setDepartment(empEntity.getDepartMent());
						      	empInfoEntity.setEmployeeType(empEntity.getEmployeeType());
						      	empInfoEntity.setEmployeerole(empEntity.getRoleName());
						      	empInfoEntity.setBranch(empEntity.getBranchName());
						      	empInfoEntity.setCountry(empEntity.getCountry());		
						      	empInfoList.add(empInfoEntity);
								schedule.getEmptable().getDataprovider().setList(empInfoList);

							}
						}
					
						schedule.getOlbEmployee().setSelectedIndex(0);
						}
						for(SuperModel mo :result1){
							ServiceProject project = (ServiceProject) mo;//komal changed model to smodel
							
								//schedule.getEmptable().getDataprovider().setList(project.getTechnicians());
							schedule.getEmptable().getDataprovider().getList().clear();
							List<EmployeeInfo> infoList = new ArrayList<EmployeeInfo>();
							if(project.getTechnicians() != null){
							for(EmployeeInfo info : project.getTechnicians()){
								infoList.add(info);
							}
							schedule.getEmptable().getDataprovider().getList().addAll(infoList);
							}
							if(infoList != null && infoList.size()>0){
							for(Employee empEntity : LoginPresenter.globalEmployee){
								if(empEntity.getFullname().equals(model.getEmployee())){
									
									EmployeeInfo empInfoEntity = new EmployeeInfo();

									empInfoEntity.setEmpCount(empEntity.getCount());
							  		empInfoEntity.setFullName(empEntity.getFullName());
							      	empInfoEntity.setCellNumber(empEntity.getCellNumber1());
							      	empInfoEntity.setDesignation(empEntity.getDesignation());
							      	empInfoEntity.setDepartment(empEntity.getDepartMent());
							      	empInfoEntity.setEmployeeType(empEntity.getEmployeeType());
							      	empInfoEntity.setEmployeerole(empEntity.getRoleName());
							      	empInfoEntity.setBranch(empEntity.getBranchName());
							      	empInfoEntity.setCountry(empEntity.getCountry());		
							      	
									schedule.getEmptable().getDataprovider().getList().set(0, empInfoEntity);

								}
							}
							}
							if(project.getTooltable() != null && project.getTooltable().size() >0){
								schedule.getToolTable().getDataprovider().setList(project.getTooltable());
							}
							if(project.getProdDetailsList() != null && project.getProdDetailsList().size() > 0){
								schedule.getMaterialProductTable().getDataprovider().setList(project.getProdDetailsList());
							}
							
							
							
							CustomerServiceMaterialTable.projectId=project.getCount();
							
						}
//						if(schedule.getMaterialProductTable().getDataprovider().getList().size() == 0){
//						if(serviceObject.getServiceProductList() != null && serviceObject.getServiceProductList().size() > 0){
//							ArrayList<ProductGroupList> list = new ArrayList<ProductGroupList>();
//							if(serviceObject.getServiceProductList() != null){
//							for(ProductGroupList  productGroupList : serviceObject.getServiceProductList()){
//								ProductGroupList technicianMaterialIfno  = new ProductGroupList();
//								technicianMaterialIfno.setName(productGroupList.getName());
//								
//									technicianMaterialIfno.setQuantity(productGroupList.getPlannedQty());
//									technicianMaterialIfno.setPlannedQty(productGroupList.getPlannedQty());
//									technicianMaterialIfno.setProActualQty(productGroupList.getPlannedQty());
//								
//							//	if(schedule.getOlbWarehouseName().getSelectedIndex()!=0)
//									technicianMaterialIfno.setWarehouse(schedule.getOlbWarehouseName().getValue(schedule.getOlbWarehouseName().getSelectedIndex()));
//							//	if(schedule.getOlbStorageLocation().getSelectedIndex()!=0)
//									technicianMaterialIfno.setStorageLocation(schedule.getOlbStorageLocation().getValue(schedule.getOlbStorageLocation().getSelectedIndex()));	
//							//	if(schedule.getOlbstorageBin().getSelectedIndex()!=0)
//									technicianMaterialIfno.setStorageBin(schedule.getOlbstorageBin().getValue(schedule.getOlbstorageBin().getSelectedIndex()));	
//
//							//	if(schedule.getOlbParentWarehouseName().getSelectedIndex()!=0)
//									technicianMaterialIfno.setParentWarentwarehouse(schedule.getOlbParentWarehouseName().getValue(schedule.getOlbParentWarehouseName().getSelectedIndex()));
//							//	if(schedule.getOlbParentStorageLocation().getSelectedIndex()!=0)
//									technicianMaterialIfno.setParentStorageLocation(schedule.getOlbParentStorageLocation().getValue(schedule.getOlbParentStorageLocation().getSelectedIndex()));	
//							//	if(schedule.getOlbParentstorageBin().getSelectedIndex()!=0)
//									technicianMaterialIfno.setParentStorageBin(schedule.getOlbParentstorageBin().getValue(schedule.getOlbParentstorageBin().getSelectedIndex()));	
//								
//								technicianMaterialIfno.setProduct_id(productGroupList.getProduct_id());
//								technicianMaterialIfno.setCode(productGroupList.getCode());
//								technicianMaterialIfno.setUnit(productGroupList.getUnit());
//								/** date 22.03.2019 added by komal to store price in project **/
//								Console.log("price : " + (productGroupList.getPrice()) / productGroupList.getPlannedQty());
//								technicianMaterialIfno.setPrice(productGroupList.getPrice());
//							
//								
//								list.add(technicianMaterialIfno);
//								
//							}
//							schedule.getMaterialProductTable().getDataprovider().setList(list);
//							}
//							
//						}
//						}
						schedule.getDateBox().setValue(model.getServiceDate());
						
						 /**
					     * @author Vijay Chougule
					     * Des :- if Automatich Scheduling process Config is active then Service Project will create with Automatic
					     * Service Scheduling for Technician App.
					     */
						if(result1.size()==0 && AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", AppConstants.AUTOMATICSCHEDULING)){
							List<ProductGroupList> bommateriallist = new ArrayList<ProductGroupList>();
							for(ServiceProductGroupList serviceprodgroup : model.getServiceProductList()){
								ProductGroupList technicianMaterialIfno  = new ProductGroupList();
								technicianMaterialIfno.setName(serviceprodgroup.getName());
								technicianMaterialIfno.setQuantity(serviceprodgroup.getQuantity());
								technicianMaterialIfno.setPlannedQty(serviceprodgroup.getPlannedQty());
								technicianMaterialIfno.setProActualQty(serviceprodgroup.getQuantity());
								technicianMaterialIfno.setProduct_id(serviceprodgroup.getProduct_id());
								technicianMaterialIfno.setCode(serviceprodgroup.getCode());
								technicianMaterialIfno.setUnit(serviceprodgroup.getUnit());
								technicianMaterialIfno.setPrice(serviceprodgroup.getPrice());

								bommateriallist.add(technicianMaterialIfno);
							}
									
							schedule.getMaterialProductTable().getDataprovider().setList(bommateriallist);		
						}
						/**
						 * ends here
						 */
						
						
					}
					
					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						
					}
				});
				
			}
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				alert.alert("Unexpected error occurred");
			}
		});
	}

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		if(materialPopup.getLblCancel() == event.getSource()){
			materialPopup.hidePopUp();
		}
		if(materialPopup.getLblOk() == event.getSource()){
			materialPopup.getLblOk().setText("Processing");
//			Company c = new Company();
//			List<ProductGroupList> materilList = materialPopup.getMaterialProductTable().getDataprovider().getList();
//			ArrayList<Integer> integerList = new ArrayList<Integer>();
//			final Map<Integer , Double> map =  new HashMap<Integer , Double>();
//			for(ProductGroupList prodId : materilList){
//				integerList.add(prodId.getProduct_id());
//				map.put(prodId.getProduct_id() , prodId.getProActualQty());
//			};
//			final TechnicianServiceSchedulePopup popup = materialPopup;
//			genService.getProductInventoryDetails(integerList, c.getCompanyId(), new AsyncCallback<ArrayList<SuperModel>>() {
//				
//				@Override
//				public void onSuccess(ArrayList<SuperModel> result) {
//					// TODO Auto-generated method stub
//				//	lblUpdate.setText("Schedule");
//					GWTCAlert alert = new GWTCAlert();
//					if(result!=null){
//					String transferToWarehouseName = materialPopup.getOlbWarehouseName().getValue(materialPopup.getOlbWarehouseName().getSelectedIndex());
//					String transferToLocationName = materialPopup.getOlbStorageLocation().getValue(materialPopup.getOlbStorageLocation().getSelectedIndex());
//					String transferToBinName = materialPopup.getOlbstorageBin().getValue(materialPopup.getOlbstorageBin().getSelectedIndex());
//					
//					String parentWarehouseName = materialPopup.getOlbParentWarehouseName().getValue(materialPopup.getOlbParentWarehouseName().getSelectedIndex());
//					String parentLocationName = materialPopup.getOlbParentStorageLocation().getValue(materialPopup.getOlbParentStorageLocation().getSelectedIndex());
//					String parentBinName = materialPopup.getOlbParentstorageBin().getValue(materialPopup.getOlbParentstorageBin().getSelectedIndex());
//
//					if(result.size()==0){
//						alert.alert("Please Define Product Inventory Master First!");
//						materialPopup.getLblOk().setText("OK");
//						return;
//					}
//					
//					double availableQuantity = 0;
//					for(SuperModel model : result){
//						boolean flag = false;
//						boolean parentFlag = false;
//						ProductInventoryView prodInventory = (ProductInventoryView) model;
//						for(ProductInventoryViewDetails productDetails: prodInventory.getDetails()){
//							if(productDetails.getWarehousename().trim().equals(transferToWarehouseName.trim()) &&
//								productDetails.getStoragelocation().trim().equals(transferToLocationName.trim()) &&
//								productDetails.getStoragebin().trim().equals(transferToBinName.trim()) ){
//								flag =true;
//								
//							}
//							if(productDetails.getWarehousename().trim().equals(parentWarehouseName.trim()) &&
//									productDetails.getStoragelocation().trim().equals(parentLocationName.trim()) &&
//									productDetails.getStoragebin().trim().equals(parentBinName.trim()) ){
//								availableQuantity = productDetails.getAvailableqty();
//								if(result.size()!=0){
//									if (materialPopup.validateQuantity(availableQuantity , map.get(productDetails.getProdid()))) {										
//										alert.alert("Insufficient available quantity in parent warehouse for product id : "+productDetails.getProdname());
//										materialPopup.getLblOk().setText("OK");
//										return;
//									} 
//								}
//								parentFlag =true;
//							}
//						}
//						if(flag==false){
//							alert.alert("Please Define Product Inventory Master for TransferTo Warehouse!");
//							materialPopup.getLblOk().setText("OK");
//							return;
//						}
//						if(parentFlag==false){
//							alert.alert("Please Define Product Inventory Master for Parent Warehouse!");
//							materialPopup.getLblOk().setText("OK");
//							return;
//						}
//					}
//					
//					}else{
//						alert.alert("Please Define Product Inventory Master for TransferTo Warehouse!");
//						materialPopup.getLblOk().setText("OK");
//						return;
//					}
//					reactOnOk();
//				}
//				
//				@Override
//				public void onFailure(Throwable caught) {
//					// TODO Auto-generated method stub
//					//lblUpdate.setText("Schedule");
//					materialPopup.getLblOk().setText("OK");
//				}
//			});
			reactOnOk();
		}
		if(serviceSchedulePopUp.getLblCancel() == event.getSource()){
			serviceSchedulePopUp.hidePopUp();
		}
		if(serviceSchedulePopUp.getLblOk() == event.getSource()){
			serviceSchedulePopUp.getLblOk().setText("Processing");
					final ArrayList<Service> list = new ArrayList<Service>();
					list.addAll(getDataprovider().getList());
	
					final Service serObj = list.get(rowIndex);
	

					final ArrayList<CompanyAsset> technicianToollist = new ArrayList<CompanyAsset>();
					technicianToollist.addAll(serviceSchedulePopUp.getToolTable().getDataprovider().getList());
					
					final ArrayList<ProductGroupList> materialInfo = new ArrayList<ProductGroupList>();
					materialInfo.addAll(serviceSchedulePopUp.getMaterialProductTable().getDataprovider().getList());
					
					final ArrayList<EmployeeInfo> emplist = new ArrayList<EmployeeInfo>();
					emplist.addAll(serviceSchedulePopUp.getEmptable().getDataprovider()
							.getList());
					if(emplist.size()>0){
						serObj.setTechnicians(emplist);
					}
					else{
						if(serObj.getTechnicians()!=null && serObj.getTechnicians().size()>0){
							List<EmployeeInfo> empList = serObj.getTechnicians();
							emplist.addAll(empList);
							Console.log("emplist size"+emplist.size());
						}
					}
				
					String serviceHrs=serviceSchedulePopUp.getP_servicehours().getItemText(serviceSchedulePopUp.getP_servicehours().getSelectedIndex());
					String serviceMin=serviceSchedulePopUp.getP_servicemin().getItemText(serviceSchedulePopUp.getP_servicemin().getSelectedIndex());
					String serviceAmPm=serviceSchedulePopUp.getP_ampm().getItemText(serviceSchedulePopUp.getP_ampm().getSelectedIndex());
					String totalTime = "";
					final String calculatedServiceTime;
					
					totalTime = serviceHrs+":"+serviceMin+""+serviceAmPm;
					System.out.println("totalTime "+totalTime);
					if(totalTime.equalsIgnoreCase("--:----") || totalTime.equalsIgnoreCase("--:--AM") ){
						calculatedServiceTime = "Flexible";
					}else{
						calculatedServiceTime = totalTime;
					}
					System.out.println("totalTime == "+totalTime);
					//selectedRecord.setAttribute("ServiceTime", calculatedServiceTime);
					

					final int InvoiceId ;
					if(serviceSchedulePopUp.getOlbInvoiceId().getSelectedIndex()!=0){
						InvoiceId  = Integer.parseInt(serviceSchedulePopUp.getOlbInvoiceId().getValue(serviceSchedulePopUp.getOlbInvoiceId().getSelectedIndex()));
					}else{
						InvoiceId = 0;
					}
					final String invoiceDate;
					if(serviceSchedulePopUp.getOlbInvoiceDate().getSelectedIndex()!=0){
						invoiceDate = serviceSchedulePopUp.getOlbInvoiceDate().getValue(serviceSchedulePopUp.getOlbInvoiceDate().getSelectedIndex());
	                 }else{
	                	 invoiceDate = null;
	                 }
					final int projectId = serObj.getProjectId();
							Console.log("Project Id=="+projectId);
					final String technicianName = serviceTable.getTechnicianName(serviceSchedulePopUp.getEmptable().getDataprovider().getList());
					
			genService.TechnicianSchedulingPlan(serObj,emplist,technicianToollist,materialInfo,serviceSchedulePopUp.getDateBox().getValue(),calculatedServiceTime,InvoiceId,invoiceDate,technicianName,projectId, new AsyncCallback<Integer>() {
									
									@Override
									public void onSuccess(Integer result) {
										// TODO Auto-generated method stub
										Console.log("iddddiiddd - - "+result);
										//selectedRecord.setAttribute("ProjectId", result);
										//grid.redraw();
										serObj.setProjectId(result);
                                        serviceObject = serObj;
										RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
										table.redrawRow(rowIndex);
										alert.alert("Saved Succesfully!");
										serviceSchedulePopUp.getLblOk().setText("OK");
										serviceSchedulePopUp.hidePopUp();
									}
					
									@Override
									public void onFailure(Throwable caught) {
										// TODO Auto-generated method stub
										serviceSchedulePopUp.hidePopUp();
										
									}
								});
								
							
							}
		if(dateChangePopup.getBtnReschedule() == event.getSource())		{

			boolean val = validateReschedule();
			if(val==true)
			{
				ModificationHistory history=new ModificationHistory();
				history.oldServiceDate=serviceObject.getServiceDate();
				history.resheduleDate=dateChangePopup.getDateBox().getValue();
				
				String serviceHrs=dateChangePopup.getP_servicehours().getItemText(dateChangePopup.getP_servicehours().getSelectedIndex());
				String serviceMin=dateChangePopup.getP_servicemin().getItemText(dateChangePopup.getP_servicemin().getSelectedIndex());
				String serviceAmPm=dateChangePopup.getP_ampm().getItemText(dateChangePopup.getP_ampm().getSelectedIndex());
				String calculatedServiceTime=serviceHrs+":"+serviceMin+""+serviceAmPm;
				history.resheduleTime=calculatedServiceTime;
				history.resheduleBranch=dateChangePopup.getP_seviceBranch().getItemText(dateChangePopup.getP_seviceBranch().getSelectedIndex());				
				System.out.println("service branch == "+dateChangePopup.getP_seviceBranch().getItemText(dateChangePopup.getP_seviceBranch().getSelectedIndex()));				
				history.reason=dateChangePopup.getTextArea().getText();
				history.userName=LoginPresenter.loggedInUser;
				history.systemDate=new Date();
				Date reschDate=dateChangePopup.getDateBox().getValue();
				serviceObject.getListHistory().add(history);
				serviceObject.setServiceDate(reschDate);
				serviceObject.setServiceTime(calculatedServiceTime);
				String serviceBranch=serviceObject.getServiceBranch();
				if(dateChangePopup.getP_seviceBranch().getSelectedIndex() != 0){
					serviceBranch=dateChangePopup.getP_seviceBranch().getItemText(dateChangePopup.getP_seviceBranch().getSelectedIndex());
					serviceObject.setServiceBranch(serviceBranch);	
					System.out.println("service branch   === "+serviceBranch);		
				}				
				saveDetails("Service Date Changed !","Failed To Change !",Service.SERVICESTATUSPLANNED, history,reschDate,calculatedServiceTime,serviceBranch);
			}
		
		
		}
		if(dateChangePopup.getBtnCancel()== event.getSource())		{
			popup.hide();
		}	
		
		if(event.getSource()==desPopup.getLblOk()){
			final ArrayList<Service> list = new ArrayList<Service>();
			list.addAll(getDataprovider().getList());

			final Service serObj = list.get(rowIndex);
			serObj.setDescription(desPopup.taDescription.getValue());
			desPopup.hidePopUp();
			
		}
		if(event.getSource()==desPopup.getLblCancel()){
			desPopup.hidePopUp();
		}
		
		if(event.getSource() == teampopup.getLblOk()){
			reactonTeampupupOk(teamPopupServiceObj);
		}
		
	}
	

	private void reactOnOk(){


		
	//	if(materialPopup.ValidatePopupData(materialPopup)){
			
	//	final ArrayList<EmployeeInfo> technicianlist = new ArrayList<EmployeeInfo>();
	//		technicianlist.addAll(materialPopup.get.getDataprovider().getList());
			final ArrayList<Service> list = new ArrayList<Service>();
			list.addAll(getDataprovider().getList());

			final Service serObj = list.get(rowIndex);
			
			final ArrayList<EmployeeInfo> emplist = new ArrayList<EmployeeInfo>();
			emplist.addAll(materialPopup.getEmptable().getDataprovider()
					.getList());
			if(emplist != null && emplist.size()>0){
				EmployeeInfo info = new EmployeeInfo();
				info.setFullName(serObj.getEmployee());
				emplist.set(0, info);
			}else{
				EmployeeInfo info = new EmployeeInfo();
				info.setFullName(serObj.getEmployee());
				emplist.add(info);
			}
			if(emplist.size()>0){
				serObj.setTechnicians(emplist);
			}
		//	final String technicianName = getTechnicianName(technicianServiceSchedulePopup.getEmptable().getDataprovider().getList());
			final ArrayList<CompanyAsset> technicianToollist = new ArrayList<CompanyAsset>();
			technicianToollist.addAll(materialPopup.getToolTable().getDataprovider().getList());
			
			final ArrayList<ProductGroupList> materialInfo = new ArrayList<ProductGroupList>();
			List<ProductGroupList> list1  = new ArrayList<ProductGroupList>();
			for(ProductGroupList  productGroupList : materialPopup.getMaterialProductTable().getDataprovider().getList()){
				ProductGroupList technicianMaterialIfno  = new ProductGroupList();
				technicianMaterialIfno.setName(productGroupList.getName());
				
					technicianMaterialIfno.setQuantity(productGroupList.getQuantity());
					technicianMaterialIfno.setPlannedQty(productGroupList.getPlannedQty());
					technicianMaterialIfno.setProActualQty(productGroupList.getProActualQty());
				
				if(materialPopup.getOlbWarehouseName().getSelectedIndex()!=0)
					technicianMaterialIfno.setWarehouse(materialPopup.getOlbWarehouseName().getValue(materialPopup.getOlbWarehouseName().getSelectedIndex()));
				if(materialPopup.getOlbStorageLocation().getSelectedIndex()!=0)
					technicianMaterialIfno.setStorageLocation(materialPopup.getOlbStorageLocation().getValue(materialPopup.getOlbStorageLocation().getSelectedIndex()));	
				if(materialPopup.getOlbstorageBin().getSelectedIndex()!=0)
					technicianMaterialIfno.setStorageBin(materialPopup.getOlbstorageBin().getValue(materialPopup.getOlbstorageBin().getSelectedIndex()));	

				if(materialPopup.getOlbParentWarehouseName().getSelectedIndex()!=0)
					technicianMaterialIfno.setParentWarentwarehouse(materialPopup.getOlbParentWarehouseName().getValue(materialPopup.getOlbParentWarehouseName().getSelectedIndex()));
				if(materialPopup.getOlbParentStorageLocation().getSelectedIndex()!=0)
					technicianMaterialIfno.setParentStorageLocation(materialPopup.getOlbParentStorageLocation().getValue(materialPopup.getOlbParentStorageLocation().getSelectedIndex()));	
				if(materialPopup.getOlbParentstorageBin().getSelectedIndex()!=0)
					technicianMaterialIfno.setParentStorageBin(materialPopup.getOlbParentstorageBin().getValue(materialPopup.getOlbParentstorageBin().getSelectedIndex()));	
				
				technicianMaterialIfno.setProduct_id(productGroupList.getProduct_id());
				technicianMaterialIfno.setCode(productGroupList.getCode());
				technicianMaterialIfno.setUnit(productGroupList.getUnit());
				/** date 22.03.2019 added by komal to store price in project **/
				Console.log("price : " + (productGroupList.getPrice()) / productGroupList.getPlannedQty());
				technicianMaterialIfno.setPrice(productGroupList.getPrice());
			
				
				list1.add(technicianMaterialIfno);
				
			}
			materialPopup.getMaterialProductTable().getDataprovider().setList(list1);
		
			materialInfo.addAll(materialPopup.getMaterialProductTable().getDataprovider().getList());
			
			//selectedRecord.setAttribute("ServiceDate", planPopUp.dbserviceDate.getValueAsDate());
			
			String serviceHrs= serObj.getHours();
			String serviceMin=serObj.getMinutes();
			String serviceAmPm=serObj.getAmpm();
			String totalTime = "";
			final String calculatedServiceTime;
			
			totalTime = serviceHrs+":"+serviceMin+""+serviceAmPm;
			if(totalTime.equalsIgnoreCase("--:----") || totalTime.equalsIgnoreCase("--:--AM") ){
				calculatedServiceTime = "Flexible";
			}else{
				calculatedServiceTime = totalTime;
			}
			
			final int projectId = serObj.getProjectId();
			System.out.println("Project Id=="+projectId);
			ArrayList<String> statuslist = new ArrayList<String>();
				statuslist.add("Scheduled");
				statuslist.add("Rescheduled");

				ArrayList<String> strEmployeelist = new ArrayList<String>();
				for(EmployeeInfo emp : emplist){
					strEmployeelist.add(emp.getFullName());
				}
				
				Date date = serObj.getServiceDate();
//		
//				Vector<Filter> filtervec=new Vector<Filter>();
//				Filter filter = null;
//				MyQuerry querry;
//				
//				filter = new Filter();
//				filter.setQuerryString("companyId");
//				filter.setLongValue(UserConfiguration.getCompanyId());
//				filtervec.add(filter);
//				
//				filter = new Filter();
//				filter.setQuerryString("status IN");
//				filter.setList(statuslist);
//				filtervec.add(filter);
//				
//				filter = new Filter();
//				filter.setQuerryString("employee IN");
//				filter.setList(strEmployeelist);
//				filtervec.add(filter);
//				
//				filter = new Filter();
//				filter.setQuerryString("serviceDate");
//				filter.setDateValue(date);
//				filtervec.add(filter);
//				
//				querry=new MyQuerry();
//				querry.setQuerryObject(new Service());
//				querry.setFilters(filtervec);
//				
//				service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
//					@Override
//					public void onSuccess(ArrayList<SuperModel> result) {
//						System.out.println("RESULT SIZE : "+result.size());	
//						materialPopup.getLblOk().setVisible(true);
//						label : for(SuperModel model:result){
//							Service customerEntity=(Service) model;	
//							if(serObj.getCount() != customerEntity.getCount()){
//							if(!(calculatedServiceTime.equalsIgnoreCase("Flexible")))
//							{
//							for(EmployeeInfo emp : emplist){
//								System.out.println("All values komal 1:" + emp.getFullName() +" " +customerEntity.getEmployee() +" "+format1.format(technicianServiceSchedulePopup.getDateBox().getValue())+" "+format1.format(customerEntity.getServiceDate())  +" " +calculatedServiceTime +" " +customerEntity.getServiceTime());
//								
//								if(emp.getFullName().equals(customerEntity.getEmployee()) && format1.format(technicianServiceSchedulePopup.getDateBox().getValue()).equals(format1.format(customerEntity.getServiceDate())) && calculatedServiceTime.equals(customerEntity.getServiceTime())){
//									System.out.println("All values komal 2:" + emp.getFullName() +" " +customerEntity.getEmployee() +" "+format1.format(technicianServiceSchedulePopup.getDateBox().getValue())+" "+format1.format(customerEntity.getServiceDate()) +" " +calculatedServiceTime +" " +customerEntity.getServiceTime());
//									validateflag = false;
//									break label;
//									
//								}
//								}
//							}
//						  }
//						}
//						
//						System.out.println("Flag value :" + validateflag);
//						if (validateflag == false) {
//							alert.alert("Selected technician is already assigned to other service at inserted date and time");
//							validateflag = true;
//							} else {
								genService.TechnicianSchedulingPlan(serObj,emplist,technicianToollist,materialInfo,serObj.getServiceDate(),calculatedServiceTime,serObj.getInvoiceId(),serObj.getInvoiceDate()+"",serObj.getEmployee(),projectId, new AsyncCallback<Integer>() {
							
							@Override
							public void onSuccess(Integer result) {
								// TODO Auto-generated method stub
								materialPopup.getLblOk().setText("OK");
								System.out.println("iddddiiddd - - "+result);
								//selectedRecord.setAttribute("ProjectId", result);
								//grid.redraw();
								if(!calculatedServiceTime.equals(""))
									serObj.setServiceTime(calculatedServiceTime);
									if(result!=0)
										serObj.setProjectId(result);

								serObj.setMaterialAdded(true);	
	                            serviceObject = serObj;
								RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
								table.redrawRow(rowIndex);
								alert.alert("Material Saved Succesfully");
								materialPopup.hidePopUp();
								try{
									
									for(EmployeeInfo emp : emplist){
										/**
										  * Rahul Verma added below for notification on 28th Sept 2018
										  * For Reschedule
										  */
										 NotificationServiceAsync asyncService=GWT.create(NotificationService.class);
//										 if(form.tbServiceEngg.getSelectedIndex()!=0){
										 
											 String message="Service Id: "+serObj.getCount()+" is assigned to you.";
											 Console.log("message"+message);
//											 Console.log("form.tbServiceEngg.getValue(form.tbServiceEngg.getSelectedIndex())"+form.tbServiceEngg.getValue(form.tbServiceEngg.getSelectedIndex()));
											 
											 asyncService.sendNotification(serObj.getCompanyId(), emp.getFullName(), AppConstants.IAndroid.EVA_PEDIO,message, new AsyncCallback<ArrayList<Integer>>() {

												@Override
												public void onFailure(Throwable caught) {
													// TODO Auto-generated method stub
													
												}

												@Override
												public void onSuccess(ArrayList<Integer> result) {
													// TODO Auto-generated method stub
													Console.log("message"+result);
												}
											});
//										 }
										 /**
										  * Ends for Rahul
										  */
									}
									}catch(Exception e){
										e.printStackTrace();
									}
//							}
//			
//							@Override
//							public void onFailure(Throwable caught) {
//								// TODO Auto-generated method stub
//								materialPopup.hidePopUp();
//								
//							}
//						});
						
//					}
					}
					
					@Override
					public void onFailure(Throwable caught) {
						materialPopup.getLblOk().setText("OK");
						materialPopup.getLblOk().setVisible(true);
						System.out.println("Failed");
						Console.log("RPC Failed");
						
					}
				});					
	//	}				


	}
	private void addColumngetColumnServiceName() {
		getColumnServiceId = new TextColumn<Service>() {
			@Override
			public String getCellStyleNames(Context context, Service object) {
				 if(object.isMaterialAdded()){
					 return "red";
				 }
				 return "black";
			}
			@Override
			public String getValue(Service object) {
				
				
				return object.getProductName()+"";
			}
		};
		
		table.addColumn(getColumnServiceId, "Service");
		table.setColumnWidth(getColumnServiceId, 100, Unit.PX);
	}
	private void createColumnReschedule() {

		ButtonCell btnCell = new ButtonCell();
		dateChangeButton = new Column<Service, String>(btnCell) {

			@Override
			public String getValue(Service object) {
				// TODO Auto-generated method stub
				return "Date Change";
			}
		};

		table.addColumn(dateChangeButton, "");
		table.setColumnWidth(dateChangeButton, 110, Unit.PX);
	}

	private void updateColumnReschedule() {
		dateChangeButton.setFieldUpdater(new FieldUpdater<Service, String>() {
			@Override
			public void update(int index, final Service object, String value) {
				if (object.getStatus().equalsIgnoreCase(Service.CANCELLED)) {
					final GWTCAlert alert = new GWTCAlert();
					alert.alert("Can not reschedule the service as it is cancelled");
				} else {
						rowIndex = index;
						serviceObject = object;
						dateChangePopup.makeCustBranchListBox(serviceObject.getPersonInfo().getCount(), serviceObject.getCompanyId(),serviceObject.getServiceBranch());
						dateChangePopup.getTime().setValue(serviceObject.getServiceTime());
						
						System.out.println("Service Date ::::::::: "+serviceObject.getServiceDate());
						Date date = CalendarUtil.copyDate(serviceObject.getServiceDate());
						System.out.println("Service Date1 ::::::::: "+date);
						
						dateChangePopup.getDateBox().setValue(date);
		
						   popup=new PopupPanel(true);
						   popup.add(dateChangePopup);
						   dateChangePopup.getBtnReschedule().setText("Save");
						   dateChangePopup.clear();
						   popup.show();
						   popup.center();
				}
				
			}
		});
	}
	public void saveDetails(final String sucesssMessage,
			final String failureMessage,
			final String statusText,final ModificationHistory history,
			final Date ServiceDate,final String serviceTime,final String serviceBranch)
	{
		SaveServiceAsync asyncService=GWT.create(SaveService.class);
		final GWTCAlert alert = new GWTCAlert();
		asyncService.saveStatus(serviceObject,new AsyncCallback<Boolean>() {
        
			@Override
			public void onFailure(Throwable caught) {
				caught.printStackTrace();
				
			}
			@Override
			public void onSuccess(Boolean result) {
				if(result==false)
				{
					alert.alert("Service Cannot be marked Completed as there are "
							+ "Scheduled Projects");
					return;
				}
				alert.alert(sucesssMessage);
				serviceObject.setServiceScheduled(false);
				serviceObject.setStatus(statusText);
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				table.redrawRow(rowIndex);
				popup.hide();			 
			}
		});
	}
	private void createColumnCommunicationLog() {

		ButtonCell btnCell = new ButtonCell();
		communicationLogButton = new Column<Service, String>(btnCell) {

			@Override
			public String getValue(Service object) {
				// TODO Auto-generated method stub
				return "Followup";
			}
		};

		table.addColumn(communicationLogButton, "");
		table.setColumnWidth(communicationLogButton, 110, Unit.PX);
	}

	private void updateColumnCommunicationLogButton() {
		communicationLogButton.setFieldUpdater(new FieldUpdater<Service, String>() {
			@Override
			public void update(int index, final Service object, String value) {
				
						rowIndex = index;
						serviceObject = object;
						rescheduleHistoryPopup.showPopUp();
						rescheduleHistoryPopup.getModificationHistoryTable().getListDataProvider().setList(
								serviceObject.getListHistory());
				
			}
		});
	}
	public boolean validateReschedule()
	{
		Date rescheduleDate = dateChangePopup.getDateBox().getValue();
		Date contractEndDate = serviceObject.getContractEndDate();
		Date contractStartDate = serviceObject.getContractStartDate();
//		Date oldServiceDate=model.getServiceDate();
		String reasonForChange=dateChangePopup.getTextArea().getText().trim();
		if(rescheduleDate==null)
		{
			alert.alert("Reschedule Date Cannot be empty !");
			return false;
		}
		int cnt =rescheduleDate.compareTo(contractStartDate);
		if(cnt < 0)
		{
			alert.alert("Reschedule date should be after contract start date..!");
			return false;
		}	
		if(reasonForChange.equals(""))
		{
			alert.alert("Reason For Change Cannot be Empty !");
			return false;
		}	
		if(serviceObject.getStatus().equalsIgnoreCase(Service.SERVICESTATUSCOMPLETED)){
			alert.alert("Service is already completed !");
			return false;
		}
		
		/**
		 * nidhi
		 * 21-06-20118
		 * for reschedule service for same month validate
		 * Date :- 23-07-2018 By Vijay as discussed with Vaishali mam this validation not for Admin level
		 */
			boolean reScheduleValide  = AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "ServiceRescheduleWithInMonthOnly");
			
			if(reScheduleValide && !LoginPresenter.myUserEntity.getRole().getRoleName().equals("ADMIN")){
					Date serDate = dateChangePopup.getDateBox().getValue();
					
					Date preFirstDate = CalendarUtil.copyDate(serviceObject.getServiceDate());
					Date preLastDate = CalendarUtil.copyDate(serviceObject.getServiceDate());
					
					/** Date 02-04-2019 by Vijay 
					 *  Des :- NBHC CCPM when service date is end of the month then user can able to reschedule the service
					 *  which is bug so i have added below if condition if service Date is end of the month then not
					 *  required add month in the date.  
					 */
					Console.log("preLastDate =="+preLastDate);
					Console.log("preFirstDate =="+preFirstDate);
					if(!AppUtility.checkDateEndOfTheMonth(preFirstDate)){
						Console.log("Not End of the month"+preLastDate);
						CalendarUtil.addMonthsToDate(preLastDate, 1);
						preLastDate.setDate(1);
						CalendarUtil.addDaysToDate(preLastDate, -1);
					}
					
					/**
					 * Date 10-Aug-2018 By Vijay
					 * Des :- Reschedule date only allow service date month only.
					 */
					Date firstDateServiceDate = CalendarUtil.copyDate(serviceObject.getServiceDate());
					CalendarUtil.setToFirstDayOfMonth(firstDateServiceDate);
					Console.log("preLastDate =="+preLastDate);
					Console.log("serDate =="+serDate);
					
					
					if(serDate.after(preLastDate) || serDate.before(firstDateServiceDate) ){
						alert.alert("Service date can be Reschedule With in Service date's month only.");
						return false;
					}
//					if(serDate.before(serviceObject.getServiceDate())){
//						form.showDialogMessage("Reschedule service date should be after current service date.");
//						return false;
//					}
					
			}
		return true;
	}
	
	private void addColumnTeam() {
		ButtonCell btnCell = new ButtonCell();
		getColumnTeam = new Column<Service, String>(btnCell) {
			
			@Override
			public String getValue(Service object) {
				return "Team";
			}
		};
		table.addColumn(getColumnTeam, "Team");
		table.setColumnWidth(getColumnTeam, 100, Unit.PX);
		
	}
	
	private void createFieldUpdaterTeam() {
		getColumnTeam.setFieldUpdater(new FieldUpdater<Service, String>() {
			
			@Override
			public void update(int index, Service object, String value) {
				teamPopupServiceObj=object;
				Console.log("1. teamPopupServiceObj is set");
//				if(object.getEmployee()!=null && !object.getEmployee().equals("")){
//					alert.alert("Please Select either Team or Service Engineer!");
//				}
//				else{
					
					if(object.getTechnicians()!=null && object.getTechnicians().size()>0){
						teampopup.emptable.getDataprovider().setList(object.getTechnicians());
						//Ashwini Patil Date:14-04-2023 Orion requirement is when they schedule multiple services and select technician in one service, same technician should get selected in other services.							
						final ArrayList<Service> list = new ArrayList<Service>();
						list.addAll(getDataprovider().getList());
						
						for(Service s:list){
							s.setTechnicians(object.getTechnicians());
							s.setEmployee(object.getTechnicians().get(0).getFullName());
						}
						
						getDataprovider().setList(list);
						Console.log("1. team set to all list items");
						
					}else if(object.getEmployee()!=null &&!object.getEmployee().equals("")) {
							Employee empEntity=new Employee();
							ArrayList<Employee> allEmployeeList=LoginPresenter.globalEmployee;
							Console.log("allEmployeeList size="+allEmployeeList.size()+"value="+value);
							for(Employee e:allEmployeeList){
								Console.log("e.getFullName()="+e.getFullName());
								if(e.getFullName().equals(object.getEmployee())){
									Console.log("employee matched with allEmployeeList");
									empEntity=e;	
									break;
								}	
								
							}
							EmployeeInfo empInfoEntity = new EmployeeInfo();
					  		
					  		empInfoEntity.setEmpCount(empEntity.getCount());
					  		empInfoEntity.setFullName(empEntity.getFullName());
					      	empInfoEntity.setCellNumber(empEntity.getCellNumber1());
					      	empInfoEntity.setDesignation(empEntity.getDesignation());
					      	empInfoEntity.setDepartment(empEntity.getDepartMent());
					      	empInfoEntity.setEmployeeType(empEntity.getEmployeeType());
					      	empInfoEntity.setEmployeerole(empEntity.getRoleName());
					      	empInfoEntity.setBranch(empEntity.getBranchName());
					      	empInfoEntity.setCountry(empEntity.getCountry());
							
//							final ArrayList<Service> list = new ArrayList<Service>();
//							list.addAll(getDataprovider().getList());
//							for(int i=0; i<list.size();i++){
//								Service serObj = list.get(i);
								
//								EmployeeInfo empInfoEntity = new EmployeeInfo();
//						  		empInfoEntity.setFullName(value);
							List<EmployeeInfo> empInfoList=new ArrayList<EmployeeInfo>();
							empInfoList.add(empInfoEntity);
							Console.log("empInfoList size"+empInfoList.size());
							object.setTechnicians(empInfoList);
							
							//Ashwini Patil Date:14-04-2023 Orion requirement is when they schedule multiple services and select technician in one service, same technician should get selected in other services.							
							final ArrayList<Service> list = new ArrayList<Service>();
							list.addAll(getDataprovider().getList());
							
							for(Service s:list){
								s.setTechnicians(empInfoList);
								if(empInfoList!=null&&empInfoList.size()>0)
									s.setEmployee(empInfoList.get(0).getFullName());
							}
							
							getDataprovider().setList(list);
							Console.log("2. team set to all list items");
							
							
							teampopup.emptable.getDataprovider().setList(object.getTechnicians());
							Console.log("after setlist of size"+object.getTechnicians().size());
					}
					
//				}
					
					teampopup.showPopUp();
//					teampopup.emptable.connectToLocal();commented by ashwini
			}
		});
	}
	
	
	private void reactonTeampupupOk(Service serviceObj) {
		
		final ArrayList<Service> list = new ArrayList<Service>();
		list.addAll(getDataprovider().getList());
		
		final Service serObj = serviceObj;
		
		final ArrayList<EmployeeInfo> emplist = new ArrayList<EmployeeInfo>();
		emplist.addAll(teampopup.getEmptable().getDataprovider()
				.getList());
		serObj.setTechnicians(emplist);
		
		generivservice.save(serObj, new AsyncCallback<ReturnFromServer>() {
			
			@Override
			public void onSuccess(ReturnFromServer result) {
				// TODO Auto-generated method stub
				teampopup.hidePopUp();
				
				for(int i=0; i<list.size();i++){
					System.out.println(" in reactonTeampupupOk list");
					if(serObj.getTechnicians()!=null && serObj.getTechnicians().size()>0){
//						list.get(rowIndex).setEmployee(serObj.getTechnicians().get(0).getFullName());
//						Console.log("rowIndex in reactonTeampupupOk="+rowIndex);
//						serObj.setEmployee(serObj.getTechnicians().get(0).getFullName());//Ashwini Patil Date:25-07-2022
						
						list.get(i).setEmployee(serObj.getTechnicians().get(0).getFullName());//Ashwini Patil Date:27-04-2023
						list.get(i).setTechnicians(serObj.getTechnicians());//Ashwini Patil Date:27-04-2023
						
					}
				}
				getDataprovider().setList(list);
				table.redraw();
				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				teampopup.hidePopUp();
			}
		});
	}
}
