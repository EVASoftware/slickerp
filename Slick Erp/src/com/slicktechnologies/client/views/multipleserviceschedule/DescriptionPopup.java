package com.slicktechnologies.client.views.multipleserviceschedule;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.TextArea;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.CompositeInterface;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.popups.ScreenNameTable;
import com.slicktechnologies.client.views.service.CustomerServiceMaterialTable;
import com.slicktechnologies.client.views.technicianwarehousedetails.TechnicianServiceSchedulePopup;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.TechnicianWareHouseDetailsList;
import com.slicktechnologies.shared.TechnicianWarehouseDetails;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
import com.slicktechnologies.shared.common.productgroup.ServiceProductGroupList;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;

public class DescriptionPopup extends PopupScreen implements CompositeInterface , ChangeHandler{

	GeneralServiceAsync genService = GWT.create(GeneralService.class);
	GenricServiceAsync service = GWT.create(GenricService.class);
	Button btnDecription;
	Button btnCollectPayment;
	public TextArea taDescription;
	//Button btnCancel;
	
	
	
	public Service serviceObj;
	
	public DescriptionPopup(){
	super();
	createGui();
	getPopup().setHeight("200px");
	getPopup().setWidth("60px");
	}
	
	private void initializeWidgets() {
		
		
		btnDecription = new Button("Description");
		btnDecription.addClickHandler(this);
		
		btnCollectPayment = new Button("Collect Payment");
		btnCollectPayment.addClickHandler(this);
		
		taDescription = new TextArea();
		
		
		
		
	}

	@Override
	public void createScreen() {
		// TODO Auto-generated method stub
		
		initializeWidgets();
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fdescInfo = fbuilder.setlabel("Description").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
//		fbuilder = new FormFieldBuilder("",  btnDecription);
//		FormField fbtnDescription = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",  btnCollectPayment);
		FormField fbtncollectpaymnet = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",  taDescription);
		FormField ftaDescription = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fblankgroup=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		FormField [][]formfield = {
				{fdescInfo},
				{fbtncollectpaymnet},
				{ftaDescription},
		};
		this.fields=formfield;
		
	}
	
	public TextArea getTaDescription() {
		return taDescription;
	}

	public void setTaDescription(TextArea taDescription) {
		this.taDescription = taDescription;
	}

	@Override
	public void onChange(ChangeEvent event) {
		
		
	}
	
	@Override
	public void applyStyle() {
		super.applyStyle();
		
	    taDescription.getElement().getStyle().setHeight(150, Unit.PX);
		
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		
	}
	
	public void onClick(ClickEvent event) {
		
		
		if(event.getSource()==btnCollectPayment){
			
			
			System.out.println("Inside Collect payment buuton");
			getDescriptionOne();
		}
	}

	
	
	public void getDescriptionOne(){
		
		
		//final int contractId = model.getContractCount();
		final ArrayList<String> statuslist = new ArrayList<String>();
		statuslist.add("Created");
		
		
		 System.out.println("Service Id =="+serviceObj.getCount());
		 final MyQuerry querry = new MyQuerry();
		 Vector<Filter> filterVec = new Vector<Filter>();
		 Filter filter =null;
		 
		 int contractId = serviceObj.getContractCount();
		 
		 System.out.println("CONTRACT");
		 filter=new Filter();
		 filter.setIntValue(contractId);
		 filter.setQuerryString("contractCount");
		 filterVec.add(filter);
		 
		 filter=new Filter();
		 filter.setQuerryString("accountType");
		 filter.setStringValue("AR");
		 filterVec.add(filter);
		 
		 
		 filter=new Filter();
		 filter.setQuerryString("status");
		 filter.setStringValue("Created");
		 filterVec.add(filter);
		 
		 
		 
		 querry.setFilters(filterVec);
		 querry.setQuerryObject(new CustomerPayment());
		 
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
					
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				final GWTCAlert alert = new GWTCAlert();
				if(result==null||result.size()==0){
					alert.alert("No payment document is open for this contract");
				}else{
					System.out.println("result ="+result.size());
					
					CustomerPayment custpay = null;
					double total=0;
					for(SuperModel model : result){
						System.out.println("result ="+result.size());
						custpay = (CustomerPayment) model;
						total=total+custpay.getPaymentAmt();
					}
					String sum="";
					String des=taDescription.getValue();
					if(custpay != null){
						sum =des+" "+"Collect Payment - "+total+" "+"/-";
					}
					
					
					taDescription.setValue(sum);
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
	
	}

}
