package com.slicktechnologies.client.views.multipleserviceschedule;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.TabPanel;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.service.CustomerServiceMaterialTable;
import com.slicktechnologies.client.views.technicianwarehousedetails.TechnicianServiceSchedulePopup;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;

public class MultipleServicesSchedulePopup extends PopupScreen implements ClickHandler{

	public TabPanel tabPanel;
	public MultipleServicesScheduleTable multipleServicesScheduleTable;
	CustomerServiceMaterialTable customerServiceMaterialTable;
	List<Service> serviceList = new ArrayList<Service>();
	Map<Integer , ServiceProject> serviceProjectList = new HashMap<Integer , ServiceProject>();
	GenricServiceAsync service = GWT.create(GenricService.class);
	Map<String, ProductGroupList> technicianTotalMaterialMap = new HashMap<String, ProductGroupList>();
	GeneralServiceAsync genService = GWT.create(GeneralService.class);
	GWTCAlert alert = new GWTCAlert();
	
	public MultipleServicesSchedulePopup(){
		super();
		multipleServicesScheduleTable.connectToLocal();

		getPopup().setHeight("560px");
		multipleServicesScheduleTable.getTable().setHeight("400px");
		/** Date 03-07-2018 by vijay for popup proper width ***/
		multipleServicesScheduleTable.getTable().setWidth("1400px");
		customerServiceMaterialTable.connectToLocal();
		customerServiceMaterialTable.getTable().setHeight("400px");
		/** Date 03-07-2018 by vijay for popup proper width ***/
		customerServiceMaterialTable.getTable().setWidth("1000px");
		getLblOk().setText("Save Services");
		getLblOk().setVisible(true);
		//getLblCancel().setVisible(t);
//		getLblOk().addClickHandler(this);
		getLblCancel().addClickHandler(this);
		createGui();
		getPopup().getElement().addClassName("multipleserviceschedulepopup");

	}
	@SuppressWarnings("unused")
	private void initalizeWidget() {
		multipleServicesScheduleTable = new MultipleServicesScheduleTable();
		customerServiceMaterialTable = new CustomerServiceMaterialTable(true);
		FlowPanel serviceDetailsPanel= new FlowPanel();
		FlowPanel summaryDetailsPnel = new FlowPanel();
		customerServiceMaterialTable.setEnable(false);
		serviceDetailsPanel.add(multipleServicesScheduleTable.getTable());
		summaryDetailsPnel.add(customerServiceMaterialTable.getTable());
		tabPanel = new TabPanel();
		tabPanel.addSelectionHandler(new SelectionHandler<Integer>() {
			@Override
			public void onSelection(SelectionEvent<Integer> event) {
				int tabId = event.getSelectedItem();
				System.out.println("TAB ID " + tabId);
				if (tabId == 0) {
					multipleServicesScheduleTable.getTable().redraw();
					if(getLblOk() != null){
					getLblOk().setText("Save Services");
					getLblOk().setVisible(true);
					}else{
						setLblOk(new InlineLabel("Save Services"));
						getLblOk().setVisible(true);
					}
				} else if (tabId == 1) {
					boolean flag = AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "TechnicianwiseMaterialPopup");
					if(flag){
						validateProjectDetails();
					}else{
						customerServiceMaterialTable.setEnable(false);
						customerServiceMaterialTable.getTable().redraw();
						setSummaryDetails();
						getLblOk().setText("Schedule (Assign to technician)");//Ashwini Patil Date:5-02-2024 added  ( Assign to technician)
						getLblOk().setVisible(true);
					}

				} 
			}
		});
		tabPanel.add(serviceDetailsPanel, "Service Details");
		tabPanel.add(summaryDetailsPnel, "Service Summary Details");
		
		tabPanel.setSize("900", "450px");
		
		tabPanel.selectTab(0);
	}
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		if(getLblCancel() ==  event.getSource()){
			this.hidePopUp();
			return;
		}
//		if(getLblOk() == event.getSource()){
//		if(getLblOk().getText().equals("Save")){
//			getLblOk().setText("Processing");
//			saveServiceDetails(multipleServicesScheduleTable.getDataprovider().getList());
//		}
//		if(getLblOk().getText().equals("Schedule")){
//			Console.log("schedule popup:" );
//			getLblOk().setText("Processing");
//			scheduleServices();
//		}
//		}
		
	}

	@Override
	public void createScreen() {
		// TODO Auto-generated method stub
		initalizeWidget();
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingServicesInfo = fbuilder
				.setlabel("Multiple Services Schedule")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(4).build();
		fbuilder = new FormFieldBuilder(" ", tabPanel);
		FormField ftabPanel = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(4).build();
		

		FormField[][] formfield = {
				{ fgroupingServicesInfo   },
				{ftabPanel } };
		this.fields = formfield;
		
		
	}
	public MultipleServicesScheduleTable getMultipleServicesScheduleTable() {
		return multipleServicesScheduleTable;
	}
	public void setMultipleServicesScheduleTable(
			MultipleServicesScheduleTable multipleServicesScheduleTable) {
		this.multipleServicesScheduleTable = multipleServicesScheduleTable;
	}
	public CustomerServiceMaterialTable getCustomerServiceMaterialTable() {
		return customerServiceMaterialTable;
	}
	public void setCustomerServiceMaterialTable(
			CustomerServiceMaterialTable customerServiceMaterialTable) {
		this.customerServiceMaterialTable = customerServiceMaterialTable;
	}
	public List<Service> getServiceList() {
		return serviceList;
	}
	public void setServiceList(List<Service> serviceList) {
		ArrayList<Service> list = new ArrayList<Service>();
		if(serviceList != null){
			list.addAll(serviceList);
			this.serviceList = list;
		}
		
	}
	public Map<Integer, ServiceProject> getServiceProjectList() {
		return serviceProjectList;
	}
	public void setServiceProjectList(
			Map<Integer, ServiceProject> serviceProjectList) {
		this.serviceProjectList = serviceProjectList;
	}

//	public void saveServiceDetails(List<Service> serviceList){
//		ArrayList<Service> list = new ArrayList<Service>();
//		list.addAll(serviceList);
//		genService.saveSelectedService(list,UserConfiguration.getCompanyId() , new AsyncCallback<String>() {
//
//			@Override
//			public void onFailure(Throwable caught) {
//				// TODO Auto-generated method stub
//				GWTCAlert alert = new GWTCAlert();
//				getLblOk().setText("Save");
//				alert.alert("Saved successfully.");
//			}
//
//			@Override
//			public void onSuccess(String result) {
//				// TODO Auto-generated method stub
//				GWTCAlert alert = new GWTCAlert();
//				getLblOk().setText("Save");
//				alert.alert("Saved successfully.");
//			}
//		});
//	}
	
	public void setSummaryDetails(){
		List<Service> serviceList = multipleServicesScheduleTable.getDataprovider().getList();
		ArrayList<Integer> serIdList = new ArrayList<Integer>();
		ArrayList<Integer> contractIdList = new ArrayList<Integer>();
		for(Service ser :serviceList){
			serIdList.add(ser.getCount());
			contractIdList.add(ser.getContractCount());
		}
		this.serviceList = new ArrayList<Service>();
		this.serviceList.addAll(serviceList);
		genService.getCombinedMaterialList(UserConfiguration.getCompanyId(), serIdList,contractIdList, new AsyncCallback<ArrayList<ProductGroupList>>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(ArrayList<ProductGroupList> result) {
				// TODO Auto-generated method stub
				Console.log("result size :" + result.size());
				customerServiceMaterialTable.getDataprovider().setList(result);
				customerServiceMaterialTable.getTable().redraw();
			}
		});
//							
	}
//	public void scheduleServices(){
//		
//		Console.log("schedule service.");
//		List<Service> serviceList = multipleServicesScheduleTable.getDataprovider().getList();
//		Console.log("schedule service size :"+ serviceList.size());
//		ArrayList<Integer> serIdList = new ArrayList<Integer>();
//		for(Service ser :serviceList){
//			serIdList.add(ser.getCount());
//		}
//		Console.log("schedule service id size :"+ serIdList.size());
//		
//		customerServiceMaterialTable.getDataprovider().getList();
//		ArrayList<ProductGroupList> list = new ArrayList<ProductGroupList>();
//		list.addAll(customerServiceMaterialTable.getDataprovider().getList());
//		genService.createMMNForTechnician(UserConfiguration.getCompanyId(), list, LoginPresenter.loggedInUser,serIdList, new AsyncCallback<String>() {
//
//			@Override
//			public void onFailure(Throwable caught) {
//				// TODO Auto-generated method stub
//				System.out.print("error"  +caught);
//				GWTCAlert alert = new GWTCAlert();
//				getLblOk().setText("Schedule");
//				alert.alert("Falied.");
//			}
//
//			@Override
//			public void onSuccess(String result) {
//				// TODO Auto-generated method stub
//				GWTCAlert alert = new GWTCAlert();
//				getLblOk().setText("Schedule");
//				alert.alert("Scheduled successfully.");
//				hidePopUp();
//			}
//		});
//	}
	public TabPanel getTabPanel() {
		return tabPanel;
	}
	public void setTabPanel(TabPanel tabPanel) {
		this.tabPanel = tabPanel;
	}
	
	private void validateProjectDetails(){
		
		List<Service> serviceList = multipleServicesScheduleTable.getDataprovider().getList();
		ArrayList<Service> serviceList1 = new ArrayList<Service>();
		serviceList1.addAll(serviceList);
		genService.validateTechnicianWarehouse(serviceList1, new ArrayList<ProductGroupList>(), new AsyncCallback<ArrayList<String>>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				Console.log("Error 1: "+ caught);
				//multipleServicesScheduleTable.getLblOk().setText("Schedule");
			}

			@Override
			public void onSuccess(ArrayList<String> result) {
				// TODO Auto-generated method stub
				Console.log("result : "+ result);
				if(result != null && result.size() > 0){
					GWTCAlert alert = new GWTCAlert();
					alert.alert(result.toString());
					tabPanel.selectTab(0);
					//multipleServicesScheduleTable.getLblOk().setText("Schedule");
				}else{
					customerServiceMaterialTable.setEnable(false);
					customerServiceMaterialTable.getTable().redraw();
					setSummaryDetails();
					getLblOk().setText("Schedule (Assign to technician)");//Ashwini Patil Date:5-02-2024 added  ( Assign to technician)
					getLblOk().setVisible(true);
				}
			}
		});
		
	}
}
