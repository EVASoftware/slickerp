package com.slicktechnologies.client.views.salesquotation;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.shared.common.salesorder.OtherChargesMargin;
import com.slicktechnologies.shared.common.salesorder.VendorMargin;

public class VendorMarginTable extends SuperTable<VendorMargin> {
	TextColumn<VendorMargin> productNameCol;
	TextColumn<VendorMargin> salesPriceCol;
	Column<VendorMargin,String> vendorPriceCol;
	TextColumn<VendorMargin> vendorMarginCol;
	Column<VendorMargin,String> remarkCol;
	
	TextColumn<VendorMargin> viewVendorPriceCol;
	TextColumn<VendorMargin> viewRemarkCol;
	
	GWTCAlert alert;
	NumberFormat nf=NumberFormat.getFormat("0.00");
	
	
	boolean purchaseFlag=false;
	
	public VendorMarginTable() {
		super();
		purchaseFlag=false;
	}
	
	@Override
	public void createTable() {
		alert=new GWTCAlert();
		productNameCol();
		salesPriceCol();
		vendorPriceCol();
		vendorMarginCol();
		remarkCol();
	}
	
	private void viewVendorPriceCol() {
		// TODO Auto-generated method stub
		viewVendorPriceCol=new TextColumn<VendorMargin>() {
			@Override
			public String getValue(VendorMargin object) {
				if(object.getVendorPrice()!=0){
					return object.getVendorPrice()+"";
				}
				return "";
			}
		};
		table.addColumn(viewVendorPriceCol,"Vendor Price (Sales Dept.)");
	}
	
	private void viewRemarkCol() {
		// TODO Auto-generated method stub
		viewRemarkCol=new TextColumn<VendorMargin>() {
			@Override
			public String getValue(VendorMargin object) {
				if(object.getRemark()!=null){
					return object.getRemark();
				}
				return "";
			}
		};
		table.addColumn(viewRemarkCol,"Remark");
	}

	private void productNameCol() {
		// TODO Auto-generated method stub
		productNameCol=new TextColumn<VendorMargin>() {
			@Override
			public String getValue(VendorMargin object) {
				if(object.getProductName()!=null){
					return object.getProductName();
				}
				return "";
			}
		};
		table.addColumn(productNameCol,"Product Name");
//		table.setColumnWidth(productNameCol,110, Unit.PX);
	}
	
	private void salesPriceCol() {
		// TODO Auto-generated method stub
		salesPriceCol=new TextColumn<VendorMargin>() {
			@Override
			public String getValue(VendorMargin object) {
				if(object.getSalesPrice()!=0){
					return object.getSalesPrice()+"";
				}
				return "";
			}
		};
		table.addColumn(salesPriceCol,"Sales Price");
//		table.setColumnWidth(salesPriceCol,110, Unit.PX);
	}
	
	private void vendorPriceCol() {
		// TODO Auto-generated method stub
		EditTextCell editCell=new EditTextCell();
		vendorPriceCol=new Column<VendorMargin,String>(editCell) {
			@Override
			public String getValue(VendorMargin object) {
				if(object.getVendorPrice()!=0){
					return object.getVendorPrice()+"";
				}
				return "";
			}
			
			@Override
			public void onBrowserEvent(Context context, Element elem,VendorMargin object, NativeEvent event) {
				if(object.isReadOnly()==false
						||((UserConfiguration.getRole()!=null&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin"))
						||(UserConfiguration.getRole()!=null&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Zonal Coordinator")))){
					super.onBrowserEvent(context, elem, object, event);
				}
			}
		};
		table.addColumn(vendorPriceCol,"#Vendor Price (Sales Dept.)");
//		table.setColumnWidth(vendorPriceCol,110, Unit.PX);
		
		vendorPriceCol.setFieldUpdater(new FieldUpdater<VendorMargin, String>(){
			@Override
			public void update(int index, VendorMargin object,String value) {
				try{
					if(value!=null){
						double price=Double.parseDouble(value);
						object.setVendorPrice(price);
						if(object.getVendorPrice()==0){
							object.setVendorMargin(0);
						}else{
							object.setVendorMargin(object.getSalesPrice()-object.getVendorPrice());
						}
						
					}else{
						object.setVendorPrice(0);
						object.setVendorMargin(0);
					}
				}
				catch (Exception e){
					alert.alert("Please enter numeric value only.");
				}
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				table.redrawRow(index);
			}
		});
	}
	
	private void vendorMarginCol() {
		// TODO Auto-generated method stub
		vendorMarginCol=new TextColumn<VendorMargin>() {
			@Override
			public String getValue(VendorMargin object) {
				if(object.getVendorMargin()!=0){
					return nf.format(object.getVendorMargin());
				}
				return "";
			}
		};
		table.addColumn(vendorMarginCol,"Vendor Margin (Sales Dept.)");
//		table.setColumnWidth(vendorMarginCol,90, Unit.PX);
	}

	private void remarkCol() {
		// TODO Auto-generated method stub
		EditTextCell editCell=new EditTextCell();
		remarkCol=new Column<VendorMargin,String>(editCell) {
			@Override
			public String getValue(VendorMargin object) {
				if(object.getRemark()!=null){
					return object.getRemark();
				}
				return "";
			}
			
			@Override
			public void onBrowserEvent(Context context, Element elem,VendorMargin object, NativeEvent event) {
				if(object.isReadOnly()==false
						||((UserConfiguration.getRole()!=null&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin"))
						||(UserConfiguration.getRole()!=null&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Zonal Coordinator")))){
					super.onBrowserEvent(context, elem, object, event);
				}
			}
		};
		table.addColumn(remarkCol,"#Remark");
//		table.setColumnWidth(vendorPriceCol,110, Unit.PX);
		
		remarkCol.setFieldUpdater(new FieldUpdater<VendorMargin, String>(){
			@Override
			public void update(int index, VendorMargin object,String value) {
				try{
					if(value!=null){
						object.setRemark(value);
					}else{
						object.setRemark(null);
					}
				}
				catch (Exception e){
				}
				table.redrawRow(index);
			}
		});
	}
	
	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		int tablecolcount = this.table.getColumnCount();
		for (int i = tablecolcount - 1; i > -1; i--){
			table.removeColumn(i);
		}
		if (state == true){
			if(purchaseFlag){
				addEditablePurchaseCol();
			}else{
				addEditableSalesCol();
			}
		}
		if (state == false){
			if(purchaseFlag){
				addViewPurchaseCol();
			}else{
				addViewSalesCol();
			}
		}
	}

	private void addViewSalesCol() {
		productNameCol();
		salesPriceCol();
		viewVendorPriceCol();
		vendorMarginCol();
		viewRemarkCol();
	}
	private void addEditableSalesCol() {
		productNameCol();
		salesPriceCol();
		vendorPriceCol();
		vendorMarginCol();
		remarkCol();
	}

	private void addEditablePurchaseCol() {
		productNameCol();
		salesVendorPriceCol();
		purchasePriceCol();
		purchaseMarginCol();
		salesRemarkCol();
		purchaseRemarkCol();
	}
	
	private void addViewPurchaseCol() {
		productNameCol();
		salesVendorPriceCol();
		purchasePriceCol();
		purchaseMarginCol();
		salesRemarkCol();
		viewPurchaseRemarkCol();
	}


	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
	
	TextColumn<VendorMargin> salesVendorPriceCol;
	TextColumn<VendorMargin> salesRemarkCol;
	TextColumn<VendorMargin> purchasePriceCol;
	TextColumn<VendorMargin> purchaseMarginCol;
	Column<VendorMargin,String> purchaseRemarkCol;
	
	TextColumn<VendorMargin> viewPurchaseRemarkCol;
	
	public VendorMarginTable(boolean flag) {
		super(flag);
		createTable1();
		purchaseFlag=true;
	}
	
	public void createTable1() {
		alert=new GWTCAlert();
		productNameCol();
		salesVendorPriceCol();
		purchasePriceCol();
		purchaseMarginCol();
		salesRemarkCol();
		purchaseRemarkCol();
	}
	
	private void viewPurchaseRemarkCol() {
		// TODO Auto-generated method stub
		viewPurchaseRemarkCol=new TextColumn<VendorMargin>() {
			@Override
			public String getValue(VendorMargin object) {
				if(object.getPurchaseRemark()!=null){
					return object.getPurchaseRemark();
				}
				return "";
			}
		};
		table.addColumn(viewPurchaseRemarkCol,"Remark (Purchase)");
	}

	private void salesVendorPriceCol() {
		// TODO Auto-generated method stub
		salesVendorPriceCol=new TextColumn<VendorMargin>() {
			@Override
			public String getValue(VendorMargin object) {
				return object.getVendorPrice()+"";
			}
		};
		table.addColumn(salesVendorPriceCol,"Vendor Price (Sales Dept.)");
	}

	private void purchasePriceCol() {
		// TODO Auto-generated method stub
		purchasePriceCol=new TextColumn<VendorMargin>() {
			@Override
			public String getValue(VendorMargin object) {
				return object.getPurchasePrice()+"";
			}
		};
		table.addColumn(purchasePriceCol,"Purchase Price");
	}

	private void purchaseMarginCol() {
		// TODO Auto-generated method stub
		purchaseMarginCol=new TextColumn<VendorMargin>() {
			@Override
			public String getValue(VendorMargin object) {
				return object.getPurchaseMargin()+"";
			}
		};
		table.addColumn(purchaseMarginCol,"Purchase Margin");
	}
	
	private void salesRemarkCol() {
		// TODO Auto-generated method stub
		salesRemarkCol=new TextColumn<VendorMargin>() {
			@Override
			public String getValue(VendorMargin object) {
				if(object.getRemark()!=null){
					return object.getRemark();
				}
				return "";
			}
		};
		table.addColumn(salesRemarkCol,"Remark (Sales)");
	}

	private void purchaseRemarkCol() {
		// TODO Auto-generated method stub
		EditTextCell editCell=new EditTextCell();
		purchaseRemarkCol=new Column<VendorMargin,String>(editCell) {
			@Override
			public String getValue(VendorMargin object) {
				if(object.getPurchaseRemark()!=null){
					return object.getPurchaseRemark();
				}
				return "";
			}
			
			@Override
			public void onBrowserEvent(Context context, Element elem,VendorMargin object, NativeEvent event) {
				if(object.isReadOnly()==false
						||((UserConfiguration.getRole()!=null&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin"))
						||(UserConfiguration.getRole()!=null&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Zonal Coordinator")))){
					super.onBrowserEvent(context, elem, object, event);
				}
			}
		};
		table.addColumn(purchaseRemarkCol,"#Remark (Purchase)");
		
		purchaseRemarkCol.setFieldUpdater(new FieldUpdater<VendorMargin, String>(){
			@Override
			public void update(int index, VendorMargin object,String value) {
				try{
					if(value!=null){
						object.setPurchaseRemark(value);
					}else{
						object.setPurchaseRemark(null);
					}
				}
				catch (Exception e){
				}
				table.redrawRow(index);
			}
		});
	
	}
	
	

}
