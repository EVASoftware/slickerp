package com.slicktechnologies.client.views.salesquotation;

import java.util.Vector;

import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.config.CategoryTypeFactory;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.customer.CustomerForm;
import com.slicktechnologies.client.views.salesquotation.SalesQuotationPresenter.SalesQuotationPresenterSearch;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.salesorder.SalesQuotation;

public class SalesQuotationPresenterSearchProxy extends SalesQuotationPresenterSearch {
	
	public PersonInfoComposite personInfo;
	public IntegerBox tbContractId;
	public IntegerBox tbQuotationId;
	public IntegerBox tbLeadId;
	public ObjectListBox<Branch> olbBranch;
	public ObjectListBox<Employee> olbEmployee;
	public DateComparator dateComparator;
	
	public ObjectListBox<Config> olbQuotationGroup;
	public ObjectListBox<ConfigCategory> olbQuotationCategory;
	public ObjectListBox<Type> olbQuotationType;
	public ListBox olbQuotationStatus;
	public ObjectListBox<Config> olbQuotationPriority;
	
	/** Date 09/03/2018 added by komal for followup date **/
	public DateComparator dateComparatorFollowUp;
	
	public Object getVarRef(String varName)
	{
		if(varName.equals("personInfo"))
			return personInfo;
		if(varName.equals("tbContractId"))
			return this.tbContractId;
		if(varName.equals("tbQuotationId"))
			return this.tbQuotationId;
		if(varName.equals("tbLeadId"))
			return this.tbLeadId;
		if(varName.equals("olbBranch"))
			return this.olbBranch;
		if(varName.equals("olbEmployee"))
			return this.olbEmployee;
		if(varName.equals("dateComparator"))
			return this.dateComparator;
		
		if(varName.equals("olbQuotationGroup"))
			return this.olbQuotationGroup;
					
		if(varName.equals("olbQuotationCategory"))
			return this.olbQuotationCategory;
					
		if(varName.equals("olbQuotationType"))
			return this.olbQuotationType;
					
		if(varName.equals("olbQuotationStatus"))
			return this.olbQuotationStatus;
								
		if(varName.equals("olbQuotationPriority"))
			return this.olbQuotationPriority;
		/** Date 09/03/2018 added by komal for followup date **/
		if(varName.equals("dateComparatorFollowUp"))
			return this.dateComparatorFollowUp;
		return null ;
	}
	
	public SalesQuotationPresenterSearchProxy()
	{
		super();
		createGui();
	}
	
	public void initWidget()
	{
		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new Customer());
		personInfo=new PersonInfoComposite(querry,false);
		tbContractId= new IntegerBox();
		tbQuotationId= new IntegerBox();
		tbLeadId= new IntegerBox();
		olbBranch= new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbBranch);
		olbEmployee= new ObjectListBox<Employee>();
//		AppUtility.makeSalesPersonListBoxLive(olbEmployee);
		olbEmployee.makeEmployeeLive(AppConstants.SALESMODULE, AppConstants.QUOTATION, "Sales Person");
		
		dateComparator= new DateComparator("creationDate",new SalesQuotation());
		
		olbQuotationGroup=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbQuotationGroup, Screen.SALESQUOTATIONGROUP);
		
		olbQuotationCategory=new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(olbQuotationCategory, Screen.SALESQUOTATIONCATEGORY);
		
		olbQuotationType=new ObjectListBox<Type>();
		AppUtility.makeTypeListBoxLive(olbQuotationType, Screen.SALESQUOTATIONTYPE);
		olbQuotationStatus=new ListBox();
		AppUtility.setStatusListBox(olbQuotationStatus, SalesQuotation.getStatusList());
		
		olbQuotationPriority=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbQuotationPriority, Screen.SALESQUOTATIONPRIORITY);
	
		
		personInfo.getCustomerId().getHeaderLabel().setText("Customer ID");
		personInfo.getCustomerName().getHeaderLabel().setText("Customer Name");
		personInfo.getCustomerCell().getHeaderLabel().setText("Customer Cell");
		/** Date 09/03/2018 added by komal for followup date **/
		dateComparatorFollowUp = new DateComparator("followUpDate" , new SalesQuotation());
	}
	public void createScreen()
	{
		initWidget();
		FormFieldBuilder builder;
		builder = new FormFieldBuilder("",personInfo);
		FormField fpersonInfo= builder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		builder = new FormFieldBuilder("Sales Order Id",tbContractId);
		FormField ftbContractId= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Quotation Id",tbQuotationId);
		FormField ftbQuotationId= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Lead Id",tbLeadId);
		FormField ftbLeadId= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Branch",olbBranch);
		FormField folbBranch= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Sales Person",olbEmployee);
		FormField folbEmployee= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("From Date (Creation Date)",dateComparator.getFromDate());
		FormField fdateComparator= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("To Date (Creation Date)",dateComparator.getToDate());
		FormField fdateComparator1= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		builder = new FormFieldBuilder("Quotation Group",olbQuotationGroup);
		FormField folbQuotationGroup= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Quotation Category",olbQuotationCategory);
		FormField folbQuotationCategory= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Quotation Type",olbQuotationType);
		FormField folbQuotationType= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Quotation Status",olbQuotationStatus);
		FormField folbQuotationStatus= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Quotation Priority",olbQuotationPriority);
		FormField folbQuotationPriority= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/** date 09/03/2018 added by komal for followup date **/
		builder = new FormFieldBuilder("From Date (Follow-up)",dateComparatorFollowUp.getFromDate());
		FormField ffollowupdateComparator= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("To Date (Follow-up)",dateComparatorFollowUp.getToDate());
		FormField ffollowupdateComparator1 = builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		this.fields=new FormField[][]{
				//fdateComparator,fdateComparator1, Ashwini Patil Removed creation date filter as per Nitin sir's Instruction. As there is no index for it.
				{ffollowupdateComparator , ffollowupdateComparator1 ,folbQuotationGroup},//date 09/03/2018 changed by komal
				{folbQuotationCategory,folbQuotationType,folbQuotationStatus,folbEmployee,folbBranch},
				{folbQuotationPriority,ftbQuotationId,ftbLeadId,ftbContractId},
				{fpersonInfo}
		};
	}
	public MyQuerry getQuerry()
	{
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		if(dateComparator.getValue()!=null)
		{
			filtervec.addAll(dateComparator.getValue());
		}
		if(olbEmployee.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbEmployee.getValue().trim());
			temp.setQuerryString("employee");
			filtervec.add(temp);
		}
		if(olbBranch.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbBranch.getValue().trim());
			temp.setQuerryString("branch");
			filtervec.add(temp);
		}
		
		if(personInfo.getIdValue()!=-1)
		  {
		  temp=new Filter();
		  temp.setIntValue(personInfo.getIdValue());
		  temp.setQuerryString("cinfo.count");
		  filtervec.add(temp);
		  }
		  
		  if(!(personInfo.getFullNameValue().equals("")))
		  {
		  temp=new Filter();
		  temp.setStringValue(personInfo.getFullNameValue());
		  temp.setQuerryString("cinfo.fullName");
		  filtervec.add(temp);
		  }
		  if(personInfo.getCellValue()!=-1l)
		  {
		  temp=new Filter();
		  temp.setLongValue(personInfo.getCellValue());
		  temp.setQuerryString("cinfo.cellNumber");
		  filtervec.add(temp);
		  }
		if(tbQuotationId.getValue()!=null){
			temp=new Filter();
			temp.setIntValue(tbQuotationId.getValue());
			temp.setQuerryString("count");
			filtervec.add(temp);
		}
		if(tbLeadId.getValue()!=null){
			temp=new Filter();
			temp.setIntValue(tbLeadId.getValue());
			filtervec.add(temp);
			temp.setQuerryString("leadCount");
			filtervec.add(temp);
		}
		if(tbContractId.getValue()!=null){
			temp=new Filter();
			temp.setIntValue(tbContractId.getValue());
			filtervec.add(temp);
			temp.setQuerryString("contractCount");
			filtervec.add(temp);
		}
		
		if(olbQuotationGroup.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbQuotationGroup.getValue().trim());
			temp.setQuerryString("group");
			filtervec.add(temp);
		}
		
		if(olbQuotationCategory.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbQuotationCategory.getValue().trim());
			temp.setQuerryString("category");
			filtervec.add(temp);
		}
		
		if(olbQuotationType.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbQuotationType.getValue().trim());
			temp.setQuerryString("type");
			filtervec.add(temp);
		}
		
		if(olbQuotationStatus.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbQuotationStatus.getValue(olbQuotationStatus.getSelectedIndex()).trim());
			temp.setQuerryString("status");
			filtervec.add(temp);
		}
		
		if(olbQuotationPriority.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbQuotationPriority.getValue().trim());
			temp.setQuerryString("priority");
			filtervec.add(temp);
		}
		/** date 09/03/2018 added by komal for followup date **/
		if(dateComparatorFollowUp.getValue()!=null)
		{
			filtervec.addAll(dateComparatorFollowUp.getValue());
		}
		MyQuerry querry= new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new SalesQuotation());
		return querry;
	}

	@Override
	public boolean validate() {
		if(LoginPresenter.branchRestrictionFlag)
		{
		if(olbBranch.getSelectedIndex()==0)
		{
			showDialogMessage("Select Branch");
			return false;
		}
		}		
		
		
		//Ashwini Patil Date:25-01-2024 	
		boolean filterAppliedFlag=false;
		if(dateComparator.getFromDate().getValue()!=null || dateComparator.getToDate().getValue()!=null||
				dateComparatorFollowUp.getFromDate().getValue()!=null || dateComparatorFollowUp.getToDate().getValue()!=null||
				olbBranch.getSelectedIndex()!=0 ||olbQuotationStatus.getSelectedIndex()!=0 ||olbQuotationGroup.getSelectedIndex()!=0 ||
				olbEmployee.getSelectedIndex()!=0 || personInfo.getIdValue()!=-1	|| !(personInfo.getFullNameValue().equals("")) || personInfo.getCellValue()!=-1l ||
			    tbLeadId.getValue()!=null || tbContractId.getValue()!=null ||
	    		olbQuotationCategory.getSelectedIndex()!=0 ||olbQuotationType.getSelectedIndex()!=0 || 
	    		olbQuotationPriority.getSelectedIndex()!=0 || tbQuotationId.getValue()!=null){
			
							filterAppliedFlag=true;
		}
						
		if(!filterAppliedFlag){
				showDialogMessage("Please apply at least one filter!");
				return false;
		}
					
		if((dateComparator.getFromDate().getValue()!=null && dateComparator.getToDate().getValue()!=null)){
				int diffdays = AppUtility.getDifferenceDays(dateComparator.getFromDate().getValue(), dateComparator.getToDate().getValue());
				Console.log("diffdays "+diffdays);
				if(diffdays>31){
						showDialogMessage("Please select from date and to date range within 30 days");
						return false;
				}
		}
		if((dateComparatorFollowUp.getFromDate().getValue()!=null && dateComparatorFollowUp.getToDate().getValue()!=null)){
					int diffdays = AppUtility.getDifferenceDays(dateComparatorFollowUp.getFromDate().getValue(), dateComparatorFollowUp.getToDate().getValue());
					Console.log("diffdays "+diffdays);
					if(diffdays>31){
						showDialogMessage("Please select from date and to date range within 30 days");
						return false;
					}
		}

				
		return super.validate();
	}


}
