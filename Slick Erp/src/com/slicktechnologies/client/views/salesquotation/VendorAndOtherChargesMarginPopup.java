package com.slicktechnologies.client.views.salesquotation;

import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Textbox;

import com.google.gwt.dom.client.Style.BorderStyle;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.salesorder.OtherChargesMargin;
import com.slicktechnologies.shared.common.salesorder.VendorMargin;

public class VendorAndOtherChargesMarginPopup extends PopupScreen implements RowCountChangeEvent.Handler {

	public VendorMarginTable vendorMarginTbl;
	public OtherChargesMarginTable otherChargesMarginTbl;
	public DoubleBox dbTotalVendorMargin;
	public DoubleBox dbOtherChargesMargin;
	
	FormField fdbTotalVendorMargin;
	FormField fdbOtherChargesMargin;
	public VendorAndOtherChargesMarginPopup() {
		super();
		createGui();
		
		fdbTotalVendorMargin.getHeaderLabel().getElement().getStyle().setMarginLeft(350, Unit.PX);
		fdbOtherChargesMargin.getHeaderLabel().getElement().getStyle().setMarginLeft(350, Unit.PX);
	
	}
	
	
	
	@Override
	public void applyStyle() {
//		super.applyStyle();
		
//		content.getElement().setId("popupformcontent");
		content.getElement().getStyle().setWidth(650, Unit.PX);
		content.getElement().getStyle().setBorderWidth(1, Unit.PX);
		content.getElement().getStyle().setBorderStyle(BorderStyle.SOLID);
		form.getElement().setId("form");
		horizontal.getElement().addClassName("popupcentering");
		lblOk.getElement().setId("addbutton");
		lblCancel.getElement().setId("addbutton");
		popup.center();
	}



	private void initializeWidget() {
		vendorMarginTbl=new VendorMarginTable();
		otherChargesMarginTbl=new OtherChargesMarginTable();
		
		vendorMarginTbl.getTable().addRowCountChangeHandler(this);
		otherChargesMarginTbl.getTable().addRowCountChangeHandler(this);
		dbTotalVendorMargin=new DoubleBox();
		dbTotalVendorMargin.setEnabled(false);
		dbOtherChargesMargin=new DoubleBox();
		dbOtherChargesMargin.setEnabled(false);
	}

	@Override
	public void createScreen() {
		// TODO Auto-generated method stub
		initializeWidget();
		
		FormFieldBuilder fbuilder;
		
		fbuilder = new FormFieldBuilder();
		FormField fVenGrping=fbuilder.setlabel("Vendor Price Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",vendorMarginTbl.getTable());
		FormField fvendorMarginTbl= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
	
		fbuilder = new FormFieldBuilder();
		FormField fOcGrping=fbuilder.setlabel("Other Charges Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",otherChargesMarginTbl.getTable());
		FormField fotherChargesMarginTbl= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		dbTotalVendorMargin.getElement().getStyle().setWidth(40, Unit.PCT);
		dbTotalVendorMargin.getElement().getStyle().setMarginLeft(350, Unit.PX);
		fbuilder = new FormFieldBuilder("Total Vendor Margin (Sales)",dbTotalVendorMargin);
		fdbTotalVendorMargin= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
		
		dbOtherChargesMargin.getElement().getStyle().setWidth(40, Unit.PCT);
		dbOtherChargesMargin.getElement().getStyle().setMarginLeft(350, Unit.PX);
		fbuilder = new FormFieldBuilder("Total Other Charges (Sales)",dbOtherChargesMargin);
		fdbOtherChargesMargin= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		
		
		fbuilder = new FormFieldBuilder();
		FormField fblankgroupthree=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		
		
		FormField[][] formfield = {
				{fVenGrping},
				{fvendorMarginTbl},
				{fblankgroupthree,fdbTotalVendorMargin},
				{fOcGrping},
				{fotherChargesMarginTbl},
				{fblankgroupthree,fdbOtherChargesMargin}
		};

		this.fields=formfield;
		
	}
	
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
	}
	
	
	public void clear() {
		// TODO Auto-generated method stub
		vendorMarginTbl.connectToLocal();
		otherChargesMarginTbl.connectToLocal();
		dbTotalVendorMargin.setValue(null);
		dbOtherChargesMargin.setValue(null);
	}



	@Override
	public void onRowCountChange(RowCountChangeEvent event) {
		if(event.getSource()==vendorMarginTbl.getTable()){
			double sum=0;
			for(VendorMargin venObj:vendorMarginTbl.getValue()){
				sum=sum+venObj.getVendorMargin();
			}
			dbTotalVendorMargin.setValue(sum);
		}
		
		if(event.getSource()==otherChargesMarginTbl.getTable()){
			double sum=0;
			for(OtherChargesMargin ocObj:otherChargesMarginTbl.getValue()){
				sum=sum+ocObj.getOtherCharges();
			}
			dbOtherChargesMargin.setValue(sum);
		}
	}
	
	public void reactOnCosting(List<SalesLineItem> itemList,List<VendorMargin> vendorMargins,List<OtherChargesMargin> otherChargesMargins) {
		if(itemList==null||itemList.size()==0){
			showDialogMessage("Please add product details first.");
			return;
		}
		clear();
		
		boolean updateFlag=true;
		if(vendorMargins!=null&&vendorMargins.size()!=0){
			updateFlag=false;
		}
		if(otherChargesMargins!=null&&otherChargesMargins.size()!=0){
			updateFlag=false;
		}
		
		Double totalVendorMargin=0d;
		Double totalOcMargin=0d;
		if(updateFlag){
			Console.log("New Entry in costing.....!!");
			ArrayList<VendorMargin> vendorMarginList=new ArrayList<VendorMargin>();
			ArrayList<OtherChargesMargin> otherChargesList=new ArrayList<OtherChargesMargin>();
			for(SalesLineItem item:itemList){
				VendorMargin venObj=new VendorMargin();
				venObj.setProductId(item.getPrduct().getCount());
				venObj.setProductSrNo(item.getProductSrNo());
				venObj.setProductName(item.getProductName());
				venObj.setSalesPrice(item.getTotalAmount());
				venObj.setVendorPrice(0);
				venObj.setVendorMargin(0);
				vendorMarginList.add(venObj);
				
				OtherChargesMargin ocObj=new OtherChargesMargin();
				ocObj.setProductId(item.getPrduct().getCount());
				ocObj.setProductSrNo(item.getProductSrNo());
				ocObj.setProductName(item.getProductName());
				ocObj.setOtherCharges(0);
				otherChargesList.add(ocObj);
			}
			vendorMarginTbl.setValue(vendorMarginList);
			otherChargesMarginTbl.setValue(otherChargesList);
		}else{
			Console.log("Updating Costing ...!!");
			for(SalesLineItem item:itemList){
				boolean addFlag=true;
				for(VendorMargin venObj:vendorMargins){
					if(venObj.getProductId()==item.getPrduct().getCount()&&venObj.getProductSrNo()==item.getProductSrNo()){
						addFlag=false;
						venObj.setProductName(item.getProductName());
						venObj.setSalesPrice(item.getTotalAmount());
						if(venObj.getVendorPrice()!=0){
							venObj.setVendorMargin(venObj.getSalesPrice()-venObj.getVendorPrice());
						}else{
							venObj.setVendorMargin(0);
						}
					}
				}
				if(addFlag){
					VendorMargin venObj=new VendorMargin();
					venObj.setProductId(item.getPrduct().getCount());
					venObj.setProductSrNo(item.getProductSrNo());
					venObj.setProductName(item.getProductName());
					venObj.setSalesPrice(item.getTotalAmount());
					venObj.setVendorPrice(0);
					venObj.setVendorMargin(0);
					vendorMargins.add(venObj);
				}
			}
			
			for(VendorMargin venObj:vendorMargins){
				boolean deleteFlag=true;
				for(SalesLineItem item:itemList){
					if(venObj.getProductId()==item.getPrduct().getCount()&&venObj.getProductSrNo()==item.getProductSrNo()){
						deleteFlag=false;
					}
				}
				if(deleteFlag){
					vendorMargins.remove(venObj);
				}
			}
			
			for(SalesLineItem item:itemList){
				boolean addFlag=true;
				for(OtherChargesMargin ocObj:otherChargesMargins){
					if(ocObj.getProductId()==item.getPrduct().getCount()&&ocObj.getProductSrNo()==item.getProductSrNo()){
						addFlag=false;
						ocObj.setProductName(item.getProductName());
					}
				}
				if(addFlag){
					OtherChargesMargin ocObj=new OtherChargesMargin();
					ocObj.setProductId(item.getPrduct().getCount());
					ocObj.setProductSrNo(item.getProductSrNo());
					ocObj.setProductName(item.getProductName());
					ocObj.setOtherCharges(0);
					otherChargesMargins.add(ocObj);
				}
			}
			
			for(OtherChargesMargin ocObj:otherChargesMargins){
				boolean deleteFlag=true;
				for(SalesLineItem item:itemList){
					if(ocObj.getProductId()==item.getPrduct().getCount()&&ocObj.getProductSrNo()==item.getProductSrNo()){
						deleteFlag=false;
					}
				}
				if(deleteFlag){
					otherChargesMargins.remove(ocObj);
				}
			}
			
			vendorMarginTbl.setValue(vendorMargins);
			otherChargesMarginTbl.setValue(otherChargesMargins);
			
			
			for(VendorMargin venObj:vendorMargins){
				totalVendorMargin=totalVendorMargin+venObj.getVendorMargin();
			}
			
			
			for(OtherChargesMargin ocObj:otherChargesMargins){
				totalOcMargin=totalOcMargin+ocObj.getOtherCharges();
			}
		}
		showPopUp();
		if(totalVendorMargin!=null){
			dbTotalVendorMargin.setValue(totalVendorMargin);
		}
		if(totalOcMargin!=null){
			dbOtherChargesMargin.setValue(totalOcMargin);
		}
		
	}
	
	public void reactOnCostingSave(List<SalesLineItem> itemList,List<VendorMargin> vendorMargins,List<OtherChargesMargin> otherChargesMargins) {
		boolean updateFlag=true;
		if(vendorMargins!=null&&vendorMargins.size()!=0){
			updateFlag=false;
		}
		if(otherChargesMargins!=null&&otherChargesMargins.size()!=0){
			updateFlag=false;
		}
		Double totalVendorMargin;
		Double totalOcMargin;
		if(updateFlag){
//			vendorMargins=new ArrayList<VendorMargin>();
//			otherChargesMargins=new ArrayList<OtherChargesMargin>();
			Console.log("SAVE New Entry in costing.....!!");
			for(SalesLineItem item:itemList){
				VendorMargin venObj=new VendorMargin();
				venObj.setProductId(item.getPrduct().getCount());
				venObj.setProductSrNo(item.getProductSrNo());
				venObj.setProductName(item.getProductName());
				venObj.setSalesPrice(item.getTotalAmount());
				venObj.setVendorPrice(0);
				venObj.setVendorMargin(0);
				vendorMargins.add(venObj);
				
				OtherChargesMargin ocObj=new OtherChargesMargin();
				ocObj.setProductId(item.getPrduct().getCount());
				ocObj.setProductSrNo(item.getProductSrNo());
				ocObj.setProductName(item.getProductName());
				ocObj.setOtherCharges(0);
				otherChargesMargins.add(ocObj);
			}
		}else{
			Console.log("SAVE Updating Costing ...!!");
			for(SalesLineItem item:itemList){
				boolean addFlag=true;
				for(VendorMargin venObj:vendorMargins){
					if(venObj.getProductId()==item.getPrduct().getCount()&&venObj.getProductSrNo()==item.getProductSrNo()){
						addFlag=false;
						venObj.setProductName(item.getProductName());
						venObj.setSalesPrice(item.getTotalAmount());
						if(venObj.getVendorMargin()!=0){
							venObj.setVendorMargin(venObj.getSalesPrice()-venObj.getVendorPrice());
						}else{
							venObj.setVendorMargin(0);
						}
					}
				}
				if(addFlag){
					VendorMargin venObj=new VendorMargin();
					venObj.setProductId(item.getPrduct().getCount());
					venObj.setProductSrNo(item.getProductSrNo());
					venObj.setProductName(item.getProductName());
					venObj.setSalesPrice(item.getTotalAmount());
					venObj.setVendorPrice(0);
					venObj.setVendorMargin(0);
					vendorMargins.add(venObj);
				}
			}
			
			for(VendorMargin venObj:vendorMargins){
				boolean deleteFlag=true;
				for(SalesLineItem item:itemList){
					if(venObj.getProductId()==item.getPrduct().getCount()&&venObj.getProductSrNo()==item.getProductSrNo()){
						deleteFlag=false;
					}
				}
				if(deleteFlag){
					vendorMargins.remove(venObj);
				}
			}
			
			for(SalesLineItem item:itemList){
				boolean addFlag=true;
				for(OtherChargesMargin ocObj:otherChargesMargins){
					if(ocObj.getProductId()==item.getPrduct().getCount()&&ocObj.getProductSrNo()==item.getProductSrNo()){
						addFlag=false;
						ocObj.setProductName(item.getProductName());
					}
				}
				if(addFlag){
					OtherChargesMargin ocObj=new OtherChargesMargin();
					ocObj.setProductId(item.getPrduct().getCount());
					ocObj.setProductSrNo(item.getProductSrNo());
					ocObj.setProductName(item.getProductName());
					ocObj.setOtherCharges(0);
					otherChargesMargins.add(ocObj);
				}
			}
			
			for(OtherChargesMargin ocObj:otherChargesMargins){
				boolean deleteFlag=true;
				for(SalesLineItem item:itemList){
					if(ocObj.getProductId()==item.getPrduct().getCount()&&ocObj.getProductSrNo()==item.getProductSrNo()){
						deleteFlag=false;
					}
				}
				if(deleteFlag){
					otherChargesMargins.remove(ocObj);
				}
			}
			totalVendorMargin=0d;
			for(VendorMargin venObj:vendorMargins){
				totalVendorMargin=totalVendorMargin+venObj.getVendorMargin();
			}
			
			totalOcMargin=0d;
			for(OtherChargesMargin ocObj:otherChargesMargins){
				totalOcMargin=totalOcMargin+ocObj.getOtherCharges();
			}
		}
	}

}
