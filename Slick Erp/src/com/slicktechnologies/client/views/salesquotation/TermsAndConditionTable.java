package com.slicktechnologies.client.views.salesquotation;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.helperlayer.TermsAndConditions;

public class TermsAndConditionTable extends SuperTable<TermsAndConditions>{

	
	TextColumn<TermsAndConditions> getcolumnTitle;
	Column<TermsAndConditions,String> deleteColumn;

 	
	@Override
	public void createTable() {
		// TODO Auto-generated method stub
		createViewColumnServicingTimeColumn();
		createColumndeleteColumn();

	}

	
	private void createViewColumnServicingTimeColumn() {
		getcolumnTitle=new TextColumn<TermsAndConditions>()
		{
			@Override
			public String getValue(TermsAndConditions object)
			{
				
				return object.getTitle();
				
			}
		};
		table.addColumn(getcolumnTitle,"Title");
		table.setColumnWidth(getcolumnTitle, 300,Unit.PX);
	}
	
	protected void createColumndeleteColumn()
	{
		ButtonCell btnCell= new ButtonCell();
		deleteColumn=new Column<TermsAndConditions,String>(btnCell)
				{
				@Override
				public String getValue(TermsAndConditions object)
				{
					return  "Delete" ;
				}
			};
		table.addColumn(deleteColumn,"Delete");
		table.setColumnWidth(deleteColumn, 100,Unit.PX);
		
		deleteColumn.setFieldUpdater(new FieldUpdater<TermsAndConditions,String>()
				{
			@Override
			public void update(int index,TermsAndConditions object,String value)
			{
				getDataprovider().getList().remove(object);
			}
		});
	}
	
	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {

		for(int i=table.getColumnCount()-1;i>-1;i--)
	    	  table.removeColumn(i); 
		
		if(state){
			createTable();
		}
		else{
			createViewColumnServicingTimeColumn();
		}
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
