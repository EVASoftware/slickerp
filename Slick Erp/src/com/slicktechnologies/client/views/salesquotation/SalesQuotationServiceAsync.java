package com.slicktechnologies.client.views.salesquotation;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.slicktechnologies.shared.common.salesorder.SalesQuotation;

public interface SalesQuotationServiceAsync {
	public void changeStatus(SalesQuotation q,AsyncCallback<Void>callback);
}
