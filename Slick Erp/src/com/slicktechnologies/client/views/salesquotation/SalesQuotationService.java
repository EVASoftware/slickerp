package com.slicktechnologies.client.views.salesquotation;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.slicktechnologies.shared.common.salesorder.SalesQuotation;
@RemoteServiceRelativePath("salesquotationservice")
public interface SalesQuotationService extends RemoteService {
	
	public void changeStatus(SalesQuotation q);

}
