package com.slicktechnologies.client.views.salesquotation;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.salesquotation.SalesQuotationPresenter.SalesQuotationPresenterTable;
import com.slicktechnologies.shared.common.salesorder.SalesQuotation;

public class SalesQuotationPresenterTableProxy extends SalesQuotationPresenterTable {
	
	TextColumn<SalesQuotation> getCustomerCellNumberColumn;
	TextColumn<SalesQuotation> getCustomerFullNameColumn;
	TextColumn<SalesQuotation> getCustomerIdColumn;
	TextColumn<SalesQuotation> getContractCountColumn;
	TextColumn<SalesQuotation> getQuotationCountColumn;
	TextColumn<SalesQuotation> getLeadCountColumn;
	TextColumn<SalesQuotation> getBranchColumn;
	TextColumn<SalesQuotation> getEmployeeColumn;
	TextColumn<SalesQuotation> getStatusColumn;
	TextColumn<SalesQuotation> getCreationDateColumn;
	TextColumn<SalesQuotation> getGroupColumn;
	TextColumn<SalesQuotation> getCategoryColumn;
	TextColumn<SalesQuotation> getTypeColumn;
	TextColumn<SalesQuotation> getCountColumn;
	TextColumn<SalesQuotation> getPriorityColumn;
	/** date 09/03/2018 added by komal for followup date **/
	TextColumn<SalesQuotation> getFollowUpDateColumn;
	
	public Object getVarRef(String varName)
	{
		if(varName.equals("getCustomerCellNumberColumn"))
			return this.getCustomerCellNumberColumn;
		if(varName.equals("getCustomerFullNameColumn"))
			return this.getCustomerFullNameColumn;
		if(varName.equals("getCustomerIdColumn"))
			return this.getCustomerIdColumn;
		if(varName.equals("getContractCountColumn"))
			return this.getContractCountColumn;
		if(varName.equals("getQuotationCountColumn"))
			return this.getQuotationCountColumn;
		if(varName.equals("getLeadCountColumn"))
			return this.getLeadCountColumn;
		if(varName.equals("getBranchColumn"))
			return this.getBranchColumn;
		if(varName.equals("getEmployeeColumn"))
			return this.getEmployeeColumn;
		if(varName.equals("getStatusColumn"))
			return this.getStatusColumn;
		if(varName.equals("getCreationDateColumn"))
			return this.getCreationDateColumn;
		if(varName.equals("getGroupColumn"))
			return this.getGroupColumn;
		if(varName.equals("getCategoryColumn"))
			return this.getCategoryColumn;
		if(varName.equals("getTypeColumn"))
			return this.getTypeColumn;
		if(varName.equals("getCountColumn"))
			return this.getCountColumn;
		/** date 09/03/2018 added by komal for followup date **/
		if(varName.equals("getFollowUpDateColumn"))
			return this.getFollowUpDateColumn;
		return null ;
	}
	
	
	public SalesQuotationPresenterTableProxy()
	{
		super();
	}
	
	
	@Override public void createTable() {
		addColumngetCount();
		/** date 09/03/2018 added by komal for followup date **/
		addCoulmnGetFollowUpDate();
		addColumngetLeadCount();
		addColumngetContractCount();
		addColumngetCustomerId();
		addColumngetCustomerFullName();
		addColumngetCustomerCellNumber();
		addColumngetEmployee();
		addColumngetBranch();
		addColumngetCreationDate();
		addColumngetGroup();
		addColumngetCategory();
		addColumngetType();
		addColumngetPriority();
		addColumngetStatus();
		
		}
	@Override
	protected void initializekeyprovider() {
		keyProvider= new ProvidesKey<SalesQuotation>()
				{
			@Override
			public Object getKey(SalesQuotation item)
			{
				if(item==null)
				{
					return null;
				}
				else
					return item.getId();
			}
				};
	}
	@Override
	public void setEnable(boolean state)
	{
	}
	@Override
	public void applyStyle()
	{
	}
	public void addColumnSorting(){
		addSortinggetCount();
		/** date 09/03/2018 added by komal for followup date **/
		addSortinggetFollowUpDate();
		addSortinggetBranch();
		addSortinggetCreationDate();
		addSortinggetEmployee();
		addSortinggetStatus();
		addSortinggetLeadCount();
		addSortinggetGroup();
		addSortinggetCategory();
		addSortinggetCustomerId();
		addSortinggetType();
		addSortinggetCustomerFullName();
		addSortinggetCustomerCellNumber();
		addSortinggetContractCount();
		addSortingQuotationPriority();
	}
	@Override public void addFieldUpdater() {
	}
	protected void addSortinggetCount()
	{
		List<SalesQuotation> list=getDataprovider().getList();
		columnSort=new ListHandler<SalesQuotation>(list);
		columnSort.setComparator(getCountColumn, new Comparator<SalesQuotation>()
				{
			@Override
			public int compare(SalesQuotation e1,SalesQuotation e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetCount()
	{
		getCountColumn=new TextColumn<SalesQuotation>()
				{
			@Override
			public String getValue(SalesQuotation object)
			{
				if( object.getCount()==-1)
					return "N.A";
				else return object.getCount()+"";
			}
				};
				table.addColumn(getCountColumn,"ID");
				table.setColumnWidth(getCountColumn,100,Unit.PX);
				getCountColumn.setSortable(true);
	}
	protected void addColumngetCreationDate()
	{
		DateTimeFormat fmt = DateTimeFormat.getFormat(PredefinedFormat.DATE_SHORT);
		DatePickerCell date= new DatePickerCell(fmt);
		getCreationDateColumn=new TextColumn<SalesQuotation>()
				{
			@Override
			public String getValue(SalesQuotation object)
			{
				if(object.getCreationDate()!=null)
				     return  AppUtility.parseDate(object.getCreationDate());
				  else 
					  return "N.A";
				
			}
				};
				table.addColumn(getCreationDateColumn,"Creation Date");
				table.setColumnWidth(getCreationDateColumn,150,Unit.PX);
				getCreationDateColumn.setSortable(true);
	}
	
	
	protected void addSortinggetCreationDate()
	{
		List<SalesQuotation> list=getDataprovider().getList();
		columnSort=new ListHandler<SalesQuotation>(list);
		columnSort.setComparator(getCreationDateColumn, new Comparator<SalesQuotation>()
				{
			@Override
			public int compare(SalesQuotation e1,SalesQuotation e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getCreationDate()!=null && e2.getCreationDate()!=null){
						return e1.getCreationDate().compareTo(e2.getCreationDate());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addSortinggetBranch()
	{
		List<SalesQuotation> list=getDataprovider().getList();
		columnSort=new ListHandler<SalesQuotation>(list);
		columnSort.setComparator(getBranchColumn, new Comparator<SalesQuotation>()
				{
			@Override
			public int compare(SalesQuotation e1,SalesQuotation e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBranch()!=null && e2.getBranch()!=null){
						return e1.getBranch().compareTo(e2.getBranch());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetBranch()
	{
		getBranchColumn=new TextColumn<SalesQuotation>()
				{
			@Override
			public String getValue(SalesQuotation object)
			{
				return object.getBranch()+"";
			}
				};
				table.addColumn(getBranchColumn,"Branch");
				table.setColumnWidth(getBranchColumn, 80, Unit.PX);
				getBranchColumn.setSortable(true);
	}
	protected void addSortinggetEmployee()
	{
		List<SalesQuotation> list=getDataprovider().getList();
		columnSort=new ListHandler<SalesQuotation>(list);
		columnSort.setComparator(getEmployeeColumn, new Comparator<SalesQuotation>()
				{
			@Override
			public int compare(SalesQuotation e1,SalesQuotation e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getEmployee()!=null && e2.getEmployee()!=null){
						return e1.getEmployee().compareTo(e2.getEmployee());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetEmployee()
	{
		getEmployeeColumn=new TextColumn<SalesQuotation>()
				{
			@Override
			public String getValue(SalesQuotation object)
			{
				return object.getEmployee()+"";
			}
				};
				table.addColumn(getEmployeeColumn,"Sales Person");
				table.setColumnWidth(getEmployeeColumn,150,Unit.PX);
				getEmployeeColumn.setSortable(true);
	}
	protected void addSortinggetStatus()
	{
		List<SalesQuotation> list=getDataprovider().getList();
		columnSort=new ListHandler<SalesQuotation>(list);
		columnSort.setComparator(getStatusColumn, new Comparator<SalesQuotation>()
				{
			@Override
			public int compare(SalesQuotation e1,SalesQuotation e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getStatus()!=null && e2.getStatus()!=null){
						return e1.getStatus().compareTo(e2.getStatus());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetStatus()
	{
		getStatusColumn=new TextColumn<SalesQuotation>()
				{
			@Override
			public String getValue(SalesQuotation object)
			{
				return object.getStatus()+"";
			}
				};
				table.addColumn(getStatusColumn,"Status");
				table.setColumnWidth(getStatusColumn,150,Unit.PX);
				getStatusColumn.setSortable(true);
	}
	protected void addSortinggetLeadCount()
	{
		List<SalesQuotation> list=getDataprovider().getList();
		columnSort=new ListHandler<SalesQuotation>(list);
		columnSort.setComparator(getLeadCountColumn, new Comparator<SalesQuotation>()
				{
			@Override
			public int compare(SalesQuotation e1,SalesQuotation e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getLeadCount()== e2.getLeadCount()){
						return 0;}
					if(e1.getLeadCount()> e2.getLeadCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetLeadCount()
	{
		getLeadCountColumn=new TextColumn<SalesQuotation>()
				{
			@Override
			public String getValue(SalesQuotation object)
			{
				if( object.getLeadCount()==-1)
					return "N.A";
				else return object.getLeadCount()+"";
			}
				};
				table.addColumn(getLeadCountColumn,"Lead ID");
				table.setColumnWidth(getLeadCountColumn,100,Unit.PX);
				getLeadCountColumn.setSortable(true);
	}
	protected void addSortinggetGroup()
	{
		List<SalesQuotation> list=getDataprovider().getList();
		columnSort=new ListHandler<SalesQuotation>(list);
		columnSort.setComparator(getGroupColumn, new Comparator<SalesQuotation>()
				{
			@Override
			public int compare(SalesQuotation e1,SalesQuotation e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getGroup()!=null && e2.getGroup()!=null){
						return e1.getGroup().compareTo(e2.getGroup());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetGroup()
	{
		getGroupColumn=new TextColumn<SalesQuotation>()
				{
			@Override
			public String getValue(SalesQuotation object)
			{
				return object.getGroup()+"";
			}
				};
				table.addColumn(getGroupColumn,"Group");
				table.setColumnWidth(getGroupColumn,150,Unit.PX);

				getGroupColumn.setSortable(true);
	}
	protected void addSortinggetQuotationCount()
	{
		List<SalesQuotation> list=getDataprovider().getList();
		columnSort=new ListHandler<SalesQuotation>(list);
		columnSort.setComparator(getQuotationCountColumn, new Comparator<SalesQuotation>()
				{
			@Override
			public int compare(SalesQuotation e1,SalesQuotation e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getQuotationCount()== e2.getQuotationCount()){
						return 0;}
					if(e1.getQuotationCount()> e2.getQuotationCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetQuotationCount()
	{
		getQuotationCountColumn=new TextColumn<SalesQuotation>()
				{
			@Override
			public String getValue(SalesQuotation object)
			{
				if( object.getQuotationCount()==-1)
					return "N.A";
				else return object.getQuotationCount()+"";
			}
				};
				table.addColumn(getQuotationCountColumn,"Quotation ID");
				getQuotationCountColumn.setSortable(true);
	}
	protected void addSortinggetCategory()
	{
		List<SalesQuotation> list=getDataprovider().getList();
		columnSort=new ListHandler<SalesQuotation>(list);
		columnSort.setComparator(getCategoryColumn, new Comparator<SalesQuotation>()
				{
			@Override
			public int compare(SalesQuotation e1,SalesQuotation e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getCategory()!=null && e2.getCategory()!=null){
						return e1.getCategory().compareTo(e2.getCategory());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetCategory()
	{
		getCategoryColumn=new TextColumn<SalesQuotation>()
				{
			@Override
			public String getValue(SalesQuotation object)
			{
				return object.getCategory()+"";
			}
				};
				table.addColumn(getCategoryColumn,"Category");
				table.setColumnWidth(getCategoryColumn,150,Unit.PX);
				getCategoryColumn.setSortable(true);
	}
	protected void addSortinggetCustomerId()
	{
		List<SalesQuotation> list=getDataprovider().getList();
		columnSort=new ListHandler<SalesQuotation>(list);
		columnSort.setComparator(getCustomerIdColumn, new Comparator<SalesQuotation>()
				{
			@Override
			public int compare(SalesQuotation e1,SalesQuotation e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCustomerId()== e2.getCustomerId()){
						return 0;}
					if(e1.getCustomerId()> e2.getCustomerId()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetCustomerId()
	{
		getCustomerIdColumn=new TextColumn<SalesQuotation>()
				{
			@Override
			public String getValue(SalesQuotation object)
			{
				if( object.getCustomerId()==-1)
					return "N.A";
				else return object.getCustomerId()+"";
			}
				};
				table.addColumn(getCustomerIdColumn,"Customer ID");
				table.setColumnWidth(getCustomerIdColumn,100,Unit.PX);
				getCustomerIdColumn.setSortable(true);
	}
	protected void addSortinggetType()
	{
		List<SalesQuotation> list=getDataprovider().getList();
		columnSort=new ListHandler<SalesQuotation>(list);
		columnSort.setComparator(getTypeColumn, new Comparator<SalesQuotation>()
				{
			@Override
			public int compare(SalesQuotation e1,SalesQuotation e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getType()!=null && e2.getType()!=null){
						return e1.getType().compareTo(e2.getType());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetType()
	{
		getTypeColumn=new TextColumn<SalesQuotation>()
				{
			@Override
			public String getValue(SalesQuotation object)
			{
				return object.getType()+"";
			}
				};
				table.addColumn(getTypeColumn,"Type");
				
				table.setColumnWidth(getTypeColumn,150,Unit.PX);
				getTypeColumn.setSortable(true);
	}
	
	protected void addSortingQuotationPriority()
	{
		List<SalesQuotation> list=getDataprovider().getList();
		columnSort=new ListHandler<SalesQuotation>(list);
		columnSort.setComparator(getPriorityColumn, new Comparator<SalesQuotation>()
				{
			@Override
			public int compare(SalesQuotation e1,SalesQuotation e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getPriority()!=null && e2.getPriority()!=null){
						return e1.getPriority().compareTo(e2.getPriority());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetPriority()
	{
		getPriorityColumn=new TextColumn<SalesQuotation>()
				{
			@Override
			public String getValue(SalesQuotation object)
			{
				return object.getPriority()+"";
			}
				};
				table.addColumn(getPriorityColumn,"Priority");
				table.setColumnWidth(getPriorityColumn,150,Unit.PX);
				getPriorityColumn.setSortable(true);
	}

	protected void addSortinggetCustomerFullName()
	{
		List<SalesQuotation> list=getDataprovider().getList();
		columnSort=new ListHandler<SalesQuotation>(list);
		columnSort.setComparator(getCustomerFullNameColumn, new Comparator<SalesQuotation>()
				{
			@Override
			public int compare(SalesQuotation e1,SalesQuotation e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getCustomerFullName()!=null && e2.getCustomerFullName()!=null){
						return e1.getCustomerFullName().compareTo(e2.getCustomerFullName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetCustomerFullName()
	{
		getCustomerFullNameColumn=new TextColumn<SalesQuotation>()
				{
			@Override
			public String getValue(SalesQuotation object)
			{
				return object.getCustomerFullName()+"";
			}
				};
				table.addColumn(getCustomerFullNameColumn,"Customer Name");
				table.setColumnWidth(getCustomerFullNameColumn,150,Unit.PX);
				getCustomerFullNameColumn.setSortable(true);
	}
	protected void addSortinggetCustomerCellNumber()
	{
		List<SalesQuotation> list=getDataprovider().getList();
		columnSort=new ListHandler<SalesQuotation>(list);
		columnSort.setComparator(getCustomerCellNumberColumn, new Comparator<SalesQuotation>()
				{
			@Override
			public int compare(SalesQuotation e1,SalesQuotation e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCustomerCellNumber()== e2.getCustomerCellNumber()){
						return 0;}
					if(e1.getCustomerCellNumber()> e2.getCustomerCellNumber()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetCustomerCellNumber()
	{
		getCustomerCellNumberColumn=new TextColumn<SalesQuotation>()
				{
			@Override
			public String getValue(SalesQuotation object)
			{
				return object.getCustomerCellNumber()+"";
			}
				};
				table.addColumn(getCustomerCellNumberColumn,"Customer Cell");
				table.setColumnWidth(getCustomerCellNumberColumn,150,Unit.PX);
				getCustomerCellNumberColumn.setSortable(true);
	}
	protected void addSortinggetContractCount()
	{
		List<SalesQuotation> list=getDataprovider().getList();
		columnSort=new ListHandler<SalesQuotation>(list);
		columnSort.setComparator(getContractCountColumn, new Comparator<SalesQuotation>()
				{
			@Override
			public int compare(SalesQuotation e1,SalesQuotation e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getContractCount()== e2.getContractCount()){
						return 0;}
					if(e1.getContractCount()> e2.getContractCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetContractCount()
	{
		getContractCountColumn=new TextColumn<SalesQuotation>()
				{
			@Override
			public String getValue(SalesQuotation object)
			{
				if( object.getContractCount()==-1)
					return "N.A";
				else return object.getContractCount()+"";
			}
				};
				table.addColumn(getContractCountColumn,"Sales Ord. ID");
				table.setColumnWidth(getContractCountColumn,100,Unit.PX);
				getContractCountColumn.setSortable(true);
	}
	/** date 09/03/2018 added by komal for followup date **/
	private void addCoulmnGetFollowUpDate() {
		getFollowUpDateColumn = new TextColumn<SalesQuotation>() {
			@Override
			public String getValue(SalesQuotation object) {
				if (object.getFollowUpDate() != null)
					return AppUtility.parseDate(object.getFollowUpDate());
				else
					return "N.A.";

			}
		};
		table.addColumn(getFollowUpDateColumn, "Follow-up Date");
		table.setColumnWidth(getFollowUpDateColumn, 120, Unit.PX);
		getFollowUpDateColumn.setSortable(true);
	}
	/** date 09/03/2018 added by komal for followup date **/
	private void addSortinggetFollowUpDate() {

		List<SalesQuotation> list = getDataprovider().getList();
		columnSort = new ListHandler<SalesQuotation>(list);
		columnSort.setComparator(getFollowUpDateColumn,
				new Comparator<SalesQuotation>() {
					@Override
					public int compare(SalesQuotation e1, SalesQuotation e2) {
						if (e1 != null && e2 != null) {
							if (e1.getFollowUpDate() == null
									&& e2.getFollowUpDate() == null) {
								return 0;
							}
							if (e1.getFollowUpDate() == null)
								return 1;
							if (e2.getFollowUpDate() == null)
								return -1;

							return e1.getFollowUpDate().compareTo(
									e2.getFollowUpDate());
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}


}
