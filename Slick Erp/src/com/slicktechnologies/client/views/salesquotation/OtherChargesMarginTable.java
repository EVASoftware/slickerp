package com.slicktechnologies.client.views.salesquotation;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.shared.BranchWiseScheduling;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetailsPO;
import com.slicktechnologies.shared.common.salesorder.OtherChargesMargin;

public class OtherChargesMarginTable extends SuperTable<OtherChargesMargin> {

	TextColumn<OtherChargesMargin> productNameCol;
	Column<OtherChargesMargin,String> otherChargesCol;
	Column<OtherChargesMargin,String> remarkCol;
	
	TextColumn<OtherChargesMargin> viewOtherChargesCol;
	TextColumn<OtherChargesMargin> viewRemarkCol;
	
	GWTCAlert alert;
	
	boolean purchaseFlag=false;
	
	public OtherChargesMarginTable() {
		super();
		purchaseFlag=false;
	}
	
	@Override
	public void createTable() {
		alert=new GWTCAlert();
		productNameCol();
		otherChargesCol();
		remarkCol();
	}
	
	private void viewOtherChargesCol() {
		// TODO Auto-generated method stub
		viewOtherChargesCol=new TextColumn<OtherChargesMargin>() {
			@Override
			public String getValue(OtherChargesMargin object) {
				if(object.getOtherCharges()!=0){
					return object.getOtherCharges()+"";
				}
				return "";
			}
		};
		table.addColumn(viewOtherChargesCol,"Other Charges");
	}
	
	private void viewRemarkCol() {
		// TODO Auto-generated method stub
		viewRemarkCol=new TextColumn<OtherChargesMargin>() {
			@Override
			public String getValue(OtherChargesMargin object) {
				if(object.getRemark()!=null){
					return object.getRemark();
				}
				return "";
			}
		};
		table.addColumn(viewRemarkCol,"Remark");
//		table.setColumnWidth(productNameCol,110, Unit.PX);
	}

	private void productNameCol() {
		// TODO Auto-generated method stub
		productNameCol=new TextColumn<OtherChargesMargin>() {
			@Override
			public String getValue(OtherChargesMargin object) {
				if(object.getProductName()!=null){
					return object.getProductName();
				}
				return "";
			}
		};
		table.addColumn(productNameCol,"Product Name");
//		table.setColumnWidth(productNameCol,110, Unit.PX);
	}
	
	private void otherChargesCol() {
		// TODO Auto-generated method stub
		EditTextCell editCell=new EditTextCell();
		otherChargesCol=new Column<OtherChargesMargin,String>(editCell) {
			@Override
			public String getValue(OtherChargesMargin object) {
				if(object.getOtherCharges()!=0){
					return object.getOtherCharges()+"";
				}
				return "";
			}
			
			@Override
			public void onBrowserEvent(Context context, Element elem,OtherChargesMargin object, NativeEvent event) {
				if(object.isReadOnly()==false
						||((UserConfiguration.getRole()!=null&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin"))
						||(UserConfiguration.getRole()!=null&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Zonal Coordinator")))){
					super.onBrowserEvent(context, elem, object, event);
				}
			}
		};
		table.addColumn(otherChargesCol,"#Other Charges");
//		table.setColumnWidth(vendorPriceCol,110, Unit.PX);
		
		otherChargesCol.setFieldUpdater(new FieldUpdater<OtherChargesMargin, String>(){
			@Override
			public void update(int index, OtherChargesMargin object,String value) {
				try{
					if(value!=null){
						double price=Double.parseDouble(value);
						object.setOtherCharges(price);
					}else{
						object.setOtherCharges(0);
					}
				}
				catch (Exception e){
					alert.alert("Please enter numeric value only.");
				}
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				table.redrawRow(index);
			}
		});
	}

	private void remarkCol() {
		// TODO Auto-generated method stub
		EditTextCell editCell=new EditTextCell();
		remarkCol=new Column<OtherChargesMargin,String>(editCell) {
			@Override
			public String getValue(OtherChargesMargin object) {
				if(object.getRemark()!=null){
					return object.getRemark();
				}
				return "";
			}
			
			@Override
			public void onBrowserEvent(Context context, Element elem,OtherChargesMargin object, NativeEvent event) {
				if(object.isReadOnly()==false
						||((UserConfiguration.getRole()!=null&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin"))
						||(UserConfiguration.getRole()!=null&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Zonal Coordinator")))){
					super.onBrowserEvent(context, elem, object, event);
				}
			}
		};
		table.addColumn(remarkCol,"#Remark");
//		table.setColumnWidth(vendorPriceCol,110, Unit.PX);
		
		remarkCol.setFieldUpdater(new FieldUpdater<OtherChargesMargin, String>(){
			@Override
			public void update(int index, OtherChargesMargin object,String value) {
				try{
					if(value!=null){
						object.setRemark(value);
					}else{
						object.setRemark(null);
					}
				}
				catch (Exception e){
				}
				table.redrawRow(index);
			}
		});
	}
	
	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		int tablecolcount = this.table.getColumnCount();
		for (int i = tablecolcount - 1; i > -1; i--){
			table.removeColumn(i);
		}
		if (state == true){
			if(purchaseFlag){
				addEditablePurchaseCol();
			}else{
				addEditableSalesCol();
			}
		}
		if (state == false){
			if(purchaseFlag){
				addViewPurchaseCol();
			}else{
				addViewSalesCol();
			}
		}		
	}

	private void addEditablePurchaseCol() {
		costTypeCol();
		viewOcChargesCol();
		remarkCol();
		deleteCol() ;
	}

	private void addViewPurchaseCol() {
		costTypeCol();
		viewOcChargesCol();
		viewRemarkCol();
	}

	private void addEditableSalesCol() {
		productNameCol();
		otherChargesCol();
		remarkCol();
	}

	private void addViewSalesCol() {
		productNameCol();
		viewOtherChargesCol();
		viewRemarkCol();
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
	
	public OtherChargesMarginTable(boolean flag) {
		super(flag);
		createTable1();
		purchaseFlag=true;
	}
	Column<OtherChargesMargin,String> deleteCol;
	TextColumn<OtherChargesMargin> viewOcChargesCol;
	public void createTable1() {
		alert=new GWTCAlert();
		costTypeCol();
		viewOcChargesCol();
		remarkCol();
		deleteCol() ;
		
	}
	
	private void costTypeCol() {
		// TODO Auto-generated method stub
		productNameCol=new TextColumn<OtherChargesMargin>() {
			@Override
			public String getValue(OtherChargesMargin object) {
				if(object.getCostType()!=null){
					return object.getCostType();
				}
				return "";
			}
		};
		table.addColumn(productNameCol,"Cost Type");
	}
	
	private void viewOcChargesCol() {
		// TODO Auto-generated method stub
		viewOcChargesCol=new TextColumn<OtherChargesMargin>() {
			@Override
			public String getValue(OtherChargesMargin object) {
				return object.getOtherCharges()+"";
			}
		};
		table.addColumn(viewOcChargesCol,"Amount");
	}
	private void deleteCol() {
		ButtonCell btnCell = new ButtonCell();
		deleteCol = new Column<OtherChargesMargin, String>(btnCell) {
			@Override
			public String getValue(OtherChargesMargin object) {
				return "Delete";
			}
			@Override
			public void render(Context context,OtherChargesMargin object,SafeHtmlBuilder sb) {
				if(object.isReadOnly()==false){
					super.render(context, object, sb);
				}
			}
//			@Override
//			public void onBrowserEvent(Context context, Element elem,OtherChargesMargin object, NativeEvent event) {
//				if(object.isReadOnly()==false
//						||((UserConfiguration.getRole()!=null&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin"))
//						||(UserConfiguration.getRole()!=null&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Zonal Coordinator")))){
//					super.onBrowserEvent(context, elem, object, event);
//				}
//			}
		};
		table.addColumn(deleteCol, "Delete");
		
		deleteCol.setFieldUpdater(new FieldUpdater<OtherChargesMargin, String>() {
			@Override
			public void update(int index, OtherChargesMargin object,String value) {
				getDataprovider().getList().remove(object);
				table.redrawRow(index);
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
			}
			
		});
	}

}
