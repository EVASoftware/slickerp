package com.slicktechnologies.client.views.salesquotation;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.SelectionCell;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.view.client.ProvidesKey;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.salesorder.ItemProductDetailsPopUp;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.TaxDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.productlayer.Tax;

public class SalesLineItemSalesQuotationTable extends SuperTable<SalesLineItem> implements ClickHandler {
	
	Column<SalesLineItem,String> deleteColumn;
	Column<SalesLineItem,String> vatColumn,serviceColumn,totalColumn,viewProdNameColumn;
	TextColumn<SalesLineItem> prodCategoryColumn,prodCodeColumn;
	Column<SalesLineItem, String> priceColumn;
	Column<SalesLineItem,String> prodQtyColumn;
	Column<SalesLineItem, String>prodPercentDiscColumn;
	Column<SalesLineItem,String> discountAmt;
	TextColumn<SalesLineItem> viewdiscountAmt;
	TextColumn<SalesLineItem> viewprodQtyColumn,viewPriceColumn,viewprodPercDiscColumn;
	TextColumn<SalesLineItem> uomColumn,prodIdColumn;
	
	Column<SalesLineItem,String> prodNameColumn;
	NumberFormat nf=NumberFormat.getFormat("0.00");
	
	/**
	 * Date 10 july 2017 added by vijay for GST Tax
	 */
	
	TextColumn<SalesLineItem> viewServiceTaxColumn;
	TextColumn<SalesLineItem> viewVatTaxColumn;
	
	public  ArrayList<TaxDetails> vattaxlist;
	public  ArrayList<TaxDetails> servicetaxlist;
	public  ArrayList<String> list;
	public  ArrayList<String> vatlist;
	
	TextColumn<SalesLineItem> viewHsnCodeColumn;
	Column<SalesLineItem,String> hsnCodeColumn;
	
	/**
	 * ends here
	 */
	
	/****** Date 05-08-2017 added by vijay for Area ****/
	Column<SalesLineItem, String> prodAreaColumn;
	TextColumn<SalesLineItem> viewProdAreaColumn;	
	
	/**
	 * nidhi
	 * 9-06-2018
	 * for productDetail
	 */
	Column<SalesLineItem, String> getDetailsButtonColumn;
	ItemProductDetailsPopUp itemDtPopUp = new ItemProductDetailsPopUp();
	int rowIndex ;
	/**
	 * end
	 */
	/**
	 * nidhi
	 * 15-06-2018
-	 */
	Column<SalesLineItem,String> getColPremiseDetails;
	TextColumn<SalesLineItem> viewColPremiseDetails;
	public SalesLineItemSalesQuotationTable()
	{
		super();
		itemDtPopUp.getLblCancel().addClickHandler(this);
		itemDtPopUp.getLblOk().addClickHandler(this);
		table.setHeight("300px");
	}
	public SalesLineItemSalesQuotationTable(UiScreen<SalesLineItem> view)
	{
		super(view);	
		itemDtPopUp.getLblCancel().addClickHandler(this);
		itemDtPopUp.getLblOk().addClickHandler(this);
		table.setHeight("300px");
	}

	public void createTable()
	{
		
//		createColumnprodCategoryColumn();
		createColumnprodNameColumn();
		createColumnUnitOfMeasurement();
		createColumnprodQtyColumn();
		createColumnpriceColumn();
		
		/**
		 * Date : 17-04-2018 BY ANIL
		 * Requirement by: HVAC (Rohan/Nitin Sir)
		 * Description : As we have decided to put area col value in qty itself ,
		 * so no need to have two different fields
		 */
//		createColumnprodAreaColumn();
		
		/**
		 * Date 10 july 2017 added by vijay
		 */
		retriveVatandServiceTax();
		
//		createColumnvatColumn();
//		createColumnserviceColumn();
		
		
		
		table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);	
		 /**
  		 *  nidhi
  		 *  02-08-2017
  		 *  For set table size auto scroll and fit data in table.
  		 */
  		table.setWidth("auto");
  		/*
  		 *  end
  		 */
		}
	
	
	
	private void retriveVatandServiceTax() {

		final GenricServiceAsync service = GWT.create(GenricService.class);
		MyQuerry query = new MyQuerry();
		Company c=new Company();
		System.out.println("Company Id :: "+c.getCompanyId());
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		query.setFilters(filtervec);
		query.setQuerryObject(new TaxDetails());
		
		service.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
			}

		
			public void onSuccess(ArrayList<SuperModel> result){
				
				System.out.println("result size of tax "+result);
				list=new ArrayList<String>();
				servicetaxlist=new ArrayList<TaxDetails>();
				vattaxlist = new ArrayList<TaxDetails>();
				vatlist=new ArrayList<String>();
				List<TaxDetails> backlist=new ArrayList<TaxDetails>();
				
				for (SuperModel model : result) {
					TaxDetails entity = (TaxDetails) model;
					
					backlist.add(entity);
					servicetaxlist.add(entity);
					vattaxlist.add(entity);
				}
				
				for(int i=0;i<backlist.size();i++){
						list.add(backlist.get(i).getTaxChargeName());
						System.out.println("list size"+list.size());
						vatlist.add(backlist.get(i).getTaxChargeName());
						System.out.println("valist size"+vatlist.size());
				}
				
				
				
				createColumnPercDiscount();
				// rohan added this column 
				createColumnDiscountAmt();
				createColumntotalColumn();
				addEditColumnVatTax();
				addEditColumnServiceTax();
				
				
				/**
				 * nidhi
				 * 15-06-2018 
				 */
				getColPremiseDetails();
				/**
				 * end
				 */
				createColumnprodHSNCodeColumn();
				
				/**
				 * nidhi
				 * 9-06-2018
				 * 
				 */
				getDetailsButtonColumn();
				createColumnprodCodeColumn();
				createColumnProdCountColumn();
				createColumndeleteColumn();
				addFieldUpdater();
			}
		});
	}
	
	
	/****** Date 24-08-2017 added by vijay for Area ****/
	private void createColumnprodAreaColumn() {
		EditTextCell editCell=new EditTextCell();
		prodAreaColumn = new Column<SalesLineItem, String>(editCell) {
			
			@Override
			public String getValue(SalesLineItem object) {
				if(object.getArea()!=null){
					return object.getArea();
				}else{
					return "";
				}
				
			}
		};
		
		table.addColumn(prodAreaColumn,"# Area");
		table.setColumnWidth(prodAreaColumn, 100,Unit.PX);
	}
	
	private void createColumnprodHSNCodeColumn() {
		EditTextCell editCell=new EditTextCell();
		hsnCodeColumn=new Column<SalesLineItem,String>(editCell)
		{
			@Override
			public String getValue(SalesLineItem object)
			{
				
				if(object.getPrduct().getHsnNumber()!=null && !object.getPrduct().getHsnNumber().equals("")){
					return object.getPrduct().getHsnNumber();
				}
				else{
					return "";
				}
			}
		};
		table.addColumn(hsnCodeColumn,"#HSN Code");
		table.setColumnWidth(hsnCodeColumn, 100,Unit.PX);
	}
	
	private void addEditColumnVatTax() {
		SelectionCell employeeselection= new SelectionCell(vatlist);
		vatColumn = new Column<SalesLineItem, String>(employeeselection) {
			@Override
			public String getValue(SalesLineItem object) {
				if (object.getVatTax().getTaxConfigName()!= null
						&&!object.getVatTax().getTaxConfigName().equals("")) {
					
					object.setVatTaxEdit(object.getVatTax().getTaxConfigName());
					
					return object.getVatTax().getTaxConfigName();
				} else
					return "NA";
			}
		};
		table.addColumn(vatColumn, "#Tax 1");
		table.setColumnWidth(vatColumn,110,Unit.PX);
		
	}
	
	private void addEditColumnServiceTax() {
		SelectionCell employeeselection= new SelectionCell(list);
		serviceColumn = new Column<SalesLineItem, String>(employeeselection) {
			@Override
			public String getValue(SalesLineItem object) {
				if (object.getServiceTax().getTaxConfigName()!= null
						&&!object.getServiceTax().getTaxConfigName().equals("")) {
					
					object.setServiceTaxEdit(object.getServiceTax().getTaxConfigName());
					
					return object.getServiceTax().getTaxConfigName();
				} else
					return "NA";
			}
		};
		table.addColumn(serviceColumn, "#Tax 2");
		table.setColumnWidth(serviceColumn,110,Unit.PX);
	}
	
	private void createColumnDiscountAmt() {
		
		EditTextCell editCell=new EditTextCell();
		discountAmt=new Column<SalesLineItem,String>(editCell)
				{
			@Override
			public String getValue(SalesLineItem object)
			{
				if(object.getDiscountAmt()==0){
					return 0+"";}
				else{
					return object.getDiscountAmt()+"";}
			}
				};
				table.addColumn(discountAmt,"#Disc Amt");
				table.setColumnWidth(discountAmt, 100,Unit.PX);
		
	}
	
	protected void createColumnProdCountColumn()
	{
		prodIdColumn = new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				if (object.getPrduct().getCount() != 0) {
					return object.getPrduct().getCount() + "";
				} else {
					return "";
				}
			}
		};
		table.addColumn(prodIdColumn, "Prod ID");
		table.setColumnWidth(prodIdColumn, 90, Unit.PX);
	}

	protected void createColumnprodCategoryColumn()
	{
		prodCategoryColumn = new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				return object.getProductCategory();
			}
		};
		table.addColumn(prodCategoryColumn, "Category");
		table.setColumnWidth(prodCategoryColumn, 90, Unit.PX);

	}
	protected void createColumnprodCodeColumn()
	{
		prodCodeColumn = new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				return object.getProductCode();
			}
		};
		table.addColumn(prodCodeColumn, "PCode");
		table.setColumnWidth(prodCodeColumn, 90, Unit.PX);

	}
	protected void createColumnprodNameColumn()
	{
		EditTextCell editCell = new EditTextCell();
		prodNameColumn = new Column<SalesLineItem, String>(editCell) {
			@Override
			public String getValue(SalesLineItem object) {
				return object.getProductName();
			}
		};
		table.addColumn(prodNameColumn, "#Name");
		table.setColumnWidth(prodNameColumn, 120, Unit.PX);

	}
	protected void createColumnprodQtyColumn()
	{
		EditTextCell editCell = new EditTextCell();
		prodQtyColumn = new Column<SalesLineItem, String>(editCell) {
			@Override
			public String getValue(SalesLineItem object) {
				if (object.getQty() == 0) {
					return 0 + "";
				} else {
					return object.getQty() + "";
				}
			}
		};
		table.addColumn(prodQtyColumn, "#Qty");
		table.setColumnWidth(prodQtyColumn, 90, Unit.PX);
	}
	
	
	protected void createViewQtyColumn()
	{
		viewprodQtyColumn = new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				if (object.getQty() == 0) {
					return 0 + "";
				} else {
					return object.getQty() + "";
				}
			}
		};
		table.addColumn(viewprodQtyColumn, "#Qty");
		table.setColumnWidth(viewprodQtyColumn, 90, Unit.PX);
	}
	
	protected void createColumnUnitOfMeasurement()
	{
		uomColumn = new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				if (object.getUnitOfMeasurement() != null) {
					return object.getUnitOfMeasurement();
				} else {
					return "";
				}
			}
		};
		table.addColumn(uomColumn, "UOM");
		table.setColumnWidth(uomColumn, 80, Unit.PX);

	}

	protected void createColumnPercDiscount()
	{
		EditTextCell editCell = new EditTextCell();
		prodPercentDiscColumn = new Column<SalesLineItem, String>(editCell) {
			@Override
			public String getValue(SalesLineItem object) {
				if (object.getPercentageDiscount() == 0
						|| object.getPercentageDiscount() == null) {
					return 0 + "";
				} else {
					return object.getPercentageDiscount() + "";
				}
			}
		};
		table.addColumn(prodPercentDiscColumn, "#% Disc");
		table.setColumnWidth(prodPercentDiscColumn, 80, Unit.PX);
               
	}
	
	
	protected void createViewPercDiscount()
	{
		viewprodPercDiscColumn = new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				if (object.getPercentageDiscount() == 0) {
					return 0 + "";
				} else {
					return object.getPercentageDiscount() + "";
				}
			}
		};
		table.addColumn(viewprodPercDiscColumn, "#% Disc");
		table.setColumnWidth(viewprodPercDiscColumn, 80, Unit.PX);
	}
	
	protected void createColumnpriceColumn()
	{
		EditTextCell editCell = new EditTextCell();
		priceColumn = new Column<SalesLineItem, String>(editCell) {
			@Override
			public String getValue(SalesLineItem object) {
				SuperProduct product = object.getPrduct();
				double tax = removeAllTaxes(product);
				double origPrice = object.getPrice() - tax;

				return nf.format(origPrice);
			}
		};
		table.addColumn(priceColumn, "#Price");
		table.setColumnWidth(priceColumn, 90, Unit.PX);
	}
	
	protected void createViewColumnpriceColumn()
	{
		viewPriceColumn = new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				if (object.getPrice() == 0) {
					return 0 + "";
				} else {
					SuperProduct product = object.getPrduct();
					double tax = removeAllTaxes(product);
					double origPrice = object.getPrice() - tax;

					return nf.format(origPrice);
				}
			}
		};
		table.addColumn(viewPriceColumn, "#Price");
		table.setColumnWidth(viewPriceColumn, 90, Unit.PX);
	}
	
	
	protected void createColumnvatColumn()
	{
		TextCell editCell = new TextCell();
		vatColumn = new Column<SalesLineItem, String>(editCell) {
			@Override
			public String getValue(SalesLineItem object) {
				// if(object.getVatTax()!=null){
				// return object.getVatTax().getPercentage()+"";
				// }
				// else{
				// return "N.A.";
				// }
				// Date 13 july 2017 below line added by vijay and above old
				// code commented
				return object.getVatTaxEdit();
			}
		};
		table.addColumn(vatColumn, "# Tax 1");
		table.setColumnWidth(vatColumn, 110, Unit.PX);
	}
	protected void createColumnserviceColumn()
	{
		TextCell editCell = new TextCell();
		serviceColumn = new Column<SalesLineItem, String>(editCell) {
			@Override
			public String getValue(SalesLineItem object) {
				// SuperProduct prod=object.getPrduct();
				// if(object.getServiceTax()!=null){
				// return object.getServiceTax().getPercentage()+"";
				// }
				// else{
				// return "N.A.";
				// }
				// Date 13 july 2017 below line added by vijay and above old
				// code commented
				return object.getServiceTaxEdit();

			}
		};
		table.addColumn(serviceColumn, "# Tax 1");
		table.setColumnWidth(serviceColumn, 110, Unit.PX);
	}
	protected void createColumntotalColumn()
	{
		TextCell editCell=new TextCell();
		totalColumn=new Column<SalesLineItem,String>(editCell)
				{
			@Override
			public String getValue(SalesLineItem object)
			{
				double total=0;
				SuperProduct product=object.getPrduct();
				double tax=removeAllTaxes(product);
				double origPrice=object.getPrice()-tax;
//				if(object.getPercentageDiscount()==null){
//					total=origPrice*object.getQty();
//				}
//				if(object.getPercentageDiscount()!=null){
//					total=origPrice-(origPrice*object.getPercentageDiscount()/100);
//					total=total*object.getQty();
//					//total=Math.round(total);
//					
//				}
//				
//				return nf.format(total);
//			}
//				};
//				table.addColumn(totalColumn,"Total");
				
				
				/** Date 05-08-2017
				 *  added by vijay add code for area calculation
				 */
				
				if(object.getArea().equals("") || object.getArea().equals("0")){
					object.setArea("NA");
				}
				
				System.out.println("area == "+object.getArea());
				//****************new code for discount by rohan ******************
				if((object.getPercentageDiscount()==null && object.getPercentageDiscount()==0) && (object.getDiscountAmt()==0) ){
					
					System.out.println("inside both 0 condition");
					/******************** Square Area Calculation code added in if condition and without square area calculation code in else block ***************************/
					if(!object.getArea().equalsIgnoreCase("NA") ){
					double area = Double.parseDouble(object.getArea());
					 total = origPrice*area;
					}else{
						total=origPrice*object.getQty();
					}
				}
				
				else if((object.getPercentageDiscount()!=null)&& (object.getDiscountAmt()!=0)){
					
					/******************** Square Area Calculation code added in if condition and without square area calculation code in else block old code ***************************/
					if(!object.getArea().equalsIgnoreCase("NA")){
						double area = Double.parseDouble(object.getArea());
						total=origPrice*area;
						total = total - (total*object.getPercentageDiscount()/100);
						total = total - object.getDiscountAmt();

					}else{
						total=origPrice*object.getQty();
						total=total-(total*object.getPercentageDiscount()/100);
						total=total-object.getDiscountAmt();
					}	
					
				}
				else 
				{
					System.out.println("inside oneof the null condition");
					
						if(object.getPercentageDiscount()!=null){
							System.out.println("inside getPercentageDiscount oneof the null condition");
							/******************** Square Area Calculation code added in if condition and without square area calculation code in else block old code ***************************/
							System.out.println("tatal $$$$$$$$$$ ===="+total);
							if(!object.getArea().equalsIgnoreCase("NA")){
								double area = Double.parseDouble(object.getArea());
								total=origPrice*area;
								System.out.println("total before discount per ===="+total);
								total = total - (total*object.getPercentageDiscount()/100);
								System.out.println("after discount per total === "+total);
								
							}else{
								
								total=origPrice*object.getQty();
								total=total-(total*object.getPercentageDiscount()/100);
							}
						}
						else 
						{
							System.out.println("inside getDiscountAmt oneof the null condition");
							
							/******************** Square Area Calculation code added in if condition and without square area calculation code in else block old code ***************************/
							System.out.println("tatal $$$$$$$$$$ ===="+total);
							if(!object.getArea().equalsIgnoreCase("NA")){
								double area = Double.parseDouble(object.getArea());
								total=origPrice*area;
								System.out.println("total before discount amt ===="+total);
								total = total - object.getDiscountAmt();
								System.out.println("after discount amt total === "+total);
								
							}else{
								
								total=origPrice*object.getQty();
								total=total-object.getDiscountAmt();
							}
						}
						
//						total=total*object.getQty();
						
				}
				/**
				 * @author Anil
				 * @since 16-07-2020
				 */
				object.setTotalAmount(nf.parse(nf.format(total)));
				
				return nf.format(total);
			}
				};
				table.addColumn(totalColumn,"Total");
				table.setColumnWidth(totalColumn, 110, Unit.PX);
				
	}
	protected void createColumndeleteColumn()
	{
		ButtonCell btnCell= new ButtonCell();
		deleteColumn=new Column<SalesLineItem,String>(btnCell)
				{
			@Override
			public String getValue(SalesLineItem object)
			{
				return  "Delete" ;
			}
				};
				table.addColumn(deleteColumn,"Delete");
				table.setColumnWidth(deleteColumn, 80, Unit.PX);
	}



	@Override public void addFieldUpdater() 
	{
		createFieldUpdaterprodQtyColumn();
		createFieldUpdaterpriceColumn();
		createFieldUpdaterPercDiscColumn();
		createFieldUpdaterDiscAmtColumn();
		createFieldUpdaterdeleteColumn();
		createFieldUpdaterproductNameColumn();
		
		/**
		 * Date 11 jul 2017 added by vijay for GST 
		 */
		
		updateServiceTaxColumn();
		updateVatTaxColumn();
		
		updateHSNCode();
		
		/**
		 * ends here
		 */
		/**
		 * nidhi
		 * 9-06-2018
		 */
		createFieldUpdaterOnBranchesColumn();
		/**
		 * ends here
		 */
		
		/** Date : 17-04-2018 BY ANIL commented**/
//		/** Date 24-08-2017 added by vijay for Area wise calculation **/
//		createFieldUpdaterProdAreaColumn();
	}
	

//
///** Date 24-08-2017 added by vijay for Area wise calculation **/
//	
//	private void createFieldUpdaterProdAreaColumn() {
//		prodAreaColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {
//			@Override
//			public void update(int index, SalesLineItem object, String value) {
//				
//				try{
//					object.setArea(value);
//					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
//				}
//				catch (NumberFormatException e)
//				{
//
//				}
//				table.redrawRow(index);
//			}
//		});
//	}

	
	private void updateHSNCode() {
		hsnCodeColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {

			@Override
			public void update(int index, SalesLineItem object, String value) {
				object.getPrduct().setHsnNumber(value);
				table.redrawRow(index);
			}
		});
	}
	
	public void updateServiceTaxColumn()
	{

		serviceColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {
			
			@Override
			public void update(int index, SalesLineItem object, String value) {
				try {
					String val1 = (value.trim());
					object.setServiceTaxEdit(val1);
					Tax tax = object.getServiceTax();
					for(int i=0;i<servicetaxlist.size();i++)
					{
						if(val1.trim().equals(servicetaxlist.get(i).getTaxChargeName())){
							tax.setTaxName(val1);
							tax.setPercentage(servicetaxlist.get(i).getTaxChargePercent());
							/**
							 * vijay added this for GST implementation Date :23-06-2017
							 */
							tax.setTaxPrintName(servicetaxlist.get(i).getTaxPrintName());
							/**
							 * Date : 10-08-2017 By Anil
							 */
							tax.setTaxConfigName(val1);
							/**
							 * End
							 */
							/**
							 * nidhi
							 * 8-06-2018
							 */
							Tax vatTax =  AppUtility.checkTaxValue(tax, object.getVatTax(), vattaxlist);
							if(vatTax!=null){
								object.setVatTax(vatTax);
							}
							/**
							 * end
							 */
							break;
						}
					}
					object.setServiceTax(tax);
					
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
					
				} catch (NumberFormatException e) {
				}
				table.redrawRow(index);
			}
		});
	}
	
	public void updateVatTaxColumn()
	{

		vatColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {
			
			@Override
			public void update(int index, SalesLineItem object, String value) {
				try {
					String val1 = (value.trim());
					object.setVatTaxEdit(val1);
					Tax tax = object.getVatTax();
					for(int i=0;i<vattaxlist.size();i++)
					{
						if(val1.trim().equals(vattaxlist.get(i).getTaxChargeName())){
							tax.setTaxName(val1);
							tax.setPercentage(vattaxlist.get(i).getTaxChargePercent());
							tax.setTaxPrintName(vattaxlist.get(i).getTaxPrintName());
							/**
							 * vijay added this for GST implementation Date :23-06-2017
							 */
							tax.setTaxPrintName(vattaxlist.get(i).getTaxPrintName());
							/**
							 * Date : 10-08-2017 By Anil
							 */
							tax.setTaxConfigName(val1);
							/**
							 * End
							 */
							/**
							 * nidhi
							 * 8-06-2018
							 * for tax selection option
							 */
//							checkTaxValue(tax,object);
							Tax taxDt =  AppUtility.checkTaxValue(tax, object.getServiceTax(), vattaxlist);
							if(taxDt != null){
								object.setServiceTax(taxDt);
							}
							/**
							 * end
							 */
							break;
						}
					}
					object.setVatTax(tax);
					
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
					
				} catch (NumberFormatException e) {
				}
				table.redrawRow(index);
			}
		});
	}
	
	protected void createFieldUpdaterproductNameColumn()
	{
		prodNameColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, String>()
				{
			@Override

			public void update(int index,SalesLineItem object,String value)
			{

				try{
					String val1=value.trim();
					object.setProductName(val1);
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);

				}
				catch (NumberFormatException e)
				{

				}
				table.redrawRow(index);
			}
				});
	}

	
	
	private void createFieldUpdaterDiscAmtColumn() {
		discountAmt.setFieldUpdater(new FieldUpdater<SalesLineItem, String>()
				{
			@Override

			public void update(int index,SalesLineItem object,String value)
			{

				try{
					Double val1=Double.parseDouble(value.trim());
					object.setDiscountAmt(val1);
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);

				}
				catch (NumberFormatException e)
				{

				}
				table.redrawRow(index);
			}
				});
	}
	

	protected void createFieldUpdaterprodQtyColumn()
	{
		prodQtyColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, String>()
				{
			@Override

			public void update(int index,SalesLineItem object,String value)
			{

				try{
					Double val1=Double.parseDouble(value.trim());
					object.setQuantity(val1);
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);

				}
				catch (NumberFormatException e)
				{

				}


				table.redrawRow(index);
			}
				});
	}
	
	protected void createFieldUpdaterPercDiscColumn()
	{
		prodPercentDiscColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, String>()
				{
			@Override

			public void update(int index,SalesLineItem object,String value)
			{

				try{
					Double val1=Double.parseDouble(value.trim());
					object.setPercentageDiscount(val1);
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);

				}
				catch (NumberFormatException e)
				{

				}
				table.redrawRow(index);
			}
				});
	}

	protected void createFieldUpdaterpriceColumn()
	{
		priceColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, String>()
				{
			@Override

			public void update(int index,SalesLineItem object,String value)
			{
				try
				{
					Double val1=Double.parseDouble(value.trim());
			
				object.setPrice(val1);
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				
				}
				catch(Exception e)
				{
				}
				table.redrawRow(index);
			}
				});
	}
	protected void createFieldUpdaterdeleteColumn()
	{
		deleteColumn.setFieldUpdater(new FieldUpdater<SalesLineItem,String>()
				{
			@Override
			public void update(int index,SalesLineItem object,String value)
			{
				getDataprovider().getList().remove(object);
				table.redrawRow(index);
			}
				});
	}


	public void addColumnSorting(){

		//createSortinglbtColumn();

	}

	@Override
	public void setEnable(boolean state)
	{
          
		
        for(int i=table.getColumnCount()-1;i>-1;i--)
    	  table.removeColumn(i); 
        	  
      
          if(state==true)
          {
        	  createColumnProdCountColumn();
//        	 createColumnprodCategoryColumn();
      		createColumnprodCodeColumn();
      		createColumnprodNameColumn();
      		createColumnprodQtyColumn();
      		createColumnUnitOfMeasurement();
      		createColumnpriceColumn();
      		
      		/** Date : 17-04-2018 BY ANIL **/
//      		createColumnprodAreaColumn();

//      		createColumnvatColumn();
//      		createColumnserviceColumn();
      		
      		/**above two methods commented by vijay
      		 * Date 10 july 2017 added by vijay for GST 
      		 */
      		
      		retriveVatandServiceTax();
      		
//      		createColumnPercDiscount();
//      		createColumnDiscountAmt();
//      		createColumntotalColumn();
//      		createColumndeleteColumn();
//      		addFieldUpdater();
      		}
 
          else
          {
        	  createColumnProdCountColumn();
//        	 createColumnprodCategoryColumn();
      		createColumnprodCodeColumn();
//      		createColumnprodNameColumn();
      		createColumnViewProdNameColumn();
      		createViewQtyColumn();
      		
      		/** Date : 17-04-2018 BY ANIL **/
//      		createViewColumnProdAreaView();

      		createColumnUnitOfMeasurement();
      		createViewColumnpriceColumn();
      		createColumnvatColumn();
      		createColumnserviceColumn();
      		createViewPercDiscount();
      		createViewDiscountAmt();
      		createColumntotalColumn();
      		createColumnViewHSNCodeColumn();
      		/**
      		 * nidhi
      		 * 15-06-2018
      		 */
      		viewColPremiseDetails();
          }
          
          /**
    		 *  nidhi
    		 *  02-08-2017
    		 *  For set table size auto scroll and fit data in table.
    		 */
    		table.setWidth("auto");
    		/*
    		 *  end
    		 */
	}


	
	private void createViewColumnProdAreaView() {
		
		viewProdAreaColumn = new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				return object.getArea();
			}
		};
		table.addColumn(viewProdAreaColumn,"#Area");
		table.setColumnWidth(viewProdAreaColumn, 100,Unit.PX);
	}

	protected void createColumnViewProdNameColumn(){
		
		viewProdNameColumn = new TextColumn<SalesLineItem>()
			{
					@Override 
					public String getValue(SalesLineItem object){
						return object.getProductName();
					}
			};
			table.addColumn(viewProdNameColumn,"Name");
			table.setColumnWidth(viewProdNameColumn, 120,Unit.PX);
	}
	
	
	private void createColumnViewHSNCodeColumn() {
		
		viewHsnCodeColumn=new TextColumn<SalesLineItem>()
			{
				@Override
				public String getValue(SalesLineItem object)
				{
					return object.getPrduct().getHsnNumber();
				}
			};
			table.addColumn(viewHsnCodeColumn,"#HSN Code");
			table.setColumnWidth(viewHsnCodeColumn, 100,Unit.PX);
	}
	

	protected void createViewDiscountAmt()
	{
		viewdiscountAmt=new TextColumn<SalesLineItem>()
				{
			@Override
			public String getValue(SalesLineItem object)
			{
				if(object.getDiscountAmt()==0){
					return 0+"";}
				else{
					return object.getDiscountAmt()+"";}
			}
				};
				table.addColumn(viewdiscountAmt,"#Disc Amt");
				table.setColumnWidth(viewdiscountAmt, 100,Unit.PX);
	}
	
	@Override
	public void applyStyle()
	{

	}
	@Override
	protected void initializekeyprovider() {
		keyProvider= new ProvidesKey<SalesLineItem>()
				{
			@Override
			public Object getKey(SalesLineItem item)
			{
				if(item==null)
				{
					return null;
				}
				else
					return item.getId();
			}
				};
	}
	
	
	public double calculateTotalExcludingTax()
	{
		List<SalesLineItem>list=getDataprovider().getList();
//		double sum=0;
//		priceVal=0;
		double total=0,origPrice=0;
		for(int i=0;i<list.size();i++)
		{
			SalesLineItem entity=list.get(i);
			SuperProduct prod=entity.getPrduct();
			double taxAmt=removeAllTaxes(prod);
//			if(entity.getPercentageDiscount()==null){
//			//	priceVal=Math.round(entity.getPrice()-taxAmt);
//				priceVal=(entity.getPrice()-taxAmt);
//				priceVal=priceVal*entity.getQty();
//				sum=sum+priceVal;
//			}
//			if(entity.getPercentageDiscount()!=null){
//				priceVal=entity.getPrice()-taxAmt;
//				priceVal=priceVal-(priceVal*entity.getPercentageDiscount()/100);
//			//	priceVal=Math.round(priceVal*entity.getQty());
//				priceVal=(priceVal*entity.getQty());
//				sum=sum+priceVal;
//			}
//			
//	
//		}
//		return sum;
			
			//*****************new code for discount by rohan ***************** 
			 origPrice = entity.getPrice()-taxAmt;
			if((entity.getPercentageDiscount()==null && entity.getPercentageDiscount()==0) && (entity.getDiscountAmt()==0) ){
				
				System.out.println("inside both 0 condition");
				/******************** Square Area Calculation code added in if condition and without square area calculation code in else block ***************************/
				if(!entity.getArea().equalsIgnoreCase("NA")){
				double squareArea = Double.parseDouble(entity.getArea());
				origPrice=origPrice*squareArea;
				total+=origPrice;
				System.out.println("RRRRRRRRRRRRRR total"+total);
				}
				else{
					System.out.println("Old code");
//					total=origPrice*entity.getQty();
					/**
					 * Date 13 july 2017 below code added by vijay for 2 or more products proper calculation
					 * and  above old code commented
					 */
					origPrice = origPrice*entity.getQty();
					total+=origPrice;
				}
			}
			
			else if((entity.getPercentageDiscount()!=null)&& (entity.getDiscountAmt()!=0)){
				System.out.println("inside both not null condition");

				/******************** Square Area Calculation code added in if condition and without square area calculation code in else block ***************************/
				if(!entity.getArea().equalsIgnoreCase("NA")){
					double squareArea = Double.parseDouble(entity.getArea());
					origPrice=origPrice*squareArea;
					origPrice=origPrice-(origPrice*entity.getPercentageDiscount()/100);
					origPrice=origPrice-entity.getDiscountAmt();
					total+=origPrice;
					System.out.println("RRRRRRRRRRRRRR total"+total);
					
				}else{
					
//					total=origPrice*entity.getQty();
//					total=total-(total*entity.getPercentageDiscount()/100);
//					total=total-entity.getDiscountAmt();
					
					/**
					 * Date 13 july 2017 below code added by vijay for 2 or more products proper calculation
					 * and  above old code commented
					 */
					
					origPrice=origPrice*entity.getQty();
					origPrice=origPrice-(origPrice*entity.getPercentageDiscount()/100);
					origPrice=origPrice-entity.getDiscountAmt();
					total +=origPrice;
				}
			}
			else 
			{
				System.out.println("inside oneof the null condition");
				
					if(entity.getPercentageDiscount()!=null){
						System.out.println("inside getPercentageDiscount oneof the null condition");
						
						if(!entity.getArea().equalsIgnoreCase("NA")){
							double squareArea = Double.parseDouble(entity.getArea());
							origPrice=origPrice*squareArea;
							origPrice=origPrice-(origPrice*entity.getPercentageDiscount()/100);
							total +=origPrice;
						}else{
							
//							total=origPrice*entity.getQty();
//							total=total-(total*entity.getPercentageDiscount()/100);
							/**
							 * Date 13 july 2017 below code added by vijay for 2 or more products proper calculation
							 * and  above old code commented
							 */
							origPrice=origPrice*entity.getQty();
							origPrice=origPrice-(origPrice*entity.getPercentageDiscount()/100);
							total +=origPrice;
		
						}	
					}
					else 
					{
						System.out.println("inside getDiscountAmt oneof the null condition");
						if(!entity.getArea().equalsIgnoreCase("NA")){
							double squareArea = Double.parseDouble(entity.getArea());
							origPrice=origPrice*squareArea;
							origPrice=origPrice*entity.getQty();
							origPrice=origPrice-entity.getDiscountAmt();
							total +=origPrice;

						}else{
							
//							total=origPrice*entity.getQty();
//							total=total-entity.getDiscountAmt();
							/**
							 * Date 13 july 2017 below code added by vijay for 2 or more products proper calculation
							 * and  above old code commented
							 */
							origPrice=origPrice*entity.getQty();
							origPrice=origPrice-entity.getDiscountAmt();
							total +=origPrice;

						}

					}
					
					System.out.println("Total = 7 "+total);
//					total=total*object.getQty();
					
			}
			
	
		}
		return total;
			
	}
	


	public double removeAllTaxes(SuperProduct entity)
	{
		double vat = 0,service = 0;
		double tax=0,retrVat=0,retrServ=0;
		if(entity instanceof ServiceProduct)
		{
			ServiceProduct prod=(ServiceProduct) entity;
			if(prod.getServiceTax()!=null&&prod.getServiceTax().isInclusive()==true){
				service=prod.getServiceTax().getPercentage();
			}
			if(prod.getVatTax()!=null&&prod.getVatTax().isInclusive()==true){
				vat=prod.getVatTax().getPercentage();
			}
		}

		if(entity instanceof ItemProduct)
		{
			ItemProduct prod=(ItemProduct) entity;
			if(prod.getVatTax()!=null&&prod.getVatTax().isInclusive()==true){
				vat=prod.getVatTax().getPercentage();
			}
			if(prod.getServiceTax()!=null&&prod.getServiceTax().isInclusive()==true){
				service=prod.getServiceTax().getPercentage();
			}
		}
		
		if(vat!=0&&service==0){
		//	retrVat=Math.round(entity.getPrice()/(1+(vat/100)));
			retrVat=(entity.getPrice()/(1+(vat/100)));
			retrVat=entity.getPrice()-retrVat;
		}
		if(service!=0&&vat==0){
		//	retrServ=Math.round(entity.getPrice()/(1+service/100));
			retrServ=(entity.getPrice()/(1+service/100));
			retrServ=entity.getPrice()-retrServ;
		}
		
		
		if(service!=0&&vat!=0){
			
			
			
//			// calculate vat.
//			double removeServiceTax=(entity.getPrice()/(1+service/100));
//			
//			//double taxPerc=service+vat;
//			//retrServ=(entity.getPrice()/(1+taxPerc/100)); //line changed below
//			retrServ=(removeServiceTax/(1+vat/100));
//			retrServ=entity.getPrice()-retrServ;
			
			/** above old code commented and added in else block
			 * Date 11 july 2017 added by vijay for GST
			 */
			
			if(entity instanceof ItemProduct)
			{
				ItemProduct prod=(ItemProduct) entity;
				if(prod.getServiceTax().getTaxPrintName()!=null && ! prod.getServiceTax().getTaxPrintName().equals("")
				   && prod.getVatTax().getTaxPrintName()!=null && ! prod.getVatTax().getTaxPrintName().equals(""))
				{

					double dot = service + vat;
					retrServ=(entity.getPrice()/(1+dot/100));
					retrServ=entity.getPrice()-retrServ;
//					retrVat=0;

				}
				else{
					// Here if both are inclusive then first remove service tax and then on that amount
					// calculate vat.
					double removeServiceTax=(entity.getPrice()/(1+service/100));
					//double taxPerc=service+vat;
					//retrServ=(entity.getPrice()/(1+taxPerc/100)); //line changed below
					retrServ=(removeServiceTax/(1+vat/100));
					retrServ=entity.getPrice()-retrServ;
				}
			}
			
			/**
			 * ends here
			 */
			
			
		}
		tax=retrVat+retrServ;
		return tax;
	}
	
	
		public boolean removeChargesOnDelete(){
		
		int tableSize=getDataprovider().getList().size();
		
		if(tableSize<1){
			return false;
		}
		return true;
	}
		@Override
		public void onClick(ClickEvent event) {
			if(event.getSource().equals(itemDtPopUp.getLblOk())){//(itemDtPopUp.getTaDetails().getValue()!= null) ? itemDtPopUp.getTaDetails().getValue() : "");
				getDataprovider().getList().get(rowIndex).getPrduct().setComment((itemDtPopUp.getTaDetails().getValue()!= null) ? itemDtPopUp.getTaDetails().getValue() : "");
				getDataprovider().getList().get(rowIndex).getPrduct().setCommentdesc((itemDtPopUp.getTaDetails1().getValue()!= null) ? itemDtPopUp.getTaDetails1().getValue() : "");
				itemDtPopUp.hidePopUp();
			}else if(event.getSource().equals(itemDtPopUp.getLblCancel())){
				itemDtPopUp.hidePopUp();
			}
			
			
		}
		/**
		 * nidhi
		 * 9-06-2018
		 * for details
		 */
		
		protected void getDetailsButtonColumn() {
			ButtonCell btnCell = new ButtonCell();
			getDetailsButtonColumn = new Column<SalesLineItem, String>(btnCell) {
				@Override
				public String getValue(SalesLineItem object) {
					return "Product Details";
				}
			};
			table.addColumn(getDetailsButtonColumn, "Product Details");
			table.setColumnWidth(getDetailsButtonColumn, 100, Unit.PX);
		}

		protected void createFieldUpdaterOnBranchesColumn() {
			getDetailsButtonColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {
				@Override
				public void update(int index, SalesLineItem object,String value) {
					rowIndex = index;
					itemDtPopUp.showPopUp();//(object.getPrduct().getCommentdesc()!= null) ? object.getPrduct().getCommentdesc() : "");
					itemDtPopUp.getTaDetails().setValue((object.getPrduct().getComment()!= null) ? object.getPrduct().getComment() : "");
					itemDtPopUp.getTaDetails1().setValue((object.getPrduct().getCommentdesc()!= null) ? object.getPrduct().getCommentdesc() : "");
					
				}
			});
		}
		
	

		private void getColPremiseDetails() {

			EditTextCell cell=new EditTextCell();
			getColPremiseDetails=new Column<SalesLineItem, String>(cell) {
				@Override
				public String getValue(SalesLineItem object) {
					if(object.getPremisesDetails()!=null){
						return object.getPremisesDetails()+"";
					}
					return "";
				}
			};
			table.addColumn(getColPremiseDetails, "#Premises");
			table.setColumnWidth(getColPremiseDetails,120,Unit.PX);
			
			getColPremiseDetails.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {
				@Override
				public void update(int index, SalesLineItem object, String value) {
					object.setPremisesDetails(value);
					table.redraw();
				}
			});
		
		}
		private void viewColPremiseDetails() {
			viewColPremiseDetails=new TextColumn<SalesLineItem>() {
				@Override
				public String getValue(SalesLineItem object) {
					if(object.getPremisesDetails()!=null){
						return object.getPremisesDetails()+"";
					}
					return "";
				}
			};
			table.addColumn(viewColPremiseDetails, "#Premises");
			table.setColumnWidth(viewColPremiseDetails,120,Unit.PX);
		}
}
