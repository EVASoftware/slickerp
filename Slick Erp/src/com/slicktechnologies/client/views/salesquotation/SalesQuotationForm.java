package com.slicktechnologies.client.views.salesquotation;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.TextAlign;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.ChangeListener;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.AddressComposite;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.composite.UploadComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.approvalutility.ApprovableFormScreen;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.config.CategoryTypeFactory;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.contract.OtherChargesTable;
import com.slicktechnologies.client.views.contract.PaymentTermsTable;
import com.slicktechnologies.client.views.contract.ProductChargesTable;
import com.slicktechnologies.client.views.contract.ProductTaxesTable;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.shared.ProductOtherCharges;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.TaxDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.TermsAndConditions;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.productlayer.Tax;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.salesorder.OtherCharges;
import com.slicktechnologies.shared.common.salesorder.OtherChargesMargin;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesorder.SalesQuotation;
import com.slicktechnologies.shared.common.salesorder.VendorMargin;

public class SalesQuotationForm extends ApprovableFormScreen<SalesQuotation> implements ClickHandler, ChangeHandler  {
	
	//Token to add the varialble declarations
		/** The db valid untill. */
		DateBox dbValidUntill;
		
		/** The olbb branch. */
		ObjectListBox<Branch> olbbBranch;
		
		/** The olbc payment terms. */
		ObjectListBox<Config> olbcPriority,olbcPaymentMethods;
		
		/** The tb quotation status. */
		TextBox tbRefeRRedBy,tbQuotationStatus;
		TextBox /*tbLeadId,*/tbContractId;
		TextBox tbLeadId;
		
		/** The tb quotatin id. */
		TextBox tbQuotatinId;
		
		/** The ta description. */
		TextArea taDescription;
		
		/** The uc upload t and cs. */
		UploadComposite ucUploadTAndCs;
		
		/** The person info composite. */
		public PersonInfoComposite personInfoComposite;
		
		/** The olb approver name. */
		ObjectListBox<Employee> olbeSalesPerson,olbApproverName;
		
		/** The saleslineitemquotationtable. */
		SalesLineItemSalesQuotationTable lineitemsalesquotationtable;
		
		/** The olb quotation group. */
		ObjectListBox<Config>olbQuotationGroup;
		
		/** The olb quotation type. */
		ObjectListBox<Type> olbQuotationType;
		
		/** The olb quotation category. */
		ObjectListBox<ConfigCategory> olbQuotationCategory;
		IntegerBox ibCreditPeriod;
		DateBox dbquotationDate;
		ListBox cbcformlis;
		
		ProductInfoComposite prodInfoComposite;
		DoubleBox dototalamt,doamtincltax,donetpayamt;
		IntegerBox ibdays;
		DoubleBox dopercent;
		TextBox tbcomment;
		Button addPaymentTerms;
		PaymentTermsTable paymentTermsTable;
		ProductChargesTable chargesTable;
		Button addOtherCharges;
		ProductTaxesTable prodTaxTable;
		CheckBox cbCheckAddress;
		DateBox dbdeliverydate;
		AddressComposite acshippingcomposite;
		Button baddproducts;
		ObjectListBox<TaxDetails> olbcstpercent;
		TextBox tbremark;
		CheckBox cbtaxSwitch;
		
		FormField fgroupingCustomerInformation;
		String updateQuotations;
		
		final GenricServiceAsync genasync=GWT.create(GenricService.class);
		/**********************************************************************************/
		
		Button btnsecurityDep;
		SalesQuotation quotationObject;
		int f_docid;
		Date f_docDate;
		String f_docStatus;
		Double f_securityDeposit;
		Date f_reDate;
		boolean f_depositStatus;
		String f_payMethod;
		String f_accountNo;
		String f_bankName;
		String f_branch;
		String f_paybleAt;
		String f_fouring;
		String f_checkName;
		int f_checkNo=0;
		Date f_checkDate;
		String f_serviceNo="";
		String f_ifscCode;
		String f_micrCode;
		String f_swiftCode;
		String f_instruction;
		Address f_address;
		
		/**
		 * Date 11 july 2017 added by vijay for Cform validation
		 */
		Date date = DateTimeFormat.getFormat("yyyy-MM-dd HH:mm:ss").parse("2017-07-01 00:00:00");
		final DateTimeFormat fmt = DateTimeFormat.getFormat("dd/MM/yyyy");
		/**
		 * ends here
		 */
		/** date 09/03/2018 added by komal for followup date **/
		DateBox dbFollowUpDate;		
		
		/**
		 * Date : 23-03-2018 BY VIJAY
		 * Adding other charges table
		 */
		OtherChargesTable tblOtherCharges;
		Button addOthrChargesBtn;
		DoubleBox dbOtherChargesTotal;
		/**
		 * End
		 */
		
		/**
		 * Date 03-05-2018 By vijay
		 * for same product sr no because allow to add duplicate product so
		 * differentiate same product with product SRNO
		 */
		public static int productSrNo=0;
		/**
		 * ends here
		 */
		
		/**
		 * Date 08/06/2018
		 * Developer :- Vijay
		 * Des :- for final total amt Round off amt 
		 */
		DoubleBox dbfinalTotalAmt;
		TextBox tbroundoffAmt;
		/**
		 * ends here
		 */
		/**
		 * nidhi
		 * 1-08-2018
		 * for status reason
		 */
		TextBox tbDocStatusreason ;
		/**date 20-6-2020 by Amol added description two and vendor price raised by **/
		TextArea taDescriptionTwo;
		DoubleBox dbVendorPrice;
		
		/**
		 * @author Anil
		 * @since 16-07-2020
		 * For PTSPL raised by Rahul Tiwari
		 * calculating vendor and other charges margin
		 */
		public Button costingBtn;
		public boolean marginFlag;
		List<VendorMargin> vendorMargins;
		List<OtherChargesMargin> otherChargesMargins;
		Double totalVendorMargin;
		Double totalOcMargin;
		public VendorAndOtherChargesMarginPopup marginPopup=new VendorAndOtherChargesMarginPopup();
		
		public ObjectListBox<CustomerBranchDetails> olbcustBranch;
		String customerBranch = "";
		Address customerBillingAddress=null;
		
		public Customer cust;

		TermsAndConditionTable termsAndConditionTable;
		ObjectListBox<TermsAndConditions> olbtermsAndConditionTitle;
		Button btnTermsAndConditionAdd;
		CheckBox chkPrintAllTermsAndCondition;
		
		
		ObjectListBox<CompanyPayment> objPaymentMode;
	    ArrayList<CompanyPayment> paymentModeList = new ArrayList<CompanyPayment>();

		/**
		 * Instantiates a new quotation form.
		 */
		public  SalesQuotationForm() {
			super();
			createGui();
			paymentTermsTable.connectToLocal();
			lineitemsalesquotationtable.connectToLocal();
			prodTaxTable.connectToLocal();
			chargesTable.connectToLocal();
			
			/**
			 * Date :14-09-2017 By Vijay for Other charges table
			 */
			tblOtherCharges.connectToLocal();
			dbOtherChargesTotal.setEnabled(false);
			/**
			 * ENd
			 */
			
			marginFlag=AppUtility.checkForProcessConfigurartionIsActiveOrNot("SalesQuotation","CalculateVendorAndOtherChargesMargin");
			marginPopup.getLblOk().addClickHandler(this);
			marginPopup.getLblCancel().addClickHandler(this);
			
			
		}

		/**
		 * Instantiates a new quotation form.
		 *
		 * @param processlevel the processlevel
		 * @param fields the fields
		 * @param formstyle the formstyle
		 */
		public SalesQuotationForm  (String[] processlevel, FormField[][] fields,
				FormStyle formstyle) 
		{
			super(processlevel, fields, formstyle);
			createGui();
		}
		
		/**
		 * Method template to initialize the declared variables.
		 */
		private void initalizeWidget()
		{
			taDescriptionTwo=new TextArea();
			dbVendorPrice=new DoubleBox();
			
			dbValidUntill=new DateBoxWithYearSelector();
			DateTimeFormat format1=DateTimeFormat.getFormat("dd-MM-yyyy");
			DateBox.DefaultFormat format2=new DateBox.DefaultFormat(format1);
			dbValidUntill.setFormat(format2);
			olbbBranch=new ObjectListBox<Branch>();
			AppUtility.makeBranchListBoxLive(olbbBranch);
			olbbBranch.addChangeHandler(this);
			
			olbcPriority=new ObjectListBox<Config>();
			AppUtility.MakeLiveConfig(olbcPriority,Screen.SALESQUOTATIONPRIORITY);

			olbcPaymentMethods=new ObjectListBox<Config>();
			AppUtility.MakeLiveConfig(olbcPaymentMethods,Screen.PAYMENTMETHODS);

//			tbLeadId=new IntegerBox();
			tbLeadId=new TextBox();
			tbQuotatinId=new TextBox();
			tbContractId=new TextBox();
			tbRefeRRedBy=new TextBox();
			tbQuotationStatus=new TextBox();
			taDescription=new TextArea();
			ucUploadTAndCs=new UploadComposite();
			lineitemsalesquotationtable=new SalesLineItemSalesQuotationTable();
			dbquotationDate=new DateBoxWithYearSelector();
			personInfoComposite=AppUtility.customerInfoComposite(new Customer());
			olbeSalesPerson=new ObjectListBox<Employee>();
//			AppUtility.makeSalesPersonListBoxLive(olbeSalesPerson);
			olbeSalesPerson.makeEmployeeLive(AppConstants.SALESMODULE, AppConstants.QUOTATION, "Sales Person");
			prodInfoComposite=AppUtility.initiateSalesProductComposite(new SuperProduct());
			
			
			olbQuotationGroup=new ObjectListBox<Config>();
			AppUtility.MakeLiveConfig(olbQuotationGroup, Screen.SALESQUOTATIONGROUP);
			olbQuotationCategory=new ObjectListBox<ConfigCategory>();
			AppUtility.MakeLiveCategoryConfig(olbQuotationCategory, Screen.SALESQUOTATIONCATEGORY);
			olbQuotationType=new ObjectListBox<Type>();
			AppUtility.makeTypeListBoxLive(olbQuotationType, Screen.SALESQUOTATIONTYPE);
			
			/************************************Catgory*********************************/
			
				olbQuotationCategory.addChangeHandler(this);
			
			/***************************************************************************/
			
			
//			setOneToManyCombo();
			ibCreditPeriod=new IntegerBox();
			olbApproverName=new ObjectListBox<Employee>();
			AppUtility.makeApproverListBoxLive(olbApproverName,"Sales Quotation");
		
			
			this.tbContractId.setEnabled(false);
			this.tbQuotationStatus.setEnabled(false);
			this.tbLeadId.setEnabled(false);
			this.tbQuotationStatus.setText(SalesOrder.CREATED);
			this.tbQuotatinId.setEnabled(false);
			
			personInfoComposite.getCustomerId().getHeaderLabel().setText("* Customer ID");
			personInfoComposite.getCustomerName().getHeaderLabel().setText("* Customer Name");
			personInfoComposite.getCustomerCell().getHeaderLabel().setText("* Customer Cell");
			
			cbtaxSwitch=new CheckBox();
			cbtaxSwitch.setEnabled(false);
			
			tbremark=new TextBox();
			tbremark.setEnabled(false);
			
			ibdays=new IntegerBox();
			dopercent=new DoubleBox();
			tbcomment=new TextBox();
			addPaymentTerms=new Button("ADD");
			addPaymentTerms.addClickHandler(this);
			paymentTermsTable=new PaymentTermsTable();
			chargesTable=new ProductChargesTable();
			addOtherCharges=new Button("+");
			addOtherCharges.addClickHandler(this);
			prodTaxTable=new ProductTaxesTable();
			dototalamt=new DoubleBox();
			dototalamt.setEnabled(false);
			doamtincltax=new DoubleBox();
			doamtincltax.setEnabled(false);
			donetpayamt=new DoubleBox();
			donetpayamt.setEnabled(false);
			
//			dbdeliverydate=new DateBox();
			dbdeliverydate = new DateBoxWithYearSelector();		
			
			acshippingcomposite=new AddressComposite();
			acshippingcomposite.setEnable(false);
			cbCheckAddress=new CheckBox();
			cbCheckAddress.setValue(false);
			cbcformlis=new ListBox();
			cbcformlis.addItem("SELECT");
			cbcformlis.addItem(AppConstants.YES);
			cbcformlis.addItem(AppConstants.NO);
			cbcformlis.setEnabled(false);
			olbcstpercent=new ObjectListBox<TaxDetails>();
			olbcstpercent.setEnabled(false);
			initializePercentCst();
			baddproducts=new Button("ADD");
			baddproducts.addClickHandler(this);
			
			btnsecurityDep = new Button("Security Deposit");
			btnsecurityDep.setVisible(false);
			validateSecurityDeposit();
			/** date 09/03/2018 added by komal for followup date **/
			dbFollowUpDate = new DateBoxWithYearSelector();		
			
			/**
			 * Date : 23-03-2018 BY VIJAY
			 * Adding other charges table
			 */
			tblOtherCharges=new OtherChargesTable();
			
			addOthrChargesBtn=new Button("+");
			addOthrChargesBtn.addClickHandler(this);
			
			dbOtherChargesTotal=new DoubleBox();
			dbOtherChargesTotal.setEnabled(false);
			
			
			/**
			 * End
			 */
			
			dbfinalTotalAmt = new DoubleBox();
			dbfinalTotalAmt.setEnabled(false);
			tbroundoffAmt = new TextBox();
			tbroundoffAmt.addChangeHandler(this);
			/**
			 * nidhi
			 * 1-08-2018
			 * for status reason
			 */
			tbDocStatusreason = new TextBox();
			tbDocStatusreason.setEnabled(false);
			/*
			 * end
			 */
			
			this.changeWidgets();
			
			costingBtn=new Button("Costing");
			costingBtn.addClickHandler(this);
			
			olbcustBranch = new ObjectListBox<CustomerBranchDetails>();
			olbcustBranch.addItem("--SELECT--");
			if(getPersonInfoComposite().getIdValue()!=-1) {
				AppUtility.makeCustomerBranchLive(olbcustBranch,getPersonInfoComposite().getIdValue());
			}
			
			
			olbtermsAndConditionTitle = new ObjectListBox<TermsAndConditions>();
			AppUtility.makeLiveTermsAndCondition(olbtermsAndConditionTitle, "SalesQuotation");
			
//			olbtermsAndConditionTitle.getElement().getStyle().setWidth(250, Unit.PX);

			
			termsAndConditionTable = new TermsAndConditionTable();
			btnTermsAndConditionAdd = new Button("Add");
			btnTermsAndConditionAdd.setEnabled(false);
			btnTermsAndConditionAdd.addClickHandler(this);
			chkPrintAllTermsAndCondition = new CheckBox();
			chkPrintAllTermsAndCondition.setValue(true);
			chkPrintAllTermsAndCondition.addClickHandler(this);
			
			objPaymentMode = new ObjectListBox<CompanyPayment>();
			loadPaymentMode();
		}

		/**
		 * method template to create screen formfields.
		 */
		@Override
		public void createScreen() {


			initalizeWidget();

			marginFlag=AppUtility.checkForProcessConfigurartionIsActiveOrNot("SalesQuotation","CalculateVendorAndOtherChargesMargin");
			
			//Token to initialize the processlevel menus.
			/** date 07/11/2017 added by komal to add reviced quotation button **/
			/**
			 * Date : 14-11-2017 By MANISHA
			 * added Cancel button
			 */
			//this.processlevelBarNames=new String[]{ManageApprovals.APPROVALREQUEST,ManageApprovals.CANCELAPPROVALREQUEST,AppConstants.CREATESALESORDER,AppConstants.Unsucessful,
					//AppConstants.NEW,ManageApprovals.SUBMIT,"Revised Quotation",AppConstants.CANCEL};
			/**
			 * Date : 28-12-2020 By Priyanka
			 * Des : Change Navigation flow as per requirement of simplification Project raised by Nitin Sir.
			 */
			this.processlevelBarNames=new String[]{ManageApprovals.SUBMIT,AppConstants.CANCEL,AppConstants.NEW,AppConstants.CREATESALESORDER,"Revised Quotation",AppConstants.Unsucessful,ManageApprovals.APPROVALREQUEST,ManageApprovals.CANCELAPPROVALREQUEST, AppConstants.CUSTOMERADDRESS};
	
			//////////////////////////// Form Field Initialization////////////////////////////////////////////////////
			//Token to initialize formfield
			
			/**
			 * @author Anil @since 27-04-2021
			 */
			String mainScreenLabel="Quotation Information";
//			String mainScreenLabel="QUATATION";
			if(quotationObject!=null&&quotationObject.getCount()!=0){
				mainScreenLabel=quotationObject.getCount()+" "+"/"+" "+quotationObject.getStatus()+" "+"/"+" "+AppUtility.parseDate(quotationObject.getCreationDate());
			}
			// fgroupingCustomerInformation.getHeaderLabel().setText(mainScreenLabel);
			FormFieldBuilder fbuilder;
			fbuilder = new FormFieldBuilder();
			fgroupingCustomerInformation=fbuilder.setlabel(mainScreenLabel).widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).setKeyField(true).build();
			
			//FormField fgroupingCustomerInformation=fbuilder.setlabel("Customer Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
			fbuilder = new FormFieldBuilder("",personInfoComposite);
			FormField fpersonInfoComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
			FormField ftbLeadId;
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("SalesQuotation","makeLeadIdMandatory")
					&& !LoginPresenter.myUserEntity.getRole().getRoleName().equalsIgnoreCase("Admin")){
				fbuilder = new FormFieldBuilder("* Lead ID",tbLeadId);
				ftbLeadId= fbuilder.setMandatory(true).setMandatoryMsg("Lead Id is Mandatory").setRowSpan(0).setColSpan(0).build();
			}else{
				fbuilder = new FormFieldBuilder("Lead ID",tbLeadId);
				ftbLeadId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			}
			fbuilder = new FormFieldBuilder("Quotation ID",tbQuotatinId);
			FormField ftbQuotatinId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("Order ID",tbContractId);
			FormField ftbContractId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("* Valid Until",dbValidUntill);
			FormField fdbValidUntill= fbuilder.setMandatory(true).setMandatoryMsg("Valid Until is Mandatory!").setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("Referred By",tbRefeRRedBy);
			FormField ftbRefeRRedBy= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder();
			FormField fgroupingDocuments=fbuilder.setlabel("Attachment").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
			fbuilder = new FormFieldBuilder("Upload T&Cs",ucUploadTAndCs);
			FormField fucUploadTAndCs= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
			fbuilder = new FormFieldBuilder();
			FormField fgroupingSalesInformation=fbuilder.setlabel("Classification").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
			fbuilder = new FormFieldBuilder("Quotation Status",tbQuotationStatus);
			FormField ftbQuotationStatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			/**
			 *  Date : 15-01-2021 By Priyanka.
			 *  Des : Quotation priority not mandotory as per Nitin Sir.
			 */
			fbuilder = new FormFieldBuilder("Priority",olbcPriority);
			FormField folbcPriority= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("* Branch",olbbBranch);
			FormField folbbBranch= fbuilder.setMandatory(true).setMandatoryMsg("Branch is mandatory!").setRowSpan(0).setColSpan(0).build();
			/**@Sheetal:01-02-2022
            Des : Making Sales person non mandatory,requirement by UDS Water**/
			fbuilder = new FormFieldBuilder("Sales Person",olbeSalesPerson);
			FormField folbeSalesPerson= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			/**
			 * date 23-4-2018
			 * By jayshree
			 * Dis.Remove the payment method mandatory
			 */
//			fbuilder = new FormFieldBuilder("* Payment Method",olbcPaymentMethods);
//			FormField folbcPaymentMethods= fbuilder.setMandatory(true).setMandatoryMsg("Payment Methods is mandatory!").setRowSpan(0).setColSpan(0).build();
			
			fbuilder = new FormFieldBuilder(" Payment Method",olbcPaymentMethods);
			FormField folbcPaymentMethods= fbuilder.setMandatory(false).setMandatoryMsg("Payment Methods is mandatory!").setRowSpan(0).setColSpan(0).build();
			
			fbuilder = new FormFieldBuilder();
			FormField fgroupingDescription=fbuilder.setlabel("Reference/General").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
			fbuilder = new FormFieldBuilder("Description 1",taDescription);
			FormField ftaDescription= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
			
			fbuilder = new FormFieldBuilder("Description 2",taDescriptionTwo);
			FormField ftaDescriptionTwo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
			
			
			fbuilder = new FormFieldBuilder("Vendor Price",dbVendorPrice);
			FormField fdbVendorPrice= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			
			
			
			
			
			fbuilder = new FormFieldBuilder("Remark",tbremark);
			FormField ftbremark= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder();
			FormField fgroupingProducts=fbuilder.setlabel("Product").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();

			fbuilder = new FormFieldBuilder("Quotation Type",olbQuotationType);
			FormField folbQuotationType= fbuilder.setMandatory(false).setMandatoryMsg("Quotation Type is mandatory!").setRowSpan(0).setColSpan(0).build();

			fbuilder = new FormFieldBuilder("Quotation Group",olbQuotationGroup);
			FormField folbQuotationGroup= fbuilder.setMandatory(false).setMandatoryMsg("Quotation Group is mandatory!").setRowSpan(0).setColSpan(0).build();

			fbuilder = new FormFieldBuilder("Quotation Category",olbQuotationCategory);
			FormField folbQuotationCategory= fbuilder.setMandatory(false).setMandatoryMsg("Quotation Category is mandatory!").setRowSpan(0).setColSpan(0).build();

			fbuilder = new FormFieldBuilder("* Approver Name",olbApproverName);
			FormField folbApproverName= fbuilder.setMandatory(true).setMandatoryMsg("Approver Name is mandatory!").setRowSpan(0).setColSpan(0).build();
			
			fbuilder = new FormFieldBuilder("Credit Days",ibCreditPeriod);
			FormField fibCreditPeriod= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("* Quotation Date",dbquotationDate);
			FormField fdbquotationDate= fbuilder.setMandatory(true).setMandatoryMsg("Quotation Date is Mandatory!").setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder();
			FormField fgroupingPaymentTermsDetails=fbuilder.setlabel("Payment Terms").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
			fbuilder = new FormFieldBuilder("Days",ibdays);
			FormField fibdays= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("Percent",dopercent);
			FormField fdopercent= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("Comment",tbcomment);
			FormField ftbcomment= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("",addPaymentTerms);
			FormField faddPaymentTerms= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("",paymentTermsTable.getTable());
			FormField fpaymentTermsTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
			fbuilder = new FormFieldBuilder();
			FormField fblankgroup=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
			fbuilder = new FormFieldBuilder();
			FormField fblankgroupthree=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
			fbuilder = new FormFieldBuilder();
			FormField fblankgroupone=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("",prodTaxTable.getTable());
			FormField fprodTaxTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
			//fbuilder = new FormFieldBuilder("Total Amount",doAmtExcludingTax);
			//FormField fdoAmtExcludingTax= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
			fbuilder = new FormFieldBuilder("Total Amount",doamtincltax);
			FormField fdoAmtIncludingTax= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
			fbuilder = new FormFieldBuilder("Total Amount",dototalamt);
			FormField fpayCompTotalAmt= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
			fbuilder = new FormFieldBuilder("",addOtherCharges);
			FormField faddOtherCharges= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("",chargesTable.getTable());
			FormField fchargesTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
			fbuilder = new FormFieldBuilder("Net Payable",donetpayamt);
			FormField fpayCompNetPay= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
			fbuilder = new FormFieldBuilder();
			FormField fgroupingShippingInfo=fbuilder.setlabel("Shipping Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
			fbuilder = new FormFieldBuilder("* Delivery Date",dbdeliverydate);
			FormField fdbdeliverydate= fbuilder.setMandatory(true).setMandatoryMsg("Delivery Date is Mandatory!").setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("Is Shipping Address Different",cbCheckAddress);
			FormField fcbcheckaddress= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("",acshippingcomposite);
			FormField facshippingcomposite=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
			fbuilder = new FormFieldBuilder("",prodInfoComposite);
			FormField fprodInfoComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
			fbuilder = new FormFieldBuilder("C Form",cbcformlis);
			FormField fcbcform= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("CST Percent",olbcstpercent);
			FormField folbcstpercent= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("",baddproducts);
			FormField fbadd= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("",lineitemsalesquotationtable.getTable());
			FormField fsaleslineitemtable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
			
			fbuilder = new FormFieldBuilder("",btnsecurityDep);
			FormField fbtnsecurity= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			
			/** date 09/03/2018 added by komal for followup date **/
			fbuilder = new FormFieldBuilder("Followup Date",dbFollowUpDate);
			FormField fdbFollowUpDate = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

			/**
			 * Date :23-03-2018 By Vijay for Other charges table
			 */
			fbuilder = new FormFieldBuilder("",tblOtherCharges.getTable());
			FormField fothChgTbl= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
			
			fbuilder = new FormFieldBuilder("",addOthrChargesBtn);
			FormField faddOthrChargesBtn= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			
			fbuilder = new FormFieldBuilder("Total Amount",dbOtherChargesTotal);
			FormField fdbOtherChargesTotal= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
			/**
			 * ends here
			 */
			
			/**
			 * Date 08-06-2018 By vijay
			 * Des :- for final Total amt and roundoff amt
			 */
			fbuilder = new FormFieldBuilder("Total Amount",dbfinalTotalAmt);
			FormField fdbfinalTotalAmt = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
			
			fbuilder = new FormFieldBuilder("Round Off",tbroundoffAmt);
			FormField ftbroundoffAmt = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
			
			/**
			 * ends here
			 */
			/**
			 * nidhi
			 * 1-08-208
			 */
			fbuilder = new FormFieldBuilder("Unsuccesful Reason" , tbDocStatusreason);
			FormField ftbdocstatusreson = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			/**
			 * end
			 */
			
			fbuilder = new FormFieldBuilder("" , costingBtn);
			FormField fcostingBtn = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).setVisible(marginFlag).build();
			
			
			fbuilder = new FormFieldBuilder();
			FormField fgroupTermsAndConditions=fbuilder.setlabel("Terms And Conditions").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
			
			
			
			fbuilder = new FormFieldBuilder("" , termsAndConditionTable.getTable());
			FormField fTermsAndConditiontable = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
			
			fbuilder = new FormFieldBuilder("Customer Branch" , olbcustBranch);
			FormField folbcustBranch = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			
		
			fbuilder = new FormFieldBuilder("Terms And Conditions Title" , olbtermsAndConditionTitle);
			FormField folbtermsAndConditionTitle = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			
			fbuilder = new FormFieldBuilder("" , btnTermsAndConditionAdd);
			FormField fbtnTermsAndConditionAdd = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			
			fbuilder = new FormFieldBuilder("Print All Terms And Condition" , chkPrintAllTermsAndCondition);
			FormField fchkPrintAll = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			
			
			fbuilder = new FormFieldBuilder("Payment Mode",objPaymentMode);
			FormField fobjPaymentMode= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
			
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

			////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////


			FormField[][] formfield = {  
//					{fgroupingCustomerInformation},
//					{fpersonInfoComposite},
//					{ftbQuotatinId,ftbLeadId,ftbContractId,fdbValidUntill},
//					{fdbquotationDate,fdbFollowUpDate},//komal
//					{fgroupingSalesInformation},
//					{ftbQuotationStatus,folbcPriority,folbbBranch,folbeSalesPerson},
//					{folbcPaymentMethods,folbQuotationGroup,folbQuotationCategory,folbQuotationType},
//					{folbApproverName,fibCreditPeriod,fdbVendorPrice,ftbremark},
//					{ftbdocstatusreson,fbtnsecurity},//nidhi
//					{fgroupingDescription},
//					{ftaDescription},
//					{ftaDescriptionTwo},
//					{fgroupingDocuments},
//					{fucUploadTAndCs},
//					{fgroupingPaymentTermsDetails},
//					{fibdays,fdopercent,ftbcomment,faddPaymentTerms},
//					{fpaymentTermsTable},
//					{fgroupingProducts},
//					{fcostingBtn},
//					{fprodInfoComposite,fbadd},
//					{fsaleslineitemtable},
//					{fblankgroupthree,fpayCompTotalAmt},
//					
//					{fblankgroup,faddOthrChargesBtn},
//					{fblankgroup,fothChgTbl},/** Date : 14-09-2017 BY VIJAY Other Charges**/
//					{fblankgroupthree,fdbOtherChargesTotal},
//					
//					{fblankgroupthree},
//					{fblankgroup,fprodTaxTable},
////					{fblankgroup,faddOtherCharges,fdoAmtIncludingTax}, /** Date : 23-03-2018 BY VIJAY for other charges**/
////					{fblankgroup,fchargesTable},
//					
//					/*** Date 08-06-2018 By vijay **/
//					{fblankgroup,fblankgroupone,fdbfinalTotalAmt},
//					{fblankgroup,fblankgroupone,ftbroundoffAmt},
//					/*** ends here ****/
//					
//					{fblankgroup,fblankgroupone,fpayCompNetPay},
//					{fgroupingShippingInfo},{fdbdeliverydate,fcbcheckaddress},
//					{facshippingcomposite}
//			};
					
					/**Main Screen**/
					{fgroupingCustomerInformation},
					{ fpersonInfoComposite},
					{ fdbquotationDate,fdbValidUntill,folbbBranch,folbeSalesPerson},
					{ fdbFollowUpDate,folbApproverName,ftbremark, ftbdocstatusreson},
					/** Product **/
					{fgroupingProducts},
					{fcostingBtn},
					{fprodInfoComposite,fbadd},
					{fsaleslineitemtable},
					{fblankgroupthree,fpayCompTotalAmt},
					{fblankgroup,faddOthrChargesBtn},
					{fblankgroup,fothChgTbl},
					{fblankgroupthree,fdbOtherChargesTotal},
					{fblankgroupthree},
					{fblankgroup,fprodTaxTable},
					{fblankgroup,fblankgroupone,fdbfinalTotalAmt},
					{fblankgroup,fblankgroupone,ftbroundoffAmt},
					{fblankgroup,fblankgroupone,fpayCompNetPay},

					/**Shipping Address**/
					{fgroupingShippingInfo},
					{folbcustBranch,fdbdeliverydate},
					{facshippingcomposite},
					/**Payment Terms**/
					{fgroupingPaymentTermsDetails},
					{folbcPaymentMethods,fobjPaymentMode,fibCreditPeriod},
					{fibdays,fdopercent,ftbcomment,faddPaymentTerms},
					{fpaymentTermsTable},
					/**Classification**/
					{fgroupingSalesInformation},
					{folbQuotationGroup,folbQuotationCategory,folbQuotationType,folbcPriority},
					/**Reference/General**/
					{fgroupingDescription},
					{ftbLeadId,ftbContractId,fdbVendorPrice},
					{ftaDescription},
					{ftaDescriptionTwo},
					/**Attachment**/
					{fgroupingDocuments},
					{fucUploadTAndCs},
					
					{fgroupTermsAndConditions},
					{folbtermsAndConditionTitle,fbtnTermsAndConditionAdd,fchkPrintAll},
					{fTermsAndConditiontable}
			};
			
					
					
			this.fields=formfield;
			
		}
		

		/**
		 * method template to update the model with token entity name.
		 *
		 * @param model the model
		 */
		@Override
		public void updateModel(SalesQuotation model) 
		{
			if(personInfoComposite.getValue()!=null)
				model.setCinfo(personInfoComposite.getValue());
			if(dbValidUntill.getValue()!=null)
				model.setValidUntill(dbValidUntill.getValue());
			if(tbRefeRRedBy.getValue()!=null)
				model.setReferedBy(tbRefeRRedBy.getValue());
			
			model.setDocument(ucUploadTAndCs.getValue());
			if(tbQuotationStatus.getValue()!=null)
				model.setStatus(tbQuotationStatus.getValue());
			if(olbcPriority.getValue()!=null)
				model.setPriority(olbcPriority.getValue());
			if(olbbBranch.getValue()!=null)
				model.setBranch(olbbBranch.getValue());
			if(olbeSalesPerson.getValue()!=null)
				model.setEmployee(olbeSalesPerson.getValue());
			if(olbcPaymentMethods.getValue()!=null)
				model.setPaymentMethod(olbcPaymentMethods.getValue());
			if(taDescription.getValue()!=null)
				model.setDescription(taDescription.getValue());
			if(olbQuotationType.getValue()!=null)
				model.setType(olbQuotationType.getValue(olbQuotationType.getSelectedIndex()));
			if(olbQuotationGroup.getValue()!=null)
				model.setGroup(olbQuotationGroup.getValue());
			if(olbQuotationCategory.getValue()!=null)
				model.setCategory(olbQuotationCategory.getValue());
			if(olbApproverName.getValue()!=null)
				model.setApproverName(olbApproverName.getValue());
			
			if(!tbContractId.getValue().equals("")){
//				Integer contractCount=Integer.parseInt(tbContractId.getValue());
				model.setContractCount(Integer.parseInt(tbContractId.getValue()));
			}
			if(!tbLeadId.getValue().equals("")){
//				Integer leadCount=Integer.parseInt(tbLeadId.getValue());
				model.setLeadCount(Integer.parseInt(tbLeadId.getValue()));
			}
			
			if(ibCreditPeriod.getValue()!=null){
				model.setCreditPeriod(ibCreditPeriod.getValue());
			}
			
			if(dototalamt.getValue()!=null)
				model.setTotalAmount(dototalamt.getValue());
			if(donetpayamt.getValue()!=null)
				model.setNetpayable(donetpayamt.getValue());
			
			if(dbdeliverydate.getValue()!=null){
				model.setDeliveryDate(dbdeliverydate.getValue());
			}
			
			if(acshippingcomposite.getValue()!=null){
				model.setShippingAddress(acshippingcomposite.getValue());
			}
			
			if(cbcformlis.getSelectedIndex()!=0){
				model.setCformstatus(cbcformlis.getItemText(cbcformlis.getSelectedIndex()));
			}
			
			if(olbcstpercent.getValue()!=null){
				TaxDetails taxperc=olbcstpercent.getSelectedItem();
				model.setCstpercent(taxperc.getTaxChargePercent());
			}
			
			if(olbcstpercent.getValue()!=null){
				model.setCstName(olbcstpercent.getValue());
			}
			if(cbCheckAddress.getValue()!=null){
				model.setCheckAddress(cbCheckAddress.getValue());
			}
			if(dbquotationDate.getValue()!=null){
				model.setQuotationDate(dbquotationDate.getValue());
			}
			if(taDescriptionTwo.getValue()!=null)
				model.setDescriptiontwo(taDescriptionTwo.getValue());
			
			if(dbVendorPrice.getValue()!=null)
				model.setVendorPrice(dbVendorPrice.getValue());
			List<SalesLineItem> saleslis=this.getLineitemsalesquotationtable().getDataprovider().getList();
			ArrayList<SalesLineItem> arrItems=new ArrayList<SalesLineItem>();
			arrItems.addAll(saleslis);
			model.setItems(arrItems);
			
			List<PaymentTerms> payTermsLis=this.paymentTermsTable.getDataprovider().getList();
			ArrayList<PaymentTerms> payTermsArr=new ArrayList<PaymentTerms>();
			payTermsArr.addAll(payTermsLis);
			model.setPaymentTermsList(payTermsArr);
			
			List<ProductOtherCharges> productTaxLis=this.prodTaxTable.getDataprovider().getList();
			ArrayList<ProductOtherCharges> productTaxArr=new ArrayList<ProductOtherCharges>();
			productTaxArr.addAll(productTaxLis);
			model.setProductTaxes(productTaxArr);
			
			/**
			 * Date :23-03-2018 By Vijay for Other charges table
			 */
			List<OtherCharges> otherChargesList=this.tblOtherCharges.getDataprovider().getList();
			ArrayList<OtherCharges> otherChargesArr=new ArrayList<OtherCharges>();
			otherChargesArr.addAll(otherChargesList);
			model.setOtherCharges(otherChargesArr);
			
			/**
			 * End
			 */
			
			
			List<ProductOtherCharges> productChargeLis=this.chargesTable.getDataprovider().getList();
			ArrayList<ProductOtherCharges> productChargeArr=new ArrayList<ProductOtherCharges>();
			productChargeArr.addAll(productChargeLis);
			model.setProductCharges(productChargeArr);
			
			
	/**************************Saving Security Deposit PopUp Date*************************/
			
			if (f_securityDeposit != null) {
				
				if(f_securityDeposit>0){
					model.setIsDepositPaid(AppConstants.YES);
				}else{
					model.setIsDepositPaid(AppConstants.NO);
				}
				model.setSdDepositAmount(f_securityDeposit);
			}
			
			if (f_reDate != null) {
				model.setSdReturnDate(f_reDate);
			}
			if (f_depositStatus != false) {
				model.setDepositReceived(f_depositStatus);
			}
			if (f_payMethod != null) {
				model.setSdPaymentMethod(f_payMethod);
			}
			if(f_accountNo !=null){
				model.setSdBankAccNo(f_accountNo);
			}
			
			if (f_bankName != null) {
				model.setSdBankName(f_bankName);
			}
			
			if (f_branch != null) {
				model.setSdBankBranch(f_branch);
			}
			if (f_paybleAt != null) {
				model.setSdPayableAt(f_paybleAt);
			}
			if (f_fouring != null) {
				model.setSdFavouring(f_fouring);
			}
			if(f_checkNo!=0){
				model.setSdChequeNo(f_checkNo);
			}
			if (f_checkDate != null) {
				model.setSdChequeDate(f_checkDate);
			}
			if(!f_serviceNo.equals("")){
				model.setSdReferenceNo(f_serviceNo);
			}
			if (f_ifscCode != null) {
				model.setSdIfscCode(f_ifscCode);
			}
			if (f_micrCode != null) {
				model.setSdMicrCode(f_micrCode);
			}
			if (f_swiftCode != null) {
				model.setSdSwiftCode(f_swiftCode);
			}

			if (f_address != null) {
				model.setSdAddress(f_address);
			}
			if (f_instruction != null) {
				model.setSdInstruction(f_instruction);
			}
			
			/** Date 18-sep-2017 added by vijay becs this total amt showing only screen now i am saving it into database ***********/
			if(doamtincltax.getValue()!=null){
				model.setInclutaxtotalAmount(doamtincltax.getValue());
			}
			/** date 09/03/2018 added by komal for followup date **/
			if(dbFollowUpDate.getValue()!=null){
				model.setFollowUpDate(dbFollowUpDate.getValue());
			}else{
				model.setFollowUpDate(dbquotationDate.getValue());
				dbFollowUpDate.setValue(dbquotationDate.getValue());
			}
			
			
			/**
			 * Date 08/06/2018
			 * Developer :- Vijay
			 * Des :- for final total amt Round off amt 
			 */
			  if(dbfinalTotalAmt.getValue()!=null){
				  model.setTotalFinalAmount(dbfinalTotalAmt.getValue());;
			  }
			  if(tbroundoffAmt.getValue()!=null && !tbroundoffAmt.getValue().equals("")){
				  model.setRoundOffAmount(Double.parseDouble(tbroundoffAmt.getValue()));
			  }
			/**
			 * ends here
			 */
			  /**
				 * nidhi
				 * 1-08-2018
				 */
				model.setDocStatusReason(tbDocStatusreason.getValue());
				/**
				 * end
				 */
				
			if(marginFlag){
//				reactOnCostingSave();
				if(vendorMargins==null){
					vendorMargins=new ArrayList<VendorMargin>();
				}
				if(otherChargesMargins==null){
					otherChargesMargins=new ArrayList<OtherChargesMargin>();
				}
				marginPopup.reactOnCostingSave(lineitemsalesquotationtable.getValue(),vendorMargins,otherChargesMargins);
				totalVendorMargin=marginPopup.dbTotalVendorMargin.getValue();
				totalOcMargin=marginPopup.dbOtherChargesMargin.getValue();
				
				Console.log("Vendor "+vendorMargins.size());
				Console.log("Other "+otherChargesMargins.size());
			}
			if(vendorMargins!=null){
				model.setVendorMargins(vendorMargins);
			}
			if(otherChargesMargins!=null){
				model.setOtherChargesMargins(otherChargesMargins);
			}
			if(totalVendorMargin!=null){
				model.setTotalVendorMargin(totalVendorMargin);
			}
			if(totalOcMargin!=null){
				model.setTotalOtherChargesMargin(totalOcMargin);
			}

			if(olbcustBranch.getSelectedIndex()!=0) {
				model.setCustomerBranch(olbcustBranch.getValue());
				if(model.getId()==null){
					CustomerBranchDetails branchdetails = olbcustBranch.getSelectedItem();
					if(branchdetails!=null) {
						if(branchdetails.getBillingAddress()!=null) {
							model.setCustBillingAddress(branchdetails.getBillingAddress());
						}
					}
				}
				
			}
			else {
				model.setCustomerBranch("");
			}
			
			if(customerBillingAddress!=null){
				model.setCustBillingAddress(customerBillingAddress);
			}
			
			System.out.println("this.termsAndConditionTable.getDataprovider().getList().size() "+this.termsAndConditionTable.getDataprovider().getList().size());
			if(this.termsAndConditionTable.getDataprovider().getList()!=null && this.termsAndConditionTable.getDataprovider().getList().size()>0){
				ArrayList<TermsAndConditions> arraylist = new ArrayList<TermsAndConditions>();
				List<TermsAndConditions> list = this.termsAndConditionTable.getDataprovider().getList();
				arraylist.addAll(list);
				model.setTermsAndConditionlist(arraylist);
			}
			
			model.setPrintAllTermsConditions(chkPrintAllTermsAndCondition.getValue());
			
			if(objPaymentMode.getSelectedIndex()!=0){
				model.setPaymentModeName(objPaymentMode.getValue(objPaymentMode.getSelectedIndex()));
			}else{
				model.setPaymentModeName("");
			}
			
			/**Date - 30-12-2020 added by Priyanka issued raised by Rahul as id and status not appears**/
			quotationObject=model;

			presenter.setModel(model);
		}

		/**
		 * method template to update the view with token entity name.
		 *
		 * @param view the view
		 */
		@Override
		public void updateView(SalesQuotation view) 
		{
			/**Date - 30-12-2020 added by Priyanka issued raised by Rahul as id and status not appears**/
			quotationObject=view;
			this.tbQuotatinId.setValue(view.getCount()+"");
			
			
			this.checkCustomerStatus(view.getCinfo().getCount());
			
			if(view.getCinfo()!=null)
				personInfoComposite.setValue(view.getCinfo());
			if(view.getValidUntill()!=null)
				dbValidUntill.setValue(view.getValidUntill());
			if(view.getReferedBy()!=null)
				tbRefeRRedBy.setValue(view.getReferedBy());
			if(view.getDocument()!=null)
				ucUploadTAndCs.setValue(view.getDocument());
			if(view.getStatus()!=null)
				tbQuotationStatus.setValue(view.getStatus());
			if(view.getPriority()!=null)
				olbcPriority.setValue(view.getPriority());
			if(view.getBranch()!=null)
				olbbBranch.setValue(view.getBranch());
			if(view.getEmployee()!=null)
				olbeSalesPerson.setValue(view.getEmployee());
			if(view.getPaymentMethod()!=null)
				olbcPaymentMethods.setValue(view.getPaymentMethod());
			if(view.getDescription()!=null)
				taDescription.setValue(view.getDescription());
			if(view.getCategory()!=null)
				olbQuotationCategory.setValue(view.getCategory());
			if(view.getType()!=null)
				olbQuotationType.setValue(view.getType());
			if(view.getGroup()!=null)
				olbQuotationGroup.setValue(view.getGroup());
			if(view.getApproverName()!=null)
				olbApproverName.setValue(view.getApproverName());
			if(view.getQuotationDate()!=null){
				dbquotationDate.setValue(view.getQuotationDate());
			}
			if(view.getCreditPeriod()!=null)
				ibCreditPeriod.setValue(view.getCreditPeriod());
			
			if(view.getLeadCount()!=-1)
				tbLeadId.setValue(view.getLeadCount()+"");
			if(view.getContractCount()!=-1)
				tbContractId.setValue(view.getContractCount()+"");
			if(view.getDeliveryDate()!=null){
				dbdeliverydate.setValue(view.getDeliveryDate());
			}
			if(view.getShippingAddress()!=null){
				acshippingcomposite.setValue(view.getShippingAddress());
			}
			lineitemsalesquotationtable.setValue(view.getItems());
			if(view.getCformstatus()!=null)
			{
				for(int i=0;i<cbcformlis.getItemCount();i++)
				{
					String data=cbcformlis.getItemText(i);
					if(data.equals(view.getCformstatus()))
					{
						cbcformlis.setSelectedIndex(i);
						break;
					}
				}
			}
			
			if(view.getCheckAddress()!=null){
				cbCheckAddress.setValue(view.getCheckAddress());
			}
			
			if(view.getRemark()!=null){
				tbremark.setValue(view.getRemark());
			}
			
			if(view.getDescriptiontwo()!=null)
				taDescriptionTwo.setValue(view.getDescriptiontwo());
			

			if(view.getVendorPrice()!=0)
				dbVendorPrice.setValue(view.getVendorPrice());
			
			if(view.getCinfo()!=null) {
				loadCustomerBranch(view.getCustomerBranch(),view.getCinfo().getCount());
			}
			olbcstpercent.setValue(view.getCstName());
			paymentTermsTable.setValue(view.getPaymentTermsList());
			prodTaxTable.setValue(view.getProductTaxes());
			chargesTable.setValue(view.getProductCharges());
			dototalamt.setValue(view.getTotalAmount());
			donetpayamt.setValue(view.getNetpayable());
			
			
			f_accountNo=view.getSdBankAccNo();
			f_bankName=view.getSdBankName();
			 f_securityDeposit=view.getSdDepositAmount();
			 f_reDate=view.getSdReturnDate();
			 f_depositStatus=view.isDepositReceived();
			 f_payMethod=view.getSdPaymentMethod();
			 f_branch=view.getSdBankBranch();
			 f_paybleAt=view.getSdPayableAt();
			 f_fouring=view.getSdFavouring();
			 f_checkNo=view.getSdChequeNo();
			 f_checkDate=view.getSdChequeDate();
			 f_serviceNo=view.getSdReferenceNo();
			 f_ifscCode=view.getSdIfscCode();
			 f_micrCode=view.getSdMicrCode();
			 f_swiftCode=view.getSdSwiftCode();
			 f_instruction=view.getSdInstruction();
			 f_address=view.getSdAddress();
			
		    /**
			 * Date : 20-09-2017 BY VIJAY
			 */
			tblOtherCharges.setValue(view.getOtherCharges());
			/**
			 * End
			 */
			doamtincltax.setValue(view.getInclutaxtotalAmount());
 
			/** date 09/03/2018 added by komal for followup date **/
			if(view.getFollowUpDate()!=null){
				dbFollowUpDate.setValue(view.getFollowUpDate());
			}
			
			
			/**
			 * Date 08/06/2018
			 * Developer :- Vijay
			 * Des :- for final total amt Round off amt 
			 */
			
			if(view.getTotalFinalAmount()!=0){
				dbfinalTotalAmt.setValue(view.getTotalFinalAmount());
			}
			if(view.getRoundOffAmount()!=0){
				tbroundoffAmt.setValue(view.getRoundOffAmount()+"");
			}
			/**
			 * ends here
			 */
			/**
			 * nidhi
			 * 1-08-2018
			 */
			tbDocStatusreason.setValue(view.getDocStatusReason());
			/**
			 * end
			 */
			
			if(view.getVendorMargins()!=null){
				vendorMargins=view.getVendorMargins();
			}
			if(view.getOtherChargesMargins()!=null){
				otherChargesMargins=view.getOtherChargesMargins();
			}
			totalVendorMargin=view.getTotalOtherChargesMargin();
			totalOcMargin=view.getTotalOtherChargesMargin();
			
			if(view.getCustomerBranch()!=null){
				olbcustBranch.setValue(view.getCustomerBranch());
				customerBranch = view.getCustomerBranch();

			}
			
			if(view.getTermsAndConditionlist()!=null && view.getTermsAndConditionlist().size()>0){
				termsAndConditionTable.getDataprovider().setList(view.getTermsAndConditionlist());
			}
			
			chkPrintAllTermsAndCondition.setValue(view.isPrintAllTermsConditions());
			
			if(view.getPaymentModeName()!=null){
	        	objPaymentMode.setValue(view.getPaymentModeName());
	        }
			
			 /* 
				 * for approval process
				 *  nidhi
				 *  5-07-2017
				 */
				if(presenter != null){
					presenter.setModel(view);
				}
				/*
				 *  end
				 */
		}
		
		/* By: Ashwini Patil
		 * Date: 5-1-2022
		 * Below method is a part of summary screen. It is used in GeneralViewDocumentPopUp to manage Revised Quotation view
		*/
		public void updateRevisedQuotationView(SalesQuotation view){

			/**Date - 30-12-2020 added by Priyanka issued raised by Rahul as id and status not appears**/
			quotationObject=view;
			this.tbQuotatinId.setValue(view.getCount()+"");
			
			//below method is commented. It is the only difference between this method and UpdateView method
			//this.checkCustomerStatus(view.getCinfo().getCount());
			
			if(view.getCinfo()!=null)
				personInfoComposite.setValue(view.getCinfo());
			if(view.getValidUntill()!=null)
				dbValidUntill.setValue(view.getValidUntill());
			if(view.getReferedBy()!=null)
				tbRefeRRedBy.setValue(view.getReferedBy());
			if(view.getDocument()!=null)
				ucUploadTAndCs.setValue(view.getDocument());
			if(view.getStatus()!=null)
				tbQuotationStatus.setValue(view.getStatus());
			if(view.getPriority()!=null)
				olbcPriority.setValue(view.getPriority());
			if(view.getBranch()!=null)
				olbbBranch.setValue(view.getBranch());
			if(view.getEmployee()!=null)
				olbeSalesPerson.setValue(view.getEmployee());
			if(view.getPaymentMethod()!=null)
				olbcPaymentMethods.setValue(view.getPaymentMethod());
			if(view.getDescription()!=null)
				taDescription.setValue(view.getDescription());
			if(view.getCategory()!=null)
				olbQuotationCategory.setValue(view.getCategory());
			if(view.getType()!=null)
				olbQuotationType.setValue(view.getType());
			if(view.getGroup()!=null)
				olbQuotationGroup.setValue(view.getGroup());
			if(view.getApproverName()!=null)
				olbApproverName.setValue(view.getApproverName());
			if(view.getQuotationDate()!=null){
				dbquotationDate.setValue(view.getQuotationDate());
			}
			if(view.getCreditPeriod()!=null)
				ibCreditPeriod.setValue(view.getCreditPeriod());
			
			if(view.getLeadCount()!=-1)
				tbLeadId.setValue(view.getLeadCount()+"");
			if(view.getContractCount()!=-1)
				tbContractId.setValue(view.getContractCount()+"");
			if(view.getDeliveryDate()!=null){
				dbdeliverydate.setValue(view.getDeliveryDate());
			}
			if(view.getShippingAddress()!=null){
				acshippingcomposite.setValue(view.getShippingAddress());
			}
			lineitemsalesquotationtable.setValue(view.getItems());
			if(view.getCformstatus()!=null)
			{
				for(int i=0;i<cbcformlis.getItemCount();i++)
				{
					String data=cbcformlis.getItemText(i);
					if(data.equals(view.getCformstatus()))
					{
						cbcformlis.setSelectedIndex(i);
						break;
					}
				}
			}
			
			if(view.getCheckAddress()!=null){
				cbCheckAddress.setValue(view.getCheckAddress());
			}
			
			if(view.getRemark()!=null){
				tbremark.setValue(view.getRemark());
			}
			
			if(view.getDescriptiontwo()!=null)
				taDescriptionTwo.setValue(view.getDescriptiontwo());
			

			if(view.getVendorPrice()!=0)
				dbVendorPrice.setValue(view.getVendorPrice());
			
			if(view.getCinfo()!=null) {
				loadCustomerBranch(view.getCustomerBranch(),view.getCinfo().getCount());
			}
			olbcstpercent.setValue(view.getCstName());
			paymentTermsTable.setValue(view.getPaymentTermsList());
			prodTaxTable.setValue(view.getProductTaxes());
			chargesTable.setValue(view.getProductCharges());
			dototalamt.setValue(view.getTotalAmount());
			donetpayamt.setValue(view.getNetpayable());
			
			
			f_accountNo=view.getSdBankAccNo();
			f_bankName=view.getSdBankName();
			 f_securityDeposit=view.getSdDepositAmount();
			 f_reDate=view.getSdReturnDate();
			 f_depositStatus=view.isDepositReceived();
			 f_payMethod=view.getSdPaymentMethod();
			 f_branch=view.getSdBankBranch();
			 f_paybleAt=view.getSdPayableAt();
			 f_fouring=view.getSdFavouring();
			 f_checkNo=view.getSdChequeNo();
			 f_checkDate=view.getSdChequeDate();
			 f_serviceNo=view.getSdReferenceNo();
			 f_ifscCode=view.getSdIfscCode();
			 f_micrCode=view.getSdMicrCode();
			 f_swiftCode=view.getSdSwiftCode();
			 f_instruction=view.getSdInstruction();
			 f_address=view.getSdAddress();
			
		    /**
			 * Date : 20-09-2017 BY VIJAY
			 */
			tblOtherCharges.setValue(view.getOtherCharges());
			/**
			 * End
			 */
			doamtincltax.setValue(view.getInclutaxtotalAmount());

			/** date 09/03/2018 added by komal for followup date **/
			if(view.getFollowUpDate()!=null){
				dbFollowUpDate.setValue(view.getFollowUpDate());
			}
			
			
			/**
			 * Date 08/06/2018
			 * Developer :- Vijay
			 * Des :- for final total amt Round off amt 
			 */
			
			if(view.getTotalFinalAmount()!=0){
				dbfinalTotalAmt.setValue(view.getTotalFinalAmount());
			}
			if(view.getRoundOffAmount()!=0){
				tbroundoffAmt.setValue(view.getRoundOffAmount()+"");
			}
			/**
			 * ends here
			 */
			/**
			 * nidhi
			 * 1-08-2018
			 */
			tbDocStatusreason.setValue(view.getDocStatusReason());
			/**
			 * end
			 */
			
			if(view.getVendorMargins()!=null){
				vendorMargins=view.getVendorMargins();
			}
			if(view.getOtherChargesMargins()!=null){
				otherChargesMargins=view.getOtherChargesMargins();
			}
			totalVendorMargin=view.getTotalOtherChargesMargin();
			totalOcMargin=view.getTotalOtherChargesMargin();
			
			if(view.getCustomerBranch()!=null){
				olbcustBranch.setValue(view.getCustomerBranch());
				customerBranch = view.getCustomerBranch();

			}
			
			 /* 
				 * for approval process
				 *  nidhi
				 *  5-07-2017
				 */
				if(presenter != null){
					presenter.setModel(view);
				}
				/*
				 *  end
				 */
		
		 
		 
	 }

		// Hand written code shift in presenter
		
		/**
		 * Toggles the app header bar menus as per screen state.
		 */

		public void toggleAppHeaderBarMenu() {
			if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				/**
				 * @author Anil @since 10-12-2021
				 */
				if(isPopUpAppMenubar()){
					 menus=GeneralViewDocumentPopup.applevelMenu;
				}
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.contains("Save")||text.contains("Discard")||text.contains("Search"))
					{
						menus[k].setVisible(true); 
					}
					else
						menus[k].setVisible(false);  		   
					
				}
			}

			else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				/**
				 * @author Anil @since 10-12-2021
				 */
				if(isPopUpAppMenubar()){
					 menus=GeneralViewDocumentPopup.applevelMenu;
				}
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.contains("Save")||text.contains("Discard")|| text.contains(AppConstants.COMMUNICATIONLOG))
						menus[k].setVisible(true); 
					
					else
						menus[k].setVisible(false);  		   
				}
			}

			else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
			{
				System.out.println("Edit StateSS");
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				/**
				 * @author Anil @since 10-12-2021
				 */
				if(isPopUpAppMenubar()){
					 menus=GeneralViewDocumentPopup.applevelMenu;
				}
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.contains("Edit")||text.contains("Discard")||text.contains("Search")
							||text.contains("Print")|| text.contains(AppConstants.COMMUNICATIONLOG))
							menus[k].setVisible(true); 
					
					else
						menus[k].setVisible(false);  		   
				}
			}
			AuthorizationHelper.setAsPerAuthorization(Screen.SALESQUOTATION,LoginPresenter.currentModule.trim());
		}


		/**
		 * Sets the viewas per process.
		 */
		public void setAppHeaderBarAsPerStatus()
		{
			SalesQuotation entity=(SalesQuotation) presenter.getModel();
			String status=entity.getStatus();
			if(status.equals(SalesQuotation.SALESQUOTATIONSUCESSFUL)||status.equals(SalesQuotation.SALESQUOTATIONUNSUCESSFUL)
					||status.equals(SalesQuotation.REJECTED)||status.equals(SalesQuotation.APPROVED))
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.contains("Discard")||text.contains("Search")||text.contains("Print")||text.contains(AppConstants.COMMUNICATIONLOG))
					{
						menus[k].setVisible(true); 
					}
					else
						menus[k].setVisible(false);  		   
				}
			}

			if(status.equals(SalesQuotation.APPROVED))
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.contains("Discard")||text.contains("Search")||text.contains("Email")||text.contains("Print")||text.contains(AppConstants.COMMUNICATIONLOG))
					{
						menus[k].setVisible(true); 
					}
					else
						menus[k].setVisible(false);  		   
				}
			}
			
			if(status.equals(SalesQuotation.REQUESTED))
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.contains("Discard")||text.contains("Search")||text.contains("Print")||text.contains(AppConstants.COMMUNICATIONLOG))
					{
						menus[k].setVisible(true); 
					}
					else
						menus[k].setVisible(false);  		   
				}
			}
			
			if(status.equals(SalesQuotation.REJECTED))
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.contains("Discard")||text.contains("Search")||text.contains("Print")||text.contains("Edit")||text.contains(AppConstants.COMMUNICATIONLOG))
					{
						menus[k].setVisible(true); 
					}
					else
						menus[k].setVisible(false);  		   
				}
			}
		}

		/**
		 * Toggle process level menu.
		 */
		public void toggleProcessLevelMenu()
		{
			SalesQuotation entity=(SalesQuotation) presenter.getModel();
			String status=entity.getStatus();
			for(int i=0;i<getProcesslevelBarNames().length;i++)
			{
				InlineLabel label=getProcessLevelBar().btnLabels[i];
				String text=label.getText().trim();
					
				if(status.equals(SalesQuotation.CREATED))
				{
					if(getManageapproval().isSelfApproval())
					{  
						if(text.equals(ManageApprovals.APPROVALREQUEST))
							label.setVisible(false);
						if(text.equals(ManageApprovals.SUBMIT))
							label.setVisible(true);
					}
					else
					{  
						if(text.equals(ManageApprovals.APPROVALREQUEST))
							label.setVisible(true);
						if(text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
							label.setVisible(false);
						if(text.equals(ManageApprovals.SUBMIT))
							label.setVisible(false);
					}
					if(text.equals(AppConstants.CREATESALESORDER))
						label.setVisible(false);
					if(text.equals(AppConstants.Unsucessful))
						label.setVisible(false);
					if(text.equals(updateQuotations))
						label.setVisible(true);
					/** date 07/11/2017 added by komal to add revised quotation button **/
					if(text.equals("Revised Quotation"))
					label.setVisible(false);
					
					/** Date :11/11/2017 by Manisha**/
					
					if(text.equals(AppConstants.CANCEL)){
						label.setVisible(true);
					}
					
					/**end*/
					
				}
				else if(status.equals(SalesQuotation.APPROVED))
				{
					if(text.equals(AppConstants.CREATESALESORDER))
						label.setVisible(true);
					if(text.equals(AppConstants.Approve))
						label.setVisible(false);	
					
					if(text.equals(updateQuotations))
						label.setVisible(true);
					if(text.equals(ManageApprovals.SUBMIT))
						label.setVisible(false);
					/** date 07/11/2017 added by komal to add revised quotation button **/
					if(text.equals("Revised Quotation"))
					label.setVisible(true);
					/** Date :11/11/2017 by Manisha**/
					
					if(text.equals(AppConstants.CANCEL)){
						label.setVisible(true);
					}
					
					/**end*/
				}
				else if(status.equals(SalesQuotation.REQUESTED))
				{
					if(text.contains(AppConstants.CREATESALESORDER))
						label.setVisible(false);
					if(text.contains(AppConstants.Unsucessful))
						label.setVisible(false);
					
					if(text.equals(updateQuotations))
						label.setVisible(true);
					if(text.equals(ManageApprovals.SUBMIT))
						label.setVisible(false);
					/** date 07/11/2017 added by komal to add revised quotation button **/
					if(text.equals("Revised Quotation"))
					label.setVisible(false);
					
					/** Date :11/11/2017 by Manisha**/
					
					if(text.equals(AppConstants.CANCEL)){
						label.setVisible(false);
					}
					
					/**end*/
				}

				else if(status.equals(SalesQuotation.SALESQUOTATIONUNSUCESSFUL)||status.equals(SalesQuotation.SALESQUOTATIONSUCESSFUL)
						||status.equals(SalesQuotation.REJECTED))
				{
					if((text.contains(AppConstants.NEW)))
						label.setVisible(true);
					else
						label.setVisible(false);
					
					if(text.equals(updateQuotations))
						label.setVisible(true);
					if(text.equals(ManageApprovals.SUBMIT))
						label.setVisible(false);
					/** date 07/11/2017 added by komal to add revised quotation button **/
					if(text.equals("Revised Quotation"))
					label.setVisible(false);
					/** Date :11/11/2017 by Manisha**/
					
					if(text.equals(AppConstants.CANCEL)){
						label.setVisible(true);
					}
					
					/**end*/
				}
				
				else if(status.equals(SalesQuotation.REJECTED))
				{
					if(text.contains(AppConstants.NEW))
						label.setVisible(true);
					else
						label.setVisible(false);
					
					if(text.equals(updateQuotations))
						label.setVisible(true);
					if(text.equals(ManageApprovals.SUBMIT))
						label.setVisible(false);
					/** Date :11/11/2017 by Manisha**/
					
					if(text.equals(AppConstants.CANCEL)){
						label.setVisible(true);
					}
					
					/**end*/
				}
				
				else if(status.equals(SalesQuotation.SALESQUOTATIONSUCESSFUL))
				{
					if(text.contains(AppConstants.NEW)){
						label.setVisible(true);
						if(text.equals(AppConstants.CREATESALESORDER)){
							label.setVisible(false);
						}
					}
					
					if(text.equals(updateQuotations))
						label.setVisible(true);
					if(text.equals(ManageApprovals.SUBMIT))
						label.setVisible(false);
					/** Date :11/11/2017 by Manisha**/
					
					if(text.equals(AppConstants.CANCEL)){
						label.setVisible(true);
					}
					
					/**end*/
					
				}
				
				/**
				 * @author Anil
				 * @since 31-12-2020
				 * if quotation is unsucessful then below option should not be visible
				 * raised by Rahul Tiwari
				 */
				if(status.equals(SalesQuotation.SALESQUOTATIONUNSUCESSFUL)){
					if(text.equals(ManageApprovals.APPROVALREQUEST))
						label.setVisible(false);
					if(text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
						label.setVisible(false);
					if(text.equals(ManageApprovals.SUBMIT))
						label.setVisible(false);
					if(text.equals(AppConstants.CREATESALESORDER))
						label.setVisible(false);
					if(text.equals(AppConstants.Unsucessful))
						label.setVisible(false);
					if(text.equals(updateQuotations))
						label.setVisible(false);
					if(text.equals("Revised Quotation"))
						label.setVisible(false);
					if(text.equals(AppConstants.CANCEL)){
						label.setVisible(false);
					}
				}
				
				
				//Ashwini Patil Date:20-09-2022
				if(isHideNavigationButtons()) {
					Console.log("in changeProcessLevel isHideNavigationButtons");
					if(text.equalsIgnoreCase(AppConstants.NEW)||text.equalsIgnoreCase(AppConstants.CREATESALESORDER)||text.equalsIgnoreCase("Revised Quotation"))
						label.setVisible(false);	
				}
			}

		}
		
		/**
		 * The method is responsible for changing Application state as per 
		 * status
		 */
		public void setMenuAsPerStatus()
		{
			
			this.setAppHeaderBarAsPerStatus();
			this.toggleProcessLevelMenu();
		}
		
		
		
		
		

		/**
		 * sets the id textbox with the passed count value.
		 *
		 * @param count the new count
		 */
		@Override
		public void setCount(int count)
		{
			tbQuotatinId.setValue(count+"");
		}


		/* (non-Javadoc)
		 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
		 */
		@Override
		public void onClick(ClickEvent event)
		{
			if(event.getSource().equals(addPaymentTerms)){
				reactOnAddPaymentTerms();
			}
			if(event.getSource().equals(addOtherCharges)){
				int size=chargesTable.getDataprovider().getList().size();
				if(this.doamtincltax.getValue()!=null){
					ProductOtherCharges prodCharges=new ProductOtherCharges();
					prodCharges.setAssessableAmount(this.getDoamtincltax().getValue());
					prodCharges.setFlagVal(false);
					prodCharges.setIndexCheck(size+1);
					this.chargesTable.getDataprovider().getList().add(prodCharges);
				}
			}
			if(event.getSource().equals(baddproducts)){
				
				boolean addressFlag=true;
				if(personInfoComposite.getId().getValue().equals("")){
					showDialogMessage("Please add customer information!");
				}
				
				/**
				 * Date 12 july 2017 added by vijay for GST and C form manage
				 */
				if(dbquotationDate.getValue()==null){
					showDialogMessage("Please add Quotation date first");
				}
				/**
				 * ends here
				 */
				
				if(acshippingcomposite.getState().getSelectedIndex()==0&&acshippingcomposite.getCountry().getSelectedIndex()==0&&acshippingcomposite.getCity().getSelectedIndex()==0)
				{
					addressFlag=false;
					showDialogMessage("Please select shipping address!");
				}
				
				if(olbbBranch.getSelectedIndex()==0){
					showDialogMessage("Please select branch!");
					return;
				}
				
				if(!prodInfoComposite.getProdCode().getValue().equals("")){
					/**
					 * Date 03-05-2018 By Vijay 
					 * for duplicate product allow to add
					 */
//					boolean productValidation=validateProductDuplicate(Integer.parseInt(prodInfoComposite.getProdID().getValue().trim()),prodInfoComposite.getProdCode().getValue());
//					if(productValidation==true){
//						showDialogMessage("Product already exists!");
//					}
//					else{
						if(validateState()&&addressFlag==true){
							this.ProdctType(this.prodInfoComposite.getProdCode().getValue().trim(),this.prodInfoComposite.getProdID().getValue().trim());
						}
						if(!validateState()&&validateCForm()&&addressFlag==true){
							this.ProdctType(this.prodInfoComposite.getProdCode().getValue().trim(),this.prodInfoComposite.getProdID().getValue().trim());
						}
//					}
				}
			}

			
			/**
			 * Date : 23-03-2018 By VIJAY for Other charges
			 */
				
			if(event.getSource().equals(addOthrChargesBtn)){
				Tax tax1=new Tax();
				Tax tax2=new Tax();
				OtherCharges prodCharges=new OtherCharges();
				prodCharges.setOtherChargeName("");
				prodCharges.setAmount(0);
				prodCharges.setTax1(tax1);
				prodCharges.setTax2(tax2);
				this.tblOtherCharges.getDataprovider().getList().add(prodCharges);
			}
			/**
			 * End
			 */
			
			if(event.getSource().equals(costingBtn)){
//				reactOnCosting();
				marginPopup.reactOnCosting(lineitemsalesquotationtable.getValue(),vendorMargins,otherChargesMargins);
			}
			
			if(event.getSource().equals(marginPopup.getLblOk())){
				
				vendorMargins=marginPopup.vendorMarginTbl.getValue();
				otherChargesMargins=marginPopup.otherChargesMarginTbl.getValue();
				totalVendorMargin=marginPopup.dbTotalVendorMargin.getValue();
				totalOcMargin=marginPopup.dbOtherChargesMargin.getValue();
				marginPopup.hidePopUp();
			}
			
			if(event.getSource().equals(marginPopup.getLblCancel())){
				marginPopup.hidePopUp();
			}
			
			
			if(event.getSource().equals(btnTermsAndConditionAdd)){
				reactonAddTermsAndCondition();
			}
			
			if(event.getSource().equals(chkPrintAllTermsAndCondition)){
				if(chkPrintAllTermsAndCondition.getValue()== false){
					this.btnTermsAndConditionAdd.setEnabled(true);
				}
				else{
					this.btnTermsAndConditionAdd.setEnabled(false);
					this.termsAndConditionTable.clear();
					this.olbtermsAndConditionTitle.setSelectedIndex(0);
				}
			}
			
		}
		
		

		

		

		/* (non-Javadoc)
		 * @see com.slicktechnologies.client.approvalutility.ApprovableScreen#getstatustextbox()
		 */
		@Override
		public TextBox getstatustextbox() {
			return this.tbQuotationStatus;
		}

		@Override
		public void clear() {
			super.clear();
			getstatustextbox().setText(SalesQuotation.CREATED);
			
			vendorMargins=null;
			otherChargesMargins=null;
			totalOcMargin=null;
			totalVendorMargin=null;
			
		}
		
		/**
		 * Sets the one to many combo.
		 */
		public void setOneToManyCombo()
		{
			 CategoryTypeFactory.initiateOneManyFunctionality(olbQuotationCategory,olbQuotationType);  
			  AppUtility.makeTypeListBoxLive(this.olbQuotationType, Screen.SALESQUOTATIONTYPE);
		} 
		 
		/******************************React On Add Payment Terms Method*******************************/
		/**
		 * This method is called when add button of payment terms is clicked.
		 * This method involves the inclusion of payment terms in the corresponding table
		 */
		
		public void reactOnAddPaymentTerms()
		{
			System.out.println("In method of payemtn terms");	
			int payTrmDays=0;
			double payTrmPercent=0;
			String payTrmComment="";
			boolean flag=true;
			boolean percFlag=true;
			boolean payTermsFlag=true;
			
			
			if(tbcomment.getValue()==null||tbcomment.getValue().equals(""))
			{
				this.showDialogMessage("Comment cannot be empty!");
			}
			
			boolean checkTermDays=this.validatePaymentTrmDays(ibdays.getValue());
			if(checkTermDays==false)
			{
				if(chk==0){
					showDialogMessage("Days already defined.Please enter unique days!");
				}
//				clearPaymentTermsFields();
				flag=false;
			}
			
			
			if(ibdays.getValue()!=null&&ibdays.getValue()>=0){
				payTrmDays=ibdays.getValue();
			}
			else{
				showDialogMessage("Days cannot be empty!");
				flag=false;
			}
			
			
			if(dopercent.getValue()!=null&&dopercent.getValue()>=0){
				if(dopercent.getValue()>100){
					showDialogMessage("Percent cannot be greater than 100");
//					dopercent.setValue(null);
					percFlag=false;
				}
				else if(dopercent.getValue()==0){
					showDialogMessage("Percent should be greater than 0!");
//					dopercent.setValue(null);
				}
				else{
				payTrmPercent=dopercent.getValue();
				}
			}
			else{
				showDialogMessage("Percent cannot be empty!");
			}
			
			if(tbcomment.getValue()!=null){
				payTrmComment=tbcomment.getValue();
			}
			
			if(paymentTermsTable.getDataprovider().getList().size()>11)
			{
				showDialogMessage("Total number of payment terms allowed are 12");
				payTermsFlag=false;
			}
			
			if(!payTrmComment.equals("")&&payTrmPercent!=0&&flag==true&&percFlag==true&&payTermsFlag==true)
			{
				PaymentTerms payTerms=new PaymentTerms();
				payTerms.setPayTermDays(payTrmDays);
				payTerms.setPayTermPercent(payTrmPercent);
				payTerms.setPayTermComment(payTrmComment);
				paymentTermsTable.getDataprovider().getList().add(payTerms);
				clearPaymentTermsFields();
			}
		}
		
		/*****************************************Add Product Taxes Method***********************************/
		
		/**
		 * This method is called when a product is added to SalesLineItem table.
		 * Taxes related to corresponding products will be added in ProductTaxesTable.
		 * @throws Exception
		 */
		public void addProdTaxes() throws Exception
		{
			System.out.println("IN TAXES");
			List<SalesLineItem> salesLineItemLis=this.lineitemsalesquotationtable.getDataprovider().getList();
			List<ProductOtherCharges> taxList=this.prodTaxTable.getDataprovider().getList();
			NumberFormat nf=NumberFormat.getFormat("0.00");
			String cformval="";
			int cformindex=this.getCbcformlis().getSelectedIndex();
			if(cformindex!=0){
				cformval=this.getCbcformlis().getItemText(cformindex);
			}
			for(int i=0;i<salesLineItemLis.size();i++)
			{
				double priceqty=0,taxPrice=0,origPrice=0;
//				if(salesLineItemLis.get(i).getPercentageDiscount()==null){
//					taxPrice=this.getLineitemsalesquotationtable().removeAllTaxes(salesLineItemLis.get(i).getPrduct());
//					priceqty=(salesLineItemLis.get(i).getPrice()-taxPrice)*salesLineItemLis.get(i).getQty();
//					priceqty=Double.parseDouble(nf.format(priceqty));
//				}
//				if(salesLineItemLis.get(i).getPercentageDiscount()!=null){
//					taxPrice=this.getLineitemsalesquotationtable().removeAllTaxes(salesLineItemLis.get(i).getPrduct());
//					priceqty=(salesLineItemLis.get(i).getPrice()-taxPrice);
//					priceqty=priceqty-(priceqty*salesLineItemLis.get(i).getPercentageDiscount()/100);
//					priceqty=priceqty*salesLineItemLis.get(i).getQty();
//					priceqty=Double.parseDouble(nf.format(priceqty));
//				}
				
				
				taxPrice=this.getLineitemsalesquotationtable().removeAllTaxes(salesLineItemLis.get(i).getPrduct());
				
				origPrice=(salesLineItemLis.get(i).getPrice()-taxPrice);
				if((salesLineItemLis.get(i).getPercentageDiscount()==null && salesLineItemLis.get(i).getPercentageDiscount()==0) && (salesLineItemLis.get(i).getDiscountAmt()==0) ){
						
						System.out.println("inside both 0 condition");
						/******************** Square Area Calculation code added in if condition and without square area calculation code in else block old code  added by vijay ***************************/
						if(!salesLineItemLis.get(i).getArea().equalsIgnoreCase("NA")){
							double squareArea = Double.parseDouble(salesLineItemLis.get(i).getArea());
							priceqty=origPrice*squareArea;
							priceqty=Double.parseDouble(nf.format(priceqty));
							System.out.println("RRRRRRRRRRRRRR price == "+priceqty);
						
						}else{
								priceqty=origPrice*salesLineItemLis.get(i).getQty();

						}
					}
					
					else if((salesLineItemLis.get(i).getPercentageDiscount()!=null)&& (salesLineItemLis.get(i).getDiscountAmt()!=0)){
						
						System.out.println("inside both not null condition");
						/******************** Square Area Calculation code added in if condition and without square area calculation code in else block old code added by vijay  ***************************/
						if(!salesLineItemLis.get(i).getArea().equalsIgnoreCase("NA")){
							double squareArea = Double.parseDouble(salesLineItemLis.get(i).getArea());
							priceqty=origPrice*squareArea;
							priceqty= priceqty-(priceqty*salesLineItemLis.get(i).getPercentageDiscount()/100);
							priceqty=priceqty-salesLineItemLis.get(i).getDiscountAmt();
							priceqty=Double.parseDouble(nf.format(priceqty));
						}else{
							priceqty=origPrice*salesLineItemLis.get(i).getQty();
							priceqty=priceqty-(priceqty*salesLineItemLis.get(i).getPercentageDiscount()/100);
							priceqty=priceqty-salesLineItemLis.get(i).getDiscountAmt();
						}
						
						
					}
					else 
					{
						System.out.println("inside oneof the null condition");
						
							if(salesLineItemLis.get(i).getPercentageDiscount()!=null){
								System.out.println("inside getPercentageDiscount oneof the null condition");
								/******************** Square Area Calculation code added in if condition and without square area calculation code in else block old code added by vijay ***************************/
								if(!salesLineItemLis.get(i).getArea().equalsIgnoreCase("NA")){
									Double squareArea = Double.parseDouble(salesLineItemLis.get(i).getArea());
									priceqty = origPrice*squareArea;
									priceqty=priceqty-(priceqty*salesLineItemLis.get(i).getPercentageDiscount()/100);
								}else{
									priceqty=origPrice*salesLineItemLis.get(i).getQty();
									priceqty=priceqty-(priceqty*salesLineItemLis.get(i).getPercentageDiscount()/100);
							
								}
							}
							else 
							{
								System.out.println("inside getDiscountAmt oneof the null condition");
								/******************** Square Area Calculation code added in if condition and without square area calculation code in else block old code added by vijay ***************************/
								if(!salesLineItemLis.get(i).getArea().equals("NA")){
									Double squareArea = Double.parseDouble(salesLineItemLis.get(i).getArea());
									priceqty = origPrice*squareArea;
									priceqty=priceqty-salesLineItemLis.get(i).getDiscountAmt();
								}else{
									priceqty=origPrice*salesLineItemLis.get(i).getQty();
									priceqty=priceqty-salesLineItemLis.get(i).getDiscountAmt();
								}
							}
							
//							total=total*object.getQty();
							
					}
				
				
				System.out.println("Total Prodcut"+priceqty);
				
				
				/**
				 * Date 20 july 2017 below code commented by vijay 
				 * for this below code by default showing vat and service tax percent 0 no nedd this if
				 * product master does not have any taxes
				 */
				
//				if(salesLineItemLis.get(i).getVatTax().getPercentage()==0&&cbcformlis.getSelectedIndex()==0){
//					ProductOtherCharges pocentity=new ProductOtherCharges();
//					
//					System.out.println("hii  1");
//					
//					if(validateState()){
//						pocentity.setChargeName("VAT");
//					}
//					if(!validateState()){
//						pocentity.setChargeName("CST");
//					}
//					
//					
//					pocentity.setChargePercent(salesLineItemLis.get(i).getVatTax().getPercentage());
//					pocentity.setIndexCheck(i+1);
//					int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getVatTax().getPercentage(),"VAT");
//					if(indexValue!=-1){
////						pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
//						pocentity.setAssessableAmount(0);
//						this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
//					}
//					
//					if(indexValue==-1){
//						pocentity.setAssessableAmount(0);
////						pocentity.setAssessableAmount(priceqty);
//					}
//					this.prodTaxTable.getDataprovider().getList().add(pocentity);
//				}
//				
//				if(salesLineItemLis.get(i).getServiceTax().getPercentage()==0){
////					if(validateState()){
//					System.out.println("hii  2");
//					ProductOtherCharges pocentity=new ProductOtherCharges();
//					pocentity.setChargeName("Service Tax");
//					pocentity.setChargePercent(salesLineItemLis.get(i).getServiceTax().getPercentage());
//					pocentity.setIndexCheck(i+1);
//					int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getServiceTax().getPercentage(),"Service");
//					if(indexValue!=-1){
////						pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
//						pocentity.setAssessableAmount(0);
//						this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
//					}
//					
//					if(indexValue==-1){
//						pocentity.setAssessableAmount(0);
////						pocentity.setAssessableAmount(priceqty);
//					}
//					this.prodTaxTable.getDataprovider().getList().add(pocentity);
////					}
//					
////					if(!validateState()&&getCbcformlis().getItemText(getCbcformlis().getSelectedIndex()).equals(AppConstants.NO)){
////						ProductOtherCharges pocentity=new ProductOtherCharges();
////						pocentity.setChargeName("Service Tax");
////						pocentity.setChargePercent(salesLineItemLis.get(i).getServiceTax().getPercentage());
////						pocentity.setIndexCheck(i+1);
////						pocentity.setAssessableAmount(priceqty);
////						this.prodTaxTable.getDataprovider().getList().add(pocentity);
////					}
//					
//					
//				}
			/**
			 * Adding Vat Tax to table if the company state and delivery state is equal
			 */
				
				
			if(validateState()){
				System.out.println("HIIII");
				if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
					
					/**
					 * Date 10 july 2017 below if condition added by vijay for GST old code added in else block
					 */
					if(salesLineItemLis.get(i).getVatTax().getTaxPrintName()!=null && !salesLineItemLis.get(i).getVatTax().getTaxPrintName().equals("")){
						
						System.out.println("GST VAT");
						ProductOtherCharges pocentity=new ProductOtherCharges();
						//Below old code commented by vijay
//						pocentity.setChargeName("VAT");
						// below code added by vijay for GST
						pocentity.setChargeName(salesLineItemLis.get(i).getVatTax().getTaxPrintName());
						pocentity.setChargePercent(salesLineItemLis.get(i).getVatTax().getPercentage());
						pocentity.setIndexCheck(i+1);
//						int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getVatTax().getPercentage(),"VAT");
						int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getVatTax().getPercentage(),salesLineItemLis.get(i).getVatTax().getTaxPrintName());

						if(indexValue!=-1){
							System.out.println("Index As Not -1");
							pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
							this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
						}
						if(indexValue==-1){
							System.out.println("VAt");
							pocentity.setAssessableAmount(priceqty);
						}
						this.prodTaxTable.getDataprovider().getList().add(pocentity);
						
					}else{
						
						System.out.println("Vattx");
						ProductOtherCharges pocentity=new ProductOtherCharges();
						pocentity.setChargeName("VAT");
						pocentity.setChargePercent(salesLineItemLis.get(i).getVatTax().getPercentage());
						pocentity.setIndexCheck(i+1);
						int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getVatTax().getPercentage(),"VAT");
						
						if(indexValue!=-1){
							System.out.println("Index As Not -1");
							pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
							this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
						}
						if(indexValue==-1){
							System.out.println("VAt");
							pocentity.setAssessableAmount(priceqty);
						}
						this.prodTaxTable.getDataprovider().getList().add(pocentity);
					}
					
					
				}
			}
				
			/**
			 * Adding Vat Tax as CST if company state and delivery state is not equal and C form is submitted.
			 */
			
			/**
			 * Date 11 july 2017 added by vijay for GST
			 * below code is applicable only if quotation date is less than 1 july 2017 for Cform
			 * below if condition added by vijay
			 */
			
			if(dbquotationDate.getValue()!=null && dbquotationDate.getValue().before(date)){

				if(this.getCbcformlis().getSelectedIndex()!=0&&AppConstants.YES.equals(cformval.trim())){
//					if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
					TaxDetails cstPerc=olbcstpercent.getSelectedItem();
						double csperc=cstPerc.getTaxChargePercent();
						System.out.println("Vattx");
						ProductOtherCharges pocentity=new ProductOtherCharges();
						pocentity.setChargeName("CST");
						pocentity.setChargePercent(csperc);
						pocentity.setIndexCheck(i+1);
						int indexValue=this.checkTaxPercent(csperc,"CST");
						
						if(indexValue!=-1){
							System.out.println("Index As Not -1");
							pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
							this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
						}
						if(indexValue==-1){
							pocentity.setAssessableAmount(priceqty);
						}
						this.prodTaxTable.getDataprovider().getList().add(pocentity);
//					}
				}
			}
				
				/**
				 * Adding Vat Tax as CST if company state and delivery state is not equal and C form is not submitted.
				 */
				
				/**
				 * Date 11 july 2017 added by vijay for GST
				 * below code is applicable only if quotation date is less than 1 july 2017 for Cform
				 * below if condition added by vijay
				 */
			if(dbquotationDate.getValue()!=null && dbquotationDate.getValue().before(date)){
				
				if(this.getCbcformlis().getSelectedIndex()!=0&&AppConstants.NO.equals(cformval.trim())){
					if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
						
						
						System.out.println("Vattx");
						ProductOtherCharges pocentity=new ProductOtherCharges();
						pocentity.setChargeName("CST");
						pocentity.setChargePercent(salesLineItemLis.get(i).getVatTax().getPercentage());
						pocentity.setIndexCheck(i+1);
						int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getVatTax().getPercentage(),"VAT");
						
						if(indexValue!=-1){
							System.out.println("vsctIndex As Not -1");
							pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
							this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
						}
						if(indexValue==-1){
							System.out.println("vcst-1");
							pocentity.setAssessableAmount(priceqty);
						}
						this.prodTaxTable.getDataprovider().getList().add(pocentity);
					}
				}
			}
				/**
				 * Adding service tax to the table
				 */
				
				if(salesLineItemLis.get(i).getServiceTax().getPercentage()!=0){
					System.out.println("hi vijay ");
					System.out.println("ServiceTx");
					ProductOtherCharges pocentity=new ProductOtherCharges();
					
					/**
					 * Date 11 july 2017 added by vijay for GST and old code added in else block 
					 */
					if(salesLineItemLis.get(i).getServiceTax().getTaxPrintName()!=null && !salesLineItemLis.get(i).getServiceTax().getTaxPrintName().equals(""))
					{
						pocentity.setChargeName(salesLineItemLis.get(i).getServiceTax().getTaxPrintName());
					}else{
						pocentity.setChargeName("Service Tax");
					}
					/**
					 * ends here
					 */
					
					
					pocentity.setChargePercent(salesLineItemLis.get(i).getServiceTax().getPercentage());
					pocentity.setIndexCheck(i+1);
					
					
					int indexValue;
					/**
					 * Date 12 july 2017 if condition for old code and else block for GST added by vijay
					 */
					if(salesLineItemLis.get(i).getServiceTax().getTaxPrintName()!=null && !salesLineItemLis.get(i).getServiceTax().getTaxPrintName().equals(""))
					{
						indexValue =this.checkTaxPercent(salesLineItemLis.get(i).getServiceTax().getPercentage(),salesLineItemLis.get(i).getServiceTax().getTaxPrintName());
					}else{
						indexValue =this.checkTaxPercent(salesLineItemLis.get(i).getServiceTax().getPercentage(),"Service");

					}
					/**
					 * ends here
					 */
					
					if(indexValue!=-1){
						System.out.println("Index As Not -1 == ");
						
						/**
						 * If validate state method is true
						 */
						
						if(validateState()){
							double assessValue=0;
							if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
								/**
								 * Date 11 july 2017 if condition added by vijay for GST and old code added in else block 
								 */
								if(salesLineItemLis.get(i).getServiceTax().getTaxPrintName()!=null && !salesLineItemLis.get(i).getServiceTax().getTaxPrintName().equals("")){
									System.out.println("For GST");
									assessValue=priceqty;
									pocentity.setAssessableAmount(assessValue+taxList.get(indexValue).getAssessableAmount());
									System.out.println("price =="+assessValue);
									System.out.println(assessValue+taxList.get(indexValue).getAssessableAmount());
								 }else{
									 System.out.println("old code ");
									assessValue=priceqty+(priceqty*salesLineItemLis.get(i).getVatTax().getPercentage()/100);
									pocentity.setAssessableAmount(assessValue+taxList.get(indexValue).getAssessableAmount());
								 }
								
								this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
							}
							if(salesLineItemLis.get(i).getVatTax().getPercentage()==0){
								pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
								this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
							}
						}
						
						/**
						 * If validate state method is true and cform is submittted.
						 */
						
						/**
						 * Date 11 july 2017 added by vijay for GST
						 * below code is applicable only if quotation date is less than 1 july 2017 for Cform
						 * below if condition added by vijay
						 */
						if(dbquotationDate.getValue()!=null && dbquotationDate.getValue().before(date)){
							
						if(!validateState()&&this.getCbcformlis().getItemText(getCbcformlis().getSelectedIndex()).equals(AppConstants.YES)){
							double assessValue=0;
							if(this.getOlbcstpercent().getSelectedIndex()!=0){
								TaxDetails cstTax=olbcstpercent.getSelectedItem();
								assessValue=priceqty+(priceqty*cstTax.getTaxChargePercent()/100);
								pocentity.setAssessableAmount(assessValue+taxList.get(indexValue).getAssessableAmount());
								this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
							}
						}
						
					  }
						
						/**
						 * If validate state method is true and cform is not submitted.
						 */
						
						/**
						 * Date 11 july 2017 added by vijay for GST
						 * below code is applicable only if quotation date is less than 1 july 2017 for Cform
						 * below if condition added by vijay
						 */
						if(dbquotationDate.getValue()!=null && dbquotationDate.getValue().before(date)){
						
						if(!validateState()&&this.getCbcformlis().getItemText(getCbcformlis().getSelectedIndex()).equals(AppConstants.NO)){
							double assessValue=0;
							assessValue=priceqty+(priceqty*salesLineItemLis.get(i).getVatTax().getPercentage()/100);
							pocentity.setAssessableAmount(assessValue+taxList.get(indexValue).getAssessableAmount());
							this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
						}
						
					   }
					}
					if(indexValue==-1){
						System.out.println("Index As -1");
						
						if(validateState()){
							double assessVal=0;
							
							
							if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
								
								/**
								 * Date 12 july 2017 if condition added by vijay for GST and old code added into else block
								 */
								if(salesLineItemLis.get(i).getServiceTax().getTaxPrintName()!=null && !salesLineItemLis.get(i).getServiceTax().getTaxPrintName().equals("")){
									System.out.println("For GST");
									assessVal=priceqty;
									pocentity.setAssessableAmount(assessVal);
									
								}
								else{
									System.out.println("Old code");
									assessVal=priceqty+(priceqty*salesLineItemLis.get(i).getVatTax().getPercentage()/100);
									pocentity.setAssessableAmount(assessVal);
									
								}
							}
							
							
							if(salesLineItemLis.get(i).getVatTax().getPercentage()==0){
								pocentity.setAssessableAmount(priceqty);
							}
						}
						
						/**
						 * Date 11 july 2017 added by vijay for GST
						 * below code is applicable only if quotation date is less than 1 july 2017 for Cform
						 * below if condition added by vijay
						 */
						if(dbquotationDate.getValue()!=null && dbquotationDate.getValue().before(date)){
						if(!validateState()&&this.getCbcformlis().getItemText(getCbcformlis().getSelectedIndex()).equals(AppConstants.NO)){
							double assessVal=0;
							if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
								assessVal=priceqty+(priceqty*salesLineItemLis.get(i).getVatTax().getPercentage()/100);
								pocentity.setAssessableAmount(assessVal);
							}
							if(salesLineItemLis.get(i).getVatTax().getPercentage()==0){
								pocentity.setAssessableAmount(priceqty);
							}
						}
						
						if(!validateState()&&this.getCbcformlis().getItemText(getCbcformlis().getSelectedIndex()).equals(AppConstants.YES)){
							double assessVal=0;
							if(this.getOlbcstpercent().getSelectedIndex()!=0){
								TaxDetails cstPercentage=olbcstpercent.getSelectedItem();
								assessVal=priceqty+(priceqty*cstPercentage.getTaxChargePercent()/100);
								pocentity.setAssessableAmount(assessVal);
							}
						}
						
						}
						/**
						 * ends here
						 */
						
					}
					this.prodTaxTable.getDataprovider().getList().add(pocentity);
				}
			}
			}

		
		
		
		/*********************************Check Tax Percent*****************************************/
		/**
		 * This method returns 
		 * @param taxValue
		 * @param taxName
		 * @return
		 */
		
		public int checkTaxPercent(double taxValue,String taxName)
		{
			System.out.println("Tav Value"+taxValue);
			List<ProductOtherCharges> taxesList=this.prodTaxTable.getDataprovider().getList();
			
			for(int i=0;i<taxesList.size();i++)
			{
				double listval=taxesList.get(i).getChargePercent();
				int taxval=(int)listval;
				
				if(taxName.equals("Service")){
					if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("Service Tax")){
						return i;
					}
					if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("Service Tax")){
						System.out.println("zero");
						return i;
					}
				}
				if(taxName.equals("VAT")){
					if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("VAT")){
						return i;
					}
					if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("VAT")){
						System.out.println("zero");
						return i;
					}
				}
				if(taxName.equals("CST")){
					if(taxValue==taxesList.get(i).getChargePercent()&&taxesList.get(i).getChargeName().trim().equals("CST")){
						return i;
					}
				}
				
				//  vijay added this for GST on 12 july 2017
				if(taxName.equals("SGST")){
					if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("SGST")){
						return i;
					}
					if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("SGST")){
						return i;
					}
				}
				
				//  vijay added this for GST on 12 july 2017
				if(taxName.equals("CGST")){
					if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("CGST")){
						return i;
					}
					if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("CGST")){
						return i;
					}
				}
				
				//  vijay added this for GST on 12 july 2017
				if(taxName.equals("IGST")){
					if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("IGST")){
						return i;
					}
					if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("IGST")){
						return i;
					}
				}
			}
			return -1;
		}
		
		public boolean checkProducts(String pcode)
		{
			List<SalesLineItem> salesitemlis=lineitemsalesquotationtable.getDataprovider().getList();
			for(int i=0;i<salesitemlis.size();i++)
			{
				if(salesitemlis.get(i).getProductCode().equals(pcode)||salesitemlis.get(i).getProductCode()==pcode){
					return true;
				}
			}
			return false;
		}
		
		
		
		/*************************Update Charges Table****************************************/
		/**
		 * This method updates the charges table.
		 * The table is updated when any row value is changed through field updater in table i.e
		 * row change event is fired
		 */
		
		public void updateChargesTable()
		{
			List<ProductOtherCharges> prodOtrLis=this.chargesTable.getDataprovider().getList();
			ArrayList<ProductOtherCharges> prodOtrArr=new ArrayList<ProductOtherCharges>();
			prodOtrArr.addAll(prodOtrLis);
			
			this.chargesTable.connectToLocal();
			for(int i=0;i<prodOtrArr.size();i++)
			{
				ProductOtherCharges prodChargesEnt=new ProductOtherCharges();
				prodChargesEnt.setChargeName(prodOtrArr.get(i).getChargeName());
				prodChargesEnt.setChargePercent(prodOtrArr.get(i).getChargePercent());
				prodChargesEnt.setChargeAbsValue(prodOtrArr.get(i).getChargeAbsValue());
				//prodChargesEnt.setAssessableAmount(prodOtrArr.get(i).getAssessableAmount());
				prodChargesEnt.setFlagVal(prodOtrArr.get(i).getFlagVal());
				prodChargesEnt.setIndexCheck(prodOtrArr.get(i).getIndexCheck());
				if(prodOtrArr.get(i).getFlagVal()==false){
					prodChargesEnt.setAssessableAmount(this.getDoamtincltax().getValue());
				}
				if(prodOtrArr.get(i).getFlagVal()==true){
					double assesableVal=this.retrieveSurchargeAssessable(prodOtrArr.get(i).getIndexCheck());
					prodChargesEnt.setAssessableAmount(assesableVal);
				}
				this.chargesTable.getDataprovider().getList().add(prodChargesEnt);
			}
		}
		
		public double retrieveSurchargeAssessable(int indexValue)
		{
			List<ProductOtherCharges> otrChrgLis=this.chargesTable.getDataprovider().getList();
			ArrayList<ProductOtherCharges> otrChrgArr=new ArrayList<ProductOtherCharges>();
			otrChrgArr.addAll(otrChrgLis);
		
			double getAssessVal=0;
			for(int i=0;i<otrChrgArr.size();i++)
			{
				if(otrChrgArr.get(i).getFlagVal()==false&&otrChrgArr.get(i).getIndexCheck()==indexValue)
				{
					if(otrChrgArr.get(i).getChargePercent()!=0){
						getAssessVal=otrChrgArr.get(i).getAssessableAmount()*otrChrgArr.get(i).getChargePercent()/100;
					}
					if(otrChrgArr.get(i).getChargeAbsValue()!=0){
						getAssessVal=otrChrgArr.get(i).getChargeAbsValue();
					}
				}
			}
			return getAssessVal;
		}

		@Override
		public void setEnable(boolean state) {
			super.setEnable(state);
			this.tbContractId.setEnabled(false);
			this.tbQuotationStatus.setEnabled(false);
			this.tbLeadId.setEnabled(false);
			this.tbQuotatinId.setEnabled(false);
			this.lineitemsalesquotationtable.setEnable(state);
			this.paymentTermsTable.setEnable(state);
			this.chargesTable.setEnable(state);
			this.prodTaxTable.setEnable(state);
			this.acshippingcomposite.setEnable(false);
			this.dototalamt.setEnabled(false);
			this.doamtincltax.setEnabled(false);
			this.donetpayamt.setEnabled(false);
			this.olbcstpercent.setEnabled(false);
			this.cbcformlis.setEnabled(false);
			this.btnsecurityDep.setVisible(false);
			this.tbremark.setEnabled(false);
			
			/**
			 * Date : 23-03-2018 BY VIJAY
			 */
			this.tblOtherCharges.setEnable(state);
			dbOtherChargesTotal.setEnabled(false);
			/**
			 * end
			 */
			
			marginPopup.getLblOk().setVisible(state);
			marginPopup.vendorMarginTbl.setEnable(state);
			marginPopup.otherChargesMarginTbl.setEnable(state);
			/**
			 * Date : 25-12-2020 BY Priyanka issue raise by Rahul.
			 */
			tbDocStatusreason.setEnabled(false);
			
			termsAndConditionTable.setEnable(state);
			objPaymentMode.setEnabled(state);

		}

		@Override
		public void setToViewState() {
			super.setToViewState();
			setMenuAsPerStatus();
			
			if(marginFlag){
				costingBtn.setEnabled(true);
			}

			/**
			 * @author Anil @since 27-04-2021
			 */
			String mainScreenLabel="Quotation Information";
//			String mainScreenLabel="QUATATION";
			if(quotationObject!=null&&quotationObject.getCount()!=0){
				mainScreenLabel=quotationObject.getCount()+" "+"/"+" "+quotationObject.getStatus()+" "+"/"+" "+AppUtility.parseDate(quotationObject.getCreationDate());
			}
			 fgroupingCustomerInformation.getHeaderLabel().setText(mainScreenLabel);


			if(olbcustBranch.getSelectedIndex()!=0){
				customerBranch = olbcustBranch.getValue();
			}

		}
		
		/**********************************Change Widgets Method******************************************/
		/**
		 * This method represents changing the widgets style. Used for modifying payment composite
		 * widget fields
		 */
		
		private void changeWidgets()
		{
			//  For changing the text alignments.
			this.changeTextAlignment(dototalamt);
			this.changeTextAlignment(donetpayamt);
			this.changeTextAlignment(doamtincltax);
			
			//  For changing widget widths.
			this.changeWidth(dototalamt, 200, Unit.PX);
			this.changeWidth(donetpayamt, 200, Unit.PX);
			this.changeWidth(doamtincltax, 200, Unit.PX);
			
			/**
			 * Date : 23-03-2018 By VIJAY
			 */
			this.changeTextAlignment(dbOtherChargesTotal);
			this.changeWidth(dbOtherChargesTotal, 200, Unit.PX);
			
			/** Date 08-06-2018 By vijay **/
			this.changeTextAlignment(dbfinalTotalAmt);
			this.changeTextAlignment(tbroundoffAmt);
			this.changeWidth(dbfinalTotalAmt,200,Unit.PX);
			this.changeWidth(tbroundoffAmt, 200, Unit.PX);
		}
		
		/*******************************Text Alignment Method******************************************/
		/**
		 * Method used for align of payment composite fields
		 * @param widg
		 */
		
		private void changeTextAlignment(Widget widg)
		{
			widg.getElement().getStyle().setTextAlign(TextAlign.CENTER);
		}
		
		/*****************************Change Width Method**************************************/
		/**
		 * Method used for changing width of fields
		 * @param widg
		 * @param size
		 * @param unit
		 */
		
		private void changeWidth(Widget widg,double size,Unit unit)
		{
			widg.getElement().getStyle().setWidth(size, unit);
		}

		/**
		 *  Overridden Validate Method
		 */

		@Override
		 public boolean validate()
		 {
			boolean superValidate = super.validate();
		     if(superValidate==false)
		        {
		        	 return false;
		        }
		   
		        if(lineitemsalesquotationtable.getDataprovider().getList().size()==0)
		        {
		            showDialogMessage("Please Select at least one Product!");
		            return false;
		        }
		        
		        boolean valPayPercent=this.validatePaymentTermPercent();
		        if(valPayPercent==false)
		        {
		        	showDialogMessage("Payment Term Not 100%, Please Correct!");
		        	return false;
		        }
		        
		        /**
				 * Date 11 july 2017 added by vijay for GST
				 * below code is applicable only if quotation date is less than 1 july 2017 for Cform
				 * below if condition added by vijay
				 */
				if(dbquotationDate.getValue()!=null && dbquotationDate.getValue().before(date)){
					
		        if(this.getAcshippingcomposite().getState().getSelectedIndex()!=0){
		        	String companyStateVal=SalesQuotationPresenter.companyState.trim();
		        	System.out.println("comp"+companyStateVal);
		        	Console.log(companyStateVal);
		        	if(!getAcshippingcomposite().getState().getValue().trim().equals(companyStateVal)&&getCbcformlis().getSelectedIndex()==0){
		        		showDialogMessage("C Form is Mandatory!");
		        		return false;
		        	}
		        }
		        
				
		        if(getCbcformlis().getItemText(getCbcformlis().getSelectedIndex()).trim().equals(AppConstants.YES)&&getOlbcstpercent().getSelectedIndex()==0){
		        	showDialogMessage("Please select CST Percent!");
		        	return false;
		        }
				}
		        if(validateSalesQuotationPrice()>0){
		        	showDialogMessage("Please enter product price greater than 0!");
		        	return false;
		        }
		        
				
				
		        if(!validateSalesQuotProdQty()){
					showDialogMessage("Please enter appropriate quantity required!");
					return false;
				}
		        
		        
//		        List<ProductOtherCharges> prodOtrChrgs=chargesTable.getDataprovider().getList();
//		        
//		        if(prodOtrChrgs.size()>0)
//		        {
//		        	int cntr=0;
//		        	for(int i=0;i<prodOtrChrgs.size();i++)
//		        	{
//		        		if(!prodOtrChrgs.get(i).getChargeName().trim().equals(null)){
//		        		}
//		        		else{
//		        			cntr=cntr+1;
//		        		}
//		        	}
//		        	
//		        	if(cntr==0){
//		        		return true;
//		        	}
//		        	else{
//		        		showDialogMessage("Charge Name Invalid. Please Correct!");
//		        		return false;
//		        	}
//		        }
		        
		        /**
				 * Date 03-07-2017 added by vijay tax validation
				 */
				List<ProductOtherCharges> prodtaxlist = this.prodTaxTable.getDataprovider().getList();
				for(int i=0;i<prodtaxlist.size();i++){
					
					if(dbquotationDate.getValue()!=null && dbquotationDate.getValue().before(date)){
						if(prodtaxlist.get(i).getChargeName().equalsIgnoreCase("CGST") || prodtaxlist.get(i).getChargeName().equalsIgnoreCase("SGST")
								|| prodtaxlist.get(i).getChargeName().equalsIgnoreCase("IGST")){
							showDialogMessage("As Quotation date "+fmt.format(dbquotationDate.getValue())+" GST Tax is not applicable");
							return false;
						}
					}
					/**
					 * @author Anil @since 21-07-2021
					 * Issue raised by Rahul for Innovative Pest(Thailand) 
					 * Vat is applicable there so system should not through validation for VAT
					 */
//					else if(dbquotationDate.getValue()!=null && dbquotationDate.getValue().equals(date) || dbquotationDate.getValue().after(date)){
//						if(prodtaxlist.get(i).getChargeName().equalsIgnoreCase("CST") || prodtaxlist.get(i).getChargeName().equalsIgnoreCase("Service Tax")
//								|| prodtaxlist.get(i).getChargeName().equalsIgnoreCase("VAT")){
//							showDialogMessage("As Quotation date "+fmt.format(dbquotationDate.getValue())+" VAT/CST/Service Tax is not applicable");
//							return false;
//						}
//					}
				}
				/**
				 * ends here
				 */
		        
		        return true;
		    }
		
		/********************************Clear Payment Terms Fields*******************************************/
		
		
		public boolean validateSalesQuotProdQty()
		{
			List<SalesLineItem> salesquotlis=lineitemsalesquotationtable.getDataprovider().getList();
			int ctr=0;
			for(int i=0;i<salesquotlis.size();i++)
			{
				if(salesquotlis.get(i).getQty()==0)
				{
					ctr=ctr+1;
				}
			}
			
			if(ctr==0){
				return true;
			}
			else{
				return false;
			}
		}
		
		
		
		
		public void clearPaymentTermsFields()
		{
			ibdays.setValue(null);
			dopercent.setValue(null);
			tbcomment.setValue("");
		}
		
		
		
		/*******************************Validate Payment Term Percent*****************************************/
		/**
		 * This method validates payment term percent. i.e the total of percent should be equal to 100
		 * @return
		 */
		
		public boolean validatePaymentTermPercent()
		{
			List<PaymentTerms> payTermsLis=this.paymentTermsTable.getDataprovider().getList();
			double totalPayPercent=0;
			for(int i=0;i<payTermsLis.size();i++)
			{
				totalPayPercent=totalPayPercent+payTermsLis.get(i).getPayTermPercent();
			}
			System.out.println("Validation of Payment Percent as 100"+totalPayPercent);
			
			if(totalPayPercent!=100)
			{
				return false;
			}
			
			return true;
		}
		
		/********************************Validate Payment Terms Days******************************************/
		/**
		 * This method represents the validation of payment terms days
		 * @param retrDays
		 * @return
		 */
		int chk=0;
		public boolean validatePaymentTrmDays(int retrDays)
		{
			List<PaymentTerms> payTermsLis=this.paymentTermsTable.getDataprovider().getList();
			for(int i=0;i<payTermsLis.size();i++)
			{
				if(retrDays==payTermsLis.get(i).getPayTermDays())
				{
					return false;
				}
				if(retrDays<payTermsLis.get(i).getPayTermDays())
				{
					showDialogMessage("Days should be defined in increasing order!");
					chk=1;
					return false;
				}
				else{
					chk=0;
				}
				if(retrDays>10000){
					showDialogMessage("Days cannot be greater than 4 digits!");
					return false;
				}
			}
			return true;
		}
		
		/**
		 * Validate For Duplicate Products
		 */
		
		public boolean validateProductDuplicate(int prodId,String pcode)
		{
			List<SalesLineItem> salesItemLis=this.getLineitemsalesquotationtable().getDataprovider().getList();
			for(int i=0;i<salesItemLis.size();i++)
			{
				if(salesItemLis.get(i).getProductCode().trim().equals(pcode.trim())&&salesItemLis.get(i).getPrduct().getCount()==prodId){
					return true;
				}
			}
			return false;
		}

		public int validateSalesQuotationPrice()
		{
			List<SalesLineItem> salesItemLis=this.getLineitemsalesquotationtable().getDataprovider().getList();
			int ctr=0;
			for(int i=0;i<salesItemLis.size();i++)
			{
				if(salesItemLis.get(i).getPrice()==0){
					ctr=ctr+1;
				}
			}
			
			return ctr;
		}
		
		
		
		public boolean validateCForm()
		{
			if(!validateState()&&this.getCbcformlis().getSelectedIndex()==0){
				showDialogMessage("Please select C Form");
				return false;
			}
			if(!validateState()&&this.getCbcformlis().getItemText(getCbcformlis().getSelectedIndex()).equals(AppConstants.YES)){
				if(this.getOlbcstpercent().getSelectedIndex()==0){
					showDialogMessage("Please select CST Percent");
					return false;
				}
				else{
					return true;
				}
			}
			return true;
		}
		
		public boolean validateState()
		{
			System.out.println("Presen"+SalesQuotationPresenter.companyState);
			
			/** Date 11 july 2017 added by vijay for GST
			 * below if condition code is applicable only if quotation date is less than 1 july 2017 for Cform
			 * below if condition added by vijay and else for no need this state validation for 1 july 2017 after quotation
			 */
			
			if(dbquotationDate.getValue()!=null && dbquotationDate.getValue().before(date)){
				
				if(this.getAcshippingcomposite().getState().getValue().trim().equals(SalesQuotationPresenter.companyState.trim())){
					return true;
				}
				return false;
			}else{
				return true;	
			}
			
		}

		@Override
		public void setToEditState() {
			super.setToEditState();
			this.processLevelBar.setVisibleFalse(false);
			if(olbcstpercent.getValue()!=null){
				olbcstpercent.setEnabled(true);
			}
			if(cbcformlis.getSelectedIndex()!=0){
				cbcformlis.setEnabled(true);
			}
			changeStatusToCreated();
			
			String mainScreenLabel="QUATATION";
			if(quotationObject!=null&&quotationObject.getCount()!=0){
				mainScreenLabel=quotationObject.getCount()+" "+"/"+" "+quotationObject.getStatus()+" "+"/"+" "+AppUtility.parseDate(quotationObject.getCreationDate());
			}
			fgroupingCustomerInformation.getHeaderLabel().setText(mainScreenLabel);
			
			customerBillingAddress = null;
			
			if(chkPrintAllTermsAndCondition.getValue()){
				btnTermsAndConditionAdd.setEnabled(false);
			}
			else{
				btnTermsAndConditionAdd.setEnabled(true);
			}
		}
		
		public void changeStatusToCreated()
		{
			SalesQuotation entity=(SalesQuotation) presenter.getModel();
			String status=entity.getStatus();
			
			if(SalesQuotation.REJECTED.equals(status.trim()))
			{
				this.tbQuotationStatus.setValue(SalesQuotation.CREATED);
			}
		}
		
		public void ProdctType(String pcode,String productId)
		{
			final GenricServiceAsync genasync=GWT.create(GenricService.class);
			final MyQuerry querry=new MyQuerry();
			
			int prodId=Integer.parseInt(productId);
			
			Vector<Filter> filtervec=new Vector<Filter>();
			
			Filter filter=new Filter();
			filter.setQuerryString("productCode");
			filter.setStringValue(pcode.trim());
			filtervec.add(filter);
			
			filter=new Filter();
			filter.setQuerryString("count");
			filter.setIntValue(prodId);
			filtervec.add(filter);
			
			filter=new Filter();
			filter.setQuerryString("status");
			filter.setBooleanvalue(true);
			filtervec.add(filter);
			
			querry.setFilters(filtervec);
			querry.setQuerryObject(new SuperProduct());
			showWaitSymbol();
			 Timer timer=new Timer() 
        	 {
 				@Override
 				public void run() {
				genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
					
					@Override
					public void onFailure(Throwable caught) {
						hideWaitSymbol();
						showDialogMessage("An Unexpected error occurred!");
					}
		
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						
						if(result.size()==0)
						{
							showDialogMessage("Please check whether product status is active or not!");
						}
						
						Branch branchEntity = olbbBranch.getSelectedItem();
						
						for(SuperModel model:result)
						{
							SuperProduct superProdEntity = (SuperProduct)model;
//							SalesLineItem lis=AppUtility.ReactOnAddProductCompositeForSales(superProdEntity);
							SalesLineItem lis=AppUtility.ReactOnAddProductCompositeForSales(superProdEntity,cust,branchEntity,true);
							/** Date 03-05-2018 By vijay for Product SrNo and allow to add same product **/
							lis.setProductSrNo(productSrNo+1);
						    getLineitemsalesquotationtable().getDataprovider().getList().add(lis);
						    /** Date 03-05-2018 By vijay for Product SrNo **/
						    productSrNo++;
						}
						
						
						}
					 });
					hideWaitSymbol();
	 				}
		 	  };
	          timer.schedule(3000); 
		}
		
		public void initializePercentCst()
		{
			MyQuerry querry=new MyQuerry();
			Company comEntity=new Company();
			Vector<Filter> filter=new Vector<Filter>();
			Filter temp=null;
			
			temp=new Filter();
			temp.setQuerryString("companyId");
			temp.setLongValue(comEntity.getCompanyId());
			filter.add(temp);
			
			temp=new Filter();
			temp.setQuerryString("isCentralTax");
			temp.setBooleanvalue(true);
			filter.add(temp);
			
			temp=new Filter();
			temp.setQuerryString("taxChargeStatus");
			temp.setBooleanvalue(true);
			filter.add(temp);
			
			querry.setFilters(filter);
			querry.setQuerryObject(new TaxDetails());
			olbcstpercent.MakeLive(querry);
		}
		
		protected void validateSecurityDeposit()
		{
			List<Config> configGlobal=LoginPresenter.globalConfig;
			
			for(int f=0;f<configGlobal.size();f++)
			{
				if(configGlobal.get(f).getType()==73&&configGlobal.get(f).getName().trim().equals("Security Deposit"))
				{
					if(configGlobal.get(f).isStatus()==true)
					{
						securityDepositButton();
					}
				}
			}
			
			}
		
		protected void securityDepositButton()
		{
			List<ProcessConfiguration> processLis=LoginPresenter.globalProcessConfig;
			List<ProcessTypeDetails> processDetailsLis=new ArrayList<ProcessTypeDetails>();
			for(int i=0;i<processLis.size();i++)
			{
				if(processLis.get(i).getProcessName().equals("SalesQuotation"))
				{
					processDetailsLis=processLis.get(i).getProcessList();
				}
			}
			
			for(int d=0;d<processDetailsLis.size();d++)
			{
				if(processDetailsLis.get(d).getProcessType().trim().equalsIgnoreCase("Security Deposit")&&processDetailsLis.get(d).isStatus()==true)
				{
					System.out.println("Status"+processDetailsLis.get(d).isStatus());
					btnsecurityDep.setVisible(true);
				}
			}

		}

		
		protected void checkCustomerStatus(int cCount)
		{
			MyQuerry querry=new MyQuerry();
			Company c=new Company();
			Vector<Filter> filtervec=new Vector<Filter>();
			Filter temp=null;
			
			temp=new Filter();
			temp.setQuerryString("companyId");
			temp.setLongValue(c.getCompanyId());
			filtervec.add(temp);
			
			temp=new Filter();
			temp.setQuerryString("count");
			temp.setIntValue(cCount);
			filtervec.add(temp);
			
			querry.setFilters(filtervec);
			querry.setQuerryObject(new Customer());
			
			genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onFailure(Throwable caught) {
					showDialogMessage("An Unexpected error occurred!");
				}

				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
						for(SuperModel model:result)
						{
							Customer custEntity = (Customer)model;
							
							
							if(custEntity.getNewCustomerFlag()==true){
								showDialogMessage("Please fill customer information by editing customer");
								personInfoComposite.clear();
							}
							else if(AppConstants.ACTIVE.equals(custEntity.getStatus().trim())&&(custEntity.getNewCustomerFlag()==false)){
								 getPersonInfoComposite().getId().getElement().addClassName("personactive");
								 getPersonInfoComposite().getName().getElement().addClassName("personactive");
								 getPersonInfoComposite().getPhone().getElement().addClassName("personactive");
								 getPersonInfoComposite().getTbpocName().getElement().addClassName("personactive");
							}
							
							cust = custEntity;
						}
						
						if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT || AppMemory.getAppMemory().currentState==ScreeenState.NEW){
							updateTaxesDetails(cust);
						}
					}
				 });
				}
		
		/**************************************Type Drop Down Logic****************************************/
		
		@Override
		public void onChange(ChangeEvent event) {
			
			if(event.getSource().equals(olbQuotationCategory)){
				
				System.out.println("Before");
				if(olbQuotationCategory.getSelectedIndex()!=0){
					
					System.out.println("After");
					ConfigCategory cat=olbQuotationCategory.getSelectedItem();
					if(cat!=null){
						AppUtility.makeLiveTypeDropDown(olbQuotationType, cat.getCategoryName(), cat.getCategoryCode(), cat.getInternalType());
					}
				}
			}
			
			
			/**
			 * Date 08/06/2018
			 * Developer :- Vijay
			 * Des :- for round off amount chnage calculation and change net payable accourdingly
			 */
			if(event.getSource().equals(tbroundoffAmt)){

				NumberFormat nf = NumberFormat.getFormat("0.00");
				double netPay=this.getDbfinalTotalAmt().getValue();
				this.getDbfinalTotalAmt().setValue(netPay);

				if(tbroundoffAmt.getValue().equals("") || tbroundoffAmt.getValue()==null){
					this.getDonetpayamt().setValue(netPay);
				}else{
					if(!tbroundoffAmt.getValue().equals("")&& tbroundoffAmt.getValue()!=null){
						String roundOff = tbroundoffAmt.getValue();
						double roundoffAmt =  AppUtility.calculateRoundOff(roundOff, netPay);
						if(roundoffAmt!=0){
							this.getDonetpayamt().setValue(roundoffAmt);
						}else{
							this.getDonetpayamt().setValue(netPay);
							this.getTbroundoffAmt().setValue("");
						}
					}
				}
			}
			/**
			 * ends here
			 */
			
			
			if(event.getSource().equals(olbbBranch)){
				if(olbbBranch.getSelectedIndex()!=0)
				updateTaxesDetails(cust);
			}
		}

		
/*******************************************Getters And Setters********************************************/
		
		public DateBox getDbValidUntill() {
			return dbValidUntill;
		}

		public void setDbValidUntill(DateBox dbValidUntill) {
			this.dbValidUntill = dbValidUntill;
		}

		public ObjectListBox<Branch> getOlbbBranch() {
			return olbbBranch;
		}

		public void setOlbbBranch(ObjectListBox<Branch> olbbBranch) {
			this.olbbBranch = olbbBranch;
		}

		public ObjectListBox<Config> getOlbcPriority() {
			return olbcPriority;
		}

		public void setOlbcPriority(ObjectListBox<Config> olbcPriority) {
			this.olbcPriority = olbcPriority;
		}

		public ObjectListBox<Config> getOlbcPaymentMethods() {
			return olbcPaymentMethods;
		}

		public void setOlbcPaymentMethods(ObjectListBox<Config> olbcPaymentMethods) {
			this.olbcPaymentMethods = olbcPaymentMethods;
		}

		public TextBox getTbRefeRRedBy() {
			return tbRefeRRedBy;
		}

		public void setTbRefeRRedBy(TextBox tbRefeRRedBy) {
			this.tbRefeRRedBy = tbRefeRRedBy;
		}

		public TextBox getTbQuotationStatus() {
			return tbQuotationStatus;
		}

		public void setTbQuotationStatus(TextBox tbQuotationStatus) {
			this.tbQuotationStatus = tbQuotationStatus;
		}

		public TextBox getTbLeadId() {
			return tbLeadId;
		}

		public void setTbLeadId(TextBox tbLeadId) {
			this.tbLeadId = tbLeadId;
		}

		public TextBox getTbContractId() {
			return tbContractId;
		}

		public void setTbContractId(TextBox tbContractId) {
			this.tbContractId = tbContractId;
		}

		public TextBox getTbQuotatinId() {
			return tbQuotatinId;
		}

		public void setTbQuotatinId(TextBox tbQuotatinId) {
			this.tbQuotatinId = tbQuotatinId;
		}

		public TextArea getTaDescription() {
			return taDescription;
		}

		public void setTaDescription(TextArea taDescription) {
			this.taDescription = taDescription;
		}

		public UploadComposite getUcUploadTAndCs() {
			return ucUploadTAndCs;
		}

		public void setUcUploadTAndCs(UploadComposite ucUploadTAndCs) {
			this.ucUploadTAndCs = ucUploadTAndCs;
		}

		public PersonInfoComposite getPersonInfoComposite() {
			return personInfoComposite;
		}

		public void setPersonInfoComposite(PersonInfoComposite personInfoComposite) {
			this.personInfoComposite = personInfoComposite;
		}

		public ObjectListBox<Employee> getOlbeSalesPerson() {
			return olbeSalesPerson;
		}

		public void setOlbeSalesPerson(ObjectListBox<Employee> olbeSalesPerson) {
			this.olbeSalesPerson = olbeSalesPerson;
		}

		public ObjectListBox<Employee> getOlbApproverName() {
			return olbApproverName;
		}

		public void setOlbApproverName(ObjectListBox<Employee> olbApproverName) {
			this.olbApproverName = olbApproverName;
		}

		public ObjectListBox<Config> getOlbQuotationGroup() {
			return olbQuotationGroup;
		}

		public void setOlbQuotationGroup(ObjectListBox<Config> olbQuotationGroup) {
			this.olbQuotationGroup = olbQuotationGroup;
		}

		public ObjectListBox<Type> getOlbQuotationType() {
			return olbQuotationType;
		}

		public void setOlbQuotationType(ObjectListBox<Type> olbQuotationType) {
			this.olbQuotationType = olbQuotationType;
		}

		public ObjectListBox<ConfigCategory> getOlbQuotationCategory() {
			return olbQuotationCategory;
		}

		public void setOlbQuotationCategory(
				ObjectListBox<ConfigCategory> olbQuotationCategory) {
			this.olbQuotationCategory = olbQuotationCategory;
		}

		public DoubleBox getDototalamt() {
			return dototalamt;
		}

		public void setDototalamt(DoubleBox dototalamt) {
			this.dototalamt = dototalamt;
		}

		public DoubleBox getDoamtincltax() {
			return doamtincltax;
		}

		public void setDoamtincltax(DoubleBox doamtincltax) {
			this.doamtincltax = doamtincltax;
		}

		public DoubleBox getDonetpayamt() {
			return donetpayamt;
		}

		public void setDonetpayamt(DoubleBox donetpayamt) {
			this.donetpayamt = donetpayamt;
		}

		public IntegerBox getIbdays() {
			return ibdays;
		}

		public void setIbdays(IntegerBox ibdays) {
			this.ibdays = ibdays;
		}

		public DoubleBox getDopercent() {
			return dopercent;
		}

		public void setDopercent(DoubleBox dopercent) {
			this.dopercent = dopercent;
		}

		public TextBox getTbcomment() {
			return tbcomment;
		}

		public void setTbcomment(TextBox tbcomment) {
			this.tbcomment = tbcomment;
		}

		public PaymentTermsTable getPaymentTermsTable() {
			return paymentTermsTable;
		}

		public void setPaymentTermsTable(PaymentTermsTable paymentTermsTable) {
			this.paymentTermsTable = paymentTermsTable;
		}

		public ProductChargesTable getChargesTable() {
			return chargesTable;
		}

		public void setChargesTable(ProductChargesTable chargesTable) {
			this.chargesTable = chargesTable;
		}

		public ProductTaxesTable getProdTaxTable() {
			return prodTaxTable;
		}

		public void setProdTaxTable(ProductTaxesTable prodTaxTable) {
			this.prodTaxTable = prodTaxTable;
		}

		public SalesLineItemSalesQuotationTable getLineitemsalesquotationtable() {
			return lineitemsalesquotationtable;
		}

		public void setLineitemsalesquotationtable(SalesLineItemSalesQuotationTable lineitemsalesquotationtable) {
			this.lineitemsalesquotationtable = lineitemsalesquotationtable;
		}

		public DateBox getDbdeliverydate() {
			return dbdeliverydate;
		}

		public void setDbdeliverydate(DateBox dbdeliverydate) {
			this.dbdeliverydate = dbdeliverydate;
		}

		public AddressComposite getAcshippingcomposite() {
			return acshippingcomposite;
		}

		public void setAcshippingcomposite(AddressComposite acshippingcomposite) {
			this.acshippingcomposite = acshippingcomposite;
		}


		public ProductInfoComposite getProdInfoComposite() {
			return prodInfoComposite;
		}

		public void setProdInfoComposite(ProductInfoComposite prodInfoComposite) {
			this.prodInfoComposite = prodInfoComposite;
		}

		public ListBox getCbcformlis() {
			return cbcformlis;
		}

		public void setCbcformlis(ListBox cbcformlis) {
			this.cbcformlis = cbcformlis;
		}

		public ObjectListBox<TaxDetails> getOlbcstpercent() {
			return olbcstpercent;
		}

		public void setOlbcstpercent(ObjectListBox<TaxDetails> olbcstpercent) {
			this.olbcstpercent = olbcstpercent;
		}
		

		public Button getBtnsecurityDep() {
			return btnsecurityDep;
		}

		public void setBtnsecurityDep(Button btnsecurityDep) {
			this.btnsecurityDep = btnsecurityDep;
		}

		public int getF_docid() {
			return f_docid;
		}

		public void setF_docid(int f_docid) {
			this.f_docid = f_docid;
		}

		public Date getF_docDate() {
			return f_docDate;
		}

		public void setF_docDate(Date f_docDate) {
			this.f_docDate = f_docDate;
		}

		public String getF_docStatus() {
			return f_docStatus;
		}

		public void setF_docStatus(String f_docStatus) {
			this.f_docStatus = f_docStatus;
		}


		public Date getF_reDate() {
			return f_reDate;
		}

		public void setF_reDate(Date f_reDate) {
			this.f_reDate = f_reDate;
		}

		public boolean isF_depositStatus() {
			return f_depositStatus;
		}

		public void setF_depositStatus(boolean f_depositStatus) {
			this.f_depositStatus = f_depositStatus;
		}

		public String getF_payMethod() {
			return f_payMethod;
		}

		public void setF_payMethod(String f_payMethod) {
			this.f_payMethod = f_payMethod;
		}


		public String getF_bankName() {
			return f_bankName;
		}

		public void setF_bankName(String f_bankName) {
			this.f_bankName = f_bankName;
		}

		public String getF_branch() {
			return f_branch;
		}

		public void setF_branch(String f_branch) {
			this.f_branch = f_branch;
		}

		public String getF_paybleAt() {
			return f_paybleAt;
		}

		public void setF_paybleAt(String f_paybleAt) {
			this.f_paybleAt = f_paybleAt;
		}

		public String getF_fouring() {
			return f_fouring;
		}

		public void setF_fouring(String f_fouring) {
			this.f_fouring = f_fouring;
		}

		public String getF_checkName() {
			return f_checkName;
		}

		public void setF_checkName(String f_checkName) {
			this.f_checkName = f_checkName;
		}

	

		public Date getF_checkDate() {
			return f_checkDate;
		}

		public void setF_checkDate(Date f_checkDate) {
			this.f_checkDate = f_checkDate;
		}


		public String getF_ifscCode() {
			return f_ifscCode;
		}

		public void setF_ifscCode(String f_ifscCode) {
			this.f_ifscCode = f_ifscCode;
		}

		public String getF_micrCode() {
			return f_micrCode;
		}

		public void setF_micrCode(String f_micrCode) {
			this.f_micrCode = f_micrCode;
		}

		public String getF_swiftCode() {
			return f_swiftCode;
		}

		public void setF_swiftCode(String f_swiftCode) {
			this.f_swiftCode = f_swiftCode;
		}

		public String getF_instruction() {
			return f_instruction;
		}

		public void setF_instruction(String f_instruction) {
			this.f_instruction = f_instruction;
		}

		public Address getF_address() {
			return f_address;
		}

		public void setF_address(Address f_address) {
			this.f_address = f_address;
		}

		public Double getF_securityDeposit() {
			return f_securityDeposit;
		}

		public void setF_securityDeposit(Double f_securityDeposit) {
			this.f_securityDeposit = f_securityDeposit;
		}

		

		public String getF_accountNo() {
			return f_accountNo;
		}

		public void setF_accountNo(String f_accountNo) {
			this.f_accountNo = f_accountNo;
		}

		public int getF_checkNo() {
			return f_checkNo;
		}

		public void setF_checkNo(int f_checkNo) {
			this.f_checkNo = f_checkNo;
		}

		public String getF_serviceNo() {
			return f_serviceNo;
		}

		public void setF_serviceNo(String f_serviceNo) {
			this.f_serviceNo = f_serviceNo;
		}

		public Button getBaddproducts() {
			return baddproducts;
		}

		public void setBaddproducts(Button baddproducts) {
			this.baddproducts = baddproducts;
		}

		public CheckBox getCbCheckAddress() {
			return cbCheckAddress;
		}

		public void setCbCheckAddress(CheckBox cbCheckAddress) {
			this.cbCheckAddress = cbCheckAddress;
		}

		public TextBox getTbremark() {
			return tbremark;
		}

		public void setTbremark(TextBox tbremark) {
			this.tbremark = tbremark;
		}

		public DateBox getDbquotationDate() {
			return dbquotationDate;
		}

		public void setDbquotationDate(DateBox dbquotationDate) {
			this.dbquotationDate = dbquotationDate;
		}

		public DateBox getDbFollowUpDate() {
			return dbFollowUpDate;
		}

		public void setDbFollowUpDate(DateBox dbFollowUpDate) {
			this.dbFollowUpDate = dbFollowUpDate;
		}

		
		/**
		 * Date : 23-09-2018 BY VIJAY
		 * Adding calculation part for other charges
		 */
		public void addOtherChargesInTaxTbl(){
			List<ProductOtherCharges> taxList=this.prodTaxTable.getDataprovider().getList();
			for(OtherCharges otherCharges:tblOtherCharges.getDataprovider().getList()){
				/**
				 * If GST tax is not applicable then we will consider tax1 as vat tax field and tax2 as service tax field.
				 */
				if(otherCharges.getTax1().getTaxPrintName()!=null&&!otherCharges.getTax1().getTaxPrintName().equals("")){
					boolean updateTaxFlag=true;
					if(otherCharges.getTax1().getTaxPrintName().equalsIgnoreCase("SELECT")){
						updateTaxFlag=false;
//						return;
					}
					if(otherCharges.getTax1().getPercentage()==0){
						updateTaxFlag=false;
//						return;
					}
					if(updateTaxFlag){
						ProductOtherCharges pocentity=new ProductOtherCharges();
						pocentity.setChargeName(otherCharges.getTax1().getTaxPrintName());
						pocentity.setChargePercent(otherCharges.getTax1().getPercentage());
	//					pocentity.setIndexCheck(i+1);
						int indexValue=this.checkTaxPercent(otherCharges.getTax1().getPercentage(),otherCharges.getTax1().getTaxPrintName());
						if(indexValue!=-1){
							pocentity.setAssessableAmount(otherCharges.getAmount()+taxList.get(indexValue).getAssessableAmount());
							this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
						}
						if(indexValue==-1){
							pocentity.setAssessableAmount(otherCharges.getAmount());
						}
						this.prodTaxTable.getDataprovider().getList().add(pocentity);
					}
				}else{
					boolean updateTaxFlag=true;
					if(otherCharges.getTax1().getPercentage()==0){
						updateTaxFlag=false;
//						return;
					}
					if(updateTaxFlag){
						ProductOtherCharges pocentity=new ProductOtherCharges();
						pocentity.setChargeName("VAT");
						pocentity.setChargePercent(otherCharges.getTax1().getPercentage());
	//					pocentity.setIndexCheck(i+1);
						int indexValue=this.checkTaxPercent(otherCharges.getTax1().getPercentage(),"VAT");
						if(indexValue!=-1){
							pocentity.setAssessableAmount(otherCharges.getAmount()+taxList.get(indexValue).getAssessableAmount());
							this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
						}
						if(indexValue==-1){
							pocentity.setAssessableAmount(otherCharges.getAmount());
						}
						this.prodTaxTable.getDataprovider().getList().add(pocentity);
					}
				}
				
				if(otherCharges.getTax2().getTaxPrintName()!=null&&!otherCharges.getTax2().getTaxPrintName().equals("")){
					System.out.println("INSIDE GST TAX PRINT NAME : "+otherCharges.getTax2().getTaxPrintName());
					boolean updateTaxFlag=true;
					if(otherCharges.getTax2().getTaxPrintName().equalsIgnoreCase("SELECT")){
						updateTaxFlag=false;
//						return;
					}
					if(otherCharges.getTax2().getPercentage()==0){
						updateTaxFlag=false;
//						return;
					}
					if(updateTaxFlag){
						ProductOtherCharges pocentity=new ProductOtherCharges();
						pocentity.setChargeName(otherCharges.getTax2().getTaxPrintName());
						pocentity.setChargePercent(otherCharges.getTax2().getPercentage());
	//					pocentity.setIndexCheck(i+1);
						int indexValue=this.checkTaxPercent(otherCharges.getTax2().getPercentage(),otherCharges.getTax2().getTaxPrintName());
						if(indexValue!=-1){
							pocentity.setAssessableAmount(otherCharges.getAmount()+taxList.get(indexValue).getAssessableAmount());
							this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
						}
						if(indexValue==-1){
							pocentity.setAssessableAmount(otherCharges.getAmount());
						}
						this.prodTaxTable.getDataprovider().getList().add(pocentity);
					}
				}else{
					System.out.println("ST OR NON GST");
					boolean updateTaxFlag=true;
					if(otherCharges.getTax2().getPercentage()==0){
						updateTaxFlag=false;
//						return;
					}
					if(otherCharges.getTax1().getTaxPrintName()!=null&&!otherCharges.getTax1().getTaxPrintName().equals("")){
						updateTaxFlag=false;
//						return;
					}
					if(updateTaxFlag){
						ProductOtherCharges pocentity=new ProductOtherCharges();
						pocentity.setChargeName("Service Tax");
						pocentity.setChargePercent(otherCharges.getTax2().getPercentage());
	//					pocentity.setIndexCheck(i+1);
						int indexValue=this.checkTaxPercent(otherCharges.getTax2().getPercentage(),"Service");
						if(indexValue!=-1){
						 	double assessValue=otherCharges.getAmount()+(otherCharges.getAmount()*otherCharges.getTax1().getPercentage()/100);
							pocentity.setAssessableAmount(assessValue+taxList.get(indexValue).getAssessableAmount());
							this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
						}
						if(indexValue==-1){
							double assessValue=otherCharges.getAmount()+(otherCharges.getAmount()*otherCharges.getTax1().getPercentage()/100);
							pocentity.setAssessableAmount(assessValue);
						}
						this.prodTaxTable.getDataprovider().getList().add(pocentity);
					}
				}
			}
		}
		/**
		 * ends here
		 */
		
		
		

		public DoubleBox getDbfinalTotalAmt() {
			return dbfinalTotalAmt;
		}

		public void setDbfinalTotalAmt(DoubleBox dbfinalTotalAmt) {
			this.dbfinalTotalAmt = dbfinalTotalAmt;
		}

		public TextBox getTbroundoffAmt() {
			return tbroundoffAmt;
		}

		public void setTbroundoffAmt(TextBox tbroundoffAmt) {
			this.tbroundoffAmt = tbroundoffAmt;
		}

		public TextBox getTbDocStatusreason() {
			return tbDocStatusreason;
		}

		public void setTbDocStatusreason(TextBox tbDocStatusreason) {
			this.tbDocStatusreason = tbDocStatusreason;
		}
		
		
		private void reactOnCosting() {
			if(lineitemsalesquotationtable.getValue()==null||lineitemsalesquotationtable.getValue().size()==0){
				showDialogMessage("Please add product details first.");
				return;
			}
			marginPopup.clear();
			
			boolean updateFlag=true;
			if(vendorMargins!=null&&vendorMargins.size()!=0){
				updateFlag=false;
//				marginPopup.vendorMarginTbl.setValue(vendorMargins);
			}
			if(otherChargesMargins!=null&&otherChargesMargins.size()!=0){
				updateFlag=false;
//				marginPopup.otherChargesMarginTbl.setValue(otherChargesMargins);
			}
			
			if(updateFlag){
				Console.log("New Entry in costing.....!!");
				ArrayList<VendorMargin> vendorMarginList=new ArrayList<VendorMargin>();
				ArrayList<OtherChargesMargin> otherChargesList=new ArrayList<OtherChargesMargin>();
				for(SalesLineItem item:lineitemsalesquotationtable.getValue()){
					VendorMargin venObj=new VendorMargin();
					venObj.setProductId(item.getPrduct().getCount());
					venObj.setProductSrNo(item.getProductSrNo());
					venObj.setProductName(item.getProductName());
					venObj.setSalesPrice(item.getTotalAmount());
					venObj.setVendorPrice(0);
					venObj.setVendorMargin(0);
					vendorMarginList.add(venObj);
					
					OtherChargesMargin ocObj=new OtherChargesMargin();
					ocObj.setProductId(item.getPrduct().getCount());
					ocObj.setProductSrNo(item.getProductSrNo());
					ocObj.setProductName(item.getProductName());
					ocObj.setOtherCharges(0);
					otherChargesList.add(ocObj);
				}
				marginPopup.vendorMarginTbl.setValue(vendorMarginList);
				marginPopup.otherChargesMarginTbl.setValue(otherChargesList);
			}else{
				Console.log("Updating Costing ...!!");
				for(SalesLineItem item:lineitemsalesquotationtable.getValue()){
					boolean addFlag=true;
					for(VendorMargin venObj:vendorMargins){
						if(venObj.getProductId()==item.getPrduct().getCount()&&venObj.getProductSrNo()==item.getProductSrNo()){
							addFlag=false;
							venObj.setProductName(item.getProductName());
							venObj.setSalesPrice(item.getTotalAmount());
							if(venObj.getVendorMargin()!=0){
								venObj.setVendorMargin(venObj.getSalesPrice()-venObj.getVendorPrice());
							}else{
								venObj.setVendorMargin(0);
							}
						}
					}
					if(addFlag){
						VendorMargin venObj=new VendorMargin();
						venObj.setProductId(item.getPrduct().getCount());
						venObj.setProductSrNo(item.getProductSrNo());
						venObj.setProductName(item.getProductName());
						venObj.setSalesPrice(item.getTotalAmount());
						venObj.setVendorPrice(0);
						venObj.setVendorMargin(0);
						vendorMargins.add(venObj);
					}
				}
				
				for(VendorMargin venObj:vendorMargins){
					boolean deleteFlag=true;
					for(SalesLineItem item:lineitemsalesquotationtable.getValue()){
						if(venObj.getProductId()==item.getPrduct().getCount()&&venObj.getProductSrNo()==item.getProductSrNo()){
							deleteFlag=false;
						}
					}
					if(deleteFlag){
						vendorMargins.remove(venObj);
					}
				}
				
				for(SalesLineItem item:lineitemsalesquotationtable.getValue()){
					boolean addFlag=true;
					for(OtherChargesMargin ocObj:otherChargesMargins){
						if(ocObj.getProductId()==item.getPrduct().getCount()&&ocObj.getProductSrNo()==item.getProductSrNo()){
							addFlag=false;
							ocObj.setProductName(item.getProductName());
						}
					}
					if(addFlag){
						OtherChargesMargin ocObj=new OtherChargesMargin();
						ocObj.setProductId(item.getPrduct().getCount());
						ocObj.setProductSrNo(item.getProductSrNo());
						ocObj.setProductName(item.getProductName());
						ocObj.setOtherCharges(0);
						otherChargesMargins.add(ocObj);
					}
				}
				
				for(OtherChargesMargin ocObj:otherChargesMargins){
					boolean deleteFlag=true;
					for(SalesLineItem item:lineitemsalesquotationtable.getValue()){
						if(ocObj.getProductId()==item.getPrduct().getCount()&&ocObj.getProductSrNo()==item.getProductSrNo()){
							deleteFlag=false;
						}
					}
					if(deleteFlag){
						otherChargesMargins.remove(ocObj);
					}
				}
				
				marginPopup.vendorMarginTbl.setValue(vendorMargins);
				marginPopup.otherChargesMarginTbl.setValue(otherChargesMargins);
				
				totalVendorMargin=0d;
				for(VendorMargin venObj:vendorMargins){
					totalVendorMargin=totalVendorMargin+venObj.getVendorMargin();
				}
				
				totalOcMargin=0d;
				for(OtherChargesMargin ocObj:otherChargesMargins){
					totalOcMargin=totalOcMargin+ocObj.getOtherCharges();
				}
			}
			
			marginPopup.showPopUp();
			if(totalVendorMargin!=null){
				marginPopup.dbTotalVendorMargin.setValue(totalVendorMargin);
			}
			if(totalOcMargin!=null){
				marginPopup.dbOtherChargesMargin.setValue(totalOcMargin);
			}
			
		}
		
		
		private void reactOnCostingSave() {
			boolean updateFlag=true;
			if(vendorMargins!=null&&vendorMargins.size()!=0){
				updateFlag=false;
			}
			if(otherChargesMargins!=null&&otherChargesMargins.size()!=0){
				updateFlag=false;
			}
			
			if(updateFlag){
				vendorMargins=new ArrayList<VendorMargin>();
				otherChargesMargins=new ArrayList<OtherChargesMargin>();
				Console.log("SAVE New Entry in costing.....!!");
				for(SalesLineItem item:lineitemsalesquotationtable.getValue()){
					VendorMargin venObj=new VendorMargin();
					venObj.setProductId(item.getPrduct().getCount());
					venObj.setProductSrNo(item.getProductSrNo());
					venObj.setProductName(item.getProductName());
					venObj.setSalesPrice(item.getTotalAmount());
					venObj.setVendorPrice(0);
					venObj.setVendorMargin(0);
					vendorMargins.add(venObj);
					
					OtherChargesMargin ocObj=new OtherChargesMargin();
					ocObj.setProductId(item.getPrduct().getCount());
					ocObj.setProductSrNo(item.getProductSrNo());
					ocObj.setProductName(item.getProductName());
					ocObj.setOtherCharges(0);
					otherChargesMargins.add(ocObj);
				}
			}else{
				Console.log("SAVE Updating Costing ...!!");
				for(SalesLineItem item:lineitemsalesquotationtable.getValue()){
					boolean addFlag=true;
					for(VendorMargin venObj:vendorMargins){
						if(venObj.getProductId()==item.getPrduct().getCount()&&venObj.getProductSrNo()==item.getProductSrNo()){
							addFlag=false;
							venObj.setProductName(item.getProductName());
							venObj.setSalesPrice(item.getTotalAmount());
							if(venObj.getVendorMargin()!=0){
								venObj.setVendorMargin(venObj.getSalesPrice()-venObj.getVendorPrice());
							}else{
								venObj.setVendorMargin(0);
							}
						}
					}
					if(addFlag){
						VendorMargin venObj=new VendorMargin();
						venObj.setProductId(item.getPrduct().getCount());
						venObj.setProductSrNo(item.getProductSrNo());
						venObj.setProductName(item.getProductName());
						venObj.setSalesPrice(item.getTotalAmount());
						venObj.setVendorPrice(0);
						venObj.setVendorMargin(0);
						vendorMargins.add(venObj);
					}
				}
				
				for(VendorMargin venObj:vendorMargins){
					boolean deleteFlag=true;
					for(SalesLineItem item:lineitemsalesquotationtable.getValue()){
						if(venObj.getProductId()==item.getPrduct().getCount()&&venObj.getProductSrNo()==item.getProductSrNo()){
							deleteFlag=false;
						}
					}
					if(deleteFlag){
						vendorMargins.remove(venObj);
					}
				}
				
				for(SalesLineItem item:lineitemsalesquotationtable.getValue()){
					boolean addFlag=true;
					for(OtherChargesMargin ocObj:otherChargesMargins){
						if(ocObj.getProductId()==item.getPrduct().getCount()&&ocObj.getProductSrNo()==item.getProductSrNo()){
							addFlag=false;
							ocObj.setProductName(item.getProductName());
						}
					}
					if(addFlag){
						OtherChargesMargin ocObj=new OtherChargesMargin();
						ocObj.setProductId(item.getPrduct().getCount());
						ocObj.setProductSrNo(item.getProductSrNo());
						ocObj.setProductName(item.getProductName());
						ocObj.setOtherCharges(0);
						otherChargesMargins.add(ocObj);
					}
				}
				
				for(OtherChargesMargin ocObj:otherChargesMargins){
					boolean deleteFlag=true;
					for(SalesLineItem item:lineitemsalesquotationtable.getValue()){
						if(ocObj.getProductId()==item.getPrduct().getCount()&&ocObj.getProductSrNo()==item.getProductSrNo()){
							deleteFlag=false;
						}
					}
					if(deleteFlag){
						otherChargesMargins.remove(ocObj);
					}
				}
				totalVendorMargin=0d;
				for(VendorMargin venObj:vendorMargins){
					totalVendorMargin=totalVendorMargin+venObj.getVendorMargin();
				}
				
				totalOcMargin=0d;
				for(OtherChargesMargin ocObj:otherChargesMargins){
					totalOcMargin=totalOcMargin+ocObj.getOtherCharges();
				}
			}
		}
		
		@Override
		public void refreshTableData() {
			// TODO Auto-generated method stub
			super.refreshTableData();
			prodTaxTable.getTable().redraw();
			chargesTable.getTable().redraw();
			lineitemsalesquotationtable.getTable().redraw();
			paymentTermsTable.getTable().redraw();
			tblOtherCharges.getTable().redraw();
			
		}
		
		public void loadCustomerBranch(final String custBranch,int custId) {
			// TODO Auto-generated method stub
			GenricServiceAsync async=GWT.create(GenricService.class);
			MyQuerry querry=new MyQuerry();
			Filter temp=null;
			Vector<Filter> vecList=new Vector<Filter>();
			
			temp=new Filter();
			temp.setQuerryString("cinfo.count");
			temp.setIntValue(custId);
			vecList.add(temp);
			
			temp=new Filter();
			temp.setQuerryString("status");
			temp.setBooleanvalue(true);
			vecList.add(temp);
			
			querry.setFilters(vecList);
			querry.setQuerryObject(new CustomerBranchDetails());
			async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {

				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
				}

				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					// TODO Auto-generated method stub
					olbcustBranch.clear();
					olbcustBranch.addItem("--SELECT--");
					if(result!=null&&result.size()!=0){
						ArrayList<CustomerBranchDetails>custBranchList=new ArrayList<CustomerBranchDetails>();
						for(SuperModel model:result){
							CustomerBranchDetails obj=(CustomerBranchDetails) model;
							custBranchList.add(obj);
						}
						olbcustBranch.setListItems(custBranchList);
						if(custBranch!=null){
							olbcustBranch.setValue(custBranch);
						}
					}
				}
			});
			
		}
		
		
		protected void updateTaxesDetails(Customer cust) {
			List<SalesLineItem> prodlist = lineitemsalesquotationtable.getDataprovider().getList();
			if(prodlist.size()>0){
				Branch branchEntity =olbbBranch.getSelectedItem();
				
				if(cust!=null){
					System.out.println("customer onselection");
					 String customerBillingaddressState = cust.getAdress().getState();
					 List<SalesLineItem> productlist = AppUtility.updateTaxesDetails(prodlist, branchEntity, customerBillingaddressState,true);
					 lineitemsalesquotationtable.getDataprovider().setList(productlist);
					 lineitemsalesquotationtable.getTable().redraw();
					RowCountChangeEvent.fire(lineitemsalesquotationtable.getTable(), lineitemsalesquotationtable.getDataprovider().getList().size(), true);
					setEnable(true);
				}
				
			}
		}

		
		private void reactonAddTermsAndCondition() {
			
			if(olbtermsAndConditionTitle.getSelectedIndex()!=0){
				TermsAndConditions termsAndCondition = olbtermsAndConditionTitle.getSelectedItem();
				termsAndCondition.setMsg("");
				this.termsAndConditionTable.getDataprovider().getList().add(termsAndCondition);
			}
			else{
				showDialogMessage("Please select terms and condition title");
			}
			
		}

		private void loadPaymentMode() {

			try {
				 GenricServiceAsync async =  GWT.create(GenricService.class);

				MyQuerry querry = new MyQuerry();
				
				
				Company c = new Company();
				Vector<Filter> filtervec = new Vector<Filter>();
				Filter filter = null;
				
				filter = new Filter();
				filter.setQuerryString("companyId");
				filter.setLongValue(c.getCompanyId());
				filtervec.add(filter);
				
				querry.setFilters(filtervec);
				
				
				querry.setQuerryObject(new CompanyPayment());

				async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
					
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						// TODO Auto-generated method stub

						System.out.println(" in side date load size"+result.size());
						Console.log("get result size of payment mode --" + result.size());
						objPaymentMode.clear();
						objPaymentMode.addItem("--SELECT--");
//						paymentModeList = new ArrayList<CompanyPayment>();
						if (result.size() == 0) {

						} else {
							for (SuperModel model : result) {
								CompanyPayment payList = (CompanyPayment) model;
								paymentModeList.add(payList);
							}
							
							if (paymentModeList.size() != 0) {
								objPaymentMode.setListItems(paymentModeList);
							}
							
							for(int i=1;i<objPaymentMode.getItemCount();i++){
								String paymentDet = objPaymentMode.getItemText(i);
								Console.log("SERVICE DETAILS : "+paymentDet);
							}	
						}
					
					}
					
					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						
					}
				});
				

			} catch (Exception e) {
				e.printStackTrace();
			}
		
			
		}
}
