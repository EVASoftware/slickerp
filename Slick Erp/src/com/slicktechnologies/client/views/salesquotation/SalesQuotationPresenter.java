package com.slicktechnologies.client.views.salesquotation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.approvalutility.ApprovableFormScreenPresenter;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.reusabledata.ConditionDialogBox;
import com.slicktechnologies.client.reusabledata.CopyDataConfig;
import com.slicktechnologies.client.services.CommunicationLogService;
import com.slicktechnologies.client.services.CommunicationLogServiceAsync;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.EmailService;
import com.slicktechnologies.client.services.EmailServiceAsync;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.popups.NewEmailPopUp;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.client.views.purchase.purchaseorder.PurchaseOrderForm;
import com.slicktechnologies.client.views.salesorder.SalesOrderForm;
import com.slicktechnologies.client.views.salesorder.SalesOrderPresenter;
import com.slicktechnologies.client.views.securitydeposit.securitydepositpopup.SecurityDepositPopUp;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.Sales;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cnc.CNC;
import com.slicktechnologies.shared.common.cnc.CNCVersion;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.fumigation.Fumigation;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.ProductInfo;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;
import com.slicktechnologies.shared.common.inventory.BillOfMaterial;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.salesorder.OtherChargesMargin;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesorder.SalesQuotation;
import com.slicktechnologies.shared.common.salesorder.VendorMargin;
import com.slicktechnologies.client.views.cnc.CNCForm;
import com.slicktechnologies.client.views.communicationlog.CommunicationLogPopUp;
import com.slicktechnologies.client.views.contract.ContractPresenter;
import com.slicktechnologies.client.views.contract.CustomerAddressPopup;
import com.slicktechnologies.client.views.documentcancellation.DocumentCancellationPopUp;
import com.slicktechnologies.client.views.fumigationAustralia.FumigationAustraliaForm;
import com.slicktechnologies.client.views.fumigationdetails.FumigationALPForm;
import com.slicktechnologies.client.views.fumigationdetails.FumigationForm;

public class SalesQuotationPresenter extends ApprovableFormScreenPresenter<SalesQuotation> implements RowCountChangeEvent.Handler, SelectionHandler<Suggestion>, ChangeHandler {
	
	//Token to set the concrete FormScreen class name
	public static  SalesQuotationForm form;
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	final SalesQuotationServiceAsync async=GWT.create(SalesQuotationService.class);
	EmailServiceAsync emailService=GWT.create(EmailService.class);
	final GenricServiceAsync genasync=GWT.create(GenricService.class);
	String retrCompnyState=retrieveCompanyState();
	static String companyState=""; 
	ConditionDialogBox conditionPopup=new ConditionDialogBox("Are you sure you want to change address. All products will be deleted!",AppConstants.YES,AppConstants.NO);
	SecurityDepositPopUp securitydeposit=new SecurityDepositPopUp();
	PopupPanel panel,panel1;

	//  rohan added this cnt for preprint functionality 
	int cnt=0;
	ConditionDialogBox conditionPopupforPreprint=new ConditionDialogBox("Do you want to print on preprinted Stationery",AppConstants.YES,AppConstants.NO);

	/**
	 * 29-06-1991 for communication log 
	 * Nidhi
	 */
	CommunicationLogPopUp communicationLogPopUp = new CommunicationLogPopUp();
//	PopupPanel communicationPanel;
	CommunicationLogServiceAsync communicationService = GWT.create(CommunicationLogService.class);
	/**
	 * ends here
	 */
	
	/**11-11-2017 Adding cancellation popup by Manisha
	 */
	
	DocumentCancellationPopUp cancelpopup=new DocumentCancellationPopUp();
	PopupPanel cancelpanel;
	
	/**ends**/
	/**
	 * nidhi
	 * 1-08-2018
	 * for 
	 */
	DocumentCancellationPopUp unSucessfullPopup = new DocumentCancellationPopUp(true);
	
	/**ends**/
	
	CustomerAddressPopup custAddresspopup = new CustomerAddressPopup();
	InlineLabel lblUpdate = new InlineLabel("Update");

	NewEmailPopUp emailpopup = new NewEmailPopUp();

	public SalesQuotationPresenter(FormScreen<SalesQuotation> view, SalesQuotation model) {
		super(view, model);
		form=(SalesQuotationForm) view;
		form.setPresenter(this);
		form.getLineitemsalesquotationtable().getTable().addRowCountChangeHandler(this);
		form.getChargesTable().getTable().addRowCountChangeHandler(this);
		form.getCbCheckAddress().addClickHandler(this);
		form.getPersonInfoComposite().getId().addSelectionHandler(this);
		form.getPersonInfoComposite().getName().addSelectionHandler(this);
		form.getPersonInfoComposite().getPhone().addSelectionHandler(this);
		form.getCbcformlis().addChangeHandler(this);
		form.getOlbcstpercent().addChangeHandler(this);
		form.getAcshippingcomposite().getState().addChangeHandler(this);
		conditionPopup.getBtnOne().addClickHandler(this);
		conditionPopup.getBtnTwo().addClickHandler(this);
		form.btnsecurityDep.addClickHandler(this);
		form.btnsecurityDep.setEnabled(true);
		securitydeposit.getBtnOk().addClickHandler(this);
		securitydeposit.getBtnCancel().addClickHandler(this);

		conditionPopupforPreprint.getBtnOne().addClickHandler(this);
		conditionPopupforPreprint.getBtnTwo().addClickHandler(this);
		
		/*
		 * nidhi 29-06-2017
		 * communicaionlog pop
		 */
		
		communicationLogPopUp.getBtnOk().addClickHandler(this);
		communicationLogPopUp.getBtnCancel().addClickHandler(this);
		communicationLogPopUp.getBtnAdd().addClickHandler(this);
		/*
		 * end
		 */
		
		/*
		 * Manisha added this on 11/11/2017
		 */
		
		cancelpopup.getBtnOk().addClickHandler(this);
		cancelpopup.getBtnCancel().addClickHandler(this);
		
		/*
		 * end
		 */
		
		/**
		 * Date : 23-03-2018 BY VIJAY
		 * Adding row count change handler on other charges table
		 */
		form.tblOtherCharges.getTable().addRowCountChangeHandler(this);
		/**
		 * End
		 */
		/**
		 * nidhi
		 * 1-08-2018
		 * for save reason
		 */
		unSucessfullPopup.getBtnOk().addClickHandler(this);
		unSucessfullPopup.getBtnCancel().addClickHandler(this);
		/**
		 * end
		 */
		
		form.olbcustBranch.addChangeHandler(this);
		
		custAddresspopup.getLblOk().addClickHandler(this);
		custAddresspopup.getLblCancel().addClickHandler(this);
		custAddresspopup.getBtnbillingaddr().addClickHandler(this);
		lblUpdate.addClickHandler(this);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.SALESQUOTATION,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		super.reactToProcessBarEvents(e);
		InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();

		
		if(text.equals(AppConstants.CREATESALESORDER)){
			if(form.isPopUpAppMenubar()){
				Console.log("SALES QUOTATION POPUP : Create Sales Order clicked!");
				return;
			}
			reactToCreateSalesOrder();
		}
		if(text.equals(AppConstants.Unsucessful)){
//			reactToUnsuccessful();
			/**
			 * nidhi
			 * 1-08-2018
			 * for  unsucesful msg
			 */
			{
				unSucessfullPopup.getPaymentDate().setValue(AppUtility.parseDate(new Date())+"");
				unSucessfullPopup.getPaymentID().setValue(model.getCount()+"");
				unSucessfullPopup.getPaymentStatus().setValue(model.getStatus());
				cancelpanel=new PopupPanel(true);
				cancelpanel.add(unSucessfullPopup);
				cancelpanel.show();
				cancelpanel.center();
			}
			/**
			 * end
			 */
		}
		if(text.equals(AppConstants.NEW)){
			if(form.isPopUpAppMenubar()){
				Console.log("SALES QUOTATION POPUP : New clicked!!");
				return;
			}
			reactToNew();
		}
		if(text.equals("Email")){
			reactOnEmail();
		}
		/** date 07/11/2017 added by komal to add functionality revised quotation button **/
		if(text.equals("Revised Quotation")){
			if(form.isPopUpAppMenubar()){
				Console.log("SALES QUOTATION POPUP : Revised Quotation clicked!");
				return;
			}
			reactOnRevisedQuotation();
		}
		
		if(text.equals(AppConstants.CUSTOMERADDRESS)) {
			reactOnCustomerAddress();
		}
		
		/**BY Manisha on 11/11/2017 **/
		
		if(text.equals(AppConstants.CANCEL)){
			cancelpopup.getPaymentDate().setValue(AppUtility.parseDate(new Date())+"");
			cancelpopup.getPaymentID().setValue(model.getCount()+"");
			cancelpopup.getPaymentStatus().setValue(model.getStatus());
			cancelpanel=new PopupPanel(true);
			cancelpanel.add(cancelpopup);
			cancelpanel.show();
			cancelpanel.center();
		}

//		if(text.equals("updateQuotations")){
//			Console.log("inside onclick");
//			System.out.println("in side onclick");
//			updateQuotation();
//		}
		
		
		
	}



//	private void updateQuotation() {
//		Console.log("inside update quotation method");
//		System.out.println("inside update quotation method");
//
//		
//		MyQuerry querry=new MyQuerry();
////		Vector<Filter> filtervec=new Vector<Filter>();
////		
////		Filter tempfilter=null;
////		tempfilter=new Filter();
////		tempfilter.setQuerryString("companyId");
////		tempfilter.setLongValue(model.getCompanyId());
////		
////		Console.log("inside update quotation method"+model.getCompanyId());
////		filtervec.add(tempfilter);
////		
////		querry.getFilters().add(tempfilter);
//		querry.setQuerryObject(new SalesQuotation());
//		genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
//			@Override
//			public void onFailure(Throwable caught) {
//				form.showDialogMessage("An Unexpected error occurred!");
//			}
//			@Override
//			public void onSuccess(ArrayList<SuperModel> result) {
//				Console.log("result size"+result.size());
//				for(SuperModel model:result)
//				{
//					SalesQuotation salesquotationEntity=(SalesQuotation)model;
//					
//					for(int i=0;i<salesquotationEntity.getItems().size();i++)
//					{
//					salesquotationEntity.getItems().get(i).getPrduct().setProductImage(new DocumentUpload());
//					salesquotationEntity.getItems().get(i).getPrduct().setTermsAndConditions(new DocumentUpload());
//					}
//					
//					genasync.save(salesquotationEntity,new AsyncCallback<ReturnFromServer>() {
//						@Override
//						public void onFailure(Throwable caught) {
//							form.showDialogMessage("An Unexpected error occurred!");
//						}
//						@Override
//						public void onSuccess(ReturnFromServer result) {
//							form.showDialogMessage("ENTITY UPDATED SUCCESSFULLY!");
//						}
//					});
//				}
//				}
//		});
//				
//	}
	
	@Override
	public void reactOnPrint() {
		
		
		MyQuerry querry = new MyQuerry();
	  	Company c = new Company();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
	  	filter = new Filter();
	  	filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("processName");
		filter.setStringValue("SalesQuotation");
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("processList.status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new ProcessConfiguration());
		
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onFailure(Throwable caught) {
			}			

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				System.out.println(" result set size +++++++"+result.size());
				
				List<ProcessTypeDetails> processList =new ArrayList<ProcessTypeDetails>();
				
				if(result.size()==0){
					
					final String url = GWT.getModuleBaseURL() + "pdfsales"+"?Id="+model.getId()+"&"+"type="+"sq"+"&"+"preprint="+"plane";
					 Window.open(url, "test", "enabled");
					
				}
				else{
				
					for(SuperModel model:result)
					{
						ProcessConfiguration processConfig=(ProcessConfiguration)model;
						processList.addAll(processConfig.getProcessList());
						
					}
				
					for(int k=0;k<processList.size();k++){	
					if(processList.get(k).getProcessType().trim().equalsIgnoreCase("CompanyAsLetterHead")&&processList.get(k).isStatus()==true){
						
						cnt=cnt+1;
					
					}

				
					}
					
					
					if(cnt>0){
						System.out.println("in side react on prinnt cnt");
						panel=new PopupPanel(true);
						panel.add(conditionPopupforPreprint);
						panel.setGlassEnabled(true);
						panel.show();
						panel.center();
					}
					else
					{
						
						final String url = GWT.getModuleBaseURL() + "pdfsales"+"?Id="+model.getId()+"&"+"type="+"sq"+"&"+"preprint="+"plane";
						 Window.open(url, "test", "enabled");
					}
					
				}
				
			}
		});
		
		
		
	}

	@Override
	public void reactOnDownload() 
	{
		ArrayList<SalesQuotation> salesqarray=new ArrayList<SalesQuotation>();
		List<SalesQuotation> list=(List<SalesQuotation>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
		
		salesqarray.addAll(list);
		
		csvservice.setsalesquotationlist(salesqarray, new AsyncCallback<Void>() {
			

			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);
				
			}

			@Override
			public void onSuccess(Void result) {
				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "csvservlet"+"?type="+60;
				Window.open(url, "test", "enabled");
			}
		});

	}

	
	/**
	 * Method token to make new model
	 */
	@Override
	protected void makeNewModel() {
		model=new SalesQuotation();
	}

	public static SalesQuotationForm initalize()
	{
				SalesQuotationForm form=new  SalesQuotationForm();
		
				SalesQuotationPresenterTable gentable=new SalesQuotationPresenterTableProxy();
				gentable.setView(form);
				gentable.applySelectionModle();
				SalesQuotationPresenterSearch.staticSuperTable=gentable;
				SalesQuotationPresenterSearch searchpopup=new SalesQuotationPresenterSearchProxy();
				form.setSearchpopupscreen(searchpopup);
		
				SalesQuotationPresenter  presenter=new SalesQuotationPresenter(form,new SalesQuotation());
				AppMemory.getAppMemory().stickPnel(form);
	   	return form;
	}

	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.salesorder.SalesQuotation")
	public static  class SalesQuotationPresenterSearch extends SearchPopUpScreen<SalesQuotation>{

		@Override
		public MyQuerry getQuerry() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public boolean validate() {
			// TODO Auto-generated method stub
			return true;
		}};

		@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.salesorder.SalesQuotation")
		public static class SalesQuotationPresenterTable extends SuperTable<SalesQuotation> implements GeneratedVariableRefrence{

			@Override
			public Object getVarRef(String varName) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void createTable() {
				// TODO Auto-generated method stub

			}

			@Override
			protected void initializekeyprovider() {

			}

			@Override
			public void addFieldUpdater() {
				// TODO Auto-generated method stub

			}

			@Override
			public void setEnable(boolean state) {

			}

			@Override
			public void applyStyle() {
				// TODO Auto-generated method stub

			}} ;

			
			
			private void reactToCreateSalesOrder()
			{
				boolean validateCopyingForData=false;
				
				validateCopyingForData=CopyDataConfig.validateDataCopying(AppConstants.SALESORDERCOPYDATA);
				
				
				boolean categoryDataVal=false;
				boolean typeDataVal=false;
				boolean groupDataVal=false;
				
				
				
				if(validateCopyingForData==true)
				{
					categoryDataVal=CopyDataConfig.validateData(AppConstants.COPYCATEGORYDATA, model.getCategory(), Screen.SALESORDERCATEGORY);
					typeDataVal=CopyDataConfig.validateData(AppConstants.COPYTYPEDATA, model.getType(), Screen.SALESORDERTYPE);
					groupDataVal=CopyDataConfig.validateData(AppConstants.COPYGROUPDATA, model.getGroup(), Screen.SALESORDERGROUP);
					
					
					
					if(categoryDataVal==false){
						form.showDialogMessage("Sales Order Category Value does not exists in Quotation Category!");
					}
					
					if(categoryDataVal==true&&typeDataVal==false){
						form.showDialogMessage("Sales Order Type value does not exists in Quotation Type!");
					}
					
					if(groupDataVal==false&&typeDataVal==true&&categoryDataVal==true){
						form.showDialogMessage("Sales Order Group value does not exists in Quotation Group!");
					}
					
					
					if(groupDataVal==true&&typeDataVal==true&&categoryDataVal==true){
						switchToOrderScreen(validateCopyingForData);
					}
					
				}
				else{
					switchToOrderScreen(validateCopyingForData);
				}
			}
			
			private void switchToOrderScreen(final boolean statusValue)
			{
				AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Sales/Sales Order",Screen.SALESORDER);
				final SalesOrderForm form=SalesOrderPresenter.initalize();
				final SalesOrder soent=new SalesOrder();
				form.showWaitSymbol();
				Timer t = new Timer() {
				      @Override
				      public void run() {
				    	  form.setToNewState();
				    	  form.hideWaitSymbol();
				    	  form.getTbContractId().setText("");
				    	  if(model.getLeadCount()==-1)
				    		  form.getTbLeadId().setText("");
				    	  form.getPersonInfoComposite().setEnabled(false);
				    	  form.getTbReferenceNumber().setText("");
				    	  form.getPersonInfoComposite().setValue(model.getCinfo());
				    	  form.getOlbbBranch().setValue(model.getBranch());
				    	  form.getOlbApproverName().setValue(model.getApproverName());
				    	  if(model.getLeadCount()!=-1){
				    		  form.getTbLeadId().setValue(model.getLeadCount()+"");
				    	  }
				    	  if(model.getContractCount()!=-1){
				    		  form.getTbContractId().setValue(model.getContractCount()+"");
				    	  }
				    	  
				    	  if(statusValue==true)
				    	  {
				    		  form.getOlbSalesOrderCategory().setValue(model.getCategory());
				    		  form.getOlbSalesOrderGroup().setValue(model.getGroup());
				    		  form.getOlbSalesOrderType().setValue(model.getType());
				    	  }
				    	  /**date 20-6-2020 by Amol added description two and vendor price raised by **/
				    	  form.getTaDescriptionTwo().setValue(model.getDescriptiontwo());
				    	  form.getDbVendorPrice().setValue(model.getVendorPrice());
				    	  
				    	  form.getSalesorderlineitemtable().setValue(model.getItems());
				    	  form.getTbQuotatinId().setValue(model.getCount()+"");
				    	  form.getOlbcPaymentMethods().setValue(model.getPaymentMethod());
				    	  form.getOlbeSalesPerson().setValue(model.getEmployee());
				    	  form.getTbQuotationStatus().setValue(SalesQuotation.CREATED);
				    	  form.getProdTaxTable().setValue(model.getProductTaxes());
				    	  form.getChargesTable().setValue(model.getProductCharges());
//				    	  form.getChargesTable().setEnable(false);
				    	  form.getPaymentTermsTable().setValue(model.getPaymentTermsList());
				    	  form.getDototalamt().setValue(model.getTotalAmount());
				    	  form.getDonetpayamt().setValue(model.getNetpayable());
				    	  form.getCbcheckaddress().setValue(model.getCheckAddress());
				    	  form.getIbCreditPeriod().setValue(model.getCreditPeriod());
			    		  form.getAcshippingcomposite().setValue(model.getShippingAddress());
			    		  form.getAcshippingcomposite().setEnable(false);
			    		  form.getDbdeliverydate().setValue(model.getDeliveryDate());
			    		  if(model.getCformstatus()!=null)
			  			  {
			  				for(int i=0;i<form.getCbcformlis().getItemCount();i++)
			  				{
			  					String data=form.getCbcformlis().getItemText(i);
			  					if(data.equals(model.getCformstatus()))
			  					{
			  						form.getCbcformlis().setSelectedIndex(i);
			  						form.getCbcformlis().setEnabled(true);
			  						break;
			  					}
			  				}
			  			  }
			    		  if(model.getCstName()!=null){
			    			  if(model.getCformstatus().equals(AppConstants.NO)){
			    				  form.getOlbcstpercent().setEnabled(false);
			    			  }
			    			  if(model.getCformstatus().equals(AppConstants.YES)){
			    				  form.getOlbcstpercent().setEnabled(true);
			    			  }
			    			  form.getOlbcstpercent().setValue(model.getCstName());
			    			 
			    		  }
			    		  
			    		  /** Date 23-03-2018 By vijay for other charges table mapping **/
			    		  form.getTblOtherCharges().setValue(model.getOtherCharges());
			    		  
			    		  double amtInclTax=model.getTotalAmount()+form.getProdTaxTable().calculateTotalTaxes();
			    		  NumberFormat nf=NumberFormat.getFormat("0.00");
			    		  amtInclTax=Double.parseDouble(nf.format(amtInclTax));
			    		  form.getDoamtincltax().setValue(amtInclTax);
			    		  soent.setStatus(SalesOrder.CREATED);
//			    		  form.setMenuAsPerStatus();
			    		  
			    		  /**
				    	   * Date 09-06-2018
				    	   * By Vijay for round off amt
				    	   */
				    	  if(model.getRoundOffAmount()!=0){
				    		  form.getTbroundoffAmt().setValue(model.getRoundOffAmount()+"");
				    	  }
				    	  /**
				    	   * ends here
				    	   */
				    	  
				    	  /**
				    	   * @author Anil
				    	   * @since 20-07-2020
				    	   */
				    	  
				    	  if(model.getVendorMargins()!=null&&model.getVendorMargins().size()!=0){
				    		  form.vendorMargins=model.getVendorMargins();
				    		  form.totalVendorMargin=model.getTotalVendorMargin();
				    		  for(VendorMargin obj:form.vendorMargins){
				    			  obj.setReadOnly(true);
				    		  }
				    	  }
				    	  if(model.getOtherChargesMargins()!=null&&model.getOtherChargesMargins().size()!=0){
				    		  form.otherChargesMargins=model.getOtherChargesMargins();
				    		  form.totalOcMargin=model.getTotalOtherChargesMargin();
				    		  for(OtherChargesMargin obj:form.otherChargesMargins){
				    			  obj.setReadOnly(true);
				    		  }
				    	  }
				    	  
				    	  for(SalesLineItem item :form.getSalesorderlineitemtable().getValue() ){
				    		  item.setReadOnly(true);
				    	  }
				    	  
				    		if(model.getCustBillingAddress()!=null && model.getCustBillingAddress().getAddrLine1()!=null 
					  				  && !model.getCustBillingAddress().getAddrLine1().equals("")) {
						  		form.customerBillingAddress = model.getCustBillingAddress();
					  		}
					  		if(model.getShippingAddress()!=null && model.getShippingAddress().getAddrLine1()!=null 
					  				  && !model.getShippingAddress().getAddrLine1().equals("")) {
						  		form.customerServiceAddress = model.getShippingAddress();
					  		}
					  		
					  		if(model.getCustomerBranch()!=null && !model.getCustomerBranch().equals("")){
					  			form.oblCustomerBranch.setValue(model.getCustomerBranch());
								form.loadCustomerBranch(model.getCustomerBranch(),model.getCinfo().getCount());

					  		}
					  		form.checkCustomerStatus(model.getCinfo().getCount());
					  		form.getOlbbBranch().setValue(model.getBranch());
					    	  Branch branchEntity =form.getOlbbBranch().getSelectedItem();
					    	  if(branchEntity.getNumberRange()!=null&&!branchEntity.getNumberRange().equals("")) {
									form.olbcNumberRange.setValue(branchEntity.getNumberRange());
									Console.log("Number range set to drop down by default");
					    	  }else {
					    		  	String range=LoginPresenter.branchWiseNumberRangeMap.get(model.getBranch());
							  		Console.log("LoginPresenter.branchWiseNumberRangeMap size="+LoginPresenter.branchWiseNumberRangeMap.size());	
									if(range!=null&&!range.equals("")) {
										form.olbcNumberRange.setValue(range);
										Console.log("in else Number range set to drop down by default");							
									}else {
										form.olbcNumberRange.setSelectedIndex(0);
										Console.log("could not set number range");
									}					
					    	  }
					  		
			    		  /** Date 23-03-2018 By vijay above line commented becs no need of that getting null pointer exception below line added **/
			    		  form.setAppHeaderBarAsPerStatus();
				      }
				    };
				    t.schedule(5000);
				
				
				
			}
			private void reactToUnsuccessful()
			{
				model.setStatus(SalesQuotation.SALESQUOTATIONUNSUCESSFUL);
				form.getTbQuotationStatus().setText(SalesQuotation.SALESQUOTATIONUNSUCESSFUL);
				save("Marked Unsuccessful !","Failed To Mark Unsuccessful !");
				form.setMenuAsPerStatus();
			}
			private void reactToNew()
			{
				form.setToNewState(); 
				this.initalize();
			}
			
			private void save(final String success,final String failure)
			{
				async.changeStatus(model,new AsyncCallback<Void>() {

					@Override
					public void onFailure(Throwable caught) {
						form.showDialogMessage(failure);
						
					}

					@Override
					public void onSuccess(Void result) {
						form.showDialogMessage(success);
						
					}
				});
			}

			@Override
			public void onRowCountChange(RowCountChangeEvent event) 
			{
				NumberFormat nf=NumberFormat.getFormat("0.00");
				if(event.getSource()==form.getLineitemsalesquotationtable().getTable()){
//				   double totalExcludingTax=form.getLineitemsalesquotationtable().calculateTotalExcludingTax();
//				   totalExcludingTax=Double.parseDouble(nf.format(totalExcludingTax));
//				    form.getDototalamt().setValue(Double.parseDouble(nf.format(totalExcludingTax)));
//				    
//					boolean chkSize=form.getLineitemsalesquotationtable().removeChargesOnDelete();
//					if(chkSize==false){
//						form.getChargesTable().connectToLocal();
//					}
//					
//					try {
//						form.prodTaxTable.connectToLocal();
//						form.addProdTaxes();
//						double totalIncludingTax=form.getDototalamt().getValue()+form.getProdTaxTable().calculateTotalTaxes();
//						form.getDoamtincltax().setValue(Double.parseDouble(nf.format(totalIncludingTax)));
//						form.updateChargesTable();
//					} catch (Exception e) {
//						e.printStackTrace();
//					}
//					double netPay=this.fillNetPayable(form.getDoamtincltax().getValue());
//					netPay=Math.round(netPay);
//					int netPayable=(int) netPay;
//					netPay=netPayable;
//					form.getDonetpayamt().setValue(Double.parseDouble(nf.format(netPay)));
					
					this.productsOnChange();
			    }
				
				if(event.getSource().equals(form.getChargesTable().getTable())){
					if(form.getDoamtincltax().getValue()!=null){
						double newnetpay=form.getDoamtincltax().getValue()+form.getChargesTable().calculateNetPayable();
						newnetpay=Math.round(newnetpay);
						int netPayAmt=(int) newnetpay;
						newnetpay=netPayAmt;
						form.getDonetpayamt().setValue(Double.parseDouble(nf.format(newnetpay)));
					}
				}
				
				/**
				 * Date : 23-03-2018 By VIJAY for other charges calculation
				 */
				if(event.getSource().equals(form.tblOtherCharges.getTable())){
					form.prodTaxTable.connectToLocal();
					try {
						form.addProdTaxes();
						form.addOtherChargesInTaxTbl();
						form.dbOtherChargesTotal.setValue(form.tblOtherCharges.calculateOtherChargesSum());
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					if(form.getDoamtincltax().getValue()!=null){
						double totalIncludingTax=form.getDototalamt().getValue()+form.getProdTaxTable().calculateTotalTaxes();
						totalIncludingTax=totalIncludingTax+form.dbOtherChargesTotal.getValue();
						form.getDoamtincltax().setValue(Double.parseDouble(nf.format(totalIncludingTax)));
						form.updateChargesTable();
					}else{
						double totalIncludingTax=form.getProdTaxTable().calculateTotalTaxes();
						totalIncludingTax=totalIncludingTax+form.dbOtherChargesTotal.getValue();
						form.getDoamtincltax().setValue(Double.parseDouble(nf.format(totalIncludingTax)));
						form.updateChargesTable();
					}
					
					double netPay=fillNetPayable(form.getDoamtincltax().getValue());
					netPay=Math.round(netPay);
					int netPayable=(int) netPay;
					netPay=netPayable;
//					form.getDonetpayamt().setValue(Double.parseDouble(nf.format(netPay)));
					
					
					/**
					 * Date 08/06/2018
					 * Developer :- Vijay
					 * Des :- for final total amt Round off amt calculations
					 */
					form.getDbfinalTotalAmt().setValue(Double.parseDouble(nf.format(netPay)));
					
					if(form.getTbroundoffAmt().getValue()!=null && !form.getTbroundoffAmt().getValue().equals("")){
						String roundOff = form.getTbroundoffAmt().getValue();
						double roundoffAmt =  AppUtility.calculateRoundOff(roundOff, netPay);
						if(roundoffAmt!=0){
							form.getDonetpayamt().setValue(roundoffAmt);
						}else{
							form.getDonetpayamt().setValue(Double.parseDouble(nf.format(netPay)));
							form.getTbroundoffAmt().setValue("");
						}
					}else{
						form.getDonetpayamt().setValue(Double.parseDouble(nf.format(netPay)));

					}
					/**
					 * ends here
					 */
					
					String approverName="";
					if(form.olbApproverName.getValue()!=null){
						approverName=form.olbApproverName.getValue();
					}
					AppUtility.getApproverListAsPerTotalAmount(form.olbApproverName, "Sales Order", approverName, form.getDonetpayamt().getValue());
				}
				/**
				 * End
				 */
			}
			
			public void reactOnEmail()
			{
//				boolean conf = Window.confirm("Do you really want to send email?");
//				if (conf == true) {
//				emailService.initiateSalesQuotationEmail((Sales) model,new AsyncCallback<Void>() {
//
//					@Override
//					public void onFailure(Throwable caught) {
//						Window.alert("Resource Quota Ended ");
//						caught.printStackTrace();
//					}
//
//					@Override
//					public void onSuccess(Void result) {
//						Window.alert("Email Sent Sucessfully !");
//					}
//				});
//				}
				
				/**
				 * @author Vijay Date 19-03-2021
				 * Des :- above old code commented 
				 * Added New Email Popup Functionality
				 */
				emailpopup.showPopUp();
				setEmailPopUpData();
				
				/**
				 * ends here
				 */
			}
			
			
			private void productsOnChange()
			{
				form.showWaitSymbol();
				Timer timer=new Timer() 
			     {
					@Override
					public void run() 
					{
					   NumberFormat nf=NumberFormat.getFormat("0.00");
					   double totalExcludingTax=form.getLineitemsalesquotationtable().calculateTotalExcludingTax();
					   totalExcludingTax=Double.parseDouble(nf.format(totalExcludingTax));
					    form.getDototalamt().setValue(Double.parseDouble(nf.format(totalExcludingTax)));
					    
						boolean chkSize=form.getLineitemsalesquotationtable().removeChargesOnDelete();
						if(chkSize==false){
							form.getChargesTable().connectToLocal();
						}
						
						try {
							form.prodTaxTable.connectToLocal();
							form.addProdTaxes();
							
							/**
							 * Date : 23-0302018 By VIJAY
							 * This method is added to calculate other charges and tax on it.
							 */
							form.addOtherChargesInTaxTbl();
							form.dbOtherChargesTotal.setValue(form.tblOtherCharges.calculateOtherChargesSum());
							/**
							 * End
							 */
							
							double totalIncludingTax=form.getDototalamt().getValue()+form.getProdTaxTable().calculateTotalTaxes();
							
							/**
							 * Date : 20-09-2017 By VIJAY
							 */
							totalIncludingTax=totalIncludingTax+form.dbOtherChargesTotal.getValue();
							/**
							 * End
							 */
							
							form.getDoamtincltax().setValue(Double.parseDouble(nf.format(totalIncludingTax)));
							form.updateChargesTable();
						} catch (Exception e) {
							e.printStackTrace();
						}
						double netPay=fillNetPayable(form.getDoamtincltax().getValue());
						netPay=Math.round(netPay);
						int netPayable=(int) netPay;
						netPay=netPayable;
//						form.getDonetpayamt().setValue(Double.parseDouble(nf.format(netPay)));
						
						/**
						 * Date 08/06/2018
						 * Developer :- Vijay
						 * Des :- for final total amt Round off amt calculation
						 */
						form.getDbfinalTotalAmt().setValue(Double.parseDouble(nf.format(netPay)));
						
						if(form.getTbroundoffAmt().getValue()!=null && !form.getTbroundoffAmt().getValue().equals("")){
							String roundOff = form.getTbroundoffAmt().getValue();
							double roundoffAmt =  AppUtility.calculateRoundOff(roundOff, netPay);
							if(roundoffAmt!=0){
								form.getDonetpayamt().setValue(roundoffAmt);
							}else{
								form.getDonetpayamt().setValue(Double.parseDouble(nf.format(netPay)));
								form.getTbroundoffAmt().setValue("");
							}
						}else{
							form.getDonetpayamt().setValue(Double.parseDouble(nf.format(netPay)));

						}
						/**
						 * ends here
						 */
						
						String approverName="";
						if(form.olbApproverName.getValue()!=null){
							approverName=form.olbApproverName.getValue();
						}
						AppUtility.getApproverListAsPerTotalAmount(form.olbApproverName, "Sales Quotation", approverName, form.getDonetpayamt().getValue());
						form.hideWaitSymbol();
					}
		    	 };
		    	 timer.schedule(3000);
			}
			
			
			/**
			 *This method is called on row change event of salesline items.
			 *If the charges table is empty or filled the net payable amount is calculated and returned.
			 *It takes the parameter as totalamount including tax. 
			 */
			private double fillNetPayable(double amtincltax)
			{
				double amtval=0;
				if(form.getChargesTable().getDataprovider().getList().size()==0)
				{
					amtval=amtincltax;
				}
				if(form.getChargesTable().getDataprovider().getList().size()!=0)
				{
					amtval=amtincltax+form.getChargesTable().calculateNetPayable();
				}
				amtval=Math.round(amtval);
				int retAmtVal=(int) amtval;
				amtval=retAmtVal;
				return amtval;
			}

			@Override
			public void onClick(ClickEvent event) {
				super.onClick(event);
				
				
				if(event.getSource().equals(conditionPopupforPreprint.getBtnOne()))
				{
					
					if(cnt >0){
						System.out.println("in side one yes");
						
						final String url = GWT.getModuleBaseURL() + "pdfsales"+"?Id="+model.getId()+"&"+"type="+"sq"+"&"+"preprint="+"yes";
						 Window.open(url, "test", "enabled");
						 
						 panel.hide();
					}
				}
				
				
				if(event.getSource().equals(conditionPopupforPreprint.getBtnTwo()))
				{
					if(cnt >0){
						System.out.println("inside two no");
						
						final String url = GWT.getModuleBaseURL() + "pdfsales"+"?Id="+model.getId()+"&"+"type="+"sq"+"&"+"preprint="+"no";
						 Window.open(url, "test", "enabled");
						 
							panel.hide();
					}
				}
				
				
				
				/**
				 *  nidhi
				 *   2-08-2017
				 */
				if(event.getSource().equals(form.getCbCheckAddress())){
//					if(!form.getPersonInfoComposite().getId().getValue().equals("")&&form.getLineitemsalesquotationtable().getDataprovider().getList().size()>0)
//					{
//						panel=new PopupPanel(true);
//						panel.add(conditionPopup);
//						panel.setGlassEnabled(true);
//						panel.show();
//						panel.center();
//					}
					
					if(!form.getPersonInfoComposite().getId().getValue().equals(""))
					{
						if(form.getCbCheckAddress().getValue()==false)
						{
							form.getAcshippingcomposite().setEnable(false);
							try {
								retrieveShippingAddress();
							} 
							catch (Exception e) {
								e.printStackTrace();
							}
						}
						
						if(form.getCbCheckAddress().getValue()==true)
						{
							form.getAcshippingcomposite().setEnable(true);
							form.getAcshippingcomposite().getAdressline1().setValue(null);
							form.getAcshippingcomposite().getAdressline2().setValue(null);
							form.getAcshippingcomposite().getLocality().setSelectedIndex(0);
							form.getAcshippingcomposite().getLandMark().setText(null);
							form.getAcshippingcomposite().getPin().setText(null);
							form.getAcshippingcomposite().getCity().setSelectedIndex(0);
							form.getAcshippingcomposite().getCountry().setSelectedIndex(0);
							form.getAcshippingcomposite().getState().setSelectedIndex(0);
						}
					}
				}
				
				if(event.getSource()==form.btnsecurityDep){
					reactToSecurityDep();
				 }
				
				if(event.getSource()==securitydeposit.getBtnOk()){
					setPopupValueToFields();
				}
				
				if(event.getSource()==securitydeposit.getBtnCancel()){
					clearProductsAndAddress();
					panel.hide();
				}
				
				if(event.getSource().equals(conditionPopup.getBtnOne())&&form.getCbCheckAddress().getValue()==true)
				{
					System.out.println("True One");
					clearProductsAndAddress();
						panel1.hide();
				}
				
				if(event.getSource().equals(conditionPopup.getBtnOne())&&form.getCbCheckAddress().getValue()==false)
				{
					System.out.println("False One");
					
					if(!form.getPersonInfoComposite().getId().getValue().equals("")){
							try {
								
								retrieveShippingAddress();
							} catch (Exception e) {
								e.printStackTrace();
							}
					}
//					clearProductsAndAddress();
					form.getProdTaxTable().connectToLocal();
					form.getChargesTable().connectToLocal();
					form.getLineitemsalesquotationtable().connectToLocal();
					form.getDoamtincltax().setValue(null);
					form.getDonetpayamt().setValue(null);
					form.getDototalamt().setValue(null);
					form.getCbcformlis().setSelectedIndex(0);
					form.getOlbcstpercent().setSelectedIndex(0);
					form.getCbcformlis().setEnabled(false);
					form.getOlbcstpercent().setEnabled(false);
					form.getAcshippingcomposite().setEnable(false);
					form.getAcshippingcomposite().getAdressline1().setValue(null);
					form.getAcshippingcomposite().getAdressline2().setValue(null);
					form.getAcshippingcomposite().getLocality().setSelectedIndex(0);
					form.getAcshippingcomposite().getLandMark().setText(null);
					form.getAcshippingcomposite().getPin().setText(null);
					form.getAcshippingcomposite().getCity().setSelectedIndex(0);
					form.getAcshippingcomposite().getCountry().setSelectedIndex(0);
					form.getAcshippingcomposite().getState().setSelectedIndex(0);
					panel1.hide();
				}
				
				if(event.getSource().equals(conditionPopup.getBtnTwo()))
				{
					boolean flag=false;
					if(form.getCbCheckAddress().getValue()==true)
					{
						System.out.println("Two True");
						form.getAcshippingcomposite().setEnable(false);
								try {
									retrieveShippingAddress();
									flag=true;
									form.getCbCheckAddress().setValue(false);
								} catch (Exception e) {
									e.printStackTrace();
								}
						
						panel1.hide();
					}
					
					if(form.getCbCheckAddress().getValue()==false&&flag==false)
					{
						System.out.println("Two False");
						form.getAcshippingcomposite().setEnable(true);
						form.getAcshippingcomposite().getAdressline1().setValue(null);
						form.getAcshippingcomposite().getAdressline2().setValue(null);
						form.getAcshippingcomposite().getLocality().setSelectedIndex(0);
						form.getAcshippingcomposite().getLandMark().setText(null);
						form.getAcshippingcomposite().getPin().setText(null);
						form.getAcshippingcomposite().getCity().setSelectedIndex(0);
						form.getAcshippingcomposite().getCountry().setSelectedIndex(0);
						form.getAcshippingcomposite().getState().setSelectedIndex(0);
						form.getCbCheckAddress().setValue(true);
						panel1.hide();
					}
				}
				
				/** added by Nidhi for communication log
				 * 29-06-2017
				 */
				if(event.getSource() == communicationLogPopUp.getBtnOk()){
					form.showWaitSymbol();
				    List<InteractionType> list = communicationLogPopUp.getCommunicationLogTable().getDataprovider().getList();
				   
				    ArrayList<InteractionType> interactionlist = new ArrayList<InteractionType>();
				    interactionlist.addAll(list);
				    
				    boolean checkNewInteraction = AppUtility.checkNewInteractionAdded(interactionlist);
				    
				    if(checkNewInteraction==false){
				    	form.showDialogMessage("Please add new interaction details");
				    	form.hideWaitSymbol();
				    }	
				    else{	
				    	
				    	
				    	/**
				    	 * Date : 09/03/2018 komal for followup date
				    	 */
				    	final Date followUpDate = interactionlist.get(interactionlist.size()-1).getInteractionDueDate();
				    	 communicationService.saveCommunicationLog(interactionlist, new AsyncCallback<Void>() {

								@Override
								public void onFailure(Throwable caught) {
									form.showDialogMessage("Unexpected Error");
									form.hideWaitSymbol();
									LoginPresenter.communicationLogPanel.hide();
								}

								@Override
								public void onSuccess(Void result) {
									form.showDialogMessage("Data Save Successfully");
									/**
							    	 * Date : 09/03/2018 komal for followup date
							    	 */
									model.setFollowUpDate(followUpDate);
									form.getDbFollowUpDate().setValue(followUpDate);
									form.hideWaitSymbol();
									LoginPresenter.communicationLogPanel.hide();
								}
							});
				    
				    }
				   
				}
				if(event.getSource() == communicationLogPopUp.getBtnCancel()){
//					communicationPanel.hide();
					LoginPresenter.communicationLogPanel.hide();
				}
				if(event.getSource() == communicationLogPopUp.getBtnAdd()){
						form.showWaitSymbol();
						String remark = communicationLogPopUp.getRemark().getValue();
						Date dueDate = communicationLogPopUp.getDueDate().getValue();
						String interactiongGroup =null;
						if(communicationLogPopUp.getOblinteractionGroup().getSelectedIndex()!=0)
							interactiongGroup=communicationLogPopUp.getOblinteractionGroup().getValue(communicationLogPopUp.getOblinteractionGroup().getSelectedIndex());
						boolean validationFlag = AppUtility.validateCommunicationlog(remark,dueDate);
						if(validationFlag){
							InteractionType communicationLog =  AppUtility.getCommunicationLog(AppConstants.SALESMODULE,AppConstants.QUOTATION,model.getCount(),model.getEmployee(),remark,dueDate,model.getCinfo(),null,interactiongGroup, model.getBranch(),"");
							communicationLogPopUp.getCommunicationLogTable().getDataprovider().getList().add(communicationLog);
							communicationLogPopUp.getRemark().setValue("");
							communicationLogPopUp.getDueDate().setValue(null);
							communicationLogPopUp.getOblinteractionGroup().setSelectedIndex(0);
						}
						form.hideWaitSymbol();
				}
				
				/**ends here
				 */
				
				/** Manisha added this for cancellation popup on 11/11/2017**/

				if (event.getSource() == cancelpopup.getBtnOk()) {
					cancelpanel.hide();
					cancelDocument(AppConstants.CANCELLED);
				}
				if (event.getSource() == cancelpopup.getBtnCancel()) {
					cancelpanel.hide();
					cancelpopup.remark.setValue("");
				}
				
				/*
				 * nidhi
				 * 1-08-2018
				 * for  unsucessfull message
				 */
				if (event.getSource() == unSucessfullPopup.getBtnOk()) {
					cancelpanel.hide();
					cancelDocument(AppConstants.Unsucessful);
				}
		
				if (event.getSource() == unSucessfullPopup.getBtnCancel()) {
					cancelpanel.hide();
					unSucessfullPopup.remark.setValue("");
				}/**
				end*/
				
				if(event.getSource()==lblUpdate){
					customerAddressEdiatable();
				}
				
				if(event.getSource()==custAddresspopup.getLblOk()){
					saveCustomerAddress();
				}
				if(event.getSource()==custAddresspopup.getLblCancel()){
					custAddresspopup.hidePopUp();
					cancelpanel.hide();
				}
				if(event.getSource()==custAddresspopup.getBtnbillingaddr()){
					custAddresspopup.getServiceAddressComposite().setValue(custAddresspopup.getBillingAddressComposite().getValue());
				}
				
			}
			
			private void saveCustomerAddress() {
				if(custAddresspopup.getBillingAddressComposite().getValue()!=null) {
					model.setCustBillingAddress(custAddresspopup.getBillingAddressComposite().getValue());
				}
				if(custAddresspopup.getServiceAddressComposite().getValue()!=null) {
					model.setShippingAddress(custAddresspopup.getServiceAddressComposite().getValue());
				}
				
				genasync.save(model, new AsyncCallback<ReturnFromServer>() {
					
					@Override
					public void onSuccess(ReturnFromServer arg0) {
						// TODO Auto-generated method stub
						custAddresspopup.hidePopUp();
						if(custAddresspopup.getServiceAddressComposite().getValue()!=null) {
							form.getAcshippingcomposite().setValue(custAddresspopup.getServiceAddressComposite().getValue());
						}
					}
					
					
					@Override
					public void onFailure(Throwable arg0) {
						// TODO Auto-generated method stub
						custAddresspopup.hidePopUp();
					}
				});
				
			}

			private void customerAddressEdiatable() {
				// TODO Auto-generated method stub
				custAddresspopup.getBillingAddressComposite().setEnable(true);
				custAddresspopup.getServiceAddressComposite().setEnable(true);
				
			}
			
				protected void clearProductsAndAddress()
				{
//					form.getProdTaxTable().connectToLocal();
//					form.getChargesTable().connectToLocal();
//					form.getLineitemsalesquotationtable().connectToLocal();
//					form.getDoamtincltax().setValue(null);
//					form.getDonetpayamt().setValue(null);
//					form.getDototalamt().setValue(null);
//					form.getCbcformlis().setSelectedIndex(0);
//					form.getOlbcstpercent().setSelectedIndex(0);
					form.getCbcformlis().setEnabled(false);
					form.getOlbcstpercent().setEnabled(false);
					form.getAcshippingcomposite().setEnable(true);
					form.getAcshippingcomposite().getAdressline1().setValue(null);
					form.getAcshippingcomposite().getAdressline2().setValue(null);
					form.getAcshippingcomposite().getLocality().setSelectedIndex(0);
					form.getAcshippingcomposite().getLandMark().setText(null);
					form.getAcshippingcomposite().getPin().setText(null);
					form.getAcshippingcomposite().getCity().setSelectedIndex(0);
					form.getAcshippingcomposite().getCountry().setSelectedIndex(0);
					form.getAcshippingcomposite().getState().setSelectedIndex(0);
				}

			private void retrieveShippingAddress() throws Exception
			{
				form.showWaitSymbol();
				 Timer timer=new Timer() 
			     {
					@Override
					public void run() 
					{
						MyQuerry querry=new MyQuerry();
						Vector<Filter> filtervec=new Vector<Filter>();
						Filter tempfilter=new Filter();
						tempfilter=new Filter();
						tempfilter.setQuerryString("count");
						int custId=Integer.parseInt(form.getPersonInfoComposite().getId().getValue());
						tempfilter.setIntValue(custId);
						filtervec.add(tempfilter);
						tempfilter=new Filter();
						tempfilter.setQuerryString("companyId");
						tempfilter.setLongValue(model.getCompanyId());
						filtervec.add(tempfilter);
						querry.setFilters(filtervec);
						querry.setQuerryObject(new Customer());
						genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
							
							@Override
							public void onFailure(Throwable caught) {
								form.hideWaitSymbol();
								form.showDialogMessage("An Unexpected error occurred!");
							}
				
							@Override
							public void onSuccess(ArrayList<SuperModel> result) {
								for(SuperModel model:result)
								{
									Customer custentity = (Customer)model;
									form.getAcshippingcomposite().setValue(custentity.getSecondaryAdress());
									
									if(!checkCompanyState()){
										form.getCbcformlis().setEnabled(true);
									}
									else{
										form.getCbcformlis().setEnabled(false);
										form.getOlbcstpercent().setEnabled(false);
										form.getOlbcstpercent().setSelectedIndex(0);
									}
									form.hideWaitSymbol();
								}
								}
							 });
						form.hideWaitSymbol();
					}
		    	 };
		    	 timer.schedule(3000);			
		    	 form.hideWaitSymbol();
			}


			@Override
			public void onSelection(SelectionEvent<Suggestion> event) {
				
				
				if(event.getSource().equals(form.getPersonInfoComposite().getId())||event.getSource().equals(form.getPersonInfoComposite().getName())||event.getSource().equals(form.getPersonInfoComposite().getPhone())){
					form.getCbCheckAddress().setValue(false);
					form.checkCustomerStatus(form.getPersonInfoComposite().getIdValue());
					try {
						retrieveShippingAddress();
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					AppUtility.makeCustomerBranchLive(form.olbcustBranch,form.getPersonInfoComposite().getIdValue());

				}

//				if(event.getSource()==form.getPersonInfoComposite().getId()){
//					if(!form.getPersonInfoComposite().getId().getValue().equals("")){
//						try {
//							retrieveShippingAddress();
//						} catch (Exception e) {
//							e.printStackTrace();
//						}
//					}
//				}
//				
//				if(event.getSource()==form.getPersonInfoComposite().getName()){
//					if(!form.getPersonInfoComposite().getName().getValue().equals("")){
//						try {
//							retrieveShippingAddress();
//						} catch (Exception e) {
//							e.printStackTrace();
//						}
//					}
//				}
//				
//				if(event.getSource()==form.getPersonInfoComposite().getPhone()){
//					if(!form.getPersonInfoComposite().getPhone().getValue().equals("")){
//						try {
//							retrieveShippingAddress();
//						} catch (Exception e) {
//							e.printStackTrace();
//						}
//					}
//				}
			}

			@Override
			public void onChange(ChangeEvent event) {
				
				/**************************************************************************************/
				if(event.getSource().equals(form.getCbcformlis()))
				{
					if(form.cbcformlis.getSelectedIndex()!=0){
						int index=form.cbcformlis.getSelectedIndex();
						if(AppConstants.YES.equals(form.cbcformlis.getItemText(index).trim())){
							form.getOlbcstpercent().setEnabled(true);
						}
						if(AppConstants.NO.equals(form.cbcformlis.getItemText(index).trim())){
							productsOnChange();
							form.getOlbcstpercent().setEnabled(false);
							form.getOlbcstpercent().setSelectedIndex(0);
						}
					}
					if(form.getCbcformlis().getSelectedIndex()==0){
						form.getOlbcstpercent().setSelectedIndex(0);
						form.getOlbcstpercent().setEnabled(false);
						productsOnChange();
					}
				}
				
				/**************************************************************************************/
				
				if(event.getSource().equals(form.getOlbcstpercent())){
					if(form.getOlbcstpercent().getSelectedIndex()!=0){
						productsOnChange();
					}
				}
				
				/**************************************************************************************/
				
				if(event.getSource().equals(form.getAcshippingcomposite().getState())){
					if(!checkCompanyState()&&form.getAcshippingcomposite().getState().getSelectedIndex()!=0)
					{
						form.getCbcformlis().setEnabled(true);
					}
					if(checkCompanyState()&&form.getAcshippingcomposite().getState().getSelectedIndex()!=0){
						form.getCbcformlis().setEnabled(false);
					}
					if(form.getAcshippingcomposite().getState().getSelectedIndex()==0){
						form.getCbcformlis().setEnabled(false);
						form.getOlbcstpercent().setEnabled(false);
						form.getCbcformlis().setSelectedIndex(0);
						form.getOlbcstpercent().setValue(null);
					}
				}
				
				if(event.getSource().equals(form.olbcustBranch)) {
					if(form.olbcustBranch.getSelectedIndex()==0) {
						try {
							retrieveCustomerShippingAddress();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					else {
						CustomerBranchDetails branchdetails = form.olbcustBranch.getSelectedItem();
						if(branchdetails!=null) {
							if(branchdetails.getAddress()!=null) {
								form.acshippingcomposite.setValue(branchdetails.getAddress());
							}
							if(branchdetails.getBillingAddress()!=null){
							form.customerBillingAddress = branchdetails.getBillingAddress();
							}
						}
					}
				}
			}

		

			/**
			 *   This method returns the name of state in which the company is located.
			 *   It is retrieved form Company address in Company Entity
			 * @return  It returns company state
			 */
			
			public String retrieveCompanyState()
			{
				final MyQuerry querry=new MyQuerry();
				Filter tempfilter=new Filter();
				tempfilter.setQuerryString("companyId");
				tempfilter.setLongValue(model.getCompanyId());
				querry.getFilters().add(tempfilter);
				querry.setQuerryObject(new Company());
				Timer timer=new Timer() 
			    {
					@Override
					public void run() 
					{
					genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
						
						@Override
						public void onFailure(Throwable caught) {
							form.showDialogMessage("An Unexpected error occurred!");
						}
			
						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							for(SuperModel model:result)
							{
								Company compentity = (Company)model;
								retrCompnyState=compentity.getAddress().getState();
								System.out.println("Retrieved "+retrCompnyState);
							}
							companyState=retrCompnyState.trim();
							System.out.println("sysouStatc"+companyState);
							}
						 });
					}
		    	 };
		    	 timer.schedule(3000);
		    	 
		    	return retrCompnyState;
			}

			/**
			 * This method is used for verifying whether the name of the state where delivery is to be done
			 * is same as that of company state.
			 * @return it returns true if company state is same as that of customer and false if it is different.
			 */
			public boolean checkCompanyState()
			{
				//String companyState=retrieveCompanyState().trim();
				System.out.println("Company State"+retrCompnyState);
				
				String custState="";
				if(form.getAcshippingcomposite().getState().getSelectedIndex()!=0){
					custState=form.getAcshippingcomposite().getState().getValue().trim();
					System.out.println("Customer State"+custState);
				}
				if(!custState.equals("")&&custState.trim().equals(retrCompnyState.trim())&&!retrCompnyState.equals("")){
					return true;
				}
				return false;
			}


			private void reactToSecurityDep() {

				System.out.println("Inside Click ......................");
				System.out.println(form.tbQuotationStatus);
				if(form.tbQuotatinId.getValue()==null){
					securitydeposit.getP_docid().setValue("");
				}else{
				securitydeposit.getP_docid().setValue(form.tbQuotatinId.getValue()+"");
				}
				securitydeposit.getP_docstatus().setValue(form.tbQuotationStatus.getValue());
				
				panel=new PopupPanel(true);
				
				ScreeenState TransactionStatus=AppMemory.getAppMemory().currentState;
				System.out.println("Status Mode"+TransactionStatus.toString());
				if(TransactionStatus.toString().equals("VIEW")){
					System.out.println("IN if loop.....................................................................");
					
					securitydeposit.getP_securityDeposite().setEnabled(false);
					securitydeposit.getP_depstatus().setEnabled(false);
					securitydeposit.getP_reDate().setEnabled(false);
					securitydeposit.getP_bankName().setEnabled(false);
					securitydeposit.getP_branch().setEnabled(false);
					securitydeposit.getP_paybleAt().setEnabled(false);
					securitydeposit.getP_favouring().setEnabled(false);
//					securitydeposit.getP_checkName().setEnabled(false);
					securitydeposit.getP_checknumber().setEnabled(false);
					securitydeposit.getP_chekDate().setEnabled(false);
					securitydeposit.getP_servisNo().setEnabled(false);
					securitydeposit.getP_address().setEnable(false);
					securitydeposit.getP_accountNo().setEnabled(false);
					securitydeposit.getP_ifscode().setEnabled(false);
					securitydeposit.getP_micrCode().setEnabled(false);
					securitydeposit.getP_swiftcode().setEnabled(false);
					securitydeposit.getP_tainstruct().setEnabled(false);
					securitydeposit.getP_paymethod().setEnabled(false);
					
				}
				
				if(TransactionStatus.toString().equals("EDIT")){
					System.out.println("IN if loop.....................................................................");
					securitydeposit.getP_securityDeposite().setEnabled(true);
					securitydeposit.getP_depstatus().setEnabled(false);
					securitydeposit.getP_reDate().setEnabled(false);
					securitydeposit.getP_bankName().setEnabled(true);
					securitydeposit.getP_branch().setEnabled(true);
					securitydeposit.getP_paybleAt().setEnabled(true);
					securitydeposit.getP_favouring().setEnabled(true);
//					securitydeposit.getP_checkName().setEnabled(true);
					securitydeposit.getP_checknumber().setEnabled(true);
					securitydeposit.getP_chekDate().setEnabled(true);
					 securitydeposit.getP_servisNo().setEnabled(true);
					 securitydeposit.getP_address().setEnable(true);
					 securitydeposit.getP_accountNo().setEnabled(true);
					 securitydeposit.getP_ifscode().setEnabled(true);
					 securitydeposit.getP_micrCode().setEnabled(true);
					 securitydeposit.getP_swiftcode().setEnabled(true);
					 securitydeposit.getP_tainstruct().setEnabled(true);
					 securitydeposit.getP_paymethod().setEnabled(true);
					 
				}
				recallfeildonpupinedit();
				panel.add(securitydeposit);
				panel.show();
				panel.center();
				
				System.out.println(TransactionStatus);
				
				
			}

			private void recallfeildonpupinedit() {

				
				securitydeposit.getP_bankName().setValue(form.f_bankName);
				securitydeposit.getP_branch().setValue(form.f_branch);
				securitydeposit.getP_securityDeposite().setValue(form.f_securityDeposit);
				securitydeposit.getP_reDate().setValue(form.f_reDate);
				securitydeposit.getP_depstatus().setValue(form.f_depositStatus);
				
				if(form.f_payMethod!=null){
				securitydeposit.getP_paymethod().setValue(form.f_payMethod);
				}			
				
				
				securitydeposit.getP_accountNo().setValue(form.f_accountNo);
				securitydeposit.getP_paybleAt().setValue(form.f_paybleAt);
				securitydeposit.getP_favouring().setValue(form.f_fouring);
//				securitydeposit.getP_checkName().setValue(form.f_checkName);
				
				securitydeposit.getP_checknumber().setValue(form.f_checkNo+"");
				securitydeposit.getP_chekDate().setValue(form.f_checkDate);
				
				securitydeposit.getP_servisNo().setValue(form.f_serviceNo);
				securitydeposit.getP_ifscode().setValue(form.f_ifscCode);
				securitydeposit.getP_micrCode().setValue(form.f_micrCode);
				securitydeposit.getP_swiftcode().setValue(form.f_swiftCode);
				
				if(form.f_address!=null){
				securitydeposit.getP_address().setValue(form.f_address);
				}
				securitydeposit.getP_tainstruct().setValue(form.f_instruction);
				
			}

			private void setPopupValueToFields() {

				if(securitydeposit.getP_securityDeposite().getValue()!=null){
				form.setF_securityDeposit(securitydeposit.getP_securityDeposite().getValue());
			}else{
				double i=0;
				form.setF_securityDeposit(i);
			}
			if(securitydeposit.getP_reDate().getValue()!=null){
				form.setF_reDate(securitydeposit.getP_reDate().getValue());
			}
			if(securitydeposit.getP_depstatus().getValue()!=false){
				form.setF_depositStatus(securitydeposit.getP_depstatus().getValue());
			}else{
				form.setF_depositStatus(false);
			}
			if(securitydeposit.getP_paymethod().getSelectedIndex()!=0){
				form.setF_payMethod(securitydeposit.getP_paymethod().getValue());
			}else{
				form.setF_payMethod(null);
			}
			
			
			if(securitydeposit.getP_accountNo().getValue()!=null){
				
				form.setF_accountNo(securitydeposit.getP_accountNo().getValue());
			}
			
			if(securitydeposit.getP_bankName().getValue()!=null){
				
				form.setF_bankName(securitydeposit.getP_bankName().getValue());
			}
			if(securitydeposit.getP_branch().getValue()!=null){
				form.setF_branch(securitydeposit.getP_branch().getValue());
			}
			if(securitydeposit.getP_paybleAt().getValue()!=null){
				form.setF_paybleAt(securitydeposit.getP_paybleAt().getValue());
			}
			if(securitydeposit.getP_favouring().getValue()!=null){
				form.setF_fouring(securitydeposit.getP_favouring().getValue());
			}
//			if(securitydeposit.getP_checkName().getValue()!=null){
//				form.setF_checkName(securitydeposit.getP_checkName().getValue());
//			}
			
				
			if(securitydeposit.getP_checknumber().getValue()==null){		
					form.setF_checkNo(0);
				}
			
				
				if(!securitydeposit.getP_checknumber().getValue().equals("")){
				form.setF_checkNo(Integer.parseInt(securitydeposit.getP_checknumber().getValue()));
			}
			if(securitydeposit.getP_chekDate().getValue()!=null){
				form.setF_checkDate(securitydeposit.getP_chekDate().getValue());
			}
				
			if(securitydeposit.getP_servisNo().getValue()==null){
					form.setF_serviceNo("");
				}
				if(securitydeposit.getP_servisNo().getValue()!=null){
				
				form.setF_serviceNo(securitydeposit.getP_servisNo().getValue());
			}
			
			if(securitydeposit.getP_address().getValue()!=null){
				form.setF_address(securitydeposit.getP_address().getValue());
			}
			
			if(securitydeposit.getP_ifscode().getValue()!=null){
				form.setF_ifscCode(securitydeposit.getP_ifscode().getValue());
			}
			if(securitydeposit.getP_micrCode().getValue()!=null){
				form.setF_micrCode(securitydeposit.getP_micrCode().getValue());
			}
			if(securitydeposit.getP_swiftcode().getValue()!=null){
				form.setF_swiftCode(securitydeposit.getP_swiftcode().getValue());
			}
			if(securitydeposit.getP_tainstruct().getValue()!=null){
				form.setF_instruction(securitydeposit.getP_tainstruct().getValue());
			}
			panel.hide();
				
			}
			
			@Override
			public void reactOnCommunicationLog() {

				form.showWaitSymbol();
				MyQuerry querry = AppUtility.communicationLogQuerry(model.getCount(), model.getCompanyId(),AppConstants.SALESMODULE,AppConstants.QUOTATION);
				
				genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
					
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						
						ArrayList<InteractionType> list = new ArrayList<InteractionType>();
						
						for(SuperModel model : result){
							
							InteractionType interactionType = (InteractionType) model;
							list.add(interactionType);
							
						}
						/**
						 * Date : 09-03-2018 BY Komal for followup date
						 */
						Comparator<InteractionType> comp=new Comparator<InteractionType>() {
							@Override
							public int compare(InteractionType e1, InteractionType e2) {
//								Integer coutn1=o1.getCount();
//								Integer count2=o2.getCount();
//								return ;
								if (e1.getCount() == e2.getCount()) {
									return 0;
								}
								if (e1.getCount() > e2.getCount()) {
									return 1;
								} else {
									return -1;
								}
							}
						};
						Collections.sort(list,comp);
						form.hideWaitSymbol();
						communicationLogPopUp.getRemark().setValue("");
						communicationLogPopUp.getDueDate().setValue(null);
						communicationLogPopUp.getOblinteractionGroup().setSelectedIndex(0);
						communicationLogPopUp.getCommunicationLogTable().getDataprovider().setList(list);
//						communicationPanel = new PopupPanel(true);
//						communicationPanel.add(CommunicationLogPopUp);
//						communicationPanel.show();
//						communicationPanel.center();
						
						LoginPresenter.communicationLogPanel = new PopupPanel(true);
						LoginPresenter.communicationLogPanel.add(communicationLogPopUp);
						LoginPresenter.communicationLogPanel.show();
						LoginPresenter.communicationLogPanel.center();
					}
					
					@Override
					public void onFailure(Throwable caught) {
						form.hideWaitSymbol();
					}
				});
				
			}
			
			/**
			 * date 07/11/2017 added by komal to add functionality revised quotation
			 * button
			 **/
			private void reactOnRevisedQuotation() {
				try {
					System.out.println("get id : " +model.getCount());
					SalesQuotation quo = (SalesQuotation)model;
					final SalesQuotation salesQuotation=(SalesQuotation)getRevisedQuotationData(model);
					salesQuotation.setCount(0);
					salesQuotation.setContractCount(0);
					salesQuotation.setQuotationCount(0);
					salesQuotation.setQuotationDate(null);
					salesQuotation.setApprovalDate(null);
					salesQuotation.setCreationDate(null);
					salesQuotation.setValidUntill(null);
					salesQuotation.setStatus(SalesQuotation.CREATED);
					salesQuotation.setId(null);
					
					System.out.println("get id after : " +salesQuotation.getCount()+ " ID "+salesQuotation.getId() + " get value model  : "+ model.getCount() +" model Id ;;;; -- "+model.getId());
					AppMemory.getAppMemory().currentState=ScreeenState.NEW;
					form.setToNewState();
					 
					Timer timer = new Timer() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							
							/** Date 11-08-2018 by Vijay added for revised old quotation status change ***/
							salesQuotation.setOldQuotationId(model.getCount());
							
							form.updateView(salesQuotation);
							form.setToEditState();
							form.toggleAppHeaderBarMenu();	
							form.getTbQuotatinId().setValue("");
							form.getTbContractId().setValue("");
							form.getTbQuotatinId().setValue("");
							form.getDbquotationDate().setValue(null);
							form.getDbValidUntill().setValue(null);
							form.getDbdeliverydate().setValue(null);
						}
					};
					timer.schedule(2000);
				
				} 
				catch (Exception e) {
					System.out.println(e.getMessage());
				}
			}
			
			private SalesQuotation getRevisedQuotationData(SalesQuotation model){
				SalesQuotation salesQuotation = new SalesQuotation();
				
				salesQuotation.setCategory(model.getCategory());
				salesQuotation.setCinfo(model.getCinfo());
				salesQuotation.setCreatedBy(model.getCreatedBy());
				salesQuotation.setTotalAmount(model.getTotalAmount());
				salesQuotation.setSdRemark(model.getSdRemark());
				salesQuotation.setCustomerCellNumber(model.getCustomerCellNumber());
				salesQuotation.setCustomerFullName(model.getCustomerFullName());
				salesQuotation.setCstpercent(model.getCstpercent());
				salesQuotation.setCstName(model.getCstName());
				salesQuotation.setEmployee(model.getEmployee());
				salesQuotation.setSdBankAccNo(model.getSdBankAccNo());
				salesQuotation.setCategoryKey(model.getCategoryKey());
				salesQuotation.setCformstatus(model.getCformstatus());
				salesQuotation.setCreationDate(model.getCreationDate());
				salesQuotation.setCreatedBy(model.getCreatedBy());
				salesQuotation.setCreationTime(model.getCreationTime());
				salesQuotation.setCreditPeriod(model.getCreditPeriod());
				salesQuotation.setDescription(model.getDescription());
				salesQuotation.setDeleted(model.isDeleted());
				salesQuotation.setDepositReceived(model.isDepositReceived());
				salesQuotation.setValidUntill(model.getValidUntill());
				salesQuotation.setSdRemark(model.getSdRemark());
				salesQuotation.setSdDocDate(model.getSdDocDate());
				salesQuotation.setSdBankAccNo(model.getSdBankAccNo());
				salesQuotation.setSdBankBranch(model.getSdBankBranch());
				salesQuotation.setSdBankName(model.getSdBankName());
				salesQuotation.setGroup(model.getGroup());
				salesQuotation.setGroupKey(model.getGroupKey());
				salesQuotation.setBranch(model.getBranch());
				salesQuotation.setPriority(model.getPriority());
				salesQuotation.setPaymentTermsList(model.getPaymentTermsList());
				salesQuotation.setProductCharges(model.getProductCharges());
				salesQuotation.setProductTaxes(model.getProductTaxes());
				salesQuotation.setPaymentTermsList(model.getPaymentTermsList());
				salesQuotation.setItems(model.getItems());
				salesQuotation.setSdDocStatus(model.getSdDocStatus());
				salesQuotation.setTotalAmount(model.getTotalAmount());
				salesQuotation.setNetpayable(model.getNetpayable());
				salesQuotation.setSdPayableAt(model.getSdPayableAt());
				salesQuotation.setSdPaymentMethod(model.getSdPaymentMethod());
				salesQuotation.setSdReturnDate(model.getSdReturnDate());
				salesQuotation.setRemark(model.getRemark());
				salesQuotation.setShippingAddress(model.getShippingAddress());
				salesQuotation.setDocument(model.getDocument());
				salesQuotation.setInclutaxtotalAmount(model.getInclutaxtotalAmount());
				salesQuotation.setApproverName(model.getApproverName());
				salesQuotation.setPaymentMethod(model.getPaymentMethod());
				salesQuotation.setType(model.getType());
				salesQuotation.setLeadCount(model.getLeadCount());
			
				return salesQuotation;
			}

			private void cancelDocument(final String status) {
				model.setStatus(status);
				model.setDescription(model.getDescription()+"\n"+"Quotation ID ="+model.getCount()+" "+"Quotation status ="+form.getTbQuotationStatus().getValue().trim()+"\n"
						+"has been "+status+"by "+LoginPresenter.loggedInUser+" with remark "+"\n"+"Remark ="+cancelpopup.getRemark().getValue().trim()
						+" "+" Date ="+AppUtility.parseDate(new Date()));
				form.showWaitSymbol();
				/**
				 * nidhi
				 * 2227 for store reason for unsucessful 
				 */
				model.setDocStatusReason(unSucessfullPopup.getReason().getValue(unSucessfullPopup.getReason().getSelectedIndex()));
				/**
				 * end
				 */
				service.save(model, new AsyncCallback<ReturnFromServer>() {
					@Override
					public void onFailure(Throwable caught) {
						form.hideWaitSymbol();
					}

					@Override
					public void onSuccess(ReturnFromServer result) {
						form.hideWaitSymbol();
						form.getTbQuotationStatus().setValue(status);
						/*
						 * nidhi
						 * 1717
						 */
						int index = unSucessfullPopup.getReason().getSelectedIndex(); 
						String status = (index != 0 ? unSucessfullPopup.getReason().getValue(unSucessfullPopup.getReason().getSelectedIndex()) : "");
						form.getTbDocStatusreason().setValue(status);
						/**
						 * end
						 */
						form.getTaDescription().setValue(model.getDescription());
						form.setMenuAsPerStatus();
						form.showDialogMessage("Document updated sucessfully!");
					}
				});
			}

			
			private void reactOnCustomerAddress() {

				lblUpdate.getElement().setId("addbutton");
				custAddresspopup.getHorizontal().add(lblUpdate);
				custAddresspopup.showPopUp();
				if(model.getCustBillingAddress()!=null && model.getCustBillingAddress().getAddrLine1()!=null &&
						!model.getCustBillingAddress().getAddrLine1().equals("")){
					
					if(model.getCustBillingAddress()!=null) {
						custAddresspopup.getBillingAddressComposite().setValue(model.getCustBillingAddress());
						}
						if(model.getShippingAddress()!=null) {
						custAddresspopup.getServiceAddressComposite().setValue(model.getShippingAddress());
						}
						custAddresspopup.getBillingAddressComposite().setEnable(false);
						custAddresspopup.getServiceAddressComposite().setEnable(false);
				}
				else {
					Console.log("from customer");
					getCustomerAddress();
				}
				
				
			}
			
			private void retrieveCustomerShippingAddress() {

				form.showWaitSymbol();
				MyQuerry querry=new MyQuerry();
				Vector<Filter> filtervec=new Vector<Filter>();
				Filter tempfilter=new Filter();
				tempfilter=new Filter();
				tempfilter.setQuerryString("count");
				int custId=Integer.parseInt(form.getPersonInfoComposite().getId().getValue());
				tempfilter.setIntValue(custId);
				filtervec.add(tempfilter);
				querry.setFilters(filtervec);
				querry.setQuerryObject(new Customer());
				genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
					
					@Override
					public void onFailure(Throwable caught) {
						form.hideWaitSymbol();
						form.showDialogMessage("An Unexpected error occurred!");
					}
		
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						for(SuperModel mmodel:result)
						{
							Customer custentity = (Customer)mmodel;
							form.getAcshippingcomposite().setValue(custentity.getSecondaryAdress());
							if(!checkCompanyState()){
								form.getCbcformlis().setEnabled(true);
							}
							else{
								form.getCbcformlis().setEnabled(false);
								form.getOlbcstpercent().setEnabled(false);
								form.getOlbcstpercent().setSelectedIndex(0);
							}
							if(model.getId()!=null){
								if(custentity.getAdress()!=null){
								model.setCustBillingAddress(custentity.getAdress());
								form.customerBillingAddress = custentity.getAdress();
								}
							}
							form.hideWaitSymbol();
						}
						}
					 });
				form.hideWaitSymbol();
			
			}
			
			

			private void getCustomerAddress() {
				final MyQuerry querry=new MyQuerry();
				Vector<Filter> temp=new Vector<Filter>();
				Filter filter=null;
				
				filter=new Filter();
				filter.setQuerryString("count");
				filter.setIntValue(model.getCinfo().getCount());
				temp.add(filter);
				
				querry.setFilters(temp);
				querry.setQuerryObject(new Customer());
				form.showWaitSymbol();
				genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
					
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						// TODO Auto-generated method stub
						form.hideWaitSymbol();

						for(SuperModel model:result){
							
							Customer customer = (Customer)model;
							lblUpdate.getElement().setId("addbutton");
							custAddresspopup.getHorizontal().add(lblUpdate);
							custAddresspopup.showPopUp();
							custAddresspopup.getBillingAddressComposite().setValue(customer.getAdress());
							custAddresspopup.getServiceAddressComposite().setValue(customer.getSecondaryAdress());
							custAddresspopup.getBillingAddressComposite().setEnable(false);
							custAddresspopup.getServiceAddressComposite().setEnable(false);
							
						}
						
					}
					
					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						form.showDialogMessage("Unexpected Error Ouccured");
						form.hideWaitSymbol();

					}
				});
			}

					
			
			private void setEmailPopUpData() {
				form.showWaitSymbol();
				String customerEmail = "";

				String branchName = form.olbbBranch.getValue();
				String fromEmailId = AppUtility.getFromEmailAddress(branchName,model.getEmployee());
				customerEmail = AppUtility.getCustomerEmailid(model.getCinfo().getCount());
				
				String customerName = model.getCinfo().getFullName();
				if(!customerName.equals("") && !customerEmail.equals("")){
					label : AppUtility.loadContactPersonlist(emailpopup.olbContactlistEmailId,"Customer",model.getCinfo().getCount(),customerName,customerEmail,null);
					emailpopup.taToEmailId.setValue(customerEmail);

				}
				else{
			
					MyQuerry querry = AppUtility.getcustomerQuerry(model.getCinfo().getCount());
					genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
						
						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							// TODO Auto-generated method stub
							for(SuperModel smodel : result){
								Customer custEntity  = (Customer) smodel;
								System.out.println("customer Name = =="+custEntity.getFullname());
								System.out.println("Customer Email = == "+custEntity.getEmail());
								
								if(custEntity.getEmail()!=null){
									label : AppUtility.loadContactPersonlist(emailpopup.olbContactlistEmailId,"Customer",model.getCinfo().getCount(),custEntity.getFullname(),custEntity.getEmail(),null);
									emailpopup.taToEmailId.setValue(custEntity.getEmail());

								}
								else{
									label : AppUtility.loadContactPersonlist(emailpopup.olbContactlistEmailId,"Customer",model.getCinfo().getCount(),custEntity.getFullname(),custEntity.getEmail(),null);
								}
								break;
							}
						}
						
						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
						}
					});
				}
				
				Screen screenName=(Screen) AppMemory.getAppMemory().currentScreen;
				Console.log("screenName "+screenName);
				AppUtility.makeLiveSmsTemplateConfig(emailpopup.oblTemplateName,AppConstants.SALESMODULE,screenName);//LoginPresenter.currentModule.trim()
				emailpopup.taFromEmailId.setValue(fromEmailId);
				emailpopup.smodel = model;
				form.hideWaitSymbol();
				
			}
			
			
}

