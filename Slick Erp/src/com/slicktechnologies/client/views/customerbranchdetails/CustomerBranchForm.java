package com.slicktechnologies.client.views.customerbranchdetails;

import java.util.ArrayList;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.AddressComposite;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.mywidgets.EmailTextBox;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.simplesoftwares.client.library.mywidgets.PhoneNumberBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.complain.TimeFormatBox;
import com.slicktechnologies.client.views.customerbranchdetails.CustomerBranchPresenter.CustomerBranchPresenterTable;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.City;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class CustomerBranchForm extends FormTableScreen<CustomerBranchDetails>  implements ClickHandler, ChangeHandler {
	
	PersonInfoComposite personInfoComposite;
	EmailTextBox etbEmail,etbPointOfContactEmail;
	AddressComposite addressComposite;
	CheckBox checkBoxStatus;
	TextBox tbBranchName,tbPointOfContactName;
	PhoneNumberBox pnbLandlinePhone,pnbCellPhoneNo1,pnbCellPhoneNo2,pnbFaxNo,pnbPointOfContactLandline,pnbPointOfContactCell;
	/**
	 * rohan bhagde 
	 */
	ObjectListBox<Branch> olbbBranch;
	CustomerBranchDetails custBranchObj;
	
	/**
	 * Added by Rahul Verma
	 * Date: 2 Sept 2017
	 * 
	 */
	TextBox tbGSTINNumber;
	/** date 16.02.2018 added by komal for area wise billing**/
	DoubleBox dlbArea;
	ObjectListBox<Config> olbUOM;
	TextBox tbCostCenter;
	/** 
	 * end komal
	 */
	
	
	/**
	 * @author Anil , Date:22-03-2019
	 * adding billing address and button to copy address from customer master
	 */
	AddressComposite billingAddress;
	Button btnCopyCustServiceAddress;
	Button btnCopyCustBillingAddress;
	
	/**
	 * @author Anil
	 * @since 28-05-2020
	 * Added tat for premium tech
	 */
	TimeFormatBox tmbTat;
	TextBox tbTier;
	
	TextBox tbServiceAddressName;//Ashwini Patil Date: 6-09-2022 Every client had different logic for service address name so made this Textbox where they can put exact name which they want to print on contract and invoice print.
	  
	
	

	public CustomerBranchForm  (SuperTable<CustomerBranchDetails> table, int mode,boolean captionmode) {
		super(table, mode, captionmode);
		createGui();
		System.out.println("In side 1st con");
	}
	
	public CustomerBranchForm  (SuperTable<CustomerBranchDetails> table, int mode,boolean captionmode, boolean popupflag) {
		super(table, mode, captionmode,popupflag);
		createGui();
		System.out.println("In side 1st con");
	}
	
	public CustomerBranchForm(CustomerBranchPresenterTable gentable) {
		super(gentable);
		createGui();
		System.out.println("in side 2nd con");
	
	}

	private void initalizeWidget()
	{
		personInfoComposite=AppUtility.customerInfoComposite(new Customer());
		personInfoComposite.getCustomerId().getHeaderLabel().setText("* Customer ID");
		personInfoComposite.getCustomerName().getHeaderLabel().setText("* Customer Name");
		personInfoComposite.getCustomerCell().getHeaderLabel().setText("* Customer Cell");
		
		personInfoComposite.btnLocation.setVisible(false);
		
		etbEmail=new EmailTextBox();
		etbPointOfContactEmail=new EmailTextBox();

		addressComposite=new AddressComposite();

		checkBoxStatus=new CheckBox();
		checkBoxStatus.setValue(true); 
		tbBranchName=new TextBox();
		tbServiceAddressName=new TextBox();
		tbPointOfContactName=new TextBox();
		pnbLandlinePhone=new PhoneNumberBox();
		pnbCellPhoneNo1=new PhoneNumberBox();
		pnbCellPhoneNo2=new PhoneNumberBox();
		pnbFaxNo=new PhoneNumberBox();
		pnbPointOfContactLandline=new PhoneNumberBox();
		pnbPointOfContactCell=new PhoneNumberBox();
		
		olbbBranch=new ObjectListBox<Branch>();
		
		/*
		 * Added by Rahul Verma
		 */
		tbGSTINNumber=new TextBox();
		
		//Ashwini Patil Date:13-05-2024 If branch restriction is active then employee is not able to select other company branches. Reported by Ultima pest.
//		AppUtility.makeBranchListBoxLive(olbbBranch);
		AppUtility.makeCustomerBranchListBoxLive(olbbBranch);
		
		/** date 16.02.2018 added by komal for area wise billing**/
		dlbArea = new DoubleBox();
		olbUOM=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbUOM, Screen.SERVICEUOM);
		tbCostCenter = new TextBox();
		/**
		 * end komal
		 */
		
		billingAddress=new AddressComposite();
		btnCopyCustBillingAddress=new Button("Copy Customer's Billing Address");
		btnCopyCustBillingAddress.addClickHandler(this);
		btnCopyCustServiceAddress=new Button("Copy Customer's Service Address");
		btnCopyCustServiceAddress.addClickHandler(this);
		
		tmbTat=new TimeFormatBox();
		tbTier=new TextBox();
		tbTier.setEnabled(false);
		
		addressComposite.getCity().addChangeHandler(this);
		addressComposite.getLocality().addChangeHandler(this);

		
	}

	/*
	 * Method template to create the formtable screen
	 */

	@Override
	public void createScreen() {

		//Token to initialize the processlevel menus.
		boolean hideArea=true;
		boolean hideUom=true;
		boolean hideCostCenter=true;
		
		/**
		 * @author Anil
		 * @since 03-06-2020
		 * Hiding fax number 
		 */
		boolean hideFaxNumber=true;
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CustomerBranchDetails", "HideArea")){
			hideArea=false;
		}
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CustomerBranchDetails", "HideUom")){
			hideUom=false;
		}
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CustomerBranchDetails", "HideCostCenter")){
			hideCostCenter=false;
		}
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CustomerBranchDetails", "HideFaxNumber")){
			hideFaxNumber=false;
		}

	    initalizeWidget();
		
		this.processlevelBarNames=new String[]{"New","Update Duplicate Branch",AppConstants.UPDATESERVICEADDRESS,"Service Location",AppConstants.generateComplainQR};
		
		
		if(isPopUpAppMenubar()){
//			this.processlevelBarNames=new String[]{};
			this.processlevelBarNames=null;
			Console.log("POP up is true and process level bar is null");
		}

		//////////////////////////////////Form Field Declaration/////////////////////////////////////////////////


		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////

		//Token to initialize formfield
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		
		FormField fgroupingCustomerInformation=fbuilder.setlabel("Customer Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("",personInfoComposite);
		FormField fpersonInfoComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		FormField fgroupingCompanyInformation=fbuilder.setlabel("Branch Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("* Branch Name",tbBranchName);
		FormField ftbBranchName= fbuilder.setMandatory(true).setMandatoryMsg("Branch Name is mandatory!").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Service Address Name",tbServiceAddressName);
		FormField ftbServiceAddressName= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Branch Email",etbEmail);
		FormField fetbEmail= fbuilder.setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Status",checkBoxStatus);
		FormField fcheckBoxStatus= fbuilder.setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Landline Phone",pnbLandlinePhone);
		FormField fpnbLandlinePhone= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Cell Phone No 1",pnbCellPhoneNo1);
		FormField fpnbCellPhoneNo1= fbuilder.setMandatory(true).setMandatoryMsg("Cell Phone No 1 is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Cell Phone No 2",pnbCellPhoneNo2);
		FormField fpnbCellPhoneNo2= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Fax No",pnbFaxNo);
		FormField fpnbFaxNo= fbuilder.setMandatory(false).setRowSpan(0).setVisible(hideFaxNumber).setColSpan(0).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingPointOfContactInformation=fbuilder.setlabel("Point Of Contact Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Point Of Contact Name",tbPointOfContactName);
		FormField ftbPointOfContactName= fbuilder.setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Point Of Contact Landline",pnbPointOfContactLandline);
		FormField fpnbPointOfContactLandline= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Point Of Contact Cell",pnbPointOfContactCell);
		FormField fpnbPointOfContactCell= fbuilder.setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Point Of Contact Email",etbPointOfContactEmail);
		FormField fetbPointOfContactEmail= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingAddressInformation=fbuilder.setlabel("Service Address").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("",addressComposite);
		FormField faddressComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();

		
		fbuilder = new FormFieldBuilder("* Servicing Branch",olbbBranch);
		FormField folbbBranch= fbuilder.setMandatory(true).setMandatoryMsg("Branch is Mandatory").setRowSpan(0).setColSpan(0).build();
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////
		fbuilder = new FormFieldBuilder("Branch GSTIN",tbGSTINNumber);
		FormField ftbGSTINNumber= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/** date 16.02.2018 added by komal for area wise billing**/
		fbuilder = new FormFieldBuilder("Area" , dlbArea);
		FormField fdlbArea = fbuilder.setMandatory(false).setRowSpan(0).setVisible(hideArea).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("UOM" , olbUOM);
		FormField folbUOM = fbuilder.setMandatory(false).setRowSpan(0).setVisible(hideUom).setColSpan(0).build();
		
		fbuilder =  new FormFieldBuilder("Cost Center" , tbCostCenter);
		FormField ftbCostCenter = fbuilder.setMandatory(false).setRowSpan(0).setVisible(hideCostCenter).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingBillAddress=fbuilder.setlabel("Billing Address").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("",billingAddress);
		FormField fbillingAddress= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();

		fbuilder =  new FormFieldBuilder("" , btnCopyCustBillingAddress);
		FormField fbtnCopyCustBillingAddress = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder =  new FormFieldBuilder("" , btnCopyCustServiceAddress);
		FormField fbtnCopyCustServiceAddress = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder =  new FormFieldBuilder("TAT" , tmbTat.getTimePanel());
		FormField ftmbTat = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder =  new FormFieldBuilder("Tier" , tbTier);
		FormField ftbTier = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

//		old Branch Information section 
//		{ftbBranchName,fetbEmail,folbbBranch,ftbGSTINNumber},
//		{fpnbLandlinePhone,fpnbCellPhoneNo1,fpnbCellPhoneNo2,fcheckBoxStatus},
//		{fpnbFaxNo,fdlbArea,folbUOM,ftbCostCenter},
		FormField[][] formfield = { 
				{fgroupingCustomerInformation},
				{fpersonInfoComposite},
				{fgroupingCompanyInformation},	
				{ftbBranchName,ftbServiceAddressName,fetbEmail,folbbBranch},
				{ftbGSTINNumber,fpnbLandlinePhone,fpnbCellPhoneNo1,fcheckBoxStatus},
				{fpnbCellPhoneNo2,fpnbFaxNo,fdlbArea,folbUOM},
				{ftbCostCenter},
				{fgroupingPointOfContactInformation},
				{ftbPointOfContactName,fpnbPointOfContactLandline,fpnbPointOfContactCell,fetbPointOfContactEmail},
				{fgroupingAddressInformation},
				{fbtnCopyCustServiceAddress},
				{faddressComposite},
				{ftbTier,ftmbTat},
				{fgroupingBillAddress},	
				{fbtnCopyCustBillingAddress},
				{fbillingAddress}
		};
		this.fields=formfield;		
	}
	
	
	@Override
	public void updateModel(CustomerBranchDetails model) 
	{

		if(personInfoComposite.getValue()!=null)
			model.setCinfo(personInfoComposite.getValue());
		if(tbBranchName.getValue()!=null)
			model.setBusinessUnitName(tbBranchName.getValue());
		if(tbServiceAddressName.getValue()!=null)
			model.setServiceAddressName(tbServiceAddressName.getValue());
		
		if(etbEmail.getValue()!=null)
			model.setEmail(etbEmail.getValue());
		if(checkBoxStatus.getValue()!=null)
			model.setStatus(checkBoxStatus.getValue());
		if(pnbLandlinePhone.getValue()!=null)
			model.setLandline(pnbLandlinePhone.getValue());
		if(pnbCellPhoneNo1.getValue()!=null)
			model.setCellNumber1(pnbCellPhoneNo1.getValue());
		if(pnbCellPhoneNo2.getValue()!=null)
			model.setCellNumber2(pnbCellPhoneNo2.getValue());
		if(pnbFaxNo.getValue()!=null)
			model.setFaxNumber(pnbFaxNo.getValue());
		if(tbPointOfContactName.getValue()!=null)
			model.setPocName(tbPointOfContactName.getValue());
		if(pnbPointOfContactLandline.getValue()!=null)
			model.setPocLandline(pnbPointOfContactLandline.getValue());
		if(pnbPointOfContactCell.getValue()!=null)
			model.setPocCell(pnbPointOfContactCell.getValue());
		if(etbPointOfContactEmail.getValue()!=null)
			model.setPocEmail(etbPointOfContactEmail.getValue());
		if(addressComposite.getValue()!=null)
			model.setAddress(addressComposite.getValue());

		if(olbbBranch.getValue(olbbBranch.getSelectedIndex())!=null)
			model.setBranch(olbbBranch.getValue(olbbBranch.getSelectedIndex()));
		
		if(tbGSTINNumber.getValue()!=null){
			model.setGSTINNumber(tbGSTINNumber.getValue());
		}
		/** date 16.02.2018 added by komal for area wise billing**/
		if(dlbArea.getValue()!=null){
			model.setArea(dlbArea.getValue());
		}
		 if(olbUOM.getSelectedIndex() !=0){
			 model.setUnitOfMeasurement(olbUOM.getValue());
		 }
		 if(tbCostCenter.getValue()!=null){
			 model.setCostCenter(tbCostCenter.getValue());
		 }
		 /**
		  * end komal
		  */
		 
		 if(billingAddress.getValue()!=null){
			 model.setBillingAddress(billingAddress.getValue());
		 }
		 
		 if(tbTier.getValue()!=null){
			 model.setTierName(tbTier.getValue());
		 }
		 
		 if(tmbTat.getValue()!=null){
			 model.setTat(tmbTat.getValue());
		 }else{
			 model.setTat(null);
		 }

		custBranchObj=model;
		
		presenter.setModel(model);


	}

//	@Override
//	public boolean validate() {
//		boolean superValidate = super.validate();
//		if(CustomerBranchPresenter.customerbranchlist.size()==1){
//        	this.showDialogMessage("Can't Add! Duplicate Data");
//        	
//        	CustomerBranchPresenter.customerbranchlist.clear();
//        	
//        	return false;
//        	
//        }
//		
//		return true;
//	}

	/**
	 * method template to update the view with token entity name
	 */
	@Override
	public void updateView(CustomerBranchDetails view) 
	{
		custBranchObj=view;
		
		/**
		 * Date 20-04-2018 By vijay for orion pest locality load  
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Locality", "DontLoadAllLocalityAndCity")){
			setAddressDropDown();
		}
		/**
		 * ends here
		 */
		
		if(view.getCinfo()!=null)
			personInfoComposite.setValue(view.getCinfo());

		if(view.getBusinessUnitName()!=null)
			tbBranchName.setValue(view.getBusinessUnitName());
		if(view.getServiceAddressName()!=null)
			tbServiceAddressName.setValue(view.getServiceAddressName());
		if(view.getEmail()!=null)
			etbEmail.setValue(view.getEmail());
		if(view.getStatus()!=null)
			checkBoxStatus.setValue(view.getStatus());
		if(view.getLandline()!=null)
			pnbLandlinePhone.setValue(view.getLandline());
		if(view.getCellNumber1()!=null)
			pnbCellPhoneNo1.setValue(view.getCellNumber1());
		if(view.getCellNumber2()!=null)
			pnbCellPhoneNo2.setValue(view.getCellNumber2());
		if(view.getFaxNumber()!=null)
			pnbFaxNo.setValue(view.getFaxNumber());
		if(view.getPocName()!=null)
			tbPointOfContactName.setValue(view.getPocName());
		if(view.getPocLandline()!=null)
			pnbPointOfContactLandline.setValue(view.getPocLandline());
		if(view.getPocCell()!=null)
			pnbPointOfContactCell.setValue(view.getPocCell());
		if(view.getPocEmail()!=null)
			etbPointOfContactEmail.setValue(view.getPocEmail());
		if(view.getAddress()!=null)
			addressComposite.setValue(view.getAddress());
		/*if(view.getCalendarName()!=null)
			calendar.setValue(view.getCalendarName());*/
		if(view.getBranch()!=null)
			olbbBranch.setValue(view.getBranch());
		if(view.getGSTINNumber()!=null){
			tbGSTINNumber.setValue(view.getGSTINNumber());
		}
		/** date 16.02.2018 added by komal for area wise billing**/
//		if(view.getArea()!=0){
			dlbArea.setValue(view.getArea());
//		}
		if(view.getUnitOfMeasurement()!=null){
			olbUOM.setValue(view.getUnitOfMeasurement());
		}
		if(view.getCostCenter()!=null){
			tbCostCenter.setValue(view.getCostCenter());
		}
		/**
		 * end komal
		 */
		
		if(view.getBillingAddress()!=null){
			billingAddress.setValue(view.getBillingAddress());
		}
		
		if(view.getTierName()!=null){
			tbTier.setValue(view.getTierName());
		}
		
		if(view.getTat()!=null){
			tmbTat.setValue(view.getTat());
		}
		
		presenter.setModel(view);



	}
	
	/**
	 * Date 20-04-2018 By vijay
	 * here i am refreshing address composite data if global list size greater than drop down list size
	 * because we are adding locality and city from an entity in view state to global list if doest not exist in global list 
	 */
	private void setAddressDropDown() {
		if(LoginPresenter.globalLocality.size()>addressComposite.locality.getItems().size()){
			addressComposite.locality.setListItems(LoginPresenter.globalLocality);
		}
	}
	/**
	 * ends here 
	 */

	/**
	 * Toggles the app header bar menus as per screen state
	 */
	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Save")||text.contains("Discard")||text.contains("Search")||text.contains(AppConstants.NAVIGATION)||text.contains("Download"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Save")||text.contains("Discard")||text.contains(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Download")||text.contains("Edit")||text.contains("Discard")||text.contains("Search")||text.contains(AppConstants.NAVIGATION)||text.contains("Download"))
				{

					/** Date 02-07-2018 added by vijay
					 * Des :-  if condition added for NBHC CCPM Edit button visible only for admin in branch level restriction 
					 *         else block normal execution.
					 * Requirement :- NBHC CCPM
					 **/
					if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CustomerBranchDetails","OnlyForNBHC")){
						if(!LoginPresenter.myUserEntity.getRole().getRoleName().equals("ADMIN"))
						{
							if(text.contains("Edit")){
								menus[k].setVisible(false);
							}else{
								menus[k].setVisible(true); 
							}
							
						}
						/**
						 * ends here
						 */
						else{
							menus[k].setVisible(true); 
						}
					}
					/**
					 * ends here
					 */
					else{
						menus[k].setVisible(true); 
					}
					
				}
				else
					menus[k].setVisible(false);  		   
			}
		}
		if(isPopUpAppMenubar()){
			return;
		}
		
		AuthorizationHelper.setAsPerAuthorization(Screen.CUSTOMERBRANCH,LoginPresenter.currentModule.trim());
	}

	/**
	 * sets the id textbox with the passed count value. 
	 */
	@Override
	public void setCount(int count)
	{

	}

	@Override
	public void onClick(ClickEvent event) {
		if(event.getSource()==btnCopyCustBillingAddress){
			if(personInfoComposite!=null&&personInfoComposite.getValue()!=null){
				getCustomerAddress(personInfoComposite.getIdValue(), "Billing Address");
			}
		}
		if(event.getSource()==btnCopyCustServiceAddress){
			if(personInfoComposite!=null&&personInfoComposite.getValue()!=null){
				getCustomerAddress(personInfoComposite.getIdValue(), "Service Address");
			}
		}
	}

	@Override
	public void clear() {
		super.clear();
		checkBoxStatus.setValue(true);
		tmbTat.clear();
	}

	@Override
	public void setToEditState() {
		super.setToEditState();
		this.processLevelBar.setVisibleFalse(false);
	}

	
	@Override
	public void setToViewState() {
		super.setToViewState();
		SuperModel model=new CustomerBranchDetails();
		model=custBranchObj;
		AppUtility.addDocumentToHistoryTable(AppConstants.SERVICEMODULE,AppConstants.CUSTOMERBRANCH, custBranchObj.getCount(),custBranchObj.getCinfo().getCount(), custBranchObj.getCinfo().getFullName(),custBranchObj.getCinfo().getCellNumber(), false, model, null);
		/**
		 * @author Anil @since 30-08-2021
		 */
		if(isPopUpAppMenubar()){
			this.processLevelBar.setVisibleFalse(false);
		}
	}
	
	@Override
	public void setToNewState() {
		super.setToNewState();
		System.out.println("Inside NEW method");
		if(custBranchObj!=null){
		SuperModel model=new CustomerBranchDetails();
		model=custBranchObj;
		AppUtility.addDocumentToHistoryTable(AppConstants.SERVICEMODULE,AppConstants.CUSTOMERBRANCH, custBranchObj.getCount(), custBranchObj.getCinfo().getCount(), custBranchObj.getCinfo().getFullName(),custBranchObj.getCinfo().getCellNumber(), false, model, null);
		}
	}

	/******************************************getter and setter*****************************************************/
	
	public PersonInfoComposite getPersonInfoComposite() {
		return personInfoComposite;
	}

	public void setPersonInfoComposite(PersonInfoComposite personInfoComposite) {
		this.personInfoComposite = personInfoComposite;
	}

	public TextBox getTbBranchName() {
		return tbBranchName;
	}

	public void setTbBranchName(TextBox tbBranchName) {
		this.tbBranchName = tbBranchName;
	}
	
	public TextBox getTbServiceAddressName() {
		return tbServiceAddressName;
	}

	public void setTbServiceAddressName(TextBox tbServiceAddressName) {
		this.tbServiceAddressName = tbServiceAddressName;
	}
	
	
	public void getCustomerAddress(int custId,final String addressType){
		MyQuerry querry=new MyQuerry();
		Filter temp=null;
		
		temp=new Filter();
		temp.setQuerryString("count");
		temp.setIntValue(custId);
		
		querry.getFilters().add(temp);
		
		querry.setQuerryObject(new Customer());
		showWaitSymbol();
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				hideWaitSymbol();
				if(result!=null){
					Customer customer=(Customer) result.get(0);
					if(addressType.equals("Service Address")){
						addressComposite.setValue(customer.getSecondaryAdress());
					}else if(addressType.equals("Billing Address")){
						billingAddress.setValue(customer.getAdress());
					}
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				hideWaitSymbol();
			}
		});
		
	}

	@Override
	public void onChange(ChangeEvent event) {
		// TODO Auto-generated method stub
		if(event.getSource().equals(addressComposite.getCity())){
			if(addressComposite.getCity().getSelectedIndex()!=0){
				City city=addressComposite.getCity().getSelectedItem();
				if(city.getClassName()!=null){
					tbTier.setValue(city.getClassName());
				}
				if(city.getTat()!=null){
					tmbTat.setValue(city.getTat());
				}else{
					tmbTat.setValue(null);
				}
			}else{
				tbTier.setValue("");
				tmbTat.setValue(null);
			}
		}
		
		/**
		 * @author Vijay Date :- 17-12-2020
		 * Des :- Tat Tier will map when user selected locality
		 */
		if(event.getSource().equals(addressComposite.getLocality())){
			if(addressComposite.getCity().getSelectedIndex()!=0){
				City city=addressComposite.getCity().getSelectedItem();
				if(city.getClassName()!=null){
					tbTier.setValue(city.getClassName());
				}
				if(city.getTat()!=null){
					tmbTat.setValue(city.getTat());
				}else{
					tmbTat.setValue(null);
				}
			}else{
				tbTier.setValue("");
				tmbTat.setValue(null);
			}
		}
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		super.setEnable(state);
		tmbTat.setEnabled(state);
		tbTier.setEnabled(false);
	}
	
	
	
	
}
