package com.slicktechnologies.client.views.customerbranchdetails;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.ProvidesKey;
import com.slicktechnologies.client.views.customerbranchdetails.CustomerBranchPresenter.CustomerBranchPresenterTable;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;

public class CustomerBranchTable extends CustomerBranchPresenterTable {
	
	TextColumn<CustomerBranchDetails> getIDColumn;
	TextColumn<CustomerBranchDetails> getNameColumn;
	TextColumn<CustomerBranchDetails> getPhoneColumn;
	TextColumn<CustomerBranchDetails> getContactNameColumn;
	TextColumn<CustomerBranchDetails> getContactPhoneColumn;
	TextColumn<CustomerBranchDetails> getStatusColumn;
	TextColumn<CustomerBranchDetails> getBranchColumn;
	
	
	public Object getVarRef(String varName)
	{
		if(varName.equals("getIDColumn"))
			return this.getIDColumn;
		if(varName.equals("getNameColumn"))
			return this.getNameColumn;
		if(varName.equals("getPhoneColumn"))
			return this.getPhoneColumn;
		if(varName.equals("getContactNameColumn"))
			return this.getContactNameColumn;
		if(varName.equals("getContactPhoneColumn"))
			return this.getContactPhoneColumn;
		if(varName.equals("getBranchColumn"))
			return this.getBranchColumn;
		if(varName.equals("getStatusColumn"))
			return this.getStatusColumn;
		return null ;
	}
	public CustomerBranchTable()
	{
		super();
	}
	@Override
	public void createTable() {
		addColumngetID();
		addColumngetName();
		addColumngetPhone();
		addColumngetContactName();
		addColumngetContactPhone();
		addColumngetBranch();
		addColumngetStatus();
	}
	
	
	
	@Override
	protected void initializekeyprovider() {
		keyProvider= new ProvidesKey<CustomerBranchDetails>()
				{
			@Override
			public Object getKey(CustomerBranchDetails item)
			{
				if(item==null)
				{
					return null;
				}
				else
					return item.getId();
			}
				};
	}
	@Override
	public void setEnable(boolean state)
	{
	}
	@Override
	public void applyStyle()
	{
	}
	public void addColumnSorting(){
		
		addSortinggetID();
		addSortinggetName();
		addSortinggetPhone();
		addSortinggetContactName();
		addSortinggetContactPhone();
		addSortinggetStatus();
	}
	private void addSortinggetID() {

		List<CustomerBranchDetails> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerBranchDetails>(list);
		columnSort.setComparator(getIDColumn, new Comparator<CustomerBranchDetails>()
				{
			@Override
			public int compare(CustomerBranchDetails e1,CustomerBranchDetails e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
		
		
	}
	private void addSortinggetName() {

		List<CustomerBranchDetails> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerBranchDetails>(list);
		columnSort.setComparator(getNameColumn, new Comparator<CustomerBranchDetails>()
				{
			@Override
			public int compare(CustomerBranchDetails e1,CustomerBranchDetails e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBusinessUnitName()!=null && e2.getBusinessUnitName()!=null){
						return e1.getBusinessUnitName().compareTo(e2.getBusinessUnitName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);

	}
	private void addSortinggetPhone() {

		List<CustomerBranchDetails> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerBranchDetails>(list);
		columnSort.setComparator(getPhoneColumn, new Comparator<CustomerBranchDetails>()
				{
			@Override
			public int compare(CustomerBranchDetails e1,CustomerBranchDetails e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCellNumber1()== e2.getCellNumber1()){
						return 0;}
					if(e1.getCellNumber1()> e2.getCellNumber1()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
		
		
	}
	private void addSortinggetContactName() {


		List<CustomerBranchDetails> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerBranchDetails>(list);
		columnSort.setComparator(getContactNameColumn, new Comparator<CustomerBranchDetails>()
				{
			@Override
			public int compare(CustomerBranchDetails e1,CustomerBranchDetails e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getCinfo().getFullName()!=null && e2.getCinfo().getFullName()!=null){
						return e1.getCinfo().getFullName().compareTo(e2.getCinfo().getFullName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
		
	}
	private void addSortinggetContactPhone() {

		List<CustomerBranchDetails> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerBranchDetails>(list);
		columnSort.setComparator(getContactPhoneColumn, new Comparator<CustomerBranchDetails>()
				{
			@Override
			public int compare(CustomerBranchDetails e1,CustomerBranchDetails e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCinfo().getCellNumber()== e2.getCinfo().getCellNumber()){
						return 0;}
					if(e1.getCinfo().getCellNumber()> e2.getCinfo().getCellNumber()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
		
		
	}
	private void addSortinggetStatus() {

		List<CustomerBranchDetails> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerBranchDetails>(list);
		columnSort.setComparator(getStatusColumn, new Comparator<CustomerBranchDetails>()
				{
			@Override
			public int compare(CustomerBranchDetails e1,CustomerBranchDetails e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getStatus()!=null && e2.getStatus()!=null){
						return e1.getStatus().compareTo(e2.getStatus());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
		
		
	}
	@Override public void addFieldUpdater() {
	}
	protected void addColumngetID()
	{
		getIDColumn=new TextColumn<CustomerBranchDetails>()
				{
			@Override
			public String getValue(CustomerBranchDetails object)
			{
				if( object.getCount()==-1)
					return "N.A";
				else return object.getCount()+"";
			}
				};
				table.addColumn(getIDColumn,"Id");
				getIDColumn.setSortable(true);
//				table.setColumnWidth(getIDColumn,"110px");
	}

	protected void addColumngetName()
	{
		getNameColumn=new TextColumn<CustomerBranchDetails>()
				{
			@Override
			public String getValue(CustomerBranchDetails object)
			{
				if(object.getBusinessUnitName()!=null){
					return object.getBusinessUnitName()+"";
				}
				return " ";
				
			}
				};
				table.addColumn(getNameColumn,"Name");
				getNameColumn.setSortable(true);
//				table.setColumnWidth(getNameColumn,"110px");
	}

	protected void addColumngetPhone()
	{
		getPhoneColumn=new TextColumn<CustomerBranchDetails>()
				{
			@Override
			public String getValue(CustomerBranchDetails object)
			{
				return object.getCellNumber1()+"";
			}
				};
				table.addColumn(getPhoneColumn,"Phone");
				getPhoneColumn.setSortable(true);
//				table.setColumnWidth(getPhoneColumn,"110px");
	}

	protected void addColumngetContactName()
	{
		getContactNameColumn=new TextColumn<CustomerBranchDetails>()
				{
			@Override
			public String getValue(CustomerBranchDetails object)
			{
				if(object.getCinfo().getFullName()!=null){
					return object.getCinfo().getFullName()+"";
				}
				return " ";
				
			}
				};
				table.addColumn(getContactNameColumn,"Customer Name");
				getContactNameColumn.setSortable(true);
//				table.setColumnWidth(getContactNameColumn,"110px");
	}

	protected void addColumngetContactPhone()
	{
		getContactPhoneColumn=new TextColumn<CustomerBranchDetails>()
				{
			@Override
			public String getValue(CustomerBranchDetails object)
			{
				return object.getCinfo().getCellNumber()+"";
			}
				};
				table.addColumn(getContactPhoneColumn,"Customer Phone");
				getContactPhoneColumn.setSortable(true);
//				table.setColumnWidth(getContactPhoneColumn,"110px");
	}

	
	
	
	protected void addColumngetBranch()
	{
		getBranchColumn=new TextColumn<CustomerBranchDetails>()
				{
			@Override
			public String getValue(CustomerBranchDetails object)
			{
				return object.getBranch();
			}
				};
				table.addColumn(getBranchColumn,"Servicing Branch");
				getBranchColumn.setSortable(true);
	}
	
	protected void addColumngetStatus()
	{
		getStatusColumn=new TextColumn<CustomerBranchDetails>()
				{
			@Override
			public String getValue(CustomerBranchDetails object)
			{
				if( object.getStatus()==true)
					return "Active";
				else 
					return "In Active";
			}
				};
				table.addColumn(getStatusColumn,"Status");
				getStatusColumn.setSortable(true);
//				table.setColumnWidth(getStatusColumn,"110px");
	}



}
