package com.slicktechnologies.client.views.customerbranchdetails;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.CustomerNameChangeService;
import com.slicktechnologies.client.services.CustomerNameChangeServiceAsync;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.FromAndToDateBoxPopup;
import com.slicktechnologies.client.views.popups.ContractIdPopUp;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;

public class CustomerBranchPresenter extends FormTableScreenPresenter<CustomerBranchDetails> implements  ValueChangeHandler<String>, SelectionHandler<Suggestion>  {
	
	CustomerBranchForm form;
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	final GenricServiceAsync async=GWT.create(GenricService.class);
	static ArrayList<CustomerBranchDetails> customerbranchlist=new ArrayList<CustomerBranchDetails>();
	
	FromAndToDateBoxPopup datePopUp=new FromAndToDateBoxPopup(true,"Customer Id");
	PopupPanel datePanel=new PopupPanel();
	
	ContractIdPopUp contractidpopup = new ContractIdPopUp();
	CustomerNameChangeServiceAsync customerbranchserviceupdate = GWT.create(CustomerNameChangeService.class);
	
	GeneralViewDocumentPopup genralInvoicePopup=new GeneralViewDocumentPopup();

	
	public CustomerBranchPresenter (FormTableScreen<CustomerBranchDetails> view, CustomerBranchDetails model, MyQuerry querry) {
		super(view, model);
		form=(CustomerBranchForm) view;
	//  rohan commented this code for duplicate branches loaded in table  on Date : 11-4-2017
		form.getSupertable().connectToLocal();
		/**
		 * Date 11-05-2019 by Vijay 
		 * NBHC CCPM for admin loading all data so system getting hang so added process config to dont load 
		 * by default all branches data  
		 */
		if(!AppUtility.checkForProcessConfigurartionIsActiveOrNot("CustomerBranchDetails", "EnableRestrictDefaultLoading")){
		//  new code by rohan 
			if(querry!=null){
				form.retriveTable(querry);
			}else{
				form.retriveTable(getBranchQuery());
			}
		}
		
		//  old code 
//		form.retriveTable(getBranchQuery());
		
		form.setPresenter(this);
		
		form.getPersonInfoComposite().getId().addSelectionHandler(this);
		form.getPersonInfoComposite().getName().addSelectionHandler(this);
		form.getPersonInfoComposite().getPhone().addSelectionHandler(this);
		form.getTbBranchName().addValueChangeHandler(this);
		
		datePopUp.getBtnOne().addClickHandler(this);
		datePopUp.getBtnTwo().addClickHandler(this);
		
		contractidpopup.getLblOk().addClickHandler(this);
		
	}

	/**
	 * Method template to set the processBar events
	 */
	@Override
	public void reactToProcessBarEvents(ClickEvent e) 
   {
		InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();
		
		if(text.equals("New"))
		{
			reactOnNew();
		}	
		
		if(text.equals("Update Duplicate Branch")){
			datePanel=new PopupPanel(true);
			datePanel.add(datePopUp);
			datePanel.center();
			datePanel.show();
		}
		
		if(text.equals(AppConstants.UPDATESERVICEADDRESS)) {
			reacttoUpdateServiceAddress();
		}
		
		if(text.equals("Service Location")) {
			reactOnServiceLocation();
		}
		
		if(text.equals(AppConstants.generateComplainQR)) {
			Console.log("in generateComplainQR");
			String gwt = com.google.gwt.core.client.GWT
					.getModuleBaseURL();
			final String url = gwt + "GenerateQRPdf" + "?CustomerId=" + model.getCinfo().getCount()+"&CustomerName="+model.getCinfo().getFullName()+"&CustomerBranch="+model.getBusinessUnitName()+"&CustomerBranchId="+model.getCount()+"&companyId="+model.getCompanyId();
			Window.open(url, "test", "enabled");
		}
	}
	
	

	private void reactOnServiceLocation() {
		genralInvoicePopup = new GeneralViewDocumentPopup();
//		genralInvoicePopup.btnClose.addClickHandler(this);
		genralInvoicePopup.setServiceLocation("Service Location",model);
	}

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void reactOnDownload() {
		
		ArrayList<CustomerBranchDetails> custbrancharray = new ArrayList<CustomerBranchDetails>();
		List<CustomerBranchDetails> list = (List<CustomerBranchDetails>) form.getSupertable().getDataprovider().getList();

		custbrancharray.addAll(list);
		
		csvservice.setCustomerBranchDetails(custbrancharray,
				new AsyncCallback<Void>() {

					@Override
					public void onFailure(Throwable caught) {
						
						System.out.println("RPC call Failed" + caught);

					}

					@Override
					public void onSuccess(Void result) {

						String gwt = com.google.gwt.core.client.GWT
								.getModuleBaseURL();
						
						/**
						 * Date 05-07-2018 By Vijay
						 * Des :- for NBHC Customer branch download with limited columns
						 */
						if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CustomerBranchDetails", "CustomerBranchDownload")){
							final String url = gwt + "csvservlet" + "?type=" + 152;
							Window.open(url, "test", "enabled");
						}
						/**
						 * ends here
						 */
						else{
							final String url = gwt + "csvservlet" + "?type=" + 87;
							Window.open(url, "test", "enabled");

						}
						
					}
				});
	}
	

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

	/**
	 * Method template to set new model
	 */
	@Override
	protected void makeNewModel() {
		
		model=new CustomerBranchDetails();
	}
	
	/**
	 * Method template to set Myquerry object
	 */
	public MyQuerry getBranchQuery()
	{
		
		/**
		 * Date 05-07-2018 added by vijay
		 * Des :- below if condition for loading data according to branch restriction and else block for load all data
		 */
		if(!LoginPresenter.myUserEntity.getRole().getRoleName().equals("ADMIN")){
			List<String> branchlist =new ArrayList<String>();
			for(int i=0;i<LoginPresenter.globalBranch.size();i++){
				branchlist.add(LoginPresenter.globalBranch.get(i).getBusinessUnitName());
			}
			MyQuerry querry = new MyQuerry();
			Vector<Filter> filtervec = new Vector<Filter>();
			Filter filter = null;
			
			if(branchlist.size()!=0){
			filter = new Filter();
			filter.setList(branchlist);
			filter.setQuerryString("branch IN");
			filtervec.add(filter);
			}
			
			querry.setFilters(filtervec);
			querry.setQuerryObject(new CustomerBranchDetails());
			
			return querry;
		}else{
			MyQuerry quer=new MyQuerry(new Vector<Filter>(), new CustomerBranchDetails());
			return quer;
		}
		
	}
	
	public void setModel(CustomerBranchDetails entity)
	{
		model=entity;
	}
	
	public static CustomerBranchForm initalize()
	{
		CustomerBranchPresenterTable gentableScreen=new CustomerBranchTable();
		CustomerBranchForm  form=new  CustomerBranchForm(gentableScreen,FormTableScreen.UPPER_MODE,true);
		gentableScreen.setView(form);
		gentableScreen.applySelectionModle();
		
		CustomerBranchPresenterSearchProxy.staticSuperTable=gentableScreen;
		CustomerBranchPresenterSearchProxy searchpopup=new CustomerBranchPresenterSearchProxy(false);
		form.setSearchpopupscreen(searchpopup);
		
		 CustomerBranchPresenter  presenter=new  CustomerBranchPresenter  (form,new CustomerBranchDetails(),null);
		 form.setToNewState();
		 AppMemory.getAppMemory().stickPnel(form);
		 return form;
	}
	
	@Override
	protected void reactOnGo() {
		super.reactOnGo();
		form.getSearchpopupscreen().hidePopUp();
		 form.setToNewState();
	}

	public static CustomerBranchForm initialize(MyQuerry querry)
	{
		CustomerBranchPresenterTable gentable=new CustomerBranchTable();
		CustomerBranchForm  form=new  CustomerBranchForm(gentable,FormTableScreen.UPPER_MODE,true);
		
		CustomerBranchPresenterSearchProxy.staticSuperTable=gentable;
		CustomerBranchPresenterSearchProxy searchpopup=new CustomerBranchPresenterSearchProxy(false);
		form.setSearchpopupscreen(searchpopup);
		
		CustomerBranchPresenter presenter=new CustomerBranchPresenter(form, new CustomerBranchDetails(),querry);
		//  rohan commented this code for duplicate branches loaded in table  on Date : 11-4-2017
//		form.retriveTable(querry);
		form.setToNewState();
		AppMemory.getAppMemory().stickPnel(form);
		return form;
	}
	
	
		@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails")
		 public static class  CustomerBranchPresenterTable extends SuperTable<CustomerBranchDetails> implements GeneratedVariableRefrence{

			@Override
			public Object getVarRef(String varName) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void createTable() {
				// TODO Auto-generated method stub
				
			}

			@Override
			protected void initializekeyprovider() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void addFieldUpdater() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void setEnable(boolean state) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void applyStyle() {
				// TODO Auto-generated method stub
				
			}} ;
			
			@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails")
			 public static class  CustomerBranchPresenterSearch extends SearchPopUpScreen<CustomerBranchDetails>{

				@Override
				public MyQuerry getQuerry() {
					// TODO Auto-generated method stub
					return null;
				}

				@Override
				public boolean validate() {
					// TODO Auto-generated method stub
					return true;
				}};
				
	  private void reactTo()
	  {
	  }
	  
	  private void reactOnNew()
	  {
		  form.setToNewState();
		  this.initalize();
	  }


	  @Override
	   public void onClick(ClickEvent event) {
		  super.onClick(event);
//	      InlineLabel label= (InlineLabel) event.getSource();
	      
	      if(event.getSource()==datePopUp.getBtnOne()){
	    	  GeneralServiceAsync async=GWT.create(GeneralService.class);
				if(datePopUp.getIbServiceId().getValue()==null){
					form.showDialogMessage("Please enter customer id.");
					return;
				}
				form.showWaitSymbol();
				async.updateDuplicateCustomerBranch(UserConfiguration.getCompanyId(),datePopUp.getIbServiceId().getValue(), new AsyncCallback<Void>() {
					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						form.hideWaitSymbol();
					}

					@Override
					public void onSuccess(Void result) {
						// TODO Auto-generated method stub
						form.hideWaitSymbol();
						datePanel.hide();
						form.showDialogMessage("Update Process Started...");
					}
				});
			}
			if(event.getSource()==datePopUp.getBtnTwo()){
				datePanel.hide();
			}
		  
			
			if(event.getSource() == contractidpopup.getLblOk()) {
				if(contractidpopup.getObcontractidlist().getSelectedIndex()==0) {
					form.showDialogMessage("Please select contract Id");
				}
				else {
					reactonUpdateServiceAddressOk(contractidpopup.getObcontractidlist().getValue());
				}
			}
	  }
	  
	  
	  
	  

	@Override
	public void onSelection(SelectionEvent<Suggestion> event) {

		if(event.getSource().equals(form.getPersonInfoComposite().getId())||event.getSource().equals(form.getPersonInfoComposite().getName())||event.getSource().equals(form.getPersonInfoComposite().getPhone()))
		{
			if(form.getTbBranchName().getValue()!=null&&form.getPersonInfoComposite().getValue()!=null)
			{
				try {
					onSaveValidation();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		
		
	}

	
	

	@Override
	public void onValueChange(ValueChangeEvent<String> event) {

		if(event.getSource().equals(form.getTbBranchName())){
			if(form.getTbBranchName().getValue()!=null&&form.getPersonInfoComposite().getValue()!=null)
			{
				onSaveValidation();
			}
		}
		
		
	}

	private void onSaveValidation() {

		String branch=form.getTbBranchName().getValue().trim();
		int count=form.getPersonInfoComposite().getIdValue();
		
		MyQuerry querry=new MyQuerry();
		Company c=new Company();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		
		temp=new Filter();
		temp.setQuerryString("companyId");
		temp.setLongValue(c.getCompanyId());
		filtervec.add(temp);
		
		temp=new Filter();
		temp.setQuerryString("cinfo.count");
		temp.setIntValue(count);
		filtervec.add(temp);
		
		temp=new Filter();
		temp.setQuerryString("buisnessUnitName");
		temp.setStringValue(branch);
		filtervec.add(temp);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new CustomerBranchDetails());
		form.showWaitSymbol();
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onFailure(Throwable caught) {
				form.showDialogMessage("An Unexpected error occurred!");
				form.hideWaitSymbol();
				
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
					if(result.size()>0)
					{
						form.showDialogMessage("Branch Name with this customer is already added!");
						form.getPersonInfoComposite().clear();
					}
					form.hideWaitSymbol();
					
				}
			 });
		
	}
	
	private void reacttoUpdateServiceAddress() {
		int count=form.getPersonInfoComposite().getIdValue();
		contractidpopup.showPopUp();	
		contractidpopup.getObcontractidlist().MakeLiveWithDesendingSorting(contractidpopup.getQuerry(count));
		contractidpopup.showWaitSymbol();
		Timer timer = new Timer() {
			
			@Override
			public void run() {
				contractidpopup.hideWaitSymbol();
			}
		};
		timer.schedule(7000);
	}

	
	private void reactonUpdateServiceAddressOk(String strcontractId) {

		int contractId = Integer.parseInt(strcontractId);
		form.showWaitSymbol();
		customerbranchserviceupdate.updateCustomerBranchServiceAddress(model.getCompanyId(), contractId, model.getBusinessUnitName(), new AsyncCallback<String>() {

			@Override
			public void onFailure(Throwable arg0) {
				// TODO Auto-generated method stub
				form.hideWaitSymbol();

			}

			@Override
			public void onSuccess(String msg) {
				// TODO Auto-generated method stub
				form.hideWaitSymbol();
				form.showDialogMessage(msg);
				contractidpopup.hidePopUp();
			}
		});		
	}
	
}
