package com.slicktechnologies.client.views.customerbranchdetails.servicelocation;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchServiceLocation;

public class ServiceLocationTable extends SuperTable<CustomerBranchServiceLocation> {

	TextColumn<CustomerBranchServiceLocation> getIdCol;
	TextColumn<CustomerBranchServiceLocation> getCustomerNameCol;
	TextColumn<CustomerBranchServiceLocation> getCustomerBranchCol;
	TextColumn<CustomerBranchServiceLocation> getServiceLocationCol;
	TextColumn<CustomerBranchServiceLocation> getstatuCol;
	
	
	
	
	public ServiceLocationTable() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public void createTable() {
		getIdCol();
		getCustomerNameCol();
		getCustomerBranchCol();
		getServiceLocationCol();
		getstatuCol();
	}

	private void getIdCol() {
		getIdCol=new TextColumn<CustomerBranchServiceLocation>() {
			@Override
			public String getValue(CustomerBranchServiceLocation object) {
				return object.getCount()+"";
			}
		};
		table.addColumn(getIdCol,"Id");
		getIdCol.setSortable(true);
	}

	private void getCustomerNameCol() {
		getCustomerNameCol=new TextColumn<CustomerBranchServiceLocation>() {
			@Override
			public String getValue(CustomerBranchServiceLocation object) {
				return object.getCinfo().getFullName();
			}
		};
		table.addColumn(getCustomerNameCol,"Customer Name");
		getCustomerNameCol.setSortable(true);
	}

	private void getCustomerBranchCol() {
		getCustomerBranchCol=new TextColumn<CustomerBranchServiceLocation>() {
			@Override
			public String getValue(CustomerBranchServiceLocation object) {
				return object.getCustomerBranchName();
			}
		};
		table.addColumn(getCustomerBranchCol,"Customer Branch Name");
		getCustomerBranchCol.setSortable(true);
	}

	private void getServiceLocationCol() {
		getServiceLocationCol=new TextColumn<CustomerBranchServiceLocation>() {
			@Override
			public String getValue(CustomerBranchServiceLocation object) {
				return object.getServiceLocation();
			}
		};
		table.addColumn(getServiceLocationCol,"Service Location");
		getServiceLocationCol.setSortable(true);
	}

	private void getstatuCol() {
		getstatuCol=new TextColumn<CustomerBranchServiceLocation>() {
			@Override
			public String getValue(CustomerBranchServiceLocation object) {
				if(object.isStatus()){
					return "Active";
				}else{
					return "Inactive";	
				}
			}
		};
		table.addColumn(getstatuCol,"Status");
		getstatuCol.setSortable(true);
		
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addColumnSorting() {
		// TODO Auto-generated method stub
		super.addColumnSorting();
		
		addSortOnIdCol();
		addSortOnCustomerNameCol();
		addSortOnCustomerBranchCol();
		addSortOnServiceLocationCol();
		addSortOnstatuCol();
	}

	private void addSortOnIdCol() {
		List<CustomerBranchServiceLocation> list = getDataprovider().getList();
		columnSort = new ListHandler<CustomerBranchServiceLocation>(list);
		columnSort.setComparator(getIdCol,new Comparator<CustomerBranchServiceLocation>() {
			@Override
			public int compare(CustomerBranchServiceLocation e1,CustomerBranchServiceLocation e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCount() == e2.getCount()) {
						return 0;
					}
					if (e1.getCount() > e2.getCount()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addSortOnCustomerNameCol() {
		List<CustomerBranchServiceLocation> list = getDataprovider().getList();
		columnSort = new ListHandler<CustomerBranchServiceLocation>(list);
		columnSort.setComparator(getCustomerNameCol,new Comparator<CustomerBranchServiceLocation>() {
			@Override
			public int compare(CustomerBranchServiceLocation e1,CustomerBranchServiceLocation e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCinfo().getFullName()!= null&& e2.getCinfo().getFullName()!= null) {
						return e1.getCinfo().getFullName().compareTo(e2.getCinfo().getFullName());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);		
	}

	private void addSortOnCustomerBranchCol() {
		List<CustomerBranchServiceLocation> list = getDataprovider().getList();
		columnSort = new ListHandler<CustomerBranchServiceLocation>(list);
		columnSort.setComparator(getCustomerBranchCol,new Comparator<CustomerBranchServiceLocation>() {
			@Override
			public int compare(CustomerBranchServiceLocation e1,CustomerBranchServiceLocation e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCustomerBranchName()!= null&& e2.getCustomerBranchName()!= null) {
						return e1.getCustomerBranchName().compareTo(e2.getCustomerBranchName());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);	
	}

	private void addSortOnServiceLocationCol() {
		List<CustomerBranchServiceLocation> list = getDataprovider().getList();
		columnSort = new ListHandler<CustomerBranchServiceLocation>(list);
		columnSort.setComparator(getServiceLocationCol,new Comparator<CustomerBranchServiceLocation>() {
			@Override
			public int compare(CustomerBranchServiceLocation e1,CustomerBranchServiceLocation e2) {
				if (e1 != null && e2 != null) {
					if (e1.getServiceLocation()!= null&& e2.getServiceLocation()!= null) {
						return e1.getServiceLocation().compareTo(e2.getServiceLocation());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);		
	}

	private void addSortOnstatuCol() {
		List<CustomerBranchServiceLocation> list = getDataprovider().getList();
		columnSort = new ListHandler<CustomerBranchServiceLocation>(list);
		columnSort.setComparator(getstatuCol,new Comparator<CustomerBranchServiceLocation>() {
			@Override
			public int compare(CustomerBranchServiceLocation e1,CustomerBranchServiceLocation e2) {
				if (e1 != null && e2 != null) {
					if (e1.isStatus() == e2.isStatus()) {
						return 0;
					}else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	

}
