package com.slicktechnologies.client.views.customerbranchdetails.servicelocation;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Index;

@Embed
public class Area implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7352462545162904623L;
	
	@Index
	String area;
	double plannedservices;
	String frequency;
	
	public Area(){
		area="";
		area = "";
		frequency ="";
	}
	
	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public double getPlannedservices() {
		return plannedservices;
	}

	public void setPlannedservices(double plannedservices) {
		this.plannedservices = plannedservices;
	}

	public String getFrequency() {
		return frequency;
	}

	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}
	
	
}
