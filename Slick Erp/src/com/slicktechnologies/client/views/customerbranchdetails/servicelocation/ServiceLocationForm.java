package com.slicktechnologies.client.views.customerbranchdetails.servicelocation;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchServiceLocation;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class ServiceLocationForm extends FormTableScreen<CustomerBranchServiceLocation> implements ClickHandler {
	PersonInfoComposite personInfoComposite;
	TextBox tbcustomerBranch;
	TextBox tbServiceLocation;
	TextArea taDescription;
	CheckBox cbStatus;
	
	AreaTable areaTbl;
	
	TextBox tbarea,tbFrequency;
	DoubleBox dbnoOfServices;
	Button btnAdd;
	
	int custBranchId=0;
	
	CustomerBranchServiceLocation serviceLocationObj;
	
	
	public ServiceLocationForm  (SuperTable<CustomerBranchServiceLocation> table, int mode,boolean captionmode) {
		super(table, mode, captionmode);
		createGui();
	}
	
	public ServiceLocationForm  (SuperTable<CustomerBranchServiceLocation> table, int mode,boolean captionmode, boolean popupflag) {
		super(table, mode, captionmode,popupflag);
		createGui();
	}
	
	public ServiceLocationForm(ServiceLocationTable gentable) {
		super(gentable);
		createGui();
	}

	private void initalizeWidget(){
//		personInfoComposite=AppUtility.customerInfoComposite(new Customer());
		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new Customer());
		personInfoComposite=new PersonInfoComposite(querry,false,true,false);
		personInfoComposite.getCustomerId().getHeaderLabel().setText("Customer ID");
		personInfoComposite.getCustomerName().getHeaderLabel().setText("Customer Name");
		personInfoComposite.getCustomerCell().getHeaderLabel().setText("Customer Cell");
		
		personInfoComposite.btnLocation.setVisible(false);
		personInfoComposite.setEnable(false);
		
		tbcustomerBranch=new TextBox();
		tbcustomerBranch.setEnabled(false);
		
		tbServiceLocation=new TextBox();
		
		areaTbl=new AreaTable();
		
		taDescription=new TextArea();
		cbStatus=new CheckBox();
		cbStatus.setValue(true);
		
		tbarea=new TextBox();
		btnAdd=new Button("Add");
		btnAdd.addClickHandler(this);
		custBranchId=0;
		
		dbnoOfServices = new DoubleBox();
		tbFrequency = new TextBox();
	}
	
	@Override
	public void createScreen() {
		initalizeWidget();
//		this.processlevelBarNames=new String[]{"New"};
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		
		FormField fgroupingCustomerInformation=fbuilder.setlabel("Customer Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder("",personInfoComposite);
		FormField fpersonInfoComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		
		FormField fgroupingCompanyInformation=fbuilder.setlabel("Service Location Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder("Customer Branch Name",tbcustomerBranch);
		FormField ftbBranchName= fbuilder.setMandatory(false).setMandatoryMsg("Branch Name is mandatory!").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Service Location",tbServiceLocation);
		FormField ftbServiceLocation= fbuilder.setMandatory(true).setMandatoryMsg("Service Location is mandatory!").setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("Status",cbStatus);
		FormField fcheckBoxStatus= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Description",taDescription);
		FormField ftaDescription= fbuilder.setRowSpan(0).setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder("Area",tbarea);
		FormField ftbarea= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Planned Service",dbnoOfServices);
		FormField fdbnoOfServices= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Frequency",tbFrequency);
		FormField ftbFrequency= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",btnAdd);
		FormField fbtnAdd= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",areaTbl.getTable());
		FormField fareaTbl= fbuilder.setRowSpan(0).setColSpan(3).build();
		
		FormField[][] formfield = { 
//				{fgroupingCustomerInformation},
//				{fpersonInfoComposite},
				{fgroupingCompanyInformation},
//				{ftbBranchName,ftbServiceLocation,fcheckBoxStatus},
				{ftbServiceLocation,fcheckBoxStatus},
				{ftbarea,fdbnoOfServices,ftbFrequency,fbtnAdd},
				{fareaTbl},
				{ftaDescription},
		};
		this.fields=formfield;	
	}
	
	@Override
	public void updateModel(CustomerBranchServiceLocation model) {
		if(personInfoComposite.getValue()!=null)
			model.setCinfo(personInfoComposite.getValue());
		if(tbcustomerBranch.getValue()!=null)
			model.setCustomerBranchName(tbcustomerBranch.getValue());
		if(tbServiceLocation.getValue()!=null)
			model.setServiceLocation(tbServiceLocation.getValue());
		if(cbStatus.getValue()!=null)
			model.setStatus(cbStatus.getValue());
		if(taDescription.getValue()!=null)
			model.setDescription(taDescription.getValue());
		if(areaTbl.getValue()!=null){
			model.setAreaList2(areaTbl.getValue());
		}
		else{
			if(areaTbl.getValue()!=null){
				model.setAreaList(areaTbl.getValueAsString());
			}
		}
		
		model.setCustBranchId(custBranchId);
		
		serviceLocationObj=model;
			
		presenter.setModel(model);
	}


	@Override
	public void updateView(CustomerBranchServiceLocation view){
		
		serviceLocationObj=view;
		
		if(view.getCinfo()!=null){
			personInfoComposite.setValue(view.getCinfo());
		}
		if(view.getCustomerBranchName()!=null){
			tbcustomerBranch.setValue(view.getCustomerBranchName());
		}
		if(view.getServiceLocation()!=null){
			tbServiceLocation.setValue(view.getServiceLocation());
		}
		cbStatus.setValue(view.isStatus());
		if(view.getDescription()!=null){
			taDescription.setValue(view.getDescription());
		}
		if(view.getAreaList()!=null){
			if(view.getAreaList2()!=null && view.getAreaList2().size()>0){
				areaTbl.setValue(view.getAreaList2());
			}
			else{
				areaTbl.setValueToTbl(view.getAreaList());
			}
		}
		if(view.getAreaList2()!=null && view.getAreaList2().size()>0){
			areaTbl.setValue(view.getAreaList2());
		}
		
		custBranchId=view.getCustBranchId();
		presenter.setModel(view);
	}
	
	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW){
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}
			for(int k=0;k<menus.length;k++){
				String text=menus[k].getText();
				if(text.contains("Save")||text.contains("Discard")){
					menus[k].setVisible(true); 
				}
				else{
					menus[k].setVisible(false); 
				}
			}
		}
		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT){
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}
			for(int k=0;k<menus.length;k++){
				String text=menus[k].getText();
				if(text.contains("Save")||text.contains("Discard")){
					menus[k].setVisible(true); 
				}
				else{
					menus[k].setVisible(false);  
				}
			}
		}
		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW){
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Edit")||text.contains("Discard")){
					menus[k].setVisible(true); 
				}else{
					menus[k].setVisible(false);  
				}
			}
		}
		if(isPopUpAppMenubar()){
			return;
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.SERVICELOCATION,LoginPresenter.currentModule.trim());
	}
	
	@Override
	public void clear() {
		super.clear();
		cbStatus.setValue(true);
		areaTbl.connectToLocal();
		
		if(serviceLocationObj!=null){
			personInfoComposite.setValue(serviceLocationObj.getCinfo());
			tbcustomerBranch.setValue(serviceLocationObj.getCustomerBranchName());
			custBranchId=serviceLocationObj.getCustBranchId();
		}
		
	}
	
	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		areaTbl.setEnable(state);
		tbcustomerBranch.setEnabled(false);
		personInfoComposite.setEnable(false);
	}

	@Override
	public void onClick(ClickEvent event) {
		if(event.getSource()==btnAdd){
			if(tbarea.getValue().equals("")){
				showDialogMessage("Please enter area to add!");
				return;
			}
			if(areaTbl.getValue()!=null){
				for(Area obj:areaTbl.getValue()){
					if(obj.getArea().equals(tbarea.getValue())){
						showDialogMessage(obj.getArea() +" is already added.");
						return;
					}
				}
			}
			
			Area obj=new Area();
			obj.setArea(tbarea.getValue());
			if(dbnoOfServices.getValue()!=null)
				obj.setPlannedservices(dbnoOfServices.getValue());
			if(tbFrequency.getValue()!=null)
				obj.setFrequency(tbFrequency.getValue());
			
			areaTbl.getDataprovider().getList().add(obj);
			areaTbl.getTable().redraw();
			tbarea.setValue("");
			dbnoOfServices.setValue(0d);
			tbFrequency.setValue("");
			
		}
	}

	public PersonInfoComposite getPersonInfoComposite() {
		return personInfoComposite;
	}

	public void setPersonInfoComposite(PersonInfoComposite personInfoComposite) {
		this.personInfoComposite = personInfoComposite;
	}

	public TextBox getTbcustomerBranch() {
		return tbcustomerBranch;
	}

	public void setTbcustomerBranch(TextBox tbcustomerBranch) {
		this.tbcustomerBranch = tbcustomerBranch;
	}

	public CheckBox getCbStatus() {
		return cbStatus;
	}

	public void setCbStatus(CheckBox cbStatus) {
		this.cbStatus = cbStatus;
	}

	public int getCustBranchId() {
		return custBranchId;
	}

	public void setCustBranchId(int custBranchId) {
		this.custBranchId = custBranchId;
	}
	
	
	
}
