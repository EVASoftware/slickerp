package com.slicktechnologies.client.views.customerbranchdetails.servicelocation;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;

public class AreaTable extends SuperTable<Area> {

	TextColumn<Area> viewAreaCol;
	TextColumn<Area> viewPlannedServiceCol;
	TextColumn<Area> viewFrequencyCol;

	Column<Area,String> deleteCol;
	
	@Override
	public void createTable() {
		viewAreaCol();
		viewPlannedServicesCol();
		viewFrequencyCol();
		deleteCol();
	}



	private void deleteCol() {
		ButtonCell btn=new ButtonCell();
		deleteCol=new Column<Area,String>(btn) {
			@Override
			public String getValue(Area object) {
				return "Delete";
			}
		};
		table.addColumn(deleteCol,"");
		
		deleteCol.setFieldUpdater(new FieldUpdater<Area, String>() {
			@Override
			public void update(int index, Area object, String value) {
				getDataprovider().getList().remove(index);
				table.redraw();
			}
		});
	}

	private void viewAreaCol() {
		viewAreaCol=new TextColumn<Area>() {
			@Override
			public String getValue(Area object) {
				if(object.getArea()!=null){
					return object.getArea();
				}
				return "";
			}
		};
		table.addColumn(viewAreaCol,"Area");
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		int tablecolcount = this.table.getColumnCount();
		for (int i = tablecolcount - 1; i > -1; i--)
			table.removeColumn(i);
		if (state == true){
			viewAreaCol();
			viewPlannedServicesCol();
			viewFrequencyCol();
			deleteCol();
		}
		if (state == false){
			viewAreaCol();
			viewPlannedServicesCol();
			viewFrequencyCol();
		}
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
	
	public ArrayList<String> getValueAsString(){
		ArrayList<String> areaList=new ArrayList<String>();
		for(Area obj:getDataprovider().getList()){
			areaList.add(obj.getArea());
		}
		return areaList;
	}
	
	public void setValueToTbl(ArrayList<String> areaList){
		List<Area> list=new ArrayList<Area>();
		for(String obj:areaList){
			Area area=new Area();
			area.setArea(obj);
			list.add(area);
		}
		
		setValue(list);
	}

	private void viewPlannedServicesCol() {

		viewPlannedServiceCol=new TextColumn<Area>() {
			@Override
			public String getValue(Area object) {
				if(object.getPlannedservices()!=0){
					return object.getPlannedservices()+"";
				}
				return "";
			}
		};
		table.addColumn(viewPlannedServiceCol,"Planned Services");
	
	
	}

	private void viewFrequencyCol() {

		viewFrequencyCol=new TextColumn<Area>() {
			@Override
			public String getValue(Area object) {
				if(object.getFrequency()!=null && !object.getFrequency().equals("")){
					return object.getFrequency()+"";
				}
				return "";
			}
		};
		table.addColumn(viewFrequencyCol,"Frequency");
	
	}
}
