package com.slicktechnologies.client.views.customerbranchdetails.servicelocation;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchServiceLocation;

public class ServiceLocationPresenter extends FormTableScreenPresenter<CustomerBranchServiceLocation> {

	ServiceLocationForm form;
	public ServiceLocationPresenter(FormTableScreen<CustomerBranchServiceLocation> view,CustomerBranchServiceLocation model) {
		super(view, model);
		
		form=(ServiceLocationForm) view;
		form.getSupertable().connectToLocal();
//		form.retriveTable(getQuery());
		form.setPresenter(this);
	}

	public ServiceLocationPresenter(ServiceLocationForm view,CustomerBranchServiceLocation model,MyQuerry query) {
		// TODO Auto-generated constructor stub
		super(view, model);
		form=(ServiceLocationForm) view;
		form.getSupertable().connectToLocal();
		form.retriveTable(query);
		form.setPresenter(this);
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();
		
//		if(text.equals("New")){
//			reactOnNew();
//		}
		
	}
	
	private void reactOnNew() {
		form.setToNewState();
		this.initalize();
	}

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void makeNewModel() {
		model=new CustomerBranchServiceLocation();
	}
	
	public static ServiceLocationForm initalize(){
		ServiceLocationTable gentableScreen=new ServiceLocationTable();
		ServiceLocationForm  form=new  ServiceLocationForm(gentableScreen,FormTableScreen.UPPER_MODE,true);
		gentableScreen.setView(form);
		gentableScreen.applySelectionModle();
		
//		CustomerBranchPresenterSearchProxy.staticSuperTable=gentableScreen;
//		CustomerBranchPresenterSearchProxy searchpopup=new CustomerBranchPresenterSearchProxy(false);
//		form.setSearchpopupscreen(searchpopup);
		
		ServiceLocationPresenter presenter=new ServiceLocationPresenter(form,new CustomerBranchServiceLocation());
		form.setToNewState();
		AppMemory.getAppMemory().stickPnel(form);
		return form;
	}

}
