package com.slicktechnologies.client.views.customerbranchdetails;

import java.util.Vector;

import com.google.gwt.dom.client.Style.Unit;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;

public class CustomerBranchPresenterSearchProxy extends SearchPopUpScreen<CustomerBranchDetails> {
	
	public PersonInfoComposite personInfo;
	
	/** Date 02-07-2018 added by vijay
	 * Des :-  for branch level restriction 
	 * Requirement :- NBHC
	 **/
	ObjectListBox<Branch> olbBranch;
	/**
	 * ends here
	 */
	
	public CustomerBranchPresenterSearchProxy()
	{
		super();
		createGui();
	}
	
	public CustomerBranchPresenterSearchProxy(boolean b) {
		super(b);
		createGui();
	}
	public void initWidget()
	{
		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new Customer());
		personInfo=new PersonInfoComposite(querry,false);
		

		olbBranch = new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbBranch);
	}
	
	public void createScreen()
	{
		initWidget();
		FormFieldBuilder builder;
		builder = new FormFieldBuilder("",personInfo);
		FormField fpersonInfo= builder.setMandatory(false).setRowSpan(1).setColSpan(5).build();
		
		builder = new FormFieldBuilder("Branch",olbBranch);
		olbBranch.getElement().getStyle().setWidth(200, Unit.PX);
		FormField fbranch = builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		this.fields=new FormField[][]{
				{fpersonInfo},
				{fbranch},
		};
	
	}
	
	public MyQuerry getQuerry()
	{
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		
		if(personInfo.getIdValue()!=-1)
		  {
		  temp=new Filter();
		  temp.setIntValue(personInfo.getIdValue());
		  temp.setQuerryString("cinfo.count");
		  filtervec.add(temp);
		  }
		  
		  if(!(personInfo.getFullNameValue().equals("")))
		  {
		  temp=new Filter();
		  temp.setStringValue(personInfo.getFullNameValue());
		  temp.setQuerryString("cinfo.fullName");
		  filtervec.add(temp);
		  }
		  if(personInfo.getCellValue()!=-1l)
		  {
		  temp=new Filter();
		  temp.setLongValue(personInfo.getCellValue());
		  temp.setQuerryString("cinfo.cellNumber");
		  filtervec.add(temp);
		  }
		  
		  
		  /** Date 02-07-2018 added by vijay **/
		  if(olbBranch.getSelectedIndex()!=0){
			  temp = new Filter();
			  temp.setStringValue(olbBranch.getValue());
			  temp.setQuerryString("branch");
			  filtervec.add(temp);
		  }
		  /** Ends here **/
		
		  MyQuerry querry= new MyQuerry();
			querry.setFilters(filtervec);
			querry.setQuerryObject(new CustomerBranchDetails());
			return querry;
	}

	@Override
	public boolean validate() {

		/** Date 02-07-2018 added by vijay
		 * Des :-  for branch level restriction 
		 * Requirement :- NBHC
		 **/
		
		if(LoginPresenter.branchRestrictionFlag)
		{
			if(olbBranch.getSelectedIndex()==0)
			{
				showDialogMessage("Select Branch");
				return false;
			}
		}
		/**
		 * ends here
		 */
		
		return true;
	}

}
