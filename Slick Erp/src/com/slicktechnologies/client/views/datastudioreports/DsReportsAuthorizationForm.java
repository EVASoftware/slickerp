package com.slicktechnologies.client.views.datastudioreports;

import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.formula.ptg.TblPtg;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.DsReport;
import com.slicktechnologies.shared.common.DsReportDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.humanresourcelayer.Department;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.role.UserRole;

public class DsReportsAuthorizationForm extends  FormTableScreen<DsReportDetails>{

	ObjectListBox<UserRole> olbRoleName;
	ObjectListBox<DsReport> olbReportName;
	DsReportTable dsTable;
	DsReportDetails dsReportObj;
	CheckBox chkStatus;
	public DsReportsAuthorizationForm(){
		super();
		createGui();
	}
	
	public DsReportsAuthorizationForm(SuperTable<DsReportDetails> table, int mode, boolean captionmode){
		super(table, mode, captionmode);
		createGui();
	}
	
	private void initalizeWidget()
	{
		olbReportName=new ObjectListBox<DsReport>();
		makeReportNameListBoxLive(olbReportName);
		
		olbRoleName=new ObjectListBox<UserRole>();
		MyQuerry qu= new MyQuerry();
		Filter filter=new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		qu.setQuerryObject(new UserRole());
		olbRoleName.MakeLive(qu);

		dsTable=new DsReportTable(true);
		chkStatus = new CheckBox();
		chkStatus.setValue(true);
	}
	
	public static void makeReportNameListBoxLive(ObjectListBox<DsReport> olbReportName)
	{
		MyQuerry querry=new MyQuerry();
		Filter filter=new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		querry.getFilters().add(filter);
		
		querry.setQuerryObject(new DsReport());
		olbReportName.MakeLive(querry);
	}

	
	

	@Override
	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.REPORTAUTHORISATION,LoginPresenter.currentModule.trim());
	}

	@Override
	public void createScreen() {
		initalizeWidget();
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingReportDetails=fbuilder.setlabel("Report Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Report Name" , olbReportName);
		FormField ftbreportName = fbuilder.setMandatory(false).setMandatoryMsg("").setRowSpan(0).setColSpan(0).build();
		
	
		fbuilder = new FormFieldBuilder("Role" ,olbRoleName);
		FormField ftbRoleName= fbuilder.setMandatory(false).setMandatoryMsg("").setRowSpan(0).setColSpan(0).build();	
		
		
		
		fbuilder = new FormFieldBuilder("Status",chkStatus);
		FormField fchkStatus = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		FormField[][] formfield = { 
				{fgroupingReportDetails},
				{ ftbRoleName,ftbreportName ,fchkStatus },
		};
		this.fields=formfield;
	}

	
	

	@Override
	public void updateModel(DsReportDetails model) {
		
		
		
		if(olbReportName.getValue()!=null)
			model.setReportName(olbReportName.getValue());
		
		if(olbRoleName.getValue()!=null)
			model.setRole(olbRoleName.getSelectedItem().toString());
		
		
		if(chkStatus.getValue()!=null)
			model.setStatus(chkStatus.getValue());
		
		DsReport obj=olbReportName.getSelectedItem();
		
		if(obj.getEntityName()!=null){
			model.setEntityName(obj.getEntityName());
		}
		
		
		
		
		
		dsReportObj=model;
		presenter.setModel(model);
		
		
	}

	@Override
	public void updateView(DsReportDetails view) {
       dsReportObj=view;
		
		if(view.getReportName()!=null)
			olbReportName.setValue(view.getReportName());
		
		if(view.getRole()!=null)
			olbRoleName.setValue(view.getRole().toString());
		
		chkStatus.setValue(view.getStatus());
		
		
		
		presenter.setModel(view);
		
		
	}

	public DsReportTable getDsTable() {
		return dsTable;
	}

	public void setDsTable(DsReportTable dsTable) {
		this.dsTable = dsTable;
	}

//	@Override
//	public void onClick(ClickEvent event) {
//		if(event.getSource().equals(btnAdd)){
//			if(vaildateFields()){
//				DsReport obj=new DsReport();
//				obj.setReportName(olbReportName.getValue());
//				obj.setRole(olbRoleName.getValue());
//				dsTable.getDataprovider().getList().add(obj);
//				
//			}
//		}		
//	}

	private boolean vaildateFields() {
		String msg = "";
		if (olbRoleName.getSelectedIndex() == 0) {
			msg = msg + "Please select Role." + "\n";
		}
		if (olbReportName.getSelectedIndex() == 0) {
			msg = msg + "Please fill Report name." + "\n";
		}
		if (!msg.equals("")) {
			showDialogMessage(msg);
			return false;
		}

		return true;
	}
	@Override
	public void clear() 
	{	
		super.clear();
		chkStatus.setValue(true);
	}
}

