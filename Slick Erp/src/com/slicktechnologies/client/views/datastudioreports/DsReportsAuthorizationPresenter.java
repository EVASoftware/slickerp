package com.slicktechnologies.client.views.datastudioreports;

import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.DsReport;
import com.slicktechnologies.shared.common.DsReportDetails;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.role.UserRole;

public class DsReportsAuthorizationPresenter extends FormTableScreenPresenter<DsReportDetails> {
	
	DsReportsAuthorizationForm form;
	
	final GenricServiceAsync async=GWT.create(GenricService.class);
	public DsReportsAuthorizationPresenter(FormTableScreen<DsReportDetails> view,DsReportDetails model) {
		super(view, model);
		
		form = (DsReportsAuthorizationForm) view;
		form.getSupertable().connectToLocal();
		form.retriveTable(getDsReportConfigQuery());
		form.setPresenter(this);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.REPORTAUTHORISATION,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}

	
	
	
	public MyQuerry getDsReportConfigQuery()
	{
		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new DsReportDetails());
		return quer;
	}
	
	
	
	public static DsReportsAuthorizationForm initalize() {
		
		DsReportAuthorizationTable gentableScreen=new DsReportAuthorizationTable();
		
		DsReportsAuthorizationForm form = new DsReportsAuthorizationForm(gentableScreen, FormTableScreen.UPPER_MODE, true);
		gentableScreen.setView(form);
		gentableScreen.applySelectionModle();
		DsReportsAuthorizationPresenter  presenter=new  DsReportsAuthorizationPresenter(form,new DsReportDetails());
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Reports/Report Authorization",Screen.REPORTAUTHORISATION);
		AppMemory.getAppMemory().stickPnel(form);
		return form;
		
	}

	
	@Override
	public void reactToProcessBarEvents(ClickEvent e) {

		InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();
		
	}
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		super.onClick(event);
		
		
	}
	@Override
	public void reactOnPrint() {
	}

	@Override
	public void reactOnEmail() {
	}

	@Override
	protected void makeNewModel() {
		model=new DsReportDetails();
		
	}
	
	public void setModel(DsReportDetails entity)
	{
		model=entity;
	}


	
	
	
}
