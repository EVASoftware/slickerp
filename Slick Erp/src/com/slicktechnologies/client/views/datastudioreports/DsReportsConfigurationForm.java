package com.slicktechnologies.client.views.datastudioreports;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.DsReport;
import com.slicktechnologies.shared.common.DummyEntityOnlyForScreen;
import com.slicktechnologies.shared.common.helperlayer.SmsConfiguration;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.role.ScreenAuthorization;
import com.slicktechnologies.shared.common.role.UserRole;

public class DsReportsConfigurationForm extends FormTableScreen<DsReport>{

	TextBox tbreportName;
	TextBox tbEntityName;
	TextBox tbReportLink;
	Button btnAdd;
	DsReportTable dsTable;
	DsReport dsReportObj;
	CheckBox chkStatus;
	
	public DsReportsConfigurationForm(SuperTable<DsReport> table, int mode, boolean captionmode){
		super(table, mode, captionmode);
		createGui();
	}
	
	
	
	private void initalizeWidget()
	{
		tbreportName=new TextBox();
		tbEntityName=new TextBox();
		tbReportLink=new TextBox();
		btnAdd = new Button("ADD");
		chkStatus = new CheckBox();
		chkStatus.setValue(true);
		dsTable=new DsReportTable();
	}
	
	
	
	
	

	@Override
	public void createScreen() {
		super.createScreen();
		initalizeWidget();
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingReportDetails=fbuilder.setlabel("Report Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("* Report Name" , tbreportName);
		FormField ftbreportName = fbuilder.setMandatory(true).setMandatoryMsg("Report Name is mandatory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Entity Name" , tbEntityName);
		FormField ftbEntityName = fbuilder.setMandatory(true).setMandatoryMsg("Entity Name is mandatory").setRowSpan(0).setColSpan(0).build();
	
		fbuilder = new FormFieldBuilder("* Report Link" , tbReportLink);
		FormField ftbReportLink= fbuilder.setMandatory(true).setMandatoryMsg("Report Link is mandatory").setRowSpan(0).setColSpan(0).build();	
		
		fbuilder = new FormFieldBuilder("" , btnAdd);
		FormField fbtnAdd= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();	
		
		fbuilder = new FormFieldBuilder("" , dsTable.getTable());
		FormField fdsTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();	
		
		fbuilder = new FormFieldBuilder("Status",chkStatus);
		FormField fchkStatus = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		FormField[][] formfield = { 
				{fgroupingReportDetails},
				{ ftbreportName, ftbEntityName,ftbReportLink, fchkStatus },
		};
		this.fields=formfield;
	}

	
	

	@Override
	public void updateModel(DsReport model) {
		if(tbreportName.getValue()!=null)
			model.setReportName(tbreportName.getValue());
		
		if(tbEntityName.getValue()!=null)
			model.setEntityName(tbEntityName.getValue());
		
		if(tbReportLink.getValue()!=null)
			model.setReportLink(tbReportLink.getValue());
		
		if(chkStatus.getValue()!=null)
			model.setStatus(chkStatus.getValue());
		
		
		
		
		
		dsReportObj=model;
		presenter.setModel(model);
		
	}

	@Override
	public void updateView(DsReport view) {
		dsReportObj=view;
		
		if(view.getReportName()!=null)
			tbreportName.setValue(view.getReportName());
		
		if(view.getEntityName()!=null)
			tbEntityName.setValue(view.getEntityName());
		
		if(view.getReportLink()!=null)
			tbReportLink.setValue(view.getReportLink());
		
		chkStatus.setValue(view.getStatus());
		
		presenter.setModel(view);
		
	}

	public DsReportTable getDsTable() {
		return dsTable;
	}

	public void setDsTable(DsReportTable dsTable) {
		this.dsTable = dsTable;
	}
	@Override
	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.REPORTSCONFIGURATIONS,LoginPresenter.currentModule.trim());
	}

//	@Override
//	public void onClick(ClickEvent event) {
//		if(event.getSource().equals(btnAdd)){
//			if(vaildateFields()){
//				DsReport obj=new DsReport();
//				obj.setReportName(tbreportName.getValue());
//				obj.setEntityName(tbEntityName.getValue());
//				obj.setReportLink(tbReportLink.getValue());
//				
//				dsTable.getDataprovider().getList().add(obj);
//				
//			}
//		}
//		
//		
//	}

	private boolean vaildateFields() {
		
		
		String msg="";
		if(tbreportName.getValue().equals("")){
			msg=msg+"Please fill Report Name."+"\n";
		}
		if(tbEntityName.getValue().equals("")){
			msg=msg+"Please fill Entity Name."+"\n";
		}
		if(tbReportLink.getValue().equals("")){
			msg=msg+"Please put Report Link."+"\n";
		}
		if(!msg.equals("")){
			showDialogMessage(msg);
			return false;
		}
		return true;
	}
	@Override
	public void clear() 
	{	
		super.clear();
		chkStatus.setValue(true);
	}
}
