package com.slicktechnologies.client.views.datastudioreports;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.DsReport;
import com.slicktechnologies.shared.common.DsReportDetails;

public class DsReportAuthorizationTable extends SuperTable<DsReportDetails> {

	TextColumn<DsReportDetails> reportNameColumn;
	TextColumn<DsReportDetails> entityNameColumn;
	TextColumn<DsReportDetails> statusColumn;
	TextColumn<DsReportDetails> roleName;

	public DsReportAuthorizationTable()
	{
		super();
		table.setWidth("100%");
		table.setHeight("700px");
		
	}
	
	
	
	
	@Override
	public void createTable() {
		addColumnRoleName();
		addColumnReportName();
		addColumnEntityName();
		addColumnStatus();
	}

	private void addColumnStatus() {
		
		statusColumn = new TextColumn<DsReportDetails>() {
			
			@Override
			public String getValue(DsReportDetails object) {
				if(object.getStatus()==true)
				return "Active";
				else return "InActive";
			}
		};
		
		table.addColumn(statusColumn,"Status");
		table.setColumnWidth(statusColumn,100,Unit.PX);
		statusColumn.setSortable(true);
	}

	private void addColumnReportName() {

		reportNameColumn = new TextColumn<DsReportDetails>() {
			@Override
			public String getValue(DsReportDetails object) {
				return object.getReportName() + " ";
			}
		};
		table.addColumn(reportNameColumn, "Report Name");
		reportNameColumn.setSortable(true);
	}

	private void addColumnEntityName() {

		entityNameColumn = new TextColumn<DsReportDetails>() {
			@Override
			public String getValue(DsReportDetails object) {
				return object.getEntityName() + " ";
			}
		};
		table.addColumn(entityNameColumn, "Entity Name");
		entityNameColumn.setSortable(true);
	}

	private void addColumnRoleName() {

		roleName = new TextColumn<DsReportDetails>() {
			@Override
			public String getValue(DsReportDetails object) {
				return object.getRole();
			}
		};
		table.addColumn(roleName, "Role");
		roleName.setSortable(true);
	}

	@Override
	protected void initializekeyprovider() {

	}

	@Override
	public void addFieldUpdater() {

	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub

	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub

	}
	@Override
	public void addColumnSorting() {
		super.addColumnSorting();
		
		getSortingReportNameColumn();
		getSortingEntityNameColumn();
		getSortingRoleNameColumn();
	}




	private void getSortingRoleNameColumn() {
		List<DsReportDetails> list=getDataprovider().getList();
		columnSort=new ListHandler<DsReportDetails>(list);
		columnSort.setComparator(reportNameColumn, new Comparator<DsReportDetails>()
				{
			@Override
			public int compare(DsReportDetails e1,DsReportDetails e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getReportName()!=null && e2.getReportName()!=null){
						return e1.getReportName().compareTo(e2.getReportName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
		
	}




	private void getSortingEntityNameColumn() {
		List<DsReportDetails> list=getDataprovider().getList();
		columnSort=new ListHandler<DsReportDetails>(list);
		columnSort.setComparator(entityNameColumn, new Comparator<DsReportDetails>()
				{
			@Override
			public int compare(DsReportDetails e1,DsReportDetails e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getEntityName()!=null && e2.getEntityName()!=null){
						return e1.getEntityName().compareTo(e2.getEntityName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
		
	}




	private void getSortingReportNameColumn() {
		List<DsReportDetails> list=getDataprovider().getList();
		columnSort=new ListHandler<DsReportDetails>(list);
		columnSort.setComparator(roleName, new Comparator<DsReportDetails>()
				{
			@Override
			public int compare(DsReportDetails e1,DsReportDetails e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getRole()!=null && e2.getRole()!=null){
						return e1.getRole().compareTo(e2.getRole());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
		
	}

}
