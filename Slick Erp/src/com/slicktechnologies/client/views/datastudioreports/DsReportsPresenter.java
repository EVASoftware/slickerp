package com.slicktechnologies.client.views.datastudioreports;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreen;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreenPresenter;
import com.slicktechnologies.client.documenthistory.DocumentHistoryListForm;
import com.slicktechnologies.client.documenthistory.DocumentHistoryListPresenter;
import com.slicktechnologies.client.documenthistory.DocumentHistorySearchPopUp;
import com.slicktechnologies.client.documenthistory.DocumentHistoryTable;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.DsReport;
import com.slicktechnologies.shared.common.DsReportDetails;
import com.slicktechnologies.shared.common.android.RegisterDevice;
import com.slicktechnologies.shared.common.documenthistorydetails.DocumentHistory;
import com.slicktechnologies.shared.common.helperlayer.LoggedIn;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.role.UserRole;
import com.smartgwt.client.util.SC;

public class DsReportsPresenter extends TableScreenPresenter<DsReport> {
	
	TableScreen<DsReport>form;
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	
	public DsReportsPresenter(TableScreen<DsReport> view,
			DsReport model) {
		super(view, model);
		form=view;
		if(LoginPresenter.myUserEntity.getRole().getRoleName().equalsIgnoreCase("ADMIN")){
			view.retriveTable(getDsReportQuery(null));
		}else{
			getReportsAsPerRole(view);
		}
		
		
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.REPORTS,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
		
	}
	
	public DsReportsPresenter(TableScreen<DsReport> view,
			DsReport model,MyQuerry querry) {
		super(view, model);
		form=view;
		view.retriveTable(querry);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.REPORTS,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
		
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();
		
		if(text.equals("")){
			reactTo();
		}
	}

	public void reactOnPrint()
	{
	
	}
	
//	@Override
//	public void reactOnDownload() {
//		ArrayList<DsReport> docarray=new ArrayList<DsReport>();
//		List<DsReport> list=form.getSuperTable().getListDataProvider().getList();
//		
//		docarray.addAll(list); 
//		
//		csvservice.setDocumentHistoryList(docarray, new AsyncCallback<Void>() {
//
//			@Override
//			public void onFailure(Throwable caught) {
//				System.out.println("RPC call Failed"+caught);
//			}
//
//			@Override
//			public void onSuccess(Void result) {
//				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
//				final String url=gwt + "csvservlet"+"?type="+79;
//				Window.open(url, "test", "enabled");
//			}
//		});
//	}

	@Override
	public void reactOnEmail() {
		
		
	}

	/*
	 * Method template to make a new model
	 */
	@Override
	protected void makeNewModel() {
		
		model=new DsReport();
	}
	
	public static void initalize()
	{
		DsReportTable table=new DsReportTable(true);
		DsReportsForm form=new DsReportsForm(table);
		DsReportsPresenter presenter=new DsReportsPresenter(form, new DsReport());
		form.setToNewState();
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Reports/Reports",Screen.REPORTS);
		AppMemory.getAppMemory().stickPnel(form);
	}
	
	public static void initalize(MyQuerry querry)
	{
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Reports/Reports",Screen.REPORTS);
		DsReportTable table=new DsReportTable(false);
		DsReportsForm form=new DsReportsForm(table);
//		DocumentHistorySearchPopUp.staticSuperTable=table;
//		DocumentHistorySearchPopUp searchPopUp=new DocumentHistorySearchPopUp(false);
//		form.setSearchpopupscreen(searchPopUp);
		DsReportsPresenter presenter=new DsReportsPresenter(form, new DsReport(),querry);
		form.setToNewState();
		AppMemory.getAppMemory().stickPnel(form);
	}
	
	/*
	 * Method template to set the query object in specific to search criteria and return the same
	 * @return MyQuerry object
	 */
	public MyQuerry getDsReportQuery(List<String> reportList) {
		MyQuerry quer = new MyQuerry();
		Vector<Filter> filters = new Vector<Filter>();
		Filter temp = null;

		temp = new Filter();
		temp.setQuerryString("status");
		temp.setBooleanvalue(true);
		filters.add(temp);

		if (reportList!=null&&reportList.size()!=0) {
			temp = new Filter();
			temp.setQuerryString("reportName IN");
			temp.setList(reportList);
			filters.add(temp);
		}

		quer.setFilters(filters);
		quer.setQuerryObject(new DsReport());
		return quer;
	}
	
	public MyQuerry getReportsAsPerRole(final TableScreen<DsReport> view) {
		MyQuerry quer = new MyQuerry();
		Vector<Filter> filters = new Vector<Filter>();
		Filter temp = null;

		temp = new Filter();
		temp.setQuerryString("status");
		temp.setBooleanvalue(true);
		filters.add(temp);

		if (!LoginPresenter.myUserEntity.getRole().getRoleName().equalsIgnoreCase("ADMIN")) {
			temp = new Filter();
			temp.setQuerryString("role");
			temp.setStringValue(LoginPresenter.myUserEntity.getRole().getRoleName());
			filters.add(temp);
		}

		quer.setFilters(filters);
		quer.setQuerryObject(new DsReportDetails());
		service.getSearchResult(quer, new AsyncCallback<ArrayList<SuperModel>>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				List<String> reportList=new ArrayList<String>();
				if(result!=null&&result.size()!=0){
					for(SuperModel model:result){
						DsReportDetails obj=(DsReportDetails) model;
						reportList.add(obj.getReportName());
					}
				}
				
				if(reportList.size()!=0){
					view.retriveTable(getDsReportQuery(reportList));
				}
			}
		});
		return quer;
	}
	
	  private void reactTo()
 {
	}

}
