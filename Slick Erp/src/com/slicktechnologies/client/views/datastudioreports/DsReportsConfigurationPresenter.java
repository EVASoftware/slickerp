package com.slicktechnologies.client.views.datastudioreports;

import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.userauthorization.UserAuthorizationForm;
import com.slicktechnologies.client.userauthorization.UserAuthorizationPresenter;
import com.slicktechnologies.client.userauthorization.UserAuthorizeTable;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.contractpandlreport.ContractPAndLReportForm;
import com.slicktechnologies.client.views.contractpandlreport.ContractPAndLReportPresenter;
import com.slicktechnologies.client.views.humanresource.employeeasset.EmployeeAssetAllocationForm;
import com.slicktechnologies.client.views.humanresource.employeeasset.EmployeeAssetAllocationPresenter;
import com.slicktechnologies.client.views.humanresource.employeeasset.EmployeeAssetPresenterSearchProxy;
import com.slicktechnologies.client.views.humanresource.employeeasset.EmployeeAssetTableProxy;
import com.slicktechnologies.client.views.smsconfiguration.SmsConfigurationForm;
import com.slicktechnologies.client.views.smsconfiguration.SmsConfigurationPresenter;
import com.slicktechnologies.client.views.smsconfiguration.SmsConfigurationTable;
import com.slicktechnologies.shared.EmployeeAsset;
import com.slicktechnologies.shared.common.DsReport;
import com.slicktechnologies.shared.common.DummyEntityOnlyForScreen;
import com.slicktechnologies.shared.common.helperlayer.SmsConfiguration;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.role.UserRole;

public class DsReportsConfigurationPresenter extends FormTableScreenPresenter<DsReport> {
	
	DsReportsConfigurationForm form;
	
	final GenricServiceAsync async=GWT.create(GenricService.class);
	public DsReportsConfigurationPresenter(FormTableScreen<DsReport> view,DsReport model) {
		super(view, model);
		
		form = (DsReportsConfigurationForm) view;
		form.getSupertable().connectToLocal();
		form.retriveTable(getDsReportConfigQuery());
		form.setPresenter(this);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.REPORTSCONFIGURATIONS,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}

	
	
	
	public MyQuerry getDsReportConfigQuery()
	{
		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new DsReport());
		return quer;
	}
	
	
	
	public static DsReportsConfigurationForm initalize() {
		
		DsReportTable gentableScreen=new DsReportTable();
		
		DsReportsConfigurationForm form = new DsReportsConfigurationForm(gentableScreen, FormTableScreen.UPPER_MODE, true);
		gentableScreen.setView(form);
		gentableScreen.applySelectionModle();
		DsReportsConfigurationPresenter  presenter=new  DsReportsConfigurationPresenter(form,new DsReport());
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Reports/Report Configuration",Screen.REPORTSCONFIGURATIONS);
		AppMemory.getAppMemory().stickPnel(form);
		return form;
		
	}

	
	@Override
	public void reactToProcessBarEvents(ClickEvent e) {

		InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();
		
	}
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		super.onClick(event);
		
		
	}
	@Override
	public void reactOnPrint() {
	}

	@Override
	public void reactOnEmail() {
	}

	@Override
	protected void makeNewModel() {
		model=new DsReport();
		
	}
	
	public void setModel(DsReport entity)
	{
		model=entity;
	}
	void reactOnServiceDeleteProcess(){
		//nbhcContractServiceCycleDelProcess
		
		
		async.nbhcContractServiceCycleDelProcess(model.getCompanyId(), new AsyncCallback<String>() {
			
			@Override
			public void onSuccess(String result) {
				// TODO Auto-generated method stub
				if(result.equals("Success")){
					form.showDialogMessage("Delete Process Started"+model.getCompanyId());
				}
				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
				form.showDialogMessage("Delete Process have some Problem can not delete recordds");
			}
		});
	}


	
	
	
}
