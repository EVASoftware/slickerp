package com.slicktechnologies.client.views.datastudioreports;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.client.Window;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.DsReport;
import com.slicktechnologies.shared.common.businessprocesslayer.GoogleDriveLink;
import com.slicktechnologies.shared.common.helperlayer.SmsConfiguration;
import com.slicktechnologies.shared.common.role.ScreenAuthorization;

public class DsReportTable extends SuperTable<DsReport> {
    
	TextColumn<DsReport> reportNameColumn;
	TextColumn<DsReport> entityNameColumn;
	TextColumn<DsReport> reportLinkColumn;
	TextColumn<DsReport> statusColumn;
	
	TextColumn<DsReport> roleName;
	Column<DsReport, String> viewReports;
	public DsReportTable()
	{
		super();
		table.setWidth("100%");
		table.setHeight("700px");
		
	}
	
	

	
	
	
	public DsReportTable(boolean isReportTable)
	{
		super(isReportTable);
		table.setWidth("100%");
		table.setHeight("500px");
		createTable1();
	}
	
	
	@Override
	public void createTable() {
		addColumnReportName();
		addColumnEntityName();
		addColumnReportLink();
		addColumnStatus();
		table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
		
	}
	private void createTable1() {
		
		addColumnReportName();
		addColumnEntityName();
//		addColumnStatus();
		addColumnView();
		
        setFieldUpdaterOnView();
	table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
}
	

	private void addColumnStatus() {
		
		statusColumn = new TextColumn<DsReport>() {
			
			@Override
			public String getValue(DsReport object) {
				if(object.getStatus()==true)
				return "Active";
				else return "InActive";
			}
		};
		
		table.addColumn(statusColumn,"Status");
		table.setColumnWidth(statusColumn,100,Unit.PX);
		statusColumn.setSortable(true);
	}

	
	private void addColumnRoleName() {
		
		roleName = new TextColumn<DsReport>() {
			@Override
			public String getValue(DsReport object) {
				return " ";
			}
		};
		table.addColumn(roleName, "Role");
		
	}

	private void setFieldUpdaterOnView() {
		viewReports.setFieldUpdater(new FieldUpdater<DsReport, String>() {
			@Override
			public void update(int index, DsReport object, String value) {
				 final String url = object.getReportLink();
		 			Window.open(url, "test", "enabled");
			}

			
		});
	}


	private void addColumnView() {
		ButtonCell btnCell = new ButtonCell();
		viewReports = new Column<DsReport, String>(btnCell) {
			@Override
			public String getValue(DsReport object) {
				return "View ";
			}
		};
		table.addColumn(viewReports, "View ");
		
	}






	private void addColumnEntityName() {
		
		entityNameColumn = new TextColumn<DsReport>() {
			@Override
			public String getValue(DsReport object) {
				return object.getEntityName()+" ";
			}
		};
		table.addColumn(entityNameColumn, "Entity Name");
		entityNameColumn.setSortable(true);
	}

	

	

	private void addColumnReportLink() {
		
		reportLinkColumn = new TextColumn<DsReport>() {
			@Override
			public String getValue(DsReport object) {
				return object.getReportLink()+" ";
			}
		};
		table.addColumn(reportLinkColumn, "Report Link");
		reportLinkColumn.setSortable(true);
	}

	private void addColumnReportName() {
		
		reportNameColumn = new TextColumn<DsReport>() {
			@Override
			public String getValue(DsReport object) {
				return object.getReportName()+" ";
			}
		};
		table.addColumn(reportNameColumn, "Report Name");
		reportNameColumn.setSortable(true);
	}

	@Override
	protected void initializekeyprovider() {
		
	}

	@Override
	public void addFieldUpdater() {
		
	}

	@Override
	public void setEnable(boolean state) {
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void addColumnSorting() {
		super.addColumnSorting();
		
		getSortingReportNameColumn();
		getSortingEntityNameColumn();
		getSortingReportLinkColumn();
	}






	






	private void getSortingReportLinkColumn() {
		List<DsReport> list=getDataprovider().getList();
		columnSort=new ListHandler<DsReport>(list);
		columnSort.setComparator(reportLinkColumn, new Comparator<DsReport>()
				{
			@Override
			public int compare(DsReport e1,DsReport e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getReportLink()!=null && e2.getReportLink()!=null){
						return e1.getReportLink().compareTo(e2.getReportLink());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
		
	}






	private void getSortingEntityNameColumn() {
		List<DsReport> list=getDataprovider().getList();
		columnSort=new ListHandler<DsReport>(list);
		columnSort.setComparator(entityNameColumn, new Comparator<DsReport>()
				{
			@Override
			public int compare(DsReport e1,DsReport e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getEntityName()!=null && e2.getEntityName()!=null){
						return e1.getEntityName().compareTo(e2.getEntityName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
		
	}






	private void getSortingReportNameColumn() {
		List<DsReport> list=getDataprovider().getList();
		columnSort=new ListHandler<DsReport>(list);
		columnSort.setComparator(reportNameColumn, new Comparator<DsReport>()
				{
			@Override
			public int compare(DsReport e1,DsReport e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getReportName()!=null && e2.getReportName()!=null){
						return e1.getReportName().compareTo(e2.getReportName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
		
	}
}
