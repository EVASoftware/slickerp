package com.slicktechnologies.client.views.service;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.apache.poi.ss.formula.ptg.TblPtg;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.CompositeInterface;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.device.ServiceMarkCompletePopUp;
import com.slicktechnologies.client.views.project.concproject.EmpolyeeTable;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.client.views.technicianschedule.MaterialProductTable;
import com.slicktechnologies.client.views.technicianschedule.ServiceCompletetionPopup;
import com.slicktechnologies.client.views.technicianschedule.TechnicianTable;
import com.slicktechnologies.client.views.technicianschedule.TechnicianToolTable;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.inventory.StorageLocation;
import com.slicktechnologies.shared.common.inventory.Storagebin;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.scheduling.TeamManagement;
import com.slicktechnologies.shared.common.servicerelated.CompanyAsset;
import com.smartgwt.client.widgets.IButton;
public class ServiceSchedulePopup extends PopupScreen implements CompositeInterface , ChangeHandler{

	GenricServiceAsync genasync=GWT.create(GenricService.class);
	long companyId = UserConfiguration.getCompanyId();
	private DateBox dateBox;
	ObjectListBox<TeamManagement> olbTeam;
	private ListBox p_servicehours;
	private ListBox p_servicemin;
	private ListBox p_ampm;
	
	/**
	 *  nidhi
	 * 23-10-2017
	 *  for set ser Engineer in service 
	 */
	  ObjectListBox<Employee> olbEmployee;
	  Button btnEmployee;
	  EmpolyeeTable emptable;
	  
	/**
	 *  end
	 */
	/** date 27.02.2018 added by komal to change popup same as technician scheduling list plan popup **/
		Button btnToolAdd;
		Button btnproductAdd;
		TechnicianToolTable toolTable;
		
		CustomerServiceMaterialTable materialProductTable;
		
		ObjectListBox<ItemProduct> olbitemProduct;
	    ObjectListBox<CompanyAsset> olbTool;
		ObjectListBox<WareHouse> olbWarehouseName;
		ObjectListBox<StorageLocation> olbStorageLocation;
		ObjectListBox<Storagebin> olbstorageBin;
		DoubleBox dbQuantity;
		ObjectListBox<Invoice> olbInvoiceId;
		ObjectListBox<Invoice> olbInvoiceDate;
		InlineLabel lblUpdate = new InlineLabel("Complete service");
		ServiceMarkCompletePopUp markcompletepopup=new ServiceMarkCompletePopUp();
		PopupPanel panel = new PopupPanel();
		Service service = null;
		VerticalPanel vpanel1;
		VerticalPanel vpanel2;
		VerticalPanel vpanel3;
		VerticalPanel vpanel4;
		VerticalPanel vpanel5;

		HorizontalPanel hpanel1 = new HorizontalPanel();
		/**
		 * end komal
		 */
		/**
		 * nidhi
		 * 21-05-2018
		 * for exclude day for service
		 * @param documentId
		 * @param documentDate
		 */
		public static ListBox e_mondaytofriday;
		public static String materialValue  ="";
		Map<String , String> locationMap= new HashMap<String , String>();
		Map<String , String> binMap = new HashMap<String ,String>();
		
	public ServiceSchedulePopup() {
		
		super();
		materialValue = "";
		createGui();
		btnToolAdd.addClickHandler(this);
		btnproductAdd.addClickHandler(this);
		lblUpdate.addClickHandler(this);	
	
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "HideCompleteServiceButton") && 
				!LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN") &&
				LoginPresenter.branchRestrictionFlag){
			lblUpdate.setVisible(false);
		}
		
	}
public ServiceSchedulePopup(String str) {
		
		super();
		materialValue = str;
		createGui();
		btnToolAdd.addClickHandler(this);
		btnproductAdd.addClickHandler(this);
		lblUpdate.addClickHandler(this);	
		lblUpdate.setVisible(false);
	
		
	}

	void initilizewidget(){
		InlineLabel nlnlblNewServiceDate = new InlineLabel("Service Date        :");
//		add(nlnlblNewServiceDate, 10, 28);
		
//		dateBox = new DateBox();
		dateBox = new DateBoxWithYearSelector();

		dateBox.setEnabled(false);
		DateTimeFormat format = DateTimeFormat.getFormat("yyyy-MM-dd");
		DateBox.DefaultFormat defformat = new DateBox.DefaultFormat(format);
		dateBox.setFormat(defformat);
//		add(dateBox, 184, 28);
		
		
		InlineLabel nlnlblNewServicetme = new InlineLabel("Service Time          :");
//		add(nlnlblNewServicetme, 10, 95);
		
		
		InlineLabel hhlable=new InlineLabel(" HH ");
//		add(hhlable,189,70);
		
		p_servicehours=new ListBox();
//		add(p_servicehours,184,95);
		p_servicehours.setSize("60px", "24px");
//		p_servicehours.insertItem("--", 0);
//		p_servicehours.insertItem("00", 1);
//		p_servicehours.insertItem("01", 2);
//		p_servicehours.insertItem("02", 3);
//		p_servicehours.insertItem("03", 4);
//		p_servicehours.insertItem("04", 5);
//		p_servicehours.insertItem("05", 6);
//		p_servicehours.insertItem("06", 7);
//		p_servicehours.insertItem("07", 8);
//		p_servicehours.insertItem("08", 9);
//		p_servicehours.insertItem("09", 10);
//		p_servicehours.insertItem("10", 11);
//		p_servicehours.insertItem("11", 12);
		
		/**
		 * Updated By ANIL Date:24-01-2017
		 * 
		 */
		
		p_servicehours.insertItem("--", 0);
		p_servicehours.insertItem("01", 1);
		p_servicehours.insertItem("02", 2);
		p_servicehours.insertItem("03", 3);
		p_servicehours.insertItem("04", 4);
		p_servicehours.insertItem("05", 5);
		p_servicehours.insertItem("06", 6);
		p_servicehours.insertItem("07", 7);
		p_servicehours.insertItem("08", 8);
		p_servicehours.insertItem("09", 9);
		p_servicehours.insertItem("10", 10);
		p_servicehours.insertItem("11", 11);
		p_servicehours.insertItem("12", 12);
		
		/**
		 * End
		 */
		
		
		
		InlineLabel mmlable=new InlineLabel(" MM ");
//		add(mmlable,264,70);
		
		p_servicemin=new ListBox();
//		add(p_servicemin,259,95);
		p_servicemin.setSize("60px", "24px");
		p_servicemin.insertItem("--", 0);
		p_servicemin.insertItem("00", 1);
		p_servicemin.insertItem("15", 2);
		p_servicemin.insertItem("30", 3);
		p_servicemin.insertItem("45", 4);
		
		InlineLabel sslable=new InlineLabel(" AM/PM ");
//		add(sslable,334,70);
		
		
		p_ampm = new ListBox();
//		add(p_ampm,329,95);
		p_ampm.setSize("60px", "24px");
		p_ampm.insertItem("--", 0);
		p_ampm.insertItem("AM", 1);
		p_ampm.insertItem("PM", 2);
		
		
		/*
		 * nidhi
		 * 2-10-2017
		 * 		 */
		   olbEmployee = new ObjectListBox<Employee>();
		  
//	        AppUtility.makeSalesPersonListBoxLive(olbEmployee);
	        
	        btnEmployee = new Button("Add Employee");
	      
	        
	        emptable = new EmpolyeeTable();
	        emptable.connectToLocal();
		/**
		 * end
		 */
		olbTeam = new ObjectListBox<TeamManagement>();
		this.initalizeTeamListBox();
		
		
		InlineLabel specificReasonlbl = new InlineLabel("Team               :");
//		add(specificReasonlbl, 10, 130);
		specificReasonlbl.setSize("200px", "18px");
		
//		add(olbTeam, 184, 133);
		olbTeam.getElement().getStyle().setHeight(23, Unit.PX);
		
		
		
//		
//		btnOk = new Button("Ok");
//		add(btnOk, 184, 200);
//		
//		
//		btnCancel = new Button("Cancel");
//		add(btnCancel,300,200);
//		
//		setSize("480px","250px");
//		this.getElement().setId("form");
		
		/*
		 * nidhi
		 * 12-10-2017
		 * 		 */
		InlineLabel serviceEnglbl = new InlineLabel("Technician Engineer          :");
//		add(serviceEnglbl, 10, 170);
//		add(olbEmployee, 184,170);
//		add(btnEmployee,400,167);
//		btnOk = new Button("Ok");
		
		
//		add(emptable.getTable(), 10, 215);
		emptable.getTable().setHeight("150px");
//		btnCancel = new Button("Cancel");
//		add(btnOk, 184, 330);
//		add(btnCancel,300,330);
		/**
		 *  end
		 */
//		setSize("60%","60%");
//		this.getElement().setId("form");
//		this.getElement().addClassName("popFormContent");
		/** date 27.02.2018 added by komal to change popup same as technician scheduling list plan popup **/
		btnToolAdd = new Button("ADD");
		btnproductAdd = new Button("ADD");
		toolTable = new TechnicianToolTable();		
		materialProductTable = new CustomerServiceMaterialTable();
		materialProductTable.connectToLocal();
		materialProductTable.getTable().setHeight("180px");
		olbitemProduct = new ObjectListBox<ItemProduct>();
		olbitemProduct.addItem("--SELECT--");
		System.out.println("before item product loading");
		initializeItemProdcut(companyId,olbitemProduct);
		olbitemProduct.addChangeHandler(this);
		

	    olbTool = new ObjectListBox<CompanyAsset>();
	    olbTool.addItem("--SELECT--");
	    AppUtility.makeCompanyAssetListBoxLive(olbTool);

		olbWarehouseName = new ObjectListBox<WareHouse>();
		olbWarehouseName.addItem("--SELECT--");
		//initializeItemProdcut(companyId,olbTool);
		olbWarehouseName.addChangeHandler(this);

		olbStorageLocation = new ObjectListBox<StorageLocation>();
		olbStorageLocation.addItem("--SELECT--");
		//initializeItemProdcut(companyId,olbTool);
		olbStorageLocation.addChangeHandler(this);

		olbstorageBin = new ObjectListBox<Storagebin>();
		olbstorageBin.addItem("--SELECT--");
		
		dbQuantity = new DoubleBox();
		olbInvoiceId = new ObjectListBox<Invoice>();
		olbInvoiceId.addItem("--SELECT--");
		olbInvoiceId.addChangeHandler(this);
		olbInvoiceDate = new ObjectListBox<Invoice>();
		olbInvoiceDate.addItem("--SELECT--");
		olbInvoiceDate.addChangeHandler(this);
		/**
		* end komal
		*/

		//komal
		vpanel1 = new VerticalPanel();
		vpanel1.getElement().addClassName("serVertical");
		vpanel2 = new VerticalPanel();
		vpanel2.getElement().addClassName("serVertical");
		vpanel3 = new VerticalPanel();
		vpanel3.getElement().addClassName("serVertical");
		vpanel4 = new VerticalPanel();
		vpanel4.getElement().addClassName("serVertical");
		vpanel5 = new VerticalPanel();
		vpanel5.getElement().addClassName("serVertical");
		hpanel1 = new HorizontalPanel();
		hpanel1.getElement().addClassName("serHor");
				Label lbl1 = new Label("HH :");
				lbl1.getElement().addClassName("lblSer");
				vpanel1.add(lbl1);
				vpanel1.add(p_servicehours);
				
				Label lbl2 = new Label("MM :");
				lbl2.getElement().addClassName("lblSer");
				
				vpanel2.add(lbl2);
				vpanel2.add(p_servicemin);
				
				Label lbl3 = new Label("AM/PM : ");
				lbl3.getElement().addClassName("lblSer");
				vpanel3.add(lbl3);
				vpanel3.add(p_ampm);
				
				Label lbl4 = new Label("Invoice Id");
				lbl4.getElement().addClassName("lblSer");
				
				vpanel4.add(lbl4);
				vpanel4.add(olbInvoiceId);
				
				Label lbl5 = new Label("Invoice Date");
				lbl5.getElement().addClassName("lblSer");
			
				vpanel5.add(lbl5);
				vpanel5.add(olbInvoiceDate);
				
				hpanel1.add(vpanel1);
				hpanel1.add(vpanel2);
				hpanel1.add(vpanel3);
				hpanel1.add(vpanel4);
				hpanel1.add(vpanel5);
				/**
				 * end komal
				 */
	}

	@Override
	public void createScreen() {
		
		// TODO Auto-generated method stub
		initilizewidget();		
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingServiceScheduleDetails=fbuilder.setlabel("Service Schedule Details").widgetType(FieldType.GREYGROUPING).setMandatory(false).setColSpan(6).build();
		
		fbuilder = new FormFieldBuilder("Service Date :",dateBox);
		FormField fdbdateBox= fbuilder.setRowSpan(0).setColSpan(0).build();
	
		
		fbuilder = new FormFieldBuilder("HH : ",p_servicehours);
		FormField fdbserviceHours= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("MM : ",p_servicemin);
		FormField fdbeserviceMin= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("AM/PM : ",p_ampm);
		FormField fdbeserviceAmPm= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingTechnicianDetails=fbuilder.setlabel("Technician Details").widgetType(FieldType.GREYGROUPING).setMandatory(false).setColSpan(6).build();
		
		fbuilder = new FormFieldBuilder("Team :",olbTeam);
		FormField fobjectList= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Technician :",olbEmployee);
		FormField fTechobjectList= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",btnEmployee);
		FormField fTechobjectBtn= fbuilder.setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder(" ",emptable.getTable());
		FormField fdbTechList= fbuilder.setRowSpan(0).setColSpan(6).build();
		fbuilder = new FormFieldBuilder();
		FormField fblankgroupthree=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/** date 27.02.2018 added by komal to change popup same as technician scheduling list plan popup **/
		fbuilder = new FormFieldBuilder("" , btnToolAdd);
		FormField fbtnToolAdd= fbuilder.setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("" , btnproductAdd);
		FormField fbtnproductAdd= fbuilder.setRowSpan(0).setColSpan(0).build();	
		
		fbuilder = new FormFieldBuilder("" , toolTable.getTable());
		FormField ftoolTable= fbuilder.setRowSpan(0).setColSpan(6).build();
		
		fbuilder = new FormFieldBuilder("" , materialProductTable.getTable());
		FormField fmaterialProductTable= fbuilder.setRowSpan(0).setColSpan(6).build();
				
		fbuilder = new FormFieldBuilder("Material Name" , olbitemProduct);
		FormField folbitemProduct= fbuilder.setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Tool" , olbTool);
		FormField folbTool= fbuilder.setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("WareHouse" , olbWarehouseName);
		FormField folbWarehouseName= fbuilder.setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Storage Location" , olbStorageLocation);
		FormField folbStorageLocation= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Storage Bin" , olbstorageBin);
		FormField folbstorageBin = fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Quantity" , dbQuantity);
		FormField fdbQuantity= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Invoice Id" , olbInvoiceId);
		FormField folbInvoiceId= fbuilder.setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Invoice Date" , olbInvoiceDate);
		FormField folbInvoiceDate= fbuilder.setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder();
		FormField fToolInformation =fbuilder.setlabel("Tool Information").widgetType(FieldType.GREYGROUPING).setMandatory(false).setColSpan(6).build();

		fbuilder = new FormFieldBuilder();
		FormField fMaterialInformation =fbuilder.setlabel("Material Information").widgetType(FieldType.GREYGROUPING).setMandatory(false).setColSpan(6).build();

		fbuilder = new FormFieldBuilder("" , hpanel1);
		FormField fhpanel1 =fbuilder.setRowSpan(0).setColSpan(5).build();

		/**
		* end komal
		*/
//fhpanel
		if(materialValue != null && materialValue.equalsIgnoreCase("Material")){
			FormField[][] formfield = {
					{fMaterialInformation },
					{folbitemProduct , fdbQuantity , folbWarehouseName , folbStorageLocation , folbstorageBin , fbtnproductAdd} ,
					{fmaterialProductTable},
					{fToolInformation},
					{folbTool , fbtnToolAdd},
					{ftoolTable}
			};
			this.fields=formfield;	
		}else{
		FormField[][] formfield = {
				{fgroupingServiceScheduleDetails},
				{fdbdateBox, fdbserviceHours , fdbeserviceMin , fdbeserviceAmPm ,folbInvoiceId ,folbInvoiceDate},
				{fgroupingTechnicianDetails},
				{fobjectList , fTechobjectList , fTechobjectBtn},
				{fdbTechList },
				{fMaterialInformation },
				{folbitemProduct , fdbQuantity , folbWarehouseName , folbStorageLocation , folbstorageBin , fbtnproductAdd} ,
				{fmaterialProductTable},
				{fToolInformation},
				{folbTool , fbtnToolAdd},
				{ftoolTable},
							
		};


		this.fields=formfield;		
		}
	}
	
	private void initalizeTeamListBox() {
		MyQuerry querry = new MyQuerry();
		querry.setQuerryObject(new TeamManagement());
		olbTeam.MakeLive(querry);
		
		/**
		 *  nidhi
		 *  12-10-2017
		 *  
		 */
		olbEmployee.makeEmployeeLive(AppConstants.SERVICEMODULE, AppConstants.CUSTOMERPROJECT, "Technician");
		/**
		 *  end
		 */
	}
	@Override
	public void setEnable(boolean status) {
		super.setEnable(status);
		dateBox.setEnabled(false);
	}

	@Override
	public boolean validate() {
		return false;
	}

	public void sethrList(int i){
		this.p_servicehours.setSelectedIndex(i);
	}
	
	public void clear() {
		this.p_servicehours.setSelectedIndex(0);
		this.p_servicemin.setSelectedIndex(0);
		this.p_ampm.setSelectedIndex(0);
		this.olbTeam.setSelectedIndex(0);
		/**
		 *  nidhi
		 *  12-10-2017
		 *  
		 */
		this.emptable.getDataprovider().getList().clear();
		this.olbEmployee.setSelectedIndex(0);
	}
	
	

	public DateBox getDateBox() {
		return dateBox;
	}


	public void setDateBox(DateBox dateBox) {
		this.dateBox = dateBox;
	}


//	public Button getBtnOk() {
//		return btnOk;
//	}
//
//
//	public void setBtnOk(Button btnOk) {
//		this.btnOk = btnOk;
//	}
//
//
//	public Button getBtnCancel() {
//		return btnCancel;
//	}
//
//
//	public void setBtnCancel(Button btnCancel) {
//		this.btnCancel = btnCancel;
//	}


	public ObjectListBox<TeamManagement> getOlbTeam() {
		return olbTeam;
	}


	public void setOlbTeam(ObjectListBox<TeamManagement> olbTeam) {
		this.olbTeam = olbTeam;
	}


	public ListBox getP_servicehours() {
		return p_servicehours;
	}


	public void setP_servicehours(ListBox p_servicehours) {
		this.p_servicehours = p_servicehours;
	}


	public ListBox getP_servicemin() {
		return p_servicemin;
	}


	public void setP_servicemin(ListBox p_servicemin) {
		this.p_servicemin = p_servicemin;
	}


	public ListBox getP_ampm() {
		return p_ampm;
	}


	public void setP_ampm(ListBox p_ampm) {
		this.p_ampm = p_ampm;
	}


	public ObjectListBox<Employee> getOlbEmployee() {
		return olbEmployee;
	}


	public void setOlbEmployee(ObjectListBox<Employee> olbEmployee) {
		this.olbEmployee = olbEmployee;
	}


	public Button getBtnEmployee() {
		return btnEmployee;
	}


	public void setBtnEmployee(Button btnEmployee) {
		this.btnEmployee = btnEmployee;
	}


	public EmpolyeeTable getEmptable() {
		return emptable;
	}


	public void setEmptable(EmpolyeeTable emptable) {
		this.emptable = emptable;
	}


	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		if(event.getSource()==btnToolAdd){
			System.out.println("btnToolAdd ==");
			if(olbTool.getSelectedIndex()!=0){
				System.out.println("btnToolAdd == 222222222");

				CompanyAsset companyAsset = olbTool.getSelectedItem();
//				CompanyAsset techniciantoolinfo = new CompanyAsset();
//				techniciantoolinfo.setName(olbTool.getValue(olbTool.getSelectedIndex()));
//				techniciantoolinfo.setCount(companyAsset.getCount());
				toolTable.getDataprovider().getList().add(companyAsset);
				
			}else{
				System.out.println("btnToolAdd else");
				showDialogMessage("Please Select Tool");
			}
			
		}
		
		if(event.getSource()==btnproductAdd){
			
			System.out.println("btnproductAdd ==");

			if(ValidateWarehouse()){
				System.out.println("btnproductAdd == 222222222222");
				
				ItemProduct product = olbitemProduct.getSelectedItem();
//				TechnicianMaterialInfo technicianMaterialIfno = new TechnicianMaterialInfo();
				ProductGroupList technicianMaterialIfno  = new ProductGroupList();
				technicianMaterialIfno.setName(olbitemProduct.getValue(olbitemProduct.getSelectedIndex()));
				if(dbQuantity.getValue()!=null && dbQuantity.getValue() != 0 )
				technicianMaterialIfno.setQuantity(dbQuantity.getValue());
				if(olbWarehouseName.getSelectedIndex()!=0)
					technicianMaterialIfno.setWarehouse(olbWarehouseName.getValue(olbWarehouseName.getSelectedIndex()));
				if(olbStorageLocation.getSelectedIndex()!=0)
					technicianMaterialIfno.setStorageLocation(olbStorageLocation.getValue(olbStorageLocation.getSelectedIndex()));	
				if(olbstorageBin.getSelectedIndex()!=0)
					technicianMaterialIfno.setStorageBin(olbstorageBin.getValue(olbstorageBin.getSelectedIndex()));	

				technicianMaterialIfno.setProduct_id(product.getCount());
				technicianMaterialIfno.setCode(product.getProductCode());
				technicianMaterialIfno.setUnit(product.getUnitOfMeasurement());
				
				materialProductTable.getDataprovider().getList().add(technicianMaterialIfno);
			}
		}
		if(event.getSource().equals(lblUpdate)){
			GWTCAlert alert =  new GWTCAlert();
			//alert.alert("hello button");
			/**
			 * date 12.04.2018 added by komal for orion
			 */
			boolean configFlag = AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "RestrictServiceCompletionForServicewiseBilling");
			if(service.isServiceWiseBilling() && configFlag){    
				panel.hide();
				alert.alert("As it is service wise bill-service, you can not complete it from here.");
				
            }else{
			if(validateCompletion()){
				Date todayDate=new Date();
				if(todayDate.before(getDateBox().getValue())){
					System.out.println("Service Date Exceeds");
					alert.alert("Service Date exceeds current date. Please reschedule the date");
					
				}else{
					System.out.println("Pop Up Showing");
					panel=new PopupPanel(true);
					panel.add(markcompletepopup);
					panel.show();
					panel.center();
					
					addDefaultValueInMarkCompletePopUp(markcompletepopup);
				}
			}
           }
		
		}	
		
	}

	private void initializeItemProdcut(Long companyId, final ObjectListBox<ItemProduct> olbitemProduct) {
		// TODO Auto-generated method stub
		System.out.println("Hi Company Id =="+companyId);
		
		GenricServiceAsync genasync=GWT.create(GenricService.class);

		Vector<Filter> filtervec=new Vector<Filter>();
		Company c  = new Company();

		Filter filter =null;
		GWTCAlert alert = new GWTCAlert();
		filter= new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		MyQuerry querry = new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new ItemProduct());

		/**Date 22-3-2019 Added by Amol to do not show the InActive Item Product.
		 * 
		 */
		 Filter productstatus=new Filter();
		 productstatus.setQuerryString("status");
		 productstatus.setBooleanvalue(true);
		 querry.getFilters().add(productstatus);
		
		genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				
				List<ItemProduct> list = new ArrayList<ItemProduct>();
				for(SuperModel model : result){
					ItemProduct item = (ItemProduct) model;
					
//					olbitemProduct.addItem(item.getProductName());
					list.add(item);
				}
				
				/**
				 * @author Vijay Updated code with sorting order
				 */
				Comparator<ItemProduct> supermodelcomparator = new Comparator<ItemProduct>() {
					public int compare(ItemProduct c1, ItemProduct c2) {
					
						return c1.getProductName().toLowerCase().compareTo(c2.getProductName().toLowerCase());
					}
				};
				Collections.sort(list,supermodelcomparator);
				
				for(ItemProduct itemprod : list){
					olbitemProduct.addItem(itemprod.getProductName());
				}
				olbitemProduct.setItems(list);
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
		
	}

	@Override
	public void onChange(ChangeEvent event) {
		// TODO Auto-generated method stub
		if(event.getSource() == olbitemProduct){
			
			if(olbitemProduct.getSelectedIndex()==0){
				olbWarehouseName.setSelectedIndex(0);
				olbStorageLocation.setSelectedIndex(0);
				olbstorageBin.setSelectedIndex(0);
			}else{
				ItemProduct product = olbitemProduct.getSelectedItem();
				initializeProductWarehouse(product.getCount());
			}
		}
		if(event.getSource()==olbWarehouseName){
			if(olbWarehouseName.getSelectedIndex()==0){
				olbStorageLocation.setSelectedIndex(0);
				olbstorageBin.setSelectedIndex(0);
			}else{
				initializeStoragelocation(olbWarehouseName.getValue(olbWarehouseName.getSelectedIndex()));
			}
		}
		if(event.getSource()==olbStorageLocation){
			initializeBin(olbStorageLocation.getValue(olbStorageLocation.getSelectedIndex()));
		}
		
		if(event.getSource() == olbInvoiceId){
			setinvoiceDateDropDown();
		}
		if(event.getSource() == olbInvoiceDate){
			setInvoiceIdDroDown();
		}
	
		
	}
	private void setInvoiceIdDroDown() {

		if(olbInvoiceDate.getSelectedIndex()!=0){
//			olbInvoiceId.removeAllItems();
//			olbInvoiceId.addItem("--SELECT--");
			Invoice invoiceEntity = olbInvoiceDate.getSelectedItem();
			olbInvoiceId.setValue(invoiceEntity.getCount()+"");
			
		}else{
			olbInvoiceId.setSelectedIndex(0);
		}
	}

	private void setinvoiceDateDropDown() {
		if(olbInvoiceId.getSelectedIndex()!=0){
//			olbInvoiceDate.removeAllItems();
//			olbInvoiceDate.addItem("--SELECT--");
			Invoice invoicentity = olbInvoiceId.getSelectedItem();
			olbInvoiceDate.setValue(DateTimeFormat.getShortDateFormat().format(invoicentity.getInvoiceDate()));
		}else{
			olbInvoiceDate.setSelectedIndex(0);
		}
	}

	private void initializeProductWarehouse(int prodId) {

		olbWarehouseName.removeAllItems();
		
		MyQuerry query = new MyQuerry();
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		

		filter = new Filter();
		filter.setQuerryString("productinfo.prodID");
		filter.setIntValue(prodId);
		filtervec.add(filter);
		
		query.setFilters(filtervec);
		query.setQuerryObject(new ProductInventoryView());
		
		olbWarehouseName.addItem("--SELECT--");
		
		genasync.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				System.out.println("Warehouse size as per product"+result.size());
				locationMap = new HashMap<String , String>();
				binMap = new HashMap<String , String>();
				HashSet<String> hsWarehouseName=new HashSet<String>();
				for(SuperModel model : result){
					ProductInventoryView product = (ProductInventoryView) model;
					for(int i=0;i<product.getDetails().size();i++){
						/**
						 * @author Anil @since 30-09-2021
						 * As we were directly adding warehouse name from stock master so multiple warehouse of same name were getting added
						 * Issue raised by Ashwini Bhagwat for Pecopp
						 */
//						olbWarehouseName.addItem(product.getDetails().get(i).getWarehousename().trim());
						hsWarehouseName.add(product.getDetails().get(i).getWarehousename().trim());
						
						locationMap.put(product.getDetails().get(i).getStoragelocation().trim() , product.getDetails().get(i).getStoragelocation().trim());
						binMap.put(product.getDetails().get(i).getStoragebin().trim() ,product.getDetails().get(i).getStoragebin().trim());
						System.out.println("hiiiiiiiiiiiiiii");
					}
				}
				/**
				 * @author Anil @since 26-10-2021
				 * If branch restriction is active then load only those branches which are applicable to that user
				 * Raised for pecopp by Nitin sir general requirment
				 */
				if(AppUtility.isBranchRestricted()&&!LoginPresenter.myUserEntity.getRole().getRoleName().trim().equalsIgnoreCase("ADMIN")){
					List<String> whList=new ArrayList<String>(hsWarehouseName);
					if(whList!=null&&whList.size()!=0){
						Console.log("Loading WH as Per Branch Authorization..!! "+whList);
						getWarehouseName(whList,olbWarehouseName);
					}
				}else{
					for(String warehouse:hsWarehouseName){
						olbWarehouseName.addItem(warehouse);
					}
				}
			}

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
		
	}
	
	private void getWarehouseName(final List<String> whList,final ObjectListBox<WareHouse> olbWarehouseName) {
		MyQuerry query = new MyQuerry();
		System.out.println("Company Id :: "+UserConfiguration.getCompanyId());
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);
		
		if(AppUtility.isBranchRestricted()){
			if (!LoginPresenter.myUserEntity.getRole().getRoleName().trim().equalsIgnoreCase("ADMIN")) {
				List<String> list=new ArrayList<String>();
				for(Branch branch:LoginPresenter.globalBranch){
					list.add(branch.getBusinessUnitName());
				}
				
				 filter=new Filter();
                 filter.setQuerryString("branchdetails.branchName IN");
                 filter.setList(list);
                 filtervec.add(filter);
			}
		}
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(UserConfiguration.getCompanyId());
		filtervec.add(filter);
		
		query.setFilters(filtervec);
		query.setQuerryObject(new WareHouse());
		
		genasync.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				ArrayList<String> warehouseList=new ArrayList<String>();
				if(result.size()!=0){
					for(SuperModel model:result){
						WareHouse entity=(WareHouse) model;
						warehouseList.add(entity.getBusinessUnitName());
					}
					Collections.sort(warehouseList);
				}
				Console.log("Authorised WH Size "+warehouseList);
				for(String wh:whList){
					if(warehouseList.contains(wh)){
						olbWarehouseName.addItem(wh);
					}
				}

				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				
			}
		});
	
	}

	private void initializeBin(String storageLocation) {
		
		olbstorageBin.removeAllItems();

		MyQuerry query = new MyQuerry();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(companyId);
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("storagelocation");
		filter.setStringValue(storageLocation);
		filtervec.add(filter);
		
		/**
		 *  Added By Priyanka - 28-08-2021
		 *  Des : Pecopp - if storage bin status is inactive then is should not in drop down selection raised by ashwini
		 */
		
		filter=new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);
	
		query.setFilters(filtervec);
		query.setQuerryObject(new Storagebin());
		
		olbstorageBin.addItem("--SELECT--");
		genasync.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				System.out.println("result of storage bin =="+result.size());
				for(SuperModel model :result){
					Storagebin storagebin = (Storagebin) model;
					if(binMap.containsKey(storagebin.getBinName())){
						olbstorageBin.addItem(storagebin.getBinName());
					}
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
	}

	private void initializeStoragelocation(String warehouseName) {

		olbStorageLocation.removeAllItems();
		MyQuerry query = new MyQuerry();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(companyId);
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("warehouseName");
		filter.setStringValue(warehouseName);
		filtervec.add(filter);
	
		query.setFilters(filtervec);
		query.setQuerryObject(new StorageLocation());
		
		olbStorageLocation.addItem("--SELECT--");
		
		genasync.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				
				
				for(SuperModel model :result){
					StorageLocation storageLocation = (StorageLocation) model;
					if(locationMap.containsKey(storageLocation.getBusinessUnitName())){
						olbStorageLocation.addItem(storageLocation.getBusinessUnitName());
					}
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
		
	}
	private boolean ValidateWarehouse() {

		if(olbitemProduct.getSelectedIndex()==0){
			System.out.println("olbitemProduct");
			showDialogMessage("Please select Material information");
			return false;
		}
		
		if(olbitemProduct.getSelectedIndex()!=0 && dbQuantity.getValue()==null ){
			showDialogMessage("Please add quantity");
			return false;
		}
		if(dbQuantity.getValue().equals("0")){
			showDialogMessage("Please add valid quantity");
			return false;
		}
		if(olbWarehouseName.getSelectedIndex()==0){
			
			showDialogMessage("Please Select Warehouse details!");
			return false;
		}
		if(olbWarehouseName.getSelectedIndex()!=0){
			if(olbStorageLocation.getSelectedIndex()==0){
				showDialogMessage("Please select storage location");
				return false;
			}
			if(olbstorageBin.getSelectedIndex()==0){
				showDialogMessage("Please select storage Bin");
				return false;
			}
			
//			if(olbitemProduct.getSelectedIndex()!=0 && dbQuantity.getValue()==null ){
//				showDialogMessage("Please add quantity");
//				return false;
//			}
			
//			for(int i=0;i<list.size();i++){
//				if(list.get(i).getWarehouse()==null){
//					showDialogMessage("Please add material either with warehouse or without warehouse");
//					return false;
//				}
//			}
		}
		
		
			
		return true;
	}

	public TechnicianToolTable getToolTable() {
		return toolTable;
	}

	public void setToolTable(TechnicianToolTable toolTable) {
		this.toolTable = toolTable;
	}

	public CustomerServiceMaterialTable getMaterialProductTable() {
		return materialProductTable;
	}

	public void setMaterialProductTable(CustomerServiceMaterialTable materialProductTable) {
		this.materialProductTable = materialProductTable;
	}

	public ObjectListBox<ItemProduct> getOlbitemProduct() {
		return olbitemProduct;
	}

	public void setOlbitemProduct(ObjectListBox<ItemProduct> olbitemProduct) {
		this.olbitemProduct = olbitemProduct;
	}

	public ObjectListBox<CompanyAsset> getOlbTool() {
		return olbTool;
	}

	public void setOlbTool(ObjectListBox<CompanyAsset> olbTool) {
		this.olbTool = olbTool;
	}

	public ObjectListBox<WareHouse> getOlbWarehouseName() {
		return olbWarehouseName;
	}

	public void setOlbWarehouseName(ObjectListBox<WareHouse> olbWarehouseName) {
		this.olbWarehouseName = olbWarehouseName;
	}

	public ObjectListBox<StorageLocation> getOlbStorageLocation() {
		return olbStorageLocation;
	}

	public void setOlbStorageLocation(
			ObjectListBox<StorageLocation> olbStorageLocation) {
		this.olbStorageLocation = olbStorageLocation;
	}

	public ObjectListBox<Storagebin> getOlbstorageBin() {
		return olbstorageBin;
	}

	public void setOlbstorageBin(ObjectListBox<Storagebin> olbstorageBin) {
		this.olbstorageBin = olbstorageBin;
	}

	public DoubleBox getDbQuantity() {
		return dbQuantity;
	}

	public void setDbQuantity(DoubleBox dbQuantity) {
		this.dbQuantity = dbQuantity;
	}

	public ObjectListBox<Invoice> getOlbInvoiceId() {
		return olbInvoiceId;
	}

	public void setOlbInvoiceId(ObjectListBox<Invoice> olbInvoiceId) {
		this.olbInvoiceId = olbInvoiceId;
	}

	public ObjectListBox<Invoice> getOlbInvoiceDate() {
		return olbInvoiceDate;
	}

	public void setOlbInvoiceDate(ObjectListBox<Invoice> olbInvoiceDate) {
		this.olbInvoiceDate = olbInvoiceDate;
	}
	private boolean validateCompletion() {
		GWTCAlert alert = new GWTCAlert();
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "OnlyForNBHC")){
			System.out.println("Config true");
			if(!UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin")){
				System.out.println("Not an admin");
				DateTimeFormat format= DateTimeFormat.getFormat("dd/MM/yyyy");
				Date serviceDate=getDateBox().getValue();
				Date lastValidDate=CalendarUtil.copyDate(serviceDate);
				Console.log("Service Date : "+format.format(serviceDate));
				System.out.println("Service Date : "+format.format(serviceDate));
				if(serviceDate!=null){
					CalendarUtil.addMonthsToDate(lastValidDate, 1);
					lastValidDate.setDate(5);
					Console.log("Valid/Deadline date "+format.format(lastValidDate));
					System.out.println("Valid/Deadline date "+format.format(lastValidDate));
					Date todaysDate=new Date();
					lastValidDate=format.parse(format.format(lastValidDate));
					todaysDate=format.parse(format.format(todaysDate));
					Console.log("Todays Date : "+format.format(todaysDate));
					System.out.println("Todays Date : "+format.format(todaysDate));
					if(todaysDate.after(lastValidDate)){
						alert.alert("Service completion of previous month is not allowed..!");
						return false;
					}
				}else{
					alert.alert("Service date is null");
					return false;
				}
			}
			return true;
		}else{
			System.out.println("Config false");
			return true;
		}
	}
private void addDefaultValueInMarkCompletePopUp(ServiceMarkCompletePopUp markcompletepopup2) {
		
		Console.log("Inside addDefaultValue");
		Console.log("model.getServiceCompleteDuration() :::"+getService().getServiceCompleteDuration());
		Console.log("model.getServiceCompletionDate():::"+getService().getServiceCompletionDate());
		Console.log("model.getServiceCompleteRemark():::"+getService().getServiceCompleteRemark());

		if (getService().getServiceCompleteDuration() != 0)
			markcompletepopup2.getServiceTime().setValue(
					getService().getServiceCompleteDuration());

		if (getService().getServiceCompletionDate() != null) {
			DateTimeFormat sdf = DateTimeFormat.getFormat("dd/MM/yyyy");

			markcompletepopup2.getServiceCompletionDate().setValue(
					sdf.parse(sdf.format(getService().getServiceCompletionDate())));

		}

		if (getService().getServiceCompleteRemark() != null
				&& !getService().getServiceCompleteRemark().equalsIgnoreCase("")) {
			markcompletepopup2.getRemark().setValue(
					getService().getServiceCompleteRemark().trim());
		}

		if (getService().getCustomerFeedback() != null
				&& !getService().getCustomerFeedback().equalsIgnoreCase("")) {
			int index = 0;
			for (int i = 0; i < markcompletepopup2.getLbRating().getItemCount(); i++) {
				if (getService()
						.getCustomerFeedback()
						.trim()
						.equalsIgnoreCase(
								markcompletepopup2.getLbRating().getValue(i))) {
					index = i;
				}
			}
			
			Console.log("index:::"+index);
			markcompletepopup2.getLbRating().setSelectedIndex(index);
		}
		
	}

		public Service getService() {
			return service;
		}
		
		public void setService(Service service) {
			this.service = service;
		}

		public ServiceMarkCompletePopUp getMarkcompletepopup() {
			return markcompletepopup;
		}

		public void setMarkcompletepopup(ServiceMarkCompletePopUp markcompletepopup) {
			this.markcompletepopup = markcompletepopup;
		}

		public PopupPanel getPanel() {
			return panel;
		}

		public void setPanel(PopupPanel panel) {
			this.panel = panel;
		}
	@Override
	public void hidePopUp() {
			// TODO Auto-generated method stub
		materialValue = "";
		super.hidePopUp();
	}
		

}
