package com.slicktechnologies.client.views.service;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.code.p.gwtchismes.client.GWTCGlassPanel;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.ClickableTextCell;
import com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.SelectionCell;
import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.cell.client.ValueUpdater;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.Slick_Erp;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.notification.NotificationService;
import com.slicktechnologies.client.notification.NotificationServiceAsync;
import com.slicktechnologies.client.reusabledata.ConditionDialogBox;
import com.slicktechnologies.client.services.CommonService;
import com.slicktechnologies.client.services.CommonServiceAsync;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.CustomerProjectService;
import com.slicktechnologies.client.services.CustomerProjectServiceAsync;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.services.ServiceListService;
import com.slicktechnologies.client.services.ServiceListServiceAsync;
import com.slicktechnologies.client.services.SmsService;
import com.slicktechnologies.client.services.SmsServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.complain.ServiceDateBoxPopup;
import com.slicktechnologies.client.views.contract.ContractForm;
import com.slicktechnologies.client.views.device.CreateSingleServiceService;
import com.slicktechnologies.client.views.device.CreateSingleServiceServiceAsync;
import com.slicktechnologies.client.views.device.SaveService;
import com.slicktechnologies.client.views.device.SaveServiceAsync;
import com.slicktechnologies.client.views.device.ServicReschdulePopUp;
import com.slicktechnologies.client.views.fumigationAustralia.FumigationAustraliaPresenter;
import com.slicktechnologies.client.views.fumigationdetails.FumigationALPPresenter;
import com.slicktechnologies.client.views.fumigationdetails.FumigationPresenter;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.client.views.paymentinfo.billinglist.BillingListPresenter;
import com.slicktechnologies.client.views.popups.AddressPopup;
import com.slicktechnologies.client.views.popups.ContractDetailsPopup;
import com.slicktechnologies.client.views.project.concproject.ProjectForm;
import com.slicktechnologies.client.views.project.concproject.ProjectPresenter;
import com.slicktechnologies.client.views.technicianschedule.MaterialProductTable;
import com.slicktechnologies.client.views.technicianwarehousedetails.TechnicianServiceSchedulePopup;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.TechnicianWareHouseDetailsList;
import com.slicktechnologies.shared.TechnicianWarehouseDetails;
import com.slicktechnologies.shared.common.ModificationHistory;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.PriceListDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerlog.MaterialRequired;
import com.slicktechnologies.shared.common.fumigation.Fumigation;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.helperlayer.SmsConfiguration;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
import com.slicktechnologies.shared.common.inventory.MaterialMovementNote;
import com.slicktechnologies.shared.common.inventory.MaterialProduct;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.scheduling.TeamManagement;
import com.slicktechnologies.shared.common.servicerelated.ClientSideAsset;
import com.slicktechnologies.shared.common.servicerelated.CompanyAsset;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;
import com.slicktechnologies.shared.common.smshistory.SmsHistory;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.twilio.sdk.resource.instance.monitor.Alert;
import com.google.gwt.user.cellview.client.Header;

public class ServiceTable extends SuperTable<Service> implements ClickHandler {

	Logger logger=Logger.getLogger("Service Table");
	PopupPanel datepopupPanel;
	ServiceDateBoxPopup datepopup = new ServiceDateBoxPopup("Service Completion Date", null);
	Service serviceObject = null;

	/**
	 * Service to Load Branch and Employee
	 */
	CsvServiceAsync csvservice = GWT.create(CsvService.class);
	/**
	 * Contract ID Column
	 */
	TextColumn<Service> contractIdCol;

	/**
	 * 
	 */
	TextColumn<Service> serviceIdCol;
	Column<Service, String> srDownloadColumn;
	Column<Service, String> idSRDownloadColumn;//Ashwini Patil Date:15-05-2023
	/**
	 * View mode Service Eng Column
	 */
	private TextColumn<Service> serviceEng;
	/**
	 * View mode status Column
	 */
	private TextColumn<Service> status;
	/**
	 * View mode Date Column
	 */
	private TextColumn<Service> datecol;
	/**
	 * View mode Reason for Change column
	 */
	private TextColumn<Service> reasonForChange;

	private TextColumn<Service> customerID;
	private TextColumn<Service> customerName;
	private TextColumn<Service> customerPhone;
	/**Date 27-9-2019 by Amol**/
	private TextColumn<Service>customerPocName;
	/**Date 30-9-2019 by Amol added a completion remark Column**/
	private TextColumn<Service> completionRemark;
	TextColumn<Service> productNameCol;

	/**
	 * rohan added completion date in service list (Bhavani pest)
	 */

	TextColumn<Service> getCompletionDateColumn;
	/**
	 * ends here
	 */

	/**
	 * View mode branch Column
	 */
	private TextColumn<Service> branch;
	private TextColumn<Service> time;
	/**
	 * edit mode employee column
	 */
	private Column<Service, String> editserviceEng;
	/**
	 * edit mode date column
	 */
	private Column<Service, Date> editDate;
	/**
	 * edit mode status column
	 */
	private Column<Service, String> editStatus;
	/**
	 * edit mode reason for change column
	 */
	private Column<Service, String> editReasonForChange;
	/**
	 * edit mode branch column
	 */
	private Column<Service, String> editBranchColumn;
	/**
	 * Status list contains list of Service Status, value is hard coded
	 */
	private ArrayList<String> statusList;
	TextColumn<Service> refNumnber;

	
	/**
	 * Employee list contains List of employees , get filed from database
	 */
	public ArrayList<String> employeeList;
	/**
	 * Branch list contains list of branches
	 */
	public ArrayList<String> branchList;

	/**
	 * columnSort object to handle sorting
	 */
	ListHandler<Service> columnSort;

	public TextColumn<Service> serviceSrNo;
	public TextColumn<Service> serviceTime;
	public TextColumn<Service> serviceDay;
	public TextColumn<Service> serviceBranch;
	Header<Boolean> selectAllHeader;//Add By jayshree
	private Column<Service, Boolean> checkRecord;
	Column<Service, Boolean> checkColumn;

	// ///////////////////////////////////////////////////////

	public TextColumn<Service> team;
	public TextColumn<Service> locality;

	int rowIndex;
	Column<Service, String> viewButtonColumn;
	ServiceSchedulePopup schedule = new ServiceSchedulePopup();
	PopupPanel panel = new PopupPanel(true);
	protected GWTCGlassPanel glassPanel;

	ServiceForm form;
	Column<Service, String> markCompleteButton;

	public final GenricServiceAsync service = GWT.create(GenricService.class);
	public final ServiceListServiceAsync servicelistservice = GWT
			.create(ServiceListService.class);

	CustomerProjectServiceAsync projectAsync = GWT
			.create(CustomerProjectService.class);

	/**
	 * Date : 24-01-2017 By Anil Added ref no. 2 for PCAAMB and Pest Cure
	 * Incorporation
	 */
	TextColumn<Service> getRefNo2;
	/**
	 * End
	 */

	/**
	 * Date :- 07 Feb 2017 by vijay contract product level service remark will
	 * display in this column
	 * 
	 */

	TextColumn<Service> getContractproductlevelRemark;

	/**
	 * end here
	 */

	/**
	 * Date 03-04-2017 added by vijay
	 */
	TextColumn<Service> getcolumnServiceWeekNo;
	/**
	 * ends here
	 */

	/** 29-09-2017 sagar sore [adding service value column ] **/
	public TextColumn<Service> serviceValue;

	/** 05-10-2017 sagar sore [adding Number range column ] **/
	TextColumn<Service> numberRangeColumn;
	/**
	 * nidhi
	 * 
	 * 23-10-2017 alert for execption
	 */
	final GWTCAlert alert;

	/**
	 * nidhi
	 * Date : 4-12-2017
	 * 
	 */
	TextColumn<Service> serviceType;
	/**
	 *  end
	 */
	//Ashwini Patil Date:24-05-2022
	TextColumn<Service> premises;
	/*
	 *Date:11/08/2018
	 *Developer:Ashwini
	 *Des:To add contract details in customer service list 
	 */
	Column<Service, String> contactdetailsbutton;
	
	/*
	 * End by Ashwini
	 */
	
	/**
	 * end
	 */
	/** date 28.02.2018 added by komal for new changes in customer service list**/
	boolean validateflag = true;
	DateTimeFormat format1= DateTimeFormat.getFormat("dd-MMM-yyyy");
	GeneralServiceAsync genservice = GWT.create(GeneralService.class);
	ServicReschdulePopUp reschedule=new ServicReschdulePopUp();
	PopupPanel popup = new PopupPanel();
	Column<Service, String> rescheduleButton;
	Column<Service, String> scheduleButton;
	private long cellNo;
	private String companyName;
	private String actualMsg;
	/**
	 * end komal
	 */
	/** date 17.4.2018 added by komal for orion address popup **/
	AddressPopup addressPopup = new AddressPopup();
	Column<Service, String> clickServicingBranch;
	
	/** date 06.04.2018 added by komal for new fumigation **/
	Column<Service, String> fumigationButton;
	ConditionDialogBox conditionPopup = new ConditionDialogBox("Select fumigation certificate : " , AppConstants.FUMIGATIONMB , AppConstants.FUMIGTAIONALP , AppConstants.FUMIGATIONAFAS);
	PopupPanel popupPanel = new PopupPanel();
	GeneralViewDocumentPopup genralInvoicePopup;
	PopupPanel generalPanel;
	/**
	 * end komal
	 */

	ContractDetailsPopup contractpopup = new ContractDetailsPopup();//Added by Ashwini
	
	public TextColumn<Service> proModelNo,proSerialNo;
	public TextColumn<Service> proInvoiceNo;
	
	/** date 13.3.2019 added by komal for new schedule popup FOR nbhc**/
	TechnicianServiceSchedulePopup technicianServiceSchedulePopup = new TechnicianServiceSchedulePopup(false);
	TechnicianServiceSchedulePopup technicianServiceCompletePopup = new TechnicianServiceSchedulePopup(true);
	// Patch Patch Patch
	
	boolean hideplanandReschudleColumn=false;
	
	/** This flag is set true when service is scheduled from MultipleServiceSchedulePopup.
	 * When flag is true , then it will reflect in app. 
	 */
	TextColumn<Service> isserviceScheduled;
	
	/**
	 * @author Vijay Chougule
	 * Date 19-12-2019 for NBHC CCPM stack details for fumigation services
	 */
	TextColumn<Service> warehouseName;
	TextColumn<Service> stackName;
	TextColumn<Service> stackQty;
	TextColumn<Service> commodityName;
	boolean stackDeatilsFlag;
	TextColumn<Service> degassingServiceId;
	TextColumn<Service> shadeName;
	TextColumn<Service> warehouseCode;
	
	/**
	 * @author Anil
	 * @since 03-06-2020
	 */
	TextColumn<Service> assetIdCol;
	TextColumn<Service> componentNameCol;
	TextColumn<Service> mfgDateCol;
	TextColumn<Service> mfgNumCol;
	TextColumn<Service> replacementDateCol;
	TextColumn<Service> tierNameCol;
	
	TextColumn<Service> unitCol;
	
	boolean complainTatFlag=false;
	
	/**
	 * @author Vijay Chougule Date 09-09-2020
	 * Des :- Adding Service duration with process config for LifeLine
	 */
	 boolean serviceDurationFlag = false; 
	 TextColumn<Service> serviceDurationCol;
	 
	 /**Added by sheetal : 16-03-2022
	  * Des : Hiding Plan button with process configuration for WHC**/
	boolean hidePlanButtonFlag = false;
	
	final CommonServiceAsync commonserviceasync=GWT.create(CommonService.class);
	
	/**
	 * @author Anil
	 * @since 21-01-200
	 * service completion popup for service wise bill. raised by Nitin and Nithila
	 */
	ServiceDateBoxPopup serDatePopup=new ServiceDateBoxPopup("Service Completion Date");
	
	NumberFormat numberformat=NumberFormat.getFormat("0.00");
	
	TextColumn<Service> productFrequency;
	
	TextColumn<Service> cancellationRemark;
	
	Column<Service, String> callButton; //Ashwini Patil Date:10-10-2024 for hi tech

	public ServiceTable() {
		super();
		createStatusList();
		setHeight("800px");

		datepopup.getBtnOne().addClickHandler(this);
		datepopup.getBtnTwo().addClickHandler(this);
		alert = new GWTCAlert();
		schedule.getBtnEmployee().addClickHandler(this);

		schedule.getLblOk().addClickHandler(this);
		schedule.getLblCancel().addClickHandler(this);
		/** date 28.02.2018 added by komal for new customer service list changes **/
		reschedule.getBtnReschedule().addClickHandler(this);
		reschedule.getBtnCancel().addClickHandler(this);
		schedule.getMarkcompletepopup().getBtnOk().addClickHandler(this);
		schedule.getMarkcompletepopup().getBtnCancel().addClickHandler(this);
		/** date 18.4.2018 added by komal for orion **/
		addressPopup.getLblCancel().addClickHandler(this);
		

		/** date 06.04.2018 added by komal for new fumigation **/
		conditionPopup.getBtnOne().addClickHandler(this);
		conditionPopup.getBtnTwo().addClickHandler(this);
		conditionPopup.getBtnThree().addClickHandler(this);
		/**
		 * end komal
		 */
		
		contractpopup.getLblOk().addClickHandler(this);
		
		technicianServiceSchedulePopup = new TechnicianServiceSchedulePopup(false);   
		technicianServiceCompletePopup = new TechnicianServiceSchedulePopup(true);    
		technicianServiceSchedulePopup.getLblOk().addClickHandler(this);
		technicianServiceSchedulePopup.getLblCancel().addClickHandler(this);
		technicianServiceCompletePopup.getLblCancel().addClickHandler(this);
		technicianServiceSchedulePopup.getBtnEmployee().addClickHandler(this);
		technicianServiceCompletePopup.getMarkcompletepopup().getBtnOk().addClickHandler(this);
		technicianServiceCompletePopup.getMarkcompletepopup().getBtnCancel().addClickHandler(this);
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","ComplainServiceWithTurnAroundTime")){
			complainTatFlag=true;
		}
		
		serDatePopup.getBtnOne().addClickHandler(this);
		serDatePopup.getBtnTwo().addClickHandler(this);
			
	}

	public void retriveEmployees() {
		Vector<Filter> filtervalue = new Vector<Filter>();
		MyQuerry query = new MyQuerry();
		query.setFilters(filtervalue);
		query.setQuerryObject(new Employee());
		service.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {

				for (SuperModel model : result) {
					Employee entity = (Employee) model;
					employeeList.add(entity.getFullname());
				}
				addEditColumnEmployee();
			}
		});
	}

	public void retriveBranch() {
		Vector<Filter> filtervalue = new Vector<Filter>();
		MyQuerry query = new MyQuerry();
		query.setFilters(filtervalue);
		query.setQuerryObject(new Branch());
		service.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				for (int i = 0; i < result.size(); i++) {
					Branch entity = (Branch) result.get(i);
					branchList.add(entity.getBusinessUnitName());

				}
				addEditColumnBranch();
			}

			@Override
			public void onFailure(Throwable caught) {
			}
		});
	}

	@Override
	public void createTable() {
		/**
		 * @author Anil
		 * @since 03-06-2020
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","ComplainServiceWithTurnAroundTime")){
			complainTatFlag=true;
		}
		// Adding in view Mode HERE VIEW COLUMNS ARE CREATED AND ADDED ''
		System.out.println("in side create table method");

		addColumnCheckBox();
		/************* Rohan *****************/
		/**
		 * Rohan added this code for NBHC for removing mark complete button hide
		 * and only show to ADMIN role Date :17/01/2017
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","HideCompleteRescheduleFumigationColoumn")){
			hideplanandReschudleColumn=true;
			
		}
		if(!hideplanandReschudleColumn){
			if (AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","MakeServiceCompletionButtonVisibleToAdminRole")) {
				if (LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN")) {
					createColumnMarkComplete();
					updateColumnMarkComplete();
				}
			} else {
				createColumnMarkComplete();
				updateColumnMarkComplete();
			}
		}
		/**Added by Sheetal : 16-03-2022
		 * Des : Removing Plan button column with below process config for WHC**/
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","PC_HIDE_CS_LIST_PLAN_BUTTON")){
			hidePlanButtonFlag=true;
		}
		if(!hidePlanButtonFlag){
		createColumnViewProductColumn();
		}
		
		if(!hideplanandReschudleColumn){
			createColumnReschedule();
		}
		createColumnSchedule();
		if(complainTatFlag){
			addtierNameCol();
		}
		
		/**
		 * @author Anil @since 29-10-2021
		 * Sequencing service column as per the requirement raised by Pecopp , Nitin Sir and Nithila
		 */
//		addColumnserviceId();
		addIDSRDownload();//Ashwini Patil Date:15-05-2023
		addColumnName();
		
		//Ashwini Patil Date:10-10-2024 for hi tech
		if (AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","ShowCallButtonColumn")) {
			createCallColumn();
			updateCallColumn();
		} 
		
		addClickColumnServicingBranch();
		
		if(!complainTatFlag){
			addColumnLocality();
		}
		
		addColumnDate();
		addColumnStatus();
		
		addColumnCancellationRemark();
		
		addColumnServiceEng();
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","FrequencyAtProductLevel")){
			addProductFrequncyColumn();
		}

	
		addColumnTime();
		addColumnserviceSrNo();
        /**added by sheetal:16-11-2021 
         * sequencing product column as per the requirement by IPM and Fumigation , jayesh**/
		
		addColumnnProduct();
		addColumnPhone();
		addColumnPocName();
		
		
		createColumnScheduledStatus();
		addColumnPremises();
		addColumnServiceType();
		addColumngetCompletionDate();
		addColumngetCompletionRemark();
		addSRDownload();
		
		if(!AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","HideServiceValueExceptAdmin")||
				LoginPresenter.myUserEntity.getRole().getRoleName().equalsIgnoreCase("Admin")){				
			addColumnServiceValue();
		}
		addColumnDay();
		addColumnBranch();
		addColumnTeam();
		addColumnNumberRange();
		
//		addColumnContractProductLevelRemark();
		createColumnContractdetails();
		updateColumnContractdetails();
		
		
		
		
		addColumnCustomerId();
		addColumnContractId();
		
		
		/**
		 * @author Vijay Chougule
		 * Date 19-12-2019 for NBHC CCPM stack details with process config
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "EnableShowStackDetails")
				&& AppMemory.getAppMemory().currentScreen == Screen.INHOUSESERVICE){
			stackDeatilsFlag = true;
			addColumnWarehouseName();
			addColumnWarehouseCode();
			addColumnShadeName();
			addColumnStackName();
			addColumnStackQty();
			addColumnCommodityName();
			addColumnDeggasingServiceId();
		}
		
		
		serviceDurationFlag = AppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.SERVICEMODULE, AppConstants.PC_ADDSERVICEDURATIONCOLUMN);
		System.out.println("serviceDurationFlag"+serviceDurationFlag);
		if(serviceDurationFlag){
			addColumnServiceDuration();
		}
		
		if(!complainTatFlag){
//			addColumnLocality();
			addColumnRefNumber();
			getRefNo2();
		}
		
		if(!complainTatFlag){
			addColumnServiceWeekNo();
		}
		
		if(LoginPresenter.mapModelSerialNoFlag){
			addColumnProModelNo();
			addColumnProSerialNo();
		}
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "ServiceInvoiceMapping")){
			addColumnInvoiceNo();
		}
		
		if(!hidePlanButtonFlag){
			updateColumnViewProductColumn();
		}
	
		if(!hideplanandReschudleColumn){
			updateColumnReschedule();	
		}
		
		if(complainTatFlag){
			addassetIdCol();
			addcomponentNameCol();
			addmfgNumCol();
			addmfgDateCol();
			addreplacementDateCol();
			addUnitCol();
		}
	}

	

	


	

	

	private void addColumnCancellationRemark() {
		cancellationRemark = new TextColumn<Service>() {
			
			@Override
			public String getValue(Service object) {
				if(object.getRemark()!=null){
					return object.getRemark();
				}
				return "";
			}
		};
		table.addColumn(cancellationRemark, "Remark");
		table.setColumnWidth(cancellationRemark, 130, Unit.PX);
		cancellationRemark.setSortable(true);
	}

	private void addColumngetCompletionRemark() {
		completionRemark = new TextColumn<Service>() {

			@Override
			public String getValue(Service object) {
				// TODO Auto-generated method stub
				return object.getServiceCompleteRemark()+"";
			}
		};

		table.addColumn(completionRemark, "Completion Remark");
		table.setColumnWidth(completionRemark, 130, Unit.PX);
		completionRemark.setSortable(true);
	}

	private void addColumnPocName() {
		customerPocName = new TextColumn<Service>() {

			@Override
			public String getValue(Service object) {
				// TODO Auto-generated method stub
				return object.getPersonInfo().getPocName()+"";
			}
		};

		table.addColumn(customerPocName, "POC Name");
		table.setColumnWidth(customerPocName, 130, Unit.PX);
		customerPocName.setSortable(true);
	}

	/** 29-09-2017 sagar sore [creating column for service value] ***/
	public void addColumnServiceValue() {
		serviceValue = new TextColumn<Service>() {

			@Override
			public String getValue(Service object) {

				return  numberformat.format(object.getServiceValue());
			}
		};

		table.addColumn(serviceValue, "Service value");
		table.setColumnWidth(serviceValue, 110, Unit.PX);
		serviceValue.setSortable(true);
	}

	/** end **/

	private void addColumnServiceWeekNo() {

		getcolumnServiceWeekNo = new TextColumn<Service>() {

			@Override
			public String getValue(Service object) {
				return object.getServiceWeekNo() + "";
			}
		};
		table.addColumn(getcolumnServiceWeekNo, "WeekNo");
		table.setColumnWidth(getcolumnServiceWeekNo, 100, Unit.PX);
		getcolumnServiceWeekNo.setCellStyleNames("wordWrap");
		getcolumnServiceWeekNo.setSortable(true);
	}

	private void addColumnProModelNo() {

		proModelNo = new TextColumn<Service>() {

			@Override
			public String getValue(Service object) {
				return object.getProModelNo() + "";
			}
		};
		table.addColumn(proModelNo, "Product Model No");
		table.setColumnWidth(proModelNo, 100, Unit.PX);
//		getcolumnServiceWeekNo.setCellStyleNames("wordWrap");
		proModelNo.setSortable(true);
	}
	private void addColumnProSerialNo() {

		proSerialNo = new TextColumn<Service>() {

			@Override
			public String getValue(Service object) {
				return object.getProSerialNo() + "";
			}
		};
		table.addColumn(proSerialNo, "Product Serail No");
		table.setColumnWidth(proSerialNo, 100, Unit.PX);
//		getcolumnServiceWeekNo.setCellStyleNames("wordWrap");
		proSerialNo.setSortable(true);
	}
	private void addColumnContractProductLevelRemark() {

		getContractproductlevelRemark = new TextColumn<Service>() {

			@Override
			public String getValue(Service object) {
				System.out.println(" in table remark ===" + object.getRemark());
				if (object.getRemark() != null)
					return object.getRemark();
				else
					return "";
			}
		};
		table.addColumn(getContractproductlevelRemark, "Remark");
		table.setColumnWidth(getContractproductlevelRemark, 180, Unit.PX);
		getContractproductlevelRemark.setCellStyleNames("wordWrap");
		getContractproductlevelRemark.setSortable(true);
	}

	protected void addColumngetCompletionDate() {

		getCompletionDateColumn = new TextColumn<Service>() {
			@Override
			public String getValue(Service object) {
				if (object.getServiceCompletionDate() != null)
					return AppUtility.parseDate(object
							.getServiceCompletionDate());
				else
					return " ";
			}
		};
		table.addColumn(getCompletionDateColumn, "Completion Date");
		table.setColumnWidth(getCompletionDateColumn, 120, Unit.PX);
		getCompletionDateColumn.setSortable(true);
	}

	private void addColumnRefNumber() {

		refNumnber = new TextColumn<Service>() {

			@Override
			public String getValue(Service object) {
				return object.getRefNo() + "";
			}
		};
		table.addColumn(refNumnber, "Ref No 1");
		table.setColumnWidth(refNumnber, "100px");
		refNumnber.setSortable(true);
	}

	private void getRefNo2() {
		getRefNo2 = new TextColumn<Service>() {
			@Override
			public String getValue(Service object) {
				if (object.getRefNo2() != null) {
					return object.getRefNo2() + "";
				} else
					return "N.A.";
			}
		};
		table.addColumn(getRefNo2, "Ref No.2");
		table.setColumnWidth(getRefNo2, 180, Unit.PX);
		getRefNo2.setSortable(true);
	}

	/********************* vijay ****************************/

	private void createColumnMarkComplete() {

		ButtonCell btnCell = new ButtonCell();
		markCompleteButton = new Column<Service, String>(btnCell) {

			@Override
			public String getValue(Service object) {
				// TODO Auto-generated method stub
				return "Complete";
			}
		};

		table.addColumn(markCompleteButton, "");
		table.setColumnWidth(markCompleteButton, 110, Unit.PX);
	}

	private void updateColumnMarkComplete() {
		markCompleteButton.setFieldUpdater(new FieldUpdater<Service, String>() {
			@Override
			public void update(int index, final Service object, String value) {
				
				ServicePresenter.viewDocumentFlag=false;
				
				/*
				 * Added by Sheetal:27-10-2021
				 * Des : Document upload in Customer Report section should be mandatory before closure of service
				 */
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "PC_DocumentUploadMandatory")){
					if(AppUtility.ValidateCustomerReport(object.getUptestReport(),object.getServiceSummaryReport2())){
						GWTCAlert alert = new GWTCAlert();
						alert.alert("Please upload atleast one document in Customer Report tab on Customer Service page to complete service");
						Console.log("Validate message");
						return;
						
					}
				
				}
				
				
				/*
				 * Ashwini Patil 
				 * Date:29-08-2024
				 * Orion does not want to allow service completion of last month services after specific deadline
				 * deadline is defined in company - Customer service setup - Service completion deadline field
				 */
				boolean validDate=AppUtility.validateServiceCompletionDate(object.getServiceDate());
				Console.log("validDate="+validDate);
				if(!validDate) {
					GWTCAlert alert = new GWTCAlert();
					alert.alert("You cannot completed old service now!");
					return;
				}
				/**
				 * date 12.04.2018 added by komal for orion
				 */
				boolean configFlag = AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "RestrictServiceCompletionForServicewiseBilling");
				logger.log(Level.SEVERE , "value of flag : " +configFlag + " "+"service wise billing : "+serviceObject.isServiceWiseBilling());
				System.out.println("value of flag : " +configFlag + " "+"service wise billing : "+serviceObject.isServiceWiseBilling());
				if(object.isServiceWiseBilling() && configFlag){
	            	GWTCAlert alert = new GWTCAlert();
					alert.alert("As it is service wise bill-service, you can not complete it from here.");
	            }else{

				/** added by komal for cancelled services **/
				System.out.println("Complete button : " + object.getStatus());
				if (object.getStatus().equalsIgnoreCase(Service.CANCELLED)) {
					final GWTCAlert alert = new GWTCAlert();
					alert.alert("Can not complete the service as it is cancelled");
				} 
				
				
				/**
				 * Date 16-4-2018
				 * By Jayshree
				 * Add validation to complete service
				 */
				else if(object.getStatus().equalsIgnoreCase(Service.SERVICESUSPENDED)){
					final GWTCAlert alert = new GWTCAlert();
					alert.alert("Can not complete the service as it is Suspended !");
				}
				
				else {
					if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "TechnicianwiseMaterialPopup")){
						if(checkStatus(object)){//komal
							rowIndex = index;
							serviceObject = object;
							getProjectAndBOMDetails(object, technicianServiceCompletePopup, true);
							technicianServiceCompletePopup.getPopup().getElement()
							.addClassName("servicepopup");
							technicianServiceCompletePopup.getPopup().center();
						}
						
					}else{
						/**
						 * @author Anil 
						 * @since 21-01-2022
						 * Capture quantity at the time of service completion
						 * raised by Nitin Sir and Nithila for Hygienic Pest
						 */
						if(object.isServiceWiseBilling()||object.isRateContractService()){
							serDatePopup.cleardate();
							serDatePopup.getServiceDate().setValue(new Date());
							serDatePopup.getDbQuantity().setValue(1d);
							datepopupPanel = new PopupPanel(true);
							datepopupPanel.add(serDatePopup);
							datepopupPanel.show();
							datepopupPanel.center();
							rowIndex = index;
							serviceObject = object;
							return;
						}
						datepopup.cleardate();
						datepopupPanel = new PopupPanel(true);
						datepopupPanel.add(datepopup);
						datepopupPanel.show();
						datepopupPanel.center();
						rowIndex = index;
						serviceObject = object;
					}
					
				}
	          }
			}
		});
	}

	private boolean checkvalidation(Date toodaydate, Date serviceDate) {
		// TODO Auto-generated method stub
		boolean flag = true;
		/**
		 * @author Anil
		 * @since 09-11-2020
		 * As service date's time is default set to 13, cuasing an issue while completing service before 13:00 PM
		 */
		if(AppUtility.formatDate(AppUtility.parseDate(serviceDate)).after(AppUtility.formatDate(AppUtility.parseDate(toodaydate))))
//		if (serviceDate.after(toodaydate))
			flag = false;

		return flag;
	}

	/******************* end here ***************************/

	private void setFieldUpdateOnCheckbox()

	{
		System.out.println("In Field Updator");
		checkRecord.setFieldUpdater(new FieldUpdater<Service, Boolean>() {

			@Override
			public void update(int index, Service object, Boolean value) {
				try {
					Boolean val1 = (value);
					object.setRecordSelect(val1);
				} catch (Exception e) {
				}
				table.redrawRow(index);

			}
		});
	}

	private void addColumnCheckBox() {
		ServicePresenter.viewDocumentFlag=false;
//		CheckboxCell checkCell = new CheckboxCell();
//		checkRecord = new Column<Service, Boolean>(checkCell) {
//
//			@Override
//			public Boolean getValue(Service object) {
//
//				return object.getRecordSelect();
//			}
//		};
//		table.addColumn(checkRecord, "Select");
//		table.setColumnWidth(checkRecord, 60, Unit.PX);

		
		/**
		 * Date 12/3/2018
		 * by Jayshree
		 * Des.add the check box to select the services
		 */

		checkColumn=new Column<Service, Boolean>(new CheckboxCell()) {
			@Override
			public Boolean getValue(Service object) {
				return object.getRecordSelect();
			}
		};
		
		
		checkColumn.setFieldUpdater(new FieldUpdater<Service, Boolean>() {
			@Override
			public void update(int index, Service object, Boolean value) {
				System.out.println("Check Column ....");
				
				ServicePresenter.viewDocumentFlag=false;

				object.setRecordSelect(value);
				table.redrawRow(index);
				selectAllHeader.getValue();
				table.redrawHeaders();
				
				glassPanel = new GWTCGlassPanel();
				glassPanel.show();
				Timer timer = new Timer() {
					@Override
					public void run() {
						glassPanel.hide();
					}
				};
				timer.schedule(2000); 
			}
		});
		
		
		selectAllHeader =new Header<Boolean>(new CheckboxCell()) {
			@Override
			public Boolean getValue() {
				if(getDataprovider().getList().size()!=0){
					
				}
				return false;
			}
		};
		
		
		selectAllHeader.setUpdater(new ValueUpdater<Boolean>() {
			@Override
			public void update(Boolean value) {
				List<Service> list=getDataprovider().getList();
				for(Service object:list){
					object.setRecordSelect(value);
				}
				getDataprovider().setList(list);
				table.redraw();
				addColumnSorting();
			}
		});
		table.addColumn(checkColumn,selectAllHeader);
		table.setColumnWidth(checkColumn, 65, Unit.PX);
		/*** Date 09-03-2021 By Vijay for Checkboxwidth to manage click on widget and row***/
		checkColumn.setCellStyleNames("checkboxWidth");
		selectAllHeader.setHeaderStyleNames("checkboxWidth");
	}

	private void addColumnTeam() {
		team = new TextColumn<Service>() {
			@Override
			public String getValue(Service object) {
				if (object.getTeam() != null) {
					return object.getTeam();
				} else {
					return "";
				}
			}
		};
		table.addColumn(team, "Team");
		table.setColumnWidth(team, 120, Unit.PX);
		team.setSortable(true);
	}

	private void addColumnLocality() {
		locality = new TextColumn<Service>() {
			@Override
			public String getValue(Service object) {
				if (object.getLocality() != null) {
					return object.getLocality();
				} else {
					return "";
				}
			}
		};
		table.addColumn(locality, "Locality");
		table.setColumnWidth(locality, 140, Unit.PX);
		locality.setSortable(true);
	}

	/*************************************** Column Addition Part ***************************************************/

	private void addColumnnProduct() {
		productNameCol = new TextColumn<Service>() {

			@Override
			public String getValue(Service object) {
				return object.getProductName();
			}
		};

		table.addColumn(productNameCol, "Product");
		table.setColumnWidth(productNameCol, 120, Unit.PX);
		productNameCol.setSortable(true);

	}

	public void addColumnContractId() {
		contractIdCol = new TextColumn<Service>() {

			@Override
			public String getValue(Service object) {
				// TODO Auto-generated method stub

				return object.getContractCount() + "";
			}
		};

		table.addColumn(contractIdCol, "Contract ID");
		table.setColumnWidth(contractIdCol, 100, Unit.PX);
		contractIdCol.setSortable(true);
	}

	public void addColumnName() {
		customerName = new TextColumn<Service>() {

			@Override
			public String getValue(Service object) {
				// TODO Auto-generated method stub
				return object.getName();
			}
		};

		table.addColumn(customerName, "Customer Name");
		table.setColumnWidth(customerName, 150, Unit.PX);
		customerName.setSortable(true);
	}

	public void addColumnBranch() {
		branch = new TextColumn<Service>() {

			@Override
			public String getValue(Service object) {
				return object.getBranch();
			}
		};

		table.addColumn(branch, "Branch");
		table.setColumnWidth(branch, 90, Unit.PX);
		branch.setSortable(true);
	}

	public void addColumnPhone() {
		customerPhone = new TextColumn<Service>() {

			@Override
			public String getValue(Service object) {
				// TODO Auto-generated method stub
				return object.getCellNumber() + "";
			}
		};

		table.addColumn(customerPhone, "Phone");
		table.setColumnWidth(customerPhone, 130, Unit.PX);
		customerPhone.setSortable(true);
	}

	public void addColumnAdress() {
		TextColumn<Service> customerAdress = new TextColumn<Service>() {

			@Override
			public String getValue(Service object) {

				if (object.getAddress() != null) {
					return object.getAddress().toString();
				}
				return "N.A";
			}
		};

		table.addColumn(customerAdress, "Customer Adress");
		customerAdress.setSortable(true);
	}

	public void addColumnDate() {
		final Date todaysDate = new Date();
		CalendarUtil.addDaysToDate(todaysDate, -1);
		todaysDate.setHours(23);
		todaysDate.setMinutes(59);
		todaysDate.setSeconds(59);

		final Date date = new Date();
		CalendarUtil.addDaysToDate(date, 7);
		date.setHours(23);
		date.setMinutes(59);
		date.setSeconds(59);

		datecol = new TextColumn<Service>() {
			@Override
			public String getCellStyleNames(Context context, Service object) {
				if ((object.getServiceDate().before(todaysDate))
						&& (object.getStatus().equals("Scheduled"))) {
					return "red";
				} else if ((object.getServiceDate().before(date) && object
						.getServiceDate().after(new Date()))
						&& (object.getStatus().equals("Scheduled"))) {
					return "orange";
				} else if (object.getStatus().equals("Completed")) {
					return "green";
				} else if (object.getStatus().trim().equals("Rescheduled")) {
					return "blue";
				} else {
					return "black";
				}
			}

			@Override
			public String getValue(Service object) {
				// TODO Auto-generated method stub
				if (object.getServiceDate() != null) {
					String serDate = AppUtility.parseDate(object
							.getServiceDate());
					return serDate;
				}
				return "Not Set";
			}
		};

		table.addColumn(datecol, "Service Date");
		table.setColumnWidth(datecol, 120, Unit.PX);
		datecol.setSortable(true);
	}

	private void addColumnTime() {

		serviceTime = new TextColumn<Service>() {

			@Override
			public String getValue(Service object) {
				return object.getServiceTime();
			}
		};

		table.addColumn(serviceTime, "Service Time");
		table.setColumnWidth(serviceTime, 120, Unit.PX);
		serviceTime.setSortable(true);

	}

	private void addColumnDay() {

		serviceDay = new TextColumn<Service>() {

			@Override
			public String getValue(Service object) {
				if (!object.getServiceDay().equals("Not Select")) {
					return object.getServiceDay();
				} else {
					return "";
				}
			}
		};

		table.addColumn(serviceDay, "Service Day");
		table.setColumnWidth(serviceDay, 120, Unit.PX);
		serviceDay.setSortable(true);

	}

	private void addColumnServiceBranch() {

		serviceBranch = new TextColumn<Service>() {

			@Override
			public String getValue(Service object) {
				return object.getServiceBranch();
			}
		};

		table.addColumn(serviceBranch, "Service Branch");
		table.setColumnWidth(serviceBranch, 120, Unit.PX);
		serviceBranch.setSortable(true);

	}

	public void addColumnReasonForChange() {
		reasonForChange = new TextColumn<Service>() {

			@Override
			public String getValue(Service object) {
				if (object.getReasonForChange().trim().equals(""))
					return "N.A";
				return object.getReasonForChange();
			}
		};

		table.addColumn(reasonForChange, "Change Reason");
		table.setColumnWidth(reasonForChange, 120, Unit.PX);
		reasonForChange.setSortable(true);
	}

	public void addColumnServiceEng() {
		serviceEng = new TextColumn<Service>() {

			@Override
			public String getValue(Service object) {
				// TODO Auto-generated method stub
				return object.getEmployee();
			}
		};

//		table.addColumn(serviceEng, "Service Engineer");
		/**@Sheetal:17-03-2022 
		 *  Des : Renaming Service Engineer to Technician, requirement by nitin sir**/
		table.addColumn(serviceEng, "Technician");
		table.setColumnWidth(serviceEng, 120, Unit.PX);
		serviceEng.setSortable(true);
	}

	public void addColumnStatus() {
		status = new TextColumn<Service>() {

			@Override
			public String getValue(Service object) {

				return object.getStatus();
			}
		};

		table.addColumn(status, "Status");
		table.setColumnWidth(status, 100, Unit.PX);
		status.setSortable(true);
	}

	public void addColumnCustomerId() {
		customerID = new TextColumn<Service>() {

			@Override
			public String getValue(Service object) {

				return object.getCustomerId() + "";
			}
		};

		table.addColumn(customerID, "Customer ID");
		table.setColumnWidth(customerID, 110, Unit.PX);
		customerID.setSortable(true);
	}

	public void addEditColumnReasonForchange() {

		EditTextCell editcel = new EditTextCell();
		editReasonForChange = new Column<Service, String>(editcel) {

			@Override
			public String getValue(Service object) {
				if (object.getReasonForChange().trim().equals(""))
					return "N.A";
				return object.getReasonForChange();
			}
		};

		table.addColumn(editReasonForChange, "Change Reason");
		table.setColumnWidth(editReasonForChange, 150, Unit.PX);

	}

	public void addEditColumnBranch() {
		SelectionCell branchselection = new SelectionCell(branchList);
		editBranchColumn = new Column<Service, String>(branchselection) {

			@Override
			public String getValue(Service object) {

				return object.getBranch();
			}
		};

	}

	public void addEditColumnEmployee() {
		SelectionCell employeeselection = new SelectionCell(employeeList);
		editserviceEng = new Column<Service, String>(employeeselection) {

			@Override
			public String getValue(Service object) {

				return object.getEmployee();
			}
		};

	}

	public void addEditColumnDate() {

		DateTimeFormat fmt = DateTimeFormat
				.getFormat(PredefinedFormat.DATE_SHORT);

		DatePickerCell startdate = new DatePickerCell(fmt);
		editDate = new Column<Service, Date>(startdate) {

			@Override
			public Date getValue(Service object) {
				if (object.getServiceDate() == null)
					return new Date();
				else
					return object.getServiceDate();

			}

		};
		table.addColumn(editDate, "Service Date");
		table.setColumnWidth(editDate, 120, Unit.PX);
		editDate.setSortable(true);
	}

	public void addEditColumnStatus() {
		SelectionCell selctcell = new SelectionCell(statusList);
		editStatus = new Column<Service, String>(selctcell) {

			@Override
			public String getValue(Service object) {

				return object.getStatus();
			}
		};
		table.addColumn(editStatus, "Status");
		table.setColumnWidth(editStatus, "130px");
		editStatus.setSortable(true);
	}

	
	public void addColumnserviceId() {

		serviceIdCol = new TextColumn<Service>() {

			@Override
			public String getValue(Service object) {
				return object.getCount() + "";
			}
		};
		table.addColumn(serviceIdCol, "Service ID");
		table.setColumnWidth(serviceIdCol, "100px");
		serviceIdCol.setSortable(true);
	}
	
	//Ashwini Patil Date:1-03-2023
		private  void addSRDownload(){
			
			srDownloadColumn=new Column<Service, String>(new ClickableTextCell()) {
				@Override
				public String getCellStyleNames(Context context, Service object) {
						return "blue";
				}
				
				@Override
				public String getValue(Service object) {
					// TODO Auto-generated method stub
					if(object.getUptestReport()!=null&&object.getUptestReport().getName()!=null)
						return object.getUptestReport().getName();
					else
						return "";
				}
			};
			
			
			table.addColumn(srDownloadColumn, "Service Report1");
			table.setColumnWidth(srDownloadColumn, 100, Unit.PX);
			
			srDownloadColumn.setFieldUpdater(new FieldUpdater<Service, String>() {
				
				@Override
				public void update(int index, Service object, String value) {
					// TODO Auto-generated method stub
					if(object.getUptestReport()!=null){
						String filename="";
						int month=object.getServiceDate().getMonth()+1;
						int year=object.getServiceDate().getYear()+1900;
						if(object.getServiceBranch()!=null)
							filename=object.getServiceBranch()+"-"+object.getCustomerName()+"-"+month+"-"+year;
						else
							filename=object.getUptestReport().getName()+"-"+object.getCustomerName()+"-"+month+"-"+year;
						
						String url=object.getUptestReport().getUrl()+"&filename="+filename+".jpg";
						Window.open(url, "_blank", "");
						//https://my-dot-evaerp282915.appspot.com/slick_erp/downloadMapImage?blobkey=AMIfv96FnGlPnJLuG2pQhkTEEhVGA1IZ2bGIy4sYJwksLeBPv7gLMQ4OYV4DAgfd-Fww-6NtSea2VfIrVkLbH-YRW3Y8kIBBVCbMZoxvF6M719z59cJ0irfqudoKvEBw3kanmirEHPyLSmH9Jd_EWJ57J978p84T5FGx5blptAeuiGrw8IAr82SteALS8eH9dUhIA5fA8UTy3qciVYJNQfXyWg397_u4xKy8DAi0ZImUU_akGhveEQGvExv0FeM3Mb3LF3wE9h-lsPAwKZ9TbRYGf2JfIknBAg&filename=abc.jpg
					}else{
						Window.alert("No image found");
					}
				}
			});
		}

	/*************************************** Field Updater Part *********************************************************/

	public void addFieldUpdaterBranch() {
		editBranchColumn.setFieldUpdater(new FieldUpdater<Service, String>() {

			@Override
			public void update(int index, Service object, String value) {
				if (value.contains("Select") == false) {
					object.setBranch(value);
					table.redrawRow(index);

				}
			}
		});
		table.addColumn(editBranchColumn, "Branch");
		editBranchColumn.setSortable(true);
	}

	public void addFieldUpdaterEmployee() {
		editserviceEng.setFieldUpdater(new FieldUpdater<Service, String>() {

			@Override
			public void update(int index, Service object, String value) {
				if (value.contains("Select") == false) {
					object.setEmployee(value);
					table.redrawRow(index);

				}
			}
		});

	}

	public void addFiledUpdaterDate() {
		editDate.setFieldUpdater(new FieldUpdater<Service, Date>() {

			@Override
			public void update(int index, Service object, Date value) {
				object.setServiceDate(value);
				table.redrawRow(index);

			}
		});

	}

	public void addFiledUpdaterReasonForchange() {
		editReasonForChange
				.setFieldUpdater(new FieldUpdater<Service, String>() {

					@Override
					public void update(int index, Service object, String value) {
						object.setReasonForChange(value);
						table.redrawRow(index);
					}
				});
	}

	public void addFieldUpdaterStatus() {
		editStatus.setFieldUpdater(new FieldUpdater<Service, String>() {

			@Override
			public void update(int index, Service object, String value) {

				object.setStatus(value);
				if (object.getStatus().equals(Service.SERVICESTATUSCANCELLED))
					table.getRowElement(index).getStyle()
							.setBackgroundColor("Red");

				if (object.getStatus().equals(Service.SERVICESTATUSCOMPLETED))
					table.getRowElement(index).getStyle()
							.setBackgroundColor("Green");

				if (object.getStatus().equals(Service.SERVICESTATUSRESCHEDULE)) {
					Service rescheduled = null;
					rescheduled = object.Myclone();

					object.setStatus(Service.SERVICESTATUSRESCHEDULE);
					object.setReasonForChange("Service Rescheduled");
					rescheduled.setStatus(Service.SERVICESTATUSSCHEDULE);
					if (rescheduled != null)
						getDataprovider().getList().add(rescheduled);
					Collections.sort(getDataprovider().getList());
					getDataprovider().refresh();
					// table.getRowElement(index).getStyle().setBackgroundColor("YELLOW");
				}
			}
		});

	}

	/*************************************** Field Updater Part *********************************************************/

	public void downloadServiceecsv() {
		ArrayList<Service> custarray = new ArrayList<Service>();
		List<Service> list = (List<Service>) getDataprovider().getList();

		custarray.addAll(list);

		csvservice.setservice(custarray, new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed" + caught);

			}

			@Override
			public void onSuccess(Void result) {
				final String url = GWT.getModuleBaseURL() + "csvservlet"
						+ "?type=" + 5;
				Window.open(url, "test", "enabled");
			}
		});
	}

	public void createStatusList() {
		statusList = new ArrayList<String>();
		statusList.add(Service.SERVICESTATUSCANCELLED);
		statusList.add(Service.SERVICESTATUSCOMPLETED);
		statusList.add(Service.SERVICESTATUSPENDING);
		statusList.add(Service.SERVICESTATUSRESCHEDULE);
		statusList.add(Service.SERVICESTATUSSCHEDULE);

	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub

	}

	public void addColumnSorting() {
		addServiceEnggSorting();
		addSerialNoSorting();
		if(!complainTatFlag){
			addsortingRefNumber();
			addsortingRefNumber2();
			addColumnServiceWeekNoSorting();
		}
		addDateSorting();
		addServiceTime();
		addServiceDay();
		addServiceBranch();

		addReasonForChangeSorting();
		addBranchSorting();
		addNameSorting();
		addPhoneSorting();
		
		addpocNameSorting();
		addCompletionRemarkSorting();
		addContractIdSorting();
		addStatusSorting();
		addServiceIdSorting();
		addCustomerIdSorting();
		addSProductSorting();
		addTeamSorting();
		/**
		 * nidhi
		 * Date : 4-12-2017
		 */
		addPremisesSorting();
		addServiceTypeSorting();
		/**
		 * END
		 */
		addLocalitySorting();
		addColumnContractProductLevelRemarkSorting();
		
		

		addServiceValueSorting();

		/*** 05-10-2017 sagar sore[ sorting for number range] **/
		addSortinggetNumberRange();
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","ServiceInvoiceMapping")){
			addSortinggetInvoiceNo();
		}
		addSortingIsServiceScheduled();
		addServiceBranchSorting();
		if(stackDeatilsFlag){
			addSortingWarehouseName();
			addSortingStackName();
			addSortingStackQty();
			addSortingCommodityName();
			addSortingDegassingServiceId();
			
			addSortingShadeName();
			addSortingWarehouseCode();
		}
		
		if(complainTatFlag){
			addSortingOnAssetIdCol();
			addSortingOnComponentName();
			addSortingOnMfgDate();
			addSortingOnMfgNo();
			addSortingOnReplacementDate();
			addSortingOnTierName();
			addSortingUnit();
		}
		if(serviceDurationFlag){
			addSortingServiceDuration();
		}
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","FrequencyAtProductLevel")){
			addSortingProductFrequncy();
		}

	}

	

	

	

	private void addCompletionRemarkSorting() {
		List<Service> list = getDataprovider().getList();
		columnSort = new ListHandler<Service>(list);
		columnSort.setComparator(completionRemark, new Comparator<Service>() {

			@Override
			public int compare(Service e1, Service e2) {
				if (e1 != null && e2 != null) {
					if (e1.getServiceCompleteRemark() != null && e2.getServiceCompleteRemark() != null) {
						return e1.getServiceCompleteRemark().compareTo(e2.getServiceCompleteRemark());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);

	
	
		// TODO Auto-generated method stub
		
	}

	private void addpocNameSorting() {
		List<Service> list = getDataprovider().getList();
		columnSort = new ListHandler<Service>(list);
		columnSort.setComparator(customerPocName, new Comparator<Service>() {

			@Override
			public int compare(Service e1, Service e2) {
				if (e1 != null && e2 != null) {
					if (e1.getPersonInfo().getPocName() != null && e2.getPersonInfo().getPocName() != null) {
						return e1.getPersonInfo().getPocName().compareTo(e2.getPersonInfo().getPocName());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);

	
	}

	/** 29-09-2017 sagar sore [ for sorting of service value] **/
	private void addServiceValueSorting() {
		List<Service> list = getDataprovider().getList();
		columnSort = new ListHandler<Service>(list);
		columnSort.setComparator(serviceValue, new Comparator<Service>() {

			@Override
			public int compare(Service e1, Service e2) {
				if (e1 != null && e2 != null) {
					if (e1.getServiceValue() == e2.getServiceValue()) {
						return 0;
					}
					if (e1.getServiceValue() > e2.getServiceValue()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	/** end **/

	private void addColumnServiceWeekNoSorting() {

		List<Service> list = getDataprovider().getList();
		columnSort = new ListHandler<Service>(list);
		columnSort.setComparator(getcolumnServiceWeekNo,
				new Comparator<Service>() {

					@Override
					public int compare(Service e1, Service e2) {
						if (e1 != null && e2 != null) {
							if (e1.getServiceWeekNo() == e2.getServiceWeekNo()) {
								return 0;
							}
							if (e1.getServiceWeekNo() > e2.getServiceWeekNo()) {
								return 1;
							} else {
								return -1;
							}
						} else {
							return 0;
						}
					}
				});
		table.addColumnSortHandler(columnSort);

	}

	private void addColumnContractProductLevelRemarkSorting() {

		List<Service> list = getDataprovider().getList();
		columnSort = new ListHandler<Service>(list);
		columnSort.setComparator(getContractproductlevelRemark,
				new Comparator<Service>() {

					@Override
					public int compare(Service e1, Service e2) {
						if (e1 != null && e2 != null) {
							if (e1.getRemark() != null
									&& e2.getRemark() != null) {
								return e1.getRemark().compareTo(e2.getRemark());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	private void addsortingRefNumber() {
		List<Service> list = getDataprovider().getList();
		columnSort = new ListHandler<Service>(list);
		columnSort.setComparator(refNumnber, new Comparator<Service>() {

			@Override
			public int compare(Service e1, Service e2) {
				if (e1 != null && e2 != null) {
					if (e1.getRefNo() != null && e2.getRefNo() != null) {
						return e1.getRefNo().compareTo(e2.getRefNo());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addsortingRefNumber2() {

		List<Service> list = getDataprovider().getList();
		columnSort = new ListHandler<Service>(list);
		columnSort.setComparator(getRefNo2, new Comparator<Service>() {

			@Override
			public int compare(Service e1, Service e2) {
				if (e1 != null && e2 != null) {
					if (e1.getRefNo2() != null && e2.getRefNo2() != null) {
						return e1.getRefNo2().compareTo(e2.getRefNo2());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	public void addServiceIdSorting() {
		List<Service> list = getDataprovider().getList();
		columnSort = new ListHandler<Service>(list);
		columnSort.setComparator(serviceIdCol, new Comparator<Service>() {

			@Override
			public int compare(Service e1, Service e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCount() == e2.getCount()) {
						return 0;
					}
					if (e1.getCount() > e2.getCount()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);

	}

	public void addCustomerIdSorting() {
		List<Service> list = getDataprovider().getList();
		columnSort = new ListHandler<Service>(list);
		columnSort.setComparator(customerID, new Comparator<Service>() {

			@Override
			public int compare(Service e1, Service e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCustomerId() == e2.getCustomerId()) {
						return 0;
					}
					if (e1.getCustomerId() > e2.getCustomerId()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	public void addServiceEnggSorting() {
		List<Service> list = getDataprovider().getList();
		columnSort = new ListHandler<Service>(list);
		columnSort.setComparator(serviceEng, new Comparator<Service>() {

			@Override
			public int compare(Service e1, Service e2) {
				if (e1 != null && e2 != null) {
					if (e1.getEmployee() != null && e2.getEmployee() != null) {
						return e1.getEmployee().compareTo(e2.getEmployee());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);

	}

	public void addSProductSorting() {
		List<Service> list = getDataprovider().getList();
		columnSort = new ListHandler<Service>(list);
		columnSort.setComparator(productNameCol, new Comparator<Service>() {

			@Override
			public int compare(Service e1, Service e2) {
				if (e1 != null && e2 != null) {
					if (e1.getProductName() != null
							&& e2.getProductName() != null) {
						return e1.getProductName().compareTo(
								e2.getProductName());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	public void addStatusSorting() {
		List<Service> list = getDataprovider().getList();
		columnSort = new ListHandler<Service>(list);
		columnSort.setComparator(status, new Comparator<Service>() {

			@Override
			public int compare(Service e1, Service e2) {
				if (e1 != null && e2 != null) {
					if (e1.getStatus() != null && e2.getStatus() != null) {
						return e1.getStatus().compareTo(e2.getStatus());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	public void addContractIdSorting() {
		List<Service> list = getDataprovider().getList();
		columnSort = new ListHandler<Service>(list);
		columnSort.setComparator(contractIdCol, new Comparator<Service>() {

			@Override
			public int compare(Service e1, Service e2) {
				if (e1 != null && e2 != null) {
					if (e1.getContractCount() == e2.getContractCount()) {
						return 0;
					}
					if (e1.getContractCount() > e2.getContractCount()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	public void addPhoneSorting() {
		List<Service> list = getDataprovider().getList();
		columnSort = new ListHandler<Service>(list);
		columnSort.setComparator(customerPhone, new Comparator<Service>() {

			@Override
			public int compare(Service e1, Service e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCellNumber() == e2.getCellNumber()) {
						return 0;
					}
					if (e1.getCellNumber() > e2.getCellNumber()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	public void addNameSorting() {
		List<Service> list = getDataprovider().getList();
		columnSort = new ListHandler<Service>(list);
		columnSort.setComparator(customerName, new Comparator<Service>() {

			@Override
			public int compare(Service e1, Service e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCustomerName() != null
							&& e2.getCustomerName() != null) {
						return e1.getCustomerName().compareTo(
								e2.getCustomerName());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	public void addDateSorting() {
		List<Service> list = getDataprovider().getList();
		columnSort = new ListHandler<Service>(list);
		columnSort.setComparator(datecol, new Comparator<Service>() {

			@Override
			public int compare(Service e1, Service e2) {
				if (e1 != null && e2 != null) {
					if (e1.getServiceDate() != null
							&& e2.getServiceDate() != null) {
						return e1.getServiceDate().compareTo(
								e2.getServiceDate());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addServiceTime() {

		List<Service> list = getDataprovider().getList();
		columnSort = new ListHandler<Service>(list);
		columnSort.setComparator(serviceTime, new Comparator<Service>() {

			@Override
			public int compare(Service e1, Service e2) {
				if (e1 != null && e2 != null) {
					if (e1.getServiceTime() != null
							&& e2.getServiceTime() != null) {
						return e1.getServiceTime().compareTo(
								e2.getServiceTime());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addServiceDay() {

		List<Service> list = getDataprovider().getList();
		columnSort = new ListHandler<Service>(list);
		columnSort.setComparator(serviceDay, new Comparator<Service>() {

			@Override
			public int compare(Service e1, Service e2) {
				if (e1 != null && e2 != null) {
					if (e1.getServiceDay() != null
							&& e2.getServiceDay() != null) {
						return e1.getServiceDateDay().compareTo(
								e2.getServiceDateDay());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);

	}

	private void addServiceBranch() {

		List<Service> list = getDataprovider().getList();
		columnSort = new ListHandler<Service>(list);
		columnSort.setComparator(serviceBranch, new Comparator<Service>() {

			@Override
			public int compare(Service e1, Service e2) {
				if (e1 != null && e2 != null) {
					if (e1.getServiceBranch() != null
							&& e2.getServiceBranch() != null) {
						return e1.getServiceBranch().compareTo(
								e2.getServiceBranch());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);

	}

	private void addTeamSorting() {
		List<Service> list = getDataprovider().getList();
		columnSort = new ListHandler<Service>(list);
		columnSort.setComparator(team, new Comparator<Service>() {

			@Override
			public int compare(Service e1, Service e2) {
				if (e1 != null && e2 != null) {
					if (e1.getTeam() != null && e2.getTeam() != null) {
						return e1.getTeam().compareTo(e2.getTeam());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);

	}

	private void addLocalitySorting() {
		List<Service> list = getDataprovider().getList();
		columnSort = new ListHandler<Service>(list);
		columnSort.setComparator(locality, new Comparator<Service>() {

			@Override
			public int compare(Service e1, Service e2) {
				if (e1 != null && e2 != null) {
					if (e1.getLocality() != null && e2.getLocality() != null) {
						return e1.getLocality().compareTo(e2.getLocality());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);

	}

	public void addReasonForChangeSorting() {
		List<Service> list = getDataprovider().getList();
		columnSort = new ListHandler<Service>(list);
		Comparator<Service> quote = new Comparator<Service>() {
			@Override
			public int compare(Service e1, Service e2) {
				if (e1 != null && e2 != null) {
					if (e1.getReasonForChange() != null
							&& e2.getReasonForChange() != null)
						return e1.getReasonForChange().compareTo(
								e2.getReasonForChange());
					return 0;
				} else
					return 0;
			}
		};

		columnSort.setComparator(reasonForChange, quote);
		columnSort.setComparator(editReasonForChange, quote);
		table.addColumnSortHandler(columnSort);
	}

	public void addBranchSorting() {
		List<Service> list = getDataprovider().getList();
		columnSort = new ListHandler<Service>(list);
		columnSort.setComparator(branch, new Comparator<Service>() {

			@Override
			public int compare(Service e1, Service e2) {
				if (e1 != null && e2 != null) {
					if (e1.getBranch() != null && e2.getBranch() != null) {
						return e1.getBranch().compareTo(e2.getBranch());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnserviceSrNo() {

		serviceSrNo = new TextColumn<Service>() {

			@Override
			public String getValue(Service object) {
				return object.getServiceSerialNo() + "";
			}
		};
		if(complainTatFlag){
			table.addColumn(serviceSrNo, "Service No.");
		}else{
			table.addColumn(serviceSrNo, "Sr No");
		}
		table.setColumnWidth(serviceSrNo, 80, Unit.PX);
		serviceSrNo.setSortable(true);
	}

	public void addSerialNoSorting() {
		List<Service> list = getDataprovider().getList();
		columnSort = new ListHandler<Service>(list);
		columnSort.setComparator(serviceSrNo, new Comparator<Service>() {

			@Override
			public int compare(Service e1, Service e2) {
				if (e1 != null && e2 != null) {
					if (e1.getServiceSerialNo() == e2.getServiceSerialNo()) {
						return 0;
					}
					if (e1.getServiceSerialNo() > e2.getServiceSerialNo()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub

	}

	@Override
	public void setEnable(boolean state) {
		/*
		 * for(int i=table.getColumnCount()-1;i>=0;i--) table.removeColumn(i);
		 * ScreeenState screenstate=AppMemory.getAppMemory().currentState;
		 * 
		 * 
		 * if((state==true&& screenstate==ScreeenState.EDIT)) {
		 * addColumnserviceId(); addColumnContractId(); addColumnName();
		 * addColumnPhone(); addColumnnProduct(); addEditColumnDate();
		 * table.addColumn(editserviceEng,"Service Engineer");
		 * table.setColumnWidth(editserviceEng,150,Unit.PX);
		 * addFieldUpdaterEmployee(); addColumnStatus();
		 * addFieldUpdaterStatus(); table.addColumn(editBranchColumn,"Branch");
		 * table.setColumnWidth(editBranchColumn,120,Unit.PX);
		 * addEditColumnReasonForchange(); addFiledUpdaterReasonForchange();
		 * addColumnSorting();
		 * 
		 * 
		 * }
		 * 
		 * else if((state==true&&(screenstate==screenstate.NEW))) {
		 * addColumnserviceId(); addColumnContractId(); addColumnName();
		 * addColumnPhone(); addColumnnProduct(); addColumnDate();
		 * addColumnServiceEng(); addColumnStatus(); addColumnBranch();
		 * addColumnReasonForChange(); addColumnSorting();
		 * 
		 * }
		 * 
		 * else if((state==false)&&(screenstate==screenstate.VIEW)) {
		 * 
		 * addColumnContractId(); addColumnName(); addColumnPhone();
		 * addColumnnProduct(); addColumnDate(); addColumnServiceEng();
		 * addColumnStatus(); addColumnBranch(); addColumnReasonForChange();
		 * addColumnSorting();
		 * 
		 * 
		 * }
		 */

	}

	@Override
	public void applyStyle() {
		
	}

	// /////////////////////////////////////////////////////////////

	public void createColumnViewProductColumn() {
		ButtonCell btnCell = new ButtonCell();
		viewButtonColumn = new Column<Service, String>(btnCell) {
			@Override
			public String getValue(Service object) {
				return "Plan";
			}
		};
		table.addColumn(viewButtonColumn, "");
		table.setColumnWidth(viewButtonColumn, 70, Unit.PX);

	}

	public void updateColumnViewProductColumn() {
		viewButtonColumn.setFieldUpdater(new FieldUpdater<Service, String>() {
			@Override
			public void update(int index, Service object, String value) {
				ServicePresenter.viewDocumentFlag = false;

				if(checkStatus(object)){
					/** Description
					 * When we maintain technician warehouse sepeately
					 */
					if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "TechnicianwiseMaterialPopup")){
						getProjectAndBOMDetails(object, technicianServiceSchedulePopup, false);
						technicianServiceSchedulePopup.getPopup().getElement()
						.addClassName("servicepopup");
						technicianServiceSchedulePopup.getPopup().center();
					}else{
						getProjectDetails(object , schedule , false);
					}
					
				}
				serviceObject = object;
//				schedule.showPopUp();
//				System.out.println("View product button click...");
//				schedule.clear();
//				schedule.lblUpdate.getElement().setId("addbutton");
//				schedule.getHorizontal().add(schedule.lblUpdate);
//				schedule.getDateBox().setValue(object.getServiceDate());
//
//				if (object.getTeam() != null) {
//					schedule.getOlbTeam() .setValue(object.getTeam());
//				}
//
//				if (!object.getServiceTime().equals("")
//						&& !object.getServiceTime().equals("Flexible")) {
//
//					String[] time = object.getServiceTime().split(":");
//					System.out.println(" 0 " + time[0]);
//					System.out.println(" 1 " + time[1]);
//					String hrs = time[0];
//					String min;
//					String ampm;
//					if (time[1].contains("PM")) {
//						min = time[1].replace("PM", "");
//						ampm = "PM";
//					} else {
//						min = time[1].replace("AM", "");
//						ampm = "AM";
//					}
//					System.out.println(" 2 " + hrs + " " + min + " " + ampm);
//
//					int count = schedule.getP_servicehours().getItemCount();
//					String item = hrs.toString();
//					for (int i = 0; i < count; i++) {
//						if (item.trim().equals(
//								schedule.getP_servicehours().getItemText(i)
//										.trim())) {
//							schedule.getP_servicehours().setSelectedIndex(i);
//							break;
//						}
//					}
//
//					int count1 = schedule.getP_servicemin().getItemCount();
//					String item1 = min.toString();
//					for (int i = 0; i < count1; i++) {
//						if (item1.trim().equals(
//								schedule.getP_servicemin().getItemText(i)
//										.trim())) {
//							schedule.getP_servicemin().setSelectedIndex(i);
//							break;
//						}
//					}
//
//					int count2 = schedule.getP_ampm().getItemCount();
//					String item2 = ampm.toString();
//					for (int i = 0; i < count2; i++) {
//						if (item2.trim().equals(
//								schedule.getP_ampm().getItemText(i).trim())) {
//							schedule.getP_ampm().setSelectedIndex(i);
//							break;
//						}
//					}
//
//				}
//
//				getTechnicianList(object.getCount(), object.getContractCount(),
//						object.getCompanyId());
//
//				// panel=new PopupPanel(true);
//				// panel.getElement().addClassName("popupServicecontent");
//				// panel.add(schedule);
//				// panel.show();
//				// panel.center();
//
//				/**
//				 * nidhi 27-10-2017
//				 */
				schedule.getPopup().getElement()
						.addClassName("popupServicecontent");

				rowIndex = index;
			}
		});
	}

	/** 05-10-2017 sagar sore [adding number range column] **/
	public void addColumnNumberRange() {
		numberRangeColumn = new TextColumn<Service>() {

			@Override
			public String getValue(Service object) {
				return object.getNumberRange();
			}
		};

		table.addColumn(numberRangeColumn, "Number Range");
		table.setColumnWidth(numberRangeColumn, 120, Unit.PX);
		numberRangeColumn.setSortable(true);
	}

	protected void addSortinggetNumberRange() {
		List<Service> list = getDataprovider().getList();
		columnSort = new ListHandler<Service>(list);
		columnSort.setComparator(numberRangeColumn, new Comparator<Service>() {
			@Override
			public int compare(Service e1, Service e2) {
				if (e1 != null && e2 != null) {

					if (e1.getNumberRange() == null
							&& e2.getNumberRange() == null) {
						return 0;
					}
					if (e1.getNumberRange() == null)
						return 1;
					if (e2.getNumberRange() == null)
						return -1;
					return e1.getNumberRange().compareTo(e2.getNumberRange());

				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	/** end **/
 
	@Override
	public void onClick(ClickEvent event) {
		
		/*
		 * Date:17/08/2018
		 * Added by Ashwwini
		 * Des:To hide contact details after clicking on ok button
		 */
		
		if(event.getSource() == contractpopup.getLblOk()){
			contractpopup.hidePopUp();
			return;
		}
		
		/*
		 * End by Ashwini
		 */
		
		if (event.getSource() == technicianServiceSchedulePopup.getBtnEmployee()) {
			
			addEmployee(technicianServiceSchedulePopup);
			return;
		}
		
		/**
		 * @author Anil
		 * @since 22-01-2022
		 * Moved compeltion logic in method so that i can reuse it
		 */
		if (event.getSource() == serDatePopup.getBtnOne()) {

			if(serDatePopup.getDbQuantity().getValue()!=null){
				serviceObject.setQuantity(serDatePopup.getDbQuantity().getValue());
				
				/**@Sheetal:07-02-2022
				 * Des : Adding validation for quantity should be less than number of premsies 
				 *       in contract,requirement by UDS water
				 **/
				
				
				if(serviceObject.getPremises()!=null&&!serviceObject.getPremises().equals("")&&!serviceObject.getPremises().equals("0")){ 
					try{
					if(serviceObject.getPremises().matches("[0-9]+")){ //Ashwini Patil Date:19-03-2022 Desription: below code will get executed  for numeric premises only
					String str=serviceObject.getPremises().trim();
					double premise= Double.parseDouble(str);
					double quantity = serDatePopup.getDbQuantity().getValue();
					
						if(quantity>premise){
							alert.alert("Quantity should be less than number of premises in contract");
							Console.log("premise" +premise);
							Console.log("Quantity" +quantity);
							return;
						}
					}
					}catch(Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				  
					}
				
				/**end**/
			}
			reactOnMarkComplete(serDatePopup.getServiceDate().getValue());
		}
		if (event.getSource() == serDatePopup.getBtnTwo()) {
			datepopupPanel.hide();
		}
		if (event.getSource() == datepopup.getBtnOne()) {
			reactOnMarkComplete(datepopup.getServiceDate().getValue());
		}
		if (event.getSource() == datepopup.getBtnTwo()) {
			datepopupPanel.hide();
		}
		
		if (event.getSource() == schedule.getBtnEmployee()) {
			//if (schedule.olbEmployee.getValue() != null)
				//reactOnAddEmployee();
				/** date 27.02.2018 added by komal for customer service list new changes **/
				reactOnAddEmployeeOrTeam();
		}
		if(event.getSource().equals(reschedule.getBtnReschedule())){

			boolean val=validateReschedule();
			if(val==true)
			{
				ModificationHistory history=new ModificationHistory();
				history.oldServiceDate=serviceObject.getServiceDate();
				history.resheduleDate=reschedule.getDateBox().getValue();
				
				String serviceHrs=reschedule.getP_servicehours().getItemText(reschedule.getP_servicehours().getSelectedIndex());
				String serviceMin=reschedule.getP_servicemin().getItemText(reschedule.getP_servicemin().getSelectedIndex());
				String serviceAmPm=reschedule.getP_ampm().getItemText(reschedule.getP_ampm().getSelectedIndex());
				String calculatedServiceTime=serviceHrs+":"+serviceMin+""+serviceAmPm;
				/**Date 18-4-2020 by Amol if service time is not selected then it will Show Flexible**/
				if(serviceHrs.equals("--") || serviceMin.equals("--")){
					Console.log("Inside Service schedule time blank");
					history.resheduleTime="Flexible";
				}else{
					Console.log("Inside Service schedule time not blank");
					history.resheduleTime=calculatedServiceTime;	
				}
				
				Console.log("Service schedule time value "+history.resheduleTime);
				history.resheduleBranch=reschedule.getP_seviceBranch().getItemText(reschedule.getP_seviceBranch().getSelectedIndex());				
				System.out.println("service branch == "+reschedule.getP_seviceBranch().getItemText(reschedule.getP_seviceBranch().getSelectedIndex()));				
				history.reason=reschedule.getTextArea().getText();
				history.userName=LoginPresenter.loggedInUser;
				history.systemDate=new Date();
				Date reschDate=reschedule.getDateBox().getValue();
				serviceObject.setServiceTime(history.resheduleTime);
				serviceObject.getListHistory().add(history);
				serviceObject.setServiceDate(reschDate);
				/** Date :- 12-03-2020 by amol for while rescheduling updating service Day ***/ 
				serviceObject.setServiceDay(ContractForm.serviceDay(reschDate));
				
				String serviceBranch=reschedule.getP_seviceBranch().getItemText(reschedule.getP_seviceBranch().getSelectedIndex());
				serviceObject.setServiceBranch(serviceBranch);				
				System.out.println("service branch   === "+serviceBranch);			
				saveDetails("Service Reschedule !","Failed To Reschedule !",Service.SERVICESTATUSRESCHEDULE, history,reschDate,calculatedServiceTime,serviceBranch);
			}
		
		}
		if(event.getSource().equals(reschedule.getBtnCancel())){
			panel.hide();
		}
		if(event.getSource() == schedule.getMarkcompletepopup().getBtnOk()){
			
//			if(schedule.getMarkcompletepopup().getServiceCompletionDate().getValue() == null){
//				alert.alert("Please add Completetion Date");
//			}else{
//				ReactOnServiceCompletionPopUp(event);
//			}
			
			updateServices(true,event);
			
		}
		if(event.getSource() == schedule.getMarkcompletepopup().getBtnCancel()){
//			panel.hide();
//			schedule.hidePopUp();
			/**
			 *  17-04-2018
			 *   for pop up
			 */
			schedule.getPanel().hide();
		}
		/*** date 18.4.2018 added by komal for address popup **/
		if(event.getSource().equals(addressPopup.getLblCancel())){
			addressPopup.hidePopUp();
		}
		/** date 06.04.2018 added by komal for fumigation **/
		if(event.getSource() == conditionPopup.getBtnOne()){
			if(serviceObject.getProduct().getProductName().equalsIgnoreCase("Methyle Bromide")){
			popupPanel.hide();
			Fumigation fumigation = new Fumigation();
			if (serviceObject.getContractCount() != -1) {
				fumigation.setContractID(serviceObject.getContractCount());
			}
			fumigation.setServiceID(serviceObject.getCount());
			if (serviceObject.getContractStartDate() != null) {
				fumigation.setContractStartdate(serviceObject.getContractStartDate());
			}
			if (serviceObject.getContractEndDate() != null) {
				fumigation.setContractEnddate(serviceObject.getContractEndDate());
			}
			if (serviceObject.getServiceDate() != null) {
				fumigation.setServiceDate(serviceObject.getServiceDate());
			}
			if(serviceObject.getPersonInfo()!=null){
				fumigation.setcInfo(serviceObject.getPersonInfo());
			}
			fumigation.setNameoffumigation("Methyle Bromide");
			/**
			 * Updated By: Viraj
			 * Date: 17-04-2019
			 * Description: changed quantity from double to string in fumigation to handle this change below code is writen
			 */
			String quantity="";
			if(serviceObject.getQuantity() != 0) {
				quantity = String.valueOf(serviceObject.getQuantity());
			}
			fumigation.setQuantity(quantity);
			/** Ends **/
			FumigationPresenter.setObject(this);
			//fumigation.setBranch(serviceObject.getBranch());
			AppMemory.getAppMemory().currentScreen = Screen.FUMIGATION;
			genralInvoicePopup = new GeneralViewDocumentPopup();
         //   genralInvoicePopup.addStyleName("conditionPopup");
			genralInvoicePopup.setModel(AppConstants.FUMIGATION,fumigation);
			generalPanel = new PopupPanel(true);
			generalPanel.add(genralInvoicePopup);
			generalPanel.show();
			generalPanel.center();
			}
			else{
				alert.alert("Product in service is Aluminium Phosphide.");
			}

		}
		if(event.getSource() == technicianServiceSchedulePopup.getLblOk()){
			technicianServiceSchedulePopup.getLblOk().setVisible(false);
			reactOnOKButton(technicianServiceSchedulePopup);
			return;
		}
		else if (event.getSource() == technicianServiceSchedulePopup.getLblCancel()) {
			technicianServiceSchedulePopup.hidePopUp();
			return;
			//schedule.getPopup().hide();
		}
		if(event.getSource() == conditionPopup.getBtnTwo()){
			if(serviceObject.getProduct().getProductName().equalsIgnoreCase("Aluminium Phosphide")){
			popupPanel.hide();
			Fumigation fumigation = new Fumigation();
			if (serviceObject.getContractCount() != -1) {
				fumigation.setContractID(serviceObject.getContractCount());
			}
			fumigation.setServiceID(serviceObject.getCount());
			if (serviceObject.getContractStartDate() != null) {
				fumigation.setContractStartdate(serviceObject.getContractStartDate());
			}
			if (serviceObject.getContractEndDate() != null) {
				fumigation.setContractEnddate(serviceObject.getContractEndDate());
			}
			if (serviceObject.getServiceDate() != null) {
				fumigation.setServiceDate(serviceObject.getServiceDate());
			}
			if(serviceObject.getPersonInfo()!=null){
				fumigation.setcInfo(serviceObject.getPersonInfo());
			}
			fumigation.setNameoffumigation("Aluminium Phosphide");
			/**
			 * Updated By: Viraj
			 * Date: 17-04-2019
			 * Description: changed quantity from double to string in fumigation to handle this change below code is writen
			 */
			String quantity="";
			if(serviceObject.getQuantity() != 0) {
				quantity = String.valueOf(serviceObject.getQuantity());
			}
			fumigation.setQuantity(quantity);
			/** Ends **/
			//fumigation.setBranch(serviceObject.getBranch());
			AppMemory.getAppMemory().currentScreen = Screen.FUMIGATIONALP;
			genralInvoicePopup = new GeneralViewDocumentPopup();
			FumigationALPPresenter.setObject(this);
         //   genralInvoicePopup.addStyleName("conditionPopup");
			genralInvoicePopup.setModel(AppConstants.FUMIGTAIONALP,fumigation);
			generalPanel = new PopupPanel();
			generalPanel.add(genralInvoicePopup);
			generalPanel.show();
			generalPanel.center();
			}else{
				alert.alert("Product in service is Methyle Bromide");
			}
		}
		if(event.getSource() == conditionPopup.getBtnThree()){
			popupPanel.hide();
			Fumigation fumigation = new Fumigation();
			if (serviceObject.getContractCount() != -1) {
				fumigation.setContractID(serviceObject.getContractCount());
			}
			fumigation.setServiceID(serviceObject.getCount());
			if (serviceObject.getContractStartDate() != null) {
				fumigation.setContractStartdate(serviceObject.getContractStartDate());
			}
			if (serviceObject.getContractEndDate() != null) {
				fumigation.setContractEnddate(serviceObject.getContractEndDate());
			}
			if (serviceObject.getServiceDate() != null) {
				fumigation.setServiceDate(serviceObject.getServiceDate());
			}
			if(serviceObject.getPersonInfo()!=null){
				fumigation.setcInfo(serviceObject.getPersonInfo());
			}
			fumigation.setNameoffumigation("Methyle Bromide");
			/**
			 * Updated By: Viraj
			 * Date: 17-04-2019
			 * Description: changed quantity from double to string in fumigation to handle this change below code is writen
			 */
			String quantity="";
			if(serviceObject.getQuantity() != 0) {
				quantity = String.valueOf(serviceObject.getQuantity());
			}
			fumigation.setQuantity(quantity);
			/** Ends **/
		//	fumigation.setBranch(serviceObject.getBranch());
			AppMemory.getAppMemory().currentScreen = Screen.FUMIGATIONAUSTRALIA;
			genralInvoicePopup = new GeneralViewDocumentPopup();
			FumigationAustraliaPresenter.setObject(this);
			//genralInvoicePopup.addStyleName("conditionPopup");
			genralInvoicePopup.setModel(AppConstants.FUMIGATIONAUS,fumigation);
			generalPanel = new PopupPanel();
			generalPanel.add(genralInvoicePopup);
			generalPanel.show();
			generalPanel.center();

			
		}
		/**
		 * end komal
		 */
		

			if(event.getSource()  ==  schedule.getLblOk()){
//				schedule.getLblOk().setText("Processing");
//				final ArrayList<ProductGroupList> materiallist = new ArrayList<ProductGroupList>();
//				materiallist.addAll(schedule.getMaterialProductTable().getDataprovider().getList());
//				if(materiallist.size()>0){
//					Company comp = new Company();
//					commonserviceasync.validateMaterialQuantity(comp.getCompanyId(), materiallist, new AsyncCallback<String>() {
//						@Override
//						public void onFailure(Throwable caught) {
//							
//						}
//
//						@Override
//						public void onSuccess(String result) {
//							// TODO Auto-generated method stub
//							if(!result.equals("")){
//								GWTCAlert alert = new GWTCAlert();
//								alert.alert(result);
//								schedule.getLblOk().setText("OK");
//							}
//							else{
//								processPlanData();
//							}
//						}
//					});
//				}
//				else{
//					processPlanData();
//				}
			
				updateServices(false,event);
			}
			else if (event.getSource() == schedule.getLblCancel()) {
				schedule.hidePopUp();
				//schedule.getPopup().hide();
			}
			 if (event.getSource() == technicianServiceCompletePopup.getLblCancel()) {
				 technicianServiceCompletePopup.hidePopUp();
					return;
					//schedule.getPopup().hide();
				}
			 if(event.getSource() == technicianServiceCompletePopup.getMarkcompletepopup().getBtnOk()){
					
					if(technicianServiceCompletePopup.getMarkcompletepopup().getServiceCompletionDate().getValue() == null){
						alert.alert("Please add Completetion Date");
					}else{
						reactOnServiceCompletionPopUp(event , technicianServiceCompletePopup);
					}
					
				}
				if(event.getSource() == technicianServiceCompletePopup.getMarkcompletepopup().getBtnCancel()){

				//	technicianServiceCompletePopup.getPanel().hide();
					popupPanel.hide();
				}
			 

		
	}

	private void saveService(Service serObj) {
		service.save(serObj, new AsyncCallback<ReturnFromServer>() {
			@Override
			public void onFailure(Throwable caught) {
				glassPanel.hide();
				final GWTCAlert alert = new GWTCAlert();
				alert.alert("An Unexpected error occurred!");
			}

			@Override
			public void onSuccess(ReturnFromServer result) {
				glassPanel.hide();
				final GWTCAlert alert = new GWTCAlert();
				alert.alert("Saved successfully!");
				schedule.getPopup().hide();
			}
		});

	}

//	private void reactOnAddEmployee() {
//		if (schedule.getOlbEmployee().getSelectedIndex() == 0) {
//			form.showDialogMessage("Please select employee!");
//		}
//
//		if (schedule.getOlbEmployee().getSelectedIndex() != 0) {
//			List<EmployeeInfo> list = schedule.getEmptable().getDataprovider()
//					.getList();
//
//			if (list.size() != 0) {
//				if (validateEmployees() == true) {
//					addToEmployeeTable();
//				}
//			} else {
//				addToEmployeeTable();
//			}
//			schedule.getOlbEmployee().setSelectedIndex(0);
//		}
//	}
	/** date 27.02.2018 added by komal for customer service list new changes **/
	private void reactOnAddEmployeeOrTeam(){
		if(ValidateTechnician()){
			
			Employee empEntity=schedule.getOlbEmployee().getSelectedItem();
			logger.log(Level.SEVERE , "Employee " + schedule.getOlbEmployee().getSelectedIndex());

			if(schedule.getOlbEmployee().getSelectedIndex()!=0){
				/** date 15/11/2017 added by komal to add employee only once **/
			    List<EmployeeInfo> empList = schedule.getEmptable().getDataprovider().getList();
			    for(EmployeeInfo empInfo : empList){
			    	if(empEntity.getCount() == empInfo.getEmpCount()){
			    		alert.alert("Technician is already present in Table..Please select different one");
			    		return;
			    	}
			    }
		  		EmployeeInfo empInfoEntity = new EmployeeInfo();
		  		
		  		empInfoEntity.setEmpCount(empEntity.getCount());
		  		empInfoEntity.setFullName(empEntity.getFullName());
		      	empInfoEntity.setCellNumber(empEntity.getCellNumber1());
		      	empInfoEntity.setDesignation(empEntity.getDesignation());
		      	empInfoEntity.setDepartment(empEntity.getDepartMent());
		      	empInfoEntity.setEmployeeType(empEntity.getEmployeeType());
		      	empInfoEntity.setEmployeerole(empEntity.getRoleName());
		      	empInfoEntity.setBranch(empEntity.getBranchName());
		      	empInfoEntity.setCountry(empEntity.getCountry());
		      	
		      	schedule.getEmptable().getDataprovider().getList().add(empInfoEntity);
			}
			else if(schedule.getOlbTeam().getSelectedIndex()!=0){
				ArrayList<EmployeeInfo> list = new ArrayList<EmployeeInfo>();
				TeamManagement teamManangementEntity = schedule.getOlbTeam().getSelectedItem();
				logger.log(Level.SEVERE , "Team management " + teamManangementEntity.getTeamList().size());
				List<Employee> empList = schedule.getOlbEmployee().getItems();
				for(int i=0;i<teamManangementEntity.getTeamList().size();i++){
					
					EmployeeInfo empInfoEntity = new EmployeeInfo();
			  		empInfoEntity.setFullName(teamManangementEntity.getTeamList().get(i).getEmployeeName());
			        empInfoEntity.setCellNumber(teamManangementEntity.getTeamList().get(i).getPhoneNumber());
			        empInfoEntity.setEmpCount(teamManangementEntity.getTeamList().get(i).getCustomerCount());
			        if(teamManangementEntity.getTeamList().get(i).getEmpDesignation()!=null)
			        empInfoEntity.setDesignation(teamManangementEntity.getTeamList().get(i).getEmpDesignation());
			  		logger.log(Level.SEVERE , "employee "+  teamManangementEntity.getTeamList().get(i).getEmployeeName());
			      	list.add(empInfoEntity);
				}
				
				schedule.getEmptable().getDataprovider().setList(list);
				
			}
		}

	}

//	public boolean validateEmployees() {
//		List<EmployeeInfo> empList = schedule.getEmptable().getDataprovider()
//				.getList();
//Emp
//		if (empList.size() != 0) {
//			for (int i = 0; i < empList.size(); i++) {
//				if (empList.get(i).getFullName().trim()
//						.equals(schedule.getOlbEmployee().getValue())) {
//					alert.alert("Employee already exists!");
//					return false;
//				}
//				if(empList.get(i).getFullName().trim().equals(schedule.olbTeam.get))
//			}
//		}
//		return true;
//	}
	private boolean ValidateTechnician() {

		if(schedule.getOlbEmployee().getSelectedIndex()==0  && schedule.getOlbTeam().getSelectedIndex()==0){
			alert.alert("Please Select either Team or Technician");
			return false;
		}
		else if(schedule.getOlbEmployee().getSelectedIndex()!=0 && schedule.getOlbTeam().getSelectedIndex()!=0){
			alert.alert("Please Select either Team or Technician");
			return false;
		}	
		return true;
	}


	protected void addToEmployeeTable() {

		Employee empEntity = schedule.getOlbEmployee().getSelectedItem();
		EmployeeInfo empInfoEntity = new EmployeeInfo();

		empInfoEntity.setEmpCount(empEntity.getCount());
		empInfoEntity.setFullName(empEntity.getFullName());
		empInfoEntity.setCellNumber(empEntity.getCellNumber1());
		empInfoEntity.setDesignation(empEntity.getDesignation());
		empInfoEntity.setDepartment(empEntity.getDepartMent());
		empInfoEntity.setEmployeeType(empEntity.getEmployeeType());
		empInfoEntity.setEmployeerole(empEntity.getRoleName());
		empInfoEntity.setBranch(empEntity.getBranchName());
		empInfoEntity.setCountry(empEntity.getCountry());

		schedule.getEmptable().getDataprovider().getList().add(empInfoEntity);

	}

	void getTechnicianList(int serviceCount, int contractCount, long companyId) {
		try {
			projectAsync.projectTechnicianList(companyId, contractCount,
					serviceCount, ServiceProject.CREATED,
					new AsyncCallback<ArrayList<EmployeeInfo>>() {

						@Override
						public void onSuccess(ArrayList<EmployeeInfo> result) {
							// TODO Auto-generated method stub
							if (result != null) {
								schedule.getEmptable().getDataprovider()
										.setList(result);
							}
						}

						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub

						}
					});
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			Console.log("Error -- getTechnicianList " + e.getMessage());
		}
	}

	//Ashwini Patil
	private void addColumnPremises(){
		premises = new TextColumn<Service>() {
			
			@Override
			public String getValue(Service object) {
				if (object.getPremises() != null) {
					return object.getPremises();
				} else {
					return "";
				}
			}
		};
		table.addColumn(premises, "Premises");
		table.setColumnWidth(premises, 120, Unit.PX);
		premises.setSortable(true);
	}
	private void addPremisesSorting(){
		List<Service> list = getDataprovider().getList();
		columnSort = new ListHandler<Service>(list);
		columnSort.setComparator(premises, new Comparator<Service>() {

			@Override
			public int compare(Service e1, Service e2) {
				if (e1 != null && e2 != null) {
					if (e1.getPremises() != null && e2.getPremises() != null) {
						return e1.getPremises().compareTo(e2.getPremises());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	/**
	 * nidhi
	 * Date : 4-12-2017
	 * 
	 */
	private void addColumnServiceType(){
		serviceType = new TextColumn<Service>() {
			
			@Override
			public String getValue(Service object) {
				if (object.getServiceType() != null) {
					return object.getServiceType();
				} else {
					return "";
				}
			}
		};
		table.addColumn(serviceType, "Servicetype");
		table.setColumnWidth(serviceType, 120, Unit.PX);
		serviceType.setSortable(true);
	}
	
	/**
	 *  nidhi
	 *  Date : 4-12-2017
	 *  ServiceType sorting method
	 */
	private void addServiceTypeSorting(){
		List<Service> list = getDataprovider().getList();
		columnSort = new ListHandler<Service>(list);
		columnSort.setComparator(serviceType, new Comparator<Service>() {

			@Override
			public int compare(Service e1, Service e2) {
				if (e1 != null && e2 != null) {
					if (e1.getServiceType() != null && e2.getServiceType() != null) {
						return e1.getServiceType().compareTo(e2.getServiceType());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	/**
	 *  end
	 */
	/** date 27.02.2018 added by komal for customer service list new changes **/
	public void getProjectDetails(final Service model , final ServiceSchedulePopup schedule , final boolean flag) {

		final int contractId = model.getContractCount();
		
		 MyQuerry querry = new MyQuerry();
		 Vector<Filter> filterVec = new Vector<Filter>();
		 
		 Filter filter =null;
		 filter=new Filter();
		 filter.setIntValue(model.getCount());
		 filter.setQuerryString("serviceId");
		 filterVec.add(filter);
		 System.out.println("proj =="+model.getProjectId());
		 int projectId = model.getProjectId();
		 if(projectId!=0){
			 System.out.println("PROJECTttt");
			 filter=new Filter();
			 filter.setIntValue(projectId);
			 filter.setQuerryString("count");
			 filterVec.add(filter);
		 }
		 
		 querry.setFilters(filterVec);
		 querry.setQuerryObject(new ServiceProject());
		 
		 service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				System.out.println("result ="+result.size());
				ServiceSchedulePopup.materialValue = "";
				schedule.showPopUp();
				schedule.clear();
				schedule.lblUpdate.getElement().setId("addbutton");
				schedule.getHorizontal().add(schedule.lblUpdate);
				if(!flag){
					loadInvoiceDropDown(contractId);
				}	
				schedule.getLblOk().setVisible(true);
				schedule.getEmptable().getDataprovider().getList().clear();
				schedule.getToolTable().getDataprovider().getList().clear();
				schedule.getMaterialProductTable().getDataprovider().getList().clear();
				schedule.getDbQuantity().setValue(0d);
				if(model.getStatus().equals("Completed")){
					schedule.setEnable(false);
					schedule.getMaterialProductTable().setEnable(false);
					schedule.getEmptable().setEnable(false);
					schedule.getToolTable().setEnable(false);
					schedule.getLblOk().setVisible(false);
				}else{
					/** date 20.07.2019 added by komal to disallow any changes in service after service has been scheduled**/

					//Ashwini Patil Date:1-03-2023
//					if(model.isServiceScheduled()){
//						Console.log("servicetable model.isServiceScheduled() true");
						//Ashwini Patil Date:26-12-2022 commented below code as we want to give edit access even it service schedule status is true
//						schedule.setEnable(false);
//						schedule.getMaterialProductTable().setEnable(false);
//						schedule.getEmptable().setEnable(false);
//						schedule.getToolTable().setEnable(false);
//
//						schedule.getLblOk().setVisible(false);
						
//					}else{
						schedule.setEnable(true);
						//Ashwini Patil Date:26-12-2022 added below code
						schedule.getMaterialProductTable().setEnable(true);
						schedule.getEmptable().setEnable(true);
						schedule.getToolTable().setEnable(true);
						schedule.getLblOk().setVisible(true);
//					}	
				}
				if(!(model.getEmployee().equals(""))){
				schedule.getOlbEmployee().setValue(model.getEmployee());
				
				/**
				 * Date 09-08-2018 By Vijay
				 * Bug :- Bug - updated code with employee information from global list
				 * old code commneted
				 */
				for(Employee empEntity : LoginPresenter.globalEmployee){
					if(empEntity.getFullname().equals(model.getEmployee())){
						
						EmployeeInfo empInfoEntity = new EmployeeInfo();

						empInfoEntity.setEmpCount(empEntity.getCount());
				  		empInfoEntity.setFullName(empEntity.getFullName());
				      	empInfoEntity.setCellNumber(empEntity.getCellNumber1());
				      	empInfoEntity.setDesignation(empEntity.getDesignation());
				      	empInfoEntity.setDepartment(empEntity.getDepartMent());
				      	empInfoEntity.setEmployeeType(empEntity.getEmployeeType());
				      	empInfoEntity.setEmployeerole(empEntity.getRoleName());
				      	empInfoEntity.setBranch(empEntity.getBranchName());
				      	empInfoEntity.setCountry(empEntity.getCountry());		
				      	
						schedule.getEmptable().getDataprovider().getList().add(empInfoEntity);

					}
				}
				/**
				 * ends here
				 */
//				Employee empEntity = schedule.getOlbEmployee().getSelectedItem();
//				EmployeeInfo empInfoEntity = new EmployeeInfo();
//		  		
//		  		empInfoEntity.setEmpCount(empEntity.getCount());
//		  		empInfoEntity.setFullName(empEntity.getFullName());
//		      	empInfoEntity.setCellNumber(empEntity.getCellNumber1());
//		      	empInfoEntity.setDesignation(empEntity.getDesignation());
//		      	empInfoEntity.setDepartment(empEntity.getDepartMent());
//		      	empInfoEntity.setEmployeeType(empEntity.getEmployeeType());
//		      	empInfoEntity.setEmployeerole(empEntity.getRoleName());
//		      	empInfoEntity.setBranch(empEntity.getBranchName());
//		      	empInfoEntity.setCountry(empEntity.getCountry());				
//				schedule.getEmptable().getDataprovider().getList().add(empInfoEntity);
				schedule.getOlbEmployee().setSelectedIndex(0);
				}
				for(SuperModel model :result){
					ServiceProject project = (ServiceProject) model;//komal changed model to smodel
					schedule.getEmptable().getDataprovider().setList(project.getTechnicians());
					schedule.getToolTable().getDataprovider().setList(project.getTooltable());
					schedule.getMaterialProductTable().getDataprovider().setList(project.getProdDetailsList());
					
					CustomerServiceMaterialTable.projectId=project.getCount();
				}
				schedule.getDateBox().setValue(model.getServiceDate());
				//setValue(model.getServiceTime(),planPopUp.servicetime);
				
				setpopupServiceTime(model.getServiceTime() , schedule ,false);			
				schedule.setService(model);
				//schedule.showPopUp();
			}
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				alert.alert("Unexpected error occurred");
			}
		});
		 
	}
	
	
	private void setpopUpInvoiceIdDate(int invoiceId, Date invoiceDate) {
		// TODO Auto-generated method stub
		int invoiceIdcount  = schedule.getOlbInvoiceId().getItemCount();
		String invoiceid = invoiceId+"";
		if(invoiceId!=0){
			for(int i=0;i<invoiceIdcount;i++){
				if(invoiceid.trim().equals(schedule.getOlbInvoiceId().getItemText(i))){
					schedule.getOlbInvoiceId().setSelectedIndex(i);
					break;
				}
			}
		}
		
		int invoiceIDDateCount = schedule.getOlbInvoiceDate().getItemCount();
		
		if(invoiceDate!=null){
			String invoiceIdDate = DateTimeFormat.getShortDateFormat().format(invoiceDate);
			for(int i=0;i<invoiceIDDateCount;i++){
				if(invoiceIdDate.trim().equals(schedule.getOlbInvoiceDate().getItemText(i))){
					schedule.getOlbInvoiceDate().setSelectedIndex(i);
					break;
				}
			}
		}
	}
	
	private void loadInvoiceDropDown(int contractId) {

		MyQuerry querry = new MyQuerry();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter =null;
		
		filter= new Filter();
		filter.setQuerryString("contractCount");
		filter.setIntValue(contractId);
		filtervec.add(filter);
		
		filter= new Filter();
		filter.setQuerryString("status");
		filter.setStringValue("Approved");
		filtervec.add(filter); 
		
		filter= new Filter();
		filter.setQuerryString("typeOfOrder");
		filter.setStringValue(AppConstants.ORDERTYPESERVICE);
		filtervec.add(filter);
		
		filter= new Filter();
		filter.setQuerryString("accountType");
		filter.setStringValue(AppConstants.BILLINGACCOUNTTYPEAR);
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Invoice());
		
		schedule.olbInvoiceId.removeAllItems();
		schedule.olbInvoiceDate.removeAllItems();
		
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				ArrayList<Invoice> list = new ArrayList<Invoice>();
				Console.log("Invoice size=="+result.size());
				
				schedule.olbInvoiceId.addItem("--SELECT--");
				schedule.olbInvoiceDate.addItem("--SELECT--");
				for(SuperModel model : result){
					Invoice invoice = (Invoice) model;
					schedule.getOlbInvoiceId().addItem(invoice.getCount()+"");
					schedule.getOlbInvoiceDate().addItem(DateTimeFormat.getShortDateFormat().format(invoice.getInvoiceDate()));
					list.add(invoice);
				}
				schedule.getOlbInvoiceId().setItems(list);
				schedule.getOlbInvoiceDate().setItems(list);
				
//				System.out.println("INVOICE IDDDDD ="+selectedRecord.getAttributeAsInt("InvoiceId"));
//				System.out.println("INVOICE DATEEEE ="+selectedRecord.getAttributeAsDate("InvoiceDate"));

				int invoiceId = serviceObject.getInvoiceId();
				Date invoiceDate = serviceObject.getInvoiceDate();
				
				setpopUpInvoiceIdDate(invoiceId,invoiceDate);
			}
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				alert.alert("Unexpected Error");
			}
		});
	}
	private void setpopupServiceTime(String serviceTime , ServiceSchedulePopup schedule , boolean flag) {

		if(!serviceTime.equals("")&&!serviceTime.equals("Flexible")){
			
			String[] time=serviceTime.split(":");
			System.out.println(" 0 "+time[0]);
			System.out.println(" 1 "+time[1]);
			String hrs=time[0];
			String min;
			String ampm;
			if(time[1].contains("PM")){
				min=time[1].replace("PM", "");
				ampm="PM";
			}else{
				min=time[1].replace("AM", "");
				ampm="AM";
			}
			System.out.println(" 2 "+hrs+" "+min+" "+ampm);
			
			int count = schedule.getP_servicehours().getItemCount();
			String item=hrs.toString();
			System.out.println("Hours =="+item);
			for(int i=0;i<count;i++)
			{
				System.out.println("HTRRSSS =="+schedule.getP_servicehours().getItemText(i).trim());
				System.out.println(item.trim().equals(schedule.getP_servicehours().getItemText(i).trim()));
				if(item.trim().equals(schedule.getP_servicehours().getItemText(i).trim()))
				{
					schedule.getP_servicehours().setSelectedIndex(i);
					break;
				}
			}
			
			int count1 = schedule.getP_servicemin().getItemCount();
			String item1=min.toString();
			for(int i=0;i<count1;i++)
			{
				if(item1.trim().equals(schedule.getP_servicemin().getItemText(i).trim()))
				{
					schedule.getP_servicemin().setSelectedIndex(i);
					break;
				}
			}
			
			int count2 = schedule.getP_ampm().getItemCount();
			String item2=ampm.toString();
			for(int i=0;i<count2;i++)
			{
				if(item2.trim().equals(schedule.getP_ampm().getItemText(i).trim()))
				{
					schedule.getP_ampm().setSelectedIndex(i);
					break;
				}
			}
			
		}else{
			schedule.getP_servicehours().setSelectedIndex(0);
			schedule.getP_servicemin().setSelectedIndex(0);
			schedule.getP_ampm().setSelectedIndex(0);
		}
	}
	private boolean ValidatePopupData() {
		GWTCAlert alert = new GWTCAlert();
		/**
		 * nidhi
		 * 14-06-2018
		 * for make Technician non Mandetory
		 * 
		 */
		boolean validate = AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "MakeTechnicianNonMandatory");
		
		if(schedule.getEmptable().getDataprovider().getList().size()==0 && !validate){
			alert.alert("Please add Technician ");
			return false;
		}
		if(schedule.getP_ampm().getSelectedIndex()==0){
			alert.alert("Please select AM/PM !");
			return false;
		}
		
		
		/**
		 * end
		 */
		/*if(schedule.getEmptable().getDataprovider().getList().size()==0){
			alert.alert("Please add Technician ");
			return false;
		}*/
		/** date 11/11/2017 commmented by komal to remove validation on service time **/
//		if(planPopUp.servicetime.getSelectedIndex()==0){
//			showDialogMessage("Please Select Service Time");
//			return false;
//		}
		if(schedule.getDateBox().getValue()==null){
			alert.alert("Service Date can not be blank");
			return false;
		}
		
		return true;
	}
	public String getTechnicianName(List<EmployeeInfo> list) {

		String technicianName ="";
		
		/**
		 * nidhi
		 * 18-06-2018
		 */
		if(list.size()==0){
			return technicianName;
		}
		/**
		 * end
		 */
		
		for(int i=0;i<schedule.getOlbTeam().getItems().size();i++){
			if(schedule.getOlbTeam().getItems().get(i).getTeamList().get(0).getEmployeeName().equals(list.get(0).getFullName())){
				System.out.println("Team employee name matched");
				technicianName = list.get(0).getFullName();
				return technicianName;
			}
		}
		
		for(int i=0;i<schedule.getOlbEmployee().getItems().size();i++){
			if(schedule.getOlbEmployee().getItems().get(i).getFullName().equals(list.get(0).getFullName())){
				System.out.println("technician Name matched");
				technicianName = list.get(0).getFullName();
				
			}
		}
		return technicianName;
		

	}
	private void createColumnReschedule() {

		ButtonCell btnCell = new ButtonCell();
		rescheduleButton = new Column<Service, String>(btnCell) {

			@Override
			public String getValue(Service object) {
				// TODO Auto-generated method stub
				return "Reschedule";
			}
		};

		table.addColumn(rescheduleButton, "");
		table.setColumnWidth(rescheduleButton, 110, Unit.PX);
	}

	private void createColumnSchedule() {

		ButtonCell btnCell = new ButtonCell();
		scheduleButton = new Column<Service, String>(btnCell) {

			@Override
			public String getValue(Service object) {
				// TODO Auto-generated method stub
				return "Schedule";
			}
		};

		table.addColumn(scheduleButton, "");
		table.setColumnWidth(scheduleButton, 110, Unit.PX);
		
		scheduleButton.setFieldUpdater(new FieldUpdater<Service, String>() {
			
			@Override
			public void update(int index, Service object, String value) {
				// TODO Auto-generated method stub
				
				ServicePresenter.viewDocumentFlag = false;
				ServicePresenter.scheduleServiceFlag = true;				
			}
				
		});
	}
	private void updateColumnReschedule() {
		rescheduleButton.setFieldUpdater(new FieldUpdater<Service, String>() {
			@Override
			public void update(int index, final Service object, String value) {
				final GWTCAlert alert = new GWTCAlert();

				ServicePresenter.viewDocumentFlag = false;
				/** added by komal for cancelled services **/
				System.out.println("Complete button : " + object.getStatus());
				if (object.getStatus().equalsIgnoreCase(Service.CANCELLED)) {
					alert.alert("Can not reschedule the service as it is cancelled");
				}
				/**
				 * @author Vijay Chougule Date :- 19-06-2020
				 * Des :- Suspended and completed Service can not be reschedule added else if condition for the validation
				 */
				else if(object.getStatus().equalsIgnoreCase(Service.SERVICESUSPENDED)){
					alert.alert("This service is marked "+Service.SERVICESUSPENDED);
				}
				else if(object.getStatus().equalsIgnoreCase(Service.SERVICESTATUSCOMPLETED)){
					alert.alert("This service is marked "+Service.SERVICESTATUSCOMPLETED);
				}
				else {
					/** date 07.03.2018 added by komal to reschedule services which are in scheduled and rescheduled status **/
				//	if(!object.getStatus().equalsIgnoreCase(Service.SERVICESTATUSTECHCOMPLETED)){
					if(checkStatus(object)){
						rowIndex = index;
						serviceObject = object;
						reschedule.makeCustBranchListBox(serviceObject.getPersonInfo().getCount(), serviceObject.getCompanyId(),serviceObject.getServiceBranch());
						reschedule.getTime().setValue(serviceObject.getServiceTime());
						
						System.out.println("Service Date ::::::::: "+serviceObject.getServiceDate());
						Date date = CalendarUtil.copyDate(serviceObject.getServiceDate());
						System.out.println("Service Date1 ::::::::: "+date);
						
//						   reschedule.getDateBox().setValue(date); 
						   reschedule.getDateBox().setValue(new Date());//Ashwini Patil Date:23-05-2022 As per Nitin sir's instruction setting current date in rescedule popup as default
						   serviceObject.setStatus(Service.SERVICESTATUSRESCHEDULE);
						   panel=new PopupPanel(true);
						   panel.add(reschedule);
						   reschedule.clear();
						   panel.show();
						   panel.center();
					}
					//date 07.03.2018 commented by komal 
//					else{
//						GWTCAlert alert = new GWTCAlert();
//						alert.alert("This service is already marked "+Service.SERVICESTATUSTECHCOMPLETED);
//					}
				}
			}
		});
	}

	@SuppressWarnings("deprecation")
	public boolean validateReschedule()
	{
		Date rescheduleDate = reschedule.getDateBox().getValue();
		Date contractEndDate = serviceObject.getContractEndDate();
		Date contractStartDate = serviceObject.getContractStartDate();
//		Date oldServiceDate=model.getServiceDate();
		String reasonForChange=reschedule.getTextArea().getText().trim();
		
		/**Date 24-10-2020 by Amol**/
		

		
		//Ashwini Patil Date:15-05-2023 Make ampm mandatory only if hour or minute selected. For orion
		if(reschedule.getP_servicehours().getSelectedIndex()!=0 || reschedule.getP_servicemin().getSelectedIndex()!=0 || reschedule.getP_ampm().getSelectedIndex()!=0){
			
			if( reschedule.getP_servicehours().getSelectedIndex()==0){
				alert.alert("Please select hours !");
				return false;
			}
			if( reschedule.getP_servicemin().getSelectedIndex()==0){
				alert.alert("Please select minutes !");
				return false;
			}
			if( reschedule.getP_ampm().getSelectedIndex()==0){
				alert.alert("Please select AM/PM !");
				return false;
			}
		}
		
		
		
		if(rescheduleDate==null)
		{
			alert.alert("Reschedule Date Cannot be empty !");
			return false;
		}
		
		/**
		 * @author Vijay Date :- 26-11-2021
		 * Des :- Bug validation not allowing reschedule on contract start date. it should not before contract start date
		 */
		if(AppUtility.formatDate(AppUtility.parseDate(rescheduleDate)).before(AppUtility.formatDate(AppUtility.parseDate(contractStartDate)))){
			alert.alert("Reschedule date should be after contract start date..!");
			return false;
		}
		
		
		//Ashwini Patil Date:23-05-2022 Adding one more validation for date and time as per Nitin sir's instruction
	   if(AppUtility.formatDate(AppUtility.parseDate(rescheduleDate)).before(AppUtility.formatDate(AppUtility.parseDate(new Date())))){			
			alert.alert("You are trying to schedule service in past..!");
			return false;
		}
	   Console.log("date1="+AppUtility.formatDate(AppUtility.parseDate(rescheduleDate)));
	   Console.log("date2="+AppUtility.formatDate(AppUtility.parseDate(new Date())));
	   if(AppUtility.formatDate(AppUtility.parseDate(rescheduleDate)).equals(AppUtility.formatDate(AppUtility.parseDate(new Date())))){			
		   Console.log("scheduling today");
		   if(reschedule.getP_servicehours().getSelectedIndex()!=0&&reschedule.getP_servicemin().getSelectedIndex()!=0&&reschedule.getP_ampm().getSelectedIndex()!=0){
			    int index=reschedule.getP_servicehours().getSelectedIndex();	
			    int hour=Integer.parseInt(reschedule.getP_servicehours().getItemText(index));
			    if(reschedule.getP_ampm().getSelectedIndex()==1&&hour==12)
			    	hour=0;
			    if(reschedule.getP_ampm().getSelectedIndex()==2&&hour!=12)
			    	hour=hour+12;
			    index=reschedule.getP_servicemin().getSelectedIndex();
			    int min=Integer.parseInt(reschedule.getP_servicemin().getItemText(index));
			    Console.log("reschedule time="+hour+":"+min);
			    Date curDate=new Date();
			    Console.log("current time="+curDate.getHours()+":"+curDate.getMinutes());
			    if(hour<curDate.getHours()){
			    	alert.alert("You are trying to schedule service in past..!");
					return false;
			    }
			    if(curDate.getHours()==hour&&min<curDate.getMinutes()){
			    	alert.alert("You are trying to schedule service in past..!");
					return false;
			    }
		   }
		}
//		int cnt =rescheduleDate.compareTo(contractStartDate);
//		if(cnt < 0)
//		{
//			alert.alert("Reschedule date should be after contract start date..!");
//			return false;
//		}	
		if(reasonForChange.equals(""))
		{
			alert.alert("Reason For Change Cannot be Empty !");
			return false;
		}	
		if(serviceObject.getStatus().equalsIgnoreCase(Service.SERVICESTATUSCOMPLETED)){
			alert.alert("Service is already completed !");
			return false;
		}
		
		/**
		 * nidhi
		 * 21-06-20118
		 * for reschedule service for same month validate
		 * Date :- 23-07-2018 By Vijay as discussed with Vaishali mam this validation not for Admin level
		 */
			boolean reScheduleValide  = AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "ServiceRescheduleWithInMonthOnly");
			
			if(reScheduleValide && !LoginPresenter.myUserEntity.getRole().getRoleName().equals("ADMIN")){
					Date serDate = reschedule.getDateBox().getValue();
					
					Date preFirstDate = CalendarUtil.copyDate(serviceObject.getServiceDate());
					Date preLastDate = CalendarUtil.copyDate(serviceObject.getServiceDate());
					
					/** Date 02-04-2019 by Vijay 
					 *  Des :- NBHC CCPM when service date is end of the month then user can able to reschedule the service
					 *  which is bug so i have added below if condition if service Date is end of the month then not
					 *  required add month in the date.  
					 */
					Console.log("preLastDate =="+preLastDate);
					Console.log("preFirstDate =="+preFirstDate);
					if(!AppUtility.checkDateEndOfTheMonth(preFirstDate)){
						Console.log("Not End of the month"+preLastDate);
						CalendarUtil.addMonthsToDate(preLastDate, 1);
						preLastDate.setDate(1);
						CalendarUtil.addDaysToDate(preLastDate, -1);
					}
					
					/**
					 * Date 10-Aug-2018 By Vijay
					 * Des :- Reschedule date only allow service date month only.
					 */
					Date firstDateServiceDate = CalendarUtil.copyDate(serviceObject.getServiceDate());
					CalendarUtil.setToFirstDayOfMonth(firstDateServiceDate);
					Console.log("preLastDate =="+preLastDate);
					Console.log("serDate =="+serDate);
					
					
					if(serDate.after(preLastDate) || serDate.before(firstDateServiceDate) ){
						alert.alert("Service date can be Reschedule With in Service date's month only.");
						return false;
					}
//					if(serDate.before(serviceObject.getServiceDate())){
//						form.showDialogMessage("Reschedule service date should be after current service date.");
//						return false;
//					}
					
			}
			
			
			/**
			 * @author Vijay Chougule
			 * Project :- Fumigation Tracker
			 * Des :- when CIO Reschedule the stack fumigation service then In EVA ERP System it can not allow to
			 * reschedule CIO Reschedule Date + 5 Days. 
			 */
			if(serviceObject.isWmsServiceFlag() && (serviceObject.getProduct().getProductCode().trim().equals("PHM-01") || serviceObject.getProduct().getProductCode().trim().equals("STK-01"))
					&& serviceObject.getCioRescheduleDate()!=null){
				if(!LoginPresenter.myUserEntity.getRole().getRoleName().equals("ADMIN")){
					Date newrescheduleDate = reschedule.getDateBox().getValue();
					Date cioRescheduleDate = serviceObject.getCioRescheduleDate();
					CalendarUtil.addDaysToDate(cioRescheduleDate, 5);
					Console.log("cioRescheduleDate"+cioRescheduleDate);
					if(newrescheduleDate.after(cioRescheduleDate)){
						form.showDialogMessage("Can not reschedule more than 5 days of Cio Reschedule Date : "+AppUtility.parseDate(cioRescheduleDate));
						return false;
					}
				}
			}
			/**
			 * ends here
			 */
		return true;
	}
	public void saveDetails(final String sucesssMessage,
			final String failureMessage,
			final String statusText,final ModificationHistory history,
			final Date ServiceDate,final String serviceTime,final String serviceBranch)
	{
		SaveServiceAsync asyncService=GWT.create(SaveService.class);
		final GWTCAlert alert = new GWTCAlert();
		asyncService.saveStatus(serviceObject,new AsyncCallback<Boolean>() {
        
			@Override
			public void onFailure(Throwable caught) {
				
		//		form.showDialogMessage(failureMessage);
	//			form.hideWaitSymbol();
				caught.printStackTrace();
				
			}
			@Override
			public void onSuccess(Boolean result) {
				if(result==false)
				{
					alert.alert("Service Cannot be marked Completed as there are "
							+ "Scheduled Projects");
					return;
				}
				final GWTCAlert alert2 = new GWTCAlert();

				getUpdatedServiceData(sucesssMessage,alert2);
//				alert.alert(sucesssMessage);
////				serviceObject.setServiceScheduled(false);
//				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
//				table.redrawRow(rowIndex);
//				panel.hide();			 
			}
		});
	}




private void ReactOnServiceCompletionPopUp(ClickEvent event) {

		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "OnlyForNBHC")){
			
				if(schedule.getMarkcompletepopup().getServiceTime().getValue()!=null){
					if(schedule.getMarkcompletepopup().getServiceTime().getValue()!=0){
						
					
						String formTime = schedule.getMarkcompletepopup().getP_servicehours().getItemText(schedule.getMarkcompletepopup().getP_servicehours().getSelectedIndex())+":"+
								schedule.getMarkcompletepopup().getP_servicemin().getItemText(schedule.getMarkcompletepopup().getP_servicemin().getSelectedIndex())+":"+
								schedule.getMarkcompletepopup().getP_ampm().getItemText(schedule.getMarkcompletepopup().getP_ampm().getSelectedIndex());
						
						System.out.println("formTime form popup "+formTime);
						
						String toTime = schedule.getMarkcompletepopup().getTo_servicehours().getItemText(schedule.getMarkcompletepopup().getTo_servicehours().getSelectedIndex())+":"+
								schedule.getMarkcompletepopup().getTo_servicemin().getItemText(schedule.getMarkcompletepopup().getTo_servicemin().getSelectedIndex())+":"+
								schedule.getMarkcompletepopup().getTo_ampm().getItemText(schedule.getMarkcompletepopup().getTo_ampm().getSelectedIndex());
						System.out.println("toTime form popup "+toTime);
						
					/**
					 * Added By Rahul Verma
					 * This method is only for nbhc. and it will check whether there stock current status is>0 and they have service or not.
					 * 
					 */
					if(serviceObject.getEmployee().trim().equalsIgnoreCase("Snehal Abhishek Palav") && serviceObject.getStackDetailsList().size()!=0){
						createExtraServiceIfStackZero(serviceObject);
					}
					/**
					 * ends here 
					 */
					String  rating = null;
					if(schedule.getMarkcompletepopup().getLbRating().getSelectedIndex() != 0){
						rating = schedule.getMarkcompletepopup().getLbRating().getValue(schedule.getMarkcompletepopup().getLbRating().getSelectedIndex());
					}
					reactOnComplete(schedule.getMarkcompletepopup().getServiceCompletionDate().getValue(),formTime,toTime,
							schedule.getMarkcompletepopup().getRemark().getValue() , schedule.getMarkcompletepopup().getServiceTime().getValue()+"" , rating, false);

					// ********vaishnavi****************
					createProjectonMarkComplete();
				}else{
					alert.alert("Please add Service Duration (In Minutes)");
				}
		}else{
			alert.alert("Service Duration (In Minutes) is mandetory..!");
		}
}
else
{
	
			String formTime = schedule.getMarkcompletepopup().getP_servicehours().getItemText(schedule.getMarkcompletepopup().getP_servicehours().getSelectedIndex())+":"+
					schedule.getMarkcompletepopup().getP_servicemin().getItemText(schedule.getMarkcompletepopup().getP_servicemin().getSelectedIndex())+":"+
					schedule.getMarkcompletepopup().getP_ampm().getItemText(schedule.getMarkcompletepopup().getP_ampm().getSelectedIndex());
			
			System.out.println("formTime form popup "+formTime);
			
			String toTime = schedule.getMarkcompletepopup().getTo_servicehours().getItemText(schedule.getMarkcompletepopup().getTo_servicehours().getSelectedIndex())+":"+
					schedule.getMarkcompletepopup().getTo_servicemin().getItemText(schedule.getMarkcompletepopup().getTo_servicemin().getSelectedIndex())+":"+
					schedule.getMarkcompletepopup().getTo_ampm().getItemText(schedule.getMarkcompletepopup().getTo_ampm().getSelectedIndex());
			System.out.println("toTime form popup "+toTime);

		if(serviceObject.getEmployee().trim().equalsIgnoreCase("Snehal Abhishek Palav") && serviceObject.getStackDetailsList().size()!=0){
			createExtraServiceIfStackZero(serviceObject);
		}

		String  rating = null;
		if(schedule.getMarkcompletepopup().getLbRating().getSelectedIndex() != 0){
			rating = schedule.getMarkcompletepopup().getLbRating().getValue(schedule.getMarkcompletepopup().getLbRating().getSelectedIndex());
		}
		reactOnComplete(schedule.getMarkcompletepopup().getServiceCompletionDate().getValue(),formTime,toTime,
				schedule.getMarkcompletepopup().getRemark().getValue() , schedule.getMarkcompletepopup().getServiceTime().getValue()+"" , rating , false);
		
		// ********vaishnavi****************
		createProjectonMarkComplete();
		
	}


	}
private void createExtraServiceIfStackZero(Service model) {
	// TODO Auto-generated method stub
	Console.log("Inside Create Extra Service");
	for (int i = 0; i < model.getStackDetailsList().size(); i++) {
		if(model.getStackDetailsList().get(i).getQauntity()!=0){
			Console.log("Quantity not eQAUL to zero");
			createAnExtraService(model);
			break;
		}else{
			System.out.println("Do not create service");
		}
	}
}

private void createAnExtraService(final Service model) {
	// TODO Auto-generated method stub
	Console.log("Calling RPC to complete services");
	final CreateSingleServiceServiceAsync createSingleServiceAsync = GWT
			.create(CreateSingleServiceService.class);
	final GWTCAlert alert = new GWTCAlert();
	Timer timer=new Timer() {
		
		@Override
		public void run() {
			// TODO Auto-generated method stub
			createSingleServiceAsync.createSingleService(model.getCount(),model.getServiceDate(),model.getServiceIndexNo(),model.getServiceSerialNo(),model.getContractCount(),model.getCompanyId(),new AsyncCallback<ArrayList<Integer>>() {

				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					Console.log("Failed"+caught);
					alert.alert("An Unexpected Error");
				}

				@Override
				public void onSuccess(ArrayList<Integer> result) {
					// TODO Auto-generated method stub
					if(result.contains(-1)){
						alert.alert("Unexpected Error");
					}else if(result.contains(-2)){
						alert.alert("Parsing Previous Date Error");
					}else if(result.contains(-3)){
						alert.alert("Service Date Parsing Error");
					}else if(result.contains(1)){
						alert.alert("Successfull created Upcoming Services");
					}
				}
			});
		}
	};
	timer.schedule(3000);
	
}
private void createProjectonMarkComplete() {
	final GWTCAlert alert = new GWTCAlert();
	Timer timer = new Timer() {
		@Override
		public void run() {

			projectAsync.createCustomerProject(serviceObject.getCompanyId(),
					serviceObject.getContractCount(), serviceObject.getCount(),serviceObject.getStatus(),
					new AsyncCallback<Integer>() {

						@Override
						public void onFailure(Throwable caught) {
							alert.alert("An Unexpected Error occured !");
						}

						@Override
						public void onSuccess(Integer result) {
							RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
							table.redrawRow(rowIndex);
							System.out.println("project created .......!!");
						}
					});
		}
	};
	timer.schedule(3000);
}
/** date 16.3.2019 added by komal to reuse method (added new parameter) **/
private void reactOnComplete(Date completionDt,String formTime,String toTime,String remark , String serviceTime ,String rating ,boolean flag) {
	
	System.out.println("in side if");
	serviceObject.setServiceCompletionDate(completionDt);
	
if(remark!=null){
	serviceObject.setServiceCompleteRemark(remark);
}

if(serviceTime!=null){
	try{
	serviceObject.setServiceCompleteDuration(Integer.parseInt(serviceTime));
	}catch(Exception e){
		serviceObject.setServiceCompleteDuration(0);
	}
}

if(!formTime.equals("")){
	serviceObject.setFromTime(formTime);
}

if(!toTime.equals("") ){
	serviceObject.setToTime(toTime);
}

if(rating != null){
	serviceObject.setCustomerFeedback(rating);
}
if(flag){

manageProcessLevelBar("Marked Completed !","Failed To Mark Complete !",Service.SERVICESTATUSCOMPLETED, 
		null, null,null,null,technicianServiceCompletePopup);
}else{
manageProcessLevelBar("Marked Completed !","Failed To Mark Complete !",Service.SERVICESTATUSCOMPLETED, 
		null, null,null,null);
}
}


public void manageProcessLevelBar(final String sucesssMessage,
		final String failureMessage,
		final String statusText,final ModificationHistory history,
		final Date ServiceDate,final String serviceTime,final String serviceBranch)
{
	ArrayList<ProductGroupList> materialList = new ArrayList<ProductGroupList>();
	materialList.addAll(schedule.getMaterialProductTable().getDataprovider().getList());
	String status = serviceObject.getStatus();
	serviceObject.setStatus(statusText);
	genservice.saveServiceProjectStatus(serviceObject, new AsyncCallback<String>() {
		
		@Override
		public void onSuccess(String result) {
			// TODO Auto-generated method stub
			alert.alert(sucesssMessage);
			RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
			table.redrawRow(rowIndex);
			schedule.hidePopUp();
			/**
			 * nidhi 17-04-2018
			 * for hide pop up
			 */
			schedule.getPanel().hide();
            panel.hide();
			sendSMSLogic();
		}
		
		@Override
		public void onFailure(Throwable caught) {
			// TODO Auto-generated method stub
			
		}
	});
	
	

	// MIN Creating 
	boolean editableFlag = false;
	if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "MakeServiceEditableAfterCompletion") && status.equals("Completed")){
		editableFlag = true;
	}
	Console.log("Before Calling MIN  ");
		if(!(status.equals("Completed"))   || editableFlag){
		int projectId = serviceObject.getProjectId();
		System.out.println("project Id =="+projectId);
			servicelistservice.TechnicianSchedulingListMarkcompleteservice(serviceObject,LoginPresenter.loggedInUser,projectId,materialList, new AsyncCallback<ArrayList<Integer>>() {
			
			@Override
			public void onSuccess(ArrayList<Integer> result) {
				// TODO Auto-generated method stub
				Console.log("MIN on success");
				if(result.contains(2)){
					//getDataprovider().getList().remove(serviceObject);
					alert.alert("Service Completed Sucessfully");
					table.redraw();
					
				}				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				alert.alert("An Unexpected error occurred!");

			}
		});
	}
	
}
private void sendSMSLogic()
{
	
	/**
	 * @author Anil @since 19-05-2021
	 * shifted SMS code to server side 
	 * for making tiny url
	 */
	if (serviceObject.getStatus().equals(Service.SERVICESTATUSCOMPLETED)) {
		ServiceListServiceAsync servicelistserviceAsync = GWT.create(ServiceListService.class);
		servicelistserviceAsync.sendServiceComletionSMS(serviceObject, new AsyncCallback<String>() {
	
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
	
			@Override
			public void onSuccess(String result) {
				// TODO Auto-generated method stub
				if(result!=null && !result.equals("")){
					form.showDialogMessage(result);
				}
			}
		});
	}
	
//	Vector<Filter> filtrvec = new Vector<Filter>();
//	Filter filtr = null;
//	
//	filtr = new Filter();
//	filtr.setLongValue(serviceObject.getCompanyId());
//	filtr.setQuerryString("companyId");
//	filtrvec.add(filtr);
//	
//	filtr = new Filter();
//	filtr.setBooleanvalue(true);
//	filtr.setQuerryString("status");
//	filtrvec.add(filtr);
//	
//	MyQuerry querry = new MyQuerry();
//	querry.setFilters(filtrvec);
//	querry.setQuerryObject(new SmsConfiguration());
////	form.showWaitSymbol();
//	
//	service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
//		
//		@Override
//		public void onSuccess(ArrayList<SuperModel> result) {
//			if(result.size()!=0){
//				for(SuperModel model:result){
//					SmsConfiguration smsconfig = (SmsConfiguration) model;
//					boolean smsStatus = smsconfig.getStatus();
//					String accountsid = smsconfig.getAccountSID();
//					String authotoken = smsconfig.getAuthToken();
//					String fromnumber = smsconfig.getPassword();
//					
//					if(smsStatus==true){
//					 sendSMS(accountsid, authotoken, fromnumber);
//					}
//					else{
//						alert.alert("SMS Configuration is InActive");
//					}
//				}
//			}
//		}
//
//		@Override
//		public void onFailure(Throwable caught) {
//		}
//	});
	
}
private void sendSMS(final String accountsid, final String authotoken,final String fromnumber) {
	
	Vector<Filter> filtervec = new Vector<Filter>();
	Filter filtr = null;
	filtr = new Filter();
	filtr.setLongValue(serviceObject.getCompanyId());
	filtr.setQuerryString("companyId");
	filtervec.add(filtr);
	
	System.out.println("SERVICE STATUS :: "+serviceObject.getStatus());
	
	if (serviceObject.getStatus().equals(Service.SERVICESTATUSCOMPLETED)) {

		filtr = new Filter();
		filtr.setStringValue("Service Completion");
		filtr.setQuerryString("event");
		filtervec.add(filtr);
	} else if (serviceObject.getStatus().equals(Service.SERVICESTATUSSCHEDULE)) {

		filtr = new Filter();
		filtr.setStringValue("Service Schedule");
		filtr.setQuerryString("event");
		filtervec.add(filtr);
	}else if (serviceObject.getStatus().equals(Service.SERVICESTATUSRESCHEDULE)) {

		filtr = new Filter();
		filtr.setStringValue("Service Reschedule");
		filtr.setQuerryString("event");
		filtervec.add(filtr);
	}else{
		filtr = new Filter();
		filtr.setStringValue("");
		filtr.setQuerryString("event");
		filtervec.add(filtr);
	}
	
	filtr = new Filter();
	filtr.setBooleanvalue(true);
	filtr.setQuerryString("status");
	filtervec.add(filtr);
	
	MyQuerry querry = new MyQuerry();
	querry.setFilters(filtervec);
	querry.setQuerryObject(new SmsTemplate());
//	form.showWaitSymbol();
	
	service.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
		@Override
		public void onSuccess(ArrayList<SuperModel> result) {
			final SmsServiceAsync smsserviceAsync=GWT.create(SmsService.class);
			if (result.size() != 0) {
				for(SuperModel smodel:result){
					final SmsTemplate sms =(SmsTemplate) smodel;
					final String templateMsgwithbraces = new String(sms.getMessage()); 
//					String custName = form.getPoc().getName().getValue();
					final String prodName = serviceObject.getProductName();
					final String serNo = serviceObject.getServiceSerialNo()+"";
					System.out.println("Service No===="+serNo);
					
					cellNo = serviceObject.getPersonInfo().getCellNumber();
					final String serDate = AppUtility.parseDate(serviceObject.getServiceDate());
					
					
					/**
					 * Date - 29 jun 2017 added by vijay for Eco Friendly getting customer name or customer correspondence name
					 */
					
					if(LoginPresenter.bhashSMSFlag){

						MyQuerry myquerry = AppUtility.getcustomerName(serviceObject.getPersonInfo().getCount(), serviceObject.getCompanyId());
						service.getSearchResult(myquerry, new AsyncCallback<ArrayList<SuperModel>>() {
							
							@Override
							public void onSuccess(ArrayList<SuperModel> result) {
								// TODO Auto-generated method stub
								for(SuperModel model :result){
									Customer customer = (Customer) model;
									 String custName;
									 Console.log("Currespondence=="+customer.getCustPrintableName());
									if(!customer.getCustPrintableName().equals("")){
										custName = customer.getCustPrintableName();
										Console.log("Customer correspondence Name =="+custName);
									}else{
										custName = customer.getFullname();
										Console.log("Customer  Name =="+custName);
									}
									String cutomerName = templateMsgwithbraces.replace("{CustomerName}", custName);
									String productName = cutomerName.replace("{ProductName}", prodName);
									String serviceDate =  productName.replace("{ServiceDate}", serDate);
									companyName = Slick_Erp.businessUnitName;
									actualMsg = serviceDate.replace("{companyName}", companyName);

									System.out.println("Actual MSG:"+actualMsg);
//									smsserviceAsync.sendSmsToClient(actualMsg, cellNo, accountsid, authotoken,fromnumber,serviceObject.getCompanyId(), new AsyncCallback<Integer>(){
//
//										@Override
//										public void onFailure(Throwable caught) {
//										}
//
//										@Override
//										public void onSuccess(Integer result) {
//											if(result==1)
//											{
//												alert.alert("SMS Sent Successfully!");
//												saveSmsHistory();
//
//											}
//										}} );
									
									smsserviceAsync.sendMessage(AppConstants.SERVICEMODULE, AppConstants.SERVICE, sms.getEvent(), serviceObject.getCompanyId(), actualMsg, cellNo,false, new AsyncCallback<String>() {
										
										@Override
										public void onSuccess(String result) {
											// TODO Auto-generated method stub
											alert.alert("Message Sent Successfully!");

										}
										
										@Override
										public void onFailure(Throwable caught) {
											// TODO Auto-generated method stub
											
										}
									});
									
								}
							}
							
							@Override
							public void onFailure(Throwable caught) {
								// TODO Auto-generated method stub
								
							}
						});
						
						
						
						
					}else{
					/**
					 * ends here
					 */
					
					String custName = serviceObject.getPersonInfo().getFullName();

					String cutomerName = templateMsgwithbraces.replace("{CustomerName}", custName);
					String productName = cutomerName.replace("{ProductName}", prodName);
					String serviceNo = productName.replace("{ServiceNo}", serNo);
					String serviceDate =  serviceNo.replace("{ServiceDate}", serDate);
					companyName = Slick_Erp.businessUnitName;
					actualMsg = serviceDate.replace("{companyName}", companyName);

					System.out.println("Actual MSG:"+actualMsg);
					
					System.out.println("Company Name========================:"+companyName);
					
//					smsserviceAsync.sendSmsToClient(actualMsg, cellNo, accountsid, authotoken,fromnumber,serviceObject.getCompanyId(), new AsyncCallback<Integer>(){
//
//						@Override
//						public void onFailure(Throwable caught) {
//						}
//
//						@Override
//						public void onSuccess(Integer result) {
//							if(result==1)
//							{
//								alert.alert("SMS Sent Successfully!");
////								saveSmsHistory();
//
//							}
//						}} );
					
					smsserviceAsync.sendMessage(AppConstants.SERVICEMODULE, AppConstants.SERVICE, sms.getEvent(), serviceObject.getCompanyId(), actualMsg, cellNo,false, new AsyncCallback<String>() {
						
						@Override
						public void onSuccess(String result) {
							// TODO Auto-generated method stub
							alert.alert("Message Sent Successfully!");

						}
						
						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							
						}
					});
					
				}
				}
			}
		
			
		}
		@Override
		public void onFailure(Throwable caught) {
		}
	});

}	
private void saveSmsHistory (){
	SmsHistory smshistory = new SmsHistory();
	smshistory.setDocumentId(serviceObject.getCount());
	smshistory.setDocumentType("Service");
	smshistory.setSmsText(actualMsg);
	smshistory.setName(serviceObject.getPersonInfo().getFullName());
	smshistory.setCellNo(cellNo);
	smshistory.setUserId(LoginPresenter.loggedInUser);
	System.out.println("UserID"+LoginPresenter.loggedInUser);
	System.out.println("Doc Id"+serviceObject.getCount());
	service.save(smshistory, new AsyncCallback<ReturnFromServer>() {
		
		@Override
		public void onSuccess(ReturnFromServer result) {
			System.out.println("Data Saved SuccessFully");
		}
		
		@Override
		public void onFailure(Throwable caught) {
			// TODO Auto-generated method stub
			System.out.println("Data Saved UnsuccessFully");

		}
	});		
}
//komal
private boolean checkStatus(Service object){
	GWTCAlert alert = new GWTCAlert();
	if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "MakeServiceEditableAfterCompletion")){
		if(object.getStatus().equals(Service.SERVICESTATUSCANCELLED)
				||object.getStatus().equals(Service.SERVICESTATUSCLOSED)
			//	||object.getStatus().equals(Service.SERVICESTATUSCOMPLETED)
				||object.getStatus().equals(Service.SERVICESTATUSREPORTED)
				||object.getStatus().equals(Service.SERVICESTATUSPENDING)
			//	||object.getStatus().equals(Service.SERVICESTATUSTECHCOMPLETED)
				){
			alert.alert("Service status should be Scheduled/Rescheduled!");
			return false;
		}
	}else{
		if(object.getStatus().equals(Service.SERVICESTATUSCANCELLED)
				||object.getStatus().equals(Service.SERVICESTATUSCLOSED)
				||object.getStatus().equals(Service.SERVICESTATUSCOMPLETED)
				||object.getStatus().equals(Service.SERVICESTATUSREPORTED)
				||object.getStatus().equals(Service.SERVICESTATUSPENDING)
				||object.getStatus().equals(Service.SERVICESTATUSTECHCOMPLETED)
				||object.getStatus().equals(Service.SERVICETCOMPLETED)){
			alert.alert("Service status should be Scheduled/Rescheduled!");
			return false;
		}
}
	return true;
 }

///** date 17.4.2018 added by komal for orion **/
//public void addClickColumnServicingBranch() {
//    
//	ClickableTextCell clickCell = new ClickableTextCell();
//	clickServicingBranch = new Column<Service, String>(clickCell) {
//
//		@Override
//		public String getValue(Service object) {
//			
//			return object.getServiceBranch();
//		}
//	};
//	clickServicingBranch.setFieldUpdater(new FieldUpdater<Service, String>() {
//        @Override
//        public void update(int index, Service object, String value) {
//        	if(object.getServiceBranch()!=null && !(object.getServiceBranch().equals(""))){
//        		//addressPopup = new AddressPopup();
//        		addressPopup.showPopUp();
//        		addressPopup.getLblAddress().setText(object.getEntireAddress());
//        		addressPopup.getLblOk().setVisible(false);
//        	}
//        }
//    });
//	table.addColumn(clickServicingBranch, "Service Branch");
//	table.setColumnWidth(clickServicingBranch, 150, Unit.PX);
//
//}

/*** Date 27-11-2018 By Vijay Updated with Button ***/
/** date 17.4.2018 added by komal for orion **/
public void addClickColumnServicingBranch() {
	ButtonCell btnCell = new ButtonCell();
	clickServicingBranch = new Column<Service, String>(btnCell) {

		@Override
		public String getValue(Service object) {
			
			return object.getServiceBranch();
		}
	};
	clickServicingBranch.setFieldUpdater(new FieldUpdater<Service, String>() {
        @Override
        public void update(int index, Service object, String value) {
        	if(object.getServiceBranch()!=null && !(object.getServiceBranch().equals(""))){
        		//addressPopup = new AddressPopup();
				ServicePresenter.viewDocumentFlag = false;
				addressPopup.getLblBranch().setText("");
				addressPopup.getLblAddress().setText("");
				addressPopup.getLblLocation().setText("");
				addressPopup.getLblArea().setText("");
				//Ashwini Patil Date:24-05-2022 	
				addressPopup.getPopup().setPixelSize(300, 300);
				String details="";
				if(object.getServiceBranch()!=null&&!object.getServiceBranch().equals("")) {
					addressPopup.getLblBranch().setText("Service Branch - "+object.getServiceBranch());
				}
				if(object.getEntireAddress()!=null&&!object.getEntireAddress().equals("")) {
					addressPopup.getLblAddress().setText("Service Address - "+object.getEntireAddress());
				}
				if(object.getServiceLocation()!=null&&!object.getServiceLocation().equals("")){
					addressPopup.getLblLocation().setText("Location - "+object.getServiceLocation());
				}				
				if(object.getServiceLocationArea()!=null&&!object.getServiceLocationArea().equals("")) {
					addressPopup.getLblArea().setText("Area - "+object.getServiceLocationArea());
				}							
        		addressPopup.showPopUp();
//        		addressPopup.getLblAddress().setText(details);//object.getEntireAddress()
        		addressPopup.getLblOk().setVisible(false);
        	}
        }
    });
	table.addColumn(clickServicingBranch, "Service Branch");
	table.setColumnWidth(clickServicingBranch, 150, Unit.PX);
	clickServicingBranch.setSortable(true);
}


private void createColumnFumigation() {

	ButtonCell btnCell = new ButtonCell();
	fumigationButton = new Column<Service, String>(btnCell) {

		@Override
		public String getValue(Service object) {
			// TODO Auto-generated method stub
			return "Fumigation";
		}
	};

	table.addColumn(fumigationButton, "");
	table.setColumnWidth(fumigationButton, 110, Unit.PX);
}

private void updateColumnFumigation() {
	fumigationButton.setFieldUpdater(new FieldUpdater<Service, String>() {
		@Override
		public void update(int index, final Service object, String value) {
			System.out.println("fumigation button : " + object.getStatus());
			ServicePresenter.viewDocumentFlag = false;
				if (object.getStatus().equalsIgnoreCase(
						Service.SERVICESTATUSCOMPLETED)) {
					rowIndex = index;
					serviceObject = object;
					popupPanel = new PopupPanel(true);
					popupPanel.add(conditionPopup);
					popupPanel.show();
					popupPanel.center();
				} else {
					final GWTCAlert alert = new GWTCAlert();
					alert.alert("Service should be completed before creating fumigation.");
				}
		}
	});
  }


/*
 * Date:11/08/2018
 * Developer:Ashwini
 * Purpose:To add contact details in customer service list
 */

public void createColumnContractdetails(){
	ButtonCell btncell2 = new ButtonCell();
	contactdetailsbutton = new Column<Service, String>(btncell2){
		@Override
		public String getValue(Service object) {
			// TODO Auto-generated method stub
			return "Contact Details";
		}
	};
	table.addColumn(contactdetailsbutton, "");
	table.setColumnWidth(contactdetailsbutton, 110, Unit.PX);
}


public void updateColumnContractdetails(){
	contactdetailsbutton.setFieldUpdater(new FieldUpdater<Service, String>() {
		@Override
		public void update(int index, final Service object, String value) {
			
	System.out.println("contactdetailsbutton");
		ServicePresenter.viewDocumentFlag = false;

			 MyQuerry querry = new MyQuerry();
			 Vector<Filter> filterVec = new Vector<Filter>();
			 
			 Filter filter =null;
			 filter=new Filter();
			 filter.setIntValue(object.getPersonInfo().getCount());
			 filter.setQuerryString("count");
			 filterVec.add(filter);
			 querry.setFilters(filterVec);
			 querry.setQuerryObject(new Customer());
			 service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					// TODO Auto-generated method stub
					
					contractpopup.showPopUp();
					for(SuperModel model : result){
						Customer customer = (Customer) model;
						contractpopup.getLandLine().setValue(customer.getLandline()+"");
						contractpopup.getCellNumber2().setValue(customer.getCellNumber2()+"");
						contractpopup.getCellNumber1().setValue(customer.getCellNumber1()+"");
						contractpopup.getEmail().setValue(customer.getEmail()+"");
						contractpopup.setEnable(false);
						contractpopup.getLblCancel().setVisible(false);
						
						break;
					}
				}
//				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					
				}
			});
			 
		}
	});
	
}
		 
		
/*
 * End by Ashwini
 */

public GeneralViewDocumentPopup getGenralInvoicePopup() {
	return genralInvoicePopup;
}

public void setGenralInvoicePopup(GeneralViewDocumentPopup genralInvoicePopup) {
	this.genralInvoicePopup = genralInvoicePopup;
}

private void addColumnInvoiceNo() {

	proInvoiceNo = new TextColumn<Service>() {

		@Override
		public String getValue(Service object) {
			return object.getInvoiceId() + "";
		}
	};
	table.addColumn(proInvoiceNo, "Invoice No");
	table.setColumnWidth(proInvoiceNo, 100, Unit.PX);
//	getcolumnServiceWeekNo.setCellStyleNames("wordWrap");
	proInvoiceNo.setSortable(true);
}

protected void addSortinggetInvoiceNo() {
	List<Service> list = getDataprovider().getList();
	columnSort = new ListHandler<Service>(list);
	columnSort.setComparator(proInvoiceNo, new Comparator<Service>() {
		@Override
		public int compare(Service e1, Service e2) {
			if (e1 != null && e2 != null) {

				if (e1.getInvoiceId() == 0
						&& e2.getInvoiceId() == 0) {
					return 0;
				}
				if (e1.getInvoiceId() == 0)
					return 1;
				if (e2.getInvoiceId() == 0)
					return -1;
				
				Integer inv1 = e1.getInvoiceId();
				Integer inv2 = e2.getInvoiceId();
				return inv1.compareTo(inv2);

			}
			return 0;
		}
	});
	table.addColumnSortHandler(columnSort);
}
/** date 27.02.2018 added by komal for customer service list new changes **/
private void getProjectAndBOMDetails(final Service model ,final TechnicianServiceSchedulePopup schedule , final boolean flag) {

	final int contractId = model.getContractCount();
	
	 MyQuerry querry = new MyQuerry();
	 Vector<Filter> filterVec = new Vector<Filter>();
	 
	 Filter filter =null;
	 filter=new Filter();
	 filter.setIntValue(model.getCount());
	 filter.setQuerryString("serviceId");
	 filterVec.add(filter);
	 System.out.println("proj =="+model.getProjectId());
	 int projectId = model.getProjectId();
	 if(projectId!=0){
		 System.out.println("PROJECTttt");
		 filter=new Filter();
		 filter.setIntValue(projectId);
		 filter.setQuerryString("count");
		 filterVec.add(filter);
	 }
	 
	 querry.setFilters(filterVec);
	 querry.setQuerryObject(new ServiceProject());
	 
	 service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
		
		@Override
		public void onSuccess(final ArrayList<SuperModel> result1) {
			// TODO Auto-generated method stub
			System.out.println("result ="+result1.size());
			
			MyQuerry querry1 = new MyQuerry();
			 Vector<Filter> filterVec = new Vector<Filter>();
			 
			 Filter filter =null;
			 filter=new Filter();
			 filter.setStringValue(model.getEmployee());
			 filter.setQuerryString("wareHouseList.technicianName");
			 filterVec.add(filter);
			 
			 querry1.setFilters(filterVec);
			 querry1.setQuerryObject(new TechnicianWareHouseDetailsList());
			
			 service.getSearchResult(querry1, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					// TODO Auto-generated method stub
					TechnicianWareHouseDetailsList technicianDetails = null;
					for(SuperModel model : result){
						technicianDetails = (TechnicianWareHouseDetailsList) model;
						break;
					}
					String warehouseName = "" ,storageLocation = "", storageBin = "",
							parentWareHouse = "" , parentStorageLocation = "" , parentStorageBin = "";
					
					if(technicianDetails != null){
						
						for(TechnicianWarehouseDetails details : technicianDetails.getWareHouseList()){
							if(details.getTechnicianName().equalsIgnoreCase(model.getEmployee())){
								warehouseName = details.getTechnicianWareHouse();
								storageLocation = details.getTechnicianStorageLocation();
								schedule.getOlbStorageLocation().addItem(storageLocation);
								storageBin = details.getTechnicianStorageBin();
								schedule.getOlbstorageBin().addItem(storageBin);
								parentWareHouse = details.getParentWareHouse();
								parentStorageLocation = details.getParentStorageLocation();
								schedule.getOlbParentStorageLocation().addItem(parentStorageLocation);
								parentStorageBin = details.getPearentStorageBin();
								schedule.getOlbParentstorageBin().addItem(parentStorageBin);
								break;
							}
						}
					}
					schedule.showPopUp();
					schedule.clear();
					if(flag){
						schedule.lblUpdate.getElement().setId("addbutton");
						schedule.getHorizontal().add(schedule.lblUpdate);
						schedule.setEnable(false);
						schedule.getMaterialProductTable().setEnable(false);
						schedule.getEmptable().setEnable(false);
						schedule.getToolTable().setEnable(false);
						
						
					}else{
						if(model.isServiceScheduled()){
							schedule.setEnable(false);
							schedule.getMaterialProductTable().setEnable(false);
							schedule.getEmptable().setEnable(false);
							schedule.getToolTable().setEnable(false);

							schedule.getLblOk().setVisible(false);
							schedule.lblSchedule.setVisible(false);
							
						}else{
							//schedule.lblSchedule.getElement().setId("addbutton");
							//schedule.getHorizontal().add(schedule.lblSchedule);
							//schedule.getLblOk().setVisible(true);
							schedule.setEnable(true);
							schedule.getMaterialProductTable().setEnable(true);
							schedule.getEmptable().setEnable(true);
							schedule.getToolTable().setEnable(true);
							schedule.getLblOk().setVisible(true);
							schedule.lblSchedule.setVisible(true);
						}
				
					}
					loadInvoiceDropDown(contractId , schedule);
					
					schedule.getOlbParentstorageBin().setValue(parentStorageBin);
					schedule.getOlbParentWarehouseName().setValue(parentWareHouse);
					schedule.getOlbParentStorageLocation().setValue(parentStorageLocation);
					schedule.getOlbStorageLocation().setValue(storageLocation);
					schedule.getOlbstorageBin().setValue(storageBin);
					schedule.getOlbWarehouseName().setValue(warehouseName);
					
					schedule.getEmptable().getDataprovider().getList().clear();
					schedule.getToolTable().getDataprovider().getList().clear();
					schedule.getMaterialProductTable().getDataprovider().getList().clear();
					schedule.getDbQuantity().setValue(0d);
//					if(model.getStatus().equals("Completed")){
//						schedule.setEnable(false);
//						schedule.getMaterialProductTable().setEnable(false);
//					}else{
//						schedule.setEnable(true);
//						schedule.getMaterialProductTable().setEnable(true);
//					}
					if(!(model.getEmployee().equals(""))){
					schedule.getOlbEmployee().setValue(model.getEmployee());
					
					/**
					 * Date 09-08-2018 By Vijay
					 * Bug :- Bug - updated code with employee information from global list
					 * old code commneted
					 */
					List<EmployeeInfo> empInfoEntityList = new ArrayList<EmployeeInfo>();
					for(Employee empEntity : LoginPresenter.globalEmployee){
						if(empEntity.getFullname().equals(model.getEmployee())){
							empInfoEntityList = new ArrayList<EmployeeInfo>();
							EmployeeInfo empInfoEntity = new EmployeeInfo();

							empInfoEntity.setEmpCount(empEntity.getCount());
					  		empInfoEntity.setFullName(empEntity.getFullName());
					      	empInfoEntity.setCellNumber(empEntity.getCellNumber1());
					      	empInfoEntity.setDesignation(empEntity.getDesignation());
					      	empInfoEntity.setDepartment(empEntity.getDepartMent());
					      	empInfoEntity.setEmployeeType(empEntity.getEmployeeType());
					      	empInfoEntity.setEmployeerole(empEntity.getRoleName());
					      	empInfoEntity.setBranch(empEntity.getBranchName());
					      	empInfoEntity.setCountry(empEntity.getCountry());		
					      	empInfoEntityList.add(empInfoEntity);
							schedule.getEmptable().getDataprovider().setList(empInfoEntityList);

						}
					}
				
					schedule.getOlbEmployee().setSelectedIndex(0);
					}
					for(SuperModel model :result1){
						ServiceProject project = (ServiceProject) model;//komal changed model to smodel
						if(project.getTechnicians() != null){
							schedule.getEmptable().getDataprovider().setList(project.getTechnicians());
						}
						if(project.getTooltable() != null){
							schedule.getToolTable().getDataprovider().setList(project.getTooltable());
						}
						if(project.getProdDetailsList() != null && project.getProdDetailsList().size() > 0){
							schedule.getMaterialProductTable().getDataprovider().setList(project.getProdDetailsList());
						}
						CustomerServiceMaterialTable.projectId=project.getCount();
						}
					/** date 19.09.2019 commented by komal as discussion with devsh he dont want material from BOM**/
//					if(schedule.getMaterialProductTable().getDataprovider().getList().size() == 0){
//					if(serviceObject.getServiceProductList() != null && serviceObject.getServiceProductList().size() > 0){
//						ArrayList<ProductGroupList> list = new ArrayList<ProductGroupList>();
//						if(serviceObject.getServiceProductList() != null){
//						for(ProductGroupList  productGroupList : serviceObject.getServiceProductList()){
//							ProductGroupList technicianMaterialIfno  = new ProductGroupList();
//							technicianMaterialIfno.setName(productGroupList.getName());
//							
//								technicianMaterialIfno.setQuantity(productGroupList.getPlannedQty());
//								technicianMaterialIfno.setPlannedQty(productGroupList.getPlannedQty());
//								technicianMaterialIfno.setProActualQty(productGroupList.getPlannedQty());
//							
//						//	if(schedule.getOlbWarehouseName().getSelectedIndex()!=0)
//								technicianMaterialIfno.setWarehouse(schedule.getOlbWarehouseName().getValue(schedule.getOlbWarehouseName().getSelectedIndex()));
//						//	if(schedule.getOlbStorageLocation().getSelectedIndex()!=0)
//								technicianMaterialIfno.setStorageLocation(schedule.getOlbStorageLocation().getValue(schedule.getOlbStorageLocation().getSelectedIndex()));	
//						//	if(schedule.getOlbstorageBin().getSelectedIndex()!=0)
//								technicianMaterialIfno.setStorageBin(schedule.getOlbstorageBin().getValue(schedule.getOlbstorageBin().getSelectedIndex()));	
//
//						//	if(schedule.getOlbParentWarehouseName().getSelectedIndex()!=0)
//								technicianMaterialIfno.setParentWarentwarehouse(schedule.getOlbParentWarehouseName().getValue(schedule.getOlbParentWarehouseName().getSelectedIndex()));
//						//	if(schedule.getOlbParentStorageLocation().getSelectedIndex()!=0)
//								technicianMaterialIfno.setParentStorageLocation(schedule.getOlbParentStorageLocation().getValue(schedule.getOlbParentStorageLocation().getSelectedIndex()));	
//						//	if(schedule.getOlbParentstorageBin().getSelectedIndex()!=0)
//								technicianMaterialIfno.setParentStorageBin(schedule.getOlbParentstorageBin().getValue(schedule.getOlbParentstorageBin().getSelectedIndex()));	
//							
//							technicianMaterialIfno.setProduct_id(productGroupList.getProduct_id());
//							technicianMaterialIfno.setCode(productGroupList.getCode());
//							technicianMaterialIfno.setUnit(productGroupList.getUnit());
//							/** date 22.03.2019 added by komal to store price in project **/
//							Console.log("price : " + (productGroupList.getPrice()) / productGroupList.getPlannedQty());
//							technicianMaterialIfno.setPrice(productGroupList.getPrice());
//						
//							
//							list.add(technicianMaterialIfno);
//							
//						}
//						schedule.getMaterialProductTable().getDataprovider().setList(list);
//						}
//						
//					}
//					}
					schedule.getDateBox().setValue(model.getServiceDate());
					//setValue(model.getServiceTime(),planPopUp.servicetime);
					
					setpopupServiceTime(model.getServiceTime() , schedule);			
					schedule.setService(model);
					if(flag){
						technicianServiceCompletePopup = schedule;
					}else{
						technicianServiceSchedulePopup = schedule;
					}
					//schedule.showPopUp();
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					
				}
			});
			
		}
		@Override
		public void onFailure(Throwable caught) {
			// TODO Auto-generated method stub
			alert.alert("Unexpected error occurred");
		}
	});
	 
}
private void addEmployee(final TechnicianServiceSchedulePopup schedule){
	 String emp = "";
	if(ValidateTechnician(schedule)){
		
		Employee empEntity=schedule.getOlbEmployee().getSelectedItem();
		logger.log(Level.SEVERE , "Employee " + schedule.getOlbEmployee().getSelectedIndex());

		if(schedule.getOlbEmployee().getSelectedIndex()!=0){
			/** date 15/11/2017 added by komal to add employee only once **/
		    List<EmployeeInfo> empList = schedule.getEmptable().getDataprovider().getList();
		    for(EmployeeInfo empInfo : empList){
		    	if(empEntity.getCount() == empInfo.getEmpCount()){
		    		alert.alert("Technician is already present in Table..Please select different one");
		    		return;
		    	}
		    }
	  		EmployeeInfo empInfoEntity = new EmployeeInfo();
	  		
	  		empInfoEntity.setEmpCount(empEntity.getCount());
	  		empInfoEntity.setFullName(empEntity.getFullName());
	      	empInfoEntity.setCellNumber(empEntity.getCellNumber1());
	      	empInfoEntity.setDesignation(empEntity.getDesignation());
	      	empInfoEntity.setDepartment(empEntity.getDepartMent());
	      	empInfoEntity.setEmployeeType(empEntity.getEmployeeType());
	      	empInfoEntity.setEmployeerole(empEntity.getRoleName());
	      	empInfoEntity.setBranch(empEntity.getBranchName());
	      	empInfoEntity.setCountry(empEntity.getCountry());
	      	
	      	schedule.getEmptable().getDataprovider().getList().add(empInfoEntity);
		}
		else if(schedule.getOlbTeam().getSelectedIndex()!=0){
			ArrayList<EmployeeInfo> list = new ArrayList<EmployeeInfo>();
			TeamManagement teamManangementEntity = schedule.getOlbTeam().getSelectedItem();
			logger.log(Level.SEVERE , "Team management " + teamManangementEntity.getTeamList().size());
			List<Employee> empList = schedule.getOlbEmployee().getItems();
			for(int i=0;i<teamManangementEntity.getTeamList().size();i++){
				
				EmployeeInfo empInfoEntity = new EmployeeInfo();
		  		empInfoEntity.setFullName(teamManangementEntity.getTeamList().get(i).getEmployeeName());
		        empInfoEntity.setCellNumber(teamManangementEntity.getTeamList().get(i).getPhoneNumber());
		        empInfoEntity.setEmpCount(teamManangementEntity.getTeamList().get(i).getCustomerCount());
		        if(teamManangementEntity.getTeamList().get(i).getEmpDesignation()!=null)
		        empInfoEntity.setDesignation(teamManangementEntity.getTeamList().get(i).getEmpDesignation());
		  		logger.log(Level.SEVERE , "employee "+  teamManangementEntity.getTeamList().get(i).getEmployeeName());
		      	list.add(empInfoEntity);
			}
			
			schedule.getEmptable().getDataprovider().setList(list);
			
		}
		if(schedule.getEmptable().getDataprovider().getList().size() > 0){
			emp = schedule.getEmptable().getDataprovider().getList().get(0).getFullName();
		}
		final String employee = emp;
		if(!serviceObject.getEmployee().equalsIgnoreCase(employee)){
		MyQuerry querry1 = new MyQuerry();
		 Vector<Filter> filterVec = new Vector<Filter>();
		 
		 Filter filter =null;
		 filter=new Filter();
		 filter.setStringValue(emp);
		 filter.setQuerryString("wareHouseList.technicianName");
		 filterVec.add(filter);
		 
		 querry1.setFilters(filterVec);
		 querry1.setQuerryObject(new TechnicianWareHouseDetailsList());
		
		 service.getSearchResult(querry1, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				TechnicianWareHouseDetailsList technicianDetails = null;
				for(SuperModel model : result){
					technicianDetails = (TechnicianWareHouseDetailsList) model;
					break;
				}
				String warehouseName = "" ,storageLocation = "", storageBin = "",
						parentWareHouse = "" , parentStorageLocation = "" , parentStorageBin = "";
				
				if(technicianDetails != null){
					
					for(TechnicianWarehouseDetails details : technicianDetails.getWareHouseList()){
						if(details.getTechnicianName().equalsIgnoreCase(employee)){
							warehouseName = details.getTechnicianWareHouse();
							storageLocation = details.getTechnicianStorageLocation();
							schedule.getOlbStorageLocation().addItem(storageLocation);
							storageBin = details.getTechnicianStorageBin();
							schedule.getOlbstorageBin().addItem(storageBin);
							parentWareHouse = details.getParentWareHouse();
							parentStorageLocation = details.getParentStorageLocation();
							schedule.getOlbStorageLocation().addItem(parentStorageLocation);
							parentStorageBin = details.getPearentStorageBin();
							schedule.getOlbParentstorageBin().addItem(parentStorageBin);
							
							schedule.getOlbParentstorageBin().setValue(parentStorageBin);
							schedule.getOlbParentWarehouseName().setValue(parentWareHouse);
							schedule.getOlbParentStorageLocation().setValue(parentStorageLocation);
							schedule.getOlbStorageLocation().setValue(storageLocation);
							schedule.getOlbstorageBin().setValue(storageBin);
							schedule.getOlbWarehouseName().setValue(warehouseName);
							List<ProductGroupList> list = new ArrayList<ProductGroupList>();
							if(serviceObject.getServiceProductList() != null){
								for(ProductGroupList  productGroupList : serviceObject.getServiceProductList()){
									ProductGroupList technicianMaterialIfno  = new ProductGroupList();
									technicianMaterialIfno.setName(productGroupList.getName());
									
										technicianMaterialIfno.setQuantity(productGroupList.getPlannedQty());
										technicianMaterialIfno.setPlannedQty(productGroupList.getPlannedQty());
										technicianMaterialIfno.setProActualQty(productGroupList.getPlannedQty());
									
								//	if(schedule.getOlbWarehouseName().getSelectedIndex()!=0)
										technicianMaterialIfno.setWarehouse(schedule.getOlbWarehouseName().getValue(schedule.getOlbWarehouseName().getSelectedIndex()));
								//	if(schedule.getOlbStorageLocation().getSelectedIndex()!=0)
										technicianMaterialIfno.setStorageLocation(schedule.getOlbStorageLocation().getValue(schedule.getOlbStorageLocation().getSelectedIndex()));	
								//	if(schedule.getOlbstorageBin().getSelectedIndex()!=0)
										technicianMaterialIfno.setStorageBin(schedule.getOlbstorageBin().getValue(schedule.getOlbstorageBin().getSelectedIndex()));	

								//	if(schedule.getOlbParentWarehouseName().getSelectedIndex()!=0)
										technicianMaterialIfno.setParentWarentwarehouse(schedule.getOlbParentWarehouseName().getValue(schedule.getOlbParentWarehouseName().getSelectedIndex()));
								//	if(schedule.getOlbParentStorageLocation().getSelectedIndex()!=0)
										technicianMaterialIfno.setParentStorageLocation(schedule.getOlbParentStorageLocation().getValue(schedule.getOlbParentStorageLocation().getSelectedIndex()));	
								//	if(schedule.getOlbParentstorageBin().getSelectedIndex()!=0)
										technicianMaterialIfno.setParentStorageBin(schedule.getOlbParentstorageBin().getValue(schedule.getOlbParentstorageBin().getSelectedIndex()));	
									
									technicianMaterialIfno.setProduct_id(productGroupList.getProduct_id());
									technicianMaterialIfno.setCode(productGroupList.getCode());
									technicianMaterialIfno.setUnit(productGroupList.getUnit());
									list.add(technicianMaterialIfno);
									
								}
								schedule.getMaterialProductTable().getDataprovider().setList(list);
								}
							break;
						}
					}
				}
				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
		}
	}	
}
private boolean ValidatePopupData(TechnicianServiceSchedulePopup popup) {
	GWTCAlert alert = new GWTCAlert();
	boolean validate = AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "MakeTechnicianNonMandatory");
	
	if(technicianServiceSchedulePopup.getEmptable().getDataprovider().getList().size()==0 && !validate){
		alert.alert("Please add Technician.");
		technicianServiceSchedulePopup.getLblOk().setVisible(true);
		return false;
	}

	/**Date 24-10-2020 by amol **/
	if(technicianServiceSchedulePopup.getP_ampm().getSelectedIndex()==0){
		alert.alert("Please select AM/PM !");
		technicianServiceSchedulePopup.getLblOk().setVisible(true);
		return false;
	}
	
	
	if(technicianServiceSchedulePopup.getDateBox().getValue()==null){
		alert.alert("Service Date can not be blank.");
		technicianServiceSchedulePopup.getLblOk().setVisible(true);
		return false;
	}
	
	return true;
}

/** date 15.03.2019 added by komal - technician warehouse new schedule popup ok  button code **/
private void reactOnTechnicianPopupOk(final TechnicianServiceSchedulePopup technicianServiceSchedulePopup){

	
	if(ValidatePopupData(technicianServiceSchedulePopup)){
		
//		final ArrayList<EmployeeInfo> technicianlist = new ArrayList<EmployeeInfo>();
//		technicianlist.addAll(planPopUp.technicianTable.getDataprovider().getList());
		final ArrayList<Service> list = new ArrayList<Service>();
		list.addAll(getDataprovider().getList());

		final Service serObj = list.get(rowIndex);

		final ArrayList<EmployeeInfo> emplist = new ArrayList<EmployeeInfo>();
		emplist.addAll(technicianServiceSchedulePopup.getEmptable().getDataprovider()
				.getList());
		serObj.setTechnicians(emplist);
		final String technicianName = getTechnicianName(technicianServiceSchedulePopup.getEmptable().getDataprovider().getList());

		final ArrayList<CompanyAsset> technicianToollist = new ArrayList<CompanyAsset>();
		technicianToollist.addAll(technicianServiceSchedulePopup.getToolTable().getDataprovider().getList());
		
		final ArrayList<ProductGroupList> materialInfo = new ArrayList<ProductGroupList>();
		List<ProductGroupList> list1  = new ArrayList<ProductGroupList>();
		for(ProductGroupList  productGroupList : technicianServiceSchedulePopup.getMaterialProductTable().getDataprovider().getList()){
			ProductGroupList technicianMaterialIfno  = new ProductGroupList();
			technicianMaterialIfno.setName(productGroupList.getName());
			
				technicianMaterialIfno.setQuantity(productGroupList.getQuantity());
				technicianMaterialIfno.setPlannedQty(productGroupList.getPlannedQty());
				technicianMaterialIfno.setProActualQty(productGroupList.getProActualQty());
			
		//	if(schedule.getOlbWarehouseName().getSelectedIndex()!=0)
				technicianMaterialIfno.setWarehouse(technicianServiceSchedulePopup.getOlbWarehouseName().getValue(technicianServiceSchedulePopup.getOlbWarehouseName().getSelectedIndex()));
		//	if(schedule.getOlbStorageLocation().getSelectedIndex()!=0)
				technicianMaterialIfno.setStorageLocation(technicianServiceSchedulePopup.getOlbStorageLocation().getValue(technicianServiceSchedulePopup.getOlbStorageLocation().getSelectedIndex()));	
		//	if(schedule.getOlbstorageBin().getSelectedIndex()!=0)
				technicianMaterialIfno.setStorageBin(technicianServiceSchedulePopup.getOlbstorageBin().getValue(technicianServiceSchedulePopup.getOlbstorageBin().getSelectedIndex()));	

		//	if(schedule.getOlbParentWarehouseName().getSelectedIndex()!=0)
				technicianMaterialIfno.setParentWarentwarehouse(technicianServiceSchedulePopup.getOlbParentWarehouseName().getValue(technicianServiceSchedulePopup.getOlbParentWarehouseName().getSelectedIndex()));
		//	if(schedule.getOlbParentStorageLocation().getSelectedIndex()!=0)
				technicianMaterialIfno.setParentStorageLocation(technicianServiceSchedulePopup.getOlbParentStorageLocation().getValue(technicianServiceSchedulePopup.getOlbParentStorageLocation().getSelectedIndex()));	
		//	if(schedule.getOlbParentstorageBin().getSelectedIndex()!=0)
				technicianMaterialIfno.setParentStorageBin(technicianServiceSchedulePopup.getOlbParentstorageBin().getValue(technicianServiceSchedulePopup.getOlbParentstorageBin().getSelectedIndex()));	
			
			technicianMaterialIfno.setProduct_id(productGroupList.getProduct_id());
			technicianMaterialIfno.setCode(productGroupList.getCode());
			technicianMaterialIfno.setUnit(productGroupList.getUnit());
			/** date 22.03.2019 added by komal to store price in project **/
			Console.log("price : " + (productGroupList.getPrice()) / productGroupList.getPlannedQty());
			technicianMaterialIfno.setPrice(productGroupList.getPrice());
		
			
			list1.add(technicianMaterialIfno);
			
		}
		technicianServiceSchedulePopup.getMaterialProductTable().getDataprovider().setList(list1);
	
		
		materialInfo.addAll(technicianServiceSchedulePopup.getMaterialProductTable().getDataprovider().getList());
		
		//selectedRecord.setAttribute("ServiceDate", planPopUp.dbserviceDate.getValueAsDate());
		
		String serviceHrs=technicianServiceSchedulePopup.getP_servicehours().getItemText(technicianServiceSchedulePopup.getP_servicehours().getSelectedIndex());
		String serviceMin=technicianServiceSchedulePopup.getP_servicemin().getItemText(technicianServiceSchedulePopup.getP_servicemin().getSelectedIndex());
		String serviceAmPm=technicianServiceSchedulePopup.getP_ampm().getItemText(technicianServiceSchedulePopup.getP_ampm().getSelectedIndex());
		String totalTime = "";
		final String calculatedServiceTime;
		if(serviceHrs.equals("--")){
			serviceHrs = "12";
		}
		if(serviceMin.equals("--")){
			serviceMin = "00";
		}
		if(serviceAmPm.equals("--")){
			serviceAmPm = "AM";
		}
		totalTime = serviceHrs+":"+serviceMin+""+serviceAmPm;
		if(totalTime.equalsIgnoreCase("--:----") || totalTime.equalsIgnoreCase("--:--AM") ){
			calculatedServiceTime = "Flexible";
		}else{
			calculatedServiceTime = totalTime;
		}
		System.out.println("calculatedServiceTime ="+calculatedServiceTime);
		//selectedRecord.setAttribute("ServiceTime", calculatedServiceTime);
		

		final int InvoiceId ;
		if(technicianServiceSchedulePopup.getOlbInvoiceId().getSelectedIndex()!=0){
			InvoiceId  = Integer.parseInt(technicianServiceSchedulePopup.getOlbInvoiceId().getValue(technicianServiceSchedulePopup.getOlbInvoiceId().getSelectedIndex()));
		}else{
			InvoiceId = 0;
		}
		final String invoiceDate;
		if(technicianServiceSchedulePopup.getOlbInvoiceDate().getSelectedIndex()!=0){
			invoiceDate = technicianServiceSchedulePopup.getOlbInvoiceDate().getValue(technicianServiceSchedulePopup.getOlbInvoiceDate().getSelectedIndex());
         }else{
        	 invoiceDate = null;
         }
		final int projectId = serObj.getProjectId();
		System.out.println("Project Id=="+projectId);
		ArrayList<String> statuslist = new ArrayList<String>();
			statuslist.add("Scheduled");
			statuslist.add("Rescheduled");

			ArrayList<String> strEmployeelist = new ArrayList<String>();
			for(EmployeeInfo emp : emplist){
				strEmployeelist.add(emp.getFullName());
			}
			
			Date date = technicianServiceSchedulePopup.getDateBox().getValue();
	
			Vector<Filter> filtervec=new Vector<Filter>();
			Filter filter = null;
			MyQuerry querry;
			
			filter = new Filter();
			filter.setQuerryString("companyId");
			filter.setLongValue(UserConfiguration.getCompanyId());
			filtervec.add(filter);
			
			filter = new Filter();
			filter.setQuerryString("status IN");
			filter.setList(statuslist);
			filtervec.add(filter);
			
			filter = new Filter();
			filter.setQuerryString("employee IN");
			filter.setList(strEmployeelist);
			filtervec.add(filter);
			
			filter = new Filter();
			filter.setQuerryString("serviceDate");
			filter.setDateValue(date);
			filtervec.add(filter);
			
			querry=new MyQuerry();
			querry.setQuerryObject(new Service());
			querry.setFilters(filtervec);
			
			service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					System.out.println("RESULT SIZE : "+result.size());	
					//technicianServiceSchedulePopup.getLblOk().setVisible(true);
					label : for(SuperModel model:result){
						Service customerEntity=(Service) model;	
						if(serObj.getCount() != customerEntity.getCount()){
						if(!(calculatedServiceTime.equalsIgnoreCase("Flexible")))
						{
						for(EmployeeInfo emp : emplist){
							System.out.println("All values komal 1:" + emp.getFullName() +" " +customerEntity.getEmployee() +" "+format1.format(technicianServiceSchedulePopup.getDateBox().getValue())+" "+format1.format(customerEntity.getServiceDate())  +" " +calculatedServiceTime +" " +customerEntity.getServiceTime());
							
							if(emp.getFullName().equals(customerEntity.getEmployee()) && format1.format(technicianServiceSchedulePopup.getDateBox().getValue()).equals(format1.format(customerEntity.getServiceDate())) && calculatedServiceTime.equals(customerEntity.getServiceTime())){
								System.out.println("All values komal 2:" + emp.getFullName() +" " +customerEntity.getEmployee() +" "+format1.format(technicianServiceSchedulePopup.getDateBox().getValue())+" "+format1.format(customerEntity.getServiceDate()) +" " +calculatedServiceTime +" " +customerEntity.getServiceTime());
								validateflag = false;
								break label;
								
							}
							}
						}
					  }
					}
					
					System.out.println("Flag value :" + validateflag);
					if (validateflag == false) {
						technicianServiceSchedulePopup.getLblOk().setVisible(true);
						alert.alert("Selected technician is already assigned to other service at inserted date and time");
						validateflag = true;
						} else {
							
							genservice.TechnicianSchedulingPlan(serObj,emplist,technicianToollist,materialInfo,technicianServiceSchedulePopup.getDateBox().getValue(),calculatedServiceTime,InvoiceId,invoiceDate,technicianName,projectId, new AsyncCallback<Integer>() {
						
						@Override
						public void onSuccess(Integer result) {
							// TODO Auto-generated method stub
							System.out.println("iddddiiddd - - "+result);
							//selectedRecord.setAttribute("ProjectId", result);
							//grid.redraw();
							if(!calculatedServiceTime.equals(""))
								serObj.setServiceTime(calculatedServiceTime);
								if(result!=0)
									serObj.setProjectId(result);
								
								if(InvoiceId!=0){
									serObj.setInvoiceId(InvoiceId);
								}
																		
								if(!technicianName.equals("")){
									serObj.setEmployee(technicianName);
								}
								if(materialInfo != null && materialInfo.size() > 0){
									serObj.setMaterialAdded(true);
								}
                            serviceObject = serObj;
							RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
							table.redrawRow(rowIndex);
							technicianServiceSchedulePopup.getLblOk().setVisible(true);
							alert.alert("Saved Succesfully");
							technicianServiceSchedulePopup.hidePopUp();
							try{
								
								for(EmployeeInfo emp : emplist){
									/**
									  * Rahul Verma added below for notification on 28th Sept 2018
									  * For Reschedule
									  */
									 NotificationServiceAsync asyncService=GWT.create(NotificationService.class);
//									 if(form.tbServiceEngg.getSelectedIndex()!=0){
									 
										 String message="Service Id: "+serObj.getCount()+" is assigned to you.";
										 Console.log("message"+message);
//										 Console.log("form.tbServiceEngg.getValue(form.tbServiceEngg.getSelectedIndex())"+form.tbServiceEngg.getValue(form.tbServiceEngg.getSelectedIndex()));
										 
										 asyncService.sendNotification(serObj.getCompanyId(), emp.getFullName(), AppConstants.IAndroid.EVA_PEDIO,message, new AsyncCallback<ArrayList<Integer>>() {

											@Override
											public void onFailure(Throwable caught) {
												// TODO Auto-generated method stub
												
											}

											@Override
											public void onSuccess(ArrayList<Integer> result) {
												// TODO Auto-generated method stub
												Console.log("message"+result);
//												int resultValue=result.get(0);
//												if(resultValue==1){
//													form.showDialogMessage("Successful to send notification to technician");
//												}else if(resultValue==2){
//													form.showDialogMessage("Device not register for "+form.tbServiceEngg.getValue(form.tbServiceEngg.getSelectedIndex()));
//												}else if(resultValue==3){
//													form.showDialogMessage("No Username defined for "+form.tbServiceEngg.getValue(form.tbServiceEngg.getSelectedIndex()));
//												}else if(resultValue==0){
//													form.showDialogMessage("Notification not sent to EVA Pedio");
//												}
											}
										});
//									 }
									 /**
									  * Ends for Rahul
									  */
								}
								}catch(Exception e){
									e.printStackTrace();
								}
						}
		
						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							technicianServiceSchedulePopup.hidePopUp();
							
						}
					});
					
				}
				}
				
				@Override
				public void onFailure(Throwable caught) {
					technicianServiceSchedulePopup.getLblOk().setVisible(true);
					System.out.println("Failed");
					Console.log("RPC Failed");
				}
			});					
	}				

}
private boolean ValidateTechnician(TechnicianServiceSchedulePopup popup) {

	if(popup.getOlbEmployee().getSelectedIndex()==0  && popup.getOlbTeam().getSelectedIndex()==0){
		alert.alert("Please Select either Team or Technician");
		return false;
	}
	else if(popup.getOlbEmployee().getSelectedIndex()!=0 && popup.getOlbTeam().getSelectedIndex()!=0){
		alert.alert("Please Select either Team or Technician");
		return false;
	}	
	return true;
}

/** date 16.3.2019 added by komal for new technician service schedule popup */
private void reactOnServiceCompletionPopUp(ClickEvent event , TechnicianServiceSchedulePopup schedule) {

	
	if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "OnlyForNBHC")){
		
			if(schedule.getMarkcompletepopup().getServiceTime().getValue()!=null){
				if(schedule.getMarkcompletepopup().getServiceTime().getValue()!=0){
					
				
					String formTime = schedule.getMarkcompletepopup().getP_servicehours().getItemText(schedule.getMarkcompletepopup().getP_servicehours().getSelectedIndex())+":"+
							schedule.getMarkcompletepopup().getP_servicemin().getItemText(schedule.getMarkcompletepopup().getP_servicemin().getSelectedIndex())+":"+
							schedule.getMarkcompletepopup().getP_ampm().getItemText(schedule.getMarkcompletepopup().getP_ampm().getSelectedIndex());
					
					System.out.println("formTime form popup "+formTime);
					
					String toTime = schedule.getMarkcompletepopup().getTo_servicehours().getItemText(schedule.getMarkcompletepopup().getTo_servicehours().getSelectedIndex())+":"+
							schedule.getMarkcompletepopup().getTo_servicemin().getItemText(schedule.getMarkcompletepopup().getTo_servicemin().getSelectedIndex())+":"+
							schedule.getMarkcompletepopup().getTo_ampm().getItemText(schedule.getMarkcompletepopup().getTo_ampm().getSelectedIndex());
					System.out.println("toTime form popup "+toTime);
					
				/**
				 * Added By Rahul Verma
				 * This method is only for nbhc. and it will check whether there stock current status is>0 and they have service or not.
				 * 
				 */
				if(serviceObject.getEmployee().trim().equalsIgnoreCase("Snehal Abhishek Palav") && serviceObject.getStackDetailsList().size()!=0){
					createExtraServiceIfStackZero(serviceObject);
				}
				/**
				 * ends here 
				 */
				String  rating = null;
				if(schedule.getMarkcompletepopup().getLbRating().getSelectedIndex() != 0){
					rating = schedule.getMarkcompletepopup().getLbRating().getValue(schedule.getMarkcompletepopup().getLbRating().getSelectedIndex());
				}
				reactOnComplete(schedule.getMarkcompletepopup().getServiceCompletionDate().getValue(),formTime,toTime,
						schedule.getMarkcompletepopup().getRemark().getValue() , schedule.getMarkcompletepopup().getServiceTime().getValue()+"" , rating , true);

				
				// ********vaishnavi****************
				createProjectonMarkComplete();
			}else{
				alert.alert("Please add Service Duration (In Minutes)");
			}
	}else{
		alert.alert("Service Duration (In Minutes) is mandetory..!");
	}
}
else
{

		String formTime = schedule.getMarkcompletepopup().getP_servicehours().getItemText(schedule.getMarkcompletepopup().getP_servicehours().getSelectedIndex())+":"+
				schedule.getMarkcompletepopup().getP_servicemin().getItemText(schedule.getMarkcompletepopup().getP_servicemin().getSelectedIndex())+":"+
				schedule.getMarkcompletepopup().getP_ampm().getItemText(schedule.getMarkcompletepopup().getP_ampm().getSelectedIndex());
		
		System.out.println("formTime form popup "+formTime);
		
		String toTime = schedule.getMarkcompletepopup().getTo_servicehours().getItemText(schedule.getMarkcompletepopup().getTo_servicehours().getSelectedIndex())+":"+
				schedule.getMarkcompletepopup().getTo_servicemin().getItemText(schedule.getMarkcompletepopup().getTo_servicemin().getSelectedIndex())+":"+
				schedule.getMarkcompletepopup().getTo_ampm().getItemText(schedule.getMarkcompletepopup().getTo_ampm().getSelectedIndex());
		System.out.println("toTime form popup "+toTime);

	if(serviceObject.getEmployee().trim().equalsIgnoreCase("Snehal Abhishek Palav") && serviceObject.getStackDetailsList().size()!=0){
		createExtraServiceIfStackZero(serviceObject);
	}

	String  rating = null;
	if(schedule.getMarkcompletepopup().getLbRating().getSelectedIndex() != 0){
		rating = schedule.getMarkcompletepopup().getLbRating().getValue(schedule.getMarkcompletepopup().getLbRating().getSelectedIndex());
	}
	reactOnComplete(schedule.getMarkcompletepopup().getServiceCompletionDate().getValue(),formTime,toTime,
			schedule.getMarkcompletepopup().getRemark().getValue() , schedule.getMarkcompletepopup().getServiceTime().getValue()+"" , rating , true);

	
	// ********vaishnavi****************
	createProjectonMarkComplete();
	
}


}
public void manageProcessLevelBar(final String sucesssMessage,
		final String failureMessage,
		final String statusText,final ModificationHistory history,
		final Date ServiceDate,final String serviceTime,final String serviceBranch,final TechnicianServiceSchedulePopup schedule)
{
	ArrayList<ProductGroupList> materialList = new ArrayList<ProductGroupList>();
	materialList.addAll(schedule.getMaterialProductTable().getDataprovider().getList());
	String status = serviceObject.getStatus();
	serviceObject.setStatus(statusText);
	genservice.saveServiceProjectStatus(serviceObject, new AsyncCallback<String>() {
		
		@Override
		public void onSuccess(String result) {
			// TODO Auto-generated method stub
			alert.alert(sucesssMessage);
			RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
			table.redrawRow(rowIndex);
			schedule.hidePopUp();
			/**
			 * nidhi 17-04-2018
			 * for hide pop up
			 */
			schedule.getPanel().hide();
            popupPanel.hide();
			sendSMSLogic();
		}
		
		@Override
		public void onFailure(Throwable caught) {
			// TODO Auto-generated method stub
			
		}
	});
	
	

	// MIN Creating 
	
	Console.log("Before Calling MIN  ");
		if(!(status.equals("Completed"))){
		int projectId = serviceObject.getProjectId();
		System.out.println("project Id =="+projectId);
			servicelistservice.TechnicianSchedulingListMarkcompleteservice(serviceObject,LoginPresenter.loggedInUser,projectId,materialList, new AsyncCallback<ArrayList<Integer>>() {
			
			@Override
			public void onSuccess(ArrayList<Integer> result) {
				// TODO Auto-generated method stub
				Console.log("MIN on success");
				if(result.contains(2)){
					//getDataprovider().getList().remove(serviceObject);
					alert.alert("Service Completed Sucessfully");
					table.redraw();
					
				}				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				alert.alert("An Unexpected error occurred!");

			}
		});
	}
	
}
private void setpopupServiceTime(String serviceTime , TechnicianServiceSchedulePopup schedule) {

	if(!serviceTime.equals("")&&!serviceTime.equals("Flexible")){
		
		String[] time=serviceTime.split(":");
		System.out.println(" 0 "+time[0]);
		System.out.println(" 1 "+time[1]);
		String hrs=time[0];
		String min;
		String ampm;
		if(time[1].contains("PM")){
			min=time[1].replace("PM", "");
			ampm="PM";
		}else{
			min=time[1].replace("AM", "");
			ampm="AM";
		}
		System.out.println(" 2 "+hrs+" "+min+" "+ampm);
		
		int count = schedule.getP_servicehours().getItemCount();
		String item=hrs.toString();
		System.out.println("Hours =="+item);
		for(int i=0;i<count;i++)
		{
			System.out.println("HTRRSSS =="+schedule.getP_servicehours().getItemText(i).trim());
			System.out.println(item.trim().equals(schedule.getP_servicehours().getItemText(i).trim()));
			if(item.trim().equals(schedule.getP_servicehours().getItemText(i).trim()))
			{
				schedule.getP_servicehours().setSelectedIndex(i);
				break;
			}
		}
		
		int count1 = schedule.getP_servicemin().getItemCount();
		String item1=min.toString();
		for(int i=0;i<count1;i++)
		{
			if(item1.trim().equals(schedule.getP_servicemin().getItemText(i).trim()))
			{
				schedule.getP_servicemin().setSelectedIndex(i);
				break;
			}
		}
		
		int count2 = schedule.getP_ampm().getItemCount();
		String item2=ampm.toString();
		for(int i=0;i<count2;i++)
		{
			if(item2.trim().equals(schedule.getP_ampm().getItemText(i).trim()))
			{
				schedule.getP_ampm().setSelectedIndex(i);
				break;
			}
		}
		
	}else{
		schedule.getP_servicehours().setSelectedIndex(0);
		schedule.getP_servicemin().setSelectedIndex(0);
		schedule.getP_ampm().setSelectedIndex(0);
	}
}
private void loadInvoiceDropDown(int contractId , final TechnicianServiceSchedulePopup schedule) {

	MyQuerry querry = new MyQuerry();
	Vector<Filter> filtervec=new Vector<Filter>();
	Filter filter =null;
	
	filter= new Filter();
	filter.setQuerryString("contractCount");
	filter.setIntValue(contractId);
	filtervec.add(filter);
	
	filter= new Filter();
	filter.setQuerryString("status");
	filter.setStringValue("Approved");
	filtervec.add(filter); 
	
	filter= new Filter();
	filter.setQuerryString("typeOfOrder");
	filter.setStringValue(AppConstants.ORDERTYPESERVICE);
	filtervec.add(filter);
	
	filter= new Filter();
	filter.setQuerryString("accountType");
	filter.setStringValue(AppConstants.BILLINGACCOUNTTYPEAR);
	filtervec.add(filter);
	
	querry.setFilters(filtervec);
	querry.setQuerryObject(new Invoice());
	
	schedule.getOlbInvoiceId().removeAllItems();
	schedule.getOlbInvoiceDate().removeAllItems();
	
	service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
		
		@Override
		public void onSuccess(ArrayList<SuperModel> result) {
			// TODO Auto-generated method stub
			ArrayList<Invoice> list = new ArrayList<Invoice>();
			Console.log("Invoice size=="+result.size());
			
			schedule.getOlbInvoiceId().addItem("--SELECT--");
			schedule.getOlbInvoiceDate().addItem("--SELECT--");
			for(SuperModel model : result){
				Invoice invoice = (Invoice) model;
				schedule.getOlbInvoiceId().addItem(invoice.getCount()+"");
				schedule.getOlbInvoiceDate().addItem(DateTimeFormat.getShortDateFormat().format(invoice.getInvoiceDate()));
				list.add(invoice);
			}
			schedule.getOlbInvoiceId().setItems(list);
			schedule.getOlbInvoiceDate().setItems(list);
			
//			System.out.println("INVOICE IDDDDD ="+selectedRecord.getAttributeAsInt("InvoiceId"));
//			System.out.println("INVOICE DATEEEE ="+selectedRecord.getAttributeAsDate("InvoiceDate"));

			int invoiceId = serviceObject.getInvoiceId();
			Date invoiceDate = serviceObject.getInvoiceDate();
			
			setpopUpInvoiceIdDate(invoiceId,invoiceDate);
		}
		@Override
		public void onFailure(Throwable caught) {
			// TODO Auto-generated method stub
			alert.alert("Unexpected Error");
		}
	});
}
private void createColumnScheduledStatus(){
	isserviceScheduled = new TextColumn<Service>() {
		
		@Override
		public String getValue(Service object) {
			// TODO Auto-generated method stub
			return object.isServiceScheduled()+"";
		}
	};
	
	//old name "Schedule Status"
	//Ashwini Patil Date:5-02-2024 changed label to "Visible to Technicia" 
	table.addColumn(isserviceScheduled, "Visible to Technician");
	table.setColumnWidth(isserviceScheduled, 150, Unit.PX);
	isserviceScheduled.setSortable(true);
}
private void addSortingIsServiceScheduled(){


	List<Service> list = getDataprovider().getList();
	columnSort = new ListHandler<Service>(list);
	columnSort.setComparator(isserviceScheduled, new Comparator<Service>() {

		@Override
		public int compare(Service e1, Service e2) {
			if (e1 != null && e2 != null) {
				if (e1.isServiceScheduled()+"" != null
						&& e2.isServiceScheduled()+"" != null) {
					return (e1.isServiceScheduled()+"").compareTo(
							(e2.isServiceScheduled()+""));
				}
			} else {
				return 0;
			}
			return 0;
		}
	});
	table.addColumnSortHandler(columnSort);


}
private void reactOnOKButton(final TechnicianServiceSchedulePopup popup){

	
	Company c = new Company();
	List<ProductGroupList> materilList = popup.getMaterialProductTable().getDataprovider().getList();
	if(materilList == null || materilList.size() == 0){
		reactOnTechnicianPopupOk(popup);
	}else{
//	ArrayList<Integer> integerList = new ArrayList<Integer>();
//	final Map<Integer , Double> map =  new HashMap<Integer , Double>();
//	for(ProductGroupList prodId : materilList){
//		integerList.add(prodId.getProduct_id());
//		map.put(prodId.getProduct_id() , prodId.getProActualQty());
//	};
	//final TechnicianServiceSchedulePopup popup = this;
//	genservice.getProductInventoryDetails(integerList, c.getCompanyId(), new AsyncCallback<ArrayList<SuperModel>>() {
//		
//		@Override
//		public void onSuccess(ArrayList<SuperModel> result) {
//			// TODO Auto-generated method stub
//		//	lblUpdate.setText("Schedule");
//			GWTCAlert alert = new GWTCAlert();
//			if(result!=null){
//			String transferToWarehouseName = popup.getOlbWarehouseName().getValue(popup.getOlbWarehouseName().getSelectedIndex());
//			String transferToLocationName = popup.getOlbStorageLocation().getValue(popup.getOlbStorageLocation().getSelectedIndex());
//			String transferToBinName = popup.getOlbstorageBin().getValue(popup.getOlbstorageBin().getSelectedIndex());
//			
//			String parentWarehouseName = popup.getOlbParentWarehouseName().getValue(popup.getOlbParentWarehouseName().getSelectedIndex());
//			String parentLocationName = popup.getOlbParentStorageLocation().getValue(popup.getOlbParentStorageLocation().getSelectedIndex());
//			String parentBinName = popup.getOlbParentstorageBin().getValue(popup.getOlbParentstorageBin().getSelectedIndex());
//
//			if(result.size()==0){
//				alert.alert("Please Define Product Inventory Master First!");
//				technicianServiceSchedulePopup.getLblOk().setVisible(true);
//				return;
//			}
//			
//			double availableQuantity = 0;
//			for(SuperModel model : result){
//				boolean flag = false;
//				boolean parentFlag = false;
//				ProductInventoryView prodInventory = (ProductInventoryView) model;
//				for(ProductInventoryViewDetails productDetails: prodInventory.getDetails()){
//					if(productDetails.getWarehousename().trim().equals(transferToWarehouseName.trim()) &&
//						productDetails.getStoragelocation().trim().equals(transferToLocationName.trim()) &&
//						productDetails.getStoragebin().trim().equals(transferToBinName.trim()) ){
//						flag =true;
//						
//					}
//					if(productDetails.getWarehousename().trim().equals(parentWarehouseName.trim()) &&
//							productDetails.getStoragelocation().trim().equals(parentLocationName.trim()) &&
//							productDetails.getStoragebin().trim().equals(parentBinName.trim()) ){
//						availableQuantity = productDetails.getAvailableqty();
//						if(result.size()!=0){
//							if (popup.validateQuantity(availableQuantity , map.get(productDetails.getProdid()))) {										
//								alert.alert("Insufficient available quantity in parent warehouse for product id : "+productDetails.getProdname());
//								technicianServiceSchedulePopup.getLblOk().setVisible(true);
//								return;
//							} 
//						}
//						parentFlag =true;
//					}
//				}
//				if(flag==false){
//					alert.alert("Please Define Product Inventory Master for TransferTo Warehouse!");
//					technicianServiceSchedulePopup.getLblOk().setVisible(true);
//					return;
//				}
//				if(parentFlag==false){
//					alert.alert("Please Define Product Inventory Master for Parent Warehouse!");
//					technicianServiceSchedulePopup.getLblOk().setVisible(true);
//					return;
//				}
//			}
//			
//			
//			
//		//	service.setServiceScheduled(true);
//			reactOnTechnicianPopupOk(popup);
//			}else{
//				alert.alert("Please Define Product Inventory Master for TransferTo Warehouse!");
//				technicianServiceSchedulePopup.getLblOk().setVisible(true);
//				return;
//			}
//			
//		}
//		
//		@Override
//		public void onFailure(Throwable caught) {
//			// TODO Auto-generated method stub
//			//lblUpdate.setText("Schedule");
//		}
//	});
	reactOnTechnicianPopupOk(popup);
}
}
public void addServiceBranchSorting() {
	List<Service> list = getDataprovider().getList();
	columnSort = new ListHandler<Service>(list);
	columnSort.setComparator(clickServicingBranch ,  new Comparator<Service>() {

		@Override
		public int compare(Service e1, Service e2) {
			if (e1 != null && e2 != null) {
				if (e1.getServiceBranch() != null && e2.getServiceBranch() != null) {
					return e1.getServiceBranch().compareTo(e2.getServiceBranch());
				}
			} else {
				return 0;
			}
			return 0;
		}
	});
	table.addColumnSortHandler(columnSort);

}

	/**
	 * @author Vijay Chougule Date 19-12-2019 for NBHC CCPM stack details with
	 *         process config
	 */
	private void addColumnCommodityName() {
		commodityName = new TextColumn<Service>() {

			@Override
			public String getValue(Service object) {
				if (object.getCommodityName() != null) {
					return object.getCommodityName();
				}
				return "NA";
			}
		};
		table.addColumn(commodityName, "CommodityName");
		table.setColumnWidth(commodityName, 120, Unit.PX);
		commodityName.setSortable(true);
	}

	private void addColumnWarehouseName() {
		warehouseName = new TextColumn<Service>() {
			
			@Override
			public String getValue(Service object) {
				if(object.getWareHouse()!=null){
					return object.getWareHouse();
				}
				return "NA";
			}
		};
		table.addColumn(warehouseName, "Warehouse Name");
		table.setColumnWidth(warehouseName, 130, Unit.PX);
		warehouseName.setSortable(true);
	}

	private void addColumnStackName() {
		stackName = new TextColumn<Service>() {
			
			@Override
			public String getValue(Service object) {
				if(object.getStackNo()!=null){
					return object.getStackNo();
				}
				return "NA";
			}
		};
		table.addColumn(stackName, "Stack Name");
		table.setColumnWidth(stackName, 80, Unit.PX);
		stackName.setSortable(true);
	}

	private void addColumnStackQty() {
		stackQty = new TextColumn<Service>() {
			
			@Override
			public String getValue(Service object) {
					return object.getStackQty()+"";
			}
		};
		table.addColumn(stackQty, "Stack Qty");
		table.setColumnWidth(stackQty, 80, Unit.PX);
		stackQty.setSortable(true);
	}
	
	private void addSortingWarehouseName() {
		List<Service> list = getDataprovider().getList();
		columnSort = new ListHandler<Service>(list);
		columnSort.setComparator(warehouseName ,  new Comparator<Service>() {

			@Override
			public int compare(Service e1, Service e2) {
				if (e1 != null && e2 != null) {
					if (e1.getWareHouse() != null && e2.getWareHouse() != null) {
						return e1.getWareHouse().compareTo(e2.getWareHouse());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);		
	}

	private void addSortingStackName() {
		List<Service> list = getDataprovider().getList();
		columnSort = new ListHandler<Service>(list);
		columnSort.setComparator(stackName ,  new Comparator<Service>() {

			@Override
			public int compare(Service e1, Service e2) {
				if (e1 != null && e2 != null) {
					if (e1.getStackNo() != null && e2.getStackNo() != null) {
						return e1.getStackNo().compareTo(e2.getStackNo());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);				
	}

	private void addSortingStackQty() {
		List<Service> list = getDataprovider().getList();
		columnSort = new ListHandler<Service>(list);
		columnSort.setComparator(stackQty, new Comparator<Service>() {

			@Override
			public int compare(Service e1, Service e2) {
				if (e1 != null && e2 != null) {
					if (e1.getStackQty() == e2.getStackQty()) {
						return 0;
					}
					if (e1.getStackQty() > e2.getStackQty()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	private void addSortingCommodityName() {

		List<Service> list = getDataprovider().getList();
		columnSort = new ListHandler<Service>(list);
		columnSort.setComparator(commodityName ,  new Comparator<Service>() {

			@Override
			public int compare(Service e1, Service e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCommodityName() != null && e2.getCommodityName() != null) {
						return e1.getCommodityName().compareTo(e2.getCommodityName());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);				
	
	}
	
	
	private void addColumnDeggasingServiceId() {
		degassingServiceId = new TextColumn<Service>() {
			
			@Override
			public String getValue(Service object) {
				if(object.getDeggassingServiceId()==0){
					return "NA";
				}
				else{
					return object.getDeggassingServiceId()+"";
				}
			}
		};
		table.addColumn(degassingServiceId, "Degassing Id");
		table.setColumnWidth(degassingServiceId, 90, Unit.PX);
		degassingServiceId.setSortable(true);
	}

	private void addSortingDegassingServiceId() {

		List<Service> list = getDataprovider().getList();
		columnSort = new ListHandler<Service>(list);
		columnSort.setComparator(degassingServiceId, new Comparator<Service>() {

			@Override
			public int compare(Service e1, Service e2) {
				if (e1 != null && e2 != null) {
					if (e1.getDeggassingServiceId() == e2.getDeggassingServiceId()) {
						return 0;
					}
					if (e1.getDeggassingServiceId() > e2.getDeggassingServiceId()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	
	}
	
	private void addColumnShadeName() {

		shadeName = new TextColumn<Service>() {
			
			@Override
			public String getValue(Service object) {
				if(object.getShade()!=null){
					return object.getShade();
				}
				return "NA";
			}
		};
		table.addColumn(shadeName, "Shed Name");
		table.setColumnWidth(shadeName, 80, Unit.PX);
		shadeName.setSortable(true);
	}
	
	private void addColumnWarehouseCode() {

		warehouseCode = new TextColumn<Service>() {
			
			@Override
			public String getValue(Service object) {
				if(object.getRefNo2()!=null){
					return object.getRefNo2();
				}
				return "NA";
			}
		};
		table.addColumn(warehouseCode, "Warehouse Code");
		table.setColumnWidth(warehouseCode, 220, Unit.PX);
		warehouseCode.setSortable(true);
	}
	
	private void addSortingWarehouseCode() {

		List<Service> list = getDataprovider().getList();
		columnSort = new ListHandler<Service>(list);
		columnSort.setComparator(warehouseCode ,  new Comparator<Service>() {

			@Override
			public int compare(Service e1, Service e2) {
				if (e1 != null && e2 != null) {
					if (e1.getRefNo2() != null && e2.getRefNo2() != null) {
						return e1.getRefNo2().compareTo(e2.getRefNo2());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);				
	
	}
	
	private void addSortingShadeName() {
		List<Service> list = getDataprovider().getList();
		columnSort = new ListHandler<Service>(list);
		columnSort.setComparator(shadeName ,  new Comparator<Service>() {

			@Override
			public int compare(Service e1, Service e2) {
				if (e1 != null && e2 != null) {
					if (e1.getShade() != null && e2.getShade() != null) {
						return e1.getShade().compareTo(e2.getShade());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);				
	
	}
	
	
	protected void addmfgDateCol() {
		mfgDateCol = new TextColumn<Service>() {
			@Override
			public String getValue(Service object) {
				if (object.getMfgDate() != null)
					return AppUtility.parseDate(object.getMfgDate());
				else
					return " ";
			}
		};
		table.addColumn(mfgDateCol, "Mfg. Date");
		table.setColumnWidth(mfgDateCol, 120, Unit.PX);
		mfgDateCol.setSortable(true);
	}
	protected void addreplacementDateCol() {
		replacementDateCol = new TextColumn<Service>() {
			@Override
			public String getValue(Service object) {
				if (object.getReplacementDate() != null)
					return AppUtility.parseDate(object.getReplacementDate());
				else
					return " ";
			}
		};
		table.addColumn(replacementDateCol, "Replacement Date");
		table.setColumnWidth(replacementDateCol, 120, Unit.PX);
		replacementDateCol.setSortable(true);
	}
	
	protected void addassetIdCol() {
		assetIdCol = new TextColumn<Service>() {
			@Override
			public String getValue(Service object) {
				if (object.getAssetId()!= 0)
					return object.getAssetId()+"";
				else
					return " ";
			}
		};
		table.addColumn(assetIdCol, "Asset Id");
		table.setColumnWidth(assetIdCol, 90, Unit.PX);
		assetIdCol.setSortable(true);
	}
	protected void addcomponentNameCol() {
		componentNameCol = new TextColumn<Service>() {
			@Override
			public String getValue(Service object) {
				if (object.getComponentName() != null)
					return object.getComponentName();
				else
					return " ";
			}
		};
		table.addColumn(componentNameCol, "Component Name");
		table.setColumnWidth(componentNameCol, 120, Unit.PX);
		componentNameCol.setSortable(true);
	}
	protected void addmfgNumCol() {
		mfgNumCol = new TextColumn<Service>() {
			@Override
			public String getValue(Service object) {
				if (object.getMfgNo() != null&&!object.getMfgNo().equals(null)&&!object.getMfgNo().equals("null"))
					return object.getMfgNo();
				else
					return " ";
			}
		};
		table.addColumn(mfgNumCol, "Mfg. Num");
		table.setColumnWidth(mfgNumCol, 120, Unit.PX);
		mfgNumCol.setSortable(true);
	}
	
	protected void addtierNameCol() {
		tierNameCol = new TextColumn<Service>() {
			@Override
			public String getValue(Service object) {
				if (object.getTierName() != null)
					return object.getTierName();
				else
					return " ";
			}
		};
		table.addColumn(tierNameCol, "Tier Name");
		table.setColumnWidth(tierNameCol, 90, Unit.PX);
		tierNameCol.setSortable(true);
	}
	
	protected void addUnitCol() {
		unitCol = new TextColumn<Service>() {
			@Override
			public String getValue(Service object) {
				if (object.getAssetUnit() != null)
					return object.getAssetUnit();
				else
					return " ";
			}
		};
		table.addColumn(unitCol, "Unit");
		table.setColumnWidth(unitCol, 120, Unit.PX);
		unitCol.setSortable(true);
	}
	
	
	private void addSortingOnAssetIdCol() {
		List<Service> list = getDataprovider().getList();
		columnSort = new ListHandler<Service>(list);
		columnSort.setComparator(assetIdCol, new Comparator<Service>() {
			@Override
			public int compare(Service e1, Service e2) {
				if (e1 != null && e2 != null) {
					if (e1.getAssetId() == e2.getAssetId()) {
						return 0;
					}
					if (e1.getAssetId() > e2.getAssetId()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	private void addSortingOnComponentName() {
		List<Service> list = getDataprovider().getList();
		columnSort = new ListHandler<Service>(list);
		columnSort.setComparator(componentNameCol ,  new Comparator<Service>() {
			@Override
			public int compare(Service e1, Service e2) {
				if (e1 != null && e2 != null) {
					if (e1.getComponentName() != null && e2.getComponentName() != null) {
						return e1.getComponentName().compareTo(e2.getComponentName());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);				
	
	}
	
	private void addSortingOnMfgNo() {
		List<Service> list = getDataprovider().getList();
		columnSort = new ListHandler<Service>(list);
		columnSort.setComparator(mfgNumCol ,  new Comparator<Service>() {
			@Override
			public int compare(Service e1, Service e2) {
				if (e1 != null && e2 != null) {
					if (e1.getMfgNo() != null && e2.getMfgNo() != null) {
						return e1.getMfgNo().compareTo(e2.getMfgNo());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);				
	}
	
	private void addSortingOnTierName() {
		List<Service> list = getDataprovider().getList();
		columnSort = new ListHandler<Service>(list);
		columnSort.setComparator(tierNameCol ,  new Comparator<Service>() {
			@Override
			public int compare(Service e1, Service e2) {
				if (e1 != null && e2 != null) {
					if (e1.getTierName() != null && e2.getTierName() != null) {
						return e1.getTierName().compareTo(e2.getTierName());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);				
	}
	
	public void addSortingOnMfgDate() {
		List<Service> list = getDataprovider().getList();
		columnSort = new ListHandler<Service>(list);
		columnSort.setComparator(mfgDateCol, new Comparator<Service>() {
			@Override
			public int compare(Service e1, Service e2) {
				if (e1 != null && e2 != null) {
					if (e1.getMfgDate() != null&& e2.getMfgDate() != null) {
						return e1.getMfgDate().compareTo(e2.getMfgDate());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	public void addSortingOnReplacementDate() {
		List<Service> list = getDataprovider().getList();
		columnSort = new ListHandler<Service>(list);
		columnSort.setComparator(replacementDateCol, new Comparator<Service>() {
			@Override
			public int compare(Service e1, Service e2) {
				if (e1 != null && e2 != null) {
					if (e1.getReplacementDate() != null&& e2.getReplacementDate() != null) {
						return e1.getReplacementDate().compareTo(e2.getReplacementDate());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	private void addSortingUnit() {
		List<Service> list = getDataprovider().getList();
		columnSort = new ListHandler<Service>(list);
		columnSort.setComparator(unitCol ,  new Comparator<Service>() {
			@Override
			public int compare(Service e1, Service e2) {
				if (e1 != null && e2 != null) {
					if (e1.getAssetUnit() != null && e2.getAssetUnit() != null) {
						return e1.getAssetUnit().compareTo(e2.getAssetUnit());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);				
	}
	
	private void addColumnServiceDuration() {
		serviceDurationCol = new TextColumn<Service>() {

			@Override
			public String getValue(Service object) {
				// TODO Auto-generated method stub
				if(object.getServiceDuration()!=0){
					return object.getServiceDuration()+"";
				}
				else{
					return "";
				}
			}
		};

		table.addColumn(serviceDurationCol, "Service Duration");
		table.setColumnWidth(serviceDurationCol, 120, Unit.PX);
		serviceDurationCol.setSortable(true);
			
	}
	
	private void addSortingServiceDuration() {
		List<Service> list = getDataprovider().getList();
		columnSort = new ListHandler<Service>(list);
		columnSort.setComparator(serviceDurationCol, new Comparator<Service>() {
			@Override
			public int compare(Service e1, Service e2) {
				if (e1 != null && e2 != null) {
					if (e1.getServiceDuration() == e2.getServiceDuration()) {
						return 0;
					}
					if (e1.getServiceDuration() > e2.getServiceDuration()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	
	protected void getUpdatedServiceData(final String sucesssMessage, final GWTCAlert alert2) {

		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		MyQuerry querry;
		
		filter = new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(serviceObject.getCount());
		filtervec.add(filter);
		
		querry=new MyQuerry();
		querry.setQuerryObject(new Service());
		querry.setFilters(filtervec);
		
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				for(SuperModel model : result ) {
					Service service = (Service) model;
					Console.log("service.isServiceScheduled() =="+service.isServiceScheduled());
					serviceObject.setServiceScheduled(service.isServiceScheduled());
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
					table.redrawRow(rowIndex);
					alert2.alert(sucesssMessage);
					panel.hide();	
					break;
				}

			}
			
			@Override
			public void onFailure(Throwable arg0) {
				// TODO Auto-generated method stub
				panel.hide();	
			}
		});
	}
	
	
	protected void processPlanData(final boolean completionFlag, final ClickEvent event) {

		if(ValidatePopupData()){
			
//			final ArrayList<EmployeeInfo> technicianlist = new ArrayList<EmployeeInfo>();
//			technicianlist.addAll(planPopUp.technicianTable.getDataprovider().getList());
			final ArrayList<Service> list = new ArrayList<Service>();
			list.addAll(getDataprovider().getList());

			final Service serObj = list.get(rowIndex);

			final ArrayList<EmployeeInfo> emplist = new ArrayList<EmployeeInfo>();
			emplist.addAll(schedule.getEmptable().getDataprovider()
					.getList());
			serObj.setTechnicians(emplist);
			final String technicianName = getTechnicianName(schedule.getEmptable().getDataprovider().getList());

			final ArrayList<CompanyAsset> technicianToollist = new ArrayList<CompanyAsset>();
			technicianToollist.addAll(schedule.getToolTable().getDataprovider().getList());
			
			final ArrayList<ProductGroupList> materialInfo = new ArrayList<ProductGroupList>();
			materialInfo.addAll(schedule.getMaterialProductTable().getDataprovider().getList());
			
			//selectedRecord.setAttribute("ServiceDate", planPopUp.dbserviceDate.getValueAsDate());
			
			String serviceHrs=schedule.getP_servicehours().getItemText(schedule.getP_servicehours().getSelectedIndex());
			String serviceMin=schedule.getP_servicemin().getItemText(schedule.getP_servicemin().getSelectedIndex());
			String serviceAmPm=schedule.getP_ampm().getItemText(schedule.getP_ampm().getSelectedIndex());
			String totalTime = "";
			final String calculatedServiceTime;
			
			if(serviceHrs.equals("--")){
				serviceHrs = "12";
			}
			if(serviceMin.equals("--")){
				serviceMin = "00";
			}
			if(serviceAmPm.equals("--")){
				serviceAmPm = "AM";
			}
			totalTime = serviceHrs+":"+serviceMin+""+serviceAmPm;
			if(totalTime.equalsIgnoreCase("--:----") || totalTime.equalsIgnoreCase("--:--AM") ){
				calculatedServiceTime = "Flexible";
			}else{
				calculatedServiceTime = totalTime;
			}
			System.out.println("calculatedServiceTime"+calculatedServiceTime);
			//selectedRecord.setAttribute("ServiceTime", calculatedServiceTime);
			

			final int InvoiceId ;
			if(schedule.getOlbInvoiceId().getSelectedIndex()!=0){
				InvoiceId  = Integer.parseInt(schedule.getOlbInvoiceId().getValue(schedule.getOlbInvoiceId().getSelectedIndex()));
			}else{
				InvoiceId = 0;
			}
			final String invoiceDate;
			if(schedule.getOlbInvoiceDate().getSelectedIndex()!=0){
				invoiceDate = schedule.getOlbInvoiceDate().getValue(schedule.getOlbInvoiceDate().getSelectedIndex());
             }else{
            	 invoiceDate = null;
             }
			final int projectId = serObj.getProjectId();
			Console.log("Project Id=="+projectId);
			ArrayList<String> statuslist = new ArrayList<String>();
			statuslist.add("Scheduled");
			statuslist.add("Rescheduled");

				
				/**
				 * Date 09-08-2018 By Vijay
				 * Des :- Updated filter with technician Name and service Date and old code commented
				 */
				ArrayList<String> strEmployeelist = new ArrayList<String>();
				for(EmployeeInfo emp : emplist){
					strEmployeelist.add(emp.getFullName());
				}
				
				Date date = schedule.getDateBox().getValue();
				
				/**
				 * ends here
				 */
				
				Vector<Filter> filtervec=new Vector<Filter>();
				Filter filter = null;
				MyQuerry querry;
				
				filter = new Filter();
				filter.setQuerryString("companyId");
				filter.setLongValue(UserConfiguration.getCompanyId());
				filtervec.add(filter);
				
				filter = new Filter();
				filter.setQuerryString("status IN");
				filter.setList(statuslist);
				filtervec.add(filter);
				
//				filter = new Filter();
//				filter.setQuerryString("serviceDate >=");
//				filter.setDateValue(date);
//				filtervec.add(filter);
//				
//				filter = new Filter();
//				filter.setQuerryString("serviceDate <=");
//				filter.setDateValue(addThirtyDate);
//				filtervec.add(filter);
				
				
				filter = new Filter();
				filter.setQuerryString("employee IN");
				filter.setList(strEmployeelist);
				filtervec.add(filter);
				
				filter = new Filter();
				filter.setQuerryString("serviceDate");
				filter.setDateValue(date);
				filtervec.add(filter);
				
				querry=new MyQuerry();
				querry.setQuerryObject(new Service());
				querry.setFilters(filtervec);
				
				service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						Console.log("RESULT SIZE : "+result.size());		
						label : for(SuperModel model:result){
							Service customerEntity=(Service) model;	
							if(serObj.getCount() != customerEntity.getCount()){
							if(!(calculatedServiceTime.equalsIgnoreCase("Flexible")))
							{
							for(EmployeeInfo emp : emplist){
								System.out.println("All values komal 1:" + emp.getFullName() +" " +customerEntity.getEmployee() +" "+format1.format(schedule.getDateBox().getValue())+" "+format1.format(customerEntity.getServiceDate())  +" " +calculatedServiceTime +" " +customerEntity.getServiceTime());
								
								if(emp.getFullName().equals(customerEntity.getEmployee()) && format1.format(schedule.getDateBox().getValue()).equals(format1.format(customerEntity.getServiceDate())) && calculatedServiceTime.equals(customerEntity.getServiceTime())){
									System.out.println("All values komal 2:" + emp.getFullName() +" " +customerEntity.getEmployee() +" "+format1.format(schedule.getDateBox().getValue())+" "+format1.format(customerEntity.getServiceDate()) +" " +calculatedServiceTime +" " +customerEntity.getServiceTime());
									validateflag = false;
									break label;
									
								}
								}
							}
						  }
						}
						
						System.out.println("Flag value :" + validateflag);
						if (validateflag == false) {
							alert.alert("Selected technician is already assigned to other service at inserted date and time");
							schedule.getLblOk().setText("OK");
							validateflag = true;
							} else {
								genservice.TechnicianSchedulingPlan(serObj,emplist,technicianToollist,materialInfo,schedule.getDateBox().getValue(),calculatedServiceTime,InvoiceId,invoiceDate,technicianName,projectId, new AsyncCallback<Integer>() {
							
							@Override
							public void onSuccess(Integer result) {
								// TODO Auto-generated method stub
								schedule.getLblOk().setText("OK");
								System.out.println("iddddiiddd - - "+result);
								//selectedRecord.setAttribute("ProjectId", result);
								//grid.redraw();
								if(!calculatedServiceTime.equals(""))
									serObj.setServiceTime(calculatedServiceTime);
									if(result!=0)
										serObj.setProjectId(result);
									
									if(InvoiceId!=0){
										serObj.setInvoiceId(InvoiceId);
									}
																			
									if(!technicianName.equals("")){
										serObj.setEmployee(technicianName);
									}
                                serviceObject = serObj;
								RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
								table.redrawRow(rowIndex);
								
								/**
								 * @author Anil @since 19-10-2021
								 * on click of mark complete also update service 
								 * raised by Rahul Tiwari
								 */
								if(completionFlag){
									if(schedule.getMarkcompletepopup().getServiceCompletionDate().getValue() == null){
										alert.alert("Please add Completetion Date");
									}else{
										ReactOnServiceCompletionPopUp(event);
									}
								}else{
									alert.alert("Saved Succesfully");
									schedule.hidePopUp();
								}
								try{
									
									for(EmployeeInfo emp : emplist){
										/**
										  * Rahul Verma added below for notification on 28th Sept 2018
										  * For Reschedule
										  */
										 NotificationServiceAsync asyncService=GWT.create(NotificationService.class);
//										 if(form.tbServiceEngg.getSelectedIndex()!=0){
										 
											 String message="Service Id: "+serObj.getCount()+" is assigned to you.";
											 Console.log("message"+message);
//											 Console.log("form.tbServiceEngg.getValue(form.tbServiceEngg.getSelectedIndex())"+form.tbServiceEngg.getValue(form.tbServiceEngg.getSelectedIndex()));
											 
											 asyncService.sendNotification(serObj.getCompanyId(), emp.getFullName(), AppConstants.IAndroid.EVA_PEDIO,message, new AsyncCallback<ArrayList<Integer>>() {

												@Override
												public void onFailure(Throwable caught) {
													// TODO Auto-generated method stub
													
												}

												@Override
												public void onSuccess(ArrayList<Integer> result) {
													// TODO Auto-generated method stub
													Console.log("message"+result);
//													int resultValue=result.get(0);
//													if(resultValue==1){
//														form.showDialogMessage("Successful to send notification to technician");
//													}else if(resultValue==2){
//														form.showDialogMessage("Device not register for "+form.tbServiceEngg.getValue(form.tbServiceEngg.getSelectedIndex()));
//													}else if(resultValue==3){
//														form.showDialogMessage("No Username defined for "+form.tbServiceEngg.getValue(form.tbServiceEngg.getSelectedIndex()));
//													}else if(resultValue==0){
//														form.showDialogMessage("Notification not sent to EVA Pedio");
//													}
												}
											});
//										 }
										 /**
										  * Ends for Rahul
										  */
									}
									}catch(Exception e){
										e.printStackTrace();
									}
							}
			
							@Override
							public void onFailure(Throwable caught) {
								// TODO Auto-generated method stub
								schedule.getLblOk().setText("OK");
								schedule.hidePopUp();
								
							}
						});
						
					}
					}
					
					@Override
					public void onFailure(Throwable caught) {
						System.out.println("Failed");
						Console.log("RPC Failed");
					}
				});					
		}else{
			schedule.getLblOk().setText("OK");
		}
		
	}
	
	
	public void updateServices(final boolean completionFlag, final ClickEvent event){
		schedule.getLblOk().setText("Processing");
		final ArrayList<ProductGroupList> materiallist = new ArrayList<ProductGroupList>();
		materiallist.addAll(schedule.getMaterialProductTable().getDataprovider().getList());
		if(materiallist.size()>0){
			Company comp = new Company();
			commonserviceasync.validateMaterialQuantity(comp.getCompanyId(), materiallist, new AsyncCallback<String>() {
				@Override
				public void onFailure(Throwable caught) {
					
				}

				@Override
				public void onSuccess(String result) {
					// TODO Auto-generated method stub
					if(!result.equals("")){
						GWTCAlert alert = new GWTCAlert();
						alert.alert(result);
						schedule.getLblOk().setText("OK");
						
//						if(completionFlag){
//							if(schedule.getMarkcompletepopup().getServiceCompletionDate().getValue() == null){
//								alert.alert("Please add Completetion Date");
//							}else{
//								ReactOnServiceCompletionPopUp(event);
//							}
//						}
					}
					else{
						processPlanData(completionFlag,event);
					}
				}
			});
		}
		else{
			processPlanData(completionFlag,event);
		}
	}
	
	/**
	 * @author Anil
	 * @since 21-01-2022
	 * moved completion logic in method so that we can reuse the same code
	 */
	private void reactOnMarkComplete(final Date completionDate) {
		System.out.println("Hi am in click btn in mark complete");

		System.out.println("Status === "+ !serviceObject.getStatus().equals("Completed"));
		final GWTCAlert alert = new GWTCAlert();

		Date toodaydate = new Date();
		// if(date.after(object.getServiceDate()))
		// System.out.println("hi vijay ------"+date.after(object.getServiceDate()));

		boolean checkvalid = checkvalidation(toodaydate,serviceObject.getServiceDate());

		/**
		 * Date 13 Feb 2017 by Vijay Description :- if condition for nbhc
		 * service engineer is mondatory if we click on this button then
		 * validation will display message if service engineer is blank or
		 * null else for all clients service engineer non mondatory
		 */

		boolean checkvaliforServiceEngg = true;

		if (AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "OnlyForNBHC")) {
			if (serviceObject.getEmployee() == null|| serviceObject.getEmployee().equals("")) {
				alert.alert("Please add Service engineer first!");
				checkvaliforServiceEngg = false;
			}

		}

		/**
		 * end here
		 */

		if (checkvalid == false)
			alert.alert(" Service Date exceeds current date. Please reschedule the date");

		if (checkvalid && checkvaliforServiceEngg) {
			glassPanel = new GWTCGlassPanel();
			glassPanel.show();

			if (!serviceObject.getStatus().equals("Completed")) {

				boolean conf = Window.confirm("If project is not created for this service then job card will not print do you want to continue");
				if (conf == true) {
					// rohan added this date parameter for completion of
					// service on specific date
					servicelistservice.markcompleteservice(completionDate, serviceObject,new AsyncCallback<ArrayList<Integer>>() {
						@Override
						public void onSuccess(ArrayList<Integer> result) {
							Console.log("Result : "+result +" size "+result.size());
//									if (result.contains(1))
							if (result!=null&&result.contains(2)) {
								glassPanel.hide();
								alert.alert("Service Completed Sucessfully");
								serviceObject.setStatus("Completed");
								serviceObject.setServiceCompletionDate(completionDate);
								table.redraw();

								// *************vaishnavi***********
								// createProjectCorrespondingToService(
								// "Button", object);
							}

						}

						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							final GWTCAlert alert = new GWTCAlert();
							alert.alert("An Unexpected error occurred!");

						}
					});
				}
			}
			if (serviceObject.getStatus().equals("Completed")) {
				alert.alert("This Service Already Mark Completed");
			}

			glassPanel.hide();
			datepopupPanel.hide();
		}
	}
	
			//Ashwini Patil Date:15-05-2023
			private  void addIDSRDownload(){
				
				idSRDownloadColumn=new Column<Service, String>(new ClickableTextCell()) {
					@Override
					public String getCellStyleNames(Context context, Service object) {
							return "blue";
					}
					
					@Override
					public String getValue(Service object) {
						// TODO Auto-generated method stub
						if(object.getUptestReport()!=null&&object.getUptestReport().getName()!=null)
							return object.getCount()+" / "+object.getUptestReport().getName();
						else
							return object.getCount()+"";
					}
				};
				
				
				table.addColumn(idSRDownloadColumn, "Service ID / Service Report1");
				table.setColumnWidth(idSRDownloadColumn, 100, Unit.PX);
				idSRDownloadColumn.setSortable(true);
				
				idSRDownloadColumn.setFieldUpdater(new FieldUpdater<Service, String>() {
					
					@Override
					public void update(int index, Service object, String value) {
						// TODO Auto-generated method stub
						ServicePresenter.viewDocumentFlag = false;
						if(object.getUptestReport()!=null){
							String url=object.getUptestReport().getUrl()+"&filename="+object.getUptestReport().getName();
							Window.open(url, "_blank", "");
							//https://my-dot-evaerp282915.appspot.com/slick_erp/downloadMapImage?blobkey=AMIfv96FnGlPnJLuG2pQhkTEEhVGA1IZ2bGIy4sYJwksLeBPv7gLMQ4OYV4DAgfd-Fww-6NtSea2VfIrVkLbH-YRW3Y8kIBBVCbMZoxvF6M719z59cJ0irfqudoKvEBw3kanmirEHPyLSmH9Jd_EWJ57J978p84T5FGx5blptAeuiGrw8IAr82SteALS8eH9dUhIA5fA8UTy3qciVYJNQfXyWg397_u4xKy8DAi0ZImUU_akGhveEQGvExv0FeM3Mb3LF3wE9h-lsPAwKZ9TbRYGf2JfIknBAg&filename=abc.jpg
						}else{
							Window.alert("No image found");
						}
					}
				});
			}

	
			private void addSortingProductFrequncy() {
				List<Service> list = getDataprovider().getList();
				columnSort = new ListHandler<Service>(list);
				columnSort.setComparator(productFrequency ,  new Comparator<Service>() {

					@Override
					public int compare(Service e1, Service e2) {
						if (e1 != null && e2 != null) {
							if (e1.getProductFrequency() != null && e2.getProductFrequency() != null) {
								return e1.getProductFrequency().compareTo(e2.getProductFrequency());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
				table.addColumnSortHandler(columnSort);
			}
			
			
			private void addProductFrequncyColumn() {

				productFrequency = new TextColumn<Service>() {
					
					@Override
					public String getValue(Service object) {
						if(object.getProductFrequency()!=null){
							return object.getProductFrequency();
						}
						return "";
					}
				};
				table.addColumn(productFrequency, "Frequency");
				table.setColumnWidth(productFrequency, 120, Unit.PX);
				productFrequency.setSortable(true);
				
			}
			
			private void createCallColumn() {

				ButtonCell btnCell = new ButtonCell();
				callButton = new Column<Service, String>(btnCell) {

					@Override
					public String getValue(Service object) {
						// TODO Auto-generated method stub
						return "Call";
					}
				};

				table.addColumn(callButton, "");
				table.setColumnWidth(callButton, 110, Unit.PX);
			}

			private void updateCallColumn() {
				callButton.setFieldUpdater(new FieldUpdater<Service, String>() {
					@Override
					public void update(int index, final Service object, String value) {
						
						ServicePresenter.viewDocumentFlag=false;
						GWTCAlert alert = new GWTCAlert();
						if(object.getCellNumber()!=null){
							
							if(LoginPresenter.company!=null) {
								
								if(LoginPresenter.company.getApiUrl()!=null&&!LoginPresenter.company.getApiUrl().equals("")) {
									if(LoginPresenter.loggedInUserCallerId!=null&&!LoginPresenter.loggedInUserCallerId.equals("")) {

										final String url = LoginPresenter.company.getApiUrl()+"user="+LoginPresenter.loggedInUserCallerId+"&number_ToDial="+object.getCellNumber();
										Window.open(url, "test", "enabled");
									}else if(LoginPresenter.company.getCallerId()!=null&&!LoginPresenter.company.getCallerId().equals("")) {

										final String url = LoginPresenter.company.getApiUrl()+"user="+LoginPresenter.company.getCallerId()+"&number_ToDial="+object.getCellNumber();
										Window.open(url, "test", "enabled");
									}else
										alert.alert("Called ID not found. Go to settings -> User -> add caller ID ");
									
								}else
									alert.alert("API url not found. Go to settings -> Company -> Telephony tab -> add api url ");
								
							}else {
								
								alert.alert("Not able to fetch company info");
							}
						
						}else {
							alert.alert("No cell number found in service");
							Console.log("No cell number found in service");
							return;
						}
					}
				});
			
			}
}

