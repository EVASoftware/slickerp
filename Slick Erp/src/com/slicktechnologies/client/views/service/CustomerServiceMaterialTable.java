package com.slicktechnologies.client.views.service;

import java.util.ArrayList;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.NumberCell;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.PopupPanel;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.device.ServiceMarkCompletePopUp;
import com.slicktechnologies.client.views.technicianschedule.ServiceCompletetionPopup;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;


public class CustomerServiceMaterialTable extends SuperTable<ProductGroupList> implements ClickHandler  {


	TextColumn<ProductGroupList> getColumnProductName;
	TextColumn<ProductGroupList> getColumnQuantity;
	TextColumn<ProductGroupList> getColumnWarehouseName;
	TextColumn<ProductGroupList> getColumnStorageLocationName;
	TextColumn<ProductGroupList> getColumnStorageBinName;
	
	TextColumn<ProductGroupList> getColumnReturnQuantity;
	Column<ProductGroupList, String> returnQuantity;
	
//	TextColumn<ProductGroupList> getcolumnProjectId;
	
	Column<ProductGroupList, String> btnreturnQuantity;
	Column<ProductGroupList, String> deleteColumn;
	
	/** date 11/11/2017 added by komal to add UOM column **/
	TextColumn<ProductGroupList> getColumnUOM;
	
	public static int projectId;
	
	int rowindex=0;
	int productId=0;
	final GenricServiceAsync genasync=GWT.create(GenricService.class);

	ServiceMarkCompletePopUp returnQty=new ServiceMarkCompletePopUp(true);
	PopupPanel panel = new PopupPanel();
	
	/** date 13.3.2019 added by komal for actual quantity **/
	Column<ProductGroupList , String> getColumnActualQuantity;
	TextColumn<ProductGroupList> getColumnViewActualQuantity;
	ServiceMarkCompletePopUp consumeQty=new ServiceMarkCompletePopUp(true , "Consumed Quantity");
	TextColumn<ProductGroupList> getTechnician;
	boolean showTechnician = false;	
	
	public CustomerServiceMaterialTable(){
		super();
		setHeight("100px");
		
		returnQty.getBtnOne().addClickHandler(this);
		returnQty.getBtnTwo().addClickHandler(this);
		consumeQty.getBtnOne().addClickHandler(this);
		consumeQty.getBtnTwo().addClickHandler(this);
	}
	public CustomerServiceMaterialTable(boolean flag){
		super();
		setHeight("100px");
		
		returnQty.getBtnOne().addClickHandler(this);
		returnQty.getBtnTwo().addClickHandler(this);
		consumeQty.getBtnOne().addClickHandler(this);
		consumeQty.getBtnTwo().addClickHandler(this);
		showTechnician = flag;
	}

	@Override
	public void createTable() {
		// TODO Auto-generated method stub
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "TechnicianwiseMaterialPopup")){
			addColumnProductName();
			addColumnUOM();
			addColumnPlannedQuantity();
			addColumnActualQuantity();
			addColumnViewConsumedQuantity();
	//		addColumnConsumedQuantity();
			addColumnDelete();
			createFieldUpdaterdeleteColumn();
		}
		else{
		
		addColumnProductName();
		/** date 11/11/2017 added by komal to add UOM column **/
		addColumnUOM();
		addColumnQuantity();
		addColumnWarehosueName();
		addColumnStorageLocation();
		addColumnStorageBin();
//		addColumnReturnQuantity();
		//Ashwini Patil Date:24-03-2023
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "MINBasedOnConsumedQuantity")){
			addColumnViewConsumedQuantity();
			addColumnConsumedQuantity();
		}else{
			addColumnViewReturnQuantity();
			addColumnReturnQuantity();
		}
		
		addColumnDelete();
		addFieldUpdater();
		}
	}
	/** date 11/11/2017 added by komal to add UOM column **/
	private void addColumnUOM(){
      getColumnUOM = new TextColumn<ProductGroupList>() {
			
			@Override
			public String getValue(ProductGroupList object) {
				// TODO Auto-generated method stub
				if(object.getUnit() != null){
					return object.getUnit();
				}
				return  "";
			}
		};
		table.addColumn(getColumnUOM, "UOM");
		table.setColumnWidth(getColumnUOM, 50, Unit.PX);
		
	}

//	private void addColumnProjectId() {
//
//		getcolumnProjectId = new TextColumn<ProductGroupList>() {
//			
//			@Override
//			public String getValue(ProductGroupList object) {
//				// TODO Auto-generated method stub
//				return "";
//			}
//		};
//		
//	}

	private void addColumnReturnQuantity() {

//		EditTextCell editCell=new EditTextCell();
		
		ButtonCell btnceell = new ButtonCell();

		returnQuantity = new Column<ProductGroupList, String>(btnceell) {
			
			@Override
			public String getValue(ProductGroupList object) {
				// TODO Auto-generated method stub
				return "Return Qty";
			}
		};
		table.addColumn(returnQuantity, "Return");
		table.setColumnWidth(returnQuantity, 100, Unit.PX);
	}
	

	private void addColumnViewReturnQuantity() {

		getColumnReturnQuantity = new TextColumn<ProductGroupList>() {
			
			@Override
			public String getValue(ProductGroupList object) {
				return object.getReturnQuantity()+"";
			}
		};
		table.addColumn(getColumnReturnQuantity, "Return Quantity");
		table.setColumnWidth(getColumnReturnQuantity, 100, Unit.PX);
	}

	
	private void addColumnProductName() {

		getColumnProductName = new TextColumn<ProductGroupList>() {
			
			@Override
			public String getValue(ProductGroupList object) {
				return object.getName();
			}
		};
		table.addColumn(getColumnProductName, "Material Name");
		table.setColumnWidth(getColumnProductName, 100, Unit.PX);
	}

	private void addColumnQuantity() {

		getColumnQuantity = new TextColumn<ProductGroupList>() {
			
			@Override
			public String getValue(ProductGroupList object) {
				// TODO Auto-generated method stub
				return object.getQuantity()+"";
			}
		};
		table.addColumn(getColumnQuantity, "Quantity");
		table.setColumnWidth(getColumnQuantity, 100, Unit.PX);
		
	}

	private void addColumnWarehosueName() {

		getColumnWarehouseName = new TextColumn<ProductGroupList>() {
			
			@Override
			public String getValue(ProductGroupList object) {
				// TODO Auto-generated method stub
				return object.getWarehouse();
			}
		};
		table.addColumn(getColumnWarehouseName, "Warehouse Name");
		table.setColumnWidth(getColumnWarehouseName, 100, Unit.PX);
	}

	private void addColumnStorageLocation() {

		getColumnStorageLocationName = new TextColumn<ProductGroupList>() {
			
			@Override
			public String getValue(ProductGroupList object) {
				// TODO Auto-generated method stub
				return object.getStorageLocation();
			}
		};
		table.addColumn(getColumnStorageLocationName, "Storage Location");
		table.setColumnWidth(getColumnStorageLocationName, 100, Unit.PX);
	}

	private void addColumnStorageBin() {

		getColumnStorageBinName = new TextColumn<ProductGroupList>() {
			
			@Override
			public String getValue(ProductGroupList object) {
				// TODO Auto-generated method stub
				return object.getStorageBin();
			}
			
		};
		table.addColumn(getColumnStorageBinName, "Storage Bin");
		table.setColumnWidth(getColumnStorageBinName, 100, Unit.PX);
	}

	private void addColumnDelete() {
		ButtonCell btnCell = new ButtonCell();
		deleteColumn = new Column<ProductGroupList, String>(btnCell) {
			@Override
			public String getValue(ProductGroupList object) {
				return "Delete";
			}
		};
		table.addColumn(deleteColumn, "Delete");
		table.setColumnWidth(deleteColumn, 100, Unit.PX);
	}
	
	

	@Override
	public void addFieldUpdater() {
		createFieldUpdaterdeleteColumn();
		createFieldUpdaterReturnQtyColumn();
	}
		
	private void createFieldUpdaterReturnQtyColumn() {

		returnQuantity.setFieldUpdater(new FieldUpdater<ProductGroupList, String>() {
			
			@Override
			public void update(int index, ProductGroupList object, String value) {
				// TODO Auto-generated method stub

				rowindex = index;
				productId = object.getProduct_id();				
				panel = new PopupPanel(true);
				panel.add(returnQty);
				panel.show();
				panel.center();
			}

			
		});
	}

	protected void createFieldUpdaterdeleteColumn() {
		deleteColumn.setFieldUpdater(new FieldUpdater<ProductGroupList, String>() {
			@Override
			public void update(int index, ProductGroupList object, String value) {
				getDataprovider().getList().remove(object);
				table.redrawRow(index);
			}
		});
	}
	
	
	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		for(int i=table.getColumnCount()-1;i>-1;i--)
	    	  table.removeColumn(i); 
		 Console.log("Flag : "+state);
		boolean processConfigFlag = false;
		processConfigFlag = AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "TechnicianwiseMaterialPopup");
		 if(state==true && !processConfigFlag){
			 createTable();
		 }else{
			 if(processConfigFlag && state){
				 Console.log("Flag 1: "+state);
				addColumnProductName();
				addColumnUOM();
				addColumnPlannedQuantity();
				addColumnActualQuantity();
				addColumnViewConsumedQuantity();
				addColumnDelete();
				createFieldUpdaterdeleteColumn();
			}else if(processConfigFlag && !state){
				 Console.log("Flag 2: "+state);
				addColumnProductName();
				addColumnUOM();
				addColumnPlannedQuantity();
				addColumnViewActualQuantity();
				if(showTechnician){
					addColumnViewTechnician();
				}else{
					addColumnViewConsumedQuantity();
					addColumnConsumedQuantity();
				}
			}else{
			 	addColumnProductName();
				addColumnQuantity();
				addColumnWarehosueName();
				addColumnStorageLocation();
				addColumnStorageBin();
				//Ashwini Patil Date:24-03-2023
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "MINBasedOnConsumedQuantity")){
					addColumnViewConsumedQuantity();
				}else{
					addColumnViewReturnQuantity();
				}
			}
		 }
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
	
	private void saveintoProject(final double returnQty) {
		MyQuerry querry = new MyQuerry();
		Vector<Filter> filtervec = new Vector<Filter>();
		
		Filter filter = null;
		
		filter = new Filter();
		filter.setIntValue(projectId);
		filter.setQuerryString("count");
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new ServiceProject());
		
		genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				for(SuperModel model : result){
					System.out.println("Resultttt =="+result.size());
					final ServiceProject project = (ServiceProject) model;
					for(int i=0;i<project.getProdDetailsList().size();i++){
						System.out.println("project Product id =="+project.getProdDetailsList().get(i).getProduct_id());
						System.out.println("product id"+productId);
						if(project.getProdDetailsList().get(i).getProduct_id().equals(productId)){
							System.out.println("Done here");
							project.getProdDetailsList().get(i).setReturnQuantity(returnQty);
						}
					}
					
					Timer timer = new Timer() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							SaveProject(project);
						}
					};
					timer.schedule(1000);
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				System.out.println("Failed");
			}
		});
		
	}
	
	
	private void SaveProject(ServiceProject project) {
		// TODO Auto-generated method stub
		
		genasync.save(project, new AsyncCallback<ReturnFromServer>() {
			
			@Override
			public void onSuccess(ReturnFromServer result) {
				// TODO Auto-generated method stub
				System.out.println("On save success");
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				System.out.println("Failed");
			}
		});
	}

	@Override
	public void onClick(ClickEvent event) {

		// return material qty
		GWTCAlert alert = new GWTCAlert();
				if(event.getSource() == returnQty.getBtnOne()){
					if(returnQty.getReturnQuantity().getValue()==null){
						alert.alert("Please add return Quantity");
					}
					else if(returnQty.getReturnQuantity().getValue()==0){
						alert.alert("Return Quantity can not be zero");
					}else{
						ReactOnreturnQuantity(returnQty.getReturnQuantity().getValue());
					}
				}
				if(event.getSource() == returnQty.getBtnTwo()){
					panel.hide();
				}
				if(event.getSource() == consumeQty.getBtnOne()){
					if(consumeQty.getReturnQuantity().getValue()==null){
						alert.alert("Please add consumed Quantity");
					}
					else if(consumeQty.getReturnQuantity().getValue()==0){
						alert.alert("Consumed Quantity can not be zero");
					}else{
						ReactOnreturnQuantity(consumeQty.getReturnQuantity().getValue());
					}
				}
				if(event.getSource() == consumeQty.getBtnTwo()){
					panel.hide();
				}
	}
	
	private void ReactOnreturnQuantity(Double returnQty) {
		
		ArrayList<ProductGroupList> list = new ArrayList<ProductGroupList>();
		list.addAll(getDataprovider().getList());
		
		list.get(rowindex).setReturnQuantity(returnQty);
		
		getDataprovider().getList().clear();
		getDataprovider().getList().addAll(list);
		
		panel.hide();
		
		saveintoProject(returnQty);
	}
	private void addColumnPlannedQuantity() {

		getColumnQuantity = new TextColumn<ProductGroupList>() {
			
			@Override
			public String getValue(ProductGroupList object) {
				// TODO Auto-generated method stub
				return object.getPlannedQty()+"";
			}
		};
		table.addColumn(getColumnQuantity, "Planned");
		table.setColumnWidth(getColumnQuantity, 100, Unit.PX);
		
	}
	
	private void addColumnActualQuantity() {
		EditTextCell editCell=new EditTextCell();
		getColumnActualQuantity = new Column<ProductGroupList , String>(editCell) {

			@Override
			public String getValue(ProductGroupList object) {
				// TODO Auto-generated method stub
				if (object.getProActualQty() == 0) {
					return 0 + "";
				} else {
					return object.getProActualQty() + "";
				}
			}
			
			
		};
		table.addColumn(getColumnActualQuantity, "# Required");
		table.setColumnWidth(getColumnActualQuantity, 100, Unit.PX);
		
		getColumnActualQuantity.setFieldUpdater(new FieldUpdater<ProductGroupList, String>()
				{
			@Override
			public void update(int index,ProductGroupList object,String value){
			double val1  = 0;
			try{
				val1 = Double.parseDouble(value);
			}catch(Exception e){
				val1 = 0;
			}
				object.setProActualQty(val1);
				object.setQuantity(val1);
				table.redrawRow(index);
			}
				});
		
		
	}
	
	private void addColumnViewActualQuantity() {

		getColumnViewActualQuantity = new TextColumn<ProductGroupList>() {
			
			@Override
			public String getValue(ProductGroupList object) {
				// TODO Auto-generated method stub
				return object.getProActualQty()+"";
			}
		};
		table.addColumn(getColumnViewActualQuantity, "Required");
		table.setColumnWidth(getColumnViewActualQuantity, 100, Unit.PX);
		
	}
	private void addColumnConsumedQuantity() {

//		EditTextCell editCell=new EditTextCell();
		
		ButtonCell btnceell = new ButtonCell();

		returnQuantity = new Column<ProductGroupList, String>(btnceell) {
			
			@Override
			public String getValue(ProductGroupList object) {
				// TODO Auto-generated method stub
				return "Consumed Qty";
			}
		};
		table.addColumn(returnQuantity, "Consumption");
		table.setColumnWidth(returnQuantity, 100, Unit.PX);
		
		

			returnQuantity.setFieldUpdater(new FieldUpdater<ProductGroupList, String>() {
				
				@Override
				public void update(int index, ProductGroupList object, String value) {
					// TODO Auto-generated method stub

					rowindex = index;
					productId = object.getProduct_id();				
					panel = new PopupPanel(true);
					
					panel.add(consumeQty);
					panel.show();
					panel.center();
				}

				
			});
	}

	private void addColumnViewConsumedQuantity() {

		getColumnReturnQuantity = new TextColumn<ProductGroupList>() {
			
			@Override
			public String getValue(ProductGroupList object) {
				if(object.getReturnQuantity() == 0){
					return 0+"";
				}else{
				return object.getReturnQuantity()+"";
				}
			}
		};
		table.addColumn(getColumnReturnQuantity, "Consumed Quantity");
		table.setColumnWidth(getColumnReturnQuantity, 100, Unit.PX);
	}
	
	private void addColumnViewTechnician() {

		getTechnician = new TextColumn<ProductGroupList>() {
			
			@Override
			public String getValue(ProductGroupList object) {
				if(object.getCreatedBy() != null){
					return object.getCreatedBy();
				}
				return "";
			}
		};
		table.addColumn(getTechnician, "Service Engineer");
		table.setColumnWidth(getTechnician, 100, Unit.PX);
	}

	}
//package com.slicktechnologies.client.views.service;
//
//import java.util.ArrayList;
//import java.util.Vector;
//
//import com.google.code.p.gwtchismes.client.GWTCAlert;
//import com.google.gwt.cell.client.ButtonCell;
//import com.google.gwt.cell.client.EditTextCell;
//import com.google.gwt.cell.client.FieldUpdater;
//import com.google.gwt.cell.client.NumberCell;
//import com.google.gwt.cell.client.TextCell;
//import com.google.gwt.core.client.GWT;
//import com.google.gwt.dom.client.Style.Unit;
//import com.google.gwt.event.dom.client.ClickEvent;
//import com.google.gwt.event.dom.client.ClickHandler;
//import com.google.gwt.user.cellview.client.Column;
//import com.google.gwt.user.cellview.client.TextColumn;
//import com.google.gwt.user.client.Timer;
//import com.google.gwt.user.client.rpc.AsyncCallback;
//import com.google.gwt.user.client.ui.DoubleBox;
//import com.google.gwt.user.client.ui.PopupPanel;
//import com.simplesoftwares.client.library.ReturnFromServer;
//import com.simplesoftwares.client.library.appstructure.SuperModel;
//import com.simplesoftwares.client.library.appstructure.SuperTable;
//import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
//import com.simplesoftwares.client.library.appstructure.search.Filter;
//import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
//import com.simplesoftwares.client.library.libservice.GenricService;
//import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
//import com.slicktechnologies.client.utility.AppUtility;
//import com.slicktechnologies.client.utils.Console;
//import com.slicktechnologies.client.views.device.ServiceMarkCompletePopUp;
//import com.slicktechnologies.client.views.technicianschedule.ServiceCompletetionPopup;
//import com.slicktechnologies.shared.SalesLineItem;
//import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
//import com.slicktechnologies.shared.common.servicerelated.ServiceProject;
//
//
//public class CustomerServiceMaterialTable extends SuperTable<ProductGroupList> implements ClickHandler  {
//
//
//	TextColumn<ProductGroupList> getColumnProductName;
//	TextColumn<ProductGroupList> getColumnQuantity;
//	TextColumn<ProductGroupList> getColumnWarehouseName;
//	TextColumn<ProductGroupList> getColumnStorageLocationName;
//	TextColumn<ProductGroupList> getColumnStorageBinName;
//	
//	TextColumn<ProductGroupList> getColumnReturnQuantity;
//	Column<ProductGroupList, String> returnQuantity;
//	
////	TextColumn<ProductGroupList> getcolumnProjectId;
//	
//	Column<ProductGroupList, String> btnreturnQuantity;
//	Column<ProductGroupList, String> deleteColumn;
//	
//	/** date 11/11/2017 added by komal to add UOM column **/
//	TextColumn<ProductGroupList> getColumnUOM;
//	
//	public static int projectId;
//	
//	int rowindex=0;
//	int productId=0;
//	final GenricServiceAsync genasync=GWT.create(GenricService.class);
//
//	ServiceMarkCompletePopUp returnQty=new ServiceMarkCompletePopUp(true);
//	PopupPanel panel = new PopupPanel();
//	
//	/** date 13.3.2019 added by komal for actual quantity **/
//	Column<ProductGroupList , String> getColumnActualQuantity;
//	TextColumn<ProductGroupList> getColumnViewActualQuantity;
//	ServiceMarkCompletePopUp consumeQty=new ServiceMarkCompletePopUp(true , "Consumed Quantity");
//	
//	public CustomerServiceMaterialTable(){
//		super();
//		setHeight("100px");
//		
//		returnQty.getBtnOne().addClickHandler(this);
//		returnQty.getBtnTwo().addClickHandler(this);
//		consumeQty.getBtnOne().addClickHandler(this);
//		consumeQty.getBtnTwo().addClickHandler(this);
//	}
//
//	@Override
//	public void createTable() {
//		// TODO Auto-generated method stub
//		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "TechnicianwiseMaterialPopup")){
//			addColumnProductName();
//			addColumnUOM();
//			addColumnPlannedQuantity();
//			addColumnActualQuantity();
//			addColumnViewConsumedQuantity();
//	//		addColumnConsumedQuantity();
//			addColumnDelete();
//			createFieldUpdaterdeleteColumn();
//		}
//		else{
//		
//		addColumnProductName();
//		/** date 11/11/2017 added by komal to add UOM column **/
//		addColumnUOM();
//		addColumnQuantity();
//		addColumnWarehosueName();
//		addColumnStorageLocation();
//		addColumnStorageBin();
////		addColumnReturnQuantity();
//		addColumnViewReturnQuantity();
//		addColumnReturnQuantity();
//		addColumnDelete();
//		addFieldUpdater();
//		}
//	}
//	/** date 11/11/2017 added by komal to add UOM column **/
//	private void addColumnUOM(){
//      getColumnUOM = new TextColumn<ProductGroupList>() {
//			
//			@Override
//			public String getValue(ProductGroupList object) {
//				// TODO Auto-generated method stub
//				return object.getUnit();
//			}
//		};
//		table.addColumn(getColumnUOM, "UOM");
//		table.setColumnWidth(getColumnUOM, 50, Unit.PX);
//		
//	}
//
////	private void addColumnProjectId() {
////
////		getcolumnProjectId = new TextColumn<ProductGroupList>() {
////			
////			@Override
////			public String getValue(ProductGroupList object) {
////				// TODO Auto-generated method stub
////				return "";
////			}
////		};
////		
////	}
//
//	private void addColumnReturnQuantity() {
//
////		EditTextCell editCell=new EditTextCell();
//		
//		ButtonCell btnceell = new ButtonCell();
//
//		returnQuantity = new Column<ProductGroupList, String>(btnceell) {
//			
//			@Override
//			public String getValue(ProductGroupList object) {
//				// TODO Auto-generated method stub
//				return "Return Qty";
//			}
//		};
//		table.addColumn(returnQuantity, "Return");
//		table.setColumnWidth(returnQuantity, 100, Unit.PX);
//	}
//
//	private void addColumnViewReturnQuantity() {
//
//		getColumnReturnQuantity = new TextColumn<ProductGroupList>() {
//			
//			@Override
//			public String getValue(ProductGroupList object) {
//				return object.getReturnQuantity()+"";
//			}
//		};
//		table.addColumn(getColumnReturnQuantity, "Return Quantity");
//		table.setColumnWidth(getColumnReturnQuantity, 100, Unit.PX);
//	}
//
//	private void addColumnProductName() {
//
//		getColumnProductName = new TextColumn<ProductGroupList>() {
//			
//			@Override
//			public String getValue(ProductGroupList object) {
//				return object.getName();
//			}
//		};
//		table.addColumn(getColumnProductName, "Material Name");
//		table.setColumnWidth(getColumnProductName, 100, Unit.PX);
//	}
//
//	private void addColumnQuantity() {
//
//		getColumnQuantity = new TextColumn<ProductGroupList>() {
//			
//			@Override
//			public String getValue(ProductGroupList object) {
//				// TODO Auto-generated method stub
//				return object.getQuantity()+"";
//			}
//		};
//		table.addColumn(getColumnQuantity, "Quantity");
//		table.setColumnWidth(getColumnQuantity, 100, Unit.PX);
//		
//	}
//
//	private void addColumnWarehosueName() {
//
//		getColumnWarehouseName = new TextColumn<ProductGroupList>() {
//			
//			@Override
//			public String getValue(ProductGroupList object) {
//				// TODO Auto-generated method stub
//				return object.getWarehouse();
//			}
//		};
//		table.addColumn(getColumnWarehouseName, "Warehouse Name");
//		table.setColumnWidth(getColumnWarehouseName, 100, Unit.PX);
//	}
//
//	private void addColumnStorageLocation() {
//
//		getColumnStorageLocationName = new TextColumn<ProductGroupList>() {
//			
//			@Override
//			public String getValue(ProductGroupList object) {
//				// TODO Auto-generated method stub
//				return object.getStorageLocation();
//			}
//		};
//		table.addColumn(getColumnStorageLocationName, "Storage Location");
//		table.setColumnWidth(getColumnStorageLocationName, 100, Unit.PX);
//	}
//
//	private void addColumnStorageBin() {
//
//		getColumnStorageBinName = new TextColumn<ProductGroupList>() {
//			
//			@Override
//			public String getValue(ProductGroupList object) {
//				// TODO Auto-generated method stub
//				return object.getStorageBin();
//			}
//			
//		};
//		table.addColumn(getColumnStorageBinName, "Storage Bin");
//		table.setColumnWidth(getColumnStorageBinName, 100, Unit.PX);
//	}
//
//	private void addColumnDelete() {
//		ButtonCell btnCell = new ButtonCell();
//		deleteColumn = new Column<ProductGroupList, String>(btnCell) {
//			@Override
//			public String getValue(ProductGroupList object) {
//				return "Delete";
//			}
//		};
//		table.addColumn(deleteColumn, "Delete");
//		table.setColumnWidth(deleteColumn, 100, Unit.PX);
//	}
//	
//	
//
//	@Override
//	public void addFieldUpdater() {
//		createFieldUpdaterdeleteColumn();
//		createFieldUpdaterReturnQtyColumn();
//	}
//		
//	private void createFieldUpdaterReturnQtyColumn() {
//
//		returnQuantity.setFieldUpdater(new FieldUpdater<ProductGroupList, String>() {
//			
//			@Override
//			public void update(int index, ProductGroupList object, String value) {
//				// TODO Auto-generated method stub
//
//				rowindex = index;
//				productId = object.getProduct_id();				
//				panel = new PopupPanel(true);
//				panel.add(returnQty);
//				panel.show();
//				panel.center();
//			}
//
//			
//		});
//	}
//
//	protected void createFieldUpdaterdeleteColumn() {
//		deleteColumn.setFieldUpdater(new FieldUpdater<ProductGroupList, String>() {
//			@Override
//			public void update(int index, ProductGroupList object, String value) {
//				getDataprovider().getList().remove(object);
//				table.redrawRow(index);
//			}
//		});
//	}
//	
//	
//	@Override
//	protected void initializekeyprovider() {
//		// TODO Auto-generated method stub
//		
//	}
//	
//	@Override
//	public void setEnable(boolean state) {
//		// TODO Auto-generated method stub
//		for(int i=table.getColumnCount()-1;i>-1;i--)
//	    	  table.removeColumn(i); 
//		 Console.log("Flag : "+state);
//		boolean processConfigFlag = false;
//		processConfigFlag = AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "TechnicianwiseMaterialPopup");
//		 if(state==true && !processConfigFlag){
//			 createTable();
//		 }else{
//			 if(processConfigFlag && state){
//				 Console.log("Flag 1: "+state);
//				addColumnProductName();
//				addColumnUOM();
//				addColumnPlannedQuantity();
//				addColumnActualQuantity();
//				addColumnViewConsumedQuantity();
//				addColumnDelete();
//				createFieldUpdaterdeleteColumn();
//			}else if(processConfigFlag && !state){
//				 Console.log("Flag 2: "+state);
//				addColumnProductName();
//				addColumnUOM();
//				addColumnPlannedQuantity();
//				addColumnViewActualQuantity();
//				addColumnViewConsumedQuantity();
//				addColumnConsumedQuantity();
//				
//			}else{
//			 	addColumnProductName();
//				addColumnQuantity();
//				addColumnWarehosueName();
//				addColumnStorageLocation();
//				addColumnStorageBin();
//				addColumnViewReturnQuantity();
//			}
//		 }
//	}
//
//	@Override
//	public void applyStyle() {
//		// TODO Auto-generated method stub
//		
//	}
//	
//	private void saveintoProject(final double returnQty) {
//		MyQuerry querry = new MyQuerry();
//		Vector<Filter> filtervec = new Vector<Filter>();
//		
//		Filter filter = null;
//		
//		filter = new Filter();
//		filter.setIntValue(projectId);
//		filter.setQuerryString("count");
//		filtervec.add(filter);
//		
//		querry.setFilters(filtervec);
//		querry.setQuerryObject(new ServiceProject());
//		
//		genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
//			
//			@Override
//			public void onSuccess(ArrayList<SuperModel> result) {
//				// TODO Auto-generated method stub
//				for(SuperModel model : result){
//					System.out.println("Resultttt =="+result.size());
//					final ServiceProject project = (ServiceProject) model;
//					for(int i=0;i<project.getProdDetailsList().size();i++){
//						System.out.println("project Product id =="+project.getProdDetailsList().get(i).getProduct_id());
//						System.out.println("product id"+productId);
//						if(project.getProdDetailsList().get(i).getProduct_id().equals(productId)){
//							System.out.println("Done here");
//							project.getProdDetailsList().get(i).setReturnQuantity(returnQty);
//						}
//					}
//					
//					Timer timer = new Timer() {
//						
//						@Override
//						public void run() {
//							// TODO Auto-generated method stub
//							SaveProject(project);
//						}
//					};
//					timer.schedule(1000);
//				}
//			}
//			
//			@Override
//			public void onFailure(Throwable caught) {
//				// TODO Auto-generated method stub
//				System.out.println("Failed");
//			}
//		});
//		
//	}
//	
//	
//	private void SaveProject(ServiceProject project) {
//		// TODO Auto-generated method stub
//		
//		genasync.save(project, new AsyncCallback<ReturnFromServer>() {
//			
//			@Override
//			public void onSuccess(ReturnFromServer result) {
//				// TODO Auto-generated method stub
//				System.out.println("On save success");
//			}
//			
//			@Override
//			public void onFailure(Throwable caught) {
//				// TODO Auto-generated method stub
//				System.out.println("Failed");
//			}
//		});
//	}
//
//	@Override
//	public void onClick(ClickEvent event) {
//
//		// return material qty
//		GWTCAlert alert = new GWTCAlert();
//				if(event.getSource() == returnQty.getBtnOne()){
//					if(returnQty.getReturnQuantity().getValue()==null){
//						alert.alert("Please add return Quantity");
//					}
//					else if(returnQty.getReturnQuantity().getValue()==0){
//						alert.alert("Return Quantity can not be zero");
//					}else{
//						ReactOnreturnQuantity(returnQty.getReturnQuantity().getValue());
//					}
//				}
//				if(event.getSource() == returnQty.getBtnTwo()){
//					panel.hide();
//				}
//				if(event.getSource() == consumeQty.getBtnOne()){
//					if(consumeQty.getReturnQuantity().getValue()==null){
//						alert.alert("Please add consumed Quantity");
//					}
//					else if(consumeQty.getReturnQuantity().getValue()==0){
//						alert.alert("Consumed Quantity can not be zero");
//					}else{
//						ReactOnreturnQuantity(consumeQty.getReturnQuantity().getValue());
//					}
//				}
//				if(event.getSource() == consumeQty.getBtnTwo()){
//					panel.hide();
//				}
//	}
//	
//	private void ReactOnreturnQuantity(Double returnQty) {
//		
//		ArrayList<ProductGroupList> list = new ArrayList<ProductGroupList>();
//		list.addAll(getDataprovider().getList());
//		
//		list.get(rowindex).setReturnQuantity(returnQty);
//		
//		getDataprovider().getList().clear();
//		getDataprovider().getList().addAll(list);
//		
//		panel.hide();
//		
//		saveintoProject(returnQty);
//	}
//	private void addColumnPlannedQuantity() {
//
//		getColumnQuantity = new TextColumn<ProductGroupList>() {
//			
//			@Override
//			public String getValue(ProductGroupList object) {
//				// TODO Auto-generated method stub
//				return object.getQuantity()+"";
//			}
//		};
//		table.addColumn(getColumnQuantity, "Planned");
//		table.setColumnWidth(getColumnQuantity, 100, Unit.PX);
//		
//	}
//	
//	private void addColumnActualQuantity() {
//		EditTextCell editCell=new EditTextCell();
//		getColumnActualQuantity = new Column<ProductGroupList , String>(editCell) {
//
//			@Override
//			public String getValue(ProductGroupList object) {
//				// TODO Auto-generated method stub
//				if (object.getProActualQty() == 0) {
//					return 0 + "";
//				} else {
//					return object.getProActualQty() + "";
//				}
//			}
//			
//			
//		};
//		table.addColumn(getColumnActualQuantity, "# Required");
//		table.setColumnWidth(getColumnActualQuantity, 100, Unit.PX);
//		
//		getColumnActualQuantity.setFieldUpdater(new FieldUpdater<ProductGroupList, String>()
//				{
//			@Override
//			public void update(int index,ProductGroupList object,String value){
//			double val1  = 0;
//			try{
//				val1 = Double.parseDouble(value);
//			}catch(Exception e){
//				val1 = 0;
//			}
//				object.setProActualQty(val1);
//				object.setQuantity(val1);
//				table.redrawRow(index);
//			}
//				});
//		
//		
//	}
//	
//	private void addColumnViewActualQuantity() {
//
//		getColumnViewActualQuantity = new TextColumn<ProductGroupList>() {
//			
//			@Override
//			public String getValue(ProductGroupList object) {
//				// TODO Auto-generated method stub
//				return object.getProActualQty()+"";
//			}
//		};
//		table.addColumn(getColumnViewActualQuantity, "Required");
//		table.setColumnWidth(getColumnViewActualQuantity, 100, Unit.PX);
//		
//	}
//	private void addColumnConsumedQuantity() {
//
////		EditTextCell editCell=new EditTextCell();
//		
//		ButtonCell btnceell = new ButtonCell();
//
//		returnQuantity = new Column<ProductGroupList, String>(btnceell) {
//			
//			@Override
//			public String getValue(ProductGroupList object) {
//				// TODO Auto-generated method stub
//				return "Consumed Qty";
//			}
//		};
//		table.addColumn(returnQuantity, "Consumption");
//		table.setColumnWidth(returnQuantity, 100, Unit.PX);
//		
//		
//
//			returnQuantity.setFieldUpdater(new FieldUpdater<ProductGroupList, String>() {
//				
//				@Override
//				public void update(int index, ProductGroupList object, String value) {
//					// TODO Auto-generated method stub
//
//					rowindex = index;
//					productId = object.getProduct_id();				
//					panel = new PopupPanel(true);
//					
//					panel.add(consumeQty);
//					panel.show();
//					panel.center();
//				}
//
//				
//			});
//	}
//
//	private void addColumnViewConsumedQuantity() {
//
//		getColumnReturnQuantity = new TextColumn<ProductGroupList>() {
//			
//			@Override
//			public String getValue(ProductGroupList object) {
//				if(object.getReturnQuantity() == 0){
//					return 0+"";
//				}else{
//				return object.getReturnQuantity()+"";
//				}
//			}
//		};
//		table.addColumn(getColumnReturnQuantity, "Consumed Quantity");
//		table.setColumnWidth(getColumnReturnQuantity, 100, Unit.PX);
//	}
//
//	}
