package com.slicktechnologies.client.views.service;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.user.client.ui.ListBox;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.shared.common.businessprocesslayer.*;
import com.slicktechnologies.shared.common.businessunitlayer.*;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.Locality;
import com.slicktechnologies.shared.common.personlayer.*;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.scheduling.TeamManagement;
import com.slicktechnologies.shared.Service;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;

public class ServiceSearchPopUp extends SearchPopUpScreen<Service> implements SelectionHandler<Suggestion>{
	public IntegerBox tbServiceId;
	public IntegerBox tbContractId;
	public PersonInfoComposite personInfo;
	public ObjectListBox<Branch> olbBranch;
	public ObjectListBox<Employee> olbEmployee;
	public DateComparator dateComparator;
	public ListBox status;
	public TextBox tbRefNumber;
	 ObjectListBox<TeamManagement> olbTeam;
	
	 //  rohan added this code on date : 19-12-2017
	public ObjectListBox<Locality> olbLocality;
	 /**
	 * Date:24-01-2017 By Anil
	 * Adding reference no.2 for PCAMB and Pest Cure Incorporation
	 */
	
	TextBox tbRefNo2;
	
	/**
	 * End
	 */	
	 
	/**05-10-2017 sagar sore [To add field to search by number range(billing and non billing)]**/
	ObjectListBox<Config> olbcNumberRange;
	
	/**
	 *  nidhi
	 *  Date 4-12-2017
	 *  add service type
	 *  
	 */
	ObjectListBox<Config> olbcServiceType;
	/**
	 * end
	 */
	
	 /***
	  * Date 03-04-2019 by Vijay
	  * Des :- Cancellation Service to search
	  */
	 public DateComparator cancellationDateComparator;	
	 
	 /**
	  * Updated By:Viraj
	  * Date: 14-06-2019
	  * Description: To add hard coded status open,closed and cancelled
	  */
	 ListBox lbStatus;
	 /** Ends **/
	 /** date 28.5.2019 added by komal for nbhc schedule services**/
	 ObjectListBox<String> olbScheduleStatus;
	 ObjectListBox<CustomerBranchDetails> olbCustomerBranch; //Ashwini Patil Date:13-03-2023
	 final static GenricServiceAsync async=GWT.create(GenricService.class);
	 List<CustomerBranchDetails> customerBranchList=new ArrayList<CustomerBranchDetails>();
	 public ProductInfoComposite prodInfoComposite;
	public ServiceSearchPopUp()
	{
		super();
		createGui();
	}
	public ServiceSearchPopUp(boolean b) {
		super(b);
		createGui();
	}
	public void initWidget()
	{
		tbContractId= new IntegerBox();
		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new Customer());
		personInfo=new PersonInfoComposite(querry,false);
		olbBranch= new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbBranch);
		
		//  rohan added this on date : 19-12-2017
		
		olbLocality= new ObjectListBox<Locality>();
		olbLocality.addItem("--SELECT--");
		for (int i = 0; i < LoginPresenter.globalLocality.size(); i++) {
			
			if(LoginPresenter.globalLocality.get(i).isStatus()==true){
				olbLocality.addItem(LoginPresenter.globalLocality.get(i).getLocality());
			}
		}
		
			
		olbEmployee= new ObjectListBox<Employee>();
//		AppUtility.makeSalesPersonListBoxLive(olbEmployee);
		olbEmployee.makeEmployeeLive(AppConstants.SERVICEMODULE, AppConstants.CUSTOMERSERVICE, "Service Engineer");//Ashwini Patil Date:19-05-2023
		
		dateComparator=new DateComparator("serviceDate",new Service());
		status=new ListBox();
		AppUtility.setStatusListBox(status,Service.getStatusList());
		tbServiceId=new IntegerBox();
		
		tbRefNumber =new TextBox();
		
		tbRefNo2=new TextBox();
		
		olbTeam = new ObjectListBox<TeamManagement>();
		this.initalizeTeamListBox();
		
		/**05-10-2017 sagar sore **/
		olbcNumberRange= new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbcNumberRange,Screen.NUMBERRANGE);
		/**
		 *  nidhi
		 *  4-12-2017
		 *  
		 */
		olbcServiceType = new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbcServiceType, Screen.SERVICETYPE);
		/**
		 *  end
		 */
		
		/*** Date 03-04-2019 by Vijay for cancelled contract search filter ****/
		cancellationDateComparator = new DateComparator("cancellationDate", new Service());
		
		/**
		 * Updated By:Viraj
		 * Date: 14-06-2019
		 * Description: To add hard coded status open,closed and cancelled
		 */
		lbStatus=new ListBox();
		lbStatus.addItem("--SELECT--");
		lbStatus.addItem("Open");
		lbStatus.addItem("Completed");
		lbStatus.addItem("Cancelled");
		lbStatus.addItem("TCompletedAndCompleted");//Ashwini Patil Date:8-05-2023 code added for orion
		/** Ends **/
		olbScheduleStatus = new  ObjectListBox<String>();
		olbScheduleStatus.addItem("--SELECT--");
		olbScheduleStatus.addItem("YES");
		olbScheduleStatus.addItem("NO");
		
		olbCustomerBranch=new ObjectListBox<CustomerBranchDetails>();
		olbCustomerBranch.addItem("--SELECT--");
		olbCustomerBranch.getElement().getStyle().setMarginTop(12, Unit.PX);
		
		personInfo.getId().addSelectionHandler(this);
		personInfo.getName().addSelectionHandler(this);
		personInfo.getPhone().addSelectionHandler(this);
		
		prodInfoComposite=AppUtility.initiateServiceProductComposite(new SuperProduct(),true);
		
	}
	
	private void initalizeTeamListBox() {
		MyQuerry querry = new MyQuerry();
		querry.setQuerryObject(new TeamManagement());
		olbTeam.MakeLive(querry);
	}
	
	public void createScreen()
	{
		initWidget();
		FormFieldBuilder builder;
		builder = new FormFieldBuilder("Contract Id",tbContractId);
		FormField ftbContractId= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("",personInfo);
		FormField fPersonInfo= builder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		builder = new FormFieldBuilder("Branch",olbBranch);
		FormField folbBranch= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
//		builder = new FormFieldBuilder("Service Engineer",olbEmployee);
		/**@Sheetal:17-03-2022 
		 *  Des : Renaming Service Engineer to Technician, requirement by nitin sir**/
		builder = new FormFieldBuilder("Technician",olbEmployee);
		FormField folbEmployee= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("To Date",dateComparator.getToDate());
		FormField fdateComparator= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("From Date",dateComparator.getFromDate());
		FormField fdateComparator1= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Status",status);
		FormField fstatus= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Service Id",tbServiceId);
		FormField ftbserviceId= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Team",olbTeam);
		FormField olbTeam= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Reference Number 1",tbRefNumber);
		FormField frefNumber= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Reference Number 2",tbRefNo2);
		FormField ftbRefNo2= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/**05-10-2017 sagar sore[ adding search by number range]**/
		FormField folbcNumberRange;	
		builder = new FormFieldBuilder("Number Range",olbcNumberRange);
		folbcNumberRange= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		/**
		 *  nidhi
		 *  Date : 4-12-2017
		 *  
		 */
		FormField folbcServiceType;	
		builder = new FormFieldBuilder("Service Type",olbcServiceType);
		folbcServiceType= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		builder = new FormFieldBuilder("Locality",olbLocality);
		FormField folbLocality= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/**
		 * Date 25-01-2018 By Vijay 
		 * differentiated date combination search and other filter search
		 */
		builder = new FormFieldBuilder();
		FormField fgroupingDateFilterInformation=builder.setlabel("Date Filter Combination").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();

		builder = new FormFieldBuilder();
		FormField fgroupingOtherFilterInformation=builder.setlabel("Other Filter Combination").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		/**
		 * ends here
		 */
		
		/**
		 * Date 03-04-2019 by Vijay for Cancelled records search
		 */
		builder = new FormFieldBuilder("From Date (Cancelled Date)",cancellationDateComparator.getFromDate());
		FormField fcancellationFrmDateComparator= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("To Date (Cancelled Date)",cancellationDateComparator.getToDate());
		FormField fcancellationToDateComparator= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		/**
		 * Updated By:Viraj
		 * Date: 14-06-2019
		 * Description: To add hard coded status open,closed and cancelled
		 */
		builder = new FormFieldBuilder("Service Status",lbStatus);
		FormField flbStatus = builder.setMandatory(false).setMandatoryMsg("Status is mandatory").setRowSpan(0).setColSpan(0).build();
		/** Ends **/
		
		//old name "Schedule Status"
		//Ashwini Patil Date:5-02-2024 changed label to "Visible to technician"  
		builder = new FormFieldBuilder("Visible to Technician",olbScheduleStatus);
		FormField folbScheduleStatus= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Customer Branch",olbCustomerBranch);
		FormField folbCustomerBranch= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("",prodInfoComposite);
		FormField fprodInfoComposite= builder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		
		FormField field[] = new FormField[5];
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "OnlyForNBHC")){
			field[0] = fdateComparator1;
			field[1] = fdateComparator;
			field[2] = folbBranch;
			field[3] = fstatus;
			field[4] = folbScheduleStatus;
		}else{
			field[0] = fdateComparator1;
			field[1] = fdateComparator;
			field[2] = folbBranch;
			field[3] = fstatus;
			field[4] = folbScheduleStatus;
		}
		
		
		this.fields=new FormField[][]{
				/**05-10-2017 sagar sore[To add Numbe range field in search pop up]**/
				{fgroupingDateFilterInformation},
				field ,
				/** date 17.04.2018 added by komal for orion**/
				{fPersonInfo,folbCustomerBranch},
				{fcancellationFrmDateComparator,fcancellationToDateComparator,flbStatus,folbEmployee},
				{fgroupingOtherFilterInformation},
				/**old code**/
//				{folbEmployee,folbBranch,fstatus},
				/**updated**/
				{ftbserviceId,ftbContractId,folbcNumberRange,folbcServiceType},
//				{olbTeam,frefNumber,ftbRefNo2},
//				{folbEmployee,folbBranch,fstatus,folbcNumberRange},
				{olbTeam,frefNumber,ftbRefNo2,folbLocality},
				{fprodInfoComposite}
			//	{fPersonInfo,}
		};
	}
	public MyQuerry getQuerry()
	{
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		if(dateComparator.getValue()!=null)
			filtervec.addAll(dateComparator.getValue());
		
		if(olbTeam.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbTeam.getValue().trim());
			temp.setQuerryString("team");
			filtervec.add(temp);
		}
		
		if(olbEmployee.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbEmployee.getValue().trim());
			temp.setQuerryString("employee");
			filtervec.add(temp);
		}
		
		if(LoginPresenter.branchRestrictionFlag==true && olbBranch.getSelectedIndex()==0)
		{
			temp=new Filter();
			temp.setList(AppUtility.getbranchlist());
			temp.setQuerryString("branch IN");
			filtervec.add(temp);
		}
		else{
			if(olbBranch.getSelectedIndex()!=0){
				temp=new Filter();temp.setStringValue(olbBranch.getValue().trim());
				temp.setQuerryString("branch");
				filtervec.add(temp);
			}
		}
		
		
		// rohan added this code for pest guard on date : 19-12-2017
		
		if(olbLocality.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbLocality.getValue(olbLocality.getSelectedIndex()).trim());
			temp.setQuerryString("address.locality");
			filtervec.add(temp);
		}
		
		if(tbContractId.getValue()!=null){
			temp=new Filter();
			temp.setIntValue(tbContractId.getValue());
			filtervec.add(temp);
			temp.setQuerryString("contractCount");
			filtervec.add(temp);
		}
		if(personInfo.getIdValue()!=-1)
		  {
		  temp=new Filter();
		  temp.setIntValue(personInfo.getIdValue());
		  temp.setQuerryString("personInfo.count");
		  filtervec.add(temp);
		  }
		  
		  if(!(personInfo.getFullNameValue().equals("")))
		  {
		  temp=new Filter();
		  temp.setStringValue(personInfo.getFullNameValue());
		  temp.setQuerryString("personInfo.fullName");
		  filtervec.add(temp);
		  }
		  /**
		   * Commented By Priyanka
		   * Des : Search data using only Id and name as per requiremnet of Rahul Tiwari
		   * @author Anil @since 23-09-2021
		   * Adding cell number filter becuase indexes are uploaded in combination of cell number
		   * if we remove it data will not search properly
		   * @author Anil @since 25-10-2021
		   * For pecopp : added new indexes and commented this
		   */
//		  if(personInfo.getCellValue()!=-1l)
//		  {
//		  temp=new Filter();
//		  temp.setLongValue(personInfo.getCellValue());
//		  temp.setQuerryString("personInfo.cellNumber");
//		  filtervec.add(temp);
//		  }
		  
		  if(!tbRefNumber.getValue().equals("")){
				temp=new Filter();
				temp.setStringValue(tbRefNumber.getValue());
				filtervec.add(temp);
				temp.setQuerryString("refNo");
				filtervec.add(temp);

			}
		 
		  /**@author Abhinav Bihade
		   * @Since 23/01/2020
		   * NBHC- Add two additional status 'Schedule / Reschedule' and 'Started / Reported / Tcompleted '
		   * on Service status dropdown list and data be searched accordingly from service search screen and service list search screen
		   */
		
		  if(this.status.getSelectedIndex()!=0)
		  {
			String selectedItem = status.getItemText(status.getSelectedIndex());
			Console.log("Abhinav inside status: "+selectedItem);
			if (selectedItem.equals(Service.SERVICESTATUSSCHEDULERESCHEDULE)) {
				Console.log("Abhinav inside status:1st if 1");
				ArrayList<String> scheduleRescheduleList = new ArrayList<String>();
				scheduleRescheduleList.add(Service.SERVICESTATUSSCHEDULE);
				scheduleRescheduleList.add(Service.SERVICESTATUSRESCHEDULE);
				Console.log("Abhinav inside status:1st if 2"+scheduleRescheduleList.size());
				temp=new Filter();
				  temp.setList(scheduleRescheduleList);
				  temp.setQuerryString("status IN");
				  filtervec.add(temp);
				  }
			
			else if(selectedItem.equals(Service.SERVICESTATUSSTARTEDREPORTEDTCOMPLETED)){
				Console.log("Abhinav inside status:else if1");
					ArrayList<String> startedReportedTcompletedList = new ArrayList<String>();
					startedReportedTcompletedList.add(Service.SERVICESTATUSSTARTED);
					startedReportedTcompletedList.add(Service.SERVICESTATUSREPORTED);
					startedReportedTcompletedList.add(Service.SERVICETCOMPLETED);
					
					Console.log("Abhinav inside status:else if2"+startedReportedTcompletedList.size());
					temp=new Filter();
					  temp.setList(startedReportedTcompletedList);
					  temp.setQuerryString("status IN");
					  filtervec.add(temp);
					
				}
			else {
				Console.log("Abhinav inside status:else1");
				temp = new Filter();
//				selectedItem = status.getItemText(status
//						.getSelectedIndex());
				temp.setStringValue(selectedItem);
				temp.setQuerryString("status");
				filtervec.add(temp);
				Console.log("Abhinav inside status:else1"+temp);
			}
			}
			
		  
		  if(tbServiceId.getValue()!=null)
		  {
		  temp=new Filter();
		  
		  temp.setIntValue(tbServiceId.getValue());
		  temp.setQuerryString("count");
		  filtervec.add(temp);
		  }
		  
		  if(!tbRefNo2.getValue().equals("")){
			temp=new Filter();
			temp.setStringValue(tbRefNo2.getValue());
			temp.setQuerryString("refNo2");
			filtervec.add(temp);
		  }
		
		  /***05-10-2017 sagar sore[ filter for number range]**/
		  if(olbcNumberRange.getSelectedIndex()!=0){
				temp=new Filter();
				temp.setStringValue(olbcNumberRange.getValue().trim());
				temp.setQuerryString("numberRange");
				filtervec.add(temp);
			}
		  /**
		   *  nidhi
		   *  Date : 4-12-2017
		   *  
		   */
		  if(olbcServiceType.getSelectedIndex()!=0){
				temp=new Filter();
				temp.setStringValue(olbcServiceType.getValue().trim());
				temp.setQuerryString("serviceType");
				filtervec.add(temp);
			}
		  
	    /**
		 * Date : 03-04-2019 by Vijay for cancelled Date search filter
		 */
		if(cancellationDateComparator.getValue()!=null){
			filtervec.addAll(cancellationDateComparator.getValue());
		}
			
		/**
		 * Updated By:Viraj
		 * Date:14-06-2019
		 * Description: To filter status according to selected value
		 */
		if(lbStatus.getSelectedIndex() != 0) {
			List<String> statusList=new ArrayList<String>();
			statusList =  Service.getStatusList();
			
			Console.log("inside status");
			if(lbStatus.getItemText(lbStatus.getSelectedIndex()).equalsIgnoreCase("open")) {
				for(int i=0;i<statusList.size();i++){
					if(statusList.get(i).trim().equalsIgnoreCase("Closed") || statusList.get(i).trim().equalsIgnoreCase("Cancelled") || statusList.get(i).trim().equalsIgnoreCase("Completed")){
						statusList.remove(i);
						i--;
					}
				}
				Console.log("StatusList Size: "+statusList.size());
				temp=new Filter();
				temp.setList(statusList);
				temp.setQuerryString("status IN");
				filtervec.add(temp);
			}else if(lbStatus.getItemText(lbStatus.getSelectedIndex()).equalsIgnoreCase("Completed")){
				Console.log("Status in Completed: "+lbStatus.getItemText(lbStatus.getSelectedIndex()));
				temp=new Filter();
				temp.setStringValue(lbStatus.getItemText(lbStatus.getSelectedIndex()));
				temp.setQuerryString("status");
				filtervec.add(temp);
			}else if(lbStatus.getItemText(lbStatus.getSelectedIndex()).equalsIgnoreCase("Cancelled")){
				Console.log("Status in Cancelled: "+lbStatus.getItemText(lbStatus.getSelectedIndex()));
				temp=new Filter();
				temp.setStringValue(lbStatus.getItemText(lbStatus.getSelectedIndex()));
				temp.setQuerryString("status");
				filtervec.add(temp);
			}
			//Ashwini Patil Date:8-05-2023 code added for orion
			else if(lbStatus.getItemText(lbStatus.getSelectedIndex()).equalsIgnoreCase("TCompletedAndCompleted")) {
				statusList=new ArrayList<String>();
				Console.log("Status list size before="+statusList.size());
				statusList.add(Service.SERVICESTATUSCOMPLETED);
				statusList.add(Service.SERVICETCOMPLETED);
				Console.log("StatusList Size: "+statusList.size());
				temp=new Filter();
				temp.setList(statusList);
				temp.setQuerryString("status IN");
				filtervec.add(temp);
			}
		}
		/** Ends **/
		if(olbScheduleStatus.getSelectedIndex()!=0){
			 boolean value = false;
			 if(olbScheduleStatus.getValue(olbScheduleStatus.getSelectedIndex()).equalsIgnoreCase("YES")){
				 value = true;
			 }
				temp=new Filter();
				temp.setBooleanvalue(value);
				temp.setQuerryString("isServiceScheduled");
				filtervec.add(temp);
			}
		
		System.out.println("AppMemory.getAppMemory().currentScreen =="+AppMemory.getAppMemory().currentScreen);
		if(AppMemory.getAppMemory().currentScreen == Screen.INHOUSESERVICE){
			temp=new Filter();
			temp.setBooleanvalue(true);
			temp.setQuerryString("wmsServiceFlag");
			filtervec.add(temp);
			Console.log("Fiter for In house service list");
		}
		else{
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "EnableRestrictInhouseServices")){
				temp=new Filter();
				temp.setBooleanvalue(false);
				temp.setQuerryString("wmsServiceFlag");
				filtervec.add(temp);
			}
		}
		
		if(olbCustomerBranch.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbCustomerBranch.getValue().trim());
			temp.setQuerryString("serviceBranch");
			filtervec.add(temp);
		}
		
		if(prodInfoComposite.getValue()!=null){
			temp=new Filter();
			temp.setIntValue(prodInfoComposite.getIdValue());
			temp.setQuerryString("product.count");
			filtervec.add(temp);
		 }

		MyQuerry querry= new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Service());
		return querry;
	}
	
	public InlineLabel getDwnload() {
		return dwnload;
	}
	@Override
	public boolean validate() {
//		 if(LoginPresenter.branchRestrictionFlag)
//		{
//		if(olbBranch.getSelectedIndex()==0)
//		{
//			showDialogMessage("Select Branch");
//			return false;
//		}
//		}
		boolean flag = false;
		if(personInfo.getValue()!=null){
			flag = true;
		}
		if(!flag){
			if((dateComparator.getFromDate().getValue()!=null && dateComparator.getToDate().getValue()!=null)){
				int diffdays = AppUtility.getDifferenceDays(dateComparator.getFromDate().getValue(), dateComparator.getToDate().getValue());
				Console.log("diffdays "+diffdays);
				if(diffdays>31){
					showDialogMessage("Please select from date and to date range within 30 days");
					return false;
				}
			}
			
			if((cancellationDateComparator.getFromDate().getValue()!=null && cancellationDateComparator.getToDate().getValue()!=null)){
				int diffdays = AppUtility.getDifferenceDays(cancellationDateComparator.getFromDate().getValue(), cancellationDateComparator.getToDate().getValue());
				Console.log("diffdays "+diffdays);
				if(diffdays>31){
					showDialogMessage("Please select from date and to date range within 30 days");
					return false;
				}
			}
		}
		if(prodInfoComposite.getValue()!=null){
			if(tbContractId.getValue()==null&&personInfo.getIdValue()==-1) {
				showDialogMessage("Select either Contract ID or Customer along with product");
				return false;
			}
		}
		
		
	 	/**
		 * Date 25-01-2018 By Vijay 
		 * for Date combination validation
		 */
		String msg = "Wrong filter selection with From Date and To Date ! Please select proper filters with Date";
		if((dateComparator.getFromDate().getValue()!=null || dateComparator.getToDate().getValue()!=null) ||
				(cancellationDateComparator.getFromDate().getValue()!=null || cancellationDateComparator.getToDate().getValue()!=null)){
			if(olbTeam.getSelectedIndex()!=0 || 
			   olbLocality.getSelectedIndex()!=0 || /** personInfo.getIdValue()!=-1 ||	
			   !(personInfo.getFullNameValue().equals("")) || personInfo.getCellValue()!=-1l ||**/ !tbRefNumber.getValue().equals("")  || 
			     tbServiceId.getValue()!=null || !tbRefNo2.getValue().equals("") ||
			    olbcNumberRange.getSelectedIndex()!=0 || olbcServiceType.getSelectedIndex()!=0 ){
				showDialogMessage(msg);
				return false;
			}
		}
		/**
		 * ends here
		 */
		return true;
	}
	@Override
	public void onSelection(SelectionEvent<Suggestion> event) {
		// TODO Auto-generated method stub
		if(event.getSource().equals(personInfo.getId())||event.getSource().equals(personInfo.getName())||event.getSource().equals(personInfo.getPhone())){
			olbCustomerBranch.clear();
			loadCustomerBranch(Integer.parseInt(personInfo.getId().getValue().trim()), null);
			
		}
	}
	
	public void loadCustomerBranch(int custId,final String custBranch){
		customerBranchList=new ArrayList<CustomerBranchDetails>();
		MyQuerry querry = new MyQuerry();
		Company c = new Company();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("cinfo.count");
		filter.setIntValue(custId);
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new CustomerBranchDetails());

		async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				olbCustomerBranch.clear();
				olbCustomerBranch.addItem("--SELECT--");
				showDialogMessage("failed." + caught);
			}
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				ArrayList<CustomerBranchDetails> custBranchList=new ArrayList<CustomerBranchDetails>();
				if(result.size()!=0){
					for(SuperModel model:result){
						CustomerBranchDetails obj=(CustomerBranchDetails) model;
						custBranchList.add(obj);
						customerBranchList.add(obj);
					}
				}
				
				if(custBranchList.size()!=0){
					olbCustomerBranch.setListItems(custBranchList);
					if(custBranch!=null){
						olbCustomerBranch.setValue(custBranch);
						if(olbCustomerBranch.getValue()!=null){
						
//							olbCustomerBranch.customerBranchName=olbCustomerBranch.getValue();
//							olbCustomerBranch.customerBranchId=olbCustomerBranch.getSelectedItem().getCount();
						}
					}else{
						olbCustomerBranch.setEnabled(true);
					}
				}else{
					olbCustomerBranch.addItem("--SELECT--");
				}
				
//				olbCustomerBranch.cinfo=personInfoComposite.getValue();
				
			}
		});
	}
}
