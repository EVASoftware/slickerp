package com.slicktechnologies.client.views.service;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.TableSelection;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreen;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
/**
 * Table screen template
 */
public class ServiceForm extends TableScreen<Service>   implements ClickHandler{

	//Token to add variable declaration
	//
	CheckBox cbSelect;
	
	/**
	 * @author Anil
	 * @since 16-06-2020
	 * 
	 */
	
	public boolean complainTatFlag=false;
	
	public static int rowcount;
	
	public ServiceForm  (SuperTable<Service> superTable) {
		super(superTable);
		createGui();
		if(getProcesslevelBarNames().length>0)
		toggleProcessLevelMenu();
		
		complainTatFlag=AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "ComplainServiceWithTurnAroundTime");
	}
	
	
	@Override
	public void createScreen() {
		/**
		 * Date : 21-02-2017 By Anil
		 * Added Cancel services Button which will be only visible for user with ADMIN role.
		 * @author Anil @since 07-12-2021
		 * For other than admin role instead of Print Schedule , Schedule Print label was coming so renamed it
		 * Raised by Nitin Sir
		 */
		/**Date :14/11/2017 By Manisha
		/**removed print service button**/
		System.out.println("LoginPresenter.myUserEntity.getRoleName()"+LoginPresenter.myUserEntity.getRoleName());
		if(LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN") || LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("Zonal Coordinator")){
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","UpdateRecordButtonVisibility")){
				this.processlevelBarNames = new String []{AppConstants.APPROVALVIEWDOCUMENT,AppConstants.VIEWCONTRACT,"Material Issue",AppConstants.SCHEDULEMULTIPLESERVICES,"Print Schedule",AppConstants.Reschedule,AppConstants.ASSIGNTECHNICIAN/**nidhi 2-08-2018 for assign multple technician to many service at a time*/,AppConstants.ASSIGNBRANCH,AppConstants.MARKCOMPLETE,AppConstants.SENDSRMAIL,AppConstants.SUSPENDSERVICES,AppConstants.RESUMESERVICES,AppConstants.ASSIGNSTATUS,"Remove Duplicates", AppConstants.CANCELSERVICES,AppConstants.CANCELPARTIALLYCANCELLEDSERVICES,AppConstants.UPDATEQUICCONTRACTSERVICES,AppConstants.UPDATEQUICLCONTRACTBILLING, "Update Component Detail","View Complain",AppConstants.RePlanServices,AppConstants.CANCELLOCATIONWISESERVICES,AppConstants.DISTRIBUTESERVICESEQUALLYONGIVENDATES,AppConstants.ScheduleOrCompleteViaExcel,AppConstants.downloadReports};
			}else{
				this.processlevelBarNames = new String []{AppConstants.APPROVALVIEWDOCUMENT,AppConstants.VIEWCONTRACT,"Material Issue",AppConstants.SCHEDULEMULTIPLESERVICES,"Print Schedule",AppConstants.Reschedule,AppConstants.ASSIGNTECHNICIAN/**nidhi 2-08-2018 for assign multple technician to many service at a time*/,AppConstants.ASSIGNBRANCH,AppConstants.MARKCOMPLETE,AppConstants.SENDSRMAIL,AppConstants.SUSPENDSERVICES,AppConstants.RESUMESERVICES,AppConstants.ASSIGNSTATUS,"Remove Duplicates", AppConstants.CANCELSERVICES,"Update Component Detail","View Complain",AppConstants.RePlanServices,AppConstants.CANCELLOCATIONWISESERVICES,AppConstants.DISTRIBUTESERVICESEQUALLYONGIVENDATES,AppConstants.ScheduleOrCompleteViaExcel,AppConstants.downloadReports};
			}//Ashwini Patil Date:5-03-2022 removed  "Material Summary" as there was no further code written for it
		}else if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","ServiceStatusAsPlanned")){
			this.processlevelBarNames = new String []{AppConstants.VIEWCONTRACT,AppConstants.APPROVALVIEWDOCUMENT,"Print Schedule",AppConstants.MARKCOMPLETE,AppConstants.ASSIGNTECHNICIAN,AppConstants.ASSIGNSTATUS,AppConstants.SCHEDULEMULTIPLESERVICES,AppConstants.RePlanServices,AppConstants.CANCELLOCATIONWISESERVICES,AppConstants.DISTRIBUTESERVICESEQUALLYONGIVENDATES,AppConstants.ScheduleOrCompleteViaExcel,AppConstants.downloadReports};
		}else{
			this.processlevelBarNames = new String []{AppConstants.VIEWCONTRACT,AppConstants.APPROVALVIEWDOCUMENT,"Print Summary", "Create MIN" ,"Print Schedule",AppConstants.SUSPENDSERVICES,AppConstants.RESUMESERVICES,AppConstants.MARKCOMPLETE,AppConstants.ASSIGNTECHNICIAN/**nidhi 2-08-2018 for assign multple technician to many service at a time*/,AppConstants.Reschedule,AppConstants.ASSIGNBRANCH,AppConstants.ASSIGNSTATUS,AppConstants.SCHEDULEMULTIPLESERVICES,
					AppConstants.SENDSRMAIL,"Remove Duplicates","Update Component Detail","View Complain",AppConstants.RePlanServices,AppConstants.CANCELLOCATIONWISESERVICES,AppConstants.DISTRIBUTESERVICESEQUALLYONGIVENDATES,AppConstants.ScheduleOrCompleteViaExcel,AppConstants.downloadReports};//,AppConstants.SUSPENDSERVICES,AppConstants.RESUMESERVICES,AppConstants.MARKCOMPLETE
		}
			
			
			

			
			
			
			
		
//		toggleProcessLevelMenu();
	}
	public void toggleProcessLevelMenu()
	{
//		Service entity=(Service) presenter.getModel();
//		String status=entity.getStatus();
		ArrayList<String> pCodeList = new ArrayList<String>();
		pCodeList.add("STK-01");
		pCodeList.add("SLM-01");
		pCodeList.add("AFC-10");
		pCodeList.add("CON-02");
		pCodeList.add("AFC-02");
		pCodeList.add("BNF-01");
		pCodeList.add("CON-01");

//		String ProCode = entity.getProduct().getProductCode();
		for(int i=0;i<getProcesslevelBarNames().length;i++){
			InlineLabel label=getProcessLevelBar().btnLabels[i];
			String text=label.getText().trim();
			//AppConstants.SUSPENDSERVICES,AppConstants.RESUMESERVICES,AppConstants.MARKCOMPLETE
			if(LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN") || LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("Zonal Coordinator")){
						label.setVisible(true);
			}else{
				if(text.equals(AppConstants.CANCELSERVICES)
						|| AppConstants.CANCELPARTIALLYCANCELLEDSERVICES.equalsIgnoreCase(text)
						|| AppConstants.UPDATEQUICCONTRACTSERVICES.equalsIgnoreCase(text)
						||AppConstants.UPDATEQUICLCONTRACTBILLING.equalsIgnoreCase(text)){
					label.setVisible(false);
				}
				if (text.equalsIgnoreCase(AppConstants.SUSPENDSERVICES) && AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","HideServiceListServiceSuspendButton")){
					label.setVisible(false);
				}else if (text.equalsIgnoreCase(AppConstants.RESUMESERVICES) && AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","HideServiceListServiceResumeButton")){
					label.setVisible(false);
				}else if (text.equalsIgnoreCase(AppConstants.MARKCOMPLETE) && AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","HideServiceListMarkCompleteButton")){
					label.setVisible(false);
				}
			}
			
			if(!complainTatFlag){
				if(text.equals("Update Component Detail")){
					label.setVisible(false);
				}
				if(text.equals("View Complain")){
					label.setVisible(false);
				}
			}
			
			
			//Ashwini Patil Date:20-09-2022
			if(isHideNavigationButtons()) {
				Console.log("in changeProcessLevel isHideNavigationButtons");
				if(text.equalsIgnoreCase("View Document")||text.equalsIgnoreCase(AppConstants.VIEWCONTRACT)||text.equalsIgnoreCase("View Complain"))
					label.setVisible(false);
			}
		}
	}

	/**
	 * method template to update the model with token entity name
	 */
	@Override
	public void updateModel(Service model)
	{
		//<ReplasableUpdateModelFromViewCode>
	}

	@SuppressWarnings("unused")
	private void initalizeWidget()
	{
		//<ReplasbleVariableInitalizerBlock>
	}

	/**
	 * method template to update the view with token entity name
	 */
	@Override
	public void updateView(Service view) 
	{
		//<ReplasableUpdateViewFromModelCode>
	}
	
	
	@Override
	public void onClick(ClickEvent event) {
		
	}

	/**
	 * Toggles the app header bar menus as per screen state
	 */
	public void toggleAppHeaderBarMenu() {
		if(superTable!=null&&superTable.getListDataProvider().getList()!=null){
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Rows:")){
					menus[k].setText("Rows:"+superTable.getListDataProvider().getList().size());	
					menus[k].getElement().setId("addlabel2");
				}
			}
		}
		
			if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Print")||text.equals("Search")||text.equals("Download")||text.equals(AppConstants.NAVIGATION) || text.contains(AppConstants.COMMUNICATIONLOG)||text.contains("Rows:"))
				{
					menus[k].setVisible(true); 
					
				}
				else
					menus[k].setVisible(false);  		  		
			}
		}
		
		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals("Download")||text.equals(AppConstants.NAVIGATION)||text.contains("Rows:"))
					menus[k].setVisible(true); 
				else
					menus[k].setVisible(false);  		   
			}
		}
		
		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			if(superTable!=null&&superTable.getListDataProvider().getList()!=null)
				Console.log("count="+superTable.getListDataProvider().getList().size());
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				Console.log("in is popup menu bar");
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else
				menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Print")||text.equals("Search")||text.equals("Download")||text.equals(AppConstants.NAVIGATION) || text.contains("Email") || text.contains(AppConstants.MESSAGE) || text.contains(AppConstants.COMMUNICATIONLOG)||text.contains("Rows:")){
					menus[k].setVisible(true); 
				}else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.SERVICE,"Customer Service List");//LoginPresenter.currentModule.trim() changed on 18-10-2022
	}


	@Override
	public void retriveTable(MyQuerry querry) {
		superTable.connectToLocal();
		service.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				
				System.out.println("Rohan in side Service form ");
				
				
				if(result.get(0) instanceof Service)
				{
					System.out.println("in side condition of method");
					result = sortingTbaleBySrNoAndContractNo(result);
				}
				
				for(SuperModel model:result){
					superTable.getListDataProvider().getList().add((Service) model);
//				superTable.getTable().setVisibleRange(0,result.size());
				}
				
			}
			
			@Override
			public void onFailure(Throwable caught) {
			caught.printStackTrace();
				
			}
		});
		toggleProcessLevelMenu();
	}

	private ArrayList<SuperModel> sortingTbaleBySrNoAndContractNo(ArrayList<SuperModel> result) {
		System.out.println("Rohan inside method ");
		
		ArrayList<Service> serlist = new ArrayList<Service>(); 
		ArrayList<SuperModel> supModel = new ArrayList<SuperModel>(); 
		
		for(SuperModel model : result)
		{
			Service ser = (Service) model;
			
			/**
			 * @author Anil
			 * @since 20-05-2020
			 */
			ser.setRecordSelect(false);
			serlist.add(ser);
		}
		System.out.println("Service list size "+serlist.size());
			Comparator<Service> comp1 =new Comparator<Service>() {
				
				@Override
				public int compare(Service o1, Service o2) {
					Integer srNo1 = o1.getServiceSerialNo();
					Integer srNo2 = o2.getServiceSerialNo();
					return srNo1.compareTo(srNo2);
				}
			};
			Collections.sort(serlist , comp1);
		
		
			
		Comparator<Service> comp2 =  new Comparator<Service>() {
			
			@Override
			public int compare(Service p1, Service p2) {
				
				Integer con1 = p1.getContractCount();
				Integer con2 = p2.getContractCount(); 
				return con1.compareTo(con2);
			}
		};
		Collections.sort(serlist ,comp2);
		
		
		
		for(Service service : serlist)
		{
			SuperModel ser = (SuperModel) service;
			supModel.add(ser);
		}
		
		return supModel;
		
	}

	 public void setToNewState() {
		super.setToNewState();
		toggleProcessLevelMenu();
	};
	@Override
	public void setToViewState() {
	
		super.setToViewState();
		toggleProcessLevelMenu();
		//Ashwini Patil Date:28-08-2023
		Console.log("In serviceform settoviewstate");
		if(superTable!=null&&superTable.getListDataProvider().getList()!=null&&superTable.getListDataProvider().getList().size()>0){
			Console.log("In serviceform settoviewstate supertable not null size="+superTable.getListDataProvider().getList().size()+" getSearchpopupscreen().getLblCount().getText()="+getSearchpopupscreen().getLblCount().getText());
			
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Rows:")){
					Console.log("In serviceform settoviewstate label=rows");
					menus[k].setText("Rows:"+superTable.getListDataProvider().getList().size());	
					menus[k].getElement().setId("addlabel2");
				}
			}
		}else{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Rows:")){
					Console.log("In serviceform settoviewstate else label=rows");
					menus[k].setText("Rows:"+rowcount);	
					menus[k].getElement().setId("addlabel2");
				}
			}
		}
	}
	

}
