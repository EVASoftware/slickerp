package com.slicktechnologies.client.views.service;
import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.code.p.gwtchismes.client.GWTCGlassPanel;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.StatusCodeException;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.view.client.NoSelectionModel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AbstractScreen;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ProcessLevelBar;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.*;
import com.simplesoftwares.client.library.appstructure.search.*;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.*;
import java.util.logging.Level;

import com.simplesoftwares.client.library.appstructure.tablescreen.*;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.libservice.PdfService;
import com.simplesoftwares.client.library.libservice.PdfServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.reusabledata.ConditionDialogBox;
import com.slicktechnologies.client.services.CommonService;
import com.slicktechnologies.client.services.CommonServiceAsync;
import com.slicktechnologies.client.services.CommunicationLogService;
import com.slicktechnologies.client.services.CommunicationLogServiceAsync;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.DataMigrationService;
import com.slicktechnologies.client.services.DataMigrationServiceAsync;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.services.ServiceListService;
import com.slicktechnologies.client.services.ServiceListServiceAsync;
import com.slicktechnologies.client.services.SmsService;
import com.slicktechnologies.client.services.SmsServiceAsync;
import com.slicktechnologies.client.services.UpdateService;
import com.slicktechnologies.client.services.UpdateServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.approval.ApprovalForm;
import com.slicktechnologies.client.views.approval.ApprovalPresenter;
import com.slicktechnologies.client.views.approval.ApprovalTable;
import com.slicktechnologies.client.views.communicationlog.CommunicationLogPopUp;
import com.slicktechnologies.client.views.complain.ComplainForm;
import com.slicktechnologies.client.views.complain.ComplainPresenter;
import com.slicktechnologies.client.views.complain.ServiceDateBoxPopup;
import com.slicktechnologies.client.views.contract.ContractForm;
import com.slicktechnologies.client.views.contract.ContractPresenter;
import com.slicktechnologies.client.views.contractrenewal.RenewalResult;
import com.slicktechnologies.client.views.customer.CustomerPresenter;
import com.slicktechnologies.client.views.customer.CustomerPresenterSearchProxy;
import com.slicktechnologies.client.views.customer.CustomerPresenter.CustomerPresenterSearch;
import com.slicktechnologies.client.views.device.DeviceForm;
import com.slicktechnologies.client.views.device.DevicePresenter;
import com.slicktechnologies.client.views.device.SaveService;
import com.slicktechnologies.client.views.device.SaveServiceAsync;
import com.slicktechnologies.client.views.device.ServicReschdulePopUp;
import com.slicktechnologies.client.views.documentcancellation.DocumentCancellationPopUp;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.FromAndToDateBoxPopup;
import com.slicktechnologies.client.views.popups.NewEmailPopUp;
import com.slicktechnologies.client.views.popups.ReScheduleServicesPopUp;
import com.slicktechnologies.client.views.popups.SMSPopUp;
import com.slicktechnologies.client.views.popups.SRSchedulePrintPopup;
import com.slicktechnologies.client.views.popups.ServiceCancellationPopup;
import com.slicktechnologies.client.views.popups.ServiceCompleteFromSchedulePopup;
import com.slicktechnologies.client.views.popups.ServiceListServiceAssignBranchPopup;
import com.slicktechnologies.client.views.popups.ServiceListServiceAssignStatusPopup;
import com.slicktechnologies.client.views.popups.ServicelistServiceAssignTechnicianPopup;
import com.slicktechnologies.client.views.popups.ServicelistServiceMarkCompleptePopup;
import com.slicktechnologies.client.views.popups.ServicesCancellationPopup;
import com.slicktechnologies.client.views.popups.WeekDaysPopup;
import com.slicktechnologies.client.views.screenmenuconfiguration.UploadPopup;
import com.slicktechnologies.client.views.settings.employee.EmployeeBranch;
import com.slicktechnologies.client.views.technicianwarehousedetails.TechnicianServiceSchedulePopup;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.*;
import com.slicktechnologies.shared.common.*;
import com.slicktechnologies.shared.common.businessprocesslayer.*;
import com.slicktechnologies.shared.common.businessunitlayer.*;
import com.slicktechnologies.shared.common.complain.Complain;
import com.slicktechnologies.shared.common.contactidentification.ContactPersonIdentification;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.*;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;
import com.slicktechnologies.shared.common.personlayer.*;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
import com.slicktechnologies.shared.common.productlayer.*;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.servicerelated.ServiceSchedule;
import com.slicktechnologies.client.views.multipleserviceschedule.MultipleServicesSchedulePopup;

/**
 * TableScreen presenter template.
 */
public class ServicePresenter extends TableScreenPresenter<Service> implements ClickHandler{
	TableScreen<Service>form;
	private static final PdfServiceAsync pdfservice = GWT
			.create(PdfService.class);
	static GWTCGlassPanel glassPanel=new GWTCGlassPanel();
	
	final ServiceListServiceAsync servicelistserviceAsync = GWT.create(ServiceListService.class);
	int cnt = 0;
	Service serviceObject;
	
	/**
	 * Date : 21-02-2017 by Anil
	 */
	DocumentCancellationPopUp cancellationPopup = new DocumentCancellationPopUp();
	DocumentCancellationPopUp suspendPopup = new DocumentCancellationPopUp();
	ConditionDialogBox conditionPopup = new ConditionDialogBox(
			"Do you want to print on preprinted Stationery", AppConstants.YES,
			AppConstants.NO);
	PopupPanel panel=new PopupPanel(true);
	UpdateServiceAsync updateService=GWT.create(UpdateService.class);
	GenricServiceAsync genasync=GWT.create(GenricService.class);
	/** date 26.02.2018 added by komal for schedule print  **/
	ServiceSchedulePrintPopup serviceSchedulePrintPopup = new ServiceSchedulePrintPopup();
	
	
	/**
	 * Date 04-05-2018
	 * Developer :- Vijay
	 * Description :- services list complete services with bulk popup
	 */
	ServicelistServiceMarkCompleptePopup serviceCompletionPopup = new ServicelistServiceMarkCompleptePopup();
	/**
	 * ends here
	 */
	/**
	 * nidhi
	 * 2-08-2018
	 * for assign technician
	 * 
	 * @param view
	 * @param model
	 */
	ServicelistServiceAssignTechnicianPopup serviceTechnicianPopup = new ServicelistServiceAssignTechnicianPopup();

	/**
	 * Date 07-09-2018 By Vijay
	 * Developer :- Vijay
	 * Description :- Reschedule services with bulk
	 */
	ServicReschdulePopUp reschedule=new ServicReschdulePopUp();
	/**
	 * ends here
	 */
	/**
	 * nidhi
	 *  15-10-2018 ||*
	 * @param view
	 * @param model
	 */
	SaveServiceAsync mapService=GWT.create(SaveService.class);
	
	/**
	 * Date 23-03-2019 by Vijay for NBHC CCPM service cancellation with dropdown 
	 */
	ServiceCancellationPopup  cancellation = new ServiceCancellationPopup();
	/**
	 * Updated By: Viraj
	 * Date: 15-06-2019
	 * Description: Assign branch in bulk
	 */
	ServiceListServiceAssignBranchPopup serviceBranchPopup = new ServiceListServiceAssignBranchPopup();
	/** Ends **/
	
	/**
	 * Updated By: Viraj
	 * Date: 29-06-2019
	 * description: To change status for multiple entries in one go
	 */
	ServiceListServiceAssignStatusPopup serviceStatusPopup = new ServiceListServiceAssignStatusPopup();
	
	/** date 13.05.2019 added by komal to schedule multiple services **/
	MultipleServicesSchedulePopup multipleServicesSchedulePopup = new MultipleServicesSchedulePopup();
	
	final GeneralServiceAsync async=GWT.create(GeneralService.class);
	
	FromAndToDateBoxPopup datePopUp=new FromAndToDateBoxPopup(true,"Service Id");
	PopupPanel datePanel=new PopupPanel();
	/**
	 * Date 14-11-2019 
	 * @author Vijay Chougule
	 * Des :- NBHC CCPM complaint service validation for if Assessment is in created status
	 */
	CommunicationLogServiceAsync validateComplaintService = GWT.create(CommunicationLogService.class);
	
	/**
	 * @author Anil
	 * @since 16-06-2020
	 */
	UpdateComponentDetailsPopup componentPopup=new UpdateComponentDetailsPopup();

	GeneralViewDocumentPopup genralInvoicePopup = new GeneralViewDocumentPopup();
	public PopupPanel generalPanel;
	public static boolean viewDocumentFlag=true;
	public static boolean scheduleServiceFlag=false;//Ashwini Patil Date:7-11-2022

	final CommonServiceAsync commonserviceasync=GWT.create(CommonService.class);

	NewEmailPopUp emailpopup = new NewEmailPopUp();
	SMSPopUp smspopup = new SMSPopUp();
	
	ReScheduleServicesPopUp replanservicepopup = new ReScheduleServicesPopUp();

	CommunicationLogPopUp CommunicationLogPopUp = new CommunicationLogPopUp();
	CommunicationLogServiceAsync communicationService = GWT.create(CommunicationLogService.class);
	InlineLabel lblComplete = new InlineLabel("Complete Service"); //Ashwini Patil Date:1-03-2023
	ServiceCompleteFromSchedulePopup serviceCompleteFromSchedulePopup=new ServiceCompleteFromSchedulePopup(); //Ashwini Patil Date:1-03-2023
	
	
	SRSchedulePrintPopup srprintschedulepopup = new SRSchedulePrintPopup();
	int srprint=0;
	ConditionDialogBox conditionPopup2 = new ConditionDialogBox(
			"Do you want to print on preprinted Stationery", AppConstants.YES,
			AppConstants.NO);
	PopupPanel panel2=new PopupPanel(true);
	
	ConditionDialogBox conditionPopup3 = new ConditionDialogBox(
			"Do you want to print on preprinted Stationery", AppConstants.YES,
			AppConstants.NO);
	PopupPanel panel3=new PopupPanel(true);
	
	ServiceDateBoxPopup otpPopup=new ServiceDateBoxPopup("OTP"); //Ashwini Patil Date:5-07-2023 to restrict download with otp for hi tech client
	PopupPanel otppopupPanel;
	int generatedOTP;
	SmsServiceAsync smsService = GWT.create(SmsService.class);
	
	ServiceCancellationPopup  suspendservicepopup = new ServiceCancellationPopup();
	ServicesCancellationPopup customerbranchwisecancelation = new ServicesCancellationPopup();
	FromAndToDateBoxPopup rescheduleServicesInDurationPopup=new FromAndToDateBoxPopup("rescheduleServicesInDuration","Select duration in which you want to distribute the selected services. Only open services will be resceduled."); //Ashwini Patil Date:4-10-2023
	UploadPopup uploadPopup=new UploadPopup("info");
	PopupPanel uploadPopupPanel=new PopupPanel();
	DataMigrationServiceAsync datamigAsync = GWT.create(DataMigrationService.class);
	String cancellationremark = "";
	
	ConditionDialogBox conditionPopupForPrint=new ConditionDialogBox("Do you want to print on preprinted Stationery",AppConstants.YES,AppConstants.NO);
	PopupPanel printPanel ;
	
	WeekDaysPopup specificDayPopup=new WeekDaysPopup("Select specific days");
	WeekDaysPopup excludeDayPopup=new WeekDaysPopup("Select exclude days");
	PopupPanel specificDayPanel=new PopupPanel(true);
	PopupPanel excludeDayPanel=new PopupPanel(true);
	
	public ServicePresenter(TableScreen<Service> view,
			Service model) {
		super(view, model);
		form=view;
		//view.retriveTable(getServiceQuery());
		/** date 18.04.2017 added by komal for orion **/
		if(!(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "RestrictByDefaultLoadingOfList"))){
			view.retriveTable(getServiceQuery());
		}
		/**
		 * Date 04-02-2020
		 * Des :- NBHC CCPP Fumigation Services for Inhouse Services
		 */
		if(AppMemory.getAppMemory().currentScreen == Screen.INHOUSESERVICE	 && 
				AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "EnableLoadInHouseOpenServices")){
			view.retriveTable(getServiceQuery());
		}
		/**
		 * ends here
		 */
		setTableSelectionOnService();
		form.getSearchpopupscreen().getDwnload().setVisible(false);
		cancellationPopup.getBtnOk().addClickHandler(this);
		cancellationPopup.getBtnCancel().addClickHandler(this);
		
		suspendPopup.getBtnOk().addClickHandler(this);
		suspendPopup.getBtnCancel().addClickHandler(this);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.SERVICE,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
		/** date 26.02.2018 added by komal for schedule print  **/
		serviceSchedulePrintPopup.getLblOk().addClickHandler(this);
		serviceSchedulePrintPopup.getLblCancel().addClickHandler(this);
		/**
		 * end komal
		 */
		
		/**
		 * Date 04-05-2018
		 * Developer :- Vijay
		 * Description :- services list complete services with bulk popup Ok and Cancel button Click handler
		 */
		serviceCompletionPopup.getLblOk().addClickHandler(this);
		serviceCompletionPopup.getLblCancel().addClickHandler(this);
		/**
		 * ends here
		 */
		/**
		 * nidhi
		 * 2-08-2018 for set tecchnician set ok cancel click event
		 */
		serviceTechnicianPopup.getLblOk().addClickHandler(this);
		serviceTechnicianPopup.getLblCancel().addClickHandler(this);
		/**
		 * end
		 */
		
		/**
		 * Date 07-09-2018 By Vijay
		 * Developer :- Vijay
		 * Description :- Reschedule services with bulk
		 */
		reschedule.getBtnReschedule().addClickHandler(this);
		reschedule.getBtnCancel().addClickHandler(this);
		/**
		 * ends here
		 */
		
		/**
		 * Date 26-03-2019 by Vijay for service cancellation popup
		 */
		cancellation.getLblOk().addClickHandler(this);
		cancellation.getLblCancel().addClickHandler(this);
		/**
		 * ends here
		 */
		/**
		 * Updated By: Viraj
		 * Date: 15-06-2019
		 * Description: Assign branch in bulk
		 */
		serviceBranchPopup.getLblOk().addClickHandler(this);
		serviceBranchPopup.getLblCancel().addClickHandler(this);
		/** Ends **/
		
		/**
		 * Updated By: Viraj
		 * Date: 29-06-2019
		 * description: To change status for multiple entries in one go
		 */
		serviceStatusPopup.getLblOk().addClickHandler(this);
		serviceStatusPopup.getLblCancel().addClickHandler(this);
		/** Ends **/
		multipleServicesSchedulePopup.getLblOk().addClickHandler(this);
		
		datePopUp.getBtnOne().addClickHandler(this);
		datePopUp.getBtnTwo().addClickHandler(this);
		
		conditionPopup.getBtnOne().addClickHandler(this);
		conditionPopup.getBtnTwo().addClickHandler(this);
		
		genralInvoicePopup.btnClose.addClickHandler(this);
		
		replanservicepopup.getLblOk().addClickHandler(this);
		replanservicepopup.getLblCancel().addClickHandler(this);
		
		CommunicationLogPopUp.getBtnOk().addClickHandler(this);
		CommunicationLogPopUp.getBtnCancel().addClickHandler(this);
		CommunicationLogPopUp.getBtnAdd().addClickHandler(this);
		
		lblComplete.addClickHandler(this);
		serviceCompleteFromSchedulePopup.getLblOk().addClickHandler(this);
		serviceCompleteFromSchedulePopup.getLblCancel().addClickHandler(this);
		
		
		srprintschedulepopup.getLblOk().addClickHandler(this);
		srprintschedulepopup.getLblCancel().addClickHandler(this);
		srprintschedulepopup.getBtnprint().addClickHandler(this);
		
		conditionPopup2.getBtnOne().addClickHandler(this);
		conditionPopup2.getBtnTwo().addClickHandler(this);

		conditionPopup3.getBtnOne().addClickHandler(this);
		conditionPopup3.getBtnTwo().addClickHandler(this);
		otpPopup.getBtnOne().addClickHandler(this);
		otpPopup.getBtnTwo().addClickHandler(this);
		
		suspendservicepopup.getLblOk().addClickHandler(this);
		suspendservicepopup.getLblCancel().addClickHandler(this);
		
		rescheduleServicesInDurationPopup.getBtnOne().addClickHandler(this);
		rescheduleServicesInDurationPopup.getBtnTwo().addClickHandler(this);
		
		uploadPopup.getDownloadFormat().addClickHandler(this);
		uploadPopup.getBtnOk().addClickHandler(this);
		uploadPopup.getBtnCancel().addClickHandler(this);
		
		conditionPopupForPrint.getBtnOne().addClickHandler(this);
		conditionPopupForPrint.getBtnTwo().addClickHandler(this);

		replanservicepopup.getBtnSpecificDay().addClickHandler(this);
		replanservicepopup.getBtnExcludeDay().addClickHandler(this);
		specificDayPopup.getBtnOk().addClickHandler(this);
		specificDayPopup.getBtnCancel().addClickHandler(this);

		excludeDayPopup.getBtnOk().addClickHandler(this);
		excludeDayPopup.getBtnCancel().addClickHandler(this);
		
	}
	
	public ServicePresenter(TableScreen<Service> view,Service model,MyQuerry querry) {
		super(view, model);
		form=view;
		view.retriveTable(querry);
		setTableSelectionOnService();
		
		cancellationPopup.getBtnOk().addClickHandler(this);
		cancellationPopup.getBtnCancel().addClickHandler(this);
		
		suspendPopup.getBtnOk().addClickHandler(this);
		suspendPopup.getBtnCancel().addClickHandler(this);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.SERVICE,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
		
		/**
		 * Date 04-05-2018
		 * Developer :- Vijay
		 * Description :- services list complete services with bulk popup Ok and Cancel button Click handler
		 */
		serviceCompletionPopup.getLblOk().addClickHandler(this);
		serviceCompletionPopup.getLblCancel().addClickHandler(this);
		/**
		 * ends here
		 */
		
		/**
		 * Date 07-09-2018 By Vijay
		 * Developer :- Vijay
		 * Description :- Reschedule services with bulk
		 */
		reschedule.getBtnReschedule().addClickHandler(this);
		reschedule.getBtnCancel().addClickHandler(this);
		/**
		 * ends here
		 */
		
		/**
		 * Date 26-03-2019 by Vijay for service cancellation popup
		 */
		cancellation.getLblOk().addClickHandler(this);
		cancellation.getLblCancel().addClickHandler(this);
		/**
		 * ends here
		 */
		 	/** date 4.4.2019 added by komal to assign technician **/
		serviceTechnicianPopup.getLblOk().addClickHandler(this);
		serviceTechnicianPopup.getLblCancel().addClickHandler(this);
		multipleServicesSchedulePopup.getLblOk().addClickHandler(this);
		
		/** date 26.02.2018 added by komal for schedule print  **/
		serviceSchedulePrintPopup.getLblOk().addClickHandler(this);
		serviceSchedulePrintPopup.getLblCancel().addClickHandler(this);
		
		datePopUp.getBtnOne().addClickHandler(this);
		datePopUp.getBtnTwo().addClickHandler(this);
		conditionPopup.getBtnOne().addClickHandler(this);
		conditionPopup.getBtnTwo().addClickHandler(this);
		
		genralInvoicePopup.btnClose.addClickHandler(this);
		
		replanservicepopup.getLblOk().addClickHandler(this);
		replanservicepopup.getLblCancel().addClickHandler(this);
		
		CommunicationLogPopUp.getBtnOk().addClickHandler(this);
		CommunicationLogPopUp.getBtnCancel().addClickHandler(this);
		CommunicationLogPopUp.getBtnAdd().addClickHandler(this);
		lblComplete.addClickHandler(this);
		serviceCompleteFromSchedulePopup.getLblOk().addClickHandler(this);
		serviceCompleteFromSchedulePopup.getLblCancel().addClickHandler(this);
		
		srprintschedulepopup.getLblOk().addClickHandler(this);
		srprintschedulepopup.getLblCancel().addClickHandler(this);
		srprintschedulepopup.getBtnprint().addClickHandler(this);
		
		conditionPopup2.getBtnOne().addClickHandler(this);
		conditionPopup2.getBtnTwo().addClickHandler(this);
		
		conditionPopup3.getBtnOne().addClickHandler(this);
		conditionPopup3.getBtnTwo().addClickHandler(this);
		otpPopup.getBtnOne().addClickHandler(this);
		otpPopup.getBtnTwo().addClickHandler(this);
		
		suspendservicepopup.getLblOk().addClickHandler(this);
		suspendservicepopup.getLblCancel().addClickHandler(this);
		
		rescheduleServicesInDurationPopup.getBtnOne().addClickHandler(this);
		rescheduleServicesInDurationPopup.getBtnTwo().addClickHandler(this);
		
		uploadPopup.getDownloadFormat().addClickHandler(this);
		uploadPopup.getBtnOk().addClickHandler(this);
		uploadPopup.getBtnCancel().addClickHandler(this);
		
		conditionPopupForPrint.getBtnOne().addClickHandler(this);
		conditionPopupForPrint.getBtnTwo().addClickHandler(this);

		replanservicepopup.getBtnSpecificDay().addClickHandler(this);
		replanservicepopup.getBtnExcludeDay().addClickHandler(this);
		
		specificDayPopup.getBtnOk().addClickHandler(this);
		specificDayPopup.getBtnCancel().addClickHandler(this);

		excludeDayPopup.getBtnOk().addClickHandler(this);
		excludeDayPopup.getBtnCancel().addClickHandler(this);
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();
		
		if(text.equals(""))
			reactTo();
		
		
		if(text.equals(AppConstants.APPROVALVIEWDOCUMENT)){
			
//			if(serviceObject!=null){
//				System.out.println("NOT NULLLLLL");
//				viewSelectedDocument();
//			}
//			else{
//				form.showDialogMessage("Please select atleast one record!");
//			}
			
			ArrayList<Service> selectedList = getAllSelectedRecords();
			if(selectedList.size()==0){
				form.showDialogMessage("Please select record!");
				return;
			}
			if(selectedList.size()>=2){
				form.showDialogMessage("Please select only one record!");
				return;
			}
			if(form.isPopUpAppMenubar()){
				Console.log("SERVICE POPUP : View Document clicked!!");
				return;
			}
			reactonViewDocument(selectedList);
		}
		
		
	
		if(text.equals("Print Summary")){
			
			ArrayList<Service> selectedlist= getAllSelectedRecords();
			System.out.println("List Size"+ selectedlist.size());

			if(selectedlist.size()!=0){
				System.out.println("NOT NULLLLLL");
				servicelistserviceAsync.setServicesSelectedArrayList(selectedlist, new AsyncCallback<String>() {
					@Override
					public void onSuccess(String result) {
						System.out.println("on success");
						
						 final String url = GWT.getModuleBaseURL() + "setpdfmr1"+"?Id="+UserConfiguration.getCompanyId();
							Window.open(url, "test", "enabled");
					}
					
					@Override
					public void onFailure(Throwable caught) {
					}
				});
			}
			else{
				form.showDialogMessage("Please select atleast one record!");
			}
		}		
		
		if(text.equals("Create MIN")){
			ArrayList<Service> selectedlist= getAllSelectedRecords();
			System.out.println("List Size"+ selectedlist.size());
			if(selectedlist.size()!=0){
				System.out.println("NOT NULLLLLL");
				servicelistserviceAsync.setServicesSelectedListArrayList(selectedlist, new AsyncCallback<String>() {
					@Override
					public void onSuccess(String result) {
						System.out.println("on success");
						if(!result.equals("")){
							form.showDialogMessage(result);
						}else{
							form.showDialogMessage("Min created Successfully!");
						}
					}
					
					@Override
					public void onFailure(Throwable caught) {
						
					}
				});
			}
			else{
				form.showDialogMessage("Please select atleast one record!");
			}
		}
		
		if (text.equals("Print Service")) {
			final ArrayList<Service> selectedlist = getAllSelectedRecords();
			System.out.println("List Size" + selectedlist.size());

			if (selectedlist.size() != 0) {
				System.out.println("NOT NULLLLLL");

				 servicelistserviceAsync.printMultipleServices(selectedlist,new AsyncCallback<String>() {

					 @Override
					 public void onSuccess(String result) {
						 for (int i = 0; i < selectedlist.size(); i++) {
	
							final String url1 = GWT.getModuleBaseURL()+ "customerserpdf";
							Window.open(url1, "test", "test");
						}
					 }
					
					
					 @Override
					 public void onFailure(Throwable caught) {
					
					 }
				 });
				
				
			} else {
				form.showDialogMessage("Please select atleast one record!");
			}
		}
		
		/**
		 * Date : 21-02-2017 By ANil
		 */
		if (text.equals(AppConstants.CANCELSERVICES)) {
			
			if(validSelectedRecord()){
				/**
				 * @author Vijay Date :- 19-07-2023
				 * Des :- Cancellation remark popup set as by default
				 */
				
				//Ashwini Patil Date:7-03-2024 orion want normal document cancellation popup in which they can put reason
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "EnableCancellationRemark")){
					cancellation.showPopUp();
				}
				else{
					panel=new PopupPanel(true);
//					cancellationPopup.clear();
					cancellationPopup.getPaymentDate().setValue(AppUtility.parseDate(new Date())+"");
					panel.add(cancellationPopup);
					panel.center();
					panel.show();
				}

			
			}
		}
		
		if(text.equals("Cancel Partially Cancelled Services")){
			form.showWaitSymbol();
			updateService.cancelDocument("PartialServiceCancellation", UserConfiguration.getCompanyId(), new AsyncCallback<String>() {

				@Override
				public void onFailure(Throwable caught) {
					form.hideWaitSymbol();
				}

				@Override
				public void onSuccess(String result) {
					form.hideWaitSymbol();
					form.showDialogMessage(result);
				}
			});
		}
		if(text.equals("Update QuickContract/Services")){
			form.showWaitSymbol();
			updateService.cancelDocument("UpdateQuickContractAndServices", UserConfiguration.getCompanyId(), new AsyncCallback<String>() {

				@Override
				public void onFailure(Throwable caught) {
					form.hideWaitSymbol();
				}

				@Override
				public void onSuccess(String result) {
					form.hideWaitSymbol();
					form.showDialogMessage(result);
				}
			});
		}
		if(text.equals("Update QuickContract/Billing")){
			form.showWaitSymbol();
			updateService.cancelDocument("UpdateQuickContractBillingInvoicePayment", UserConfiguration.getCompanyId(), new AsyncCallback<String>() {

				@Override
				public void onFailure(Throwable caught) {
					form.hideWaitSymbol();
				}

				@Override
				public void onSuccess(String result) {
					form.hideWaitSymbol();
					form.showDialogMessage(result);
				}
			});
		}
		/** Date 26.02.2018 added by komal for schedule print **/
		
		/**
		 * @author Anil @since 28-01-2021 
		 * Changed label here from Schedule Print to Print Schedule
		 */
		if(text.equalsIgnoreCase("Print Schedule")){
			/**
			 * @author Vijay added for Orion to send custom SR Copy on email
			 */
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.SERVICE, AppConstants.PC_SRFORMATVERSION1)){
				srprintschedulepopup.showPopUp();
				srprintschedulepopup.getOlbEmployee().setSelectedIndex(0);
				srprintschedulepopup.getDbFromDate().setValue(null);
				srprintschedulepopup.getDbTodate().setValue(null);
			}
			/**
			 * ends here
			 */
			else{
				serviceSchedulePrintPopup.showPopUp();
				serviceSchedulePrintPopup.getOlbTeam().setSelectedIndex(0);
				serviceSchedulePrintPopup.getOlbEmployee().setSelectedIndex(0);
				serviceSchedulePrintPopup.getDbFromDate().setValue(null);
				serviceSchedulePrintPopup.getDbTodate().setValue(null);
			}
			
		}
		/** 
		 * end komal
		 */
		
		/**
		 * Date 12/3/2018
		 * By Jayshree 
		 * Des.add the suspend service and Resume service button 
		 */
		if(text.equals(AppConstants.SUSPENDSERVICES)){
			if(validSelectedRecord()){
				//Ashwini Patil Date:7-03-2024 orion want normal document cancellation popup in which they can put reason
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "EnableSuspendRemark")){
					suspendservicepopup.showPopUp();
				}
				else{
					panel=new PopupPanel(true);
//					suspendPopup.clear();
					suspendPopup.getPaymentDate().setValue(AppUtility.parseDate(new Date())+"");
					panel.add(suspendPopup);
					panel.center();
					panel.show();
				}
				
				
			}
			
//			form.showWaitSymbol();
//			ArrayList<Service> selectedlist = getAllSelectedRecords();
//			
//			
//			if(validSelectedRecord()){
//				ArrayList<SuperModel> servicelist=new ArrayList<SuperModel>();
//				for(Service service:selectedlist){
//					if(service.getStatus().equals(Service.SERVICESTATUSSCHEDULE)||
//							service.getStatus().equals(Service.SERVICESTATUSRESCHEDULE)){
//						service.setStatus(Service.SERVICESUSPENDED);
//						
//						}
//					servicelist.add(service);
//				}
//			
//					genasync.save(servicelist, new AsyncCallback<ArrayList<ReturnFromServer>>() {
//
//						@Override
//						public void onFailure(Throwable caught) {
////							form.hideWaitSymbol();
//						}
//
//						@Override
//						public void onSuccess(ArrayList<ReturnFromServer> result) {
//							
//							form.hideWaitSymbol();
//							DeviceForm deviceform=new DeviceForm();
//							deviceform.tbStatus.setValue(model.getStatus());
//							try {
//								
//								form.showDialogMessage("Services suspend Succesfully");
//								form.getSuperTable().getTable().redraw();
//							} catch (Exception e2) {
//								System.out.println(e2);
//							}
//							
//						}
//
//										});
//				}
			
			form.hideWaitSymbol();
			}
		/*** End Of suspend button***/
		
		if(text.equals(AppConstants.RESUMESERVICES)){
			form.showWaitSymbol();
			
			ArrayList<Service> selectedlist = getAllSelectedRecords();
			
//			if(selectedlist.size()==0){
//				form.showDialogMessage("Please select records!");
//				
//			}else{
//				ArrayList<SuperModel> servicelist=new ArrayList<SuperModel>();
//				for(Service service:selectedlist){
//					if(service.getStatus().equals(Service.SERVICESUSPENDED)){
//						service.setStatus(Service.SERVICESTATUSRESCHEDULE);
////						form.showDialogMessage("Services Rescheduled Succesfully");
//						}
//					else{
//						validSelectedRecordForSuspend();
//					}
//					
//					servicelist.add(service);
//				}
				
			
			if(validSelectedRecordForSuspend()){
				ArrayList<SuperModel> servicelist=new ArrayList<SuperModel>();
				for(Service service:selectedlist){
					if(service.getStatus().equals(Service.SERVICESUSPENDED)){
						service.setStatus(Service.SERVICESTATUSRESCHEDULE);
						}
					
					servicelist.add(service);
				}
				
				
				genasync.save(servicelist, new AsyncCallback<ArrayList<ReturnFromServer>>() {

					@Override
					public void onFailure(Throwable caught) {
						
					}

					@Override
					public void onSuccess(ArrayList<ReturnFromServer> result) {
						
						form.hideWaitSymbol();
						DeviceForm deviceform=new DeviceForm();
						deviceform.tbStatus.setValue(model.getStatus());
						try {
							
							form.showDialogMessage("Services Reschedule Succesfully");
							form.getSuperTable().getTable().redraw();
						} catch (Exception e2) {
							System.out.println(e2);
						}
						
					}

									});
			}
			form.hideWaitSymbol();
		}
		
		
		//end for resume button
		
		/**
		 * Date 04-05-2018
		 * Developer :- Vijay
		 * Description :- This button for select services from list and complete services with bulk
		 */
		if(text.equals(AppConstants.MARKCOMPLETE)){
			final ArrayList<Service> selectedlist = getAllSelectedRecords();
			if(selectedlist.size()==0){
				form.showDialogMessage("Please select records!");
				return;
			}
			/**
			 * @author Vijay Date :- 26-07-2022
			 * Des :- added validation service wise billing can not complete from three line menu.
			 */
			if(selectedlist.size()>=400){
					form.showDialogMessage("Please select services less than 400 to complete services in one lot");
					return;
			}
			/**
			 * ends here
			 */
			
			/*
			 * Added by Sheetal:27-10-2021
			 * Des : Document upload in Customer Report section should be mandatory before closure of service
			 */
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "PC_DocumentUploadMandatory")){
				for(int i=0;i<selectedlist.size();i++){
					if(AppUtility.ValidateCustomerReport(selectedlist.get(i).getUptestReport(),selectedlist.get(i).getServiceSummaryReport2())){
						form.showDialogMessage("Please upload atleast one document in Customer Report tab on Customer Service page to complete service");
						return;
				      }
			    }
	        }
			
			ArrayList<Integer> complaintServiceId = new ArrayList<Integer>();
			boolean complaintServiceflag = false;
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "EnableValidateAssesmentReport")){
				complaintServiceflag = true;
			}
			for(int i=0;i<selectedlist.size();i++){
				if(selectedlist.get(i).getStatus().equalsIgnoreCase("Completed") 
						|| selectedlist.get(i).getStatus().equalsIgnoreCase(Service.CANCELLED)
						|| selectedlist.get(i).getStatus().equalsIgnoreCase(Service.SERVICESUSPENDED)){
					form.showDialogMessage("Please select Scheduled and Rescheduled Services only!");
					return;
				}
				/**
				 * Date 14-11-2019 @author Vijay Chougule
				 * Des :- NBHC CCPM complaint service assessment should not be in created status for scheduling
				 */
				if(selectedlist.get(i).getTicketNumber()!=-1 && complaintServiceflag){
					complaintServiceId.add(selectedlist.get(i).getCount());
				}
				
				//for orion
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("EVA Pedio", "PC_DocumentUploadMandatory")){
					if(selectedlist.get(i).getUptestReport()==null){
						form.showDialogMessage("Service Report upload is mandatory! Please upload Service Report in Service "+selectedlist.get(i).getCount());
						return;
					}						
				}
				/*
				 * Ashwini Patil 
				 * Date:29-08-2024
				 * Orion does not want to allow service completion of last month services after specific deadline
				 * deadline is defined in company - Customer service setup - Service completion deadline field
				 */
				boolean validDate=AppUtility.validateServiceCompletionDate(selectedlist.get(i).getServiceDate());
				Console.log("validDate="+validDate);
				if(!validDate) {
					form.showDialogMessage("You cannot completed old service now! Service Id: "+selectedlist.get(i).getCount());
					return;
				}
			}
			
			/**
			 * @author Anil
			 * @since 12-05-2020
			 */
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Complain", "ComplainServiceWithTurnAroundTime")){
				Console.log("markCompleteServicesWithBulk");
				for(Service service : selectedlist){
					/**
					 * @author Anil
					 * @since 09-11-2020
					 * As service date's time is default set to 13, cuasing an issue while completing service before 13:00 PM
					 */
					if(AppUtility.formatDate(AppUtility.parseDate(service.getServiceDate())).after(AppUtility.formatDate(AppUtility.parseDate(new Date())))){
//					if(service.getServiceDate().after(new Date())){
						form.showDialogMessage("Service Date exceeds current date. Please reschedule the date");
						return;
					}
				}
				final Date completionDate = new Date();
				form.showWaitSymbol();
				servicelistserviceAsync.markCompleteServicesWithBulk(selectedlist, "", completionDate, new AsyncCallback<Integer>() {
					@Override
					public void onSuccess(Integer result) {
						// TODO Auto-generated method stub
						if(result==1){
							List<Service> list = form.getSuperTable().getDataprovider().getList();
							for(int i=0;i<selectedlist.size();i++){
								for(int j=0;j<list.size();j++){
									if(selectedlist.get(i).getCount() == list.get(j).getCount()){
										list.get(j).setStatus("Completed");
										list.get(j).setServiceCompleteRemark("");
										if(completionDate!=null){
											list.get(j).setServiceCompletionDate(completionDate);
										}
										else{
											list.get(j).setServiceCompletionDate(list.get(j).getServiceDate());
										}
										list.get(j).setRecordSelect(false);
									}
								}
							}
							form.showDialogMessage("Service Completed, Process Started Successfully.It will take some time to update.!");
							form.getSuperTable().getDataprovider().setList(list);
							serviceCompletionPopup.hidePopUp();
							form.getSuperTable().getTable().redraw();
							
						}else if(result==-1){
							form.showDialogMessage("Please try after 5 minutes! Last task is InProcess");
						}else if(result==-2){
							form.showDialogMessage("Please select services less than 100 ");
						}
						form.hideWaitSymbol();
					}
					
					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						form.hideWaitSymbol();

					}
				});
				
				return;
			}
			
			/**
			 * Date 14-11-2019 @author Vijay Chougule
			 * Des :- NBHC CCPM complaint service assessment validation
			 * Assessment must completed then allowing service to scheduling
			 */
			if(complaintServiceId.size()!=0){
				form.showWaitSymbol();
				validateComplaintService.ValidateAssessmentForComplaintService(complaintServiceId, selectedlist.get(0).getCompanyId(), new AsyncCallback<String>() {
					
					@Override
					public void onSuccess(String result) {
						// TODO Auto-generated method stub
						if(result.equals("success")){
							serviceCompletionPopup.getCbServiceDateAsServiceCompletionDate().setValue(false);
							serviceCompletionPopup.showPopUp();
						}
						else{
							form.showDialogMessage(result);
						}
						form.hideWaitSymbol();
					}
					
					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						form.hideWaitSymbol();
						form.showDialogMessage(caught.getMessage());
					}
				});
			}
			else{
				serviceCompletionPopup.getCbServiceDateAsServiceCompletionDate().setValue(false);
				serviceCompletionPopup.showPopUp();
			}
			
			
		}
		/**
		 * ends here
		 */
		/**
		 * nidhi
		 * 2-08-2018 
		 * for assign single technician to many service
		 */
		if(text.equals(AppConstants.ASSIGNTECHNICIAN)){
			ArrayList<Service> selectedlist = getAllSelectedRecords();
			if(selectedlist.size()==0){
				form.showDialogMessage("Please select records!");
				return;
			}
			for(int i=0;i<selectedlist.size();i++){
				if(!selectedlist.get(i).getStatus().equalsIgnoreCase("Scheduled") 
						&& !selectedlist.get(i).getStatus().equalsIgnoreCase("Rescheduled") ){
					
					form.showDialogMessage("Please select Scheduled and Rescheduled Services only!");
					return;
				}
			}
			serviceTechnicianPopup.showPopUp();
		}
		/**
		 * nidhi
		 * 2-08-2018 
		 * for assign single technician to many service
		 */
//		if(text.equals(AppConstants.ASSIGNTECHNICIAN)){
//			ArrayList<Service> selectedlist = getAllSelectedRecords();
//			if(selectedlist.size()==0){
//				form.showDialogMessage("Please select records!");
//				return;
//			}
//			/*for(int i=0;i<selectedlist.size();i++){
//				if(!selectedlist.get(i).getStatus().equalsIgnoreCase(Service.SERVICESTATUSSCHEDULE) 
//						&& !selectedlist.get(i).getStatus().equalsIgnoreCase(Service.SERVICESTATUSRESCHEDULE) ){
//					
//					form.showDialogMessage("Please select Scheduled and Rescheduled Services only!");
//					return;
//				}
//			}*/
//			serviceTechnicianPopup.showPopUp();
//		}
		
		/**
		 * Date 07-09-2018 
		 * Developer :- Vijay
		 * Description :- This button for select services from list and Reschedule services with bulk
		 */
		if(text.equals(AppConstants.Reschedule)){
			Console.log("Rescheduled clicked!");
			ArrayList<Service> selectedlist = getAllSelectedRecords();
			if(selectedlist.size()==0){
				form.showDialogMessage("Please select records!");
				return;
			}
			if(selectedlist.size()>=200){
				form.showDialogMessage("Please select Less than 200 records only!");
				return;
			}
			for(int i=0;i<selectedlist.size();i++){
				if(selectedlist.get(i).getStatus().equalsIgnoreCase("Completed") ||
						selectedlist.get(i).getStatus().equals(Service.SERVICETCOMPLETED)||
						selectedlist.get(i).getStatus().equals(Service.SERVICESTATUSCANCELLED) ||
						selectedlist.get(i).getStatus().equals(Service.SERVICESUSPENDED)){
					form.showDialogMessage("Please select Scheduled and Rescheduled Services only!");
					return;
				}
				
				/**
				 * @author Vijay Chougule
				 * Project :- Fumigation Tracker
				 * Des :- when CIO Reschedule the stack fumigation service then In EVA ERP System it can not allow to
				 * reschedule CIO Reschedule Date + 5 Days. 
				 */
				if(selectedlist.get(i).isWmsServiceFlag() && (selectedlist.get(i).getProduct().getProductCode().trim().equals("PHM-01") || selectedlist.get(i).getProduct().getProductCode().trim().equals("STK-01"))
						&& selectedlist.get(i).getCioRescheduleDate()!=null){
					if(!LoginPresenter.myUserEntity.getRole().getRoleName().equals("ADMIN")){
						Date newrescheduleDate = reschedule.getDateBox().getValue();
						Date cioRescheduleDate = selectedlist.get(i).getCioRescheduleDate();
						CalendarUtil.addDaysToDate(cioRescheduleDate, 5);
						Console.log("cioRescheduleDate"+cioRescheduleDate);
						if(newrescheduleDate.after(cioRescheduleDate)){
							form.showDialogMessage("Can not reschedule more than 5 days of Cio Reschedule Date : "+AppUtility.parseDate(cioRescheduleDate));
							return;
						}
					}
				}
				/**
				 * ends here
				 */
			}
			
			
			reactOnReschedule();
		}
		
		/**
		 * Date 28-11-2018 By Vijay
		 * Des :- View Contract Button to view contract
		 */
		if(text.equals(AppConstants.VIEWCONTRACT)){
			ArrayList<Service> selectedList = getAllSelectedRecords();
			if(selectedList.size()==0){
				form.showDialogMessage("Please select record!");
				return;
			}
			if(selectedList.size()>=2){
				form.showDialogMessage("Please select only one record!");
				return;
			}
			if(form.isPopUpAppMenubar()){
				Console.log("SERVICE POPUP : Contract clicked!!");
				return;
			}
			reactOnViewContract(selectedList);
		}
		
		/**
		 * Updated By:Viraj
		 * Date: 15-06-2019
		 * Description: To assign branch in bulk
		 */
		if(text.equals(AppConstants.ASSIGNBRANCH)){
			ArrayList<Service> selectedlist = getAllSelectedRecords();
			if(selectedlist.size()==0){
				form.showDialogMessage("Please select records!");
				return;
			}
			
			serviceBranchPopup.showPopUp();
		}
		/** Ends **/
		
		/**
		 * Updated By: Viraj
		 * Date: 29-06-2019
		 * description: To change status for multiple entries in one go
		 */
		if(text.equals(AppConstants.ASSIGNSTATUS)) {
			ArrayList<Service> selectedlist = getAllSelectedRecords();
			boolean status = false;
			Console.log("Inside ASSIGNSTATUS");
			if(selectedlist.size() != 0){
				for(int i=0; i<selectedlist.size(); i++) {
					if(selectedlist.get(i).getStatus().equalsIgnoreCase("Cancelled")) {
						status = true;
						Console.log("Inside cancelled if");
					} else {
						status = false;
						break;
					}
				}
				
				if(status == true) {
					Console.log("Inside true of status");
					serviceStatusPopup.showPopUp();
				} else {
					form.showDialogMessage("selected services must be cancelled");
				}
			}
		}
		/** Ends **/
/** date 13.5.2019 added by komal to schedule multiple services **/
		if(text.equalsIgnoreCase(AppConstants.SCHEDULEMULTIPLESERVICES)){
			final List<Service> serviceList = getAllSelectedRecords();
			if(serviceList.size()==0){
				form.showDialogMessage("Please select record!");
				return;
			}
			for(Service service: serviceList){
//				if(service.isServiceScheduled()){
//					form.showDialogMessage("Please select unscheduled services.");
//					return;
//				}
				
			}
			boolean complaintServiceflag = false;
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "EnableValidateAssesmentReport")){
				complaintServiceflag = true;
			}
			ArrayList<Integer> complaintServiceId = new ArrayList<Integer>();
			for(Service object:serviceList){
				
				if(object.getStatus().equals(Service.SERVICESTATUSCANCELLED)
						||object.getStatus().equals(Service.SERVICESTATUSCLOSED)
						||object.getStatus().equals(Service.SERVICESTATUSCOMPLETED)
						||object.getStatus().equals(Service.SERVICESTATUSREPORTED)
						||object.getStatus().equals(Service.SERVICESTATUSPENDING)
						||object.getStatus().equals(Service.SERVICESTATUSTECHCOMPLETED)
						||object.getStatus().equals(Service.SERVICESUSPENDED)){
		
					form.showDialogMessage("Service status should be Scheduled/Rescheduled!");
					return ;
				}
				
				/**
				 * Date 14-11-2019 @author Vijay Chougule
				 * Des :- NBHC CCPM complaint service assessment should not be in created status for scheduling
				 */
				if(complaintServiceflag && object.getTicketNumber()!=-1){
					complaintServiceId.add(object.getCount());
				}
			}
			/**
			 * Date 14-11-2019 @author Vijay Chougule
			 * Des :- NBHC CCPM complaint service assessment validation
			 * Assessment must completed then allowing service to scheduling
			 */
			if(complaintServiceId.size()!=0){
				form.showWaitSymbol();
				validateComplaintService.ValidateAssessmentForComplaintService(complaintServiceId, serviceList.get(0).getCompanyId(), new AsyncCallback<String>() {
					
					@Override
					public void onSuccess(String result) {
						// TODO Auto-generated method stub
						if(result.equals("success")){
							if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "ENABLECOMPLETEBUTTONONSCHEDULEPOPUP")){
								lblComplete.getElement().setId("addbutton");	
								multipleServicesSchedulePopup.getHorizontal().insert(lblComplete,2);					
							}
							
							multipleServicesSchedulePopup.showPopUp();
							multipleServicesSchedulePopup.getPopup().setHeight("580px");
							multipleServicesSchedulePopup.getPopup().setWidth("800px");
							multipleServicesSchedulePopup.getTabPanel().selectTab(0);
							multipleServicesSchedulePopup.getMultipleServicesScheduleTable().getDataprovider().setList(serviceList);
							multipleServicesSchedulePopup.setServiceList(serviceList);
						}
						else{
							form.showDialogMessage(result);
						}
						form.hideWaitSymbol();
					}
					
					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						form.hideWaitSymbol();
						form.showDialogMessage(caught.getMessage());
					}
				});
			}
			else{
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "ENABLECOMPLETEBUTTONONSCHEDULEPOPUP")){
					lblComplete.getElement().setId("addbutton");	
					multipleServicesSchedulePopup.getHorizontal().insert(lblComplete,2);					
				}
				multipleServicesSchedulePopup.showPopUp();
				multipleServicesSchedulePopup.getPopup().setHeight("580px");
				multipleServicesSchedulePopup.getPopup().setWidth("800px");
				multipleServicesSchedulePopup.getTabPanel().selectTab(0);
				multipleServicesSchedulePopup.getMultipleServicesScheduleTable().getDataprovider().setList(serviceList);
				multipleServicesSchedulePopup.setServiceList(serviceList);
			}
			
		}
		/**
		 * end komal
		 */
		if(text.equalsIgnoreCase(AppConstants.SENDSRMAIL)){
			List<Service> serviceList = getAllSelectedRecords();
			if(serviceList.size()==0){
				form.showDialogMessage("Please select record!");
				return;
			}
			ArrayList<Integer> serviceIdList = new ArrayList<Integer>();
			for(Service ser : serviceList){
				serviceIdList.add(ser.getCount());
			}
			
			async.sendSrMail(UserConfiguration.getCompanyId(), serviceIdList, new AsyncCallback<String>() {

				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					form.showDialogMessage("Failed");
				}

				@Override
				public void onSuccess(String result) {
					// TODO Auto-generated method stub
					form.showDialogMessage("Email sent successfully.");
				}
			});
		}
		/**
		 * @author Anil @since 05-10-2021
		 * Earlier label was "Update Duplicate Services" now as per Nitin Sir request it has been changed to "Remove Duplicates"
		 * Changing label here also for execution of functionality
		 */
//		if(text.equals("Update Duplicate Services")){
		if(text.equals("Remove Duplicates")){
			datePanel=new PopupPanel(true);
			datePanel.add(datePopUp);
			datePanel.center();
			datePanel.show();
			
		}
		
		if(text.equals("Update Component Detail")){
			componentPopup.clear();
			componentPopup.showPopUp();
		}
		
		if(text.equals("View Complain")){
			reactOnViewComplain();
		}
		
		if(text.equals(AppConstants.RePlanServices)){
			reactOnReplanServices();
		}
		
		if(text.equals(AppConstants.CANCELLOCATIONWISESERVICES)){
			customerbranchwisecancelation.showPopUp();
			customerbranchwisecancelation.customerbranchtable.getDataprovider().getList().clear();
		}
		
		if(text.equals(AppConstants.DISTRIBUTESERVICESEQUALLYONGIVENDATES)){
			Console.log("in "+AppConstants.DISTRIBUTESERVICESEQUALLYONGIVENDATES);
			ArrayList<Service> selectedlist = getAllSelectedRecords();
			if(selectedlist==null|| selectedlist.size()==0){
				form.showDialogMessage("Please select records!");
				return;
			}
			if(selectedlist.size()==1){
				form.showDialogMessage("User rescedule option to change date of single service.");
				return;
			}
			
			datePanel=new PopupPanel(true);
			datePanel.add(rescheduleServicesInDurationPopup);
			datePanel.center();
			datePanel.show();
		}
		if(text.equals(AppConstants.ScheduleOrCompleteViaExcel)){
			Console.log("in "+AppConstants.ScheduleOrCompleteViaExcel);
			ArrayList<Service> selectedlist = getAllSelectedRecords();
			
//			if(selectedlist==null||selectedlist.size()==0) {
//				form.showDialogMessage("Select Services!");
//				return;
//			}
			
			String msg="Instructions:\n";
			msg+="Use bulk schedule, Cancel & complete your daily services. You can make plan in excel by downloading due services. Follow below instructions\n";
			msg+="1.Search the services you would like to schedule & complete using search option \n";
			msg+="2.Download services to be scheduled using download button below \n";
			msg+="3.Download & Open in Excel  \n";
			msg+="4.To Schedule \n";
			msg+="   a.Update the service date, time technician, service description, service status as \"Scheduled\" in the excel to schedule services \n";
			msg+="   b.Upload the same excel & get all services scheduled in bulk \n";
			msg+="5.To Complete\n";
			msg+="   a.Update service status = \"Completed\" , Completion Date, Completion Time, Completion Remark, Customer Rating \n";
			msg+="6.To Cancel \n";
			msg+="   a.Update service status = \"Cancelled\" & Reason for Cancellation  \n";
			msg+="7.Upload the file in the same structure. \n ";
			msg+="Note: Data has to match in the system i.e. technician name, status = \"Scheduled\", \"Completed\" etc. ";
			
			uploadPopup.getInfo().setText(msg);	
			uploadPopup.getInfo().setEnabled(false);
			uploadPopup.isDownloadClickedFromDifferentProgram=true;
			uploadPopupPanel=new PopupPanel(true);
			uploadPopupPanel.add(uploadPopup);
			uploadPopupPanel.center();
			uploadPopupPanel.show();		
		}
		
		if(text.equals(AppConstants.downloadReports)){
			ArrayList<Service> selectedList = getAllSelectedRecords();
			if(selectedList==null||selectedList.size()==0){
				form.showDialogMessage("Please select records!");
				return;
			}
			if(selectedList!=null&&selectedList.size()>0) {
				for(Service object:selectedList) {
					if(object.getUptestReport()!=null){
						String filename="";
						int month=object.getServiceDate().getMonth()+1;
						int year=object.getServiceDate().getYear()+1900;
						if(object.getServiceBranch()!=null)
							filename=object.getServiceBranch()+"-"+object.getCustomerName()+"-"+month+"-"+year;
						else
							filename=object.getUptestReport().getName()+"-"+object.getCustomerName()+"-"+month+"-"+year;
						
						String url=object.getUptestReport().getUrl()+"&filename="+filename+".jpg";
						Window.open(url, "_blank", "");
					}
				}
			
			}
		}
		
	}

	

	

	/**
	 * Date 28-11-2018 By Vijay
	 * Des :- View Contract Button to view contract
	 */
	private void reactOnViewContract(ArrayList<Service> selectedList) {

		MyQuerry querry = new MyQuerry();
		Vector<Filter> filterVec = new Vector<Filter>();
		Filter filter;
		
		filter = new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(selectedList.get(0).getContractCount());
		filterVec.add(filter);
		
		querry.setFilters(filterVec);
		querry.setQuerryObject(new Contract());
		form.showWaitSymbol();
		genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				form.hideWaitSymbol();
				if(result.size()==0){
					form.showDialogMessage("Contract Document Not Found!");
				}else{
					final Contract contract = (Contract) result.get(0);
					final ContractForm form = ContractPresenter.initalize();
					Timer timer = new Timer() {
						
						@Override
						public void run() {
							form.updateView(contract);
							form.setToViewState();
						}
					};
					timer.schedule(7000); // Changed timer from 1 sec to 7 sec to give proper initialization time
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
			}
		});
	}
	/**
	 * ends here
	 */

	
	
	/**
	 * Date 07-09-2018 
	 * Developer :- Vijay
	 * Des :- For Rescheduling Popup for multiple services reschedule
	 */
	private void reactOnReschedule() {
		Console.log("reactOnReschedule clicked!");
		reschedule.getP_seviceBranch().setVisible(false);
		reschedule.nlnlblNewServiceBranch.setVisible(false);//Ashwini Patil Date:27-05-2022
		reschedule.getDateBox().setValue(new Date());
		   panel=new PopupPanel(true);
		   panel.add(reschedule);
		   reschedule.clear();
		   panel.show();
		   panel.center();
	}
	/**
	 * ends here
	 */
	
	private boolean validSelectedRecordForSuspend() {
		String message="";
		ArrayList<Service> selectedlist = getAllSelectedRecords();
		
		if(selectedlist.size()==0){
			form.showDialogMessage("Please select records!");
			return false;
		}
		for(Service object:selectedlist){
			/**
			 * Added validation on selected record for cancellation(NBHC CCPM)
			 */		
			if(object.getStatus().equals(Service.SERVICESTATUSCANCELLED)
					||object.getStatus().equals(Service.SERVICESTATUSCLOSED)
					||object.getStatus().equals(Service.SERVICESTATUSCOMPLETED)
					||object.getStatus().equals(Service.SERVICESTATUSREPORTED)
					||object.getStatus().equals(Service.SERVICESTATUSPENDING)
					||object.getStatus().equals(Service.SERVICESTATUSTECHCOMPLETED)
					||object.getStatus().equals(Service.SERVICESTATUSSCHEDULE)
					||object.getStatus().equals(Service.SERVICESTATUSRESCHEDULE))
			{
				message=message+"Service status should be Suspended.";
				form.showDialogMessage("Service status should be Suspended");
				return false;
			}
		}
		
		return true;
	}

	/**
	 * Date : 21-02-2017 by Anil
	 * Added validation on selected record for cancellation(NBHC CCPM)
	 */
	private boolean validSelectedRecord() {
		String message="";
		ArrayList<Service> selectedlist = getAllSelectedRecords();
		
		if(selectedlist.size()==0){
			form.showDialogMessage("Please select records!");
			return false;
		}
		for(Service object:selectedlist){
			/**
			 * @author Anil
			 * @since 20-05-2020
			 * adding cancellation restriction for in house fumigation services
			 */
			if(object.isWmsServiceFlag()==true){
				form.showDialogMessage("Can not cancel/suspend In-House services!");
				return false;
			}
			
			/**
			 * Added validation on selected record for cancellation(NBHC CCPM)
			 */		
			if(object.getStatus().equals(Service.SERVICESTATUSCANCELLED)
					||object.getStatus().equals(Service.SERVICESTATUSCLOSED)
					||object.getStatus().equals(Service.SERVICESTATUSCOMPLETED)
					||object.getStatus().equals(Service.SERVICESTATUSREPORTED)
					||object.getStatus().equals(Service.SERVICESTATUSPENDING)
					||object.getStatus().equals(Service.SERVICESTATUSTECHCOMPLETED)
					||object.getStatus().equals(Service.SERVICESUSPENDED)){
				message=message+"Service status should be Scheduled/Rescheduled.";
				form.showDialogMessage("Service status should be Scheduled/Rescheduled!");
				return false;
			}
		}
		
		return true;
	}


	public void reactOnPrint()
	{
		
		
			final ArrayList<Service> ser=new ArrayList<Service>();
			List<Service> aray=form.getSuperTable().getDataprovider().getList();
			ser.addAll(aray);
		
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.SERVICE, AppConstants.PC_SRFORMATVERSION1)){
				ArrayList<Service> selectedList = getAllSelectedRecords();
				if(selectedList.size()==0){
					form.showDialogMessage("Please select record!");
					return;
				}
				for(Service service : selectedList){
					if(service.getStatus().equals(Service.SERVICESTATUSCOMPLETED) ||  service.getStatus().equals(Service.SERVICESTATUSCLOSED) ||
						service.getStatus().equals(Service.SERVICESTATUSCANCELLED)||service.getStatus().equals(Service.SERVICESTATUSREPORTED) ||
						service.getStatus().equals(Service.SERVICESTATUSSTARTED) || service.getStatus().equals(Service.SERVICESUSPENDED) ||
						service.getStatus().equals(Service.SERVICETCOMPLETED) ){ 
						form.showDialogMessage("Please select Scheduled/Rescheduled services only");
						return;
					}
				}
				ser.clear();
				System.out.println("selectedList list size "+selectedList.size());
				ser.addAll(selectedList);
			}

			
			pdfservice.setServiceList(ser, new AsyncCallback<Void>() {
				
				@Override
				public void onSuccess(Void result) {
					
					if(AppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.SERVICE, AppConstants.PC_SRFORMATVERSION1)){
						
						if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "CompanyAsLetterHead")){
							srprint = srprint + 1;
							Console.log("inside CompanyAsLetterHead");
							if(srprint>0){
					    		panel3=new PopupPanel(true);
					    		panel3.add(conditionPopup3);
					    		panel3.setGlassEnabled(true);
					    		panel3.show();
					    		panel3.center();
					             Console.log("inside print reminder");
					    	}
							
						}
						else{
							final String url = GWT.getModuleBaseURL()+"srformateversion1"+"?"
									+"&"+"companyId="+ser.get(0).getCompanyId()+""+ "&"+ "preprint=" + "plane";
							Window.open(url, "test", "enabled");
						}
						
					}
					else{
						printPanel=new PopupPanel(true);
						printPanel.add(conditionPopupForPrint);
						printPanel.setGlassEnabled(true);
						printPanel.show();
						printPanel.center();
						
						
					}
					
					
				}
				
				@Override
				public void onFailure(Throwable caught) {
					System.out.println("RPC call FAiled"+caught);
					
				}
			});
	
}
	
	
	
	
	
	

	@Override
	public void reactOnDownload() {
		if(view.getSearchpopupscreen().getSupertable().getListDataProvider().getList()==null||view.getSearchpopupscreen().getSupertable().getListDataProvider().getList().size()==0)
		{
			form.showDialogMessage("No records found for download!");
			return;
		}
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "RestrictServiceDownloadWithOTP")){
			generatedOTP=(int) Math.floor(100000 + Math.random() * 900000);
			Console.log("generatedOTP="+generatedOTP);
			smsService.checkConfigAndSendServiceDownloadOTPMessage(model.getCompanyId(), LoginPresenter.loggedInUser, generatedOTP, new  AsyncCallback<String>() {

				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					form.showDialogMessage("Failed");
				}

				@Override
				public void onSuccess(String result) {
					// TODO Auto-generated method stub
					Console.log("in success");
					if(result!=null&&result.equalsIgnoreCase("success")) {						
						otpPopup.clearOtp();
						otppopupPanel = new PopupPanel(true);
						otppopupPanel.add(otpPopup);
						otppopupPanel.show();
						otppopupPanel.center();
					}else {
						form.showDialogMessage(result);
					}
						
				}
			} );
		
		}else
			runRegularDownloadProgram();
		
		
	}

	@Override
	public void reactOnEmail() {
		

		ArrayList<Service> selectedList = getAllSelectedRecords();
		if(selectedList.size()==0){
			form.showDialogMessage("Please select record!");
			return;
		}
		if(selectedList.size()>=2){
			form.showDialogMessage("Please select only one record!");
			return;
		}
		
		/**
		 * @author Vijay Date 29-10-2021
		 * Added New Email Popup Functionality
		 */
		emailpopup.showPopUp();
		setEmailPopUpData(selectedList.get(0));
		
		/**
		 * ends here
		 */
	}

	/*
	 * Method template to make a new model
	 */
	@Override
	protected void makeNewModel() {
		
		model=new Service();
	}
	
//	public static void initalize()
//	{
//		glassPanel.show();
//		Timer timer=new Timer() 
//   	 	{
//			@Override
//			public void run() 
//			{
//				/**
//				 * @author Anil , Date : 13-11-2019
//				 * setting schedule popup material value to blank
//				 * for some case on click of plan button technician details were not visible
//				 * raised by Prashad
//				 */
//				ServiceSchedulePopup.materialValue="";
//				TechnicianServiceSchedulePopup.materialValue="";
//				
//				ServiceTable table=new ServiceTable();
//				ServiceForm form=new ServiceForm(table);
//				ServiceSearchPopUp.staticSuperTable=table;
//				ServiceSearchPopUp searchPopUp=new ServiceSearchPopUp(false);
//				form.setSearchpopupscreen(searchPopUp);
//				ServicePresenter presenter=new ServicePresenter(form, new Service());
//				form.setToViewState();
//				AppMemory.getAppMemory().stickPnel(form);
//				glassPanel.hide();
//			}
//		};
//		timer.schedule(3000); 
//	}
	
	public static void initalize(final AbstractScreen value)
	{
		glassPanel.show();
		Timer timer=new Timer() 
   	 	{
			@Override
			public void run() 
			{
				/**
				 * @author Anil , Date : 13-11-2019
				 * setting schedule popup material value to blank
				 * for some case on click of plan button technician details were not visible
				 * raised by Prashad
				 */
				ServiceSchedulePopup.materialValue="";
				TechnicianServiceSchedulePopup.materialValue="";
				
				ServiceTable table=new ServiceTable();
				ServiceForm form=new ServiceForm(table);
				
				AppMemory.getAppMemory().currentScreen=value;
				
				ServiceSearchPopUp.staticSuperTable=table;
				ServiceSearchPopUp searchPopUp=new ServiceSearchPopUp(false);
				form.setSearchpopupscreen(searchPopUp);
				/**
				 * @author Anil @since 29-10-2021
				 * Setting Service list height as per the browser height
				 */
				int height = Window.getClientHeight();
				Console.log("Window Height : " + height);
				height = height * 80 / 100;
				Console.log("Updated Height : " + height);
				searchPopUp.getSupertable().getTable().setHeight(height+"px");
				
				ServicePresenter presenter=new ServicePresenter(form, new Service());
				form.setToViewState();
				AppMemory.getAppMemory().stickPnel(form);
				glassPanel.hide();
			}
		};
		timer.schedule(3000); 
	}
	
	public static void initalize(MyQuerry querry)
	{
		/**
		 * @author Anil , Date : 13-11-2019
		 * setting schedule popup material value to blank
		 * for some case on click of plan button technician details were not visible
		 * raised by Prashad
		 */
		ServiceSchedulePopup.materialValue="";
		TechnicianServiceSchedulePopup.materialValue="";
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Customer Service List",Screen.SERVICE);
		ServiceTable table=new ServiceTable();
		ServiceForm form=new ServiceForm(table);
		ServiceSearchPopUp.staticSuperTable=table;
		ServiceSearchPopUp searchPopUp=new ServiceSearchPopUp(false);
		form.setSearchpopupscreen(searchPopUp);
		/**
		 * @author Anil @since 29-10-2021
		 * Setting Service list height as per the browser height
		 */
		int height = Window.getClientHeight();
		Console.log("Window Height : " + height);
		height = height * 80 / 100;
		Console.log("Updated Height : " + height);
		searchPopUp.getSupertable().getTable().setHeight(height+"px");
		
		ServicePresenter presenter=new ServicePresenter(form, new Service(),querry);
		form.setToViewState();
		AppMemory.getAppMemory().stickPnel(form);
	}
	
	/*
	 * Method template to set the query object in specific to search criteria and return the same
	 * @return MyQuerry object
	 */
	public MyQuerry getServiceQuery()
	{
		MyQuerry querry=new MyQuerry();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter=null;
		Company c=new Company();
		List<String> statusList=new ArrayList<String>();
		statusList.add(Service.SERVICESTATUSSCHEDULE);
		statusList.add(Service.SERVICESTATUSRESCHEDULE);
		
		filter=new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		filter=new Filter();
		filter.setQuerryString("status IN");
		filter.setList(statusList);
		filtervec.add(filter);
		
		/**
		 * Developed by : Rohan added this branch  
		 */
			List<String> branchList ;
			branchList= new ArrayList<String>();
			System.out.println("item count in branch "+LoginPresenter.globalBranch.size());
			for (int i = 0; i < LoginPresenter.globalBranch.size(); i++) {
				branchList.add(LoginPresenter.globalBranch.get(i).getBusinessUnitName());
			}
			System.out.println("branchList list "+branchList.size());
		
			filter = new Filter();
			filter.setQuerryString("branch IN");
			filter.setList(branchList);
			filtervec.add(filter);
		/**
		 * ends here 
		 */
			
		
		/**
		 * @author Vijay Chougule
		 * Des :- NBHC CCPM Inhouse services should display by default next 2 months open servies of
		 * fu,igation services only
		 * and else block for customer service list 
		 */
		if(AppMemory.getAppMemory().currentScreen == Screen.INHOUSESERVICE){
			
			Date todayDate = new Date();
			CalendarUtil.addMonthsToDate(todayDate, 2);
			filter=new Filter();
			filter.setQuerryString("serviceDate <");
			filter.setDateValue(todayDate);
			filtervec.add(filter);
			
			filter = new Filter();
			filter.setQuerryString("wmsServiceFlag");
			filter.setBooleanvalue(true);
			filtervec.add(filter);
		}
		else{
			
			/**
			 * @author Abhinav Bihade
			 * @since 18/12/2019
			 * As per Rahu Tiwari's Requirement and with Discuss with Anil Sir write 'todayDate' 
			 */
			Date todayDate = new Date();
			CalendarUtil.addDaysToDate(todayDate, 1);
			filter=new Filter();
			filter.setQuerryString("serviceDate <");
			filter.setDateValue(todayDate);
			filtervec.add(filter);
		}
			
		
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Service());
		
		return querry;
	}
	
	  private void reactTo()
  {
}
	  public void setTableSelectionOnService()
		 {
			final NoSelectionModel<Service> selectionModelMyObj = new NoSelectionModel<Service>();

			SelectionChangeEvent.Handler tableHandler = new SelectionChangeEvent.Handler() {
				@Override
				public void onSelectionChange(SelectionChangeEvent event) {
					Console.log("viewDocumentFlag in service preseneter"+viewDocumentFlag);
					Timer timer = new Timer() {

						@Override
						public void run() {
							Console.log("viewDocumentFlag in service preseneter == "+viewDocumentFlag);
							if(viewDocumentFlag){
								final Service entity = selectionModelMyObj.getLastSelectedObject();
								serviceObject=entity;
								System.out.println("on selection table service ");
								AppMemory.getAppMemory().currentScreen = Screen.SERVICE;

								genralInvoicePopup.setModel(AppConstants.CUSTOMERSERVICE,entity);
								generalPanel = new PopupPanel();
								generalPanel.add(genralInvoicePopup);
								generalPanel.show();
								generalPanel.center();
							}
							else{
								if(scheduleServiceFlag)
								{
									
									scheduleServiceFlag=false;
									final Service entity = selectionModelMyObj.getLastSelectedObject();
									serviceObject=entity;
									scheduleSingleService(entity);									
								}
								viewDocumentFlag = true;
							}
						}
					};
					timer.schedule(2000);
					
					
				}
			};
			// Add the handler to the selection model
			selectionModelMyObj.addSelectionChangeHandler(tableHandler);
			// Add the selection model to the table
			form.getSuperTable().getTable().setSelectionModel(selectionModelMyObj);
			 
		 }
	  
	  
	  private void viewSelectedDocument()
		{
		  	serviceObject.setRecordSelect(false);//Date 17-4-2018 By Jayshree add this to set the check box list value default false
			final Service entity = serviceObject;
			AppMemory.getAppMemory().currentState = ScreeenState.VIEW;
			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Customer Service", Screen.DEVICE);
			DeviceForm.flag = false;
			final DeviceForm form = DevicePresenter.initalize();
			form.showWaitSymbol();
			AppMemory.getAppMemory().stickPnel(form);
			Timer timer = new Timer() {
				@Override
				public void run() {
					form.hideWaitSymbol();
					form.updateView(entity);
					form.setToViewState();
				}
			};
			timer.schedule(3000);
		}
	  
	  
	  private ArrayList<Service> getAllSelectedRecords() 
		
		{
			List<Service> lisservicetable=form.getSuperTable().getDataprovider().getList();
			System.out.println("table===="+lisservicetable.size());
			 ArrayList<Service> selectedlist=new ArrayList<Service>();
				 for(Service ser:lisservicetable)
				 {
					 if(ser.getRecordSelect()==true){
						 selectedlist.add(ser);
					 }
				 }
				 return selectedlist;
		}
	  
	  @Override
		public void onClick(ClickEvent event) {
			super.onClick(event);
			  System.out.println("CLICK");
			  Console.log("service presenter onclick");
			if(event.getSource()==cancellationPopup.getBtnOk()){
				System.out.println("POPUP OK CLICK");
				cancelServices(getAllSelectedRecords());
				panel.hide();
				
			}
			
			if(event.getSource().equals(cancellationPopup.getBtnCancel())){
				System.out.println("POPUP CANCEL CLICK");
				panel.hide();
			}
			
			if(event.getSource()==suspendPopup.getBtnOk()){
				System.out.println("POPUP OK CLICK");
				suspendServicesFromDocumentCancellationPopup(getAllSelectedRecords());
				panel.hide();
				
			}
			
			if(event.getSource().equals(suspendPopup.getBtnCancel())){
				System.out.println("POPUP CANCEL CLICK");
				panel.hide();
			}
			
			
			/** Date 26.02.2018 added by komal for schedule print **/
			if(event.getSource().equals(serviceSchedulePrintPopup.getLblOk())){
				if(validateSchedulePrintPopup()){
					Employee emp = null;
					String technicianId = null;
					if( serviceSchedulePrintPopup.getOlbEmployee().getSelectedIndex() != 0){
						emp = serviceSchedulePrintPopup.getOlbEmployee().getSelectedItem();
						technicianId = emp.getCount()+"";
					}
				
				String team="";
				if(serviceSchedulePrintPopup.getOlbTeam().getSelectedIndex()!=0)
					team=serviceSchedulePrintPopup.getOlbTeam().getValue(serviceSchedulePrintPopup.getOlbTeam().getSelectedIndex());
				
				String frmDate=AppUtility .parseDate(serviceSchedulePrintPopup.getDbFromDate().getValue());
				String toDate= AppUtility .parseDate(serviceSchedulePrintPopup.getDbTodate().getValue());
				String companyid=UserConfiguration.getCompanyId()+"";
				String status="";
				if(serviceSchedulePrintPopup.getLbStatus().getSelectedIndex()!=0) {
					status=serviceSchedulePrintPopup.getLbStatus().getValue(serviceSchedulePrintPopup.getLbStatus().getSelectedIndex());
				}
				System.out.println("companyid==="+UserConfiguration.getCompanyId()+"");		
				System.out.println("TEAM -"+team);
				System.out.println("From Date =="+serviceSchedulePrintPopup.getDbFromDate().getValue());
				System.out.println("To Date "+serviceSchedulePrintPopup.getDbTodate().getValue());
				
				/**Date 14-10-2020 by Amol **/
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "CompanyAsLetterHead")){
					cnt = cnt + 1;
					Console.log("inside CompanyAsLetterHead");
					if(cnt>0){
			    		panel=new PopupPanel(true);
			    		panel.add(conditionPopup);
			    		panel.setGlassEnabled(true);
			    		panel.show();
			    		panel.center();
			             Console.log("inside print reminder");
			    	}
					
					
					
				}else{
					final String url = GWT.getModuleBaseURL()+"intechPestControlServlet"+"?"+"techTeam="+team+
							"&"+"technicianId="+technicianId+"&"+"fromDate="+frmDate+"&"+"toDate="+toDate+"&"+"companyId="+companyid+ "&"+ "preprint=" + "plane"+"&"+"status="+status;
					Window.open(url, "test", "enabled");	
				}
				
				
				
				
				
				
				
		
				serviceSchedulePrintPopup.hidePopUp();
				}
				
			}
			
		if (event.getSource().equals(conditionPopup.getBtnOne())) {
			Console.log("inside CompanyAsLetterHead conditionPopup.getBtnOne()");
			Employee emp = null;
			String technicianId = null;
			if (serviceSchedulePrintPopup.getOlbEmployee().getSelectedIndex() != 0) {
				emp = serviceSchedulePrintPopup.getOlbEmployee()
						.getSelectedItem();
				technicianId = emp.getCount() + "";
			}

			String team = "";
			if (serviceSchedulePrintPopup.getOlbTeam().getSelectedIndex() != 0)
				team = serviceSchedulePrintPopup.getOlbTeam().getValue(serviceSchedulePrintPopup.getOlbTeam().getSelectedIndex());

			String frmDate = AppUtility.parseDate(serviceSchedulePrintPopup.getDbFromDate().getValue());
			String toDate = AppUtility.parseDate(serviceSchedulePrintPopup.getDbTodate().getValue());
			String companyid = UserConfiguration.getCompanyId() + "";
			
			String status="";
			if(serviceSchedulePrintPopup.getLbStatus().getSelectedIndex()!=0) {
				status=serviceSchedulePrintPopup.getLbStatus().getValue(serviceSchedulePrintPopup.getLbStatus().getSelectedIndex());
			}
			if (cnt > 0) {
				final String url = GWT.getModuleBaseURL()
						+ "intechPestControlServlet" + "?" + "techTeam=" + team
						+ "&" + "technicianId=" + technicianId + "&"
						+ "fromDate=" + frmDate + "&" + "toDate=" + toDate
						+ "&" + "companyId=" + companyid + "&" + "preprint="
						+ "yes"+"&"+"status="+status;
				Window.open(url, "test", "enabled");
				panel.hide();
			}
		}
		if (event.getSource().equals(conditionPopup.getBtnTwo())) {
			Console.log("inside CompanyAsLetterHead conditionPopup.getBtnTwo()");
			
			Employee emp = null;
			String technicianId = null;
			if (serviceSchedulePrintPopup.getOlbEmployee().getSelectedIndex() != 0) {
				emp = serviceSchedulePrintPopup.getOlbEmployee()
						.getSelectedItem();
				technicianId = emp.getCount() + "";
			}

			String team = "";
			if (serviceSchedulePrintPopup.getOlbTeam().getSelectedIndex() != 0)
				team = serviceSchedulePrintPopup.getOlbTeam().getValue(serviceSchedulePrintPopup.getOlbTeam().getSelectedIndex());

			String frmDate = AppUtility.parseDate(serviceSchedulePrintPopup.getDbFromDate().getValue());
			String toDate = AppUtility.parseDate(serviceSchedulePrintPopup.getDbTodate().getValue());
			String companyid = UserConfiguration.getCompanyId() + "";
			String status="";
			if(serviceSchedulePrintPopup.getLbStatus().getSelectedIndex()!=0) {
				status=serviceSchedulePrintPopup.getLbStatus().getValue(serviceSchedulePrintPopup.getLbStatus().getSelectedIndex());
			}
			if (cnt > 0) {
				final String url = GWT.getModuleBaseURL()
						+ "intechPestControlServlet" + "?" + "techTeam=" + team
						+ "&" + "technicianId=" + technicianId + "&"
						+ "fromDate=" + frmDate + "&" + "toDate=" + toDate
						+ "&" + "companyId=" + companyid + "&" + "preprint="
						+ "no"+"&"+"status="+status;
				Window.open(url, "test", "enabled");
				panel.hide();
			}
		}
			
			
			
			
			if(event.getSource().equals(serviceSchedulePrintPopup.getLblCancel())){
				serviceSchedulePrintPopup.hidePopUp();
			}
			/**
			 * end komal
			 */
			
			/**
			 * Date 04-05-2018
			 * Developer :- Vijay
			 * Description :- services list complete services with bulk  
			 */
			if(event.getSource()==serviceCompletionPopup.getLblOk()){
				final ArrayList<Service> selectedlist = getAllSelectedRecords();
				for(Service service : selectedlist){
//					if(service.getServiceDate().after(new Date())){
					/**
					 * @author Vijay Date :- 11-02-2022 
					 * Des :- the default time set to 13 so its giving validation when we completing today service also so parse the date to time will set to 00
					 */
					if(AppUtility.formatDate(AppUtility.parseDate(service.getServiceDate())).after(AppUtility.formatDate(AppUtility.parseDate(new Date())))){
						form.showDialogMessage("Service Date exceeds current date. Please reschedule the date");
						serviceCompletionPopup.hidePopUp();
						return;
					}
				}
				System.out.println("serviceCompletionPopup.getCbServiceDateAsServiceCompletionDate().getValue() =="+serviceCompletionPopup.getDbServiceCompletionDate().getValue());
				if(serviceCompletionPopup.getCbServiceDateAsServiceCompletionDate().getValue()==true && serviceCompletionPopup.getDbServiceCompletionDate().getValue()!=null
					|| serviceCompletionPopup.getCbServiceDateAsServiceCompletionDate().getValue()==false && serviceCompletionPopup.getDbServiceCompletionDate().getValue() == null ){
					form.showDialogMessage("Please select Either Service Completion Date OR Service Date As Service completion Date");
				}
				else if(serviceCompletionPopup.getTaRemark().getValue().equals("") ){
					form.showDialogMessage("Please add Remark!");
				}
				else{
					
					final String remark = serviceCompletionPopup.getTaRemark().getValue();
					Date serviceCompletionDate = null;
					if(serviceCompletionPopup.getDbServiceCompletionDate().getValue()!=null){
						 serviceCompletionDate = serviceCompletionPopup.getDbServiceCompletionDate().getValue();
					}
					final Date completionDate = serviceCompletionDate;
					form.showWaitSymbol();
					servicelistserviceAsync.markCompleteServicesWithBulk(selectedlist, remark, serviceCompletionDate, new AsyncCallback<Integer>() {
						
						@Override
						public void onSuccess(Integer result) {
							// TODO Auto-generated method stub
							if(result==1){
								List<Service> list = form.getSuperTable().getDataprovider().getList();
								for(int i=0;i<selectedlist.size();i++){
									for(int j=0;j<list.size();j++){
										if(selectedlist.get(i).getCount() == list.get(j).getCount()){
											list.get(j).setStatus("Completed");
											list.get(j).setServiceCompleteRemark(remark);
											if(completionDate!=null){
												list.get(j).setServiceCompletionDate(completionDate);
											}
											else{
												list.get(j).setServiceCompletionDate(list.get(j).getServiceDate());
											}
											/*** Date 1-12-2018 By Vijay for selected records set false for untick **/
											list.get(j).setRecordSelect(false);

										}
									}
									
								}

								/**
								 * @author Abhinav Bihade
								 * @since  6/12/2019
								 * Rohan Bhagade Created this taks as per NBHC Requirement Approved by Anil Pal for change Dialog Message  
								 * 
								 */
//								form.showDialogMessage("Services Completion process started.!");
								form.showDialogMessage("Service Completed, Process Started Successfully.It will take some time to update.!");
								form.getSuperTable().getDataprovider().setList(list);
								serviceCompletionPopup.hidePopUp();
								form.getSuperTable().getTable().redraw();
								
							}else if(result==-1){
								form.showDialogMessage("Please try after 5 minutes! Last task is InProcess");
								serviceCompletionPopup.hidePopUp();

							}
							/**
							 * @author Vijay Chougule
							 * Date 30-11-2018
							 * Des :- if service wise billing process config is active then only allow
							 * to complete services with bulk of 100 only not more than this validation
							 */
							else if(result==-2){
								form.showDialogMessage("Please select services less than 100 ");
							}
							form.hideWaitSymbol();
							serviceCompletionPopup.hidePopUp();

						}
						
						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							form.hideWaitSymbol();

						}
					});
				}
				
			}
			if(event.getSource()==serviceCompletionPopup.getLblCancel()){
				serviceCompletionPopup.hidePopUp();
			}
			/**
			 * ends here
			 */
			/**
			 * nidhi
			 * 2-08-2018
			 * for technician popup
			 */
			if(event.getSource()== serviceTechnicianPopup.getLblOk()){
				if(serviceTechnicianPopup.getOlbEmployee().getSelectedIndex()!=0){
					ArrayList<Service> selectedlist = getAllSelectedRecords();
					
					/**
					 * @author Anil @since 23-03-2021
					 * validating stock once service is schedule and we are changing technician
					 */
					form.showWaitSymbol();
					servicelistserviceAsync.validateTechnicianStock(UserConfiguration.getCompanyId(), serviceTechnicianPopup.getOlbEmployee().getValue(), selectedlist,new AsyncCallback<ArrayList<String>>() {

						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							form.hideWaitSymbol();
							if (caught instanceof StatusCodeException && ((StatusCodeException) caught).getStatusCode() == 0) {
								view.showDialogMessage("Server unreachable, Please check your internet connection!");
					        }
						}

						@Override
						public void onSuccess(ArrayList<String> result) {
							// TODO Auto-generated method stub
							form.hideWaitSymbol();
							if(result==null||result.size()==0){
								setTechnicianToServices();
							}else{
								form.showDialogMessage(result.toString());
							}
						}
					});
					
					
				}else{
					form.showDialogMessage("Please Select Technician.");
				}
			}
			
			if(event.getSource()== serviceTechnicianPopup.getLblCancel()){
				serviceTechnicianPopup.hidePopUp();
			}
			
			/**
			 * Updated By:Viraj
			 * Date: 15-06-2019
			 * Description: To assign branch in bulk
			 */
			if(event.getSource()== serviceBranchPopup.getLblOk()){
				if(serviceBranchPopup.getOlbBranch().getSelectedIndex()!=0){
					setBranchToServices();
				}else{
					form.showDialogMessage("Please Select Branch.");
				}
			}
			
			if(event.getSource()== serviceBranchPopup.getLblCancel()){
				serviceBranchPopup.hidePopUp();
			}
			/** Ends **/
			
			/**
			 * Updated By:Viraj
			 * Date: 29-06-2019
			 * Description: To change status for multiple entries in one go
			 */
			if(event.getSource()== serviceStatusPopup.getLblOk()){
				if(serviceStatusPopup.getStatus().getSelectedIndex()!=0){
					setStatusToServices();
				}else{
					form.showDialogMessage("Please Select Status.");
				}
			}
			
			if(event.getSource()== serviceStatusPopup.getLblCancel()){
				serviceStatusPopup.hidePopUp();
			}
			/** Ends **/
			
			/**
			 * Date 07-09-2018 By Vijay
			 * Developer :- Vijay
			 * Description :- Reschedule services with bulk
			 */

			if(event.getSource()==reschedule.getBtnReschedule())
			{
				boolean val=validateReschedule();
				if(val==true)
				{
					final ArrayList<Service> selectedlist = getAllSelectedRecords();
					ArrayList<SuperModel> servicelist = new ArrayList<SuperModel>();
					for(Service service:selectedlist){
					ModificationHistory history=new ModificationHistory();
					history.oldServiceDate=service.getServiceDate();
					history.resheduleDate=reschedule.getDateBox().getValue();
					
					String serviceHrs=reschedule.getP_servicehours().getItemText(reschedule.getP_servicehours().getSelectedIndex());
					String serviceMin=reschedule.getP_servicemin().getItemText(reschedule.getP_servicemin().getSelectedIndex());
					String serviceAmPm=reschedule.getP_ampm().getItemText(reschedule.getP_ampm().getSelectedIndex());
					String calculatedServiceTime=serviceHrs+":"+serviceMin+""+serviceAmPm;
					if(serviceHrs.equals("--") || serviceMin.equals("--")){
						history.resheduleTime="Flexible";
					}else{
						history.resheduleTime=calculatedServiceTime;
					}
					history.reason=reschedule.getTextArea().getText();
					history.userName=LoginPresenter.loggedInUser;
					history.systemDate=new Date();
					Date reschDate=reschedule.getDateBox().getValue();
					service.getListHistory().add(history);
					service.setServiceDate(reschDate);
					service.setStatus(Service.SERVICESTATUSRESCHEDULE);
					
					/** Date :- 15-01-2020 by vijay for while rescheduling updating service Day ***/ 
					service.setServiceDay(ContractForm.serviceDay(reschDate));
                    service.setServiceTime(	history.resheduleTime);
                    service.setServiceScheduled(false); //added on 26-07-2024 
					servicelist.add(service);
					
					}
					genasync.save(servicelist, new AsyncCallback<ArrayList<ReturnFromServer>>() {
						
						@Override
						public void onSuccess(ArrayList<ReturnFromServer> result) {
							// TODO Auto-generated method stub
							updateComplaint(selectedlist);
							
							List<Service> list = form.getSuperTable().getDataprovider().getList();
							ArrayList<Service> selectedlist = getAllSelectedRecords();
							for(int i=0;i<selectedlist.size();i++){
								for(int j=0;j<list.size();j++){
									if(selectedlist.get(i).getCount() == list.get(j).getCount()){
										list.get(j).setStatus("Rescheduled");
										list.get(j).setServiceDate(reschedule.getDateBox().getValue());
										String serviceHrs=reschedule.getP_servicehours().getItemText(reschedule.getP_servicehours().getSelectedIndex());
										String serviceMin=reschedule.getP_servicemin().getItemText(reschedule.getP_servicemin().getSelectedIndex());
										String serviceAmPm=reschedule.getP_ampm().getItemText(reschedule.getP_ampm().getSelectedIndex());
										String calculatedServiceTime=serviceHrs+":"+serviceMin+""+serviceAmPm;
										if(serviceHrs.equals("--") || serviceMin.equals("--")){
											list.get(j).setServiceTime("Flexible");
										}else{
											list.get(j).setServiceTime(calculatedServiceTime);
										}
										/**
										 * @author Anil @since 20-07-2021
										 * Issue raised by if we are rescheduling service then its scheduled flag gets false but same thing is not reflected on list
										 * need to refresh the list
										 */
										list.get(j).setServiceScheduled(false);
									}
								}
								
							}
								form.showDialogMessage("Services Rescheduled successfully!");
								form.getSuperTable().getDataprovider().setList(list);
								panel.hide();
								form.hideWaitSymbol();
								form.getSuperTable().getTable().redraw();
						}
						
						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							
						}
					});
				}
			}
			if(event.getSource()==reschedule.getBtnCancel()){
				panel.hide();
			}
			
			/**
			 * ends here
			 */
			
			/*** Date 26-03-2019 by Vijay for NBHC CCPM cancellation with config remark****/
			if(event.getSource() == cancellation.getLblOk()){
				if(cancellation.getOlbCancellationRemark().getSelectedIndex()==0){
					form.showDialogMessage("Remark is mandatory!");
				}else{
					cancelServices(getAllSelectedRecords());
					cancellation.hidePopUp();
				}

			}
			if(event.getSource()==cancellation.getLblCancel()){
				cancellation.hidePopUp();
			}
			if(multipleServicesSchedulePopup.getLblOk() == event.getSource()){
				
				GWTCAlert alert = new GWTCAlert();
				List<Service> serviceList=new ArrayList<Service>();
				serviceList=multipleServicesSchedulePopup.getMultipleServicesScheduleTable().getDataprovider().getList();
				boolean serviceCompletedFlag=false;
				for(Service s : serviceList) {		
						if(s.getStatus().equals(Service.SERVICESTATUSCOMPLETED)){
							alert.alert("This service is already Completed. Do a Reverse Service Completion if required.");
							serviceCompletedFlag=true;
							break;
						}
				}
				if(serviceCompletedFlag)
					return;
				
				if(multipleServicesSchedulePopup.getLblOk().getText().equals("Save Services")){
//					List<Service> serviceList=new ArrayList<Service>();
//					serviceList=multipleServicesSchedulePopup.getMultipleServicesScheduleTable().getDataprovider().getList();
					for(Service s : serviceList) {								
						Date rescheduleDate = s.getServiceDate();
						Console.log("ashwini before calling saveServiceDetails rescheduleDate="+rescheduleDate);
						
						if(!validateRescheduleDate(rescheduleDate,s.getServiceTime()))	{
							alert.alert("You are trying to schedule service in past..!");
							return;
						}
						
					}
					multipleServicesSchedulePopup.getLblOk().setText("Processing");
					saveServiceDetails(multipleServicesSchedulePopup.getMultipleServicesScheduleTable().getDataprovider().getList());
					
				}
				if(multipleServicesSchedulePopup.getLblOk().getText().equals("Schedule (Assign to technician)")){
					Console.log("schedule popup:" );
					multipleServicesSchedulePopup.getLblOk().setText("Processing");
					//scheduleServices();
					validateMaterial();
				}
				}
			
			if(event.getSource()==datePopUp.getBtnOne()){
			//Ashwini Patil Date:18-01-2023 added from date and todate input fields to resolve duplicate service id issue in services created in given duration.
				boolean serviceIdFlag=false;
				boolean dateFlag=false;
				boolean day1and2flag=false;
				boolean contractIdFlag=false;
				boolean fromToServiceIdFlag=false;
				int serviceid=0;
				int contractId=0;
				int fromServiceId=0;
				int toServiceId=0;
				int filterCount=0;
				Date fromdate=null;
				Date todate=null;
				Date day1=null;
				Date day2=null;
				
				if(datePopUp.getFromDate().getValue()!=null&&datePopUp.getToDate().getValue()!=null){
					dateFlag=true;	
					filterCount++;
					Console.log("fromdate="+datePopUp.getFromDate().getValue()+" todate="+datePopUp.getToDate().getValue());					
					Date formDate;
					Date toDate;
					formDate=datePopUp.getFromDate().getValue();
					toDate=datePopUp.getToDate().getValue();
					
					int diffdays = AppUtility.getDifferenceDays(formDate, toDate);
					Console.log("diffdays "+diffdays);
					if(diffdays>7){
						form.showDialogMessage("Please select from date and to date range within 7 days");
						return;
					}
				}
				if(datePopUp.getDay1().getValue()!=null&&datePopUp.getDay2().getValue()!=null){
					day1and2flag=true;	
					filterCount++;
					Console.log("day1="+datePopUp.getDay1().getValue()+" day2="+datePopUp.getDay2().getValue());										
				}
				if(datePopUp.getIbServiceId().getValue()!=null){
					serviceIdFlag=true;	
					filterCount++;
				}
				if(datePopUp.getIbContractId().getValue()!=null){
					contractIdFlag=true;	
					filterCount++;
					contractId=datePopUp.getIbContractId().getValue();
				}
				if(datePopUp.getIbFromServiceId().getValue()!=null&&datePopUp.getIbToServiceId().getValue()!=null){
					fromToServiceIdFlag=true;	
					filterCount++;
					fromServiceId=datePopUp.getIbFromServiceId().getValue();
					toServiceId=datePopUp.getIbToServiceId().getValue();
					if(fromServiceId>toServiceId) {
						form.showDialogMessage("From Service Id has to be smaller than To Service Id!");
						return;
					}
				}
				if(!serviceIdFlag&&!dateFlag&&!day1and2flag&&!contractIdFlag&&!fromToServiceIdFlag){
					form.showDialogMessage("Please enter Service ID or Contract ID or Form date - To date or Day1 -Day2 or From Service ID - To Service ID!");
					return;
				}
//				if(serviceIdFlag&&dateFlag&&day1and2flag&&contractIdFlag&&fromToServiceIdFlag){
				if(filterCount>1) {
					form.showDialogMessage("Enter either Service ID or Contract ID or Form date - To date or Day1 -Day2 or From Service ID - To Service ID!");
					return;
				}
				
				if(datePopUp.getFromDate().getValue()!=null) {
					fromdate=datePopUp.getFromDate().getValue();
					CalendarUtil.addDaysToDate(fromdate, 1);
				}
				if(datePopUp.getToDate().getValue()!=null) {
					todate=datePopUp.getToDate().getValue();
					CalendarUtil.addDaysToDate(todate, 1);
				}
				if(datePopUp.getDay1().getValue()!=null) {
					day1=datePopUp.getDay1().getValue();
					CalendarUtil.addDaysToDate(day1, 1);
					Console.log("Day 1 before call="+day1);
				}
				if(datePopUp.getDay2().getValue()!=null) {
					day2=datePopUp.getDay2().getValue();
					CalendarUtil.addDaysToDate(day2, 1);
					Console.log("Day 2 before call="+day2);
				}
				if(datePopUp.getIbServiceId().getValue()!=null)
					serviceid=datePopUp.getIbServiceId().getValue();
				
				form.showWaitSymbol();
				async.updateDuplicateServices(UserConfiguration.getCompanyId(), fromdate, todate,serviceid,day1,day2,contractId,fromServiceId,toServiceId, new AsyncCallback<Void>() {
//				async.updateDuplicateServices(UserConfiguration.getCompanyId(), datePopUp.getFromDate().getValue(), datePopUp.getToDate().getValue(),0, new AsyncCallback<Void>() {
					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						form.hideWaitSymbol();
					}

					@Override
					public void onSuccess(Void result) {
						// TODO Auto-generated method stub
						form.hideWaitSymbol();
						datePopUp.getFromDate().setValue(null);
						datePopUp.getToDate().setValue(null);
						datePopUp.getDay1().setValue(null);
						datePopUp.getDay2().setValue(null);
						datePopUp.getIbContractId().setValue(null);
						datePopUp.getIbFromServiceId().setValue(null);
						datePopUp.getIbToServiceId().setValue(null);
						datePopUp.getIbServiceId().setValue(null);
						datePanel.hide();
						form.showDialogMessage("Update Process Started...");
						
					}
				});
			}
			if(event.getSource()==datePopUp.getBtnTwo()){
				datePopUp.getFromDate().setValue(null);
				datePopUp.getToDate().setValue(null);
				datePopUp.getDay1().setValue(null);
				datePopUp.getDay2().setValue(null);
				datePopUp.getIbContractId().setValue(null);
				datePopUp.getIbFromServiceId().setValue(null);
				datePopUp.getIbToServiceId().setValue(null);
				datePopUp.getIbServiceId().setValue(null);
				datePanel.hide();
			}
			
			Console.log("on service on click on service presenter");
			if(event.getSource() == genralInvoicePopup.getBtnClose()){
				System.out.println("on close in service presenter");
				Console.log("on close in service presenter");
				List<Service> servieclist =  form.getSuperTable().getListDataProvider().getList();
				Service selectedService = (Service) genralInvoicePopup.getPresenter().getModel();
				for(Service serviceentity : servieclist){
					if(serviceentity.getContractCount()==selectedService.getContractCount()
							&& serviceentity.getCount() == selectedService.getCount()){
						serviceentity.setStatus(selectedService.getStatus());
						if(selectedService.getServiceCompletionDate()!=null)
						serviceentity.setServiceCompletionDate(selectedService.getServiceCompletionDate());
						if(selectedService.getServiceCompleteDuration()!=0)
						serviceentity.setServiceCompleteDuration(selectedService.getServiceCompleteDuration());
						if(selectedService.getServiceCompleteRemark()!=null && !selectedService.getServiceCompleteRemark().equals(""))
						serviceentity.setServiceCompleteRemark(selectedService.getServiceCompleteRemark());
					}
				}
				form.getSuperTable().getDataprovider().setList(servieclist);
				genralInvoicePopup.viewDocumentPanel.hide();
				
				generalPanel.hide();
				
				AppMemory.getAppMemory().currentScreen = Screen.SERVICE;
				
				/**
				 * @author Anil @since 28-01-2021
				 */
				form.initializeScreen();
				form.intializeScreenMenus();
				form.addClickEventOnScreenMenus();
				form.addClickEventOnActionAndNavigationMenus();
//				setEventHandeling();
				for (int k = 0; k < mem.skeleton.menuLabels.length; k++) {
					if (mem.skeleton.registration[k] != null)
						mem.skeleton.registration[k].removeHandler();
				}

				for (int k = 0; k < mem.skeleton.menuLabels.length; k++)
					mem.skeleton.registration[k] = mem.skeleton.menuLabels[k].addClickHandler(this);
				form.setToViewState();
			}
			
			
			if(event.getSource().equals(replanservicepopup.getLblOk())){
				reactonReplanServicesOK();
			}
			if(event.getSource().equals(replanservicepopup.getLblCancel())){
				replanservicepopup.hidePopUp();
			}
			
			
			
			if(event.getSource() == CommunicationLogPopUp.getBtnOk()){
				form.showWaitSymbol();
			    List<InteractionType> list = CommunicationLogPopUp.getCommunicationLogTable().getDataprovider().getList();
			    ArrayList<InteractionType> interactionlist = new ArrayList<InteractionType>();
			    interactionlist.addAll(list);
			    boolean checkNewInteraction = AppUtility.checkNewInteractionAdded(interactionlist);
			    if(checkNewInteraction==false){
			    	form.showDialogMessage("Please add new interaction details");
			    	form.hideWaitSymbol();
			    }else{	
//			    	final Date followUpDate = interactionlist.get(interactionlist.size()-1).getInteractionDueDate();
			    	 communicationService.saveCommunicationLog(interactionlist, new AsyncCallback<Void>() {
						@Override
						public void onFailure(Throwable caught) {
							form.showDialogMessage("Unexpected Error");
							form.hideWaitSymbol();
							LoginPresenter.communicationLogPanel.hide();
						}

						@Override
						public void onSuccess(Void result) {
							form.showDialogMessage("Data Save Successfully");
							form.hideWaitSymbol();
//							model.setFollowUpDate(followUpDate);
//							form.getDbleaddate().setValue(followUpDate);
							LoginPresenter.communicationLogPanel.hide();
						}
					});
			    }
			}
			if(event.getSource() == CommunicationLogPopUp.getBtnCancel()){
				LoginPresenter.communicationLogPanel.hide();
			}
			if(event.getSource() == CommunicationLogPopUp.getBtnAdd()){
					form.showWaitSymbol();
					String remark = CommunicationLogPopUp.getRemark().getValue();
					Date dueDate = CommunicationLogPopUp.getDueDate().getValue();
					String interactiongGroup =null;
					if(CommunicationLogPopUp.getOblinteractionGroup().getSelectedIndex()!=0)
						interactiongGroup=CommunicationLogPopUp.getOblinteractionGroup().getValue(CommunicationLogPopUp.getOblinteractionGroup().getSelectedIndex());
					boolean validationFlag = AppUtility.validateCommunicationlog(remark,dueDate);
					
					List<Service> serviceList = getAllSelectedRecords();

					if(serviceList.size()==0){
						form.showDialogMessage("Please select processed record !");
					}else if(serviceList.size()>1){
						form.showDialogMessage("Please select only one processed record !");
					}else{
						if(validationFlag){

							InteractionType communicationLog =  AppUtility.getCommunicationLog(AppConstants.SERVICEMODULE,AppConstants.SERVICE,serviceList.get(0).getCount(),
									serviceList.get(0).getEmployee(),remark,dueDate,serviceList.get(0).getPersonInfo(),null,interactiongGroup, serviceList.get(0).getBranch(),serviceList.get(0).getStatus());
							CommunicationLogPopUp.getCommunicationLogTable().getDataprovider().getList().add(communicationLog);
							CommunicationLogPopUp.getRemark().setValue("");
							CommunicationLogPopUp.getDueDate().setValue(null);
							CommunicationLogPopUp.getOblinteractionGroup().setSelectedIndex(0);
						}
					}
					form.hideWaitSymbol();
			}
			
			
			/**
			 * @author Ashwini Patil
			 * @since 1-03-2022
			 * Service can be completed from 3 places in erp.
			 * This code if refered from Complete option from 3 line menu of customer service list.
			 * */
			if(event.getSource()==lblComplete){
//				form.showDialogMessage("Complete button clicked");
				Console.log("lblComplete clicked");
				Console.log("serviceObject id="+serviceObject.getCount());
				
				//for orion
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("EVA Pedio", "PC_DocumentUploadMandatory")){
					if(serviceObject.getUptestReport()==null){
						form.showDialogMessage("Service Report upload is mandatory! Please upload Service Report in Service "+serviceObject.getCount());
						return;
					}						
				}
				
				/*
				 * Ashwini Patil 
				 * Date:29-08-2024
				 * Orion does not want to allow service completion of last month services after specific deadline
				 * deadline is defined in company - Customer service setup - Service completion deadline field
				 */
				boolean validDate=AppUtility.validateServiceCompletionDate(serviceObject.getServiceDate());
				Console.log("validDate="+validDate);
				if(!validDate) {
					form.showDialogMessage("You cannot completed old service now!");
					return;
				}
				final ArrayList<Service> selectedlist=new ArrayList<Service>();
				if(serviceObject.getStatus().equals("Completed")){
					form.showDialogMessage("Service already completed");
					return;
				}
				if(serviceObject.getEmployee()==null||serviceObject.getEmployee().equals("")){
					form.showDialogMessage("Select technician");
					return;
				}
				selectedlist.add(serviceObject);
				/*
				 * Added by Sheetal:27-10-2021
				 * Des : Document upload in Customer Report section should be mandatory before closure of service
				 */
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "PC_DocumentUploadMandatory")){
					
						if(AppUtility.ValidateCustomerReport(serviceObject.getUptestReport(),serviceObject.getServiceSummaryReport2())){
							form.showDialogMessage("Please upload atleast one document in Customer Report tab on Customer Service page to complete service");
							return;
					      }
				    
		        }
				ArrayList<Integer> complaintServiceId = new ArrayList<Integer>();
				boolean complaintServiceflag = false;
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "EnableValidateAssesmentReport")){
					complaintServiceflag = true;
				}
				
				/**
				 * Date 14-11-2019 @author Vijay Chougule
				 * Des :- NBHC CCPM complaint service assessment should not be in created status for scheduling
				 */
				if(serviceObject.getTicketNumber()!=-1 && complaintServiceflag){
					complaintServiceId.add(serviceObject.getCount());
				}
				
					
				
				
				/**
				 * @author Anil
				 * @since 12-05-2020
				 */
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Complain", "ComplainServiceWithTurnAroundTime")){
						Console.log("lblcomplete click - ComplainServiceWithTurnAroundTime true");
						/**
						 * @author Anil
						 * @since 09-11-2020
						 * As service date's time is default set to 13, cuasing an issue while completing service before 13:00 PM
						 */
						if(AppUtility.formatDate(AppUtility.parseDate(serviceObject.getServiceDate())).after(AppUtility.formatDate(AppUtility.parseDate(new Date())))){
//						if(service.getServiceDate().after(new Date())){
							form.showDialogMessage("Service Date exceeds current date. Please reschedule the date");
							return;
						}
					
					final Date completionDate = new Date();
					form.showWaitSymbol();
					servicelistserviceAsync.markCompleteServicesWithBulk(selectedlist, "", completionDate, new AsyncCallback<Integer>() {
						@Override
						public void onSuccess(Integer result) {
							// TODO Auto-generated method stub
							if(result==1){
								List<Service> list = form.getSuperTable().getDataprovider().getList();
								for(int i=0;i<selectedlist.size();i++){
									for(int j=0;j<list.size();j++){
										if(selectedlist.get(i).getCount() == list.get(j).getCount()){
											list.get(j).setStatus("Completed");
											list.get(j).setServiceCompleteRemark("");
											if(completionDate!=null){
												list.get(j).setServiceCompletionDate(completionDate);
											}
											else{
												list.get(j).setServiceCompletionDate(list.get(j).getServiceDate());
											}
											list.get(j).setRecordSelect(false);
										}
									}
								}
								form.showDialogMessage("Service Completed, Process Started Successfully.It will take some time to update.!");
								form.getSuperTable().getDataprovider().setList(list);
								serviceCompleteFromSchedulePopup.hidePopUp();
								form.getSuperTable().getTable().redraw();
								
							}else if(result==-1){
								form.showDialogMessage("Please try after 5 minutes! Last task is InProcess");
							}else if(result==-2){
								form.showDialogMessage("Please select services less than 100 ");
							}
							form.hideWaitSymbol();
						}
						
						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							form.hideWaitSymbol();

						}
					});
					
					return;
				}
				
				
				/**
				 * Date 14-11-2019 @author Vijay Chougule
				 * Des :- NBHC CCPM complaint service assessment validation
				 * Assessment must completed then allowing service to scheduling
				 */
				if(complaintServiceId.size()!=0){
					Console.log("lblcomplete click - complaintServiceId condition" );
					form.showWaitSymbol();
					validateComplaintService.ValidateAssessmentForComplaintService(complaintServiceId, selectedlist.get(0).getCompanyId(), new AsyncCallback<String>() {
						
						@Override
						public void onSuccess(String result) {
							// TODO Auto-generated method stub
							if(result.equals("success")){
								serviceCompleteFromSchedulePopup.getCbServiceDateAsServiceCompletionDate().setValue(false);
								serviceCompleteFromSchedulePopup.showPopUp();
							}
							else{
								form.showDialogMessage(result);
							}
							form.hideWaitSymbol();
						}
						
						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							form.hideWaitSymbol();
							form.showDialogMessage(caught.getMessage());
						}
					});
				}
				else{
					serviceCompleteFromSchedulePopup.getCbServiceDateAsServiceCompletionDate().setValue(false);
					serviceCompleteFromSchedulePopup.showPopUp();
				}
			}
			
			if(event.getSource()==serviceCompleteFromSchedulePopup.getLblOk()){
				Console.log("serviceCompleteFromSchedulePopup ok clicked");
				final ArrayList<Service> selectedlist = new ArrayList<Service>();
				
				
					/**
					 * @author Vijay Date :- 11-02-2022 
					 * Des :- the default time set to 13 so its giving validation when we completing today service also so parse the date to time will set to 00
					 */
					if(AppUtility.formatDate(AppUtility.parseDate(serviceObject.getServiceDate())).after(AppUtility.formatDate(AppUtility.parseDate(new Date())))){
						form.showDialogMessage("Service Date exceeds current date. Please reschedule the date");
						serviceCompleteFromSchedulePopup.hidePopUp();
						return;
					}
				
				if(serviceCompleteFromSchedulePopup.getCbServiceDateAsServiceCompletionDate().getValue()==true && serviceCompleteFromSchedulePopup.getDbServiceCompletionDate().getValue()!=null
					|| serviceCompleteFromSchedulePopup.getCbServiceDateAsServiceCompletionDate().getValue()==false && serviceCompleteFromSchedulePopup.getDbServiceCompletionDate().getValue() == null ){
					form.showDialogMessage("Please select Either Service Completion Date OR Service Date As Service completion Date");
				}
				else if(serviceCompleteFromSchedulePopup.getTaCustomerFeedback().getValue().equals("") ){
					form.showDialogMessage("Please add Customer Feedback!");
				}
				else{
					
					final String remark = serviceCompleteFromSchedulePopup.getTaCustomerFeedback().getValue();
					Date serviceCompletionDate = null;
					if(serviceCompleteFromSchedulePopup.getDbServiceCompletionDate().getValue()!=null){
						 serviceCompletionDate = serviceCompleteFromSchedulePopup.getDbServiceCompletionDate().getValue();
					}
					final Date completionDate = serviceCompletionDate;
					form.showWaitSymbol();
					
					serviceObject.setServiceCompleteRemark(serviceCompleteFromSchedulePopup.getTaCustomerFeedback().getValue());
					if(serviceCompleteFromSchedulePopup.getTaTechRemark().getValue()!=null)
						serviceObject.setTechnicianRemark(serviceCompleteFromSchedulePopup.getTaTechRemark().getValue());
					
					if(serviceCompleteFromSchedulePopup.getLbRating().getSelectedIndex()!=0)
						serviceObject.setCustomerFeedback(serviceCompleteFromSchedulePopup.getLbRating().getValue(serviceCompleteFromSchedulePopup.getLbRating().getSelectedIndex()));
					Console.log("before calling markCompleteServicesWithBulk");
					
					selectedlist.add(serviceObject);
					servicelistserviceAsync.markCompleteServicesWithBulk(selectedlist, remark, serviceCompletionDate, new AsyncCallback<Integer>() {
						
						@Override
						public void onSuccess(Integer result) {
							// TODO Auto-generated method stub
							Console.log("in success of markCompleteServicesWithBulk");
							if(result==1){
								List<Service> list = form.getSuperTable().getDataprovider().getList();
								for(int i=0;i<selectedlist.size();i++){
									for(int j=0;j<list.size();j++){
										if(selectedlist.get(i).getCount() == list.get(j).getCount()){
											list.get(j).setStatus("Completed");
											list.get(j).setServiceCompleteRemark(remark);
											if(completionDate!=null){
												list.get(j).setServiceCompletionDate(completionDate);
											}
											else{
												list.get(j).setServiceCompletionDate(list.get(j).getServiceDate());
											}
											/*** Date 1-12-2018 By Vijay for selected records set false for untick **/
											list.get(j).setRecordSelect(false);

										}
									}
									
								}

								/**
								 * @author Abhinav Bihade
								 * @since  6/12/2019
								 * Rohan Bhagade Created this taks as per NBHC Requirement Approved by Anil Pal for change Dialog Message  
								 * 
								 */
//								form.showDialogMessage("Services Completion process started.!");
								form.showDialogMessage("Service Completed, Process Started Successfully.It will take some time to update.!");
								form.getSuperTable().getDataprovider().setList(list);
								serviceCompleteFromSchedulePopup.hidePopUp();
								form.getSuperTable().getTable().redraw();
								
							}else if(result==-1){
								form.showDialogMessage("Please try after 5 minutes! Last task is InProcess");
								serviceCompleteFromSchedulePopup.hidePopUp();

							}
							form.hideWaitSymbol();
							serviceCompleteFromSchedulePopup.hidePopUp();
							multipleServicesSchedulePopup.hidePopUp();
						}
						
						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							form.hideWaitSymbol();

						}
					});
					
					
				}
				
			}
			
			if(event.getSource()==serviceCompleteFromSchedulePopup.getLblCancel()){
				serviceCompleteFromSchedulePopup.hidePopUp();
			}
			
			
			if(event.getSource()==srprintschedulepopup.getLblOk()){
				reactonPrintsceduleOnEmail();
			}
			if(event.getSource()==srprintschedulepopup.getLblCancel()){
				srprintschedulepopup.hidePopUp();
			}
			
			if(event.getSource().equals(srprintschedulepopup.getBtnprint())){
				reactonprintSrCopy();
			}
			
			if (event.getSource().equals(conditionPopup2.getBtnOne())) {
				
				if(srprintschedulepopup.getDbFromDate().getValue()==null){
					form.showDialogMessage("Please add from date");
					return;
				}
				if(srprintschedulepopup.getDbTodate().getValue()==null){
					form.showDialogMessage("Please add to date");
					return;
				}
				if(srprintschedulepopup.getOlbEmployee().getSelectedIndex()==0){
					form.showDialogMessage("Please select technician");
					return;
				}
				
				String technicianName="";
				if(srprintschedulepopup.getOlbEmployee().getSelectedIndex() != 0){
					technicianName = srprintschedulepopup.getOlbEmployee().getValue(srprintschedulepopup.getOlbEmployee().getSelectedIndex());
				}
				
				String frmDate = "";
				frmDate = AppUtility.parseDate(srprintschedulepopup.getDbFromDate().getValue());
				
				String toDate = "";
				toDate = AppUtility.parseDate(srprintschedulepopup.getDbFromDate().getValue());
				
				Company comp = new Company();
				
				if(srprint>0){
					final String url = GWT.getModuleBaseURL()+"srformateversion1"+"?"+
							"&"+"technicianName="+technicianName+"&"+"fromDate="+frmDate+"&"+"toDate="+toDate+"&"+"companyId="+comp.getCompanyId()+""+ "&"+ "preprint=" + "yes";
					Window.open(url, "test", "enabled");
					panel2.hide();
		    	}
			
			}
			if (event.getSource().equals(conditionPopup2.getBtnTwo())) {
				if(srprintschedulepopup.getDbFromDate().getValue()==null){
					form.showDialogMessage("Please add from date");
					return;
				}
				if(srprintschedulepopup.getDbTodate().getValue()==null){
					form.showDialogMessage("Please add to date");
					return;
				}
				if(srprintschedulepopup.getOlbEmployee().getSelectedIndex()==0){
					form.showDialogMessage("Please select technician");
					return;
				}
				
				String technicianName="";
				if(srprintschedulepopup.getOlbEmployee().getSelectedIndex() != 0){
					technicianName = srprintschedulepopup.getOlbEmployee().getValue(srprintschedulepopup.getOlbEmployee().getSelectedIndex());
				}
				
				String frmDate = "";
				frmDate = AppUtility.parseDate(srprintschedulepopup.getDbFromDate().getValue());
				
				String toDate = "";
				toDate = AppUtility.parseDate(srprintschedulepopup.getDbFromDate().getValue());
				
				Company comp = new Company();
				
				if(srprint>0){
					final String url = GWT.getModuleBaseURL()+"srformateversion1"+"?"+
							"&"+"technicianName="+technicianName+"&"+"fromDate="+frmDate+"&"+"toDate="+toDate+"&"+"companyId="+comp.getCompanyId()+""+ "&"+ "preprint=" + "no";
					Window.open(url, "test", "enabled");
					panel2.hide();
		    	}
			}
			
			if (event.getSource().equals(conditionPopup3.getBtnOne())) {
				Company comp = new Company();
				final String url = GWT.getModuleBaseURL()+"srformateversion1"+"?"
						+"&"+"companyId="+comp.getCompanyId()+""+ "&"+ "preprint=" + "yes";
				Window.open(url, "test", "enabled");
				panel3.hide();

			}
			if (event.getSource().equals(conditionPopup3.getBtnTwo())) {
				Company comp = new Company();
				final String url = GWT.getModuleBaseURL()+"srformateversion1"+"?"
						+"&"+"companyId="+comp.getCompanyId()+""+ "&"+ "preprint=" + "no";
				Window.open(url, "test", "enabled");
				panel3.hide();

			}
			if(event.getSource() == otpPopup.getBtnOne()){
				Console.log("otp popup ok button clicked");
				Console.log("otp value="+otpPopup.getOtp().getValue());
				if(otpPopup.getOtp().getValue()!=null){
					if(generatedOTP==otpPopup.getOtp().getValue()){
						runRegularDownloadProgram();
						otppopupPanel.hide();
					}else {
						form.showDialogMessage("Invalid OTP");	
						smsService.checkConfigAndSendWrongOTPAttemptMessage(model.getCompanyId(), LoginPresenter.loggedInUser, generatedOTP, "Customer Service", new  AsyncCallback<String>() {

							@Override
							public void onFailure(Throwable caught) {
								// TODO Auto-generated method stub
								form.showDialogMessage("Failed");
							}

							@Override
							public void onSuccess(String result) {
								// TODO Auto-generated method stub
								Console.log("InvalidOTPDownloadNotification result="+result);
							}
						} );
					
					}
				}else
					form.showDialogMessage("Enter OTP");
			}
			if(event.getSource() == otpPopup.getBtnTwo()){
				otppopupPanel.hide();
			}


			if(event.getSource().equals(suspendservicepopup.getLblOk())){
				
				if(suspendservicepopup.getOlbCancellationRemark().getSelectedIndex()==0){
					form.showDialogMessage("Remark is mandatory!");
				}else{
					suspendServices(getAllSelectedRecords());
					suspendservicepopup.hidePopUp();
				}
			}
			
			if(event.getSource().equals(suspendservicepopup.getLblCancel())){
				suspendservicepopup.hidePopUp();
			}
			
			if(event.getSource().equals(rescheduleServicesInDurationPopup.getBtnOne())){
				Date fromdate=null;
				Date todate=null;
				ArrayList<Service> selectedlist = getAllSelectedRecords();
				if(selectedlist!=null){
					Console.log("selectedlist size="+selectedlist.size());
					if(rescheduleServicesInDurationPopup.getFromDate().getValue()==null||rescheduleServicesInDurationPopup.getToDate().getValue()==null){
						form.showDialogMessage("Select both from date and to date!");
						return;
					}
					if(rescheduleServicesInDurationPopup.getFromDate().getValue()!=null) {
						fromdate=rescheduleServicesInDurationPopup.getFromDate().getValue();
//						CalendarUtil.addDaysToDate(fromdate, 1);
					}
					if(rescheduleServicesInDurationPopup.getToDate().getValue()!=null) {
						todate=rescheduleServicesInDurationPopup.getToDate().getValue();
//						CalendarUtil.addDaysToDate(todate, 1);
					}
					form.showWaitSymbol();
					Console.log("company id="+selectedlist.get(0).getCompanyId()+" fromdate="+fromdate+" todate="+todate);
					async.rescheduleForPeriod(selectedlist.get(0).getCompanyId(), fromdate, todate,selectedlist, new AsyncCallback<Void>() {
						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							form.hideWaitSymbol();
							form.showDialogMessage("Service Date Updation Process Failed!");
						}

						@Override
						public void onSuccess(Void result) {
							// TODO Auto-generated method stub
							form.hideWaitSymbol();
							rescheduleServicesInDurationPopup.getFromDate().setValue(null);
							rescheduleServicesInDurationPopup.getToDate().setValue(null);		
							form.showDialogMessage("Service Date Updation Process Started...");
							
						}
					});
					
					
					
				}else{
					form.showDialogMessage("Select Services!");
					return;
				}
				
				datePanel.hide();
			}
			if(event.getSource().equals(rescheduleServicesInDurationPopup.getBtnTwo())){
				datePanel.hide();
			}
			
			if(event.getSource().equals(uploadPopup.getDownloadFormat())){
				Console.log("Download button clicked ");
				ArrayList<Service> selectedlist = getAllSelectedRecords();
				ArrayList<Service> openServicesList=new ArrayList<Service>();
				if(selectedlist!=null&&selectedlist.size()>0){
					Console.log("selectedlist size="+selectedlist.size());
					for(Service s:selectedlist){
						if(!s.getStatus().equals(Service.SERVICESTATUSCOMPLETED)&&!s.getStatus().equals(Service.SERVICESTATUSCANCELLED))
							openServicesList.add(s);
					}
				}
				if(openServicesList!=null&&openServicesList.size()>0){
					Console.log("openServicesList size="+openServicesList.size());
					servicelistserviceAsync.setServicesForScheduleOrCompleteViaExcelDonwload(openServicesList, new AsyncCallback<String>() {

						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							
						}

						@Override
						public void onSuccess(String result) {
							// TODO Auto-generated method stub
							String gwt = com.google.gwt.core.client.GWT
									.getModuleBaseURL();
							final String url = gwt + "csvservlet"
									+ "?type=" + 219;
							Window.open(url, "test", "enabled");
						}
					});
					
					
				}else{
					form.showDialogMessage("Select Open Services you wish to reschedule / complete /cancel / update!");
					return;
				}
				
			}
			if(event.getSource().equals(uploadPopup.getBtnOk())){
				Console.log("ok clicked from service presenter");
				reactOnServiceUpload(AppConstants.ScheduleOrCompleteViaExcel);
				uploadPopup.isDownloadClickedFromDifferentProgram=false;
				uploadPopupPanel.hide();
				uploadPopup.clear();
			}
			if(event.getSource().equals(uploadPopup.getBtnCancel())){
				Console.log("cancel clicked from service presenter");
				uploadPopup.isDownloadClickedFromDifferentProgram=false;
				uploadPopupPanel.hide();
				uploadPopup.clear();
			}
			if(event.getSource().equals(conditionPopupForPrint.getBtnOne())){
				final String url = GWT.getModuleBaseURL() + "serviceprintpdf"+"?compId=" + model.getCompanyId()+"&"+"preprint="+"yes";
				 Window.open(url, "test", "enabled");
				 printPanel.hide();
			}
			if(event.getSource().equals(conditionPopupForPrint.getBtnTwo())){
				final String url = GWT.getModuleBaseURL() + "serviceprintpdf"+"?compId=" + model.getCompanyId()+"&"+"preprint="+"no";
				 Window.open(url, "test", "enabled");
				 printPanel.hide();
			}
			 
			
			if(event.getSource()==replanservicepopup.getBtnSpecificDay()){
				specificDayPanel=new PopupPanel(true);
				specificDayPanel.add(specificDayPopup);
				specificDayPanel.center();
				specificDayPanel.show();
			}
			if(event.getSource()==replanservicepopup.getBtnExcludeDay()){
				excludeDayPanel=new PopupPanel(true);
				excludeDayPanel.add(excludeDayPopup);
				excludeDayPanel.center();
				excludeDayPanel.show();
			}	
			if(event.getSource()==specificDayPopup.getBtnOk()){

				ArrayList<Integer> specificDays=new ArrayList<Integer>();
				if(specificDayPopup.getCbSun().getValue())
					specificDays.add(0);
				if(specificDayPopup.getCbMon().getValue())
					specificDays.add(1);
				if(specificDayPopup.getCbTues().getValue())
					specificDays.add(2);
				if(specificDayPopup.getCbWed().getValue())
					specificDays.add(3);
				if(specificDayPopup.getCbThurs().getValue())
					specificDays.add(4);
				if(specificDayPopup.getCbFri().getValue())
					specificDays.add(5);
				if(specificDayPopup.getCbSat().getValue())
					specificDays.add(6);
				
				if(specificDays.size()==0)
				{
					form.showDialogMessage("Select at least one day of the week");
					return;
				}
				if(specificDays.size()>0) {
					replanservicepopup.specificDaysList=specificDays;
					Console.log("in specific days ok click. selected days="+specificDays.size());
				List<Service> servicelist = replanservicepopup.getServicetable().getDataprovider().getList();
				replanservicepopup.getP_interval().setValue(null);
				replanservicepopup.getWeek().setSelectedIndex(0);
//				replanservicepopup.getSpecificDay().setSelectedIndex(0);
				int selectedWeek = replanservicepopup.getWeek().getSelectedIndex();
				
				List<Service> updatedServicelist = replanservicepopup.getServiceSchedulelistNew(AppConstants.DAY, servicelist, null,specificDays,selectedWeek, -1);
				replanservicepopup.getServicetable().getDataprovider().setList(updatedServicelist);

				}
				specificDayPopup.clear();
				specificDayPanel.hide();
			}
			if(event.getSource()==specificDayPopup.getBtnCancel()){
				specificDayPopup.clear();
				specificDayPanel.hide();
			}
			if(event.getSource()==excludeDayPopup.getBtnOk()){
				
				ArrayList<Integer> excludeDays=new ArrayList<Integer>();
				if(excludeDayPopup.getCbSun().getValue())
					excludeDays.add(0);
				if(excludeDayPopup.getCbMon().getValue())
					excludeDays.add(1);
				if(excludeDayPopup.getCbTues().getValue())
					excludeDays.add(2);
				if(excludeDayPopup.getCbWed().getValue())
					excludeDays.add(3);
				if(excludeDayPopup.getCbThurs().getValue())
					excludeDays.add(4);
				if(excludeDayPopup.getCbFri().getValue())
					excludeDays.add(5);
				if(excludeDayPopup.getCbSat().getValue())
					excludeDays.add(6);
				
				if(excludeDays.size()==7)
				{
					form.showDialogMessage("You can not exclude all days of the week");
					return;
				}
				if(excludeDays.size()>0) {
					replanservicepopup.specificDaysList=null;
					Console.log("in exclude days ok click. selected days="+excludeDays.size());
				List<Service> servicelist = replanservicepopup.getServicetable().getDataprovider().getList();
				replanservicepopup.getP_interval().setValue(null);
				replanservicepopup.getWeek().setSelectedIndex(0);
//				replanservicepopup.getSpecificDay().setSelectedIndex(0);
				Console.log("setting specific list null");
				if(replanservicepopup.specificDaysList!=null&&replanservicepopup.specificDaysList.size()>0)			
					replanservicepopup.specificDaysList.clear();
				Console.log("calling getServiceSchedulelistNew");
				List<Service> updatedServicelist = replanservicepopup.getServiceSchedulelistNew(AppConstants.EXCLUDEDAY, servicelist, excludeDays, null, 0, -1);
				replanservicepopup.getServicetable().getDataprovider().setList(updatedServicelist);

				}
//				replanservicepopup.getSpecificDay().setSelectedIndex(0);
				if(replanservicepopup.specificDaysList!=null&&replanservicepopup.specificDaysList.size()>0)			
					replanservicepopup.specificDaysList.clear();
				excludeDayPopup.clear();
				excludeDayPanel.hide();
			}
			if(event.getSource()==excludeDayPopup.getBtnCancel()){
				excludeDayPopup.clear();
				excludeDayPanel.hide();
			}
		}	  
	  

	

	
	

	/**
	   * Updated By:Virajj
	   * Date: 29-06-2019
	   * Description: To change status for multiple entries in one go	  
	   */
	  private void setStatusToServices() {
		  final ArrayList<Service> selectedlist = getAllSelectedRecords();
			form.showWaitSymbol();
			final String status = serviceStatusPopup.getStatus().getValue(serviceStatusPopup.getStatus().getSelectedIndex());
			
			servicelistserviceAsync.setServiceStatusToService(selectedlist, "", status,UserConfiguration.getCompanyId(), new AsyncCallback<Integer>() {

				@Override
				public void onFailure(Throwable caught) {
					serviceStatusPopup.hidePopUp();
					form.hideWaitSymbol();
					
				}

				@Override
				public void onSuccess(Integer result) {
					if(result == selectedlist.size()){
						
						List<Service> list = form.getSuperTable().getDataprovider().getList();
						for(int i=0;i<selectedlist.size();i++){
							for(int j=0;j<list.size();j++){
								if(selectedlist.get(i).getCount() == list.get(j).getCount()){
									list.get(j).setStatus(status);
									list.get(j).setRecordSelect(false);

								}
							}
							
						}
						form.getSuperTable().getDataprovider().setList(list);
						serviceStatusPopup.hidePopUp();
						form.getSuperTable().getTable().redraw();
						
						
						form.showDialogMessage("Data updated sucessfully");
					}else{
						form.showDialogMessage("Data not updated.!");
					}
					form.hideWaitSymbol();
					
				}
				
			});
	}
	/** Ends **/
	  
	/**
	   * Updated By:Viraj
	   * Date: 15-06-2019
	   * Description: To assign branch in bulk
	   */	  
	  private void setBranchToServices() {
  		final ArrayList<Service> selectedlist = getAllSelectedRecords();
		form.showWaitSymbol();
		final String branch = serviceBranchPopup.getOlbBranch().getValue();
		
		servicelistserviceAsync.setSeviceBranchToService(selectedlist, "", branch,UserConfiguration.getCompanyId(), new AsyncCallback<Integer>() {

			@Override
			public void onFailure(Throwable caught) {
				serviceBranchPopup.hidePopUp();
				form.hideWaitSymbol();
				
			}

			@Override
			public void onSuccess(Integer result) {
				if(result == selectedlist.size()){
					
					List<Service> list = form.getSuperTable().getDataprovider().getList();
					for(int i=0;i<selectedlist.size();i++){
						for(int j=0;j<list.size();j++){
							if(selectedlist.get(i).getCount() == list.get(j).getCount()){
								list.get(j).setBranch(branch);
								list.get(j).setRecordSelect(false);

							}
						}
						
					}
					form.getSuperTable().getDataprovider().setList(list);
					serviceBranchPopup.hidePopUp();
					form.getSuperTable().getTable().redraw();
					
					
					form.showDialogMessage("Data updated sucessfully");
				}else{
					form.showDialogMessage("Data not updated.!");
				}
				form.hideWaitSymbol();
				
			}
			
		});
	  }
	  /** Ends **/
	  
	   /**
		 * Date 07-09-2018 By Vijay
		 * Developer :- Vijay
		 * Description :- Reschedule services with bulk
		 */
		  
		  public boolean validateReschedule()
			{
				Date rescheduleDate = reschedule.getDateBox().getValue();
				String reasonForChange=reschedule.getTextArea().getText().trim();
				
				if(rescheduleDate==null)
				{
					form.showDialogMessage("Reschedule Date Cannot be empty !");
					return false;
				}

				//Ashwini Patil Date:15-05-2023 Make ampm mandatory only if hour or minute selected. For orion
				if(reschedule.getP_servicehours().getSelectedIndex()!=0 || reschedule.getP_servicemin().getSelectedIndex()!=0||reschedule.getP_ampm().getSelectedIndex()!=0){
					
					if( reschedule.getP_servicehours().getSelectedIndex()==0){
						form.showDialogMessage("Please select hours !");
						return false;
					}
					if( reschedule.getP_servicemin().getSelectedIndex()==0){
						form.showDialogMessage("Please select minutes !");
						return false;
					}
					if( reschedule.getP_ampm().getSelectedIndex()==0){
						form.showDialogMessage("Please select AM/PM !");
						return false;
					}
				}
				
				//Ashwini Patil Date:23-05-2022 Adding one more validation for date and time as per Nitin sir's instruction
				if(AppUtility.formatDate(AppUtility.parseDate(rescheduleDate)).before(AppUtility.formatDate(AppUtility.parseDate(new Date())))){
						form.showDialogMessage("You are trying to schedule service in past..!");
					return false;
				}
				Console.log("date1="+AppUtility.formatDate(AppUtility.parseDate(rescheduleDate)));
				Console.log("date2="+AppUtility.formatDate(AppUtility.parseDate(new Date())));
				if(AppUtility.formatDate(AppUtility.parseDate(rescheduleDate)).equals(AppUtility.formatDate(AppUtility.parseDate(new Date())))){			
					   Console.log("scheduling today");
					   if(reschedule.getP_servicehours().getSelectedIndex()!=0&&reschedule.getP_servicemin().getSelectedIndex()!=0&&reschedule.getP_ampm().getSelectedIndex()!=0){
						   int index=reschedule.getP_servicehours().getSelectedIndex();	
						    int hour=Integer.parseInt(reschedule.getP_servicehours().getItemText(index));
						    if(reschedule.getP_ampm().getSelectedIndex()==1&&hour==12)
						    	hour=0;
						    if(reschedule.getP_ampm().getSelectedIndex()==2&&hour!=12)
						    	hour=hour+12;
						    index=reschedule.getP_servicemin().getSelectedIndex();
						    int min=Integer.parseInt(reschedule.getP_servicemin().getItemText(index));
						    Console.log("reschedule time="+hour+":"+min);
						    Date curDate=new Date();
						    Console.log("current time="+curDate.getHours()+":"+curDate.getMinutes());
						    if(hour<curDate.getHours()){
						    	form.showDialogMessage("You are trying to schedule service in past..!");
								return false;
						    }
						    if(curDate.getHours()==hour&&min<curDate.getMinutes()){
						    	form.showDialogMessage("You are trying to schedule service in past..!");
								return false;
						    }
					   }
					}
				/**
				 * nidhi
				 * 21-06-20118
				 * for reschedule service for same month validate
				 * Date :- 23-07-2018 By Vijay as discussed with Vaishali mam this validation not for Admin level
				 */
					boolean reScheduleValide  = AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "ServiceRescheduleWithInMonthOnly");
					
					if(reScheduleValide && !LoginPresenter.myUserEntity.getRole().getRoleName().equals("ADMIN")){
							Date serDate = reschedule.getDateBox().getValue();
							ArrayList<Service> selectedlist = getAllSelectedRecords();
							for(int i=0;i<selectedlist.size();i++){
								
								Date preFirstDate = CalendarUtil.copyDate(selectedlist.get(i).getServiceDate());
								Date preLastDate = CalendarUtil.copyDate(selectedlist.get(i).getServiceDate());
								
								CalendarUtil.addMonthsToDate(preLastDate, 1);
								
								preLastDate.setDate(1);
								CalendarUtil.addDaysToDate(preLastDate, -1);
								
								/**
								 * Date 10-Aug-2018 By Vijay
								 * Des :- Reschedule date only allow service date month only.
								 */
								Date firstDateServiceDate = CalendarUtil.copyDate(selectedlist.get(i).getServiceDate());
								CalendarUtil.setToFirstDayOfMonth(firstDateServiceDate);
								
								if(serDate.after(preLastDate) || serDate.before(firstDateServiceDate) ){
									form.showDialogMessage("Service date can be Reschedule With in Service date's month only.");
									return false;
								}
								
//								if(serDate.before(form.getTbServiceDate().getValue())){
//									form.showDialogMessage("Reschedule service date should be after current service date.");
//									return false;
//								}
								
								/**
								 * ends here and above old code commented because reschedule date allow for the same month date. 
								 */
							}
							
							
					}
				
				
				if(reasonForChange.equals(""))
				{
					form.showDialogMessage("Reason For Change Cannot be Empty !");
					return false;
				}
				
				return true;
			  
			
			}
		  
		  /**
		   * ends here
		   */
		  
		  
		public void cancelServices(final ArrayList<Service>list){
//			List<Service> servicelist = new ArrayList<Service>();
			
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "EnableCancellationRemark")){
				cancellationremark = cancellation.getOlbCancellationRemark().getValue();
			}else{
				cancellationremark = cancellationPopup.getRemark().getValue().trim();
			}
				
				
//			for(Service ser:list){
//				ser.setReasonForChange("Service ID ="+ser.getCount()+" "+"Service status ="+ser.getStatus().trim()+"\n"
//					+"has been cancelled by "+LoginPresenter.loggedInUser+" with remark "+"\n"
//					+"Remark ="+cancellationPopup.getRemark().getValue().trim()+" "+"Cancellation Date ="+AppUtility.parseDate(new Date()));
//				ser.setStatus(Service.SERVICESTATUSCANCELLED);
//				ser.setRecordSelect(false);
//				/***** Date 26-03-2019 by vijay for NBHC CCPM remark must store in remark filed only and historical data as it is in resonforchange **/
//				
//				/** Date 27-07-2019 by Vijay Cancellation Date set seperately *****/
//				ser.setCancellationDate(new Date());
//				servicelist.add(ser);
//			}
			form.showWaitSymbol();
			updateService.cancelServices(list,cancellationremark , LoginPresenter.loggedInUser, new AsyncCallback<String>() {
				@Override
				public void onFailure(Throwable caught) {
					form.hideWaitSymbol();
					form.showDialogMessage("An Unexpected Error occured !");
					Console.log("Inside On fail");
				}
				@Override
				public void onSuccess(String result) {
					/**Date 3-4-2020 by Amol added this for to update the status of service to cancel**/
					List<Service> allServlist = form.getSuperTable().getDataprovider().getList();
					for(int i=0;i<list.size();i++){
						for(int j=0;j<allServlist.size();j++){
							if(list.get(i).getCount() == allServlist.get(j).getCount()){
								allServlist.get(j).setStatus(Service.SERVICESTATUSCANCELLED);
								allServlist.get(j).setRecordSelect(false);
								allServlist.get(j).setRemark(cancellationremark);
							}
						}
					}
					
					form.hideWaitSymbol();
					form.showDialogMessage(result);
					form.getSuperTable().getTable().redraw();
					Console.log("Inside On Success");
					
					
					
					
				}
			});
		}
		 /** date 26.02.2018 added by komal for schedule print **/
		private boolean validateSchedulePrintPopup() {
			if(!AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "DateTechnicianWiseServicePopup")){
				/** date 18.3.2019 added by komal to make technician non mandatoty for nbhc schedule print **/
			if(serviceSchedulePrintPopup.getOlbTeam().getSelectedIndex()==0 && serviceSchedulePrintPopup.getOlbEmployee().getSelectedIndex()==0){
				form.showDialogMessage("Please Select Team or Technician");
				return false;
			}
			if(serviceSchedulePrintPopup.getOlbTeam().getSelectedIndex()!=0 && serviceSchedulePrintPopup.getOlbEmployee().getSelectedIndex()!=0){
				form.showDialogMessage("Please Select Either Team Or Technician");
				return false;
			}
			}
			if(serviceSchedulePrintPopup.getDbFromDate().getValue()==null && serviceSchedulePrintPopup.getDbTodate().getValue()==null){
				form.showDialogMessage("Please Select Form Date and To Date");
				return false;
			}
			
			return true;
		}
 
		public void setTechnicianToServices(){
			final ArrayList<Service> selectedlist = getAllSelectedRecords();
			form.showWaitSymbol();
			final String technician = serviceTechnicianPopup.getOlbEmployee().getValue();
			servicelistserviceAsync.setServiceTechnicianToServices(selectedlist, "", technician,UserConfiguration.getCompanyId(), new AsyncCallback<Integer>() {
				
				@Override
				public void onSuccess(Integer result) {
					if(result == selectedlist.size()){
						
						List<Service> list = form.getSuperTable().getDataprovider().getList();
						for(int i=0;i<selectedlist.size();i++){
							for(int j=0;j<list.size();j++){
								if(selectedlist.get(i).getCount() == list.get(j).getCount()){
									list.get(j).setEmployee(technician);
									/*** Date 27-11-2018 By Vijay for selected records set false for untick **/
									list.get(j).setRecordSelect(false);

								}
							}
							
						}
//						form.showDialogMessage("Services Mark Completed successfully!");
						form.getSuperTable().getDataprovider().setList(list);
						serviceTechnicianPopup.hidePopUp();
						form.getSuperTable().getTable().redraw();
						
						
						form.showDialogMessage("Data updated sucessfully");
					}else{
						form.showDialogMessage("Data not updated.!");
					}
					form.hideWaitSymbol();
				}
				@Override
				public void onFailure(Throwable caught) {
					serviceTechnicianPopup.hidePopUp();
					form.hideWaitSymbol();
				}
			});
		}
		/**
		 * Description : This is used to save changed service time , technician into service
		 * @param serviceList
		 */
		public void saveServiceDetails(final List<Service> serviceList){
			ArrayList<Service> list = new ArrayList<Service>();
						if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","ServiceStatusAsPlanned")){
							for(Service s : serviceList) {
								s.setStatus(Service.SERVICESTATUSPLANNED);
								list.add(s);
							}
						}else{
							list.addAll(serviceList);
						}
			async.saveSelectedService(list,UserConfiguration.getCompanyId() , new AsyncCallback<String>() {

				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					Console.log("Save Services");
					GWTCAlert alert = new GWTCAlert();
					multipleServicesSchedulePopup.getLblOk().setText("Save Services");
					alert.alert("Saved successfully.");
				}

				@Override
				public void onSuccess(String result) {
					// TODO Auto-generated method stub
					GWTCAlert alert = new GWTCAlert();
					multipleServicesSchedulePopup.getLblOk().setText("Save Services");
					
					List<Service> list = form.getSuperTable().getDataprovider().getList();
					for(int i=0;i<serviceList.size();i++){
						for(int j=0;j<list.size();j++){
							if(serviceList.get(i).getCount() == list.get(j).getCount()){
								list.get(j).setEmployee(serviceList.get(i).getEmployee());
								list.get(j).setRecordSelect(false);
								if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","ServiceStatusAsPlanned")){
									list.get(j).setStatus(Service.SERVICESTATUSPLANNED);
								}
							}
						}
						
					}
//					
					form.getSuperTable().getDataprovider().setList(list);
					
					form.getSuperTable().getTable().redraw();
					alert.alert("Saved successfully.");
					multipleServicesSchedulePopup.tabPanel.selectTab(1);//Ashwini Patil Date:3-11-2022
					
				}
			});
		}
		/** 
		 * This method is used to schedule service and create mmn**/
		 
		public void scheduleServices(){
			
			Console.log("schedule service.");
			final List<Service> serviceList = multipleServicesSchedulePopup.getMultipleServicesScheduleTable().getDataprovider().getList();
			Console.log("schedule service size :"+ serviceList.size());
			ArrayList<Integer> serIdList = new ArrayList<Integer>();
			for(Service ser :serviceList){
				serIdList.add(ser.getCount());
			}
			
			Console.log("schedule service id size :"+ serIdList.size());
			
			multipleServicesSchedulePopup.getCustomerServiceMaterialTable().getDataprovider().getList();
			ArrayList<ProductGroupList> list = new ArrayList<ProductGroupList>();
			list.addAll(multipleServicesSchedulePopup.getCustomerServiceMaterialTable().getDataprovider().getList());
			async.createMMNForTechnician(UserConfiguration.getCompanyId(), list, LoginPresenter.loggedInUser,serIdList, new AsyncCallback<String>() {

				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					System.out.print("error"  +caught);
					GWTCAlert alert = new GWTCAlert();
					multipleServicesSchedulePopup.getLblOk().setText("Schedule (Assign to technician)");
					alert.alert("Falied.");
				}

				@Override
				public void onSuccess(String result) {
					// TODO Auto-generated method stub
					GWTCAlert alert = new GWTCAlert();
					multipleServicesSchedulePopup.getLblOk().setText("Schedule (Assign to technician)");
					alert.alert("Scheduled successfully.");
					multipleServicesSchedulePopup.hidePopUp();

					
					List<Service> list = form.getSuperTable().getDataprovider().getList();
					for(int i=0;i<serviceList.size();i++){
						for(int j=0;j<list.size();j++){
							if(serviceList.get(i).getCount() == list.get(j).getCount()){
								list.get(j).setEmployee(serviceList.get(i).getEmployee());
								list.get(j).setRecordSelect(false);
								list.get(j).setServiceScheduled(true);
								if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","ServiceStatusAsPlanned")){
									list.get(j).setStatus(Service.SERVICESTATUSSCHEDULE);
								}
								
							}
						}
						
					}
//					
					form.getSuperTable().getDataprovider().setList(list);
					
					form.getSuperTable().getTable().redraw();
					
					
					//form.showDialogMessage("Data updated sucessfully");
				
				}
			});
			
		}
		
		public void validateMaterial(){
			boolean flag = AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "TechnicianwiseMaterialPopup");
			final ArrayList<Service> serviceList = new ArrayList<Service>();
			serviceList.addAll(multipleServicesSchedulePopup.getMultipleServicesScheduleTable().getDataprovider().getList());
			final ArrayList<ProductGroupList> summaryList = new ArrayList<ProductGroupList>();
			summaryList.addAll(multipleServicesSchedulePopup.getCustomerServiceMaterialTable().getDataprovider().getList());
			for(Service service : serviceList){
				if(service.getEmployee() == null || service.getEmployee().equalsIgnoreCase("")){
					form.showDialogMessage("Please assign technician to service id : "+  service.getCount());
					 return;
				}
			}
			
			/**
			 * @author Vijay Chougule Date 15-09-2020
			 * Des :- As per Nitin Sir when scheduling multiple services with different technician then giving msg info
			 */
			if(summaryList.size()>1){
				String technicianName = summaryList.get(0).getCreatedBy();
				Console.log("technicianName "+technicianName);
				for(ProductGroupList productgroup : summaryList){
					Console.log("Table Technician Name"+productgroup.getCreatedBy());
					if(!technicianName.equals(productgroup.getCreatedBy())){
						form.showDialogMessage("When you schedule multiple services with different technician then it may take time to schedule the services");
					}
				}
			}
			/**
			 * ends here
			 */
			if(flag && summaryList.size() > 0){
//			async.validateTechnicianWarehouse(serviceList, summaryList, new AsyncCallback<ArrayList<String>>() {
//
//				@Override
//				public void onFailure(Throwable caught) {
//					// TODO Auto-generated method stub
//					Console.log("Error 1: "+ caught);
//					multipleServicesSchedulePopup.getLblOk().setText("Schedule");
//				}
//
//				@Override
//				public void onSuccess(ArrayList<String> result) {
//					// TODO Auto-generated method stub
//					Console.log("result : "+ result);
//					if(result != null && result.size() > 0){
//						form.showDialogMessage(result.toString());
//						multipleServicesSchedulePopup.getLblOk().setText("Schedule");
//					}else{
/** Description : This is used to check whether the parent warehouse contains sufficient quantity to transfer
 * into technician bin
 */
						async.validateTechnicianWarehouseQuantity(UserConfiguration.getCompanyId(), serviceList,  summaryList, new AsyncCallback<ArrayList<String>>() {

							@Override
							public void onFailure(Throwable caught) {
								// TODO Auto-generated method stub
								Console.log("Error 2: "+ caught);
								multipleServicesSchedulePopup.getLblOk().setText("Schedule (Assign to technician)");
							}

							@Override
							public void onSuccess(ArrayList<String> result1) {
								// TODO Auto-generated method stub
								if(result1 != null && result1.size() >0){
									form.showDialogMessage(result1.toString());
									multipleServicesSchedulePopup.getLblOk().setText("Schedule (Assign to technician)");
								}else{
									scheduleServices();
								}
							}
						});
						
		//			}
		//		}
		//	});
			}else{
				
				if(summaryList.size()>0){
					commonserviceasync.validateMaterialQuantity(model.getCompanyId(), summaryList, new AsyncCallback<String>() {

						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							multipleServicesSchedulePopup.getLblOk().setText("Schedule (Assign to technician)");

						}

						@Override
						public void onSuccess(String result) {
							// TODO Auto-generated method stub
							Console.log("result "+result);
							if(!result.equals("")){
								Console.log("resulttt");
								form.showDialogMessage(result);
								multipleServicesSchedulePopup.getLblOk().setText("Schedule (Assign to technician)");
							}
							else{
								Console.log("else block");
								scheduleServices();
							}
						}
					});
				}
				else{
					scheduleServices();
				}
//				scheduleServices();
			}

			
		}
		
		public void updateComplaint(ArrayList<Service> selectedlist){
			 Console.log("Rescheduled");
			 if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Complain", "ComplainServiceWithTurnAroundTime")){
				 Console.log("updateComplainDocument");
				 servicelistserviceAsync.updateComplainDocument(selectedlist, true, false, false, false, new AsyncCallback<String>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						 Console.log("onFailure"+caught.getMessage());
					}

					@Override
					public void onSuccess(String result) {
						// TODO Auto-generated method stub
						 Console.log("onSuccess");
					}
				});
			 }
		}
		
		private void reactOnViewComplain() {
			ArrayList<Service> selectedlist = getAllSelectedRecords();
			
			if(selectedlist.size()==0){
				form.showDialogMessage("Please select anyone record!");
				return;
			}else if(selectedlist.size()>1){
				form.showDialogMessage("Please select only one record!");
				return;
			}
			
			Service serviceObj=selectedlist.get(0);
			if(serviceObj.getTicketNumber()==null||serviceObj.getTicketNumber()==0){
				form.showDialogMessage("Please select complain service only!");
				return;
			}
			

			final MyQuerry querry=new MyQuerry();
			Vector<Filter> temp=new Vector<Filter>();
			Filter filter=null;
			
			filter=new Filter();
			filter.setQuerryString("count");
			filter.setIntValue(serviceObj.getTicketNumber());
			temp.add(filter);
			
			querry.setFilters(temp);
			querry.setQuerryObject(new Complain());
			form.showWaitSymbol();
			service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				@Override
				public void onFailure(Throwable caught) {
					form.hideWaitSymbol();
				}

				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					form.hideWaitSymbol();
					if(result.size()==0){
						form.showDialogMessage("No complain document found.");
						return;
					}
					if(result.size()==1){
						AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Customer Support",Screen.COMPLAIN);
						final Complain complaiin=(Complain) result.get(0);
						final ComplainForm form=ComplainPresenter.initalize();
						Timer timer=new Timer() {
							@Override
							public void run() {
								form.updateView(complaiin);
								form.setToViewState();
							}
						};
						timer.schedule(3000);
					}
				}
			});
		}

		private void reactonViewDocument(ArrayList<Service> selectedList) {
			AppMemory.getAppMemory().currentState = ScreeenState.VIEW;
			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Customer Service", Screen.DEVICE);
			DeviceForm.flag = false;
			final Service serviceEntity = selectedList.get(0);
			final DeviceForm form = DevicePresenter.initalize();
			form.showWaitSymbol();
			AppMemory.getAppMemory().stickPnel(form);
			Timer timer = new Timer() {
				@Override
				public void run() {
					form.hideWaitSymbol();
					form.updateView(serviceEntity);
					form.setToViewState();
				}
			};
			timer.schedule(3000);	
		}
		
		
		private void setEmailPopUpData(Service service) {
			// TODO Auto-generated method stub
			if(!service.getServiceBranch().equalsIgnoreCase("Service Address")){
				getCustomerBranchDetails(service.getServiceBranch(),service,"Email");
			}
			else{
				setEmailAllData(null,service);
			}
		}
		
		private void getCustomerBranchDetails(String serviceBranch, final Service service, final String popupType) {

			MyQuerry querry=new MyQuerry();
			Company c=new Company();
			Vector<Filter> filtervec=new Vector<Filter>();
			Filter temp=null;
			
			temp=new Filter();
			temp.setQuerryString("companyId");
			temp.setLongValue(c.getCompanyId());
			filtervec.add(temp);
			
			temp=new Filter();
			temp.setQuerryString("cinfo.count");
			temp.setIntValue(model.getPersonInfo().getCount());
			filtervec.add(temp);
			
			temp=new Filter();
			temp.setQuerryString("status");
			temp.setBooleanvalue(true);
			filtervec.add(temp);
			
			temp=new Filter();
			temp.setQuerryString("buisnessUnitName");
			temp.setStringValue(serviceBranch);
			filtervec.add(temp);

			querry.setFilters(filtervec);
			querry.setQuerryObject(new CustomerBranchDetails());
			
			genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					ArrayList<ContactPersonIdentification> contactpersonlist = new ArrayList<ContactPersonIdentification>();
					for(SuperModel model : result){
						CustomerBranchDetails customerBranch = (CustomerBranchDetails) model;
						if(popupType.trim().equals("Email")){
							if(customerBranch.getEmail()!=null && !customerBranch.getEmail().equals("")){
								ContactPersonIdentification contactperson = new ContactPersonIdentification();
								contactperson.setName(customerBranch.getBusinessUnitName());
								contactperson.setEmail(customerBranch.getEmail());
								contactpersonlist.add(contactperson);
								if(emailpopup!=null && customerBranch.getEmail()!=null)
									emailpopup.taToEmailId.setValue(customerBranch.getEmail());

							}
						}
						else{
							if(customerBranch.getCellNumber1()!=0){
								ContactPersonIdentification contactperson = new ContactPersonIdentification();
								contactperson.setName(customerBranch.getBusinessUnitName());
								contactpersonlist.add(contactperson);
							}
						}
						
					}
					if(popupType.trim().equals("Email")){
						setEmailAllData(contactpersonlist,service);

					}
					else{
						System.out.println("contactpersonlist "+contactpersonlist.size());
						loadSMSPopUpData(contactpersonlist,service);
					}
				}
				
				@Override
				public void onFailure(Throwable caught) {
					
				}
			});
		}
		
		public void setEmailAllData(final ArrayList<ContactPersonIdentification> contactpersonlist, final Service service){
			
			form.showWaitSymbol();
			String customerEmail = "";

			String branchName = service.getBranch();
			String fromEmailId = AppUtility.getFromEmailAddress(branchName,service.getEmployee());
			customerEmail = AppUtility.getCustomerEmailid(service.getPersonInfo().getCount());
			
			String customerName = service.getPersonInfo().getFullName();
			if(!customerName.equals("") && !customerEmail.equals("")){
				label : AppUtility.loadContactPersonlist(emailpopup.olbContactlistEmailId,"Customer",service.getPersonInfo().getCount(),customerName,customerEmail,contactpersonlist);
				emailpopup.taToEmailId.setValue(customerEmail);
			}
			else{
		
				MyQuerry querry = AppUtility.getcustomerQuerry(service.getPersonInfo().getCount());
				genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
					
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						// TODO Auto-generated method stub
						for(SuperModel smodel : result){
							Customer custEntity  = (Customer) smodel;
							System.out.println("customer Name = =="+custEntity.getFullname());
							System.out.println("Customer Email = == "+custEntity.getEmail());
							
							if(custEntity.getEmail()!=null){
								label : AppUtility.loadContactPersonlist(emailpopup.olbContactlistEmailId,"Customer",service.getPersonInfo().getCount(),custEntity.getFullname(),custEntity.getEmail(),contactpersonlist);
								emailpopup.taToEmailId.setValue(custEntity.getEmail());
							}
							else{
								label : AppUtility.loadContactPersonlist(emailpopup.olbContactlistEmailId,"Customer",service.getPersonInfo().getCount(),custEntity.getFullname(),custEntity.getEmail(),contactpersonlist);
							}
							break;
						}
					}
					
					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
					}
				});
			}
			
			Screen screenName=(Screen) AppMemory.getAppMemory().currentScreen;
			Console.log("screenName "+screenName);
			AppUtility.makeLiveSmsTemplateConfig(emailpopup.oblTemplateName,AppConstants.SERVICE,screenName);
			emailpopup.taFromEmailId.setValue(fromEmailId);
			emailpopup.smodel = service;
			form.hideWaitSymbol();
			
		}
		
		@Override
		public void reactOnSMS() {
			
			ArrayList<Service> selectedList = getAllSelectedRecords();
			if(selectedList.size()==0){
				form.showDialogMessage("Please select record!");
				return;
			}
			if(selectedList.size()>=2){
				form.showDialogMessage("Please select only one record!");
				return;
			}
			
			smspopup.showPopUp();
			
			if(!selectedList.get(0).getServiceBranch().equalsIgnoreCase("Service Address")){
				getCustomerBranchDetails(model.getServiceBranch(),selectedList.get(0), "SMS");
			}
			else{
				loadSMSPopUpData(null,selectedList.get(0));

			}
			
		}

		private void loadSMSPopUpData(ArrayList<ContactPersonIdentification> contactpersonlist, Service service) {

			form.showWaitSymbol();
			String customerCellNo = service.getPersonInfo().getCellNumber()+"";
			System.out.println("contactpersonlist = "+contactpersonlist.size());
			String customerName = service.getPersonInfo().getFullName();
			if(!customerName.equals("") && !customerCellNo.equals("")){
				AppUtility.loadContactPersonlist(smspopup.olbContactlistEmailId,"Customer",service.getPersonInfo().getCount(),customerName,customerCellNo,contactpersonlist,true);
			}
			Screen screenName=(Screen) AppMemory.getAppMemory().currentScreen;
			Console.log("screenName "+screenName);
			AppUtility.makeLiveSmsTemplateConfig(smspopup.oblsmsTemplate,AppConstants.SERVICE,screenName,true,AppConstants.SMS);
			smspopup.getRbSMS().setValue(true);
			smspopup.smodel = service;
			form.hideWaitSymbol();
		
		}
		
		private void reactOnReplanServices() {
			replanservicepopup.getServicetable().connectToLocal();
			final ArrayList<Service> selectedlist = getAllSelectedRecords();
			if(selectedlist.size()==0){
				form.showDialogMessage("Please select records!");
				return;
			}
			
			HashSet<Integer> hscontractId = new HashSet<Integer>();
			
			for(int i=0;i<selectedlist.size();i++){
				if(selectedlist.get(i).getStatus().equalsIgnoreCase("Completed") 
						|| selectedlist.get(i).getStatus().equalsIgnoreCase(Service.CANCELLED)
						|| selectedlist.get(i).getStatus().equalsIgnoreCase(Service.SERVICESUSPENDED)){
					form.showDialogMessage("Please select Scheduled and Rescheduled Services only!");
					return;
				}
				hscontractId.add(selectedlist.get(i).getContractCount());
			}
			
			if(hscontractId.size()>1){
				form.showDialogMessage("Please select only one contract services!");
				return;
			}
			
			Comparator<Service> comp=new Comparator<Service>() {
				@Override
				public int compare(Service e1, Service e2) {
					return e1.getServiceDate().compareTo(e2.getServiceDate());
				}
			};
			Collections.sort(selectedlist,comp);
			
		
			int noofservices = selectedlist.size();
			int  duration = AppUtility.getDifferenceDays(new Date(), selectedlist.get(0).getContractEndDate());
			int interval = duration/noofservices;
			
//			List<Service> updatedServicelist = replanservicepopup.getServiceSchedulelistNew(AppConstants.INTERNAL, selectedlist,0, 0, 0, interval);
			
			List<Service> updatedServicelist = replanservicepopup.getServiceSchedulelistNew(AppConstants.INTERNAL, selectedlist,null, null, 0, interval);
			
			replanservicepopup.showPopUp();
			replanservicepopup.getServicetable().clear();
			replanservicepopup.getServicetable().getDataprovider().setList(updatedServicelist);
//			replanservicepopup.getPopup().setHeight("580px");
			replanservicepopup.getPopup().setHeight("622px");
			replanservicepopup.getPopup().setWidth("800px");
			
			replanservicepopup.getDbContractEndDate().setValue(selectedlist.get(0).getContractEndDate());
			replanservicepopup.getDbCurrentDate().setValue(new Date());
			
			
		}
		
		private void reactonReplanServicesOK() {
			form.showWaitSymbol();
			List<Service> servicelist = replanservicepopup.getServicetable().getDataprovider().getList();
			
			List<Service> list = form.getSuperTable().getDataprovider().getList();

			for(int i=0;i<servicelist.size();i++){
				for(int j=0;j<list.size();j++){
					if(servicelist.get(i).getCount() == list.get(j).getCount()){
						ModificationHistory history=new ModificationHistory();
						history.setOldServiceDate(list.get(j).getServiceDate());
						history.setResheduleDate(servicelist.get(i).getServiceDate());
						history.setUserName(LoginPresenter.loggedInUser);
						history.setSystemDate(new Date());
						servicelist.get(i).getListHistory().add(history);
					}
					
				}
                
			}
			
			ArrayList<SuperModel> spservicelist=new ArrayList<SuperModel>();
			spservicelist.addAll(servicelist);

			genasync.save(spservicelist, new AsyncCallback<ArrayList<ReturnFromServer>>() {

				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void onSuccess(ArrayList<ReturnFromServer> result) {

					List<Service> servicelist = replanservicepopup.getServicetable().getDataprovider().getList();

					List<Service> list = form.getSuperTable().getDataprovider().getList();
					for(int i=0;i<servicelist.size();i++){
						for(int j=0;j<list.size();j++){
							if(servicelist.get(i).getCount() == list.get(j).getCount()){
								list.get(j).setServiceDate(servicelist.get(i).getServiceDate());
								list.get(j).setServiceDay(servicelist.get(i).getServiceDay());
								list.get(j).setServiceWeekNo(servicelist.get(i).getServiceWeekNo());
							}
						}
						
					}
						form.showDialogMessage("Services updated successfully!");
						replanservicepopup.hidePopUp();
						form.getSuperTable().getDataprovider().setList(list);
						form.hideWaitSymbol();
						form.getSuperTable().getTable().redraw();
						
				}
			});
		}
		
		
		@Override
		public void reactOnCommunicationLog() {
			
			List<Service> serviceList = getAllSelectedRecords();

			if(serviceList.size()==0){
				form.showDialogMessage("Please select record !");
			}else if(serviceList.size()>1){
				form.showDialogMessage("Please select only one record !");
			}else{
				
				form.showWaitSymbol();
				MyQuerry querry = AppUtility.communicationLogQuerry(serviceList.get(0).getCount(), serviceList.get(0).getCompanyId(),AppConstants.SERVICEMODULE,AppConstants.SERVICE);

				genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
					
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						
						ArrayList<InteractionType> list = new ArrayList<InteractionType>();
						
						for(SuperModel model : result){
							
							InteractionType interactionType = (InteractionType) model;
							list.add(interactionType);
						}
						form.hideWaitSymbol();
						Comparator<InteractionType> comp=new Comparator<InteractionType>() {
							@Override
							public int compare(InteractionType e1, InteractionType e2) {
								Integer ee1=e1.getCount();
								Integer ee2=e2.getCount();
								return ee2.compareTo(ee1);
								
							}
						};
						Collections.sort(list,comp);
						/**
						 * End
						 */
						CommunicationLogPopUp.getRemark().setValue("");
						CommunicationLogPopUp.getDueDate().setValue(null);
						CommunicationLogPopUp.getOblinteractionGroup().setSelectedIndex(0);
						CommunicationLogPopUp.getCommunicationLogTable().getDataprovider().setList(list);
						
						LoginPresenter.communicationLogPanel = new PopupPanel(true);
						LoginPresenter.communicationLogPanel.add(CommunicationLogPopUp);
						LoginPresenter.communicationLogPanel.show();
						LoginPresenter.communicationLogPanel.center();
					}
					
					@Override
					public void onFailure(Throwable caught) {
						form.hideWaitSymbol();
					}
				});
				
			
			}
			
		}
		
			//Ashwini Patil
		public boolean validateRescheduleDate(Date rescheduleDate,String serviceTime) {
			if(AppUtility.formatDate(AppUtility.parseDate(rescheduleDate)).before(AppUtility.formatDate(AppUtility.parseDate(new Date())))){
				return false;
			}
			if(AppUtility.formatDate(AppUtility.parseDate(rescheduleDate)).equals(AppUtility.formatDate(AppUtility.parseDate(new Date())))){			
				   Console.log("scheduling today serviceTime="+serviceTime+" "+serviceTime.length());
				   String hour=serviceTime.substring(0,1);
				   String min=serviceTime.substring(3,4);
				   String ampm=serviceTime.substring(5);
				   Console.log("hour="+hour+" min="+min+" ampm="+ampm);
				}
			return true;
		}
		
		//Ashwini Patil Date:16-11-2022
		public void scheduleSingleService(Service object){
			final GWTCAlert alert = new GWTCAlert();
			final List<Service> serviceList =new ArrayList<Service>();
			serviceList.add(object);
			
			boolean complaintServiceflag = false;
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "EnableValidateAssesmentReport")){
				complaintServiceflag = true;
			}
			ArrayList<Integer> complaintServiceId = new ArrayList<Integer>();
			
			//Ashwini Patil commented this on 1-03-2023
//			if(object.getStatus().equals(Service.SERVICESTATUSCANCELLED)
//						||object.getStatus().equals(Service.SERVICESTATUSCLOSED)
//						||object.getStatus().equals(Service.SERVICESTATUSCOMPLETED)
//						||object.getStatus().equals(Service.SERVICESTATUSREPORTED)
//						||object.getStatus().equals(Service.SERVICESTATUSPENDING)
//						||object.getStatus().equals(Service.SERVICESTATUSTECHCOMPLETED)
//						||object.getStatus().equals(Service.SERVICESUSPENDED)){
//		
//					alert.alert("Service status should be Scheduled/Rescheduled!");
//					return ;
//				}
				
				
			if(complaintServiceflag && object.getTicketNumber()!=-1){
					complaintServiceId.add(object.getCount());
			}				
			
			if(complaintServiceId.size()!=0){
//				CommunicationLogServiceAsync validateComplaintService = GWT.create(CommunicationLogService.class);	

				validateComplaintService.ValidateAssessmentForComplaintService(complaintServiceId, object.getCompanyId(), new AsyncCallback<String>() {
					
					@Override
					public void onSuccess(String result) {
						// TODO Auto-generated method stub
						if(result.equals("success")){
							if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "ENABLECOMPLETEBUTTONONSCHEDULEPOPUP")){
								lblComplete.getElement().setId("addbutton");	
								multipleServicesSchedulePopup.getHorizontal().insert(lblComplete,2);					
							}
//							MultipleServicesSchedulePopup multipleServicesSchedulePopup = new MultipleServicesSchedulePopup();
							multipleServicesSchedulePopup.showPopUp();
							multipleServicesSchedulePopup.getPopup().setHeight("580px");
							multipleServicesSchedulePopup.getPopup().setWidth("800px");
							multipleServicesSchedulePopup.getTabPanel().selectTab(0);
							multipleServicesSchedulePopup.getMultipleServicesScheduleTable().getDataprovider().setList(serviceList);
							multipleServicesSchedulePopup.setServiceList(serviceList);
						}
						else{
							alert.alert(result);
						}
						
					}
					
					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						
						alert.alert(caught.getMessage());
					}
				});
			}
			else{
//				MultipleServicesSchedulePopup multipleServicesSchedulePopup = new MultipleServicesSchedulePopup();
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "ENABLECOMPLETEBUTTONONSCHEDULEPOPUP")){
					lblComplete.getElement().setId("addbutton");	
					multipleServicesSchedulePopup.getHorizontal().insert(lblComplete,2);					
				}
				multipleServicesSchedulePopup.showPopUp();
				multipleServicesSchedulePopup.getPopup().setHeight("580px");
				multipleServicesSchedulePopup.getPopup().setWidth("800px");
				multipleServicesSchedulePopup.getTabPanel().selectTab(0);
				multipleServicesSchedulePopup.getMultipleServicesScheduleTable().getDataprovider().setList(serviceList);
				multipleServicesSchedulePopup.setServiceList(serviceList);
			}
		}
		
		
		private void reactonPrintsceduleOnEmail() {
			
			if(srprintschedulepopup.getDbFromDate().getValue()==null){
				form.showDialogMessage("Please add from date");
				return;
			}
			if(srprintschedulepopup.getDbTodate().getValue()==null){
				form.showDialogMessage("Please add to date");
				return;
			}
			if(srprintschedulepopup.getOlbEmployee().getSelectedIndex()==0){
				form.showDialogMessage("Please select technician");
				return;
			}
			
			Employee emp = null;
			String technicianName="";
			if(srprintschedulepopup.getOlbEmployee().getSelectedIndex() != 0){
				technicianName = srprintschedulepopup.getOlbEmployee().getValue(srprintschedulepopup.getOlbEmployee().getSelectedIndex());
			}
		
			String team="";
//			if(srprintschedulepopup.getOlbTeam().getSelectedIndex()!=0)
//				team=srprintschedulepopup.getOlbTeam().getValue(srprintschedulepopup.getOlbTeam().getSelectedIndex());
		
			ArrayList<String> toemaillist = new ArrayList<String>();
			
			List<EmployeeBranch> list = srprintschedulepopup.getEmailTable().getDataprovider().getList();
			for(EmployeeBranch empbranch : list){
				toemaillist.add(empbranch.getBranchName());
			}
			
			Company comp = new Company();
			
			commonserviceasync.sendSRFormatVersion1CopyOnEmail(srprintschedulepopup.getDbFromDate().getValue(),
					srprintschedulepopup.getDbTodate().getValue(), team, technicianName, toemaillist,comp.getCompanyId(), new AsyncCallback<String>() {

						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							
						}

						@Override
						public void onSuccess(String result) {
							// TODO Auto-generated method stub
							form.showDialogMessage(result);
							srprintschedulepopup.hidePopUp();
						}
					});
			
		}
		
		
		private void reactonprintSrCopy() {
			
			
			if(srprintschedulepopup.getDbFromDate().getValue()==null){
				form.showDialogMessage("Please add from date");
				return;
			}
			if(srprintschedulepopup.getDbTodate().getValue()==null){
				form.showDialogMessage("Please add to date");
				return;
			}
			if(srprintschedulepopup.getOlbEmployee().getSelectedIndex()==0){
				form.showDialogMessage("Please select technician");
				return;
			}
			
			String technicianName="";
			if(srprintschedulepopup.getOlbEmployee().getSelectedIndex() != 0){
				technicianName = srprintschedulepopup.getOlbEmployee().getValue(srprintschedulepopup.getOlbEmployee().getSelectedIndex());
			}
			
			String frmDate = "";
			frmDate = AppUtility.parseDate(srprintschedulepopup.getDbFromDate().getValue());
			
			String toDate = "";
			toDate = AppUtility.parseDate(srprintschedulepopup.getDbFromDate().getValue());
			
			Company comp = new Company();

			
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "CompanyAsLetterHead")){
				srprint = srprint + 1;
				Console.log("inside CompanyAsLetterHead");
				if(srprint>0){
		    		panel2=new PopupPanel(true);
		    		panel2.add(conditionPopup2);
		    		panel2.setGlassEnabled(true);
		    		panel2.show();
		    		panel2.center();
		             Console.log("inside print reminder");
		    	}
				
			}
			else{
				final String url = GWT.getModuleBaseURL()+"srformateversion1"+"?"+
						"&"+"technicianName="+technicianName+"&"+"fromDate="+frmDate+"&"+"toDate="+toDate+"&"+"companyId="+comp.getCompanyId()+""+ "&"+ "preprint=" + "plane";
				Window.open(url, "test", "enabled");
			}
			
			
			
		}
		
		
		private void runRegularDownloadProgram() {
			// TODO Auto-generated method stub
			final CsvServiceAsync async=GWT.create(CsvService.class);
			ArrayList<Service> custarray=new ArrayList<Service>();
			List<Service> list=form.getSuperTable().getListDataProvider().getList();
			
			custarray.addAll(list); 
			/*** Date 08-05-2019 by Vijay as discussed with Nitin Sir I have commented this code ****/
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "ServiceBillingDetailMapingProcess")){
				mapService.mapBillingDetails(custarray, new AsyncCallback<ArrayList<Service>>() {
					
					@Override
					public void onSuccess(ArrayList<Service> result) {
						// TODO Auto-generated method stub
						
						async.setservice(result, new AsyncCallback<Void>() {

							@Override
							public void onFailure(Throwable caught) {
								System.out.println("RPC call Failed"+caught);
								
							}

							@Override
							public void onSuccess(Void result) {
								
								/**
								 * nidhi
								 * 16-4-2018
								 * for service list custom report download using process configration
								 * 
								 */
								
								if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "ServiceReport")){
									String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
									final String url = gwt + "csvservlet" + "?type=" + 137;
									Window.open(url, "test", "enabled");
								}else{
									String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
									final String url=gwt + "csvservlet"+"?type="+5;
									Window.open(url, "test", "enabled");
								}
							}
						});
					}
					
					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						
					}
				});
			}else{
			
			
			
				
//			int columnCount = 44; //Ashwini Patil Date:10-03-2023 changed 43 to 44
//			/**
//			 * nidhi
//			 */
//			Long companyId = (long) 0 ;
//			
//			if(list.size()>0)
//				companyId = list.get(0).getCompanyId();
//			
//			boolean complainSeviceTatFlag = false;
//			boolean serviceDurationFlag = false;
//			boolean showHideSerialNo = false;
//			
//			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "ComplainServiceWithTurnAroundTime")){
//				complainSeviceTatFlag=true;
//				columnCount +=7;
//			}
//			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.SERVICEMODULE, AppConstants.PC_ADDSERVICEDURATIONCOLUMN)){
//				serviceDurationFlag=true;
//				columnCount++;
//			}
	//
//			 /**
//			 * nidhi
//			 * 9-08-2018
//			 * for map serial no
//			 */
//			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("HVASC", "HideOrShowModelNoAndSerialNoDetails")){
//				showHideSerialNo = true;
//				columnCount +=2; 
//			}
//			
//			ArrayList<String> servicelist = new ArrayList<String>();
//			servicelist.add("SERVICE ID");
//			servicelist.add("SERVICE NUMBER");
//			servicelist.add("CONTRACT ID");
//			servicelist.add("REF NO.1");
//			servicelist.add("REF NO.2");
//			servicelist.add("CONTRACT START DATE");
//			servicelist.add("CONTRACT END DATE");
//			servicelist.add("CUSTOMER ID");
//			servicelist.add("CUSTOMER NAME");
//			servicelist.add("CUSTOMER CELL NO.1");
	//
////			/*
////			 * Date:22/08/2018
////			 * Developer:Ashwini
////			 * Des:To add contact Details in customer Service list
////			 */
////			servicelist.add("CUSTOMER CELL NO.2");
////			servicelist.add("LANDLINE NO");
////			servicelist.add("Email Id");
//			
//			/*
//			 * End by Ashwini
//		    */
//			servicelist.add("ADDRESS LINE 1");
//			servicelist.add("ADDRESS LINE 2");
//			servicelist.add("LANDMARK");
//			servicelist.add("COUNTRY");
//			servicelist.add("STATE");
//			servicelist.add("CITY");
//			servicelist.add("LOCALITY");
//			servicelist.add("PIN");
//			servicelist.add("PRODUCT ID");
//			servicelist.add("PRODUCT CODE");
//			servicelist.add("PRODUCT NAME");
//			servicelist.add("SERVICE DATE");
//			servicelist.add("SERVICE TIME");
	//
//			
//			if(serviceDurationFlag){
//				servicelist.add("SERVICE DURATION");
//			}
//			servicelist.add("SERVICE DAY");
//			servicelist.add("SERVICE COMPLETION DATE");
	//
//			/*
//			 * Date:14/07/2018
//			 * Developer:Ashwini
//			 */
//			servicelist.add("SERVICE COMPLETION REMARK");
//			/*
//		    * End by Ashwini
//		    */
//			
//			servicelist.add("SERVICE REPORT1");
//			servicelist.add("SERVICE BRANCH");
//			/**
//			 *  nidhi
//			 *  4-12-2017
//			 */
//			servicelist.add("SERVICE TYPE");
//			servicelist.add("SERVICE ENGINEER");
//			servicelist.add("TEAM");
//			servicelist.add("REASON FOR CANCELLATION");
//			servicelist.add("QUANTITY");
//			servicelist.add("PREMISES");
//			servicelist.add("UOM");
//			servicelist.add("PER UNIT PRICE");
	//
//			/**
//			 * Date : 30-11-2017 BY ANIL
//			 * add service value column
//			 * 
//			 */
//			servicelist.add("SERVICE VALUE");
//			/**
//			 * End
//			 */
//			servicelist.add("BRANCH");
//			servicelist.add("STATUS");
//			servicelist.add("TECHNICIANS");
//			servicelist.add("COST CENTER");
	//
//	        /**
//	         * Date 30-4-2018
//	         * By jayshree
//	         * Des.Add date And Time And Status column
//	         */
//			servicelist.add("STARTED");
//			servicelist.add("REPORTED");
//			servicelist.add("COMPLETED");//COMPLETED BY TECHNICIAN
//	        //End
//	       
//			
//			if(showHideSerialNo){
//				servicelist.add("Product Model No.");
//				servicelist.add("Product Serial No.");
//			}
//			/**
//			 * end
//			 */
//			
//			/**
//			 * @author Anil
//			 * @since 06-06-2020
//			 */
//			if(complainSeviceTatFlag){
//				servicelist.add("TIER NAME");
//				servicelist.add("ASSET ID");
//				servicelist.add("COMPONENT NAME");
//				servicelist.add("MFG. NO.");
//				servicelist.add("MFG. DATE");
//				servicelist.add("REPLACEMENT DATE");
//				servicelist.add("UNIT");
//			}
//			
//			/**
//			 * end
//			 */
//			
//			for(Service s:list)
//			{
//				servicelist.add(s.getCount()+"");
//				servicelist.add(s.getServiceSerialNo()+"");
//				servicelist.add(s.getContractCount()+"");
//				/**
//				 * Date:24-01-2017 By ANil
//				 */
//				if(s.getRefNo()!=null){
//					servicelist.add(s.getRefNo()+"");
//				}else{
//					servicelist.add("");
//				}
//				
//				if(s.getRefNo2()!=null){
//					servicelist.add(s.getRefNo2()+"");
//				}else{
//					servicelist.add("");
//				}
//				/**
//				 * End
//				 */
//				
//				if(s.getContractStartDate()!=null){
//					servicelist.add(AppUtility.parseDate(s.getContractStartDate(),"dd-MMM-yyyy"));
	//
//				}else{
//					servicelist.add("");
//				}
//				
//				if(s.getContractEndDate()!=null){
//					servicelist.add(AppUtility.parseDate(s.getContractEndDate(),"dd-MMM-yyyy"));
	//
//				}else{
//					servicelist.add("");
//				}
//				servicelist.add(s.getCustomerId()+"");
//				servicelist.add(s.getCustomerName());
	//
//				if(s.getCellNumber()!=null && s.getCellNumber()!=0){
//					servicelist.add(s.getCellNumber()+"");
//				}
//				else{
//					servicelist.add("");
//				}
//				
//				if(s.getAddrLine1()!=null){
//					servicelist.add(s.getAddrLine1());
//				}
//				else{
//					servicelist.add("");
//				}
//				
//				if(s.getAddrLine2()!=null){
//					servicelist.add(s.getAddrLine2());
//				}
//				else{
//					servicelist.add("");
//				}
//				
//				if(s.getLandmark()!=null){
//					servicelist.add(s.getLandmark());
//				}
//				else{
//					servicelist.add("");
//				}
//				if(s.getCountry()!=null){
//					servicelist.add(s.getCountry());
//				}else{
//					servicelist.add("");
//				}
//				
//				if(s.getState()!=null){
//					servicelist.add(s.getState());
//				}else{
//					servicelist.add("");
//				}
//			
//				if(s.getCity()!=null){
//					servicelist.add(s.getCity());
//				}else{
//					servicelist.add("");
//				}
//				if(s.getLocality()!=null){
//					servicelist.add(s.getLocality());
//				}
//				else{
//					servicelist.add("");
//				}
//				servicelist.add(s.getPin()+"");
	//
//				if(s.getProduct()!=null){
//					servicelist.add(s.getProduct().getCount()+"");
//				}else{
//					servicelist.add("");
//				}
//				
//				if(s.getProduct()!=null&&s.getProduct().getProductCode()!=null){
//					servicelist.add(s.getProduct().getProductCode()+"");
//				}else{
//					servicelist.add("");
//				}
//				
//				if(s.getProduct()!=null&&s.getProduct().getProductName()!=null){
//					servicelist.add(s.getProduct().getProductName()+"");
//				}else{
//					servicelist.add("");
//				}
//				if(s.getServiceDate()!=null){
//					servicelist.add(AppUtility.parseDate(s.getServiceDate(),"dd-MMM-yyyy"));
//				}else{
//					servicelist.add("");
//				}
//				if(s.getServiceTime()!=null){
//					servicelist.add(s.getServiceTime());
//				}else{
//					servicelist.add("");
//				}
//				
//				if(serviceDurationFlag){
//					if(s.getServiceDuration()!=0){
//						servicelist.add(s.getServiceDuration()+"");
//					}else{
//						servicelist.add("");
//					}
//				}
//				
//				if(s.getServiceDateDay()!=null){
//					servicelist.add(s.getServiceDateDay());
//				}else{
//					servicelist.add("");
//				}
//				
//				if(s.getServiceCompletionDate()!=null)
//				{
//					servicelist.add(AppUtility.parseDate(s.getServiceCompletionDate(),"dd-MMM-yyyy"));
//				}
//				else
//				{
//					servicelist.add("NA");
//				}
//				
//				/*
//				 * Date:14/07/2018
//				 * Developer:Ashwini
//				 */
//				if(s.getServiceCompleteRemark()!=null)
//				{
//					servicelist.add(s.getServiceCompleteRemark());
//				}
//				else  {
//					servicelist.add("");
//				}
//				//end by Ashwini
//				//Ashwini Patil Date:10-03-2023
//				if(s.getUptestReport()!=null){
//					String url=s.getUptestReport().getUrl()+"&filename="+s.getUptestReport().getName();				
//					servicelist.add(url);
//				}else{
//					servicelist.add("");
//				}
//				
//				if(s.getServiceBranch()!=null){
//					servicelist.add(s.getServiceBranch());
//				}
//				else{
//					servicelist.add("");
//				}
//				/**
//				 *  nidhi
//				 *  4-12-2017
//				 *  for display service type
//				 */
//				if(s.getServiceType()!=null){
//					servicelist.add(s.getServiceType());
//				}
//				else{
//					servicelist.add("");
//				}
//				/**
//				 * end
//				 */
//				if(s.getEmployee()!=null){
//					servicelist.add(s.getEmployee());
//				}else{	
//					servicelist.add("N.A");
//				}
//				if(s.getTeam()!=null && !s.getTeam().equals("")) 
//					servicelist.add(s.getTeam());
//				else	
//					servicelist.add("N.A");
//				
//				if(s.getReasonForChange()!=null&&!s.getReasonForChange().equals("")){
//					servicelist.add(s.getReasonForChange());
//				}else{	
//					servicelist.add("");
//				}
//				
//				if(s.getQuantity()!=0){
//					servicelist.add(s.getQuantity()+"");
//				}else{
//					servicelist.add("");
//				}
//				//Ashwini Patil Date:22-07-2022 Adding Premises column as per Nitin sir's instruction
//				if(s.getPremises()!=null&&!s.getPremises().equals("")){
//					servicelist.add(s.getPremises());
//				}else{
//					servicelist.add("");
//				}
//				
//				if(s.getUom()!=null){
//					servicelist.add(s.getUom());
//				}else{
//					servicelist.add("");
//				}
//				/** date 16.02.2018 added by komal for area wise billing**/
//				if(s.getPerUnitPrice()!=0){
//					servicelist.add(s.getPerUnitPrice()+"");
//				}else{
//					servicelist.add("");
//				}
//				/**
//				 * Date : 30-11-2017 BY ANIL
//				 */
//				if(s.getServiceValue()!=0){
//					servicelist.add(s.getServiceValue()+"");
//				}else{
//					servicelist.add("");
//				}
//				/**
//				 * End
//				 */
//				if(s.getBranch()!=null){
//					servicelist.add(s.getBranch());
//				}else{
//					servicelist.add("");
//				}
//				
//				if(s.getStatus()!=null){
//					servicelist.add(s.getStatus());
//				}else{
//					servicelist.add("");
//				}
//				/**
//				 * Date : 06-11-2017 BY ANIL
//				 * Added technician info in service
//				 * for NBHC
//				 */
//				String technicians="";
//				if(s.getTechnicians()!=null){
//					for(EmployeeInfo object:s.getTechnicians()){
//						technicians=technicians+object.getFullName()+"/";
//					}
//					try{
//						if(!technicians.equals("")){
//							technicians=technicians.substring(0,technicians.length()-1);
//						}
//					}catch(Exception e){
//						
//					}
//				}
//				servicelist.add(technicians);
//				/**
//				 * End
//				 */
//				/** date 17.02.2018 added by komal for area wise billing**/
//		        if(s.getCostCenter()!=null){
//					servicelist.add(s.getCostCenter());
//		        }else{
//					servicelist.add("");
//		        }
//		        /**
//		         * Date 30-4-2018
//		         * By Jayshree
//		         * Des.add the stated time,reported time ,completion by technician 
//		         */
//		        
//		        String starttime=null;
//		        String reportedtime=null;
//		        String completedbytch=null;
//		        if(s.getTrackServiceTabledetails().size()!=0){
//		        	
//		        	for (int i = 0; i <s.getTrackServiceTabledetails().size(); i++) {
//						if(s.getTrackServiceTabledetails().get(i).getStatus().equalsIgnoreCase("Started")){
//							starttime=s.getTrackServiceTabledetails().get(i).getDate_time();
//						}
//						if(s.getTrackServiceTabledetails().get(i).getStatus().equalsIgnoreCase("Reported")){
//							reportedtime=s.getTrackServiceTabledetails().get(i).getDate_time();
//						}
//						
//						if(s.getTrackServiceTabledetails().get(i).getStatus().equalsIgnoreCase("Completed")){//Completed By Technician
//							completedbytch=s.getTrackServiceTabledetails().get(i).getDate_time();
//						}
//					}
//		        	}
//		        
//		        if(starttime!=null){
//					servicelist.add(starttime);
//		        }
//		        else{
//					servicelist.add("");
//		        }
//		        if(reportedtime!=null){
//					servicelist.add(reportedtime);
//		        }
//		        else{
//					servicelist.add("");
//		        }
//		        if(completedbytch!=null){
//					servicelist.add(completedbytch);
//		        }
//		        else{
//					servicelist.add("");
//		        }
//		        //End By jayshree
//		        /**
//				 * nidhi
//				 * 9-08-2018
//				 * for map serial no
//				 */
//				if(showHideSerialNo){
//					servicelist.add(s.getProModelNo());
//					servicelist.add(s.getProSerialNo());
//				}
//				/**
//				 * end
//				 */
//				
//				/**
//				 * @author Anil
//				 * @since 06-06-2020
//				 */
//				if(complainSeviceTatFlag){
//					servicelist.add(s.getProSerialNo());
//					servicelist.add(s.getTierName());
//					servicelist.add(s.getAssetId()+"");
//					servicelist.add(s.getComponentName());
//					servicelist.add(s.getMfgNo());
//					if(s.getMfgDate()!=null){
//						servicelist.add(AppUtility.parseDate(s.getMfgDate(),"dd-MMM-yyyy"));
//					}
//					else{
//						servicelist.add("");
//					}
//					if(s.getReplacementDate()!=null){
//						servicelist.add(AppUtility.parseDate(s.getReplacementDate(),"dd-MMM-yyyy"));
//					}
//					else{
//						servicelist.add("");
//					}
//					if(s.getAssetUnit()!=null&&!s.getAssetUnit().equals("")) {
//						servicelist.add(s.getAssetUnit());
//					}
//					else{
//						servicelist.add("");
//					}
//				}
//			}
//			
//			async.setExcelData(servicelist, columnCount, AppConstants.SERVICE, new AsyncCallback<String>() {
//				
//				@Override
//				public void onSuccess(String result) {
//					
//					String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
//					final String url=gwt + "CreateXLXSServlet"+"?type="+13;
//					Window.open(url, "test", "enabled");
//				}
//				
//				@Override
//				public void onFailure(Throwable caught) {
//					// TODO Auto-generated method stub
//					
//				}
//			});
			
				async.setservice(custarray, new AsyncCallback<Void>() {

					@Override
					public void onFailure(Throwable caught) {
						System.out.println("RPC call Failed"+caught);
						
					}

					@Override
					public void onSuccess(Void result) {
						
						/**
						 * nidhi
						 * 16-4-2018
						 * for service list custom report download using process configration
						 * 
						 */
						
						if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "ServiceReport")){
							/**
							 * Date 13-03-2020
							 * Des :- NBHC InHouse Service download and customer service list download 
							 * if block for InHouse Service download and else block for customer service list download
							 */
							if(AppMemory.getAppMemory().currentScreen == Screen.INHOUSESERVICE){
								String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
								final String url = gwt + "csvservlet" + "?type=" + 181;
								Window.open(url, "test", "enabled");
							}
							else{
								String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
								final String url = gwt + "csvservlet" + "?type=" + 137;
								Window.open(url, "test", "enabled");
							}
							
						}else{
							String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
							final String url=gwt + "csvservlet"+"?type="+5;
							Window.open(url, "test", "enabled");
							
//							final String url=gwt + "CreateXLXSServlet"+"?type="+13;
//							Window.open(url, "test", "enabled");
						}
					}
				});
			}
			
		}
		
		
		
		private void suspendServices(final ArrayList<Service> list) {

			final String suspendRemark = suspendservicepopup.getOlbCancellationRemark().getValue();
				
			form.showWaitSymbol();
			
			commonserviceasync.suspendServices(list, suspendRemark, new AsyncCallback<String>() {
				
				@Override
				public void onSuccess(String result) {
					// TODO Auto-generated method stub
					List<Service> allServlist = form.getSuperTable().getDataprovider().getList();
					for(int i=0;i<list.size();i++){
						for(int j=0;j<allServlist.size();j++){
							if(list.get(i).getCount() == allServlist.get(j).getCount()){
								allServlist.get(j).setStatus(Service.SERVICESUSPENDED);
								allServlist.get(j).setRecordSelect(false);
								allServlist.get(j).setRemark(suspendRemark);
							}
						}
					}
					form.hideWaitSymbol();
					form.showDialogMessage(result);
					form.getSuperTable().getTable().redraw();
					Console.log("Inside On Success");
					
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					form.hideWaitSymbol();
					form.showDialogMessage("An Unexpected Error occured !");
					Console.log("Inside On fail");
				}
			});
			
		}
		
		//Ashwini Patil Date:31-10-2023 
		private void reactOnServiceUpload(final String name){
			final Company c = new Company();
			datamigAsync.savedRecordsDeatils(c.getCompanyId(), name, true,LoginPresenter.loggedInUser,
					new AsyncCallback<ArrayList<Integer>>() {

						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							
						}

						@Override
						public void onSuccess(ArrayList<Integer> result) {
								if (result.contains(-1)) {
									String gwt = com.google.gwt.core.client.GWT
											.getModuleBaseURL();
									final String url = gwt + "csvservlet"
											+ "?type=" + 220;
									Window.open(url, "test", "enabled");
									form.showDialogMessage("Please find error file!");
								} else if(result.contains(-2)) {
									form.showDialogMessage("Excel sheet header not matched Please get latest Excel sheet to upload! ");
								}
								else if(result.contains(0)){
									Console.log("Service Update Process Started");
									form.showDialogMessage("Service Update Process Started");
								}
								else{
									Console.log("Failed"+result);
								}
									
								return;							
						}
				
			});
		}
		
		public void suspendServicesFromDocumentCancellationPopup(final ArrayList<Service>list){

			final String suspendRemark = suspendPopup.getRemark().getValue().trim();
			

			form.showWaitSymbol();
			
			commonserviceasync.suspendServices(list, suspendRemark, new AsyncCallback<String>() {
				
				@Override
				public void onSuccess(String result) {
					// TODO Auto-generated method stub
					List<Service> allServlist = form.getSuperTable().getDataprovider().getList();
					for(int i=0;i<list.size();i++){
						for(int j=0;j<allServlist.size();j++){
							if(list.get(i).getCount() == allServlist.get(j).getCount()){
								allServlist.get(j).setStatus(Service.SERVICESUSPENDED);
								allServlist.get(j).setRecordSelect(false);
								allServlist.get(j).setRemark(suspendRemark);
							}
						}
					}
					form.hideWaitSymbol();
					form.showDialogMessage(result);
					form.getSuperTable().getTable().redraw();
					Console.log("Inside On Success");
					
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					form.hideWaitSymbol();
					form.showDialogMessage("An Unexpected Error occured !");
					Console.log("Inside On fail");
				}
			});
		
		}
		

}
