package com.slicktechnologies.client.views.service;

import java.util.ArrayList;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.scheduling.TeamManagement;

public class ServiceSchedulePrintPopup extends PopupScreen{

	ObjectListBox<TeamManagement> olbTeam;
	ObjectListBox<Employee> olbEmployee;
	DateBox dbFromDate;
	DateBox dbTodate;
	ListBox lbStatus;
	
	public ServiceSchedulePrintPopup(){
		super();
		getPopup().setWidth("500px");
		createGui();
	}
	
	private void initializeWidget() {
		olbTeam = new ObjectListBox<TeamManagement>();
		olbEmployee = new ObjectListBox<Employee>();
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "LoadOperatorDropDown")){
			olbEmployee.makeEmployeeLive(AppConstants.SERVICEMODULE, AppConstants.CUSTOMERPROJECT, "Operator");
		}else{
			olbEmployee.makeEmployeeLive(AppConstants.SERVICEMODULE, AppConstants.CUSTOMERSERVICELIST, "Service Engineer");
		}
		
		
		dbFromDate = new DateBox();
		dbTodate = new DateBox();
		
		lbStatus=new ListBox();
		lbStatus.addItem("--SELECT--");
		for(String s:Service.getStatusList()) {
			lbStatus.addItem(s);
		}
	}

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
	}

	@Override
	public void createScreen() {
		// TODO Auto-generated method stub
		initializeWidget();
		initalizeTeamListBox();
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingSchedulePrint=fbuilder.setlabel("Schedule Print").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("Team", olbTeam);
		FormField folbTeam= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
//		fbuilder = new FormFieldBuilder("Service Engineer", olbEmployee);
		/**@Sheetal:17-03-2022 
		 *  Des : Renaming Service Engineer to Technician, requirement by nitin sir**/
		fbuilder = new FormFieldBuilder("Technician", olbEmployee);
		FormField folbEmployee= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("From Date", dbFromDate);
		FormField fdbFromDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	
		
		fbuilder = new FormFieldBuilder("To Date", dbTodate);
		FormField fdbTodate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Status", lbStatus);
		FormField flbStatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "DateTechnicianWiseServicePopup")){
			FormField[][] 	formfield = {  
					{fgroupingSchedulePrint},
					{fdbFromDate , fdbTodate},
					{flbStatus,folbEmployee}		
			};
			this.fields=formfield;
		}else{
			FormField[][]  formfield = {  
					{fgroupingSchedulePrint},
					{folbTeam , folbEmployee},
					{fdbFromDate , fdbTodate},
					{flbStatus},
				
			};
			this.fields=formfield;
		}
		
		

	}
	private void initalizeTeamListBox() {
		MyQuerry querry = new MyQuerry();
		querry.setQuerryObject(new TeamManagement());
		getOlbTeam().MakeLive(querry);
	}

	public ObjectListBox<TeamManagement> getOlbTeam() {
		return olbTeam;
	}

	public void setOlbTeam(ObjectListBox<TeamManagement> olbTeam) {
		this.olbTeam = olbTeam;
	}

	public ObjectListBox<Employee> getOlbEmployee() {
		return olbEmployee;
	}

	public void setOlbEmployee(ObjectListBox<Employee> olbEmployee) {
		this.olbEmployee = olbEmployee;
	}

	public DateBox getDbFromDate() {
		return dbFromDate;
	}

	public void setDbFromDate(DateBox dbFromDate) {
		this.dbFromDate = dbFromDate;
	}

	public DateBox getDbTodate() {
		return dbTodate;
	}

	public void setDbTodate(DateBox dbTodate) {
		this.dbTodate = dbTodate;
	}

	public ListBox getLbStatus() {
		return lbStatus;
	}

	public void setLbStatus(ListBox lbStatus) {
		this.lbStatus = lbStatus;
	}
	
}
