package com.slicktechnologies.client.views.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.contract.ComponentDetailsTable;
import com.slicktechnologies.shared.BranchWiseScheduling;
import com.slicktechnologies.shared.ComponentDetails;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;

public class UpdateComponentDetailsPopup extends PopupScreen implements SelectionHandler<Suggestion>, ChangeHandler {
	
	PersonInfoComposite personInfoComposite;
	ObjectListBox<Contract> oblContract;
	ObjectListBox<SalesLineItem> oblProduct;
	ComponentDetailsTable componentTable;
	
	
	DateBox dbContractStartDate;
	DateBox dbContractEndDate;
	ObjectListBox<BranchWiseScheduling> oblBranchScheduling;
	
	GenricServiceAsync async = GWT.create(GenricService.class);
	GeneralServiceAsync genAsync = GWT.create(GeneralService.class);
	
	public UpdateComponentDetailsPopup() {
		super();
		createGui();
	}
	private void initializeWidget() {
		personInfoComposite=AppUtility.customerInfoComposite(new Customer());
		personInfoComposite.setWidth("101%");
		oblContract=new ObjectListBox<Contract>();
		oblContract.addItem("--SELECT--");
		oblProduct=new ObjectListBox<SalesLineItem>();
		oblProduct.addItem("--SELECT--");
		componentTable=new ComponentDetailsTable(true);
		componentTable.getTable().setHeight("250px");
		
		personInfoComposite.getId().addSelectionHandler(this);
		personInfoComposite.getName().addSelectionHandler(this);
		personInfoComposite.getPhone().addSelectionHandler(this);
		
		oblContract.addChangeHandler(this);
		oblProduct.addChangeHandler(this);
		
//		dbContractStartDate=new DateBox();
		dbContractStartDate = new DateBoxWithYearSelector();
//		dbContractEndDate=new DateBox();
		dbContractEndDate = new DateBoxWithYearSelector();

		
		dbContractStartDate.setEnabled(false);
		dbContractEndDate.setEnabled(false);
		
		oblBranchScheduling=new ObjectListBox<BranchWiseScheduling>();
		oblBranchScheduling.addItem("--SELECT--");
		
		oblBranchScheduling.addChangeHandler(this);
	}
	

	@Override
	public void createScreen() {
		initializeWidget();
		FormFieldBuilder fbuilder;
		
		fbuilder = new FormFieldBuilder();
		FormField fgrpingFilters=fbuilder.setlabel("Apply Filter").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",personInfoComposite);
		FormField fpersonInfoComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Contract List",oblContract);
		FormField foblContract= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("Product List",oblProduct);
		FormField foblProduct= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("Contract Start Date",dbContractStartDate);
		FormField fdbContractStartDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("Contract End Date",dbContractEndDate);
		FormField fdbContractEndDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("Customer Branch",oblBranchScheduling);
		FormField foblBranchScheduling= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
		InlineLabel lbl=new InlineLabel();
		lbl.setVisible(false);
		fbuilder = new FormFieldBuilder(" ",lbl);
		FormField flbl= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
		
		fbuilder = new FormFieldBuilder();
		FormField fgrpingComponentDetails=fbuilder.setlabel("Component Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",componentTable.getTable());
		FormField fcomponentTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		FormField[][] formfield = {
				{fgrpingFilters},
				{fpersonInfoComposite},
				{foblContract,fdbContractStartDate,fdbContractEndDate},
				{foblProduct,foblBranchScheduling,flbl},
				{fgrpingComponentDetails},
				{fcomponentTable},
		};

		this.fields=formfield;
		
		
	}

	
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		if(event.getSource()== getLblOk()){
			updateComponentDetailsOnContract();
		}
		
		if(event.getSource()== getLblCancel()){
			hidePopUp();
		}
	}
	@Override
	public void onSelection(SelectionEvent<Suggestion> event) {
		if (event.getSource().equals(personInfoComposite.getId())|| event.getSource().equals(personInfoComposite.getName())|| event.getSource().equals(personInfoComposite.getPhone())) {
			Console.log("Customer Selection");
			oblContract.clear();
			oblProduct.clear();
			oblContract.addItem("--SELECT--");
			oblProduct.addItem("--SELECT--");
			oblBranchScheduling.clear();
			oblBranchScheduling.addItem("--SELECT--");
			componentTable.connectToLocal();
			loadContractDetails(Integer.parseInt(personInfoComposite.getId().getValue().trim()));	
		}
	}
	@Override
	public void onChange(ChangeEvent event) {
		// TODO Auto-generated method stub
		if(event.getSource()== oblContract){
			if(oblContract.getSelectedIndex()!=0){
				Contract obj=oblContract.getSelectedItem();
				oblProduct.setListItems(obj.getItems());
				dbContractStartDate.setValue(obj.getStartDate());
				dbContractEndDate.setValue(obj.getEndDate());
				
				oblBranchScheduling.clear();
				oblBranchScheduling.addItem("--SELECT--");
				componentTable.connectToLocal();
			}else{
				oblProduct.clear();
				oblProduct.addItem("--SELECT--");
				oblBranchScheduling.clear();
				oblBranchScheduling.addItem("--SELECT--");
				dbContractStartDate.setValue(null);
				dbContractEndDate.setValue(null);
				componentTable.connectToLocal();
			}
		}
		
		if(event.getSource()== oblProduct){
			if(oblProduct.getSelectedIndex()!=0){
				SalesLineItem itemObj=oblProduct.getSelectedItem();
				List<BranchWiseScheduling> branchList=new ArrayList<BranchWiseScheduling>();
				for(BranchWiseScheduling branch:itemObj.getCustomerBranchSchedulingInfo().get(itemObj.getProductSrNo())){
					if(branch.isCheck()){
						branchList.add(branch);
					}
				}
				oblBranchScheduling.setListItems(branchList);
				
				componentTable.connectToLocal();
				
			}else{
				oblBranchScheduling.clear();
				oblBranchScheduling.addItem("--SELECT--");
				componentTable.connectToLocal();
			}
		}
		
		if(event.getSource()== oblBranchScheduling){
			if(oblBranchScheduling.getSelectedIndex()!=0){
				BranchWiseScheduling itemObj=oblBranchScheduling.getSelectedItem();
				List<ComponentDetails> compList=new ArrayList<ComponentDetails>();
				if(itemObj.getComponentList()!=null&&itemObj.getComponentList().size()!=0){
					compList.addAll(itemObj.getComponentList());
				}else{
					showDialogMessage("No component details found!");
					return;
				}
				componentTable.setValue(compList);
				
				
			}else{
				componentTable.connectToLocal();
			}
		}
	}
	
	
	public void loadContractDetails(int customerId) {

		MyQuerry querry = new MyQuerry();
		Company c = new Company();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("cinfo.count");
		filter.setIntValue(customerId);
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Contract());

		async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				oblContract.clear();
				oblProduct.clear();
				oblContract.addItem("--SELECT--");
				oblProduct.addItem("--SELECT--");
				oblBranchScheduling.clear();
				oblBranchScheduling.addItem("--SELECT--");
				componentTable.connectToLocal();
			}
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				oblContract.clear();
				oblProduct.clear();
				oblContract.addItem("--SELECT--");
				oblProduct.addItem("--SELECT--");
				oblBranchScheduling.clear();
				oblBranchScheduling.addItem("--SELECT--");
				componentTable.connectToLocal();
				if (result.size() > 0) {
					List<Contract> contractList = new ArrayList<Contract>();
					for (SuperModel model : result) {
						Contract conEntity = (Contract) model;
						if (conEntity.getStatus().equals("Approved")) {
							contractList.add(conEntity);
						}
					}
					
					Comparator<Contract> comp = new Comparator<Contract>() {
						@Override
						public int compare(Contract p1, Contract p2) {
							Integer con1 = p1.getCount();
							Integer con2 = p2.getCount();
							return con2.compareTo(con1);
						}
					};
					Collections.sort(contractList, comp);
					
					
					oblContract.setListItems(contractList);
				}
			}
		});
	}
	
	
	private void updateComponentDetailsOnContract(){
		String msg="";
		if(oblContract.getSelectedIndex()==0){
			msg=msg+"Please select contract details.";
		}
		if(oblProduct.getSelectedIndex()==0){
			msg=msg+"Please select product details.";
		}
		if(!msg.equals("")){
			showDialogMessage(msg);
			return;
		}
		final Contract contract=oblContract.getSelectedItem();
		showWaitSymbol();
		async.save(contract, new AsyncCallback<ReturnFromServer>() {
			@Override
			public void onFailure(Throwable caught) {
				hideWaitSymbol();
				showDialogMessage("Data updated failed. "+caught.getMessage());
			}
			@Override
			public void onSuccess(ReturnFromServer result) {
				
				genAsync.updateComponentDetailsOnServices(contract, new AsyncCallback<Void>() {
					@Override
					public void onFailure(Throwable caught) {
						hideWaitSymbol();
						showDialogMessage("Data updated failed. "+caught.getMessage());
					}
					@Override
					public void onSuccess(Void result) {
						hideWaitSymbol();
						showDialogMessage("Data updated successfully.");
						hidePopUp();
					}
				});
			}
		});
	}
	public void clear() {
		// TODO Auto-generated method stub
		componentTable.connectToLocal();
		oblContract.clear();
		oblProduct.clear();
		oblContract.addItem("--SELECT--");
		oblProduct.addItem("--SELECT--");
		oblBranchScheduling.clear();
		oblBranchScheduling.addItem("--SELECT--");
		personInfoComposite.clear();
	}
	

}
