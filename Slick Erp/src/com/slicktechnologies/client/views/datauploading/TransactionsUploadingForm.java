package com.slicktechnologies.client.views.datauploading;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.composite.UploadComposite;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.DummyEntityOnlyForScreen;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class TransactionsUploadingForm extends FormScreen<DummyEntityOnlyForScreen> implements ClickHandler {

	ListBox lstboxTransaction;
	Label lblTransaction, lblfilecount, lblsavedrecords,lblPurchaseOrdreq,masterPriceflag;
	UploadComposite transactionUploadComposite;
	Button btnTransactionGo;

	ListBox lstboxRequisition;
	UploadComposite transactionUpload_PRequisition;
	Button btnRequisitionGo;
	CheckBox checkBoxmasterPrice;
	
	public TransactionsUploadingForm() {
		super();
		createGui();
		/***
		 * Date 18-07-2019 by Vijay for Uploading with processbar Buttons
		 *****/
		if (processLevelBar != null)
			processLevelBar.setVisibleFalse(true);

	}

	public TransactionsUploadingForm(String[] processlevel, FormField[][] fields, FormStyle formstyle) {
		super(processlevel, fields, formstyle);
		createGui();
		/***
		 * Date 18-07-2019 by Vijay for Uploading with processbar Buttons
		 *****/
		if (processLevelBar != null)
			processLevelBar.setVisibleFalse(true);
	}

	private void initalizeWidget() {

		lstboxTransaction = new ListBox();
		lstboxTransaction.addItem("--SELECT--");
		lstboxTransaction.addItem("Contract Upload");

		lblTransaction = new Label();
		lblTransaction.setText("TRANSACTIONS");

		transactionUploadComposite = new UploadComposite("Contract", true);

		btnTransactionGo = new Button("Go");
		
		/*** Date:-19-11-2019 Deepak Salve added code for Purchase Requisition Excel Upload ***/
		lstboxRequisition = new ListBox();
		lstboxRequisition.addItem("--SELECT--");
		lstboxRequisition.addItem("Purchase Requisition Upload");
		
		lblPurchaseOrdreq = new Label();
		lblPurchaseOrdreq.setText("PURCHASE REQUISITION");
		masterPriceflag = new Label();
		masterPriceflag.setText(" Material Price from Master");
		checkBoxmasterPrice = new CheckBox();
		checkBoxmasterPrice.setValue(true);
		
		
		
		transactionUpload_PRequisition =new UploadComposite("PurchaseRequisition",true);
		
		btnRequisitionGo = new Button("GO");
        /***End***/
	}

	@Override
	public void createScreen() {

		initalizeWidget();

		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingDataMigration = fbuilder.setlabel("Transaction Uploading").widgetType(FieldType.Grouping)
				.setMandatory(false).setColSpan(6).build();

		fbuilder = new FormFieldBuilder("", lblTransaction);
		FormField flblTransaction = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("", lstboxTransaction);
		FormField flstboxTransaction = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("", transactionUploadComposite);
		FormField ftransactionUploadComposite = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("", btnTransactionGo);
		FormField fbtnTransactionGo = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",lblPurchaseOrdreq);
		FormField flblPurchaseOrdreq = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",lstboxRequisition);
		FormField flstboxRequisition = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",transactionUpload_PRequisition);
		FormField ftransactionUpload_PORequisition = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",btnRequisitionGo);
		FormField fbtnRequisitionGo = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",masterPriceflag);
		FormField flablemasterPriceflag = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",checkBoxmasterPrice);
		FormField fcheckboxmasterPrice = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		

		FormField[][] formfield = { { fgroupingDataMigration, },
				{ flblTransaction, flstboxTransaction, ftransactionUploadComposite, fbtnTransactionGo },
				{ flblPurchaseOrdreq, flstboxRequisition, ftransactionUpload_PORequisition,flablemasterPriceflag,fcheckboxmasterPrice,fbtnRequisitionGo },};

		this.fields = formfield;

	}

	@Override
	public void toggleAppHeaderBarMenu() {
		// TODO Auto-generated method stub

		if (AppMemory.getAppMemory().currentState == ScreeenState.NEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Discard")) {
					menus[k].setVisible(true);
				} else
					menus[k].setVisible(false);
			}
		}

		else if (AppMemory.getAppMemory().currentState == ScreeenState.EDIT) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Discard"))
					menus[k].setVisible(true);

				else
					menus[k].setVisible(false);
			}
		}

		else if (AppMemory.getAppMemory().currentState == ScreeenState.VIEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Discard"))
					menus[k].setVisible(true);

				else
					menus[k].setVisible(false);
			}
		}

		AuthorizationHelper.setAsPerAuthorization(Screen.TRANSACTIONUPLOADING, LoginPresenter.currentModule.trim());

	}

	@Override
	public void updateModel(DummyEntityOnlyForScreen model) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateView(DummyEntityOnlyForScreen model) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setToNewState() {
		if (processLevelBar != null)
			processLevelBar.setVisibleFalse(true);
	}

}
