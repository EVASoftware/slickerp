package com.slicktechnologies.client.views.datauploading;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.itextpdf.text.log.SysoCounter;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.slicktechnologies.client.services.DataMigrationTaskQueueService;
import com.slicktechnologies.client.services.DataMigrationTaskQueueServiceAsync;
import com.slicktechnologies.client.services.DocumentUploadService;
import com.slicktechnologies.client.services.DocumentUploadServiceAsync;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.shared.common.DummyEntityOnlyForScreen;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;

public class TransactionsUploadingPresenter extends FormScreenPresenter<DummyEntityOnlyForScreen> {

	final DataMigrationTaskQueueServiceAsync datamigtaskqueueAsync = GWT.create(DataMigrationTaskQueueService.class);
	Logger logger = Logger.getLogger("Name of logger");

	TransactionsUploadingForm form;

	public TransactionsUploadingPresenter(FormScreen<DummyEntityOnlyForScreen> view, DummyEntityOnlyForScreen model) {
		super(view, model);

		form = (TransactionsUploadingForm) view;

		form.setPresenter(this);
		form.btnTransactionGo.addClickHandler(this);
		form.btnRequisitionGo.addClickHandler(this);
	}

	public static void initialize() {

		AppMemory.getAppMemory().currentState = ScreeenState.NEW;

		TransactionsUploadingForm form = new TransactionsUploadingForm();

		TransactionsUploadingPresenter presenter = new TransactionsUploadingPresenter(form,
				new DummyEntityOnlyForScreen());
		AppMemory.getAppMemory().stickPnel(form);

	}

	@Override
	public void onClick(ClickEvent event) {

		GeneralServiceAsync genAsync = GWT.create(GeneralService.class);
		
		final DocumentUploadServiceAsync documentuploadAsync = GWT.create(DocumentUploadService.class);

		if (event.getSource() == form.btnTransactionGo) {
			String entityName = form.lstboxTransaction.getValue(form.lstboxTransaction.getSelectedIndex());
			final Company comp = new Company();
			form.showWaitSymbol();
			/**
			 * @author Vijay Chougule
			 * Des :- 18-10-2019 for NBHC CCPM Contract Upload
			 */
			documentuploadAsync.readContractUploadExcelFile(entityName, comp.getCompanyId(), new AsyncCallback<ArrayList<String>>() {

				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					form.hideWaitSymbol();
				}

				@Override
				public void onSuccess(ArrayList<String> contractExcellist) {
					// TODO Auto-generated method stub
					if(contractExcellist.size()==1){
						form.showDialogMessage(contractExcellist.get(0));
						form.hideWaitSymbol();
					}
					else{
						documentuploadAsync.reactonValidateContractUpload(contractExcellist, comp.getCompanyId(), new AsyncCallback<ArrayList<String>>() {

							@Override
							public void onFailure(Throwable caught) {
								// TODO Auto-generated method stub
								form.hideWaitSymbol();
							}

							@Override
							public void onSuccess(ArrayList<String> contractexcel) {
								// TODO Auto-generated method stub
								if(contractexcel.size()==1){
									form.showDialogMessage(contractexcel.get(0));
									form.hideWaitSymbol();
								}
								else{
									documentuploadAsync.reactonContractUpload(contractexcel.get(1), comp.getCompanyId(), new AsyncCallback<String>() {

										@Override
										public void onFailure(Throwable caught) {
											// TODO Auto-generated method stub
											form.hideWaitSymbol();
										}

										@Override
										public void onSuccess(String result) {
											// TODO Auto-generated method stub
											form.showDialogMessage(result);
											form.hideWaitSymbol();
										}
									});
								}
							}
						});
					   }
					}
				});
		}
		
		
		if (event.getSource() == form.btnRequisitionGo) {
		   
			/*** Date-19-11-2019 Deepak Salve added code Purchase Requisition Excel Upload ***/
			
			logger.log(Level.SEVERE, "Inside Purchase Requisition ");
			
			final boolean masterPriceFlag=form.checkBoxmasterPrice.getValue();
			String entityName=form.lstboxRequisition.getValue(form.lstboxRequisition.getSelectedIndex());
			final Company company = new Company();
			
			
			if(entityName.equals("Purchase Requisition Upload"))
			{ 
			
			   String blobkeyURL=form.transactionUpload_PRequisition.getValue().getUrl();
			   logger.log(Level.SEVERE, " blobkeyURL :- "+form.transactionUpload_PRequisition.getValue().getUrl());
			   if(form.transactionUpload_PRequisition.getValue()==null){
				   form.showDialogMessage("Please upload PR file ");
				   return ;
			   }	
			   form.showWaitSymbol();
			   documentuploadAsync.reactOnPurchaseRequisitionExcelRead(entityName,company.getCompanyId(),blobkeyURL,new AsyncCallback<ArrayList<String>>() {
				 @Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					form.showDialogMessage(caught.getMessage());	
					form.hideWaitSymbol();
				}
				@Override
				public void onSuccess(ArrayList<String> excelData) {
					logger.log(Level.SEVERE, "Inside Purchase Requisition First RPC execute Successful ");
					// TODO Auto-generated method stub
					if (excelData.size() == 1) {
						form.showDialogMessage(excelData.get(0));
						form.hideWaitSymbol();
					}else{
						System.out.println(" Excel Heading read Sucessfully Validation Strat  ");
						logger.log(Level.SEVERE, "Inside Purchase Requisition First RPC execute Successful :-  Excel Heading read Sucessfully Validation Strat  ");
					documentuploadAsync.reactOnPurchaseRequisitionExcelValidation(masterPriceFlag,excelData,company.getCompanyId(),new AsyncCallback<ArrayList<String>>() {

						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							form.showDialogMessage("Please Check Enter Values");	
							form.hideWaitSymbol();
						}
						@Override
						public void onSuccess(ArrayList<String> prExcelData) {
							// TODO Auto-generated method stub
							logger.log(Level.SEVERE, "Inside Purchase Requisition Second RPC execute Successful :- Validation done  ");  
							if (prExcelData.size() == 1) {
							     System.out.println(" Second Method Excel Value "+prExcelData.size()+" prExcelData.get(0)"+prExcelData.get(0));
									form.showDialogMessage(prExcelData.get(0));
									form.hideWaitSymbol();
								}else{
									
									logger.log(Level.SEVERE, "Inside Purchase Requisition Second RPC execute Successful :- Upload Process Start  "+prExcelData.get(1));
									documentuploadAsync.reactOnPurchaseRequisitionExcelUpload(masterPriceFlag,prExcelData.get(1),company.getCompanyId(),new AsyncCallback<String>() {

										@Override
										public void onFailure(Throwable caught) {
											// TODO Auto-generated method stub
											System.out.println("Please Check Enter Values ");
											form.hideWaitSymbol();	
										}

										@Override
										public void onSuccess(String result) {
											// TODO Auto-generated method stub
											System.out.println(" Execute Sucessful ");
											logger.log(Level.SEVERE, "Inside Purchase Requisition Third RPC execute Successful :- Execute Sucessful  ");
											System.out.println(" Result Value "+result);
											form.showDialogMessage(result);
											form.hideWaitSymbol();	
										}
									});
								}
							
						}
					});
					}
				}
			});
		 }	
		}
		
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {

		InlineLabel label = (InlineLabel) e.getSource();
		String text = label.getText().trim();
	}

	private void reactOnContractUpload() {
		// TODO Auto-generated method stub
	}

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub

	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void makeNewModel() {
		// TODO Auto-generated method stub

	}

}
