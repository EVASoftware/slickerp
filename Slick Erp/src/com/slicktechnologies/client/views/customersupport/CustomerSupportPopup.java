package com.slicktechnologies.client.views.customersupport;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.LongBox;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.TabPanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FlexForm;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.composite.CompositeInterface;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.EmailTextBox;
import com.simplesoftwares.client.library.mywidgets.MyLongBox;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.simplesoftwares.client.library.mywidgets.PhoneNumberBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.complain.ActivityTable;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.complain.Complain;
import com.slicktechnologies.shared.common.complain.ComplainList;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.customersupport.ServiceDetails;
import com.slicktechnologies.shared.common.helperlayer.City;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.smartgwt.client.util.SC;

public class CustomerSupportPopup extends PopupScreen implements ValueChangeHandler<Date>{

	public InlineLabel getLblbtnCancel() {
		return lblbtnCancel;
	}

	public void setLblbtnCancel(InlineLabel lblbtnCancel) {
		this.lblbtnCancel = lblbtnCancel;
	}
	final GenricServiceAsync genasync = GWT.create(GenricService.class);
	GeneralServiceAsync generalasync = GWT.create(GeneralService.class);

	public PersonInfoComposite personinfoComposite;
	
	TabPanel tabPanel;
	FlowPanel flowpanel1,flowpanel2,flowpanel3,flowpanel4;
	
	public TextBox ticketNo;
	DateBox dbCreationDate;
	TextBox tbtime;
	TextBox tbCallerName;
	PhoneNumberBox pnbCallerCellNo;
	EmailTextBox etbEmail ;
	TextBox tbStatus;
	ObjectListBox<Employee> olbeSalesPerson,olbassignto;
	
	ObjectListBox<Branch> olbbBranch;
	ObjectListBox<Config> olbcPriority;
	DateBox dbDueDate;
	
	ObjectListBox<ConfigCategory> olbTicketCategory;
	DateBox dbcompaintDate;
	DateBox dbNewServiceDate;
	TextArea taDescription,taRemark;
	public ObjectListBox<CustomerBranchDetails> olbcustomerBranch;
	
	ServiceDetailsTable lastServicedetailsTable;
	ProductInfoComposite prodInfoComposite;
	TextBox tbBrandName,tbModel,tbSerialNumber;
	TextBox tbProSerialNo ;

	ActivityTable activitytable;
	Button btnadd;

	
	protected FormField[][]formfileds;
	protected FormField[][]formfiled2;
	protected FormField[][]formfiled3;
	
	
	private InlineLabel lblUpdate = new InlineLabel("Edit");
	private InlineLabel lblbtnSave = new InlineLabel("Save");
	private InlineLabel lblbtnCancel = new InlineLabel("Cancel");
	Complain complainEntity;
//	ObjectListBox<City>olbCity;
	public Button btnMarkComplete;
	
	boolean otherComplaintFlag = false;
	protected static FormStyle DEFAULT_FORM_STYLE=FormStyle.ROWFORM;
	 InlineLabel lblmarkComplete = new InlineLabel("MarkComplete");
	public CustomerSupportPopup(){
		super();
		createGui();
		getLblUpdate().addClickHandler(this);
	}
	
	public CustomerSupportPopup(Complain complain){
		super();
		createGui();
		lblUpdate.addClickHandler(this);
		lblbtnSave.addClickHandler(this);
		lblbtnCancel.addClickHandler(this);
		this.complainEntity = complain;
	}
	
	/** Date 14-08-2019 by Vijay for Oher than service complaint dashboard ****/
	public CustomerSupportPopup(Complain complain, boolean flag){
		super(DEFAULT_FORM_STYLE,flag);
		this.otherComplaintFlag=flag;
		lblmarkComplete.getElement().setId("addbutton");
		lblmarkComplete.addClickHandler(this);
		this.horizontal.add(lblmarkComplete);
		createScreen();
		form=new FlexForm(fields,formstyle);
		createGui();
		getLblUpdate().addClickHandler(this);
		lblUpdate.addClickHandler(this);
		lblbtnSave.addClickHandler(this);
		lblbtnCancel.addClickHandler(this);
		this.complainEntity = complain;
	}
	
	public CustomerSupportPopup(boolean flag){
		super(DEFAULT_FORM_STYLE,flag);
		this.otherComplaintFlag=flag;
		
		createScreen();
		form=new FlexForm(fields,formstyle);
		createGui();
		getLblUpdate().addClickHandler(this);
	}
	
	private void initializeWidgets() {
		
		personinfoComposite=AppUtility.customerInfoComposite(new Customer());

		tabPanel = new TabPanel();

		ticketNo = new TextBox();
		ticketNo.setEnabled(false);
		dbCreationDate = new DateBoxWithYearSelector();
		dbCreationDate.setEnabled(false);
		tbtime = new TextBox();
		tbtime.setEnabled(false);

		tbCallerName = new TextBox();
		
		pnbCallerCellNo = new PhoneNumberBox();
		tbStatus = new TextBox();
		tbStatus.setEnabled(false);
		tbStatus.setValue("Created");
		
		etbEmail = new EmailTextBox();
		
		olbbBranch=new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbbBranch);
		
		olbcPriority=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbcPriority,Screen.COMPLAINPRIORITY);
		
		olbeSalesPerson=new ObjectListBox<Employee>();
		olbeSalesPerson.makeEmployeeLive(AppConstants.SERVICEMODULE, AppConstants.CUSTOMERSUPPORT, "Sales Person");
		
		olbassignto=new ObjectListBox<Employee>();
		olbassignto.makeEmployeeLive(AppConstants.SERVICEMODULE, AppConstants.CUSTOMERSUPPORT, "Assigned To");
		
		dbDueDate = new DateBoxWithYearSelector();
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Complain", "EnableComplaintDueDate")){
			dbDueDate.setEnabled(false);
		}
		
		olbTicketCategory = new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(olbTicketCategory, Screen.TICKETCATEGORY);
		
		dbcompaintDate = new DateBoxWithYearSelector();
		dbcompaintDate.addValueChangeHandler(this);
		
		dbNewServiceDate = new DateBoxWithYearSelector();
//		dbNewServiceDate.addValueChangeHandler(this);
		dbNewServiceDate.setEnabled(false);
		
		taDescription = new TextArea();
		olbcustomerBranch = new ObjectListBox<CustomerBranchDetails>();
		olbcustomerBranch.addItem("--SELECT--");
		
		
		lastServicedetailsTable = new ServiceDetailsTable();
		
		
		prodInfoComposite=AppUtility.initiatePurchaseProductComposite(new SuperProduct());
		tbBrandName=new TextBox();
		tbModel=new TextBox();
		tbSerialNumber=new TextBox();
		tbProSerialNo = new TextBox();
		
		activitytable=new ActivityTable();
		activitytable.connectToLocal();
		btnadd=new Button("Add");
		btnadd.addClickHandler(this);
		taRemark = new TextArea();
		
//		olbCity = new ObjectListBox<City>();
//		City.makeOjbectListBoxLive(olbCity);

		btnMarkComplete = new Button("Mark Complete");
		btnMarkComplete.addClickHandler(this);
		
		flowpanel1 = new  FlowPanel();
		flowpanel2 = new FlowPanel();
		flowpanel3 = new FlowPanel();
		tabPanel.addSelectionHandler(new SelectionHandler<Integer>() {
			
			@Override
			public void onSelection(SelectionEvent<Integer> event) {
				// TODO Auto-generated method stub
				int tabId = event.getSelectedItem();
				System.out.println("TAB ID " + tabId);
				if (tabId == 0) {

				} else if (tabId == 1) {
				} else if (tabId == 2) {
				activitytable.getTable().redraw();
				} else if (tabId == 3) {
				} else if (tabId == 4) {
				}
			}
		});
		
	}
	
	@Override
	public void createScreen() {
		initializeWidgets();
		
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		
//		fbuilder = new FormFieldBuilder("",btnMarkComplete);
//		FormField fbtnMarkComplete= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",personinfoComposite);
		FormField fpersoninfo= fbuilder.setRowSpan(0).setColSpan(3).build();
		
		
		FormField fgroupingComplaintInformation2=fbuilder.setlabel("Customer Information").widgetType(com.simplesoftwares.client.library.FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingOfficeUse=fbuilder.setlabel("Ticket Information").widgetType(com.simplesoftwares.client.library.FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Ticket No.",ticketNo);
		FormField ftbcountId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Creation Date",dbCreationDate);
		FormField fdbDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Ticket Time",tbtime);
		FormField btntime= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		FormField ftbCallerName;
		FormField fpnbCallerCellNo;

		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Complain","MakeCallerNameAndCellMandatory")){
			fbuilder = new FormFieldBuilder("* Caller Name",tbCallerName );
			ftbCallerName= fbuilder.setMandatory(true).setMandatoryMsg("Caller Name is Mandatory").setRowSpan(0).setColSpan(0).build();
			
			fbuilder = new FormFieldBuilder("* Caller Cell No.",pnbCallerCellNo );
			fpnbCallerCellNo= fbuilder.setMandatory(true).setMandatoryMsg("Caller Cell No. is Mandatory").setRowSpan(0).setColSpan(0).build();
			
		}else{
			fbuilder = new FormFieldBuilder("Caller Name",tbCallerName );
			ftbCallerName= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			
			fbuilder = new FormFieldBuilder("Caller Cell No.",pnbCallerCellNo );
			fpnbCallerCellNo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			
		}
		

		/** date 13.03.2018 added by komal for nbhc**/
		FormField fetbEmail;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Complain", "makeEmailMandatory")){
			fbuilder = new FormFieldBuilder("* Caller Email",etbEmail);
			fetbEmail= fbuilder.setMandatory(true).setMandatoryMsg("Caller Email is Mandatory").setRowSpan(0).setColSpan(0).build();
		}else{
			fbuilder = new FormFieldBuilder("Caller Email",etbEmail);
			fetbEmail= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
		
		
		fbuilder = new FormFieldBuilder("* Person Responsible",olbeSalesPerson);
		FormField folbeSalesPerson= fbuilder.setMandatory(true).setMandatoryMsg("Person Responsible is Mandatory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Status", tbStatus);
		FormField ftbStatus = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Branch",olbbBranch);
		FormField folbbBranch= fbuilder.setMandatory(true).setMandatoryMsg("Branch is Mandatory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Priority",olbcPriority);
		FormField folbcPriority= fbuilder.setMandatory(true).setMandatoryMsg("Priority is Mandatory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Assigned To",olbassignto);
		FormField fassinto= fbuilder.setMandatory(true).setMandatoryMsg("Assigned To is Mandatory").setRowSpan(0).setColSpan(0).build();
	
		fbuilder = new FormFieldBuilder("Due Date",dbDueDate);
		FormField fdbDueDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		FormField fTicketolbCategory;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Complain", "MakeTicketCategoryMandatory")){
			System.out.println("Inside Process config");
			fbuilder = new FormFieldBuilder("* Ticket Category", olbTicketCategory);
			fTicketolbCategory = fbuilder.setMandatory(true).setMandatoryMsg("Ticket Category is mandatory!").setRowSpan(0).setColSpan(0).build();
		}else{
			System.out.println("Else block no process config");
			fbuilder = new FormFieldBuilder("Ticket Category", olbTicketCategory);
			fTicketolbCategory = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
		
		fbuilder = new FormFieldBuilder("* Ticket Date",dbcompaintDate);
		FormField fdbcompaintDate= fbuilder.setMandatory(true).setMandatoryMsg("Ticket Date is Mandatory").setRowSpan(0).setColSpan(0).build();
		
		FormField fdbNewServiceDate;
		System.out.println("otherComplaintFlag =="+otherComplaintFlag);
		if(this.otherComplaintFlag){
			fbuilder = new FormFieldBuilder(" Service Date",dbNewServiceDate);
			fdbNewServiceDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
		else{
			fbuilder = new FormFieldBuilder("* Service Date",dbNewServiceDate);
			 fdbNewServiceDate= fbuilder.setMandatory(true).setMandatoryMsg("Service Date is Mandatory!").setRowSpan(0).setColSpan(0).build();
			
		}
		
		FormField ftaDescription;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Complain","MakeDescriptionMandatory")){
			fbuilder = new FormFieldBuilder("* Description",taDescription);
			ftaDescription= fbuilder.setMandatory(true).setMandatoryMsg("Description is Mandatory").setRowSpan(0).setColSpan(4).build();
		}else{
			fbuilder = new FormFieldBuilder("Description",taDescription);
			ftaDescription= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		}
		
		FormField folbcustomerBranch;
		if(this.otherComplaintFlag){
			fbuilder = new FormFieldBuilder("Service Location/City",olbcustomerBranch);
			folbcustomerBranch = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			
		}
		else{
			fbuilder = new FormFieldBuilder("* Service Location",olbcustomerBranch);
			folbcustomerBranch = fbuilder.setMandatory(true).setMandatoryMsg("Service Location is Mandatory!").setRowSpan(0).setColSpan(0).build();
			
		}
//		fbuilder = new FormFieldBuilder("* Service Location",olbcustomerBranch);
//		FormField folbcustomerBranch = fbuilder.setMandatory(true).setMandatoryMsg("Service Location is Mandatory!").setRowSpan(0).setColSpan(0).build();
		
		FormField lastServiceDetailsGrouping=fbuilder.setlabel("Service Details").widgetType(com.simplesoftwares.client.library.FieldType.Grouping).setMandatory(false).setColSpan(4).build();

		fbuilder = new FormFieldBuilder("",lastServicedetailsTable.getTable());
		FormField flastServicedetailsTable = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
//		fbuilder = new FormFieldBuilder("City",olbCity);
//		FormField folbCity= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	
		
		FormField formfileds [][] = {
				{fgroupingOfficeUse},
				{ftbStatus,ftbcountId,btntime,fdbDate,},
				{ftbCallerName,fpnbCallerCellNo,fetbEmail,folbeSalesPerson},
				{folbbBranch,folbcPriority,fassinto,fdbDueDate},
				{fTicketolbCategory,fdbcompaintDate,fdbNewServiceDate,folbcustomerBranch},
				{ftaDescription},
				{lastServiceDetailsGrouping},
				{flastServicedetailsTable}
				
				
		};
		
		this.formfileds = formfileds;
		tabPanel.add(getFormFileds(formfileds),"Ticket");
		
		FormField fgroupingProductinfo=fbuilder.setlabel("Product Informtaion").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("",prodInfoComposite);
		FormField fprodInfoComposite= fbuilder.setMandatory(true).setMandatoryMsg("Product is Mandatory").setRowSpan(0).setColSpan(3).build();
		
		fbuilder=new FormFieldBuilder("Brand Name", tbBrandName);
		FormField ftbBrandName=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder=new FormFieldBuilder("Model", tbModel);
		FormField ftbModel=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Product Serial Number",tbProSerialNo);
		FormField ftbProSerialNo = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		FormField formfileds2 [][] = {
				{fgroupingProductinfo},
				{fprodInfoComposite},
				{ftbBrandName,ftbModel,ftbProSerialNo}
				
				
		};
		this.formfiled2 = formfileds2;
		tabPanel.add(getFormFileds(formfileds2),"Product Info");

		fbuilder = new FormFieldBuilder();
		FormField fgroupingActivityTable=fbuilder.setlabel("Complain History").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("",activitytable.getTable());
		FormField factivitytable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
	

		fbuilder = new FormFieldBuilder("Remark",taRemark);
		FormField fdbremark= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder("",btnadd);
		FormField fdbtnadd= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		FormField formfileds3 [][] = {
				{fgroupingActivityTable},
				{fdbremark,fdbtnadd},
				{factivitytable},
		};
		
		tabPanel.add(getFormFileds(formfileds3), "Ticket History");
		this.formfiled3 = formfileds3;

		tabPanel.selectTab(0);
		tabPanel.setSize("1107px", "430px");
		
		fbuilder = new FormFieldBuilder("",tabPanel);
		FormField ftabPanel = fbuilder.setRowSpan(0).setColSpan(4).build();
		
		
		FormField fields [][] = {
//				{fbtnMarkComplete},
				{fgroupingComplaintInformation2},
				{fpersoninfo},
				{ftabPanel}
				
		};
		
		this.fields = fields;
		
	}

	

	@Override
	public void onClick(ClickEvent event) {

		if(event.getSource().equals(btnadd)){
			System.out.println("add remark out event");
			if(!this.taRemark.getValue().trim().equals("")){
				System.out.println("add remark In event");
				reactOnAdd();
				this.taRemark.setValue(" ");
			}else{
				showDialogMessage("Please Enter Remark First!");
			}
		}
		
		if(event.getSource() == this.getLblUpdate()){
			this.lblbtnSave.setVisible(true);
			this.lblUpdate.setVisible(false);
			setEnable(true);
		}
		if(event.getSource()==this.lblbtnSave){
			System.out.println("only for update model");
			updateModel(this.complainEntity);
		}
	
		if(event.getSource()==this.lblbtnCancel){
			hidePopUp();
		}
		
//		if(event.getSource()==this.lblmarkComplete){
//			System.out.println("lblmarkComplete");
//			if(!this.ticketNo.getValue().equals("")){
//				reactonMarkComplete(this.ticketNo.getValue());
//			}
//			else{
//				showDialogMessage("Ticket Id not found!");
//			}
//		}
	}

	private void reactonMarkComplete(String ticketId) {
		int complaintId = Integer.parseInt(ticketId);
		Company comp = new Company();
		System.out.println("Comp =="+comp.getCompanyId());
		generalasync.markCompleteComplaint(comp.getCompanyId(), complaintId, new AsyncCallback<String>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(String result) {
				// TODO Auto-generated method stub
				if(result.equals("Success")){
					showDialogMessage("Complaint Completed Successfully ");
					hidePopUp();
				}
				else{
					showDialogMessage("Failed! Please try again");
				}
			}
		});
	}

	@Override
	public void applyStyle() {
		form.getElement().setId("form");  //form   popupform
		horizontal.getElement().addClassName("popupcentering");
		lblOk.getElement().setId("addbutton");
		lblCancel.getElement().setId("addbutton");
		
		popup.center();
	}

	@Override
	public void onValueChange(ValueChangeEvent<Date> event) {

		if(event.getSource() == dbcompaintDate){
			reactonTicketDate();
		}
//		if(event.getSource() == dbNewServiceDate){
//			reactonValidateServiceDate();
//		}
	}

	private void reactonValidateServiceDate() {
		if(dbcompaintDate.getValue()==null){
			showDialogMessage("Please select ticket date first!");
			dbNewServiceDate.setValue(null);
			return;
		}
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Complain", "EnableComplaintDueDate")){
			if(dbDueDate.getValue()!=null){
				System.out.println("dbNewServiceDate.getValue()=="+dbNewServiceDate.getValue());
				System.out.println("dbDueDate.getValue()"+dbDueDate.getValue());
				if(dbNewServiceDate.getValue().after(dbDueDate.getValue())){
					showDialogMessage("Service Date should not be greater than Due Date!");
					dbNewServiceDate.setValue(null);
				}
			}
		}
		
	}

	private void reactonTicketDate() {
		/**
		 * @author Anil @since 20-09-2021
		 * issue raised by Ashwini for pecopp
		 */
		Date todayDate = new Date();
		if(AppUtility.formatDate(AppUtility.parseDate(dbcompaintDate.getValue())).after(AppUtility.formatDate(AppUtility.parseDate(todayDate)))){
//		if(dbcompaintDate.getValue().after(todayDate)){
			showDialogMessage("Ticket Date should not be future date!");
			dbcompaintDate.setValue(null);
			return;
		}
		String tickateDate = AppUtility.getForProcessConfigurartionIsActiveOrNot("EnableTicketDateRestriction");
		System.out.println("tickateDate=="+tickateDate);
		if(tickateDate!=null){
			int ticketDateRestrictionValue = Integer.parseInt(tickateDate);
			Date ticketDateRestrcitionDate = new Date();
			CalendarUtil.addDaysToDate(ticketDateRestrcitionDate, -ticketDateRestrictionValue);
			System.out.println("ticketDateRestrcitionDate =="+ticketDateRestrcitionDate);
			ticketDateRestrcitionDate.setHours(00);
			ticketDateRestrcitionDate.setMinutes(00);
			ticketDateRestrcitionDate.setSeconds(00);
			ticketDateRestrcitionDate.getTimezoneOffset();
			if(dbcompaintDate.getValue().before(ticketDateRestrcitionDate)){
				dbcompaintDate.setValue(null);
				dbDueDate.setValue(null);
				dbNewServiceDate.setValue(null);
				showDialogMessage("Ticket Date should not be on or before "+AppUtility.parseDate(ticketDateRestrcitionDate));
				return;
			}
		}
		String complaintDueDate = AppUtility.getForProcessConfigurartionIsActiveOrNot("EnableComplaintDueDate");
		if(complaintDueDate!=null){
			int dueDateDays = Integer.parseInt(complaintDueDate);
			Date dueDate = dbcompaintDate.getValue();
			CalendarUtil.addDaysToDate(dueDate, dueDateDays);
			dbDueDate.setValue(dueDate);
		}
		if(dbcompaintDate.getValue()!=null && !otherComplaintFlag){
			Date ticketDate = dbcompaintDate.getValue();
			CalendarUtil.addDaysToDate(ticketDate, 2);
			dbNewServiceDate.setValue(ticketDate);
		}
		
		
	}
	
	public void reactOnAdd()
	{
		List<ComplainList> complainLis=this.activitytable.getDataprovider().getList();
		String remark=taRemark.getValue();
		Date date=new Date();
		
		DateTimeFormat dtf = DateTimeFormat.getFormat("HH:mm:ss");
		System.err.println(dtf.format(date).toString());
	
		
	     String time=new String(dtf.format(date).toString());
		
		ComplainList complist=new ComplainList();
		complist.setSrNo(complainLis.size()+1);
		complist.setDate(date);
		complist.setTime(time);
		complist.setRemark(remark);
		
		if(LoginPresenter.loggedInUser!=null){
			complist.setUserName(LoginPresenter.loggedInUser);
		}
		
		List<ComplainList>list=activitytable.getDataprovider().getList();
			list.add(complist);
	}

	@Override
	public boolean validate() {
		boolean supervalue= super.validate();
		boolean formfields1 =validateform(this.formfileds);
		boolean formfields2 =validateform(this.formfiled2);
		boolean formfields3 =validateform(this.formfiled3);
		
		if(activitytable.getDataprovider().getList().size()==0){
			showDialogMessage("Ticket history remark is mandatory!");
			return false;
		}
		
//		boolean serviceAddressFlag = true;
//		if(olbcustomerBranch.getSelectedIndex()!=0){
//			if(olbcustomerBranch.getValue().equals("Service Address") && olbCity.getSelectedIndex()==0){
//				showDialogMessage("City is mandatory!");
//				serviceAddressFlag =false;
//			}
//		}

		return supervalue && formfields1 && formfields2 && formfields3;
	}

	private boolean validateform(FormField[][] formfileds) {
		boolean result=true;
		int row=formfileds.length;
		
		for(int k=0;k<row;k++)
		{
			int column=formfileds[k].length;
			
			for(int i=0;i<column;i++)
			{
				Widget widg=formfileds[k][i].getWidget();
				
				if(formfileds[k][i].getLabel()!=null&&formfileds[k][i].getLabel().contains("Email"))
				{
					TextBox tb=(TextBox) widg;
					String value=tb.getText();
					
					boolean result1=validateEmail(value.trim());

					if(result1==false)
				    {
				    	if(tb.getText().trim().equals("")==false)
				    	{
				    		result=false;
							changeBorderColor(widg,"#dd4b39");
							
							
							
							InlineLabel l=formfileds[k][i].getMandatoryMsgWidget();
							if(l!=null)
							{
								l.setVisible(true);
								formfileds[k][i].getMandatoryMsgWidget().setText("Invalid Email Format!");
				    	}}
				     }	
				}
				
				if(widg instanceof CompositeInterface)
				{
					
					CompositeInterface a= (CompositeInterface) widg;
					boolean resultadress=a.validate();
					if(resultadress==false)
						result=resultadress;	
				}
				
				if(formfileds[k][i].isMandatory())
				{
					if(widg instanceof TextBox)
					{
						TextBox tb=(TextBox) widg;
						String value=tb.getText();
						
						if(value.equals(""))
						{
							result=false;
							changeBorderColor(widg,"#dd4b39");
							InlineLabel l=formfileds[k][i].getMandatoryMsgWidget();
							if(l!=null)
								l.setVisible(true);
							
						}
						
						
						
					}
					
					
					if(widg instanceof TextArea)
					{
						TextArea tb=(TextArea) widg;
						String value=tb.getText();
						
						if(value.equals(""))
						{
							result=false;
							changeBorderColor(widg,"#dd4b39");
							InlineLabel l=formfileds[k][i].getMandatoryMsgWidget();
							if(l!=null)
								l.setVisible(true);
							
						}
						
						
						
					}
					
					else if(widg instanceof ListBox)
					{
						ListBox lb=(ListBox) widg;
					
						int index=lb.getSelectedIndex();
						if(index==0)
						{
							result=false;
							changeBorderColor(widg,"#dd4b39");
							InlineLabel l=formfileds[k][i].getMandatoryMsgWidget();
							if(l!=null)
								l.setVisible(true);
							
						}
					}
					
					else if(widg instanceof ObjectListBox)
					{
						ListBox lb=(ListBox) widg;
						
						int index=lb.getSelectedIndex();
						if(index==0)
						{
							result=false;
							changeBorderColor(widg,"#dd4b39");
							InlineLabel l=formfileds[k][i].getMandatoryMsgWidget();
							if(l!=null)
								l.setVisible(true);
							
						}
					}
					
					else if(widg instanceof DateBox)
					{
						DateBox db=(DateBox) widg;
						Date value=db.getValue();
						if(value==null)
						{
							result=false;
							changeBorderColor(widg,"#dd4b39");
							InlineLabel l=formfileds[k][i].getMandatoryMsgWidget();
							if(l!=null)
								l.setVisible(true);
							
						}
					}
					
					if(widg instanceof DoubleBox)
					{
						DoubleBox db=(DoubleBox) widg;
						String value=db.getText().trim();
						if(value.equals(""))
						{
							result=false;
							changeBorderColor(widg,"#dd4b39");
							InlineLabel l=formfileds[k][i].getMandatoryMsgWidget();
							if(l!=null)
								l.setVisible(true);
							
						}
					}
					
					if(widg instanceof SuggestBox)
					{
						SuggestBox sb=(SuggestBox) widg;
						String value=sb.getValue();
						
						if(value.equals(""))
						{
							result=false;
							changeBorderColor(widg,"#dd4b39");
							InlineLabel l=formfileds[k][i].getMandatoryMsgWidget();
							if(l!=null)
								l.setVisible(true);
						}
					}
					
					if(widg instanceof LongBox)
					{
						LongBox sb=(LongBox) widg;
						Long value=sb.getValue();
						if(value==null)
						{
							result=false;
							changeBorderColor(widg,"#dd4b39");
							InlineLabel l=formfileds[k][i].getMandatoryMsgWidget();
							if(l!=null)
								l.setVisible(true);
						}
					}
				
					if(widg instanceof MyLongBox)
					{
						MyLongBox sb=(MyLongBox) widg;
						
						Long value=sb.getValue();
						if(value==null)
						{
							result=false;
							changeBorderColor(widg,"#dd4b39");
							InlineLabel l=formfileds[k][i].getMandatoryMsgWidget();
							if(l!=null)
								l.setVisible(true);
						}
					}
					
					
					if(widg instanceof IntegerBox)
					{
						IntegerBox sb=(IntegerBox) widg;
						
						Integer value=sb.getValue();
						if(value==null)
						{
							result=false;
							changeBorderColor(widg,"#dd4b39");
							InlineLabel l=formfileds[k][i].getMandatoryMsgWidget();
							if(l!=null)
								l.setVisible(true);
						}
					}
				}
			}
		}
		return result;
	}

	public PersonInfoComposite getPersoninfoComposite() {
		return personinfoComposite;
	}

	public void setPersoninfoComposite(PersonInfoComposite personinfoComposite) {
		this.personinfoComposite = personinfoComposite;
	}

	public TabPanel getTabPanel() {
		return tabPanel;
	}

	public void setTabPanel(TabPanel tabPanel) {
		this.tabPanel = tabPanel;
	}

	public TextBox getTicketNo() {
		return ticketNo;
	}

	public void setTicketNo(TextBox ticketNo) {
		this.ticketNo = ticketNo;
	}

	public DateBox getDbCreationDate() {
		return dbCreationDate;
	}

	public void setDbCreationDate(DateBox dbCreationDate) {
		this.dbCreationDate = dbCreationDate;
	}

	public TextBox getTbtime() {
		return tbtime;
	}

	public void setTbtime(TextBox tbtime) {
		this.tbtime = tbtime;
	}

	public TextBox getTbCallerName() {
		return tbCallerName;
	}

	public void setTbCallerName(TextBox tbCallerName) {
		this.tbCallerName = tbCallerName;
	}

	public PhoneNumberBox getPnbCallerCellNo() {
		return pnbCallerCellNo;
	}

	public void setPnbCallerCellNo(PhoneNumberBox pnbCallerCellNo) {
		this.pnbCallerCellNo = pnbCallerCellNo;
	}

	public EmailTextBox getEtbEmail() {
		return etbEmail;
	}

	public void setEtbEmail(EmailTextBox etbEmail) {
		this.etbEmail = etbEmail;
	}

	public TextBox getTbStatus() {
		return tbStatus;
	}

	public void setTbStatus(TextBox tbStatus) {
		this.tbStatus = tbStatus;
	}

	public ObjectListBox<Employee> getOlbeSalesPerson() {
		return olbeSalesPerson;
	}

	public void setOlbeSalesPerson(ObjectListBox<Employee> olbeSalesPerson) {
		this.olbeSalesPerson = olbeSalesPerson;
	}

	public ObjectListBox<Employee> getOlbassignto() {
		return olbassignto;
	}

	public void setOlbassignto(ObjectListBox<Employee> olbassignto) {
		this.olbassignto = olbassignto;
	}

	public ObjectListBox<Branch> getOlbbBranch() {
		return olbbBranch;
	}

	public void setOlbbBranch(ObjectListBox<Branch> olbbBranch) {
		this.olbbBranch = olbbBranch;
	}

	public ObjectListBox<Config> getOlbcPriority() {
		return olbcPriority;
	}

	public void setOlbcPriority(ObjectListBox<Config> olbcPriority) {
		this.olbcPriority = olbcPriority;
	}

	public DateBox getDbDueDate() {
		return dbDueDate;
	}

	public void setDbDueDate(DateBox dbDueDate) {
		this.dbDueDate = dbDueDate;
	}

	public ObjectListBox<ConfigCategory> getOlbTicketCategory() {
		return olbTicketCategory;
	}

	public void setOlbTicketCategory(ObjectListBox<ConfigCategory> olbTicketCategory) {
		this.olbTicketCategory = olbTicketCategory;
	}

	public DateBox getDbcompaintDate() {
		return dbcompaintDate;
	}

	public void setDbcompaintDate(DateBox dbcompaintDate) {
		this.dbcompaintDate = dbcompaintDate;
	}

	public DateBox getDbNewServiceDate() {
		return dbNewServiceDate;
	}

	public void setDbNewServiceDate(DateBox dbNewServiceDate) {
		this.dbNewServiceDate = dbNewServiceDate;
	}

	public TextArea getTaDescription() {
		return taDescription;
	}

	public void setTaDescription(TextArea taDescription) {
		this.taDescription = taDescription;
	}

	public TextArea getTaRemark() {
		return taRemark;
	}

	public void setTaRemark(TextArea taRemark) {
		this.taRemark = taRemark;
	}

	public ObjectListBox<CustomerBranchDetails> getOlbcustomerBranch() {
		return olbcustomerBranch;
	}

	public void setOlbcustomerBranch(ObjectListBox<CustomerBranchDetails> olbcustomerBranch) {
		this.olbcustomerBranch = olbcustomerBranch;
	}

	public ServiceDetailsTable getLastServicedetailsTable() {
		return lastServicedetailsTable;
	}

	public void setLastServicedetailsTable(ServiceDetailsTable lastServicedetailsTable) {
		this.lastServicedetailsTable = lastServicedetailsTable;
	}

	public ProductInfoComposite getProdInfoComposite() {
		return prodInfoComposite;
	}

	public void setProdInfoComposite(ProductInfoComposite prodInfoComposite) {
		this.prodInfoComposite = prodInfoComposite;
	}

	public TextBox getTbBrandName() {
		return tbBrandName;
	}

	public void setTbBrandName(TextBox tbBrandName) {
		this.tbBrandName = tbBrandName;
	}

	public TextBox getTbModel() {
		return tbModel;
	}

	public void setTbModel(TextBox tbModel) {
		this.tbModel = tbModel;
	}

	public TextBox getTbSerialNumber() {
		return tbSerialNumber;
	}

	public void setTbSerialNumber(TextBox tbSerialNumber) {
		this.tbSerialNumber = tbSerialNumber;
	}

	public TextBox getTbProSerialNo() {
		return tbProSerialNo;
	}

	public void setTbProSerialNo(TextBox tbProSerialNo) {
		this.tbProSerialNo = tbProSerialNo;
	}

	public ActivityTable getActivitytable() {
		return activitytable;
	}

	public void setActivitytable(ActivityTable activitytable) {
		this.activitytable = activitytable;
	}

	public Button getBtnadd() {
		return btnadd;
	}

	public void setBtnadd(Button btnadd) {
		this.btnadd = btnadd;
	}
	
	public void updateModel(Complain complain){
		complain.setCompanyId(complain.getCompanyId());
		complain.setPersoninfo(this.getPersoninfoComposite().getValue());
		complain.setCompStatus(this.tbStatus.getValue());
	    complain.setCallerName(this.tbCallerName.getValue());
	    if(this.pnbCallerCellNo.getValue()!=null)
	    complain.setCallerCellNo(this.pnbCallerCellNo.getValue());
	    complain.setEmail(this.etbEmail.getValue());
	    if(this.olbeSalesPerson.getSelectedIndex()!=0)
	    complain.setSalesPerson(this.olbeSalesPerson.getValue());
	    if(this.olbbBranch.getSelectedIndex()!=0)
	    complain.setBranch(this.olbbBranch.getValue());
	    if(this.olbcPriority.getSelectedIndex()!=0)
	    complain.setPriority(this.olbcPriority.getValue());
	    if(this.olbassignto.getSelectedIndex()!=0)
	    complain.setAssignto(this.olbassignto.getValue());
	    complain.setDueDate(this.dbDueDate.getValue());
	    if(this.olbTicketCategory.getSelectedIndex()!=0){
		    complain.setCategory(this.olbTicketCategory.getValue());
	    }
	    if(this.dbcompaintDate.getValue()!=null)
	    complain.setComplainDate(this.dbcompaintDate.getValue());
	    if(this.dbNewServiceDate.getValue()!=null)
	    complain.setServiceDate(this.dbNewServiceDate.getValue());
	    if(this.olbcustomerBranch.getSelectedIndex()!=0)
	    complain.setCustomerBranch(this.olbcustomerBranch.getValue());
	    complain.setDestription(this.taDescription.getValue());
	    List<ServiceDetails> lastservicedetailslist = this.lastServicedetailsTable.getDataprovider().getList();
	    if(lastservicedetailslist.size()!=0){
			ArrayList<ServiceDetails> servicelist = new ArrayList<ServiceDetails>();
			servicelist.addAll(lastservicedetailslist);
			complain.setServicedetails(servicelist);
			if (servicelist.size() != 0) {
				complain.setExistingServiceId(servicelist.get(0).getServiceId());
				complain.setExistingContractId(servicelist.get(0).getContractId());
			}
	    }
	    
	    if(this.prodInfoComposite.getValue()!=null)
	    complain.setPic(this.prodInfoComposite.getValue());
	    if(this.tbBrandName.getValue()!=null)
	    complain.setBrandName(this.tbBrandName.getValue());
	    complain.setModelNumber(this.tbModel.getValue());
	    complain.setProSerialNo(this.tbProSerialNo.getValue());
	    List<ComplainList> complaintlist = this.activitytable.getDataprovider().getList();
	    if(complaintlist.size()!=0){
			ArrayList<ComplainList> complaintarray = new ArrayList<ComplainList>();
			complaintarray.addAll(complaintlist);
			complain.setComplainList(complaintarray);
	    }
	   
	    complain.setBbillable(false);
//	    if(this.olbCity.getSelectedIndex()!=0){
//		    complain.setCity(this.olbCity.getValue());
//	    }
	    genasync.save(complain, new AsyncCallback<ReturnFromServer>() {
			
			@Override
			public void onSuccess(ReturnFromServer result) {
				// TODO Auto-generated method stub
				showDialogMessage("Data Saved Successfully!");
				hidePopUp();
			}

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				hidePopUp();
			}
		});
	}
		
	public void updateView(Complain complainEntity){
		this.getPersoninfoComposite().setValue(complainEntity.getPersoninfo());
//		AppUtility.makeCustomerBranchLive(complainEntity.getPersoninfo(),this.getOlbcustomerBranch());
		if(complainEntity.getServicedetails()!=null)
		this.getLastServicedetailsTable().getDataprovider().setList(complainEntity.getServicedetails());
		if(complainEntity.getTime()!=null)
		this.getTbtime().setValue(complainEntity.getTime());
		if(complainEntity.getPic()!=null)
		this.getProdInfoComposite().setValue(complainEntity.getPic());
		if(complainEntity.getCompStatus()!=null)
		this.getTbStatus().setValue(complainEntity.getCompStatus());
		this.getTicketNo().setValue(complainEntity.getCount()+"");
		this.getTbtime().setValue(complainEntity.getTime());
		if(complainEntity.getDate()!=null)
		this.getDbCreationDate().setValue(complainEntity.getDate());
		this.getTbCallerName().setValue(complainEntity.getCallerName());
		this.getPnbCallerCellNo().setValue(complainEntity.getCallerCellNo());
		this.getEtbEmail().setValue(complainEntity.getEmail());
		this.getOlbeSalesPerson().setValue(complainEntity.getSalesPerson());
		this.getOlbbBranch().setValue(complainEntity.getBranch());
		this.getOlbcPriority().setValue(complainEntity.getPriority());
		this.getOlbassignto().setValue(complainEntity.getAssignto());
		if(complainEntity.getDueDate()!=null)
		this.getDbDueDate().setValue(complainEntity.getDueDate());
		this.getOlbTicketCategory().setValue(complainEntity.getCategory());
		if(complainEntity.getComplainDate()!=null)
		this.getDbcompaintDate().setValue(complainEntity.getComplainDate());
		if(complainEntity.getNewServiceDate()!=null)
		this.getDbNewServiceDate().setValue(complainEntity.getNewServiceDate());
		if(complainEntity.getCustomerBranch()!=null)
		this.getOlbcustomerBranch().setValue(complainEntity.getCustomerBranch());
		this.getTaDescription().setValue(complainEntity.getDestription());
		this.getTbBrandName().setValue(complainEntity.getBrandName());
		this.getTbModel().setValue(complainEntity.getModelNumber());
		this.getTbProSerialNo().setValue(complainEntity.getProSerialNo());
		this.getTaRemark().setValue(complainEntity.getRemark());
		this.getActivitytable().getDataprovider().setList(complainEntity.getComplainList());
		System.out.println("complaint remrk history == "+complainEntity.getComplainList().size());
        Console.log("complaint remrk history == "+complainEntity.getComplainList().size());
	}
	
	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		this.tbStatus.setEnabled(false);
		this.getTicketNo().setEnabled(false);
		this.getTbtime().setEnabled(false);
		this.getDbCreationDate().setEnabled(false);
		this.getDbNewServiceDate().setEnabled(false);
		
		setEnabled(state,this.formfileds);
		setEnabled(state,this.formfiled2);
		setEnabled(state,this.formfiled3);
	}

	
	public void setEnabled(boolean status, FormField[][] formfields)
	{
		int row=formfields.length;
		for(int k=0;k<row;k++)
		{
			int column=formfields[k].length;
			for(int i=0;i<column;i++)
			{
				Widget widg=formfields[k][i].getWidget();
				if(widg instanceof TextBox)
				{
					TextBox tb=(TextBox) widg;
					tb.setEnabled(status);
				}
				
				else if(widg instanceof ListBox)
				{
					ListBox lb=(ListBox) widg;
					lb.setEnabled(status);
				}
				
				else if(widg instanceof SuggestBox)
				{
					SuggestBox sb=(SuggestBox) widg;
					sb.setEnabled(status);
				}
			
				else if(widg instanceof LongBox)
				{
					LongBox lb=(LongBox) widg;
					lb.setEnabled(status);
				}
				
				else if(widg instanceof MyLongBox)
				{
					MyLongBox mb=(MyLongBox) widg;
					mb.setEnabled(status);
				}
				
				else if(widg instanceof IntegerBox)
				{
					IntegerBox sb=(IntegerBox) widg;
					sb.setEnabled(status);
				}
				
				else if(widg instanceof DoubleBox)
				{
					DoubleBox sb=(DoubleBox) widg;
					sb.setEnabled(status);
				}
				
				else if(widg instanceof TextArea)
				{
					TextArea sb=(TextArea) widg;
					sb.setEnabled(status);
				}
				
				
				
				else if(widg instanceof CheckBox)
				{
					CheckBox c=(CheckBox) widg;
					c.setEnabled(status);
				}
				
			
				else if(widg instanceof DateBox)
				{
					DateBox comp=(DateBox) widg;    
					comp.setEnabled(status);
				}
				
				
				
				
				else if(widg instanceof Button)
				{
					Button comp=(Button) widg;
					comp.setEnabled(status);
				}
				
				else if(widg instanceof CompositeInterface)
				{
					CompositeInterface comp=(CompositeInterface) widg;
					comp.setEnable(status);
				}
				
				
				
			}
		}
	}

	public InlineLabel getLblUpdate() {
		return lblUpdate;
	}

	public void setLblUpdate(InlineLabel lblUpdate) {
		this.lblUpdate = lblUpdate;
	}

	public InlineLabel getLblbtnSave() {
		return lblbtnSave;
	}

	public void setLblbtnSave(InlineLabel lblbtnSave) {
		this.lblbtnSave = lblbtnSave;
	}
	public void clear(){
		this.getPersoninfoComposite().clear();
		this.getTicketNo().setValue("");
		this.getTbtime().setValue("");
		this.getDbCreationDate().setValue(null);
		this.getTbCallerName().setValue("");
		this.getPnbCallerCellNo().setValue(null);
		this.getEtbEmail().setValue("");
		this.getOlbeSalesPerson().setSelectedIndex(0);
		this.getOlbbBranch().setSelectedIndex(0);
		this.getOlbcPriority().setSelectedIndex(0);
		this.getOlbassignto().setSelectedIndex(0);
		this.getDbDueDate().setValue(null);
		this.getOlbTicketCategory().setSelectedIndex(0);
		this.getDbcompaintDate().setValue(null);
		this.getDbNewServiceDate().setValue(null);
		this.getOlbcustomerBranch().setSelectedIndex(0);
		this.getTaDescription().setValue("");
		this.getTbBrandName().setValue("");
		this.getTbModel().setValue("");
		this.getTbProSerialNo().setValue("");
	}

	public InlineLabel getLblmarkComplete() {
		return lblmarkComplete;
	}

	public void setLblmarkComplete(InlineLabel lblmarkComplete) {
		this.lblmarkComplete = lblmarkComplete;
	}
	
}
