package com.slicktechnologies.client.views.customersupport;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import org.apache.poi.hssf.record.HideObjRecord;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CustomerNameChangeService;
import com.slicktechnologies.client.services.CustomerNameChangeServiceAsync;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.complain.ComplainPresenter;
import com.slicktechnologies.client.views.complain.ComplainPresenterSearchProxy;
import com.slicktechnologies.client.views.complain.ComplainPresenterTableProxy;
import com.slicktechnologies.client.views.complain.ComplainPresenter.ComplainPresenterTable;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.complain.Complain;
import com.slicktechnologies.shared.common.complain.ComplainList;
import com.slicktechnologies.shared.common.customersupport.ServiceDetails;
import com.slicktechnologies.shared.common.customersupport.TechnicianInfo;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.helperlayer.ProductInfo;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;

public class CustomerSupportPresenter extends FormScreenPresenter<Complain> implements CellClickHandler {

	static CustomerSupportForm form;
	
	int position;
	Service serviceEntity=null;
	ListGridRecord selectedRecord=null;
	ArrayList<Service> globalServiceList=new ArrayList<Service>();
	final GenricServiceAsync genasync = GWT.create(GenricService.class);
	GeneralServiceAsync generalservice = GWT.create(GeneralService.class);

	CustomerSupportPopup complaintpopup = new CustomerSupportPopup();
	
	CustomerSupportPopup otherComplaint = new CustomerSupportPopup(true);

	ServiceProduct serviceProdEntity=new ServiceProduct();
	
	CustomerNameChangeServiceAsync complaintService = GWT.create(CustomerNameChangeService.class);
	
	public CustomerSupportPresenter(FormScreen<Complain> view, Complain model) {
		super(view, model);
		form = (CustomerSupportForm) view;
		form.setPresenter(this);
		form.grid.addCellClickHandler(this);
		form.btnGo.addClickHandler(this);
		
		complaintpopup.getLblCancel().addClickHandler(this);
		complaintpopup.getLblOk().addClickHandler(this);
		
		form.radioBtnServiceComplaint.addClickHandler(this);
		form.radioBtnOtherComplaint.addClickHandler(this);
		form.btnRegisterComplaint.addClickHandler(this);
		
		otherComplaint.getLblCancel().addClickHandler(this);
		otherComplaint.getLblOk().addClickHandler(this);

	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void makeNewModel() {
		// TODO Auto-generated method stub
		model = new  Complain();
	}

	public static CustomerSupportForm initialize() {
		CustomerSupportForm form = new CustomerSupportForm();
		ComplainPresenterTable gentableSearch = new ComplainPresenterTableProxy();
		gentableSearch.setView(form);
		gentableSearch.applySelectionModle();
		ComplainPresenterSearchProxy.staticSuperTable = gentableSearch;
		ComplainPresenterSearchProxy searchpopup = new ComplainPresenterSearchProxy();
		form.setSearchpopupscreen(searchpopup);

		CustomerSupportPresenter presenter = new CustomerSupportPresenter(form,new Complain());
		AppMemory.getAppMemory().stickPnel(form);
		
		return form;
	}

	@Override
	public void onCellClick(CellClickEvent event) {

		ListGridRecord record = event.getRecord();
		int colNum = event.getColNum();
		ListGridField field = form.grid.getField(colNum);
		String fieldName = form.grid.getFieldName(colNum);
		String fieldTitle = field.getTitle();
		position=event.getRowNum();
		System.out.println("Clicked " + fieldTitle + ":"+ record.getAttribute(fieldName) + "  Lead Id :: "+ record.getAttribute("leadId")+"POSITION "+position);
		serviceEntity = getServiceDetails((Integer.parseInt(record.getAttribute("ServiceId").trim())));
		if (fieldName.equals("RegisterComplaint")) {
			reactonRegisterComplaint(serviceEntity);
		}
		else if (fieldName.equals("Technician")) {

		}
		else{
//			mapToOtherInfo();
		}
	}
	
	private void reactonRegisterComplaint(Service serviceEntity2) {
		
		 getLastServiceDetails(serviceEntity);
	}

	

	private Service getServiceDetails(int serviceId) {
		for(Service object:globalServiceList){
			if(object.getCount()==serviceId){
				return object;
			}
		}
		return null;
	}

	@Override
	public void onClick(ClickEvent event) {
		if(event.getSource()==form.btnGo){
			reactOnGO();
		}
		if(event.getSource()==complaintpopup.getLblOk()){
			complaintpopup.getLblOk().setVisible(false);
			if(complaintpopup.validate()){
				reactOnOK(complaintpopup,true);
			}else{
				complaintpopup.getLblOk().setVisible(true);
			}
		}
		
		if(event.getSource()==complaintpopup.getLblCancel()){
			complaintpopup.hidePopUp();
		}
		
		if(event.getSource()==form.radioBtnServiceComplaint){
			setenableBasedonComplaintType(true);
			form.radioBtnOtherComplaint.setValue(false);
		}
		if(event.getSource()==form.radioBtnOtherComplaint){
			form.personinfoComposite.clear();
			form.dateComparator.clear();
			form.olbBranch.setSelectedIndex(0);
			setenableBasedonComplaintType(false);
			form.radioBtnServiceComplaint.setValue(false);
			ArrayList<Service> list = new ArrayList<Service>();
			form.setDataToListGrid(list);
		}
		if(event.getSource()==form.btnRegisterComplaint){
			 getLastServiceDetails(null);
		}
		/** Date 13-08-2019 by vijay for Other complaint other than service complaint for NBHC CCPM ***/
		if(event.getSource()==otherComplaint.getLblOk()){
			if(otherComplaint.validate()){
				reactOnOK(otherComplaint,false);
			}
		}
		
		if(event.getSource()==otherComplaint.getLblCancel()){
			otherComplaint.hidePopUp();
		}
	}
	
	private void setenableBasedonComplaintType(boolean flag) {
		form.btnRegisterComplaint.setEnabled(!flag);
		form.personinfoComposite.setEnable(flag);
		form.btnGo.setEnabled(flag);
		form.olbBranch.setEnabled(flag);
		form.dateComparator.setEnable(flag);
	}
	
	private void reactOnOK(final CustomerSupportPopup complaint, final boolean serviceComplaintFlag) {
		final Complain complain = new Complain();
		complain.setCompanyId(model.getCompanyId());
		complain.setPersoninfo(complaint.personinfoComposite.getValue());
		complain.setCompStatus(complaint.tbStatus.getValue());
	    complain.setCreatedBy(LoginPresenter.loggedInUser);
	    complain.setDate(complaint.dbCreationDate.getValue());
	    complain.setCallerName(complaint.tbCallerName.getValue());
	    if(complaint.pnbCallerCellNo.getValue()!=null)
	    complain.setCallerCellNo(complaint.pnbCallerCellNo.getValue());
	    complain.setEmail(complaint.etbEmail.getValue());
	    if(complaint.olbeSalesPerson.getSelectedIndex()!=0)
	    complain.setSalesPerson(complaint.olbeSalesPerson.getValue());
	    if(complaint.olbbBranch.getSelectedIndex()!=0)
	    complain.setBranch(complaint.olbbBranch.getValue());
	    if(complaint.olbcPriority.getSelectedIndex()!=0)
	    complain.setPriority(complaint.olbcPriority.getValue());
	    if(complaint.olbassignto.getSelectedIndex()!=0)
	    complain.setAssignto(complaint.olbassignto.getValue());
	    complain.setDueDate(complaint.dbDueDate.getValue());
	    if(complaint.olbTicketCategory.getSelectedIndex()!=0){
		    complain.setCategory(complaint.olbTicketCategory.getValue());
	    }
	    complain.setComplainDate(complaint.dbcompaintDate.getValue());
	    complain.setNewServiceDate(complaint.dbNewServiceDate.getValue());
	    if(complaint.olbcustomerBranch.getSelectedIndex()!=0)
	    complain.setCustomerBranch(complaint.olbcustomerBranch.getValue());
	    complain.setDestription(complaint.taDescription.getValue());
	    List<ServiceDetails> lastservicedetailslist = complaint.lastServicedetailsTable.getDataprovider().getList();
	    if(lastservicedetailslist.size()!=0){
	    	   ArrayList<ServiceDetails> servicelist =  new ArrayList<ServiceDetails>();
	   	    servicelist.addAll(lastservicedetailslist);
	   	    complain.setServicedetails(servicelist);
	   	    if(servicelist.size()!=0){
	   	    	System.out.println("service Id ="+servicelist.get(0).getServiceId());
	   	    	System.out.println("service Id ="+servicelist.get(0).getContractId());
	   	    	complain.setExistingServiceId(servicelist.get(0).getServiceId());
	   	    	complain.setExistingContractId(servicelist.get(0).getContractId());
	   	    }
	    }
	 
	    if(complaint.prodInfoComposite.getValue()!=null)
	    complain.setPic(complaint.prodInfoComposite.getValue());
	    if(complaint.tbBrandName.getValue()!=null)
	    complain.setBrandName(complaint.tbBrandName.getValue());
	    complain.setModelNumber(complaint.tbModel.getValue());
	    complain.setProSerialNo(complaint.tbProSerialNo.getValue());
	    List<ComplainList> complaintlist = complaint.activitytable.getDataprovider().getList();
	    ArrayList<ComplainList> complaintarray = new ArrayList<ComplainList>();
	    complaintarray.addAll(complaintlist);
	    complain.setComplainList(complaintarray);
	    complain.setBbillable(false);
	    if(complaint.getTbtime().getValue()!=null && !complaint.getTbtime().getValue().equals("")){
		    complain.setTime(complaint.getTbtime().getValue());
	    }
	    
	    if(serviceComplaintFlag){
	    	 complaintService.createServiceAssesmentForComplain(complain, new AsyncCallback<String>() {
	 			
	 			@Override
	 			public void onSuccess(String result) {
	 				// TODO Auto-generated method stub
	 				Console.log("hide popup");
	 				complaintpopup.showDialogMessage(result);
	 				complaintpopup.hidePopUp();
	 				complaintpopup.getLblOk().setVisible(true);

	 			}
	 			
	 			@Override
	 			public void onFailure(Throwable caught) {
	 				// TODO Auto-generated method stub
	 				complaintpopup.showDialogMessage(caught.getMessage());	
	 				complaintpopup.hidePopUp();
	 				complaintpopup.getLblOk().setVisible(true);
	 			}
	 		});
	 	   
	    	
	    }
	    else{
	    	 genasync.save(complain, new AsyncCallback<ReturnFromServer>() {
	 			
	 			@Override
	 			public void onSuccess(ReturnFromServer result) {
	 				// TODO Auto-generated method stub
	 				complaint.showDialogMessage("Data Saved Successfully!");
	 				complaint.hidePopUp();
	 			}

	 			@Override
	 			public void onFailure(Throwable caught) {
	 				// TODO Auto-generated method stub
	 				complaint.hidePopUp();
	 			}
	 		});
	 	    
	    	
	    }
	    
	}


	private void reactonCreateService(Complain complain, int complaintId) {
		
		complaintService.createComplaintService(complain, complaintId, new AsyncCallback<String>() {
			
			@Override
			public void onSuccess(String result) {
				// TODO Auto-generated method stub
				Console.log("Service Created Sucessfully");
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				Console.log("Service creation failed");
			}
		});
		
	}
	
	private void reactOnGO() {

		MyQuerry querry = new MyQuerry();
		Vector<Filter> filterVec = new Vector<Filter>();
		Filter filter = null;
		
		if(form.personinfoComposite.getIdValue()!=-1)
		{
			System.out.println("CUSTOMER ID NOT NULL ::: "+form.personinfoComposite.getIdValue());
			filter=new Filter();
			filter.setIntValue(form.personinfoComposite.getIdValue());
			filter.setQuerryString("personInfo.count");
			filterVec.add(filter);
		}
		  
		if(!(form.personinfoComposite.getFullNameValue().equals("")))
		{
			System.out.println("CUSTOMER NAME NOT NULL "+form.personinfoComposite.getFullNameValue());
			filter=new Filter();
			filter.setStringValue(form.personinfoComposite.getFullNameValue());
			filter.setQuerryString("personInfo.fullName");
			filterVec.add(filter);
		}
		if(form.personinfoComposite.getCellValue()!=-1l)
		{
			System.out.println("CUSTOMER CELL NOT NULL "+form.personinfoComposite.getCellValue());
			filter=new Filter();
			filter.setLongValue(form.personinfoComposite.getCellValue());
			filter.setQuerryString("personInfo.cellNumber");
			filterVec.add(filter);
		}
		
		if(form.olbBranch.getSelectedIndex()!=0){
			filter = new Filter();
			filter.setQuerryString("branch");
			filter.setStringValue(form.olbBranch.getValue());
			filterVec.add(filter);
		}
		if(form.dateComparator.getValue()!=null){
			filterVec.addAll(form.dateComparator.getValue());
		}
		
		if(form.tbServiceId.getValue()!=null && !form.tbServiceId.getValue().equals("")){

			filter = new Filter();
			filter.setQuerryString("count");
			filter.setIntValue(Integer.parseInt(form.tbServiceId.getValue()));
			filterVec.add(filter);
		}
		
		filter = new Filter();
		filter.setQuerryString("status");
		filter.setStringValue(Service.SERVICESTATUSCOMPLETED);
		filterVec.add(filter);
		
		querry.setFilters(filterVec);
		querry.setQuerryObject(new Service());
		form.showWaitSymbol();
		
		genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				globalServiceList = new ArrayList<Service>();
				for(SuperModel model : result){
					Service service = (Service) model;
					globalServiceList.add(service);
				}
				if(globalServiceList.size()!=0){
					form.setDataToListGrid(globalServiceList);
				}
				else{
					form.grid.clear();
				}
				form.hideWaitSymbol();
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				form.hideWaitSymbol();
			}
		});
		
	}

	
	private void getLastServiceDetails(final Service serviceEntity2) {
		if(serviceEntity2!=null){
			MyQuerry querry = getServiceProjectDetails(serviceEntity2);
			genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					ArrayList<ServiceDetails> servicedetailslist = new ArrayList<ServiceDetails>();
					if(result.size()!=0){
						for(SuperModel model : result){
							ServiceProject project = (ServiceProject) model;
						
							ServiceDetails servicedeatils = new ServiceDetails();
							servicedeatils.setServiceDate(serviceEntity2.getServiceDate());
							servicedeatils.setServiceId(serviceEntity2.getCount());
							if(serviceEntity2.getServiceTime()!=null){
								servicedeatils.setServiceTime(serviceEntity2.getServiceTime());
							}
							servicedeatils.setBranch(serviceEntity2.getBranch());
							servicedeatils.setContractId(serviceEntity2.getContractCount());
//							if(project.getTechnicians()!=null && project.getTechnicians().size()!=0){
//								ArrayList<EmployeeInfo> technicianlist = new ArrayList<EmployeeInfo>();
//								List<EmployeeInfo> employeelist =project.getTechnicians();
//								technicianlist.addAll(employeelist);
//								HashMap<Integer, ArrayList<EmployeeInfo>> technicianhashmap = new HashMap<Integer, ArrayList<EmployeeInfo>>();
//								technicianhashmap.put(serviceEntity2.getCount(), technicianlist);
//								servicedeatils.setTechnicianlist(technicianhashmap);
//							}
							
							if(project.getTechnicians()!=null && project.getTechnicians().size()!=0){
								ArrayList<TechnicianInfo> technicianlist = new ArrayList<TechnicianInfo>();
								List<TechnicianInfo> employeelist = getTechnician(project.getTechnicians());
								technicianlist.addAll(employeelist);
								HashMap<Integer, ArrayList<TechnicianInfo>> technicianhashmap = new HashMap<Integer, ArrayList<TechnicianInfo>>();
								technicianhashmap.put(serviceEntity2.getCount(), technicianlist);
								servicedeatils.setTechnicians(technicianhashmap);
							}
							
							if(project.getProdDetailsList()!=null && project.getProdDetailsList().size()!=0){
								ArrayList<ProductGroupList> productarrylist = new ArrayList<ProductGroupList>();
								List<ProductGroupList> productlist =project.getProdDetailsList();
								productarrylist.addAll(productlist);
								HashMap<Integer, ArrayList<ProductGroupList>> productgrouphashmap = new HashMap<Integer, ArrayList<ProductGroupList>>();
								productgrouphashmap.put(serviceEntity2.getCount(), productarrylist);
								servicedeatils.setServiceProdGroupList(productgrouphashmap);
							}
							
							servicedetailslist.add(servicedeatils);
							break;
						}
					}
					else{
						ServiceDetails servicedeatils = new ServiceDetails();
						servicedeatils.setServiceDate(serviceEntity2.getServiceDate());
						servicedeatils.setServiceId(serviceEntity2.getCount());
						if(serviceEntity2.getServiceTime()!=null){
							servicedeatils.setServiceTime(serviceEntity2.getServiceTime());
						}
						servicedeatils.setContractId(serviceEntity2.getContractCount());
						servicedeatils.setBranch(serviceEntity2.getBranch());
						servicedetailslist.add(servicedeatils);
					}
					Date date=new Date();
					DateTimeFormat dtf = DateTimeFormat.getFormat("HH:mm:ss");
					DateTimeFormat dtf1 = DateTimeFormat.getFormat("dd/MM/yy");
				    String time=new String(dtf.format(date).toString());
				    complaintpopup.clear();
				    complaintpopup.getActivitytable().getDataprovider().getList().clear();
				    complaintpopup.prodInfoComposite.clear();
				    complaintpopup.showPopUp();
				    complaintpopup.btnMarkComplete.setVisible(false);//vijay
				    complaintpopup.getLblOk().setText("Save");
				    complaintpopup.getPopup().getElement().addClassName("customerSupportPopup");
				    complaintpopup.personinfoComposite.setValue(serviceEntity2.getPersonInfo());
					AppUtility.makeCustomerBranchLive(serviceEntity2.getPersonInfo(),complaintpopup.olbcustomerBranch,serviceEntity2.getServiceBranch());
					complaintpopup.lastServicedetailsTable.getDataprovider().setList(servicedetailslist);
					complaintpopup.tbtime.setValue(time);
					complaintpopup.dbCreationDate.setValue(date);
					complaintpopup.getOlbcustomerBranch().setValue(serviceEntity2.getServiceBranch());
					if(serviceEntity.getProduct()!=null){
						ProductInfo prodInfo=new ProductInfo();
						prodInfo.setProdID(serviceEntity.getProduct().getCount());
						prodInfo.setProductCode(serviceEntity.getProduct().getProductCode());
						prodInfo.setProductName(serviceEntity.getProduct().getProductName());
						prodInfo.setProductCategory(serviceEntity.getProduct().getProductCategory());
						prodInfo.setProductPrice(serviceEntity.getProduct().getPrice());
						complaintpopup.prodInfoComposite.setValue(prodInfo);
					}
					
				}
				
				

				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					complaintpopup.hidePopUp();
				}
			});
		}
		else{
			
			Date date=new Date();
			DateTimeFormat dtf = DateTimeFormat.getFormat("HH:mm:ss");
		    String time=new String(dtf.format(date).toString());
			otherComplaint.clear();
			otherComplaint.showPopUp();
			otherComplaint.btnMarkComplete.setVisible(false);//vijay
			otherComplaint.getLblOk().setText("Save");
			otherComplaint.getPopup().getElement().addClassName("customerSupportPopup");
			otherComplaint.tbtime.setValue(time);
			otherComplaint.dbCreationDate.setValue(date);
			
		}
	
		
	}

	private MyQuerry getServiceProjectDetails(Service serviceEntity) {
		MyQuerry querry = new MyQuerry();
		Vector<Filter> filterVec = new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("serviceId");
		filter.setIntValue(serviceEntity.getCount());
		filterVec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("contractId");
		filter.setIntValue(serviceEntity.getContractCount());
		filterVec.add(filter);
		
		querry.setFilters(filterVec);
		querry.setQuerryObject(new ServiceProject());
		
		return querry;
	}
	
	private List<TechnicianInfo> getTechnician(List<EmployeeInfo> technicians) {
		List<TechnicianInfo> list = new ArrayList<TechnicianInfo>();
		for(EmployeeInfo employee : technicians){
			TechnicianInfo technician = new TechnicianInfo();
			technician.setEmployeeName(employee.getFullName());
			technician.setEmployeeId(employee.getCount());
			technician.setEmpCellNumber(employee.getCellNumber());
			technician.setEmployeeDesignation(employee.getDesignation());
			list.add(technician);
		}
		return list;
	}
}
