package com.slicktechnologies.client.views.customersupport;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.customersupport.ServiceDetails;
import com.slicktechnologies.shared.common.customersupport.TechnicianInfo;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;

public class ServiceDetailsTable extends SuperTable<ServiceDetails>{

	TextColumn<ServiceDetails> columnServiceId;
	TextColumn<ServiceDetails> columnServiceDate;
	TextColumn<ServiceDetails> columnBranch;
	TextColumn<ServiceDetails> columnServiceTime;
	Column<ServiceDetails, String> btncolumnTechnicain;
	TextColumn<ServiceDetails> columnContractId;
	
	@Override
	public void createTable() {
		// TODO Auto-generated method stub
		createColumnServiceID();
		CreateColumnServiceDate();
		CreateColumnServiceTime();
		createColumnBranch();
		createColumnContractId();
		createColumnTechnicianMaterial();
		
	}

	private void createColumnContractId() {
		columnContractId = new TextColumn<ServiceDetails>() {
			
			@Override
			public String getValue(ServiceDetails object) {
				// TODO Auto-generated method stub
				return object.getContractId()+"";
			}
		};
		table.addColumn(columnContractId,"Contract ID");
		table.setColumnWidth(columnContractId,80, Unit.PX);
	}

	private void createColumnServiceID() {
		columnServiceId = new TextColumn<ServiceDetails>() {
			
			@Override
			public String getValue(ServiceDetails object) {
				// TODO Auto-generated method stub
				return object.getServiceId()+"";
			}
		};
		table.addColumn(columnServiceId,"Service ID");
		table.setColumnWidth(columnServiceId,80, Unit.PX);
	}

	private void CreateColumnServiceDate() {
		columnServiceDate = new TextColumn<ServiceDetails>() {
			
			@Override
			public String getValue(ServiceDetails object) {
				System.out.println("Service Date =="+object.getServiceDate());
				if(object.getServiceDate()!=null){
					return AppUtility.parseDate(object.getServiceDate());
				}
				return "NA";
			}
		};
		table.addColumn(columnServiceDate,"Service Date");
		table.setColumnWidth(columnServiceDate,80, Unit.PX);
	}

	private void CreateColumnServiceTime() {
		columnServiceTime = new TextColumn<ServiceDetails>() {
			
			@Override
			public String getValue(ServiceDetails object) {
				return object.getServiceTime();
			}
		};
		table.addColumn(columnServiceTime,"Service Time");
		table.setColumnWidth(columnServiceTime,80, Unit.PX);
	}

	private void createColumnBranch() {
		columnBranch = new TextColumn<ServiceDetails>() {
			
			@Override
			public String getValue(ServiceDetails object) {
				return object.getBranch();
			}
		};
		table.addColumn(columnBranch,"Branch");
		table.setColumnWidth(columnBranch,100, Unit.PX);
	}

	private void createColumnTechnicianMaterial() {
		ButtonCell btnCell = new ButtonCell();
		btncolumnTechnicain = new Column<ServiceDetails, String>(btnCell) {
			
			@Override
			public String getValue(ServiceDetails object) {
				return "Technician/Material";
			}
		};
		table.addColumn(btncolumnTechnicain,"Technician/Material");
		table.setColumnWidth(btncolumnTechnicain,100, Unit.PX);
		
		btncolumnTechnicain.setFieldUpdater(new FieldUpdater<ServiceDetails, String>() {
			
			@Override
			public void update(int index, ServiceDetails object, String value) {
				TechnicianAndMaterialPopUp technoician = new TechnicianAndMaterialPopUp();
				technoician.showPopUp();
				technoician.getPopup().getElement().addClassName("customerSupportPopup");
				technoician.setEnable(false);
				technoician.getLblOk().setVisible(false);
//				technoician.getEmptable().getDataprovider().setList(object.getTechnicianlist());
//				technoician.getMaterialProdTable().getDataprovider().setList(object.getProdDetailsList());
//				ArrayList<EmployeeInfo> technicianlist = object.getTechnicianlist().get(object.getServiceId());
				
//				if(object.getTechnicianlist()!=null && object.getTechnicianlist().size()!=0)
//				technoician.getEmptable().getDataprovider().setList(object.getTechnicianlist().get(object.getServiceId()));
				if(object.getServiceProdGroupList()!=null && object.getServiceProdGroupList().size()!=0)
				technoician.getMaterialProdTable().getDataprovider().setList(object.getServiceProdGroupList().get(object.getServiceId()));
				
				if(object.getTechnicianlist()!=null && object.getTechnicianlist().size()!=0){
					List<TechnicianInfo> employeelist = getTechnician(object.getTechnicianlist().get(object.getServiceId()));
					technoician.getEmpTable().getDataprovider().setList(employeelist);
				}
				else if(object.getTechnicians()!=null && object.getTechnicians().size()!=0){
					technoician.getEmpTable().getDataprovider().setList(object.getTechnicians().get(object.getServiceId()));
				}
			}
		});
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
	
	private List<TechnicianInfo> getTechnician(List<EmployeeInfo> technicians) {
		List<TechnicianInfo> list = new ArrayList<TechnicianInfo>();
		for(EmployeeInfo employee : technicians){
			TechnicianInfo technician = new TechnicianInfo();
			technician.setEmployeeName(employee.getFullName());
			technician.setEmployeeId(employee.getCount());
			technician.setEmpCellNumber(employee.getCellNumber());
			technician.setEmployeeDesignation(employee.getDesignation());
			list.add(technician);
		}
		return list;
	}

	
}
