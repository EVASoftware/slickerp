package com.slicktechnologies.client.views.customersupport;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TabPanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
//import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.simplesoftwares.client.library.mywidgets.PhoneNumberBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.complain.ActivityTable;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.complain.Complain;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.smartgwt.client.core.DataClass;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.DataSourceField;
import com.smartgwt.client.data.fields.DataSourceDateField;
import com.smartgwt.client.data.fields.DataSourceIntegerField;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.AutoFitWidthApproach;
import com.smartgwt.client.types.Autofit;
import com.smartgwt.client.types.FieldType;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;


public class CustomerSupportForm extends FormScreen<Complain> implements ClickHandler{
	
	PersonInfoComposite personinfoComposite;
	ObjectListBox<Branch> olbBranch;
	public DateComparator dateComparator;
	
	Button btnGo,btnRegisterComplaint;
	
	ListGrid grid;
	DataSource ds;
	VerticalPanel verticalpanel;
	RadioButton radioBtnServiceComplaint,radioBtnOtherComplaint;
	
	TextBox tbServiceId;

	public CustomerSupportForm(){
		super(FormStyle.DEFAULT);
//		super();
		createGui();
	}
	
	private void initializeWidget() {
		
		initializeListgrid();
		
		personinfoComposite=AppUtility.customerInfoComposite(new Customer());
		olbBranch = new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbBranch);
		dateComparator= new DateComparator("serviceDate",new Service());
		btnGo = new Button("Go");
		tbServiceId = new TextBox();
	}


	private void initializeListgrid() {
		grid = new ListGrid();
		grid.setStyleName("GridList");
		setGridProperty(grid);
		grid.setFields(getGridFields());
		
		ds= new DataSource();
		ds.setClientOnly(true);
		setDataSourceField(ds);
		grid.setDataSource(ds);
		verticalpanel = new VerticalPanel();
		verticalpanel.getElement().addClassName("varticalGridPanel2");

		grid.getElement().addClassName("technicianSchedule");
		verticalpanel.add(grid);
	}

	@Override
	public void createScreen() {
		initializeWidget();

		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();

		radioBtnServiceComplaint = new RadioButton("Service Complaint");
		radioBtnOtherComplaint = new RadioButton("Other Complaint");
		btnRegisterComplaint = new  Button("Register Complaint");
		btnRegisterComplaint.setEnabled(false);
		
		FormField fgroupingComplaintTypeInformation=fbuilder.setlabel("Complaint Type Information").widgetType(com.simplesoftwares.client.library.FieldType.Grouping).setMandatory(false).setColSpan(5).build();

		fbuilder = new FormFieldBuilder("Service Complaint",radioBtnServiceComplaint);
		FormField fradioBtnServiceComplaint= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Other Complaint",radioBtnOtherComplaint);
		FormField fradioBtnOtherComplaint= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",btnRegisterComplaint);
		FormField fbtnRegisterComplaint= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		FormField fgroupingCustomerInformation=fbuilder.setlabel("Customer Information").widgetType(com.simplesoftwares.client.library.FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		
		fbuilder = new FormFieldBuilder("",personinfoComposite);
		FormField fpersoninfo= fbuilder.setRowSpan(0).setColSpan(5).build();
		
		fbuilder = new FormFieldBuilder("Branch",olbBranch);
		FormField olbBranch= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("From Date (Service Date)",dateComparator.getFromDate());
		FormField fromDate= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("To Date (Service Date)",dateComparator.getToDate());
		FormField toDate= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder(" ",btnGo);
		FormField fbtnGo= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		/** Here i am initializing listgrid again becuase for first initilization there is an one CSS #isc_0 which will apply
		 * to this first listgrid object so my design will overlapping so i have reinitialize listgrid again 
		 */
		initializeListgrid();

		
		fbuilder = new FormFieldBuilder(" ",verticalpanel);
		FormField fverticalpanel= fbuilder.setRowSpan(0).setColSpan(5).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fblankgroup=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(5).build();
		
		fbuilder = new FormFieldBuilder("Service Id",tbServiceId);
		FormField ftbServiceId= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		
		FormField formfield [][] = {
					{fgroupingComplaintTypeInformation},
					{fradioBtnServiceComplaint,fradioBtnOtherComplaint,fbtnRegisterComplaint},
					{fgroupingCustomerInformation},
					{fpersoninfo},
					{olbBranch,fromDate,toDate,ftbServiceId,fbtnGo},
					{fblankgroup},
					{fverticalpanel},
				};
		this.fields = formfield;
		
		
	}

	

	private ListGridField[] getGridFields() {

		 
		   ListGridField field1 = new ListGridField("RegisterComplaint","Register Complaint");
		   field1.setWrap(true);
		   field1.setAlign(Alignment.CENTER);
		   field1.setType(ListGridFieldType.IMAGE);
		   field1.setDefaultValue("Complaint.png");
		   field1.setCanEdit(false);
		   field1.setImageSize(140);
		   field1.setImageHeight(30);
		   
		   ListGridField field2 = new ListGridField("ServiceId","Service Id");
		   field2.setWrap(true);
		   field2.setAlign(Alignment.CENTER);
		   
		   ListGridField field3 = new ListGridField("ServiceDate","Service Date");
		   field3.setWrap(true);
		   field3.setAlign(Alignment.CENTER);
		   
		   ListGridField field4 = new ListGridField("ServiceTime","Service Time");
		   field4.setWrap(true);
		   field4.setAlign(Alignment.CENTER);
		   
		   ListGridField field5 = new ListGridField("CustomerID","Customer ID");
		   field5.setWrap(true);
		   field5.setAlign(Alignment.CENTER);
		   
		   ListGridField field6 = new ListGridField("CustomerName","Customer Name");
		   field6.setWrap(true);
		   field6.setAlign(Alignment.CENTER);
		   
		   ListGridField field7 = new ListGridField("Branch","Branch");
		   field7.setWrap(true);
		   field7.setAlign(Alignment.CENTER);
		   
//		   ListGridField field8 = new ListGridField("Technician","Technician/Material");
//		   field8.setWrap(true);
//		   field8.setAlign(Alignment.CENTER);
//		   field8.setType(ListGridFieldType.IMAGE);
//		   field8.setDefaultValue("Complaint.png");
//		   field8.setCanEdit(false);
//		   field8.setImageSize(80);
//		   field8.setImageHeight(30);
		   
		   return new ListGridField[] { 
				  field1, field2,field3,field4,field5,field6,field7
				   };
		
	}

	private void setDataSourceField(DataSource dataSource) {
		DataSourceIntegerField pkField = new DataSourceIntegerField("pk");  
        pkField.setHidden(true);  
        pkField.setPrimaryKey(true);
		
	   DataSourceField field1 = new DataSourceField("RegisterComplaint", FieldType.IMAGE);
	   DataSourceField field2 = new DataSourceField("ServiceId", FieldType.TEXT);

	   DataSourceDateField field3 = new DataSourceDateField("ServiceDate");
	   DataSourceField field4 = new DataSourceField("ServiceTime",FieldType.TEXT);
	   
	   DataSourceField field5 = new DataSourceField("CustomerID", FieldType.TEXT);

	   DataSourceField field6 = new DataSourceField("CustomerName", FieldType.TEXT);
	   DataSourceField field7 = new DataSourceField("Branch", FieldType.TEXT);
//	   DataSourceField field8 = new DataSourceField("Technician", FieldType.IMAGE);
	   
	   dataSource.setFields(pkField,field1,field2,field3,field4,field5,field6,field7);
	}
	
	

	public void setDataToListGrid(ArrayList<Service> list){
		System.out.println("FORM SETTING DATA....");
		
		ds.setTestData(getGridData(list));
		grid.setDataSource(ds);
		grid.setFields(getGridFields());
		grid.fetchData();
		
	}

	private DataClass[] getGridData(ArrayList<Service> Servicelist) {
		DateTimeFormat dateformat = DateTimeFormat.getFormat("dd/MM/yyyy");

		 ListGridRecord[] records = null; 
		 records = new ListGridRecord[Servicelist.size()];
		 for(int i=0;i<Servicelist.size();i++){
			 ListGridRecord record = new ListGridRecord();
			 Service service=Servicelist.get(i);
			 
			 record.setAttribute("ServiceId", service.getCount());
			 record.setAttribute("RegisterComplaint", "Complaint.png");
			 if(service.getServiceDate()!=null){

				 record.setAttribute("ServiceDate", dateformat.format(service.getServiceDate()));
			 }else{
				 record.setAttribute("ServiceDate", "");
			 }
			 if(service.getServiceTime()!=null){
				 record.setAttribute("ServiceTime",service.getServiceTime());
			 }
			 else{
				 record.setAttribute("ServiceTime","");
			 }
			 record.setAttribute("CustomerID", service.getPersonInfo().getCount());
			 record.setAttribute("CustomerName",service.getPersonInfo().getFullName());
			 record.setAttribute("Branch", service.getBranch());
//			 record.setAttribute("RegisterComplaint", "Complaint.png");
			 records[i]=record;
		 }
		 System.out.println("RECORDS LENGTH : "+records.length);
		 
		 return records;
	}

	private void setGridProperty(ListGrid grid) {
		   grid.setWidth("76%");
		   grid.setHeight("60%");
		   grid.setAutoFetchData(true);
		   grid.setAlternateRecordStyles(true);
		   grid.setShowAllRecords(true);
		   grid.setShowFilterEditor(true);
		   grid.setFilterOnKeypress(true);
		   
		   grid.setWrapCells(true);	
		   grid.setFixedRecordHeights(false);
		   grid.setAutoFitData(Autofit.HORIZONTAL);	
		   grid.setAutoFitWidthApproach(AutoFitWidthApproach.BOTH);	
		   grid.setCanResizeFields(true);
		   
	}
	
	
	
	@Override
	public void updateModel(Complain model) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateView(Complain model) {
		// TODO Auto-generated method stub
		
	}

	
	
	
	 /**
	 * Toggles the app header bar menus as per screen state
	 */
	@Override
	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
//				if(text.equals("Download")||text.equals("Search"))
//				{
//					menus[k].setVisible(true); 
//				}
//				else
					menus[k].setVisible(false);  		  		
			}
		}
		
		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
//				String text=menus[k].getText();
//				if(text.equals("Download"))
//					menus[k].setVisible(true); 
//				else
					menus[k].setVisible(false);  		   
			}
		}
		
		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
//				String text=menus[k].getText();
//				if(text.equals("Download"))
//					menus[k].setVisible(true); 
//				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.CUSTOMERSUPPORT,LoginPresenter.currentModule.trim());
	}

	@Override
	public void onClick(ClickEvent event) {

		
	}

	
	




	
	
}
