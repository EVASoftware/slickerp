package com.slicktechnologies.client.views.customersupport;

import com.google.gwt.event.dom.client.ClickEvent;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.slicktechnologies.client.views.project.concproject.EmpolyeeTable;
import com.slicktechnologies.client.views.project.concproject.MaterialProductGroupTable;

/**
 * @author Vijay Chougule
 *
 */
public class TechnicianAndMaterialPopUp extends PopupScreen{

	
    MaterialProductGroupTable materialProdTable;
//    EmpolyeeTable emptable;
    TechnicianInfoTable empTable;
    
	public TechnicianAndMaterialPopUp(){
		super();
//		getPopup().getElement().addClassName("setWidth");
		materialProdTable.connectToLocal();
		getPopup().setWidth("600px");
		getMaterialProdTable().getTable().setHeight("200px");
		getMaterialProdTable().getTable().setWidth("600px");
		
		createGui();
	}
	
	@Override
	public void createScreen() {
		materialProdTable = new MaterialProductGroupTable();
//		emptable = new EmpolyeeTable();
		empTable = new TechnicianInfoTable();
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		
		FormField fgroupingComplaintInformation2=fbuilder.setlabel("Material Information").widgetType(com.simplesoftwares.client.library.FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",materialProdTable.getTable());
		FormField fmaterialProdTable= fbuilder.setRowSpan(0).setColSpan(4).build();
		
		FormField fTechnician=fbuilder.setlabel("Technician Information").widgetType(com.simplesoftwares.client.library.FieldType.Grouping).setMandatory(false).setColSpan(4).build();

		fbuilder = new FormFieldBuilder("",empTable.getTable());
		FormField femptable= fbuilder.setRowSpan(0).setColSpan(4).build();
		
		FormField formfileds [][] = {
				{fgroupingComplaintInformation2},
				{fmaterialProdTable},
				{fTechnician},
				{femptable}
				
		};
		this.fields = formfileds;
	}
	

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		if(event.getSource()==lblCancel){
			hidePopUp();
		}
	}

	public MaterialProductGroupTable getMaterialProdTable() {
		return materialProdTable;
	}

	public void setMaterialProdTable(MaterialProductGroupTable materialProdTable) {
		this.materialProdTable = materialProdTable;
	}

	public TechnicianInfoTable getEmpTable() {
		return empTable;
	}

	public void setEmpTable(TechnicianInfoTable empTable) {
		this.empTable = empTable;
	}

//	public EmpolyeeTable getEmptable() {
//		return emptable;
//	}
//
//	public void setEmptable(EmpolyeeTable emptable) {
//		this.emptable = emptable;
//	}
	
	
	
}
