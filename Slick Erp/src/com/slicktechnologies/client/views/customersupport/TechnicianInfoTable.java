package com.slicktechnologies.client.views.customersupport;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.customersupport.TechnicianInfo;

public class TechnicianInfoTable extends SuperTable<TechnicianInfo>{

	TextColumn<TechnicianInfo> getcolumnEmployeeName;
	TextColumn<TechnicianInfo> getcolumnEmployeeId;
	TextColumn<TechnicianInfo> getcolumnEmployeeCellNo;
	TextColumn<TechnicianInfo> getcolumnEmployeeDesignation;
	
	@Override
	public void createTable() {
		// TODO Auto-generated method stub
		addcolumnEmployeeName();
		addcolumnEmployeeId();
		addcolumnEmployeeCellNo();
		addcolumnEmployeeDesignation();
	}

	private void addcolumnEmployeeName() {
		getcolumnEmployeeName = new TextColumn<TechnicianInfo>() {
			
			@Override
			public String getValue(TechnicianInfo object) {
				if(object.getEmployeeName()!=null){
					return object.getEmployeeName();
				}
				return "NA";
			}
		};
        table.addColumn(getcolumnEmployeeName, "Employee Name");
        table.setColumnWidth(getcolumnEmployeeName, 100,Unit.PX);

	}

	private void addcolumnEmployeeId() {
		getcolumnEmployeeId = new TextColumn<TechnicianInfo>() {
			
			@Override
			public String getValue(TechnicianInfo object) {
				return object.getEmployeeId()+"";
			}
		};
		 table.addColumn(getcolumnEmployeeId, "Employee Id");
	     table.setColumnWidth(getcolumnEmployeeId, 100,Unit.PX);
	}

	private void addcolumnEmployeeCellNo() {
		getcolumnEmployeeCellNo = new TextColumn<TechnicianInfo>() {
			
			@Override
			public String getValue(TechnicianInfo object) {
				// TODO Auto-generated method stub
				return object.getEmpCellNumber()+"";
			}
		};
		 table.addColumn(getcolumnEmployeeCellNo, "Employee PhoneNo");
	     table.setColumnWidth(getcolumnEmployeeCellNo, 100,Unit.PX);
	}

	private void addcolumnEmployeeDesignation() {
		getcolumnEmployeeDesignation = new TextColumn<TechnicianInfo>() {
			
			@Override
			public String getValue(TechnicianInfo object) {
				if(object.getEmployeeDesignation()!=null){
					return object.getEmployeeDesignation();
				}
				return "NA";
			}
		};
		 table.addColumn(getcolumnEmployeeDesignation, "Employee Designation");
	     table.setColumnWidth(getcolumnEmployeeDesignation, 100,Unit.PX);
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
