package com.slicktechnologies.client.views.documentcancellation;

import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.helperlayer.Config;

public class DocumentCancellationPopUp extends AbsolutePanel {
	
	public TextBox paymentID;
	public TextBox paymentDate;
	public TextBox paymentStatus;
	
	public TextArea remark;
	/**
	 * Date 
	 */
	ObjectListBox<Config>reasons;
	Button btnCancel,btnOk;
	
	InlineLabel reasonlab= new InlineLabel("Reason");
	InlineLabel Remark = new InlineLabel("Remark");
		public DocumentCancellationPopUp() {
			
			btnCancel= new Button("Cancel");
			btnOk= new Button("Ok");
			
//			InlineLabel remark1 = new InlineLabel("Remark");
//			add(remark1,160,10);
//			remark1.getElement().getStyle().setFontSize(20, Unit.PX);
			
			
			InlineLabel payID = new InlineLabel("Document ID");
			add(payID,10,50);
			paymentID= new TextBox();
			add(paymentID,10,70);
			
			paymentID.setSize("100px", "18px");
			paymentID.setEnabled(false);
			
			
			
			InlineLabel payDt = new InlineLabel("Document Date");
			add(payDt,130,50);
			paymentDate= new TextBox();
			add(paymentDate,130,70);
			
			paymentDate.setSize("110px", "18px");
			paymentDate.setEnabled(false);
			
			
			InlineLabel paystatus = new InlineLabel("Document Status");
			add(paystatus,260,50);
			paymentStatus= new TextBox();
			add(paymentStatus,260,70);
			
			paymentStatus.setSize("100px", "18px");
			paymentStatus.setEnabled(false);
			
			
			
			add(Remark,10,110);
			remark= new TextArea();
			add(remark,10,140);
			
			remark.setSize("250px", "30px");
			
			
//			InlineLabel reasonlab= new InlineLabel("Reason");
			add(reasonlab,10,190);
			
			reasons=new ObjectListBox<Config>();
			AppUtility.MakeLiveConfig(reasons, Screen.LEADUNSUCCESFULREASONS);
			add(reasons,10,220);
			reasons.setSize("250px", "35px");
			
			add(btnOk,100,260);
			add(btnCancel,200,260);
			setSize("450px", "300px");
			/**
			 * nidhi
			 */
			reasons.setVisible(false);
			reasonlab.setVisible(false);
			this.getElement().setId("form");
		}

		
		public DocumentCancellationPopUp(boolean flag) {
			this();
			
			
			
			reasonlab= new InlineLabel("Reason");
			add(reasonlab,10,110);
			
			
			reasons= new ObjectListBox<Config>();
			AppUtility.MakeLiveConfig(reasons, Screen.LEADUNSUCCESFULREASONS);
			add(reasons,10,140);
			reasons.setSize("250px", "30px");
			
			
			remark.setVisible(false);
			Remark.setVisible(false);
			add(btnOk,100,220);
			add(btnCancel,200,220);
			
			setSize("450px", "260px");
		}
		
//************************getters and setters*************************
		public TextArea getRemark() {
			return remark;
		}


		public void setRemark(TextArea remark) {
			this.remark = remark;
		}


		public Button getBtnCancel() {
			return btnCancel;
		}


		public void setBtnCancel(Button btnCancel) {
			this.btnCancel = btnCancel;
		}


		public Button getBtnOk() {
			return btnOk;
		}


		public void setBtnOk(Button btnOk) {
			this.btnOk = btnOk;
		}

		public TextBox getPaymentID() {
			return paymentID;
		}

		public void setPaymentID(TextBox paymentID) {
			this.paymentID = paymentID;
		}

		public TextBox getPaymentDate() {
			return paymentDate;
		}

		public void setPaymentDate(TextBox paymentDate) {
			this.paymentDate = paymentDate;
		}

		public TextBox getPaymentStatus() {
			return paymentStatus;
		}

		public void setPaymentStatus(TextBox paymentStatus) {
			this.paymentStatus = paymentStatus;
		}

		/*
		 * Date 16-6-2018
		 * By Jayshree
		 * 
		 */
		
		public  ObjectListBox<Config> getReason() {
			return reasons;
		}

		public void setReason(ObjectListBox<Config> reasons) {
			this.reasons = reasons;
		}

}
