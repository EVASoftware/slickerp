package com.slicktechnologies.client.views.documentcancellation;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Embed;

@Embed
public class CancellationSummary implements Serializable {
	
	 /**
	 * 
	 */
	private static final long serialVersionUID = -6478242450312442554L;
	
	
	int srNo;;
	 String docType;
	 int docID;
	 String loggedInUser;
	 String oldStatus;
	 String newStatus;;
	 String remark;
	 
	 
	 //************************getter and setters********************************
	 
	public int getSrNo() {
		return srNo;
	}
	public void setSrNo(int srNo) {
		this.srNo = srNo;
	}
	public String getDocType() {
		return docType;
	}
	public void setDocType(String docType) {
		this.docType = docType;
	}
	public int getDocID() {
		return docID;
	}
	public void setDocID(int docID) {
		this.docID = docID;
	}
	public String getLoggedInUser() {
		return loggedInUser;
	}
	public void setLoggedInUser(String loggedInUser) {
		this.loggedInUser = loggedInUser;
	}
	public String getOldStatus() {
		return oldStatus;
	}
	public void setOldStatus(String oldStatus) {
		this.oldStatus = oldStatus;
	}
	public String getNewStatus() {
		return newStatus;
	}
	public void setNewStatus(String newStatus) {
		this.newStatus = newStatus;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}

}
