package com.slicktechnologies.client.views.documentcancellation;

import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.TextBox;

public class CancellationSummaryPopUp extends AbsolutePanel {
	
	public TextBox docType;
	public TextBox docID;
	Button btnDownload,btnOk;
	CancellationSummaryPopUpTable summaryTable;
	public CancellationSummaryPopUp()
	{
		InlineLabel remark1 = new InlineLabel("Cancellation Summary");
		add(remark1,300,10);
		remark1.getElement().getStyle().setFontSize(20, Unit.PX);
		
		summaryTable= new CancellationSummaryPopUpTable();
		summaryTable.connectToLocal();
		
		add(summaryTable.getTable(),10,40);
		
		
		btnDownload= new Button("Download");
		btnOk= new Button("Ok");
		add(btnOk,250,400);
		add(btnDownload,350,400);
		setSize("750px", "450px");
	}
	
	
	/************getter and setters*************************************/

	public TextBox getDocType() {
		return docType;
	}

	public void setDocType(TextBox docType) {
		this.docType = docType;
	}

	public TextBox getDocID() {
		return docID;
	}

	public void setDocID(TextBox docID) {
		this.docID = docID;
	}

	public Button getBtnDownload() {
		return btnDownload;
	}

	public void setBtnDownload(Button btnDownload) {
		this.btnDownload = btnDownload;
	}

	public Button getBtnOk() {
		return btnOk;
	}

	public void setBtnOk(Button btnOk) {
		this.btnOk = btnOk;
	}


	public CancellationSummaryPopUpTable getSummaryTable() {
		return summaryTable;
	}


	public void setSummaryTable(List<CancellationSummary> summaryTable) {
		this.summaryTable.getDataprovider().setList(summaryTable); 
		
	}

}
