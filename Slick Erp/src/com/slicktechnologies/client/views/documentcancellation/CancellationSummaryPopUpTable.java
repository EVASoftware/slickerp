package com.slicktechnologies.client.views.documentcancellation;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;

public class CancellationSummaryPopUpTable extends SuperTable<CancellationSummary> {
	
	public TextColumn<CancellationSummary> srNo;
	public TextColumn<CancellationSummary> docType;
	public TextColumn<CancellationSummary> docID;
	public TextColumn<CancellationSummary> docDate;
	public TextColumn<CancellationSummary> loggedInUser;
	public TextColumn<CancellationSummary> oldStatus;
	public TextColumn<CancellationSummary> newStatus;
	public TextColumn<CancellationSummary> remark;
	
	public CancellationSummaryPopUpTable()
	{
		setHeight("350px");
	}
	
	public void createTable(){
		addColumngetsrNO();
		addColumngetdocType();
		addColumngetdocID();
		addColumngetloggedInUser();
		addColumngetoldStatus();
		addColumngetnewStatus();
		addColumngetremark();
		
	}
	
	private void addColumngetsrNO() {
		srNo = new  TextColumn<CancellationSummary>() {
			public String getValue(CancellationSummary object)
			{
				return object.getSrNo()+"";
			}
		};
		table.addColumn(srNo,"SrNo" );
		table.setColumnWidth(srNo, 30, Unit.PX);
	}

	private void addColumngetdocType() {
		docType = new  TextColumn<CancellationSummary>() {
			public String getValue(CancellationSummary object)
			{
				return object.getDocType();
			}
		};
		table.addColumn(docType,"Doc Type" );
		table.setColumnWidth(docType, 30, Unit.PX);
		
	}
	
	private void addColumngetdocID() {
		docID = new  TextColumn<CancellationSummary>() {
			public String getValue(CancellationSummary object)
			{
				return object.getDocID()+"";
			}
		};
		table.addColumn(docID,"Doc Id" );
		table.setColumnWidth(docID, 30, Unit.PX);
	}
	
	private void addColumngetloggedInUser() {
		loggedInUser = new  TextColumn<CancellationSummary>() {
			public String getValue(CancellationSummary object)
			{
				return object.getLoggedInUser();
			}
		};
		table.addColumn(loggedInUser,"Logged In User" );
		table.setColumnWidth(loggedInUser, 40, Unit.PX);
	}
	
	private void addColumngetoldStatus() {
		oldStatus = new  TextColumn<CancellationSummary>() {
			public String getValue(CancellationSummary object)
			{
				return object.getOldStatus();
			}
		};
		table.addColumn(oldStatus,"Old Status" );
		table.setColumnWidth(oldStatus, 30, Unit.PX);
	}
	
	private void addColumngetnewStatus() {
		newStatus = new  TextColumn<CancellationSummary>() {
			public String getValue(CancellationSummary object)
			{
				return object.getNewStatus();
			}
		};
		table.addColumn(newStatus,"New Status" );
		table.setColumnWidth(newStatus, 30, Unit.PX);
	}
	
	private void addColumngetremark() {
		remark = new  TextColumn<CancellationSummary>() {
			public String getValue(CancellationSummary object)
			{
				return object.getRemark();
			}
		};
		table.addColumn(remark,"Remark" );
		table.setColumnWidth(remark, 50, Unit.PX);
	}
	
	
	
	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
