package com.slicktechnologies.client.views.declaration;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.helperlayer.Declaration;

public class DeclarationTable extends SuperTable<Declaration>{

	TextColumn<Declaration> getCountColumn;
	TextColumn<Declaration> getTitleColumn;
	TextColumn<Declaration> getDeclarationMessageColumn;
	TextColumn<Declaration> getStatusColumn;
	
	
	public DeclarationTable()
	{
		super();
	}

	@Override
	public void createTable() {
		addColumngetCount();
		addColumngetTitle();
		addColumngetDeclarationMessage();
		addColumngetStatus();
		
		
	}

	private void addColumngetStatus() {
		getStatusColumn = new TextColumn<Declaration>() {
			
			@Override
			public String getValue(Declaration object) {
				if(object.getStatus()==true)
					return "Active";
				else return "In Active";
						
			}
		};
		table.addColumn(getStatusColumn, "Status");
		getStatusColumn.setSortable(true);
		table.setColumnWidth(getStatusColumn, 100, Unit.PX);
		
	}


	private void addColumngetDeclarationMessage() {
		getDeclarationMessageColumn = new TextColumn<Declaration>() {
		
			@Override
			public String getValue(Declaration object) {
				return object.getDeclaratiomMsg();
			}
		};
		table.addColumn(getDeclarationMessageColumn,"Declaration Message");
		getDeclarationMessageColumn.setSortable(true);
		table.setColumnWidth(getDeclarationMessageColumn, 100, Unit.PX);
	}

	private void addColumngetTitle() {
		getTitleColumn = new TextColumn<Declaration>() {
			
			@Override
			public String getValue(Declaration object) {
				return object.getTitle();
			}
		};
		table.addColumn(getTitleColumn,"Title");
		getTitleColumn.setSortable(true);
		table.setColumnWidth(getTitleColumn, 100, Unit.PX);
		
	}

	
	private void addColumngetCount() {

		getCountColumn = new TextColumn<Declaration>() {

			@Override
			public String getValue(Declaration object) {
				if(object.getCount()==-1)
					return "N.A";
				else return object.getCount()+"";
			}
		};
		table.addColumn(getCountColumn, "ID");
		getCountColumn.setSortable(true);
		table.setColumnWidth(getCountColumn, 100, Unit.PX);
	}

	public void addColumnSorting(){
			addSortinggetCount();
			addSortinggetTitle();
			addSortingetDeclarationMessage();
			addSortinggetStatus();	
	}
	
	
	private void addSortinggetStatus() {
			
		
		List<Declaration> list = getDataprovider().getList();
		columnSort = new ListHandler<Declaration>(list);
		columnSort.setComparator(getStatusColumn, new Comparator<Declaration>() {
			
			@Override
			public int compare(Declaration e1, Declaration e2) {

				if(e1!=null && e2!=null)
				{
					if(e1.getStatus()==e2.getStatus()){
						return 0;
					}
					else{
						return -1;
					}
				}
				else{
					return 0;
				}
				
			}
		});
		
		table.addColumnSortHandler(columnSort);
	}


	private void addSortingetDeclarationMessage() {
		List<Declaration> list = getDataprovider().getList();
		columnSort = new ListHandler<Declaration>(list);
		columnSort.setComparator(getDeclarationMessageColumn, new Comparator<Declaration>() {

			@Override
			public int compare(Declaration e1, Declaration e2) {
				
				if(e1!=null && e2!=null)
					{
						if( e1.getDeclaratiomMsg()!=null && e2.getDeclaratiomMsg()!=null){
							return e1.getDeclaratiomMsg().compareTo(e2.getDeclaratiomMsg());}
					}
					else{
						return 0;}
					return 0;
			}
		});
		
		table.addColumnSortHandler(columnSort); 
	}
	

	private void addSortinggetTitle() {

		List<Declaration> list = getDataprovider().getList();
		columnSort = new ListHandler<Declaration>(list);
		columnSort.setComparator(getTitleColumn, new Comparator<Declaration>() {

			@Override
			public int compare(Declaration e1, Declaration e2) {
				
				if(e1!=null && e2!=null)
					{
						if( e1.getTitle()!=null && e2.getTitle()!=null){
							return e1.getTitle().compareTo(e2.getTitle());}
					}
					else{
						return 0;}
					return 0;
			}
		});
		
		table.addColumnSortHandler(columnSort); 
		
	}

	private void addSortinggetCount() {

		List<Declaration> list=getDataprovider().getList();
		columnSort=new ListHandler<Declaration>(list);
		columnSort.setComparator(getCountColumn, new Comparator<Declaration>()
				{
			@Override
			public int compare(Declaration e1,Declaration e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}


}
