package com.slicktechnologies.client.views.declaration;

import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.helperlayer.Declaration;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class DeclarationForm extends FormTableScreen<Declaration>{

	
	TextBox tbTitle;
	CheckBox chkboxStatus;
	TextArea taDeclarationMessage;
	TextBox ibId;
	
	public DeclarationForm  (SuperTable<Declaration> table, int mode,
			boolean captionmode) {
		super(table, mode, captionmode);
		createGui();

	}
	
	
	private void initalizeWidget(){
		
		tbTitle = new TextBox();
		chkboxStatus = new CheckBox();
		chkboxStatus.setValue(true);
		taDeclarationMessage = new TextArea();
		ibId = new TextBox();
		ibId.setEnabled(false);
	}
	
	
	@Override
	public void createScreen() {
		
		initalizeWidget();
		
		
		this.processlevelBarNames=new String[]{"New"};
		
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingMessageInformation=fbuilder.setlabel("Declaration Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("ID",ibId);
		FormField fibId = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Title",tbTitle);
		FormField ftbEvent= fbuilder.setMandatory(true).setMandatoryMsg("Title is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Declaration Message",taDeclarationMessage);
		FormField ftaMessage = fbuilder.setMandatory(true).setMandatoryMsg("Declaration Message is Mandatory!").setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Status",chkboxStatus);
		FormField fchkbxStatus = fbuilder.setRowSpan(0).setColSpan(0).build();
		
		FormField[][] formfield = { {fgroupingMessageInformation},
				  {fibId,ftbEvent,fchkbxStatus},
				  {ftaMessage},
				  };

				this.fields=formfield;		
		
	}
	
	
	@Override
	public void updateModel(Declaration model) 
	{
		if(tbTitle.getValue()!=null)
			model.setTitle(tbTitle.getValue());
		if(taDeclarationMessage.getValue()!=null)
			model.setDeclaratiomMsg(taDeclarationMessage.getValue());
		model.setStatus(chkboxStatus.getValue());
			
			presenter.setModel(model);
	}
	
	
	@Override
	public void updateView(Declaration view) 
	{
		if(view.getTitle()!=null)
			tbTitle.setValue(view.getTitle());
		if(view.getDeclaratiomMsg()!=null)
			taDeclarationMessage.setValue(view.getDeclaratiomMsg());
		ibId.setValue(view.getCount()+"");
		chkboxStatus.setValue(view.getStatus());
		
		presenter.setModel(view);

	}	
	
	
	/**
	 * Toggles the app header bar menus as per screen state
	 */

	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.SMSTEMPLATE,LoginPresenter.currentModule.trim());
	}

	/**
	 * sets the id textbox with the passed count value. 
	 */
	@Override
	public void setCount(int count)
	{
		ibId.setValue(count+"");	
	}

	
	
	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		ibId.setEnabled(false);
	}

	

	//******************getters and setters *********************
	
	
	public TextBox getTbTitle() {
		return tbTitle;
	}
	public void setTbTitle(TextBox tbTitle) {
		this.tbTitle = tbTitle;
	}
	public CheckBox getChkboxStatus() {
		return chkboxStatus;
	}
	public void setChkboxStatus(CheckBox chkboxStatus) {
		this.chkboxStatus = chkboxStatus;
	}
	public TextArea getTaDeclarationMessag() {
		return taDeclarationMessage;
	}
	public void setTaDeclarationMessag(TextArea taDeclarationMessag) {
		this.taDeclarationMessage = taDeclarationMessag;
	}
	
	
	
}
