package com.slicktechnologies.client.views.salesdashboard;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CaptionPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.ViewContainer;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.MyInlineLabel;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.deliverynote.DeliveryNotePresenterTableProxy;
import com.slicktechnologies.client.views.lead.LeadPresenter;
import com.slicktechnologies.client.views.salesorder.SalesOrderPresenter;
import com.slicktechnologies.client.views.salesquotation.SalesQuotationPresenter;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.deliverynote.DeliveryNote;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.role.ScreenAuthorization;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesorder.SalesQuotation;

public class SalesHomeForm extends ViewContainer implements ClickHandler {
	
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	/** String array repersenting the table header menus*/
	String[] tableheaderbarnames={"New","Download"};
	
	/** Flow panel for holding the Table header menus */
	FlowPanel tableheader;
	
	/** Lead Table which can come on the form, this component can be made generic */
	public SalesLeadPresenterTableProxy salesLeadTable;
	
	/** Quotation Table which can come on form component can be made generic */
	public SalesQuotationPresenterTableProxy salesQuotationTable;
	
	/** Contract table which can come on form component can be made generic */
	public SalesOrderPresenterTableProxy salesOrderTable;
	/** Service table which can come on form component can be made generic */
	public DeliveryNoteTableProxy deliveryNoteTable;
	
	
	//**************************rohan added this fields in dashboard for searching by branch and sales person  
	
			ObjectListBox<Branch> olbbranch=new ObjectListBox<Branch>();
			ObjectListBox<Employee> olbsalesPerson = new ObjectListBox<Employee>();
//			/**
//			 * Updated By: Viraj
//			 * Date: 10-01-2019
//			 * Description: To add Status filter in dashboard
//			 */
//			ObjectListBox<ConfigCategory> olbLeadStatus;
//			/** Ends **/
		//************************************changes ends here ********************
	
	DateBox dbFromDate;
	DateBox dbToDate;
	Button btnGo;
	
	
	public SalesHomeForm()
	{
		//Toggle the App header bar menu , In this case only search should come
		toggleAppHeaderBarMenu();
		//Create the Gui, actual Gui gets created here.
		createGui();
		//Sets the event handling
	}
	
	private void initializeWidget(){
		
		//***********************rohan changes here for searching for by branch and sales person ****
		
		olbbranch =new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbbranch);
		
		olbsalesPerson =new ObjectListBox<Employee>();
		AppUtility.makeSalesPersonListBoxLive(olbsalesPerson);
		
//		/**
//		 * Updated By: Viraj
//		 * Date: 10-01-2019
//		 * Description: To add Status filter in dashboard
//		 */
//		olbLeadStatus= new ObjectListBox<ConfigCategory>();
//		AppUtility.MakeLiveConfig(olbLeadStatus,Screen.HOME);
//		/** Ends **/
// ******************************changescomplete ********************************
		
		
		dbFromDate=new DateBoxWithYearSelector();
		dbToDate=new DateBoxWithYearSelector();
		DateTimeFormat dateFormat = DateTimeFormat.getFormat("dd-MM-yyyy");
		dbFromDate.setFormat(new DateBox.DefaultFormat(dateFormat));
		dbToDate.setFormat(new DateBox.DefaultFormat(dateFormat));
		btnGo=new Button("GO");
	}
	
	/**
	 * Toggles the app header bar menu.
	 */
	private void toggleAppHeaderBarMenu() {
		InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
		for(int k=0;k<menus.length;k++)
		{
			String text=menus[k].getText();
			if(text.contains("Search"))
			{
				menus[k].setVisible(true); 
			}
			else
				menus[k].setVisible(false);  		   
			
		}
		
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.ViewContainer#createGui()
	 */
	@Override
	public void createGui() 
	{
		initializeWidget();
		
		FlowPanel panel= new FlowPanel();
		
		panel.getElement().getStyle().setMarginTop(10, Unit.PX);
		panel.getElement().getStyle().setMarginLeft(75, Unit.PX);
		panel.getElement().getStyle().setMarginRight(75, Unit.PX);
		
		FlowPanel innerpanel= new FlowPanel();
		
		InlineLabel blank = new InlineLabel("    ");
		
		
		//************rohan added here search by branch and sales person***************  
		InlineLabel branch = new InlineLabel("  Branch  ");
		branch.setSize("90px", "20px");
		innerpanel.add(branch);
		innerpanel.add(olbbranch);
//		innerpanel.add(blank);
//		innerpanel.add(blank);
//		innerpanel.add(blank);
//		innerpanel.add(blank);
//		
		InlineLabel salesPerson = new InlineLabel(" Sales Person  ");
		salesPerson.setSize("90px", "20px");
		innerpanel.add(salesPerson);
		innerpanel.add(olbsalesPerson);
//		innerpanel.add(blank);
//		innerpanel.add(blank);
//		innerpanel.add(blank);
//		innerpanel.add(blank);
		
//**************************changes ends here ********************************
		
		
		InlineLabel fromDate = new InlineLabel("  From Date ");
		fromDate.setSize("90px", "20px");
		innerpanel.add(fromDate);
		innerpanel.add(dbFromDate);
		
		InlineLabel toDate = new InlineLabel("  To Date ");
		/**
		 * Updated By: Viraj
		 * Date: 10-01-2019
		 * Description: To add Status filter in dashboard
		 */
//		InlineLabel status = new InlineLabel("Status");
		innerpanel.add(toDate);
		innerpanel.add(dbToDate);
//		innerpanel.add(status);
//		innerpanel.add(olbLeadStatus);
		/** Ends **/
		innerpanel.add(blank);
		innerpanel.add(blank);
		innerpanel.add(btnGo);
		
		panel.add(innerpanel);
		content.add(panel);
		
		
		
		//Create the Four Scroll Panel To hold four types of Screen
		ScrollPanel[] scrollPanes=new ScrollPanel[4];
		//String array to hold captions
		String[]captions={"Leads","Quotations","Sales Order","Delivery Note"};
		//Screen array to hold new Screen object for the purpose of Redirection
		Screen[]screens={Screen.SALESLEAD,Screen.SALESQUOTATION,Screen.SALESORDER,Screen.DELIVERYNOTE};	
		
		
		
		
		
		//Create  Flow Panel,Inside Flow Panel 
		for(int i=0;i<captions.length;i++)
		{
			FlowPanel holder= new FlowPanel();
			tableheader= new FlowPanel();
			tableheader.getElement().setId("tableheader");
			// Provides Table Header which is responsible for new or Download Buttons
			setTableHeaderBar(screens[i]);
			
			tableheader.getElement().getStyle().setMarginTop(10, Unit.PX);
			tableheader.getElement().getStyle().setMarginBottom(10, Unit.PX);
			
			scrollPanes[i]= new ScrollPanel();
			scrollPanes[i].setHeight("150px");
		
			holder.add(tableheader);
			holder.add(scrollPanes[i]);
			//Caption Panel inside which a Flow Panel is added
			CaptionPanel cap= new CaptionPanel(captions[i]);
			cap.add(holder);
			cap.getElement().setClassName("tablecaption");
			//Add caption Panel on Content
		    content.add(cap);
		}
		//Add Lead Table inside the Scroll Panel
		salesLeadTable = new SalesLeadPresenterTableProxy();
		
		scrollPanes[0].add(salesLeadTable.content);
		//Add Quotation Table inside the Scroll Panel
		salesQuotationTable = new SalesQuotationPresenterTableProxy();
		scrollPanes[1].add(salesQuotationTable.content);
		//Add Contract Panel inside the table
		salesOrderTable = new SalesOrderPresenterTableProxy();
		scrollPanes[2].add(salesOrderTable.content);
		//Add Service Table
		deliveryNoteTable = new DeliveryNoteTableProxy();
		scrollPanes[3].add(deliveryNoteTable.content);
		
		content.getElement().setId("homeview");
	  }
	
	public void setTableHeaderBar(Screen screen)
	{
		for(int j=0;j<getTableheaderbarnames().length;j++)
		{
			MyInlineLabel lbl;
			lbl=new MyInlineLabel(getTableheaderbarnames()[j],screen);
            getTableheader().add(lbl);
			lbl.getElement().setId("tableheadermenu");
			lbl.addClickHandler(this);
		}
	}
	
	@Override
	public void applyStyle() {
		
	}
	
	@Override
	public void onClick(ClickEvent event) {
		if(event.getSource() instanceof MyInlineLabel)
		{
			MyInlineLabel lbl=(MyInlineLabel) event.getSource();
			redirectOnScreens(lbl);
			
		}
		
	}

	private void redirectOnScreens(MyInlineLabel lbl)
	{
		Screen screen=(Screen) lbl.redirectScreen;
		String title=lbl.getText().trim();
		switch(screen)
		{
		  case SALESLEAD:
			  
				if(title.equals("Download"))
				{
					reactOnLeadDownLoad();
				}
				
				if(title.equals("New"))
				{
					/**
					 * Date : 28-10-2017 BY ANIL
					 * Loading Lead Screen from dashboard
					 */
					AppMemory.getAppMemory().currentState=ScreeenState.NEW;
					AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Sales/Lead", Screen.SALESLEAD);
					screen.changeScreen();
					/**
					 * End
					 */
					LeadPresenter.initalize();
				}
				
				break;
				
		  case SALESQUOTATION:
			  
				if(title.equals("Download"))
				{
					reactOnQuotationDownLoad();
				}
				
				if(title.equals("New"))
				{
					SalesQuotationPresenter.initalize();    		  
				}
				
				break;
				
		  case SALESORDER:
			  
				if(title.equals("Download"))
				{
					reactOnSalesOrderDownLoad();
				}
				
				if(title.equals("New"))
				{
					SalesOrderPresenter.initalize();
				}
				
				break;
				
		  case DELIVERYNOTE:
			 
				if(title.equals("Download"))
				{
					reactOnDeliveryNoteDownLoad();
				}
				break;
		default:
			break;
		}
		}
	
		private void reactOnLeadDownLoad()
		{
			ArrayList<Lead> leadarray=new ArrayList<Lead>();
			List<Lead> list=(List<Lead>) salesLeadTable.getListDataProvider().getList();
			
			leadarray.addAll(list);
			
			csvservice.setleadslist(leadarray, new AsyncCallback<Void>() {
	
				@Override
				public void onFailure(Throwable caught) {
					System.out.println("RPC call Failed"+caught);
					
				}
	
				@Override
				public void onSuccess(Void result) {
					
	
					String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url=gwt + "csvservlet"+"?type="+3;
					Window.open(url, "test", "enabled");
					
				}
			});
			 
		}
	
	
		private void reactOnDeliveryNoteDownLoad()
		{
			ArrayList<DeliveryNote> notearray=new ArrayList<DeliveryNote>();
			List<DeliveryNote> list=(List<DeliveryNote>) deliveryNoteTable.getListDataProvider().getList();
			
			notearray.addAll(list);
			
			csvservice.setdeliverynotelist(notearray, new AsyncCallback<Void>() {
	
				@Override
				public void onFailure(Throwable caught) {
					System.out.println("RPC call Failed"+caught);
					
				}
	
				@Override
				public void onSuccess(Void result) {
					
	
					String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url=gwt + "csvservlet"+"?type="+62;
					Window.open(url, "test", "enabled");
					
				}
			});
			 
		}
	
	
		private void reactOnSalesOrderDownLoad()
		{
			ArrayList<SalesOrder> salesorderarray=new ArrayList<SalesOrder>();
			List<SalesOrder> list=(List<SalesOrder>) salesOrderTable.getDataprovider().getList();
			
			salesorderarray.addAll(list);
			
			csvservice.setSalesOrderList(salesorderarray, new AsyncCallback<Void>() {
	
				@Override
				public void onFailure(Throwable caught) {
					System.out.println("RPC call Failed"+caught);
					
				}
	
				@Override
				public void onSuccess(Void result) {
					String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url=gwt + "csvservlet"+"?type="+60;
					Window.open(url, "test", "enabled");
				}
			});
			 
		}
	
		private void reactOnQuotationDownLoad()
		{
			ArrayList<SalesQuotation> quotarray=new ArrayList<SalesQuotation>();
			List<SalesQuotation> list=(List<SalesQuotation>) salesQuotationTable.getDataprovider().getList();
			
			quotarray.addAll(list);
			
			csvservice.setsalesquotationlist(quotarray, new AsyncCallback<Void>() {
				
	
				@Override
				public void onFailure(Throwable caught) {
					System.out.println("RPC call Failed"+caught);
					
				}
	
				@Override
				public void onSuccess(Void result) {
					String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url=gwt + "csvservlet"+"?type="+61;
					Window.open(url, "test", "enabled");
				}
			});
			 
		}
	
		
	
	
		/*****************************************Getters And Setters*****************************************/
	
	public String[] getTableheaderbarnames() {
		return tableheaderbarnames;
	}
	
	public void setTableheaderbarnames(String[] tableheaderbarnames) {
		this.tableheaderbarnames = tableheaderbarnames;
	}
	
	public FlowPanel getTableheader() {
		return tableheader;
	}
	
	public void setTableheader(FlowPanel tableheader) {
		this.tableheader = tableheader;
	}
	
	public SalesLeadPresenterTableProxy getSalesLeadTable() {
		return salesLeadTable;
	}
	
	public void setSalesLeadTable(SalesLeadPresenterTableProxy salesLeadTable) {
		this.salesLeadTable = salesLeadTable;
	}
	
	public SalesQuotationPresenterTableProxy getSalesQuotationTable() {
		return salesQuotationTable;
	}
	
	public void setSalesQuotationTable(
			SalesQuotationPresenterTableProxy salesQuotationTable) {
		this.salesQuotationTable = salesQuotationTable;
	}
	
	public SalesOrderPresenterTableProxy getSalesOrderTable() {
		return salesOrderTable;
	}
	
	public void setSalesOrderTable(SalesOrderPresenterTableProxy salesOrderTable) {
		this.salesOrderTable = salesOrderTable;
	}
	
	public DeliveryNoteTableProxy getDeliveryNoteTable() {
		return deliveryNoteTable;
	}

	public void setDeliveryNoteTable(DeliveryNoteTableProxy deliveryNoteTable) {
		this.deliveryNoteTable = deliveryNoteTable;
	}

	public DateBox getDbFromDate() {
		return dbFromDate;
	}

	public void setDbFromDate(DateBox dbFromDate) {
		this.dbFromDate = dbFromDate;
	}

	public DateBox getDbToDate() {
		return dbToDate;
	}

	public void setDbToDate(DateBox dbToDate) {
		this.dbToDate = dbToDate;
	}

	public Button getBtnGo() {
		return btnGo;
	}

	public void setBtnGo(Button btnGo) {
		this.btnGo = btnGo;
	}

	public ObjectListBox<Branch> getOlbbranch() {
		return olbbranch;
	}

	public void setOlbbranch(ObjectListBox<Branch> olbbranch) {
		this.olbbranch = olbbranch;
	}

	public ObjectListBox<Employee> getOlbsalesPerson() {
		return olbsalesPerson;
	}

	public void setOlbsalesPerson(ObjectListBox<Employee> olbsalesPerson) {
		this.olbsalesPerson = olbsalesPerson;
	}
	
//	/**
//	 * Updated By: Viraj
//	 * Date: 10-01-2019
//	 * Description: To add Status filter in dashboard
//	 */
//	public ObjectListBox<ConfigCategory> getOlbLeadStatus() {
//		return olbLeadStatus;
//	}
//
//	public void setOlbLeadStatus(ObjectListBox<ConfigCategory> olbLeadStatus) {
//		this.olbLeadStatus = olbLeadStatus;
//	}
//	/** Ends **/
	
}
