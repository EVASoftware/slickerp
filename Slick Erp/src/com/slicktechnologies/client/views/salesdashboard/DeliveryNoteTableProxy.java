package com.slicktechnologies.client.views.salesdashboard;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.view.client.ProvidesKey;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.deliverynote.DeliveryNotePresenter.DeliveryNotePresenterTable;
import com.slicktechnologies.shared.common.deliverynote.DeliveryNote;

public class DeliveryNoteTableProxy extends DeliveryNotePresenterTable {TextColumn<DeliveryNote> getCustomerCellNumberColumn;
TextColumn<DeliveryNote> getCustomerFullNameColumn;
TextColumn<DeliveryNote> getCustomerIdColumn;
TextColumn<DeliveryNote> getSalesOrderCountColumn;
TextColumn<DeliveryNote> getBranchColumn;
TextColumn<DeliveryNote> getEmployeeColumn;
TextColumn<DeliveryNote> getStatusColumn;
TextColumn<DeliveryNote> getDeliveryDateColumn;
TextColumn<DeliveryNote> getGroupColumn;
TextColumn<DeliveryNote> getCategoryColumn;
TextColumn<DeliveryNote> getTypeColumn;
TextColumn<DeliveryNote> getCountColumn;

public Object getVarRef(String varName) {
	if (varName.equals("getCustomerCellNumberColumn"))
		return this.getCustomerCellNumberColumn;
	if (varName.equals("getCustomerFullNameColumn"))
		return this.getCustomerFullNameColumn;
	if (varName.equals("getCustomerIdColumn"))
		return this.getCustomerIdColumn;
	if (varName.equals("getSalesOrderCountColumn"))
		return this.getSalesOrderCountColumn;
	if (varName.equals("getBranchColumn"))
		return this.getBranchColumn;
	if (varName.equals("getEmployeeColumn"))
		return this.getEmployeeColumn;
	if (varName.equals("getStatusColumn"))
		return this.getStatusColumn;
	if (varName.equals("getDeliveryDateColumn"))
		return this.getDeliveryDateColumn;
	if (varName.equals("getGroupColumn"))
		return this.getGroupColumn;
	if (varName.equals("getCategoryColumn"))
		return this.getCategoryColumn;
	if (varName.equals("getTypeColumn"))
		return this.getTypeColumn;
	if (varName.equals("getCountColumn"))
		return this.getCountColumn;
	return null;
}

public DeliveryNoteTableProxy() {
	super();
}

@Override
public void createTable() {
	addColumngetCount();
	addColumngetSalesOrderCount();
	addColumngetCustomerId();
	addColumngetCustomerFullName();
	addColumngetCustomerCellNumber();
	addColumngetEmployee();
	addColumngetBranch();
	addColumngetDeliveryDate();
	addColumngetGroup();
	addColumngetCategory();
	addColumngetType();
	addColumngetStatus();
}

@Override
protected void initializekeyprovider() {
	keyProvider = new ProvidesKey<DeliveryNote>() {
		@Override
		public Object getKey(DeliveryNote item) {
			if (item == null) {
				return null;
			} else
				return item.getId();
		}
	};
}

@Override
public void setEnable(boolean state) {
}

@Override
public void applyStyle() {
}

public void addColumnSorting() {
	addSortinggetCount();
	addSortinggetBranch();
	addSortinggetEmployee();
	addSortinggetStatus();
	addSortinggetGroup();
	addSortinggetCategory();
	addSortinggetCustomerId();
	addSortinggetType();
	addSortinggetCustomerFullName();
	addSortinggetCustomerCellNumber();
	addSortinggetSalesOrderCount();
	addSortinggetDeliveryDate();
}

@Override
public void addFieldUpdater() {
}

protected void addSortinggetCount() {
	List<DeliveryNote> list = getDataprovider().getList();
	columnSort = new ListHandler<DeliveryNote>(list);
	columnSort.setComparator(getCountColumn,
			new Comparator<DeliveryNote>() {
				@Override
				public int compare(DeliveryNote e1, DeliveryNote e2) {
					if (e1 != null && e2 != null) {
						if (e1.getCount() == e2.getCount()) {
							return 0;
						}
						if (e1.getCount() > e2.getCount()) {
							return 1;
						} else {
							return -1;
						}
					} else {
						return 0;
					}
				}
			});
	table.addColumnSortHandler(columnSort);
}

protected void addColumngetCount() {
	getCountColumn = new TextColumn<DeliveryNote>() {
		@Override
		public String getValue(DeliveryNote object) {
			if (object.getCount() == -1)
				return "N.A";
			else
				return object.getCount() + "";
		}
	};
	table.addColumn(getCountColumn, "ID");

	table.setColumnWidth(getCountColumn, 100, Unit.PX);
	getCountColumn.setSortable(true);
}

protected void addColumngetDeliveryDate() {
	final Date todaysDate=new Date();
	CalendarUtil.addDaysToDate(todaysDate,-1);
	todaysDate.setHours(23);
	todaysDate.setMinutes(59);
	todaysDate.setSeconds(59);
	System.out.println("Todays Date Converted ::: "+todaysDate);
	
	getDeliveryDateColumn = new TextColumn<DeliveryNote>() {
		
		@Override
		public String getCellStyleNames(Context context, DeliveryNote object) {
			
			 if(object.getDeliveryDate().before(todaysDate)&&(object.getStatus().equals("Created")||object.getStatus().equals("Requested"))){
				 return "red";
			 }
			 else if(object.getStatus().equals("Completed")||object.getStatus().equals("Approved")){
				 return "green";
			 }
			 else{
				 return "black";
			 }
		}
		
		@Override
		public String getValue(DeliveryNote object) {
			if (object.getCreationDate() != null)
				return AppUtility.parseDate(object.getDeliveryDate());
			else
				return "N.A";

		}
	};
	table.addColumn(getDeliveryDateColumn, "Delivery Date");
	table.setColumnWidth(getDeliveryDateColumn, 100, Unit.PX);
	getDeliveryDateColumn.setSortable(true);
}

protected void addSortinggetDeliveryDate() {
	List<DeliveryNote> list = getDataprovider().getList();
	columnSort = new ListHandler<DeliveryNote>(list);
	columnSort.setComparator(getDeliveryDateColumn,
			new Comparator<DeliveryNote>() {
				@Override
				public int compare(DeliveryNote e1, DeliveryNote e2) {
					if (e1 != null && e2 != null) {
						if (e1.getDeliveryDate() != null
								&& e2.getDeliveryDate() != null) {
							return e1.getDeliveryDate().compareTo(
									e2.getDeliveryDate());
						}
					} else {
						return 0;
					}
					return 0;
				}
			});
	table.addColumnSortHandler(columnSort);
}

protected void addSortinggetBranch() {
	List<DeliveryNote> list = getDataprovider().getList();
	columnSort = new ListHandler<DeliveryNote>(list);
	columnSort.setComparator(getBranchColumn,
			new Comparator<DeliveryNote>() {
				@Override
				public int compare(DeliveryNote e1, DeliveryNote e2) {
					if (e1 != null && e2 != null) {
						if (e1.getBranch() != null
								&& e2.getBranch() != null) {
							return e1.getBranch().compareTo(e2.getBranch());
						}
					} else {
						return 0;
					}
					return 0;
				}
			});
	table.addColumnSortHandler(columnSort);
}

protected void addColumngetBranch() {
	getBranchColumn = new TextColumn<DeliveryNote>() {
		@Override
		public String getValue(DeliveryNote object) {
			return object.getBranch() + "";
		}
	};
	table.addColumn(getBranchColumn, "Branch");
	getBranchColumn.setSortable(true);
}

protected void addSortinggetEmployee() {
	List<DeliveryNote> list = getDataprovider().getList();
	columnSort = new ListHandler<DeliveryNote>(list);
	columnSort.setComparator(getEmployeeColumn,
			new Comparator<DeliveryNote>() {
				@Override
				public int compare(DeliveryNote e1, DeliveryNote e2) {
					if (e1 != null && e2 != null) {
						if (e1.getEmployee() != null
								&& e2.getEmployee() != null) {
							return e1.getEmployee().compareTo(
									e2.getEmployee());
						}
					} else {
						return 0;
					}
					return 0;
				}
			});
	table.addColumnSortHandler(columnSort);
}

protected void addColumngetEmployee() {
	getEmployeeColumn = new TextColumn<DeliveryNote>() {
		@Override
		public String getValue(DeliveryNote object) {
			return object.getEmployee() + "";
		}
	};
	table.addColumn(getEmployeeColumn, "Sales Person");
	getEmployeeColumn.setSortable(true);
	table.setColumnWidth(getEmployeeColumn, 100, Unit.PX);

}

protected void addSortinggetStatus() {
	List<DeliveryNote> list = getDataprovider().getList();
	columnSort = new ListHandler<DeliveryNote>(list);
	columnSort.setComparator(getStatusColumn,
			new Comparator<DeliveryNote>() {
				@Override
				public int compare(DeliveryNote e1, DeliveryNote e2) {
					if (e1 != null && e2 != null) {
						if (e1.getStatus() != null
								&& e2.getStatus() != null) {
							return e1.getStatus().compareTo(e2.getStatus());
						}
					} else {
						return 0;
					}
					return 0;
				}
			});
	table.addColumnSortHandler(columnSort);
}

protected void addColumngetStatus() {
	getStatusColumn = new TextColumn<DeliveryNote>() {
		@Override
		public String getValue(DeliveryNote object) {
			return object.getStatus() + "";
		}
	};
	table.addColumn(getStatusColumn, "Status");
	getStatusColumn.setSortable(true);
	table.setColumnWidth(getStatusColumn, 80, Unit.PX);

}

protected void addSortinggetGroup() {
	List<DeliveryNote> list = getDataprovider().getList();
	columnSort = new ListHandler<DeliveryNote>(list);
	columnSort.setComparator(getGroupColumn,
			new Comparator<DeliveryNote>() {
				@Override
				public int compare(DeliveryNote e1, DeliveryNote e2) {
					if (e1 != null && e2 != null) {
						if (e1.getGroup() != null && e2.getGroup() != null) {
							return e1.getGroup().compareTo(e2.getGroup());
						}
					} else {
						return 0;
					}
					return 0;
				}
			});
	table.addColumnSortHandler(columnSort);
}

protected void addColumngetGroup() {
	getGroupColumn = new TextColumn<DeliveryNote>() {
		@Override
		public String getValue(DeliveryNote object) {
			return object.getGroup() + "";
		}
	};
	table.addColumn(getGroupColumn, "Group");
	table.setColumnWidth(getGroupColumn, 120, Unit.PX);
	getGroupColumn.setSortable(true);
}

protected void addSortinggetCategory() {
	List<DeliveryNote> list = getDataprovider().getList();
	columnSort = new ListHandler<DeliveryNote>(list);
	columnSort.setComparator(getCategoryColumn,
			new Comparator<DeliveryNote>() {
				@Override
				public int compare(DeliveryNote e1, DeliveryNote e2) {
					if (e1 != null && e2 != null) {
						if (e1.getCategory() != null
								&& e2.getCategory() != null) {
							return e1.getCategory().compareTo(
									e2.getCategory());
						}
					} else {
						return 0;
					}
					return 0;
				}
			});
	table.addColumnSortHandler(columnSort);
}

protected void addColumngetCategory() {
	getCategoryColumn = new TextColumn<DeliveryNote>() {
		@Override
		public String getValue(DeliveryNote object) {
			return object.getCategory() + "";
		}
	};
	table.addColumn(getCategoryColumn, "Category");
	table.setColumnWidth(getCategoryColumn, "120px");
	getCategoryColumn.setSortable(true);
}

protected void addSortinggetCustomerId() {
	List<DeliveryNote> list = getDataprovider().getList();
	columnSort = new ListHandler<DeliveryNote>(list);
	columnSort.setComparator(getCustomerIdColumn,
			new Comparator<DeliveryNote>() {
				@Override
				public int compare(DeliveryNote e1, DeliveryNote e2) {
					if (e1 != null && e2 != null) {
						if (e1.getCinfo().getCount() == e2.getCinfo()
								.getCount()) {
							return 0;
						}
						if (e1.getCinfo().getCount() > e2.getCinfo()
								.getCount()) {
							return 1;
						} else {
							return -1;
						}
					} else {
						return 0;
					}
				}
			});
	table.addColumnSortHandler(columnSort);
}

protected void addColumngetCustomerId() {
	getCustomerIdColumn = new TextColumn<DeliveryNote>() {
		@Override
		public String getValue(DeliveryNote object) {
			if (object.getCinfo().getCount() == -1)
				return "N.A";
			else
				return object.getCinfo().getCount() + "";
		}
	};
	table.addColumn(getCustomerIdColumn, "Customer Id");
	table.setColumnWidth(getCustomerIdColumn, 100, Unit.PX);
	getCustomerIdColumn.setSortable(true);
}

protected void addSortinggetType() {
	List<DeliveryNote> list = getDataprovider().getList();
	columnSort = new ListHandler<DeliveryNote>(list);
	columnSort.setComparator(getTypeColumn, new Comparator<DeliveryNote>() {
		@Override
		public int compare(DeliveryNote e1, DeliveryNote e2) {
			if (e1 != null && e2 != null) {
				if (e1.getType() != null && e2.getType() != null) {
					return e1.getType().compareTo(e2.getType());
				}
			} else {
				return 0;
			}
			return 0;
		}
	});
	table.addColumnSortHandler(columnSort);
}

protected void addColumngetType() {
	getTypeColumn = new TextColumn<DeliveryNote>() {
		@Override
		public String getValue(DeliveryNote object) {
			return object.getType() + "";
		}
	};
	table.addColumn(getTypeColumn, "Type");
	table.setColumnWidth(getTypeColumn, 100, Unit.PX);
	getTypeColumn.setSortable(true);
}

protected void addSortinggetCustomerFullName() {
	List<DeliveryNote> list = getDataprovider().getList();
	columnSort = new ListHandler<DeliveryNote>(list);
	columnSort.setComparator(getCustomerFullNameColumn,
			new Comparator<DeliveryNote>() {
				@Override
				public int compare(DeliveryNote e1, DeliveryNote e2) {
					if (e1 != null && e2 != null) {
						if (e1.getCinfo().getFullName() != null
								&& e2.getCinfo().getFullName() != null) {
							return e1.getCinfo().getFullName()
									.compareTo(e2.getCinfo().getFullName());
						}
					} else {
						return 0;
					}
					return 0;
				}
			});
	table.addColumnSortHandler(columnSort);

}

protected void addColumngetCustomerFullName() {
	getCustomerFullNameColumn = new TextColumn<DeliveryNote>() {
		@Override
		public String getValue(DeliveryNote object) {
			return object.getCinfo().getFullName();
		}
	};
	table.addColumn(getCustomerFullNameColumn, "Customer Name");
	table.setColumnWidth(getCustomerFullNameColumn, 150, Unit.PX);
	getCustomerFullNameColumn.setSortable(true);
}

protected void addSortinggetCustomerCellNumber() {
	List<DeliveryNote> list = getDataprovider().getList();
	columnSort = new ListHandler<DeliveryNote>(list);
	columnSort.setComparator(getCustomerCellNumberColumn,
			new Comparator<DeliveryNote>() {
				@Override
				public int compare(DeliveryNote e1, DeliveryNote e2) {
					if (e1 != null && e2 != null) {
						if (e1.getCinfo().getCellNumber() == e2.getCinfo()
								.getCellNumber()) {
							return 0;
						}
						if (e1.getCinfo().getCellNumber() > e2.getCinfo()
								.getCellNumber()) {
							return 1;
						} else {
							return -1;
						}
					} else {
						return 0;
					}
				}
			});
	table.addColumnSortHandler(columnSort);
}

protected void addColumngetCustomerCellNumber() {
	getCustomerCellNumberColumn = new TextColumn<DeliveryNote>() {
		@Override
		public String getValue(DeliveryNote object) {
			return object.getCinfo().getCellNumber() + "";
		}
	};
	table.addColumn(getCustomerCellNumberColumn, "Customer Cell");
	table.setColumnWidth(getCustomerCellNumberColumn, 150, Unit.PX);
	getCustomerCellNumberColumn.setSortable(true);
}

protected void addSortinggetSalesOrderCount() {
	List<DeliveryNote> list = getDataprovider().getList();
	columnSort = new ListHandler<DeliveryNote>(list);
	columnSort.setComparator(getSalesOrderCountColumn,
			new Comparator<DeliveryNote>() {
				@Override
				public int compare(DeliveryNote e1, DeliveryNote e2) {
					if (e1 != null && e2 != null) {
						if (e1.getSalesOrderCount() == e2
								.getSalesOrderCount()) {
							return 0;
						}
						if (e1.getSalesOrderCount() > e2
								.getSalesOrderCount()) {
							return 1;
						} else {
							return -1;
						}
					} else {
						return 0;
					}
				}
			});
	table.addColumnSortHandler(columnSort);
}

protected void addColumngetSalesOrderCount() {
	getSalesOrderCountColumn = new TextColumn<DeliveryNote>() {
		@Override
		public String getValue(DeliveryNote object) {
			if (object.getSalesOrderCount() == -1)
				return "N.A";
			else
				return object.getSalesOrderCount() + "";
		}
	};
	table.addColumn(getSalesOrderCountColumn, "SalesOrder ID");
	table.setColumnWidth(getSalesOrderCountColumn, 160, Unit.PX);

	getSalesOrderCountColumn.setSortable(true);
}}
