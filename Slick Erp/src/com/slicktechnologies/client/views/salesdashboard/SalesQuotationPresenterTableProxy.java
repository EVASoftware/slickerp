package com.slicktechnologies.client.views.salesdashboard;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.code.p.gwtchismes.client.GWTCGlassPanel;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.view.client.ProvidesKey;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.services.CommunicationLogService;
import com.slicktechnologies.client.services.CommunicationLogServiceAsync;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.communicationlog.CommunicationLogPopUp;
import com.slicktechnologies.client.views.salesquotation.SalesQuotationPresenter.SalesQuotationPresenterTable;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;
import com.slicktechnologies.shared.common.salesorder.SalesQuotation;

public class SalesQuotationPresenterTableProxy extends SalesQuotationPresenterTable implements ClickHandler {TextColumn<SalesQuotation> getCustomerCellNumberColumn;
TextColumn<SalesQuotation> getCustomerFullNameColumn;
TextColumn<SalesQuotation> getCustomerIdColumn;
TextColumn<SalesQuotation> getSalesOrderCountColumn;
TextColumn<SalesQuotation> getQuotationCountColumn;
TextColumn<SalesQuotation> getLeadCountColumn;
TextColumn<SalesQuotation> getBranchColumn;
TextColumn<SalesQuotation> getEmployeeColumn;
TextColumn<SalesQuotation> getStatusColumn;
TextColumn<SalesQuotation> getCreationDateColumn;
TextColumn<SalesQuotation> getValidUntilDateColumn;
TextColumn<SalesQuotation> getGroupColumn;
TextColumn<SalesQuotation> getCategoryColumn;
TextColumn<SalesQuotation> getTypeColumn;
TextColumn<SalesQuotation> getCountColumn;
TextColumn<SalesQuotation> getPriorityColumn;
/** date 09/03/2018 added by komal for followup date **/
TextColumn<SalesQuotation> getFollowUpDateColumn;

Column<SalesQuotation, String> followUpDateButton;
PopupPanel communicationPanel;
CommunicationLogPopUp communicationLogPopUp = new CommunicationLogPopUp();
CommunicationLogServiceAsync communicationService = GWT
		.create(CommunicationLogService.class);
int rowIndex;
SalesQuotation quotation;
final GWTCAlert alert = new GWTCAlert();
final GWTCGlassPanel glassPanel = new GWTCGlassPanel();
final GenricServiceAsync service = GWT.create(GenricService.class);
static boolean followUpFlag = false;
/**
 * end komal
 */
public Object getVarRef(String varName)
{
	if(varName.equals("getCustomerCellNumberColumn"))
		return this.getCustomerCellNumberColumn;
	if(varName.equals("getCustomerFullNameColumn"))
		return this.getCustomerFullNameColumn;
	if(varName.equals("getCustomerIdColumn"))
		return this.getCustomerIdColumn;
	if(varName.equals("getSalesOrderCountColumn"))
		return this.getSalesOrderCountColumn;
	if(varName.equals("getQuotationCountColumn"))
		return this.getQuotationCountColumn;
	if(varName.equals("getLeadCountColumn"))
		return this.getLeadCountColumn;
	if(varName.equals("getBranchColumn"))
		return this.getBranchColumn;
	if(varName.equals("getEmployeeColumn"))
		return this.getEmployeeColumn;
	if(varName.equals("getStatusColumn"))
		return this.getStatusColumn;
	if(varName.equals("getValidUntilDateColumn"))
		return this.getValidUntilDateColumn;
	if(varName.equals("getGroupColumn"))
		return this.getGroupColumn;
	if(varName.equals("getCategoryColumn"))
		return this.getCategoryColumn;
	if(varName.equals("getTypeColumn"))
		return this.getTypeColumn;
	if(varName.equals("getCountColumn"))
		return this.getCountColumn;
	if(varName.equals("getCreationDateColumn"))
		return this.getCreationDateColumn;
	/** date 09/03/2018 added by komal for followup date **/
	if (varName.equals("getFollowUpDateColumn"))
		return this.getFollowUpDateColumn;
	return null ;
}
public SalesQuotationPresenterTableProxy()
{
	super();
	/** date 09/03/2018 added by komal for followup date **/
	communicationLogPopUp.getBtnOk().addClickHandler(this);
	communicationLogPopUp.getBtnCancel().addClickHandler(this);
	communicationLogPopUp.getBtnAdd().addClickHandler(this);
}
@Override 
public void createTable() {
	addColumngetCount();
	/** date 09/03/2018 added by komal for followup date **/
	createColumnFollowupDate();
	updateColumnFollowupDate();
	addCoulmnGetFollowUpDate();
	/**
	 * end komal
	 */
	addColumngetLeadCount();
	addColumngetSalesOrderCount();
	addColumngetCustomerId();
	addColumngetCustomerFullName();
	addColumngetCustomerCellNumber();
	addColumngetEmployee();
	addColumngetCreationDate();
	addColumngetValidUntilDate();
	addColumngetGroup();
	addColumngetCategory();
	addColumngetType();
	addColumngetPriority();
	addColumngetStatus();
	addColumngetBranch();
	}
@Override
protected void initializekeyprovider() {
	keyProvider= new ProvidesKey<SalesQuotation>()
			{
		@Override
		public Object getKey(SalesQuotation item)
		{
			if(item==null)
			{
				return null;
			}
			else
				return item.getId();
		}
			};
}
@Override
public void setEnable(boolean state)
{
}
@Override
public void applyStyle()
{
}
public void addColumnSorting(){
	addSortinggetCount();
	/** Date 09/03/2018 added by komal for follow-up date button **/
	addSortinggetFollowUpDate();
	addSortinggetBranch();
	addSortinggetValidUntilDate();
	addSortinggetCreationDate();
	addSortinggetEmployee();
	addSortinggetStatus();
	addSortinggetLeadCount();
	addSortinggetGroup();
	addSortinggetQuotationCount();
	addSortinggetCategory();
	addSortinggetCustomerId();
	addSortinggetType();
	addSortinggetCustomerFullName();
	addSortinggetCustomerCellNumber();
	addSortinggetSalesOrderCount();
}
@Override public void addFieldUpdater() {
}
protected void addSortinggetCount()
{
	List<SalesQuotation> list=getDataprovider().getList();
	columnSort=new ListHandler<SalesQuotation>(list);
	columnSort.setComparator(getCountColumn, new Comparator<SalesQuotation>()
			{
		@Override
		public int compare(SalesQuotation e1,SalesQuotation e2)
		{
			if(e1!=null && e2!=null)
			{
				if(e1.getCount()== e2.getCount()){
					return 0;}
				if(e1.getCount()> e2.getCount()){
					return 1;}
				else{
					return -1;}
			}
			else{
				return 0;}
		}
			});
	table.addColumnSortHandler(columnSort);
}
protected void addColumngetCount()
{
	getCountColumn=new TextColumn<SalesQuotation>()
			{
		@Override
		public String getValue(SalesQuotation object)
		{
			if( object.getCount()==-1)
				return "N.A";
			else return object.getCount()+"";
		}
			};
			table.addColumn(getCountColumn,"ID");
			table.setColumnWidth(getCountColumn,90,Unit.PX);
			getCountColumn.setSortable(true);
}
protected void addColumngetValidUntilDate()
{
	getValidUntilDateColumn=new TextColumn<SalesQuotation>()
			{
		@Override
		public String getValue(SalesQuotation object)
		{
			if(object.getCreationDate()!=null)
			     return  AppUtility.parseDate(object.getValidUntill());
			  else 
				  return "N.A";
			
		}
			};
			table.addColumn(getValidUntilDateColumn,"Valid Until");
			table.setColumnWidth(getValidUntilDateColumn,120,Unit.PX);
			getValidUntilDateColumn.setSortable(true);
}




protected void addSortinggetValidUntilDate()
{
	List<SalesQuotation> list=getDataprovider().getList();
	columnSort=new ListHandler<SalesQuotation>(list);
	columnSort.setComparator(getValidUntilDateColumn, new Comparator<SalesQuotation>()
			{
		@Override
		public int compare(SalesQuotation e1,SalesQuotation e2)
		{
			if(e1!=null && e2!=null)
			{
				if( e1.getValidUntill()!=null && e2.getValidUntill()!=null){
					return e1.getValidUntill().compareTo(e2.getValidUntill());}
			}
			else{
				return 0;}
			return 0;
		}
			});
	table.addColumnSortHandler(columnSort);
}

protected void addColumngetCreationDate()
{
	final Date sub7Days=new Date();
	System.out.println("Todays Date ::: "+sub7Days);
	CalendarUtil.addDaysToDate(sub7Days,-7);
	System.out.println("Date - 7 Days ::: "+sub7Days);
	sub7Days.setHours(23);
	sub7Days.setMinutes(59);
	sub7Days.setSeconds(59);
	System.out.println("Converted Date ::: "+sub7Days);
	
	getCreationDateColumn=new TextColumn<SalesQuotation>()
	{
		@Override
		public String getCellStyleNames(Context context, SalesQuotation object) {
//			 if(object.getCreationDate().before(add7Days)&&object.getCreationDate().after(new Date())&&object.getStatus().equals("Created")){
//				 return "red";
//			 }
			 if(object.getCreationDate().before(sub7Days)&&(object.getStatus().equals("Created")||object.getStatus().equals("Requested"))){
				 return "red";
			 }
			 else if(object.getStatus().equals("Successful")||object.getStatus().equals("UnSuccessful")||object.getStatus().equals("Approved")){
				 return "green";
			 }
			 else{
				 return "black";
			 }
		}
		
		@Override
		public String getValue(SalesQuotation object)
		{
			if(object.getCreationDate()!=null)
			     return  AppUtility.parseDate(object.getCreationDate());
			  else 
				  return "N.A";
			
		}
	};
	table.addColumn(getCreationDateColumn,"Creation Date");
	table.setColumnWidth(getCreationDateColumn,120,Unit.PX);
	getCreationDateColumn.setSortable(true);
}


protected void addSortinggetCreationDate()
{
	List<SalesQuotation> list=getDataprovider().getList();
	columnSort=new ListHandler<SalesQuotation>(list);
	columnSort.setComparator(getCreationDateColumn, new Comparator<SalesQuotation>()
			{
		@Override
		public int compare(SalesQuotation e1,SalesQuotation e2)
		{
			if(e1!=null && e2!=null)
			{
				if( e1.getCreationDate()!=null && e2.getCreationDate()!=null){
					return e1.getCreationDate().compareTo(e2.getCreationDate());}
			}
			else{
				return 0;}
			return 0;
		}
			});
	table.addColumnSortHandler(columnSort);
}


protected void addSortinggetBranch()
{
	List<SalesQuotation> list=getDataprovider().getList();
	columnSort=new ListHandler<SalesQuotation>(list);
	columnSort.setComparator(getBranchColumn, new Comparator<SalesQuotation>()
			{
		@Override
		public int compare(SalesQuotation e1,SalesQuotation e2)
		{
			if(e1!=null && e2!=null)
			{
				if( e1.getBranch()!=null && e2.getBranch()!=null){
					return e1.getBranch().compareTo(e2.getBranch());}
			}
			else{
				return 0;}
			return 0;
		}
			});
	table.addColumnSortHandler(columnSort);
}
protected void addColumngetBranch()
{
	getBranchColumn=new TextColumn<SalesQuotation>()
			{
		@Override
		public String getValue(SalesQuotation object)
		{
			return object.getBranch()+"";
		}
			};
			table.addColumn(getBranchColumn,"Branch");
			table.setColumnWidth(getBranchColumn,90,Unit.PX);
			getBranchColumn.setSortable(true);
}
protected void addSortinggetEmployee()
{
	List<SalesQuotation> list=getDataprovider().getList();
	columnSort=new ListHandler<SalesQuotation>(list);
	columnSort.setComparator(getEmployeeColumn, new Comparator<SalesQuotation>()
			{
		@Override
		public int compare(SalesQuotation e1,SalesQuotation e2)
		{
			if(e1!=null && e2!=null)
			{
				if( e1.getEmployee()!=null && e2.getEmployee()!=null){
					return e1.getEmployee().compareTo(e2.getEmployee());}
			}
			else{
				return 0;}
			return 0;
		}
			});
	table.addColumnSortHandler(columnSort);
}
protected void addColumngetEmployee()
{
	getEmployeeColumn=new TextColumn<SalesQuotation>()
			{
		@Override
		public String getValue(SalesQuotation object)
		{
			return object.getEmployee()+"";
		}
			};
			table.addColumn(getEmployeeColumn,"Sales Person");
			table.setColumnWidth(getEmployeeColumn,120,Unit.PX);
			getEmployeeColumn.setSortable(true);
}
protected void addSortinggetStatus()
{
	List<SalesQuotation> list=getDataprovider().getList();
	columnSort=new ListHandler<SalesQuotation>(list);
	columnSort.setComparator(getStatusColumn, new Comparator<SalesQuotation>()
			{
		@Override
		public int compare(SalesQuotation e1,SalesQuotation e2)
		{
			if(e1!=null && e2!=null)
			{
				if( e1.getStatus()!=null && e2.getStatus()!=null){
					return e1.getStatus().compareTo(e2.getStatus());}
			}
			else{
				return 0;}
			return 0;
		}
			});
	table.addColumnSortHandler(columnSort);
}
protected void addColumngetStatus()
{
	getStatusColumn=new TextColumn<SalesQuotation>()
			{
		@Override
		public String getValue(SalesQuotation object)
		{
			return object.getStatus()+"";
		}
			};
			table.addColumn(getStatusColumn,"Status");
			table.setColumnWidth(getStatusColumn,90,Unit.PX);
			getStatusColumn.setSortable(true);
}
protected void addSortinggetLeadCount()
{
	List<SalesQuotation> list=getDataprovider().getList();
	columnSort=new ListHandler<SalesQuotation>(list);
	columnSort.setComparator(getLeadCountColumn, new Comparator<SalesQuotation>()
			{
		@Override
		public int compare(SalesQuotation e1,SalesQuotation e2)
		{
			if(e1!=null && e2!=null)
			{
				if(e1.getLeadCount()== e2.getLeadCount()){
					return 0;}
				if(e1.getLeadCount()> e2.getLeadCount()){
					return 1;}
				else{
					return -1;}
			}
			else{
				return 0;}
		}
			});
	table.addColumnSortHandler(columnSort);
}
protected void addColumngetLeadCount()
{
	getLeadCountColumn=new TextColumn<SalesQuotation>()
			{
		@Override
		public String getValue(SalesQuotation object)
		{
			if( object.getLeadCount()==-1)
				return "N.A";
			else return object.getLeadCount()+"";
		}
			};
			table.addColumn(getLeadCountColumn,"Lead Id");
			table.setColumnWidth(getLeadCountColumn,90,Unit.PX);
			getLeadCountColumn.setSortable(true);
}
protected void addSortinggetGroup()
{
	List<SalesQuotation> list=getDataprovider().getList();
	columnSort=new ListHandler<SalesQuotation>(list);
	columnSort.setComparator(getGroupColumn, new Comparator<SalesQuotation>()
			{
		@Override
		public int compare(SalesQuotation e1,SalesQuotation e2)
		{
			if(e1!=null && e2!=null)
			{
				if( e1.getGroup()!=null && e2.getGroup()!=null){
					return e1.getGroup().compareTo(e2.getGroup());}
			}
			else{
				return 0;}
			return 0;
		}
			});
	table.addColumnSortHandler(columnSort);
}
protected void addColumngetGroup()
{
	getGroupColumn=new TextColumn<SalesQuotation>()
			{
		@Override
		public String getValue(SalesQuotation object)
		{
			return object.getGroup()+"";
		}
			};
			table.addColumn(getGroupColumn,"Group");
			table.setColumnWidth(getGroupColumn,90,Unit.PX);

			getGroupColumn.setSortable(true);
}
protected void addSortinggetQuotationCount()
{
	List<SalesQuotation> list=getDataprovider().getList();
	columnSort=new ListHandler<SalesQuotation>(list);
	columnSort.setComparator(getQuotationCountColumn, new Comparator<SalesQuotation>()
			{
		@Override
		public int compare(SalesQuotation e1,SalesQuotation e2)
		{
			if(e1!=null && e2!=null)
			{
				if(e1.getQuotationCount()== e2.getQuotationCount()){
					return 0;}
				if(e1.getQuotationCount()> e2.getQuotationCount()){
					return 1;}
				else{
					return -1;}
			}
			else{
				return 0;}
		}
			});
	table.addColumnSortHandler(columnSort);
}
protected void addColumngetQuotationCount()
{
	getQuotationCountColumn=new TextColumn<SalesQuotation>()
			{
		@Override
		public String getValue(SalesQuotation object)
		{
			if( object.getQuotationCount()==-1)
				return "N.A";
			else return object.getQuotationCount()+"";
		}
			};
			table.addColumn(getQuotationCountColumn,"Quotation Id");
			table.setColumnWidth(getQuotationCountColumn,90,Unit.PX);
			getQuotationCountColumn.setSortable(true);
}
protected void addSortinggetCategory()
{
	List<SalesQuotation> list=getDataprovider().getList();
	columnSort=new ListHandler<SalesQuotation>(list);
	columnSort.setComparator(getCategoryColumn, new Comparator<SalesQuotation>()
			{
		@Override
		public int compare(SalesQuotation e1,SalesQuotation e2)
		{
			if(e1!=null && e2!=null)
			{
				if( e1.getCategory()!=null && e2.getCategory()!=null){
					return e1.getCategory().compareTo(e2.getCategory());}
			}
			else{
				return 0;}
			return 0;
		}
			});
	table.addColumnSortHandler(columnSort);
}
protected void addColumngetCategory()
{
	getCategoryColumn=new TextColumn<SalesQuotation>()
			{
		@Override
		public String getValue(SalesQuotation object)
		{
			return object.getCategory()+"";
		}
			};
			table.addColumn(getCategoryColumn,"Category");
			table.setColumnWidth(getCategoryColumn,90,Unit.PX);
			getCategoryColumn.setSortable(true);
}
protected void addSortinggetCustomerId()
{
	List<SalesQuotation> list=getDataprovider().getList();
	columnSort=new ListHandler<SalesQuotation>(list);
	columnSort.setComparator(getCustomerIdColumn, new Comparator<SalesQuotation>()
			{
		@Override
		public int compare(SalesQuotation e1,SalesQuotation e2)
		{
			if(e1!=null && e2!=null)
			{
				if(e1.getCustomerId()== e2.getCustomerId()){
					return 0;}
				if(e1.getCustomerId()> e2.getCustomerId()){
					return 1;}
				else{
					return -1;}
			}
			else{
				return 0;}
		}
			});
	table.addColumnSortHandler(columnSort);
}
protected void addColumngetCustomerId()
{
	getCustomerIdColumn=new TextColumn<SalesQuotation>()
			{
		@Override
		public String getValue(SalesQuotation object)
		{
			if( object.getCustomerId()==-1)
				return "N.A";
			else return object.getCustomerId()+"";
		}
			};
			table.addColumn(getCustomerIdColumn,"Customer Id");
			table.setColumnWidth(getCustomerIdColumn,100,Unit.PX);
			getCustomerIdColumn.setSortable(true);
}
protected void addSortinggetType()
{
	List<SalesQuotation> list=getDataprovider().getList();
	columnSort=new ListHandler<SalesQuotation>(list);
	columnSort.setComparator(getTypeColumn, new Comparator<SalesQuotation>()
			{
		@Override
		public int compare(SalesQuotation e1,SalesQuotation e2)
		{
			if(e1!=null && e2!=null)
			{
				if( e1.getType()!=null && e2.getType()!=null){
					return e1.getType().compareTo(e2.getType());}
			}
			else{
				return 0;}
			return 0;
		}
			});
	table.addColumnSortHandler(columnSort);
}
protected void addColumngetType()
{
	getTypeColumn=new TextColumn<SalesQuotation>()
			{
		@Override
		public String getValue(SalesQuotation object)
		{
			return object.getType()+"";
		}
			};
			table.addColumn(getTypeColumn,"Type");
			table.setColumnWidth(getTypeColumn,90,Unit.PX);
			getTypeColumn.setSortable(true);
}

protected void addColumngetPriority()
{
	getPriorityColumn=new TextColumn<SalesQuotation>()
			{
		@Override
		public String getValue(SalesQuotation object)
		{
			return object.getPriority()+"";
		}
			};
			table.addColumn(getPriorityColumn,"Priority");
			table.setColumnWidth(getPriorityColumn,120,Unit.PX);
			getPriorityColumn.setSortable(true);
}

protected void addSortinggetCustomerFullName()
{
	List<SalesQuotation> list=getDataprovider().getList();
	columnSort=new ListHandler<SalesQuotation>(list);
	columnSort.setComparator(getCustomerFullNameColumn, new Comparator<SalesQuotation>()
			{
		@Override
		public int compare(SalesQuotation e1,SalesQuotation e2)
		{
			if(e1!=null && e2!=null)
			{
				if( e1.getCustomerFullName()!=null && e2.getCustomerFullName()!=null){
					return e1.getCustomerFullName().compareTo(e2.getCustomerFullName());}
			}
			else{
				return 0;}
			return 0;
		}
			});
	table.addColumnSortHandler(columnSort);
}
protected void addColumngetCustomerFullName()
{
	getCustomerFullNameColumn=new TextColumn<SalesQuotation>()
			{
		@Override
		public String getValue(SalesQuotation object)
		{
			return object.getCustomerFullName()+"";
		}
			};
			table.addColumn(getCustomerFullNameColumn,"Customer Name");
			table.setColumnWidth(getCustomerFullNameColumn,150,Unit.PX);
			getCustomerFullNameColumn.setSortable(true);
}
protected void addSortinggetCustomerCellNumber()
{
	List<SalesQuotation> list=getDataprovider().getList();
	columnSort=new ListHandler<SalesQuotation>(list);
	columnSort.setComparator(getCustomerCellNumberColumn, new Comparator<SalesQuotation>()
			{
		@Override
		public int compare(SalesQuotation e1,SalesQuotation e2)
		{
			if(e1!=null && e2!=null)
			{
				if(e1.getCustomerCellNumber()== e2.getCustomerCellNumber()){
					return 0;}
				if(e1.getCustomerCellNumber()> e2.getCustomerCellNumber()){
					return 1;}
				else{
					return -1;}
			}
			else{
				return 0;}
		}
			});
	table.addColumnSortHandler(columnSort);
}
protected void addColumngetCustomerCellNumber()
{
	getCustomerCellNumberColumn=new TextColumn<SalesQuotation>()
			{
		@Override
		public String getValue(SalesQuotation object)
		{
			return object.getCustomerCellNumber()+"";
		}
			};
			table.addColumn(getCustomerCellNumberColumn,"Customer Cell");
			table.setColumnWidth(getCustomerCellNumberColumn,150,Unit.PX);
			getCustomerCellNumberColumn.setSortable(true);
}
protected void addSortinggetSalesOrderCount()
{
	List<SalesQuotation> list=getDataprovider().getList();
	columnSort=new ListHandler<SalesQuotation>(list);
	columnSort.setComparator(getSalesOrderCountColumn, new Comparator<SalesQuotation>()
			{
		@Override
		public int compare(SalesQuotation e1,SalesQuotation e2)
		{
			if(e1!=null && e2!=null)
			{
				if(e1.getContractCount()== e2.getContractCount()){
					return 0;}
				if(e1.getContractCount()> e2.getContractCount()){
					return 1;}
				else{
					return -1;}
			}
			else{
				return 0;}
		}
			});
	table.addColumnSortHandler(columnSort);
}
protected void addColumngetSalesOrderCount()
{
	getSalesOrderCountColumn=new TextColumn<SalesQuotation>()
			{
		@Override
		public String getValue(SalesQuotation object)
		{
			if( object.getContractCount()==-1)
				return "N.A";
			else return object.getContractCount()+"";
		}
			};
			table.addColumn(getSalesOrderCountColumn,"Sales Order ID");
			table.setColumnWidth(getSalesOrderCountColumn,110,Unit.PX);
			getSalesOrderCountColumn.setSortable(true);
}
/** Date 09/03/2018 added by komal for follow-up date button **/

protected void addCoulmnGetFollowUpDate()
{
	final Date sub7Days=new Date();
	System.out.println("Todays Date ::: "+sub7Days);
	CalendarUtil.addDaysToDate(sub7Days,-7);
	System.out.println("Date - 7 Days ::: "+sub7Days);
	sub7Days.setHours(23);
	sub7Days.setMinutes(59);
	sub7Days.setSeconds(59);
	System.out.println("Converted Date ::: "+sub7Days);
	getFollowUpDateColumn=new TextColumn<SalesQuotation>()
			{
	
		/** date 18/10/2017 added by komal to add style of follow-up date column **/
		@Override
		public String getCellStyleNames(Context context, SalesQuotation object) {
			 if(object.getFollowUpDate().before(sub7Days)&&object.getStatus().equals("Created")){
				 return "red";
			 }
			 else if(object.getStatus().equals("Closed")||object.getStatus().equals("Successful")||object.getStatus().equals("Unsuccessful")){
				 return "green";
			 }
			 else{
				 return "black";
			 }
		}
	
	@Override
	public String getValue(SalesQuotation object)
	{
		if(object.getFollowUpDate()!=null)
		     return  AppUtility.parseDate(object.getFollowUpDate());
		  else 
			  return "N.A.";
		
	}
		};
		table.addColumn(getFollowUpDateColumn, "Follow-up Date");
		table.setColumnWidth(getFollowUpDateColumn,120,Unit.PX);
		getFollowUpDateColumn.setSortable(true);
}

protected void addSortinggetFollowUpDate()
{
	List<SalesQuotation> list=getDataprovider().getList();
	columnSort=new ListHandler<SalesQuotation>(list);
	columnSort.setComparator(getFollowUpDateColumn, new Comparator<SalesQuotation>()
			{
		@Override
		public int compare(SalesQuotation e1,SalesQuotation e2)
		{
			if(e1!=null && e2!=null)
			{
				 if(e1.getFollowUpDate() == null && e2.getFollowUpDate()==null){
				        return 0;
				    }
				    if(e1.getFollowUpDate() == null) return 1;
				    if(e2.getFollowUpDate() == null) return -1;
				    return e1.getFollowUpDate().compareTo(e2.getFollowUpDate());
			}
			return 0;
		}
			});
	table.addColumnSortHandler(columnSort);
}

protected void createColumnFollowupDate()
{
	ButtonCell btnCell = new ButtonCell();
   followUpDateButton = new Column<SalesQuotation, String>(btnCell) {
		
		@Override
		public String getValue(SalesQuotation object) {
			// TODO Auto-generated method stub
			return "Follow-up Date";
		}
	};                 
	
	table.addColumn(followUpDateButton, "");
	table.setColumnWidth(followUpDateButton,130,Unit.PX);
	
}


protected void updateColumnFollowupDate()
{
	// TODO Auto-generated method stub
	followUpDateButton.setFieldUpdater(new FieldUpdater<SalesQuotation, String>() {
				
				@Override
				public void update(int index, final SalesQuotation object, String value) {					
					rowIndex=index;
					followUpFlag = true;
					quotation= object;
					getLogDetails(object);
				}
			});
	
	
}

private void getLogDetails(SalesQuotation quotation)
{
	glassPanel.show();
	MyQuerry querry = AppUtility.communicationLogQuerry(quotation.getCount(), quotation.getCompanyId(),AppConstants.SALESMODULE,AppConstants.QUOTATION);
	
	service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
		
		@Override
		public void onSuccess(ArrayList<SuperModel> result) {
			
			ArrayList<InteractionType> list = new ArrayList<InteractionType>();
			
			for(SuperModel model : result){
				
				InteractionType interactionType = (InteractionType) model;
				list.add(interactionType);
				
			}
			/**
			 * Date : 08-11-2017 BY Komal
			 */
			Comparator<InteractionType> comp=new Comparator<InteractionType>() {
				@Override
				public int compare(InteractionType e1, InteractionType e2) {
//					Integer coutn1=o1.getCount();
//					Integer count2=o2.getCount();
//					return ;
					if (e1.getCount() == e2.getCount()) {
						return 0;
					}
					if (e1.getCount() > e2.getCount()) {
						return 1;
					} else {
						return -1;
					}
				}
			};
			Collections.sort(list,comp);
            
			glassPanel.hide();
			communicationLogPopUp.getRemark().setValue("");
			communicationLogPopUp.getDueDate().setValue(null);
			communicationLogPopUp.getOblinteractionGroup().setSelectedIndex(0);
			communicationLogPopUp.getCommunicationLogTable().getDataprovider().setList(list);
			communicationPanel = new PopupPanel(true);
			communicationPanel.add(communicationLogPopUp);
			communicationPanel.show();	
			communicationPanel.center();

		}
		
		@Override
		public void onFailure(Throwable caught) {
			glassPanel.hide();
		}
	});
}

@Override
public void onClick(ClickEvent event) {
	if(event.getSource() == communicationLogPopUp.getBtnOk()){
	
		glassPanel.show();
	    List<InteractionType> list = communicationLogPopUp.getCommunicationLogTable().getDataprovider().getList();
	   
	    ArrayList<InteractionType> interactionlist = new ArrayList<InteractionType>();
	    interactionlist.addAll(list);
	    
	    boolean checkNewInteraction = AppUtility.checkNewInteractionAdded(interactionlist);
	    
	    if(checkNewInteraction==false){
	    	alert.alert("Please add new interaction details");
	    	glassPanel.hide();
	    }	
	    else{	
	    	
	    	final Date followUpDate = interactionlist.get(interactionlist.size()-1).getInteractionDueDate();
	    	 communicationService.saveCommunicationLog(interactionlist, new AsyncCallback<Void>() {
	    		 
					@Override
					public void onFailure(Throwable caught) {
						alert.alert("Unexpected Error");
						glassPanel.hide();
						communicationPanel.hide();
					}

					@Override
					public void onSuccess(Void result) {
						alert.alert("Data Save Successfully");
						List<SalesQuotation> list = getDataprovider().getList();
						list.get(rowIndex).setFollowUpDate(followUpDate);
						SalesQuotation quotationObj = list.get(rowIndex);
						saveQuotation(quotationObj);
						followUpFlag = false;
						table.redraw();
						glassPanel.hide();
						communicationPanel.hide();
					}
				});
	    }
	   
	}
	if(event.getSource() == communicationLogPopUp.getBtnCancel()){
		followUpFlag = false;
		communicationPanel.hide();
	}
	if(event.getSource() == communicationLogPopUp.getBtnAdd()){
			glassPanel.show();
			String remark = communicationLogPopUp.getRemark().getValue();
			Date dueDate = communicationLogPopUp.getDueDate().getValue();
			String interactiongGroup =null;
			if(communicationLogPopUp.getOblinteractionGroup().getSelectedIndex()!=0)
				interactiongGroup=communicationLogPopUp.getOblinteractionGroup().getValue(communicationLogPopUp.getOblinteractionGroup().getSelectedIndex());
			boolean validationFlag = AppUtility.validateCommunicationlog(remark,dueDate);
			List<InteractionType> list = communicationLogPopUp.getCommunicationLogTable().getDataprovider().getList();
			Comparator<InteractionType> comp=new Comparator<InteractionType>() {
				@Override
				public int compare(InteractionType e1, InteractionType e2) {
					if (e1.getCount() == e2.getCount()) {
						return 0;
					}
					if (e1.getCount() > e2.getCount()) {
						return 1;
					} else {
						return -1;
					}
				}
			};
			Collections.sort(list,comp);
            
			if(validationFlag){
				InteractionType communicationLog =  AppUtility.getCommunicationLog(AppConstants.SALESMODULE,AppConstants.QUOTATION,quotation.getCount(),quotation.getEmployee(),remark,dueDate,quotation.getCinfo(),null,interactiongGroup, quotation.getBranch(),"");
				list.add(communicationLog);
				communicationLogPopUp.getCommunicationLogTable().getDataprovider().setList(list);
				communicationLogPopUp.getRemark().setValue("");
				communicationLogPopUp.getDueDate().setValue(null);
				communicationLogPopUp.getOblinteractionGroup().setSelectedIndex(0);
			}
			glassPanel.hide();
	}
	
}
	private void saveQuotation(SalesQuotation quotationObj) {
		service.save(quotationObj,new AsyncCallback<ReturnFromServer>() {
		@Override
		public void onFailure(Throwable caught) {
			glassPanel.hide();			
		    alert.alert("An Unexpected error occurred!");
		}
		@Override
		public void onSuccess(ReturnFromServer result) {
			glassPanel.hide();			
		    alert.alert("Saved successfully!");
		}
	});
		
	}
}
