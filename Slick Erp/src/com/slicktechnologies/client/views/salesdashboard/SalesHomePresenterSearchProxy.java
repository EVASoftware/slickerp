package com.slicktechnologies.client.views.salesdashboard;

import java.util.Vector;

import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.simplesoftwares.client.library.FlexForm;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appstructure.ViewContainer;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.deliverynote.DeliveryNote;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesorder.SalesQuotation;

public class SalesHomePresenterSearchProxy extends ViewContainer {
	
	protected PersonInfoComposite personInfo;
	protected MyQuerry salesquotation,salesorder,lead,deliverynote;
	protected PopupPanel popup;
	protected InlineLabel golbl;
	public HorizontalPanel horizontal;
	FlexForm form;

	public SalesHomePresenterSearchProxy()
	{
		super();
		createGui();
		applyStyle();
	}
	
	public void initWidget()
	{
		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new Customer());
		personInfo=new PersonInfoComposite(querry,false);
	}
	
	public void createScreen()
	{
		initWidget();
		FormFieldBuilder builder;
		builder = new FormFieldBuilder();
		FormField fblanktwo=builder.setMandatory(false).setColSpan(2).build();
		builder = new FormFieldBuilder();
		FormField fgroupingCustomerInformation=builder.setlabel("                              Customer").setMandatory(false).setColSpan(5).build();
		builder = new FormFieldBuilder("",personInfo);
		FormField fpersonInfo= builder.setMandatory(false).setRowSpan(1).setColSpan(5).build();
		FormField[][] fields=new FormField[][]{
//				{fgroupingCustomerInformation},
				{fpersonInfo}};
		form=new FlexForm(fields,FormStyle.ROWFORM);
		content.add(form);
		horizontal=new HorizontalPanel();
		//Go lbl
		golbl=new InlineLabel("Go");
		golbl.getElement().setId("addbutton");
		horizontal.add(golbl);
		horizontal.getElement().addClassName("centering");
	
		content.add(horizontal);
		
		popup=new PopupPanel(true);
		popup.add(content);
		
	
	}
	
	public void showPopUp()
	{
		//Clear the previous
		personInfo.clear();
		popup.center();
		popup.setSize("400px","200px");
		popup.getElement().setId("searchpopup");
	    popup.setAnimationEnabled(true);
	}
	
	
	public void hidePopUp()
	{
		popup.hide();
	}
	
	public void applyHandler(ClickHandler handler)
	{
		golbl.addClickHandler(handler);
	}
	
	public void createAllFilter1()
	 {
		 Vector<Filter>filterVec = new Vector<Filter>();
		 Filter temp;
		 if(personInfo.getId().getText().trim().equals("")==false)
			{
				temp=new Filter();
				temp.setLongValue(Long.parseLong(personInfo.getId().getValue()));
				temp.setQuerryString("personInfo.id");
				filterVec.add(temp);	
			}
			
			if((personInfo.getName().getText()).trim().equals("")==false)
			{
				temp=new Filter();
				temp.setStringValue(personInfo.getName().getValue());
				temp.setQuerryString("personInfo.fullName");
				filterVec.add(temp);	
			}
			
			if(personInfo.getPhone().getText().trim().equals("")==false)
			{
				temp=new Filter();
				temp.setLongValue(Long.parseLong(personInfo.getPhone().getValue()));
				temp.setQuerryString("personInfo.cellNumber");
				filterVec.add(temp);	
			}
			
			this.salesquotation=new MyQuerry();
			this.salesquotation.setFilters(filterVec);
			this.salesquotation.setQuerryObject(new SalesQuotation());
			
			this.lead=new MyQuerry();
			this.lead.setQuerryObject(new Lead());
			this.lead.setFilters(filterVec);
			
			this.salesorder=new MyQuerry();
			this.salesorder.setQuerryObject(new SalesOrder());
			this.salesorder.setFilters(filterVec);
			
			this.deliverynote=new MyQuerry();
			this.deliverynote.setQuerryObject(new DeliveryNote());
			this.deliverynote.setFilters(filterVec);
	}

	@Override
	protected void createGui() {
		createScreen();
	}

	@Override
	public void applyStyle() {
		content.getElement().setId("formcontent");
		form.getElement().setId("form");
	}


}
