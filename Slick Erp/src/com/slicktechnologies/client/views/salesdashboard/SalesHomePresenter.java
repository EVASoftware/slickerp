package com.slicktechnologies.client.views.salesdashboard;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.view.client.NoSelectionModel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.deliverynote.DeliveryNoteForm;
import com.slicktechnologies.client.views.deliverynote.DeliveryNotePresenter;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.client.views.lead.LeadForm;
import com.slicktechnologies.client.views.lead.LeadPresenter;
import com.slicktechnologies.client.views.quotation.SalesLineItemQuotationTable;
import com.slicktechnologies.client.views.salesorder.SalesOrderForm;
import com.slicktechnologies.client.views.salesorder.SalesOrderPresenter;
import com.slicktechnologies.client.views.salesquotation.SalesQuotationForm;
import com.slicktechnologies.client.views.salesquotation.SalesQuotationPresenter;
import com.slicktechnologies.shared.Sales;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.deliverynote.DeliveryNote;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesorder.SalesQuotation;

public class SalesHomePresenter implements ClickHandler {SalesHomeForm form;
SalesHomePresenterSearchProxy searchpopup;
protected GenricServiceAsync service=GWT.create(GenricService.class);

GeneralViewDocumentPopup gvPopup = null;

public  SalesHomePresenter(SalesHomeForm form,SalesHomePresenterSearchProxy searchpopup)
{
	this.form=form;
	this.searchpopup=searchpopup;
	SetEventHandling();
	searchpopup.applyHandler(this);
	form.getBtnGo().addClickHandler(this);
	

	Date date=new Date();
	System.out.println("Today Date ::::::::: "+date);
	// **** Here We Add 7 Days to current date and pass this date to default search.
	CalendarUtil.addDaysToDate(date,7);
    System.out.println("Changed Date + 7 ::::::::: "+date);
	
    Long compId=UserConfiguration.getCompanyId();
    System.out.println("Company Id :: "+compId);
	
	Vector<Filter> filtervec=null;
	Filter filter = null;
	MyQuerry querry;
//	List<String> statusList;
	filtervec=new Vector<Filter>();
	
	filter = new Filter();
	filter.setQuerryString("companyId");
	filter.setLongValue(compId);
	filtervec.add(filter);
	
	/**
	 * Old code
	 */
//	filter = new Filter();
//	filter.setQuerryString("status");
//	filter.setStringValue(Lead.CREATED);
//	filtervec.add(filter);
	
	/**
	 * Date : 02-08-2017 By ANIL
	 * Loading all the status of lead except Successful,Unsuccessful,Cancelled,Cancel,Rejected and Closed.
	 */
	List<String> statusList=new ArrayList<String>();
	statusList=AppUtility.getConfigValue(6);
	System.out.println("STATUS LIST SIZE:  "+statusList.size());
	
	for(int i=0;i<statusList.size();i++){
		if(statusList.get(i).trim().equalsIgnoreCase(AppConstants.Sucessful)
				/**
				 * UPdated By: Viraj
				 * Date: 10-01-2019
				 * Description: To remove successful status
				 */
				||statusList.get(i).trim().equalsIgnoreCase(AppConstants.Successful)
//				||statusList.get(i).trim().equalsIgnoreCase("successfull")
//				||statusList.get(i).trim().equalsIgnoreCase("unsuccessfull")
				/** Ends **/
				||statusList.get(i).trim().equalsIgnoreCase(AppConstants.Unsucessful)
				||statusList.get(i).trim().equalsIgnoreCase(AppConstants.CANCELLED)
				||statusList.get(i).trim().equalsIgnoreCase(AppConstants.CANCEL)
				||statusList.get(i).trim().equalsIgnoreCase(AppConstants.REJECTED)
				||statusList.get(i).trim().equalsIgnoreCase(AppConstants.CLOSED)
				||statusList.get(i).trim().equalsIgnoreCase("Completed")
				||statusList.get(i).trim().equalsIgnoreCase("Quotation Created")
				||statusList.get(i).trim().equalsIgnoreCase("Revised")){
			statusList.remove(i);
			i--;
		}
	}
	System.out.println("AFTER STATUS LIST SIZE:  "+statusList.size());
	filter = new Filter();
	filter.setQuerryString("status IN");
	filter.setList(statusList);
	filtervec.add(filter);
	/**
	 * End
	 */
	
	/**
	 * Developed by : Rohan added this branch  
	 */
		List<String> branchList ;
		branchList= new ArrayList<String>();
		System.out.println("item count in branch "+LoginPresenter.globalBranch.size());
		for (int i = 0; i < LoginPresenter.globalBranch.size(); i++) {
			branchList.add(LoginPresenter.globalBranch.get(i).getBusinessUnitName());
		}
		System.out.println("branchList list "+branchList.size());
	
		filter = new Filter();
		filter.setQuerryString("branch IN");
		filter.setList(branchList);
		filtervec.add(filter);
	/**
	 * ends here 
	 */
		/**
		 * date 10/3/2018 commented by komal to show records according to followup
		 * date 
		 **/
		//	filter = new Filter();
		//	filter.setQuerryString("creationDate <=");
		//	filter.setDateValue(date);
		//	filtervec.add(filter);
	
		/**
		 * date 10/3/2018 added by komal to show records according to followup
		 * date 
		 **/
		filter = new Filter();
		filter.setQuerryString("followUpDate <=");
		filter.setDateValue(date);
		filtervec.add(filter);
		/**
		 * end komal
		 */
	
	querry=new MyQuerry();
	querry.setQuerryObject(new Lead());
	querry.setFilters(filtervec);
	this.retriveTable(querry,form.getSalesLeadTable());
	
	////////////////////////////////////Create Querry For Quotation///////////////////////////// 

	
	filtervec=new Vector<Filter>();
	statusList=new ArrayList<String>();
	statusList.add(SalesQuotation.CREATED);
	statusList.add(SalesQuotation.REQUESTED);
	statusList.add(SalesQuotation.APPROVED);
	
	filter = new Filter();
	filter.setQuerryString("companyId");
	filter.setLongValue(compId);
	filtervec.add(filter);
	
	filter = new Filter();
	filter.setQuerryString("status IN");
	filter.setList(statusList);
	filtervec.add(filter);
	
	
	/**
	 * Developed by : Rohan added this branch  
	 */
		filter = new Filter();
		filter.setQuerryString("branch IN");
		filter.setList(branchList);
		filtervec.add(filter);
	
	/**
	 * ends here 
	 */
	
//		filter = new Filter();
			//	filter.setQuerryString("creationDate <=");
			//	filter.setDateValue(date);
			//	filtervec.add(filter);
			
			/**
			 * date 10/3/2018 added by komal to show records according to followup
			 * date
			 **/
			filter = new Filter();
			filter.setQuerryString("followUpDate <=");
			filter.setDateValue(date);
			filtervec.add(filter);
			/**
			 * end komal
			 */
	
    querry=new MyQuerry();
	querry.setQuerryObject(new SalesQuotation());
	querry.setFilters(filtervec);
	retriveTable(querry,form.getSalesQuotationTable());
	
	
	///////////Create Querry For Contract/////////////////////////////////////////////////////
	
	filtervec=new Vector<Filter>();
	statusList=new ArrayList<String>();
	statusList.add(SalesOrder.CREATED);
	statusList.add(SalesOrder.REQUESTED);
	
	filter = new Filter();
	filter.setQuerryString("companyId");
	filter.setLongValue(compId);
	filtervec.add(filter);
	
	filter = new Filter();
	filter.setQuerryString("status IN");
	filter.setList(statusList);
	filtervec.add(filter);
	
	/**
	 * Developed by : Rohan added this branch  
	 */
		filter = new Filter();
		filter.setQuerryString("branch IN");
		filter.setList(branchList);
		filtervec.add(filter);
	
	/**
	 * ends here 
	 */
	
	filter = new Filter();
	filter.setQuerryString("creationDate <=");
	filter.setDateValue(date);
	filtervec.add(filter);
	
	querry=new MyQuerry();
	querry.setQuerryObject(new SalesOrder());
	querry.setFilters(filtervec);
	retriveTable(querry,form.getSalesOrderTable());
	
	
	////////////////////Create Querry For Service//////////////////////
///////////Create Querry For Contract/////////////////////////////////////////////////////
	filtervec=new Vector<Filter>();
	statusList=new ArrayList<String>();
	statusList.add(DeliveryNote.CREATED);
	statusList.add(DeliveryNote.REQUESTED);
	statusList.add(DeliveryNote.APPROVED);
	
	filter = new Filter();
	filter.setQuerryString("companyId");
	filter.setLongValue(compId);
	filtervec.add(filter);
	
	filter = new Filter();
	filter.setQuerryString("status IN");
	filter.setList(statusList);
	filtervec.add(filter);
	
	/**
	 * Developed by : Rohan added this branch  
	 */
		filter = new Filter();
		filter.setQuerryString("branch IN");
		filter.setList(branchList);
		filtervec.add(filter);
	
	/**
	 * ends here 
	 */
	
	filter = new Filter();
	filter.setQuerryString("deliveryDate <=");
	filter.setDateValue(date);
	filtervec.add(filter);
	
	querry=new MyQuerry();
	querry.setQuerryObject(new DeliveryNote());
	querry.setFilters(filtervec);
	retriveTable(querry,form.getDeliveryNoteTable());
	
	setTableSelectionOnLeads();
	setTableSelectionOnSalesOrder();
	setTableQuotation();
	setTableSelectionOnDeliveryNote();
	}

public static void initalize()
{
	SalesHomeForm homeForm = new SalesHomeForm();
	AppMemory.getAppMemory().currentScreen=Screen.SALESHOME;
	AppMemory.getAppMemory().skeleton.getProcessName().setText("Dashboard/Home");
	SalesHomePresenterSearchProxy searchpopup=new SalesHomePresenterSearchProxy();
	SalesHomePresenter presenter= new SalesHomePresenter(homeForm,searchpopup);
	AppMemory.getAppMemory().stickPnel(homeForm);	
}


@Override
public void onClick(ClickEvent event) {
	
	if(event.getSource() instanceof InlineLabel)
	{
	InlineLabel lbl= (InlineLabel) event.getSource();
	String text= lbl.getText();
	if(text.equals("Search"))
		searchpopup.showPopUp();
	
	if(text.equals("Go"))
		reactOnGo();
	}
	
//	if(event.getSource().equals(form.getBtnGo())){if(form.getDbFromDate().getValue()==null||form.getDbToDate().getValue()==null){
//		final GWTCAlert alert = new GWTCAlert();
//		alert.alert("Select From and To Date.");
//	}
//	if(form.getDbFromDate().getValue()!=null&&form.getDbToDate()!=null){
//		Date formDate=form.getDbFromDate().getValue();
//		Date toDate=form.getDbToDate().getValue();
//		if(toDate.equals(formDate)||toDate.after(formDate)){
//			System.out.println("From Date ::::::::: "+formDate);
//			CalendarUtil.addDaysToDate(formDate,7);
//			System.out.println("Changed Date ::::::::: "+formDate);
//			System.out.println("To Date ::::::::: "+toDate);
//		
//			if(toDate.before(formDate)){
//				System.out.println("Inside Date Condition"); 
//				searchByFromToDate();
//			}
//			else{
//				final GWTCAlert alert = new GWTCAlert();
//				alert.alert("From Date and To Date Difference Can Not Be More Than 7 Days.");
//			}
//		}
//		else{
//			final GWTCAlert alert = new GWTCAlert();
//			alert.alert("To Date should be greater than From Date.");
//		}
//	}}
		
	
	if(event.getSource().equals(form.getBtnGo())){
		if(form.getOlbbranch().getSelectedIndex()==0&&
		form.getOlbsalesPerson().getSelectedIndex()==0&&
		form.getDbFromDate().getValue()==null&&form.getDbToDate().getValue()==null){
	
		final GWTCAlert alert = new GWTCAlert();
		alert.alert("Select atleast one field to search");
	}
		
	else{	
		
		if(form.getOlbsalesPerson().getSelectedIndex()!=0&&form.getOlbbranch().getSelectedIndex()==0
		   &&( form.getDbFromDate().getValue()==null&&form.getDbToDate()!=null)){
			
			System.out.println("salesperson ");
			searchByFromToDate();
			
		}
		else if(form.getOlbsalesPerson().getSelectedIndex()==0&&form.getOlbbranch().getSelectedIndex()!=0
				   &&( form.getDbFromDate().getValue()==null&&form.getDbToDate()!=null)){
			
			System.out.println("branch ");
			searchByFromToDate();
		}
		else if(form.getOlbsalesPerson().getSelectedIndex()!=0&&form.getOlbbranch().getSelectedIndex()!=0
				   &&( form.getDbFromDate().getValue()==null&&form.getDbToDate()!=null)){
			
			System.out.println("for both sales person and branch");
			searchByFromToDate();
		}
		else{
		System.out.println("Remaining all situations ");
		
		if(form.getDbFromDate().getValue()==null||form.getDbToDate().getValue()==null){
			final GWTCAlert alert = new GWTCAlert();
			alert.alert("Select From and To Date.");
		}
		 if(form.getDbFromDate().getValue()!=null&&form.getDbToDate()!=null){
		Date formDate=form.getDbFromDate().getValue();
		Date toDate=form.getDbToDate().getValue();
		if(toDate.equals(formDate)||toDate.after(formDate)){
			System.out.println("From Date ::::::::: "+formDate);
			CalendarUtil.addDaysToDate(formDate,7);
			System.out.println("Changed Date ::::::::: "+formDate);
			System.out.println("To Date ::::::::: "+toDate);
		
			if(toDate.before(formDate)){
				System.out.println("Inside Date Condition"); 
				searchByFromToDate();
			}
			else{
				final GWTCAlert alert = new GWTCAlert();
				alert.alert("From Date and To Date Difference Can Not Be More Than 7 Days.");
			}
		}
		else{
			final GWTCAlert alert = new GWTCAlert();
			alert.alert("To Date should be greater than From Date.");
		}
		}
		}
	}
	}
	
}

/**
 * Sets the event handling.
 */
public void SetEventHandling()
{
	 InlineLabel label[] =AppMemory.getAppMemory().skeleton.getMenuLabels();
	  for(int k=0;k<label.length;k++)
	  {
		 if( AppMemory.getAppMemory().skeleton.registration[k]!=null)
		    AppMemory.getAppMemory().skeleton.registration[k].removeHandler();
		 
			 
	  }
	 
	  for( int k=0;k<label.length;k++)
	  {
		  
		  AppMemory.getAppMemory().skeleton.registration[k]= label[k].addClickHandler(this);
	  }
}


public void reactOnGo()
{ 
	
	if(!searchpopup.personInfo.getId().getValue().equals("")&&!searchpopup.personInfo.getName().getValue().equals("")&&!searchpopup.personInfo.getPhone().getValue().equals(""))
	{

		MyQuerry quer=createSalesFilter(new SalesOrder());
		 this.form.salesOrderTable.connectToLocal();
		retriveTable(quer, form.getSalesOrderTable());
		 
		quer=createLeadFilter();
		 this.form.salesLeadTable.connectToLocal();
		 retriveTable(quer, form.getSalesLeadTable());
		 
		 quer=createSalesFilter(new SalesQuotation());
		 this.form.salesQuotationTable.connectToLocal();
		 retriveTable(quer, form.getSalesQuotationTable());
		 
		 quer=createDeliveryNoteFilter(new DeliveryNote());
		 this.form.deliveryNoteTable.connectToLocal();
		 retriveTable(quer, form.getDeliveryNoteTable());
		 searchpopup.hidePopUp();
	}
	
	if(searchpopup.personInfo.getId().getValue().equals("")&&searchpopup.personInfo.getName().getValue().equals("")&&searchpopup.personInfo.getPhone().getValue().equals(""))
	{
		GWTCAlert alertMsg=new GWTCAlert();
		alertMsg.alert("Please enter customer information");
	}
	
	
	 
}

private MyQuerry createLeadFilter()
{
	Vector<Filter>filterVec = new Vector<Filter>();
	 Filter temp;
	 
	 if(searchpopup.personInfo.getId().getText().trim().equals("")==false)
		{
			temp=new Filter();
			temp.setLongValue(Long.parseLong(searchpopup.personInfo.getId().getValue()));
			temp.setQuerryString("personInfo.count");
			filterVec.add(temp);	
		}
		
		if((searchpopup.personInfo.getName().getText()).trim().equals("")==false)
		{
			temp=new Filter();
			temp.setStringValue(searchpopup.personInfo.getName().getValue());
			temp.setQuerryString("personInfo.fullName");
			filterVec.add(temp);	
		}
		
		if(searchpopup.personInfo.getPhone().getText().trim().equals("")==false)
		{
			temp=new Filter();
			temp.setLongValue(Long.parseLong(searchpopup.personInfo.getPhone().getValue()));
			temp.setQuerryString("personInfo.cellNumber");
			filterVec.add(temp);	
		}
		
		MyQuerry lead=new MyQuerry();
		lead.setQuerryObject(new Lead());
		lead.setFilters(filterVec);
		
		return lead;
}

private MyQuerry createSalesFilter(Sales sales)
{
	Vector<Filter>filterVec = new Vector<Filter>();
	 Filter temp;
	 if(searchpopup.personInfo.getId().getText().trim().equals("")==false)
		{
			temp=new Filter();
			temp.setLongValue(Long.parseLong(searchpopup.personInfo.getId().getValue()));
			temp.setQuerryString("cinfo.count");
			filterVec.add(temp);	
		}
		
		if((searchpopup.personInfo.getName().getText()).trim().equals("")==false)
		{
			temp=new Filter();
			temp.setStringValue(searchpopup.personInfo.getName().getValue());
			temp.setQuerryString("cinfo.fullName");
			filterVec.add(temp);	
		}
		
		if(searchpopup.personInfo.getPhone().getText().trim().equals("")==false)
		{
			temp=new Filter();
			temp.setLongValue(Long.parseLong(searchpopup.personInfo.getPhone().getValue()));
			temp.setQuerryString("cinfo.cellNumber");
			filterVec.add(temp);	
		}
		
		MyQuerry quotation=new MyQuerry();
		quotation.setFilters(filterVec);
		quotation.setQuerryObject(sales);
		
		return quotation;
}


private MyQuerry createDeliveryNoteFilter(DeliveryNote sales)
{
	Vector<Filter>filterVec = new Vector<Filter>();
	 Filter temp;
	 if(searchpopup.personInfo.getId().getText().trim().equals("")==false)
		{
			temp=new Filter();
			temp.setLongValue(Long.parseLong(searchpopup.personInfo.getId().getValue()));
			temp.setQuerryString("cinfo.count");
			filterVec.add(temp);	
		}
		
		if((searchpopup.personInfo.getName().getText()).trim().equals("")==false)
		{
			temp=new Filter();
			temp.setStringValue(searchpopup.personInfo.getName().getValue());
			temp.setQuerryString("cinfo.fullName");
			filterVec.add(temp);	
		}
		
		if(searchpopup.personInfo.getPhone().getText().trim().equals("")==false)
		{
			temp=new Filter();
			temp.setLongValue(Long.parseLong(searchpopup.personInfo.getPhone().getValue()));
			temp.setQuerryString("cinfo.cellNumber");
			filterVec.add(temp);	
		}
		
		MyQuerry quotation=new MyQuerry();
		quotation.setFilters(filterVec);
		quotation.setQuerryObject(sales);
		
		return quotation;
}

public void setContractRedirection()
 {
 }

 public void setLeadRedirection()
 {
 }
 
 public void setServiceRedirection()
 {
 }
 
 public void setQuotationRedirection()
 {
 }	
 
 public void setTableSelectionOnLeads()
 {
	 final NoSelectionModel<Lead> selectionModelMyObj = new NoSelectionModel<Lead>();
	 
     SelectionChangeEvent.Handler  tableHandler = new SelectionChangeEvent.Handler() 
     {
         @Override
         public void onSelectionChange(SelectionChangeEvent event) 
         {         	 
        	  Timer timer1 = new Timer(){

   				@Override
   				public void run() {
   					if(SalesLeadPresenterTableProxy.followUpFlag == false){
   						/**
   						 * Updated By: Viraj
   						 * Date: 13-04-2019
   						 * Description: To show lead details in popup with edit 
   						 */
//   						 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Sales/Lead",Screen.SALESLEAD);
//   			        	 final Lead entity=selectionModelMyObj.getLastSelectedObject();
//   			        	 final LeadForm form = LeadPresenter.initalize();
//   			        	  
//   							form.showWaitSymbol();
//   							Timer timer=new Timer() {
//   			 				
//   			 				@Override
//   			 				public void run() {
//   			 					form.hideWaitSymbol();
//   							     form.updateView(entity);
//   							     form.setToViewState();
//   							     form.getPic().setEnabled(false);
//   			 				}
//   			 			};
//   			             timer.schedule(5000); 
   						AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Sales/Lead",Screen.SALESLEAD);
   						final Lead entity=selectionModelMyObj.getLastSelectedObject();
//			        	final LeadForm form = LeadPresenter.initalize();
   						
//			        	form.content.getElement().getStyle().setHeight(570, Unit.PX);
//			    		form.content.getElement().getStyle().setWidth(950, Unit.PX);
			        	
   						gvPopup = new GeneralViewDocumentPopup();
   						gvPopup.setModel(AppConstants.LEAD, entity);
   						/** Ends **/
   					}else{
   						SalesLeadPresenterTableProxy.followUpFlag = false;
   					}
   				}
        	  };
        	  timer1.schedule(1000);
         }
        	   
//        	 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Sales/Lead",Screen.SALESLEAD);
//        	 final Lead entity=selectionModelMyObj.getLastSelectedObject();
//        	 final LeadForm form = LeadPresenter.initalize();
//        	  
//				form.showWaitSymbol();
//        	 Timer timer=new Timer() {
// 				
// 				@Override
// 				public void run() {
// 					form.hideWaitSymbol();
//				     form.updateView(entity);
//				     form.setToViewState();
//				     form.getPic().setEnabled(false);
// 				}
// 			};
//            
//             timer.schedule(5000); 
//             
//          }
     };
     // Add the handler to the selection model
     selectionModelMyObj.addSelectionChangeHandler( tableHandler );
     // Add the selection model to the table
     form.salesLeadTable.getTable().setSelectionModel(selectionModelMyObj);
	 
	 
 }
 
 private void setTableSelectionOnDeliveryNote()
 {
	 final NoSelectionModel<DeliveryNote> selectionModelMyObj = new NoSelectionModel<DeliveryNote>();
     
     SelectionChangeEvent.Handler  tableHandler = new SelectionChangeEvent.Handler() 
     {
         @Override
         public void onSelectionChange(SelectionChangeEvent event) 
         {
        	 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Sales/Delivery Note",Screen.DELIVERYNOTE);
        	 final DeliveryNote entity=selectionModelMyObj.getLastSelectedObject();
        	 final DeliveryNoteForm form = DeliveryNotePresenter.initalize();
			 form.showWaitSymbol();
			 AppMemory.getAppMemory().stickPnel(form);
        	 Timer timer=new Timer() {
 				
 				@Override
 				public void run() {
 					 form.hideWaitSymbol();
				     form.updateView(entity);
				     form.setToViewState();
 				}
 			};
            
             timer.schedule(5000); 
                         
          }
     };
     // Add the handler to the selection model
     selectionModelMyObj.addSelectionChangeHandler( tableHandler );
     // Add the selection model to the table
     form.deliveryNoteTable.getTable().setSelectionModel(selectionModelMyObj);

 }
 public void setTableSelectionOnSalesOrder()
 {
	 final NoSelectionModel<SalesOrder> selectionModelMyObj = new NoSelectionModel<SalesOrder>();
     
     SelectionChangeEvent.Handler  tableHandler = new SelectionChangeEvent.Handler() 
     {
         @Override
         public void onSelectionChange(SelectionChangeEvent event) 
         {
        	 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Sales/Sales Order",Screen.SALESORDER);
        	 final SalesOrder entity=selectionModelMyObj.getLastSelectedObject();
        	 final SalesOrderForm form = SalesOrderPresenter.initalize();
				 form.showWaitSymbol();
			 AppMemory.getAppMemory().stickPnel(form);
        	 Timer timer=new Timer() {
 				
 				@Override
 				public void run() {
 					 form.hideWaitSymbol();
 					 form.getPersonInfoComposite().setEnable(false);
				     form.updateView(entity);
				     form.setToViewState();
 				}
 			};
             timer.schedule(5000); 
          }
     };
     // Add the handler to the selection model
     selectionModelMyObj.addSelectionChangeHandler( tableHandler );
     // Add the selection model to the table
     form.salesOrderTable.getTable().setSelectionModel(selectionModelMyObj);
     
 }
 
 
 public void setTableQuotation()
 {
	 final NoSelectionModel<SalesQuotation> selectionModelMyObj = new NoSelectionModel<SalesQuotation>();
     
     SelectionChangeEvent.Handler  tableHandler = new SelectionChangeEvent.Handler() 
     {
         @Override
         public void onSelectionChange(SelectionChangeEvent event) 
         {

        	 Timer timer1 = new Timer(){

				@Override
				public void run() {
					if(SalesQuotationPresenterTableProxy.followUpFlag == false){
						
			        	 setQuotationRedirection();
			             AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Sales/Quotation",Screen.SALESQUOTATION);
			             final SalesQuotation entity=selectionModelMyObj.getLastSelectedObject();
			             SalesLineItemQuotationTable.newModeFlag=true;
			             final SalesQuotationForm form = SalesQuotationPresenter.initalize();			             
			             form.showWaitSymbol();
			             Timer timer=new Timer() {
							@Override
							public void run() {
							     form.hideWaitSymbol();
							     form.getPersonInfoComposite().setEnable(false);
							     form.updateView(entity);
							     form.setToViewState();
							}
						};
			            timer.schedule(5000); 
				}
				else
				 {
					 SalesQuotationPresenterTableProxy.followUpFlag = false;
				 }	
				}
        	 };
        	 timer1.schedule(1000);
          
            
//        	 setQuotationRedirection();
//            
//            
//             AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Sales/Quotation",Screen.SALESQUOTATION);
//             final SalesQuotation entity=selectionModelMyObj.getLastSelectedObject();
//             final SalesQuotationForm form = SalesQuotationPresenter.initalize();
//             
//             form.showWaitSymbol();
//             Timer timer=new Timer() {
//				
//				@Override
//				public void run() {
//					     
//					     form.hideWaitSymbol();
//					     form.getPersonInfoComposite().setEnable(false);
//					     form.updateView(entity);
//					     form.setToViewState();
//				}
//			};
//            timer.schedule(5000); 
          }
     };
     // Add the handler to the selection model
     selectionModelMyObj.addSelectionChangeHandler( tableHandler );
     // Add the selection model to the table
     form.salesQuotationTable.getTable().setSelectionModel(selectionModelMyObj);
	 
	 
 }
 
 
 private void searchByFromToDate(){
	 Vector<Filter> filtervec=null;
		Filter filter = null;
		MyQuerry querry;
		List<String> statusList;
		
		Long compId=UserConfiguration.getCompanyId();
	    System.out.println("Company Id :: "+compId);
		
		filtervec=new Vector<Filter>();
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(compId);
		filtervec.add(filter);
		
//		/**
//		 * Updated By: Viraj
//		 * Date: 10-01-2019
//		 * Description: To add Status filter in dashboard
//		 */
//		if(form.getOlbLeadStatus().getSelectedIndex() != 0) {
//			filter = new Filter();
//			filter.setQuerryString("status");
//			filter.setStringValue(form.getOlbLeadStatus().getValue());
//			filtervec.add(filter);
//		} else {
			filter = new Filter();
			filter.setQuerryString("status");
			filter.setStringValue(Lead.CREATED);
			filtervec.add(filter);
//		}
		/** Ends **/
		
		if(form.getDbFromDate().getValue()!=null){
		filter = new Filter();
		filter.setQuerryString("creationDate >=");
		filter.setDateValue(form.getDbFromDate().getValue());
		filtervec.add(filter);
		}
		
		if(form.getDbToDate().getValue()!=null){
		filter = new Filter();
		filter.setQuerryString("creationDate <=");
		filter.setDateValue(form.getDbToDate().getValue());
		filtervec.add(filter);
		}
		
		if(form.getOlbbranch().getSelectedIndex()!=0){
			System.out.println("inside lead***********");
				filter = new Filter();
				filter.setQuerryString("branch");
				filter.setStringValue(form.getOlbbranch().getValue());
				filtervec.add(filter);
				
			}
			
			if(form.getOlbsalesPerson().getSelectedIndex()!=0){
				System.out.println("for service    ");
				filter = new Filter();
				filter.setQuerryString("employee");
				filter.setStringValue(form.getOlbsalesPerson().getValue());
				filtervec.add(filter);
			}
		
		
		querry=new MyQuerry();
		querry.setQuerryObject(new Lead());
		querry.setFilters(filtervec);
		form.getSalesLeadTable().connectToLocal();
		this.retriveTable(querry,form.getSalesLeadTable());
		
		////////////////////////////////////Create Querry For Quotation///////////////////////////// 
		
		filtervec=new Vector<Filter>();
		statusList=new ArrayList<String>();
		statusList.add(SalesQuotation.CREATED);
		statusList.add(SalesQuotation.REQUESTED);
		statusList.add(SalesQuotation.APPROVED);
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(compId);
		filtervec.add(filter);
		
//		/**
//		 * Updated By: Viraj
//		 * Date: 10-01-2019
//		 * Description: To add Status filter in dashboard
//		 */
//		if(form.getOlbLeadStatus().getSelectedIndex() != 0) {
//			filter = new Filter();
//			filter.setQuerryString("status");
//			filter.setStringValue(form.getOlbLeadStatus().getValue());
//			filtervec.add(filter);
//		} else {
			filter = new Filter();
			filter.setQuerryString("status IN");
			filter.setList(statusList);
			filtervec.add(filter);
//		}
		/** Ends **/
		
		if(form.getDbFromDate().getValue()!=null){
		filter = new Filter();
		filter.setQuerryString("creationDate >=");
		filter.setDateValue(form.getDbFromDate().getValue());
		filtervec.add(filter);
		}
		
		if(form.getDbToDate().getValue()!=null){
		filter = new Filter();
		filter.setQuerryString("creationDate <=");
		filter.setDateValue(form.getDbToDate().getValue());
		filtervec.add(filter);
		}
		
		if(form.getOlbbranch().getSelectedIndex()!=0){
			System.out.println("inside lead***********");
				filter = new Filter();
				filter.setQuerryString("branch");
				filter.setStringValue(form.getOlbbranch().getValue());
				filtervec.add(filter);
				
			}
			
			if(form.getOlbsalesPerson().getSelectedIndex()!=0){
				System.out.println("for service    ");
				filter = new Filter();
				filter.setQuerryString("employee");
				filter.setStringValue(form.getOlbsalesPerson().getValue());
				filtervec.add(filter);
			}
		
		
	    querry=new MyQuerry();
		querry.setQuerryObject(new SalesQuotation());
		querry.setFilters(filtervec);
		form.getSalesQuotationTable().connectToLocal();
		retriveTable(querry,form.getSalesQuotationTable());
		
		///////////Create Querry For Contract/////////////////////////////////////////////////////
		
		filtervec=new Vector<Filter>();
		statusList=new ArrayList<String>();
		statusList.add(SalesOrder.CREATED);
		statusList.add(SalesOrder.REQUESTED);
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(compId);
		filtervec.add(filter);
		
//		/**
//		 * Updated By: Viraj
//		 * Date: 10-01-2019
//		 * Description: To add Status filter in dashboard
//		 */
//		if(form.getOlbLeadStatus().getSelectedIndex() != 0) {
//			filter = new Filter();
//			filter.setQuerryString("status");
//			filter.setStringValue(form.getOlbLeadStatus().getValue());
//			filtervec.add(filter);
//		} else {
			filter = new Filter();
			filter.setQuerryString("status IN");
			filter.setList(statusList);
			filtervec.add(filter);
//		}
		/** Ends **/
		
		if(form.getDbFromDate().getValue()!=null){
		filter = new Filter();
		filter.setQuerryString("creationDate >=");
		filter.setDateValue(form.getDbFromDate().getValue());
		filtervec.add(filter);
		}
		
		if(form.getDbToDate().getValue()!=null){
		filter = new Filter();
		filter.setQuerryString("creationDate <=");
		filter.setDateValue(form.getDbToDate().getValue());
		filtervec.add(filter);
		}
		
		
		if(form.getOlbbranch().getSelectedIndex()!=0){
			System.out.println("inside lead***********");
				filter = new Filter();
				filter.setQuerryString("branch");
				filter.setStringValue(form.getOlbbranch().getValue());
				filtervec.add(filter);
				
			}
			
			if(form.getOlbsalesPerson().getSelectedIndex()!=0){
				System.out.println("for service    ");
				filter = new Filter();
				filter.setQuerryString("employee");
				filter.setStringValue(form.getOlbsalesPerson().getValue());
				filtervec.add(filter);
			}
		
		querry=new MyQuerry();
		querry.setQuerryObject(new SalesOrder());
		querry.setFilters(filtervec);
		form.getSalesOrderTable().connectToLocal();
		retriveTable(querry,form.getSalesOrderTable());
		
		////////////////////Create Querry For Delivery Note//////////////////////
		
		filtervec=new Vector<Filter>();
		statusList=new ArrayList<String>();
		statusList.add(DeliveryNote.CREATED);
		statusList.add(DeliveryNote.REQUESTED);
		statusList.add(DeliveryNote.APPROVED);
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(compId);
		filtervec.add(filter);
		
//		/**
//		 * Updated By: Viraj
//		 * Date: 10-01-2019
//		 * Description: To add Status filter in dashboard
//		 */
//		if(form.getOlbLeadStatus().getSelectedIndex() != 0) {
//			filter = new Filter();
//			filter.setQuerryString("status");
//			filter.setStringValue(form.getOlbLeadStatus().getValue());
//			filtervec.add(filter);
//		} else {
			filter = new Filter();
			filter.setQuerryString("status IN");
			filter.setList(statusList);
			filtervec.add(filter);
//		}
		/** Ends **/
		
		
		if(form.getDbFromDate().getValue()!=null){
		filter = new Filter();
		filter.setQuerryString("deliveryDate >=");
		filter.setDateValue(form.getDbFromDate().getValue());
		filtervec.add(filter);
		}
		
		if(form.getDbToDate().getValue()!=null){
		filter = new Filter();
		filter.setQuerryString("deliveryDate <=");
		filter.setDateValue(form.getDbToDate().getValue());
		filtervec.add(filter);
		}
		
		
		if(form.getOlbbranch().getSelectedIndex()!=0){
			System.out.println("inside lead***********");
				filter = new Filter();
				filter.setQuerryString("branch");
				filter.setStringValue(form.getOlbbranch().getValue());
				filtervec.add(filter);
				
			}
			
			if(form.getOlbsalesPerson().getSelectedIndex()!=0){
				System.out.println("for service    ");
				filter = new Filter();
				filter.setQuerryString("employee");
				filter.setStringValue(form.getOlbsalesPerson().getValue());
				filtervec.add(filter);
			}
		
		querry=new MyQuerry();
		querry.setQuerryObject(new DeliveryNote());
		querry.getFilters().add(filter);
		form.getDeliveryNoteTable().connectToLocal();
		retriveTable(querry,form.getDeliveryNoteTable());
		
	}
 
 


 public <T> void retriveTable(MyQuerry querry,final SuperTable<T>table)
 {
	 
			table.connectToLocal();
			service.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					
					for(SuperModel model:result)
					{
						table.getListDataProvider().getList().add((T) model);
					}
				}
				
				@Override
				public void onFailure(Throwable caught) {
				caught.printStackTrace();
					
				}
			}); 
 }}
