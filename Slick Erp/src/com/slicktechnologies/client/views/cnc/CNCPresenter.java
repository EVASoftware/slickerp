package com.slicktechnologies.client.views.cnc;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.Vector;
import java.util.logging.Level;

import org.apache.poi.hssf.record.DBCellRecord;
import org.apache.poi.ss.formula.ptg.TblPtg;

import com.google.appengine.labs.repackaged.com.google.common.collect.Table;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.approvalutility.ApprovableFormScreenPresenter;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.EmailService;
import com.slicktechnologies.client.services.EmailServiceAsync;
import com.slicktechnologies.client.services.XlsxService;
import com.slicktechnologies.client.services.XlsxServiceAsync;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.contract.ContractPresenter;
import com.slicktechnologies.client.views.contract.CustomerAddressPopup;
import com.slicktechnologies.client.views.documentcancellation.DocumentCancellationPopUp;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.BillingDetailsForm;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.BillingDetailsPresenter;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.FromAndToDateBoxPopup;
import com.slicktechnologies.client.views.paymentinfo.billinglist.BillingListPresenter;
import com.slicktechnologies.client.views.popups.VersionPopup;
import com.slicktechnologies.client.views.projectallocation.DepartmentPopup;
import com.slicktechnologies.client.views.projectallocation.ProjectAllocationForm;
import com.slicktechnologies.client.views.projectallocation.ProjectAllocationPresenter;
import com.slicktechnologies.client.views.projectallocation.ProjectAllocationPresenterSearchProxy;
import com.slicktechnologies.client.views.projectallocation.ProjectAllocationPresenterTableProxy;
import com.slicktechnologies.client.views.salesorder.SalesOrderForm;
import com.slicktechnologies.client.views.salesorder.SalesOrderPresenter;
import com.slicktechnologies.client.views.projectallocation.ProjectAllocationPresenter.ProjectAllocationPresenterSearch;
import com.slicktechnologies.client.views.projectallocation.ProjectAllocationPresenter.ProjectAllocationPresenterTable;
import com.slicktechnologies.server.Email;
import com.slicktechnologies.shared.CNCArrearsDetails;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.ProjectAllocation;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cnc.CNC;
import com.slicktechnologies.shared.common.cnc.CNCVersion;
import com.slicktechnologies.shared.common.cnc.Consumables;
import com.slicktechnologies.shared.common.cnc.EquipmentRental;
import com.slicktechnologies.shared.common.cnc.OtherServices;
import com.slicktechnologies.shared.common.cnc.StaffingDetails;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.Department;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTCTemplate;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;

public class CNCPresenter extends ApprovableFormScreenPresenter<CNC> implements RowCountChangeEvent.Handler,ChangeHandler ,SelectionHandler<Suggestion>, ClickHandler, ValueChangeHandler<Date>{


	public static CNCForm form;
	CsvServiceAsync csvservice = GWT.create(CsvService.class);

	/**
	 * Date : 08-06-2018 By ANIL
	 */
	FromAndToDateBoxPopup fromToDatePopup=new FromAndToDateBoxPopup("Staffing and Rental Different");
	PopupPanel panel=new PopupPanel(true);
	CncBillServiceAsync cncBillService = GWT.create(CncBillService.class);
	/** date 8.8.18 added by komal for cost sheet **/
	XlsxServiceAsync xlxWriter = GWT.create(XlsxService.class);
	VersionPopup versionPopup = new VersionPopup();
	GenricServiceAsync service = GWT.create(GenricService.class);
	/** date 9.10.2018 added by komal for email**/
	DepartmentPopup departmentPopup = new DepartmentPopup();
	EmailServiceAsync email = GWT.create(EmailService.class);
	DocumentCancellationPopUp popup = new DocumentCancellationPopUp();
	PopupPanel cancelpanel;
	/**Date 25-6-2019 
	 * by Amol make project name enable by process configuration raised by Rahul for riseon
	 */
	boolean projectNameEnable=false;
	CNCArrearsBillPopup cNCArrearsBillPopup = new CNCArrearsBillPopup();
	public CustomerAddressPopup custAddresspopup = new CustomerAddressPopup();
	InlineLabel lblUpdate = new InlineLabel("Update");
	final GenricServiceAsync genasync = GWT.create(GenricService.class);
	
	public static CNCForm initalize() {
		
		CNCForm form = new CNCForm();
		CNCPresenterTable gentable = new CNCPresenterTableProxy();
		gentable.setView(form);
		gentable.applySelectionModle();
		CNCPresenterSearch.staticSuperTable = gentable;
		CNCPresenterSearch searchpopup = new CNCPresenterSearchProxy();
		form.setSearchpopupscreen(searchpopup);
		new CNCPresenter(form,new CNC());
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/CNC",Screen.CNC);
		AppMemory.getAppMemory().stickPnel(form);
		return form;
	}
	
	public CNCPresenter(FormScreen<CNC> view, CNC model) {
		super(view, model);
		form = (CNCForm) view;
		form.setPresenter(this);
		form.personInfoComposite.getId().addSelectionHandler(this);
		form.personInfoComposite.getName().addSelectionHandler(this);
		form.personInfoComposite.getPhone().addSelectionHandler(this);
		form.bt_addStaffingDetails.addClickHandler(this);
		form.bt_addEquipmentDetails.addClickHandler(this);
		form.bt_consumablesDetails.addClickHandler(this);
		form.bt_otherServices.addClickHandler(this);
		form.oblCustomerBranch.addChangeHandler(this);
		custAddresspopup.getLblCancel().addClickHandler(this);
		custAddresspopup.getLblOk().addClickHandler(this);
		lblUpdate.addClickHandler(this);
		/**
		 * Date : 08-06-2018 BY ANIL
		 */
	//	fromToDatePopup.getBtnOne().addClickHandler(this);
	//	fromToDatePopup.getBtnTwo().addClickHandler(this);
		form.getStaffingTable().getTable().addRowCountChangeHandler(this);
		form.getEquipmentRentalTable().getTable().addRowCountChangeHandler(this);
		form.getConsumableTable().getTable().addRowCountChangeHandler(this);
		form.getOtherServiceTable().getTable().addRowCountChangeHandler(this);
		/** date 24.3.2018 added by komal**/
		versionPopup.getLblOk().addClickHandler(this);
		versionPopup.getLblCancel().addClickHandler(this);
		/** date 9.10.2018 added by komal for email**/
		departmentPopup.getLblOk().addClickHandler(this);
		departmentPopup.getLblCancel().addClickHandler(this);
		
		popup.getBtnOk().addClickHandler(this);
		popup.getBtnCancel().addClickHandler(this);
		/**
		 * Updated By: Viraj
		 * Date: 23-02-2019
		 * Description: To validate on value change
		 */
		form.getDbContractEndDate().addValueChangeHandler(this);
		form.getDbContractStartDate().addValueChangeHandler(this);
		
		cNCArrearsBillPopup.getLblOk().addClickHandler(this);
		cNCArrearsBillPopup.getLblCancel().addClickHandler(this);
	}

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void makeNewModel() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void onSelection(SelectionEvent<Suggestion> event) {
		if (event.getSource().equals(form.getPersonInfoComposite().getId())
				|| event.getSource().equals(
						form.getPersonInfoComposite().getName())
				|| event.getSource().equals(
						form.getPersonInfoComposite().getPhone())) {
			form.oblCustomerBranch.clear();
			form.loadCustomerBranch(Integer.parseInt(form.getPersonInfoComposite().getId().getValue().trim()), null,true);
		  getCustomerAddress();
		}
		
	}
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		super.onClick(event);
		if(event.getSource()==form.bt_addStaffingDetails){
			 StaffingDetails staffingDetails=new StaffingDetails();
			/**
			 * @author Anil , Date : 06-04-2019
			 */
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CNC","AutoCalculatePtAndLwf")){
				if (form.olbbBranch.getValue() == null|| form.olbbBranch.getSelectedIndex() == 0) {
					form.showDialogMessage("Please select branch.");
					return;
				}
				Branch branch=form.olbbBranch.getSelectedItem();
//				staffingDetails.setSpMWF(0+"");
//				staffingDetails.setSdLWF(0+"");
//				staffingDetails.setSdPT(0+"");
				form.staffingTable.setState(branch.getAddress().getState());
				form.staffingTable.ptAndLwfDropdownFlag=true;
				
				/**
				 * @author Anil
				 * @since 03-09-2020
				 * addStateFlag
				 */
				if(form.addStateFlag){
					if(form.oblState.getSelectedIndex()==0){
						form.showDialogMessage("Please select state.");
						return;
					}
					form.staffingTable.setState(form.oblState.getValue());
				}
				System.out.println("Selected branchs state : "+form.staffingTable.getState());
			}
			
			double cBonusPercent=0,managementFees=0;
			if(form.getTbcBonus().getValue()!=null&&!form.getTbcBonus().getValue().equals("")){
				try{
					cBonusPercent=Double.parseDouble(form.getTbcBonus().getValue().trim());
				}catch(Exception e){
					e.printStackTrace();
					cBonusPercent=0;
				}
			}
			if(form.getTbManagementFees().getValue()!=null&&!form.getTbManagementFees().getValue().equals("")){
				try{
					managementFees=Double.parseDouble(form.getTbManagementFees().getValue().trim());
				}catch(Exception e){
					e.printStackTrace();
					managementFees=0;
				}
			}
			staffingDetails.setcBonusPercentage(cBonusPercent);
			staffingDetails.setManageMentfeesPercent(managementFees);
			staffingDetails.setBonusRelever(form.getCb_Relever_for_Bonus().getValue());
			staffingDetails.setPaidLeaveRelever(form.getCb_Relever_for_PaidLeaves().getValue());
			staffingDetails.setcBonusApplicable(form.cb_Cbonus.getValue());
			staffingDetails.setcLeaveApplicable(form.cb_CLeave.getValue());
			
			/**
			 * @author Anil
			 * @since 03-09-2020
			 * addPaidLeaveFlag
			 */
			if(form.addPaidLeaveFlag){
				staffingDetails.setBonusRelever(true);
				staffingDetails.setcBonusApplicable(true);
				
				staffingDetails.setPaidLeaveRelever(true);
				staffingDetails.setcLeaveApplicable(true);
			}
			
			staffingDetails.setGratuityApplicable(form.cb_Gratuity.getValue());
			/** date 15.8.2018 added by komal**/
			if(form.dbFlatTravelling.getValue()!=null){
				staffingDetails.setTravellingAllowance(form.dbFlatTravelling.getValue());
			}
			if(form.dbFlatOverheadCost.getValue()!=null){
				staffingDetails.setOverHeadCost(form.dbFlatOverheadCost.getValue()+"");
			}else{
				staffingDetails.setOverHeadCost(0+"");
			}
			if(form.dbFlatManagementFees.getValue()!=null){
				staffingDetails.setManagementFees(form.dbFlatManagementFees.getValue()+"");
			//	form.tbManagementFeesValue.setValue(form.dbFlatManagementFees.getValue()+"");
				/**
				 * @author Anil
				 * @since 03-09-2020
				 * for calculating management fees as per the staff
				 */
				staffingDetails.setManagementFeesRate(form.dbFlatManagementFees.getValue());
			}
			if(form.dbFlatBonus.getValue()!=null){
				staffingDetails.setSpBonus(form.dbFlatBonus.getValue()+"");
			}
			if(form.tbPaidLeavesDays.getValue()!=null){
				try{
					staffingDetails.setPaidLeaveDays(form.tbPaidLeavesDays.getValue());
				}catch(Exception e){
					staffingDetails.setPaidLeaveDays(0+"");
				}
			}
			/** date 25.1.2019 added by komal to select designation and ctctemplate first **/
			if(form.olbDesignation.getSelectedIndex() == 0){
				form.showDialogMessage("Please select designation.");
				return;
			}else{
				staffingDetails.setDesignation(form.olbDesignation.getValue(form.olbDesignation.getSelectedIndex()));
			}
			if(form.generalComposite.name.getValue() == null || form.generalComposite.name.getValue().equals("")){
				form.showDialogMessage("Please select CTC template.");
				return;
			}else{
				staffingDetails.setCtcTemplates(form.generalComposite.name.getValue());
			}
			
			/**
			 * @author Anil
			 * @since 07-07-2020
			 */
			
			CTCTemplate ctcTemp=(CTCTemplate) form.generalComposite.getModel();
			if(!form.tbSupervision.getValue().equals("")&&ctcTemp!=null){
				staffingDetails.setGrossSalary(Math.round((ctcTemp.getGrossEarning()/12)));
//				staffingDetails.setSupervision(Double.parseDouble(form.tbSupervision.getValue().trim()));
				staffingDetails.setSupervision((staffingDetails.getGrossSalary()*(Double.parseDouble(form.tbSupervision.getValue().trim())))/(100));
			}
			if(!form.tbTrainingCharges.getValue().equals("")&&ctcTemp!=null){
				staffingDetails.setGrossSalary(Math.round((ctcTemp.getGrossEarning()/12)));
//				staffingDetails.setTrainingCharges(Double.parseDouble(form.tbTrainingCharges.getValue().trim()));
				staffingDetails.setTrainingCharges((staffingDetails.getGrossSalary()*(Double.parseDouble(form.tbTrainingCharges.getValue().trim())))/(100));
			}
			
			/**
			 * @author Anil
			 * @since 20-08-2020
			 */
			if(form.oblPaidLeave.getSelectedIndex()!=0){
				form.staffingTable.setPaidleave(form.oblPaidLeave.getSelectedItem());
			}else{
				form.staffingTable.setPaidleave(null);
			}
			
			System.out.println("STAFFING ... ");
			form.staffingTable.getDataprovider().getList().add(staffingDetails);
			System.out.println("STAFFING ..... ");
			form.staffingTable.UpdateCTCTemplate(0 , staffingDetails);
			System.out.println("STAFFING ......... ");
			form.staffingTable.getTable().redraw();
			System.out.println("STAFFING ..............");
		
		}
		
		if(event.getSource()==form.bt_addEquipmentDetails){
			double machineryInterestValue=0,maintanancePercentage=0;
			try{
				machineryInterestValue=Double.parseDouble(form.getTbInterestOfMachinary().getValue().trim());
			}catch(Exception e){
				
			}
			try{
				maintanancePercentage=Double.parseDouble(form.getTbMaintaniancePercentage().getValue().trim());
			}catch(Exception e){
				
			}
			EquipmentRental equipmentRental=new EquipmentRental();
			equipmentRental.setMachineryInterest(machineryInterestValue);
			equipmentRental.setMaintanancePercentage(maintanancePercentage);
			form.equipmentRentalTable.getDataprovider().getList().add(equipmentRental);
		}
		
		if(event.getSource()==form.bt_consumablesDetails){
			Consumables consumables=new Consumables();
			form.consumableTable.siteLocationList=form.getSiteLocationList();
			form.consumableTable.setEnable(true);
			form.consumableTable.getDataprovider().getList().add(consumables);
		}
		
		if(event.getSource()==form.bt_otherServices){
			OtherServices otherServices=new OtherServices();
			form.otherServiceTable.getDataprovider().getList().add(otherServices);
		}
		
		if(event.getSource()==fromToDatePopup.getBtnOne()){
			form.showWaitSymbol();
			if(fromToDatePopup.getCbFlag().getValue() != null){
				model.setStaffingRentalDifferent(fromToDatePopup.getCbFlag().getValue());
			}else{
				model.setStaffingRentalDifferent(false);
			}
			cncBillService.generateBill(fromToDatePopup.getFromDate().getValue(), fromToDatePopup.getToDate().getValue(), model,0, 
					new AsyncCallback<String>() {
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					form.hideWaitSymbol();
				}

				@Override
				public void onSuccess(String result) {
					// TODO Auto-generated method stub
					form.hideWaitSymbol();
					panel.hide();
					form.showDialogMessage(result);
				}
			});
		}
		
		if(event.getSource()==fromToDatePopup.getBtnTwo()){
			panel.hide();
		}
		/** date 24.8.2018 added by komal**/
		if(event.getSource().equals(versionPopup.getLblCancel())){
			versionPopup.hidePopUp();
		}
		if(event.getSource().equals(versionPopup.getLblOk())){
			if(versionPopup.getOlbVersion().getSelectedIndex()!=0){
				String ver = versionPopup.getOlbVersion().getValue(versionPopup.getOlbVersion().getSelectedIndex());
				MyQuerry querry = new MyQuerry();
				Vector<Filter> filtervec=new Vector<Filter>();				
				Filter filter = new Filter();
				filter.setQuerryString("version");
				filter.setStringValue(ver);
				filtervec.add(filter);
				filter = new Filter();
				filter.setQuerryString("cnc.count");
				filter.setIntValue(model.getCount());
				filtervec.add(filter);
				querry.setFilters(filtervec);
				querry.setQuerryObject(new CNCVersion());
				service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
					
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						// TODO Auto-generated method stub
						Console.log("result :" + result.size());
						if(result.size()>0){
							CNCVersion cncVersion = (CNCVersion) result.get(0);
							CNC cnc = cncVersion.getCnc();
							form.updateView(cnc);
						}
						versionPopup.hidePopUp();
					}
					
					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						
					}
				});
			}
			
		}
		/** date 9.10.2018 added by komal for email**/
		if(event.getSource().equals(departmentPopup.getLblCancel())){
			departmentPopup.hidePopUp();
		}
		if(event.getSource().equals(departmentPopup.getLblOk())){
			List<Department> departmentList = departmentPopup.getDepartmentTable().getDataprovider().getList();
			departmentPopup.hidePopUp();
			reactOnMarkConfirmed(departmentList);
			
		}
		/** date 23.1.2019 added by komal to cancel CNC **/
		if(event.getSource().equals(popup.getBtnCancel())){
			cancelpanel.hide();
			popup.remark.setValue("");
		}
		if(event.getSource().equals(custAddresspopup.getLblCancel())){
			custAddresspopup.hidePopUp();
		}
		if(event.getSource().equals(custAddresspopup.getLblOk())){
//			custAddresspopup.hidePopUp();
			saveCustomerAddress();
		}
		if(event.getSource()==lblUpdate){
			custAddresspopup.getBillingAddressComposite().setEnable(true);
			custAddresspopup.getServiceAddressComposite().setEnable(true);
		}
		
		if(event.getSource().equals(popup.getBtnOk())){
			if (validateRemark()) {
				cncBillService.cancelCNC(model, popup.getRemark().getValue(),
						new AsyncCallback<String>() {

							@Override
							public void onSuccess(String result) {
								// TODO Auto-generated method stub
								form.showDialogMessage(result);
								cancelpanel.hide();
								model.setStatus(AppConstants.CANCELLED);
								form.tbStatus.setValue(AppConstants.CANCELLED);
//								model.setCancellationRemark(popup.getRemark().getValue());
								form.tbCancellationRemark.setValue(popup.getRemark().getValue());
								form.setToViewState();
							}

							@Override
							public void onFailure(Throwable caught) {
								// TODO Auto-generated method stub

							}
						});
			}
				else{
					form.showDialogMessage("Please enter remark to cancel CNC.");
				}
			
		}
		if(event.getSource() == cNCArrearsBillPopup.getLblOk()){
			
			
			ArrayList<CNCArrearsDetails> list = new ArrayList<CNCArrearsDetails>();
			list.addAll(cNCArrearsBillPopup.getcNCArrearsTable().getDataprovider().getList());
			if(cNCArrearsBillPopup.getDbBillfromDate().getValue() == null){
				form.showDialogMessage("Please enter bill from date.");
				return;
			}
			if(cNCArrearsBillPopup.getDbBillToDate().getValue() == null){
				form.showDialogMessage("Please enter bill to date.");
				return;
			}
			if(list == null || list.size() == 0){
				form.showDialogMessage("Arrears list should not be null");
				return;
			}
			cncBillService.generateArrearsBill(cNCArrearsBillPopup.getDbBillfromDate().getValue()
					, cNCArrearsBillPopup.getDbBillToDate().getValue(), model
					, list, new AsyncCallback<String>() {
				
				@Override
				public void onSuccess(String result) {
					// TODO Auto-generated method stub
					form.showDialogMessage(result);
					cNCArrearsBillPopup.hidePopUp();
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					Console.log(caught+"");
					cNCArrearsBillPopup.hidePopUp();
				}
			});
			
		}
		if(event.getSource() == cNCArrearsBillPopup.getLblCancel()){
			cNCArrearsBillPopup.hidePopUp();
		}
	}

	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.cnc.CNC")
	public static class CNCPresenterTable extends
			SuperTable<CNC> implements GeneratedVariableRefrence {

		@Override
		public Object getVarRef(String varName) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void createTable() {
			// TODO Auto-generated method stub
			
		}

		@Override
		protected void initializekeyprovider() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void addFieldUpdater() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void setEnable(boolean state) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void applyStyle() {
			// TODO Auto-generated method stub
			
		}
		
	};
	
	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.cnc.CNC")
	public static class CNCPresenterSearch extends SearchPopUpScreen<CNC> {

		@Override
		public MyQuerry getQuerry() {
			System.out.println("Super Get Querry Method");
			return null;
		}

		@Override
		public boolean validate() {
			// TODO Auto-generated method stub
			return true;
		}
	};
	
	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		// TODO Auto-generated method stub
		super.reactToProcessBarEvents(e);

		InlineLabel label = (InlineLabel) e.getSource();
		String text = label.getText().trim();
//		if (text.equals(ManageApprovals.APPROVALREQUEST)) {
//			form.getManageapproval().setBpId(model.getPersonInfo().getCount()+"");
//			form.getManageapproval().setBpName(model.getPersonInfo().getFullName());
//			form.getManageapproval().reactToRequestForApproval();
//		}
		
		
		if(text.equals(AppConstants.VIEWBILL)){

			final MyQuerry querry=new MyQuerry();
			Vector<Filter> temp=new Vector<Filter>();
			Filter filter=null;
			
			filter=new Filter();
			filter.setQuerryString("contractCount");
			filter.setIntValue(model.getCount());
			temp.add(filter);
			/** Date : 1/06/2021 by Priyanka. need filter on types on bill from cnc screen**/
			filter=new Filter();
			filter.setQuerryString("typeOfOrder");
			filter.setStringValue(AppConstants.ORDERTYPEFORSERVICE);
			temp.add(filter);
			
			/** date25.01.2019 added by komal to show only AP bills**/
			filter=new Filter();
			filter.setQuerryString("accountType");
			filter.setStringValue(AppConstants.ACCOUNTTYPE);
			temp.add(filter);
			
			querry.setFilters(temp);
			querry.setQuerryObject(new BillingDocument());
			form.showWaitSymbol();
			service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				@Override
				public void onFailure(Throwable caught) {
					form.hideWaitSymbol();
				}

				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					form.hideWaitSymbol();
					if(result.size()==0){
						form.showDialogMessage("No billing document found.");
						return;
					}
					if(result.size()==1){
						final BillingDocument billDocument=(BillingDocument) result.get(0);
						final BillingDetailsForm form=BillingDetailsPresenter.initalize();
						Timer timer=new Timer() {
							@Override
							public void run() {
								form.updateView(billDocument);
								form.setToViewState();
							}
						};
						timer.schedule(1000);
					}else{
						BillingListPresenter.initalize(querry);
					}
					
				}
			});
		}
		
		if(text.equals("View Project")){
			final MyQuerry querry=new MyQuerry();
			Vector<Filter> temp=new Vector<Filter>();
			Filter filter=null;
			
			filter=new Filter();
			filter.setQuerryString("contractId");
			filter.setIntValue(model.getCount());
			temp.add(filter);
			
			querry.setFilters(temp);
			querry.setQuerryObject(new ProjectAllocation());
			form.showWaitSymbol();
			service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				@Override
				public void onFailure(Throwable caught) {
					form.hideWaitSymbol();
				}

				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					form.hideWaitSymbol();
					if(result.size()==0){
						form.showDialogMessage("No Project found.");
						return;
					}
					if(result.size()!=0){
						final ProjectAllocation projectAllocation=(ProjectAllocation) result.get(0);
						final ProjectAllocationForm proform=ProjectAllocationPresenter.initalize();
						proform.loadCustomerBranch(form.getPersonInfoComposite().getIdValue(), null);
						Timer timer=new Timer() {
							@Override
							public void run() {
								if(form.getOblCustomerBranch().getSelectedIndex()!=0) {
									proform.getOblCustomerBranch().setValue(form.getOblCustomerBranch().getValue());
								}
								proform.updateView(projectAllocation);
								proform.setToViewState();
							}
						};
						timer.schedule(1000);
					}
				}
			});
		
		}
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CNC", "MakeProjectNameEnable")){
			projectNameEnable=true;
		}
		if(text.equals("Amend Contract")){
//			int nextVersionValue=model.getVersionMap().size();
//			form.lstbVersion.addItem((nextVersionValue+1)+"");
			
			if(model.getStatus().equals(AppConstants.APPROVED) || model.getStatus().equals(CNC.CONTRACTCONFIRMED)){
			
			NumberFormat nf = NumberFormat.getFormat("0.0");
			double ver = form.dbVersionNumber.getValue();
			double value = Math.floor(ver);
			String diff = nf.format(ver - value);
			System.out.println("version :" + diff);
			String myVer;
			if(nf.format(ver - value).equals(0.9+"")){
				NumberFormat nf1 = NumberFormat.getFormat("0.00");
				ver = ver + 0.01;
				myVer = nf1.format(ver);
			}else{
				ver = ver + 0.1;
				myVer = nf.format(ver);
			}
			model.setVersionNumber(Double.parseDouble(myVer));
			form.dbVersionNumber.setValue(Double.parseDouble(myVer));
			model.setIslatestVersion(true);
			form.isAmended=true;
			form.tbStatus.setValue(Contract.CREATED);
			if(projectNameEnable){
			form.tbProjectName.setEnabled(true);
			}else{
			form.tbProjectName.setEnabled(false);
			}
			model.setStatus(Contract.CREATED);
			model.setIslatestVersion(true);
			form.setToEditState();
		}else{
			form.showDialogMessage("Only approved and mark confirmed cnc can be amended.");
		}
		}
		if(text.equals("Raise Bill")){
			panel=new PopupPanel(true);
			fromToDatePopup = new FromAndToDateBoxPopup("Staffing and Rental Different");
			fromToDatePopup.getBtnOne().addClickHandler(this);
			fromToDatePopup.getBtnTwo().addClickHandler(this);
			fromToDatePopup.cbFlag.setValue(false);
			panel.add(fromToDatePopup);
			panel.center();
			panel.show();
		}
		
		if(text.equals("Mark Confirmed")){
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CNC", "ConfirmationMailToDepartment")){
				departmentPopup.showPopUp();
				
			}else{
				List<Department> departmentList = new ArrayList<Department>();
				reactOnMarkConfirmed(departmentList);
			}
			
//			model.setStatus(CNC.CONTRACTCONFIRMED);
//			CncBillServiceAsync cncBillService = GWT.create(CncBillService.class);
//		
//			cncBillService.generateProject(model, new AsyncCallback<String>() {
//
//				@Override
//				public void onFailure(Throwable caught) {
//					// TODO Auto-generated method stub
//					
//				}
//
//				@Override
//				public void onSuccess(String result) {
//					// TODO Auto-generated method stub
//					form.showDialogMessage(result);
//					/** date 24.8.2018 added by komal**/
//					if(form.dbVersionNumber.getValue()!=null){
//						form.dbVersionNumber.setValue(Math.ceil(form.dbVersionNumber.getValue()));
//					}
//					form.tbStatus.setValue(CNC.CONTRACTCONFIRMED);
//				}
//			});
		}
		/** date 08.08.2018 added by komal for cost sheet download**/
		if(text.equals(AppConstants.COSTSHHETDOWNLOAD)){
			xlxWriter.setCostSheetData(model, new AsyncCallback<Void>() {
				
				@Override
				public void onSuccess(Void result) {
					// TODO Auto-generated method stub
					String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url=gwt + "CreateXLXSServlet"+"?type="+3;
					Window.open(url, "test", "enabled");
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					
				}
			});
			
		}
		
		/*************Date 31-1-2019  added by Amol for Payroll Sheet Download************************/
		if(text.equals(AppConstants.PAYROLLSHEETDOWNLOAD)){
			xlxWriter.setCostSheetData(model, new AsyncCallback<Void>() {
				
				@Override
				public void onSuccess(Void result) {
					// TODO Auto-generated method stub
					String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url=gwt + "CreateXLXSServlet"+"?type="+7;
					Window.open(url, "test", "enabled");
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					
				}
			});
			
		}
		
		
	
		if(text.equals(AppConstants.NEW)){
			this.reactToNew();
		}
		/**
		 * end komal
		 */
		/** date 24.8.2018 added by komal**/
		if(text.equals(AppConstants.SEARCHVERSION)){
		//	versionPopup = new VersionPopup();
			  MyQuerry querry=new MyQuerry();
			  Filter filter = new Filter();
			  filter.setQuerryString("cnc.count");
			  filter.setIntValue(model.getCount());
			  querry.getFilters().add(filter);
			  querry.setQuerryObject(new CNCVersion());
			  versionPopup.getOlbVersion().MakeLiveUniqueItems(querry);
			  versionPopup.showPopUp();
		}
		/** date 23.1.2018 added by komal to add cancellation of CNC**/
		if(text.equalsIgnoreCase(AppConstants.CANCLECONTRACT)){
			 cncBillService.validateProjectAttendanceAndBilling(model, new AsyncCallback<String>() {
             
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void onSuccess(String result) {
					// TODO Auto-generated method stub
					if(result.equalsIgnoreCase("success")){
						popup.getPaymentDate().setValue(new Date() + "");
						popup.getPaymentID().setValue(
								form.getTbContractId().getValue().trim());
						popup.getPaymentStatus().setValue(
								form.getstatustextbox().getValue().trim());
						cancelpanel = new PopupPanel(true);
						cancelpanel.add(popup);
						cancelpanel.show();
						cancelpanel.center();
					}else{
						form.showDialogMessage(result);
					}
				}
			});
		}
		
		
		if(text.equals(AppConstants.GROSSPROFITSHEETDOWNLOAD)){
			xlxWriter.setCostSheetData(model, new AsyncCallback<Void>() {
				
				@Override
				public void onSuccess(Void result) {
					// TODO Auto-generated method stub
					String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url=gwt + "CreateXLXSServlet"+"?type="+8;
					Window.open(url, "test", "enabled");
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					
				}
			});
			
		}
		if(text.equalsIgnoreCase(AppConstants.ARREARSBILL)){
			  MyQuerry querry=new MyQuerry();
			  Filter filter = new Filter();
			  filter.setQuerryString("cnc.count");
			  filter.setIntValue(model.getCount());
			  querry.getFilters().add(filter);
			  querry.setQuerryObject(new CNCVersion());
			  cNCArrearsBillPopup.getObjVersion().MakeLiveUniqueItems(querry);
			  cNCArrearsBillPopup.showPopUp();
		}
		if(text.equals(AppConstants.CUSTOMERADDRESS)){
			if(model.getBillingAddress()!=null && model.getBillingAddress().getAddrLine1()!=null && !model.getBillingAddress().getAddrLine1().equals("")
					&& model.getServiceAddress()!=null &&model.getServiceAddress().getAddrLine1()!=null && !model.getServiceAddress().getAddrLine1().equals("")){
				lblUpdate.getElement().setId("addbutton");
				custAddresspopup.getHorizontal().add(lblUpdate);
				
				custAddresspopup.showPopUp();
				custAddresspopup.getBillingAddressComposite().setValue(model.getBillingAddress());
				custAddresspopup.getServiceAddressComposite().setValue(model.getServiceAddress());
				
				custAddresspopup.getBillingAddressComposite().setEnable(false);
				custAddresspopup.getServiceAddressComposite().setEnable(false);
			}else{
				getCustomerAddress1();
			}
			
			
		}
		// Added By Priyanka.
		/** Des : add salesorder on cnc req raised by Nitin Sir **/
		if(text.equals(AppConstants.CREATESALESORDER)){
			reactOnSalesOrder();
		}
	}

	private void getCustomerAddress1() {
		final MyQuerry querry=new MyQuerry();
		Vector<Filter> temp=new Vector<Filter>();
		Filter filter=null;
		
		filter=new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(model.getPersonInfo().getCount());
		temp.add(filter);
		
		querry.setFilters(temp);
		querry.setQuerryObject(new Customer());
		form.showWaitSymbol();
		genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				form.hideWaitSymbol();

				for(SuperModel model:result){
					
					Customer customer = (Customer)model;
					System.out.println("hi vijay"+customer.getAdress().getCompleteAddress());
					lblUpdate.getElement().setId("addbutton");
					custAddresspopup.getHorizontal().add(lblUpdate);
//					custAddresspopup.getLblOk().setText("Save");
					custAddresspopup.showPopUp();
					
					custAddresspopup.getBillingAddressComposite().setValue(customer.getAdress());
					custAddresspopup.getServiceAddressComposite().setValue(customer.getSecondaryAdress());

					custAddresspopup.getBillingAddressComposite().setEnable(false);
					custAddresspopup.getServiceAddressComposite().setEnable(false);
					
				}
				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				form.showDialogMessage("Unexpected Error Ouccured");
				form.hideWaitSymbol();

			}
		});
	}

	private void reactOnSalesOrder() {
		try{
			
			final SalesOrderForm form=SalesOrderPresenter.initalize();
			form.showWaitSymbol();
			System.out.println("Starting timer");
			Timer t = new Timer() {
				@Override
				public void run() {
					Console.log("Inside run");
					form.setToNewState();
					form.hideWaitSymbol();
//					PersonInfo personInfo = new PersonInfo();
//					personInfo.setCount(model.getCount());
					if (model.getStatus().equals(AppConstants.ACTIVE)) {
						form.getPersonInfoComposite().getId().getElement().addClassName("personactive");
						form.getPersonInfoComposite().getName().getElement().addClassName("personactive");
						form.getPersonInfoComposite().getPhone().getElement().addClassName("personactive");
						form.getPersonInfoComposite().getTbpocName().getElement().addClassName("personactive");
					}
					form.getPersonInfoComposite().setValue(model.getPersonInfo());
					form.getOlbbBranch().setValue(model.getBranch());
					Console.log("Inside run 1");
					
					try{
						form.getOlbeSalesPerson().setValue(model.getSalesPerson());
						form.getTbQuotatinId().setText("");
						form.getTbContractId().setText("");
						form.getPersonInfoComposite().setEnabled(false);
					}catch(Exception e){
						
					}
					
					/**
					 * @author Anil @since 09-11-2021
					 * Copying client shipping address to sale order shipping section
					 */
					Console.log("Create Sales Order ...!!");
					form.loadCustomerBranch(null, model.getPersonInfo().getCount());
					form.customerBillingAddress=model.getBillingAddress();
					form.customerServiceAddress=model.getServiceAddress();
					form.getAcshippingcomposite().setValue(model.getServiceAddress());
					form.getTbReferenceNumber().setValue(model.getCount()+"");
					form.getTbReferenceNumber().setEnabled(false);
					form.cncEntity=model;
					Console.log("Create Sales Order Clicked...!!");
					
				}
			};
			t.schedule(3000);

		}catch(Exception e) {
				Console.log("Exception..." + e.getMessage());
			}
		
		
	}

	public void getCustomerAddress() {
		final MyQuerry querry=new MyQuerry();
		Vector<Filter> temp=new Vector<Filter>();
		Filter filter=null;
		
		filter=new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(form.getPersonInfoComposite().getIdValue());
		temp.add(filter);
		
		querry.setFilters(temp);
		querry.setQuerryObject(new Customer());
		form.showWaitSymbol();
		genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				form.hideWaitSymbol();

				for(SuperModel model:result){
					
					Customer customer = (Customer)model;
					
					form.billingAddress=customer.getAdress();
					form.serviceAddress=customer.getSecondaryAdress();
					form.olbbBranch.setValue(customer.getBranch());
					form.olbeSalesPerson.setValue(customer.getEmployee());
					form.olbApproverName.setValue(customer.getApproverName());
				}
				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				form.showDialogMessage("Unexpected Error Ouccured");
				form.hideWaitSymbol();

			}
		});
	}

	@Override
	public void onRowCountChange(RowCountChangeEvent event) {
		// TODO Auto-generated method stub
		try{
		Console.log("Inside row index chnaged");
		if(event.getSource().equals(form.getStaffingTable().getTable())){
			double netPayableForStaffing=0;
			double managementValue=0 , val =0 ;
			for (int i = 0; i < form.getStaffingTable().getDataprovider().getList().size(); i++) {
//				netPayableForStaffing+=Double.parseDouble(form.getStaffingTable().getDataprovider().getList().get(i).getGrandTotalOfCTC());
				netPayableForStaffing+=form.getStaffingTable().getDataprovider().getList().get(i).getTotalPaidIntoEmployee();
				val = 0;
				try{
					val = Double.parseDouble(form.getStaffingTable().getDataprovider().getList().get(i).getManagementFees());
				}catch(Exception e){
					val = 0;
				}
				managementValue += val;
			}
			form.tbGrossTotalOfStaffing.setValue((Math.round(netPayableForStaffing*100.0)/100.0)+"");
//			try{
//				managementValue=((Math.round(netPayableForStaffing*100.0)/100.0)*Double.parseDouble(form.tbManagementFeesPercentage.getValue().trim()))/100;
//			}catch(Exception e){
//				e.printStackTrace();
//				managementValue=0;
//			}
//			if(managementValue==0){
//				if(form.dbFlatManagementFees.getValue()!=null){
//					managementValue = form.dbFlatManagementFees.getValue();
//				}
//			}
//			form.tbManagementFeesValue.setValue((Math.round(managementValue*100.0)/100.0)+"");
			form.tbManagementFeesValue.setValue(managementValue+"");
			form.tbNetPayableOFStaffing.setValue((Math.round((netPayableForStaffing+managementValue)*100.0)/100.0)+"");
			
			/**
			 * @author Anil
			 * @since 23-11-2020
			 * Filtering desigation dropdown list for OT details
			 * designation added in staffing details table should appear 
			 * raised by Rahul Tiwari for Sun facility
			 */
			if(form.siteLocation){
				form.updateDesignationDropDownList();
			}
		}
		}catch(Exception e){
			e.printStackTrace();
		}
		try{
		if(event.getSource().equals(form.getEquipmentRentalTable().getTable())){
			double netPayableForEquipment=0;
			for (int i = 0; i < form.getEquipmentRentalTable().getDataprovider().getList().size(); i++) {
				netPayableForEquipment+=Double.parseDouble(form.getEquipmentRentalTable().getDataprovider().getList().get(i).getTotal());
			}
			//komal
			double discount = 0;
			if(form.dbDiscount.getValue()!=null){
				discount = form.dbDiscount.getValue();
			}
			form.dbTotalWithoutDiscount.setValue(Math.round(netPayableForEquipment*100.0)/100.0);
			form.tbNetPayableOFEquipment.setValue((Math.round((netPayableForEquipment - discount)*100.0)/100.0)+"");
		}
		}catch(Exception e){
			e.printStackTrace();
		}
		try{
		if(event.getSource().equals(form.getConsumableTable().getTable())){
			double netPayableForEquipment=0;
			for (int i = 0; i < form.getConsumableTable().getDataprovider().getList().size(); i++) {
				/**
				 * @author Anil @since 12-11-2021
				 */
				if(form.getConsumableTable().getDataprovider().getList().get(i).getChargable()!=null
						&&!form.getConsumableTable().getDataprovider().getList().get(i).getChargable().equals("")){
					netPayableForEquipment+=Double.parseDouble(form.getConsumableTable().getDataprovider().getList().get(i).getChargable());
				}
			}
			form.tbNetPayableOFConsumabables.setValue((Math.round(netPayableForEquipment*100.0)/100.0)+"");
		}
		}catch(Exception e){
			e.printStackTrace();
		}
		try{
		if(event.getSource().equals(form.getOtherServiceTable().getTable())){
			double netPayableForEquipment=0;
			for (int i = 0; i < form.getOtherServiceTable().getDataprovider().getList().size(); i++) {
				netPayableForEquipment+=form.getOtherServiceTable().getDataprovider().getList().get(i).getChargableRates();
			}
			form.tbNetPayableOFOther.setValue((Math.round(netPayableForEquipment*100.0)/100.0)+"");
		}
		}catch(Exception e){
			e.printStackTrace();
		}
		double total1 = 0,total2 = 0,total3 = 0,total4 = 0;
		try{
			total1=Double.parseDouble(form.tbNetPayableOFStaffing.getValue().trim());
		}catch(Exception e){
			e.printStackTrace();
		}
		try{
			total2=Double.parseDouble(form.tbNetPayableOFEquipment.getValue().trim());
		}catch(Exception e){
			e.printStackTrace();
		}
		try{
			total3=Double.parseDouble(form.tbNetPayableOFConsumabables.getValue().trim());
		}catch(Exception e){
			e.printStackTrace();
		}
		try{
			total4=Double.parseDouble(form.tbNetPayableOFOther.getValue().trim());
		}catch(Exception e){
			e.printStackTrace();
		}
		
		double netPayOFCNC=Math.round(total1+total2+total3+total4);
		form.donetpayamt.setValue(netPayOFCNC);
		
	}
	private void reactToNew() {
		form.setToNewState();
		initalize();
	}
	private void reactOnMarkConfirmed(final List<Department> departmentList){
		model.setStatus(CNC.CONTRACTCONFIRMED);
		CncBillServiceAsync cncBillService = GWT.create(CncBillService.class);
		
		cncBillService.generateProject(model, new AsyncCallback<String>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
			@Override
			public void onSuccess(String result) {
				// TODO Auto-generated method stub	
				form.showDialogMessage(result);
				/** date 24.8.2018 added by komal**/
				if(form.dbVersionNumber.getValue()!=null){
					form.dbVersionNumber.setValue(Math.ceil(form.dbVersionNumber.getValue()));
				}
				form.tbStatus.setValue(CNC.CONTRACTCONFIRMED);
				if(departmentList.size()>0){
					sendConfirmationEmail(departmentList);
				}
				/**
				 * @author Vijay Chougule
				 * Date :- 14-08-2020
				 * Des :-Updating global list when new CNC project will create
				 */
				if(result.equals("Project Successfully Created!")){
					String processHRProject=AppConstants.GLOBALHRPROJECT+"-"+model.getCompanyId();
					LoginPresenter.globalMakeLiveHRProject(processHRProject);
				}
			}
		});
	
	}
	private void sendConfirmationEmail(List<Department> departmentList){
		Company com = new Company();
		final ArrayList<String> toEmailList = new ArrayList<String>();
		for(Department dept : departmentList){
		toEmailList.add(dept.getEmail());
		}
		final String mailSub="CNC Id - " + model.getCount() + " / " + model.getProjectName() + " Confirmation.";
		final String mailMsg="Dear Team"+","+"\n"
				+"    The CNC : "
				+ model.getCount() +" has been mark confirmed and "+model.getProjectName()+" is created successfully."+"\n"
				+"Kindly start planning your project on the immediate basis." +"\n"+"\n "+"\n "+"Thanks & Regards, "+"\n "+ model.getSalesPerson() ;
						/*+"Service : "+service.getCount() +" has been reschedule by "+techName +
						"\n Remark : - " + service.getReasonForChange() +" \n"+"\n"+"\n";*/
		/*builder.append("</body>");
		builder.append("</html>");*/
	//	Console.log("EMAIL Sending --"+comp.getEmail());
		MyQuerry query = new MyQuerry();
		Filter filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(com.getCompanyId());
		query.getFilters().add(filter);
		query.setQuerryObject(new Company());
		service.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				Company comp = (Company) result.get(0);
				String fromEmail = "";
				if(comp!=null){
					if(comp.getEmail()!=null&&!comp.getEmail().equals("")){
						fromEmail=comp.getEmail();
					}	
				}
				email.sendEmail(mailSub, mailMsg, toEmailList, new ArrayList<String>(), fromEmail, new AsyncCallback<Void>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void onSuccess(Void result) {
						// TODO Auto-generated method stub
						form.showDialogMessage("Email sent successfully.");
					}
				});

			}
			
		});
				
	
			

}
	
	public void reactOnDownload() {
		ArrayList<CNC> cncArray = new ArrayList<CNC>();
		List<CNC> list = (List<CNC>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
		cncArray.addAll(list);
		csvservice.setCNClist(cncArray, new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed" + caught);
			}

			@Override
			public void onSuccess(Void result) {
				String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url = gwt + "csvservlet" + "?type=" + 155;
				Window.open(url, "test", "enabled");
			}
		});
	}
	
			private boolean validateRemark(){
				
				if(popup.getRemark().getValue().trim()!= null && !popup.getRemark().getValue().trim().equals("")){
					
					return true;
				}
				return false;
			}

			/**
			 * Updated By: Viraj
			 * Date: 23-02-2019
			 * Description: To validate end date(it should be always greater than start date)
			 */
			@Override
			public void onValueChange(ValueChangeEvent<Date> event) {
				System.out.println();
				if(event.getSource().equals(form.dbContractEndDate)) {
					System.out.println("Inside value change contract end date");
					if(form.dbContractEndDate.getValue() != null) {
						System.out.println("Inside contract end date");
						if(form.dbContractEndDate.getValue().before(form.dbContractStartDate.getValue())) {
							form.showDialogMessage("Please Select end date greater than start date");
							form.dbContractEndDate.setValue(null);
						}
					}
				}
				
				if(event.getSource().equals(form.dbContractStartDate)) {
					System.out.println("Inside value change contract start date");
					if(form.dbContractStartDate.getValue() != null && form.dbContractEndDate.getValue() != null) {
						System.out.println("Inside contract start date");
						if(form.dbContractStartDate.getValue().after(form.dbContractEndDate.getValue())) {
							form.showDialogMessage("Please Select start date less than end date");
							form.dbContractStartDate.setValue(null);
						}
					}
				}
				
			}
			/** Ends **/

			@Override
			public void onChange(ChangeEvent event) {
				if (event.getSource().equals(form.oblCustomerBranch)) {
//					setCustomerBranchAddressToPopup(form.oblCustomerBranch.getValue());
					if(form.oblCustomerBranch.getSelectedIndex()!=0){
						CustomerBranchDetails custBranch=form.oblCustomerBranch.getSelectedItem();
						if(custBranch.getBillingAddress()!=null){
							form.billingAddress=custBranch.getBillingAddress();
						}
						if(custBranch.getAddress()!=null){
							form.serviceAddress=custBranch.getAddress();
						}
					}else{
						getCustomerAddress();
					}
				}
			}
			
			private void saveCustomerAddress() {
				model.setBillingAddress(custAddresspopup.getBillingAddressComposite().getValue());
				model.setServiceAddress(custAddresspopup.getServiceAddressComposite().getValue());
				genasync.save(model, new AsyncCallback<ReturnFromServer>() {
					
					@Override
					public void onSuccess(ReturnFromServer result) {
						// TODO Auto-generated method stub
						form.showDialogMessage("Customer Address Updated Succesfully");
						custAddresspopup.hidePopUp();
						form.setToViewState();
					}
					
					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						form.showDialogMessage("Unexpected Error");
						custAddresspopup.hidePopUp();
					}
				});
			}

	}
