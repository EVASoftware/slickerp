package com.slicktechnologies.client.views.cnc;

import java.util.ArrayList;
import java.util.List;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.shared.CNCArrearsDetails;
import com.slicktechnologies.shared.common.cnc.CNC;
import com.slicktechnologies.shared.common.cnc.CNCVersion;

public class CNCArrearsBillPopup extends PopupScreen implements ClickHandler {

	DateBox dbBillfromDate,dbBillToDate;	
	DateBox dbfromDate,dbToDate;
	CNCArrearsTable cNCArrearsTable;
	ObjectListBox<CNCVersion> objVersion;
	Button btAdd;
	DateTimeFormat fmt = DateTimeFormat.getFormat("dd/MM/yyyy");
	
	public CNCArrearsBillPopup() {
		super();
		getPopup().setWidth("300px");
		cNCArrearsTable.getTable().setHeight("300px");
		cNCArrearsTable.getTable().setWidth("600px");
		createGui();
		
		
	}
	
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		GWTCAlert alert = new GWTCAlert();
		if(event.getSource() == btAdd){
			List<CNCArrearsDetails> arrearsList = new ArrayList<CNCArrearsDetails>();
			arrearsList.addAll(cNCArrearsTable.getDataprovider().getList());
			CNCArrearsDetails details = new CNCArrearsDetails();
			if(dbfromDate.getValue() != null){
				details.setFromDate(fmt.parse(fmt.format(dbfromDate.getValue())));
			}else{
				alert.alert("Please select from date.");
				return;
			}
			if(dbToDate.getValue() != null){
				details.setToDate(fmt.parse(fmt.format(dbToDate.getValue())));
			}else{
				alert.alert("Please select to date.");
				return;
			}
			if(objVersion.getSelectedIndex() != 0){
				try{
					details.setVersion(objVersion.getValue(objVersion.getSelectedIndex()));
				}catch(Exception e){
					
				}
			}else{
				alert.alert("Please select CNC version.");
				return;
			}
			arrearsList.add(details);
			cNCArrearsTable.getDataprovider().setList(arrearsList);
		}
	}

	@Override
	public void createScreen() {
		initializeWidgets();
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fsearchFilters = fbuilder.setlabel("Arrears Bill").widgetType(FieldType.Grouping).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Bill From Date",  dbBillfromDate);
		FormField fdbBillfromDate = fbuilder.setMandatory(false).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Bill To Date",  dbBillToDate);
		FormField fdbBillToDate = fbuilder.setMandatory(false).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("From Date",  dbfromDate);
		FormField fdbfromDate = fbuilder.setMandatory(false).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("To Date",  dbToDate);
		FormField fdbToDate = fbuilder.setMandatory(false).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Version",  objVersion);
		FormField fobjVersion = fbuilder.setMandatory(false).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",  btAdd);
		FormField fbtAdd = fbuilder.setMandatory(false).setColSpan(0).build();
				
	
		fbuilder = new FormFieldBuilder("",  cNCArrearsTable.getTable());
		FormField fcNCArrearsTable = fbuilder.setMandatory(false).setColSpan(4).build();
		
		FormField [][]formfield = {
				{fsearchFilters},
				{fdbBillfromDate , fdbBillToDate },
				{fdbfromDate,fdbToDate , fobjVersion ,fbtAdd},
				{fcNCArrearsTable }
		};
		this.fields=formfield;
		
	}
	private void initializeWidgets() {
		dbfromDate = new DateBoxWithYearSelector();
		dbToDate = new DateBoxWithYearSelector();
		dbBillfromDate = new DateBoxWithYearSelector();
		dbBillToDate = new DateBoxWithYearSelector();
		objVersion = new ObjectListBox<CNCVersion> ();
		objVersion.addItem("--SELECT--");
		btAdd = new Button("ADD");
		btAdd.addClickHandler(this);
		cNCArrearsTable = new CNCArrearsTable();
		
	}

	public DateBox getDbBillfromDate() {
		return dbBillfromDate;
	}

	public void setDbBillfromDate(DateBox dbBillfromDate) {
		this.dbBillfromDate = dbBillfromDate;
	}

	public DateBox getDbBillToDate() {
		return dbBillToDate;
	}

	public void setDbBillToDate(DateBox dbBillToDate) {
		this.dbBillToDate = dbBillToDate;
	}

	public DateBox getDbfromDate() {
		return dbfromDate;
	}

	public void setDbfromDate(DateBox dbfromDate) {
		this.dbfromDate = dbfromDate;
	}

	public DateBox getDbToDate() {
		return dbToDate;
	}

	public void setDbToDate(DateBox dbToDate) {
		this.dbToDate = dbToDate;
	}

	public CNCArrearsTable getcNCArrearsTable() {
		return cNCArrearsTable;
	}

	public void setcNCArrearsTable(CNCArrearsTable cNCArrearsTable) {
		this.cNCArrearsTable = cNCArrearsTable;
	}

	public ObjectListBox<CNCVersion> getObjVersion() {
		return objVersion;
	}

	public void setObjVersion(ObjectListBox<CNCVersion> objVersion) {
		this.objVersion = objVersion;
	}

	public Button getBtAdd() {
		return btAdd;
	}

	public void setBtAdd(Button btAdd) {
		this.btAdd = btAdd;
	}

}
