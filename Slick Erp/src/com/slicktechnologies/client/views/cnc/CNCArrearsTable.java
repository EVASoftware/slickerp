package com.slicktechnologies.client.views.cnc;

import java.util.Date;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.CNCArrearsDetails;
import com.slicktechnologies.shared.EmployeeAssetBean;
import com.slicktechnologies.shared.common.arrears.ArrearsInfoBean;

public class CNCArrearsTable extends SuperTable<CNCArrearsDetails> {

	Column<CNCArrearsDetails,String> deleteColumn;
	TextColumn<CNCArrearsDetails> viewFromDateCol;
	TextColumn<CNCArrearsDetails> viewDateToCol;
	TextColumn<CNCArrearsDetails> versionColumn;

	@Override
	public void createTable() {
	
		createViewFromDateColumn();
		createViewToDateColumn();
		createViewVersionColumn();
		createColumndeleteColumn();
		addFieldUpdater();
	}


	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		createFieldUpdaterdeleteColumn();
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		 for(int i=table.getColumnCount()-1;i>-1;i--)
	    	  table.removeColumn(i); 
		 
		 
		}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
	

	protected void createColumndeleteColumn()
	{
		ButtonCell btnCell= new ButtonCell();
		deleteColumn=new Column<CNCArrearsDetails,String>(btnCell)
				{
			@Override
			public String getValue(CNCArrearsDetails object)
			{
				return  "Delete" ;
			}
				};
				table.addColumn(deleteColumn,"Delete");
			//	table.setColumnWidth(deleteColumn, 100,Unit.PX);
	}
		protected void createFieldUpdaterdeleteColumn()
	{
		deleteColumn.setFieldUpdater(new FieldUpdater<CNCArrearsDetails,String>()
				{
			@Override
			public void update(int index,CNCArrearsDetails object,String value)
			{
				getDataprovider().getList().remove(object);
				table.redrawRow(index);
			}
		});
	}
	
	protected void createViewFromDateColumn() {
		final DateTimeFormat fmt = DateTimeFormat.getFormat("dd/MM/yyyy");
		viewFromDateCol = new TextColumn<CNCArrearsDetails>() {
			@Override
			public String getValue(CNCArrearsDetails object) {
				if (object.getFromDate() != null) {
					return fmt.format(object.getFromDate());
				} else {
					return "N.A.";
				}
			}
		};
		table.addColumn(viewFromDateCol, "From Date");
	//	table.setColumnWidth(viewFromDateCol, 100, Unit.PX);
	}
	
	protected void createViewToDateColumn() {
		final DateTimeFormat fmt = DateTimeFormat.getFormat("dd/MM/yyyy");
		viewDateToCol = new TextColumn<CNCArrearsDetails>() {
			@Override
			public String getValue(CNCArrearsDetails object) {
				if (object.getToDate() != null) {
					return fmt.format(object.getToDate());
				} else {
					return "N.A.";
				}
			}
		};
		table.addColumn(viewDateToCol, "To Date");
		//table.setColumnWidth(viewDateToCol, 100, Unit.PX);
	}
	
	
	protected void createViewVersionColumn()
	{
		
		versionColumn=new TextColumn<CNCArrearsDetails>()
				{
			@Override
			public String getValue(CNCArrearsDetails object)
			{				
					return object.getVesion();
				}
				};
				table.addColumn(versionColumn,"Version");
			//	table.setColumnWidth(prodPriceColumn, 100,Unit.PX);
	}
	
}
