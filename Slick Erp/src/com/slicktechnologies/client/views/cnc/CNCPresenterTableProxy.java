package com.slicktechnologies.client.views.cnc;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.cnc.CNCPresenter.CNCPresenterTable;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.cnc.CNC;

public class CNCPresenterTableProxy extends CNCPresenterTable {

	TextColumn<CNC> customerCellNumberColumn;
	TextColumn<CNC> customerFullNameColumn;
	TextColumn<CNC> customerIdColumn;
	TextColumn<CNC> contractCountColumn;
	TextColumn<CNC> contractStartDateColumn;
	TextColumn<CNC> contractEndDateColumn;
	/** date 4.10.2018 added by komal for project name **/
	TextColumn<CNC> contractProjectNameColumn;
	TextColumn<CNC> StatusColumn;
	@Override
	public void createTable() {
		// TODO Auto-generated method stub
		addcontractCountColumn();	
		addcustomerFullNameColumn();
		/** date 4.10.2018 added by komal for project name **/
		addProjectNameColumn();
		addcustomerIdColumn(); 
		addcustomerCellNumberColumn();     	
		addcontractStartDateColumn();  	
		addcontractEndDateColumn(); 
		addStatusColumn();
	}

	private void addcontractEndDateColumn() {
		// TODO Auto-generated method stub
		contractEndDateColumn = new TextColumn<CNC>() {

			@Override
			public String getValue(CNC object) {
				// TODO Auto-generated method stub
				return AppUtility.parseDate(object.getContractEndDate())+"";
			}

		};

		table.addColumn(contractEndDateColumn, "Contract End Date");
		table.setColumnWidth(contractEndDateColumn, 130, Unit.PX);
		contractEndDateColumn.setSortable(true);}

	private void addcontractStartDateColumn() {
		// TODO Auto-generated method stub
		contractStartDateColumn = new TextColumn<CNC>() {

			@Override
			public String getValue(CNC object) {
				// TODO Auto-generated method stub
				return AppUtility.parseDate(object.getContractStartDate())+"";
			}

		};

		table.addColumn(contractStartDateColumn, "Contract Start Date");
		table.setColumnWidth(contractStartDateColumn, 130, Unit.PX);
		contractStartDateColumn.setSortable(true);
	}

	private void addcontractCountColumn() {
		// TODO Auto-generated method stub
		contractCountColumn = new TextColumn<CNC>() {

			@Override
			public String getValue(CNC object) {
				// TODO Auto-generated method stub
				return object.getCount()+"";
			}

		};

		table.addColumn(contractCountColumn, "Contract Id");
		table.setColumnWidth(contractCountColumn, 130, Unit.PX);
		contractCountColumn.setSortable(true);}

	private void addcustomerIdColumn() {
		// TODO Auto-generated method stub
		customerIdColumn = new TextColumn<CNC>() {

			@Override
			public String getValue(CNC object) {
				// TODO Auto-generated method stub
				return object.getPersonInfo().getCount()+"";
			}

		};

		table.addColumn(customerIdColumn, "Customer Id");
		table.setColumnWidth(customerIdColumn, 130, Unit.PX);
		customerIdColumn.setSortable(true);}

	private void addcustomerFullNameColumn() {
		// TODO Auto-generated method stub
		customerFullNameColumn = new TextColumn<CNC>() {

			@Override
			public String getValue(CNC object) {
				// TODO Auto-generated method stub
				return object.getPersonInfo().getFullName();
			}

		};

		table.addColumn(customerFullNameColumn, "Customer Name");
		table.setColumnWidth(customerFullNameColumn, 130, Unit.PX);
		customerFullNameColumn.setSortable(true);}

	private void addcustomerCellNumberColumn() {
		// TODO Auto-generated method stub
		customerCellNumberColumn = new TextColumn<CNC>() {

			@Override
			public String getValue(CNC object) {
				// TODO Auto-generated method stub
				return object.getPersonInfo().getCellNumber()+"";
			}

		};

		table.addColumn(customerCellNumberColumn, "Customer Cell");
		table.setColumnWidth(customerCellNumberColumn, 130, Unit.PX);
		customerCellNumberColumn.setSortable(true);}
           
	/** date 4.10.2018 added by komal for project name **/
	private void addProjectNameColumn() {
		contractProjectNameColumn = new TextColumn<CNC>() {
			@Override
			public String getValue(CNC object) {
				// TODO Auto-generated method stub
				return object.getProjectName();
			}

		};
		table.addColumn(contractProjectNameColumn, "Project Name");
		table.setColumnWidth(contractProjectNameColumn, 130, Unit.PX);
		contractProjectNameColumn.setSortable(true);
	}
	
	
	
	private void addStatusColumn(){
		StatusColumn=new TextColumn<CNC>() {
			
			public String getValue(CNC object) {
				
				return object.getStatus();
				
			}
		};
		table.addColumn(StatusColumn, "Status");
		table.setColumnWidth(StatusColumn, 130, Unit.PX);
		StatusColumn.setSortable(true);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public void addColumnSorting() {
		// TODO Auto-generated method stub
		super.addColumnSorting();
		 addSortingcustomerCellNumberColumn();
		  addSortingcustomerFullNameColumn();
		  addSortingcustomerIdColumn();
		  addSortingcontractCountColumn();
		  addSortingcontractProjectNameColumn();
		  addSortingcontractStartDateColumn();
		  addSortingcontractEndDateColumn();
		  addSortingaddStatusColumn();
		}
	
	
	    public void addSortingcustomerCellNumberColumn() {
		List<CNC> list=getDataprovider().getList();
		  columnSort=new ListHandler<CNC>(list);
		  columnSort.setComparator(customerCellNumberColumn, new Comparator<CNC>()
		  {
		  @Override
		  public int compare(CNC e1,CNC e2)
		  {
			  if(e1!=null && e2!=null)
		  {
		  if(e1.getPersonInfo().getCellNumber()== e2.getPersonInfo().getCellNumber()){
		  return 0;}
		  if(e1.getPersonInfo().getCellNumber()> e2.getPersonInfo().getCellNumber()){
		  return 1;}
		  else{
		  return -1;}
		  }
		  else{
		  return 0;}
		  }
		  });
		  table.addColumnSortHandler(columnSort);
		  
		
		
		
		
		
		
		
		
		
	}
   public void addSortingcustomerFullNameColumn() {
	   
	   List<CNC> list=getDataprovider().getList();
   
   columnSort=new ListHandler<CNC>(list);
   columnSort.setComparator(customerFullNameColumn, new Comparator<CNC>()
   {
   @Override
   public int compare(CNC e1,CNC e2)
   {
   if(e1!=null && e2!=null)
   {
   if( e1.getPersonInfo().getFullName()!=null && e2.getPersonInfo().getFullName()!=null){
   return e1.getPersonInfo().getFullName().compareTo(e2.getPersonInfo().getFullName());}
   }
   else{
   return 0;}
   return 0;
   }
   });
   table.addColumnSortHandler(columnSort);
    }
	
	public void addSortingcustomerIdColumn() {
		List<CNC> list=getDataprovider().getList();
	  columnSort=new ListHandler<CNC>(list);
	  columnSort.setComparator(customerIdColumn, new Comparator<CNC>()
	  {
	  @Override
	  public int compare(CNC e1,CNC e2)
	  {
	  if(e1!=null && e2!=null)
	  {
	  if(e1.getCount()== e2.getCount()){
	  return 0;}
	  if(e1.getCount()> e2.getCount()){
	  return 1;}
	  else{
	  return -1;}
	  }
	  else{
	  return 0;}
	  }
	  });
	  table.addColumnSortHandler(columnSort);
	  
		
	}
	
	public void addSortingcontractCountColumn() {List<CNC> list=getDataprovider().getList();
	  columnSort=new ListHandler<CNC>(list);
	  columnSort.setComparator(contractCountColumn, new Comparator<CNC>()
	  {
	  @Override
	  public int compare(CNC e1,CNC e2)
	  {
	  if(e1!=null && e2!=null)
	  {
	  if(e1.getCount()== e2.getCount()){
	  return 0;}
	  if(e1.getCount()> e2.getCount()){
	  return 1;}
	  else{
	  return -1;}
	  }
	  else{
	  return 0;}
	  }
	  });
	  table.addColumnSortHandler(columnSort);
	  
		
	}
	
	
	
	public void addSortingcontractProjectNameColumn() {
		List<CNC> list=getDataprovider().getList();
		  columnSort=new ListHandler<CNC>(list);
		  columnSort.setComparator(contractProjectNameColumn, new Comparator<CNC>()
		  {
		  @Override
		  public int compare(CNC e1,CNC e2)
		  {
		  if(e1!=null && e2!=null)
		  {
		  if( e1.getProjectName()!=null && e2.getProjectName()!=null){
		  return e1.getProjectName().compareTo(e2.getProjectName());}
		  }
		  else{
		  return 0;}
		  return 0;
		  }
		  });
		  table.addColumnSortHandler(columnSort);
	  
	  
		
	}
	public void addSortingcontractStartDateColumn() {
		List<CNC> list=getDataprovider().getList();
		  columnSort=new ListHandler<CNC>(list);
		  columnSort.setComparator(contractStartDateColumn, new Comparator<CNC>()
		  {
		  @Override
		  public int compare(CNC e1,CNC e2)
		  {
		  if(e1!=null && e2!=null)
		  {
		  if( e1.getContractStartDate()!=null && e2.getContractStartDate()!=null){
		  return e1.getContractStartDate().compareTo(e2.getContractStartDate());}
		  }
		  else{
		  return 0;}
		  return 0;
		  }
		  });
		  table.addColumnSortHandler(columnSort);
	  
		
	}
	public void addSortingcontractEndDateColumn() {
		List<CNC> list=getDataprovider().getList();
	  columnSort=new ListHandler<CNC>(list);
	  columnSort.setComparator(contractEndDateColumn, new Comparator<CNC>()
	  {
	  @Override
	  public int compare(CNC e1,CNC e2)
	  {
	  if(e1!=null && e2!=null)
	  {
	  if( e1.getContractEndDate()!=null && e2.getContractEndDate()!=null){
	  return e1.getContractEndDate().compareTo(e2.getContractEndDate());}
	  }
	  else{
	  return 0;}
	  return 0;
	  }
	  });
	  table.addColumnSortHandler(columnSort);

	  
		
	}
	
	public void addSortingaddStatusColumn() {
		List<CNC> list=getDataprovider().getList();
		  columnSort=new ListHandler<CNC>(list);
		  columnSort.setComparator(StatusColumn, new Comparator<CNC>()
		  {
		  @Override
		  public int compare(CNC e1,CNC e2)
		  {
		  if(e1!=null && e2!=null)
		  {
		  if( e1.getStatus()!=null && e2.getStatus()!=null){
		  return e1.getStatus().compareTo(e2.getStatus());}
		  }
		  else{
		  return 0;}
		  return 0;
		  }
		  });
		  table.addColumnSortHandler(columnSort);
		
	}
	
	
	
	
}	

