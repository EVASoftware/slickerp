package com.slicktechnologies.client.views.cnc;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.slicktechnologies.shared.CNCArrearsDetails;
import com.slicktechnologies.shared.common.cnc.CNC;
import com.slicktechnologies.shared.common.salesprocess.AnnexureType;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

@RemoteServiceRelativePath("cncbillservice")
public interface CncBillService extends RemoteService {
	public String generateBill(Date fromDate,Date toDate,CNC cnc,int salesOrderId);
	
	public String generateProject(CNC cnc);
	/** date 23.1.2019 added by komal to add validation while cancelling CNC
	 *  project attendance and billing validation 
	 */
	public String validateProjectAttendanceAndBilling(CNC cnc);
	/** date 23.1.2019 added by komal
	 *  Description : to cancel cnc and to inactive cnc and hr project
	 */
	public String cancelCNC(CNC cnc , String remark);
	
	public String generateArrearsBill(Date fromDate,Date toDate,CNC cnc,ArrayList<CNCArrearsDetails> list);
	
	/**
	 * @author Anil @since 25-03-2021
	 * This method returns attendance summary in case of CNC bills
	 * For Sunrise raised by Rahul
	 * @param companyId
	 * @param invoice
	 * @return
	 */
	public HashMap<String,ArrayList<AnnexureType>> getAnnexureDetailsForCNCBill(long companyId,Invoice invoice);
}
