package com.slicktechnologies.client.views.cnc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Vector;







import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.dom.client.Style.VerticalAlign;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TabPanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.approvalutility.ApprovableFormScreen;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.DataMigrationService;
import com.slicktechnologies.client.services.DataMigrationServiceAsync;
import com.slicktechnologies.client.services.DocumentUploadService;
import com.slicktechnologies.client.services.DocumentUploadServiceAsync;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.popups.GeneralComposite;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.common.businessprocesslayer.ConcreteBusinessProcess;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cnc.CNC;
import com.slicktechnologies.shared.common.cnc.Consumables;
import com.slicktechnologies.shared.common.cnc.EquipmentRental;
import com.slicktechnologies.shared.common.cnc.OtRateDetails;
import com.slicktechnologies.shared.common.cnc.StaffingDetails;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Declaration;
import com.slicktechnologies.shared.common.helperlayer.State;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.humanresourcelayer.PaidLeave;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTCTemplate;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.LWF;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.ProfessionalTax;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.screenmenuconfiguration.MenuConfiguration;
import com.slicktechnologies.shared.screenmenuconfiguration.ScreenMenuConfiguration;

public class CNCForm extends ApprovableFormScreen<CNC> implements ClickHandler,
		ChangeHandler, ValueChangeHandler<String> {

	CNC cncObject = null;
	PersonInfoComposite personInfoComposite;
	TextBox tbContractId, tbStatus, tbRefNum, tbCreditPeriod, tbApprovedRemark,
	/****** tbNoOFMonthsForRental *****/
	tbInterestOfMachinary, tbMaintaniancePercentage, /**
	 * 
	 * tbOverHeadCostPerEmployee,
	 */
	tbManagementFeesPercentage, tbcBonus, tbNetPayableOFStaffing,
			tbGrossTotalOfStaffing, tbManagementFeesValue,
			tbNetPayableOFEquipment, tbNetPayableOFConsumabables,
			tbNetPayableOFOther, tbPaidLeavesDays/*
												 * Rahul Verma added on 08 Aug
												 * 2018
												 */   ;
	TextArea tbCancellationRemark;
	DateBox dbContractStartDate;
	DateBox dbContractEndDate;
	ObjectListBox<ConfigCategory> olbContractCategory, olbPaymentTerms;
	ObjectListBox<Config> olbContractGroup, olbcPaymentMethods;
	ObjectListBox<Type> olbContractType;
	ObjectListBox<Branch> olbbBranch;
	ObjectListBox<Employee> olbeSalesPerson, olbApproverName;
	ObjectListBox<HrProject> olbProject;
	/**
	 * Tables to be shown in tab
	 */
	StaffingDetailsTable staffingTable;
	EquipmentRentalTable equipmentRentalTable;
	ConsumablesTable consumableTable;
	OtherServicesTable otherServiceTable;
		// For version update only
	public static boolean isAmended = false;
	ListBox lstbVersion;

	Button bt_addStaffingDetails, bt_addEquipmentDetails,
			bt_consumablesDetails, bt_otherServices;

	TabPanel tabPanel;
	DoubleBox donetpayamt;
	CheckBox cb_Relever_for_Bonus;
	CheckBox cb_Relever_for_PaidLeaves;
	CheckBox cb_Cbonus;
	CheckBox cb_CLeave;
	
	/**Date 24-6-2019
	 * @author Amol
	 * added gratuity checkbox
	 */
	CheckBox cb_Gratuity;
	
	/**
	 * date 11.8.2018 added by komal for terms and conditions details in cost
	 * sheet
	 **/
	ObjectListBox<Declaration> obTermsandCondition;
	Button btAddTermsandConditions;
	TextArea taTermsandConditions;
	/** date 13.8.2018 added by komal for % and flat values**/
	DoubleBox dbFlatManagementFees;
	DoubleBox dbBonusPercentage;
	/**Date 29-5-2019 by amol for supervision and traning charges**/
	TextBox tbSupervision;
	TextBox tbTrainingCharges;
	
	DoubleBox dbFlatBonus;
	DoubleBox dbTravellingPercentage;
	DoubleBox dbFlatTravelling;
	DoubleBox dbOverheadCostPercentage;
	DoubleBox dbFlatOverheadCost;
	
	TextBox tbProjectName;
	/**
	 * end komal
	 */
	/*** date 21.8.2018 added by komal for version number**/
	DoubleBox dbVersionNumber;
	DoubleBox dbDiscount;
	DoubleBox dbTotalWithoutDiscount;
	/** date 27.10.2018 added by komal for invoice under contract number field in sasha cnc billing **/
	TextBox tbContractNumber;
	
	/** date 25.1.2019 added by komal to select designation and ctctemplate first **/
	GeneralComposite generalComposite;
	ObjectListBox<Config> olbDesignation;
	
	/**
	 * 
	 */
	ArrayList<ProfessionalTax> ptList=new ArrayList<ProfessionalTax>();
	ArrayList<LWF> lwfList=new ArrayList<LWF>();
	/**Date 25-6-2019 
	 * by Amol make project name enable by process configuration raised by Rahul for riseon
	 */
	boolean projectNameEnable=false;
	
	  boolean hideColumn=false;
	  
	  /**
	   * @author Anil , Date : 11-10-2019
	   * Adding actual monthly finance plan,interest on Financial Amount and Estimated Repair
	   */
	DoubleBox dbMonthlyFinPlan,dbInterestOnFinAmt,dbEstimatedRepair,dbAdminOhCost,dbApproxUniformCost;
	
	/**Date 3-7-2020 by Amol Added customer branch dropdown raised by Rahul Tiwari**/
	ObjectListBox<CustomerBranchDetails> oblCustomerBranch;
	List<CustomerBranchDetails> customerBranchList=new ArrayList<CustomerBranchDetails>();
	final GenricServiceAsync async =GWT.create(GenricService.class);
	Address billingAddress;
	Address serviceAddress;
	
	/**
	 * @author Anil
	 * @since 24-07-2020
	 * Adding field payroll start day which will define payroll cycle
	 */
	IntegerBox ibPayrollStartDay;
	boolean payrollCycleFlag;
	
	/**
	 * @author Anil
	 * @since 19-08-2020
	 * Sunrise facility : state and site location changes
	 * Raised by Rahul Tiwari
	 */
	ObjectListBox<State> oblState;
	ObjectListBox<PaidLeave> oblPaidLeave;
	boolean addStateFlag=false;
	boolean addPaidLeaveFlag=false;
	
	/**
	 * @author Anil
	 * @since 20-10-2020
	 * Sunrise facility : overtime details component
	 */
	ObjectListBox<Config> olbDesignation1;
	ListBox lbOtName;
	DoubleBox dbFlatRate;
	DoubleBox dbFactorRate;
//	CheckBox cbTypeDay;
//	CheckBox cbTypeHourly;
	Button btnAddOt;
	OtRateDetailTable otRateTbl;
	public boolean siteLocation=false;
	
	/**
	 * @author Anil @since 05-04-2021
	 * commenting below working hour 
	 * Raised by Rahul Tiwari for Alkosh
	 */
//	DoubleBox dbWorkingHours;
	
	
	/**
	 * @author Anil @since 30-03-2021
	 * Adding GST number field on CNC
	 * Raised by Rahul Tiwari for Sunrise
	 */
	TextBox tbCustomerGstNumber;
	boolean addGstFieldFlag=false;
	
	/**
	 * @author Anil @since 10-11-2021
	 * Sunrise SCM
	 */
	Button btnUploadConsumable;
	boolean uploadConsumableFlag=false;
	
	public CNCForm() {
		super(FormStyle.DEFAULT);
		System.out.println("CNC COnstructor");

		cncObject = new CNC();
		createGui();
		this.tbContractId.setEnabled(false);
		this.tbStatus.setEnabled(false);
		this.tbStatus.setText(Contract.CREATED);
		staffingTable.connectToLocal();
		equipmentRentalTable.connectToLocal();
		consumableTable.connectToLocal();
		otherServiceTable.connectToLocal();

		// tbNoOFMonthsForRental.addChangeHandler(this);
		tbManagementFeesPercentage.addChangeHandler(this);
		tbInterestOfMachinary.addChangeHandler(this);
		tbMaintaniancePercentage.addChangeHandler(this);
		lstbVersion.addChangeHandler(this);
		cb_Relever_for_Bonus.addClickHandler(this);
		cb_Relever_for_PaidLeaves.addClickHandler(this);
		cb_Cbonus.addClickHandler(this);
		cb_CLeave.addClickHandler(this);
		cb_Gratuity.addClickHandler(this);
		/**
		 * Rahul Verma added on 08 Aug 2018
		 */
		tbPaidLeavesDays.addChangeHandler(this);
	//	btAddTermsandConditions.addClickHandler(this);
		/** date 13.8.2018 added by komal for % and flat values**/
		tbManagementFeesPercentage.addChangeHandler(this);
		dbFlatManagementFees.addChangeHandler(this);
		dbBonusPercentage.addChangeHandler(this);
		dbFlatBonus.addChangeHandler(this);
		dbTravellingPercentage.addChangeHandler(this);
		dbFlatTravelling.addChangeHandler(this);
		dbOverheadCostPercentage.addChangeHandler(this);
		dbFlatOverheadCost.addChangeHandler(this);
		dbDiscount.addChangeHandler(this);
		tbTrainingCharges.addChangeHandler(this);
		tbSupervision.addChangeHandler(this);
		
		otRateTbl.connectToLocal();
		
		btnAddOt.addClickHandler(this);		
//		cbTypeDay.addClickHandler(this);
//		cbTypeHourly.addClickHandler(this);
		
		siteLocation=AppUtility.checkForProcessConfigurartionIsActiveOrNot("CNC", "EnableSiteLocation");
		clearTabFormFields();
		
		payrollCycleFlag=AppUtility.checkForProcessConfigurartionIsActiveOrNot("CNC", "PayrollDayForMultiplePayrollCycle");
		
		addStateFlag=AppUtility.checkForProcessConfigurartionIsActiveOrNot("CNC", "AddStatefield");
		addPaidLeaveFlag=AppUtility.checkForProcessConfigurartionIsActiveOrNot("CNC", "HidePaidLeaveFieldAndAddDropdown");
		uploadConsumableFlag=AppUtility.checkForProcessConfigurartionIsActiveOrNot("CNC", "PC_UploadLocationWiseBudget");

		uploadPopup.getBtnOk().addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				
				if(uploadPopup.validate()){
					reactToConsumableUploadProcess();
				}
			}
		});
		
		uploadPopup.getBtnCancel().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				uploadPanel.hide();
			}
		});
	}

	@SuppressWarnings("unused")
	private void initalizeWidget() {
		siteLocation=AppUtility.checkForProcessConfigurartionIsActiveOrNot("CNC", "EnableSiteLocation");
		uploadConsumableFlag=AppUtility.checkForProcessConfigurartionIsActiveOrNot("CNC", "PC_UploadLocationWiseBudget");
		System.out.println("InitializeWidget");
		staffingTable = new StaffingDetailsTable();
		equipmentRentalTable = new EquipmentRentalTable();
		consumableTable = new ConsumablesTable();
		otherServiceTable = new OtherServicesTable();

		personInfoComposite = AppUtility.customerInfoComposite(new Customer());
		personInfoComposite.getCustomerId().getHeaderLabel()
				.setText("* Customer ID");
		personInfoComposite.getCustomerName().getHeaderLabel()
				.setText("* Customer Name");
		personInfoComposite.getCustomerCell().getHeaderLabel()
				.setText("* Customer Cell");
		personInfoComposite.getPocName().getHeaderLabel().setText("POC Name");

		tbContractId = new TextBox();
		tbContractId.setEnabled(false);

//		dbContractStartDate = new DateBox();
		dbContractStartDate = new DateBoxWithYearSelector();
//		dbContractEndDate = new DateBox();
		dbContractEndDate = new DateBoxWithYearSelector();


		tbStatus = new TextBox();
		tbStatus.setText(Contract.CREATED);

		donetpayamt = new DoubleBox();
		donetpayamt.setEnabled(false);

		olbContractGroup = new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbContractGroup, Screen.CONTRACTGROUP);

		olbContractCategory = new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(olbContractCategory,
				Screen.CONTRACTCATEGORY);
		olbContractCategory.addChangeHandler(this);

		olbContractType = new ObjectListBox<Type>();
		AppUtility.makeTypeListBoxLive(olbContractType, Screen.CONTRACTTYPE);

		olbbBranch = new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbbBranch);

		olbeSalesPerson = new ObjectListBox<Employee>();
//		AppUtility.makeSalesPersonListBoxLive(olbeSalesPerson);
		olbeSalesPerson.makeEmployeeLive(AppConstants.SERVICEMODULE, AppConstants.CNC, "Sales Person");


		tbRefNum = new TextBox();

		olbcPaymentMethods = new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbcPaymentMethods, Screen.PAYMENTMETHODS);

		olbPaymentTerms = new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(olbPaymentTerms,
				Screen.ADDPAYMENTTERMS);

		tbCreditPeriod = new TextBox();

		olbApproverName = new ObjectListBox<Employee>();
		AppUtility.makeApproverListBoxLive(olbApproverName, "CNC");

		tbApprovedRemark = new TextBox();
		tbApprovedRemark.setEnabled(false);

		// tbNoOFMonthsForRental = new TextBox();
		tbInterestOfMachinary = new TextBox();
		tbInterestOfMachinary.setValue(0 + "");
		tbMaintaniancePercentage = new TextBox();
		tbMaintaniancePercentage.setValue(0 + "");
		tbManagementFeesPercentage = new TextBox();
		tbManagementFeesPercentage.setValue(0 + "");
		
		tbSupervision=new TextBox();
		tbSupervision.setValue(0 + "");
		
		tbTrainingCharges=new TextBox();
		tbTrainingCharges.setValue(0 + "");
		
		
		
		// tbOverHeadCostPerEmployee = new TextBox();

		lstbVersion = new ListBox();
		lstbVersion.addItem("--SELECT--");

		olbProject = new ObjectListBox<HrProject>();
		HrProject.MakeProjectListBoxLive(olbProject);

		bt_addStaffingDetails = new Button("Add Staffing Details");
		bt_addStaffingDetails.getElement().addClassName("buttonMarginBottom");
		bt_addStaffingDetails.addClickHandler(this);

		bt_addEquipmentDetails = new Button("Add Housekeeping Equipment");
		bt_addEquipmentDetails.getElement().addClassName("buttonMarginBottom");
		bt_addEquipmentDetails.addClickHandler(this);

		bt_consumablesDetails = new Button("Add Housekeeping Consumables");
		bt_consumablesDetails.getElement().addClassName("buttonMarginBottom");
		bt_consumablesDetails.addClickHandler(this);

		bt_otherServices = new Button("Add Other Services");
		bt_otherServices.getElement().addClassName("buttonMarginBottom");
		bt_otherServices.addClickHandler(this);

		tbcBonus = new TextBox();
		tbcBonus.addChangeHandler(this);
		tbNetPayableOFStaffing = new TextBox();
		tbNetPayableOFStaffing.setEnabled(false);
		tbGrossTotalOfStaffing = new TextBox();
		tbGrossTotalOfStaffing.setValue(0 + "");
		tbGrossTotalOfStaffing.setEnabled(false);
		tbManagementFeesValue = new TextBox();
		tbManagementFeesValue.setValue(0 + "");
		tbManagementFeesValue.setEnabled(false);
		tbNetPayableOFEquipment = new TextBox();
		tbNetPayableOFEquipment.setEnabled(false);
		tbNetPayableOFConsumabables = new TextBox();
		tbNetPayableOFConsumabables.setEnabled(false);
		tbNetPayableOFOther = new TextBox();
		tbNetPayableOFOther.setEnabled(false);
		donetpayamt = new DoubleBox();
		donetpayamt.setEnabled(false);

		cb_Relever_for_Bonus = new CheckBox();
		cb_Relever_for_Bonus.setValue(false);

		cb_Relever_for_PaidLeaves = new CheckBox();
		cb_Relever_for_PaidLeaves.setValue(false);

		cb_Cbonus = new CheckBox();
		cb_Cbonus.setValue(false);

		cb_CLeave = new CheckBox();
		cb_CLeave.setValue(false);
		
		cb_Gratuity=new CheckBox();
		cb_Gratuity.setValue(false);
		
		
		/*****31-1-2019  added by amol****/
		tbCancellationRemark=new TextArea();
		tbCancellationRemark.setEnabled(false);
	
		/**
		 * date 11.8.2018 added by komal for terms and conditions details in
		 * cost sheet
		 **/
		obTermsandCondition = new ObjectListBox<Declaration>();
		MyQuerry querry123 = new MyQuerry();
		querry123.setQuerryObject(new Declaration());
		obTermsandCondition.MakeLive(querry123);
		btAddTermsandConditions = new Button("ADD");
		btAddTermsandConditions.getElement().addClassName("buttonMarginBottom");
		btAddTermsandConditions.addClickHandler(this);
		taTermsandConditions = new TextArea();
		/**
		 * end komal
		 */
		/**
		 * Rahul added on 08 Aug 2018
		 */
		tbPaidLeavesDays=new TextBox();
		tbPaidLeavesDays.setValue(0+"");
		/** date 13.8.2018 added by komal for % and flat values**/
		dbFlatManagementFees = new DoubleBox();
		dbBonusPercentage = new DoubleBox();
		dbFlatBonus = new DoubleBox();
		dbTravellingPercentage = new DoubleBox();
		dbFlatTravelling = new DoubleBox();
		dbOverheadCostPercentage = new DoubleBox();
		dbFlatOverheadCost = new DoubleBox();
		
		
		
		
		tbProjectName = new TextBox();
		tbProjectName.addValueChangeHandler(this);
		/**
		 * end komal
		 */
		/*** date 21.8.2018 added by komal for version number**/
		dbVersionNumber = new DoubleBox();
		dbVersionNumber.setEnabled(false);
		dbDiscount = new DoubleBox();
		dbTotalWithoutDiscount = new DoubleBox();
		dbTotalWithoutDiscount.setEnabled(false);
		/*** date 21.8.2018 added by komal for version number**/
		dbVersionNumber = new DoubleBox();
		dbVersionNumber.setEnabled(false);
		tbContractNumber = new TextBox();
		
		dbMonthlyFinPlan=new DoubleBox();
		dbInterestOnFinAmt=new DoubleBox();
		dbEstimatedRepair=new DoubleBox();
		dbAdminOhCost=new DoubleBox();
		dbApproxUniformCost=new DoubleBox();
		
		oblCustomerBranch=new ObjectListBox<CustomerBranchDetails>();
		oblCustomerBranch.addItem("--SELECT--");
//		oblCustomerBranch.addItem("Actual Address");
		
		oblPaidLeave=new ObjectListBox<PaidLeave>();
		Filter temp=new Filter();
		temp.setQuerryString("status");
		temp.setBooleanvalue(true);
		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new PaidLeave());
		querry.getFilters().add(temp);
		oblPaidLeave.MakeLive(querry);
		oblPaidLeave.addChangeHandler(this);
		
		btnUploadConsumable=new Button("Upload Consumables");
		btnUploadConsumable.addClickHandler(this);
		
		/** date 25.1.2019 added by komal to select designation and ctctemplate first **/
		MyQuerry qu = new MyQuerry();
		Company c = new Company();
		System.out.println("Company Id :: " + c.getCompanyId());

		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;

		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		/**
		 * @author Vijay Date :- 15-06-2023
		 * Des :- Speak & Span want to load both ctc on CNC
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.CNC, AppConstants.PC_ENABLELOADCNCHRTEMPLATE)){
			ArrayList<String> list = new ArrayList<String>();
			list.add(AppConstants.CNC);
			list.add(AppConstants.HR);
			list.add("cnc");
			list.add("hr");
			list.add("");

			filter = new Filter();
			filter.setQuerryString("category IN");
			filter.setList(list);
			filtervec.add(filter);
		}
		else{
			filter = new Filter();
			filter.setQuerryString("category");
			filter.setStringValue(AppConstants.CNC);
			filtervec.add(filter);
		}
		
		
		qu.setFilters(filtervec);
		qu.setQuerryObject(new CTCTemplate());
		generalComposite = new GeneralComposite("CTC Template", qu);
		generalComposite.setTitle("Set category as CNC in your CTC Template, then only you can select it from this box.\r\n If you want to see both CNC and HR ctc templates then activate process config "+AppConstants.PC_ENABLELOADCNCHRTEMPLATE);//5-12-2023
		olbDesignation = new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbDesignation, Screen.EMPLOYEEDESIGNATION);
		
		FlowPanel fp_LeverPanel = new FlowPanel();
		FlowPanel fp_employeePanel = new FlowPanel();
		FlowPanel fp_equipmentPanel = new FlowPanel();
		FlowPanel fp_consumablesPanel = new FlowPanel();
		FlowPanel fp_otherServicesPanel = new FlowPanel();
		/**
		 * date 11.8.2018 added by komal for terms and conditions details in
		 * cost sheet
		 **/
		FlowPanel fp_termsAndConditionsPanel = new FlowPanel();
		fp_LeverPanel.setWidth("100%");
		VerticalPanel verLeverPanel = new VerticalPanel();
		verLeverPanel.setWidth("100%");
		// verLeverPanel.setHeight("600px");
		verLeverPanel.add(getLeverPanel());

		VerticalPanel verPanelStaffingPanel = new VerticalPanel();
		verPanelStaffingPanel.setWidth("100%");
		// verPanelStaffingPanel.setHeight("600px");
		verPanelStaffingPanel.add(getPanelForStaffingDetails());
		verPanelStaffingPanel.add(staffingTable.getTable());

		HorizontalPanel horizontalPanel = new HorizontalPanel();
		horizontalPanel.getElement().getStyle().setWidth(100, Unit.PCT);
		horizontalPanel.add(getFieldLabel(" "));
		horizontalPanel.add(getFieldLabel(" "));
		horizontalPanel.add(getFieldLabel(" "));
		horizontalPanel.add(getFieldLabel(" "));
		horizontalPanel.add(getFieldLabel(" "));
		horizontalPanel.add(getFieldLabel(" "));
		//horizontalPanel.add(getFieldTB("Gross Total", tbGrossTotalOfStaffing));
		horizontalPanel.add(getFieldTB("Grand Total", tbGrossTotalOfStaffing));
		verPanelStaffingPanel.add(horizontalPanel);

		horizontalPanel = new HorizontalPanel();
		horizontalPanel.getElement().getStyle().setWidth(100, Unit.PCT);
		horizontalPanel.add(getFieldLabel(" "));
		horizontalPanel.add(getFieldLabel(" "));
		horizontalPanel.add(getFieldLabel(" "));
		horizontalPanel.add(getFieldLabel(" "));
		horizontalPanel.add(getFieldLabel(" "));
		horizontalPanel.add(getFieldLabel(" "));
		horizontalPanel
				.add(getFieldTB("Management Fees", tbManagementFeesValue));
		verPanelStaffingPanel.add(horizontalPanel);

		horizontalPanel = new HorizontalPanel();
		horizontalPanel.getElement().getStyle().setWidth(100, Unit.PCT);
		horizontalPanel.add(getFieldLabel(" "));
		horizontalPanel.add(getFieldLabel(" "));
		horizontalPanel.add(getFieldLabel(" "));
		horizontalPanel.add(getFieldLabel(" "));
		horizontalPanel.add(getFieldLabel(" "));
		horizontalPanel.add(getFieldLabel(" "));
	//	horizontalPanel.add(getFieldTB("Total Payable", tbNetPayableOFStaffing));
		horizontalPanel.add(getFieldTB("Grand total Including Management Fees", tbNetPayableOFStaffing));
		
		verPanelStaffingPanel.add(horizontalPanel);

		VerticalPanel equipmentVerPanel = new VerticalPanel();
		equipmentVerPanel.setWidth("100%");
		// equipmentVerPanel.setHeight("600px");
		// equipmentVerPanel.add();
		equipmentVerPanel.add(getFormPanelForEquiment());
		equipmentVerPanel.add(equipmentRentalTable.getTable());
//		HorizontalPanel horizontalPanelEquip = new HorizontalPanel();
//		horizontalPanelEquip.getElement().getStyle().setWidth(100, Unit.PCT);
//		horizontalPanelEquip.add(getFieldLabel(" "));
//		horizontalPanelEquip.add(getFieldLabel(" "));
//		horizontalPanelEquip.add(getFieldLabel(" "));
//		horizontalPanelEquip.add(getFieldLabel(" "));
//		horizontalPanelEquip.add(getFieldLabel(" "));
//		horizontalPanelEquip.add(getFieldLabel(" "));
//		horizontalPanelEquip.add(getFieldTB("Total Payable",
//				tbNetPayableOFEquipment));
//		equipmentVerPanel.add(horizontalPanelEquip);
		HorizontalPanel horizontalPanelEquip = new HorizontalPanel();
		horizontalPanelEquip.getElement().getStyle().setWidth(100, Unit.PCT);
		horizontalPanelEquip.add(getFieldLabel(" "));
		horizontalPanelEquip.add(getFieldLabel(" "));
		horizontalPanelEquip.add(getFieldLabel(" "));
		horizontalPanelEquip.add(getFieldLabel(" "));
		horizontalPanelEquip.add(getFieldLabel(" "));
		horizontalPanelEquip.add(getFieldLabel(" "));
		horizontalPanelEquip.add(getFieldTB("Total",
				dbTotalWithoutDiscount));
		equipmentVerPanel.add(horizontalPanelEquip);
		
		horizontalPanelEquip = new HorizontalPanel();
		horizontalPanelEquip.getElement().getStyle().setWidth(100, Unit.PCT);
		horizontalPanelEquip.add(getFieldLabel(" "));
		horizontalPanelEquip.add(getFieldLabel(" "));
		horizontalPanelEquip.add(getFieldLabel(" "));
		horizontalPanelEquip.add(getFieldLabel(" "));
		horizontalPanelEquip.add(getFieldLabel(" "));
		horizontalPanelEquip.add(getFieldLabel(" "));
		horizontalPanelEquip.add(getFieldTB("Discount",
				dbDiscount));
		equipmentVerPanel.add(horizontalPanelEquip);
		
		horizontalPanelEquip = new HorizontalPanel();
		horizontalPanelEquip.getElement().getStyle().setWidth(100, Unit.PCT);
		horizontalPanelEquip.add(getFieldLabel(" "));
		horizontalPanelEquip.add(getFieldLabel(" "));
		horizontalPanelEquip.add(getFieldLabel(" "));
		horizontalPanelEquip.add(getFieldLabel(" "));
		horizontalPanelEquip.add(getFieldLabel(" "));
		horizontalPanelEquip.add(getFieldLabel(" "));
		horizontalPanelEquip.add(getFieldTB("Total Payable",
				tbNetPayableOFEquipment));
		equipmentVerPanel.add(horizontalPanelEquip);

		VerticalPanel consumablesVerPanel = new VerticalPanel();
		consumablesVerPanel.setWidth("100%");
		// consumablesVerPanel.setHeight("600px");
		
		if(uploadConsumableFlag){
			HorizontalPanel horizontalPanelCon = new HorizontalPanel();
			horizontalPanelCon.getElement().getStyle().setWidth(25, Unit.PCT);
			horizontalPanelCon.add(bt_consumablesDetails);
			horizontalPanelCon.add(btnUploadConsumable);
			consumablesVerPanel.add(horizontalPanelCon);
		}else{
			consumablesVerPanel.add(bt_consumablesDetails);
		}
		
		consumablesVerPanel.add(consumableTable.getTable());
		HorizontalPanel horizontalPanelCon = new HorizontalPanel();
		horizontalPanelCon.getElement().getStyle().setWidth(100, Unit.PCT);
		horizontalPanelCon.add(getFieldLabel(" "));
		horizontalPanelCon.add(getFieldLabel(" "));
		horizontalPanelCon.add(getFieldLabel(" "));
		horizontalPanelCon.add(getFieldLabel(" "));
		horizontalPanelCon.add(getFieldLabel(" "));
		horizontalPanelCon.add(getFieldLabel(" "));
		horizontalPanelCon.add(getFieldTB("Total Payable",
				tbNetPayableOFConsumabables));
		consumablesVerPanel.add(horizontalPanelCon);

		VerticalPanel otherServiceVerPanel = new VerticalPanel();
		otherServiceVerPanel.setWidth("100%");
		// otherServiceVerPanel.setHeight("600px");
		otherServiceVerPanel.add(bt_otherServices);
		otherServiceVerPanel.add(otherServiceTable.getTable());
		HorizontalPanel horizontalPanelOther = new HorizontalPanel();
		horizontalPanelOther.getElement().getStyle().setWidth(100, Unit.PCT);
		horizontalPanelOther.add(getFieldLabel(" "));
		horizontalPanelOther.add(getFieldLabel(" "));
		horizontalPanelOther.add(getFieldLabel(" "));
		horizontalPanelOther.add(getFieldLabel(" "));
		horizontalPanelOther.add(getFieldLabel(" "));
		horizontalPanelOther.add(getFieldLabel(" "));
		horizontalPanelOther.add(getFieldTB("Total Payable",
				tbNetPayableOFOther));
		otherServiceVerPanel.add(horizontalPanelOther);

		/**
		 * date 11.8.2018 added by komal for terms and conditions details in
		 * cost sheet
		 **/
		VerticalPanel verTermsAndConditionsPanel = new VerticalPanel();
		verTermsAndConditionsPanel.setWidth("100%");
		// verLeverPanel.setHeight("600px");
		verTermsAndConditionsPanel.add(getPanelForTermasandConditions());

		fp_LeverPanel.add(verLeverPanel);
		fp_employeePanel.add(verPanelStaffingPanel);
		fp_equipmentPanel.add(equipmentVerPanel);
		fp_consumablesPanel.add(consumablesVerPanel);
		fp_otherServicesPanel.add(otherServiceVerPanel);
		/**
		 * date 11.8.2018 added by komal for terms and conditions details in
		 * cost sheet
		 **/
		fp_termsAndConditionsPanel.add(verTermsAndConditionsPanel);
		
		
		/**
		 * @author Anil
		 * @since 20-10-2020
		 * Adding overtime details tab for sunrise raised by Rahul
		 */
		
		olbDesignation1=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbDesignation1, Screen.EMPLOYEEDESIGNATION);
		
		lbOtName=new ListBox();
		lbOtName.addItem("--SELECT--");
		lbOtName.addItem("Normal Day OT");
		lbOtName.addItem("Public Holiday OT");
		lbOtName.addItem("National Holiday OT");
		
		dbFactorRate=new DoubleBox();
		dbFlatRate=new DoubleBox();
		
		dbFactorRate.addValueChangeHandler(new ValueChangeHandler<Double>() {
			
			@Override
			public void onValueChange(ValueChangeEvent<Double> event) {
				// TODO Auto-generated method stub
				if(dbFactorRate.getValue()!=0){
					dbFlatRate.setValue(null);
				}
			}
		});
		
		dbFlatRate.addValueChangeHandler(new ValueChangeHandler<Double>() {
			
			@Override
			public void onValueChange(ValueChangeEvent<Double> event) {
				// TODO Auto-generated method stub
				if(dbFlatRate.getValue()!=0){
					dbFactorRate.setValue(null);
				}
			}
		});
		
//		cbTypeDay=new CheckBox();
//		cbTypeHourly=new CheckBox();
		btnAddOt=new Button("Add");
		
		otRateTbl=new OtRateDetailTable();
		
		VerticalPanel verOtRateDetailsPanel = new VerticalPanel();
		verOtRateDetailsPanel.setWidth("100%");
		
		HorizontalPanel hrOtRateDetailsPanel = new HorizontalPanel();
		hrOtRateDetailsPanel.getElement().getStyle().setWidth(100, Unit.PCT);
		hrOtRateDetailsPanel.add(getFieldOB("Designation", olbDesignation1));
		hrOtRateDetailsPanel.add(getFieldLB("Overtime", lbOtName));
		
		hrOtRateDetailsPanel.add(getFieldTB("Flat Rate", dbFlatRate));
		hrOtRateDetailsPanel.add(getFieldTB("Rate", dbFactorRate));
		dbFlatRate.getElement().getStyle().setHeight(18, Unit.PX);
		dbFactorRate.getElement().getStyle().setHeight(18, Unit.PX);
		
//		hrOtRateDetailsPanel.add(getFieldCB("Day", cbTypeDay));
//		hrOtRateDetailsPanel.add(getFieldCB("Hourly", cbTypeHourly));
		
		hrOtRateDetailsPanel.add(getFieldButton("", btnAddOt));
		btnAddOt.getElement().getStyle().setMarginTop(11, Unit.PX);
		
		verOtRateDetailsPanel.add(hrOtRateDetailsPanel);
		verOtRateDetailsPanel.add(otRateTbl.getTable());
		
		tabPanel = new TabPanel();
		tabPanel.addSelectionHandler(new SelectionHandler<Integer>() {
			@Override
			public void onSelection(SelectionEvent<Integer> event) {
				int tabId = event.getSelectedItem();
				System.out.println("TAB ID " + tabId);
				if (tabId == 0) {

				} else if (tabId == 1) {
					staffingTable.getTable().redraw();
				} else if (tabId == 2) {
					equipmentRentalTable.getTable().redraw();
				} else if (tabId == 3) {
					consumableTable.getTable().redraw();
				} else if (tabId == 4) {
					otherServiceTable.getTable().redraw();
				}else if (tabId == 6) {
					otRateTbl.getTable().redraw();
				}
			}
		});
		tabPanel.add(fp_LeverPanel, "Lever");
		tabPanel.add(fp_employeePanel, "Staffing Details");
		tabPanel.add(fp_equipmentPanel, "Housekeeping Equipment Charges");
		tabPanel.add(fp_consumablesPanel, "Housekeeping Consumables");
		tabPanel.add(fp_otherServicesPanel, "Other Services");
		/** date 11.8.2018 added by komal for terms and conditions details in cost sheet**/
		tabPanel.add(fp_termsAndConditionsPanel, "Terms and Conditions");
		
		if(siteLocation){
			tabPanel.add(verOtRateDetailsPanel, "Overtime");
		}
		tabPanel.setSize("100%", "700px");

		tabPanel.selectTab(0);
		
		ibPayrollStartDay=new IntegerBox();
		
		oblState=new ObjectListBox<State>();
		State.makeOjbectListBoxLive(oblState);
		oblState.addChangeHandler(this);
		
//		dbWorkingHours=new DoubleBox();		
		
		tbCustomerGstNumber=new TextBox();
		
	}

	private void loadPtAndLwf() {
		// TODO Auto-generated method stub
		final GenricServiceAsync service = GWT.create(GenricService.class);
		MyQuerry querry1=new MyQuerry();
		Filter temp1=null;
		temp1=new Filter();
		temp1.setQuerryString("status");
		temp1.setBooleanvalue(true);
		querry1.getFilters().add(temp1);
		querry1.setQuerryObject(new ProfessionalTax());
		service.getSearchResult(querry1, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				System.out.println("QUERRY PT SIZE : "+result.size());
				ptList=new ArrayList<ProfessionalTax>();
				for(SuperModel model:result){
					ProfessionalTax pt=(ProfessionalTax) model;
					ptList.add(pt);
				}
				// TODO Auto-generated method stub
				MyQuerry querry2=new MyQuerry();
				Filter temp2=null;
				temp2=new Filter();
				temp2.setQuerryString("status");
				temp2.setBooleanvalue(true);
				querry2.getFilters().add(temp2);
				querry2.setQuerryObject(new LWF());
				service.getSearchResult(querry2, new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						System.out.println("QUERRY LWF SIZE : "+result.size());
						// TODO Auto-generated method stub
						lwfList=new ArrayList<LWF>();
						for(SuperModel model:result){
							LWF lwf=(LWF) model;
							lwfList.add(lwf);
						}
						
						staffingTable=new StaffingDetailsTable(ptList,lwfList);
					}
					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
					}
				});
			}
		});
		
		
	}

	private Widget getFieldLabel(String labelName) {
		// TODO Auto-generated method stub
		VerticalPanel verticalPanel = new VerticalPanel();
		verticalPanel.getElement().getStyle().setMargin(2.0, Unit.PCT);
		Label label = new Label(labelName);
		label.setWidth("100px");
		verticalPanel.add(label);
		return verticalPanel;
	}

	private Widget getPanelForStaffingDetails() {
		// TODO Auto-generated method stub
		VerticalPanel verticalPnl = new VerticalPanel();
		HorizontalPanel horizontalPanel = new HorizontalPanel();
		horizontalPanel.getElement().getStyle().setWidth(100, Unit.PCT);
		/** date 25.1.2019 added by komal to select designation and ctctemplate first **/
		horizontalPanel.add(getFieldOB("Designation", olbDesignation));
		horizontalPanel.add(generalComposite);
		horizontalPanel.add(getFieldButton(". ", bt_addStaffingDetails));
		horizontalPanel.getElement().getStyle().setWidth(100, Unit.PCT);
		horizontalPanel.add(getFieldTB("C Bonus(%)", tbcBonus));
		
		if(addPaidLeaveFlag){
			horizontalPanel.add(getFieldOB("Paid Leave", oblPaidLeave));
		}else{
			horizontalPanel.add(getFieldCB("Bonus Reliever", cb_Relever_for_Bonus));
			horizontalPanel.add(getFieldCB("PL Reliever",cb_Relever_for_PaidLeaves));
			horizontalPanel.add(getFieldCB("C Leave", cb_CLeave));// d
			horizontalPanel.add(getFieldCB("C Bonus", cb_Cbonus));
	//		horizontalPanel.add(getFieldCB("Gratuity",cb_Gratuity));
		}

		verticalPnl.add(horizontalPanel);

		return verticalPnl;
	}

	private Widget getFieldButton(String labelName, Button button) {
		// TODO Auto-generated method stub
		VerticalPanel verticalPanel = new VerticalPanel();
		verticalPanel.getElement().getStyle().setMargin(2.0, Unit.PCT);
		Label label = new Label(labelName);
		// label.getElement().getStyle().setMarginBottom(5.0,Unit.PX);
		// label.getElement().getStyle().setMarginRight(5.0,Unit.PX);

		verticalPanel.add(label);
		button.getElement().getStyle().setMarginRight(5.0, Unit.PX);
		button.getElement().getStyle().setWidth(150, Unit.PX);
		verticalPanel.add(button);

		return verticalPanel;
	}

	private Widget getFieldCB(String labelName, CheckBox checkBox) {
		// TODO Auto-generated method stub
		VerticalPanel verticalPanel = new VerticalPanel();
		verticalPanel.getElement().getStyle().setMargin(2.0, Unit.PCT);
		Label label = new Label(labelName);
		label.setWidth("100px");
		// label.getElement().getStyle().setMarginBottom(5.0,Unit.PX);
		// label.getElement().getStyle().setMarginRight(5.0,Unit.PX);

		verticalPanel.add(label);
		checkBox.getElement().getStyle().setMarginRight(5.0, Unit.PX);
		checkBox.getElement().getStyle().setMarginTop(5.0, Unit.PX);
		checkBox.getElement().getStyle().setWidth(150, Unit.PX);
		verticalPanel.add(checkBox);

		return verticalPanel;
	}

	private VerticalPanel getLeverPanel() {
		VerticalPanel panel = new VerticalPanel();
		VerticalPanel panel1 = new VerticalPanel();
		VerticalPanel panel2 = new VerticalPanel();
		
		/**
		 * @author Anil
		 * @since 07-07-2020
		 */
		HorizontalPanel hpGroupingPanel1 = new HorizontalPanel();
		hpGroupingPanel1.getElement().getStyle().setWidth(115, Unit.PCT);
		InlineLabel lbl1=new InlineLabel();
		lbl1.setText("Chargeable Rate (Monthly Amt)");
		hpGroupingPanel1.add(lbl1);
		hpGroupingPanel1.getElement().addClassName("grouping");
		
		HorizontalPanel hpGroupingPanel2 = new HorizontalPanel();
		hpGroupingPanel2.getElement().getStyle().setWidth(115, Unit.PCT);
		InlineLabel lbl2=new InlineLabel();
		lbl2.setText("Actual Rate (Monthly Amt)");
		hpGroupingPanel2.add(lbl2);
		hpGroupingPanel2.getElement().addClassName("grouping");
		
		panel2.getElement().getStyle().setVerticalAlign(VerticalAlign.MIDDLE);
		
		HorizontalPanel horizontalPanel = new HorizontalPanel();
		horizontalPanel.getElement().getStyle().setWidth(100, Unit.PCT);
		
		horizontalPanel.add(getFieldTB("Management Fees %", tbManagementFeesPercentage));
		
		if(!addPaidLeaveFlag){
			horizontalPanel.add(getFieldTB("Paid Leave Days", tbPaidLeavesDays));
		}
		
		horizontalPanel.add(getFieldTB("Management Fees (Flat)", dbFlatManagementFees));
		horizontalPanel.add(getFieldTB("Bonus (Flat)", dbFlatBonus));
		
		horizontalPanel.add(getFieldTB("Travelling (Flat)", dbFlatTravelling));
		horizontalPanel.add(getFieldTB("Overhead Cost (Flat)", dbFlatOverheadCost));
		
		panel.add(horizontalPanel);
		
		HorizontalPanel horizontalPanel1 = new HorizontalPanel();
		horizontalPanel1.getElement().getStyle().setWidth(100, Unit.PCT);
//		horizontalPanel1.add(getFieldTB("Management Fees (Flat)", dbFlatManagementFees));
//		horizontalPanel1.add(getFieldTB("Bonus (Flat)", dbFlatBonus));
//		horizontalPanel1.add(getFieldTB("Travelling (Flat)", dbFlatTravelling));
//		horizontalPanel1.add(getFieldTB("Overhead Cost (Flat)", dbFlatOverheadCost));
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CNC", "CncEarningColumn")){
			hideColumn=true;
		}
		if(hideColumn){
			horizontalPanel1.add(getFieldTB("Supervision %", tbSupervision));
			horizontalPanel1.add(getFieldTB("Training Charges %", tbTrainingCharges));
		}
		panel1.add(horizontalPanel1);
		
		VerticalPanel panel3 = new VerticalPanel();
		HorizontalPanel horizontalPanel2 = new HorizontalPanel();
		horizontalPanel2.getElement().getStyle().setWidth(100, Unit.PCT);
		horizontalPanel2.add(getFieldTB("Monthly Finance plan",dbMonthlyFinPlan));
		horizontalPanel2.add(getFieldTB("Interest On Finance(%)",dbInterestOnFinAmt));
		horizontalPanel2.add(getFieldTB("Estimated Repairs(%)",dbEstimatedRepair));
		horizontalPanel2.add(getFieldTB("Admin OH Cost",dbAdminOhCost));
		horizontalPanel2.add(getFieldTB("Approx Uniform Cost",dbApproxUniformCost));
		panel3.add(horizontalPanel2);
		
		VerticalPanel panel4 = new VerticalPanel();
		HorizontalPanel horizontalPanel3 = new HorizontalPanel();
		horizontalPanel3.getElement().getStyle().setWidth(100, Unit.PCT);
//		horizontalPanel3.add(getFieldTB("Approx Uniform Cost",dbApproxUniformCost));
		panel4.add(horizontalPanel3);
		
		
		
		panel2.add(hpGroupingPanel1);
		panel2.add(panel);
		panel2.add(panel1);
		panel2.add(hpGroupingPanel2);
		panel2.add(panel3);
		panel2.add(panel4);
		return panel2;
	}

	private VerticalPanel getFormPanelForEquiment() {
		// TODO Auto-generated method stub

		VerticalPanel verticalPnl = new VerticalPanel();

		HorizontalPanel horizontalPanel = new HorizontalPanel();
		horizontalPanel.getElement().getStyle().setWidth(100, Unit.PCT);
		horizontalPanel.add(bt_addEquipmentDetails);
		horizontalPanel.add(getFieldTB(" ",bt_addEquipmentDetails));
		
		// verticalPnl.add(horizontalPanel);
		//
		// horizontalPanel = new HorizontalPanel();
		// horizontalPanel.getElement().getStyle().setWidth(100, Unit.PCT);
		// horizontalPanel.add(getFieldTB("No of months for Rental",
		// tbNoOFMonthsForRental));
		horizontalPanel.add(getFieldTB("Interest On Machinary(%)",tbInterestOfMachinary));
		horizontalPanel.add(getFieldTB("Maintainance %",tbMaintaniancePercentage));
		
		verticalPnl.add(horizontalPanel);

		return verticalPnl;
	}

	private Widget getFieldTB(String labelName,Widget widget) {// TextBox textbox
		// TODO Auto-generated method stub
		if(widget instanceof TextBox){
			widget = (TextBox) widget;
			widget.getElement().getStyle().setWidth(150, Unit.PX);
		}else if(widget instanceof DoubleBox){
			widget = (DoubleBox) widget;
			widget.getElement().getStyle().setHeight(25, Unit.PX);
			widget.getElement().getStyle().setWidth(160, Unit.PX);
		}else if(widget instanceof Button){
			widget = (Button) widget;
		}
		VerticalPanel verticalPanel = new VerticalPanel();
		verticalPanel.getElement().getStyle().setMargin(2.0, Unit.PCT);
		Label label = new Label(labelName);
		// label.getElement().getStyle().setMarginBottom(5.0,Unit.PX);
		// label.getElement().getStyle().setMarginRight(5.0,Unit.PX);

		verticalPanel.add(label);
		widget.getElement().getStyle().setMarginRight(5.0, Unit.PX);
		//widget.getElement().getStyle().setWidth(150, Unit.PX);
		verticalPanel.add(widget);

		return verticalPanel;
	}

	@Override
	public void createScreen() {
		
		payrollCycleFlag=AppUtility.checkForProcessConfigurartionIsActiveOrNot("CNC", "PayrollDayForMultiplePayrollCycle");
		
		addStateFlag=AppUtility.checkForProcessConfigurartionIsActiveOrNot("CNC", "AddStatefield");
		addPaidLeaveFlag=AppUtility.checkForProcessConfigurartionIsActiveOrNot("CNC", "HidePaidLeaveFieldAndAddDropdown");
		
		addGstFieldFlag=AppUtility.checkForProcessConfigurartionIsActiveOrNot("CNC", "AddGSTNoFieldOnCNC");
		
		System.out.println("Overridden methods are createScreen called");
		initalizeWidget();
		/**
		 * Date : 08-06-2018 By ANIL Added Raise Bill Button for generating Bill
		 */
		 /** Added By Priyanka , Des : Add salesOrder from CNC Requirment by Nitin Sir **/
		this.processlevelBarNames = new String[] {
				ManageApprovals.APPROVALREQUEST,
				ManageApprovals.CANCELAPPROVALREQUEST,
				AppConstants.CANCLECONTRACT, "View Project", AppConstants.NEW,
				ManageApprovals.SUBMIT, 
				"Amend Contract"/* ,AppConstants.UPLOADTERMSANDCONDITION */,
				ManageApprovals.APPROVALSTATUS, "Mark Confirmed",
				AppConstants.COSTSHHETDOWNLOAD ,AppConstants.SEARCHVERSION,AppConstants.VIEWBILL, "Raise Bill",AppConstants.PAYROLLSHEETDOWNLOAD,AppConstants.GROSSPROFITSHEETDOWNLOAD,AppConstants.CUSTOMERADDRESS,AppConstants.CREATESALESORDER};//,AppConstants.ARREARSBILL
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingCustomerInformation = fbuilder
				.setlabel("Customer Information")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(4).build();
		fbuilder = new FormFieldBuilder("", personInfoComposite);
		FormField fpersonInfoComposite = fbuilder.setMandatory(false)
				.setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Document Id", tbContractId);
		FormField ftbContractId = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Start Date", dbContractStartDate);
		FormField fdbContractStartDate = fbuilder.setMandatory(true)
				.setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* End Date", dbContractEndDate);
		FormField fdbContractEndDate = fbuilder.setMandatory(true)
				.setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Status", tbStatus);
		FormField ftbStatus = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Contract Period", olbContractGroup);
		FormField folbContractGroup = fbuilder.setMandatory(false)
				.setRowSpan(0).setColSpan(0).setVisible(!addStateFlag).build();
		fbuilder = new FormFieldBuilder("Contract Category",
				olbContractCategory);
		FormField folbContractCategory = fbuilder.setMandatory(false)
				.setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Contract Type", olbContractType);
		FormField folbContractType = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Branch", olbbBranch);
		FormField folbbBranch = fbuilder.setMandatory(true).setRowSpan(0)
				.setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Sales Person", olbeSalesPerson);
		FormField folbeSalesPerson = fbuilder.setMandatory(true).setRowSpan(0)
				.setColSpan(0).build();
//		fbuilder = new FormFieldBuilder("* Project Name", olbProject);
//		FormField folbProject = fbuilder.setMandatory(true).setRowSpan(0)
//				.setColSpan(0).build();
		/** date 14.8.2018 changed by komal**/
		fbuilder = new FormFieldBuilder("* Project Name", tbProjectName);
		FormField folbProject = fbuilder.setMandatory(true).setRowSpan(0)
				.setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Reference Number", tbRefNum);
		FormField ftbRefNum = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Payment Method", olbcPaymentMethods);
		FormField folbcPaymentMethods = fbuilder.setMandatory(false)
				.setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Payment Terms", olbPaymentTerms);
		FormField folbPaymentTerms = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Credit Period", tbCreditPeriod);
		FormField ftbCreditPeriod = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Approver Name", olbApproverName);
		FormField folbApproverName = fbuilder.setMandatory(true).setRowSpan(0)
				.setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Approved Remark", tbApprovedRemark);
		FormField ftbApprovedRemark = fbuilder.setMandatory(false)
				.setRowSpan(0).setColSpan(0).build();
		FormField fgroupingContractInfo = fbuilder
				.setlabel("Contract Information")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(4).build();
		fbuilder = new FormFieldBuilder(" ", tabPanel);
		FormField ftabPanel = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(4).build();
		//fbuilder = new FormFieldBuilder("Version", lstbVersion);
		fbuilder = new FormFieldBuilder("Version", dbVersionNumber);/** date 21.8.2018 added by komal**/
		FormField flstbVersion = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder();
		FormField fblankgroup = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(2).build();
		fbuilder = new FormFieldBuilder();
		FormField fblankgroupOne = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(1).build();

		fbuilder = new FormFieldBuilder("Net Payable", donetpayamt);
		FormField fpayCompNetPay = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("Contract Number", tbContractNumber);
		FormField ftbContractNumber = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(1).build();
		
		
		fbuilder = new FormFieldBuilder("Cancellation Remark", tbCancellationRemark);
		FormField ftbCancellationRemark = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Customer Branch",oblCustomerBranch);
		FormField flbCustomerBranch = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Payroll (Start Day)",ibPayrollStartDay);
		FormField fibPayrollStartDay = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).setVisible(payrollCycleFlag).build();

		fbuilder = new FormFieldBuilder("* State",oblState);
		FormField foblState = fbuilder.setMandatory(addStateFlag).setMandatoryMsg("State is mandatory.").setRowSpan(0).setColSpan(0).setVisible(addStateFlag).build();
		
//		fbuilder = new FormFieldBuilder("* Working Hours",dbWorkingHours);
//		FormField fdbWorkingHours = fbuilder.setMandatory(siteLocation).setRowSpan(0).setColSpan(0).setVisible(siteLocation).build();
		
		fbuilder = new FormFieldBuilder("GSTIN Number",tbCustomerGstNumber);
		FormField ftbCustomerGstNumber = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).setVisible(addGstFieldFlag).build();

		
		FormField[][] formfield = {
				{ fgroupingCustomerInformation },
				{ fpersonInfoComposite },
				{ flbCustomerBranch},
				{ fgroupingContractInfo },
				{ ftbContractId, fdbContractStartDate, fdbContractEndDate,ftbStatus },
				{ folbContractGroup,foblState, folbContractCategory, folbContractType,folbbBranch },
				{ folbeSalesPerson, ftbRefNum, folbcPaymentMethods,folbPaymentTerms },
				{ ftbCreditPeriod, folbApproverName, ftbApprovedRemark,flstbVersion }, 
				{ folbProject ,ftbContractNumber,fibPayrollStartDay ,ftbCustomerGstNumber},
				{ ftbCancellationRemark}, 
				{ ftabPanel },
				{ fblankgroup, fblankgroupOne, fpayCompNetPay }
		};
		this.fields = formfield;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void updateModel(CNC model) {
		/**
		 * For updating only
		 */
		if (isAmended) {
//			int versionNumber = model.getVersionMap().size();
//			if (versionNumber != 0)
//				model.getVersionMap().put(versionNumber + 1, model);
			/*** date 21.8.2018 added by komal for version number**/
			if(dbVersionNumber.getValue()!=null){
				model.setVersionNumber(dbVersionNumber.getValue());
				model.setIslatestVersion(true);
			}else{
				model.setVersionNumber(0.0);
				model.setIslatestVersion(true);
			}
				
			Console.log("version number :" + model.getVersionNumber());
		}
		System.out.println("Update Model Got Called");
		if (personInfoComposite.getValue() != null)
			model.setPersonInfo(personInfoComposite.getValue());

		if (dbContractStartDate != null)
			model.setContractStartDate(dbContractStartDate.getValue());

		if (dbContractEndDate != null)
			model.setContractEndDate(dbContractEndDate.getValue());

		if (olbContractGroup.getSelectedIndex() != 0)
			model.setContractGroup(olbContractGroup.getValue(olbContractGroup
					.getSelectedIndex()));

		if (olbContractCategory.getSelectedIndex() != 0)
			model.setContractCategory(olbContractCategory
					.getValue(olbContractCategory.getSelectedIndex()));

		if (olbContractType.getSelectedIndex() != 0)
			model.setContractType(olbContractType.getValue(olbContractType
					.getSelectedIndex()));

		if (olbbBranch.getSelectedIndex() != 0)
			model.setBranch(olbbBranch.getValue(olbbBranch.getSelectedIndex()));

		if (olbeSalesPerson.getSelectedIndex() != 0)
			model.setSalesPerson(olbeSalesPerson.getValue(olbeSalesPerson
					.getSelectedIndex()));

		if (tbRefNum.getValue() != null)
			model.setRefNum(tbRefNum.getValue());

		if (olbcPaymentMethods.getSelectedIndex() != 0)
			model.setPaymentMethod(olbcPaymentMethods
					.getValue(olbcPaymentMethods.getSelectedIndex()));

		if (olbPaymentTerms.getSelectedIndex() != 0)
			model.setPaymentTerms(olbPaymentTerms.getValue(olbPaymentTerms
					.getSelectedIndex()));

		if (tbCreditPeriod.getValue() != null) {
			try {
				model.setCreditPeriod(Integer.parseInt(tbCreditPeriod
						.getValue().trim()));
			} catch (Exception e) {
				model.setCreditPeriod(0);
			}
		}

		if (olbApproverName.getSelectedIndex() != 0) {
			model.setApproverName(olbApproverName.getValue(olbApproverName
					.getSelectedIndex()));
		}
		/******2-1-2019 added by amol*******/
		if(tbCancellationRemark.getValue()!=null){
			model.setCancellationRemark(tbCancellationRemark.getValue());
			}
		
		
		
		
		if (tbApprovedRemark.getValue() != null) {
			model.setRemark(tbApprovedRemark.getValue());
		}
		// VersionDetails versionDetails=new VersionDetails();
		if (staffingTable.getValue().size() != 0) {
			model.setSaffingDetailsList(staffingTable.getValue());
		}
		if (equipmentRentalTable.getValue().size() != 0) {
			model.setEquipmentRentalList(equipmentRentalTable.getValue());
		}
		if (consumableTable.getValue().size() != 0) {
			model.setConsumablesList(consumableTable.getValue());
		}
		if (otherServiceTable.getValue().size() != 0) {
			model.setOtherServicesList(otherServiceTable.getValue());
		}
		// Here we will update the list instead of adding
		if (model.getVersionMap() == null) {
			HashMap<Integer, CNC> versionMap = new HashMap<Integer, CNC>();
			versionMap.put(1, model);
			model.setVersionMap(versionMap);
		}
		if (tbStatus.getValue() != null)
			model.setStatus(tbStatus.getValue().trim());
		// if (tbNoOFMonthsForRental.getValue() != null) {
		// try {
		// model.setNoOfMonthsForRental(Double
		// .parseDouble(tbNoOFMonthsForRental.getValue().trim()));
		// } catch (Exception e) {
		// model.setNoOfMonthsForRental(0);
		// }
		// }
		if (tbInterestOfMachinary.getValue() != null) {
			try {
				model.setInterestOnMachinary(Double.parseDouble(tbInterestOfMachinary.getValue().trim()));
			} catch (Exception e) {
				model.setInterestOnMachinary(0);
			}
		}
		if (tbMaintaniancePercentage.getValue() != null) {
			try {
				model.setMaintainanceCharges(Double.parseDouble(tbMaintaniancePercentage.getValue().trim()));
			} catch (Exception e) {
				model.setMaintainanceCharges(0);
			}
		}

		// if (tbOverHeadCostPerEmployee.getValue() != null) {
		// try {
		// model.setOverheadCostPerEmployee(Double
		// .parseDouble(tbOverHeadCostPerEmployee.getValue()
		// .trim()));
		// } catch (Exception e) {
		// model.setOverheadCostPerEmployee(0);
		// }
		// }
		if (tbManagementFeesPercentage.getValue() != null) {
			try {
				model.setManagementFees(Double.parseDouble(tbManagementFeesPercentage.getValue().trim()));
			} catch (Exception e) {
				model.setManagementFees(0);
			}
		}

//		if (olbProject.getSelectedIndex() != 0) {
//			model.setProjectName(olbProject.getValue(olbProject
//					.getSelectedIndex()));
//		}
		/** date 14.8.2018 added by komal**/
		if(tbProjectName.getValue()!=null){
			model.setProjectName(tbProjectName.getValue());
		}
		if (tbcBonus.getValue() != null) {
			model.setcBonusPercentage(tbcBonus.getValue().trim());
		}

		if (cb_Relever_for_Bonus.getValue() != null) {
			model.setBonus_relever(cb_Relever_for_Bonus.getValue());
		}

		if (cb_Relever_for_PaidLeaves.getValue() != null) {
			model.setPaidLeave_Relever(cb_Relever_for_PaidLeaves.getValue());
		}

		// if(cb_CLeave.getValue()!=null){
		model.setCLeaveApplicable(cb_CLeave.getValue());
		model.setCBonusApplicable(cb_Cbonus.getValue());
		model.setGratuityApplicable(cb_Gratuity.getValue());

		// }

		if (tbNetPayableOFStaffing.getValue() != null) {
			try {
				model.setNetPayableOFStaffing(Double
						.parseDouble(tbNetPayableOFStaffing.getValue().trim()));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		if (tbGrossTotalOfStaffing.getValue() != null) {
			try {
				model.setGrossTotalOfStaffing(Double
						.parseDouble(tbGrossTotalOfStaffing.getValue().trim()));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		if (tbManagementFeesValue.getValue() != null) {
			try {
				model.setManagementFeesValue(Double
						.parseDouble(tbManagementFeesValue.getValue().trim()));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		if (tbNetPayableOFEquipment.getValue() != null) {
			try {
				model.setNetPayableOFEquipment(Double
						.parseDouble(tbNetPayableOFEquipment.getValue().trim()));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		if (tbNetPayableOFConsumabables.getValue() != null) {
			try {
				model.setNetPayableOFConsumables(Double
						.parseDouble(tbNetPayableOFConsumabables.getValue()
								.trim()));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		if (tbNetPayableOFOther.getValue() != null) {
			try {
				model.setNetPayableOFOther(Double
						.parseDouble(tbNetPayableOFOther.getValue().trim()));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if (donetpayamt.getValue() != 0) {
			model.setNetPayableOFCNC(donetpayamt.getValue());
		}
		/** date 10.8.2018 added by komal for terms and conditions save**/
		if(taTermsandConditions.getValue()!=null){
			model.setTermsAndConditions(taTermsandConditions.getValue());
		}
		/**
		 * Rahul Verma added on 08 Aug 2018
		 */
		if(tbPaidLeavesDays.getValue()!=null){
			model.setPaidLeavesDays(tbPaidLeavesDays.getValue());
		}
		/** date 13.8.2018 added by komal for % and flat values**/
		if(dbFlatManagementFees.getValue()!=null){
			model.setFlatManagementFees(dbFlatManagementFees.getValue());
		}
		if(dbBonusPercentage.getValue()!=null){
			model.setBonusPercent(dbFlatBonus.getValue());
		}
		if(dbFlatBonus.getValue()!=null){
			model.setFlatBonus(dbFlatBonus.getValue());
		}
//		if(dbTravellingPercentage.getValue()!=null){
//			model.setTravellingPercent(dbTravellingPercentage.getValue());
//		}
		if(dbFlatTravelling.getValue()!=null){
			model.setFlatTravelling(dbFlatTravelling.getValue());
		}
//		if(dbOverheadCostPercentage.getValue()!=null){
//			model.setOverheadCostPercent(dbOverheadCostPercentage.getValue());
//		}
		if(dbFlatOverheadCost.getValue()!=null){
			model.setFlatOverheadCost(dbFlatOverheadCost.getValue());
		}
		
		if(tbSupervision.getValue()!=null){
			model.setSupervision(Double.parseDouble(tbSupervision.getValue()));
		}
		
		if(tbTrainingCharges.getValue()!=null){
			model.setTrainingCharges(Double.parseDouble(tbTrainingCharges.getValue()));
		}
		
		
		
		/**
		 * end komal
		 */
//		/*** date 21.8.2018 added by komal for version number**/
//		if(dbVersionNumber.getValue()!=null){
//			NumberFormat nf = NumberFormat.getFormat("0.0");
//			double ver = dbVersionNumber.getValue();
//			double value = Math.floor(ver);
//			String diff = nf.format(ver - value);
//			System.out.println("version :" + diff);
//			String myVer;
//			if(nf.format(ver - value).equals(0.9+"")){
//				NumberFormat nf1 = NumberFormat.getFormat("0.00");
//				ver = ver + 0.01;
//				myVer = nf1.format(ver);
//			}else{
//				ver = ver + 0.1;
//				myVer = nf.format(ver);
//			}
//			model.setVersionNumber(Double.parseDouble(myVer));
//			model.setIslatestVersion(true);
//		}else{
//			model.setVersionNumber(0.0);
//			model.setIslatestVersion(true);
//
//		}
//		Console.log("version number :" + model.getVersionNumber());
		if(tbContractNumber.getValue() != null){
			model.setContractNumber(tbContractNumber.getValue());
		}
			/** date 21.01.2019 added by komal to show discount amount and total amount **/
		if(dbTotalWithoutDiscount.getValue() != null){
			model.setTotalEquipmentAmount(dbTotalWithoutDiscount.getValue());
		}
		if(dbDiscount.getValue() != null){
			model.setEquipmentDiscountAmount(dbDiscount.getValue());
		}
		
		if(dbMonthlyFinPlan.getValue()!=null){
			model.setMonthlyFinancePlan(dbMonthlyFinPlan.getValue());
		}else{
			model.setMonthlyFinancePlan(0);
		}
		if(dbInterestOnFinAmt.getValue()!=null){
			model.setInterestOnFinAmt(dbInterestOnFinAmt.getValue());
		}else{
			model.setInterestOnFinAmt(0);
		}
		
		if(dbEstimatedRepair.getValue()!=null){
			model.setEstimatedRepair(dbEstimatedRepair.getValue());
		}else{
			model.setEstimatedRepair(0);
		}
		
		if(dbAdminOhCost.getValue()!=null){
			model.setAdminOverheadCost(dbAdminOhCost.getValue());
		}else{
			model.setAdminOverheadCost(0);
		}
		
		if(dbApproxUniformCost.getValue()!=null){
			model.setApproxUniformCost(dbApproxUniformCost.getValue());
		}else{
			model.setApproxUniformCost(0);
		}
		if(oblCustomerBranch.getValue()!=null){
			model.setCustomerBranch(oblCustomerBranch.getValue());
		}else{
			model.setCustomerBranch("");
		}
		if(billingAddress!=null){
			model.setBillingAddress(billingAddress);
		}else{
			model.setBillingAddress(null);
		}
		if(serviceAddress!=null){
			model.setServiceAddress(serviceAddress);
		}else{
			model.setServiceAddress(null);
		}
		
		if(ibPayrollStartDay.getValue()!=null){
			model.setPayrollStartDay(ibPayrollStartDay.getValue());
		}else{
			model.setPayrollStartDay(null);
		}
		
		model.setState(oblState.getValue());
		model.setPaidLeaveName(oblPaidLeave.getValue());
		
//		if(dbWorkingHours.getValue()!=null){
//			model.setWorkingHours(dbWorkingHours.getValue());
//		}else{
//			model.setWorkingHours(0);
//		}
		
		if(otRateTbl.getValue()!=null){
			model.setOtRateDetailsList(otRateTbl.getValue());
		}
		
		if(tbCustomerGstNumber.getValue()!=null){
			model.setCustomerGstNumber(tbCustomerGstNumber.getValue());
		}
		
		cncObject = model;
		presenter.setModel(model);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void updateView(CNC view) {
		Console.log("CNC LOG 4A");
//		clearTabFormFields();
		cncObject = view;
		/**
		 * Date : 17-12-2018 BY ANIL
		 * This code clears all the from all table
		 */
//		staffingTable.getDataprovider().getList().clear();
//		equipmentRentalTable.getDataprovider().getList().clear();
//		otherServiceTable.getDataprovider().getList().clear();
//		consumableTable.getDataprovider().getList().clear();
		/**
		 * End
		 */
		
		if (view.getPersonInfo() != null)
			personInfoComposite.setValue(view.getPersonInfo());

		tbContractId.setValue(view.getCount() + "");
		if (view.getContractStartDate() != null)
			dbContractStartDate.setValue(view.getContractStartDate());

		if (view.getContractEndDate() != null)
			dbContractEndDate.setValue(view.getContractEndDate());

		if (view.getStatus() != null)
			tbStatus.setValue(view.getStatus());

		if (view.getContractGroup() != null)
			olbContractGroup.setValue(view.getContractGroup());

		if (view.getContractCategory() != null)
			olbContractCategory.setValue(view.getContractCategory());

		if (view.getContractType() != null)
			olbContractType.setValue(view.getContractType());

		if (view.getBranch() != null)
			olbbBranch.setValue(view.getBranch());

		if (view.getSalesPerson() != null)
			olbeSalesPerson.setValue(view.getSalesPerson());

		if (view.getRefNum() != null)
			tbRefNum.setValue(view.getRefNum());

		if (view.getPaymentMethod() != null)
			olbcPaymentMethods.setValue(view.getPaymentMethod());

		if (view.getPaymentTerms() != null)
			olbPaymentTerms.setValue(view.getPaymentTerms());

		if (view.getCreditPeriod() != 0)
			tbCreditPeriod.setValue(view.getCreditPeriod() + "");

		if (view.getApproverName() != null)
			olbApproverName.setValue(view.getApproverName());

//		if (view.getProjectName() != null)
//			olbProject.setValue(view.getProjectName());
		/** date 14.8.2018 added by komal**/
		if (view.getProjectName() != null)
			tbProjectName.setValue(view.getProjectName());
		
		if (view.getRemark() != null)
			tbApprovedRemark.setValue(view.getRemark());
		
		if (view.getCancellationRemark()!=null)
		 tbCancellationRemark.setValue(view.getCancellationRemark());

		/**
		 * Date : 17-12-2018 BY ANIL
		 */
//		if (view.getSaffingDetailsList().size() != 0) {
//			staffingTable.setValue(view.getSaffingDetailsList());
//		}
//		if (view.getEquipmentRentalList().size() != 0) {
//			equipmentRentalTable.setValue(view.getEquipmentRentalList());
//		}
//		if (view.getConsumablesList().size() != 0) {
//			consumableTable.setValue(view.getConsumablesList());
//		}
//		if (view.getOtherServicesList().size() != 0) {
//			otherServiceTable.setValue(view.getOtherServicesList());
//		}
		Console.log("CNC LOG 4b");
		if(view.getPaidLeaveName()!=null){
			Console.log("CNC LOG 4bB "+view.getPaidLeaveName());
			oblPaidLeave.setValue(view.getPaidLeaveName());
			if(oblPaidLeave.getValue()!=null){
				Console.log("CNC LOG 4bc "+oblPaidLeave.getValue());
				staffingTable.setPaidleave(oblPaidLeave.getSelectedItem());
			}else{
				Console.log("CNC LOG 4bd ");
				staffingTable.setPaidleave(null);
			}
		}else{
			Console.log("CNC LOG 4be ");
			staffingTable.setPaidleave(null);
		}
		Console.log("CNC LOG 4c");
		
		if (view.getSaffingDetailsList()!=null) {
			staffingTable.setValue(view.getSaffingDetailsList());
		}
		Console.log("CNC LOG 4d");
		if (view.getEquipmentRentalList()!=null) {
			equipmentRentalTable.setValue(view.getEquipmentRentalList());
		}
		if (view.getConsumablesList()!=null) {
			consumableTable.setValue(view.getConsumablesList());
		}
		if (view.getOtherServicesList()!=null) {
			otherServiceTable.setValue(view.getOtherServicesList());
		}
		/**
		 * End
		 */
		
		
		// if (view.getNoOfMonthsForRental() != 0) {
		// tbNoOFMonthsForRental.setValue(view.getNoOfMonthsForRental() + "");
		// }
		if (view.getMaintainanceCharges() != 0) {
			tbMaintaniancePercentage.setValue(view.getMaintainanceCharges()+ "");
		}
		if (view.getManagementFees() != 0) {
			tbManagementFeesPercentage.setValue(view.getManagementFees() + "");
		}
		// if (view.getOverheadCostPerEmployee() != 0) {
		// tbOverHeadCostPerEmployee.setValue(view
		// .getOverheadCostPerEmployee() + "");
		// }
		if (view.getInterestOnMachinary() != 0) {
			tbInterestOfMachinary.setValue(view.getInterestOnMachinary() + "");
		}
		lstbVersion = new ListBox();
		lstbVersion.addItem("--SELECT--");
		if (view.getVersionMap() != null) {
			ArrayList<Integer> keySetList = null;
			if (view.getVersionMap().size() != 0) {
				keySetList = new ArrayList<Integer>(view.getVersionMap()
						.keySet());
				for (int i = 0; i < keySetList.size(); i++) {
					lstbVersion.addItem(keySetList.get(i) + "");
				}
			}
			lstbVersion.setItemSelected(keySetList.size() - 1, true);
		}
		if (view.getcBonusPercentage() != null) {
			tbcBonus.setValue(view.getcBonusPercentage());
		}

		if (view.getNetPayableOFStaffing() != 0) {
			tbNetPayableOFStaffing
					.setValue(view.getNetPayableOFStaffing() + "");
		}

		if (view.getGrossTotalOfStaffing() != 0) {
			tbGrossTotalOfStaffing
					.setValue(view.getGrossTotalOfStaffing() + "");
		}

		if (view.getManagementFeesValue() != 0) {
			tbManagementFeesValue.setValue(view.getManagementFeesValue() + "");
		}

		if (view.getNetPayableOFConsumables() != 0) {
			tbNetPayableOFConsumabables.setValue(view
					.getNetPayableOFConsumables() + "");
		}

		if (view.getNetPayableOFEquipment() != 0) {
			tbNetPayableOFEquipment.setValue(view.getNetPayableOFEquipment()
					+ "");
		}

		if (view.getNetPayableOFOther() != 0) {
			tbNetPayableOFOther.setValue(view.getNetPayableOFOther() + "");
		}

		if (view.getNetPayableOFCNC() != 0) {
			donetpayamt.setValue(view.getNetPayableOFCNC());
		}
		cb_Relever_for_Bonus.setValue(view.isBonus_relever());
		cb_Relever_for_PaidLeaves.setValue(view.isPaidLeave_Relever());
		cb_CLeave.setValue(view.isCLeaveApplicable());
		cb_Cbonus.setValue(view.isCBonusApplicable());
		cb_Gratuity.setValue(view.isGratuityApplicable());
		
		/** date 10.8.2018 added by komal for terms and conditions save**/
		taTermsandConditions.setValue(view.getTermsAndConditions());
		
		if(view.getPaidLeavesDays()!=null){
			tbPaidLeavesDays.setValue(view.getPaidLeavesDays());
		}
		/** date 13.8.2018 added by komal for % and flat values**/
		dbFlatManagementFees.setValue(view.getFlatManagementFees());
		dbBonusPercentage.setValue(view.getBonusPercent());
		dbFlatBonus.setValue(view.getFlatBonus());
	//	dbTravellingPercentage.setValue(view.getTravellingPercent());
		dbFlatTravelling.setValue(view.getFlatTravelling());
	//	dbOverheadCostPercentage.setValue(view.getOverheadCostPercent());
		dbFlatOverheadCost.setValue(view.getFlatOverheadCost());
		
		tbSupervision.setValue(view.getSupervision()+"");
		tbTrainingCharges.setValue(view.getTrainingCharges()+"");
		
		/**
		 * end komal
		 */
		/** date 21.8.2018 added by komal */
		Console.log("version number in view:" + view.getVersionNumber());
		dbVersionNumber.setValue(view.getVersionNumber());
		if(view.getContractNumber() != null){
			tbContractNumber.setValue(view.getContractNumber());
		}
		/** date 21.01.2019 added by komal to show discount amount and total amount **/
		dbTotalWithoutDiscount.setValue(view.getTotalEquipmentAmount());
		dbDiscount.setValue(view.getEquipmentDiscountAmount());
		/****2-2-2019 added by amol*****/
		tbCancellationRemark.setValue(view.getDescription());
		
		dbMonthlyFinPlan.setValue(view.getMonthlyFinancePlan());
		dbInterestOnFinAmt.setValue(view.getInterestOnFinAmt());
		dbEstimatedRepair.setValue(view.getEstimatedRepair());
		dbAdminOhCost.setValue(view.getAdminOverheadCost());
		dbApproxUniformCost.setValue(view.getApproxUniformCost());
		
		loadCustomerBranch(view.getPersonInfo().getCount(), view.getCustomerBranch(),true);
//		if(view.getCustomerBranch()!=null&&!view.getCustomerBranch().equals("")&&view.getPersonInfo()!=null&&view.getPersonInfo().getCount()!=0){
//			oblCustomerBranch.setValue(view.getCustomerBranch());
//		}
		if(view.getBillingAddress()!=null){
			billingAddress=view.getBillingAddress();
		}
		if(view.getServiceAddress()!=null){
			serviceAddress=view.getServiceAddress();
		}
		
		if(view.getPayrollStartDay()!=null){
			ibPayrollStartDay.setValue(view.getPayrollStartDay());
		}
		
		if(view.getState()!=null){
			oblState.setValue(view.getState());
		}
		
//		dbWorkingHours.setValue(view.getWorkingHours());
		
		if(view.getOtRateDetailsList()!=null){
			otRateTbl.setValue(view.getOtRateDetailsList());
		}
		
		if(siteLocation){
			updateDesignationDropDownList();
		}
		
		if(view.getCustomerGstNumber()!=null){
			tbCustomerGstNumber.setValue(view.getCustomerGstNumber());
		}
		
		if (presenter != null) {
			presenter.setModel(view);
		}
	}

	public void loadCustomerBranch(int custId,final String custBranch,final boolean consumableFlag) {
		customerBranchList=new ArrayList<CustomerBranchDetails>();
		MyQuerry querry = new MyQuerry();
		Company c = new Company();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("cinfo.count");
		filter.setIntValue(custId);
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new CustomerBranchDetails());

		async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				oblCustomerBranch.clear();
				oblCustomerBranch.addItem("--SELECT--");
				showDialogMessage("failed." + caught);
			}
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				ArrayList<CustomerBranchDetails> custBranchList=new ArrayList<CustomerBranchDetails>();
				if(result.size()!=0){
					for(SuperModel model:result){
						CustomerBranchDetails obj=(CustomerBranchDetails) model;
						custBranchList.add(obj);
						customerBranchList.add(obj);
					}
					Console.log("custBranchList Size11 "+custBranchList.size());
					Console.log("customerBranchList Size222 "+customerBranchList.size());
				}
				
				if(custBranchList.size()!=0){
					oblCustomerBranch.setListItems(custBranchList);
					if(custBranch!=null){
						oblCustomerBranch.setValue(custBranch);
					}else{
						oblCustomerBranch.setEnabled(true);
					}
				}else{
					oblCustomerBranch.addItem("--SELECT--");
				}
				
				if(consumableFlag){
					if(siteLocation&&oblCustomerBranch.getItems()!=null&&oblCustomerBranch.getItems().size()!=0){
						if(consumableTable.getValue()!=null&&consumableTable.getValue().size()!=0){
							consumableTable.siteLocationList=getSiteLocationList();
//							consumableTable.setEnable(true);
						}
					}
				}
			}
		});
	}


	
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		if (event.getSource().equals(cb_Relever_for_Bonus)) {
			if (cb_Relever_for_Bonus.getValue().equals(true)) {
				int count = 0;
				for (StaffingDetails staffingDetails : this.staffingTable.getDataprovider().getList()) {
					staffingDetails.setBonusRelever(true);
					this.staffingTable.updateForBonusRelever(staffingDetails,count);
					count += 1;
				}
			} else {
				int count = 0;
				for (StaffingDetails staffingDetails : this.staffingTable.getDataprovider().getList()) {
					staffingDetails.setBonusRelever(false);
					this.staffingTable.updateForBonusRelever(staffingDetails,count);
					count += 1;
				}
			}

			if (this.staffingTable != null) {
				this.staffingTable.getTable().redraw();
			}
		}

		if (event.getSource().equals(cb_Relever_for_PaidLeaves)) {
			if (cb_Relever_for_PaidLeaves.getValue().equals(true)) {
				int count = 0;
				for (StaffingDetails staffingDetails : this.staffingTable.getDataprovider().getList()) {
					staffingDetails.setPaidLeaveRelever(true);
					this.staffingTable.updateForPaidLeaveRelever(staffingDetails, count);
					count += 1;
				}
			} else {
				int count = 0;
				for (StaffingDetails staffingDetails : this.staffingTable.getDataprovider().getList()) {
					staffingDetails.setPaidLeaveRelever(false);
					this.staffingTable.updateForPaidLeaveRelever(staffingDetails, count);
					count += 1;
				}
			}

			if (this.staffingTable != null) {
				this.staffingTable.getTable().redraw();
			}
		}

		if (event.getSource().equals(cb_CLeave)) {
			if (cb_CLeave.getValue().equals(true)) {
				int count = 0;
				for (StaffingDetails staffingDetails : this.staffingTable
						.getDataprovider().getList()) {
					staffingDetails.setcLeaveApplicable(true);
					this.staffingTable.updateCLeaveApplicable(staffingDetails,
							count);
					count += 1;
				}
			} else {
				int count = 0;
				for (StaffingDetails staffingDetails : this.staffingTable
						.getDataprovider().getList()) {
					staffingDetails.setcLeaveApplicable(false);
					this.staffingTable.updateCLeaveApplicable(staffingDetails,
							count);
					count += 1;
				}
			}

			if (this.staffingTable != null) {
				this.staffingTable.getTable().redraw();
			}
		}

		if (event.getSource().equals(cb_Cbonus)) {
			if (cb_Cbonus.getValue().equals(true)) {
				int count = 0;
				for (StaffingDetails staffingDetails : this.staffingTable
						.getDataprovider().getList()) {
					staffingDetails.setcBonusApplicable(true);
					this.staffingTable.updateCBonusApplicable(staffingDetails,
							count);
					count += 1;
				}
			} else {
				int count = 0;
				for (StaffingDetails staffingDetails : this.staffingTable
						.getDataprovider().getList()) {
					staffingDetails.setcBonusApplicable(false);
					this.staffingTable.updateCBonusApplicable(staffingDetails,
							count);
					count += 1;
				}
			}

			if (this.staffingTable != null) {
				this.staffingTable.getTable().redraw();
			}
		}
		/** date 11.8.2018 added by komal to add terms and conditions in textarea **/
//		if(event.getSource().equals(btAddTermsandConditions)){
//			if(obTermsandCondition.getSelectedIndex()!=0){
//				String value = "*  "+obTermsandCondition.getValue(obTermsandCondition.getSelectedIndex());
//					if(taTermsandConditions.getValue()!=null){
//						taTermsandConditions.setValue(taTermsandConditions.getValue()+"\n"+value);
//					}else{
//						taTermsandConditions.setValue(value);
//					}
//			}
//		}
		/** date 11.8.2018 added by komal to add terms and conditions in textarea **/
		if(event.getSource().equals(btAddTermsandConditions)){
			if(obTermsandCondition.getSelectedIndex()!=0){
				Declaration obj = obTermsandCondition.getSelectedItem();
				String value = "*  "+obj.getDeclaratiomMsg();
					if(taTermsandConditions.getValue()!=null && !taTermsandConditions.getValue().equals("")){
						taTermsandConditions.setValue(taTermsandConditions.getValue()+"\n"+value);
					}else{
						taTermsandConditions.setValue(value);
					}
			}
		}
		
//		if(event.getSource().equals(cbTypeDay)){
//			if(cbTypeDay.getValue()==true){
//				cbTypeHourly.setValue(false);
//			}
//		}
//		
//		if(event.getSource().equals(cbTypeHourly)){
//			if(cbTypeHourly.getValue()==true){
//				cbTypeDay.setValue(false);
//			}
//		}
		
		if(event.getSource().equals(btnAddOt)){
			
			if(lbOtName.getSelectedIndex()==0){
				showDialogMessage("Please select overtime!");
				return;
			}
			
			if(dbFlatRate.getValue()==null&&dbFactorRate.getValue()==null){
				showDialogMessage("Please select flat/factor rate!");
				return;
			}
			
//			if(cbTypeDay.getValue()==false&&cbTypeHourly.getValue()==false){
//				showDialogMessage("Please select day/hourly!");
//				return;
//			}
			
			if(olbDesignation1.getSelectedIndex()!=0){
				OtRateDetails ot = new OtRateDetails();
				ot.setDesignation(olbDesignation1.getValue());
				ot.setOtName(lbOtName.getValue(lbOtName.getSelectedIndex()));
				if(dbFlatRate.getValue()!=null){
					ot.setFlatRate(dbFlatRate.getValue());
				}
				if(dbFactorRate.getValue()!=null){
					ot.setFactorRate(dbFactorRate.getValue());
				}
//				if(cbTypeDay.getValue()==true){
//					ot.setType("Day");
//				}else if(cbTypeHourly.getValue()==true){
//					ot.setType("Hourly");
//				}
				
				for(OtRateDetails obj:otRateTbl.getValue()){
					if(ot.getDesignation().equals(obj.getDesignation())&&ot.getOtName().equals(obj.getOtName())){
						showDialogMessage(ot.getOtName()+" is already for designation "+ot.getDesignation());
						return;
					}
				}
				
				otRateTbl.getDataprovider().getList().add(ot);
				
				
			}else{
				for(Config designation:olbDesignation1.getItems()){
					
					OtRateDetails ot = new OtRateDetails();
					ot.setDesignation(designation.getName());
					ot.setOtName(lbOtName.getValue(lbOtName.getSelectedIndex()));
					if(dbFlatRate.getValue()!=null){
						ot.setFlatRate(dbFlatRate.getValue());
					}
					if(dbFactorRate.getValue()!=null){
						ot.setFactorRate(dbFactorRate.getValue());
					}
//					if(cbTypeDay.getValue()==true){
//						ot.setType("Day");
//					}else if(cbTypeHourly.getValue()==true){
//						ot.setType("Hourly");
//					}
					
					boolean addFlag=false;
					for(OtRateDetails obj:otRateTbl.getValue()){
						if(ot.getDesignation().equals(obj.getDesignation())&&ot.getOtName().equals(obj.getOtName())){
							addFlag=true;
							break;
						}
					}
					
					if(!addFlag){
						otRateTbl.getDataprovider().getList().add(ot);
					}
				}
				otRateTbl.getTable().redraw();
			}
		}
		
		if (event.getSource().equals(btnUploadConsumable)) {
			reactToUploadConsumable();
		}

	}

	@Override
	public void onChange(ChangeEvent event) {
		// TODO Auto-generated method stub
		System.out.println("IN SIDE ON CHANGE EVENT FORM....");
		
		
		
		if (event.getSource().equals(olbContractCategory)) {
			if (olbContractCategory.getSelectedIndex() != 0) {
				ConfigCategory cat = olbContractCategory.getSelectedItem();
				if (cat != null) {
					AppUtility.makeLiveTypeDropDown(olbContractType,
							cat.getCategoryName(), cat.getCategoryCode(),
							cat.getInternalType());
				}
			}
		}

		
		if(event.getSource().equals(tbSupervision)){
			if(tbSupervision.getValue()!=null&&!tbSupervision.getValue().equals("")){
			
			if (staffingTable != null) {

				for (int i = 0; i < this.staffingTable.getDataprovider()
						.getList().size(); i++) {
					StaffingDetails staffingDetails = this.staffingTable
							.getDataprovider().getList().get(i);
					
					staffingDetails.setSupervision((staffingDetails.getGrossSalary()*(Double.parseDouble(tbSupervision.getValue())))/(100));
					this.staffingTable.updateRow(staffingDetails, i,false);
				}
			}
		}
			}
		
		if(event.getSource().equals(tbTrainingCharges)){
			if(tbTrainingCharges.getValue()!=null&&!tbTrainingCharges.getValue().equals("")){
			
			if (staffingTable != null) {

				for (int i = 0; i < this.staffingTable.getDataprovider()
						.getList().size(); i++) {
					StaffingDetails staffingDetails = this.staffingTable
							.getDataprovider().getList().get(i);
					
					staffingDetails.setTrainingCharges((staffingDetails.getGrossSalary()*(Double.parseDouble(tbTrainingCharges.getValue())))/(100));
					this.staffingTable.updateRow(staffingDetails, i,false);
				}
			}
		}
			}
		
		
		
		
		
		
		
		if (event.getSource().equals(tbManagementFeesPercentage)) {
			/** date 13.8.2018 added by komal **/
			if(tbManagementFeesPercentage.getValue()!=null&&!tbManagementFeesPercentage.getValue().equals("")){
				double value = Double.parseDouble(tbManagementFeesPercentage.getValue().trim());
				Console.log("Value :" + value +" "+tbManagementFeesPercentage.getValue().trim());
				if(value!=0){
					dbFlatManagementFees.setValue(0.0);
				}
			}else{
				dbFlatManagementFees.setValue(0.0);
			}
			Console.log("Inside Management Fee change");
			double netPayableForStaffing = 0;
			double managementValue = 0;
			if (staffingTable != null) {

				for (int i = 0; i < this.staffingTable.getDataprovider()
						.getList().size(); i++) {
					StaffingDetails staffingDetails = this.staffingTable
							.getDataprovider().getList().get(i);
					netPayableForStaffing += staffingDetails
							.getTotalPaidIntoEmployee();
					staffingDetails.setManageMentfeesPercent(Double.parseDouble(tbManagementFeesPercentage.getValue().trim()));
					this.staffingTable.updateRow(staffingDetails, i,false);
				}
			}
			
//			this.tbGrossTotalOfStaffing.setValue(Math.round(netPayableForStaffing) + "");
//		//	double managementValue = 0;
//////			double managementFeePercent = Double
//////					.parseDouble(this.tbManagementFeesPercentage.getValue()
//////							.trim());
//////			double managementFeesValue = ((netPayableForStaffing / 100) * managementFeePercent);
//////			Console.log("managementFeesValue" + managementFeesValue + " "+managementFeePercent);
////			try {
////				managementValue = managementFeesValue;
////			} catch (Exception e) {
////				e.printStackTrace();
////				managementValue = 0;
////			}
////			this.tbManagementFeesValue.setValue(Math.round(managementValue)
////				+ "");
//			double netPayableOfStaffing = netPayableForStaffing
//					+ managementValue;
//			this.tbNetPayableOFStaffing.setValue(Math
//					.round(netPayableOfStaffing) + "");
//
//			double total1 = 0, total2 = 0, total3 = 0, total4 = 0;
//			try {
//				if(tbNetPayableOFStaffing.getValue()!=null&&!tbNetPayableOFStaffing.getValue().equals("")){
//				total1 = Double.parseDouble(this.tbNetPayableOFStaffing
//						.getValue().trim());
//				}
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//			try {
//				if(tbNetPayableOFEquipment.getValue()!=null&&!tbNetPayableOFEquipment.getValue().equals("")){
//				total2 = Double.parseDouble(this.tbNetPayableOFEquipment
//						.getValue().trim());
//				}
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//			try {
//				if(tbNetPayableOFConsumabables.getValue()!=null&&!tbNetPayableOFConsumabables.getValue().equals("")){
//				total3 = Double.parseDouble(this.tbNetPayableOFConsumabables
//						.getValue().trim());
//				}
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//			try {
//				if(tbNetPayableOFOther.getValue()!=null&&!tbNetPayableOFOther.getValue().equals("")){
//				total4 = Double.parseDouble(this.tbNetPayableOFOther.getValue()
//						.trim());
//				}
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//
//			double netPayOFCNC = total1 + total2 + total3 + total4;
//			this.donetpayamt.setValue(netPayOFCNC);

		}
		if (event.getSource().equals(tbInterestOfMachinary)) {
			if (equipmentRentalTable != null) {
				for (int i = 0; i < this.equipmentRentalTable.getDataprovider()
						.getList().size(); i++) {
					EquipmentRental equipmentRental = this.equipmentRentalTable
							.getDataprovider().getList().get(i);
					try {
						equipmentRental.setMachineryInterest(Double
								.parseDouble(tbInterestOfMachinary.getValue()));
					} catch (Exception e) {
						e.printStackTrace();
						equipmentRental.setMachineryInterest(0d);
					}
					this.equipmentRentalTable.updateRow(equipmentRental, i);
				}
			}
		}

		if (event.getSource().equals(tbMaintaniancePercentage)) {
			if (equipmentRentalTable != null) {
				for (int i = 0; i < this.equipmentRentalTable.getDataprovider()
						.getList().size(); i++) {
					EquipmentRental equipmentRental = this.equipmentRentalTable
							.getDataprovider().getList().get(i);
					try {
						equipmentRental.setMaintanancePercentage(Double
								.parseDouble(tbMaintaniancePercentage.getValue()));
					} catch (Exception e) {
						e.printStackTrace();
						equipmentRental.setMaintanancePercentage(0d);
					}
					this.equipmentRentalTable.updateRow(equipmentRental, i);
				}
			}
		}

		if (event.getSource().equals(tbcBonus)) {
			if (tbcBonus.getValue() != null) {
				if (staffingTable != null) {
					int count = 0;
					for (StaffingDetails staffingDetails : this.staffingTable
							.getDataprovider().getList()) {
						try {
							staffingDetails.setcBonusPercentage(Double
									.parseDouble(tbcBonus.getValue().trim()));
							this.staffingTable.updateCBonusPercentage(
									staffingDetails, count);
							count += 1;
						} catch (Exception e) {
							e.printStackTrace();
							staffingDetails.setcBonusPercentage(0d);
							showDialogMessage("Only numbers are allowed!!");
						}

					}
					// if(equipmentRentalTable!=null){
					// equipmentRentalTable.getTable().redraw();
					// }
				}
			}
		}

		if (event.getSource().equals(lstbVersion)) {
			try {
				CNC cnc = cncObject.getVersionMap().get(
						lstbVersion.getValue(lstbVersion.getSelectedIndex()));

				staffingTable.getDataprovider().getList().clear();
				staffingTable.getDataprovider().getList()
						.addAll(cnc.getSaffingDetailsList());

				equipmentRentalTable.getDataprovider().getList().clear();
				equipmentRentalTable.getDataprovider().getList()
						.addAll(cnc.getEquipmentRentalList());

				consumableTable.getDataprovider().getList().clear();
				consumableTable.getDataprovider().getList()
						.addAll(cnc.getConsumablesList());

				otherServiceTable.getDataprovider().getList().clear();
				otherServiceTable.getDataprovider().getList()
						.addAll(cnc.getOtherServicesList());

			} catch (Exception e) {
				Console.log("Error in list box of version " + e);
			}
		}
		
		if(event.getSource().equals(tbPaidLeavesDays)){
			if(tbPaidLeavesDays.getValue()!=null){
				if(staffingTable!=null){
					int count=0;
					for(StaffingDetails staffingDetails : this.staffingTable.getDataprovider().getList()){
						try{
							staffingDetails.setPaidLeaveDays(tbPaidLeavesDays.getValue());
							this.staffingTable.updatePaidLeaves(staffingDetails, count);
							count+=1;
						}catch(Exception e){
							e.printStackTrace();
							staffingDetails.setPaidLeaveDays(0+"");
//							showDialogMessage("Only numbers are allowed!!");
						}
						
					}
				}
			}
		}
		/** date 13.8.2018 added by komal **/
	
		if(event.getSource().equals(dbFlatManagementFees)){
			if(dbFlatManagementFees.getValue()!=null && dbFlatManagementFees.getValue()!=0){
				tbManagementFeesPercentage.setValue(0+"");
			}
				double value = dbFlatManagementFees.getValue();
				Console.log("Value :" + value +" "+dbFlatManagementFees.getValue());
				
			Console.log("Inside Management Fee change");
			double netPayableForStaffing = 0;
			if (staffingTable != null) {

				for (int i = 0; i < this.staffingTable.getDataprovider()
						.getList().size(); i++) {
					StaffingDetails staffingDetails = this.staffingTable
							.getDataprovider().getList().get(i);
					netPayableForStaffing += staffingDetails.getTotalPaidIntoEmployee();
					staffingDetails.setManagementFees(value+"");
					
					staffingDetails.setManageMentfeesPercent(0.0);
					staffingDetails.setManagementFeesRate(value);
					
					this.staffingTable.updateRow(staffingDetails, i,false);
				}
			}
			this.tbGrossTotalOfStaffing.setValue(Math
					.round(netPayableForStaffing) + "");
//			this.tbManagementFeesValue.setValue(Math.round(value)
//					+ "");
//			double netPayableOfStaffing = netPayableForStaffing
//					+ value;
//			this.tbNetPayableOFStaffing.setValue(Math
//					.round(netPayableOfStaffing) + "");

			double total1 = 0, total2 = 0, total3 = 0, total4 = 0;
			try {
				total1 = Double.parseDouble(this.tbNetPayableOFStaffing
						.getValue().trim());
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				total2 = Double.parseDouble(this.tbNetPayableOFEquipment
						.getValue().trim());
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				total3 = Double.parseDouble(this.tbNetPayableOFConsumabables
						.getValue().trim());
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				total4 = Double.parseDouble(this.tbNetPayableOFOther.getValue()
						.trim());
			} catch (Exception e) {
				e.printStackTrace();
			}

			double netPayOFCNC = total1 + total2 + total3 + total4;
			this.donetpayamt.setValue(netPayOFCNC);

		
		}
			if(event.getSource().equals(dbFlatBonus)){
			if(dbFlatBonus.getValue()!=null){
				dbBonusPercentage.setValue(0.0);
				double bonus = dbFlatBonus.getValue();
				for(StaffingDetails details : staffingTable.getDataprovider().getList()){
					details.setSpBonus(bonus+"");
					this.staffingTable.updateSPBonus(details , 0);
				}
				if(staffingTable.getDataprovider().getList().size()>0){
					RowCountChangeEvent.fire(staffingTable.getTable(), staffingTable.getDataprovider().getList().size(), true);
					staffingTable.getTable().redraw();
				}
			}
		}
//		if(event.getSource().equals(dbTravellingPercentage)){
//			if(dbTravellingPercentage.getValue()!=null && dbTravellingPercentage.getValue()!=0){
//				dbFlatTravelling.setValue(0.0);
//			}
//		}
		if(event.getSource().equals(dbFlatTravelling)){
			if(dbFlatTravelling.getValue()!=null){
				//dbTravellingPercentage.setValue(0.0);
				double travelling = dbFlatTravelling.getValue();
				for(StaffingDetails details : staffingTable.getDataprovider().getList()){
					details.setTravellingAllowance(travelling);
					this.staffingTable.updateSPBonus(details , 0);
				}
				if(staffingTable.getDataprovider().getList().size()>0){
					RowCountChangeEvent.fire(staffingTable.getTable(), staffingTable.getDataprovider().getList().size(), true);
					staffingTable.getTable().redraw();
				}
			}
		}
//		if(event.getSource().equals(dbOverheadCostPercentage)){
//			if(dbOverheadCostPercentage.getValue()!=null && dbOverheadCostPercentage.getValue()!=0){
//				dbFlatOverheadCost.setValue(0.0);
//			}
//		}
		if(event.getSource().equals(dbFlatOverheadCost)){
			if(dbFlatOverheadCost.getValue()!=null){
				dbOverheadCostPercentage.setValue(0.0);
				double overheadeCost = dbFlatOverheadCost.getValue();
				for(StaffingDetails details : staffingTable.getDataprovider().getList()){
					details.setOverHeadCost(overheadeCost+"");
					this.staffingTable.updateSPBonus(details, 0);
				}
				if(staffingTable.getDataprovider().getList().size()>0){
					RowCountChangeEvent.fire(staffingTable.getTable(), staffingTable.getDataprovider().getList().size(), true);
					staffingTable.getTable().redraw();
				}

			}
		}
		/** end komal */
		//komal
		if(event.getSource().equals(dbDiscount)){
			double discount = 0;
			double netPayableForEquipment = 0;
			if(dbTotalWithoutDiscount.getValue()!=0){
				netPayableForEquipment = dbTotalWithoutDiscount.getValue();
			}
			if(dbDiscount.getValue()!=null){
				discount = dbDiscount.getValue();
			}
			tbNetPayableOFEquipment.setValue((netPayableForEquipment - discount)+"");
			
			double total1 = 0, total2 = 0, total3 = 0, total4 = 0;
			try {
				total1 = Double.parseDouble(this.tbNetPayableOFStaffing
						.getValue().trim());
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				total2 = Double.parseDouble(this.tbNetPayableOFEquipment
						.getValue().trim());
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				total3 = Double.parseDouble(this.tbNetPayableOFConsumabables
						.getValue().trim());
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				total4 = Double.parseDouble(this.tbNetPayableOFOther.getValue()
						.trim());
			} catch (Exception e) {
				e.printStackTrace();
			}

			double netPayOFCNC = total1 + total2 + total3 + total4;
			this.donetpayamt.setValue(netPayOFCNC);

		}
		/** end komal */
		
		/**
		 * @author Anil
		 * @since 20-08-2020
		 */
		if(event.getSource().equals(oblPaidLeave)){
			if(oblPaidLeave.getValue()!=null){
				staffingTable.setPaidleave(oblPaidLeave.getSelectedItem());
			}else{
				staffingTable.setPaidleave(null);
			}
			if (staffingTable != null) {
				for (int i = 0; i < this.staffingTable.getDataprovider().getList().size(); i++) {
					StaffingDetails staffingDetails = this.staffingTable.getDataprovider().getList().get(i);
					this.staffingTable.updateRow(staffingDetails, i,true);
				}
				if(staffingTable.getDataprovider().getList().size()>0){
					RowCountChangeEvent.fire(staffingTable.getTable(), staffingTable.getDataprovider().getList().size(), true);
					staffingTable.getTable().redraw();
				}
			}
		}
		
		if(event.getSource().equals(oblState)) {
			
			if(siteLocation&&oblCustomerBranch.getItems()!=null&&oblCustomerBranch.getItems().size()!=0){
				if(consumableTable.getValue()!=null&&consumableTable.getValue().size()!=0){
					consumableTable.siteLocationList=getSiteLocationList();
					consumableTable.setEnable(true);
				}
			}
		}
	}

	public ArrayList<String> getSiteLocationList(){
		/**
		 * @author Anil @since 10-11-2021
		 * setting up site location as per the state
		 * Sunrise SCM
		 */
		ArrayList<String> siteLocList=new ArrayList<String>();
		siteLocList.add("--SELECT--");
		if(oblCustomerBranch.getItems()!=null){
			Console.log("Site Location updating..!!");
			for(CustomerBranchDetails obj:oblCustomerBranch.getItems()){
				if(oblState.getValue()!=null&&obj.getAddress()!=null){
					if(obj.getAddress().getState().equals(oblState.getValue())){
						siteLocList.add(obj.getBusinessUnitName());
					}
				}else{
					siteLocList.add(obj.getBusinessUnitName());
				}
			}
		}
		Console.log("Site Location Size "+siteLocList.size());
		return siteLocList;
	}

	/****************** Getters and Setters *********************************/

	public PersonInfoComposite getPersonInfoComposite() {
		return personInfoComposite;
	}

	public void setPersonInfoComposite(PersonInfoComposite personInfoComposite) {
		this.personInfoComposite = personInfoComposite;
	}

	public TextBox getTbContractId() {
		return tbContractId;
	}

	public void setTbContractId(TextBox tbContractId) {
		this.tbContractId = tbContractId;
	}

	public TextBox getTbStatus() {
		return tbStatus;
	}

	public void setTbStatus(TextBox tbStatus) {
		this.tbStatus = tbStatus;
	}

	public TextBox getTbRefNum() {
		return tbRefNum;
	}

	public void setTbRefNum(TextBox tbRefNum) {
		this.tbRefNum = tbRefNum;
	}

	public TextBox getTbCreditPeriod() {
		return tbCreditPeriod;
	}

	public void setTbCreditPeriod(TextBox tbCreditPeriod) {
		this.tbCreditPeriod = tbCreditPeriod;
	}

	public TextBox getTbApprovedRemark() {
		return tbApprovedRemark;
	}

	public void setTbApprovedRemark(TextBox tbApprovedRemark) {
		this.tbApprovedRemark = tbApprovedRemark;
	}

	public DateBox getDbContractStartDate() {
		return dbContractStartDate;
	}

	public void setDbContractStartDate(DateBox dbContractStartDate) {
		this.dbContractStartDate = dbContractStartDate;
	}

	public DateBox getDbContractEndDate() {
		return dbContractEndDate;
	}

	public void setDbContractEndDate(DateBox dbContractEndDate) {
		this.dbContractEndDate = dbContractEndDate;
	}

	public ObjectListBox<ConfigCategory> getOlbContractCategory() {
		return olbContractCategory;
	}

	public void setOlbContractCategory(
			ObjectListBox<ConfigCategory> olbContractCategory) {
		this.olbContractCategory = olbContractCategory;
	}

	public ObjectListBox<ConfigCategory> getOlbPaymentTerms() {
		return olbPaymentTerms;
	}

	public void setOlbPaymentTerms(ObjectListBox<ConfigCategory> olbPaymentTerms) {
		this.olbPaymentTerms = olbPaymentTerms;
	}

	public ObjectListBox<Config> getOlbContractGroup() {
		return olbContractGroup;
	}

	public void setOlbContractGroup(ObjectListBox<Config> olbContractGroup) {
		this.olbContractGroup = olbContractGroup;
	}

	public ObjectListBox<Config> getOlbcPaymentMethods() {
		return olbcPaymentMethods;
	}

	public void setOlbcPaymentMethods(ObjectListBox<Config> olbcPaymentMethods) {
		this.olbcPaymentMethods = olbcPaymentMethods;
	}

	public ObjectListBox<Type> getOlbContractType() {
		return olbContractType;
	}

	public void setOlbContractType(ObjectListBox<Type> olbContractType) {
		this.olbContractType = olbContractType;
	}

	public ObjectListBox<Branch> getOlbbBranch() {
		return olbbBranch;
	}

	public void setOlbbBranch(ObjectListBox<Branch> olbbBranch) {
		this.olbbBranch = olbbBranch;
	}

	public ObjectListBox<Employee> getOlbeSalesPerson() {
		return olbeSalesPerson;
	}

	public void setOlbeSalesPerson(ObjectListBox<Employee> olbeSalesPerson) {
		this.olbeSalesPerson = olbeSalesPerson;
	}

	public ObjectListBox<Employee> getOlbApproverName() {
		return olbApproverName;
	}

	public void setOlbApproverName(ObjectListBox<Employee> olbApproverName) {
		this.olbApproverName = olbApproverName;
	}

	public StaffingDetailsTable getStaffingTable() {
		return staffingTable;
	}

	public void setStaffingTable(StaffingDetailsTable staffingTable) {
		this.staffingTable = staffingTable;
	}

	public EquipmentRentalTable getEquipmentRentalTable() {
		return equipmentRentalTable;
	}

	public void setEquipmentRentalTable(
			EquipmentRentalTable equipmentRentalTable) {
		this.equipmentRentalTable = equipmentRentalTable;
	}

	public ConsumablesTable getConsumableTable() {
		return consumableTable;
	}

	public void setConsumableTable(ConsumablesTable consumableTable) {
		this.consumableTable = consumableTable;
	}

	public OtherServicesTable getOtherServiceTable() {
		return otherServiceTable;
	}

	public void setOtherServiceTable(OtherServicesTable otherServiceTable) {
		this.otherServiceTable = otherServiceTable;
	}

	public TabPanel getTabPanel() {
		return tabPanel;
	}

	public void setTabPanel(TabPanel tabPanel) {
		this.tabPanel = tabPanel;
	}

	public void toggleAppHeaderBarMenu() {
//		System.out.println("toggleAppHeaderBarMenu called");
		if (AppMemory.getAppMemory().currentState == ScreeenState.NEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Save") || text.contains("Discard")
						|| text.contains("Search")
						|| text.contains(AppConstants.NAVIGATION)) {
					menus[k].setVisible(true);
				} else
					menus[k].setVisible(false);

			}
		}

		else if (AppMemory.getAppMemory().currentState == ScreeenState.EDIT) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Save") || text.contains("Discard")
						|| text.contains(AppConstants.NAVIGATION))
					menus[k].setVisible(true);
				else
					menus[k].setVisible(false);
			}
		}

		else if (AppMemory.getAppMemory().currentState == ScreeenState.VIEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Edit") || text.contains("Discard")
						|| text.contains("Search")
						|| text.contains(AppConstants.NAVIGATION))//|| text.contains(AppConstants.COMMUNICATIONLOG) || text.contains("Print")
					menus[k].setVisible(true);

				else
					menus[k].setVisible(false);
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.CNC,
				LoginPresenter.currentModule.trim());
	}

	@Override
	public void setToViewState() {

		super.setToViewState();
		setMenuAsPerStatus();
		bt_addEquipmentDetails.setEnabled(false);
		bt_addStaffingDetails.setEnabled(false);
		bt_consumablesDetails.setEnabled(false);
		bt_otherServices.setEnabled(false);
//		staffingTable.setEnable(false);
//		consumableTable.setEnable(false);
//		equipmentRentalTable.setEnable(false);
//		otherServiceTable.setEnable(false);

		SuperModel model = new CNC();
		model = cncObject;

	}

	@Override
	public void setToEditState() {
		
		Console.log("version in edit :" + dbVersionNumber.getValue());
		/** date 28.1.2019 added by komal to change status , when CNC is in Rejected status**/
		CNC entity=(CNC) presenter.getModel();
		
		String status=entity.getStatus();		
		if(SalesOrder.REJECTED.equals(status.trim()))
		{
			entity.setIslatestVersion(true);
			changeStatusToCreated();
		}
		if(entity.isIslatestVersion() || dbVersionNumber.getValue() > 0 || dbVersionNumber.getValue() ==0){
		super.setToEditState();
		this.processLevelBar.setVisibleFalse(false);
		if (isAmended) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Save")) {
					menus[k].setVisible(true);
				}
			}
		}
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CNC", "MakeProjectNameEnable")){
			projectNameEnable=true;
		}
		if(projectNameEnable){
		tbProjectName.setEnabled(true);
		}else{
		tbProjectName.setEnabled(false);
		}
		bt_addEquipmentDetails.setEnabled(true);
		bt_addStaffingDetails.setEnabled(true);
		bt_consumablesDetails.setEnabled(true);
		bt_otherServices.setEnabled(true);
//		staffingTable.setEnable(true);
//		consumableTable.setEnable(true);
//		equipmentRentalTable.setEnable(true);
//		otherServiceTable.setEnable(true);
		/** date 11.8.2018 added by komal for terms and condition details **/
		btAddTermsandConditions.setEnabled(true);
		}else{
			setToViewState();
			showDialogMessage("Please select latest version.");
		}
	
	
	}

	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		staffingTable.setEnable(state);
		equipmentRentalTable.setEnable(state);
		consumableTable.setEnable(state);
		otherServiceTable.setEnable(state);
		// tbOverHeadCostPerEmployee.setEnabled(state);
		tbMaintaniancePercentage.setEnabled(state);
		tbInterestOfMachinary.setEnabled(state);
		tbManagementFeesPercentage.setEnabled(state);
		tbcBonus.setEnabled(state);
		cb_Relever_for_Bonus.setEnabled(state);
		cb_Relever_for_PaidLeaves.setEnabled(state);
		cb_Cbonus.setEnabled(state);
		cb_CLeave.setEnabled(state);
		/**
		 * Rahul Verma added on 08 Aug 2018
		 */
		tbPaidLeavesDays.setEnabled(state);
		/** date 11.8.2018 added by komal for terms and condition details **/
		obTermsandCondition.setEnabled(state);
		taTermsandConditions.setEnabled(state);
		btAddTermsandConditions.setEnabled(state);
		dbFlatManagementFees.setEnabled(state);
		dbBonusPercentage.setEnabled(state);
		dbFlatBonus.setEnabled(state);
		dbTravellingPercentage.setEnabled(state);
		dbFlatTravelling.setEnabled(state);
		dbOverheadCostPercentage.setEnabled(state);
		dbFlatOverheadCost.setEnabled(state);
		/** date 21.8.2018 added by komal**/
		dbVersionNumber.setEnabled(false);
		tbContractId.setEnabled(false);
		tbStatus.setEnabled(false);
		if(cncObject !=null && cncObject.getStatus()!=null){
			if(cncObject.getStatus().equals(AppConstants.APPROVED)){
				tbProjectName.setEnabled(false);
				olbApproverName.setEnabled(false);
				personInfoComposite.setEnable(false);
				tbApprovedRemark.setEnabled(false);
			}
		}
		dbTotalWithoutDiscount.setEnabled(false);
		tbCancellationRemark.setEnabled(false);
		
		dbMonthlyFinPlan.setEnabled(state);
		dbInterestOnFinAmt.setEnabled(state);
		dbEstimatedRepair.setEnabled(state);
		dbAdminOhCost.setEnabled(state);
		dbApproxUniformCost.setEnabled(state);
		
		oblPaidLeave.setEnabled(state);
		
		olbDesignation1.setEnabled(state);
		lbOtName.setEnabled(state);
		dbFlatRate.setEnabled(state);
		dbFactorRate.setEnabled(state);
//		cbTypeDay.setEnabled(state);
//		cbTypeHourly.setEnabled(state);
		btnAddOt.setEnabled(state);
		otRateTbl.setEnable(state);
		
		btnUploadConsumable.setEnabled(state);
	}

	@Override
	public TextBox getstatustextbox() {
		// TODO Auto-generated method stub
		return this.tbStatus;
	}

	private void setMenuAsPerStatus() {
		// TODO Auto-generated method stub
		this.toggleAppHeaderBarMenu();
		this.toggleProcessLevelMenu();
	}

	private void toggleProcessLevelMenu() {
		
		CNC entity = (CNC) presenter.getModel();

		String status = entity.getStatus();
		for (int i = 0; i < getProcesslevelBarNames().length; i++) {
			InlineLabel label = getProcessLevelBar().btnLabels[i];
			String text = label.getText().trim();

			if (status.equals(Contract.CREATED)) {
				if (getManageapproval().isSelfApproval()) { 
					/**
					 * Ajinkya added this code Date:1/08/2017
					 */
					if (text.equals(ManageApprovals.SUBMIT))
						label.setVisible(true);
					if (text.equals(ManageApprovals.APPROVALREQUEST))
						label.setVisible(false);
					/**
					 * End here
					 */
				} else {
					/**
					 * Ajinkya added this code Date:1/08/2017
					 */
					if (text.equals(ManageApprovals.SUBMIT))
						label.setVisible(false);
					if (text.equals(ManageApprovals.APPROVALREQUEST))
						label.setVisible(true);
					/**
					 * End here
					 */
				}
				if (text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
					label.setVisible(false);
				if (text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
					label.setVisible(false);
				if (text.equals(AppConstants.CANCLECONTRACT))
					label.setVisible(true);
				if (text.equals("View Project"))
					label.setVisible(false);
				if (text.equals(AppConstants.NEW))
					label.setVisible(true);
				if (text.equals(AppConstants.VIEWBILL))
					label.setVisible(false);
				if (text.equals("Amend Contract"))
					label.setVisible(false);
				if (text.equals(ManageApprovals.APPROVALSTATUS))
					label.setVisible(false);
				/**
				 * date : 08-06-2018 BY ANIL
				 */
				if (text.equals("Raise Bill"))
					label.setVisible(false);

				if (text.equals("Mark Confirmed"))
					label.setVisible(false);
				
				if (text.equals(AppConstants.ARREARSBILL))
					label.setVisible(false);
				/**
				 * date : 09-03-2021 BY PRIYANKA
				 */
				if (text.equals(AppConstants.CREATESALESORDER))
					label.setVisible(false);

			} else if (status.equals(Contract.APPROVED)) {
				if (getManageapproval().isSelfApproval()) {
					/**
					 * Ajinkya added this code Date:1/08/2017
					 */
					if (text.equals(ManageApprovals.SUBMIT))
						label.setVisible(false);
					if (text.equals(ManageApprovals.APPROVALREQUEST))
						label.setVisible(false);
					/**
					 * End here
					 */
				} else {
					/**
					 * Ajinkya added this code Date:1/08/2017
					 */
					if (text.equals(ManageApprovals.SUBMIT))
						label.setVisible(false);
					if (text.equals(ManageApprovals.APPROVALREQUEST))
						label.setVisible(false);
					/**
					 * End here
					 */
				}
				if (text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
					label.setVisible(false);
				if (text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
					label.setVisible(false);
				if (text.equals(AppConstants.CANCLECONTRACT))
					label.setVisible(true);
				if (text.equals("View Project"))
					label.setVisible(true);
				if (text.equals(AppConstants.NEW))
					label.setVisible(true);
				if (text.equals(AppConstants.VIEWBILL))
					label.setVisible(true);
				if (text.equals("Amend Contract"))
					label.setVisible(true);
				if (text.equals(ManageApprovals.APPROVALSTATUS))
					label.setVisible(true);
				/**
				 * date : 08-06-2018 BY ANIL
				 */
				if (text.equals("Raise Bill"))
					label.setVisible(true);
				if (text.equals("Mark Confirmed"))
					label.setVisible(true);
				
				if (text.equals(AppConstants.ARREARSBILL))
					label.setVisible(false);
				/**
				 * date : 09-03-2021 BY PRIYANKA
				 */
				if (text.equals(AppConstants.CREATESALESORDER))
					label.setVisible(true);
				
			} else if (status.equals(Contract.CONTRACTCANCEL)) {
				if (getManageapproval().isSelfApproval()) {
					/**
					 * Ajinkya added this code Date:1/08/2017
					 */
					if (text.equals(ManageApprovals.SUBMIT))
						label.setVisible(false);
					if (text.equals(ManageApprovals.APPROVALREQUEST))
						label.setVisible(false);
					/**
					 * End here
					 */
				} else {
					/**
					 * Ajinkya added this code Date:1/08/2017
					 */
					if (text.equals(ManageApprovals.SUBMIT))
						label.setVisible(false);
					if (text.equals(ManageApprovals.APPROVALREQUEST))
						label.setVisible(false);
					/**
					 * End here
					 */
				}
				if (text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
					label.setVisible(false);
				if (text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
					label.setVisible(false);
				if (text.equals(AppConstants.CANCLECONTRACT))
					label.setVisible(false);
				if (text.equals("View Project"))
					label.setVisible(true);
				if (text.equals(AppConstants.NEW))
					label.setVisible(true);
				if (text.equals(AppConstants.VIEWBILL))
					label.setVisible(true);
				if (text.equals("Amend Contract"))
					label.setVisible(false);
				if (text.equals(ManageApprovals.APPROVALSTATUS))
					label.setVisible(false);
				/**
				 * date : 08-06-2018 BY ANIL
				 */
				if (text.equals("Raise Bill"))
					label.setVisible(false);
				if (text.equals("Mark Confirmed"))
					label.setVisible(false);
				
				if (text.equals(AppConstants.ARREARSBILL))
					label.setVisible(false);
				// by Priyanka
				if (text.equals(AppConstants.CREATESALESORDER))
					label.setVisible(true);
				
			}

			else if (status.equals(Contract.REQUESTED)) {
				if (getManageapproval().isSelfApproval()) {
					/**
					 * Ajinkya added this code Date:1/08/2017
					 */
					if (text.equals(ManageApprovals.SUBMIT))
						label.setVisible(false);
					if (text.equals(ManageApprovals.APPROVALREQUEST))
						label.setVisible(false);
					/**
					 * End here
					 */
				} else {
					/**
					 * Ajinkya added this code Date:1/08/2017
					 */
					if (text.equals(ManageApprovals.SUBMIT))
						label.setVisible(false);
					if (text.equals(ManageApprovals.APPROVALREQUEST))
						label.setVisible(false);
					/**
					 * End here
					 */
				}
				if (text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
					label.setVisible(true);
				if (text.equals(AppConstants.CANCLECONTRACT))
					label.setVisible(true);
				if (text.equals("View Project"))
					label.setVisible(false);
				if (text.equals(AppConstants.NEW))
					label.setVisible(true);
				if (text.equals(AppConstants.VIEWBILL))
					label.setVisible(false);
				if (text.equals("Amend Contract"))
					label.setVisible(false);
				if (text.equals(ManageApprovals.APPROVALSTATUS))
					label.setVisible(false);
				/**
				 * date : 08-06-2018 BY ANIL
				 */
				if (text.equals("Raise Bill"))
					label.setVisible(false);
				if (text.equals("Mark Confirmed"))
					label.setVisible(false);
				
				if (text.equals(AppConstants.ARREARSBILL))
					label.setVisible(false);
				//by Priyanka
				if (text.equals(AppConstants.CREATESALESORDER))
					label.setVisible(false);
				
			}

			else if (status.equals(Contract.REJECTED)) {
				if (getManageapproval().isSelfApproval()) {
					/**
					 * Ajinkya added this code Date:1/08/2017
					 */
					if (text.equals(ManageApprovals.SUBMIT))
						label.setVisible(false);
					if (text.equals(ManageApprovals.APPROVALREQUEST))
						label.setVisible(false);
					/**
					 * End here
					 */
				} else {
					/**
					 * Ajinkya added this code Date:1/08/2017
					 */
					if (text.equals(ManageApprovals.SUBMIT))
						label.setVisible(false);
					if (text.equals(ManageApprovals.APPROVALREQUEST))
						label.setVisible(false);
					/**
					 * End here
					 */
				}
				if (text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
					label.setVisible(false);
				if (text.equals(AppConstants.CANCLECONTRACT))
					label.setVisible(true);
				if (text.equals("View Project"))
					label.setVisible(false);
				if (text.equals(AppConstants.NEW))
					label.setVisible(true);
				if (text.equals(AppConstants.VIEWBILL))
					label.setVisible(false);
				if (text.equals("Amend Contract"))
					label.setVisible(false);
				if (text.equals(ManageApprovals.APPROVALSTATUS))
					label.setVisible(false);
				/**
				 * date : 08-06-2018 BY ANIL
				 */
				if (text.equals("Raise Bill"))
					label.setVisible(false);
				if (text.equals("Mark Confirmed"))
					label.setVisible(false);
				
				if (text.equals(AppConstants.ARREARSBILL))
					label.setVisible(false);
				
				//by Priyanka
				if (text.equals(AppConstants.CREATESALESORDER))
					label.setVisible(false);
				
				
			} else if (status.equals(CNC.CONTRACTCONFIRMED)) {
				if (getManageapproval().isSelfApproval()) {
					/**
					 * Ajinkya added this code Date:1/08/2017
					 */
					if (text.equals(ManageApprovals.SUBMIT))
						label.setVisible(false);
					if (text.equals(ManageApprovals.APPROVALREQUEST))
						label.setVisible(false);
					/**
					 * End here
					 */
				} else {
					/**
					 * Ajinkya added this code Date:1/08/2017
					 */
					if (text.equals(ManageApprovals.SUBMIT))
						label.setVisible(false);
					if (text.equals(ManageApprovals.APPROVALREQUEST))
						label.setVisible(false);
					/**
					 * End here
					 */
				}
				if (text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
					label.setVisible(false);
				if (text.equals(AppConstants.CANCLECONTRACT))
					label.setVisible(true);
				if (text.equals("View Project"))
					label.setVisible(true);
				if (text.equals(AppConstants.NEW))
					label.setVisible(true);
				if (text.equals(AppConstants.VIEWBILL))
					label.setVisible(true);
				if (text.equals("Amend Contract"))
					label.setVisible(true);
				if (text.equals(ManageApprovals.APPROVALSTATUS))
					label.setVisible(true);
				/**
				 * date : 08-06-2018 BY ANIL
				 */
				if (text.equals("Raise Bill"))
					label.setVisible(true);
				if (text.equals("Mark Confirmed"))
					label.setVisible(false);
				
				if (text.equals(AppConstants.ARREARSBILL))
					label.setVisible(true);
				
				//by Priyanka
				if (text.equals(AppConstants.CREATESALESORDER))
					label.setVisible(true);
				
			}
		}
		setAppHeaderBarAsPerStatus();
	
	}

	public void setAppHeaderBarAsPerStatus()
	{
		CNC entity = (CNC) presenter.getModel();
		String status = entity.getStatus();
		if(status.equals(ConcreteBusinessProcess.REQUESTED))
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Edit"))
					menus[k].setVisible(false); 
			}

		}

		if(status.equals(Quotation.APPROVED) || status.equals(CNC.CONTRACTCONFIRMED))
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
//				/** date 22.8.2018 added by komal**/
//				if(businessprocess instanceof CNC){
//					if(text.contains("Discard") || text.contains("Save") || text.contains("Search"))
//					{
//						menus[k].setVisible(true); 
//					}
//					else
//						menus[k].setVisible(false);  
//
//				}else{
					if(text.contains("Discard")||text.contains("Search")||text.contains("Email")||text.contains("Print"))
					{
						menus[k].setVisible(true); 
					}
					else
						menus[k].setVisible(false);  

				//}
				
			}
		}
		
	}
	/**************** Getters and Setters ***********************************/

	// public TextBox getTbNoOFMonthsForRental() {
	// return tbNoOFMonthsForRental;
	// }

	// public void setTbNoOFMonthsForRental(TextBox tbNoOFMonthsForRental) {
	// this.tbNoOFMonthsForRental = tbNoOFMonthsForRental;
	// }

	public TextBox getTbInterestOfMachinary() {
		return tbInterestOfMachinary;
	}

	public void setTbInterestOfMachinary(TextBox tbInterestOfMachinary) {
		this.tbInterestOfMachinary = tbInterestOfMachinary;
	}

	public TextBox getTbMaintaniancePercentage() {
		return tbMaintaniancePercentage;
	}

	public void setTbMaintaniancePercentage(TextBox tbMaintaniancePercentage) {
		this.tbMaintaniancePercentage = tbMaintaniancePercentage;
	}

	public TextBox getTbcBonus() {
		return tbcBonus;
	}

	public void setTbcBonus(TextBox tbcBonus) {
		this.tbcBonus = tbcBonus;
	}

	public TextBox getTbManagementFees() {
		return tbManagementFeesPercentage;
	}

	public CheckBox getCb_Relever_for_Bonus() {
		return cb_Relever_for_Bonus;
	}

	public void setCb_Relever_for_Bonus(CheckBox cb_Relever_for_Bonus) {
		this.cb_Relever_for_Bonus = cb_Relever_for_Bonus;
	}

	public CheckBox getCb_Relever_for_PaidLeaves() {
		return cb_Relever_for_PaidLeaves;
	}

	public void setCb_Relever_for_PaidLeaves(CheckBox cb_Relever_for_PaidLeaves) {
		this.cb_Relever_for_PaidLeaves = cb_Relever_for_PaidLeaves;
	}

	public CheckBox getCb_Cbonus() {
		return cb_Cbonus;
	}

	public void setCb_Cbonus(CheckBox cb_Cbonus) {
		this.cb_Cbonus = cb_Cbonus;
	}

	public CheckBox getCb_CLeave() {
		return cb_CLeave;
	}

	public void setCb_CLeave(CheckBox cb_CLeave) {
		this.cb_CLeave = cb_CLeave;
	}

	public void setTbManagementFees(TextBox tbManagementFees) {
		this.tbManagementFeesPercentage = tbManagementFees;
	}
	
	

	public TextBox getTbPaidLeavesDays() {
		return tbPaidLeavesDays;
	}

	public void setTbPaidLeavesDays(TextBox tbPaidLeavesDays) {
		this.tbPaidLeavesDays = tbPaidLeavesDays;
	}
	

	public TextArea getTbCancellationRemark() {
		return tbCancellationRemark;
	}

	public void setTbCancellationRemark(TextArea tbCancellationRemark) {
		this.tbCancellationRemark = tbCancellationRemark;
	}

	// komal
	private Widget getFieldOB(String labelName, ObjectListBox listBox) {
		// TODO Auto-generated method stub
		VerticalPanel verticalPanel = new VerticalPanel();
		verticalPanel.getElement().getStyle().setMargin(2.0, Unit.PCT);
		Label label = new Label(labelName);
		verticalPanel.add(label);
		listBox.getElement().getStyle().setMarginRight(5.0, Unit.PX);
		listBox.getElement().getStyle().setWidth(200, Unit.PX);
		listBox.getElement().getStyle().setHeight(130, Unit.PCT);
		verticalPanel.add(listBox);
		return verticalPanel;
	}
	
	private Widget getFieldLB(String labelName,ListBox listBox) {
		// TODO Auto-generated method stub
		VerticalPanel verticalPanel = new VerticalPanel();
		verticalPanel.getElement().getStyle().setMargin(2.0, Unit.PCT);
		Label label = new Label(labelName);
		verticalPanel.add(label);
		listBox.getElement().getStyle().setMarginRight(5.0, Unit.PX);
		listBox.getElement().getStyle().setWidth(200, Unit.PX);
		listBox.getElement().getStyle().setHeight(130, Unit.PCT);
		verticalPanel.add(listBox);
		return verticalPanel;
	}

	// komal
	private Widget getPanelForTermasandConditions() {
		// TODO Auto-generated method stub
		VerticalPanel verticalPnl = new VerticalPanel();
		VerticalPanel verticalPn2 = new VerticalPanel();
		VerticalPanel verticalPn3 = new VerticalPanel();
		HorizontalPanel horizontalPanel = new HorizontalPanel();
		horizontalPanel.getElement().getStyle().setWidth(100, Unit.PCT);
		horizontalPanel.add(getFieldOB("Select Terms and Conditions",
				obTermsandCondition));
		horizontalPanel.getElement().getStyle().setWidth(100, Unit.PCT);
		horizontalPanel.add(getFieldButton(".   ", btAddTermsandConditions));
		HorizontalPanel horizontalPanel2 = new HorizontalPanel();
		horizontalPanel2.add(getFieldTextArea("Terms and Conditions",
				taTermsandConditions));
		horizontalPanel2.getElement().getStyle().setWidth(100, Unit.PCT);
		verticalPnl.add(horizontalPanel);
		verticalPn2.add(horizontalPanel2);
		verticalPn3.add(verticalPnl);
		verticalPn3.add(verticalPn2);
		return verticalPn3;
	}

	private Widget getFieldTextArea(String labelName, TextArea textArea) {
		// TODO Auto-generated method stub
		VerticalPanel verticalPanel = new VerticalPanel();
		verticalPanel.getElement().getStyle().setMargin(2.0, Unit.PCT);
		Label label = new Label(labelName);
		verticalPanel.add(label);
		textArea.getElement().getStyle().setMarginRight(5.0, Unit.PX);
		textArea.getElement().getStyle().setWidth(680, Unit.PCT);
		textArea.getElement().getStyle().setHeight(200, Unit.PX);
		verticalPanel.add(textArea);
		return verticalPanel;
	}
	
	/**
	 * Date : 22-10-2018 By ANIL
	 * Validating Project Name,if any already created
	 */
	
	public void validateProject(){
		
		final GenricServiceAsync async=GWT.create(GenricService.class);
		MyQuerry query=new MyQuerry();
		Filter temp=null;
		Vector<Filter> vecFilter=new Vector<Filter>();
		
		temp=new Filter();
		temp.setQuerryString("status");
		temp.setBooleanvalue(true);
		vecFilter.add(temp);
		
		temp=new Filter();
		temp.setQuerryString("projectName");
		temp.setStringValue(tbProjectName.getValue().trim());
		vecFilter.add(temp);
		
		query.setQuerryObject(new HrProject());
		query.setFilters(vecFilter);
		showWaitSymbol();
		async.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				hideWaitSymbol();
				if(result!=null&&result.size()!=0){
					if(result.size()==1){
						HrProject project=(HrProject) result.get(0);
						int cncId=0;
						if(tbContractId.getValue()!=null&&!tbContractId.getValue().equals("")){
							try{
								cncId=Integer.parseInt(tbContractId.getValue().trim());
							}catch(Exception e){
								
							}
							if(cncId==0&&project.getCncProjectCount()==0){
								tbProjectName.setValue("");
								showDialogMessage("Project with same name already exist.");
							}else if(cncId!=project.getCncProjectCount()){
								tbProjectName.setValue("");
								showDialogMessage("Project with same name already exist.");
							}
						}else{
							if(cncId==0&&project.getCncProjectCount()==0){
								tbProjectName.setValue("");
								showDialogMessage("Project with same name already exist.");
							}else{
								tbProjectName.setValue("");
								showDialogMessage("Project with same name already exist.");
							}
						}
						
					}else{
						tbProjectName.setValue("");
						showDialogMessage("Project with same name already exist.");
					}
					
				}else{
					/** date 25.1.2019 added by komal to check validation in cnc also**/
					MyQuerry query=new MyQuerry();
					Filter temp=null;
					Vector<Filter> vecFilter=new Vector<Filter>();
					
//					temp=new Filter();
//					temp.setQuerryString("status");
//					temp.setBooleanvalue(true);
//					vecFilter.add(temp);
					
					temp=new Filter();
					temp.setQuerryString("projectName");
					temp.setStringValue(tbProjectName.getValue().trim());
					vecFilter.add(temp);
					
					query.setQuerryObject(new CNC());
					query.setFilters(vecFilter);
				
					async.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
						
						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							// TODO Auto-generated method stub
							if(result!=null&&result.size()!=0){
								int cncId = 0;
								if(result.size()==1){
									CNC cnc=(CNC) result.get(0);
									if(!cnc.getStatus().equals(cnc.CANCELLED)){
									if(tbContractId.getValue()!=null && !tbContractId.getValue().equals("")){
										try{
											cncId=Integer.parseInt(tbContractId.getValue().trim());
										}catch(Exception e){
											
										}
										if(cncId!=cnc.getCount()){
											tbProjectName.setValue("");
											showDialogMessage("CNC with this Project name already exist.");
										}
									}else{
											tbProjectName.setValue("");
											showDialogMessage("CNC with this Project name already exist.");
										}
									}
								}else{
									tbProjectName.setValue("");
									showDialogMessage("CNC with this Project name already exist.");
								}
							}	
						}
						
						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							
						}
					});
				}
				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				hideWaitSymbol();
			}
		});
		
	}

	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
		// TODO Auto-generated method stub
		if(event.getSource()==tbProjectName){
			if(!tbProjectName.getValue().equals("")){
				validateProject();
			}
		}
	}
	
	
	/**
	 * @author Anil,Date : 17-01-2019
	 * This method is used to clear the data from form fields used in Tab view
	 */
	
	public void clearTabFormFields(){
		System.out.println("clearTabFormFields");
		
		staffingTable.connectToLocal();
		equipmentRentalTable.connectToLocal();
		consumableTable.connectToLocal();
		otherServiceTable.connectToLocal();
		
		tbManagementFeesPercentage.setValue(0+"");
		tbPaidLeavesDays.setValue(0+"");
		dbFlatManagementFees.setValue(null);
		dbFlatBonus.setValue(null);
		dbFlatTravelling.setValue(null);
		dbFlatOverheadCost.setValue(null);
		
		
		
		
		tbGrossTotalOfStaffing.setText("");
		tbInterestOfMachinary.setValue("");
		tbMaintaniancePercentage.setValue("");
		tbNetPayableOFConsumabables.setValue("");
		tbNetPayableOFOther.setValue("");
		tbManagementFeesValue.setText("");
		tbNetPayableOFStaffing.setText("");
		dbTotalWithoutDiscount.setValue(null);
		dbDiscount.setValue(null);
		tbNetPayableOFEquipment.setValue("");
		
		dbMonthlyFinPlan.setValue(null);
		dbInterestOnFinAmt.setValue(null);
		dbEstimatedRepair.setValue(null);
		dbAdminOhCost.setValue(null);
		dbApproxUniformCost.setValue(null);
		
		oblPaidLeave.setSelectedIndex(0);
		
		
		olbDesignation1.setSelectedIndex(0);
		otRateTbl.connectToLocal();
		lbOtName.setSelectedIndex(0);
		dbFactorRate.setValue(null);
		dbFlatRate.setValue(null);
//		cbTypeDay.setValue(false);
//		cbTypeHourly.setValue(false);
	}

	/**
	 * @author Anil,Date : 17-01-2019
	 * added this method to clear previous data from form fields when we are viewing next CNC
	 */
	@Override
	public void clear() {
		System.out.println("INSIDE CNC FORM CLEAR METHOD");
		// TODO Auto-generated method stub
		super.clear();
		clearTabFormFields();
		
	}
	
	public void changeStatusToCreated()
	{
		this.tbStatus.setValue(SalesOrder.CREATED);		
	}

	public ObjectListBox<CustomerBranchDetails> getOblCustomerBranch() {
		return oblCustomerBranch;
	}

	public void setOblCustomerBranch(
			ObjectListBox<CustomerBranchDetails> oblCustomerBranch) {
		this.oblCustomerBranch = oblCustomerBranch;
	}
	
	/**
	 * @author Anil
	 * @since 23-11-2020
	 * Filtering desigation dropdown list for OT details
	 * designation added in staffing details table should appear 
	 * raised by Rahul Tiwari for Sun facility
	 */
	public void updateDesignationDropDownList() {
		olbDesignation1.clear();
		if(staffingTable.getValue()!=null&&staffingTable.getValue().size()!=0){
			HashSet<String> desgHs=new HashSet<String>();
			for(StaffingDetails obj:staffingTable.getValue()){
				desgHs.add(obj.getDesignation());
			}
			
			
			List<Config> designationList=new ArrayList<Config>();
			for(String obj:desgHs){
				for(Config desg:olbDesignation.getItems()){
					if(obj.equals(desg.getName())){
						designationList.add(desg);
						break;
					}
				}
			}
			if(designationList.size()!=0){
				olbDesignation1.setListItems(designationList);
			}else{
				olbDesignation1.setListItems(olbDesignation.getItems());
			}
		}else{
			olbDesignation1.setListItems(olbDesignation.getItems());
		}
	}
	
	public void reactToUploadConsumable() {
		
		if(!uploadConsumableFlag){
			return;
		}
		for (ScreenMenuConfiguration menu : LoginPresenter.globalScrMenuConf) {
			if (menu.getModuleName().equals(LoginPresenter.currentModule)&& menu.getDocumentName().equals(LoginPresenter.currentDocumentName)) {
				if (menu.getMenuName().equals("Upload")) {
					for (MenuConfiguration confg : menu.getMenuConfigurationList()) {
						if (confg.isStatus()) {
							uploadMap.put(confg.getName(), confg.getLink());
						}
					}
				}
			}
		}
		
		final String url = uploadMap.get(btnUploadConsumable.getText());
		Console.log("URL :: "+url);
		uploadPopup.clear();
		uploadPopup.getHeadingLabel().setText(btnUploadConsumable.getText());
		uploadPopup.setUploadDoc(btnUploadConsumable.getText());
		uploadPopup.setUrl(url);
		
		try{
			uploadPanel.add(uploadPopup);
		}catch(Exception e){
			
		}
		
		uploadPanel.center();
		uploadPanel.show();
	}

	protected void reactToConsumableUploadProcess() {
		Console.log("reactToConsumableUploadProcess()");
		if(personInfoComposite.getValue()==null||personInfoComposite.getIdValue()==-1){
			showDialogMessage("Please select customer.");
			return;
		}
		DataMigrationServiceAsync dmimplSer = GWT.create(DataMigrationService.class);
		Company company = new Company();
		showWaitSymbol();
		dmimplSer.validateConsumables(company.getCompanyId(),personInfoComposite.getIdValue(), new AsyncCallback<ArrayList<Consumables>>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				hideWaitSymbol();
			}

			@Override
			public void onSuccess(ArrayList<Consumables> result) {
				hideWaitSymbol();
				uploadPanel.hide();
				if(result==null){
					showDialogMessage("Invalid file.");
//					hideWaitSymbol();
//					uploadPanel.hide();
					return;
				}
				
				for(Consumables obj:result){
					if(obj.getConsumbales().equals("-1")){
//						hideWaitSymbol();
//						uploadPanel.hide();
						String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
						final String url = gwt + "csvservlet" + "?type=" + 205;
						Window.open(url, "test", "enabled");
						showDialogMessage("Please find error file.");
						return;
					}
				}
				
				if(consumableTable.getValue()!=null&&consumableTable.getValue().size()!=0){
					for(Consumables obj1:consumableTable.getValue()){
						for(int i=0;i<result.size();i++){
							Consumables obj2=result.get(i);
							if(obj1.getSiteLocation()!=null&&obj2.getSiteLocation()!=null&&obj1.getSiteLocation().equals(obj2.getSiteLocation())){
								obj1.setConsumbales(obj2.getConsumbales());
								obj1.setCostCode(obj2.getCostCode());
								obj1.setBranchCarpetArea(obj2.getBranchCarpetArea());
								obj1.setBillingType(obj2.getBillingType());
								obj1.setChargable(obj2.getChargable());
								obj1.setOpsBudget(obj2.getOpsBudget());
								
								result.remove(i);
								i--;
							}
						}
					}
				}
				
				if(result.size()!=0){
					consumableTable.getDataprovider().getList().addAll(result);
					consumableTable.getTable().redraw();
				}
				
			}
		});
	}
	
	//Ashwini Patil Date:19-12-2023
	//Issue : On CNC screen without staffing details CNC is not getting saved. But No error message it is showing.
	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
		boolean superValidate = super.validate();
		if(superValidate==false){
       	 return false;
       }
		if(staffingTable.getDataprovider().getList()==null||staffingTable.getDataprovider().getList().size()==0){
			showDialogMessage("Add staffing details");
			return false;
		}
		
		
		return true;
	}

}
