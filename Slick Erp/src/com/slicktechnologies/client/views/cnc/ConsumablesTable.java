package com.slicktechnologies.client.views.cnc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Vector;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.SelectionCell;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.EmployeeProjectAllocation;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cnc.Consumables;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.productlayer.Category;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;

public class ConsumablesTable extends SuperTable<Consumables>{
	
	TextColumn<Consumables> viewConsumables,viewBillingType,viewChargable,viewOPSBudget,viewDeleteButton,viewSiteLocation,viewBranchCarpetArea,viewCostCode;
	
	Column<Consumables,String> consumables,billingType,chargable,oPSBudget,deleteButton,siteLocation,branchCarpetArea,costCode;
	
	
	ArrayList<String> productList;
	HashMap<String, ItemProduct> productMap;
	
	
	/**
	 * @author Anil @since 09-11-2021
	 * Sunrise SCM
	 */
	boolean siteLocationFlag=false;
	public ArrayList<String> siteLocationList=new ArrayList<String>();
	
	
	public ConsumablesTable() {
		super();
	//	table.setHeight("500px");
		productMap=new HashMap<String, ItemProduct>();
		siteLocationFlag=AppUtility.checkForProcessConfigurartionIsActiveOrNot("CNC", "PC_UploadLocationWiseBudget");
	}


	@Override
	public void createTable() {
		// TODO Auto-generated method stub
		siteLocationFlag=AppUtility.checkForProcessConfigurartionIsActiveOrNot("CNC", "PC_UploadLocationWiseBudget");
		addConsumables();
	}



	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	private void addConsumables() {
		// TODO Auto-generated method stub

		// TODO Auto-generated method stub
		final GenricServiceAsync service = GWT.create(GenricService.class);
		MyQuerry query = new MyQuerry();
		Company c=new Company();
		System.out.println("Company Id :: "+c.getCompanyId());
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		/**
		 * @author Anil @since 09-11-2021
		 * Loading product category instead of product master and then loading product category
		 * for Sunrise SCM
		 */
		filter = new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("isSellCategory");
		filter.setBooleanvalue(true);
		filtervec.add(filter);
		
		
		
		query.setFilters(filtervec);
		
//		query.setQuerryObject(new ItemProduct());
		query.setQuerryObject(new Category());
		
		service.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				productList=new ArrayList<String>();
				productList.add("--SELECT--");
				HashSet<String> productData=new HashSet<String>();
				for (SuperModel model : result) {
//					ItemProduct itemProduct=(ItemProduct) model;
					Category itemProduct=(Category) model;
					
//					if(itemProduct.isStatus()){
//						productData.add(itemProduct.getProductCategory());
						productData.add(itemProduct.getCatName());
						
//						productList.add(productData);
//						productMap.put(itemProduct.getProductName(), itemProduct);
//					}
						
				}
				productList.addAll(productData);
				SelectionCell productListSelection= new SelectionCell(productList);
				consumables = new Column<Consumables, String>(productListSelection) {
					@Override
					public String getValue(Consumables object) {
						// TODO Auto-generated method stub
						if (object.getConsumbales() != null&&!object.getConsumbales().equals("")){
							return object.getConsumbales();
						}else if(productList.contains("Consumable")){
							object.setConsumbales("Consumable");
							return object.getConsumbales();
						}
						return "";
						
					}
				};
				table.addColumn(consumables, "#Consumables");
//				table.setColumnWidth(consumables, 100, Unit.PX);
				
				consumables.setFieldUpdater(new FieldUpdater<Consumables, String>() {
					
					@Override
					public void update(int index, Consumables object, String value) {
						// TODO Auto-generated method stub
						if(value!=null){
							object.setConsumbales(value);
						}
						RowCountChangeEvent.fire(table, index, true);
						table.redrawRow(index);
						
					}
				});
				
				if(siteLocationFlag){
					addSiteLocationCol();
					addCostCodeCol();
					addBranchCarpetAreaCol();
				}
				
				addBillingType();
				addChargable();
				addOPSBudget();
				addDeleteButton();
				setFieldUpdaterOnDelete();
			}
			
			private void setFieldUpdaterOnDelete() {
				// TODO Auto-generated method stub
				deleteButton.setFieldUpdater(new FieldUpdater<Consumables, String>() {
					
					@Override
					public void update(int index, Consumables object, String value) {
						getDataprovider().getList().remove(index);
						table.redrawRow(index);
					}
				});
			}

			private void addOPSBudget() {
				// TODO Auto-generated method stub
				EditTextCell editCell=new EditTextCell();
				oPSBudget=new Column<Consumables, String>(editCell) {
					
					@Override
					public String getValue(Consumables object) {
						// TODO Auto-generated method stub
						if(object.getOpsBudget()!=null){
							return object.getOpsBudget();
						}else {
							return 0+"";
						}
					}
				};

				table.addColumn(oPSBudget, "#OPS Budget");
//				table.setColumnWidth(oPSBudget, 100, Unit.PX);
				
				oPSBudget.setFieldUpdater(new FieldUpdater<Consumables, String>() {
					
					@Override
					public void update(int index, Consumables object, String value) {
						// TODO Auto-generated method stub
						if(value!=null){
							object.setOpsBudget(value);
						}
						RowCountChangeEvent.fire(table, index, true);					
						table.redrawRow(index);
						
					}
				});
			}

			private void addChargable() {
				EditTextCell editCell=new EditTextCell();
				chargable=new Column<Consumables, String>(editCell) {
					
					@Override
					public String getValue(Consumables object) {
						// TODO Auto-generated method stub
						if(object.getChargable()!=null){
							return object.getChargable();
						}else {
							return 0+"";
						}
					}
				};
				table.addColumn(chargable, "#Chargeable");
//				table.setColumnWidth(chargable, 100, Unit.PX);
				
				chargable.setFieldUpdater(new FieldUpdater<Consumables, String>() {
					
					@Override
					public void update(int index, Consumables object, String value) {
						// TODO Auto-generated method stub
						if(value!=null){
							object.setChargable(value);
						}
						RowCountChangeEvent.fire(table, index, true);					
						table.redrawRow(index);
						
					}
				});
			}

			private void addBillingType() {
				// TODO Auto-generated method stub
				ArrayList<String> billingTypeList=new ArrayList<String>();
				billingTypeList.add("--SELECT--");
				billingTypeList.add("FIXED");
				billingTypeList.add("ACTUALS");
				
				SelectionCell productListSelection= new SelectionCell(billingTypeList);
				billingType = new Column<Consumables, String>(productListSelection) {
					@Override
					public String getValue(Consumables object) {
						// TODO Auto-generated method stub
						if (object.getBillingType() != null)
							return object.getBillingType();
						else
							return "";
						
					}
				};
				table.addColumn(billingType, "#BillingType");
//				table.setColumnWidth(billingType, 100, Unit.PX);
				
				billingType.setFieldUpdater(new FieldUpdater<Consumables, String>() {
					
					@Override
					public void update(int index, Consumables object, String value) {
						// TODO Auto-generated method stub
						if(value!=null){
							object.setBillingType(value);
						}
						RowCountChangeEvent.fire(table, index, true);						
						table.redrawRow(index);
						
					}
				});
			}

			private void addDeleteButton() {
				// TODO Auto-generated method stub
				ButtonCell btnCell= new ButtonCell();
				deleteButton = new Column<Consumables, String>(btnCell) {

					@Override
					public String getValue(Consumables object) {
						
						return " - ";
					}
				};
				table.addColumn(deleteButton,"Delete");
//				table.setColumnWidth(deleteButton, 50,Unit.PX);
			}
			

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
	
	}

	protected void viewCostCodeCol() {
		viewCostCode = new TextColumn<Consumables>() {
			@Override
			public String getValue(Consumables object) {
				if(object.getCostCode()!=null){
					return object.getCostCode();
				}else{
					return "";
				}
			}
		};
		table.addColumn(viewCostCode,"Cost Code");
//		table.setColumnWidth(viewCostCode, 90,Unit.PX);
	}


	protected void viewBranchCarpetAreaCol() {
		viewBranchCarpetArea = new TextColumn<Consumables>() {
			@Override
			public String getValue(Consumables object) {
				if(object.getBranchCarpetArea()!=null){
					return object.getBranchCarpetArea();
				}else{
					return "";
				}
			}
		};
		table.addColumn(viewBranchCarpetArea,"Carpet Area");
//		table.setColumnWidth(viewBranchCarpetArea, 90,Unit.PX);		
	}


	protected void viewSiteLocationCol() {
		viewSiteLocation = new TextColumn<Consumables>() {
			@Override
			public String getValue(Consumables object) {
				if(object.getSiteLocation()!=null){
					return object.getSiteLocation();
				}else{
					return "";
				}
			}
		};
		table.addColumn(viewSiteLocation,"Site Location");
//		table.setColumnWidth(viewSiteLocation, 120,Unit.PX);
	}


	protected void addCostCodeCol() {
		EditTextCell editCell=new EditTextCell();
		costCode=new Column<Consumables, String>(editCell) {
			@Override
			public String getValue(Consumables object) {
				if(object.getCostCode()!=null){
					return object.getCostCode();
				}else {
					return "";
				}
			}
		};
		table.addColumn(costCode, "#Cost Code");
//		table.setColumnWidth(costCode, 90, Unit.PX);
		
		costCode.setFieldUpdater(new FieldUpdater<Consumables, String>() {
			@Override
			public void update(int index, Consumables object, String value) {
				if(value!=null){
					object.setCostCode(value);
				}
//				RowCountChangeEvent.fire(table, index, true);					
				table.redrawRow(index);
			}
		});		
	}


	protected void addBranchCarpetAreaCol() {
		EditTextCell editCell=new EditTextCell();
		branchCarpetArea=new Column<Consumables, String>(editCell) {
			@Override
			public String getValue(Consumables object) {
				if(object.getBranchCarpetArea()!=null){
					return object.getBranchCarpetArea();
				}else {
					return "";
				}
			}
		};
		table.addColumn(branchCarpetArea, "#Carpet Area");
//		table.setColumnWidth(branchCarpetArea, 90, Unit.PX);
		
		branchCarpetArea.setFieldUpdater(new FieldUpdater<Consumables, String>() {
			@Override
			public void update(int index, Consumables object, String value) {
				if(value!=null){
					object.setBranchCarpetArea(value);
				}
//				RowCountChangeEvent.fire(table, index, true);					
				table.redrawRow(index);
			}
		});
	}


	protected void addSiteLocationCol() {
		SelectionCell locLis= new SelectionCell(siteLocationList);
		siteLocation = new Column<Consumables, String>(locLis) {
			@Override
			public String getValue(Consumables object) {
				if (object.getSiteLocation() != null)
					return object.getSiteLocation();
				else
					return "";
				
			}
		};
		table.addColumn(siteLocation, "#Site Location");
//		table.setColumnWidth(siteLocation, 120, Unit.PX);
		
		siteLocation.setFieldUpdater(new FieldUpdater<Consumables, String>() {
			@Override
			public void update(int index,Consumables object, String value) {
				if(value!=null){
					object.setSiteLocation(value);
				}
				table.redrawRow(index);
			}
		});
	}


	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		siteLocationFlag=AppUtility.checkForProcessConfigurartionIsActiveOrNot("CNC", "PC_UploadLocationWiseBudget");
		
		int tablecolcount=this.table.getColumnCount();
		for(int i=tablecolcount-1;i>-1;i--)
		  table.removeColumn(i);
		if(state ==true)
			addeditColumn();
		if(state==false)
			addViewColumn();
	
	
	}

	private void addViewColumn() {
		// TODO Auto-generated method stub
		addViewConsumables();
		if(siteLocationFlag){
			viewSiteLocationCol();
			viewCostCodeCol();
			viewBranchCarpetAreaCol();
		}
		addViewBillingType();
		addViewChargable();
		addViewOPSBudget();
//		addViewDeleteButton();
	}

	private void addViewDeleteButton() {
		// TODO Auto-generated method stub
		viewDeleteButton=new TextColumn<Consumables>() {

			@Override
			public String getValue(Consumables object) {
				// TODO Auto-generated method stub
				return "";
			}
		};
		table.addColumn(viewDeleteButton, " ");
//		table.setColumnWidth(viewDeleteButton, 90, Unit.PX);
	}

	private void addViewOPSBudget() {
		// TODO Auto-generated method stub
		viewOPSBudget=new TextColumn<Consumables>() {
			
			@Override
			public String getValue(Consumables object) {
				// TODO Auto-generated method stub
				if(object.getOpsBudget()!=null){
					return object.getOpsBudget();
					
				}else{
					return "";
				}
				
			}
		};
		table.addColumn(viewOPSBudget,"OPS Budget");
//		table.setColumnWidth(viewOPSBudget, 120,Unit.PX);
	}

	private void addViewChargable() {
		// TODO Auto-generated method stub
		viewChargable=new TextColumn<Consumables>() {
			
			@Override
			public String getValue(Consumables object) {
				// TODO Auto-generated method stub
				if(object.getChargable()!=null){
					return object.getChargable();
					
				}else{
					return "";
				}
				
			}
		};

		table.addColumn(viewChargable,"Chargable");
//		table.setColumnWidth(viewChargable, 120,Unit.PX);
	}

	private void addViewBillingType() {
		// TODO Auto-generated method stub
		viewBillingType=new TextColumn<Consumables>() {
			
			@Override
			public String getValue(Consumables object) {
				// TODO Auto-generated method stub
				if(object.getBillingType()!=null){
					return object.getBillingType();
					
				}else{
					return "";
				}
				
			}
		};

		table.addColumn(viewBillingType,"Billing Type");
//		table.setColumnWidth(viewBillingType, 120,Unit.PX);
	}

	private void addViewConsumables() {
		// TODO Auto-generated method stub
		viewConsumables=new TextColumn<Consumables>() {
			
			@Override
			public String getValue(Consumables object) {
				// TODO Auto-generated method stub
				if(object.getConsumbales()!=null){
					return object.getConsumbales();
					
				}else{
					return "";
				}
				
			}
		};
		table.addColumn(viewConsumables,"Consumables");
//		table.setColumnWidth(viewConsumables, 120,Unit.PX);
	}

	private void addeditColumn() {
		// TODO Auto-generated method stub
		addConsumables();
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
