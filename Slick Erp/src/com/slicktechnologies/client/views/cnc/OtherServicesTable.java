package com.slicktechnologies.client.views.cnc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Vector;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.SelectionCell;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cnc.Consumables;
import com.slicktechnologies.shared.common.cnc.EquipmentRental;
import com.slicktechnologies.shared.common.cnc.OtherServices;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTCTemplate;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;

public class OtherServicesTable extends SuperTable<OtherServices>{

	TextColumn<OtherServices> viewotherService,viewchargeableRate,viewdetails,viewfrequencyOfService,viewvendorRate,viewspecialIntruction,viewdeleteButton,viewOtherServiceName;
	
	Column<OtherServices,String> otherService,chargeableRate,details,frequencyOfService,vendorRate,specialIntruction,deleteButton,editOtherServiceName;
	
	ArrayList<String> productList;
	HashMap<String, ServiceProduct> productMap;
	
	public OtherServicesTable() {
		super();
		table.setHeight("300px");
		productList = new ArrayList<String>();
		productMap = new HashMap<String, ServiceProduct>();
	}
	@Override
	public void createTable() {
		// TODO Auto-generated method stub
		addotherService();
//		addchargeableRate();
//		adddetails();
//		addfrequencyOfService();
//		addvendorRate();
//		();
//		adddeleteButton();
//		setFieldUpdaterOnDelete();
	}

	private void setFieldUpdaterOnDelete() {
		// TODO Auto-generated method stub
		deleteButton.setFieldUpdater(new FieldUpdater<OtherServices, String>() {
			
			@Override
			public void update(int index, OtherServices object, String value) {
				getDataprovider().getList().remove(index);
				table.redrawRow(index);
			}
		});
	}
	
	private void adddeleteButton() {
		// TODO Auto-generated method stub
		ButtonCell btnCell= new ButtonCell();
		deleteButton = new Column<OtherServices, String>(btnCell) {

			@Override
			public String getValue(OtherServices object) {
				
				return " - ";
			}
		};
		table.addColumn(deleteButton,"Delete");
		table.setColumnWidth(deleteButton, 50,Unit.PX);
	}

	private void addspecialIntruction() {
		// TODO Auto-generated method stub
		EditTextCell editCell = new EditTextCell();
		specialIntruction = new Column<OtherServices, String>(editCell) {

			@Override
			public String getValue(OtherServices object) {
				// TODO Auto-generated method stub
				if (object.getSpecialInformation() != null) {
					return object.getSpecialInformation();
				} else {
					return "";
				}
			}
		};
		table.addColumn(specialIntruction, "#Special Instruction");
		table.setColumnWidth(specialIntruction, 100, Unit.PX);
		
		specialIntruction.setFieldUpdater(new FieldUpdater<OtherServices, String>() {

			@Override
			public void update(int index, OtherServices object, String value) {
				// TODO Auto-generated method stub
				if(value!=null){
					object.setSpecialInformation(value);
				}else {
					object.setSpecialInformation("");
				}
				table.redrawRow(index);
			}
		});
	
	
	}

	private void addvendorRate() {
		// TODO Auto-generated method stub
		EditTextCell editCell = new EditTextCell();
		vendorRate = new Column<OtherServices, String>(editCell) {

			@Override
			public String getValue(OtherServices object) {
				// TODO Auto-generated method stub
				if (object.getVendorRates() != 0) {
					return object.getVendorRates()+"";
				} else {
					return 0+"";
				}
			}
		};
		table.addColumn(vendorRate, "#Vendor Rate");
		table.setColumnWidth(vendorRate, 100, Unit.PX);
		
		vendorRate.setFieldUpdater(new FieldUpdater<OtherServices, String>() {

			@Override
			public void update(int index, OtherServices object, String value) {
				// TODO Auto-generated method stub
				if(value!=null){
					object.setVendorRates(Double.parseDouble(value));
				}else {
					object.setVendorRates(0);
				}
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				table.redrawRow(index);
			}
		});
	
	
	}

	private void addfrequencyOfService() {
		// TODO Auto-generated method stub
		EditTextCell editCell = new EditTextCell();
		frequencyOfService = new Column<OtherServices, String>(editCell) {

			@Override
			public String getValue(OtherServices object) {
				// TODO Auto-generated method stub
				if (object.getFrequencyOfServices() != null) {
					return object.getFrequencyOfServices()+"";
				} else {
					return "";
				}
			}
		};
		table.addColumn(frequencyOfService, "#Frequency");
		table.setColumnWidth(frequencyOfService, 100, Unit.PX);
		
		frequencyOfService.setFieldUpdater(new FieldUpdater<OtherServices, String>() {

			@Override
			public void update(int index, OtherServices object, String value) {
				// TODO Auto-generated method stub
				if(value!=null){
					object.setFrequencyOfServices(value);
				}else {
					object.setFrequencyOfServices("");
				}
				table.redrawRow(index);
			}
		});
	
	}

	private void adddetails() {
		// TODO Auto-generated method stub
		EditTextCell editCell = new EditTextCell();
		details = new Column<OtherServices, String>(editCell) {

			@Override
			public String getValue(OtherServices object) {
				// TODO Auto-generated method stub
				if (object.getDetails() != null) {
					return object.getDetails()+"";
				} else {
					return "";
				}
			}
		};
		table.addColumn(details, "#Details");
		table.setColumnWidth(details, 100, Unit.PX);
		
		details.setFieldUpdater(new FieldUpdater<OtherServices, String>() {

			@Override
			public void update(int index, OtherServices object, String value) {
				// TODO Auto-generated method stub
				if(value!=null){
					object.setDetails(value);
				}else {
					object.setDetails("");
				}
				table.redrawRow(index);
			}
		});
	}

	private void addchargeableRate() {
		// TODO Auto-generated method stub
		EditTextCell editCell = new EditTextCell();
		chargeableRate = new Column<OtherServices, String>(editCell) {

			@Override
			public String getValue(OtherServices object) {
				// TODO Auto-generated method stub
				if (object.getChargableRates() != 0) {
					return object.getChargableRates()+"";
				} else {
					return 0 + "";
				}
			}
		};
		table.addColumn(chargeableRate, "#Chargeable Rates");
		table.setColumnWidth(chargeableRate, 100, Unit.PX);
		
		
		chargeableRate.setFieldUpdater(new FieldUpdater<OtherServices, String>() {

			@Override
			public void update(int index, OtherServices object, String value) {
				// TODO Auto-generated method stub
				if(value!=null){
					try{
						object.setChargableRates(Double.parseDouble(value));
					}catch(Exception e){
						object.setChargableRates(0);
					}
				}else {
					object.setChargableRates(0);
				}
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				table.redrawRow(index);
			}
		});
	
	}

	private void addotherService() {
		final GenricServiceAsync service = GWT.create(GenricService.class);
		MyQuerry query = new MyQuerry();
		Company c = new Company();
		System.out.println("Company Id :: " + c.getCompanyId());

		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;

		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		query.setFilters(filtervec);

		query.setQuerryObject(new ServiceProduct());

		service.getSearchResult(query,
				new AsyncCallback<ArrayList<SuperModel>>() {

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						productList.clear();
						productList.add("--SELECT--");
						for (SuperModel model : result) {
							ServiceProduct serviceProduct = (ServiceProduct) model;
							if (serviceProduct.isStatus()) {
								productList.add(serviceProduct.getProductName());
								productMap.put(serviceProduct.getProductName(),serviceProduct);
							}
						}
						Comparator<String> compare = new  Comparator<String>() {
							
							@Override
							public int compare(String o1, String o2) {
								// TODO Auto-generated method stub
								return o1.compareTo(o2);
							}
						};
						Collections.sort(productList,compare);

						SelectionCell productListSelection = new SelectionCell(
								productList);
						
						otherService = new Column<OtherServices, String>(productListSelection) {
				
							@Override
							public String getValue(OtherServices object) {
								// TODO Auto-generated method stub
								if (object.getOtherService() != null) {
									return object.getOtherService();
								} else {
									return "";
								}
							}
						};
						table.addColumn(otherService, "#Other Services");
						table.setColumnWidth(otherService, 100, Unit.PX);
						
						otherService.setFieldUpdater(new FieldUpdater<OtherServices, String>() {
				
							@Override
							public void update(int index, OtherServices object, String value) {
								// TODO Auto-generated method stub

								if (value.equalsIgnoreCase("--SELECT--")) {
									object.setOtherService("");
								} else {
									object.setOtherService(value);
								}
								ServiceProduct product=productMap.get(value);
								if(product !=null){
									/**
									 * @author Anil
									 * @since 08-09-2020
									 * setting upload data to null to avoid data corrupt and mismatch of line item
									 */
									product.setTermsAndConditions(new DocumentUpload());
									product.setProductImage(new DocumentUpload());
									product.setProductImage1(new DocumentUpload());
									product.setProductImage2(new DocumentUpload());
									product.setProductImage3(new DocumentUpload());
									product.setComment("");
									product.setCommentdesc("");
									product.setCommentdesc1("");
									product.setCommentdesc2("");
									
									object.setServiceProduct(product);
									object.setChargableRates(product.getPrice());
								}
								RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
								table.redrawRow(index);
							}
						});
						addEditOtherServiceName();
						addchargeableRate();
						adddetails();
						addfrequencyOfService();
						addvendorRate();
						addspecialIntruction();
						adddeleteButton();
						setFieldUpdaterOnDelete();
					}

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub

					}
				});
	
	}
//		EditTextCell editCell = new EditTextCell();
//		otherService = new Column<OtherServices, String>(editCell) {
//
//			@Override
//			public String getValue(OtherServices object) {
//				// TODO Auto-generated method stub
//				if (object.getOtherService() != null) {
//					return object.getOtherService();
//				} else {
//					return "";
//				}
//			}
//		};
//		table.addColumn(otherService, "#Other Services");
//		table.setColumnWidth(otherService, 100, Unit.PX);
//		
//		otherService.setFieldUpdater(new FieldUpdater<OtherServices, String>() {
//
//			@Override
//			public void update(int index, OtherServices object, String value) {
//				// TODO Auto-generated method stub
//				if(value!=null)
//					object.setOtherService(value);
//				else 
//					object.setOtherService("");
//				
//				table.redrawRow(index);
//			}
//		});
//	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		int tablecolcount=this.table.getColumnCount();
		for(int i=tablecolcount-1;i>-1;i--)
		  table.removeColumn(i);
		if(state ==true)
			addotherService();
		if(state==false)
			addViewColumn();
	
	
	
	}

	private void addViewColumn() {
		// TODO Auto-generated method stub
		addViewotherService();
		addViewOtherServiceName();
		addViewchargeableRate();
		addViewdetails();
		addViewfrequencyOfService();
		addViewvendorRate();
		addViewspecialIntruction();
		addViewdeleteButton();
	}

	private void addViewdeleteButton() {
		// TODO Auto-generated method stub
		viewdeleteButton=new TextColumn<OtherServices>() {

			@Override
			public String getValue(OtherServices object) {
				// TODO Auto-generated method stub
				return "";
			}
		};

		table.addColumn(viewdeleteButton,"");
		table.setColumnWidth(viewdeleteButton, 120,Unit.PX);
	}

	private void addViewspecialIntruction() {
		// TODO Auto-generated method stub
		viewspecialIntruction=new TextColumn<OtherServices>() {

			@Override
			public String getValue(OtherServices object) {
				// TODO Auto-generated method stub
				return object.getSpecialInformation();
			}
		};
		table.addColumn(viewspecialIntruction,"Special Instruction");
		table.setColumnWidth(viewspecialIntruction, 120,Unit.PX);
	}

	private void addViewvendorRate() {
		// TODO Auto-generated method stub
		viewvendorRate=new TextColumn<OtherServices>() {

			@Override
			public String getValue(OtherServices object) {
				// TODO Auto-generated method stub
				return object.getVendorRates()+"";
			}
		};

		table.addColumn(viewvendorRate,"Vendor Rate");
		table.setColumnWidth(viewvendorRate, 120,Unit.PX);
	}

	private void addViewfrequencyOfService() {
		// TODO Auto-generated method stub
		viewfrequencyOfService=new TextColumn<OtherServices>() {

			@Override
			public String getValue(OtherServices object) {
				// TODO Auto-generated method stub
				return object.getFrequencyOfServices()+"";
			}
		};

		table.addColumn(viewfrequencyOfService,"Frequency");
		table.setColumnWidth(viewfrequencyOfService, 120,Unit.PX);
	}

	private void addViewdetails() {
		// TODO Auto-generated method stub
		viewdetails=new TextColumn<OtherServices>() {

			@Override
			public String getValue(OtherServices object) {
				// TODO Auto-generated method stub
				return object.getDetails()+"";
			}
		};

		table.addColumn(viewdetails,"View Details");
		table.setColumnWidth(viewdetails, 120,Unit.PX);
	}

	private void addViewchargeableRate() {
		// TODO Auto-generated method stub
		viewchargeableRate=new TextColumn<OtherServices>() {

			@Override
			public String getValue(OtherServices object) {
				// TODO Auto-generated method stub
				return object.getChargableRates()+"";
			}
		};

		table.addColumn(viewchargeableRate,"Chargable Rate");
		table.setColumnWidth(viewchargeableRate, 120,Unit.PX);
	}

	private void addViewotherService() {
		// TODO Auto-generated method stub
		viewotherService=new TextColumn<OtherServices>() {

			@Override
			public String getValue(OtherServices object) {
				// TODO Auto-generated method stub
				return object.getOtherService()+"";
			}
		};

		table.addColumn(viewotherService,"Other Service");
		table.setColumnWidth(viewotherService, 120,Unit.PX);
	}

	private void addeditColumn() {
		// TODO Auto-generated method stub
		addotherService();
		final GenricServiceAsync service = GWT.create(GenricService.class);
		MyQuerry query = new MyQuerry();
		Company c = new Company();
		System.out.println("Company Id :: " + c.getCompanyId());

		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;

		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		query.setFilters(filtervec);

		query.setQuerryObject(new ServiceProduct());

		service.getSearchResult(query,
				new AsyncCallback<ArrayList<SuperModel>>() {

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						productList.clear();
						productList.add("--SELECT--");
						for (SuperModel model : result) {
							ServiceProduct serviceProduct = (ServiceProduct) model;
							if (serviceProduct.isStatus()) {
								productList.add(serviceProduct.getProductName());
								productMap.put(serviceProduct.getProductName(),serviceProduct);
							}
						}
						Comparator<String> compare = new  Comparator<String>() {
							
							@Override
							public int compare(String o1, String o2) {
								// TODO Auto-generated method stub
								return o1.compareTo(o2);
							}
						};
						Collections.sort(productList,compare);

						SelectionCell productListSelection = new SelectionCell(
								productList);
						
						otherService = new Column<OtherServices, String>(productListSelection) {
				
							@Override
							public String getValue(OtherServices object) {
								// TODO Auto-generated method stub
								if (object.getOtherService() != null) {
									return object.getOtherService();
								} else {
									return "";
								}
							}
						};
						table.addColumn(otherService, "#Other Services");
						table.setColumnWidth(otherService, 100, Unit.PX);
						
						otherService.setFieldUpdater(new FieldUpdater<OtherServices, String>() {
				
							@Override
							public void update(int index, OtherServices object, String value) {
								// TODO Auto-generated method stub

								if (value.equalsIgnoreCase("--SELECT--")) {
									object.setOtherService("");
								} else {
									object.setOtherService(value);
								}
								ServiceProduct product=productMap.get(value);
								if(product !=null){
									object.setServiceProduct(product);
									object.setChargableRates(product.getPrice());
								}
								table.redrawRow(index);
							}
						});
						addEditOtherServiceName();
						addchargeableRate();
						adddetails();
						addfrequencyOfService();
						addvendorRate();
						addspecialIntruction();
						adddeleteButton();	
					}

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub

					}
				});
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
	private void addViewOtherServiceName() {
		// TODO Auto-generated method stub
		viewOtherServiceName=new TextColumn<OtherServices>() {

			@Override
			public String getValue(OtherServices object) {
				// TODO Auto-generated method stub
				return object.getOtherService()+"";
			}
		};

		table.addColumn(viewOtherServiceName,"Name");
		table.setColumnWidth(viewOtherServiceName, 120,Unit.PX);
	}
	private void addEditOtherServiceName() {
		// TODO Auto-generated method stub
		EditTextCell editCell = new EditTextCell();
		editOtherServiceName = new Column<OtherServices, String>(editCell) {

			@Override
			public String getValue(OtherServices object) {
				// TODO Auto-generated method stub
				if (object.getOtherService() != null) {
					return object.getOtherService()+"";
				} else {
					return "";
				}
			}
		};
		table.addColumn(editOtherServiceName, "#Name");
		table.setColumnWidth(editOtherServiceName, 100, Unit.PX);
				
		editOtherServiceName.setFieldUpdater(new FieldUpdater<OtherServices, String>() {

			@Override
			public void update(int index, OtherServices object, String value) {
				// TODO Auto-generated method stub
				if(value!=null){
					object.setOtherService(value);					
				}else {
					object.setOtherService("");
				}
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				table.redrawRow(index);
			}
		});
	
	}
	
}
