package com.slicktechnologies.client.views.cnc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Vector;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.SelectionCell;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cnc.EquipmentRental;
import com.slicktechnologies.shared.common.cnc.StaffingDetails;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTCTemplate;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;

public class EquipmentRentalTable extends SuperTable<EquipmentRental> {

	TextColumn<EquipmentRental> viewMachine, viewBrand, viewmarketPrice,
			viewQty, viewInditotalPrice, viewnoOFMonthsForRental,
			viewRentalOnEquipment, viewNoOFMonthsForRental,viewInterestOnMachineryPercent,
			viewInterestOnMachinery,viewMaitanienceandInsurancePercent,viewMaitanienceandInsurance, viewTotal,
			viewDeleteButton,/** date  31.8.2018 **/viewDiscount , viewTotalWithoutDiscount,viewPurchasePrice;

	Column<EquipmentRental, String> machine, brand, marketPrice, qty,
			totalPrice, noOFMonthsForRental, rentalOnEquipment,interestOnMachineryPercent,
			interestOnMachinery,maitanienceandInsurancePercent, maitanienceandInsurance, total, deleteButton,
			/** date  31.8.2018 **/editDiscount , editTotalWithoutDiscount,editPurchasePrice;

	ArrayList<String> productList;
	HashMap<String, ItemProduct> productMap;

	public EquipmentRentalTable() {
		// TODO Auto-generated constructor stub
		super();
		table.setHeight("300px");
		productList = new ArrayList<String>();
		productMap = new HashMap<String, ItemProduct>();
	}

	@Override
	public void createTable() {
		// TODO Auto-generated method stub
		addMachince();
	}

	private void addMachince() {
		// TODO Auto-generated method stub
		final GenricServiceAsync service = GWT.create(GenricService.class);
		MyQuerry query = new MyQuerry();
		Company c = new Company();
		System.out.println("Company Id :: " + c.getCompanyId());

		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;

		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		query.setFilters(filtervec);
		query.setQuerryObject(new ItemProduct());

		service.getSearchResult(query,
				new AsyncCallback<ArrayList<SuperModel>>() {

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						productList.clear();
						productList.add("--SELECT--");
						for (SuperModel model : result) {
							ItemProduct itemProduct = (ItemProduct) model;
							if (itemProduct.isStatus()) {
								productList.add(itemProduct.getProductName()+"/"+itemProduct.getProductCode());
								productMap.put(itemProduct.getProductName()+"/"+itemProduct.getProductCode(),itemProduct);
							}
						}
						Comparator<String> compare = new  Comparator<String>() {
							@Override
							public int compare(String o1, String o2) {
								// TODO Auto-generated method stub
								return o1.compareTo(o2);
							}
						};
						Collections.sort(productList,compare);

						SelectionCell productListSelection = new SelectionCell(productList);
						machine = new Column<EquipmentRental, String>(productListSelection) {
							@Override
							public String getValue(EquipmentRental object) {
								// TODO Auto-generated method stub
								if (object.getMachine() != null)
									return object.getMachine();
								else
									return "";

							}
						};
						table.addColumn(machine, "#Machine");
						table.setColumnWidth(machine, 100, Unit.PX);

						machine.setFieldUpdater(new FieldUpdater<EquipmentRental, String>() {

							@Override
							public void update(int index,EquipmentRental object, String value) {
								// TODO Auto-generated method stub
								if (value != null) {
									object.setMachine(value);
								}
								ItemProduct itemProduct = productMap.get(value);
								if (itemProduct.getBrandName() != null)
									object.setBrand(itemProduct.getBrandName());
								if (itemProduct.getPrice() != 0)
									object.setMarketPrice(itemProduct.getPrice());
								/**4-3-2019 added by Amol **/
								if(itemProduct.getModel()!=null)
									object.setModelNumber(itemProduct.getModel());
								else
									object.setMarketPrice(0);
								object.setQty(1d);
								object.setIndividual_total(object.getQty()* object.getMarketPrice());
								object.setProductId(itemProduct.getCount());
								object.setProductCode(itemProduct.getProductCode());
								object.setModelNumber(itemProduct.getModel());
//								object.setRentalEquipmentPerMonth(((object
//										.getQty() * object.getMarketPrice()) / Double
//										.parseDouble(object.getNoOFMonths()
//												.trim()))
//										+ "");
								
								/**
								 * @author Anil , Date : 11-10-2019
								 */
								object.setPurchasePrice(itemProduct.getPurchasePrice());

								updateRow(object, index);

							}

						});

						addBrandColumn();
						addPurchasePrice();
						addMarketPrice();
						addQtyColumn();
						addIndividualTotal();
						addNoOFMonthsForRental();
						addRentalOnEquipPerMonth();
						addInterestOnMacPerMonthPercent();
						addInterestOnMacPerMonth();
						addMarketingAndInsurancePercent();
						addMarketingAndInsurance();
						/** date 31.8.2018 added by komal**/
						addTotalWithoutDiscount();
						EditDiscount();
						/** end komal**/
						addTotal();
						addDeleteButton();
						setFieldUpdaterOnDelete();
					}

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub

					}
				});
	}

	protected void addMarketingAndInsurancePercent() {
		// TODO Auto-generated method stub
		EditTextCell editCell = new EditTextCell();
		maitanienceandInsurancePercent = new Column<EquipmentRental, String>(editCell) {
			@Override
			public String getValue(EquipmentRental object) {
				if (object.getMaintanancePercentage() == 0) {
					return 0 + "";
				} else {
					return object.getMaintanancePercentage() + "";
				}
			}
		};
		table.addColumn(maitanienceandInsurancePercent, "#Maintanance Percent");
		table.setColumnWidth(maitanienceandInsurancePercent, 120, Unit.PX);

		maitanienceandInsurancePercent
				.setFieldUpdater(new FieldUpdater<EquipmentRental, String>() {

					@Override
					public void update(int index, EquipmentRental object,
							String value) {
						// TODO Auto-generated method stub
						if (value != null) {
							object.setMaintanancePercentage(Double.parseDouble(value));
						}
						updateRow(object, index);
					}
				});
	}

	protected void addInterestOnMacPerMonthPercent() {
		// TODO Auto-generated method stub
		EditTextCell editCell = new EditTextCell();
		interestOnMachineryPercent = new Column<EquipmentRental, String>(editCell) {
			@Override
			public String getValue(EquipmentRental object) {
				if (object.getMachineryInterest() == 0) {
					return 0 + "";
				} else {
					return object.getMachineryInterest() + "";
				}
			}
		};
		table.addColumn(interestOnMachineryPercent, "#Interest on Machinery Percent");
		table.setColumnWidth(interestOnMachineryPercent, 120, Unit.PX);

		interestOnMachineryPercent
				.setFieldUpdater(new FieldUpdater<EquipmentRental, String>() {

					@Override
					public void update(int index, EquipmentRental object,
							String value) {
						// TODO Auto-generated method stub
						if (value != null) {
							object.setMachineryInterest(Double.parseDouble(value));
						}
						updateRow(object, index);
					}
				});
	}

	private void setFieldUpdaterOnDelete() {
		// TODO Auto-generated method stub
		deleteButton
				.setFieldUpdater(new FieldUpdater<EquipmentRental, String>() {

					@Override
					public void update(int index, EquipmentRental object,
							String value) {
						getDataprovider().getList().remove(index);
						table.redrawRow(index);
					}
				});
	}

	private void addDeleteButton() {
		// TODO Auto-generated method stub
		ButtonCell btnCell = new ButtonCell();
		deleteButton = new Column<EquipmentRental, String>(btnCell) {

			@Override
			public String getValue(EquipmentRental object) {

				return " - ";
			}
		};
		table.addColumn(deleteButton, "Delete");
		table.setColumnWidth(deleteButton, 50, Unit.PX);
	}

	private void addTotal() {
		// TODO Auto-generated method stub
		viewTotal = new TextColumn<EquipmentRental>() {

			@Override
			public String getValue(EquipmentRental object) {
				// TODO Auto-generated method stub
				if (object.getTotal() != null) {
					return object.getTotal() + "";
				} else {
					return "";
				}
			}
		};
		table.addColumn(viewTotal, "Total");
		table.setColumnWidth(viewTotal, 120, Unit.PX);

	}

	private void addMarketingAndInsurance() {
		// TODO Auto-generated method stub

		// TODO Auto-generated method stub
		EditTextCell editCell = new EditTextCell();
		maitanienceandInsurance = new Column<EquipmentRental, String>(editCell) {
			@Override
			public String getValue(EquipmentRental object) {
				if (object.getMaintananceAndInsurancePerMonth() == null) {
					return 0 + "";
				} else {
					return object.getMaintananceAndInsurancePerMonth() + "";
				}
			}
		};
		table.addColumn(maitanienceandInsurance, "#Maintanance / \nMonth (Amt)");
		table.setColumnWidth(maitanienceandInsurance, 120, Unit.PX);

		maitanienceandInsurance
				.setFieldUpdater(new FieldUpdater<EquipmentRental, String>() {

					@Override
					public void update(int index, EquipmentRental object,
							String value) {
						// TODO Auto-generated method stub
						if (value != null) {
							object.setMaintananceAndInsurancePerMonth(value);
						}
//						double totalValue = object.getIndividual_total()
//								+ Double.parseDouble(object
//										.getRentalEquipmentPerMonth());
//						double totalValueWithPercent = ((totalValue / 100)
//								* Double.parseDouble(object
//										.getInterestOnMachineryPerMonth()
//										.trim()) + totalValue);
//						double totalValueWithMain = totalValueWithPercent
//								+ Double.parseDouble(object
//										.getMaintananceAndInsurancePerMonth()); 
//						object.setTotal(totalValueWithMain + "");
//						table.redrawRow(index);
						double totalValue = Double.parseDouble(object.getMaintananceAndInsurancePerMonth())+Double.parseDouble(object.getInterestOnMachineryPerMonth()) + Double.parseDouble(object.getRentalEquipmentPerMonth());//rentalPerMonthValue;
						double grandTotal = totalValue;
						if(object.getDiscount()!=0){
							grandTotal = totalValue - object.getDiscount();
						}
						object.setTotalWithoutDiscount(Math.round(totalValue));
						object.setTotal(grandTotal + "");
						RowCountChangeEvent.fire(table, index, true);
//						updateRow(object, index);
					}
				});

	}

	private void addInterestOnMacPerMonth() {
		// TODO Auto-generated method stub
		EditTextCell editCell = new EditTextCell();
		interestOnMachinery = new Column<EquipmentRental, String>(editCell) {
			@Override
			public String getValue(EquipmentRental object) {
				if (object.getInterestOnMachineryPerMonth() == null) {
					return 0 + "";
				} else {
					return object.getInterestOnMachineryPerMonth() + "";
				}
			}
		};
		table.addColumn(interestOnMachinery, "#Interest / \nMonth(Amt)");
		table.setColumnWidth(interestOnMachinery, 120, Unit.PX);

		interestOnMachinery
				.setFieldUpdater(new FieldUpdater<EquipmentRental, String>() {

					@Override
					public void update(int index, EquipmentRental object,
							String value) {
						// TODO Auto-generated method stub
						if (value != null) {
							object.setInterestOnMachineryPerMonth(value);
						}
						// double
						// totalValue=object.getIndividual_total()+object.getQty();
						// double
						// totalValueWithPercent=((totalValue/100)*Double.parseDouble(object.getInterestOnMachineryPerMonth().trim())+totalValue);
						// double
						// totalValueWithMain=totalValueWithPercent+Double.parseDouble(object.getMaintananceAndInsurancePerMonth());
						// object.setTotal(totalValueWithMain+"");
						double totalValue = Double.parseDouble(object.getMaintananceAndInsurancePerMonth())+Double.parseDouble(object.getInterestOnMachineryPerMonth()) + Double.parseDouble(object.getRentalEquipmentPerMonth());//rentalPerMonthValue;
					//	object.setTotal(totalValue + "");
						double grandTotal = totalValue;
						if(object.getDiscount()!=0){
							grandTotal = totalValue - object.getDiscount();
						}
						object.setTotalWithoutDiscount(Math.round(totalValue));
						object.setTotal(grandTotal + "");
						RowCountChangeEvent.fire(table, index, true);
//						updateRow(object, index);
						
					}
				});
	}

	private void addRentalOnEquipPerMonth() {
		// TODO Auto-generated method stub
		EditTextCell editCell = new EditTextCell();
		rentalOnEquipment = new Column<EquipmentRental, String>(editCell) {
			@Override
			public String getValue(EquipmentRental object) {
				if (object.getRentalEquipmentPerMonth() == null) {
					return 0 + "";
				} else {
					return object.getRentalEquipmentPerMonth() + "";
				}
			}
		};
		table.addColumn(rentalOnEquipment, "#Rental / \nMonth");
		table.setColumnWidth(rentalOnEquipment, 120, Unit.PX);

		rentalOnEquipment
				.setFieldUpdater(new FieldUpdater<EquipmentRental, String>() {

					@Override
					public void update(int index, EquipmentRental object,
							String value) {
						// TODO Auto-generated method stub
						if (value != null) {
							object.setRentalEquipmentPerMonth(value);
						}
//
//						double rentalForMonth = 0;
//						double interestOnMachince = 0;
//						double maintanance = 0;
//						try {
//							rentalForMonth = Double.parseDouble(object
//									.getRentalEquipmentPerMonth());
//						} catch (Exception e) {
//							rentalForMonth = 0;
//						}
//						try {
//							interestOnMachince = Double.parseDouble(object
//									.getInterestOnMachineryPerMonth());
//						} catch (Exception e) {
//							interestOnMachince = 0;
//						}
//						try {
//							maintanance = Double.parseDouble(object
//									.getMaintananceAndInsurancePerMonth());
//						} catch (Exception e) {
//							maintanance = 0;
//						}
//
//						object.setTotal((rentalForMonth + interestOnMachince + maintanance)
//								+ "");
//						table.redrawRow(index);
						updateRow(object, index);
					}
				});
	}

	private void addIndividualTotal() {
		// TODO Auto-generated method stub
		viewInditotalPrice = new TextColumn<EquipmentRental>() {

			@Override
			public String getValue(EquipmentRental object) {
				// TODO Auto-generated method stub
				if (object.getIndividual_total() != 0) {
					return object.getIndividual_total() + "";
				} else {
					return "";
				}
			}
		};
		table.addColumn(viewInditotalPrice, "Total Price");
		table.setColumnWidth(viewInditotalPrice, 120, Unit.PX);
	}

	private void addQtyColumn() {
		// TODO Auto-generated method stub
		EditTextCell editCell = new EditTextCell();
		qty = new Column<EquipmentRental, String>(editCell) {
			@Override
			public String getValue(EquipmentRental object) {
				if (object.getQty() == 0) {
					return 0 + "";
				} else {
					return object.getQty() + "";
				}
			}
		};
		table.addColumn(qty, "#Qty");
		table.setColumnWidth(qty, 120, Unit.PX);

		qty.setFieldUpdater(new FieldUpdater<EquipmentRental, String>() {

			@Override
			public void update(int index, EquipmentRental object, String value) {
				// TODO Auto-generated method stub
				if (!value.trim().equals("")) {
					try {
						object.setQty(Double.parseDouble(value.trim()));
					} catch (Exception e) {
						object.setQty(0);
					}
				} else {
					object.setQty(0);
				}
				// double totalAmout=object.getQty()*object.getMarketPrice();
				// object.setIndividual_total(totalAmout);
				// // CNCForm form=new CNCForm();
				// double
				// monthForRental=0,interestOfMachinary=0,maintanancePercentage=0;
				// monthForRental=Double.parseDouble(object.getNoOFMonths());
				// try{
				// interestOfMachinary=object.getMachineryInterest();//Double.parseDouble(form.getTbInterestOfMachinary().getValue());
				// }catch(Exception e){
				// interestOfMachinary=0;
				// }
				// try{
				// maintanancePercentage=object.getMaintanancePercentage();//Double.parseDouble(form.getTbMaintaniancePercentage().getValue());
				// }catch(Exception e){
				// maintanancePercentage=0;
				// }
				// object.setRentalEquipmentPerMonth((totalAmout/monthForRental)+"");
				// object.setInterestOnMachineryPerMonth((((totalAmout/100)*interestOfMachinary)/12)+"");
				// object.setMaintananceAndInsurancePerMonth((((totalAmout/100)*maintanancePercentage)/12)+"");
				// object.setTotal(((totalAmout/monthForRental)+(((totalAmout/100)*interestOfMachinary)/12)+(((totalAmout/100)*maintanancePercentage)/12))+"");
				updateRow(object, index);
				// RowCountChangeEvent.fire(table, index, true);
				// table.redrawRow(index);
			}
		});

	}
	
	private void addPurchasePrice() {
		// TODO Auto-generated method stub
		EditTextCell editCell = new EditTextCell();
		editPurchasePrice = new Column<EquipmentRental, String>(editCell) {
			@Override
			public String getValue(EquipmentRental object) {
				if (object.getPurchasePrice() == 0) {
					return 0 + "";
				} else {
					return object.getPurchasePrice() + "";
				}
			}
		};
		table.addColumn(editPurchasePrice, "#Purchase Price");
		table.setColumnWidth(editPurchasePrice, 120, Unit.PX);

		editPurchasePrice.setFieldUpdater(new FieldUpdater<EquipmentRental, String>() {

			@Override
			public void update(int index, EquipmentRental object,
					String value) {
				// TODO Auto-generated method stub
				if (!value.trim().equals("")) {
					try {
						object.setPurchasePrice(Double.parseDouble(value.trim()));
					} catch (Exception e) {
						object.setPurchasePrice(0);
					}
				} else {
					object.setPurchasePrice(0);
				}
				updateRow(object, index);
			}
		});

	}
	
	private void addMarketPrice() {
		// TODO Auto-generated method stub
		EditTextCell editCell = new EditTextCell();
		marketPrice = new Column<EquipmentRental, String>(editCell) {
			@Override
			public String getValue(EquipmentRental object) {
				if (object.getMarketPrice() == 0) {
					return 0 + "";
				} else {
					return object.getMarketPrice() + "";
				}
			}
		};
		table.addColumn(marketPrice, "#Price");
		table.setColumnWidth(marketPrice, 120, Unit.PX);

		marketPrice
				.setFieldUpdater(new FieldUpdater<EquipmentRental, String>() {

					@Override
					public void update(int index, EquipmentRental object,
							String value) {
						// TODO Auto-generated method stub
						if (!value.trim().equals("")) {
							try {
								object.setMarketPrice(Double.parseDouble(value
										.trim()));
							} catch (Exception e) {
								object.setMarketPrice(0);
							}
						} else {
							object.setMarketPrice(0);
						}
						updateRow(object, index);
					}
				});

	}

	private void addBrandColumn() {
		// TODO Auto-generated method stub
		viewBrand = new TextColumn<EquipmentRental>() {

			@Override
			public String getValue(EquipmentRental object) {
				// TODO Auto-generated method stub
				if (object.getBrand() != null)
					return object.getBrand();
				else
					return "";
			}
		};
	}

	protected void addNoOFMonthsForRental() {
		// TODO Auto-generated method stub
		EditTextCell editCell = new EditTextCell();
		noOFMonthsForRental = new Column<EquipmentRental, String>(editCell) {
			@Override
			public String getValue(EquipmentRental object) {
				if (object.getNoOFMonths() == null) {
					return 0 + "";
				} else {
					return object.getNoOFMonths() + "";
				}
			}
		};
		table.addColumn(noOFMonthsForRental, "#No Of Months");
		table.setColumnWidth(noOFMonthsForRental, 120, Unit.PX);
		noOFMonthsForRental
				.setFieldUpdater(new FieldUpdater<EquipmentRental, String>() {

					@Override
					public void update(int index, EquipmentRental object,
							String value) {
						// TODO Auto-generated method stub
						object.setNoOFMonths(value);
						updateRow(object, index);
						// object.setRentalEquipmentPerMonth((object.getIndividual_total()/Double.parseDouble(value))+"");
						// table.redrawRow(index);

					}
				});
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub

	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub

	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		System.out.println("in set Enable in Table" + state);
		int tablecolcount = this.table.getColumnCount();
		for (int i = tablecolcount - 1; i > -1; i--)
			table.removeColumn(i);
		if (state == true)
			addeditColumn();
		if (state == false)
			addViewColumn();

	}

	private void addViewColumn() {
		// TODO Auto-generated method stub
		addViewMachince();
		addViewBrandColumn();
		addViewPurchasePrice();
		addViewMarketPrice();
		addViewQtyColumn();
		addViewIndividualTotal();
		addViewNoOfMonths();
		addViewRentalOnEquipPerMonth();
		addViewInterestOnMacPerMonthPercent();
		addViewInterestOnMacPerMonth();
		addViewMarketingAndInsurancePercent();
		addViewMarketingAndInsurance();
		/** date 31.8.2018 added by komal**/
		addTotalWithoutDiscount();
		addDiscount();
		/** end komal**/
		addViewTotal();
		addViewDeleteButton();
	}

	private void addViewInterestOnMacPerMonthPercent() {
		// TODO Auto-generated method stub
		viewInterestOnMachineryPercent = new TextColumn<EquipmentRental>() {

			@Override
			public String getValue(EquipmentRental object) {
				// TODO Auto-generated method stub
				if (object.getBrand() != null)
					return object.getMachineryInterest()+"";
				else
					return "";
			}
		};
		table.addColumn(viewInterestOnMachineryPercent, "Interest on Machinery Percent");
		table.setColumnWidth(viewInterestOnMachineryPercent, 120, Unit.PX);
	}

	private void addViewMarketingAndInsurancePercent() {
		// TODO Auto-generated method stub
		viewMaitanienceandInsurancePercent = new TextColumn<EquipmentRental>() {

			@Override
			public String getValue(EquipmentRental object) {
				// TODO Auto-generated method stub
				if (object.getBrand() != null)
					return object.getMachineryInterest()+"";
				else
					return "";
			}
		};
		table.addColumn(viewMaitanienceandInsurancePercent, "Maintainance Percent");
		table.setColumnWidth(viewMaitanienceandInsurancePercent, 120, Unit.PX);
	}

	private void addViewNoOfMonths() {
		// TODO Auto-generated method stub
		viewnoOFMonthsForRental = new TextColumn<EquipmentRental>() {

			@Override
			public String getValue(EquipmentRental object) {
				// TODO Auto-generated method stub
				if (object.getNoOFMonths() != null) {
					return object.getNoOFMonths();
				} else {
					return 0 + "";
				}
			}
		};
		table.addColumn(viewnoOFMonthsForRental, "No Of Months");
		table.setColumnWidth(viewnoOFMonthsForRental, 120, Unit.PX);
	}

	private void addViewDeleteButton() {
		// TODO Auto-generated method stub
		viewDeleteButton = new TextColumn<EquipmentRental>() {

			@Override
			public String getValue(EquipmentRental object) {
				// TODO Auto-generated method stub
				return "";
			}
		};
		table.addColumn(viewTotal, "Total");
		table.setColumnWidth(viewTotal, 120, Unit.PX);
	}

	private void addViewTotal() {
		// TODO Auto-generated method stub
		viewTotal = new TextColumn<EquipmentRental>() {

			@Override
			public String getValue(EquipmentRental object) {
				// TODO Auto-generated method stub
				if (object.getTotal() != null)
					return object.getTotal();
				else
					return "0";
			}
		};
		table.addColumn(viewTotal, "Total");
		table.setColumnWidth(viewTotal, 120, Unit.PX);
	}

	private void addViewMarketingAndInsurance() {
		// TODO Auto-generated method stub
		viewMaitanienceandInsurance = new TextColumn<EquipmentRental>() {

			@Override
			public String getValue(EquipmentRental object) {
				// TODO Auto-generated method stub
				if (object.getMaintananceAndInsurancePerMonth() != null)
					return object.getMaintananceAndInsurancePerMonth();
				else
					return "0";
			}
		};
		table.addColumn(viewMaitanienceandInsurance, "Maintanance/\nMonth (Amt)");
		table.setColumnWidth(viewMaitanienceandInsurance, 120, Unit.PX);
	}

	private void addViewInterestOnMacPerMonth() {
		// TODO Auto-generated method stub
		viewInterestOnMachinery = new TextColumn<EquipmentRental>() {

			@Override
			public String getValue(EquipmentRental object) {
				// TODO Auto-generated method stub
				if (object.getInterestOnMachineryPerMonth() != null)
					return object.getInterestOnMachineryPerMonth();
				else
					return "0";
			}
		};

		table.addColumn(viewInterestOnMachinery, "Interest/\nMonth(Amt)");
		table.setColumnWidth(viewInterestOnMachinery, 120, Unit.PX);
	}

	private void addViewRentalOnEquipPerMonth() {
		// TODO Auto-generated method stub
		viewRentalOnEquipment = new TextColumn<EquipmentRental>() {

			@Override
			public String getValue(EquipmentRental object) {
				// TODO Auto-generated method stub
				if (object.getRentalEquipmentPerMonth() != null)
					return object.getRentalEquipmentPerMonth();
				else
					return "";
			}
		};

		table.addColumn(viewRentalOnEquipment, "Rental/\nMonth");
		table.setColumnWidth(viewRentalOnEquipment, 120, Unit.PX);
	}

	private void addViewQtyColumn() {
		// TODO Auto-generated method stub
		viewQty = new TextColumn<EquipmentRental>() {

			@Override
			public String getValue(EquipmentRental object) {
				// TODO Auto-generated method stub
				if (object.getQty() != 0)
					return object.getQty() + "";
				else
					return "0";
			}
		};
		table.addColumn(viewQty, "Qty");
		table.setColumnWidth(viewQty, 120, Unit.PX);
	}

	
	
	private void addViewPurchasePrice() {
		// TODO Auto-generated method stub
		viewPurchasePrice = new TextColumn<EquipmentRental>() {

			@Override
			public String getValue(EquipmentRental object) {
				// TODO Auto-generated method stub
				if (object.getPurchasePrice() != 0)
					return object.getPurchasePrice() + "";
				else
					return "0";
			}
		};
		table.addColumn(viewPurchasePrice, "Purchase Price");
		table.setColumnWidth(viewPurchasePrice, 120, Unit.PX);
	}
	
	private void addViewMarketPrice() {
		// TODO Auto-generated method stub
		viewmarketPrice = new TextColumn<EquipmentRental>() {

			@Override
			public String getValue(EquipmentRental object) {
				// TODO Auto-generated method stub
				if (object.getMarketPrice() != 0)
					return object.getMarketPrice() + "";
				else
					return "0";
			}
		};
		table.addColumn(viewmarketPrice, "Market Price");
		table.setColumnWidth(viewmarketPrice, 120, Unit.PX);
	}

	private void addViewIndividualTotal() {
		// TODO Auto-generated method stub
		viewInditotalPrice = new TextColumn<EquipmentRental>() {

			@Override
			public String getValue(EquipmentRental object) {
				// TODO Auto-generated method stub
				if (object.getIndividual_total() != 0)
					return object.getIndividual_total() + "";
				else
					return "0";
			}
		};
		table.addColumn(viewInditotalPrice, "Total Price");
		table.setColumnWidth(viewInditotalPrice, 120, Unit.PX);
	}

	private void addViewBrandColumn() {
		// TODO Auto-generated method stub
		viewBrand = new TextColumn<EquipmentRental>() {

			@Override
			public String getValue(EquipmentRental object) {
				// TODO Auto-generated method stub
				if (object.getBrand() != null)
					return object.getBrand();
				else
					return "";
			}
		};
		table.addColumn(viewBrand, "Brand");
		table.setColumnWidth(viewBrand, 120, Unit.PX);
	}

	private void addViewMachince() {
		// TODO Auto-generated method stub
		viewMachine = new TextColumn<EquipmentRental>() {

			@Override
			public String getValue(EquipmentRental object) {
				// TODO Auto-generated method stub
				if (object.getMachine() != null)
					return object.getMachine();
				else
					return "";
			}
		};
		table.addColumn(viewMachine, "Machine");
		table.setColumnWidth(viewMachine, 120, Unit.PX);
	}

	private void addeditColumn() {
		// TODO Auto-generated method stub
		addMachince();
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub

	}

	public void updateRow(EquipmentRental object, int index) {
		// TODO Auto-generated method stub
		object.setMachine(object.getMachine());
		object.setBrand(object.getBrand());
		object.setMarketPrice(object.getMarketPrice());
		object.setQty(object.getQty());
		/**4-3-2019 added by Amol**/
		object.setModelNumber(object.getModelNumber());
		double individualTotalValue=object.getQty() * object.getMarketPrice();
		object.setIndividual_total(Math.round(individualTotalValue));
		object.setNoOFMonths(object.getNoOFMonths());
		double rentalPerMonthValue =0;
		try{
			if(Double.parseDouble(object.getNoOFMonths())!=0){
			rentalPerMonthValue = (object.getQty() * object.getMarketPrice() / Double
					.parseDouble(object.getNoOFMonths()));
			}
		}catch(Exception e){
			rentalPerMonthValue =0;
		}
		object.setRentalEquipmentPerMonth(Math.round(rentalPerMonthValue) + "");
		object.setMachineryInterest(object.getMachineryInterest());
//		if(Double.parseDouble(object.getInterestOnMachineryPerMonth().trim())!=0){
			double interestPerMonthValue = ((object.getMachineryInterest() / 100)
					* object.getIndividual_total())/12;
			object.setInterestOnMachineryPerMonth(Math.round(interestPerMonthValue) + "");
//		}
//		if(Double.parseDouble(object.getMaintananceAndInsurancePerMonth())!=0){
		object.setMaintanancePercentage(object.getMaintanancePercentage());
		double maintanceValue = ((object.getMaintanancePercentage() / 100)
				* object.getIndividual_total())/12;
		object.setMaintananceAndInsurancePerMonth(Math.round(maintanceValue) + "");
//		}
		double totalValue = Double.parseDouble(object.getMaintananceAndInsurancePerMonth())+Double.parseDouble(object.getInterestOnMachineryPerMonth()) + Double.parseDouble(object.getRentalEquipmentPerMonth());//rentalPerMonthValue;
	//	object.setTotal(Math.round(totalValue) + "");
		double grandTotal = totalValue;
		if(object.getDiscount()!=0){
			grandTotal = totalValue - object.getDiscount();
		}
		object.setTotalWithoutDiscount(Math.round(totalValue));
		object.setTotal(grandTotal + "");
		RowCountChangeEvent.fire(table, index, true);
		table.redrawRow(index);
	}
	/** date 31.8.2018 added by komal**/
	private void addTotalWithoutDiscount() {
		// TODO Auto-generated method stub
		viewTotalWithoutDiscount = new TextColumn<EquipmentRental>() {

			@Override
			public String getValue(EquipmentRental object) {
				// TODO Auto-generated method stub
			//	if (object.getTotalWithoutDiscount() != null) {
					return object.getTotalWithoutDiscount() + "";
//				} else {
//					return "";
//				}
			}
		};
		table.addColumn(viewTotalWithoutDiscount, "Total");
		table.setColumnWidth(viewTotalWithoutDiscount, 120, Unit.PX);

	}
	private void addDiscount() {
	// TODO Auto-generated method stub
	viewDiscount = new TextColumn<EquipmentRental>() {

		@Override
		public String getValue(EquipmentRental object) {
			// TODO Auto-generated method stub
		//	if (object.getTotalWithoutDiscount() != null) {
				return object.getDiscount() + "";
//			} else {
//				return "";
//			}
		}
	};
	table.addColumn(viewDiscount, "Total");
	table.setColumnWidth(viewDiscount, 120, Unit.PX);

}
	protected void EditDiscount() {
		// TODO Auto-generated method stub
		EditTextCell editCell = new EditTextCell();
		editDiscount = new Column<EquipmentRental, String>(editCell) {
			@Override
			public String getValue(EquipmentRental object) {
				if (object.getDiscount() == 0) {
					return 0 + "";
				} else {
					return object.getDiscount() + "";
				}
			}
		};
		table.addColumn(editDiscount, "#Dicount");
		table.setColumnWidth(editDiscount, 120, Unit.PX);

		editDiscount.setFieldUpdater(new FieldUpdater<EquipmentRental, String>() {

					@Override
					public void update(int index, EquipmentRental object,
							String value) {
						// TODO Auto-generated method stub
						if (value != null) {
							object.setDiscount(Double.parseDouble(value));
						}
						updateRow(object, index);
					}
				});
	}

	
}
