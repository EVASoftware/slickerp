package com.slicktechnologies.client.views.cnc;

import java.util.Vector;

import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.cnc.CNCPresenter.CNCPresenterSearch;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.cnc.CNC;

public class CNCPresenterSearchProxy extends CNCPresenterSearch {
	public PersonInfoComposite personInfo;
	public IntegerBox tbContractId;
	public ListBox olbContractStatus;
	public DateBox dbStartDate;
	public DateBox dbEndDate;
	
	public CNCPresenterSearchProxy() {
		super();
		// TODO Auto-generated constructor stub
		createGui();
	}
	
	@Override
	public void createScreen() {
		// TODO Auto-generated method stub
//		super.createScreen();
		initWidget();
		FormFieldBuilder builder;
		builder = new FormFieldBuilder("",personInfo);
		FormField fpersonInfo= builder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		builder = new FormFieldBuilder("Documnet Id",tbContractId);
		FormField ftbContractId= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Status",olbContractStatus);
		FormField folbContractStatus= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Start Date",dbStartDate);
		FormField fdbStartDate= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("End Date",dbEndDate);
		FormField fdbEndDate= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder();
		FormField fgroupingCustomerDetails=builder.setlabel("Customer Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		builder = new FormFieldBuilder();
		FormField fgroupingContractDetails=builder.setlabel("Contract Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		this.fields=new FormField[][]{
				{fgroupingCustomerDetails},
				{fpersonInfo},
				{fgroupingContractDetails},
				{ftbContractId,fdbStartDate,fdbEndDate,folbContractStatus},
		};
	}

	private void initWidget() {
		// TODO Auto-generated method stub
		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new Customer());
		personInfo=new PersonInfoComposite(querry,false);
		personInfo.getCustomerId().getHeaderLabel().setText("* Customer ID");
		personInfo.getCustomerName().getHeaderLabel().setText("* Customer Name");
		personInfo.getCustomerCell().getHeaderLabel().setText("* Customer Cell");
		personInfo.getPocName().getHeaderLabel().setText("POC Name");
		tbContractId=new IntegerBox();
		olbContractStatus=new ListBox();
		AppUtility.setStatusListBox(olbContractStatus, CNC.getStatusList());
		
		dbStartDate=new DateBox();
		dbEndDate=new DateBox();
	}
	
	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
		return super.validate();
	}

	@Override
	public MyQuerry getQuerry()
	{
		System.out.println("Child Get Query");
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		
		if(personInfo.getIdValue()!=-1)
		  {
		  temp=new Filter();
		  temp.setIntValue(personInfo.getIdValue());
		  temp.setQuerryString("personInfo.count");
		  filtervec.add(temp);
		  }
		  
		  if(!(personInfo.getFullNameValue().equals("")))
		  {
		  temp=new Filter();
		  temp.setStringValue(personInfo.getFullNameValue());
		  temp.setQuerryString("personInfo.fullName");
		  filtervec.add(temp);
		  }
		  if(personInfo.getCellValue()!=-1l)
		  {
		  temp=new Filter();
		  temp.setLongValue(personInfo.getCellValue());
		  temp.setQuerryString("personInfo.cellNumber");
		  filtervec.add(temp);
		  }
		if(tbContractId.getValue()!=null&&!tbContractId.getValue().equals("")){
			temp=new Filter();
			temp.setIntValue(tbContractId.getValue());
			temp.setQuerryString("count");
			filtervec.add(temp);
		}
		
		if(olbContractStatus.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(olbContractStatus.getValue(olbContractStatus.getSelectedIndex()).trim());
			temp.setQuerryString("status");
			filtervec.add(temp);
		}
		
		if(dbStartDate.getValue()!=null){
			temp=new Filter();
			temp.setDateValue(dbStartDate.getValue());
			temp.setQuerryString("contractStartDate");
			filtervec.add(temp);
		}
		
		if(dbEndDate.getValue()!=null){
			temp=new Filter();
			temp.setDateValue(dbEndDate.getValue());
			temp.setQuerryString("contractEndDate");
			filtervec.add(temp);
		}
		
		MyQuerry querry= new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new CNC());
		
		return querry;
	}
	
}
