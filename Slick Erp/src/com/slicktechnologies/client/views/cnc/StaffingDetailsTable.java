package com.slicktechnologies.client.views.cnc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.SelectionCell;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cnc.StaffingDetails;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.humanresourcelayer.ProvidentFund;
import com.slicktechnologies.shared.common.humanresourcelayer.PaidLeave;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.OtEarningComponent;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTCTemplate;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CtcComponent;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.LWF;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.ProfessionalTax;

public class StaffingDetailsTable extends SuperTable<StaffingDetails> {

	TextColumn<StaffingDetails> viewCTCTemplates, viewdesignation,
			viewgrossSalary, viewnoOfStaff, viewbasic, viewda, viewhra,
			viewothers, viewnetSalary, viewpayableOTRates,
			viewchargableOTRates, viewDeleteButton, viewUniformCost,
			viewWashingAllowance, viewBandP, viewMedical, viewConveyance,
			viewCLeave, viewCBonus, viewTakeHome, viewsdPf, viewsdESIC,
			viewsdPT, viewsdLWF, viewspPf, viewspESIC, viewspEDLI, viewspMWF,
			viewspBonus, viewstatutoryDeduction, viewstatutoryPayments,
			viewtotal/* A+B */, viewpaidLeaves, viewemployementOverHeadCost,
			viewtotalOfPaidAndOverhead,viewmanagementFeesPercent, viewmanagementFees,
			viewtotalIncludingManagementFees, viewoverHeadCost,viewtotalPaidIntoEmployee,
			viewgrandTotalOfCTC,viewTravellingAllowance,viewTravellingAllowanceOverhead
			,viewAdditionalAllowance , viewCTCAmount,viewcityCompensatoryAllowance,viewWorkingHoursCol,
			/*Date 11-5-2019 added by Amol For Gratuity*/viewGratuity,viewAdministrativecost,viewSupervisionCost,viewTrainingCost,viewEsicCost,viewLeaveSalary,viewPlEsic,viewBonusEsic,viewCPlEsic,viewCBonusEsic;/** date 4.10.2018 added by komal**/
	Column<StaffingDetails, String> ctcTemplates, grossSalary, designation,
			noOfStaff, basic, da, hra, others, netSalary, payableOTRates,
			chargableOTRates, uniformCost, deleteButton, washingAllowance,
			bandP, medical, conveyance, cLeave, cBonus, takeHome, sdPf, sdESIC,
			sdPT, sdLWF, spPf, spESIC, spEDLI, spMWF, spBonus,
			statutoryDeduction, statutoryPayments, total/* A+B */, paidLeaves,
			employementOverHeadCost, totalOfPaidAndOverhead,totalPaidIntoEmployee,managementFeesPercent, managementFees,
			totalIncludingManagementFees,travellingAllowance,travellingAllowanceOverhead, overHeadCost, grandTotalOfCTC 
			, additionalAllowance , editCTCAmount,cityCompensatoryAllowance/*Date 11-5-2019 added by Amol For Gratuity*/,Gratuity,administrativeCost,supervisionCost,trainingCost,EsicCost,leaveSalary,plEsic,bonusEsic,cPlEsic,cBonusEsic,workingHourCol; /** date 4.10.2018 added by komal**/
	ArrayList<String> designationNameList;
	HashMap<String, Config> designationEntityMap;
	ArrayList<String> ctcList;
	HashMap<String, CTCTemplate> ctcTemplateMap;
	/**Date 25-2-2019 CCA option added by amol***/
	boolean hideColumn=false;
	/**
	 * We will use same method for updating row. Here we will define which field is updated.
	 */
	boolean editedNoOfStaff,editedBasic,editedDA,editedHRA,editedMedical,editedWashingAllowance,editedConveyance,editedBandP,editedLWF,editedMWF,editedPaidLeave,editedOverheadCost,editedSPPF,editedcLeaves,editedcBonus,editedManaagementFees,editedcityCompensatoryAllowance,
	editedGratuity,editedAdminiStrativeCost,editSupervisionCost,editTrainingCost,editEsicCost,editPayableOtCost,editedUniformCost,editedLeaveSalary;
	double amount = 0;
	
	/**
	 * @author Anil , Date : 06-04-2018
	 * if process configuration(ShowDropDownListForPtAndLwf) is active then we show
	 * drop down list
	 */
	ArrayList<ProfessionalTax> ptList=new ArrayList<ProfessionalTax>();
	ArrayList<LWF> lwfList=new ArrayList<LWF>();
	boolean ptAndLwfDropdownFlag=false;
	String state="";
	
	ArrayList<ProvidentFund> pfEntityList = new ArrayList<ProvidentFund>();
	/**
	 * @author Anil
	 * @since 19-08-2020
	 * paid leave
	 */
	PaidLeave paidleave=null;

	NumberFormat df =NumberFormat.getFormat("#0.00");
	
	/**
	 * @author Anil
	 * @since 03-09-2020
	 * Adding management fees rate
	 * for sun facility raised by Rahul Tiwari
	 */
	boolean managementFeesAsPerStaff=AppUtility.checkForProcessConfigurartionIsActiveOrNot("CNC", "CalculateManagementFeesAsPerStaff");
	boolean siteLocation=AppUtility.checkForProcessConfigurartionIsActiveOrNot("CNC", "EnableSiteLocation");
	public StaffingDetailsTable() {
		super();
		table.setHeight("300px");
		ctcList = new ArrayList<String>();
		ctcTemplateMap = new HashMap<String, CTCTemplate>();
		ptAndLwfDropdownFlag=false;
		state="";
		managementFeesAsPerStaff=AppUtility.checkForProcessConfigurartionIsActiveOrNot("CNC", "CalculateManagementFeesAsPerStaff");
	
		siteLocation=AppUtility.checkForProcessConfigurartionIsActiveOrNot("CNC", "EnableSiteLocation");
	}
	
	/**
	 * 
	 * 
	 */
	
	public StaffingDetailsTable(ArrayList<ProfessionalTax> ptList,ArrayList<LWF> lwfList) {
		super();
		table.setHeight("300px");
		ctcList = new ArrayList<String>();
		ctcTemplateMap = new HashMap<String, CTCTemplate>();
		this.ptList=ptList;
		this.lwfList=lwfList;
		ptAndLwfDropdownFlag=true;
		state="";
	}
	
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public ArrayList<String> loadLwfListAsPerBranch(ArrayList<LWF> lwfList) {
		ArrayList<String> list=new ArrayList<String>();
		list.add("--SELECT--");
		for(LWF lwf:lwfList){
			if(state!=null&&!state.equals("")){
				if(state.equals(lwf.getState())){
					list.add(lwf.getEmployeeContribution()+"");
				}
			}else{
				list.add(lwf.getEmployeeContribution()+"");
			}
		}
		System.out.println("LWF LIST : "+list.size()+" State : "+state+" OG : "+lwfList.size());
		return list;
	}
	
	public ArrayList<String> loadClwfListAsPerBranch(ArrayList<LWF> lwfList) {
		ArrayList<String> list=new ArrayList<String>();
		list.add("--SELECT--");
		for(LWF lwf:lwfList){
			if(state!=null&&!state.equals("")){
				if(state.equals(lwf.getState())){
					list.add(lwf.getEmployerContribution()+"");
				}
			}else{
				list.add(lwf.getEmployerContribution()+"");
			}
		}
		System.out.println("CLWF LIST : "+list.size()+" State : "+state+" OG : "+lwfList.size());
		return list;
	}

	private ArrayList<String> loadPtListAsPerBranch(ArrayList<ProfessionalTax> ptList) {
		ArrayList<String> list=new ArrayList<String>();
		list.add("--SELECT--");
		for(ProfessionalTax pt:ptList){
			if(state!=null&&!state.equals("")){
				if(state.equals(pt.getState())){
					list.add(pt.getPtAmount()+"");
				}
			}else{
				list.add(pt.getPtAmount()+"");
			}
		}
		System.out.println("PT LIST : "+list.size()+" State : "+state+" OG : "+ptList.size());
		return list;
	}
	
	
	public double getApplicablePtAmt(CTCTemplate ctcTemp){
		double ptAmt=0;
		if(ctcTemp!=null&&ptList!=null&&ptList.size()!=0&&state!=null&&!state.equals("")){
			double monthlyAmt=ctcTemp.getTotalCtcAmount()/12;
			for(ProfessionalTax pt:ptList){
				if(pt.getState().equals(state)){
					if(pt.getRangeToAmt()!=0){
						if(monthlyAmt>=pt.getRangeFromAmt()&&monthlyAmt<=pt.getRangeToAmt()){
							return pt.getPtAmount();
						}
					}else{
						if(monthlyAmt>=pt.getRangeFromAmt()){
							return pt.getPtAmount();
						}
					}
				}
			}
		}
		return ptAmt;
	}
	
	public double getApplicableLwfAmt(CTCTemplate ctcTemp){
		double lwfAmt=0;
		if(ctcTemp!=null&&lwfList!=null&&lwfList.size()!=0&&state!=null&&!state.equals("")){
			double monthlyAmt=ctcTemp.getTotalCtcAmount()/12;
			for(LWF lwf:lwfList){
				int noOfMonth=lwf.getLwfList().size();
				if(lwf.getState().equals(state)){
					if(lwf.getToAmt()!=0){
						if(monthlyAmt>=lwf.getFromAmt()&&monthlyAmt<=lwf.getToAmt()){
							return round((lwf.getEmployeeContribution()/12)*noOfMonth,2);
						}
					}else{
						if(monthlyAmt>=lwf.getFromAmt()){
							return round((lwf.getEmployeeContribution()/12)*noOfMonth,2);
						}
					}
				}
			}
		}
		return lwfAmt;
	}
	
	public double getApplicableClwfAmt(CTCTemplate ctcTemp){
		double clwfAmt=0;
		if(ctcTemp!=null&&lwfList!=null&&lwfList.size()!=0&&state!=null&&!state.equals("")){
			double monthlyAmt=ctcTemp.getTotalCtcAmount()/12;
			for(LWF lwf:lwfList){
				int noOfMonth=lwf.getLwfList().size();
				if(lwf.getState().equals(state)){
					if(lwf.getToAmt()!=0){
						if(monthlyAmt>=lwf.getFromAmt()&&monthlyAmt<=lwf.getToAmt()){
							return round((lwf.getEmployerContribution()/12)*noOfMonth,2);
						}
					}else{
						if(monthlyAmt>=lwf.getFromAmt()){
							return round((lwf.getEmployerContribution()/12)*noOfMonth,2);
						}
					}
				}
			}
		}
		return clwfAmt;
	}
	
	public double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    long factor = (long) Math.pow(10, places);
	    value = value * factor;
	    long tmp = Math.round(value);
	    return (double) tmp / factor;
	}

	/**
	 * 
	 * 
	 */

	public StaffingDetailsTable(boolean flag) {
		super(flag);
		// TODO Auto-generated constructor stub
	}

	public StaffingDetailsTable(UiScreen<StaffingDetails> mv) {
		super(mv);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void createTable() {
		// TODO Auto-generated method stub
		System.out.println("Create Table");
		siteLocation=AppUtility.checkForProcessConfigurartionIsActiveOrNot("CNC", "EnableSiteLocation");
		managementFeesAsPerStaff=AppUtility.checkForProcessConfigurartionIsActiveOrNot("CNC", "CalculateManagementFeesAsPerStaff");
		makeListOfDesignation();
		addDeleteButton();
		addDesignation();
	//	editCTCAmount();
		addCTCTemplates();
		loadProvidentFund();
	}

	private void addCTCTemplates() {
		final GenricServiceAsync service = GWT.create(GenricService.class);
		MyQuerry query = new MyQuerry();
		Company c = new Company();
		System.out.println("Company Id :: " + c.getCompanyId());

		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;

		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		

		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.CNC, AppConstants.PC_ENABLELOADCNCHRTEMPLATE)){
			ArrayList<String> list = new ArrayList<String>();
			list.add(AppConstants.CNC);
			list.add(AppConstants.HR);
			list.add("cnc");
			list.add("hr");
			list.add("");

			filter = new Filter();
			filter.setQuerryString("category IN");
			filter.setList(list);
			filtervec.add(filter);
		}
		else{
			filter = new Filter();
			filter.setQuerryString("category");
			filter.setStringValue(AppConstants.CNC);
			filtervec.add(filter);
		}
		
		query.setFilters(filtervec);

		query.setQuerryObject(new CTCTemplate());

		service.getSearchResult(query,
				new AsyncCallback<ArrayList<SuperModel>>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						// TODO Auto-generated method stub
						ctcTemplateMap.clear();
						ctcList.clear();
						ctcList.add("--SELECT--");
						for (SuperModel model : result) {
							CTCTemplate ctcTemplate = (CTCTemplate) model;
							/**
							 * @author Anil , Date : 03-05-2019
							 * update LWF calculation in CtcTemplate
							 * raised by rahul tiwari for v-alliance
							 */
//							updateLWFinCtcTemplate(ctcTemplate);
							
							
							//if(amount !=0 && amount == ctcTemplate.getTotalCtcAmount()){
								ctcList.add(ctcTemplate.getCtcTemplateName());
							//}
						//	amount = 0;
							ctcTemplateMap.put(
									ctcTemplate.getCtcTemplateName().trim(),
									ctcTemplate);
						}
						
						
						if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CNC", "AutoCalculatePtAndLwf")){
							loadPtAndLwf();
						}else{
						editCTCTemplate();

						addFieldUpdaterOFCTC();

						addNoOfStaff();
						/**
						 * @author Anil @since 05-04-2021
						 * Adding working hours col for Alkosh raised by Rahul Tiwari
						 */
						workingHourCol();
						
						addViewGrossSalary();
						addViewBasic();
						addViewDA();
						addViewHRA();
						addViewMedical();
						/**20-2-2019 added by amol***/
						if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CNC", "CncEarningColumn")){
							hideColumn=true;
						}
						Console.log("hideCOLUMNVALUE"+hideColumn);	
						if(hideColumn){
							addCityCompensatoryAllowance();
						}
						addViewWashingAllowance();
						addViewConyevance();
						addViewBandP();
//						/**date 11-5-2019 added by Amol for Gratuity Column**/
//						if(hideColumn){
//						addLeaveSalary();	
//						addGratuity();
//						addAdministrativecost();
//						}
						addViewOthers();
						addViewSDPF();
						
						addViewSDESIC();
						addPlEsic();
						addBonusEsic();
						addViewSDPT();
						addViewSDLWF();
						addViewStatutoryDeductions();
						addViewNetSalary();
						addCLeaves();
						addCBonus();
						addTravellingAllowance();
						addViewTakeHome();
						
						/**
						 * @author Anil @since 10-02-2021
						 * Added Editable Pf Column;
						 */
						if(siteLocation){
							addSPPF();	
						}else{
							addViewSPPF();
						}
						
						addViewSPESIC();
						addCPlEsic();
						addCBonusEsic();
						
						/**
						 * @author Anil @since 10-02-2021
						 * Added Editable Pf Column;
						 */
						if(siteLocation){
							addSPEDLI();	
						}else{
							addViewSPEDLI();
						}
						addViewSPMWF();
						addBonus();
						addPaidLeaves();
						
						/**
						 * @author Anil
						 * @since 07-07-2020
						 * rearrangin components
						 * raise by Rahul Tiwari
						 */
						/**date 11-5-2019 added by Amol for Gratuity Column**/
						if (hideColumn) {
//							addLeaveSalary();
							addAdministrativecost();
							addGratuity();
						}
						if(hideColumn){
							addViewSupervisionCost();
							addViewTrainingCost();
						}
						
						addViewStatutoryPayments();
						addViewTotal();// A+B
						addEmployementOverHeadCost();
						addTravellingAllowanceAfterOverHead();
						addAdditionalAllowance();
						
						addUniformCost();
						
						addViewTotalOfPaidAndOverhead();
						addViewTotalPaidIntoEmployees();
						addManagementFeesPercent();
						addViewManagementFees();
						addViewTotalIncludingManagementFees();
//						if(hideColumn){
//							addViewSupervisionCost();
//							addViewTrainingCost();
//						}
						addViewGrandTotalOfCTC();
						addPayableOTRates();
						if(hideColumn){
							addViewEsicAmount();
						}
						addChargeableOTRates();
//						addUniformCost();
//						addDeleteButton();
						setFieldUpdaterOnDelete();
					}}
				});
	}
	
	protected void addViewCBonusEsic() {
		// TODO Auto-generated method stub
		viewCBonusEsic = new TextColumn<StaffingDetails>() {

			@Override
			public String getValue(StaffingDetails object) {
				// TODO Auto-generated method stub
				double cbonusEsic=0;
				if(object.getSpBonus()!=null){
					cbonusEsic=((Double.parseDouble(object.getSpBonus())*3.25)/100);	
					object.setcBonusEsic(cbonusEsic);
				}else{
					object.setcBonusEsic(0);
					cbonusEsic=0;	
				}
				
				return df.format(cbonusEsic);
			}
		};

		table.addColumn(viewCBonusEsic, "CBonus Esic");
		table.setColumnWidth(viewCBonusEsic, 120, Unit.PX);
	}

	protected void addViewCPlEsic() {
		// TODO Auto-generated method stub
		 viewCPlEsic= new TextColumn<StaffingDetails>() {

			@Override
			public String getValue(StaffingDetails object) {
				double cplEsic=0;
				if(object.getPaidLeaves()!=null){
					cplEsic=((Double.parseDouble(object.getPaidLeaves())*3.25)/100);
					Console.log("CPL ESIC Value "+cplEsic);
					object.setcPlEsic(cplEsic);
				}else{
					object.setcPlEsic(0);
					cplEsic=0;
				}
				
				return df.format(cplEsic);
			}
		};

		table.addColumn(viewCPlEsic, "CPL Esic");
		table.setColumnWidth(viewCPlEsic, 120, Unit.PX);
	}

	protected void addViewBonusEsic() {
		// TODO Auto-generated method stub
		viewBonusEsic = new TextColumn<StaffingDetails>() {

			@Override
			public String getValue(StaffingDetails object) {
				// TODO Auto-generated method stub
				double bonusEsic=0;
				if(object.iscBonusApplicable()&&object.getcBonus()!=null){
					bonusEsic=((Double.parseDouble(object.getcBonus())*0.75)/100);	
					object.setBonusEsic(bonusEsic);
				}else{
					object.setBonusEsic(0);
					bonusEsic=0;	
				}
				
				return df.format(bonusEsic);
//				if(object.getBonusEsic()!=0){
//					return object.getBonusEsic()+"";
//				}else{
//					return 0+"";
//				}
			}
		};

		table.addColumn(viewBonusEsic, "Bonus Esic");
		table.setColumnWidth(viewBonusEsic, 120, Unit.PX);
	}

	protected void addViewPlEsic() {
		// TODO Auto-generated method stub
		viewPlEsic = new TextColumn<StaffingDetails>() {

			@Override
			public String getValue(StaffingDetails object) {
				// TODO Auto-generated method stub
				
				double plEsic=0;
				if(object.iscLeaveApplicable()&&object.getcLeave()!=null){
					plEsic=((Double.parseDouble(object.getcLeave())*0.75)/100);
					Console.log("PL ESIC Value "+plEsic);
					object.setPlEsic(plEsic);
				}else{
					object.setPlEsic(0);
					plEsic=0;
				}
				
				return df.format(plEsic);
//				if(object.getPlEsic()!=0){
//					return object.getPlEsic()+"";
//				}else{
//					return 0+"";
//				}
			}
		};

		table.addColumn(viewPlEsic, "PL Esic");
		table.setColumnWidth(viewPlEsic, 120, Unit.PX);
	}

	private void updateLWFinCtcTemplate(CTCTemplate ctcTemplate) {
		// TODO Auto-generated method stub
		if(ctcTemplate!=null){
			if(ctcTemplate.getDeduction()!=null&&ctcTemplate.getDeduction().size()!=0){
				ArrayList<CtcComponent> lwfList=new ArrayList<CtcComponent>();
				System.out.println("BF DEDUCTION SIZE "+ctcTemplate.getDeduction().size());
				for(int i=0;i<ctcTemplate.getDeduction().size();i++){
					CtcComponent comp=ctcTemplate.getDeduction().get(i);
					if(comp.getShortName().equalsIgnoreCase("LWF")
							||comp.getShortName().trim().equalsIgnoreCase("MLWF")
							||comp.getShortName().trim().equalsIgnoreCase("MWF")){
						lwfList.add(comp);
						ctcTemplate.getDeduction().remove(i);
						i--;
					}
				}
				System.out.println("AF DEDUCTION SIZE "+ctcTemplate.getDeduction().size()+" LWF SIZE : "+lwfList.size());
				
				
			}
		}
	}
	
	private void loadPtAndLwf() {
		// TODO Auto-generated method stub
		ptAndLwfDropdownFlag=true;
		hideColumn=true;
		// TODO Auto-generated method stub
		final GenricServiceAsync service = GWT.create(GenricService.class);
		MyQuerry querry1=new MyQuerry();
		Filter temp1=null;
		temp1=new Filter();
		temp1.setQuerryString("status");
		temp1.setBooleanvalue(true);
		querry1.getFilters().add(temp1);
		querry1.setQuerryObject(new ProfessionalTax());
		service.getSearchResult(querry1, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				System.out.println("QUERRY PT SIZE : "+result.size());
				ptList=new ArrayList<ProfessionalTax>();
				for(SuperModel model:result){
					ProfessionalTax pt=(ProfessionalTax) model;
					ptList.add(pt);
				}
				// TODO Auto-generated method stub
				MyQuerry querry2=new MyQuerry();
				Filter temp2=null;
				temp2=new Filter();
				temp2.setQuerryString("status");
				temp2.setBooleanvalue(true);
				querry2.getFilters().add(temp2);
				querry2.setQuerryObject(new LWF());
				service.getSearchResult(querry2, new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						System.out.println("QUERRY LWF SIZE : "+result.size());
						// TODO Auto-generated method stub
						lwfList=new ArrayList<LWF>();
						for(SuperModel model:result){
							LWF lwf=(LWF) model;
							lwfList.add(lwf);
						}
						
						editCTCTemplate();

						addFieldUpdaterOFCTC();

						addNoOfStaff();
						
						/**
						 * @author Anil @since 05-04-2021
						 * Adding working hours col for Alkosh raised by Rahul Tiwari
						 */
						workingHourCol();
						
						addViewGrossSalary();
						addViewBasic();
						addViewDA();
						addViewHRA();
						addViewMedical();
						if(hideColumn){
						addCityCompensatoryAllowance();
						Console.log("addCityCompensatoryAllowance"+hideColumn);
						}
						addViewWashingAllowance();
						addViewConyevance();
						addViewBandP();
//						if(hideColumn){
//							addLeaveSalary();
//							addGratuity();
//							addAdministrativecost();
//						}
						addViewOthers();
						addViewSDPF();
						addViewSDESIC();
						addPlEsic();
						addBonusEsic();
//						addViewSDPT();
						addSDPT();
//						addViewSDLWF();
						addSDLWF();
						addViewStatutoryDeductions();
						addViewNetSalary();
						addCLeaves();
						addCBonus();
						addTravellingAllowance();
						addViewTakeHome();
						
						/**
						 * @author Anil @since 10-02-2021
						 * Added Editable Pf Column;
						 */
						if(siteLocation){
							addSPPF();	
						}else{
							addViewSPPF();
						}
						addViewSPESIC();
						addCPlEsic();
						addCBonusEsic();
						
						/**
						 * @author Anil @since 10-02-2021
						 * Added Editable Pf Column;
						 */
						if(siteLocation){
							addSPEDLI();	
						}else{
							addViewSPEDLI();
						}
//						addViewSPMWF();
						addSPMWF();
						addBonus();
						addPaidLeaves();
						
						if(hideColumn){
//							addLeaveSalary();
							addAdministrativecost();
							addGratuity();
						}
						if(hideColumn){
							addViewSupervisionCost();
							addViewTrainingCost();
							Console.log("addViewEsicAmount"+hideColumn);
						}
						
						addViewStatutoryPayments();
						addViewTotal();// A+B
						addEmployementOverHeadCost();
						addTravellingAllowanceAfterOverHead();
						addAdditionalAllowance();
						
						addUniformCost();
						
						addViewTotalOfPaidAndOverhead();
						addViewTotalPaidIntoEmployees();
						addManagementFeesPercent();
						addViewManagementFees();
						addViewTotalIncludingManagementFees();
//						if(hideColumn){
//							addViewSupervisionCost();
//							addViewTrainingCost();
//							
//							Console.log("addViewEsicAmount"+hideColumn);
//						}
						addViewGrandTotalOfCTC();
						addPayableOTRates();
						if(hideColumn){
							addViewEsicAmount();
						}
						addChargeableOTRates();
//						addUniformCost();
	                   
						
//						addDeleteButton();
						setFieldUpdaterOnDelete();
						
					}
					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
					}
				});
			}
		});
		
		
	
	}

	  private void addCBonusEsic() {
		// TODO Auto-generated method stub
		EditTextCell editCell = new EditTextCell();
		cBonusEsic = new Column<StaffingDetails, String>(editCell) {
			@Override
			public String getValue(StaffingDetails object) {
				
				/**
				 * @author Anil
				 * @since 19-09-2020
				 * Not to calculate at the time of display
				 */
//				double cbonusEsic=0;
//				if(object.getSpBonus()!=null){
//					cbonusEsic=((Double.parseDouble(object.getSpBonus())*3.25)/100);	
//					object.setcBonusEsic(cbonusEsic);
//				}else{
//					object.setcBonusEsic(0);
//					cbonusEsic=0;	
//				}
//				return df.format(cbonusEsic) + "";
				
				if(object.getcBonusEsic()!=0){
					return df.format(object.getcBonusEsic())+"";
				}else{
					return 0+"";
				}
			}
		};
		table.addColumn(cBonusEsic, "#CBonus Esic");
		table.setColumnWidth(cBonusEsic, 120, Unit.PX);
		
		/**
		 * @author Anil
		 * @since 18-09-2020
		 * Sun facility statutory payment and deduction is not updated
		 */
		cBonusEsic.setFieldUpdater(new FieldUpdater<StaffingDetails, String>() {

			@Override
			public void update(int index, StaffingDetails object, String value) {
				
				if(value!=null){
					double cBonusEsic=0;
					try{
						cBonusEsic=Double.parseDouble(value);
					}catch(Exception e){
						GWTCAlert alert=new GWTCAlert();
						alert.alert("Please enter numeric value only.");
					}
					object.setcBonusEsic(cBonusEsic);
				}
				updateRow(object, index,false);
			}
		});
	}
	
	  private void addCPlEsic() {
		// TODO Auto-generated method stub
		EditTextCell editCell = new EditTextCell();
		cPlEsic = new Column<StaffingDetails, String>(editCell) {
			@Override
			public String getValue(StaffingDetails object) {
				/**
				 * @author Anil
				 * @since 19-09-2020
				 */
//				double cplEsic=0;
//				if(object.getPaidLeaves()!=null){
//					cplEsic=((Double.parseDouble(object.getPaidLeaves())*3.25)/100);
//					Console.log("#CPL ESIC Value "+plEsic);
//					object.setcPlEsic(cplEsic);
//				}else{
//					object.setcPlEsic(0);
//					cplEsic=0;
//				}
//				return df.format(cplEsic) + "";
				if(object.getcPlEsic()!=0){
					return df.format(object.getcPlEsic())+"";
				}else{
					return 0+"";
				}
			}
		};
		table.addColumn(cPlEsic, "#CPL Esic");
		table.setColumnWidth(cPlEsic, 120, Unit.PX);
		/**
		 * @author Anil
		 * @since 18-09-2020
		 * Sun facility statutory payment and deduction is not updated
		 */
		cPlEsic.setFieldUpdater(new FieldUpdater<StaffingDetails, String>() {

			@Override
			public void update(int index, StaffingDetails object, String value) {
				// TODO Auto-generated method stub
				if(value!=null){
					double cplEsic=0;
					try{
						cplEsic=Double.parseDouble(value);
					}catch(Exception e){
						GWTCAlert alert=new GWTCAlert();
						alert.alert("Please enter numeric value only.");
					}
					object.setcPlEsic(cplEsic);
				}
				updateRow(object, index,false);
			}
		});
	}
	
	
	
	
	
	
	
	
	
	
	private void addPlEsic() {
		// TODO Auto-generated method stub
		EditTextCell editCell = new EditTextCell();
		plEsic = new Column<StaffingDetails, String>(editCell) {
			@Override
			public String getValue(StaffingDetails object) {
				
				/**
				 * @author Anil
				 * @since 19-09-2020
				 */
				double plEsic=0;
//				if(object.iscLeaveApplicable()&&object.getcLeave()!=null){
//					plEsic=((Double.parseDouble(object.getcLeave())*0.75)/100);
//					Console.log("#PL ESIC Value "+plEsic);
//					object.setPlEsic(plEsic);
//				}else{
//					object.setPlEsic(0);
//					plEsic=0;
//				}
				
				plEsic=object.getPlEsic();
				return df.format(plEsic) + "";
				
			}
		};
		table.addColumn(plEsic, "#PL Esic");
		table.setColumnWidth(plEsic, 120, Unit.PX);
		
		/**
		 * @author Anil
		 * @since 18-09-2020
		 * Sun facility statutory payment and deduction is not updated
		 */
		plEsic.setFieldUpdater(new FieldUpdater<StaffingDetails, String>() {

			@Override
			public void update(int index, StaffingDetails object, String value) {
				// TODO Auto-generated method stub
				if(value!=null){
					double cplEsic=0;
					try{
						cplEsic=Double.parseDouble(value);
					}catch(Exception e){
						GWTCAlert alert=new GWTCAlert();
						alert.alert("Please enter numeric value only.");
					}
					object.setPlEsic(cplEsic);
				}
				updateRow(object, index,false);
			}
		});
	}
	 private void addBonusEsic() {
		// TODO Auto-generated method stub
		EditTextCell editCell = new EditTextCell();
		bonusEsic = new Column<StaffingDetails, String>(editCell) {
			@Override
			public String getValue(StaffingDetails object) {
				double bonusEsic=0;
				/**
				 * @author Anil
				 * @since 19-09-2020
				 * do not caculate at the time of display
				 */
//				if(object.iscBonusApplicable()&&object.getcBonus()!=null){
//					bonusEsic=((Double.parseDouble(object.getcBonus())*0.75)/100);
//					object.setBonusEsic(bonusEsic);
//				}else{
//					object.setBonusEsic(0);
//					bonusEsic=0;	
//				}
				bonusEsic=object.getBonusEsic();
				
				return df.format(bonusEsic) + "";
			}
		};
		table.addColumn(bonusEsic, "#Bonus Esic");
		table.setColumnWidth(bonusEsic, 120, Unit.PX);
		
		/**
		 * @author Anil
		 * @since 18-09-2020
		 * Sun facility statutory payment and deduction is not updated
		 */
		bonusEsic.setFieldUpdater(new FieldUpdater<StaffingDetails, String>() {

			@Override
			public void update(int index, StaffingDetails object, String value) {
				// TODO Auto-generated method stub
				if(value!=null){
					double cplEsic=0;
					try{
						cplEsic=Double.parseDouble(value);
					}catch(Exception e){
						GWTCAlert alert=new GWTCAlert();
						alert.alert("Please enter numeric value only.");
					}
					object.setBonusEsic(cplEsic);
				}
				updateRow(object, index,false);
			}
		});
	}
	
	protected void addViewAdministravtiveCost() {
		// TODO Auto-generated method stub
		viewAdministrativecost = new TextColumn<StaffingDetails>() {
			@Override
			public String getValue(StaffingDetails object) {
				if (object.getAdministrativecost()!= 0) {
					return object.getAdministrativecost() + "";
				} else {
					return 0 + "";
				}
			}
		};

		table.addColumn(viewAdministrativecost, "Administrative Cost");
		table.setColumnWidth(viewAdministrativecost, 120, Unit.PX);
	}
	

	

	


	   private void addViewGratuity() {
		viewGratuity = new TextColumn<StaffingDetails>(){

			@Override
			public String getValue(StaffingDetails object) {
				if (object.getGratuity()!= null) {
					return object.getGratuity() + "";
				} else {
					return 0.00 + "";
				}
			}
		};

		table.addColumn(viewGratuity, "Gratuity");
		table.setColumnWidth(viewGratuity, 120, Unit.PX);
	}

	protected void addTravellingAllowanceAfterOverHead() {
		// TODO Auto-generated method stub
		EditTextCell editCell = new EditTextCell();
		travellingAllowanceOverhead=new Column<StaffingDetails, String>(editCell) {

			@Override
			public String getValue(StaffingDetails object) {
				// TODO Auto-generated method stub
				return object.getTravellingAllowanceOverhead()+"";
			}
		};
		table.addColumn(travellingAllowanceOverhead, "#Travelling Allowance");
		table.setColumnWidth(travellingAllowanceOverhead, 120, Unit.PX);
		travellingAllowanceOverhead.setFieldUpdater(new FieldUpdater<StaffingDetails, String>() {

			@Override
			public void update(int index, StaffingDetails object, String value) {
				// TODO Auto-generated method stub
				try{
					object.setTravellingAllowanceOverhead(Double.parseDouble(value));
				}catch(Exception e){
					e.printStackTrace();
					object.setTravellingAllowanceOverhead(0);
				}
				
				/**
				 * @author Anil
				 * @since 18-09-2020
				 */
				updateRow(object, index,false);
//				object.setUniformCost(object.getUniformCost());
////				updateRow(object, index);
////				object.setTravellingAllowanceOverhead(object.getTravellingAllowanceOverhead());
//				double totalPaidAndOverheadValue = Double
//						.parseDouble(object.getTotal())+ object.getAdditionalAllowance()+ Double.parseDouble(object.getEmployementOverHeadCost())
//						+ object.getTravellingAllowanceOverhead()+object.getUniformCost();
//				Console.log("totalPaidAndOverheadValue"
//						+ totalPaidAndOverheadValue);
//				object.setTotalOfPaidAndOverhead((Math.round(totalPaidAndOverheadValue))
//						+ "");
//				object.setTotalPaidIntoEmployee(Math.round((totalPaidAndOverheadValue*object.getNoOfStaff())));
//				double feePercent = 0;
//				double managementFeesValue =0;
//				try {
////					if (object.getManageMentfeesPercent()!=0) {
////						feePercent = object.getManageMentfeesPercent();
////					} 
//					if (object.getManageMentfeesPercent()!=0) {
//						feePercent = object.getManageMentfeesPercent();
//						//komal
//						managementFeesValue = object.getTotalPaidIntoEmployee()*( feePercent / 100);
//						Console.log("managementFeesValue"
//								+ managementFeesValue);
//						//object.setManagementFees(Math.round(managementFeesValue) + "");
//					} else{
//						if(object.getManagementFees()!=null && !object.getManagementFees().equals("")){
//							managementFeesValue = Double.parseDouble(object.getManagementFees());
//						}
//						/**
//						 * @author Anil
//						 * @since 03-09-2020
//						 */
//						if(managementFeesAsPerStaff&&object.getNoOfStaff()!=0&&object.getManagementFeesRate()!=0){
//							managementFeesValue=object.getNoOfStaff()*object.getManagementFeesRate();
//						}
//					}
//					Console.log("feePercent" + feePercent);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//
////				double managementFeesValue = object.getTotalPaidIntoEmployee()*( feePercent / 100);
////				Console.log("managementFeesValue"
////						+ managementFeesValue);
//				object.setManagementFees(managementFeesValue + "");
//
//				double managementTotal = managementFeesValue+object.getTotalPaidIntoEmployee();
//				Console.log("managementTotal" + managementTotal);
//				object.setTotalIncludingManagementFees((Math.round(managementTotal))+ "");
//				
//				double grandTotalValue = managementTotal;
//				object.setGrandTotalOfCTC((Math.round(grandTotalValue*100.0)/100.0) + "");
//				Console.log("grandTotalValue" + grandTotalValue);
//				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
//				table.redrawRow(index);
			}
		});
	}

	protected void addTravellingAllowance() {
		// TODO Auto-generated method stub
		EditTextCell editCell = new EditTextCell();
		travellingAllowance=new Column<StaffingDetails, String>(editCell) {

			@Override
			public String getValue(StaffingDetails object) {
				// TODO Auto-generated method stub
				return object.getTravellingAllowance()+"";
			}
		};
		table.addColumn(travellingAllowance, "#Travelling Allowance");
		table.setColumnWidth(travellingAllowance, 120, Unit.PX);
		travellingAllowance.setFieldUpdater(new FieldUpdater<StaffingDetails, String>() {

			@Override
			public void update(int index, StaffingDetails object, String value) {
				// TODO Auto-generated method stub
				try{
					object.setTravellingAllowance(Double.parseDouble(value));
				}catch(Exception e){
					e.printStackTrace();
					object.setTravellingAllowance(0);
				}
//				updateRow(object, index);
				object.setTravellingAllowance(Math.round(object.getTravellingAllowance()*100.0)/100.0);
				//Take Home Salary
				double takeHomeValue = object.getNetSalary()
						+ Double.parseDouble(object.getcLeave())
						+ Double.parseDouble(object.getcBonus())
						+ object.getTravellingAllowance();
				Console.log("takeHomeValue" + takeHomeValue);
				object.setTakeHome((Math.round(takeHomeValue)) + "");
				// Statutory Payments
				//pfValue
				double cpfValue = ((object.getBasic() + Double.parseDouble(object.getDa())) / 100) * 12;
				Console.log("cpfValue" + cpfValue);
				// if(!editedSPPF)
				object.setSpPF(object.getSpPF());
				
//				if (object.getGrossSalary() < 21001) {
//					double esicCalValue = object.getGrossSalary()-object.getWashingAllowance();
//					double esicValue = (esicCalValue / 100) * 4.75;
//					Console.log("c esicValue" + esicValue);
//					object.setSpESIC(Math.round(esicValue) + "");
//				} else {
					object.setSpESIC(object.getSpESIC());
//				}
				
				double edilCalValue = object.getBasic()
						+ Double.parseDouble(object.getDa().trim());
				double edilValue = edilCalValue * (1.16 / 100);
				Console.log("edilValue" + edilValue);
				object.setSpEDLI(object.getSpEDLI());
				object.setSpMWF(object.getSpMWF()+"");
				double cBonusPercent = object.getcBonusPercentage();
				double bonusValue = 0;
				/**
				 * Rahul Verma added paid days. And changed(21/12) to (paiddays/12) where paiddays is provided by User
				 */
				double paiddaysForBonus=Double.parseDouble(object.getPaidLeaveDays().trim());
				
				if (object.isBonusRelever()) {
					bonusValue = ((object.getBasic() + Double
							.parseDouble(object.getDa())) * (cBonusPercent / 100))
							+ (((object.getBasic() + Double
									.parseDouble(object.getDa())) * (cBonusPercent / 100)) * ((paiddaysForBonus/12)/ 30));
				} else {
					bonusValue = ((object.getBasic() + Double
							.parseDouble(object.getDa())) * (cBonusPercent / 100));// +(1.75/30);
				}
				Console.log("bonusValue" + bonusValue);
				object.setSpBonus((Math.round(bonusValue*100.0)/100.0) + "");
				
				/** date 17.11.2018 added by komal for paid leave calculation as discussed with maradona**/
				double paidLeavesValue = 0;
//				Console.log("form.cb_Relever.getValue()"
//						+ object.isRelever() );
				/**
				 * Rahul Verma added paid days. And changed(21/12) to (paiddays/12) where paiddays is provided by User
				 */
				double paiddays=Double.parseDouble(object.getPaidLeaveDays().trim());
				double totalValue = Double.parseDouble(object.getSpEDLI())
						+ Double.parseDouble(object.getSpESIC())
						+ Double.parseDouble(object.getSpMWF())
						+ Double.parseDouble(object.getSpPF());
				totalValue = Math.round(totalValue*100.0)/100.0;
				if (object.isPaidLeaveRelever()) {
					paidLeavesValue = ((object.getGrossSalary() + (totalValue)) * ((paiddays/12)/30))
															+
							(((object.getGrossSalary() + (totalValue)))*((paiddays/12)/30))*( (paiddays/12)/ 30);
				} else {
					paidLeavesValue = (object.getGrossSalary() + (totalValue)) * ((paiddays/12)/30);
				}
				Console.log("paidLeavesValue" + paidLeavesValue);

				object.setPaidLeaves((Math.round(paidLeavesValue*100.0)/100.0) + "");
				/**
				 *  end komal
				 */
				
				/**
				 * @author Anil
				 * @since 04-09-2020
				 */
				CTCTemplate ctcTemplate=ctcTemplateMap.get(object.getCtcTemplates());
				if(paidleave!=null){
					double plAmt=0;
					if(paidleave!=null){
						for(OtEarningComponent plComp:paidleave.getOtEarningCompList()){
							System.out.println("NAME : "+plComp.getComponentName());
							for(CtcComponent earningComp:ctcTemplate.getEarning()){
								System.out.println("EARN NAME : "+earningComp.getName());
								if(plComp.getComponentName().equalsIgnoreCase("Gross Earning")){
									plAmt=plAmt+earningComp.getAmount()/12;
								}else if(plComp.getComponentName().equals(earningComp.getName())){
									System.out.println("AMT : "+earningComp.getAmount()/12);
									plAmt=plAmt+earningComp.getAmount()/12;
								}
								
							}
						}
					}
					System.out.println("BASE AMT : "+plAmt);
					plAmt=plAmt*paidleave.getRate()/100;
					System.out.println("PL AMT : "+plAmt);
					object.setPaidLeaves((Math.round(plAmt*100.0)/100.0)+"");
				}

				/**Date 24-6-2019 
				 * @author Amol
				 * adding gratuity formula
				 */
				double gratuity=0;
//				if(object.isGratuityApplicable()){
//				gratuity
//					=((((object.getBasic())+Double.parseDouble(object.getDa()))*4.81)/(100));
//					object.setGratuity((Math.round(gratuity)+""));
//				  }else{
//					  object.setGratuity(0+"");
//				 }
				object.setGratuity(object.getGratuity());
				object.setSupervision(object.getSupervision());
				object.setTrainingCharges(object.getTrainingCharges());
				
				double statutoryPayments = Double
						.parseDouble(object.getSpBonus())
						+ Double.parseDouble(object.getSpEDLI())
						+ Double.parseDouble(object.getSpESIC())
						+ Double.parseDouble(object.getSpMWF())
						+ Double.parseDouble(object.getSpPF())
						+ Double.parseDouble(object.getPaidLeaves())
						+Double.parseDouble(object.getGratuity())
						+(object.getAdministrativecost())
//						+(object.getUniformCost())
						+(object.getSupervision())
						+(object.getTrainingCharges());
				        
				object.setSatutoryPayments((Math.round(statutoryPayments)) + "");
				object.setTotal((Math.round((object.getGrossSalary()
						+ Double.parseDouble(object
								.getSatutoryPayments())))) + "");
//				double paidLeavesValue = 0;
//				Console.log("form.cb_Relever.getValue()"
//						+ object.isRelever() );
				/**
				 * Rahul Verma added paid days. And changed(21/12) to (paiddays/12) where paiddays is provided by User
				 */
//				double paiddays=Double.parseDouble(object.getPaidLeaveDays().trim());
//				
//				if (object.isPaidLeaveRelever()) {
//					paidLeavesValue = ((object.getGrossSalary() + ((Double.parseDouble(object.getSatutoryPayments()))-Double.parseDouble(object.getSpBonus()))) * ((paiddays/12)/30))
//															+
//							(((object.getGrossSalary() + ((Double.parseDouble(object.getSatutoryPayments()))-Double.parseDouble(object.getSpBonus()))))*((paiddays/12)/30))*((paiddays/12)/ 30);
//				} else {
//					paidLeavesValue = (object.getGrossSalary() + ((Double.parseDouble(object.getSatutoryPayments()))-Double.parseDouble(object.getSpBonus()))) * ((paiddays/12)/30);
//				}
//				Console.log("paidLeavesValue" + paidLeavesValue);
//
//				object.setPaidLeaves((Math.round(paidLeavesValue*100.0)/100.0) + "");
				object.setEmployementOverHeadCost(object.getOverHeadCost());
				object.setTravellingAllowanceOverhead(object.getTravellingAllowanceOverhead());
				object.setUniformCost(object.getUniformCost());
				double totalPaidAndOverheadValue = Double
						.parseDouble(object.getTotal())
						+ object.getAdditionalAllowance()
						+ Double.parseDouble(object
								.getEmployementOverHeadCost())
						+ object.getTravellingAllowanceOverhead()+object.getUniformCost();
				Console.log("totalPaidAndOverheadValue"
						+ totalPaidAndOverheadValue);
				object.setTotalOfPaidAndOverhead((Math.round(totalPaidAndOverheadValue))
						+ "");
				object.setTotalPaidIntoEmployee(Math.round((totalPaidAndOverheadValue*object.getNoOfStaff())));
				double feePercent = 0;
				double managementFeesValue = 0;
				try {
//					if (object.getManageMentfeesPercent()!=0) {
//						feePercent = object.getManageMentfeesPercent();
//					} 
					if (object.getManageMentfeesPercent()!=0) {
						feePercent = object.getManageMentfeesPercent();
						//komal
						managementFeesValue = object.getTotalPaidIntoEmployee()*( feePercent / 100);
						Console.log("managementFeesValue"
								+ managementFeesValue);
						//object.setManagementFees(Math.round(managementFeesValue) + "");
					} else{
						if(object.getManagementFees()!=null && !object.getManagementFees().equals("")){
							managementFeesValue = Double.parseDouble(object.getManagementFees());
						}
						/**
						 * @author Anil
						 * @since 03-09-2020
						 */
						if(managementFeesAsPerStaff&&object.getNoOfStaff()!=0&&object.getManagementFeesRate()!=0){
							managementFeesValue=object.getNoOfStaff()*object.getManagementFeesRate();
						}
					}

					Console.log("feePercent" + feePercent);
				} catch (Exception e) {
					e.printStackTrace();
				}

//				double managementFeesValue = object.getTotalPaidIntoEmployee()*( feePercent / 100);
//				Console.log("managementFeesValue"
//						+ managementFeesValue);
				object.setManagementFees(managementFeesValue + "");

				double managementTotal = managementFeesValue+object.getTotalPaidIntoEmployee();
				Console.log("managementTotal" + managementTotal);
				object.setTotalIncludingManagementFees((Math.round(managementTotal))
						+ "");
				
				double grandTotalValue = managementTotal;
				object.setGrandTotalOfCTC((Math.round(grandTotalValue*100.0)/100.0) + "");
				Console.log("grandTotalValue" + grandTotalValue);
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				table.redrawRow(index);
			}
		});
	}

	protected void addManagementFeesPercent() {
		// TODO Auto-generated method stub
		EditTextCell editCell = new EditTextCell();
		managementFeesPercent = new Column<StaffingDetails, String>(editCell) {
			@Override
			public String getValue(StaffingDetails object) {
				if (object.getManageMentfeesPercent() != null) {
					return object.getManageMentfeesPercent() + "";
				} else {
					return 0 + "";
				}
			}
		};
		table.addColumn(managementFeesPercent, "#Management %");
		table.setColumnWidth(managementFeesPercent, 120, Unit.PX);
		managementFeesPercent.setFieldUpdater(new FieldUpdater<StaffingDetails, String>() {

			@Override
			public void update(int index, StaffingDetails object, String value) {
				// TODO Auto-generated method stub
				object.setManageMentfeesPercent(Double.parseDouble(value.trim()));
				double feePercent = 0;
				
				double managementFeesValue = 0;
				try {
//					if (object.getManageMentfeesPercent()!=0) {
//						feePercent = object.getManageMentfeesPercent();
//					} 
					if (object.getManageMentfeesPercent()!=-1) {
						feePercent = object.getManageMentfeesPercent();
						managementFeesValue = object.getTotalPaidIntoEmployee()*( feePercent / 100);
						Console.log("managementFeesValue"
								+ managementFeesValue);
					//	object.setManagementFees(managementFeesValue + "");

					} else{
						if(object.getManagementFees()!=null && !object.getManagementFees().equals("")){
							managementFeesValue = Double.parseDouble(object.getManagementFees());
						}
						/**
						 * @author Anil
						 * @since 03-09-2020
						 */
						if(managementFeesAsPerStaff&&object.getNoOfStaff()!=0&&object.getManagementFeesRate()!=0){
							managementFeesValue=object.getNoOfStaff()*object.getManagementFeesRate();
						}
					}

					Console.log("feePercent" + feePercent);
				} catch (Exception e) {
					e.printStackTrace();
				}
//				double managementFeesValue = object.getTotalPaidIntoEmployee()*( feePercent / 100);
//				Console.log("managementFeesValue"
//						+ managementFeesValue);
				object.setManagementFees((Math.round(managementFeesValue*100.0)/100.0) + "");

				double managementTotal = managementFeesValue+object.getTotalPaidIntoEmployee();
				Console.log("managementTotal" + managementTotal);
				object.setTotalIncludingManagementFees((Math.round(managementTotal))
						+ "");
				
				double grandTotalValue = managementTotal;
				object.setGrandTotalOfCTC((Math.round(grandTotalValue*100.0)/100.0) + "");
				Console.log("grandTotalValue" + grandTotalValue);
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				table.redrawRow(index);
			}
		});
	}

	protected void addTotalPaidIntoEmployees() {
		// TODO Auto-generated method stub
		EditTextCell editCell = new EditTextCell();
		totalPaidIntoEmployee = new Column<StaffingDetails, String>(editCell) {
			@Override
			public String getValue(StaffingDetails object) {
				if (object.getTotalOfPaidAndOverhead() != null) {
					return object.getTotalPaidIntoEmployee() + "";
				} else {
					return 0 + "";
				}
			}
		};
		table.addColumn(totalPaidIntoEmployee, "#Total(x Emp)");
		table.setColumnWidth(totalPaidIntoEmployee, 120, Unit.PX);
	}

	protected void addGrandTotalOfCTC() {
		// TODO Auto-generated method stub
		EditTextCell editCell = new EditTextCell();
		grandTotalOfCTC = new Column<StaffingDetails, String>(editCell) {
			@Override
			public String getValue(StaffingDetails object) {
				if (object.getGrandTotalOfCTC() != null) {
					return object.getGrandTotalOfCTC() + "";
				} else {
					return 0 + "";
				}
			}
		};
		table.addColumn(grandTotalOfCTC, "#Grand Total");
		table.setColumnWidth(grandTotalOfCTC, 120, Unit.PX);
	}

	protected void addTotalIncludingManagementFees() {
		// TODO Auto-generated method stub
		EditTextCell editCell = new EditTextCell();
		totalIncludingManagementFees = new Column<StaffingDetails, String>(
				editCell) {
			@Override
			public String getValue(StaffingDetails object) {
				if (object.getTotalIncludingManagementFees() != null) {
					return object.getTotalIncludingManagementFees() + "";
				} else {
					return 0 + "";
				}
			}
		};
		table.addColumn(totalIncludingManagementFees, "#Total");
		table.setColumnWidth(totalIncludingManagementFees, 120, Unit.PX);
	}

	protected void addManagementFees() {
		// TODO Auto-generated method stub
		EditTextCell editCell = new EditTextCell();
		managementFees = new Column<StaffingDetails, String>(editCell) {
			@Override
			public String getValue(StaffingDetails object) {
				if (object.getManagementFees() != null) {
					return object.getManagementFees() + "";
				} else {
					return 0 + "";
				}
			}
		};
		table.addColumn(managementFees, "#Management Fees");
		table.setColumnWidth(managementFees, 120, Unit.PX);
		managementFees.setFieldUpdater(new FieldUpdater<StaffingDetails, String>() {

			@Override
			public void update(int index, StaffingDetails object, String value) {
				// TODO Auto-generated method stub
//				editedManaagementFees=true;
				object.setManagementFees(value);
				updateRow(object, index,false);
			}
		});
	}

	protected void addTotalOfPaidAndOverhead() {
		// TODO Auto-generated method stub
		EditTextCell editCell = new EditTextCell();
		totalOfPaidAndOverhead = new Column<StaffingDetails, String>(editCell) {
			@Override
			public String getValue(StaffingDetails object) {
				if (object.getTotalOfPaidAndOverhead() != null) {
					return object.getTotalOfPaidAndOverhead() + "";
				} else {
					return 0 + "";
				}
			}
		};
		table.addColumn(totalOfPaidAndOverhead, "#Total");
		table.setColumnWidth(totalOfPaidAndOverhead, 120, Unit.PX);
	}

	protected void addEmployementOverHeadCost() {
		// TODO Auto-generated method stub
		EditTextCell editCell = new EditTextCell();
		overHeadCost = new Column<StaffingDetails, String>(editCell) {
			@Override
			public String getValue(StaffingDetails object) {
				if (object.getOverHeadCost() != null && !object.getOverHeadCost().equals("")) {
					return object.getOverHeadCost() + "";
				} else {
					return 0 + "";
				}
			}
		};
		table.addColumn(overHeadCost, "#OverHead Cost");
		table.setColumnWidth(overHeadCost, 120, Unit.PX);
		overHeadCost.setFieldUpdater(new FieldUpdater<StaffingDetails, String>() {

			@Override
			public void update(int index, StaffingDetails object, String value) {
				// TODO Auto-generated method stub
				/** date 4.10.2018 added by komal **/
				object.setOverHeadCost(value);
//				object.setEmployementOverHeadCost(value);
				object.setEmployementOverHeadCost(object.getOverHeadCost());
				object.setTravellingAllowanceOverhead(object.getTravellingAllowanceOverhead());
				object.setUniformCost(object.getUniformCost());
				double totalPaidAndOverheadValue = Double
						.parseDouble(object.getTotal())
						+ object.getAdditionalAllowance()
						+ Double.parseDouble(object
								.getEmployementOverHeadCost())
						+ object.getTravellingAllowanceOverhead()+object.getUniformCost();
				Console.log("totalPaidAndOverheadValue"
						+ totalPaidAndOverheadValue);
				object.setTotalOfPaidAndOverhead((Math.round(totalPaidAndOverheadValue))
						+ "");
				object.setTotalPaidIntoEmployee(Math.round((totalPaidAndOverheadValue*object.getNoOfStaff())));
				double feePercent = 0;
				double managementFeesValue = 0;
				try {
//					if (object.getManageMentfeesPercent()!=0) {
//						feePercent = object.getManageMentfeesPercent();
//					} 
					if (object.getManageMentfeesPercent()!=0) {
						feePercent = object.getManageMentfeesPercent();
						managementFeesValue = object.getTotalPaidIntoEmployee()*( feePercent / 100);
						Console.log("managementFeesValue"
								+ managementFeesValue);
				//		object.setManagementFees(managementFeesValue + "");

					} else{
						if(object.getManagementFees()!=null && !object.getManagementFees().equals(""))
						managementFeesValue = Double.parseDouble(object.getManagementFees());
					}

					Console.log("feePercent" + feePercent);
				} catch (Exception e) {
					e.printStackTrace();
				}

//				double managementFeesValue = object.getTotalPaidIntoEmployee()*( feePercent / 100);
//				Console.log("managementFeesValue"
//						+ managementFeesValue);
				object.setManagementFees(managementFeesValue + "");

				double managementTotal = managementFeesValue+object.getTotalPaidIntoEmployee();
				Console.log("managementTotal" + managementTotal);
				object.setTotalIncludingManagementFees((Math.round(managementTotal))
						+ "");
				
				double grandTotalValue = managementTotal;
				object.setGrandTotalOfCTC((Math.round(grandTotalValue*100.0)/100.0) + "");
				Console.log("grandTotalValue" + grandTotalValue);
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				table.redrawRow(index);
			}
		});
	}

	protected void addPaidLeaves() {
		// TODO Auto-generated method stub
		EditTextCell editCell = new EditTextCell();
		paidLeaves = new Column<StaffingDetails, String>(editCell) {
			@Override
			public String getValue(StaffingDetails object) {
				if (object.getPaidLeaves() != null) {
					return object.getPaidLeaves() + "";
				} else {
					return 0 + "";
				}
			}
		};
		table.addColumn(paidLeaves, "#Paid Leaves");
		table.setColumnWidth(paidLeaves, 120, Unit.PX);
		paidLeaves.setFieldUpdater(new FieldUpdater<StaffingDetails, String>() {

			@Override
			public void update(int index, StaffingDetails object, String value) {
				// TODO Auto-generated method stub
				object.setPaidLeaves(value);
//				editedPaidLeave=true;
//				updateRow(object, index);
				
				/**
				 * @author Anil
				 * @since 19-09-2020
				 */
				double cplEsic=0;
				if(object.getPaidLeaves()!=null){
					cplEsic=((Double.parseDouble(object.getPaidLeaves())*3.25)/100);
					Console.log("#CPL ESIC Value "+plEsic);
					object.setcPlEsic(cplEsic);
				}else{
					object.setcPlEsic(0);
					cplEsic=0;
				}
				
				/**Date 24-6-2019 
				 * @author Amol
				 * adding gratuity formula
				 */
				double gratuity=0;
//				if(object.isGratuityApplicable()){
//				gratuity
//					=((((object.getBasic())+Double.parseDouble(object.getDa()))*4.81)/(100));
//					object.setGratuity((Math.round(gratuity)+""));
//				  }else{
//					  object.setGratuity(0+"");
//				 }
				object.setGratuity(object.getGratuity());
				object.setSupervision(object.getSupervision());
				object.setTrainingCharges(object.getTrainingCharges());
				
				double statutoryPayments = Double
						.parseDouble(object.getSpBonus())
						+ Double.parseDouble(object.getSpEDLI())
						+ Double.parseDouble(object.getSpESIC())
						+ Double.parseDouble(object.getSpMWF())
						+ Double.parseDouble(object.getSpPF())
						+ Double.parseDouble(object.getPaidLeaves())
						+Double.parseDouble(object.getGratuity())
						+(object.getAdministrativecost())
//						+(object.getUniformCost())
						+(object.getSupervision())
				        +(object.getTrainingCharges())+(object.getcPlEsic())
			            +(object.getcBonusEsic());
						
						
				object.setSatutoryPayments((Math.round(statutoryPayments)) + "");
				object.setTotal((Math.round((object.getGrossSalary()
						+ Double.parseDouble(object
								.getSatutoryPayments())))) + "");

				object.setEmployementOverHeadCost(object.getOverHeadCost());
				object.setTravellingAllowanceOverhead(object.getTravellingAllowanceOverhead());
				object.setUniformCost(object.getUniformCost());
				double totalPaidAndOverheadValue = Double
						.parseDouble(object.getTotal())
						+ object.getAdditionalAllowance()
						+ Double.parseDouble(object
								.getEmployementOverHeadCost())
						+ object.getTravellingAllowanceOverhead()+object.getUniformCost();
				Console.log("totalPaidAndOverheadValue"
						+ totalPaidAndOverheadValue);
				object.setTotalOfPaidAndOverhead((Math.round(totalPaidAndOverheadValue))
						+ "");
				object.setTotalPaidIntoEmployee(Math.round((totalPaidAndOverheadValue*object.getNoOfStaff())));
				double feePercent = 0;
				double managementFeesValue = 0;
				try {
//					if (object.getManageMentfeesPercent()!=0) {
//						feePercent = object.getManageMentfeesPercent();
//					} 
					if (object.getManageMentfeesPercent()!=0) {
						feePercent = object.getManageMentfeesPercent();
						managementFeesValue = object.getTotalPaidIntoEmployee()*( feePercent / 100);
						Console.log("managementFeesValue"
								+ managementFeesValue);
					//	object.setManagementFees(managementFeesValue + "");

					} else{
						if(object.getManagementFees()!=null && !object.getManagementFees().equals("")){
							managementFeesValue = Double.parseDouble(object.getManagementFees());
						}
						/**
						 * @author Anil
						 * @since 03-09-2020
						 */
						if(managementFeesAsPerStaff&&object.getNoOfStaff()!=0&&object.getManagementFeesRate()!=0){
							managementFeesValue=object.getNoOfStaff()*object.getManagementFeesRate();
						}
					}

					Console.log("feePercent" + feePercent);
				} catch (Exception e) {
					e.printStackTrace();
				}

//				double managementFeesValue = object.getTotalPaidIntoEmployee()*( feePercent / 100);
//				Console.log("managementFeesValue"
//						+ managementFeesValue);
				object.setManagementFees(managementFeesValue + "");

				double managementTotal = managementFeesValue+object.getTotalPaidIntoEmployee();
				Console.log("managementTotal" + managementTotal);
				object.setTotalIncludingManagementFees((Math.round(managementTotal))
						+ "");
				
				double grandTotalValue = managementTotal;
				object.setGrandTotalOfCTC((Math.round(grandTotalValue*100.0)/100.0) + "");
				Console.log("grandTotalValue" + grandTotalValue);
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				table.redrawRow(index);
			}
		});
	}
	
	public void updatePaidLeaves(StaffingDetails object,int count){

		// TODO Auto-generated method stub
		
//		double paiddays=Double.parseDouble(object.getPaidLeaveDays().trim());
//		double paidLeavesValue=0;
//		if (object.isPaidLeaveRelever()) {
//			paidLeavesValue = ((object.getGrossSalary() + ((Double.parseDouble(object.getSatutoryPayments()))-Double.parseDouble(object.getSpBonus()))) * ((paiddays/12)/30))
//													+
//					(((object.getGrossSalary() + ((Double.parseDouble(object.getSatutoryPayments()))-Double.parseDouble(object.getSpBonus()))))*((paiddays/12)/30))*((paiddays/12)/ 30);
//		} else {
//			paidLeavesValue = (object.getGrossSalary() + ((Double.parseDouble(object.getSatutoryPayments()))-Double.parseDouble(object.getSpBonus()))) * ((paiddays/12)/30);
//		}
//		Console.log("paidLeavesValue" + paidLeavesValue);
//
//		object.setPaidLeaves((Math.round(paidLeavesValue*100.0)/100.0) + "");
//		object.setPaidLeaves();
//		editedPaidLeave=true;
//		updateRow(object, index);
		/** date 17.11.2018 added by komal for paid leave calculation as discussed with maradona**/
		double paidLeavesValue = 0;
//		Console.log("form.cb_Relever.getValue()"
//				+ object.isRelever() );
		/**
		 * Rahul Verma added paid days. And changed(21/12) to (paiddays/12) where paiddays is provided by User
		 */
		double paiddays=Double.parseDouble(object.getPaidLeaveDays().trim());
		double totalValue = Double.parseDouble(object.getSpEDLI())
				+ Double.parseDouble(object.getSpESIC())
				+ Double.parseDouble(object.getSpMWF())
				+ Double.parseDouble(object.getSpPF());
		totalValue = Math.round(totalValue*100.0)/100.0;
		if (object.isPaidLeaveRelever()) {
			paidLeavesValue = ((object.getGrossSalary() + (totalValue)) * ((paiddays/12)/30))
													+
					(((object.getGrossSalary() + (totalValue)))*((paiddays/12)/30))*( (paiddays/12)/ 30);
		} else {
			paidLeavesValue = (object.getGrossSalary() + (totalValue)) * ((paiddays/12)/30);
		}
		Console.log("paidLeavesValue" + paidLeavesValue);

		object.setPaidLeaves((Math.round(paidLeavesValue*100.0)/100.0) + "");
		/**
		 *  end komal
		 */
		/**Date 27-8-2020**/
		if(object.iscLeaveApplicable()){
			double cLeaveValue = ((object.getBasic() + Double
					.parseDouble(object.getDa().trim()))*((paiddays/12)/30));
			Console.log("cLeaveValue" + cLeaveValue);
			object.setcLeave((Math.round(cLeaveValue*100.0)/100.0)+"");
		}else{
			object.setcLeave(0+"");
		}

		/**
		 * @author Anil
		 * @since 04-09-2020
		 */
		CTCTemplate ctcTemplate=ctcTemplateMap.get(object.getCtcTemplates());
		if(paidleave!=null){
			double plAmt=0;
			if(paidleave!=null){
				for(OtEarningComponent plComp:paidleave.getOtEarningCompList()){
					System.out.println("NAME : "+plComp.getComponentName());
					for(CtcComponent earningComp:ctcTemplate.getEarning()){
						System.out.println("EARN NAME : "+earningComp.getName());
						if(plComp.getComponentName().equalsIgnoreCase("Gross Earning")){
							plAmt=plAmt+earningComp.getAmount()/12;
						}else if(plComp.getComponentName().equals(earningComp.getName())){
							System.out.println("AMT : "+earningComp.getAmount()/12);
							plAmt=plAmt+earningComp.getAmount()/12;
						}
						
					}
				}
			}
			System.out.println("BASE AMT : "+plAmt);
			plAmt=plAmt*paidleave.getRate()/100;
			System.out.println("PL AMT : "+plAmt);
			object.setPaidLeaves((Math.round(plAmt*100.0)/100.0)+"");
			object.setcLeave((Math.round(plAmt*100.0)/100.0)+"");
		}
		
		/**
		 * @author Anil
		 * @since 19-09-2020
		 */
		double cplEsic=0;
		if(object.getPaidLeaves()!=null){
			cplEsic=((Double.parseDouble(object.getPaidLeaves())*3.25)/100);
			Console.log("#CPL ESIC Value "+plEsic);
			object.setcPlEsic(cplEsic);
		}else{
			object.setcPlEsic(0);
			cplEsic=0;
		}
		
		/**Date 24-6-2019 
		 * @author Amol
		 * adding gratuity formula
		 */
		double gratuity=0;
//		if(object.isGratuityApplicable()){
//		gratuity
//			=((((object.getBasic())+Double.parseDouble(object.getDa()))*4.81)/(100));
//			object.setGratuity((Math.round(gratuity)+""));
//		  }else{
//			  object.setGratuity(0+"");
//		 }
		object.setGratuity(object.getGratuity());
		object.setSupervision(object.getSupervision());
		object.setTrainingCharges(object.getTrainingCharges());
		double statutoryPayments = Double
				.parseDouble(object.getSpBonus())
				+ Double.parseDouble(object.getSpEDLI())
				+ Double.parseDouble(object.getSpESIC())
				+ Double.parseDouble(object.getSpMWF())
				+ Double.parseDouble(object.getSpPF())
				+ Double.parseDouble(object.getPaidLeaves())
		        +Double.parseDouble(object.getGratuity())
		        +(object.getAdministrativecost())
//		        +(object.getUniformCost())
		        +(object.getSupervision())
		        +(object.getTrainingCharges())+(object.getcPlEsic())
	            +(object.getcBonusEsic());
		
		
		
		object.setSatutoryPayments((Math.round(statutoryPayments)) + "");
		object.setTotal((Math.round((object.getGrossSalary()
				+ Double.parseDouble(object
						.getSatutoryPayments())))) + "");

		object.setEmployementOverHeadCost(object.getOverHeadCost());
		object.setTravellingAllowanceOverhead(object.getTravellingAllowanceOverhead());
		object.setUniformCost(object.getUniformCost());
		double totalPaidAndOverheadValue = Double
				.parseDouble(object.getTotal())
				+ object.getAdditionalAllowance()
				+ Double.parseDouble(object
						.getEmployementOverHeadCost())
				+ object.getTravellingAllowanceOverhead()+object.getUniformCost();
		Console.log("totalPaidAndOverheadValue"
				+ totalPaidAndOverheadValue);
		object.setTotalOfPaidAndOverhead((Math.round(totalPaidAndOverheadValue))
				+ "");
		object.setTotalPaidIntoEmployee(Math.round((totalPaidAndOverheadValue*object.getNoOfStaff())));
		double feePercent = 0;
		double managementFeesValue = 0;
		try {
//			if (object.getManageMentfeesPercent()!=0) {
//				feePercent = object.getManageMentfeesPercent();
//			} 
			if (object.getManageMentfeesPercent()!=0) {
				feePercent = object.getManageMentfeesPercent();
				managementFeesValue = object.getTotalPaidIntoEmployee()*( feePercent / 100);
				Console.log("managementFeesValue"
						+ managementFeesValue);
				//object.setManagementFees(managementFeesValue + "");

			} else{
				if(object.getManagementFees()!=null && !object.getManagementFees().equals("")){
					managementFeesValue = Double.parseDouble(object.getManagementFees());
				}
				/**
				 * @author Anil
				 * @since 03-09-2020
				 */
				if(managementFeesAsPerStaff&&object.getNoOfStaff()!=0&&object.getManagementFeesRate()!=0){
					managementFeesValue=object.getNoOfStaff()*object.getManagementFeesRate();
				}
			}

			Console.log("feePercent" + feePercent);
		} catch (Exception e) {
			e.printStackTrace();
		}

//		double managementFeesValue = object.getTotalPaidIntoEmployee()*( feePercent / 100);
//		Console.log("managementFeesValue"
//				+ managementFeesValue);
		object.setManagementFees(managementFeesValue + "");

		double managementTotal = managementFeesValue+object.getTotalPaidIntoEmployee();
		Console.log("managementTotal" + managementTotal);
		object.setTotalIncludingManagementFees((Math.round(managementTotal))
				+ "");
		
		double grandTotalValue = managementTotal;
		object.setGrandTotalOfCTC((Math.round(grandTotalValue*100.0)/100.0) + "");
		Console.log("grandTotalValue" + grandTotalValue);
		RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
		table.redrawRow(count);
	}

	protected void addTotal() {
		// TODO Auto-generated method stub
		EditTextCell editCell = new EditTextCell();
		total = new Column<StaffingDetails, String>(editCell) {
			@Override
			public String getValue(StaffingDetails object) {
				if (object.getTotal() != null) {
					return object.getTotal() + "";
				} else {
					return 0 + "";
				}
			}
		};
		table.addColumn(total, "#Total");
		table.setColumnWidth(total, 120, Unit.PX);
	}

	protected void addBonus() {
		// TODO Auto-generated method stub
		EditTextCell editCell = new EditTextCell();
		spBonus = new Column<StaffingDetails, String>(editCell) {
			@Override
			public String getValue(StaffingDetails object) {
				if (object.getSpBonus() != null) {
					return object.getSpBonus() + "";
				} else {
					return 0 + "";
				}
			}
		};
		table.addColumn(spBonus, "#Bonus");
		table.setColumnWidth(spBonus, 120, Unit.PX);
		
		
		spBonus.setFieldUpdater(new FieldUpdater<StaffingDetails, String>() {

			@Override
			public void update(int index, StaffingDetails object, String value) {
				// TODO Auto-generated method stub
				object.setSpBonus((Math.round(Double.parseDouble(value)*100.0)/100.0)+"");
				updateSPBonus(object , index);
//				double statutoryPayments = Double
//						.parseDouble(object.getSpBonus())
//						+ Double.parseDouble(object.getSpEDLI())
//						+ Double.parseDouble(object.getSpESIC())
//						+ Double.parseDouble(object.getSpMWF())
//						+ Double.parseDouble(object.getSpPF()); 
//				object.setSatutoryPayments(Math.round(statutoryPayments) + "");
//				object.setTotal(Math.round(object.getGrossSalary()
//						+ Double.parseDouble(object
//								.getSatutoryPayments())) + "");
//				double paidLeavesValue = 0;
////				Console.log("form.cb_Relever.getValue()"
////						+ object.isRelever() );
//				/**
//				 * Rahul Verma added paid days. And changed(21/12) to (paiddays/12) where paiddays is provided by User
//				 */
//				double paiddays=Double.parseDouble(object.getPaidLeaveDays().trim());
//				
//				if (object.isPaidLeaveRelever()) {
//					paidLeavesValue = ((object.getGrossSalary() + ((Double.parseDouble(object.getSatutoryPayments()))-Double.parseDouble(object.getSpBonus()))) * ((paiddays/12)/30))
//															+
//							(((object.getGrossSalary() + ((Double.parseDouble(object.getSatutoryPayments()))-Double.parseDouble(object.getSpBonus()))))*((paiddays/12)/30))*((paiddays/12)/ 30);
//				} else {
//					paidLeavesValue = (object.getGrossSalary() + ((Double.parseDouble(object.getSatutoryPayments()))-Double.parseDouble(object.getSpBonus()))) * ((paiddays/12)/30);
//				}
//				Console.log("paidLeavesValue" + paidLeavesValue);
//
//				object.setPaidLeaves(Math.round(paidLeavesValue) + "");
//				object.setEmployementOverHeadCost(object.getOverHeadCost());
//				object.setTravellingAllowanceOverhead(object.getTravellingAllowanceOverhead());
//				double totalPaidAndOverheadValue = Double
//						.parseDouble(object.getTotal())
//						+ Double.parseDouble(object.getPaidLeaves())
//						+ Double.parseDouble(object
//								.getEmployementOverHeadCost())
//						+ object.getTravellingAllowanceOverhead();
//				Console.log("totalPaidAndOverheadValue"
//						+ totalPaidAndOverheadValue);
//				object.setTotalOfPaidAndOverhead(Math.round(totalPaidAndOverheadValue)
//						+ "");
//				object.setTotalPaidIntoEmployee(Math.round(totalPaidAndOverheadValue*object.getNoOfStaff()));
//				double feePercent = 0;
//				double managementFeesValue =0;
//				try {
////					if (object.getManageMentfeesPercent()!=0) {
////						feePercent = object.getManageMentfeesPercent();
////					} 
//					if (object.getManageMentfeesPercent()!=0) {
//						feePercent = object.getManageMentfeesPercent();
//						managementFeesValue = object.getTotalPaidIntoEmployee()*( feePercent / 100);
//						Console.log("managementFeesValue"
//								+ managementFeesValue);
//					//	object.setManagementFees(managementFeesValue + "");
//
//					} else{
//						if(object.getManagementFees()!=null && !object.getManagementFees().equals(""))
//						managementFeesValue = Double.parseDouble(object.getManagementFees());
//					}
//
//					Console.log("feePercent" + feePercent);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//
////				double managementFeesValue = object.getTotalPaidIntoEmployee()*( feePercent / 100);
////				Console.log("managementFeesValue"
////						+ managementFeesValue);
//				object.setManagementFees(managementFeesValue + "");
//
//				double managementTotal = managementFeesValue+object.getTotalPaidIntoEmployee();
//				Console.log("managementTotal" + managementTotal);
//				object.setTotalIncludingManagementFees(Math.round(managementTotal)
//						+ "");
//				
//				double grandTotalValue = managementTotal;
//				object.setGrandTotalOfCTC(Math.round(grandTotalValue) + "");
//				Console.log("grandTotalValue" + grandTotalValue);
//				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
//				table.redrawRow(index);
			}
		});
	}

	protected void addSPMWF() {
		// TODO Auto-generated method stub
//		if(ptAndLwfDropdownFlag){
//			SelectionCell cell = new SelectionCell(loadClwfListAsPerBranch(lwfList));
//			spMWF = new Column<StaffingDetails, String>(cell) {
//				@Override
//				public String getValue(StaffingDetails object) {
//					if (object.getSpMWF() != null) {
//						return object.getSpMWF() + "";
//					} else {
//						return 0 + "";
//					}
//				}
//			};
//		}else{
			EditTextCell editCell = new EditTextCell();
			spMWF = new Column<StaffingDetails, String>(editCell) {
				@Override
				public String getValue(StaffingDetails object) {
					if (object.getSpMWF() != null) {
						return object.getSpMWF() + "";
					} else {
						return 0 + "";
					}
				}
			};
//		}
//		table.addColumn(spMWF, "#SP MWF");
		table.addColumn(spMWF, "#CLWF/CMWF");
		table.setColumnWidth(spMWF, 120, Unit.PX);
		spMWF.setFieldUpdater(new FieldUpdater<StaffingDetails, String>() {

			@Override
			public void update(int index, StaffingDetails object, String value) {
				// TODO Auto-generated method stub
				object.setSpMWF(value);
//				object.setSpMWF(object.getSpMWF()+"");
//				double cBonusPercent=object.getcBonusPercentage();
//				double bonusValue = 0;
//				/**
//				 * Rahul Verma added paid days. And changed(21/12) to (paiddays/12) where paiddays is provided by User
//				 */
//				double paiddaysforBonus=Double.parseDouble(object.getPaidLeaveDays().trim());
//				if (object.isBonusRelever()) {
//					bonusValue = ((object.getBasic() + Double
//							.parseDouble(object.getDa())) * (cBonusPercent / 100))
//							+ (((object.getBasic() + Double
//									.parseDouble(object.getDa())) * (cBonusPercent / 100)) * ((paiddaysforBonus/12)/ 30));
//				} else {
//					bonusValue = ((object.getBasic() + Double
//							.parseDouble(object.getDa())) * (cBonusPercent / 100));// +(1.75/30);
//				}
//				Console.log("bonusValue" + bonusValue);
//				object.setSpBonus(object.getSpBonus());//Math.round(bonusValue) + "");
//				/** date 17.11.2018 added by komal for paid leave calculation as discussed with maradona**/
//				double paidLeavesValue = 0;
////				Console.log("form.cb_Relever.getValue()"
////						+ object.isRelever() );
//				/**
//				 * Rahul Verma added paid days. And changed(21/12) to (paiddays/12) where paiddays is provided by User
//				 */
//				double paiddays=Double.parseDouble(object.getPaidLeaveDays().trim());
//				double totalValue = Double.parseDouble(object.getSpEDLI())
//						+ Double.parseDouble(object.getSpESIC())
//						+ Double.parseDouble(object.getSpMWF())
//						+ Double.parseDouble(object.getSpPF());
//				totalValue = Math.round(totalValue*100.0)/100.0;
//				if (object.isPaidLeaveRelever()) {
//					paidLeavesValue = ((object.getGrossSalary() + (totalValue)) * ((paiddays/12)/30))
//															+
//							(((object.getGrossSalary() + (totalValue)))*((paiddays/12)/30))*( (paiddays/12)/ 30);
//				} else {
//					paidLeavesValue = (object.getGrossSalary() + (totalValue)) * ((paiddays/12)/30);
//				}
//				Console.log("paidLeavesValue" + paidLeavesValue);
//
//				object.setPaidLeaves((Math.round(paidLeavesValue*100.0)/100.0) + "");
//				/**
//				 *  end komal
//				 */
//				double statutoryPayments = Double
//						.parseDouble(object.getSpBonus())
//						+ Double.parseDouble(object.getSpEDLI())
//						+ Double.parseDouble(object.getSpESIC())
//						+ Double.parseDouble(object.getSpMWF())
//						+ Double.parseDouble(object.getSpPF())
//						+ Double.parseDouble(object.getPaidLeaves());
//				object.setSatutoryPayments((Math.round(statutoryPayments)) + "");
//				object.setTotal((Math.round((object.getGrossSalary()
//						+ Double.parseDouble(object
//								.getSatutoryPayments())))) + "");
////				double paidLeavesValue = 0;
//////				Console.log("form.cb_Relever.getValue()"
//////						+ object.isRelever() );
////				/**
////				 * Rahul Verma added paid days. And changed(21/12) to (paiddays/12) where paiddays is provided by User
////				 */
////				double paiddays=Double.parseDouble(object.getPaidLeaveDays().trim());
////				
////				if (object.isPaidLeaveRelever()) {
////					paidLeavesValue = ((object.getGrossSalary() + ((Double.parseDouble(object.getSatutoryPayments()))-Double.parseDouble(object.getSpBonus()))) * ((paiddays/12)/30))
////															+
////							(((object.getGrossSalary() + ((Double.parseDouble(object.getSatutoryPayments()))-Double.parseDouble(object.getSpBonus()))))*((paiddays/12)/30))*((paiddays/12)/ 30);
////				} else {
////					paidLeavesValue = (object.getGrossSalary() + ((Double.parseDouble(object.getSatutoryPayments()))-Double.parseDouble(object.getSpBonus()))) * ((paiddays/12)/30);
////				}
////				Console.log("paidLeavesValue" + paidLeavesValue);
////
////				object.setPaidLeaves(object.getPaidLeaves());//Math.round(paidLeavesValue) + "");
//				object.setEmployementOverHeadCost(object.getOverHeadCost());
//				object.setTravellingAllowanceOverhead(object.getTravellingAllowanceOverhead());
//				double totalPaidAndOverheadValue = Double
//						.parseDouble(object.getTotal())
//						+ object.getAdditionalAllowance()
//						+ Double.parseDouble(object
//								.getEmployementOverHeadCost())
//						+ object.getTravellingAllowanceOverhead();
//				Console.log("totalPaidAndOverheadValue"
//						+ totalPaidAndOverheadValue);
//				object.setTotalOfPaidAndOverhead((Math.round(totalPaidAndOverheadValue))
//						+ "");
//				object.setTotalPaidIntoEmployee(Math.round((totalPaidAndOverheadValue*object.getNoOfStaff())));
//				double feePercent = 0;
//				double managementFeesValue = 0;
//				try {
//					if (object.getManageMentfeesPercent()!=0) {
//						feePercent = object.getManageMentfeesPercent();
//						managementFeesValue = object.getTotalPaidIntoEmployee()*( feePercent / 100);
//						Console.log("managementFeesValue"
//								+ managementFeesValue);
//						//object.setManagementFees(managementFeesValue + "");
//
//					} else{
//						if(object.getManagementFees()!=null && !object.getManagementFees().equals(""))
//						managementFeesValue = Double.parseDouble(object.getManagementFees());
//					}
//					Console.log("feePercent" + feePercent);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//				object.setManagementFees(managementFeesValue + "");
//				
//				double managementTotal = managementFeesValue+object.getTotalPaidIntoEmployee();
//				Console.log("managementTotal" + managementTotal);
//				object.setTotalIncludingManagementFees((Math.round(managementTotal))
//						+ "");
//				
//				double grandTotalValue = managementTotal;
//				object.setGrandTotalOfCTC((Math.round(grandTotalValue*100.0)/100.0) + "");
//				Console.log("grandTotalValue" + grandTotalValue);
				
				/**
				 * @author Anil
				 * @since 18-09-2020
				 * commented below method and called upddateRow method for statutory payment update
				 */
//				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
//				table.redrawRow(index);
				updateRow(object, index,false);
			}
		});
	}

	protected void addSPEDLI() {
		// TODO Auto-generated method stub
		EditTextCell editCell = new EditTextCell();
		spEDLI = new Column<StaffingDetails, String>(editCell) {
			@Override
			public String getValue(StaffingDetails object) {
				if (object.getSpEDLI() != null) {
					return object.getSpEDLI() + "";
				} else {
					return 0 + "";
				}
			}
		};
//		table.addColumn(spEDLI, "#SP EDLI");
		table.addColumn(spEDLI, "#CEDLI");
		table.setColumnWidth(spEDLI, 120, Unit.PX);
		
		spEDLI.setFieldUpdater(new FieldUpdater<StaffingDetails, String>() {

			@Override
			public void update(int index, StaffingDetails object, String value) {
				// TODO Auto-generated method stub
				object.setSpEDLI(value);
				updateRow(object, index,false);
			}
		});
	}

	protected void addStatutoryPayments() {
		// TODO Auto-generated method stub
		EditTextCell editCell = new EditTextCell();
		statutoryPayments = new Column<StaffingDetails, String>(editCell) {
			@Override
			public String getValue(StaffingDetails object) {
				if (object.getSatutoryPayments() != null) {
					return object.getSatutoryPayments() + "";
				} else {
					return 0 + "";
				}
			}
		};
		table.addColumn(statutoryPayments, "#Statutory Payments");
		table.setColumnWidth(statutoryPayments, 120, Unit.PX);
	}

	// protected void addSPLWF() {}

	// protected void addSPPT() {
	// // TODO Auto-generated method stub
	// EditTextCell editCell=new EditTextCell();
	// spPT=new Column<StaffingDetails,String>(editCell)
	// {
	// @Override
	// public String getValue(StaffingDetails object)
	// {
	// if(object.getSpPT()!=null){
	// return 0+"";}
	// else{
	// return object.getSpPT()+"";}
	// }
	// };
	// table.addColumn(spPT,"#SP PT");
	// table.setColumnWidth(spPT, 120,Unit.PX);
	// }

	protected void addSPESIC() {
		// TODO Auto-generated method stub
		EditTextCell editCell = new EditTextCell();
		spESIC = new Column<StaffingDetails, String>(editCell) {
			@Override
			public String getValue(StaffingDetails object) {
				if (object.getSpESIC() != null && !object.getSpESIC().equals("")) {
					return (Math.round((Double.parseDouble(object.getSpESIC()))*100.0)/100.0) + "";
				} else {
					return 0 + "";
				}
			}
		};
//		table.addColumn(spESIC, "#SP ESIC");
		table.addColumn(spESIC, "#CESIC");
		table.setColumnWidth(spESIC, 120, Unit.PX);
	}

	protected void addSPPF() {
		// TODO Auto-generated method stub
		EditTextCell editCell = new EditTextCell();
		spPf = new Column<StaffingDetails, String>(editCell) {
			@Override
			public String getValue(StaffingDetails object) {
				if (object.getSpPF() != null) {
					return object.getSpPF() + "";
				} else {
					return 0 + "";
				}
			}
		};
//		spPf.setCellStyleNames("background-color:red");
//		table.addColumn(spPf, "#SP PF");
		table.addColumn(spPf, "#CPF");
		table.setColumnWidth(spPf, 120, Unit.PX);
		spPf.setFieldUpdater(new FieldUpdater<StaffingDetails, String>() {

			@Override
			public void update(int index, StaffingDetails object, String value) {
				// TODO Auto-generated method stub
				object.setSpPF(value);
				editedSPPF=true;
				updateRow(object, index,false);
			}
		});
	}

	protected void addStatutoryDeductions() {
		// TODO Auto-generated method stub
		EditTextCell editCell = new EditTextCell();
		statutoryDeduction = new Column<StaffingDetails, String>(editCell) {
			@Override
			public String getValue(StaffingDetails object) {
				if (object.getSatutoryDeductions() != null) {
					return object.getSatutoryDeductions() + "";
				} else {
					return 0 + "";
				}
			}
		};
		table.addColumn(statutoryDeduction, "#Statutory Deduction");
		table.setColumnWidth(statutoryDeduction, 120, Unit.PX);
	}

	protected void addSDLWF() {
		// TODO Auto-generated method stub
//		if(ptAndLwfDropdownFlag){
//			SelectionCell cell = new SelectionCell(loadLwfListAsPerBranch(lwfList));
//			sdLWF = new Column<StaffingDetails, String>(cell) {
//				@Override
//				public String getValue(StaffingDetails object) {
//					if (object.getSdLWF() != null) {
//						return object.getSdLWF() + "";
//					} else {
//						return 0 + "";
//					}
//				}
//			};
//		}else{
			EditTextCell editCell = new EditTextCell();
			sdLWF = new Column<StaffingDetails, String>(editCell) {
				@Override
				public String getValue(StaffingDetails object) {
					if (object.getSdLWF() != null) {
						return object.getSdLWF() + "";
					} else {
						return 0 + "";
					}
				}
			};
//		}
		table.addColumn(sdLWF, "#LWF/MWF");
		table.setColumnWidth(sdLWF, 120, Unit.PX);
		
		sdLWF.setFieldUpdater(new FieldUpdater<StaffingDetails, String>() {

			@Override
			public void update(int index, StaffingDetails object, String value) {
				// TODO Auto-generated method stub
				object.setSdLWF(value);
				
//				double satutoryDeduction = Double
//						.parseDouble(object.getSdPF())
//						+ Double.parseDouble(object.getSdESIC())
//						+ Double.parseDouble(object.getSdLWF())
//						+ Double.parseDouble(object.getSdPT());
//				Console.log("satutoryDeduction" + satutoryDeduction);
//				object.setSatutoryDeductions(Math.round(satutoryDeduction) + "");
//				
//				double netSalaryValue = object.getGrossSalary()
//						- Double.parseDouble(object
//								.getSatutoryDeductions().trim());
//
//				object.setNetSalary(Math.round(netSalaryValue));
//				
//				// C Leave
////				double cLeaveValue = ((object.getBasic() + Double
////						.parseDouble(object.getDa().trim()))*(1.75/30));
////				Console.log("cLeaveValue" + cLeaveValue);
//				/**
//				 * Rahul Verma added paid days. And changed(21/12) to (paiddays/12) where paiddays is provided by User
//				 */
//				double paiddaysValue=Double.parseDouble(object.getPaidLeaveDays().trim());
//				
//				if(object.iscLeaveApplicable()){
//					double cLeaveValue = ((object.getBasic() + Double
//							.parseDouble(object.getDa().trim()))*((paiddaysValue)/30));
//					Console.log("cLeaveValue" + cLeaveValue);
//					object.setcLeave((Math.round(cLeaveValue*100.0)/100.0)+"");
//				}else{
//					object.setcLeave(0+"");
//				}
////				object.setcLeave(object.getcLeave());
//				
//				double cBonusPercent = 0;
//				cBonusPercent = object.getcBonusPercentage();//Double.parseDouble(form.getTbcBonus().getValue().trim());
//				Console.log("cBonusPercent" + cBonusPercent);
//				double cBonusValue = ((object.getBasic() + Double
//						.parseDouble(object.getDa().trim())))
//						* (cBonusPercent / 100);
//				Console.log("cBonusValue" + cBonusValue);
//				object.setcBonus(object.getcBonus());//Math.round(cBonusValue) + "");
//				object.setTravellingAllowance(Math.round(object.getTravellingAllowance()*100.0)/100.0);
//				//Take Home Salary
//				double takeHomeValue = object.getNetSalary()
//						+ Double.parseDouble(object.getcLeave())
//						+ Double.parseDouble(object.getcBonus())
//						+ object.getTravellingAllowance();
//				Console.log("takeHomeValue" + takeHomeValue);
//				object.setTakeHome((Math.round(takeHomeValue)) + "");
//				// Statutory Payments
//				//pfValue
//				double cpfValue = ((object.getBasic() + Double.parseDouble(object.getDa())) / 100) * 12;
//				Console.log("cpfValue" + cpfValue);
//				// if(!editedSPPF)
//				object.setSpPF(object.getSpPF() + "");
//				
////				if (object.getGrossSalary() < 21001) {
////					double esicCalValue = object.getGrossSalary()-object.getWashingAllowance();
////					double esicValue = (esicCalValue / 100) * 4.75;
////					Console.log("c esicValue" + esicValue);
////					object.setSpESIC(Math.round(esicValue) + "");
////				} else {
//					object.setSpESIC(object.getSpESIC() + "");
////				}
//				
//				double edilCalValue = object.getBasic()
//						+ Double.parseDouble(object.getDa().trim());
//				double edilValue = edilCalValue * (1.16 / 100);
//				Console.log("edilValue" + edilValue);
//				object.setSpEDLI(object.getSpEDLI() + "");
//				object.setSpMWF(object.getSpMWF()+"");
//				
//				double bonusValue = 0;
//				/**
//				 * Rahul Verma added paid days. And changed(21/12) to (paiddays/12) where paiddays is provided by User
//				 */
//				double paiddaysforBonus=Double.parseDouble(object.getPaidLeaveDays().trim());
//				if (object.isPaidLeaveRelever()) {
//					bonusValue = ((object.getBasic() + Double
//							.parseDouble(object.getDa())) * (cBonusPercent / 100))
//							+ (((object.getBasic() + Double
//									.parseDouble(object.getDa())) * (cBonusPercent / 100)) * ((paiddaysforBonus/12) / 30));
//				} else {
//					bonusValue = ((object.getBasic() + Double
//							.parseDouble(object.getDa())) * (cBonusPercent / 100));// +(1.75/30);
//				}
//				Console.log("bonusValue" + bonusValue);
//				object.setSpBonus(object.getSpBonus());//Math.round(bonusValue) + "");
//				/** date 17.11.2018 added by komal for paid leave calculation as discussed with maradona**/
//				double paidLeavesValue = 0;
////				Console.log("form.cb_Relever.getValue()"
////						+ object.isRelever() );
//				/**
//				 * Rahul Verma added paid days. And changed(21/12) to (paiddays/12) where paiddays is provided by User
//				 */
//				double paiddays=Double.parseDouble(object.getPaidLeaveDays().trim());
//				double totalValue = Double.parseDouble(object.getSpEDLI())
//						+ Double.parseDouble(object.getSpESIC())
//						+ Double.parseDouble(object.getSpMWF())
//						+ Double.parseDouble(object.getSpPF());
//				totalValue = Math.round(totalValue*100.0)/100.0;
//				if (object.isPaidLeaveRelever()) {
//					paidLeavesValue = ((object.getGrossSalary() + (totalValue)) * ((paiddays/12)/30))
//															+
//							(((object.getGrossSalary() + (totalValue)))*((paiddays/12)/30))*( (paiddays/12)/ 30);
//				} else {
//					paidLeavesValue = (object.getGrossSalary() + (totalValue)) * ((paiddays/12)/30);
//				}
//				Console.log("paidLeavesValue" + paidLeavesValue);
//
//				object.setPaidLeaves((Math.round(paidLeavesValue*100.0)/100.0) + "");
//				/**
//				 *  end komal
//				 */
//				double statutoryPayments = Double
//						.parseDouble(object.getSpBonus())
//						+ Double.parseDouble(object.getSpEDLI())
//						+ Double.parseDouble(object.getSpESIC())
//						+ Double.parseDouble(object.getSpMWF())
//						+ Double.parseDouble(object.getSpPF())
//						+ Double.parseDouble(object.getPaidLeaves());
//				object.setSatutoryPayments((Math.round(statutoryPayments)) + "");
//				object.setTotal(Math.round((object.getGrossSalary()
//						+ Double.parseDouble(object
//								.getSatutoryPayments()))) + "");
////				double paidLeavesValue = 0;
//////				Console.log("form.cb_Relever.getValue()"
//////						+ object.isRelever() );
////				/**
////				 * Rahul Verma added paid days. And changed(21/12) to (paiddays/12) where paiddays is provided by User
////				 */
////				double paiddays=Double.parseDouble(object.getPaidLeaveDays().trim());
////				
////				if (object.isPaidLeaveRelever()) {
////					paidLeavesValue = ((object.getGrossSalary() + ((Double.parseDouble(object.getSatutoryPayments()))-Double.parseDouble(object.getSpBonus()))) * ((paiddays/12)/30))
////															+
////							(((object.getGrossSalary() + ((Double.parseDouble(object.getSatutoryPayments()))-Double.parseDouble(object.getSpBonus()))))*((paiddays/12)/30))*((paiddays/12)/ 30);
////				} else {
////					paidLeavesValue = (object.getGrossSalary() + ((Double.parseDouble(object.getSatutoryPayments()))-Double.parseDouble(object.getSpBonus()))) * ((paiddays/12)/30);
////				}
////				Console.log("paidLeavesValue" + paidLeavesValue);
////
////				object.setPaidLeaves(object.getPaidLeaves());//Math.round(paidLeavesValue) + "");
//				object.setEmployementOverHeadCost(object.getOverHeadCost());
//				object.setTravellingAllowanceOverhead(object.getTravellingAllowanceOverhead());
//				double totalPaidAndOverheadValue = Double
//						.parseDouble(object.getTotal())
//						+ object.getAdditionalAllowance()
//						+ Double.parseDouble(object
//								.getEmployementOverHeadCost())
//						+ object.getTravellingAllowanceOverhead();
//				Console.log("totalPaidAndOverheadValue"
//						+ totalPaidAndOverheadValue);
//				object.setTotalOfPaidAndOverhead((Math.round(totalPaidAndOverheadValue))
//						+ "");
//				object.setTotalPaidIntoEmployee(Math.round((totalPaidAndOverheadValue*object.getNoOfStaff())));
//				double feePercent = 0;
//				double managementFeesValue = 0;
//				try {
////					if (object.getManageMentfeesPercent()!=0) {
////						feePercent = object.getManageMentfeesPercent()
////					} 
//					if (object.getManageMentfeesPercent()!=0) {
//						feePercent = object.getManageMentfeesPercent();
//						//komal
//						managementFeesValue = object.getTotalPaidIntoEmployee()*( feePercent / 100);
//						Console.log("managementFeesValue"
//								+ managementFeesValue);
//						object.setManagementFees((Math.round(managementFeesValue*100.0)/100.0) + "");
//					} else{
//						if(object.getManagementFees()!=null && !object.getManagementFees().equals(""))
//							managementFeesValue = Double.parseDouble(object.getManagementFees());
//						}
//					Console.log("feePercent" + feePercent);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//
////				double managementFeesValue = object.getTotalPaidIntoEmployee()*( feePercent / 100);
////				Console.log("managementFeesValue"
////						+ managementFeesValue);
//				object.setManagementFees(managementFeesValue + "");
//
//				double managementTotal = managementFeesValue+object.getTotalPaidIntoEmployee();
//				Console.log("managementTotal" + managementTotal);
//				object.setTotalIncludingManagementFees((Math.round(managementTotal))
//						+ "");
//				
//				double grandTotalValue = managementTotal;
//				object.setGrandTotalOfCTC((Math.round(grandTotalValue*100.0)/100.0) + "");
//				Console.log("grandTotalValue" + grandTotalValue);
				
//				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
//				table.redrawRow(index);

				/**
				 * @author Anil
				 * @since 18-09-2020
				 */
				updateRow(object, index,false);
			}
		});
	}

	protected void addSDPT() {
		// TODO Auto-generated method stub
//		if(ptAndLwfDropdownFlag){
//			SelectionCell cell = new SelectionCell(loadPtListAsPerBranch(ptList));
//			sdPT = new Column<StaffingDetails, String>(cell) {
//				@Override
//				public String getValue(StaffingDetails object) {
//					if (object.getSdPT() != null&&!object.getSdPT().equals("")) {
//						return object.getSdPT() + "";
//					} else {
//						return 0 + "";
//					}
//				}
//			};
//		}else{
			EditTextCell editCell = new EditTextCell();
			sdPT = new Column<StaffingDetails, String>(editCell) {
				@Override
				public String getValue(StaffingDetails object) {
					if (object.getSdPT() != null) {
						return object.getSdPT() + "";
					} else {
						return 0 + "";
					}
				}
			};
//		}
//		table.addColumn(sdPT, "#SD PT");
		table.addColumn(sdPT, "#PT");
		table.setColumnWidth(sdPT, 120, Unit.PX);
		
		sdPT.setFieldUpdater(new FieldUpdater<StaffingDetails, String>() {

			@Override
			public void update(int index, StaffingDetails object, String value) {
				// TODO Auto-generated method stub
				object.setSdPT(value);
				
				/**
				 * @author Anil
				 * @since 18-09-2020
				 */
				updateRow(object, index,false);
				
//				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
//				table.redrawRow(index);
			}
		});
	}

	protected void addSDESIC() {
		// TODO Auto-generated method stub
		EditTextCell editCell = new EditTextCell();
		sdESIC = new Column<StaffingDetails, String>(editCell) {
			@Override
			public String getValue(StaffingDetails object) {
				if (object.getSdESIC() != null) {
					return object.getSdESIC() + "";
				} else {
					return 0 + "";
				}
			}
		};
//		table.addColumn(sdESIC, "#SD ESIC");
		table.addColumn(sdESIC, "#EESIC");
		table.setColumnWidth(sdESIC, 120, Unit.PX);
	}

	protected void addSDPF() {
		// TODO Auto-generated method stub
		EditTextCell editCell = new EditTextCell();
		sdPf = new Column<StaffingDetails, String>(editCell) {
			@Override
			public String getValue(StaffingDetails object) {
				if (object.getSdPF() != null) {
					return object.getSdPF() + "";
				} else {
					return 0 + "";
				}
			}
		};
//		table.addColumn(sdPf, "#SD PF");
		table.addColumn(sdPf, "#EPF");
		table.setColumnWidth(sdPf, 120, Unit.PX);
		
		/**
		 * @author Anil @since 10-02-2021
		 * Added field updated in pf updater
		 */
		sdPf.setFieldUpdater(new FieldUpdater<StaffingDetails, String>() {

			@Override
			public void update(int index, StaffingDetails object, String value) {
				
				object.setSdPF(value);
				updateRow(object, index, false);
			}
		});
	}

	protected void addTakeHome() {
		// TODO Auto-generated method stub
		EditTextCell editCell = new EditTextCell();
		takeHome = new Column<StaffingDetails, String>(editCell) {
			@Override
			public String getValue(StaffingDetails object) {
				if (object.getTakeHome() != null) {
					return object.getTakeHome() + "";
				} else {
					return 0 + "";
				}
			}
		};
		table.addColumn(takeHome, "#TakeHome");
		table.setColumnWidth(takeHome, 120, Unit.PX);
	}

	protected void addCBonus() {
		// TODO Auto-generated method stub
		EditTextCell editCell = new EditTextCell();
		cBonus = new Column<StaffingDetails, String>(editCell) {
			@Override
			public String getValue(StaffingDetails object) {
				if (object.iscBonusApplicable()&&object.getcBonus() != null) {
					return object.getcBonus() + "";
				} else {
					return 0 + "";
				}
			}
		};
		table.addColumn(cBonus, "#CBonus");
		table.setColumnWidth(cBonus, 120, Unit.PX);
		cBonus.setFieldUpdater(new FieldUpdater<StaffingDetails, String>() {

			@Override
			public void update(int index, StaffingDetails object, String value) {
				// TODO Auto-generated method stub
//				editedcBonus=true;
				object.setcBonus(value);
				
				/**
				 * @author Anil
				 * @since  19-09-2020
				 */
				if(object.iscBonusApplicable()&&object.getcBonus()!=null){
					double bonusEsic=((Double.parseDouble(object.getcBonus())*0.75)/100);
					object.setBonusEsic(bonusEsic);
				}else{
					object.setBonusEsic(0);
				}
				
				
//				object.setTravellingAllowance(Math.round(object.getTravellingAllowance()*100.0)/100.0);
//				//Take Home Salary
//				double takeHomeValue = object.getNetSalary()
//						+ Double.parseDouble(object.getcLeave())
//						+ Double.parseDouble(object.getcBonus())
//						+ object.getTravellingAllowance();
//				Console.log("takeHomeValue" + takeHomeValue);
//				object.setTakeHome((Math.round(takeHomeValue)) + "");
//				// Statutory Payments
//				//pfValue
//				double cpfValue = ((object.getBasic() + Double.parseDouble(object.getDa())) / 100) * 12;
//				Console.log("cpfValue" + cpfValue);
//				// if(!editedSPPF)
//				object.setSpPF(object.getSpPF() + "");
//				
////				if (object.getGrossSalary() < 21001) {
////					double esicCalValue = object.getGrossSalary()-object.getWashingAllowance();
////					double esicValue = (esicCalValue / 100) * 4.75;
////					Console.log("c esicValue" + esicValue);
////					object.setSpESIC(Math.round(esicValue) + "");
////				} else {
//					object.setSpESIC(object.getSpESIC() + "");
////				}
//				
//				double edilCalValue = object.getBasic()
//						+ Double.parseDouble(object.getDa().trim());
//				double edilValue = edilCalValue * (1.16 / 100);
//				Console.log("edilValue" + edilValue);
//				object.setSpEDLI(object.getSpEDLI() + "");
//				object.setSpMWF(object.getSpMWF()+"");
//				double cBonusPercent = object.getcBonusPercentage();
//				double bonusValue = 0;
//				/**
//				 * Rahul Verma added paid days. And changed(21/12) to (paiddays/12) where paiddays is provided by User
//				 */
//				double paiddaysforBonus=Double.parseDouble(object.getPaidLeaveDays().trim());
//				if (object.isBonusRelever()) {
//					bonusValue = ((object.getBasic() + Double
//							.parseDouble(object.getDa())) * (cBonusPercent / 100))
//							+ (((object.getBasic() + Double
//									.parseDouble(object.getDa())) * (cBonusPercent / 100)) * ((paiddaysforBonus/12) / 30));
//				} else {
//					bonusValue = ((object.getBasic() + Double
//							.parseDouble(object.getDa())) * (cBonusPercent / 100));// +(1.75/30);
//				}
//				Console.log("bonusValue" + bonusValue);
//				object.setSpBonus((Math.round(bonusValue*100.0)/100.0) + "");
//				/** date 17.11.2018 added by komal for paid leave calculation as discussed with maradona**/
//				double paidLeavesValue = 0;
////				Console.log("form.cb_Relever.getValue()"
////						+ object.isRelever() );
//				/**
//				 * Rahul Verma added paid days. And changed(21/12) to (paiddays/12) where paiddays is provided by User
//				 */
//				double paiddays=Double.parseDouble(object.getPaidLeaveDays().trim());
//				double totalValue = Double.parseDouble(object.getSpEDLI())
//						+ Double.parseDouble(object.getSpESIC())
//						+ Double.parseDouble(object.getSpMWF())
//						+ Double.parseDouble(object.getSpPF());
//				totalValue = Math.round(totalValue*100.0)/100.0;
//				if (object.isPaidLeaveRelever()) {
//					paidLeavesValue = ((object.getGrossSalary() + (totalValue)) * ((paiddays/12)/30))
//															+
//							(((object.getGrossSalary() + (totalValue)))*((paiddays/12)/30))*( (paiddays/12)/ 30);
//				} else {
//					paidLeavesValue = (object.getGrossSalary() + (totalValue)) * ((paiddays/12)/30);
//				}
//				Console.log("paidLeavesValue" + paidLeavesValue);
//
//				object.setPaidLeaves((Math.round(paidLeavesValue*100.0)/100.0) + "");
//				/**
//				 *  end komal
//				 */
//				
//
//				/**
//				 * @author Anil
//				 * @since 04-09-2020
//				 */
//				CTCTemplate ctcTemplate=ctcTemplateMap.get(object.getCtcTemplates());
//				if(paidleave!=null){
//					double plAmt=0;
//					if(paidleave!=null){
//						for(OtEarningComponent plComp:paidleave.getOtEarningCompList()){
//							System.out.println("NAME : "+plComp.getComponentName());
//							for(CtcComponent earningComp:ctcTemplate.getEarning()){
//								System.out.println("EARN NAME : "+earningComp.getName());
//								if(plComp.getComponentName().equalsIgnoreCase("Gross Earning")){
//									plAmt=plAmt+earningComp.getAmount()/12;
//								}else if(plComp.getComponentName().equals(earningComp.getName())){
//									System.out.println("AMT : "+earningComp.getAmount()/12);
//									plAmt=plAmt+earningComp.getAmount()/12;
//								}
//								
//							}
//						}
//					}
//					System.out.println("BASE AMT : "+plAmt);
//					plAmt=plAmt*paidleave.getRate()/100;
//					System.out.println("PL AMT : "+plAmt);
//					object.setPaidLeaves((Math.round(plAmt*100.0)/100.0)+"");
//				}
//				
//				/**Date 24-6-2019 
//				 * @author Amol
//				 * adding gratuity formula
//				 */
//				double gratuity=0;
////				if(object.isGratuityApplicable()){
////				gratuity
////					=((((object.getBasic())+Double.parseDouble(object.getDa()))*4.81)/(100));
////					object.setGratuity((Math.round(gratuity)+""));
////				  }else{
////					  object.setGratuity(0+"");
////				 }
//				 object.setGratuity(object.getGratuity());
//				 object.setSupervision(object.getSupervision());
//				 object.setTrainingCharges(object.getTrainingCharges());
//				
//				
//				double statutoryPayments = Double
//						.parseDouble(object.getSpBonus())
//						+ Double.parseDouble(object.getSpEDLI())
//						+ Double.parseDouble(object.getSpESIC())
//						+ Double.parseDouble(object.getSpMWF())
//						+ Double.parseDouble(object.getSpPF())
//						+ Double.parseDouble(object.getPaidLeaves())
//				        +Double.parseDouble(object.getGratuity())
//				        +(object.getAdministrativecost())
////				        +(object.getUniformCost())
//				        +(object.getSupervision())
//				        +(object.getTrainingCharges());
//				
//				object.setSatutoryPayments((Math.round(statutoryPayments)) + "");
//				object.setTotal((Math.round((object.getGrossSalary()
//						+ Double.parseDouble(object
//								.getSatutoryPayments())))) + "");
////				double paidLeavesValue = 0;
//////				Console.log("form.cb_Relever.getValue()" 
//////						+ object.isRelever() );
////				/**
////				 * Rahul Verma added paid days. And changed(21/12) to (paiddays/12) where paiddays is provided by User
////				 */
////				double paiddays=Double.parseDouble(object.getPaidLeaveDays().trim());
////				
////				if (object.isPaidLeaveRelever()) {
////					paidLeavesValue = ((object.getGrossSalary() + ((Double.parseDouble(object.getSatutoryPayments()))-Double.parseDouble(object.getSpBonus()))) * ((paiddays/12)/30))
////															+
////							(((object.getGrossSalary() + ((Double.parseDouble(object.getSatutoryPayments()))-Double.parseDouble(object.getSpBonus()))))*((paiddays/12)/30))*((paiddays/12)/ 30);
////				} else {
////					paidLeavesValue = (object.getGrossSalary() + ((Double.parseDouble(object.getSatutoryPayments()))-Double.parseDouble(object.getSpBonus()))) * ((paiddays/12)/30);
////				}
////				Console.log("paidLeavesValue" + paidLeavesValue);
////
////				object.setPaidLeaves((Math.round(paidLeavesValue*100.0)/100.0) + "");
//				object.setEmployementOverHeadCost(object.getOverHeadCost());
//				object.setTravellingAllowanceOverhead(object.getTravellingAllowanceOverhead());
//				object.setUniformCost(object.getUniformCost());
//				double totalPaidAndOverheadValue = Double
//						.parseDouble(object.getTotal())
//						+ object.getAdditionalAllowance()
//						+ Double.parseDouble(object
//								.getEmployementOverHeadCost())
//						+ object.getTravellingAllowanceOverhead()+object.getUniformCost();
//				Console.log("totalPaidAndOverheadValue"
//						+ totalPaidAndOverheadValue);
//				object.setTotalOfPaidAndOverhead((Math.round(totalPaidAndOverheadValue))
//						+ "");
//				object.setTotalPaidIntoEmployee(Math.round((totalPaidAndOverheadValue*object.getNoOfStaff())));
//				double feePercent = 0;
//				double managementFeesValue = 0;
//				try {
////					if (object.getManageMentfeesPercent()!=0) {
////						feePercent = object.getManageMentfeesPercent();
////					} 
//					if (object.getManageMentfeesPercent()!=0) {
//						feePercent = object.getManageMentfeesPercent();
//						//komal
//						managementFeesValue = object.getTotalPaidIntoEmployee()*( feePercent / 100);
//						Console.log("managementFeesValue"
//								+ managementFeesValue);
//						object.setManagementFees((Math.round(managementFeesValue*100.0)/100.0) + "");
//					} else{
//						if(object.getManagementFees()!=null && !object.getManagementFees().equals("")){
//							managementFeesValue = Double.parseDouble(object.getManagementFees());
//						}
//						/**
//						 * @author Anil
//						 * @since 03-09-2020
//						 */
//						if(managementFeesAsPerStaff&&object.getNoOfStaff()!=0&&object.getManagementFeesRate()!=0){
//							managementFeesValue=object.getNoOfStaff()*object.getManagementFeesRate();
//						}
//					}
//					Console.log("feePercent" + feePercent);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//
////				double managementFeesValue = object.getTotalPaidIntoEmployee()*( feePercent / 100);
////				Console.log("managementFeesValue"
////						+ managementFeesValue);
//				object.setManagementFees(managementFeesValue + "");
//
//				double managementTotal = managementFeesValue+object.getTotalPaidIntoEmployee();
//				Console.log("managementTotal" + managementTotal);
//				object.setTotalIncludingManagementFees((Math.round(managementTotal))
//						+ "");
//				
//				double grandTotalValue = managementTotal;
//				object.setGrandTotalOfCTC((Math.round(grandTotalValue*100.0)/100.0) + "");
//				Console.log("grandTotalValue" + grandTotalValue);
//				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
//				table.redrawRow(index);
				
				updateRow(object, index, false);
			}
		});
	}

	protected void addCLeaves() {
		// TODO Auto-generated method stub
		EditTextCell editCell = new EditTextCell();
		cLeave = new Column<StaffingDetails, String>(editCell) {
			@Override
			public String getValue(StaffingDetails object) {
				if (object.iscLeaveApplicable()&&object.getcLeave() != null) {
					return object.getcLeave() + "";
				} else {
					return 0 + "";
				}
			}
		};
		table.addColumn(cLeave, "#CLeave");
		table.setColumnWidth(cLeave, 120, Unit.PX);
		cLeave.setFieldUpdater(new FieldUpdater<StaffingDetails, String>() {

			@Override
			public void update(int index, StaffingDetails object, String value) {
				// TODO Auto-generated method stub
//				editedcLeaves=true;
				if(object.iscLeaveApplicable()){
					object.setcLeave(value);
				}else{
					object.setcLeave(0+"");
				}
			
				/**
				 * @author Anil
				 * @since 24-08-2020
				 */
				if(paidleave!=null){
					object.setcLeave(value);
				}
				
				/**
				 * @author Anil
				 * @since 19-09-2020
				 * Updated below code for added new code
				 */
				if(object.iscLeaveApplicable()&&object.getcLeave()!=null){
					double plEsic=((Double.parseDouble(object.getcLeave())*0.75)/100);
					Console.log("#PL ESIC Value "+plEsic);
					object.setPlEsic(plEsic);
				}else{
					object.setPlEsic(0);
				}
				
//				double cBonusPercent = 0;
//				cBonusPercent = object.getcBonusPercentage();//Double.parseDouble(form.getTbcBonus().getValue().trim());
//				Console.log("cBonusPercent" + cBonusPercent);
//				double cBonusValue = ((object.getBasic() + Double
//						.parseDouble(object.getDa().trim())))
//						* (cBonusPercent / 100);
//				Console.log("cBonusValue" + cBonusValue);
////				if(!editedcBonus)
//				object.setcBonus(object.getcBonus() + "");
//				
//				object.setTravellingAllowance(Math.round(object.getTravellingAllowance()*100.0)/100.0);
//				//Take Home Salary
//				double takeHomeValue = object.getNetSalary()
//						+ Double.parseDouble(object.getcLeave())
//						+ Double.parseDouble(object.getcBonus())
//						+ object.getTravellingAllowance();
//				Console.log("takeHomeValue" + takeHomeValue);
//				object.setTakeHome((Math.round(takeHomeValue)) + "");
//				// Statutory Payments
//				//pfValue
//				double cpfValue = ((object.getBasic() + Double.parseDouble(object.getDa())) / 100) * 12;
//				Console.log("cpfValue" + cpfValue);
//				// if(!editedSPPF)
//				object.setSpPF(object.getSpPF() + "");
//				
////				if (object.getGrossSalary() < 21001) {
////					double esicCalValue = object.getGrossSalary()-object.getWashingAllowance();
////					double esicValue = (esicCalValue / 100) * 4.75;
////					Console.log("c esicValue" + esicValue);
////					object.setSpESIC(Math.round(esicValue) + "");
////				} else {
//					object.setSpESIC(object.getSpESIC() + "");
////				}
//				
//				double edilCalValue = object.getBasic()
//						+ Double.parseDouble(object.getDa().trim());
//				double edilValue = edilCalValue * (1.16 / 100);
//				Console.log("edilValue" + edilValue);
//				object.setSpEDLI(object.getSpEDLI() + "");
//				object.setSpMWF(object.getSpMWF()+"");
//				
//				double bonusValue = 0;
//				/**
//				 * Rahul Verma added paid days. And changed(21/12) to (paiddays/12) where paiddays is provided by User
//				 */
//				double paiddaysforBonus=Double.parseDouble(object.getPaidLeaveDays().trim());
//				if (object.isBonusRelever()) {
//					bonusValue = ((object.getBasic() + Double
//							.parseDouble(object.getDa())) * (cBonusPercent / 100))
//							+ (((object.getBasic() + Double
//									.parseDouble(object.getDa())) * (cBonusPercent / 100)) * ((paiddaysforBonus/12) / 30));
//				} else {
//					bonusValue = ((object.getBasic() + Double
//							.parseDouble(object.getDa())) * (cBonusPercent / 100));// +(1.75/30);
//				}
//				Console.log("bonusValue" + bonusValue);
//				object.setSpBonus((Math.round(bonusValue*100.0)/100.0) + "");
//				/** date 17.11.2018 added by komal for paid leave calculation as discussed with maradona**/
//				double paidLeavesValue = 0;
////				Console.log("form.cb_Relever.getValue()"
////						+ object.isRelever() );
//				/**
//				 * Rahul Verma added paid days. And changed(21/12) to (paiddays/12) where paiddays is provided by User
//				 */
//				double paiddays=Double.parseDouble(object.getPaidLeaveDays().trim());
//				double totalValue = Double.parseDouble(object.getSpEDLI())
//						+ Double.parseDouble(object.getSpESIC())
//						+ Double.parseDouble(object.getSpMWF())
//						+ Double.parseDouble(object.getSpPF());
//				totalValue = Math.round(totalValue*100.0)/100.0;
//				if (object.isPaidLeaveRelever()) {
//					paidLeavesValue = ((object.getGrossSalary() + (totalValue)) * ((paiddays/12)/30))
//															+
//							(((object.getGrossSalary() + (totalValue)))*((paiddays/12)/30))*( (paiddays/12)/ 30);
//				} else {
//					paidLeavesValue = (object.getGrossSalary() + (totalValue)) * ((paiddays/12)/30);
//				}
//				Console.log("paidLeavesValue" + paidLeavesValue);
//
////				object.setPaidLeaves((Math.round(paidLeavesValue*100.0)/100.0) + "");
//				
//				/**
//				 * @author Anil
//				 * @since 24-08-2020
//				 */
//				if(paidleave==null){
//					object.setPaidLeaves((Math.round(paidLeavesValue*100.0)/100.0) + "");
//				}
//				
//				/**
//				 *  end komal
//				 */
//				/**Date 24-6-2019 
//				 * @author Amol
//				 * adding gratuity formula
//				 */
//				double gratuity=0;
////				if(object.isApplicable()){
////				gratuity
////					=((((object.getBasic())+Double.parseDouble(object.getDa()))*4.81)/(100));
////					object.setGratuity((Math.round(gratuity)+""));
////				  }else{
////					  object.setGratuity(0+"");
////				 }
//				object.setGratuity(object.getGratuity());
//				object.setSupervision(object.getSupervision());
//				object.setTrainingCharges(object.getTrainingCharges());
//				double statutoryPayments = Double
//						.parseDouble(object.getSpBonus())
//						+ Double.parseDouble(object.getSpEDLI())
//						+ Double.parseDouble(object.getSpESIC())
//						+ Double.parseDouble(object.getSpMWF())
//						+ Double.parseDouble(object.getSpPF())
//						+ Double.parseDouble(object.getPaidLeaves())
//				        +Double.parseDouble(object.getGratuity())
//				        +(object.getAdministrativecost())
////				        +(object.getUniformCost())
//				        +(object.getSupervision())
//				        +(object.getTrainingCharges());
//				
//				
//				object.setSatutoryPayments((Math.round(statutoryPayments)) + "");
//				object.setTotal((Math.round((object.getGrossSalary()
//						+ Double.parseDouble(object
//								.getSatutoryPayments())))) + "");
////				double paidLeavesValue = 0;
//////				Console.log("form.cb_Relever.getValue()"
//////						+ object.isRelever() );
////				/**
////				 * Rahul Verma added paid days. And changed(21/12) to (paiddays/12) where paiddays is provided by User
////				 */
////				double paiddays=Double.parseDouble(object.getPaidLeaveDays().trim());
////				
////				if (object.isPaidLeaveRelever()) {
////					paidLeavesValue = ((object.getGrossSalary() + ((Double.parseDouble(object.getSatutoryPayments()))-Double.parseDouble(object.getSpBonus()))) * ((paiddays/12)/30))
////															+
////							(((object.getGrossSalary() + ((Double.parseDouble(object.getSatutoryPayments()))-Double.parseDouble(object.getSpBonus()))))*((paiddays/12)/30))*((paiddays/12)/ 30);
////				} else {
////					paidLeavesValue = (object.getGrossSalary() + ((Double.parseDouble(object.getSatutoryPayments()))-Double.parseDouble(object.getSpBonus()))) * ((paiddays/12)/30);
////				}
////				Console.log("paidLeavesValue" + paidLeavesValue);
////
////				object.setPaidLeaves((Math.round(paidLeavesValue*100.0)/100.0) + "");
//				object.setEmployementOverHeadCost(object.getOverHeadCost());
//				object.setTravellingAllowanceOverhead(object.getTravellingAllowanceOverhead());
//				object.setUniformCost(object.getUniformCost());
//				double totalPaidAndOverheadValue = Double
//						.parseDouble(object.getTotal())
//						+ object.getAdditionalAllowance()
//						+ Double.parseDouble(object
//								.getEmployementOverHeadCost())
//						+ object.getTravellingAllowanceOverhead()+object.getUniformCost();
//				Console.log("totalPaidAndOverheadValue"
//						+ totalPaidAndOverheadValue);
//				object.setTotalOfPaidAndOverhead((Math.round(totalPaidAndOverheadValue))
//						+ "");
//				object.setTotalPaidIntoEmployee(Math.round((totalPaidAndOverheadValue*object.getNoOfStaff())));
//				double feePercent = 0;
//				double managementFeesValue = 0;
//				try {
////					if (object.getManageMentfeesPercent()!=0) {
////						feePercent = object.getManageMentfeesPercent();
////					} 
//					if (object.getManageMentfeesPercent()!=0) {
//						feePercent = object.getManageMentfeesPercent();
//						//komal
//						managementFeesValue = object.getTotalPaidIntoEmployee()*( feePercent / 100);
//						Console.log("managementFeesValue"
//								+ managementFeesValue);
//						object.setManagementFees((Math.round(managementFeesValue*100.0)/100.0) + "");
//					} else{
//						if(object.getManagementFees()!=null && !object.getManagementFees().equals("")){
//							managementFeesValue = Double.parseDouble(object.getManagementFees());
//						}
//						/**
//						 * @author Anil
//						 * @since 03-09-2020
//						 */
//						if(managementFeesAsPerStaff&&object.getNoOfStaff()!=0&&object.getManagementFeesRate()!=0){
//							managementFeesValue=object.getNoOfStaff()*object.getManagementFeesRate();
//						}
//					}
//					Console.log("feePercent" + feePercent);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//
////				double managementFeesValue = object.getTotalPaidIntoEmployee()*( feePercent / 100);
////				Console.log("managementFeesValue"
////						+ managementFeesValue);
//				object.setManagementFees(managementFeesValue + "");
//
//				double managementTotal = managementFeesValue+object.getTotalPaidIntoEmployee();
//				Console.log("managementTotal" + managementTotal);
//				object.setTotalIncludingManagementFees((Math.round(managementTotal))
//						+ "");
//				
//				double grandTotalValue = managementTotal;
//				object.setGrandTotalOfCTC((Math.round(grandTotalValue*100.0)/100.0) + "");
//				Console.log("grandTotalValue" + grandTotalValue);
//				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
//				table.redrawRow(index);
				
				updateRow(object, index,false);
			}
		});
	}

	protected void addBandP() {
		// TODO Auto-generated method stub
		EditTextCell editCell = new EditTextCell();
		bandP = new Column<StaffingDetails, String>(editCell) {
			@Override
			public String getValue(StaffingDetails object) {
				if (object.getBandP() != null) {
					return object.getBandP() + "";
				} else {
					return 0 + "";
				}
			}
		};
		table.addColumn(bandP, "#B&P");
		table.setColumnWidth(bandP, 120, Unit.PX);
		bandP.setFieldUpdater(new FieldUpdater<StaffingDetails, String>() {

			@Override
			public void update(int index, StaffingDetails object, String value) {
				// TODO Auto-generated method stub
				if (!value.trim().equals("")) {
					try {
						object.setBandP(value.trim());
					} catch (Exception e) {
						object.setBandP(0 + "");
					}
				} else {
					object.setBandP(0 + "");
				}
				editedBandP=true;
				updateRow(object, index,true);
//				table.redrawRow(index);
			}
		});
	}

	protected void addConyevance() {
		// TODO Auto-generated method stub
		EditTextCell editCell = new EditTextCell();
		conveyance = new Column<StaffingDetails, String>(editCell) {
			@Override
			public String getValue(StaffingDetails object) {
				if (object.getConveyance() != null) {
					return object.getConveyance() + "";
				} else {
					return 0 + "";
				}
			}
		};
		table.addColumn(conveyance, "#Conveyance");
		table.setColumnWidth(conveyance, 120, Unit.PX);
		conveyance.setFieldUpdater(new FieldUpdater<StaffingDetails, String>() {

			@Override
			public void update(int index, StaffingDetails object, String value) {
				// TODO Auto-generated method stub
				if (!value.trim().equals("")) {
					try {
						object.setConveyance(value.trim());
					} catch (Exception e) {
						object.setConveyance(0 + "");
					}
				} else {
					object.setConveyance(0 + "");
				}
				editedConveyance=true;
				updateRow(object, index,true);
//				
//				table.redrawRow(index);
			}
		});
	}

	protected void addWashingAllowance() {
		// TODO Auto-generated method stub
		EditTextCell editCell = new EditTextCell();
		washingAllowance = new Column<StaffingDetails, String>(editCell) {
			@Override
			public String getValue(StaffingDetails object) {
				if (object.getWashingAllowance() != 0) {
					return object.getWashingAllowance() + "";
				} else {
					return 0 + "";
				}
			}
		};
		table.addColumn(washingAllowance, "#Washing Allowance");
		table.setColumnWidth(washingAllowance, 120, Unit.PX);
		washingAllowance
				.setFieldUpdater(new FieldUpdater<StaffingDetails, String>() {

					@Override
					public void update(int index, StaffingDetails object,
							String value) {
						// TODO Auto-generated method stub
						if (!value.trim().equals("")) {
							try {
								object.setWashingAllowance(Double
										.parseDouble(value.trim()));
							} catch (Exception e) {
								object.setWashingAllowance(0);
							}
						} else {
							object.setWashingAllowance(0);
						}
						editedWashingAllowance=true;
						updateRow(object, index,true);
//						table.redrawRow(index);
					}
				});
	}
	
	
	/***Date 26-2-2019 added by Amol for CCA column update***/

protected void addCityCompensatoryAllowance(){
	
	// TODO Auto-generated method stub
	EditTextCell editCell = new EditTextCell();
	cityCompensatoryAllowance = new Column<StaffingDetails, String>(editCell) {
		@Override
		public String getValue(StaffingDetails object) {
			if (object.getCityCompensatoryAllowance() != 0) {
				return object.getCityCompensatoryAllowance() + "";
			} else {
				return 0.00 + "";
			}
		}
	};
	table.addColumn(cityCompensatoryAllowance, " #City Compensatory Allowance (CCA)");
	table.setColumnWidth(cityCompensatoryAllowance, 120, Unit.PX);
	cityCompensatoryAllowance.setFieldUpdater(new FieldUpdater<StaffingDetails, String>() {

				@Override
				public void update(int index, StaffingDetails object,
						String value) {
					// TODO Auto-generated method stub
					if (!value.trim().equals("")) {
						try {
							object.setCityCompensatoryAllowance(Double
									.parseDouble(value.trim()));
						} catch (Exception e) {
							object.setCityCompensatoryAllowance(0);
						}
					} else {
						object.setCityCompensatoryAllowance(0);
					}
					editedcityCompensatoryAllowance=true;
					updateRow(object, index,true);
//					table.redrawRow(index);
				}
			});
	}
	
      protected void addLeaveSalary(){
		
		// TODO Auto-generated method stub
		EditTextCell editCell = new EditTextCell();
		leaveSalary = new Column<StaffingDetails, String>(editCell) {
			@Override
			public String getValue(StaffingDetails object) {
				if (object.getLeaveSalary()!= 0) {
					return object.getLeaveSalary()+ "";
				} else {
					return 0.00 + "";
				}
			}
		};
		table.addColumn(leaveSalary, " #Leave Salary");
		table.setColumnWidth(leaveSalary, 120, Unit.PX);
		leaveSalary.setFieldUpdater(new FieldUpdater<StaffingDetails, String>() {

					@Override
					public void update(int index, StaffingDetails object,
							String value) {
						// TODO Auto-generated method stub
						if (!value.trim().equals("")) {
							try {
								object.setLeaveSalary(Double
										.parseDouble(value.trim()));
							} catch (Exception e) {
								object.setLeaveSalary(0);
							}
						} else {
							object.setLeaveSalary(0);
						}
						editedLeaveSalary=true;
						updateRow(object, index,false);
//						table.redrawRow(index);
					}
				});
		}












	protected void addMedical() {
		// TODO Auto-generated method stub
		EditTextCell editCell = new EditTextCell();
		medical = new Column<StaffingDetails, String>(editCell) {
			@Override
			public String getValue(StaffingDetails object) {
				if (object.getMedical() != null) {
					return object.getMedical() + "";
				} else {
					return 0 + "";
				}
			}
		};
		table.addColumn(medical, "#Medical");
		table.setColumnWidth(medical, 120, Unit.PX);
		medical.setFieldUpdater(new FieldUpdater<StaffingDetails, String>() {

			@Override
			public void update(int index, StaffingDetails object, String value) {
				// TODO Auto-generated method stub
				if (!value.trim().equals("")) {
					try {
						object.setMedical(value.trim());
					} catch (Exception e) {
						object.setMedical(0 + "");
					}
				} else {
					object.setMedical(0 + "");
				}
				editedMedical=true;
				updateRow(object, index,true);
//				table.redrawRow(index);
			}
		});
	}

	private void addUniformCost() {
		// TODO Auto-generated method stub
		EditTextCell editCell = new EditTextCell();
		uniformCost = new Column<StaffingDetails, String>(editCell) {
			@Override
			public String getValue(StaffingDetails object) {
				if (object.getUniformCost() != 0) {
					return object.getUniformCost() + "";
				} else {
					return 0 + "";
				}
			}
		};
		table.addColumn(uniformCost, "#Uniform Cost");
		table.setColumnWidth(uniformCost, 120, Unit.PX);
		uniformCost.setFieldUpdater(new FieldUpdater<StaffingDetails, String>() {

			@Override
			public void update(int index, StaffingDetails object,
					String value) {
				// TODO Auto-generated method stub
				try{
					object.setUniformCost(Double.parseDouble(value));
				}catch(Exception e){
					e.printStackTrace();
					object.setUniformCost(0);
				}
//				updateRow(object, index);
//				object.setTravellingAllowanceOverhead(object.getTravellingAllowanceOverhead());
				double totalPaidAndOverheadValue = Double
						.parseDouble(object.getTotal())
						+ object.getAdditionalAllowance()
						+ Double.parseDouble(object
								.getEmployementOverHeadCost())
						+ object.getTravellingAllowanceOverhead()+object.getUniformCost();
				Console.log("totalPaidAndOverheadValue"
						+ totalPaidAndOverheadValue);
				object.setTotalOfPaidAndOverhead((Math.round(totalPaidAndOverheadValue))
						+ "");
				object.setTotalPaidIntoEmployee(Math.round((totalPaidAndOverheadValue*object.getNoOfStaff())));
				double feePercent = 0;
				double managementFeesValue =0;
				try {
//					if (object.getManageMentfeesPercent()!=0) {
//						feePercent = object.getManageMentfeesPercent();
//					} 
					if (object.getManageMentfeesPercent()!=0) {
						feePercent = object.getManageMentfeesPercent();
						//komal
						managementFeesValue = object.getTotalPaidIntoEmployee()*( feePercent / 100);
						Console.log("managementFeesValue"
								+ managementFeesValue);
						//object.setManagementFees(Math.round(managementFeesValue) + "");
					} else{
						if(object.getManagementFees()!=null && !object.getManagementFees().equals("")){
							managementFeesValue = Double.parseDouble(object.getManagementFees());
						}
						/**
						 * @author Anil
						 * @since 03-09-2020
						 */
						if(managementFeesAsPerStaff&&object.getNoOfStaff()!=0&&object.getManagementFeesRate()!=0){
							managementFeesValue=object.getNoOfStaff()*object.getManagementFeesRate();
						}
					}
					Console.log("feePercent" + feePercent);
				} catch (Exception e) {
					e.printStackTrace();
				}

//				double managementFeesValue = object.getTotalPaidIntoEmployee()*( feePercent / 100);
//				Console.log("managementFeesValue"
//						+ managementFeesValue);
		        object.setManagementFees(managementFeesValue + "");

				double managementTotal = managementFeesValue+object.getTotalPaidIntoEmployee();
				Console.log("managementTotal" + managementTotal);
				object.setTotalIncludingManagementFees((Math.round(managementTotal))
						+ "");
				
				double grandTotalValue = managementTotal;
				object.setGrandTotalOfCTC((Math.round(grandTotalValue*100.0)/100.0) + "");
				Console.log("grandTotalValue" + grandTotalValue);
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				table.redrawRow(index);
			}
		});
	}

	private void addChargeableOTRates() {
		// TODO Auto-generated method stub
		EditTextCell editCell = new EditTextCell();
		chargableOTRates = new Column<StaffingDetails, String>(editCell) {
			@Override
			public String getValue(StaffingDetails object) {
				if (object.getChargableOTRates() != 0) {
					return object.getChargableOTRates() + "";
				} else {
					return 0 + "";
				}
			}
		};
		table.addColumn(chargableOTRates, "#Chargeable OT Rates");
		table.setColumnWidth(chargableOTRates, 120, Unit.PX);
	}

	private void addPayableOTRates() {
		// TODO Auto-generated method stub
		EditTextCell editCell = new EditTextCell();
		payableOTRates = new Column<StaffingDetails, String>(editCell) {
			@Override
			public String getValue(StaffingDetails object) {
				if (object.getPayableOTRates() != 0) {
					return object.getPayableOTRates() + "";
				} else {
					return 0 + "";
				}
			}
		};
		table.addColumn(payableOTRates, "#Payable OT Rates");
		table.setColumnWidth(payableOTRates, 120, Unit.PX);
		payableOTRates.setFieldUpdater(new FieldUpdater<StaffingDetails, String>() {

			@Override
			public void update(int index, StaffingDetails object,
					String value) {
				// TODO Auto-generated method stub
				if (!value.trim().equals("")) {
					try {
						object.setPayableOTRates((Double.parseDouble(value.trim())));
					} catch (Exception e) {
						object.setPayableOTRates(0);
					}
				} else {
					object.setPayableOTRates(0);
				}
				editPayableOtCost=true;
				updateRow(object, index,false);
//				table.redrawRow(index);
			}
		});

	}

	private void addNetSalary() {
		// TODO Auto-generated method stub
		netSalary = new TextColumn<StaffingDetails>() {

			@Override
			public String getValue(StaffingDetails object) {
				// TODO Auto-generated method stub
				return object.getNetSalary()+"";
			}
		};

		table.addColumn(netSalary, "Net Salary");
		table.setColumnWidth(netSalary, 120, Unit.PX);
	}

	private void addOthers() {
		// TODO Auto-generated method stub
		EditTextCell editCell = new EditTextCell();
		others = new Column<StaffingDetails, String>(editCell) {
			@Override
			public String getValue(StaffingDetails object) {
				if (object.getOthers() != null) {
					return object.getOthers();
				} else {
					return 0 + "";
				}
			}
		};
		table.addColumn(others, "#Others");
		table.setColumnWidth(others, 120, Unit.PX);
		others.setFieldUpdater(new FieldUpdater<StaffingDetails, String>() {

			@Override
			public void update(int index, StaffingDetails object, String value) {
				// TODO Auto-generated method stub
				object.setOthers(value);
//				table.redraw();
			}
		});
	}

	private void addHRA() {
		// TODO Auto-generated method stub
		EditTextCell editCell = new EditTextCell();
		hra = new Column<StaffingDetails, String>(editCell) {
			@Override
			public String getValue(StaffingDetails object) {
				if (object.getHra() != null) {
					return object.getHra() + "";
				} else {
					return 0+"";
				}
			}
		};
		table.addColumn(hra, "#HRA");
		table.setColumnWidth(hra, 120, Unit.PX);
		hra.setFieldUpdater(new FieldUpdater<StaffingDetails, String>() {

			@Override
			public void update(int index, StaffingDetails object, String value) {
				// TODO Auto-generated method stub
				if (!value.trim().equals("")) {
					try {
						object.setHra(value.trim());
					} catch (Exception e) {
						object.setHra(0 + "");
					}
				} else {
					object.setHra(0 + "");
				}
				editedHRA=true;
				updateRow(object, index,true);
//				table.redrawRow(index);
			}
		});
	}

	private void addDA() {
		// TODO Auto-generated method stub
		EditTextCell editCell = new EditTextCell();
		da = new Column<StaffingDetails, String>(editCell) {
			@Override
			public String getValue(StaffingDetails object) {
				if (object.getDa() != null) {
					return object.getDa();
				} else {
					return 0 + "";
				}
			}
		};
		table.addColumn(da, "#DA");
		table.setColumnWidth(da, 120, Unit.PX);

		da.setFieldUpdater(new FieldUpdater<StaffingDetails, String>() {

			@Override
			public void update(int index, StaffingDetails object, String value) {
				// TODO Auto-generated method stub
				if (!value.trim().equals("")) {
					try {
						object.setDa(value + "");
					} catch (Exception e) {
						object.setDa(0 + "");
					}
				} else {
					object.setDa(0 + "");
				}
				editedDA=true;
				updateRow(object, index,true);
//				table.redrawRow(index);
			}
		});
		
	}

	private void addBasic() {
		// TODO Auto-generated method stub
//		EditTextCell editCell = new EditTextCell();
//		basic = new Column<StaffingDetails, String>(editCell) {
//			@Override
//			public String getValue(StaffingDetails object) {
//				if (object.getBasic() != 0) {
//					return object.getBasic() + "";
//				} else {
//					return 0 + "";
//				}
//			}
//		};
//		table.addColumn(basic, "#Basic");
//		table.setColumnWidth(basic, 120, Unit.PX);
//
//		basic.setFieldUpdater(new FieldUpdater<StaffingDetails, String>() {
//
//			@Override
//			public void update(int index, StaffingDetails object, String value) {
//				// TODO Auto-generated method stub
//				if (!value.trim().equals("")) {
//					try {
//						object.setBasic(Double.parseDouble(value.trim()));
//					} catch (Exception e) {
//						object.setBasic(0);
//					}
//				} else {
//					object.setBasic(0);
//				}
//				editedBasic=true;
//				updateRow(object, index);
////				table.redrawRow(index);
//			}
//		});

		// TODO Auto-generated method stub
		viewbasic = new TextColumn<StaffingDetails>() {

			@Override
			public String getValue(StaffingDetails object) {
				// TODO Auto-generated method stub
				return object.getBasic() + "";
			}
		};

		table.addColumn(viewbasic, "Basic");
		table.setColumnWidth(viewbasic, 120, Unit.PX);
	
	}

	private void setFieldUpdaterOnDelete() {
		// TODO Auto-generated method stub
		deleteButton
				.setFieldUpdater(new FieldUpdater<StaffingDetails, String>() {

					@Override
					public void update(int index, StaffingDetails object,
							String value) {
						getDataprovider().getList().remove(index);
						RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
						table.redrawRow(index);
					}
				});
	}

	private void addDeleteButton() {
		// TODO Auto-generated method stub
		ButtonCell btnCell = new ButtonCell();
		deleteButton = new Column<StaffingDetails, String>(btnCell) {

			@Override
			public String getValue(StaffingDetails object) {

				return " - ";
			}
		};
		table.addColumn(deleteButton, "Delete");
		table.setColumnWidth(deleteButton, 50, Unit.PX);
	}

	private void addGrossSalary() {
		// TODO Auto-generated method stub
//		EditTextCell editCell = new EditTextCell();
//		grossSalary = new Column<StaffingDetails, String>(editCell) {
//			@Override
//			public String getValue(StaffingDetails object) {
//				if (object.getGrossSalary() != 0) {
//					return object.getGrossSalary() + "";
//				} else {
//					return 0 + "";
//				}
//
//			}
//		};
//		table.addColumn(grossSalary, "#Gross Salary");
//		table.setColumnWidth(grossSalary, 120, Unit.PX);
//
//		grossSalary
//				.setFieldUpdater(new FieldUpdater<StaffingDetails, String>() {
//
//					@Override
//					public void update(int index, StaffingDetails object,
//							String value) {
//						// TODO Auto-generated method stub
//						if (value != null) {
//							object.setGrossSalary(Double.parseDouble(value.trim()));
////							editedBasic=false;
////							editedDA=false;
////							editedHRA=false;
////							editedMedical=false;
////							editedWashingAllowance=false;
////							editedConveyance=false;
////							editedBandP=false;
////							editedLWF=false;
////							editedMWF=false;
////							editedPaidLeave=false;
////							editedOverheadCost=false;
////							editedSPPF=false;
////							editedcLeaves=false;
////							editedcBonus=false;
//							updateRow(object,index);
//						}
//					}
//				});

		// TODO Auto-generated method stub
		viewgrossSalary = new TextColumn<StaffingDetails>() {

			@Override
			public String getValue(StaffingDetails object) {
				// TODO Auto-generated method stub
				return object.getGrossSalary() + "";
			}
		};

		table.addColumn(viewgrossSalary, "Gross Salary");
		table.setColumnWidth(viewgrossSalary, 120, Unit.PX);
	

	}

	/**
	 * @author Anil
	 * @since 19-09-2020
	 * @param updateFlag
	 * added new update flag whoes value is true if you edit any earning component which will result in recalculating 
	 * paid leave and bonus component
	 * if you update any deduction or employer contribution component then its value will be false
	 * NOTE : This flag should true only if updating any earning component for else its value should be false
	 */
	protected void updateRow(StaffingDetails object,int index,boolean updateFlag) {
		/**Amol**/
		try{
			object.setLeaveSalary(object.getLeaveSalary());
		}catch(Exception e){
			object.setLeaveSalary(0);
		}

//		if(ctcTemplate!=null&&ctcTemplate.getPaidLeaveAmt()!=0){
//			object.setLeaveSalary(ctcTemplate.getPaidLeaveAmt());
//		}
		
		
		
		try {
			object.setGratuity(object.getGratuity());
		} catch (Exception e) {
			object.setGratuity(0+"");
		}
		try{
			object.setAdministrativecost(object.getAdministrativecost());
		}catch(Exception e){
			object.setAdministrativecost(0);
		}
		
		try{
			object.setUniformCost(object.getUniformCost());
		}catch(Exception e){
			object.setUniformCost(0);
		}
		
		/**
		 * 
		 */
		double esicCost=0;
		if(object.getEsicPercent()!=0){
			esicCost=((object.getPayableOTRates()*object.getEsicPercent())/(100));
		}
//		double esicCost=((object.getPayableOTRates()*4.75)/(100));
		try{
			object.setEsicCost(Math.round(esicCost * 100.0)/100.0);
		}catch(Exception e){
			object.setEsicCost(0);
		}
		
		double chargableOtRate=(object.getPayableOTRates()+object.getEsicCost());
		try{
			object.setChargableOTRates(chargableOtRate);
		}catch(Exception e){
			object.setChargableOTRates(0);
		}
		
		
		
		try {
			object.setGrossSalary(object.getGrossSalary());
		} catch (Exception e) {
			object.setGrossSalary(0);
		}
		if(!editedNoOfStaff)
			object.setNoOfStaff(object.getNoOfStaff());
		
		// object.setGrossSalary(grossSalaryAmt);
		Console.log("object.getGrossSalary()/2"
				+ object.getGrossSalary() / 2);
		object.setBasic(object.getBasic());
		
		Console.log("object.getDa()" + object.getDa());
		object.setDa(object.getDa());
		
		Console.log("object.getHra()" + object.getHra());
		object.setHra(object.getHra());
		
		Console.log("object.getMedical()"
				+ object.getMedical());
		object.setMedical(object.getMedical());
		
//		if(!editedWashingAllowance){
			try {
				object.setWashingAllowance(object
						.getWashingAllowance());
			} catch (Exception e) {
				e.printStackTrace();
				object.setWashingAllowance(0);
			}
//		}
		
		Console.log("object.getWashingAllowance()"
				+ object.getWashingAllowance());
		Console.log("object.getConveyance()"
				+ object.getConveyance());
//		if(!editedConveyance)
		object.setConveyance(object.getConveyance());
		Console.log("object.getOthers()"
				+ object.getOthers());
		object.setOthers(object.getOthers());
//		double bandpValue=object.getGrossSalary()-(Double.parseDouble(object.getDa())+Double.parseDouble(object.getHra())+Double.parseDouble(object.getMedical())+object.getWashingAllowance()+Double.parseDouble(object.getConveyance()));
//		Console.log("object.getBandP()" + bandpValue);
		if(!editedBandP)
		object.setBandP(object.getBandP());

		// Statutory Deductions
		double pfValue = ((object.getBasic() + Double
				.parseDouble(object.getDa())) / 100) * 12;
		Console.log("pfValue" + pfValue);
		object.setSdPF(object.getSdPF() + "");
//		if (object.getGrossSalary() < 21001) {
//			double esicCalValue = object.getGrossSalary()
//					- object.getWashingAllowance();
//			double esicValue = (esicCalValue / 100) * 1.75;
//			Console.log("esicValue" + esicValue);
//			object.setSdESIC(Math.round(esicValue) + "");
//		} else {
			object.setSdESIC(object.getSdESIC() + "");
//		}
		
//		if (object.getGrossSalary() >= 10000) {
//			object.setSdPT(200 + "");
//		} else {
//			object.setSdPT(175 + "");
//		}
		object.setSdPT(object.getSdPT());
		
		Console.log("pt" + object.getSdPT());
		Console.log("object.getSdLWF()" + object.getSdLWF());
		if(!editedLWF)
		object.setSdLWF(object.getSdLWF());
		
		try{
			object.setPlEsic(object.getPlEsic());
		}catch(Exception e){
			object.setPlEsic(0);
		}
		try{
			object.setBonusEsic(object.getBonusEsic());
		}catch(Exception e){
			object.setBonusEsic(0);
		}
		
		
		/**
		 * Shifting this code below for statutory deduction calculation
		 */
		
//		double satutoryDeduction = Double
//				.parseDouble(object.getSdPF())
//				+ Double.parseDouble(object.getSdESIC())
//				+ Double.parseDouble(object.getSdLWF())
//				+ Double.parseDouble(object.getSdPT())+object.getBonusEsic()+object.getPlEsic();
//		Console.log("satutoryDeduction" + satutoryDeduction);
//		object.setSatutoryDeductions((Math.round(satutoryDeduction)) + "");
//		
//		double netSalaryValue = object.getGrossSalary()
//				- Double.parseDouble(object
//						.getSatutoryDeductions().trim());
//		Console.log("netSalaryValue" + netSalaryValue);
//		// Net Salary
//		object.setNetSalary(Math.round(netSalaryValue));
		
		/**
		 * Rahul Verma added paid days. And changed(21/12) to (paiddays/12) where paiddays is provided by User
		 */
		double paiddaysValue=Double.parseDouble(object.getPaidLeaveDays().trim());
		
		// C Leave
		double cLeaveValue = ((object.getBasic() + Double
				.parseDouble(object.getDa().trim()))*((paiddaysValue/12)/30));
		Console.log("cLeaveValue" + cLeaveValue);
		if(!editedcLeaves)
			
		if(updateFlag){	
			object.setcLeave((Math.round(cLeaveValue*100.0)/100.0) + "");
			
			/**
			 * @author Anil
			 * @since 19-09-2020
			 */
			if(object.iscLeaveApplicable()&&object.getcLeave()!=null){
				double plEsic=((Double.parseDouble(object.getcLeave())*0.75)/100);
				Console.log("#PL ESIC Value "+plEsic);
				object.setPlEsic(plEsic);
			}else{
				object.setPlEsic(0);
			}
		}
		/**
		 * @author Anil
		 * @since 20-08-2020
		 */
		CTCTemplate ctcTemplate=ctcTemplateMap.get(object.getCtcTemplates());
		if(paidleave!=null){
			double plAmt=0;
			if(paidleave!=null){
				for(OtEarningComponent plComp:paidleave.getOtEarningCompList()){
					System.out.println("NAME : "+plComp.getComponentName());
					for(CtcComponent earningComp:ctcTemplate.getEarning()){
						System.out.println("EARN NAME : "+earningComp.getName());
						if(plComp.getComponentName().equalsIgnoreCase("Gross Earning")){
							plAmt=plAmt+earningComp.getAmount()/12;
						}else if(plComp.getComponentName().equals(earningComp.getName())){
							System.out.println("AMT : "+earningComp.getAmount()/12);
							plAmt=plAmt+earningComp.getAmount()/12;
						}
						
					}
				}
			}
			System.out.println("BASE AMT : "+plAmt);
			plAmt=plAmt*paidleave.getRate()/100;
			System.out.println("PL AMT : "+plAmt);
			
			if(updateFlag){
				object.setcLeave((Math.round(plAmt*100.0)/100.0)+"");
				
				/**
				 * @author Anil
				 * @since 19-09-2020
				 */
				if(object.iscLeaveApplicable()&&object.getcLeave()!=null){
					double plEsic=((Double.parseDouble(object.getcLeave())*0.75)/100);
					Console.log("#PL ESIC Value "+plEsic);
					object.setPlEsic(plEsic);
				}else{
					object.setPlEsic(0);
				}
			}
		}
		
		
//		CNCForm form = new CNCForm();
		double cBonusPercent = 0;
		cBonusPercent = object.getcBonusPercentage();//Double.parseDouble(form.getTbcBonus().getValue().trim());
		Console.log("cBonusPercent" + cBonusPercent);
		double cBonusValue = ((object.getBasic() + Double
				.parseDouble(object.getDa().trim())))
				* (cBonusPercent / 100);
		Console.log("cBonusValue" + cBonusValue);
		
		if(!editedcBonus){
			if(updateFlag){
				object.setcBonus((Math.round(cBonusValue*100.0)/100.0) + "");
				/**
				 * @author Anil
				 * @since 19-09-2020
				 */
				if(object.iscBonusApplicable()&&object.getcBonus()!=null){
					double bonusEsic=((Double.parseDouble(object.getcBonus())*0.75)/100);
					object.setBonusEsic(bonusEsic);
				}else{
					object.setBonusEsic(0);
				}
			}
		}
		
		Console.log("pl esic111 "+object.getPlEsic());
		Console.log("bonus esic111 "+object.getBonusEsic());
		double satutoryDeduction = Double
				.parseDouble(object.getSdPF())
				+ Double.parseDouble(object.getSdESIC())
				+ Double.parseDouble(object.getSdLWF())
				+ Double.parseDouble(object.getSdPT())+object.getBonusEsic()+object.getPlEsic();
		Console.log("satutoryDeduction" + satutoryDeduction);
		object.setSatutoryDeductions((Math.round(satutoryDeduction)) + "");
		
		double netSalaryValue = object.getGrossSalary()
				- Double.parseDouble(object
						.getSatutoryDeductions().trim());
		Console.log("netSalaryValue" + netSalaryValue);
		// Net Salary
		object.setNetSalary(Math.round(netSalaryValue));
		
		
		
		
		object.setTravellingAllowance(Math.round(object.getTravellingAllowance()*100.0)/100.0);
		double takeHomeValue = object.getNetSalary()
				+ Double.parseDouble(object.getcLeave())
				+ Double.parseDouble(object.getcBonus())
				+ object.getTravellingAllowance();
		Console.log("takeHomeValue" + takeHomeValue);
		object.setTakeHome((Math.round(takeHomeValue)) + "");

		// Statutory Payments
		double cpfValue = ((object.getBasic() + Double.parseDouble(object.getDa())) / 100) * 12;
		Console.log("cpfValue" + cpfValue);
		if(!editedSPPF)
		object.setSpPF(object.getSpPF()+ "");
		
//		if (object.getGrossSalary() < 21001) {
//			double esicCalValue = object.getGrossSalary()
//					- object.getWashingAllowance();
//			double esicValue = (esicCalValue / 100) * 4.75;
//			Console.log("c esicValue" + esicValue);
//			object.setSpESIC(Math.round(esicValue) + "");
//		} else {
			object.setSpESIC(object.getSpESIC() + "");
//		}
		
//		double edilCalValue = object.getBasic()
//				+ Double.parseDouble(object.getDa().trim());
//		double edilValue = edilCalValue * (1.16 / 100);
//		Console.log("edilValue" + edilValue);
		object.setSpEDLI(object.getSpEDLI()+ "");
		if(!editedMWF)
		object.setSpMWF(object.getSpMWF()+"");
			
		double bonusValue = 0;
		/**
		 * Rahul Verma added paid days. And changed(21/12) to (paiddays/12) where paiddays is provided by User
		 */
		double paiddaysforBonus=Double.parseDouble(object.getPaidLeaveDays().trim());
		if (object.isBonusRelever()) {
			bonusValue = ((object.getBasic() + Double
					.parseDouble(object.getDa())) * (cBonusPercent / 100))
					+ (((object.getBasic() + Double
							.parseDouble(object.getDa())) * (cBonusPercent / 100)) * ((paiddaysforBonus/12)/ 30));
		} else {
			bonusValue = ((object.getBasic() + Double
					.parseDouble(object.getDa())) * (cBonusPercent / 100));// +(1.75/30);
		}
		Console.log("bonusValue" + bonusValue);
		
		if(updateFlag){
			object.setSpBonus((Math.round(bonusValue*100.0)/100.0) + "");
			
			/**
			 * @author Anil
			 * @since 19-09-2020
			 * Calculating CBonus based on Bonus (% and reliever)
			 */
			double cbonusEsic=0;
			if(object.getSpBonus()!=null){
				cbonusEsic=((Double.parseDouble(object.getSpBonus())*3.25)/100);	
				object.setcBonusEsic(cbonusEsic);
			}else{
				object.setcBonusEsic(0);
				cbonusEsic=0;	
			}
		}
		
		/** date 17.11.2018 added by komal for paid leave calculation as discussed with maradona**/
		double paidLeavesValue = 0;
//		Console.log("form.cb_Relever.getValue()"
//				+ object.isRelever() );
		/**
		 * Rahul Verma added paid days. And changed(21/12) to (paiddays/12) where paiddays is provided by User
		 */
		double paiddays=Double.parseDouble(object.getPaidLeaveDays().trim());
		if(!editedPaidLeave){
		double totalValue = Double.parseDouble(object.getSpEDLI())
				+ Double.parseDouble(object.getSpESIC())
				+ Double.parseDouble(object.getSpMWF())
				+ Double.parseDouble(object.getSpPF());
		totalValue = Math.round(totalValue*100.0)/100.0;
		if (object.isPaidLeaveRelever()) {
			paidLeavesValue = ((object.getGrossSalary() + (totalValue)) * ((paiddays/12)/30))
													+
					(((object.getGrossSalary() + (totalValue)))*((paiddays/12)/30))*( (paiddays/12)/ 30);
		} else {
			paidLeavesValue = (object.getGrossSalary() + (totalValue)) * ((paiddays/12)/30);
		}
		Console.log("paidLeavesValue" + paidLeavesValue);

		if(updateFlag){
			object.setPaidLeaves((Math.round(paidLeavesValue*100.0)/100.0) + "");
			
			/**
			 * @author Anil
			 * @since 19-09-2020
			 */
			double cplEsic=0;
			if(object.getPaidLeaves()!=null){
				cplEsic=((Double.parseDouble(object.getPaidLeaves())*3.25)/100);
				Console.log("#CPL ESIC Value "+plEsic);
				object.setcPlEsic(cplEsic);
			}else{
				object.setcPlEsic(0);
				cplEsic=0;
			}
		}
		
		if(paidleave!=null){
			double plAmt=0;
			if(paidleave!=null){
				for(OtEarningComponent plComp:paidleave.getOtEarningCompList()){
					System.out.println("NAME : "+plComp.getComponentName());
					for(CtcComponent earningComp:ctcTemplate.getEarning()){
						System.out.println("EARN NAME : "+earningComp.getName());
						if(plComp.getComponentName().equalsIgnoreCase("Gross Earning")){
							plAmt=plAmt+earningComp.getAmount()/12;
						}else if(plComp.getComponentName().equals(earningComp.getName())){
							System.out.println("AMT : "+earningComp.getAmount()/12);
							plAmt=plAmt+earningComp.getAmount()/12;
						}
						
					}
				}
			}
			System.out.println("BASE AMT : "+plAmt);
			plAmt=plAmt*paidleave.getRate()/100;
			System.out.println("PL AMT : "+plAmt);
			
			if(updateFlag){
				object.setPaidLeaves((Math.round(plAmt*100.0)/100.0)+"");
				
				/**
				 * @author Anil
				 * @since 19-09-2020
				 */
				double cplEsic=0;
				if(object.getPaidLeaves()!=null){
					cplEsic=((Double.parseDouble(object.getPaidLeaves())*3.25)/100);
					Console.log("#CPL ESIC Value "+plEsic);
					object.setcPlEsic(cplEsic);
				}else{
					object.setcPlEsic(0);
					cplEsic=0;
				}
			}
		}
		
		}
		/**
		 *  end komal
		 */
		/**Date 24-6-2019 
		 * @author Amol
		 * adding gratuity formula
		 */
//		
//		try{
//			object.setGratuity(object.getGratuity());
//		}catch(Exception e){
//			object.setGratuity(null);
//		}
//		
		
		
		double gratuity=0;
		
//		if(object.isGratuityApplicable()){
//			gratuity=((((object.getBasic())+Double.parseDouble(object.getDa()))*4.81)/(100));
//		}else{
//			gratuity=0;
//		}
		object.setGratuity(object.getGratuity());
		object.setSupervision(object.getSupervision());
		object.setTrainingCharges(object.getTrainingCharges());
		
		object.setcBonusEsic(object.getcBonusEsic());
		object.setcPlEsic(object.getcPlEsic());
		
		double statutoryPayments = Double
				.parseDouble(object.getSpBonus())
				+ Double.parseDouble(object.getSpEDLI())
				+ Double.parseDouble(object.getSpESIC())
				+ Double.parseDouble(object.getSpMWF())
				+ Double.parseDouble(object.getSpPF())
				+ Double.parseDouble(object.getPaidLeaves())
				+Double.parseDouble(object.getGratuity())
				+(object.getAdministrativecost())
//				+(object.getUniformCost())
				+(object.getSupervision())
		        +(object.getTrainingCharges())
		        +object.getcPlEsic()+object.getcBonusEsic();
		
		
		
				//+ Double.parseDouble(object.getSpBonus());
		
		Console.log("statutoryPayments" + statutoryPayments);
		object.setSatutoryPayments((Math.round(statutoryPayments)) + "");
		Console.log("object.getGrossSalary()+Double.parseDouble(object.getSatutoryPayments())"
				+ object.getGrossSalary()
				+ Double.parseDouble(object
						.getSatutoryPayments()));
		object.setTotal((Math.round((object.getGrossSalary()
				+ Double.parseDouble(object
						.getSatutoryPayments())))) + "");

//		double paidLeavesValue = 0;
//		Console.log("form.cb_Relever.getValue()"
//				+ object.isRelever() );
		/**
		 * Rahul Verma added paid days. And changed(21/12) to (paiddays/12) where paiddays is provided by User
		 */
// paiddays=Double.parseDouble(object.getPaidLeaveDays().trim());
		
//		if(!editedPaidLeave){
//			if (object.isPaidLeaveRelever()) {
//				paidLeavesValue = ((object.getGrossSalary() + ((Double.parseDouble(object.getSatutoryPayments()))-Double.parseDouble(object.getSpBonus()))) * ((paiddays/12)/30))
//														+
//						(((object.getGrossSalary() + ((Double.parseDouble(object.getSatutoryPayments()))-Double.parseDouble(object.getSpBonus()))))*((paiddays/12)/30))*((paiddays/12)/ 30);
//			} else {
//				paidLeavesValue = (object.getGrossSalary() + ((Double.parseDouble(object.getSatutoryPayments()))-Double.parseDouble(object.getSpBonus()))) * ((paiddays/12)/30);
//			}
//		Console.log("paidLeavesValue" + paidLeavesValue);
//
//		object.setPaidLeaves((Math.round(paidLeavesValue*100.0)/100.0) + "");
//		}

		object.setEmployementOverHeadCost(object.getOverHeadCost());
		object.setTravellingAllowanceOverhead(object.getTravellingAllowanceOverhead());
		object.setUniformCost(object.getUniformCost());
		double totalPaidAndOverheadValue = Double
				.parseDouble(object.getTotal())
				+ object.getAdditionalAllowance()
				+ Double.parseDouble(object
						.getEmployementOverHeadCost())
				+ object.getTravellingAllowanceOverhead()+object.getUniformCost();
		Console.log("totalPaidAndOverheadValue"
				+ totalPaidAndOverheadValue);
		object.setTotalOfPaidAndOverhead((Math.round(totalPaidAndOverheadValue))
				+ "");
		object.setTotalPaidIntoEmployee(Math.round((totalPaidAndOverheadValue*object.getNoOfStaff())));
		
		double feePercent = 0;
		double managementFeesValue = 0;
		try {
//			if (object.getManageMentfeesPercent()!=0) {
//				feePercent = object.getManageMentfeesPercent();
//			} 
			if (object.getManageMentfeesPercent()!=0) {
				feePercent = object.getManageMentfeesPercent();
				//komal
				managementFeesValue = object.getTotalPaidIntoEmployee()*( feePercent / 100);
				Console.log("managementFeesValue"
						+ managementFeesValue);
				object.setManagementFees((Math.round(managementFeesValue*100.0)/100.0)+ "");
			} else{
				if(object.getManagementFees()!=null && !object.getManagementFees().equals("")){
					managementFeesValue = Double.parseDouble(object.getManagementFees());
				}
				/**
				 * @author Anil
				 * @since 03-09-2020
				 */
				if(managementFeesAsPerStaff&&object.getNoOfStaff()!=0&&object.getManagementFeesRate()!=0){
					managementFeesValue=object.getNoOfStaff()*object.getManagementFeesRate();
				}
			}
			Console.log("feePercent" + feePercent);
		} catch (Exception e) {
			e.printStackTrace();
		}

		//double managementFeesValue = object.getTotalPaidIntoEmployee()*( feePercent / 100);
		
		Console.log("managementFeesValue"
				+ managementFeesValue);
		if(!editedManaagementFees)
		object.setManagementFees(managementFeesValue + "");

		double managementTotal = managementFeesValue+object.getTotalPaidIntoEmployee();
		Console.log("managementTotal" + managementTotal);
		object.setTotalIncludingManagementFees((Math.round(managementTotal))
				+ "");
		
		double grandTotalValue = managementTotal;
		object.setGrandTotalOfCTC((Math.round(grandTotalValue*100.0)/100.0) + "");
		Console.log("grandTotalValue" + grandTotalValue);
		
		editedBasic=false;
		editedDA=false;
		editedHRA=false;
		editedMedical=false;
		editedWashingAllowance=false;
		editedConveyance=false;
		editedBandP=false;
		editedLWF=false;
		editedMWF=false;
		editedPaidLeave=false;
		editedOverheadCost=false;
		editedSPPF=false;

		RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
		table.redrawRow(index);
	}
	
	/**
	 * @author Anil @since 05-04-2021
	 * Adding working hours column for OT Bill Calculation
	 * Raised by Rahul Tiwari for Alkosh
	 */
	private void workingHourCol() {
		
		// TODO Auto-generated method stub
		EditTextCell editCell = new EditTextCell();
		workingHourCol = new Column<StaffingDetails, String>(editCell) {
			@Override
			public String getValue(StaffingDetails object) {
				if (object.getWorkingHours() == 0) {
					return 0 + "";
				} else {
					return object.getWorkingHours() + "";
				}
			}
		};
		table.addColumn(workingHourCol, "#Working Hours");
		table.setColumnWidth(workingHourCol, 90, Unit.PX);

		workingHourCol.setFieldUpdater(new FieldUpdater<StaffingDetails, String>() {

			@Override
			public void update(int index, StaffingDetails object, String value) {
				// TODO Auto-generated method stub
				if (!value.trim().equals("")) {
					try {
						object.setWorkingHours(Integer.parseInt(value.trim()));
					} catch (Exception e) {
						object.setWorkingHours(0);
					}
				} else {
					object.setWorkingHours(0);
				}
			}
		});
	}
	
	private void viewWorkingHourCol() {
		viewWorkingHoursCol= new TextColumn<StaffingDetails>() {
			@Override
			public String getValue(StaffingDetails object) {
				if (object.getWorkingHours() == 0) {
					return 0 + "";
				} else {
					return object.getWorkingHours() + "";
				}
			}
		};
		table.addColumn(viewWorkingHoursCol, "Working Hours");
		table.setColumnWidth(viewWorkingHoursCol, 90, Unit.PX);
	}

	private void addNoOfStaff() {
		
		// TODO Auto-generated method stub
		EditTextCell editCell = new EditTextCell();
		noOfStaff = new Column<StaffingDetails, String>(editCell) {
			@Override
			public String getValue(StaffingDetails object) {
				if (object.getNoOfStaff() == 0) {
					return 0 + "";
				} else {
					return object.getNoOfStaff() + "";
				}
			}
		};
		table.addColumn(noOfStaff, "#No Of Staff");
		table.setColumnWidth(noOfStaff, 120, Unit.PX);

		noOfStaff.setFieldUpdater(new FieldUpdater<StaffingDetails, String>() {

			@Override
			public void update(int index, StaffingDetails object, String value) {
				// TODO Auto-generated method stub
				if (!value.trim().equals("")) {
					try {
						object.setNoOfStaff(Integer.parseInt(value.trim()));
					} catch (Exception e) {
						object.setNoOfStaff(0);
					}
				} else {
					object.setNoOfStaff(0);
				}
				object.setTotalPaidIntoEmployee(Math.round((Double.parseDouble(object.getTotalOfPaidAndOverhead())*object.getNoOfStaff())));
				double feePercent = 0;
				double managementFeesValue = 0;
				try {
//					if (object.getManageMentfeesPercent()!=0) {
//						feePercent = object.getManageMentfeesPercent();
//					} 
					if (object.getManageMentfeesPercent()!=0) {
						feePercent = object.getManageMentfeesPercent();
						//komal
						managementFeesValue = object.getTotalPaidIntoEmployee()*( feePercent / 100);
						Console.log("managementFeesValue"
								+ managementFeesValue);
						object.setManagementFees((Math.round(managementFeesValue*100.0)/100.0) + "");
					} else{
						if(object.getManagementFees()!=null && !object.getManagementFees().equals("")){
							managementFeesValue = Double.parseDouble(object.getManagementFees());
						}
						/**
						 * @author Anil
						 * @since 03-09-2020
						 */
						if(managementFeesAsPerStaff&&object.getNoOfStaff()!=0&&object.getManagementFeesRate()!=0){
							managementFeesValue=object.getNoOfStaff()*object.getManagementFeesRate();
						}
					}
					Console.log("feePercent" + feePercent);
				} catch (Exception e) {
					e.printStackTrace();
				}

//				double managementFeesValue = object.getTotalPaidIntoEmployee()*( feePercent / 100);
//				Console.log("managementFeesValue"
//						+ managementFeesValue);
				object.setManagementFees(managementFeesValue + "");

				double managementTotal = managementFeesValue+object.getTotalPaidIntoEmployee();
				Console.log("managementTotal" + managementTotal);
				object.setTotalIncludingManagementFees((Math.round(managementTotal))
						+ "");
				
				double grandTotalValue = managementTotal;
				object.setGrandTotalOfCTC((Math.round(grandTotalValue*100.0)/100.0) + "");
				Console.log("grandTotalValue" + grandTotalValue);
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				table.redrawRow(index);
			}
		});
	}

	private void addDesignation() {
		// TODO Auto-generated method stub
		SelectionCell designationSelection = new SelectionCell(
				designationNameList);
		designation = new Column<StaffingDetails, String>(designationSelection) {
			@Override
			public String getValue(StaffingDetails object) {
				// TODO Auto-generated method stub
				if (object.getDesignation() != null)
					return object.getDesignation();
				else
					return "";

			}
		};
		table.addColumn(designation, "#Designation");
		table.setColumnWidth(designation, 100, Unit.PX);

		designation
				.setFieldUpdater(new FieldUpdater<StaffingDetails, String>() {

					@Override
					public void update(int index, StaffingDetails object,
							String value) {

						if (value.equalsIgnoreCase("--SELECT--")) {
							object.setDesignation("");
						} else {
							object.setDesignation(value);
						}
						// getDataprovider().getList().remove(index);
						RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
						table.redrawRow(index);
					}
				});
	}

	private void makeListOfDesignation() {

		ArrayList<Config> designationList = new ArrayList<Config>();
		designationList = LoginPresenter.globalConfig;
		designationNameList = new ArrayList<String>();
		designationNameList.add("--SELECT--");

		if (designationList.size() != 0) {
			for (int i = 0; i < designationList.size(); i++) {
				if (designationList.get(i).getType() == 13
						&& designationList.get(i).isStatus() == true) {
					if (designationList.get(i).getName() != null) {
						try {
							designationNameList.add(designationList.get(i)
									.getName());
						} catch (Exception e) {
							System.out.println(e);
						}
					}
				}
			}
		}

	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub

	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub

	}

	@Override
	public void setEnable(boolean state) {
		int tablecolcount = this.table.getColumnCount();
		for (int i = tablecolcount - 1; i > -1; i--)
			table.removeColumn(i);
		if (state == true)
			addeditColumn();
		if (state == false)
			addViewColumn();
	}

	private void addViewColumn() {
		// TODO Auto-generated method stub
		System.out.println("View Table");
		addViewDesignation();
		addViewCTCTemplates();
		addViewNoOfStaff();
		/**
		 * @author Anil @since 05-04-2021
		 * Adding working hours col for Alkosh raised by Rahul Tiwari
		 */
		viewWorkingHourCol();
		
		addViewGrossSalary();
		addViewBasic();
		addViewDA();
		addViewHRA();
		addViewMedical();
		/**20-1-2019 added by amol for City Compensatory Allowance (CCA)*/
		if(hideColumn){
		addViewCityCompensatoryAllowance();
		}
		addViewWashingAllowance();
		addViewConyevance();
		addViewBandP();
		/**date 11-5-2019 added by Amol for Gratuity Column**/
//		if(hideColumn){
//		addViewGratuity();
//		addViewAdministravtiveCost();
//		}
		addViewOthers();
		addViewSDPF();
		addViewSDESIC();
		addViewPlEsic();
		addViewBonusEsic();
		addViewSDPT();
		addViewSDLWF();
		addViewStatutoryDeductions();
		addViewNetSalary();
		addViewCLeaves();
		addViewCBonus();
		addViewTravellingAllowance();
		addViewTakeHome();
		addViewSPPF();
		addViewSPESIC();
		addViewCPlEsic();
		addViewCBonusEsic();
		addViewSPEDLI();
		addViewSPMWF();
		addViewBonus();
		addViewPaidLeaves();
		
		if(hideColumn){
			addViewAdministravtiveCost();
			addViewGratuity();
		}
		if(hideColumn){
			addViewSupervisionCost();
			addViewTrainingCost();
//			addViewEsicAmount();
		}
		
		addViewStatutoryPayments();
		addViewTotal();// A+B
		addViewEmployementOverHeadCost();
		addViewTravellingAllowanceAfterOverhead();
		viewAdditionalAllowance();
		
		addViewUniformCost();
		
		addViewTotalOfPaidAndOverhead();
		addViewTotalPaidIntoEmployees();
		addViewManagementFeesPercent();
		addViewManagementFees();
		addViewTotalIncludingManagementFees();
		
		addViewGrandTotalOfCTC();
		addViewPayableOTRates();
		
		if(hideColumn){
//			addViewSupervisionCost();
//			addViewTrainingCost();
			addViewEsicAmount();
		}
		
		addViewChargeableOTRates();
//		addViewUniformCost();
       
		
//		addViewDeleteButton();
		addDeleteButton();
		// setFieldUpdaterOnDelete();
	}
	private void addViewEsicAmount() {
		viewEsicCost= new TextColumn<StaffingDetails>(){

			@Override
			public String getValue(StaffingDetails object) {
				if (object.getEsicCost()!= 0) {
					return object.getEsicCost() + "";
				} else {
					return 0 + "";
				}
			}
		};

		table.addColumn(viewEsicCost, "ESIC OT Cost(Amt)");
		table.setColumnWidth(viewEsicCost, 120, Unit.PX);
	}

	private void addViewTrainingCost() {
		viewTrainingCost= new TextColumn<StaffingDetails>(){

			@Override
			public String getValue(StaffingDetails object) {
				if (object.getTrainingCharges()!= 0) {
					return object.getTrainingCharges() + "";
				} else {
					return 0 + "";
				}
			}
		};

		table.addColumn(viewTrainingCost, "Training Charges");
		table.setColumnWidth(viewTrainingCost, 120, Unit.PX);
	}

	private void addViewSupervisionCost() {
		viewSupervisionCost= new TextColumn<StaffingDetails>(){

			@Override
			public String getValue(StaffingDetails object) {
				if (object.getSupervision()!= 0) {
					return object.getSupervision()+ "";
				} else {
					return 0 + "";
				}
			}
		};

		table.addColumn(viewSupervisionCost, "Supervision");
		table.setColumnWidth(viewSupervisionCost, 120, Unit.PX);
	}

	/**Date 11-5-2019
	 * @author Amol
	 * Added a Administrative cost Column 
	 */
	protected void addAdministrativecost() {
		
		// TODO Auto-generated method stub
		EditTextCell editCell = new EditTextCell();
		administrativeCost = new Column<StaffingDetails, String>(editCell) {
			@Override
			public String getValue(StaffingDetails object) {
				if (object.getAdministrativecost() != 0) {
					return object.getAdministrativecost()+ "";
				} else {
					return 0 + "";
				}
			}
		};
		table.addColumn(administrativeCost, "#Administrative Cost");
		table.setColumnWidth(administrativeCost, 120, Unit.PX);
		administrativeCost.setFieldUpdater(new FieldUpdater<StaffingDetails, String>() {

					@Override
					public void update(int index, StaffingDetails object,
							String value) {
						// TODO Auto-generated method stub
						if (!value.trim().equals("")) {
							try {
								object.setAdministrativecost(Double.parseDouble(value.trim()));
							} catch (Exception e) {
								object.setAdministrativecost(0);
							}
						} else {
							object.setAdministrativecost(0);
						}
						editedAdminiStrativeCost=true;
						updateRow(object, index,false);
//						table.redrawRow(index);
					}
				});
		}

	/**Date 11-5-2019
	 * @author Amol
	 * Added a Gratuity Column 
	 */
	protected void addGratuity() {
		
		// TODO Auto-generated method stub
		EditTextCell editCell = new EditTextCell();
		Gratuity = new Column<StaffingDetails, String>(editCell) {
			@Override
			public String getValue(StaffingDetails object) {
				if (object.getGratuity() != null) {
					return object.getGratuity()+ "";
				} else {
					return 0.00 + "";
				}
			}
		};
		table.addColumn(Gratuity, " #Gratuity");
		table.setColumnWidth(Gratuity, 120, Unit.PX);
		Gratuity.setFieldUpdater(new FieldUpdater<StaffingDetails, String>() {

					@Override
					public void update(int index, StaffingDetails object,String value) {
						// TODO Auto-generated method stub
//						if (!value.trim().equals("")) {
//							try {
//								object.setGratuity((value.trim()));
//							} catch (Exception e) {
//								object.setGratuity(null);
//							}
//						} else {
						
							object.setGratuity(value);
							Console.log("GratuityValue in Edit Cell"+value);
//						}
						editedGratuity=true;
						updateRow(object, index,false);
						table.redrawRow(index);
					}
				});
		}
	
	protected void addEsicCost(){
		
		// TODO Auto-generated method stub
		EditTextCell editCell = new EditTextCell();
		EsicCost = new Column<StaffingDetails, String>(editCell) {
			@Override
			public String getValue(StaffingDetails object) {
				if (object.getEsicCost() != 0) {
					return object.getEsicCost()+ "";
				} else {
					return 0 + "";
				}
			}
		};
		table.addColumn(EsicCost, " #Esic OT Cost%");
		table.setColumnWidth(EsicCost, 120, Unit.PX);
		EsicCost.setFieldUpdater(new FieldUpdater<StaffingDetails, String>() {

					@Override
					public void update(int index, StaffingDetails object,String value) {
						// TODO Auto-generated method stub
//					
						
							object.setEsicCost(Double.parseDouble(value));
							Console.log("editEsicCost in Edit Cell"+value);
//						}
						editEsicCost=true;
						updateRow(object, index,false);
						table.redrawRow(index);
					}
				});
		}
	
	
	
	
	
	
	
	
	

	/***Date 21-2-2019 
	 *   added by amol for CityCompensatoryAllowance
	 * 
	 */
	private void addViewCityCompensatoryAllowance(){
		// TODO Auto-generated method stub
		viewcityCompensatoryAllowance = new TextColumn<StaffingDetails>() {

			@Override
			public String getValue(StaffingDetails object) {
				// TODO Auto-generated method stub
				return object.getCityCompensatoryAllowance() + "";
			}
		};

		table.addColumn(viewcityCompensatoryAllowance, "City Compensatory Allowance (CCA)");
		table.setColumnWidth(viewcityCompensatoryAllowance, 120, Unit.PX);
	}
	

	private void addViewLeaveSalary(){
		// TODO Auto-generated method stub
		viewLeaveSalary = new TextColumn<StaffingDetails>() {

			@Override
			public String getValue(StaffingDetails object) {
				// TODO Auto-generated method stub
				return object.getLeaveSalary() + "";
			}
		};

		table.addColumn(viewLeaveSalary, "Leave Salary");
		table.setColumnWidth(viewLeaveSalary, 120, Unit.PX);
	}
	
	
	
	
	
	
	
	
	
	private void addViewTravellingAllowanceAfterOverhead() {
		// TODO Auto-generated method stub
		viewTravellingAllowanceOverhead = new TextColumn<StaffingDetails>() {

			@Override
			public String getValue(StaffingDetails object) {
				// TODO Auto-generated method stub
				return object.getTravellingAllowanceOverhead() + "";
			}
		};

		table.addColumn(viewTravellingAllowanceOverhead, "Travelling Allowance");
		table.setColumnWidth(viewTravellingAllowanceOverhead, 120, Unit.PX);
	}

	private void addViewTravellingAllowance() {
		// TODO Auto-generated method stub
		viewTravellingAllowance = new TextColumn<StaffingDetails>() {

			@Override
			public String getValue(StaffingDetails object) {
				// TODO Auto-generated method stub
				return object.getTravellingAllowance() + "";
			}
		};

		table.addColumn(viewTravellingAllowance, "Travelling Allowance");
		table.setColumnWidth(viewTravellingAllowance, 120, Unit.PX);
	}

	private void addViewManagementFeesPercent() {
		// TODO Auto-generated method stub
		viewmanagementFeesPercent = new TextColumn<StaffingDetails>() {

			@Override
			public String getValue(StaffingDetails object) {
				// TODO Auto-generated method stub
				return object.getManageMentfeesPercent() + "";
			}
		};
		table.addColumn(viewmanagementFeesPercent, "Management %");
		table.setColumnWidth(viewmanagementFeesPercent, 120, Unit.PX);
	}

	private void addViewTotalPaidIntoEmployees() {
		// TODO Auto-generated method stub
		viewtotalPaidIntoEmployee = new TextColumn<StaffingDetails>() {

			@Override
			public String getValue(StaffingDetails object) {
				// TODO Auto-generated method stub
				return object.getTotalPaidIntoEmployee() + "";
			}
		};
		//table.addColumn(viewtotalPaidIntoEmployee, "Total(x Emp)");
		table.addColumn(viewtotalPaidIntoEmployee, "Total CTC");
		table.setColumnWidth(viewtotalPaidIntoEmployee, 120, Unit.PX);
	}

	private void addViewGrandTotalOfCTC() {
		// TODO Auto-generated method stub
		viewgrandTotalOfCTC = new TextColumn<StaffingDetails>() {

			@Override
			public String getValue(StaffingDetails object) {
				// TODO Auto-generated method stub
				return object.getGrandTotalOfCTC() + "";
			}
		};

		table.addColumn(viewgrandTotalOfCTC, "Grand Total");
		table.setColumnWidth(viewgrandTotalOfCTC, 120, Unit.PX);
	}

	private void addViewTotalIncludingManagementFees() {
		// TODO Auto-generated method stub
		viewtotalIncludingManagementFees = new TextColumn<StaffingDetails>() {

			@Override
			public String getValue(StaffingDetails object) {
				// TODO Auto-generated method stub
				return object.getTotalIncludingManagementFees() + "";
			}
		};

	//	table.addColumn(viewtotalIncludingManagementFees, "Total");
		table.addColumn(viewtotalIncludingManagementFees, "Grand total Including Management Fees");
		table.setColumnWidth(viewtotalIncludingManagementFees, 120, Unit.PX);
	}

	private void addViewManagementFees() {
		// TODO Auto-generated method stub
		viewmanagementFees = new TextColumn<StaffingDetails>() {

			@Override
			public String getValue(StaffingDetails object) {
				// TODO Auto-generated method stub
				return object.getManagementFees() + "";
			}
		};

		table.addColumn(viewmanagementFees, "Management Fees");
		table.setColumnWidth(viewmanagementFees, 120, Unit.PX);
	}

	private void addViewTotalOfPaidAndOverhead() {
		// TODO Auto-generated method stub
		viewtotalOfPaidAndOverhead = new TextColumn<StaffingDetails>() {

			@Override
			public String getValue(StaffingDetails object) {
				// TODO Auto-generated method stub
				return object.getTotalOfPaidAndOverhead() + "";
			}
		};

		table.addColumn(viewtotalOfPaidAndOverhead, "Total");
		table.setColumnWidth(viewtotalOfPaidAndOverhead, 120, Unit.PX);
	}

	private void addViewEmployementOverHeadCost() {
		// TODO Auto-generated method stub
		viewemployementOverHeadCost = new TextColumn<StaffingDetails>() {

			@Override
			public String getValue(StaffingDetails object) {
				// TODO Auto-generated method stub
				return object.getOverHeadCost() + "";
			}
		};

		table.addColumn(viewemployementOverHeadCost, "Overhead Cost");
		table.setColumnWidth(viewemployementOverHeadCost, 120, Unit.PX);
	}

	private void addViewPaidLeaves() {
		// TODO Auto-generated method stub
		viewpaidLeaves = new TextColumn<StaffingDetails>() {

			@Override
			public String getValue(StaffingDetails object) {
				// TODO Auto-generated method stub
				return object.getPaidLeaves() + "";
			}
		};

		table.addColumn(viewpaidLeaves, "Paid Leaves");
		table.setColumnWidth(viewpaidLeaves, 120, Unit.PX);
	}

	private void addViewTotal() {
		// TODO Auto-generated method stub
		viewtotal = new TextColumn<StaffingDetails>() {

			@Override
			public String getValue(StaffingDetails object) {
				// TODO Auto-generated method stub
				return object.getTotal() + "";
			}
		};

		table.addColumn(viewtotal, "Total");
		table.setColumnWidth(viewtotal, 120, Unit.PX);
	}

	private void addViewStatutoryPayments() {
		// TODO Auto-generated method stub
		viewstatutoryPayments = new TextColumn<StaffingDetails>() {
			@Override
			public String getValue(StaffingDetails object) {
				// TODO Auto-generated method stub
				return object.getSatutoryPayments() + "";
			}
		};

		table.addColumn(viewstatutoryPayments, "Statutory Payments");
		table.setColumnWidth(viewstatutoryPayments, 120, Unit.PX);
	}

	private void addViewBonus() {
		// TODO Auto-generated method stub
		viewspBonus = new TextColumn<StaffingDetails>() {

			@Override
			public String getValue(StaffingDetails object) {
				// TODO Auto-generated method stub
				return object.getSpBonus() + "";
			}
		};

		table.addColumn(viewspBonus, "Bonus");
		table.setColumnWidth(viewspBonus, 120, Unit.PX);
	}

	private void addViewSPMWF() {
		// TODO Auto-generated method stub
		viewspMWF = new TextColumn<StaffingDetails>() {

			@Override
			public String getValue(StaffingDetails object) {
				// TODO Auto-generated method stub
				return object.getSpMWF() + "";
			}
		};

//		table.addColumn(viewspMWF, "SP MWF");
		table.addColumn(viewspMWF, "CLWF/CMWF");
		table.setColumnWidth(viewspMWF, 120, Unit.PX);
		
	}

	private void addViewSPEDLI() {
		// TODO Auto-generated method stub
		viewspEDLI = new TextColumn<StaffingDetails>() {

			@Override
			public String getValue(StaffingDetails object) {
				// TODO Auto-generated method stub
				return object.getSpEDLI() + "";
			}
		};

//		table.addColumn(viewspEDLI, "SP EDLI");
		table.addColumn(viewspEDLI, "CEDLI");
		table.setColumnWidth(viewspEDLI, 120, Unit.PX);
	}

	private void addViewSPESIC() {
		// TODO Auto-generated method stub
		viewspESIC = new TextColumn<StaffingDetails>() {

			@Override
			public String getValue(StaffingDetails object) {
				// TODO Auto-generated method stub
				return object.getSpESIC() + "";
			}
		};

//		table.addColumn(viewspESIC, "SP ESIC");
		table.addColumn(viewspESIC, "CESIC");
		table.setColumnWidth(viewspESIC, 120, Unit.PX);
	}

	private void addViewSPPF() {
		// TODO Auto-generated method stub
		viewspPf = new TextColumn<StaffingDetails>() {

			@Override
			public String getValue(StaffingDetails object) {
				// TODO Auto-generated method stub
				return object.getSpPF() + "";
			}
		};

//		table.addColumn(viewspPf, "SP PF");
		table.addColumn(viewspPf, "CPF");
		table.setColumnWidth(viewspPf, 120, Unit.PX);
	}

	private void addViewStatutoryDeductions() {
		// TODO Auto-generated method stub
		viewstatutoryDeduction = new TextColumn<StaffingDetails>() {

			@Override
			public String getValue(StaffingDetails object) {
				// TODO Auto-generated method stub
				return object.getSatutoryDeductions() + "";
			}
		};

		table.addColumn(viewstatutoryDeduction, "Statutory Deduction");
		table.setColumnWidth(viewstatutoryDeduction, 120, Unit.PX);
	}

	private void addViewSDLWF() {
		// TODO Auto-generated method stub
		viewsdLWF = new TextColumn<StaffingDetails>() {

			@Override
			public String getValue(StaffingDetails object) {
				// TODO Auto-generated method stub
				return object.getSdLWF() + "";
			}
		};

//		table.addColumn(viewsdLWF, "SD LWF");
		table.addColumn(viewsdLWF, "LWF/MWF");
		table.setColumnWidth(viewsdLWF, 120, Unit.PX);
		
	}

	private void addViewSDPT() {
		// TODO Auto-generated method stub
		viewsdPT = new TextColumn<StaffingDetails>() {

			@Override
			public String getValue(StaffingDetails object) {
				// TODO Auto-generated method stub
				return object.getSdPT() + "";
			}
		};

//		table.addColumn(viewsdPT, "SD PT");
		table.addColumn(viewsdPT, "PT");
		table.setColumnWidth(viewsdPT, 120, Unit.PX);
	}

	private void addViewSDESIC() {
		// TODO Auto-generated method stub
		viewsdESIC = new TextColumn<StaffingDetails>() {

			@Override
			public String getValue(StaffingDetails object) {
				// TODO Auto-generated method stub
				return object.getSdESIC() + "";
			}
		};

//		table.addColumn(viewsdESIC, "SD ESIC");
		table.addColumn(viewsdESIC, "EESIC");
		table.setColumnWidth(viewsdESIC, 120, Unit.PX);
	}

	private void addViewSDPF() {
		// TODO Auto-generated method stub
		viewsdPf = new TextColumn<StaffingDetails>() {

			@Override
			public String getValue(StaffingDetails object) {
				// TODO Auto-generated method stub
				return object.getSdPF() + "";
			}
		};

//		table.addColumn(viewsdPf, "SD PF");
		table.addColumn(viewsdPf, "EPF");
		table.setColumnWidth(viewsdPf, 120, Unit.PX);
	}

	private void addViewTakeHome() {
		// TODO Auto-generated method stub
		viewTakeHome = new TextColumn<StaffingDetails>() {

			@Override
			public String getValue(StaffingDetails object) {
				// TODO Auto-generated method stub
				return object.getTakeHome() + "";
			}
		};

		table.addColumn(viewTakeHome, "TakeHome");
		table.setColumnWidth(viewTakeHome, 120, Unit.PX);
	}

	private void addViewCBonus() {
		// TODO Auto-generated method stub
		viewCBonus = new TextColumn<StaffingDetails>() {

			@Override
			public String getValue(StaffingDetails object) {
				// TODO Auto-generated method stub
				return object.getcBonus() + "";
			}
		};

		table.addColumn(viewCBonus, "CBonus");
		table.setColumnWidth(viewCBonus, 120, Unit.PX);
	}

	private void addViewCLeaves() {
		// TODO Auto-generated method stub
		viewCLeave = new TextColumn<StaffingDetails>() {

			@Override
			public String getValue(StaffingDetails object) {
				// TODO Auto-generated method stub
				return object.getcLeave() + "";
			}
		};

		table.addColumn(viewCLeave, "C Leave");
		table.setColumnWidth(viewCLeave, 120, Unit.PX);
	}

	private void addViewBandP() {
		// TODO Auto-generated method stub
		viewBandP = new TextColumn<StaffingDetails>() {

			@Override
			public String getValue(StaffingDetails object) {
				// TODO Auto-generated method stub
				return object.getBandP() + "";
			}
		};

		table.addColumn(viewBandP, "B&P");
		table.setColumnWidth(viewBandP, 120, Unit.PX);
	}

	private void addViewConyevance() {
		// TODO Auto-generated method stub
		viewConveyance = new TextColumn<StaffingDetails>() {

			@Override
			public String getValue(StaffingDetails object) {
				// TODO Auto-generated method stub
				return object.getConveyance() + "";
			}
		};

		table.addColumn(viewConveyance, "Conveyance");
		table.setColumnWidth(viewConveyance, 120, Unit.PX);
	}

	private void addViewWashingAllowance() {
		// TODO Auto-generated method stub
		viewWashingAllowance = new TextColumn<StaffingDetails>() {

			@Override
			public String getValue(StaffingDetails object) {
				// TODO Auto-generated method stub
				return object.getWashingAllowance() + "";
			}
		};

		table.addColumn(viewWashingAllowance, "Washing Allowance");
		table.setColumnWidth(viewWashingAllowance, 120, Unit.PX);
	}

	private void addViewMedical() {
		// TODO Auto-generated method stub
		viewMedical = new TextColumn<StaffingDetails>() {

			@Override
			public String getValue(StaffingDetails object) {
				// TODO Auto-generated method stub
				return object.getMedical() + "";
			}
		};

		table.addColumn(viewMedical, "Medical");
		table.setColumnWidth(viewMedical, 120, Unit.PX);
	}

	private void addViewUniformCost() {
		// TODO Auto-generated method stub
		viewUniformCost = new TextColumn<StaffingDetails>() {

			@Override
			public String getValue(StaffingDetails object) {
				// TODO Auto-generated method stub
				return object.getUniformCost() + "";
			}
		};

		table.addColumn(viewUniformCost, "Uniform Cost");
		table.setColumnWidth(viewUniformCost, 120, Unit.PX);
	}

	private void addViewDeleteButton() {
		// TODO Auto-generated method stub
		viewDeleteButton = new TextColumn<StaffingDetails>() {

			@Override
			public String getValue(StaffingDetails object) {
				// TODO Auto-generated method stub
				return "-";
			}
		};
		table.addColumn(viewDeleteButton, "Delete");
		table.setColumnWidth(viewDeleteButton, 120, Unit.PX);

	}

	private void addViewChargeableOTRates() {
		// TODO Auto-generated method stub
		viewchargableOTRates = new TextColumn<StaffingDetails>() {

			@Override
			public String getValue(StaffingDetails object) {
				// TODO Auto-generated method stub
				return object.getChargableOTRates() + "";
			}
		};

		table.addColumn(viewchargableOTRates, "Chargeable OT Rates");
		table.setColumnWidth(viewchargableOTRates, 120, Unit.PX);
	}

	private void addViewPayableOTRates() {
		// TODO Auto-generated method stub
		viewpayableOTRates = new TextColumn<StaffingDetails>() {

			@Override
			public String getValue(StaffingDetails object) {
				// TODO Auto-generated method stub
				return object.getPayableOTRates() + "";
			}
		};

		table.addColumn(viewpayableOTRates, "Payable OT Rates");
		table.setColumnWidth(viewpayableOTRates, 120, Unit.PX);
	}

	private void addViewNetSalary() {
		// TODO Auto-generated method stub
		viewnetSalary = new TextColumn<StaffingDetails>() {

			@Override
			public String getValue(StaffingDetails object) {
				// TODO Auto-generated method stub
				return object.getNetSalary() + "";
			}
		};

		table.addColumn(viewnetSalary, "Net Salary");
		table.setColumnWidth(viewnetSalary, 120, Unit.PX);
	}

	private void addViewOthers() {
		// TODO Auto-generated method stub
		viewothers = new TextColumn<StaffingDetails>() {

			@Override
			public String getValue(StaffingDetails object) {
				// TODO Auto-generated method stub
				return object.getOthers() + "";
			}
		};

		table.addColumn(viewothers, "Other");
		table.setColumnWidth(viewothers, 120, Unit.PX);
	}

	private void addViewHRA() {
		// TODO Auto-generated method stub
		viewhra = new TextColumn<StaffingDetails>() {

			@Override
			public String getValue(StaffingDetails object) {
				// TODO Auto-generated method stub
				return object.getHra() + "";
			}
		};

		table.addColumn(viewhra, "HRA");
		table.setColumnWidth(viewhra, 120, Unit.PX);
	}

	private void addViewDA() {
		// TODO Auto-generated method stub
		viewda = new TextColumn<StaffingDetails>() {

			@Override
			public String getValue(StaffingDetails object) {
				// TODO Auto-generated method stub
				return object.getDa() + "";
			}
		};

		table.addColumn(viewda, "DA");
		table.setColumnWidth(viewda, 120, Unit.PX);
	}

	private void addViewBasic() {
		// TODO Auto-generated method stub
		viewbasic = new TextColumn<StaffingDetails>() {

			@Override
			public String getValue(StaffingDetails object) {
				// TODO Auto-generated method stub
				return object.getBasic() + "";
			}
		};

		table.addColumn(viewbasic, "Basic");
		table.setColumnWidth(viewbasic, 120, Unit.PX);
	}

	private void addViewGrossSalary() {
		// TODO Auto-generated method stub
		hideColumn=true;
		viewgrossSalary = new TextColumn<StaffingDetails>() {

			@Override
			public String getValue(StaffingDetails object) {
				// TODO Auto-generated method stub
				return object.getGrossSalary() + "";
			}
		};

		table.addColumn(viewgrossSalary, "Gross Salary");
		table.setColumnWidth(viewgrossSalary, 120, Unit.PX);
	}

	private void addViewNoOfStaff() {
		// TODO Auto-generated method stub
		viewnoOfStaff = new TextColumn<StaffingDetails>() {

			@Override
			public String getValue(StaffingDetails object) {
				// TODO Auto-generated method stub
				return object.getNoOfStaff() + "";
			}
		};

		table.addColumn(viewnoOfStaff, "No Of Staff");
		table.setColumnWidth(viewnoOfStaff, 120, Unit.PX);
	}

	private void addViewDesignation() {
		// TODO Auto-generated method stub
		viewdesignation = new TextColumn<StaffingDetails>() {

			@Override
			public String getValue(StaffingDetails object) {
				// TODO Auto-generated method stub
				return object.getDesignation();
			}
		};

		table.addColumn(viewdesignation, "Designation");
		table.setColumnWidth(viewdesignation, 120, Unit.PX);
	}

	private void addViewCTCTemplates() {
		// TODO Auto-generated method stub
		viewCTCTemplates = new TextColumn<StaffingDetails>() {

			@Override
			public String getValue(StaffingDetails object) {
				// TODO Auto-generated method stub
				return object.getCtcTemplates();
			}
		};

		table.addColumn(viewCTCTemplates, "CTC Template");
		table.setColumnWidth(viewCTCTemplates, 100, Unit.PX);
	}

	private void addeditColumn() {
		System.out.println("Edit Table");
		makeListOfDesignation();
		addDeleteButton();
		addDesignation();
	//	editCTCAmount();
		addCTCTemplates();
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub

	}
	
//	public void updateFromScreen(StaffingDetails object,int index){
//
//		try {
//			object.setGrossSalary(object.getGrossSalary());
//		} catch (Exception e) {
//			object.setGrossSalary(0);
//		}
//		object.setNoOfStaff(object.getNoOfStaff());
//		
//		// object.setGrossSalary(grossSalaryAmt);
//		object.setBasic(object.getBasic());
//		object.setDa(object.getDa());
//		object.setHra(object.getHra());
//		object.setMedical(object.getMedical());
//		try {
//			object.setWashingAllowance(object
//						.getWashingAllowance());
//		} catch (Exception e) {
//				e.printStackTrace();
//				object.setWashingAllowance(0);
//			}
//		object.setConveyance(object.getConveyance());
//		object.setOthers(object.getOthers());
////		double bandpValue=object.getGrossSalary()-(Double.parseDouble(object.getDa())+Double.parseDouble(object.getHra())+Double.parseDouble(object.getMedical())+object.getWashingAllowance()+Double.parseDouble(object.getConveyance())+Double.parseDouble(object.getBandP()));
////		Console.log("object.getBandP()" + bandpValue);
////		if(!editedBandP)
//		object.setBandP(object.getBandP());
//		double pfValue = ((object.getBasic() + Double
//				.parseDouble(object.getDa())) / 100) * 12;
//		Console.log("pfValue" + pfValue);
//		object.setSdPF(pfValue + "");
//		if (object.getGrossSalary() < 21001) {
//			double esicCalValue = object.getGrossSalary()
//					- object.getWashingAllowance();
//			double esicValue = (esicCalValue / 100) * 1.75;
//			Console.log("esicValue" + esicValue);
//			object.setSdESIC(esicValue + "");
//		} else {
//			object.setSdESIC(0 + "");
//		}
//
//		if (object.getGrossSalary() >= 10000) {
//			object.setSdPT(200 + "");
//		} else {
//			object.setSdPT(175 + "");
//		}
//		Console.log("pt" + object.getSdPT());
//		Console.log("object.getSdLWF()" + object.getSdLWF());
//		object.setSdLWF(object.getSdLWF());
//		
//		double satutoryDeduction = Double
//				.parseDouble(object.getSdPF())
//				+ Double.parseDouble(object.getSdESIC())
//				+ Double.parseDouble(object.getSdLWF())
//				+ Double.parseDouble(object.getSdPT());
//		Console.log("satutoryDeduction" + satutoryDeduction);
//		object.setSatutoryDeductions(satutoryDeduction + "");
//		double netSalaryValue = object.getGrossSalary()
//				- Double.parseDouble(object
//						.getSatutoryDeductions().trim());
//		Console.log("netSalaryValue" + netSalaryValue);
//		// Net Salary
//		object.setNetSalary(netSalaryValue);
//		// C Leave
//		double cLeaveValue = ((object.getBasic() + Double
//				.parseDouble(object.getDa().trim()))*(1.75/30));
//		Console.log("cLeaveValue" + cLeaveValue);
//		object.setcLeave(cLeaveValue + "");
////		CNCForm form = new CNCForm();
//		double cBonusPercent = 0;
//		cBonusPercent = object.getcBonusPercentage();//Double.parseDouble(form.getTbcBonus().getValue().trim());
//		Console.log("cBonusPercent" + cBonusPercent);
//		double cBonusValue = ((object.getBasic() + Double
//				.parseDouble(object.getDa().trim())))
//				* (cBonusPercent / 100);
//		Console.log("cBonusValue" + cBonusValue);
//		object.setcBonus(cBonusValue + "");
//		
//		double takeHomeValue = object.getNetSalary()
//				+ Double.parseDouble(object.getcLeave())
//				+ Double.parseDouble(object.getcBonus());
//		Console.log("takeHomeValue" + takeHomeValue);
//		object.setTakeHome(takeHomeValue + "");
//
//		// Statutory Payments
//		double cpfValue = ((object.getBasic() + Double
//				.parseDouble(object.getDa())) / 100) * 12;
//		Console.log("cpfValue" + cpfValue);
//		object.setSpPF(cpfValue + "");
//		if (object.getGrossSalary() < 21001) {
//			double esicCalValue = object.getGrossSalary()
//					- object.getWashingAllowance();
//			double esicValue = (esicCalValue / 100) * 4.75;
//			Console.log("c esicValue" + esicValue);
//			object.setSpESIC(esicValue + "");
//		} else {
//			object.setSpESIC(0 + "");
//		}
//		double edilCalValue = object.getBasic()
//				+ Double.parseDouble(object.getDa().trim());
//		double edilValue = edilCalValue * (1.16 / 100);
//		Console.log("edilValue" + edilValue);
//		object.setSpEDLI(edilValue + "");
//		object.setSpMWF(object.getSpMWF());
//		double bonusValue = 0;
//		if (object.isRelever()) {
//			bonusValue = ((object.getBasic() + Double
//					.parseDouble(object.getDa())) * (cBonusPercent / 100))
//					+ (((object.getBasic() + Double
//							.parseDouble(object.getDa())) * (cBonusPercent / 100)) * (1.75 / 30));
//		} else {
//			bonusValue = ((object.getBasic() + Double
//					.parseDouble(object.getDa())) * (cBonusPercent / 100));// +(1.75/30);
//		}
//		Console.log("bonusValue" + bonusValue);
//		object.setSpBonus(bonusValue + "");
//
//		double statutoryPayments = Double
//				.parseDouble(object.getSpBonus())
//				+ Double.parseDouble(object.getSpEDLI())
//				+ Double.parseDouble(object.getSpESIC())
//				+ Double.parseDouble(object.getSpMWF())
//				+ Double.parseDouble(object.getSpPF());
//		Console.log("statutoryPayments" + statutoryPayments);
//		object.setSatutoryPayments(statutoryPayments + "");
//		Console.log("object.getGrossSalary()+Double.parseDouble(object.getSatutoryPayments())"
//				+ object.getGrossSalary()
//				+ Double.parseDouble(object
//						.getSatutoryPayments()));
//		object.setTotal(object.getGrossSalary()
//				+ Double.parseDouble(object
//						.getSatutoryPayments()) + "");
//
//		double paidLeavesValue = 0;
//		Console.log("form.cb_Relever.getValue()"
//				+ object.isRelever() );
//
//		if (object.isRelever()) {
//			paidLeavesValue = ((object.getGrossSalary() + ((Double.parseDouble(object.getSatutoryPayments()))-Double.parseDouble(object.getSpBonus()))) * (1.75/30))
//													+
//					(((object.getGrossSalary() + ((Double.parseDouble(object.getSatutoryPayments()))-Double.parseDouble(object.getSpBonus()))))*(1.75/30))*(1.75 / 30);
//		} else {
//			paidLeavesValue = (object.getGrossSalary() + ((Double.parseDouble(object.getSatutoryPayments()))-Double.parseDouble(object.getSpBonus()))) * (1.75/30);
//		}
//		Console.log("paidLeavesValue" + paidLeavesValue);
//
//		object.setPaidLeaves(paidLeavesValue + "");
//		
//			object.setEmployementOverHeadCost(object.getOverHeadCost());
//		
//		
//		double totalPaidAndOverheadValue = Double
//				.parseDouble(object.getTotal())
//				+ Double.parseDouble(object.getPaidLeaves())
//				+ Double.parseDouble(object
//						.getEmployementOverHeadCost());
//		Console.log("totalPaidAndOverheadValue"
//				+ totalPaidAndOverheadValue);
//		object.setTotalOfPaidAndOverhead(totalPaidAndOverheadValue
//				+ "");
//		object.setTotalPaidIntoEmployee(totalPaidAndOverheadValue*object.getNoOfStaff());
//		double feePercent = 0;
//		try {
//			if (object.getManageMentfeesPercent()!=0) {
//				feePercent = object.getManageMentfeesPercent();
//			} 
//			Console.log("feePercent" + feePercent);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		double managementFeesValue = object.getTotalPaidIntoEmployee()*( feePercent / 100);
//		Console.log("managementFeesValue"
//				+ managementFeesValue);
//		if(!editedManaagementFees)
//		object.setManagementFees(managementFeesValue + "");
//
//		double managementTotal = managementFeesValue+object.getTotalPaidIntoEmployee();
//		Console.log("managementTotal" + managementTotal);
//		object.setTotalIncludingManagementFees(managementTotal
//				+ "");
//		double grandTotalValue = managementTotal;
//		object.setGrandTotalOfCTC(grandTotalValue + "");
//		Console.log("grandTotalValue" + grandTotalValue);
//	
//		RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
//		table.redrawRow(index);
//	
//	}
//


	public void updateCLeaveApplicable(StaffingDetails object,int count) {
		// TODO Auto-generated method stub
//		editedcLeaves=true;
		/**
		 * Rahul Verma added paid days. And changed(21/12) to (paiddays/12) where paiddays is provided by User
		 */
		double paiddaysValue=Double.parseDouble(object.getPaidLeaveDays().trim());
		
		if(object.iscLeaveApplicable()){
			double cLeaveValue = ((object.getBasic() + Double
					.parseDouble(object.getDa().trim()))*((paiddaysValue/12)/30));
			Console.log("cLeaveValue" + cLeaveValue);
			object.setcLeave((Math.round(cLeaveValue*100.0)/100.0)+"");
		}else{
			object.setcLeave(0+"");
		}
		

		/**
		 * @author Anil
		 * @since 04-09-2020
		 */
		CTCTemplate ctcTemplate=ctcTemplateMap.get(object.getCtcTemplates());
		if(paidleave!=null){
			double plAmt=0;
			if(paidleave!=null){
				for(OtEarningComponent plComp:paidleave.getOtEarningCompList()){
					System.out.println("NAME : "+plComp.getComponentName());
					for(CtcComponent earningComp:ctcTemplate.getEarning()){
						System.out.println("EARN NAME : "+earningComp.getName());
						if(plComp.getComponentName().equalsIgnoreCase("Gross Earning")){
							plAmt=plAmt+earningComp.getAmount()/12;
						}else if(plComp.getComponentName().equals(earningComp.getName())){
							System.out.println("AMT : "+earningComp.getAmount()/12);
							plAmt=plAmt+earningComp.getAmount()/12;
						}
						
					}
				}
			}
			System.out.println("BASE AMT : "+plAmt);
			plAmt=plAmt*paidleave.getRate()/100;
			System.out.println("PL AMT : "+plAmt);
			object.setcLeave((Math.round(plAmt*100.0)/100.0)+"");
		}
		
		/**
		 * Commented below code
		 */
		double cBonusPercent = 0;
		cBonusPercent = object.getcBonusPercentage();//Double.parseDouble(form.getTbcBonus().getValue().trim());
//		Console.log("cBonusPercent" + cBonusPercent);
//		double cBonusValue = ((object.getBasic() + Double
//				.parseDouble(object.getDa().trim())))
//				* (cBonusPercent / 100);
//		Console.log("cBonusValue" + cBonusValue);
////		if(!editedcBonus)
//		object.setcBonus(object.getcBonus() + "");
		
		
		object.setTravellingAllowance(Math.round(object.getTravellingAllowance()*100.0)/100.0);
		/*****8-5-2020 by Amol*******/
		
		double plEsic=0;
		if(object.iscLeaveApplicable()&&object.getcLeave()!=null){
			plEsic=((Double.parseDouble(object.getcLeave())*0.75)/100);
			Console.log("PL ESIC Value "+plEsic);
			object.setPlEsic(plEsic);
		}else{
			object.setPlEsic(0);
		}
		
		
		
//		double ePlEsic=0;
//		if(object.iscLeaveApplicable()&&object.getPaidLeaves()!=null){
//			ePlEsic=((Double.parseDouble(object.getPaidLeaves())*3.25)/100);
//			object.setcPlEsic(ePlEsic);
//		}else{
//			object.setcPlEsic(0);
//		}
		
		
		
		
		//Take Home Salary
		double takeHomeValue = object.getNetSalary()
				+ Double.parseDouble(object.getcLeave())
				+ Double.parseDouble(object.getcBonus())
				+ object.getTravellingAllowance();
		Console.log("takeHomeValue" + takeHomeValue);
		object.setTakeHome((Math.round(takeHomeValue)) + "");
		// Statutory Payments
		//pfValue
		double cpfValue = ((object.getBasic() + Double.parseDouble(object.getDa())) / 100) * 12;
		Console.log("cpfValue" + cpfValue);
		// if(!editedSPPF)
		object.setSpPF(object.getSpPF() + "");
		
//		if (object.getGrossSalary() < 21001) {
//			double esicCalValue = object.getGrossSalary()-object.getWashingAllowance();
//			double esicValue = (esicCalValue / 100) * 4.75;
//			Console.log("c esicValue" + esicValue);
//			object.setSpESIC(Math.round(esicValue) + "");
//		} else {
			object.setSpESIC(object.getSpESIC() + "");
//		}
		
		double edilCalValue = object.getBasic()
				+ Double.parseDouble(object.getDa().trim());
		double edilValue = edilCalValue * (1.16 / 100);
		Console.log("edilValue" + edilValue);
		object.setSpEDLI(object.getSpEDLI() + "");
		object.setSpMWF(object.getSpMWF()+"");
		
		double bonusValue = 0;
		/**
		 * Rahul Verma added paid days. And changed(21/12) to (paiddays/12) where paiddays is provided by User
		 */
		double paiddaysforBonus=Double.parseDouble(object.getPaidLeaveDays().trim());
		if (object.isBonusRelever()) {
			bonusValue = ((object.getBasic() + Double
					.parseDouble(object.getDa())) * (cBonusPercent / 100))
					+ (((object.getBasic() + Double
							.parseDouble(object.getDa())) * (cBonusPercent / 100)) * ((paiddaysforBonus/12)/ 30));
		} else {
			bonusValue = ((object.getBasic() + Double
					.parseDouble(object.getDa())) * (cBonusPercent / 100));// +(1.75/30);
		}
		Console.log("bonusValue" + bonusValue);
		object.setSpBonus((Math.round(bonusValue*100.0)/100.0) + "");
		/** date 17.11.2018 added by komal for paid leave calculation as discussed with maradona**/
		double paidLeavesValue = 0;
//		Console.log("form.cb_Relever.getValue()"
//				+ object.isRelever() );
		/**
		 * Rahul Verma added paid days. And changed(21/12) to (paiddays/12) where paiddays is provided by User
		 */
		double paiddays=Double.parseDouble(object.getPaidLeaveDays().trim());
		double totalValue = Double.parseDouble(object.getSpEDLI())
				+ Double.parseDouble(object.getSpESIC())
				+ Double.parseDouble(object.getSpMWF())
				+ Double.parseDouble(object.getSpPF());
		totalValue = Math.round(totalValue*100.0)/100.0;
		if (object.isPaidLeaveRelever()) {
			paidLeavesValue = ((object.getGrossSalary() + (totalValue)) * ((paiddays/12)/30))
													+
					(((object.getGrossSalary() + (totalValue)))*((paiddays/12)/30))*( (paiddays/12)/ 30);
		} else {
			paidLeavesValue = (object.getGrossSalary() + (totalValue)) * ((paiddays/12)/30);
		}
		Console.log("paidLeavesValue" + paidLeavesValue);

		object.setPaidLeaves((Math.round(paidLeavesValue*100.0)/100.0) + "");
		
		/**
		 *  end komal
		 */
		

		/**
		 * @author Anil
		 * @since 04-09-2020
		 */
//		CTCTemplate ctcTemplate=ctcTemplateMap.get(object.getCtcTemplates());
		if(paidleave!=null){
			double plAmt=0;
			if(paidleave!=null){
				for(OtEarningComponent plComp:paidleave.getOtEarningCompList()){
					System.out.println("NAME : "+plComp.getComponentName());
					for(CtcComponent earningComp:ctcTemplate.getEarning()){
						System.out.println("EARN NAME : "+earningComp.getName());
						if(plComp.getComponentName().equalsIgnoreCase("Gross Earning")){
							plAmt=plAmt+earningComp.getAmount()/12;
						}else if(plComp.getComponentName().equals(earningComp.getName())){
							System.out.println("AMT : "+earningComp.getAmount()/12);
							plAmt=plAmt+earningComp.getAmount()/12;
						}
						
					}
				}
			}
			System.out.println("BASE AMT : "+plAmt);
			plAmt=plAmt*paidleave.getRate()/100;
			System.out.println("PL AMT : "+plAmt);
			object.setPaidLeaves((Math.round(plAmt*100.0)/100.0)+"");
		}
		
		/**Date 24-6-2019 
		 * @author Amol
		 * adding gratuity formula
		 */
		double gratuity=0;
//		if(object.isGratuityApplicable()){
//		gratuity
//			=((((object.getBasic())+Double.parseDouble(object.getDa()))*4.81)/(100));
//			object.setGratuity((Math.round(gratuity)+""));
//		  }else{
//			  object.setGratuity(0+"");
//		 }
		object.setGratuity(object.getGratuity());
		object.setSupervision(object.getSupervision());
		object.setTrainingCharges(object.getTrainingCharges());
		double statutoryPayments = Double
				.parseDouble(object.getSpBonus())
				+ Double.parseDouble(object.getSpEDLI())
				+ Double.parseDouble(object.getSpESIC())
				+ Double.parseDouble(object.getSpMWF())
				+ Double.parseDouble(object.getSpPF())
				+ Double.parseDouble(object.getPaidLeaves())
		        +Double.parseDouble(object.getGratuity())
		        +(object.getAdministrativecost())
//		        +(object.getUniformCost())
		        +(object.getSupervision())
		        +(object.getTrainingCharges())
		        +(object.getcPlEsic());
		
		object.setSatutoryPayments((Math.round(statutoryPayments)) + "");
		object.setTotal((Math.round((object.getGrossSalary()
				+ Double.parseDouble(object
						.getSatutoryPayments())))) + "");
//		double paidLeavesValue = 0;
////		Console.log("form.cb_Relever.getValue()"
////				+ object.isRelever() );
//		/**
//		 * Rahul Verma added paid days. And changed(21/12) to (paiddays/12) where paiddays is provided by User
//		 */
//		double paiddays=Double.parseDouble(object.getPaidLeaveDays().trim());
//		
//		if (object.isPaidLeaveRelever()) {
//			paidLeavesValue = ((object.getGrossSalary() + ((Double.parseDouble(object.getSatutoryPayments()))-Double.parseDouble(object.getSpBonus()))) * ((paiddays/12)/30))
//													+
//					(((object.getGrossSalary() + ((Double.parseDouble(object.getSatutoryPayments()))-Double.parseDouble(object.getSpBonus()))))*((paiddays/12)/30))*((paiddays/12)/ 30);
//		} else {
//			paidLeavesValue = (object.getGrossSalary() + ((Double.parseDouble(object.getSatutoryPayments()))-Double.parseDouble(object.getSpBonus()))) * ((paiddays/12)/30);
//		}
//		Console.log("paidLeavesValue" + paidLeavesValue);
//
//		object.setPaidLeaves((Math.round(paidLeavesValue*100.0)/100.0) + "");
		object.setEmployementOverHeadCost(object.getOverHeadCost());
		object.setTravellingAllowanceOverhead(object.getTravellingAllowanceOverhead());
		object.setUniformCost(object.getUniformCost());
		double totalPaidAndOverheadValue = Double
				.parseDouble(object.getTotal())
				+ object.getAdditionalAllowance()
				+ Double.parseDouble(object
						.getEmployementOverHeadCost())
				+ object.getTravellingAllowanceOverhead()+object.getUniformCost();
		Console.log("totalPaidAndOverheadValue"
				+ totalPaidAndOverheadValue);
		object.setTotalOfPaidAndOverhead((Math.round(totalPaidAndOverheadValue))
				+ "");
		object.setTotalPaidIntoEmployee(Math.round((totalPaidAndOverheadValue*object.getNoOfStaff())));
		double feePercent = 0;
		double managementFeesValue = 0;
		try {
//			if (object.getManageMentfeesPercent()!=0) {
//				feePercent = object.getManageMentfeesPercent();
//			} 
			if (object.getManageMentfeesPercent()!=0) {
				feePercent = object.getManageMentfeesPercent();
				//komal
				managementFeesValue = object.getTotalPaidIntoEmployee()*( feePercent / 100);
				Console.log("managementFeesValue"
						+ managementFeesValue);
				object.setManagementFees((Math.round(managementFeesValue*100.0)/100.0) + "");
			} else{
				if(object.getManagementFees()!=null && !object.getManagementFees().equals("")){
					managementFeesValue = Double.parseDouble(object.getManagementFees());
				}
				/**
				 * @author Anil
				 * @since 03-09-2020
				 */
				if(managementFeesAsPerStaff&&object.getNoOfStaff()!=0&&object.getManagementFeesRate()!=0){
					managementFeesValue=object.getNoOfStaff()*object.getManagementFeesRate();
				}
			}
			Console.log("feePercent" + feePercent);
		} catch (Exception e) {
			e.printStackTrace();
		}

//		double managementFeesValue = object.getTotalPaidIntoEmployee()*( feePercent / 100);
//		Console.log("managementFeesValue"
//				+ managementFeesValue);
		object.setManagementFees(managementFeesValue + "");

		double managementTotal = managementFeesValue+object.getTotalPaidIntoEmployee();
		Console.log("managementTotal" + managementTotal);
		object.setTotalIncludingManagementFees((Math.round(managementTotal))
				+ "");
		
		double grandTotalValue = managementTotal;
		object.setGrandTotalOfCTC((Math.round(grandTotalValue*100.0)/100.0) + "");
		Console.log("grandTotalValue" + grandTotalValue);
		updateRow(object, count,false);
		RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
		table.redrawRow(count);
		
//		updateRow(object, index);
	}

	public void updateCBonusApplicable(StaffingDetails object,int count) {
		// TODO Auto-generated method stub
//		editedcBonus=true;
		if(object.iscBonusApplicable()){
			double cBonusValue = ((object.getBasic() + Double
					.parseDouble(object.getDa().trim())))
					* (object.getcBonusPercentage() / 100);
			Console.log("cBonusValue" + cBonusValue);
			object.setcBonus((Math.round(cBonusValue*100.0)/100.0)+"");
		}else{
			object.setcBonus(0+"");
		}
		object.setTravellingAllowance(Math.round(object.getTravellingAllowance()*100.0)/100.0);
		
		double bonusEsic=0;
		if(object.iscBonusApplicable()&&object.getcBonus()!=null){
			bonusEsic=((Double.parseDouble(object.getcBonus())*0.75)/100);	
			Console.log("bonusEsic "+bonusEsic);
			object.setBonusEsic(bonusEsic);
		}else{
			object.setBonusEsic(0);	
		}
		
		
//		double eBonusEsic=0;
//		if(object.iscBonusApplicable()&&object.getSpBonus()!=null){
//			eBonusEsic=((Double.parseDouble(object.getSpBonus())*3.25)/100);	
//			object.setcBonusEsic(eBonusEsic);
//		}else{
//			object.setcBonusEsic(0);	
//		}
		
		
		
		
		
		
		//Take Home Salary
		double takeHomeValue = object.getNetSalary()
				+ Double.parseDouble(object.getcLeave())
				+ Double.parseDouble(object.getcBonus())
				+ object.getTravellingAllowance();
		Console.log("takeHomeValue" + takeHomeValue);
		object.setTakeHome((Math.round(takeHomeValue)) + "");
		// Statutory Payments
		//pfValue
		double cpfValue = ((object.getBasic() + Double.parseDouble(object.getDa())) / 100) * 12;
		Console.log("cpfValue" + cpfValue);
		// if(!editedSPPF)
		object.setSpPF(object.getSpPF()+ "");
		
//		if (object.getGrossSalary() < 21001) {
//			double esicCalValue = object.getGrossSalary()-object.getWashingAllowance();
//			double esicValue = (esicCalValue / 100) * 4.75;
//			Console.log("c esicValue" + esicValue);
//			object.setSpESIC(Math.round(esicValue) + "");
//		} else {
			object.setSpESIC(object.getSpESIC() + "");
//		}
		
		double edilCalValue = object.getBasic()
				+ Double.parseDouble(object.getDa().trim());
		double edilValue = edilCalValue * (1.16 / 100);
		Console.log("edilValue" + edilValue);
		object.setSpEDLI(object.getSpEDLI() + "");
		object.setSpMWF(object.getSpMWF()+"");
		double cBonusPercent = object.getcBonusPercentage();
		double bonusValue = 0;
		/**
		 * Rahul Verma added paid days. And changed(21/12) to (paiddays/12) where paiddays is provided by User
		 */
		double paiddaysforBonus=Double.parseDouble(object.getPaidLeaveDays().trim());
		if (object.isBonusRelever()) {
			bonusValue = ((object.getBasic() + Double
					.parseDouble(object.getDa())) * (cBonusPercent / 100))
					+ (((object.getBasic() + Double
							.parseDouble(object.getDa())) * (cBonusPercent / 100)) * ((paiddaysforBonus/12)/ 30));
		} else {
			bonusValue = ((object.getBasic() + Double
					.parseDouble(object.getDa())) * (cBonusPercent / 100));// +(1.75/30);
		}
		Console.log("bonusValue" + bonusValue);
		object.setSpBonus((Math.round(bonusValue*100.0)/100.0) + "");
		/** date 17.11.2018 added by komal for paid leave calculation as discussed with maradona**/
		double paidLeavesValue = 0;
//		Console.log("form.cb_Relever.getValue()"
//				+ object.isRelever() );
		/**
		 * Rahul Verma added paid days. And changed(21/12) to (paiddays/12) where paiddays is provided by User
		 */
		double paiddays=Double.parseDouble(object.getPaidLeaveDays().trim());
		double totalValue = Double.parseDouble(object.getSpEDLI())
				+ Double.parseDouble(object.getSpESIC())
				+ Double.parseDouble(object.getSpMWF())
				+ Double.parseDouble(object.getSpPF());
		totalValue = Math.round(totalValue*100.0)/100.0;
		if (object.isPaidLeaveRelever()) {
			paidLeavesValue = ((object.getGrossSalary() + (totalValue)) * ((paiddays/12)/30))
													+
					(((object.getGrossSalary() + (totalValue)))*((paiddays/12)/30))*( (paiddays/12)/ 30);
		} else {
			paidLeavesValue = (object.getGrossSalary() + (totalValue)) * ((paiddays/12)/30);
		}
		Console.log("paidLeavesValue" + paidLeavesValue);

		object.setPaidLeaves((Math.round(paidLeavesValue*100.0)/100.0) + "");
		

		/**
		 * @author Anil
		 * @since 04-09-2020
		 */
		CTCTemplate ctcTemplate=ctcTemplateMap.get(object.getCtcTemplates());
		if(paidleave!=null){
			double plAmt=0;
			if(paidleave!=null){
				for(OtEarningComponent plComp:paidleave.getOtEarningCompList()){
					System.out.println("NAME : "+plComp.getComponentName());
					for(CtcComponent earningComp:ctcTemplate.getEarning()){
						System.out.println("EARN NAME : "+earningComp.getName());
						if(plComp.getComponentName().equalsIgnoreCase("Gross Earning")){
							plAmt=plAmt+earningComp.getAmount()/12;
						}else if(plComp.getComponentName().equals(earningComp.getName())){
							System.out.println("AMT : "+earningComp.getAmount()/12);
							plAmt=plAmt+earningComp.getAmount()/12;
						}
						
					}
				}
			}
			System.out.println("BASE AMT : "+plAmt);
			plAmt=plAmt*paidleave.getRate()/100;
			System.out.println("PL AMT : "+plAmt);
			object.setPaidLeaves((Math.round(plAmt*100.0)/100.0)+"");
		}
		
		/**
		 *  end komal
		 */
		/**Date 24-6-2019 
		 * @author Amol
		 * adding gratuity formula
		 */
		
		double gratuity=0;
//		if(object.isGratuityApplicable()){
//		gratuity
//			=((((object.getBasic())+Double.parseDouble(object.getDa()))*4.81)/(100));
//			object.setGratuity((Math.round(gratuity)+""));
//		  }else{
//			  object.setGratuity(0+"");
//		 }
		object.setGratuity(object.getGratuity());
		object.setSupervision(object.getSupervision());
		object.setTrainingCharges(object.getTrainingCharges());
		
		double statutoryPayments = Double
				.parseDouble(object.getSpBonus())
				+ Double.parseDouble(object.getSpEDLI())
				+ Double.parseDouble(object.getSpESIC())
				+ Double.parseDouble(object.getSpMWF())
				+ Double.parseDouble(object.getSpPF())
				+ Double.parseDouble(object.getPaidLeaves())
		        +Double.parseDouble(object.getGratuity())
		        +(object.getAdministrativecost())
//		        +(object.getUniformCost())
		        +(object.getSupervision())
		        +(object.getTrainingCharges())
		        +(object.getcBonusEsic());
		object.setSatutoryPayments((Math.round(statutoryPayments)) + "");
		object.setTotal((Math.round((object.getGrossSalary()
				+ Double.parseDouble(object
						.getSatutoryPayments())))) + "");
//		double paidLeavesValue = 0;
////		Console.log("form.cb_Relever.getValue()"
////				+ object.isRelever() );
//		/**
//		 * Rahul Verma added paid days. And changed(21/12) to (paiddays/12) where paiddays is provided by User
//		 */
//		double paiddays=Double.parseDouble(object.getPaidLeaveDays().trim());
//		
//		if (object.isPaidLeaveRelever()) {
//			paidLeavesValue = ((object.getGrossSalary() + ((Double.parseDouble(object.getSatutoryPayments()))-Double.parseDouble(object.getSpBonus()))) * ((paiddays/12)/30))
//													+
//					(((object.getGrossSalary() + ((Double.parseDouble(object.getSatutoryPayments()))-Double.parseDouble(object.getSpBonus()))))*((paiddays/12)/30))*((paiddays/12)/ 30);
//		} else {
//			paidLeavesValue = (object.getGrossSalary() + ((Double.parseDouble(object.getSatutoryPayments()))-Double.parseDouble(object.getSpBonus()))) * ((paiddays/12)/30);
//		}
//		Console.log("paidLeavesValue" + paidLeavesValue);
//
//		object.setPaidLeaves((Math.round(paidLeavesValue*100.0)/100.0) + "");
		object.setEmployementOverHeadCost(object.getOverHeadCost());
		object.setTravellingAllowanceOverhead(object.getTravellingAllowanceOverhead());
		object.setUniformCost(object.getUniformCost());
		double totalPaidAndOverheadValue = Double
				.parseDouble(object.getTotal())
				+ object.getAdditionalAllowance()
				+ Double.parseDouble(object
						.getEmployementOverHeadCost())
				+ object.getTravellingAllowanceOverhead()+object.getUniformCost();
		Console.log("totalPaidAndOverheadValue"
				+ totalPaidAndOverheadValue);
		object.setTotalOfPaidAndOverhead((Math.round(totalPaidAndOverheadValue))
				+ "");
		object.setTotalPaidIntoEmployee(Math.round((totalPaidAndOverheadValue*object.getNoOfStaff())));
		double feePercent = 0;
		double managementFeesValue = 0;
		try {
			if (object.getManageMentfeesPercent()!=0) {
				feePercent = object.getManageMentfeesPercent();
				//komal
				managementFeesValue = object.getTotalPaidIntoEmployee()*( feePercent / 100);
				Console.log("managementFeesValue"
						+ managementFeesValue);
				object.setManagementFees((Math.round(managementFeesValue*100.0)/100.0) + "");
			} else{
				if(object.getManagementFees()!=null && !object.getManagementFees().equals("")){
					managementFeesValue = Double.parseDouble(object.getManagementFees());
				}
				/**
				 * @author Anil
				 * @since 03-09-2020
				 */
				if(managementFeesAsPerStaff&&object.getNoOfStaff()!=0&&object.getManagementFeesRate()!=0){
					managementFeesValue=object.getNoOfStaff()*object.getManagementFeesRate();
				}
			}
//			if (object.getManageMentfeesPercent()!=0) {
//				feePercent = object.getManageMentfeesPercent();
//			} 
			Console.log("feePercent" + feePercent);
		} catch (Exception e) {
			e.printStackTrace();
		}

//		double managementFeesValue = object.getTotalPaidIntoEmployee()*( feePercent / 100);
//		Console.log("managementFeesValue"
//				+ managementFeesValue);
		object.setManagementFees(managementFeesValue + "");

		double managementTotal = managementFeesValue+object.getTotalPaidIntoEmployee();
		Console.log("managementTotal" + managementTotal);
		object.setTotalIncludingManagementFees((Math.round(managementTotal))
				+ "");
		
		double grandTotalValue = managementTotal;
		object.setGrandTotalOfCTC((Math.round(grandTotalValue*100.0)/100.0) + "");
		Console.log("grandTotalValue" + grandTotalValue);
		updateRow(object, count,false);
		RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
		table.redrawRow(count);
	}

	public void updateCBonusPercentage(StaffingDetails object,
			int count) {
		// TODO Auto-generated method stub
//		editedcBonus=true;
		if(object.iscBonusApplicable()){
			double cBonusValue = ((object.getBasic() + Double
					.parseDouble(object.getDa().trim())))
					* (object.getcBonusPercentage() / 100);
			Console.log("cBonusValue" + cBonusValue);
			object.setcBonus((Math.round(cBonusValue*100.0)/100.0)+"");
		}else{
			object.setcBonus(0+"");
		}
		
		/**
		 * @author Anil
		 * @since 19-09-2020
		 * added code to calculate bonus esic
		 */
		if(object.iscBonusApplicable()&&object.getcBonus()!=null){
			double bonusEsic=((Double.parseDouble(object.getcBonus())*0.75)/100);
			object.setBonusEsic(bonusEsic);
		}else{
			object.setBonusEsic(0);
		}
		
		object.setTravellingAllowance(Math.round(object.getTravellingAllowance()*100.0)/100.0);
		//Take Home Salary
		double takeHomeValue = object.getNetSalary()
				+ Double.parseDouble(object.getcLeave())
				+ Double.parseDouble(object.getcBonus())
				+ object.getTravellingAllowance();
		Console.log("takeHomeValue" + takeHomeValue);
		object.setTakeHome((Math.round(takeHomeValue)) + "");
		// Statutory Payments
		//pfValue
		double cpfValue = ((object.getBasic() + Double.parseDouble(object.getDa())) / 100) * 12;
		Console.log("cpfValue" + cpfValue);
		// if(!editedSPPF)
		object.setSpPF(object.getSpPF() + "");
		
//		if (object.getGrossSalary() < 21001) {
//			double esicCalValue = object.getGrossSalary()-object.getWashingAllowance();
//			double esicValue = (esicCalValue / 100) * 4.75;
//			Console.log("c esicValue" + esicValue);
//			object.setSpESIC(Math.round(esicValue) + "");
//		} else {
			object.setSpESIC(object.getSpESIC()+ "");
//		}
		
		double edilCalValue = object.getBasic()
				+ Double.parseDouble(object.getDa().trim());
		double edilValue = edilCalValue * (1.16 / 100);
		Console.log("edilValue" + edilValue);
		object.setSpEDLI(object.getSpEDLI() + "");
		object.setSpMWF(object.getSpMWF()+"");
		double cBonusPercent = object.getcBonusPercentage();
		double bonusValue = 0;/**
		 * Rahul Verma added paid days. And changed(21/12) to (paiddays/12) where paiddays is provided by User
		 */
		double paiddaysforBonus=Double.parseDouble(object.getPaidLeaveDays().trim());
		if (object.isBonusRelever()) {
			bonusValue = ((object.getBasic() + Double
					.parseDouble(object.getDa())) * (cBonusPercent / 100))
					+ (((object.getBasic() + Double
							.parseDouble(object.getDa())) * (cBonusPercent / 100)) * ((paiddaysforBonus/12)/ 30));
		} else {
			bonusValue = ((object.getBasic() + Double
					.parseDouble(object.getDa())) * (cBonusPercent / 100));// +(1.75/30);
		}
		Console.log("bonusValue" + bonusValue);
		object.setSpBonus((Math.round(bonusValue*100.0)/100.0) + "");
		
		/**
		 * @author Anil
		 * @since 19-09-2020
		 * Calculating CBonus based on Bonus (% and reliever)
		 */
		double cbonusEsic=0;
		if(object.getSpBonus()!=null){
			cbonusEsic=((Double.parseDouble(object.getSpBonus())*3.25)/100);	
			object.setcBonusEsic(cbonusEsic);
		}else{
			object.setcBonusEsic(0);
			cbonusEsic=0;	
		}
		
		/** date 17.11.2018 added by komal for paid leave calculation as discussed with maradona**/
		double paidLeavesValue = 0;
//		Console.log("form.cb_Relever.getValue()"
//				+ object.isRelever() );
		/**
		 * Rahul Verma added paid days. And changed(21/12) to (paiddays/12) where paiddays is provided by User
		 */
		double paiddays=Double.parseDouble(object.getPaidLeaveDays().trim());
		double totalValue = Double.parseDouble(object.getSpEDLI())
				+ Double.parseDouble(object.getSpESIC())
				+ Double.parseDouble(object.getSpMWF())
				+ Double.parseDouble(object.getSpPF());
		totalValue = Math.round(totalValue*100.0)/100.0;
		if (object.isPaidLeaveRelever()) {
			paidLeavesValue = ((object.getGrossSalary() + (totalValue)) * ((paiddays/12)/30))
													+
					(((object.getGrossSalary() + (totalValue)))*((paiddays/12)/30))*( (paiddays/12)/ 30);
		} else {
			paidLeavesValue = (object.getGrossSalary() + (totalValue)) * ((paiddays/12)/30);
		}
		Console.log("paidLeavesValue" + paidLeavesValue);

		object.setPaidLeaves((Math.round(paidLeavesValue*100.0)/100.0) + "");
		
		/**
		 *  end komal
		 */

		/**
		 * @author Anil
		 * @since 04-09-2020
		 */
		CTCTemplate ctcTemplate=ctcTemplateMap.get(object.getCtcTemplates());
		if(paidleave!=null){
			double plAmt=0;
			if(paidleave!=null){
				for(OtEarningComponent plComp:paidleave.getOtEarningCompList()){
					System.out.println("NAME : "+plComp.getComponentName());
					for(CtcComponent earningComp:ctcTemplate.getEarning()){
						System.out.println("EARN NAME : "+earningComp.getName());
						if(plComp.getComponentName().equalsIgnoreCase("Gross Earning")){
							plAmt=plAmt+earningComp.getAmount()/12;
						}else if(plComp.getComponentName().equals(earningComp.getName())){
							System.out.println("AMT : "+earningComp.getAmount()/12);
							plAmt=plAmt+earningComp.getAmount()/12;
						}
						
					}
				}
			}
			System.out.println("BASE AMT : "+plAmt);
			plAmt=plAmt*paidleave.getRate()/100;
			System.out.println("PL AMT : "+plAmt);
			object.setPaidLeaves((Math.round(plAmt*100.0)/100.0)+"");
		}
		
		/**
		 * @author Anil
		 * @since 19-09-2020
		 */
		double cplEsic=0;
		if(object.getPaidLeaves()!=null){
			cplEsic=((Double.parseDouble(object.getPaidLeaves())*3.25)/100);
			Console.log("#CPL ESIC Value "+plEsic);
			object.setcPlEsic(cplEsic);
		}else{
			object.setcPlEsic(0);
			cplEsic=0;
		}
		
		/**Date 24-6-2019 
		 * @author Amol
		 * adding gratuity formula
		 */
		double gratuity=0;
//		if(object.isGratuityApplicable()){
//		gratuity
//			=((((object.getBasic())+Double.parseDouble(object.getDa()))*4.81)/(100));
//			object.setGratuity((Math.round(gratuity)+""));
//		  }else{
//			  object.setGratuity(0+"");
//		 }
		object.setGratuity(object.getGratuity());
		object.setSupervision(object.getSupervision());
		object.setTrainingCharges(object.getTrainingCharges());
		
		double statutoryPayments = Double
				.parseDouble(object.getSpBonus())
				+ Double.parseDouble(object.getSpEDLI())
				+ Double.parseDouble(object.getSpESIC())
				+ Double.parseDouble(object.getSpMWF())
				+ Double.parseDouble(object.getSpPF())
				+ Double.parseDouble(object.getPaidLeaves())
		        +Double.parseDouble(object.getGratuity())
		        +(object.getAdministrativecost())
//		        +(object.getUniformCost())
		        +(object.getSupervision())
		        +(object.getTrainingCharges())+(object.getcPlEsic())
	            +(object.getcBonusEsic());
		
		
		object.setSatutoryPayments((Math.round(statutoryPayments)) + "");
		object.setTotal((Math.round((object.getGrossSalary()
				+ Double.parseDouble(object
						.getSatutoryPayments())))) + "");
//		double paidLeavesValue = 0;
////		Console.log("form.cb_Relever.getValue()"
////				+ object.isRelever() );
//		/**
//		 * Rahul Verma added paid days. And changed(21/12) to (paiddays/12) where paiddays is provided by User
//		 */
//		double paiddays=Double.parseDouble(object.getPaidLeaveDays().trim());
//		
//		if (object.isPaidLeaveRelever()) {
//			paidLeavesValue = ((object.getGrossSalary() + ((Double.parseDouble(object.getSatutoryPayments()))-Double.parseDouble(object.getSpBonus()))) * ((paiddays/12)/30))
//													+
//					(((object.getGrossSalary() + ((Double.parseDouble(object.getSatutoryPayments()))-Double.parseDouble(object.getSpBonus()))))*((paiddays/12)/30))*((paiddays/12)/ 30);
//		} else {
//			paidLeavesValue = (object.getGrossSalary() + ((Double.parseDouble(object.getSatutoryPayments()))-Double.parseDouble(object.getSpBonus()))) * ((paiddays/12)/30);
//		}
//		Console.log("paidLeavesValue" + paidLeavesValue);

//		object.setPaidLeaves((Math.round(paidLeavesValue*100.0)/100.0) + "");
		object.setEmployementOverHeadCost(object.getOverHeadCost());
		object.setTravellingAllowanceOverhead(object.getTravellingAllowanceOverhead());
		object.setUniformCost(object.getUniformCost());
		double totalPaidAndOverheadValue = Double
				.parseDouble(object.getTotal())
				+ object.getAdditionalAllowance()
				+ Double.parseDouble(object
						.getEmployementOverHeadCost())
				+ object.getTravellingAllowanceOverhead()+object.getUniformCost();
		Console.log("totalPaidAndOverheadValue"
				+ totalPaidAndOverheadValue);
		object.setTotalOfPaidAndOverhead((Math.round(totalPaidAndOverheadValue))
				+ "");
		object.setTotalPaidIntoEmployee(Math.round((totalPaidAndOverheadValue*object.getNoOfStaff())));
		double feePercent = 0;
		double managementFeesValue = 0;
		try {
//			if (object.getManageMentfeesPercent()!=0) {
//				feePercent = object.getManageMentfeesPercent();
//			} 
			if (object.getManageMentfeesPercent()!=0) {
				feePercent = object.getManageMentfeesPercent();
				//komal
				managementFeesValue = object.getTotalPaidIntoEmployee()*( feePercent / 100);
				Console.log("managementFeesValue"
						+ managementFeesValue);
				object.setManagementFees((Math.round(managementFeesValue*100.0)/100.0) + "");
			} else{
				if(object.getManagementFees()!=null && !object.getManagementFees().equals("")){
					managementFeesValue = Double.parseDouble(object.getManagementFees());
				}
				/**
				 * @author Anil
				 * @since 03-09-2020
				 */
				if(managementFeesAsPerStaff&&object.getNoOfStaff()!=0&&object.getManagementFeesRate()!=0){
					managementFeesValue=object.getNoOfStaff()*object.getManagementFeesRate();
				}
			}
			Console.log("feePercent" + feePercent);
		} catch (Exception e) {
			e.printStackTrace();
		}

//		double managementFeesValue = object.getTotalPaidIntoEmployee()*( feePercent / 100);
//		Console.log("managementFeesValue"
//				+ managementFeesValue);
		object.setManagementFees(managementFeesValue + "");

		double managementTotal = managementFeesValue+object.getTotalPaidIntoEmployee();
		Console.log("managementTotal" + managementTotal);
		object.setTotalIncludingManagementFees((Math.round(managementTotal))
				+ "");
		
		double grandTotalValue = managementTotal;
		object.setGrandTotalOfCTC((Math.round(grandTotalValue*100.0)/100.0) + "");
		Console.log("grandTotalValue" + grandTotalValue);
		RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
		table.redrawRow(count);
	}

	public void updateForBonusRelever(StaffingDetails object, int index) {
		double bonusValue = 0;
		/**
		 * Rahul Verma added paid days. And changed(21/12) to (paiddays/12)
		 * where paiddays is provided by User
		 */
		double paiddaysforBonus = Double.parseDouble(object.getPaidLeaveDays().trim());
		double cBonusPercent = object.getcBonusPercentage();
		if (object.isBonusRelever()) {
			bonusValue = ((object.getBasic() + Double.parseDouble(object.getDa())) * (cBonusPercent / 100))
					+ (((object.getBasic() + Double.parseDouble(object.getDa())) * (cBonusPercent / 100)) * ((paiddaysforBonus / 12) / 30));
		} else {
			bonusValue = ((object.getBasic() + Double.parseDouble(object
					.getDa())) * (cBonusPercent / 100));// +(1.75/30);
		}
		Console.log("bonusValue" + bonusValue);
		object.setSpBonus((Math.round(bonusValue * 100.0) / 100.0) + "");
		
		/**
		 * @author Anil
		 * @since 19-09-2020
		 * Calculating CBonus based on Bonus (% and reliever)
		 */
		double cbonusEsic=0;
		if(object.getSpBonus()!=null){
			cbonusEsic=((Double.parseDouble(object.getSpBonus())*3.25)/100);	
			object.setcBonusEsic(cbonusEsic);
		}else{
			object.setcBonusEsic(0);
			cbonusEsic=0;	
		}
		
		/**
		 * date 17.11.2018 added by komal for paid leave calculation as
		 * discussed with maradona
		 **/
		double paidLeavesValue = 0;
		// Console.log("form.cb_Relever.getValue()"
		// + object.isRelever() );
		/**
		 * Rahul Verma added paid days. And changed(21/12) to (paiddays/12)
		 * where paiddays is provided by User
		 */
		double paiddays = Double.parseDouble(object.getPaidLeaveDays().trim());
		double totalValue = Double.parseDouble(object.getSpEDLI())
				+ Double.parseDouble(object.getSpESIC())
				+ Double.parseDouble(object.getSpMWF())
				+ Double.parseDouble(object.getSpPF());
		totalValue = Math.round(totalValue * 100.0) / 100.0;
		if (object.isPaidLeaveRelever()) {
			paidLeavesValue = ((object.getGrossSalary() + (totalValue)) * ((paiddays / 12) / 30))
					+ (((object.getGrossSalary() + (totalValue))) * ((paiddays / 12) / 30))
					* ((paiddays / 12) / 30);
		} else {
			paidLeavesValue = (object.getGrossSalary() + (totalValue))
					* ((paiddays / 12) / 30);
		}
		Console.log("paidLeavesValue" + paidLeavesValue);

		object.setPaidLeaves((Math.round(paidLeavesValue * 100.0) / 100.0) + "");

		/**
		 * end komal
		 */

		/**
		 * @author Anil
		 * @since 04-09-2020
		 */
		CTCTemplate ctcTemplate = ctcTemplateMap.get(object.getCtcTemplates());
		if (paidleave != null) {
			double plAmt = 0;
			if (paidleave != null) {
				for (OtEarningComponent plComp : paidleave.getOtEarningCompList()) {
					System.out.println("NAME : " + plComp.getComponentName());
					for (CtcComponent earningComp : ctcTemplate.getEarning()) {
						System.out.println("EARN NAME : "+ earningComp.getName());
						if (plComp.getComponentName().equalsIgnoreCase("Gross Earning")) {
							plAmt = plAmt + earningComp.getAmount() / 12;
						} else if (plComp.getComponentName().equals(earningComp.getName())) {
							System.out.println("AMT : "+ earningComp.getAmount() / 12);
							plAmt = plAmt + earningComp.getAmount() / 12;
						}
					}
				}
			}
			System.out.println("BASE AMT : " + plAmt);
			plAmt = plAmt * paidleave.getRate() / 100;
			System.out.println("PL AMT : " + plAmt);
			object.setPaidLeaves((Math.round(plAmt * 100.0) / 100.0) + "");
		}
		
		/**
		 * @author Anil
		 * @since 19-09-2020
		 */
		double cplEsic=0;
		if(object.getPaidLeaves()!=null){
			cplEsic=((Double.parseDouble(object.getPaidLeaves())*3.25)/100);
			Console.log("#CPL ESIC Value "+plEsic);
			object.setcPlEsic(cplEsic);
		}else{
			object.setcPlEsic(0);
			cplEsic=0;
		}

		/**
		 * Date 24-6-2019
		 * 
		 * @author Amol adding gratuity formula
		 */
		double gratuity = 0;
		// if(object.isGratuityApplicable()){
		// gratuity
		// =((((object.getBasic())+Double.parseDouble(object.getDa()))*4.81)/(100));
		// object.setGratuity((Math.round(gratuity)+""));
		// }else{
		// object.setGratuity(0+"");
		// }
		object.setGratuity(object.getGratuity());
		object.setSupervision(object.getSupervision());
		object.setTrainingCharges(object.getTrainingCharges());
		double statutoryPayments = Double.parseDouble(object.getSpBonus())
				+ Double.parseDouble(object.getSpEDLI())
				+ Double.parseDouble(object.getSpESIC())
				+ Double.parseDouble(object.getSpMWF())
				+ Double.parseDouble(object.getSpPF())
				+ Double.parseDouble(object.getPaidLeaves())
				+ Double.parseDouble(object.getGratuity())
				+ (object.getAdministrativecost())
				// +(object.getUniformCost())
				+ (object.getSupervision()) + (object.getTrainingCharges())+(object.getcPlEsic())
			            +(object.getcBonusEsic());

		object.setSatutoryPayments((Math.round(statutoryPayments)) + "");
		object.setTotal((Math.round((object.getGrossSalary() + Double
				.parseDouble(object.getSatutoryPayments())))) + "");
		// double paidLeavesValue = 0;
		// Console.log("form.cb_Relever.getValue()"
		// + object.isBonusRelever() );
		// /**
		// * Rahul Verma added paid days. And changed(21/12) to (paiddays/12)
		// where paiddays is provided by User
		// */
		// double paiddays=Double.parseDouble(object.getPaidLeaveDays().trim());
		//
		// if (object.isPaidLeaveRelever()) {
		// paidLeavesValue = ((object.getGrossSalary() +
		// ((Double.parseDouble(object.getSatutoryPayments()))-Double.parseDouble(object.getSpBonus())))
		// * ((paiddays/12)/30))
		// +
		// (((object.getGrossSalary() +
		// ((Double.parseDouble(object.getSatutoryPayments()))-Double.parseDouble(object.getSpBonus()))))*((paiddays/12)/30))*((paiddays/12)/
		// 30);
		// } else {
		// paidLeavesValue = (object.getGrossSalary() +
		// ((Double.parseDouble(object.getSatutoryPayments()))-Double.parseDouble(object.getSpBonus())))
		// * ((paiddays/12)/30);
		// }
		// Console.log("paidLeavesValue" + paidLeavesValue);
		//
		// object.setPaidLeaves((Math.round(paidLeavesValue*100.0)/100.0) + "");
		object.setEmployementOverHeadCost(object.getOverHeadCost());
		object.setTravellingAllowanceOverhead(object
				.getTravellingAllowanceOverhead());
		object.setUniformCost(object.getUniformCost());
		double totalPaidAndOverheadValue = Double
				.parseDouble(object.getTotal())
				+ object.getAdditionalAllowance()
				+ Double.parseDouble(object.getEmployementOverHeadCost())
				+ object.getTravellingAllowanceOverhead()
				+ object.getUniformCost();
		Console.log("totalPaidAndOverheadValue" + totalPaidAndOverheadValue);
		object.setTotalOfPaidAndOverhead((Math.round(totalPaidAndOverheadValue))
				+ "");
		object.setTotalPaidIntoEmployee(Math
				.round((totalPaidAndOverheadValue * object.getNoOfStaff())));
		double feePercent = 0;
		double managementFeesValue = 0;
		try {
			// if (object.getManageMentfeesPercent()!=0) {
			// feePercent = object.getManageMentfeesPercent();
			// }
			if (object.getManageMentfeesPercent() != 0) {
				feePercent = object.getManageMentfeesPercent();
				// komal
				managementFeesValue = object.getTotalPaidIntoEmployee()* (feePercent / 100);
				Console.log("managementFeesValue" + managementFeesValue);
				object.setManagementFees((Math.round(managementFeesValue * 100.0) / 100.0) + "");
			} else {
				if (object.getManagementFees() != null
						&& !object.getManagementFees().equals("")) {
					managementFeesValue = Double.parseDouble(object.getManagementFees());
				}
				/**
				 * @author Anil
				 * @since 03-09-2020
				 */
				if (managementFeesAsPerStaff && object.getNoOfStaff() != 0
						&& object.getManagementFeesRate() != 0) {
					managementFeesValue = object.getNoOfStaff()* object.getManagementFeesRate();
				}
			}
			Console.log("feePercent" + feePercent);
		} catch (Exception e) {
			e.printStackTrace();
		}

		// double managementFeesValue = object.getTotalPaidIntoEmployee()*(
		// feePercent / 100);
		// Console.log("managementFeesValue"
		// + managementFeesValue);
		object.setManagementFees(managementFeesValue + "");

		double managementTotal = managementFeesValue+ object.getTotalPaidIntoEmployee();
		Console.log("managementTotal" + managementTotal);
		object.setTotalIncludingManagementFees((Math.round(managementTotal))+ "");

		double grandTotalValue = managementTotal;
		object.setGrandTotalOfCTC((Math.round(grandTotalValue * 100.0) / 100.0)+ "");
		Console.log("grandTotalValue" + grandTotalValue);
		RowCountChangeEvent.fire(table, getDataprovider().getList().size(),true);
		table.redrawRow(index);
	}
	
	
	public void updateForPaidLeaveRelever(StaffingDetails object, int index) {
		double bonusValue = 0;
		double cBonusPercent = object.getcBonusPercentage();
		/**
		 * Rahul Verma added paid days. And changed(21/12) to (paiddays/12)
		 * where paiddays is provided by User
		 */
		double paiddaysforBonus = Double.parseDouble(object.getPaidLeaveDays().trim());
		if (object.isBonusRelever()) {
			bonusValue = ((object.getBasic() + Double.parseDouble(object.getDa())) * (cBonusPercent / 100))
					+ (((object.getBasic() + Double.parseDouble(object.getDa())) * (cBonusPercent / 100)) * ((paiddaysforBonus / 12) / 30));
		} else {
			bonusValue = ((object.getBasic() + Double.parseDouble(object
					.getDa())) * (cBonusPercent / 100));// +(1.75/30);
		}
		Console.log("bonusValue" + bonusValue);
		object.setSpBonus((Math.round(bonusValue * 100.0) / 100.0) + "");
		
		/**
		 * @author Anil
		 * @since 19-09-2020
		 * Calculating CBonus based on Bonus (% and reliever)
		 */
		double cbonusEsic=0;
		if(object.getSpBonus()!=null){
			cbonusEsic=((Double.parseDouble(object.getSpBonus())*3.25)/100);	
			object.setcBonusEsic(cbonusEsic);
		}else{
			object.setcBonusEsic(0);
			cbonusEsic=0;	
		}
		
		/**
		 * date 17.11.2018 added by komal for paid leave calculation as
		 * discussed with maradona
		 **/
		double paidLeavesValue = 0;
		// Console.log("form.cb_Relever.getValue()"
		// + object.isRelever() );
		/**
		 * Rahul Verma added paid days. And changed(21/12) to (paiddays/12)
		 * where paiddays is provided by User
		 */
		double paiddays = Double.parseDouble(object.getPaidLeaveDays().trim());
		double totalValue = Double.parseDouble(object.getSpEDLI())
				+ Double.parseDouble(object.getSpESIC())
				+ Double.parseDouble(object.getSpMWF())
				+ Double.parseDouble(object.getSpPF());
		totalValue = Math.round(totalValue * 100.0) / 100.0;
		if (object.isPaidLeaveRelever()) {
			paidLeavesValue = ((object.getGrossSalary() + (totalValue)) * ((paiddays / 12) / 30))
					+ (((object.getGrossSalary() + (totalValue))) * ((paiddays / 12) / 30))
					* ((paiddays / 12) / 30);
		} else {
			paidLeavesValue = (object.getGrossSalary() + (totalValue))
					* ((paiddays / 12) / 30);
		}
		Console.log("paidLeavesValue" + paidLeavesValue);

		object.setPaidLeaves((Math.round(paidLeavesValue * 100.0) / 100.0) + "");

		/**
		 * end komal
		 */

		/**
		 * @author Anil
		 * @since 04-09-2020
		 */
		CTCTemplate ctcTemplate = ctcTemplateMap.get(object.getCtcTemplates());
		if (paidleave != null) {
			double plAmt = 0;
			if (paidleave != null) {
				for (OtEarningComponent plComp : paidleave.getOtEarningCompList()) {
					System.out.println("NAME : " + plComp.getComponentName());
					for (CtcComponent earningComp : ctcTemplate.getEarning()) {
						System.out.println("EARN NAME : "+ earningComp.getName());
						if (plComp.getComponentName().equalsIgnoreCase("Gross Earning")) {
							plAmt = plAmt + earningComp.getAmount() / 12;
						} else if (plComp.getComponentName().equals(earningComp.getName())) {
							System.out.println("AMT : "+ earningComp.getAmount() / 12);
							plAmt = plAmt + earningComp.getAmount() / 12;
						}

					}
				}
			}
			System.out.println("BASE AMT : " + plAmt);
			plAmt = plAmt * paidleave.getRate() / 100;
			System.out.println("PL AMT : " + plAmt);
			object.setPaidLeaves((Math.round(plAmt * 100.0) / 100.0) + "");
		}
		
		/**
		 * @author Anil
		 * @since 19-09-2020
		 */
		double cplEsic=0;
		if(object.getPaidLeaves()!=null){
			cplEsic=((Double.parseDouble(object.getPaidLeaves())*3.25)/100);
			Console.log("#CPL ESIC Value "+plEsic);
			object.setcPlEsic(cplEsic);
		}else{
			object.setcPlEsic(0);
			cplEsic=0;
		}

		/**
		 * Date 24-6-2019
		 * 
		 * @author Amol adding gratuity formula
		 */
		double gratuity = 0;
		// if(object.isGratuityApplicable()){
		// gratuity
		// =((((object.getBasic())+Double.parseDouble(object.getDa()))*4.81)/(100));
		// object.setGratuity((Math.round(gratuity)+""));
		// }else{
		// object.setGratuity(0+"");
		// }
		object.setGratuity(object.getGratuity());
		object.setSupervision(object.getSupervision());
		object.setTrainingCharges(object.getTrainingCharges());

		double statutoryPayments = Double.parseDouble(object.getSpBonus())
				+ Double.parseDouble(object.getSpEDLI())
				+ Double.parseDouble(object.getSpESIC())
				+ Double.parseDouble(object.getSpMWF())
				+ Double.parseDouble(object.getSpPF())
				+ Double.parseDouble(object.getPaidLeaves())
				+ Double.parseDouble(object.getGratuity())
				+ (object.getAdministrativecost())
				// +(object.getUniformCost())
				+ (object.getSupervision()) + (object.getTrainingCharges())+(object.getcPlEsic())
	            +(object.getcBonusEsic());

		object.setSatutoryPayments((Math.round(statutoryPayments)) + "");
		object.setTotal((Math.round((object.getGrossSalary() + Double
				.parseDouble(object.getSatutoryPayments())))) + "");
		// double paidLeavesValue = 0;
		// // Console.log("form.cb_Relever.getValue()"
		// // + object.isRelever() );
		// /**
		// * Rahul Verma added paid days. And changed(21/12) to (paiddays/12)
		// where paiddays is provided by User
		// */
		// double paiddays=Double.parseDouble(object.getPaidLeaveDays().trim());
		//
		// if (object.isPaidLeaveRelever()) {
		// paidLeavesValue = ((object.getGrossSalary() +
		// ((Double.parseDouble(object.getSatutoryPayments()))-Double.parseDouble(object.getSpBonus())))
		// * ((paiddays/12)/30))
		// +
		// (((object.getGrossSalary() +
		// ((Double.parseDouble(object.getSatutoryPayments()))-Double.parseDouble(object.getSpBonus()))))*((paiddays/12)/30))*((paiddays/12)/
		// 30);
		// } else {
		// paidLeavesValue = (object.getGrossSalary() +
		// ((Double.parseDouble(object.getSatutoryPayments()))-Double.parseDouble(object.getSpBonus())))
		// * ((paiddays/12)/30);
		// }
		// Console.log("paidLeavesValue" + paidLeavesValue);
		//
		// object.setPaidLeaves((Math.round(paidLeavesValue*100.0)/100.0) + "");
		object.setEmployementOverHeadCost(object.getOverHeadCost());
		object.setTravellingAllowanceOverhead(object
				.getTravellingAllowanceOverhead());
		object.setUniformCost(object.getUniformCost());
		double totalPaidAndOverheadValue = Double
				.parseDouble(object.getTotal())
				+ object.getAdditionalAllowance()
				+ Double.parseDouble(object.getEmployementOverHeadCost())
				+ object.getTravellingAllowanceOverhead()
				+ object.getUniformCost();
		Console.log("totalPaidAndOverheadValue" + totalPaidAndOverheadValue);
		object.setTotalOfPaidAndOverhead((Math.round(totalPaidAndOverheadValue))
				+ "");
		object.setTotalPaidIntoEmployee(Math
				.round((totalPaidAndOverheadValue * object.getNoOfStaff())));
		double feePercent = 0;
		double managementFeesValue = 0;
		try {
			// if (object.getManageMentfeesPercent()!=0) {
			// feePercent = object.getManageMentfeesPercent();
			// }
			if (object.getManageMentfeesPercent() != 0) {
				feePercent = object.getManageMentfeesPercent();
				// komal
				managementFeesValue = object.getTotalPaidIntoEmployee()
						* (feePercent / 100);
				Console.log("managementFeesValue" + managementFeesValue);
				object.setManagementFees((Math
						.round(managementFeesValue * 100.0) / 100.0) + "");
			} else {
				if (object.getManagementFees() != null
						&& !object.getManagementFees().equals("")) {
					managementFeesValue = Double.parseDouble(object
							.getManagementFees());
				}
			}
			Console.log("feePercent" + feePercent);
		} catch (Exception e) {
			e.printStackTrace();
		}

		// double managementFeesValue = object.getTotalPaidIntoEmployee()*(
		// feePercent / 100);
		// Console.log("managementFeesValue"
		// + managementFeesValue);
		object.setManagementFees(managementFeesValue + "");

		double managementTotal = managementFeesValue
				+ object.getTotalPaidIntoEmployee();
		Console.log("managementTotal" + managementTotal);
		object.setTotalIncludingManagementFees((Math.round(managementTotal))
				+ "");

		double grandTotalValue = managementTotal;
		object.setGrandTotalOfCTC((Math.round(grandTotalValue * 100.0) / 100.0)+ "");
		Console.log("grandTotalValue" + grandTotalValue);
		RowCountChangeEvent.fire(table, getDataprovider().getList().size(),true);
		table.redrawRow(index);
	}
	
public void updateSPBonus(StaffingDetails object , int index){
	/**
	 * @author Anil
	 * @since 19-09-2020
	 * No need to recalculate Paid leave on update of bonus
	 * thats why commenting below code which was used to calculate paid leave
	 * also adding code for CPL ESIC and C Bonus ESIC
	 */
	
//	
//	//object.setSpBonus(Math.round(Double.parseDouble(value))+"");
////	/** date 17.11.2018 added by komal for paid leave calculation as discussed with maradona**/
//	double paidLeavesValue = 0;
////	Console.log("form.cb_Relever.getValue()"
////			+ object.isRelever() );
//	/**
//	 * Rahul Verma added paid days. And changed(21/12) to (paiddays/12) where paiddays is provided by User
//	 */
//	double paiddays=Double.parseDouble(object.getPaidLeaveDays().trim());
//	double totalValue = Double.parseDouble(object.getSpEDLI())
//			+ Double.parseDouble(object.getSpESIC())
//			+ Double.parseDouble(object.getSpMWF())
//			+ Double.parseDouble(object.getSpPF());
//	totalValue = Math.round(totalValue*100.0)/100.0;
//	if (object.isPaidLeaveRelever()) {
//		paidLeavesValue = ((object.getGrossSalary() + (totalValue)) * ((paiddays/12)/30))+
//				(((object.getGrossSalary() + (totalValue)))*((paiddays/12)/30))*((paiddays/12)/30);
//	} else {
//		paidLeavesValue = (object.getGrossSalary() + (totalValue)) * ((paiddays/12)/30);
//	}
//	Console.log("paidLeavesValue" + paidLeavesValue);
//	object.setPaidLeaves((Math.round(paidLeavesValue*100.0)/100.0) + "");
//	
//	/**
//	 *  end komal
//	 */
//	
//
//	/**
//	 * @author Anil
//	 * @since 04-09-2020
//	 */
//	CTCTemplate ctcTemplate=ctcTemplateMap.get(object.getCtcTemplates());
//	if(paidleave!=null){
//		double plAmt=0;
//		if(paidleave!=null){
//			for(OtEarningComponent plComp:paidleave.getOtEarningCompList()){
//				System.out.println("NAME : "+plComp.getComponentName());
//				for(CtcComponent earningComp:ctcTemplate.getEarning()){
//					System.out.println("EARN NAME : "+earningComp.getName());
//					if(plComp.getComponentName().equalsIgnoreCase("Gross Earning")){
//						plAmt=plAmt+earningComp.getAmount()/12;
//					}else if(plComp.getComponentName().equals(earningComp.getName())){
//						System.out.println("AMT : "+earningComp.getAmount()/12);
//						plAmt=plAmt+earningComp.getAmount()/12;
//					}
//					
//				}
//			}
//		}
//		System.out.println("BASE AMT : "+plAmt);
//		plAmt=plAmt*paidleave.getRate()/100;
//		System.out.println("PL AMT : "+plAmt);
//		object.setPaidLeaves((Math.round(plAmt*100.0)/100.0)+"");
//	}
	
	/**
	 * @author Anil
	 * @since 19-09-2020
	 * Calculating CBonus based on Bonus (% and reliever)
	 */
	double cbonusEsic=0;
	if(object.getSpBonus()!=null){
		cbonusEsic=((Double.parseDouble(object.getSpBonus())*3.25)/100);	
		object.setcBonusEsic(cbonusEsic);
	}else{
		object.setcBonusEsic(0);
		cbonusEsic=0;	
	}
	
	/**Date 24-6-2019 
	 * @author Amol
	 * adding gratuity formula
	 */
	double gratuity=0;
//	if(object.isGratuityApplicable()){
//	gratuity
//		=((((object.getBasic())+Double.parseDouble(object.getDa()))*4.81)/(100));
//		object.setGratuity((Math.round(gratuity)+""));
//	  }else{
//		  object.setGratuity(0+"");
//	 }
	object.setGratuity(object.getGratuity());
	object.setSupervision(object.getSupervision());
	object.setTrainingCharges(object.getTrainingCharges());
	
			double statutoryPayments = Double
					.parseDouble(object.getSpBonus())
					+ Double.parseDouble(object.getSpEDLI())
					+ Double.parseDouble(object.getSpESIC())
					+ Double.parseDouble(object.getSpMWF())
					+ Double.parseDouble(object.getSpPF())
					+ Double.parseDouble(object.getPaidLeaves())
			        +Double.parseDouble(object.getGratuity())
	                +(object.getAdministrativecost())
//	                +(object.getUniformCost())
	                +(object.getSupervision())
	                +(object.getTrainingCharges())+(object.getcPlEsic())
	                +(object.getcBonusEsic());
	
			object.setSatutoryPayments((Math.round(statutoryPayments)) + "");
			object.setTotal((Math.round((object.getGrossSalary()
					+ Double.parseDouble(object
							.getSatutoryPayments())))) + "");
//			double paidLeavesValue = 0;
//			Console.log("form.cb_Relever.getValue()"
//					+ object.isRelever() );
			
			object.setTravellingAllowance(Math.round(object.getTravellingAllowance()*100.0)/100.0);
			//Take Home Salary
			double takeHomeValue = object.getNetSalary()
					+ Double.parseDouble(object.getcLeave())
					+ Double.parseDouble(object.getcBonus())
					+ object.getTravellingAllowance();
			Console.log("takeHomeValue" + takeHomeValue);
			object.setTakeHome((Math.round(takeHomeValue)) + "");

			/**
			 * Rahul Verma added paid days. And changed(21/12) to (paiddays/12) where paiddays is provided by User
			 */
//			double paiddays=Double.parseDouble(object.getPaidLeaveDays().trim());
//			
//			if (object.isPaidLeaveRelever()) {
//				paidLeavesValue = ((object.getGrossSalary() + ((Double.parseDouble(object.getSatutoryPayments()))-Double.parseDouble(object.getSpBonus()))) * ((paiddays/12)/30))
//														+
//						(((object.getGrossSalary() + ((Double.parseDouble(object.getSatutoryPayments()))-Double.parseDouble(object.getSpBonus()))))*((paiddays/12)/30))*((paiddays/12)/ 30);
//			} else {
//				paidLeavesValue = (object.getGrossSalary() + ((Double.parseDouble(object.getSatutoryPayments()))-Double.parseDouble(object.getSpBonus()))) * ((paiddays/12)/30);
//			}
//			Console.log("paidLeavesValue" + paidLeavesValue);
//
//			object.setPaidLeaves((Math.round(paidLeavesValue*100.0)/100.0) + "");
			object.setEmployementOverHeadCost(object.getOverHeadCost());
			object.setTravellingAllowanceOverhead(object.getTravellingAllowanceOverhead());
			object.setUniformCost(object.getUniformCost());
			double totalPaidAndOverheadValue = Double
					.parseDouble(object.getTotal())
					+ object.getAdditionalAllowance()
					+ Double.parseDouble(object
							.getEmployementOverHeadCost())
					+ object.getTravellingAllowanceOverhead()+object.getUniformCost();
			Console.log("totalPaidAndOverheadValue"
					+ totalPaidAndOverheadValue);
			object.setTotalOfPaidAndOverhead((Math.round(totalPaidAndOverheadValue))
					+ "");
			object.setTotalPaidIntoEmployee(Math.round((totalPaidAndOverheadValue*object.getNoOfStaff())));
			double feePercent = 0;
			double managementFeesValue =0;
			try {
//				if (object.getManageMentfeesPercent()!=0) {
//					feePercent = object.getManageMentfeesPercent();
//				} 
				if (object.getManageMentfeesPercent()!=0) {
					feePercent = object.getManageMentfeesPercent();
					managementFeesValue = object.getTotalPaidIntoEmployee()*( feePercent / 100);
					Console.log("managementFeesValue"
							+ managementFeesValue);
				//	object.setManagementFees(managementFeesValue + "");

				} else{
					if(object.getManagementFees()!=null && !object.getManagementFees().equals("")){
						managementFeesValue = Double.parseDouble(object.getManagementFees());
					}
					/**
					 * @author Anil
					 * @since 03-09-2020
					 */
					if(managementFeesAsPerStaff&&object.getNoOfStaff()!=0&&object.getManagementFeesRate()!=0){
						managementFeesValue=object.getNoOfStaff()*object.getManagementFeesRate();
					}
				}

				Console.log("feePercent" + feePercent);
			} catch (Exception e) {
				e.printStackTrace();
			}

//			double managementFeesValue = object.getTotalPaidIntoEmployee()*( feePercent / 100);
//			Console.log("managementFeesValue"
//					+ managementFeesValue);
			object.setManagementFees(managementFeesValue + "");

			double managementTotal = managementFeesValue+object.getTotalPaidIntoEmployee();
			Console.log("managementTotal" + managementTotal);
			object.setTotalIncludingManagementFees((Math.round(managementTotal))
					+ "");
			
			double grandTotalValue = managementTotal;
			object.setGrandTotalOfCTC((Math.round(grandTotalValue*100.0)/100.0) + "");
			Console.log("grandTotalValue" + grandTotalValue);
			RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
			table.redrawRow(index);
		
}
private void viewAdditionalAllowance() {
	// TODO Auto-generated method stub
	viewAdditionalAllowance = new TextColumn<StaffingDetails>() {

		@Override
		public String getValue(StaffingDetails object) {
			// TODO Auto-generated method stub
			return object.getAdditionalAllowance()+ "";
		}
	};

	table.addColumn(viewAdditionalAllowance, "Additional Allowance");
	table.setColumnWidth(viewAdditionalAllowance, 120, Unit.PX);
}
protected void addAdditionalAllowance() {
	// TODO Auto-generated method stub
	EditTextCell editCell = new EditTextCell();
	additionalAllowance=new Column<StaffingDetails, String>(editCell) {

		@Override
		public String getValue(StaffingDetails object) {
			// TODO Auto-generated method stub
			return object.getAdditionalAllowance()+"";
		}
	};
	table.addColumn(additionalAllowance, "#Additional Allowance");
	table.setColumnWidth(additionalAllowance, 120, Unit.PX);
	additionalAllowance.setFieldUpdater(new FieldUpdater<StaffingDetails, String>() {

		@Override
		public void update(int index, StaffingDetails object, String value) {
			// TODO Auto-generated method stub
			try{
				object.setAdditionalAllowance(Double.parseDouble(value));
			}catch(Exception e){
				e.printStackTrace();
				object.setAdditionalAllowance(0);
			}
			object.setUniformCost(object.getUniformCost());
//			updateRow(object, index);
//			object.setTravellingAllowanceOverhead(object.getTravellingAllowanceOverhead());
			double totalPaidAndOverheadValue = Double
					.parseDouble(object.getTotal())
					+ object.getAdditionalAllowance()
					+ Double.parseDouble(object
							.getEmployementOverHeadCost())
					+ object.getTravellingAllowanceOverhead()+object.getUniformCost();
			Console.log("totalPaidAndOverheadValue"
					+ totalPaidAndOverheadValue);
			object.setTotalOfPaidAndOverhead((Math.round(totalPaidAndOverheadValue))
					+ "");
			object.setTotalPaidIntoEmployee(Math.round((totalPaidAndOverheadValue*object.getNoOfStaff())));
			double feePercent = 0;
			double managementFeesValue =0;
			try {
//				if (object.getManageMentfeesPercent()!=0) {
//					feePercent = object.getManageMentfeesPercent();
//				} 
				if (object.getManageMentfeesPercent()!=0) {
					feePercent = object.getManageMentfeesPercent();
					//komal
					managementFeesValue = object.getTotalPaidIntoEmployee()*( feePercent / 100);
					Console.log("managementFeesValue"
							+ managementFeesValue);
					//object.setManagementFees(Math.round(managementFeesValue) + "");
				} else{
					if(object.getManagementFees()!=null && !object.getManagementFees().equals("")){
						managementFeesValue = Double.parseDouble(object.getManagementFees());
					}
					/**
					 * @author Anil
					 * @since 03-09-2020
					 */
					if(managementFeesAsPerStaff&&object.getNoOfStaff()!=0&&object.getManagementFeesRate()!=0){
						managementFeesValue=object.getNoOfStaff()*object.getManagementFeesRate();
					}
				}
				Console.log("feePercent" + feePercent);
			} catch (Exception e) {
				e.printStackTrace();
			}

//			double managementFeesValue = object.getTotalPaidIntoEmployee()*( feePercent / 100);
//			Console.log("managementFeesValue"
//					+ managementFeesValue);
	object.setManagementFees(managementFeesValue + "");

			double managementTotal = managementFeesValue+object.getTotalPaidIntoEmployee();
			Console.log("managementTotal" + managementTotal);
			object.setTotalIncludingManagementFees((Math.round(managementTotal))
					+ "");
			
			double grandTotalValue = managementTotal;
			object.setGrandTotalOfCTC((Math.round(grandTotalValue*100.0)/100.0) + "");
			Console.log("grandTotalValue" + grandTotalValue);
			RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
			table.redrawRow(index);
		}
	});
}

/** date 24.1.2019 added by komal to filter ctc templates **/
private void editCTCAmount() {
	
	// TODO Auto-generated method stub
	EditTextCell editCell = new EditTextCell();
	editCTCAmount = new Column<StaffingDetails, String>(editCell) {
		@Override
		public String getValue(StaffingDetails object) {
			
			return "";
		}
	};
	table.addColumn(editCTCAmount, "#Gross Salary Filter");
	table.setColumnWidth(editCTCAmount, 120, Unit.PX);

	editCTCAmount.setFieldUpdater(new FieldUpdater<StaffingDetails, String>() {

		@Override
		public void update(int index, StaffingDetails object, String value) {
			// TODO Auto-generated method stub
		//	List<CTCTemplate> list = new ArrayList<CTCTemplate>();
			try{
			double amount1  = Double.parseDouble(value);
			double totalCTC = amount1 * 12;
			amount = totalCTC;
			 for (Map.Entry<String, CTCTemplate> entry : ctcTemplateMap.entrySet()) { 
		            System.out.println("Key = " + entry.getKey() + 
		                             ", Value = " + entry.getValue()); 
			 CTCTemplate ctcTemplate = entry.getValue();
			 if(ctcTemplate.getTotalCtcAmount() == amount1){
				 //list.add(ctcTemplate);
				
			 }else{
				 ctcList.remove(ctcTemplate.getCtcTemplateName());
			 }
		    } 
		//	ctcList.clear();
		//	ctcList.addAll(list);
			//RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
			setEnable(true);
				table.redrawRow(index);
			}catch(Exception e){
				GWTCAlert alert = new GWTCAlert();
				alert.alert("Please enter numeric value.");
				table.redrawRow(index);
			}
			
		}
	});
}
public void addFieldUpdaterOFCTC(){
	ctcTemplates
	.setFieldUpdater(new FieldUpdater<StaffingDetails, String>() {

		@Override
		public void update(int index,
				StaffingDetails object, String value) {

			if (value
					.equalsIgnoreCase("--SELECT--")) {
				object.setCtcTemplates("");
			} else {
				object.setCtcTemplates(value);
			}
			UpdateCTCTemplate(index , object);

		}
	});
}
private void editCTCTemplate(){
	SelectionCell ctcListSelection = new SelectionCell(
			ctcList);
	ctcTemplates = new Column<StaffingDetails, String>(
			ctcListSelection) {
		@Override
		public String getValue(StaffingDetails object) {
			// TODO Auto-generated method stub
			if (object.getCtcTemplates() != null)
				return object.getCtcTemplates();
			else
				return "";

		}
	};
	table.addColumn(ctcTemplates, "#CTC Templates");
	table.setColumnWidth(ctcTemplates, 100, Unit.PX);
}








public void UpdateCTCTemplate(int index , StaffingDetails object){
	hideColumn=true;
	CTCTemplate ctcTemplate=ctcTemplateMap.get(object.getCtcTemplates());
	
	/**
	 * @author Anil
	 * @since 07-07-2020
	 */
	if(ctcTemplate!=null){
		double esicPercent=0;
//		for(CtcComponent dedComp:ctcTemplate.getDeduction()){
//			if(dedComp.getShortName().equalsIgnoreCase("ESIC")){
//				esicPercent=esicPercent+dedComp.getMaxPerOfCTC();
//			}
//		}
		
		/**
		 * @author Anil @since 15-03-2021
		 * if company contribution is null in ctc template then it is throwing and error
		 */
		if(ctcTemplate.getCompanyContribution()!=null){
			for(CtcComponent compComp:ctcTemplate.getCompanyContribution()){
				if(compComp.getShortName().equalsIgnoreCase("ESIC")){
					esicPercent=esicPercent+compComp.getMaxPerOfCTC();
					break;
				}
			}
		}
		object.setEsicPercent(esicPercent);
	}
	
	object.setGrossSalary(Math.round((ctcTemplate.getGrossEarning()/12)));
	double basicValue=0,daValue=0,hraValue=0,medicalValue=0,washingAllowanceValue=0,
	conveyenceValue=0,bandPValue=0,otherValue=0,cityCompensatoryValue=0;
	
	
	/**
	 * Date 17-08-2020 by Vijay
	 * Des :- Updated Code as Per the EPF CPF and CEDLI calculation using prodvident fund Master
	 */
	double sumOfMatchingComponents=0;
	
	for (CtcComponent ctcComponent : ctcTemplate.getEarning()) {
		sumOfMatchingComponents += getTotalOfComponents(ctcComponent);
		
		if(ctcComponent.getShortName().equalsIgnoreCase("Basic")||ctcComponent.getName().equalsIgnoreCase("Basic")){
			basicValue=ctcComponent.getAmount()/12;
		}
		else if(ctcComponent.getShortName().equalsIgnoreCase("DA")||ctcComponent.getName().equalsIgnoreCase("DA")){
			daValue=ctcComponent.getAmount()/12;
		}
		
		else if(ctcComponent.getShortName().equalsIgnoreCase("HRA")||ctcComponent.getName().equalsIgnoreCase("HRA")){
			hraValue=ctcComponent.getAmount()/12;
		}
		
		else if(ctcComponent.getShortName().equalsIgnoreCase("MED")||ctcComponent.getName().equalsIgnoreCase("MEDICAL ALLOWANCE")){
			medicalValue=ctcComponent.getAmount()/12;
		}
		
		/****Date 22-2-2019  added CCA option*****/
		else if(ctcComponent.getShortName().equalsIgnoreCase("CCA")||ctcComponent.getName().equalsIgnoreCase("CITY COMPENSATORY ALLOWANCE ")){
			cityCompensatoryValue=ctcComponent.getAmount()/12;
		}
		
		
		
		else if(ctcComponent.getShortName().equalsIgnoreCase("WASHING ALLOWANCE")||ctcComponent.getName().equalsIgnoreCase("WASHING ALLOWANCE")){
			washingAllowanceValue=ctcComponent.getAmount()/12;
		}
		
		else if(ctcComponent.getShortName().equalsIgnoreCase("CONV")||ctcComponent.getName().equalsIgnoreCase("Conveyance")){
			conveyenceValue=ctcComponent.getAmount()/12;
		}
		else if(ctcComponent.getShortName().equalsIgnoreCase("BandP")||ctcComponent.getName().equalsIgnoreCase("BandP")){
			bandPValue=ctcComponent.getAmount()/12;
		}
		else if(ctcComponent.getShortName().equalsIgnoreCase("Others")||ctcComponent.getName().equalsIgnoreCase("Others")||ctcComponent.getName().equalsIgnoreCase("Other")||ctcComponent.getShortName().equalsIgnoreCase("Other")||ctcComponent.getShortName().equalsIgnoreCase("OA")){
			otherValue+=ctcComponent.getAmount()/12;
		}
		else {
			/*
			 * Ashwini Patil
			 * 12-.2-2024
			 * We allow users to create as many CTC components as they want but on CNC staffing details table, we have limited CTC columns.
			 * for example Special allowance column is not there.
			 * So as per advance fm requirement, all extra component's value need to be added to other column
			 */
			otherValue+=ctcComponent.getAmount()/12; 
		}
		
//		
		
		
		
		
		
	}
	object.setBasic(basicValue);
	object.setDa(daValue+"");
	object.setHra(hraValue+"");
	object.setMedical(medicalValue+"");
	/*** date 25-2-2019 added by Amol**/
	object.setCityCompensatoryAllowance(cityCompensatoryValue);
	object.setWashingAllowance(washingAllowanceValue);
	object.setConveyance(conveyenceValue+"");
	object.setBandP(bandPValue+"");
	object.setOthers(otherValue+"");
	object.setBandP(object.getBandP());
	
	if(hideColumn){
	if(ctcTemplate!=null&&ctcTemplate.getPaidLeaveAmt()!=0){
		object.setLeaveSalary(ctcTemplate.getPaidLeaveAmt());
//	   if(ctcTemplate.isPlIncludedInEarning()){
	  // ctcTemplate.setGrossEarning(ctcTemplate.getGrossEarning()+ctcTemplate.getPaidLeaveAmt());
//	     object.setGrossSalary(Math.round((ctcTemplate.getGrossEarning()/12))+ctcTemplate.getPaidLeaveAmt());
	     object.setGrossSalary(Math.round((ctcTemplate.getGrossEarning()/12)));
	     Console.log(object.getGrossSalary()+"GROSS SALARY AMOUnt");
//			}
	}
	}
	
	
	/**date 15-2-2019 added by Amol**/
//	object.setGratuity(gratuityvalue);
	
	//Satuatory Deductions
	double sdpfValue=0,sdesicCalValue=0,sdptValue=0,sdlwf=0;
	/**
	 * @author Anil @since 15-03-2021
	 * if employee deduction is null in ctc template then it is throwing and error
	 */
	if(ctcTemplate.getDeduction()!=null){
		for (CtcComponent ctcComponent : ctcTemplate.getDeduction()) {
			if(ctcComponent.isDeduction()){
				if(ctcComponent.getShortName().equalsIgnoreCase("PF")/*||ctcComponent.getName().equalsIgnoreCase("BandP")*/){
					sdpfValue=sdpfValue+(ctcComponent.getAmount()/12);
				}
				if(ctcComponent.getShortName().equalsIgnoreCase("ESIC")/*||ctcComponent.getName().equalsIgnoreCase("BandP")*/){
					sdesicCalValue=sdesicCalValue+(ctcComponent.getAmount()/12);
				}
				if(ctcComponent.getShortName().equalsIgnoreCase("PT")/*||ctcComponent.getName().equalsIgnoreCase("BandP")*/){
					sdptValue=sdptValue+(ctcComponent.getAmount()/12);
				}
				if(ctcComponent.getShortName().equalsIgnoreCase("MLWF")||ctcComponent.getShortName().equalsIgnoreCase("LWF")||ctcComponent.getShortName().equalsIgnoreCase("MWF")){
					sdlwf=sdlwf+(ctcComponent.getAmount()/12);
				}
	//			if(ctcComponent.getShortName().equalsIgnoreCase("Gratuity")){
	//				gratuityvalue=gratuityvalue+(ctcComponent.getAmount()/12);
	//				
	//			}
			}
		}
	}
	
//	double pfValue = ((object.getBasic() + Double
//			.parseDouble(object.getDa())) / 100) * 12;
//	Console.log("pfValue" + pfValue);
	
	/**
	 * Date 17-08-2020 by Vijay
	 * Des :- Updated Code as Per the EPF CPF and CEDLI calculation using prodvident fund Master
	 * if PF Master not defined the it will calculate as per CTC Template Old Logic
	 */
//	double emppf = calculateWithPFMaster(sumOfMatchingComponents, "Employee PF");
	/**
	 * @author Anil @since 19-03-2021
	 * Updating above logic of calculating PF from PF master
	 * Just checking whether PF is added on ctc template
	 * if added then only we will calculate PF else it will be zero
	 */
	double emppf = 0;
	if(sdpfValue!=0){
		emppf = calculateWithPFMaster(sumOfMatchingComponents, "Employee PF");
	}
	
	if(emppf!=0){
		object.setSdPF((Math.round(emppf*100.0)/100.0) + "");
	}
	else{
		object.setSdPF((Math.round(sdpfValue*100.0)/100.0) + "");	
	}
	
	
	//ESIC Calculations
//	if (object.getGrossSalary() < 21001) {
//		double esicCalValue = object.getGrossSalary()
//				- object.getWashingAllowance();
//		double esicValue = (esicCalValue / 100) * 1.75;
//		Console.log("esicValue" + esicValue);
//		object.setSdESIC(Math.round(esicValue) + "");
//	} else {
		object.setSdESIC((Math.round(sdesicCalValue * 100.0)/100.0)+"");
//	}
	
//	if (object.getGrossSalary() >= 10000) {
		/**
		 * @author Anil,Date : 09-04-2019
		 */
		if(ptAndLwfDropdownFlag){
			object.setSdPT(getApplicablePtAmt(ctcTemplate)+"");
		}else{
			object.setSdPT(sdptValue+"");
		}
		/**
		 * 
		 */
//	} else {
//		object.setSdPT(175 + "");
//	}
	
	Console.log("pt" + object.getSdPT());
	Console.log("object.getSdLWF()" + object.getSdLWF());
	/**
	 * @author Anil,Date : 09-04-2019
	 */
	if(ptAndLwfDropdownFlag){
		object.setSdLWF(getApplicableLwfAmt(ctcTemplate)+"");
	}else{
		object.setSdLWF(sdlwf+"");
	}
	 /**Date 28-6-2019 by Amol for gratuity**/
//	 object.setGratuity(Math.round(gratuityvalue)+"");
	
	
	
	/**
	 * Shifting this code to below after cleave and cbonus calculation
	 */
//	Console.log("pl esic "+object.getPlEsic());
//	Console.log("bonus esic "+object.getBonusEsic());
//	
//	double satutoryDeduction = Double.parseDouble(object.getSdPF())+ Double.parseDouble(object.getSdESIC())
//			+ Double.parseDouble(object.getSdLWF())
//			+ Double.parseDouble(object.getSdPT())+(object.getPlEsic())+(object.getBonusEsic());
//	Console.log("satutoryDeduction" + satutoryDeduction);
//	object.setSatutoryDeductions(Math.round(satutoryDeduction) + "");
//	
//	double netSalaryValue = object.getGrossSalary()
//			- Double.parseDouble(object
//					.getSatutoryDeductions().trim());
//
//	object.setNetSalary(Math.round(netSalaryValue));
	
	// C Leave
	/**
	 * Rahul Verma added paid days. And changed(21/12) to (paiddays/12) where paiddays is provided by User
	 */
	double paiddaysValue=Double.parseDouble(object.getPaidLeaveDays().trim());
	
	if(object.iscLeaveApplicable()){
		double cLeaveValue = ((object.getBasic() + Double
				.parseDouble(object.getDa().trim()))*((paiddaysValue/12)/30));
		Console.log("cLeaveValue" + cLeaveValue);
		object.setcLeave((Math.round(cLeaveValue*100.0)/100.0)+"");
	}else{
		object.setcLeave(0+"");
	}
	/**
	 * @author Anil
	 * @since 20-08-2020
	 * paid leave calculation
	 * for sunrise raised by rahul tiwari
	 */
	if(paidleave!=null){
		double plAmt=0;
		if(paidleave.getOtEarningCompList()!=null){
			for(OtEarningComponent plComp:paidleave.getOtEarningCompList()){
				System.out.println("NAME : "+plComp.getComponentName());
				for(CtcComponent earningComp:ctcTemplate.getEarning()){
					System.out.println("EARN NAME : "+earningComp.getName());
					if(plComp.getComponentName().equalsIgnoreCase("Gross Earning")){
						plAmt=plAmt+earningComp.getAmount()/12;
					}else if(plComp.getComponentName().equals(earningComp.getName())){
						System.out.println("AMT : "+earningComp.getAmount()/12);
						plAmt=plAmt+earningComp.getAmount()/12;
					}
					
				}
			}
		}
		System.out.println("BASE AMT : "+plAmt);
		plAmt=plAmt*paidleave.getRate()/100;
		System.out.println("PL AMT : "+plAmt);
		object.setcLeave((Math.round(plAmt*100.0)/100.0)+"");
	}
	
//	double cLeaveValue = ((object.getBasic() + Double
//			.parseDouble(object.getDa().trim()))*(1.75/30));
//	Console.log("cLeaveValue" + cLeaveValue);
//	object.setcLeave(Math.round(cLeaveValue) + "");
	
	double cBonusPercent = 0;
	cBonusPercent = object.getcBonusPercentage();//Double.parseDouble(form.getTbcBonus().getValue().trim());
	Console.log("cBonusPercent" + cBonusPercent);
//	double cBonusValue = ((object.getBasic() + Double
//			.parseDouble(object.getDa().trim())))
//			* (cBonusPercent / 100);
//	Console.log("cBonusValue" + cBonusValue);
//	if(!editedcBonus)
	if(object.iscBonusApplicable()){
		double cBonusValue = ((object.getBasic() + Double
				.parseDouble(object.getDa().trim())))
				* (object.getcBonusPercentage() / 100);
		Console.log("cBonusValue" + cBonusValue);
		object.setcBonus((Math.round(cBonusValue*100.0)/100.0)+"");
	}else{
		object.setcBonus(0+"");
	}
	
	
	/**
	 * @author Anil
	 * @since 19-09-2020
	 */
	if(object.iscBonusApplicable()&&object.getcBonus()!=null){
		double bonusEsic=((Double.parseDouble(object.getcBonus())*0.75)/100);
		object.setBonusEsic(bonusEsic);
	}else{
		object.setBonusEsic(0);
	}
	
	
	if(object.iscLeaveApplicable()&&object.getcLeave()!=null){
		double plEsic=((Double.parseDouble(object.getcLeave())*0.75)/100);
		Console.log("#PL ESIC Value "+plEsic);
		object.setPlEsic(plEsic);
	}else{
		object.setPlEsic(0);
	}
	
	
	
	Console.log("pl esic "+object.getPlEsic());
	Console.log("bonus esic "+object.getBonusEsic());
	
	double satutoryDeduction = Double.parseDouble(object.getSdPF())+ Double.parseDouble(object.getSdESIC())
			+ Double.parseDouble(object.getSdLWF())
			+ Double.parseDouble(object.getSdPT())+(object.getPlEsic())+(object.getBonusEsic());
	Console.log("satutoryDeduction" + satutoryDeduction);
	object.setSatutoryDeductions(Math.round(satutoryDeduction) + "");
	
	double netSalaryValue = object.getGrossSalary()
			- Double.parseDouble(object
					.getSatutoryDeductions().trim());

	object.setNetSalary(Math.round(netSalaryValue));
	
	
	
	
//	object.setcBonus(Math.round(cBonusValue) + "");
	object.setTravellingAllowance(Math.round(object.getTravellingAllowance()*100.0)/100.0);
	//Take Home Salary
	double takeHomeValue = object.getNetSalary()
			+ Double.parseDouble(object.getcLeave())
			+ Double.parseDouble(object.getcBonus())
			+ object.getTravellingAllowance();
	Console.log("takeHomeValue" + takeHomeValue);
	object.setTakeHome((Math.round(takeHomeValue)) + "");
	// Statutory Payments
	double sppfValue=0,spesicCalValue=0,spptValue=0,splwf=0,spedli=0,gratuityvalue=0;
	/**
	 * @author Anil @since 15-03-2021
	 * if company contribution is null in ctc template then it is throwing and error
	 */
	if(ctcTemplate.getCompanyContribution()!=null){
		for (CtcComponent ctcComponent : ctcTemplate.getCompanyContribution()) {
	//		if(!ctcComponent.isDeduction()){
				if(ctcComponent.getShortName().trim().equalsIgnoreCase("PF")/*||ctcComponent.getName().equalsIgnoreCase("BandP")*/){
					/**
					 * @author Anil , Date : 29-06-2019
					 * calculating pf as per new pf master
					 */
					if(ctcComponent.getPercent1()!=0&&ctcComponent.getPercent2()!=0){
						double pfBase=((ctcComponent.getAmount()/12)*100)/ctcComponent.getMaxPerOfCTC();
						System.out.println("PF BASE : "+pfBase);
						double epfBase=(pfBase*ctcComponent.getPercent1())/100;
						System.out.println("EPF BASE : "+epfBase);
						sppfValue=sppfValue+epfBase;
						double edliBase=(pfBase*ctcComponent.getPercent2())/100;
						System.out.println("CEDLI : "+edliBase);
						spedli=spedli+edliBase;
					}else{
						sppfValue=sppfValue+(ctcComponent.getAmount()/12);
					}
					
					
				}
				if(ctcComponent.getShortName().equalsIgnoreCase("ESIC")/*||ctcComponent.getName().equalsIgnoreCase("BandP")*/){
					spesicCalValue=spesicCalValue+(ctcComponent.getAmount()/12);
				}
				if(ctcComponent.getShortName().equalsIgnoreCase("PT")/*||ctcComponent.getName().equalsIgnoreCase("BandP")*/){
					spptValue=spptValue+(ctcComponent.getAmount()/12);
				}
				if(ctcComponent.getShortName().equalsIgnoreCase("MLWF")||ctcComponent.getShortName().equalsIgnoreCase("LWF")||ctcComponent.getShortName().equalsIgnoreCase("MWF")){
					splwf=splwf+(ctcComponent.getAmount()/12);
				}
				
				if(ctcComponent.getShortName().equalsIgnoreCase("edli")){
					spedli=spedli+(ctcComponent.getAmount()/12);
				}
				if(ctcComponent.getShortName().equalsIgnoreCase("Gratuity")){
					gratuityvalue=gratuityvalue+(ctcComponent.getAmount()/12);
					Console.log("IN COMPANYCONTRIBUTION METHOD"+gratuityvalue);
					
				}
	//		}
		}
	}
	//pfValue
//	double cpfValue = ((object.getBasic() + Double.parseDouble(object.getDa())) / 100) * 12;
//	Console.log("cpfValue" + cpfValue);
	// if(!editedSPPF)
	/**
	 * Date 17-08-2020 by Vijay
	 * Des :- Updated Code as Per the EPF CPF and CEDLI calculation using prodvident fund Master
	 * if PF Master not defined the it will calculate as per CTC Template Old Logic
	 */
//	double spPf = calculateWithPFMaster(sumOfMatchingComponents, "Employeer PF");
	/**
	 * @author Anil @since 19-03-2021
	 * Updating above logic of calculating PF from PF master
	 * Just checking whether PF is added on ctc template
	 * if added then only we will calculate PF else it will be zero
	 */
	double spPf = 0;
	if(sppfValue!=0){
		spPf = calculateWithPFMaster(sumOfMatchingComponents, "Employeer PF");
	}
	
	if(spPf!=0){
		object.setSpPF((Math.round(spPf*100.0)/100.0) + "");
	}
	else{
		object.setSpPF((Math.round(sppfValue*100.0)/100.0) + "");
	}
	
//	if (object.getGrossSalary() < 21001) {
//		double esicCalValue = object.getGrossSalary()-object.getWashingAllowance();
//		double esicValue = (esicCalValue / 100) * 4.75;
//		Console.log("c esicValue" + esicValue);
//		object.setSpESIC(Math.round(esicValue) + "");
//	} else {
		object.setSpESIC((Math.round(spesicCalValue*100.0)/100.0) + "");
//	}
	
//	double edilCalValue = object.getBasic()
//			+ Double.parseDouble(object.getDa().trim());
//	double edilValue = edilCalValue * (1.16 / 100);
//	Console.log("edilValue" + edilValue);
	/**
	 * Date 17-08-2020 by Vijay
	 * Des :- Updated Code as Per the EPF CPF and CEDLI calculation using prodvident fund Master
	 * if PF Master not defined the it will calculate as per CTC Template Old Logic
	 */	
//	double spEDLI = calculateWithPFMaster(sumOfMatchingComponents, "EDLI");
	
	/**
	 * @author Anil @since 19-03-2021
	 * Updating above logic of calculating PF from PF master
	 * Just checking whether PF is added on ctc template
	 * if added then only we will calculate PF else it will be zero
	 */
	double spEDLI = 0;
	if(spedli!=0){
		spEDLI = calculateWithPFMaster(sumOfMatchingComponents, "EDLI");
	}
	
	if(spEDLI!=0){
		object.setSpEDLI((Math.round(spEDLI*100.0)/100.0) + "");
	}
	else{
		object.setSpEDLI((Math.round(spedli*100.0)/100.0) + "");

	}
	/**@author Amol Date 28-4-2019 for gratuity Calculation**/
	object.setGratuity(Math.round(gratuityvalue)+"");
	Console.log("IN SET GRATUITY"+gratuityvalue);
	/**
	 * @author Anil,Date : 09-04-2019
	 */
	if(ptAndLwfDropdownFlag){
		object.setSpMWF(getApplicableClwfAmt(ctcTemplate)+"");
	}else{
		object.setSpMWF(splwf+"");
	}
	/**
	 * Rahul Verma added paid days. And changed(21/12) to (paiddays/12) where paiddays is provided by User
	 */
	double paiddaysforBonus=Double.parseDouble(object.getPaidLeaveDays().trim());
	double bonusValue = 0;
	if (object.isBonusRelever()) {
		bonusValue = ((object.getBasic() + Double
				.parseDouble(object.getDa())) * (cBonusPercent / 100))
				+ (((object.getBasic() + Double
						.parseDouble(object.getDa())) * (cBonusPercent / 100)) * ((paiddaysforBonus/12) / 30));
	} else {
		bonusValue = ((object.getBasic() + Double
				.parseDouble(object.getDa())) * (cBonusPercent / 100));// +(1.75/30);
	}
	Console.log("bonusValue" + bonusValue);
	if(cBonusPercent!=0){
		object.setSpBonus((Math.round(bonusValue*100.0)/100.0) + "");
	}
	/**
	 * @author Anil
	 * @since 19-09-2020
	 * Calculating CBonus based on Bonus (% and reliever)
	 */
	double cbonusEsic=0;
	if(object.getSpBonus()!=null){
		cbonusEsic=((Double.parseDouble(object.getSpBonus())*3.25)/100);	
		object.setcBonusEsic(cbonusEsic);
	}else{
		object.setcBonusEsic(0);
		cbonusEsic=0;	
	}
	
	/** date 17.11.2018 added by komal for paid leave calculation as discussed with maradona**/
	double paidLeavesValue = 0;
//	Console.log("form.cb_Relever.getValue()"
//			+ object.isRelever() );
	/**
	 * Rahul Verma added paid days. And changed(21/12) to (paiddays/12) where paiddays is provided by User
	 */
	double paiddays=Double.parseDouble(object.getPaidLeaveDays().trim());
	double totalValue = Double.parseDouble(object.getSpEDLI())
			+ Double.parseDouble(object.getSpESIC())
			+ Double.parseDouble(object.getSpMWF())
			+ Double.parseDouble(object.getSpPF());
	totalValue = Math.round(totalValue*100.0)/100.0;
	if (object.isPaidLeaveRelever()) {
		paidLeavesValue = ((object.getGrossSalary() + (totalValue)) * ((paiddays/12)/30))
												+
				(((object.getGrossSalary() + (totalValue)))*((paiddays/12)/30))*( (paiddays/12)/ 30);
	} else {
		paidLeavesValue = (object.getGrossSalary() + (totalValue)) * ((paiddays/12)/30);
	}
	Console.log("paidLeavesValue" + paidLeavesValue);

	object.setPaidLeaves((Math.round(paidLeavesValue*100.0)/100.0) + "");
	
	/**
	 * @author Anil
	 * @since 20-08-2020
	 * paid leave calculation
	 * for sunrise raised by rahul tiwari
	 */
	if(paidleave!=null){
		double plAmt=0;
		if(paidleave.getOtEarningCompList()!=null){
			for(OtEarningComponent plComp:paidleave.getOtEarningCompList()){
				System.out.println("NAME : "+plComp.getComponentName());
				for(CtcComponent earningComp:ctcTemplate.getEarning()){
					System.out.println("EARN NAME : "+earningComp.getName());
					if(plComp.getComponentName().equalsIgnoreCase("Gross Earning")){
						plAmt=plAmt+earningComp.getAmount()/12;
					}else if(plComp.getComponentName().equals(earningComp.getName())){
						System.out.println("AMT : "+earningComp.getAmount()/12);
						plAmt=plAmt+earningComp.getAmount()/12;
					}
					
				}
			}
		}
		System.out.println("BASE AMT : "+plAmt);
		plAmt=plAmt*paidleave.getRate()/100;
		System.out.println("PL AMT : "+plAmt);
		object.setPaidLeaves((Math.round(plAmt*100.0)/100.0)+"");
	}
	
	/**
	 *  end komal
	 */
	
	/**
	 * @author Anil
	 * @since 19-09-2020
	 */
	double cplEsic=0;
	if(object.getPaidLeaves()!=null){
		cplEsic=((Double.parseDouble(object.getPaidLeaves())*3.25)/100);
		Console.log("#CPL ESIC Value "+plEsic);
		object.setcPlEsic(cplEsic);
	}else{
		object.setcPlEsic(0);
		cplEsic=0;
	}
	
	/**Date 24-6-2019 
	 * @author Amol
	 * adding gratuity formula
	 */
	double gratuity=0;
//	if(object.isGratuityApplicable()){
//	gratuity
//		=((((object.getBasic())+Double.parseDouble(object.getDa()))*4.81)/(100));
//		object.setGratuity((Math.round(gratuity)+""));
//	  }else{
//		  object.setGratuity(0+"");
//	 }
	object.setGratuity(object.getGratuity());
	object.setSupervision(object.getSupervision());
	object.setTrainingCharges(object.getTrainingCharges());
	
	double statutoryPayments = Double
			.parseDouble(object.getSpBonus())
			+ Double.parseDouble(object.getSpEDLI())
			+ Double.parseDouble(object.getSpESIC())
			+ Double.parseDouble(object.getSpMWF())
			+ Double.parseDouble(object.getSpPF())
			+ Double.parseDouble(object.getPaidLeaves())
	        +Double.parseDouble(object.getGratuity())
            +(object.getAdministrativecost())
//            +(object.getUniformCost())
            +(object.getSupervision())
            +(object.getTrainingCharges())
            +(object.getcPlEsic())
            +(object.getcBonusEsic());

	object.setSatutoryPayments((Math.round(statutoryPayments)) + "");
	object.setTotal((Math.round(((object.getGrossSalary()
			+ Double.parseDouble(object
					.getSatutoryPayments()))))) + "");
//	double paidLeavesValue = 0;
////	Console.log("form.cb_Relever.getValue()"
////			+ object.isRelever() );
//	/**
//	 * Rahul Verma added paid days. And changed(21/12) to (paiddays/12) where paiddays is provided by User
//	 */
//	double paiddays=Double.parseDouble(object.getPaidLeaveDays().trim());
//	if (object.isPaidLeaveRelever()) {
//		paidLeavesValue = ((object.getGrossSalary() + ((Double.parseDouble(object.getSatutoryPayments()))-Double.parseDouble(object.getSpBonus()))) * ((paiddays/12)/30))
//												+
//				(((object.getGrossSalary() + ((Double.parseDouble(object.getSatutoryPayments()))-Double.parseDouble(object.getSpBonus()))))*((paiddays/12)/30))*( (paiddays/12)/ 30);
//	} else {
//		paidLeavesValue = (object.getGrossSalary() + ((Double.parseDouble(object.getSatutoryPayments()))-Double.parseDouble(object.getSpBonus()))) * ((paiddays/12)/30);
//	}
//	Console.log("paidLeavesValue" + paidLeavesValue);
//
//	object.setPaidLeaves((Math.round(paidLeavesValue*100.0)/100.0) + "");
	object.setEmployementOverHeadCost(object.getOverHeadCost());
	object.setTravellingAllowanceOverhead(object.getTravellingAllowanceOverhead());
	object.setUniformCost(object.getUniformCost());
	double totalPaidAndOverheadValue = Double
			.parseDouble(object.getTotal())
			+ object.getAdditionalAllowance()
			+ Double.parseDouble(object
					.getEmployementOverHeadCost())
			+ object.getTravellingAllowanceOverhead()+object.getUniformCost();
	Console.log("totalPaidAndOverheadValue"
			+ totalPaidAndOverheadValue);
	object.setTotalOfPaidAndOverhead((Math.round(totalPaidAndOverheadValue))+ "");
	object.setTotalPaidIntoEmployee(Math.round((totalPaidAndOverheadValue*object.getNoOfStaff())));
	double feePercent = 0;
	double managementFeesValue = 0;
	try {
		if (object.getManageMentfeesPercent()!=0) {
		 	feePercent = object.getManageMentfeesPercent();
			//komal
			managementFeesValue = object.getTotalPaidIntoEmployee()*( feePercent / 100);
			Console.log("managementFeesValue"
					+ managementFeesValue);
			//object.setManagementFees(Math.round(managementFeesValue) + "");
		} else{
			if(object.getManagementFees()!=null && !object.getManagementFees().equals("")){
				managementFeesValue = Double.parseDouble(object.getManagementFees());
			}
			/**
			 * @author Anil
			 * @since 03-09-2020
			 */
			if(managementFeesAsPerStaff&&object.getNoOfStaff()!=0&&object.getManagementFeesRate()!=0){
				managementFeesValue=object.getNoOfStaff()*object.getManagementFeesRate();
			}
		}
		Console.log("feePercent" + feePercent);
	} catch (Exception e) {
		e.printStackTrace();
	}

//	double managementFeesValue = object.getTotalPaidIntoEmployee()*( feePercent / 100);
//	Console.log("managementFeesValue"
//			+ managementFeesValue);
object.setManagementFees((Math.round(managementFeesValue*100.0)/100.0) + "");

	double managementTotal = managementFeesValue+object.getTotalPaidIntoEmployee();
	Console.log("managementTotal" + managementTotal);
	object.setTotalIncludingManagementFees((Math.round(managementTotal))+ "");
	
	double grandTotalValue = managementTotal;
	object.setGrandTotalOfCTC((Math.round(grandTotalValue*100.0)/100.0) + "");
	Console.log("grandTotalValue" + grandTotalValue);
	RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
	table.redrawRow(index);
	
//	editedBasic=false;
//	editedDA=false;
//	editedHRA=false;
//	editedMedical=false;
//	editedWashingAllowance=false;
//	editedConveyance=false;
//	editedBandP=false;
//	editedLWF=false;
//	editedMWF=false;
//	editedPaidLeave=false;
//	editedOverheadCost=false;
//	editedSPPF=false;
//	editedcLeaves=false;
//	editedcBonus=false;
//	updateRow(object, index);
}

//public void updateGratuityApplicable(StaffingDetails staffingDetails, int count) {
//	if(staffingDetails.isGratuityApplicable()){
//		double gratuity
//		=((((staffingDetails.getBasic())+Double.parseDouble(staffingDetails.getDa()))*4.81)/(100));
//		staffingDetails.setGratuity((Math.round(gratuity)+""));
//	  }else{
//		staffingDetails.setGratuity(0+"");
//	 }
//}

public PaidLeave getPaidleave() {
	return paidleave;
}

public void setPaidleave(PaidLeave paidleave) {
	this.paidleave = paidleave;
}	

private void loadProvidentFund() {
		Vector<Filter> vecfilter = new Vector<Filter>();
		Filter filter = null;

		filter = new Filter();
		filter.setBooleanvalue(true);
		filter.setQuerryString("status");
		vecfilter.add(filter);

		MyQuerry quer = new MyQuerry(vecfilter, new ProvidentFund());
		generivservice.getSearchResult(quer, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				if(result.size()!=0){
					for(SuperModel model : result){
						ProvidentFund pfEntity = (ProvidentFund) model;
						pfEntityList.add(pfEntity);
						break;
					}
				}
				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
	}
	
	
	private double calculateWithPFMaster(double sumOfMatchingComponents, String contibutionName) {
		// TODO Auto-generated method stub
		if (pfEntityList.size() != 0) {
			for (ProvidentFund providentEntity : pfEntityList) {
				String strPfMaxFixedValue = AppUtility.getForProcessConfigurartionIsActiveOrNot("PfMaxFixedValue");
				System.out.println("strPfMaxFixedValue "+strPfMaxFixedValue);
				System.out.println("sumOfMatchingComponents"+sumOfMatchingComponents);
				double empContribution = 0;
				if(contibutionName.equals("Employee PF")){
					empContribution = providentEntity.getEmployeeContribution();
				}
				else if(contibutionName.equals("Employeer PF")){
					empContribution = providentEntity.getEmpoyerPF();
				}
				else if(contibutionName.equals("EDLI")){
					empContribution = providentEntity.getEdli();
				}
				
				if(empContribution!=0){

					int PfMaxFixedValue = 0;
					try {
						 PfMaxFixedValue = Integer.parseInt(strPfMaxFixedValue);
					} catch (Exception e) {
					}
					
					if(PfMaxFixedValue>0 && sumOfMatchingComponents>=PfMaxFixedValue){
						return ((PfMaxFixedValue * empContribution)/100);
					}
					else{
						return ((sumOfMatchingComponents * empContribution)/100);
					}
						
				}
			}
		}

		return 0;
	}
	
	private double getTotalOfComponents(CtcComponent ctcComponent) {
		double sum = 0;
		System.out.println("ctcComponent Name"+ctcComponent.getCtcComponentName());
		if (pfEntityList.size() != 0) {
			for (ProvidentFund providentEntity : pfEntityList) {
				for(OtEarningComponent earningComponent : providentEntity.getCompList()){
					if (earningComponent.getComponentName().equals(ctcComponent.getShortName()) || earningComponent.getComponentName().equals(ctcComponent.getName())) {
						sum +=ctcComponent.getAmount()/12;
					}
				}
			}
		}
		return sum;
	}

}
