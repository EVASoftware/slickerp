package com.slicktechnologies.client.views.cnc;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.cnc.OtRateDetails;

public class OtRateDetailTable extends SuperTable<OtRateDetails> {

	TextColumn<OtRateDetails> designationCol,otNameCol,flatRateCol,factorRateCol,typeCol;
	Column<OtRateDetails,String> deleteCol;
	
	public OtRateDetailTable(){
		super();
		table.setHeight("300px");
	}
	
	@Override
	public void createTable() {
		// TODO Auto-generated method stub
		designationCol();
		otNameCol();
		flatRateCol();
		factorRateCol();
//		typeCol();
		deleteCol();
	}

	private void designationCol() {
		// TODO Auto-generated method stub
		designationCol=new TextColumn<OtRateDetails>() {
			@Override
			public String getValue(OtRateDetails object) {
				// TODO Auto-generated method stub
				return object.getDesignation()+"";
			}
		};
		table.addColumn(designationCol,"Designation");
//		table.setColumnWidth(designationCol, 120,Unit.PX);
	}

	private void otNameCol() {
		// TODO Auto-generated method stub
		otNameCol=new TextColumn<OtRateDetails>() {
			@Override
			public String getValue(OtRateDetails object) {
				// TODO Auto-generated method stub
				return object.getOtName()+"";
			}
		};
		table.addColumn(otNameCol,"Overtime");
	}

	private void flatRateCol() {
		// TODO Auto-generated method stub
		flatRateCol=new TextColumn<OtRateDetails>() {
			@Override
			public String getValue(OtRateDetails object) {
				// TODO Auto-generated method stub
				if(object.getFlatRate()!=0){
					return object.getFlatRate()+"";
				}else{
					return "";
				}
			}
		};
		table.addColumn(flatRateCol,"Flat Rate");
	}

	private void factorRateCol() {
		// TODO Auto-generated method stub
		factorRateCol=new TextColumn<OtRateDetails>() {
			@Override
			public String getValue(OtRateDetails object) {
				// TODO Auto-generated method stub
				if(object.getFactorRate()!=0){
					return object.getFactorRate()+"";
				}else{
					return "";
				}
			}
		};
		table.addColumn(factorRateCol,"Rate");
	}

	private void typeCol() {
		// TODO Auto-generated method stub
		typeCol=new TextColumn<OtRateDetails>() {
			@Override
			public String getValue(OtRateDetails object) {
				// TODO Auto-generated method stub
				return object.getType()+"";
			}
		};
		table.addColumn(typeCol,"Type");
	}

	private void deleteCol() {
		// TODO Auto-generated method stub
		ButtonCell btnCell= new ButtonCell();
		deleteCol = new Column<OtRateDetails, String>(btnCell) {
			@Override
			public String getValue(OtRateDetails object) {
				return " - ";
			}
		};
		table.addColumn(deleteCol,"Delete");
//		table.setColumnWidth(deleteCol, 50,Unit.PX);
		
		deleteCol.setFieldUpdater(new FieldUpdater<OtRateDetails, String>() {
			@Override
			public void update(int index, OtRateDetails object, String value) {
				getDataprovider().getList().remove(index);
				table.redrawRow(index);
			}
		});
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		int tablecolcount=this.table.getColumnCount();
		for(int i=tablecolcount-1;i>-1;i--){
		  table.removeColumn(i);
		}
		if(state ==true){
			designationCol();
			otNameCol();
			flatRateCol();
			factorRateCol();
//			typeCol();
			deleteCol();
		}
		if(state==false){
			designationCol();
			otNameCol();
			flatRateCol();
			factorRateCol();
//			typeCol();
		}
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
