package com.slicktechnologies.client.views.cnc;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.slicktechnologies.shared.CNCArrearsDetails;
import com.slicktechnologies.shared.common.ProjectAllocation;
import com.slicktechnologies.shared.common.cnc.CNC;
import com.slicktechnologies.shared.common.salesprocess.AnnexureType;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

public interface CncBillServiceAsync {
	void generateBill(Date fromDate,Date toDate,CNC cnc,int salesOrderId,AsyncCallback<String>callback);

	void generateProject(CNC cnc, AsyncCallback<String> callback);
	/** date 23.1.2019 added by komal to add validation while cancelling CNC
	 *  project attendance and billing validation 
	 */
	void validateProjectAttendanceAndBilling(CNC cnc , AsyncCallback<String> callback);
	/** date 23.1.2019 added by komal
	 *  Description : to cancel cnc and to inactive cnc and hr project
	 */
	void cancelCNC(CNC cnc , String remark , AsyncCallback<String> callback);
	
	void generateArrearsBill(Date fromDate,Date toDate,CNC cnc,ArrayList<CNCArrearsDetails> list, AsyncCallback<String> callback);
	
	/**
	 * @author Anil @since 25-03-2021
	 * This method returns attendance summary in case of CNC bills
	 * For Sunrise raised by Rahul
	 * @param companyId
	 * @param invoice
	 * @return
	 */
	void getAnnexureDetailsForCNCBill(long companyId,Invoice invoice, AsyncCallback<HashMap<String,ArrayList<AnnexureType>>> callback);
}

