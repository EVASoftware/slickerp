package com.slicktechnologies.client.views.customerlog.amc;

import java.util.ArrayList;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.TextArea;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.slicktechnologies.client.views.contract.CancelContractTable;

public class AMCAllocationPopup extends PopupScreen {	
	AMCAllocationTable amcAllocationTable;
    Button btnCancel,btnOk;
	TextArea remark;
	
	public AMCAllocationPopup(){
		super();
		amcAllocationTable.connectToLocal();
		getPopup().setWidth("400px");
		//"getPopup().setHeight("500px");
		getAmcAllocationTable().getTable().setHeight("400px");
		createGui();
	}
	
	private void initializeWidget() {

		amcAllocationTable = new AMCAllocationTable();
	}

	@Override
	public void createScreen() {
		// TODO Auto-generated method stub
		
		initializeWidget();
		
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingAMCAllocation=fbuilder.setlabel("AMC Allocation").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("", amcAllocationTable.getTable());
		FormField famcAllocationTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		FormField[][] formfield = {  
				{fgroupingAMCAllocation},
				{famcAllocationTable}
			
		};
		this.fields=formfield;
	}

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
	}
	public TextArea getRemark() {
		return remark;
	}

	public void setRemark(TextArea remark) {
		this.remark = remark;
	}

	public AMCAllocationTable getAmcAllocationTable() {
		return amcAllocationTable;
	}

	public void setAmcAllocationTable(AMCAllocationTable amcAllocationTable) {
		this.amcAllocationTable = amcAllocationTable;
	}
	
}
