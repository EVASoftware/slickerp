package com.slicktechnologies.client.views.customerlog.amc;

import java.util.Comparator;
import java.util.List;

import com.google.code.p.gwtchismes.client.GWTCGlassPanel;
import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.ValueUpdater;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.Header;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.inventory.MaterialMovementNote;
import com.slicktechnologies.shared.common.servicerelated.CompanyAsset;

public class AMCAllocationTable extends SuperTable<CompanyAsset> {
	
	TextColumn<CompanyAsset> getColumnAMCNumber;
	TextColumn<CompanyAsset> getColumnAMCStatus;	
	Column<CompanyAsset, Boolean> checkColumn;
	Header<Boolean> selectAllHeader;
	
	@Override
	public void createTable() {
		addCheckBoxCell();
		createColumnAMCNumber();
		createColumnAMCStatus();	
		addColumnSorting();
	}
	public void addColumnSorting(){
		addSortinggetColumnAMCNumber();
		addSortinggetColumnAMCStatus();
	}
	public void createColumnAMCNumber(){	
		getColumnAMCNumber=new TextColumn<CompanyAsset>() {

				@Override
				public String getValue(CompanyAsset object) {
					if(object.getSrNo()!= null)
					{
						return object.getName();
					}
					return "";
				}
			};
			
			table.addColumn(getColumnAMCNumber,"AMC Number");		
		}		
   public void createColumnAMCStatus(){
	
	getColumnAMCStatus=new TextColumn<CompanyAsset>() {

			@Override
			public String getValue(CompanyAsset object) {
				if(object.getAmcStatus()!= null)
				{
					return object.getAmcStatus();
				}
				return "";
			}
		};
		
		table.addColumn(getColumnAMCStatus,"Status");
		
    }
   protected void addSortinggetColumnAMCStatus()
	{
		List<CompanyAsset> list=getDataprovider().getList();
		columnSort=new ListHandler<CompanyAsset>(list);
		columnSort.setComparator(getColumnAMCStatus, new Comparator<CompanyAsset>()
				{
			@Override
			public int compare(CompanyAsset e1,CompanyAsset e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getName()!=null && e2.getName()!=null){
						return e1.getName().compareTo(e2.getName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
   protected void addSortinggetColumnAMCNumber()
  	{
  		List<CompanyAsset> list=getDataprovider().getList();
  		columnSort=new ListHandler<CompanyAsset>(list);
  		columnSort.setComparator(getColumnAMCNumber, new Comparator<CompanyAsset>()
  				{
  			@Override
  			public int compare(CompanyAsset e1,CompanyAsset e2)
  			{
  				if(e1!=null && e2!=null)
  				{
  					if( e1.getName()!=null && e2.getName()!=null){
  						return e1.getName().compareTo(e2.getName());}
  				}
  				else{
  					return 0;}
  				return 0;
  			}
  				});
  		table.addColumnSortHandler(columnSort);
  	}
		private void addCheckBoxCell() {
			
			checkColumn=new Column<CompanyAsset, Boolean>(new CheckboxCell()) {
				@Override
				public Boolean getValue(CompanyAsset object) {
					return object.isRecordSelect();
				}
			};
			
			checkColumn.setFieldUpdater(new FieldUpdater<CompanyAsset, Boolean>() {
				@Override
				public void update(int index, CompanyAsset object, Boolean value) {
					System.out.println("Check Column ....");
					object.setRecordSelect(value);
					table.redrawRow(index);
					selectAllHeader.getValue();
					table.redrawHeaders();
					

				}
			});
			
			
			selectAllHeader =new Header<Boolean>(new CheckboxCell()) {
				@Override
				public Boolean getValue() {
					if(getDataprovider().getList().size()!=0){
						
					}
					return false;
				}
			};
			
			
			selectAllHeader.setUpdater(new ValueUpdater<Boolean>() {
				@Override
				public void update(Boolean value) {
					List<CompanyAsset> list=getDataprovider().getList();
					for(CompanyAsset object:list){
						object.setRecordSelect(value);
					}
					getDataprovider().setList(list);
					table.redraw();
					addColumnSorting();
				}
			});
			table.addColumn(checkColumn,selectAllHeader);

		
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		if(state == false){
			createColumnAMCNumber();
			createColumnAMCStatus();	
		}
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

	

}
