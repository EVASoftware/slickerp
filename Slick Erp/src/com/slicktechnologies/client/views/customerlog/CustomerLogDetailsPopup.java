package com.slicktechnologies.client.views.customerlog;

import java.util.ArrayList;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.AddressComposite;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.MyInlineLabel;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.interaction.interactiondetails.InteractionPresenter;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerlog.CustomerLogDetails;
import com.slicktechnologies.shared.common.customerlog.MaterialRequired;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.workorder.WorkOrderProductDetails;

public class CustomerLogDetailsPopup extends PopupScreen implements ClickHandler{
	TextBox tbComplaintNumber;
	TextBox tbCustomerName , tbCellNumber , tbCellNumber2 , tbSerialNo,tbStatus,tbModelNo;
	CheckBox IsCallback;
	ObjectListBox<ConfigCategory> olbWarrantyStatus;
	ObjectListBox<Employee> olbTechnician;
	ObjectListBox<ConfigCategory> olbCallType;
	//ObjectListBox<Customer> olbCompany;
	ObjectListBox<Branch> olbCompany;
	AddressComposite addressComposite;
	DateBox dbAppointmentDate , dbCreatedOn;
	ObjectListBox<ConfigCategory> olbProduct;
	ObjectListBox<ConfigCategory> olbCallbackStatus;
	ObjectListBox<ConfigCategory> olbVisitStatus;
	TextArea taRemark;
	CheckBox cbScheme;  
    TextBox tbSchemeAmount;
	CustomerLogDetails customerLogObj;
	ProductInfoComposite productInfoComposite;
	/**
	 * Date 15-5-2018 
	 * by jayshree
	 * des.change the editpopupas per the mascom requrment
	 */
	PersonInfoComposite personInfoComposite;
	Button btnNewCustomer;
	final GenricServiceAsync genasync=GWT.create(GenricService.class);
	
	/** date 6.7.2018 added by komal for technician remark  and closing date**/
	TextArea taResolution;
	DateBox dbClosingDate;
	
	public CustomerLogDetailsPopup(){
		super();
		getPopup().getElement().addClassName("popupCustomerLogDetails");
		this.getDbCreatedOn().setEnabled(false);
		this.getTbStatus().setEnabled(false);
		this.getTbCustomerName().setEnabled(false);
		this.getTbCellNumber().setEnabled(false);
		this.getTbComplaintNumber().setEnabled(true);//by jayshree
		//this.getOlbCallType().setEnabled(false);
		this.getOlbCompany().setEnabled(true);//by jayshree
		
		/**
		 * Date 13-6-2018 
		 * by jayshree
		 * des.make a complain number non editable
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CustomerLogDetails","OnlyForMASCOM"))
		this.tbComplaintNumber.setEnabled(false);
		
		//this.getOlbTechnician().setEnabled(false);
		this.getCbScheme().addClickHandler(this);
		
		
		createGui();
	}
	
	private void initializeWidget() {
		tbComplaintNumber = new TextBox();
		tbCustomerName = new TextBox();
		tbCellNumber = new TextBox();
		tbCellNumber2 = new TextBox();
		tbSerialNo = new TextBox();
		tbStatus = new TextBox();
		dbCreatedOn = new DateBox();
		IsCallback = new CheckBox();
		IsCallback.setValue(false);
		tbModelNo = new TextBox();
		taRemark = new TextArea();
		dbAppointmentDate = new DateBox();
		addressComposite = new AddressComposite(true);
		olbWarrantyStatus = new ObjectListBox<ConfigCategory>(); 
		AppUtility.MakeLiveConfig(olbWarrantyStatus,Screen.WARRANTYSTATUS);
		olbTechnician = new ObjectListBox<Employee>();
		olbTechnician.makeEmployeeLive(AppConstants.SALESMODULE, "Customer Call Log Details", "Technician");
        olbCallType = new ObjectListBox<ConfigCategory>();
        AppUtility.MakeLiveConfig(olbCallType,Screen.CALLTYPE);
//		olbCompany = new ObjectListBox<Customer>();
//		AppUtility.makeCompanyNameListBoxLive(olbCompany);
        olbCompany = new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbCompany);
		olbCallbackStatus = new ObjectListBox<ConfigCategory>();
	    AppUtility.MakeLiveConfig(olbCallbackStatus,Screen.CALLBACKSTATUS);
	   // olbProduct = new ObjectListBox<ConfigCategory>();
	  //  AppUtility.MakeLiveConfig(olbProduct,Screen.PRODUCT);
		olbVisitStatus = new ObjectListBox<ConfigCategory>();
	    AppUtility.MakeLiveConfig(olbVisitStatus,Screen.VISITSTATUS);
	    productInfoComposite = AppUtility.initiatePurchaseProductComposite(new ServiceProduct());
	    cbScheme = new CheckBox("Is Scheme");
		cbScheme.setValue(false);
		tbSchemeAmount = new TextBox();
		/**
		 * Date 15-5-2018 
		 * by jayshree
		 * des.change the editpopupas per the mascom requrment
		 */
		btnNewCustomer = new Button("New Customer");
		personInfoComposite=AppUtility.customerInfoComposite(new Customer());
		personInfoComposite.getCustomerId().getHeaderLabel().setText("* Customer ID");
		personInfoComposite.getCustomerName().getHeaderLabel().setText("* Customer Name");
		personInfoComposite.getCustomerCell().getHeaderLabel().setText("* Customer Cell");
		/** date 6.7.2018 added by komal for technician remark  and closing date**/
		taResolution = new TextArea();
		taResolution.setEnabled(false);
		dbClosingDate =new DateBox();
		dbClosingDate.setEnabled(false);
		}
	@Override
	public void createScreen() {
		// TODO Auto-generated method stub	
		initializeWidget();		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingCustomerCallLog = fbuilder.setlabel("Customer Call Log").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("* Warranty Status",olbWarrantyStatus);
		FormField folbWarrantyStatus = fbuilder.setMandatory(true).setMandatoryMsg("WarrantyStatus is mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Call Type",olbCallType);
		FormField folbCallType= fbuilder.setMandatory(true).setRowSpan(0).setColSpan(0).build();	
		fbuilder = new FormFieldBuilder("* Technician",olbTechnician);
		FormField folbTechnician= fbuilder.setMandatory(true).setMandatoryMsg("Technician is mandatory").setRowSpan(0).setColSpan(0).build();			
		fbuilder = new FormFieldBuilder("* Company",olbCompany);
		FormField folbCompany= fbuilder.setMandatory(true).setRowSpan(0).setColSpan(0).build();	
		fbuilder = new FormFieldBuilder("* Customer Name",tbCustomerName);
		FormField ftbCustomerName= fbuilder.setMandatory(true).setRowSpan(0).setColSpan(0).build();	
		fbuilder = new FormFieldBuilder("* Cell No",tbCellNumber);
		FormField ftbCellNumber= fbuilder.setMandatory(true).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Complaint Number",tbComplaintNumber);
		FormField ftbComplaintNumber= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		FormField fgroupingAddressInformation=fbuilder.setlabel("Customer Address Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("", addressComposite);
		FormField faddressComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Appointment Date",dbAppointmentDate);
		FormField fdbAppointmentDate= fbuilder.setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Product",olbProduct);
		FormField ftbProduct= fbuilder.setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("CallBack Status",olbCallbackStatus);
		FormField folbolbCallbackStatus= fbuilder.setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Is Callback",IsCallback);
		FormField fIsCallback= fbuilder.setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Cell Number2",tbCellNumber2);
		FormField ftbCellNumber2= fbuilder.setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Product Serial No",tbSerialNo);
		FormField ftbSerialNo= fbuilder.setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Status", tbStatus);
		FormField ftbStatus= fbuilder.setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Created On", dbCreatedOn);
		FormField fdbCreatedOn= fbuilder.setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Visit Status", olbVisitStatus);
		FormField folbVisitStatus= fbuilder.setMandatory(true).setMandatoryMsg("Visit Status is mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Remark ", taRemark);
		FormField ftaRemark= fbuilder.setMandatory(true).setMandatoryMsg("Remark is mandatory!").setRowSpan(0).setColSpan(5).build();
		fbuilder = new FormFieldBuilder("ModelNo", tbModelNo);
		FormField ftbModelNo= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("", productInfoComposite);
		FormField ftbProductInfoComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		fbuilder = new FormFieldBuilder("",cbScheme);
		FormField fcbScheme= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Scheme Amount",tbSchemeAmount);
		FormField ftbSchemeAmount= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		/**
		 * Date 15-5-2018 
		 * by jayshree
		 * des.change the editpopupas per the mascom requrment
		 */
		fbuilder = new FormFieldBuilder("",btnNewCustomer);
		FormField fbtnNewCustomer= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("",personInfoComposite);
		FormField fpersonInfoComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		FormField fgroupingCustomerInformation=fbuilder.setlabel("Customer Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("",personInfoComposite);
		
		/** date 6.7.2018 added by komal for technician remark  and closing date**/
		fbuilder = new FormFieldBuilder("Resolution ", taResolution);
		FormField ftaResolution= fbuilder.setMandatory(false).setMandatoryMsg("Resolution is mandatory!").setRowSpan(0).setColSpan(5).build();
		
		fbuilder = new FormFieldBuilder("Closing Date", dbClosingDate);
		FormField fdbClosingDate= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		FormField[][] formfield = {
				{fgroupingCustomerInformation},
				{fbtnNewCustomer},
				{fpersonInfoComposite },
				{fgroupingCustomerCallLog},
				{ftbComplaintNumber , folbCompany, ftbCellNumber2 },//ftbCustomerName ftbCellNumber
				{ folbCallType ,folbTechnician , folbWarrantyStatus },
				{ftbProductInfoComposite },
				{ftbModelNo , ftbSerialNo, fcbScheme , ftbSchemeAmount},
				{folbVisitStatus,fdbAppointmentDate ,ftbStatus ,fdbCreatedOn},
				{folbolbCallbackStatus ,fdbClosingDate},/** date 6.7.2018 added by komal for closing date**/
				{ftaRemark} ,
				{ftaResolution}, /** date 6.7.2018 added by komal for technician remark **/
				{fgroupingAddressInformation},
				{faddressComposite}
		};
				this.fields = formfield;
	}

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		if(event.getSource().equals(getCbScheme())){
			if(this.getCbScheme().getValue()){
				this.getTbSchemeAmount().setEnabled(true);
			}else{
				this.getTbSchemeAmount().setValue(0+"");
				this.getTbSchemeAmount().setEnabled(false);				
			}
		}	
	}
	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		super.setEnable(state);
		this.getDbCreatedOn().setEnabled(false);
		this.getTbStatus().setEnabled(false);
		this.getTbCustomerName().setEnabled(false);
		this.getTbCellNumber().setEnabled(false);
		/**
		 * dATE 15-6-2018
		 * BY JAYSHREE
		 */
		this.getTbComplaintNumber().setEnabled(true);
	//	this.getOlbCallType().setEnabled(false);
		this.getOlbCompany().setEnabled(true);
		this.getTbComplaintNumber().setEnabled(true);
		/** date 6.7.2018 added by komal for technician remark  and closing date**/
		this.taResolution.setEnabled(false);
		this.dbClosingDate.setEnabled(false);
//		this.getOlbTechnician().setEnabled(state);
//		this.getOlbWarrantyStatus().setEnabled(state);
//		this.getOlbCallbackStatus().setEnabled(state);
//		this.getOlbProduct().setEnabled(state);
//		this.getDbAppointmentDate().setEnabled(state);
//		this.getAddressComposite().setEnable(state);
//		this.getTbCellNumber2().setEnabled(state);
//	    this.getTbSerialNo().setEnabled(state);
	}
	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
		return super.validate();
	}
	public TextBox getTbCustomerName() {
		return tbCustomerName;
	}

	public void setTbCustomerName(TextBox tbCustomerName) {
		this.tbCustomerName = tbCustomerName;
	}

	public TextBox getTbCellNumber() {
		return tbCellNumber;
	}

	public void setTbCellNumber(TextBox tbCellNumber) {
		this.tbCellNumber = tbCellNumber;
	}

	public ObjectListBox<ConfigCategory> getOlbWarrantyStatus() {
		return olbWarrantyStatus;
	}

	public void setOlbWarrantyStatus(ObjectListBox<ConfigCategory> olbWarrantyStatus) {
		this.olbWarrantyStatus = olbWarrantyStatus;
	}

	public ObjectListBox<Employee> getOlbTechnician() {
		return olbTechnician;
	}

	public void setOlbTechnician(ObjectListBox<Employee> olbTechnician) {
		this.olbTechnician = olbTechnician;
	}

	public ObjectListBox<ConfigCategory> getOlbCallType() {
		return olbCallType;
	}

	public void setOlbCallType(ObjectListBox<ConfigCategory> olbCallType) {
		this.olbCallType = olbCallType;
	}

	public ObjectListBox<Branch> getOlbCompany() {
		return olbCompany;
	}

	public void setOlbCompany(ObjectListBox<Branch> olbCompany) {
		this.olbCompany = olbCompany;
	}

	public TextBox getTbComplaintNumber() {
		return tbComplaintNumber;
	}

	public void setTbComplaintNumber(TextBox tbComplaintNumber) {
		this.tbComplaintNumber = tbComplaintNumber;
	}

	public TextBox getTbStatus() {
		return tbStatus;
	}

	public void setTbStatus(TextBox tbStatus) {
		this.tbStatus = tbStatus;
	}

	public DateBox getDbCreatedOn() {
		return dbCreatedOn;
	}

	public void setDbCreatedOn(DateBox dbCreatedOn) {
		this.dbCreatedOn = dbCreatedOn;
	}

	public CustomerLogDetails getCustomerLogObj() {
		return customerLogObj;
	}

	public void setCustomerLogObj(CustomerLogDetails customerLogObj) {
		this.customerLogObj = customerLogObj;
	}

	public TextBox getTbModelNo() {
		return tbModelNo;
	}

	public void setTbModelNo(TextBox tbModelNo) {
		this.tbModelNo = tbModelNo;
	}

	public ObjectListBox<ConfigCategory> getOlbVisitStatus() {
		return olbVisitStatus;
	}

	public void setOlbVisitStatus(ObjectListBox<ConfigCategory> olbVisitStatus) {
		this.olbVisitStatus = olbVisitStatus;
	}

	public TextArea getTaRemark() {
		return taRemark;
	}

	public void setTaRemark(TextArea taRemark) {
		this.taRemark = taRemark;
	}

	public TextBox getTbCellNumber2() {
		return tbCellNumber2;
	}

	public void setTbCellNumber2(TextBox tbCellNumber2) {
		this.tbCellNumber2 = tbCellNumber2;
	}

	public TextBox getTbSerialNo() {
		return tbSerialNo;
	}

	public void setTbSerialNo(TextBox tbSerialNo) {
		this.tbSerialNo = tbSerialNo;
	}

	public CheckBox getIsCallback() {
		return IsCallback;
	}

	public void setIsCallback(CheckBox isCallback) {
		IsCallback = isCallback;
	}

	public AddressComposite getAddressComposite() {
		return addressComposite;
	}

	public void setAddressComposite(AddressComposite addressComposite) {
		this.addressComposite = addressComposite;
	}

	public DateBox getDbAppointmentDate() {
		return dbAppointmentDate;
	}

	public void setDbAppointmentDate(DateBox dbAppointmentDate) {
		this.dbAppointmentDate = dbAppointmentDate;
	}	
	public ObjectListBox<ConfigCategory> getOlbProduct() {
		return olbProduct;
	}

	public void setOlbProduct(ObjectListBox<ConfigCategory> olbProduct) {
		this.olbProduct = olbProduct;
	}

	public ObjectListBox<ConfigCategory> getOlbCallbackStatus() {
		return olbCallbackStatus;
	}

	public void setOlbCallbackStatus(ObjectListBox<ConfigCategory> olbCallbackStatus) {
		this.olbCallbackStatus = olbCallbackStatus;
	}

	public ProductInfoComposite getProductInfoComposite() {
		return productInfoComposite;
	}

	public void setProductInfoComposite(ProductInfoComposite productInfoComposite) {
		this.productInfoComposite = productInfoComposite;
	}

	public CheckBox getCbScheme() {
		return cbScheme;
	}

	public void setCbScheme(CheckBox cbScheme) {
		this.cbScheme = cbScheme;
	}

	public TextBox getTbSchemeAmount() {
		return tbSchemeAmount;
	}

	public void setTbSchemeAmount(TextBox tbSchemeAmount) {
		this.tbSchemeAmount = tbSchemeAmount;
	}
	
	/**
	 * Date 15-6-2018
	 * by jayshree
	 * 
	 */
	
	public PersonInfoComposite getPersonInfoComposite() {
		return personInfoComposite;
	}
	public void setPersonInfoComposite(PersonInfoComposite personInfoComposite) {
		this.personInfoComposite = personInfoComposite;
	}

	public Button getBtnNewCustomer() {
		return btnNewCustomer;
	}
	public void setBtnNewCustomer(Button btnNewCustomer) {
		this.btnNewCustomer = btnNewCustomer;
	}

	

	public void checkCustomerStatus(int cCount) {
		MyQuerry querry=new MyQuerry();
		Company c=new Company();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		
		temp=new Filter();
		temp.setQuerryString("companyId");
		temp.setLongValue(c.getCompanyId());
		filtervec.add(temp);
		
		temp=new Filter();
		temp.setQuerryString("count");
		temp.setIntValue(cCount);
		filtervec.add(temp);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Customer());
		
		genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onFailure(Throwable caught) {
				showDialogMessage("An Unexpected error occurred!");
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
					for(SuperModel model:result)
					{
						Customer custEntity = (Customer)model;
						
						if(custEntity.getNewCustomerFlag()==true){
							showDialogMessage("Please fill customer information by editing customer");
							personInfoComposite.clear();
						}
						else if(AppConstants.ACTIVE.equals(custEntity.getStatus().trim())&&(custEntity.getNewCustomerFlag()==false)){
							 getPersonInfoComposite().getId().getElement().addClassName("personactive");
							 getPersonInfoComposite().getName().getElement().addClassName("personactive");
							 getPersonInfoComposite().getPhone().getElement().addClassName("personactive");
							 getPersonInfoComposite().getTbpocName().getElement().addClassName("personactive");
						}
						
						
						
					}
				}
			 });
		}

	public TextArea getTaResolution() {
		return taResolution;
	}

	public void setTaResolution(TextArea taResolution) {
		this.taResolution = taResolution;
	}

	public DateBox getDbClosingDate() {
		return dbClosingDate;
	}

	public void setDbClosingDate(DateBox dbClosingDate) {
		this.dbClosingDate = dbClosingDate;
	}
	
}