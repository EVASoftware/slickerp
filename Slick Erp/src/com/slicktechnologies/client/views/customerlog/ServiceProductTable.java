package com.slicktechnologies.client.views.customerlog;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.SelectionCell;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.views.inventory.materialreuestnote.MaterialRequestNotePresenter;
import com.slicktechnologies.client.views.inventory.recievingnote.InventoryLocationPopUp;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.TaxDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerlog.MaterialRequired;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.productlayer.Tax;

public class ServiceProductTable extends SuperTable<MaterialRequired> implements ClickHandler {
	
	TextColumn<MaterialRequired> getColumnProductId;
	TextColumn<MaterialRequired> getColumnProductCode;
	Column<MaterialRequired , String> getColumnProductName;
	TextColumn<MaterialRequired> getColumnProductRequiredQty;
	Column<MaterialRequired, String> getColumnProductDeleteButton;
	Column<MaterialRequired , String> getColumnProdPrice;
	//
	Column<MaterialRequired,String> vatColumn,serviceColumn;
	
	TextColumn<MaterialRequired> viewServiceTaxColumn;
	TextColumn<MaterialRequired> viewVatTaxColumn;
	Column<MaterialRequired,String> discountAmt;
	public  ArrayList<TaxDetails> vattaxlist;
	public  ArrayList<TaxDetails> servicetaxlist;
	public  ArrayList<String> list;
	public  ArrayList<String> vatlist;
	NumberFormat nf=NumberFormat.getFormat("0.00");
	int productIdCount=0;
	int productSrNoCount=0;
	Column<MaterialRequired,String> totalColumn;
	boolean flag = false;
	public ServiceProductTable() {
		
	}

	@Override
	public void createTable() {
		addColumnProductId();
		addColumnProductCode();
		addColumnProductName();
		addColumnProdPrice();
		addColumnProductRequiredQty();
		retriveServiceTax();
				
	}
	@Override
	public void addFieldUpdater() {
		addColumnProductNameUpdater();
		addColumnPriceUpdater();
		updateServiceTaxColumn();
		updateVatTaxColumn();
		createFieldUpdaterDiscAmtColumn();
		//addColumnProductQtyUpdater();
		//if(flag ==true){
			addColumnDeleteUpdater();
		//}
	}	
	@Override
	public void setEnable(boolean state) {
		int tablecolcount = this.table.getColumnCount();
		for (int i = tablecolcount - 1; i > -1; i--)
			table.removeColumn(i);
		if(state == false){
			addColumnProductId();
			addColumnProductCode();
			addColumnProductName();
			addColumnProdPrice();
			addColumnProductRequiredQty();
			retriveServiceTaxForView();
		}
	}
	
	private void addColumnProductQtyUpdater() {
		getColumnProductRequiredQty.setFieldUpdater(new FieldUpdater<MaterialRequired, String>() {
			@Override
			public void update(int index, MaterialRequired object,String value) {
				try {
					double val1 = Double.parseDouble(value.trim());
					object.setProductIssueQty(val1);
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				} catch (NumberFormatException e) {

				}
				table.redrawRow(index);
			}
		});
	}
	private void addColumnPriceUpdater(){
		getColumnProdPrice.setFieldUpdater(new FieldUpdater<MaterialRequired, String>() {
			@Override
			public void update(int index, MaterialRequired object,String value) {
				try {
					double val1 = Double.parseDouble(value.trim());
					object.setProductPrice(val1);
					calculateTotalAmount(object);
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				} catch (NumberFormatException e) {

				}
				table.redrawRow(index);
			}
		});
	}
	private void addColumnProductNameUpdater(){
		getColumnProductName.setFieldUpdater(new FieldUpdater<MaterialRequired, String>() {
			@Override
			public void update(int index, MaterialRequired object,String value) {
				try {
					object.setProductName(value);
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				} catch (NumberFormatException e) {

				}
				table.redrawRow(index);
			}
		});
	}
	private void addColumnProductRequiredQty() {
		//EditTextCell editCell = new EditTextCell();
		getColumnProductRequiredQty=new TextColumn<MaterialRequired> (){
			@Override
			public String getValue(MaterialRequired object) {
				return nf.format(object.getProductIssueQty());
			}
		};
		table.addColumn(getColumnProductRequiredQty, "Qunatity");
		table.setColumnWidth(getColumnProductRequiredQty,100,Unit.PX);
	}
	
	private void addColumnDeleteUpdater() {
		getColumnProductDeleteButton.setFieldUpdater(new FieldUpdater<MaterialRequired, String>() {
			@Override
			public void update(int index, MaterialRequired object,String value) {
				getDataprovider().getList().remove(object);
				table.redrawRow(index);
			}
		});
	}
	
	/****************************************** Editable Column  ***********************************************/
	private void addColumnProductDeleteButton() {
		ButtonCell btnCell = new ButtonCell();
		getColumnProductDeleteButton = new Column<MaterialRequired, String>(btnCell) {
			@Override
			public String getValue(MaterialRequired object) {
				return "Delete";
			}
		};
		table.addColumn(getColumnProductDeleteButton, "Delete");
		table.setColumnWidth(getColumnProductDeleteButton,60,Unit.PX);
	}
	
	private void addColumnProductName() {
		EditTextCell editCell = new EditTextCell();
		getColumnProductName=new Column<MaterialRequired , String>(editCell) {
			@Override
			public String getValue(MaterialRequired object) {
				return object.getProductName();
			}
		};
		table.addColumn(getColumnProductName, "# Name");
		table.setColumnWidth(getColumnProductName,175,Unit.PX);
	}

	private void addColumnProductCode() {
		getColumnProductCode=new TextColumn<MaterialRequired>() {
			@Override
			public String getValue(MaterialRequired object) {
				return object.getProductCode();
			}
		};
		table.addColumn(getColumnProductCode, "Code");
		table.setColumnWidth(getColumnProductCode,90,Unit.PX);
	}

	private void addColumnProductId() {
		getColumnProductId=new TextColumn<MaterialRequired>() {
			@Override
			public String getValue(MaterialRequired object) {
				return object.getProductId()+"";
			}
		};
		table.addColumn(getColumnProductId, "ID");
		table.setColumnWidth(getColumnProductId,130,Unit.PX);
	}
	
	protected void addColumnProdPrice() {
		EditTextCell editCell = new EditTextCell();
		getColumnProdPrice = new Column<MaterialRequired , String>(editCell) {

		@Override
		public String getValue(MaterialRequired object) {
			CreateInvoicePopup createInvoice = new CreateInvoicePopup();
			SuperProduct product=object.getPrduct();
			//product.setPrice(object.getProductPrice());
			product.setServiceTax(object.getServiceTax());
			product.setVatTax(object.getVatTax());
			double tax=createInvoice.removeAllTaxes(product);
			double origPrice=object.getPrice()-tax;		
			//object.setProductPrice(origPrice);
			return nf.format(origPrice)+"";
		}
	};
	table.addColumn(getColumnProdPrice, "# Price");
	table.setColumnWidth(getColumnProdPrice,80,Unit.PX);
 }


	@Override
	public void onClick(ClickEvent event) {
		
	}

	/*****************************************************************************************************************/

	@Override
	public void applyStyle() {
		
	}
	

	@Override
	protected void initializekeyprovider() {
		
	}
	private void retriveServiceTax() {
		final GenricServiceAsync service = GWT.create(GenricService.class);
		MyQuerry query = new MyQuerry();
		Company c=new Company();
		System.out.println("Company Id :: "+c.getCompanyId());
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		query.setFilters(filtervec);
		query.setQuerryObject(new TaxDetails());
		
		service.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
			}		
			public void onSuccess(ArrayList<SuperModel> result){
				
				System.out.println("result size of tax "+result);
				list=new ArrayList<String>();
				servicetaxlist=new ArrayList<TaxDetails>();
				vattaxlist = new ArrayList<TaxDetails>();
				vatlist=new ArrayList<String>();
				List<TaxDetails> backlist=new ArrayList<TaxDetails>();
				
				for (SuperModel model : result) {
					TaxDetails entity = (TaxDetails) model;
					
					backlist.add(entity);
					servicetaxlist.add(entity);
					vattaxlist.add(entity);
				}
				
				for(int i=0;i<backlist.size();i++){
					//  rohan commented this code for loading all the taxes in drop down
//					if(backlist.get(i).getServiceTax().equals(true)){
						list.add(backlist.get(i).getTaxChargeName());
						System.out.println("list size"+list.size());
//					}
//					else if(backlist.get(i).getVatTax().equals(true)){
						vatlist.add(backlist.get(i).getTaxChargeName());
						System.out.println("valist size"+vatlist.size());
//					}
				}
				
				
				
				addEditColumnVatTax();
				addEditColumnServiceTax();
				createColumnDiscountAmt();
				createColumntotalColumn();
				//if(flag ==true){
				addColumnProductDeleteButton();
				//}
				addFieldUpdater();
			}		
		});
	}
	public void addEditColumnVatTax(){
		SelectionCell employeeselection= new SelectionCell(vatlist);
		vatColumn = new Column<MaterialRequired, String>(employeeselection) {
			@Override
			public String getValue(MaterialRequired object) {
				if (object.getVatTax().getTaxConfigName()!= null
						&&!object.getVatTax().getTaxConfigName().equals("")) {
					
					object.setVatTaxEdit(object.getVatTax().getTaxConfigName());

					return object.getVatTax().getTaxConfigName();
				} else
					return "NA";

			}
		};
		table.addColumn(vatColumn, "# Tax 1");
		table.setColumnWidth(vatColumn,130,Unit.PX);
		table.setColumnWidth(vatColumn, 100,Unit.PX);		
	}	
	
	public void updateVatTaxColumn()
	{
		vatColumn.setFieldUpdater(new FieldUpdater<MaterialRequired, String>() {
			
			@Override
			public void update(int index, MaterialRequired object, String value) {
				try {
					String val1 = (value.trim());
					object.setVatTaxEdit(val1);
					Tax tax = object.getVatTax();
					for(int i=0;i<vattaxlist.size();i++)
					{
						if(val1.trim().equals(vattaxlist.get(i).getTaxChargeName())){
							tax.setTaxName(val1);
							tax.setPercentage(vattaxlist.get(i).getTaxChargePercent());
							tax.setTaxPrintName(vattaxlist.get(i).getTaxPrintName());
							tax.setTaxConfigName(val1);
							break;
						}
					}
					object.setVatTax(tax);
					calculateTotalAmount(object);
					productIdCount=object.getPrduct().getCount();
					productSrNoCount = object.getProductSrNo();
					
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				} catch (NumberFormatException e) {
				}
				table.redrawRow(index);
			}
		});
	}
	
	public void addEditColumnServiceTax() {
		SelectionCell employeeselection= new SelectionCell(list);
		serviceColumn = new Column<MaterialRequired, String>(employeeselection) {
			@Override
			public String getValue(MaterialRequired object) {
				if (object.getServiceTax().getTaxConfigName()!= null
						&&!object.getServiceTax().getTaxConfigName().equals("")) {
					
					object.setServiceTaxEdit(object.getServiceTax().getTaxConfigName());

					return object.getServiceTax().getTaxConfigName();
				} else
					return "NA";

			}

			
		};
		table.addColumn(serviceColumn, "# Tax 2");
		table.setColumnWidth(serviceColumn,140,Unit.PX);
		table.setColumnWidth(serviceColumn, 100,Unit.PX);
	}
	
	
	public void updateServiceTaxColumn()
	{
		serviceColumn.setFieldUpdater(new FieldUpdater<MaterialRequired, String>() {
			
			@Override
			public void update(int index, MaterialRequired object, String value) {
				try {
					String val1 = (value.trim());
					object.setServiceTaxEdit(val1);
					Tax tax = object.getServiceTax();
					for(int i=0;i<servicetaxlist.size();i++)
					{
						if(val1.trim().equals(servicetaxlist.get(i).getTaxChargeName())){
							tax.setTaxName(val1);
							tax.setPercentage(servicetaxlist.get(i).getTaxChargePercent());
							/**
							 * rohan added this for GST implementation Date :23-06-2017
							 */
							tax.setTaxPrintName(servicetaxlist.get(i).getTaxPrintName());
							/**
							 * Date : 03-08-2017 By ANIL
							 */
							tax.setTaxConfigName(val1);
							
							/**
							 * End
							 */
							break;
						}
					}
					object.setServiceTax(tax);
					System.out.println("TZZZXXX ="+tax.getTaxPrintName());
					calculateTotalAmount(object);
					productIdCount=object.getPrduct().getCount();
					productSrNoCount = object.getProductSrNo();
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
					
				} catch (NumberFormatException e) {
				}
				table.redrawRow(index);
			}
		});
	}
	private void createColumnDiscountAmt() {
		EditTextCell editCell=new EditTextCell();
		discountAmt=new Column<MaterialRequired,String>(editCell)
				{
			@Override
			public String getValue(MaterialRequired object)
			{
				if(object.getDiscountAmt()==0){
					return 0+"";}
				else{
					return object.getDiscountAmt()+"";}
			}
				};
				table.addColumn(discountAmt,"#Disc Amt");
				table.setColumnWidth(discountAmt, 100,Unit.PX);
	}
	protected void createFieldUpdaterDiscAmtColumn()
	{
		discountAmt.setFieldUpdater(new FieldUpdater<MaterialRequired, String>()
				{
			@Override

			public void update(int index,MaterialRequired object,String value)
			{

				try{
					Double val1=Double.parseDouble(value.trim());
					object.setDiscountAmt(val1);
					
					productIdCount=object.getPrduct().getCount();
					productSrNoCount = object.getProductSrNo();
					calculateTotalAmount(object);
					
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);

				}
				catch (NumberFormatException e)
				{

				}
				table.redrawRow(index);
			}
				});
	}
	public double calculateTotalAmount(MaterialRequired object){	
		CreateInvoicePopup createInvoice = new CreateInvoicePopup();
		double total=0;
		double quantity = 1;
		SuperProduct product=object.getPrduct();
		product.setPrice(object.getProductPrice());
		product.setServiceTax(object.getServiceTax());
		product.setVatTax(object.getVatTax());
		double tax=createInvoice.removeAllTaxes(product);
		//double origPrice=object.getPrice()-tax;
		double origPrice = product.getPrice() - tax;
//		if(object.getProductPrice() != origPrice){
//			origPrice = object.getProductPrice();
//		}
		//double origPrice=object.getPrice()-tax;		
		object.setArea(object.getProductIssueQty()+"");
		GWTCAlert alert = new GWTCAlert();
		//alert.alert("Qunatity : " + object.getProductIssueQty());
		if(object.getDiscountAmt()==0)
		{			
			System.out.println("inside both 0 condition");
			if(!object.getArea().equalsIgnoreCase("NA") ){
				double area = Double.parseDouble(object.getArea());
				//total = object.getProductPrice()*area;
				quantity = area;
				total = origPrice * area;
			}else{
				//total = object.getProductPrice();
				total = origPrice;
			}
			System.out.println("tax value : " + tax);
			if(tax == 0){
				//object.setPrice(total);
				object.setTaxAmount(addAllTaxes(product)* quantity);
			}else{
				object.setTaxAmount(tax*quantity);
			}
		}
		
		else if(object.getDiscountAmt()!=0){
			System.out.println("total amount before area calculation =="+total);
			if(!object.getArea().equalsIgnoreCase("NA")){
				double area = Double.parseDouble(object.getArea());
				total=origPrice*area;
				quantity = area;
				//total = total - (total*object.getPercentageDiscount()/100);
				total = total - object.getDiscountAmt();
			}else{
				total=origPrice;
				//total=total-(total*object.getPercentageDiscount()/100);
				total=total-object.getDiscountAmt();
			}
			System.out.println("tax value : " + tax);
			//if(tax == 0){
				//object.setPrice(total);
				//total = total + addAllTaxes(product);
				object.setTaxAmount(addAllTaxes(product)*quantity);
			//}
		}
		object.setTotalAmount(total);
		return total;
		}
//	public double removeAllTaxes(SuperProduct entity)
//	{
//		double vat = 0,service = 0;
//		double tax=0,retrVat=0,retrServ=0;
//		if(entity instanceof ServiceProduct)
//		{
//			ServiceProduct prod=(ServiceProduct) entity;
//			if(prod.getServiceTax()!=null&&prod.getServiceTax().isInclusive()==true){
//				service=prod.getServiceTax().getPercentage();
//			}
//			if(prod.getVatTax()!=null&&prod.getVatTax().isInclusive()==true){
//				vat=prod.getVatTax().getPercentage();
//			}
//		}
//
//		if(entity instanceof ItemProduct)
//		{
//			ItemProduct prod=(ItemProduct) entity;
//			if(prod.getVatTax()!=null&&prod.getVatTax().isInclusive()==true){
//				vat=prod.getVatTax().getPercentage();
//			}
//			if(prod.getServiceTax()!=null&&prod.getServiceTax().isInclusive()==true){
//				service=prod.getServiceTax().getPercentage();
//			}
//		}
//		
//		if(vat!=0&&service==0){
//			retrVat=entity.getPrice()/(1+(vat/100));
//			retrVat=entity.getPrice()-retrVat;
//		}
//		if(service!=0&&vat==0){
//			retrServ=entity.getPrice()/(1+service/100);
//			retrServ=entity.getPrice()-retrServ;
//		}
//		if(service!=0&&vat!=0){
//			if(entity instanceof ItemProduct)
//			{
//				ItemProduct prod=(ItemProduct) entity;
//				if(prod.getServiceTax().getTaxPrintName()!=null && ! prod.getServiceTax().getTaxPrintName().equals("")
//				   && prod.getVatTax().getTaxPrintName()!=null && ! prod.getVatTax().getTaxPrintName().equals(""))
//				{
//
//					double dot = service + vat;
//					retrServ=(entity.getPrice()/(1+dot/100));
//					retrServ=entity.getPrice()-retrServ;
////					retrVat=0;
//
//				}
//				else{
//					// Here if both are inclusive then first remove service tax and then on that amount
//					// calculate vat.
//					double removeServiceTax=(entity.getPrice()/(1+service/100));
//					//double taxPerc=service+vat;
//					//retrServ=(entity.getPrice()/(1+taxPerc/100)); //line changed below
//					retrServ=(removeServiceTax/(1+vat/100));
//					retrServ=entity.getPrice()-retrServ;
//				}
//			}
//			
//			if(entity instanceof ServiceProduct)
//			{
//				ServiceProduct prod=(ServiceProduct) entity;
//				if(prod.getServiceTax().getTaxPrintName()!=null && ! prod.getServiceTax().getTaxPrintName().equals("")
//				   && prod.getVatTax().getTaxPrintName()!=null && ! prod.getVatTax().getTaxPrintName().equals(""))
//				{
//					double dot = service + vat;
//					retrServ=(entity.getPrice()/(1+dot/100));
//					retrServ=entity.getPrice()-retrServ;
//				}
//				else
//				{
//					// Here if both are inclusive then first remove service tax and then on that amount
//					// calculate vat.
//					double removeServiceTax=(entity.getPrice()/(1+service/100));
//					//double taxPerc=service+vat;
//					//retrServ=(entity.getPrice()/(1+taxPerc/100)); //line changed below
//					retrServ=(removeServiceTax/(1+vat/100));
//					retrServ=entity.getPrice()-retrServ;
//				}
//			}
//		}
//		tax=retrVat+retrServ;
//		return tax;
//	}
	public double addAllTaxes(SuperProduct entity)
	{

		double vat = 0,service = 0;
		double tax=0,retrVat=0,retrServ=0;
		if(entity instanceof ServiceProduct)
		{
			ServiceProduct prod=(ServiceProduct) entity;
			if(prod.getServiceTax()!=null){
				service=prod.getServiceTax().getPercentage();
			}
			if(prod.getVatTax()!=null){
				vat=prod.getVatTax().getPercentage();
			}
		}

		if(entity instanceof ItemProduct)
		{
			ItemProduct prod=(ItemProduct) entity;
			if(prod.getVatTax()!=null){
				vat=prod.getVatTax().getPercentage();
			}
			if(prod.getServiceTax()!=null){
				service=prod.getServiceTax().getPercentage();
			}
		}
		
		if(vat!=0&&service==0){
			retrVat=entity.getPrice()*vat/100;
			//retrVat=entity.getPrice()+retrVat;
		}
		if(service!=0&&vat==0){
			retrServ=entity.getPrice()*service/100;
			//retrServ=entity.getPrice()+retrServ;
		}
		if(service!=0&&vat!=0){			
			if(entity instanceof ItemProduct)
			{
				ItemProduct prod=(ItemProduct) entity;
				if(prod.getServiceTax().getTaxPrintName()!=null && ! prod.getServiceTax().getTaxPrintName().equals("")
				   && prod.getVatTax().getTaxPrintName()!=null && ! prod.getVatTax().getTaxPrintName().equals(""))
				{

					double dot = service + vat;
					retrServ=(entity.getPrice()*dot/100);
					//retrServ=entity.getPrice()+retrServ;
				}

			}
			
			if(entity instanceof ServiceProduct)
			{
				ServiceProduct prod=(ServiceProduct) entity;
				if(prod.getServiceTax().getTaxPrintName()!=null && ! prod.getServiceTax().getTaxPrintName().equals("")
				   && prod.getVatTax().getTaxPrintName()!=null && ! prod.getVatTax().getTaxPrintName().equals(""))
				{
					double dot = service + vat;
					retrServ=(entity.getPrice()*dot/100);
					//retrServ=entity.getPrice()+retrServ;
				}
				
			}
		}
		tax=retrVat+retrServ;
		return tax;
	
	}
	protected void createColumntotalColumn(){
	TextCell editCell=new TextCell();
	totalColumn=new Column<MaterialRequired,String>(editCell)
			{
		@Override
		public String getValue(MaterialRequired object)
		{
//			CreateInvoicePopup createInvoice = new CreateInvoicePopup();
//			double total=0;
//			SuperProduct product=object.getPrduct();
//			double tax=createInvoice.removeAllTaxes(product);
//			double origPrice=object.getPrice()-tax;	
//			if(object.getTotalAmount()!=0){
				return nf.format(calculateTotalAmount(object));
//			}else{
//				return nf.format(calculateTotalAmount(object));
//			}
			//object.setTotalAmount(origPrice);
			//return nf.format(origPrice)+"";
		}
			};
			table.addColumn(totalColumn,"Total");
			table.setColumnWidth(totalColumn, 100,Unit.PX);
	}
	private void retriveServiceTaxForView() {
		final GenricServiceAsync service = GWT.create(GenricService.class);
		MyQuerry query = new MyQuerry();
		Company c=new Company();
		System.out.println("Company Id :: "+c.getCompanyId());
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		query.setFilters(filtervec);
		query.setQuerryObject(new TaxDetails());
		
		service.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
			}		
			public void onSuccess(ArrayList<SuperModel> result){
				
				System.out.println("result size of tax "+result);
				list=new ArrayList<String>();
				servicetaxlist=new ArrayList<TaxDetails>();
				vattaxlist = new ArrayList<TaxDetails>();
				vatlist=new ArrayList<String>();
				List<TaxDetails> backlist=new ArrayList<TaxDetails>();
				
				for (SuperModel model : result) {
					TaxDetails entity = (TaxDetails) model;
					
					backlist.add(entity);
					servicetaxlist.add(entity);
					vattaxlist.add(entity);
				}
				
				for(int i=0;i<backlist.size();i++){
					//  rohan commented this code for loading all the taxes in drop down
//					if(backlist.get(i).getServiceTax().equals(true)){
						list.add(backlist.get(i).getTaxChargeName());
						System.out.println("list size"+list.size());
//					}
//					else if(backlist.get(i).getVatTax().equals(true)){
						vatlist.add(backlist.get(i).getTaxChargeName());
						System.out.println("valist size"+vatlist.size());
//					}
				}
				
				
				
				addEditColumnVatTax();
				addEditColumnServiceTax();
				createColumnDiscountAmt();
				createColumntotalColumn();
				//if(flag ==true){
				//addColumnProductDeleteButton();
				//}
				//addFieldUpdater();
				addColumnProductNameUpdater();
				addColumnPriceUpdater();
				updateServiceTaxColumn();
				updateVatTaxColumn();
				createFieldUpdaterDiscAmtColumn();
			}		
		});
	}
}



