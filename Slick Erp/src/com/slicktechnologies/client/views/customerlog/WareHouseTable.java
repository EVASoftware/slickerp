package com.slicktechnologies.client.views.customerlog;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.apache.poi.util.SystemOutLogger;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.thirdparty.javascript.rhino.head.ast.FunctionNode.Form;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.approvalutility.ApprovableScreen;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.reusabledata.ConditionDialogBox;
import com.slicktechnologies.client.services.EmailService;
import com.slicktechnologies.client.services.EmailServiceAsync;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.ApproverFactory;
import com.slicktechnologies.client.views.approval.ApprovalService;
import com.slicktechnologies.client.views.approval.ApprovalServiceAsync;
import com.slicktechnologies.client.views.customerlog.CustomerLogDetailsPresenter.CustomerLogDetailsPresenterTable;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.common.businessprocesslayer.ApprovableProcess;
import com.slicktechnologies.shared.common.businessprocesslayer.ConcreteBusinessProcess;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerlog.CustomerLogDetails;
import com.slicktechnologies.shared.common.customerlog.MaterialRequired;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.inventory.MaterialMovementNote;
import com.slicktechnologies.shared.common.inventory.MaterialProduct;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;
import com.slicktechnologies.shared.common.inventory.Storagebin;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.personlayer.Employee;

public class WareHouseTable extends CustomerLogDetailsPresenterTable implements ClickHandler {
	TextColumn<CustomerLogDetails> getColumnId;
	TextColumn<CustomerLogDetails> getComplaintNumber;
	TextColumn<CustomerLogDetails> getCallType;
	TextColumn<CustomerLogDetails> getStatus;
	TextColumn<CustomerLogDetails> getProductGroup;
	TextColumn<CustomerLogDetails> getProduct;
	TextColumn<CustomerLogDetails> getTechnician;
	TextColumn<CustomerLogDetails> getAppointmentDate;
	TextColumn<CustomerLogDetails> getCreationDate;
	TextColumn<CustomerLogDetails> getCallBackStatus;
	TextColumn<CustomerLogDetails> getWarrantyStatus;
	TextColumn<CustomerLogDetails> getColumnCustomerName;
	Column<CustomerLogDetails , String> getMaterialIssueButton;
	Column<CustomerLogDetails , String> getMaterialReturnButton;
	Column<CustomerLogDetails, Boolean> checkRecord;
	MaterialPopup materialIssuePopup = new MaterialPopup();
	MaterialPopup materialReturnPopup = new MaterialPopup();
	CustomerLogDetails customerLogDetails;
	GenricServiceAsync service = GWT.create(GenricService.class);
	GeneralServiceAsync generalService = GWT.create(GeneralService.class);
	ApprovalServiceAsync approvalService=GWT.create(ApprovalService.class);
	int rowIndex;
	GWTCAlert alert = new GWTCAlert();
	TextColumn<MaterialProduct> warehouseViewColumn;
	Column<MaterialProduct, String> columnviewBin;
	TextColumn<MaterialProduct> columnviewLoc;
	boolean issueFlag = false;
	ConditionDialogBox conditionPopup = new ConditionDialogBox("Do you want to complete call? ", AppConstants.YES, AppConstants.NO);
//	PopupPanel panel = new PopupPanel(); //commented by Ashwini
	PopupPanel panel;//Added by Ashwini
	
	/** date 18.12.2018 added by komal to reissue material **/
	MaterialReIssuePopup materialReIssuePopup = new MaterialReIssuePopup();
	public WareHouseTable()
	{
		super();
		getTable().setHeight("500px");
		getTable().setWidth("1125px");
		materialIssuePopup.getLblOk().addClickHandler(this);
		materialIssuePopup.getLblCancel().addClickHandler(this);
		materialReturnPopup.getLblCancel().addClickHandler(this);
		materialReturnPopup.getLblOk().addClickHandler(this);
		conditionPopup.getBtnOne().addClickHandler(this);
		conditionPopup.getBtnTwo().addClickHandler(this);
		/** date 18.12.2018 added by komal to reissue material **/
		materialReIssuePopup.getLblCancel().addClickHandler(this);
		materialReIssuePopup.getLblOk().addClickHandler(this);
		
	}
	@Override
	public void createTable() {
		//addColumnCheckBox();
		//addColumngetId();
		addColumngetCreationDate();
		addColumngetMaterialIssueButton();
		addColumngetMaterialReturnButton();
		addColumnCustomerName();
		addColumngetComplaintNumber();
		addColumngetStatus();
		//addColumngetProductGroup();
		addColumngetProduct();
		addColumngetTechnician();
		addColumngetCallType();
		addColumngetWarratyStatus();
		addColumngetCallBackStatus();
		addColumngetAppointmentDate();
		if(LoginPresenter.myUserEntity.getRole().getRoleName().trim().equalsIgnoreCase("WareHouse") || LoginPresenter.myUserEntity.getRole().getRoleName().trim().equalsIgnoreCase("Admin")) {
			addFieldUpdater();
		}
		addColumnSorting();
	}
	protected void addColumngetId(){
		getColumnId = new TextColumn<CustomerLogDetails>() {
			@Override
			public String getValue(CustomerLogDetails object) {
					return object.getCount() + "";
			}
		};
		table.addColumn(getColumnId, "Id");
		addSortinggetID();
		getColumnId.setSortable(true);
	    table.setColumnWidth(getColumnId,"90px");
	}
	
    protected void addColumngetComplaintNumber(){
    	getComplaintNumber = new TextColumn<CustomerLogDetails>() {
			@Override
			public String getValue(CustomerLogDetails object) {
				if(object.getComplaintNumber() == null){
					return "";
				}else{
					return object.getComplaintNumber()+ "";
				}
			}
		};
		table.addColumn(getComplaintNumber, "Complaint Number");
		getComplaintNumber.setSortable(true);
		table.setColumnWidth(getComplaintNumber,"90px");
    }
  
    protected void addColumngetStatus(){
    	getStatus = new TextColumn<CustomerLogDetails>() {
			@Override
			public String getValue(CustomerLogDetails object) {
					return object.getStatus();
			}
		};
		table.addColumn(getStatus, "Status");
		getStatus.setSortable(true);
		table.setColumnWidth(getStatus,"90px");
    }
  
//    protected void addColumngetProductGroup(){
//    	getProductGroup = new TextColumn<CustomerLogDetails>() {
//			@Override
//			public String getValue(CustomerLogDetails object) {
//					return object.getProductGroup();
//			}
//		};
//		table.addColumn(getProductGroup, "Product Group");
//		getProductGroup.setSortable(true);
//		table.setColumnWidth(getProductGroup,"90px");
//    }
    /**
     * Date 21-6-2018
     * by jayshree 
     */
    protected void addColumngetProduct(){
    	getProduct = new TextColumn<CustomerLogDetails>() {
			@Override
			public String getValue(CustomerLogDetails object) {
				if(object.getServiceCharges().size()>0){
					return object.getServiceCharges().get(0).getProductName();
				}
					return "";
			}
		};
		table.addColumn(getProduct, "Product");
		getProduct.setSortable(true);
		table.setColumnWidth(getProduct,"90px");
    }
    //end by jayshree
  
    protected void addColumngetAppointmentDate(){
    	getAppointmentDate = new TextColumn<CustomerLogDetails>() {
        	@Override
    		public String getValue(CustomerLogDetails object)
    		{
    			if(object.getAppointmentDate()!=null){
    				return AppUtility.parseDate(object.getAppointmentDate());}
    			else{
    				return "";
    			}
    		}
    			};
    			table.addColumn(getAppointmentDate,"Appointment Date");
    			getAppointmentDate.setSortable(true);
    			table.setColumnWidth(getAppointmentDate,"90px");
    }

    protected void addColumngetTechnician(){
    	getTechnician = new TextColumn<CustomerLogDetails>() {
			@Override
			public String getValue(CustomerLogDetails object) {
					return object.getTechnician();
			}
		};
		table.addColumn(getTechnician, "Technician");
		getTechnician.setSortable(true);
		table.setColumnWidth(getTechnician,"90px");
    }
    protected void addColumngetCallBackStatus(){
    	getCallBackStatus = new TextColumn<CustomerLogDetails>() {
			@Override
			public String getValue(CustomerLogDetails object) {
					return object.getCallbackStatus();
			}
		};
		table.addColumn(getCallBackStatus, "CallBack Status");
		getTechnician.setSortable(true);
		table.setColumnWidth(getCallBackStatus,"90px");
    }
    protected void addColumngetWarratyStatus(){
    	getWarrantyStatus = new TextColumn<CustomerLogDetails>() {
			@Override
			public String getValue(CustomerLogDetails object) {
					return object.getWarrantyStatus();
			}
		};
		table.addColumn(getWarrantyStatus, "Warranty Staus");
		getWarrantyStatus.setSortable(true);
		table.setColumnWidth(getWarrantyStatus,"90px");
    }
    private void addColumngetMaterialIssueButton() {

		ButtonCell btnCell = new ButtonCell();
		getMaterialIssueButton = new Column<CustomerLogDetails, String>(btnCell) {

			@Override
			public String getValue(CustomerLogDetails object) {
				// TODO Auto-generated method stub
				return "Issue";
			}
		};

		table.addColumn(getMaterialIssueButton, "Issue");
		table.setColumnWidth(getMaterialIssueButton, 90, Unit.PX);
	}

	private void updateColumngetMaterialIssueButton() {
		getMaterialIssueButton.setFieldUpdater(new FieldUpdater<CustomerLogDetails, String>() {
			@Override
			public void update(int index, CustomerLogDetails object, String value) {
					System.out.println("Object onclick event :"+ object);
					if(object.getStatus().equalsIgnoreCase(CustomerLogDetails.MATERIALPENDNG)){						
					materialIssuePopup.getDefectiveMaterialTable().setEnable(false);
					materialIssuePopup.getDefectiveProductInfoComposite().setEnable(false);
					materialIssuePopup.btnAdd1.setEnabled(false);
					materialIssuePopup.showPopUp();
					if(object.getStatus().equalsIgnoreCase(CustomerLogDetails.MATERIAISSUED) || object.getStatus().equalsIgnoreCase(CustomerLogDetails.COMPLETED)){
						materialIssuePopup.setEnable(false);
					}else{
						materialIssuePopup.setEnable(true);
					}
					materialIssuePopup.getTbId().setValue(object.getCount()+"");
					materialIssuePopup.getTbCustomerName().setValue(object.getCinfo().getFullName());
					materialIssuePopup.getTbTechnicianName().setValue(object.getTechnician());
					materialIssuePopup.getTbCompanyName().setValue(object.getCompanyName());
//					if(object.getProduct()!=null){
//						materialIssuePopup.getOlbProduct().setValue(object.getProduct());
//					}
					if(object.getServiceCharges().size()>0){
						materialIssuePopup.getTbProductName().setValue(object.getServiceCharges().get(0).getProductName());
					}
					if(object.getModelNo()!=null){
						materialIssuePopup.getTbModelNo().setValue(object.getModelNo());
					}
					materialIssuePopup.materialIssueTable.clear();
//					if(object.getIssueMaterial() !=null){
//						materialIssuePopup.getMaterialIssueTable().getDataprovider().setList(object.getIssueMaterial());
//					}
					if(object.getWarrantyStatus().equalsIgnoreCase("Warranty") || object.getWarrantyStatus().equalsIgnoreCase("AMC")){
						materialIssuePopup.getMaterialIssueTable().warrantyFlag =true;
					}
					if(object.getToWarehouse()!=null ){
						materialIssuePopup.getOlbToWarehouse().setValue(object.getToWarehouse());
					}
					if(object.getToLocation()!=null ){
						materialIssuePopup.getOlbToLoc().clear();
						materialIssuePopup.getOlbToLoc().addItem("--SELECT--");
						materialIssuePopup.getOlbToLoc().addItem(object.getToLocation());
						materialIssuePopup.getOlbToLoc().setSelectedIndex(1);
						//materialIssuePopup.getOlbToLoc().setValue(object.getToLocation());
					}
					if(object.getToBin() != null){
						materialIssuePopup.getOlbToBin().clear();
						materialIssuePopup.getOlbToBin().addItem("--SELECT--");
						materialIssuePopup.getOlbToBin().addItem(object.getToBin());
						materialIssuePopup.getOlbToBin().setSelectedIndex(1);
						//materialIssuePopup.getOlbToBin().setValue(object.getToBin());
					}
					materialIssuePopup.getOlbWarrantyStatus().setValue(object.getWarrantyStatus());
					//materialIssuePopup.getDefectiveMaterialTable().setValue(object.getDefectiveMaterial());
					System.out.println("value:"+object.isIssueFlag());
					rowIndex = index;
					customerLogDetails = object;
					boolean val = customerLogDetails.isIssueFlag();
					System.out.println("value:"+val);
					//materialIssuePopup.getMaterialIssueTable().getDataprovider().setList(customerLogDetails.getIssueMaterial());
					materialIssuePopup.setCustomerLogObj(customerLogDetails);
					rowIndex = index;
					//getCallLogDetails(customerLogDetails);
//				}
			 }else if(object.getStatus().equalsIgnoreCase(CustomerLogDetails.MATERIAISSUED)){
					
				
				materialReIssuePopup.showPopUp();
				materialReIssuePopup.getTbId().setValue(object.getCount()+"");
				materialReIssuePopup.getTbCustomerName().setValue(object.getCinfo().getFullName());
				materialReIssuePopup.getTbTechnicianName().setValue(object.getTechnician());
				materialReIssuePopup.getTbCompanyName().setValue(object.getCompanyName());
//				if(object.getProduct()!=null){
//					materialIssuePopup.getOlbProduct().setValue(object.getProduct());
//				}
				if(object.getServiceCharges().size()>0){
					materialReIssuePopup.getTbProductName().setValue(object.getServiceCharges().get(0).getProductName());
				}
				if(object.getModelNo()!=null){
					materialReIssuePopup.getTbModelNo().setValue(object.getModelNo());
				}
				materialReIssuePopup.materialIssueTable.clear();
//				if(object.getIssueMaterial() !=null){
//					materialIssuePopup.getMaterialIssueTable().getDataprovider().setList(object.getIssueMaterial());
//				}
				if(object.getWarrantyStatus().equalsIgnoreCase("Warranty") || object.getWarrantyStatus().equalsIgnoreCase("AMC")){
					materialReIssuePopup.getMaterialIssueTable().warrantyFlag =true;
				}
				if(object.getToWarehouse()!=null ){
					materialReIssuePopup.getOlbToWarehouse().setValue(object.getToWarehouse());
				}
				if(object.getToLocation()!=null ){
					materialReIssuePopup.getOlbToLoc().clear();
					materialReIssuePopup.getOlbToLoc().addItem("--SELECT--");
					materialReIssuePopup.getOlbToLoc().addItem(object.getToLocation());
					materialReIssuePopup.getOlbToLoc().setSelectedIndex(1);
					//materialIssuePopup.getOlbToLoc().setValue(object.getToLocation());
				}
				if(object.getToBin() != null){
					materialReIssuePopup.getOlbToBin().clear();
					materialReIssuePopup.getOlbToBin().addItem("--SELECT--");
					materialReIssuePopup.getOlbToBin().addItem(object.getToBin());
					materialReIssuePopup.getOlbToBin().setSelectedIndex(1);
					//materialIssuePopup.getOlbToBin().setValue(object.getToBin());
				}
				materialReIssuePopup.getOlbWarrantyStatus().setValue(object.getWarrantyStatus());
				//materialIssuePopup.getDefectiveMaterialTable().setValue(object.getDefectiveMaterial());
				System.out.println("value:"+object.isIssueFlag());
				rowIndex = index;
				customerLogDetails = object;
				boolean val = customerLogDetails.isIssueFlag();
				System.out.println("value:"+val);
				//materialIssuePopup.getMaterialIssueTable().getDataprovider().setList(customerLogDetails.getIssueMaterial());
				materialReIssuePopup.setCustomerLogObj(customerLogDetails);
				rowIndex = index;
				//getCallLogDetails(customerLogDetails);
//			}
		 
			 }else {
				 GWTCAlert alert = new GWTCAlert();
				 alert.alert("Material is not pending for this call log");
			 }
			}
		});
	}
	
	 private void addColumngetMaterialReturnButton() {
			ButtonCell btnCell = new ButtonCell();
			getMaterialReturnButton = new Column<CustomerLogDetails, String>(btnCell) {

				@Override
				public String getValue(CustomerLogDetails object) {
					// TODO Auto-generated method stub
					return "Return";
				}
			};

			table.addColumn(getMaterialReturnButton, "Return");
			table.setColumnWidth(getMaterialReturnButton, 90, Unit.PX);
		}

		private void updateColumngetMaterialReturnButton() {
			getMaterialReturnButton.setFieldUpdater(new FieldUpdater<CustomerLogDetails, String>() {
				@Override
				public void update(int index, CustomerLogDetails object, String value) {
						System.out.println("Object onclick event :"+ object);
						if(object.getStatus().equalsIgnoreCase(CustomerLogDetails.MATERIAISSUED)){
						materialReturnPopup.showPopUp();
//						if(object.getStatus().equalsIgnoreCase(CustomerLogDetails.COMPLETED)){
//						materialReturnPopup.setEnable(false);
//						}
						materialReturnPopup.getMaterialIssueTable().setEnable(false);
						materialReturnPopup.materialIssueTable.clear();
						materialReturnPopup.DefectiveMaterialTable.clear();
						materialReturnPopup.getOlbToWarehouse().setEnabled(false);
						materialReturnPopup.getOlbToLoc().setEnabled(false);
						materialReturnPopup.getOlbToBin().setEnabled(false);
						materialReturnPopup.getTbId().setValue(object.getCount()+"");
						materialReturnPopup.getTbCustomerName().setValue(object.getCinfo().getFullName());
						materialReturnPopup.getTbTechnicianName().setValue(object.getTechnician());
						materialReturnPopup.getTbCompanyName().setValue(object.getCompanyName());
						materialReturnPopup.getOlbWarrantyStatus().setValue(object.getWarrantyStatus());
						materialReturnPopup.getMaterialIssueTable().setValue(object.getIssueMaterial());
						materialReturnPopup.getDefectiveMaterialTable().setValue(object.getDefectiveMaterial());
						materialReturnPopup.getOlbToWarehouse().setValue(object.getToWarehouse());
						materialReturnPopup.getOlbToLoc().clear();
						materialReturnPopup.getOlbToLoc().addItem("--SELECT--");
						materialReturnPopup.getOlbToLoc().addItem(object.getToLocation());
						materialReturnPopup.getOlbToLoc().setSelectedIndex(1);
						materialReturnPopup.getOlbToBin().clear();
						materialReturnPopup.getOlbToBin().addItem("--SELECT--");
						materialReturnPopup.getOlbToBin().addItem(object.getToBin());
						materialReturnPopup.getOlbToBin().setSelectedIndex(1);
//						if(object.getProduct()!=null){
//							materialReturnPopup.getOlbProduct().setValue(object.getProduct());
//						}
						if(object.getServiceCharges().size()>0){
							materialReturnPopup.getTbProductName().setValue(object.getServiceCharges().get(0).getProductName());
						}
						if(object.getModelNo()!=null){
							materialReturnPopup.getTbModelNo().setValue(object.getModelNo());
						}
						System.out.println(object.isIssueFlag());
						rowIndex = index;
						customerLogDetails = object;
						System.out.println(customerLogDetails.isIssueFlag());
						//customerLogDetails.setMaterialFlag(true);
						//materialReturnPopup.getMaterialIssueTable().getDataprovider().setList(customerLogDetails.getIssueMaterial());
						materialReturnPopup.setCustomerLogObj(customerLogDetails);
						rowIndex = index;
						//getCallLogDetails(customerLogDetails);
//					}
				 }else{
						GWTCAlert alert = new GWTCAlert();
						alert.alert("Material is not issued for this call log");
					}
				}
			});
		}
	private void setFieldUpdateOnCheckbox()
	{
		System.out.println("In Field Updator");
		checkRecord.setFieldUpdater(new FieldUpdater<CustomerLogDetails, Boolean>() {
			@Override
			public void update(int index, CustomerLogDetails object, Boolean value) {
				try {
					object.setRecordSelect(value);
				} catch (Exception e) {
				}
				table.redrawRow(index);
			}
		});
	}
	protected void addColumnCustomerName(){
		getColumnCustomerName = new TextColumn<CustomerLogDetails>() {
			@Override
			public String getValue(CustomerLogDetails object) {
					return object.getCinfo().getFullName();
			}
		};
		table.addColumn(getColumnCustomerName, "Customer Name");
		getColumnCustomerName.setSortable(true);
	    table.setColumnWidth(getColumnCustomerName,"120px");
	}
	 protected void addColumngetCreationDate(){
		 getCreationDate = new TextColumn<CustomerLogDetails>() {
	        	@Override
	    		public String getValue(CustomerLogDetails object)
	    		{
	    			if(object.getCreatedOn()!=null){
	    				return AppUtility.parseDate(object.getCreatedOn());}
	    			else{
	    				return "";
	    			}
	    		}
	    			};
	    			table.addColumn(getCreationDate,"Creation Date");
	    			getCreationDate.setSortable(true);
	    			table.setColumnWidth(getCreationDate,"90px");
	    }
	private void addColumnCheckBox() {
		CheckboxCell checkCell = new CheckboxCell();
		checkRecord = new Column<CustomerLogDetails, Boolean>(checkCell) {

			@Override
			public Boolean getValue(CustomerLogDetails object) {

				return object.getRecordSelect();
			}
		};
		table.addColumn(checkRecord, "Select");
		table.setColumnWidth(checkRecord, 60, Unit.PX);

	}

	@Override
	public void setEnable(boolean state)
	{
		if(state == false){
			addColumngetId();
			addColumngetMaterialIssueButton();
			addColumngetMaterialReturnButton();
			addColumngetComplaintNumber();
			addColumngetStatus();
			//addColumngetProductGroup();
			addColumngetProduct();
			addColumngetTechnician();
			addColumngetAppointmentDate();
			addColumngetCallBackStatus();
			addColumngetWarratyStatus();
		}
	}
	@Override
	public void applyStyle()
	{
	}
	public void addColumnSorting(){
		//addSortinggetID();
		addSortinggetCreationDate();	
		addSortinggetCustomerName();
		addSortinggetComplaintNumber();
		addSortinggetStatus();
		addSortinggetProduct();	
		addSortinggetTechnician();
		addSortinggetCallType();
		addSortinggetWarrantyStatus();
		addSortinggetCallBackStatus();
		addSortinggetAppointmentDate();
		
	}
	private void addSortinggetID() {

		List<CustomerLogDetails> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerLogDetails>(list);
		columnSort.setComparator(getColumnId, new Comparator<CustomerLogDetails>()
				{
			@Override
			public int compare(CustomerLogDetails e1,CustomerLogDetails e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCinfo().getCount()== e2.getCinfo().getCount()){
						return 0;}
					if(e1.getCinfo().getCount()> e2.getCinfo().getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
		
		
	}
	
	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		//setFieldUpdateOnCheckbox();
		updateColumngetMaterialIssueButton();
		updateColumngetMaterialReturnButton();
	}
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		if(event.getSource().equals(materialIssuePopup.getLblOk())){
			final List<MaterialRequired> issueList = new ArrayList<MaterialRequired>(); 
			List<MaterialRequired> defectiveList = new ArrayList<MaterialRequired>();
			if(materialIssuePopup.validate()){
//			if(customerLogDetails.isIssueFlag()){
//				materialIssuePopup.getMaterialIssueTable().setEnable(false);
//				alert.alert("Material is already issued for this call log");
//			}
			if(materialIssuePopup.getMaterialIssueTable().getDataprovider().getList().size()!=0 && !(customerLogDetails.isIssueFlag())){
				issueList.addAll(materialIssuePopup.getMaterialIssueTable().getDataprovider().getList());
				//defectiveList.addAll(materialIssuePopup.getDefectiveMaterialTable().getDataprovider().getList());		
				//customerLogDetails.setDefectiveMaterial(defectiveList);	
				
				
				/**
				 * Date 10-09-2018 By Vijay
				 * Des :- added product inventory master defined or not validation for TransferTo warehouse
				 */
				Company c = new Company();
				List<MaterialRequired> materilList = materialIssuePopup.getMaterialIssueTable().getDataprovider().getList();
				ArrayList<Integer> integerList = new ArrayList<Integer>();
				for(MaterialRequired prodId : materilList){
					integerList.add(prodId.getProductId());
				}
				generalService.getProductInventoryDetails(integerList, c.getCompanyId(), new AsyncCallback<ArrayList<SuperModel>>() {
					
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						// TODO Auto-generated method stub
						if(result!=null){
						String transferToWarehouseName = materialIssuePopup.olbToWarehouse.getValue(materialIssuePopup.olbToWarehouse.getSelectedIndex());
						String transferToLocationName = materialIssuePopup.olbToLoc.getValue(materialIssuePopup.olbToLoc.getSelectedIndex());
						String transferToBinName = materialIssuePopup.olbToBin.getValue(materialIssuePopup.olbToBin.getSelectedIndex());

						if(result.size()==0){
							alert.alert("Please Define Product Inventory Master First!");
						}
						boolean flag = false;
						for(SuperModel model : result){
							ProductInventoryView prodInventory = (ProductInventoryView) model;
							for(ProductInventoryViewDetails productDetails: prodInventory.getDetails()){
								if(productDetails.getWarehousename().trim().equals(transferToWarehouseName.trim()) &&
									productDetails.getStoragelocation().trim().equals(transferToLocationName.trim()) &&
									productDetails.getStoragebin().trim().equals(transferToBinName.trim()) ){
									flag =true;
								}
							}
						}
						if(flag==false){
							alert.alert("Please Define Product Inventory Master for TransferTo Warehouse!");
							return;
						}
						/**
						 * Date 10-09-2018 By Vijay
						 * Des :- old code added here after validation check
						 */
						if(result.size()!=0){
							if (validateQuantity(materialIssuePopup.getMaterialIssueTable()
									.getDataprovider().getList())) {
								customerLogDetails.setStatus(CustomerLogDetails.ONHOLD);
								saveLog(customerLogDetails);
								alert.alert("Insufficient available quantity or select warehouse details");
							} else {
								customerLogDetails.setIssueMaterial(issueList);
								customerLogDetails.setToWarehouse(materialIssuePopup.getOlbToWarehouse().getValue());
								customerLogDetails.setToLocation(materialIssuePopup.getOlbToLoc().getValue(materialIssuePopup.getOlbToLoc().getSelectedIndex()));
								customerLogDetails.setToBin(materialIssuePopup.getOlbToBin().getValue(materialIssuePopup.getOlbToBin().getSelectedIndex()));
								customerLogDetails.setStatus(CustomerLogDetails.MATERIAISSUED);
								customerLogDetails.setIssueFlag(true);
								createMMNForIssueMaterial();
								saveLog(customerLogDetails);
							}	
						}
						
						}else{
							alert.alert("Please Define Product Inventory Master for TransferTo Warehouse!");
							return;
						}
						
					}
					
					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						
					}
				});
				
								
		      }
			 }
			}
		
		if (event.getSource().equals(materialIssuePopup.getLblCancel())) {
			materialIssuePopup.getMaterialIssueTable().clear();
			materialIssuePopup.getDefectiveMaterialTable().clear();
			materialIssuePopup.hidePopUp();
		}
		if(event.getSource().equals(materialReturnPopup.getLblOk())){
			panel = new PopupPanel(true);//added by ashwini
			panel.add(conditionPopup);
			panel.center();
			panel.show();
		}
		if(event.getSource().equals(conditionPopup.getBtnOne())){
			panel.hide();
			saveOnReturnMaterial(true , customerLogDetails.COMPLETED);
			
		}
		if(event.getSource().equals(conditionPopup.getBtnTwo())){
			panel.hide();
			saveOnReturnMaterial(false , customerLogDetails.CREATED);
			
		}
		if (event.getSource().equals(materialReturnPopup.getLblCancel())) {
			materialReturnPopup.getMaterialIssueTable().clear();
			materialReturnPopup.getDefectiveMaterialTable().clear();
			materialReturnPopup.hidePopUp();
		}
		
		if (event.getSource().equals(materialReIssuePopup.getLblCancel())) {
			materialReIssuePopup.getMaterialIssueTable().clear();
			materialReIssuePopup.hidePopUp();
		}
		if(event.getSource().equals(materialReIssuePopup.getLblOk())){

			final List<MaterialRequired> issueList = new ArrayList<MaterialRequired>(); 
			if(materialReIssuePopup.validate()){
//			if(customerLogDetails.isIssueFlag()){
//				materialReIssuePopup.getMaterialIssueTable().setEnable(false);
//				alert.alert("Material is already issued for this call log");
//			}
			customerLogDetails.setIssueFlag(false);
			if(materialReIssuePopup.getMaterialIssueTable().getDataprovider().getList().size()!=0 && !(customerLogDetails.isIssueFlag())){
				issueList.addAll(materialReIssuePopup.getMaterialIssueTable().getDataprovider().getList());
				//defectiveList.addAll(materialIssuePopup.getDefectiveMaterialTable().getDataprovider().getList());		
				//customerLogDetails.setDefectiveMaterial(defectiveList);	
				
				
				/**
				 * Date 10-09-2018 By Vijay
				 * Des :- added product inventory master defined or not validation for TransferTo warehouse
				 */
				Company c = new Company();
				List<MaterialRequired> materilList = materialReIssuePopup.getMaterialIssueTable().getDataprovider().getList();
				ArrayList<Integer> integerList = new ArrayList<Integer>();
				for(MaterialRequired prodId : materilList){
					integerList.add(prodId.getProductId());
				}
				generalService.getProductInventoryDetails(integerList, c.getCompanyId(), new AsyncCallback<ArrayList<SuperModel>>() {
					
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						// TODO Auto-generated method stub
						if(result!=null){
						String transferToWarehouseName = "";
						if(materialReIssuePopup.olbToWarehouse.getSelectedIndex() !=0){
							transferToWarehouseName = materialReIssuePopup.olbToWarehouse.getValue(materialReIssuePopup.olbToWarehouse.getSelectedIndex());	
						}else{
							transferToWarehouseName = customerLogDetails.getToWarehouse();
						}
						String transferToLocationName = "";
						if(materialReIssuePopup.olbToLoc.getSelectedIndex() != 0){
							transferToLocationName = materialReIssuePopup.olbToLoc.getValue(materialReIssuePopup.olbToLoc.getSelectedIndex());
						}else{
							transferToLocationName = customerLogDetails.getToLocation();
						}				
						String transferToBinName = "";
						if(materialReIssuePopup.olbToBin.getSelectedIndex() != 0){
							transferToBinName = materialReIssuePopup.olbToBin.getValue(materialReIssuePopup.olbToBin.getSelectedIndex());
						}else{
							transferToBinName = customerLogDetails.getToBin();
						}
						if(result.size()==0){
							alert.alert("Please Define Product Inventory Master First!");
						}
						boolean flag = false;
						for(SuperModel model : result){
							ProductInventoryView prodInventory = (ProductInventoryView) model;
							for(ProductInventoryViewDetails productDetails: prodInventory.getDetails()){
								if(productDetails.getWarehousename().trim().equals(transferToWarehouseName.trim()) &&
									productDetails.getStoragelocation().trim().equals(transferToLocationName.trim()) &&
									productDetails.getStoragebin().trim().equals(transferToBinName.trim()) ){
									flag =true;
								}
							}
						}
						if(flag==false){
							alert.alert("Please Define Product Inventory Master for TransferTo Warehouse!");
							return;
						}
						/**
						 * Date 10-09-2018 By Vijay
						 * Des :- old code added here after validation check
						 */
						if(result.size()!=0){
							if (validateQuantity(materialReIssuePopup.getMaterialIssueTable()
									.getDataprovider().getList())) {
								customerLogDetails.setStatus(CustomerLogDetails.ONHOLD);
								saveLog(customerLogDetails);
								alert.alert("Insufficient available quantity or select warehouse details");
							} else {
								customerLogDetails.getIssueMaterial().addAll(issueList);
								customerLogDetails.setToWarehouse(transferToWarehouseName);
								customerLogDetails.setToLocation(transferToLocationName);
								customerLogDetails.setToBin(transferToBinName);
								customerLogDetails.setStatus(CustomerLogDetails.MATERIAISSUED);
								customerLogDetails.setIssueFlag(true);
								createMMNForReIssueMaterial();
								saveLog(customerLogDetails);
							}	
						}
						
						}else{
							alert.alert("Please Define Product Inventory Master for TransferTo Warehouse!");
							return;
						}
						
					}
					
					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						
					}
				});
				
								
		      }
			 }
			
		}
	}

	private void createMMNForIssueMaterial(){
		int customerId;
		String customerName;
		customerId = materialIssuePopup.getCustomerLogObj().getCinfo().getCount();
		customerName = materialIssuePopup.getCustomerLogObj().getCinfo().getFullName();
		final MaterialMovementNote mmn=new MaterialMovementNote();
		mmn.setCompanyId(materialIssuePopup.getCustomerLogObj().getCompanyId());		
		mmn.setMmnDate(new Date());
	    mmn.setBranch(materialIssuePopup.getCustomerLogObj().getCompanyName());
		mmn.setMmnTransactionType("TransferOUT");
		mmn.setTransDirection("TRANSFEROUT");
		mmn.setEmployee(materialIssuePopup.getTbTechnicianName().getValue());
		mmn.setMmnPersonResposible(LoginPresenter.loggedInUser);
		mmn.setApproverName(LoginPresenter.loggedInUser);
		mmn.setCreatedBy(LoginPresenter.loggedInUser);
		mmn.setStatus(MaterialMovementNote.REQUESTED);
		//mmn.setMmnTitle(materialIssuePopup.getCustomerLogObj().getCount()+"");
		mmn.setMmnTitle(materialIssuePopup.getCustomerLogObj().getComplaintNumber()+"");
		mmn.setCreationDate(new Date());
//		System.out.println("TransToStorBin******************"+materialIssuePopup.getCustomerLogObj().getTechnician());
//		mmn.setTransToStorBin(materialIssuePopup.getCustomerLogObj().getTechnician());
//		System.out.println("TransToStorLoc*****************"+materialIssuePopup.getCustomerLogObj().getCompanyName().toUpperCase().trim());
//		mmn.setTransToStorLoc(materialIssuePopup.getCustomerLogObj().getCompanyName().toUpperCase().trim());
//		System.out.println("TransToWareHouse*************"+"Technician");
//		mmn.setTransToWareHouse("Technician");
		System.out.println("TransToStorBin******************"+materialIssuePopup.getOlbToBin().getValue(materialIssuePopup.getOlbToBin().getSelectedIndex()));
		mmn.setTransToStorBin(materialIssuePopup.getOlbToBin().getValue(materialIssuePopup.getOlbToBin().getSelectedIndex()));
		System.out.println("TransToStorLoc*****************"+materialIssuePopup.getOlbToLoc().getValue(materialIssuePopup.getOlbToLoc().getSelectedIndex()));
		mmn.setTransToStorLoc(materialIssuePopup.getOlbToLoc().getValue(materialIssuePopup.getOlbToLoc().getSelectedIndex()));
		System.out.println("TransToWareHouse*************"+materialIssuePopup.getOlbToWarehouse().getValue());
		mmn.setTransToWareHouse(materialIssuePopup.getOlbToWarehouse().getValue());
		System.out.println("from popup product*************"+materialIssuePopup.getMaterialIssueTable().getDataprovider().getList().toString());
	    ArrayList<MaterialProduct> list = new ArrayList<MaterialProduct>();
	    for(MaterialRequired m : materialIssuePopup.getMaterialIssueTable().getDataprovider().getList()){
	    	MaterialProduct mp = new MaterialProduct();
	    	Company c = new Company();
	    	mp.setCompanyId(c.getCompanyId());
	    	mp.setMaterialProductId(m.getProductId());
	    	mp.setMaterialProductCode(m.getProductCode());
	    	mp.setMaterialProductName(m.getProductName());
	    	mp.setMaterialProductCategory(m.getProductCategory());
	    	mp.setMaterialProductStorageBin(m.getMaterialProductStorageBin());
	    	mp.setMaterialProductStorageLocation(m.getMaterialProductStorageLocation());
	    	mp.setMaterialProductWarehouse(m.getMaterialProductWarehouse());
	    	mp.setMaterialProductUOM(m.getProductUOM());
	    	mp.setMaterialProductRequiredQuantity(m.getProductIssueQty());
	    	mp.setMaterialProductAvailableQuantity(m.getProductAvailableQty());
	    	list.add(mp);
	    }
	    System.out.println("product list*************"+list.toString());
		mmn.setSubProductTableMmn(list);
		String id = customerId+"";
		generalService.saveAndApproveDocument(mmn, id, customerName, AppConstants.MMN, new AsyncCallback<String>(){
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				materialIssuePopup.hidePopUp();
				alert.alert("An Unexpected error occurred!");
			}
			@Override
			public void onSuccess(String result) {
				// TODO Auto-generated method stub
				materialIssuePopup.hidePopUp();
				System.out.println("Saved Successfully ");
				//alert.alert("Saved successfully!");
			}			
		});
//		service.save(mmn,new AsyncCallback<ReturnFromServer>() {
//			@Override
//			public void onFailure(Throwable caught) {
//				 materialIssuePopup.hidePopUp();
//			     alert.alert("An Unexpected error occurred!");
//			}
//			@Override
//			public void onSuccess(ReturnFromServer result) 
//			{    materialIssuePopup.hidePopUp();
//			     System.out.println("latest mmn count"+ result.count);
//				 mmn.setCount(result.count);		 
//				 submitMMN(mmn,customerId,customerName,AppConstants.MMN);
//			     alert.alert("Saved successfully!");
//			}
//		});		
	  }	
	private void createMMNForReturnMaterial(){
		final int customerId;
		final String customerName;
		customerId = materialReturnPopup.getCustomerLogObj().getCinfo().getCount();
		customerName = materialReturnPopup.getCustomerLogObj().getCinfo().getFullName();
		final MaterialMovementNote mmn=new MaterialMovementNote();
		mmn.setCompanyId(materialReturnPopup.getCustomerLogObj().getCompanyId());	
		mmn.setBranch(materialReturnPopup.getCustomerLogObj().getCompanyName());
		mmn.setMmnDate(new Date());
		mmn.setMmnTransactionType("TransferOUT");
		mmn.setTransDirection("TRANSFEROUT");
		mmn.setEmployee(materialReturnPopup.getTbTechnicianName().getValue());
		mmn.setMmnPersonResposible(LoginPresenter.loggedInUser);
		mmn.setApproverName(LoginPresenter.loggedInUser);
		mmn.setCreatedBy(LoginPresenter.loggedInUser);
		mmn.setStatus(MaterialMovementNote.REQUESTED);
		mmn.setCreationDate(new Date());
		//mmn.setMmnTitle(materialReturnPopup.getCustomerLogObj().getCount()+"");
		mmn.setMmnTitle(materialReturnPopup.getCustomerLogObj().getComplaintNumber()+"");
		System.out.println("from popup product*************"+materialReturnPopup.getMaterialIssueTable().getDataprovider().getList().toString());
		String toWareHouse="" , toLocation="" ,toBin="";
	    ArrayList<MaterialProduct> list = new ArrayList<MaterialProduct>();
	    for(MaterialRequired m : materialReturnPopup.getMaterialIssueTable().getDataprovider().getList()){
	    	MaterialProduct mp = new MaterialProduct();
	    	Company c = new Company();
	    	mp.setCompanyId(c.getCompanyId());
	    	mp.setMaterialProductId(m.getProductId());
	    	mp.setMaterialProductCode(m.getProductCode());
	    	mp.setMaterialProductName(m.getProductName());
	    	mp.setMaterialProductCategory(m.getProductCategory());
//	    	mp.setMaterialProductStorageBin(materialReturnPopup.getCustomerLogObj().getTechnician());
//	    	mp.setMaterialProductStorageLocation(materialReturnPopup.getCustomerLogObj().getCompanyName().toUpperCase().trim());
//	    	mp.setMaterialProductWarehouse("Technician");
	    	mp.setMaterialProductStorageBin(materialReturnPopup.getOlbToBin().getValue(materialReturnPopup.getOlbToBin().getSelectedIndex()));
	    	mp.setMaterialProductStorageLocation(materialReturnPopup.getOlbToLoc().getValue(materialReturnPopup.getOlbToLoc().getSelectedIndex()));
	    	mp.setMaterialProductWarehouse(materialReturnPopup.getOlbToWarehouse().getValue());
	    	mp.setMaterialProductUOM(m.getProductUOM());
	    	mp.setMaterialProductRequiredQuantity(m.getProductReturnQty());
	    	mp.setMaterialProductAvailableQuantity(m.getProductAvailableQty());
	    	toWareHouse = m.getMaterialProductWarehouse();
	    	toLocation = m.getMaterialProductStorageLocation();
	    	toBin = 	m.getMaterialProductStorageBin();	
	    	list.add(mp);
	    }
	    System.out.println("TransToStorBin******************"+toBin);
		mmn.setTransToStorBin(toBin);
		System.out.println("TransToStorLoc*****************"+toLocation);
		mmn.setTransToStorLoc(toLocation);
		System.out.println("TransToWareHouse*************"+toWareHouse);
		mmn.setTransToWareHouse(toWareHouse);
		mmn.setSubProductTableMmn(list);
		String id = customerId+"";
		generalService.saveAndApproveDocument(mmn, id, customerName, AppConstants.MMN, new AsyncCallback<String>(){
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				materialReturnPopup.hidePopUp();
				alert.alert("An Unexpected error occurred!");
			}
			@Override
			public void onSuccess(String result) {
				// TODO Auto-generated method stub
				materialReturnPopup.hidePopUp();
				System.out.println("Saved successfully!");
			}			
		});
//		service.save(mmn,new AsyncCallback<ReturnFromServer>() {
//			@Override
//			public void onFailure(Throwable caught) {
//				 materialReturnPopup.hidePopUp();
//			     alert.alert("An Unexpected error occurred!");
//			}
//			@Override
//			public void onSuccess(ReturnFromServer result) 
//			{    materialReturnPopup.hidePopUp();
//			     System.out.println("latest mmn count"+ result.count);
//				 mmn.setCount(result.count);		 
//				 submitMMN(mmn,customerId,customerName,AppConstants.MMN);
//			     alert.alert("Saved successfully!");
//			}
//		});		
	  }	
//	private void submitMMN(SuperModel s,int bpId,String bpName,String DocumentType){
//		MaterialMovementNote mmn = (MaterialMovementNote)s;
//		ApproverFactory appFactory=new ApproverFactory();
//		//ApprovableProcess businessprocess = 
//		String id = bpId+"";
//  		final Approvals approval=appFactory.getApprovalMMN(mmn.getEmployee(), mmn.getApproverName()
//				, mmn.getBranch(), mmn.getCount(),mmn.getMmnPersonResposible(),id,bpName,DocumentType);
//  		approval.setStatus(ConcreteBusinessProcess.APPROVED);
//		approval.setApprovalOrRejectDate(new Date());
//		service.save(approval, new AsyncCallback<ReturnFromServer>() {
//			@Override
//			public void onFailure(Throwable caught) {	
//			}
//			@Override
//			public void onSuccess(ReturnFromServer result) {
//				approval.setId(result.id);
//				System.out.println("approval id"+approval.getId());
//				approvalService.sendApproveRequest(approval, new AsyncCallback<String>() {
//					@Override
//					public void onSuccess(String result) {
//						if(result.equals("Success")){
//							System.out.println("Success : "+"Submited successfully!");	
//						}else{
//							System.out.println("result : "+result);
//						}
//					}	
//					@Override
//					public void onFailure(Throwable caught) {
//						System.out.println("failed : "+ caught);
//					}
//				});
//			}
//		});
//	}
	private void createMMNForDefectiveMaterialTable(){
		final int customerId;
		final String customerName;
		customerId = materialReturnPopup.getCustomerLogObj().getCinfo().getCount();
		customerName = materialReturnPopup.getCustomerLogObj().getCinfo().getFullName();
		final MaterialMovementNote mmn=new MaterialMovementNote();
		mmn.setCompanyId(materialReturnPopup.getCustomerLogObj().getCompanyId());	
		mmn.setBranch(materialReturnPopup.getCustomerLogObj().getCompanyName());
		mmn.setMmnDate(new Date());
		mmn.setMmnTransactionType("Return");
		mmn.setTransDirection("IN");
		mmn.setEmployee(materialReturnPopup.getTbTechnicianName().getValue());
		mmn.setMmnPersonResposible(LoginPresenter.loggedInUser);
		mmn.setApproverName(LoginPresenter.loggedInUser);
		mmn.setCreatedBy(LoginPresenter.loggedInUser);
		mmn.setStatus(MaterialMovementNote.REQUESTED);
		mmn.setCreationDate(new Date());
		mmn.setMmnCommentOnStatusChange("Defective");
		//mmn.setMmnTitle(materialReturnPopup.getCustomerLogObj().getCount()+"");
		mmn.setMmnTitle(materialReturnPopup.getCustomerLogObj().getComplaintNumber()+"");
	    ArrayList<MaterialProduct> list = new ArrayList<MaterialProduct>();
	    for(MaterialRequired m : materialReturnPopup.getDefectiveMaterialTable().getDataprovider().getList()){
	    	MaterialProduct mp = new MaterialProduct();
	    	Company c = new Company();
	    	mp.setCompanyId(c.getCompanyId());
	    	mp.setMaterialProductId(m.getProductId());
	    	mp.setMaterialProductCode(m.getProductCode());
	    	mp.setMaterialProductName(m.getProductName());
	    	mp.setMaterialProductCategory(m.getProductCategory());
	    	mp.setMaterialProductStorageBin(m.getMaterialProductStorageBin());
	    	mp.setMaterialProductStorageLocation(m.getMaterialProductStorageLocation());
	    	mp.setMaterialProductWarehouse(m.getMaterialProductWarehouse());
	    	mp.setMaterialProductUOM(m.getProductUOM());
	    	mp.setMaterialProductRequiredQuantity(m.getProductIssueQty());
	    	mp.setMaterialProductAvailableQuantity(m.getProductAvailableQty());
	    	mp.setCreditReceived(m.isMaterialCredit());
	    	list.add(mp);
	    }
	    for(MaterialProduct m :list){
			if(m.isCreditReceived()==false){
				mmn.setMmnGRNStatus(AppConstants.MATERIALPENDING);
				break;
			}else{
				mmn.setMmnGRNStatus(AppConstants.CREDITRECEIVED);
			}
		}
		
	    System.out.println("product list*************"+list.toString());
		mmn.setSubProductTableMmn(list);
		String id = customerId+"";
		generalService.saveAndApproveDocument(mmn, id, customerName, AppConstants.MMN, new AsyncCallback<String>(){
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				materialReturnPopup.hidePopUp();
				alert.alert("An Unexpected error occurred!");
			}
			@Override
			public void onSuccess(String result) {
				// TODO Auto-generated method stub
				materialReturnPopup.hidePopUp();
				System.out.println("Saved successfully!");
			}			
		});
	}
		private void createMIN(){
			final int customerId;
			final String customerName;
			customerId = materialReturnPopup.getCustomerLogObj().getCinfo().getCount();
			customerName = materialReturnPopup.getCustomerLogObj().getCinfo().getFullName();
			MaterialIssueNote min=new MaterialIssueNote();
			min.setCompanyId(materialReturnPopup.getCustomerLogObj().getCompanyId());
			min.setMinDate(new Date());
			min.setCreationDate(new Date());
			min.setEmployee(materialReturnPopup.getTbTechnicianName().getValue());
			//min.setMinPersonResposible(LoginPresenter.loggedInUser);
			min.setApproverName(LoginPresenter.loggedInUser);
			min.setCreatedBy(LoginPresenter.loggedInUser);
			min.setStatus(MaterialMovementNote.REQUESTED);
			//min.setMinTitle(materialReturnPopup.getCustomerLogObj().getCount()+"");
			min.setMinTitle(materialReturnPopup.getCustomerLogObj().getComplaintNumber()+"");
			min.setBranch(materialReturnPopup.getCustomerLogObj().getCompanyName());
		    ArrayList<MaterialProduct> list = new ArrayList<MaterialProduct>();
		    for(MaterialRequired m : materialReturnPopup.getMaterialIssueTable().getDataprovider().getList()){
		    	MaterialProduct mp = new MaterialProduct();
		    	Company c = new Company();
		    	mp.setCompanyId(c.getCompanyId());
		    	mp.setMaterialProductId(m.getProductId());
		    	mp.setMaterialProductCode(m.getProductCode());
		    	mp.setMaterialProductName(m.getProductName());
		    	mp.setMaterialProductCategory(m.getProductCategory());
//		    	mp.setMaterialProductStorageBin(m.getMaterialProductStorageBin());
//		    	mp.setMaterialProductStorageLocation(m.getMaterialProductStorageLocation());
//		    	mp.setMaterialProductWarehouse(m.getMaterialProductWarehouse());
		    	mp.setMaterialProductStorageBin(materialReturnPopup.getOlbToBin().getValue(materialReturnPopup.getOlbToBin().getSelectedIndex()));
		    	mp.setMaterialProductStorageLocation(materialReturnPopup.getOlbToLoc().getValue(materialReturnPopup.getOlbToLoc().getSelectedIndex()));
		    	mp.setMaterialProductWarehouse(materialReturnPopup.getOlbToWarehouse().getValue());
		    	mp.setMaterialProductUOM(m.getProductUOM());
		    	mp.setMaterialProductRequiredQuantity(m.getProductIssueQty()-m.getProductReturnQty());
		    	mp.setMaterialProductAvailableQuantity(m.getProductAvailableQty());
		    	list.add(mp);
		    }
		    System.out.println("product list*************"+list.toString());
			min.setSubProductTablemin(list);
			String id = customerId+"";
			generalService.saveAndApproveDocument(min, id, customerName, AppConstants.MIN, new AsyncCallback<String>(){
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					materialReturnPopup.hidePopUp();
					alert.alert("An Unexpected error occurred!");
				}
				@Override
				public void onSuccess(String result) {
					// TODO Auto-generated method stub
					materialReturnPopup.hidePopUp();
					System.out.println("Saved successfully!");
				}			
			});
//		service.save(mmn,new AsyncCallback<ReturnFromServer>() {
//			@Override
//			public void onFailure(Throwable caught) {
//				 materialReturnPopup.hidePopUp();
//			     alert.alert("An Unexpected error occurred!");
//			}
//			@Override
//			public void onSuccess(ReturnFromServer result) 
//			{    materialReturnPopup.hidePopUp();
//			     System.out.println("latest mmn count"+ result.count);
//				 mmn.setCount(result.count);		 
//				 submitMMN(mmn,customerId,customerName,AppConstants.MMN);
//				 System.out.println("Saved successfully!");
//			}
//		});		
	  }	

	private boolean validateQuantity(List<MaterialRequired> list) {
		if(list.size() != 0){
		for (MaterialRequired m : list) {
			if (m.getProductAvailableQty() == 0) {
				return true;
			} else if ((m.getProductAvailableQty() - m.getProductIssueQty()) < 0) {
				return true;
			}
		}
		}
		return false;
	 }

	private boolean validateQuantityForDefective(List<MaterialRequired> list) {
		if (list.size() != 0) {
			for (MaterialRequired m : list) {
				if (m.getMaterialProductWarehouse().equals("")) {
					return true;
				}
			}
		}
		return false;
	 }
	 private boolean validateReturnQuantity(List<MaterialRequired> list){
		 int count = 0;
		 for (MaterialRequired m : list) {
				if (m.getProductReturnQty() != 0) {
					count++;
				}
		 }
		 if(count == 0){
			 return false;
		 }
		 return true;
	  }
	 private void saveLog(final CustomerLogDetails customerLog){
		 service.save(customerLog,
					new AsyncCallback<ReturnFromServer>() {
						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							System.out.println("failed");
						}
						@Override
						public void onSuccess(ReturnFromServer result) {
							// TODO Auto-generated method stub
							CustomerLogDetailsForm form = new CustomerLogDetailsForm();
							if(customerLog.getStatus().equals(CustomerLogDetails.COMPLETED)){
								form.getAccountTable().getTable().redraw();
								alert.alert("Data saved successfully");
							}
							if(customerLog.getStatus().equals(CustomerLogDetails.MATERIAISSUED) || customerLog.getStatus().equals(CustomerLogDetails.COMPLETED) ){
								form.getWareHouseTable().getTable().redraw();
								alert.alert("Data saved successfully");
							}
							System.out.println("success (issue table)"
									+ result.count);
							RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
							table.redrawRow(rowIndex);
						}
					});
	 }
	
		private void addSortinggetComplaintNumber(){
			List<CustomerLogDetails> list=getDataprovider().getList();
			columnSort=new ListHandler<CustomerLogDetails>(list);
			columnSort.setComparator(getComplaintNumber, new Comparator<CustomerLogDetails>()
					{
				@Override
				public int compare(CustomerLogDetails e1,CustomerLogDetails e2)
				{
					if(e1!=null && e2!=null)
					{
						if( e1.getComplaintNumber()!=null && e2.getComplaintNumber()!=null){
							return e1.getComplaintNumber().compareTo(e2.getComplaintNumber());}
					}
					else{
						return 0;}
					return 0;
				}
					});
			table.addColumnSortHandler(columnSort);
		}
		private void addSortinggetCustomerName(){

			List<CustomerLogDetails> list=getDataprovider().getList();
			columnSort=new ListHandler<CustomerLogDetails>(list);
			columnSort.setComparator(getColumnCustomerName, new Comparator<CustomerLogDetails>()
					{
				@Override
				public int compare(CustomerLogDetails e1,CustomerLogDetails e2)
				{
					if(e1!=null && e2!=null)
					{
						if( e1.getCinfo().getFullName()!=null && e2.getCinfo().getFullName()!=null){
							return e1.getCinfo().getFullName().compareTo(e2.getCinfo().getFullName());}
					}
					else{
						return 0;}
					return 0;
				}
					});
			table.addColumnSortHandler(columnSort);	
		}
		private void addSortinggetCreationDate(){
			  List<CustomerLogDetails> list=getDataprovider().getList();
			  columnSort=new ListHandler<CustomerLogDetails>(list);
			  columnSort.setComparator(getCreationDate, new Comparator<CustomerLogDetails>()
			  {
			  @Override
			  public int compare(CustomerLogDetails e1,CustomerLogDetails e2)
			  {
			  if(e1!=null && e2!=null)
			  {
				  if(e1.getCreatedOn() == null && e2.getCreatedOn()==null){
				        return 0;
				    }
				    if(e1.getCreatedOn() == null) return 1;
				    if(e2.getCreatedOn() == null) return -1;
				    return e1.getCreatedOn().compareTo(e2.getCreatedOn());
			  }
			  return 0;
			  }
			  });
			  table.addColumnSortHandler(columnSort);  
		}
		private void addSortinggetAppointmentDate(){
			  List<CustomerLogDetails> list=getDataprovider().getList();
			  columnSort=new ListHandler<CustomerLogDetails>(list);
			  columnSort.setComparator(getAppointmentDate, new Comparator<CustomerLogDetails>()
			  {
			  @Override
			  public int compare(CustomerLogDetails e1,CustomerLogDetails e2)
			  {
			  if(e1!=null && e2!=null)
			  {
				  if(e1.getAppointmentDate() == null && e2.getAppointmentDate()==null){
				        return 0;
				    }
				    if(e1.getAppointmentDate() == null) return 1;
				    if(e2.getAppointmentDate() == null) return -1;
				    return e1.getAppointmentDate().compareTo(e2.getAppointmentDate());
			  }
			  return 0;
			  }
			  });
			  table.addColumnSortHandler(columnSort); 
		}
		private void addSortinggetProduct(){
			List<CustomerLogDetails> list=getDataprovider().getList();
			columnSort=new ListHandler<CustomerLogDetails>(list);
			columnSort.setComparator(getProduct, new Comparator<CustomerLogDetails>()
					{
				@Override
				public int compare(CustomerLogDetails e1,CustomerLogDetails e2)
				{
					if(e1!=null && e2!=null)
					{
						if( e1.getProduct()!=null && e2.getProduct()!=null){
							return e1.getProduct().compareTo(e2.getProduct());}
					}
					else{
						return 0;}
					return 0;
				}
					});
			table.addColumnSortHandler(columnSort);
		}
		private void addSortinggetWarrantyStatus(){
			List<CustomerLogDetails> list=getDataprovider().getList();
			columnSort=new ListHandler<CustomerLogDetails>(list);
			columnSort.setComparator(getWarrantyStatus, new Comparator<CustomerLogDetails>()
					{
				@Override
				public int compare(CustomerLogDetails e1,CustomerLogDetails e2)
				{
					if(e1!=null && e2!=null)
					{
						if( e1.getWarrantyStatus()!=null && e2.getWarrantyStatus()!=null){
							return e1.getWarrantyStatus().compareTo(e2.getWarrantyStatus());}
					}
					else{
						return 0;}
					return 0;
				}
					});
			table.addColumnSortHandler(columnSort);
		}
		private void addSortinggetTechnician(){
			List<CustomerLogDetails> list=getDataprovider().getList();
			columnSort=new ListHandler<CustomerLogDetails>(list);
			columnSort.setComparator(getTechnician, new Comparator<CustomerLogDetails>()
					{
				@Override
				public int compare(CustomerLogDetails e1,CustomerLogDetails e2)
				{
					if(e1!=null && e2!=null)
					{
						if( e1.getTechnician()!=null && e2.getTechnician()!=null){
							return e1.getTechnician().compareTo(e2.getTechnician());}
					}
					else{
						return 0;}
					return 0;
				}
					});
			table.addColumnSortHandler(columnSort);
		}
		private void addSortinggetStatus(){
			List<CustomerLogDetails> list=getDataprovider().getList();
			columnSort=new ListHandler<CustomerLogDetails>(list);
			columnSort.setComparator(getStatus, new Comparator<CustomerLogDetails>()
					{
				@Override
				public int compare(CustomerLogDetails e1,CustomerLogDetails e2)
				{
					if(e1!=null && e2!=null)
					{
						if( e1.getStatus()!=null && e2.getStatus()!=null){
							return e1.getStatus().compareTo(e2.getStatus());}
					}
					else{
						return 0;}
					return 0;
				}
					});
			table.addColumnSortHandler(columnSort);
		}
		private void addSortinggetCallBackStatus(){
			List<CustomerLogDetails> list=getDataprovider().getList();
			columnSort=new ListHandler<CustomerLogDetails>(list);
			columnSort.setComparator(getCallBackStatus, new Comparator<CustomerLogDetails>()
					{
				@Override
				public int compare(CustomerLogDetails e1,CustomerLogDetails e2)
				{
					if(e1!=null && e2!=null)
					{
						if( e1.getCallbackStatus()!=null && e2.getCallbackStatus()!=null){
							return e1.getCallbackStatus().compareTo(e2.getCallbackStatus());}
					}
					else{
						return 0;}
					return 0;
				}
					});
			table.addColumnSortHandler(columnSort);	
		}
		protected void addColumngetCallType(){
	    	getCallType = new TextColumn<CustomerLogDetails>() {
				@Override
				public String getValue(CustomerLogDetails object) {
						return object.getCallType();
				}
			};
			table.addColumn(getCallType, "Call Type");
			getCallType.setSortable(true);
			table.setColumnWidth(getCallType,"90px");
	    }
	 
	 private void addSortinggetCallType(){
			List<CustomerLogDetails> list=getDataprovider().getList();
			columnSort=new ListHandler<CustomerLogDetails>(list);
			columnSort.setComparator(getCallType, new Comparator<CustomerLogDetails>()
				{
				@Override
				public int compare(CustomerLogDetails e1,CustomerLogDetails e2)
				{
					if(e1!=null && e2!=null)
					{
						if( e1.getCallType()!=null && e2.getCallType()!=null){
							return e1.getCallType().compareTo(e2.getCallType());}
					}
					else{
						return 0;}
					return 0;
				}
					});
			table.addColumnSortHandler(columnSort);
		}

		private MaterialRequired setObject(MaterialRequired m){
			 MaterialRequired material = new MaterialRequired();
			 material.setPrduct(m.getPrduct());
			 material.setProductPrice(m.getProductPrice());
			 material.setProductId(m.getProductId());
			 material.setProductCategory(m.getProductCategory());
			 material.setProductCode(m.getProductCategory());
			 material.setProductName(m.getProductName());
			 material.setProductIssueQty(m.getProductIssueQty());
			 material.setProductPayableQuantity(m.getProductPayableQuantity());
			 material.setProductReturnQty(m.getProductReturnQty());
			 material.setVatTax(m.getVatTax());
			 material.setServiceTax(m.getServiceTax());
			 return material;
		 }
		private void saveOnReturnMaterial(boolean flag , String status){
			List<MaterialRequired> returnList = new ArrayList<MaterialRequired>(); 
			List<MaterialRequired> defectiveList = new ArrayList<MaterialRequired>(); 
			if(validateQuantityForDefective(materialReturnPopup.getDefectiveMaterialTable().getDataprovider().getList())){
				alert.alert("select warehouse details for defective");
			}
			else{
			if (materialReturnPopup.getDefectiveMaterialTable().getDataprovider().getList().size() != 0 && !(customerLogDetails.isDefectiveFlag())) {
				defectiveList.addAll(materialReturnPopup.getDefectiveMaterialTable().getDataprovider().getList());
				//customerLogDetails.setDefectiveMaterial(defectiveList);
				customerLogDetails.getDefectiveMaterial().addAll(defectiveList);
				customerLogDetails.setDefectiveFlag(flag);
				createMMNForDefectiveMaterialTable();
			}	
			if(materialReturnPopup.getMaterialIssueTable().getDataprovider().getList().size()!=0 && !(customerLogDetails.isReturnFlag())){
				returnList.addAll(materialReturnPopup.getMaterialIssueTable().getDataprovider().getList());
				customerLogDetails.setReturnMaterial(returnList);	
				createMIN();
				if(validateReturnQuantity(materialReturnPopup.getMaterialIssueTable()
						.getDataprovider().getList())){
					customerLogDetails.setReturnFlag(flag);
					customerLogDetails.setIssueFlag(flag);
					Timer t = new Timer() {
						@Override
						public void run() {
							// TODO Auto-generated method stub
					         createMMNForReturnMaterial();
						}
					};t.schedule(7000);
				}
				
			  }
			       customerLogDetails.setStatus(status);
			       if(status.equals(CustomerLogDetails.CREATED)){
			    	   customerLogDetails.setVisitStatus("Hold");
			       }
			        if(customerLogDetails.getReturnMaterial()!=null){
						ServiceProductTable t = new ServiceProductTable();
						List<MaterialRequired> companyList = new ArrayList<MaterialRequired>();
						List<MaterialRequired> customerList = new ArrayList<MaterialRequired>();
						List<MaterialRequired> materialList = new ArrayList<MaterialRequired>();
						for(MaterialRequired material : customerLogDetails.getReturnMaterial()){
							materialList.add(setObject(material));
						}
						//materialList.addAll(customerLogDetails.getReturnMaterial());
						for(MaterialRequired m : materialList){
							double quant = m.getProductIssueQty();
							MaterialRequired m1 = new MaterialRequired();
							m1 = setObject(m);
							//showDialogMessage("issue quant :" + quant);
						 if((materialReturnPopup.getOlbWarrantyStatus().getValue().equalsIgnoreCase("Warranty") || materialReturnPopup.getOlbWarrantyStatus().getValue().equalsIgnoreCase("AMC")) && m.getProductPayableQuantity()!=0){				 
							 m.setProductIssueQty(m.getProductPayableQuantity());
							 double amount = t.calculateTotalAmount(m);
							 m.setTotalAmount(amount);
							 customerList.add(m);
						  }
						 if(materialReturnPopup.getOlbWarrantyStatus().getValue().equalsIgnoreCase("Warranty") || materialReturnPopup.getOlbWarrantyStatus().getValue().equalsIgnoreCase("AMC")){
							 m1.setProductIssueQty(quant - m1.getProductReturnQty()-m1.getProductPayableQuantity());
							 double amount = t.calculateTotalAmount(m1);
							 m1.setTotalAmount(amount);
							 companyList.add(m1);
						 }
						  if(materialReturnPopup.getOlbWarrantyStatus().getValue().equalsIgnoreCase("Out Of Warranty")){					  
							  m.setProductIssueQty(quant - m.getProductReturnQty()-m.getProductPayableQuantity());
							  m.setTotalAmount(t.calculateTotalAmount(m));
							  customerList.add(m);
      					  }
						}
//						customerLogDetails.setCompanyCost(companyList);
//						customerLogDetails.setCustomerCost(customerList);
						customerLogDetails.getCustomerCost().addAll(customerList);
						customerLogDetails.getCompanyCost().addAll(companyList);
					}					
			        generalService.saveCustomerCallLogDetails(customerLogDetails, customerLogDetails.getCompanyId(), new AsyncCallback<CustomerLogDetails>() {						
						@Override
						public void onSuccess(CustomerLogDetails result) {
							// TODO Auto-generated method stub
							alert.alert("Data saved Successfully");
							RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
							table.redrawRow(rowIndex);
						}
						
						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							alert.alert("Unexpected error");
						}
					});
			}
		
		}
		private void createMMNForReIssueMaterial(){
			int customerId;
			String customerName;
			customerId = materialReIssuePopup.getCustomerLogObj().getCinfo().getCount();
			customerName = materialReIssuePopup.getCustomerLogObj().getCinfo().getFullName();
			final MaterialMovementNote mmn=new MaterialMovementNote();
			mmn.setCompanyId(materialReIssuePopup.getCustomerLogObj().getCompanyId());		
			mmn.setMmnDate(new Date());
		    mmn.setBranch(materialReIssuePopup.getCustomerLogObj().getCompanyName());
			mmn.setMmnTransactionType("TransferOUT");
			mmn.setTransDirection("TRANSFEROUT");
			mmn.setEmployee(materialReIssuePopup.getTbTechnicianName().getValue());
			mmn.setMmnPersonResposible(LoginPresenter.loggedInUser);
			mmn.setApproverName(LoginPresenter.loggedInUser);
			mmn.setCreatedBy(LoginPresenter.loggedInUser);
			mmn.setStatus(MaterialMovementNote.REQUESTED);
			//mmn.setMmnTitle(materialIssuePopup.getCustomerLogObj().getCount()+"");
			mmn.setMmnTitle(materialReIssuePopup.getCustomerLogObj().getComplaintNumber()+"");
			mmn.setCreationDate(new Date());
//			System.out.println("TransToStorBin******************"+materialIssuePopup.getCustomerLogObj().getTechnician());
//			mmn.setTransToStorBin(materialIssuePopup.getCustomerLogObj().getTechnician());
//			System.out.println("TransToStorLoc*****************"+materialIssuePopup.getCustomerLogObj().getCompanyName().toUpperCase().trim());
//			mmn.setTransToStorLoc(materialIssuePopup.getCustomerLogObj().getCompanyName().toUpperCase().trim());
//			System.out.println("TransToWareHouse*************"+"Technician");
//			mmn.setTransToWareHouse("Technician");
			System.out.println("TransToStorBin******************"+materialReIssuePopup.getOlbToBin().getValue(materialReIssuePopup.getOlbToBin().getSelectedIndex()));
			mmn.setTransToStorBin(materialReIssuePopup.getOlbToBin().getValue(materialReIssuePopup.getOlbToBin().getSelectedIndex()));
			System.out.println("TransToStorLoc*****************"+materialReIssuePopup.getOlbToLoc().getValue(materialReIssuePopup.getOlbToLoc().getSelectedIndex()));
			mmn.setTransToStorLoc(materialReIssuePopup.getOlbToLoc().getValue(materialReIssuePopup.getOlbToLoc().getSelectedIndex()));
			System.out.println("TransToWareHouse*************"+materialReIssuePopup.getOlbToWarehouse().getValue());
			mmn.setTransToWareHouse(materialReIssuePopup.getOlbToWarehouse().getValue());
			System.out.println("from popup product*************"+materialReIssuePopup.getMaterialIssueTable().getDataprovider().getList().toString());
		    ArrayList<MaterialProduct> list = new ArrayList<MaterialProduct>();
		    for(MaterialRequired m : materialReIssuePopup.getMaterialIssueTable().getDataprovider().getList()){
		    	MaterialProduct mp = new MaterialProduct();
		    	Company c = new Company();
		    	mp.setCompanyId(c.getCompanyId());
		    	mp.setMaterialProductId(m.getProductId());
		    	mp.setMaterialProductCode(m.getProductCode());
		    	mp.setMaterialProductName(m.getProductName());
		    	mp.setMaterialProductCategory(m.getProductCategory());
		    	mp.setMaterialProductStorageBin(m.getMaterialProductStorageBin());
		    	mp.setMaterialProductStorageLocation(m.getMaterialProductStorageLocation());
		    	mp.setMaterialProductWarehouse(m.getMaterialProductWarehouse());
		    	mp.setMaterialProductUOM(m.getProductUOM());
		    	mp.setMaterialProductRequiredQuantity(m.getProductIssueQty());
		    	mp.setMaterialProductAvailableQuantity(m.getProductAvailableQty());
		    	list.add(mp);
		    }
		    System.out.println("product list*************"+list.toString());
			mmn.setSubProductTableMmn(list);
			String id = customerId+"";
			generalService.saveAndApproveDocument(mmn, id, customerName, AppConstants.MMN, new AsyncCallback<String>(){
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					materialReIssuePopup.hidePopUp();
					alert.alert("An Unexpected error occurred!");
				}
				@Override
				public void onSuccess(String result) {
					// TODO Auto-generated method stub
					materialReIssuePopup.hidePopUp();
					System.out.println("Saved Successfully ");
					//alert.alert("Saved successfully!");
				}			
			});	
		  }	

 }
 
	

