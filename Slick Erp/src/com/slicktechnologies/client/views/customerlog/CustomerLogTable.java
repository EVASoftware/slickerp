package com.slicktechnologies.client.views.customerlog;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.PopupPanel;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.MyInlineLabel;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CommunicationLogService;
import com.slicktechnologies.client.services.CommunicationLogServiceAsync;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.communicationlog.CommunicationLogPopUp;
import com.slicktechnologies.client.views.customerlog.CustomerLogDetailsPresenter.CustomerLogDetailsPresenterTable;
import com.slicktechnologies.client.views.interaction.interactiondetails.InteractionPresenter;
import com.slicktechnologies.shared.common.customerlog.CustomerLogDetails;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;
import com.slicktechnologies.shared.common.inventory.MaterialMovementNote;

public class CustomerLogTable extends CustomerLogDetailsPresenterTable implements ClickHandler
//extends CustomerLogDetailsPresenterTable /extends SuperTable<CustomerLogDetails> 
{
	TextColumn<CustomerLogDetails> getColumnId;
	TextColumn<CustomerLogDetails> getComplaintNumber;
	TextColumn<CustomerLogDetails> getCallType;
	TextColumn<CustomerLogDetails> getPriority;
	TextColumn<CustomerLogDetails> getStatus;
	TextColumn<CustomerLogDetails> getComplaintDate;
	TextColumn<CustomerLogDetails> getProductGroup;
	TextColumn<CustomerLogDetails> getProduct;
	TextColumn<CustomerLogDetails> getCustomerName;
	TextColumn<CustomerLogDetails> getTechnician;
	TextColumn<CustomerLogDetails> getAppointmentDate;
	TextColumn<CustomerLogDetails> getAddress;
	TextColumn<CustomerLogDetails> getLocality;
	TextColumn<CustomerLogDetails> getCity;
	TextColumn<CustomerLogDetails> getCellNumber;
	TextColumn<CustomerLogDetails> getCellNumber2;
	TextColumn<CustomerLogDetails> getWarrantyStatus;
	TextColumn<CustomerLogDetails> getCompanyName;
	TextColumn<CustomerLogDetails> getCreationDate;
	Column<CustomerLogDetails,String> getCommunicationLogButton;
	CommunicationLogPopUp communicationLogPopUp = new CommunicationLogPopUp();
	CommunicationLogServiceAsync communicationService = GWT.create(CommunicationLogService.class);
	GenricServiceAsync service = GWT.create(GenricService.class);
	CustomerLogDetails customerLog = new CustomerLogDetails();
	
	public CustomerLogTable()
	{
		super();
		getTable().setHeight("500px");
		getTable().setWidth("100%");
		communicationLogPopUp.getBtnAdd().addClickHandler(this);
		communicationLogPopUp.getBtnOk().addClickHandler(this);
		communicationLogPopUp.getBtnCancel().addClickHandler(this);
	}
	@Override
	public void createTable() {
		addColumngetId();
		addgetCommunicationLogButtonButton();
		addColumngetCreationDate();
		addColumngetCustomerName();
		addColumngetProduct();
		addColumngetCallType();
		addColumngetStatus();
		addColumngetTechnician();
		
		addColumngetCompanyName();
		
		addColumngetWarrantyStatus();
		
		
		
		//addColumngetProductGroup();
		
		addColumngetAppointmentDate();
		addColumngetComplaintNumber();
		
		addColumngetCellNumber();
			
		
		//addColumngetPriority();
		
		//addColumngetAddress();
		addColumngetLocality();
		//addCoumngetCity();
		addColumngetCellNumber2();	
		addFieldUpdater();
		addColumnSorting();
	}
	protected void addColumngetId(){
		getColumnId = new TextColumn<CustomerLogDetails>() {
			@Override
			public String getValue(CustomerLogDetails object) {
					return object.getCount() + "";
			}
		};
		table.addColumn(getColumnId, "Id");
		addSortinggetID();
		getColumnId.setSortable(true);
	    table.setColumnWidth(getColumnId,"90px");
	}
	
    protected void addColumngetComplaintNumber(){
    	getComplaintNumber = new TextColumn<CustomerLogDetails>() {
			@Override
			public String getValue(CustomerLogDetails object) {
				if(object.getComplaintNumber()!=null){
					return object.getComplaintNumber()+ "";
				}
				else{
					return "";
				}
			}
		};
		table.addColumn(getComplaintNumber, "Complaint Number");
		getComplaintNumber.setSortable(true);
		table.setColumnWidth(getComplaintNumber,"90px");
    }
    
    protected void addColumngetWarrantyStatus(){
    	getWarrantyStatus = new TextColumn<CustomerLogDetails>() {
			@Override
			public String getValue(CustomerLogDetails object) {
					return object.getWarrantyStatus();
			}
		};
		table.addColumn(getWarrantyStatus, "Warranty Status");
		getWarrantyStatus.setSortable(true);
		table.setColumnWidth(getWarrantyStatus,"90px");
    } 
    protected void addColumngetCallType(){
    	getCallType = new TextColumn<CustomerLogDetails>() {
			@Override
			public String getValue(CustomerLogDetails object) {
					return object.getCallType();
			}
		};
		table.addColumn(getCallType, "Call Type");
		getCallType.setSortable(true);
		table.setColumnWidth(getCallType,"90px");
    } 
//    protected void addColumngetPriority(){
//    	getPriority = new TextColumn<CustomerLogDetails>() {
//			@Override
//			public String getValue(CustomerLogDetails object) {
//					return object.getPriority();
//			}
//		};
//		table.addColumn(getPriority, "Priority");
//		getPriority.setSortable(true);
//		table.setColumnWidth(getPriority,"90px");
//    }
    protected void addColumngetStatus(){
    	getStatus = new TextColumn<CustomerLogDetails>() {
			@Override
			public String getValue(CustomerLogDetails object) {
					return object.getStatus();
			}
		};
		table.addColumn(getStatus, "Status");
		getStatus.setSortable(true);
		table.setColumnWidth(getStatus,"90px");
    }
    protected void addColumngetCreationDate(){
		 getCreationDate = new TextColumn<CustomerLogDetails>() {
	        	@Override
	    		public String getValue(CustomerLogDetails object)
	    		{
	    			if(object.getCreatedOn()!=null){
	    				return AppUtility.parseDate(object.getCreatedOn());}
	    			else{
	    				return "";
	    			}
	    		}
	    			};
	    			table.addColumn(getCreationDate,"Creation Date");
	    			getCreationDate.setSortable(true);
	    			table.setColumnWidth(getCreationDate,"90px");
	    }
//    protected void addColumngetComplaintDate(){
//    	getComplaintDate = new TextColumn<CustomerLogDetails>() {
//    	@Override
//		public String getValue(CustomerLogDetails object)
//		{
//			if(object.getComplaintDate()!=null){
//				return AppUtility.parseDate(object.getComplaintDate());}
//			else{
//				return "N.A.";
//			}
//		}
//			};
//			table.addColumn(getComplaintDate,"Complaint Date");
//			getComplaintDate.setSortable(true);
//			table.setColumnWidth(getStatus, "90px");
//    }
//    protected void addColumngetProductGroup(){
//    	getProductGroup = new TextColumn<CustomerLogDetails>() {
//			@Override
//			public String getValue(CustomerLogDetails object) {
//					return object.getProductGroup();
//			}
//		};
//		table.addColumn(getProductGroup, "Product Group");
//		getProductGroup.setSortable(true);
//		table.setColumnWidth(getProductGroup,"90px");
//    }
    /**
     * Date 21-6-2018 by jayshree
     */
    protected void addColumngetProduct(){
    	getProduct = new TextColumn<CustomerLogDetails>() {
			@Override
			public String getValue(CustomerLogDetails object) {
				if(object.getServiceCharges().size()>0){
					return object.getServiceCharges().get(0).getProductName();
				}
					return "";
			}
		};
		table.addColumn(getProduct, "Product");
		getProduct.setSortable(true);
		table.setColumnWidth(getProduct,"90px");
    }
    
    //End by jayshree
    protected void addColumngetLocality(){
    	getLocality = new TextColumn<CustomerLogDetails>() {
			@Override
			public String getValue(CustomerLogDetails object) {
					return object.getAddress().getLocality();
			}
		};
		table.addColumn(getLocality, "Locality");
		getLocality.setSortable(true);
		table.setColumnWidth(getLocality,"90px");
    }
    protected void addCoumngetCity(){
    	getCity = new TextColumn<CustomerLogDetails>() {
			@Override
			public String getValue(CustomerLogDetails object) {
					return object.getAddress().getCity();
			}
		};
		table.addColumn(getCity, "City");
		getCity.setSortable(true);
		table.setColumnWidth(getCity,"90px");
    }
    protected void addColumngetAppointmentDate(){
    	getAppointmentDate = new TextColumn<CustomerLogDetails>() {
        	@Override
    		public String getValue(CustomerLogDetails object)
    		{
    			if(object.getAppointmentDate()!=null){
    				return AppUtility.parseDate(object.getAppointmentDate());}
    			else{
    				return "";
    			}
    		}
    			};
    			table.addColumn(getAppointmentDate,"Appointment Date");
    			getAppointmentDate.setSortable(true);
    			table.setColumnWidth(getAppointmentDate,"100px");
    }
    protected void addColumngetAddress(){
    	getAddress = new TextColumn<CustomerLogDetails>() {
			@Override
			public String getValue(CustomerLogDetails object) {
					return object.getAddress().getAddrLine1()+" "+object.getAddress().getAddrLine2();
			}
		};
		table.addColumn(getAddress, "Address");
		getAddress.setSortable(true);
		table.setColumnWidth(getAddress,"90px");
    }
   protected void addColumngetCustomerName(){
	   getCustomerName = new TextColumn<CustomerLogDetails>() {
			@Override
			public String getValue(CustomerLogDetails object) {
					return object.getCinfo().getFullName();
			}
		};
		table.addColumn(getCustomerName, "Customer Name");
		getCustomerName.setSortable(true);
		table.setColumnWidth(getCustomerName,"90px");
    }
    protected void addColumngetTechnician(){
    	getTechnician = new TextColumn<CustomerLogDetails>() {
			@Override
			public String getValue(CustomerLogDetails object) {
					return object.getTechnician();
			}
		};
		table.addColumn(getTechnician, "Technician");
		getTechnician.setSortable(true);
		table.setColumnWidth(getTechnician,"90px");
    }
    protected void addColumngetCellNumber(){
    	getCellNumber = new TextColumn<CustomerLogDetails>() {
			@Override
			public String getValue(CustomerLogDetails object) {
					return object.getCinfo().getCellNumber()+"";
			}
		};
		table.addColumn(getCellNumber, "Cell Number");
		getCellNumber.setSortable(true);
		table.setColumnWidth(getCellNumber,"110px");
    }
    protected void addColumngetCellNumber2(){
    	getCellNumber2 = new TextColumn<CustomerLogDetails>() {
			@Override
			public String getValue(CustomerLogDetails object) {
				if(object.getCellNumber2()!=null){	
				    return object.getCellNumber2()+"";
				}
				else{
					return "";
				}
			}
		};
		table.addColumn(getCellNumber2, "Cell Number2");
		getCellNumber2.setSortable(true);
		table.setColumnWidth(getCellNumber2,"110px");
    }
    protected void addColumngetCompanyName(){
    	getCompanyName = new TextColumn<CustomerLogDetails>() {
			@Override
			public String getValue(CustomerLogDetails object) {
					return object.getCompanyName();
			}
		};
		table.addColumn(getCompanyName, "Company Name");
		getCompanyName.setSortable(true);
		table.setColumnWidth(getCompanyName,"90px");
    } 
	@Override
	public void setEnable(boolean state)
	{super.setEnable(state);
	}
	@Override
	public void applyStyle()
	{
	}
	public void addColumnSorting(){
		addSortinggetID();
		addSortingetCompanyName();
		addSortinggetComplaintNumber();
		addSortinggetCustomerName();
		addSortinggetCellNumber();
		addSortinggetProduct();	
		addSortinggetCallType();
		//addColumngetPriority();
		addSortinggetWarrantyStatus();
		addSortinggetTechnician();
		addSortinggetStatus();
		addSortinggetCreationDate();
		//addColumngetProductGroup();
		
		addSortinggetAppointmentDate();
		//addColumngetAddress();
		addSortinggetLocality();
		//addCoumngetCity();
		addSortinggetCellNumber2();	
		
	}
	private void addSortinggetID() {
		List<CustomerLogDetails> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerLogDetails>(list);
		columnSort.setComparator(getColumnId, new Comparator<CustomerLogDetails>()
				{
			@Override
			public int compare(CustomerLogDetails e1,CustomerLogDetails e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
		});
		table.addColumnSortHandler(columnSort);	
	}
	private void addSortingetCompanyName(){
		List<CustomerLogDetails> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerLogDetails>(list);
		columnSort.setComparator(getCompanyName, new Comparator<CustomerLogDetails>()
				{
			@Override
			public int compare(CustomerLogDetails e1,CustomerLogDetails e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getCompanyName()!=null && e2.getCompanyName()!=null){
						return e1.getCompanyName().compareTo(e2.getCompanyName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	private void addSortinggetComplaintNumber(){
		List<CustomerLogDetails> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerLogDetails>(list);
		columnSort.setComparator(getComplaintNumber, new Comparator<CustomerLogDetails>()
		{
			@Override
			public int compare(CustomerLogDetails e1,CustomerLogDetails e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getComplaintNumber()!=null && e2.getComplaintNumber()!=null){
						return e1.getComplaintNumber().compareTo(e2.getComplaintNumber());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	private void addSortinggetCustomerName(){

		List<CustomerLogDetails> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerLogDetails>(list);
		columnSort.setComparator(getCustomerName, new Comparator<CustomerLogDetails>()
				{
			@Override
			public int compare(CustomerLogDetails e1,CustomerLogDetails e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCinfo().getFullName()!=null && e2.getCinfo().getFullName()!=null){
						return e1.getCinfo().getFullName().compareTo(e2.getCinfo().getFullName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);	
	}
	private void addSortinggetCellNumber(){
		List<CustomerLogDetails> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerLogDetails>(list);
		columnSort.setComparator(getCellNumber, new Comparator<CustomerLogDetails>()
				{
			@Override
			public int compare(CustomerLogDetails e1,CustomerLogDetails e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCinfo().getCellNumber()== e2.getCinfo().getCellNumber()){
						return 0;}
					if(e1.getCinfo().getCellNumber()> e2.getCinfo().getCellNumber()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);	
	}
	private void addSortinggetCellNumber2(){
		List<CustomerLogDetails> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerLogDetails>(list);
		columnSort.setComparator(getCellNumber2, new Comparator<CustomerLogDetails>()
				{
			@Override
			public int compare(CustomerLogDetails e1,CustomerLogDetails e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCellNumber2()== e2.getCellNumber2()){
						return 0;}
					if(e1.getCellNumber2()> e2.getCellNumber2()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);	
	}
	private void addSortinggetCreationDate(){
		  List<CustomerLogDetails> list=getDataprovider().getList();
		  columnSort=new ListHandler<CustomerLogDetails>(list);
		  columnSort.setComparator(getCreationDate, new Comparator<CustomerLogDetails>()
		  {
		  @Override
		  public int compare(CustomerLogDetails e1,CustomerLogDetails e2)
		  {
		  if(e1!=null && e2!=null)
		  {
			  if(e1.getCreatedOn() == null && e2.getCreatedOn()==null){
			        return 0;
			    }
			    if(e1.getCreatedOn() == null) return 1;
			    if(e2.getCreatedOn() == null) return -1;
			    return e1.getCreatedOn().compareTo(e2.getCreatedOn());
		  }
		  return 0;
		  }
		  });
		  table.addColumnSortHandler(columnSort);
		  
	}
	private void addSortinggetAppointmentDate(){
		  List<CustomerLogDetails> list=getDataprovider().getList();
		  columnSort=new ListHandler<CustomerLogDetails>(list);
		  columnSort.setComparator(getAppointmentDate, new Comparator<CustomerLogDetails>()
		  {
		  @Override
		  public int compare(CustomerLogDetails e1,CustomerLogDetails e2)
		  {
		  if(e1!=null && e2!=null)
		  {
			  if(e1.getAppointmentDate() == null && e2.getAppointmentDate()==null){
			        return 0;
			    }
			    if(e1.getAppointmentDate() == null) return 1;
			    if(e2.getAppointmentDate() == null) return -1;
			    return e1.getAppointmentDate().compareTo(e2.getAppointmentDate());
		  }
		  return 0;
		  }
		  });
		  table.addColumnSortHandler(columnSort); 
	}
	private void addSortinggetProduct(){
		List<CustomerLogDetails> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerLogDetails>(list);
		columnSort.setComparator(getProduct, new Comparator<CustomerLogDetails>()
				{
			@Override
			public int compare(CustomerLogDetails e1,CustomerLogDetails e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getProduct()!=null && e2.getProduct()!=null){
						return e1.getProduct().compareTo(e2.getProduct());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	private void addSortinggetCallType(){
		List<CustomerLogDetails> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerLogDetails>(list);
		columnSort.setComparator(getCallType, new Comparator<CustomerLogDetails>()
				{
			@Override
			public int compare(CustomerLogDetails e1,CustomerLogDetails e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getCallType()!=null && e2.getCallType()!=null){
						return e1.getCallType().compareTo(e2.getCallType());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	private void addSortinggetWarrantyStatus(){
		List<CustomerLogDetails> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerLogDetails>(list);
		columnSort.setComparator(getWarrantyStatus, new Comparator<CustomerLogDetails>()
				{
			@Override
			public int compare(CustomerLogDetails e1,CustomerLogDetails e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getWarrantyStatus()!=null && e2.getWarrantyStatus()!=null){
						return e1.getWarrantyStatus().compareTo(e2.getWarrantyStatus());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	private void addSortinggetTechnician(){
		List<CustomerLogDetails> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerLogDetails>(list);
		columnSort.setComparator(getTechnician, new Comparator<CustomerLogDetails>()
				{
			@Override
			public int compare(CustomerLogDetails e1,CustomerLogDetails e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getTechnician()!=null && e2.getTechnician()!=null){
						return e1.getTechnician().compareTo(e2.getTechnician());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	private void addSortinggetStatus(){
		List<CustomerLogDetails> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerLogDetails>(list);
		columnSort.setComparator(getStatus, new Comparator<CustomerLogDetails>()
				{
			@Override
			public int compare(CustomerLogDetails e1,CustomerLogDetails e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getStatus()!=null && e2.getStatus()!=null){
						return e1.getStatus().compareTo(e2.getStatus());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	private void addSortinggetLocality(){
		List<CustomerLogDetails> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerLogDetails>(list);
		columnSort.setComparator(getLocality, new Comparator<CustomerLogDetails>()
				{
			@Override
			public int compare(CustomerLogDetails e1,CustomerLogDetails e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getAddress().getLocality()!=null && e2.getAddress().getLocality()!=null){
						return e1.getAddress().getLocality().compareTo(e2.getAddress().getLocality());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	 private void addgetCommunicationLogButtonButton() {

			ButtonCell btnCell = new ButtonCell();
			getCommunicationLogButton = new Column<CustomerLogDetails, String>(btnCell) {

				@Override
				public String getValue(CustomerLogDetails object) {
					// TODO Auto-generated method stub
					return "Visit Log";
				}
			};

			table.addColumn(getCommunicationLogButton, "Visit Log");
			table.setColumnWidth(getCommunicationLogButton, 90, Unit.PX);
		}

		private void updateColumngetMaterialIssueButton() {
			getCommunicationLogButton.setFieldUpdater(new FieldUpdater<CustomerLogDetails, String>() {
				@Override
				public void update(int index, CustomerLogDetails object, String value) {
					//form.showWaitSymbol();
					customerLog = object;
					MyQuerry querry = AppUtility.communicationLogQuerry(object.getCount(), object.getCompanyId(),AppConstants.SALESMODULE,"Customer Log Details");
					
					service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
						
						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							
							ArrayList<InteractionType> list = new ArrayList<InteractionType>();
							
							for(SuperModel model : result){
								
								InteractionType interactionType = (InteractionType) model;
								list.add(interactionType);
							}
							Comparator<InteractionType> comp=new Comparator<InteractionType>() {
								@Override
								public int compare(InteractionType e1, InteractionType e2) {
//									Integer coutn1=o1.getCount();
//									Integer count2=o2.getCount();
//									return ;
									if (e1.getCount() == e2.getCount()) {
										return 0;
									}
									if (e1.getCount() > e2.getCount()) {
										return 1;
									} else {
										return -1;
									}
								}
							};
							Collections.sort(list,comp);
							/**
							 * End
							 */
							communicationLogPopUp.getRemark().setValue("");
							communicationLogPopUp.getDueDate().setValue(null);
							communicationLogPopUp.getOblinteractionGroup().setSelectedIndex(0);
							communicationLogPopUp.getCommunicationLogTable().getDataprovider().setList(list);
							
							LoginPresenter.communicationLogPanel = new PopupPanel(true);
							LoginPresenter.communicationLogPanel.add(communicationLogPopUp);
							LoginPresenter.communicationLogPanel.show();
							LoginPresenter.communicationLogPanel.center();
						}
						
						@Override
						public void onFailure(Throwable caught) {
							//form.hideWaitSymbol();
						}
					});
				
				}
			});
		}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		updateColumngetMaterialIssueButton();
	}
	@Override
	public void onClick(ClickEvent event){
		if(event.getSource() == communicationLogPopUp.getBtnOk()){
			//form.showWaitSymbol();
		    List<InteractionType> list = communicationLogPopUp.getCommunicationLogTable().getDataprovider().getList();
		    ArrayList<InteractionType> interactionlist = new ArrayList<InteractionType>();
		    interactionlist.addAll(list);
		    boolean checkNewInteraction = AppUtility.checkNewInteractionAdded(interactionlist);
		    final GWTCAlert alert = new GWTCAlert();
		    if(checkNewInteraction==false){
		    alert.alert("Please add new interaction details");
		    //	form.hideWaitSymbol();
		    }else{	
		    	/**
		    	 * Date : 07-11-2017 KOMAL
		    	 */
		    	final Date followUpDate = interactionlist.get(interactionlist.size()-1).getInteractionDueDate();
		    	 communicationService.saveCommunicationLog(interactionlist, new AsyncCallback<Void>() {
					@Override
					public void onFailure(Throwable caught) {
						alert.alert("Unexpected Error");
						//form.hideWaitSymbol();
						LoginPresenter.communicationLogPanel.hide();
					}

					@Override
					public void onSuccess(Void result) {
						alert.alert("Data Save Successfully");
						LoginPresenter.communicationLogPanel.hide();
						customerLog = null;
					}
				});
		    }
		}
		if(event.getSource() == communicationLogPopUp.getBtnCancel()){
//			communicationPanel.hide();
			LoginPresenter.communicationLogPanel.hide();
			customerLog = null;
		}
		if(event.getSource() == communicationLogPopUp.getBtnAdd()){
				//form.showWaitSymbol();
				String remark = communicationLogPopUp.getRemark().getValue();
				Date dueDate = communicationLogPopUp.getDueDate().getValue();
				String interactiongGroup =null;
				if(communicationLogPopUp.getOblinteractionGroup().getSelectedIndex()!=0)
					interactiongGroup=communicationLogPopUp.getOblinteractionGroup().getValue(communicationLogPopUp.getOblinteractionGroup().getSelectedIndex());
				boolean validationFlag = AppUtility.validateCommunicationlog(remark,dueDate);
				if(validationFlag){
					InteractionType communicationLog =  AppUtility.getCommunicationLog(AppConstants.SALESMODULE,"Customer Log Details",customerLog.getCount(),customerLog.getTechnician()
							,remark,dueDate,customerLog.getCinfo(),null,interactiongGroup, customerLog.getCompanyName(),"");
					communicationLogPopUp.getCommunicationLogTable().getDataprovider().getList().add(communicationLog);
					communicationLogPopUp.getRemark().setValue("");
					communicationLogPopUp.getDueDate().setValue(null);
					communicationLogPopUp.getOblinteractionGroup().setSelectedIndex(0);
				}
				//form.hideWaitSymbol();
		}

	}
}
