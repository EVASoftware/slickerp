package com.slicktechnologies.client.views.customerlog;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.approvalutility.ApprovableScreen;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.EmailService;
import com.slicktechnologies.client.services.EmailServiceAsync;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.ApproverFactory;
import com.slicktechnologies.client.views.approval.ApprovalService;
import com.slicktechnologies.client.views.approval.ApprovalServiceAsync;
import com.slicktechnologies.client.views.customerlog.CustomerLogDetailsPresenter.CustomerLogDetailsPresenterTable;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.common.businessprocesslayer.ApprovableProcess;
import com.slicktechnologies.shared.common.businessprocesslayer.ConcreteBusinessProcess;
import com.slicktechnologies.shared.common.customerlog.CustomerLogDetails;
import com.slicktechnologies.shared.common.customerlog.MaterialRequired;
import com.slicktechnologies.shared.common.inventory.MaterialMovementNote;
import com.slicktechnologies.shared.common.inventory.MaterialProduct;
import com.slicktechnologies.shared.common.inventory.Storagebin;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.personlayer.Employee;

public class AccountTable extends CustomerLogDetailsPresenterTable implements ClickHandler {
	TextColumn<CustomerLogDetails> getColumnId;
	TextColumn<CustomerLogDetails> getColumnCustomerName;
	TextColumn<CustomerLogDetails> getComplaintNumber;
	TextColumn<CustomerLogDetails> getStatus;
	TextColumn<CustomerLogDetails> getProduct;
	TextColumn<CustomerLogDetails> getTechnician;
	TextColumn<CustomerLogDetails> getAppointmentDate;
	TextColumn<CustomerLogDetails> getCallBackStatus;
	TextColumn<CustomerLogDetails> getWarrantyStatus;
	TextColumn<CustomerLogDetails> getPaymentStatus;
	TextColumn<CustomerLogDetails> getCellNumber;
	TextColumn<CustomerLogDetails> getCreationDate;
	TextColumn<CustomerLogDetails> getCallType;
	Column<CustomerLogDetails, Boolean> checkRecord;
	CustomerLogDetails customerLogDeatils;
	GenricServiceAsync service = GWT.create(GenricService.class);
	ApprovalServiceAsync approvalService=GWT.create(ApprovalService.class);
	int rowIndex;
	GWTCAlert alert = new GWTCAlert();
	public AccountTable()
	{
		super();
		getTable().setHeight("500px");
		getTable().setWidth("1125px");

	}
	@Override
	public void createTable() {
		//addColumnCheckBox();
		addColumngetId();
		addColumnCustomerName();
		addCoumngetCellNumber();
		addColumngetComplaintNumber();
		addColumngetCreationDate();
		addColumngetStatus();
		addColumngetProduct();
		addColumngetTechnician();
		addColumngetCallType();
		addColumngetWarratyStatus();
		addColumngetCallBackStatus();
		addColumngetAppointmentDate();
		addFieldUpdater();
		addColumnSorting();
	}
	  protected void addCoumngetCellNumber(){
	    	getCellNumber = new TextColumn<CustomerLogDetails>() {
				@Override
				public String getValue(CustomerLogDetails object) {
						return object.getCinfo().getCellNumber()+"";
				}
			};
			table.addColumn(getCellNumber, "Cell Number");
			getCellNumber.setSortable(true);
			table.setColumnWidth(getCellNumber,"100px");
	    }
	protected void addColumngetId(){
		getColumnId = new TextColumn<CustomerLogDetails>() {
			@Override
			public String getValue(CustomerLogDetails object) {
					return object.getCount() + "";
			}
		};
		table.addColumn(getColumnId, "Id");
		addSortinggetID();
		getColumnId.setSortable(true);
	    table.setColumnWidth(getColumnId,"90px");
	}
	
	protected void addColumnCustomerName(){
		getColumnCustomerName = new TextColumn<CustomerLogDetails>() {
			@Override
			public String getValue(CustomerLogDetails object) {
					return object.getCinfo().getFullName();
			}
		};
		table.addColumn(getColumnCustomerName, "Customer Name");
		getColumnCustomerName.setSortable(true);
	    table.setColumnWidth(getColumnCustomerName,"120px");
	}
    protected void addColumngetComplaintNumber(){
    	getComplaintNumber = new TextColumn<CustomerLogDetails>() {
			@Override
			public String getValue(CustomerLogDetails object) {
				if(object.getComplaintNumber() == null){
					return "";
				}else{
					return object.getComplaintNumber()+ "";
				}
			}
		};
		table.addColumn(getComplaintNumber, "Complaint Number");
		getComplaintNumber.setSortable(true);
		table.setColumnWidth(getComplaintNumber,"90px");
    }
  
    protected void addColumngetStatus(){
    	getStatus = new TextColumn<CustomerLogDetails>() {
			@Override
			public String getValue(CustomerLogDetails object) {
					return object.getStatus();
			}
		};
		table.addColumn(getStatus, "Status");
		getStatus.setSortable(true);
		table.setColumnWidth(getStatus,"90px");
    }
  
    protected void addColumngetPaymentStatus(){
    	getPaymentStatus = new TextColumn<CustomerLogDetails>() {
			@Override
			public String getValue(CustomerLogDetails object) {
					return object.getPaymentStatus();
			}
		};
		table.addColumn(getPaymentStatus, "Payment Status");
		getPaymentStatus.setSortable(true);
		table.setColumnWidth(getPaymentStatus,"90px");
    }
    protected void addColumngetProduct(){
    	getProduct = new TextColumn<CustomerLogDetails>() {
			@Override
			public String getValue(CustomerLogDetails object) {
				if(object.getServiceCharges().size()>0){
					return object.getServiceCharges().get(0).getProductName();
				}
					return "";
			}
		};
		table.addColumn(getProduct, "Product");
		getProduct.setSortable(true);
		table.setColumnWidth(getProduct,"90px");
    }
    protected void addColumngetAppointmentDate(){
    	getAppointmentDate = new TextColumn<CustomerLogDetails>() {
        	@Override
    		public String getValue(CustomerLogDetails object)
    		{
    			if(object.getAppointmentDate()!=null){
    				return AppUtility.parseDate(object.getAppointmentDate());}
    			else{
    				return "";
    			}
    		}
    			};
    			table.addColumn(getAppointmentDate,"Appointment Date");
    			getAppointmentDate.setSortable(true);
    			table.setColumnWidth(getAppointmentDate,"90px");
    }

    protected void addColumngetTechnician(){
    	getTechnician = new TextColumn<CustomerLogDetails>() {
			@Override
			public String getValue(CustomerLogDetails object) {
					return object.getTechnician();
			}
		};
		table.addColumn(getTechnician, "Technician");
		getTechnician.setSortable(true);
		table.setColumnWidth(getTechnician,"90px");
    }
    protected void addColumngetCallBackStatus(){
    	getCallBackStatus = new TextColumn<CustomerLogDetails>() {
			@Override
			public String getValue(CustomerLogDetails object) {
					return object.getCallbackStatus();
			}
		};
		table.addColumn(getCallBackStatus, "CallBack Status");
		getTechnician.setSortable(true);
		table.setColumnWidth(getCallBackStatus,"90px");
    }
    protected void addColumngetWarratyStatus(){
    	getWarrantyStatus = new TextColumn<CustomerLogDetails>() {
			@Override
			public String getValue(CustomerLogDetails object) {
					return object.getWarrantyStatus();
			}
		};
		table.addColumn(getWarrantyStatus, "Warranty Staus");
		getWarrantyStatus.setSortable(true);
		table.setColumnWidth(getWarrantyStatus,"90px");
    }
	private void setFieldUpdateOnCheckbox()
	{
		System.out.println("In Field Updator");
		checkRecord.setFieldUpdater(new FieldUpdater<CustomerLogDetails, Boolean>() {
			@Override
			public void update(int index, CustomerLogDetails object, Boolean value) {
				try {
					object.setRecordSelect(value);
				} catch (Exception e) {
				}
				table.redrawRow(index);
			}
		});
	}
	protected void addColumngetCreationDate(){
		 getCreationDate = new TextColumn<CustomerLogDetails>() {
	        	@Override
	    		public String getValue(CustomerLogDetails object)
	    		{
	    			if(object.getCreatedOn()!=null){
	    				return AppUtility.parseDate(object.getCreatedOn());}
	    			else{
	    				return "";
	    			}
	    		}
	    			};
	    			table.addColumn(getCreationDate,"Creation Date");
	    			getCreationDate.setSortable(true);
	    			table.setColumnWidth(getCreationDate,"90px");
	    }
	private void addColumnCheckBox() {

		CheckboxCell checkCell = new CheckboxCell();
		checkRecord = new Column<CustomerLogDetails, Boolean>(checkCell) {

			@Override
			public Boolean getValue(CustomerLogDetails object) {

				return object.getRecordSelect();
			}
		};
		table.addColumn(checkRecord, "Select");
		table.setColumnWidth(checkRecord, 60, Unit.PX);

	}

	@Override
	public void setEnable(boolean state)
	{
	}
	@Override
	public void applyStyle()
	{
	}
	public void addColumnSorting(){
		addSortinggetID();
		addSortinggetCustomerName();
		addSortinggetCellNumber();
		addSortinggetComplaintNumber();
		addSortinggetCreationDate();
		addSortinggetStatus();
		addSortinggetProduct();
		addSortinggetTechnician();
		addSortinggetCallType();
		addSortinggetWarrantyStatus();
		addSortinggetCallBackStatus();
		addSortinggetAppointmentDate();
		//addColumngetPaymentStatus();
		
	}
	private void addSortinggetID() {
		List<CustomerLogDetails> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerLogDetails>(list);
		columnSort.setComparator(getColumnId, new Comparator<CustomerLogDetails>()
				{
			@Override
			public int compare(CustomerLogDetails e1,CustomerLogDetails e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);	
	}
	private void addSortinggetComplaintNumber(){
		List<CustomerLogDetails> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerLogDetails>(list);
		columnSort.setComparator(getComplaintNumber, new Comparator<CustomerLogDetails>()
				{
			@Override
			public int compare(CustomerLogDetails e1,CustomerLogDetails e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getComplaintNumber()!=null && e2.getComplaintNumber()!=null){
						return e1.getComplaintNumber().compareTo(e2.getComplaintNumber());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	private void addSortinggetCustomerName(){

		List<CustomerLogDetails> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerLogDetails>(list);
		columnSort.setComparator(getColumnCustomerName, new Comparator<CustomerLogDetails>()
				{
			@Override
			public int compare(CustomerLogDetails e1,CustomerLogDetails e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getCinfo().getFullName()!=null && e2.getCinfo().getFullName()!=null){
						return e1.getCinfo().getFullName().compareTo(e2.getCinfo().getFullName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);	
	}
	private void addSortinggetCreationDate(){
		  List<CustomerLogDetails> list=getDataprovider().getList();
		  columnSort=new ListHandler<CustomerLogDetails>(list);
		  columnSort.setComparator(getCreationDate, new Comparator<CustomerLogDetails>()
		  {
		  @Override
		  public int compare(CustomerLogDetails e1,CustomerLogDetails e2)
		  {
		  if(e1!=null && e2!=null)
		  {
			  if(e1.getCreatedOn() == null && e2.getCreatedOn()==null){
			        return 0;
			    }
			    if(e1.getCreatedOn() == null) return 1;
			    if(e2.getCreatedOn() == null) return -1;
			    return e1.getCreatedOn().compareTo(e2.getCreatedOn());
		  }
		  return 0;
		  }
		  });
		  table.addColumnSortHandler(columnSort);  
	}
	private void addSortinggetAppointmentDate(){
		  List<CustomerLogDetails> list=getDataprovider().getList();
		  columnSort=new ListHandler<CustomerLogDetails>(list);
		  columnSort.setComparator(getAppointmentDate, new Comparator<CustomerLogDetails>()
		  {
		  @Override
		  public int compare(CustomerLogDetails e1,CustomerLogDetails e2)
		  {
		  if(e1!=null && e2!=null)
		  {
			  if(e1.getAppointmentDate() == null && e2.getAppointmentDate()==null){
			        return 0;
			    }
			    if(e1.getAppointmentDate() == null) return 1;
			    if(e2.getAppointmentDate() == null) return -1;
			    return e1.getAppointmentDate().compareTo(e2.getAppointmentDate());
		  }
		  return 0;
		  }
		  });
		  table.addColumnSortHandler(columnSort); 
	}
	private void addSortinggetProduct(){
		List<CustomerLogDetails> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerLogDetails>(list);
		columnSort.setComparator(getProduct, new Comparator<CustomerLogDetails>()
				{
			@Override
			public int compare(CustomerLogDetails e1,CustomerLogDetails e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getProduct()!=null && e2.getProduct()!=null){
						return e1.getProduct().compareTo(e2.getProduct());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	private void addSortinggetWarrantyStatus(){
		List<CustomerLogDetails> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerLogDetails>(list);
		columnSort.setComparator(getWarrantyStatus, new Comparator<CustomerLogDetails>()
				{
			@Override
			public int compare(CustomerLogDetails e1,CustomerLogDetails e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getWarrantyStatus()!=null && e2.getWarrantyStatus()!=null){
						return e1.getWarrantyStatus().compareTo(e2.getWarrantyStatus());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	private void addSortinggetTechnician(){
		List<CustomerLogDetails> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerLogDetails>(list);
		columnSort.setComparator(getTechnician, new Comparator<CustomerLogDetails>()
				{
			@Override
			public int compare(CustomerLogDetails e1,CustomerLogDetails e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getTechnician()!=null && e2.getTechnician()!=null){
						return e1.getTechnician().compareTo(e2.getTechnician());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	private void addSortinggetStatus(){
		List<CustomerLogDetails> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerLogDetails>(list);
		columnSort.setComparator(getStatus, new Comparator<CustomerLogDetails>()
				{
			@Override
			public int compare(CustomerLogDetails e1,CustomerLogDetails e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getStatus()!=null && e2.getStatus()!=null){
						return e1.getStatus().compareTo(e2.getStatus());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	private void addSortinggetCallBackStatus(){
		List<CustomerLogDetails> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerLogDetails>(list);
		columnSort.setComparator(getCallBackStatus, new Comparator<CustomerLogDetails>()
				{
			@Override
			public int compare(CustomerLogDetails e1,CustomerLogDetails e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getCallbackStatus()!=null && e2.getCallbackStatus()!=null){
						return e1.getCallbackStatus().compareTo(e2.getCallbackStatus());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);	
	}
	private void addSortinggetCellNumber(){
		List<CustomerLogDetails> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerLogDetails>(list);
		columnSort.setComparator(getCellNumber, new Comparator<CustomerLogDetails>()
				{
			@Override
			public int compare(CustomerLogDetails e1,CustomerLogDetails e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCinfo().getCellNumber()== e2.getCinfo().getCellNumber()){
						return 0;}
					if(e1.getCinfo().getCellNumber()> e2.getCinfo().getCellNumber()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);	
	}
	 protected void addColumngetCallType(){
	    	getCallType = new TextColumn<CustomerLogDetails>() {
				@Override
				public String getValue(CustomerLogDetails object) {
						return object.getCallType();
				}
			};
			table.addColumn(getCallType, "Call Type");
			getCallType.setSortable(true);
			table.setColumnWidth(getCallType,"90px");
	    }
	 
	 private void addSortinggetCallType(){
			List<CustomerLogDetails> list=getDataprovider().getList();
			columnSort=new ListHandler<CustomerLogDetails>(list);
			columnSort.setComparator(getCallType, new Comparator<CustomerLogDetails>()
					{
				@Override
				public int compare(CustomerLogDetails e1,CustomerLogDetails e2)
				{
					if(e1!=null && e2!=null)
					{
						if( e1.getCallType()!=null && e2.getCallType()!=null){
							return e1.getCallType().compareTo(e2.getCallType());}
					}
					else{
						return 0;}
					return 0;
				}
					});
			table.addColumnSortHandler(columnSort);
		}
	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		//setFieldUpdateOnCheckbox();
	}
	@Override
	public void onClick(ClickEvent event) {
		
	}

}


