package com.slicktechnologies.client.views.customerlog;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreen;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreenPresenter;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.inventory.MaterialMovementNote;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;


public class DefectiveMaterialPresenter extends TableScreenPresenter<MaterialMovementNote> {
		
		TableScreen<MaterialMovementNote>form;
		GeneralServiceAsync service = GWT.create(GeneralService.class);
		GenricServiceAsync genasync = GWT.create(GenricService.class);
		public DefectiveMaterialPresenter(TableScreen<MaterialMovementNote> view,MaterialMovementNote model) {
			super(view, model);
			form=view;
			view.retriveTable(getDefectiveQuery());
		}
		
		public DefectiveMaterialPresenter(TableScreen<MaterialMovementNote> view,MaterialMovementNote model,MyQuerry querry) {
			super(view, model);
			form=view;
			view.retriveTable(querry);
		}

		public static void initalize()
		{
			DefectiveMaterialTable table=new DefectiveMaterialTable();
			DefectiveMaterialForm form=new DefectiveMaterialForm(table);
			DefectiveMaterialSearchProxy.staticSuperTable=table;
			DefectiveMaterialSearchProxy searchPopUp=new DefectiveMaterialSearchProxy(false);
			form.setSearchpopupscreen(searchPopUp);
			DefectiveMaterialPresenter presenter=new DefectiveMaterialPresenter(form , new MaterialMovementNote());
			form.setToViewState();
			AppMemory.getAppMemory().stickPnel(form);
		}
		public static void initalize(MyQuerry querry)
		{
			AuthorizationHelper.setAsPerAuthorization(Screen.DEFECTIVEMATERIALLIST,LoginPresenter.currentModule.trim());		
			DefectiveMaterialTable table=new DefectiveMaterialTable();
			DefectiveMaterialForm form=new DefectiveMaterialForm(table);
			
			DefectiveMaterialSearchProxy.staticSuperTable=table;
			DefectiveMaterialSearchProxy searchPopUp=new DefectiveMaterialSearchProxy(false);
			form.setSearchpopupscreen(searchPopUp);
			
			DefectiveMaterialPresenter presenter=new DefectiveMaterialPresenter(form, new MaterialMovementNote(),querry);
			form.setToViewState();
			AppMemory.getAppMemory().stickPnel(form);
		}
		 
		@Override
		public void reactToProcessBarEvents(ClickEvent e) {
			InlineLabel label=(InlineLabel) e.getSource();
			String text=label.getText().trim();
			if(text.equalsIgnoreCase("Send Defective")){
				final ArrayList<MaterialMovementNote> selectedlist = getAllSelectedRecords();
				if(selectedlist.size() !=0){
					System.out.println("Company Id :" + model.getCompanyId());
					service.sendDefective(selectedlist, model.getCompanyId(), LoginPresenter.loggedInUser ,new AsyncCallback<String>() {						
						@Override
						public void onSuccess(String result) {
							// TODO Auto-generated method stub
							form.showDialogMessage("Data sent to company successfully");
							for(MaterialMovementNote m : selectedlist){
								m.setRecordSelect(false);
							}
							form.retriveTable(getDefectiveQuery());
						}
						
						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							form.showDialogMessage("error : "+ caught);
						}
					});
				}
			}
		}

		@Override
		public void onClick(ClickEvent event) {
			super.onClick(event);
		}

		@Override
		public void reactOnPrint() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void reactOnEmail() {
			// TODO Auto-generated method stub
			
		}

		private ArrayList<MaterialMovementNote> getAllSelectedRecords() 	
		{
			List<MaterialMovementNote> listMaterialTable=form.getSuperTable().getDataprovider().getList();
			System.out.println("table===="+listMaterialTable.size());
			 ArrayList<MaterialMovementNote> selectedlist=new ArrayList<MaterialMovementNote>();
				 for(MaterialMovementNote m:listMaterialTable)
				 {
					 if(m.isRecordSelect()==true){
//						  if(m.getMmnGRNStatus().equalsIgnoreCase(AppConstants.MATERIALPENDING) || m.getMmnGRNStatus().equalsIgnoreCase(AppConstants.CREDITRECEIVED)){
							 selectedlist.add(m);
//						 }
					 }
				 }
				 return selectedlist;
		}
		private MyQuerry getDefectiveQuery(){
			List<String> statusList = new ArrayList<String>();
			statusList.add(AppConstants.MATERIALPENDING);
			statusList.add(AppConstants.CREDITRECEIVED);
	        //form.showDialogMessage("company id :" + model.getCompanyId());
	//		statusList.add(AppConstants.SENTTOCOMPANY);
			Vector<Filter> filtervec = new Vector<Filter>();
			Filter temp = null;
			temp = new Filter();
			temp.setLongValue(model.getCompanyId());
			temp.setQuerryString("companyId");
			filtervec.add(temp);
                temp = new Filter();
				temp.setStringValue("Return");
				temp.setQuerryString("mmnTransactionType");
				filtervec.add(temp);
				
				temp = new Filter();
				temp.setList(statusList);
				temp.setQuerryString("mmnGRNStatus IN");
				filtervec.add(temp);
			
				List<String> branchList = new ArrayList<String>();
				for(Branch b : LoginPresenter.globalBranch){
					branchList.add(b.getBusinessUnitName());
				}
				temp = new Filter();
				temp.setQuerryString("branch IN");
				temp.setList(branchList);
				filtervec.add(temp);
//
			MyQuerry querry = new MyQuerry();
			querry.setFilters(filtervec);
			querry.setQuerryObject(new MaterialMovementNote());
            return querry;
		}
}
