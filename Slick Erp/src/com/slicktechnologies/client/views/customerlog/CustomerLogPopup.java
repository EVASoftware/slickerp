package com.slicktechnologies.client.views.customerlog;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.LongBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.composite.AddressComposite;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.mywidgets.MyInlineLabel;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.interaction.interactiondetails.InteractionPresenter;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.customerlog.CustomerLogDetails;
import com.slicktechnologies.shared.common.customerlog.MaterialRequired;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.workorder.WorkOrderProductDetails;

public class CustomerLogPopup extends PopupScreen implements ClickHandler{
	PersonInfoComposite personInfoComposite;
	TextBox tbComplaintNumber;
	TextBox tbCustomerName;
	LongBox lbCellNumber;
	ObjectListBox<ConfigCategory> olbWarrantyStatus;
	ObjectListBox<Employee> olbTechnician;
	ObjectListBox<ConfigCategory> olbCallType;
	//ObjectListBox<Customer> olbCompany;
	//Branch
	ObjectListBox<Branch> olbCompany;
    CheckBox cbRepeatCall;  
    Button btnNewCustomer;
	CustomerLogDetails customerLogObj;
	
	public CustomerLogPopup(){
		super();
		getPopup().getElement().addClassName("popupCustomerLog");
		createGui();
	}
	private void initializeWidget() {
		personInfoComposite=AppUtility.customerInfoComposite(new Customer());
		personInfoComposite.getCustomerId().getHeaderLabel().setText("* Customer ID");
		personInfoComposite.getCustomerName().getHeaderLabel().setText("* Customer Name");
		personInfoComposite.getCustomerCell().getHeaderLabel().setText("* Customer Cell");
		tbComplaintNumber = new TextBox();
		tbCustomerName = new TextBox();
		lbCellNumber = new LongBox();
		olbWarrantyStatus = new ObjectListBox<ConfigCategory>(); 
		AppUtility.MakeLiveConfig(olbWarrantyStatus,Screen.WARRANTYSTATUS);
		olbTechnician = new ObjectListBox<Employee>();
		olbTechnician.makeEmployeeLive(AppConstants.SALESMODULE, "Customer Call Log Details", "Technician");
        olbCallType = new ObjectListBox<ConfigCategory>();
        AppUtility.MakeLiveConfig(olbCallType,Screen.CALLTYPE);
        //olbCompany = new ObjectListBox<Customer>();
        olbCompany = new ObjectListBox<Branch>();
		//AppUtility.makeCompanyNameListBoxLive(olbCompany);
		AppUtility.makeBranchListBoxLive(olbCompany);
		cbRepeatCall = new CheckBox("IsRepeat Call");
		cbRepeatCall.setValue(false);
		btnNewCustomer = new Button("New Customer");
		
		}

	@Override
	public void createScreen() {
		// TODO Auto-generated method stub	
		initializeWidget();		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingCustomerCallLog = fbuilder.setlabel("Customer Call Log").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();//POPUPGROPING
		fbuilder = new FormFieldBuilder("* Warranty Status",olbWarrantyStatus);
		FormField folbWarrantyStatus = fbuilder.setMandatory(true).setMandatoryMsg("WarrantyStatus is mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Call Type",olbCallType);
		FormField folbCallType= fbuilder.setMandatory(true).setMandatoryMsg("CallType is mandatory!").setRowSpan(0).setColSpan(0).build();	
		fbuilder = new FormFieldBuilder("Technician",olbTechnician);
		FormField folbTechnician= fbuilder.setMandatory(false).setMandatoryMsg("Technician is mandatory!").setRowSpan(0).setColSpan(0).build();			
		fbuilder = new FormFieldBuilder("* Company",olbCompany);
		FormField folbCompany= fbuilder.setMandatory(true).setMandatoryMsg("Company is mandatory!").setRowSpan(0).setColSpan(0).build();	
		fbuilder = new FormFieldBuilder("* Customer Name",tbCustomerName);
		FormField ftbCustomerName= fbuilder.setMandatory(true).setMandatoryMsg("CustomerName is mandatory!").setRowSpan(0).setColSpan(0).build();	
		fbuilder = new FormFieldBuilder("* Cell No",lbCellNumber);
		FormField ftbCellNumber= fbuilder.setMandatory(true).setMandatoryMsg("CellNo is mandatory!").setRowSpan(0).setColSpan(0).build();
		FormField ftbComplaintNumber;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CustomerLogDetails", "MakeComplainNumberNonMandatory")){
			fbuilder = new FormFieldBuilder("Complaint Number",tbComplaintNumber);
			ftbComplaintNumber= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}else{
			fbuilder = new FormFieldBuilder("* Complaint Number",tbComplaintNumber);
			ftbComplaintNumber= fbuilder.setMandatory(true).setMandatoryMsg("ComplaintNumber is mandatory!").setRowSpan(0).setColSpan(0).build();
		}
		fbuilder = new FormFieldBuilder();
		FormField fgroupingCustomerInformation=fbuilder.setlabel("Customer Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("",personInfoComposite);
		FormField fpersonInfoComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("",cbRepeatCall);
		FormField fcbRepeatCall= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		fbuilder = new FormFieldBuilder("",btnNewCustomer);
		FormField fbtnNewCustomer= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();

		FormField[][] formfield = {
				{fgroupingCustomerInformation},
				{fbtnNewCustomer },
				{fpersonInfoComposite },
				{ fgroupingCustomerCallLog },
				{ ftbComplaintNumber ,folbCompany ,  folbCallType },//,ftbCustomerName , ftbCellNumber },
				{ folbTechnician , folbWarrantyStatus , fcbRepeatCall},
				};
				this.fields = formfield;
	}

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
		return super.validate();	
	}

	public TextBox getTbCustomerName() {
		return tbCustomerName;
	}

	public void setTbCustomerName(TextBox tbCustomerName) {
		this.tbCustomerName = tbCustomerName;
	}
	public LongBox getLbCellNumber() {
		return lbCellNumber;
	}

	public void setLbCellNumber(LongBox lbCellNumber) {
		this.lbCellNumber = lbCellNumber;
	}

	public ObjectListBox<ConfigCategory> getOlbWarrantyStatus() {
		return olbWarrantyStatus;
	}

	public void setOlbWarrantyStatus(ObjectListBox<ConfigCategory> olbWarrantyStatus) {
		this.olbWarrantyStatus = olbWarrantyStatus;
	}

	public ObjectListBox<Employee> getOlbTechnician() {
		return olbTechnician;
	}

	public void setOlbTechnician(ObjectListBox<Employee> olbTechnician) {
		this.olbTechnician = olbTechnician;
	}

	public ObjectListBox<ConfigCategory> getOlbCallType() {
		return olbCallType;
	}

	public void setOlbCallType(ObjectListBox<ConfigCategory> olbCallType) {
		this.olbCallType = olbCallType;
	}

	public ObjectListBox<Branch> getOlbCompany() {
		return olbCompany;
	}

	public void setOlbCompany(ObjectListBox<Branch> olbCompany) {
		this.olbCompany = olbCompany;
	}

	public TextBox getTbComplaintNumber() {
		return tbComplaintNumber;
	}

	public void setTbComplaintNumber(TextBox tbComplaintNumber) {
		this.tbComplaintNumber = tbComplaintNumber;
	}
	public PersonInfoComposite getPersonInfoComposite() {
		return personInfoComposite;
	}
	public void setPersonInfoComposite(PersonInfoComposite personInfoComposite) {
		this.personInfoComposite = personInfoComposite;
	}
	public Button getBtnNewCustomer() {
		return btnNewCustomer;
	}
	public void setBtnNewCustomer(Button btnNewCustomer) {
		this.btnNewCustomer = btnNewCustomer;
	}

	
}







