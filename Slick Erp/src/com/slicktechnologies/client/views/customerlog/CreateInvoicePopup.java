package com.slicktechnologies.client.views.customerlog;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.AddressComposite;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.MyInlineLabel;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.interaction.interactiondetails.InteractionPresenter;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.customerlog.CustomerLogDetails;
import com.slicktechnologies.shared.common.customerlog.MaterialRequired;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.servicerelated.ServiceSchedule;
import com.slicktechnologies.shared.common.workorder.WorkOrderProductDetails;

public class CreateInvoicePopup extends PopupScreen implements ChangeHandler , RowCountChangeEvent.Handler{
	NumberFormat nf=NumberFormat.getFormat("0.00");
	TextBox tbComplaintNumber , tbLogId , tbBookNo , tbTCRNo ,tbCompanyName ,tbWarrantyStatus;
	TextBox tbbankAccNo,tbbankName,tbbankBranch,tbchequeNo,tbreferenceno,tbChequeIssuedBy;
	TextBox tbTotalAmount ,tbDiscountAmount ,tbNetPayable, tbAmountReceived ,tbBalanceAmount;
	DateBox dbChequeDate,dbtransferDate;
	ProductInfoComposite productInfoComposite ;
	ServiceProductTable serviceCharegesTable;
	ObjectListBox<Config> olbPaymentMethods;
	ServiceProductTable materialTable , customerTable;
	Button btnAdd;
	CustomerLogDetails customerLogObj;
	
	
	public CreateInvoicePopup(){
		super();
		getPopup().getElement().addClassName("popupCreateInvoice");
		this.serviceCharegesTable.connectToLocal();
		this.materialTable.connectToLocal();
		this.customerTable.connectToLocal();
		this.getTbLogId().setEnabled(false);
		this.getTbComplaintNumber().setEnabled(false);
		this.getTbCompanyName().setEnabled(false);
		this.getTbWarrantyStatus().setEnabled(false);
		this.tbNetPayable.setEnabled(false);
		this.tbTotalAmount.setEnabled(false);
		this.tbBalanceAmount.setEnabled(false);
		this.getTbDiscountAmount().addChangeHandler(this);
		this.getTbAmountReceived().addChangeHandler(this);
		this.getServiceCharegesTable().getTable().addRowCountChangeHandler(this);
		this.getCustomerTable().getTable().addRowCountChangeHandler(this);
		this.getMaterialTable().getTable().addRowCountChangeHandler(this);
		createGui();
	}
	
	private void initializeWidget() {
		tbComplaintNumber = new TextBox();
		tbLogId = new TextBox();
		tbBookNo = new TextBox();
		tbTCRNo = new TextBox();
		tbCompanyName =  new TextBox();
		tbWarrantyStatus = new TextBox();
		tbbankAccNo = new TextBox();
		tbbankName =  new TextBox();
		tbbankBranch =  new TextBox();
		tbchequeNo =  new TextBox();
		tbreferenceno =  new TextBox();
		tbChequeIssuedBy = new TextBox();
		tbTotalAmount= new TextBox();
		tbDiscountAmount = new TextBox();
		tbNetPayable = new TextBox();
		tbAmountReceived = new TextBox() ;
		tbBalanceAmount = new TextBox();
		dbChequeDate = new DateBox();
		dbtransferDate = new DateBox();
		productInfoComposite = AppUtility.initiatePurchaseProductComposite(new ServiceProduct());
		serviceCharegesTable = new ServiceProductTable();
		btnAdd = new Button("ADD");
		btnAdd.addClickHandler(this);
		olbPaymentMethods=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbPaymentMethods,Screen.PAYMENTMETHODS);
		olbPaymentMethods.addChangeHandler(this);
		materialTable = new ServiceProductTable();
		customerTable = new ServiceProductTable();
		}

	@Override
	public void createScreen() {
		// TODO Auto-generated method stub	
		initializeWidget();		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingCustomerCallLog = fbuilder.setlabel("Service Charge Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Complaint Number",tbComplaintNumber);
		FormField ftbComplaintNumber = fbuilder.setMandatory(true).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Call Log Id", tbLogId);
		FormField ftbLogId= fbuilder.setMandatory(true).setRowSpan(0).setColSpan(0).build();	
		fbuilder = new FormFieldBuilder("Book No.", tbBookNo);
		FormField ftbBookNo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();			
		fbuilder = new FormFieldBuilder("TCR No.", tbTCRNo);
		FormField ftbTCRNo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Company Name", tbCompanyName);
		FormField ftbCompanyName= fbuilder.setMandatory(true).setRowSpan(0).setColSpan(0).build();			
		fbuilder = new FormFieldBuilder("Warranty Status", tbWarrantyStatus);
		FormField ftbWarrantyStatus= fbuilder.setMandatory(true).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder();
		FormField fmrnSubProductGrouping = fbuilder.setlabel("Service Charge Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("", productInfoComposite);
		FormField fproductInfoComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		fbuilder = new FormFieldBuilder("", btnAdd);
		FormField fbtnAdd= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();	
		fbuilder = new FormFieldBuilder("", serviceCharegesTable.getTable());
		FormField fServiceCharegesTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fPaymentInformation=fbuilder.setlabel("Customer Payment Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();		
		fbuilder = new FormFieldBuilder("Cheque-Bank Account No.",tbbankAccNo);
		FormField ftbbankAccNo=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Cheque-Bank Name",tbbankName);
		FormField ftbbankName=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Cheque-Bank Branch",tbbankBranch);
		FormField ftbbankBranch=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Cheque No",tbchequeNo);
		FormField fibchequeNo=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Cheque Date",dbChequeDate);
		FormField fdbChequeDate=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Reference No.",tbreferenceno);
		FormField ftbreferenceno=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("NEFT-Transfer Date",dbtransferDate);
		FormField fdbtransferDate=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Cheque Issued By",tbChequeIssuedBy);
		FormField ftbChequeIssuedBy=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Payment Methods",olbPaymentMethods);
		FormField folbcPaymentMethods= fbuilder.setMandatory(false).setMandatoryMsg("Payment Methods is Mandatory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("TotalAmount",tbTotalAmount);
		FormField ftbTotalAmount=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Discount",tbDiscountAmount);
		FormField ftbDiscountAmount=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Net Payable",tbNetPayable);
		FormField ftbNetPayable=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Amount Received",tbAmountReceived);
		FormField ftbAmountReceived=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		//tbBalanceAmount
		fbuilder = new FormFieldBuilder("Balance Amount",tbBalanceAmount);
		FormField ftbBalanceAmount=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder();
		FormField fcustomerPayment=fbuilder.setlabel("Customer Material Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("", materialTable.getTable());
		FormField fmaterialTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder();
		FormField fcompanyPayment=fbuilder.setlabel("Company Material Deatils").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("", customerTable.getTable());
		FormField fcustomerTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		FormField[][] formfield = {
				{ fgroupingCustomerCallLog },
				{ ftbComplaintNumber ,ftbLogId , ftbCompanyName , ftbWarrantyStatus},
				{ fcompanyPayment } ,
				{ fmaterialTable },
				{ fcustomerPayment},
				{ fcustomerTable },
				{ fmrnSubProductGrouping },
				{ fproductInfoComposite, fbtnAdd},
				{ fServiceCharegesTable },
				{ fPaymentInformation},
				{ ftbTotalAmount , ftbDiscountAmount , ftbNetPayable },
				{ folbcPaymentMethods ,ftbBookNo , ftbTCRNo , ftbAmountReceived},
				{ ftbBalanceAmount},
				{ ftbbankAccNo, ftbbankName, ftbbankBranch ,fibchequeNo },
				{ fdbChequeDate,ftbreferenceno ,fdbtransferDate ,ftbChequeIssuedBy }
		};
				this.fields = formfield;
	}

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		//MaterialRequired m;
		if(event.getSource().equals(btnAdd)){
		  addProductToTable(serviceCharegesTable , productInfoComposite);
		  
//			if(m  != null)
//				serviceCharegesTable.getDataprovider().getList().add(m);
		  //calcaulateTotalAmount();
		}
		
	}
	@Override
	public void onChange(ChangeEvent event) {
		String payMethodVal=null;
		if(this.olbPaymentMethods.getSelectedIndex()!=0)
		{
			payMethodVal=olbPaymentMethods.getValue(olbPaymentMethods.getSelectedIndex()).trim();
			if(payMethodVal.equalsIgnoreCase("Cash"))
			{				
				this.tbbankAccNo.setEnabled(false);
				this.tbbankName.setEnabled(false);
				this.tbbankBranch.setEnabled(false);
				this.tbchequeNo.setEnabled(false);
				this.dbChequeDate.setEnabled(false);
				this.tbreferenceno.setEnabled(false);
				this.dbtransferDate.setEnabled(false);
			}
			else if(payMethodVal.equalsIgnoreCase("Cheque"))
			{			
				this.tbbankAccNo.setEnabled(true);
				this.tbbankName.setEnabled(true);
				this.tbbankBranch.setEnabled(true);
				this.tbchequeNo.setEnabled(true);
				this.dbChequeDate.setEnabled(true);
				this.tbreferenceno.setEnabled(false);
				this.dbtransferDate.setEnabled(false);
			}	
			else if(payMethodVal.equalsIgnoreCase("NEFT"))
			{			
				this.tbbankAccNo.setEnabled(false);
				this.tbbankName.setEnabled(false);
				this.tbbankBranch.setEnabled(false);
				this.tbchequeNo.setEnabled(false);
				this.dbChequeDate.setEnabled(false);
				this.tbreferenceno.setEnabled(true);
				this.dbtransferDate.setEnabled(true);
			}
		}		
		else
		{
			this.tbbankAccNo.setEnabled(false);
			this.tbbankName.setEnabled(false);
			this.tbbankBranch.setEnabled(false);
			this.tbchequeNo.setEnabled(false);
			this.dbChequeDate.setEnabled(false);
			this.tbreferenceno.setEnabled(false);
			this.dbtransferDate.setEnabled(false);
		}
		if(event.getSource().equals(getTbDiscountAmount())){
			Double total = 0.0d;
			if(this.getTbDiscountAmount().getValue()!=null && this.getTbTotalAmount().getValue()!=null){
				total = Double.parseDouble(this.getTbTotalAmount().getValue()) - Double.parseDouble(this.getTbDiscountAmount().getValue());
			}
			this.tbNetPayable.setValue(nf.format(total)+"");
			//this.tbAmountReceived.setValue(nf.format(total)+"");
		}
		if(event.getSource().equals(getTbAmountReceived())){

			Double balance = 0.0d;
			if(this.getTbAmountReceived().getValue()!=null){
				balance = Double.parseDouble(this.getTbNetPayable().getValue()) - Double.parseDouble(this.getTbAmountReceived().getValue());
			}
			this.tbBalanceAmount.setValue(nf.format(balance)+"");
		
		}
	}
	public TextBox getTbComplaintNumber() {
		return tbComplaintNumber;
	}

	public void setTbComplaintNumber(TextBox tbComplaintNumber) {
		this.tbComplaintNumber = tbComplaintNumber;
	}

	public TextBox getTbLogId() {
		return tbLogId;
	}

	public void setTbLogId(TextBox tbLogId) {
		this.tbLogId = tbLogId;
	}

	public ServiceProductTable getServiceCharegesTable() {
		return serviceCharegesTable;
	}

	public void setServiceCharegesTable(ServiceProductTable serviceCharegesTable) {
		this.serviceCharegesTable = serviceCharegesTable;
	}

	public Button getBtnAdd() {
		return btnAdd;
	}

	public void setBtnAdd(Button btnAdd) {
		this.btnAdd = btnAdd;
	}

	public CustomerLogDetails getCustomerLogObj() {
		return customerLogObj;
	}

	public void setCustomerLogObj(CustomerLogDetails customerLogObj) {
		this.customerLogObj = customerLogObj;
	}

	public TextBox getTbBookNo() {
		return tbBookNo;
	}

	public void setTbBookNo(TextBox tbBookNo) {
		this.tbBookNo = tbBookNo;
	}

	public TextBox getTbTCRNo() {
		return tbTCRNo;
	}

	public void setTbTCRNo(TextBox tbTCRNo) {
		this.tbTCRNo = tbTCRNo;
	}
	
	public TextBox getTbCompanyName() {
		return tbCompanyName;
	}

	public void setTbCompanyName(TextBox tbCompanyName) {
		this.tbCompanyName = tbCompanyName;
	}

	public TextBox getTbWarrantyStatus() {
		return tbWarrantyStatus;
	}

	public void setTbWarrantyStatus(TextBox tbWarrantyStatus) {
		this.tbWarrantyStatus = tbWarrantyStatus;
	}

	public ServiceProductTable getMaterialTable() {
		return materialTable;
	}

	public void setMaterialTable(ServiceProductTable materialTable) {
		this.materialTable = materialTable;
	}

	public TextBox getTbbankAccNo() {
		return tbbankAccNo;
	}

	public void setTbbankAccNo(TextBox tbbankAccNo) {
		this.tbbankAccNo = tbbankAccNo;
	}

	public TextBox getTbbankName() {
		return tbbankName;
	}

	public void setTbbankName(TextBox tbbankName) {
		this.tbbankName = tbbankName;
	}

	public TextBox getTbbankBranch() {
		return tbbankBranch;
	}

	public void setTbbankBranch(TextBox tbbankBranch) {
		this.tbbankBranch = tbbankBranch;
	}

	public TextBox getTbchequeNo() {
		return tbchequeNo;
	}

	public void setTbchequeNo(TextBox tbchequeNo) {
		this.tbchequeNo = tbchequeNo;
	}

	public TextBox getTbreferenceno() {
		return tbreferenceno;
	}

	public void setTbreferenceno(TextBox tbreferenceno) {
		this.tbreferenceno = tbreferenceno;
	}

	public TextBox getTbChequeIssuedBy() {
		return tbChequeIssuedBy;
	}

	public void setTbChequeIssuedBy(TextBox tbChequeIssuedBy) {
		this.tbChequeIssuedBy = tbChequeIssuedBy;
	}

	public TextBox getTbTotalAmount() {
		return tbTotalAmount;
	}

	public void setTbTotalAmount(TextBox tbTotalAmount) {
		this.tbTotalAmount = tbTotalAmount;
	}

	public TextBox getTbDiscountAmount() {
		return tbDiscountAmount;
	}

	public void setTbDiscountAmount(TextBox tbDiscountAmount) {
		this.tbDiscountAmount = tbDiscountAmount;
	}

	public TextBox getTbNetPayable() {
		return tbNetPayable;
	}

	public void setTbNetPayable(TextBox tbNetPayable) {
		this.tbNetPayable = tbNetPayable;
	}

//	public TextBox getTbAmountReceived() {
//		return tbAmountReceived;
//	}
//
//	public void setTbAmountReceived(TextBox tbAmountReceived) {
//		this.tbAmountReceived = tbAmountReceived;
//	}

	public DateBox getDbChequeDate() {
		return dbChequeDate;
	}

	public void setDbChequeDate(DateBox dbChequeDate) {
		this.dbChequeDate = dbChequeDate;
	}

	public DateBox getDbtransferDate() {
		return dbtransferDate;
	}

	public void setDbtransferDate(DateBox dbtransferDate) {
		this.dbtransferDate = dbtransferDate;
	}

	public ProductInfoComposite getProductInfoComposite() {
		return productInfoComposite;
	}

	public void setProductInfoComposite(ProductInfoComposite productInfoComposite) {
		this.productInfoComposite = productInfoComposite;
	}

	public ObjectListBox<Config> getOlbPaymentMethods() {
		return olbPaymentMethods;
	}

	public void setOlbcPaymentMethods(ObjectListBox<Config> olbPaymentMethods) {
		this.olbPaymentMethods = olbPaymentMethods;
	}

	public TextBox getTbAmountReceived() {
		return tbAmountReceived;
	}

	public void setTbAmountReceived(TextBox tbAmountReceived) {
		this.tbAmountReceived = tbAmountReceived;
	}

	public TextBox getTbBalanceAmount() {
		return tbBalanceAmount;
	}

	public void setTbBalanceAmont(TextBox tbBalanceAmount) {
		this.tbBalanceAmount = tbBalanceAmount;
	}

	public ServiceProductTable getCustomerTable() {
		return customerTable;
	}

	public void setCustomerTable(ServiceProductTable customerTable) {
		this.customerTable = customerTable;
	}

	public void addProductToTable(ServiceProductTable MaterialTable , final ProductInfoComposite productInfoComposite) {
		if (productInfoComposite.getValue() != null) {
			if (validateProduct(MaterialTable , productInfoComposite)) {
				final GenricServiceAsync genasync=GWT.create(GenricService.class);
				
				Vector<Filter> filtervec=new Vector<Filter>();				
				final MyQuerry querry=new MyQuerry();
				Filter filter=new Filter();
				filter.setQuerryString("productCode");
				filter.setStringValue(productInfoComposite.getProdCode().getValue().trim());
				filtervec.add(filter);
				
				filter=new Filter();
				filter.setQuerryString("count");
				filter.setIntValue(Integer.parseInt(productInfoComposite.getProdID().getValue()));
				filtervec.add(filter);
				
				filter=new Filter();
				filter.setQuerryString("status");
				filter.setBooleanvalue(true);
				filtervec.add(filter);
				
				querry.setFilters(filtervec);
				querry.setQuerryObject(new SuperProduct());
					genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {					
						@Override
						public void onFailure(Throwable caught) {
							showDialogMessage("An Unexpected error occurred!");
						}			
						@Override
						public void onSuccess(ArrayList<SuperModel> result) {

							if(result.size()==0)
							{
								showDialogMessage("Please check whether product status is active or not!");
							}
							for(SuperModel model:result)
							{
								SuperProduct superProdEntity = (SuperProduct)model;		
								MaterialRequired materialRequired = new MaterialRequired();
								materialRequired.setProductId(Integer
										.parseInt(productInfoComposite.getProdID().getValue()));
								materialRequired.setProductCode(productInfoComposite
										.getProdCode().getValue());
								materialRequired.setProductName(productInfoComposite
										.getProdName().getValue());
								materialRequired.setProductCategory(productInfoComposite
										.getProdCategory().getValue());
								materialRequired.setProductIssueQty(1);
								materialRequired.setProductPrice(productInfoComposite.getProdPrice().getValue() - customerLogObj.getSchemeAmount());
								materialRequired.setProductUOM(productInfoComposite
										.getUnitOfmeasuerment().getValue());
								materialRequired.setPrduct(superProdEntity);
								materialRequired.setVatTax(superProdEntity.getVatTax());
								materialRequired.setServiceTax(superProdEntity.getServiceTax());
								materialRequired.setDiscountAmt(0);
								materialRequired.setFlag(true);
								ServiceProductTable s = new ServiceProductTable();
								s.calculateTotalAmount(materialRequired);
								//serviceCharegesTable.flag = true;
								getServiceCharegesTable().getDataprovider().getList().add(materialRequired);
								//return materialRequired;
							}
							calcaulateTotalAmount();
			           }
						
					});
				
			} else {
				showDialogMessage("Can not add Duplicate Product!");
			}
		} else {
			showDialogMessage("Please Enter Product Details!");
		}
	}

	public boolean validateProduct(ServiceProductTable MaterialTable ,ProductInfoComposite productInfoComposite) {
		if (MaterialTable.getDataprovider().getList().size() != 0) {
			for (int i = 0; i < MaterialTable.getDataprovider().getList()
					.size(); i++) {
				if (MaterialTable.getDataprovider().getList().get(i)
						.getProductId() == Integer
						.parseInt(productInfoComposite.getProdID().getValue())) {
					return false;
				}
			}
		} else {
			return true;
		}
		return true;
	}
//	protected void calcaulateTotalAmount(){
//		List<MaterialRequired> productList = new ArrayList<MaterialRequired>();
//		  if(getMaterialTable().getDataprovider().getList().size() != 0){
//			  productList.addAll(getMaterialTable().getDataprovider().getList());
//		  }
//		  System.out.println("prod size:" + productList.size());
////		  if(getServiceCharegesTable().getDataprovider().getList().size() != 0){
////			  productList.addAll(getServiceCharegesTable().getDataprovider().getList());
////		  }
//		  double customerTotal = 0.0d;
//		  double companyTotal = 0.0d;
//		  for(MaterialRequired m :productList){
//		  if((getTbWarrantyStatus().getValue().equalsIgnoreCase("Warranty") || getTbWarrantyStatus().getValue().equalsIgnoreCase("AMC")) && m.getProductPayableQuantity()!=0){
//			  customerTotal = customerTotal + (m.getProductPayableQuantity()*m.getProductPrice());	 
//		  }
//		  if(getTbWarrantyStatus().getValue().equalsIgnoreCase("Warranty") || getTbWarrantyStatus().getValue().equalsIgnoreCase("AMC")){
//			  companyTotal = companyTotal + (m.getProductIssueQty() - m.getProductReturnQty()-m.getProductPayableQuantity())*m.getProductPrice();	 
//		  }
//		  if(getTbWarrantyStatus().getValue().equalsIgnoreCase("Out Of Warranty")){		
//			  customerTotal = customerTotal + (m.getProductIssueQty() - m.getProductReturnQty()-m.getProductPayableQuantity())*m.getProductPrice();			  
//		  }
//	   }
//		  List<MaterialRequired> serviceList = new ArrayList<MaterialRequired>();
//		  serviceList = getServiceCharegesTable().getDataprovider().getList();
//		  for(MaterialRequired m :serviceList){
////			  if((getTbWarrantyStatus().getValue().equalsIgnoreCase("Warranty") || getTbWarrantyStatus().getValue().equalsIgnoreCase("AMC")) && m.getProductPayableQuantity()!=0){
////				  customerTotal = customerTotal + m.getTotalAmount();	 
////			  }
//			  if(getTbWarrantyStatus().getValue().equalsIgnoreCase("Warranty") || getTbWarrantyStatus().getValue().equalsIgnoreCase("AMC")){
//				  companyTotal = companyTotal + m.getTotalAmount();	 
//			  }
//			  if(getTbWarrantyStatus().getValue().equalsIgnoreCase("Out Of Warranty")){		
//				  customerTotal = customerTotal + m.getTotalAmount();			  
//			  }
//		   }
//		  getTbTotalAmount().setValue(nf.format(customerTotal)+"");
//		  double discount = 0.0d;
//		  if(getTbDiscountAmount().getValue()!=null && getTbDiscountAmount().getValue()!=null){
//			 discount = Double.parseDouble(getTbDiscountAmount().getValue());
//		  }
//		  
//		  getTbNetPayable().setValue(nf.format(customerTotal - discount)+"");
//	}
//	protected void calcaulateTotalAmount(){
//		List<MaterialRequired> productList = new ArrayList<MaterialRequired>();
//		  if(this.getCustomerTable().getDataprovider().getList().size() != 0){
//			  productList.addAll(this.getCustomerTable().getDataprovider().getList());
//		  }
//		  System.out.println("prod size:" + productList.size());
//		  if(this.getServiceCharegesTable().getDataprovider().getList().size() != 0){
//			  productList.addAll(this.getServiceCharegesTable().getDataprovider().getList());
//		  }
//		  GWTCAlert alert = new GWTCAlert();
//		  double customerTotal = 0.0d;
//		 
//		  for(MaterialRequired m :productList){
//		  if((this.getTbWarrantyStatus().getValue().equalsIgnoreCase("Warranty") || this.getTbWarrantyStatus().getValue().equalsIgnoreCase("AMC")) && m.getProductPayableQuantity()!=0){
//			  customerTotal = customerTotal + m.getTotalAmount()+m.getTaxAmount(); 
//		  }
//		  if(this.getTbWarrantyStatus().getValue().equalsIgnoreCase("Warranty") && this.getCustomerLogObj().getCallType().equals("Installation")){
//			  if(getCustomerLogObj().isSchemeFlag()){
//				  customerTotal = customerTotal + getCustomerLogObj().getSchemeAmount();
//			 		
//		    }
//		  }
//		  if(this.getTbWarrantyStatus().getValue().equalsIgnoreCase("Out Of Warranty")){		
//			  customerTotal = customerTotal + m.getTotalAmount()+m.getTaxAmount(); 
//		     }
//	        }
//		  
//		  if(productList.size() == 0){
//			  if(this.getTbWarrantyStatus().getValue().equalsIgnoreCase("Warranty") && this.getCustomerLogObj().getCallType().equals("Installation")){
//				  if(customerLogObj.isSchemeFlag()){
//					  customerTotal =  customerLogObj.getSchemeAmount();
//					  alert.alert("total :"+customerTotal);
//				  }
//			  	}
//			  }
//		  this.getTbTotalAmount().setValue(nf.format(Math.round(customerTotal))+"");
//		  this.getTbNetPayable().setValue(nf.format(Math.round(customerTotal))+"");
//		 // this.getTbAmountReceived().setValue(nf.format(customerTotal)+"");;
//	}
	protected void calcaulateTotalAmount(){
		List<MaterialRequired> productList = new ArrayList<MaterialRequired>();
		  if(this.getCustomerTable().getDataprovider().getList().size() != 0){
			  productList.addAll(this.getCustomerTable().getDataprovider().getList());
		  }
		  System.out.println("prod size:" + productList.size());
		  if(this.getServiceCharegesTable().getDataprovider().getList().size() != 0){
			  productList.addAll(this.getServiceCharegesTable().getDataprovider().getList());
		  }
		  double customerTotal = 0.0d;
		  for(MaterialRequired m :productList){
		  if((this.getTbWarrantyStatus().getValue().equalsIgnoreCase("Warranty") || this.getTbWarrantyStatus().getValue().equalsIgnoreCase("AMC")) && m.getProductPayableQuantity()!=0){
			  customerTotal = customerTotal + m.getTotalAmount()+m.getTaxAmount(); 
		  }
		  if(this.getTbWarrantyStatus().getValue().equalsIgnoreCase("Warranty") && this.getCustomerLogObj().getCallType().equals("Installation")){
			  if(customerLogObj.isSchemeFlag()){
				  customerTotal = customerTotal + customerLogObj.getSchemeAmount();
			  }
		  }
		  if(this.getTbWarrantyStatus().getValue().equalsIgnoreCase("Out Of Warranty")){		
			  customerTotal = customerTotal + m.getTotalAmount()+m.getTaxAmount(); 
		  }
	   }
		//  showDialogMessage("customer total : "+customerTotal );
		  this.getTbTotalAmount().setValue(nf.format(Math.round(customerTotal))+"");
		  this.getTbNetPayable().setValue(nf.format(Math.round(customerTotal))+"");
	}
	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
		return super.validate();
	}

	@Override
	public void onRowCountChange(RowCountChangeEvent event) {
		// TODO Auto-generated method stub
		if(event.getSource().equals(serviceCharegesTable.getTable())){
			calcaulateTotalAmount();
		}
		if(event.getSource().equals(materialTable.getTable())){
			calcaulateTotalAmount();
		}
		if(event.getSource().equals(customerTable.getTable())){
			calcaulateTotalAmount();
		}
	}
	public double removeAllTaxes(SuperProduct entity)
	{
		double vat = 0,service = 0;
		double tax=0,retrVat=0,retrServ=0;
		if(entity instanceof ServiceProduct)
		{
			ServiceProduct prod=(ServiceProduct) entity;
			if(prod.getServiceTax()!=null&&prod.getServiceTax().isInclusive()==true){
				service=prod.getServiceTax().getPercentage();
			}
			if(prod.getVatTax()!=null&&prod.getVatTax().isInclusive()==true){
				vat=prod.getVatTax().getPercentage();
			}
		}

		if(entity instanceof ItemProduct)
		{
			ItemProduct prod=(ItemProduct) entity;
			if(prod.getVatTax()!=null&&prod.getVatTax().isInclusive()==true){
				vat=prod.getVatTax().getPercentage();
			}
			if(prod.getServiceTax()!=null&&prod.getServiceTax().isInclusive()==true){
				service=prod.getServiceTax().getPercentage();
			}
		}
		
		if(vat!=0&&service==0){
			retrVat=entity.getPrice()/(1+(vat/100));
			retrVat=entity.getPrice()-retrVat;
		}
		if(service!=0&&vat==0){
			retrServ=entity.getPrice()/(1+service/100);
			retrServ=entity.getPrice()-retrServ;
		}
		if(service!=0&&vat!=0){
			if(entity instanceof ItemProduct)
			{
				ItemProduct prod=(ItemProduct) entity;
				if(prod.getServiceTax().getTaxPrintName()!=null && ! prod.getServiceTax().getTaxPrintName().equals("")
				   && prod.getVatTax().getTaxPrintName()!=null && ! prod.getVatTax().getTaxPrintName().equals(""))
				{

					double dot = service + vat;
					retrServ=(entity.getPrice()/(1+dot/100));
					retrServ=entity.getPrice()-retrServ;
//					retrVat=0;

				}
				else{
					// Here if both are inclusive then first remove service tax and then on that amount
					// calculate vat.
					double removeServiceTax=(entity.getPrice()/(1+service/100));
					//double taxPerc=service+vat;
					//retrServ=(entity.getPrice()/(1+taxPerc/100)); //line changed below
					retrServ=(removeServiceTax/(1+vat/100));
					retrServ=entity.getPrice()-retrServ;
				}
			}
			
			if(entity instanceof ServiceProduct)
			{
				ServiceProduct prod=(ServiceProduct) entity;
				if(prod.getServiceTax().getTaxPrintName()!=null && ! prod.getServiceTax().getTaxPrintName().equals("")
				   && prod.getVatTax().getTaxPrintName()!=null && ! prod.getVatTax().getTaxPrintName().equals(""))
				{
					double dot = service + vat;
					retrServ=(entity.getPrice()/(1+dot/100));
					retrServ=entity.getPrice()-retrServ;
				}
				else
				{
					// Here if both are inclusive then first remove service tax and then on that amount
					// calculate vat.
					double removeServiceTax=(entity.getPrice()/(1+service/100));
					//double taxPerc=service+vat;
					//retrServ=(entity.getPrice()/(1+taxPerc/100)); //line changed below
					retrServ=(removeServiceTax/(1+vat/100));
					retrServ=entity.getPrice()-retrServ;
				}
			}
		}
		tax=retrVat+retrServ;
		return tax;
	}
	public double addAllTaxes(SuperProduct entity)
	{

		double vat = 0,service = 0;
		double tax=0,retrVat=0,retrServ=0;
		if(entity instanceof ServiceProduct)
		{
			ServiceProduct prod=(ServiceProduct) entity;
			if(prod.getServiceTax()!=null){
				service=prod.getServiceTax().getPercentage();
			}
			if(prod.getVatTax()!=null){
				vat=prod.getVatTax().getPercentage();
			}
		}

		if(entity instanceof ItemProduct)
		{
			ItemProduct prod=(ItemProduct) entity;
			if(prod.getVatTax()!=null){
				vat=prod.getVatTax().getPercentage();
			}
			if(prod.getServiceTax()!=null){
				service=prod.getServiceTax().getPercentage();
			}
		}
		
		if(vat!=0&&service==0){
			retrVat=entity.getPrice()*vat/100;
			//retrVat=entity.getPrice()+retrVat;
		}
		if(service!=0&&vat==0){
			retrServ=entity.getPrice()*service/100;
			//retrServ=entity.getPrice()+retrServ;
		}
		if(service!=0&&vat!=0){			
			if(entity instanceof ItemProduct)
			{
				ItemProduct prod=(ItemProduct) entity;
				if(prod.getServiceTax().getTaxPrintName()!=null && ! prod.getServiceTax().getTaxPrintName().equals("")
				   && prod.getVatTax().getTaxPrintName()!=null && ! prod.getVatTax().getTaxPrintName().equals(""))
				{

					double dot = service + vat;
					retrServ=(entity.getPrice()*dot/100);
					//retrServ=entity.getPrice()+retrServ;
				}

			}
			
			if(entity instanceof ServiceProduct)
			{
				ServiceProduct prod=(ServiceProduct) entity;
				if(prod.getServiceTax().getTaxPrintName()!=null && ! prod.getServiceTax().getTaxPrintName().equals("")
				   && prod.getVatTax().getTaxPrintName()!=null && ! prod.getVatTax().getTaxPrintName().equals(""))
				{
					double dot = service + vat;
					retrServ=(entity.getPrice()*dot/100);
					//retrServ=entity.getPrice()+retrServ;
				}
				
			}
		}
		tax=retrVat+retrServ;
		return tax;
	
	}
}

