package com.slicktechnologies.client.views.customerlog;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.customerlog.MaterialRequired;

public class MaterialRequiredTable extends SuperTable<MaterialRequired> {
	NumberFormat nf=NumberFormat.getFormat("0.00");
	TextColumn<MaterialRequired> prodId;
	TextColumn<MaterialRequired> prodCode;
	TextColumn<MaterialRequired> prodName;
	TextColumn<MaterialRequired> prodCategory;
	TextColumn<MaterialRequired> prodRequestQty;
	Column<MaterialRequired, String> prodRequestQtyEdit;
	TextColumn<MaterialRequired> prodUOM;
	TextColumn<MaterialRequired> prodPrice;
	Column<MaterialRequired, String> deleteColumn;
	@Override
	public void createTable() {
		addColumnprodID();
		addColumnprodcode();
		addColumnprodname();
		addColumnprodcategory();
		addColumnprodRequestQty();
		addColumnUnitOfMeasurement();
		//addColumnprodPrice();
		createColumndeleteColumn();
		addFieldUpdater();

	}

	public void addeditColumn() {
		addColumnprodID();
		addColumnprodcode();
		addColumnprodname();
		addColumnprodcategory();
		addColumnprodRequestQty();
		addColumnUnitOfMeasurement();
		//addColumnprodPrice();
		createColumndeleteColumn();
		addFieldUpdater();

	}

	public void addViewColumn() {
		addColumnprodID();
		addColumnprodcode();
		addColumnprodname();
		addColumnprodcategory();
		addColumnviewRequestQty();
		//addColumnprodPrice();
		addColumnUnitOfMeasurement();
	}

	@Override
	public void addFieldUpdater() {
		createFieldUpdaterprodRequestQtyColumn();
		createFieldUpdaterDelete();
		
	}
	
	@Override
	public void setEnable(boolean state) {
		int tablecolcount = this.table.getColumnCount();
		for (int i = tablecolcount - 1; i > -1; i--)
			table.removeColumn(i);
		if (state == true)
			addeditColumn();
		if (state == false)
			addViewColumn();
	}
	
	@Override
	protected void initializekeyprovider() {
		
	}

	@Override
	public void applyStyle() {
		
	}
	
/*********************************************Product View Column ********************************************/
	
	public void addColumnprodID() {
		prodId = new TextColumn<MaterialRequired>() {

			@Override
			public String getValue(MaterialRequired object) {
				return object.getProductId() + "";
			}
		};
		table.addColumn(prodId, "ID");
	}
	
	public void addColumnprodcode() {
		prodCode = new TextColumn<MaterialRequired>() {

			@Override
			public String getValue(MaterialRequired object) {
				return object.getProductCode();
			}
		};
		table.addColumn(prodCode, "Code");
	}

	public void addColumnprodname() {
		prodName = new TextColumn<MaterialRequired>() {

			@Override
			public String getValue(MaterialRequired object) {
				return object.getProductName();
			}
		};
		table.addColumn(prodName, "Name");
	}
	
	public void addColumnprodcategory() {
		prodCategory = new TextColumn<MaterialRequired>() {

			@Override
			public String getValue(MaterialRequired object) {
				return object.getProductCategory();
			}
		};
		table.addColumn(prodCategory, "Category");
	}
	
	
	//Read Only Quantity Column
	public void addColumnviewRequestQty() {
		prodRequestQty = new TextColumn<MaterialRequired>() {

			@Override
			public String getValue(MaterialRequired object) {
				return nf.format(object.getProductRequestQty());
			}
		};
		table.addColumn(prodRequestQty, "#Quantity");
	}
	
	//Editable Quantity Column
	public void addColumnprodRequestQty() {
		EditTextCell editCell = new EditTextCell();
		prodRequestQtyEdit = new Column<MaterialRequired, String>(editCell) {

			@Override
			public String getValue(MaterialRequired object) {
				return nf.format(object.getProductRequestQty());
			}
		};
		table.addColumn(prodRequestQtyEdit, "#Quantity");
	}
	
	protected void addColumnUnitOfMeasurement() {
		prodUOM = new TextColumn<MaterialRequired>() {

			@Override
			public String getValue(MaterialRequired object) {
				if (object.getProductUOM().equals(""))
					return "N/A";
				else
					return object.getProductUOM();
			}
		};
		table.addColumn(prodUOM, "UOM");
	}
	
	protected void addColumnprodPrice() {
		prodPrice = new TextColumn<MaterialRequired>() {

			@Override
			public String getValue(MaterialRequired object) {
					return object.getProductPrice()+"";
			}
		};
		table.addColumn(prodPrice, "Price");
	}

	public void createColumndeleteColumn() {
		ButtonCell btnCell = new ButtonCell();
		deleteColumn = new Column<MaterialRequired, String>(btnCell) {
			@Override
			public String getValue(MaterialRequired object) {
				return "Delete";
			}
		};
		table.addColumn(deleteColumn, "Delete");

	}
	
	
	public void createFieldUpdaterDelete() {
		deleteColumn
				.setFieldUpdater(new FieldUpdater<MaterialRequired, String>() {
					@Override
					public void update(int index, MaterialRequired object,
							String value) {
						getDataprovider().getList().remove(object);

						table.redrawRow(index);
					}
				});

	}

	protected void createFieldUpdaterprodRequestQtyColumn() {
		prodRequestQtyEdit.setFieldUpdater(new FieldUpdater<MaterialRequired, String>() {
			@Override
			public void update(int index, MaterialRequired object, String value) {

				try {
					double val1 = Double.parseDouble(value.trim());
					object.setProductRequestQty(val1);
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				} catch (NumberFormatException e) {

				}

				table.redrawRow(index);
			}
		});
	}
}
