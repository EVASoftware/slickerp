package com.slicktechnologies.client.views.customerlog;

import java.util.ArrayList;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerlog.CustomerLogDetails;
import com.slicktechnologies.shared.common.customerlog.MaterialRequired;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.inventory.StorageLocation;
import com.slicktechnologies.shared.common.inventory.Storagebin;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;

public class MaterialPopup extends PopupScreen implements ClickHandler,
		ChangeHandler {
	TextBox tbId, tbCustomerName, tbTechnicianName, tbCompanyName , tbProductName;
	ObjectListBox<ConfigCategory> olbCallbackStatus;
	ObjectListBox<WareHouse> olbToWarehouse;
	ObjectListBox<StorageLocation> olbToLoc;
	ObjectListBox<Storagebin> olbToBin;
	ObjectListBox<ConfigCategory> olbWarrantyStatus;
	ProductInfoComposite productInfoComposite, defectiveProductInfoComposite;
	MaterialIssueTable materialIssueTable;
	DefectiveMaterialIssueTable DefectiveMaterialTable;
	Button btnAdd, btnAdd1;
	//ObjectListBox<ConfigCategory> olbProduct;
	TextBox tbModelNo;
	CustomerLogDetails customerLogObj;
	GenricServiceAsync service = GWT.create(GenricService.class);
	GWTCAlert alert = new GWTCAlert();

	public MaterialPopup() {
		super();
		getPopup().getElement().addClassName("popupWarehouse");
		this.getTbCustomerName().setEnabled(false);
		this.getTbId().setEnabled(false);
		this.getTbTechnicianName().setEnabled(false);
		this.getTbCompanyName().setEnabled(false);
		this.getOlbWarrantyStatus().setEnabled(false);
		this.getTbProductName().setEnabled(false);
		//this.getOlbProduct().setEnabled(false);
		this.getTbModelNo().setEnabled(false);
		materialIssueTable.connectToLocal();
		DefectiveMaterialTable.connectToLocal();
		getOlbToWarehouse().addChangeHandler(this);
		getOlbToLoc().addChangeHandler(this);
		getOlbToBin().addChangeHandler(this);
		createGui();
	}

	@Override
	public void onClick(ClickEvent event) {
		MaterialRequired m;
		if (event.getSource().equals(btnAdd)) {
			addProductToTable(materialIssueTable, productInfoComposite);
		}
		if (event.getSource().equals(btnAdd1)) {
			addProductToTable(DefectiveMaterialTable,defectiveProductInfoComposite);
		}
	}

	@Override
	public void createScreen() {
		// TODO Auto-generated method stub
		initializeWidget();
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingMaterialInformation = fbuilder
				.setlabel("Basic Information").widgetType(FieldType.Grouping)
				.setMandatory(false).setColSpan(4).build();
		// fbuilder = new FormFieldBuilder("CallBack Status",
		// olbCallbackStatus);
		// FormField folbCallbackStatus =
		// fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Customer Name", tbCustomerName);
		FormField ftbCustomerName = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		fbuilder = new FormFieldBuilder("ID", tbId);
		FormField ftbId = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Technician Name", tbTechnicianName);
		FormField ftbTechnicianName = fbuilder.setMandatory(false)
				.setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Company Name", tbCompanyName);
		FormField ftbCompanyName = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		FormField fmrnSubProductGrouping = fbuilder
				.setlabel("Material Information")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(4).build();
		fbuilder = new FormFieldBuilder("", productInfoComposite);
		FormField fproductInfoComposite = fbuilder.setMandatory(false)
				.setRowSpan(0).setColSpan(3).build();
		fbuilder = new FormFieldBuilder("", btnAdd);
		FormField fbtnAdd = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		fbuilder = new FormFieldBuilder("", materialIssueTable.getTable());
		FormField fmaterialIssueTable = fbuilder.setMandatory(false)
				.setRowSpan(0).setColSpan(4).build();
		FormField fmrnDefectiveProductGrouping = fbuilder
				.setlabel("Defective Material Information")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(4).build();
		fbuilder = new FormFieldBuilder("", defectiveProductInfoComposite);
		FormField fdefectiveProductComposite = fbuilder.setMandatory(false)
				.setRowSpan(0).setColSpan(3).build();
		fbuilder = new FormFieldBuilder("", btnAdd1);
		FormField fbtnAdd1 = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		fbuilder = new FormFieldBuilder("", DefectiveMaterialTable.getTable());
		FormField fDefectiveMaterialTable = fbuilder.setMandatory(false)
				.setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Transfer to - Warehouse",
				olbToWarehouse);
		FormField folbToWarehouse = fbuilder.setMandatory(true)
				.setMandatoryMsg("Please select warehouse").setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder(" Transfer to - Storage Bin", olbToBin);
		FormField flbToBin = fbuilder.setMandatory(true)
				.setMandatoryMsg("Please select storage bin").setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Transfer To - Storage Location",
				olbToLoc);
		FormField flbToLoc = fbuilder.setMandatory(true)
				.setMandatoryMsg("Please select storage location")
				.setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Warranty Status", olbWarrantyStatus);
		FormField olbWarrantyStatus = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		fbuilder = new FormFieldBuilder("ModelNo", tbModelNo);
		FormField ftbModelNo= fbuilder.setRowSpan(0).setColSpan(0).build();
		//fbuilder = new FormFieldBuilder("Product",olbProduct);
		//FormField folbProduct= fbuilder.setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Product", tbProductName);
		FormField ftbProductName= fbuilder.setRowSpan(0).setColSpan(0).build();
		FormField fmrnTechnicianWareHouseDetails= fbuilder
				.setlabel("Technician Warehouse Information")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(4).build();
		if (defectiveProductInfoComposite.isVisible()) {
			FormField[][] formfield = {
					{ fgroupingMaterialInformation },
					{ ftbId, ftbCustomerName, ftbTechnicianName, ftbCompanyName },
					{ olbWarrantyStatus , ftbProductName ,ftbModelNo } ,
					{ fmrnTechnicianWareHouseDetails },
					{ folbToWarehouse, flbToLoc, flbToBin },
					{ fmrnSubProductGrouping },
					{ fproductInfoComposite, fbtnAdd },
					{ fmaterialIssueTable }, { fmrnDefectiveProductGrouping },
					{ fdefectiveProductComposite, fbtnAdd1 },
					{ fDefectiveMaterialTable } };
			this.fields = formfield;
		} else {
			FormField[][] formfield = {
					{ fgroupingMaterialInformation },
					{ ftbId, ftbCustomerName, ftbTechnicianName, ftbCompanyName },
					{ olbWarrantyStatus , ftbProductName ,ftbModelNo } ,
					{ fmrnTechnicianWareHouseDetails },
					{ olbWarrantyStatus, folbToWarehouse, flbToLoc, flbToBin },
					{ fmrnSubProductGrouping },
					{ fproductInfoComposite, fbtnAdd }, { fmaterialIssueTable } };
			this.fields = formfield;

		}

	}

	private void initializeWidget() {
		tbId = new TextBox();
		tbCustomerName = new TextBox();
		tbTechnicianName = new TextBox();
		tbCompanyName = new TextBox();
		tbModelNo = new TextBox();
		tbProductName = new TextBox();
		olbWarrantyStatus = new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveConfig(olbWarrantyStatus, Screen.WARRANTYSTATUS);
		olbCallbackStatus = new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveConfig(olbCallbackStatus, Screen.CALLBACKSTATUS);
		productInfoComposite = AppUtility
				.initiatePurchaseProductComposite(new ItemProduct());
		materialIssueTable = new MaterialIssueTable();
		btnAdd = new Button("ADD");
		btnAdd.addClickHandler(this);
		defectiveProductInfoComposite = AppUtility
				.initiatePurchaseProductComposite(new ItemProduct());
		DefectiveMaterialTable = new DefectiveMaterialIssueTable();
		btnAdd1 = new Button("ADD");
		btnAdd1.addClickHandler(this);
		olbToWarehouse = new ObjectListBox<WareHouse>();
		olbToWarehouse.MakeLive(new MyQuerry(new Vector<Filter>(),
				new WareHouse()));
		olbToLoc = new ObjectListBox<StorageLocation>();
		olbToLoc.addItem("--SELECT--");
		olbToBin = new ObjectListBox<Storagebin>();
		olbToBin.addItem("--SELECT--");
	 //   olbProduct = new ObjectListBox<ConfigCategory>();
	//    AppUtility.MakeLiveConfig(olbProduct,Screen.PRODUCT);
	}

	public TextBox getTbId() {
		return tbId;
	}

	public void setTbId(TextBox tbId) {
		this.tbId = tbId;
	}

	public TextBox getTbCustomerName() {
		return tbCustomerName;
	}

	public void setTbCustomerName(TextBox tbCustomerName) {
		this.tbCustomerName = tbCustomerName;
	}

	public TextBox getTbTechnicianName() {
		return tbTechnicianName;
	}

	public void setTbTechnicianName(TextBox tbTechnicianName) {
		this.tbTechnicianName = tbTechnicianName;
	}

	public CustomerLogDetails getCustomerLogObj() {
		return customerLogObj;
	}

	public void setCustomerLogObj(CustomerLogDetails customerLogObj) {
		this.customerLogObj = customerLogObj;
	}

	public void addProductToTable(final SuperTable<MaterialRequired> materialTable,
			final ProductInfoComposite productInfoComposite) {
//		if (productInfoComposite.getValue() != null) {
//			if (validateProduct(MaterialTable, productInfoComposite)) {
//				MaterialRequired materialRequired = new MaterialRequired();
//				materialRequired.setProductId(Integer
//						.parseInt(productInfoComposite.getProdID().getValue()));
//				materialRequired.setProductCode(productInfoComposite
//						.getProdCode().getValue());
//				materialRequired.setProductName(productInfoComposite
//						.getProdName().getValue());
//				materialRequired.setProductCategory(productInfoComposite
//						.getProdCategory().getValue());
//				materialRequired.setProductAvailableQty(0);
//				materialRequired.setProductIssueQty(1);
//				materialRequired.setProductReturnQty(0);
//				materialRequired.setProductPayableQuantity(0);
//				materialRequired.setProductPrice(productInfoComposite
//						.getProdPrice().getValue());
//				materialRequired.setProductUOM(productInfoComposite
//						.getUnitOfmeasuerment().getValue());
//				materialRequired.setPrduct(superProdEntity);
//				materialRequired.setVatTax(superProdEntity.getVatTax());
//				materialRequired.setServiceTax(superProdEntity.getServiceTax());
//				materialRequired.setDiscountAmt(0);
//				materialRequired.setFlag(true);
//				// MaterialTable.getDataprovider().getList().add(materialRequired);
//				return materialRequired;
//			} else {
//				showDialogMessage("Can not add Duplicate Product!");
//			}
//		} else {
//			showDialogMessage("Please Enter Product Details!");
//		}
//		return null;
		if (productInfoComposite.getValue() != null) {
			if (validateProduct(materialTable , productInfoComposite)) {
				final GenricServiceAsync genasync=GWT.create(GenricService.class);
				
				Vector<Filter> filtervec=new Vector<Filter>();				
				final MyQuerry querry=new MyQuerry();
				Filter filter=new Filter();
				filter.setQuerryString("productCode");
				filter.setStringValue(productInfoComposite.getProdCode().getValue().trim());
				filtervec.add(filter);
				
				filter=new Filter();
				filter.setQuerryString("count");
				filter.setIntValue(Integer.parseInt(productInfoComposite.getProdID().getValue()));
				filtervec.add(filter);
				
				filter=new Filter();
				filter.setQuerryString("status");
				filter.setBooleanvalue(true);
				filtervec.add(filter);
				
				querry.setFilters(filtervec);
				querry.setQuerryObject(new SuperProduct());
					genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {					
						@Override
						public void onFailure(Throwable caught) {
							showDialogMessage("An Unexpected error occurred!");
						}			
						@Override
						public void onSuccess(ArrayList<SuperModel> result) {

							if(result.size()==0)
							{
								showDialogMessage("Please check whether product status is active or not!");
							}
							for(SuperModel model:result)
							{
								SuperProduct superProdEntity = (SuperProduct)model;		
								MaterialRequired materialRequired = new MaterialRequired();
								superProdEntity.setTermsAndConditions(new DocumentUpload());
								superProdEntity.setProductImage(new DocumentUpload());
								superProdEntity.setProductImage1(new DocumentUpload());
								superProdEntity.setProductImage2(new DocumentUpload());
								superProdEntity.setProductImage3(new DocumentUpload());
								materialRequired.setProductId(Integer
										.parseInt(productInfoComposite.getProdID().getValue()));
								materialRequired.setProductCode(productInfoComposite
										.getProdCode().getValue());
								materialRequired.setProductName(productInfoComposite
										.getProdName().getValue());
								materialRequired.setProductCategory(productInfoComposite
										.getProdCategory().getValue());
								materialRequired.setProductIssueQty(1);
								materialRequired.setProductPrice(productInfoComposite
										.getProdPrice().getValue());
								materialRequired.setProductUOM(productInfoComposite
										.getUnitOfmeasuerment().getValue());
								materialRequired.setPrduct(superProdEntity);
								materialRequired.setVatTax(superProdEntity.getVatTax());
								materialRequired.setServiceTax(superProdEntity.getServiceTax());
								materialRequired.setDiscountAmt(0);
								materialRequired.setMaterialCredit(false);
								materialRequired.setFlag(true);
								materialRequired.setMaterialProductWarehouse("");
								materialRequired.setTermsAndConditions(new DocumentUpload());
								materialRequired.setProductImage(new DocumentUpload());
								
								if (materialTable instanceof MaterialIssueTable) {
									materialIssueTable.getDataprovider().getList().add(materialRequired);
								}
								if (materialTable instanceof DefectiveMaterialIssueTable) {
									DefectiveMaterialTable.getDataprovider().getList().add(materialRequired);
								}
							
								//return materialRequired;
							}
						
			           }
					});
				
			} else {
				showDialogMessage("Can not add Duplicate Product!");
			}
		} else {
			showDialogMessage("Please Enter Product Details!");
		}
	}

	public boolean validateProduct(SuperTable<MaterialRequired> MaterialTable,
			ProductInfoComposite productInfoComposite) {
		if (MaterialTable.getDataprovider().getList().size() != 0) {
			for (int i = 0; i < MaterialTable.getDataprovider().getList()
					.size(); i++) {
				if (MaterialTable.getDataprovider().getList().get(i).getProductId() == Integer.parseInt(productInfoComposite.getProdID().getValue())) {
					return false;
				}
			}
		} else {
			return true;
		}
		return true;
	}

	public MaterialIssueTable getMaterialIssueTable() {
		return materialIssueTable;
	}

	public void setMaterialIssueTable(MaterialIssueTable materialIssueTable) {
		this.materialIssueTable = materialIssueTable;
	}
	public DefectiveMaterialIssueTable getDefectiveMaterialTable() {
		return DefectiveMaterialTable;
	}

	public void setDefectiveMaterialTable(
			DefectiveMaterialIssueTable defectiveMaterialTable) {
		DefectiveMaterialTable = defectiveMaterialTable;
	}

	public TextBox getTbCompanyName() {
		return tbCompanyName;
	}

	public void setTbCompanyName(TextBox tbCompanyName) {
		this.tbCompanyName = tbCompanyName;
	}

	public ObjectListBox<WareHouse> getOlbToWarehouse() {
		return olbToWarehouse;
	}

	public void setOlbToWarehouse(ObjectListBox<WareHouse> olbToWarehouse) {
		this.olbToWarehouse = olbToWarehouse;
	}

	public ObjectListBox<StorageLocation> getOlbToLoc() {
		return olbToLoc;
	}

	public void setOlbToLoc(ObjectListBox<StorageLocation> olbToLoc) {
		this.olbToLoc = olbToLoc;
	}

	public ObjectListBox<Storagebin> getOlbToBin() {
		return olbToBin;
	}

	public void setOlbToBin(ObjectListBox<Storagebin> olbToBin) {
		this.olbToBin = olbToBin;
	}

	public ObjectListBox<ConfigCategory> getOlbWarrantyStatus() {
		return olbWarrantyStatus;
	}

	public void setOlbWarrantyStatus(
			ObjectListBox<ConfigCategory> olbWarrantyStatus) {
		this.olbWarrantyStatus = olbWarrantyStatus;
	}

	public ProductInfoComposite getDefectiveProductInfoComposite() {
		return defectiveProductInfoComposite;
	}

	public void setDefectiveProductInfoComposite(
			ProductInfoComposite defectiveProductInfoComposite) {
		this.defectiveProductInfoComposite = defectiveProductInfoComposite;
	}

	public Button getBtnAdd1() {
		return btnAdd1;
	}

	public void setBtnAdd1(Button btnAdd1) {
		this.btnAdd1 = btnAdd1;
	}

		public TextBox getTbModelNo() {
		return tbModelNo;
	}

	public void setTbModelNo(TextBox tbModelNo) {
		this.tbModelNo = tbModelNo;
	}
	
	public TextBox getTbProductName() {
		return tbProductName;
	}

	public void setTbProductName(TextBox tbProductName) {
		this.tbProductName = tbProductName;
	}

	@Override
	public void setEnable(boolean state) {
		if(state == true){
			this.getTbCustomerName().setEnabled(false);
			this.getTbId().setEnabled(false);
			this.getTbTechnicianName().setEnabled(false);
			this.getTbCompanyName().setEnabled(false);
			this.getOlbWarrantyStatus().setEnabled(false);
		//	this.getOlbProduct().setEnabled(false);
			this.getTbModelNo().setEnabled(false);
			//this.getMaterialIssueTable().setEnable(true);
			getOlbToWarehouse().setEnabled(true);
			getOlbToLoc().setEnabled(true);
			getOlbToBin().setEnabled(true);
			//super.setEnable(state);
		}
		if(state == false){
			this.getTbCustomerName().setEnabled(false);
			this.getTbId().setEnabled(false);
			this.getTbTechnicianName().setEnabled(false);
			this.getTbCompanyName().setEnabled(false);
			this.getOlbWarrantyStatus().setEnabled(false);
			//this.getOlbProduct().setEnabled(false);
			this.getTbModelNo().setEnabled(false);
			this.getMaterialIssueTable().setEnable(false);
			getOlbToWarehouse().setEnabled(false);
			getOlbToLoc().setEnabled(false);
			getOlbToBin().setEnabled(false);
			//super.setEnable(state);
		}
		
	}

	@Override
	public void onChange(ChangeEvent event) {
		// TODO Auto-generated method stub
		if (event.getSource().equals(getOlbToWarehouse())) {
			retriveStorageLocation();
		}
		if (event.getSource().equals(getOlbToLoc())) {
			retriveStorageBin();
		}
	}

	private void retriveStorageLocation() {

		getOlbToLoc().clear();
		getOlbToLoc().addItem("--SELECT--");
		Timer timer = new Timer() {
			@Override
			public void run() {
				MyQuerry querry = new MyQuerry();
				Company c = new Company();
				Vector<Filter> filtervec = new Vector<Filter>();
				Filter filter = null;

				filter = new Filter();
				filter.setQuerryString("companyId");
				filter.setLongValue(c.getCompanyId());
				filtervec.add(filter);

				filter = new Filter();
				filter.setQuerryString("warehouseName");
				filter.setStringValue(getOlbToWarehouse().getValue());
				filtervec.add(filter);

				querry.setFilters(filtervec);
				querry.setQuerryObject(new StorageLocation());

				service.getSearchResult(querry,
						new AsyncCallback<ArrayList<SuperModel>>() {

							@Override
							public void onFailure(Throwable caught) {
								alert.alert("An Unexpected error occurred!");
							}

							@Override
							public void onSuccess(ArrayList<SuperModel> result) {
								for (SuperModel model : result) {
									StorageLocation locEntity = (StorageLocation) model;
									getOlbToLoc().addItem(
											locEntity.getBusinessUnitName());
								}
							}
						});

			}
		};
		timer.schedule(2000);

	}

	private void retriveStorageBin() {

		getOlbToBin().clear();
		getOlbToBin().addItem("--SELECT--");
		Timer timer = new Timer() {
			@Override
			public void run() {
				MyQuerry querry = new MyQuerry();
				Company c = new Company();

				Vector<Filter> filtervec = new Vector<Filter>();
				Filter filter = null;

				filter = new Filter();
				filter.setQuerryString("companyId");
				filter.setLongValue(c.getCompanyId());
				filtervec.add(filter);

				filter = new Filter();
				filter.setQuerryString("storagelocation");
				filter.setStringValue(getOlbToLoc().getValue(
						getOlbToLoc().getSelectedIndex()));
				filtervec.add(filter);

				querry.setFilters(filtervec);
				querry.setQuerryObject(new Storagebin());

				service.getSearchResult(querry,
						new AsyncCallback<ArrayList<SuperModel>>() {

							@Override
							public void onFailure(Throwable caught) {
								alert.alert("An Unexpected error occurred!");
							}

							@Override
							public void onSuccess(ArrayList<SuperModel> result) {
								for (SuperModel model : result) {
									Storagebin binEntity = (Storagebin) model;
									getOlbToBin().addItem(
											binEntity.getBinName());
								}
							}
						});
			}
		};
		timer.schedule(2000);

	}

	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
		return super.validate();
	}

}
