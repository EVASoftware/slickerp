package com.slicktechnologies.client.views.customerlog;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.contract.ContractPresenterSearchProxy;
import com.slicktechnologies.client.views.lead.LeadPresenterSearchProxy;
import com.slicktechnologies.client.views.lead.LeadPresenterTableProxy;
import com.slicktechnologies.client.views.lead.LeadPresenter.LeadPresenterSearch;
import com.slicktechnologies.client.views.lead.LeadPresenter.LeadPresenterTable;
import com.slicktechnologies.client.views.quickcontract.NewCustomerPopup;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.customerlog.CustomerLogDetails;
import com.slicktechnologies.shared.common.customerlog.MaterialRequired;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
public class CustomerLogDetailsPresenter extends
		FormScreenPresenter<CustomerLogDetails> implements ClickHandler, SelectionHandler<Suggestion> {

	CustomerLogDetailsForm form;
	GenricServiceAsync genasync = GWT.create(GenricService.class);
	GeneralServiceAsync service = GWT.create(GeneralService.class);
	CustomerLogDetails customerLogDetails;
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
//komal
	NewCustomerPopup newcustomerPopup = new NewCustomerPopup();
	
	public CustomerLogDetailsPresenter(FormScreen<CustomerLogDetails> view,
			CustomerLogDetails model) {
		super(view, model);
		form = (CustomerLogDetailsForm) view;
		form.setPresenter(this);
		form.getCustomerLogPopup().getLblOk().addClickHandler(this);
		form.getCustomerLogPopup().getLblCancel().addClickHandler(this);
		form.getCustomerLogDetailsPopup().getLblOk().addClickHandler(this);
		form.getCustomerLogDetailsPopup().getLblCancel().addClickHandler(this);
		//form.getInvoicePaymentPopup().getBtnClose().addClickHandler(this);
		form.getCreateInvoicePopup().getLblOk().addClickHandler(this);
		form.getCreateInvoicePopup().getLblCancel().addClickHandler(this);
		form.getBtgo().addClickHandler(this);
		form.retriveTable(form.getCustomerLogQuery(), form.getCustomerLogTable());
		//komal
		
		/**
		 * Date 15-5-2018
		 * By jayshree
		 */
//		form.getCustomerLogPopup().getBtnNewCustomer().addClickHandler(this);
		form.getCustomerLogDetailsPopup().btnNewCustomer.addClickHandler(this);
		newcustomerPopup.getLblOk().addClickHandler(this);
		newcustomerPopup.getLblCancel().addClickHandler(this);
		form.getCustomerLogDetailsPopup().getPersonInfoComposite().getId().addSelectionHandler(this);
		form.getCustomerLogDetailsPopup().getPersonInfoComposite().getName().addSelectionHandler(this);
		form.getCustomerLogDetailsPopup().getPersonInfoComposite().getPhone().addSelectionHandler(this);
		
		
		//form.setLogCount(form.getCustomerLogTable().getDataprovider().getList());
	//	form.retriveTable(form.getCustomerLogQuery(), form.getWareHouseTable());		
	//	form.retriveTable(form.getCustomerLogQuery(), form.getAccountTable());
//	
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub

	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void makeNewModel() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onClick(ClickEvent event) {
		super.onClick(event);
		System.out.println("NOT NULLLLLL");
		if (event.getSource().equals(form.getCustomerLogPopup().getLblOk())) {
			CustomerLogDetailsForm.popupFlag = false;
			if(form.getCustomerLogPopup().validate()){
				if(checkComplaintNumber())	{			
					saveCustomerCallLogDetails();
				}
			}
		}
		if (event.getSource().equals(form.getCustomerLogPopup().getLblCancel())) {
			form.getCustomerLogPopup().hidePopUp();
			form.setCustomerLogDetails(null);
		}
		if (event.getSource().equals(form.getCustomerLogDetailsPopup().getLblOk())) {
			CustomerLogDetailsForm.popupFlag = true;
			if(form.getCustomerLogDetailsPopup().validate()){
				if(checkComplaintNumber())	{	
				 saveCustomerCallLogDetails();
				}
				}
		}
		if (event.getSource().equals(form.getCustomerLogDetailsPopup().getLblCancel())) {
			form.getCustomerLogDetailsPopup().hidePopUp();
			form.setCustomerLogDetails(null);
		}
		if(event.getSource().equals(form.getCreateInvoicePopup().getLblCancel())){
			form.getCreateInvoicePopup().hidePopUp();
			form.setCustomerLogDetails(null);
		}
		if(event.getSource().equals(form.getCreateInvoicePopup().getLblOk())){
			final GWTCAlert alert = new GWTCAlert();
			if(form.getCreateInvoicePopup().validate()){
			final CustomerLogDetails customerLog = form.getCreateInvoicePopup().getCustomerLogObj();
			String str ="";
			ArrayList<MaterialRequired> list = new ArrayList<MaterialRequired>();
			if(form.getCreateInvoicePopup().getServiceCharegesTable().getDataprovider().getList().size() !=0){
				list.addAll(form.getCreateInvoicePopup().getServiceCharegesTable().getDataprovider().getList());
				customerLog.setServiceCharges(list);
			}
			if(form.getCreateInvoicePopup().getTbTotalAmount().getValue()!= null){
				customerLog.getPaymentInfo().setPaymentAmt(Double.parseDouble(form.getCreateInvoicePopup().getTbTotalAmount().getValue()));
			}
			str = validatePaymentDetails(customerLog);
			if(str.equalsIgnoreCase("paymethod")){
				alert.alert("Payment method is mandatory");
			}else if(str.equalsIgnoreCase("bookno")){
				alert.alert("Book no is mandatory");
			}else if(str.equalsIgnoreCase("tcrno")){
				alert.alert("TCR no is mandatory");
			}else if(str.equalsIgnoreCase("amount")){
				alert.alert("Amount Received is mandatory");
			}else if(str.equals("size")){
				alert.alert("Please fill service charge details");
			}else if(str.equals("true")){
//			ArrayList<MaterialRequired> list = new ArrayList<MaterialRequired>();
//			if(form.getCreateInvoicePopup().getServiceCharegesTable().getDataprovider().getList().size() !=0){
//				list.addAll(form.getCreateInvoicePopup().getServiceCharegesTable().getDataprovider().getList());
//				customerLog.setServiceCharges(list);
//			}
			System.out.println("approver name :" + LoginPresenter.loggedInUser);
			if(form.getCreateInvoicePopup().getOlbPaymentMethods().getValue()!=null){
				customerLog.setPaymentMethod(form.getCreateInvoicePopup().getOlbPaymentMethods().getValue());
			}
			if(form.getCreateInvoicePopup().getTbTCRNo().getValue()!=null){
				customerLog.setTcrNo(form.getCreateInvoicePopup().getTbTCRNo().getValue());
			}
			if(form.getCreateInvoicePopup().getTbBookNo().getValue()!=null){
				customerLog.setBookNo(form.getCreateInvoicePopup().getTbBookNo().getValue());
			}
			if(form.getCreateInvoicePopup().getTbbankAccNo().getValue()!=null){
				customerLog.getPaymentInfo().setBankAccNo(form.getCreateInvoicePopup().getTbbankAccNo().getValue());
			}
            if(form.getCreateInvoicePopup().getTbbankBranch().getValue()!=null){
            	customerLog.getPaymentInfo().setBankBranch(form.getCreateInvoicePopup().getTbbankBranch().getValue());
            }
            if(form.getCreateInvoicePopup().getTbbankName().getValue()!=null){
            	customerLog.getPaymentInfo().setBankName(form.getCreateInvoicePopup().getTbbankName().getValue());
            }
            if(form.getCreateInvoicePopup().getDbChequeDate().getValue()!=null){
            	customerLog.getPaymentInfo().setChequeDate(form.getCreateInvoicePopup().getDbChequeDate().getValue());
            }
            if(form.getCreateInvoicePopup().getDbtransferDate().getValue()!=null){
            	customerLog.getPaymentInfo().setAmountTransferDate(form.getCreateInvoicePopup().getDbtransferDate().getValue());
            }
            if(form.getCreateInvoicePopup().getTbChequeIssuedBy().getValue()!=null){
            	customerLog.getPaymentInfo().setChequeIssuedBy(form.getCreateInvoicePopup().getTbChequeIssuedBy().getValue());
            }
            if(form.getCreateInvoicePopup().getTbreferenceno().getValue()!=null){
            	customerLog.getPaymentInfo().setReferenceNo(form.getCreateInvoicePopup().getTbreferenceno().getValue());
            }
            if(form.getCreateInvoicePopup().getTbchequeNo().getValue()!=null){
            	customerLog.getPaymentInfo().setChequeNo(form.getCreateInvoicePopup().getTbchequeNo().getValue());
            }
//			if(form.getCreateInvoicePopup().getTbTotalAmount().getValue()!= null){
//				customerLog.getPaymentInfo().setPaymentAmt(Double.parseDouble(form.getCreateInvoicePopup().getTbTotalAmount().getValue()));
//			}
			if(form.getCreateInvoicePopup().getTbNetPayable().getValue() !=null){
				customerLog.getPaymentInfo().setNetPay(Double.parseDouble(form.getCreateInvoicePopup().getTbNetPayable().getValue()));
			}	
			if(form.getCreateInvoicePopup().getTbDiscountAmount().getValue() != null){
				customerLog.getPaymentInfo().setDiscountAmount(Double.parseDouble(form.getCreateInvoicePopup().getTbDiscountAmount().getValue()));
			}
			if(form.getCreateInvoicePopup().getTbAmountReceived().getValue() != null){
				customerLog.getPaymentInfo().setPaymentReceived(Double.parseDouble(form.getCreateInvoicePopup().getTbAmountReceived().getValue()));
				customerLog.getPaymentInfo().setBalancePayment(customerLog.getPaymentInfo().getNetPay() - customerLog.getPaymentInfo().getPaymentReceived());
			}
			ArrayList<MaterialRequired> list1 = new ArrayList<MaterialRequired>();
			if(form.getCreateInvoicePopup().getCustomerTable().getDataprovider().getList().size()!=0){
				list1.addAll(form.getCreateInvoicePopup().getCustomerTable().getDataprovider().getList());
				//customerLog.setCustomerCost(form.getCreateInvoicePopup().getCustomerTable().getDataprovider().getList());
			
			}
//			if(customerLog.getWarrantyStatus().equalsIgnoreCase("Out Of Warranty")){
//				   list1.addAll(list);
//			}
		
			ArrayList<MaterialRequired> list2 = new ArrayList<MaterialRequired>();
			if(form.getCreateInvoicePopup().getMaterialTable().getDataprovider().getList().size()!=0){
				list2.addAll(form.getCreateInvoicePopup().getMaterialTable().getDataprovider().getList());
			}
//			if(customerLog.getWarrantyStatus().equalsIgnoreCase("Warranty") && customerLog.getCallType().equalsIgnoreCase("Installation")){  
//				list2.addAll(list);
//			  }
//			  if(customerLog.getWarrantyStatus().equalsIgnoreCase("Warranty") && !(customerLog.getCallType().equalsIgnoreCase("Installation")) ){
//				  list2.addAll(list);
//			  }
//			  if(customerLog.getWarrantyStatus().equalsIgnoreCase("AMC")){
//				  list2.addAll(list);
//			  }

			  if(list1.size()!=0){
				   customerLog.setCustomerCost(list1);
				}
			if(list2.size()!=0){
				customerLog.setCompanyCost(list2);
			}
			service.createBilling(customerLog ,customerLog.getCount(),LoginPresenter.loggedInUser ,list, customerLog.getCompanyId(), new AsyncCallback<String>() {
			@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					form.setCustomerLogDetails(null);
					form.getCreateInvoicePopup().hidePopUp();
					form.showDialogMessage("msg: " +caught);
				}

				@Override
				public void onSuccess(String result) {
					// TODO Auto-generated method stub
					form.showDialogMessage("Data saved successfully");
					form.getAccountTable().getDataprovider().getList().remove(customerLog);
					form.getCreateInvoicePopup().hidePopUp();
					form.setCustomerLogDetails(null);
				}
			});
			}
			}
		}
		if (event.getSource().equals(form.getBtgo())) {
			final GWTCAlert alert = new GWTCAlert();
			if (form.getOlbCompany().getSelectedIndex() == 0
					&& form.getDbfromDate().getValue() == null
					&& form.getDbtoDate().getValue() == null) {
				alert.alert("Select atleast one field to search");
			}
			else {
				if(form.getOlbCompany().getSelectedIndex() != 0 && form.getDbfromDate().getValue() == null && form.getDbtoDate() == null){
					searchByFromToDate();
				}
				if (form.getDbfromDate().getValue() != null && form.getDbtoDate() == null){
					searchByFromToDate();
				}
				if (form.getDbfromDate().getValue() == null && form.getDbtoDate() != null){
					searchByFromToDate();
				}
				if (form.getDbfromDate().getValue() != null
						&& form.getDbtoDate() != null) {
					Date formDate = form.getDbfromDate().getValue();
					Date toDate = form.getDbtoDate().getValue();
					if (toDate.before(formDate)) {
						System.out.println("Inside Date Condition");
						searchByFromToDate();
					} else {
						if (toDate.equals(formDate) || toDate.after(formDate)) {
							searchByFromToDate();
						} else {
							alert.alert("To Date should be greater than From Date.");
						}
					}
				}
			}
		}
		/**
		 * Date 15-6-2018 by jayshree
		 */
		
//		if(event.getSource()==form.getCustomerLogPopup().getBtnNewCustomer()){
//			newcustomerPopup.showPopUp();
//		}
		if(event.getSource()==form.getCustomerLogDetailsPopup().getBtnNewCustomer()){
			newcustomerPopup.showPopUp();
		}
		if(event.getSource().equals(newcustomerPopup.getLblOk())){
			if(validateCustomer()){
				saveNewCustomer();
			}
		}
		if(event.getSource().equals(newcustomerPopup.getLblCancel())){
			System.out.println("Cancel");
			newcustomerPopup.hidePopUp();

		}
//		if(event.getSource() instanceof InlineLabel){
//			InlineLabel lnlInline = (InlineLabel) event.getSource();
//			String lblText = lnlInline.getText().trim();
//			System.out.println("lbltext"+lblText);
//			if(lblText.equals("OK")){
//				if(validateCustomer()){
//					saveNewCustomer();
//				}
//			}
//			if(lblText.equals("Cancel")){
//				System.out.println("Cancel");
//				newcustomerPopup.hidePopUp();
//			}
//		}
	}

	public static CustomerLogDetailsForm initalize() {
		CustomerLogDetailsForm form = new CustomerLogDetailsForm();
		CustomerLogDetailsPresenterTable gentable=new CustomerLogTable();
		gentable.setView(form);
		gentable.applySelectionModle();
		CustomerLogDetailsPresenterSearch.staticSuperTable=gentable;
		CustomerLogDetailsPresenterSearch searchpopup= new CustomerLogDetailsPresenterSearchProxy();
		form.setSearchpopupscreen(searchpopup);
		
		CustomerLogDetailsPresenter presenter = new CustomerLogDetailsPresenter(
				form, new CustomerLogDetails());
		form.setToNewState();
		AppMemory.getAppMemory().stickPnel(form);
		return form;
	}

	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.customerlog.CustomerLogDetails")
	public static  class CustomerLogDetailsPresenterSearch extends SearchPopUpScreen<CustomerLogDetails>{

		@Override
		public MyQuerry getQuerry() {
			return null;
		}

		@Override
		public boolean validate() {
			// TODO Auto-generated method stub
			return true;
		}};


	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.customerlog.CustomerLogDetails")
	public static class CustomerLogDetailsPresenterTable extends
			SuperTable<CustomerLogDetails> implements GeneratedVariableRefrence {

		@Override
		public Object getVarRef(String varName) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void createTable() {
			// TODO Auto-generated method stub

		}

		@Override
		protected void initializekeyprovider() {
			// TODO Auto-generated method stub

		}

		@Override
		public void addFieldUpdater() {
			// TODO Auto-generated method stub

		}

		@Override
		public void setEnable(boolean state) {
			// TODO Auto-generated method stub

		}

		@Override
		public void applyStyle() {
			// TODO Auto-generated method stub
		}
	};

	private void saveCustomerCallLogDetails() {
		final CustomerLogDetails customerLogDetails = getCustomerLog();//new CustomerLogDetails();
		form.setCustomerLogDetails(null);
		form.showWaitSymbol();
		service.saveCustomerCallLogDetails(customerLogDetails,
				model.getCompanyId(), new AsyncCallback<CustomerLogDetails>() {
					@Override
					public void onFailure(Throwable caught) {
						form.hideWaitSymbol();
						CustomerLogDetailsForm.popupFlag = false;
						form.showDialogMessage("An unexpected error occurred");
						form.getCustomerLogPopup().hidePopUp();
						form.getCustomerLogDetailsPopup().hidePopUp();
					}
					@Override
					public void onSuccess(CustomerLogDetails result) {
						form.hideWaitSymbol();
						form.showDialogMessage("Data Saved Successfully");
						if(customerLogDetails.getCount() == 0){
							form.getCustomerLogTable().getDataprovider().getList().add(result);
							if(customerLogDetails.getWarrantyStatus().equalsIgnoreCase("Out Of Warranty")){
			                  form.getWareHouseTable().getDataprovider().getList().add(result);
			                  form.getAccountTable().getDataprovider().getList().add(result);
	                         }
							if(customerLogDetails.getWarrantyStatus().equalsIgnoreCase("Warranty") && customerLogDetails.getCallType().equalsIgnoreCase("Installation")){
								  form.getWareHouseTable().getDataprovider().getList().add(result);
				                  form.getAccountTable().getDataprovider().getList().add(result);
							}
							Comparator<SuperModel> comp=new Comparator<SuperModel>() {
								@Override
								public int compare(SuperModel arg0, SuperModel arg1) {
									// TODO Auto-generated method stub
									if (arg0.getCount() == arg1.getCount()) {
										return 0;
									}
									if (arg0.getCount() > arg1.getCount()) {
										return -1;
									} else {
										return 1;
									}
								}		
							};
							Collections.sort(form.getCustomerLogTable().getDataprovider().getList(), comp);
							form.setLogCount(form.getCustomerLogTable().getDataprovider().getList()); 

						}else{
							int index = 0;
							List<CustomerLogDetails> list = form.getCustomerLogTable().getDataprovider().getList();
						//	RowCountChangeEvent.fire(form.getCustomerLogTable().getTable(), form.getCustomerLogTable().getDataprovider().getList().size(), true);
							if(list.contains(customerLogDetails)){
								index = form.getCustomerLogTable().getDataprovider().getList().indexOf(customerLogDetails);
								form.getCustomerLogTable().getDataprovider().getList().set(index, result);
							}	
							if(result.getStatus().equalsIgnoreCase(CustomerLogDetails.MATERIALPENDNG)){							
								if(list.contains(customerLogDetails)){
									form.getWareHouseTable().getTable().redraw();
								}else{
									//f+orm.getWareHouseTable().getDataprovider().getList().add(result);
									form.retriveTable(form.getCustomerLogQuery(), form.getWareHouseTable());
								}
							}
							if(result.getStatus().equalsIgnoreCase(CustomerLogDetails.COMPLETED)){
								if(list.contains(customerLogDetails)){
									form.getAccountTable().getTable().redraw();
								}else{
									form.retriveTable(form.getCustomerLogQuery(), form.getAccountTable());
									//form.getAccountTable().getDataprovider().getList().add(result);
								}
							}
//							if(form.getWareHouseTable().getDataprovider().getList().contains(customerLogDetails)){
//								index = form.getWareHouseTable().getDataprovider().getList().indexOf(customerLogDetails);
//								form.getWareHouseTable().getDataprovider().getList().set(index, result);
//							}
//							if(form.getAccountTable().getDataprovider().getList().contains(customerLogDetails)){
//								index = form.getAccountTable().getDataprovider().getList().indexOf(customerLogDetails);
//								form.getAccountTable().getDataprovider().getList().set(index, result);
//							}
						}		
						CustomerLogDetailsForm.popupFlag = false;
						form.getCustomerLogPopup().hidePopUp();
						form.getCustomerLogDetailsPopup().hidePopUp();
					}

				});
	}
	private CustomerLogDetails getCustomerLog(){
		CustomerLogDetails customerLog = new CustomerLogDetails();
		if (CustomerLogDetailsForm.popupFlag == true) {
			System.out.println("edit popup");
			/**
			 * Date 15-6-201 by jayshree
			 */
			if(form.getCustomerLogDetailsPopup().getCustomerLogObj()!=null){
				customerLog = form.getCustomerLogDetailsPopup().getCustomerLogObj();
			}
			
			if(form.getCustomerLogDetailsPopup().getPersonInfoComposite().getValue()!=null)
			{
				customerLog.setCinfo(form.getCustomerLogDetailsPopup().getPersonInfoComposite().getValue());
			}
			if(form.getCustomerLogDetailsPopup().getOlbCompany().getValue()!=null)
			{
				customerLog.setCompanyName(form.getCustomerLogDetailsPopup().getOlbCompany().getValue());
			}
			
			if(form.getCustomerLogDetailsPopup().getOlbWarrantyStatus().getValue()!=null){
			customerLog.setWarrantyStatus(form.getCustomerLogDetailsPopup().getOlbWarrantyStatus().getValue());
			}
			//End by Jayshree
			
			if(form.getCustomerLogDetailsPopup().getAddressComposite().getValue()!=null){
				customerLog.setAddress(form.getCustomerLogDetailsPopup().getAddressComposite().getValue());
			}
			customerLog.setComplaintNumber(form.getCustomerLogDetailsPopup().getTbComplaintNumber().getValue());
			customerLog.setCallType(form.getCustomerLogDetailsPopup().getOlbCallType().getValue());
			if(form.getCustomerLogDetailsPopup().getOlbTechnician().getSelectedIndex() != 0){
				customerLog.setTechnician(form.getCustomerLogDetailsPopup().getOlbTechnician().getValue());
			}
			if(form.getCustomerLogDetailsPopup().getDbAppointmentDate().getValue()!=null)
			customerLog.setAppointmentDate(form.getCustomerLogDetailsPopup().getDbAppointmentDate().getValue());
			if(form.getCustomerLogDetailsPopup().getTbCellNumber2().getValue()!=null && !(form.getCustomerLogDetailsPopup().getTbCellNumber2().getValue().equals(""))){
				System.out.println("2nd phone number : " +form.getCustomerLogDetailsPopup().getTbCellNumber2().getValue());
				customerLog.setCellNumber2(Long.parseLong(form.getCustomerLogDetailsPopup().getTbCellNumber2().getValue()));
			}
//			if(form.getCustomerLogDetailsPopup().getOlbProduct().getSelectedIndex()!=0)
//			customerLog.setProduct(form.getCustomerLogDetailsPopup().getOlbProduct().getValue());
			customerLog.setProdSerialNumber(form.getCustomerLogDetailsPopup().getTbSerialNo().getValue());
			if(form.getCustomerLogDetailsPopup().getTaRemark().getValue()!=null || form.getCustomerLogDetailsPopup().getTaRemark().getValue().equals("")){
				customerLog.setRemark(form.getCustomerLogDetailsPopup().getTaRemark().getValue());
			}
			if(form.getCustomerLogDetailsPopup().getTbModelNo().getValue()!=null || form.getCustomerLogDetailsPopup().getTbModelNo().getValue().equals("")){
				customerLog.setModelNo(form.getCustomerLogDetailsPopup().getTbModelNo().getValue());
			}
			if(form.getCustomerLogDetailsPopup().getOlbVisitStatus().getSelectedIndex()!=0){
				customerLog.setVisitStatus(form.getCustomerLogDetailsPopup().getOlbVisitStatus().getValue());
			}	
			if(form.getCustomerLogDetailsPopup().getOlbCallbackStatus().getSelectedIndex() !=0)
			customerLog.setCallbackStatus(form.getCustomerLogDetailsPopup().getOlbCallbackStatus().getValue());
			if(form.getCustomerLogDetailsPopup().getOlbCallbackStatus().getSelectedIndex() != 0){
			if(form.getCustomerLogDetailsPopup().getOlbCallbackStatus().getValue().equalsIgnoreCase("IN") || form.getCustomerLogDetailsPopup().getOlbCallbackStatus().getValue().equalsIgnoreCase("OUT"))
				customerLog.setCallback(true);
			}
			if(form.getCustomerLogDetailsPopup().getProductInfoComposite().getValue()!=null){
			MaterialRequired materialRequired = new MaterialRequired();
			materialRequired.setProductId(Integer
					.parseInt(form.getCustomerLogDetailsPopup().getProductInfoComposite().getProdID().getValue()));
			materialRequired.setProductCode(form.getCustomerLogDetailsPopup().getProductInfoComposite()
					.getProdCode().getValue());
			materialRequired.setProductName(form.getCustomerLogDetailsPopup().getProductInfoComposite()
					.getProdName().getValue());
			materialRequired.setProductCategory(form.getCustomerLogDetailsPopup().getProductInfoComposite()
					.getProdCategory().getValue());
			materialRequired.setProductIssueQty(1);
			materialRequired.setProductPrice(form.getCustomerLogDetailsPopup().getProductInfoComposite().getProdPrice().getValue() - customerLog.getSchemeAmount());
			materialRequired.setProductUOM(form.getCustomerLogDetailsPopup().getProductInfoComposite()
					.getUnitOfmeasuerment().getValue());		
//			materialRequired.setVatTax(superProdEntity.getVatTax());
//			materialRequired.setServiceTax(superProdEntity.getServiceTax());
			materialRequired.setDiscountAmt(0);
			ArrayList<MaterialRequired> list = new ArrayList<MaterialRequired>();
			list.add(materialRequired);
            customerLog.setServiceCharges(list);
            Console.log("service charge :"+ customerLog.getServiceCharges().size());
			}
			customerLog.setSchemeFlag(form.getCustomerLogDetailsPopup().cbScheme.getValue());
			if(Integer.parseInt(form.getCustomerLogDetailsPopup().getTbSchemeAmount().getValue()) !=0){
				customerLog.setSchemeAmount(Double.parseDouble(form.getCustomerLogDetailsPopup().getTbSchemeAmount().getValue()));
			}
		} else {
			System.out.println("new popup");	
			if(form.getCustomerLogPopup().getPersonInfoComposite().getValue()!=null){
				customerLog.setCinfo(form.getCustomerLogPopup().getPersonInfoComposite().getValue());
			}
//			customerLog.getCinfo().setFullName(
//					form.getCustomerLogPopup().getTbCustomerName().getValue().toUpperCase().trim());
//			customerLog.getCinfo().setCellNumber((form.getCustomerLogPopup().getLbCellNumber().getValue()));
			customerLog.setWarrantyStatus(form.getCustomerLogPopup()
					.getOlbWarrantyStatus().getValue());
			if(form.getCustomerLogPopup().getOlbTechnician().getSelectedIndex() != 0){
				customerLog.setTechnician(form.getCustomerLogPopup()
					.getOlbTechnician().getValue());
			}
			customerLog.setCompanyName(form.getCustomerLogPopup()
					.getOlbCompany().getValue());
			customerLog.setCallType(form.getCustomerLogPopup().getOlbCallType()
					.getValue());
			customerLog.setComplaintNumber(form.getCustomerLogPopup().getTbComplaintNumber()
					.getValue());
			customerLog.setReapeatCall(form.getCustomerLogPopup().cbRepeatCall.getValue());
			}
		return customerLog;
	}
//	private void createInvoice(CustomerLogDetails customerLog){
//		customerLog.setApproverName(LoginPresenter.loggedInUser);
//		System.out.println("log : "+ customerLog+"user: "+LoginPresenter.loggedInUser);
//		form.showWaitSymbol();
//		service.createBilling(customerLog.getCount(), customerLog.getCompanyId(), new AsyncCallback<String>() {
//
//			@Override
//			public void onFailure(Throwable caught) {
//				// TODO Auto-generated method stub
//				form.hideWaitSymbol();
//				form.getCreateInvoicePopup().hidePopUp();
//				form.setCustomerLogDetails(null);
//				form.showDialogMessage("exception"+caught);
//			}
//
//			@Override
//			public void onSuccess(String result) {
//				// TODO Auto-generated method stub
//				form.showDialogMessage("Data Saved Successfully");
//				form.setCustomerLogDetails(null);
//				form.getCreateInvoicePopup().hidePopUp();
//			}
//			
//		});
//		service.createInvoice(customerLog, new AsyncCallback<String>(){
//			@Override
//			public void onFailure(Throwable caught) {
//				// TODO Auto-generated method stub
//				form.hideWaitSymbol();
//				form.getCreateInvoicePopup().hidePopUp();
//				form.setCustomerLogDetails(null);
//				form.showDialogMessage("exception"+caught);
//			}
//			@Override
//			public void onSuccess(String result) {
//				// TODO Auto-generated method stub
//				form.showDialogMessage("Data Saved Successfully");
//				form.setCustomerLogDetails(null);
//				form.getCreateInvoicePopup().hidePopUp();
//				
//				if(customerLogDetails.getCount() != 0){
//					int index = 0;
//					if(form.getAccountTable().getDataprovider().getList().contains(customerLogDetails)){
//						index = form.getAccountTable().getDataprovider().getList().indexOf(customerLogDetails);
//						form.getAccountTable().getDataprovider().getList().set(index, result);
//					}	
//			}
//			}
			
//		});
//	}

	private void searchByFromToDate() {
		Vector<Filter> filtervec = null;
		Filter filter = null;
		MyQuerry querry;
		List<String> statusList = new ArrayList<String>();
		statusList.add(CustomerLogDetails.CREATED);
		statusList.add(CustomerLogDetails.MATERIAISSUED);
		statusList.add(CustomerLogDetails.MATERIALPENDNG);
		statusList.add(CustomerLogDetails.ONHOLD);
		statusList.add(CustomerLogDetails.COMPLETED);
		filtervec = new Vector<Filter>();

		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(model.getCompanyId());
		filtervec.add(filter);

		filter = new Filter();
		filter.setQuerryString("status IN");
		filter.setList(statusList);
		filtervec.add(filter);

		if (form.getDbfromDate().getValue() != null) {
			filter = new Filter();
			filter.setQuerryString("createdOn >=");
			filter.setDateValue(form.getDbfromDate().getValue());
			filtervec.add(filter);
		}

		if (form.getDbtoDate().getValue() != null) {
			filter = new Filter();
			filter.setQuerryString("createdOn <=");
			filter.setDateValue(form.getDbtoDate().getValue());
			filtervec.add(filter);
		}

		if (form.getOlbCompany().getSelectedIndex() != 0) {
			System.out.println("inside customer log***********");
			filter = new Filter();
			filter.setQuerryString("companyName");
			filter.setStringValue(form.getOlbCompany().getValue());
			filtervec.add(filter);
		}

		querry = new MyQuerry();
		querry.setQuerryObject(new CustomerLogDetails());
		querry.setFilters(filtervec);
		//form.retriveTable(querry, form.getCustomerLogTable());
		form.retriveTable(querry, form.getCustomerLogTable());
//		form.setLogCount(form.getCustomerLogTable().getDataprovider().getList());
//		form.retriveTable(querry, form.getWareHouseTable());
//		form.retriveTable(querry, form.getAccountTable());
	}
	
	@Override
	public void reactOnSearch() {
		super.reactOnSearch();
	}	
	private String validatePaymentDetails(CustomerLogDetails customerLog){
		if (customerLog.getWarrantyStatus().equalsIgnoreCase("Warranty") && customerLog.getCallType().equalsIgnoreCase("Installation") && !(customerLog.isSchemeFlag())) {
			  if(form.getCreateInvoicePopup().getServiceCharegesTable().getDataprovider().getList().size() == 0)
				  return "size";
		}
		if (customerLog.getWarrantyStatus().equalsIgnoreCase("Out of Warranty")&&form.getCreateInvoicePopup().getServiceCharegesTable().getDataprovider().getList().size() == 0) {
			return "size";
		}
		if (customerLog.getPaymentInfo().getPaymentAmt() != 0) {
			if (form.getCreateInvoicePopup().getOlbPaymentMethods()
					.getSelectedIndex() == 0) {
				return "paymethod";
			}
			if (form.getCreateInvoicePopup().getTbBookNo().getValue() == null || form.getCreateInvoicePopup().getTbBookNo().getValue().equals("")) {
				return "bookno";
			}
			if (form.getCreateInvoicePopup().getTbTCRNo().getValue() == null || form.getCreateInvoicePopup().getTbTCRNo().getValue().equals("")) {
				return "tcrno";
			}
			if (form.getCreateInvoicePopup().getTbAmountReceived().getValue() == "0") {
				return "amount";
			}
		}
		return "true";
	}
	@Override
	public void reactOnDownload(){
		
		ArrayList<CustomerLogDetails> logArray=new ArrayList<CustomerLogDetails>();
		List<CustomerLogDetails> list=(List<CustomerLogDetails>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
		logArray.addAll(list); 
		csvservice.setCustomerCallLog(logArray, new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);
				
			}

			@Override
			public void onSuccess(Void result) {
				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "csvservlet"+"?type="+140;//addedby jayashree on 19.6.2018
				Window.open(url, "test", "enabled");
			}
		});
	}
  private boolean checkComplaintNumber(){
	  List<CustomerLogDetails> list = form.getCustomerLogTable().getDataprovider().getList();
	  if(form.getCustomerLogPopup().getTbComplaintNumber().getValue()!=null){
		for(CustomerLogDetails customerLogDetails : list){
			if(!customerLogDetails.getComplaintNumber().equals("")){
			if(customerLogDetails.getComplaintNumber().equals(form.getCustomerLogPopup().getTbComplaintNumber().getValue()) && !(form.getCustomerLogPopup().cbRepeatCall.getValue())){
				form.showDialogMessage("Complaint number is already exist");
				return false;
			}
		  }
		}
	  }
	  return true;
  }
  private void saveNewCustomer() {
		final Customer cust=new Customer();
		
		if(!newcustomerPopup.getTbFullName().getValue().trim().equals("")){
			cust.setFullname(newcustomerPopup.getTbFullName().getValue().toUpperCase().trim());
		}
		if(!newcustomerPopup.getTbCompanyName().getValue().trim().equals(""))
		{
			System.out.println("Inside if condition");
			cust.setCompanyName(newcustomerPopup.getTbCompanyName().getValue().toUpperCase().trim());
			cust.setCompany(true);
		}
		
		
		if(!newcustomerPopup.getTbCellNo().getValue().equals("")){
			cust.setCellNumber1(Long.parseLong(newcustomerPopup.getTbCellNo().getValue().trim()));
		}
		if(!newcustomerPopup.getEtbEmail().getValue().equals("")){
			cust.setEmail(newcustomerPopup.getEtbEmail().getValue().trim());
		}
		//  rohan added this code for setting branch in customer 
		
		if(newcustomerPopup.getOlbbBranch().getSelectedIndex()!=0){
			cust.setBranch(newcustomerPopup.getOlbbBranch().getValue(newcustomerPopup.getOlbbBranch().getSelectedIndex()).trim());
		}
		
		/**
		 * Date : 08-08-2017 by ANIL
		 */
		boolean addressFlag=false;
		if(newcustomerPopup.getAddressComp().getValue()!=null){
			cust.setAdress(newcustomerPopup.getAddressComp().getValue());
			cust.setSecondaryAdress(newcustomerPopup.getAddressComp().getValue());
			addressFlag=true;
		}
		
		if(newcustomerPopup.getTbGstNumber().getValue()!=null&&!newcustomerPopup.getTbGstNumber().getValue().equals("")){
			ArticleType articalType=new ArticleType();
			articalType.setDocumentName("Invoice");
			articalType.setArticleTypeName("GSTIN");
			articalType.setArticleTypeValue(newcustomerPopup.getTbGstNumber().getValue());
			articalType.setArticlePrint("Yes");
			cust.getArticleTypeDetails().add(articalType);
		}
		
		if(addressFlag){
			cust.setNewCustomerFlag(false);
		}else{
			cust.setNewCustomerFlag(true);
		}
		/**
		 * End
		 */
		form.showWaitSymbol();
		genasync.save(cust,new AsyncCallback<ReturnFromServer>() {
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
				form.showDialogMessage("An unexpected error occurred");
			}
			@Override
			public void onSuccess(ReturnFromServer result) {
				System.out.println("BEFORRERERRERERERRERE");
				newcustomerPopup.hidePopUp();
				cust.setCount(result.count);
				setCustomerDetails(cust);
				form.hideWaitSymbol();
			}
		});
		
		/**
		 * Date 21-6-2018
		 * by jayshree
		 */
		form.getCustomerLogDetailsPopup().addressComposite.setValue(newcustomerPopup.getAddressComp().getValue());
	}
	
	public void setCustomerDetails(Customer entity){
		try {
			PersonInfo info=new PersonInfo();
			
			if(entity.isCompany()==true)
			{
				info.setCount(entity.getCount());
				info.setFullName(entity.getCompanyName());
				info.setCellNumber(entity.getCellNumber1());
				info.setPocName(entity.getFullname());
			}
			else
			{
				info.setCount(entity.getCount());
				info.setFullName(entity.getFullname());
				info.setCellNumber(entity.getCellNumber1());
				info.setPocName(entity.getFullname());
			}
						
			PersonInfoComposite.globalCustomerArray.add(info);
			setNewCustomerDetails(info);
			form.showDialogMessage("Customer Saved Successfully!");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}
	
	public void setNewCustomerDetails(PersonInfo pInfo){
		try {
			
			
			form.getCustomerLogDetailsPopup().getPersonInfoComposite().getId().setValue(pInfo.getCount()+"");
			form.getCustomerLogDetailsPopup().getPersonInfoComposite().getName().setValue(pInfo.getFullName());
			form.getCustomerLogDetailsPopup().getPersonInfoComposite().getTbpocName().setValue(pInfo.getPocName());
			form.getCustomerLogDetailsPopup().getPersonInfoComposite().getPhone().setValue(pInfo.getCellNumber()+"");
			System.out.println(" set customer values  "+pInfo.getFullName());
			form.getCustomerLogDetailsPopup().getPersonInfoComposite().setPersonInfoHashMap(pInfo);
			/***
			 * Date 14-6-2018
			 * by jayshree
			 */
//			form.getCustomerLogPopup().getPersonInfoComposite().getId().setValue(pInfo.getCount()+"");
//			form.getCustomerLogPopup().getPersonInfoComposite().getName().setValue(pInfo.getFullName());
//			form.getCustomerLogPopup().getPersonInfoComposite().getTbpocName().setValue(pInfo.getPocName());
//			form.getCustomerLogPopup().getPersonInfoComposite().getPhone().setValue(pInfo.getCellNumber()+"");
//			System.out.println(" set customer values  "+pInfo.getFullName());
//			form.getCustomerLogPopup().getPersonInfoComposite().setPersonInfoHashMap(pInfo);
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}
	
	/**
	 * ends here
	 */
	public boolean validateCustomer() {

		//  rohan added these validations 
		
		if(newcustomerPopup.getTbFullName().getValue().equals("")){
			form.showDialogMessage("Please add Customer Fullname..!");
			return false;
		}
		
		if(newcustomerPopup.getOlbbBranch().getSelectedIndex()==0){
			form.showDialogMessage("Branch is mandetory..!");
			return false;
		}
		
		if(newcustomerPopup.getTbCellNo().getValue().equals("") && newcustomerPopup.getEtbEmail().getValue().equals("")){
			form.showDialogMessage("Please Fill either customer cell or customer email..!");
			return false;
		}
		
		System.out.println("value address=="+newcustomerPopup.getAddressComp());
		
		if(newcustomerPopup.getAddressComp().getValue()==null){
			form.showDialogMessage("Please add address..!");
			return false;
		}
		
		if(!newcustomerPopup.getTbFullName().getValue().equals("") || !newcustomerPopup.getTbCompanyName().getValue().equals("")  ){
			if(newcustomerPopup.getAddressComp().getAdressline1().getValue().equals("") || newcustomerPopup.getAddressComp().getCountry().getSelectedIndex()==0 || newcustomerPopup.getAddressComp().getState().getSelectedIndex()==0 || newcustomerPopup.getAddressComp().getCity().getSelectedIndex()==0 ){
				form.showDialogMessage("Please add new customer complete address!");
				return false;
			}
		}
		
//		if(!custpoppup.getTbfName().getValue().equals("")&&custpoppup.getTblName().getValue().equals("")){
//			form.showDialogMessage("Please Fill Customer Last Name!");
//			return false;
//		}
//		if(custpoppup.getTbfName().getValue().equals("")&&!custpoppup.getTblName().getValue().equals("")){
//			form.showDialogMessage("Please Fill Customer First Name!");
//			return false;
//		}
//		
//		if((!custpoppup.getTbfName().getValue().equals("")&&!custpoppup.getTblName().getValue().equals(""))&&(custpoppup.getPnbLandlineNo().getValue().equals(""))){
//			form.showDialogMessage("Please Fill customer cell!");
//			return false;
//		}
		return true;
	}

	/***
	 * Date 21-5-2018
	 * by jayshree
	 */
	
	@Override
	public void onSelection(SelectionEvent<Suggestion> event) {if(event.getSource().equals(form.getCustomerLogDetailsPopup().getPersonInfoComposite().getId())||event.getSource().equals(form.getCustomerLogDetailsPopup().getPersonInfoComposite().getName())||event.getSource().equals(form.getCustomerLogDetailsPopup().getPersonInfoComposite().getPhone())){
		form.customerLogDetailsPopup.checkCustomerStatus(form.getCustomerLogDetailsPopup().getPersonInfoComposite().getIdValue());
		try {
			retrieveShippingAddress();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}}

	private void retrieveShippingAddress() {
//		customerLogDetailsPopup.showWaitSymbol();
		 Timer timer=new Timer() 
	     {
			@Override
			public void run() 
			{
			MyQuerry querry=new MyQuerry();
			Vector<Filter> filtervec=new Vector<Filter>();
			Filter tempfilter=new Filter();
			tempfilter=new Filter();
			tempfilter.setQuerryString("count");
			int custId=Integer.parseInt(form.getCustomerLogDetailsPopup().getPersonInfoComposite().getId().getValue());
			tempfilter.setIntValue(custId);
			filtervec.add(tempfilter);
			tempfilter=new Filter();
			tempfilter.setQuerryString("companyId");
			tempfilter.setLongValue(model.getCompanyId());
			filtervec.add(tempfilter);
			querry.setFilters(filtervec);
			querry.setQuerryObject(new Customer());
			genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
					
					@Override
					public void onFailure(Throwable caught) {
						form.getCustomerLogDetailsPopup().showDialogMessage("An Unexpected error occurred!");
//						customerLogDetailsPopup.hideWaitSymbol();
					}
		
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						System.out.println("Size Of Result In SalesQuotation Presenter"+result.size());
						for(SuperModel model:result)
						{
							Customer custentity = (Customer)model;
							form.getCustomerLogDetailsPopup().getAddressComposite().setValue(custentity.getSecondaryAdress());
							
//							if(!checkCompanyState()){
//								customerLogDetailsPopup.getCbcformlis().setEnabled(true);
//							}
//							customerLogDetailsPopup.hideWaitSymbol();
						}
						}
					 });
//			form.hideWaitSymbol();
			}
   	 };
   	 timer.schedule(3000);		
//   	 form.hideWaitSymbol();
	}

}

