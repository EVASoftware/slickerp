package com.slicktechnologies.client.views.customerlog;

import java.util.ArrayList;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.views.inventory.materialreuestnote.MaterialRequestNotePresenter;
import com.slicktechnologies.client.views.inventory.recievingnote.InventoryLocationPopUp;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.customerlog.MaterialRequired;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;


public class DefectiveMaterialIssueTable extends SuperTable<MaterialRequired> implements ClickHandler {
	final GenricServiceAsync async = GWT.create(GenricService.class);
	NumberFormat nf=NumberFormat.getFormat("0.00");
	
	InventoryLocationPopUp invLoc=new InventoryLocationPopUp();
	PopupPanel panel=new PopupPanel(true);
	int rowIndex;
	
	TextColumn<MaterialRequired> getColumnProductId;
	TextColumn<MaterialRequired> getColumnProductCode;
	TextColumn<MaterialRequired> getColumnProductName;
	TextColumn<MaterialRequired> getColumnProductAvailableQty;
	TextColumn<MaterialRequired> getViewColumnProductRequiredQty;
	Column<MaterialRequired, String> getColumnProductRequiredQty;
	TextColumn<MaterialRequired> getColumnProductUom;
	Column<MaterialRequired, String> getColumnProductDeleteButton;
	Column<MaterialRequired, String> addColumn;
	Column<MaterialRequired, Boolean> checkRecord;
	TextColumn<MaterialRequired> getColumnProdPrice;
	public  ArrayList<String> warehouseList;
	public  ArrayList<String> storageBinList;
	public  ArrayList<String> storagelocList;
	
	//public  ArrayList<Storagebin> storagelocList1;
	
	Column<MaterialRequired, String> warehouseColumn;
	TextColumn<MaterialRequired> warehouseViewColumn;
	
	Column<MaterialRequired, String> storagebin;
	Column<MaterialRequired, String> columnviewBin;
	
	Column<MaterialRequired, String> storageLoc;
	TextColumn<MaterialRequired> columnviewLoc;
	
	public DefectiveMaterialIssueTable() {
		invLoc.getAddButton().addClickHandler(this);
		invLoc.getCancelButton().addClickHandler(this);
	}
	
	
	@Override
	public void createTable() {
		addColumnProductId();
		addColumnProductCode();
		addColumnProductName();
		addColumnProdPrice();
		addColumnCheckBox();
		addColumnProductRequiredQty();
		addColumnProductAvailableQty();
		//addColumnProductUom();
		viewWarehouse();
		viewstarageLoc();
		viewstorageBin();
		createColumnAddColumn();
		addColumnProductDeleteButton();
		addFieldUpdater();
		table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);	
	}

	public void addViewColumn(){
		addColumnProductId();
		addColumnProductCode();
		addColumnProductName();
		addColumnProdPrice();
		addColumnCheckBox();
		addColumnProductViewRequiredQty();
		addColumnProductAvailableQty();
		//addColumnProductUom();
		
		viewWarehouse();
		viewstarageLoc();
		viewstorageBin();
		
	}

	public void addEditColumn(){
		addColumnProductId();
		addColumnProductCode();
		addColumnProductName();
		addColumnProdPrice();
		addColumnCheckBox();
		addColumnProductRequiredQty();
		addColumnProductAvailableQty();
		//addColumnProductUom();
		
		viewWarehouse();
		viewstarageLoc();
		viewstorageBin();
		createColumnAddColumn();	
		addColumnProductDeleteButton();
		addFieldUpdater();
	}
	
	@Override
	public void addFieldUpdater() {
		addColumnDeleteUpdater();
		setFieldUpdateOnCheckbox();
		addColumnProductQtyUpdater();
		createFieldUpdaterAdd();
	
		//addColumnRemarkUpdater();
	}

	public void setEnableOnlySelectWarehouseButton() {
		int tablecolcount = this.table.getColumnCount();
		for (int i = tablecolcount - 1; i > -1; i--)
		table.removeColumn(i);
		
		addColumnProductId();
		addColumnProductCode();
		addColumnProductName();
		addColumnProductViewRequiredQty();
		addColumnProductAvailableQty();
		addColumnProductUom();
		//addColumnProductViewRemarks();
		viewWarehouse();
		viewstarageLoc();
		viewstorageBin();
		createColumnAddColumn();
		addFieldUpdater();
	}


	/**
	 * ends here 
	 */

	@Override
	public void setEnable(boolean state) {
		int tablecolcount = this.table.getColumnCount();
		for (int i = tablecolcount - 1; i > -1; i--)
			table.removeColumn(i);
		if (state == true)
			addEditColumn();
		if (state == false)
			addViewColumn();
	}
	private void addColumnCheckBox() {

		CheckboxCell checkCell = new CheckboxCell();
		checkRecord = new Column<MaterialRequired, Boolean>(checkCell) {

			@Override
			public Boolean getValue(MaterialRequired object) {

				return object.isMaterialCredit();
			}
		};
		table.addColumn(checkRecord, "Credit Received");
		table.setColumnWidth(checkRecord, 100, Unit.PX);

	}
	private void setFieldUpdateOnCheckbox()
	{
		System.out.println("In Field Updator");
		checkRecord.setFieldUpdater(new FieldUpdater<MaterialRequired, Boolean>() {

			@Override
			public void update(int index, MaterialRequired object, Boolean value) {
				try {
					Boolean val1 = (value);
					object.setMaterialCredit(value);
				} catch (Exception e) {
				}
				table.redrawRow(index);

			}
		});
	}

	
	public void createColumnAddColumn() {
		ButtonCell btnCell = new ButtonCell();
		addColumn = new Column<MaterialRequired, String>(btnCell) {
			@Override
			public void render(Context context, MaterialRequired object,
					SafeHtmlBuilder sb) {
				if(object.isFlag()){
					super.render(context, object, sb);
				}
				
			}
			@Override
			public String getValue(MaterialRequired object) {
				if(object.isFlag()){
					return "Select Warehouse";
				}else{
					return "";
				}
			}
		};
		table.addColumn(addColumn, "");
		table.setColumnWidth(addColumn,180,Unit.PX);

	}		
	private void addColumnProductQtyUpdater() {
		getColumnProductRequiredQty.setFieldUpdater(new FieldUpdater<MaterialRequired, String>() {
			@Override
			public void update(int index, MaterialRequired object,String value) {
				try {
					double val1 = Double.parseDouble(value.trim());
					object.setProductIssueQty(val1);
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				} catch (NumberFormatException e) {

				}
				table.redrawRow(index);
			}
		});
	}
	
	private void addColumnDeleteUpdater() {
		getColumnProductDeleteButton.setFieldUpdater(new FieldUpdater<MaterialRequired, String>() {
			@Override
			public void update(int index, MaterialRequired object,String value) {
				getDataprovider().getList().remove(object);
				table.redrawRow(index);
			}
		});
	}
	
	/****************************************** Editable Column  ***********************************************/
	private void addColumnProductDeleteButton() {
		ButtonCell btnCell = new ButtonCell();
		getColumnProductDeleteButton = new Column<MaterialRequired, String>(btnCell) {
			@Override
			public void render(Context context, MaterialRequired object,
					SafeHtmlBuilder sb) {
				if(object.isFlag()){
					super.render(context, object, sb);
				}
				
			}
			@Override
			public String getValue(MaterialRequired object) {
				if(object.isFlag()){
					return "Delete";
				}else{
					return "";
				}
			}
		};
		table.addColumn(getColumnProductDeleteButton, "Delete");
		table.setColumnWidth(getColumnProductDeleteButton,60,Unit.PX);
	}
	
	private void addColumnProductRequiredQty() {
		EditTextCell editCell = new EditTextCell();
		getColumnProductRequiredQty=new Column<MaterialRequired, String>(editCell) {
			@Override
			public String getValue(MaterialRequired object) {
				return nf.format(object.getProductIssueQty());
			}
		};
		table.addColumn(getColumnProductRequiredQty, "#Quantity");
		table.setColumnWidth(getColumnProductRequiredQty,81,Unit.PX);
	}
	
	public void viewWarehouse() {
		warehouseViewColumn = new TextColumn<MaterialRequired>() {
			@Override
			public String getValue(MaterialRequired object) {
				return object.getMaterialProductWarehouse();
			}
		};
		table.addColumn(warehouseViewColumn, "Warehouse");
		table.setColumnWidth(warehouseViewColumn,153,Unit.PX);
	}
	
	public void viewstarageLoc() {
		columnviewLoc = new TextColumn<MaterialRequired>() {
			@Override
			public String getValue(MaterialRequired object) {
				return object.getMaterialProductStorageLocation();
			}
		};
		table.addColumn(columnviewLoc, "Storage Location");
		table.setColumnWidth(columnviewLoc,153,Unit.PX);
	}
	public void viewstorageBin() {
		columnviewBin = new TextColumn<MaterialRequired>() {
			@Override
			public String getValue(MaterialRequired object) {
				return object.getMaterialProductStorageBin();
			}
		};
		table.addColumn(columnviewBin, "Storage Bin");
		table.setColumnWidth(columnviewBin,153,Unit.PX);
	}
	
	private void addColumnProductUom() {
		getColumnProductUom=new TextColumn<MaterialRequired>() {
			@Override
			public String getValue(MaterialRequired object) {
				return object.getProductUOM();
			}
		};
		table.addColumn(getColumnProductUom, "UOM");
		table.setColumnWidth(getColumnProductUom,45,Unit.PX);
	}

	private void addColumnProductAvailableQty() {
		getColumnProductAvailableQty=new TextColumn<MaterialRequired>() {
			@Override
			public String getValue(MaterialRequired object) {
				return nf.format(object.getProductAvailableQty());
			}
		};
		table.addColumn(getColumnProductAvailableQty, "Available Quantity");
		table.setColumnWidth(getColumnProductAvailableQty,180,Unit.PX);
	}

	private void addColumnProductViewRequiredQty() {
		getViewColumnProductRequiredQty=new TextColumn<MaterialRequired>() {
			
			@Override
			public String getValue(MaterialRequired object) {
				return nf.format(object.getProductIssueQty());
			}
		};
		table.addColumn(getViewColumnProductRequiredQty,"#Quantity");
		table.setColumnWidth(getViewColumnProductRequiredQty,81,Unit.PX);
	}

	private void addColumnProductName() {
		getColumnProductName=new TextColumn<MaterialRequired>() {
			@Override
			public String getValue(MaterialRequired object) {
				return object.getProductName();
			}
		};
		table.addColumn(getColumnProductName, "Name");
		table.setColumnWidth(getColumnProductName,153,Unit.PX);
	}

	private void addColumnProductCode() {
		getColumnProductCode=new TextColumn<MaterialRequired>() {
			@Override
			public String getValue(MaterialRequired object) {
				return object.getProductCode();
			}
		};
		table.addColumn(getColumnProductCode, "Code");
		table.setColumnWidth(getColumnProductCode,90,Unit.PX);
	}

	private void addColumnProductId() {
		getColumnProductId=new TextColumn<MaterialRequired>() {
			@Override
			public String getValue(MaterialRequired object) {
				return object.getProductId()+"";
			}
		};
		table.addColumn(getColumnProductId, "ID");
		table.setColumnWidth(getColumnProductId,100,Unit.PX);
	}
	
	protected void addColumnProdPrice() {
	getColumnProdPrice = new TextColumn<MaterialRequired>() {

		@Override
		public String getValue(MaterialRequired object) {
				return object.getProductPrice()+"";
		}
	};
	table.addColumn(getColumnProdPrice, "Price");
	table.setColumnWidth(getColumnProdPrice,80,Unit.PX);
}
	
	public void createFieldUpdaterAdd() {
		addColumn.setFieldUpdater(new FieldUpdater<MaterialRequired, String>() {
			@Override
			public void update(int index, MaterialRequired object, String value) {

				panel = new PopupPanel(true);
				panel.add(invLoc);
				InventoryLocationPopUp.initialzeWarehouse(object.getProductId());
				if (object.getMaterialProductWarehouse() != null
						&& object.getMaterialProductStorageLocation() != null
						&& object.getMaterialProductStorageBin() != null) {
					System.out
							.println("Inside table not null list settt...........");
					for (int i = 0; i < InventoryLocationPopUp.getLsWarehouse()
							.getItemCount(); i++) {
						String data = InventoryLocationPopUp.lsWarehouse
								.getItemText(i);
						if (data.equals(object.getMaterialProductWarehouse())) {
							InventoryLocationPopUp.lsWarehouse
									.setSelectedIndex(i);
							break;
						}
					}

					invLoc.getLsStorageLoc().clear();
					invLoc.lsStorageLoc.addItem("--SELECT--");
					invLoc.lsStorageLoc.addItem(object
							.getMaterialProductStorageLocation());

					invLoc.getLsStoragebin().clear();
					invLoc.lsStoragebin.addItem("--SELECT--");

					for (int i = 0; i < invLoc.arraylist2.size(); i++) {
						if (invLoc.arraylist2.get(i).getWarehouseName()
								.equals(object.getMaterialProductWarehouse())
								&& invLoc.arraylist2
										.get(i)
										.getStorageLocation()
										.equals(object
												.getMaterialProductStorageLocation())) {
							invLoc.lsStoragebin.addItem(invLoc.arraylist2
									.get(i).getStorageBin());
						}
					}

					for (int i = 0; i < invLoc.getLsStorageLoc().getItemCount(); i++) {
						String data = invLoc.lsStorageLoc.getItemText(i);
						if (data.equals(object
								.getMaterialProductStorageLocation())) {
							invLoc.lsStorageLoc.setSelectedIndex(i);
							break;
						}
					}
					for (int i = 0; i < invLoc.getLsStoragebin().getItemCount(); i++) {
						String data = invLoc.lsStoragebin.getItemText(i);
						if (data.equals(object.getMaterialProductStorageBin())) {
							invLoc.lsStoragebin.setSelectedIndex(i);
							break;
						}
					}
				}

				invLoc.formView();
				panel.show();
				panel.center();
				rowIndex = index;
			}
		});
	}

	@Override
	public void onClick(ClickEvent event) {
		if (event.getSource() == invLoc.getAddButton()) {
			if (invLoc.getLsWarehouse().getSelectedIndex() != 0
					&& invLoc.getLsStorageLoc().getSelectedIndex() != 0
					&& invLoc.getLsStoragebin().getSelectedIndex() != 0) {
				ArrayList<MaterialRequired> list = new ArrayList<MaterialRequired>();
				if (getDataprovider().getList().size() != 0) {
					list.addAll(getDataprovider().getList());
					for (int i = rowIndex; i < getDataprovider().getList()
							.size(); i++) {
						list.get(rowIndex).setMaterialProductWarehouse(
								invLoc.getLsWarehouse().getValue(
										invLoc.getLsWarehouse()
												.getSelectedIndex()));
						list.get(rowIndex).setMaterialProductStorageLocation(
								invLoc.getLsStorageLoc().getValue(
										invLoc.getLsStorageLoc()
												.getSelectedIndex()));
						list.get(rowIndex).setMaterialProductStorageBin(
								invLoc.getLsStoragebin().getValue(
										invLoc.getLsStoragebin()
												.getSelectedIndex()));
						list.get(rowIndex).setProductAvailableQty(
								invLoc.getAvailableQty());

						// list.get(rowIndex).setMatReqReOdrLvlQty(invLoc.getReorderQty());
						getDataprovider().getList().clear();
						getDataprovider().getList().addAll(list);

						// ///////////////////

						// double
						// reqQty=list.get(rowIndex).getProductIssueQty();
						// double
						// avlQty=list.get(rowIndex).getProductAvailableQty();
						// double
						// reOdrQty=list.get(rowIndex).getMatReqReOdrLvlQty();
						// double diff=avlQty-reqQty;
						// String
						// location=list.get(rowIndex).getMaterialRequiredWarehouse()+"/"
						// +list.get(rowIndex).getMaterialRequiredStorageLocation()+"/"
						// +list.get(rowIndex).getMaterialRequiredStorageBin();
						//
						// System.out.println("REorder Qty in Select : "+reOdrQty);
						//
						// if(reOdrQty!=0){
						// System.out.println("REorder Qty in Select1 : "+reOdrQty);
						// if(avlQty<=reOdrQty){
						// GWTCAlert alert=new GWTCAlert();
						// alert.alert("Available quantity in warehouse "+location+" is less than reorder level!");
						// }else if(diff<=reOdrQty){
						// GWTCAlert alert=new GWTCAlert();
						// alert.alert("Available quantity in warehouse "+location+" is less than reorder level!");
						// }
						// }
					}
				}
				panel.hide();
			}
			if (invLoc.getLsWarehouse().getSelectedIndex() == 0
					&& invLoc.getLsStorageLoc().getSelectedIndex() == 0
					&& invLoc.getLsStoragebin().getSelectedIndex() == 0) {
				MaterialRequestNotePresenter
						.showMessage("Please Select Warehouse.");
			} else if (invLoc.getLsStorageLoc().getSelectedIndex() == 0
					&& invLoc.getLsStoragebin().getSelectedIndex() == 0) {
				MaterialRequestNotePresenter
						.showMessage("Please Select Storage Location.");
			} else if (invLoc.getLsStoragebin().getSelectedIndex() == 0) {
				MaterialRequestNotePresenter
						.showMessage("Please Select Storage Bin.");
			}

		}
		if (event.getSource() == invLoc.getCancelButton()) {
			panel.hide();
		}
	}

	/*****************************************************************************************************************/

	@Override
	public void applyStyle() {
		
	}
	

	@Override
	protected void initializekeyprovider() {
		
	}
	
	

}

//package com.slicktechnologies.client.views.customerlog;
//
//import java.util.ArrayList;
//
//import com.google.code.p.gwtchismes.client.GWTCAlert;
//import com.google.gwt.cell.client.ButtonCell;
//import com.google.gwt.cell.client.EditTextCell;
//import com.google.gwt.cell.client.FieldUpdater;
//import com.google.gwt.dom.client.Style.Unit;
//import com.google.gwt.event.dom.client.ClickEvent;
//import com.google.gwt.i18n.client.NumberFormat;
//import com.google.gwt.user.cellview.client.Column;
//import com.google.gwt.user.cellview.client.TextColumn;
//import com.google.gwt.user.client.ui.PopupPanel;
//import com.google.gwt.view.client.RowCountChangeEvent;
//import com.simplesoftwares.client.library.appstructure.SuperTable;
//import com.slicktechnologies.client.views.inventory.materialreuestnote.MaterialRequestNotePresenter;
//import com.slicktechnologies.client.views.inventory.recievingnote.InventoryLocationPopUp;
//import com.slicktechnologies.shared.common.customerlog.MaterialRequired;
//import com.slicktechnologies.shared.common.inventory.MaterialRequired;
//
//public class MaterialIssueTable extends SuperTable<MaterialRequired> {
//	NumberFormat nf=NumberFormat.getFormat("0.00");
//	TextColumn<MaterialRequired> prodId;
//	TextColumn<MaterialRequired> prodCode;
//	TextColumn<MaterialRequired> prodName;
//	TextColumn<MaterialRequired> prodCategory;
//	TextColumn<MaterialRequired> prodRequestQty;
//	Column<MaterialRequired, String> prodRequestQtyEdit;
//	TextColumn<MaterialRequired> prodIssueQty;
//	Column<MaterialRequired, String> prodIssueQtyEdit;
//	TextColumn<MaterialRequired> prodAvailableQty;
//	Column<MaterialRequired, String> prodAvailableQtyEdit;
//	TextColumn<MaterialRequired> prodReturnQty;
//	Column<MaterialRequired, String> prodReturnQtyEdit;
//	TextColumn<MaterialRequired> prodUOM;
//	TextColumn<MaterialRequired> prodPrice;
//    TextColumn<MaterialRequired> warehouse;	
//	TextColumn<MaterialRequired> storageBin;
//	TextColumn<MaterialRequired> storageLoc;
//	Column<MaterialRequired, String> addColumn;
//	Column<MaterialRequired, String> deleteColumn;
//	
//	InventoryLocationPopUp invLoc=new InventoryLocationPopUp();
//	PopupPanel panel=new PopupPanel(true);
//	int rowIndex;
//	@Override
//	public void createTable() {
//		addColumnprodID();
//		addColumnprodcode();
//		addColumnprodname();
//		addColumnprodcategory();
//		//addColumnprodRequestQty();
//		addColumnprodIssueQty();
//		addColumnprodAvailableQty();
//		addColumnprodReturnQty();
//		addColumnUnitOfMeasurement();
//		//addColumnprodPrice();
//		addColumnWarehouse();
//		addColumnstarageLoc();
//		addColumnstorageBin();
//		createColumndeleteColumn();
//		addFieldUpdater();
//
//	}
//
//	public void addeditColumn() {
//		addColumnprodID();
//		addColumnprodcode();
//		addColumnprodname();
//		addColumnprodcategory();
//		//addColumnprodRequestQty();
//		addColumnprodIssueQty();
//		addColumnprodAvailableQty();
//		addColumnprodReturnQty();
//		addColumnUnitOfMeasurement();
//		//addColumnprodPrice();
//		createColumndeleteColumn();
//		addFieldUpdater();
//
//	}
//
//	public void addViewColumn() {
//		addColumnprodID();
//		addColumnprodcode();
//		addColumnprodname();
//		addColumnprodcategory();
//		//addColumnviewprodRequestQty();
//		addColumnviewprodIssueQty();
//		addColumnviewprodAvailableQty();
//		addColumnviewprodReturnQty();
//		//addColumnprodPrice();
//		addColumnUnitOfMeasurement();
//	}
//
//	@Override
//	public void addFieldUpdater() {
//		//createFieldUpdaterprodRequestQtyColumn();
//		createFieldUpdaterprodIssueQtyColumn();
//		createFieldUpdaterprodAvailableQtyColumn();
//		createFieldUpdaterprodReturnQtyColumn();
//		createFieldUpdaterDelete();
//		
//	}
//	
//	@Override
//	public void setEnable(boolean state) {
//		int tablecolcount = this.table.getColumnCount();
//		for (int i = tablecolcount - 1; i > -1; i--)
//			table.removeColumn(i);
//		if (state == true)
//			addeditColumn();
//		if (state == false)
//			addViewColumn();
//	}
//	
//	@Override
//	protected void initializekeyprovider() {
//		
//	}
//
//	@Override
//	public void applyStyle() {
//		
//	}
//	
///*********************************************Product View Column ********************************************/
//	
//	public void addColumnprodID() {
//		prodId = new TextColumn<MaterialRequired>() {
//
//			@Override
//			public String getValue(MaterialRequired object) {
//				return object.getProductId() + "";
//			}
//		};
//		table.addColumn(prodId, "ID");
//	}
//	
//	public void addColumnprodcode() {
//		prodCode = new TextColumn<MaterialRequired>() {
//
//			@Override
//			public String getValue(MaterialRequired object) {
//				return object.getProductCode();
//			}
//		};
//		table.addColumn(prodCode, "Code");
//	}
//
//	public void addColumnprodname() {
//		prodName = new TextColumn<MaterialRequired>() {
//
//			@Override
//			public String getValue(MaterialRequired object) {
//				return object.getProductName();
//			}
//		};
//		table.addColumn(prodName, "Name");
//	}
//	
//	public void addColumnprodcategory() {
//		prodCategory = new TextColumn<MaterialRequired>() {
//
//			@Override
//			public String getValue(MaterialRequired object) {
//				return object.getProductCategory();
//			}
//		};
//		table.addColumn(prodCategory, "Category");
//	}
//	
//	
//	//Read Only Quantity Column
//	public void addColumnviewprodRequestQty() {
//		prodRequestQty = new TextColumn<MaterialRequired>() {
//
//			@Override
//			public String getValue(MaterialRequired object) {
//				return nf.format(object.getProductRequestQty());
//			}
//		};
//		table.addColumn(prodRequestQty, "#Quantity");
//	}
//	
//	//Editable Quantity Column
//	public void addColumnprodRequestQty() {
//		EditTextCell editCell = new EditTextCell();
//		prodRequestQtyEdit = new Column<MaterialRequired, String>(editCell) {
//
//			@Override
//			public String getValue(MaterialRequired object) {
//				return nf.format(object.getProductRequestQty());
//			}
//		};
//		table.addColumn(prodRequestQtyEdit, "#Quantity");
//	}
//	
//	protected void addColumnUnitOfMeasurement() {
//		prodUOM = new TextColumn<MaterialRequired>() {
//
//			@Override
//			public String getValue(MaterialRequired object) {
//				if (object.getProductUOM().equals(""))
//					return "N/A";
//				else
//					return object.getProductUOM();
//			}
//		};
//		table.addColumn(prodUOM, "UOM");
//	}
//	
//	protected void addColumnprodPrice() {
//		prodPrice = new TextColumn<MaterialRequired>() {
//
//			@Override
//			public String getValue(MaterialRequired object) {
//					return object.getProductPrice()+"";
//			}
//		};
//		table.addColumn(prodPrice, "Price");
//	}
//
//	public void createColumndeleteColumn() {
//		ButtonCell btnCell = new ButtonCell();
//		deleteColumn = new Column<MaterialRequired, String>(btnCell) {
//			@Override
//			public String getValue(MaterialRequired object) {
//				return "Delete";
//			}
//		};
//		table.addColumn(deleteColumn, "Delete");
//
//	}
//	
//	
//	public void createFieldUpdaterDelete() {
//		deleteColumn
//				.setFieldUpdater(new FieldUpdater<MaterialRequired, String>() {
//					@Override
//					public void update(int index, MaterialRequired object,
//							String value) {
//						getDataprovider().getList().remove(object);
//
//						table.redrawRow(index);
//					}
//				});
//
//	}
//
//	protected void createFieldUpdaterprodRequestQtyColumn() {
//		prodRequestQtyEdit.setFieldUpdater(new FieldUpdater<MaterialRequired, String>() {
//			@Override
//			public void update(int index, MaterialRequired object, String value) {
//
//				try {
//					double val1 = Double.parseDouble(value.trim());
//					object.setProductRequestQty(val1);
//					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
//				} catch (NumberFormatException e) {
//
//				}
//
//				table.redrawRow(index);
//			}
//		});
//	}
//	
//	//Read Only Quantity Column
//	public void addColumnviewprodIssueQty() {
//		prodIssueQty = new TextColumn<MaterialRequired>() {
//
//			@Override
//			public String getValue(MaterialRequired object) {
//				return nf.format(object.getProductIssueQty());
//			}
//		};
//		table.addColumn(prodIssueQty, "#Issue Quantity");
//	}
//	
//	//Editable Quantity Column
//	public void addColumnprodIssueQty() {
//		EditTextCell editCell = new EditTextCell();
//		prodIssueQtyEdit = new Column<MaterialRequired, String>(editCell) {
//
//			@Override
//			public String getValue(MaterialRequired object) {
//				return nf.format(object.getProductIssueQty());
//			}
//		};
//		table.addColumn(prodIssueQtyEdit, "#Issue Quantity");
//	}
//	protected void createFieldUpdaterprodIssueQtyColumn() {
//		prodIssueQtyEdit.setFieldUpdater(new FieldUpdater<MaterialRequired, String>() {
//			@Override
//			public void update(int index, MaterialRequired object, String value) {
//
//				try {
//					double val1 = Double.parseDouble(value.trim());
//					object.setProductIssueQty(val1);
//					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
//				} catch (NumberFormatException e) {
//
//				}
//
//				table.redrawRow(index);
//			}
//		});
//	}
//	//Read Only Quantity Column
//		public void addColumnviewprodAvailableQty() {
//			prodAvailableQty = new TextColumn<MaterialRequired>() {
//
//				@Override
//				public String getValue(MaterialRequired object) {
//					return nf.format(object.getProductAvailableQty());
//				}
//			};
//			table.addColumn(prodAvailableQty, "#Available Quantity");
//		}
//		
//		//Editable Quantity Column
//		public void addColumnprodAvailableQty() {
//			EditTextCell editCell = new EditTextCell();
//			prodAvailableQtyEdit = new Column<MaterialRequired, String>(editCell) {
//
//				@Override
//				public String getValue(MaterialRequired object) {
//					return nf.format(object.getProductAvailableQty());
//				}
//			};
//			table.addColumn(prodAvailableQtyEdit, "#Available Quantity");
//		}
//		protected void createFieldUpdaterprodAvailableQtyColumn() {
//			prodAvailableQtyEdit.setFieldUpdater(new FieldUpdater<MaterialRequired, String>() {
//				@Override
//				public void update(int index, MaterialRequired object, String value) {
//
//					try {
//						double val1 = Double.parseDouble(value.trim());
//						object.setProductAvailableQty(val1);
//						RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
//					} catch (NumberFormatException e) {
//
//					}
//
//					table.redrawRow(index);
//				}
//			});
//		}
//		//Read Only Quantity Column
//		public void addColumnviewprodReturnQty() {
//					prodReturnQty = new TextColumn<MaterialRequired>() {
//
//						@Override
//						public String getValue(MaterialRequired object) {
//							return nf.format(object.getProductReturnQty());
//						}
//					};
//					table.addColumn(prodReturnQty, "#Return Quantity");
//				}
//				
//				//Editable Quantity Column
//		public void addColumnprodReturnQty() {
//					EditTextCell editCell = new EditTextCell();
//					prodReturnQtyEdit = new Column<MaterialRequired, String>(editCell) {
//
//						@Override
//						public String getValue(MaterialRequired object) {
//							return nf.format(object.getProductReturnQty());
//						}
//					};
//					table.addColumn(prodReturnQtyEdit, "#Return Quantity");
//				}
//		protected void createFieldUpdaterprodReturnQtyColumn() {
//					prodReturnQtyEdit.setFieldUpdater(new FieldUpdater<MaterialRequired, String>() {
//						@Override
//						public void update(int index, MaterialRequired object, String value) {
//
//							try {
//								double val1 = Double.parseDouble(value.trim());
//								object.setProductReturnQty(val1);
//								RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
//							} catch (NumberFormatException e) {
//
//							}
//
//							table.redrawRow(index);
//						}
//					});
//				}
//		public void addColumnWarehouse() {
//			warehouse = new TextColumn<MaterialRequired>() {
//				@Override
//				public String getValue(MaterialRequired object) {
//					return object.getMaterialRequiredWarehouse();
//				}
//			};
//			table.addColumn(warehouse, "Warehouse");
//			table.setColumnWidth(warehouse,150,Unit.PX);
//		}
//		
//		public void addColumnstarageLoc() {
//			storageLoc = new TextColumn<MaterialRequired>() {
//				@Override
//				public String getValue(MaterialRequired object) {
//					return object.getMaterialRequiredStorageLocation();
//				}
//			};
//			table.addColumn(storageLoc, "Storage Location");
//			table.setColumnWidth(storageLoc,150,Unit.PX);
//		}
//		public void addColumnstorageBin() {
//			storageBin = new TextColumn<MaterialRequired>() {
//				@Override
//				public String getValue(MaterialRequired object) {
//					return object.getMaterialRequiredStorageBin();
//				}
//			};
//			table.addColumn(storageBin, "Storage Bin");
//			table.setColumnWidth(storageBin,150,Unit.PX);
//		}
//		public void createFieldUpdaterAdd() {
//			addColumn.setFieldUpdater(new FieldUpdater<MaterialRequired, String>() {
//				@Override
//				public void update(int index, MaterialRequired object, String value) {
//					
//					panel=new PopupPanel(true);
//					panel.add(invLoc);
//					InventoryLocationPopUp.initialzeWarehouse(object.getMaterialRequiredId());
//					
//					if(object.getMaterialRequiredWarehouse()!=null&&object.getMaterialRequiredStorageLocation()!=null
//							&&object.getMaterialRequiredStorageBin()!=null){
//						System.out.println("Inside table not null list settt...........");
//						for(int i=0;i<InventoryLocationPopUp.getLsWarehouse().getItemCount();i++)
//						{
//							String data=InventoryLocationPopUp.lsWarehouse.getItemText(i);
//							if(data.equals(object.getMaterialRequiredWarehouse()))
//							{
//								InventoryLocationPopUp.lsWarehouse.setSelectedIndex(i);
//								break;
//							}
//						}
//						
//						invLoc.getLsStorageLoc().clear();
//						invLoc.lsStorageLoc.addItem("--SELECT--");
//						invLoc.lsStorageLoc.addItem(object.getMaterialRequiredStorageLocation());
//						
//						
//						invLoc.getLsStoragebin().clear();
//						invLoc.lsStoragebin.addItem("--SELECT--");
//						
//						for(int i=0;i<invLoc.arraylist2.size();i++){
//							if(invLoc.arraylist2.get(i).getWarehouseName().equals(object.getMaterialRequiredWarehouse())
//									&&invLoc.arraylist2.get(i).getStorageLocation().equals(object.getMaterialRequiredStorageLocation())){
//								invLoc.lsStoragebin.addItem(invLoc.arraylist2.get(i).getStorageBin());
//							}
//						}
//						
//						for(int i=0;i<invLoc.getLsStorageLoc().getItemCount();i++)
//						{
//							String data=invLoc.lsStorageLoc.getItemText(i);
//							if(data.equals(object.getMaterialRequiredStorageLocation()))
//							{
//								invLoc.lsStorageLoc.setSelectedIndex(i);
//								break;
//							}
//						}
//						for(int i=0;i<invLoc.getLsStoragebin().getItemCount();i++)
//						{
//							String data=invLoc.lsStoragebin.getItemText(i);
//							if(data.equals(object.getMaterialRequiredStorageBin()))
//							{
//								invLoc.lsStoragebin.setSelectedIndex(i);
//								break;
//							}
//						}
//					}
//					
//					
//					
//					invLoc.formView();
//					panel.show();
//					panel.center();
//					rowIndex=index;
//				}
//			});
//		}
//
//		@Override
//		public void onClick(ClickEvent event) {
//			if(event.getSource()==invLoc.getAddButton())
//			{	
//				if(invLoc.getLsWarehouse().getSelectedIndex()!=0&&invLoc.getLsStorageLoc().getSelectedIndex()!=0&&invLoc.getLsStoragebin().getSelectedIndex()!=0){
//					ArrayList<MaterialRequired> list=new ArrayList<MaterialRequired>();
//					if(getDataprovider().getList().size()!=0){
//						list.addAll(getDataprovider().getList());
//						for( int i=rowIndex;i<getDataprovider().getList().size();i++){
//							list.get(rowIndex).setMaterialRequiredWarehouse(invLoc.getLsWarehouse().getValue(invLoc.getLsWarehouse().getSelectedIndex()));
//							list.get(rowIndex).setMaterialRequiredStorageLocation(invLoc.getLsStorageLoc().getValue(invLoc.getLsStorageLoc().getSelectedIndex()));
//							list.get(rowIndex).setMaterialRequiredStorageBin(invLoc.getLsStoragebin().getValue(invLoc.getLsStoragebin().getSelectedIndex()));
//							list.get(rowIndex).setMaterialRequiredAvailableQuantity(invLoc.getAvailableQty());
//							
//
//							/**
//							 * Added by Anil On 26-08-2016
//							 * If available qty is below reorder level then generate alert msg. 
//							 */
//							
//							list.get(rowIndex).setMatReqReOdrLvlQty(invLoc.getReorderQty());
//							getDataprovider().getList().clear();
//							getDataprovider().getList().addAll(list);
//							
//							/////////////////////
//							
//							double reqQty=list.get(rowIndex).getMaterialRequiredRequiredQuantity();
//							double avlQty=list.get(rowIndex).getMaterialRequiredAvailableQuantity();
//							double reOdrQty=list.get(rowIndex).getMatReqReOdrLvlQty();
//							double diff=avlQty-reqQty;
//							String location=list.get(rowIndex).getMaterialRequiredWarehouse()+"/"
//									+list.get(rowIndex).getMaterialRequiredStorageLocation()+"/"
//									+list.get(rowIndex).getMaterialRequiredStorageBin();
//							
//							System.out.println("REorder Qty in Select : "+reOdrQty);
//							
//							if(reOdrQty!=0){
//								System.out.println("REorder Qty in Select1 : "+reOdrQty);
//								if(avlQty<=reOdrQty){
//									GWTCAlert alert=new GWTCAlert();
//									alert.alert("Available quantity in warehouse "+location+" is less than reorder level!");
//								}else if(diff<=reOdrQty){
//									GWTCAlert alert=new GWTCAlert();
//									alert.alert("Available quantity in warehouse "+location+" is less than reorder level!");
//								}
//							}
//						}
//					}
//					panel.hide();
//				}
//				if(invLoc.getLsWarehouse().getSelectedIndex()==0&&invLoc.getLsStorageLoc().getSelectedIndex()==0&&invLoc.getLsStoragebin().getSelectedIndex()==0){
//					MaterialRequestNotePresenter.showMessage("Please Select Warehouse.");
//				}
//				else if(invLoc.getLsStorageLoc().getSelectedIndex()==0&&invLoc.getLsStoragebin().getSelectedIndex()==0){
//					MaterialRequestNotePresenter.showMessage("Please Select Storage Location.");
//				}
//				else if(invLoc.getLsStoragebin().getSelectedIndex()==0){
//					MaterialRequestNotePresenter.showMessage("Please Select Storage Bin.");
//				}
//				
//			}
//			if(event.getSource()==invLoc.getCancelButton())
//			{	
//				panel.hide();
//			}
//		}
//				
//}
//

