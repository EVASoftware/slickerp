
package com.slicktechnologies.client.views.customerlog;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCGlassPanel;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.TabPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.view.client.NoSelectionModel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.reusabledata.ConditionDialogBox;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.quicksalesorder.QuickSalesOrderForm;
import com.slicktechnologies.client.views.quicksalesorder.QuickSalesOrderPresenter;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerlog.CustomerLogDetails;
import com.slicktechnologies.shared.common.customerlog.MaterialRequired;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesprocess.SalesOrderProductLineItem;
import com.simplesoftwares.client.library.FieldType;
public class CustomerLogDetailsForm extends FormScreen<CustomerLogDetails> implements ClickHandler{
	NumberFormat nf=NumberFormat.getFormat("0.00");
    CustomerLogTable customerLogTable ;
    WareHouseTable wareHouseTable;
    AccountTable accountTable;
    PersonInfoComposite personInfoComposite;
    DateBox dbfromDate;
    DateBox dbtoDate;
    TextBox tbtotalCallLog;
    TextBox tbregisteredCallLog;
    TextBox tbpendingCallLog;
    TextBox tbmaterialIssuedCallLog;
    TextBox tbcompletedCallLog;
    TextBox tbInstallation;
    TextBox tbBreakdown;
    TextBox tbPMS;
    TextBox tbDemo;
    TextBox tbcallBack;
    ObjectListBox<Branch> olbCompany;
    TabPanel tabPanel;
    Button btgo;
    public static boolean popupFlag = false;
    String[] tableheaderbarnames ;//={"New"};
	
	/** Flow panel for holding the Table header menus */
	FlowPanel tableheader;
	CustomerLogPopup customerLogPopup = new CustomerLogPopup(); 
	CustomerLogDetailsPopup customerLogDetailsPopup = new CustomerLogDetailsPopup();
	CreateInvoicePopup createInvoicePopup = new CreateInvoicePopup();
	String[] backOfficeHeader = new String[]{"New", "Edit Call", "View Sales Order", "Cancel Call" , "BackOffice Refresh"}; //"Mark Completed" "Direct Billing",
	String[] wareHouseHeader = new String[]{"View Call" , "WareHouse Refresh"};
	String[] accountsHeader = new String[]{"View Call" ,"create Bill" , "Reverse Status" ,"Account Refresh"}; //, "View Document"
	CustomerLogDetails customerLogDetails;
	PopupPanel invoicePaymentPanel;
	GenricServiceAsync genasync = GWT.create(GenricService.class);
	int tabId = 0;
	@Override
	public void createScreen() {
		// TODO Auto-generated method stub
		//super.createScreen();
		initalizeWidget();
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		customerLogTable = new CustomerLogTable();
		wareHouseTable = new WareHouseTable();
		accountTable = new AccountTable();
		tabPanel = new TabPanel();	
		//this.processlevelBarNames = new String[]{"New"};
		fbuilder = new FormFieldBuilder("",tabPanel);
		FormField fpanel= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(5).build();
		FormField fgroupingCustomerCallInformation=fbuilder.setlabel("Customer Call Log Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		
		fbuilder = new FormFieldBuilder("From Date", dbfromDate );
		FormField fdbfromDate = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("To Date", dbtoDate);
		FormField fdbtoDate = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Total CallLogs", tbtotalCallLog );
		FormField ftbtotalCallLogs = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Created CallLogs", tbregisteredCallLog);
		FormField ftbregisteredCallLogs= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Pending CallLogs", tbpendingCallLog);
		FormField ftbpendingCallLogs = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Callbacks", tbcallBack);
		FormField ftbcallbacks = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Material Issued", tbmaterialIssuedCallLog);
		FormField ftbmaterialIssuedCallLog = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Completed", tbcompletedCallLog);
		FormField ftbcompletedCallLog = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Installation", tbInstallation);
		FormField ftbInstallation = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Demo", tbDemo);
		FormField ftbDemo = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("PMS", tbPMS);
		FormField ftbPMS = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("BreakDown", tbBreakdown);
		FormField ftbBreakdown = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Company", olbCompany);
		FormField folbCompany = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder();
		FormField fempty = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("", btgo);
		FormField fbtgo = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		FormField[][] formfield = {
				{fgroupingCustomerCallInformation},
				{folbCompany,fdbfromDate , fdbtoDate, fbtgo},
				{ftbtotalCallLogs , ftbregisteredCallLogs ,	ftbpendingCallLogs,ftbmaterialIssuedCallLog ,ftbcompletedCallLog},
				{ftbInstallation ,ftbDemo , ftbPMS ,ftbBreakdown, ftbcallbacks},
				{fpanel}
		};
				this.fields = formfield;
	}

	protected GWTCGlassPanel glassPanel;

	public CustomerLogDetailsForm(){
		
		// super();
		this.tbtotalCallLog.setEnabled(false);
		this.tbregisteredCallLog.setEnabled(false);
		this.tbpendingCallLog.setEnabled(false);
		this.tbcallBack.setEnabled(false);
		this.tbBreakdown.setEnabled(false);
		this.tbDemo.setEnabled(false);
		this.tbInstallation.setEnabled(false);
		this.tbPMS.setEnabled(false);
		this.tbmaterialIssuedCallLog.setEnabled(false);
		this.tbcompletedCallLog.setEnabled(false);
		
		tabPanel.addSelectionHandler(new SelectionHandler<Integer>() {
			@Override
			public void onSelection(SelectionEvent<Integer> event) {
				tabId = event.getSelectedItem();
				System.out.println("TAB ID "+tabId); 
				 glassPanel = new GWTCGlassPanel();
				 	
				 if(tabId==0){
					 customerLogTable.getTable().redraw();
					 setLogCount(customerLogTable.getDataprovider().getList()); 
				 }
				 if(tabId==1){
					 wareHouseTable.getTable().redraw();
					 setLogCount(wareHouseTable.getDataprovider().getList());
				 }
				 else if(tabId==2){
					 accountTable.getTable().redraw();
					 setLogCount(accountTable.getDataprovider().getList());
				 }			
			}
		});
		FlowPanel flowPanel1=new FlowPanel();
		FlowPanel flowPanel2=new FlowPanel();
		FlowPanel flowPanel3=new FlowPanel();

		String tabTitleBackoffice = "BackOffice";
		String tabTitleWarehouse = "WareHouse";
		String tabTitleAccounts = "Account";
		if(LoginPresenter.myUserEntity.getRole().getRoleName().trim().equalsIgnoreCase("WareHouse") || LoginPresenter.myUserEntity.getRole().getRoleName().trim().equalsIgnoreCase("Account")){		
		}else{
			setHeaderMenu(backOfficeHeader);
			flowPanel1.add(tableheader);
		}
		flowPanel1.add(customerLogTable.getTable());

		if(LoginPresenter.myUserEntity.getRole().getRoleName().trim().equalsIgnoreCase("BackOffice") || LoginPresenter.myUserEntity.getRole().getRoleName().trim().equalsIgnoreCase("Account")){		
		}else{
			setHeaderMenu(wareHouseHeader);
			flowPanel2.add(tableheader);
		}
		flowPanel2.add(wareHouseTable.getTable());
		if(LoginPresenter.myUserEntity.getRole().getRoleName().trim().equalsIgnoreCase("BackOffice") || LoginPresenter.myUserEntity.getRole().getRoleName().trim().equalsIgnoreCase("WareHouse")){		
		}else{
			setHeaderMenu(accountsHeader);
			flowPanel3.add(tableheader);
		}
		
		flowPanel3.add(accountTable.getTable());	
		tabPanel.add(flowPanel1, tabTitleBackoffice);
		tabPanel.add(flowPanel2, tabTitleWarehouse);
		tabPanel.add(flowPanel3, tabTitleAccounts);

		tabPanel.getTabBar().getElement().addClassName("popupCustomerLogTab");
		if(LoginPresenter.myUserEntity.getRole().getRoleName().trim().equalsIgnoreCase("WareHouse")){
			setLogCount(wareHouseTable.getDataprovider().getList());
			tabPanel.selectTab(1);
		}else if(LoginPresenter.myUserEntity.getRole().getRoleName().trim().equalsIgnoreCase("Account")){
			setLogCount(accountTable.getDataprovider().getList());
			tabPanel.selectTab(2);
		}else{
			setLogCount(customerLogTable.getDataprovider().getList());
			tabPanel.selectTab(0);
		}
		
		tabPanel.setSize("100%", "500px");
	
		setTableSelectionOnCustomerLogTable();
		setTableSelectionOnWareHouseTable();
		setTableSelectionOnAccountTable();
		createGui();
	
	}

	@Override
	public void toggleAppHeaderBarMenu() {
		// TODO Auto-generated method stub
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Search"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
	}


	@Override
	public void updateModel(CustomerLogDetails model) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateView(CustomerLogDetails model) {
		// TODO Auto-generated method stub
		
	}

	private void initalizeWidget(){
		personInfoComposite=AppUtility.customerInfoComposite(new Customer());
		personInfoComposite.getCustomerId().getHeaderLabel().setText("* Customer ID");
		personInfoComposite.getCustomerName().getHeaderLabel().setText("* Customer Name");
		personInfoComposite.getCustomerCell().getHeaderLabel().setText("* Customer Cell");
		dbfromDate = new DateBox();
		dbtoDate = new DateBox();
		tbtotalCallLog = new TextBox();
	    tbregisteredCallLog = new TextBox();
		tbpendingCallLog = new TextBox();
		tbcallBack = new TextBox();
		tbInstallation = new TextBox();
		tbBreakdown = new TextBox();
		tbPMS = new TextBox();
		tbDemo = new TextBox();
		btgo = new Button("GO");
		olbCompany = new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbCompany);
		tbmaterialIssuedCallLog = new TextBox();
		tbcompletedCallLog = new TextBox();
	}
	
	@Override
	public void setToNewState() {
		if(processLevelBar!=null)
		    processLevelBar.setVisibleFalse(true);
	}
	
	protected void setTableHeaderBar() {
		// TODO Auto-generated method stub
		
		for(int j=0;j<getTableheaderbarnames().length;j++)
		{
		    InlineLabel lbl;
			lbl=new InlineLabel(getTableheaderbarnames()[j]);
            getTableheader().add(lbl);
			lbl.getElement().setId("tableHeaderMenuCallLog");
			lbl.addClickHandler(this);
		}
		
	}
	public String[] getTableheaderbarnames() {
		return tableheaderbarnames;
	}

	public void setTableheaderbarnames(String[] tableheaderbarnames) {
		this.tableheaderbarnames = tableheaderbarnames;
	}

	public FlowPanel getTableheader() {
		return tableheader;
	}

	public void setTableheader(FlowPanel tableheader) {
		this.tableheader = tableheader;
	}

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		if(event.getSource() instanceof InlineLabel)
		  {
			InlineLabel lbl=(InlineLabel) event.getSource();
			String title=lbl.getText().trim();
			if (title.equals("New")) {				
//				customerLogPopup.showPopUp();
//				customerLogPopup.cbRepeatCall.setValue(false);
				
				customerLogDetailsPopup.showPopUp();
				customerLogDetailsPopup.setCustomerLogObj(new CustomerLogDetails());
				customerLogDetailsPopup.cbScheme.setValue(false);
				customerLogDetailsPopup.getTbSchemeAmount().setValue(0+"");
				customerLogDetailsPopup.getTbSchemeAmount().setEnabled(false);
				/**
				 * nidhi
				 * 30-07-2018
				 * for enble customer details
				 * 
				 */
				customerLogDetailsPopup.getPersonInfoComposite().setEnable(true);
			}
			if(title.equals("Edit Call")){
				if(customerLogDetails!=null){
					System.out.println("IN EDIT");
					popupFlag = true;
					System.out.println("customer Name" +customerLogDetails.getCinfo().getFullName());
					customerLogDetailsPopup.showPopUp();
					customerLogDetailsPopup.cbScheme.setValue(false);
					customerLogDetailsPopup.getTbSchemeAmount().setValue(0+"");
					customerLogDetailsPopup.getTbSchemeAmount().setEnabled(false);
					/**
					 * Date 15-6-2018by jayshree
					 * 
					 */
					//End by jayshree
					//false
					setPopupValueForView(customerLogDetails ,customerLogDetailsPopup);
					if(customerLogDetails.getStatus().equalsIgnoreCase(CustomerLogDetails.MATERIAISSUED) || customerLogDetails.getStatus().equalsIgnoreCase(CustomerLogDetails.ONHOLD) || customerLogDetails.getStatus().equalsIgnoreCase(CustomerLogDetails.COMPLETED)){
						if(customerLogDetails.getCallbackStatus() != null){
							if(customerLogDetails.getCallbackStatus().equalsIgnoreCase("IN")){
								getCustomerLogDetailsPopup().setEnable(true);
							}
							}else if(customerLogDetails.getVisitStatus()!=null){
								if(customerLogDetails.getVisitStatus().equalsIgnoreCase("Material Pending")){
									getCustomerLogDetailsPopup().setEnable(true);
								}
							   }else{
								getCustomerLogDetailsPopup().setEnable(false);
							}
					}else{
						getCustomerLogDetailsPopup().setEnable(true);
					}
					/**
					 * nidhi
					 * 30-07-2018
					 * for enble customer details
					 * 
					 */
					customerLogDetailsPopup.getPersonInfoComposite().setEnable(false);
					customerLogDetailsPopup.getLblCancel().setVisible(true);
					customerLogDetailsPopup.getLblOk().setVisible(true);
					customerLogDetailsPopup.setCustomerLogObj(customerLogDetails);
					customerLogDetails = null;
				}
				else{
					showDialogMessage("Please select atleast one record!");
				}			
		    }	
			if(title.equals("View Sales Order")){
				if(customerLogDetails!=null){
					System.out.println("IN SALES ORDER");
					viewSelectedDocument();
					customerLogDetails = null;
				}
				else{
					showDialogMessage("Please select atleast one record!");
				}
			}
			if(title.equals("View Call")){
				if(customerLogDetails!=null){
					System.out.println("VIEW CALL LOG");
					customerLogDetailsPopup.showPopUp();
					setPopupValueForView(customerLogDetails , customerLogDetailsPopup);
					getCustomerLogDetailsPopup().setEnable(false);
					customerLogDetailsPopup.getLblCancel().setVisible(false);
					customerLogDetailsPopup.getLblOk().setVisible(false);
					customerLogDetails = null;
				}
				else{
					showDialogMessage("Please select atleast one record!");
				}
			}
//			if(title.equals("View Document")){
//				if(customerLogDetails!=null){
//					reactOnViewDocument(customerLogDetails);
//					customerLogDetails = null;
//				}else{
//					showDialogMessage("Please select atleast one record!");
//				}
//				
//			}
			if(title.equals("create Bill")){
				if(customerLogDetails!=null){
					if(customerLogDetails.getStatus().equalsIgnoreCase(CustomerLogDetails.COMPLETED)){
					createInvoicePopup.showPopUp();
					//createInvoicePopup.getMaterialTable().setEnable(false);
					reactOnCreateInvoice(customerLogDetails);
					}else{
						showDialogMessage("Only applicable for Completed Call Logs ");
					}
					customerLogDetails = null;
				}else{
					showDialogMessage("Please select atleast one record!");
				}
			}
			if(title.equals("Reverse Status")){
				if(customerLogDetails!=null){
					customerLogDetails.setStatus(CustomerLogDetails.CREATED);
					genasync.save(customerLogDetails,
							new AsyncCallback<ReturnFromServer>() {
								@Override
								public void onFailure(Throwable caught) {
									// TODO Auto-generated method stub
									System.out.println("failed");
								}
								@Override
								public void onSuccess(ReturnFromServer result) {
									// TODO Auto-generated method stub
									customerLogDetails = null;
									showDialogMessage("Status of "+"call Log Id: "+ result.count+" has changed to created successfully!");
									System.out.println("success (reverse status)"
											+ result.count);
									retriveTable(getCustomerLogQuery(), getCustomerLogTable());
								}
							});
				}else{				
					showDialogMessage("Please select atleast one record!");
				}
			}
			if(title.equals("Cancel Call")){
				if(customerLogDetails!=null){
					customerLogDetails.setStatus(CustomerLogDetails.CANCELLED);
					genasync.save(customerLogDetails,
							new AsyncCallback<ReturnFromServer>() {
								@Override
								public void onFailure(Throwable caught) {
									// TODO Auto-generated method stub
									System.out.println("failed");
								}
								@Override
								public void onSuccess(ReturnFromServer result) {
									// TODO Auto-generated method stub
									customerLogDetails = null;
									showDialogMessage("Call Log Id: "+ result.count+" has cancelled successfully!");
									System.out.println("success (cancel)"
											+ result.count);
								}
							});
				}else{
					showDialogMessage("Please select atleast one record!");
				}
			}
			if(title.equals("BackOffice Refresh")){
				retriveTable(getCustomerLogQuery(), customerLogTable);
			}
			if(title.equals("WareHouse Refresh")){
				retriveTable(getCustomerLogQuery(), wareHouseTable);
			}
			if(title.equals("Account Refresh")){
				retriveTable(getCustomerLogQuery(), accountTable);
			}
		  }
	}
	private void setHeaderMenu(String[] str){
		tableheaderbarnames= str;
		tableheader= new FlowPanel();
		tableheader.getElement().setId("tableheader");
		setTableHeaderBar();	
		tableheader.getElement().getStyle().setMarginTop(10, Unit.PX);
		tableheader.getElement().getStyle().setMarginBottom(10, Unit.PX);
	}

	public CustomerLogPopup getCustomerLogPopup() {
		return customerLogPopup;
	}

	public void setCustomerLogPopup(CustomerLogPopup customerLogPopup) {
		this.customerLogPopup = customerLogPopup;
	}

	public CustomerLogTable getCustomerLogTable() {
		return customerLogTable;
	}

	public void setCustomerLogTable(CustomerLogTable customerLogTable) {
		this.customerLogTable = customerLogTable;
	}

	public WareHouseTable getWareHouseTable() {
		return wareHouseTable;
	}

	public void setWareHouseTable(WareHouseTable wareHouseTable) {
		this.wareHouseTable = wareHouseTable;
	}
	
	 public AccountTable getAccountTable() {
		return accountTable;
	}

	public void setAccountTable(AccountTable accountTable) {
		this.accountTable = accountTable;
	}

	public Button getBtgo() {
		return btgo;
	}

	public void setBtgo(Button btgo) {
		this.btgo = btgo;
	}

	public DateBox getDbfromDate() {
		return dbfromDate;
	}

	public void setDbfromDate(DateBox dbfromDate) {
		this.dbfromDate = dbfromDate;
	}

	public DateBox getDbtoDate() {
		return dbtoDate;
	}

	public void setDbtoDate(DateBox dbtoDate) {
		this.dbtoDate = dbtoDate;
	}

	public ObjectListBox<Branch> getOlbCompany() {
		return olbCompany;
	}

	public void setOlbCompany(ObjectListBox<Branch> olbCompany) {
		this.olbCompany = olbCompany;
	}
    
	public CustomerLogDetailsPopup getCustomerLogDetailsPopup() {
		return customerLogDetailsPopup;
	}

	public void setCustomerLogDetailsPopup(
		CustomerLogDetailsPopup customerLogDetailsPopup) {
		this.customerLogDetailsPopup = customerLogDetailsPopup;
	}

	public CustomerLogDetails getCustomerLogDetails() {
		return customerLogDetails;
	}

	public void setCustomerLogDetails(CustomerLogDetails customerLogDetails) {
		this.customerLogDetails = customerLogDetails;
	}

	public CreateInvoicePopup getCreateInvoicePopup() {
		return createInvoicePopup;
	}

	public TextBox getTbtotalCallLog() {
		return tbtotalCallLog;
	}

	public void setTbtotalCallLog(TextBox tbtotalCallLog) {
		this.tbtotalCallLog = tbtotalCallLog;
	}

	public TextBox getTbregisteredCallLog() {
		return tbregisteredCallLog;
	}

	public void setTbregisteredCallLog(TextBox tbregisteredCallLog) {
		this.tbregisteredCallLog = tbregisteredCallLog;
	}

	public TextBox getTbpendingCallLog() {
		return tbpendingCallLog;
	}

	public void setTbpendingCallLog(TextBox tbpendingCallLog) {
		this.tbpendingCallLog = tbpendingCallLog;
	}

	public TextBox getTbInstallation() {
		return tbInstallation;
	}

	public void setTbInstallation(TextBox tbInstallation) {
		this.tbInstallation = tbInstallation;
	}

	public TextBox getTbBreakdown() {
		return tbBreakdown;
	}

	public void setTbBreakdown(TextBox tbBreakdown) {
		this.tbBreakdown = tbBreakdown;
	}

	public TextBox getTbPMS() {
		return tbPMS;
	}

	public void setTbPMS(TextBox tbPMS) {
		this.tbPMS = tbPMS;
	}

	public TextBox getTbDemo() {
		return tbDemo;
	}

	public void setTbDemo(TextBox tbDemo) {
		this.tbDemo = tbDemo;
	}

	public TextBox getTbcallBack() {
		return tbcallBack;
	}

	public void setTbcallBack(TextBox tbcallBack) {
		this.tbcallBack = tbcallBack;
	}

	public void setCreateInvoicePopup(CreateInvoicePopup createInvoicePopup) {
		this.createInvoicePopup = createInvoicePopup;
	}

	public TextBox getTbmaterialIssuedCallLog() {
		return tbmaterialIssuedCallLog;
	}

	public void setTbmaterialIssuedCallLog(TextBox tbmaterialIssuedCallLog) {
		this.tbmaterialIssuedCallLog = tbmaterialIssuedCallLog;
	}

	public TextBox getTbcompletedCallLog() {
		return tbcompletedCallLog;
	}

	public void setTbcompletedCallLog(TextBox tbcompletedCallLog) {
		this.tbcompletedCallLog = tbcompletedCallLog;
	}

	public void setTableSelectionOnCustomerLogTable()
	 {
		final NoSelectionModel<CustomerLogDetails> selectionModelMyObj = new NoSelectionModel<CustomerLogDetails>();

		SelectionChangeEvent.Handler tableHandler = new SelectionChangeEvent.Handler() {
			@Override
			public void onSelectionChange(SelectionChangeEvent event) {
				CustomerLogDetails entity = selectionModelMyObj.getLastSelectedObject();
				customerLogDetails=entity;
				System.out.println("selected object :"+ customerLogDetails);
			}
		};
	    System.out.println("selected object :"+ selectionModelMyObj);
		selectionModelMyObj.addSelectionChangeHandler(tableHandler);
		customerLogTable.getTable().setSelectionModel(selectionModelMyObj);
		 
	 }
	public void setTableSelectionOnWareHouseTable()
	 {
		final NoSelectionModel<CustomerLogDetails> selectionModelMyObj = new NoSelectionModel<CustomerLogDetails>();

		SelectionChangeEvent.Handler tableHandler = new SelectionChangeEvent.Handler() {
			@Override
			public void onSelectionChange(SelectionChangeEvent event) {
				CustomerLogDetails entity = selectionModelMyObj.getLastSelectedObject();
				customerLogDetails=entity;
				System.out.println("selected object :"+ customerLogDetails);
			}
		};
	    System.out.println("selected object :"+ selectionModelMyObj);
		selectionModelMyObj.addSelectionChangeHandler(tableHandler);
		wareHouseTable.getTable().setSelectionModel(selectionModelMyObj);
		 
	 }
	public void setTableSelectionOnAccountTable()
	 {
		final NoSelectionModel<CustomerLogDetails> selectionModelMyObj = new NoSelectionModel<CustomerLogDetails>();

		SelectionChangeEvent.Handler tableHandler = new SelectionChangeEvent.Handler() {
			@Override
			public void onSelectionChange(SelectionChangeEvent event) {
				CustomerLogDetails entity = selectionModelMyObj.getLastSelectedObject();
				customerLogDetails=entity;
				System.out.println("selected object :"+ customerLogDetails);
			}
		};
	    System.out.println("selected object :"+ selectionModelMyObj);
		selectionModelMyObj.addSelectionChangeHandler(tableHandler);
		accountTable.getTable().setSelectionModel(selectionModelMyObj);
		 
	 }
    private void viewSelectedDocument()
	{
		AppMemory.getAppMemory().currentState = ScreeenState.VIEW;
	    AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Sales/Quick Sales Order",Screen.QUICKSALESORDER);
		final QuickSalesOrderForm form=QuickSalesOrderPresenter.initalize();
		final SalesOrder salesOrder = new SalesOrder();
		salesOrder.setCinfo(customerLogDetails.getCinfo());
		salesOrder.setEmployee(customerLogDetails.getTechnician());
		salesOrder.setSalesOrderDate(new Date());
		salesOrder.setApproverName(customerLogDetails.getUserId());
		salesOrder.setQuickSalesInvoiceDate(new Date());
		salesOrder.setSodeliveryDate(new Date());
		salesOrder.setRefNo(customerLogDetails.getCount()+"");
		salesOrder.setApproverName(LoginPresenter.loggedInUser);
		if(customerLogDetails.getAddress()!=null){
			salesOrder.setShippingAddress(customerLogDetails.getAddress());
		}
		salesOrder.setBranch(customerLogDetails.getCompanyName());
		form.showWaitSymbol();
		AppMemory.getAppMemory().stickPnel(form);
		Timer timer = new Timer() {
			@Override
			public void run() {
				form.hideWaitSymbol();
				form.updateView(salesOrder);
				form.setToEditState();
			}
		};
		timer.schedule(3000);
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		//super.setEnable(state);
		
	}
	public  void setPopupValueForView(CustomerLogDetails customerLogDetails , CustomerLogDetailsPopup customerLogDetailsPopup){
		
		/**
		 * Date 15-6-2018 by jayshree
		 */
		
		if(customerLogDetails.getCinfo() != null){
			customerLogDetailsPopup.getPersonInfoComposite().setValue(customerLogDetails.getCinfo());
		}
		//End
		if(customerLogDetails.getCreatedOn() != null){
			customerLogDetailsPopup.getDbCreatedOn().setValue(customerLogDetails.getCreatedOn());
		}
		if(customerLogDetails.getStatus() !=null){
			customerLogDetailsPopup.getTbStatus().setValue(customerLogDetails.getStatus());
		}
		if(customerLogDetails.getCinfo().getFullName() !=null){
			customerLogDetailsPopup.getTbCustomerName().setValue(customerLogDetails.getCinfo().getFullName());
		}
		if(customerLogDetails.getCinfo().getCellNumber() !=null){
			customerLogDetailsPopup.getTbCellNumber().setValue(customerLogDetails.getCinfo().getCellNumber()+"");
		}
		if(customerLogDetails.getCallType() !=null){
			customerLogDetailsPopup.getOlbCallType().setValue(customerLogDetails.getCallType());
		}
		if(customerLogDetails.getCompanyName() !=null){
			customerLogDetailsPopup.getOlbCompany().setValue(customerLogDetails.getCompanyName());
		}
		if(customerLogDetails.getTechnician() !=null){
			customerLogDetailsPopup.getOlbTechnician().setValue(customerLogDetails.getTechnician());
		}
		if(customerLogDetails.getWarrantyStatus() !=null){
			customerLogDetailsPopup.getOlbWarrantyStatus().setValue(customerLogDetails.getWarrantyStatus());
		}
		if(customerLogDetails.getCellNumber2() != null){
			customerLogDetailsPopup.getTbCellNumber2().setValue(customerLogDetails.getCellNumber2()+"");
		}
		if(customerLogDetails.getComplaintNumber() !=null){
			customerLogDetailsPopup.getTbComplaintNumber().setValue(customerLogDetails.getComplaintNumber()+"");
		}
		if(customerLogDetails.getProdSerialNumber() !=null){
			customerLogDetailsPopup.getTbSerialNo().setValue(customerLogDetails.getProdSerialNumber());
		}
		if(customerLogDetails.getAppointmentDate() !=null){
			customerLogDetailsPopup.getDbAppointmentDate().setValue(customerLogDetails.getAppointmentDate());
		}
		if(customerLogDetails.getAddress() !=null){
			customerLogDetailsPopup.getAddressComposite().setValue(customerLogDetails.getAddress());
		}
		if(customerLogDetails.getCallbackStatus() !=null){
			customerLogDetailsPopup.getOlbCallbackStatus().setValue(customerLogDetails.getCallbackStatus());
		}
		if(customerLogDetails.getVisitStatus() !=null){
			customerLogDetailsPopup.getOlbVisitStatus().setValue(customerLogDetails.getVisitStatus());
		}
		if(customerLogDetails.getProduct() !=null){
			customerLogDetailsPopup.getOlbProduct().setValue(customerLogDetails.getProduct());
		}
		if(customerLogDetails.getModelNo() !=null){
			customerLogDetailsPopup.getTbModelNo().setValue(customerLogDetails.getModelNo());
		}
		if(customerLogDetails.getRemark() !=null){
			customerLogDetailsPopup.getTaRemark().setValue(customerLogDetails.getRemark());
		}
		if(customerLogDetails.getServiceCharges().size()>0){
			MaterialRequired m = customerLogDetails.getServiceCharges().get(0);
			customerLogDetailsPopup.getProductInfoComposite().getProdID().setValue(m.getProductId()+"");
			customerLogDetailsPopup.getProductInfoComposite().getProdName().setValue(m.getProductName()+"");
			customerLogDetailsPopup.getProductInfoComposite().getProdCode().setValue(m.getProductCode()+"");
			customerLogDetailsPopup.getProductInfoComposite().getProdCategory().setValue(m.getProductCategory());
			customerLogDetailsPopup.getProductInfoComposite().getProdPrice().setValue(m.getProductPrice());
			customerLogDetailsPopup.getProductInfoComposite().getUnitOfmeasuerment().setValue(m.getUnitOfMeasurement());
		}
		if(customerLogDetails.getSchemeAmount()!=0){
			customerLogDetailsPopup.getTbSchemeAmount().setValue(customerLogDetails.getSchemeAmount()+"");
		}
		if(customerLogDetails.isSchemeFlag()){
			customerLogDetailsPopup.getCbScheme().setValue(true);
		}
		/** date 6.7.2018 added by komal for closing date and resolution**/
		if(customerLogDetails.getClosingDate()!=null){
			customerLogDetailsPopup.getDbClosingDate().setValue(customerLogDetails.getClosingDate());
		}
		if(customerLogDetails.getResolution()!=null){
			customerLogDetailsPopup.getTaResolution().setValue(customerLogDetails.getResolution());
		}
	}
//	private void reactOnViewDocument(CustomerLogDetails customerLogDetails) {
//		invoicePaymentPopup.companyId = customerLogDetails.getCompanyId();
//		invoicePaymentPopup.OrderId = customerLogDetails.getCount();
//		invoicePaymentPanel = new PopupPanel();
//		invoicePaymentPanel.add(invoicePaymentPopup);
//		invoicePaymentPanel.center();
//		invoicePaymentPanel.show();
//	}
	private void reactOnCreateInvoice(CustomerLogDetails customerLogDetails){		
		this.createInvoicePopup.getMaterialTable().setEnable(false);
		this.createInvoicePopup.getCustomerTable().setEnable(false);
		this.createInvoicePopup.getServiceCharegesTable().connectToLocal();
		this.createInvoicePopup.getCustomerTable().connectToLocal();
		this.createInvoicePopup.getMaterialTable().clear();
		createInvoicePopup.getTbAmountReceived().setValue(0+"");
		createInvoicePopup.getTbDiscountAmount().setValue(0+"");
		createInvoicePopup.getTbBalanceAmount().setValue(0+"");
		createInvoicePopup.getTbComplaintNumber().setValue(customerLogDetails.getComplaintNumber());
		createInvoicePopup.getTbLogId().setValue(customerLogDetails.getCount()+"");
		createInvoicePopup.getTbCompanyName().setValue(customerLogDetails.getCompanyName());
		createInvoicePopup.getTbWarrantyStatus().setValue(customerLogDetails.getWarrantyStatus());
		createInvoicePopup.setCustomerLogObj(customerLogDetails);
		if(customerLogDetails.getServiceCharges().size()>0){
			ArrayList<MaterialRequired> list = new ArrayList<MaterialRequired>();
			for(MaterialRequired m : customerLogDetails.getServiceCharges()){
				ServiceProductTable s = new ServiceProductTable();
				s.calculateTotalAmount(m);
				list.add(m);
			}
			createInvoicePopup.getServiceCharegesTable().getDataprovider().setList(list);
		}
		if(customerLogDetails.getBookNo()!=null){
			createInvoicePopup.getTbBookNo().setValue(customerLogDetails.getBookNo());
		}
		if(customerLogDetails.getTcrNo()!=null){
			createInvoicePopup.getTbTCRNo().setValue(customerLogDetails.getTcrNo());
		}
 
		if(customerLogDetails.getCompanyCost().size()>0){
			createInvoicePopup.getMaterialTable().getDataprovider().getList().addAll(customerLogDetails.getCompanyCost());
		}
		if(customerLogDetails.getCustomerCost().size()>0){
			createInvoicePopup.getCustomerTable().getDataprovider().getList().addAll(customerLogDetails.getCustomerCost());
		}
		if(customerLogDetails.getPaymentMethod()!=null){
			createInvoicePopup.getOlbPaymentMethods().setValue(customerLogDetails.getPaymentMethod());
		}
		if(customerLogDetails.getTcrNo()!=null){
			createInvoicePopup.getTbTCRNo().setValue(customerLogDetails.getTcrNo());
		}
		if(customerLogDetails.getBookNo()!=null){
			createInvoicePopup.getTbBookNo().setValue(customerLogDetails.getBookNo());
		}
		if(customerLogDetails.getWarrantyStatus().equalsIgnoreCase("Warranty") && customerLogDetails.getCallType().equalsIgnoreCase("Installation")){
			if(customerLogDetails.isSchemeFlag()){
				createInvoicePopup.getTbTotalAmount().setValue(customerLogDetails.getSchemeAmount()+"");
				createInvoicePopup.getTbNetPayable().setValue(customerLogDetails.getSchemeAmount()+"");
			}else{
				createInvoicePopup.calcaulateTotalAmount();
			}
		}
		if(customerLogDetails.getPaymentInfo()!=null){
			createInvoicePopup.getTbbankAccNo().setValue(customerLogDetails.getPaymentInfo().getBankAccNo());;
			createInvoicePopup.getTbbankBranch().setValue(customerLogDetails.getPaymentInfo().getBankBranch());
			createInvoicePopup.getTbbankName().setValue(customerLogDetails.getPaymentInfo().getBankName());
			createInvoicePopup.getDbChequeDate().setValue(customerLogDetails.getPaymentInfo().getChequeDate());
			createInvoicePopup.getDbtransferDate().setValue(customerLogDetails.getPaymentInfo().getAmountTransferDate());
			createInvoicePopup.getTbChequeIssuedBy().setValue(customerLogDetails.getPaymentInfo().getChequeIssuedBy());
			createInvoicePopup.getTbreferenceno().setValue(customerLogDetails.getPaymentInfo().getReferenceNo());
			createInvoicePopup.getTbchequeNo().setValue(customerLogDetails.getPaymentInfo().getChequeNo());
			if(customerLogDetails.getPaymentInfo().getPaymentAmt()!= 0){
				createInvoicePopup.getTbTotalAmount().setValue(customerLogDetails.getPaymentInfo().getPaymentAmt()+"");
			}
			else{
				createInvoicePopup.calcaulateTotalAmount();
			}
			if(customerLogDetails.getPaymentInfo().getNetPay() !=0){
				createInvoicePopup.getTbNetPayable().setValue(customerLogDetails.getPaymentInfo().getNetPay()+"");
			}
			else{
				if(customerLogDetails.getWarrantyStatus().equalsIgnoreCase("Warranty") && customerLogDetails.getCallType().equalsIgnoreCase("Installation")){
					if(customerLogDetails.isSchemeFlag()){
						createInvoicePopup.getTbTotalAmount().setValue(nf.format(customerLogDetails.getSchemeAmount())+"");
						createInvoicePopup.getTbNetPayable().setValue(nf.format(customerLogDetails.getSchemeAmount())+"");
					}
				}else{
				createInvoicePopup.getTbNetPayable().setValue(customerLogDetails.getPaymentInfo().getPaymentAmt()+"");
				}
			}
			if(customerLogDetails.getPaymentInfo().getDiscountAmount() != 0){
				createInvoicePopup.getTbDiscountAmount().setValue(customerLogDetails.getPaymentInfo().getDiscountAmount()+"");
			}else{
				
				createInvoicePopup.getTbDiscountAmount().setValue(0+"");
			}
			if(customerLogDetails.getPaymentInfo().getPaymentReceived() != 0){
				createInvoicePopup.getTbAmountReceived().setValue(customerLogDetails.getPaymentInfo().getPaymentReceived()+"");
			}
			else{
				createInvoicePopup.getTbAmountReceived().setValue(0+"");
			}
		}
	//		createInvoicePopup.setCustomerLogObj(customerLogDetails);
	}
//	protected void calcaulateTotalAmount(){
//		List<MaterialRequired> productList = new ArrayList<MaterialRequired>();
//		  if(createInvoicePopup.getMaterialTable().getDataprovider().getList().size() != 0){
//			  productList.addAll(createInvoicePopup.getMaterialTable().getDataprovider().getList());
//		  }
//		  if(createInvoicePopup.getCustomerTable().getDataprovider().getList().size() != 0){
//			  productList.addAll(createInvoicePopup.getCustomerTable().getDataprovider().getList());
//		  }
//		  System.out.println("prod size:" + productList.size());
//		  if(createInvoicePopup.getServiceCharegesTable().getDataprovider().getList().size() != 0){
//			  productList.addAll(createInvoicePopup.getServiceCharegesTable().getDataprovider().getList());
//		  }
//		  double customerTotal = 0.0d;
//		  double companyTotal = 0.0d;
//		  for(MaterialRequired m :productList){
//		  if((createInvoicePopup.getTbWarrantyStatus().getValue().equalsIgnoreCase("Warranty") || createInvoicePopup.getTbWarrantyStatus().getValue().equalsIgnoreCase("AMC")) && m.getProductPayableQuantity()!=0){
//			  customerTotal = customerTotal + m.getTotalAmount(); 
//		  }
//		  if(createInvoicePopup.getTbWarrantyStatus().getValue().equalsIgnoreCase("Warranty") || createInvoicePopup.getTbWarrantyStatus().getValue().equalsIgnoreCase("AMC")){
//			  companyTotal = companyTotal + m.getTotalAmount();  
//		  }
//		  if(createInvoicePopup.getTbWarrantyStatus().getValue().equalsIgnoreCase("Out Of Warranty")){		
//			  customerTotal = customerTotal + m.getTotalAmount(); 
//		  }
//	   }
//		  showDialogMessage("customer total : "+customerTotal );
//		  createInvoicePopup.getTbTotalAmount().setValue(nf.format(customerTotal)+"");
//		  createInvoicePopup.getTbNetPayable().setValue(nf.format(customerTotal)+"");
//		  createInvoicePopup.getTbAmountReceived().setValue(nf.format(customerTotal)+"");
//	}
	protected void setLogCount(List<CustomerLogDetails> list){
		System.out.println("list size :" +  list.size());
		ArrayList<Integer> countList = new ArrayList<Integer>();
			for (int i = 0; i <= 9; i++) {
				countList.add(0);
			}
			for(CustomerLogDetails c : list){
				countList.set(0 ,countList.get(0)+1);
				if(c.getStatus().equalsIgnoreCase(CustomerLogDetails.CREATED)){
					countList.set(1 ,countList.get(1)+1);
				}
				if(c.getStatus().equalsIgnoreCase(CustomerLogDetails.MATERIALPENDNG)){
					countList.set(2 ,countList.get(2)+1);
				}
				if(c.getCallbackStatus() != null){
				if(c.getCallbackStatus().equalsIgnoreCase("IN") || c.getCallbackStatus().equalsIgnoreCase("OUT")){
					countList.set(3 ,countList.get(3)+1);
				}
				}
				if(c.getCallType().equalsIgnoreCase("Installation")){
					countList.set(4 ,countList.get(4)+1);
				}
				if(c.getCallType().equalsIgnoreCase("Demo")){
					countList.set(5 ,countList.get(5)+1);
				}
				if(c.getCallType().equalsIgnoreCase("PMS")){
					countList.set(6 ,countList.get(6)+1);
				}
				if(c.getCallType().equalsIgnoreCase("BreakDown")){
					countList.set(7 ,countList.get(7)+1);
				}
				if(c.getStatus().equalsIgnoreCase(CustomerLogDetails.MATERIAISSUED)){
					countList.set(8 ,countList.get(8)+1);
				}
				if(c.getStatus().equalsIgnoreCase(CustomerLogDetails.COMPLETED)){
					countList.set(9 ,countList.get(9)+1);
				}
			}
			getTbtotalCallLog().setValue(countList.get(0) + "");
			getTbregisteredCallLog().setValue(countList.get(1) + "");
			getTbpendingCallLog().setValue(countList.get(2) + "");
			getTbcallBack().setValue(countList.get(3) + "");
			getTbInstallation().setValue(countList.get(4) + "");
			getTbDemo().setValue(countList.get(5) + "");
			getTbPMS().setValue(countList.get(6) + "");
			getTbBreakdown().setValue(countList.get(7) + "");
			getTbmaterialIssuedCallLog().setValue(countList.get(8)+"");
			getTbcompletedCallLog().setValue(countList.get(9)+"");
		}
	 public <T> void retriveTable(MyQuerry querry, final SuperTable<T> table) {
		customerLogTable.connectToLocal();
		customerLogTable.getDataprovider().getList().clear();
		wareHouseTable.connectToLocal();
		wareHouseTable.getDataprovider().getList().clear();
		accountTable.connectToLocal();
		accountTable.getDataprovider().getList().clear();
		genasync.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {
					@SuppressWarnings("unchecked")
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						for (SuperModel model : result) {
							CustomerLogDetails c = (CustomerLogDetails) model;
							customerLogTable.getListDataProvider().getList().add(c);
							if(c.getWarrantyStatus().equalsIgnoreCase("Warranty") && c.getCallType().equalsIgnoreCase("Installation")){
								wareHouseTable.getListDataProvider().getList().add(c);
							}else if(c.getStatus().equalsIgnoreCase(CustomerLogDetails.MATERIAISSUED) || c.getStatus().equalsIgnoreCase(CustomerLogDetails.MATERIALPENDNG) || c.getWarrantyStatus().equalsIgnoreCase("Out Of Warranty")){
								wareHouseTable.getListDataProvider().getList().add(c);
							}
							if(c.getWarrantyStatus().equalsIgnoreCase("Warranty") && c.getCallType().equalsIgnoreCase("Installation")){
								accountTable.getListDataProvider().getList().add(c);
							}else if (c.getStatus().equalsIgnoreCase(CustomerLogDetails.COMPLETED)|| c.getWarrantyStatus().equalsIgnoreCase("Out Of Warranty")) {
								accountTable.getListDataProvider().getList().add(c);								
							}

						}
						Comparator<SuperModel> comp = new Comparator<SuperModel>() {
							@Override
							public int compare(SuperModel arg0, SuperModel arg1) {
								// TODO Auto-generated method stub
								if (arg0.getCount() == arg1.getCount()) {
									return 0;
								}
								if (arg0.getCount() > arg1.getCount()) {
									return -1;
								} else {
									return 1;
								}
							}

						};
						Collections.sort(customerLogTable.getListDataProvider().getList(), comp);
						Collections.sort(wareHouseTable.getListDataProvider().getList(), comp);
						Collections.sort(accountTable.getListDataProvider().getList(), comp);
						
						//if (table instanceof CustomerLogTable) {
							if (LoginPresenter.myUserEntity.getRole().getRoleName().trim().equalsIgnoreCase("BackOffice") || LoginPresenter.myUserEntity.getRole().getRoleName().trim().equalsIgnoreCase("ADMIN")) {
								setLogCount(customerLogTable.getDataprovider().getList());
							}

						//}
						//if (table instanceof WareHouseTable) {
							if (LoginPresenter.myUserEntity.getRole().getRoleName().trim().equalsIgnoreCase("WareHouse")) {
								setLogCount(wareHouseTable.getDataprovider().getList());
							}

						//}
						//if (table instanceof AccountTable) {
							if (LoginPresenter.myUserEntity.getRole().getRoleName().trim().equalsIgnoreCase("Account")) {
								setLogCount(accountTable.getDataprovider().getList());
							}
				//		}
					}

					@Override
					public void onFailure(Throwable caught) {
						caught.printStackTrace();

					}
				});
	}
	 protected MyQuerry getCustomerLogQuery(){
			List<String> statusList = new ArrayList<String>();
			statusList.add(CustomerLogDetails.CREATED);
			statusList.add(CustomerLogDetails.MATERIAISSUED);
			statusList.add(CustomerLogDetails.MATERIALPENDNG);
			statusList.add(CustomerLogDetails.ONHOLD);
			statusList.add(CustomerLogDetails.COMPLETED);
			Filter filter = null;
			Company c = new Company();
			MyQuerry querry;
			Vector<Filter> filtervec = new Vector<Filter>();
			filter = new Filter();
			filter.setQuerryString("companyId");
			filter.setLongValue(c.getCompanyId());
			filtervec.add(filter);
			filter = new Filter();
			filter.setQuerryString("status IN");
			filter.setList(statusList);
			filtervec.add(filter);
			if(!(LoginPresenter.myUserEntity.getRole().getRoleName().trim().equalsIgnoreCase("ADMIN"))){
				List<String> branchList = new ArrayList<String>();
				for(Branch b : LoginPresenter.globalBranch){
					branchList.add(b.getBusinessUnitName());
				}
				filter = new Filter();
				filter.setQuerryString("companyName IN");
				filter.setList(branchList);
				filtervec.add(filter);
			}
			querry = new MyQuerry();
			querry.setQuerryObject(new CustomerLogDetails());
			querry.setFilters(filtervec);
			return querry;

		}
}
