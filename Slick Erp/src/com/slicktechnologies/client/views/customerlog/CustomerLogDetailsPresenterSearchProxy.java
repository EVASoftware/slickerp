package com.slicktechnologies.client.views.customerlog;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.customerlog.CustomerLogDetailsPresenter.CustomerLogDetailsPresenterSearch;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerlog.CustomerLogDetails;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.personlayer.Employee;

public class CustomerLogDetailsPresenterSearchProxy extends CustomerLogDetailsPresenterSearch {

	PersonInfoComposite personInfo;
	//DateComparator dateComparator;
	//ObjectListBox<Customer> olbCompany;
	DateBox dbfromDate;
    DateBox dbtoDate;
    DateBox dbclosingfromDate;
    DateBox dbclosingtoDate;
	ObjectListBox<Branch> olbCompany;
	ObjectListBox<ConfigCategory> olbWarrantyStatus;
	ObjectListBox<Employee> olbTechnician;
	ObjectListBox<ConfigCategory> olbCallType;
	ObjectListBox<ConfigCategory> olbStatus;
	TextBox tbComplaintNumber;
	TextBox tbSerialNumber;
	IntegerBox tbCallId;

	public CustomerLogDetailsPresenterSearchProxy()
	{
		super();
		createGui();
		System.out.println("in side sdjklamasdsfdds");
	}
	public void initWidget()
	{
		System.out.println("inside initialize methods");
		
		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new Customer());
		personInfo=new PersonInfoComposite(querry,false,true,false);//komal
		//dateComparator= new DateComparator(new CustomerLogDetails());
		dbfromDate = new DateBox();
		dbtoDate = new DateBox();
		dbclosingfromDate = new DateBox();
		dbclosingtoDate = new DateBox();
		personInfo.getCustomerId().getHeaderLabel().setText("Customer ID");
		personInfo.getCustomerName().getHeaderLabel().setText("Customer Name");
		personInfo.getCustomerCell().getHeaderLabel().setText("Customer Cell");
//		olbCompany = new ObjectListBox<Customer>();
//		AppUtility.makeCompanyNameListBoxLive(olbCompany);
	    olbCompany = new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbCompany);
		olbWarrantyStatus = new ObjectListBox<ConfigCategory>(); 
		AppUtility.MakeLiveConfig(olbWarrantyStatus,Screen.WARRANTYSTATUS);
		olbTechnician = new ObjectListBox<Employee>();
		olbTechnician.makeEmployeeLive(AppConstants.SALESMODULE, "Customer Call Log Details", "Technician");
        olbCallType = new ObjectListBox<ConfigCategory>();
        AppUtility.MakeLiveConfig(olbCallType,Screen.CALLTYPE);
        olbStatus = new ObjectListBox<ConfigCategory>();
        AppUtility.MakeLiveConfig(olbStatus, Screen.CALLLOGSTATUS);
        tbComplaintNumber = new TextBox();
        tbSerialNumber = new TextBox();
        tbCallId = new IntegerBox();
	}	
	public void createScreen()
	{
		initWidget();
		FormFieldBuilder builder;
		builder = new FormFieldBuilder("",personInfo);
		FormField fpersonInfo= builder.setMandatory(false).setRowSpan(0).setColSpan(5).build();
		builder = new FormFieldBuilder("From Date(Closing)", dbclosingfromDate);
		FormField fclosingfromDate= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("To Date(Closing)",dbclosingtoDate);
		FormField fclosingToDate= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("From Date", dbfromDate );
		FormField fdbfromDate = builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("To Date", dbtoDate);
		FormField fdbtoDate = builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		builder = new FormFieldBuilder("* Company", olbCompany);
		FormField folbCompany = builder.setMandatory(true).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Warranty Status", olbWarrantyStatus);
		FormField folbWarrantyStatus = builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Call Type", olbCallType);
		FormField folbCallType = builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Technician", olbTechnician);
		FormField folbTechnician = builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Call Log Status", olbStatus);
		FormField folbStatus = builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Serial No", tbSerialNumber);
		FormField ftbSerialNumber = builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Complaint Number", tbComplaintNumber);
		FormField ftbComplaintNumber = builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("CallLog Id", tbCallId);
		FormField ftbCallId = builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		this.fields=new FormField[][]{
				{folbCompany,folbStatus,fdbfromDate,fdbtoDate},
				{fclosingfromDate ,fclosingToDate },
				{folbWarrantyStatus ,folbCallType , folbTechnician  , ftbComplaintNumber},
				{ftbSerialNumber ,ftbCallId },
				{fpersonInfo},
		};
	}
	
	@Override
	public MyQuerry getQuerry()
	{
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		Company c = new Company();
		temp = new Filter();
		temp.setQuerryString("companyId");
		temp.setLongValue(c.getCompanyId());
		filtervec.add(temp);

		if (dbfromDate.getValue() != null) {
			temp = new Filter();
			temp.setQuerryString("createdOn >=");
			temp.setDateValue(dbfromDate.getValue());
			filtervec.add(temp);
		}

		if (dbtoDate.getValue() != null) {
			temp = new Filter();
			temp.setQuerryString("createdOn <=");
			temp.setDateValue(dbtoDate.getValue());
			filtervec.add(temp);
		}	
		if (dbclosingfromDate.getValue() != null) {
			temp = new Filter();
			temp.setQuerryString("closingDate >=");
			temp.setDateValue(dbclosingfromDate.getValue());
			filtervec.add(temp);
		}

		if (dbclosingtoDate.getValue() != null) {
			temp = new Filter();
			temp.setQuerryString("closingDate <=");
			temp.setDateValue(dbclosingtoDate.getValue());
			filtervec.add(temp);
		}	

		if(olbCompany.getSelectedIndex()!=0){
			temp = new Filter();
			temp.setQuerryString("companyName");
			temp.setStringValue(olbCompany.getValue());
			filtervec.add(temp);
		}
		if(olbWarrantyStatus.getSelectedIndex()!=0){
			temp = new Filter();
			temp.setQuerryString("warrantyStatus");
			temp.setStringValue(olbWarrantyStatus.getValue());
			filtervec.add(temp);
			}
		if(olbCallType.getSelectedIndex()!=0){
			temp = new Filter();
			temp.setQuerryString("callType");
			temp.setStringValue(olbCallType.getValue());
			filtervec.add(temp);
			}
		if(olbTechnician.getSelectedIndex()!=0){
			temp = new Filter();
			temp.setQuerryString("technician");
			temp.setStringValue(olbTechnician.getValue());
			filtervec.add(temp);
			}
		if(olbStatus.getSelectedIndex()!=0){
			temp = new Filter();
			temp.setQuerryString("status");
			temp.setStringValue(olbStatus.getValue().trim());
			filtervec.add(temp);
		}
		if(personInfo.getIdValue()!=-1)
		{
			temp=new Filter();
			temp.setIntValue(personInfo.getIdValue());
			temp.setQuerryString("cinfo.count");
			filtervec.add(temp);
		}

		if(!(personInfo.getFullNameValue().equals("")))
		{
			temp=new Filter();
			temp.setStringValue(personInfo.getFullNameValue());
			temp.setQuerryString("cinfo.fullName");
			filtervec.add(temp);
		}
		if(personInfo.getCellValue()!=-1l)
		{
			temp=new Filter();
			temp.setLongValue(personInfo.getCellValue());
			temp.setQuerryString("cinfo.cellNumber");
			filtervec.add(temp);
		}
		if(!tbSerialNumber.getValue().equals("")){
			temp=new Filter();
			temp.setStringValue(tbSerialNumber.getValue());
			temp.setQuerryString("prodSerialNumber");
			filtervec.add(temp);
		}
		if(!tbComplaintNumber.getValue().equals("")){
			temp=new Filter();
			temp.setStringValue(tbComplaintNumber.getValue());
			temp.setQuerryString("complaintNumber");
			filtervec.add(temp);
		}
		if(tbCallId.getValue()!=null){
			temp=new Filter();
			temp.setIntValue(tbCallId.getValue());
			temp.setQuerryString("count");
			filtervec.add(temp);

		}
		MyQuerry querry= new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new CustomerLogDetails());
		return querry;
	}
	@Override
	public boolean validate() {
		if(LoginPresenter.branchRestrictionFlag)
		{
		if(olbCompany.getSelectedIndex()==0)
		{
			showDialogMessage("Select Branch");
			return false;
		}
		}
		return true;

	}
	
	
	

}
