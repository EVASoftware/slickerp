package com.slicktechnologies.client.views.customerlog;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreen;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.helperlayer.LoggedIn;
import com.slicktechnologies.shared.common.inventory.MaterialMovementNote;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class DefectiveMaterialForm extends TableScreen<MaterialMovementNote> {
		
		public DefectiveMaterialForm (SuperTable<MaterialMovementNote> superTable) {
			super(superTable);
			createGui();
			
		}

		@Override
		public void createScreen(){
			this.processlevelBarNames = new String []{"Send Defective"};		
		}
		
		@Override
		public void toggleAppHeaderBarMenu() {
			if(superTable!=null&&superTable.getListDataProvider().getList()!=null){
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.contains("Rows:")){
						menus[k].setText("Rows:"+superTable.getListDataProvider().getList().size());	
						menus[k].getElement().setId("addlabel2");
					}
				}
			}
			if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.equals("Search")||text.contains("Rows:"))
					{
						menus[k].setVisible(true); 
					}
					else
						menus[k].setVisible(false);  		  		
				}
			}
			
			else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.equals("Search")||text.contains("Rows:"))
						menus[k].setVisible(true); 
					
					else
						menus[k].setVisible(false);  		   
				}
			}
			
			else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.equals("Search")||text.contains("Rows:"))
							menus[k].setVisible(true); 
					
					else
						menus[k].setVisible(false);  		   
				}
			}
			AuthorizationHelper.setAsPerAuthorization(Screen.DEFECTIVEMATERIALLIST,LoginPresenter.currentModule.trim());		
		}

		@Override
		public void retriveTable(MyQuerry querry) {
			superTable.connectToLocal();
			service.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					for(SuperModel model:result){
						superTable.getListDataProvider().getList().add((MaterialMovementNote) model);
					}
					System.out.println("size :" +superTable.getListDataProvider().getList().size());
//					superTable.getTable().setVisibleRange(0,result.size());
				}
				
				@Override
				public void onFailure(Throwable caught) {
				caught.printStackTrace();
					
				}
			});
		}

		@Override
		public void updateModel(MaterialMovementNote model) {
			super.updateModel(model);
		}

		@Override
		public void updateView(MaterialMovementNote view) {
			super.updateView(view);
		}

}
