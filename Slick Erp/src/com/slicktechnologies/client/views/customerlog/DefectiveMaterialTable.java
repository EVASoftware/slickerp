package com.slicktechnologies.client.views.customerlog;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.ValueUpdater;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.Header;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.PopupPanel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseRequisition;
import com.slicktechnologies.shared.common.contractcancel.CancelContract;
import com.slicktechnologies.shared.common.customerlog.CustomerLogDetails;
import com.slicktechnologies.shared.common.inventory.GRN;
import com.slicktechnologies.shared.common.inventory.MaterialMovementNote;
import com.slicktechnologies.shared.common.inventory.MaterialProduct;
import com.slicktechnologies.shared.common.inventory.ProductInventoryTransaction;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

public class DefectiveMaterialTable extends SuperTable<MaterialMovementNote> {
	GeneralServiceAsync service = GWT.create(GeneralService.class);
	Column<MaterialMovementNote, Boolean> checkColumn;
	TextColumn<MaterialMovementNote> getColumnMmnId;
	TextColumn<MaterialMovementNote> getColumnMmnDate;
	TextColumn<MaterialMovementNote> getColumnMmnTitle;
	TextColumn<MaterialMovementNote> getColumnMmnStatus;
	TextColumn<MaterialMovementNote> getColumnMmnTransaction;
	TextColumn<MaterialMovementNote> getColumnMaterialStatus;
	TextColumn<MaterialMovementNote> getColumnGRNDate;
	TextColumn<MaterialMovementNote> getColumnGRNId;
	TextColumn<MaterialMovementNote> getColumnMmnBranch;
	Column<MaterialMovementNote, String> getColumnViewGRNButton;
	Header<Boolean> selectAllHeader;
	GeneralViewDocumentPopup genralPopup;
	public static PopupPanel generalPanel;
	
	public DefectiveMaterialTable() {
		super();
		setHeight("1600px");
	}

	@Override
	public void createTable() {
		addCheckBoxCell();
		addColumnMmnTitle();
		addColumngetViewGRNButton();
		addColumnMmnId();
		addColumnMmnDate();
		
		addColumnMmnBranch();
		addColumnMaterialStatus();
		//addColumnMmnTransaction();
		addColumnGRNId();
		addColumnGRNDate();
		//addColumnMmnStatus();
		addFieldUpdater();
		addColumnSorting();
	}	
	public void addColumnSorting() {
		addSortinggetColumnMmnTitle();
		addSortinggetColumnMmnId();
		addSortinggetColumnMmnDate();
		addSortinggetColumnMmnBranch();
		addSortinggetColumnMaterialStatus();
		//addSortingMmnTransaction();
		addSortinggetColumnGRNId();
		addSortinggetColumnGRNDate();
		//addSortinggetColumnMmnStatus();
	}
	 private void addColumngetViewGRNButton() {
			ButtonCell btnCell = new ButtonCell();
			getColumnViewGRNButton = new Column<MaterialMovementNote, String>(btnCell) {
				@Override
				public String getValue(MaterialMovementNote object) {
					// TODO Auto-generated method stub
				if(isCredit(object.getSubProductTableMmn())==false){
					return "View GRN";
				}
				   return "NA";
			  }
			};
			table.addColumn(getColumnViewGRNButton, "View GRN");
			table.setColumnWidth(getColumnViewGRNButton, 90, Unit.PX);
		}

		private void updateColumngetMaterialIssueButton() {
			getColumnViewGRNButton.setFieldUpdater(new FieldUpdater<MaterialMovementNote, String>() {
				@Override
				public void update(int index, MaterialMovementNote object, String value) {
						System.out.println("Object onclick event :"+ object.getMmnWoId());
						if(object.getMmnWoId() !=0){
						service.getGRNForDefective(object.getMmnWoId(), object.getCompanyId(), new AsyncCallback<GRN>() {
							@Override
							public void onSuccess(GRN result) {
								// TODO Auto-generated method stub
								AppMemory.getAppMemory().currentScreen = Screen.GRN;
								genralPopup = new GeneralViewDocumentPopup();
								genralPopup.setModel(AppConstants.GRN,result);
								generalPanel = new PopupPanel();
								generalPanel.add(genralPopup);
								generalPanel.show();
								generalPanel.center();
							}						
							@Override
							public void onFailure(Throwable caught) {
								// TODO Auto-generated method stub
							
							}
						});	
				  }else{
					  GWTCAlert alert = new GWTCAlert();
					  alert.alert("Not Applicable");
				}
					
				}
			});
		}
	private void addColumnGRNDate(){	
		getColumnGRNDate = new TextColumn<MaterialMovementNote>() {
			@Override
			public String getValue(MaterialMovementNote object) {
				if (object.getMmnWoDate()!= null) {
					return AppUtility.parseDate(object.getMmnWoDate())+"";
				}
				return "";
			}
		};
		table.addColumn(getColumnGRNDate, "Defective Release Date");
		table.setColumnWidth(getColumnGRNDate,120,Unit.PX);
		getColumnGRNDate.setSortable(true);

	}
	
	private void addColumnGRNId(){	
		getColumnGRNId = new TextColumn<MaterialMovementNote>() {
			@Override
			public String getValue(MaterialMovementNote object) {
				if (object.getMmnWoId()!= 0) {
					return object.getMmnWoId()+"";
				}
				return "";
			}
		};
		table.addColumn(getColumnGRNId, "GRN Id");
		table.setColumnWidth(getColumnGRNId,120,Unit.PX);
		getColumnGRNId.setSortable(true);

	}

	private void addColumnMaterialStatus(){
		getColumnMaterialStatus= new TextColumn<MaterialMovementNote>() {
			@Override
			public String getValue(MaterialMovementNote object) {
				if(object.getMmnGRNStatus()!=null){
					return object.getMmnGRNStatus();
				}	
				return "";
			}
		};
		table.addColumn(getColumnMaterialStatus, "Material Status");
		table.setColumnWidth(getColumnMaterialStatus,100,Unit.PX);
		getColumnMaterialStatus.setSortable(true);
	}
	private void addColumnMmnBranch(){
		getColumnMmnBranch= new TextColumn<MaterialMovementNote>() {
			@Override
			public String getValue(MaterialMovementNote object) {
				if(object.getBranch()!=null){
					return object.getBranch();
				}else{
					return "";
				}
			
			}
		};
		table.addColumn(getColumnMmnBranch, "Company Name");
		table.setColumnWidth(getColumnMmnBranch,100,Unit.PX);
		getColumnMmnBranch.setSortable(true);
	}
	private void addColumnMmnTransaction() {
		getColumnMmnTransaction= new TextColumn<MaterialMovementNote>() {

			@Override
			public String getValue(MaterialMovementNote object) {
				return object.getTransDirection();
			}
		};
		table.addColumn(getColumnMmnTransaction, "Type");
		table.setColumnWidth(getColumnMmnTransaction,100,Unit.PX);
		getColumnMmnTransaction.setSortable(true);
	}
	private void addCheckBoxCell() {
		
		checkColumn=new Column<MaterialMovementNote, Boolean>(new CheckboxCell()) {
			@Override
			public Boolean getValue(MaterialMovementNote object) {
				return false;
			}
		};
		
		checkColumn.setFieldUpdater(new FieldUpdater<MaterialMovementNote, Boolean>() {
			@Override
			public void update(int index, MaterialMovementNote object, Boolean value) {
				System.out.println("Check Column ....");
				object.setRecordSelect(value);
				table.redrawRow(index);
				selectAllHeader.getValue();
				table.redrawHeaders();
			}
		});
		selectAllHeader =new Header<Boolean>(new CheckboxCell()) {
			@Override
			public Boolean getValue() {
				if(getDataprovider().getList().size()!=0){
					
				}
				return false;
			}
		};
		
		
		selectAllHeader.setUpdater(new ValueUpdater<Boolean>() {
			@Override
			public void update(Boolean value) {
				List<MaterialMovementNote> list=getDataprovider().getList();
				for(MaterialMovementNote object:list){
					object.setRecordSelect(value);
				}
				getDataprovider().setList(list);
				table.redraw();
				addColumnSorting();
			}
		});
		table.addColumn(checkColumn,selectAllHeader);
		table.setColumnWidth(checkColumn,50,Unit.PX);
	
}
	private void addColumnMmnId() {
		getColumnMmnId = new TextColumn<MaterialMovementNote>() {
			@Override
			public String getValue(MaterialMovementNote object) {
				if (object.getCount() == -1)
					return "N.A";
				else
					return object.getCount() + "";
			}
		};
		table.addColumn(getColumnMmnId, "MMN ID");
		table.setColumnWidth(getColumnMmnId,100,Unit.PX);
		getColumnMmnId.setSortable(true);
	}

	private void addColumnMmnDate() {
		getColumnMmnDate = new TextColumn<MaterialMovementNote>() {
			@Override
			public String getValue(MaterialMovementNote object) {
				if (object.getMmnDate()!= null) {
					return AppUtility.parseDate(object.getMmnDate())+"";
				}
				return "";
			}
		};
		table.addColumn(getColumnMmnDate, "Transaction Date");
		table.setColumnWidth(getColumnMmnDate,120,Unit.PX);
		getColumnMmnDate.setSortable(true);
	}

	private void addColumnMmnTitle() {
		getColumnMmnTitle = new TextColumn<MaterialMovementNote>() {
			@Override
			public String getValue(MaterialMovementNote object) {
				if (object.getMmnTitle()!=null){
					return object.getMmnTitle();
				}
				return "";
			}
		};
		table.addColumn(getColumnMmnTitle, "Complaint No.");
		table.setColumnWidth(getColumnMmnTitle,120,Unit.PX);
		getColumnMmnTitle.setSortable(true);
	}
	private void addColumnMmnStatus() {
		getColumnMmnStatus = new TextColumn<MaterialMovementNote>() {
			@Override
			public String getValue(MaterialMovementNote object) {
				if (object.getStatus()!=null){
					return object.getStatus();
				}
				return null;
			}
		};
		table.addColumn(getColumnMmnStatus, "Status");
		table.setColumnWidth(getColumnMmnStatus,90,Unit.PX);
		getColumnMmnStatus.setSortable(true);
	}
		/*********************************************************************************************************************/
	protected void addSortinggetColumnMmnId()
	 {
			List<MaterialMovementNote> list = getDataprovider().getList();
			columnSort = new ListHandler<MaterialMovementNote>(list);
			columnSort.setComparator(getColumnMmnId, new Comparator<MaterialMovementNote>() {
				@Override
				public int compare(MaterialMovementNote e1, MaterialMovementNote e2) {
					if (e1 != null && e2 != null) {
						if (e1.getCount() == e2.getCount()) {
							return 0;
						}
						if (e1.getCount() > e2.getCount()) {
							return 1;
						} else {
							return -1;
						}
					} else {
						return 0;
					}
				}
			});
			table.addColumnSortHandler(columnSort);
		}
	protected void addSortinggetColumnGRNId()
	 {
			List<MaterialMovementNote> list = getDataprovider().getList();
			columnSort = new ListHandler<MaterialMovementNote>(list);
			columnSort.setComparator(getColumnGRNId, new Comparator<MaterialMovementNote>() {
				@Override
				public int compare(MaterialMovementNote e1, MaterialMovementNote e2) {
					if (e1 != null && e2 != null) {
						if (e1.getMmnWoId() == e2.getMmnWoId()) {
							return 0;
						}
						if (e1.getMmnWoId() > e2.getMmnWoId()) {
							return 1;
						} else {
							return -1;
						}
					} else {
						return 0;
					}
				}
			});
			table.addColumnSortHandler(columnSort);
		}
	protected void addSortinggetColumnMmnBranch()
	{
		List<MaterialMovementNote> list=getDataprovider().getList();
		columnSort=new ListHandler<MaterialMovementNote>(list);
		columnSort.setComparator(getColumnMmnBranch, new Comparator<MaterialMovementNote>()
				{
			@Override
			public int compare(MaterialMovementNote e1,MaterialMovementNote e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBranch()!=null && e2.getBranch()!=null){
						return e1.getBranch().compareTo(e2.getBranch());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addSortinggetColumnMaterialStatus()
	{
		List<MaterialMovementNote> list=getDataprovider().getList();
		columnSort=new ListHandler<MaterialMovementNote>(list);
		columnSort.setComparator(getColumnMaterialStatus, new Comparator<MaterialMovementNote>()
				{
			@Override
			public int compare(MaterialMovementNote e1,MaterialMovementNote e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getMmnGRNStatus()!=null && e2.getMmnGRNStatus()!=null){
						return e1.getMmnGRNStatus().compareTo(e2.getMmnGRNStatus());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addSortinggetColumnMmnTitle()
	{
		List<MaterialMovementNote> list=getDataprovider().getList();
		columnSort=new ListHandler<MaterialMovementNote>(list);
		columnSort.setComparator(getColumnMmnTitle, new Comparator<MaterialMovementNote>()
				{
			@Override
			public int compare(MaterialMovementNote e1,MaterialMovementNote e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getMmnTitle()!=null && e2.getMmnTitle()!=null){
						return e1.getMmnTitle().compareTo(e2.getMmnTitle());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addSortinggetColumnMmnDate()
	  {
	  List<MaterialMovementNote> list=getDataprovider().getList();
	  columnSort=new ListHandler<MaterialMovementNote>(list);
	  columnSort.setComparator(getColumnMmnDate, new Comparator<MaterialMovementNote>()
	  {
	  @Override
	  public int compare(MaterialMovementNote e1,MaterialMovementNote e2)
	  {
	  if(e1!=null && e2!=null)
	  {
		  if(e1.getMmnDate() == null && e2.getMmnDate()==null){
		        return 0;
		    }
		    if(e1.getMmnDate() == null) return 1;
		    if(e2.getMmnDate() == null) return -1;
		    return e1.getMmnDate().compareTo(e2.getMmnDate());
	  }
	  return 0;
	  }
	  });
	  table.addColumnSortHandler(columnSort);
	  }
	protected void addSortinggetColumnGRNDate()
	  {
	  List<MaterialMovementNote> list=getDataprovider().getList();
	  columnSort=new ListHandler<MaterialMovementNote>(list);
	  columnSort.setComparator(getColumnGRNDate, new Comparator<MaterialMovementNote>()
	  {
	  @Override
	  public int compare(MaterialMovementNote e1,MaterialMovementNote e2)
	  {
	  if(e1!=null && e2!=null)
	  {
		  if(e1.getMmnWoDate() == null && e2.getMmnWoDate()==null){
		        return 0;
		    }
		    if(e1.getMmnWoDate() == null) return 1;
		    if(e2.getMmnWoDate() == null) return -1;
		    return e1.getMmnWoDate().compareTo(e2.getMmnWoDate());
	  }
	  return 0;
	  }
	  });
	  table.addColumnSortHandler(columnSort);
	  }
	@Override
	protected void initializekeyprovider() {
		
	}

	@Override
	public void addFieldUpdater() {
		updateColumngetMaterialIssueButton();
	}

	@Override
	public void setEnable(boolean state) {
		
	}

	@Override
	public void applyStyle() {
		
	}
	public boolean isCredit(List<MaterialProduct> list){
		for(MaterialProduct m :list ){
			if(m.isCreditReceived()==false){
				return false;
			}
		}
		return true;
	}
}

