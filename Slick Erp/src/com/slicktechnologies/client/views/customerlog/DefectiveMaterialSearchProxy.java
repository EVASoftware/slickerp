package com.slicktechnologies.client.views.customerlog;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.simplesoftwares.client.library.composite.UploadComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.config.CategoryTypeFactory;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.inventory.materialreuestnote.SubProductTableMrn;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.inventory.MaterialMovementNote;
import com.slicktechnologies.shared.common.inventory.MaterialMovementType;
import com.slicktechnologies.shared.common.inventory.StorageLocation;
import com.slicktechnologies.shared.common.inventory.Storagebin;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;

public class DefectiveMaterialSearchProxy extends SearchPopUpScreen<MaterialMovementNote> implements ChangeHandler{

	GenricServiceAsync async = GWT.create(GenricService.class);

	IntegerBox ibMmnMrnId,ibMmnMinId;
	
	IntegerBox ibMmnWoId;
	TextBox ibOrderId;
	
	IntegerBox ibMmnId;
	public DateComparator dateComparator;
	
	TextBox tbMmnTitle;
	
	
	ObjectListBox<ConfigCategory> oblMmnCategory;
	ObjectListBox<Type> oblMmnType;
	
	ObjectListBox<MaterialMovementType> oblMmnTransactionType;
	
	ObjectListBox<Branch> oblMmnBranch;
	ObjectListBox<Employee> oblMmnApproverName;
	ObjectListBox<Employee> oblMmnRequestedBy;
	ObjectListBox<Employee> oblMmnPersonResponsible;
	ObjectListBox<WareHouse> oblWarehouse;
	ListBox oblStorageLocation;
	ListBox oblStorageBin;
	
	
	ListBox tbMmnStatus;
	ProductInfoComposite productInfoComposite;
	
	public DefectiveMaterialSearchProxy() {
		super();
		createGui();
	}	
	public DefectiveMaterialSearchProxy(boolean b) {
		super(b);
		createGui();
	}	
	private void initializeWidget(){
		ibMmnMrnId=new IntegerBox();
		ibMmnMinId=new IntegerBox();
		
		ibMmnWoId=new IntegerBox();
		
		ibMmnId=new IntegerBox();
		dateComparator = new DateComparator("mmnDate", new MaterialMovementNote());
		tbMmnTitle=new TextBox();
		
		oblMmnTransactionType=new ObjectListBox<MaterialMovementType>();
		initalizTransactionName();
//		oblMmnDirection=new ObjectListBox<Config>();
//		AppUtility.MakeLiveConfig(oblMmnDirection, Screen.MMNDIRECTION);
		
		oblMmnCategory= new ObjectListBox<ConfigCategory>();
		oblMmnType = new ObjectListBox<Type>();
		CategoryTypeFactory.initiateOneManyFunctionality(oblMmnCategory,oblMmnType);
		AppUtility.makeTypeListBoxLive(oblMmnType,Screen.MMNCATEGORY);	
		oblMmnBranch = new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(oblMmnBranch);
		
		oblMmnApproverName = new ObjectListBox<Employee>();
		AppUtility.makeSalesPersonListBoxLive(oblMmnApproverName);
		
		oblMmnPersonResponsible = new ObjectListBox<Employee>();
//		AppUtility.makeSalesPersonListBoxLive(oblMmnPersonResponsible);
		oblMmnPersonResponsible.makeEmployeeLive(AppConstants.INVENTORYMODULE, "Material Movement Note", "Person Responsible");
		
		oblMmnRequestedBy = new ObjectListBox<Employee>();
//		AppUtility.makeSalesPersonListBoxLive(oblMmnRequestedBy);
		oblMmnRequestedBy.makeEmployeeLive(AppConstants.INVENTORYMODULE, "Material Movement Note", "Requested By");
		
		ibOrderId=new TextBox();
		
//		oblMmnWarehouse = new ObjectListBox<WareHouse>();
//		initalizWarehouseName();
		
		tbMmnStatus=new ListBox();
		tbMmnStatus.addItem("--SELECT--");
		tbMmnStatus.addItem(AppConstants.CREDITRECEIVED);
		tbMmnStatus.addItem(AppConstants.MATERIALPENDING);
		tbMmnStatus.addItem(AppConstants.SENTTOCOMPANY);
		tbMmnStatus.addItem(AppConstants.MATERIALRECEIVED);
		//AppUtility.setStatusListBox(tbMmnStatus,MaterialMovementNote.getStatusList());
		
		productInfoComposite = AppUtility.initiateMrnProductComposite(new SuperProduct());
		
		oblWarehouse = new ObjectListBox<WareHouse>();
		AppUtility.initializeWarehouse(oblWarehouse);
		oblWarehouse.addChangeHandler(this);
		
		oblStorageLocation = new ListBox();
		oblStorageLocation.addItem("--SELECT--");
		oblStorageLocation.addChangeHandler(this);
		
		oblStorageBin = new ListBox();
		oblStorageBin.addItem("--SELECT--");
		
		
	}
	
	
	@Override
	public void createScreen() {
		initializeWidget();
		
		
		FormFieldBuilder fbuilder;
		
//		fbuilder = new FormFieldBuilder("MIN ID", ibMmnMinId);
//		FormField fibMmnMinId = fbuilder.setMandatory(false).setRowSpan(0)
//				.setColSpan(0).build();
//		
//		fbuilder = new FormFieldBuilder("MRN ID", ibMmnMrnId);
//		FormField fibMmnMrnId = fbuilder.setMandatory(false).setRowSpan(0)
//				.setColSpan(0).build();
//
//		fbuilder = new FormFieldBuilder("Work Order Nr.", ibMmnWoId);
//		FormField fibMmnWoId = fbuilder.setMandatory(false).setRowSpan(0)
//				.setColSpan(0).build();
//		
//		fbuilder = new FormFieldBuilder("MMN ID", ibMmnId);
//		FormField fibMmnId= fbuilder.setMandatory(false).setMandatoryMsg("MMN ID is Mandatory!").setRowSpan(0)
//				.setColSpan(0).build();
//		
		fbuilder = new FormFieldBuilder("Call Log Id", tbMmnTitle);
		FormField ftbMmnTitle = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("From Date", dateComparator.getFromDate());
		FormField fdbMinFrmDate = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("To Date", dateComparator.getToDate());
		FormField fdbMinToDate = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Warehouse", oblWarehouse);
		FormField fwarehouse = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Storage Location", oblStorageLocation);
		FormField flocation = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Storage Bin", oblStorageBin);
		FormField fbin = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		
//		fbuilder = new FormFieldBuilder("MMN Category", oblMmnCategory);
//		FormField foblMmnCategory = fbuilder.setMandatory(false).setRowSpan(0)
//				.setColSpan(0).build();
//		
//		fbuilder = new FormFieldBuilder("MMN Type", oblMmnType);
//		FormField foblMmnType = fbuilder.setMandatory(false).setRowSpan(0)
//				.setColSpan(0).build();
//		
//		fbuilder = new FormFieldBuilder("Order ID", ibOrderId);
//		FormField fibOrderId = fbuilder.setMandatory(false).setRowSpan(0)
//				.setColSpan(0).build();
//		
		fbuilder = new FormFieldBuilder("Transaction Type", oblMmnTransactionType);
		FormField foblMmnTransactionType = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("* Branch", oblMmnBranch);
		FormField foblMmnBranch = fbuilder.setMandatory(true).setMandatoryMsg("Branch is Mandatory!").setRowSpan(0)
				.setColSpan(0).build();
//		
//		fbuilder = new FormFieldBuilder("Approver", oblMmnApproverName);
//		FormField foblMmnApproverName = fbuilder.setMandatory(true).setMandatoryMsg("Approver is Mandatory!").setRowSpan(0)
//				.setColSpan(0).build();
//		
//		fbuilder = new FormFieldBuilder("Requested By", oblMmnRequestedBy);
//		FormField foblMmnRequestedBy = fbuilder.setMandatory(true).setMandatoryMsg("Requested By is Mandatory!").setRowSpan(0)
//				.setColSpan(0).build();
//		
//		fbuilder = new FormFieldBuilder("Person Responsible", oblMmnPersonResponsible);
//		FormField foblMmnPersonResponsible = fbuilder.setMandatory(true).setMandatoryMsg("Issued By is Mandatory!").setRowSpan(0)
//				.setColSpan(0).build();
//		
//		fbuilder = new FormFieldBuilder("WareHouse", oblMmnWarehouse);
//		FormField foblMmnWarehouse = fbuilder.setMandatory(false).setRowSpan(0)
//				.setColSpan(0).build();
//		
		fbuilder = new FormFieldBuilder("Status", tbMmnStatus);
		FormField ftbMmnStatus = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("", productInfoComposite);
		FormField fproductInfoComposite= fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(3).build();
		
		
		FormField[][] formfield = { 
				{foblMmnBranch ,ftbMmnTitle,fdbMinFrmDate,fdbMinToDate},
				{ ftbMmnStatus, fwarehouse , flocation , fbin},
				{ fproductInfoComposite},
		};
		this.fields = formfield;
	}
	
	@Override
	public MyQuerry getQuerry() {
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter temp = null;

		if (dateComparator.getValue() != null) {
			filtervec.addAll(dateComparator.getValue());
		}

		
		if (oblMmnTransactionType.getSelectedIndex() != 0) {
			temp = new Filter();
			temp.setStringValue(oblMmnTransactionType.getValue().trim());
			temp.setQuerryString("mmnTransactionType");
			filtervec.add(temp);
		}
		if (tbMmnStatus.getSelectedIndex()!=0) {
			temp = new Filter();
			temp.setStringValue(tbMmnStatus.getValue(tbMmnStatus.getSelectedIndex()));
			temp.setQuerryString("mmnGRNStatus");
			filtervec.add(temp);
		}
		if (!tbMmnTitle.getValue().equals("")) {
			temp = new Filter();
			temp.setStringValue(tbMmnTitle.getValue());
			temp.setQuerryString("mmnTitle");
			filtervec.add(temp);
		}
		
		if (!productInfoComposite.getProdCode().getValue().equals("")) {
			temp = new Filter();
			temp.setStringValue(productInfoComposite.getProdCode().getValue());
			temp.setQuerryString("subProductTableMmn.materialProductCode");
			filtervec.add(temp);
		}

		if (!productInfoComposite.getProdID().getValue().equals("")) {
			temp = new Filter();
			temp.setIntValue(Integer.parseInt(productInfoComposite.getProdID().getValue()));
			temp.setQuerryString("subProductTableMmn.materialProductId");
			filtervec.add(temp);
		}
		if (oblWarehouse.getSelectedIndex()!=0) {
			temp = new Filter();
			temp.setStringValue(oblWarehouse.getValue(oblWarehouse.getSelectedIndex()).trim());
			temp.setQuerryString("subProductTableMmn.materialProductWarehouse");
			filtervec.add(temp);
		}
		
		if (oblStorageLocation.getSelectedIndex()!=0) {
			temp = new Filter();
			temp.setStringValue(oblStorageLocation.getValue(oblStorageLocation.getSelectedIndex()).trim());
			temp.setQuerryString("subProductTableMmn.materialProductStorageLocation");
			filtervec.add(temp);
		}
	
		if (oblStorageBin.getSelectedIndex()!=0) {
			temp = new Filter();
			temp.setStringValue(oblStorageBin.getValue(oblStorageBin.getSelectedIndex()).trim());
			temp.setQuerryString("subProductTableMmn.materialProductStorageBin");
			filtervec.add(temp);
		}
//		List<String> branchList = new ArrayList<String>();
//		for(Branch b : LoginPresenter.globalBranch){
//			branchList.add(b.getBusinessUnitName());
//		}
//		temp = new Filter();
//		temp.setQuerryString("branch IN");
//		temp.setList(branchList);
//		filtervec.add(temp);
		if(oblMmnBranch.getSelectedIndex()!=0){
			temp = new Filter();
			temp.setQuerryString("branch");
			temp.setStringValue(oblMmnBranch.getValue());
			filtervec.add(temp);
		}

		temp = new Filter();
		temp.setQuerryString("transDirection");
		temp.setStringValue("IN");
		filtervec.add(temp);
		
		MyQuerry querry = new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new MaterialMovementNote());
		return querry;
	}
	
	protected void initalizTransactionName() {
		MyQuerry querry = new MyQuerry(new Vector<Filter>(), new MaterialMovementType());
		oblMmnTransactionType.MakeLive(querry);
	}
	@Override
	public void onChange(ChangeEvent event) {
		if(event.getSource().equals(oblWarehouse)){
			retriveStorageLocation();
	}
	if(event.getSource().equals(oblStorageLocation)){
			retriveStorageBin();
	}
	}

private void retriveStorageLocation(){
	MyQuerry query = new MyQuerry();
	Company c=new Company();
	System.out.println("Company Id :: "+c.getCompanyId());
	
	Vector<Filter> filtervec=new Vector<Filter>();
	Filter filter = null;
	
	filter = new Filter();
	filter.setQuerryString("companyId");
	filter.setLongValue(c.getCompanyId());
	filtervec.add(filter);
	
	filter = new Filter();
	filter.setQuerryString("warehouseName");
	filter.setStringValue(oblWarehouse.getValue(oblWarehouse.getSelectedIndex()));
	filtervec.add(filter);
	
	query.setFilters(filtervec);
	query.setQuerryObject(new StorageLocation());
	
	async.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
		@Override
		public void onFailure(Throwable caught) {
		}

		@Override
		public void onSuccess(ArrayList<SuperModel> result) {
			oblStorageLocation.clear();
			oblStorageBin.clear();
			oblStorageLocation.addItem("--SELECT--");
			oblStorageBin.addItem("--SELECT--");
			for (SuperModel model : result) {
				StorageLocation entity = (StorageLocation) model;
//				storageList.add(entity.getBusinessUnitName());
				oblStorageLocation.addItem(entity.getBusinessUnitName());
				System.out.println("Location :"+entity.getBusinessUnitName());
			}
			
		}
	});
}

// ****************************** Retrieving Storage Bin From Storage location **********************************

private void retriveStorageBin(){
	MyQuerry query = new MyQuerry();
	Company c=new Company();
	System.out.println("Company Id :: "+c.getCompanyId());
	
	Vector<Filter> filtervec=new Vector<Filter>();
	Filter filter = null;
	
	filter = new Filter();
	filter.setQuerryString("companyId");
	filter.setLongValue(c.getCompanyId());
	filtervec.add(filter);
	
	filter = new Filter();
	filter.setQuerryString("storagelocation");
	filter.setStringValue(oblStorageLocation.getValue(oblStorageLocation.getSelectedIndex()));
	filtervec.add(filter);
	
	query.setFilters(filtervec);
	query.setQuerryObject(new Storagebin());
	
	async.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
		@Override
		public void onFailure(Throwable caught) {
		}

		@Override
		public void onSuccess(ArrayList<SuperModel> result) {
			oblStorageBin.clear();
			oblStorageBin.addItem("--SELECT--");
			for (SuperModel model : result) {
				Storagebin entity = (Storagebin) model;
				oblStorageBin.addItem(entity.getBinName());
				System.out.println("BIN :"+entity.getBinName());
			}
			
		}
	});
}
@Override
public boolean validate() {
	if(LoginPresenter.branchRestrictionFlag)
	{
	if(oblMmnBranch.getSelectedIndex()==0)
	{
		showDialogMessage("Select Branch");
		return false;
	}
	}
	return true;
}

}
