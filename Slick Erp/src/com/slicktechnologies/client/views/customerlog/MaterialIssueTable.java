
package com.slicktechnologies.client.views.customerlog;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.SelectionCell;
import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.views.inventory.materialreuestnote.MaterialRequestNotePresenter;
import com.slicktechnologies.client.views.inventory.recievingnote.InventoryLocationPopUp;
import com.slicktechnologies.shared.TaxDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerlog.MaterialRequired;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;
import com.slicktechnologies.shared.common.productlayer.Tax;


public class MaterialIssueTable extends SuperTable<MaterialRequired> implements ClickHandler {
	GenricServiceAsync async = GWT.create(GenricService.class);
	NumberFormat nf=NumberFormat.getFormat("0.00");
	
	InventoryLocationPopUp invLoc=new InventoryLocationPopUp();
	PopupPanel panel=new PopupPanel(true);
	int rowIndex;
	boolean warrantyFlag = false;
	TextColumn<MaterialRequired> getColumnProductId;
	TextColumn<MaterialRequired> getColumnProductCode;
	TextColumn<MaterialRequired> getColumnProductName;
	TextColumn<MaterialRequired> getColumnProductAvailableQty;
	TextColumn<MaterialRequired> getViewColumnProductRequiredQty;
	Column<MaterialRequired, String> getColumnProductRequiredQty;
	TextColumn<MaterialRequired> getViewColumnProductReturnQty;
	Column<MaterialRequired, String> getColumnProductReturnQty;
	TextColumn<MaterialRequired> getViewColumnProductPayableQty;
	Column<MaterialRequired, String> getColumnProductPayableQty;
	TextColumn<MaterialRequired> getColumnProductUom;
	Column<MaterialRequired, String> getColumnProductDeleteButton;
	Column<MaterialRequired, String> addColumn;
	Column<MaterialRequired , String> getColumnProdPrice;
	TextColumn<MaterialRequired> getColumnViewProdPrice;
	public  ArrayList<String> warehouseList;
	public  ArrayList<String> storageBinList;
	public  ArrayList<String> storagelocList;
	
	//public  ArrayList<Storagebin> storagelocList1;
	
	Column<MaterialRequired, String> warehouseColumn;
	TextColumn<MaterialRequired> warehouseViewColumn;
	
	Column<MaterialRequired, String> storagebin;
	Column<MaterialRequired, String> columnviewBin;
	
	Column<MaterialRequired, String> storageLoc;
	TextColumn<MaterialRequired> columnviewLoc;
	
	Column<MaterialRequired,String> vatColumn,serviceColumn;
	
	TextColumn<MaterialRequired> viewServiceTaxColumn;
	TextColumn<MaterialRequired> viewVatTaxColumn;
	Column<MaterialRequired,String> discountAmt;
	public  ArrayList<TaxDetails> vattaxlist;
	public  ArrayList<TaxDetails> servicetaxlist;
	public  ArrayList<String> list;
	public  ArrayList<String> vatlist;
	int productIdCount=0;
	int productSrNoCount=0;
	
	public MaterialIssueTable() {
		invLoc.getAddButton().addClickHandler(this);
		invLoc.getCancelButton().addClickHandler(this);
	}
	
	
	@Override
	public void createTable() {
		addColumnProductId();
		addColumnProductCode();
		addColumnProductName();
		//addColumnProdPrice();
		addColumnViewProdPrice();
		addColumnProductRequiredQty();
		addColumnProductAvailableQty();

		viewWarehouse();
		viewstarageLoc();
		viewstorageBin();
		createColumnAddColumn();
		addColumnProductDeleteButton();
		addFieldUpdater();
		table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);	
	}

	public void addViewColumn(){
		addColumnProductId();
		addColumnProductCode();
		addColumnProductName();
		addColumnViewProdPrice();
		addColumnProductViewRequiredQty();
		addColumnProductAvailableQty();
		//addColumnProductViewPayableQty();
		addColumnProductPayableQty();
		addColumnProductReturnQty();
		//addColumnProductUom();
		
		viewWarehouse();
		viewstarageLoc();
		viewstorageBin();
		addFieldUpdater1();
	}

	public void addEditColumn(){
		addColumnProductId();
		addColumnProductCode();
		addColumnProductName();
		addColumnProdPrice();
		addColumnProductRequiredQty();
		addColumnProductAvailableQty();
		addColumnProductPayableQtyUpdater();
		//addColumnProductPayableQty();
		addColumnProductReturnQty();
		//addColumnProductUom();
		
		viewWarehouse();
		viewstarageLoc();
		viewstorageBin();
		createColumnAddColumn();	
		addColumnProductDeleteButton();
		addFieldUpdater();
	}
	
	@Override
	public void addFieldUpdater() {
		addColumnDeleteUpdater();
	//	addColumnPriceUpdater();
		addColumnProductQtyUpdater();
		//addColumnProductReturnQtyUpdater();
		//if(warrantyFlag){
	//	addColumnProductPayableQtyUpdater();
		//}
//		updateServiceTaxColumn();
//		updateVatTaxColumn();
//		createFieldUpdaterDiscAmtColumn();
		createFieldUpdaterAdd();
		//addColumnRemarkUpdater();
	}

	public void addFieldUpdater1(){
		addColumnProductReturnQtyUpdater();
		addColumnProductPayableQtyUpdater();

	}
	/**
	 * Develpoed By : Rohan Bhagde
	 * Date : 16/11/2016
	 * Reason: This is used to lock all the fields in table when we issue material from project. 
	 */
	

	
	public void setEnableOnlySelectWarehouseButton() {
		int tablecolcount = this.table.getColumnCount();
		for (int i = tablecolcount - 1; i > -1; i--)
		table.removeColumn(i);
		
		addColumnProductId();
		addColumnProductCode();
		addColumnProductName();
		addColumnProductViewRequiredQty();
		addColumnProductAvailableQty();
		addColumnProductUom();
		//addColumnProductViewRemarks();
		viewWarehouse();
		viewstarageLoc();
		viewstorageBin();
		createColumnAddColumn();
		addFieldUpdater();
	}


	/**
	 * ends here 
	 */

	@Override
	public void setEnable(boolean state) {
		int tablecolcount = this.table.getColumnCount();
		for (int i = tablecolcount - 1; i > -1; i--)
			table.removeColumn(i);
		if (state == true)
			addEditColumn();
		if (state == false)
			addViewColumn();
	}
	
	
	public void createColumnAddColumn() {
		ButtonCell btnCell = new ButtonCell();
		addColumn = new Column<MaterialRequired, String>(btnCell) {
			@Override
			public void render(Context context, MaterialRequired object,
					SafeHtmlBuilder sb) {
				if(object.isFlag()){
					super.render(context, object, sb);
				}
				
			}
			@Override
			public String getValue(MaterialRequired object) {
				if(object.isFlag()){
					return "Select Warehouse";
				}else{
					return "";
				}
			}
		};
		table.addColumn(addColumn, "");
		table.setColumnWidth(addColumn,180,Unit.PX);

	}	
	private void addColumnPriceUpdater(){
		getColumnProdPrice.setFieldUpdater(new FieldUpdater<MaterialRequired, String>() {
			@Override
			public void update(int index, MaterialRequired object,String value) {
				try {
					double val1 = Double.parseDouble(value.trim());
					object.setProductPrice(val1);
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				} catch (NumberFormatException e) {

				}
				table.redrawRow(index);
			}
		});

	}
	private void addColumnProductQtyUpdater() {
		getColumnProductRequiredQty.setFieldUpdater(new FieldUpdater<MaterialRequired, String>() {
			@Override
			public void update(int index, MaterialRequired object,String value) {
				try {
					double val1 = Double.parseDouble(value.trim());
					object.setProductIssueQty(val1);
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				} catch (NumberFormatException e) {

				}
				table.redrawRow(index);
			}
		});
	}
	private void addColumnProductReturnQtyUpdater(){
		getColumnProductReturnQty.setFieldUpdater(new FieldUpdater<MaterialRequired, String>() {
			@Override
			public void update(int index, MaterialRequired object,String value) {
				try {
					double val1 = Double.parseDouble(value.trim());
					object.setProductReturnQty(val1);
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				} catch (NumberFormatException e) {
				}
				table.redrawRow(index);
			}
		});
	}

	private void addColumnProductPayableQtyUpdater(){
		getColumnProductPayableQty.setFieldUpdater(new FieldUpdater<MaterialRequired, String>() {
			@Override
			public void update(int index, MaterialRequired object,String value) {
				try {
					double val1 = Double.parseDouble(value.trim());
					object.setProductPayableQuantity(val1);
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				} catch (NumberFormatException e) {
				}
				table.redrawRow(index);
			}
		});
	}
	private void addColumnDeleteUpdater() {
		getColumnProductDeleteButton.setFieldUpdater(new FieldUpdater<MaterialRequired, String>() {
			@Override
			public void update(int index, MaterialRequired object,String value) {
				getDataprovider().getList().remove(object);
				table.redrawRow(index);
			}
		});
	}
	
	/****************************************** Editable Column  ***********************************************/
	private void addColumnProductDeleteButton() {
		ButtonCell btnCell = new ButtonCell();
		getColumnProductDeleteButton = new Column<MaterialRequired, String>(btnCell) {
			@Override
			public void render(Context context, MaterialRequired object,
					SafeHtmlBuilder sb) {
				if(object.isFlag()){
					super.render(context, object, sb);
				}
				
			}
			@Override
			public String getValue(MaterialRequired object) {
				if(object.isFlag()){
					return "Delete";
				}else{
					return "";
				}
			}
		};
		table.addColumn(getColumnProductDeleteButton, "Delete");
		table.setColumnWidth(getColumnProductDeleteButton,60,Unit.PX);
	}
	
	private void addColumnProductRequiredQty() {
		EditTextCell editCell = new EditTextCell();
		getColumnProductRequiredQty=new Column<MaterialRequired, String>(editCell) {
			@Override
			public String getValue(MaterialRequired object) {
				return nf.format(object.getProductIssueQty());
			}
		};
		table.addColumn(getColumnProductRequiredQty, "#Quantity");
		table.setColumnWidth(getColumnProductRequiredQty,81,Unit.PX);
	}
	private void addColumnProductReturnQty() {
		EditTextCell editCell = new EditTextCell();
		getColumnProductReturnQty=new Column<MaterialRequired, String>(editCell) {
			@Override
			public String getValue(MaterialRequired object) {
				return nf.format(object.getProductReturnQty());
			}
		};
		table.addColumn(getColumnProductReturnQty, "#Return Quantity");
		table.setColumnWidth(getColumnProductReturnQty,81,Unit.PX);
	}

	private void addColumnProductPayableQty() {
		EditTextCell editCell = new EditTextCell();
		getColumnProductPayableQty=new Column<MaterialRequired, String>(editCell) {
			@Override
			public String getValue(MaterialRequired object) {
				return nf.format(object.getProductPayableQuantity());
			}
		};
		table.addColumn(getColumnProductPayableQty, "#Out Of Warranty Quantity");
		table.setColumnWidth(getColumnProductPayableQty,85,Unit.PX);
	}
	
	public void viewWarehouse() {
		warehouseViewColumn = new TextColumn<MaterialRequired>() {
			@Override
			public String getValue(MaterialRequired object) {
				return object.getMaterialProductWarehouse();
			}
		};
		table.addColumn(warehouseViewColumn, "Warehouse");
		table.setColumnWidth(warehouseViewColumn,153,Unit.PX);
	}
	
	public void viewstarageLoc() {
		columnviewLoc = new TextColumn<MaterialRequired>() {
			@Override
			public String getValue(MaterialRequired object) {
				return object.getMaterialProductStorageLocation();
			}
		};
		table.addColumn(columnviewLoc, "Storage Location");
		table.setColumnWidth(columnviewLoc,153,Unit.PX);
	}
	public void viewstorageBin() {
		columnviewBin = new TextColumn<MaterialRequired>() {
			@Override
			public String getValue(MaterialRequired object) {
				return object.getMaterialProductStorageBin();
			}
		};
		table.addColumn(columnviewBin, "Storage Bin");
		table.setColumnWidth(columnviewBin,153,Unit.PX);
	}
	
	private void addColumnProductUom() {
		getColumnProductUom=new TextColumn<MaterialRequired>() {
			@Override
			public String getValue(MaterialRequired object) {
				return object.getProductUOM();
			}
		};
		table.addColumn(getColumnProductUom, "UOM");
		table.setColumnWidth(getColumnProductUom,45,Unit.PX);
	}

	private void addColumnProductAvailableQty() {
		getColumnProductAvailableQty=new TextColumn<MaterialRequired>() {
			@Override
			public String getValue(MaterialRequired object) {
				return nf.format(object.getProductAvailableQty());
			}
		};
		table.addColumn(getColumnProductAvailableQty, "Available Quantity");
		table.setColumnWidth(getColumnProductAvailableQty,180,Unit.PX);
	}

	private void addColumnProductViewRequiredQty() {
		getViewColumnProductRequiredQty=new TextColumn<MaterialRequired>() {
			
			@Override
			public String getValue(MaterialRequired object) {
				return nf.format(object.getProductIssueQty());
			}
		};
		table.addColumn(getViewColumnProductRequiredQty,"#Quantity");
		table.setColumnWidth(getViewColumnProductRequiredQty,81,Unit.PX);
	}
	private void addColumnProductViewReturnQty() {
		getViewColumnProductReturnQty=new TextColumn<MaterialRequired>() {
			
			@Override
			public String getValue(MaterialRequired object) {
				return nf.format(object.getProductReturnQty());
			}
		};
		table.addColumn(getViewColumnProductReturnQty,"#Return Quantity");
		table.setColumnWidth(getViewColumnProductReturnQty,81,Unit.PX);
	}
	private void addColumnProductViewPayableQty() {
		getViewColumnProductPayableQty=new TextColumn<MaterialRequired>() {		
			@Override
			public String getValue(MaterialRequired object) {
				return nf.format(object.getProductPayableQuantity());
			}
		};
		table.addColumn(getViewColumnProductPayableQty,"#Out Of Warranty Quantity");
		table.setColumnWidth(getViewColumnProductPayableQty,85,Unit.PX);
	}

	private void addColumnProductName() {
		getColumnProductName=new TextColumn<MaterialRequired>() {
			@Override
			public String getValue(MaterialRequired object) {
				return object.getProductName();
			}
		};
		table.addColumn(getColumnProductName, "Name");
		table.setColumnWidth(getColumnProductName,153,Unit.PX);
	}

	private void addColumnProductCode() {
		getColumnProductCode=new TextColumn<MaterialRequired>() {
			@Override
			public String getValue(MaterialRequired object) {
				return object.getProductCode();
			}
		};
		table.addColumn(getColumnProductCode, "Code");
		table.setColumnWidth(getColumnProductCode,90,Unit.PX);
	}

	private void addColumnProductId() {
		getColumnProductId=new TextColumn<MaterialRequired>() {
			@Override
			public String getValue(MaterialRequired object) {
				return object.getProductId()+"";
			}
		};
		table.addColumn(getColumnProductId, "ID");
		table.setColumnWidth(getColumnProductId,100,Unit.PX);
	}
	
	protected void addColumnProdPrice() {
	EditTextCell editCell = new EditTextCell();
	getColumnProdPrice = new Column<MaterialRequired, String>(editCell) {

		@Override
		public String getValue(MaterialRequired object) {
				return object.getProductPrice()+"";
		}
	};
	table.addColumn(getColumnProdPrice, "# Price");
	table.setColumnWidth(getColumnProdPrice,80,Unit.PX);
}
	protected void addColumnViewProdPrice() {
		getColumnViewProdPrice = new TextColumn<MaterialRequired>() {

			@Override
			public String getValue(MaterialRequired object) {
					return object.getProductPrice()+"";
			}
		};
		table.addColumn(getColumnViewProdPrice, "Price");
		table.setColumnWidth(getColumnViewProdPrice,80,Unit.PX);
	}

	public void createFieldUpdaterAdd() {
		addColumn.setFieldUpdater(new FieldUpdater<MaterialRequired, String>() {
			@Override
			public void update(int index, MaterialRequired object, String value) {

				panel = new PopupPanel(true);
				panel.add(invLoc);
				InventoryLocationPopUp.initialzeWarehouse(object.getProductId());
				if (object.getMaterialProductWarehouse() != null
						&& object.getMaterialProductStorageLocation() != null
						&& object.getMaterialProductStorageBin() != null) {
					System.out
							.println("Inside table not null list settt...........");
					for (int i = 0; i < InventoryLocationPopUp.getLsWarehouse()
							.getItemCount(); i++) {
						String data = InventoryLocationPopUp.lsWarehouse
								.getItemText(i);
						if (data.equals(object.getMaterialProductWarehouse())) {
							InventoryLocationPopUp.lsWarehouse
									.setSelectedIndex(i);
							break;
						}
					}

					invLoc.getLsStorageLoc().clear();
					invLoc.lsStorageLoc.addItem("--SELECT--");
					invLoc.lsStorageLoc.addItem(object
							.getMaterialProductStorageLocation());

					invLoc.getLsStoragebin().clear();
					invLoc.lsStoragebin.addItem("--SELECT--");

					for (int i = 0; i < invLoc.arraylist2.size(); i++) {
						if (invLoc.arraylist2.get(i).getWarehouseName()
								.equals(object.getMaterialProductWarehouse())
								&& invLoc.arraylist2
										.get(i)
										.getStorageLocation()
										.equals(object
												.getMaterialProductStorageLocation())) {
							invLoc.lsStoragebin.addItem(invLoc.arraylist2
									.get(i).getStorageBin());
						}
					}

					for (int i = 0; i < invLoc.getLsStorageLoc().getItemCount(); i++) {
						String data = invLoc.lsStorageLoc.getItemText(i);
						if (data.equals(object
								.getMaterialProductStorageLocation())) {
							invLoc.lsStorageLoc.setSelectedIndex(i);
							break;
						}
					}
					for (int i = 0; i < invLoc.getLsStoragebin().getItemCount(); i++) {
						String data = invLoc.lsStoragebin.getItemText(i);
						if (data.equals(object.getMaterialProductStorageBin())) {
							invLoc.lsStoragebin.setSelectedIndex(i);
							break;
						}
					}
					//object.setWarehouseName(warehouseName);
				}

				invLoc.formView();
				panel.show();
				panel.center();
				rowIndex = index;
			}
		});
	}

	@Override
	public void onClick(ClickEvent event) {
		if (event.getSource() == invLoc.getAddButton()) {
			if (invLoc.getLsWarehouse().getSelectedIndex() != 0
					&& invLoc.getLsStorageLoc().getSelectedIndex() != 0
					&& invLoc.getLsStoragebin().getSelectedIndex() != 0) {
				ArrayList<MaterialRequired> list = new ArrayList<MaterialRequired>();
				if (getDataprovider().getList().size() != 0) {
					list.addAll(getDataprovider().getList());
					for (int i = rowIndex; i < getDataprovider().getList()
							.size(); i++) {
						list.get(rowIndex).setMaterialProductWarehouse(
								invLoc.getLsWarehouse().getValue(
										invLoc.getLsWarehouse()
												.getSelectedIndex()));
						list.get(rowIndex).setMaterialProductStorageLocation(
								invLoc.getLsStorageLoc().getValue(
										invLoc.getLsStorageLoc()
												.getSelectedIndex()));
						list.get(rowIndex).setMaterialProductStorageBin(
								invLoc.getLsStoragebin().getValue(
										invLoc.getLsStoragebin()
												.getSelectedIndex()));
						list.get(rowIndex).setProductAvailableQty(
								invLoc.getAvailableQty());

						// list.get(rowIndex).setMatReqReOdrLvlQty(invLoc.getReorderQty());
						getDataprovider().getList().clear();
						getDataprovider().getList().addAll(list);

						// ///////////////////

						// double
						// reqQty=list.get(rowIndex).getProductIssueQty();
						// double
						// avlQty=list.get(rowIndex).getProductAvailableQty();
						// double
						// reOdrQty=list.get(rowIndex).getMatReqReOdrLvlQty();
						// double diff=avlQty-reqQty;
						// String
						// location=list.get(rowIndex).getMaterialRequiredWarehouse()+"/"
						// +list.get(rowIndex).getMaterialRequiredStorageLocation()+"/"
						// +list.get(rowIndex).getMaterialRequiredStorageBin();
						//
						// System.out.println("REorder Qty in Select : "+reOdrQty);
						//
						// if(reOdrQty!=0){
						// System.out.println("REorder Qty in Select1 : "+reOdrQty);
						// if(avlQty<=reOdrQty){
						// GWTCAlert alert=new GWTCAlert();
						// alert.alert("Available quantity in warehouse "+location+" is less than reorder level!");
						// }else if(diff<=reOdrQty){
						// GWTCAlert alert=new GWTCAlert();
						// alert.alert("Available quantity in warehouse "+location+" is less than reorder level!");
						// }
						// }
					}
				}
				panel.hide();
			}
			if (invLoc.getLsWarehouse().getSelectedIndex() == 0
					&& invLoc.getLsStorageLoc().getSelectedIndex() == 0
					&& invLoc.getLsStoragebin().getSelectedIndex() == 0) {
				MaterialRequestNotePresenter
						.showMessage("Please Select Warehouse.");
			} else if (invLoc.getLsStorageLoc().getSelectedIndex() == 0
					&& invLoc.getLsStoragebin().getSelectedIndex() == 0) {
				MaterialRequestNotePresenter
						.showMessage("Please Select Storage Location.");
			} else if (invLoc.getLsStoragebin().getSelectedIndex() == 0) {
				MaterialRequestNotePresenter
						.showMessage("Please Select Storage Bin.");
			}

		}
		if (event.getSource() == invLoc.getCancelButton()) {
			panel.hide();
		}
	}

	/*****************************************************************************************************************/

	@Override
	public void applyStyle() {
		
	}
	

	@Override
	protected void initializekeyprovider() {
		
	}
	
	private void retriveServiceTax() {
		final GenricServiceAsync service = GWT.create(GenricService.class);
		MyQuerry query = new MyQuerry();
		Company c=new Company();
		System.out.println("Company Id :: "+c.getCompanyId());
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		query.setFilters(filtervec);
		query.setQuerryObject(new TaxDetails());
		
		service.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
			}		
			public void onSuccess(ArrayList<SuperModel> result){
				
				System.out.println("result size of tax "+result);
				list=new ArrayList<String>();
				servicetaxlist=new ArrayList<TaxDetails>();
				vattaxlist = new ArrayList<TaxDetails>();
				vatlist=new ArrayList<String>();
				List<TaxDetails> backlist=new ArrayList<TaxDetails>();
				
				for (SuperModel model : result) {
					TaxDetails entity = (TaxDetails) model;
					
					backlist.add(entity);
					servicetaxlist.add(entity);
					vattaxlist.add(entity);
				}
				
				for(int i=0;i<backlist.size();i++){
					//  rohan commented this code for loading all the taxes in drop down
//					if(backlist.get(i).getServiceTax().equals(true)){
						list.add(backlist.get(i).getTaxChargeName());
						System.out.println("list size"+list.size());
//					}
//					else if(backlist.get(i).getVatTax().equals(true)){
						vatlist.add(backlist.get(i).getTaxChargeName());
						System.out.println("valist size"+vatlist.size());
//					}
				}
				
				
				
				addEditColumnVatTax();
				addEditColumnServiceTax();
				createColumnDiscountAmt();
				
				addColumnProductDeleteButton();
				addFieldUpdater();
			}


			
		});
	}
	public void addEditColumnVatTax(){
		SelectionCell employeeselection= new SelectionCell(vatlist);
		vatColumn = new Column<MaterialRequired, String>(employeeselection) {
			@Override
			public String getValue(MaterialRequired object) {
				if (object.getVatTax().getTaxConfigName()!= null
						&&!object.getVatTax().getTaxConfigName().equals("")) {
					
					object.setVatTaxEdit(object.getVatTax().getTaxConfigName());

					return object.getVatTax().getTaxConfigName();
				} else
					return "NA";

			}
		};
		table.addColumn(vatColumn, "# Tax 1");
		table.setColumnWidth(vatColumn,130,Unit.PX);
		table.setColumnWidth(vatColumn, 100,Unit.PX);		
	}	
	
	public void updateVatTaxColumn()
	{

		vatColumn.setFieldUpdater(new FieldUpdater<MaterialRequired, String>() {
			
			@Override
			public void update(int index, MaterialRequired object, String value) {
				try {
					String val1 = (value.trim());
					object.setVatTaxEdit(val1);
					Tax tax = object.getVatTax();
					for(int i=0;i<vattaxlist.size();i++)
					{
						if(val1.trim().equals(vattaxlist.get(i).getTaxChargeName())){
							tax.setTaxName(val1);
							tax.setPercentage(vattaxlist.get(i).getTaxChargePercent());
							tax.setTaxPrintName(vattaxlist.get(i).getTaxPrintName());
							tax.setTaxConfigName(val1);
							break;
						}
					}
					object.setVatTax(tax);
					
					productIdCount=object.getPrduct().getCount();
					productSrNoCount = object.getProductSrNo();
					
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
					//calculateTotalAmount(object);
				} catch (NumberFormatException e) {
				}
				table.redrawRow(index);
			}
		});
	}
	
	public void addEditColumnServiceTax() {
		SelectionCell employeeselection= new SelectionCell(list);
		serviceColumn = new Column<MaterialRequired, String>(employeeselection) {
			@Override
			public String getValue(MaterialRequired object) {
				if (object.getServiceTax().getTaxConfigName()!= null
						&&!object.getServiceTax().getTaxConfigName().equals("")) {
					
					object.setServiceTaxEdit(object.getServiceTax().getTaxConfigName());

					return object.getServiceTax().getTaxConfigName();
				} else
					return "NA";

			}

			
		};
		table.addColumn(serviceColumn, "# Tax 2");
		table.setColumnWidth(serviceColumn,140,Unit.PX);
		table.setColumnWidth(serviceColumn, 100,Unit.PX);
	}
	
	
	public void updateServiceTaxColumn()
	{
		serviceColumn.setFieldUpdater(new FieldUpdater<MaterialRequired, String>() {
			
			@Override
			public void update(int index, MaterialRequired object, String value) {
				try {
					String val1 = (value.trim());
					object.setServiceTaxEdit(val1);
					Tax tax = object.getServiceTax();
					for(int i=0;i<servicetaxlist.size();i++)
					{
						if(val1.trim().equals(servicetaxlist.get(i).getTaxChargeName())){
							tax.setTaxName(val1);
							tax.setPercentage(servicetaxlist.get(i).getTaxChargePercent());
							/**
							 * rohan added this for GST implementation Date :23-06-2017
							 */
							tax.setTaxPrintName(servicetaxlist.get(i).getTaxPrintName());
							/**
							 * Date : 03-08-2017 By ANIL
							 */
							tax.setTaxConfigName(val1);
							
							/**
							 * End
							 */
							break;
						}
					}
					object.setServiceTax(tax);
					System.out.println("TZZZXXX ="+tax.getTaxPrintName());
					
					productIdCount=object.getPrduct().getCount();
					productSrNoCount = object.getProductSrNo();
					//calculateTotalAmount(object);
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
					
				} catch (NumberFormatException e) {
				}
				table.redrawRow(index);
			}
		});
	}
	private void createColumnDiscountAmt() {
		EditTextCell editCell=new EditTextCell();
		discountAmt=new Column<MaterialRequired,String>(editCell)
				{
			@Override
			public String getValue(MaterialRequired object)
			{
				if(object.getDiscountAmt()==0){
					return 0+"";}
				else{
					return object.getDiscountAmt()+"";}
			}
				};
				table.addColumn(discountAmt,"#Disc Amt");
				table.setColumnWidth(discountAmt, 100,Unit.PX);
	}
	protected void createFieldUpdaterDiscAmtColumn()
	{
		discountAmt.setFieldUpdater(new FieldUpdater<MaterialRequired, String>()
				{
			@Override

			public void update(int index,MaterialRequired object,String value)
			{

				try{
					Double val1=Double.parseDouble(value.trim());
					object.setDiscountAmt(val1);
					
					productIdCount=object.getPrduct().getCount();
					productSrNoCount = object.getProductSrNo();
					//calculateTotalAmount(object);
					
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);

				}
				catch (NumberFormatException e)
				{

				}
				table.redrawRow(index);
			}
				});
	}

}

