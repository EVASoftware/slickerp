package com.slicktechnologies.client.views.democonfig;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.ListBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.helperlayer.DemoConfigSummary;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar;
import com.slicktechnologies.shared.common.relationalLayer.EmployeeRelation;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class DemoConfigSummaryForm extends FormScreen<DemoConfigSummary> {ObjectListBox<EmployeeRelation> olbownername;
	
	Button btnConfigs,btnCategorys,btnEmployee,btnProducts,btnInventory,btnAsset,btnModules;
	ListBox lbClientType;
	
	/**
	 * Buttons added by Rohan Bhagde 
	 * Date : 21/11/2016
	 * Reason : These buttons are used for distributing load of configs, categories, otherDetails   
	 */
	
		Button btnExtraConfigs,btnExtraCategory,btnExtraOtherEmployeeDetails;
	
	/**
	 * ends here 
	 */
	public  DemoConfigSummaryForm() {
		super();
		createGui();
	}
	
	
	public DemoConfigSummaryForm(String[] processlevel, FormField[][] fields, FormStyle formstyle) 
	{
		super(processlevel, fields, formstyle);
		createGui();
	}

	/**
	 * Method template to initialize the declared variables.
	 */
	private void initalizeWidget()
	{
		btnConfigs=new Button("Configs");
		btnConfigs.getElement().getStyle().setWidth(120, Unit.PX);
		btnCategorys=new Button("Categories");
		btnCategorys.getElement().getStyle().setWidth(120, Unit.PX);
		btnEmployee=new Button("Employee");
		btnEmployee.getElement().getStyle().setWidth(120, Unit.PX);
		btnAsset=new Button("Asset Mgmt");
		btnAsset.getElement().getStyle().setWidth(120, Unit.PX);
		btnProducts=new Button("Products");
		btnProducts.getElement().getStyle().setWidth(120, Unit.PX);
		btnInventory=new Button("Inventory");
		btnInventory.getElement().getStyle().setWidth(120, Unit.PX);
		btnModules=new Button("Modules");
		btnModules.getElement().getStyle().setWidth(120, Unit.PX);
		lbClientType=new ListBox();
		lbClientType.addItem("SELECT");
		lbClientType.addItem("Pest Control");
		lbClientType.addItem("Other");
		
		
		btnExtraConfigs = new Button("Extra Configs");
		btnExtraCategory = new Button("Extra Category");
		btnExtraOtherEmployeeDetails = new Button("Extra ");
	}
	
	/**
	 * method template to create screen formfields
	 */
	@Override
	public void createScreen() {
		
	    
		initalizeWidget();
		
		
		//Token to initialize the processlevel menus.
	
		this.processlevelBarNames=new String[]{AppConstants.SUBMIT,AppConstants.NEW};		
		
		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////
		//Token to initialize formfield
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingConfigInformation=fbuilder.setlabel("Demo Config Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingClientType=fbuilder.setlabel("Client Type").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Client Type",lbClientType);
		FormField flbClientType= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",btnConfigs);
		FormField fbtnConfigs= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",btnCategorys);
		FormField fbtnCategorys= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",btnEmployee);
		FormField fbtnEmployee= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",btnAsset);
		FormField fbtnAsset= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",btnProducts);
		FormField fbtnProducts= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",btnInventory);
		FormField fbtnInventory= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",btnModules);
		FormField fbtnModules= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		FormField[][] formfield = {
		{fgroupingClientType},
		{flbClientType},
		{fgroupingConfigInformation},
		{fbtnConfigs,fbtnCategorys,fbtnEmployee},
		{fbtnAsset,fbtnProducts,fbtnInventory},
		{fbtnModules}
		};
		
		this.fields=formfield;
		}

	/**
	 * method template to update the model with token entity name
	 */
	@Override
	public void updateModel(DemoConfigSummary model) 
	{
		
	presenter.setModel(model);
	}

	
	/**
	 * method template to update the view with token entity name
	 */
	@Override
	public void updateView(DemoConfigSummary view) 
	{

		presenter.setModel(view);
	}

	// Hand written code shift in presenter
	
	
	/**
	 * Toggles the app header bar menus as per screen state
	 */
	
	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals("Search"))
				{
					menus[k].setVisible(false); 
				}
				else
					menus[k].setVisible(false);  		   
				
			}
		}
		
		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
					menus[k].setVisible(false); 
				
				else
					menus[k].setVisible(false);  		   
			}
		}
		
		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit")||text.equals("Search"))
						menus[k].setVisible(false); 
				
				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.DEMOCONFIGURATION,LoginPresenter.currentModule.trim());
	}
	
}