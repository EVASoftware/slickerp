package com.slicktechnologies.client.views.democonfig;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.services.DemoConfigService;
import com.slicktechnologies.client.services.DemoConfigServiceAsync;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.DemoConfigSummary;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.servicerelated.CompanyAsset;

public class DemoConfigSummaryPresenter extends FormScreenPresenter<DemoConfigSummary> {

	//Token to set the concrete FormScreen class name
	DemoConfigSummaryForm form;
	final GenricServiceAsync async=GWT.create(GenricService.class);
	DemoConfigServiceAsync demoAsync = GWT.create(DemoConfigService.class);
	
	
	
	public DemoConfigSummaryPresenter(FormScreen<DemoConfigSummary> view, DemoConfigSummary model) {
		super(view, model);
		form=(DemoConfigSummaryForm) view;
		form.btnConfigs.addClickHandler(this);
		form.btnCategorys.addClickHandler(this);
		form.btnEmployee.addClickHandler(this);
		form.btnAsset.addClickHandler(this);
		form.btnProducts.addClickHandler(this);
		form.btnInventory.addClickHandler(this);
		form.btnModules.addClickHandler(this);
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
	}

	@Override
	public void reactOnPrint() {
		
	}
	
	@Override
	public void reactOnEmail() {
		
		
	}

	/**
	 * Method token to make new model
	 */
	@Override
	protected void makeNewModel() {
		model=new DemoConfigSummary();
	}
	
	public void reactOnDownload()
    {
		
    }
	
	
	public static void initalize()
	{
		AppMemory.getAppMemory().currentState=ScreeenState.NEW;
		DemoConfigSummaryForm  form=new  DemoConfigSummaryForm();
		DemoConfigSummaryPresenter  presenter=new DemoConfigSummaryPresenter(form,new DemoConfigSummary());
		AppMemory.getAppMemory().stickPnel(form);
	}

	@Override
	public void onClick(ClickEvent event) {
		super.onClick(event);
		
		
		if(event.getSource().equals(form.btnConfigs)){
			if(validateClientType()){
				String clientType=form.lbClientType.getItemText(form.lbClientType.getSelectedIndex());
				reactOnConfig(clientType);
			}
			else{
				form.showDialogMessage("Please select client type!");
			}
		}
		
		if(event.getSource().equals(form.btnCategorys)){
			if(validateClientType()){
				String clientType=form.lbClientType.getItemText(form.lbClientType.getSelectedIndex());
				reactOnConfigCategory(clientType);
			}
			else{
				form.showDialogMessage("Please select client type!");
			}
		}
		
		if(event.getSource().equals(form.btnEmployee)){
			if(validateClientType()){
				String clientType=form.lbClientType.getItemText(form.lbClientType.getSelectedIndex());
				reactOnOther(clientType);
			}
			else{
				form.showDialogMessage("Please select client type!");
			}
		}
		
		if(event.getSource().equals(form.btnAsset)){
			if(validateClientType()){
				String clientType=form.lbClientType.getItemText(form.lbClientType.getSelectedIndex());
				reactOnAsset(clientType);
			}
			else{
				form.showDialogMessage("Please select client type!");
			}
		}
		
		if(event.getSource().equals(form.btnProducts)){
			if(validateClientType()){
				String clientType=form.lbClientType.getItemText(form.lbClientType.getSelectedIndex());
				reactOnProducts(clientType);
			}
			else{
				form.showDialogMessage("Please select client type!");
			}
		}
		
		if(event.getSource().equals(form.btnInventory)){
			if(validateClientType()){
				String clientType=form.lbClientType.getItemText(form.lbClientType.getSelectedIndex());
				reactOnInventory(clientType);
			}
			else{
				form.showDialogMessage("Please select client type!");
			}
		}
		
		if(event.getSource().equals(form.btnModules)){
			if(validateClientType()){
				String clientType=form.lbClientType.getItemText(form.lbClientType.getSelectedIndex());
				reactOnDefaultConfigs(clientType);
			}
			else{
				form.showDialogMessage("Please select client type!");
			}
		}
	}
	
	
	/******************************************Config**********************************************/
	
	public void reactOnConfig(final String clientType){
		Company c = new Company();
		MyQuerry query = new MyQuerry();
		Filter filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		query.getFilters().add(filter);
		query.setQuerryObject(new Config());
		
		 async.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onFailure(Throwable caught) {
				}
	
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					
					if(result.size()>0){
						form.showDialogMessage("You cannot click again on Config");
					}
				
					 if(result.size()==0){
						 reactOnDemoConfig(clientType);
					}
				}	
		 });
	}
	
	protected void reactOnDemoConfig(String cType){
		Company c = new Company();
		
			form.showWaitSymbol();
			demoAsync.saveDemoConfig(c.getCompanyId(),cType,new AsyncCallback<Integer>() {
	
				@Override
				public void onFailure(Throwable caught) {
					form.hideWaitSymbol();
					form.showDialogMessage("Unexpected Error Occurred in configs");
				}
				@Override
				public void onSuccess(Integer result) {
					form.showDialogMessage("Config Process Completed Successfully");
					form.hideWaitSymbol();
				}
			});
	}
	
	/************************************* ConfigCategory *********************************************/ 
	
	private void reactOnConfigCategory(final String clientType) {
		Company c = new Company();
		MyQuerry query = new MyQuerry();
		Filter filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		query.getFilters().add(filter);
		query.setQuerryObject(new ConfigCategory());
		
		 async.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onFailure(Throwable caught) {
				}
	
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					
					if(result.size()>0){
						form.showDialogMessage("You cannot click again on ConfigCategory");
					}
				
					 if(result.size()==0){
						 reactOnDemoConfigCategory(clientType);
					}
				}
		 });
	}

	private void reactOnDemoConfigCategory(String cType) {
		Company c = new Company();
		form.showWaitSymbol();
		demoAsync.saveDemoConfigCategory(c.getCompanyId(),cType,new AsyncCallback<Integer>() {

			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
				form.showDialogMessage("Unexpected Error Occurred category");
			}
			@Override
			public void onSuccess(Integer result) {
				form.showDialogMessage("Category Process Completed Successfully");
				form.hideWaitSymbol();
			}
		});
	}	
	/************************************* Other *********************************************/ 
	
	private void reactOnOther(final String clientType) {
		Company c = new Company();
		MyQuerry query = new MyQuerry();
		Filter filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		query.getFilters().add(filter);
		query.setQuerryObject(new Employee());
		
		 async.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onFailure(Throwable caught) {
				}
	
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					
					if(result.size()>0){
						form.showDialogMessage("You cannot click again on Other");
					}
				
					 if(result.size()==0){
						 reactOnDemoOther(clientType);
					}
				}

		 });
	}
	
	private void reactOnDemoOther(String cType) {
		Company c = new Company();
		form.showWaitSymbol();
		demoAsync.saveDemoOtherDetails(c.getCompanyId(),cType,new AsyncCallback<Integer>() {

			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
				form.showDialogMessage("Unexpected Error Occurred");
			}
			@Override
			public void onSuccess(Integer result) {
				form.showDialogMessage("Employee Process Completed Successfully");
				form.hideWaitSymbol();
			}
		});
	}	
	
	/***********************************************Asset*********************************************/
	
	private void reactOnAsset(final String clientType) {
		Company c = new Company();
		MyQuerry query = new MyQuerry();
		Filter filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		query.getFilters().add(filter);
		query.setQuerryObject(new CompanyAsset());
		
		 async.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onFailure(Throwable caught) {
				}
	
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					
					if(result.size()>0){
						form.showDialogMessage("You cannot click again on Asset");
					}
				
					 if(result.size()==0){
						 reactOnDemoAsset(clientType);
					}
				}

		 });
	}
	
	private void reactOnDemoAsset(String cType) {
		Company c = new Company();
		form.showWaitSymbol();
		demoAsync.saveDemoAssetDetails(c.getCompanyId(),cType,new AsyncCallback<Integer>() {

			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
				form.showDialogMessage("Unexpected Error Occurred");
			}
			@Override
			public void onSuccess(Integer result) {
				form.showDialogMessage("Asset Process Completed Successfully");
				form.hideWaitSymbol();
			}
		});
	}	
	
	
	private void reactOnProducts(String clientType) {
		reactOnDemoProducts(clientType);
	}
	
	private void reactOnDemoProducts(String cType) {
		Company c = new Company();
		form.showWaitSymbol();
		demoAsync.saveDemoProductsDetails(c.getCompanyId(),cType,new AsyncCallback<Integer>() {

			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
				form.showDialogMessage("Unexpected Error Occurred");
			}
			@Override
			public void onSuccess(Integer result) {
				form.showDialogMessage("Products Process Completed Successfully");
				form.hideWaitSymbol();
			}
		});
	}	
	
	private void reactOnInventory(final String clientType) {
		Company c = new Company();
		MyQuerry query = new MyQuerry();
		Filter filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		query.getFilters().add(filter);
		query.setQuerryObject(new WareHouse());
		
		 async.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onFailure(Throwable caught) {
				}
	
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					
					if(result.size()>0){
						form.showDialogMessage("You cannot click again on Inventory");
					}
				
					 if(result.size()==0){
						 reactOnDemoInventory(clientType);
					}
				}

		 });
	}
	
	private void reactOnDemoInventory(String cType) {
		Company c = new Company();
		form.showWaitSymbol();
		demoAsync.saveDemoInventoryDetails(c.getCompanyId(),cType,new AsyncCallback<Integer>() {

			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
				form.showDialogMessage("Unexpected Error Occurred");
			}
			@Override
			public void onSuccess(Integer result) {
				form.showDialogMessage("Inventory Process Completed Successfully");
				form.hideWaitSymbol();
			}
		});
	}	
	
	
	private void reactOnDefault(final String clientType) {
		Company c = new Company();
		MyQuerry query = new MyQuerry();
		Filter filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		query.getFilters().add(filter);
		query.setQuerryObject(new ConfigCategory());
		
		 async.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onFailure(Throwable caught) {
				}
	
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					
					if(result.size()>0){
						for(SuperModel configModel:result)
						{
							ConfigCategory configResult=(ConfigCategory)configModel;
							if(configResult.getInternalType()==13){
								form.showDialogMessage("You cannot click again on Default Configs");
							}
							else{
								reactOnDemoInventory(clientType);
							}
						}
					}
				
					 if(result.size()==0){
						 reactOnDemoInventory(clientType);
					}
				}

		 });
	}
	
	private void reactOnDefaultConfigs(String cType) {
		Company c = new Company();
		form.showWaitSymbol();
		demoAsync.saveDefaultConfigs(c.getCompanyId(),cType,new AsyncCallback<Integer>() {

			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
				form.showDialogMessage("Unexpected Error Occurred");
			}
			@Override
			public void onSuccess(Integer result) {
				form.showDialogMessage("Default Config Process Completed Successfully");
				form.btnModules.setEnabled(false);
				form.hideWaitSymbol();
			}
		});
	}
	
	
	protected boolean validateClientType()
	{
		if(form.lbClientType.getSelectedIndex()==0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
}
