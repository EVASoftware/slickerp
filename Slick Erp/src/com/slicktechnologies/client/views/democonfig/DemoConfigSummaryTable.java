package com.slicktechnologies.client.views.democonfig;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.helperlayer.DemoConfigSummary;

public class DemoConfigSummaryTable extends SuperTable<DemoConfigSummary> {
	
	TextColumn<DemoConfigSummary> documentNameColumn;
	TextColumn<DemoConfigSummary> documentIdColumn;
	TextColumn<DemoConfigSummary> statusColumn;

	
	public  DemoConfigSummaryTable() {
		super();
	}
	
	@Override
	public void createTable() {
		addColumngetdocumentName();
		addColumngetdocumentId();
		addColumngetstatus();
	}
	
	public void addColumnSorting(){
		
		addSortinggetdocumentName();
		addSortinggetdocumentId();
		addSortinggetstatus();
	}

	private void addSortinggetdocumentName() {
		List<DemoConfigSummary> list=getDataprovider().getList();
		columnSort=new ListHandler<DemoConfigSummary>(list);
		columnSort.setComparator(documentNameColumn, new Comparator<DemoConfigSummary>()
				{
			@Override
			public int compare(DemoConfigSummary e1,DemoConfigSummary e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getDocumentName()!=null && e2.getDocumentName()!=null){
						return e1.getDocumentName().compareTo(e2.getDocumentName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}

	private void addColumngetdocumentName() {
		documentNameColumn=new TextColumn<DemoConfigSummary>()
				{
			@Override
			public String getValue(DemoConfigSummary object)
			{
				return object.getDocumentName().trim();
			}
				};
				table.addColumn(documentNameColumn,"Document Name");
				table.setColumnWidth(documentNameColumn, 130, Unit.PX);
				documentNameColumn.setSortable(true);
	}
	
	
	
	
	private void addSortinggetdocumentId() {
		List<DemoConfigSummary> list=getDataprovider().getList();
		columnSort=new ListHandler<DemoConfigSummary>(list);
		columnSort.setComparator(documentIdColumn, new Comparator<DemoConfigSummary>()
				{
			@Override
			public int compare(DemoConfigSummary e1,DemoConfigSummary e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getDocumentId()== e2.getDocumentId()){
						return 0;}
					if(e1.getDocumentId()> e2.getDocumentId()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);	
	}
	
	
	private void addColumngetdocumentId() {
		documentIdColumn=new TextColumn<DemoConfigSummary>() {
			
			@Override
			public String getValue(DemoConfigSummary object) {
				if(object.getDocumentId()!=0)
				return object.getDocumentId()+"";
				else
					return "";
			}
		};
		
		table.addColumn(documentIdColumn,"Doc. ID");
		table.setColumnWidth(documentIdColumn, 100, Unit.PX);
		documentIdColumn.setSortable(true);
	}
	
	

	
	private void addSortinggetstatus() {
		List<DemoConfigSummary> list = getDataprovider().getList();
		columnSort = new ListHandler<DemoConfigSummary>(list);
		columnSort.setComparator(statusColumn,new Comparator<DemoConfigSummary>() {
					@Override
					public int compare(DemoConfigSummary e1, DemoConfigSummary e2) {
						if (e1 != null && e2 != null) {
							if (e1.getStatus() == e2.getStatus()) {
								return 0;
							} else {
								return -1;
							}
						} else {
							return 0;
						}
					}
				});
		table.addColumnSortHandler(columnSort);		
	}

	private void addColumngetstatus() {
		statusColumn =new TextColumn <DemoConfigSummary>() {
			
			@Override
			public String getValue(DemoConfigSummary object) {
				if(object.getStatus()!=null)
				return object.getStatus();
				else
					return "";
			}
		};
		
		table.addColumn(statusColumn,"Status");
		table.setColumnWidth(statusColumn, 90, Unit.PX);
		statusColumn.setSortable(true);
	}
	
	@Override
	protected void initializekeyprovider() {
		
	}

	@Override
	public void addFieldUpdater() {
		
	}

	@Override
	public void setEnable(boolean state) {
		
	}

	@Override
	public void applyStyle() {
		
	}


}
