package com.slicktechnologies.client.views.approval;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.code.p.gwtchismes.client.GWTCGlassPanel;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.reusabledata.ConditionDialogBox;
import com.slicktechnologies.client.services.EmailService;
import com.slicktechnologies.client.services.EmailServiceAsync;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.services.SessionService;
import com.slicktechnologies.client.services.SessionServiceAsync;
import com.slicktechnologies.client.services.UpdateService;
import com.slicktechnologies.client.services.UpdateServiceAsync;
import com.slicktechnologies.client.services.XlsxService;
import com.slicktechnologies.client.services.XlsxServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.contract.CancellationPopUp;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.client.views.popups.NewVendorInformationPopUp;
import com.slicktechnologies.client.views.popups.PurchaseRequisitionVendorInfoPopup;
import com.slicktechnologies.client.views.popups.ServiceCancellationPopup;
import com.slicktechnologies.client.views.purchase.purchaserequisition.PurchaseRequisitionForm;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.ApproversRemark;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.common.CustomerTrainingDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.ConcreteBusinessProcess;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Expense;
import com.slicktechnologies.shared.common.businessprocesslayer.LetterOfIntent;
import com.slicktechnologies.shared.common.businessprocesslayer.MultipleExpenseMngt;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseRequisition;
import com.slicktechnologies.shared.common.businessprocesslayer.RequsestForQuotation;
import com.slicktechnologies.shared.common.businessprocesslayer.ServicePo;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cnc.CNC;
import com.slicktechnologies.shared.common.complain.Complain;
import com.slicktechnologies.shared.common.contractcancel.CancelContract;
import com.slicktechnologies.shared.common.deliverynote.DeliveryLineItems;
import com.slicktechnologies.shared.common.deliverynote.DeliveryNote;
import com.slicktechnologies.shared.common.humanresourcelayer.advancelayer.Loan;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveApplication;
import com.slicktechnologies.shared.common.humanresourcelayer.timereport.TimeReport;
import com.slicktechnologies.shared.common.inventory.GRN;
import com.slicktechnologies.shared.common.inventory.Inspection;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.inventory.MaterialMovementNote;
import com.slicktechnologies.shared.common.inventory.MaterialProduct;
import com.slicktechnologies.shared.common.inventory.MaterialRequestNote;
import com.slicktechnologies.shared.common.inventory.PhysicalInventoryMaintaince;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.multilevelapproval.MultilevelApproval;
import com.slicktechnologies.shared.common.multilevelapproval.MultilevelApprovalDetails;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesorder.SalesQuotation;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.workorder.WorkOrder;


public class ApprovalTable extends SuperTable<Approvals> implements ClickHandler{
	
	/**
	 * rohan added this on Date : 07/04/2017
	 * User for making RPC to check wether session is active or not
	 */
	private final static SessionServiceAsync sessionService=GWT.create(SessionService.class);
	
	TextColumn<Approvals> businessprocesstypeCol;
	TextColumn<Approvals> businessprocessidCol;
	TextColumn<Approvals> requestedbyCol;
	TextColumn<Approvals> requestdateCol;
	TextColumn<Approvals> statusCol;
	TextColumn<Approvals> branchCol;
	TextColumn<Approvals> approverNameCol;
	TextColumn<Approvals> requestCol;
	TextColumn<Approvals>remarkCol;
	TextColumn<Approvals> approvalOrRejectDateCol;
	/**@Sheetal : 08-03-2022 , Adding print button column**/
	Column<Approvals, String> print;
	Column<Approvals, String> approve;
	Column<Approvals, String> reject;
	TextColumn<Approvals> levelCol;
	
	public static  ApprovalForm form;
	String docType="";
	int pestoIndia=0,vCare=0,omPestInvoice=0,hygeia=0,reddysPestControl=0;
	int customQuotation=0,pestQuotation=0,nbhc=0,universal=0,vcare=0,genesis=0;
	Contract con= null;
	Quotation quotation=null;
	Invoice invoiceEntity=null;
	PopupPanel conditionpanel;
	ConditionDialogBox conditionPopup=new ConditionDialogBox("Do you want to print on preprinted Stationery ? ",AppConstants.YES,AppConstants.NO);
	SuperModel model = null;
	String identify="";
	String markStatus="";
	Date today= new Date();
	int rowIndex=0; 
	int approvalLevelValue=0;
	ApprovalScreenPopup popup=new ApprovalScreenPopup();
	Approvals app=null;
	PopupPanel panel ;
	final GenricServiceAsync async = GWT.create(GenricService.class);
	public  ArrayList<Employee> approvalnameList;
	public  List<String> binList;
	
	int level=0;
	
	// vijay added this for rejection of document 
	ApprovalScreenPopup popupapproval=new ApprovalScreenPopup("for reject");
	
	EmailServiceAsync emailService=GWT.create(EmailService.class);
	
	protected GWTCGlassPanel glassPanel;
	
	/**
	 * This the Remark History Button
	 * Date : 14-10-2016 By Anil
	 * Release : 30 Sept 2016 
	 * Project : PURCHASE MODIFICATION (NBHC)
	 */
	Column<Approvals, String> remarkHisBtnColumn;
	ApproversRemarkHistoryPopup remarkPopup=new ApproversRemarkHistoryPopup();
	
	/**
	 * ENd
	 */
	/******************************************Qty Logic*******************************************/
	
	static double qty=0;
	static int prodID;
	static String wareHouse="";
	static String storageloc="";
	static String storagebin="";
	String doctype="";
	String transDirection="";
	int docID;
	int flag=0;
	int cnt=0;
	int i=0;
	
	/*********************************************************************************************/
	/**
	 * Date : 17-05-2017 By ANIL
	 * This field stores the total value of particular document.
	 */
	Double totalAmount;
	/**
	 * End
	 */
	
	
	
	
	/**
	 *  nidhi
	 *  7-10-2017
	 *  for display name of customer, and id
	 */
	TextColumn<Approvals>bpId;
	TextColumn<Approvals>bpName;
	/**
	 *  end
	 */
   //A PATCH SO THAT Table can call Presenter specifec methods
	ApprovalPresenter presenter;
	boolean validDate  = true;
	
	/**
	 * Date 12-01-2019 By Vijay
	 * Des :- for NBHC Inventory Management Vendor Confirmation local or centralised
	 */
	Column<Approvals, String> confirmVendor;
	PurchaseRequisitionVendorInfoPopup prVendorInfoPopup = new PurchaseRequisitionVendorInfoPopup();
	GeneralServiceAsync generalAsync = GWT.create(GeneralService.class);
	/**
	 * ends here
	 */
	/**
	 * Date 31-01-2019 by Vijay
	 * Des :- for NBHC Inventory Management Revised button for PR option added
	 */
	Column<Approvals, String> revisedDoc;
	GeneralViewDocumentPopup genralInvoicePopup;
	public static PopupPanel generalPanel;
	/**
	 * ends here
	 */
	
	/*** Date 26-03-2019 by Vijay for NBHC CCPM Contract Discontinue Approval *****/
	UpdateServiceAsync update = GWT.create(UpdateService.class);
	CancellationPopUp cancellationPopup = new CancellationPopUp();

	/*** Date 30-08-2019 by Vijay 
	 * Des :- NBHC Inventory Management PR Vendor Information
	 */
	Column<Approvals, String> columnViewVendor;
	NewVendorInformationPopUp viewVendorInfoPopup = new NewVendorInformationPopUp();
	
	ServiceCancellationPopup  cancellation = new ServiceCancellationPopup();
	
	
	public ApprovalTable()
	{
		super();
		addFieldUpdater();
		popup.getBtnOk().addClickHandler(this);
		popup.getBtnCancel().addClickHandler(this);
		
		//vijay
		popupapproval.getBtnOk().addClickHandler(this);
		popupapproval.getBtnCancel().addClickHandler(this);
		setHeight("800px");
		
		/**
		 * Date 13-01-2019 by vijay for NBHC Inventory Management PR confirm Vendor
		 */
		prVendorInfoPopup.getLblOk().addClickHandler(this);
		prVendorInfoPopup.getLblCancel().addClickHandler(this);
		/**
		 * ends here
		 */
		
		/*** Date 26-03-2019 by Vijay for NBHC CCPM Contract Discontinue Approval *****/
		cancellationPopup.getLblOk().addClickHandler(this);
		cancellationPopup.getLblCancel().addClickHandler(this);
		
		cancellation.getLblOk().addClickHandler(this);
		cancellation.getLblCancel().addClickHandler(this);
		
		conditionPopup.getBtnOne().addClickHandler(this);
		conditionPopup.getBtnTwo().addClickHandler(this);
	}
	
	private void addPrintBtnColumn(){
		ButtonCell btnCell= new ButtonCell();
		print= new Column<Approvals, String>(btnCell){

			@Override
			public String getValue(Approvals object) {
				
				return "Print";
			}
			
		};
		table.addColumn(print,"Print");
		table.setColumnWidth(print, 90, Unit.PX);
	}
	private void setFieldUpdatorOnPrintBtn(){
		print.setFieldUpdater(new FieldUpdater<Approvals, String>(){

			@Override
			public void update(int index, Approvals object, String value) {
				docType=object.getBusinessprocesstype();
				Console.log("Inside PRINT button : "+docType);
				printSelectedDocument(object);
				
			}
			
		});
	}
	private void addRemarkHisBtnColumn(){
		ButtonCell btnCell= new ButtonCell();
		remarkHisBtnColumn= new Column<Approvals, String>(btnCell) {
			@Override
			public String getValue(Approvals object) {
				return "Remark History";
			}
			
			@Override
			public void render(Context context, Approvals object,SafeHtmlBuilder sb) {
				if(object.getRemarkList()!=null&&object.getRemarkList().size()!=0){
					super.render(context, object, sb);
				}
			}
		};
		table.addColumn(remarkHisBtnColumn,"");
		table.setColumnWidth(remarkHisBtnColumn, 150, Unit.PX);
		
	}
	
	
	private void setFieldUpdatorOnRemarkHisBtn(){
		remarkHisBtnColumn.setFieldUpdater(new FieldUpdater<Approvals, String>() {
			@Override
			public void update(int index, Approvals object, String value) {
				System.out.println("REMARK CLICKED");
				panel=new PopupPanel(true);
				remarkPopup.clear();
				if(object.getRemarkList()!=null&&object.getRemarkList().size()!=0){
					remarkPopup.remarkTbl.getDataprovider().setList(object.getRemarkList());
				}else{
					ApprovalPresenter.showMessage("No Remark History !");
					return;
				}
				panel.add(remarkPopup);
				panel.show();
				panel.center();
				
			}
		});
	}
	
	
	
	private void addColumnApprove()
	{
		 
		ButtonCell btnCell= new ButtonCell();
		approve = new Column<Approvals, String>(btnCell) {

			@Override
			public String getValue(Approvals object) {
				if(object.getStatus().equals("Approved")){
//					return "Approved";
					return "";
				}else if(object.getStatus().equals("Rejected")){
					return "";
					}
				else if(object.getStatus().equals("Cancelled")){
					return "Cancelled";
				}
				else{
					return "Approve";
				}
			}
			
			@Override
			public void render(Context context, Approvals object,SafeHtmlBuilder sb) {
				if(object.getStatus().equals(Approvals.PENDING)
						||object.getStatus().equals(ConcreteBusinessProcess.APPROVED)
						||object.getStatus().equals(ConcreteBusinessProcess.CANCELLED)){
					super.render(context, object, sb);
				}
			}
		};
		
		table.addColumn(approve,"Approve");
		table.setColumnWidth(approve, 100, Unit.PX);
	}
      
	private void setFieldUpdaterOnApprove()
	{


		approve.setFieldUpdater(new FieldUpdater<Approvals, String>() {
		
		@Override
		public void update(int index, Approvals object, String value) {
			
			/**
			 * Date 13-01-2019 by Vijay for Inventory Management NBHC for local vendor or HO vendor
			 */
			app=object;
			System.out.println("object.getDocumentValidation() =="+object.getDocumentValidation());
			if(businessprocesstypeCol.getValue(object).equals(AppConstants.PURCHASEREQUISION)){
				if( LoginPresenter.automaticPROrderQtyFlag &&
						object.getApprovalLevel()==getProcessConfigWithApprovalLevel()  && object.getDocumentValidation()==false){
					ApprovalPresenter.showMessage("Please Confirm Vendor first!");
					return;
				}
			}
			/**
			 * ends here
			 */
			
			
			popup.getBusinessprocesstypeCol().setValue(businessprocesstypeCol.getValue(object));
			popup.getBusinessprocessidCol().setValue(businessprocessidCol.getValue(object));
//			app=object;
			
			System.out.println("out validate condition");
			
			updateSessionLastAccessedTimeInApprove("Approve",index,object);
		}
		
	});
	}
	
	private void addColumnReject()
	{
		 
		ButtonCell btnCell= new ButtonCell();
		reject = new Column<Approvals, String>(btnCell) {

			@Override
			public String getValue(Approvals object) 
			{
				if(object.getStatus().equals("Approved")){
					return "";
				}
				else if(object.getStatus().equals("Rejected")){
					return "Rejected";
				}
				else if(object.getStatus().equals("Cancelled")){
					return "";
				}
				else{
					return "Reject";
				}
			}
			
			
			@Override
			public void render(Context context, Approvals object,SafeHtmlBuilder sb) {
				if(object.getStatus().equals(Approvals.PENDING)
						||object.getStatus().equals(ConcreteBusinessProcess.REJECTED)){
					super.render(context, object, sb);
				}
			};
		};
		
		table.addColumn(reject,"Reject");
		table.setColumnWidth(reject, 100, Unit.PX);
	}
      
	private void setFieldUpdaterOnReject()
	{
		reject.setFieldUpdater(new FieldUpdater<Approvals, String>() {
			
			@Override
			public void update(int index, Approvals object, String value) {
				
				popupapproval.getBusinessprocesstypeCol().setValue(businessprocesstypeCol.getValue(object));
				popupapproval.getBusinessprocessidCol().setValue(businessprocessidCol.getValue(object));
				
				
				//*************rohan remove this lines for rejection in approval ****************
				
//				popup.getApprovalname().setVisible(false);
//				popup.getApprovalName().setVisible(false);
//				popup.getMsgapprovalname().setVisible(false);
				
				
				panel=new PopupPanel(true);
				panel.add(popupapproval);
				panel.show();
				panel.center();
				app=object;
				identify="REJECTED";
				markStatus=object.getStatus();
				
				/**
				 * Date: 15-05-2017 By ANIL
				 */
				doctype=object.getBusinessprocesstype();
				docID=object.getBusinessprocessId();
				/**
				 * END
				 */
				
				if(markStatus.equals("Approved")||markStatus.equals("Rejected")||markStatus.equals("Cancelled"))
				{
					ApprovalPresenter.showMessage("This Request Is Already "+markStatus);
					panel.hide();
				}
				
			}
		});
	}
	
	private void createColumnBusinessProcessType()
	{
	  businessprocesstypeCol =  new TextColumn<Approvals>()
  		  {
       @Override
       public String getValue(Approvals object) 
       {
          return object.getBusinessprocesstype();
       }
    };
    table.addColumn(businessprocesstypeCol, "Doc Type");
    businessprocesstypeCol.setSortable(true);
    table.setColumnWidth(businessprocesstypeCol, 110, Unit.PX);
   
   }

	private void createColumnBusinessprocessId()
	{
		businessprocessidCol=new TextColumn<Approvals>() {

			@Override
			public String getValue(Approvals object) {
				
				return object.getBusinessprocessId()+"";
			}
		};
		table.addColumn(businessprocessidCol,"Doc Id");
		businessprocessidCol.setSortable(true);
		table.setColumnWidth(businessprocessidCol, 100, Unit.PX);
	}
	
	private void createColumnRequestedBy()
	{
		requestedbyCol=new TextColumn<Approvals>() {

			@Override
			public String getValue(Approvals object) {
				
				return object.getRequestedBy();
			}
			
		};
		table.addColumn(requestedbyCol,"Requested By");
		requestedbyCol.setSortable(true);
		table.setColumnWidth(requestedbyCol,120, Unit.PX);
	}
	
	private void createColumnBranch()
	{
		branchCol=new TextColumn<Approvals>() {

			@Override
			public String getValue(Approvals object) {
				
				return object.getBranchname();
			}
			
		};
		table.addColumn(branchCol,"Branch");
		branchCol.setSortable(true);
		table.setColumnWidth(branchCol, 100, Unit.PX);
	}
	
	private void createColumnStatus()
	{
		statusCol=new TextColumn<Approvals>() {

			@Override
			public String getValue(Approvals object) {
				
				return object.getStatus();
			}
			
		};
		table.addColumn(statusCol,"Status");
		statusCol.setSortable(true);
		table.setColumnWidth(statusCol, 100, Unit.PX);
	}
	
	private void createColumnApproverName()
	{
		approverNameCol=new TextColumn<Approvals>() {

			@Override
			public String getValue(Approvals object) {
				
				return object.getApproverName();
			}
		};
		table.addColumn(approverNameCol,"Approver Name");
		approverNameCol.setSortable(true);
		table.setColumnWidth(approverNameCol,120, Unit.PX);
	}
	
	private void createColumnRequestDate()
	{
		requestdateCol =new TextColumn<Approvals>() {

			@Override
			public String getValue(Approvals object) {
				if(object.getCreationDate()!=null){
					return  AppUtility.parseDate(object.getCreationDate());
				}
				else {
					return ""; 
				}
			}
		};
		table.addColumn(requestdateCol,"Requested Date");
		requestdateCol.setSortable(true);
		table.setColumnWidth(requestdateCol, 110, Unit.PX);
	}
	
		private void addColumnRemark() {
		
		remarkCol=new TextColumn<Approvals>() {

			@Override
			public String getValue(Approvals object) {
				
				return object.getRemark();
			}
		};
		table.addColumn(remarkCol,"Remark");
		remarkCol.setSortable(true);
		table.setColumnWidth(remarkCol, 110, Unit.PX);
			
		}
		
		private void addColumnApproveOrRejectDate() {
			
			approvalOrRejectDateCol =new TextColumn<Approvals>() {

				@Override
				public String getValue(Approvals object) {
					if(object.getApprovalOrRejectDate()!=null){
						return AppUtility.parseDate(object.getApprovalOrRejectDate());
					}
					else{
						return "";
					}
				}
			};
			table.addColumn(approvalOrRejectDateCol,"Approved/Reject Date");
			approvalOrRejectDateCol.setSortable(true);
			table.setColumnWidth(approvalOrRejectDateCol, 150, Unit.PX);
		}
		
		private void createColumnLevel() {

			levelCol=new TextColumn<Approvals>() {

				@Override
				public String getValue(Approvals object) {
					
					return object.getApprovalLevel()+"";
				}
			};
			table.addColumn(levelCol,"Approver Level");
			levelCol.setSortable(true);
			table.setColumnWidth(levelCol,120, Unit.PX);
			
			
		}

		
	@Override
	public void createTable() {
		
		boolean approvalFlag;
		addPrintBtnColumn();
		addColumnApprove();
		addColumnReject();
		/**
		 * Date 13-01-2019 by vijay for Inventory management
		 */
		if(LoginPresenter.automaticPROrderQtyFlag){
			
			approvalFlag=false;
			addColumnRevisedDoc(approvalFlag);
			addcloumnConfirmVendor();
			addcolumnViewVendor();
		}
		
		/***16-01-2020 Deepak Salve added this code for uploaded PR Edit***/ 
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("PurchaseRequisition","PReditableonPRUpload")){
			approvalFlag=true;
			addColumnRevisedDoc(approvalFlag);
		}
		/***End***/
		
		
		/**
		 * ends here
		 */
		createColumnBusinessProcessType();
		createColumnBusinessprocessId();
		/**
		 *  nidhi
		 *  7-10-2017
		 *  for display id and name
		 */
	
		createColumnBpName();
		/**
		 *  end
		 */
		createColumnRequestedBy();
		createColumnRequestDate();
		createColumnApproverName();
		createColumnBranch();
		createColumnStatus();
		createColumnLevel();
//		addColumnRemark();
		addColumnApproveOrRejectDate();
		/**
		 *  nidhi
		 *  7-10-2017
		 *  for display id and name
		 */
	
		createColumnBpId();
		/**
		 *  end
		 */
		addRemarkHisBtnColumn();
		
//		addColumnDelete();
//		addColumnReject();
		
	}





	



	@Override
	protected void initializekeyprovider() {
		
		
	}

	@Override
	public void addFieldUpdater() {
		setFieldUpdatorOnPrintBtn();
		setFieldUpdaterOnApprove();
		setFieldUpdaterOnReject();
		setFieldUpdatorOnRemarkHisBtn();
	}
	
	public void addColumnSorting() {
		
		addColumnBusinessProcessTypeSorting();
		addColumnBusinessprocessIdSorting();
		/**
		 *  nidhi
		 *  7-10-2017
		 *  sorting apply
		 */
		addColumnBpNameSorting();
		/**
		 * end
		 */
		addColumRequestedBySorting();
		addColumnApproverNameSorting();
		addColumnBranchSorting();
		addColumnStatusSorting();
		addColumnRequestDate();
		/**
		 *  nidhi
		 *  7-10-2017
		 *  sorting apply
		 */
		addColumnBpIdSorting();
		/**
		 * end
		 */
		addColumnApprovalOrRejDateSorting();
		
		/**
		 * Date : 03-03-2017 By ANil
		 */
		addSortingOnApprovalCol();
	}
	
	/**
	 * Date : 03-03-2017 By ANil
	 */
	private void addSortingOnApprovalCol() {
		List<Approvals> list = getDataprovider().getList();
		columnSort = new ListHandler<Approvals>(list);
		columnSort.setComparator(levelCol,new Comparator<Approvals>() {
			@Override
			public int compare(Approvals e1, Approvals e2) {
				if (e1 != null && e2 != null) {
					if (e1.getApprovalLevel() == e2.getApprovalLevel()) {
						return 0;
					}
					if (e1.getApprovalLevel() > e2.getApprovalLevel()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}
	
	
		private void addColumnRequestDate() {
				
				List<Approvals> list=getDataprovider().getList();
				columnSort=new ListHandler<Approvals>(list);
				columnSort.setComparator(requestdateCol, new Comparator<Approvals>()
						{
					@Override
					public int compare(Approvals e1,Approvals e2)
					{
						if(e1!=null && e2!=null)
						{
							if( e1.getCreationDate()!=null && e2.getCreationDate()!=null){
								return e1.getCreationDate().compareTo(e2.getCreationDate());}
						}
						else{
							return 0;}
						return 0;
					}
						});
				table.addColumnSortHandler(columnSort);
				
			}
		
			private void addColumnStatusSorting() {
				
				List<Approvals> list=getDataprovider().getList();
				columnSort=new ListHandler<Approvals>(list);
				columnSort.setComparator(statusCol, new Comparator<Approvals>()
						{
					@Override
					public int compare(Approvals e1,Approvals e2)
					{
						if(e1!=null && e2!=null)
						{
							if( e1.getStatus()!=null && e2.getStatus()!=null){
								return e1.getStatus().compareTo(e2.getStatus());}
						}
						else{
							return 0;}
						return 0;
					}
						});
				table.addColumnSortHandler(columnSort);
				
			}
			
			private void addColumnBranchSorting() {
				
				List<Approvals> list=getDataprovider().getList();
				columnSort=new ListHandler<Approvals>(list);
				columnSort.setComparator(branchCol, new Comparator<Approvals>()
						{
					@Override
					public int compare(Approvals e1,Approvals e2)
					{
						if(e1!=null && e2!=null)
						{
							if( e1.getBranchname()!=null && e2.getBranchname()!=null){
								return e1.getBranchname().compareTo(e2.getBranchname());}
						}
						else{
							return 0;}
						return 0;
					}
						});
				table.addColumnSortHandler(columnSort);
			}
			
			private void addColumnApproverNameSorting() {
				
				List<Approvals> list=getDataprovider().getList();
				columnSort=new ListHandler<Approvals>(list);
				columnSort.setComparator(approverNameCol, new Comparator<Approvals>()
						{
					@Override
					public int compare(Approvals e1,Approvals e2)
					{
						if(e1!=null && e2!=null)
						{
							if( e1.getApproverName()!=null && e2.getApproverName()!=null){
								return e1.getApproverName().compareTo(e2.getApproverName());}
						}
						else{
							return 0;}
						return 0;
					}
						});
				table.addColumnSortHandler(columnSort);
			
			}
				
			private void addColumRequestedBySorting() {
				
				List<Approvals> list=getDataprovider().getList();
				columnSort=new ListHandler<Approvals>(list);
				columnSort.setComparator(requestedbyCol, new Comparator<Approvals>()
						{
					@Override
					public int compare(Approvals e1,Approvals e2)
					{
						if(e1!=null && e2!=null)
						{
							if( e1.getRequestedBy()!=null && e2.getRequestedBy()!=null){
								return e1.getRequestedBy().compareTo(e2.getRequestedBy());}
						}
						else{
							return 0;}
						return 0;
					}
						});
				table.addColumnSortHandler(columnSort);
				
			}
			
			private void addColumnBusinessprocessIdSorting() {
				
				List<Approvals> list=getDataprovider().getList();
				columnSort=new ListHandler<Approvals>(list);
				columnSort.setComparator(businessprocessidCol, new Comparator<Approvals>()
						{
					@Override
					public int compare(Approvals e1,Approvals e2)
					{
						if(e1!=null && e2!=null)
						{
							if(e1.getBusinessprocessId()== e2.getBusinessprocessId()){
								return 0;}
							if(e1.getBusinessprocessId()> e2.getBusinessprocessId()){
								return 1;}
							else{
								return -1;}
						}
						else{
							return 0;}
					}
						});
				table.addColumnSortHandler(columnSort);
			
			}
			
			private void addColumnBusinessProcessTypeSorting() {
				
				List<Approvals> list=getDataprovider().getList();
				columnSort=new ListHandler<Approvals>(list);
				columnSort.setComparator(businessprocesstypeCol, new Comparator<Approvals>()
						{
					@Override
					public int compare(Approvals e1,Approvals e2)
					{
						if(e1!=null && e2!=null)
						{
							if( e1.getBusinessprocesstype()!=null && e2.getBusinessprocesstype()!=null){
								return e1.getBusinessprocesstype().compareTo(e2.getBusinessprocesstype());}
						}
						else{
							return 0;
							}
						return 0;
					}
						});
				table.addColumnSortHandler(columnSort);
				
			}
			
			private void addColumnApprovalOrRejDateSorting() {
				
				List<Approvals> list=getDataprovider().getList();
				columnSort=new ListHandler<Approvals>(list);
				columnSort.setComparator(approvalOrRejectDateCol, new Comparator<Approvals>()
				{
					@Override
					public int compare(Approvals e1,Approvals e2)
					{
						if(e1!=null && e2!=null)
						{
							if( e1.getApprovalOrRejectDate()!=null && e2.getApprovalOrRejectDate()!=null){
								return e1.getApprovalOrRejectDate().compareTo(e2.getApprovalOrRejectDate());}
						}
						else{
							return 0;}
						return 0;
					}
						});
				table.addColumnSortHandler(columnSort);
			}
			
			
			
			
		
			
	@Override
	public void setEnable(boolean state) {
		
		
	}

	@Override
	public void applyStyle() {
		
		
	}
	
	
	public ApprovalPresenter getPresenter() {
		return presenter;
	}
	
	
	public void setPresenter(ApprovalPresenter presenter) {
		this.presenter = presenter;
	}
	
	
	@Override
	public void onClick(ClickEvent event) {

		if(event.getSource()==popup.getBtnOk()){
			
			if(identify.equals(AppConstants.APPROVALBTN))
			{
				boolean flagApproval=validateMultiLevelApproval(doctype,app);
			if(flagApproval==true)
			{
				ArrayList<MultilevelApproval> multiLevelApprovalLis=LoginPresenter.globalMultilevelApproval;
				List<MultilevelApprovalDetails> multiLevelTableLis=new ArrayList<MultilevelApprovalDetails>();
				
				for(int d=0;d<multiLevelApprovalLis.size();d++)
				{
					if(multiLevelApprovalLis.get(d).getDocumentType().trim().equals(app.getBusinessprocesstype().trim())&&multiLevelApprovalLis.get(d).isStatus()==true)
					{
						multiLevelTableLis=multiLevelApprovalLis.get(d).getApprovalLevelDetails();
					}
				}
				
				/**
				 * Added by Anil On 30-08-2016
				 * get max level of approval
				 */
				int maxLevel=0;
				int temp=0;
				for(int i=0;i<multiLevelTableLis.size();i++){
					if(i==0){
						maxLevel=Integer.parseInt(multiLevelTableLis.get(i).getLevel());
					}else{
						temp=Integer.parseInt(multiLevelTableLis.get(i).getLevel());
						if(temp>maxLevel){
							maxLevel=temp;
						}
					}
				}
				
				Console.log("MAX LEVEL APPROVAL "+maxLevel);
				
				/**
				 * Date : 17-05-2017 BY ANIL
				 */
				boolean amtLvlAppFlag=false;
				for(MultilevelApprovalDetails obj:multiLevelTableLis){
					Console.log("BTN OK AMT MUL lvl "+Integer.parseInt(obj.getLevel().trim())+" DOC TYPE lvl "+app.getApprovalLevel());
					int lvl=Integer.parseInt(obj.getLevel().trim());
					if(lvl==app.getApprovalLevel()&&obj.getAmountUpto()!=null){
						amtLvlAppFlag=true;
						break;
					}
				}
				Console.log("BTN OK AMT MUL FLAF "+amtLvlAppFlag+" DOC TYPE "+popup.getValidApprovalDocument());
				if(popup.getValidApprovalDocument()&&amtLvlAppFlag){
					Console.log("BTN OK For Valid Document amount lvl approval process ");
					boolean maxLvlFlag=false;
					for(MultilevelApprovalDetails obj:multiLevelTableLis){
						/**
						 * @author Vijay Chougule Date 01-12-2018 
						 * Des : - if amount upto is not defined then user unable to approve the document  due to exception
						 * so i have updated the code with !=null check
						 * @author Anil @since 13-08-2021
						 * Compared approver name with approvers list condition
						 */
						if(obj.getLevel().equals(app.getApprovalLevel()+"")&&obj.getAmountUpto()!=null&&obj.getAmountUpto()>=totalAmount&&app.getApproverName().equals(obj.getEmployeeName())){
							maxLvlFlag=true;
							maxLevel=app.getApprovalLevel();
							break;
						}
					}
				}
				
				/**
				 * End
				 */
					
				Console.log("AFTER MAX LEVEL APPROVAL "+maxLevel);
				
				
//				int maxLevel=Integer.parseInt(multiLevelTableLis.get(multiLevelTableLis.size()-1).getLevel());
				
				if(app.getApprovalLevel()==maxLevel){
					Console.log("Approver level AND max level is same ");
					popup.getApprovalName().setEnabled(false);
					popup.getMsgapprovalname().setVisible(false);
				}
				
				if(popup.approvalName.getSelectedIndex()==0&&app.getApprovalLevel()!=maxLevel){
					ApprovalPresenter.showMessage("Please Fill Approver Name for next level");
				}
				else
				{
					if(app.getApprovalLevel()==maxLevel)
					{
						Console.log("Approver level AND max level is same 1");
						approvalOkButton();
					}
					else
					{
						Approvals approval=new Approvals();
						approval.setApproverName(popup.getApprovalName().getItemText(popup.getApprovalName().getSelectedIndex()));
						approval.setBranchname(app.getBranchname());
						approval.setBusinessprocessId(app.getBusinessprocessId());
						//Date 01-05-2017 commented by vijay becs no need this. this storing date only we needed time for NBHC Deadstock automatic approval cron job also so creation date code added in onsave method
//						approval.setCreationDate(new Date());
						approval.setRequestedBy(app.getApproverName());
						approval.setApprovalLevel(app.getApprovalLevel()+1);
						approval.setStatus(Approvals.PENDING);
						approval.setBusinessprocesstype(app.getBusinessprocesstype());
						
						// vijay
						approval.setPersonResponsible(app.getPersonResponsible());
						
						/**
						 * Date 02-03-2017
						 * added by vijay for seting in aproval entity for next level document created by
						 */
						if(app.getDocumentCreatedBy()!=null)
							approval.setDocumentCreatedBy(app.getDocumentCreatedBy());
						/**
						 * ends here
						 */

//						approval.setCreatedBy();
						
						/**
						 * Setting remark list in approval entity
						 * Date : 14-10-2016 By Anil
						 * Release : 30 Sept 2016
						 * Project : Purchase Modification(nbhc)
						 */
						if(!popup.remark.getValue().equals("")){
							ArrayList<ApproversRemark> list=new ArrayList<ApproversRemark>();
							ApproversRemark apprem=new ApproversRemark();
							apprem.setApproverLevel(app.getApprovalLevel());
							apprem.setApproverName(app.getApproverName());
							apprem.setApproverRemark(popup.remark.getValue());
							list.add(apprem);
							app.setRemarkList(list);
							approval.setRemarkList(app.getRemarkList());
							
						}else{
							/**
							 * Date:- 2-03-2017
							 * added by vijay
							 * for if approver remark didnt entered then i am saving approver name and its level in remark list for sending email
							 */
							ArrayList<ApproversRemark> list=new ArrayList<ApproversRemark>();
							ApproversRemark apprem=new ApproversRemark();
							apprem.setApproverLevel(app.getApprovalLevel());
							apprem.setApproverName(app.getApproverName());
							list.add(apprem);
							app.setRemarkList(list);
							approval.setRemarkList(app.getRemarkList());
							/**
							 * ends here vijay
							 */
						}
						/**
						 * End
						 * 
						 */
						
						
						/**
						 * Date 24-04-2017 added by vijay for Deadstock for NBHC
						 * when 1 st level approver approved the documnet then if same person is for 
						 * 2 and 3 level approver then directly request must sent to  4  level
						 */
						 
						System.out.println("Approver list for PR =="+multiLevelTableLis.size());
						boolean checkProcesssConfig =  AppUtility.checkForProcessConfigurartionIsActiveOrNot("MultilevelApproval", "SamePersonNextLevelApproval");
						if(checkProcesssConfig){
							String approverName = popup.getApprovalName().getItemText(popup.getApprovalName().getSelectedIndex());
							if(app.getApproverName().equals(approverName)){
								int level = approval.getApprovalLevel();
								int atlastlevel = checkNextLevel(approval.getApprovalLevel(),approverName, multiLevelTableLis);
								ArrayList<ApproversRemark> list =  new ArrayList<ApproversRemark>();
								for(int i=level;i<atlastlevel;i++){
									ApproversRemark apprem=new ApproversRemark();
									apprem.setApproverLevel(i);
									apprem.setApproverName(approval.getRemarkList().get(0).getApproverName());
									apprem.setApproverRemark(approval.getRemarkList().get(0).getApproverRemark());
									list.add(apprem);
								}
								for(int i=0;i<multiLevelTableLis.size();i++){
									if(Integer.parseInt(multiLevelTableLis.get(i).getLevel()) == atlastlevel){
										approverName = multiLevelTableLis.get(i).getEmployeeName();
									}
								}
								
								approval.setRemarkList(list);
								approval.setApprovalLevel(atlastlevel);
								approval.setApproverName(approverName);
							}
						}
						
						/**
						 * ends here
						 */
						/** Vijay on 13-01-2019 for NBHC Inventory Management **/
						approval.setDocumentValidation(app.getDocumentValidation());
						saveApprovalReq(approval,ConcreteBusinessProcess.REQUESTED,"Request Sent!");
						
						/** Date :- 19-sep-2016
						 * Release Date - 30-sep-2016
						 * by - vijay 
						 * Description :- here i am added code for getting all approver level name.
						 * In toEmailEmployeeList added who is approving the document and
						 * in ccEmailEmployeeList added remaining all approver level name and requested by and person responsible
						 * To send Emails for approved document TO approver and CC to all approver level CC - Requested by CC - person responsible
						 * 
						 */
						ArrayList<String> toEmailEmployeeList = new ArrayList<String>();
						ArrayList<String> ccEmailEmployeeList = new ArrayList<String>();
						String loggedinuserApprover = UserConfiguration.getInfo().getFullName();
						System.out.println("hi vijay === logged in user ==="+loggedinuserApprover);
						
						if(!loggedinuserApprover.equals("")){
							toEmailEmployeeList.add(loggedinuserApprover);
							System.out.println("TO == for Approved === email id"+loggedinuserApprover);
							Console.log("for  approval To"+loggedinuserApprover);
							/**
							 * vijay commented because here we adding all approver in cc no need this  with new change in cc adding last all approver all name
							 */
//							for(int i=0;i<multiLevelTableLis.size();i++){
//								if(!loggedinuserApprover.equals(multiLevelTableLis.get(i).getEmployeeName())){
//									ccEmailEmployeeList.add(multiLevelTableLis.get(i).getEmployeeName());
//									System.out.println("CC == for Approved === email id"+multiLevelTableLis.get(i).getEmployeeName());
//									Console.log("for  approval CC"+multiLevelTableLis.get(i).getEmployeeName());
//								}
//							}
							/**
							 * ends here
							 */
						}
						Console.log("Appoved document ==");
						Console.log("Sales person === "+app.getPersonResponsible());
						Console.log("Requested By === "+app.getRequestedBy());
						Console.log("Created By === "+app.getDocumentCreatedBy());

						//vijay added if contion on 02-03-2017
						if(!loggedinuserApprover.equals(app.getRequestedBy())){
							ccEmailEmployeeList.add(app.getRequestedBy());
						}
						if(!loggedinuserApprover.equals(app.getPersonResponsible())){
							ccEmailEmployeeList.add(app.getPersonResponsible());
						}
						
						/**Date 2-03-2017
						 * added by vijay for adding cc in last all approver level names
						 */
						
						/**
						 * Date 28-04-2017 commented by vijay for this is no need deadstock release - II NBHC  
						 * cc will be last requester and purchase engineer and document created and To for Approver Name
						 */
						
//						if(app.getRemarkList().size()!=0){
//							for(int i=0;i<app.getRemarkList().size();i++){
//								if(!loggedinuserApprover.equals(app.getRemarkList().get(i).getApproverName()))
//								ccEmailEmployeeList.add(app.getRemarkList().get(i).getApproverName());
//							}
//						}
						/**
						 * ends here
						 */
//						app.getRemarkList()
						
						/**
						 * Added By Anil Date : 14-10-2016
						 * Release : 30 Sept 2016 
						 * Project : PURCHASE MODIFICATION (NBHC)
						 * sending mail to the person who created document 
						 */
						if(app.getDocumentCreatedBy()!=null){
							//vijay added if contion on 3-03-2017
							if(!loggedinuserApprover.equals(app.getDocumentCreatedBy()))
								ccEmailEmployeeList.add(app.getDocumentCreatedBy());
						}
						
						/**
						 * End
						 */
						
						/** Vijay on 13-01-2019 for NBHC Inventory Management **/
						app.setDocumentValidation(app.getDocumentValidation());
						
						markApproved(app,toEmailEmployeeList,ccEmailEmployeeList);
						// end here
						
						getDataprovider().getList().remove(app);
						panel.hide();
						popup.remark.setValue("");
						
						
						/** Date :- 16-sep-2016
						 * Release Date - 30-sep-2016
						 * by - vijay 
						 * Description :- here i am added code for getting all approver level name.
						 * In toEmailList added who will approve the document and
						 * in ccEmailList added remaining all approver level name and requested by and person responsible
						 * To send Emails for request for approval To approver and CC to all approver name CC - Requested by CC - person responsible
						 * 
						 */
						
						ArrayList<String> toEmailList = new ArrayList<String>();
						ArrayList<String> ccEmailList = new ArrayList<String>();

						for(int i=0;i<multiLevelTableLis.size();i++){
						
							if((popup.getApprovalName().getItemText(popup.getApprovalName().getSelectedIndex())).equals(multiLevelTableLis.get(i).getEmployeeName())){
								toEmailList.add(multiLevelTableLis.get(i).getEmployeeName());
								System.out.println("to emails name ==="+multiLevelTableLis.get(i).getEmployeeName());
								Console.log("for request To"+multiLevelTableLis.get(i).getEmployeeName());

							}
							/**
							 * vijay commented beacuse here we adding all approver in cc no need this  with new change in cc adding last all approver level name
							 */
//							else{
//								ccEmailList.add(multiLevelTableLis.get(i).getEmployeeName());
//								System.out.println("cc emails name ==="+multiLevelTableLis.get(i).getEmployeeName());
//								Console.log("for request CC"+multiLevelTableLis.get(i).getEmployeeName());
//							}
							/**
							 * ends here
							 */
						}
						Console.log("Request for approval ");
						Console.log("Sales person === "+app.getPersonResponsible());
						Console.log("Requested By === "+app.getRequestedBy());
						Console.log("Created By === "+app.getDocumentCreatedBy());


						/**
						 * Date 02-03-2017 by vijay
						 * for 1 st level approver requester is request for approval person
						 * and 2 nd level onwards requester is last approver
						 */
						if(approval.getApprovalLevel()==1){
							ccEmailList.add(app.getRequestedBy());
						}else{
							ccEmailList.add(approval.getRequestedBy());
						}
						ccEmailList.add(app.getPersonResponsible());
						/**
						 * ends here
						 */
						
//						ccEmailList.add(app.getRequestedBy());
//						ccEmailList.add(app.getPersonResponsible());
						
						
						/**
						 * Added By Anil Date : 14-10-2016
						 * Release : 30 Sept 2016 
						 * Project : PURCHASE MODIFICATION (NBHC)
						 * sending mail to the person who created document 
						 */
						if(app.getDocumentCreatedBy()!=null){
							ccEmailList.add(app.getDocumentCreatedBy());
						}
						
						/**
						 * End
						 */
						
						
						/***
						 * Date 2-3-2017 added by vijay for get last approver name to send email
						 */
						
						/**
						 * Date 28-04-2017 commented by vijay for this is no need deadstock NBHC 
						 * cc will be last requester and purchase engineer and document created and To for Approver Name
						 */
						
//						if(app.getRemarkList().size()!=0){
//							for(int i=0;i<app.getRemarkList().size();i++){
//									ccEmailList.add(app.getRemarkList().get(i).getApproverName());
//							}
//						}
						Console.log("Request for approval CC size==="+ccEmailList);
						presenter.reactRequestForMultilevelApprovalEmail(approval,toEmailList,ccEmailList);
						/**
						 * ends here
						 */
					}
				}
			}
			else
			{
				approvalOkButton();
			}
		}
			
	//  commnted by vijay and call in approval popup 		
//			if(identify.equals(AppConstants.REJECTBTN))
//			{
//				if(popup.remark.getValue().equals("")){
//					ApprovalPresenter.showMessage("Please Fill Remark Field");
//				}
//				else{
//					ApprovalForm form=presenter.form;
//					form.showWaitSymbol();
//					presenter.manageBusinessProcessStatus(app,ConcreteBusinessProcess.REJECTED,"Request Rejected !","Failed to Approve Request !");
//					getDataprovider().getList().remove(app);
//					panel.hide();
//					popup.remark.setValue("");
//				}
//			}
//				
		}
			else if(event.getSource()==popup.getBtnCancel()){
				panel.hide();
				popup.remark.setValue("");
				
			}	
			// below code added by vijay
			else if(event.getSource()==popupapproval.getBtnOk()){
				
				if(identify.equals(AppConstants.REJECTBTN))
				{
					if(popupapproval.remark.getValue().equals("")){
						ApprovalPresenter.showMessage("Please Fill Remark Field");
					}
					else
					{
						System.out.println(" vijay new code dateeee"+app.getApprovalOrRejectDate());
						
						
						ArrayList<Approvals> approvalArray= new ArrayList<Approvals>();
						approvalArray.addAll(getDataprovider().getList());
						
						approvalArray.get(rowIndex).setRemark(popupapproval.getRemark().getValue());
						approvalArray.get(rowIndex).setApprovalOrRejectDate(today);
						
						app.setApprovalOrRejectDate(today);
						app.setRemark(popupapproval.getRemark().getValue());
						
						/**
						 * Date : 15-05-2017 By ANIL
						 * Added customer approval process for NBHC CCPM
						 */
						if(doctype.equals("Customer")){
							System.out.println("customer Rejection....");
							approveCustomer(app,ConcreteBusinessProcess.REJECTED,"Request Rejected !");
							return;
						}
						/**
						 * End
						 */
						/*** Date 27-03-2019 by Vijay for NBHC CCPM contract discontinue approval process ***/
						if(doctype.equals(AppConstants.CONTRACTDISCOUNTINUED)){
							reactonContractDiscontinueReject(app,"Request Rejected !");
							getDataprovider().getList().remove(app);
							panel.hide();
							popupapproval.remark.setValue("");
							return;
						}
						
						System.out.println(" date");
						ApprovalForm form=presenter.form;
						form.showWaitSymbol();
						presenter.manageBusinessProcessStatus(app,ConcreteBusinessProcess.REJECTED,"Request Rejected !","Failed to Approve Request !");
						getDataprovider().getList().remove(app);
						panel.hide();
						popupapproval.remark.setValue("");
						
						/**
						 * Date 24-07-2018 By Vijay
						 * Des :- View document popup if approve the document then hide the popup
						 */
						if(ApprovalPresenter.docApprovalPanel!=null){
							ApprovalPresenter.docApprovalPanel.hide();
						}
						/**
						 * ends here
						 */
					}
				}
			
			}else if(event.getSource()==popupapproval.getBtnCancel()){
				System.out.println("456");
				panel.hide();
				popupapproval.remark.setValue("");
			}
		
			/**
			 * Date 13-01-2019 by vijay for Inventory Management NBHC 
			 */
			if(event.getSource()==prVendorInfoPopup.getLblOk()){
				if(validateConfirmPR()){
					reactonConfirmPROK(prVendorInfoPopup.vendorprice,prVendorInfoPopup.getVendorComp());
				}
			}
			if(event.getSource()==prVendorInfoPopup.getLblCancel()){
				prVendorInfoPopup.hidePopUp();
			}
			/**
			 * ends here
			 */
			
			/*** Date 26-03-2019 by Vijay for NBHC CCPM Contract Discontinue Approval *****/
			if(event.getSource() == cancellationPopup.getLblOk()){
				
				/*** Date 23-03-2019 by Vijay for NBHC CCPM with cancellation remark with configuration values and old code in else block****/
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "EnableCancellationRemark")){
					if(cancellationPopup.getOlbCancellationRemark().getSelectedIndex()==0){
						ApprovalPresenter.showMessage("Please enter valid remark to cancel contract..!");
					}
					else{
						reactOnCancelContractButton(cancellationPopup.getOlbCancellationRemark().getValue(),app);
						cancellationPopup.hidePopUp();
					}
				} /*** ends here ****/
				else{
					if(cancellationPopup.getRemark().getValue().trim()!= null && !cancellationPopup.getRemark().getValue().trim().equals("")){
						//  react on contract cancel
						reactOnCancelContractButton(cancellationPopup.getRemark().getValue().trim(),app);
						cancellationPopup.hidePopUp();
					}
					else{
						ApprovalPresenter.showMessage("Please enter valid remark to cancel contract..!");
					}
				}
				
			}
	        if(event.getSource() == cancellationPopup.getLblCancel()){
	        	cancellationPopup.hidePopUp();
				cancellationPopup.remark.setValue("");
			}
	        if(event.getSource() == cancellation.getLblOk()){
	        	GWTCAlert alert = new GWTCAlert();
	        	if(cancellation.getOlbCancellationRemark().getSelectedIndex()==0){
					alert.alert("Please enter valid remark to cancel contract..!");
					return;
				}
				final String remark = cancellation.getOlbCancellationRemark().getValue(cancellation.getOlbCancellationRemark().getSelectedIndex());
				
				MyQuerry querry = new MyQuerry();
			  	Vector<Filter> filtervec=new Vector<Filter>();
			  	Filter filter = null;
				filter = new Filter();
				filter.setQuerryString("count");
				filter.setIntValue(app.getBusinessprocessId());
				filtervec.add(filter);
				querry.setFilters(filtervec);
				querry.setQuerryObject(new Contract());
				final ApprovalForm form=presenter.form;
			    form.showWaitSymbol();
				async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
					
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						// TODO Auto-generated method stub
						if(result.size()!=0){
							for(SuperModel model : result){
							final Contract	contract =  (Contract) model;
							contract.setStatus(Contract.CONTRACTDISCOUNTINED);
							update.reactOnCancelContract(contract, remark, LoginPresenter.loggedInUser, new AsyncCallback<Void>() {

								@Override
								public void onSuccess(Void result) {
									// TODO Auto-generated method stub
									/**
									 * @author Anil , Date : 09-12-2019
									 * Atfer discontinuing contract still contract record was visible at approval list
									 */
									updateApprovalStatus(app);
									
									contract.setStatus(Contract.CONTRACTDISCOUNTINED);
									contract.setDescription(contract.getDescription() + "\n" + "Contract Id ="
										+ contract.getCount() + " "
										+ "Contract Status = "+contract.getStatus()
										+ "\n"
										+ "has been cancelled by " + LoginPresenter.loggedInUser
										+ " with remark" + "\n" + "Remark ="
										+ remark);
										ApprovalPresenter.showMessage("Contract Discontinued Successfully..!");
										getDataprovider().getList().remove(app);
										cancellation.hidePopUp();
										/*** Date - 01-04-2019 by Vijay for NBHC CCPM need Only remark column in download report  ****/
										contract.setRemark(remark);
										contract.setCancellationDate(new Date());
										updateContract(contract);
										form.hideWaitSymbol();
								}
								
								

								@Override
								public void onFailure(Throwable caught) {
									// TODO Auto-generated method stub
									  form.hideWaitSymbol();
								}
								
							});
							
							}
						}
						  form.hideWaitSymbol();
					}
					
					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						  form.hideWaitSymbol();
					}
				});
	        }
	        if(event.getSource() == cancellation.getLblCancel()){
	        	cancellation.hidePopUp();
	        }
	        
	        //Ashwini Patil Date:23-07-2022
	        if(event.getSource()==conditionPopup.getBtnOne())
			{
				Console.log("inside onlick of button 1");
				Console.log("cnt="+cnt+" pestoIndia="+pestoIndia+" vcare="+vcare+" universal="+universal);
				
				if(docType.equals(AppConstants.APPROVALCONTRACT)){
					if (cnt > 0) {
						final String url = GWT.getModuleBaseURL() + "pdfservice"+ "?Id=" + con.getId() + "&" + "type=" + "c" + "&"+ "preprint=" + "yes";
						Window.open(url, "test", "enabled");
						conditionpanel.hide();
					}

					else if (pestoIndia > 0) {
						final String url = GWT.getModuleBaseURL()+ "pdfpestoIndiaservice" + "?Id=" + con.getId() + "&"+ "type=" + "c" + "&" + "preprint=" + "yes";
						Window.open(url, "test", "enabled");
						conditionpanel.hide();
						
					} else if (vcare > 0) {
						final String url = GWT.getModuleBaseURL() + "Contract" + "?Id="	+ con.getId() + "&" + "preprint=" + "yes";
						Window.open(url, "test", "enabled");
						conditionpanel.hide();
						
					} else if (universal > 0) {
						final String url = GWT.getModuleBaseURL()+ "universalServiceQuotation" + "?Id=" + con.getId()	+ "&" + "preprint=" + "yes" + "&" + "type=" + "c";
						Window.open(url, "test", "enabled");
						conditionpanel.hide();
					}
				}
				
				if(docType.equals(AppConstants.APPROVALQUOTATION)){
					if(cnt >0){
						System.out.println("in side one yes");
						final String url = GWT.getModuleBaseURL() + "pdfservice"+"?Id="+quotation.getId()+"&"+"type="+"q"+"&"+"preprint="+"yes";
						Window.open(url, "test", "enabled");
						
						conditionpanel.hide();
					}
					
					else if(customQuotation >0)
					{
						System.out.println("customQuotation value"+customQuotation);
						
						if(customQuotation > 0 && quotation.getGroup().equalsIgnoreCase("Five Years"))
						{
							System.out.println("in side five years ");
							final String url = GWT.getModuleBaseURL() + "customQuotation"+"?Id="+quotation.getId()+"&"+"group="+"Five Years"+"&"+"preprint="+"yes";
							 Window.open(url, "test", "enabled");
							 
							 conditionpanel.hide();
						}
						
						else if(customQuotation > 0 && quotation.getGroup().equalsIgnoreCase("Direct Quotation"))
						{
							System.out.println("in side Direct Quotation ");
							final String url = GWT.getModuleBaseURL() + "customQuotation"+"?Id="+quotation.getId()+"&"+"group="+"Direct Quotation"+"&"+"preprint="+"yes";
							 Window.open(url, "test", "enabled");
							 
							 conditionpanel.hide();
						}
						
						else if(customQuotation > 0 && quotation.getGroup().equalsIgnoreCase("Building"))
						{
							System.out.println("in side Building ");
							 final String url = GWT.getModuleBaseURL() + "customQuotation"+"?Id="+quotation.getId()+"&"+"group="+"Building"+"&"+"preprint="+"yes";
							 Window.open(url, "test", "enabled");
							 
							 conditionpanel.hide();
						}
						
						else if(customQuotation > 0 && quotation.getGroup().equalsIgnoreCase("After Inspection"))
						{
							System.out.println("in side After Inspection ");
							 final String url = GWT.getModuleBaseURL() + "customQuotation"+"?Id="+quotation.getId()+"&"+"group="+"After Inspection"+"&"+"preprint="+"yes";
							 Window.open(url, "test", "enabled");
							 
							 conditionpanel.hide();
						}
						
						
					}
					
					
					else if(pestQuotation >0)
					{
						System.out.println("Pestle Pest quotation");
						final String url = GWT.getModuleBaseURL() + "pdfpestleservice"+"?Id="+quotation.getId()+"&"+"group="+quotation.getGroup()+"&"+"preprint="+"yes";//1st PDF
						Window.open(url, "test", "enabled");
						conditionpanel.hide();
					}
					
					else if(pestoIndia > 0)
					{
						System.out.println("Pesto india quotation");
						final String url = GWT.getModuleBaseURL() + "pdfpestoIndiaservice"+"?Id="+quotation.getId()+"&"+"type="+"q"+"&"+"preprint="+"yes"; //+"&"+"group="+model.getGroup();//1st PDF
						Window.open(url, "test", "enabled");
						conditionpanel.hide();
					}
					else if(nbhc > 0)
					{
						System.out.println("nbhc quotation");
						final String url = GWT.getModuleBaseURL() + "nbhcServiceQuotation"+"?Id="+quotation.getId()+"&"+"preprint="+"yes"; 
						Window.open(url, "test", "enabled");
						conditionpanel.hide();
					}
					else if(universal > 0)
					{
						System.out.println("universal pest quotation");
						final String url = GWT.getModuleBaseURL() + "universalServiceQuotation"+"?Id="+quotation.getId()+"&"+"preprint="+"yes"+"&"+"type="+"q"; 
						Window.open(url, "test", "enabled");
						conditionpanel.hide();
					}
				}
				
				if(docType.equals(AppConstants.APPROVALINVOICEDETAILS)){
					
					if(cnt >0)
					{
						System.out.println("in side one yes");
						final String url = GWT.getModuleBaseURL() + "serviceinvoiceprint"+"?Id="+invoiceEntity.getId()+"&"+"preprint="+"yes"+"&"+"type="+"SingleBilling";
						Window.open(url, "test", "enabled");
						conditionpanel.hide();
					}
					
					else if(pestoIndia > 0)
					{
						System.out.println("Pesto india quotation");
						final String url = GWT.getModuleBaseURL() + "pdfpestoIndiaservice"+"?Id="+invoiceEntity.getId()+"&"+"type="+"i"+"&"+"preprint="+"yes"; 
						Window.open(url, "test", "enabled");
						
						conditionpanel.hide();
						
					}
					
					else if(vCare > 0)
					{
						System.out.println("V care quotation");
						
						final String url = GWT.getModuleBaseURL() + "Invoice"+"?Id="+invoiceEntity.getId()+"&"+"preprint="+"yes";
						Window.open(url, "test", "enabled");
						
						conditionpanel.hide();
						
					}
					else if(omPestInvoice > 0)
					{
						final String url = GWT.getModuleBaseURL() + "invoicepdf"+"?Id="+invoiceEntity.getId()+"&"+"preprint="+"yes";
						Window.open(url, "test", "enabled");
						conditionpanel.hide();
					}
				}
				
				
				
			}
	        
	      //Ashwini Patil Date:23-07-2022
	        if(event.getSource()==conditionPopup.getBtnTwo())
			{
				Console.log("inside onlick of button 2");
				Console.log("cnt="+cnt+" pestoIndia="+pestoIndia+" vcare="+vcare+" universal="+universal);
				if(docType.equals(AppConstants.APPROVALCONTRACT)){
					if (cnt > 0) {

						final String url = GWT.getModuleBaseURL() + "pdfservice"+ "?Id=" + con.getId() + "&" + "type=" + "c" + "&"+ "preprint=" + "no";
						Window.open(url, "test", "enabled");
						conditionpanel.hide();

					}

					else if (pestoIndia > 0) {
						System.out.println("Pesto india quotation");
						final String url = GWT.getModuleBaseURL()+ "pdfpestoIndiaservice" + "?Id=" + con.getId() + "&"+ "type=" + "c" + "&" + "preprint=" + "no"; // +"&"+"group="+model.getGroup();//1st
						Window.open(url, "test", "enabled");
						conditionpanel.hide();

					} else if (vcare > 0) {
						System.out.println("vcare contract");
						final String url = GWT.getModuleBaseURL() + "Contract" + "?Id="+ con.getId() + "&" + "preprint=" + "no";
						Window.open(url, "test", "enabled");
						conditionpanel.hide();
					}

					else if (universal > 0) {
						System.out.println("universal pest quotation");
						final String url = GWT.getModuleBaseURL()+ "universalServiceQuotation" + "?Id=" + con.getId()	+ "&" + "preprint=" + "no" + "&" + "type=" + "c";
						Window.open(url, "test", "enabled");
						conditionpanel.hide();
					}
				}
				
				if(docType.equals(AppConstants.APPROVALQUOTATION)){
					if(cnt >0){
						System.out.println("inside two no");
						final String url = GWT.getModuleBaseURL() + "pdfservice"+"?Id="+quotation.getId()+"&"+"type="+"q"+"&"+"preprint="+"no";
						Window.open(url, "test", "enabled");
					    
						conditionpanel.hide();
					}
					
					else if(customQuotation >0)
					{
						System.out.println("customQuotation value"+customQuotation);
						
						if(customQuotation > 0 && quotation.getGroup().equalsIgnoreCase("Five Years"))
						{
							System.out.println("in side five years ");
							final String url = GWT.getModuleBaseURL() + "customQuotation"+"?Id="+quotation.getId()+"&"+"group="+"Five Years"+"&"+"preprint="+"no";
							 Window.open(url, "test", "enabled");
							 
							 conditionpanel.hide();
						}
						
						else if(customQuotation > 0 && quotation.getGroup().equalsIgnoreCase("Direct Quotation"))
						{
							System.out.println("in side Direct Quotation ");
							final String url = GWT.getModuleBaseURL() + "customQuotation"+"?Id="+quotation.getId()+"&"+"group="+"Direct Quotation"+"&"+"preprint="+"no";
							 Window.open(url, "test", "enabled");
							 
							 conditionpanel.hide();
						}
						
						else if(customQuotation > 0 && quotation.getGroup().equalsIgnoreCase("Building"))
						{
							System.out.println("in side Building ");
							 final String url = GWT.getModuleBaseURL() + "customQuotation"+"?Id="+quotation.getId()+"&"+"group="+"Building"+"&"+"preprint="+"no";
							 Window.open(url, "test", "enabled");
							 
							 conditionpanel.hide();
						}
						
						else if(customQuotation > 0 && quotation.getGroup().equalsIgnoreCase("After Inspection"))
						{
							System.out.println("in side After Inspection ");
							 final String url = GWT.getModuleBaseURL() + "customQuotation"+"?Id="+quotation.getId()+"&"+"group="+"After Inspection"+"&"+"preprint="+"no";
							 Window.open(url, "test", "enabled");
							 
							 conditionpanel.hide();
						}
					}
					
					else if(pestQuotation >0)
					{
								System.out.println("in side no condition");
								final String url = GWT.getModuleBaseURL() + "pdfpestleservice"+"?Id="+quotation.getId()+"&"+"group="+quotation.getGroup()+"&"+"preprint="+"no";//1st PDF
								Window.open(url, "test", "enabled");
								
								conditionpanel.hide();
						
					}
					
					else if(pestoIndia > 0)
					{
						System.out.println("Pesto india quotation");
						final String url = GWT.getModuleBaseURL() + "pdfpestoIndiaservice"+"?Id="+quotation.getId()+"&"+"type="+"q"+"&"+"preprint="+"no"; //+"&"+"group="+model.getGroup();//1st PDF
						Window.open(url, "test", "enabled");
					
						conditionpanel.hide();
					
					}
					
					else if(nbhc > 0)
					{
						System.out.println("nbhc quotation");
						final String url = GWT.getModuleBaseURL() + "nbhcServiceQuotation"+"?Id="+quotation.getId()+"&"+"preprint="+"no"; 
						Window.open(url, "test", "enabled");
						conditionpanel.hide();
					}
					else if(universal > 0)
					{
						System.out.println("universal pest quotation");
						final String url = GWT.getModuleBaseURL() + "universalServiceQuotation"+"?Id="+quotation.getId()+"&"+"preprint="+"no"+"&"+"type="+"q"; 
						Window.open(url, "test", "enabled");
						conditionpanel.hide();
					}
				}
				
				if(docType.equals(AppConstants.APPROVALINVOICEDETAILS)){
					
					if(cnt >0)
					{
						System.out.println("inside two no");
						final String url = GWT.getModuleBaseURL() + "serviceinvoiceprint"+"?Id="+invoiceEntity.getId()+"&"+"preprint="+"no"+"&"+"type="+"SingleBilling";
						Window.open(url, "test", "enabled");
						conditionpanel.hide();
						
					}
					
					else if(pestoIndia > 0)
					{
						System.out.println("Pesto india quotation");
						final String url = GWT.getModuleBaseURL() + "pdfpestoIndiaservice"+"?Id="+invoiceEntity.getId()+"&"+"type="+"i"+"&"+"preprint="+"no"; //+"&"+"group="+model.getGroup();//1st PDF
						Window.open(url, "test", "enabled");
						conditionpanel.hide();
					
					}
					else if(vCare > 0)
					{
						System.out.println("V care quotation");
						final String url = GWT.getModuleBaseURL() + "Invoice"+"?Id="+invoiceEntity.getId()+"&"+"preprint="+"no";
						Window.open(url, "test", "enabled");
						
						conditionpanel.hide();
						
					}
					
					else if(omPestInvoice > 0)
					{
						final String url = GWT.getModuleBaseURL() + "invoicepdf"+"?Id="+invoiceEntity.getId()+"&"+"preprint="+"no";
						Window.open(url, "test", "enabled");
						conditionpanel.hide();
					}
				}

			}
	        
		}
	
	


	



	/**  Date 24-04-2017 Added by vijay for deadstock this method check Approver name is Same for next level
	 * Approver Name
	 */
	
	private int checkNextLevel(int approvalLevel, String approverName, List<MultilevelApprovalDetails> multiLevelTableLis) {
		
		int level = approvalLevel+1;
		
		for(int i=0;i<multiLevelTableLis.size();i++){
			
			if(multiLevelTableLis.get(i).getEmployeeName().equals(approverName) && Integer.parseInt(multiLevelTableLis.get(i).getLevel()) == level ){
				level++;
			}
		}
		
		return level;
	}
	/** ends here
	 */
	
	
	private void markApproved(Approvals approvalDataEntity,final ArrayList<String> toEmailEmployeeList, final ArrayList<String> ccEmailEmployeeList) {

		final Approvals appentity=approvalDataEntity;
		
		appentity.setStatus(Approvals.APPROVED);
		appentity.setRemark(popup.getRemark().getValue());
		appentity.setApprovalOrRejectDate(new Date());
		showWaitSymbol();
		async.save(appentity, new AsyncCallback<ReturnFromServer>() {
			
			@Override
			public void onSuccess(ReturnFromServer result) 
			{
				hideWaitSymbol();
				ApprovalPresenter.showMessage("Request Approved!");
				
				/**
				 * Release 30 sep 2016
				 * added by :- Vijay
				 * here i am sending email for Document approved to approver and cc to all remaining aprover level and CC- requester and CC - sales person or purchase eng or person responsible
				 */
				presenter.reactMultilevelApprovedEmail(appentity,toEmailEmployeeList,ccEmailEmployeeList);
			}
			
			@Override
			public void onFailure(Throwable caught) {
				hideWaitSymbol();
				ApprovalPresenter.showMessage("Request Failed! Try Again!");
			}
		});
	
	}
	
	
	public void approvalOkButton()
	{
		
		Console.log("INSIDE APPROVAL OK BUTTON");
		
		/***************************************Qty Logic**************************************/
				
				if(doctype.equals(AppConstants.APPROVALMATERIALISSUENOTE)){
					chequeForMINQuantity();
				}
				
				
				else if(doctype.equals(AppConstants.APPROVALMATERIALMOVEMENTNOTE)){
					chequeForMMNQuantity();
				}
				
				else if(doctype.equals("Delivery Note")){
					chequeForDeliveryNoteQuantity();
				}
				
				/**
				 * Developed By : Rohan Bhagde
				 * reason : for approving employee in approval (for NBHC)
				 * date : 20/10/2016
				 */
				else if(doctype.equals("Employee"))
				{
					approvedEmployee(app);
				}
				
				
				Console.log("Level-1");
				/**
				 * Setting remark list in approval entity
				 * Date : 14-10-2016 By Anil
				 * Release : 30 Sept 2016
				 * Project : Purchase Modification(nbhc)
				 */
				
				if(!popup.remark.getValue().equals("")){
					ArrayList<ApproversRemark> list=new ArrayList<ApproversRemark>();
					ApproversRemark apprem=new ApproversRemark();
					apprem.setApproverLevel(app.getApprovalLevel());
					apprem.setApproverName(app.getApproverName());
					apprem.setApproverRemark(popup.remark.getValue());
					list.add(apprem);
					app.setRemarkList(list);
				}
				
				/**
				 * End
				 */
				
				Console.log("Level-2");
				/****************************************************************************************/
				
				ArrayList<Approvals> approvalArray= new ArrayList<Approvals>();
				approvalArray.addAll(getDataprovider().getList());
				Console.log("Level-2a ");
				Console.log("Level-2b "+rowIndex);
				approvalArray.get(rowIndex).setRemark(popup.getRemark().getValue());
				Console.log("Level-2c ");
				Console.log("Level-2d "+today);
				approvalArray.get(rowIndex).setApprovalOrRejectDate(today);
				Console.log("Level-2e ");
				app.setApprovalOrRejectDate(today);
				Console.log("Level-2f ");
				app.setRemark(popup.getRemark().getValue());
				Console.log("Level-3");
				/**
				 * Date : 15-05-2017 By ANIL
				 * Added customer approval process for NBHC CCPM
				 */
				if(doctype.equals("Customer")){
					approveCustomer(app,ConcreteBusinessProcess.APPROVED,"Request Approved.");
					return;
				}
				/**
				 * End
				 */
				Console.log("Level-4");
				if(!doctype.equals("Employee") && !doctype.equals(AppConstants.APPROVALMATERIALISSUENOTE)&& !doctype.equals(AppConstants.APPROVALMATERIALMOVEMENTNOTE)&& !doctype.equals(AppConstants.APPROVALDELIVERYNOTE)){
					Console.log("Level-5");
					if(identify.equals(AppConstants.APPROVALBTN)){	
						Console.log("Level-6");
						ApprovalForm form=presenter.form;
					    form.showWaitSymbol();
					    Console.log("Level-7");
						presenter.manageBusinessProcessStatus(app,ConcreteBusinessProcess.APPROVED,"Request Approved !","Failed to Approve Request !");
						getDataprovider().getList().remove(app);
						panel.hide();
						popup.remark.setValue("");
						Console.log("Level-8");
						/**
						 * Date 24-07-2018 By Vijay
						 * Des :- View document popup if approve the document then hide the popup
						 */
						if(ApprovalPresenter.docApprovalPanel!=null){
							ApprovalPresenter.docApprovalPanel.hide();
						}
						/**
						 * ends here
						 */
						Console.log("Level-9");
					}
				}
	}

	/**
	 * Develpoed By : Rohan Bhagde 
	 * Date : 20/10/2016
	 * Reason : This is used for approving employee used for NBHC 
	 */
	private void approvedEmployee(final Approvals approvalmodel) {
		final ApprovalForm form=presenter.form;
		System.out.println("in side employee approvals ");
		 form.showWaitSymbol();
		MyQuerry querry = new MyQuerry();
	  	Company c = new Company();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
	  	filter = new Filter();
	  	filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(docID);
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Employee());
		
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onFailure(Throwable caught) {
					form.hideWaitSymbol();
				}

				@Override
			public void onSuccess(ArrayList<SuperModel> result) {
					
					for(SuperModel model:result)
					{
						Employee sup=(Employee)model;
					
					sup.setApproveStatus("Approved");
					sup.setStatus(true);
					
					async.save(sup, new AsyncCallback<ReturnFromServer>() {
						
						@Override
						public void onSuccess(ReturnFromServer result) {
							
							
						   
						    form.showDialogMessage("Request Approved ..!");
						    // email code aaded by vijay on 8 Feb 2017
							reactOnApprovalSuccessEmail(approvalmodel);
							getDataprovider().getList().remove(app);
							panel.hide();
							popup.remark.setValue("");
							
							form.hideWaitSymbol();
						}
						
						@Override
						public void onFailure(Throwable caught) {
							form.hideWaitSymbol();
						}
					});
					
					
				}
			}
		});
		
		
		app.setStatus("Approved");
		async.save(app, new AsyncCallback<ReturnFromServer>() {
			
			@Override
			public void onSuccess(ReturnFromServer result) {
				
				getDataprovider().getList().remove(app);
			}
			
			@Override
			public void onFailure(Throwable caught) {
				
			}
		});
		
		
	}
	
	/**
	 * ends here 
	 */
	
	
	
	private void reactOnApprovalSuccessEmail(Approvals approvalEntity) {
		
		emailService.intiateOnApproveRequestEmail(approvalEntity, new AsyncCallback<Void>(){

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Unable To Send Email");
				caught.printStackTrace();
			}

			@Override
			public void onSuccess(Void result) {
//				Window.alert("Email Sent Sucessfully !");
			}
		});
}
	/*************************************************Qty Logic******************************************/
	
	public void chequeForMMNQuantity() {
		MyQuerry querry = new MyQuerry();
		Company c = new Company();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(docID);
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new MaterialMovementNote());

		async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {

			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {

				ArrayList<MaterialProduct> list = new ArrayList<MaterialProduct>();
				for (SuperModel model : result) {
					MaterialMovementNote sup = (MaterialMovementNote) model;
					list.addAll(sup.getSubProductTableMmn());
					transDirection = sup.getTransDirection();
				}

				if (transDirection.equals("OUT")|| transDirection.equals("TRANSFEROUT")) {

					List<ApprovalCheckForQuantity> checklist = new ArrayList<ApprovalCheckForQuantity>();
					for (int i = 0; i < list.size(); i++) {

						ApprovalCheckForQuantity objcheck = new ApprovalCheckForQuantity();

						objcheck.setStrBin(list.get(i).getMaterialProductStorageBin());
						objcheck.setStrLoc(list.get(i).getMaterialProductStorageLocation());
						objcheck.setStrWarehouse(list.get(i).getMaterialProductWarehouse());
						objcheck.setQty(list.get(i).getMaterialProductRequiredQuantity());
						objcheck.setProdID(list.get(i).getMaterialProductId());

						checklist.add(objcheck);

					}

					validateQuantity(doctype, checklist);
				} else {

					ApprovalForm form = presenter.form;
					form.showWaitSymbol();
					presenter.manageBusinessProcessStatus(app,ConcreteBusinessProcess.APPROVED,
							"Request Approved !","Failed to Approve Request !");
					getDataprovider().getList().remove(app);
					panel.hide();
					popup.remark.setValue("");

				}
			}
		});
	}


	public void chequeForMINQuantity(){
		
		MyQuerry querry = new MyQuerry();
	  	Company c = new Company();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
	  	filter = new Filter();
	  	filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(docID);
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new MaterialIssueNote());
		
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onFailure(Throwable caught) {
				}

				@Override
			public void onSuccess(ArrayList<SuperModel> result) {
					
					ArrayList<MaterialProduct> list=new ArrayList<MaterialProduct>();
				for(SuperModel model:result)
				{
					MaterialIssueNote sup=(MaterialIssueNote)model;
					list.addAll(sup.getSubProductTablemin());
				}
				
				List<ApprovalCheckForQuantity> checklist= new ArrayList<ApprovalCheckForQuantity>();
				for(int i=0;i<list.size();i++){
					
					ApprovalCheckForQuantity objcheck = new ApprovalCheckForQuantity();
					
					objcheck.setStrBin(list.get(i).getMaterialProductStorageBin());
					objcheck.setStrLoc(list.get(i).getMaterialProductStorageLocation());
					objcheck.setStrWarehouse(list.get(i).getMaterialProductWarehouse());
					objcheck.setQty(list.get(i).getMaterialProductRequiredQuantity());
					objcheck.setProdID(list.get(i).getMaterialProductId());
				
				checklist.add(objcheck);
				
				}
				validateQuantity(doctype,checklist);
				}
		});
	}


	public void chequeForDeliveryNoteQuantity(){
		
		MyQuerry querry = new MyQuerry();
	  	Company c = new Company();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
	  	filter = new Filter();
	  	filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(docID);
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new DeliveryNote());
		
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onFailure(Throwable caught) {
				}

				@Override
			public void onSuccess(ArrayList<SuperModel> result) {
					
					ArrayList<DeliveryLineItems> list=new ArrayList<DeliveryLineItems>();
				for(SuperModel model:result)
				{
					DeliveryNote sup=(DeliveryNote)model;
					list.addAll(sup.getDeliveryItems());
				}
				
				List<ApprovalCheckForQuantity> checklist= new ArrayList<ApprovalCheckForQuantity>();
				for(int i=0;i<list.size();i++){
					
					ApprovalCheckForQuantity objcheck = new ApprovalCheckForQuantity();
					
					objcheck.setStrBin(list.get(i).getStorageBinName());
					objcheck.setStrLoc(list.get(i).getStorageLocName());
					objcheck.setStrWarehouse(list.get(i).getWareHouseName());
					objcheck.setQty(list.get(i).getQuantity());
					objcheck.setProdID(list.get(i).getProdId());
				
				checklist.add(objcheck);
				
				}
				validateQuantity(doctype,checklist);
				
				}
		});
	}


	public void validateQuantity(String docType,final List<ApprovalCheckForQuantity> lis) {

		for (final ApprovalCheckForQuantity temp : lis) {
			prodID = temp.getProdID();
			MyQuerry querry = new MyQuerry();
			Company c = new Company();
			Vector<Filter> filtervec = new Vector<Filter>();
			Filter filter = null;
			filter = new Filter();
			filter.setQuerryString("companyId");
			filter.setLongValue(c.getCompanyId());
			filtervec.add(filter);
			filter = new Filter();
			filter.setQuerryString("count");
			filter.setIntValue(prodID);
			filtervec.add(filter);
			querry.setFilters(filtervec);
			querry.setQuerryObject(new ProductInventoryView());

			async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
				@Override
				public void onFailure(Throwable caught) {
				}

				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					flag = flag + 1;

					qty = temp.getQty();
					prodID = temp.getProdID();
					wareHouse = temp.getStrWarehouse();
					storageloc = temp.getStrLoc();
					storagebin = temp.getStrBin();

					ArrayList<ProductInventoryView> list = new ArrayList<ProductInventoryView>();
					for (SuperModel model : result) {

						ProductInventoryView sup = (ProductInventoryView) model;

						for (int i = 0; i < sup.getDetails().size(); i++) {
							/**
							 * @author Anil , Date : 04-12-2019
							 * Nbhc Neha facing problem while approving MMN, beacuse stock was checked against storage location only
							 * now updated condition , also checking with warehouse name and storage location
							 */
							if (storagebin.equals(sup.getDetails().get(i).getStoragebin().trim())
									&&storageloc.equals(sup.getDetails().get(i).getStoragelocation().trim())
									&&wareHouse.equals(sup.getDetails().get(i).getWarehousename().trim())) {

								if (wareHouse.equals(sup.getDetails().get(i).getWarehousename().trim())) {
								}

								if (qty > sup.getDetails().get(i).getAvailableqty()) {
									ApprovalPresenter.showMessage("Required quantity is greatter than available quantity."
													+ " Available Quantity ="+ sup.getDetails().get(i).getAvailableqty());
									cnt = cnt + 1;

								}
							}
						}
					}
					if (lis.size() == 1 && cnt == 0) {

						if (identify == "APPROVAL") {

							ApprovalForm form = presenter.form;
							form.showWaitSymbol();
							presenter.manageBusinessProcessStatus(app,
									ConcreteBusinessProcess.APPROVED,
									"Request Approved !",
									"Failed to Approve Request !");
							getDataprovider().getList().remove(app);
							panel.hide();
							popup.remark.setValue("");
						}
					} else if (flag == lis.size() - 1 && cnt == 0) {

						if (identify == "APPROVAL") {

							ApprovalForm form = presenter.form;
							form.showWaitSymbol();
							presenter.manageBusinessProcessStatus(app,
									ConcreteBusinessProcess.APPROVED,
									"Request Approved !",
									"Failed to Approve Request !");
							getDataprovider().getList().remove(app);
							panel.hide();
							popup.remark.setValue("");
						}
					}
				}
			});
		}
	}
	
	private void saveApprovalReq(Approvals approval,final String status,final String dialogmsg)
	{
		async.save(approval, new AsyncCallback<ReturnFromServer>() {
			
			@Override
			public void onSuccess(ReturnFromServer result) 
			{
			}
			
			@Override
			public void onFailure(Throwable caught) {
				ApprovalPresenter.showMessage("Request Failed! Try Again!");
			}
		});
	}
	
	private boolean validateMultiLevelApproval(String doc,Approvals obj)
	{ArrayList<ProcessName> processNme=LoginPresenter.globalProcessName;
	ArrayList<MultilevelApproval> multiLevelApprovalLis1=LoginPresenter.globalMultilevelApproval;
	boolean flagApproval=false;
	for(int b=0;b<processNme.size();b++)
	{
		if(processNme.get(b).getProcessName().trim().equals(AppConstants.GLOBALRETRIEVALMULTILEVELAPPROVAL)&&processNme.get(b).isStatus()==true)
		{
			for(int d=0;d<multiLevelApprovalLis1.size();d++)
			{
				if(multiLevelApprovalLis1.get(d).getDocumentType().trim().equals(obj.getBusinessprocesstype().trim())
						&&multiLevelApprovalLis1.get(d).isStatus()==true
						&&multiLevelApprovalLis1.get(d).isDocStatus()==true)
				{
					flagApproval=true;
				}
			}
		}
	}
	return flagApproval;
	}

	public void showWaitSymbol() {
		glassPanel = new GWTCGlassPanel();
		glassPanel.show();
		
		
	}
	public void hideWaitSymbol() {
	   glassPanel.hide();
		
	}
	
	
	
//////////////////////////////////////Rohan added Session code here //////////////////////////////////////	
/**
* Rohan added session code here for termination of session on particular click only   	
* Date : 07-04-2017
 * @param object 
 * @param index 
* @param string Opration (can be Save, )
*/

private void updateSessionLastAccessedTimeInApprove(final String operation, final int index, final Approvals object) {
	System.out.println("Inside update last accessed time method");
	
	if(LoginPresenter.sessionCountValue !=0){
	Console.log("in side slick erp update session count="+LoginPresenter.sessionCountValue+"- userName"+LoginPresenter.proxyString+"- LoggedinUser"+LoginPresenter.loggedInUser);	
	Company comp=new Company();
	sessionService.ping(LoginPresenter.proxyString,LoginPresenter.sessionCountValue,true,comp.getCompanyId(),LoginPresenter.loggedInUser,"Approval",LoginPresenter.NumberOfUser,new AsyncCallback<Integer>() {
	public void onSuccess(Integer result) {
	
	if(result== -1){
		LoginPresenter.displaySessionTimedOut("Session time out due to simultaneous login and exceed no of user ..!");
	}
	else if(result== -10){
		LoginPresenter.displaySessionTimedOut("Session time out on server side..!");
	}
	else if(result== -20){
		LoginPresenter.displaySessionTimedOut("Your session has been terminated as <User Name= "+LoginPresenter.proxyString+"> has logged in  to other terminal or session..!");
	}
	else if(result== -2){
		LoginPresenter.displaySessionTimedOut("You Have been forcefully terminated by Admin..!");
	}
	else{
		

		/**
		 * Date 07-07-2018 By Vijay
		 * Des :- NBHC CCPM As per vaishali mam at approval this validation not required approver can able to approve old GRN
		 * so below validation code commented
		 */  
		
//		/**
//		 * nidhi
//		 * 15-05-2018
//		 */
//		
//		if(!UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin")){
//			if(object.getBusinessprocesstype().equalsIgnoreCase("MIN") || object.getBusinessprocesstype().equalsIgnoreCase("GRN")){
//				if(!validateMINGRNDate(object)){
//				validateMINGRNDate(object);
//				
////					return;
//				}
//			}
//		}
//		/**
//		 * end
//		 */
//		
//		if(!validDate){
//			return;
//			
//		}else{
		
		
		
		if(validateMultiLevelApproval(businessprocesstypeCol.getValue(object),object)){
			
			/**
			 * Date : 16-05-2017 By ANIL
			 * Amount Level approval
			 */
			
			ArrayList<MultilevelApproval> multiLevelApprovalLis1=LoginPresenter.globalMultilevelApproval;
			List<MultilevelApprovalDetails> multiLevelTableLis1=new ArrayList<MultilevelApprovalDetails>();
			
			for(int d=0;d<multiLevelApprovalLis1.size();d++)
			{
				if(multiLevelApprovalLis1.get(d).getDocumentType().trim().equals(app.getBusinessprocesstype().trim())&&multiLevelApprovalLis1.get(d).isStatus()==true)
				{
					multiLevelTableLis1=multiLevelApprovalLis1.get(d).getApprovalLevelDetails();
				}
			}
			
			
			boolean amtLvlAppFlag=false;
			for(MultilevelApprovalDetails obj:multiLevelTableLis1){
				Console.log("AMT MUL lvl "+Integer.parseInt(obj.getLevel().trim())+" DOC TYPE lvl "+object.getApprovalLevel());
				int lvl=Integer.parseInt(obj.getLevel().trim());
				if(lvl==object.getApprovalLevel()&&obj.getAmountUpto()!=null){
					amtLvlAppFlag=true;
					break;
				}
			}
			Console.log("AMT MUL FLAF "+amtLvlAppFlag+" DOC TYPE "+popup.getValidApprovalDocument());
			if(popup.getValidApprovalDocument()&&amtLvlAppFlag){
				Console.log("For Valid Document amount lvl approval process ");
				initializeAmountLevelApprovalProcess(index,object,multiLevelTableLis1);
			}else{
				
			
			Console.log("1-Multiapproval Doc Status "+object.getApprovalLevel());
			System.out.println("in validate condition flag");
			popup.getApprovalName().clear();
			
			System.out.println("in condition validate level == "+object.getApprovalLevel());
//			int level=object.getApprovalLevel()+1;
			popup.initializeApprover(object.getApprovalLevel(), object.getBusinessprocesstype().trim());
			
			
			
			/**
			 * Added by Anil On 30-08-2016
			 * get max level of approval
			 */
			int maxlevel=0;
			int temp=0;
			for(int i=0;i<multiLevelTableLis1.size();i++){
				if(i==0){
					maxlevel=Integer.parseInt(multiLevelTableLis1.get(i).getLevel());
				}else{
					temp=Integer.parseInt(multiLevelTableLis1.get(i).getLevel());
					if(temp>maxlevel){
						maxlevel=temp;
					}
				}
			}
			
//			int maxLevel1=Integer.parseInt(multiLevelTableLis1.get(multiLevelTableLis1.size()-1).getLevel());
//			System.out.println("Max Level = "+maxLevel1);
			
			System.out.println("Max Level = "+maxlevel);
			System.out.println("max level 2 ="+app.getApprovalLevel());
			
			if(app.getApprovalLevel()==maxlevel){
				popup.getApprovalname().setVisible(false);
				popup.getApprovalName().setVisible(false);
				popup.getMsgapprovalname().setVisible(false);
			}else{
				popup.getApprovalname().setVisible(true);
				popup.getApprovalName().setVisible(true);
				popup.getMsgapprovalname().setVisible(true);
			}
			
		}
			
			
			
		}else{
			
			if(popup.validateApproval()==true){
				popup.getApprovalname().setVisible(false);
				popup.getApprovalName().setVisible(false);
				popup.getMsgapprovalname().setVisible(false);
			}
			
		}
		
		/*** Date 26-03-2019 for NBHC CCCPM ****/
		System.out.println("object.getBusinessprocesstype() =="+object.getBusinessprocesstype());
		if(object.getBusinessprocesstype().equals(AppConstants.CONTRACTDISCOUNTINUED)){
			reactonContractDiscontinueApprove(object);
			return;
		}
		
		panel=new PopupPanel(true);
		panel.add(popup);
		panel.show();
		panel.center();
		doctype=object.getBusinessprocesstype();
		docID=object.getBusinessprocessId();
		rowIndex=index;	
//		app=object;
		identify="APPROVAL";
		markStatus=object.getStatus();
		
		if(markStatus.equals("Approved")||markStatus.equals("Rejected")||markStatus.equals("Cancelled"))
		{
			ApprovalPresenter.showMessage("This Request Is Already " + markStatus);
			panel.hide();
		}
		
//		}
	  }
	}
	
	

	public void onFailure(Throwable caught) {
	}
	});
	}
	}

/*
* ends here  	
*/




/**
 * Anil has added this code for customer approval process Date : 17-05-2017
 * @param approvalmodel
 * @param status
 * @param msg
 */

private void approveCustomer(final Approvals approvalmodel,final String status,final String msg) {
	final ApprovalForm form = presenter.form;
	
	MyQuerry querry = new MyQuerry();
	Company c = new Company();
	Vector<Filter> filtervec = new Vector<Filter>();
	Filter filter = null;
	filter = new Filter();
	filter.setQuerryString("companyId");
	filter.setLongValue(c.getCompanyId());
	filtervec.add(filter);
	filter = new Filter();
	filter.setQuerryString("count");
	filter.setIntValue(docID);
	filtervec.add(filter);
	querry.setFilters(filtervec);
	querry.setQuerryObject(new Customer());
	form.showWaitSymbol();
	async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {

		@Override
		public void onFailure(Throwable caught) {
			form.hideWaitSymbol();
		}

		@Override
		public void onSuccess(ArrayList<SuperModel> result) {

			for (SuperModel model : result) {
				final Customer sup = (Customer) model;
				if(status.equals(ConcreteBusinessProcess.APPROVED)){
					sup.setStatus("Active");
				}else{
					sup.setStatus(status);
				}
				sup.setRemark(approvalmodel.getRemark());

				async.save(sup,new AsyncCallback<ReturnFromServer>() {

					@Override
					public void onSuccess(ReturnFromServer result) {

						form.showDialogMessage(msg);
						reactOnApprovalSuccessEmail(approvalmodel);
						getDataprovider().getList().remove(app);
						panel.hide();
						popup.remark.setValue("");
						 /*** Date 01-06-2018 By vijay for approved customer load in composite **/
						reactOnUpdateCustomerStatus(sup); 
						form.hideWaitSymbol();
					}

					@Override
					public void onFailure(Throwable caught) {
						form.hideWaitSymbol();
					}
				});
			}
		}
	});

	app.setStatus(status);
	async.save(app, new AsyncCallback<ReturnFromServer>() {

		@Override
		public void onSuccess(ReturnFromServer result) {

			getDataprovider().getList().remove(app);
		}

		@Override
		public void onFailure(Throwable caught) {

		}
	});

}

/**
 * ends here
 */


	/**
	 * Date 01-06-2018 By vijay
	 * Des :- for NBHC approved employee not loaded in customer composite so here 
	 * i am updated the code for laod the customer in customer composite
	 */
	private void reactOnUpdateCustomerStatus(Customer customer) {
		PersonInfoComposite personinfo = new PersonInfoComposite();
		System.out.println("Global list value =="+personinfo.globalCustomerArray.size());
		for(int i=0;i<personinfo.globalCustomerArray.size();i++){
			if(personinfo.globalCustomerArray.get(i).getCount() == customer.getCount()){
				System.out.println("Customer existed in global list");
				personinfo.globalCustomerArray.get(i).setStatus("Active");
			}
		}
	}
	/**
	 * Ends here
	 */

/**
 * Date : 16-05-2017 By ANIL
 * Initialization amount level approval process 
 */
private void initializeAmountLevelApprovalProcess(final int index,final Approvals object,final List<MultilevelApprovalDetails> multiLevelTableLis1) {
	Console.log("AML 1-Multiapproval Doc Status "+object.getApprovalLevel());
	popup.getApprovalName().clear();
	int level=object.getApprovalLevel()+1;
	
	showWaitSymbol();
	async.getSearchResult(popup.getDocSearchQuerry(), new AsyncCallback<ArrayList<SuperModel>>() {
		@Override
		public void onSuccess(ArrayList<SuperModel> result) {
			hideWaitSymbol();
			if(result.size()!=0){
				Double amount=0d;
				for(SuperModel model:result){
					if(model instanceof Contract){
						System.out.println("Contract ");
						Contract entity=(Contract) model;
						amount=entity.getNetpayable();
						
						System.out.println("Total Amount "+amount);
						
					}else if(model instanceof Quotation){
						Quotation entity=(Quotation) model;
						amount=entity.getNetpayable();
					}else if(model instanceof SalesQuotation){
						SalesQuotation entity=(SalesQuotation) model;
						amount=entity.getNetpayable();
					}else if(model instanceof SalesOrder){
						SalesOrder entity=(SalesOrder) model;
						amount=entity.getNetpayable();
					}else if(model instanceof RequsestForQuotation){
						RequsestForQuotation entity=(RequsestForQuotation) model;
						for(ProductDetails prodDet:entity.getProductinfo()){
							amount=amount+prodDet.getProdPrice();
						}
					}else if(model instanceof LetterOfIntent){
						LetterOfIntent entity=(LetterOfIntent) model;
						for(ProductDetails prodDet:entity.getProductinfo()){
							amount=amount+prodDet.getProdPrice();
						}
					}else if(model instanceof PurchaseOrder){
						PurchaseOrder entity=(PurchaseOrder) model;
						amount=entity.getNetpayble();
					}
					/**
					 * @author Anil , Date : 07-08-2019
					 */
					else if(model instanceof MultipleExpenseMngt){
						MultipleExpenseMngt entity=(MultipleExpenseMngt) model;
						amount=entity.getTotalAmount();
					}
				}
				
				popup.initializeAmountLvlApprover(object.getApprovalLevel(), object.getBusinessprocesstype().trim(), amount);
				totalAmount=amount;
				int maxlevel=0;
				int temp=0;
				for(int i=0;i<multiLevelTableLis1.size();i++){
					if(i==0){
						maxlevel=Integer.parseInt(multiLevelTableLis1.get(i).getLevel());
					}else{
						temp=Integer.parseInt(multiLevelTableLis1.get(i).getLevel());
						if(temp>maxlevel){
							maxlevel=temp;
						}
					}
				}
				boolean maxLvlFlag=false;
				for(MultilevelApprovalDetails obj:multiLevelTableLis1){
					/**
					 * @author Anil @since 05-08-2021
					 * Added one more if condition, checked employee name
					 * issue raised by Ashwini for pecopp
					 * 
					 */
					if(obj.getLevel().equals(object.getApprovalLevel()+"")&&obj.getAmountUpto()>=amount&&object.getApproverName().equals(obj.getEmployeeName())){
						maxLvlFlag=true;
						break;
					}
				}
				Console.log("Max Level = "+maxlevel+" / "+maxLvlFlag);
				Console.log("max level 2 ="+app.getApprovalLevel());
				
				if(app.getApprovalLevel()==maxlevel||maxLvlFlag){
					popup.getApprovalname().setVisible(false);
					popup.getApprovalName().setVisible(false);
					popup.getMsgapprovalname().setVisible(false);
				}else{
					popup.getApprovalname().setVisible(true);
					popup.getApprovalName().setVisible(true);
					popup.getMsgapprovalname().setVisible(true);
				}
				
				panel=new PopupPanel(true);
				panel.add(popup);
				panel.show();
				panel.center();
				
				doctype=object.getBusinessprocesstype();
				docID=object.getBusinessprocessId();
				rowIndex=index;	
//				app=object;
				identify="APPROVAL";
				markStatus=object.getStatus();
				
				if(markStatus.equals("Approved")||markStatus.equals("Rejected")||markStatus.equals("Cancelled"))
				{
					ApprovalPresenter.showMessage("This Request Is Already " + markStatus);
					panel.hide();
				}
			}
		}
		@Override
		public void onFailure(Throwable caught) {
			hideWaitSymbol();
		}
	});
  }

public void popupApprovalBtnClick(Approvals appObj ,int index ){
	try {
		popup.getBusinessprocesstypeCol().setValue(appObj.getBusinessprocesstype());
		popup.getBusinessprocessidCol().setValue(appObj.getBusinessprocessId()+"");
		app=appObj;
		
		Console.log("INDEX "+index);
		
		System.out.println("out validate condition");
		
		updateSessionLastAccessedTimeInApprove("Approve",index,appObj);
	} catch (Exception e) {
		e.printStackTrace();
		System.out.println(e.getMessage());
	}
}


public void popupRejectBtnClick(Approvals appObj ,int index ){
	try {
		popupapproval.getBusinessprocesstypeCol().setValue(appObj.getBusinessprocesstype());
		popupapproval.getBusinessprocessidCol().setValue(appObj.getBusinessprocessId()+"");
		
		
		//*************rohan remove this lines for rejection in approval ****************
		
		
		panel=new PopupPanel(true);
		panel.add(popupapproval);
		panel.show();
		panel.center();
		app=appObj;
		identify="REJECTED";
		markStatus=appObj.getStatus();
		
		/**
		 * Date: 15-05-2017 By ANIL
		 */
		doctype=appObj.getBusinessprocesstype();
		docID=appObj.getBusinessprocessId();
		/**
		 * END
		 */
		
		if(markStatus.equals("Approved")||markStatus.equals("Rejected")||markStatus.equals("Cancelled"))
		{
			ApprovalPresenter.showMessage("This Request Is Already "+markStatus);
			panel.hide();
			presenter.docApprovalPanel.hide();
		}
		
	
	} catch (Exception e) {
		e.printStackTrace();
		System.out.println(e.getMessage());
	}
}
	

private void createColumnBpId(){
	bpId=new TextColumn<Approvals>() {

		@Override
		public String getValue(Approvals object) {
			if(object.getBpId()!=null){
				return object.getBpId();
			}else{
				return "";
			}
			
		}
		
	};
	table.addColumn(bpId,"BP Id");
	bpId.setSortable(true);
	table.setColumnWidth(bpId,120, Unit.PX);
	
}

private void createColumnBpName(){
	bpName=new TextColumn<Approvals>() {

		@Override
		public String getValue(Approvals object) {
			if(object.getBpName()!=null){
				return object.getBpName();
			}else{
				return "";
			}
		}
		
	};
	table.addColumn(bpName,"BP Name");
	bpName.setSortable(true);
	table.setColumnWidth(bpName,120, Unit.PX);
	
}

private void addColumnBpIdSorting() {
	
	List<Approvals> list=getDataprovider().getList();
	columnSort=new ListHandler<Approvals>(list);
	columnSort.setComparator(bpId, new Comparator<Approvals>()
			{
		@Override
		public int compare(Approvals e1,Approvals e2)
		{
			if(e1!=null && e2!=null)
			{
				if( e1.getBpId()!=null && e2.getBpId()!=null){
					return e1.getBpId().compareTo(e2.getBpId());}
			}
			else{
				return 0;}
			return 0;
		}
			});
	table.addColumnSortHandler(columnSort);

}

private void addColumnBpNameSorting() {
	
	List<Approvals> list=getDataprovider().getList();
	columnSort=new ListHandler<Approvals>(list);
	columnSort.setComparator(bpName, new Comparator<Approvals>()
			{
		@Override
		public int compare(Approvals e1,Approvals e2)
		{
			if(e1!=null && e2!=null)
			{
				/**
				 * nidhi
				 * 27-10-2017
				 * for sorting changes
				 */
//				if( e1.getBpName()!=null && e2.getBpName()!=null){
//					return e1.getBpName().compareTo(e2.getBpName());}
				
				if(e1.getBpName()==null && e2.getBpName()==null){
					return 0;
				}
				if(e1.getBpName() == null) return 1;
				if(e2.getBpName() == null) return -1;
				
				return e1.getBpName().compareTo(e2.getBpName());
				/**
				 *  end
				 */
			}
			else{
				return 0;}
		}
			});
	table.addColumnSortHandler(columnSort);

}



public boolean validateMINGRNDate(Approvals appObj){
	final boolean flag= true;
	
		final ApprovalForm form=presenter.form;
		System.out.println("in side employee approvals ");
		 form.showWaitSymbol();
		
		async.getSearchResult(approvalSelectionQuerry(appObj), new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onFailure(Throwable caught) {
					form.hideWaitSymbol();
				}

				@Override
			public void onSuccess(ArrayList<SuperModel> result) {
					ArrayList<SuperModel> resultlist = new ArrayList<SuperModel>();
					resultlist.addAll(resultlist);
					validateDate(result,form);
					form.hideWaitSymbol();
					}
		});
		
		
	return flag;
}



public void validateDate(ArrayList<SuperModel> result,ApprovalForm form){

	
	
	if(app.getBusinessprocesstype().equals(AppConstants.APPROVALGRN)){
		
		ArrayList<GRN> grnList =  new ArrayList<GRN>(); 
//		grnList.addAll(result);
		for(SuperModel grn : result){
			GRN grnDt = (GRN) grn;
			
			if(grnDt.getStatus().equalsIgnoreCase(GRN.REQUESTED)){

				DateTimeFormat format= DateTimeFormat.getFormat("dd/MM/yyyy");
				String diffDays = AppUtility.getForProcessConfigurartionIsActiveOrNot("GRNDateRestriction");
				
				if(diffDays==null){
					validDate = true;
				}else{

					
					int days = Integer.parseInt(diffDays);
					
					if(days>0){
						Date preFirstDate = new Date();
						Date preLastDate = new Date();
						
						CalendarUtil.addMonthsToDate(preFirstDate, -1);
						preFirstDate.setDate(1);
						
						preLastDate.setDate(1);
						CalendarUtil.addDaysToDate(preLastDate, -1);
						
						
						Date currentDate = new Date();
						
						Date validateDate = new Date();
						
						currentDate=format.parse(format.format(currentDate));
						
						validateDate=format.parse(format.format(validateDate));
						preLastDate=format.parse(format.format(preLastDate));
						preFirstDate=format.parse(format.format(preFirstDate));
						
						validateDate.setDate(days);
						
						Date firstDate = new Date();
						firstDate.setDate(1);
						
						if(grnDt.getCreationDate().before(firstDate) ){
							if((grnDt.getCreationDate().after(preFirstDate) && grnDt.getCreationDate().before(preLastDate)) || 
									grnDt.getCreationDate().equals(preLastDate) || grnDt.getCreationDate().equals(preFirstDate) ){
								if(currentDate.after(validateDate) || grnDt.getCreationDate().before(firstDate)){
									
									form.showDialogMessage("GRN of previous month is not allowed for Approval..!");
									validDate = false;
									form.hideWaitSymbol();
								}
							}else{
								if(currentDate.after(validateDate) || grnDt.getCreationDate().before(firstDate)){
									
									form.showDialogMessage("GRN of previous month is not allowed for Approval..!");
									validDate = false;
									form.hideWaitSymbol();
								}
								
							}
						}
					}
					
		
				}
			}
		}
		
	}else if(app.getBusinessprocesstype().equals(AppConstants.APPROVALMATERIALISSUENOTE)){
		

		
		for(SuperModel min : result){
			MaterialIssueNote minDt = (MaterialIssueNote) min;
			
			if(minDt.getStatus().equalsIgnoreCase(MaterialIssueNote.REQUESTED)){

				DateTimeFormat format= DateTimeFormat.getFormat("dd/MM/yyyy");
				String diffDays = AppUtility.getForProcessConfigurartionIsActiveOrNot("MINDateRestriction");
				
				
				
				if(diffDays==null){
					validDate = true;
				}else{
					int days = Integer.parseInt(diffDays);
					if(days>0){
						Date preFirstDate = new Date();
						Date preLastDate = new Date();
						
						CalendarUtil.addMonthsToDate(preFirstDate, -1);
						preFirstDate.setDate(1);
						
						preLastDate.setDate(1);
						CalendarUtil.addDaysToDate(preLastDate, -1);
						
						
						Date currentDate = new Date();
						
						Date validateDate = new Date();
						
						
						
						currentDate=format.parse(format.format(currentDate));
						
						validateDate=format.parse(format.format(validateDate));
						preLastDate=format.parse(format.format(preLastDate));
						preFirstDate=format.parse(format.format(preFirstDate));
						
						validateDate.setDate(days);
						
						Date firstDate = new Date();
						firstDate.setDate(1);
						if(minDt.getMinDate().before(firstDate) ){
							if((minDt.getMinDate().after(preFirstDate) && minDt.getMinDate().before(preLastDate)) || 
								minDt.getMinDate().equals(preLastDate) || minDt.getMinDate().equals(preFirstDate) ){
//								form.showDialogMessage(" correct duration.!!");
								if(currentDate.after(validateDate) || minDt.getMinDate().before(firstDate)){
									form.showDialogMessage("MIN of previous month is not allowed for Approval..!");
									validDate = false;
								}
							}else{
								if(currentDate.after(validateDate) || minDt.getMinDate().before(firstDate)){
									form.showDialogMessage("MIN of previous month is not allowed for Approval..!");
									validDate = false;
								}
							}
						}
					}
				
				}
			}
		}
		
	
	}
}


private MyQuerry approvalSelectionQuerry(Approvals object){
	
	MyQuerry querry=new MyQuerry();
	Vector<Filter> filtervec=new Vector<Filter>();
	Filter temp=null;
	
	temp=new Filter();
	temp.setQuerryString("companyId");
	temp.setLongValue(object.getCompanyId());
	filtervec.add(temp);
	
	temp=new Filter();
	temp.setQuerryString("count");
	temp.setIntValue(object.getBusinessprocessId());
	filtervec.add(temp);
	
	querry.setFilters(filtervec);
	
	if(docType.equals(AppConstants.APPROVALCONTRACT)){
		querry.setQuerryObject(new Contract());
	}else if(docType.equals(AppConstants.APPROVALSALESORDER)){
		querry.setQuerryObject(new SalesOrder());
	}else if(docType.equals(AppConstants.APPROVALQUOTATION)){
		querry.setQuerryObject(new Quotation());
	}else if(docType.equals(AppConstants.APPROVALSALESQUOTATION)){
		querry.setQuerryObject(new SalesQuotation());
	}else if(docType.equals(AppConstants.APPROVALPURCHASEORDER)){
		querry.setQuerryObject(new PurchaseOrder());
	}else if(docType.equals(AppConstants.APPROVALGRN)){
		querry.setQuerryObject(new GRN());
	}else if(docType.equals(AppConstants.APPROVALPURCHASEREQUISITION)){
		querry.setQuerryObject(new PurchaseRequisition());
	}else if(docType.equals(AppConstants.APPROVALEXPENSEMANAGMENT)){
		querry.setQuerryObject(new Expense());
	}else if(docType.equals(AppConstants.APPROVALMATERIALISSUENOTE)){
		querry.setQuerryObject(new MaterialIssueNote());
	}else if(docType.equals(AppConstants.APPROVALMATERIALMOVEMENTNOTE)){
		querry.setQuerryObject(new MaterialMovementNote());
	}else if(docType.equals(AppConstants.APPROVALMATERIALREQUESTNOTE)){
		querry.setQuerryObject(new MaterialRequestNote());
	}else if(docType.equals(AppConstants.APPROVALREQUESTFORQUOTATION)){
		querry.setQuerryObject(new RequsestForQuotation());
	}else if(docType.equals(AppConstants.APPROVALINVOICEDETAILS)){
		querry.setQuerryObject(new Invoice());
	}else if(docType.equals(AppConstants.APPROVALBILLINGDETAILS)){
		querry.setQuerryObject(new BillingDocument());
	}else if(docType.equals(AppConstants.APPROVALLETTEROFINTENT)){
		querry.setQuerryObject(new LetterOfIntent());
	}else if(docType.equals(AppConstants.APPROVALLEAVEAPPLICATION)){
		querry.setQuerryObject(new LeaveApplication());
	}else if(docType.equals(AppConstants.APPROVALCOMPLAIN)){
		querry.setQuerryObject(new Complain());
	}else if(docType.equals(AppConstants.APPROVALADVANCE)){
		querry.setQuerryObject(new Loan());
	}
	else if(docType.equals(AppConstants.APPROVALDELIVERYNOTE)){
		querry.setQuerryObject(new DeliveryNote());
	}
	else if(docType.equals(AppConstants.APPROVALINSPECTION)){
		querry.setQuerryObject(new Inspection());
	}
	else if(docType.equals(AppConstants.APPROVALWORKORDER)){
		querry.setQuerryObject(new WorkOrder());
	}
	else if(docType.equals(AppConstants.APPROVALSERVICEPO)){
		querry.setQuerryObject(new ServicePo());
	}
	else if(docType.equals(AppConstants.MULTIPLEEXPENSEMANAGEMET)){
		querry.setQuerryObject(new MultipleExpenseMngt());
	}else if(docType.equals(AppConstants.CUSTOMER)){
		querry.setQuerryObject(new Customer());
	}else if(docType.equals(AppConstants.APPROVALCAMPAIGNMANNAGEMENT)){
		querry.setQuerryObject(new CustomerTrainingDetails());

	}else if(docType.equals(AppConstants.APPROVALTIMEREPORT)){
		querry.setQuerryObject(new TimeReport());

	}else if(docType.equals(AppConstants.APPROVALEMPLOYEE)){
		querry.setQuerryObject(new Employee());
	}else if(docType.equals(AppConstants.CNC)){/** date 29.9.2018 added by komal */
		querry.setQuerryObject(new CNC());
	}else if(docType.equals(AppConstants.PHYSICALINVENTORY)){/** date 12-4.2018 added by nidhi */
		querry.setQuerryObject(new PhysicalInventoryMaintaince());
	}
	if(object.getBusinessprocesstype().equals(AppConstants.APPROVALMATERIALISSUENOTE)){
		querry.setQuerryObject(new MaterialIssueNote());
	}else if(object.getBusinessprocesstype().equals(AppConstants.APPROVALGRN)){
		querry.setQuerryObject(new GRN());
	}
	/********* Date 01-02-2019 by Vijay for NBHC inventory management added for Revise PR ******/
	else if(object.getBusinessprocesstype().equals(AppConstants.PURCHASEREQUISION)){
		querry.setQuerryObject(new PurchaseRequisition());
		System.out.println("OK PR");
	}
	System.out.println("app.getBusinessprocesstype() =="+object.getBusinessprocesstype());
	return querry;
}


	/**
	 * Date 13-01-2019 by Vijay for NBHC Inventory Management for Purchase requisition 
	 * confirm vendor locally or HO Vendor
	 */
	private void addcloumnConfirmVendor() {
		ButtonCell btnCell= new ButtonCell();
		confirmVendor = new Column<Approvals, String>(btnCell) {
			
			@Override
			public String getValue(Approvals object) {
				if(object.getBusinessprocesstype().equals(AppConstants.PURCHASEREQUISION)
					&& object.getApprovalLevel()==getProcessConfigWithApprovalLevel()){
					return "Confirm Vendor";
				}
				else{
					return "";
				}
				
				
			}


			@Override
			public void render(Context context, Approvals object, SafeHtmlBuilder sb) {
				if(object.getBusinessprocesstype().equals(AppConstants.PURCHASEREQUISION)
					&&	object.getApprovalLevel()==getProcessConfigWithApprovalLevel()){
					super.render(context, object, sb);
				}
			}
		};
		table.addColumn(confirmVendor,"Confirm Vendor");
		table.setColumnWidth(confirmVendor, 100,Unit.PX);

		
		confirmVendor.setFieldUpdater(new FieldUpdater<Approvals, String>() {
			
			@Override
			public void update(int index, Approvals object, String value) {
				app=object;
				prVendorInfoPopup.showPopUp();
				prVendorInfoPopup.getChkHOVendor().setValue(false);
				prVendorInfoPopup.getChkLocalVendor().setValue(false);
			}
		});
	}
	/**
	 * ends here
	 */

	
	private boolean validateConfirmPR() {
			System.out.println("prVendorInfoPopup.getChkLocalVendor().getValue() =="+prVendorInfoPopup.getChkLocalVendor().getValue());
			if(prVendorInfoPopup.getChkLocalVendor().getValue()&&
					prVendorInfoPopup.getChkHOVendor().getValue()){
				prVendorInfoPopup.showDialogMessage("Please select either local vendor or HO vendor!");
				return false;
			}
			if(prVendorInfoPopup.getChkLocalVendor().getValue()==false &&
					prVendorInfoPopup.getChkHOVendor().getValue()==false){
				prVendorInfoPopup.showDialogMessage("Please select local vendor or HO vendor!");
				return false;
			}
			System.out.println("Vendor info"+prVendorInfoPopup.getVendorComp().getValue());
			if(prVendorInfoPopup.getChkLocalVendor().getValue() && prVendorInfoPopup.getVendorComp().getValue()==null){
				prVendorInfoPopup.showDialogMessage("Please add vendor information!");
				return false;
			}
			return true;
		}
	
	private void reactonConfirmPROK(final double vendorprice, final PersonInfoComposite personInfoComposite) {
		System.out.println("app =="+app);
		if(app!=null){
			generalAsync.updatePRDoc(app, true, personInfoComposite.getIdValue(), vendorprice, new AsyncCallback<String>() {
				
				@Override
				public void onSuccess(String result) {
					app.setDocumentValidation(true);
					if(personInfoComposite!=null && personInfoComposite.getIdValue()!=-1){
						app.setVendorId(personInfoComposite.getIdValue());
						app.setVendorPrice(vendorprice);
					}
					getTable().redraw();
					prVendorInfoPopup.hidePopUp();
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					prVendorInfoPopup.hidePopUp();
				}
			});
		}
	}


	/**
	 * Date 28-01-2019 By Vijay
	 * Des :- PR approval at specific level they need to confirm the vendor then proceed for further
	 * so below process configuration is used to restriction at specific level with configuration  
	 */
	private int getProcessConfigWithApprovalLevel() {
		String value = AppUtility.getForProcessConfigurartionIsActiveOrNot("EnablePRConfirmVendorRestriction");
		if(value==null){
			return 0;
		}
		else{
			return Integer.parseInt(value);
		}
		
	}
	/**
	 * Date 31-01-2019 by Vijay For NBHC Inventory Management Revise document for change the qty or other information
	 * allow to edit
	 * @param approvalFlag 
	 */
	private void addColumnRevisedDoc(final boolean approvalFlag) {
		ButtonCell btnCell = new ButtonCell();
		
		revisedDoc = new Column<Approvals, String>(btnCell) {
			
			@Override
			public String getValue(Approvals object) {
				// TODO Auto-generated method stub
				String approvalRequestReturn="";
				level=0;
				level=object.getApprovalLevel();
				System.out.println("Approval Level Value "+object.getApprovalLevel());
//				level=(Integer)object.getApprovalLevel();
				if(approvalFlag){
					if(object.getBusinessprocesstype().equals(AppConstants.PURCHASEREQUISION)&&object.getApprovalLevel()==1){
						approvalRequestReturn="Revise PR";
//						return "Revise PR";
						return approvalRequestReturn;
					}
					else{
						return approvalRequestReturn;
//						return "";
					}
			   }
				else{
					if(object.getBusinessprocesstype().equals(AppConstants.PURCHASEREQUISION)){
						approvalRequestReturn="Revise PR";
//						return "Revise PR";
						return approvalRequestReturn;
					}
					else{
						return approvalRequestReturn;
//						return "";
					}
				}
			}
			
			@Override
			public void render(Context context, Approvals object, SafeHtmlBuilder sb) {
				if(object.getBusinessprocesstype().equals(AppConstants.PURCHASEREQUISION)){
					if(approvalFlag){
						if(object.getApprovalLevel()==1){
							super.render(context, object, sb);
						}
					}else{
						super.render(context, object, sb);
					}
				}
			}
		};
		table.addColumn(revisedDoc,"Revise");
		table.setColumnWidth(revisedDoc, 100,Unit.PX);
		
		revisedDoc.setFieldUpdater(new FieldUpdater<Approvals, String>() {
			
			@Override
			public void update(int index, final Approvals object, String value) {
				
					final PurchaseRequisitionForm form = new PurchaseRequisitionForm();
					form.showWaitSymbol();
					app = object;
					
					Timer timer = new Timer() {
						@Override
						public void run() {
							
							async.getSearchResult(approvalSelectionQuerry(object),
									new AsyncCallback<ArrayList<SuperModel>>() {

										@Override
										public void onFailure(Throwable caught) {
											form.showDialogMessage("An Unexpected Error occured !");
										}

										@Override
										public void onSuccess(ArrayList<SuperModel> result) {

											for (SuperModel pmodel : result) {
												PurchaseRequisition purchaseReq = (PurchaseRequisition) pmodel;
//												AppMemory.getAppMemory().currentScreen = Screen.PURCHASEREQUISITE;
												genralInvoicePopup = new GeneralViewDocumentPopup();
												genralInvoicePopup.setModel(AppConstants.PURCHASEREQUISION,purchaseReq);
												generalPanel = new PopupPanel();
												generalPanel.add(genralInvoicePopup);
												generalPanel.show();
												generalPanel.center();
												form.hideWaitSymbol();
											}
										}
									});

						}
					};
					timer.schedule(3000);
			}
		});
	}
	
	
private void reactonContractDiscontinueApprove(final Approvals object) {
		
	cancellation.showPopUp();
	cancellation.getOlbCancellationRemark().setEnabled(false);
	cancellation.getOlbCancellationRemark().setValue(object.getRemark());
//	update.getRecordsForContractCancel(object.getCompanyId(), object.getBusinessprocessId(),true, new AsyncCallback<ArrayList<CancelContract>>(){
//
//		@Override
//		public void onSuccess(ArrayList<CancelContract> result) {
//			// TODO Auto-generated method stub
//			cancellationPopup.getAllContractDocumentTable().getDataprovider().getList().clear();
//            System.out.println("result size cancel : "+result.size());
//			cancellationPopup.showPopUp();
//            cancellationPopup.getAllContractDocumentTable().getDataprovider().getList().addAll(result);
//			cancellationPopup.getAllContractDocumentTable().getTable().redraw();
//			cancellationPopup.getOlbCancellationRemark().setEnabled(false);
//			cancellationPopup.getOlbCancellationRemark().setValue(object.getRemark());
//		} 
//		
//		@Override
//		public void onFailure(Throwable caught) {
//			// TODO Auto-generated method stub
//			System.out.println("can not load data");
//		}
//		
//	});
	}

private void reactOnCancelContractButton(final String remark, final Approvals approval){
	final String loginUser = LoginPresenter.loggedInUser.trim();
	List<CancelContract> cancelContractList = cancellationPopup.getAllContractDocumentTable().getDataprovider().getList();
	final ArrayList<CancelContract> selectedCancelContractList = new ArrayList<CancelContract>();
	for(CancelContract canContract : cancelContractList){
		if(canContract.isRecordSelect()){
			selectedCancelContractList.add(canContract);
		}
	}
	System.out.println("Selected list size :" + selectedCancelContractList.size());
	
		MyQuerry querry = new MyQuerry();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
		filter = new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(approval.getBusinessprocessId());
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Contract());
		final ApprovalForm form=presenter.form;
	    form.showWaitSymbol();
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				if(result.size()!=0){
					for(SuperModel model : result){
					final Contract	contract =  (Contract) model;
					update.saveContractCancelData(contract, remark, loginUser, selectedCancelContractList, new AsyncCallback<Void>(){

						@Override
						public void onSuccess(Void result) {
							// TODO Auto-generated method stub
							
							contract.setStatus(Contract.CONTRACTDISCOUNTINED);
							contract.setDescription(contract.getDescription() + "\n" + "Contract Id ="
								+ contract.getCount() + " "
								+ "Contract Status = "+contract.getStatus()
								+ "\n"
								+ "has been cancelled by " + loginUser
								+ " with remark" + "\n" + "Remark ="
								+ remark);
								ApprovalPresenter.showMessage("Contract Discontinued Successfully..!");
								getDataprovider().getList().remove(app);
								cancellationPopup.hidePopUp();
								/*** Date - 01-04-2019 by Vijay for NBHC CCPM need Only remark column in download report  ****/
								contract.setRemark(remark);
								contract.setCancellationDate(new Date());
								updateContract(contract);
								updateApprovalStatus(approval);
								   form.hideWaitSymbol();
						}
						
						

						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							  form.hideWaitSymbol();
						}
						
					});
					
					}
				}
				  form.hideWaitSymbol();
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				  form.hideWaitSymbol();
			}
		});
	
   }
   
		private void updateContract(Contract contract) {
			async.save(contract, new AsyncCallback<ReturnFromServer>() {
				
				@Override
				public void onSuccess(ReturnFromServer result) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					
				}
			});
		}
		
		private void updateApprovalStatus(Approvals approval) {
			approval.setStatus(Approvals.APPROVED);
			approval.setApprovalOrRejectDate(new Date());
			async.save(approval, new AsyncCallback<ReturnFromServer>() {
				
				@Override
				public void onSuccess(ReturnFromServer result) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					
				}
			});
		}
		
		
		private void reactonContractDiscontinueReject(Approvals app, String Message) {
			app.setStatus(Approvals.REJECTED);
			ApprovalForm form=presenter.form;
			form.showDialogMessage(Message);
			async.save(app, new AsyncCallback<ReturnFromServer>() {
				
				@Override
				public void onSuccess(ReturnFromServer result) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					
				}
			});
		}
		
		private void addcolumnViewVendor() {
			ButtonCell btnCell= new ButtonCell();
			columnViewVendor = new Column<Approvals, String>(btnCell) {
				
				@Override
				public String getValue(Approvals object) {
					if(object.getBusinessprocesstype().equals(AppConstants.PURCHASEREQUISION)){
						if(object.getDocumentValidation()){
							if(object.getVendorId()!=-1){
								return "View Vendor";
							}
							else{
								return "HO Vendor";
							}
						}
						else{
							return "NA";
						}
						
					}
					else{
						return "";
					}
					
					
				}

			};
			table.addColumn(columnViewVendor,"View Vendor");
			table.setColumnWidth(columnViewVendor, 100,Unit.PX);

			
			columnViewVendor.setFieldUpdater(new FieldUpdater<Approvals, String>() {
				
				@Override
				public void update(int index, Approvals object, String value) {
					if(columnViewVendor.getValue(object).equals("View Vendor")){
						reactonShowVendorInfo(object);
					}
				}
			});
		
		}
		
		
		private void reactonShowVendorInfo(final Approvals object) {
			viewVendorInfoPopup.getLblOk().setVisible(false);
			viewVendorInfoPopup.showPopUp();
			viewVendorInfoPopup.setEnable(false);
			MyQuerry querry = getQuerry(object);
			generivservice.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					// TODO Auto-generated method stub
					for(SuperModel model : result){
						Vendor vendor = (Vendor) model;
						viewVendorInfoPopup.getTbfullName().setValue(vendor.getPointOfContact().getFullname());
						viewVendorInfoPopup.getTbVendorName().setValue(vendor.getfullName()); 
						viewVendorInfoPopup.getEtbEmail().setValue(vendor.getEmail());
						viewVendorInfoPopup.getPnbCellNo1().setValue(vendor.getCellNumber1());
						viewVendorInfoPopup.getDbvendorPrice().setValue(object.getVendorPrice());
						viewVendorInfoPopup.getPrimaryAddressComposite().setValue(vendor.getPrimaryAddress());
					}
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					
				}
			});
		}

		private MyQuerry getQuerry(Approvals object) {
			MyQuerry querry = new MyQuerry();
			Vector<Filter> filterVec = new Vector<Filter>();
			Filter filter = null;
			
			filter = new Filter();
			filter.setQuerryString("count");
			filter.setIntValue(object.getVendorId());
			filterVec.add(filter);
			
			querry.setFilters(filterVec);
			querry.setQuerryObject(new Vendor());
					
			return querry;
		}
		public void printSelectedDocument(Approvals object)
		{
			Console.log("Inside Selected Document== "+docType);
//			doctype=object.getBusinessprocesstype();
			if(docType.equals(AppConstants.APPROVALCONTRACT)){
				Console.log("Inside contract print");
				reactOnPrint(object);
			}
			if(docType.equals(AppConstants.APPROVALDELIVERYNOTE)){
				reactOnPrint(object);
			}
			if(docType.equals(AppConstants.APPROVALSALESORDER)){
				reactOnPrint(object);
			}
			if(docType.equals(AppConstants.APPROVALQUOTATION)){
				reactOnPrint(object);
			}
			if(docType.equals(AppConstants.APPROVALSALESQUOTATION)){
				reactOnPrint(object);
			}
			if(docType.equals(AppConstants.APPROVALPURCHASEORDER)){
				reactOnPrint(object);
			}
			if(docType.equals(AppConstants.APPROVALGRN)){
				reactOnPrint(object);
			}
			if(docType.equals(AppConstants.APPROVALPURCHASEREQUISITION)){
				reactOnPrint(object);
			}
			if(docType.equals(AppConstants.APPROVALEXPENSEMANAGMENT)){
				reactOnPrint(object);
			}
			if(docType.equals(AppConstants.APPROVALMATERIALISSUENOTE)){
				reactOnPrint(object);
			}
			if(docType.equals(AppConstants.APPROVALMATERIALMOVEMENTNOTE)){
				reactOnPrint(object);
			}
			if(docType.equals(AppConstants.APPROVALMATERIALREQUESTNOTE)){
				reactOnPrint(object);
			}
			if(docType.equals(AppConstants.APPROVALREQUESTFORQUOTATION)){
				reactOnPrint(object);
			}
			if(docType.equals(AppConstants.APPROVALINVOICEDETAILS)){
				reactOnPrint(object);
			}
			if(docType.equals(AppConstants.APPROVALBILLINGDETAILS)){
				reactOnPrint(object);
			}
			if(docType.equals(AppConstants.APPROVALLETTEROFINTENT)){
				reactOnPrint(object);
			}
			if(docType.equals(AppConstants.APPROVALLEAVEAPPLICATION)){
				reactOnPrint(object);
			}
			if(docType.equals(AppConstants.APPROVALCOMPLAIN)){
				reactOnPrint(object);
			}
			if(docType.equals(AppConstants.APPROVALADVANCE)){
				reactOnPrint(object);
			}
			if(docType.equals(AppConstants.APPROVALINSPECTION)){
				reactOnPrint(object);
			}
			if(docType.equals(AppConstants.APPROVALSERVICEPO)){
				reactOnPrint(object);
			}
			if(docType.equals(AppConstants.APPROVALMULTIPLEEXPENSEMANAGMENT)){
				reactOnPrint(object);
			}
			/**
			 * Date : 12-09-2018 BY ANIL
			 */
			if(docType.equals(AppConstants.APPROVALEMPLOYEE)){
				printingApprovalDocument(object);
			}
			/** date 29.09.2018 added by komal**/
			if(docType.equals(AppConstants.CNC)){
				reactOnCNCDownload(object);
			}
		}
		public void reactOnPrint(Approvals object) {
			Console.log("Inside react on print");
			printingApprovalDocument(object);
		}
		
		protected void printingApprovalDocument(final Approvals object){
			Console.log("Inside printingApprovalDocument print");
			final ApprovalForm form=ApprovalPresenter.form;

		if(docType.equals(AppConstants.APPROVALCONTRACT)){
			Console.log("Inside printing contract document");
			Timer timer=new Timer() 
			{
					@Override
					public void run() {
						
						async.getSearchResult(approvalSelectionQuerry(object),new AsyncCallback<ArrayList<SuperModel>>() {
		
							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected Error occured !");
								
							}
							@Override
							public void onSuccess(ArrayList<SuperModel> result) {
								
								
								for(SuperModel pmodel:result)
								{
										con=(Contract)pmodel;
//										final String url = GWT.getModuleBaseURL() + "pdfservice"+"?Id="+con.getId()+"&"+"type="+"c";
//										Window.open(url, "test", "enabled");
										
										
										//*************rohan changes ********************
										
										MyQuerry querry = new MyQuerry();
									  	Company c = new Company();
									  	Vector<Filter> filtervec=new Vector<Filter>();
									  	Filter filter = null;
									  	filter = new Filter();
									  	filter.setQuerryString("companyId");
										filter.setLongValue(c.getCompanyId());
										filtervec.add(filter);
										filter = new Filter();
										filter.setQuerryString("processName");
										filter.setStringValue("Contract");
										filtervec.add(filter);
										
										filter = new Filter();
										filter.setQuerryString("processList.status");
										filter.setBooleanvalue(true);
										filtervec.add(filter);
										
										querry.setFilters(filtervec);
										querry.setQuerryObject(new ProcessConfiguration());
										
										async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
											
											@Override
											public void onFailure(Throwable caught) {
											}			

											@Override
											public void onSuccess(ArrayList<SuperModel> result) {
												System.out.println(" result set size +++++++"+result.size());
												
												List<ProcessTypeDetails> processList =new ArrayList<ProcessTypeDetails>();
												if(result.size()==0){
													
													final String url = GWT.getModuleBaseURL() + "pdfservice"+"?Id="+con.getId()+"&"+"type="+"c"+"&"+"preprint="+"plane";
													Window.open(url, "test", "enabled");
												}
												else{
												
													for(SuperModel model:result)
													{
														ProcessConfiguration processConfig=(ProcessConfiguration)model;
														processList.addAll(processConfig.getProcessList());
														
													}
												
													 vcare =0;cnt =0;pestoIndia=0;hygeia=0;genesis=0;universal=0;
													
												for(int k=0;k<processList.size();k++){	
												if(processList.get(k).getProcessType().trim().equalsIgnoreCase("CompanyAsLetterHead")&&processList.get(k).isStatus()==true){
													
													cnt=cnt+1;
												
												}
												if (processList	.get(k).getProcessType().trim().equalsIgnoreCase("PestoIndiaQuotations")&& processList.get(k).isStatus() == true) {

													pestoIndia = pestoIndia + 1;

												}

												else if (processList.get(k).getProcessType().trim().equalsIgnoreCase("VCareCustomization")&& processList.get(k).isStatus() == true) {

													vcare = vcare + 1;
													break;
												}

												else if (processList.get(k).getProcessType().trim().equalsIgnoreCase("HygeiaPestManagement")&& processList.get(k).isStatus() == true) {

													hygeia = hygeia + 1;
													break;
												}

												else if (processList.get(k).getProcessType().trim().equalsIgnoreCase("AMCForGenesis")&& processList.get(k).isStatus() == true) {

													genesis = genesis + 1;
													break;
												}
												else if (processList.get(k).getProcessType().trim().equalsIgnoreCase("UniversalPestCustomization")&& processList.get(k).isStatus() == true) {
													universal = universal + 1;
													break;
												}

												else if (processList.get(k).getProcessType().trim().equalsIgnoreCase("PCAMBCustomization")&& processList.get(k).isStatus() == true) {
													final String url = GWT.getModuleBaseURL()+ "PcambContractpdf" + "?Id="+ con.getId();
													Window.open(url, "test", "enabled");
													return;
												}
											}
												
												
												if(cnt>0){
													System.out.println("in side react on prinnt");
													conditionpanel=new PopupPanel(true);
													conditionpanel.add(conditionPopup);
													conditionpanel.setGlassEnabled(true);
													conditionpanel.show();
													conditionpanel.center();
												}else if (pestoIndia > 0) {
													System.out.println("in side react on prinnt");
													conditionpanel = new PopupPanel(true);
													conditionpanel.add(conditionPopup);
													conditionpanel.setGlassEnabled(true);
													conditionpanel.show();
													conditionpanel.center();
														   
												} else if (universal > 0) {
													System.out.println("in side react on prinnt Universal pest Quotation");
													conditionpanel = new PopupPanel(true);
													conditionpanel.add(conditionPopup);
													conditionpanel.setGlassEnabled(true);
													conditionpanel.show();
													conditionpanel.center();
												}

												else if (vcare > 0) {
													System.out.println("in side react on prinnt");
													conditionpanel = new PopupPanel(true);
													conditionpanel.add(conditionPopup);
													conditionpanel.setGlassEnabled(true);
													conditionpanel.show();
													conditionpanel.center();

												} else if (hygeia > 0) {
													final String url = GWT.getModuleBaseURL()+ "pdfcon" + "?Id=" + con.getId();
													Window.open(url, "test", "enabled");

												} else if (genesis > 0) {

													final String url = GWT.getModuleBaseURL()+ "pdfamc" + "?Id=" + con.getId();
													Window.open(url, "test", "enabled");

												}
												else
												{
													final String url = GWT.getModuleBaseURL() + "pdfservice"+"?Id="+con.getId()+"&"+"type="+"c"+"&"+"preprint="+"plane";
													 Window.open(url, "test", "enabled");
													
												}
											}
											}
										});
										//**************************changes ends here ********************************
								}
							}
						});
						
//						form.hideWaitSymbol();
					}
				};
			timer.schedule(3000); 
			
		}
		if(docType.equals(AppConstants.APPROVALDELIVERYNOTE)){
			
			Timer timer=new Timer() 
			{
					@Override
					public void run() {
						
						async.getSearchResult(approvalSelectionQuerry(object),new AsyncCallback<ArrayList<SuperModel>>() {
		
							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected Error occured !");
								
							}
							@Override
							public void onSuccess(ArrayList<SuperModel> result) {
								
								
								for(SuperModel pmodel:result)
								{
										DeliveryNote con=(DeliveryNote)pmodel;
										final String url = GWT.getModuleBaseURL() + "deliverynotepdf"+"?Id="+con.getId();
										Window.open(url, "test", "enabled");
								}
							}
						});
						
						form.hideWaitSymbol();
					}
				};
			timer.schedule(3000); 
			
		}
		if(docType.equals(AppConstants.APPROVALSALESORDER)){
			
			 Timer timer=new Timer() 
	     	 {
					@Override
					public void run() {
						
						async.getSearchResult(approvalSelectionQuerry(object),new AsyncCallback<ArrayList<SuperModel>>() {

							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected Error occured !");
								
							}
							@Override
							public void onSuccess(ArrayList<SuperModel> result) {
								
								
								for(SuperModel pmodel:result)
								{
									SalesOrder sales=(SalesOrder)pmodel;
									final String url = GWT.getModuleBaseURL() + "pdfsales"+"?Id="+sales.getId()+"&"+"type="+"so"+"&"+"preprint="+"plane";
									Window.open(url, "test", "enabled");
								}
								
							}
						});
						
						form.hideWaitSymbol();
					}
				};
	          timer.schedule(3000); 
			
			
			
		}
		if(docType.equals(AppConstants.APPROVALQUOTATION)){
			
			 Timer timer=new Timer() 
		   	 {
						@Override
						public void run() {
							
							async.getSearchResult(approvalSelectionQuerry(object),new AsyncCallback<ArrayList<SuperModel>>() {

								@Override
								public void onFailure(Throwable caught) {
									form.showDialogMessage("An Unexpected Error occured !");
								}
								@Override
								public void onSuccess(ArrayList<SuperModel> result) {
									
									
									for(SuperModel pmodel:result)
									{
										 quotation=(Quotation)pmodel;
										
//										final String url = GWT.getModuleBaseURL() + "pdfservice"+"?Id="+quotation.getId()+"&"+"type="+"q";
//										 Window.open(url, "test", "enabled");
										 
										 
											//*************rohan changes ********************
											
											MyQuerry querry = new MyQuerry();
										  	Company c = new Company();
										  	Vector<Filter> filtervec=new Vector<Filter>();
										  	Filter filter = null;
										  	filter = new Filter();
										  	filter.setQuerryString("companyId");
											filter.setLongValue(c.getCompanyId());
											filtervec.add(filter);
											filter = new Filter();
											filter.setQuerryString("processName");
											filter.setStringValue("Quotation");
											filtervec.add(filter);
											
											filter = new Filter();
											filter.setQuerryString("processList.status");
											filter.setBooleanvalue(true);
											filtervec.add(filter);
											
											querry.setFilters(filtervec);
											querry.setQuerryObject(new ProcessConfiguration());
											
											async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
												
												@Override
												public void onFailure(Throwable caught) {
												}			

												@Override
												public void onSuccess(ArrayList<SuperModel> result) {
													System.out.println(" result set size +++++++"+result.size());
													
													List<ProcessTypeDetails> processList =new ArrayList<ProcessTypeDetails>();
													
													if(result.size()==0){
														
														final String url = GWT.getModuleBaseURL() + "pdfservice"+"?Id="+quotation.getId()+"&"+"type="+"q"+"&"+"preprint="+"plane";
														Window.open(url, "test", "enabled");
													}
													else{
													
														for(SuperModel model:result)
														{
															ProcessConfiguration processConfig=(ProcessConfiguration)model;
															processList.addAll(processConfig.getProcessList());
															
														}
													
														
														 cnt=0;customQuotation=0;pestQuotation=0;nbhc=0;universal=0;
														 
													for(int k=0;k<processList.size();k++){	
													if(processList.get(k).getProcessType().trim().equalsIgnoreCase("CompanyAsLetterHead")&&processList.get(k).isStatus()==true){
														
														cnt=cnt+1;
													
													}
													if(processList.get(k).getProcessType().trim().equalsIgnoreCase("PrintCustomQuotation")&&processList.get(k).isStatus()==true){
														
														customQuotation=customQuotation+1;
													
													}
													
													//******************pestal quotations code ******************
													
													if(processList.get(k).getProcessType().trim().equalsIgnoreCase("PestleQuotations")&&processList.get(k).isStatus()==true){
														System.out.println("in side pestle ");
														pestQuotation=pestQuotation+1;
													
													}
													
													
													if(processList.get(k).getProcessType().trim().equalsIgnoreCase("PestoIndiaQuotations")&&processList.get(k).isStatus()==true){
														
														pestoIndia=pestoIndia+1;
													
													}
													
													if(processList.get(k).getProcessType().trim().equalsIgnoreCase("NBHCQuotation")&&processList.get(k).isStatus()==true){
														nbhc=nbhc+1;
													}
													
													if(processList.get(k).getProcessType().trim().equalsIgnoreCase("UniversalPestCustomization")&&processList.get(k).isStatus()==true){
														universal=universal+1;
													}
													}
													
													
													if(pestQuotation > 0)
													{
														if(quotation.getGroup().equalsIgnoreCase("Five Years Quotation")|| quotation.getGroup().equalsIgnoreCase("Normal Quotation"))
														{
														System.out.println("in side react on prinnt pestQuotation");
														conditionpanel=new PopupPanel(true);
														conditionpanel.add(conditionPopup);
														conditionpanel.setGlassEnabled(true);
														conditionpanel.show();
														conditionpanel.center();
														}
														else
														{
																final String url = GWT.getModuleBaseURL() + "pdfservice"+"?Id="+model.getId()+"&"+"type="+"q"+"&"+"preprint="+"plane";
																Window.open(url, "test", "enabled");
																panel.hide();
														}
													
													}
													else if(nbhc > 0)
													{
														System.out.println("in side react on prinnt NBHC Quotation");
														conditionpanel=new PopupPanel(true);
														conditionpanel.add(conditionPopup);
														conditionpanel.setGlassEnabled(true);
														conditionpanel.show();
														conditionpanel.center();
													}
													else if(universal > 0)
													{
														System.out.println("in side react on prinnt Universal pest Quotation");
														conditionpanel=new PopupPanel(true);
														conditionpanel.add(conditionPopup);
														conditionpanel.setGlassEnabled(true);
														conditionpanel.show();
														conditionpanel.center();
													}
													else if(pestoIndia > 0)
													{
														System.out.println("in side react on prinnt pestQuotation");
														conditionpanel=new PopupPanel(true);
														conditionpanel.add(conditionPopup);
														conditionpanel.setGlassEnabled(true);
														conditionpanel.show();
														conditionpanel.center();
														
													}
													
													else if(customQuotation > 0)
													{
														
														if(quotation.getGroup().equalsIgnoreCase("Five Years")|| quotation.getGroup().equalsIgnoreCase("Direct Quotation")
																|| quotation.getGroup().equalsIgnoreCase("Building")|| quotation.getGroup().equalsIgnoreCase("After Inspection"))
														{
														
														System.out.println("in side react on prinnt customQuotation");
														conditionpanel=new PopupPanel(true);
														conditionpanel.add(conditionPopup);
														conditionpanel.setGlassEnabled(true);
														conditionpanel.show();
														conditionpanel.center();
														}
														else
														{
																final String url = GWT.getModuleBaseURL() + "pdfservice"+"?Id="+model.getId()+"&"+"type="+"q"+"&"+"preprint="+"plane";
																Window.open(url, "test", "enabled");
														}
													
													}
													
													else if(cnt>0){
														conditionpanel=new PopupPanel(true);
														conditionpanel.add(conditionPopup);
														conditionpanel.setGlassEnabled(true);
														conditionpanel.show();
														conditionpanel.center();
													}
													else
													{
															final String url = GWT.getModuleBaseURL() + "pdfservice"+"?Id="+quotation.getId()+"&"+"type="+"q"+"&"+"preprint="+"plane";
															Window.open(url, "test", "enabled");
													}
												}
												}
											});
											//**************************changes ends here ********************************
										
										 
										 
									}
								}
							});
							
							form.hideWaitSymbol();
						}
					};
		        timer.schedule(3000); 
			
			
			
		}
		if(docType.equals(AppConstants.APPROVALSALESQUOTATION)){
			
			 Timer timer=new Timer() 
		 	 {
					@Override
					public void run() {
						
						async.getSearchResult(approvalSelectionQuerry(object),new AsyncCallback<ArrayList<SuperModel>>() {
		
							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected Error occured !");
								
							}
							@Override
							public void onSuccess(ArrayList<SuperModel> result) {
								
								
								for(SuperModel pmodel:result)
								{
									SalesQuotation salesquotation=(SalesQuotation)pmodel;
									
									final String url = GWT.getModuleBaseURL() + "pdfsales"+"?Id="+salesquotation.getId()+"&"+"type="+"sq"+"&"+"preprint="+"plane";
									 Window.open(url, "test", "enabled");
								}
								
							}
						});
						
						form.hideWaitSymbol();
					}
				};
		      timer.schedule(3000); 
			
		}
		if(docType.equals(AppConstants.APPROVALPURCHASEORDER)){
			
			
			 Timer timer=new Timer() 
			 {
					@Override
					public void run() {
						
						async.getSearchResult(approvalSelectionQuerry(object),new AsyncCallback<ArrayList<SuperModel>>() {
		
							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected Error occured !");
							}
							@Override
							public void onSuccess(ArrayList<SuperModel> result) {
								
								
								for(SuperModel pmodel:result)
								{
									PurchaseOrder purchase=(PurchaseOrder)pmodel;
									final String url = GWT.getModuleBaseURL() + "pdfpurchase"+"?Id="+purchase.getId()+"&"+"preprint="+"plane";
									 Window.open(url, "test", "enabled");
								}
							}
						});
						
						form.hideWaitSymbol();
					}
				};
		     timer.schedule(3000); 
		}
		if(docType.equals(AppConstants.APPROVALGRN)){

			form.showDialogMessage("Pdf for this document is not available");
		}
		if(docType.equals(AppConstants.APPROVALPURCHASEREQUISITION)){
			
			form.showDialogMessage("Pdf for this document is not available");
		}
		if(docType.equals(AppConstants.APPROVALEXPENSEMANAGMENT)){
			
			form.showDialogMessage("Pdf for this document is not available");
		}
		if(docType.equals(AppConstants.APPROVALMATERIALISSUENOTE)){
			
			form.showDialogMessage("Pdf for this document is not available");
		}
		if(docType.equals(AppConstants.APPROVALMATERIALMOVEMENTNOTE)){
			
			form.showDialogMessage("Pdf for this document is not available");
		}
		if(docType.equals(AppConstants.APPROVALMATERIALREQUESTNOTE)){
			
			form.showDialogMessage("Pdf for this document is not available");
		}
		if(docType.equals(AppConstants.APPROVALREQUESTFORQUOTATION)){
			
			form.showDialogMessage("Pdf for this document is not available");
		}
		if(docType.equals(AppConstants.APPROVALINVOICEDETAILS)){
			
			 Timer timer=new Timer() 
			 {
					@Override
					public void run() {
						
						async.getSearchResult(approvalSelectionQuerry(object),new AsyncCallback<ArrayList<SuperModel>>() {
		
							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected Error occured !");
							}
							@Override
							public void onSuccess(ArrayList<SuperModel> result) {
								
								
								for(SuperModel pmodel:result)
								{
									 invoiceEntity=(Invoice)pmodel;
									
									if(invoiceEntity.getTypeOfOrder().equals("Sales Order"))
									{
										final String url = GWT.getModuleBaseURL() + "salesinvoiceprint"+"?Id="+invoiceEntity.getId();
										Window.open(url, "test", "enabled");
									}
									
									if(invoiceEntity.getTypeOfOrder().equals("Purchase Order"))
									{
										final String url = GWT.getModuleBaseURL() + "salesinvoiceprint"+"?Id="+invoiceEntity.getId();
										Window.open(url, "test", "enabled");
									}
									
								
//									if(invoiceEntity.getTypeOfOrder().equals("Service Order"))
//									{
//									final String url = GWT.getModuleBaseURL() + "serviceinvoiceprint"+"?Id="+invoiceEntity.getId();
//									Window.open(url, "test", "enabled");
//									}
										
										//*************rohan changes ********************
										
										MyQuerry querry = new MyQuerry();
									  	Company c = new Company();
									  	Vector<Filter> filtervec=new Vector<Filter>();
									  	Filter filter = null;
									  	filter = new Filter();
									  	filter.setQuerryString("companyId");
										filter.setLongValue(c.getCompanyId());
										filtervec.add(filter);
										filter = new Filter();
										filter.setQuerryString("processName");
										filter.setStringValue("Invoice");
										filtervec.add(filter);
										
										filter = new Filter();
										filter.setQuerryString("processList.status");
										filter.setBooleanvalue(true);
										filtervec.add(filter);
										
										querry.setFilters(filtervec);
										querry.setQuerryObject(new ProcessConfiguration());
										
										async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
											
											@Override
											public void onFailure(Throwable caught) {
											}			

											@Override
											public void onSuccess(ArrayList<SuperModel> result) {
												System.out.println(" result set size +++++++"+result.size());
												
												List<ProcessTypeDetails> processList =new ArrayList<ProcessTypeDetails>();
											
												if(result.size()==0){
													
													final String url = GWT.getModuleBaseURL() + "serviceinvoiceprint"+"?Id="+invoiceEntity.getId()+"&"+"preprint="+"plane"+"&"+"type="+"SingleBilling";
													Window.open(url, "test", "enabled");
													
													
													
												}
												else{
												
													for(SuperModel model:result)
													{
														ProcessConfiguration processConfig=(ProcessConfiguration)model;
														processList.addAll(processConfig.getProcessList());
														
													}
												int cnt=0,pestoIndia=0,vCare=0,omPestInvoice=0,hygeia=0,reddysPestControl=0;
												for(int k=0;k<processList.size();k++){	
												if(processList.get(k).getProcessType().trim().equalsIgnoreCase("CompanyAsLetterHead")&&processList.get(k).isStatus()==true){
													
													cnt=cnt+1;
												
												}
												
												if(processList.get(k).getProcessType().trim().equalsIgnoreCase("PestoIndiaQuotations")&&processList.get(k).isStatus()==true){
													
													pestoIndia=pestoIndia+1;
												
												}
												if(processList.get(k).getProcessType().trim().equalsIgnoreCase("VCareCustomization")&&processList.get(k).isStatus()==true){
													
													vCare=vCare+1;
												
												}
												
												if(processList.get(k).getProcessType().trim().equalsIgnoreCase("InvoiceAndRefDocCustmization")&&processList.get(k).isStatus()==true){
													
													omPestInvoice=omPestInvoice+1;
												
												}
												
												
												if (processList.get(k).getProcessType().trim().equalsIgnoreCase("HygeiaPestManagement")&& processList.get(k).isStatus() == true) {

													hygeia = hygeia + 1;

												}
												
												if (processList.get(k).getProcessType().trim().equalsIgnoreCase("PCAMBCustomization")&& processList.get(k).isStatus() == true) {

													final String url = GWT.getModuleBaseURL() + "pdfpcambinvoice"+"?Id="+invoiceEntity.getId();
													Window.open(url, "test", "enabled");
													return;

												}
												
												
												System.out.println("ROHAN   =====  "+processList.get(k).getProcessType().trim());
												if(processList.get(k).getProcessType().trim().equalsIgnoreCase("ReddysPestControlQuotations")&&processList.get(k).isStatus()==true){
												
													reddysPestControl=reddysPestControl+1;
													
												}
												}
												
												
												if(cnt>0){
													System.out.println("in side react on prinnt");
													conditionpanel=new PopupPanel(true);
													conditionpanel.add(conditionPopup);
													conditionpanel.setGlassEnabled(true);
													conditionpanel.show();
													conditionpanel.center();
												}
												
												else if(pestoIndia > 0)
												{
													
													System.out.println("in side react on prinnt");
													conditionpanel=new PopupPanel(true);
													conditionpanel.add(conditionPopup);
													conditionpanel.setGlassEnabled(true);
													conditionpanel.show();
													conditionpanel.center();
													
												}
												else if(vCare > 0)
												{
													
													System.out.println("in side react on prinnt");
													conditionpanel=new PopupPanel(true);
													conditionpanel.add(conditionPopup);
													conditionpanel.setGlassEnabled(true);
													conditionpanel.show();
													conditionpanel.center();
													
												}
												else if(omPestInvoice > 0)
												{
													
													System.out.println("in side react on prinnt");
													conditionpanel=new PopupPanel(true);
													conditionpanel.add(conditionPopup);
													conditionpanel.setGlassEnabled(true);
													conditionpanel.show();
													conditionpanel.center();
													
												}
												else if (hygeia > 0) {
													
													final String url = GWT.getModuleBaseURL()+ "pdfinvoice" + "?Id="+ model.getId();
													Window.open(url, "test", "enabled");

												} 
												else if(reddysPestControl > 0){
													
													final String url = GWT.getModuleBaseURL() + "pdfrinvoice"+"?Id="+model.getId();
													Window.open(url, "test", "enabled");
												}
												else
												{
													final String url = GWT.getModuleBaseURL() + "serviceinvoiceprint"+"?Id="+invoiceEntity.getId()+"&"+"preprint="+"plane"+"&"+"type="+"SingleBilling";
													Window.open(url, "test", "enabled");
													
												}
											}
											}
										});
										//**************************changes ends here ********************************
								}
							}
						});
						
						form.hideWaitSymbol();
					}
				};
		     timer.schedule(3000); 
		}
		
		if(docType.equals(AppConstants.APPROVALINSPECTION)){
			
			Timer timer=new Timer() 
			{
					@Override
					public void run() {
						
						async.getSearchResult(approvalSelectionQuerry(object),new AsyncCallback<ArrayList<SuperModel>>() {
		
							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected Error occured !");
								
							}
							@Override
							public void onSuccess(ArrayList<SuperModel> result) {
								
								
								for(SuperModel pmodel:result)
								{
										Inspection inspectionEntity=(Inspection)pmodel;
										final String url = GWT.getModuleBaseURL() + "pdfinspection"+"?Id="+inspectionEntity.getId();
										Window.open(url, "test", "enabled");
								}
							}
						});
						
						form.hideWaitSymbol();
					}
				};
			timer.schedule(3000); 
		}
		
		
		
		
		
		if(docType.equals(AppConstants.APPROVALBILLINGDETAILS)){
			
			form.showDialogMessage("Pdf for this document is not available");
		}
		if(docType.equals(AppConstants.APPROVALLETTEROFINTENT)){
			
			form.showDialogMessage("Pdf for this document is not available");
		}
		if(docType.equals(AppConstants.APPROVALLEAVEAPPLICATION)){
			
			form.showDialogMessage("Pdf for this document is not available");
		}
		if(docType.equals(AppConstants.APPROVALCOMPLAIN)){
			
			form.showDialogMessage("Pdf for this document is not available");
		}
		if(docType.equals(AppConstants.APPROVALADVANCE)){
			
			form.showDialogMessage("Pdf for this document is not available");
		}
		
		if(docType.equals(AppConstants.APPROVALSERVICEPO)){
				
				form.showWaitSymbol();
				async.getSearchResult(approvalSelectionQuerry(object), new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						form.hideWaitSymbol();
						ServicePo po=(ServicePo) result.get(0);
						Long id=po.getId();
						final String url = GWT.getModuleBaseURL()+ "servicePoPdf" + "?Id="+ id;
						Window.open(url, "test", "enabled");
					}
					
					@Override
					public void onFailure(Throwable caught) {
						form.hideWaitSymbol();
					}
				});
				
				
			}
		
		if(docType.equals(AppConstants.APPROVALMULTIPLEEXPENSEMANAGMENT)){
			
			form.showWaitSymbol();
			async.getSearchResult(approvalSelectionQuerry(object), new AsyncCallback<ArrayList<SuperModel>>() {
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					form.hideWaitSymbol();
					MultipleExpenseMngt po=(MultipleExpenseMngt) result.get(0);
					Long id=po.getId();
					
					
					final String url1 = GWT.getModuleBaseURL() + "pdfexpense"+"?Id="+id+ "&" + "type=" + "multiple"+"&"+"companyId="+model.getCompanyId();
					Window.open(url1, "test", "enabled");
					
				}
				
				@Override
				public void onFailure(Throwable caught) {
					form.hideWaitSymbol();
				}
			});
			
			
		}
		
		/**
		 * Date : 12-09-2018 By ANIL
		 */
		if(docType.equals(AppConstants.APPROVALEMPLOYEE)){
			
			form.showWaitSymbol();
			async.getSearchResult(approvalSelectionQuerry(object), new AsyncCallback<ArrayList<SuperModel>>() {
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					form.hideWaitSymbol();
					Employee po=(Employee) result.get(0);
					Long id=po.getId();
					
					final String url = GWT.getModuleBaseURL() + "employee" + "?Id="+po.getId()+"&"+"type="+"EmployeeForm"+"&"+"subtype="+"EmployeeFormPortrate";
					Window.open(url, "test", "enable");
					
				}
				
				@Override
				public void onFailure(Throwable caught) {
					form.hideWaitSymbol();
				}
			});
			
			
		}
		}
		
		private void reactOnCNCDownload(Approvals object) {
			final ApprovalForm form=presenter.form;
			final XlsxServiceAsync xlxWriter = GWT.create(XlsxService.class);		
			form.showWaitSymbol();
			async.getSearchResult(approvalSelectionQuerry(object), new AsyncCallback<ArrayList<SuperModel>>() {
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					form.hideWaitSymbol();
					CNC cnc=(CNC) result.get(0);
					xlxWriter.setCostSheetData(cnc, new AsyncCallback<Void>() {

						@Override
						public void onSuccess(Void result) {
							// TODO Auto-generated method stub
							String gwt = com.google.gwt.core.client.GWT
									.getModuleBaseURL();
							final String url = gwt + "CreateXLXSServlet" + "?type=" + 3;
							Window.open(url, "test", "enabled");
						}

						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub

						}
					});

					
				}
				
				@Override
				public void onFailure(Throwable caught) {
					form.hideWaitSymbol();
				}
			});
			
		
		}
}
