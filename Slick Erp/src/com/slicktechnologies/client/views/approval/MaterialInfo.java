package com.slicktechnologies.client.views.approval;

import java.io.Serializable;

public class MaterialInfo implements Serializable{
	int serProdId;
	String serProdName;
	int serNo;
	int materialProdId;
	String materialProdCode;
	String materialProdName;
	double materialProdQty;
	String matrialProdUom;
	
	
	public MaterialInfo() {
		
	}


	public int getSerProdId() {
		return serProdId;
	}


	public void setSerProdId(int serProdId) {
		this.serProdId = serProdId;
	}


	public int getSerNo() {
		return serNo;
	}


	public void setSerNo(int serNo) {
		this.serNo = serNo;
	}


	public int getMaterialProdId() {
		return materialProdId;
	}


	public void setMaterialProdId(int materialProdId) {
		this.materialProdId = materialProdId;
	}


	public String getMaterialProdCode() {
		return materialProdCode;
	}


	public void setMaterialProdCode(String materialProdCode) {
		this.materialProdCode = materialProdCode;
	}


	public String getMaterialProdName() {
		return materialProdName;
	}


	public void setMaterialProdName(String materialProdName) {
		this.materialProdName = materialProdName;
	}


	public String getMatrialProdUom() {
		return matrialProdUom;
	}


	public void setMatrialProdUom(String matrialProdUom) {
		this.matrialProdUom = matrialProdUom;
	}


	public double getMaterialProdQty() {
		return materialProdQty;
	}


	public void setMaterialProdQty(double materialProdQty) {
		this.materialProdQty = materialProdQty;
	}


	public String getSerProdName() {
		return serProdName;
	}


	public void setSerProdName(String serProdName) {
		this.serProdName = serProdName;
	}
	
	
	
}
