package com.slicktechnologies.client.views.approval;

public class ApprovalCheckForQuantity {
	
	int docID;
	String docType;
	String strBin;
	String strLoc;
	String strWarehouse;
	int prodID;
	double qty;
	
	
	
	
	
	//**************getters and setters**************************
	
	public int getDocID() {
		return docID;
	}
	public void setDocID(int docID) {
		this.docID = docID;
	}
	public String getDocType() {
		return docType;
	}
	public void setDocType(String docType) {
		this.docType = docType;
	}
	public String getStrBin() {
		return strBin;
	}
	public void setStrBin(String strBin) {
		this.strBin = strBin;
	}
	public String getStrLoc() {
		return strLoc;
	}
	public void setStrLoc(String strLoc) {
		this.strLoc = strLoc;
	}
	public String getStrWarehouse() {
		return strWarehouse;
	}
	public void setStrWarehouse(String strWarehouse) {
		this.strWarehouse = strWarehouse;
	}
	public int getProdID() {
		return prodID;
	}
	public void setProdID(int prodID) {
		this.prodID = prodID;
	}
	public double getQty() {
		return qty;
	}
	public void setQty(double qty) {
		this.qty = qty;
	}
	
	
	
}
