package com.slicktechnologies.client.views.approval;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.ApproversRemark;

public class ApproversRemarkTable extends SuperTable<ApproversRemark> {

	TextColumn<ApproversRemark> getApproverLevelCol;
	TextColumn<ApproversRemark> getApproverLevelName;
	TextColumn<ApproversRemark> getApproverLevelRemark;
	
	public ApproversRemarkTable() {
		super();
	}
	@Override
	public void createTable() {
		getApproverLevelCol();
		getApproverLevelName();
		getApproverLevelRemark();
	}

	private void getApproverLevelCol() {
		getApproverLevelCol=new TextColumn<ApproversRemark>() {
			@Override
			public String getValue(ApproversRemark object) {
				return object.getApproverLevel()+"";
			}
		};
		table.addColumn(getApproverLevelCol,"Approver Level");
		table.setColumnWidth(getApproverLevelCol, 70,Unit.PX);
	}
	private void getApproverLevelName() {
		getApproverLevelName=new TextColumn<ApproversRemark>() {
			@Override
			public String getValue(ApproversRemark object) {
				return object.getApproverName();
			}
		};
		table.addColumn(getApproverLevelName,"Approver");
		table.setColumnWidth(getApproverLevelName, 120,Unit.PX);
		
	}
	private void getApproverLevelRemark() {
		getApproverLevelRemark=new TextColumn<ApproversRemark>() {
			@Override
			public String getValue(ApproversRemark object) {
				return object.getApproverRemark();
			}
		};
		table.addColumn(getApproverLevelRemark,"Remark");
//		table.setColumnWidth(getApproverLevelRemark, 120,Unit.PX);	
	}
	
	@Override
	protected void initializekeyprovider() {
		
	}

	@Override
	public void addFieldUpdater() {
		
	}

	@Override
	public void setEnable(boolean state) {
		
	}

	@Override
	public void applyStyle() {
		
	}

}
