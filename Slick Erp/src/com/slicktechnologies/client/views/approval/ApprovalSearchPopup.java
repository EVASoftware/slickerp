package com.slicktechnologies.client.views.approval;

import java.util.Vector;

import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;

public class ApprovalSearchPopup extends SearchPopUpScreen<Approvals> {

	public TextBox tbbusinessprocessidCol;
	public ListBox tbbusinessprocesstypeCol;
	public ListBox tbstatusCol;
	public ObjectListBox<Branch> tbbranchCol;
	public ObjectListBox<Employee>tbrequestedbyCol;
	public ObjectListBox<Employee>tbapproverNameCol;
	public DateComparator dateComparator;
	public ListBox lbEmployee;
	
	
	
	public ApprovalSearchPopup(){
	    super();
	    createGui();
	}
	
	public ApprovalSearchPopup(boolean b) {
		super(b);
		createGui();
	}
	
	
	public void initWidget(){
		
		tbbusinessprocesstypeCol=new ListBox();
		AppUtility.setStatusListBox(tbbusinessprocesstypeCol, Approvals.getDocTypeList());
		
		tbbusinessprocessidCol=new TextBox();
		
		tbrequestedbyCol= new ObjectListBox<Employee>();
		AppUtility.makeSalesPersonListBoxLive(tbrequestedbyCol);
		
		tbstatusCol=new ListBox();
		AppUtility.setStatusListBox(tbstatusCol, Approvals.getStatusList());
		
		tbbranchCol=new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(tbbranchCol);
		
		tbapproverNameCol=new ObjectListBox<Employee>();
		AppUtility.makeSalesPersonListBoxLive(tbapproverNameCol);
		
		dateComparator=new DateComparator("creationDate",new Approvals());
		
		lbEmployee=new ListBox();
		lbEmployee.addItem(LoginPresenter.loggedInUser);
		
		lbEmployee.setSelectedIndex(1);
		lbEmployee.setEnabled(false);
	}
	
	public void createScreen(){	
		initWidget();
		
		FormFieldBuilder builder;
		
		builder = new FormFieldBuilder("Doc Type",tbbusinessprocesstypeCol);
		FormField ftbbusinessprocesstypeCol =builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	
		builder = new FormFieldBuilder("Doc Id",tbbusinessprocessidCol);
		FormField ftbbusinessprocessidCol =builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Requested By",tbrequestedbyCol);
		FormField ftbrequestedbyCol =builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Status",tbstatusCol);
		FormField ftbstatusCol =builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Branch",tbbranchCol);
		FormField ftbbranchCol =builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Approver Name",tbapproverNameCol);
		FormField ftbapproverNameCol =builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("From Date",dateComparator.getFromDate());
		FormField fdateComparator= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("To Date",dateComparator.getToDate());
		FormField fdateComparator1= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Approver Name",lbEmployee);
		FormField flbEmployee= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
	    this.fields= new FormField[][]{
				{ftbbusinessprocesstypeCol,ftbbusinessprocessidCol,ftbrequestedbyCol},
				{ftbstatusCol,ftbbranchCol,flbEmployee},
				{fdateComparator,fdateComparator1},
	    		};
	}    

	@Override
	public MyQuerry getQuerry() {
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		
		if(dateComparator.getValue()!=null)
		{
			filtervec.addAll(dateComparator.getValue());
		}
		
		if(tbbusinessprocesstypeCol.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(tbbusinessprocesstypeCol.getValue(tbbusinessprocesstypeCol.getSelectedIndex()).trim());
			temp.setQuerryString("businessprocesstype");
			filtervec.add(temp);
		}
		
		if (!tbbusinessprocessidCol.getValue().equals("")){
			temp=new Filter();
			temp.setIntValue(Integer.parseInt(tbbusinessprocessidCol.getValue()));
			temp.setQuerryString("businessprocessId");
			filtervec.add(temp);
		}
		
		if(tbrequestedbyCol.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(tbrequestedbyCol.getValue().trim());
			temp.setQuerryString("requestedBy");
			filtervec.add(temp);
		}

		if(tbbranchCol.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(tbbranchCol.getValue().trim());
			temp.setQuerryString("branchname");
			filtervec.add(temp);
		}
		
		if(tbstatusCol.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(tbstatusCol.getValue(tbstatusCol.getSelectedIndex()).trim());
			temp.setQuerryString("status");
			filtervec.add(temp);
		}
		
			temp=new Filter();
			temp.setStringValue(lbEmployee.getItemText(lbEmployee.getSelectedIndex()).trim());
			temp.setQuerryString("approverName");
			filtervec.add(temp);
		
		
		MyQuerry querry = new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Approvals());
		return querry;
	}

	@Override
	public boolean validate() {
		if(LoginPresenter.branchRestrictionFlag)
		{
		if(tbbranchCol.getSelectedIndex()==0)
		{
			showDialogMessage("Select Branch");
			return false;
		}
		}
		return true;
	}
}	