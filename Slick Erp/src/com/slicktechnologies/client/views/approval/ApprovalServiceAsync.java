package com.slicktechnologies.client.views.approval;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.common.businessprocesslayer.ApprovableProcess;

public interface ApprovalServiceAsync {

	/**
	 * Date : 26-02-2017 By Anil
	 * Old Code for approval
	 */
//	public void sendApproveRequest(Approvals model,AsyncCallback<Void>call);
	
	/**
	 * Date : 26 -02-2017 By Anil
	 * Changing return type of method form void to String
	 **/
	public void sendApproveRequest(Approvals model,AsyncCallback<String>call);
	
	public void cancelDocument(Approvals model,AsyncCallback<Void>call);
	
}
