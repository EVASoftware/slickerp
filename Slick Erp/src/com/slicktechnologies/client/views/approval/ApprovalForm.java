package com.slicktechnologies.client.views.approval;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreen;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;


// TODO: Auto-generated Javadoc
/**
 * Class Approval Form
 */
public class ApprovalForm extends TableScreen<Approvals>{

	
	public ApprovalTable table;
	/**
	 * Instantiates a new approval form.
	 *
	 * @param table the table
	 */
	public ApprovalForm(SuperTable<Approvals> table) {
		super(table);
		this.table=(ApprovalTable) table;	
		createGui();
	} 
	
	@Override
	public void createScreen() 
	{
		this.processlevelBarNames=new String[]{AppConstants.APPROVALVIEWDOCUMENT,AppConstants.APPROVALPRINTDOCUMENT,AppConstants.APPROVALVIEWMATERIAL};
	}
	
	
	
	
	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.UiScreen#toggleAppHeaderBarMenu()
	 */
	@Override
	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Search"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.APPROVAL,LoginPresenter.currentModule.trim());
	}
	
	
	
	
	
	public ApprovalTable getTable() {
		return table;
	}

	public void setTable(ApprovalTable table) {
		this.table = table;
	}
	

}
