package com.slicktechnologies.client.views.approval;

import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;

public class ApprovalMaterialPopup extends AbsolutePanel {
//	TextBox label;
	InlineLabel displayMessage;
	MaterialTable productList;
	private Button btnClose,btnDownload,btnPrint;
	
	
	public ApprovalMaterialPopup() {
		
		
		displayMessage = new InlineLabel();
		add(displayMessage,10,10);
		
		productList=new MaterialTable();
		productList.connectToLocal();
		add(productList.getTable(),10,40);
		
		btnClose=new Button("  Close  ");
		add(btnClose, 10, 450);
		
		btnDownload=new Button("  Download  ");
		add(btnDownload, 750, 10);
		btnPrint=new Button("  Print  ");
		add(btnPrint, 850, 10);
		
		setSize("900px","500px");
		this.getElement().setId("form");
		
	}


	public InlineLabel getDisplayMessage() {
		return displayMessage;
	}


	public void setDisplayMessage(InlineLabel displayMessage) {
		this.displayMessage = displayMessage;
	}


	public MaterialTable getProductList() {
		return productList;
	}


	public void setProductList(MaterialTable productList) {
		this.productList = productList;
	}


	public Button getBtnDownload() {
		return btnDownload;
	}


	public void setBtnDownload(Button btnDownload) {
		this.btnDownload = btnDownload;
	}


	public Button getBtnPrint() {
		return btnPrint;
	}


	public void setBtnPrint(Button btnPrint) {
		this.btnPrint = btnPrint;
	}


	public Button getBtnClose() {
		return btnClose;
	}


	public void setBtnClose(Button btnClose) {
		this.btnClose = btnClose;
	}
	
	
	
}
