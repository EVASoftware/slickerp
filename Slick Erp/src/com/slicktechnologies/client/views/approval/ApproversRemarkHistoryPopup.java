package com.slicktechnologies.client.views.approval;

import com.google.gwt.dom.client.Style.FontStyle;
import com.google.gwt.dom.client.Style.FontWeight;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.InlineLabel;

public class ApproversRemarkHistoryPopup extends AbsolutePanel {
	ApproversRemarkTable remarkTbl;
	
	private void initializeWidget(){
		remarkTbl=new ApproversRemarkTable();
		remarkTbl.getTable().setHeight("200px");
	}
	
	public ApproversRemarkHistoryPopup() {
		initializeWidget();
		
		InlineLabel label=new InlineLabel("Remark");
		label.getElement().getStyle().setFontSize(20, Unit.PX);
		label.getElement().getStyle().setFontStyle(FontStyle.NORMAL);
		label.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		
		add(label,180,10);
		add(remarkTbl.getTable(),10,30);
		
		setSize("450px","220px");
		this.getElement().setId("login-form");
	}
	
	public void clear(){
		remarkTbl.connectToLocal();
	}
	
}
