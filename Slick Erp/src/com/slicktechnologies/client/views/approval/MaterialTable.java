package com.slicktechnologies.client.views.approval;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.inventory.GRN;

public class MaterialTable extends SuperTable<MaterialInfo>{

	TextColumn<MaterialInfo> getColumnSerProdId;
	TextColumn<MaterialInfo> getColumnSerProdName;
	TextColumn<MaterialInfo> getColumnSerNo;
	TextColumn<MaterialInfo> getColumnMateProdId;
	TextColumn<MaterialInfo> getColumnMateProdCode;
	TextColumn<MaterialInfo> getColumnMateProdName;
	TextColumn<MaterialInfo> getColumnMateProdQty;
	TextColumn<MaterialInfo> getColumnMateProdUom;
	
	public MaterialTable() {
		super();
		setHeight("400px");
	}
	
	@Override
	public void createTable() {
		getColumnSerProdId();
		getColumnSerProdName();
		getColumnSerNo();
		getColumnMateProdId();
		getColumnMateProdCode();
		getColumnMateProdName();
		getColumnMateProdQty();
		getColumnMateProdUom();
		getColumnSortingSerProdId();
	}

	private void getColumnSortingSerProdId() {
		List<MaterialInfo> list = getDataprovider().getList();
		columnSort = new ListHandler<MaterialInfo>(list);
		columnSort.setComparator(getColumnSerProdId, new Comparator<MaterialInfo>() {
			@Override
			public int compare(MaterialInfo e1, MaterialInfo e2) {
				if (e1 != null && e2 != null) {
					if (e1.getSerProdId() == e2.getSerProdId()) {
						return 0;
					}
					if (e1.getSerProdId() > e2.getSerProdId()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void getColumnSerProdName() {
		getColumnSerProdName=new TextColumn<MaterialInfo>() {
			@Override
			public String getValue(MaterialInfo object) {
				if(object.getSerProdName()!=null){
					return object.getSerProdName()+"";
				}
				return "";
			}
		};
		table.addColumn(getColumnSerProdName,"Service Product Name");
		table.setColumnWidth(getColumnSerProdName, "180px");	
	}

	private void getColumnSerProdId() {
		getColumnSerProdId=new TextColumn<MaterialInfo>() {
			@Override
			public String getValue(MaterialInfo object) {
				if(object.getSerProdId()!=0){
					return object.getSerProdId()+"";
				}
				return "";
			}
		};
		table.addColumn(getColumnSerProdId,"Service Product ID");
		table.setColumnWidth(getColumnSerProdId, "180px");	
		getColumnSerProdId.setSortable(true);
	}

	private void getColumnSerNo() {
		getColumnSerNo=new TextColumn<MaterialInfo>() {
			@Override
			public String getValue(MaterialInfo object) {
				if(object.getSerProdId()!=0){
					return object.getSerNo()+"";
				}
				return "";
			}
		};
		table.addColumn(getColumnSerNo,"Service No.");
		table.setColumnWidth(getColumnSerNo, "99px");	
	}

	private void getColumnMateProdId() {
		getColumnMateProdId=new TextColumn<MaterialInfo>() {
			@Override
			public String getValue(MaterialInfo object) {
				if(object.getMaterialProdId()!=0){
					return object.getMaterialProdId()+"";
				}
				return "";
			}
		};
		table.addColumn(getColumnMateProdId,"Material ID");
		table.setColumnWidth(getColumnMateProdId, "100px");	
	}

	private void getColumnMateProdCode() {
		getColumnMateProdCode=new TextColumn<MaterialInfo>() {
			@Override
			public String getValue(MaterialInfo object) {
				if(object.getMaterialProdCode()!=null){
					return object.getMaterialProdCode()+"";
				}
				return "";
			}
		};
		table.addColumn(getColumnMateProdCode,"Material Code");
		table.setColumnWidth(getColumnMateProdId, "120px");	
	}

	private void getColumnMateProdName() {
		getColumnMateProdName=new TextColumn<MaterialInfo>() {
			@Override
			public String getValue(MaterialInfo object) {
				if(object.getMaterialProdName()!=null){
					return object.getMaterialProdName()+"";
				}
				return "";
			}
		};
		table.addColumn(getColumnMateProdName,"Material Name");
		table.setColumnWidth(getColumnMateProdName, "120px");		
	}

	private void getColumnMateProdQty() {
		getColumnMateProdQty=new TextColumn<MaterialInfo>() {
			@Override
			public String getValue(MaterialInfo object) {
				if(object.getMaterialProdQty()!=0){
					return object.getMaterialProdQty()+"";
				}
				return "";
			}
		};
		table.addColumn(getColumnMateProdQty,"Material QTY");
		table.setColumnWidth(getColumnMateProdQty, "110px");	
	}

	private void getColumnMateProdUom() {
		getColumnMateProdUom=new TextColumn<MaterialInfo>() {
			@Override
			public String getValue(MaterialInfo object) {
				if(object.getMatrialProdUom()!=null){
					return object.getMatrialProdUom()+"";
				}
				return "";
			}
		};
		table.addColumn(getColumnMateProdUom,"Material UOM");
		table.setColumnWidth(getColumnMateProdUom, "110px");	
	}

	@Override
	protected void initializekeyprovider() {
		
	}

	@Override
	public void addFieldUpdater() {
		
	}

	@Override
	public void setEnable(boolean state) {
		
	}

	@Override
	public void applyStyle() {
		
	}

}
