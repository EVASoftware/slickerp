package com.slicktechnologies.client.views.approval;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.CloseEvent;
import com.google.gwt.event.logical.shared.CloseHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.NoSelectionModel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreen;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreenPresenter;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.Slick_Erp;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.reusabledata.ConditionDialogBox;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.DropDownService;
import com.slicktechnologies.client.services.DropDownServiceAsync;
import com.slicktechnologies.client.services.EmailService;
import com.slicktechnologies.client.services.EmailServiceAsync;
import com.slicktechnologies.client.services.SmsService;
import com.slicktechnologies.client.services.SmsServiceAsync;
import com.slicktechnologies.client.services.XlsxService;
import com.slicktechnologies.client.services.XlsxServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.account.expensemanagment.ExpensemanagmentForm;
import com.slicktechnologies.client.views.account.expensemanagment.ExpensemanagmentPresenter;
import com.slicktechnologies.client.views.account.multipleexpensemanagment.MultipleExpensemanagmentForm;
import com.slicktechnologies.client.views.account.multipleexpensemanagment.MultipleExpensemanagmentPresenter;
import com.slicktechnologies.client.views.cnc.CNCForm;
import com.slicktechnologies.client.views.cnc.CNCPresenter;
import com.slicktechnologies.client.views.complain.ComplainForm;
import com.slicktechnologies.client.views.complain.ComplainPresenter;
import com.slicktechnologies.client.views.contract.ContractForm;
import com.slicktechnologies.client.views.contract.ContractPresenter;
import com.slicktechnologies.client.views.customer.CustomerForm;
import com.slicktechnologies.client.views.customer.CustomerPresenter;
import com.slicktechnologies.client.views.customertraining.CustomerTrainingForm;
import com.slicktechnologies.client.views.customertraining.CustomerTrainingPresenter;
import com.slicktechnologies.client.views.deliverynote.DeliveryNoteForm;
import com.slicktechnologies.client.views.deliverynote.DeliveryNotePresenter;
import com.slicktechnologies.client.views.humanresource.advance.AdvanceForm;
import com.slicktechnologies.client.views.humanresource.advance.AdvancePresenter;
import com.slicktechnologies.client.views.humanresource.leaveapplication.LeaveApplicationForm;
import com.slicktechnologies.client.views.humanresource.leaveapplication.LeaveApplicationPresenter;
import com.slicktechnologies.client.views.humanresource.timereport.TimeReportForm;
import com.slicktechnologies.client.views.humanresource.timereport.TimeReportPresenter;
import com.slicktechnologies.client.views.inventory.inspection.InspectionForm;
import com.slicktechnologies.client.views.inventory.inspection.InspectionPresenter;
import com.slicktechnologies.client.views.inventory.materialissuenote.MaterialIssueNoteForm;
import com.slicktechnologies.client.views.inventory.materialissuenote.MaterialIssueNotePresenter;
import com.slicktechnologies.client.views.inventory.materialmovementnote.MaterialMovementNotePresenter;
import com.slicktechnologies.client.views.inventory.materialmovementnote.MaterialMovementNoteForm;
import com.slicktechnologies.client.views.inventory.materialreuestnote.MaterialRequestNoteForm;
import com.slicktechnologies.client.views.inventory.materialreuestnote.MaterialRequestNotePresenter;
import com.slicktechnologies.client.views.inventory.recievingnote.GRNForm;
import com.slicktechnologies.client.views.inventory.recievingnote.GRNPresenter;
import com.slicktechnologies.client.views.inventory.warehousedetails.WareHouseDetailsForm;
import com.slicktechnologies.client.views.inventory.warehousedetails.WareHouseDetailsPresenter;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.BillingDetailsForm;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.BillingDetailsPresenter;
import com.slicktechnologies.client.views.paymentinfo.invoicedetails.InvoiceDetailsForm;
import com.slicktechnologies.client.views.paymentinfo.invoicedetails.InvoiceDetailsPresenter;
import com.slicktechnologies.client.views.purchase.letterofintent.LetterOfIntentForm;
import com.slicktechnologies.client.views.purchase.letterofintent.LetterOfIntentPresenter;
import com.slicktechnologies.client.views.purchase.purchaseorder.PurchaseOrderForm;
import com.slicktechnologies.client.views.purchase.purchaseorder.PurchaseOrderPresenter;
import com.slicktechnologies.client.views.purchase.purchaserequisition.PurchaseRequisitionForm;
import com.slicktechnologies.client.views.purchase.purchaserequisition.PurchaseRequitionPresenter;
import com.slicktechnologies.client.views.purchase.requestquotation.RequestForQuotationForm;
import com.slicktechnologies.client.views.purchase.requestquotation.RequestForQuotationPresenter;
import com.slicktechnologies.client.views.purchase.servicepo.ServicePoForm;
import com.slicktechnologies.client.views.purchase.servicepo.ServicePoPresenter;
import com.slicktechnologies.client.views.salesorder.SalesOrderForm;
import com.slicktechnologies.client.views.salesorder.SalesOrderPresenter;
import com.slicktechnologies.client.views.salesquotation.SalesQuotationForm;
import com.slicktechnologies.client.views.salesquotation.SalesQuotationPresenter;
import com.slicktechnologies.client.views.settings.employee.EmployeeForm;
import com.slicktechnologies.client.views.quotation.QuotationForm;
import com.slicktechnologies.client.views.quotation.QuotationPresenter;
import com.slicktechnologies.client.views.workorder.WorkOrderForm;
import com.slicktechnologies.client.views.workorder.WorkOrderPresenter;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.common.CustomerTrainingDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.Activity;
import com.slicktechnologies.shared.common.businessprocesslayer.ConcreteBusinessProcess;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Expense;
import com.slicktechnologies.shared.common.businessprocesslayer.LetterOfIntent;
import com.slicktechnologies.shared.common.businessprocesslayer.MultipleExpenseMngt;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseRequisition;
import com.slicktechnologies.shared.common.businessprocesslayer.RequsestForQuotation;
import com.slicktechnologies.shared.common.businessprocesslayer.ServicePo;
import com.slicktechnologies.shared.common.businessprocesslayer.VendorDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cnc.CNC;
import com.slicktechnologies.shared.common.complain.Complain;
import com.slicktechnologies.shared.common.deliverynote.DeliveryNote;
import com.slicktechnologies.shared.common.email.EmailDetails;
import com.slicktechnologies.shared.common.email.EmailTemplate;
import com.slicktechnologies.shared.common.helperlayer.SmsConfiguration;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
import com.slicktechnologies.shared.common.humanresourcelayer.advancelayer.Loan;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveApplication;
import com.slicktechnologies.shared.common.humanresourcelayer.timereport.TimeReport;
import com.slicktechnologies.shared.common.inventory.BillOfMaterial;
import com.slicktechnologies.shared.common.inventory.GRN;
import com.slicktechnologies.shared.common.inventory.Inspection;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.inventory.MaterialMovementNote;
import com.slicktechnologies.shared.common.inventory.MaterialRequestNote;
import com.slicktechnologies.shared.common.inventory.PhysicalInventoryMaintaince;
import com.slicktechnologies.shared.common.multilevelapproval.MultilevelApproval;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.productgroup.ProductGroupDetails;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesorder.SalesQuotation;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.smshistory.SmsHistory;
import com.slicktechnologies.shared.common.workorder.WorkOrder;

// TODO: Auto-generated Javadoc
/**
 * The Class ApprovalPresenter.Handels the Approvel Process.
 * To Do : Implemnt transections remove the two RPC in a single RPC.
 */
public class ApprovalPresenter extends TableScreenPresenter<Approvals> {
	
	/** The form. */
	
	public static  ApprovalForm form;
	String docType="";
	int docId=0;
	EmailServiceAsync emailService=GWT.create(EmailService.class);
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	final GenricServiceAsync async=GWT.create(GenricService.class);
	
	final SmsServiceAsync smsserviceAsync = GWT.create(SmsService.class);
	
	/***  for SMS global Variable*/
	private String templateMsgWithBraces;
	private long cellNo;
	private String companyName;
	private String actualMsg;
	
	
	//************rohan added this popup ********************
	int cnt=0,pestoIndia=0,vCare=0,omPestInvoice=0,hygeia=0,reddysPestControl=0;
	int customQuotation=0,pestQuotation=0,nbhc=0,universal=0,vcare=0,genesis=0;
	ConditionDialogBox conditionPopup=new ConditionDialogBox("Do you want to print on preprinted Stationery ? ",AppConstants.YES,AppConstants.NO);
	PopupPanel conditionpanel;
	Contract con= null;
	Quotation quotation=null;
	Invoice invoiceEntity=null;
	/*
	 * For Material View
	 */
	
	ArrayList<MaterialInfo> materialIst;
	int counter;
	int durCounter;
	
	ApprovalMaterialPopup materialpopup=new ApprovalMaterialPopup();
	PopupPanel panel = new PopupPanel(true);
	
	/** The Constant service. */
	public final static ApprovalServiceAsync service=GWT.create(ApprovalService.class);
	
	/*
	 *  nidhi
	 *  4-07-2017
	 */
	public static PopupPanel docApprovalPanel = null;
	VerticalPanel docVerticalPanel = null;
	FlowPanel docflowpanel = null;
	
	/**
	 * End
	 */
	/**
	 *  nidhi
	 *  5-10-2017
	 *  for approval and reject button on 
	 */
	/**
	 * Instantiates a new approval presenter.
	 *
	 */ 
	   
		Button btnApproval, btnReject;
		VerticalPanel mainDocVerticalPanel = null;
		/**
		 * end
		 */
		/**
		 *  nidhi
		 *  5-10-2017
		 *  for popup approval button
		 */
		Approvals appobj;
		int rowIndex = 0;
		/**
		 * end
		 */
	/**
	 * Instantiates a new approval presenter.
	 *
	 * @param view the view
	 * @param model the model
	 */
	public ApprovalPresenter(TableScreen<Approvals> view, Approvals model) {
		super(view, model);
		form=(ApprovalForm) view;
		form.table.connectToLocal();
		Console.log("Rohan Inside constructor");
		form.retriveTable(getApprovalQuery());
		form.getTable().setPresenter(this);
		setSelectionOnApprovalSelection();
		form.getSearchpopupscreen().getDwnload().setVisible(false);
		
		materialpopup.getBtnDownload().addClickHandler(this);
		materialpopup.getBtnClose().addClickHandler(this);
		materialpopup.getBtnPrint().addClickHandler(this);
		
		
		//**********rohan ************************
		conditionPopup.getBtnOne().addClickHandler(this);
		conditionPopup.getBtnTwo().addClickHandler(this);
		
		/**
		 *  nidhi
		 *  5-10-2017
		 *  for approval and reject button on 
		 */
		docApprovalPanel = new PopupPanel(true);
		
		docApprovalPanel.addCloseHandler(new CloseHandler<PopupPanel>() {
			@Override
			public void onClose(CloseEvent<PopupPanel> event) {
				
				Console.log("Inside close event..");
				AppMemory.getAppMemory().currentScreen = Screen.APPROVAL;
				form.initializeScreen();
				form.intializeScreenMenus();
				form.addClickEventOnScreenMenus();
				form.addClickEventOnActionAndNavigationMenus();
				
//				for (int k = 0; k < mem.skeleton.menuLabels.length; k++) {
//					if (mem.skeleton.registration[k] != null)
//						mem.skeleton.registration[k].removeHandler();
//				}
//				for (int k = 0; k < mem.skeleton.menuLabels.length; k++)
//					mem.skeleton.registration[k] = mem.skeleton.menuLabels[k].addClickHandler(this);
				form.setToViewState();
			}
		});
		
		docflowpanel = new FlowPanel();
		
		btnApproval = new Button("Approve");
		btnReject = new Button("Reject");
	
		btnApproval.getElement().addClassName("approvalBtn");
		btnReject.getElement().addClassName("approvalBtn");
		
		mainDocVerticalPanel = new VerticalPanel();
		btnApproval.addClickHandler(this);
		btnReject.addClickHandler(this);
		
		docflowpanel.add(btnApproval);
		docflowpanel.add(btnReject);
		
		mainDocVerticalPanel.add(docflowpanel);
		
		docVerticalPanel = new VerticalPanel();
		mainDocVerticalPanel.add(docVerticalPanel);
				
		docApprovalPanel.add(mainDocVerticalPanel);
				
		
	}
	
	
	public static void showMessage(String msg){
		form.showDialogMessage(msg);
	}
	

	/**
	 * Initalize.
	 */
	public static void initalize()
	{
		
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Approval/Approval",Screen.APPROVAL);

		/**
		 * @author Anil , Date : 22-04-2020
		 * Loading next level approver list at the time of approval screen initialization if it is  not already loaded
		 */
		if(LoginPresenter.globalMultilevelApproval.size()==0){
			Console.log("Retriving Approver!!");
			globalMakeLiveMultiLevelApproval();
		}
		ApprovalTable table=new ApprovalTable();
		ApprovalForm form=new ApprovalForm(table);
		ApprovalSearchPopup.staticSuperTable=table;
		ApprovalSearchPopup searchPopUp=new ApprovalSearchPopup(false);
		form.setSearchpopupscreen(searchPopUp);
		/**Added by sheetal : 08-03-2022
		 * Approval window to occupy full screen
		 */
		int height = Window.getClientHeight();
		Console.log("Window Height : " + height);
		height = height * 80 / 100;
		Console.log("Updated Height : " + height);
		searchPopUp.getSupertable().getTable().setHeight(height+"px");
		/**end**/
		ApprovalPresenter presenter=new ApprovalPresenter(form, new Approvals());
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Approval/Approval",Screen.APPROVAL);
		form.setToViewState();
		AppMemory.getAppMemory().stickPnel(form);
	}
	
	/*
	 * Method template to set the query object in specific to search criteria and return the same
	 * @return MyQuerry object
	 */
	/**
	 * Fills the Approval Process With data of Pending Approvels.
	 *
	 * @return the approval query
	 */
	public MyQuerry getApprovalQuery()
	{
		/**
		 * Date : 6-03-2017 BY Anil
		 */
		List<String>statusList=new ArrayList<String>();
		statusList.add("In Process");
		statusList.add(Approvals.PENDING);
		
		Vector<Filter> vecfilter=new Vector<Filter>();
		Filter filter =null;
//		filter =new Filter();
//		filter.setStringValue(Approvals.PENDING);
//		filter.setQuerryString("status");
//		vecfilter.add(filter);
		
		filter =new Filter();
		filter.setList(statusList);
		filter.setQuerryString("status IN");
		vecfilter.add(filter);
		
		filter=new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(model.getCompanyId());
		vecfilter.add(filter);
		filter=new Filter();
		String employeeName=UserConfiguration.getInfo().getFullName().trim();
		filter.setStringValue(employeeName);
		filter.setQuerryString("approverName");
		vecfilter.add(filter);
		MyQuerry quer=new MyQuerry(vecfilter, new Approvals());
		return quer;
	}
	
	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.EntityPresenter#reactToProcessBarEvents(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		
		InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();
		if(text.equals(AppConstants.APPROVALVIEWDOCUMENT)){
		
			if(docType.equals("")){
				form.showDialogMessage("Please select atleast one record!");
			}
			else{
				/**
				 * Old code for viewing approvable document
				 */
//				viewSelectedDocument();
				/**
				 * This is new code by Nidhi
				 */
				viewSelectedDocumentOnPopup();
			}
		}	
		
		if(text.equals(AppConstants.APPROVALPRINTDOCUMENT)){
			
			
			if(docType.equals("")){
				form.showDialogMessage("Please select atleast one document to print!");
			}
			else{
				System.out.println("Doc Type"+docType);
				printSelectedDocument();
			}
		}	
		
		
		if(text.equals(AppConstants.APPROVALVIEWMATERIAL)){
			
			if(docType.equals("")){
				form.showDialogMessage("Please select atleast one record!");
			}
			else{
				viewSelectedDocumentMaterial();
			}
		}
		
	}

	/**
	 * Manage the Approvel/Rejection Process on Button click
	 *
	 * @param model the model
	 * @param status the status
	 */
	public  void manageBusinessProcessStatus(final Approvals model,final String status,final String Successmessage,final String failureMessage)
	{

		/**
		 * Date : 11-03-2017 By Anil
		 * This query is used to check whether the selected document is in approval/In process state
		 * If this is the case we will not approve the records
		 */
		Vector<Filter> temp=new Vector<Filter>();
		MyQuerry query=new MyQuerry();
		Filter filter=null;
		
		filter=new Filter();
		filter.setQuerryString("businessprocessId");
		filter.setIntValue(model.getBusinessprocessId());
		temp.add(filter);
		
		filter=new Filter();
		filter.setQuerryString("businessprocesstype");
		filter.setStringValue(model.getBusinessprocesstype());
		temp.add(filter);
		
		filter=new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(model.getCount());
		temp.add(filter);
		
		filter=new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(model.getCompanyId());
		temp.add(filter);
		
		query.setFilters(temp);
		query.setQuerryObject(new Approvals());
		
		async.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {

			@Override
			public void onFailure(Throwable caught) {
				view.hideWaitSymbol();
				view.showDialogMessage("Unexpected error occured!");
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				Console.log("Approval Lis Size : "+result.size());
				if(result.size()==0){
					view.hideWaitSymbol();
					view.showDialogMessage("No record found for approval.");
					return;
				}else if(result.size()>1){
					/*** Date 01-11-2019 By Vijay for NBHC CCPM Only with process config ****/
					if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "EnableNewInvoiceIdForDuplicateId") 
							&& model.getBusinessprocesstype().trim().equals("Invoice Details")){
						Console.log("For New invoice id Logic written in ApprovableserviceImpl");
					}
					else{
						view.hideWaitSymbol();
						view.showDialogMessage("Multiple records found for approval.");
						return;
					}
					
				}else{
					Approvals approval=(Approvals) result.get(0);
					if(approval.getStatus().equals(Approvals.APPROVED)){
						view.hideWaitSymbol();
						view.showDialogMessage("Document is already in approved state.");
						return;
					}else if(approval.getStatus().equals("In Process")){
						view.hideWaitSymbol();
						view.showDialogMessage("Document is in process state.");
						return;
					}
					else if(approval.getStatus().equals(Approvals.CANCELLED)){
						view.hideWaitSymbol();
						view.showDialogMessage("Document is in cancelled state.");
						return;
					}else if(approval.getStatus().equals(Approvals.REJECTED)){
						view.hideWaitSymbol();
						view.showDialogMessage("Document is in rejected state.");
						return;
					}
				}
				
				
				
				/**
				 * Date : 11/2/2017
				 * By : Anil Pal 
				 * This method save approval in processed state then after send for approval process
				 * If any error occur user will not allowed to approve for 2nd time 
				 * he has to contact EVA 
				 */
				
				model.setStatus(ConcreteBusinessProcess.PROCESSED);
				async.save(model, new AsyncCallback<ReturnFromServer>() {
					@Override
					public void onSuccess(ReturnFromServer result) {
						model.setStatus(status);
						service.sendApproveRequest(model, new AsyncCallback<String>()
						{
							@Override
							public void onFailure(Throwable caught) {
								view.hideWaitSymbol();
								view.showDialogMessage(failureMessage);
								
							}
							@Override
							public void onSuccess(String result) {
								if(result.equals("Success")){
									view.showDialogMessage(Successmessage,GWTCAlert.OPTION_ROUNDED_BLUE ,GWTCAlert.OPTION_ANIMATION);
									reactOnApprovalSuccessEmail(model);
									if(model.getBusinessprocesstype().trim().equalsIgnoreCase(AppConstants.APPROVALDELIVERYNOTE)){
										checkSmsConfigVal(model);
									}else if(model.getBusinessprocesstype().trim().equalsIgnoreCase(AppConstants.APPROVALPURCHASEORDER)){
										reactOnPOApproval(model);
									}
								}else{
									view.showDialogMessage(result);
								}
								view.hideWaitSymbol();
							}
							
						});
					}
					@Override
					public void onFailure(Throwable caught) {
						view.hideWaitSymbol();
					}
				});
				
			}
		});
		
		
	}
	
	/**
	 *     Added By : Priyanka Bhagwat
	 *     Date : 1/06/2021
	 *     Des : When Submit PO then SMS and EMAIL will be send Req by Pecopp raised By Ashwini.	
	 */
	private void reactOnPOApproval(Approvals model) {
		// TODO Auto-generated method stub
		/**
		 * Priyanka
		 * First load PO
		 * Then write same method that you have used in PuchaseOrderPresenter
		 * write sms and email method here
		 */
		
		int poId=model.getBusinessprocessId();
		
		final MyQuerry querry=new MyQuerry();
		Vector<Filter> temp=new Vector<Filter>();
		Filter filter=null;
		
		filter=new Filter();
		filter.setQuerryString("count");
	
		filter.setIntValue(poId);
		temp.add(filter);
		
		querry.setFilters(temp);
		querry.setQuerryObject(new PurchaseOrder());
		
		async.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						
						if (result.size() != 0) {
							Console.log("Result");
							for (SuperModel model1 : result) {
								PurchaseOrder model = (PurchaseOrder) model1;
//								int vendorId = 0;
//								if (model != null) {
//									if (model.getVendorDetails() != null
//											&& model.getVendorDetails().size() != 0) {
//										for (VendorDetails vendor : model
//												.getVendorDetails()) {
//											if (vendor.getStatus()) {//
//												// if(vendor.getVendorPhone()!=null&&!vendor.getVendorPhone().equals("")))
//												// {
//												if (vendor.getVendorPhone() != 0) {
//													Console.log("Vendor contact"
//															+ vendor.getVendorPhone());
//													cellNo = vendor
//															.getVendorPhone();
//												} else {
//													cellNo = AppUtility
//															.getVendorCellNo(vendorId);
//												}
//												vendorId = vendor.getVendorId();
//											}
//										}
//									}
//								}
								checkSendSmsStatus(model);
								reactOnEmailPO(model);
							}
						}
					}

					@Override
					public void onFailure(Throwable caught) {

					}
				});
	
		
		
	}
	
	private void reactOnEmailPO(PurchaseOrder model) {

		String toEmail = "";
		String fromEmail = "";
		int vendorId = 0;

		String branchName = model.getBranch();
		fromEmail = AppUtility.getFromEmailAddress(branchName,model.getEmployee());
		// logger.log(Level.SEVERE,"Inside Email from mailId:" +fromEmail);
		if (fromEmail.equals("")) {
			return;
		}

		if (model != null) {
			if (model.getVendorDetails() != null
					&& model.getVendorDetails().size() != 0) {
				for (VendorDetails vendor : model.getVendorDetails()) {
					if (vendor.getStatus()) {//
						if (vendor.getVendorEmailId() != null
								&& !vendor.getVendorEmailId().equals("")) {
							toEmail = vendor.getVendorEmailId();
						}
						vendorId = vendor.getVendorId();
					}
				}

				if (toEmail.equals("")) {
					toEmail = AppUtility.getVendorEmailid(vendorId);
//					logger.log(Level.SEVERE, "Inside Email to mailId:"
//							+ toEmail);
					// toEmail = AppUtility.getvenodrEmailId(vendorlist,false);
				}
			}
		}

		if (toEmail.equals("")) {
			return;
		}

		EmailDetails email = new EmailDetails();
		email.setFromEmailid(fromEmail);
		ArrayList<String> toemailId = new ArrayList<String>();
		// ArrayList<String> toemailId = new ArrayList<String>();
		// toemailId = getEmailIdlist(taToEmailId);
		toemailId.add(toEmail);
		email.setToEmailId(toemailId);

		ArrayList<String> ccemailId = new ArrayList<String>();
		email.setCcEmailId(ccemailId);

		ArrayList<String> bccemailId = new ArrayList<String>();
		email.setBccEmailId(bccemailId);

		EmailTemplate emailTemp = null;
		if (LoginPresenter.globalEmailTemplatelist != null
				&& LoginPresenter.globalEmailTemplatelist.size() != 0) {
			for (EmailTemplate obj : LoginPresenter.globalEmailTemplatelist) {
				if (obj.getTemplateName().equals("Email PO On Approval")
						&& obj.isTemplateStatus()) { //
					emailTemp = obj;
					break;
				}
			}
		}

		String emailSubject = "";
		String emailBody = "";
		if (emailTemp == null) {
			return;
		}

		emailSubject = emailTemp.getSubject();
		Console.log("emailTemp.getSubject()");
		emailSubject = emailSubject.replace("{PurchaseOrderNo}",
				model.getCount() + "");
		email.setSubject(emailSubject);

		emailBody = emailTemp.getEmailBody();
		emailBody = emailBody.replace("{PurchaseOrderNo}", model.getCount()+ "");
		
		final String serDate = AppUtility.parseDate(model.getPODate());
		emailBody = emailBody.replace("{PODate}", serDate);// Slick_Erp.businessUnitName;
		emailBody = emailBody.replace("{companyName}",Slick_Erp.businessUnitName + "");

		emailBody = emailBody.replaceAll("[\n]", "<br>");
		emailBody = emailBody.replaceAll("[\\s]", "&nbsp;");
		email.setEmailBody(emailBody);

		email.setPdfAttachment(emailTemp.isPdfattachment());
		Console.log("emailTemp.isPdfattachment()" + emailTemp.isPdfattachment());

//		Screen screenName = (Screen) AppMemory.getAppMemory().currentScreen;
//		String EntityName = AppUtility.getEntityName(screenName);
		email.setEntityName("PurchaseOrder");
//		Console.log("screenName" + screenName);
//		Console.log("EntityName" + EntityName);
		if (model != null) {
			email.setModel(model);
		}

		EmailServiceAsync emailAsync = GWT.create(EmailService.class);
		emailAsync.sendEmail(email, model.getCompanyId(),
				new AsyncCallback<String>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
					}

					@Override
					public void onSuccess(String result) {
						// TODO Auto-generated method stub
					}
				});

	}
	
	private void checkSendSmsStatus(final PurchaseOrder po) {

		Console.log("Inside check sms status");
//		Vector<Filter> filtrvec = new Vector<Filter>();
//		Filter filtr = null;
//
//		filtr = new Filter();
//		filtr.setLongValue(model.getCompanyId());
//		filtr.setQuerryString("companyId");
//		filtrvec.add(filtr);
//
//		filtr = new Filter();
//		filtr.setBooleanvalue(true);
//		filtr.setQuerryString("status");
//		filtrvec.add(filtr);
//
//		MyQuerry querry = new MyQuerry();
//		querry.setFilters(filtrvec);
//		querry.setQuerryObject(new SmsConfiguration());
//		// form.showWaitSymbol();
//		Console.log("status of sms");
//		async.getSearchResult(querry,
//				new AsyncCallback<ArrayList<SuperModel>>() {
//
//					@Override
//					public void onSuccess(ArrayList<SuperModel> result) {
//						Console.log("Success of check sms status");
//						if (result.size() != 0) {
//							Console.log("Result");
//							for (SuperModel model : result) {
//								SmsConfiguration smsconfig = (SmsConfiguration) model;
//								boolean smsStatus = smsconfig.getStatus();
//								String accountsid = smsconfig.getAccountSID();
//								String authotoken = smsconfig.getAuthToken();
//								String fromnumber = smsconfig.getPassword();
//								Console.log("Status" + smsStatus);
//								if (smsStatus == true) {
//									sendSMS(accountsid, authotoken, fromnumber,po);
//								}
//							}
//						}
//					}
//
//					@Override
//					public void onFailure(Throwable caught) {
//
//					}
//				});
		
		sendSMS(po);

	}
	
	
	private void sendSMS(final PurchaseOrder po) {

		// if(model.getStatus().equals(AppConstants.SUBMIT)){
		Vector<Filter> filtrvec = new Vector<Filter>();
		Filter filtr = null;
		filtr = new Filter();
		filtr.setLongValue(po.getCompanyId());
		filtr.setQuerryString("companyId");
		filtrvec.add(filtr);

		filtr = new Filter();
		filtr.setStringValue("Purchase Order");
		filtr.setQuerryString("event");
		filtrvec.add(filtr);

		filtr = new Filter();
		filtr.setBooleanvalue(true);
		filtr.setQuerryString("status");
		filtrvec.add(filtr);

		MyQuerry querry = new MyQuerry();
		querry.setFilters(filtrvec);
		querry.setQuerryObject(new SmsTemplate());

		async.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						Console.log("Inside Success method of sms");
						if (result.size() != 0) {
							Console.log("Result" + result.size());
							for (SuperModel model1 : result) {
								SmsTemplate sms = (SmsTemplate) model1;

								int vendorId = 0;
								if (po != null) {
									if (po.getVendorDetails() != null
											&& po.getVendorDetails().size() != 0) {
										for (VendorDetails vendor : po.getVendorDetails()) {
											if (vendor.getStatus()) {
												if (vendor.getVendorPhone() != 0) {
													Console.log("Vendor contact"+ vendor.getVendorPhone());
													cellNo = vendor.getVendorPhone();
												} else {
													cellNo = AppUtility.getVendorCellNo(vendorId);
												}
												vendorId = vendor.getVendorId();
											}
										}
									}
								}

								Console.log("sms getMsg" + sms.getMessage());
								String templateMsgwithbraces = sms.getMessage();
								String purchaseno =po.getCount()+"";
								
								Date podate = po.getPODate();
								String frminvoiceDate = AppUtility.parseDate(podate);

								// Console.log("Vendor contact"+vendor.getCellNumber1());
								// cellNo = vendor.getCellNumber1();

								String purchaseorderno = templateMsgwithbraces
										.replace("{PurchaseOrderNo}",
												purchaseno);
								String purDate = purchaseorderno.replace(
										"{PODate}", frminvoiceDate);
								companyName = Slick_Erp.businessUnitName;
								actualMsg = purDate.replace("{CompanyName}",
										companyName);

								// companyName = Slick_Erp.businessUnitName;
								System.out.println("Actual MSG" + actualMsg
										+ "\n" + companyName);

//								smsserviceAsync.sendSmsToClient(actualMsg,
//										cellNo, accountsid, authotoken,
//										fromnumber, model1.getCompanyId(),
//										new AsyncCallback<Integer>() {
//
//											@Override
//											public void onSuccess(Integer result) {
//												// form.hideWaitSymbol();
//												Console.log("Inside Success sms 2");
//												if (result == 1) {
//													form.showDialogMessage("SMS Sent Successfully!");
//													savesmshistory();
//												}
//											}
//
//											@Override
//											public void onFailure(
//													Throwable caught) {
//
//											}
//										});
								
								smsserviceAsync.sendMessage(AppConstants.PURCHASEMODULE, AppConstants.PURCHASEORDER, sms.getEvent(), model1.getCompanyId(), actualMsg, cellNo,false, new AsyncCallback<String>() {
									
									@Override
									public void onSuccess(String result) {
										// TODO Auto-generated method stub
										form.hideWaitSymbol();
										form.showDialogMessage("Message Sent Successfully!");

									}
									
									@Override
									public void onFailure(Throwable caught) {
										// TODO Auto-generated method stub
										form.hideWaitSymbol();
									}
								});
								
							}
						}
					}

					@Override
					public void onFailure(Throwable caught) {
					}
				});
		// }
	}
	
	private void savesmshistory() {
		SmsHistory smshistory = new SmsHistory();
		smshistory.setDocumentId(model.getCount());
		smshistory.setDocumentType("Purchase Order");
		//smshistory.setName(Form.getTbVendorName().getValue());
		smshistory.setCellNo(cellNo);
		smshistory.setSmsText(actualMsg);
		smshistory.setUserId(LoginPresenter.loggedInUser);

		async.save(smshistory, new AsyncCallback<ReturnFromServer>() {

			@Override
			public void onSuccess(ReturnFromServer result) {
				System.out.println("Data Saved Successfully");
			}

			@Override
			public void onFailure(Throwable caught) {
				System.out.println("Data Save Unsuccessfull");
			}
		});

	}
	
	/**
	 * Retrieve business process.
	 *
	 * @param businessprocessType the businessprocess type
	 * @param businessprocessId the businessprocess id
	 * @param status the status
	 */
	
	
	/**
	 * Return doc object.
	 *
	 * @param businessprocesstype the businessprocesstype
	 * @return the concrete business process
	 */
	
	
	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.EntityPresenter#reactOnPrint()
	 */
	@Override
	public void reactOnPrint() {
		printingApprovalDocument();
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.EntityPresenter#reactOnEmail()
	 */
	@Override
	public void reactOnEmail() {
		
	}
	
	
	@Override
	public void reactOnDownload() {
		
		if(form.getSuperTable().getDataprovider().getList().size()==0)
		{
			form.showDialogMessage("No record in Approval List to download");
		}
		
		if(form.getSuperTable().getDataprovider().getList().size()>0)
		{
	
		ArrayList<Approvals> apprarray=new ArrayList<Approvals>();
		List<Approvals> list=form.getSuperTable().getListDataProvider().getList();
		apprarray.addAll(list); 
		
		csvservice.setApprovalsList(apprarray, new AsyncCallback<Void>() {
	
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);
			}
	
			@Override
			public void onSuccess(Void result) {
				
				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "csvservlet"+"?type="+76;
				Window.open(url, "test", "enabled");
			}
		});
	  }
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.EntityPresenter#makeNewModel()
	 */
	@Override
	protected void makeNewModel() {
		this.model=new Approvals();
	}
	
	
	/******************************************************************************************************/
	
	/**
	 * This method is used for retrieving Document Type and Document Id on selection of record in approval
	 * table.
	 */
	
	private void setSelectionOnApprovalSelection() {
		
		 final NoSelectionModel<Approvals> selectionModelMyObj = new NoSelectionModel<Approvals>();
	    SelectionChangeEvent.Handler  tableHandler = new SelectionChangeEvent.Handler() 
	    {
	        @Override
	        public void onSelectionChange(SelectionChangeEvent event) 
	        {
	       	 final Approvals entity=selectionModelMyObj.getLastSelectedObject();
	       	 
	       	 	 docType = entity.getBusinessprocesstype().trim();
	       	 	 docId=entity.getBusinessprocessId();
	       	 	 /**
	       	 	  * nidhi
	       	 	  * 5-10-2017
	       	 	  * pop up approval process
	       	 	  */
	       	 	appobj = entity;
	       	 	 
	       	 	 List<Approvals> arrList =  form.table.getDataprovider().getList();
	       	 	 int  i = 0;
	       	 	 for(Approvals app : arrList){
	       	 		 /**
	       	 		  * Date : 28-12-2018 BY ANIL
	       	 		  * approval ok button click was not working because of increment,as data grid count starts from 0.
	       	 		  */
//	       	 		 i++;
	       	 		 if(app.equals(appobj)){
	       	 			rowIndex = i;
	       	 			 break;
	       	 		 }
	       	 		 i++;
	       	 		 /**
	       	 		  * End
	       	 		  */
	       	 	 }
	       	 	 /**
	       	 	  * end
	       	 	  */
	         }
	    };
	    
	    // Add the handler to the selection model
	    selectionModelMyObj.addSelectionChangeHandler( tableHandler );
	    // Add the selection model to the table
	    form.getTable().getTable().setSelectionModel(selectionModelMyObj);
	}
	
	
	/**
	 * Method for viewing selected document
	 */
	

	private void viewSelectedDocument()
	{
		if(docType.equals(AppConstants.APPROVALCONTRACT)){
			setTableSelectionForContract();	
		}
		if(docType.equals(AppConstants.APPROVALDELIVERYNOTE)){
			setTableSelectionForDeliveryNote();	
		}
		if(docType.equals(AppConstants.APPROVALSALESORDER)){
			setTableSelectionForSalesOrder();
		}
		if(docType.equals(AppConstants.APPROVALQUOTATION)){
			setTableSelectionForQuotation();
		}
		if(docType.equals(AppConstants.APPROVALSALESQUOTATION)){
			setTableSelectionForSalesQuotation();
		}
		if(docType.equals(AppConstants.APPROVALPURCHASEORDER)){
			setTableSelectionForPurchaseOrder();
		}
		if(docType.equals(AppConstants.APPROVALGRN)){
			setTableSelectionForGRN();
		}
		if(docType.equals(AppConstants.APPROVALPURCHASEREQUISITION)){
			setTableSelectionForPurchaseRequisition();	
		}
		if(docType.equals(AppConstants.APPROVALEXPENSEMANAGMENT)){
			setTableSelectionForExpenseManagement();
		}
		if(docType.equals(AppConstants.APPROVALMATERIALISSUENOTE)){
			setTableSelectionForMaterialIssueNote();
		}
		if(docType.equals(AppConstants.APPROVALMATERIALMOVEMENTNOTE)){
			setTableSelectionForMaterialMovementNote();
		}
		if(docType.equals(AppConstants.APPROVALMATERIALREQUESTNOTE)){
			setTableSelectionForMaterialRequestNote();
		}
		if(docType.equals(AppConstants.APPROVALREQUESTFORQUOTATION)){
			setTableSelectionForRequestForQuotation();
		}
		if(docType.equals(AppConstants.APPROVALINVOICEDETAILS)){
			setTableSelectionForInvoiceDetails();
		}
		if(docType.equals(AppConstants.APPROVALBILLINGDETAILS)){
			setTableSelectionForBillingDetails();
		}
		if(docType.equals(AppConstants.APPROVALLETTEROFINTENT)){
			setTableSelectionForLetterOfIntent();
		}
		if(docType.equals(AppConstants.APPROVALLEAVEAPPLICATION)){
			setTableSelectionForLeaveApplication();
		}
		if(docType.equals(AppConstants.APPROVALCOMPLAIN)){
			setTableSelectionForComplain();
		}
		if(docType.equals(AppConstants.APPROVALADVANCE)){
			setTableSelectionForAdvanceRequest();
		}
		if(docType.equals(AppConstants.APPROVALINSPECTION)){
			setTableSelectionForInspectionRequest();
		}
		if(docType.equals(AppConstants.APPROVALWORKORDER)){
			setTableSelectionForWorkOrderRequest();
		}
		
		if(docType.equals(AppConstants.APPROVALSERVICEPO)){
			setTableSelectionForServicePoRequest();
		}
		
		if(docType.equals(AppConstants.APPROVALMULTIPLEEXPENSEMANAGMENT)){
			setTableSelectionForMultipleExpenseRequest();
		}
		/**
		 * Date: 17-06-2017 BY ANIL
		 * View document for Customer
		 */
		if(docType.equals(AppConstants.APPROVALCUSTOMER)){
			setTableSelectionForCustomerRequest();
		}
		/**
		 * End
		 */
		
		if(docType.equals(AppConstants.APPROVALCAMPAIGNMANNAGEMENT)){
			setTableSelectionForcampainMgnt();
		}
		/**DATE 29.9.2018 added by komal for cnc**/
		if(docType.equals(AppConstants.CNC)){
			setTableSelectionForCNC();
		}
		
	}
	
	
	/*****************************************For Printing Document*****************************************/
	
	public void printSelectedDocument()
	{
		if(docType.equals(AppConstants.APPROVALCONTRACT)){
			reactOnPrint();
		}
		if(docType.equals(AppConstants.APPROVALDELIVERYNOTE)){
			reactOnPrint();
		}
		if(docType.equals(AppConstants.APPROVALSALESORDER)){
			reactOnPrint();
		}
		if(docType.equals(AppConstants.APPROVALQUOTATION)){
			reactOnPrint();
		}
		if(docType.equals(AppConstants.APPROVALSALESQUOTATION)){
			reactOnPrint();
		}
		if(docType.equals(AppConstants.APPROVALPURCHASEORDER)){
			reactOnPrint();
		}
		if(docType.equals(AppConstants.APPROVALGRN)){
			reactOnPrint();
		}
		if(docType.equals(AppConstants.APPROVALPURCHASEREQUISITION)){
			reactOnPrint();
		}
		if(docType.equals(AppConstants.APPROVALEXPENSEMANAGMENT)){
			reactOnPrint();
		}
		if(docType.equals(AppConstants.APPROVALMATERIALISSUENOTE)){
			reactOnPrint();
		}
		if(docType.equals(AppConstants.APPROVALMATERIALMOVEMENTNOTE)){
			reactOnPrint();
		}
		if(docType.equals(AppConstants.APPROVALMATERIALREQUESTNOTE)){
			reactOnPrint();
		}
		if(docType.equals(AppConstants.APPROVALREQUESTFORQUOTATION)){
			reactOnPrint();
		}
		if(docType.equals(AppConstants.APPROVALINVOICEDETAILS)){
			reactOnPrint();
		}
		if(docType.equals(AppConstants.APPROVALBILLINGDETAILS)){
			reactOnPrint();
		}
		if(docType.equals(AppConstants.APPROVALLETTEROFINTENT)){
			reactOnPrint();
		}
		if(docType.equals(AppConstants.APPROVALLEAVEAPPLICATION)){
			reactOnPrint();
		}
		if(docType.equals(AppConstants.APPROVALCOMPLAIN)){
			reactOnPrint();
		}
		if(docType.equals(AppConstants.APPROVALADVANCE)){
			reactOnPrint();
		}
		if(docType.equals(AppConstants.APPROVALINSPECTION)){
			reactOnPrint();
		}
		if(docType.equals(AppConstants.APPROVALSERVICEPO)){
			reactOnPrint();
		}
		if(docType.equals(AppConstants.APPROVALMULTIPLEEXPENSEMANAGMENT)){
			reactOnPrint();
		}
		/**
		 * Date : 12-09-2018 BY ANIL
		 */
		if(docType.equals(AppConstants.APPROVALEMPLOYEE)){
			printingApprovalDocument();
		}
		/** date 29.09.2018 added by komal**/
		if(docType.equals(AppConstants.CNC)){
			reactOnCNCDownload();
		}
	}
	
	
	
	/**
	 * Querries for selected approval document.
	 */
	
	
	private MyQuerry approvalSelectionQuerry(){
		
		MyQuerry querry=new MyQuerry();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		
		temp=new Filter();
		temp.setQuerryString("companyId");
		temp.setLongValue(model.getCompanyId());
		filtervec.add(temp);
		
		temp=new Filter();
		temp.setQuerryString("count");
		temp.setIntValue(docId);
		filtervec.add(temp);
		
		querry.setFilters(filtervec);
		
		if(docType.equals(AppConstants.APPROVALCONTRACT)){
			querry.setQuerryObject(new Contract());
		}else if(docType.equals(AppConstants.APPROVALSALESORDER)){
			querry.setQuerryObject(new SalesOrder());
		}else if(docType.equals(AppConstants.APPROVALQUOTATION)){
			querry.setQuerryObject(new Quotation());
		}else if(docType.equals(AppConstants.APPROVALSALESQUOTATION)){
			querry.setQuerryObject(new SalesQuotation());
		}else if(docType.equals(AppConstants.APPROVALPURCHASEORDER)){
			querry.setQuerryObject(new PurchaseOrder());
		}else if(docType.equals(AppConstants.APPROVALGRN)){
			querry.setQuerryObject(new GRN());
		}else if(docType.equals(AppConstants.APPROVALPURCHASEREQUISITION)){
			querry.setQuerryObject(new PurchaseRequisition());
		}else if(docType.equals(AppConstants.APPROVALEXPENSEMANAGMENT)){
			querry.setQuerryObject(new Expense());
		}else if(docType.equals(AppConstants.APPROVALMATERIALISSUENOTE)){
			querry.setQuerryObject(new MaterialIssueNote());
		}else if(docType.equals(AppConstants.APPROVALMATERIALMOVEMENTNOTE)){
			querry.setQuerryObject(new MaterialMovementNote());
		}else if(docType.equals(AppConstants.APPROVALMATERIALREQUESTNOTE)){
			querry.setQuerryObject(new MaterialRequestNote());
		}else if(docType.equals(AppConstants.APPROVALREQUESTFORQUOTATION)){
			querry.setQuerryObject(new RequsestForQuotation());
		}else if(docType.equals(AppConstants.APPROVALINVOICEDETAILS)){
			querry.setQuerryObject(new Invoice());
		}else if(docType.equals(AppConstants.APPROVALBILLINGDETAILS)){
			querry.setQuerryObject(new BillingDocument());
		}else if(docType.equals(AppConstants.APPROVALLETTEROFINTENT)){
			querry.setQuerryObject(new LetterOfIntent());
		}else if(docType.equals(AppConstants.APPROVALLEAVEAPPLICATION)){
			querry.setQuerryObject(new LeaveApplication());
		}else if(docType.equals(AppConstants.APPROVALCOMPLAIN)){
			querry.setQuerryObject(new Complain());
		}else if(docType.equals(AppConstants.APPROVALADVANCE)){
			querry.setQuerryObject(new Loan());
		}
		else if(docType.equals(AppConstants.APPROVALDELIVERYNOTE)){
			querry.setQuerryObject(new DeliveryNote());
		}
		else if(docType.equals(AppConstants.APPROVALINSPECTION)){
			querry.setQuerryObject(new Inspection());
		}
		else if(docType.equals(AppConstants.APPROVALWORKORDER)){
			querry.setQuerryObject(new WorkOrder());
		}
		else if(docType.equals(AppConstants.APPROVALSERVICEPO)){
			querry.setQuerryObject(new ServicePo());
		}
		else if(docType.equals(AppConstants.MULTIPLEEXPENSEMANAGEMET)){
			querry.setQuerryObject(new MultipleExpenseMngt());
		}else if(docType.equals(AppConstants.CUSTOMER)){
			querry.setQuerryObject(new Customer());
		}else if(docType.equals(AppConstants.APPROVALCAMPAIGNMANNAGEMENT)){
			querry.setQuerryObject(new CustomerTrainingDetails());

		}else if(docType.equals(AppConstants.APPROVALTIMEREPORT)){
			querry.setQuerryObject(new TimeReport());

		}else if(docType.equals(AppConstants.APPROVALEMPLOYEE)){
			querry.setQuerryObject(new Employee());
		}else if(docType.equals(AppConstants.CNC)){/** date 29.9.2018 added by komal */
			querry.setQuerryObject(new CNC());
		}else if(docType.equals(AppConstants.PHYSICALINVENTORY)){/** date 12-4.2018 added by nidhi */
			querry.setQuerryObject(new PhysicalInventoryMaintaince());
		}
		
		return querry;				
	}
	
	/**
	 * Presenter initializing for selected approval document
	 */
	
	private void setTableSelectionForContract() {
		
		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Contract",Screen.CONTRACT);
		final ContractForm form = ContractPresenter.initalize();
	
		AppMemory.getAppMemory().stickPnel(form);
		form.showWaitSymbol();
	
		Timer timer=new Timer() 
		{
				@Override
				public void run() {
					
					async.getSearchResult(approvalSelectionQuerry(),new AsyncCallback<ArrayList<SuperModel>>() {
	
						@Override
						public void onFailure(Throwable caught) {
							form.showDialogMessage("An Unexpected Error occured !");
							
						}
						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							
							
							for(SuperModel pmodel:result)
							{
									Contract con=(Contract)pmodel;
									form.updateView(con);
								    form.setToViewState();
							}
						}
					});
					
					form.hideWaitSymbol();
				}
			};
		timer.schedule(3000); 
	}
	
	
	private void setTableSelectionForDeliveryNote() {
		
		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Sales/Delivery Note",Screen.DELIVERYNOTE);
		final DeliveryNoteForm form = DeliveryNotePresenter.initalize();
	
		AppMemory.getAppMemory().stickPnel(form);
		form.showWaitSymbol();
	
		Timer timer=new Timer() 
		{
				@Override
				public void run() {
					
					async.getSearchResult(approvalSelectionQuerry(),new AsyncCallback<ArrayList<SuperModel>>() {
	
						@Override
						public void onFailure(Throwable caught) {
							form.showDialogMessage("An Unexpected Error occured !");
							
						}
						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							
							
							for(SuperModel pmodel:result)
							{
									DeliveryNote con=(DeliveryNote)pmodel;
									form.updateView(con);
								    form.setToViewState();
							}
						}
					});
					
					form.hideWaitSymbol();
				}
			};
		timer.schedule(3000); 
	}
	
	
	
	
	private void setTableSelectionForSalesOrder(){

     	 AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
     	 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Sales/Sales Order",Screen.SALESORDER);
     	 final SalesOrderForm form = SalesOrderPresenter.initalize();
     	
     	 AppMemory.getAppMemory().stickPnel(form);
     	 form.showWaitSymbol();
     	
			 Timer timer=new Timer() 
     	 {
				@Override
				public void run() {
					
					async.getSearchResult(approvalSelectionQuerry(),new AsyncCallback<ArrayList<SuperModel>>() {

						@Override
						public void onFailure(Throwable caught) {
							form.showDialogMessage("An Unexpected Error occured !");
							
						}
						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							
							
							for(SuperModel pmodel:result)
							{
								SalesOrder sales=(SalesOrder)pmodel;
								form.updateView(sales);
								form.setToViewState();
							}
							
						}
					});
					
					form.hideWaitSymbol();
				}
			};
          timer.schedule(3000); 
    }
	
	
	private void setTableSelectionForQuotation(){

   	 AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
   	 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Quotation",Screen.QUOTATION);
   	 final QuotationForm form = QuotationPresenter.initalize();
   	 AppMemory.getAppMemory().stickPnel(form);
   	 form.showWaitSymbol();
   	
			 Timer timer=new Timer() 
   	 {
				@Override
				public void run() {
					
					async.getSearchResult(approvalSelectionQuerry(),new AsyncCallback<ArrayList<SuperModel>>() {

						@Override
						public void onFailure(Throwable caught) {
							form.showDialogMessage("An Unexpected Error occured !");
						}
						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							
							
							for(SuperModel pmodel:result)
							{
								Quotation quotation=(Quotation)pmodel;
								form.updateView(quotation);
								 form.setToViewState();
							}
						}
					});
					
					form.hideWaitSymbol();
				}
			};
        timer.schedule(3000); 
	}
	
	
	private void setTableSelectionForSalesQuotation(){
	
	 	 AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
	 	 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Sales/Quotation",Screen.SALESQUOTATION);
	 	 final SalesQuotationForm form = SalesQuotationPresenter.initalize();
	 	 
	 	 AppMemory.getAppMemory().stickPnel(form);
	 	 form.showWaitSymbol();
	 	
			 Timer timer=new Timer() 
	 	 {
				@Override
				public void run() {
					
					async.getSearchResult(approvalSelectionQuerry(),new AsyncCallback<ArrayList<SuperModel>>() {
	
						@Override
						public void onFailure(Throwable caught) {
							form.showDialogMessage("An Unexpected Error occured !");
							
						}
						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							
							
							for(SuperModel pmodel:result)
							{
								SalesQuotation quotation=(SalesQuotation)pmodel;
								form.updateView(quotation);
								form.setToViewState();
							}
							
						}
					});
					
					form.hideWaitSymbol();
				}
			};
	      timer.schedule(3000); 
	}
	
	private void setTableSelectionForPurchaseOrder(){
	
		 AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
		 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Purchase/Purchase Order",Screen.PURCHASEORDER);
		 final PurchaseOrderForm form = PurchaseOrderPresenter.initalize();
		 AppMemory.getAppMemory().stickPnel(form);
		 form.showWaitSymbol();
		
			 Timer timer=new Timer() 
		 {
				@Override
				public void run() {
					
					async.getSearchResult(approvalSelectionQuerry(),new AsyncCallback<ArrayList<SuperModel>>() {
	
						@Override
						public void onFailure(Throwable caught) {
							form.showDialogMessage("An Unexpected Error occured !");
						}
						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							
							
							for(SuperModel pmodel:result)
							{
								PurchaseOrder purchase=(PurchaseOrder)pmodel;
								form.updateView(purchase);
								form.setToViewState();
								form.getProcessLevelBar().setVisibleFalse(false);
							}
						}
					});
					
					form.hideWaitSymbol();
				}
			};
	     timer.schedule(3000); 
	}
	
	private void setTableSelectionForGRN(){

   	 AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
   	 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Inventory/GRN",Screen.GRN);
   	 final GRNForm form = GRNPresenter.initalize();
//   	 form.setToViewState();
   	 AppMemory.getAppMemory().stickPnel(form);
   	 form.showWaitSymbol();
   	
			 Timer timer=new Timer() 
   	 {
				@Override
				public void run() {
					
					async.getSearchResult(approvalSelectionQuerry(),new AsyncCallback<ArrayList<SuperModel>>() {

						@Override
						public void onFailure(Throwable caught) {
							form.showDialogMessage("An Unexpected Error occured !");
						}
						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							
							
							for(SuperModel pmodel:result)
							{
								GRN grn=(GRN)pmodel;
								form.updateView(grn);
								form.setToViewState();
								form.getProcessLevelBar().setVisibleFalse(false);
							}
						}
					});
					
					form.hideWaitSymbol();
				}
			};
        timer.schedule(3000); 
	}
	
	private void setTableSelectionForPurchaseRequisition(){

  	 AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  	 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Purchase/Purchase Requisition",Screen.PURCHASEREQUISITE);
  	 final PurchaseRequisitionForm form = PurchaseRequitionPresenter.initalize();
  	 AppMemory.getAppMemory().stickPnel(form);
  	 form.showWaitSymbol();
  	
			 Timer timer=new Timer() 
  	 {
				@Override
				public void run() {
					
					async.getSearchResult(approvalSelectionQuerry(),new AsyncCallback<ArrayList<SuperModel>>() {

						@Override
						public void onFailure(Throwable caught) {
							form.showDialogMessage("An Unexpected Error occured !");
						}
						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							
							
							for(SuperModel pmodel:result)
							{
								PurchaseRequisition pr=(PurchaseRequisition)pmodel;
								form.updateView(pr);
								form.setToViewState();
								form.getProcessLevelBar().setVisibleFalse(false);
							}
						}
					});
					
					form.hideWaitSymbol();
				}
			};
       timer.schedule(3000); 
	}
	
	
	private void setTableSelectionForExpenseManagement(){

	    	 AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
	    	 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Accounts/Expense Management",Screen.EXPENSEMANAGMENT);
	    	 final ExpensemanagmentForm form = ExpensemanagmentPresenter.initalize();
	    	 AppMemory.getAppMemory().stickPnel(form);
	    	 form.showWaitSymbol();
	    	
				 Timer timer=new Timer() 
	    	 {
					@Override
					public void run() {
						
						async.getSearchResult(approvalSelectionQuerry(),new AsyncCallback<ArrayList<SuperModel>>() {

							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected Error occured !");
							}
							@Override
							public void onSuccess(ArrayList<SuperModel> result) {
								
								
								for(SuperModel pmodel:result)
								{
									Expense expence=(Expense)pmodel;
									form.updateView(expence);
									 form.setToViewState();
								}
							}
						});
						
						form.hideWaitSymbol();
					}
				};
	         timer.schedule(3000); 
	}
	
	
	private void setTableSelectionForMaterialIssueNote(){

   	 AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
   	 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Material Issue Note",Screen.MATERIALISSUENOTE);
   	 final MaterialIssueNoteForm form = MaterialIssueNotePresenter.initialize();
   	 AppMemory.getAppMemory().stickPnel(form);
   	 form.showWaitSymbol();
   	
			 Timer timer=new Timer() 
   	 {
				@Override
				public void run() {
					
					async.getSearchResult(approvalSelectionQuerry(),new AsyncCallback<ArrayList<SuperModel>>() {

						@Override
						public void onFailure(Throwable caught) {
							form.showDialogMessage("An Unexpected Error occured !");
						}
						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							
							
							for(SuperModel pmodel:result)
							{
								MaterialIssueNote min=(MaterialIssueNote)pmodel;
								form.updateView(min);
								 form.setToViewState();
								 form.getProcessLevelBar().setVisibleFalse(false);
							}
						}
					});
					
					form.hideWaitSymbol();
				}
			};
        timer.schedule(3000); 
}
	
	private void setTableSelectionForMaterialMovementNote(){

   	 AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
   	 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Material Movement Note",Screen.MATERIALMOVEMENTNOTE);
   	 final MaterialMovementNoteForm form = MaterialMovementNotePresenter.initialize();
   	 AppMemory.getAppMemory().stickPnel(form);
   	 form.showWaitSymbol();
   	
			 Timer timer=new Timer() 
   	 {
				@Override
				public void run() {
					
					async.getSearchResult(approvalSelectionQuerry(),new AsyncCallback<ArrayList<SuperModel>>() {

						@Override
						public void onFailure(Throwable caught) {
							form.showDialogMessage("An Unexpected Error occured !");
						}
						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							
							
							for(SuperModel pmodel:result)
							{
								MaterialMovementNote mMnote=(MaterialMovementNote)pmodel;
								form.updateView(mMnote);
								 form.setToViewState();
								 form.getProcessLevelBar().setVisibleFalse(false);
							}
						}
					});
					
					form.hideWaitSymbol();
				}
			};
        timer.schedule(3000); 
	}


	private void setTableSelectionForMaterialRequestNote(){

   	 AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
   	 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Material Request Note",Screen.MATERIALREQUESTNOTE);
   	 final MaterialRequestNoteForm form = MaterialRequestNotePresenter.initialize();
   	 AppMemory.getAppMemory().stickPnel(form);
   	 form.showWaitSymbol();
   	
			 Timer timer=new Timer() 
   	 {
				@Override
				public void run() {
					
					async.getSearchResult(approvalSelectionQuerry(),new AsyncCallback<ArrayList<SuperModel>>() {

						@Override
						public void onFailure(Throwable caught) {
							form.showDialogMessage("An Unexpected Error occured !");
						}
						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							
							
							for(SuperModel pmodel:result)
							{
								MaterialRequestNote mrn=(MaterialRequestNote)pmodel;
								form.updateView(mrn);
								 form.setToViewState();
								 form.getProcessLevelBar().setVisibleFalse(false);
							}
						}
					});
					
					form.hideWaitSymbol();
				}
			};
        timer.schedule(3000); 
	}


	private void setTableSelectionForRequestForQuotation(){

   	 AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
   	 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Purchase/RFQ",Screen.REQUESTFORQUOTATION);
   	 final RequestForQuotationForm form = RequestForQuotationPresenter.initialize();
   	 AppMemory.getAppMemory().stickPnel(form);
   	 form.showWaitSymbol();
   	
			 Timer timer=new Timer() 
   	 {
				@Override
				public void run() {
					
					async.getSearchResult(approvalSelectionQuerry(),new AsyncCallback<ArrayList<SuperModel>>() {

						@Override
						public void onFailure(Throwable caught) {
							form.showDialogMessage("An Unexpected Error occured !");
						}
						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							
							
							for(SuperModel pmodel:result)
							{
								RequsestForQuotation rfq=(RequsestForQuotation)pmodel;
								form.updateView(rfq);
								 form.setToViewState();
								 form.getProcessLevelBar().setVisibleFalse(false);
							}
						}
					});
					
					form.hideWaitSymbol();
				}
			};
        timer.schedule(3000); 
	}
	
	private void setTableSelectionForInvoiceDetails(){

   	 AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
   	 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Accounts/Invoice Details",Screen.INVOICEDETAILS);
   	 final InvoiceDetailsForm form = InvoiceDetailsPresenter.initalize();
   	 AppMemory.getAppMemory().stickPnel(form);
   	 form.showWaitSymbol();
   	
			 Timer timer=new Timer() 
   	 {
				@Override
				public void run() {
					
					async.getSearchResult(approvalSelectionQuerry(),new AsyncCallback<ArrayList<SuperModel>>() {

						@Override
						public void onFailure(Throwable caught) {
							form.showDialogMessage("An Unexpected Error occured !");
						}
						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							
							
							for(SuperModel pmodel:result)
							{
								Invoice inv=(Invoice)pmodel;
								form.updateView(inv);
								 form.setToViewState();
							}
						}
					});
					
					form.hideWaitSymbol();
				}
			};
        timer.schedule(3000); 
	}
	
	
	private void setTableSelectionForBillingDetails(){

   	 AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
   	 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Accounts/Billing Details",Screen.BILLINGDETAILS);
   	 final BillingDetailsForm form = BillingDetailsPresenter.initalize();
   	 AppMemory.getAppMemory().stickPnel(form);
   	 form.showWaitSymbol();
   	
			 Timer timer=new Timer() 
   	 {
				@Override
				public void run() {
					
					async.getSearchResult(approvalSelectionQuerry(),new AsyncCallback<ArrayList<SuperModel>>() {

						@Override
						public void onFailure(Throwable caught) {
							form.showDialogMessage("An Unexpected Error occured !");
						}
						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							
							
							for(SuperModel pmodel:result)
							{
								BillingDocument billingdoc=(BillingDocument)pmodel;
								form.updateView(billingdoc);
								 form.setToViewState();
							}
						}
					});
					
					form.hideWaitSymbol();
				}
			};
        timer.schedule(3000); 
	}


	private void setTableSelectionForLetterOfIntent(){

   	 AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
   	 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Purchase/Letter Of Intent(LOI)",Screen.LETTEROFINTENT);
   	 final LetterOfIntentForm form = LetterOfIntentPresenter.initalize();
   	 AppMemory.getAppMemory().stickPnel(form);
   	 form.showWaitSymbol();
   	
			 Timer timer=new Timer() 
			 {
				@Override
				public void run() {
					
					async.getSearchResult(approvalSelectionQuerry(),new AsyncCallback<ArrayList<SuperModel>>() {

						@Override
						public void onFailure(Throwable caught) {
							form.showDialogMessage("An Unexpected Error occured !");
						}
						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							
							for(SuperModel pmodel:result)
							{
								LetterOfIntent loi=(LetterOfIntent)pmodel;
								form.updateView(loi);
								 form.setToViewState();
								form.setMenuAsPerStatus();
								form.getProcessLevelBar().setVisibleFalse(false);
							}
						}
					});
					
					form.hideWaitSymbol();
				}
			};
        timer.schedule(3000); 
	}


	private void setTableSelectionForLeaveApplication(){
   
   	 AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
   	 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Human Resource/Leave Application",Screen.LEAVEAPPLICATION);
   	 final LeaveApplicationForm form = LeaveApplicationPresenter.initalize();
   	 AppMemory.getAppMemory().stickPnel(form);
   	 form.showWaitSymbol();
   	
			 Timer timer=new Timer() 
   	 {
				@Override
				public void run() {
					
					async.getSearchResult(approvalSelectionQuerry(),new AsyncCallback<ArrayList<SuperModel>>() {

						@Override
						public void onFailure(Throwable caught) {
							form.showDialogMessage("An Unexpected Error occured !");
						}
						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							
							
							for(SuperModel pmodel:result)
							{
								LeaveApplication leaveapp=(LeaveApplication)pmodel;
								form.updateView(leaveapp);
								form.setToViewState();
							}
						}
					});
					
					form.hideWaitSymbol();
				}
			};
        timer.schedule(3000); 
	}


	private void setTableSelectionForComplain(){

   	 AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
   	 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Complain/Complain",Screen.COMPLAIN);
   	 final ComplainForm form = ComplainPresenter.initalize();
   	 AppMemory.getAppMemory().stickPnel(form);
   	 form.showWaitSymbol();
   	
			 Timer timer=new Timer() 
   	 {
				@Override
				public void run() {
					
					async.getSearchResult(approvalSelectionQuerry(),new AsyncCallback<ArrayList<SuperModel>>() {

						@Override
						public void onFailure(Throwable caught) {
							form.showDialogMessage("An Unexpected Error occured !");
						}
						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							
							
							for(SuperModel pmodel:result)
							{
								Activity act=(Activity)pmodel;
//								form.updateView(act);
								form.setToViewState();
							}
						}
					});
					form.hideWaitSymbol();
				}
			};
        timer.schedule(3000); 
	}
	private void setTableSelectionForInspectionRequest(){

	   	 AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
	   	 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Inventory/Inspection ",Screen.INSPECTION);
	   	 final InspectionForm form = InspectionPresenter.initalize();
	   	 AppMemory.getAppMemory().stickPnel(form);
	   	 form.showWaitSymbol();
	   	
		Timer timer=new Timer() 
	   	 {
					@Override
					public void run() {
						
						async.getSearchResult(approvalSelectionQuerry(),new AsyncCallback<ArrayList<SuperModel>>() {

							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected Error occured !");
							}
							@Override
							public void onSuccess(ArrayList<SuperModel> result) {
								
								
								for(SuperModel pmodel:result)
								{
									Inspection loan=(Inspection)pmodel;
									form.updateView(loan);
									form.setToViewState();
									form.setMenuAsPerStatus();
								}
							}
						});
						
						form.hideWaitSymbol();
					}
				};
	        timer.schedule(3000); 
	     }


	private void setTableSelectionForAdvanceRequest(){

   	 AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
   	 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Human Resource/Advance Request ",Screen.ADVANCE);
   	 final AdvanceForm form = AdvancePresenter.initalize();
   	 AppMemory.getAppMemory().stickPnel(form);
   	 form.showWaitSymbol();
   	
			 Timer timer=new Timer() 
   	 {
				@Override
				public void run() {
					
					async.getSearchResult(approvalSelectionQuerry(),new AsyncCallback<ArrayList<SuperModel>>() {

						@Override
						public void onFailure(Throwable caught) {
							form.showDialogMessage("An Unexpected Error occured !");
						}
						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							
							
							for(SuperModel pmodel:result)
							{
								Loan loan=(Loan)pmodel;
								form.updateView(loan);
								form.setToViewState();
							}
						}
					});
					
					form.hideWaitSymbol();
				}
			};
        timer.schedule(3000); 
     }
	
	private void setTableSelectionForWorkOrderRequest() {
		 AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
	   	 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Sales/Work Order ",Screen.WORKORDER);
	   	 final WorkOrderForm form = WorkOrderPresenter.initialize();
	   	 AppMemory.getAppMemory().stickPnel(form);
	   	 form.showWaitSymbol();
	   	
		Timer timer=new Timer() 
	   	 {
					@Override
					public void run() {
						
						async.getSearchResult(approvalSelectionQuerry(),new AsyncCallback<ArrayList<SuperModel>>() {

							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected Error occured !");
							}
							@Override
							public void onSuccess(ArrayList<SuperModel> result) {
								
								
								for(SuperModel pmodel:result)
								{
									WorkOrder wo=(WorkOrder)pmodel;
									form.updateView(wo);
									form.setToViewState();
									form.setMenuAsPerStatus();
								}
							}
						});
						
						form.hideWaitSymbol();
					}
				};
	        timer.schedule(3000); 
	}
	
	
	
	
	private void setTableSelectionForMultipleExpenseRequest() {
		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
	   	AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Accounts/Expense Management",Screen.MULTIPLEEXPENSEMANAGMENT);
	   	final MultipleExpensemanagmentForm form = MultipleExpensemanagmentPresenter.initalize();
	   	AppMemory.getAppMemory().stickPnel(form);
	   	form.showWaitSymbol();
		Timer timer=new Timer() 
	   	{
			@Override
			public void run() {
				async.getSearchResult(approvalSelectionQuerry(),new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onFailure(Throwable caught) {
						form.showDialogMessage("An Unexpected Error occured !");
					}
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						
						
						for(SuperModel pmodel:result)
						{
							MultipleExpenseMngt exp=(MultipleExpenseMngt)pmodel;
							form.updateView(exp);
							form.setToViewState();
						}
					}
				});
				form.hideWaitSymbol();
			}
		};
        timer.schedule(3000); 
	}
	
	private void setTableSelectionForServicePoRequest() {
		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
	   	AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Purchase/Service PO",Screen.SERVICEPO);
	   	final ServicePoForm form = ServicePoPresenter.initialize();
	   	AppMemory.getAppMemory().stickPnel(form);
	   	form.showWaitSymbol();
		Timer timer=new Timer() 
	   	{
			@Override
			public void run() {
				async.getSearchResult(approvalSelectionQuerry(),new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onFailure(Throwable caught) {
						form.showDialogMessage("An Unexpected Error occured !");
					}
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						
						
						for(SuperModel pmodel:result)
						{
							ServicePo po=(ServicePo)pmodel;
							form.updateView(po);
							form.setToViewState();
						}
					}
				});
				form.hideWaitSymbol();
			}
		};
        timer.schedule(3000); 
	}
	
	
	private void setTableSelectionForCustomerRequest() {
		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
	   	AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Customer",Screen.CUSTOMER);
	   	final CustomerForm form = CustomerPresenter.initalize();
	   	AppMemory.getAppMemory().stickPnel(form);
	   	form.showWaitSymbol();
		Timer timer=new Timer() 
	   	{
			@Override
			public void run() {
				async.getSearchResult(approvalSelectionQuerry(),new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onFailure(Throwable caught) {
						form.showDialogMessage("An Unexpected Error occured !");
					}
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						
						
						for(SuperModel pmodel:result)
						{
							Customer po=(Customer)pmodel;
							form.updateView(po);
							form.setToViewState();
						}
					}
				});
				form.hideWaitSymbol();
			}
		};
        timer.schedule(3000); 
	}
	
	public void reactOnApprovalSuccessEmail(Approvals approvalEntity)
	{
		//Quotation Entity patch patch patch
		emailService.intiateOnApproveRequestEmail(approvalEntity, new AsyncCallback<Void>(){

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Unable To Send Email");
				caught.printStackTrace();
			}

			@Override
			public void onSuccess(Void result) {
//				Window.alert("Email Sent Sucessfully !");
			}
		});
	}
	
	protected void printingApprovalDocument()
	{
		if(docType.equals(AppConstants.APPROVALCONTRACT)){
			
			Timer timer=new Timer() 
			{
					@Override
					public void run() {
						
						async.getSearchResult(approvalSelectionQuerry(),new AsyncCallback<ArrayList<SuperModel>>() {
		
							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected Error occured !");
								
							}
							@Override
							public void onSuccess(ArrayList<SuperModel> result) {
								
								
								for(SuperModel pmodel:result)
								{
										con=(Contract)pmodel;
//										final String url = GWT.getModuleBaseURL() + "pdfservice"+"?Id="+con.getId()+"&"+"type="+"c";
//										Window.open(url, "test", "enabled");
										
										
										//*************rohan changes ********************
										
										MyQuerry querry = new MyQuerry();
									  	Company c = new Company();
									  	Vector<Filter> filtervec=new Vector<Filter>();
									  	Filter filter = null;
									  	filter = new Filter();
									  	filter.setQuerryString("companyId");
										filter.setLongValue(c.getCompanyId());
										filtervec.add(filter);
										filter = new Filter();
										filter.setQuerryString("processName");
										filter.setStringValue("Contract");
										filtervec.add(filter);
										
										filter = new Filter();
										filter.setQuerryString("processList.status");
										filter.setBooleanvalue(true);
										filtervec.add(filter);
										
										querry.setFilters(filtervec);
										querry.setQuerryObject(new ProcessConfiguration());
										
										async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
											
											@Override
											public void onFailure(Throwable caught) {
											}			

											@Override
											public void onSuccess(ArrayList<SuperModel> result) {
												System.out.println(" result set size +++++++"+result.size());
												
												List<ProcessTypeDetails> processList =new ArrayList<ProcessTypeDetails>();
												if(result.size()==0){
													
													final String url = GWT.getModuleBaseURL() + "pdfservice"+"?Id="+con.getId()+"&"+"type="+"c"+"&"+"preprint="+"plane";
													Window.open(url, "test", "enabled");
												}
												else{
												
													for(SuperModel model:result)
													{
														ProcessConfiguration processConfig=(ProcessConfiguration)model;
														processList.addAll(processConfig.getProcessList());
														
													}
												
													 vcare =0;cnt =0;pestoIndia=0;hygeia=0;genesis=0;universal=0;
													
												for(int k=0;k<processList.size();k++){	
												if(processList.get(k).getProcessType().trim().equalsIgnoreCase("CompanyAsLetterHead")&&processList.get(k).isStatus()==true){
													
													cnt=cnt+1;
												
												}
												if (processList	.get(k).getProcessType().trim().equalsIgnoreCase("PestoIndiaQuotations")&& processList.get(k).isStatus() == true) {

													pestoIndia = pestoIndia + 1;

												}

												else if (processList.get(k).getProcessType().trim().equalsIgnoreCase("VCareCustomization")&& processList.get(k).isStatus() == true) {

													vcare = vcare + 1;
													break;
												}

												else if (processList.get(k).getProcessType().trim().equalsIgnoreCase("HygeiaPestManagement")&& processList.get(k).isStatus() == true) {

													hygeia = hygeia + 1;
													break;
												}

												else if (processList.get(k).getProcessType().trim().equalsIgnoreCase("AMCForGenesis")&& processList.get(k).isStatus() == true) {

													genesis = genesis + 1;
													break;
												}
												else if (processList.get(k).getProcessType().trim().equalsIgnoreCase("UniversalPestCustomization")&& processList.get(k).isStatus() == true) {
													universal = universal + 1;
													break;
												}

												else if (processList.get(k).getProcessType().trim().equalsIgnoreCase("PCAMBCustomization")&& processList.get(k).isStatus() == true) {
													final String url = GWT.getModuleBaseURL()+ "PcambContractpdf" + "?Id="+ con.getId();
													Window.open(url, "test", "enabled");
													return;
												}
											}
												
												
												if(cnt>0){
													System.out.println("in side react on prinnt");
													conditionpanel=new PopupPanel(true);
													conditionpanel.add(conditionPopup);
													conditionpanel.setGlassEnabled(true);
													conditionpanel.show();
													conditionpanel.center();
												}else if (pestoIndia > 0) {
													System.out.println("in side react on prinnt");
													conditionpanel = new PopupPanel(true);
													conditionpanel.add(conditionPopup);
													conditionpanel.setGlassEnabled(true);
													conditionpanel.show();
													conditionpanel.center();
														   
												} else if (universal > 0) {
													System.out.println("in side react on prinnt Universal pest Quotation");
													conditionpanel = new PopupPanel(true);
													conditionpanel.add(conditionPopup);
													conditionpanel.setGlassEnabled(true);
													conditionpanel.show();
													conditionpanel.center();
												}

												else if (vcare > 0) {
													System.out.println("in side react on prinnt");
													conditionpanel = new PopupPanel(true);
													conditionpanel.add(conditionPopup);
													conditionpanel.setGlassEnabled(true);
													conditionpanel.show();
													conditionpanel.center();

												} else if (hygeia > 0) {
													final String url = GWT.getModuleBaseURL()+ "pdfcon" + "?Id=" + con.getId();
													Window.open(url, "test", "enabled");

												} else if (genesis > 0) {

													final String url = GWT.getModuleBaseURL()+ "pdfamc" + "?Id=" + con.getId();
													Window.open(url, "test", "enabled");

												}
												else
												{
													final String url = GWT.getModuleBaseURL() + "pdfservice"+"?Id="+con.getId()+"&"+"type="+"c"+"&"+"preprint="+"plane";
													 Window.open(url, "test", "enabled");
													
												}
											}
											}
										});
										//**************************changes ends here ********************************
								}
							}
						});
						
//						form.hideWaitSymbol();
					}
				};
			timer.schedule(3000); 
			
		}
		if(docType.equals(AppConstants.APPROVALDELIVERYNOTE)){
			
			Timer timer=new Timer() 
			{
					@Override
					public void run() {
						
						async.getSearchResult(approvalSelectionQuerry(),new AsyncCallback<ArrayList<SuperModel>>() {
		
							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected Error occured !");
								
							}
							@Override
							public void onSuccess(ArrayList<SuperModel> result) {
								
								
								for(SuperModel pmodel:result)
								{
										DeliveryNote con=(DeliveryNote)pmodel;
										final String url = GWT.getModuleBaseURL() + "deliverynotepdf"+"?Id="+con.getId();
										Window.open(url, "test", "enabled");
								}
							}
						});
						
						form.hideWaitSymbol();
					}
				};
			timer.schedule(3000); 
			
		}
		if(docType.equals(AppConstants.APPROVALSALESORDER)){
			
			 Timer timer=new Timer() 
	     	 {
					@Override
					public void run() {
						
						async.getSearchResult(approvalSelectionQuerry(),new AsyncCallback<ArrayList<SuperModel>>() {

							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected Error occured !");
								
							}
							@Override
							public void onSuccess(ArrayList<SuperModel> result) {
								
								
								for(SuperModel pmodel:result)
								{
									SalesOrder sales=(SalesOrder)pmodel;
									final String url = GWT.getModuleBaseURL() + "pdfsales"+"?Id="+sales.getId()+"&"+"type="+"so"+"&"+"preprint="+"plane";
									Window.open(url, "test", "enabled");
								}
								
							}
						});
						
						form.hideWaitSymbol();
					}
				};
	          timer.schedule(3000); 
			
			
			
		}
		if(docType.equals(AppConstants.APPROVALQUOTATION)){
			
			 Timer timer=new Timer() 
		   	 {
						@Override
						public void run() {
							
							async.getSearchResult(approvalSelectionQuerry(),new AsyncCallback<ArrayList<SuperModel>>() {

								@Override
								public void onFailure(Throwable caught) {
									form.showDialogMessage("An Unexpected Error occured !");
								}
								@Override
								public void onSuccess(ArrayList<SuperModel> result) {
									
									
									for(SuperModel pmodel:result)
									{
										 quotation=(Quotation)pmodel;
										
//										final String url = GWT.getModuleBaseURL() + "pdfservice"+"?Id="+quotation.getId()+"&"+"type="+"q";
//										 Window.open(url, "test", "enabled");
										 
										 
											//*************rohan changes ********************
											
											MyQuerry querry = new MyQuerry();
										  	Company c = new Company();
										  	Vector<Filter> filtervec=new Vector<Filter>();
										  	Filter filter = null;
										  	filter = new Filter();
										  	filter.setQuerryString("companyId");
											filter.setLongValue(c.getCompanyId());
											filtervec.add(filter);
											filter = new Filter();
											filter.setQuerryString("processName");
											filter.setStringValue("Quotation");
											filtervec.add(filter);
											
											filter = new Filter();
											filter.setQuerryString("processList.status");
											filter.setBooleanvalue(true);
											filtervec.add(filter);
											
											querry.setFilters(filtervec);
											querry.setQuerryObject(new ProcessConfiguration());
											
											async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
												
												@Override
												public void onFailure(Throwable caught) {
												}			

												@Override
												public void onSuccess(ArrayList<SuperModel> result) {
													System.out.println(" result set size +++++++"+result.size());
													
													List<ProcessTypeDetails> processList =new ArrayList<ProcessTypeDetails>();
													
													if(result.size()==0){
														
														final String url = GWT.getModuleBaseURL() + "pdfservice"+"?Id="+quotation.getId()+"&"+"type="+"q"+"&"+"preprint="+"plane";
														Window.open(url, "test", "enabled");
													}
													else{
													
														for(SuperModel model:result)
														{
															ProcessConfiguration processConfig=(ProcessConfiguration)model;
															processList.addAll(processConfig.getProcessList());
															
														}
													
														
														 cnt=0;customQuotation=0;pestQuotation=0;nbhc=0;universal=0;
														 
													for(int k=0;k<processList.size();k++){	
													if(processList.get(k).getProcessType().trim().equalsIgnoreCase("CompanyAsLetterHead")&&processList.get(k).isStatus()==true){
														
														cnt=cnt+1;
													
													}
													if(processList.get(k).getProcessType().trim().equalsIgnoreCase("PrintCustomQuotation")&&processList.get(k).isStatus()==true){
														
														customQuotation=customQuotation+1;
													
													}
													
													//******************pestal quotations code ******************
													
													if(processList.get(k).getProcessType().trim().equalsIgnoreCase("PestleQuotations")&&processList.get(k).isStatus()==true){
														System.out.println("in side pestle ");
														pestQuotation=pestQuotation+1;
													
													}
													
													
													if(processList.get(k).getProcessType().trim().equalsIgnoreCase("PestoIndiaQuotations")&&processList.get(k).isStatus()==true){
														
														pestoIndia=pestoIndia+1;
													
													}
													
													if(processList.get(k).getProcessType().trim().equalsIgnoreCase("NBHCQuotation")&&processList.get(k).isStatus()==true){
														nbhc=nbhc+1;
													}
													
													if(processList.get(k).getProcessType().trim().equalsIgnoreCase("UniversalPestCustomization")&&processList.get(k).isStatus()==true){
														universal=universal+1;
													}
													}
													
													
													if(pestQuotation > 0)
													{
														if(quotation.getGroup().equalsIgnoreCase("Five Years Quotation")|| quotation.getGroup().equalsIgnoreCase("Normal Quotation"))
														{
														System.out.println("in side react on prinnt pestQuotation");
														conditionpanel=new PopupPanel(true);
														conditionpanel.add(conditionPopup);
														conditionpanel.setGlassEnabled(true);
														conditionpanel.show();
														conditionpanel.center();
														}
														else
														{
																final String url = GWT.getModuleBaseURL() + "pdfservice"+"?Id="+model.getId()+"&"+"type="+"q"+"&"+"preprint="+"plane";
																Window.open(url, "test", "enabled");
																panel.hide();
														}
													
													}
													else if(nbhc > 0)
													{
														System.out.println("in side react on prinnt NBHC Quotation");
														conditionpanel=new PopupPanel(true);
														conditionpanel.add(conditionPopup);
														conditionpanel.setGlassEnabled(true);
														conditionpanel.show();
														conditionpanel.center();
													}
													else if(universal > 0)
													{
														System.out.println("in side react on prinnt Universal pest Quotation");
														conditionpanel=new PopupPanel(true);
														conditionpanel.add(conditionPopup);
														conditionpanel.setGlassEnabled(true);
														conditionpanel.show();
														conditionpanel.center();
													}
													else if(pestoIndia > 0)
													{
														System.out.println("in side react on prinnt pestQuotation");
														conditionpanel=new PopupPanel(true);
														conditionpanel.add(conditionPopup);
														conditionpanel.setGlassEnabled(true);
														conditionpanel.show();
														conditionpanel.center();
														
													}
													
													else if(customQuotation > 0)
													{
														
														if(quotation.getGroup().equalsIgnoreCase("Five Years")|| quotation.getGroup().equalsIgnoreCase("Direct Quotation")
																|| quotation.getGroup().equalsIgnoreCase("Building")|| quotation.getGroup().equalsIgnoreCase("After Inspection"))
														{
														
														System.out.println("in side react on prinnt customQuotation");
														conditionpanel=new PopupPanel(true);
														conditionpanel.add(conditionPopup);
														conditionpanel.setGlassEnabled(true);
														conditionpanel.show();
														conditionpanel.center();
														}
														else
														{
																final String url = GWT.getModuleBaseURL() + "pdfservice"+"?Id="+model.getId()+"&"+"type="+"q"+"&"+"preprint="+"plane";
																Window.open(url, "test", "enabled");
														}
													
													}
													
													else if(cnt>0){
														conditionpanel=new PopupPanel(true);
														conditionpanel.add(conditionPopup);
														conditionpanel.setGlassEnabled(true);
														conditionpanel.show();
														conditionpanel.center();
													}
													else
													{
															final String url = GWT.getModuleBaseURL() + "pdfservice"+"?Id="+quotation.getId()+"&"+"type="+"q"+"&"+"preprint="+"plane";
															Window.open(url, "test", "enabled");
													}
												}
												}
											});
											//**************************changes ends here ********************************
										
										 
										 
									}
								}
							});
							
							form.hideWaitSymbol();
						}
					};
		        timer.schedule(3000); 
			
			
			
		}
		if(docType.equals(AppConstants.APPROVALSALESQUOTATION)){
			
			 Timer timer=new Timer() 
		 	 {
					@Override
					public void run() {
						
						async.getSearchResult(approvalSelectionQuerry(),new AsyncCallback<ArrayList<SuperModel>>() {
		
							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected Error occured !");
								
							}
							@Override
							public void onSuccess(ArrayList<SuperModel> result) {
								
								
								for(SuperModel pmodel:result)
								{
									SalesQuotation salesquotation=(SalesQuotation)pmodel;
									
									final String url = GWT.getModuleBaseURL() + "pdfsales"+"?Id="+salesquotation.getId()+"&"+"type="+"sq"+"&"+"preprint="+"plane";
									 Window.open(url, "test", "enabled");
								}
								
							}
						});
						
						form.hideWaitSymbol();
					}
				};
		      timer.schedule(3000); 
			
		}
		if(docType.equals(AppConstants.APPROVALPURCHASEORDER)){
			
			
			 Timer timer=new Timer() 
			 {
					@Override
					public void run() {
						
						async.getSearchResult(approvalSelectionQuerry(),new AsyncCallback<ArrayList<SuperModel>>() {
		
							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected Error occured !");
							}
							@Override
							public void onSuccess(ArrayList<SuperModel> result) {
								
								
								for(SuperModel pmodel:result)
								{
									PurchaseOrder purchase=(PurchaseOrder)pmodel;
									final String url = GWT.getModuleBaseURL() + "pdfpurchase"+"?Id="+purchase.getId()+"&"+"preprint="+"plane";
									 Window.open(url, "test", "enabled");
								}
							}
						});
						
						form.hideWaitSymbol();
					}
				};
		     timer.schedule(3000); 
		}
		if(docType.equals(AppConstants.APPROVALGRN)){

			form.showDialogMessage("Pdf for this document is not available");
		}
		if(docType.equals(AppConstants.APPROVALPURCHASEREQUISITION)){
			
			form.showDialogMessage("Pdf for this document is not available");
		}
		if(docType.equals(AppConstants.APPROVALEXPENSEMANAGMENT)){
			
			form.showDialogMessage("Pdf for this document is not available");
		}
		if(docType.equals(AppConstants.APPROVALMATERIALISSUENOTE)){
			
			form.showDialogMessage("Pdf for this document is not available");
		}
		if(docType.equals(AppConstants.APPROVALMATERIALMOVEMENTNOTE)){
			
			form.showDialogMessage("Pdf for this document is not available");
		}
		if(docType.equals(AppConstants.APPROVALMATERIALREQUESTNOTE)){
			
			form.showDialogMessage("Pdf for this document is not available");
		}
		if(docType.equals(AppConstants.APPROVALREQUESTFORQUOTATION)){
			
			form.showDialogMessage("Pdf for this document is not available");
		}
		if(docType.equals(AppConstants.APPROVALINVOICEDETAILS)){
			
			 Timer timer=new Timer() 
			 {
					@Override
					public void run() {
						
						async.getSearchResult(approvalSelectionQuerry(),new AsyncCallback<ArrayList<SuperModel>>() {
		
							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected Error occured !");
							}
							@Override
							public void onSuccess(ArrayList<SuperModel> result) {
								
								
								for(SuperModel pmodel:result)
								{
									 invoiceEntity=(Invoice)pmodel;
									
									if(invoiceEntity.getTypeOfOrder().equals("Sales Order"))
									{
										final String url = GWT.getModuleBaseURL() + "salesinvoiceprint"+"?Id="+invoiceEntity.getId();
										Window.open(url, "test", "enabled");
									}
									
									if(invoiceEntity.getTypeOfOrder().equals("Purchase Order"))
									{
										final String url = GWT.getModuleBaseURL() + "salesinvoiceprint"+"?Id="+invoiceEntity.getId();
										Window.open(url, "test", "enabled");
									}
									
								
//									if(invoiceEntity.getTypeOfOrder().equals("Service Order"))
//									{
//									final String url = GWT.getModuleBaseURL() + "serviceinvoiceprint"+"?Id="+invoiceEntity.getId();
//									Window.open(url, "test", "enabled");
//									}
										
										//*************rohan changes ********************
										
										MyQuerry querry = new MyQuerry();
									  	Company c = new Company();
									  	Vector<Filter> filtervec=new Vector<Filter>();
									  	Filter filter = null;
									  	filter = new Filter();
									  	filter.setQuerryString("companyId");
										filter.setLongValue(c.getCompanyId());
										filtervec.add(filter);
										filter = new Filter();
										filter.setQuerryString("processName");
										filter.setStringValue("Invoice");
										filtervec.add(filter);
										
										filter = new Filter();
										filter.setQuerryString("processList.status");
										filter.setBooleanvalue(true);
										filtervec.add(filter);
										
										querry.setFilters(filtervec);
										querry.setQuerryObject(new ProcessConfiguration());
										
										async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
											
											@Override
											public void onFailure(Throwable caught) {
											}			

											@Override
											public void onSuccess(ArrayList<SuperModel> result) {
												System.out.println(" result set size +++++++"+result.size());
												
												List<ProcessTypeDetails> processList =new ArrayList<ProcessTypeDetails>();
											
												if(result.size()==0){
													
													final String url = GWT.getModuleBaseURL() + "serviceinvoiceprint"+"?Id="+invoiceEntity.getId()+"&"+"preprint="+"plane"+"&"+"type="+"SingleBilling";
													Window.open(url, "test", "enabled");
													
													
													
												}
												else{
												
													for(SuperModel model:result)
													{
														ProcessConfiguration processConfig=(ProcessConfiguration)model;
														processList.addAll(processConfig.getProcessList());
														
													}
												int cnt=0,pestoIndia=0,vCare=0,omPestInvoice=0,hygeia=0,reddysPestControl=0;
												for(int k=0;k<processList.size();k++){	
												if(processList.get(k).getProcessType().trim().equalsIgnoreCase("CompanyAsLetterHead")&&processList.get(k).isStatus()==true){
													
													cnt=cnt+1;
												
												}
												
												if(processList.get(k).getProcessType().trim().equalsIgnoreCase("PestoIndiaQuotations")&&processList.get(k).isStatus()==true){
													
													pestoIndia=pestoIndia+1;
												
												}
												if(processList.get(k).getProcessType().trim().equalsIgnoreCase("VCareCustomization")&&processList.get(k).isStatus()==true){
													
													vCare=vCare+1;
												
												}
												
												if(processList.get(k).getProcessType().trim().equalsIgnoreCase("InvoiceAndRefDocCustmization")&&processList.get(k).isStatus()==true){
													
													omPestInvoice=omPestInvoice+1;
												
												}
												
												
												if (processList.get(k).getProcessType().trim().equalsIgnoreCase("HygeiaPestManagement")&& processList.get(k).isStatus() == true) {

													hygeia = hygeia + 1;

												}
												
												if (processList.get(k).getProcessType().trim().equalsIgnoreCase("PCAMBCustomization")&& processList.get(k).isStatus() == true) {

													final String url = GWT.getModuleBaseURL() + "pdfpcambinvoice"+"?Id="+invoiceEntity.getId();
													Window.open(url, "test", "enabled");
													return;

												}
												
												
												System.out.println("ROHAN   =====  "+processList.get(k).getProcessType().trim());
												if(processList.get(k).getProcessType().trim().equalsIgnoreCase("ReddysPestControlQuotations")&&processList.get(k).isStatus()==true){
												
													reddysPestControl=reddysPestControl+1;
													
												}
												}
												
												
												if(cnt>0){
													System.out.println("in side react on prinnt");
													conditionpanel=new PopupPanel(true);
													conditionpanel.add(conditionPopup);
													conditionpanel.setGlassEnabled(true);
													conditionpanel.show();
													conditionpanel.center();
												}
												
												else if(pestoIndia > 0)
												{
													
													System.out.println("in side react on prinnt");
													conditionpanel=new PopupPanel(true);
													conditionpanel.add(conditionPopup);
													conditionpanel.setGlassEnabled(true);
													conditionpanel.show();
													conditionpanel.center();
													
												}
												else if(vCare > 0)
												{
													
													System.out.println("in side react on prinnt");
													conditionpanel=new PopupPanel(true);
													conditionpanel.add(conditionPopup);
													conditionpanel.setGlassEnabled(true);
													conditionpanel.show();
													conditionpanel.center();
													
												}
												else if(omPestInvoice > 0)
												{
													
													System.out.println("in side react on prinnt");
													conditionpanel=new PopupPanel(true);
													conditionpanel.add(conditionPopup);
													conditionpanel.setGlassEnabled(true);
													conditionpanel.show();
													conditionpanel.center();
													
												}
												else if (hygeia > 0) {
													
													final String url = GWT.getModuleBaseURL()+ "pdfinvoice" + "?Id="+ model.getId();
													Window.open(url, "test", "enabled");

												} 
												else if(reddysPestControl > 0){
													
													final String url = GWT.getModuleBaseURL() + "pdfrinvoice"+"?Id="+model.getId();
													Window.open(url, "test", "enabled");
												}
												else
												{
													final String url = GWT.getModuleBaseURL() + "serviceinvoiceprint"+"?Id="+invoiceEntity.getId()+"&"+"preprint="+"plane"+"&"+"type="+"SingleBilling";
													Window.open(url, "test", "enabled");
													
												}
											}
											}
										});
										//**************************changes ends here ********************************
								}
							}
						});
						
						form.hideWaitSymbol();
					}
				};
		     timer.schedule(3000); 
		}
		
		if(docType.equals(AppConstants.APPROVALINSPECTION)){
			
			Timer timer=new Timer() 
			{
					@Override
					public void run() {
						
						async.getSearchResult(approvalSelectionQuerry(),new AsyncCallback<ArrayList<SuperModel>>() {
		
							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected Error occured !");
								
							}
							@Override
							public void onSuccess(ArrayList<SuperModel> result) {
								
								
								for(SuperModel pmodel:result)
								{
										Inspection inspectionEntity=(Inspection)pmodel;
										final String url = GWT.getModuleBaseURL() + "pdfinspection"+"?Id="+inspectionEntity.getId();
										Window.open(url, "test", "enabled");
								}
							}
						});
						
						form.hideWaitSymbol();
					}
				};
			timer.schedule(3000); 
		}
		
		
		
		
		
		if(docType.equals(AppConstants.APPROVALBILLINGDETAILS)){
			
			form.showDialogMessage("Pdf for this document is not available");
		}
		if(docType.equals(AppConstants.APPROVALLETTEROFINTENT)){
			
			form.showDialogMessage("Pdf for this document is not available");
		}
		if(docType.equals(AppConstants.APPROVALLEAVEAPPLICATION)){
			
			form.showDialogMessage("Pdf for this document is not available");
		}
		if(docType.equals(AppConstants.APPROVALCOMPLAIN)){
			
			form.showDialogMessage("Pdf for this document is not available");
		}
		if(docType.equals(AppConstants.APPROVALADVANCE)){
			
			form.showDialogMessage("Pdf for this document is not available");
		}
		
		if(docType.equals(AppConstants.APPROVALSERVICEPO)){
				
				form.showWaitSymbol();
				async.getSearchResult(approvalSelectionQuerry(), new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						form.hideWaitSymbol();
						ServicePo po=(ServicePo) result.get(0);
						Long id=po.getId();
						final String url = GWT.getModuleBaseURL()+ "servicePoPdf" + "?Id="+ id;
						Window.open(url, "test", "enabled");
					}
					
					@Override
					public void onFailure(Throwable caught) {
						form.hideWaitSymbol();
					}
				});
				
				
			}
		
		if(docType.equals(AppConstants.APPROVALMULTIPLEEXPENSEMANAGMENT)){
			
			form.showWaitSymbol();
			async.getSearchResult(approvalSelectionQuerry(), new AsyncCallback<ArrayList<SuperModel>>() {
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					form.hideWaitSymbol();
					MultipleExpenseMngt po=(MultipleExpenseMngt) result.get(0);
					Long id=po.getId();
					
					final String url1 = GWT.getModuleBaseURL() + "pdfexpense"+"?Id="+id+ "&" + "type=" + "multiple"+"&"+"companyId="+model.getCompanyId();
					Window.open(url1, "test", "enabled");
					
				}
				
				@Override
				public void onFailure(Throwable caught) {
					form.hideWaitSymbol();
				}
			});
			
			
		}
		
		/**
		 * Date : 12-09-2018 By ANIL
		 */
		if(docType.equals(AppConstants.APPROVALEMPLOYEE)){
			
			form.showWaitSymbol();
			async.getSearchResult(approvalSelectionQuerry(), new AsyncCallback<ArrayList<SuperModel>>() {
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					form.hideWaitSymbol();
					Employee po=(Employee) result.get(0);
					Long id=po.getId();
					
					final String url = GWT.getModuleBaseURL() + "employee" + "?Id="+po.getId()+"&"+"type="+"EmployeeForm"+"&"+"subtype="+"EmployeeFormPortrate";
					Window.open(url, "test", "enable");
					
				}
				
				@Override
				public void onFailure(Throwable caught) {
					form.hideWaitSymbol();
				}
			});
			
			
		}
		
		
	}
	
	public void checkSmsConfigVal(final Approvals approvalEntity)
	{
//		Vector<Filter> filtervec = new Vector<Filter>();
//		Filter filtr = null;
//		
//		filtr = new Filter();
//		filtr.setLongValue(model.getCompanyId());
//		filtr.setQuerryString("companyId");
//		filtervec.add(filtr);
//		
//		filtr = new Filter();
//		filtr.setBooleanvalue(true);
//		filtr.setQuerryString("status");
//		filtervec.add(filtr);
//		
//		MyQuerry querry = new MyQuerry();
//		querry.setFilters(filtervec);
//		querry.setQuerryObject(new SmsConfiguration());
//
//		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
//			
//			@Override
//			public void onSuccess(ArrayList<SuperModel> result) {
//
//				if(result.size()!=0){
//					for(SuperModel smodel:result){
//						SmsConfiguration smsconfig = (SmsConfiguration) smodel;
//						boolean smsStatus = smsconfig.getStatus();
//						String accountSid = smsconfig.getAccountSID();
//						String authoToken = smsconfig.getAuthToken();
//						String fromNumber = smsconfig.getPassword();
//						
//						if(smsStatus==true){
							sendSmsForDeliveryNote(approvalEntity.getBusinessprocessId(),approvalEntity.getCompanyId());
//						}
//						else{
//						}
//					}
//				}
//				else{
//				}
//			}
//			@Override
//			public void onFailure(Throwable caught) {
//
//			}
//		});
		
	}
	
	
	private void sendSmsForDeliveryNote(final int businessprocessId,
			final Long companyId) {
	
		companyName=Slick_Erp.businessUnitName;
		System.out.println("123456789---------------");

		
			System.out.println("123456789------&&&&&&&&&&&&&&&&&&&&&&&&&&&-");

			Vector<Filter> filtervec = new Vector<Filter>();
			Filter filtr = null;
			
			filtr = new Filter();
			filtr.setLongValue(model.getCompanyId());
			filtr.setQuerryString("companyId");
			filtervec.add(filtr);
			
			filtr = new Filter();
			filtr.setStringValue("Delivery Note Approved");	
			filtr.setQuerryString("event");
			filtervec.add(filtr);
			
			filtr = new Filter();
			filtr.setBooleanvalue(true);
			filtr.setQuerryString("status");
			filtervec.add(filtr);
			
			MyQuerry querry = new MyQuerry();
			querry.setFilters(filtervec);
			querry.setQuerryObject(new SmsTemplate());
			
			async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					
					if(result.size()!=0){
						for(SuperModel smodel:result){
							
							final SmsTemplate sms = (SmsTemplate) smodel;
							
							templateMsgWithBraces = sms.getMessage();
							
							System.out.println("Template Msg"+templateMsgWithBraces);
							
							Vector<Filter> filtrvec = new Vector<Filter>();
							Filter filtr = null;
							
							filtr = new Filter();
							filtr.setLongValue(companyId);
							filtr.setQuerryString("companyId");
							filtrvec.add(filtr);
							
							filtr = new Filter();
							filtr.setIntValue(businessprocessId);
							filtr.setQuerryString("count");
							filtrvec.add(filtr);
							
							MyQuerry querry = new MyQuerry();
							querry.setFilters(filtrvec);
							querry.setQuerryObject(new DeliveryNote());
							
							async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
								
								@Override
								public void onSuccess(ArrayList<SuperModel> result) {
									if(result.size()!=0){
										for(SuperModel model:result){
											DeliveryNote deliverynote = (DeliveryNote) model;
											
											final String custName = deliverynote.getCinfo().getFullName();
											String orderNo = deliverynote.getSalesOrderCount()+"";
											Date dilivrydate = deliverynote.getDeliveryDate();
											String deliverydate = AppUtility.parseDate(dilivrydate);
											String lrno = deliverynote.getLrNumber();
											String trackingno = deliverynote.getTrackingNumber();
											String deliveryperson = deliverynote.getDriverName();
											String deliveryprsoncell = deliverynote.getDriverPhone()+"";
											
											cellNo = deliverynote.getCinfo().getCellNumber();
											String customerName = templateMsgWithBraces.replace("{CustomerName}", custName);
											String orderId = customerName.replace("{OrderID}", orderNo);
											String deliveryDate = orderId.replace("{DeliveryDate}",deliverydate );
											String lrNo = deliveryDate.replace("{LRNo}", lrno);
											String trackingNo = lrNo.replace("{TrackingNo}",trackingno );
											String deliveryPerson = trackingNo.replace("{DeliveryPerson}", deliveryperson);
											actualMsg = deliveryPerson.replace("{CellNo}", deliveryprsoncell);
											

//											smsserviceAsync.sendSmsToClient(actualMsg+"\n"+companyName, cellNo, accountSid, authoToken, fromNumber,companyId, new AsyncCallback<Integer>(){
//
//												@Override
//												public void onFailure(Throwable caught) {
//												}
//
//												@Override
//												public void onSuccess(Integer result) {
//													form.hideWaitSymbol();
//													if(result==1)
//													{
//														form.showDialogMessage("SMS Sent Successfully!");
//														saveSMSHistory(businessprocessId,custName);
//													}
//												}} );
											
											smsserviceAsync.sendMessage(AppConstants.SALESMODULE, AppConstants.DELIVERYNOTE, sms.getEvent(), model.getCompanyId(), actualMsg, cellNo,false, new AsyncCallback<String>() {
												
												@Override
												public void onSuccess(String result) {
													// TODO Auto-generated method stub
													form.hideWaitSymbol();
													form.showDialogMessage("Message Sent Successfully!");

												}
												
												@Override
												public void onFailure(Throwable caught) {
													// TODO Auto-generated method stub
													form.hideWaitSymbol();
												}
											});
											
										}
									}
								}
								@Override
								public void onFailure(Throwable caught) {
								}
							});
						}
					}
				}
				
				@Override
				public void onFailure(Throwable caught) {
				}
			});
	}	

	
	private void saveSMSHistory(int businessprocessid,String cname) {
		
		SmsHistory smshistory = new SmsHistory();
		smshistory.setDocumentId(businessprocessid);
		smshistory.setDocumentType("Delivery Note Approvable");
		smshistory.setName(cname);
		smshistory.setCellNo(cellNo);
		smshistory.setSmsText(actualMsg);
		smshistory.setUserId(LoginPresenter.loggedInUser);
		
		async.save(smshistory, new AsyncCallback<ReturnFromServer>() {
			
			@Override
			public void onSuccess(ReturnFromServer result) {
				System.out.println("Data Saved Successfully");
			}
			
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("Data Saved Successfully");
			}
		});
		
	}
	
	
	/**
	 * Method for viewing selected document material
	 */
	
	
	private void viewSelectedDocumentMaterial()
	{
		if(docType.equals(AppConstants.APPROVALCONTRACT)){
			System.out.println("INSIDE CONTRACT VIEW MATERIAL");
			getContractDetails(docId);
		}
		else if(docType.equals(AppConstants.APPROVALQUOTATION)){
			System.out.println("INSIDE QUOTATION VIEW MATERIAL");
			getQuotationDetails(docId);
		}else{
			form.showDialogMessage("This option is not available for "+docType+ " !");
		}
	}
	
	

	
	private void getContractDetails(final int contractId) {
		async.getSearchResult(approvalSelectionQuerry(),new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				form.showDialogMessage("An Unexpected Error occured !");
			}
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				for(SuperModel pmodel:result)
				{
					materialIst=new ArrayList<MaterialInfo>();
					counter=0;
					durCounter=0;
					materialpopup.productList.connectToLocal();
					Contract con=(Contract)pmodel;
					System.out.println("PRODUCT LIST SIZE :: "+con.getItems().size());
					System.out.println();
					int contractProductListSize=con.getItems().size();
					for(int i=0;i<con.getItems().size();i++){
						System.out.println("ContractId :: "+contractId);
						System.out.println("ProductId :: "+con.getItems().get(i).getPrduct().getCount());
						System.out.println("Product Duration :: "+con.getItems().get(i).getDuration());
						System.out.println();
						getBillOfMaterialDeatils(contractId,con.getItems().get(i).getPrduct().getCount(),con.getItems().get(i).getProductName(),con.getItems().get(i).getDuration(),contractProductListSize);
					}
				}
			}
		});
//		form.hideWaitSymbol();
	}
	
	
	private void getQuotationDetails(final int quotationId) {
		async.getSearchResult(approvalSelectionQuerry(),new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				form.showDialogMessage("An Unexpected Error occured !");
			}
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				for(SuperModel pmodel:result)
				{
					materialIst=new ArrayList<MaterialInfo>();
					counter=0;
					durCounter=0;
					materialpopup.productList.connectToLocal();
					Quotation quon=(Quotation)pmodel;
					System.out.println("PRODUCT LIST SIZE :: "+quon.getItems().size());
					int productListSize=quon.getItems().size();
					for(int i=0;i<quon.getItems().size();i++){
						System.out.println("ContractId :: "+quotationId);
						System.out.println("ProductId :: "+quon.getItems().get(i).getPrduct().getCount());
						System.out.println("Duration :: "+quon.getItems().get(i).getDuration());
						System.out.println();
						getBillOfMaterialDeatils(quotationId,quon.getItems().get(i).getPrduct().getCount(),quon.getItems().get(i).getProductName(),quon.getItems().get(i).getDuration(),productListSize);
					}
				}
			}
		});
	}
	
	
	
	private void getBillOfMaterialDeatils(final int conttractId,final int productId,final String productName,final int duration,final int prodListSize){
		
		System.out.println("BOM COntract ID : "+conttractId);
		System.out.println("BOM Product ID : "+productId);
		System.out.println("BOM Product Duration : "+duration);
		System.out.println("BOM CON Product Size : "+prodListSize);
		System.out.println();
		
		async.getSearchResult(billOfMaterialQuerry(productId),new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				form.showDialogMessage("An Unexpected Error occured !");
			}
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				if(result.size()!=0){
					for(SuperModel pmodel:result)
					{
						BillOfMaterial bom=(BillOfMaterial)pmodel;
						System.out.println("BOM PRODUCT ID :"+bom.getProduct_id());
						System.out.println("BILL OF MATERIAl LIST SIZE ::"+bom.getBillProdItems().size());
						System.out.println();
						
						if(bom.getBillProdItems().size()>=duration){
							System.out.println("BOM LIST IS LESS");
							durCounter=durCounter+duration;
							System.out.println("BILL OF MATERIAL SIZE > : "+duration+" TOTAL : "+durCounter);
							System.out.println();
							for(int i=0;i<duration;i++){
								System.out.println("BOM PRODUCT SERVICE NO. "+bom.getBillProdItems().get(i).getServiceNo());
								getProductGroupDeatils(conttractId,productId,productName,duration,bom.getBillProdItems().get(i).getProdGroupId(),bom.getBillProdItems().get(i).getServiceNo(),durCounter);
							}
						}
						if(bom.getBillProdItems().size()<duration)
						{
							System.out.println("ELSE BOM LIST IS GREATER");
//							durCounter=bom.getBillProdItems().size();
							durCounter=durCounter+bom.getBillProdItems().size();;
							System.out.println("BILL OF MATERIAL SIZE < : "+bom.getBillProdItems().size()+" TOTAL : "+durCounter);
							System.out.println();
							for(int i=0;i<bom.getBillProdItems().size();i++){
								System.out.println("BOM PRODUCT SERVICE NO. "+bom.getBillProdItems().get(i).getServiceNo());
								getProductGroupDeatils(conttractId,productId,productName,duration,bom.getBillProdItems().get(i).getProdGroupId(),bom.getBillProdItems().get(i).getServiceNo(),durCounter);
							}
						}
					}
				}else{
					form.showDialogMessage("Bill of material is not defined for "+productId);
//					return;
				}
			}
		});
	}
	
	private void getProductGroupDeatils(final int conttractId,final int productId,final String productName,final int duration,final int productGrpId,final int serviceNo,final int totalCounter){
		System.out.println();
		System.out.println("PGD COntract ID : "+conttractId);
		System.out.println("PGD Product ID : "+productId);
		System.out.println("PGD Product Duration : "+duration);
		System.out.println("PGD Grp ID : "+productGrpId);
		System.out.println();
		async.getSearchResult(productgroupQuerry(productGrpId),new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				form.showDialogMessage("An Unexpected Error occured !");
			}
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				System.out.println("PGD TOTAL COUNTER :"+totalCounter);
				
				if(result.size()!=0){
					for(SuperModel pmodel:result)
					{
						counter++;
						ProductGroupDetails pgd=(ProductGroupDetails)pmodel;
						for(int i=0;i<pgd.getPitems().size();i++){
							MaterialInfo info =new MaterialInfo();
							info.setSerProdId(productId);
							info.setSerNo(serviceNo);
							info.setSerProdName(productName);
							info.setMaterialProdId(pgd.getPitems().get(i).getProduct_id());
							info.setMaterialProdCode(pgd.getPitems().get(i).getCode());
							info.setMaterialProdName(pgd.getPitems().get(i).getName());
							info.setMaterialProdQty(pgd.getPitems().get(i).getQuantity());
							info.setMatrialProdUom(pgd.getPitems().get(i).getUnit());
							
							materialIst.add(info);
						}
					}
				}else{
					counter++;
				}
				
				System.out.println("Counter :: "+counter);
				if(totalCounter==counter){
					System.out.println("Inside Poppup Method :: "+counter);
					panel = new PopupPanel(true);
					panel.add(materialpopup);
					panel.show();
					panel.center();
					
					String text=docType+" : "+docId;
					materialpopup.getDisplayMessage().setText(text);
					
					Comparator<MaterialInfo> dropDownComparator1 = new Comparator<MaterialInfo>() {
						public int compare(MaterialInfo c1, MaterialInfo c2) {
							Integer configName1=c1.getSerNo();
							Integer configName2=c2.getSerNo();
							return configName1.compareTo(configName2);
						}
					};
					Collections.sort(materialIst,dropDownComparator1);
					
					
					Comparator<MaterialInfo> dropDownComparator = new Comparator<MaterialInfo>() {
						public int compare(MaterialInfo c1, MaterialInfo c2) {
							Integer configName1=c1.getSerProdId();
							Integer configName2=c2.getSerProdId();
							return configName1.compareTo(configName2);
						}
					};
					Collections.sort(materialIst,dropDownComparator);
					
					
					materialpopup.getProductList().getDataprovider().setList(materialIst);
				}
				
			}
		});
		
		
	}
	
	
	
	private MyQuerry billOfMaterialQuerry(int productId){
		
		MyQuerry querry=new MyQuerry();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		
		temp=new Filter();
		temp.setQuerryString("companyId");
		temp.setLongValue(model.getCompanyId());
		filtervec.add(temp);
		
		temp=new Filter();
		temp.setQuerryString("product_id");
		temp.setIntValue(productId);
		filtervec.add(temp);
		
		querry.setFilters(filtervec);
		
		querry.setQuerryObject(new BillOfMaterial());
		return querry;
	}
	
	private MyQuerry productgroupQuerry(int productGrpId){
		
		MyQuerry querry=new MyQuerry();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		
		temp=new Filter();
		temp.setQuerryString("companyId");
		temp.setLongValue(model.getCompanyId());
		filtervec.add(temp);
		
		temp=new Filter();
		temp.setQuerryString("count");
		temp.setIntValue(productGrpId);
		filtervec.add(temp);
		
		querry.setFilters(filtervec);
		
		querry.setQuerryObject(new ProductGroupDetails());
		return querry;
	}


	@Override
	public void onClick(ClickEvent event) {
		super.onClick(event);
		System.out.println("Inside Click");
		if(event.getSource()==materialpopup.getBtnClose()){
			System.out.println("CLOSE");
			panel.hide();
		}
		
		if(event.getSource()==materialpopup.getBtnDownload()){
			System.out.println("DOWNLOAD");
			reactOnDownload1();
//			panel.hide();
		}
		if(event.getSource()==materialpopup.getBtnPrint()){
			System.out.println("PRINT");
			reactOnPrint1();
		}
		
		
		if(event.getSource().equals(conditionPopup.getBtnOne()))
		{
			System.out.println("Rohan inside onlick of button 1");
			
			if(docType.equals(AppConstants.APPROVALCONTRACT)){
				if (cnt > 0) {
					final String url = GWT.getModuleBaseURL() + "pdfservice"+ "?Id=" + con.getId() + "&" + "type=" + "c" + "&"+ "preprint=" + "yes";
					Window.open(url, "test", "enabled");
					conditionpanel.hide();
				}

				else if (pestoIndia > 0) {
					final String url = GWT.getModuleBaseURL()+ "pdfpestoIndiaservice" + "?Id=" + con.getId() + "&"+ "type=" + "c" + "&" + "preprint=" + "yes";
					Window.open(url, "test", "enabled");
					conditionpanel.hide();
					
				} else if (vcare > 0) {
					final String url = GWT.getModuleBaseURL() + "Contract" + "?Id="	+ con.getId() + "&" + "preprint=" + "yes";
					Window.open(url, "test", "enabled");
					conditionpanel.hide();
					
				} else if (universal > 0) {
					final String url = GWT.getModuleBaseURL()+ "universalServiceQuotation" + "?Id=" + con.getId()	+ "&" + "preprint=" + "yes" + "&" + "type=" + "c";
					Window.open(url, "test", "enabled");
					conditionpanel.hide();
				}
			}
			
			if(docType.equals(AppConstants.APPROVALQUOTATION)){
				if(cnt >0){
					System.out.println("in side one yes");
					final String url = GWT.getModuleBaseURL() + "pdfservice"+"?Id="+quotation.getId()+"&"+"type="+"q"+"&"+"preprint="+"yes";
					Window.open(url, "test", "enabled");
					
					conditionpanel.hide();
				}
				
				else if(customQuotation >0)
				{
					System.out.println("customQuotation value"+customQuotation);
					
					if(customQuotation > 0 && quotation.getGroup().equalsIgnoreCase("Five Years"))
					{
						System.out.println("in side five years ");
						final String url = GWT.getModuleBaseURL() + "customQuotation"+"?Id="+quotation.getId()+"&"+"group="+"Five Years"+"&"+"preprint="+"yes";
						 Window.open(url, "test", "enabled");
						 
						 conditionpanel.hide();
					}
					
					else if(customQuotation > 0 && quotation.getGroup().equalsIgnoreCase("Direct Quotation"))
					{
						System.out.println("in side Direct Quotation ");
						final String url = GWT.getModuleBaseURL() + "customQuotation"+"?Id="+quotation.getId()+"&"+"group="+"Direct Quotation"+"&"+"preprint="+"yes";
						 Window.open(url, "test", "enabled");
						 
						 conditionpanel.hide();
					}
					
					else if(customQuotation > 0 && quotation.getGroup().equalsIgnoreCase("Building"))
					{
						System.out.println("in side Building ");
						 final String url = GWT.getModuleBaseURL() + "customQuotation"+"?Id="+quotation.getId()+"&"+"group="+"Building"+"&"+"preprint="+"yes";
						 Window.open(url, "test", "enabled");
						 
						 conditionpanel.hide();
					}
					
					else if(customQuotation > 0 && quotation.getGroup().equalsIgnoreCase("After Inspection"))
					{
						System.out.println("in side After Inspection ");
						 final String url = GWT.getModuleBaseURL() + "customQuotation"+"?Id="+quotation.getId()+"&"+"group="+"After Inspection"+"&"+"preprint="+"yes";
						 Window.open(url, "test", "enabled");
						 
						 conditionpanel.hide();
					}
					
					
				}
				
				
				else if(pestQuotation >0)
				{
					System.out.println("Pestle Pest quotation");
					final String url = GWT.getModuleBaseURL() + "pdfpestleservice"+"?Id="+quotation.getId()+"&"+"group="+quotation.getGroup()+"&"+"preprint="+"yes";//1st PDF
					Window.open(url, "test", "enabled");
					conditionpanel.hide();
				}
				
				else if(pestoIndia > 0)
				{
					System.out.println("Pesto india quotation");
					final String url = GWT.getModuleBaseURL() + "pdfpestoIndiaservice"+"?Id="+quotation.getId()+"&"+"type="+"q"+"&"+"preprint="+"yes"; //+"&"+"group="+model.getGroup();//1st PDF
					Window.open(url, "test", "enabled");
					conditionpanel.hide();
				}
				else if(nbhc > 0)
				{
					System.out.println("nbhc quotation");
					final String url = GWT.getModuleBaseURL() + "nbhcServiceQuotation"+"?Id="+quotation.getId()+"&"+"preprint="+"yes"; 
					Window.open(url, "test", "enabled");
					conditionpanel.hide();
				}
				else if(universal > 0)
				{
					System.out.println("universal pest quotation");
					final String url = GWT.getModuleBaseURL() + "universalServiceQuotation"+"?Id="+quotation.getId()+"&"+"preprint="+"yes"+"&"+"type="+"q"; 
					Window.open(url, "test", "enabled");
					conditionpanel.hide();
				}
			}
			
			if(docType.equals(AppConstants.APPROVALINVOICEDETAILS)){
				
				if(cnt >0)
				{
					System.out.println("in side one yes");
					final String url = GWT.getModuleBaseURL() + "serviceinvoiceprint"+"?Id="+invoiceEntity.getId()+"&"+"preprint="+"yes"+"&"+"type="+"SingleBilling";
					Window.open(url, "test", "enabled");
					conditionpanel.hide();
				}
				
				else if(pestoIndia > 0)
				{
					System.out.println("Pesto india quotation");
					final String url = GWT.getModuleBaseURL() + "pdfpestoIndiaservice"+"?Id="+invoiceEntity.getId()+"&"+"type="+"i"+"&"+"preprint="+"yes"; 
					Window.open(url, "test", "enabled");
					
					conditionpanel.hide();
					
				}
				
				else if(vCare > 0)
				{
					System.out.println("V care quotation");
					
					final String url = GWT.getModuleBaseURL() + "Invoice"+"?Id="+invoiceEntity.getId()+"&"+"preprint="+"yes";
					Window.open(url, "test", "enabled");
					
					conditionpanel.hide();
					
				}
				else if(omPestInvoice > 0)
				{
					final String url = GWT.getModuleBaseURL() + "invoicepdf"+"?Id="+invoiceEntity.getId()+"&"+"preprint="+"yes";
					Window.open(url, "test", "enabled");
					conditionpanel.hide();
				}
			}
			
			
			
		}
		
		if(event.getSource().equals(conditionPopup.getBtnTwo()))
		{
			System.out.println("Rohan inside onlick of button 2");
			
			if(docType.equals(AppConstants.APPROVALCONTRACT)){
				if (cnt > 0) {

					final String url = GWT.getModuleBaseURL() + "pdfservice"+ "?Id=" + con.getId() + "&" + "type=" + "c" + "&"+ "preprint=" + "no";
					Window.open(url, "test", "enabled");
					conditionpanel.hide();

				}

				else if (pestoIndia > 0) {
					System.out.println("Pesto india quotation");
					final String url = GWT.getModuleBaseURL()+ "pdfpestoIndiaservice" + "?Id=" + con.getId() + "&"+ "type=" + "c" + "&" + "preprint=" + "no"; // +"&"+"group="+model.getGroup();//1st
					Window.open(url, "test", "enabled");
					conditionpanel.hide();

				} else if (vcare > 0) {
					System.out.println("vcare contract");
					final String url = GWT.getModuleBaseURL() + "Contract" + "?Id="+ con.getId() + "&" + "preprint=" + "no";
					Window.open(url, "test", "enabled");
					conditionpanel.hide();
				}

				else if (universal > 0) {
					System.out.println("universal pest quotation");
					final String url = GWT.getModuleBaseURL()+ "universalServiceQuotation" + "?Id=" + con.getId()	+ "&" + "preprint=" + "no" + "&" + "type=" + "c";
					Window.open(url, "test", "enabled");
					conditionpanel.hide();
				}
			}
			
			if(docType.equals(AppConstants.APPROVALQUOTATION)){
				if(cnt >0){
					System.out.println("inside two no");
					final String url = GWT.getModuleBaseURL() + "pdfservice"+"?Id="+quotation.getId()+"&"+"type="+"q"+"&"+"preprint="+"no";
					Window.open(url, "test", "enabled");
				    
					conditionpanel.hide();
				}
				
				else if(customQuotation >0)
				{
					System.out.println("customQuotation value"+customQuotation);
					
					if(customQuotation > 0 && quotation.getGroup().equalsIgnoreCase("Five Years"))
					{
						System.out.println("in side five years ");
						final String url = GWT.getModuleBaseURL() + "customQuotation"+"?Id="+quotation.getId()+"&"+"group="+"Five Years"+"&"+"preprint="+"no";
						 Window.open(url, "test", "enabled");
						 
						 conditionpanel.hide();
					}
					
					else if(customQuotation > 0 && quotation.getGroup().equalsIgnoreCase("Direct Quotation"))
					{
						System.out.println("in side Direct Quotation ");
						final String url = GWT.getModuleBaseURL() + "customQuotation"+"?Id="+quotation.getId()+"&"+"group="+"Direct Quotation"+"&"+"preprint="+"no";
						 Window.open(url, "test", "enabled");
						 
						 conditionpanel.hide();
					}
					
					else if(customQuotation > 0 && quotation.getGroup().equalsIgnoreCase("Building"))
					{
						System.out.println("in side Building ");
						 final String url = GWT.getModuleBaseURL() + "customQuotation"+"?Id="+quotation.getId()+"&"+"group="+"Building"+"&"+"preprint="+"no";
						 Window.open(url, "test", "enabled");
						 
						 conditionpanel.hide();
					}
					
					else if(customQuotation > 0 && quotation.getGroup().equalsIgnoreCase("After Inspection"))
					{
						System.out.println("in side After Inspection ");
						 final String url = GWT.getModuleBaseURL() + "customQuotation"+"?Id="+quotation.getId()+"&"+"group="+"After Inspection"+"&"+"preprint="+"no";
						 Window.open(url, "test", "enabled");
						 
						 conditionpanel.hide();
					}
				}
				
				else if(pestQuotation >0)
				{
							System.out.println("in side no condition");
							final String url = GWT.getModuleBaseURL() + "pdfpestleservice"+"?Id="+quotation.getId()+"&"+"group="+quotation.getGroup()+"&"+"preprint="+"no";//1st PDF
							Window.open(url, "test", "enabled");
							
							conditionpanel.hide();
					
				}
				
				else if(pestoIndia > 0)
				{
					System.out.println("Pesto india quotation");
					final String url = GWT.getModuleBaseURL() + "pdfpestoIndiaservice"+"?Id="+quotation.getId()+"&"+"type="+"q"+"&"+"preprint="+"no"; //+"&"+"group="+model.getGroup();//1st PDF
					Window.open(url, "test", "enabled");
				
					conditionpanel.hide();
				
				}
				
				else if(nbhc > 0)
				{
					System.out.println("nbhc quotation");
					final String url = GWT.getModuleBaseURL() + "nbhcServiceQuotation"+"?Id="+quotation.getId()+"&"+"preprint="+"no"; 
					Window.open(url, "test", "enabled");
					conditionpanel.hide();
				}
				else if(universal > 0)
				{
					System.out.println("universal pest quotation");
					final String url = GWT.getModuleBaseURL() + "universalServiceQuotation"+"?Id="+quotation.getId()+"&"+"preprint="+"no"+"&"+"type="+"q"; 
					Window.open(url, "test", "enabled");
					conditionpanel.hide();
				}
			}
			
			if(docType.equals(AppConstants.APPROVALINVOICEDETAILS)){
				
				if(cnt >0)
				{
					System.out.println("inside two no");
					final String url = GWT.getModuleBaseURL() + "serviceinvoiceprint"+"?Id="+invoiceEntity.getId()+"&"+"preprint="+"no"+"&"+"type="+"SingleBilling";
					Window.open(url, "test", "enabled");
					conditionpanel.hide();
					
				}
				
				else if(pestoIndia > 0)
				{
					System.out.println("Pesto india quotation");
					final String url = GWT.getModuleBaseURL() + "pdfpestoIndiaservice"+"?Id="+invoiceEntity.getId()+"&"+"type="+"i"+"&"+"preprint="+"no"; //+"&"+"group="+model.getGroup();//1st PDF
					Window.open(url, "test", "enabled");
					conditionpanel.hide();
				
				}
				else if(vCare > 0)
				{
					System.out.println("V care quotation");
					final String url = GWT.getModuleBaseURL() + "Invoice"+"?Id="+invoiceEntity.getId()+"&"+"preprint="+"no";
					Window.open(url, "test", "enabled");
					
					conditionpanel.hide();
					
				}
				
				else if(omPestInvoice > 0)
				{
					final String url = GWT.getModuleBaseURL() + "invoicepdf"+"?Id="+invoiceEntity.getId()+"&"+"preprint="+"no";
					Window.open(url, "test", "enabled");
					conditionpanel.hide();
				}
			}

		}
		
		/**
		 *  nidhi
		 *  5-10-2017
		 *  for popup approval  button click
		 */
		if(event.getSource().equals(btnApproval)){
//			form.showDialogMessage("An Unexpected Error occured !");
			/**
			 * Date:27-12-2018 BY ANIL
			 * added try catch block to identify the exception
			 */
			try{
				form.table.popupApprovalBtnClick(appobj, rowIndex);
			}catch(Exception e){
				e.printStackTrace();
				Console.log(e.getMessage());
			}
			/**
			 * End
			 */
		}
		
		if(event.getSource().equals(btnReject)){
//			form.showDialogMessage("An Unexpected Error occured !");
			form.table.popupRejectBtnClick(appobj, rowIndex);
		}
		
	}
	
	public void reactOnDownload1() {
		ArrayList<MaterialInfo> woArray = new ArrayList<MaterialInfo>();
		List<MaterialInfo> list = materialpopup.productList.getDataprovider().getList();
		woArray.addAll(list);
		csvservice.setMateriallist(woArray, new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed" + caught);
			}
			@Override
			public void onSuccess(Void result) {
				String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url = gwt + "csvservlet" + "?type=" + 96;
				Window.open(url, "test", "enabled");
			}
		});
	}
	
	public void reactOnPrint1(){
		System.out.println("DOC TYPE :: "+docType);
		System.out.println("DOC ID :: "+docId);
		System.out.println("Company ID :: "+model.getCompanyId());
		final String url = GWT.getModuleBaseURL() + "materialpdfservice" + "?docType="+ docType+ "&"+"docId="+docId+"&"+"CompanyId="+ model.getCompanyId();
		Window.open(url, "test", "enabled");
	}
	

	/* Date :- 19-sep-2016
	 * Release Date - 30-sep-2016
	 * by - RPC call to send email for Request for Approval TO Approver and CC to all remaining approver level 
	 */
	
	public void reactRequestForMultilevelApprovalEmail(Approvals approve, ArrayList<String> toEmailList, ArrayList<String> ccEmailList){
		
		emailService.requestForApprovalEmailForMultilevelapproval(approve,toEmailList,ccEmailList, new AsyncCallback<Void>() {
			
			@Override
			public void onSuccess(Void result) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				Window.alert("Unable To Send Email");
				caught.printStackTrace();
			}
		});
	}

	/* Date :- 19-sep-2016
	 * Release Date - 30-sep-2016
	 * by - RPC call to send email for Approved Document TO Approver and CC to all remaining approver level and CC - sales person or purchase eng or person responsible
	 */
	public void reactMultilevelApprovedEmail(Approvals approval,ArrayList<String> toEmailEmployeeList,ArrayList<String> ccEmailEmployeeList) {
		// TODO Auto-generated method stub
		emailService.emailForMultilevelapproval(approval, toEmailEmployeeList, ccEmailEmployeeList, new AsyncCallback<Void>() {
			
			@Override
			public void onSuccess(Void result) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				Window.alert("Unable To Send Email");
				caught.printStackTrace();
			}
		});
	}
	
	
	private void setTableSelectionForcampainMgnt() {
		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
	   	AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Campaign Management",Screen.CUSTOMERTRAINING);
	   	final CustomerTrainingForm form = CustomerTrainingPresenter.initalize();
	   	AppMemory.getAppMemory().stickPnel(form);
	   	form.showWaitSymbol();
		Timer timer=new Timer() 
	   	{
			@Override
			public void run() {
				async.getSearchResult(approvalSelectionQuerry(),new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onFailure(Throwable caught) {
						form.showDialogMessage("An Unexpected Error occured !");
					}
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						
						
						for(SuperModel pmodel:result)
						{
							CustomerTrainingDetails po=(CustomerTrainingDetails)pmodel;
							form.updateView(po);
							form.setToViewState();
						}
					}
				});
				form.hideWaitSymbol();
			}
		};
        timer.schedule(3000); 
	}
	
	/*
	 *  nidhi
	 *  	5-07-2017
	 *   Pop approval doc detail display methods call
	 *   
	 *   Date : 11-07-2017 By ANIL
	 *   New method added by Nidhi for viewing approvable document at the time of approval'\
	 *   it loads screen on popup instead of loading whole form
	 *
	 */
	private void viewSelectedDocumentOnPopup()
	{
		if(docType.equals(AppConstants.APPROVALCONTRACT)){
			setTableSelectionPopForContract();	
		}
		if(docType.equals(AppConstants.APPROVALDELIVERYNOTE)){
			setTableSelectionPopForDeliveryNote();	
		}
		if(docType.equals(AppConstants.APPROVALSALESORDER)){
			setTableSelectionPopForSalesOrder();
		}
		if(docType.equals(AppConstants.APPROVALQUOTATION)){
			setTableSelectionPopForQuotation();
		}
		if(docType.equals(AppConstants.APPROVALSALESQUOTATION)){
			setTableSelectionPopForSalesQuotation();
		}
		if(docType.equals(AppConstants.APPROVALPURCHASEORDER)){
			setTableSelectionPopForPurchaseOrder();
		}
		if(docType.equals(AppConstants.APPROVALGRN)){
			setTableSelectionPopForGRN();
		}
		if(docType.equals(AppConstants.APPROVALPURCHASEREQUISITION)){
			setTableSelectionPopForPurchaseRequisition();	
		}
		if(docType.equals(AppConstants.APPROVALEXPENSEMANAGMENT)){
			setTableSelectionPopForExpenseManagement();
		}
		if(docType.equals(AppConstants.APPROVALMATERIALISSUENOTE)){
			setTableSelectionPopForMaterialIssueNote();
		}
		if(docType.equals(AppConstants.APPROVALMATERIALMOVEMENTNOTE)){
			setTableSelectionPopForMaterialMovementNote();
		}
		if(docType.equals(AppConstants.APPROVALMATERIALREQUESTNOTE)){
			setTableSelectionPopForMaterialRequestNote();
		}
		if(docType.equals(AppConstants.APPROVALREQUESTFORQUOTATION)){
			setTableSelectionPopForRequestForQuotation();
		}
		if(docType.equals(AppConstants.APPROVALINVOICEDETAILS)){
			setTableSelectionPopForInvoiceDetails();
		}
		if(docType.equals(AppConstants.APPROVALBILLINGDETAILS)){
			setTableSelectionPopForBillingDetails();
		}
		if(docType.equals(AppConstants.APPROVALLETTEROFINTENT)){
			setTableSelectionPopForLetterOfIntent();
		}
		if(docType.equals(AppConstants.APPROVALLEAVEAPPLICATION)){
			setTableSelectionPopForLeaveApplication();
		}
		if(docType.equals(AppConstants.APPROVALCOMPLAIN)){
			setTableSelectionPopForComplain();
		}
		if(docType.equals(AppConstants.APPROVALADVANCE)){
			setTableSelectionPopForAdvanceRequest();
		}
		if(docType.equals(AppConstants.APPROVALINSPECTION)){
			setTableSelectionPopForInspectionRequest();
		}
		if(docType.equals(AppConstants.APPROVALWORKORDER)){
			setTableSelectionPopForWorkOrderRequest();
		}
		
		if(docType.equals(AppConstants.APPROVALSERVICEPO)){
			setTableSelectionPopForServicePoRequest();
		}
		
		if(docType.equals(AppConstants.APPROVALMULTIPLEEXPENSEMANAGMENT)){
			setTableSelectionPopForMultipleExpenseRequest();
		}
		/**
		 * Date: 17-06-2017 BY ANIL
		 * View document for Customer
		 */
		if(docType.equals(AppConstants.APPROVALCUSTOMER)){
			setTableSelectionPopForCustomerRequest();
		}
		/**
		 * End
		 */
		
		
		/*
		 *  NIDHI 
		 *  10-07-2017
		 */
		if(docType.equals(AppConstants.APPROVALCAMPAIGNMANNAGEMENT)){
			setTableSelectionPopForcampainMgnt();
		}
		/**
		 * nidhi
		 * 26-04-2018
		 * for all pop up view
		 */
		if(docType.equals(AppConstants.APPROVALTIMEREPORT)){
			setTableSelectionPopForTimeReport();
		}
		
		/**
		 * Date: 12-09-2018 BY ANIL
		 * View document for Employee
		 */
		if(docType.equals(AppConstants.APPROVALEMPLOYEE)){
			setTableSelectionForEmployeeRequest();
		}
		/**
		 * End
		 */
		if(docType.equals(AppConstants.CNC)){
			setTableSelectionForCNC();
		}
		if(docType.equals(AppConstants.PHYSICALINVENTORY)){
			setPhysicalInventoryMaintence();
		}
	}
	
	/*
	 *  end
	 */
	
	
	private void setTableSelectionForEmployeeRequest() {
		// TODO Auto-generated method stub
		removePopObjects();

		final EmployeeForm form = new EmployeeForm();
		docVerticalPanel.add(form.content);
		form.content.getElement().getStyle().setHeight(570, Unit.PX);
		form.content.getElement().getStyle().setWidth(950, Unit.PX);
		docApprovalPanel.show();
		docApprovalPanel.center();
		form.showWaitSymbol();

		Timer timer = new Timer() {
			@Override
			public void run() {
				async.getSearchResult(approvalSelectionQuerry(),new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onFailure(Throwable caught) {
						form.showDialogMessage("An Unexpected Error occured !");
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {

						for (SuperModel pmodel : result) {
							Employee po = (Employee) pmodel;
							form.updateView(po);
							form.setEnable(false);
							form.getProcessLevelBar().setVisibleFalse(false);
						}
					}
				});
				form.hideWaitSymbol();
			}
		};
		timer.schedule(3000);
	}
	

	
	/*
	 *  nidhi
	 *  	5-07-2017
	 *   popview display method starts
	 *
	 */
	
	/*
	 *  method for remove pop objects
	 */
	void removePopObjects() {
		try {

			for (Widget wi : docVerticalPanel) {
				boolean flag = docApprovalPanel.remove(wi);
				wi.removeFromParent();
				System.out.println("get item remove status " + flag);
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	/*
	 *  end
	 */
	
	private void setTableSelectionPopForContract() {
		try {
			removePopObjects();

			final ContractForm form = new ContractForm();
//			ContractPresenter presenter = new ContractPresenter(form,new Contract());
			this.form.toggleAppHeaderBarMenu();
			form.content.getElement().getStyle().setHeight(570, Unit.PX);
			form.content.getElement().getStyle().setWidth(950, Unit.PX);
			docVerticalPanel.add(form.content);
			// docApprovalPanel.add(docVerticalPanel);
			docApprovalPanel.setHeight("600px");
			docApprovalPanel.show();
			docApprovalPanel.center();

			form.showWaitSymbol();

			Timer timer = new Timer() {
				@Override
				public void run() {

					async.getSearchResult(approvalSelectionQuerry(),new AsyncCallback<ArrayList<SuperModel>>() {

						@Override
						public void onFailure(Throwable caught) {
							form.showDialogMessage("An Unexpected Error occured !");

						}

						@Override
						public void onSuccess(
								ArrayList<SuperModel> result) {

							System.out.println("get sucess...!!");
							for (SuperModel pmodel : result) {
								Contract con = (Contract) pmodel;
								form.updateView(con);
								form.setEnable(false);
								form.getProcessLevelBar().setVisibleFalse(false);
							}
						}
					});

					form.hideWaitSymbol();
				}
			};
			timer.schedule(3000);

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		//
	}
	
	
	private void setTableSelectionPopForSalesOrder() {

		removePopObjects();

		final SalesOrderForm form = new SalesOrderForm();
		form.content.getElement().getStyle().setHeight(570, Unit.PX);
		form.content.getElement().getStyle().setWidth(950, Unit.PX);
		docVerticalPanel.add(form.content);
		docApprovalPanel.setHeight("600px");
		docApprovalPanel.show();
		docApprovalPanel.center();

		form.showWaitSymbol();

		Timer timer = new Timer() {
			@Override
			public void run() {

				async.getSearchResult(approvalSelectionQuerry(),
						new AsyncCallback<ArrayList<SuperModel>>() {

							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected Error occured !");

							}

							@Override
							public void onSuccess(ArrayList<SuperModel> result) {

								for (SuperModel pmodel : result) {
									SalesOrder sales = (SalesOrder) pmodel;
									form.updateView(sales);
									form.setEnable(false);
									if(form.marginFlag){
										form.costingBtn.setEnabled(true);
										form.marginPopup.vendorMarginTbl.setEnable(false);
										form.marginPopup.otherChargesMarginTbl.setEnable(false);
									}
									form.getProcessLevelBar().setVisibleFalse(false);
								}

							}
						});

				form.hideWaitSymbol();
			}
		};
		timer.schedule(3000);
	}

	private void setTableSelectionPopForQuotation() {

		removePopObjects();
		final QuotationForm form = new QuotationForm();
//		QuotationPresenter presenter = new QuotationPresenter(form,new Quotation());
		this.form.toggleAppHeaderBarMenu();
		docVerticalPanel.add(form.content);
		form.content.getElement().getStyle().setHeight(570, Unit.PX);
		form.content.getElement().getStyle().setWidth(950, Unit.PX);
		docApprovalPanel.show();
		docApprovalPanel.center();
		form.showWaitSymbol();
		Timer timer = new Timer() {
			@Override
			public void run() {

				async.getSearchResult(approvalSelectionQuerry(),
						new AsyncCallback<ArrayList<SuperModel>>() {

							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected Error occured !");
							}

							@Override
							public void onSuccess(ArrayList<SuperModel> result) {

								for (SuperModel pmodel : result) {
									Quotation quotation = (Quotation) pmodel;
									form.updateView(quotation);
									form.setEnable(false);
									form.getProcessLevelBar().setVisibleFalse(
											false);
								}
							}
						});

				form.hideWaitSymbol();
			}
		};
		timer.schedule(3000);
	}

	private void setTableSelectionPopForSalesQuotation() {

		removePopObjects();

		final SalesQuotationForm form = new SalesQuotationForm();
		docVerticalPanel.add(form.content);
		form.content.getElement().getStyle().setHeight(570, Unit.PX);
		form.content.getElement().getStyle().setWidth(950, Unit.PX);
		docApprovalPanel.show();
		docApprovalPanel.center();
		form.showWaitSymbol();
		Timer timer = new Timer() {
			@Override
			public void run() {

				async.getSearchResult(approvalSelectionQuerry(),
						new AsyncCallback<ArrayList<SuperModel>>() {

							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected Error occured !");

							}

							@Override
							public void onSuccess(ArrayList<SuperModel> result) {

								for (SuperModel pmodel : result) {
									SalesQuotation quotation = (SalesQuotation) pmodel;
									form.updateView(quotation);
									form.setEnable(false);
									if(form.marginFlag){
										form.costingBtn.setEnabled(true);
										form.marginPopup.vendorMarginTbl.setEnable(false);
										form.marginPopup.otherChargesMarginTbl.setEnable(false);
									}
									form.getProcessLevelBar().setVisibleFalse(
											false);
								}

							}
						});

				form.hideWaitSymbol();
			}
		};
		timer.schedule(3000);
	}

	private void setTableSelectionPopForPurchaseOrder() {

		removePopObjects();

		final PurchaseOrderForm form = new PurchaseOrderForm();
		docVerticalPanel.add(form.content);
		form.content.getElement().getStyle().setHeight(570, Unit.PX);
		form.content.getElement().getStyle().setWidth(950, Unit.PX);
		docApprovalPanel.show();
		docApprovalPanel.center();

		form.showWaitSymbol();

		Timer timer = new Timer() {
			@Override
			public void run() {

				async.getSearchResult(approvalSelectionQuerry(),
						new AsyncCallback<ArrayList<SuperModel>>() {

							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected Error occured !");
							}

							@Override
							public void onSuccess(ArrayList<SuperModel> result) {

								for (SuperModel pmodel : result) {
									PurchaseOrder purchase = (PurchaseOrder) pmodel;
									form.updateView(purchase);
									form.setEnable(false);
									if(form.marginFlag){
										form.costingBtn.setEnabled(true);
										form.marginPopup.vendorMarginTbl.setEnable(false);
										form.marginPopup.otherChargesMarginTbl.setEnable(false);
										form.marginPopup.btnAdd.setEnabled(false);
										form.marginPopup.tbCostType.setEnabled(false);
										form.marginPopup.dbAmount.setEnabled(false);
									}
									form.getProcessLevelBar().setVisibleFalse(
											false);
								}
							}
						});

				form.hideWaitSymbol();
			}
		};
		timer.schedule(3000);
	}

	private void setTableSelectionPopForGRN() {

		removePopObjects();
		final GRNForm form = new GRNForm();
//		GRNPresenter presenter = new GRNPresenter(form, new GRN());
		docVerticalPanel.add(form.content);
		form.content.getElement().getStyle().setHeight(570, Unit.PX);
		form.content.getElement().getStyle().setWidth(950, Unit.PX);
		docApprovalPanel.show();
		docApprovalPanel.center();
		form.showWaitSymbol();

		Timer timer = new Timer() {
			@Override
			public void run() {

				async.getSearchResult(approvalSelectionQuerry(),
						new AsyncCallback<ArrayList<SuperModel>>() {

							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected Error occured !");
							}

							@Override
							public void onSuccess(ArrayList<SuperModel> result) {

								for (SuperModel pmodel : result) {
									GRN grn = (GRN) pmodel;
									form.updateView(grn);
									form.setEnable(false);
									form.getProcessLevelBar().setVisibleFalse(false);
								}
							}
						});

				form.hideWaitSymbol();
			}
		};
		timer.schedule(3000);
	}

	private void setTableSelectionPopForDeliveryNote() {

		removePopObjects();

		final DeliveryNoteForm form = new DeliveryNoteForm();
//		DeliveryNotePresenter presenter = new DeliveryNotePresenter(form,new DeliveryNote());
		docVerticalPanel.add(form.content);
		form.content.getElement().getStyle().setHeight(570, Unit.PX);
		form.content.getElement().getStyle().setWidth(950, Unit.PX);
		docApprovalPanel.show();
		docApprovalPanel.center();
		form.showWaitSymbol();

		Timer timer = new Timer() {
			@Override
			public void run() {

				async.getSearchResult(approvalSelectionQuerry(),
						new AsyncCallback<ArrayList<SuperModel>>() {

							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected Error occured !");

							}

							@Override
							public void onSuccess(ArrayList<SuperModel> result) {

								for (SuperModel pmodel : result) {
									DeliveryNote con = (DeliveryNote) pmodel;
									form.updateView(con);
									form.setEnable(false);
									form.getProcessLevelBar().setVisibleFalse(false);
								}
							}
						});

				form.hideWaitSymbol();
			}
		};
		timer.schedule(3000);
	}

	private void setTableSelectionPopForPurchaseRequisition() {

		removePopObjects();

		final PurchaseRequisitionForm form = new PurchaseRequisitionForm();
		docVerticalPanel.add(form.content);
		form.content.getElement().getStyle().setHeight(570, Unit.PX);
		form.content.getElement().getStyle().setWidth(950, Unit.PX);
		docApprovalPanel.show();
		docApprovalPanel.center();
		form.showWaitSymbol();

		Timer timer = new Timer() {
			@Override
			public void run() {

				async.getSearchResult(approvalSelectionQuerry(),
						new AsyncCallback<ArrayList<SuperModel>>() {

							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected Error occured !");
							}

							@Override
							public void onSuccess(ArrayList<SuperModel> result) {

								for (SuperModel pmodel : result) {
									PurchaseRequisition pr = (PurchaseRequisition) pmodel;
									form.updateView(pr);
									form.setEnable(false);
									form.getProcessLevelBar().setVisibleFalse(
											false);
								}
							}
						});

				form.hideWaitSymbol();
			}
		};
		timer.schedule(3000);
	}

	private void setTableSelectionPopForExpenseManagement() {

		removePopObjects();

		final ExpensemanagmentForm form = new ExpensemanagmentForm();
		docVerticalPanel.add(form.content);
		form.content.getElement().getStyle().setHeight(570, Unit.PX);
		form.content.getElement().getStyle().setWidth(950, Unit.PX);
		docApprovalPanel.show();
		docApprovalPanel.center();
		form.showWaitSymbol();

		Timer timer = new Timer() {
			@Override
			public void run() {

				async.getSearchResult(approvalSelectionQuerry(),
						new AsyncCallback<ArrayList<SuperModel>>() {

							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected Error occured !");
							}

							@Override
							public void onSuccess(ArrayList<SuperModel> result) {

								for (SuperModel pmodel : result) {
									Expense expence = (Expense) pmodel;
									form.updateView(expence);
									form.setEnable(false);
									form.getProcessLevelBar().setVisibleFalse(false);
								}
							}
						});

				form.hideWaitSymbol();
			}
		};
		timer.schedule(3000);
	}

	private void setTableSelectionPopForMaterialIssueNote() {

		removePopObjects();

		final MaterialIssueNoteForm form = new MaterialIssueNoteForm();
		docVerticalPanel.add(form.content);
		form.content.getElement().getStyle().setHeight(570, Unit.PX);
		form.content.getElement().getStyle().setWidth(950, Unit.PX);
		docApprovalPanel.show();
		docApprovalPanel.center();

		form.showWaitSymbol();

		Timer timer = new Timer() {
			@Override
			public void run() {

				async.getSearchResult(approvalSelectionQuerry(),
						new AsyncCallback<ArrayList<SuperModel>>() {

							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected Error occured !");
							}

							@Override
							public void onSuccess(ArrayList<SuperModel> result) {

								for (SuperModel pmodel : result) {
									MaterialIssueNote min = (MaterialIssueNote) pmodel;
									form.updateView(min);
									form.setEnable(false);
									form.getProcessLevelBar().setVisibleFalse(
											false);
								}
							}
						});

				form.hideWaitSymbol();
			}
		};
		timer.schedule(3000);
	}

	private void setTableSelectionPopForMaterialMovementNote() {

		removePopObjects();

		final MaterialMovementNoteForm form = new MaterialMovementNoteForm();
		docVerticalPanel.add(form.content);
		form.content.getElement().getStyle().setHeight(570, Unit.PX);
		form.content.getElement().getStyle().setWidth(950, Unit.PX);
		docApprovalPanel.show();
		docApprovalPanel.center();
		form.showWaitSymbol();

		Timer timer = new Timer() {
			@Override
			public void run() {

				async.getSearchResult(approvalSelectionQuerry(),
						new AsyncCallback<ArrayList<SuperModel>>() {

							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected Error occured !");
							}

							@Override
							public void onSuccess(ArrayList<SuperModel> result) {

								for (SuperModel pmodel : result) {
									MaterialMovementNote mMnote = (MaterialMovementNote) pmodel;
									form.updateView(mMnote);
									form.setEnable(false);
									form.getProcessLevelBar().setVisibleFalse(
											false);
								}
							}
						});

				form.hideWaitSymbol();
			}
		};
		timer.schedule(3000);
	}

	private void setTableSelectionPopForMaterialRequestNote() {

		removePopObjects();

		final MaterialRequestNoteForm form = new MaterialRequestNoteForm();
		docVerticalPanel.add(form.content);
		form.content.getElement().getStyle().setHeight(570, Unit.PX);
		form.content.getElement().getStyle().setWidth(950, Unit.PX);
		docApprovalPanel.show();
		docApprovalPanel.center();
		form.showWaitSymbol();

		Timer timer = new Timer() {
			@Override
			public void run() {

				async.getSearchResult(approvalSelectionQuerry(),
						new AsyncCallback<ArrayList<SuperModel>>() {

							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected Error occured !");
							}

							@Override
							public void onSuccess(ArrayList<SuperModel> result) {

								for (SuperModel pmodel : result) {
									MaterialRequestNote mrn = (MaterialRequestNote) pmodel;
									form.updateView(mrn);
									form.setEnable(false);
									form.getProcessLevelBar().setVisibleFalse(
											false);
								}
							}
						});

				form.hideWaitSymbol();
			}
		};
		timer.schedule(3000);
	}

	private void setTableSelectionPopForRequestForQuotation() {

		removePopObjects();

		final RequestForQuotationForm form = new RequestForQuotationForm();
		docVerticalPanel.add(form.content);
		form.content.getElement().getStyle().setHeight(570, Unit.PX);
		form.content.getElement().getStyle().setWidth(950, Unit.PX);
		docApprovalPanel.show();
		docApprovalPanel.center();
		form.showWaitSymbol();

		Timer timer = new Timer() {
			@Override
			public void run() {

				async.getSearchResult(approvalSelectionQuerry(),
						new AsyncCallback<ArrayList<SuperModel>>() {

							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected Error occured !");
							}

							@Override
							public void onSuccess(ArrayList<SuperModel> result) {

								for (SuperModel pmodel : result) {
									RequsestForQuotation rfq = (RequsestForQuotation) pmodel;
									form.updateView(rfq);
									form.setEnable(false);
									form.getProcessLevelBar().setVisibleFalse(
											false);
								}
							}
						});

				form.hideWaitSymbol();
			}
		};
		timer.schedule(3000);
	}

	private void setTableSelectionPopForInvoiceDetails() {

		removePopObjects();

		final InvoiceDetailsForm form = new InvoiceDetailsForm();
		docVerticalPanel.add(form.content);
		form.content.getElement().getStyle().setHeight(570, Unit.PX);
		form.content.getElement().getStyle().setWidth(950, Unit.PX);
		docApprovalPanel.show();
		docApprovalPanel.center();
		form.showWaitSymbol();

		Timer timer = new Timer() {
			@Override
			public void run() {

				async.getSearchResult(approvalSelectionQuerry(),
						new AsyncCallback<ArrayList<SuperModel>>() {

							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected Error occured !");
							}

							@Override
							public void onSuccess(ArrayList<SuperModel> result) {

								for (SuperModel pmodel : result) {
									Invoice inv = (Invoice) pmodel;
									form.updateView(inv);
									form.setEnable(false);
									form.getProcessLevelBar().setVisibleFalse(
											false);
								}
							}
						});

				form.hideWaitSymbol();
			}
		};
		timer.schedule(3000);
	}

	private void setTableSelectionPopForBillingDetails() {

		removePopObjects();

		final BillingDetailsForm form = new BillingDetailsForm();
		docVerticalPanel.add(form.content);
		form.content.getElement().getStyle().setHeight(570, Unit.PX);
		form.content.getElement().getStyle().setWidth(950, Unit.PX);
		docApprovalPanel.show();
		docApprovalPanel.center();
		form.showWaitSymbol();

		Timer timer = new Timer() {
			@Override
			public void run() {

				async.getSearchResult(approvalSelectionQuerry(),
						new AsyncCallback<ArrayList<SuperModel>>() {

							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected Error occured !");
							}

							@Override
							public void onSuccess(ArrayList<SuperModel> result) {

								for (SuperModel pmodel : result) {
									BillingDocument billingdoc = (BillingDocument) pmodel;
									form.updateView(billingdoc);
									form.setEnable(false);
									form.getProcessLevelBar().setVisibleFalse(
											false);
								}
							}
						});

				form.hideWaitSymbol();
			}
		};
		timer.schedule(3000);
	}

	private void setTableSelectionPopForLetterOfIntent() {

		removePopObjects();

		final LetterOfIntentForm form = new LetterOfIntentForm();
//		LetterOfIntentPresenter presenter = new LetterOfIntentPresenter(form,
//				new LetterOfIntent());
		docVerticalPanel.add(form.content);
		form.content.getElement().getStyle().setHeight(570, Unit.PX);
		form.content.getElement().getStyle().setWidth(950, Unit.PX);
		docApprovalPanel.show();
		docApprovalPanel.center();
		form.showWaitSymbol();

		Timer timer = new Timer() {
			@Override
			public void run() {

				async.getSearchResult(approvalSelectionQuerry(),
						new AsyncCallback<ArrayList<SuperModel>>() {

							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected Error occured !");
							}

							@Override
							public void onSuccess(ArrayList<SuperModel> result) {

								for (SuperModel pmodel : result) {
									LetterOfIntent loi = (LetterOfIntent) pmodel;
									form.updateView(loi);
									form.setEnable(false);
									form.setMenuAsPerStatus();
									form.getProcessLevelBar().setVisibleFalse(
											false);
								}
							}
						});

				form.hideWaitSymbol();
			}
		};
		timer.schedule(3000);
	}

	private void setTableSelectionPopForLeaveApplication() {
		/*
		 * changes not done as required because of some leaveapplication class
		 * constructor
		 * 
		 * //
		 */
		// AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
		// AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Human Resource/Leave Application",Screen.LEAVEAPPLICATION);
		// final LeaveApplicationForm form =
		// LeaveApplicationPresenter.initalize();
		// AppMemory.getAppMemory().stickPnel(form);
		// form.showWaitSymbol();

		removePopObjects();

		final LeaveApplicationForm form = LeaveApplicationPresenter.initalize();
		docVerticalPanel.add(form.content);
		form.content.getElement().getStyle().setHeight(570, Unit.PX);
		form.content.getElement().getStyle().setWidth(950, Unit.PX);
		docApprovalPanel.show();
		docApprovalPanel.center();
		form.showWaitSymbol();

		Timer timer = new Timer() {
			@Override
			public void run() {

				async.getSearchResult(approvalSelectionQuerry(),
						new AsyncCallback<ArrayList<SuperModel>>() {

							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected Error occured !");
							}

							@Override
							public void onSuccess(ArrayList<SuperModel> result) {

								for (SuperModel pmodel : result) {
									LeaveApplication leaveapp = (LeaveApplication) pmodel;
									form.updateView(leaveapp);
									form.setToViewState();
									form.getProcessLevelBar().setVisibleFalse(
											false);
								}
							}
						});

				form.hideWaitSymbol();
			}
		};
		timer.schedule(3000);
	}

	private void setTableSelectionPopForComplain() {

		removePopObjects();

		final ComplainForm form = new ComplainForm();
		docVerticalPanel.add(form.content);
		form.content.getElement().getStyle().setHeight(570, Unit.PX);
		form.content.getElement().getStyle().setWidth(950, Unit.PX);
		docApprovalPanel.show();
		docApprovalPanel.center();
		form.showWaitSymbol();

		Timer timer = new Timer() {
			@Override
			public void run() {

				async.getSearchResult(approvalSelectionQuerry(),
						new AsyncCallback<ArrayList<SuperModel>>() {

							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected Error occured !");
							}

							@Override
							public void onSuccess(ArrayList<SuperModel> result) {

								for (SuperModel pmodel : result) {
									Complain act = (Complain) pmodel;
									 form.updateView(act);
									form.setEnable(false);
								}
							}
						});
				form.hideWaitSymbol();
			}
		};
		timer.schedule(3000);
	}

	private void setTableSelectionPopForAdvanceRequest() {

		removePopObjects();

		final AdvanceForm form = AdvancePresenter.initalize();
		docVerticalPanel.add(form.content);
		form.content.getElement().getStyle().setHeight(570, Unit.PX);
		form.content.getElement().getStyle().setWidth(950, Unit.PX);
		docApprovalPanel.show();
		docApprovalPanel.center();
		form.showWaitSymbol();

		Timer timer = new Timer() {
			@Override
			public void run() {

				async.getSearchResult(approvalSelectionQuerry(),
						new AsyncCallback<ArrayList<SuperModel>>() {

							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected Error occured !");
							}

							@Override
							public void onSuccess(ArrayList<SuperModel> result) {

								for (SuperModel pmodel : result) {
									Loan loan = (Loan) pmodel;
									form.updateView(loan);
									form.setToViewState();

								}
							}
						});

				form.hideWaitSymbol();
			}
		};
		timer.schedule(3000);
	}

	private void setTableSelectionPopForInspectionRequest() {

		removePopObjects();

		final InspectionForm form = new InspectionForm();
//		InspectionPresenter presenter = new InspectionPresenter(form,
//				new Inspection());
		docVerticalPanel.add(form.content);
		form.content.getElement().getStyle().setHeight(570, Unit.PX);
		form.content.getElement().getStyle().setWidth(950, Unit.PX);
		docApprovalPanel.show();
		docApprovalPanel.center();
		form.showWaitSymbol();

		Timer timer = new Timer() {
			@Override
			public void run() {

				async.getSearchResult(approvalSelectionQuerry(),
						new AsyncCallback<ArrayList<SuperModel>>() {

							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected Error occured !");
							}

							@Override
							public void onSuccess(ArrayList<SuperModel> result) {

								for (SuperModel pmodel : result) {
									Inspection loan = (Inspection) pmodel;
									form.updateView(loan);
									form.setEnable(false);
									form.setMenuAsPerStatus();
									form.getProcessLevelBar().setVisibleFalse(
											false);
								}
							}
						});

				form.hideWaitSymbol();
			}
		};
		timer.schedule(3000);
	}

	private void setTableSelectionPopForWorkOrderRequest() {

		removePopObjects();

		final WorkOrderForm form = new WorkOrderForm();
//		WorkOrderPresenter presenter = new WorkOrderPresenter(form,
//				new WorkOrder());
		docVerticalPanel.add(form.content);
		form.content.getElement().getStyle().setHeight(570, Unit.PX);
		form.content.getElement().getStyle().setWidth(950, Unit.PX);
		docApprovalPanel.show();
		docApprovalPanel.center();
		form.showWaitSymbol();

		Timer timer = new Timer() {
			@Override
			public void run() {

				async.getSearchResult(approvalSelectionQuerry(),
						new AsyncCallback<ArrayList<SuperModel>>() {

							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected Error occured !");
							}

							@Override
							public void onSuccess(ArrayList<SuperModel> result) {

								for (SuperModel pmodel : result) {
									WorkOrder wo = (WorkOrder) pmodel;
									form.updateView(wo);
									form.setEnable(false);
									form.setMenuAsPerStatus();
									form.getProcessLevelBar().setVisibleFalse(
											false);
								}
							}
						});

				form.hideWaitSymbol();
			}
		};
		timer.schedule(3000);
	}

	private void setTableSelectionPopForServicePoRequest() {
		removePopObjects();

		final ServicePoForm form = new ServicePoForm();
		docVerticalPanel.add(form.content);
		form.content.getElement().getStyle().setHeight(570, Unit.PX);
		form.content.getElement().getStyle().setWidth(950, Unit.PX);
		docApprovalPanel.show();
		docApprovalPanel.center();
		form.showWaitSymbol();

		Timer timer = new Timer() {
			@Override
			public void run() {
				async.getSearchResult(approvalSelectionQuerry(),
						new AsyncCallback<ArrayList<SuperModel>>() {
							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected Error occured !");
							}

							@Override
							public void onSuccess(ArrayList<SuperModel> result) {

								for (SuperModel pmodel : result) {
									ServicePo po = (ServicePo) pmodel;
									form.updateView(po);
									form.setEnable(false);
									form.getProcessLevelBar().setVisibleFalse(
											false);
								}
							}
						});
				form.hideWaitSymbol();
			}
		};
		timer.schedule(3000);
	}

	private void setTableSelectionPopForMultipleExpenseRequest() {

		removePopObjects();

		final MultipleExpensemanagmentForm form = new MultipleExpensemanagmentForm();
		docVerticalPanel.add(form.content);
		form.content.getElement().getStyle().setHeight(570, Unit.PX);
		form.content.getElement().getStyle().setWidth(950, Unit.PX);
		docApprovalPanel.show();
		docApprovalPanel.center();
		form.showWaitSymbol();

		Timer timer = new Timer() {
			@Override
			public void run() {
				async.getSearchResult(approvalSelectionQuerry(),
						new AsyncCallback<ArrayList<SuperModel>>() {
							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected Error occured !");
							}

							@Override
							public void onSuccess(ArrayList<SuperModel> result) {

								for (SuperModel pmodel : result) {
									MultipleExpenseMngt exp = (MultipleExpenseMngt) pmodel;
									form.updateView(exp);
									form.setEnable(false);
									form.getProcessLevelBar().setVisibleFalse(
											false);
								}
							}
						});
				form.hideWaitSymbol();
			}
		};
		timer.schedule(3000);
	}

	private void setTableSelectionPopForCustomerRequest() {
		removePopObjects();

		final CustomerForm form = new CustomerForm();
		docVerticalPanel.add(form.content);
		form.content.getElement().getStyle().setHeight(570, Unit.PX);
		form.content.getElement().getStyle().setWidth(950, Unit.PX);
		docApprovalPanel.show();
		docApprovalPanel.center();
		form.showWaitSymbol();

		Timer timer = new Timer() {
			@Override
			public void run() {
				async.getSearchResult(approvalSelectionQuerry(),
						new AsyncCallback<ArrayList<SuperModel>>() {
							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected Error occured !");
							}

							@Override
							public void onSuccess(ArrayList<SuperModel> result) {

								for (SuperModel pmodel : result) {
									Customer po = (Customer) pmodel;
									form.updateView(po);
									form.setEnable(false);
									form.getProcessLevelBar().setVisibleFalse(
											false);
								}
							}
						});
				form.hideWaitSymbol();
			}
		};
		timer.schedule(3000);
	}

	private void setTableSelectionPopForcampainMgnt() {
		removePopObjects();
		// CustomerTrainingPresenter
		final CustomerTrainingForm form = new CustomerTrainingForm();
		docVerticalPanel.add(form.content);
		form.content.getElement().getStyle().setHeight(570, Unit.PX);
		form.content.getElement().getStyle().setWidth(950, Unit.PX);
		docApprovalPanel.show();
		docApprovalPanel.center();
		form.showWaitSymbol();

		Timer timer = new Timer() {
			@Override
			public void run() {
				async.getSearchResult(approvalSelectionQuerry(),
						new AsyncCallback<ArrayList<SuperModel>>() {
							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected Error occured !");
							}

							@Override
							public void onSuccess(ArrayList<SuperModel> result) {

								for (SuperModel pmodel : result) {
									CustomerTrainingDetails po = (CustomerTrainingDetails) pmodel;
									form.updateView(po);
									form.setEnable(false);
									form.getProcessLevelBar().setVisibleFalse(false);
								}
							}
						});
				form.hideWaitSymbol();
			}
		};
		timer.schedule(3000);
	}

	private void setTableSelectionPopForTimeReport() {
		removePopObjects();
		// CustomerTrainingPresenter
		final TimeReportForm form = new TimeReportForm();
		docVerticalPanel.add(form.content);
	
		form.content.getElement().getStyle().setHeight(570, Unit.PX);
		form.content.getElement().getStyle().setWidth(950, Unit.PX);
		docApprovalPanel.show();
		docApprovalPanel.center();
		form.showWaitSymbol();

		Timer timer = new Timer() {
			@Override
			public void run() {
				async.getSearchResult(approvalSelectionQuerry(),
						new AsyncCallback<ArrayList<SuperModel>>() {
							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected Error occured !");
							}

							@Override
							public void onSuccess(ArrayList<SuperModel> result) {

								for (SuperModel pmodel : result) {
									
									final TimeReport po = (TimeReport) pmodel;
									final TimeReportPresenter pre = new TimeReportPresenter(form, po);
									Timer tim = new Timer() {
										
										@Override
										public void run() {
											form.updateView(po);
											form.setEnable(false);
											form.getProcessLevelBar().setVisibleFalse(false);
										}
									};
									tim.schedule(2000);
								}
							}
						});
				form.hideWaitSymbol();
			}
		};
		timer.schedule(3000);
	}

	
	/*
	 * end pop methods
	 */
	private void setTableSelectionForCNC() {
//		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
//	   	AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/CNC",Screen.CNC);
//	   	final CNCForm form = CNCPresenter.initalize();
//	   	AppMemory.getAppMemory().stickPnel(form);
//	   	form.showWaitSymbol();
//		Timer timer=new Timer() 
//	   	{
//			@Override
//			public void run() {
//				async.getSearchResult(approvalSelectionQuerry(),new AsyncCallback<ArrayList<SuperModel>>() {
//					@Override
//					public void onFailure(Throwable caught) {
//						form.showDialogMessage("An Unexpected Error occured !");
//					}
//					@Override
//					public void onSuccess(ArrayList<SuperModel> result) {
//						
//						
//						for(SuperModel pmodel:result)
//						{
//							CNC cnc =(CNC)pmodel;
//							form.updateView(cnc);
//							form.setToViewState();
//						}
//					}
//				});
//				form.hideWaitSymbol();
//			}
//		};
//        timer.schedule(3000); 

		// TODO Auto-generated method stub
		removePopObjects();

		final CNCForm form = new CNCForm();
		docVerticalPanel.add(form.content);
		form.content.getElement().getStyle().setHeight(570, Unit.PX);
		form.content.getElement().getStyle().setWidth(950, Unit.PX);
		docApprovalPanel.show();
		docApprovalPanel.center();
		form.showWaitSymbol();

		Timer timer = new Timer() {
			@Override
			public void run() {
				async.getSearchResult(approvalSelectionQuerry(),new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onFailure(Throwable caught) {
						form.showDialogMessage("An Unexpected Error occured !");
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {

						for (SuperModel pmodel : result) {
							CNC cnc = (CNC) pmodel;
							form.updateView(cnc);
							form.setEnable(false);
							form.getProcessLevelBar().setVisibleFalse(false);
						}
					}
				});
				form.hideWaitSymbol();
			}
		};
		timer.schedule(3000);
	
	}

	/** date 08.08.2018 added by komal for cost sheet download**/
	private void reactOnCNCDownload() {
		final XlsxServiceAsync xlxWriter = GWT.create(XlsxService.class);		
		form.showWaitSymbol();
		async.getSearchResult(approvalSelectionQuerry(), new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				form.hideWaitSymbol();
				CNC cnc=(CNC) result.get(0);
				xlxWriter.setCostSheetData(cnc, new AsyncCallback<Void>() {

					@Override
					public void onSuccess(Void result) {
						// TODO Auto-generated method stub
						String gwt = com.google.gwt.core.client.GWT
								.getModuleBaseURL();
						final String url = gwt + "CreateXLXSServlet" + "?type=" + 3;
						Window.open(url, "test", "enabled");
					}

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub

					}
				});

				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
			}
		});
		
	
	}
	
	private void setPhysicalInventoryMaintence() {
		removePopObjects();
		final WareHouseDetailsForm form = new WareHouseDetailsForm();
		docVerticalPanel.add(form.content);
		form.content.getElement().getStyle().setHeight(570, Unit.PX);
		form.content.getElement().getStyle().setWidth(950, Unit.PX);
		docApprovalPanel.show();
		docApprovalPanel.center();
		form.showWaitSymbol();

		Timer timer = new Timer() {
			@Override
			public void run() {
				async.getSearchResult(approvalSelectionQuerry(),
						new AsyncCallback<ArrayList<SuperModel>>() {
							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected Error occured !");
							}
							@Override
							public void onSuccess(ArrayList<SuperModel> result) {
								for (SuperModel pmodel : result) {
									final PhysicalInventoryMaintaince po = (PhysicalInventoryMaintaince) pmodel;
									final WareHouseDetailsPresenter pre = new WareHouseDetailsPresenter(form, po);
									Timer tim = new Timer() {
										@Override
										public void run() {
											form.updateView(po);
											form.setEnable(false);
											form.getProcessLevelBar().setVisibleFalse(false);
										}
									};
									tim.schedule(2000);
								}
							}
						});
				form.hideWaitSymbol();
			}
		};
		timer.schedule(3000);
	}
	
	public static void globalMakeLiveMultiLevelApproval() {
		Company c=new Company();
		String processMultiLevel=AppConstants.GLOBALRETRIEVALMULTILEVELAPPROVAL+"-"+c.getCompanyId();
		DropDownServiceAsync dpservice=GWT.create(DropDownService.class);
		dpservice.getDropDown(processMultiLevel,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				caught.printStackTrace();
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				LoginPresenter.globalMultilevelApproval.clear();
				for (SuperModel model : result) {
					MultilevelApproval multiLevelEntity = (MultilevelApproval) model;
					LoginPresenter.globalMultilevelApproval.add(multiLevelEntity);
				}
				Console.log("Multi Level Size"+ LoginPresenter.globalMultilevelApproval.size());
			}
		});
	}
}

