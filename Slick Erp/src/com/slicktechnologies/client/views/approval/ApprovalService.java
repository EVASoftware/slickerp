package com.slicktechnologies.client.views.approval;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.common.businessprocesslayer.ApprovableProcess;

@RemoteServiceRelativePath("approvalservice")
public interface ApprovalService extends RemoteService 
{
	/**
	 * Date : 26-02-2017 By Anil
	 * Old Code for approval
	 */
//   public void sendApproveRequest(Approvals model);
	
	/**
	 * Date : 26 -02-2017 By Anil
	 * Changing return type of method form void to String
	 **/
	 public String sendApproveRequest(Approvals model);
	 
   public void cancelDocument(Approvals model);

}
