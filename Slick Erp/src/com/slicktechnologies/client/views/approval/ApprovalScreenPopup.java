package com.slicktechnologies.client.views.approval;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCGlassPanel;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.DropDownService;
import com.slicktechnologies.client.services.DropDownServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.common.businessprocesslayer.LetterOfIntent;
import com.slicktechnologies.shared.common.businessprocesslayer.MultipleExpenseMngt;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessprocesslayer.RequsestForQuotation;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.multilevelapproval.MultilevelApproval;
import com.slicktechnologies.shared.common.multilevelapproval.MultilevelApprovalDetails;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesorder.SalesQuotation;


public class ApprovalScreenPopup extends AbsolutePanel {
	
	GenricServiceAsync async=GWT.create(GenricService.class);
	
	TextBox businessprocesstypeCol;
	TextBox businessprocessidCol;
	TextArea remark;
	Button btnCancel,btnOk;
	InlineLabel msgapprovalname; 
	InlineLabel approvalname;
	ListBox approvalName;
	
	
	protected GWTCGlassPanel glassPanel;
	
		public ApprovalScreenPopup() {
			
			InlineLabel remark1 = new InlineLabel("Remark");
			add(remark1,160,10);
			remark1.getElement().getStyle().setFontSize(20, Unit.PX);
			
			InlineLabel processType = new InlineLabel("Doc Type");
			add(processType,10,50);
			businessprocesstypeCol= new TextBox();
			add(businessprocesstypeCol,10,70);
			
			businessprocesstypeCol.setSize("90px", "18px");
			businessprocesstypeCol.setEnabled(false);
			
			InlineLabel processId = new InlineLabel("Doc Id");
			add(processId,150,50);
			businessprocessidCol= new TextBox();
			add(businessprocessidCol,150,70);
			
			businessprocessidCol.setSize("90px", "18px");
			businessprocessidCol.setEnabled(false);
			
			
			InlineLabel Remark = new InlineLabel("Remark");
			add(Remark,10,110);
			remark= new TextArea();
			add(remark,10,140);
			remark.setSize("380px", "40px");
			
			btnCancel= new Button("Cancel");
			btnOk= new Button("Ok");
			add(btnOk,120,210);
			add(btnCancel,220,210);
			btnOk.getElement().getStyle().setWidth(60, Unit.PX);
			btnCancel.getElement().getStyle().setWidth(60, Unit.PX);

			if(validateApproval()==true){
				approvalname = new InlineLabel("Next Level Approver");
				add(approvalname,280,50);
				approvalName= new ListBox();
				add(approvalName,280,70);
				approvalName.setSize("116px", "28px");
				
				
				msgapprovalname = new InlineLabel("* Multilevel Approval is active. Please select approver name!");
				add(msgapprovalname,20,260);
				msgapprovalname.getElement().addClassName("redtext");
				
			}
			
			
			
			setSize("420px", "290px");
			this.getElement().setId("form");
		}

		
	// this is added by vijay for no multilevel approver for reject btn
		
		public ApprovalScreenPopup(String value) {
			
			
			InlineLabel remark1 = new InlineLabel("Remark");
			add(remark1,160,10);
			remark1.getElement().getStyle().setFontSize(20, Unit.PX);
			
			InlineLabel processType = new InlineLabel("Doc Type");
			add(processType,10,50);
			businessprocesstypeCol= new TextBox();
			add(businessprocesstypeCol,10,70);
			
			businessprocesstypeCol.setSize("90px", "18px");
			businessprocesstypeCol.setEnabled(false);
			
			InlineLabel processId = new InlineLabel("Doc Id");
			add(processId,150,50);
			businessprocessidCol= new TextBox();
			add(businessprocessidCol,150,70);
			
			businessprocessidCol.setSize("90px", "18px");
			businessprocessidCol.setEnabled(false);
			
			
			InlineLabel Remark = new InlineLabel("Remark");
			add(Remark,10,110);
			remark= new TextArea();
			add(remark,10,140);
			remark.setSize("380px", "40px");
			
			btnCancel= new Button("Cancel");
			btnOk= new Button("Ok");
			add(btnOk,120,210);
			add(btnCancel,220,210);
			btnOk.getElement().getStyle().setWidth(60, Unit.PX);
			btnCancel.getElement().getStyle().setWidth(60, Unit.PX);
			
			
			setSize("420px", "290px");
			this.getElement().setId("form");
		}

		/****************getter and setters*****************/
		
		public TextBox getBusinessprocesstypeCol() {
			return businessprocesstypeCol;
		}

		public void setBusinessprocesstypeCol(TextBox businessprocesstypeCol) {
			this.businessprocesstypeCol = businessprocesstypeCol;
		}

		public TextBox getBusinessprocessidCol() {
			return businessprocessidCol;
		}

		public void setBusinessprocessidCol(TextBox businessprocessidCol) {
			this.businessprocessidCol = businessprocessidCol;
		}

		public TextArea getRemark() {
			return remark;
		}

		public void setRemark(TextArea remark) {
			this.remark = remark;
		}

		public Button getBtnCancel() {
			return btnCancel;
		}

		public void setBtnCancel(Button btnCancel) {
			this.btnCancel = btnCancel;
		}

		public Button getBtnOk() {
			return btnOk;
		}

		public void setBtnOk(Button btnOk) {
			this.btnOk = btnOk;
		}


		public ListBox getApprovalName() {
			return approvalName;
		}


		public void setApprovalName(ListBox approvalName) {
			this.approvalName = approvalName;
		}
		
		
		
		public InlineLabel getMsgapprovalname() {
			return msgapprovalname;
		}


		public void setMsgapprovalname(InlineLabel msgapprovalname) {
			this.msgapprovalname = msgapprovalname;
		}

		
		
		

		public InlineLabel getApprovalname() {
			return approvalname;
		}


		public void setApprovalname(InlineLabel approvalname) {
			this.approvalname = approvalname;
		}


		public boolean validateApproval(){
			
			ArrayList<ProcessName> processNme=LoginPresenter.globalProcessName;
			boolean flagApproval=false;
			for(int b=0;b<processNme.size();b++)
			{
				if(processNme.get(b).getProcessName().trim().equals(AppConstants.GLOBALRETRIEVALMULTILEVELAPPROVAL)&&processNme.get(b).isStatus()==true)
				{
					flagApproval=true;
				}
			}
			
			return flagApproval;
			
		}
		
		/**
		 * Date : 16-05-2017 BY ANIL
		 * @param level
		 * @param docType
		 * @param amount
		 */
		public void initializeAmountLvlApprover(int level,String docType,Double amount)
		{
			Console.log("4-"+docType+" Id "+businessprocessidCol.getValue()+"Total Amount "+amount+" Level "+level);
			
			System.out.println(docType+" Id "+businessprocessidCol.getValue()+"Total Amount "+amount);
			final ArrayList<MultilevelApproval> approvalList=LoginPresenter.globalMultilevelApproval;
			for(int y=0;y<approvalList.size();y++)
			{
				if(approvalList.get(y).getDocumentType().trim().equalsIgnoreCase(docType.trim()))
				{
					setApprovalDropDown(approvalList.get(y).getApprovalLevelDetails(),(level+1),amount);
				}
			}
		}
		/**
		 * End
		 */
		
		public void initializeApprover(final int level,final String docType)
		{
			Console.log("2-Approver initialization "+docType);
			//  old code 
//			ArrayList<MultilevelApproval> approvalList=LoginPresenter.globalMultilevelApproval;
//			
//			System.out.println("level 1=== "+level);
//			
//			for(int y=0;y<approvalList.size();y++)
//			{
//				if(approvalList.get(y).getDocumentType().trim().equalsIgnoreCase(docType.trim()))
//				{
//					setApprovalDropDown(approvalList.get(y).getApprovalLevelDetails(),(level+1));
//				}
//			}
			
			/*new code by anil for multilevel & Amt level Approval Date : 8-9-2016*/
			final ArrayList<MultilevelApproval> approvalList=LoginPresenter.globalMultilevelApproval;
			System.out.println("level 1=== "+level);
			
			if(getValidApprovalDocument()){
				System.out.println("INSID VALID APPROVAL ---- true " );
				Console.log("3-Doc with Amount Level "+docType);
				showWaitSymbol();
				async.getSearchResult(getDocSearchQuerry(), new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						hideWaitSymbol();
						if(result.size()!=0){
							Double amount=0d;
							for(SuperModel model:result){
								if(model instanceof Contract){
									System.out.println("Contract ");
									Contract entity=(Contract) model;
									amount=entity.getNetpayable();
									
									System.out.println("Total Amount "+amount);
									
								}else if(model instanceof Quotation){
									Quotation entity=(Quotation) model;
									amount=entity.getNetpayable();
								}else if(model instanceof SalesQuotation){
									SalesQuotation entity=(SalesQuotation) model;
									amount=entity.getNetpayable();
								}else if(model instanceof SalesOrder){
									SalesOrder entity=(SalesOrder) model;
									amount=entity.getNetpayable();
								}else if(model instanceof RequsestForQuotation){
									RequsestForQuotation entity=(RequsestForQuotation) model;
									for(ProductDetails prodDet:entity.getProductinfo()){
										amount=amount+prodDet.getProdPrice();
									}
								}else if(model instanceof LetterOfIntent){
									LetterOfIntent entity=(LetterOfIntent) model;
									for(ProductDetails prodDet:entity.getProductinfo()){
										amount=amount+prodDet.getProdPrice();
									}
								}else if(model instanceof PurchaseOrder){
									PurchaseOrder entity=(PurchaseOrder) model;
									amount=entity.getNetpayble();
								}
							}
							Console.log("4-"+docType+" Id "+businessprocessidCol.getValue()+"Total Amount "+amount);
							
							System.out.println(docType+" Id "+businessprocessidCol.getValue()+"Total Amount "+amount);
							for(int y=0;y<approvalList.size();y++)
							{
								if(approvalList.get(y).getDocumentType().trim().equalsIgnoreCase(docType.trim()))
								{
									setApprovalDropDown(approvalList.get(y).getApprovalLevelDetails(),(level+1),amount);
								}
							}
							
						}
					}
					@Override
					public void onFailure(Throwable caught) {
						hideWaitSymbol();
					}
				});
			
			}else{
				
				for(int y=0;y<approvalList.size();y++)
				{
					if(approvalList.get(y).getDocumentType().trim().equalsIgnoreCase(docType.trim()))
					{
						setApprovalDropDown(approvalList.get(y).getApprovalLevelDetails(),(level+1));
					}
				}
			
			}
			
		}

		public void setApprovalDropDown(List<MultilevelApprovalDetails> levelApproval,int levelVal,Double amount)
		{
			

			/**
			 * Date : 15-02-2017 By Anil
			 * Updating Multilevel approval code for amount validation
			 */
			System.out.println("Level 3=== "+levelVal);
			System.out.println("Amount == "+amount+" app lis siz "+levelApproval.size());
			
			Console.log("5-Amount Level Approval Document "+amount+" "+levelApproval.size()+" level "+levelVal);
			approvalName.addItem("SELECT");
			
			/**
			 * Date : 16-05-2017 By ANIL
			 */
			for(MultilevelApprovalDetails obj:levelApproval){
				if(obj.getLevel().equals(levelVal+"")){
					Console.log("5A- Approval Document "+obj.getLevel()+" / "+obj.getEmployeeName());
					if(getRoleEmployees(obj.getEmployeeName())!=null){
						Console.log("5B-"+getRoleEmployees(obj.getEmployeeName()));
						approvalName.addItem(getRoleEmployees(obj.getEmployeeName()));
					}
				}
			}
			/**
			 * End
			 */
			
			
			/**
			 * Date : 16-05-2017 By Anil
			 * Commented old code of amount level approval 
			 */
			/////////////////////////////////////////////////////////
			
//			boolean amountLevelFlag=false;
//			ArrayList<MultilevelApprovalDetails> amountLevelApproverList=new ArrayList<MultilevelApprovalDetails>();
//			for(MultilevelApprovalDetails obj:levelApproval){
//				if(obj.getLevel().equals(levelVal+"")&&obj.getAmountUpto()!=null){
//					amountLevelFlag=true;
//					amountLevelApproverList.add(obj);
//				}
//			}
//			if(!amountLevelFlag){
//				Console.log("6-Amount Level Status False");
//				
//				for(MultilevelApprovalDetails obj:levelApproval){
//					if(obj.getLevel().equals(levelVal+"")){
//						if(getRoleEmployees(obj.getEmployeeName())!=null){
//							approvalName.addItem(getRoleEmployees(obj.getEmployeeName()));
//						}
//					}
//				}
//			}
//			
//			if(amountLevelFlag){
//				Console.log("7-Amount Level Status True "+amountLevelApproverList.size()+"  "+amount);
//				
//				/**
//				 * Date : 28-02-2017 By Anil
//				 * Removing other branches approvers from approvers list 
//				 */
//				if(LoginPresenter.branchRestrictionFlag==true
//						&&!UserConfiguration.getRole().getRoleName().equalsIgnoreCase("ADMIN")){
//					
//					for(int i=0;i<amountLevelApproverList.size();i++){
//						boolean empFoundFlag=false;
//						for(Employee emp:LoginPresenter.globalEmployee){
//							if(emp.getFullname().equals(amountLevelApproverList.get(i).getEmployeeName())){
//								empFoundFlag=true;
//							}
//						}
//						if(!empFoundFlag){
//							amountLevelApproverList.remove(i);
//							i--;
//						}
//					}
//				}
//				Console.log("7 A-Branch Restricted Approvers list  "+amountLevelApproverList.size()+"  "+amount);
//				/**
//				 * End
//				 */
//				Comparator<MultilevelApprovalDetails> approvalComparator = new Comparator<MultilevelApprovalDetails>() {
//					public int compare(MultilevelApprovalDetails c1, MultilevelApprovalDetails c2) {
//						Double amount1=c1.getAmountUpto();
//						Double amount2=c2.getAmountUpto();
//						return amount1.compareTo(amount2);
//					}
//				};
//				Collections.sort(amountLevelApproverList,approvalComparator);
//				
//				Double maxAmount = null;
//				for(int i=0;i<amountLevelApproverList.size();i++){
//					Console.log("7 B-  "+amountLevelApproverList.get(i).getEmployeeName()+" "+amountLevelApproverList.get(i).getAmountUpto()+"  DOC AMT: "+amount);
//					if(LoginPresenter.branchRestrictionFlag==true
//							&&!UserConfiguration.getRole().getRoleName().equalsIgnoreCase("ADMIN")){
//						if(maxAmount!=null){
//							if(amountLevelApproverList.get(i).getAmountUpto()>=amount&&amountLevelApproverList.get(i).getAmountUpto()<=maxAmount){
//								maxAmount=amountLevelApproverList.get(i).getAmountUpto();
//								Console.log("7 F-  "+amountLevelApproverList.get(i).getEmployeeName()+" "+amountLevelApproverList.get(i).getAmountUpto()+"  DOC AMT: "+amount);
//								if(getRoleEmployees(amountLevelApproverList.get(i).getEmployeeName())!=null){
//									approvalName.addItem(getRoleEmployees(amountLevelApproverList.get(i).getEmployeeName()));
//								}
//							}
//						}else{
//							if(amountLevelApproverList.get(i).getAmountUpto()>=amount){
//								Console.log("7 C-  "+amountLevelApproverList.get(i).getEmployeeName()+" "+amountLevelApproverList.get(i).getAmountUpto()+"  DOC AMT: "+amount);
//								maxAmount=amountLevelApproverList.get(i).getAmountUpto();
//								if(getRoleEmployees(amountLevelApproverList.get(i).getEmployeeName())!=null){
//									Console.log("7 D-  "+amountLevelApproverList.get(i).getEmployeeName()+" "+amountLevelApproverList.get(i).getAmountUpto()+"  DOC AMT: "+amount);
//									approvalName.addItem(getRoleEmployees(amountLevelApproverList.get(i).getEmployeeName()));
//								}
//							}
//						}
//					}else{
//						if(amountLevelApproverList.get(i).getAmountUpto()>=amount){
//							if(getRoleEmployees(amountLevelApproverList.get(i).getEmployeeName())!=null){
//								approvalName.addItem(getRoleEmployees(amountLevelApproverList.get(i).getEmployeeName()));
//							}
//						}
//					}
//				}
//			}
			
			
			
			/**
			 * End
			 */
			
			/////////////////////////////////////
			
//			for(MultilevelApprovalDetails mulAppDet:levelApproval){
//				if(Integer.parseInt(mulAppDet.getLevel())==levelVal){
//					System.out.println(" in loop ");
//					if(amount!=null){
//						if(mulAppDet.getAmountUpto()!=null){
//							System.out.println("app emp "+mulAppDet.getEmployeeName()+" amount "+mulAppDet.getAmountUpto());
//							if(mulAppDet.getAmountUpto()>=amount){
//								approvalName.addItem(getRoleEmployees(mulAppDet.getEmployeeName()));
//							}
//						}else{
//							approvalName.addItem(getRoleEmployees(mulAppDet.getEmployeeName()));
//						}
//					}else{
//						approvalName.addItem(getRoleEmployees(mulAppDet.getEmployeeName()));
//					}
//				}
//			}
			
			///////////////////////////////////
		}
		
		
		////
		
		public void setApprovalDropDown(List<MultilevelApprovalDetails> levelApproval,int levelVal)
		{
			System.out.println("Level 2=== "+levelVal);
			

			
			approvalName.addItem("SELECT");
			for(MultilevelApprovalDetails mulAppDet:levelApproval){
				if(Integer.parseInt(mulAppDet.getLevel())==levelVal){
					if(getRoleEmployees(getRoleEmployees(mulAppDet.getEmployeeName()))!=null){
						approvalName.addItem(getRoleEmployees(getRoleEmployees(mulAppDet.getEmployeeName())));
					}
				}
			}
			
//			final DropDownServiceAsync async=GWT.create(DropDownService.class);
//			ArrayList<MultilevelApprovalDetails> arrOfApproval=new ArrayList<MultilevelApprovalDetails>();
//			arrOfApproval.addAll(levelApproval);
//			approvalName.addItem("SELECT");
//			Company c=new Company();
//				async.getApprovalDropDown(c.getCompanyId(),arrOfApproval,levelVal,new AsyncCallback<ArrayList<String>>() {
//
//						@Override
//						public void onFailure(Throwable caught) {
//							
//						}
//						@SuppressWarnings("unchecked")
//						@Override
//						public void onSuccess(ArrayList<String> result) {
//							
//							System.out.println("size( of result) =="+result.size());
//							
//							for(int g=0;g<result.size();g++)
//							{
//								approvalName.addItem(getRoleEmployees(result.get(g)));
//							}
//						}
//					});
					 
		}
		
		
		
		public String getRoleEmployees(String empName)
		{
			String emp=null;
			for(int r=0;r<LoginPresenter.globalEmployee.size();r++)
			{
				if(LoginPresenter.globalEmployee.get(r).getFullname().trim().equals(empName))
				{
					emp=LoginPresenter.globalEmployee.get(r).getFullname();
				}
			}
			
			return emp;
		}
		

		public MyQuerry getDocSearchQuerry(){
			MyQuerry querry=new MyQuerry();
			Vector<Filter> temp=new Vector<Filter>();
			Filter filter=null;
			
			filter=new Filter();
			filter.setQuerryString("companyId");
			filter.setLongValue(UserConfiguration.getCompanyId());
			temp.add(filter);
			
			filter=new Filter();
			filter.setQuerryString("count");
			filter.setIntValue(Integer.parseInt(businessprocessidCol.getValue()));
			temp.add(filter);
			
			querry.setFilters(temp);
			
			String docType=businessprocesstypeCol.getValue();
			
			if(docType.equals(AppConstants.APPROVALCONTRACT)){
				querry.setQuerryObject(new Contract());
			}else if(docType.equals(AppConstants.APPROVALQUOTATION)){
				querry.setQuerryObject(new Quotation());
			}else if(docType.equals(AppConstants.APPROVALSALESQUOTATION)){
				querry.setQuerryObject(new SalesQuotation());
			}else if(docType.equals(AppConstants.APPROVALSALESORDER)){
				querry.setQuerryObject(new SalesOrder());
			}else if(docType.equals(AppConstants.APPROVALPURCHASEORDER)){
				querry.setQuerryObject(new PurchaseOrder());
			}else if(docType.equals(AppConstants.APPROVALREQUESTFORQUOTATION)){
				querry.setQuerryObject(new RequsestForQuotation());
			}else if(docType.equals(AppConstants.APPROVALLETTEROFINTENT)){
				querry.setQuerryObject(new LetterOfIntent());
			}else if(docType.equals(AppConstants.APPROVALEXPENSEMANAGMENT)){
				querry.setQuerryObject(new MultipleExpenseMngt());
			}
			
			
			return querry;
		}
		
		/**
		 * Comment: This method checks those document whose net payable amount is checked for multilevel approval
		 * Date : 15-02-2017 By Anil
		 */
		public boolean getValidApprovalDocument(){
			String docType=businessprocesstypeCol.getValue();
			
			if(docType.equals(AppConstants.APPROVALCONTRACT)){
				return true;
			}else if(docType.equals(AppConstants.APPROVALQUOTATION)){
				return true;
			}else if(docType.equals(AppConstants.APPROVALSALESQUOTATION)){
				return true;
			}else if(docType.equals(AppConstants.APPROVALSALESORDER)){
				return true;
			}else if(docType.equals(AppConstants.APPROVALPURCHASEORDER)){
				return true;
			}else if(docType.equals(AppConstants.APPROVALREQUESTFORQUOTATION)){
				return true;
			}else if(docType.equals(AppConstants.APPROVALLETTEROFINTENT)){
				return true;
			}else if(docType.equals(AppConstants.APPROVALEXPENSEMANAGMENT)){
				return true;
			}
			
			return false;
		}

		public void showWaitSymbol() {
			glassPanel = new GWTCGlassPanel();
			glassPanel.show();
			
			
		}
		public void hideWaitSymbol() {
		   glassPanel.hide();
			
		}
		
		
}
