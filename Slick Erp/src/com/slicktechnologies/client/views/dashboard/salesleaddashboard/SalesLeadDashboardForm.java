package com.slicktechnologies.client.views.dashboard.salesleaddashboard;
/**
 * Author: Viraj
 * Date: 15-04-2019
 * Description: To create lead dashboard in sales
 */
import java.util.ArrayList;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FlexForm;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.ViewContainer;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.DataSourceField;
import com.smartgwt.client.data.fields.DataSourceDateField;
import com.smartgwt.client.data.fields.DataSourceIntegerField;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.AutoFitWidthApproach;
import com.smartgwt.client.types.Autofit;
import com.smartgwt.client.types.FieldType;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class SalesLeadDashboardForm extends ViewContainer{
	ListGrid grid;
	DataSource ds;
	
	ObjectListBox<Branch> olbbranch=new ObjectListBox<Branch>();
	ObjectListBox<Employee> olbsalesPerson = new ObjectListBox<Employee>();
	DateBox dbFromDate;
	DateBox dbToDate;
	Button btnGo;
	
	FlexForm form;
	/**
	 * Updated By: Viraj
	 * Date: 11-01-2019
	 * Description: To add Status filter in lead dashboard
	 */
	ObjectListBox<ConfigCategory> olbLeadStatus;
	/** Ends **/
	
	HorizontalPanel horizontalpanel;
	VerticalPanel verticalpanel;
	
	public SalesLeadDashboardForm() {
		super();
		createGui();
	}
	
	@Override
	protected void createGui() {
		System.out.println("CREATE GUI");
		toggleAppHeaderBarMenu();
		
		olbbranch =new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbbranch);
		olbsalesPerson =new ObjectListBox<Employee>();
		AppUtility.makeSalesPersonListBoxLive(olbsalesPerson);
		
		/**
		 * Updated By: Viraj
		 * Date: 11-01-2019
		 * Description: To add Status filter in lead dashboard
		 */
		olbLeadStatus= new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveConfig(olbLeadStatus,Screen.LEADSTATUS);
		olbLeadStatus.getElement().getStyle().setHeight(26, Unit.PX);
		/** Ends **/
		
		olbbranch.getElement().getStyle().setHeight(26, Unit.PX);
		olbsalesPerson.getElement().getStyle().setHeight(26, Unit.PX);
		
		dbFromDate=new DateBoxWithYearSelector();
		dbToDate=new DateBoxWithYearSelector();
		DateTimeFormat dateFormat = DateTimeFormat.getFormat("dd-MM-yyyy");
		dbFromDate.setFormat(new DateBox.DefaultFormat(dateFormat));
		dbToDate.setFormat(new DateBox.DefaultFormat(dateFormat));
		btnGo=new Button("GO");
		
		
		horizontalpanel = new HorizontalPanel();
		
		
		VerticalPanel vpanel1=new VerticalPanel();
		VerticalPanel vpanel2=new VerticalPanel();
		VerticalPanel vpanel3=new VerticalPanel();
		VerticalPanel vpanel4=new VerticalPanel();
		VerticalPanel vpanel5=new VerticalPanel();
		/**
		 * Updated By: Viraj
		 * Date: 11-01-2019
		 * Description: To add Status filter in lead dashboard
		 */
		VerticalPanel vpanel6=new VerticalPanel();
		/** Ends **/
		
		InlineLabel lablel1=new InlineLabel("Branch");
		InlineLabel lablel2=new InlineLabel("Sales Person");
		InlineLabel lablel3=new InlineLabel("From Date");
		InlineLabel lablel4=new InlineLabel("To Date");
		/**
		 * Updated By: Viraj
		 * Date: 11-01-2019
		 * Description: To add Status filter in lead dashboard
		 */
		InlineLabel lable16=new InlineLabel("Status");
		/** Ends **/
		
		InlineLabel lablel5=new InlineLabel(". ");
		
		vpanel1.add(lablel1);
		vpanel1.add(olbbranch);
		
		vpanel2.add(lablel2);
		vpanel2.add(olbsalesPerson);
		
		vpanel3.add(lablel3);
		vpanel3.add(dbFromDate);
		
		vpanel4.add(lablel4);
		vpanel4.add(dbToDate);
		/**
		 * Updated By: Viraj
		 * Date: 11-01-2019
		 * Description: To add Status filter in lead dashboard
		 */
		vpanel6.add(lable16);
		vpanel6.add(olbLeadStatus);
		/** Ends **/
		
		vpanel5.add(lablel5);
		vpanel5.add(btnGo);
		
		horizontalpanel.add(vpanel1);
		horizontalpanel.add(vpanel2);
		horizontalpanel.add(vpanel3);
		horizontalpanel.add(vpanel4);
		/**
		 * Updated By: Viraj
		 * Date: 11-01-2019
		 * Description: To add Status filter in lead dashboard
		 */
		horizontalpanel.add(vpanel6);
		/** Ends **/
		horizontalpanel.add(vpanel5);

//		horizontalpanel.add(olbbranch);
//		horizontalpanel.add(olbsalesPerson);
//		horizontalpanel.add(dbFromDate);
//		horizontalpanel.add(dbToDate);
//		horizontalpanel.add(btnGo);
		horizontalpanel.setSpacing(10);
		horizontalpanel.getElement().addClassName("technicianscheduleButtons");
		
		
		verticalpanel = new VerticalPanel();
				
		
		grid = new ListGrid();
		grid.setStyleName("GridList");
		setGridProperty(grid);
		grid.setFields(getGridFields());
		
		ds= new DataSource();
		ds.setClientOnly(true);
		setDataSourceField(ds);
		grid.setDataSource(ds);
		
		verticalpanel.getElement().addClassName("varticalGridPanel");
		grid.getElement().addClassName("technicianSchedule");
		verticalpanel.add(grid);
		
		content.add(horizontalpanel);
		content.add(verticalpanel);
		
//		builder = new FormFieldBuilder("",grid);
//		FormField fgrid= builder.setMandatory(false).setRowSpan(1).setColSpan(5).build();
//		
//		
//		FormField[][] fields=new FormField[][]{
////				{folbbranch,folbsalesPerson,fdbFromDate,fdbToDate,fbtnGo},
//				{fgrid}
//		};
//		form=new FlexForm(fields,FormStyle.ROWFORM);
//		
////		content.add(grid);
//		content.add(form);
		
	}
	
	public void setDataToListGrid(ArrayList<Lead> list){
		System.out.println("FORM SETTING DATA....");
		
		ds.setTestData(getGridData(list));
		grid.setDataSource(ds);
		grid.setFields(getGridFields());
		grid.fetchData();
		
//		grid.redraw();
	}

	
	
	//grid property
	private static void setGridProperty(ListGrid grid) {
	   grid.setWidth("83%");
	   grid.setHeight("70%");
	   grid.setAutoFetchData(true);
	   grid.setAlternateRecordStyles(true);
	   grid.setShowAllRecords(true);
	   grid.setShowFilterEditor(true);
	   grid.setFilterOnKeypress(true);
	   
	   grid.setWrapCells(true);	
	   grid.setFixedRecordHeights(false);
	   grid.setAutoFitData(Autofit.HORIZONTAL);	
	   grid.setAutoFitWidthApproach(AutoFitWidthApproach.BOTH);	
	   grid.setCanResizeFields(true);
	}
	
	//fields
	private static ListGridField[] getGridFields() {
//		 DateTimeFormat dateFormatter = DateTimeFormat.getFormat("dd-MM-yyyy");
//		   DateUtil.setDefaultDisplayTimezone("+05:30");
//	       DateUtil.setShortDatetimeDisplayFormatter(dateFormatter.format(date));
////	       DateUtil.setDateParser(dateString -> {SC.say("Not called"); return null;});
//		   field14.setDateFormatter(DateDisplayFormat.TOLOCALESTRING);
		
	   ListGridField field1 = new ListGridField("leadId","Lead Id");
	   field1.setWrap(true);
	   field1.setAlign(Alignment.CENTER);
	   
	   ListGridField field2 = new ListGridField("followupButton","Follow Up");
	   field2.setWrap(true);
	   field2.setAlign(Alignment.CENTER);
	   field2.setType(ListGridFieldType.IMAGE);
	   field2.setDefaultValue("followup.png");
	   field2.setCanEdit(false);
	   field2.setImageSize(80);
	   field2.setImageHeight(30);
	   
	   ListGridField field14 = new ListGridField("followupDate","Follow Up Date");
	   field14.setWrap(true);
	   field14.setAlign(Alignment.CENTER);
	   
	   ListGridField field3 = new ListGridField("creationDate","Creation Date");
	   field3.setWrap(true);
	   field3.setAlign(Alignment.CENTER);
	   
	   ListGridField field4 = new ListGridField("priority","Priority");
	   field4.setWrap(true);
	   field4.setAlign(Alignment.CENTER);
	   
	   ListGridField field5 = new ListGridField("customerId","Customer Id");
	   field5.setWrap(true);
	   field5.setAlign(Alignment.CENTER);
	   
	   ListGridField field6 = new ListGridField("customerName","Customer Name");
	   field6.setWrap(true);
	   field6.setAlign(Alignment.CENTER);
	   
	   /**
	   * Updated By: Viraj
	   * Date: 08-01-2019
	   * Description: To add customer point of contact in lead
	   */
	   ListGridField field15 = new ListGridField("customerPOC","Customer POC");
	   field15.setWrap(true);
	   field15.setAlign(Alignment.CENTER);
	   /** Ends **/
	   
	   ListGridField field7 = new ListGridField("customerCell","Customer Cell");
	   field7.setWrap(true);
	   field7.setAlign(Alignment.CENTER);
	   
	   ListGridField field8 = new ListGridField("category","Category");
	   field8.setWrap(true);
	   field8.setAlign(Alignment.CENTER);
	   
	   ListGridField field9 = new ListGridField("type","Type");
	   field9.setWrap(true);
	   field9.setAlign(Alignment.CENTER);
	   
	   ListGridField field10 = new ListGridField("group","Group"); 
	   field10.setWrap(true);
	   field10.setAlign(Alignment.CENTER);
	   
	   ListGridField field11 = new ListGridField("branch","Branch");
	   field11.setWrap(true);
	   field11.setAlign(Alignment.CENTER);
	   
	   ListGridField field12 = new ListGridField("status","Status");
	   field12.setWrap(true);
	   field12.setAlign(Alignment.CENTER);
	   
	   ListGridField field13 = new ListGridField("salesPerson","Sales Person");
	   field13.setWrap(true);
	   field13.setAlign(Alignment.CENTER);
	   
	   
	   
	   
	   return new ListGridField[] { 
			   field1, field2 ,field14,field3,field4,field5,field6,field15,field7,field13,field8,
			   field10,field11,field12,
			   };
	}
	
	//data source field
	protected static void setDataSourceField(DataSource dataSource) {
		DataSourceIntegerField pkField = new DataSourceIntegerField("pk");  
        pkField.setHidden(true);  
        pkField.setPrimaryKey(true);
		
	   DataSourceField field1 = new DataSourceField("leadId", FieldType.TEXT);
	   DataSourceField field2 = new DataSourceField("followupButton", FieldType.IMAGE);
	   
	   DataSourceDateField field14 = new DataSourceDateField("followupDate");
	   DataSourceDateField field3 = new DataSourceDateField("creationDate");
//	   DataSourceField field14 = new DataSourceField("followupDate", FieldType.TEXT);
//	   DataSourceField field3 = new DataSourceField("creationDate", FieldType.TEXT);
	   
	   DataSourceField field4 = new DataSourceField("priority", FieldType.TEXT);
	   DataSourceField field5 = new DataSourceField("customerId", FieldType.TEXT);
	   DataSourceField field6 = new DataSourceField("customerName", FieldType.TEXT);
	   /**
	   * Updated By: Viraj
	   * Date: 08-01-2019
	   * Description: To add customer point of contact in lead
	   */
	   DataSourceField field15 = new DataSourceField("customerPOC", FieldType.TEXT);
	   /** Ends **/
	   DataSourceField field7 = new DataSourceField("customerCell", FieldType.TEXT);
	   DataSourceField field8 = new DataSourceField("category", FieldType.TEXT);
	   DataSourceField field9 = new DataSourceField("type", FieldType.TEXT);
	   DataSourceField field10 = new DataSourceField("group", FieldType.TEXT);
	   DataSourceField field11 = new DataSourceField("branch", FieldType.TEXT);
	   DataSourceField field12 = new DataSourceField("status", FieldType.TEXT);
	   DataSourceField field13 = new DataSourceField("salesPerson", FieldType.TEXT);
	  
	   
	   dataSource.setFields(pkField,field1, field2 ,field14,field3,field4,field5,field6,field15,field7,field13,field8,field9,field10,field11,field12);
	}
	
	
	 private static ListGridRecord[] getGridData(ArrayList<Lead> leadList) {
		 
		 ListGridRecord[] records = null; 
		 records = new ListGridRecord[leadList.size()];
		 for(int i=0;i<leadList.size();i++){
			 ListGridRecord record = new ListGridRecord();
			 Lead lead=leadList.get(i);
			 
			 record.setAttribute("leadId", lead.getCount());
			 record.setAttribute("followupButton", "followup.png");
			 if(lead.getFollowUpDate()!=null){
				 record.setAttribute("followupDate", lead.getFollowUpDate());
			 }else{
				 record.setAttribute("followupDate", "");
			 }
			 record.setAttribute("creationDate",lead.getCreationDate());
			 record.setAttribute("priority", lead.getPriority());
			 record.setAttribute("customerId", lead.getPersonInfo().getCount());
			 record.setAttribute("customerName",lead.getPersonInfo().getFullName());
			 /**
			 * Updated By: Viraj
			 * Date: 08-01-2019
			 * Description: To add customer point of contact in lead
			 */
			 record.setAttribute("customerPOC", lead.getPersonInfo().getPocName());
			 /** Ends **/
			 record.setAttribute("customerCell", lead.getPersonInfo().getCellNumber());
			 record.setAttribute("salesPerson", lead.getEmployee());
			 record.setAttribute("category", lead.getCategory());
			 record.setAttribute("type", lead.getType());
			 record.setAttribute("group", lead.getGroup());
			 record.setAttribute("branch", lead.getBranch());
			 record.setAttribute("status", lead.getStatus());
			 records[i]=record;
		 }
		 System.out.println("RECORDS LENGTH : "+records.length);
		 
		 return records;
	}
	 
	 /**
		 * Toggles the app header bar menus as per screen state
		 */
		public void toggleAppHeaderBarMenu() {
			if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.equals("Download")||text.equals("Search"))
					{
						menus[k].setVisible(true); 
					}
					else
						menus[k].setVisible(false);  		  		
				}
			}
			
			else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.equals("Download"))
						menus[k].setVisible(true); 
					else
						menus[k].setVisible(false);  		   
				}
			}
			
			else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.equals("Download"))
						menus[k].setVisible(true); 
					else
						menus[k].setVisible(false);  		   
				}
			}
			AuthorizationHelper.setAsPerAuthorization(Screen.LEADDASHBOARD,LoginPresenter.currentModule.trim());
		}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
	
	public void showMessage(String msg){
		Window alert=new Window();
		
		alert.setWidth("25%");
        alert.setHeight("20%"); 
        alert.setTitle("Alert");
        alert.setShowMinimizeButton(false);
        alert.setIsModal(true);
        alert.setShowModalMask(true);
        alert.centerInPage();
        alert.setMargin(4);
        
//		InlineLabel msgLbl=new InlineLabel(msg);
//		alert.addItem(msgLbl);
		
//		window.setWidth(200);
//		window.setHeight(200);
//		window.setCanDragResize(true);
//		window.setShowFooter(true);

		Label label = new Label();
		label.setContents(msg);
		label.setAlign(Alignment.CENTER);
		label.setPadding(5);
		label.setHeight100();
		
		alert.addItem(label);
		
		
		alert.show();
	}
	
	
	

	public ListGrid getGrid() {
		return grid;
	}

	public void setGrid(ListGrid grid) {
		this.grid = grid;
	}

	public DataSource getDs() {
		return ds;
	}

	public void setDs(DataSource ds) {
		this.ds = ds;
	}

	public ObjectListBox<Branch> getOlbbranch() {
		return olbbranch;
	}

	public void setOlbbranch(ObjectListBox<Branch> olbbranch) {
		this.olbbranch = olbbranch;
	}

	public ObjectListBox<Employee> getOlbsalesPerson() {
		return olbsalesPerson;
	}

	public void setOlbsalesPerson(ObjectListBox<Employee> olbsalesPerson) {
		this.olbsalesPerson = olbsalesPerson;
	}
	
	/**
	 * Updated By: Viraj
	 * Date: 11-01-2019
	 * Description: To add Status filter in lead dashboard
	 */
	public ObjectListBox<ConfigCategory> getOlbLeadStatus() {
		return olbLeadStatus;
	}

	public void setOlbLeadStatus(ObjectListBox<ConfigCategory> olbLeadStatus) {
		this.olbLeadStatus = olbLeadStatus;
	}
	/** Ends **/

	public DateBox getDbFromDate() {
		return dbFromDate;
	}

	public void setDbFromDate(DateBox dbFromDate) {
		this.dbFromDate = dbFromDate;
	}

	public DateBox getDbToDate() {
		return dbToDate;
	}

	public void setDbToDate(DateBox dbToDate) {
		this.dbToDate = dbToDate;
	}

	public Button getBtnGo() {
		return btnGo;
	}

	public void setBtnGo(Button btnGo) {
		this.btnGo = btnGo;
	}

	public FlexForm getForm() {
		return form;
	}

	public void setForm(FlexForm form) {
		this.form = form;
	}

	public HorizontalPanel getHorizontalpanel() {
		return horizontalpanel;
	}

	public void setHorizontalpanel(HorizontalPanel horizontalpanel) {
		this.horizontalpanel = horizontalpanel;
	}

	public VerticalPanel getVerticalpanel() {
		return verticalpanel;
	}

	public void setVerticalpanel(VerticalPanel verticalpanel) {
		this.verticalpanel = verticalpanel;
	}
}
/** Ends **/