package com.slicktechnologies.client.views.dashboard.leaddashboard;

import java.util.Date;

import org.apache.poi.ss.usermodel.HorizontalAlignment;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.view.client.NoSelectionModel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FlexForm;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.ViewContainer;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.communicationlog.CommunicationLogTable;
import com.slicktechnologies.client.views.interaction.interactiondetails.InteractionForm;
import com.slicktechnologies.client.views.interaction.interactiondetails.InteractionPresenter;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.widgets.Window;

public class CommunicationSmartPoup extends AbsolutePanel {
	
	private TextArea remark;
	private DateBox dueDate;
	Button btnAdd,btnOk,btnCancel;
	
	CommunicationLogTable communicationLogTable;
	ObjectListBox<Config> oblinteractionGroup;
	
//	GWTCAlert alert = new GWTCAlert();
	
  public Window winModal ;
	
	FlexForm form;
	ObjectListBox<ConfigCategory> olbLeadStatus;
	ObjectListBox<Employee> olbsalesPerson = new ObjectListBox<Employee>();
//	@Override
//	protected void createGui() {
//		winModal = new Window();
//		winModal.setWidth("60%");
//        winModal.setHeight("65%"); 
//        winModal.setTitle("");
//        winModal.setShowMinimizeButton(false);
//        winModal.setIsModal(true);
//        winModal.setShowModalMask(true);
//        winModal.centerInPage();
//        winModal.setMargin(4);
//        
//        
////		InlineLabel lbRemark = new InlineLabel("Interaction Remark (500 character only)");
////		add(lbRemark,10,20);
//		
//		remark = new TextArea();
////		remark.setWidth("400px");
////		add(remark,10,40);
//		
////		InlineLabel lbDueDate = new InlineLabel("Interaction Due Date");
////		add(lbDueDate,440,20);
//		
//		dueDate = new DateBoxWithYearSelector();
////		dueDate.setWidth("120px");
////		add(dueDate,440,40);
//		
//		DateTimeFormat dateFormat = DateTimeFormat.getFormat("dd/MM/yyyy"); 
//		dueDate.setFormat(new DateBox.DefaultFormat(dateFormat));
//		
////		InlineLabel lblInteractionGroup = new InlineLabel("Interaction Group");
////		add(lblInteractionGroup,590,20);
//		
//		oblinteractionGroup = new ObjectListBox<Config>();
//		AppUtility.MakeLiveConfig(oblinteractionGroup, Screen.GROUPTYPE);
////		add(oblinteractionGroup,590,40);
//		
//		
//		btnAdd = new Button("Add");
////		add(btnAdd,740,35);
//		
//		communicationLogTable = new CommunicationLogTable();
//		communicationLogTable.connectToLocal();
////		add(communicationLogTable.getTable(),10,100);
//		
//		btnOk = new Button("Save");
////		add(btnOk,270,470);
//		
//		btnCancel = new Button("Cancel");
////		add(btnCancel,380,470);
//		
//		
////		setSize("800px", "500px");
////		this.getElement().setId("form");
//		
//		
//		setTableSelectionOnInteractionToDo();
//		
//		
//		
////		FormFieldBuilder builder;
////		
////		builder = new FormFieldBuilder();
////		FormField fgroupingCustInfo=builder.setlabel("Communication Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
////		
////		builder = new FormFieldBuilder("Interaction Remark (500 character only)",remark);
////		FormField fremark= builder.setMandatory(false).setRowSpan(1).setColSpan(2).build();
////		
////		builder = new FormFieldBuilder("Interaction Due Date",dueDate);
////		FormField fdueDate= builder.setMandatory(false).setRowSpan(1).setColSpan(1).build();
////		
////		builder = new FormFieldBuilder("Interaction Group",oblinteractionGroup);
////		FormField foblinteractionGroup= builder.setMandatory(false).setRowSpan(1).setColSpan(1).build();
////		
////		builder = new FormFieldBuilder("",btnAdd);
////		FormField fbtnAdd= builder.setMandatory(false).setRowSpan(1).setColSpan(1).build();
////		
////		builder = new FormFieldBuilder("",communicationLogTable.getTable());
////		FormField fcommunicationLogTable= builder.setMandatory(false).setRowSpan(7).setColSpan(5).build();
////		
////		builder = new FormFieldBuilder("",btnOk);
////		FormField fbtnOk= builder.setMandatory(false).setRowSpan(1).setColSpan(0).build();
////		
////		builder = new FormFieldBuilder("",btnCancel);
////		FormField fbtnCancel= builder.setMandatory(false).setRowSpan(1).setColSpan(0).build();
////		
////		FormField[][] fields=new FormField[][]{
////				{fgroupingCustInfo},
////				{fremark,fdueDate,foblinteractionGroup,fbtnAdd},
////				{fcommunicationLogTable},
////				{fbtnOk,fbtnCancel},
////				};
////		
////		form=new FlexForm(fields,FormStyle.ROWFORM);
////		content.add(form);
////
////		winModal.addItem(content);
//	}
	public  CommunicationSmartPoup(){
//		super();
//		createGui();
		
		winModal = new Window();
		winModal.setWidth("825px");
        winModal.setHeight("575px"); 
        winModal.setTitle("");
        winModal.setShowMinimizeButton(false);
        winModal.setIsModal(true);
        winModal.setShowModalMask(true);
        winModal.centerInPage();
        winModal.setMargin(4);
        winModal.setCanDragResize(true);
        
        
		InlineLabel lbRemark = new InlineLabel("Interaction Remark (500 character only)");
		add(lbRemark,10,20);
		
		remark = new TextArea();
		remark.setWidth("400px");
		add(remark,10,40);
		
		InlineLabel lbDueDate = new InlineLabel("Interaction Due Date");
		add(lbDueDate,440,20);
		
		dueDate = new DateBoxWithYearSelector();
		dueDate.setWidth("120px");
		add(dueDate,440,40);
		
		DateTimeFormat dateFormat = DateTimeFormat.getFormat("dd-MM-yyyy"); 
		dueDate.setFormat(new DateBox.DefaultFormat(dateFormat));
		
		InlineLabel lblInteractionGroup = new InlineLabel("Interaction Group");
		add(lblInteractionGroup,590,20);
		
		oblinteractionGroup = new ObjectListBox<Config>();
		/**** Date 19-10-2018 By Vijay for interaction group overlap issue updated width   ***/
		oblinteractionGroup.getElement().getStyle().setWidth(110, Unit.PX);
		AppUtility.MakeLiveConfig(oblinteractionGroup, Screen.GROUPTYPE);
		add(oblinteractionGroup,590,40);
		
		
		btnAdd = new Button("Add");
		add(btnAdd,740,35);
		
		communicationLogTable = new CommunicationLogTable();
		communicationLogTable.connectToLocal();
		add(communicationLogTable.getTable(),10,100);
		
		btnOk = new Button("Save");
		add(btnOk,270,470);
		
		btnCancel = new Button("Cancel");
		add(btnCancel,380,470);
		
		
		setSize("800px", "500px");
		this.getElement().setId("form");
		
		
		setTableSelectionOnInteractionToDo();
		
		winModal.addItem(this);

	}
	
	public CommunicationSmartPoup(boolean isLead){
//		super();
//		createGui();
		
		winModal = new Window();
		winModal.setWidth("825px");
        winModal.setHeight("575px"); 
//        winModal.setTitle(this.getTitle());
        winModal.setAlign(Alignment.CENTER);
        winModal.setShowMinimizeButton(false);
        winModal.setIsModal(true);
        winModal.setShowModalMask(true);
        winModal.centerInPage();
        winModal.setMargin(4);
        winModal.setCanDragResize(true);
        
        
//		InlineLabel lbRemark = new InlineLabel("Interaction Remark (500 character only)");
//        InlineLabel lbRemark = new InlineLabel("Note (500 character only)");
//		add(lbRemark,10,20);
//		
//		remark = new TextArea();
//		remark.setWidth("400px");
//		add(remark,10,40);
		
//		InlineLabel lbDueDate = new InlineLabel("Interaction Due Date");
		InlineLabel lbDueDate = new InlineLabel("Follow Up Date");
		add(lbDueDate,10,20);
		
		
		dueDate = new DateBoxWithYearSelector();
		dueDate.setWidth("130px");
		add(dueDate,10,40);
		
		DateTimeFormat dateFormat = DateTimeFormat.getFormat("dd-MM-yyyy"); 
		dueDate.setFormat(new DateBox.DefaultFormat(dateFormat));
		
//		InlineLabel lblInteractionGroup = new InlineLabel("Interaction Group");
		InlineLabel lblInteractionGroup = new InlineLabel("Remark");
		add(lblInteractionGroup,220,20);
		
		oblinteractionGroup = new ObjectListBox<Config>();
		/**** Date 19-10-2018 By Vijay for interaction group overlap issue updated width   ***/
		oblinteractionGroup.getElement().getStyle().setWidth(130, Unit.PX);
		AppUtility.MakeLiveConfig(oblinteractionGroup, Screen.GROUPTYPE);
		add(oblinteractionGroup,220,40);
		
	
		
		InlineLabel lblleadStatus = new InlineLabel("Lead Status");
		add(lblleadStatus,440,20);
		
		olbLeadStatus = new ObjectListBox<ConfigCategory>();
		olbLeadStatus.getElement().getStyle().setWidth(130, Unit.PX);
		AppUtility.MakeLiveConfig(olbLeadStatus, Screen.LEADSTATUS);
		add(olbLeadStatus,440,40);
		
		
		    InlineLabel lbRemark1 = new InlineLabel("Note (500 character only)");
		    add(lbRemark1,10,70);
			
			remark = new TextArea();
			remark.setWidth("400px");
			add(remark,10,90);
		
		
		InlineLabel lblSalesperson = new InlineLabel("Sales Person");
		add(lblSalesperson,440,70);
		
		olbsalesPerson = new ObjectListBox<Employee>();
		olbsalesPerson.getElement().getStyle().setWidth(130, Unit.PX);
		AppUtility.makeSalesPersonListBoxLive(olbsalesPerson);
		add(olbsalesPerson,440,90);
		
		
		
		
		btnAdd = new Button("Add");
		add(btnAdd,740,35);
		
		communicationLogTable = new CommunicationLogTable(true);
//		communicationLogTsable.setEnable(false);
		communicationLogTable.connectToLocal();
		add(communicationLogTable.getTable(),10,150);
		
		btnOk = new Button("Save");
		add(btnOk,270,470);
		
		btnCancel = new Button("Cancel");
		add(btnCancel,380,470);
		
		
		setSize("800px", "500px");
		this.getElement().setId("form");
		
		
		setTableSelectionOnInteractionToDo();
		
		winModal.addItem(this);

	}
	
	public void showPopup(){
		winModal.show();
	}
	
	public void hidePopup(){
		winModal.hide();
	}


	private void setTableSelectionOnInteractionToDo() {

		 final NoSelectionModel<InteractionType> selectionModelMyObj = new NoSelectionModel<InteractionType>();
	     
	     SelectionChangeEvent.Handler  tableHandler = new SelectionChangeEvent.Handler() 
	     {
	         @Override
	         public void onSelectionChange(SelectionChangeEvent event) 
	         {
	        	 final InteractionType entity=selectionModelMyObj.getLastSelectedObject();
	        	 if(entity.isCommunicationlogFlag() == false){
	        	 LoginPresenter.communicationLogPanel.hide();	 
	        	 AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
	        	 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Interaction TO DO",Screen.INTERACTION);
	        	 final InteractionForm form = InteractionPresenter.initalize();
	        	 form.showWaitSymbol();
				 AppMemory.getAppMemory().stickPnel(form);
	        	
				 Timer timer=new Timer() 
	        	 {
	 				@Override
	 				public void run() {
	 					 form.hideWaitSymbol();
					     form.updateView(entity);
					     form.setToViewState();
	 				}
	 			};
	            
	             timer.schedule(3000); 
	             
	        	 }
	          }
	     };
	     
	     // Add the handler to the selection model
	     selectionModelMyObj.addSelectionChangeHandler( tableHandler );
	     // Add the selection model to the table
	     
	     communicationLogTable.getTable().setSelectionModel(selectionModelMyObj);
	 
	
	}

	public ObjectListBox<Config> getOblinteractionGroup() {
		return oblinteractionGroup;
	}

	public void setOblinteractionGroup(ObjectListBox<Config> oblinteractionGroup) {
		this.oblinteractionGroup = oblinteractionGroup;
	}

	private void reactOnAdd() {

	InteractionType interaction = new InteractionType();
	
	Date date = new Date();
	DateTimeFormat dtf = DateTimeFormat.getFormat("HH:mm:ss");
    String time=new String(dtf.format(date).toString());

    interaction.setinteractionCreationDate(date);
    interaction.setInteractionTime(time);
    interaction.setCreatedBy(LoginPresenter.loggedInUser);
    interaction.setInteractionPurpose(remark.getValue());
    interaction.setInteractionDueDate(dueDate.getValue());
    interaction.setCommunicationlogFlag(true);
    this.communicationLogTable.getDataprovider().getList().add(interaction);
	
	}

	public Button getBtnOk() {
		return btnOk;
	}

	public void setBtnOk(Button btnOk) {
		this.btnOk = btnOk;
	}

	public Button getBtnCancel() {
		return btnCancel;
	}

	public void setBtnCancel(Button btnCancel) {
		this.btnCancel = btnCancel;
	}

	public CommunicationLogTable getCommunicationLogTable() {
		return communicationLogTable;
	}

	public void setCommunicationLogTable(CommunicationLogTable communicationLogTable) {
		this.communicationLogTable = communicationLogTable;
	}
	
	public Button getBtnAdd() {
		return btnAdd;
	}

	public void setBtnAdd(Button btnAdd) {
		this.btnAdd = btnAdd;
	}	
	public TextArea getRemark() {
		return remark;
	}

	public void setRemark(TextArea remark) {
		this.remark = remark;
	}

	public DateBox getDueDate() {
		return dueDate;
	}

	public void setDueDate(DateBox dueDate) {
		this.dueDate = dueDate;
	}

	public ObjectListBox<ConfigCategory> getOlbLeadStatus() {
		return olbLeadStatus;
	}

	public void setOlbLeadStatus(ObjectListBox<ConfigCategory> olbLeadStatus) {
		this.olbLeadStatus = olbLeadStatus;
	}

	public ObjectListBox<Employee> getOlbsalesPerson() {
		return olbsalesPerson;
	}

	public void setOlbsalesPerson(ObjectListBox<Employee> olbsalesPerson) {
		this.olbsalesPerson = olbsalesPerson;
	}

//	@Override
//	public void applyStyle() {
//		// TODO Auto-generated method stub
//		
//	}
	
	

	

}
