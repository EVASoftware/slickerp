package com.slicktechnologies.client.views.dashboard.leaddashboard;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CommunicationLogService;
import com.slicktechnologies.client.services.CommunicationLogServiceAsync;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.device.DeviceForm;
import com.slicktechnologies.client.views.device.DevicePresenter;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.client.views.lead.LeadForm;
import com.slicktechnologies.client.views.lead.LeadPresenter;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;

public class LeadDashboardPresenter implements ClickHandler, CellClickHandler {

	LeadDashboardForm form;
	LeadDashboardSearchForm searchForm;
	
	GenricServiceAsync service=GWT.create(GenricService.class);
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	ArrayList<Lead> globalLeadList=new ArrayList<Lead>();

	
	CommunicationLogServiceAsync communicationService = GWT.create(CommunicationLogService.class);
	
	Lead leadEntity=null;
	CommunicationSmartPoup commSmrtPopup=new CommunicationSmartPoup(true);
	ListGridRecord selectedRecord=null;
	int position;
	
	protected AppMemory mem;
	
	/**
	 * Updated By: Viraj
	 * Date: 17-01-2019
	 * Description: To fill data of lead in popup
	 */
	public static PopupPanel docLeadPanel = null;
	VerticalPanel docVerticalPanel = null;
	FlowPanel docflowpanel = null;
	VerticalPanel mainDocVerticalPanel = null;
	/** Ends **/
	/** Updated By: Viraj Date: 16-03-2019 Description: To make popup editable **/
	GeneralViewDocumentPopup gvPopup = null;
	public LeadDashboardPresenter(LeadDashboardForm homeForm,LeadDashboardSearchForm searchpopup) {
		this.form=homeForm;
		this.searchForm=searchpopup;
		form.grid.addCellClickHandler(this);
		mem=AppMemory.getAppMemory();
		setEventHandeling();
	/**Date 30-9-2019 by Amol commented this method to stop Laoding All data at the time 
	 * of initilaisation raised by Rahul T. For Ankitapest**/
//		retrieveLeadData(); //Ashwini Patil Date:23-08-2024 to stop auto loading

		
		/**
	     * Updated By: Viraj
	     * Date: 17-01-2019
	     * Description: To fill data of lead in popup
	     */
	    docLeadPanel = new PopupPanel(true);
	    docflowpanel = new FlowPanel();
	    mainDocVerticalPanel = new VerticalPanel();
	    
	    mainDocVerticalPanel.add(docflowpanel);
		
//		docVerticalPanel = new VerticalPanel();
//		mainDocVerticalPanel.add(docVerticalPanel);
//				
//		docLeadPanel.add(mainDocVerticalPanel);
		/** Ends **/
	}
	
	private void setEventHandeling() {
		form.getBtnGo().addClickHandler(this);
		searchForm.golbl.addClickHandler(this);
		
		commSmrtPopup.getBtnOk().addClickHandler(this);
		commSmrtPopup.getBtnCancel().addClickHandler(this);
		commSmrtPopup.getBtnAdd().addClickHandler(this);
		
		InlineLabel label[] = AppMemory.getAppMemory().skeleton.getMenuLabels();
		for (int k = 0; k < label.length; k++) {
			if (AppMemory.getAppMemory().skeleton.registration[k] != null)
				AppMemory.getAppMemory().skeleton.registration[k].removeHandler();
		}
		for (int k = 0; k < label.length; k++) {
			AppMemory.getAppMemory().skeleton.registration[k] = label[k].addClickHandler(this);
		}
	}

	public static void initialize(){
		System.out.println("INSIDE CHART INITIALIZATION");
		LeadDashboardForm homeForm = new LeadDashboardForm();
		AppMemory.getAppMemory().currentScreen=Screen.HOME;
		AppMemory.getAppMemory().skeleton.getProcessName().setText(AppConstants.SERVICEMODULE+"/"+"Lead Dashboard");
		
		LeadDashboardSearchForm searchForm=new LeadDashboardSearchForm();
		LeadDashboardPresenter presenter= new LeadDashboardPresenter(homeForm,searchForm);
		
		AppMemory.getAppMemory().stickPnel(homeForm);	
	}
	
	public void retrieveLeadData(){
		Date date=new Date();
		System.out.println("Today Date ::::::::: "+date);
//		CalendarUtil.addDaysToDate(date,7);
//	    System.out.println("Changed Date + 7 ::::::::: "+date);
		
		Vector<Filter> filtervec=null;
		Filter filter = null;
		MyQuerry querry;
		filtervec=new Vector<Filter>();
		List<String> statusList=new ArrayList<String>();
		statusList=AppUtility.getConfigValue(6);
		System.out.println("STATUS LIST SIZE:  "+statusList.size());
		
		for(int i=0;i<statusList.size();i++){
			if(statusList.get(i).trim().equalsIgnoreCase(AppConstants.Sucessful)
					/**
					 * Updated By: Viraj
					 * Date: 11-01-2019
					 * Description: To add Status filter in lead dashboard
					 */
					||statusList.get(i).trim().equalsIgnoreCase(AppConstants.Successful)
					||statusList.get(i).trim().equalsIgnoreCase("successfull")
//					||statusList.get(i).trim().equalsIgnoreCase("unsuccessfull")
					/** Ends **/
					||statusList.get(i).trim().equalsIgnoreCase(AppConstants.Unsucessful)
					||statusList.get(i).trim().equalsIgnoreCase(AppConstants.CANCELLED)
					||statusList.get(i).trim().equalsIgnoreCase(AppConstants.CANCEL)
					||statusList.get(i).trim().equalsIgnoreCase(AppConstants.REJECTED)
					||statusList.get(i).trim().equalsIgnoreCase(AppConstants.CLOSED)
					||statusList.get(i).trim().equalsIgnoreCase("Completed")
					||statusList.get(i).trim().equalsIgnoreCase(AppConstants.Successful)
					||statusList.get(i).trim().equalsIgnoreCase("Quotation Sent")){
				statusList.remove(i);
				i--;
			}
		}
		System.out.println("AFTER STATUS LIST SIZE:  "+statusList.size());
		
		filter = new Filter();
		filter.setQuerryString("status IN");
		filter.setList(statusList);
		filtervec.add(filter);
				
		filter = new Filter();
//		filter.setQuerryString("followUpDate <=");
		filter.setQuerryString("followUpDate");
		filter.setDateValue(date);
		filtervec.add(filter);
		
		List<String> branchList ;
		branchList= new ArrayList<String>();
		for (int i = 0; i < LoginPresenter.globalBranch.size(); i++) {
			branchList.add(LoginPresenter.globalBranch.get(i).getBusinessUnitName());
		}
	
		filter = new Filter();
		filter.setQuerryString("branch IN");
		filter.setList(branchList);
		filtervec.add(filter);
		
		querry=new MyQuerry();
		querry.setQuerryObject(new Lead());
		querry.setFilters(filtervec);
		this.retriveTable(querry);
		
	}
	
	
	public void retriveTable(MyQuerry querry){
		service.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				globalLeadList=new ArrayList<>();
				
				if(result.size()==0){
					SC.say("No result found.");
				}
				for(SuperModel model:result){
					Lead lead=(Lead) model;
					
					if(lead.getLeadScreenType().equals(AppConstants.SERVICETYPECUST))//Ashwini Patil Date:26-07-2023 service dashboard was showing sales leads as well
						globalLeadList.add(lead);
				}
				if(globalLeadList.size()==0){
					SC.say("No result found.");
				}
				form.setDataToListGrid(globalLeadList);
			}
			@Override
			public void onFailure(Throwable caught) {
				caught.printStackTrace();
			}
		}); 
	 }
	

	@Override
	public void onClick(ClickEvent event) {
		System.out.println("CLICK EVENT..!!!");
		if(event.getSource() instanceof InlineLabel){
			InlineLabel lbl= (InlineLabel) event.getSource();
			if(lbl.getText().equals("Download")){
				reactOnDownload();
			}
			if(lbl.getText().equals("Search")){
				System.out.println("ON CLICK OF SEARCH..!!!");
				searchForm.showPopUp();
			}
			if(lbl.getText().equals("Go")){
				reactOnGo();
			}
		}
		
		if (event.getSource().equals(form.getBtnGo())) {
			System.out.println("BTN GO CLICK");
			if(LoginPresenter.branchRestrictionFlag==true&&form.getOlbbranch().getSelectedIndex()==0){
				SC.say("Please select branch.");
				return;
			}
			System.out.println("FRM DT "+form.getDbFromDate().getValue());
			if (form.getOlbbranch().getSelectedIndex() == 0
					&& form.getOlbsalesPerson().getSelectedIndex() == 0
					&& form.getDbFromDate().getValue()==null
					&& form.getDbToDate().getValue()==null
					&& form.getOlbLeadStatus().getSelectedIndex() == 0) {		//added by viraj 11-01-2019 for status 
				SC.say("Please select atleast one filter.");
				return;
			}else {

				if (form.getOlbsalesPerson().getSelectedIndex() != 0
						&& form.getOlbbranch().getSelectedIndex() == 0
						&& (form.getDbFromDate().getValue() == null && form.getDbToDate() != null)) {

					searchByFromToDate();

				}
				/**
				 * Updated By: Viraj
				 * Date: 11-01-2019
				 * Description: To add Status filter in lead dashboard
				 */
				else if (form.getOlbLeadStatus().getSelectedIndex() != 0
						&& form.getOlbbranch().getSelectedIndex() == 0
						&& form.getOlbsalesPerson().getSelectedIndex() == 0
						&& (form.getDbFromDate().getValue() == null && form.getDbToDate() != null)) {
					
					searchByFromToDate();
				}
				/** Ends **/
				else if (form.getOlbsalesPerson().getSelectedIndex() == 0
						&& form.getOlbbranch().getSelectedIndex() != 0
						&& (form.getDbFromDate().getValue() == null && form.getDbToDate() != null)) {

					searchByFromToDate();
				} else if (form.getOlbsalesPerson().getSelectedIndex() != 0
						&& form.getOlbbranch().getSelectedIndex() != 0
						&& (form.getDbFromDate().getValue() == null && form.getDbToDate() != null)) {

					searchByFromToDate();
				} else {
					if (form.getDbFromDate().getValue() == null
							|| form.getDbToDate().getValue() == null) {
						
						SC.say("Select From and To Date.");
					}
					if (form.getDbFromDate().getValue() != null&& form.getDbToDate() != null) {
						Date formDate = form.getDbFromDate().getValue();
						Date toDate = form.getDbToDate().getValue();
						/**
						 * @author Vijay Chougule Date 08-07-2020
						 * Des :- Updated validation to search for 1 month 
						 */
						if (toDate.equals(formDate) || toDate.after(formDate)) {
							CalendarUtil.addDaysToDate(formDate, 31);
							if (toDate.before(formDate)) {
								searchByFromToDate();
							} else {
								SC.say("From Date and To Date Difference Can Not Be More Than 1 Month.");
							}
						} else {
							SC.say("To Date should be greater than From Date.");
						}
					}
				}
			}
		}
		
		
		
		if (event.getSource() == commSmrtPopup.getBtnOk()) {
//			glassPanel.show();
			List<InteractionType> list = commSmrtPopup.getCommunicationLogTable().getDataprovider().getList();
			ArrayList<InteractionType> interactionlist = new ArrayList<InteractionType>();
			interactionlist.addAll(list);

			boolean checkNewInteraction = AppUtility.checkNewInteractionAdded(interactionlist);

			if (checkNewInteraction == false) {
				SC.say("Please add new interaction details");
//				glassPanel.hide();
			} else {
				final Date followUpDate = interactionlist.get(interactionlist.size() - 1).getInteractionDueDate();
				final String status=interactionlist.get(interactionlist.size() - 1).getInteractionStatus();
				final String salesPerson=	interactionlist.get(interactionlist.size() - 1).getInteractionPersonResponsible();	
				Console.log(" lead status 11"+status);
				Console.log(" lead sales person 11 "+salesPerson);
				communicationService.saveCommunicationLog(interactionlist,new AsyncCallback<Void>() {
					@Override
					public void onFailure(Throwable caught) {
						SC.say("Unexpected Error");
//						glassPanel.hide();
//						LoginPresenter.communicationLogPanel.hide();
					}

					@Override
					public void onSuccess(Void result) {
						commSmrtPopup.hidePopup();
						SC.say("Data Save Successfully");
//						List<Lead> list = getDataprovider().getList();
//						list.get(rowIndex).setFollowUpDate(followUpDate);
//						Lead leadObj = list.get(rowIndex);
						leadEntity.setFollowUpDate(followUpDate);
						leadEntity.setStatus(status);
						leadEntity.setEmployee(salesPerson);
						Console.log(" lead status 22"+status);
						Console.log(" lead sales person 22 "+salesPerson);
						
						selectedRecord.setAttribute("status", status);
						selectedRecord.setAttribute("salesPerson", salesPerson);
						selectedRecord.setAttribute("followupDate", AppUtility.parseDate(followUpDate));
						saveLead(leadEntity);
						form.grid.getDataSource().updateData(selectedRecord);
						form.grid.redraw();
//						followUpFlag = false;
//						table.redraw();
//						glassPanel.hide();
//						communicationPanel.hide();
					}
				});
			}

		}
		if (event.getSource() == commSmrtPopup.getBtnCancel()) {
//			followUpFlag = false;
//			communicationPanel.hide();
			
			commSmrtPopup.hidePopup();
		}
		if (event.getSource() == commSmrtPopup.getBtnAdd()) {
//			glassPanel.show();
			String remark = commSmrtPopup.getRemark().getValue();
			Date dueDate = commSmrtPopup.getDueDate().getValue();
			String status=commSmrtPopup.getOlbLeadStatus().getValue();
			String salesPerson=commSmrtPopup.getOlbsalesPerson().getValue();
			String interactiongGroup = null;
			if (commSmrtPopup.getOblinteractionGroup().getSelectedIndex() != 0)
				interactiongGroup = commSmrtPopup.getOblinteractionGroup().getValue(commSmrtPopup.getOblinteractionGroup().getSelectedIndex());
				boolean validationFlag = AppUtility.validateCommunicationlogSmartGwt(remark, dueDate);

				List<InteractionType> list = commSmrtPopup.getCommunicationLogTable().getDataprovider().getList();
			
				Comparator<InteractionType> comp = new Comparator<InteractionType>() {
					@Override
					public int compare(InteractionType e1, InteractionType e2) {
						if (e1.getCount() == e2.getCount()) {
							return 0;
						}
						if (e1.getCount() > e2.getCount()) {
							return 1;
						} else {
							return -1;
						}
					}
				};
				Collections.sort(list, comp);
	
				if (validationFlag) {
					InteractionType communicationLog = AppUtility.getCommunicationLog(AppConstants.SERVICEMODULE,
									AppConstants.LEAD, leadEntity.getCount(),salesPerson, remark, dueDate,
									leadEntity.getPersonInfo(),null, interactiongGroup,leadEntity.getBranch(),status);
					list.add(communicationLog);
					commSmrtPopup.getCommunicationLogTable().getDataprovider().setList(list);
					commSmrtPopup.getRemark().setValue("");
					commSmrtPopup.getDueDate().setValue(null);
					commSmrtPopup.getOblinteractionGroup().setSelectedIndex(0);
				}
//				glassPanel.hide();
		}
		
	}
	
	private void saveLead(Lead leadObj) {
		service.save(leadObj, new AsyncCallback<ReturnFromServer>() {
			@Override
			public void onFailure(Throwable caught) {
//				glassPanel.hide();
				SC.say("An Unexpected error occurred!");
			}
			@Override
			public void onSuccess(ReturnFromServer result) {
//				glassPanel.hide();
				SC.say("Saved successfully!");
			}
		});

	}
	
	private void searchByFromToDate() {
		System.out.println("FROM AND DATE ---");
		Long compId = UserConfiguration.getCompanyId();
		Vector<Filter> filtervec = null;
		Filter filter = null;
		MyQuerry querry;

		filtervec = new Vector<Filter>();

		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(compId);
		filtervec.add(filter);
		
		List<String> statusList=new ArrayList<String>();
		statusList=AppUtility.getConfigValue(6);
		
		for(int i=0;i<statusList.size();i++){
			if(statusList.get(i).trim().equalsIgnoreCase(AppConstants.Sucessful)
					/**
					 * Updated By: Viraj
					 * Date: 11-01-2019
					 * Description: To add Status filter in lead dashboard
					 */
					||statusList.get(i).trim().equalsIgnoreCase(AppConstants.Successful)
//					||statusList.get(i).trim().equalsIgnoreCase("successfull")
//					||statusList.get(i).trim().equalsIgnoreCase("unsuccessfull")
					/** Ends **/
					||statusList.get(i).trim().equalsIgnoreCase(AppConstants.Unsucessful)
					||statusList.get(i).trim().equalsIgnoreCase(AppConstants.CANCELLED)
					||statusList.get(i).trim().equalsIgnoreCase(AppConstants.CANCEL)
					||statusList.get(i).trim().equalsIgnoreCase(AppConstants.REJECTED)
					||statusList.get(i).trim().equalsIgnoreCase(AppConstants.CLOSED)
					||statusList.get(i).trim().equalsIgnoreCase("Completed")
					||statusList.get(i).trim().equalsIgnoreCase(AppConstants.Successful)
					||statusList.get(i).trim().equalsIgnoreCase("Quotation Sent")){
				statusList.remove(i);
				i--;
			}
		}
		
		/**
		 * Updated By: Viraj
		 * Date: 11-01-2019
		 * Description: To add Status filter in lead dashboard
		 */
		if(form.getOlbLeadStatus().getSelectedIndex() != 0) {
			filter = new Filter();
			filter.setQuerryString("status");
			filter.setStringValue(form.getOlbLeadStatus().getValue());
			filtervec.add(filter);
		} else {
			filter = new Filter();
			filter.setQuerryString("status IN");
			filter.setList(statusList);
			filtervec.add(filter);
		}
		/** Ends **/

		if (form.getDbFromDate().getValue() != null) {
			filter = new Filter();
			filter.setQuerryString("followUpDate >=");
			filter.setDateValue(form.getDbFromDate().getValue());
			filtervec.add(filter);
		}

		if (form.getDbToDate().getValue() != null) {

			filter = new Filter();
			filter.setQuerryString("followUpDate <=");
			filter.setDateValue(form.getDbToDate().getValue());
			filtervec.add(filter);
		}

		if (form.getOlbbranch().getSelectedIndex() != 0) {
			filter = new Filter();
			filter.setQuerryString("branch");
			filter.setStringValue(form.getOlbbranch().getValue());
			filtervec.add(filter);

		}

		if (form.getOlbsalesPerson().getSelectedIndex() != 0) {
			filter = new Filter();
			filter.setQuerryString("employee");
			filter.setStringValue(form.getOlbsalesPerson().getValue());
			filtervec.add(filter);
		}

		querry = new MyQuerry();
		querry.setQuerryObject(new Lead());
		querry.setFilters(filtervec);
		this.retriveTable(querry);

	}
	
	
	public void reactOnGo() {

		if (!searchForm.personInfo.getId().getValue().equals("")
				&& !searchForm.personInfo.getName().getValue().equals("")
				&& !searchForm.personInfo.getPhone().getValue().equals("")) {
			searchForm.hidePopUp();
			MyQuerry querry = createLeadFilter();
			retriveTable(querry);
			
		}

		if (searchForm.personInfo.getId().getValue().equals("")
				&& searchForm.personInfo.getName().getValue().equals("")
				&& searchForm.personInfo.getPhone().getValue().equals("")) {
			SC.say("Please enter customer information");
		}

	}

	private MyQuerry createLeadFilter() {
		Vector<Filter> filterVec = new Vector<Filter>();
		Filter temp;

		if (searchForm.personInfo.getId().getText().trim().equals("") == false) {
			temp = new Filter();
			temp.setLongValue(Long.parseLong(searchForm.personInfo.getId().getValue()));
			temp.setQuerryString("personInfo.count");
			filterVec.add(temp);
		}

		if ((searchForm.personInfo.getName().getText()).trim().equals("") == false) {
			temp = new Filter();
			temp.setStringValue(searchForm.personInfo.getName().getValue());
			temp.setQuerryString("personInfo.fullName");
			filterVec.add(temp);
		}

		if (searchForm.personInfo.getPhone().getText().trim().equals("") == false) {
			temp = new Filter();
			temp.setLongValue(Long.parseLong(searchForm.personInfo.getPhone().getValue()));
			temp.setQuerryString("personInfo.cellNumber");
			filterVec.add(temp);
		}

		MyQuerry lead = new MyQuerry();
		lead.setQuerryObject(new Lead());
		lead.setFilters(filterVec);

		return lead;
	}

	private void reactOnDownload() {
		ArrayList<Lead> leadList = new ArrayList<Lead>();
		ListGridRecord[] allRecords = form.grid.getRecords();
        if (allRecords != null) {
        	List<ListGridRecord> allRecordsList = Arrays.asList(allRecords);
	   		for(int i=0;i<allRecordsList.size();i++){
	   			 final Lead model=getLeadDetails(Integer.parseInt(allRecordsList.get(i).getAttribute("leadId").trim()));
	   			 if(model!=null){
	   				leadList.add(model);
	   			 }
	   		}
        }
		
		csvservice.setleadslist(leadList, new AsyncCallback<Void>(){
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);
			}
			
			@Override
			public void onSuccess(Void result) {
				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "csvservlet"+"?type="+3;
				Window.open(url, "test", "enabled");
			}
		});
	}

	private Lead getLeadDetails(int leadId) {
		for(Lead object:globalLeadList){
			if(object.getCount()==leadId){
				return object;
			}
		}
		return null;
	}

	@Override
	public void onCellClick(CellClickEvent event) {
		
		ListGridRecord record = event.getRecord();
		int colNum = event.getColNum();
		ListGridField field = form.grid.getField(colNum);
		String fieldName = form.grid.getFieldName(colNum);
		String fieldTitle = field.getTitle();
		position=event.getRowNum();
		System.out.println("Clicked " + fieldTitle + ":"+ record.getAttribute(fieldName) + "  Lead Id :: "+ record.getAttribute("leadId")+"POSITION "+position);
		leadEntity = getLeadDetails((Integer.parseInt(record.getAttribute("leadId").trim())));
		if (fieldName.equals("followupButton")) {
			selectedRecord = record;
			getLogDetails(leadEntity);
		}else{
			mapToLeadScreen();
		}
	}
	
	private void mapToLeadScreen() {
		/**
		 * Updated By:Viraj
		 * Date: 17-01-2019
		 * Description: To display lead in popup
		 */
		removePopObjects();
		
//		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Lead", Screen.LEAD);
		final Lead entity = leadEntity;
		final LeadForm form = new LeadForm();
//		final LeadForm form = LeadPresenter.initalize();
		form.content.getElement().getStyle().setHeight(570, Unit.PX);
		form.content.getElement().getStyle().setWidth(950, Unit.PX);
//		docVerticalPanel.add(form.content);
		AppMemory.getAppMemory().currentScreen = Screen.LEAD;
		gvPopup = new GeneralViewDocumentPopup();
		gvPopup.setModel(AppConstants.LEAD, entity);
		
//		docLeadPanel.setHeight("600px");
//		docLeadPanel.setStyleName("gwt-SuggestBoxPopup");
//		docLeadPanel.show();
//		docLeadPanel.center();
		
		
//		form.showWaitSymbol();
//		Timer timer = new Timer() {
//
//			@Override
//			public void run() {
//				form.hideWaitSymbol();
//				form.updateView(entity);
////				form.setProcessLevelBar(null);
////				form.setToViewState();
////				form.setEnable(false);
//				form.getPic().setEnabled(false);
//			}
//		};
//		timer.schedule(5000);
		/** Ends **/
    }

	private void getLogDetails(final Lead lead) {
		
		MyQuerry querry = AppUtility.communicationLogQuerry(lead.getCount(),lead.getCompanyId(), AppConstants.SERVICEMODULE,AppConstants.LEAD);
		service.getSearchResult(querry,	new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				ArrayList<InteractionType> list = new ArrayList<InteractionType>();
				for (SuperModel model : result) {
					InteractionType interactionType = (InteractionType) model;
					list.add(interactionType);
				}
				Comparator<InteractionType> comp = new Comparator<InteractionType>() {
					@Override
					public int compare(InteractionType e1,InteractionType e2) {
//						if (e1.getCount() == e2.getCount()) {
//							return 0;
//						}
//						if (e1.getCount() > e2.getCount()) {
//							return 1;
//						} else {
//							return -1;
//						}
						/**
						 * Updated By: Viraj
						 * Date: 06-02-2018
						 * Description: To sort communication log
						 */
						Integer ee1=e1.getCount();
						Integer ee2=e2.getCount();
						return ee2.compareTo(ee1);
						/** Ends **/
					}
				};
				Collections.sort(list, comp);
				String title= lead.getCount()+"   "+lead.getCustomerFullName();
				commSmrtPopup.winModal.setTitle(title);
//				commSmrtPopup.setTitle(title);
				commSmrtPopup.getRemark().setValue("");
				commSmrtPopup.getDueDate().setValue(null);
				commSmrtPopup.getOblinteractionGroup().setSelectedIndex(0);
				commSmrtPopup.getOlbLeadStatus().setSelectedIndex(0);
				commSmrtPopup.getOlbsalesPerson().setSelectedIndex(0);
				commSmrtPopup.getCommunicationLogTable().getDataprovider().setList(list);
				
				commSmrtPopup.showPopup();
			}

			@Override
			public void onFailure(Throwable caught) {
			}
		});

	}
	
	/**
	 * Updated By: Viraj
	 * Date: 17-01-2019
	 * Description: To remove items added in popup
	 */
	 void removePopObjects() {
			try {

				for (Widget wi : docVerticalPanel) {
					boolean flag = docLeadPanel.remove(wi);
					wi.removeFromParent();
					System.out.println("get item remove status " + flag);
				}

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}
	/** Ends **/

}
