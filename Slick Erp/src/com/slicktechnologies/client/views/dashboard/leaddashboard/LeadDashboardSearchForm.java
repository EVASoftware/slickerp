package com.slicktechnologies.client.views.dashboard.leaddashboard;

import java.util.Vector;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FlexForm;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appstructure.ViewContainer;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;

public class LeadDashboardSearchForm extends ViewContainer{
	public PersonInfoComposite personInfo;
	protected MyQuerry quotation,contract,lead,services;
	public InlineLabel golbl;
	public HorizontalPanel horizontal;
	FlexForm form;
	
	Window winModal ;
	
	
	
	
	
	public LeadDashboardSearchForm()
	{
		super();
		
		winModal = new Window();
		winModal.setWidth("50%");
        winModal.setHeight("35%"); 
        winModal.setTitle("");
        winModal.setShowMinimizeButton(false);
        winModal.setIsModal(true);
        winModal.setShowModalMask(true);
        winModal.centerInPage();
        winModal.setMargin(4);
		
		createGui();
		applyStyle();
	}
	
	public void initWidget()
	{
		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new Customer());
		personInfo=new PersonInfoComposite(querry,false);
	}
	
	public void createScreen()
	{
		initWidget();
		FormFieldBuilder builder;
		
		builder = new FormFieldBuilder();
		FormField fgroupingCustInfo=builder.setlabel("Customer Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		
		builder = new FormFieldBuilder("",personInfo);
		FormField fpersonInfo= builder.setMandatory(false).setRowSpan(1).setColSpan(5).build();
		
		FormField[][] fields=new FormField[][]{
				{fgroupingCustInfo},
				{fpersonInfo},
				};
		form=new FlexForm(fields,FormStyle.ROWFORM);
		content.add(form);
		
		
		horizontal=new HorizontalPanel();
		//Go lbl
		golbl=new InlineLabel("Go");
		golbl.getElement().setId("addbutton");
		horizontal.add(golbl);
		horizontal.getElement().addClassName("centering");
	
		content.add(horizontal);
		
		
		winModal.addItem(content);
		
	
	}
	
	public void showPopUp()
	{
		//Clear the previous
		personInfo.clear();
	    
	    winModal.show();
	}
	
	
	public void hidePopUp()
	{
		winModal.hide();
	}
	
	
	public void createAllFilter1() {
		Vector<Filter> filterVec = new Vector<Filter>();
		Filter temp;
		if (personInfo.getId().getText().trim().equals("") == false) {
			temp = new Filter();
			temp.setLongValue(Long.parseLong(personInfo.getId().getValue()));
			temp.setQuerryString("personInfo.id");
			filterVec.add(temp);
		}

		if ((personInfo.getName().getText()).trim().equals("") == false) {
			temp = new Filter();
			temp.setStringValue(personInfo.getName().getValue());
			temp.setQuerryString("personInfo.fullName");
			filterVec.add(temp);
		}

		if (personInfo.getPhone().getText().trim().equals("") == false) {
			temp = new Filter();
			temp.setLongValue(Long.parseLong(personInfo.getPhone().getValue()));
			temp.setQuerryString("personInfo.cellNumber");
			filterVec.add(temp);
		}

//		this.quotation = new MyQuerry();
//		this.quotation.setFilters(filterVec);
//		this.quotation.setQuerryObject(new Quotation());

		this.lead = new MyQuerry();
		this.lead.setQuerryObject(new Lead());
		this.lead.setFilters(filterVec);

//		this.contract = new MyQuerry();
//		this.contract.setQuerryObject(new Contract());
//		this.contract.setFilters(filterVec);
//
//		this.services = new MyQuerry();
//		this.services.setQuerryObject(new Service());
//		this.services.setFilters(filterVec);
	}

	@Override
	protected void createGui() {
		createScreen();
	}

	@Override
	public void applyStyle() {
		content.getElement().setId("formcontent");
		form.getElement().setId("form");
	}
	
	
}
