package com.slicktechnologies.client.views.dashboard.contractdashboard;

import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FlexForm;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.ViewContainer;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.DataSourceField;
import com.smartgwt.client.data.fields.DataSourceDateField;
import com.smartgwt.client.data.fields.DataSourceIntegerField;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.AutoFitWidthApproach;
import com.smartgwt.client.types.Autofit;
import com.smartgwt.client.types.FieldType;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.grid.CellFormatter;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class ContractDashboardForm extends ViewContainer{


	ListGrid grid;
	DataSource ds;
	
	ObjectListBox<Branch> olbbranch=new ObjectListBox<Branch>();
	ObjectListBox<Employee> olbsalesPerson = new ObjectListBox<Employee>();
	DateBox dbFromDate;
	DateBox dbToDate;
	
	DateBox dbFromFollowupDate;
	DateBox dbToFollowupDate;
	
	
	Button btnGo;
	
	FlexForm form;
	
	
	HorizontalPanel horizontalpanel;
	VerticalPanel verticalpanel;
	
	public ContractDashboardForm() {
		super();
		createGui();
	}
	
	@Override
	protected void createGui() {
		System.out.println("CREATE GUI");
		toggleAppHeaderBarMenu();
		
		olbbranch =new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbbranch);
		olbsalesPerson =new ObjectListBox<Employee>();
		AppUtility.makeSalesPersonListBoxLive(olbsalesPerson);
		
		olbbranch.getElement().getStyle().setHeight(26, Unit.PX);
		olbsalesPerson.getElement().getStyle().setHeight(26, Unit.PX);
		
		dbFromDate=new DateBoxWithYearSelector();
		dbToDate=new DateBoxWithYearSelector();
		
		DateTimeFormat dateFormat = DateTimeFormat.getFormat("dd-MM-yyyy");
		
		dbFromDate.setFormat(new DateBox.DefaultFormat(dateFormat));
		dbToDate.setFormat(new DateBox.DefaultFormat(dateFormat));
		

		
		
		dbFromFollowupDate=new DateBoxWithYearSelector();
		dbToFollowupDate=new DateBoxWithYearSelector();
		
		DateTimeFormat dateFormat1 = DateTimeFormat.getFormat("dd-MM-yyyy");
		
		dbFromFollowupDate.setFormat(new DateBox.DefaultFormat(dateFormat1));
		dbToFollowupDate.setFormat(new DateBox.DefaultFormat(dateFormat1));
		
		
		
		btnGo=new Button("GO");
		
		
		horizontalpanel = new HorizontalPanel();
		
		
		VerticalPanel vpanel1=new VerticalPanel();
		VerticalPanel vpanel2=new VerticalPanel();
		VerticalPanel vpanel3=new VerticalPanel();
		VerticalPanel vpanel4=new VerticalPanel();
		VerticalPanel vpanel6=new VerticalPanel();
		VerticalPanel vpanel7=new VerticalPanel();
		VerticalPanel vpanel5=new VerticalPanel();
		
		
		
		InlineLabel lablel1=new InlineLabel("Branch");
		InlineLabel lablel2=new InlineLabel("Sales Person");
		InlineLabel lablel3=new InlineLabel("From Date (Creation Date)");
		InlineLabel lablel4=new InlineLabel("To Date (Creation Date)");
		InlineLabel lablel6=new InlineLabel("From Date (Follow up)");
		InlineLabel lablel7=new InlineLabel("To Date (Follow up)");
		InlineLabel lablel5=new InlineLabel(". ");
		
		vpanel1.add(lablel1);
		vpanel1.add(olbbranch);
		
		vpanel2.add(lablel2);
		vpanel2.add(olbsalesPerson);
		
		vpanel3.add(lablel3);
		vpanel3.add(dbFromDate);
		
		vpanel4.add(lablel4);
		vpanel4.add(dbToDate);
		
		vpanel6.add(lablel6);
		vpanel6.add(dbFromFollowupDate);
		
		vpanel7.add(lablel7);
		vpanel7.add(dbToFollowupDate);
		
		vpanel5.add(lablel5);
		vpanel5.add(btnGo);
		
		horizontalpanel.add(vpanel1);
		horizontalpanel.add(vpanel2);
		horizontalpanel.add(vpanel3);
		horizontalpanel.add(vpanel4);
		horizontalpanel.add(vpanel6);
		horizontalpanel.add(vpanel7);
		horizontalpanel.add(vpanel5);

//		horizontalpanel.add(olbbranch);
//		horizontalpanel.add(olbsalesPerson);
//		horizontalpanel.add(dbFromDate);
//		horizontalpanel.add(dbToDate);
//		horizontalpanel.add(btnGo);
		horizontalpanel.setSpacing(10);
		horizontalpanel.getElement().addClassName("technicianscheduleButtons");
		
		
		verticalpanel = new VerticalPanel();
				
		
		grid = new ListGrid();
		grid.setStyleName("GridList");
		setGridProperty(grid);
		grid.setFields(getGridFields());
		
		ds= new DataSource();
		ds.setClientOnly(true);
		setDataSourceField(ds);
		grid.setDataSource(ds);
		
		verticalpanel.getElement().addClassName("varticalGridPanel");
		grid.getElement().addClassName("technicianSchedule");
		verticalpanel.add(grid);
		
		content.add(horizontalpanel);
		content.add(verticalpanel);
		
//		builder = new FormFieldBuilder("",grid);
//		FormField fgrid= builder.setMandatory(false).setRowSpan(1).setColSpan(5).build();
//		
//		
//		FormField[][] fields=new FormField[][]{
////				{folbbranch,folbsalesPerson,fdbFromDate,fdbToDate,fbtnGo},
//				{fgrid}
//		};
//		form=new FlexForm(fields,FormStyle.ROWFORM);
//		
////		content.add(grid);
//		content.add(form);
		
	}
	
	public void setDataToListGrid(ArrayList<Contract> list){
		System.out.println("FORM SETTING DATA....");
		
		ds.setTestData(getGridData(list));
		grid.setDataSource(ds);
		grid.setFields(getGridFields());
		grid.fetchData();
		
//		grid.redraw();
	}

	
	
	//grid property
	private static void setGridProperty(ListGrid grid) {
	   grid.setWidth("83%");
	   grid.setHeight("70%");
	   grid.setAutoFetchData(true);
	   grid.setAlternateRecordStyles(true);
	   grid.setShowAllRecords(true);
	   grid.setShowFilterEditor(true);
	   grid.setFilterOnKeypress(true);
	   
	   grid.setWrapCells(true);	
	   grid.setFixedRecordHeights(false);
	   grid.setAutoFitData(Autofit.HORIZONTAL);	
	   grid.setAutoFitWidthApproach(AutoFitWidthApproach.BOTH);	
	   grid.setCanResizeFields(true);
	   
	   /**
	    * @author Anil
	    * @since 31-12-2020
	    * for resizing smartgrid
	    */
	   if(AppMemory.getAppMemory().enableMenuBar){
		   grid.setWidth("100%");
	   }
	}
	
	//fields
	private static ListGridField[] getGridFields() {
		 
		
	   ListGridField field1 = new ListGridField("contractId","Contract Id");
	   field1.setWrap(true);
	   field1.setAlign(Alignment.CENTER);
	   field1.setWidth(72);
	   
	   /** date 7.1.2019 uncommented by komal for communication log button **/
	   ListGridField field17 = new ListGridField("communicationLog","Communication Log");
	   field17.setWrap(true);
	   field17.setAlign(Alignment.CENTER);
	   field17.setType(ListGridFieldType.IMAGE);
	   field17.setDefaultValue("communicationLog.png");
	   field17.setCanEdit(false);
	   field17.setImageSize(80);
	   field17.setImageHeight(30);
	   field17.setWidth(72);
	  // field17.setAutoFitWidth(true);
	   
	   ListGridField field2 = new ListGridField("contractEndDate","Contract End Date");
	   field2.setWrap(true);
	   field2.setAlign(Alignment.CENTER);
	   field2.setWidth(72);
	   
	   ListGridField field14 = new ListGridField("contractStartDate","Contract Start Date");
	   field14.setWrap(true);
	   field14.setAlign(Alignment.CENTER);
	   field14.setWidth(72);
	   
	   
	   ListGridField field20 = new ListGridField("followupDate","Follow Up Date");
	   field20.setWrap(true);
	   field20.setAlign(Alignment.CENTER);
	   field20.setWidth(72);
	   field20.setCellFormatter(new CellFormatter() {
			@Override
			public String format(Object value, ListGridRecord record, int rowNum,int colNum) {
				 if (value instanceof Date) {
		                try {
		                    return AppUtility.parseDate((Date) value);
		                } catch (Exception e) {
		                    return value.toString();
		                }
		            } else {
		                return value == null ? null : value.toString();
		            }
			}
	   });
	   //
	   
	   ListGridField field3 = new ListGridField("creationDate","Creation Date");
	   field3.setWrap(true);
	   field3.setAlign(Alignment.CENTER);
	   field3.setWidth(72);
	   
	   field3.setCellFormatter(new CellFormatter() {
			@Override
			public String format(Object value, ListGridRecord record, int rowNum,int colNum) {
				 if (value instanceof Date) {
		                try {
		                    return AppUtility.parseDate((Date) value);
		                } catch (Exception e) {
		                    return value.toString();
		                }
		            } else {
		                return value == null ? null : value.toString();
		            }
			}
	   });
	   
	   ListGridField field5 = new ListGridField("customerId","Customer Id");
	   field5.setWrap(true);
	   field5.setAlign(Alignment.CENTER);
	   field5.setWidth(72);
	   
	   ListGridField field6 = new ListGridField("customerName","Customer Name");
	   field6.setWrap(true);
	   field6.setAlign(Alignment.CENTER);
	   field6.setWidth(72);
	   
	   ListGridField field7 = new ListGridField("customerCell","Customer Cell");
	   field7.setWrap(true);
	   field7.setAlign(Alignment.CENTER);
	   field7.setWidth(72);
	   
	   ListGridField field8 = new ListGridField("category","Category");
	   field8.setWrap(true);
	   field8.setAlign(Alignment.CENTER);
	   field8.setWidth(72);
	   
	   ListGridField field9 = new ListGridField("type","Type");
	   field9.setWrap(true);
	   field9.setAlign(Alignment.CENTER);
	   field9.setWidth(72);
	   
	   ListGridField field10 = new ListGridField("group","Group"); 
	   field10.setWrap(true);
	   field10.setAlign(Alignment.CENTER);
	   field10.setWidth(72);
	   
	   ListGridField field11 = new ListGridField("branch","Branch");
	   field11.setWrap(true);
	   field11.setAlign(Alignment.CENTER);
	   field11.setWidth(72);
	   
	   ListGridField field12 = new ListGridField("status","Status");
	   field12.setWrap(true);
	   field12.setAlign(Alignment.CENTER);
	   field12.setWidth(72);
	   
	   ListGridField field13 = new ListGridField("salesPerson","Sales Person");
	   field13.setWrap(true);
	   field13.setAlign(Alignment.CENTER);
	   field13.setWidth(72);
	   
	   ListGridField field4 = new ListGridField("quotationId","Quotation Id");
	   field4.setWrap(true);
	   field4.setAlign(Alignment.CENTER);
	   field4.setWidth(72);
	   
	   /**
	    * nidhi
	    * 7-06-2018
	    * for orien requirment
	    * 
	    */
	   ListGridField field16 = new ListGridField("salesSource","Sale Source");
	   field16.setWrap(true);
	   field16.setAlign(Alignment.CENTER);
	   field16.setWidth(72);
	   
	   return new ListGridField[] { 
			   field1,field17, field20,field3,field14,field2,field16/** nidhi 7-06-2018 for orien */,field5,field6,field7,field13,field8,
			   field9,field10,field11,field4,field12
			   };

	   /**
	    * end
	    */
	 /*  
	   return new ListGridField[] { 
			   field1, field3,field14,field2 ,field5,field6,field7,field13,field8,
			   field10,field11,field4,field12,
			   };*/
	}
	
	//data source field
	protected static void setDataSourceField(DataSource dataSource) {
		DataSourceIntegerField pkField = new DataSourceIntegerField("pk");  
        pkField.setHidden(true);  
        pkField.setPrimaryKey(true);
		
	   DataSourceField field1 = new DataSourceField("contractId", FieldType.TEXT);
	   
	   DataSourceDateField field2 = new DataSourceDateField("contractEndDate");
	   DataSourceDateField field14 = new DataSourceDateField("contractStartDate");
	   DataSourceField field20 = new DataSourceField("followupDate", FieldType.TEXT);
	   DataSourceDateField field3 = new DataSourceDateField("creationDate");
	  
//	   DataSourceField field3 = new DataSourceField("creationDate", FieldType.TEXT);
	   
	   DataSourceField field4 = new DataSourceField("priority", FieldType.TEXT);
	   DataSourceField field5 = new DataSourceField("customerId", FieldType.TEXT);
	   DataSourceField field6 = new DataSourceField("customerName", FieldType.TEXT);
	   DataSourceField field7 = new DataSourceField("customerCell", FieldType.TEXT);
	   DataSourceField field8 = new DataSourceField("category", FieldType.TEXT);
	   DataSourceField field9 = new DataSourceField("type", FieldType.TEXT);
	   DataSourceField field10 = new DataSourceField("group", FieldType.TEXT);
	   DataSourceField field11 = new DataSourceField("branch", FieldType.TEXT);
	   DataSourceField field12 = new DataSourceField("status", FieldType.TEXT);
	   DataSourceField field13 = new DataSourceField("salesPerson", FieldType.TEXT);
	   
	   DataSourceField field15 = new DataSourceField("quotationId", FieldType.TEXT);
	  
	   /**
	    * nidhi
	    * 7-06-2018
	    * for orien requirement
	    */
	   DataSourceField field16 = new DataSourceField("salesSource", FieldType.TEXT);
	   /** date 07.01.2018 added by komal for communication log button **/
	   DataSourceField field17 = new DataSourceField("communicationLog", FieldType.IMAGE);
	   
	   dataSource.setFields(pkField,field1,field17,field20 ,field3,field14, field2, field16 /** nidhi 7-06-2018 */,field5,field6,field7,field13,field8,field9,field10,field11,field15,field4,field12);
	 
	 
	   
//	   dataSource.setFields(pkField,field1 ,field3,field14, field2,field4,field5,field6,field7,field13,field8,field9,field10,field11,field15,field12);
	   /**
	    * end
	    */
	}
	
	
	 private static ListGridRecord[] getGridData(ArrayList<Contract> contractList) {
		 
		 ListGridRecord[] records = null; 
		 records = new ListGridRecord[contractList.size()];
		 for(int i=0;i<contractList.size();i++){
			 ListGridRecord record = new ListGridRecord();
			 Contract contract=contractList.get(i);
			 
			 record.setAttribute("contractId", contract.getCount());
			 /** date 7.1.2019 added by komal to show communication log image **/
			 record.setAttribute("communicationLog", "communicationLog.png");
			 
			 if(contract.getFollowUpDate()!=null){
				 record.setAttribute("followupDate", contract.getFollowUpDate());
			 }else{
				 record.setAttribute("followupDate", "");
			 }
			
			 if(contract.getStartDate()!=null){
				 record.setAttribute("contractStartDate", contract.getStartDate());
			 }else{
				 record.setAttribute("contractStartDate", "");
			 }
			 if(contract.getEndDate()!=null){
				 record.setAttribute("contractEndDate", contract.getEndDate());
			 }else{
				 record.setAttribute("contractEndDate", "");
			 }
			 record.setAttribute("creationDate",contract.getCreationDate());
			 /**
			  * nidhi
			  * 7-06-2018
			  * for orien requirment
			  */
			 if(contract.getContractPeriod()!=null){
				 record.setAttribute("salesSource", contract.getContractPeriod());
			 }else{
				 record.setAttribute("salesSource", "");
			 }
			 /**
			  * end
			  */
//			 record.setAttribute("priority", contract.getPriority());
			 record.setAttribute("customerId", contract.getCinfo().getCount());
			 record.setAttribute("customerName",contract.getCinfo().getFullName());
			 record.setAttribute("customerCell", contract.getCinfo().getCellNumber());
			 record.setAttribute("salesPerson", contract.getEmployee());
			 record.setAttribute("category", contract.getCategory());
			 record.setAttribute("type", contract.getType());
			 record.setAttribute("group", contract.getGroup());
			 record.setAttribute("branch", contract.getBranch());
			 record.setAttribute("status", contract.getStatus());
			 
			 if(contract.getQuotationCount()!=0&&contract.getQuotationCount()!=-1){
				 record.setAttribute("quotationId", contract.getQuotationCount());
			 }else{
				 record.setAttribute("quotationId", "");
			 }
			 records[i]=record;
		 }
		 System.out.println("RECORDS LENGTH : "+records.length);
		 
		 return records;
	}
	 
	 /**
		 * Toggles the app header bar menus as per screen state
		 */
		public void toggleAppHeaderBarMenu() {
			if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.equals("Download")||text.equals("Search"))
					{
						menus[k].setVisible(true); 
					}
					else
						menus[k].setVisible(false);  		  		
				}
			}
			
			else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.equals("Download"))
						menus[k].setVisible(true); 
					else
						menus[k].setVisible(false);  		   
				}
			}
			
			else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.equals("Download"))
						menus[k].setVisible(true); 
					else
						menus[k].setVisible(false);  		   
				}
			}
			AuthorizationHelper.setAsPerAuthorization(Screen.CONTRACTDASHBOARD,LoginPresenter.currentModule.trim());
		}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
	
	public void showMessage(String msg){
		Window alert=new Window();
		
		alert.setWidth("25%");
        alert.setHeight("20%"); 
        alert.setTitle("Alert");
        alert.setShowMinimizeButton(false);
        alert.setIsModal(true);
        alert.setShowModalMask(true);
        alert.centerInPage();
        alert.setMargin(4);
        
//		InlineLabel msgLbl=new InlineLabel(msg);
//		alert.addItem(msgLbl);
		
//		window.setWidth(200);
//		window.setHeight(200);
//		window.setCanDragResize(true);
//		window.setShowFooter(true);

		Label label = new Label();
		label.setContents(msg);
		label.setAlign(Alignment.CENTER);
		label.setPadding(5);
		label.setHeight100();
		
		alert.addItem(label);
		
		
		alert.show();
	}
	
	
	

	public ListGrid getGrid() {
		return grid;
	}

	public void setGrid(ListGrid grid) {
		this.grid = grid;
	}

	public DataSource getDs() {
		return ds;
	}

	public void setDs(DataSource ds) {
		this.ds = ds;
	}

	public ObjectListBox<Branch> getOlbbranch() {
		return olbbranch;
	}

	public void setOlbbranch(ObjectListBox<Branch> olbbranch) {
		this.olbbranch = olbbranch;
	}

	public ObjectListBox<Employee> getOlbsalesPerson() {
		return olbsalesPerson;
	}

	public void setOlbsalesPerson(ObjectListBox<Employee> olbsalesPerson) {
		this.olbsalesPerson = olbsalesPerson;
	}

	public DateBox getDbFromDate() {
		return dbFromDate;
	}

	public void setDbFromDate(DateBox dbFromDate) {
		this.dbFromDate = dbFromDate;
	}

	public DateBox getDbToDate() {
		return dbToDate;
	}

	public void setDbToDate(DateBox dbToDate) {
		this.dbToDate = dbToDate;
	}

	public Button getBtnGo() {
		return btnGo;
	}

	public void setBtnGo(Button btnGo) {
		this.btnGo = btnGo;
	}

	public FlexForm getForm() {
		return form;
	}

	public void setForm(FlexForm form) {
		this.form = form;
	}

	public HorizontalPanel getHorizontalpanel() {
		return horizontalpanel;
	}

	public void setHorizontalpanel(HorizontalPanel horizontalpanel) {
		this.horizontalpanel = horizontalpanel;
	}

	public VerticalPanel getVerticalpanel() {
		return verticalpanel;
	}

	public void setVerticalpanel(VerticalPanel verticalpanel) {
		this.verticalpanel = verticalpanel;
	}

	public DateBox getDbFromFollowupDate() {
		return dbFromFollowupDate;
	}

	public void setDbFromFollowupDate(DateBox dbFromFollowupDate) {
		this.dbFromFollowupDate = dbFromFollowupDate;
	}

	public DateBox getDbToFollowupDate() {
		return dbToFollowupDate;
	}

	public void setDbToFollowupDate(DateBox dbToFollowupDate) {
		this.dbToFollowupDate = dbToFollowupDate;
	}
	
	
	
	
	

}
