package com.slicktechnologies.client.views.dashboard.contractdashboard;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CommunicationLogService;
import com.slicktechnologies.client.services.CommunicationLogServiceAsync;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.contract.ContractForm;
import com.slicktechnologies.client.views.contract.ContractPresenter;
import com.slicktechnologies.client.views.contract.SalesLineItemTable;
import com.slicktechnologies.client.views.dashboard.leaddashboard.CommunicationSmartPoup;
import com.slicktechnologies.client.views.dashboard.leaddashboard.LeadDashboardSearchForm;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;

public class ContractDashboardPresenter implements ClickHandler, CellClickHandler {

	ContractDashboardForm form;
	LeadDashboardSearchForm searchForm;
	
	GenricServiceAsync service=GWT.create(GenricService.class);
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	ArrayList<Contract> globalContractList=new ArrayList<Contract>();

	CommunicationSmartPoup commSmrtPopup=new CommunicationSmartPoup();
	CommunicationLogServiceAsync communicationService = GWT.create(CommunicationLogService.class);
	
	Contract contractEntity=null;
	ListGridRecord selectedRecord=null;
	int position;
	
	protected AppMemory mem;
	/**
	 * Updated By: Viraj
	 * Date: 17-01-2019
	 * Description: To fill data of Contract in popup
	 */
	public static PopupPanel docLeadPanel = null;
	VerticalPanel docVerticalPanel = null;
	FlowPanel docflowpanel = null;
	VerticalPanel mainDocVerticalPanel = null;
	/** Ends **/
	
	public ContractDashboardPresenter(ContractDashboardForm homeForm,LeadDashboardSearchForm searchpopup) {
		this.form=homeForm;
		this.searchForm=searchpopup;
		form.grid.addCellClickHandler(this);
		mem=AppMemory.getAppMemory();
		setEventHandeling();
		
		
		/**@author Abhinav Bihade
		 * @since 17/12/2019
		 * Vaishnavi Pawar created this task- Got call from  Reshma Borivali from Ankita Pest Control- 
		 * needs by default contract loading to be stopped in contract dashboard
		 *
		 */
		if(!AppUtility.checkForProcessConfigurartionIsActiveOrNot( "Contract", "DisableDefaultContractLoading" )){
			retrieveContractData();
		}
		
		
		/**
	     * Updated By: Viraj
	     * Date: 17-01-2019
	     * Description: To fill data of Contract in popup
	     */
	    docLeadPanel = new PopupPanel(true);
	    docflowpanel = new FlowPanel();
	    mainDocVerticalPanel = new VerticalPanel();
	    
	    mainDocVerticalPanel.add(docflowpanel);
		
		docVerticalPanel = new VerticalPanel();
		mainDocVerticalPanel.add(docVerticalPanel);
				
		docLeadPanel.add(mainDocVerticalPanel);
		/** Ends **/
	}
	
	private void setEventHandeling() {
		form.getBtnGo().addClickHandler(this);
		searchForm.golbl.addClickHandler(this);
		
		commSmrtPopup.getBtnOk().addClickHandler(this);
		commSmrtPopup.getBtnCancel().addClickHandler(this);
		commSmrtPopup.getBtnAdd().addClickHandler(this);
		
		InlineLabel label[] = AppMemory.getAppMemory().skeleton.getMenuLabels();
		for (int k = 0; k < label.length; k++) {
			if (AppMemory.getAppMemory().skeleton.registration[k] != null)
				AppMemory.getAppMemory().skeleton.registration[k].removeHandler();
		}
		for (int k = 0; k < label.length; k++) {
			AppMemory.getAppMemory().skeleton.registration[k] = label[k].addClickHandler(this);
		}
	}

	public static void initialize(){
		System.out.println("INSIDE CHART INITIALIZATION");
		ContractDashboardForm homeForm = new ContractDashboardForm();
		AppMemory.getAppMemory().currentScreen=Screen.HOME;
		AppMemory.getAppMemory().skeleton.getProcessName().setText(AppConstants.SERVICEMODULE+"/"+"Contract Dashboard");
		
		LeadDashboardSearchForm searchForm=new LeadDashboardSearchForm();
		ContractDashboardPresenter presenter= new ContractDashboardPresenter(homeForm,searchForm);
		
		AppMemory.getAppMemory().stickPnel(homeForm);	
	}
	
	public void retrieveContractData(){
		Date date=new Date();
		System.out.println("Today Date ::::::::: "+date);
		CalendarUtil.addDaysToDate(date,7);
	    System.out.println("Changed Date + 7 ::::::::: "+date);
		
		Vector<Filter> filtervec=null;
		Filter filter = null;
		MyQuerry querry;
		filtervec=new Vector<Filter>();
		
		List<String> statusList=new ArrayList<String>();
		statusList=new ArrayList<String>();
		statusList.add(Contract.CREATED);
		statusList.add(Contract.REQUESTED);
		
		List<String> branchList ;
		branchList= new ArrayList<String>();
		for (int i = 0; i < LoginPresenter.globalBranch.size(); i++) {
			branchList.add(LoginPresenter.globalBranch.get(i).getBusinessUnitName());
		}
		
		filter = new Filter();
		filter.setQuerryString("status IN");
		filter.setList(statusList);
		filtervec.add(filter);

		filter = new Filter();
		filter.setQuerryString("branch IN");
		filter.setList(branchList);
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("startDate <=");
		filter.setDateValue(date);
		filtervec.add(filter);
		
		MyQuerry querry1=new MyQuerry();
		querry1.setQuerryObject(new Contract());
		querry1.setFilters(filtervec);
//		retriveTable(querry);
		
		
		filtervec=new Vector<Filter>();
		
		filter = new Filter();
		filter.setQuerryString("status");
		filter.setStringValue(Contract.APPROVED);
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("customerInterestFlag");
		filter.setBooleanvalue(false);
		filtervec.add(filter);

		filter = new Filter();
		filter.setQuerryString("branch IN");
		filter.setList(branchList);
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("endDate <=");
		filter.setDateValue(date);
		filtervec.add(filter);
		
		MyQuerry querry2=new MyQuerry();
		querry2.setQuerryObject(new Contract());
		querry2.setFilters(filtervec);
		retriveTable(querry1,querry2);
		
	}
	
	
	public void retriveTable(MyQuerry querry){
		service.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				globalContractList=new ArrayList<>();
				
				if(result.size()==0){
					SC.say("No result found.");
				}
				for(SuperModel model:result){
					Contract lead=(Contract) model;
					globalContractList.add(lead);
				}
				form.setDataToListGrid(globalContractList);
			}
			@Override
			public void onFailure(Throwable caught) {
				caught.printStackTrace();
			}
		}); 
	 }
	
	public void retriveTable(MyQuerry querry1,final MyQuerry querry2){
		service.getSearchResult(querry1,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				globalContractList=new ArrayList<>();
				for(SuperModel model:result){
					Contract lead=(Contract) model;
					globalContractList.add(lead);
				}
				service.getSearchResult(querry2, new AsyncCallback<ArrayList<SuperModel>>() {

					@Override
					public void onFailure(Throwable caught) {
						
					}
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						for(SuperModel model:result){
							Contract lead=(Contract) model;
							globalContractList.add(lead);
						}
						if(globalContractList.size()==0){
							SC.say("No result found.");
						}
						form.setDataToListGrid(globalContractList);
					}
				});
				
			}
			@Override
			public void onFailure(Throwable caught) {
				caught.printStackTrace();
			}
		}); 
	 }
	

	@Override
	public void onClick(ClickEvent event) {
		System.out.println("CLICK EVENT..!!!");
		if(event.getSource() instanceof InlineLabel){
			InlineLabel lbl= (InlineLabel) event.getSource();
			if(lbl.getText().equals("Download")){
				reactOnDownload();
			}
			if(lbl.getText().equals("Search")){
				System.out.println("ON CLICK OF SEARCH..!!!");
				searchForm.showPopUp();
			}
			if(lbl.getText().equals("Go")){
				reactOnGo();
			}
		}
		
		if (event.getSource().equals(form.getBtnGo())) {
			System.out.println("BTN GO CLICK");
			if(LoginPresenter.branchRestrictionFlag==true&&form.getOlbbranch().getSelectedIndex()==0){
				SC.say("Please select branch.");
				return;
			}
			System.out.println("FRM DT "+form.getDbFromDate().getValue());
			if (form.getOlbbranch().getSelectedIndex() == 0
					&& form.getOlbsalesPerson().getSelectedIndex() == 0
					&& form.getDbFromDate().getValue()==null
					&& form.getDbToDate().getValue()==null
					&& form.getDbFromFollowupDate().getValue()==null
					&& form.getDbToFollowupDate().getValue()==null) {
				SC.say("Please select atleast one filter.");
				return;
			}else {

				if (form.getOlbsalesPerson().getSelectedIndex() != 0
						&& form.getOlbbranch().getSelectedIndex() == 0
						&& (form.getDbFromDate().getValue() == null && form.getDbToDate() != null)) {

					searchByFromToDate();

				} else if (form.getOlbsalesPerson().getSelectedIndex() == 0
						&& form.getOlbbranch().getSelectedIndex() != 0
						&& (form.getDbFromDate().getValue() == null && form.getDbToDate() != null)) {

					searchByFromToDate();
				} else if (form.getOlbsalesPerson().getSelectedIndex() != 0
						&& form.getOlbbranch().getSelectedIndex() != 0
						&& (form.getDbFromDate().getValue() == null && form.getDbToDate() != null)) {

					searchByFromToDate();
				}else if(form.getDbFromDate().getValue() != null&& form.getDbToDate() != null){
					  
					//SC.say("Select From and To Date.");
					
					//if (form.getDbFromDate().getValue() != null&& form.getDbToDate() != null) {
						Date formDate = form.getDbFromDate().getValue();
						Date toDate = form.getDbToDate().getValue();
						
						/**
						 * @author Vijay Chougule Date 06-08-2020
						 * Des :- Updated validation to search for 1 month as per pecopp requirement standard for all
						 */
						if (toDate.equals(formDate) || toDate.after(formDate)) {
							CalendarUtil.addDaysToDate(formDate, 31);
							if (toDate.before(formDate)) {
								searchByFromToDate();
							} else {
								SC.say("From Date and To Date Difference Can Not Be More Than 1 Month.");
							}
						} else {
							SC.say("To Date should be greater than From Date.");
						}
					//}
					
				}
				else {

//					if(form.getDbFromFollowupDate().getValue() == null|| form.getDbToFollowupDate().getValue() == null) {
//						
//						SC.say("Select Follow up From and To Date.");
//					}
					if (form.getDbFromFollowupDate().getValue() != null&& form.getDbToFollowupDate() != null) {
						Date formDate = form.getDbFromFollowupDate().getValue();
						Date toDate = form.getDbToFollowupDate().getValue();
						
						/**
						 * @author Vijay Chougule Date 06-08-2020
						 * Des :- Updated validation to search for 1 month as per pecopp requirement standard for all
						 */
						if (toDate.equals(formDate) || toDate.after(formDate)) {
							CalendarUtil.addDaysToDate(formDate, 31);
							if (toDate.before(formDate)) {
								searchByFromToDate();
							} else {
								SC.say("From Date and To Date Difference Can Not Be More Than 1 Month.");
							}
						} else {
							SC.say("To Date should be greater than From Date.");
						}
					}
					
				}
			}
		}
		
		
		
		if (event.getSource() == commSmrtPopup.getBtnOk()) {
//			glassPanel.show();
			List<InteractionType> list = commSmrtPopup.getCommunicationLogTable().getDataprovider().getList();
			ArrayList<InteractionType> interactionlist = new ArrayList<InteractionType>();
			interactionlist.addAll(list);

			boolean checkNewInteraction = AppUtility.checkNewInteractionAdded(interactionlist);

			if (checkNewInteraction == false) {
				SC.say("Please add new interaction details");
//				glassPanel.hide();
			} else {
				final Date followUpDate = interactionlist.get(interactionlist.size() - 1).getInteractionDueDate();
				communicationService.saveCommunicationLog(interactionlist,new AsyncCallback<Void>() {
					@Override
					public void onFailure(Throwable caught) {
						SC.say("Unexpected Error");
//						glassPanel.hide();
//						LoginPresenter.communicationLogPanel.hide();
					}

					@Override
					public void onSuccess(Void result) {
						commSmrtPopup.hidePopup();
						SC.say("Data Save Successfully");
//						List<Lead> list = getDataprovider().getList();
//						list.get(rowIndex).setFollowUpDate(followUpDate);
//						Lead leadObj = list.get(rowIndex);
						contractEntity.setFollowUpDate(followUpDate);
						selectedRecord.setAttribute("followupDate", AppUtility.parseDate(followUpDate));
     					saveContract(contractEntity);
						form.grid.getDataSource().updateData(selectedRecord);
						form.grid.redraw();
//						followUpFlag = false;
//						table.redraw();
//						glassPanel.hide();
//						communicationPanel.hide();
					}
				});
			}

		}
		if (event.getSource() == commSmrtPopup.getBtnCancel()) {
//			followUpFlag = false;
//			communicationPanel.hide();
			
			commSmrtPopup.hidePopup();
		}
		if (event.getSource() == commSmrtPopup.getBtnAdd()) {
//			glassPanel.show();
			String remark = commSmrtPopup.getRemark().getValue();
			Date dueDate = commSmrtPopup.getDueDate().getValue();
			String interactiongGroup = null;
			if (commSmrtPopup.getOblinteractionGroup().getSelectedIndex() != 0)
				interactiongGroup = commSmrtPopup.getOblinteractionGroup().getValue(commSmrtPopup.getOblinteractionGroup().getSelectedIndex());
				boolean validationFlag = AppUtility.validateCommunicationlogSmartGwt(remark, dueDate);

				List<InteractionType> list = commSmrtPopup.getCommunicationLogTable().getDataprovider().getList();
			
				Comparator<InteractionType> comp = new Comparator<InteractionType>() {
					@Override
					public int compare(InteractionType e1, InteractionType e2) {
						if (e1.getCount() == e2.getCount()) {
							return 0;
						}
						if (e1.getCount() > e2.getCount()) {
							return -1;
						} else {
							return 1;
						}
					}
				};
				Collections.sort(list, comp);
	
				if (validationFlag) {
					InteractionType communicationLog = AppUtility.getCommunicationLog(AppConstants.SERVICEMODULE,
									AppConstants.CONTRACT, contractEntity.getCount(),contractEntity.getEmployee(), remark, dueDate,
									contractEntity.getCinfo(), null, interactiongGroup,contractEntity.getBranch(),"");
					list.add(communicationLog);
					commSmrtPopup.getCommunicationLogTable().getDataprovider().setList(list);
					commSmrtPopup.getRemark().setValue("");
					commSmrtPopup.getDueDate().setValue(null);
					commSmrtPopup.getOblinteractionGroup().setSelectedIndex(0);
				}
//				glassPanel.hide();
		}
		
	}
	
	private void saveContract(Contract leadObj) {
		service.save(leadObj, new AsyncCallback<ReturnFromServer>() {
			@Override
			public void onFailure(Throwable caught) {
//				glassPanel.hide();
				SC.say("An Unexpected error occurred!");
			}
			@Override
			public void onSuccess(ReturnFromServer result) {
//				glassPanel.hide();
				SC.say("Saved successfully!");
			}
		});

	}
	
	private void searchByFromToDate() {
		System.out.println("FROM AND DATE ---");
		Long compId = UserConfiguration.getCompanyId();
		Vector<Filter> filtervec = null;
		Filter filter = null;
		MyQuerry querry;

		filtervec = new Vector<Filter>();

		
		List<String> statusList=new ArrayList<String>();
		filtervec=new Vector<Filter>();
		statusList=new ArrayList<String>();
		statusList.add(Contract.CREATED);
		statusList.add(Contract.REQUESTED);
		statusList.add(Contract.APPROVED);
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(compId);
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("status IN");
		filter.setList(statusList);
		filtervec.add(filter);
		
		if (form.getDbFromDate().getValue() != null) {
			filter = new Filter();
//			filter.setQuerryString("followUpDate >=");
			filter.setQuerryString("creationDate >="); //Ashwini Patil Date:26-04-2022 commented above line and added creationDate filter
			filter.setDateValue(form.getDbFromDate().getValue());
			filtervec.add(filter);
		}

		if (form.getDbToDate().getValue() != null) {

			filter = new Filter();
//			filter.setQuerryString("followUpDate <=");
			filter.setQuerryString("creationDate <="); //Ashwini Patil Date:26-04-2022 commented above line and added creationDate filter
			filter.setDateValue(form.getDbToDate().getValue());
			filtervec.add(filter);
		}
		
		if (form.getDbFromFollowupDate().getValue() != null) {
			filter = new Filter();
			filter.setQuerryString("followUpDate >=");
			filter.setDateValue(form.getDbFromFollowupDate().getValue());
			filtervec.add(filter);
		}

		if (form.getDbToFollowupDate().getValue() != null) {

			filter = new Filter();
			filter.setQuerryString("followUpDate <=");
			filter.setDateValue(form.getDbToFollowupDate().getValue());
			filtervec.add(filter);
		}

		if (form.getOlbbranch().getSelectedIndex() != 0) {
			filter = new Filter();
			filter.setQuerryString("branch");
			filter.setStringValue(form.getOlbbranch().getValue());
			filtervec.add(filter);

		}

		if (form.getOlbsalesPerson().getSelectedIndex() != 0) {
			filter = new Filter();
			filter.setQuerryString("employee");
			filter.setStringValue(form.getOlbsalesPerson().getValue());
			filtervec.add(filter);
		}

		querry = new MyQuerry();
		querry.setQuerryObject(new Contract());
		querry.setFilters(filtervec);
		this.retriveTable(querry);

	}
	
	
	public void reactOnGo() {

		if (!searchForm.personInfo.getId().getValue().equals("")
				&& !searchForm.personInfo.getName().getValue().equals("")
				&& !searchForm.personInfo.getPhone().getValue().equals("")) {
			searchForm.hidePopUp();
			MyQuerry querry = createContractFilter();
			retriveTable(querry);
			
		}

		if (searchForm.personInfo.getId().getValue().equals("")
				&& searchForm.personInfo.getName().getValue().equals("")
				&& searchForm.personInfo.getPhone().getValue().equals("")) {
			SC.say("Please enter customer information");
		}

	}

	private MyQuerry createContractFilter() {
		Vector<Filter> filterVec = new Vector<Filter>();
		Filter temp;

		if (searchForm.personInfo.getId().getText().trim().equals("") == false) {
			temp = new Filter();
			temp.setLongValue(Long.parseLong(searchForm.personInfo.getId().getValue()));
			temp.setQuerryString("cinfo.count");
			filterVec.add(temp);
		}

		if ((searchForm.personInfo.getName().getText()).trim().equals("") == false) {
			temp = new Filter();
			temp.setStringValue(searchForm.personInfo.getName().getValue());
			temp.setQuerryString("cinfo.fullName");
			filterVec.add(temp);
		}

		if (searchForm.personInfo.getPhone().getText().trim().equals("") == false) {
			temp = new Filter();
			temp.setLongValue(Long.parseLong(searchForm.personInfo.getPhone().getValue()));
			temp.setQuerryString("cinfo.cellNumber");
			filterVec.add(temp);
		}

		MyQuerry lead = new MyQuerry();
		lead.setQuerryObject(new Contract());
		lead.setFilters(filterVec);

		return lead;
	}

	private void reactOnDownload() {
		ArrayList<Contract> contractList = new ArrayList<Contract>();
		ListGridRecord[] allRecords = form.grid.getRecords();
        if (allRecords != null) {
        	List<ListGridRecord> allRecordsList = Arrays.asList(allRecords);
	   		for(int i=0;i<allRecordsList.size();i++){
	   			 final Contract model=getContractDetails(Integer.parseInt(allRecordsList.get(i).getAttribute("contractId").trim()));
	   			 if(model!=null){
	   				contractList.add(model);
	   			 }
	   		}
        }
		
		csvservice.setcontractlist(contractList, new AsyncCallback<Void>(){
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);
			}
			
			@Override
			public void onSuccess(Void result) {
				String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "ContractReport")){
					final String url = gwt + "csvservlet" + "?type=" + 127;
					Window.open(url, "test", "enabled");
				}else{
					final String url = gwt + "csvservlet" + "?type=" + 6;
					Window.open(url, "test", "enabled");
				}
			}
		});
	}

	private Contract getContractDetails(int leadId) {
		for(Contract object:globalContractList){
			if(object.getCount()==leadId){
				return object;
			}
		}
		return null;
	}

	@Override
	public void onCellClick(CellClickEvent event) {
		
		ListGridRecord record = event.getRecord();
		int colNum = event.getColNum();
		ListGridField field = form.grid.getField(colNum);
		String fieldName = form.grid.getFieldName(colNum);
		String fieldTitle = field.getTitle();
		position=event.getRowNum();
		System.out.println("Clicked " + fieldTitle + ":"+ record.getAttribute(fieldName) + "  Contract Id :: "+ record.getAttribute("contrcatId")+"POSITION "+position);
		
//		if (fieldName.equals("followupButton")) {
		//	selectedRecord = record;
			contractEntity = getContractDetails((Integer.parseInt(record.getAttribute("contractId").trim())));
		//	mapToContractScreen();
			
//			getLogDetails(contractEntity);
//		}
			/** date 7.1.2019 added by komal to show communication popup in contract form **/
			if (fieldName.equals("communicationLog")) {
				selectedRecord = record;
				getLogDetails(contractEntity);
			}else{
				System.out.println("Inside else of oncellclick");
				mapToContractScreen();
			}
	}
	
	public void mapToContractScreen(){
		/**
		 * Updated By:Viraj
		 * Date: 17-01-2019
		 * Description: To display Contract in popup
		 */
		System.out.println("Inside map to contract screen");
		 removePopObjects();
		 
//		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Contract", Screen.CONTRACT);
		final Contract entity = contractEntity;
		 SalesLineItemTable.newModeFlag = true;
		final ContractForm form = new ContractForm();
//		final ContractForm form = ContractPresenter.initalize();
		form.content.getElement().getStyle().setHeight(570, Unit.PX);
		form.content.getElement().getStyle().setWidth(950, Unit.PX);
		docVerticalPanel.add(form.content);
		docLeadPanel.setHeight("600px");
		docLeadPanel.setStyleName("gwt-SuggestBoxPopup");
		docLeadPanel.show();
		docLeadPanel.center();
		
		form.showWaitSymbol();
//		AppMemory.getAppMemory().stickPnel(form);
		Timer timer = new Timer() {
			@Override
			public void run() {
				form.hideWaitSymbol();
				form.getPersonInfoComposite().setEnable(false);
				form.updateView(entity);
				form.setProcessLevelBar(null);
				form.setToViewState();
			}
			/** Ends **/
		};
		timer.schedule(5000);
	}
	
	
	private void getLogDetails(Contract contract) {
		
		MyQuerry querry = AppUtility.communicationLogQuerry(contract.getCount(),contract.getCompanyId(), AppConstants.SERVICEMODULE,AppConstants.CONTRACT);
		service.getSearchResult(querry,	new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				ArrayList<InteractionType> list = new ArrayList<InteractionType>();
				for (SuperModel model : result) {
					InteractionType interactionType = (InteractionType) model;
					list.add(interactionType);
				}
				Comparator<InteractionType> comp = new Comparator<InteractionType>() {
					@Override
					public int compare(InteractionType e1,InteractionType e2) {
						if (e1.getCount() == e2.getCount()) {
							return 0;
						}
						if (e1.getCount() > e2.getCount()) {
							return -1;
						} else {
							return 1;
						}
					}
				};
				Collections.sort(list, comp);
				
				commSmrtPopup.getRemark().setValue("");
				commSmrtPopup.getDueDate().setValue(null);
				commSmrtPopup.getOblinteractionGroup().setSelectedIndex(0);
				commSmrtPopup.getCommunicationLogTable().getDataprovider().setList(list);
				commSmrtPopup.showPopup();
			}

			@Override
			public void onFailure(Throwable caught) {
			}
		});

	}
	
	/**
	 * Updated By: Viraj
	 * Date: 17-01-2019
	 * Description: To remove items added in popup
	 */
	 void removePopObjects() {
			try {

				for (Widget wi : docVerticalPanel) {
					boolean flag = docLeadPanel.remove(wi);
					wi.removeFromParent();
					System.out.println("get item remove status " + flag);
				}

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}
	/** Ends **/

}
