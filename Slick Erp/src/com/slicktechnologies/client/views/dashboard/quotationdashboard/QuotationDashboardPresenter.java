package com.slicktechnologies.client.views.dashboard.quotationdashboard;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CommunicationLogService;
import com.slicktechnologies.client.services.CommunicationLogServiceAsync;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.dashboard.leaddashboard.CommunicationSmartPoup;
import com.slicktechnologies.client.views.dashboard.leaddashboard.LeadDashboardSearchForm;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.client.views.quotation.QuotationForm;
import com.slicktechnologies.client.views.quotation.QuotationPresenter;
import com.slicktechnologies.client.views.quotation.SalesLineItemQuotationTable;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;

public class QuotationDashboardPresenter implements ClickHandler, CellClickHandler {

	QuotationDashboardForm form;
	LeadDashboardSearchForm searchForm;
	
	GenricServiceAsync service=GWT.create(GenricService.class);
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	ArrayList<Quotation> globalQuotationoList=new ArrayList<Quotation>();

	CommunicationSmartPoup commSmrtPopup=new CommunicationSmartPoup();
	CommunicationLogServiceAsync communicationService = GWT.create(CommunicationLogService.class);
	
	Quotation quotationEntity=null;
	ListGridRecord selectedRecord=null;
	int position;
	
	protected AppMemory mem;
	
	/**
	 * Updated By: Viraj
	 * Date: 17-01-2019
	 * Description: To fill data of quotation in popup
	 */
	public static PopupPanel docLeadPanel = null;
	VerticalPanel docVerticalPanel = null;
	FlowPanel docflowpanel = null;
	VerticalPanel mainDocVerticalPanel = null;
	/** Ends **/
	
	GeneralViewDocumentPopup generalviewpopup = new GeneralViewDocumentPopup();


	
	public QuotationDashboardPresenter(QuotationDashboardForm homeForm,LeadDashboardSearchForm searchpopup) {
		this.form=homeForm;
		this.searchForm=searchpopup;
		form.grid.addCellClickHandler(this);
		mem=AppMemory.getAppMemory();
		setEventHandeling();
//		retrieveQuotationData(); //Ashwini Patil Date:23-08-2024 to stop auto loading
		
		/**
	     * Updated By: Viraj
	     * Date: 17-01-2019
	     * Description: To fill data of quotation in popup
	     */
	    docLeadPanel = new PopupPanel(true);
	    docflowpanel = new FlowPanel();
	    mainDocVerticalPanel = new VerticalPanel();
	    
	    mainDocVerticalPanel.add(docflowpanel);
		
		docVerticalPanel = new VerticalPanel();
		mainDocVerticalPanel.add(docVerticalPanel);
				
		docLeadPanel.add(mainDocVerticalPanel);
		/** Ends **/
		
		generalviewpopup.getBtnClose().addClickHandler(this);

	}
	
	private void setEventHandeling() {
		form.getBtnGo().addClickHandler(this);
		searchForm.golbl.addClickHandler(this);
		
		commSmrtPopup.getBtnOk().addClickHandler(this);
		commSmrtPopup.getBtnCancel().addClickHandler(this);
		commSmrtPopup.getBtnAdd().addClickHandler(this);
		
		InlineLabel label[] = AppMemory.getAppMemory().skeleton.getMenuLabels();
		for (int k = 0; k < label.length; k++) {
			if (AppMemory.getAppMemory().skeleton.registration[k] != null)
				AppMemory.getAppMemory().skeleton.registration[k].removeHandler();
		}
		for (int k = 0; k < label.length; k++) {
			AppMemory.getAppMemory().skeleton.registration[k] = label[k].addClickHandler(this);
		}
	}

	public static void initialize(){
		System.out.println("INSIDE CHART INITIALIZATION");
		QuotationDashboardForm homeForm = new QuotationDashboardForm();
		AppMemory.getAppMemory().currentScreen=Screen.HOME;
		AppMemory.getAppMemory().skeleton.getProcessName().setText(AppConstants.SERVICEMODULE+"/"+"Quotation Dashboard");
		
		LeadDashboardSearchForm searchForm=new LeadDashboardSearchForm();
		QuotationDashboardPresenter presenter= new QuotationDashboardPresenter(homeForm,searchForm);
		
		AppMemory.getAppMemory().stickPnel(homeForm);	
	}
	
	public void retrieveQuotationData(){
		Date date=new Date();
		System.out.println("Today Date ::::::::: "+date);
		CalendarUtil.addDaysToDate(date,7);
	    System.out.println("Changed Date + 7 ::::::::: "+date);
		
		Vector<Filter> filtervec=null;
		Filter filter = null;
		MyQuerry querry;
		filtervec=new Vector<Filter>();
		List<String>statusList=new ArrayList<String>();
		
		statusList.add(Quotation.CREATED);
		statusList.add(Quotation.REQUESTED);
		statusList.add(Quotation.APPROVED);
		
		List<String> branchList ;
		branchList= new ArrayList<String>();
		for (int i = 0; i < LoginPresenter.globalBranch.size(); i++) {
			branchList.add(LoginPresenter.globalBranch.get(i).getBusinessUnitName());
		}
		
		filter = new Filter();
		filter.setQuerryString("status IN");
		filter.setList(statusList);
		filtervec.add(filter);

		filter = new Filter();
		filter.setQuerryString("branch IN");
		filter.setList(branchList);
		filtervec.add(filter);

		filter = new Filter();
		filter.setQuerryString("followUpDate <=");
		filter.setDateValue(date);
		filtervec.add(filter);
		
	    querry=new MyQuerry();
		querry.setQuerryObject(new Quotation());
		querry.setFilters(filtervec);
		retriveTable(querry);
		
	}
	
	
	public void retriveTable(MyQuerry querry){
		service.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				globalQuotationoList=new ArrayList<>();
				
				if(result.size()==0){
					SC.say("No result found.");
				}
				for(SuperModel model:result){
					Quotation entity=(Quotation) model;
					globalQuotationoList.add(entity);
				}
				form.setDataToListGrid(globalQuotationoList);
			}
			@Override
			public void onFailure(Throwable caught) {
				caught.printStackTrace();
			}
		}); 
	 }
	

	@Override
	public void onClick(ClickEvent event) {
		System.out.println("CLICK EVENT..!!!");
		if(event.getSource() instanceof InlineLabel){
			InlineLabel lbl= (InlineLabel) event.getSource();
			if(lbl.getText().equals("Download")){
				reactOnDownload();
			}
			if(lbl.getText().equals("Search")){
				System.out.println("ON CLICK OF SEARCH..!!!");
				searchForm.showPopUp();
			}
			if(lbl.getText().equals("Go")){
				reactOnGo();
			}
		}
		
		if (event.getSource().equals(form.getBtnGo())) {
			System.out.println("BTN GO CLICK");
			if(LoginPresenter.branchRestrictionFlag==true&&form.getOlbbranch().getSelectedIndex()==0){
				SC.say("Please select branch.");
				return;
			}
			System.out.println("FRM DT "+form.getDbFromDate().getValue());
			if (form.getOlbbranch().getSelectedIndex() == 0
					&& form.getOlbsalesPerson().getSelectedIndex() == 0
					&& form.getDbFromDate().getValue()==null
					&& form.getDbToDate().getValue()==null) {
				SC.say("Please select atleast one filter.");
				return;
			}else {

				if (form.getOlbsalesPerson().getSelectedIndex() != 0
						&& form.getOlbbranch().getSelectedIndex() == 0
						&& (form.getDbFromDate().getValue() == null && form.getDbToDate() != null)) {

					searchByFromToDate();

				} else if (form.getOlbsalesPerson().getSelectedIndex() == 0
						&& form.getOlbbranch().getSelectedIndex() != 0
						&& (form.getDbFromDate().getValue() == null && form.getDbToDate() != null)) {

					searchByFromToDate();
				} else if (form.getOlbsalesPerson().getSelectedIndex() != 0
						&& form.getOlbbranch().getSelectedIndex() != 0
						&& (form.getDbFromDate().getValue() == null && form.getDbToDate() != null)) {

					searchByFromToDate();
				} else {
					if (form.getDbFromDate().getValue() == null
							|| form.getDbToDate().getValue() == null) {
						
						SC.say("Select From and To Date.");
					}
					if (form.getDbFromDate().getValue() != null&& form.getDbToDate() != null) {
						Date formDate = form.getDbFromDate().getValue();
						Date toDate = form.getDbToDate().getValue();
						/**
						 * @author Vijay Chougule Date 06-08-2020
						 * Des :- Updated validation to search for 1 month as per pecopp requirement standard for all
						 */
						if (toDate.equals(formDate) || toDate.after(formDate)) {
							CalendarUtil.addDaysToDate(formDate, 31);
							if (toDate.before(formDate)) {
								searchByFromToDate();
							} else {
								SC.say("From Date and To Date Difference Can Not Be More Than 1 Month.");
							}
						} else {
							SC.say("To Date should be greater than From Date.");
						}
					}
				}
			}
		}
		
		
		
		if (event.getSource() == commSmrtPopup.getBtnOk()) {
//			glassPanel.show();
			List<InteractionType> list = commSmrtPopup.getCommunicationLogTable().getDataprovider().getList();
			ArrayList<InteractionType> interactionlist = new ArrayList<InteractionType>();
			interactionlist.addAll(list);

			boolean checkNewInteraction = AppUtility.checkNewInteractionAdded(interactionlist);

			if (checkNewInteraction == false) {
				SC.say("Please add new interaction details");
//				glassPanel.hide();
			} else {
				final Date followUpDate = interactionlist.get(interactionlist.size() - 1).getInteractionDueDate();
				communicationService.saveCommunicationLog(interactionlist,new AsyncCallback<Void>() {
					@Override
					public void onFailure(Throwable caught) {
						SC.say("Unexpected Error");
//						glassPanel.hide();
//						LoginPresenter.communicationLogPanel.hide();
					}

					@Override
					public void onSuccess(Void result) {
						commSmrtPopup.hidePopup();
						SC.say("Data Save Successfully");
//						List<Lead> list = getDataprovider().getList();
//						list.get(rowIndex).setFollowUpDate(followUpDate);
//						Lead leadObj = list.get(rowIndex);
						quotationEntity.setFollowUpDate(followUpDate);
						selectedRecord.setAttribute("followupDate", AppUtility.parseDate(followUpDate));
						saveQuotation(quotationEntity);
						form.grid.getDataSource().updateData(selectedRecord);
						form.grid.redraw();
//						followUpFlag = false;
//						table.redraw();
//						glassPanel.hide();
//						communicationPanel.hide();
					}
				});
			}

		}
		if (event.getSource() == commSmrtPopup.getBtnCancel()) {
//			followUpFlag = false;
//			communicationPanel.hide();
			
			commSmrtPopup.hidePopup();
		}
		if (event.getSource() == commSmrtPopup.getBtnAdd()) {
//			glassPanel.show();
			String remark = commSmrtPopup.getRemark().getValue();
			Date dueDate = commSmrtPopup.getDueDate().getValue();
			String interactiongGroup = null;
			if (commSmrtPopup.getOblinteractionGroup().getSelectedIndex() != 0)
				interactiongGroup = commSmrtPopup.getOblinteractionGroup().getValue(commSmrtPopup.getOblinteractionGroup().getSelectedIndex());
				boolean validationFlag = AppUtility.validateCommunicationlogSmartGwt(remark, dueDate);

				List<InteractionType> list = commSmrtPopup.getCommunicationLogTable().getDataprovider().getList();
			
				Comparator<InteractionType> comp = new Comparator<InteractionType>() {
					@Override
					public int compare(InteractionType e1, InteractionType e2) {
						if (e1.getCount() == e2.getCount()) {
							return 0;
						}
						if (e1.getCount() > e2.getCount()) {
							return 1;
						} else {
							return -1;
						}
					}
				};
				Collections.sort(list, comp);
	
				if (validationFlag) {
					InteractionType communicationLog = AppUtility.getCommunicationLog(AppConstants.SERVICEMODULE,
									AppConstants.QUOTATION, quotationEntity.getCount(),quotationEntity.getEmployee(), remark, dueDate,
									quotationEntity.getCinfo(), null, interactiongGroup,quotationEntity.getBranch(),"");
					list.add(communicationLog);
					commSmrtPopup.getCommunicationLogTable().getDataprovider().setList(list);
					commSmrtPopup.getRemark().setValue("");
					commSmrtPopup.getDueDate().setValue(null);
					commSmrtPopup.getOblinteractionGroup().setSelectedIndex(0);
				}
//				glassPanel.hide();
		}
		
		 if(event.getSource() == generalviewpopup.getBtnClose()){
			 refreshscreenMenu();

		 }

		
	}
	
	private void refreshscreenMenu() {

		generalviewpopup.viewDocumentPanel.hide();
//		QuotationDashboardPresenter.initialize();
		AppMemory.getAppMemory().currentScreen = Screen.QUOTATIONDASHBOARD;
		form.toggleAppHeaderBarMenu();
		setEventHandeling();
		for (int k = 0; k < mem.skeleton.menuLabels.length; k++) {
			if (mem.skeleton.registration[k] != null)
				mem.skeleton.registration[k].removeHandler();
		}

		for (int k = 0; k < mem.skeleton.menuLabels.length; k++)
			mem.skeleton.registration[k] = mem.skeleton.menuLabels[k].addClickHandler(this);
		
	}

	private void saveQuotation(Quotation leadObj) {
		service.save(leadObj, new AsyncCallback<ReturnFromServer>() {
			@Override
			public void onFailure(Throwable caught) {
//				glassPanel.hide();
				SC.say("An Unexpected error occurred!");
			}
			@Override
			public void onSuccess(ReturnFromServer result) {
//				glassPanel.hide();
				SC.say("Saved successfully!");
			}
		});

	}
	
	private void searchByFromToDate() {
		System.out.println("FROM AND DATE ---");
		Long compId = UserConfiguration.getCompanyId();
		Vector<Filter> filtervec = null;
		Filter filter = null;
		MyQuerry querry;

		filtervec = new Vector<Filter>();

		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(compId);
		filtervec.add(filter);
		
		List<String> statusList=new ArrayList<String>();
		statusList=new ArrayList<String>();
		statusList.add(Quotation.CREATED);
		statusList.add(Quotation.REQUESTED);
		statusList.add(Quotation.APPROVED);
		
		filter = new Filter();
		filter.setQuerryString("status IN");
		filter.setList(statusList);
		filtervec.add(filter);

		if (form.getDbFromDate().getValue() != null) {
			filter = new Filter();
			filter.setQuerryString("followUpDate >=");
			filter.setDateValue(form.getDbFromDate().getValue());
			filtervec.add(filter);
		}

		if (form.getDbToDate().getValue() != null) {

			filter = new Filter();
			filter.setQuerryString("followUpDate <=");
			filter.setDateValue(form.getDbToDate().getValue());
			filtervec.add(filter);
		}

		if (form.getOlbbranch().getSelectedIndex() != 0) {
			filter = new Filter();
			filter.setQuerryString("branch");
			filter.setStringValue(form.getOlbbranch().getValue());
			filtervec.add(filter);

		}

		if (form.getOlbsalesPerson().getSelectedIndex() != 0) {
			filter = new Filter();
			filter.setQuerryString("employee");
			filter.setStringValue(form.getOlbsalesPerson().getValue());
			filtervec.add(filter);
		}

		querry = new MyQuerry();
		querry.setQuerryObject(new Quotation());
		querry.setFilters(filtervec);
		this.retriveTable(querry);

	}
	
	
	public void reactOnGo() {

		if (!searchForm.personInfo.getId().getValue().equals("")
				&& !searchForm.personInfo.getName().getValue().equals("")
				&& !searchForm.personInfo.getPhone().getValue().equals("")) {
			searchForm.hidePopUp();
			MyQuerry querry = createQuotationFilter();
			retriveTable(querry);
			
		}

		if (searchForm.personInfo.getId().getValue().equals("")
				&& searchForm.personInfo.getName().getValue().equals("")
				&& searchForm.personInfo.getPhone().getValue().equals("")) {
			SC.say("Please enter customer information");
		}

	}

	private MyQuerry createQuotationFilter() {
		Vector<Filter> filterVec = new Vector<Filter>();
		Filter temp;

		if (searchForm.personInfo.getId().getText().trim().equals("") == false) {
			temp = new Filter();
			temp.setLongValue(Long.parseLong(searchForm.personInfo.getId().getValue()));
			temp.setQuerryString("cinfo.count");
			filterVec.add(temp);
		}

		if ((searchForm.personInfo.getName().getText()).trim().equals("") == false) {
			temp = new Filter();
			temp.setStringValue(searchForm.personInfo.getName().getValue());
			temp.setQuerryString("cinfo.fullName");
			filterVec.add(temp);
		}

		if (searchForm.personInfo.getPhone().getText().trim().equals("") == false) {
			temp = new Filter();
			temp.setLongValue(Long.parseLong(searchForm.personInfo.getPhone().getValue()));
			temp.setQuerryString("cinfo.cellNumber");
			filterVec.add(temp);
		}
		
		 

		MyQuerry lead = new MyQuerry();
		lead.setQuerryObject(new Quotation());
		lead.setFilters(filterVec);

		return lead;
	}

	private void reactOnDownload() {
		ArrayList<Quotation> quotationList = new ArrayList<Quotation>();
		ListGridRecord[] allRecords = form.grid.getRecords();
        if (allRecords != null) {
        	List<ListGridRecord> allRecordsList = Arrays.asList(allRecords);
	   		for(int i=0;i<allRecordsList.size();i++){
	   			 final Quotation model=getQuotationDetails(Integer.parseInt(allRecordsList.get(i).getAttribute("quotationId").trim()));
	   			 if(model!=null){
	   				quotationList.add(model);
	   			 }
	   		}
        }
		
		csvservice.setquotationlist(quotationList, new AsyncCallback<Void>(){
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);
			}
			
			@Override
			public void onSuccess(Void result) {
				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "csvservlet"+"?type="+2;
				Window.open(url, "test", "enabled");
			}
		});
	}

	private Quotation getQuotationDetails(int quotationId) {
		for(Quotation object:globalQuotationoList){
			if(object.getCount()==quotationId){
				return object;
			}
		}
		return null;
	}

	@Override
	public void onCellClick(CellClickEvent event) {
		
		ListGridRecord record = event.getRecord();
		int colNum = event.getColNum();
		ListGridField field = form.grid.getField(colNum);
		String fieldName = form.grid.getFieldName(colNum);
		String fieldTitle = field.getTitle();
		position=event.getRowNum();
		System.out.println("Clicked " + fieldTitle + ":"+ record.getAttribute(fieldName) + "  Quotation Id :: "+ record.getAttribute("quotationId")+"POSITION "+position);
		quotationEntity = getQuotationDetails((Integer.parseInt(record.getAttribute("quotationId").trim())));
		if (fieldName.equals("followupButton")) {

			selectedRecord = record;
			
			
			getLogDetails(quotationEntity);
		}else{
			mapToQuotationScreen();
		}
	}
	
	private void mapToQuotationScreen() {
		/**
		 * Updated By:Viraj
		 * Date: 17-01-2019
		 * Description: To display Quotation in popup
		 */
		 removePopObjects();
		 
//		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Quotation",Screen.QUOTATION);
        final Quotation entity=quotationEntity;
        SalesLineItemQuotationTable.newModeFlag=true;
        final QuotationForm form = new QuotationForm();
//        final QuotationForm form = QuotationPresenter.initalize();
        form.content.getElement().getStyle().setHeight(570, Unit.PX);
		form.content.getElement().getStyle().setWidth(950, Unit.PX);
//		docVerticalPanel.add(form.content);
//		docLeadPanel.setHeight("600px");
//		docLeadPanel.setStyleName("gwt-SuggestBoxPopup");
//		docLeadPanel.show();
//		docLeadPanel.center();
//        
//        form.showWaitSymbol();
//        Timer timer=new Timer() {
//			@Override
//			public void run() {
//			     form.hideWaitSymbol();
//			     form.getPersonInfoComposite().setEnable(false);
//			     form.updateView(entity);
//			     form.setProcessLevelBar(null);
//			     form.setToViewState();
//			}
//			/** Ends **/
//		};
//       timer.schedule(5000); 
		
		AppMemory.getAppMemory().currentScreen = Screen.QUOTATION;
		generalviewpopup.setModel(AppConstants.QUOTATION, entity);
	}

	private void getLogDetails(Quotation quotation) {
		
		MyQuerry querry = AppUtility.communicationLogQuerry(quotation.getCount(),quotation.getCompanyId(), AppConstants.SERVICEMODULE,AppConstants.QUOTATION);
		service.getSearchResult(querry,	new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				ArrayList<InteractionType> list = new ArrayList<InteractionType>();
				for (SuperModel model : result) {
					InteractionType interactionType = (InteractionType) model;
					list.add(interactionType);
				}
				Comparator<InteractionType> comp = new Comparator<InteractionType>() {
					@Override
					public int compare(InteractionType e1,InteractionType e2) {
//						if (e1.getCount() == e2.getCount()) {
//							return 0;
//						}
//						if (e1.getCount() > e2.getCount()) {
//							return 1;
//						} else {
//							return -1;
//						}
						/**
						 * Updated By: Viraj
						 * Date: 06-02-2018
						 * Description: To sort communication log
						 */
						Integer ee1=e1.getCount();
						Integer ee2=e2.getCount();
						return ee2.compareTo(ee1);
						/** Ends **/
						
					}
				};
				Collections.sort(list, comp);
				
				commSmrtPopup.getRemark().setValue("");
				commSmrtPopup.getDueDate().setValue(null);
				commSmrtPopup.getOblinteractionGroup().setSelectedIndex(0);
				commSmrtPopup.getCommunicationLogTable().getDataprovider().setList(list);
				commSmrtPopup.showPopup();
			}

			@Override
			public void onFailure(Throwable caught) {
			}
		});

	}
	
	/**
	 * Updated By: Viraj
	 * Date: 17-01-2019
	 * Description: To remove items added in popup
	 */
	 void removePopObjects() {
			try {

				for (Widget wi : docVerticalPanel) {
					boolean flag = docLeadPanel.remove(wi);
					wi.removeFromParent();
					System.out.println("get item remove status " + flag);
				}

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}
	/** Ends **/

}
