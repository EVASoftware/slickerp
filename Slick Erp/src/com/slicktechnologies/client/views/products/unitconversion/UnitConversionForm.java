package com.slicktechnologies.client.views.products.unitconversion;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.config.configurations.ConfigTypes;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.cronjobcongiration.CronJobConfigration;
import com.slicktechnologies.shared.common.cronjobcongiration.CronJobConfigrationDetails;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.unitconversion.UnitConversion;
import com.slicktechnologies.shared.common.unitconversion.UnitConversionDetailList;

public class UnitConversionForm  extends FormTableScreen<UnitConversion> implements ClickHandler{

	

	/** The status. */
	public CheckBox status;

	/** The presenter. */
	protected UnitConversionPresenter presenter;
	
	ObjectListBox<Config> olbBaseUnit;
	ObjectListBox<Config> olbMaxUnit;
	ObjectListBox<Config> olbMinUnit;
	
	public TextBox tbUnitPrName;
	public TextBox tbMaxUnitPrName;
	public TextBox tbMinUnitPrName,tbBaseUnitPrName;
	
	DoubleBox dbMaxValue;
	DoubleBox dbMinValue;
	TextArea description;
	UnitConversionDetailsTable unitConversionListTable;
	Button addprocess;
	int screenCode;
	public UnitConversionForm()
	{
		super();
		createGui();
	}
	
	public UnitConversionForm(SuperTable<UnitConversion>table,int mode,boolean captionmode,int screenCode)
	{
		super(table, mode, captionmode);
		this.screenCode = screenCode;
		createGui();
		table.connectToLocal();
		
		if(screenCode == ConfigTypes.getConfigType(Screen.UNITOFMEASUREMENT)){
			AppUtility.MakeLiveConfig(olbBaseUnit, Screen.UNITOFMEASUREMENT);
		}else{
			AppUtility.MakeLiveConfig(olbBaseUnit, Screen.SERVICEUOM);
		}
		
		if(screenCode == ConfigTypes.getConfigType(Screen.UNITOFMEASUREMENT)){
			AppUtility.MakeLiveConfig(olbMaxUnit, Screen.UNITOFMEASUREMENT);
		}else{
			AppUtility.MakeLiveConfig(olbMaxUnit, Screen.SERVICEUOM);
		}
		
		if(screenCode == ConfigTypes.getConfigType(Screen.UNITOFMEASUREMENT)){
			AppUtility.MakeLiveConfig(olbMinUnit, Screen.UNITOFMEASUREMENT);
		}else{
			AppUtility.MakeLiveConfig(olbMinUnit, Screen.SERVICEUOM);
		}
	}
	
	private void initalizeWidget()
	{
		status = new CheckBox("",true);

		/** The presenter. */
		
		olbBaseUnit = new ObjectListBox<Config>();
		if(screenCode == ConfigTypes.getConfigType(Screen.UNITOFMEASUREMENT)){
			AppUtility.MakeLiveConfig(olbBaseUnit, Screen.UNITOFMEASUREMENT);
		}else{
			AppUtility.MakeLiveConfig(olbBaseUnit, Screen.SERVICEUOM);
		}
		
		olbMaxUnit = new ObjectListBox<Config>();
		if(screenCode == ConfigTypes.getConfigType(Screen.UNITOFMEASUREMENT)){
			AppUtility.MakeLiveConfig(olbMaxUnit, Screen.UNITOFMEASUREMENT);
		}else{
			AppUtility.MakeLiveConfig(olbMaxUnit, Screen.SERVICEUOM);
		}
		
		olbMinUnit =  new ObjectListBox<Config>();
		if(screenCode == ConfigTypes.getConfigType(Screen.UNITOFMEASUREMENT)){
			AppUtility.MakeLiveConfig(olbMinUnit, Screen.UNITOFMEASUREMENT);
		}else{
			AppUtility.MakeLiveConfig(olbMinUnit, Screen.SERVICEUOM);
		}
		
		tbUnitPrName = new TextBox();
		tbMaxUnitPrName = new TextBox();
		tbMinUnitPrName = new TextBox();
		tbBaseUnitPrName = new TextBox();
		
		tbUnitPrName.setEnabled(false);
		tbMinUnitPrName.setEnabled(false);
		tbMaxUnitPrName.setEnabled(false);
		tbBaseUnitPrName.setEnabled(false);
		
		dbMaxValue = new DoubleBox();
		dbMinValue = new DoubleBox();
		description = new TextArea();
		addprocess = new Button("ADD");
		addprocess.addClickHandler(this);
		unitConversionListTable = new UnitConversionDetailsTable();
		
	}
	
	@Override
	public void createScreen() {

		//Token to initialize the processlevel menus.
		initalizeWidget();

		this.processlevelBarNames=new String[]{"New"};
	
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingUnitInformation=fbuilder.setlabel("Unit Conversion Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("* Base Unit",olbBaseUnit);
		FormField fOlbBaseUnit= fbuilder.setMandatory(true).setMandatoryMsg("Base unit is Mandatory!").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Convert Unit",olbMaxUnit);
		FormField fOlbMaxUnit= fbuilder.setMandatory(true).setMandatoryMsg("Max unit is Mandatory!").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Base Unit",olbMinUnit);
		FormField fOlbMinUnit= fbuilder.setMandatory(false).setMandatoryMsg("Min is Mandatory!").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Convert Unit Value",dbMaxValue);
		FormField fOlbMaxUnitValue= fbuilder.setMandatory(true).setMandatoryMsg("Max Unit Value is Mandatory!").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Base Unit Value",dbMinValue);
		FormField fOlbMinUnitValue= fbuilder.setMandatory(true).setMandatoryMsg("Min Unit Value is Mandatory!").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Convert Unit Printable name",tbMaxUnitPrName);
		FormField fOlbMaxUnitPrName= fbuilder.setMandatory(false).setMandatoryMsg("Min Unit Value is Mandatory!").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Base Unit Printable name",tbMinUnitPrName);
		FormField fOlbMinUnitPrName= fbuilder.setMandatory(false).setMandatoryMsg("Min Unit Value is Mandatory!").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Base Unit Printable name",tbBaseUnitPrName);
		FormField fOlbbaseUnitPrName= fbuilder.setMandatory(false).setMandatoryMsg("Min Unit Value is Mandatory!").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",addprocess);
		FormField fBtnaddprocess= fbuilder.setMandatory(false).setMandatoryMsg("").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",unitConversionListTable.getTable());
		FormField fTblunitConversionListTable= fbuilder.setMandatory(false).setMandatoryMsg("Min Unit Value is Mandatory!").setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingUnitConvertionListInformation=fbuilder.setlabel("Unit Conversion List").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Status",status);
		FormField fStatus= fbuilder.setMandatory(false).setMandatoryMsg("Min Unit Value is Mandatory!").setRowSpan(0).setColSpan(0).build();
		
		FormField[][] formfield = { 
					{fgroupingUnitInformation},
					{fOlbBaseUnit,fOlbbaseUnitPrName},
					{fOlbMinUnit,fOlbMinUnitValue,fOlbMinUnitPrName},
					{fOlbMaxUnit,fOlbMaxUnitValue,fOlbMaxUnitPrName},
					{fStatus,fBtnaddprocess},
					{fgroupingUnitConvertionListInformation},
					{fTblunitConversionListTable},
				};


		this.fields=formfield;	
		
	}
	
	@Override
	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.PROCESSCONFIGURATION,LoginPresenter.currentModule.trim());
	}
	@Override
	public void updateModel(UnitConversion model) {
		
		if(olbBaseUnit.getValue()!=null){
//			model.setProcessName(processName.getValue());
			model.setUnit(olbBaseUnit.getValue());
			model.setUnitPrAbleName(olbBaseUnit.getSelectedItem().getDescription().trim());
			model.setUnitCount(olbBaseUnit.getSelectedItem().getCount());
			model.setUnitPrAbleName(olbBaseUnit.getSelectedItem().getDescription());
		}
		
		List<UnitConversionDetailList> processConfigType=this.unitConversionListTable.getDataprovider().getList();
		ArrayList<UnitConversionDetailList> arrItems=new ArrayList<UnitConversionDetailList>();
		arrItems.addAll(processConfigType);
		model.setUnitConversionList(arrItems);
		model.setUnitStatus(status.getValue());
		model.setUnitCode(screenCode);
		presenter.setModel(model);
	}
	
	@Override
	public void updateView(UnitConversion view) {
		
		unitConversionListTable.setValue(view.getUnitConversionList());
		
		if(view.getUnit()!=null){
			olbBaseUnit.setValue(view.getUnit());
//			olbBaseUnit.getSelectedItem().ge
		}
		
		status.setValue(view.isUnitStatus());
		screenCode = view.getUnitCode();
		presenter.setModel(view);
	}
	
	
	
	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		
		unitConversionListTable.setEnable(state);
		olbBaseUnit.setEnabled(state);
		olbMaxUnit.setEnabled(state);
		olbMinUnit.setEnabled(state);
		dbMaxValue.setEnabled(state);
		dbMinValue.setEnabled(state);
		tbBaseUnitPrName.setEnabled(state);
		tbMaxUnitPrName.setEnabled(state);
		tbMinUnitPrName.setEnabled(state);
		
	}
	
	@Override
	public void setToEditState() {
		// TODO Auto-generated method stub
		super.setToEditState();
		
		olbBaseUnit.setEnabled(false);
	}
	
	@Override
	public void setToViewState() {
		// TODO Auto-generated method stub
		super.setToViewState();
	}
	
	@Override
	public void setToNewState() {
		// TODO Auto-generated method stub
		super.setToNewState();
		olbBaseUnit.setEnabled(true);
	}
	
	@Override
	public void clear() {
			super.clear();
			unitConversionListTable.clear();
	}

	@Override
	public void onClick(ClickEvent event) {
		if(event.getSource().equals(addprocess)){
			System.out.println("Base name  --" + olbBaseUnit.getSelectedItem().getName());
			System.out.println("max name --" +olbMaxUnit.getSelectedItem().getName());
			System.out.println("min name --" +olbMinUnit.getSelectedItem().getName());
			if(olbBaseUnit.getSelectedIndex()==0 || 
					olbMaxUnit.getSelectedIndex() == 0 || olbBaseUnit.getSelectedIndex() == 0){
				showDialogMessage("Please selected all unit Options");
				return;
			}else if(!olbBaseUnit.getSelectedItem().getName().equals(olbMaxUnit.getSelectedItem().getName())
					&& (!olbBaseUnit.getSelectedItem().getName().equals(olbMinUnit.getSelectedItem().getName()))){
					showDialogMessage("Please select base unit in one of two unit selection option");
					return;
			}else{
				addUnitList();
			}
		}
	}
	
	void addUnitList(){
		UnitConversionDetailList unitConvert  = new UnitConversionDetailList();
			unitConvert.setMaxUnit(olbMaxUnit.getSelectedItem().getName());
			unitConvert.setMaxUnitPrName(tbMaxUnitPrName.getValue());
			unitConvert.setMinUnit(olbMinUnit.getSelectedItem().getName());
			unitConvert.setMinUnitPrName(tbMinUnitPrName.getValue());
			unitConvert.setMaxValue(dbMaxValue.getValue());
			unitConvert.setMinValue(dbMinValue.getValue());
			unitConvert.setStatus(status.getValue());
		unitConversionListTable.getDataprovider().getList().add(unitConvert);
		unitConversionListTable.getTable().redraw();
	}

	
	@Override
	public boolean validate() {
		
		
		if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT){
			if(olbMaxUnit.getSelectedIndex() ==0  && olbMinUnit.getSelectedIndex() == 0 );
			return true;
		}
		boolean flag = super.validate();
		return flag;
	};
	
	
	public CheckBox getStatus() {
		return status;
	}

	public void setStatus(CheckBox status) {
		this.status = status;
	}

	public UnitConversionPresenter getPresenter() {
		return presenter;
	}

	public void setPresenter(UnitConversionPresenter presenter) {
		this.presenter = presenter;
	}

	public ObjectListBox<Config> getOlbBaseUnit() {
		return olbBaseUnit;
	}

	public void setOlbBaseUnit(ObjectListBox<Config> olbBaseUnit) {
		this.olbBaseUnit = olbBaseUnit;
	}

	public ObjectListBox<Config> getOlbMaxUnit() {
		return olbMaxUnit;
	}

	public void setOlbMaxUnit(ObjectListBox<Config> olbMaxUnit) {
		this.olbMaxUnit = olbMaxUnit;
	}

	public ObjectListBox<Config> getOlbMinUnit() {
		return olbMinUnit;
	}

	public void setOlbMinUnit(ObjectListBox<Config> olbMinUnit) {
		this.olbMinUnit = olbMinUnit;
	}

	public TextBox getTbUnitPrName() {
		return tbUnitPrName;
	}

	public void setTbUnitPrName(TextBox tbUnitPrName) {
		this.tbUnitPrName = tbUnitPrName;
	}

	public TextBox getTbMaxUnitPrName() {
		return tbMaxUnitPrName;
	}

	public void setTbMaxUnitPrName(TextBox tbMaxUnitPrName) {
		this.tbMaxUnitPrName = tbMaxUnitPrName;
	}

	public TextBox getTbMinUnitPrName() {
		return tbMinUnitPrName;
	}

	public void setTbMinUnitPrName(TextBox tbMinUnitPrName) {
		this.tbMinUnitPrName = tbMinUnitPrName;
	}

	public TextBox getTbBaseUnitPrName() {
		return tbBaseUnitPrName;
	}

	public void setTbBaseUnitPrName(TextBox tbBaseUnitPrName) {
		this.tbBaseUnitPrName = tbBaseUnitPrName;
	}

	public DoubleBox getDbMaxValue() {
		return dbMaxValue;
	}

	public void setDbMaxValue(DoubleBox dbMaxValue) {
		this.dbMaxValue = dbMaxValue;
	}

	public DoubleBox getDbMinValue() {
		return dbMinValue;
	}

	public void setDbMinValue(DoubleBox dbMinValue) {
		this.dbMinValue = dbMinValue;
	}

	public TextArea getDescription() {
		return description;
	}

	public void setDescription(TextArea description) {
		this.description = description;
	}

	public UnitConversionDetailsTable getUnitConversionListTable() {
		return unitConversionListTable;
	}

	public void setUnitConversionListTable(
			UnitConversionDetailsTable unitConversionListTable) {
		this.unitConversionListTable = unitConversionListTable;
	}

	public Button getAddprocess() {
		return addprocess;
	}

	public void setAddprocess(Button addprocess) {
		this.addprocess = addprocess;
	}

	public int getScreenCode() {
		return screenCode;
	}

	public void setScreenCode(int screenCode) {
		this.screenCode = screenCode;
	}
	

	
	
}
