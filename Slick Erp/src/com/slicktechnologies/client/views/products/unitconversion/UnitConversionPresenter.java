package com.slicktechnologies.client.views.products.unitconversion;

import java.util.Vector;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.settings.cronjobconfig.CronJobConfigrationForm;
import com.slicktechnologies.client.views.settings.cronjobconfig.CronJobConfigrationPresenter;
import com.slicktechnologies.client.views.settings.cronjobconfig.CronJobConfigrationTable;
import com.slicktechnologies.shared.common.cronjobcongiration.CronJobConfigration;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.unitconversion.UnitConversion;

public class UnitConversionPresenter extends FormTableScreenPresenter<UnitConversion> implements ChangeHandler {

	UnitConversionForm form;
	public UnitConversionPresenter(FormTableScreen<UnitConversion> view,
			UnitConversion model) {
		super(view, model);
		form=(UnitConversionForm) view;
		form.getSupertable().connectToLocal();
		form.retriveTable(getUserQuery(form.getScreenCode()));
		form.setPresenter(this);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.PROCESSCONFIGURATION,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
		
		form.getOlbBaseUnit().addChangeHandler(this);
		form.getOlbMaxUnit().addChangeHandler(this);
		form.getOlbMinUnit().addChangeHandler(this);
	}

	private MyQuerry getUserQuery(int screenCode) {
		
		MyQuerry quer=new MyQuerry();
		
		Vector<Filter> temp=new Vector<Filter>();
		Filter filter=null;
		
		filter=new Filter();
		filter.setQuerryString("unitCode");
		filter.setIntValue(screenCode);
		temp.add(filter);
		
		quer.setFilters(temp);
		quer.setQuerryObject(new UnitConversion());
		return quer;
	}
	
	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void makeNewModel() {
		model = new UnitConversion();
	}

	public void setModel(UnitConversion entity) {
		model = entity;
	}
	public static void initalize(int screenCode) {
		UnitConversionTableProxy gentable=new UnitConversionTableProxy();
		UnitConversionForm form=new UnitConversionForm(gentable,FormTableScreen.UPPER_MODE,true,screenCode);
		gentable.setView(form);
		gentable.applySelectionModle();
		UnitConversionPresenter presenter=new UnitConversionPresenter(form, new UnitConversion());
		AppMemory.getAppMemory().stickPnel(form);
	}

	
	@Override
	public void onChange(ChangeEvent event) {
		if(form.getOlbBaseUnit().equals(event.getSource())){
			
		}else if(event.getSource().equals(form.getOlbMaxUnit())){
			form.getTbMaxUnitPrName().setValue(form.getOlbMaxUnit().getSelectedItem().getDescription().trim());
		}else if(event.getSource().equals(form.getOlbMinUnit())){
			form.getTbMinUnitPrName().setValue(form.getOlbMinUnit().getSelectedItem().getDescription().trim());
		}
	}
	
	
//	valida
}
