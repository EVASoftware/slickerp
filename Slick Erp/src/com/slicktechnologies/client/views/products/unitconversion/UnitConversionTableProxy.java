package com.slicktechnologies.client.views.products.unitconversion;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.unitconversion.UnitConversion;

public class UnitConversionTableProxy extends SuperTable<UnitConversion>{

	TextColumn<UnitConversion> countColumn;
	TextColumn<UnitConversion> processNameColumn;
	
	public UnitConversionTableProxy(){
		super();
	}
	
	@Override
	public void createTable() {
		addColumngetCount();
		addColumngetprocessName();
	}
	
	


	public void addColumnSorting(){
		addSortinggetCount();
		addSortinggetprocessName();
	}
	
	private void addSortinggetCount() {
		List<UnitConversion> list=getDataprovider().getList();
		columnSort=new ListHandler<UnitConversion>(list);
		columnSort.setComparator(countColumn, new Comparator<UnitConversion>()
				{
			@Override
			public int compare(UnitConversion e1,UnitConversion e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	private void addColumngetCount() {
		countColumn=new TextColumn<UnitConversion>() {

			@Override
			public String getValue(UnitConversion object) {
				if(object.getCount()==-1)
					return "N.A";
				else return object.getCount()+"";
			
			}
		};
		table.addColumn(countColumn, "ID");
		table.setColumnWidth(countColumn, 100, Unit.PX);
		countColumn.setSortable(true);
	}




		private void addSortinggetprocessName() {
			List<UnitConversion> list=getDataprovider().getList();
			columnSort=new ListHandler<UnitConversion>(list);
			columnSort.setComparator(processNameColumn, new Comparator<UnitConversion>()
					{
				@Override
				public int compare(UnitConversion e1,UnitConversion e2)
				{
					if(e1!=null && e2!=null)
					{
						if( e1.getUnit()!=null && e2.getUnit()!=null){
							return e1.getUnit().compareTo(e2.getUnit());}
					}
					else{
						return 0;}
					return 0;
				}
					});
			table.addColumnSortHandler(columnSort);
	}
	
		private void addColumngetprocessName() {
			processNameColumn=new TextColumn<UnitConversion>()
					{
				@Override
				public String getValue(UnitConversion object)
				{
					return object.getUnit().trim();
				}
					};
					table.addColumn(processNameColumn,"Process Name");
					table.setColumnWidth(processNameColumn, 130, Unit.PX);
					processNameColumn.setSortable(true);
		}
	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
