package com.slicktechnologies.client.views.products.unitconversion;

import java.util.Date;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.view.client.ProvidesKey;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.cronjobcongiration.CronJobConfigrationDetails;
import com.slicktechnologies.shared.common.unitconversion.UnitConversionDetailList;

public class UnitConversionDetailsTable  extends SuperTable<UnitConversionDetailList>  {

	TextColumn<UnitConversionDetailList> minUnitNameColumn;
	TextColumn<UnitConversionDetailList> maxUnitNameColumn;
	TextColumn<UnitConversionDetailList> maxUnitValueColumn;
	TextColumn<UnitConversionDetailList> minUnitValueColumn;
	Column<UnitConversionDetailList, Boolean> statusColumn;
    TextColumn<UnitConversionDetailList> viewStatusColumn;
    Column<UnitConversionDetailList,String> deleteColumn;
	
	@Override
	public void createTable() {
		// TODO Auto-generated method stub
		addColumngetMinUnit();
		addColumngetMinUnitValue();
		addColumngetMaxUnit();
		addColumngetMaxUnitValue();
		addColumnStatus();
		addColumngetdelete();
		createFieldUpdaterdeleteColumn();
		table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
	}
	
	
	private void addColumngetMinUnit()
	{
		minUnitNameColumn=new TextColumn<UnitConversionDetailList>()
				{
			@Override
			public String getValue(UnitConversionDetailList object)
			{
				return object.getMinUnit().trim();
			}
				};
				table.addColumn(minUnitNameColumn,"Min Unit");
				table.setColumnWidth(minUnitNameColumn,200, Unit.PX);
				minUnitNameColumn.setSortable(true);	
	}
	
	private void addColumngetMinUnitValue()
	{
		minUnitValueColumn=new TextColumn<UnitConversionDetailList>()
				{
			@Override
			public String getValue(UnitConversionDetailList object)
			{
				return object.getMinValue()+"";
			}
				};
				table.addColumn(minUnitValueColumn,"Min Unit Value");
				table.setColumnWidth(minUnitValueColumn,200, Unit.PX);
				minUnitValueColumn.setSortable(true);	
	}
	
	private void addColumngetMaxUnit()
	{
		maxUnitNameColumn=new TextColumn<UnitConversionDetailList>()
				{
			@Override
			public String getValue(UnitConversionDetailList object)
			{
				return object.getMaxUnit().trim();
			}
				};
				table.addColumn(maxUnitNameColumn,"Max Unit");
				table.setColumnWidth(maxUnitNameColumn,200, Unit.PX);
				maxUnitNameColumn.setSortable(true);	
	}
	
	private void addColumngetMaxUnitValue()
	{
		minUnitNameColumn=new TextColumn<UnitConversionDetailList>()
				{
			@Override
			public String getValue(UnitConversionDetailList object)
			{
				return object.getMaxValue()+"";
			}
				};
				table.addColumn(minUnitNameColumn,"Max Unit Value");
				table.setColumnWidth(minUnitNameColumn,200, Unit.PX);
				minUnitNameColumn.setSortable(true);	
	}
	
	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub

		keyProvider = new ProvidesKey<UnitConversionDetailList>() {
			@Override
			public Object getKey(UnitConversionDetailList item) {
				if (item == null) {
					return null;
				} else
					return new Date().getTime();
			}
		};

	
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		
		for(int i=table.getColumnCount()-1;i>-1;i--){
			table.removeColumn(i);
		}
			if(state==true){
				addColumngetMinUnit();
				addColumngetMinUnitValue();
				addColumngetMaxUnit();
				addColumngetMaxUnitValue();
				addColumnStatus();
				addColumngetdelete();
				createFieldUpdaterdeleteColumn();
			}else {
				addColumngetMinUnit();
				addColumngetMinUnitValue();
				addColumngetMaxUnit();
				addColumngetMaxUnitValue();
				addColumnStatus();
			}
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
	protected void addColumnViewgetstatus() {
		
		viewStatusColumn = new TextColumn<UnitConversionDetailList>() {
			@Override
			public String getValue(UnitConversionDetailList object) {
				return object.isStatus()+"";
			}
		};
		
		table.addColumn(viewStatusColumn, "Status");
		
	}
	
	private void addColumnStatus() {
		CheckboxCell cb = new CheckboxCell();
		statusColumn = new Column<UnitConversionDetailList, Boolean>(cb) {

			@Override
			public Boolean getValue(UnitConversionDetailList object) {
				return object.isStatus();
			}
		};
		table.addColumn(statusColumn, "Status");
		table.setColumnWidth(statusColumn, 200, Unit.PX);
		statusColumn.setSortable(true);		
	}
	
	private void addColumngetdelete() {
		ButtonCell btnCell = new ButtonCell();
		deleteColumn = new Column<UnitConversionDetailList,String>(btnCell) {

            public String getValue(UnitConversionDetailList object)
            {
                return "Delete";
            }
        };
        table.setColumnWidth(deleteColumn, 200, Unit.PX);
        table.addColumn(deleteColumn, "Delete");		
	}
	
	
	
	
	
	private void createFieldUpdaterdeleteColumn() {
		 deleteColumn.setFieldUpdater(new FieldUpdater<UnitConversionDetailList,String>() {

	            public void update(int index, UnitConversionDetailList object, String value)
	            {
	                getDataprovider().getList().remove(index);
	                table.redrawRow(index);
	            }
	        }
	);		
	}
	
	
	protected void setFieldUpdaterOnStatus()
	{
		statusColumn.setFieldUpdater(new FieldUpdater<UnitConversionDetailList, Boolean>() {
			@Override
			public void update(int index, UnitConversionDetailList object, Boolean value) {
				object.setStatus(value);
				getDataprovider().getList();
				table.redrawRow(index);
			}
		});
	}
}
