package com.slicktechnologies.client.views.products.itemproduct;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import com.simplesoftwares.client.library.appstructure.search.*;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.formtablescreen.*;
import com.slicktechnologies.shared.*;
import com.slicktechnologies.shared.common.*;
import com.slicktechnologies.shared.common.businessprocesslayer.*;
import com.slicktechnologies.shared.common.businessunitlayer.*;
import com.slicktechnologies.shared.common.helperlayer.*;
import com.slicktechnologies.shared.common.personlayer.*;
import com.slicktechnologies.shared.common.productlayer.*;

import java.util.Vector;

import com.simplesoftwares.client.library.appstructure.search.*;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.google.gwt.core.shared.GWT;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.DataMigrationService;
import com.slicktechnologies.client.services.DataMigrationServiceAsync;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.customer.CustomerForm;
import com.slicktechnologies.client.views.customer.CustomerPresenter.CustomerPresenterSearch;
import com.slicktechnologies.client.views.project.tool.ToolForm;
import com.slicktechnologies.client.views.project.tool.ToolPresenter;
import com.slicktechnologies.client.views.project.tool.UploadAssetPopup;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.shared.common.paymentlayer.*;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.salesorder.SalesQuotation;
import com.slicktechnologies.shared.common.servicerelated.CompanyAsset;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;

/**
 * FormTableScreen presenter template
 */
public class ItemproductPresenter extends FormScreenPresenter<ItemProduct>
		implements ValueChangeHandler<Boolean>, ChangeHandler {

	// Token to set the concrete form
	ItemproductForm form;
	CsvServiceAsync csvservice = GWT.create(CsvService.class);
	final GenricServiceAsync genasync = GWT.create(GenricService.class);

	/**
	 * This fields are used to create large no. of assets In upload popup you
	 * can download the format and upload asset excel to create assetts in the
	 * system Date : 22-10-2016 By ANil Release : 30 Sept 2016 Project :
	 * Purchase Modification(NBHC)
	 *
	 */
	UploadAssetPopup uploadPopup = new UploadAssetPopup();
	PopupPanel panel;
	DataMigrationServiceAsync datamigAsync = GWT
			.create(DataMigrationService.class);

	/**
	 * End
	 */
	
	/**
	 * @author Anil , Date : 13-06-2019
	 * 
	 */
	GeneralServiceAsync generalAsync = GWT.create(GeneralService.class);

	public ItemproductPresenter(FormScreen<ItemProduct> view, ItemProduct model) {
		super(view, model);
		form = (ItemproductForm) view;
		form.setPresenter(this);
		form.tcVatTax.taxch.addValueChangeHandler(this);
		form.tcServiceTax.taxch.addValueChangeHandler(this);
		form.tcVatTax.percentageBox.addChangeHandler(this);
		form.tcServiceTax.percentageBox.addChangeHandler(this);
		/****
		 * Rahul Verma
		 * 07 July 2018
		 * 
		 */
		form.tcPurTax1.taxch.addValueChangeHandler(this);
		form.tcPurTax2.taxch.addValueChangeHandler(this);
		form.tcPurTax1.percentageBox.addChangeHandler(this);
		form.tcPurTax2.percentageBox.addChangeHandler(this);
		/**
		 * Ends
		 */
		uploadPopup.getBtnOk().addClickHandler(this);
		uploadPopup.getBtnCancel().addClickHandler(this);
		uploadPopup.getDownloadFormat().addClickHandler(this);
		
		/** date 14.7.2018 added by komal for margin **/
		form.dbMargin.addChangeHandler(this);


		boolean isDownload = AuthorizationHelper.getDownloadAuthorization(
				Screen.ITEMPRODUCT, LoginPresenter.currentModule.trim());
		if (isDownload == false) {
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}

	/**
	 * Method template to set the processBar events
	 */
	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel label = (InlineLabel) e.getSource();
		String text = label.getText().trim();

		if (text.equals("New")) {
			reactToNew();
		}
		if (text.equals(AppConstants.CREATEASSET)) {
			reactToCreateAsset();
		}
		if (text.equals(AppConstants.CREATEMASSASSET)) {
			reactToMassAssetCreation();
		}
		if (text.equals("Update Product Code")) {
			form.showWaitSymbol();
			generalAsync.updateProductCode(UserConfiguration.getCompanyId(), new AsyncCallback<Void>() {

				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					form.hideWaitSymbol();
				}

				@Override
				public void onSuccess(Void result) {
					// TODO Auto-generated method stub
					form.hideWaitSymbol();
					form.showDialogMessage("Process Started...");
				}
			});
		}
	}

	/**
	 * This method is used to create 'n' no. of asset at one click. Date :
	 * 21-10-2016 By Anil Release : 30 Sept 2016 Project: Purchase
	 * Modification(NBHC)
	 */

	private void reactToMassAssetCreation() {
		System.out.println("Mass Asset Creation");

		uploadPopup.clear();
		panel = new PopupPanel(true);
		panel.add(uploadPopup);
		panel.show();
		panel.center();
	}

	/**
	 * This method initialize company asset form with basic details copied from
	 * item product Date : 21-10-2016 By Anil Release : 30 Sept 2016 Project:
	 * Purchase Modification(NBHC)
	 */
	private void reactToCreateAsset() {
		final ToolForm toolform = ToolPresenter.initalize();
		final CompanyAsset entity = new CompanyAsset();

		entity.setProductId(Integer.parseInt(form.tbproductId.getValue()));
		entity.setBrand(form.oblBrandName.getValue());
		entity.setModelNo(form.tbModel.getValue());
		entity.setSrNo(form.tbSerialNumber.getValue());
		String assetName = "";
		if (!form.tbSerialNumber.getValue().equals("")) {
			assetName = assetName + form.tbProductName.getValue() + "-"
					+ form.tbSerialNumber.getValue();
		} else {
			assetName = assetName + form.tbProductName.getValue() + "-"
					+ "Sr.No.";
		}
		entity.setName(assetName);

		Timer t = new Timer() {
			@Override
			public void run() {
				toolform.setToNewState();
				toolform.updateView(entity);
			}
		};
		t.schedule(5000);
	}

	@Override
	public void onClick(ClickEvent event) {
		super.onClick(event);

		if (event.getSource().equals(uploadPopup.getDownloadFormat())) {

			System.out.println("MODEL COUNT " + model.getCount());
			csvservice.getAssetUploadFormat(model, new AsyncCallback<Void>() {
				@Override
				public void onSuccess(Void result) {
					String gwt = com.google.gwt.core.client.GWT
							.getModuleBaseURL();
					final String url = gwt + "csvservlet" + "?type=" + 113;
					Window.open(url, "test", "enabled");
				}

				@Override
				public void onFailure(Throwable caught) {

				}
			});
			panel.hide();
		}
		if (event.getSource().equals(uploadPopup.getBtnOk())) {
			datamigAsync.savedRecordsDeatils(UserConfiguration.getCompanyId(),
					"Company Asset", true,LoginPresenter.loggedInUser,
					new AsyncCallback<ArrayList<Integer>>() {
						@Override
						public void onFailure(Throwable caught) {
							panel.hide();
						}

						@Override
						public void onSuccess(ArrayList<Integer> result) {
							if (result.contains(-1)) {
								form.showDialogMessage("Invalid Excel File");
							} else if (result.contains(-16)) {
								form.showDialogMessage("Error: Asset Name Already Exists!!");
							} else if (result.contains(-17)) {
								form.showDialogMessage("Error: Please set date format to date column!!");
							} else if (result.contains(-18)) {
								form.showDialogMessage("Error: Please upload .xls format file!!");
							} else {
								form.showDialogMessage("Sucessfully");
								panel.hide();
							}
						}
					});

		}
		if (event.getSource().equals(uploadPopup.getBtnCancel())) {
			panel.hide();
		}
	}

	private void reactToNew() {
		form.setToNewState();
		this.initalize();
		form.toggleAppHeaderBarMenu();
	}

	@Override
	public void reactOnPrint() {

	}

	@Override
	public void reactOnEmail() {

	}

	@Override
	public void reactOnDownload() {
		ArrayList<ItemProduct> iparray = new ArrayList<ItemProduct>();
		List<ItemProduct> list = (List<ItemProduct>) form
				.getSearchpopupscreen().getSupertable().getDataprovider()
				.getList();
		iparray.addAll(list);
		csvservice.setItemProductList(iparray, new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed" + caught);
			}

			@Override
			public void onSuccess(Void result) {
				String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url = gwt + "csvservlet" + "?type=" + 74;
				Window.open(url, "test", "enabled");
			}
		});
	}

	/**
	 * Method template to set new model
	 */
	@Override
	protected void makeNewModel() {
		model = new ItemProduct();
	}

	/*
	 * Method template to set Myquerry object
	 */
	public MyQuerry getItemProductQuery() {
		MyQuerry quer = new MyQuerry(new Vector<Filter>(), new ItemProduct());
		return quer;
	}

	public void setModel(ItemProduct entity) {
		model = entity;
	}

	public static ItemproductForm initalize() {
		ItemproductPresenterTable gentableScreen = new ItemproductPresenterTableProxy();
		ItemproductForm form = new ItemproductForm();
		gentableScreen.setView(form);
		gentableScreen.applySelectionModle();

		ItemproductPresenterSearch.staticSuperTable = gentableScreen;
		ItemproductPresenterSearch searchpopup = new ItemProductPresenterSearchProxy();
		form.setSearchpopupscreen(searchpopup);

		ItemproductPresenter presenter = new ItemproductPresenter(form,
				new ItemProduct());
		AppMemory.getAppMemory().stickPnel(form);
		return form;
	}

	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.productlayer.ItemProduct")
	public static class ItemproductPresenterTable extends
			SuperTable<ItemProduct> implements GeneratedVariableRefrence {

		@Override
		public Object getVarRef(String varName) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void createTable() {
			// TODO Auto-generated method stub

		}

		@Override
		protected void initializekeyprovider() {
			// TODO Auto-generated method stub

		}

		@Override
		public void addFieldUpdater() {
			// TODO Auto-generated method stub

		}

		@Override
		public void setEnable(boolean state) {
			// TODO Auto-generated method stub

		}

		@Override
		public void applyStyle() {
			// TODO Auto-generated method stub

		}
	};

	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.productlayer.ItemProduct")
	public static class ItemproductPresenterSearch extends
			SearchPopUpScreen<ItemProduct> {

		@Override
		public MyQuerry getQuerry() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public boolean validate() {
			// TODO Auto-generated method stub
			return true;
		}
	};

	@Override
	public void onValueChange(ValueChangeEvent<Boolean> event) {
		if (event.getSource().equals(form.tcVatTax.taxch)) {

			if (form.tcVatTax.taxch.getValue() == true) {
				form.tcVatTax.percentageBox.setEnabled(true);
			}
			if (form.tcVatTax.taxch.getValue() == false) {
				form.tcVatTax.percentageBox.setSelectedIndex(0);
				form.tcVatTax.percentageBox.setEnabled(false);
				form.tcVatTax.taxInclusivech.setValue(false);
				form.tcVatTax.dbPercent.setValue(null);
			}
		}
		
		/***
		 * Rahul Verma
		 * 07 July 2018
		 * 
		 */
		if(event.getSource().equals(form.tcPurTax1.taxch)){
			if (form.tcPurTax1.taxch.getValue() == true) {
				form.tcPurTax1.percentageBox.setEnabled(true);
			}
			if (form.tcPurTax1.taxch.getValue() == false) {
				form.tcPurTax1.percentageBox.setSelectedIndex(0);
				form.tcPurTax1.percentageBox.setEnabled(false);
				form.tcPurTax1.taxInclusivech.setValue(false);
				form.tcPurTax1.dbPercent.setValue(null);
			}
		
		}
		
		if(event.getSource().equals(form.tcPurTax2.taxch)){
			if (form.tcPurTax2.taxch.getValue() == true) {
				form.tcPurTax2.percentageBox.setEnabled(true);
			}
			if (form.tcPurTax2.taxch.getValue() == false) {
				form.tcPurTax2.percentageBox.setSelectedIndex(0);
				form.tcPurTax2.percentageBox.setEnabled(false);
				form.tcPurTax2.taxInclusivech.setValue(false);
				form.tcPurTax2.dbPercent.setValue(null);
			}
		}
		/**
		 * Ends
		 */
		if (event.getSource().equals(form.tcServiceTax.taxch)) {
			if (form.tcServiceTax.taxch.getValue() == true) {
				form.tcServiceTax.percentageBox.setEnabled(true);
			}
			if (form.tcServiceTax.taxch.getValue() == false) {
				form.tcServiceTax.percentageBox.setSelectedIndex(0);
				form.tcServiceTax.percentageBox.setEnabled(false);
				form.tcServiceTax.taxInclusivech.setValue(false);
				form.tcServiceTax.dbPercent.setValue(null);
			}
		}
	}

	@Override
	public void onChange(ChangeEvent event) {
		System.out.println("HI ------------");
		if (event.getSource().equals(form.tcServiceTax.percentageBox)) {
			if (form.tcServiceTax.percentageBox.getSelectedIndex() != 0) {
				TaxDetails percServ = form.tcServiceTax.percentageBox
						.getSelectedItem();
				if (percServ != null) {
					form.tcServiceTax.dbPercent.setValue(percServ
							.getTaxChargePercent());
					/**
					 * Date : 01/07/2017 by Rohan
					 */
					form.tcServiceTax.taxPrintableName=percServ.getTaxPrintName();
					System.out.println("PRINTABEL_ Service -"+percServ.getTaxPrintName());
					/**
					 * End
					 */
				}
			} else {
				form.tcServiceTax.percentageBox.setSelectedIndex(0);
			}
		}
		
		/***
		 * Rahul Verma
		 * 07 July 2018
		 */
		if (event.getSource().equals(form.tcPurTax1.percentageBox)) {
			if (form.tcPurTax1.percentageBox.getSelectedIndex() != 0) {
				TaxDetails percServ = form.tcPurTax1.percentageBox
						.getSelectedItem();
				if (percServ != null) {
					form.tcPurTax1.dbPercent.setValue(percServ
							.getTaxChargePercent());
					/**
					 * Date : 01/07/2017 by Rohan
					 */
					form.tcPurTax1.taxPrintableName=percServ.getTaxPrintName();
					System.out.println("PRINTABEL_ Service -"+percServ.getTaxPrintName());
					/**
					 * End
					 */
				}
			} else {
				form.tcPurTax1.percentageBox.setSelectedIndex(0);
			}
		}
		
		if (event.getSource().equals(form.tcPurTax2.percentageBox)) {
			if (form.tcPurTax2.percentageBox.getSelectedIndex() != 0) {
				TaxDetails percServ = form.tcPurTax2.percentageBox
						.getSelectedItem();
				if (percServ != null) {
					form.tcPurTax2.dbPercent.setValue(percServ
							.getTaxChargePercent());
					/**
					 * Date : 01/07/2017 by Rohan
					 */
					form.tcPurTax2.taxPrintableName=percServ.getTaxPrintName();
					System.out.println("PRINTABEL_ Service -"+percServ.getTaxPrintName());
					/**
					 * End
					 */
				}
			} else {
				form.tcPurTax2.percentageBox.setSelectedIndex(0);
			}
		}
		
		/**
		 * Ends
		 */

		if (event.getSource().equals(form.tcVatTax.percentageBox)) {
			if (form.tcVatTax.percentageBox.getSelectedIndex() != 0) {
				TaxDetails percServ = form.tcVatTax.percentageBox
						.getSelectedItem();
				if (percServ != null) {
					form.tcVatTax.dbPercent.setValue(percServ
							.getTaxChargePercent());
					/**
					 * Date : 01/07/2017 by Rohan
					 */
					form.tcVatTax.taxPrintableName=percServ.getTaxPrintName();
					System.out.println("PRINTABEL_ VAT -"+percServ.getTaxPrintName());

					/**
					 * End
					 */
				}
			} else {
				form.tcVatTax.percentageBox.setSelectedIndex(0);
			}
		}
		/** date 14.7.2018 added by komal for margin **/
		if(event.getSource().equals(form.dbMargin)){
			System.out.println("in change");
			if(form.dbProductPurchasePrice.getValue()!=null){
			if(form.dbMargin.getValue()!=null){
				System.out.println("in change");
				double margin = form.dbMargin.getValue();
				double purchasePrice = form.dbProductPurchasePrice.getValue();
				System.out.println("in change"+margin);
				double salesPrice = purchasePrice + (purchasePrice*margin/100.0);
				System.out.println("in change"+salesPrice);
				form.dbProductSalesPrice.setValue(salesPrice);
				form.tcServiceTax.dbPercent.setValue(form.tcPurTax2.dbPercent.getValue());
				form.tcServiceTax.taxInclusivech.setValue(form.tcPurTax2.taxInclusivech.getValue());
				if(form.tcPurTax2.percentageBox.getValue()!=null)
				form.tcServiceTax.percentageBox.setValue(form.tcPurTax2.percentageBox.getValue());
				form.tcServiceTax.taxch.setValue(form.tcPurTax2.taxch.getValue());
				form.tcVatTax.dbPercent.setValue(form.tcPurTax1.dbPercent.getValue());
				form.tcVatTax.taxInclusivech.setValue(form.tcPurTax1.taxInclusivech.getValue());
				if(form.tcPurTax1.percentageBox.getValue()!=null)
				form.tcVatTax.percentageBox.setValue(form.tcPurTax1.percentageBox.getValue());
				form.tcVatTax.taxch.setValue(form.tcPurTax1.taxch.getValue());
			
			}
		 }else{
			 	
				form.showDialogMessage("Please enter purchase price first.");
			}		
		}
		/**
		 * end komal
		 */
	}

}
