package com.slicktechnologies.client.views.products.itemproduct;

import java.util.ArrayList;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.products.itemproduct.ItemproductPresenter.ItemproductPresenterSearch;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.productlayer.Category;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;

public class ItemProductPresenterSearchProxy extends ItemproductPresenterSearch {
	
	ObjectListBox<Category> olbcProductCategory;
	ObjectListBox<Config> olbUnitOfMeasurement;
	public ProductInfoComposite prodComposite;
	/** date 13.10.2018 added by komal for sasha**/
	ObjectListBox<Config> olbProductGroup;
	ObjectListBox<Config> olbProductType;
	ObjectListBox<Config> olbProductClassification;
//	TextBox tbBrandName;
	/**
	 * Date  : 29-11-2018 BY ANIL
	 * updated brand name from  textbox to objectlistbox
	 */
	ObjectListBox<Config>oblBrandName;
	public ListBox lbStatus; //Ashwini Patil Date:8-02-2024
	final GenricServiceAsync async = GWT.create(GenricService.class);
	
	public ItemProductPresenterSearchProxy() {
		super();
		createGui();
	}
	
	public void initWidget()
	{
		olbcProductCategory=new ObjectListBox<Category>();
		initalizeCategoryComboBox();
		olbUnitOfMeasurement=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbUnitOfMeasurement, Screen.UNITOFMEASUREMENT);
		
		/** Date 25-3-2019
		 * added by amol
		 */
//		prodComposite=AppUtility.initiateSalesProductComposite(new SuperProduct());
		
		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new ItemProduct());
		prodComposite=new ProductInfoComposite(querry, false,true); 
		
		
		/**
		 * 
		 */
		
		/** date 13.10.2018 added by komal for sasha**/
		olbProductGroup=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbProductGroup, Screen.PRODUCTGROUP);
		olbProductType=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbProductType, Screen.PRODUCTTYPE);
		olbProductClassification=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbProductClassification, Screen.PRODUCTCLASSIFICATION);
//		tbBrandName = new TextBox();
		oblBrandName=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(oblBrandName, Screen.BRAND);
		lbStatus= new ListBox();
		lbStatus.addItem("--Select--");
		lbStatus.addItem("Active");
		lbStatus.addItem("Inactive");
	}
	
	
	public void createScreen()
	{
		initWidget();
		FormFieldBuilder builder;
		builder = new FormFieldBuilder("Product Category",olbcProductCategory);
		FormField folbproductcategory= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Unit Of Measurement",olbUnitOfMeasurement);
		FormField folbUnitOfMeasurement= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("",prodComposite);
		FormField fprodComposite= builder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		/** date 13.10.2018 added by komal for sasha **/
		builder = new FormFieldBuilder("Product Group",olbProductGroup);
		FormField folbProductGroup= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Product Type",olbProductType);
		FormField folbProductType= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Product Classification",olbProductClassification);
		FormField folbProductClassification= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Brand Name",oblBrandName);
		FormField ftbBrandName= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Status",lbStatus);
		FormField fstatus= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		this.fields=new FormField[][]{
				{fprodComposite},
				{folbproductcategory,folbUnitOfMeasurement ,ftbBrandName},
				{folbProductGroup ,folbProductType , folbProductClassification,fstatus}/** date 13.10.2018 added by komal for sasha **/
		};
	}
	
	public MyQuerry getQuerry()
	{
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		
		
		if(olbcProductCategory.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(olbcProductCategory.getValue().trim());
			temp.setQuerryString("productCategory");
			filtervec.add(temp);
		}
		
		if(olbUnitOfMeasurement.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(olbUnitOfMeasurement.getValue().trim());
			temp.setQuerryString("unitOfMeasurement");
			filtervec.add(temp);
		}
		
		if (!prodComposite.getProdID().getValue().equals("")) {
			temp = new Filter();
			temp.setIntValue(Integer.parseInt(prodComposite.getProdID().getValue()));
			temp.setQuerryString("count");
			filtervec.add(temp);
		}
		/** date 13.10.2018 added by komal for sasha **/
		if(olbProductGroup.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(olbProductGroup.getValue().trim());
			temp.setQuerryString("productGroup");
			filtervec.add(temp);
		}
		if(olbProductType.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(olbProductType.getValue().trim());
			temp.setQuerryString("productType");
			filtervec.add(temp);
		}
		if(olbProductClassification.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(olbProductClassification.getValue().trim());
			temp.setQuerryString("productClassification");
			filtervec.add(temp);
		}
		if(oblBrandName.getValue()!=null){
			temp=new Filter();
			temp.setStringValue(oblBrandName.getValue().trim());
			temp.setQuerryString("brandName");
			filtervec.add(temp);
		}
		if(lbStatus.getSelectedIndex()!=0)
		{
			temp=new Filter();
			temp.setQuerryString("status");
			
			String selVal=lbStatus.getItemText(lbStatus.getSelectedIndex());
			if(selVal.equals("Active"))
			{
				temp.setBooleanvalue(true);
			}
			
			else
			{
				temp.setBooleanvalue(false);
			}
			
			filtervec.add(temp);
		}
		MyQuerry querry= new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new ItemProduct());
		return querry;
	}

	
	
		
	
	
	public void initalizeCategoryComboBox()
	{
	   MyQuerry querry=new MyQuerry();
	   querry.setQuerryObject(new Category());
	   Filter categoryFilter = new Filter();
	   categoryFilter.setQuerryString("isSellCategory");
	   categoryFilter.setBooleanvalue(true);
	   Filter statusfilter=new Filter();
	   statusfilter.setQuerryString("status");
	   statusfilter.setBooleanvalue(true);
	   querry.getFilters().add(categoryFilter);
	   querry.getFilters().add(statusfilter);
	   olbcProductCategory.MakeLive(querry);
	}

	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
		return super.validate();
	}

	
	

}
