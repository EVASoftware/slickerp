package com.slicktechnologies.client.views.products.itemproduct;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;

import java.util.Comparator;

import com.google.gwt.cell.client.FieldUpdater;

import java.util.List;

import com.google.gwt.user.cellview.client.Column;
import  com.google.gwt.cell.client.NumberCell;
import com.google.gwt.dom.client.Style.Unit;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.client.views.products.itemproduct.ItemproductPresenter.ItemproductPresenterTable;

public class ItemproductPresenterTableProxy extends ItemproductPresenterTable {
	  TextColumn<ItemProduct> specificationColumn;
	  TextColumn<ItemProduct> getVatTaxColumn;
	  TextColumn<ItemProduct> getProductCodeColumn;
	  TextColumn<ItemProduct> getProductNameColumn;
	  TextColumn<ItemProduct> getProductCategoryColumn;
	  Column<ItemProduct,Number> getPriceColumn;
	  TextColumn<ItemProduct> getServiceTaxColumn;
	  TextColumn<ItemProduct> getCountColumn;
	  TextColumn<ItemProduct> getStatusColumn;
	  TextColumn<ItemProduct> getUnitColumn;
	  /** date 13.10.2018 added by komal **/
	  TextColumn<ItemProduct> getBrandName;
  
  public Object getVarRef(String varName)
  {
	  if(varName.equals("specificationColumn"))
	  return this.specificationColumn;
	  if(varName.equals("getVatTaxColumn"))
	  return this.getVatTaxColumn;
	  if(varName.equals("getProductCodeColumn"))
	  return this.getProductCodeColumn;
	  if(varName.equals("getProductNameColumn"))
	  return this.getProductNameColumn;
	  if(varName.equals("getProductCategoryColumn"))
	  return this.getProductCategoryColumn;
	  if(varName.equals("getPriceColumn"))
	  return this.getPriceColumn;
	  if(varName.equals("getServiceTaxColumn"))
		  return this.getServiceTaxColumn;
	  if(varName.equals("getCountColumn"))
	  return this.getCountColumn;
	  if(varName.equals("getStatusColumn"))
			return this.getStatusColumn;
	  /** date 13.10.2018 added by komal **/
	  if(varName.equals("getBrandName"))
			return this.getBrandName;

	   return null ;
  }
  
  
  public ItemproductPresenterTableProxy()
  {
  super();
  }
  
  
  @Override public void createTable() {
		  addColumngetCount();
		  addColumngetProductName();
		  addColumngetProductCode();
		  addColumngetProductCategory();
		  addColumngetPrice();
		  addColumnSpecification();
		  /** date 13.10.2018 added by komal **/
		  addColumnBrandName();
		  addColumnUnitOfMeasurement();
		  addColumngetStatus();
  }
  
  
  @Override
  protected void initializekeyprovider() {
	  keyProvider= new ProvidesKey<ItemProduct>()
	  {
	  @Override
	  public Object getKey(ItemProduct item)
	  {
	  if(item==null)
	  {
	  return null;
	  }
	  else
	  return item.getId();
	  }
	  };
  }
 
  protected void addColumngetStatus()
	{
		getStatusColumn=new TextColumn<ItemProduct>()
				{
			@Override
			public String getValue(ItemProduct object) {
				if(object.isStatus()==true)
              	return "Active";
              else
              	return "In Active";
			} 
      };
      table.addColumn(getStatusColumn, "Status");
      table.setColumnWidth(getStatusColumn, 120, Unit.PX);
      getStatusColumn.setSortable(true);
	}
  
  
  @Override
  public void setEnable(boolean state)
  {
  }
  @Override
  public void applyStyle()
  {
  }
   public void addColumnSorting(){
	  addSortinggetCount();
	  addSortinggetProductCategory();
	  addSortinggetProductCode();
	  addSortinggetProductName();
	  addSortinggetPrice();
	  addSortingSpecification();
	  /** date 13.10.2018 added by komal**/
	  addSortingBrandName();
	  addSortingUnitOfMeasurement();
  }
   
   
  @Override public void addFieldUpdater() {
  }
  
  
  protected void addSortinggetCount()
  {
		  List<ItemProduct> list=getDataprovider().getList();
		  columnSort=new ListHandler<ItemProduct>(list);
		  columnSort.setComparator(getCountColumn, new Comparator<ItemProduct>()
		  {
		  @Override
		  public int compare(ItemProduct e1,ItemProduct e2)
		  {
		  if(e1!=null && e2!=null)
		  {
		  if(e1.getCount()== e2.getCount()){
		  return 0;}
		  if(e1.getCount()> e2.getCount()){
		  return 1;}
		  else{
		  return -1;}
		  }
		  else{
		  return 0;}
		  }
		  });
		  table.addColumnSortHandler(columnSort);
  }
  
  
  protected void addColumngetCount()
  {
	  getCountColumn=new TextColumn<ItemProduct>()
	  {
	  @Override
	  public String getValue(ItemProduct object)
	  {
	  if( object.getCount()==-1)
	  return "N.A";
	  else return object.getCount()+"";
	  }
	  };
	  table.addColumn(getCountColumn,"Id");
	  table.setColumnWidth(getCountColumn, 100, Unit.PX);
	  getCountColumn.setSortable(true);
  }
  
  
  
  protected void addSortinggetProductCategory()
  {
		  List<ItemProduct> list=getDataprovider().getList();
		  columnSort=new ListHandler<ItemProduct>(list);
		  columnSort.setComparator(getProductCategoryColumn, new Comparator<ItemProduct>()
		  {
		  @Override
		  public int compare(ItemProduct e1,ItemProduct e2)
		  {
		  if(e1!=null && e2!=null)
		  {
		  if( e1.getProductCategory()!=null && e2.getProductCategory()!=null){
		  return e1.getProductCategory().compareTo(e2.getProductCategory());}
		  }
		  else{
		  return 0;}
		  return 0;
		  }
		  });
		  table.addColumnSortHandler(columnSort);
  }
  
  
  
  protected void addColumngetProductCategory()
  {
	  getProductCategoryColumn=new TextColumn<ItemProduct>()
	  {
	  @Override
	  public String getValue(ItemProduct object)
	  {
	  return object.getProductCategory()+"";
	  }
	  };
	  table.addColumn(getProductCategoryColumn,"Category");
	  table.setColumnWidth(getProductCategoryColumn, 130, Unit.PX);
	  getProductCategoryColumn.setSortable(true);
  }
  
  
  
  protected void addSortinggetProductCode()
  {
	  List<ItemProduct> list=getDataprovider().getList();
	  columnSort=new ListHandler<ItemProduct>(list);
	  columnSort.setComparator(getProductCodeColumn, new Comparator<ItemProduct>()
	  {
	  @Override
	  public int compare(ItemProduct e1,ItemProduct e2)
	  {
	  if(e1!=null && e2!=null)
	  {
	  if( e1.getProductCode()!=null && e2.getProductCode()!=null){
	  return e1.getProductCode().compareTo(e2.getProductCode());}
	  }
	  else{
	  return 0;}
	  return 0;
	  }
	  });
	  table.addColumnSortHandler(columnSort);
  }
  
  
  
  protected void addColumngetProductCode()
  {
	  getProductCodeColumn=new TextColumn<ItemProduct>()
	  {
	  @Override
	  public String getValue(ItemProduct object)
	  {
	  return object.getProductCode()+"";
	  }
	  };
	  table.addColumn(getProductCodeColumn,"PCode");
	  table.setColumnWidth(getProductCodeColumn, 120, Unit.PX);
	  getProductCodeColumn.setSortable(true);
  }
  
  
  
  protected void addSortinggetProductName()
  {
	  List<ItemProduct> list=getDataprovider().getList();
	  columnSort=new ListHandler<ItemProduct>(list);
	  columnSort.setComparator(getProductNameColumn, new Comparator<ItemProduct>()
	  {
	  @Override
	  public int compare(ItemProduct e1,ItemProduct e2)
	  {
	  if(e1!=null && e2!=null)
	  {
	  if( e1.getProductName()!=null && e2.getProductName()!=null){
	  return e1.getProductName().compareTo(e2.getProductName());}
	  }
	  else{
	  return 0;}
	  return 0;
	  }
	  });
	  table.addColumnSortHandler(columnSort);
  }
  
  
  protected void addColumngetProductName()
	  {
	  getProductNameColumn=new TextColumn<ItemProduct>()
	  {
	  @Override
	  public String getValue(ItemProduct object)
	  {
	  return object.getProductName()+"";
	  }
	  };
	  table.addColumn(getProductNameColumn,"Name");
	  table.setColumnWidth(getProductNameColumn, 120, Unit.PX);
	  getProductNameColumn.setSortable(true);
  }
  
  
  
  protected void addSortinggetPrice()
  {
	  List<ItemProduct> list=getDataprovider().getList();
	  columnSort=new ListHandler<ItemProduct>(list);
	  columnSort.setComparator(getPriceColumn, new Comparator<ItemProduct>()
	  {
	  @Override
	  public int compare(ItemProduct e1,ItemProduct e2)
	  {
	  if(e1!=null && e2!=null)
	  {
	  if(e1.getPrice()== e2.getPrice()){
	  return 0;}
	  if(e1.getPrice()> e2.getPrice()){
	  return 1;}
	  else{
	  return -1;}
	  }
	  else{
	  return 0;}
	  }
	  });
	  table.addColumnSortHandler(columnSort);
  }
  
  
  
  protected void addColumngetPrice()
  {
	  NumberCell editCell=new NumberCell();
	  getPriceColumn=new Column<ItemProduct,Number>(editCell)
	  {
	  @Override
	  public Number getValue(ItemProduct object)
	  {
	  if(object.getPrice()==0){
	  return 0;}
	  else{
	  return object.getPrice();}
	  }
	  };
	  table.addColumn(getPriceColumn,"Price");
	  table.setColumnWidth(getPriceColumn, 120, Unit.PX);
	  getPriceColumn.setSortable(true);
  }
  
  
  
  protected void addFieldUpdatergetPrice()
  {
	  getPriceColumn.setFieldUpdater(new FieldUpdater<ItemProduct, Number>()
	  {
	  @Override
	  public void update(int index,ItemProduct object,Number value)
	  {
	  Double val1=(Double) value;
	  object.setPrice(val1);
	  table.redrawRow(index);
	  }
	  });
  }
  
  protected void addColumnUnitOfMeasurement()
  {
	  getUnitColumn=new TextColumn<ItemProduct>() {
		
		@Override
		public String getValue(ItemProduct object) {
			return object.getUnitOfMeasurement();
		}
	};
	table.addColumn(getUnitColumn,"Unit of Measuerment");
	table.setColumnWidth(getUnitColumn, 130, Unit.PX);
	getUnitColumn.setSortable(true);
  }
  
  protected void addSortingUnitOfMeasurement()
  {
	  List<ItemProduct> list=getDataprovider().getList();
	  columnSort=new ListHandler<ItemProduct>(list);
	  columnSort.setComparator(getUnitColumn, new Comparator<ItemProduct>()
	  {
	  @Override
	  public int compare(ItemProduct e1,ItemProduct e2)
	  {
	  if(e1!=null && e2!=null)
	  {
	  if( e1.getUnitOfMeasurement()!=null && e2.getUnitOfMeasurement()!=null){
	  return e1.getUnitOfMeasurement().compareTo(e2.getUnitOfMeasurement());}
	  }
	  else{
	  return 0;}
	  return 0;
	  }
	  });
	  table.addColumnSortHandler(columnSort);
  }
  
  
  protected void addSortingSpecification()
  {
	  List<ItemProduct> list=getDataprovider().getList();
	  columnSort=new ListHandler<ItemProduct>(list);
	  columnSort.setComparator(specificationColumn, new Comparator<ItemProduct>()
	  {
	  @Override
	  public int compare(ItemProduct e1,ItemProduct e2)
	  {
	  if(e1!=null && e2!=null)
	  {
	  if( e1.getSpecification()!=null && e2.getSpecification()!=null){
	  return e1.getSpecification().compareTo(e2.getSpecification());}
	  }
	  else{
	  return 0;}
	  return 0;
	  }
	  });
	  table.addColumnSortHandler(columnSort);
  }
  
  
  
  protected void addColumnSpecification()
  {
	  specificationColumn=new TextColumn<ItemProduct>()
	  {
	  @Override
	  public String getValue(ItemProduct object)
	  {
	  return object.getSpecification()+"";
	  }
	  };
	  table.addColumn(specificationColumn,"Specification");
	  table.setColumnWidth(specificationColumn, 130, Unit.PX);
	  specificationColumn.setSortable(true);
  }
  
  /** date 13.10.2018 added by komal */
  protected void addSortingBrandName()
  {
	  List<ItemProduct> list=getDataprovider().getList();
	  columnSort=new ListHandler<ItemProduct>(list);
	  columnSort.setComparator(getBrandName, new Comparator<ItemProduct>()
	  {
	  @Override
	  public int compare(ItemProduct e1,ItemProduct e2)
	  {
	  if(e1!=null && e2!=null)
	  {
	  if( e1.getBrandName()!=null && e2.getBrandName()!=null){
	  return e1.getBrandName().compareTo(e2.getBrandName());}
	  }
	  else{
	  return 0;}
	  return 0;
	  }
	  });
	  table.addColumnSortHandler(columnSort);
  }
  
  
  
  protected void addColumnBrandName()
  {
	  getBrandName=new TextColumn<ItemProduct>()
	  {
	  @Override
	  public String getValue(ItemProduct object)
	  {
		  if(object.getBrandName() != null){
			  return object.getBrandName();
		  }
		  return "";
	  } 
	  };
	  table.addColumn(getBrandName,"Brand Name");
	  table.setColumnWidth(getBrandName, 130, Unit.PX);
	  getBrandName.setSortable(true);
  }

}
