package com.slicktechnologies.client.views.products.itemproduct;

import java.util.ArrayList;
import java.util.Vector;

import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import com.simplesoftwares.client.library.*;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.*;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.*;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.*;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.*;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.*;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.client.views.products.serviceproduct.ServiceProductPresenterSearchProxy;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.simplesoftwares.client.library.appstructure.*;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.shared.TaxDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ProductInfo;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;
import com.slicktechnologies.shared.common.productlayer.*;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.google.gwt.aria.client.TextboxRole;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
/**
 * FormTablescreen template.
 */
public class ItemproductForm extends FormScreen<ItemProduct>  implements ClickHandler,ValueChangeHandler<String> {

	//***********************************Variable Declaration Starts********************************************//
	//Token to add the varialble declarations
	TaxComposite tcVatTax;/*tcLBT*/
	TaxComposite tcServiceTax;
	ObjectListBox<Category> olbcProductCategory;
	CheckBox checkBoxStatus;
	TextBox tbProductCode,tbProductName,tbSpecification;
	UploadComposite ucUploadTAndCs,ucUploadProductImage,ucUploadProductImage1,ucUploadProductImage2,ucUploadProductImage3;
	TextArea taComments,taComments1;
	DoubleBox dbProductPrice;
	TextBox tbModel,tbSerialNumber;
	/**
	 * Date  : 29-11-2018 BY ANIL
	 * updated brand name from  textbox to objectlistbox
	 */
	ObjectListBox<Config>oblBrandName;
	
	ObjectListBox<Config> olbUnitOfMeasurement;
	ObjectListBox<TaxDetails> olbTaxName;
	ObjectListBox<Config> olbProductGroup;
	ObjectListBox<Config> olbProductType;
	ObjectListBox<Config> olbProductClassification;
	TextBox tbRefNumOne,tbRefNumTwo;
	TextBox tbproductId;
	Label lable;
	/* defining warranty peroid for product in months */
	IntegerBox ibwarrantyperiod;
	ItemProduct itemproductObj;
	final GenricServiceAsync async = GWT.create(GenricService.class);
	
	/**
	 * Rohan added this code for GSC on date : 22-06-2017
	 */
	TextBox tbHSNNumber;
	
	
	/**
	 * Date : 16-04-2018 By ANIL
	 * here we are storing the reference service product against item product
	 * Requirement By : Rohan and Nitin sir.
	 * Description : for creating amc contract against sales order invoice
	 */
	
	ObjectListBox<ServiceProduct> oblServiceProduct;
	IntegerBox ibDuration;
	IntegerBox ibNo_Of_Service;
	
	
	
	/**
	 * Date : 04-07-2018 BY RAHUL
	 */
	TaxComposite tcPurTax1;
	TaxComposite tcPurTax2;
	/**
	 * Date : 04-07-2018 BY RAHUL
	 */
	DoubleBox dbProductMrp;
	
	DoubleBox dbProductSalesPrice;
	DoubleBox dbProductPurchasePrice;
	
	/** date 14.7.2018 added  by komal for margin**/
	DoubleBox dbMargin;
	//***********************************Variable Declaration Ends********************************************//
	//Token to add the concrete presenter class name
	//protected <PresenterClassName> presenter;
	boolean isProductInventoryZero=false;
	boolean isStatusChanged=false;
	
	public ItemproductForm()
	{
		super();
		createGui();
		if(AppUtility.isAutoGenerateCode("ItemProduct")){
			tbProductCode.setEnabled(false);
		}
		
		ibDuration.setEnabled(false);
		ibNo_Of_Service.setEnabled(false);
	}
	/**
	 * @author Vijay Date 25-05-2021
	 * Des :- added below constructor to show screen on Popup screen
	 * 
	 */
	public ItemproductForm(boolean popupflag)
	{
		super(popupflag);
		createGui();
		tbProductName.setEnabled(true);
		if(AppUtility.isAutoGenerateCode("ItemProduct")){
			tbProductCode.setEnabled(false);
		}
		
		ibDuration.setEnabled(false);
		ibNo_Of_Service.setEnabled(false);
	}
	
	public ItemproductForm  (String[] processlevel, FormField[][] fields,FormStyle formstyle) 
	{
		super(processlevel, fields, formstyle);
		createGui();
	}

	//***********************************Variable Initialization********************************************//
	private void initalizeWidget()
	{
		tcVatTax=new TaxComposite();
		olbcProductCategory=new ObjectListBox<Category>();
		initalizeCategoryComboBox();
		//tcVatTax=new TaxComposite();
		//tcLBT=new TaxComposite();
		tcServiceTax=new TaxComposite();
		checkBoxStatus=new CheckBox();
		checkBoxStatus.addClickHandler(this);
		
		if(AppUtility.isAutoGenerateCode("ItemProduct")){
			tbProductCode=new TextBox();
			tbProductCode.setEnabled(false);
			tbProductCode.addValueChangeHandler(this);
		}else{
			tbProductCode=new TextBox();
			tbProductCode.addValueChangeHandler(this);
		}
		
		tbProductName=new TextBox();
		/**
		 * @author Priyanka
		 * @since 31-12-2020
		 * Product name field can take change value.
		 * raised by Rahul Tiwari
		 */
		tbProductName.addValueChangeHandler(this);
		
		tbSpecification=new TextBox();
		ucUploadTAndCs=new UploadComposite();
		ucUploadProductImage=new UploadComposite();
		ucUploadProductImage1= new UploadComposite();
		ucUploadProductImage2= new UploadComposite();
		ucUploadProductImage3= new UploadComposite();
		
		lable = new Label("Note :- Plese upload image of size 400x400 pixels and size upto 20KB");
		
		taComments=new TextArea();
		taComments1=new TextArea();
		dbProductPrice=new DoubleBox();
		this.checkBoxStatus.setValue(true);
		oblBrandName=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(oblBrandName, Screen.BRAND);
		tbModel=new TextBox();
		tbSerialNumber=new TextBox();
		
		TaxDetails.initializeVatTax(tcVatTax.percentageBox);
		TaxDetails.initializeServiceTax(tcServiceTax.percentageBox);
		
		olbUnitOfMeasurement=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbUnitOfMeasurement, Screen.UNITOFMEASUREMENT);
		olbProductGroup=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbProductGroup, Screen.PRODUCTGROUP);
		olbProductType=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbProductType, Screen.PRODUCTTYPE);
		olbProductClassification=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbProductClassification, Screen.PRODUCTCLASSIFICATION);
		tbRefNumOne=new TextBox();
		tbRefNumTwo=new TextBox();
		tbproductId=new TextBox();
		tbproductId.setEnabled(false);
		
		ibwarrantyperiod = new IntegerBox();
		
		tbHSNNumber= new TextBox();
		

		
		
		oblServiceProduct=new ObjectListBox<ServiceProduct>();
		MyQuerry querry =new MyQuerry();
		Filter temp=new Filter();
		temp.setQuerryString("status");
		temp.setBooleanvalue(true);
		querry.getFilters().add(temp);
		querry.setQuerryObject(new ServiceProduct());
		oblServiceProduct.MakeLive(querry);
		
		ibDuration=new IntegerBox();
		ibNo_Of_Service=new IntegerBox();
		ibDuration.setEnabled(false);
		ibNo_Of_Service.setEnabled(false);
		
		oblServiceProduct.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				if(oblServiceProduct.getSelectedIndex()!=0){
					ServiceProduct product=oblServiceProduct.getSelectedItem();
					ibDuration.setValue(product.getDuration());
					ibNo_Of_Service.setValue(product.getNumberOfServices());
				}else{
					ibDuration.setValue(null);
					ibNo_Of_Service.setValue(null);
				}
			}
		});
		
		/**
		 * Rahul Verma added on 04 July 2018
		 */
		dbProductSalesPrice=new DoubleBox();
		dbProductPurchasePrice=new DoubleBox();
		tcPurTax1=new TaxComposite();
		tcPurTax2=new TaxComposite();
		TaxDetails.initializeVatTax(tcPurTax1.percentageBox);
		TaxDetails.initializeServiceTax(tcPurTax2.percentageBox);
		dbProductMrp=new DoubleBox();
		/** date 14.7.2018 added  by komal for margin**/
		dbMargin = new DoubleBox();
		isStatusChanged=false;
	}

	/*
	 * Method template to create the formtable screen
	 */

	@Override
	public void createScreen() {

		//Token to initialize the processlevel menus.
		initalizeWidget();

		/**
		 * @author Anil, Date: 13-06-2019
		 * added update product code button  for updating product code on product inventory view and product inventory list
		 * raised by Sonu for sasha
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("ItemProduct", "UpdateProductCode")){
			//Token to initialize the processlevel menus.
			this.processlevelBarNames=new String[]{"New",AppConstants.CREATEASSET,AppConstants.CREATEMASSASSET,"Update Product Code"};
		}else{
			//Token to initialize the processlevel menus.
			this.processlevelBarNames=new String[]{"New",AppConstants.CREATEASSET,AppConstants.CREATEMASSASSET};
		}
		//////////////////////////////////Form Field Declaration/////////////////////////////////////////////////


		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////

		
		
		//Token to initialize formfield
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingProductInformation=fbuilder.setlabel("Product Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).setKeyField(true).build();
		FormField ftbProductCode;
		if(AppUtility.isAutoGenerateCode("ItemProduct")){
			fbuilder = new FormFieldBuilder("Product Code",tbProductCode);
			ftbProductCode= fbuilder.setMandatory(false).setMandatoryMsg("Product Code is Mandatory!").setRowSpan(0).setColSpan(0).build();
			
		}else{
			fbuilder = new FormFieldBuilder("* Product Code",tbProductCode);
			ftbProductCode= fbuilder.setMandatory(true).setMandatoryMsg("Product Code is Mandatory!").setRowSpan(0).setColSpan(0).build();
			
		}
		
		
		fbuilder = new FormFieldBuilder("Product Category",olbcProductCategory);
		FormField folbcProductCategory= fbuilder.setMandatory(false)/**.setMandatoryMsg("Product Category is Mandatory!")**/.setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Product Group",olbProductGroup);
		FormField folbProductGroup= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Product Type",olbProductType);
		FormField folbProductType= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Product Classification",olbProductClassification);
		FormField folbProductClassification= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Product Name",tbProductName);
		FormField ftbProductName= fbuilder.setMandatory(true).setMandatoryMsg("Product Name is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Specification",tbSpecification);
		FormField ftbSpecification= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingPriceInformation=fbuilder.setlabel("Pricing").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingImage=fbuilder.setlabel("Terms And Condition").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("* Product Price",dbProductPrice);
		FormField fdbProductPrice= fbuilder.setMandatory(true).setMandatoryMsg("Product Price is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Status",checkBoxStatus);
		FormField fcheckBoxStatus= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		/**
		 * Date : 13-09-2017 BY JAYSHREE
		 */
		fbuilder = new FormFieldBuilder("Tax 1",tcVatTax);
		FormField ftcVatTax= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Tax 2",tcServiceTax);
		FormField ftcServiceTax= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		/**
		 * End
		 */
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingDescription=fbuilder.setlabel("Reference/General").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Description 1 (Max 500 characters)",taComments);
		FormField ftaComments= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Description 2 (Max 500 characters)",taComments1);
		FormField ftaComments1= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Upload Terms & Conditions",ucUploadTAndCs);
		FormField fucUploadTAndCs= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		FormField fuploadProductImage=fbuilder.setlabel("Image").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("",lable);
		FormField lable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		fbuilder = new FormFieldBuilder("Image 1(<200kb)",ucUploadProductImage);
		FormField fucUploadProductImage= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		fbuilder = new FormFieldBuilder("Image 2(<200kb)",ucUploadProductImage1);
		FormField fucUploadProductImage1= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		fbuilder = new FormFieldBuilder("Image 3(<200kb)",ucUploadProductImage2);
		FormField fucUploadProductImage2= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		fbuilder = new FormFieldBuilder("Image 4(<200kb)",ucUploadProductImage3);
		FormField fucUploadProductImage3= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		fbuilder=new FormFieldBuilder("Brand Name", oblBrandName);
		FormField ftbBrandName=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder=new FormFieldBuilder("Model", tbModel);
		FormField ftbModel=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder=new FormFieldBuilder("Serial Number", tbSerialNumber);
		FormField ftbSerialNumber=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder=new FormFieldBuilder("* UOM", olbUnitOfMeasurement);
		FormField folbUnitOfMeasurement=fbuilder.setMandatory(true).setMandatoryMsg("Unit Of Measurement is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder=new FormFieldBuilder("Reference No. 1", tbRefNumOne);
		FormField ftbRefNumOne=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder=new FormFieldBuilder("Reference No. 2", tbRefNumTwo);
		FormField ftbRefNumTwo=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder=new FormFieldBuilder("Product ID", tbproductId);
		FormField fibproductId=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Warranty Period(In Months)",ibwarrantyperiod);
		FormField fibwarrantyperiod = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* HSN Code",tbHSNNumber);
		FormField ftbHSNNumber= fbuilder.setMandatory(true).setMandatoryMsg("HSN Code is Mandatory..!").setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingwarrenty=fbuilder.setlabel("Product Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Service Product",oblServiceProduct);
		FormField foblServiceProduct= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Duration",ibDuration);
		FormField fibDuration= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("No. Of Service",ibNo_Of_Service);
		FormField fibNo_Of_Service= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("Purchase Tax 1",tcPurTax1);
		FormField ftcPurTax1= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Purchase Tax 2",tcPurTax2);
		FormField ftcPurTax2= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("MRP",dbProductMrp);
		FormField fdbProductMrp= fbuilder.setRowSpan(0).setColSpan(0).build();
		/**Added by Sheetal : 19-03-2022
		 * Des : Making sales price and purchase price non mandatory with process config,requirement by nitin sir**/
		FormField fdbProductSalesPrice,fdbProductPurchasePrice;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("ItemProduct", "PC_ItemProduct_SalesPriceMandatory")){
		fbuilder = new FormFieldBuilder("* Sales Price",dbProductSalesPrice);
		 fdbProductSalesPrice= fbuilder.setMandatory(true).setMandatoryMsg("Product Sales Price is Mandatory!").setRowSpan(0).setColSpan(0).build();
		}else{
			fbuilder = new FormFieldBuilder("Sales Price",dbProductSalesPrice);
			fdbProductSalesPrice= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();	
		}
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("ItemProduct", "PC_ItemProduct_PurchasePriceMandatory")){
		fbuilder = new FormFieldBuilder("* Purchase Price",dbProductPurchasePrice);
		 fdbProductPurchasePrice= fbuilder.setMandatory(true).setMandatoryMsg("Product Purchase Price is Mandatory!").setRowSpan(0).setColSpan(0).build();
		}else{
			fbuilder = new FormFieldBuilder("Purchase Price",dbProductPurchasePrice);
			fdbProductPurchasePrice= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}/**end**/
		/** date 14.7.2018 added by komal for margin**/
		fbuilder = new FormFieldBuilder("Margin %",dbMargin);
		FormField fdbMargin= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////
		/**
		 * Updated By: Viraj
		 * Date: 24-06-2019
		 * Description: Show warranty when process config is active
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("ItemProduct", "showWarrantyInfo")){
			FormField[][] formfield = {   
//					{fgroupingProductInformation},
//					{fibproductId,ftbProductCode,folbcProductCategory,ftbProductName},
//					{ftbSpecification,folbProductGroup,folbProductType,folbProductClassification},
//					{ftbRefNumOne,ftbRefNumTwo,fibwarrantyperiod,ftbHSNNumber},
//					{fcheckBoxStatus},
//					{fgroupingPriceInformation},
//					{fdbProductPurchasePrice,ftcPurTax1,ftcPurTax2,fdbMargin},/** date 14.7.2018 added by komal for margin**/
//					{fdbProductSalesPrice,ftcVatTax,ftcServiceTax,fdbProductMrp},//change sales and purchase tax position
//					{ftbBrandName,ftbModel,ftbSerialNumber,folbUnitOfMeasurement},
////					{fdbProductPurchasePrice,ftcPurTax1,ftcPurTax2},
//					{fgroupingwarrenty},
//					{foblServiceProduct,fibDuration,fibNo_Of_Service},
//					
//					{fgroupingDescription},
//					{ftaComments},
//					{ftaComments1},
//					{fgroupingImage},
//					{fucUploadTAndCs},
//					{fuploadProductImage},
//					{fucUploadProductImage,fucUploadProductImage1},
//					{fucUploadProductImage2,fucUploadProductImage3}
					
					{fgroupingProductInformation},
					{fibproductId,ftbProductName,ftbProductCode,folbUnitOfMeasurement},
					{ftbHSNNumber,fibwarrantyperiod,fcheckBoxStatus},
					
					{fgroupingPriceInformation},
					{fdbProductPurchasePrice,ftcPurTax1,ftcPurTax2,fdbMargin},/** date 14.7.2018 added by komal for margin**/
					{fdbProductSalesPrice,ftcVatTax,ftcServiceTax,fdbProductMrp},//change sales and purchase tax position
					
					{fgroupingwarrenty},
					{ftbBrandName,ftbModel,ftbSerialNumber,ftbSpecification},
					{folbProductGroup,folbcProductCategory,folbProductType,folbProductClassification},
					{foblServiceProduct,fibDuration,fibNo_Of_Service},
					
					{fgroupingDescription},
					{ftbRefNumOne,ftbRefNumTwo,},
					{ftaComments},
					{ftaComments1},
					
					{fuploadProductImage},
					{fucUploadProductImage,fucUploadProductImage1},
					{fucUploadProductImage2,fucUploadProductImage3},
					
					{fgroupingImage},
					{fucUploadTAndCs},
					
				
			};


			this.fields=formfield;
		} else {
			FormField[][] formfield = {   
//					{fgroupingProductInformation},
//					{fibproductId,ftbProductCode,folbcProductCategory,ftbProductName},
//					{ftbSpecification,folbProductGroup,folbProductType,folbProductClassification},
//					{ftbRefNumOne,ftbRefNumTwo,fibwarrantyperiod,ftbHSNNumber},
//					{fcheckBoxStatus},
//					{fgroupingPriceInformation},
//					{fdbProductPurchasePrice,ftcPurTax1,ftcPurTax2,fdbMargin},/** date 14.7.2018 added by komal for margin**/
//					{fdbProductSalesPrice,ftcVatTax,ftcServiceTax,fdbProductMrp},//change sales and purchase tax position
//					{ftbBrandName,ftbModel,ftbSerialNumber,folbUnitOfMeasurement},
////					{fdbProductPurchasePrice,ftcPurTax1,ftcPurTax2},
////					{fgroupingwarrenty},
////					{foblServiceProduct,fibDuration,fibNo_Of_Service},
//					
//					{fgroupingDescription},
//					{ftaComments},
//					{ftaComments1},
//					{fgroupingImage},
//					{fucUploadTAndCs},
//					{fuploadProductImage},
//					{fucUploadProductImage,fucUploadProductImage1},
//					{fucUploadProductImage2,fucUploadProductImage3}
					
					{fgroupingProductInformation},
					{fibproductId,ftbProductName,ftbProductCode,folbUnitOfMeasurement},
					{ftbHSNNumber,fibwarrantyperiod,fcheckBoxStatus},
					
					{fgroupingPriceInformation},
					{fdbProductPurchasePrice,ftcPurTax1,ftcPurTax2,fdbMargin},/** date 14.7.2018 added by komal for margin**/
					{fdbProductSalesPrice,ftcVatTax,ftcServiceTax,fdbProductMrp},//change sales and purchase tax position
					
					{fgroupingwarrenty},
					{ftbBrandName,ftbModel,ftbSerialNumber,ftbSpecification},
					{folbProductGroup,folbcProductCategory,folbProductType,folbProductClassification},
					{foblServiceProduct,fibDuration,fibNo_Of_Service},
					
					{fgroupingDescription},
					{ftbRefNumOne,ftbRefNumTwo,},
					{ftaComments},
					{ftaComments1},
					
					{fuploadProductImage},
					{fucUploadProductImage,fucUploadProductImage1},
					{fucUploadProductImage2,fucUploadProductImage3},
					
					{fgroupingImage},
					{fucUploadTAndCs},
			};


			this.fields=formfield;
		}
		/** Ends **/
	}

	/**
	 * method template to update the model with token entity name
	 */
	@Override
	public void updateModel(ItemProduct model) 
	{

		if(tbProductCode.getValue()!=null)
			model.setProductCode(tbProductCode.getValue());
		if(olbcProductCategory.getValue()!=null)
			model.setProductCategory(olbcProductCategory.getValue());
		if(tbProductName.getValue()!=null)
			model.setProductName(tbProductName.getValue());
		if(tbSpecification.getValue()!=null)
			model.setSpecifications(tbSpecification.getValue());
		if(olbProductGroup.getValue()!=null)
			model.setProductGroup(olbProductGroup.getValue());
		if(olbProductType.getValue()!=null)
			model.setProductType(olbProductType.getValue());
		if(olbProductClassification.getValue()!=null)
			model.setProductClassification(olbProductClassification.getValue());
		if(dbProductPrice.getValue()!=null)
			model.setPrice(dbProductPrice.getValue());
		if(checkBoxStatus.getValue()!=null)
			model.setStatus(checkBoxStatus.getValue());
		if(tcVatTax.getValue()!=null)
			model.setVatTax(tcVatTax.getValue());
		if(tcServiceTax.getValue()!=null)
			model.setServiceTax(tcServiceTax.getValue());
		if(taComments.getValue()!=null)
			model.setComment(taComments.getValue());
		if(taComments1.getValue()!=null)
			model.setCommentdesc(taComments1.getValue());
		
			model.setTermsAndConditions(ucUploadTAndCs.getValue());
		
			model.setProductImage(ucUploadProductImage.getValue());
			model.setProductImage1(ucUploadProductImage1.getValue());
			model.setProductImage2(ucUploadProductImage2.getValue());
			model.setProductImage3(ucUploadProductImage3.getValue());
		
		if(oblBrandName.getValue()!=null)
			model.setBrandName(oblBrandName.getValue());
		if(tbModel.getValue()!=null)
			model.setModel(tbModel.getValue());
		if(tbSerialNumber.getValue()!=null)
			model.setSerialNumber(tbSerialNumber.getValue());
		if(olbUnitOfMeasurement.getValue()!=null)
			model.setUnitOfMeasurement(olbUnitOfMeasurement.getValue());
		if(tbRefNumOne.getValue()!=null)
			model.setRefNumber1(tbRefNumOne.getValue());
		if(tbRefNumTwo.getValue()!=null)
			model.setRefNumber2(tbRefNumTwo.getValue());
		
		if(ibwarrantyperiod.getValue()!=null){
			model.setWarrantyPeriodInMonths(ibwarrantyperiod.getValue());
		}
		
		if(tbHSNNumber.getValue()!= null){
			model.setHsnNumber(tbHSNNumber.getValue());
		}
		
		if(oblServiceProduct.getSelectedIndex()!=0){
			ServiceProduct serProd=oblServiceProduct.getSelectedItem();
//			model.setServiceProduct(serProd);
			model.setSerProdId(serProd.getCount());
			model.setSerProdCode(serProd.getProductCode());
			model.setSerProdName(serProd.getProductName());
			model.setNoOfServices(serProd.getNumberOfServices());
			model.setWarrantyDuration(serProd.getDuration());
		}else{
			model.setSerProdId(0);
			model.setSerProdCode("");
			model.setSerProdName("");
			model.setNoOfServices(0);
			model.setWarrantyDuration(0);
		}
		
		if(dbProductSalesPrice.getValue()!=null)
			model.setPrice(dbProductSalesPrice.getValue());
		
		if(dbProductPurchasePrice.getValue()!=null){
			model.setPurchasePrice(dbProductPurchasePrice.getValue());
		}
		if(tcPurTax1.getValue()!=null){
			model.setPurchaseTax1(tcPurTax1.getValue());
		}
		if(tcPurTax2.getValue()!=null){
			model.setPurchaseTax2(tcPurTax2.getValue());
		}
		if(dbProductMrp.getValue()!=null){
			model.setMrp(dbProductMrp.getValue());
		}
		/** date 14.7.2018 added by komal for margin**/
		if(dbMargin.getValue()!=null){
			model.setMargin(dbMargin.getValue());
		}
		
		itemproductObj=model;
		presenter.setModel(model);

	}

	/**
	 * method template to update the view with token entity name
	 */
	@Override
	public void updateView(ItemProduct view) 
	{
		itemproductObj=view;
		
		tbproductId.setValue(view.getCount()+"");
		if(view.getProductCode()!=null)
			tbProductCode.setValue(view.getProductCode());
		if(view.getProductCategory()!=null)
			olbcProductCategory.setValue(view.getProductCategory());
		if(view.getProductName()!=null)
			tbProductName.setValue(view.getProductName());
		if(view.getSpecifications()!=null)
			tbSpecification.setValue(view.getSpecifications());
		if(view.getProductGroup()!=null)
			olbProductGroup.setValue(view.getProductGroup());
		if(view.getProductType()!=null)
			olbProductType.setValue(view.getProductType());
		if(view.getProductClassification()!=null)
			olbProductClassification.setValue(view.getProductClassification());
		if(view.getPrice()!=null)
			dbProductPrice.setValue(view.getPrice());
		if(view.isStatus()!=null)
			checkBoxStatus.setValue(view.isStatus());
		if(view.getVatTax()!=null)
			tcVatTax.setValue(view.getVatTax());
		if(view.getServiceTax()!=null)
			tcServiceTax.setValue(view.getServiceTax());
		if(view.getComment()!=null)
			taComments.setValue(view.getComment());
		if(view.getCommentdesc()!=null)
			taComments1.setValue(view.getCommentdesc());
		if(view.getTermsAndConditions()!=null)
			ucUploadTAndCs.setValue(view.getTermsAndConditions());
		if(view.getProductImage()!=null)
			ucUploadProductImage.setValue(view.getProductImage());
		
		if(view.getProductImage1()!=null)
			ucUploadProductImage1.setValue(view.getProductImage1());
		if(view.getProductImage2()!=null)
			ucUploadProductImage2.setValue(view.getProductImage2());
		if(view.getProductImage3()!=null)
			ucUploadProductImage3.setValue(view.getProductImage3());
		
		
		if(view.getBrandName()!=null)
			oblBrandName.setValue(view.getBrandName());
		if(view.getModel()!=null)
			tbModel.setValue(view.getModel());
		if(view.getSerialNumber()!=null)
			tbSerialNumber.setValue(view.getSerialNumber());
		if(view.getUnitOfMeasurement()!=null)
			olbUnitOfMeasurement.setValue(view.getUnitOfMeasurement());
		if(view.getRefNumber1()!=null)
			tbRefNumOne.setValue(view.getRefNumber1());
		if(view.getRefNumber2()!=null)
			tbRefNumTwo.setValue(view.getRefNumber2());
		
		if(view.getWarrantyPeriodInMonths()!=null){
			ibwarrantyperiod.setValue(view.getWarrantyPeriodInMonths());
		}
		
		if(view.getHsnNumber()!= null){
			tbHSNNumber.setValue(view.getHsnNumber());
		}
		
//		if(view.getServiceProduct()!=null){
//			oblServiceProduct.setValue(view.getServiceProduct().getProductName());
//			ibDuration.setValue(view.getServiceProduct().getDuration());
//			ibNo_Of_Service.setValue(view.getServiceProduct().getNumberOfServices());
//		}
		
		if(view.getSerProdName()!=null){
			oblServiceProduct.setValue(view.getSerProdName());
			ibDuration.setValue(view.getWarrantyDuration());
			ibNo_Of_Service.setValue(view.getNoOfServices());
		}
		if(view.getPrice()!=null)
			dbProductSalesPrice.setValue(view.getPrice());
		
		/**
		 * Date :04-01-2018   BY ANIL
		 */
		if(view.getPurchaseTax1()!=null){
			tcPurTax1.setValue(view.getPurchaseTax1());
		}
		if(view.getPurchaseTax2()!=null){
			tcPurTax2.setValue(view.getPurchaseTax2());
		}
		if(view.getMrp()!=0){
			dbProductMrp.setValue(view.getMrp());
		}
		
		dbProductPurchasePrice.setValue(view.getPurchasePrice());
		/** date 14.7.2018 added by komal for margin**/
		dbMargin.setValue(view.getMargin());
		
		presenter.setModel(view);

	}

	/**
	 * Toggles the app header bar menus as per screen state
	 */

	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			/**
			 * @author Vijay Date - 21-04-2021
			 * Des :- updated code to manage popup and regular screen app level menus
			 */
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
				 System.out.println("popup menus");
			}else{
				 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				 System.out.println("Normal menus");
			}
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals("Search")||text.equals(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			/**
			 * @author Vijay Date - 21-04-2021
			 * Des :- updated code to manage popup and regular screen app level menus
			 */
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			}
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			/**
			 * @author Vijay Date - 21-04-2021
			 * Des :- updated code to manage popup and regular screen app level menus
			 */
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			}
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit")||text.equals("Search")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 
				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.ITEMPRODUCT,LoginPresenter.currentModule.trim());
	}
	
	

		@Override
	public void setToEditState() {
		super.setToEditState();
		/**
		 * @author Anil,Date : 25-04-2019
		 * Making Product code editable in Edit mode
		 * raised by Sonu/Nitin Sasha for Sasha
		 */
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("ItemProduct", "MakeProductCodeEditableInEditMode")){
			this.tbProductCode.setEnabled(true);
		}else{
			this.tbProductCode.setEnabled(false);
		}
		 Console.log("inside edit state item prod");
		 if(itemproductObj.isFreez()){
			 Console.log("product name disable in edit state");
			this.tbProductName.setEnabled(false);	
		}
		
		
		this.processLevelBar.setVisibleFalse(false);
	}


		@Override
		public void setToViewState() {
			super.setToViewState();
			
			setToggleMenuAsPerProduct();
			
			SuperModel model=new ItemProduct();
			model=itemproductObj;
			
			try{
				if(itemproductObj.isFreez()){
					 Console.log("product name disable");
					this.tbProductName.setEnabled(false);	
				}
				AppUtility.addDocumentToHistoryTable(AppConstants.PRODUCTMODULE,AppConstants.ITEMPRODUCT, itemproductObj.getCount(), null,null,null, false, model, null);
			}catch(Exception e){
				
			}
			/***
			 * @author Anil @since 29-09-2021
			 * After creating new product it should be reflected on product search composite
			 * raised by Nitin Sir
			 */
			try {
				ProductInfo info = new ProductInfo();
				info.setProdID(this.getPresenter().getModel().getCount());
				info.setProductCode(itemproductObj.getProductCode());
				info.setProductName(itemproductObj.getProductName());
				if(itemproductObj.getProductCategory()!=null){
					info.setProductCategory(itemproductObj.getProductCategory());
				}
				info.setProductPrice(itemproductObj.getPrice());
				info.setStatus(itemproductObj.isStatus());
				ItemProductPresenterSearchProxy search = (ItemProductPresenterSearchProxy) this.getSearchpopupscreen();
				search.prodComposite.setProductInfoHashMap(info);
				ProductInfoComposite.globalItemProdArray.add(info);	
			} catch (Exception e) {
				e.printStackTrace();
				Console.log("Exception View Method adding product.."+e.getMessage());
			}
			
			/**
			 * ends here
			 */
		}
		/**
		 * This method manage toggle button as per product category.
		 * Date : 21-10-2016 By Anil
		 * Release : 30 Sept 2016 
		 * Project : Purchase Modification (NBHC)
		 */
		private void setToggleMenuAsPerProduct() {
			ItemProduct entity=(ItemProduct) presenter.getModel();
			
			for (int i = 0; i < getProcesslevelBarNames().length; i++) {
				InlineLabel label = getProcessLevelBar().btnLabels[i];
				String text = label.getText().trim();
				
				if (!entity.getProductCategory().equals("ASSET")) {
					if (text.equals(AppConstants.NEW)){
						label.setVisible(true);
					}
					if (text.equals(AppConstants.CREATEASSET)){
						label.setVisible(false);
					}
					if (text.equals(AppConstants.CREATEMASSASSET)){
						label.setVisible(false);
					}
					
					/**
					 * @author Anil , Date : 13-06-2019
					 */
					if (text.equals("Update Product Code")){
						label.setVisible(true);
					}
				} 
			}
		}

		/**
		 * @author Priyanka
		 * @since 31-12-2020
		 * Product name field can take change value.
		 * raised by Rahul Tiwari
		 */
		public void checkItemProductName()
		{
			MyQuerry querry = new MyQuerry();
			Company c=new Company();
			Vector<Filter> filtervec=new Vector<Filter>();
			Filter filter = null;
			
			filter = new Filter();
			filter.setQuerryString("companyId");
			filter.setLongValue(c.getCompanyId());
			filtervec.add(filter);
			
			filter = new Filter();
			filter.setQuerryString("productName");
			filter.setStringValue(tbProductName.getValue().trim());
			filtervec.add(filter);
			
			querry.setFilters(filtervec);
			querry.setQuerryObject(new SuperProduct());
			this.showWaitSymbol();
			async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
		
				@Override
				public void onFailure(Throwable caught) {
					hideWaitSymbol();
					showDialogMessage("An Unexpected Error occured !");
				}
		
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					/**
					 * @author Anil , Date : 25-04-2019
					 * Product code validation in edit mode if code is editable
					 */
					hideWaitSymbol();
					if(result.size()>0){
						
						if(result.size()>1){
							showDialogMessage("Product Name Already Exists!");
							tbProductName.setValue("");
						}else if(result.size()==1){
							if(tbproductId.getValue()!=null&&!tbproductId.getValue().equals("")){
								int productId=0;
								try{
									productId=Integer.parseInt(tbproductId.getValue().trim());
									if(result.get(0).getCount()!=productId){
										showDialogMessage("Product Name Already Exists!");
										tbProductName.setValue("");
									}
									
								}catch(Exception e){
									showDialogMessage("Product Name Already Exists!");
									tbProductName.setValue("");
								}
							}else{
								showDialogMessage("Product Name Already Exists!");
								tbProductName.setValue("");
							}
						}
						
					}
					
				}		
			});
		}
		
		
		
		/**
		 * 
		 */
		
		public void checkItemProductCode()
		{
			MyQuerry querry = new MyQuerry();
			Company c=new Company();
			Vector<Filter> filtervec=new Vector<Filter>();
			Filter filter = null;
			
			filter = new Filter();
			filter.setQuerryString("companyId");
			filter.setLongValue(c.getCompanyId());
			filtervec.add(filter);
			
			filter = new Filter();
			filter.setQuerryString("productCode");
			filter.setStringValue(tbProductCode.getValue().trim());
			filtervec.add(filter);
			
			querry.setFilters(filtervec);
			querry.setQuerryObject(new SuperProduct());
			this.showWaitSymbol();
			async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
		
				@Override
				public void onFailure(Throwable caught) {
					hideWaitSymbol();
					showDialogMessage("An Unexpected Error occured !");
				}
		
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					/**
					 * @author Anil , Date : 25-04-2019
					 * Product code validation in edit mode if code is editable
					 */
					hideWaitSymbol();
					if(result.size()>0){
						
						if(result.size()>1){
							showDialogMessage("Product Code Already Exists!");
							tbProductCode.setValue("");
						}else if(result.size()==1){
							if(tbproductId.getValue()!=null&&!tbproductId.getValue().equals("")){
								int productId=0;
								try{
									productId=Integer.parseInt(tbproductId.getValue().trim());
									if(result.get(0).getCount()!=productId){
										showDialogMessage("Product Code Already Exists!");
										tbProductCode.setValue("");
									}
									
								}catch(Exception e){
									showDialogMessage("Product Code Already Exists!");
									tbProductCode.setValue("");
								}
							}else{
								showDialogMessage("Product Code Already Exists!");
								tbProductCode.setValue("");
							}
						}
						
					}
					
				}		
			});
		}
		
		
	/**
	 * sets the id textbox with the passed count value. 
	 */
	@Override
	public void setCount(int count)
	{
		tbproductId.setValue(count+"");
	}
	
	@Override
	public void clear()
	{
		super.clear();
		this.checkBoxStatus.setValue(true);
	}


	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		
		tbproductId.setEnabled(false);
		
		ibDuration.setEnabled(false);
		ibNo_Of_Service.setEnabled(false);
		
		/**
		 * @author Vijay  Date :- 04-09-2020
		 * Des :- if stock master defined for chemical and then user changed the name of this product
		 * then it will not update in the stock master so if user try to search the stock master for the product
		 * then it will not search because of name will mismatch in product master and stock master
		 * so here not allowing to edit Product Name As per Nitin Sir
		 */
		tbProductName.setEnabled(false);
		
		if(popUpAppMenubar && AppMemory.getAppMemory().currentState==ScreeenState.NEW){
			tbProductName.setEnabled(true);
		}
	}

	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
		
		if(event.getSource().equals(tbProductCode)){
			if(!tbProductCode.getValue().equals("")){
				checkItemProductCode();
			}
		}
		
		/**
		 * @author Priyanka
		 * @since 31-12-2020
		 * Product name field can take change value.
		 * raised by Rahul Tiwari
		 */
		if(event.getSource().equals(tbProductName)){
			if(!tbProductName.getValue().equals("")){
				checkItemProductName();
			}
		}
	}
	
	@Override
	public boolean validate() {
		boolean superRes= super.validate();
		boolean descBox=true;
		boolean descBox1Limit=true,descBox2Limit=true;
		
		
//		if(flag==false)
//		{
//			this.showDialogMessage("Product Code already exists!");
//			prodCode=false;
//		}
		
		if(taComments.getValue().equals("")&&!taComments1.getValue().equals("")){
			showDialogMessage("Description 1 cannot be empty!");
			descBox=false;
		}
		
		int commentsLength=taComments.getValue().length();
		int comments1Length=taComments1.getValue().length();
		if(commentsLength>=500)
		{
			descBox1Limit=false;
			showDialogMessage("Description 1 text exceeds 500 characters.");
		}
		if(comments1Length>=500)
		{
			descBox2Limit=false;
			showDialogMessage("Description 2 text exceeds 500 characters.");
		}
		
		/**
		 * Date 31-08-2019 by Vijay 
		 * Des :- NBHC Inventory Management Price non zero
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("ItemProduct", "EnableSalesPriceNonZero") && 
				dbProductSalesPrice.getValue()!=null){
			if(dbProductSalesPrice.getValue()==0){
				showDialogMessage("Purchase Price should not be 0");
				return false;
			}
			else if(dbProductSalesPrice.getValue()<0){
				showDialogMessage("Purchase Price should not be less than 0");
				return false;
			}
		}
		/**
		 * ends here
		 */
		

		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("ItemProduct", AppConstants.PC_ENABLETAXMANDATORY)){
			
			if(tcPurTax1.getValue().getPercentage()==0 && tcPurTax2.getValue().getPercentage()==0){
				showDialogMessage("Purchase Tax is mandatory");
				return false;
			}
		}
		
		/*
		 * Ashwini Patil
		 * Date:8-02-2024
		 * Inactive products are getting displayed in closing stock report
		 * So we are making sure that when you set product inactive, there should not be any stock of that product in any of your warehouse
		 * Then we will excluse inactive products from closing stock report
		 */
		Console.log("isStatusChanged="+isStatusChanged);
		if(isStatusChanged&&itemproductObj!=null&&itemproductObj.getCount()!=0&& checkBoxStatus.getValue()!=null&&checkBoxStatus.getValue()==false){
			if(!isProductInventoryZero) {
				showDialogMessage("To make product inactive, you have to set available stock to zero in all warehouses. Go to Inventory menu -> Product Inventory view -> search product -> set available quantity to zero in all warehouses and save. Then come back to item product screen and inactive the product.");
				return false;
			}			
		}
		return superRes&&descBox&&descBox1Limit&&descBox2Limit;
	}


	public void initalizeCategoryComboBox()
	{
	   MyQuerry querry=new MyQuerry();
	   querry.setQuerryObject(new Category());
	   Filter categoryFilter = new Filter();
	   categoryFilter.setQuerryString("isSellCategory");
	   categoryFilter.setBooleanvalue(true);
	   Filter statusfilter=new Filter();
	   statusfilter.setQuerryString("status");
	   statusfilter.setBooleanvalue(true);
	   querry.getFilters().add(categoryFilter);
	   querry.getFilters().add(statusfilter);
	   olbcProductCategory.MakeLive(querry);
	}
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		if(event.getSource()==checkBoxStatus) {
			/*
			 * Ashwini Patil
			 * Date:8-02-2024
			 * Inactive products are getting displayed in closing stock report
			 * So we are making sure that when you set product inactive, there should not be any stock of that product in any of your warehouse
			 * Then we will excluse inactive products from closing stock report
			 */
			isStatusChanged=true;
			if(itemproductObj!=null&&itemproductObj.getCount()!=0&& checkBoxStatus.getValue()!=null&&checkBoxStatus.getValue()==false){
				Vector<Filter> filtervec = new Vector<Filter>();
				Filter temp = null;
				
					temp = new Filter();
					temp.setStringValue(itemproductObj.getProductName());
					temp.setQuerryString("productinfo.productName");
					filtervec.add(temp);
				
					temp = new Filter();
					temp.setStringValue(itemproductObj.getProductCode());
					temp.setQuerryString("productinfo.productCode");
					filtervec.add(temp);
					
					temp = new Filter();
					temp.setIntValue(itemproductObj.getCount());
					temp.setQuerryString("productinfo.prodID");
					filtervec.add(temp);
					
					MyQuerry querry = new MyQuerry();
					querry.setFilters(filtervec);
					querry.setQuerryObject(new ProductInventoryView());
					
					async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {

						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							Console.log("Failed to fetch product inventory view");
						}

						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							// TODO Auto-generated method stub
							Console.log("fetched product inventory view");
							if(result==null||result.size()==0){
								Console.log("result null or zero");
								isProductInventoryZero=true;
							}else{
								Console.log("in else result size="+result.size());
								for (SuperModel model : result){
									ProductInventoryView view=(ProductInventoryView) model;
									ArrayList<ProductInventoryViewDetails>  details=view.getDetails();
									if(details!=null){
										boolean enteredinIf=false;
										for(ProductInventoryViewDetails detail:details){
											if(detail.getAvailableqty()>0){
												Console.log("detail.getAvailableqty()="+detail.getAvailableqty());
												isProductInventoryZero=false;
												enteredinIf=true;
												break;
											}
										}
											if(!enteredinIf)
												isProductInventoryZero=true;
									}
								}
							}
						}
					});
					
			}else{
				isProductInventoryZero=true;
			}
		}
	}
	

	

}
