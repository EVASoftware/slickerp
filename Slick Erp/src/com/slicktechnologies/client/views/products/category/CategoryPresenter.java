package com.slicktechnologies.client.views.products.category;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.InlineLabel;

import java.util.List;
import java.util.Vector;

import com.simplesoftwares.client.library.appstructure.search.*;
import com.simplesoftwares.client.library.appstructure.formtablescreen.*;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.productlayer.*;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.google.gwt.core.shared.GWT;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utility.Screen;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
/**
 *  FormTableScreen presenter template
 */
public class CategoryPresenter extends FormTableScreenPresenter<Category>{

	//Token to set the concrete form
	CategoryForm form;
	
	public CategoryPresenter (FormTableScreen<Category> view,
		Category model) {
		super(view, model);
		form=(CategoryForm) view;
		form.getSupertable().connectToLocal();
		form.retriveTable(CategoryQuery());
		form.setPresenter(this);
		
		 boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.CATEGORY,LoginPresenter.currentModule.trim());
			if(isDownload==false){
				form.getSearchpopupscreen().getDwnload().setVisible(false);
			}
	}

	/**
	 * Method template to set the processBar events
	 */
	@Override
	public void reactToProcessBarEvents(ClickEvent e) 
   {
		
		InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();
		
		
		if(label.getText().contains("Delete"))
		{
			
			if(this.form.validate())
			{
			List<Category>data=form.getSupertable().getDataprovider().getList();
			/*
			 * Deleting
			 */
			for(int i=0;i<data.size();i++)
			{
				if(data.get(i).getId().equals(model.getId()))
				{
					data.remove(i);
				}
				form.getSupertable().getDataprovider().refresh();
			}
			reactOnDelete();
			
			}
		}

		if(text.equals(AppConstants.NEW))
			reactToNew();
		

	}
	
	
	
	private void reactToNew()
	{
		form.setToNewState(); 
		this.initalize();
		form.toggleAppHeaderBarMenu();
	}

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

	/**
	 * Method template to set new model
	 */
	@Override
	protected void makeNewModel() {
		
		model=new Category();
	}
	
	/*
	 * Method template to set Myquerry object
	 */
	
	public MyQuerry CategoryQuery()
	{
		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new Category());
		return quer;
	}
	
	public void setModel(com.slicktechnologies.shared.common.productlayer.Category entity)
	{
		model=entity;
	}
	
	public static void initalize()
	{
			 CategoryPresenterTable gentableScreen=new CategoryPresenterTableProxy();
			
			CategoryForm  form=new  CategoryForm(gentableScreen,FormTableScreen.UPPER_MODE,true);
			gentableScreen.setView(form);
			gentableScreen.applySelectionModle();
			
			  //// CategoryPresenterTable gentableSearch=GWT.create( CategoryPresenterTable.class);
			  ////   CategoryPresenterSearch.staticSuperTable=gentableSearch;
			  ////     CategoryPresenterSearch searchpopup=GWT.create( CategoryPresenterSearch.class);
			  ////         form.setSearchpopupscreen(searchpopup);
			
			 CategoryPresenter  presenter=new  CategoryPresenter  (form,new Category());
			AppMemory.getAppMemory().stickPnel(form);
			
			
		 
	}
	
	
		@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.productlayer.Category")
		 public static class  CategoryPresenterTable extends SuperTable<Category> implements GeneratedVariableRefrence{

			@Override
			public Object getVarRef(String varName) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void createTable() {
				// TODO Auto-generated method stub
				
			}

			@Override
			protected void initializekeyprovider() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void addFieldUpdater() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void setEnable(boolean state) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void applyStyle() {
				// TODO Auto-generated method stub
				
			}} ;
			
			@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.productlayer.Category")
			 public static class  CategoryPresenterSearch extends SearchPopUpScreen<Category>{

				@Override
				public MyQuerry getQuerry() {
					// TODO Auto-generated method stub
					return null;
				}

				@Override
				public boolean validate() {
					// TODO Auto-generated method stub
					return true;
				}};
				

}

