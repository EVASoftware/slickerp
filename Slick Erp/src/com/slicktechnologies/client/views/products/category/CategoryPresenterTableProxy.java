package com.slicktechnologies.client.views.products.category;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;
import java.util.Comparator;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.google.gwt.cell.client.FieldUpdater;
import java.util.List;
import java.util.Date;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.cell.client.EditTextCell;
import  com.google.gwt.cell.client.NumberCell;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import  com.google.gwt.cell.client.DatePickerCell;
import com.slicktechnologies.shared.common.productlayer.Category;
import com.slicktechnologies.client.views.products.category.CategoryPresenter.CategoryPresenterTable;
import com.slicktechnologies.client.utility.Screen;

public class CategoryPresenterTableProxy extends CategoryPresenterTable {
  TextColumn<Category> getCatCodeColumn;
  TextColumn<Category> getCatNameColumn;
  TextColumn<Category> getStatusColumn;
  TextColumn<Category> getIsSellCategoryColumn;
  TextColumn<Category> isServiceCategoryColumn;
  TextColumn<Category> getCountColumn;
  public Object getVarRef(String varName)
  {
  if(varName.equals("getCatCodeColumn"))
  return this.getCatCodeColumn;
  if(varName.equals("getCatNameColumn"))
  return this.getCatNameColumn;
  if(varName.equals("getStatusColumn"))
  return this.getStatusColumn;
  if(varName.equals("getIsSellCategoryColumn"))
  return this.getIsSellCategoryColumn;
  if(varName.equals("isServiceCategoryColumn"))
  return this.isServiceCategoryColumn;
  if(varName.equals("getCountColumn"))
  return this.getCountColumn;
   return null ;
  }
  public CategoryPresenterTableProxy()
  {
  super();
  }
  @Override public void createTable() {
  addColumngetCount();
  addColumngetCatName();
  addColumngetCatCode();
  addColumnisServiceCategory();
  addColumngetIsSellCategory();
  addColumngetStatus();
  }
  @Override
  protected void initializekeyprovider() {
  keyProvider= new ProvidesKey<Category>()
  {
  @Override
  public Object getKey(Category item)
  {
  if(item==null)
  {
  return null;
  }
  else
  return item.getId();
  }
  };
  }
  @Override
  public void setEnable(boolean state)
  {
  }
  @Override
  public void applyStyle()
  {
  }
   public void addColumnSorting(){
  addSortinggetCount();
  addSortinggetCatCode();
  addSortinggetCatName();
  addSortinggetStatus();
  addSortinggetIsSellCategory();
  addSortingisServiceCategory();
  }
  @Override public void addFieldUpdater() {
  }
  protected void addSortinggetCount()
  {
  List<Category> list=getDataprovider().getList();
  columnSort=new ListHandler<Category>(list);
  columnSort.setComparator(getCountColumn, new Comparator<Category>()
  {
  @Override
  public int compare(Category e1,Category e2)
  {
  if(e1!=null && e2!=null)
  {
  if(e1.getCount()== e2.getCount()){
  return 0;}
  if(e1.getCount()> e2.getCount()){
  return 1;}
  else{
  return -1;}
  }
  else{
  return 0;}
  }
  });
  table.addColumnSortHandler(columnSort);
  }
  protected void addColumngetCount()
  {
  getCountColumn=new TextColumn<Category>()
  {
  @Override
  public String getValue(Category object)
  {
  if( object.getCount()==-1)
  return "N.A";
  else return object.getCount()+"";
  }
  };
  table.addColumn(getCountColumn,"Id");
  getCountColumn.setSortable(true);
  }
  protected void addSortinggetCatCode()
  {
  List<Category> list=getDataprovider().getList();
  columnSort=new ListHandler<Category>(list);
  columnSort.setComparator(getCatCodeColumn, new Comparator<Category>()
  {
  @Override
  public int compare(Category e1,Category e2)
  {
  if(e1!=null && e2!=null)
  {
  if( e1.getCatCode()!=null && e2.getCatCode()!=null){
  return e1.getCatCode().compareTo(e2.getCatCode());}
  }
  else{
  return 0;}
  return 0;
  }
  });
  table.addColumnSortHandler(columnSort);
  }
  protected void addColumngetCatCode()
  {
  getCatCodeColumn=new TextColumn<Category>()
  {
  @Override
  public String getValue(Category object)
  {
  return object.getCatCode()+"";
  }
  };
  table.addColumn(getCatCodeColumn,"Category Code");
  getCatCodeColumn.setSortable(true);
  }
  protected void addSortinggetCatName()
  {
  List<Category> list=getDataprovider().getList();
  columnSort=new ListHandler<Category>(list);
  columnSort.setComparator(getCatNameColumn, new Comparator<Category>()
  {
  @Override
  public int compare(Category e1,Category e2)
  {
  if(e1!=null && e2!=null)
  {
  if( e1.getCatName()!=null && e2.getCatName()!=null){
  return e1.getCatName().compareTo(e2.getCatName());}
  }
  else{
  return 0;}
  return 0;
  }
  });
  table.addColumnSortHandler(columnSort);
  }
  protected void addColumngetCatName()
  {
  getCatNameColumn=new TextColumn<Category>()
  {
  @Override
  public String getValue(Category object)
  {
  return object.getCatName()+"";
  }
  };
  table.addColumn(getCatNameColumn,"Category Name");
  getCatNameColumn.setSortable(true);
  }
  protected void addSortinggetStatus()
  {
  List<Category> list=getDataprovider().getList();
  columnSort=new ListHandler<Category>(list);
  columnSort.setComparator(getStatusColumn, new Comparator<Category>()
  {
  @Override
  public int compare(Category e1,Category e2)
  {
  if(e1!=null && e2!=null)
  {
  if(e1.getStatus()== e2.getStatus()){
  return 0;}
  else{
  return -1;}
  }
  else{
  return 0;}
  }
  });
  table.addColumnSortHandler(columnSort);
  }
  protected void addColumngetStatus()
  {
  getStatusColumn=new TextColumn<Category>()
  {
  @Override
  public String getValue(Category object)
  {
  if( object.getStatus()==true)
  return "Active";
  else 
  return "In Active";
  }
  };
  table.addColumn(getStatusColumn,"Category Status");
  getStatusColumn.setSortable(true);
  }
  protected void addSortinggetIsSellCategory()
  {
  List<Category> list=getDataprovider().getList();
  columnSort=new ListHandler<Category>(list);
  columnSort.setComparator(getIsSellCategoryColumn, new Comparator<Category>()
  {
  @Override
  public int compare(Category e1,Category e2)
  {
  if(e1!=null && e2!=null)
  {
  if(e1.getIsSellCategory()== e2.getIsSellCategory()){
  return 0;}
  else{
  return -1;}
  }
  else{
  return 0;}
  }
  });
  table.addColumnSortHandler(columnSort);
  }
  protected void addColumngetIsSellCategory()
  {
  getIsSellCategoryColumn=new TextColumn<Category>()
  {
  @Override
  public String getValue(Category object)
  {
  if( object.getIsSellCategory()==true)
  return "Yes";
  else 
  return "No";
  }
  };
  table.addColumn(getIsSellCategoryColumn,"Item Type");
  getIsSellCategoryColumn.setSortable(true);
  }
  protected void addSortingisServiceCategory()
  {
  
  List<Category> list=getDataprovider().getList();
  columnSort=new ListHandler<Category>(list);
  columnSort.setComparator(isServiceCategoryColumn, new Comparator<Category>()
  {
  @Override
  public int compare(Category e1,Category e2)
  {
  if(e1!=null && e2!=null)
  {
  if(e1.isServiceCategory()== e2.isServiceCategory()){
  return 0;}
  else{
  return -1;}
  }
  else{
  return 0;}
  }
  });
  table.addColumnSortHandler(columnSort);
  }
  protected void addColumnisServiceCategory()
  {
  isServiceCategoryColumn=new TextColumn<Category>()
  {
  @Override
  public String getValue(Category object)
  {
  if( object.isServiceCategory()==true)
  return "Yes";
  else 
  return "No";
  }
  };
  table.addColumn(isServiceCategoryColumn,"Service Type");
  isServiceCategoryColumn.setSortable(true);
  }
}
