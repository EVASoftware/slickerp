package com.slicktechnologies.client.views.products.category;


import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.helperlayer.ProductCategoryChecklist;


public class ChecklistTable extends SuperTable<ProductCategoryChecklist>{
	
	TextColumn<ProductCategoryChecklist> checkListColumn;
    Column<ProductCategoryChecklist,String> deleteColumn;

    public ChecklistTable(){
		super();
	}

	@Override
	public void createTable() {
		addColumngetcheckList();
		addColumngetdelete();
		createFieldUpdaterdeleteColumn();
		
	}
	
   private void addColumngetcheckList() {
		
		checkListColumn=new TextColumn<ProductCategoryChecklist>()
				{	
			    @Override
					public String getValue(ProductCategoryChecklist object) {
						
			    	   return object.getChecklistName().trim();
					}
			    };
			    table.addColumn(checkListColumn,"Checklist");
				table.setColumnWidth(checkListColumn, 300, Unit.PX);
				checkListColumn.setSortable(true);	
		}
   
   private void addColumngetdelete() {
		ButtonCell btnCell = new ButtonCell();
		deleteColumn = new Column<ProductCategoryChecklist,String>(btnCell){

			@Override
			public String getValue(ProductCategoryChecklist object) {
				
				 return "Delete";
			}
			};
		table.addColumn(deleteColumn, "Delete");		
	}


	private void createFieldUpdaterdeleteColumn() {
		deleteColumn.setFieldUpdater(new FieldUpdater<ProductCategoryChecklist,String>() {

            public void update(int index, ProductCategoryChecklist object, String value)
            {
                getDataprovider().getList().remove(index);
                table.redrawRow(index);
            }
        }
);		
		
	}

	
	

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
