package com.slicktechnologies.client.views.products.category;

import static com.googlecode.objectify.ObjectifyService.ofy;

import javax.swing.ButtonGroup;

import com.google.gwt.user.client.ui.*;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.*;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.*;
import com.simplesoftwares.client.library.appstructure.formtablescreen.*;
import com.simplesoftwares.client.library.appstructure.formtablescreen.*;
import com.simplesoftwares.client.library.composite.*;
import com.simplesoftwares.client.library.mywidgets.*;
import com.simplesoftwares.client.library.libservice.*;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.*;
import com.simplesoftwares.client.library.appstructure.*;
import com.slicktechnologies.shared.*;
import com.slicktechnologies.shared.common.*;
import com.slicktechnologies.shared.common.businessprocesslayer.*;
import com.slicktechnologies.shared.common.businessunitlayer.*;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ProductCategoryChecklist;
import com.slicktechnologies.shared.common.personlayer.*;
import com.slicktechnologies.shared.common.productlayer.*;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;

import java.util.ArrayList;
import java.util.List;

import com.slicktechnologies.shared.common.businessunitlayer.Company;
/**
 * FormTablescreen template.
 */
public class CategoryForm extends FormTableScreen<Category> implements ClickHandler{

	//***********************************Variable Declaration Starts********************************************//
	//Token to add the varialble declarations
	CheckBox cbStatus;

	RadioButton cbsellCategory;
	RadioButton cbserviceCategory;

	
  TextBox tbCategoryName;
  TextBox tbCatcode;
  TextArea taDescription;
  
  /**Added by sheetal:08-02-2022
     Des : Adding option for checklist**/
  ChecklistTable checklistTable;
  ObjectListBox<Config> checklist;
  Button addprocess;
  
  DoubleBox dbFirstServicePercentage;
//***********************************Variable Declaration Ends********************************************//
	

	//Token to add the concrete presenter class name
	//protected <PresenterClassName> presenter;



	public CategoryForm  (SuperTable<com.slicktechnologies.shared.common.productlayer.Category> table, int mode,
			boolean captionmode) {
		super(table, mode, captionmode);
		createGui();
		cbsellCategory.setValue(false);
		cbserviceCategory.setValue(false);

	}

	//***********************************Variable Initialization********************************************//
	private void initalizeWidget()
	{

		cbStatus=new CheckBox();
		
		//  Anil has commented this for NBHC Date - 07/09/2016.
		
//		cbsellCategory=new RadioButton("buttonGroup", "");
//		cbserviceCategory=new RadioButton("buttonGroup","");
		
		cbsellCategory=new RadioButton("Item Product");
		cbsellCategory.setValue(false);
		cbsellCategory.addClickHandler(this);
		
		cbserviceCategory=new RadioButton("Service Product");
		cbserviceCategory.setValue(false);
		cbserviceCategory.addClickHandler(this);

		tbCategoryName=new TextBox();
		tbCatcode=new TextBox();

		taDescription=new TextArea();
		cbStatus.setValue(true);
		
		/**Added by Sheetal :08-02-2022**/
		checklistTable=new ChecklistTable();
		
		checklist=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(checklist, Screen.SERVICECHECKLIST);
		
		addprocess = new Button("ADD");
		addprocess.addClickHandler(this);
		
		dbFirstServicePercentage = new DoubleBox();

	}

	/*
	 * Method template to create the formtable screen
	 */

	@Override
	public void createScreen() {

		//Token to initialize the processlevel menus.
		initalizeWidget();


		this.processlevelBarNames=new String[]{"New"};

		//////////////////////////////////Form Field Declaration/////////////////////////////////////////////////


		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////

		//Token to initialize formfield
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder("* Category Name",tbCategoryName);


		FormField ftbCategoryName= fbuilder.setMandatory(true).setMandatoryMsg("Category Name is mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Category Code",tbCatcode);
		FormField ftbCategoryCode= fbuilder.setMandatory(true).setMandatoryMsg("Category Code is mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Status",cbStatus);
		FormField fcbStatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Item Product",cbsellCategory);
		FormField fcbSellProduct= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Service Product",cbserviceCategory);
		FormField fcbServiceProduct= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();



		fbuilder = new FormFieldBuilder("Description",taDescription);
		FormField ftaDescription= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(5).build();
		fbuilder = new FormFieldBuilder("Checklist",checklist);
		FormField olbchecklist= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",addprocess);
		FormField faddprocess= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder=new FormFieldBuilder("",checklistTable.getTable());
		FormField ftable=fbuilder.setColSpan(4).build();


		fbuilder = new FormFieldBuilder("First Service Value % ",dbFirstServicePercentage);
		FormField fdbTermiteFirstServicePercentage= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////


		FormField[][] formfield = {{ftbCategoryName,ftbCategoryCode,fcbStatus,fcbSellProduct,fcbServiceProduct},
				{ftaDescription},
				{olbchecklist,faddprocess},
				{ftable},
				{fdbTermiteFirstServicePercentage},
		};


		this.fields=formfield;		
	}

	/**
	 * method template to update the model with token entity name
	 */
	@Override
	public void updateModel(com.slicktechnologies.shared.common.productlayer.Category model) 
	{

		if(tbCategoryName.getValue()!=null)
			model.setCatName(tbCategoryName.getValue());
		if(cbStatus.getValue()!=null)
			model.setStatus(cbStatus.getValue());
		if(tbCatcode.getValue()!=null)
			model.setCatCode(tbCatcode.getValue());
		if(taDescription.getValue()!=null)
			model.setDescription(taDescription.getValue());
		if(cbsellCategory.getValue()!=null)
			model.setSellCategory(cbsellCategory.getValue());
		if(cbserviceCategory.getValue()!=null)
			model.setServiceCategory(cbserviceCategory.getValue());
		/**Added by sheetal : 08-02-2022**/
		List<ProductCategoryChecklist> checklist=this.checklistTable.getDataprovider().getList();
		ArrayList<ProductCategoryChecklist> arrItems=new ArrayList<ProductCategoryChecklist>();
		arrItems.addAll(checklist);
		model.setcheckList(arrItems);
		
		if(dbFirstServicePercentage.getValue()!=null && !dbFirstServicePercentage.getValue().equals("")) {
			model.setServiceValuePercentage(dbFirstServicePercentage.getValue());
		}
		else {
			model.setServiceValuePercentage(0);
		}
		
		presenter.setModel(model);
		

	}

	/**
	 * method template to update the view with token entity name
	 */
	@Override
	public void updateView(com.slicktechnologies.shared.common.productlayer.Category view) 
	{


		if(view.getCatName()!=null)
			tbCategoryName.setValue(view.getCatName());
		if(view.getStatus()!=null)
			cbStatus.setValue(view.getStatus());
		if(view.getDescription()!=null)
			taDescription.setValue(view.getDescription());
		if(view.getCatCode()!=null)
			tbCatcode.setValue(view.getCatCode());
		if(view.getDescription()!=null)
			taDescription.setValue(view.getDescription());
		if(view.getIsSellCategory()!=null)
			cbsellCategory.setValue(view.getIsSellCategory());
		if(view.isServiceCategory()!=null)
			cbserviceCategory.setValue(view.isServiceCategory());
		
		checklistTable.setValue(view.getcheckList());
		
		if( view.getServiceValuePercentage()!=0)
			dbFirstServicePercentage.setValue(view.getServiceValuePercentage());

		presenter.setModel(view);

	}

	/**
	 * Toggles the app header bar menus as per screen state
	 */

	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.CATEGORY,LoginPresenter.currentModule.trim());	
	}

	/**
	 * sets the id textbox with the passed count value. 
	 */
	@Override
	public void setCount(int count)
	{


	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		super.clear();
		this.cbStatus.setValue(true);
		this.cbsellCategory.setValue(false);
		this.cbserviceCategory.setValue(false);
		checklistTable.clear();
	}

	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
		boolean superValidate = super.validate();
        
        if(superValidate==false)
        	return false;
		
		if((cbsellCategory.getValue()==false)&& (cbserviceCategory.getValue()==false))
		{
			showDialogMessage("Please choose Type of Product(Service/Item)");
			return false;
		}
		
		if(dbFirstServicePercentage.getValue()!=null && dbFirstServicePercentage.getValue()>0) {
			if(dbFirstServicePercentage.getValue()>100) {
				showDialogMessage("First service value % should not be greater than 100 %");
				return false;
			}
		}
		
		return true;
	}

	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		
		checklistTable.setEnable(state);
	}

	@Override
	public void onClick(ClickEvent event) {
		
		if(event.getSource().equals(cbsellCategory)){
			if(cbsellCategory.getValue()==true){
				cbserviceCategory.setValue(false);
			}
			
		}
		if(event.getSource().equals(cbserviceCategory)){
			if(cbserviceCategory.getValue()==true){
				cbsellCategory.setValue(false);
			}
		}
		/**Added by Sheetal : 08-02-2022**/
		if(event.getSource().equals(addprocess)){
			if(checklist.getSelectedIndex()==0)
			{
				showDialogMessage("Please select checklist");
			}
			else{
				addToTable();
			}
			
		}
	}

	private void addToTable() {
		
		ProductCategoryChecklist productcategorychecklist = new ProductCategoryChecklist();
		productcategorychecklist.setChecklistName(checklist.getValue());
		
		checklistTable.getDataprovider().getList().add(productcategorychecklist);
		
		
	}

	

}

