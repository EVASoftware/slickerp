package com.slicktechnologies.client.views.products.serviceproduct;

import java.util.ArrayList;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.LongBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.composite.TaxComposite;
import com.simplesoftwares.client.library.composite.UploadComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.customer.CustomerPresenterSearchProxy;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.shared.TaxDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.helperlayer.ProductInfo;
import com.slicktechnologies.shared.common.productlayer.Category;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
/**
 * FormTablescreen template.
 */
public class ServiceproductForm extends FormScreen<ServiceProduct>  implements ClickHandler, ValueChangeHandler<String> {

	//***********************************Variable Declaration Starts********************************************//
	//Token to add the varialble declarations
	TaxComposite tcServiceTax;
	TaxComposite tcVatTax;
	IntegerBox ibDurationInMonths,ibNoOfServices;
	ObjectListBox<Category> olbcProductCategory;
	CheckBox cbStatus;
	TextBox tbProductCode,tbProductName,tbSpecification;
	UploadComposite ucUploadTAndCs,ucUploadProductImage;
	TextArea taComments,taComments1,taComments2,taComments3;
	DoubleBox dbProductPrice;
	ObjectListBox<Config> olbUnitOfMeasurement;
	ObjectListBox<Config> olbProductGroup;
	ObjectListBox<Config> olbProductType;
	ObjectListBox<Config> olbProductClassification;
	TextBox tbRefNumOne,tbRefNumTwo;
	TextBox tbproductid;
	final GenricServiceAsync async = GWT.create(GenricService.class);
	boolean flag=true;

	DoubleBox ibSeviceTime;
	
	TextBox tbTermsOfTreatment;
	
	/**
	 * Date : 24-04-2018 BY ANIL
	 * when we are creating services from sales order ,whether the service creation date should be at start of the day or after interval
	 * interval=duration/no_of_service
	 * for HVAC/Rohan
	 */
	CheckBox cbStartDate;
	
	
	ServiceProduct serviceProductObj;
	//***********************************Variable Declaration Ends********************************************//
	//Token to add the concrete presenter class name
	//protected <PresenterClassName> presenter;
	
	/**
	 * Rohan added this code for GSC on date : 22-06-2017
	 */
	TextBox tbHSNNumber;
	
	

	/**
	 * Date 06-06-2018 by Vijay
	 * Des :- for Service product warranty period 
	 * Requirement :- Neatedge Services
	 */
	IntegerBox ibWarrantyPeriod;
	
	/**
	 * ends here
	 */
	
	TextBox tbproductTermsAndConditionLink;
	
	TextBox tbZohoItemId;

	public ServiceproductForm()
	{
		super();
		createGui();
	}
	/**
	 * @author Vijay Date 25-05-2021
	 * Des :- added below constructor to show screen on Popup screen
	 * 
	 */
	public ServiceproductForm(boolean popupFlag)
	{
		super(popupFlag);
		createGui();
	}
	
	public ServiceproductForm  (String[] processlevel, FormField[][] fields,FormStyle formstyle) 
	{
		super(processlevel, fields, formstyle);
		createGui();
	}


	//***********************************Variable Initialization********************************************//
	private void initalizeWidget()
	{
		tcServiceTax=new TaxComposite();
		tcVatTax=new TaxComposite();
		ibDurationInMonths=new IntegerBox();
		ibNoOfServices=new IntegerBox();
		olbcProductCategory=new ObjectListBox<Category>();
		initalizeCategoryComboBox();
		cbStatus=new CheckBox();
		tbProductCode=new TextBox();
		tbProductCode.addValueChangeHandler(this);
		tbProductName=new TextBox();
		/**
		 * @author Priyanka
		 * @since 31-12-2020
		 * Product name field can take change value.
		 * raised by Rahul Tiwari
		 */
		tbProductName.addValueChangeHandler(this);
		tbSpecification=new TextBox();
		ucUploadTAndCs=new UploadComposite();
		ucUploadProductImage=new UploadComposite();
		taComments=new TextArea();
		taComments1=new TextArea();
		taComments2 = new TextArea();
		taComments3 = new TextArea();
		dbProductPrice=new DoubleBox();
		cbStatus.setValue(true);
		olbUnitOfMeasurement=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbUnitOfMeasurement, Screen.UNITOFMEASUREMENT);
		olbProductGroup=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbProductGroup, Screen.PRODUCTGROUP);
		olbProductType=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbProductType, Screen.PRODUCTTYPE);
		olbProductClassification=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbProductClassification, Screen.PRODUCTCLASSIFICATION);
		tbRefNumOne=new TextBox();
		tbRefNumTwo=new TextBox();
		TaxDetails.initializeVatTax(tcVatTax.percentageBox);
		TaxDetails.initializeServiceTax(tcServiceTax.percentageBox);
		
		tbproductid=new TextBox();
		tbproductid.setEnabled(false);
		
		ibSeviceTime=new DoubleBox();
		
		tbTermsOfTreatment=new TextBox();
		
		tbHSNNumber= new TextBox();
		
		cbStartDate=new CheckBox();
		
		ibWarrantyPeriod = new IntegerBox();
		
		tbproductTermsAndConditionLink = new TextBox();
		
		tbZohoItemId=new TextBox();

	}

	/*
	 * Method template to create the formtable screen
	 */

	@Override
	public void createScreen() {

		//Token to initialize the processlevel menus.
		initalizeWidget();

		this.processlevelBarNames=new String[]{"New"};

		//////////////////////////////////Form Field Declaration/////////////////////////////////////////////////


		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////

		//Token to initialize formfield
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingProductInformation=fbuilder.setlabel("Product Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).setKeyField(true).build();
		fbuilder = new FormFieldBuilder("* Duration(Days)",ibDurationInMonths);
		FormField fibDurationInMonths= fbuilder.setMandatory(true).setMandatoryMsg("Duration In Months is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* No Of Services",ibNoOfServices);
		FormField fibNoOfServices= fbuilder.setMandatory(true).setMandatoryMsg("No Of Services is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Status",cbStatus);
		FormField fcbStatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Product Code",tbProductCode);
		FormField ftbProductCode= fbuilder.setMandatory(true).setMandatoryMsg("Product Code is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Product Category",olbcProductCategory);
		FormField folbcProductCategory= fbuilder.setMandatory(false).setMandatoryMsg("Product Category is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Product Name",tbProductName);
		FormField ftbProductName= fbuilder.setMandatory(true).setMandatoryMsg("Product Name is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Specification",tbSpecification);
		FormField ftbSpecification= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Product Group",olbProductGroup);
		FormField folbProductGroup= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Product Type",olbProductType);
		FormField folbProductType= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Product Classification",olbProductClassification);
		FormField folbProductClassification= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Product Price",dbProductPrice);
		FormField fdbProductPrice= fbuilder.setMandatory(true).setMandatoryMsg("Product Price is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Tax 1",tcServiceTax);
		FormField ftcServiceTax= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Tax 2",tcVatTax);
		FormField ftcVatTax= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingDescription=fbuilder.setlabel("Reference/General").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Description 1  (Max 500 characters)",taComments);
		FormField ftaComments= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Description 2 (Max 500 characters)",taComments1);
		FormField ftaComments1= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Description 3 (Max 500 characters)",taComments2);
		FormField ftaComments2= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Description 4 (Max 500 characters)",taComments3);
		FormField ftaComments3= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingPriceInformation=fbuilder.setlabel("Images").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingImage=fbuilder.setlabel("Attachment").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Upload Terms & Conditions",ucUploadTAndCs);
		FormField fucUploadTAndCs= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Upload Product Image",ucUploadProductImage);
		FormField fucUploadProductImage= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/**@author Sheetal : 28-05-2022 , Changing label of reference no 1 for envocare**/
		FormField ftbRefNumOne;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", AppConstants.PC_EnableOracletab)){
		fbuilder=new FormFieldBuilder("* Memo Line Name", tbRefNumOne);
		   ftbRefNumOne=fbuilder.setMandatory(true).setRowSpan(0).setColSpan(0).build();
		}else{
			fbuilder=new FormFieldBuilder("Reference No. 1", tbRefNumOne);
			 ftbRefNumOne=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
		fbuilder=new FormFieldBuilder("Reference No. 2", tbRefNumTwo);
		FormField ftbRefNumTwo=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Unit of Measuerment",olbUnitOfMeasurement);
		FormField folbUnitOfMeasurement= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Product ID",tbproductid);
		FormField ftbproductid= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Servicing Time In Hours",ibSeviceTime);
		FormField fibServicesTime= fbuilder.setMandatory(false).setMandatoryMsg("Servicing Time is Mandatory!").setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Terms of Treatment",tbTermsOfTreatment);
		FormField ftbtermsoftreatment= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		//   this is used for SAC before that it is used for HSN 
		fbuilder = new FormFieldBuilder("SAC Code",tbHSNNumber);
		FormField ftbHSNNumber= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Is Start Date",cbStartDate);
		FormField fcbStartDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Warranty Period",ibWarrantyPeriod);
		FormField fibWarrantyPeriod = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Product Terms And Condition Hyperlink",tbproductTermsAndConditionLink);
		FormField ftbproductTermsAndConditionLink = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Zoho Item ID",tbZohoItemId);
		FormField ftbZohoItemId = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////


//		FormField[][] formfield = {   {fgroupingProductInformation},
//				{ftbproductid,fibDurationInMonths,fibNoOfServices,fcbStatus},
//				{ftbProductCode,folbcProductCategory,ftbProductName,ftbSpecification},
//				{folbProductGroup,folbProductType,folbProductClassification,fibServicesTime},
//				{ftbRefNumOne,ftbRefNumTwo,ftbtermsoftreatment,ftbHSNNumber},
//				{fcbStartDate,fibWarrantyPeriod},//
//				{fgroupingPriceInformation},
//				{fdbProductPrice,ftcServiceTax,ftcVatTax,folbUnitOfMeasurement},
//				{fgroupingDescription},
//				{ftaComments},
//				{ftaComments1},
//				{ftaComments2},
//				{ftaComments3},
//				{fgroupingImage},
//				{fucUploadTAndCs,fucUploadProductImage},
		
		FormField[][] formfield = { 
				{fgroupingProductInformation},
				{ftbproductid,ftbProductName,ftbProductCode,fibDurationInMonths},
				{fibNoOfServices,ftbHSNNumber,fibServicesTime,fcbStatus},
				{fdbProductPrice,ftcServiceTax,ftcVatTax,folbUnitOfMeasurement},
				{fcbStartDate,fibWarrantyPeriod},
				/**Reference and General**/
				{fgroupingDescription},
				{folbProductGroup,folbcProductCategory,folbProductType,folbProductClassification},
				{ftbSpecification,ftbRefNumOne,ftbRefNumTwo,ftbtermsoftreatment},
				{ftbproductTermsAndConditionLink,ftbZohoItemId},
				{ftaComments},
				{ftaComments1},
				{ftaComments2},
				{ftaComments3},
				/**Images**/
				{fgroupingPriceInformation},
				{fucUploadProductImage},
				/**Attachment**/
				{fgroupingImage},
				{fucUploadTAndCs},
		};


		this.fields=formfield;		
	}

	/**
	 * method template to update the model with token entity name
	 */
	@Override
	public void updateModel(ServiceProduct model) 
	{
		
		if(ibDurationInMonths.getValue()!=null&&ibDurationInMonths.getValue()!=0){
			model.setDuration(ibDurationInMonths.getValue());
		}
		if(ibNoOfServices.getValue()!=null&&ibNoOfServices.getValue()!=0){
			model.setNumberOfServices(ibNoOfServices.getValue());
		 }
		if(cbStatus.getValue()!=null)
			model.setStatus(cbStatus.getValue());
		if(tbProductCode.getValue()!=null)
			model.setProductCode(tbProductCode.getValue());
		if(olbcProductCategory.getValue()!=null){
			model.setProductCategory(olbcProductCategory.getValue());
		}else{
			model.setProductCategory("");
		}
		if(tbProductName.getValue()!=null)
			model.setProductName(tbProductName.getValue());
		if(tbSpecification.getValue()!=null)
			model.setSpecifications(tbSpecification.getValue());
		if(olbProductGroup.getValue()!=null){
			model.setProductGroup(olbProductGroup.getValue());
		}else{
			model.setProductGroup("");
		}
		if(olbProductType.getValue()!=null){
			model.setProductType(olbProductType.getValue());
		}else{
			model.setProductType("");
		}
		if(olbProductClassification.getValue()!=null){
			model.setProductClassification(olbProductClassification.getValue());
		}else{
			model.setProductClassification("");
		}
		if(dbProductPrice.getValue()!=null&&dbProductPrice.getValue()!=0)
		{	
			model.setPrice(dbProductPrice.getValue());	
		}
		if(tcServiceTax.getValue()!=null)
			model.setServiceTax(tcServiceTax.getValue());
		if(tcVatTax.getValue()!=null)
			model.setVatTax(tcVatTax.getValue());
		if(taComments.getValue()!=null)
			model.setComment(taComments.getValue());
		if(taComments1.getValue()!=null)
			model.setCommentdesc(taComments1.getValue());
		
		if(taComments2.getValue()!=null)
		{
			model.setCommentdesc1(taComments2.getValue());
		}
		if(taComments3.getValue()!=null)
		{
			model.setCommentdesc2(taComments3.getValue());
		}
		
		
		model.setTermsAndConditions(ucUploadTAndCs.getValue());
	
		model.setProductImage(ucUploadProductImage.getValue());
		if(olbUnitOfMeasurement.getValue()!=null){
			model.setUnitOfMeasurement(olbUnitOfMeasurement.getValue());
		}else{
			model.setUnitOfMeasurement("");
		}
		
		if(tbRefNumOne.getValue()!=null)
			model.setRefNumber1(tbRefNumOne.getValue());
		if(tbRefNumTwo.getValue()!=null)
			model.setRefNumber2(tbRefNumTwo.getValue());
		
		if(ibSeviceTime.getValue()!=null){
			model.setServiceTime(ibSeviceTime.getValue());
		}
		
		if(tbTermsOfTreatment.getValue()!=null)
			model.setTermsoftreatment(tbTermsOfTreatment.getValue());
		
		if(tbHSNNumber.getValue()!= null){
			model.setHsnNumber(tbHSNNumber.getValue());
		}
		model.setStartDate(cbStartDate.getValue());
		
	
		if(ibWarrantyPeriod.getValue()!=null){
			model.setWarrantyPeriod(ibWarrantyPeriod.getValue());
		}else {
			model.setWarrantyPeriod(0); //Ashwini Patil Date:11-06-2024
		}
		
		if(tbproductTermsAndConditionLink.getValue()!=null){
			model.setTermsAndConditionLink(tbproductTermsAndConditionLink.getValue());
		}
		/**
		 * @author Vijay Date :- 29-03-2022
		 * Des :- below fields are used for item product so when we create contract data from website so data get corrupted
		 * so below code
		 */
		model.setProductImage1(new DocumentUpload());
		model.setProductImage2(new DocumentUpload());
		model.setProductImage3(new DocumentUpload());
		
		if(tbZohoItemId!=null&&!tbZohoItemId.getValue().equals(""))
			model.setZohoItemID(Long.parseLong(tbZohoItemId.getValue()));
		else
			model.setZohoItemID(0);
		/**
		 * ends here
		 */
		serviceProductObj=model;
		presenter.setModel(model);


	}

	/**
	 * method template to update the view with token entity name
	 */
	@Override
	public void updateView(ServiceProduct view) 
	{
		serviceProductObj=view;
		tbproductid.setValue(view.getCount()+"");
		if(view.getDuration()!=null)
			ibDurationInMonths.setValue(view.getDuration());
		if(view.getNumberOfServices()!=null)
			ibNoOfServices.setValue(view.getNumberOfServices());
		if(view.isStatus()!=null)
			cbStatus.setValue(view.isStatus());
		if(view.getProductCode()!=null)
			tbProductCode.setValue(view.getProductCode());
		if(view.getProductCategory()!=null)
			olbcProductCategory.setValue(view.getProductCategory());
		if(view.getProductName()!=null)
			tbProductName.setValue(view.getProductName());
		if(view.getSpecifications()!=null)
			tbSpecification.setValue(view.getSpecifications());
		if(view.getProductGroup()!=null)
			olbProductGroup.setValue(view.getProductGroup());
		if(view.getProductType()!=null)
			olbProductType.setValue(view.getProductType());
		if(view.getProductClassification()!=null)
			olbProductClassification.setValue(view.getProductClassification());
		if(view.getPrice()!=null)
			dbProductPrice.setValue(view.getPrice());
		if(view.getServiceTax()!=null)
			tcServiceTax.setValue(view.getServiceTax());
		if(view.getVatTax()!=null)
			tcVatTax.setValue(view.getVatTax());
		if(view.getComment()!=null)
			taComments.setValue(view.getComment());
		if(view.getCommentdesc()!=null)
			taComments1.setValue(view.getCommentdesc());
		
		if(view.getCommentdesc1()!=null)
			taComments2.setValue(view.getCommentdesc1());
		
		if(view.getCommentdesc2()!=null)
			taComments3.setValue(view.getCommentdesc2());
		
		if(view.getTermsAndConditions()!=null)
			ucUploadTAndCs.setValue(view.getTermsAndConditions());
		if(view.getProductImage()!=null)
			ucUploadProductImage.setValue(view.getProductImage());
		if(view.getUnitOfMeasurement()!=null)
			olbUnitOfMeasurement.setValue(view.getUnitOfMeasurement());
		if(view.getRefNumber1()!=null)
			tbRefNumOne.setValue(view.getRefNumber1());
		if(view.getRefNumber2()!=null)
			tbRefNumTwo.setValue(view.getRefNumber2());
		
		
		if(view.getServiceTime()!=null){
			ibSeviceTime.setValue(view.getServiceTime());
		}
		
		if(view.getTermsoftreatment()!=null)
			tbTermsOfTreatment.setValue(view.getTermsoftreatment());
		
		if(view.getHsnNumber()!= null){
			tbHSNNumber.setValue(view.getHsnNumber());
		}
		
		cbStartDate.setValue(view.isStartDate());
		
		if(view.getWarrantyPeriod()!=0){
			ibWarrantyPeriod.setValue(view.getWarrantyPeriod());
		}
		
		if(view.getTermsAndConditionLink()!=null){
			tbproductTermsAndConditionLink.setValue(view.getTermsAndConditionLink());
		}
		if(view.getZohoItemID()!=0)
			tbZohoItemId.setValue(view.getZohoItemID()+"");
		
		presenter.setModel(view);

	}

	/**
	 * Toggles the app header bar menus as per screen state
	 */

	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			/**
			 * @author Vijay Date - 21-04-2021
			 * Des :- updated code to manage popup and regular screen app level menus
			 */
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();

			}
			
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals("Search")||text.equals(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			/**
			 * @author Vijay Date - 21-04-2021
			 * Des :- updated code to manage popup and regular screen app level menus
			 */
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();

			}
			
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			/**
			 * @author Vijay Date - 21-04-2021
			 * Des :- updated code to manage popup and regular screen app level menus
			 */
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();

			}
			
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit")||text.equals("Search")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.SERVICEPRODUCT,LoginPresenter.currentModule.trim());
	}

	/**
	 * sets the id textbox with the passed count value. 
	 */
	@Override
	public void setCount(int count)
	{
		tbproductid.setValue(count+"");
	}
	
	
	public void checkServiceProduct(){
		MyQuerry querry = new MyQuerry();
		Company c=new Company();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("productCode");
		filter.setStringValue(tbProductCode.getValue().trim());
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new SuperProduct());
		this.showWaitSymbol();
		async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
	
			@Override
			public void onFailure(Throwable caught) {
	
			showDialogMessage("An Unexpected Error occured !");
			}
	
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				
				if(result.size()>0){
					
					showDialogMessage("Product Code Already Exists!");
					tbProductCode.setValue("");
				}
				
				hideWaitSymbol();
				
			}		
		});
		
	}
	/**
	 * @author Priyanka
	 * @since 31-12-2020
	 * Product name field can take change value.
	 * raised by Rahul Tiwari
	 */
	public void checkServiceProductName(){
		MyQuerry querry = new MyQuerry();
		Company c=new Company();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("productName");
		filter.setStringValue(tbProductName.getValue().trim());
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new SuperProduct());
		this.showWaitSymbol();
		async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
	
			@Override
			public void onFailure(Throwable caught) {
	
			showDialogMessage("An Unexpected Error occured !");
			}
	
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				
				if(result.size()>0){
					
					showDialogMessage("Product Name Already Exists!");
					tbProductName.setValue("");
				}
				
				hideWaitSymbol();
				
			}		
		});
		
	}

	@Override
	public void clear()
	{
		super.clear();
		cbStatus.setValue(true);
	}

	@Override
	public void onClick(ClickEvent event) {

	}
	
	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		tbproductid.setEnabled(false);
		tbProductName.setEnabled(false);
		//Ashwini Patil Date:13-02-2023 added below condition  as product name was not editable when we click new button from product compisite
		if(popUpAppMenubar && AppMemory.getAppMemory().currentState==ScreeenState.NEW){
			tbProductName.setEnabled(true);
		}
	}

	
	@Override
	public void setToViewState() {
		super.setToViewState();
		

		SuperModel model=new ServiceProduct();
		model=serviceProductObj;
		AppUtility.addDocumentToHistoryTable(AppConstants.PRODUCTMODULE,AppConstants.SERVICEPRODUCT, serviceProductObj.getCount(), null,null,null, false, model, null);
	   /**
	    * Date : 21/02/2021 Added By : Priyanka
	    * Des : New added product not immediately reflected on search product composite issue raised by Nitin Sir. 
	    */
		try {
	
			ProductInfo info = new ProductInfo();
			info.setProdID(this.getPresenter().getModel().getCount());
			//info.setCount(this.getPresenter().getModel().getCount());
			//info.setProdID(Integer.parseInt(tbproductid.getValue()));
			info.setProductCode(tbProductCode.getValue());
			info.setProductName(tbProductName.getValue());
			
			ServiceProductPresenterSearchProxy search = (ServiceProductPresenterSearchProxy) this.getSearchpopupscreen();
			search.prodComposite.setProductInfoHashMap(info);
			ProductInfoComposite.globalServiceProdArray.add(info);			
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
		
		/**
		 * ends here
		 */

		
	}
	
	@Override
	public void setToEditState() {
		super.setToEditState();
		this.processLevelBar.setVisibleFalse(false);
		this.tbProductCode.setEnabled(false);
	}

	@Override
	public boolean validate() {
		boolean superRes= super.validate();
		boolean descBox=true;
		boolean descBox1Limit=true,descBox2Limit=true;;
		
//		if(flag==false)
//		{
//			this.showDialogMessage("Product Code already exists!");
//			prodCode=false;
//		}
		
		
		if(taComments.getValue().equals("")&&!taComments1.getValue().equals("")){
			showDialogMessage("Description 1 cannot be empty!");
			descBox=false;
		}
		
		int commentsLength=taComments.getValue().length();
		int comments1Length=taComments1.getValue().length();
		if(commentsLength>=500)
		{
			descBox1Limit=false;
			showDialogMessage("Description 1 text exceeds 500 characters.");
		}
		if(comments1Length>=500)
		{
			descBox2Limit=false;
			showDialogMessage("Description 2 text exceeds 500 characters.");
		}
		
		
		/**
		 * Date 14-01-2018 by vijay for 3 and 4 description 500 chars limit
		 */
		int description3Lenght = taComments2.getValue().length();
		int description4Lenght = taComments3.getValue().length();
		if(description3Lenght>=500){
			descBox1Limit=false;
			showDialogMessage("Description 3 text exceeds 500 characters.");
		}
		if(description4Lenght>=500){
			descBox1Limit=false;
			showDialogMessage("Description 4 text exceeds 500 characters.");
		}
		/***27-12-2019 Deepak Salve added this code for Price not be zero***/
		if(ibDurationInMonths.getValue()==0||ibNoOfServices.getValue()==0||dbProductPrice.getValue()==0){
			showDialogMessage(" Durations In Days or No of Service or Product price Should not be Zero ");
			descBox=false;
			descBox1Limit=false;
			descBox2Limit=false;
			return superRes&&descBox&&descBox1Limit&&descBox2Limit;
		}
		/***End***/
		
		/**
		 * ends here
		 */
		
		return superRes&&descBox&&descBox1Limit&&descBox2Limit;
	}
	
	

	//****************************Buisness Logic Part*************************************************//


	public void initalizeCategoryComboBox()
	{
		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new Category());
		Filter categoryFilter = new Filter();
		categoryFilter.setQuerryString("isServiceCategory");
		categoryFilter.setBooleanvalue(true);
		Filter statusfilter=new Filter();
		   statusfilter.setQuerryString("status");
		   statusfilter.setBooleanvalue(true);
		querry.getFilters().add(categoryFilter);
		querry.getFilters().add(statusfilter);
		this.olbcProductCategory.MakeLive(querry);
	}

	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
		if(event.getSource().equals(tbProductCode)){
			if(!tbProductCode.getValue().equals("")){
				checkServiceProduct();
			}
		}
		
		/**
		 * @author Priyanka
		 * @since 31-12-2020
		 * Product name field can take change value.
		 * raised by Rahul Tiwari
		 */
		if(event.getSource().equals(tbProductName)){
			if(!tbProductName.getValue().equals("")){
				checkServiceProductName();
			}
		}
	}

	
}
