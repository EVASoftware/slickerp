package com.slicktechnologies.client.views.products.serviceproduct;

import java.util.Vector;

import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.products.serviceproduct.ServiceproductPresenter.ServiceproductPresenterSearch;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.productlayer.Category;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;

public class ServiceProductPresenterSearchProxy extends ServiceproductPresenterSearch {
	
	ObjectListBox<Category> olbcProductCategory;
	ObjectListBox<Config> olbUnitOfMeasurement;
	ProductInfoComposite prodComposite;
	ObjectListBox<Config>olbProductGroup;
	public ListBox lbStatus; //Ashwini Patil Date:8-02-2024
	
	public ServiceProductPresenterSearchProxy() {
		super();
		createGui();
	}
	
	public void initWidget()
	{
		olbcProductCategory=new ObjectListBox<Category>();
		initalizeCategoryComboBox();
		olbUnitOfMeasurement=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbUnitOfMeasurement, Screen.UNITOFMEASUREMENT);
		prodComposite=AppUtility.initiateServiceProductComposite(new SuperProduct());
		olbProductGroup=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbProductGroup, Screen.PRODUCTGROUP);
		lbStatus= new ListBox();
		lbStatus.addItem("--Select--");
		lbStatus.addItem("Active");
		lbStatus.addItem("Inactive");
	}
	
	public void createScreen()
	{
		initWidget();
		FormFieldBuilder builder;
		builder = new FormFieldBuilder("Product Category",olbcProductCategory);
		FormField folbproductcategory= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Unit Of Measurement",olbUnitOfMeasurement);
		FormField folbUnitOfMeasurement= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("",prodComposite);
		FormField fprodComposite= builder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		
		builder = new FormFieldBuilder("Product Group",olbProductGroup);
		FormField folbProductGroup= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Status",lbStatus);
		FormField fstatus= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		this.fields=new FormField[][]{
				{fprodComposite},
				{folbproductcategory,folbUnitOfMeasurement,folbProductGroup,fstatus}
		};
	}
	
	public MyQuerry getQuerry()
	{
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		
		
		if(olbcProductCategory.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(olbcProductCategory.getValue().trim());
			temp.setQuerryString("productCategory");
			filtervec.add(temp);
		}
		
		if(olbUnitOfMeasurement.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(olbUnitOfMeasurement.getValue().trim());
			temp.setQuerryString("unitOfMeasurement");
			filtervec.add(temp);
		}
		
		if (!prodComposite.getProdID().getValue().equals("")) {
			temp = new Filter();
			temp.setIntValue(Integer.parseInt(prodComposite.getProdID().getValue()));
			temp.setQuerryString("count");
			filtervec.add(temp);
		}		  
		if(olbProductGroup.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(olbProductGroup.getValue().trim());
			temp.setQuerryString("productGroup");
			filtervec.add(temp);
		} 
		if(lbStatus.getSelectedIndex()!=0)
		{
			temp=new Filter();
			temp.setQuerryString("status");
			
			String selVal=lbStatus.getItemText(lbStatus.getSelectedIndex());
			if(selVal.equals("Active"))
			{
				temp.setBooleanvalue(true);
			}
			
			else
			{
				temp.setBooleanvalue(false);
			}
			
			filtervec.add(temp);
		}
		  
		MyQuerry querry= new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new ServiceProduct());
		return querry;
	}

	
	
		
	
	
	public void initalizeCategoryComboBox()
	{
		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new Category());
		Filter categoryFilter = new Filter();
		categoryFilter.setQuerryString("isServiceCategory");
		categoryFilter.setBooleanvalue(true);
		Filter statusfilter=new Filter();
		statusfilter.setQuerryString("status");
		statusfilter.setBooleanvalue(true);
		querry.getFilters().add(categoryFilter);
		querry.getFilters().add(statusfilter);
		this.olbcProductCategory.MakeLive(querry);
	}

	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
		return super.validate();
	}



}
