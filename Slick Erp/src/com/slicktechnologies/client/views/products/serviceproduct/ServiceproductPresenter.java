package com.slicktechnologies.client.views.products.serviceproduct;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import com.simplesoftwares.client.library.appstructure.search.*;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.formtablescreen.*;
import com.slicktechnologies.shared.*;
import com.slicktechnologies.shared.common.*;
import com.slicktechnologies.shared.common.businessprocesslayer.*;
import com.slicktechnologies.shared.common.businessunitlayer.*;
import com.slicktechnologies.shared.common.helperlayer.*;
import com.slicktechnologies.shared.common.personlayer.*;
import com.slicktechnologies.shared.common.productlayer.*;

import java.util.Vector;

import com.simplesoftwares.client.library.appstructure.search.*;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.google.gwt.core.shared.GWT;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.products.itemproduct.ItemProductPresenterSearchProxy;
import com.slicktechnologies.client.views.products.itemproduct.ItemproductForm;
import com.slicktechnologies.client.views.products.itemproduct.ItemproductPresenter.ItemproductPresenterSearch;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.shared.common.paymentlayer.*;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
/**
 *  FormTableScreen presenter template
 */
public class ServiceproductPresenter extends FormScreenPresenter<ServiceProduct> implements ValueChangeHandler<Boolean>, ChangeHandler  {

	//Token to set the concrete form
	ServiceproductForm form;
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	final GenricServiceAsync genasync=GWT.create(GenricService.class);
	
	public ServiceproductPresenter(FormScreen<ServiceProduct> view, ServiceProduct model) {
		super(view, model);
		form=(ServiceproductForm) view;
		form.setPresenter(this);
		form.tcVatTax.taxch.addValueChangeHandler(this);
		form.tcServiceTax.taxch.addValueChangeHandler(this);
		form.tcVatTax.percentageBox.addChangeHandler(this);
		form.tcServiceTax.percentageBox.addChangeHandler(this);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.SERVICEPRODUCT,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}
	

	/**
	 * Method template to set the processBar events
	 */
	@Override
	public void reactToProcessBarEvents(ClickEvent e) 
   {
		InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();
		
		if(text.equals("New"))
			reactToNew();
		

		
	}
	
	private void reactToNew()
	{
		form.setToNewState();
		this.initalize();
		form.toggleAppHeaderBarMenu();
	}

	
	

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}
	
	
	@Override
	public void reactOnDownload() 
	{
		ArrayList<ServiceProduct> sparray=new ArrayList<ServiceProduct>();
		List<ServiceProduct> list=(List<ServiceProduct>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
		
		sparray.addAll(list);
		
		csvservice.setServiceProductList(sparray, new AsyncCallback<Void>() {
			

			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);
				
			}

			@Override
			public void onSuccess(Void result) {
				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "csvservlet"+"?type="+75;
				Window.open(url, "test", "enabled");
			}
		});

	}

	/**
	 * Method template to set new model
	 */
	@Override
	protected void makeNewModel() {
		
		model=new ServiceProduct();
	}
	
	/*
	 * Method template to set Myquerry object
	 */
	public MyQuerry getServiceProductQuery()
	{
		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new ServiceProduct());
		return quer;
	}
	
	public void setModel(ServiceProduct entity)
	{
		model=entity;
	}
	
	public static ServiceproductForm initalize()
	{
			 ServiceproductPresenterTable gentableScreen=new ServiceproductPresenterTableProxy();
			
			ServiceproductForm  form=new  ServiceproductForm();
			gentableScreen.setView(form);
			gentableScreen.applySelectionModle();
			
			ServiceproductPresenterSearch.staticSuperTable=gentableScreen;
			ServiceproductPresenterSearch searchpopup=new ServiceProductPresenterSearchProxy();
			form.setSearchpopupscreen(searchpopup);
			
			ServiceproductPresenter  presenter=new  ServiceproductPresenter  (form,new ServiceProduct());
			AppMemory.getAppMemory().stickPnel(form);
			return form;
	}
	
	
		@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.productlayer.ServiceProduct")
		 public static class  ServiceproductPresenterTable extends SuperTable<ServiceProduct> implements GeneratedVariableRefrence{

			@Override
			public Object getVarRef(String varName) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void createTable() {
				// TODO Auto-generated method stub
				
			}

			@Override
			protected void initializekeyprovider() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void addFieldUpdater() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void setEnable(boolean state) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void applyStyle() {
				// TODO Auto-generated method stub
				
			}} ;
			
			@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.productlayer.ServiceProduct")
			 public static class  ServiceproductPresenterSearch extends SearchPopUpScreen<  ServiceProduct>{

				@Override
				public MyQuerry getQuerry() {
					// TODO Auto-generated method stub
					return null;
				}

				@Override
				public boolean validate() {
					// TODO Auto-generated method stub
					return true;
				}};
				
	  private void reactTo()
  {
}


	@Override
	public void onValueChange(ValueChangeEvent<Boolean> event) {
		if(event.getSource().equals(form.tcVatTax.taxch)){
			
			if(form.tcVatTax.taxch.getValue()==true){
				form.tcVatTax.percentageBox.setEnabled(true);
			}
			if(form.tcVatTax.taxch.getValue()==false){
				form.tcVatTax.percentageBox.setSelectedIndex(0);
				form.tcVatTax.percentageBox.setEnabled(false);
				form.tcVatTax.taxInclusivech.setValue(false);
				form.tcVatTax.dbPercent.setValue(null);
			}
		}
		
		if(event.getSource().equals(form.tcServiceTax.taxch)){
			if(form.tcServiceTax.taxch.getValue()==true){
				form.tcServiceTax.percentageBox.setEnabled(true);
			}
			if(form.tcServiceTax.taxch.getValue()==false){
				form.tcServiceTax.percentageBox.setSelectedIndex(0);
				form.tcServiceTax.percentageBox.setEnabled(false);
				form.tcServiceTax.taxInclusivech.setValue(false);
				form.tcServiceTax.dbPercent.setValue(null);
			}
		}
	}


	@Override
	public void onChange(ChangeEvent event) {
		if(event.getSource().equals(form.tcServiceTax.percentageBox))
		{
			if(form.tcServiceTax.percentageBox.getSelectedIndex()!=0){
				TaxDetails percServ=form.tcServiceTax.percentageBox.getSelectedItem();
				if(percServ!=null)
				{
					form.tcServiceTax.dbPercent.setValue(percServ.getTaxChargePercent());
					/**
					 * Date : 01/07/2017 by Rohan
					 */
					form.tcServiceTax.taxPrintableName=percServ.getTaxPrintName();
					/**
					 * End
					 */
					
				}
			}
			else{
				form.tcServiceTax.percentageBox.setSelectedIndex(0);
			}
		}
		
		if(event.getSource().equals(form.tcVatTax.percentageBox))
		{
			if(form.tcVatTax.percentageBox.getSelectedIndex()!=0){
				TaxDetails percServ=form.tcVatTax.percentageBox.getSelectedItem();
				if(percServ!=null)
				{
					form.tcVatTax.dbPercent.setValue(percServ.getTaxChargePercent());
					/**
					 * Date : 01/07/2017 by Rohan
					 */
					form.tcVatTax.taxPrintableName=percServ.getTaxPrintName();
					/**
					 * End
					 */
					
				}
			}
			else{
				form.tcVatTax.percentageBox.setSelectedIndex(0);
			}
		}
	}

}
