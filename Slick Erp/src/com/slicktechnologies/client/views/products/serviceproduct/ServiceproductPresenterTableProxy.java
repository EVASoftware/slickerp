package com.slicktechnologies.client.views.products.serviceproduct;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;

import java.util.Comparator;

import com.google.gwt.cell.client.FieldUpdater;

import java.util.List;

import com.google.gwt.user.cellview.client.Column;
import  com.google.gwt.cell.client.NumberCell;
import com.google.gwt.dom.client.Style.Unit;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.client.views.products.serviceproduct.ServiceproductPresenter.ServiceproductPresenterTable;

public class ServiceproductPresenterTableProxy extends ServiceproductPresenterTable {
	Column<ServiceProduct,Number> getDurationColumn;
	Column<ServiceProduct,Number> getNumberOfServicesColumn;
	TextColumn<ServiceProduct> getServiceTaxColumn;
	TextColumn<ServiceProduct> getProductCodeColumn;
	TextColumn<ServiceProduct> getProductNameColumn;
	TextColumn<ServiceProduct> getProductCategoryColumn;
	Column<ServiceProduct,Number> getPriceColumn;
	TextColumn<ServiceProduct> getCountColumn;
	TextColumn<ServiceProduct> getStatusColumn;
	TextColumn<ServiceProduct> getUnitColumn;
	
	
	
	public Object getVarRef(String varName)
	{
		if(varName.equals("getDurationColumn"))
			return this.getDurationColumn;
		if(varName.equals("getNumberOfServicesColumn"))
			return this.getNumberOfServicesColumn;
		if(varName.equals("getServiceTaxColumn"))
			return this.getServiceTaxColumn;
		if(varName.equals("getProductCodeColumn"))
			return this.getProductCodeColumn;
		if(varName.equals("getProductNameColumn"))
			return this.getProductNameColumn;
		if(varName.equals("getProductCategoryColumn"))
			return this.getProductCategoryColumn;
		if(varName.equals("getPriceColumn"))
			return this.getPriceColumn;
		if(varName.equals("getCountColumn"))
			return this.getCountColumn;
		if(varName.equals("getStatusColumn"))
			return this.getStatusColumn;
		return null ;
	}
	
	
	
	public ServiceproductPresenterTableProxy()
	{
		super();
	}
	
	
	
	@Override 
	public void createTable() {
		addColumngetCount();
		addColumngetProductName();
		addColumngetProductCode();
		addColumngetProductCategory();
		addColumngetPrice();
		addColumngetDuration();
		addColumngetNumberOfServices();
		addColumnUnitOfMeasurement();
		addColumngetStatus();
	}
	@Override
	protected void initializekeyprovider() {
		keyProvider= new ProvidesKey<ServiceProduct>()
				{
			@Override
			public Object getKey(ServiceProduct item)
			{
				if(item==null)
				{
					return null;
				}
				else
					return item.getId();
			}
				};
	}
	@Override
	public void setEnable(boolean state)
	{
	}
	@Override
	public void applyStyle()
	{
	}
	public void addColumnSorting(){
		addSortinggetCount();
		addSortinggetProductName();
		addSortinggetProductCode();
		addSortinggetProductCategory();
		addSortinggetPrice();
		addSortinggetDuration();
		addSortinggetNumberOfServices();
		addSortinggetUnitOfMeasurement();
	}
	
	@Override public void addFieldUpdater() {
	}
	
	protected void addSortinggetCount()
	{
		List<ServiceProduct> list=getDataprovider().getList();
		columnSort=new ListHandler<ServiceProduct>(list);
		columnSort.setComparator(getCountColumn, new Comparator<ServiceProduct>()
				{
			@Override
			public int compare(ServiceProduct e1,ServiceProduct e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	
	protected void addColumngetCount()
	{
		getCountColumn=new TextColumn<ServiceProduct>()
				{
			@Override
			public String getValue(ServiceProduct object)
			{
				if( object.getCount()==-1)
					return "N.A";
				else return object.getCount()+"";
			}
				};
				table.addColumn(getCountColumn,"ID");
				table.setColumnWidth(getCountColumn, 100, Unit.PX);
				getCountColumn.setSortable(true);
	}
	protected void addSortinggetProductCategory()
	{
		List<ServiceProduct> list=getDataprovider().getList();
		columnSort=new ListHandler<ServiceProduct>(list);
		columnSort.setComparator(getProductCategoryColumn, new Comparator<ServiceProduct>()
				{
			@Override
			public int compare(ServiceProduct e1,ServiceProduct e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getProductCategory()!=null && e2.getProductCategory()!=null){
						return e1.getProductCategory().compareTo(e2.getProductCategory());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetProductCategory()
	{
		getProductCategoryColumn=new TextColumn<ServiceProduct>()
				{
			@Override
			public String getValue(ServiceProduct object)
			{
				return object.getProductCategory()+"";
			}
				};
				table.addColumn(getProductCategoryColumn,"Category");
				table.setColumnWidth(getProductCategoryColumn, 130, Unit.PX);
				getProductCategoryColumn.setSortable(true);
	}
	protected void addSortinggetProductCode()
	{
		List<ServiceProduct> list=getDataprovider().getList();
		columnSort=new ListHandler<ServiceProduct>(list);
		columnSort.setComparator(getProductCodeColumn, new Comparator<ServiceProduct>()
				{
			@Override
			public int compare(ServiceProduct e1,ServiceProduct e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getProductCode()!=null && e2.getProductCode()!=null){
						return e1.getProductCode().compareTo(e2.getProductCode());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetProductCode()
	{
		getProductCodeColumn=new TextColumn<ServiceProduct>()
				{
			@Override
			public String getValue(ServiceProduct object)
			{
				return object.getProductCode()+"";
			}
				};
				table.addColumn(getProductCodeColumn,"Product Code");
				table.setColumnWidth(getProductCodeColumn, 120, Unit.PX);
				getProductCodeColumn.setSortable(true);
	}
	protected void addSortinggetProductName()
	{
		List<ServiceProduct> list=getDataprovider().getList();
		columnSort=new ListHandler<ServiceProduct>(list);
		columnSort.setComparator(getProductNameColumn, new Comparator<ServiceProduct>()
				{
			@Override
			public int compare(ServiceProduct e1,ServiceProduct e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getProductName()!=null && e2.getProductName()!=null){
						return e1.getProductName().compareTo(e2.getProductName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetProductName()
	{
		getProductNameColumn=new TextColumn<ServiceProduct>()
				{
			@Override
			public String getValue(ServiceProduct object)
			{
				return object.getProductName()+"";
			}
				};
				table.addColumn(getProductNameColumn,"Product Name");
				table.setColumnWidth(getProductNameColumn, 120, Unit.PX);
				getProductNameColumn.setSortable(true);
	}
	protected void addSortinggetPrice()
	{
		List<ServiceProduct> list=getDataprovider().getList();
		columnSort=new ListHandler<ServiceProduct>(list);
		columnSort.setComparator(getPriceColumn, new Comparator<ServiceProduct>()
				{
			@Override
			public int compare(ServiceProduct e1,ServiceProduct e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getPrice()== e2.getPrice()){
						return 0;}
					if(e1.getPrice()> e2.getPrice()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetPrice()
	{
		NumberCell editCell=new NumberCell();
		getPriceColumn=new Column<ServiceProduct,Number>(editCell)
				{
			@Override
			public Number getValue(ServiceProduct object)
			{
				if(object.getPrice()==0){
					return 0;}
				else{
					return object.getPrice();}
			}
				};
				table.addColumn(getPriceColumn,"Price");
				table.setColumnWidth(getPriceColumn, 120, Unit.PX);
				getPriceColumn.setSortable(true);
	}
	protected void addFieldUpdatergetPrice()
	{
		getPriceColumn.setFieldUpdater(new FieldUpdater<ServiceProduct, Number>()
				{
			@Override
			public void update(int index,ServiceProduct object,Number value)
			{
				Double val1=(Double) value;
				object.setPrice(val1);
				table.redrawRow(index);
			}
				});
	}
	/*protected void addColumngetLbtTax()
	{
		getLbtTaxColumn=new TextColumn<ServiceProduct>()
				{
			@Override
			public String getValue(ServiceProduct object)
			{
				return object.getLbtTax()+"";
			}
				};
				table.addColumn(getLbtTaxColumn,"Lbt");
	}
	*/
	
	protected void addColumngetStatus()
	{
		getStatusColumn=new TextColumn<ServiceProduct>()
				{
			@Override
			public String getValue(ServiceProduct object) {
				if(object.isStatus()==true)
                	return "Active";
                else
                	return "In Active";
			} 
        }
;
        table.addColumn(getStatusColumn, "Status");
        table.setColumnWidth(getStatusColumn, 120, Unit.PX);
        getStatusColumn.setSortable(true);
	}
	
	protected void addSortinggetDuration()
	{
		List<ServiceProduct> list=getDataprovider().getList();
		columnSort=new ListHandler<ServiceProduct>(list);
		columnSort.setComparator(getDurationColumn, new Comparator<ServiceProduct>()
				{
			@Override
			public int compare(ServiceProduct e1,ServiceProduct e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getDuration()== e2.getDuration()){
						return 0;}
					if(e1.getDuration()> e2.getDuration()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetDuration()
	{
		NumberCell editCell=new NumberCell();
		getDurationColumn=new Column<ServiceProduct,Number>(editCell)
				{
			@Override
			public Number getValue(ServiceProduct object)
			{
				if(object.getDuration()==0){
					return 0;}
				else{
					return object.getDuration();}
			}
				};
				table.addColumn(getDurationColumn,"Duration(In Days)");
				table.setColumnWidth(getDurationColumn, 120, Unit.PX);
				getDurationColumn.setSortable(true);
	}
	protected void addFieldUpdatergetDuration()
	{
		getDurationColumn.setFieldUpdater(new FieldUpdater<ServiceProduct, Number>()
				{
			@Override
			public void update(int index,ServiceProduct object,Number value)
			{
				Integer val1=(Integer) value;
				object.setDuration(val1);
				table.redrawRow(index);
			}
				});
	}
	protected void addSortinggetNumberOfServices()
	{
		List<ServiceProduct> list=getDataprovider().getList();
		columnSort=new ListHandler<ServiceProduct>(list);
		columnSort.setComparator(getNumberOfServicesColumn, new Comparator<ServiceProduct>()
				{
			@Override
			public int compare(ServiceProduct e1,ServiceProduct e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getNumberOfServices()== e2.getNumberOfServices()){
						return 0;}
					if(e1.getNumberOfServices()> e2.getNumberOfServices()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetNumberOfServices()
	{
		NumberCell editCell=new NumberCell();
		getNumberOfServicesColumn=new Column<ServiceProduct,Number>(editCell)
				{
			@Override
			public Number getValue(ServiceProduct object)
			{
				if(object.getNumberOfServices()==0){
					return 0;}
				else{
					return object.getNumberOfServices();}
			}
				};
				table.addColumn(getNumberOfServicesColumn,"No of Services");
				table.setColumnWidth(getNumberOfServicesColumn, 120, Unit.PX);
				getNumberOfServicesColumn.setSortable(true);
	}
	protected void addFieldUpdatergetNumberOfServices()
	{
		getNumberOfServicesColumn.setFieldUpdater(new FieldUpdater<ServiceProduct, Number>()
				{
			@Override
			public void update(int index,ServiceProduct object,Number value)
			{
				Integer val1=(Integer) value;
				object.setNumberOfServices(val1);
				table.redrawRow(index);
			}
				});
	}
	
	protected void addColumnUnitOfMeasurement()
	{
		getUnitColumn=new TextColumn<ServiceProduct>() {
			
			@Override
			public String getValue(ServiceProduct object) {
				return object.getUnitOfMeasurement();
			}
		};
		table.addColumn(getUnitColumn,"Unit of Measurement");
		table.setColumnWidth(getUnitColumn, 130, Unit.PX);
		getUnitColumn.setSortable(true);
	}
	
	protected void addSortinggetUnitOfMeasurement()
	{
		List<ServiceProduct> list=getDataprovider().getList();
		columnSort=new ListHandler<ServiceProduct>(list);
		columnSort.setComparator(getUnitColumn, new Comparator<ServiceProduct>()
				{
			@Override
			public int compare(ServiceProduct e1,ServiceProduct e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getUnitOfMeasurement()!=null && e2.getUnitOfMeasurement()!=null){
						return e1.getUnitOfMeasurement().compareTo(e2.getUnitOfMeasurement());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
}
