package com.slicktechnologies.client.views.device;

import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperTable;

import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;

import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.shared.*;
import com.slicktechnologies.shared.common.*;
import com.slicktechnologies.shared.common.businessprocesslayer.*;
import com.slicktechnologies.shared.common.businessunitlayer.*;
import com.slicktechnologies.shared.common.helperlayer.*;
import com.slicktechnologies.shared.common.personlayer.*;
import com.slicktechnologies.shared.common.productlayer.*;
import com.slicktechnologies.shared.common.scheduling.TeamManagement;
import com.slicktechnologies.client.views.device.DevicePresenter.DevicePresenterSearch;
import com.simplesoftwares.client.library.composite.CommonSearchComposite;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.google.gwt.user.client.ui.CheckBox;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utils.Console;

public class ServiceSearchPopUp extends SearchPopUpScreen<Service>{
	public IntegerBox tbServiceId;
	public IntegerBox tbContractId;
	public PersonInfoComposite personInfo;
	public ObjectListBox<Branch> olbBranch;
	public ObjectListBox<Employee> olbEmployee;
	public DateComparator dateComparator;
	public IntegerBox tbTicketNumber;
	public ListBox status;
	public TextBox tbRefNumber;
	 ObjectListBox<TeamManagement> olbTeam;
	 
	 // rohan added this on date : 19-12-2017 for pest guard
	 public ObjectListBox<Locality> olbLocality; 
	 
	 /**
		 * Description: These field is added For NBHC, so that they can search with WareHouse, StorageLocation and Stack present in Service Form
		 * Date:09-09-2016
		 * Project: EVA PEST ERP(NBHC )
		 * Required For: NBHC Required these to search.
		 */
		public TextBox tbwareHouse,tbstorageLocation,tbstack;
		/**
		 * End 
		 */
		
		/**
		 * Date:24-01-2017 By Anil
		 * Adding reference no.2 for PCAMB and Pest Cure Incorporation
		 */
		
		TextBox tbRefNo2;
		
		/**
		 * End
		 */		
		
		/**05-10-2017 sagar sore [To add field number range(billing and non billing) in search popup]**/
		ObjectListBox<Config> olbcNumberRange;
		
	 /***
	  * Date 03-04-2019 by Vijay
	  * Des :- Cancellation Service to search
	  */
	 public DateComparator cancellationDateComparator;	
		
	public ServiceSearchPopUp()
	{
		super();
		createGui();
	}
	public ServiceSearchPopUp(boolean b) {
		super(b);
		createGui();
	}
	public void initWidget()
	{
		tbContractId= new IntegerBox();
		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new Customer());
		personInfo=new PersonInfoComposite(querry,false);
		olbBranch= new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbBranch);
		
		//  rohan added this code on date : 19-12-2017
		
		olbLocality= new ObjectListBox<Locality>();
		olbLocality.addItem("--SELECT--");
		for (int i = 0; i < LoginPresenter.globalLocality.size(); i++) {
			
			if(LoginPresenter.globalLocality.get(i).isStatus()==true){
				olbLocality.addItem(LoginPresenter.globalLocality.get(i).getLocality());
			}
		}
		
		
		olbEmployee= new ObjectListBox<Employee>();
//		AppUtility.makeSalesPersonListBoxLive(olbEmployee);
		olbEmployee.makeEmployeeLive(AppConstants.SERVICEMODULE, AppConstants.CUSTOMERSERVICE, "Service Engineer");
		dateComparator=new DateComparator("serviceDate",new Service());
		status=new ListBox();
		AppUtility.setStatusListBox(status,Service.getStatusList());
		tbServiceId=new IntegerBox();
		tbTicketNumber=new IntegerBox();
		
		tbRefNumber =new TextBox();
		
		tbwareHouse=new TextBox();
		tbstorageLocation=new TextBox();
		tbstack=new TextBox();
		
		olbTeam = new ObjectListBox<TeamManagement>();
		this.initalizeTeamListBox();
		
		tbRefNo2=new TextBox();
		
		/**05-10-2017 sagar sore [ Number range initialization, Displaying values added from process configuration for number range]**/
		olbcNumberRange= new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbcNumberRange,Screen.NUMBERRANGE);
		
		/*** Date 03-04-2019 by Vijay for cancelled contract search filter ****/
		cancellationDateComparator = new DateComparator("cancellationDate", new Service());
	}
	
	private void initalizeTeamListBox() {
		MyQuerry querry = new MyQuerry();
		querry.setQuerryObject(new TeamManagement());
		olbTeam.MakeLive(querry);
	}
	
	public void createScreen()
	{
		initWidget();
		FormFieldBuilder builder;
		builder = new FormFieldBuilder("Contract Id",tbContractId);
		FormField ftbContractId= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("",personInfo);
		FormField fPersonInfo= builder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		builder = new FormFieldBuilder("Branch",olbBranch);
		FormField folbBranch= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		//  rohan added this code on date : 19-12-2017
	
		builder = new FormFieldBuilder("Locality",olbLocality);
		FormField folbLocality= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Service Engineer",olbEmployee);
		FormField folbEmployee= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Ticket Id",tbTicketNumber);
		FormField ftbTicketNumber= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("To Date (Service Date)",dateComparator.getToDate());
		FormField fdateComparator= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("From Date (Service Date)",dateComparator.getFromDate());
		FormField fdateComparator1= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Status",status);
		FormField fstatus= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Service Id",tbServiceId);
		FormField ftbserviceId= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Team",olbTeam);
		FormField olbTeam= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Ref Number 1",tbRefNumber);
		FormField frefNumber= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Warehouse",tbwareHouse);
		FormField ftbwareHouse= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Storage Location",tbstorageLocation);
		FormField ftbstorageLocation= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Stack",tbstack);
		FormField ftbstack= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Reference Number 2",tbRefNo2);
		FormField ftbRefNo2= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("* Note : If no filter is selected, by default today's data will be displayed on click of Go Button.",null);
		FormField fnote= builder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		
		/**05-10-2017 sagar sore[ To add field number range in search pop up]**/
		FormField folbcNumberRange;
		builder = new FormFieldBuilder("Number Range",olbcNumberRange);
		folbcNumberRange= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/**
		 * Date 25-01-2018 By Vijay 
		 * differentiated date combination search and other filter search
		 */
		builder = new FormFieldBuilder();
		FormField fgroupingDateFilterInformation=builder.setlabel("Date Filter Combination").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();

		builder = new FormFieldBuilder();
		FormField fgroupingOtherFilterInformation=builder.setlabel("Other Filter Combination").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		/**
		 * ends here
		 */
		
		/**
		 * Date 03-04-2019 by Vijay for Cancelled records search
		 */
		builder = new FormFieldBuilder("From Date (Cancelled Date)",cancellationDateComparator.getFromDate());
		FormField fcancellationFrmDateComparator= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("To Date (Cancelled Date)",cancellationDateComparator.getToDate());
		FormField fcancellationToDateComparator= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		this.fields=new FormField[][]{
				/**05-10-2017 sagar sore[ Number range field added and order of fields updated]**/
				/**old code**/
//				{ftbserviceId,ftbContractId,fdateComparator1,fdateComparator},
//				{folbEmployee,folbBranch,fstatus,ftbTicketNumber},
//				{olbTeam,frefNumber,ftbRefNo2},
//				{ftbwareHouse,ftbstorageLocation,ftbstack},
//				{fPersonInfo},
//				{fnote}
				/**updated order of fields**/
				{fgroupingDateFilterInformation},
				{fdateComparator1,fdateComparator,folbBranch,fstatus},
				{fcancellationFrmDateComparator,fcancellationToDateComparator,folbEmployee},
				{fPersonInfo},
				{fgroupingOtherFilterInformation},
				{ftbserviceId,ftbContractId,folbcNumberRange,frefNumber},
				{ftbRefNo2,ftbTicketNumber,folbLocality}, //olbTeam,
				{ftbwareHouse,ftbstorageLocation,ftbstack},
				{fnote}
		};
	}
	public MyQuerry getQuerry()
	{
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		if(dateComparator.getValue()!=null)
			filtervec.addAll(dateComparator.getValue());
		
		if(olbTeam.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbTeam.getValue().trim());
			temp.setQuerryString("team");
			filtervec.add(temp);
		}
		
		
		if(olbEmployee.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbEmployee.getValue().trim());
			temp.setQuerryString("employee");
			filtervec.add(temp);
		}
		
		if(LoginPresenter.branchRestrictionFlag==true && olbBranch.getSelectedIndex()==0)
		{
			temp=new Filter();
			temp.setList(AppUtility.getbranchlist());
			temp.setQuerryString("branch IN");
			filtervec.add(temp);
		}
		else{
			if(olbBranch.getSelectedIndex()!=0){
				temp=new Filter();temp.setStringValue(olbBranch.getValue().trim());
				temp.setQuerryString("branch");
				filtervec.add(temp);
			}
		}
		
		
		//  rohan added this code on date : 19-12-2017
		if(olbLocality.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbLocality.getValue(olbLocality.getSelectedIndex()).trim());
			temp.setQuerryString("address.locality");
			filtervec.add(temp);
		}
		
		if(tbContractId.getValue()!=null){
			temp=new Filter();
			temp.setIntValue(tbContractId.getValue());
			filtervec.add(temp);
			temp.setQuerryString("contractCount");
			filtervec.add(temp);
		}
		
		if(!tbRefNumber.getValue().equals("")){
			temp=new Filter();
			temp.setStringValue(tbRefNumber.getValue());
			filtervec.add(temp);
			temp.setQuerryString("refNo");
			filtervec.add(temp);

		}
		
		if(personInfo.getIdValue()!=-1)
		  {
		  temp=new Filter();
		  temp.setIntValue(personInfo.getIdValue());
		  temp.setQuerryString("personInfo.count");
		  filtervec.add(temp);
		  }
		  
		  if(!(personInfo.getFullNameValue().equals("")))
		  {
		  temp=new Filter();
		  temp.setStringValue(personInfo.getFullNameValue());
		  temp.setQuerryString("personInfo.fullName");
		  filtervec.add(temp);
		  }
		  
		  /**
		   * Commented By Priyanka
		   * Des : Search data using only Id and name as per requiremnet of Rahul Tiwari
		   * @author Anil @since 23-09-2021
		   * Adding cell number filter becuase indexes are uploaded in combination of cell number
		   * if we remove it data will not search properly
		   * @author Anil @since 25-10-2021
		   * For pecopp : added new indexes and commented this
		   */
//		  if(personInfo.getCellValue()!=-1l)
//		  {
//		  temp=new Filter();
//		  temp.setLongValue(personInfo.getCellValue());
//		  temp.setQuerryString("personInfo.cellNumber");
//		  filtervec.add(temp);
//		  }
		  
		  
		  /**@author Abhinav Bihade
		   * @Since 23/01/2020
		   * NBHC- Add two additional status 'Schedule / Reschedule' and 'Started / Reported / Tcompleted '
		   * on Service status dropdown list and data be searched accordingly from service search screen and service list search screen
		   */
		
		  if(this.status.getSelectedIndex()!=0)
		  {
			String selectedItem = status.getItemText(status.getSelectedIndex());
	
			if (selectedItem.equals(Service.SERVICESTATUSSCHEDULERESCHEDULE)) {
				
				ArrayList<String> scheduleRescheduleList = new ArrayList<String>();
				scheduleRescheduleList.add(Service.SERVICESTATUSSCHEDULE);
				scheduleRescheduleList.add(Service.SERVICESTATUSRESCHEDULE);
				
				temp=new Filter();
				  temp.setList(scheduleRescheduleList);
				  temp.setQuerryString("status IN");
				  filtervec.add(temp);
				  }
			
			else if(selectedItem.equals(Service.SERVICESTATUSSTARTEDREPORTEDTCOMPLETED)){
				
					ArrayList<String> startedReportedTcompletedList = new ArrayList<String>();
					startedReportedTcompletedList.add(Service.SERVICESTATUSSTARTED);
					startedReportedTcompletedList.add(Service.SERVICESTATUSREPORTED);
					startedReportedTcompletedList.add(Service.SERVICETCOMPLETED);
					
				
					temp=new Filter();
					  temp.setList(startedReportedTcompletedList);
					  temp.setQuerryString("status IN");
					  filtervec.add(temp);
					
				}
			else {
				
				temp = new Filter();
//				selectedItem = status.getItemText(status
//						.getSelectedIndex());
				temp.setStringValue(selectedItem);
				temp.setQuerryString("status");
				filtervec.add(temp);
				
			}
			}
		  
//		  if(this.status.getSelectedIndex()!=0)
//		  {
//		  temp=new Filter();
//		  String selectedItem=status.getItemText(status.getSelectedIndex());
//		  temp.setStringValue(selectedItem);
//		  temp.setQuerryString("status");
//		  filtervec.add(temp);
//		  }
		  
		  if(tbServiceId.getValue()!=null)
		  {
		  temp=new Filter();
		  
		  temp.setIntValue(tbServiceId.getValue());
		  temp.setQuerryString("count");
		  filtervec.add(temp);
		  }
		  
		  if(tbTicketNumber.getValue()!=null){
				temp=new Filter();
				temp.setIntValue(tbTicketNumber.getValue());
				filtervec.add(temp);
				temp.setQuerryString("ticketNumber");
				filtervec.add(temp);
			}
		  
		  if(!tbwareHouse.getValue().equals("")){
				temp=new Filter();
				temp.setStringValue(tbwareHouse.getValue());
				filtervec.add(temp);
				temp.setQuerryString("stackDetailsList.wareHouse");
				filtervec.add(temp);

			}
			
			if(!tbstorageLocation.getValue().equals("")){
				temp=new Filter();
				temp.setStringValue(tbstorageLocation.getValue());
				filtervec.add(temp);
				temp.setQuerryString("stackDetailsList.storageLocation");
				filtervec.add(temp);
			}
					
			if(!tbstack.getValue().equals("")){
				temp=new Filter();
				temp.setStringValue(tbstack.getValue());
				filtervec.add(temp);
				temp.setQuerryString("stackDetailsList.stackNo");
				filtervec.add(temp);
			}
			
			if(!tbRefNo2.getValue().equals("")){
				temp=new Filter();
				temp.setStringValue(tbRefNo2.getValue());
				temp.setQuerryString("refNo2");
				filtervec.add(temp);
			}
		
			

			/***05-10-2017 sagar sore[ filter for number range]**/
			  if(olbcNumberRange.getSelectedIndex()!=0){
					temp=new Filter();
					temp.setStringValue(olbcNumberRange.getValue().trim());
					temp.setQuerryString("numberRange");
					filtervec.add(temp);
				}
		
			  
			   /**
				 * Date : 03-04-2019 by Vijay for cancelled Date search filter
				 */
				if(cancellationDateComparator.getValue()!=null){
					filtervec.addAll(cancellationDateComparator.getValue());
				}
				
			/**
			 * Added by Rahul on 16 June 2017
			 * When we press GO directly then only today's data will get download.
			 */
			if(filtervec.size()==0){
				temp=new Filter();
				temp.setQuerryString("serviceDate"+" >=");
				temp.setDateValue(new Date());
				filtervec.add(temp);
				
				temp=new Filter();
				temp.setQuerryString("serviceDate"+" <=");
				temp.setDateValue(new Date());
				filtervec.add(temp);
			}
			
			
			  
		MyQuerry querry= new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Service());
		return querry;
	}
	@Override
	public boolean validate() {
//		if(LoginPresenter.branchRestrictionFlag)
//		{
//		if(olbBranch.getSelectedIndex()==0)
//		{
//			showDialogMessage("Select Branch");
//			return false;
//		}
//		}
		
		boolean flag = false;
		if(personInfo.getValue()!=null){
			flag = true;
		}
		if(!flag){
			if((dateComparator.getFromDate().getValue()!=null && dateComparator.getToDate().getValue()!=null)){
				int diffdays = AppUtility.getDifferenceDays(dateComparator.getFromDate().getValue(), dateComparator.getToDate().getValue());
				Console.log("diffdays "+diffdays);
				if(diffdays>31){
					showDialogMessage("Please select from date and to date range within 30 days");
					return false;
				}
			}
			
			if((cancellationDateComparator.getFromDate().getValue()!=null && cancellationDateComparator.getToDate().getValue()!=null)){
				int diffdays = AppUtility.getDifferenceDays(cancellationDateComparator.getFromDate().getValue(), cancellationDateComparator.getToDate().getValue());
				Console.log("diffdays "+diffdays);
				if(diffdays>31){
					showDialogMessage("Please select from date and to date range within 30 days");
					return false;
				}
			}
		}

		
		
		/**
		 * Date 25-01-2018 By Vijay 
		 * for Date combination validation
		 */
		/**
		 * Date 13-09-2018 By Vijay
		 * Des :- Bug :- We have updated for search filter with Service Date and Customer Name Search Combination
		 */
		String msg = "Wrong filter selection with From Date and To Date ! Please select proper filters with Date";
		if((dateComparator.getFromDate().getValue()!=null || dateComparator.getToDate().getValue()!=null) || 
				(cancellationDateComparator.getFromDate().getValue()!=null || cancellationDateComparator.getToDate().getValue()!=null) ){
			if(olbTeam.getSelectedIndex()!=0 || 
			  olbLocality.getSelectedIndex()!=0 || !tbRefNumber.getValue().equals("") ||	
			     tbServiceId.getValue()!=null || tbTicketNumber.getValue()!=null || !tbwareHouse.getValue().equals("") || 
			    !tbstorageLocation.getValue().equals("") || !tbstack.getValue().equals("")||
			    !tbRefNo2.getValue().equals("") || olbcNumberRange.getSelectedIndex()!=0 ){
				showDialogMessage(msg);
				return false;
			}
		}
		/**
		 * ends here
		 */
		return true;
	}
}
