package com.slicktechnologies.client.views.device;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.slicktechnologies.shared.Service;
@RemoteServiceRelativePath("service")
public interface SaveService extends RemoteService {
	public boolean saveStatus(Service service);
	public ArrayList<Service> mapBillingDetails(ArrayList<Service>sevicelist);

}
