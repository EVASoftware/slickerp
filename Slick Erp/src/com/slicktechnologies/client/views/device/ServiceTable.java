package com.slicktechnologies.client.views.device;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.SelectionCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.view.client.NoSelectionModel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.complain.Complain;
import com.slicktechnologies.shared.common.personlayer.Employee;

public class ServiceTable extends SuperTable<Service> {

	/**
	 * Service to Load Branch and Employee
	 */
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	/**
	 * Contract ID Column
	 */
	TextColumn<Service>contractIdCol;
	
	/**
	 * 
	 */
	TextColumn<Service>serviceIdCol;
	TextColumn<Service>refNumnber;
	/**
	 * View mode Service Eng Column
	 */
	private TextColumn<Service>serviceEng;
	/**
	 * View mode status Column
	 */
	private TextColumn<Service>status;
	/**
	 * View mode Date Column
	 */
	private TextColumn<Service>datecol;
	/**
	 * View mode Reason for Change column
	 */
	private TextColumn<Service>reasonForChange;
	
	private TextColumn<Service>customerID;
	private TextColumn<Service>customerName;
	private TextColumn<Service>customerPhone;
	TextColumn<Service>productNameCol;
	/**
	 * View mode branch Column
	 */
	private TextColumn<Service>branch;
	private TextColumn<Service>time;
	/**
	 * edit mode employee column
	 */
	private Column<Service, String> editserviceEng;
	/**
	 * edit mode date column
	 */
	private Column<Service, Date> editDate ;
	/**
	 * edit mode status column
	 */
	private Column<Service, String> editStatus;
	/**
	 * edit mode reason for change column
	 */
	private Column<Service, String> editReasonForChange;
	/**
	 * edit mode branch column
	 */
	private Column<Service, String> editBranchColumn;
	/**
	 * Status list contains list of Service Status, value is hard coded
	 */
	private ArrayList<String> statusList;
	/**
	 * Employee list contains List of employees , get filed from database
	 */
	public ArrayList<String> employeeList;
	/**
	 * Branch list contains list of branches
	 */
	public ArrayList<String> branchList;
	
	/**
	 * columnSort object to handle sorting
	 */
	ListHandler<Service> columnSort;
	
	
	TextColumn<Service>getColumnTicketID;
	
	private TextColumn<Service>team;
	
	/**
	 * Date : 24-01-2017 By Anil
	 * Added ref no. 2 for PCAAMB and Pest Cure Incorporation
	 */
	TextColumn<Service> getRefNo2;
	/**
	 * End
	 */
	
	/** 05-10-2017 sagar sore [adding Number range column ]**/
	TextColumn<Service> numberRangeColumn;
	
	public final GenricServiceAsync service=GWT.create(GenricService.class);
	
	//Patch Patch Patch
	public ServiceTable() {
		super();
		createStatusList();
		//Patch Patch Patch
		employeeList=new ArrayList<String>();
		branchList=new ArrayList<String>();
		retriveEmployees();
		retriveBranch();
		setHeight("800px");
		
	}

	
	public void retriveEmployees()
	{
		Vector<Filter>filtervalue=new Vector<Filter>(); 
		MyQuerry query=new MyQuerry();
		query.setFilters(filtervalue);
		query.setQuerryObject(new Employee());
		service.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
		@Override
		public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				
				for(SuperModel model:result)
				{
				 Employee entity=(Employee) model;
			     employeeList.add(entity.getFullname());
				}
				addEditColumnEmployee();
				
				
			}
		});
		
	}
	
	public void retriveBranch()
	{
		Vector<Filter>filtervalue=new Vector<Filter>(); 
		MyQuerry query=new MyQuerry();
		query.setFilters(filtervalue);
		query.setQuerryObject(new Branch());
		service.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				for(int i=0;i<result.size();i++)
				{
					Branch entity=(Branch) result.get(i);
					    branchList.add(entity.getBusinessUnitName());
					    
				}
				
				addEditColumnBranch();
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
		
	}

	@Override
	public void createTable() {
		//Adding in view Mode HERE VIEW COLUMNS ARE CREATED AND ADDED 
		
		/***  05-10-2017 sagar sore [ number range column added and order of columns updated]***/
		/**old code**/
//		addColumnserviceId();
//		addColumnRefNumber();
//		getRefNo2();
//		addColumnContractId();
//		createColumnTicketID();
//		addColumnCustomerId();
//		addColumnName();
//		addColumnPhone();
//		addColumnnProduct();
//		//addColumnAdress();
//		addColumnDate();
//		//addColumnTime();
//		addColumnServiceEng();
//		addColumnteam();
//		addColumnBranch();
//		addColumnStatus();
		
		/**updated code**/
		addColumnserviceId();
		addColumnRefNumber();
		getRefNo2();
		addColumnContractId();
		addColumnCustomerId();
		addColumnName();
		addColumnPhone();
		addColumnnProduct();
		addColumnStatus();
		addColumnBranch();
		addColumnNumberRange();
		addColumnDate();
		createColumnTicketID();
		addColumnServiceEng();
		addColumnteam();
		
	}
	
	private void getRefNo2() {
	getRefNo2=new TextColumn<Service>() {
		@Override
		public String getValue(Service object) {
			if(object.getRefNo2()!=null){
				return object.getRefNo2()+"";
			}else
				return "N.A.";
		}
	};
	table.addColumn(getRefNo2, "Ref No.2");
	table.setColumnWidth(getRefNo2,100,Unit.PX);
	getRefNo2.setSortable(true);
}

private void addColumnRefNumber() {
	refNumnber=new TextColumn<Service>() {

		@Override
		public String getValue(Service object) {
			return object.getRefNo()+"";
		}
	};
	table.addColumn(refNumnber,"Ref No");
	table.setColumnWidth(refNumnber,"100px");
	refNumnber.setSortable(true);		
	}


/***************************************Column Addition Part***************************************************/
	
	private void addColumnteam() {
		
		team =new TextColumn<Service>() {
			
			@Override
			public String getValue(Service object) {
				if(object.getTeam()!=null)
				{
					return object.getTeam();
				}
				
				return "";
			}
			
			
		};
		
		table.addColumn(team,"Team");
		table.setColumnWidth(team,90,Unit.PX);
		team.setSortable(true);
	}

	
	private void createColumnTicketID() {
		getColumnTicketID=new TextColumn<Service>() {
			@Override
			public String getValue(Service object) {
				if(object.getTicketNumber()!=-1){
				return object.getTicketNumber()+"";
				}else
					return "N.A.";
			}
		};
		table.addColumn(getColumnTicketID, "Ticket Id");
		table.setColumnWidth(getColumnTicketID,120,Unit.PX);
		getColumnTicketID.setSortable(true);
		
	}
	
	private void addColumnTicketNoSorting() {
		List<Service> list = getDataprovider().getList();
		columnSort = new ListHandler<Service>(list);
		Comparator<Service> quote = new Comparator<Service>() {
			@Override
			public int compare(Service e1, Service e2) {
				if (e1 != null && e2 != null) {
					if(e1.getTicketNumber()== e2.getTicketNumber()){
					  return 0;}
				  if(e1.getTicketNumber()> e2.getTicketNumber()){
					  return 1;}
				  else{
					  return -1;}} else
					return 0;
			}
		};
		columnSort.setComparator(getColumnTicketID, quote);
		table.addColumnSortHandler(columnSort);
	}
	
	
	private void addColumnnProduct() {
		productNameCol=new TextColumn<Service>() {

			@Override
			public String getValue(Service object) {
				// TODO Auto-generated method stub
				return object.getProductName();
			}
		};
		
		table.addColumn(productNameCol,"Product");
		table.setColumnWidth(productNameCol,120,Unit.PX);
		productNameCol.setSortable(true);
		
	}
	
	public void addColumnContractId()
	{
		contractIdCol=new TextColumn<Service>() {

			@Override
			public String getValue(Service object) {
				// TODO Auto-generated method stub
				
				return object.getContractCount()+"";
			}
		};
		
		table.addColumn(contractIdCol,"Contract ID");
		table.setColumnWidth(contractIdCol,100,Unit.PX);
		contractIdCol.setSortable(true);
	}
	
	public void addColumnName()
	{
		customerName=new TextColumn<Service>() {

			@Override
			public String getValue(Service object) {
				// TODO Auto-generated method stub
				return object.getName();
			}
		};
		
		table.addColumn(customerName,"Customer Name");
		table.setColumnWidth(customerName,150,Unit.PX);
		customerName.setSortable(true);
	}
	
	public void addColumnBranch()
	{
		branch=new TextColumn<Service>() {

			@Override
			public String getValue(Service object) {
				// TODO Auto-generated method stub
				return object.getBranch();
			}
		};
		
		table.addColumn(branch,"Branch");
		table.setColumnWidth(branch,90,Unit.PX);
	    branch.setSortable(true);
	}
	
	public void addColumnPhone()
	{
		customerPhone=new TextColumn<Service>() {

			@Override
			public String getValue(Service object) {
				// TODO Auto-generated method stub
				return object.getCellNumber()+"";
			}
		};
		
		table.addColumn(customerPhone,"Phone");
		table.setColumnWidth(customerPhone,130,Unit.PX);
		customerPhone.setSortable(true);
	}
	
	public void addColumnAdress()
	{
		TextColumn<Service>customerAdress=new TextColumn<Service>() {

			@Override
			public String getValue(Service object) {
				
				if(object.getAddress()!=null)
				{
					return object.getAddress().toString();
				}
				return "N.A";
			}
		};
		
		table.addColumn(customerAdress,"Customer Adress");
		customerAdress.setSortable(true);
	}
	
	public void addColumnDate()
	{
		datecol=new TextColumn<Service>() {

			@Override
			public String getValue(Service object) {
				// TODO Auto-generated method stub
				if(object.getServiceDate()!=null)
				{
				    String serDate=AppUtility.parseDate(object.getServiceDate());
				    return serDate;
				}
				return "Not Set";
			}
		};
		
		table.addColumn(datecol,"Service Date");
        table.setColumnWidth(datecol,120,Unit.PX);
		datecol.setSortable(true);
	}
	
	public void addColumnReasonForChange()
	{
		reasonForChange=new TextColumn<Service>() {

			@Override
			public String getValue(Service object) {
				if(object.getReasonForChange().trim().equals(""))
					return "N.A";
				return object.getReasonForChange();
			}
		};
		
		table.addColumn(reasonForChange,"Change Reason");
		table.setColumnWidth(reasonForChange,120,Unit.PX);
		reasonForChange.setSortable(true);
	}
	
	public void addColumnServiceEng()
	{
		serviceEng=new TextColumn<Service>() {

			@Override
			public String getValue(Service object) {
				// TODO Auto-generated method stub
				return object.getEmployee();
			}
		};
		
		table.addColumn(serviceEng,"Service Engineer");
		table.setColumnWidth(serviceEng,120,Unit.PX);
		serviceEng.setSortable(true);
	}
	
	public void addColumnStatus()
	{
		status=new TextColumn<Service>() {

			@Override
			public String getValue(Service object) {
			
				return object.getStatus();
			}
		};
		
		table.addColumn(status,"Status");
		table.setColumnWidth(status,100,Unit.PX);
		status.setSortable(true);
	}
	
	
	public void addColumnCustomerId()
	{
		customerID=new TextColumn<Service>() {

			@Override
			public String getValue(Service object) {
			
				return object.getCustomerId()+"";
			}
		};
		
		table.addColumn(customerID,"Customer ID");
		table.setColumnWidth(customerID,110,Unit.PX);
		customerID.setSortable(true);
	}
	
	public void addEditColumnReasonForchange()
	{
		  
			EditTextCell editcel=new EditTextCell();
		    editReasonForChange=new Column<Service, String>(editcel) {

						@Override
						public String getValue(Service object) {
							 if(object.getReasonForChange().trim().equals(""))
								 return "N.A";
							 return object.getReasonForChange();
						}
					};
					
					table.addColumn(editReasonForChange,"Change Reason");
					table.setColumnWidth(editReasonForChange,150,Unit.PX);
					
	}
	
	public void addEditColumnBranch()
	{
		SelectionCell branchselection=new SelectionCell(branchList);
		editBranchColumn=new Column<Service, String>(branchselection) {

			@Override
			public String getValue(Service object) {	
				
				
				return object.getBranch();
			}
		};
		
		
		
	}
	
	public void addEditColumnEmployee()
	{
		SelectionCell employeeselection=new SelectionCell(employeeList);
		editserviceEng=new Column<Service, String>(employeeselection) {

			@Override
			public String getValue(Service object) {	
				
				
				return object.getEmployee();
			}
		};
		
	}
	
	public void addEditColumnDate()
	{
	
		DateTimeFormat fmt = DateTimeFormat.getFormat(PredefinedFormat.DATE_SHORT);
		  
	      DatePickerCell startdate= new DatePickerCell(fmt);
		  editDate = new Column<Service,Date>(startdate) {

				@Override
				public Date getValue(Service object) {
					// TODO Auto-generated method stub
					if(object.getServiceDate()==null)
						return new Date();
					else 
						return object.getServiceDate();
				}
			};
			table.addColumn(editDate,"Service Date");
			table.setColumnWidth(editDate,120,Unit.PX);
			editDate.setSortable(true);
	}
	
	
	public void addEditColumnStatus()
	{
		 SelectionCell selctcell=new SelectionCell(statusList);
		editStatus=new Column<Service, String>(selctcell) {

			@Override
			public String getValue(Service object) {
				
				return object.getStatus();
			}
		};
		table.addColumn(editStatus,"Status");
		table.setColumnWidth(editStatus,"130px");
	   editStatus.setSortable(true);
	}
	
	public void addColumnserviceId()
	{
		
		serviceIdCol=new TextColumn<Service>() {

			@Override
			public String getValue(Service object) {
				return object.getCount()+"";
			}
		};
		table.addColumn(serviceIdCol,"Service ID");
		table.setColumnWidth(serviceIdCol,"100px");
		serviceIdCol.setSortable(true);
	}
	
/***************************************Field Updater Part*********************************************************/	
	
	
	
	public void addFieldUpdaterBranch()
	{
		  editBranchColumn.setFieldUpdater(new FieldUpdater<Service, String>() {

				@Override
				public void update(int index, Service object, String value) {
					if(value.contains("Select")==false)
					{
						object.setBranch(value);
					    table.redrawRow(index);
					    
					}
					}
			});
		  table.addColumn(editBranchColumn,"Branch");
		  editBranchColumn.setSortable(true);
	}
	
	
	
	
	public void addFieldUpdaterEmployee()
	{
		  editserviceEng.setFieldUpdater(new FieldUpdater<Service, String>() {

				@Override
				public void update(int index, Service object, String value) {
					if(value.contains("Select")==false)
					{
						object.setEmployee(value);
					    table.redrawRow(index);
					    
					}
					}
			});
    
	}
	
	public void addFiledUpdaterDate()
	{
		editDate.setFieldUpdater(new FieldUpdater<Service, Date>() {

			@Override
			public void update(int index, Service object, Date value) {
				object.setServiceDate(value);
				table.redrawRow(index);
				
			}
		});
		
	}
	
	public void addFiledUpdaterReasonForchange()
	{
		editReasonForChange.setFieldUpdater(new FieldUpdater<Service, String>() {

			@Override
			public void update(int index, Service object, String value) {
				object.setReasonForChange(value);
				table.redrawRow(index);
			}
		});
		

	}
	
	
	
	
	
	
	public void addFieldUpdaterStatus()
	{
         editStatus.setFieldUpdater(new FieldUpdater<Service, String>() {
			
			@Override
			public void update(int index, Service object, String value) {
				
				object.setStatus(value);
				if(object.getStatus().equals(Service.SERVICESTATUSCANCELLED))
				     table.getRowElement(index).getStyle().setBackgroundColor("Red");
				
				if(object.getStatus().equals(Service.SERVICESTATUSCOMPLETED))
					 table.getRowElement(index).getStyle().setBackgroundColor("Green");
				
				if(object.getStatus().equals(Service.SERVICESTATUSRESCHEDULE))
				{
					     Service rescheduled = null;
						 rescheduled = object.Myclone();
						
					     
						
						 object.setStatus(Service.SERVICESTATUSRESCHEDULE);
						 object.setReasonForChange("Service Rescheduled");
						 rescheduled.setStatus(Service.SERVICESTATUSSCHEDULE);
						 if(rescheduled!=null)
						   getDataprovider().getList().add(rescheduled);
						 Collections.sort(getDataprovider().getList());
						 getDataprovider().refresh();
						 //table.getRowElement(index).getStyle().setBackgroundColor("YELLOW");
						 
				}
				
			}
		});
		
	}
	
	/***************************************Field Updater Part*********************************************************/	
	
	public void downloadServiceecsv()
	{
		ArrayList<Service> custarray=new ArrayList<Service>();
		List<Service> list=(List<Service>) getDataprovider().getList();
		
		custarray.addAll(list);
		
		csvservice.setservice(custarray, new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);
				
			}

			@Override
			public void onSuccess(Void result) {
				final String url=GWT.getModuleBaseURL() + "csvservlet"+"?type="+5;
				Window.open(url, "test", "enabled");
			}
		});
    }



	public void createStatusList()
	{
		statusList=new ArrayList<String>();
		statusList.add(Service.SERVICESTATUSCANCELLED);
		statusList.add(Service.SERVICESTATUSCOMPLETED);
		statusList.add(Service.SERVICESTATUSPENDING);
		statusList.add(Service.SERVICESTATUSRESCHEDULE);
		statusList.add(Service.SERVICESTATUSSCHEDULE);
		
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}
	
	public void addColumnSorting()
	{
		addServiceIdSorting();
		addsortingRefNumber();
		addContractIdSorting();
		addColumnTicketNoSorting();
		addCustomerIdSorting();
		addNameSorting();
		addPhoneSorting();
		addSProductSorting();
		addServiceEnggSorting();
		addDateSorting();
		addReasonForChangeSorting();
		addBranchSorting();
		addStatusSorting();
		addTeamSorting();
		addsortingRefNumber2();		
		/***05-10-2017 sagar sore[ sorting for number range]**/
		addSortinggetNumberRange();
	}
	private void addsortingRefNumber2() {
		
		List<Service> list=getDataprovider().getList();
		columnSort=new ListHandler<Service>(list);
		columnSort.setComparator(getRefNo2, new Comparator<Service>() {
			
			@Override
			public int compare(Service e1, Service e2) {
				if(e1!=null && e2!=null)
				{
					if( e1.getRefNo2()!=null && e2.getRefNo2()!=null){
						return e1.getRefNo2().compareTo(e2.getRefNo2());}
				}
				else{
					return 0;}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	private void addsortingRefNumber() {
		
		List<Service> list=getDataprovider().getList();
		columnSort=new ListHandler<Service>(list);
		columnSort.setComparator(refNumnber, new Comparator<Service>() {
			
			@Override
			public int compare(Service e1, Service e2) {
				if(e1!=null && e2!=null)
				{
					if( e1.getRefNo()!=null && e2.getRefNo()!=null){
						return e1.getRefNo().compareTo(e2.getRefNo());}
				}
				else{
					return 0;}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}


	private void addTeamSorting() {
		
		List<Service> list=getDataprovider().getList();
		columnSort=new ListHandler<Service>(list);
		columnSort.setComparator(team, new Comparator<Service>() {
			
			@Override
			public int compare(Service e1, Service e2) {
				if(e1!=null && e2!=null)
				{
					if( e1.getTeam()!=null && e2.getTeam()!=null){
						return e1.getTeam().compareTo(e2.getTeam());}
				}
				else{
					return 0;}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}


	public void addServiceIdSorting()
	{
		List<Service> list=getDataprovider().getList();
		columnSort=new ListHandler<Service>(list);
		columnSort.setComparator(serviceIdCol, new Comparator<Service>() {

			@Override
			public int compare(Service e1, Service e2) {
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	public void addContractIdSorting()
	{
		List<Service> list=getDataprovider().getList();
		columnSort=new ListHandler<Service>(list);
		columnSort.setComparator(contractIdCol, new Comparator<Service>() {

			@Override
			public int compare(Service e1, Service e2) {
				if(e1!=null && e2!=null)
				{
					if(e1.getContractCount()== e2.getContractCount()){
						return 0;}
					if(e1.getContractCount()> e2.getContractCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
		});
		table.addColumnSortHandler(columnSort);	
	}
	
	public void addCustomerIdSorting()
	{
		List<Service> list=getDataprovider().getList();
		columnSort=new ListHandler<Service>(list);
		columnSort.setComparator(customerID, new Comparator<Service>() {

			@Override
			public int compare(Service e1, Service e2) {
				if(e1!=null && e2!=null)
				{
					if(e1.getCustomerId()== e2.getCustomerId()){
						return 0;}
					if(e1.getCustomerId()> e2.getCustomerId()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	public void addNameSorting()
	{
		List<Service> list=getDataprovider().getList();
		columnSort=new ListHandler<Service>(list);
		columnSort.setComparator(customerName, new Comparator<Service>() {
			
			@Override
			public int compare(Service e1, Service e2) {
				if(e1!=null && e2!=null)
				{
					if( e1.getCustomerName()!=null && e2.getCustomerName()!=null){
						return e1.getCustomerName().compareTo(e2.getCustomerName());}
				}
				else{
					return 0;}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	public void addPhoneSorting()
	{
		List<Service> list=getDataprovider().getList();
		columnSort=new ListHandler<Service>(list);
		columnSort.setComparator(customerPhone, new Comparator<Service>() {
			
			@Override
			public int compare(Service e1, Service e2) {
				if(e1!=null && e2!=null)
				{
					if(e1.getCellNumber()== e2.getCellNumber()){
						return 0;}
					if(e1.getCellNumber()> e2.getCellNumber()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	public void addSProductSorting()
	{
		List<Service> list=getDataprovider().getList();
		columnSort=new ListHandler<Service>(list);
		columnSort.setComparator(productNameCol, new Comparator<Service>() {
			
			@Override
			public int compare(Service e1, Service e2) {
				if(e1!=null && e2!=null)
				{
					if( e1.getProductName()!=null && e2.getProductName()!=null){
						return e1.getProductName().compareTo(e2.getProductName());}
				}
				else{
					return 0;}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	public void addDateSorting()
	{
		List<Service> list=getDataprovider().getList();
		columnSort=new ListHandler<Service>(list);
		columnSort.setComparator(datecol, new Comparator<Service>() {
			
			@Override
			public int compare(Service e1, Service e2) {
				if(e1!=null && e2!=null)
				{
					if( e1.getServiceDate()!=null && e2.getServiceDate()!=null){
						return e1.getServiceDate().compareTo(e2.getServiceDate());}
				}
				else{
					return 0;}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	public void addServiceEnggSorting()
	{
		List<Service> list=getDataprovider().getList();
		columnSort=new ListHandler<Service>(list);
		columnSort.setComparator(serviceEng, new Comparator<Service>() {
			
			@Override
			public int compare(Service e1, Service e2) {
				if(e1!=null && e2!=null)
				{
					if( e1.getEmployee()!=null && e2.getEmployee()!=null){
						return e1.getEmployee().compareTo(e2.getEmployee());}
				}
				else{
					return 0;}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);

	}
	
	
	public void addStatusSorting()
	{
		List<Service> list=getDataprovider().getList();
		columnSort=new ListHandler<Service>(list);
		columnSort.setComparator(status, new Comparator<Service>() {
			
			@Override
			public int compare(Service e1, Service e2) {
				if(e1!=null && e2!=null)
				{
					if( e1.getStatus()!=null && e2.getStatus()!=null){
						return e1.getStatus().compareTo(e2.getStatus());}
				}
				else{
					return 0;}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	
	public void addReasonForChangeSorting()
	{
		List<Service> list=getDataprovider().getList();
		columnSort=new ListHandler<Service>(list);
		Comparator<Service>quote= new Comparator<Service>()
				{

					@Override
					public int compare(Service e1, Service e2) 
					{
						if(e1!=null && e2!=null)
						{
							if(e1.getReasonForChange()!=null&&e2.getReasonForChange()!=null)
								return e1.getReasonForChange().compareTo(e2.getReasonForChange());
							return 0;
						}
						else
							return 0;
					}

				};
				
			columnSort.setComparator(reasonForChange, quote);
			columnSort.setComparator(editReasonForChange, quote);
			table.addColumnSortHandler(columnSort);
	}
	
	public void addBranchSorting()
	{
		List<Service> list=getDataprovider().getList();
		columnSort=new ListHandler<Service>(list);
		columnSort.setComparator(branch, new Comparator<Service>() {
			
			@Override
			public int compare(Service e1, Service e2) {
				if(e1!=null && e2!=null)
				{
					if( e1.getBranch()!=null && e2.getBranch()!=null){
						return e1.getBranch().compareTo(e2.getBranch());}
				}
				else{
					return 0;}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	/**05-10-2017 sagar sore [creating number range column and sorting for it]**/
	public void addColumnNumberRange()
	{
		numberRangeColumn=new TextColumn<Service>() {
			@Override
			public String getValue(Service object) {
				return object.getNumberRange();
			}
		};
		
		table.addColumn(numberRangeColumn,"Number Range");
		table.setColumnWidth(numberRangeColumn, 120, Unit.PX);
		numberRangeColumn.setSortable(true);
	}	
	
	protected void addSortinggetNumberRange()
	{
		List<Service> list=getDataprovider().getList();
		columnSort=new ListHandler<Service>(list);
		columnSort.setComparator(numberRangeColumn, new Comparator<Service>()
				{
			@Override
			public int compare(Service e1,Service e2)
			{
				if (e1 != null && e2 != null) {
					
					if (e1.getNumberRange() == null && e2.getNumberRange() == null){
						return 0;
					}
					if(e1.getNumberRange() == null) return 1;							
					if(e2.getNumberRange() == null) return -1;							
					return e1.getNumberRange().compareTo(e2.getNumberRange());

				}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	/**end**/



	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void setEnable(boolean state) {
		/*for(int i=table.getColumnCount()-1;i>=0;i--)
			table.removeColumn(i);
		ScreeenState screenstate=AppMemory.getAppMemory().currentState;
		
		
		if((state==true&& screenstate==ScreeenState.EDIT))
		{
			addColumnserviceId();
			addColumnContractId();
			addColumnName();
			addColumnPhone();
			addColumnnProduct();
		    addEditColumnDate();
		    table.addColumn(editserviceEng,"Service Engineer");
		    table.setColumnWidth(editserviceEng,150,Unit.PX);
		    addFieldUpdaterEmployee();
		    addColumnStatus();
		    addFieldUpdaterStatus();
		    table.addColumn(editBranchColumn,"Branch");
		    table.setColumnWidth(editBranchColumn,120,Unit.PX);
		    addEditColumnReasonForchange();
		    addFiledUpdaterReasonForchange();
		    addColumnSorting();
		    
			
		}
		
		else if((state==true&&(screenstate==screenstate.NEW)))
		{
			addColumnserviceId();
			addColumnContractId();
			addColumnName();
			addColumnPhone();
			addColumnnProduct();
			addColumnDate();
			addColumnServiceEng();
			addColumnStatus();
			addColumnBranch();
			addColumnReasonForChange();
			addColumnSorting();
			
		}
		
		else if((state==false)&&(screenstate==screenstate.VIEW))
		{
			
			addColumnContractId();
			addColumnName();
			addColumnPhone();
			addColumnnProduct();
			addColumnDate();
			addColumnServiceEng();
			addColumnStatus();
			addColumnBranch();
			addColumnReasonForChange();
			addColumnSorting();
			
			
		}*/
		
	}



	@Override
	public void applyStyle() {
		
		
	}
	
}
