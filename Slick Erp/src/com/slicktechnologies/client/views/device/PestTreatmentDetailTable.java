package com.slicktechnologies.client.views.device;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;

public class PestTreatmentDetailTable extends SuperTable<PestTreatmentDetails> {

	TextColumn<PestTreatmentDetails>treatmentAreaCol;
	TextColumn<PestTreatmentDetails>methodOfApplicationCol;
	TextColumn<PestTreatmentDetails>pestTargetCol;
	Column<PestTreatmentDetails, String> deleteColumn;
	
	@Override
	public void createTable() {
		treatmentAreaCol();
		methodOfApplicationCol();
		pestTargetCol();
		createColumndeleteColumn();
	}
	
	private void createColumndeleteColumn() {
		ButtonCell btnCell = new ButtonCell();
		deleteColumn = new Column<PestTreatmentDetails, String>(btnCell) {
			@Override
			public String getValue(PestTreatmentDetails object) {
				return "Delete";
			}
		};		
		table.addColumn(deleteColumn, "Remove");
		table.setColumnWidth(deleteColumn,120,Unit.PX);
		
		deleteColumn.setFieldUpdater(new FieldUpdater<PestTreatmentDetails,String>()
		{
			@Override
			public void update(int index,PestTreatmentDetails object,String value)
			{
				getDataprovider().getList().remove(object);
				table.redrawRow(index);
			}
		});
	}
	
	private void methodOfApplicationCol(){
		methodOfApplicationCol=new TextColumn<PestTreatmentDetails>() {
			@Override
			public String getValue(PestTreatmentDetails object) {
				if (object.getMethodOfApplication() != null) {
					return object.getMethodOfApplication();
				} else{
					return "";
				}
			}
		};
		table.addColumn(methodOfApplicationCol, "Method of application");
		table.setColumnWidth(methodOfApplicationCol,120,Unit.PX);
		methodOfApplicationCol.setSortable(true);
		
	}
	private void pestTargetCol(){
		pestTargetCol=new TextColumn<PestTreatmentDetails>() {
			@Override
			public String getValue(PestTreatmentDetails object) {
				if (object.getPestTarget() != null) {
					return object.getPestTarget() + "";
				} else{
					return "";
				}
			}
		};
		table.addColumn(pestTargetCol, "Target");//Ashwini Patil Date:06-05-2022 changing "Pest Target" to "Target"
		table.setColumnWidth(pestTargetCol,120,Unit.PX);
		pestTargetCol.setSortable(true);
		
	}
	
	private void treatmentAreaCol(){
		treatmentAreaCol=new TextColumn<PestTreatmentDetails>() {
			@Override
			public String getValue(PestTreatmentDetails object) {
				
				if (object.getTreatmentArea() != null) {
					return object.getTreatmentArea() + "";
				} else{
					return "";
				}
			}
		};
		table.addColumn(treatmentAreaCol, "Applicable Area");//Ashwini Patil Date:06-05-2022 changing "Treatment Area" to Applicable Area
		table.setColumnWidth(treatmentAreaCol,120,Unit.PX);
		treatmentAreaCol.setSortable(true);
		
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		for(int i=table.getColumnCount()-1;i>-1;i--)
	    	  table.removeColumn(i); 
	        
		if(state){
			treatmentAreaCol();
			methodOfApplicationCol();
			pestTargetCol();
			createColumndeleteColumn();
		}else{
			treatmentAreaCol();
			methodOfApplicationCol();
			pestTargetCol();
		}
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
