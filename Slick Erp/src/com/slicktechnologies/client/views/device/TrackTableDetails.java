package com.slicktechnologies.client.views.device;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;

@Embed
@Index
public class TrackTableDetails extends SuperModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2620706351053120402L;

	int srNo;
	String status;
	String date_time;
	/**
	 * Updated By: Viraj
	 * Date: 01-03-2019
	 * Description: added lat ,long and location
	 */
	String latitude;
	String longitude;
	String locationName;
	/** Ends **/
	/** date 3.4.2019 added by komal to save time seperately **/
	String time;
	double kiloMeters;
	
	public TrackTableDetails(){
		super();
	}
	/**
	 *Updated By: Viraj
	 * Date: 01-03-2019
	 * Description: added lat ,long and location
	 */
	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	/** Ends **/


	public int getSrNo() {
		return srNo;
	}
	public void setSrNo(int srNo) {
		this.srNo = srNo;
	}

	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDate_time() {
		return date_time;
	}
	public void setDate_time(String date_time) {
		this.date_time = date_time;
	}
	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public double getKiloMeters() {
		return kiloMeters;
	}
	public void setKiloMeters(double kiloMeters) {
		this.kiloMeters = kiloMeters;
	}
	
}
