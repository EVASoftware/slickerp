package com.slicktechnologies.client.views.device;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.LongBox;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.FlexForm;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.AddressComposite;
import com.simplesoftwares.client.library.composite.CompositeInterface;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.composite.UploadComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.MyLongBox;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.contract.ContractForm;
import com.slicktechnologies.client.views.contract.ContractPresenter;
import com.slicktechnologies.client.views.contract.ShowMonthlyPeriodPopup;
import com.slicktechnologies.client.views.customerDetails.CustomerDetailsForm;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.client.views.history.History;
import com.slicktechnologies.client.views.project.concproject.MaterialProductGroupTable;
import com.slicktechnologies.client.views.project.concproject.ProjectPresenter;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.MultipleExpenseMngt;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.helperlayer.ProductInfo;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.scheduling.TeamManagement;
import com.slicktechnologies.shared.common.service.StackDetails;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;

public class DeviceForm extends FormScreen<Service>  implements ClickHandler , ChangeHandler
{
	TextBox tbContractId,ibServiceId,tbWarrantyDate;
	public TextBox tbStatus;
	DateBox tbServiceDate,tbServiceDateCopy;
	ObjectListBox<Branch> tbBranch;
	ObjectListBox<Employee> tbServiceEngg,tbServiceEnggCopy;
	TextArea taReasonForChange;
	UploadComposite upTestReport,upDevice;
	PersonInfoComposite poc;
//	AddressComposite addr;
    DateBox startDate,endDate;
    ModificationHistoryTable table;
    IntegerBox ibServiceSrNo;
    ProductInfoComposite prodComposite;
	DateBox dbServiceCompletionDate,dbServiceCompletionDateCopy;
	IntegerBox ibServiceDuration;
	IntegerBox ibTotalServiceDuration;
	TextBox tbservicecompletionremark;
	ObjectListBox<Config> olbservicetype;
	TextBox tbServiceday,tbServicetime;
	
	TextBox tbTicketNumber;
	
	TextBox tbCustomerFeedback;
	DoubleBox dbServicingTime;
	
	String f_markRemark;
	int f_markduration;
	
	//  rohan added this code for ref no in service bpt requirment / Ankita 
	
	TextBox refNo;
	TextBox premisesDetails;
	TextArea remark;
	/**
	 * Date 2-7-2018 by jayshree
	 */
	TextArea technicianRemark;
	TextArea team;//Added by sheetal
	//  rohan added this code for 
	
	ObjectListBox<Config> pestName;
	IntegerBox pestCount;
	Button catchAddButton ;
	CatchTrapsTable catchTrap;
	
	/**
	 * Added by Rahul Verma from NBHC
	 * 
	 */
		Button btstackDetails;
		UploadComposite uploadCustomerSignature;
		TextArea tacomment;
		TextBox customerSignName;
		TextBox customerSignNumber;
		
		/***30-12-2019 Deepak Salve added this code for Designation & Emp code***/
		TextBox customerSignPersonDesignation;
		TextBox customerSignPersonEmpcode;
		/***End***/
	
	// Done Adding by Rahul
	
	
	 public ServiceProduct serviceProdEntity=new ServiceProduct();
	 public Address addresInfo=new Address();
	 public int serviceAdHocIndex=0;
	 public static boolean prodflag=false;
	 public static boolean flag=false;
	 
	 public boolean ticketBillableFlage=false;
	 
	 AddressComposite addressComp;
	 ObjectListBox<TeamManagement> olbTeam;
	 
	 
	 /**
	 * Date:24-01-2017 By Anil
	 * Adding reference no.2 for PCAMB and Pest Cure Incorporation
	 */
	
	 TextBox tbRefNo2;
	
	/**
	 * End
	 */
	 
	 /**
	  * Date 03-04-2017
	  * added by vijay for service week no
	  */
	 IntegerBox ibServiceWeekNo;
	 /**
	  * ends here
	  */
	 
	 /**
	  * Added by Rahul on 16 June 2017
	  * Two field named quantity and UOM
	  */
	 DoubleBox quantity;
	 ObjectListBox<Config> olbUOM;
	 /**
	  * Rahul changes ends here
	  */
	 
	 /**29-09-2017 sagar sore [ adding service value box ]**/
	  DoubleBox serviceValue;
	  
    /**
	 * Date 14-02-2018 By vijay for Adhoc service setting Number Range;
	 */
	public String numberRange=null;
	 
	 Service serviceObj;
	 
	 /** date 16.02.2018 added by komal for area wise biling **/
	 DoubleBox  dlbPerUnitPrice;
	 
	 /**
	  * nidhi
	  * 8-08-2018
	  * map for model and serialnumber
	  */
	 
	 TextBox tbProModelNo;
	 TextBox tbProSerialNo;
	 /**
	  * nidhi
	  * 15-10-2018
	  * 
	  */
	 TextBox tbBillingCount;
	 
	 /**
	  * @author Anil ,Date : 16-03-2019
	  * upload composite to show service images sent from pedio app
	  */
	 UploadComposite ucServiceImage1;
	 UploadComposite ucServiceImage2;
	 UploadComposite ucServiceImage3;
	 UploadComposite ucServiceImage4;
	 /** date 26.3.2019 added by komal to add location / number in trap catches **/
	 TextBox tbLocation;
	 FormField fgroupingCustomerInformation  ,
		fserviceid,ftbContractId,ftbStartDate,ftbEndDate,
		fpic,
		ftbTicketNumber,fdbServicingTime,ftbReferenceNumber,ftbRefNo2,
		
		fgroupingProductInformation,
		fprodComposite,
		fgroupingServiceInformation,
		fibServiceSrNo,ftbServiceEngg,ftbBranch,ftbSrviceDate,
		fibServiceDay,fibServiceTime,fibServiceBranch,ftbServiceLocation,ftbServiceLocationArea,folbservicetype,
		ftbStatus,fdbServiceCompletionDate,fibServiceDuration,fibTotalServiceDuration,ftbservicecompletionremark,
		ftbCustomerFeedback,folbTeam,fpremisesDetails,fbtstackDetails,
		ftbBillingCount,
		ftaReasonForChange,
		fremark,ftbServiceWeekNo,fquantity,fUOM,fWarrantyDate,
		
		fdlbPerUnitPrice , fserviceValue,ftechnicianRemark,
		fgroupingUploadInformation,
		fupTestReport,fupDevice,
		fgroupingDeviceInformation,
		modif,
		faddressComp,
		gtrapCatches,
		array,
		ftable,
		ftacomment,
		fgrpingPhotos,
		fgrpingBefore,
		fucServiceImage1,fucServiceImage2,
		fgrpingAfter,
		fucServiceImage3,fucServiceImage4,
		fgroupingCustomerSignatureInfo,fgroupingReferenceInfo, fgroupingAddress,fgroupingcustfeedback,
		fuploadCustomerSignature,fcustomerSignName,fcustomerSignNumber,fcustomerSignPersonDesignation,fcustomerSignPersonEmpcode,fcustomer,ftbCommodity , 
		ftbContainerNumber , ftbLocation ,fcatchAddButton, fpestName ,fpestCount,ftbProModelNo , ftbProSerialNo,fserviceSummaryReport2 ,
		fmaterialCost,flabourCost,fexpenses,ftotalCost,
		ftbServiceEnggCopy,ftbSrviceDateCopy,fdbServiceCompletionDateCopy,fgroupingMaterial,fchecklistStatus;
	    
	 	TextBox tbCommodity;
	 	TextBox tbContainerNumber;

 	/**
 	 * Date 04-05-2019 by Vijay for NBHC CCPM
 	 */
	ArrayList<StackDetails> stackdetails; 
	GenricServiceAsync genasync=GWT.create(GenricService.class);
	
	/**
	 * @author Anil
	 * @since 18-06-2020
	 * Changing widget type of service branch from textbox to listbox
	 */
	public ObjectListBox<CustomerBranchDetails>tbserviceBranch;
	
	TextBox tbServiceLocation,tbServiceLocationArea; //Ashwini Patil Date: 24-05-2022
	/**
	 * @author Anil
	 * @since 19-06-2020
	 * For ptspl
	 */
	UploadComposite serviceSummaryReport2;
	
	/**
	 * @author Anil
	 * @since 26-06-2020
	 * FOR UMAS- storing check list separately earlier finding and checklist were getting stored in same field
	 */
	ObjectListBox<Config> oblCheckList;
	TextBox tbChkRemark ;
	Button addBtnChkList ;
	CatchTrapsTable checkListTbl;
	
	FormField foblCheckList,ftbChkRemark,faddBtnChkList,fcheckListTbl,fgroupingChecklist;
	
	/**
	 * @author Anil
	 * @since 03-07-2020
	 * Adding pest treatment details
	 * for UAMD-raised by Nitin Sir
	 */
	
	ObjectListBox<Config> oblPestList;
	TextBox tbTreatmentArea ;
	TextBox tbMethodOfApp ;
	Button addBtnPestTreatment ;
	PestTreatmentDetailTable pestTreatmentTbl;
	
	FormField foblPestList,ftbTreatmentArea,ftbMethodOfApp,faddBtnPestTreatment,fpestTreatmentTbl,fgroupingPestTreatment,ftrackServiceCompletionTable;
	
	/**
	 * @author Anil @since 19-10-2021
	 * Adding fields to capture before and after service image
	 * earlier we were storing only 4, now as per the new requirement we neen to store 5 more
	 * Raised by Rahul Tiwari and Nitin Sir
	 */
	UploadComposite ucServiceImage5;
	UploadComposite ucServiceImage6;
	UploadComposite ucServiceImage7;
	UploadComposite ucServiceImage8;
	UploadComposite ucServiceImage9;
	UploadComposite ucServiceImage10;
	
	FormField fucServiceImage5,fucServiceImage6,fucServiceImage7,fucServiceImage8,fucServiceImage9,fucServiceImage10;
	TrackServiceCompletionTable trackServiceCompletionTable;//Ashwini Patil Date:3-11-2022
	DoubleBox materialCost,labourCost,expenses,totalCost;//Ashwini Patil Date:4-11-2022
	NumberFormat df=NumberFormat.getFormat("0.00");
	
	MaterialProductGroupTable materialProdTable;//Ashwini Patil Date:15-02-2023
	
	CheckBox chkcheckliststatus;
	
	boolean isdocketUploadChanged;
	String docketurl;
	
	public DeviceForm()
	{
		super();
		createGui();
		poc.setEnable(false);
		
		if(flag==true)
			tbContractId.setEnabled(false);
		else
			tbContractId.setEnabled(true);
		startDate.setEnabled(false);
		endDate.setEnabled(false);
		
		poc.getCustomerId().getHeaderLabel().setText("Customer ID");
		poc.getCustomerName().getHeaderLabel().setText("Customer Name");
		poc.getCustomerCell().getHeaderLabel().setText("Customer Cell");
		poc.setValidate(false);
		
		
		tbStatus.setEnabled(false);
		taReasonForChange.setEnabled(false);
//		tbCustomerFeedback.setEnabled(false);//Ashwini Patil
		//this.taReasonForChange.setEnabled(false);
		/** date 20.02.2018 added by komal  for area wise billing **/
		dlbPerUnitPrice.addChangeHandler(this);
		quantity.addChangeHandler(this);
		isdocketUploadChanged=false;
		docketurl="";
		
	}
	
	public DeviceForm  (String[] processlevel, FormField[][] fields,
			FormStyle formstyle) 
	{
		super(processlevel, fields, formstyle);
		createGui();
		poc.setEnable(false);
		if(flag==true)
			tbContractId.setEnabled(false);
		else
			tbContractId.setEnabled(true);
		
		
		startDate.setEnabled(false);
		endDate.setEnabled(false);
//		tbCustomerFeedback.setEnabled(false);//Ashwini Patil
		
		poc.getCustomerId().getHeaderLabel().setText("Customer ID");
		poc.getCustomerName().getHeaderLabel().setText("Customer Name");
		poc.getCustomerName().getHeaderLabel().setText("Customer Cell");
		
//		stackDetailsTable.connectToLocal();
//		addStack.addClickHandler(this);
	}
	
	private void initalizeWidget()
	{
		
		tbContractId=new TextBox();
		poc=AppUtility.customerInfoComposite(new Customer());
		tbServiceEngg=new ObjectListBox<Employee>();
		tbServiceEnggCopy=new ObjectListBox<Employee>();
		tbServiceEnggCopy.setEnabled(false);
//		AppUtility.makeSalesPersonListBoxLive(tbServiceEngg);
		tbServiceEngg.makeEmployeeLive(AppConstants.SERVICEMODULE, AppConstants.CUSTOMERSERVICE, "Service Engineer");
		tbServiceEnggCopy.makeEmployeeLive(AppConstants.SERVICEMODULE, AppConstants.CUSTOMERSERVICE, "Service Engineer");
		
		tbBranch=new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(tbBranch);
		tbServiceDate=new DateBoxWithYearSelector();;
		tbServiceDateCopy=new DateBoxWithYearSelector();
		tbWarrantyDate=new TextBox();
		tbWarrantyDate.setTitle("When you click on Print Service Certificate, system asks you warranty date. That selected date gets stored in this field");
		tbWarrantyDate.setEnabled(false);
		tbStatus=new TextBox();
		tbStatus.setText(Service.SERVICESTATUSSCHEDULE);
//		dbServiceCompletionDate=new DateBox();
		dbServiceCompletionDate = new DateBoxWithYearSelector();
		dbServiceCompletionDate.setEnabled(false);
		dbServiceCompletionDateCopy = new DateBoxWithYearSelector();
		dbServiceCompletionDateCopy.setEnabled(false);
	    taReasonForChange=new TextArea();
		upTestReport=new UploadComposite();
		upDevice=new UploadComposite();
//		addr=new AddressComposite();
		startDate=new DateBoxWithYearSelector();;
		endDate=new DateBoxWithYearSelector();;
		
		table=new ModificationHistoryTable();
		table.connectToLocal();
		//clientsideassettable=new ClientSideAssetTable();
		//clientsideassettable.connectToLocal();
		//tooltable=new ToolTable();
		//tooltable.connectToLocal();
		olbservicetype=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbservicetype, Screen.SERVICETYPE);
		ibServiceDuration=new IntegerBox();
		ibTotalServiceDuration=new IntegerBox();
		ibServiceDuration.setEnabled(false);
		ibTotalServiceDuration.setEnabled(false);
		ibServiceId=new TextBox();
		ibServiceId.setEnabled(false);
		ibServiceSrNo=new IntegerBox();
		ibServiceSrNo.setEnabled(false);
		prodComposite=AppUtility.initiateServiceProductComposite(new SuperProduct());
		prodComposite.setEnabled(false);
		tbservicecompletionremark=new TextBox();
//		tbservicecompletionremark.setEnabled(false);//Ashwini Patil
		tbServiceday=new TextBox();
		tbServicetime=new TextBox();
//		tbserviceBranch=new TextBox();
		tbserviceBranch=new ObjectListBox<CustomerBranchDetails>();
		tbServiceLocation=new TextBox();
		tbServiceLocationArea=new TextBox();
		tbserviceBranch.addItem("--SELECT--");
		tbServiceday.setEnabled(false);
		tbServicetime.setEnabled(false);
		tbserviceBranch.setEnabled(false);
		
		tbTicketNumber=new TextBox();
		tbTicketNumber.setEnabled(false);
		
		tbCustomerFeedback=new TextBox();
//		tbCustomerFeedback.setEnabled(false);//Ashwini Patil
		
		dbServicingTime=new DoubleBox();
		
		addressComp=new AddressComposite();
		addressComp.setEnable(false);
		olbTeam = new ObjectListBox<TeamManagement>();
		olbTeam.setEnabled(false);
		this.initalizeTeamListBox();
		
		
		
		refNo = new TextBox();
		refNo.setEnabled(false);
		
		// 4 Fields for NBHC
//				tbWareHouse = new TextBox();
//				tbStorageLocation = new TextBox();
//				tbStorageBin = new TextBox();
//				tbDescription = new TextBox();
//				addStack = new Button("ADD");
//				stackDetailsTable = new StackDetailsTable();
		
		pestName = new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(pestName, Screen.CATCHTRAPPESTNAME);
		
		
		pestCount = new IntegerBox();
		catchAddButton =new Button("Add");
		catchTrap = new CatchTrapsTable();
		
			
		premisesDetails = new TextBox();
		remark = new TextArea();
		/**
		 * Date 2-7-2018 by jayshree
		 */
		technicianRemark= new TextArea();
		
		
		tbRefNo2=new TextBox();
		tbRefNo2.setEnabled(false);
		
		btstackDetails=new Button("Stack Details");
		this.btstackDetails.setEnabled(true);
		
		ibServiceWeekNo = new IntegerBox();
		ibServiceWeekNo.setEnabled(false);
		ibServiceWeekNo.getElement().getStyle().setMarginLeft(8, Unit.PX);
		
		quantity=new DoubleBox();
		quantity.setEnabled(true);
		
		olbUOM=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbUOM, Screen.SERVICEUOM);
		
		/** 29-09-2017 sagar sore[ for service value box]**/
		serviceValue=new DoubleBox();
		
		tacomment=new TextArea();
		uploadCustomerSignature=new UploadComposite();
		customerSignName=new TextBox();
		customerSignNumber=new TextBox();
		/** date 16.02.2018 added by komal for area wise billing **/
		dlbPerUnitPrice = new DoubleBox();
		
		/***30-12-2019 Deepak Salve added this code for Designation & Emp code ***/
		customerSignPersonDesignation=new TextBox();
		customerSignPersonEmpcode=new TextBox();
		/***End***/
		
		
		/**
		 * nidhi
		 * 9-08-2018
		 * for map model and serail no
		 */
		 tbProModelNo = new TextBox();
		  tbProSerialNo = new TextBox();
		tbProModelNo.setEnabled(false);
		tbProSerialNo.setEnabled(false);
		
		if(!LoginPresenter.mapModelSerialNoFlag)
		{
			tbProModelNo.setVisible(false);
			tbProSerialNo.setVisible(false);
		}
		/**
		 * end
		 */
		/**
		 * nidhi
		 * 15-10-2018
		 */
		tbBillingCount = new TextBox();
		
		ucServiceImage1=new UploadComposite();
		ucServiceImage2=new UploadComposite();
		ucServiceImage3=new UploadComposite();
		ucServiceImage4=new UploadComposite();
		/** date 26.3.2019 added by komal to add location / number in trap catches **/
		tbLocation = new TextBox();
		tbCommodity = new  TextBox();
		tbContainerNumber =  new  TextBox();
		
		serviceSummaryReport2=new UploadComposite();
		
		
		oblCheckList = new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(oblCheckList, Screen.SERVICECHECKLIST);
		tbChkRemark=new TextBox();
		addBtnChkList=new Button("Add");
		addBtnChkList.addClickHandler(this);
		checkListTbl=new CatchTrapsTable(true);
		
		oblPestList = new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(oblPestList, Screen.CATCHTRAPPESTNAME);
		tbTreatmentArea=new TextBox();
		tbMethodOfApp=new TextBox();
		addBtnPestTreatment=new Button("Add");
		addBtnPestTreatment.addClickHandler(this);
		pestTreatmentTbl=new PestTreatmentDetailTable();
		
		ucServiceImage5=new UploadComposite();
		ucServiceImage6=new UploadComposite();
		ucServiceImage7=new UploadComposite();
		ucServiceImage8=new UploadComposite();
		ucServiceImage9=new UploadComposite();
		ucServiceImage10=new UploadComposite();
		team=new TextArea();//Added by sheetal
		
		trackServiceCompletionTable=new TrackServiceCompletionTable();
		materialCost=new DoubleBox();
		labourCost=new DoubleBox();
		expenses=new DoubleBox();
		totalCost=new DoubleBox();
		
		materialProdTable=new MaterialProductGroupTable();
		
		chkcheckliststatus = new CheckBox();
		chkcheckliststatus.setValue(false);
	}

	private void initalizeTeamListBox() {
		MyQuerry querry = new MyQuerry();
		querry.setQuerryObject(new TeamManagement());
		olbTeam.MakeLive(querry);
	}
	
	@Override
	public void createScreen() {


		initalizeWidget();

		//Token to initialize the processlevel menus.   
		//  for NBHC add "MIS" button
		/**
		 * Date : 31-10-2017 BY ANIL
		 * Added view bill button for rate contract and service wise billing
		 */
		
		
		/**
		 * @author  abhinav bihade
		 * @since 24/01/2020
		 * NBHC-Process button -'Commodity Fumigation Report' and 'Service Revanue Report'on customer service should  
		 * only visible if through process configuration is true as it is NBHC specific reports
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "OnlyForNBHC"))
		{
			this.processlevelBarNames=new String[]{"Manage Customer Project","Reschedule","Cancel","Mark Completed","Reminder SMS","New","Print Service Certificate","Service Details","Periodic Servic record","WPM Fumigation","Commodity Fumigation","Reverse Service Completion","MIS",AppConstants.VIEWBILL/*nidhi 18-11-2017 for display fumigation report*/,AppConstants.SERVICEFUMIGATIONREPORT, AppConstants.SERVICEFUMIGATIONVALUEREPORT/**
			nidhi
			18-11-2017
			for contract revanue details report
			**/ ,AppConstants.VIEWORDER, "Service Record","View Complain",AppConstants.SERVICEVOUCHER,"Register Expense"};
		}
		else
		{/**
			 * Date : 27/3/2018 BY Manisha
			 * Added PrintIntimationLetter button for Pcamb..!!
			 */
			
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","PCAMBCustomization"))
			{
			this.processlevelBarNames=new String[]{"Manage Customer Project","Reschedule","Cancel","Mark Completed","printIntimationLetter","Reminder SMS","New","Print Service Certificate","Service Details","Reverse Service Completion",AppConstants.VIEWBILL,AppConstants.VIEWORDER,AppConstants.SERVICEVOUCHER,"Register Expense","View Complain"};
		    }
			/**
			 * Date 22-5-2018 By jayshree
			 * des.added PrintserviceVoucherbutton forIPM
			 */
			else if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","PrintServiceVoucher")){
				this.processlevelBarNames=new String[]{"Manage Customer Project","Reschedule","Cancel","Mark Completed","Reminder SMS","New","Print Service Certificate","Service Details","Reverse Service Completion",AppConstants.VIEWBILL,AppConstants.VIEWORDER,AppConstants.SUSPEND,AppConstants.SERVICEVOUCHER,"Register Expense","View Complain"};
			
			}else if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","ServiceStatusAsPlanned")){
				this.processlevelBarNames=new String[]{"Manage Customer Project","Reschedule","Cancel","Mark Completed","New","Print Service Certificate","Service Details",AppConstants.VIEWBILL,AppConstants.VIEWORDER,AppConstants.SERVICEVOUCHER,"Register Expense","View Complain"};
			}else{
				this.processlevelBarNames=new String[]{"Manage Customer Project","Reschedule","Cancel","Mark Completed","Reminder SMS","New","Print Service Certificate","Service Details","Reverse Service Completion",AppConstants.VIEWBILL,AppConstants.VIEWORDER,AppConstants.SUSPEND,AppConstants.SERVICEVOUCHER,"View Complain","Register Expense","View Expense"};
			}
			/**End**/
		}

//      ,"Service Record","Print Fumigation","Update Entity",

		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////
		//Token to initialize formfield
		String mainScreenLabel="CUSTOMER SERVICE";
		if(serviceObj!=null&&serviceObj.getCount()!=0){
			mainScreenLabel=serviceObj.getCount()+"/"+serviceObj.getStatus()+"/"+AppUtility.parseDate(serviceObj.getCreationDate());
		}
		
			//fgroupingCustomerInformation.getHeaderLabel().setText(mainScreenLabel);
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		fgroupingCustomerInformation=fbuilder.setlabel(mainScreenLabel).widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).setKeyField(true).build(); 
	
		fbuilder = new FormFieldBuilder("* Contract Id",tbContractId);
		ftbContractId= fbuilder.setMandatory(true).setRowSpan(0).setColSpan(0).setMandatoryMsg("Contract ID is Mandatory !").
				build();
		
		fbuilder = new FormFieldBuilder("",poc);
		fpic= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
//		fbuilder = new FormFieldBuilder("",addr);
//		FormField faddr= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fgroupingServiceInformation=fbuilder.setlabel("Service Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		/** date 17.4.2018 added by komal for orion**/
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "makeServiceEngineerMandatory")){
			fbuilder = new FormFieldBuilder("* Service Engineer",tbServiceEngg);
			ftbServiceEngg= fbuilder.setMandatory(true).setMandatoryMsg("Service Engineer is Mandatory").setRowSpan(0).setColSpan(0).build();

		}else{
//			fbuilder = new FormFieldBuilder("Service Engineer",tbServiceEngg);
			/**@Sheetal:17-03-2022 
			 *  Des : Renaming Service Engineer to Technician, requirement by nitin sir**/
			fbuilder = new FormFieldBuilder("Technician",tbServiceEngg);
			ftbServiceEngg= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		}
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "makeServiceEngineerMandatory")){
			fbuilder = new FormFieldBuilder("* Service Engineer",tbServiceEnggCopy);
			ftbServiceEnggCopy= fbuilder.setMandatory(false).setMandatoryMsg("Service Engineer is Mandatory").setRowSpan(0).setColSpan(0).build();

		}else{
//			fbuilder = new FormFieldBuilder("Service Engineer",tbServiceEngg);
			/**@Sheetal:17-03-2022 
			 *  Des : Renaming Service Engineer to Technician, requirement by nitin sir**/
			fbuilder = new FormFieldBuilder("Technician",tbServiceEnggCopy);
			ftbServiceEnggCopy= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		}
				
		fbuilder = new FormFieldBuilder("Contract Start Date",startDate);
		ftbStartDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Contract End Date",endDate);
		ftbEndDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Branch",tbBranch);
		ftbBranch= fbuilder.setMandatory(true).setRowSpan(0).setColSpan(0).setMandatoryMsg("Branch is Mandatory").build();
		fbuilder = new FormFieldBuilder("* Service Type",olbservicetype);
		folbservicetype= fbuilder.setMandatory(true).setRowSpan(0).setColSpan(0).setMandatoryMsg("Service Type is Mandatory").build();
		fbuilder = new FormFieldBuilder("Service Date",tbServiceDate);
		ftbSrviceDate= fbuilder.setMandatory(true).setRowSpan(0).setColSpan(0).setMandatoryMsg("Service Date  is Mandatory").build();
		fbuilder = new FormFieldBuilder("Service Date",tbServiceDateCopy);
		ftbSrviceDateCopy= fbuilder.setMandatory(true).setRowSpan(0).setColSpan(0).setMandatoryMsg("Service Date  is Mandatory").build();
		
		fbuilder = new FormFieldBuilder("Status",tbStatus);
		ftbStatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Service Execution Duration",ibServiceDuration);//old label Service Completion Duration
		fibServiceDuration= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Total Service Duration",ibTotalServiceDuration);//old label Service Completion Duration
		fibTotalServiceDuration= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Customer Feedback",tbservicecompletionremark);//old label "Service Completion Remark"
		ftbservicecompletionremark= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",prodComposite);
		fprodComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Reason For Cancellation",taReasonForChange);
		ftaReasonForChange= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Team",team);//Added by sheetal
		FormField ftaTeam = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("EVA Pedio", "PC_DocumentUploadMandatory")){
			
			fbuilder = new FormFieldBuilder("* Service Report 1",upTestReport);
			fupTestReport= fbuilder.setMandatory(true).setMandatoryMsg("Service Report upload is mandatory! Click on Customer Report tab and upload the report.").setRowSpan(0).setColSpan(2).build();		
		}else {
			fbuilder = new FormFieldBuilder("Service Report 1",upTestReport);
			fupTestReport= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		}
		
		
		
		fbuilder = new FormFieldBuilder("Customer Feedback",upDevice);
		fupDevice= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		fbuilder = new FormFieldBuilder("Service ID",ibServiceId);
		fserviceid= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).setMandatoryMsg("Service ID is Mandatory !").build();
		
		fbuilder = new FormFieldBuilder();
		fgroupingcustfeedback=fbuilder.setlabel("Customer Feedback").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder();
		fgroupingUploadInformation=fbuilder.setlabel("Customer Service Report").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
  		fgroupingDeviceInformation=fbuilder.setlabel("Service Reshedule History").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
 
  		fbuilder = new FormFieldBuilder("Modification History Table",table.getTable());
		modif= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Service Sr No",ibServiceSrNo);
		fibServiceSrNo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Service Completion Date",dbServiceCompletionDate);
		fdbServiceCompletionDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Service Completion Date",dbServiceCompletionDateCopy);
		fdbServiceCompletionDateCopy= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder();
		fgroupingProductInformation=fbuilder.setlabel("Product Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Service Day",tbServiceday);
		fibServiceDay= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Service Time",tbServicetime);
		fibServiceTime= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Service Branch",tbserviceBranch);
		fibServiceBranch= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		//Ashwini Patil Date:24-05-2022  adding service Location and area
		fbuilder = new FormFieldBuilder("Service Location",tbServiceLocation);
		ftbServiceLocation= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Area",tbServiceLocationArea);
		ftbServiceLocationArea= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/******************** ANIL 10Sept2015 *******************************/
		fbuilder = new FormFieldBuilder("Ticket Number",tbTicketNumber);
		ftbTicketNumber= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/******************** ANIL 10Sept2015 *******************************/
		
		//fbuilder = new FormFieldBuilder("",device);
		//FormField fdevice= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
	/*	fbuilder = new FormFieldBuilder("",clientsideassettable.getTable());
		FormField fclientsideassettable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		FormField fgroupingToolInformation=fbuilder.setlabel("Tool Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",tooltable.getTable());
		FormField ftooltable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();

		//fbuilder = new FormFieldBuilder("",btnAdd);
		//FormField fbtnAdd= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

*/
		
		fbuilder = new FormFieldBuilder("Customer Rating",tbCustomerFeedback);//old label "Customer Rating"
		ftbCustomerFeedback= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "OnlyForNBHC")){
			fbuilder = new FormFieldBuilder("Servicing Time In Hours",dbServicingTime);
			 fdbServicingTime= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).setMandatoryMsg("Servicing Time is Mandatory !").build();
		}else{
			 fbuilder = new FormFieldBuilder("Servicing Time In Hours",dbServicingTime);
			 fdbServicingTime= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).setMandatoryMsg("Servicing Time is Mandatory !").build();
		}
		
		
		fbuilder = new FormFieldBuilder("Team",olbTeam);
		folbTeam= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",addressComp);
		faddressComp= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Reference Number 1",refNo);
		ftbReferenceNumber= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	
		
		
		//   rohan added this for trap catches 
		/**
		 * @author Anil ,Date : 16-03-2019
		 * Renaming label from 'Trap Catches Information' to 'Service Finding'
		 * Renaming label from 'Pest Name' to 'Name'
		 * Renaming label from 'Pest Count' to 'Count'
		 * Raised by Nitin Sir,for NBHC 
		 * changed by komal 'Name' to 'Type'(26.3.2019)
		 */
		
		/** Added by sheetal:20-12-2021
		   Renaming label from 'Service Finding' to 'Finding Type'**/
		fbuilder = new FormFieldBuilder();
		gtrapCatches=fbuilder.setlabel("Finding Type").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Type",pestName);
		fpestName= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Count",pestCount);
		fpestCount= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder(" ",catchAddButton);
		fcatchAddButton= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder(" ",catchTrap.getTable());
		ftable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		/**
		 * End
		 */
		fbuilder = new FormFieldBuilder("Premises Details",premisesDetails);
		fpremisesDetails= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Remark",remark);
		fremark= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		// Three Fields for NBHC by RAHUl
//		fbuilder = new FormFieldBuilder();
//		FormField gtrapCatches = fbuilder.setlabel("Stack ")
//				.widgetType(FieldType.Grouping).setMandatory(false)
//				.setColSpan(4).build();
		
//		fbuilder = new FormFieldBuilder("Warehouse", tbWareHouse);
//		FormField ftbWareHouse = fbuilder.setMandatory(false).setRowSpan(0)
//				.setColSpan(0).build();
//
//		fbuilder = new FormFieldBuilder("Storage Location", tbStorageLocation);
//		FormField ftbStorageLocation = fbuilder.setMandatory(false)
//				.setRowSpan(0).setColSpan(0).build();
//
//		fbuilder = new FormFieldBuilder("Stack No", tbStorageBin);
//		FormField ftbStorageBin = fbuilder.setMandatory(false).setRowSpan(0)
//				.setColSpan(0).build();
//
//		fbuilder = new FormFieldBuilder("Description", tbDescription);
//		FormField ftbDescription = fbuilder.setMandatory(false).setRowSpan(0)
//				.setColSpan(0).build();
//
//		fbuilder = new FormFieldBuilder("", addStack);
//		FormField faddStack = fbuilder.setMandatory(false).setRowSpan(0)
//				.setColSpan(0).build();
//
//		fbuilder = new FormFieldBuilder(" ", stackDetailsTable.getTable());
//		FormField fstackDetailsTable = fbuilder.setMandatory(false)
//				.setRowSpan(0).setColSpan(4).build();
		
		
		fbuilder = new FormFieldBuilder("Reference Number 2",tbRefNo2);
		ftbRefNo2= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/**********************Rahul 09-10-2016****************/
//		fbtstackDetails = null;
//		FormField folbServiceEngg = null;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","OnlyForNBHC"))
		{
			fbuilder = new FormFieldBuilder("",btstackDetails);
			fbtstackDetails= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			
//			fbuilder = new FormFieldBuilder("* Service Engineer",tbServiceEngg);
//			folbServiceEngg = fbuilder.setMandatory(true).setRowSpan(0).setColSpan(0).build();
		}
		
		StringBuilder sb = new StringBuilder();
		sb.append(" ");
		sb.append(" ");
		sb.append(" ");
		sb.append(" ");
		sb.append(" ");
		sb.append("Week No");
		String weekNo = sb.toString();
		
		fbuilder = new FormFieldBuilder(weekNo,ibServiceWeekNo);
		ftbServiceWeekNo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/**
		 * Date : 24-10-2017 BY ANIL
		 * Made Quantity and UOM mandatory for NBHC
		 * with process Type MakeQty&UomMandatory
		 * 
		 */
	
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "MakeQty&UomMandatory")){
			fbuilder = new FormFieldBuilder("* Quantity",quantity);
			fquantity= fbuilder.setMandatory(true).setMandatoryMsg("Quantity is mandatory!").setRowSpan(0).setColSpan(0).build();
			
			fbuilder = new FormFieldBuilder("* UOM",olbUOM);
			fUOM= fbuilder.setMandatory(true).setMandatoryMsg("UOM is mandatory!").setRowSpan(0).setColSpan(0).build();
		}else{
			fbuilder = new FormFieldBuilder("Quantity",quantity);
			fquantity= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			
			fbuilder = new FormFieldBuilder("UOM",olbUOM);
			fUOM= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
		fbuilder = new FormFieldBuilder("Warranty Date",tbWarrantyDate);
		fWarrantyDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	
		/**
		 * Date 2-7-2018 
		 * by jayshree
		 */
	
		fbuilder = new FormFieldBuilder("Technician Remark",technicianRemark);
		ftechnicianRemark= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/**
		 * End
		 */
		
		/**29-09-2017 sagar sore[adding service value field]**/
		fbuilder = new FormFieldBuilder("Service value",serviceValue);
		fserviceValue= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Description",tacomment);
		ftacomment= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder();
		fgroupingCustomerSignatureInfo=fbuilder.setlabel("Completion information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();//old label "Customer Signature Details"
		
		fbuilder = new FormFieldBuilder("Signed By",customerSignName);
		fcustomerSignName= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/**
		 * @author Anil,Date : 16-03-2019
		 * Changed colspan from 0 to 2
		 */
		fbuilder = new FormFieldBuilder("Customer Signature",uploadCustomerSignature);
		fuploadCustomerSignature= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("Signed Person Number",customerSignNumber);
		fcustomerSignNumber= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/** date 16.02.2018 added by komal for area wise billing **/ 
		fbuilder = new FormFieldBuilder("Per Unit Price",dlbPerUnitPrice);
		fdlbPerUnitPrice= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		

		fbuilder = new FormFieldBuilder("Sign Person Designation",customerSignPersonDesignation);
		fcustomerSignPersonDesignation= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Sign Person Emp Code",customerSignPersonEmpcode);
		fcustomerSignPersonEmpcode= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		
		/**
		 * nidhi
		 * 9-08-2018
		 * for 
		 */
		fbuilder = new FormFieldBuilder("Product Model No",tbProModelNo);
		ftbProModelNo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Product Serial No",tbProSerialNo);
		ftbProSerialNo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Image 1",ucServiceImage1);
		fucServiceImage1= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("Image 2",ucServiceImage2);
		fucServiceImage2= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("Image 6",ucServiceImage3);
		fucServiceImage3= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("Image 7",ucServiceImage4);
		fucServiceImage4= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		
		fbuilder = new FormFieldBuilder("Image 3",ucServiceImage5);
		fucServiceImage5= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("Image 4",ucServiceImage6);
		fucServiceImage6= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("Image 5",ucServiceImage7);
		fucServiceImage7= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("Image 8",ucServiceImage8);
		fucServiceImage8= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("Image 9",ucServiceImage9);
		fucServiceImage9= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("Image 10",ucServiceImage10);
		fucServiceImage10= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder();
		fgrpingPhotos=fbuilder.setlabel("Site Photos").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder();
		fgrpingBefore=fbuilder.setlabel("Before").setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder();
		fgrpingAfter=fbuilder.setlabel("After").setMandatory(false).setColSpan(4).build();
		
		/** date 26.3.2019 added by komal to add location / number in trap catches **/
		fbuilder = new FormFieldBuilder("Location/No",tbLocation);
		ftbLocation= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/** date 29.3.2019 added by komal to add commodity in trap catches **/
		fbuilder = new FormFieldBuilder("Stack No./Commodity",tbCommodity);
		ftbCommodity= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/** date 26.3.2019 added by komal to container number in trap catches **/
		fbuilder = new FormFieldBuilder("No. Of Bags/ Container No.",tbContainerNumber);
		ftbContainerNumber= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////
		fbuilder = new FormFieldBuilder("Billing Id",tbBillingCount);
		ftbBillingCount= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Service Report 2",serviceSummaryReport2);
		fserviceSummaryReport2= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		
		fbuilder = new FormFieldBuilder();
		fgroupingChecklist=fbuilder.setlabel("Service Checklist").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Name",oblCheckList);
		foblCheckList= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Remark",tbChkRemark);
		ftbChkRemark= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder(" ",addBtnChkList);
		faddBtnChkList= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder(" ",checkListTbl.getTable());
		fcheckListTbl= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		//fgroupingAddress
		fbuilder = new FormFieldBuilder();
		fgroupingAddress=fbuilder.setlabel("Service Address").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder();
		fgroupingPestTreatment=fbuilder.setlabel("Technical Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		//Ashwini Patil Date:05-05-2022 changing label from Treatment Area to Applicable Area
//		fbuilder = new FormFieldBuilder("Treatment Area",tbTreatmentArea);
		fbuilder = new FormFieldBuilder("Applicable Area",tbTreatmentArea);
		ftbTreatmentArea= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Method of application",tbMethodOfApp);
		ftbMethodOfApp= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		//Ashwini Patil Date:05-05-2022 changing label from Pest Target to Target
		fbuilder = new FormFieldBuilder("Target",oblPestList);
		foblPestList= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder(" ",addBtnPestTreatment);
		faddBtnPestTreatment= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder(" ",pestTreatmentTbl.getTable());
		fpestTreatmentTbl= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fgroupingReferenceInfo=fbuilder.setlabel("Reference/General").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder(" ",trackServiceCompletionTable.getTable());
		ftrackServiceCompletionTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Material Cost",materialCost);
		fmaterialCost= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Labour Cost",labourCost);
		flabourCost= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Expenses",expenses);
		fexpenses= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Total Cost",totalCost);
		ftotalCost= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder();
		fgroupingMaterial=fbuilder.setlabel("Material").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("", materialProdTable.getTable());
	    FormField fmaterialProdTable = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
       
	    
	    fbuilder = new FormFieldBuilder("Status",chkcheckliststatus);
	    fchecklistStatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		FormField array[] = new FormField[4];
		
			array[0] =fpestName;
			array[1] = fpestCount;
			array[2] = ftbLocation ;
			array[3] = fcatchAddButton;
		
			
		//Orion does not want to show service value to users other than admin
		FormField array2[];
		Console.log("config="+AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","HideServiceValueExceptAdmin"));
		Console.log("user role="+LoginPresenter.myUserEntity.getRole().getRoleName());
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","HideServiceValueExceptAdmin")&&!LoginPresenter.myUserEntity.getRole().getRoleName().equalsIgnoreCase("Admin")){
			Console.log("in condition1 ");
			array2 = new FormField[3];
			array2[0] =ftbStartDate;
			array2[1] = ftbEndDate;
			array2[2] = ftbBillingCount ;	
		}else{
			Console.log("in condition2 ");
			array2 = new FormField[4];
			array2[0] =ftbStartDate;
			array2[1] = ftbEndDate;
			array2[2] = ftbBillingCount ;
			array2[3] = fserviceValue;
		}
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","OnlyForNBHC"))
		{
				FormField[][] formfield = {  
				{fgroupingCustomerInformation},
				{fserviceid,ftbContractId,ftbStartDate,ftbEndDate},
				{fpic},
				{ftbTicketNumber,fdbServicingTime,ftbReferenceNumber,ftbRefNo2},
				//{faddr},
				{fgroupingProductInformation},
				{fprodComposite},
				{fgroupingServiceInformation},
				{fibServiceSrNo,ftbServiceEngg,ftbBranch,ftbSrviceDate},
				{fibServiceDay,fibServiceTime,fibServiceBranch,ftbServiceLocation},
				{ftbServiceLocationArea,folbservicetype,ftbStatus},
				{folbTeam},
				{fpremisesDetails,fbtstackDetails,ftbBillingCount},//nidhi 15-10-2018
////				{ ftbWareHouse, ftbStorageLocation, ftbStorageBin,ftbDescription, faddStack }, 
////				{ fstackDetailsTable },
				{ftaReasonForChange},
				{fremark,ftbServiceWeekNo,fquantity,fUOM},
				/** 29-09-2017 sagar sore [for service value]**/
				{fdlbPerUnitPrice , fserviceValue},//add by jayshree
				{fgroupingUploadInformation},
				{fupTestReport,fserviceSummaryReport2},
				{fupDevice},
				{fgroupingDeviceInformation},
				{modif},
				{faddressComp},
				{gtrapCatches},
				{fpestName ,fpestCount ,ftbLocation , fcatchAddButton},
				{ftable},
				{ftacomment},
				{fgrpingBefore},
				{fucServiceImage1,fucServiceImage2},
				{fucServiceImage5,fucServiceImage6},
				{fucServiceImage7},
				{fgrpingAfter},
				{fucServiceImage3,fucServiceImage4},
				{fucServiceImage8,fucServiceImage9},
				{fucServiceImage10},
						
				{fgroupingMaterial},
				{fmaterialProdTable},
				
				{fgroupingCustomerSignatureInfo},
				{fdbServiceCompletionDate,fibServiceDuration,fibTotalServiceDuration,ftbCustomerFeedback},
				{ftechnicianRemark,ftbservicecompletionremark},
				{ftrackServiceCompletionTable},
				{fcustomerSignName,fcustomerSignNumber,fcustomerSignPersonDesignation,fcustomerSignPersonEmpcode},
				{fuploadCustomerSignature},
				
				/**Service Checklist**/
				{fgroupingChecklist},
				{foblCheckList,ftbChkRemark,fchecklistStatus,faddBtnChkList},
				{fcheckListTbl},
				
				/**PestTreatment Details**/
				{fgroupingPestTreatment},
				{ftbTreatmentArea,ftbMethodOfApp,foblPestList,faddBtnPestTreatment},
				{fpestTreatmentTbl},

				
				
				//{fgroupingToolInformation},
				//{ftooltable}
				//{fdevice},
				//{fbtnAdd},
				//{fdevicetable}
				
		};

		this.fields=formfield;
		}
		else
		{
//			FormField[][] formfield = {   {fgroupingCustomerInformation},
//			{fserviceid,ftbContractId,ftbStartDate,ftbEndDate},
//			{fpic},
//			{ftbTicketNumber,fdbServicingTime,ftbReferenceNumber,ftbRefNo2},
//			//{faddr},
//			{fgroupingProductInformation},
//			{fprodComposite},
//			{fgroupingServiceInformation},
//			{fibServiceSrNo,ftbServiceEngg,ftbBranch,ftbSrviceDate},
//			{fibServiceDay,fibServiceTime,fibServiceBranch,folbservicetype},
//			{ftbStatus,fdbServiceCompletionDate,fibServiceDuration,ftbservicecompletionremark},
//			{ftbCustomerFeedback,folbTeam,fpremisesDetails},
//			{ftbProModelNo,ftbProSerialNo,ftbBillingCount},//nidhi 15-10-2018},
////			{ ftbWareHouse, ftbStorageLocation, ftbStorageBin,ftbDescription, faddStack }, 
////			{ fstackDetailsTable },
//			{ftaReasonForChange},
//			{fremark,ftbServiceWeekNo,fquantity,fUOM},
//			/** 29-09-2017 sagar sore [for service value]**/
//			{fdlbPerUnitPrice ,fserviceValue,ftechnicianRemark},//add by jayshree
//			{fgroupingUploadInformation},
//			{fupTestReport,fserviceSummaryReport2},
//			{fupDevice},
//			{fgroupingDeviceInformation},
//			{modif},
//			{faddressComp},
//			{gtrapCatches},
//			{fpestName,fpestCount,ftbLocation,fcatchAddButton},
//			{ftable},
//			{ftacomment},
//			
//			{fgroupingChecklist},
//			{foblCheckList,ftbChkRemark,faddBtnChkList},
//			{fcheckListTbl},
//			
//			{fgroupingPestTreatment},
//			{ftbTreatmentArea,ftbMethodOfApp,foblPestList,faddBtnPestTreatment},
//			{fpestTreatmentTbl},
//			
//			{fgrpingBefore},
//			{fucServiceImage1,fucServiceImage2},
//			{fgrpingAfter},
//			{fucServiceImage3,fucServiceImage4},
//			{fgroupingCustomerSignatureInfo},
//			{fuploadCustomerSignature,fcustomerSignName,fcustomerSignNumber}
//			//{fgroupingToolInformation},
//			//{ftooltable}
//			//{fdevice},
//			//{fbtnAdd},
//			//{fdevicetable}
			
			
			FormField[][] formfield = { 
					/**Main Screen**/
					{fgroupingCustomerInformation},
					{fpic},
					{ftbContractId,folbservicetype,ftbBranch},
					{fprodComposite},
					{ftbServiceEnggCopy,ftbSrviceDateCopy,fdbServiceCompletionDateCopy},										
					
					
					/**Service Information**/
					{fgroupingServiceInformation},
					{fibServiceSrNo,ftbSrviceDate,fibServiceDay,fibServiceTime},
					{fdbServicingTime,ftbServiceEngg,ftaTeam,folbTeam},
					{fibServiceBranch,ftbServiceLocation,ftbServiceLocationArea,fpremisesDetails},					
//					{ftbStartDate,ftbEndDate,ftbBillingCount,fserviceValue},
					array2,
					{fremark},
					{ftaReasonForChange},
					
					/**Completion Information**/
					{fgroupingCustomerSignatureInfo},
					{fdbServiceCompletionDate,fibServiceDuration,fibTotalServiceDuration,ftbCustomerFeedback},
					{flabourCost,fmaterialCost,fexpenses,ftotalCost},
					{ftbservicecompletionremark,ftechnicianRemark},
					{ftrackServiceCompletionTable},
					{fcustomerSignName,fcustomerSignNumber,fcustomerSignPersonDesignation,fcustomerSignPersonEmpcode},
					{fuploadCustomerSignature},
					
					
					/**Address**/
					{fgroupingAddress},
					{faddressComp},
					
					{fgroupingMaterial},
					{fmaterialProdTable},
					
					//Ashwini Patil Date:15-05-2023 changed position of Customer report tab as per orion requirement
					/**Attachment**/
					{fgroupingUploadInformation},
					{fupTestReport,fserviceSummaryReport2},
					
					/**Service Reshedule History**/
					{fgroupingDeviceInformation},
					{modif},
					
					
					/**Service Finding**/
					{gtrapCatches},
					{fpestName,fpestCount,ftbLocation,fcatchAddButton},
					{ftable},
					
					/**Service Checklist**/
					{fgroupingChecklist},
					{foblCheckList,ftbChkRemark,fchecklistStatus,faddBtnChkList},
					{fcheckListTbl},
					
					/**PestTreatment Details**/
					{fgroupingPestTreatment},
					{ftbTreatmentArea,ftbMethodOfApp,foblPestList,faddBtnPestTreatment},
					{fpestTreatmentTbl},
					
					/**Refernce and General**/
					{fgroupingReferenceInfo},
					{ftbTicketNumber,ftbReferenceNumber,ftbRefNo2,ftbServiceWeekNo},
					{fdlbPerUnitPrice,fquantity,fUOM,fWarrantyDate},
					{ftacomment},
					
					/**Images**/
					{fgrpingPhotos},
					{fgrpingBefore},
					{fucServiceImage1,fucServiceImage2},
					{fucServiceImage5,fucServiceImage6},
					{fucServiceImage7},
					{fgrpingAfter},
					{fucServiceImage3,fucServiceImage4},
					{fucServiceImage8,fucServiceImage9},
					{fucServiceImage10},
					
					
					/**Customer Feedback**/
					{fgroupingcustfeedback},
					{fupDevice}

			
	};

	this.fields=formfield;
		}
		/**
		 * Date 15-09-2018 By Vijay
		 * Des :- Bug :- getting null pionter exception so added timer to delay execute
		 */
		Timer timer = new Timer() {
			@Override
			public void run() {
				if(!LoginPresenter.mapModelSerialNoFlag){
					ftbProModelNo.getHeaderLabel().setVisible(false);
					ftbProSerialNo.getHeaderLabel().setVisible(false);
				}
			}
		};
		timer.schedule(2000);
	
	}

	@Override
	public void updateModel(Service model)
	{
		
		
		
	if(!tbTicketNumber.getValue().equals("")){
		model.setTicketNumber(Integer.parseInt(tbTicketNumber.getValue()));
	}
	
	if(addressComp.getValue()!=null){
		model.setAddress(addressComp.getValue());
	}
	if(olbTeam.getValue()!=null){
		model.setTeam(olbTeam.getValue());
	}
		
	if(dbServicingTime.getValue()!=null){
		model.setServicingTime(dbServicingTime.getValue());
	}
	if(!tbCustomerFeedback.getValue().equals("")){
		model.setCustomerFeedback(tbCustomerFeedback.getValue());
	}
		if(tbContractId.getValue()!=null)
			model.setContractCount(Integer.parseInt(tbContractId.getValue()));
		if(tbServiceEngg.getValue()!=null)
			model.setEmployee(tbServiceEngg.getValue());
		if(tbBranch.getValue()!=null)
			model.setBranch(tbBranch.getValue());
		if(tbServiceDate.getValue()!=null)
			model.setServiceDate(tbServiceDate.getValue());
		
		if(tbStatus.getValue()!=null)
			model.setStatus(tbStatus.getText());
		
			model.setUptestReport(upTestReport.getValue());
		if(upTestReport.getValue()!=null&&upTestReport.getValue().getUrl()!=null) {
			Console.log("upTestReport.getValue().getUrl() in updatemodel="+upTestReport.getValue().getUrl());
			if(!docketurl.equals(upTestReport.getValue().getUrl()))
				model.setDocketUploadedByApp(false);
				
			
		}
		
			model.setUpDeviceDocument(upDevice.getValue());
		if(poc.getValue()!=null)
			model.setPersonInfo(poc.getValue());
//		if(addr.getValue()!=null){
//			model.setAddress(addr.getValue());
//		}
		if(dbServiceCompletionDate.getValue()!=null){
			model.setServiceCompletionDate(dbServiceCompletionDate.getValue());
		}
		if(table.getValue()!=null)
			model.setListHistory(table.getValue());
		
		if(ibServiceSrNo.getValue()!=null){
			model.setServiceSerialNo(ibServiceSrNo.getValue());
		}
		
		/*
		 * commented by Ashwini
		 */
//		if(olbservicetype.getValue()!=null){
//			model.setServiceType(olbservicetype.getValue());
//		}
		
		/*
		 * Date:12-12-2018	
		 * @author Ashwini
		 */
		
		if(olbservicetype.getSelectedIndex()!=0){
			model.setServiceType(olbservicetype.getValue(olbservicetype.getSelectedIndex()));
			Console.log("Service type "+olbservicetype.getValue(olbservicetype.getSelectedIndex()));
		}else{
			model.setServiceType("");
		}
		
		
		if(tbServiceday.getValue()!=null){
			model.setServiceDay(tbServiceday.getValue());
		}
		if(tbServicetime.getValue()!=null){
			model.setServiceTime(tbServicetime.getValue());
		}
		if(tbserviceBranch.getValue()!=null){
			model.setServiceBranch(tbserviceBranch.getValue());
		}
		if(tbServiceLocation.getValue()!=null) {
			model.setServiceLocation(tbServiceLocation.getValue());
		}
		if(tbServiceLocationArea.getValue()!=null) {
			model.setServiceLocationArea(tbServiceLocationArea.getValue());
		}
		if(ibServiceDuration.getValue()!=null){
			model.setServiceCompleteDuration(ibServiceDuration.getValue());
		}
		if(tbservicecompletionremark.getValue()!=null){
			model.setServiceCompleteRemark(tbservicecompletionremark.getValue());//old f_markRemark
		}
		if(remark.getValue()!=null)
		{
			model.setRemark(remark.getValue());
		}
		
		
		/**
		 * Date 2-7-2018
		 * By jayshree
		 */
		
		if(technicianRemark.getValue()!=null)
		{
			model.setTechnicianRemark(technicianRemark.getValue());
		}
		
		if(premisesDetails.getValue()!=null)
		{
			model.setPremises(premisesDetails.getValue());
		}
		
		if(prodflag==true&&prodComposite.getValue()!=null){
			model.setProduct(serviceProdEntity);
			model.setContractStartDate(startDate.getValue());
			model.setContractEndDate(endDate.getValue());
			model.setAddress(addresInfo);
			model.setAdHocService(true);
//			createNewAdHocProject(model);
			prodflag=false;
		}
		
		
	System.out.println("TICKET BILLABLE FLAG +++ "+ticketBillableFlage);
		
		if(ticketBillableFlage==true&&prodComposite.getValue()!=null){
			System.out.println("INSIDE PaiD SERVICE UPDATE MODEL.............");
			model.setProduct(serviceProdEntity);
			model.setContractStartDate(startDate.getValue());
			model.setContractEndDate(endDate.getValue());
			model.setAddress(addresInfo);
		}
		
		if(refNo.getValue()!=null)
			model.setRefNo(refNo.getValue());
		
		
		
		if(catchTrap.getDataprovider().getList().size()!=0)
			model.setCatchtrapList(catchTrap.getDataprovider().getList());
		
		if(tbRefNo2.getValue()!=null){
			model.setRefNo2(tbRefNo2.getValue());
		}		
				
		if(ibServiceWeekNo.getValue()!=null){
			model.setServiceWeekNo(ibServiceWeekNo.getValue());
		}
		
		if(quantity.getValue()!=null){
			model.setQuantity(quantity.getValue());
		}
		if(olbUOM.getValue()!=null){
			model.setUom(olbUOM.getValue().trim());
		}
		
		/**29-09-2017 sagar sore [for service value]**/
		if(serviceValue.getValue()!=null)
		{
			model.setServiceValue(serviceValue.getValue());
		}
		
		if(uploadCustomerSignature.getValue()!=null){
			model.setCustomerSignature(uploadCustomerSignature.getValue());
		}
		
		if(customerSignName.getValue()!=null){
			model.setCustomerSignName(customerSignName.getValue());
		}
		
		/***30-12-2019 Deepak Salve addded this code for Designation and Emp code***/
		if(customerSignPersonDesignation.getValue()!=null){
			model.setCustomerPersonDesignation(customerSignPersonDesignation.getValue());
		}
		if(customerSignPersonEmpcode.getValue()!=null){
			model.setCustomerPersonEmpId(customerSignPersonEmpcode.getValue());
		}
		/***End***/
		
		if(customerSignNumber.getValue()!=null){
			model.setCustomerSignNumber(customerSignNumber.getValue());
		}
		
		if(tacomment.getValue()!=null){
			model.setDescription(tacomment.getValue());
		}
		
		/** Date 14-01-2018 By vijay for Adhoc services Setting number Range **/
		if(numberRange!=null){
			model.setNumberRange(numberRange);
		}
		
		/** date 16.02.2018 added by komal for area wise billing **/
		if(dlbPerUnitPrice.getValue()!=null){
			model.setPerUnitPrice(dlbPerUnitPrice.getValue());
		}
		/**
		 * nidhi
		 * 9-08-2018
		 * 
		 */
		model.setProModelNo(tbProModelNo.getValue());
		model.setProSerialNo(tbProSerialNo.getValue());
		/**
		 * end
		 */
		/**
		 * nidhi
		 * 15-10-2018
		 */
		if(tbBillingCount.getValue()!=null && tbBillingCount.getValue().trim().length()>0){
			model.setBillingCount(Integer.parseInt(tbBillingCount.getValue()));
		}
		
		if(ucServiceImage1.getValue()!=null){
			model.setServiceImage1(ucServiceImage1.getValue());
		}else{
			model.setServiceImage1(null);
		}
		if(ucServiceImage2.getValue()!=null){
			model.setServiceImage2(ucServiceImage2.getValue());
		}else{
			model.setServiceImage2(null);
		}
		if(ucServiceImage3.getValue()!=null){
			model.setServiceImage3(ucServiceImage3.getValue());
		}else{
			model.setServiceImage3(null);
		}
		if(ucServiceImage4.getValue()!=null){
			model.setServiceImage4(ucServiceImage4.getValue());
		}else{
			model.setServiceImage4(null);
		}
		model.setServiceSummaryReport2(serviceSummaryReport2.getValue());
		
		if(checkListTbl.getValue()!=null){
			model.setCheckList(checkListTbl.getValue());
		}
		
		if(pestTreatmentTbl.getValue()!=null){
			model.setPestTreatmentList(pestTreatmentTbl.getValue());
		}
		
		if(ucServiceImage5.getValue()!=null){
			model.setServiceImage5(ucServiceImage5.getValue());
		}else{
			model.setServiceImage5(null);
		}
		if(ucServiceImage6.getValue()!=null){
			model.setServiceImage6(ucServiceImage6.getValue());
		}else{
			model.setServiceImage6(null);
		}
		if(ucServiceImage7.getValue()!=null){
			model.setServiceImage7(ucServiceImage7.getValue());
		}else{
			model.setServiceImage7(null);
		}
		if(ucServiceImage8.getValue()!=null){
			model.setServiceImage8(ucServiceImage8.getValue());
		}else{
			model.setServiceImage8(null);
		}
		if(ucServiceImage9.getValue()!=null){
			model.setServiceImage9(ucServiceImage9.getValue());
		}else{
			model.setServiceImage9(null);
		}
		if(ucServiceImage10.getValue()!=null){
			model.setServiceImage10(ucServiceImage10.getValue());
		}else{
			model.setServiceImage10(null);
		}

		if(team.getValue()!=null){
			model.setTeam(team.getValue());
			Console.log("model team "+team.getValue());
			
		}
		if(isdocketUploadChanged) {
			model.setDocketUploadedByApp(false);
		}
		
		
		serviceObj=model;
		
		presenter.setModel(model);
		if(model.getProduct() != null && model.getProduct().getProductCode()!= null){
			if(model.getFumigationProductCodes().contains(model.getProduct().getProductCode())){
				setRearrangedArray();
				ftbLocation.getHeaderLabel().setText("Total Quantity(MT)/ Container Size(ft.)");
			//	setToEditState();
				catchTrap.flag = true;
				catchTrap.setEnable(true);
			}
			
		}
	}

	@Override
	public void updateView(Service view)
	{
	
		serviceObj=view;
		
		if(view.getProduct() != null && view.getProduct().getProductCode()!= null){
			if(view.getFumigationProductCodes().contains(view.getProduct().getProductCode())){
//				fpestName.getHeaderLabel().setText("Stack No./Commodity");
//				fpestCount.getHeaderLabel().setText("No. Of Bags/ Container No.");
			
				setRearrangedArray();
				ftbLocation.getHeaderLabel().setText("Total Quantity(MT)/ Container Size(ft.)");
				serviceObj=view;
			//	setToViewState();
				catchTrap.flag = true;
				catchTrap.setEnable(false);
			}
			
		}
		/**
		 * Date 20-04-2018 By vijay for orion pest locality load  
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Locality", "DontLoadAllLocalityAndCity")){
			setAddressDropDown();
		}
		/**
		 * ends here
		 */
		
		if(view.getTicketNumber()!=-1){
			tbTicketNumber.setValue(view.getTicketNumber()+"");
			}
		
		
		if(view.getCustomerFeedback()!=null){
			tbCustomerFeedback.setValue(view.getCustomerFeedback());
		}
		
		if(view.getServicingTime()!=null){
			dbServicingTime.setValue(view.getServicingTime());
		}
		
		ibServiceId.setValue(view.getCount()+"");
		ibServiceSrNo.setValue(view.getServiceSerialNo());
		if(view.getContractCount()!=null)
			tbContractId.setValue(view.getContractCount()+"");
		if(view.getEmployee()!=null){
			tbServiceEngg.setValue(view.getEmployee());
			tbServiceEnggCopy.setValue(view.getEmployee());
		}
		if(view.getBranch()!=null)
			tbBranch.setValue(view.getBranch());
		if(view.getPersonInfo()!=null)
			poc.setValue(view.getPersonInfo());
		if(view.getReasonForChange()!=null)
			taReasonForChange.setValue(view.getReasonForChange());
		if(view.getServiceDate()!=null) {
			tbServiceDate.setValue(view.getServiceDate());
			tbServiceDateCopy.setValue(view.getServiceDate());
		}
//		if(view.getAddress()!=null)
//			addr.setValue(view.getAddress());
		if(view.getUptestReport()!=null) {
			upTestReport.setValue(view.getUptestReport());
			docketurl=view.getUptestReport().getUrl();
			Console.log("docketurl from view="+docketurl);
		}
		if(view.getUpDeviceDocument()!=null)
			upDevice.setValue(view.getUpDeviceDocument());
		if(view.getStatus()!=null)
			tbStatus.setText(view.getStatus());
		if(view.getListHistory()!=null&&view.getListHistory().size()!=0)
			table.setValue(view.getListHistory());
		if(view.getContractStartDate()!=null)
			startDate.setValue(view.getContractStartDate());
		if(view.getContractEndDate()!=null)
			endDate.setValue(view.getContractEndDate());
		if(view.getServiceCompletionDate()!=null){
			dbServiceCompletionDate.setValue(view.getServiceCompletionDate());
			dbServiceCompletionDateCopy.setValue(view.getServiceCompletionDate());
		}
		if(view.getServiceCompleteDuration()!=0){
			ibServiceDuration.setValue(view.getServiceCompleteDuration());
		}
		if(view.getServiceCompleteRemark()!=null){
			tbservicecompletionremark.setValue(view.getServiceCompleteRemark());
		}
		if(view.getServiceType()!=null){
			olbservicetype.setValue(view.getServiceType());
			
			Console.log("Service type123"+view.getServiceType());
			
		}
		
		
		
//		if(view.getServiceDay()!=null)
//			tbServiceday.setValue(view.getServiceDay());
		/**
		 * @author Vijay Chougule
		 * Des :- As per Nitin Sir if service Day is not mapped then here i mapped at run time using service Date
		 * when service is reschedule then its service day is not updated so here updated .
		 */
		if(view.getServiceDay()!=null){
			String serviceDay = ContractForm.serviceDay(view.getServiceDate());
			if(!view.getServiceDay().equals(serviceDay)){
				tbServiceday.setValue(serviceDay);
			}
			else{
				tbServiceday.setValue(view.getServiceDay());
			}
		}
		else{
			String serviceDay = ContractForm.serviceDay(view.getServiceDate());
			tbServiceday.setValue(serviceDay);
		}
		/**
		 * ends here
		 */
		if(view.getServiceTime()!=null)
			tbServicetime.setValue(view.getServiceTime());
//		if(view.getServiceBranch()!=null)
//			tbserviceBranch.setValue(view.getServiceBranch());
		try {
		if(view.getServiceLocation()!=null) {
			Console.log("ServiceLocation not null");
			tbServiceLocation.setValue(view.getServiceLocation());
		}
		if(view.getServiceLocationArea()!=null) {
			Console.log("ServiceLocation area not null");
			tbServiceLocationArea.setValue(view.getServiceLocationArea());
		}
		}catch(Exception e){
			Console.log(e.getMessage());			
		}
		if(view.getServiceBranch()!=null){
			tbserviceBranch.clear();
			tbserviceBranch.addItem("--SELECT--");
			CustomerBranchDetails custBranch=new CustomerBranchDetails();
			custBranch.setBusinessUnitName(view.getServiceBranch());
			tbserviceBranch.getItems().add(custBranch);
			tbserviceBranch.addItem(view.getServiceBranch());
			tbserviceBranch.setValue(view.getServiceBranch());
		}
		
		if(view.getServiceCompleteRemark()!=null)
			f_markRemark=view.getServiceCompleteRemark();
		
		if(view.getServiceCompleteDuration()!=0)
			f_markduration=view.getServiceCompleteDuration();
		
		
//		if(!view.getRemark().equals(""))
//			remark.setValue(view.getRemark());
		
		/**
		 * Date : 11-02-2017 by ANil
		 * checked remark not equal null
		 */
		if(view.getRemark()!=null&&!view.getRemark().equals("")){
			remark.setValue(view.getRemark());
		}
		/**
		 * End
		 */
		
		
		/**
		 * Date 2-7-2018 by jayshree
		 * 
		 */
		
		if(view.getTechnicianRemark()!=null&&!view.getTechnicianRemark().equals("")){
			technicianRemark.setValue(view.getTechnicianRemark());
		}
		
		if(premisesDetails.getValue()!=null)
		{
			/**
			 * Date 19-5-2018 by jayshree
			 */
			premisesDetails.setValue(view.getPremises());
		}
		
		if(view.getAddress()!=null){
			System.out.println(" AAAAAAAAAAAAAAAA in side update model ");
			addressComp.setValue(view.getAddress());
		}
		if(view.getTeam()!=null){
			olbTeam.setValue(view.getTeam());
		}
		
		if(view.getProduct()!=null){
			ProductInfo prodInfo=new ProductInfo();
			prodInfo.setProdID(view.getProduct().getCount());
			prodInfo.setProductCode(view.getProduct().getProductCode());
			prodInfo.setProductName(view.getProduct().getProductName());
			prodInfo.setProductCategory(view.getProduct().getProductCategory());
			prodInfo.setProductPrice(view.getProduct().getPrice());
			prodComposite.setValue(prodInfo);
		}
		
		if(view.getRefNo()!=null)
			refNo.setValue(view.getRefNo());
		
		

		if(view.getCatchtrapList()!=null&&view.getCatchtrapList().size()!=0)
			catchTrap.setValue(view.getCatchtrapList());
		
		
		if(view.getRefNo2()!=null){
			tbRefNo2.setValue(view.getRefNo2());
		}
		
		ibServiceWeekNo.setValue(view.getServiceWeekNo());
		
		if(view.getUom()!=null&&!view.getUom().trim().equals("")){
			olbUOM.setValue(view.getUom().trim());
		}
		if(view.getQuantity()!=0){
			quantity.setValue(view.getQuantity());
		}
		
		/**29-09-2017 sagar sore[for service value] **/
		if(view.getServiceValue()!=0.0)
			serviceValue.setValue(view.getServiceValue());
		
		/**
		 * Date 14-08-2017 addesd by vijay for Popup from quickcontract getting null exception
		 */
		
		if(view.getCustomerSignature()!=null){
			uploadCustomerSignature.setValue(view.getCustomerSignature());
		}
		/**
		 * Date 14-08-2017 addesd by vijay for Popup from quickcontract getting null exception
		 */
		
		if(view.getCustomerSignName()!=null){
			customerSignName.setValue(view.getCustomerSignName());;
		}
		
		if(view.getCustomerSignNumber()!=null){
			customerSignNumber.setValue(view.getCustomerSignNumber());
		}
		
		if(view.getDescription()!=null){
			tacomment.setValue(view.getDescription());
		}
		
		/***30-12-2019 Deepak Salve added this code for Designation and Emp Code***/
		if(view.getCustomerPersonDesignation()!=null){
			customerSignPersonDesignation.setValue(view.getCustomerPersonDesignation());
		}
		if(view.getCustomerPersonEmpId()!=null){
			customerSignPersonEmpcode.setValue(view.getCustomerPersonEmpId());
		}
		/***End***/
		
		/** date 16.02.2018 added by komal for area wise billing **/
		if(view.getPerUnitPrice()!=0){
			dlbPerUnitPrice.setValue(view.getPerUnitPrice());
		}
		/**
		 * nidhi
		 * 9-08-2018
		 * 
		 */
//		showDialogMessage(view.getProModelNo());
			tbProModelNo.setValue(view.getProModelNo());
			tbProSerialNo.setValue(view.getProSerialNo());
//			showDialogMessage(tbProSerialNo.getValue());
		/**
		 * end
		 */
		valideconfigrationDetail(view);
	
		/**
		 * nidhi 15-10-2018
		 * ||*
		 *if(view.getBillingCount()!=0)*/
		tbBillingCount.setValue(view.getBillingCount()+"");
		
		if(view.getServiceImage1()!=null){
			ucServiceImage1.setValue(view.getServiceImage1());
		}
		if(view.getServiceImage2()!=null){
			ucServiceImage2.setValue(view.getServiceImage2());
		}
		if(view.getServiceImage3()!=null){
			ucServiceImage3.setValue(view.getServiceImage3());
		}
		if(view.getServiceImage4()!=null){
			ucServiceImage4.setValue(view.getServiceImage4());
		}
		
		if(view.getServiceSummaryReport2()!=null){
			serviceSummaryReport2.setValue(view.getServiceSummaryReport2());
		}
		
		/**
		 * Date 04-05-2019 by vijay for NBHC CCPM
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "EnableStackDetailsMandatory")){
			getStackDetails(view.getCompanyId(),view.getContractCount());
		}
		
		if(view.getCheckList()!=null){
			checkListTbl.setValue(view.getCheckList());
		}
		
		if(view.getPestTreatmentList()!=null){
			pestTreatmentTbl.setValue(view.getPestTreatmentList());
		}
		
		if(view.getServiceImage5()!=null){
			ucServiceImage5.setValue(view.getServiceImage5());
		}
		if(view.getServiceImage6()!=null){
			ucServiceImage6.setValue(view.getServiceImage6());
		}
		if(view.getServiceImage7()!=null){
			ucServiceImage7.setValue(view.getServiceImage7());
		}
		if(view.getServiceImage8()!=null){
			ucServiceImage8.setValue(view.getServiceImage8());
		}
		if(view.getServiceImage9()!=null){
			ucServiceImage9.setValue(view.getServiceImage9());
		}
		if(view.getServiceImage10()!=null){
			ucServiceImage10.setValue(view.getServiceImage10());
		}
		/**Added by sheetal:31-01-2022
		 * Des:If team of technicians are selected then names of all 
		       technicians will print, requirement raised by poonam**/
		if(view.getTechnicians()!=null && view.getTechnicians().size()>1){
			String stremployeName = "";
			for(int i =0;i< view.getTechnicians().size();i++){
				stremployeName = stremployeName + view.getTechnicians().get(i).getFullName();
				if(i!=view.getTechnicians().size()-1){
					stremployeName +=",";
				}
			}
			Console.log("team"+stremployeName);
			team.setValue(stremployeName);
		}
		
		
		if(presenter!=null)
		presenter.setModel(view);
		
		if(view.getTrackServiceTabledetails()!=null){
			Console.log("track table not null"+view.getTrackServiceTabledetails().size());
			trackServiceCompletionTable.setValue(view.getTrackServiceTabledetails());
		}
		
		if(view.getTotalServiceDuration()>=0) {
			ibTotalServiceDuration.setValue(view.getTotalServiceDuration());
		}
		
		if(view.getMaterialCost()>=0)
			materialCost.setValue(view.getMaterialCost());
		
		if(view.getLaborCost()>=0)
			labourCost.setValue(view.getLaborCost());
		
		if(view.getExpenseCost()>=0)
			expenses.setValue(view.getExpenseCost());
		
		if(view.getCertificateValidTillDate()!=null)
			tbWarrantyDate.setValue(view.getCertificateValidTillDate());
		double cost=view.getMaterialCost()+view.getLaborCost()+view.getExpenseCost();
		totalCost.setValue(cost);
		
		loadServiceProject(view);
		
	}
	
	

	private void getStackDetails(Long companyId, Integer contractCount) {
		MyQuerry querry  = new MyQuerry();
		Vector<Filter> filterVec = new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(contractCount);
		filterVec.add(filter);
		
		querry.setFilters(filterVec);
		querry.setQuerryObject(new Contract());
		
		genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				stackdetails = new ArrayList<StackDetails>();
				for(SuperModel model : result){
					Contract contract  = (Contract) model;
					if(contract.getStackDetailsList()!=null)
					stackdetails = contract.getStackDetailsList();
					break;
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
		
	}

	/**
	 * Date 20-04-2018 By vijay
	 * here i am refreshing address composite data if global list size greater than drop down list size
	 * because we are adding locality and city from an entity in view state to global list if doest not exist in global list 
	 */
	private void setAddressDropDown() {
		if(LoginPresenter.globalLocality.size()>addressComp.locality.getItems().size()	){
			addressComp.locality.setListItems(LoginPresenter.globalLocality);
		}
	}
	/**
	 * ends here 
	 */
	
void valideconfigrationDetail(Service view){
		
		boolean flag = false;
		System.out.println("get here service ");
		//olbservicetype=new ObjectListBox<Config>();
//		AppUtility.MakeLiveConfig(olbservicetype, Screen.SERVICETYPE);
		if(view.getServiceType() != null){ 
			for(Config confi : LoginPresenter.globalConfig){
				if(confi.getName().equalsIgnoreCase(view.getServiceType()) && confi.getType() == 78){
					flag = true;
					break;
				}
			}
			
			if(!flag){
				String configType=AppConstants.GLOBALRETRIEVALCONFIG+"-"+view.getCompanyId();
				int i =AppUtility.globalMakeLiveConfig(configType);
				
				/*olbservicetype=new ObjectListBox<Config>();*/
				olbservicetype.clear();
				AppUtility.MakeLiveConfig(olbservicetype, Screen.SERVICETYPE);
				
				if(view.getServiceType() != null){
					olbservicetype.setValue(view.getServiceType());
				}
			}
		}
		
	}
	
	@Override
	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Save")||text.contains("Discard")||text.contains("Search")||text.contains(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
				
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			}
			
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Save")||text.contains("Discard")||text.contains(AppConstants.NAVIGATION)){
					menus[k].setVisible(true);
				}
				
				else{
					menus[k].setVisible(false);
				}
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			System.out.println("View Stattet");
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			}
			
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Edit")||text.contains("Discard")||text.contains("Search")||text.contains("Print")||text.contains(AppConstants.NAVIGATION) ||text.contains("Email") || text.contains(AppConstants.COMMUNICATIONLOG))
						menus[k].setVisible(true); 
				
				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.DEVICE,LoginPresenter.currentModule.trim());
	}


	
	
	@Override
	public void setEnable(boolean state)
    {
        super.setEnable(state);
        if(flag==false){
           tbContractId.setEnabled(false);
        }
        ibServiceSrNo.setEnabled(false);
        poc.setEnabled(false);
        startDate.setEnabled(false);
    	endDate.setEnabled(false);
    	tbStatus.setEnabled(false);
    	ibServiceId.setEnabled(false);
    	this.tbServiceDate.setEnabled(false);
    	this.tbServiceDateCopy.setEnabled(false);
    	taReasonForChange.setEnabled(false);
    	prodComposite.setEnabled(false);
    	dbServiceCompletionDate.setEnabled(false);
    	tbServiceday.setEnabled(false);
		tbServicetime.setEnabled(false);
		tbserviceBranch.setEnabled(false);
		ibServiceDuration.setEnabled(false);
//		tbservicecompletionremark.setEnabled(false);//Ashwini Patil
		this.tbTicketNumber.setEnabled(false);
//		tbCustomerFeedback.setEnabled(false);//Ashwini Patil
		//addressComp.setEnable(false);
		/** date 19.4.2018 changed by komal **/
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "MakeAddressEditable")){
			addressComp.setEnable(true);
		}else{
			addressComp.setEnable(false);
		}
		olbTeam.setEnabled(false);
		refNo.setEnabled(false);
		tbRefNo2.setEnabled(false);
		this.btstackDetails.setEnabled(true);
		
		ibServiceWeekNo.setEnabled(false);

		serviceValue.setEnabled(false);
		/**
		 * nidhi
		 * 20-08-2018
		 * 
		 */
		tbProModelNo.setEnabled(false);
		tbProSerialNo.setEnabled(false);
		
		catchTrap.setEnable(state);
		checkListTbl.setEnable(state);
		pestTreatmentTbl.setEnable(state);
		trackServiceCompletionTable.setEnable(state);
		materialProdTable.setEnable(false);
		ibTotalServiceDuration.setEnabled(false);
		labourCost.setEnabled(false);
		materialCost.setEnabled(false);
		expenses.setEnabled(false);
		totalCost.setEnabled(false);
    }


	@Override
	public void clear()
	{
		super.clear();
		this.tbStatus.setText(Service.SERVICESTATUSSCHEDULE);
	}
	
	@Override
	public void setCount(int count)
	{
		ibServiceId.setValue(count+"");
	}
	
	public void toggleProcessLevelMenu()
	{
		Service entity=(Service) presenter.getModel();
		
		String status=entity.getStatus();
		ArrayList<String> pCodeList = new ArrayList<String>();
		pCodeList.add("STK-01");
		pCodeList.add("SLM-01");
		pCodeList.add("AFC-10");
		pCodeList.add("CON-02");
		pCodeList.add("AFC-02");
		pCodeList.add("BNF-01");
		pCodeList.add("CON-01");

		/**
		 * Date 06-07-2018 By Vijay
		 * Des :- For NBHC Reverse Service Completion button not visible at all status
		 */
		boolean reverseCompletionflag = false;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "OnlyForNBHC")){
			reverseCompletionflag = true;
		}
		/**
		 * ends here
		 */
		String ProCode = entity.getProduct().getProductCode();
		for(int i=0;i<getProcesslevelBarNames().length;i++)
		{
			InlineLabel label=getProcessLevelBar().btnLabels[i];
			String text=label.getText().trim();
			
			
			if(status.equals(Service.SERVICESTATUSTECHCOMPLETED)){
				
				
				if(text.equals("Reschedule"))
					label.setVisible(false);
				if(text.equals("Cancel"))
					label.setVisible(false);
				
				if(text.equals("Reverse Service Completion"))
				{
					if(reverseCompletionflag){
						label.setVisible(false);
					}else{
						label.setVisible(true);
					}
				}
				
				/**
				 * Date : 16-03-2017 By ANIL
				 */
				if(text.equals("MIS")){
					label.setVisible(true);
				}
				
				/**
				 * Date : 31-10-2017 By ANIL
				 */
				if(text.equals(AppConstants.VIEWBILL)){
					label.setVisible(true);
				}
				/**@Sheetal : 15-02-2022**/
				if(text.equals(AppConstants.VIEWORDER)){
					label.setVisible(true);
				}
				/**
				 * End
				 */
				
				/**
				 * @author Anil
				 * @since 12-05-2020
				 */
				if(tbTicketNumber.getValue()==null||tbTicketNumber.getValue().equals("")){
					if(text.equals("View Complain")){
						label.setVisible(false);
					}
				}else{
					if(text.equals("View Complain")){
						label.setVisible(true);
					}
				}
				
			}
			
			else if(status.equals(Service.CANCELLED)||status.equals(Service.SERVICESTATUSCLOSED)
					||status.equals(Service.SERVICESTATUSCOMPLETED))
			{
				if(text.equals("Reschedule"))
					label.setVisible(false);
				else if(text.equals("Cancel"))
					label.setVisible(false);
				else if(text.equals("Mark Completed"))
					label.setVisible(false);
				else if(text.equals("Reverse Service Completion"))
				{
					if(reverseCompletionflag){
						label.setVisible(false);
					}else{
						label.setVisible(true);
					}
				}
				
				/**
				 * Date : 16-03-2017 By ANIL
				 */
				if(text.equals("MIS")){
					label.setVisible(true);
				}
				/**
				 * Date : 31-10-2017 By ANIL
				 */
				if(text.equals(AppConstants.VIEWBILL)){
					label.setVisible(true);
				}
				/**
				 * End
				 */
				/**@Sheetal : 15-02-2022**/
				if(text.equals(AppConstants.VIEWORDER)){
					label.setVisible(true);
				}
				/*else if(text.equals("Manage Customer Project"))
					label.setVisible(false);*/
				/**
				 * @author Anil
				 * @since 12-05-2020
				 */
				if(tbTicketNumber.getValue()==null||tbTicketNumber.getValue().equals("")){
					if(text.equals("View Complain")){
						label.setVisible(false);
					}
				}else{
					if(text.equals("View Complain")){
						label.setVisible(true);
					}
				}
			}
			
			else if(status.equals(Service.SERVICESTATUSCOMPLETED))
			{
				if(text.equals("Reschedule"))
					label.setVisible(false);
				else if(text.equals("Cancel"))
					label.setVisible(false);
				else if(text.equals("Mark Completed"))
					label.setVisible(false);
				
				/**
				 * Date : 16-03-2017 By ANIL
				 */
				if(text.equals("MIS")){
					label.setVisible(true);
				}
				
				/**
				 * rohan added this code for NBHC Changes ADMIN role should  
				 */
				if(LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN")){
				if(text.equals("Reverse Service Completion"))
					label.setVisible(true);
				}
				else
				{
					if(text.equals("Reverse Service Completion"))
						label.setVisible(false);
				}
				
				/**
				 * Date : 31-10-2017 By ANIL
				 */
				if(text.equals(AppConstants.VIEWBILL)){
					label.setVisible(true);
				}
				/**
				 * End
				 */
				/**@Sheetal : 15-02-2022**/
				if(text.equals(AppConstants.VIEWORDER)){
					label.setVisible(true);
				}
				/**
				 * @author Anil
				 * @since 12-05-2020
				 */
				if(tbTicketNumber.getValue()==null||tbTicketNumber.getValue().equals("")){
					if(text.equals("View Complain")){
						label.setVisible(false);
					}
				}else{
					if(text.equals("View Complain")){
						label.setVisible(true);
					}
				}
				
			}
			else
			{
				/**
				 * rohan added this code for NBHC Changes ADMIN role should  
				 */
				if(LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN") ||
						LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("Zonal Coordinator")){
					
					if(LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN")){
						if(text.equals("Reverse Service Completion")){
							label.setVisible(true);
						}
					}
					
					/** Date 02-10-2019 by Vijay for NBHC CCPM Cancellation to zonal coordinator and Admin **/
					if(LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN") ||
							LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("Zonal Coordinator") ){
						if(text.equals("Cancel")){
							label.setVisible(true);
						}
					}
				
				}
				else
				{
					if(text.equals("Reverse Service Completion"))
						label.setVisible(false);
					
					if(text.equals("Cancel")){
						label.setVisible(false);
					}
				}
				
				/**
				 * Date : 16-03-2017 By ANIL
				 */
				if(text.equals("MIS")){
					label.setVisible(true);
				}
				
				/**
				 * Date : 31-10-2017 By ANIL
				 */
				if(text.equals(AppConstants.VIEWBILL)){
					label.setVisible(true);
				}
				/**
				 * End
				 */
				/**@Sheetal : 15-02-2022**/
				if(text.equals(AppConstants.VIEWORDER)){
					label.setVisible(true);
				}
				/**
				 * @author Anil
				 * @since 12-05-2020
				 */
				if(tbTicketNumber.getValue()==null||tbTicketNumber.getValue().equals("")){
					if(text.equals("View Complain")){
						label.setVisible(false);
					}
				}else{
					if(text.equals("View Complain")){
						label.setVisible(true);
					}
				}
			}
			//Date 15-08-2017 added by vijay for popup manage menus
			if(isPopUpAppMenubar()){
			
				 if(status.equals(Service.SERVICESTATUSSCHEDULE) || status.equals(Service.SERVICESTATUSSCHEDULE)){
					 if(text.equals("Manage Customer Project"))
							label.setVisible(false);
					
					/**
					 * Date : 31-10-2017 By ANIL
					 */
					if(text.equals(AppConstants.VIEWBILL)){
						label.setVisible(false);
					}
					/**
					 * End
					 */
				 }

			}
			//Ashwini Patil Date:11-07-2022
			if(isSummaryPopupFlag()) {
				if(text.equals("Manage Customer Project")) {
					Console.log("Manage Customer Project set to visible false");
					label.setVisible(false);
				}
			}
			if(text.equals(AppConstants.SERVICEFUMIGATIONREPORT)){
				label.setVisible(true);
			}
			
			if(text.equals(AppConstants.SERVICEFUMIGATIONVALUEREPORT)){
				label.setVisible(true);
			}
			/**
			 * Date : 14-12-2017 BY ANIL
			 * hiding commodity fumigation report and service revenue report for users other than admin
			 */
			if(!UserConfiguration.getRole().getRoleName().equalsIgnoreCase("ADMIN")){
				if(text.equals(AppConstants.SERVICEFUMIGATIONREPORT)||text.equals(AppConstants.SERVICEFUMIGATIONVALUEREPORT)){
					label.setVisible(false);
				}
			}
						
			/**
			 * date 26/2/2018
			 * By jayshree
			 * Des.To show the print service certificate button if service status is completed 
			 */
			
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "ENABLEPRINTSERVICECERTIFICATE")){
				if(status.equals(Service.SERVICESTATUSCOMPLETED)){
					System.out.println("status111");
				if(text.equals("Print Service Certificate")){
					label.setVisible(true);
				}
				}
				else if(status.equals(Service.SERVICESTATUSRESCHEDULE)){
					System.out.println("status222");
					if(text.equals("Print Service Certificate")){
						label.setVisible(true);
					}
				}
				else if(status.equals(Service.SERVICESTATUSSCHEDULE)){
					System.out.println("status333");
					if(text.equals("Print Service Certificate")){
						label.setVisible(true);
					}
				}

				else if(status.equals(Service.SERVICESTATUSCANCELLED)){
					System.out.println("status444");
					if(text.equals("Print Service Certificate")){
						label.setVisible(true);
					}
				}
				

				else if(status.equals(Service.SERVICESTATUSCLOSED)){
					System.out.println("status555");
					if(text.equals("Print Service Certificate")){
						label.setVisible(true);
					}
				}
				
				else if(status.equals(Service.SERVICESTATUSPENDING)){
					System.out.println("status666");
					if(text.equals("Print Service Certificate")){
						label.setVisible(true);
					}
				}
				
				else if(status.equals(Service.SERVICESUSPENDED)){
					System.out.println("status666");
					if(text.equals("Print Service Certificate")){
						label.setVisible(true);
					}
				}
			}
			else{
			if(status.equals(Service.SERVICESTATUSCOMPLETED)){
				System.out.println("status111");
			if(text.equals("Print Service Certificate")){
				label.setVisible(true);
			}
			}
			else if(status.equals(Service.SERVICESTATUSRESCHEDULE)){
				System.out.println("status222");
				if(text.equals("Print Service Certificate")){
					label.setVisible(false);
				}
			}
			else if(status.equals(Service.SERVICESTATUSSCHEDULE)){
				System.out.println("status333");
				if(text.equals("Print Service Certificate")){
					label.setVisible(false);
				}
			}

			else if(status.equals(Service.SERVICESTATUSCANCELLED)){
				System.out.println("status444");
				if(text.equals("Print Service Certificate")){
					label.setVisible(false);
				}
			}
			

			else if(status.equals(Service.SERVICESTATUSCLOSED)){
				System.out.println("status555");
				if(text.equals("Print Service Certificate")){
					label.setVisible(false);
				}
			}
			
			else if(status.equals(Service.SERVICESTATUSPENDING)){
				System.out.println("status666");
				if(text.equals("Print Service Certificate")){
					label.setVisible(false);
				}
			}
			
			else if(status.equals(Service.SERVICESUSPENDED)){
				System.out.println("status666");
				if(text.equals("Print Service Certificate")){
					label.setVisible(false);
				}
			}
			}
			//End By Jayshree
			
			
			/**
			 * Date 10/3/2018 
			 * By Jayshree
			 * Des.Add the suspend button to hold the service 
			 */
			
			
			if(status.equals(Service.SERVICESTATUSCOMPLETED)){
				System.out.println("status111");
			if(text.equals(AppConstants.SUSPEND)){
				label.setVisible(false);
			}
			}
			else if(status.equals(Service.SERVICESTATUSRESCHEDULE)){
				System.out.println("status222");
				if(text.equals(AppConstants.SUSPEND)){
					label.setVisible(true);
				}
			}
			else if(status.equals(Service.SERVICESTATUSSCHEDULE)){
				System.out.println("status333");
				if(text.equals(AppConstants.SUSPEND)){
					label.setVisible(true);
				}
			}

			else if(status.equals(Service.SERVICESTATUSCANCELLED)){
				System.out.println("status444");
				if(text.equals(AppConstants.SUSPEND)){
					label.setVisible(false);
				}
			}
			

			else if(status.equals(Service.SERVICESTATUSCLOSED)){
				System.out.println("status555");
				if(text.equals(AppConstants.SUSPEND)){
					label.setVisible(false);
				}
			}
			
			else if(status.equals(Service.SERVICESTATUSPENDING)){
				System.out.println("status666");
				if(text.equals(AppConstants.SUSPEND)){
					label.setVisible(false);
				}
			}
			
			else if(status.equals(Service.SERVICESUSPENDED)){
				System.out.println("status666");
				if(text.equals(AppConstants.SUSPEND)){
					label.setVisible(false);
				}
			}
			//End By Jayshree
			
//			if(status.equals(Service.SERVICESUSPENDED)){
//				System.out.println("status666");
//				if(text.equals("Cancel")){
//					label.setVisible(false);
//				}
//				else if(text.equals("Mark Completed")){
//					label.setVisible(false);
//				}
//			}
			
			

			/**
			 * nidhi for hide wpm fumigation 
			 * for 
			 */
			if(text.equals("WPM Fumigation")|| text.equals("Commodity Fumigation")){
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "ONLYFORNBHC") && pCodeList.contains(ProCode)){
					label.setVisible(true);
				}else{
					label.setVisible(false);
				}
			}
			
			if(text.equals("Periodic Servic record")){
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "ONLYFORNBHC") && status.equals(Service.SERVICESTATUSCOMPLETED)){
					label.setVisible(false);
				}else{
					label.setVisible(true);
				}
			}
			
			if(text.equals("Reverse Service Completion")){
				if((AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "HideReverseServiceCompletionButton") && LoginPresenter.branchRestrictionFlag && !LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN"))||status.equals(Service.CANCELLED)){//Ashwini Patil Date:11-11-2024 added as per ultra request as this option was visible to cancelled services also which is wrong 
					label.setVisible(false);
				}else if(status.equals(Service.CANCELLED)){//Ashwini Patil Date:11-11-2024 added as per ultra request as this option was visible to cancelled services also which is wrong 
					
					if(LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN")){
						if(text.equals("Reverse Service Completion"))
							label.setVisible(true);
						}
						else
						{
							if(text.equals("Reverse Service Completion"))
								label.setVisible(false);
						}
						
				}else{
					label.setVisible(true);
				}
			}
			
			if(text.equals("Create Fumigation")){
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "HideCreateFumigationButton")){
					label.setVisible(false);
				}else{
					label.setVisible(true);
				}
			}
			
			
			/**
			 * @author Anil
			 * @since 12-05-2020
			 */
			if(tbTicketNumber.getValue()==null||tbTicketNumber.getValue().equals("")){
				if(text.equals("View Complain")){
					label.setVisible(false);
				}
			}else{
				if(text.equals("View Complain")){
					label.setVisible(true);
				}
			}
			
			
			if(isPopUpAppMenubar()){
				if(text.equals("New")){
					label.setVisible(false);
				}
			}
			
			//Ashwini Patil Date:20-09-2022
			if(isHideNavigationButtons()) {
				Console.log("in changeProcessLevel isHideNavigationButtons");
				if(text.equalsIgnoreCase("Manage Customer Project")||text.equalsIgnoreCase("New")||text.equalsIgnoreCase(AppConstants.VIEWBILL)||text.equalsIgnoreCase(AppConstants.VIEWORDER)||text.equalsIgnoreCase("View Expense")||text.equalsIgnoreCase("View Complain"))
					label.setVisible(false);
			}
		}	

	}
	/**
	 * Sets the Application Header Bar as Per Status
	 */

	public void setAppHeaderBarAsPerStatus()
	{
		Service entity=(Service) presenter.getModel();
		String status=entity.getStatus();
		
		//Ashwini Patil Date:13-01-2022 As per Nitin Sir's instruction made edit button available after service completion
		//||status.equals(Service.SERVICESTATUSCOMPLETED
		if(status.equals(Service.CANCELLED)||status.equals(Service.SERVICESTATUSCLOSED))
		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			/**
			 * Date 14-08-2017 added by vijay for normal and popup level menus bar manage
			 */
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			}
			
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Discard")||text.contains("Search")||text.contains("Print")||text.contains(AppConstants.NAVIGATION) || text.contains("Email") || text.contains(AppConstants.MESSAGE))
				{
					menus[k].setVisible(true); 
					
				}
				else
				{
					menus[k].setVisible(false);  
					
//					/**
//					 * @author Anil
//					 * @since 13-05-2020
//					 */
//					if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Complain", "ComplainServiceWithTurnAroundTime")&&text.contains("Email")){
//						menus[k].setVisible(true);
//					}
					

				}

			}

		}
		
//		if(status.equals(Service.SERVICESTATUSSCHEDULE)||status.equals(Service.SERVICESTATUSRESCHEDULE))
//		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
//			for(int k=0;k<menus.length;k++)
//			{
//				String text=menus[k].getText();
//				if(text.contains("Discard")||text.contains("Search")||text.contains("Edit")||text.contains("Print"))
//				{
//					menus[k].setVisible(true); 
//				}
//				else
//				{
//					menus[k].setVisible(false);  
//				}
//			}
//		}
		
	}
	/**
	 * The method is responsible for changing Application state as per 
	 * status
	 */
	public void setMenuAsPerStatus()
	{
		this.setAppHeaderBarAsPerStatus();
		this.toggleProcessLevelMenu();
		
	}
	@Override
	public void setToViewState()
	{
		super.setToViewState();
		setMenuAsPerStatus();
		
		
		
		if(dbServiceCompletionDate.getValue()!=null){
			if(!dbServiceCompletionDate.getValue().equals(tbServiceDate.getValue())){
				dbServiceCompletionDate.addStyleName("red");
			}
		}
		
		SuperModel model=new Service();
		model=serviceObj;
		if(poc.getPhone().getValue()!=null&&!poc.getPhone().getValue().equals("")&&!poc.getPhone().getValue().equals("null")) {
			Console.log("In Device form phone !=null value="+poc.getPhone().getValue());
			AppUtility.addDocumentToHistoryTable(AppConstants.SERVICEMODULE,AppConstants.CUSTOMERSERVICE, serviceObj.getCount(), Integer.parseInt(poc.getId().getValue()),poc.getName().getValue(),Long.parseLong(poc.getPhone().getValue()), false, model, null);			
		}else {
			Console.log("In device form cell number null");
			AppUtility.checkDocumentIdAlreadyExist(AppConstants.SERVICEMODULE,AppConstants.CUSTOMERSERVICE,serviceObj.getCount());
			String moduleName=AppConstants.SERVICEMODULE;
			String documentType=AppConstants.CUSTOMERSERVICE; 
			int documentId=	serviceObj.getCount();
			int personId=Integer.parseInt(poc.getId().getValue());
			String personName=poc.getName().getValue();
			boolean isList=false;
			if(LoginPresenter.globalHistoryList.size()<10){
				Console.log("3");
				History his=new History();
				
				his.setModuleName(moduleName);
				his.setDocumentType(documentType);
				his.setDocumentId(documentId);
				his.setListForm(isList);
				his.setModel(model);
				his.setListModel(null);
				his.setPersonId(personId);
				his.setPersonName(personName);
				
				LoginPresenter.globalHistoryList.add(his);
			}else{
				Console.log("4");
				LoginPresenter.globalHistoryList.remove(0);
				
				History his=new History();
				
				his.setModuleName(moduleName);
				his.setDocumentType(documentType);
				his.setDocumentId(documentId);
				his.setListForm(isList);
				his.setModel(model);
				his.setListModel(null);
				
				LoginPresenter.globalHistoryList.add(his);
				
			}
		}
		String mainScreenLabel="CUSTOMER SERVICE";
		if(serviceObj!=null&&serviceObj.getCount()!=0){
			mainScreenLabel=serviceObj.getCount()+"/"+serviceObj.getStatus()+"/"+AppUtility.parseDate(serviceObj.getCreationDate());
		}
		
			fgroupingCustomerInformation.getHeaderLabel().setText(mainScreenLabel);
	
	
	
	}
	
	@Override
	public void setToEditState()
	{
		super.setToEditState();
		this.processLevelBar.setVisibleFalse(false);
		setMenuAsPerStatus();
		
		String mainScreenLabel="CUSTOMER SERVICE";
		if(serviceObj!=null&&serviceObj.getCount()!=0){
			mainScreenLabel=serviceObj.getCount()+"/"+serviceObj.getStatus()+"/"+AppUtility.parseDate(serviceObj.getCreationDate());
		}
		
			fgroupingCustomerInformation.getHeaderLabel().setText(mainScreenLabel);
	}
	
	@Override
	public boolean validate()
	{
		/**
		 * @author Anil
		 * @since 06-05-2020
		 * Updating validation for NBHC fumigation services
		 * NBHC INTERNAL 100007885
		 */
		boolean superValidate=true;
		if(serviceObj!=null&&
				serviceObj.getPersonInfo().getCount()==100007885&&
				serviceObj.getPersonInfo().getFullName().equalsIgnoreCase("NBHC INTERNAL")) {
			superValidate=validateForInHouseServices();
		}else {
			superValidate=super.validate();
		}
		
		if(superValidate==false)
			return false;
		else
		{
			Date servDate=tbServiceDate.getValue();
			Date contractStartDate=startDate.getValue();
			Date endDate=this.endDate.getValue();
			
			if(servDate.before(contractStartDate))
			{
				this.showDialogMessage("Service Date can't be before Contract Start Date !");
			    return false;	
			}
			
			if(prodflag==true&&this.prodComposite.getValue()==null){
				showDialogMessage("Please select product!");
				return false;
			}
			
			/*if(servDate.after(endDate))
			{
				this.showDialogMessage("Service Date can't be after Contract End Date !");
				 return false;
			}*/
			
			/**
			 * Date 11-08-2018 By Vijay
			 * Des :- NBHC CCPM Quatity should be greater than 0
			 */
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "OnlyForNBHC"))
			{
				if(quantity.getValue() != null && quantity.getValue().toString().trim().length()>0 &&( quantity.getValue()==0||quantity.getValue()==0.0)){
					showDialogMessage("Quantity should be greater than 0");
					return false;
				}
			}
			/**
			 * ends here
			 */
			/*** Date 26-04-2019 by Vijay for NBHC CCPM stack details mandatory ***/
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "EnableStackDetailsMandatory")){
				boolean validateflag = validateStackDetails();
				if(validateflag==false){
					return false;
				}
			}
				
		}
//		if(serviceObj!=null&&serviceObj.getStatus().equals(Service.SERVICESTATUSTECHCOMPLETED)){
//			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("EVA Pedio", "PC_DocumentUploadMandatory")){
//				Console.log("in validate");
//				if(upTestReport.getValue()==null){
//					showDialogMessage("Service Report upload is mandatory! Click on Customer Report tab and upload the report.");
//					return false;
//				}else{
//					Console.log("in validate url="+upTestReport.getValue().getUrl());
//				}
//			}		
//		}
		return true;
	}
	 
	
	private boolean validateStackDetails() {
		Service servie = (Service) this.getPresenter().getModel();
		DevicePresenter prsenter = (DevicePresenter) this.getPresenter();
		if(servie.getStackDetailsList()==null && stackdetails.size()==0){
			return true;
		}
		ArrayList<StackDetails> stackdetails = null;
		if(servie.getStackDetailsList()!=null){
			stackdetails = prsenter.getSelectedRecord(servie.getStackDetailsList());
		}
		if(stackdetails==null || stackdetails.size()==0){
			showDialogMessage("Please select stack details");
			return false;
		}
		return true;
	}

	public TextBox getTbContractId() {
		return tbContractId;
	}

	public void setTbContractId(TextBox tbContractId) {
		this.tbContractId = tbContractId;
	}

	public TextBox getTbStatus() {
		return tbStatus;
	}

	public void setTbStatus(TextBox tbStatus) {
		this.tbStatus = tbStatus;
	}

	public DateBox getTbServiceDate() {
		return tbServiceDate;
	}

	public void setTbServiceDate(DateBox tbServiceDate) {
		this.tbServiceDate = tbServiceDate;
	}

	public ObjectListBox<Branch> getTbBranch() {
		return tbBranch;
	}

	public void setTbBranch(ObjectListBox<Branch> tbBranch) {
		this.tbBranch = tbBranch;
	}

	public ObjectListBox<Employee> getTbServiceEngg() {
		return tbServiceEngg;
	}

	public void setTbServiceEngg(ObjectListBox<Employee> tbServiceEngg) {
		this.tbServiceEngg = tbServiceEngg;
	}

	

	public UploadComposite getUpTestReport() {
		return upTestReport;
	}

	public void setUpTestReport(UploadComposite upTestReport) {
		this.upTestReport = upTestReport;
	}

	public UploadComposite getUpDevice() {
		return upDevice;
	}

	public void setUpDevice(UploadComposite upDevice) {
		this.upDevice = upDevice;
	}

	public PersonInfoComposite getPoc() {
		return poc;
	}

	public void setPoc(PersonInfoComposite poc) {
		this.poc = poc;
	}

//	public AddressComposite getAddr() {
//		return addr;
//	}
//
//	public void setAddr(AddressComposite addr) {
//		this.addr = addr;
//	}
	
	public ProductInfoComposite getProdComposite() {
		return prodComposite;
	}

	public void setProdComposite(ProductInfoComposite prodComposite) {
		this.prodComposite = prodComposite;
	}
	
	public IntegerBox getIbServiceSrNo() {
		return ibServiceSrNo;
	}

	public void setIbServiceSrNo(IntegerBox ibServiceSrNo) {
		this.ibServiceSrNo = ibServiceSrNo;
	}

	public TextBox getTbServiceday() {
		return tbServiceday;
	}

	public void setTbServiceday(TextBox tbServiceday) {
		this.tbServiceday = tbServiceday;
	}

	public TextBox getTbServicetime() {
		return tbServicetime;
	}

	public void setTbServicetime(TextBox tbServicetime) {
		this.tbServicetime = tbServicetime;
	}

//	public TextBox getTbserviceBranch() {
//		return tbserviceBranch;
//	}
//
//	public void setTbserviceBranch(TextBox tbserviceBranch) {
//		this.tbserviceBranch = tbserviceBranch;
//	}
	
	public ObjectListBox<CustomerBranchDetails> getTbserviceBranch() {
		return tbserviceBranch;
	}

	public void setTbserviceBranch(ObjectListBox<CustomerBranchDetails> tbserviceBranch) {
		this.tbserviceBranch = tbserviceBranch;
	}

	public String getF_markRemark() {
		return f_markRemark;
	}

	public void setF_markRemark(String f_markRemark) {
		this.f_markRemark = f_markRemark;
	}

	public int getF_markduration() {
		return f_markduration;
	}

	public void setF_markduration(int f_markduration) {
		this.f_markduration = f_markduration;
	}

	public IntegerBox getIbServiceDuration() {
		return ibServiceDuration;
	}

	public void setIbServiceDuration(IntegerBox ibServiceDuration) {
		this.ibServiceDuration = ibServiceDuration;
	}

	public TextBox getTbservicecompletionremark() {
		return tbservicecompletionremark;
	}

	public void setTbservicecompletionremark(TextBox tbservicecompletionremark) {
		this.tbservicecompletionremark = tbservicecompletionremark;
	}

	public ObjectListBox<Config> getOlbservicetype() {
		return olbservicetype;
	}

	public void setOlbservicetype(ObjectListBox<Config> olbservicetype) {
		this.olbservicetype = olbservicetype;
	}

	public DateBox getDbServiceCompletionDate() {
		return dbServiceCompletionDate;
	}

	public void setDbServiceCompletionDate(DateBox dbServiceCompletionDate) {
		this.dbServiceCompletionDate = dbServiceCompletionDate;
	}

	public Address getAddresInfo() {
		return addresInfo;
	}

	public void setAddresInfo(Address addresInfo) {
		this.addresInfo = addresInfo;
	}
	
	public List<SalesLineItem> getLineItemLis() {
		return DevicePresenter.lineItemLis;
	}

	public void setLineItemLis(List<SalesLineItem> lineItemLis) {
		DevicePresenter.lineItemLis=new ArrayList<SalesLineItem>();
		DevicePresenter.lineItemLis = lineItemLis;
	}

	public AddressComposite getAddressComp() {
		return addressComp;
	}

	public void setAddressComp(AddressComposite addressComp) {
		this.addressComp = addressComp;
	}


	public IntegerBox getPestCount() {
		return pestCount;
	}

	public void setPestCount(IntegerBox pestCount) {
		this.pestCount = pestCount;
	}

	public ObjectListBox<Config> getPestName() {
		return pestName;
	}

	public void setPestName(ObjectListBox<Config> pestName) {
		this.pestName = pestName;
	}
	
	@Override
	public void onClick(ClickEvent event) {
		if(event.getSource()==addBtnChkList){
			if(oblCheckList.getValue()!=null){
				if(checkListTbl.getDataprovider().getList().size()!=0){
					for(CatchTraps obj:checkListTbl.getDataprovider().getList()){
						if(obj.pestName.equals(oblCheckList.getValue())){
							showDialogMessage("Checklist name alreaday entered.");
							return;
						}
					}
				}
				
				CatchTraps checkObj=new CatchTraps();
				checkObj.setPestName(oblCheckList.getValue());
				checkObj.setLocation(tbChkRemark.getValue());
				if(chkcheckliststatus.getValue()!=null){
					checkObj.setStatus(chkcheckliststatus.getValue());
				}
				checkListTbl.getDataprovider().getList().add(checkObj);
				
				oblCheckList.setSelectedIndex(0);
				tbChkRemark.setText("");
			}else{
				showDialogMessage("Please select the checklist name.");
			}
		}
		
		if(event.getSource()==addBtnPestTreatment){
			if(oblPestList.getValue()!=null&&!tbTreatmentArea.equals("")&&!tbMethodOfApp.equals("")){
				if(pestTreatmentTbl.getDataprovider().getList().size()!=0){
					for(PestTreatmentDetails obj:pestTreatmentTbl.getDataprovider().getList()){
						if(obj.pestTarget.equals(oblPestList.getValue())&&obj.treatmentArea.equals(tbTreatmentArea)&&obj.methodOfApplication.equals(tbMethodOfApp)){
							showDialogMessage("Treatment details already added.");
							return;
						}
					}
				}
				
				PestTreatmentDetails pestObj=new PestTreatmentDetails();
				pestObj.setTreatmentArea(tbTreatmentArea.getValue());
				pestObj.setMethodOfApplication(tbMethodOfApp.getValue());
				pestObj.setPestTarget(oblPestList.getValue());
				pestTreatmentTbl.getDataprovider().getList().add(pestObj);
				
				tbTreatmentArea.setText("");
				tbMethodOfApp.setText("");
				oblPestList.setSelectedIndex(0);
			}else{
				showDialogMessage("Please select the treatment details.");
			}
		}
	}

	@Override
	public void onChange(ChangeEvent event) {
		// TODO Auto-generated method stub
		if(event.getSource().equals(dlbPerUnitPrice) || event.getSource().equals(quantity)){
			if(dlbPerUnitPrice.getValue()!=null){	
				if(quantity.getValue()!= null && !(quantity.getValue().equals(""))){
					serviceValue.setValue(dlbPerUnitPrice.getValue()*quantity.getValue());
				}
			}
		}
		

	}

	public TextBox getTbBillingCount() {
		return tbBillingCount;
	}

	public void setTbBillingCount(TextBox tbBillingCount) {
		this.tbBillingCount = tbBillingCount;
	}

	
private void setRearrangedArray(){

	FormField[][] formfield = {   {fgroupingCustomerInformation},
	{fserviceid,ftbContractId,ftbStartDate,ftbEndDate},
	{fpic},
	{ftbTicketNumber,fdbServicingTime,ftbReferenceNumber,ftbRefNo2},
	//{faddr},
	{fgroupingProductInformation},
	{fprodComposite},
	{fgroupingServiceInformation},
	{fibServiceSrNo,ftbServiceEngg,ftbBranch,ftbSrviceDate},
	{fibServiceDay,fibServiceTime,fibServiceBranch,folbservicetype},
	{ftbStatus},
	{ftbCustomerFeedback,folbTeam,fpremisesDetails,fbtstackDetails},
	{ftbBillingCount},//nidhi 15-10-2018
//	{ ftbWareHouse, ftbStorageLocation, ftbStorageBin,ftbDescription, faddStack }, 
//	{ fstackDetailsTable },
	{ftaReasonForChange},
	{fremark,ftbServiceWeekNo,fquantity,fUOM},
	/** 29-09-2017 sagar sore [for service value]**/
	{fdlbPerUnitPrice , fserviceValue},//add by jayshree
	{fgroupingUploadInformation},
	{fupTestReport},
	{fgroupingDeviceInformation},
	{modif},
	{faddressComp},
	{gtrapCatches},
	{ftbCommodity ,ftbContainerNumber ,ftbLocation , fcatchAddButton},
	{ftable},
	{ftacomment},
	{fgrpingBefore},
	{fucServiceImage1,fucServiceImage2},
	{fgrpingAfter},
	{fucServiceImage3,fucServiceImage4},
	
	{fgroupingCustomerSignatureInfo},
	{fdbServiceCompletionDate,fibServiceDuration,fibTotalServiceDuration,fupDevice},
	{ftechnicianRemark,ftbservicecompletionremark},
	{ftrackServiceCompletionTable},
	{fcustomerSignName,fcustomerSignNumber,fcustomerSignPersonDesignation,fcustomerSignPersonEmpcode},
	

	
};

this.fields=formfield;
content.clear();
form=new FlexForm(fields,formstyle);

createGui();
}





public boolean validateForInHouseServices() {
	Console.log("Inside validateForInHouseServices");
	boolean result = true;
	int row = this.fields.length;

	for (int k = 0; k < row; k++) {
		int column = fields[k].length;

		for (int i = 0; i < column; i++) {
			Widget widg = fields[k][i].getWidget();

//			if (fields[k][i].getLabel() != null && fields[k][i].getLabel().contains("Email")) {
//				TextBox tb = (TextBox) widg;
//				String value = tb.getText();
//
//				boolean result1 = validateEmail(value.trim());
//
//				if (result1 == false) {
//					if (tb.getText().trim().equals("") == false) {
//						result = false;
//						changeBorderColor(widg, "#dd4b39");
//
//						InlineLabel l = formfields[k][i].getMandatoryMsgWidget();
//						if (l != null) {
//							l.setVisible(true);
//							fields[k][i].getMandatoryMsgWidget().setText("Invalid Email Format!");
//						}
//					}
//				}
//			}

			if (widg instanceof CompositeInterface) {

				CompositeInterface a = (CompositeInterface) widg;
				if(a instanceof AddressComposite){
					continue;
				}
				boolean resultadress = a.validate();
				if (resultadress == false)
					result = resultadress;
			}

			if (fields[k][i].isMandatory()) {
				InlineLabel lbl = fields[k][i].getHeaderLabel();
				Console.log("Label : "+lbl.getText());
				if(lbl.getText().equals("* Service Type")) {
					continue;
				}else if(lbl.getText().equals("* Quantity")) {
					continue;
				}else if(lbl.getText().equals("* UOM")) {
					continue;
				}else if(lbl.getText().equals("* Address Line 1")) {
					continue;
				}else if(lbl.getText().equals("* City")) {
					continue;
				}else if(lbl.getText().equals("* State")) {
					continue;
				}else if(lbl.getText().equals("* Country")) {
					continue;
				}
				
				if (widg instanceof TextBox) {
					TextBox tb = (TextBox) widg;
					String value = tb.getText();

					if (value.equals("")) {
						result = false;
						changeBorderColor(widg, "#dd4b39");
						InlineLabel l = fields[k][i].getMandatoryMsgWidget();
						if (l != null)
							l.setVisible(true);

					}

				}

				if (widg instanceof TextArea) {
					TextArea tb = (TextArea) widg;
					String value = tb.getText();

					if (value.equals("")) {
						result = false;
						changeBorderColor(widg, "#dd4b39");
						InlineLabel l = fields[k][i].getMandatoryMsgWidget();
						if (l != null)
							l.setVisible(true);

					}

				}

				else if (widg instanceof ListBox) {
					ListBox lb = (ListBox) widg;

					int index = lb.getSelectedIndex();
					if (index == 0) {
						result = false;
						changeBorderColor(widg, "#dd4b39");
						InlineLabel l = fields[k][i].getMandatoryMsgWidget();
						if (l != null)
							l.setVisible(true);

					}
				}

				else if (widg instanceof ObjectListBox) {
					ListBox lb = (ListBox) widg;

					int index = lb.getSelectedIndex();
					if (index == 0) {
						result = false;
						changeBorderColor(widg, "#dd4b39");
						InlineLabel l = fields[k][i].getMandatoryMsgWidget();
						if (l != null)
							l.setVisible(true);

					}
				}

				else if (widg instanceof DateBox) {
					DateBox db = (DateBox) widg;
					Date value = db.getValue();
					if (value == null) {
						result = false;
						changeBorderColor(widg, "#dd4b39");
						InlineLabel l = fields[k][i].getMandatoryMsgWidget();
						if (l != null)
							l.setVisible(true);

					}
				}

				if (widg instanceof DoubleBox) {
					DoubleBox db = (DoubleBox) widg;
					String value = db.getText().trim();
					if (value.equals("")) {
						result = false;
						changeBorderColor(widg, "#dd4b39");
						InlineLabel l = fields[k][i].getMandatoryMsgWidget();
						if (l != null)
							l.setVisible(true);

					}
				}

				if (widg instanceof SuggestBox) {
					SuggestBox sb = (SuggestBox) widg;
					String value = sb.getValue();

					if (value.equals("")) {
						result = false;
						changeBorderColor(widg, "#dd4b39");
						InlineLabel l = fields[k][i].getMandatoryMsgWidget();
						if (l != null)
							l.setVisible(true);
					}
				}

				if (widg instanceof LongBox) {
					LongBox sb = (LongBox) widg;
					Long value = sb.getValue();
					if (value == null) {
						result = false;
						changeBorderColor(widg, "#dd4b39");
						InlineLabel l = fields[k][i].getMandatoryMsgWidget();
						if (l != null)
							l.setVisible(true);
					}
				}

				if (widg instanceof MyLongBox) {
					MyLongBox sb = (MyLongBox) widg;

					Long value = sb.getValue();
					if (value == null) {
						result = false;
						changeBorderColor(widg, "#dd4b39");
						InlineLabel l = fields[k][i].getMandatoryMsgWidget();
						if (l != null)
							l.setVisible(true);
					}
				}

				if (widg instanceof IntegerBox) {
					IntegerBox sb = (IntegerBox) widg;

					Integer value = sb.getValue();
					if (value == null) {
						result = false;
						changeBorderColor(widg, "#dd4b39");
						InlineLabel l = fields[k][i].getMandatoryMsgWidget();
						if (l != null)
							l.setVisible(true);
					}
				}
			}
		}
	}
	return result;
}

private void changeBorderColor(Widget widg, String color) {
	widg.getElement().getStyle().setBorderColor(color);
}

/**
 *  15-01-2020 Added by Priyanka issue raised by Nitin Sir
 */
@Override
public void refreshTableData() {
	// TODO Auto-generated method stub
	super.refreshTableData();
	catchTrap.getTable().redraw();
	checkListTbl.getTable().redraw();
	pestTreatmentTbl.getTable().redraw();
	trackServiceCompletionTable.getTable().redraw();
	materialProdTable.getTable().redraw();
	table.getTable().redraw();
}

/**
 *  End
 */

public void loadServiceProject(Service view){
	Console.log("In loadServiceProject");
	MyQuerry querry = new MyQuerry();
	Vector<Filter> filterVec = new Vector<Filter>();
	Filter filter;
	
	filter = new Filter();
	filter.setQuerryString("serviceId");
	filter.setIntValue(view.getCount());
	filterVec.add(filter);
	
	querry.setFilters(filterVec);
	querry.setQuerryObject(new ServiceProject());
	showWaitSymbol();
	genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
		
		@Override
		public void onSuccess(ArrayList<SuperModel> result) {
			hideWaitSymbol();
			Console.log("Ashwini in loadServiceProject result="+result.size());
			if(result.size()==0){
//				showDialogMessage("No service project found!");
				Console.log("No  service project found!");
			}else{
				final ServiceProject project = (ServiceProject) result.get(0);
				Timer timer = new Timer() {
					
					@Override
					public void run() {
						if(project.getProdDetailsList()!=null){
							Console.log("material size ="+project.getProdDetailsList().size());
//							materialProdTable.getDataprovider().setList(project.getProdDetailsList());
//							materialProdTable.connectToLocal();
							materialProdTable.setValue(project.getProdDetailsList());
							Console.log("materialProdTable size="+materialProdTable.getDataprovider().getList().size());
						}else
							Console.log("no material found in project");
					}
				};
				timer.schedule(1000); 
			}
		}
		
		@Override
		public void onFailure(Throwable caught) {
			hideWaitSymbol();
		}
	});

	
}

}

