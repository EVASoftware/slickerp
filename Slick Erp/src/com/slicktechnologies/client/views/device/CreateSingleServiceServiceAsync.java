package com.slicktechnologies.client.views.device;

import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
public interface CreateSingleServiceServiceAsync {

	public void createSingleService(int serviceCount,Date previousDate,int serviceNo,int serviceSerialNo,int contractId,long companyId,AsyncCallback<ArrayList<Integer>> callback);
	public void createSinglecustomer(int serviceCount,ArrayList<Integer> custId,long companyId,AsyncCallback<ArrayList<Customer>>callback);//Added by Ashwini
}
