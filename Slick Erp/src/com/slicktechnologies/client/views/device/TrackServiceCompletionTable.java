package com.slicktechnologies.client.views.device;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utils.Console;

public class TrackServiceCompletionTable extends SuperTable<TrackTableDetails>{


	
	TextColumn<TrackTableDetails> StepColumn;
	TextColumn<TrackTableDetails> dateTimeColumn;
	TextColumn<TrackTableDetails> locationColumn;
	TextColumn<TrackTableDetails> kmTravelledColumn;
	

	
	public TrackServiceCompletionTable() {
		// TODO Auto-generated constructor stub
		super();
	}
	
	@Override
	public void createTable() {
		
			addColumnStep();
			addColumndateTime();
			addColumnLocation();
			addColumnKm();
		
	}

	
	private void addColumnStep() {
		StepColumn=new TextColumn<TrackTableDetails>() {
			@Override
			public String getValue(TrackTableDetails object) {
				if(object.getStatus()!=null){
					return object.getStatus();
				}else
					return "";
			}
		};
		table.addColumn(StepColumn, "Status");
		table.setColumnWidth(StepColumn,120,Unit.PX);
		StepColumn.setSortable(true);		
	}
	
	private void addColumndateTime() {
		dateTimeColumn=new TextColumn<TrackTableDetails>() {
			@Override
			public String getValue(TrackTableDetails object) {
				if(object.getDate_time()!=null){
					return object.getDate_time();
				}else
					return "";
			}
		};
		table.addColumn(dateTimeColumn, "Date-Time");
		table.setColumnWidth(dateTimeColumn,120,Unit.PX);
		dateTimeColumn.setSortable(true);		
	}
	
	private void addColumnLocation() {
		locationColumn=new TextColumn<TrackTableDetails>() {
			@Override
			public String getValue(TrackTableDetails object) {
				if(object.getLocationName()!=null){
					return object.getLocationName();
				}else
					return "";
			}
		};
		table.addColumn(locationColumn, "Location");
		table.setColumnWidth(locationColumn,120,Unit.PX);
		locationColumn.setSortable(true);		
	}
	
	private void addColumnKm() {
		kmTravelledColumn=new TextColumn<TrackTableDetails>() {
			@Override
			public String getValue(TrackTableDetails object) {
				if(object.getKiloMeters()>=0){
					return object.getKiloMeters()+"";
				}else
					return "";
			}
		};
		table.addColumn(kmTravelledColumn, "KM");
		table.setColumnWidth(kmTravelledColumn,120,Unit.PX);
		kmTravelledColumn.setSortable(true);		
	}
	
	
	
	
	
	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		System.out.println("in side field updator");
			
	}
	
	
	@Override
	public void setEnable(boolean state) {
		Console.log("setEnable in trackservice table state="+state);
		int tablecolcount = this.table.getColumnCount();
		for (int i = tablecolcount - 1; i > -1; i--)
			table.removeColumn(i);
			
		addColumnStep();
		addColumndateTime();
		addColumnLocation();
		addColumnKm();
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
	


}
