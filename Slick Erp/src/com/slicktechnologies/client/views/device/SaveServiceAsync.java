package com.slicktechnologies.client.views.device;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.slicktechnologies.shared.Service;

public interface SaveServiceAsync {
	public void saveStatus(Service service,AsyncCallback<Boolean>bool);
	
	public void mapBillingDetails(ArrayList<Service>sevicelist,AsyncCallback<ArrayList<Service>>seviceLt);
}
