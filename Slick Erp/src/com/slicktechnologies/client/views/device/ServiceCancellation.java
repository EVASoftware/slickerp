package com.slicktechnologies.client.views.device;

import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.Button;

public class ServiceCancellation extends AbsolutePanel {
	private TextArea textArea;
	private Button btnCancelService;
	public ServiceCancellation() {
		
		textArea = new TextArea();
		add(textArea, 10, 71);
		textArea.setSize("350px", "114px");
		
		InlineLabel nlnlblReasonForChange = new InlineLabel("Reason For Cancellation");
		add(nlnlblReasonForChange, 10, 41);
		nlnlblReasonForChange.setSize("166px", "18px");
		
		btnCancelService = new Button("Cancel Service");
		add(btnCancelService, 146, 201);
		this.setSize("400px","400px");
	}
	public TextArea getTextArea() {
		return textArea;
	}
	public void setTextArea(TextArea textArea) {
		this.textArea = textArea;
	}
	public Button getBtnCancelService() {
		return btnCancelService;
	}
	public void setBtnCancelService(Button btnCancelService) {
		this.btnCancelService = btnCancelService;
	}
	
	public void clear() {
		this.textArea.setText("");
		
	}
}
