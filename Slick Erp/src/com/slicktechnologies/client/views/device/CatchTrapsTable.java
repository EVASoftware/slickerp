package com.slicktechnologies.client.views.device;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.views.settings.employee.EmployeeBranch;
import com.slicktechnologies.shared.Service;

public class CatchTrapsTable extends SuperTable<CatchTraps>{

	
	TextColumn<CatchTraps>pestcountColumn;
	TextColumn<CatchTraps>pestNameColumn;
	TextColumn<CatchTraps>locationColumn;
	TextColumn<CatchTraps>numberOfBagsColumn;
	TextColumn<CatchTraps>commodityColumn;
	TextColumn<CatchTraps>quantityColumn;
	TextColumn<CatchTraps>containerNumberColumn;
	TextColumn<CatchTraps>containerSizeColumn;
	Column<CatchTraps, String> deleteColumn;
	
	TextColumn<CatchTraps>statusColumn;

	public boolean flag = false;
	
	/**
	 * @author Anil
	 * @since 26-06-2020
	 * checklist table flag
	 */
	boolean chkListFlag;
	
	@Override
	public void createTable() {
		if(!flag){
			addColumnPestName();
			addColumnPestCount();
			createCoulmnLocation();
		}else{
			createContainerNumberColumn();
			createContainerSizeColumn();
			createCommodityColumn();
		}
		createColumndeleteColumn();
		addFieldUpdater();
	}

	private void createColumndeleteColumn() {
		ButtonCell btnCell = new ButtonCell();
		deleteColumn = new Column<CatchTraps, String>(btnCell) {
			@Override
			public String getValue(CatchTraps object) {
				return "Delete";
			}
		};		
		table.addColumn(deleteColumn, "Remove");
		table.setColumnWidth(deleteColumn,120,Unit.PX);
	}

	private void addColumnPestCount() {
		pestcountColumn=new TextColumn<CatchTraps>() {
			@Override
			public String getValue(CatchTraps object) {
				if(object.getCatchLevel()!=null&&!object.getCatchLevel().equals("")) //Ashwini Patil Date:14-10-2024 for psipl
					return object.getCatchLevel();
				if(object.getCount()!=-1){
					return object.getCount()+"";
				}else
					return "N.A.";
			}
		};
		table.addColumn(pestcountColumn, "Count");
		table.setColumnWidth(pestcountColumn,120,Unit.PX);
		pestcountColumn.setSortable(true);
		
	}
	
	
	private void addColumnPestName() {
		pestNameColumn=new TextColumn<CatchTraps>() {
			@Override
			public String getValue(CatchTraps object) {
				if(object.getPestName()!=null){
				return object.getPestName()+"";
				}else
					return "N.A.";
			}
		};
		table.addColumn(pestNameColumn, "Type");
		table.setColumnWidth(pestNameColumn,120,Unit.PX);
		pestNameColumn.setSortable(true);
		
	}
	
	
	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		System.out.println("in side field updator");
		createFieldUpdaterdeleteColumn();		
	}

	protected void createFieldUpdaterdeleteColumn()
	{
		System.out.println("in side field updator method");
		deleteColumn.setFieldUpdater(new FieldUpdater<CatchTraps,String>()
				{
			@Override
			public void update(int index,CatchTraps object,String value)
			{
				getDataprovider().getList().remove(object);
				table.redrawRow(index);
			}
		});
	}
	
	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
		if(chkListFlag){
			setEnableCheckListTbl(state);
			return;
		}
		 for(int i=table.getColumnCount()-1;i>-1;i--)
	    	  table.removeColumn(i); 
	        
		if(state){
			if(!flag){
				addColumnPestName();
				addColumnPestCount();
				createCoulmnLocation();
			}else{
				createCommodityColumn();
				createContainerNumberColumn();
				createContainerSizeColumn();
				
			}
			createColumndeleteColumn();
			addFieldUpdater();
		}else{
			if(!flag){
				addColumnPestName();
				addColumnPestCount();
				createCoulmnLocation();
			}else{
				createCommodityColumn();
				createContainerNumberColumn();
				createContainerSizeColumn();
			}		
		}
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
	
	private void createCoulmnLocation(){
	locationColumn=new TextColumn<CatchTraps>() {
		@Override
		public String getValue(CatchTraps object) {
			if(object.getLocation()!=null){
			return object.getLocation()+"";
			}else
				return "N.A.";
		}
	};
	table.addColumn(locationColumn, "Location/No");
	table.setColumnWidth(locationColumn,120,Unit.PX);
	locationColumn.setSortable(true);
	
}
	
	
	private void createCommodityColumn(){
		commodityColumn=new TextColumn<CatchTraps>() {
			@Override
			public String getValue(CatchTraps object) {
				
				if(object.getPestName()!=null){
					return object.getPestName()+"";
					}else
						return "N.A.";
				}
		};
		table.addColumn(commodityColumn, "Stack No./Commodity");
		table.setColumnWidth(commodityColumn,120,Unit.PX);
		commodityColumn.setSortable(true);
		
	}

	
	private void createContainerNumberColumn(){
		containerNumberColumn=new TextColumn<CatchTraps>() {
			@Override
			public String getValue(CatchTraps object) {
				
				if(object.getContainerNo()!=null){
					return object.getContainerNo()+"";
					}else
						return "N.A.";
				}
		};
		table.addColumn(containerNumberColumn, "No. Of Bags/ Container No.");
		table.setColumnWidth(containerNumberColumn,120,Unit.PX);
		containerNumberColumn.setSortable(true);
		
	}
	private void createContainerSizeColumn(){
		containerSizeColumn=new TextColumn<CatchTraps>() {
			@Override
			public String getValue(CatchTraps object) {
				
				if(object.getContainerSize()!=null){
					return object.getContainerSize()+"";
					}else
						return "N.A.";
				}
		};
		table.addColumn(containerSizeColumn, "Total Quantity(MT)/ Container Size(ft.)");
		table.setColumnWidth(containerSizeColumn,120,Unit.PX);
		containerSizeColumn.setSortable(true);
		
	}
	public CatchTrapsTable() {
		// TODO Auto-generated constructor stub
		super();
	}
	
	public CatchTrapsTable(boolean chkLisFlag) {
		// TODO Auto-generated constructor stub
		super(chkLisFlag);
		chkListFlag=true;
		createTable1();
	}
	public void createTable1() {
		addColumnPestName();
		createCoulmnRemark();
		createColumndeleteColumn();
		createColumnStatus();
		addFieldUpdater();
	}
	
	private void createCoulmnRemark(){
		locationColumn=new TextColumn<CatchTraps>() {
			@Override
			public String getValue(CatchTraps object) {
				if(object.getLocation()!=null){
				return object.getLocation()+"";
				}else
					return "N.A.";
			}
		};
		table.addColumn(locationColumn, "Remark");
		table.setColumnWidth(locationColumn,120,Unit.PX);
		locationColumn.setSortable(true);
	}
	
	public void setEnableCheckListTbl(boolean state){
		for(int i=table.getColumnCount()-1;i>-1;i--)
	    	  table.removeColumn(i); 
	        
		if(state){
			addColumnPestName();
			createCoulmnRemark();
			createColumnStatus();
			createColumndeleteColumn();
			addFieldUpdater();
		}else{
			addColumnPestName();
			createCoulmnRemark();
			createColumnStatus();

		}
	}
	
	private void createColumnStatus() {
		statusColumn = new TextColumn<CatchTraps>() {
			
			@Override
			public String getValue(CatchTraps object) {
				if(object.getStatus()==null){
					return "";
				}
				else if(object.getStatus()){
					return "Active";
				}
				else{
					return "InActive";
				}
			}
		};
		
		table.addColumn(statusColumn, "Status");
		table.setColumnWidth(statusColumn,90,Unit.PX);
		
	}
}
