package com.slicktechnologies.client.views.device;

import com.googlecode.objectify.annotation.Embed;
import com.simplesoftwares.client.library.appstructure.SuperModel;
@Embed
public class PestTreatmentDetails extends SuperModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5990780267480919956L;
	
	
	String treatmentArea;
	String methodOfApplication;
	String pestTarget;

	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}

	public String getTreatmentArea() {
		return treatmentArea;
	}

	public void setTreatmentArea(String pestTreatmentArea) {
		this.treatmentArea = pestTreatmentArea;
	}

	public String getMethodOfApplication() {
		return methodOfApplication;
	}

	public void setMethodOfApplication(String methodOfApplication) {
		this.methodOfApplication = methodOfApplication;
	}

	public String getPestTarget() {
		return pestTarget;
	}

	public void setPestTarget(String pestTarget) {
		this.pestTarget = pestTarget;
	}

	
}
