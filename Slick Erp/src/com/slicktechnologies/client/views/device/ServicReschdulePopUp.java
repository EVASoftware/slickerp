package com.slicktechnologies.client.views.device;

import java.util.ArrayList;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.Button;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.CompositeInterface;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.Config;

public class ServicReschdulePopUp extends AbsolutePanel implements CompositeInterface,ChangeHandler
{
	private DateBox dateBox;
	private TextArea textArea;
	private Button btnReschedule;
	private Button btnCancel;
	
	
	/**
	 * Add field for edit service time
	 */
	
	private ListBox p_servicehours;
	private ListBox p_servicemin;
	private ListBox p_ampm;
	private ListBox p_seviceBranch;
	private TextBox time;
	final GenricServiceAsync service = GWT.create(GenricService.class);
	public  ArrayList<String> binList;
	public  ArrayList<CustomerBranchDetails> customerBranchList;
	public InlineLabel nlnlblNewServiceBranch; //Ashwini Patil Date:27-05-2022
	
	
	
	ObjectListBox<Config> olbSpecificReason;
	
	
	public ServicReschdulePopUp() {
		
		InlineLabel nlnlblNewServiceDate = new InlineLabel("Service Date        :");
		add(nlnlblNewServiceDate, 10, 28);
		
		dateBox = new DateBox();
		DateTimeFormat format=DateTimeFormat.getFormat("yyyy-MM-dd");
		  DateBox.DefaultFormat defformat=new DateBox.DefaultFormat(format);
		  dateBox.setFormat(defformat);
//		add(dateBox, 184, 28);
		  add(dateBox, 200, 28);
		/***************************add field start******************************************/
		
		
		time = new TextBox();
		add(time,400,63);
		time.setSize("60px", "16px");
		time.setEnabled(false);
		
		InlineLabel nlnlblNewServicetme = new InlineLabel("Service Time          :");
		add(nlnlblNewServicetme, 10, 95);
		
		
		InlineLabel hhlable=new InlineLabel(" HH ");
//		add(hhlable,189,70);
		add(hhlable,200,70);
		
		p_servicehours=new ListBox();
//		add(p_servicehours,184,95);
		add(p_servicehours,200,95);
		p_servicehours.setSize("60px", "24px");
		p_servicehours.insertItem("--", 0);
		p_servicehours.insertItem("01", 1);
		p_servicehours.insertItem("02", 2);
		p_servicehours.insertItem("03", 3);
		p_servicehours.insertItem("04", 4);
		p_servicehours.insertItem("05", 5);
		p_servicehours.insertItem("06", 6);
		p_servicehours.insertItem("07", 7);
		p_servicehours.insertItem("08", 8);
		p_servicehours.insertItem("09", 9);
		p_servicehours.insertItem("10", 10);
		p_servicehours.insertItem("11", 11);
		p_servicehours.insertItem("12", 12);

		
		InlineLabel mmlable=new InlineLabel(" MM ");
//		add(mmlable,264,70);
		add(mmlable,280,70);
		
		p_servicemin=new ListBox();
//		add(p_servicemin,259,95);
		add(p_servicemin,275,95);
		p_servicemin.setSize("60px", "24px");
		p_servicemin.insertItem("--", 0);
		p_servicemin.insertItem("00", 1);
		p_servicemin.insertItem("15", 2);
		p_servicemin.insertItem("30", 3);
		p_servicemin.insertItem("45", 4);
		
		InlineLabel sslable=new InlineLabel(" AM/PM ");
//		add(sslable,334,70);
		add(sslable,350,70);
		
		p_ampm = new ListBox();
//		add(p_ampm,329,95);
		add(p_ampm,345,95);
		p_ampm.setSize("60px", "24px");
		p_ampm.insertItem("--", 0);
		p_ampm.insertItem("AM", 1);
		p_ampm.insertItem("PM", 2);
		
		
		nlnlblNewServiceBranch = new InlineLabel("Service Branch    :");
		add(nlnlblNewServiceBranch, 10, 140);
		
		p_seviceBranch = new ListBox();
//		AppUtility.MakeLiveConfig(p_seviceBranch, Screen.CUSTOMERBRANCH);
//		add(p_seviceBranch,184,140);
		add(p_seviceBranch,200,140);
		p_seviceBranch.getElement().getStyle().setHeight(20, Unit.PX);
		
//		p_seviceBranch.setSize("100px", "24px");
		
		
//		p_seviceBranch.addItem(binList);
		
		
		
		
		
		olbSpecificReason=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbSpecificReason, Screen.SPECIFICREASONFORRESCHEDULING);
		olbSpecificReason.addChangeHandler(this);
		/**
		 *  Date : 24-03-2021 Added By Priyanka
		 *  Des : reschedule popup - label change "Client Interaction" to Reason and Remark as per Nitin Sirs Suggestion.
		 */
		
		//InlineLabel specificReasonlbl = new InlineLabel("Client Interaction      :");
//		InlineLabel specificReasonlbl = new InlineLabel("Reason      :");
		/** Added by sheetal:21-12-2021
		 *  Des: reschedule popup - label change "Reason" to "Service Reschedule Reason"as per Nitin Sirs Suggestion.
		     **/
		
		InlineLabel specificReasonlbl = new InlineLabel("Service Reschedule Reason      :");
		add(specificReasonlbl, 10, 180);
		specificReasonlbl.setSize("200px", "18px");
		
//		add(olbSpecificReason, 184, 183);
		add(olbSpecificReason, 200, 183);
		olbSpecificReason.getElement().getStyle().setHeight(20, Unit.PX);
		
		/***************************add field end******************************************/
		
		
		//InlineLabel nlnlblReasonForChange = new InlineLabel("Client Interaction      :");
		InlineLabel nlnlblReasonForChange = new InlineLabel("Remark      :");
		add(nlnlblReasonForChange, 10, 220);
		nlnlblReasonForChange.setSize("200px", "18px");
		
		textArea = new TextArea();
//		add(textArea, 184, 223);
		add(textArea, 200, 223);
		textArea.setSize("232px", "89px");
		
		btnReschedule = new Button("Reschedule");
		add(btnReschedule, 170, 342);
		
		
		btnCancel = new Button("Cancel");
		add(btnCancel,300,342);
		
		setSize("480px","410px");
		this.getElement().setId("form");
	}
	
	
	public void makeCustBranchListBox(int custmorID,long companyId,final String serBranch){
		MyQuerry querry=new MyQuerry();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter=null;
		filter=new Filter();
		filter.setQuerryString("cinfo.count");
		filter.setIntValue(custmorID);
		filtervec.add(filter);
		
		filter=new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(companyId);
		filtervec.add(filter);
		
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new CustomerBranchDetails());
//		p_seviceBranch.MakeLive(querry);
//		p_seviceBranch.addItem("Service Address");
		
		service.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
			}

		
			public void onSuccess(ArrayList<SuperModel> result) {
//				binList=new ArrayList<String>();
//				customerBranchList=new ArrayList<CustomerBranchDetails>();
//				binList.add("SELECT");
//				binList.add("Service Address");
				p_seviceBranch.clear();
				p_seviceBranch.addItem("SELECT");
				p_seviceBranch.addItem("Service Address");
				
				for (SuperModel model : result) {
					CustomerBranchDetails entity = (CustomerBranchDetails) model;
//					binList.add(entity.getBusinessUnitName());
//					customerBranchList.add(entity);
					p_seviceBranch.addItem(entity.getBusinessUnitName());
				}
				
				for(int i=0;i<getP_seviceBranch().getItemCount();i++){
					
					if(getP_seviceBranch().getItemText(i).equals(serBranch)){
						
						getP_seviceBranch().setSelectedIndex(i);
					}
				}
			}
		});
	}
	
		public DateBox getDateBox() {
			return dateBox;
		}
		public void setDateBox(DateBox dateBox) {
			this.dateBox = dateBox;
		}
		public TextArea getTextArea() {
			return textArea;
		}
		public void setTextArea(TextArea textArea) {
			this.textArea = textArea;
		}
		public Button getBtnReschedule() {
			return btnReschedule;
		}
		public void setBtnReschedule(Button btnReschedule) {
			this.btnReschedule = btnReschedule;
		}
		
		public ListBox getP_servicehours() {
			return p_servicehours;
		}
		public void setP_servicehours(ListBox p_servicehours) {
			this.p_servicehours = p_servicehours;
		}
		public ListBox getP_servicemin() {
			return p_servicemin;
		}
		public void setP_servicemin(ListBox p_servicemin) {
			this.p_servicemin = p_servicemin;
		}
		public ListBox getP_ampm() {
			return p_ampm;
		}
		public void setP_ampm(ListBox p_ampm) {
			this.p_ampm = p_ampm;
		}
		

		public ListBox getP_seviceBranch() {
			return p_seviceBranch;
		}


		public void setP_seviceBranch(ListBox p_seviceBranch) {
			if(p_seviceBranch!=null){
				this.p_seviceBranch = p_seviceBranch;
			}
			
		}


		public TextBox getTime() {
			return time;
		}


		public void setTime(TextBox time) {
			this.time = time;
		}


		public Button getBtnCancel() {
			return btnCancel;
		}


		public void setBtnCancel(Button btnCancel) {
			this.btnCancel = btnCancel;
		}


		@Override
		public void setEnable(boolean status) {
			// TODO Auto-generated method stub
			
		}
		@Override
		public boolean validate() {
			// TODO Auto-generated method stub
			return false;
		}
	
	
	public void clear() {
		this.textArea.setText("");
//		this.dateBox.getTextBox().setValue("");
		this.p_servicehours.setSelectedIndex(0);
		this.p_servicemin.setSelectedIndex(0);
		this.p_ampm.setSelectedIndex(0);
		this.olbSpecificReason.setSelectedIndex(0);
	}


	@Override
	public void onChange(ChangeEvent event) {
		if(event.getSource()==olbSpecificReason){
			if(olbSpecificReason.getValue()!=null){
				textArea.setValue(null);
				textArea.setValue(olbSpecificReason.getValue());
			}else{
				textArea.setValue(null);
			}
		}
		
	}
}
